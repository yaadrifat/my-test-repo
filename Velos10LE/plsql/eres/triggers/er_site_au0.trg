CREATE OR REPLACE TRIGGER ER_SITE_AU0 AFTER UPDATE ON ER_SITE FOR EACH ROW
DECLARE
  raid NUMBER(10);
  old_codetype VARCHAR2(200) ;
  new_codetype VARCHAR2(200) ;
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  old_account VARCHAR2(30) ;
  new_account VARCHAR2(30) ;
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  usr := Getuser(:NEW.last_modified_by);
  Audit_Trail.record_transaction
    (raid, 'ER_SITE', :OLD.rid, 'U', usr);
  IF NVL(:OLD.pk_site,0) !=
     NVL(:NEW.pk_site,0) THEN
     Audit_Trail.column_update
       (raid, 'PK_SITE',
       :OLD.pk_site, :NEW.pk_site);
  END IF;
  IF NVL(:OLD.fk_codelst_type,0) !=
     NVL(:NEW.fk_codelst_type,0) THEN
    BEGIN
	SELECT trim(codelst_desc) INTO old_codetype
	FROM ER_CODELST WHERE pk_codelst =  :OLD.fk_codelst_type ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_codetype := NULL;
    END ;
    BEGIN
	SELECT trim(codelst_desc) INTO new_codetype
	FROM ER_CODELST WHERE pk_codelst =  :NEW.fk_codelst_type ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_codetype := NULL;
    END ;
     Audit_Trail.column_update
       (raid, 'FK_CODELST_TYPE',
       old_codetype, new_codetype);
  END IF;
  IF NVL(:OLD.fk_account,0) !=
     NVL(:NEW.fk_account,0) THEN
    BEGIN
     SELECT ac_name INTO old_account
     FROM ER_ACCOUNT WHERE pk_account = :OLD.fk_account ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_account := NULL;
    END ;
    BEGIN
     SELECT ac_name INTO new_account
     FROM ER_ACCOUNT WHERE pk_account = :NEW.fk_account ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_account := NULL;
    END ;
     Audit_Trail.column_update
       (raid, 'FK_ACCOUNT',
       old_account, new_account);
  END IF;
  IF NVL(:OLD.fk_peradd,0) !=
     NVL(:NEW.fk_peradd,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_PERADD',
       :OLD.fk_peradd, :NEW.fk_peradd);
  END IF;
  IF NVL(:OLD.site_name,' ') !=
     NVL(:NEW.site_name,' ') THEN
     Audit_Trail.column_update
       (raid, 'SITE_NAME',
       :OLD.site_name, :NEW.site_name);
  END IF;
  IF NVL(:OLD.site_info,' ') !=
     NVL(:NEW.site_info,' ') THEN
     Audit_Trail.column_update
       (raid, 'SITE_INFO',
       :OLD.site_info, :NEW.site_info);
  END IF;
  -- Added by Amar
  IF NVL(:OLD.site_notes,' ') !=
     NVL(:NEW.site_notes,' ')THEN
    Audit_Trail.column_update
       (raid, 'SITE_NOTES',
       :OLD.site_notes,:NEW.site_notes);
  END IF;

  IF NVL(:OLD.site_parent,0) !=
     NVL(:NEW.site_parent,0) THEN
     Audit_Trail.column_update
       (raid, 'SITE_PARENT',
       :OLD.site_parent, :NEW.site_parent);
  END IF;
  IF NVL(:OLD.site_stat,' ') !=
     NVL(:NEW.site_stat,' ') THEN
     Audit_Trail.column_update
       (raid, 'SITE_STAT',
       :OLD.site_stat, :NEW.site_stat);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     Audit_Trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) ||', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_modby := NULL;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_modby := NULL;
    END ;
     Audit_Trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.IP_ADD,' ') !=
     NVL(:NEW.IP_ADD,' ') THEN
     Audit_Trail.column_update
       (raid, 'IP_ADD',
       :OLD.IP_ADD, :NEW.IP_ADD);
  END IF;
    IF NVL(:OLD.SITE_ID,' ') !=
     NVL(:NEW.SITE_ID,' ') THEN
     Audit_Trail.column_update
       (raid, 'SITE_ID',
       :OLD.SITE_ID, :NEW.SITE_ID);
  END IF;
  --Added by Manimaran for Enh.#U11
   IF NVL(:OLD.SITE_HIDDEN,0) !=
     NVL(:NEW.SITE_HIDDEN,0) THEN
     Audit_Trail.column_update
       (raid, 'SITE_HIDDEN',
       :OLD.SITE_HIDDEN, :NEW.SITE_HIDDEN);
  END IF;
  -- 22-Dec-2011, New column GUID added, Ankit
  IF NVL(:OLD.GUID,' ') !=
     NVL(:NEW.GUID,' ') THEN
     Audit_Trail.column_update
       (raid, 'GUID',
       :OLD.GUID, :NEW.GUID);
  END IF;

END;
/