CREATE OR REPLACE TRIGGER "ER_DYNREPVIEW_AU0"
AFTER UPDATE OF
PK_DYNREPVIEW,FK_DYNREP,DYNREPVIEW_COL,DYNREPVIEW_SEQ,DYNREPVIEW_WIDTH,DYNREPVIEW_FORMAT,DYNREPVIEW_DISPNAME,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,DYNREPVIEW_COLNAME,DYNREPVIEW_COLDATATYPE,FK_FORM,FORM_TYPE,USEUNIQUEID,USEDATAVAL,CALCPER,CALCSUM
ON ER_DYNREPVIEW REFERENCING OLD AS OLD NEW AS NEW
--Created by Gopu for Audit Report After Update
FOR EACH ROW
declare
  raid number(10);
   usr varchar2(100);
   old_modby varchar2(100);
   new_modby varchar2(100);
begin
  select seq_audit.nextval into raid from dual;
   usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_DYNREPVIEW', :old.rid, 'U', usr);
  if nvl(:old.PK_DYNREPVIEW,0) !=
     NVL(:new.PK_DYNREPVIEW,0) then
     audit_trail.column_update
       (raid, 'PK_DYNREPVIEW',
       :old.PK_DYNREPVIEW, :new.PK_DYNREPVIEW);
  end if;

  if nvl(:old.FK_DYNREP,0) !=
     NVL(:new.FK_DYNREP,0) then
     audit_trail.column_update
       (raid, 'FK_DYNREP',
       :old.FK_DYNREP, :new.FK_DYNREP);
  end if;

if nvl(:old.DYNREPVIEW_COL,' ') !=
     NVL(:new.DYNREPVIEW_COL,' ') then
     audit_trail.column_update
       (raid, 'DYNREPVIEW_COL',
       :old.DYNREPVIEW_COL, :new.DYNREPVIEW_COL);
  end if;

 if nvl(:old.DYNREPVIEW_SEQ,0) !=
     NVL(:new.DYNREPVIEW_SEQ,0) then
     audit_trail.column_update
       (raid, 'DYNREPVIEW_SEQ',
       :old.DYNREPVIEW_SEQ, :new.DYNREPVIEW_SEQ);
  end if;

if nvl(:old.DYNREPVIEW_WIDTH,' ') !=
     NVL(:new.DYNREPVIEW_WIDTH,' ') then
     audit_trail.column_update
       (raid, 'DYNREPVIEW_WIDTH',
       :old.DYNREPVIEW_WIDTH, :new.DYNREPVIEW_WIDTH);
  end if;

if nvl(:old.DYNREPVIEW_FORMAT,' ') !=
     NVL(:new.DYNREPVIEW_FORMAT,' ') then
     audit_trail.column_update
       (raid, 'DYNREPVIEW_FORMAT',
       :old.DYNREPVIEW_FORMAT, :new.DYNREPVIEW_FORMAT);
  end if;

if nvl(:old.DYNREPVIEW_DISPNAME,' ') !=
     NVL(:new.DYNREPVIEW_DISPNAME,' ') then
     audit_trail.column_update
       (raid, 'DYNREPVIEW_DISPNAME',
       :old.DYNREPVIEW_DISPNAME, :new.DYNREPVIEW_DISPNAME);
  end if;

if nvl(:old.RID,0) !=
     NVL(:new.RID,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.RID, :new.RID);
  end if;

if nvl(:old.LAST_MODIFIED_BY,0) !=
 NVL(:new.LAST_MODIFIED_BY,0) then
	Begin
		Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
		into old_modby from er_user  where pk_user = :old.last_modified_by ;
		Exception When NO_DATA_FOUND then
			old_modby := null;
	End ;
	Begin
		Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
		into new_modby   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
		Exception When NO_DATA_FOUND then
			new_modby := null;
	 End ;
	audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
 end if;

if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;

if nvl(:old.IP_ADD,' ') !=
     NVL(:new.IP_ADD,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.IP_ADD, :new.IP_ADD);
  end if;

if nvl(:old.DYNREPVIEW_COLNAME,' ') !=
     NVL(:new.DYNREPVIEW_COLNAME,' ') then
     audit_trail.column_update
       (raid, 'DYNREPVIEW_COLNAME',
       :old.DYNREPVIEW_COLNAME, :new.DYNREPVIEW_COLNAME);
  end if;

if nvl(:old.DYNREPVIEW_COLDATATYPE, ' ') !=
     NVL(:new.DYNREPVIEW_COLDATATYPE, ' ') then
     audit_trail.column_update
       (raid, 'DYNREPVIEW_COLDATATYPE',
       :old.DYNREPVIEW_COLDATATYPE, :new.DYNREPVIEW_COLDATATYPE);
  end if;

if nvl(:old.FK_FORM,0) !=
     NVL(:new.FK_FORM,0) then
     audit_trail.column_update
       (raid, 'FK_FORM',
       :old.FK_FORM, :new.FK_FORM);
  end if;

if nvl(:old.FORM_TYPE,' ') !=
     NVL(:new.FORM_TYPE,' ') then
     audit_trail.column_update
       (raid, 'FORM_TYPE',
       :old.FORM_TYPE, :new.FORM_TYPE);
  end if;

if nvl(:old.USEUNIQUEID,0) !=
     NVL(:new.USEUNIQUEID,0) then
     audit_trail.column_update
       (raid, 'USEUNIQUEID',
       :old.USEUNIQUEID, :new.USEUNIQUEID);
  end if;

if nvl(:old.USEDATAVAL,0) !=
     NVL(:new.USEDATAVAL,0) then
     audit_trail.column_update
       (raid, 'USEDATAVAL',
       :old.USEDATAVAL, :new.USEDATAVAL);
  end if;


if nvl(:old.CALCPER,0) !=
     NVL(:new.CALCPER,0) then
     audit_trail.column_update
       (raid, 'CALCPER',
       :old.CALCPER, :new.CALCPER);
  end if;

if nvl(:old.CALCSUM,0) !=
     NVL(:new.CALCSUM,0) then
     audit_trail.column_update
       (raid, 'CALCSUM',
       :old.CALCSUM, :new.CALCSUM);
  end if;
if nvl(:old.repeat_number,0) !=
     NVL(:new.repeat_number,0) then
     audit_trail.column_update
       (raid, 'REPEAT_NUMBER',
       :old.repeat_number, :new.repeat_number);
  end if;


end;
/


