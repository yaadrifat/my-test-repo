CREATE OR REPLACE TRIGGER "ER_CRFFORMS_AU1" 
BEFORE UPDATE OF RECORD_TYPE
ON ER_CRFFORMS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
NEW.RECORD_TYPE ='D'
      )
DECLARE


BEGIN

/* Whenever an answered form is deleted its datacount is decreased by one */
UPDATE
er_linkedforms
SET lf_datacnt = lf_datacnt - 1
WHERE fk_formlib = :NEW.fk_formlib ;

/*Whenever an answered form is deleted, delete data from er_formslinear and er_formslineardata */

DELETE FROM er_formslinear
WHERE  fk_filledform = :NEW.pk_crfforms  AND fk_form = :NEW.fk_formlib ;





END;
/


