CREATE OR REPLACE TRIGGER "ER_FORMSLINEAR_AU1" 
AFTER UPDATE
ON ER_FORMSLINEAR
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

v_oldStatus VARCHAR2(500);
v_newStatus VARCHAR2(500);

usrName VARCHAR2(500);
 pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'ER_FL2_TRIGGER', pLEVEL  => Plog.LFATAL);
/******************************************************************************
   NAME:       ER_FORMSLINEAR_AU1
   PURPOSE:    After update of er_formslinear for forms audit trail

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        5/23/2006             1. Created this trigger.

   NOTES:
   This is the second after update trigger. The code for was first after update trigger was too large to be modified...
******************************************************************************/
BEGIN

   SELECT Getuser(:NEW.last_modified_by)INTO usrName FROM dual;


    IF NVL(:OLD.FORM_COMPLETED,0) != NVL(:NEW.FORM_COMPLETED,0) THEN
   BEGIN
   		SELECT F_Codelst_Desc(:OLD.FORM_COMPLETED)
		INTO v_oldStatus from dual;
   EXCEPTION WHEN OTHERS THEN
      		 v_oldStatus := '';
   END;
   BEGIN
   		SELECT F_Codelst_Desc(:new.FORM_COMPLETED)
		INTO v_newStatus  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 v_newStatus := '';
   END;

INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,
fa_oldvalue,fa_newvalue,fa_modifiedby_name)
VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,'Form Status',SYSDATE,'Form Status',
 v_oldStatus,  v_newStatus ,usrName);

  END IF;

 IF NVL(:OLD.COL502,' ') != NVL(:NEW.COL502,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col502'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col502'),:OLD.COL502,:NEW.COL502,usrName);
  END IF;

IF NVL(:OLD.COL503,' ') != NVL(:NEW.COL503,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col503'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col503'),:OLD.COL503,:NEW.COL503,usrName);
  END IF;

IF NVL(:OLD.COL504,' ') != NVL(:NEW.COL504,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col504'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col504'),:OLD.COL504,:NEW.COL504,usrName);
  END IF;

IF NVL(:OLD.COL505,' ') != NVL(:NEW.COL505,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col505'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col505'),:OLD.COL505,:NEW.COL505,usrName);
  END IF;

IF NVL(:OLD.COL506,' ') != NVL(:NEW.COL506,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col506'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col506'),:OLD.COL506,:NEW.COL506,usrName);
  END IF;

IF NVL(:OLD.COL507,' ') != NVL(:NEW.COL507,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col507'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col507'),:OLD.COL507,:NEW.COL507,usrName);
  END IF;

IF NVL(:OLD.COL508,' ') != NVL(:NEW.COL508,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col508'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col508'),:OLD.COL508,:NEW.COL508,usrName);
  END IF;

IF NVL(:OLD.COL509,' ') != NVL(:NEW.COL509,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col509'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col509'),:OLD.COL509,:NEW.COL509,usrName);
  END IF;

IF NVL(:OLD.COL510,' ') != NVL(:NEW.COL510,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col510'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col510'),:OLD.COL510,:NEW.COL510,usrName);
  END IF;


IF NVL(:OLD.COL511,' ') != NVL(:NEW.COL511,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col511'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col511'),:OLD.COL511,:NEW.COL511,usrName);
  END IF;

IF NVL(:OLD.COL512,' ') != NVL(:NEW.COL512,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col512'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col512'),:OLD.COL512,:NEW.COL512,usrName);
  END IF;

IF NVL(:OLD.COL513,' ') != NVL(:NEW.COL513,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col513'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col513'),:OLD.COL513,:NEW.COL513,usrName);
  END IF;

IF NVL(:OLD.COL514,' ') != NVL(:NEW.COL514,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col514'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col514'),:OLD.COL514,:NEW.COL514,usrName);
  END IF;

  IF NVL(:OLD.COL515,' ') != NVL(:NEW.COL515,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col515'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col515'),:OLD.COL515,:NEW.COL515,usrName);
  END IF;

IF NVL(:OLD.COL516,' ') != NVL(:NEW.COL516,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col516'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col516'),:OLD.COL516,:NEW.COL516,usrName);
  END IF;

IF NVL(:OLD.COL517,' ') != NVL(:NEW.COL517,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col517'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col517'),:OLD.COL517,:NEW.COL517,usrName);
  END IF;

IF NVL(:OLD.COL518,' ') != NVL(:NEW.COL518,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col518'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col518'),:OLD.COL518,:NEW.COL518,usrName);
  END IF;

IF NVL(:OLD.COL519,' ') != NVL(:NEW.COL519,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col519'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col519'),:OLD.COL519,:NEW.COL519,usrName);
  END IF;

IF NVL(:OLD.COL520,' ') != NVL(:NEW.COL520,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col520'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col520'),:OLD.COL520,:NEW.COL520,usrName);
  END IF;

IF NVL(:OLD.COL521,' ') != NVL(:NEW.COL521,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col521'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col521'),:OLD.COL521,:NEW.COL521,usrName);
  END IF;

IF NVL(:OLD.COL522,' ') != NVL(:NEW.COL522,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col522'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col522'),:OLD.COL522,:NEW.COL522,usrName);
  END IF;

IF NVL(:OLD.COL523,' ') != NVL(:NEW.COL523,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col523'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col523'),:OLD.COL523,:NEW.COL523,usrName);
  END IF;

IF NVL(:OLD.COL524,' ') != NVL(:NEW.COL524,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col524'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col524'),:OLD.COL524,:NEW.COL524,usrName);
  END IF;

IF NVL(:OLD.COL525,' ') != NVL(:NEW.COL525,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col525'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col525'),:OLD.COL525,:NEW.COL525,usrName);
  END IF;

IF NVL(:OLD.COL526,' ') != NVL(:NEW.COL526,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col526'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col526'),:OLD.COL526,:NEW.COL526,usrName);
  END IF;

IF NVL(:OLD.COL527,' ') != NVL(:NEW.COL527,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col527'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col527'),:OLD.COL527,:NEW.COL527,usrName);
  END IF;

IF NVL(:OLD.COL528,' ') != NVL(:NEW.COL528,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col528'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col528'),:OLD.COL528,:NEW.COL528,usrName);
  END IF;

IF NVL(:OLD.COL529,' ') != NVL(:NEW.COL529,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col529'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col529'),:OLD.COL529,:NEW.COL529,usrName);
  END IF;

IF NVL(:OLD.COL530,' ') != NVL(:NEW.COL530,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col530'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col530'),:OLD.COL530,:NEW.COL530,usrName);
  END IF;

IF NVL(:OLD.COL531,' ') != NVL(:NEW.COL531,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col531'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col531'),:OLD.COL531,:NEW.COL531,usrName);
  END IF;

IF NVL(:OLD.COL532,' ') != NVL(:NEW.COL532,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col532'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col532'),:OLD.COL532,:NEW.COL532,usrName);
  END IF;

IF NVL(:OLD.COL533,' ') != NVL(:NEW.COL533,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col533'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col533'),:OLD.COL533,:NEW.COL533,usrName);
  END IF;

IF NVL(:OLD.COL534,' ') != NVL(:NEW.COL534,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col534'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col534'),:OLD.COL534,:NEW.COL534,usrName);
  END IF;

IF NVL(:OLD.COL535,' ') != NVL(:NEW.COL535,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col535'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col535'),:OLD.COL535,:NEW.COL535,usrName);
  END IF;

IF NVL(:OLD.COL536,' ') != NVL(:NEW.COL536,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col536'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col536'),:OLD.COL536,:NEW.COL536,usrName);
  END IF;

IF NVL(:OLD.COL537,' ') != NVL(:NEW.COL537,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col537'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col537'),:OLD.COL537,:NEW.COL537,usrName);
  END IF;

IF NVL(:OLD.COL538,' ') != NVL(:NEW.COL538,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col538'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col538'),:OLD.COL538,:NEW.COL538,usrName);
  END IF;

IF NVL(:OLD.COL539,' ') != NVL(:NEW.COL539,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col539'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col539'),:OLD.COL539,:NEW.COL539,usrName);
  END IF;

IF NVL(:OLD.COL540,' ') != NVL(:NEW.COL540,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col540'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col540'),:OLD.COL540,:NEW.COL540,usrName);
  END IF;

IF NVL(:OLD.COL541,' ') != NVL(:NEW.COL541,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col541'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col541'),:OLD.COL541,:NEW.COL541,usrName);
  END IF;

IF NVL(:OLD.COL542,' ') != NVL(:NEW.COL542,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col542'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col542'),:OLD.COL542,:NEW.COL542,usrName);
  END IF;

IF NVL(:OLD.COL543,' ') != NVL(:NEW.COL543,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col543'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col543'),:OLD.COL543,:NEW.COL543,usrName);
  END IF;

IF NVL(:OLD.COL544,' ') != NVL(:NEW.COL544,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col544'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col544'),:OLD.COL544,:NEW.COL544,usrName);
  END IF;

IF NVL(:OLD.COL545,' ') != NVL(:NEW.COL545,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col545'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col545'),:OLD.COL545,:NEW.COL545,usrName);
  END IF;

IF NVL(:OLD.COL546,' ') != NVL(:NEW.COL546,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col546'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col546'),:OLD.COL546,:NEW.COL546,usrName);
  END IF;

IF NVL(:OLD.COL547,' ') != NVL(:NEW.COL547,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col547'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col547'),:OLD.COL547,:NEW.COL547,usrName);
  END IF;

IF NVL(:OLD.COL548,' ') != NVL(:NEW.COL548,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col548'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col548'),:OLD.COL548,:NEW.COL548,usrName);
  END IF;

IF NVL(:OLD.COL549,' ') != NVL(:NEW.COL549,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col549'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col549'),:OLD.COL549,:NEW.COL549,usrName);
  END IF;

IF NVL(:OLD.COL550,' ') != NVL(:NEW.COL550,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col550'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col550'),:OLD.COL550,:NEW.COL550,usrName);
  END IF;

IF NVL(:OLD.COL551,' ') != NVL(:NEW.COL551,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col551'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col551'),:OLD.COL551,:NEW.COL551,usrName);
  END IF;

IF NVL(:OLD.COL552,' ') != NVL(:NEW.COL552,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col552'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col552'),:OLD.COL552,:NEW.COL552,usrName);
  END IF;

IF NVL(:OLD.COL553,' ') != NVL(:NEW.COL553,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col553'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col553'),:OLD.COL553,:NEW.COL553,usrName);
  END IF;

IF NVL(:OLD.COL554,' ') != NVL(:NEW.COL554,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col554'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col554'),:OLD.COL554,:NEW.COL554,usrName);
  END IF;

IF NVL(:OLD.COL555,' ') != NVL(:NEW.COL555,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col555'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col555'),:OLD.COL555,:NEW.COL555,usrName);
  END IF;

IF NVL(:OLD.COL556,' ') != NVL(:NEW.COL556,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col556'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col556'),:OLD.COL556,:NEW.COL556,usrName);
  END IF;

IF NVL(:OLD.COL557,' ') != NVL(:NEW.COL557,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col557'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col557'),:OLD.COL557,:NEW.COL557,usrName);
  END IF;

IF NVL(:OLD.COL558,' ') != NVL(:NEW.COL558,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col558'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col558'),:OLD.COL558,:NEW.COL558,usrName);
  END IF;

IF NVL(:OLD.COL559,' ') != NVL(:NEW.COL559,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col559'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col559'),:OLD.COL559,:NEW.COL559,usrName);
  END IF;

IF NVL(:OLD.COL560,' ') != NVL(:NEW.COL560,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col560'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col560'),:OLD.COL560,:NEW.COL560,usrName);
  END IF;

IF NVL(:OLD.COL561,' ') != NVL(:NEW.COL561,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col561'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col561'),:OLD.COL561,:NEW.COL561,usrName);
  END IF;

IF NVL(:OLD.COL562,' ') != NVL(:NEW.COL562,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col562'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col562'),:OLD.COL562,:NEW.COL562,usrName);
  END IF;

IF NVL(:OLD.COL563,' ') != NVL(:NEW.COL563,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col563'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col563'),:OLD.COL563,:NEW.COL563,usrName);
  END IF;

IF NVL(:OLD.COL564,' ') != NVL(:NEW.COL564,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col564'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col564'),:OLD.COL564,:NEW.COL564,usrName);
  END IF;

IF NVL(:OLD.COL565,' ') != NVL(:NEW.COL565,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col565'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col565'),:OLD.COL565,:NEW.COL565,usrName);
  END IF;

IF NVL(:OLD.COL566,' ') != NVL(:NEW.COL566,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col566'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col566'),:OLD.COL566,:NEW.COL566,usrName);
  END IF;

IF NVL(:OLD.COL567,' ') != NVL(:NEW.COL567,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col567'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col567'),:OLD.COL567,:NEW.COL567,usrName);
  END IF;

IF NVL(:OLD.COL568,' ') != NVL(:NEW.COL568,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col568'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col568'),:OLD.COL568,:NEW.COL568,usrName);
  END IF;

IF NVL(:OLD.COL569,' ') != NVL(:NEW.COL569,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col569'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col569'),:OLD.COL569,:NEW.COL569,usrName);
  END IF;

IF NVL(:OLD.COL570,' ') != NVL(:NEW.COL570,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col570'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col570'),:OLD.COL570,:NEW.COL570,usrName);
  END IF;

IF NVL(:OLD.COL571,' ') != NVL(:NEW.COL571,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col571'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col571'),:OLD.COL571,:NEW.COL571,usrName);
  END IF;


IF NVL(:OLD.COL572,' ') != NVL(:NEW.COL572,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col572'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col572'),:OLD.COL572,:NEW.COL572,usrName);
  END IF;

IF NVL(:OLD.COL573,' ') != NVL(:NEW.COL573,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col573'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col573'),:OLD.COL573,:NEW.COL573,usrName);
  END IF;

IF NVL(:OLD.COL574,' ') != NVL(:NEW.COL574,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col574'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col574'),:OLD.COL574,:NEW.COL574,usrName);
  END IF;

IF NVL(:OLD.COL575,' ') != NVL(:NEW.COL575,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col575'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col575'),:OLD.COL575,:NEW.COL575,usrName);
  END IF;

IF NVL(:OLD.COL576,' ') != NVL(:NEW.COL576,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col576'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col576'),:OLD.COL576,:NEW.COL576,usrName);
  END IF;

IF NVL(:OLD.COL577,' ') != NVL(:NEW.COL577,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col577'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col577'),:OLD.COL577,:NEW.COL577,usrName);
  END IF;

IF NVL(:OLD.COL578,' ') != NVL(:NEW.COL578,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col578'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col578'),:OLD.COL578,:NEW.COL578,usrName);
  END IF;

IF NVL(:OLD.COL579,' ') != NVL(:NEW.COL579,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col579'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col579'),:OLD.COL579,:NEW.COL579,usrName);
  END IF;

IF NVL(:OLD.COL580,' ') != NVL(:NEW.COL580,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col580'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col580'),:OLD.COL580,:NEW.COL580,usrName);
  END IF;

IF NVL(:OLD.COL581,' ') != NVL(:NEW.COL581,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col581'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col581'),:OLD.COL581,:NEW.COL581,usrName);
  END IF;

IF NVL(:OLD.COL582,' ') != NVL(:NEW.COL582,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col582'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col582'),:OLD.COL582,:NEW.COL582,usrName);
  END IF;

IF NVL(:OLD.COL583,' ') != NVL(:NEW.COL583,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col583'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col583'),:OLD.COL583,:NEW.COL583,usrName);
  END IF;

IF NVL(:OLD.COL584,' ') != NVL(:NEW.COL584,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col584'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col584'),:OLD.COL584,:NEW.COL584,usrName);
  END IF;

IF NVL(:OLD.COL585,' ') != NVL(:NEW.COL585,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col585'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col585'),:OLD.COL585,:NEW.COL585,usrName);
  END IF;

IF NVL(:OLD.COL586,' ') != NVL(:NEW.COL586,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col586'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col586'),:OLD.COL586,:NEW.COL586,usrName);
  END IF;

IF NVL(:OLD.COL587,' ') != NVL(:NEW.COL587,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col587'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col587'),:OLD.COL587,:NEW.COL587,usrName);
  END IF;

IF NVL(:OLD.COL588,' ') != NVL(:NEW.COL588,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col588'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col588'),:OLD.COL588,:NEW.COL588,usrName);
  END IF;

IF NVL(:OLD.COL589,' ') != NVL(:NEW.COL589,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col589'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col589'),:OLD.COL589,:NEW.COL589,usrName);
  END IF;

IF NVL(:OLD.COL590,' ') != NVL(:NEW.COL590,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col590'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col590'),:OLD.COL590,:NEW.COL590,usrName);
  END IF;

IF NVL(:OLD.COL591,' ') != NVL(:NEW.COL591,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col591'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col591'),:OLD.COL591,:NEW.COL591,usrName);
  END IF;

IF NVL(:OLD.COL592,' ') != NVL(:NEW.COL592,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col592'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col592'),:OLD.COL592,:NEW.COL592,usrName);
  END IF;

IF NVL(:OLD.COL593,' ') != NVL(:NEW.COL593,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col593'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col593'),:OLD.COL593,:NEW.COL593,usrName);
  END IF;

IF NVL(:OLD.COL594,' ') != NVL(:NEW.COL594,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col594'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col594'),:OLD.COL594,:NEW.COL594,usrName);
  END IF;

IF NVL(:OLD.COL595,' ') != NVL(:NEW.COL595,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col595'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col595'),:OLD.COL595,:NEW.COL595,usrName);
  END IF;

IF NVL(:OLD.COL596,' ') != NVL(:NEW.COL596,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col596'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col596'),:OLD.COL596,:NEW.COL596,usrName);
  END IF;

 IF NVL(:OLD.COL597,' ') != NVL(:NEW.COL597,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col597'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col597'),:OLD.COL597,:NEW.COL597,usrName);
  END IF;

IF NVL(:OLD.COL598,' ') != NVL(:NEW.COL598,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col598'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col598'),:OLD.COL598,:NEW.COL598,usrName);
  END IF;

IF NVL(:OLD.COL599,' ') != NVL(:NEW.COL599,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col599'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col599'),:OLD.COL599,:NEW.COL599,usrName);
  END IF;

IF NVL(:OLD.COL600,' ') != NVL(:NEW.COL600,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col600'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col600'),:OLD.COL600,:NEW.COL600,usrName);
  END IF;

IF NVL(:OLD.COL601,' ') != NVL(:NEW.COL601,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col601'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col601'),:OLD.COL601,:NEW.COL601,usrName);
  END IF;

IF NVL(:OLD.COL602,' ') != NVL(:NEW.COL602,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col602'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col602'),:OLD.COL602,:NEW.COL602,usrName);
  END IF;

IF NVL(:OLD.COL603,' ') != NVL(:NEW.COL603,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col603'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col603'),:OLD.COL603,:NEW.COL603,usrName);
  END IF;

IF NVL(:OLD.COL604,' ') != NVL(:NEW.COL604,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col604'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col604'),:OLD.COL604,:NEW.COL604,usrName);
  END IF;

IF NVL(:OLD.COL605,' ') != NVL(:NEW.COL605,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col605'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col605'),:OLD.COL605,:NEW.COL605,usrName);
  END IF;

IF NVL(:OLD.COL606,' ') != NVL(:NEW.COL606,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col606'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col606'),:OLD.COL606,:NEW.COL606,usrName);
  END IF;

IF NVL(:OLD.COL607,' ') != NVL(:NEW.COL607,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col607'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col607'),:OLD.COL607,:NEW.COL607,usrName);
  END IF;

IF NVL(:OLD.COL608,' ') != NVL(:NEW.COL608,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col608'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col608'),:OLD.COL608,:NEW.COL608,usrName);
  END IF;

IF NVL(:OLD.COL609,' ') != NVL(:NEW.COL609,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col609'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col609'),:OLD.COL609,:NEW.COL609,usrName);
  END IF;

IF NVL(:OLD.COL610,' ') != NVL(:NEW.COL610,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col610'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col610'),:OLD.COL610,:NEW.COL610,usrName);
  END IF;


IF NVL(:OLD.COL611,' ') != NVL(:NEW.COL611,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col611'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col611'),:OLD.COL611,:NEW.COL611,usrName);
  END IF;

IF NVL(:OLD.COL612,' ') != NVL(:NEW.COL612,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col612'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col612'),:OLD.COL612,:NEW.COL612,usrName);
  END IF;

IF NVL(:OLD.COL613,' ') != NVL(:NEW.COL613,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col613'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col613'),:OLD.COL613,:NEW.COL613,usrName);
  END IF;

IF NVL(:OLD.COL614,' ') != NVL(:NEW.COL614,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col614'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col614'),:OLD.COL614,:NEW.COL614,usrName);
  END IF;

IF NVL(:OLD.COL615,' ') != NVL(:NEW.COL615,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col615'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col615'),:OLD.COL615,:NEW.COL615,usrName);
  END IF;

IF NVL(:OLD.COL616,' ') != NVL(:NEW.COL616,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col616'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col616'),:OLD.COL616,:NEW.COL616,usrName);
  END IF;

IF NVL(:OLD.COL617,' ') != NVL(:NEW.COL617,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col617'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col617'),:OLD.COL617,:NEW.COL617,usrName);
  END IF;

IF NVL(:OLD.COL618,' ') != NVL(:NEW.COL618,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col618'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col618'),:OLD.COL618,:NEW.COL618,usrName);
  END IF;

IF NVL(:OLD.COL619,' ') != NVL(:NEW.COL619,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col619'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col619'),:OLD.COL619,:NEW.COL619,usrName);
  END IF;

IF NVL(:OLD.COL620,' ') != NVL(:NEW.COL620,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col620'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col620'),:OLD.COL620,:NEW.COL620,usrName);
  END IF;


IF NVL(:OLD.COL621,' ') != NVL(:NEW.COL621,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col621'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col621'),:OLD.COL621,:NEW.COL621,usrName);
  END IF;

IF NVL(:OLD.COL622,' ') != NVL(:NEW.COL622,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col622'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col622'),:OLD.COL622,:NEW.COL622,usrName);
  END IF;

IF NVL(:OLD.COL623,' ') != NVL(:NEW.COL623,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col623'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col623'),:OLD.COL623,:NEW.COL623,usrName);
  END IF;

IF NVL(:OLD.COL624,' ') != NVL(:NEW.COL624,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col624'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col624'),:OLD.COL624,:NEW.COL624,usrName);
  END IF;

IF NVL(:OLD.COL625,' ') != NVL(:NEW.COL625,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col625'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col625'),:OLD.COL625,:NEW.COL625,usrName);
  END IF;

IF NVL(:OLD.COL626,' ') != NVL(:NEW.COL626,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col626'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col626'),:OLD.COL626,:NEW.COL626,usrName);
  END IF;

IF NVL(:OLD.COL627,' ') != NVL(:NEW.COL627,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col627'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col627'),:OLD.COL627,:NEW.COL627,usrName);
  END IF;

IF NVL(:OLD.COL628,' ') != NVL(:NEW.COL628,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col628'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col628'),:OLD.COL628,:NEW.COL628,usrName);
  END IF;

IF NVL(:OLD.COL629,' ') != NVL(:NEW.COL629,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col629'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col629'),:OLD.COL629,:NEW.COL629,usrName);
  END IF;

IF NVL(:OLD.COL630,' ') != NVL(:NEW.COL630,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col630'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col630'),:OLD.COL630,:NEW.COL630,usrName);
  END IF;

IF NVL(:OLD.COL631,' ') != NVL(:NEW.COL631,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col631'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col631'),:OLD.COL631,:NEW.COL631,usrName);
  END IF;

IF NVL(:OLD.COL632,' ') != NVL(:NEW.COL632,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col632'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col632'),:OLD.COL632,:NEW.COL632,usrName);
  END IF;

IF NVL(:OLD.COL633,' ') != NVL(:NEW.COL633,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col633'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col633'),:OLD.COL633,:NEW.COL633,usrName);
  END IF;

IF NVL(:OLD.COL634,' ') != NVL(:NEW.COL634,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col634'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col634'),:OLD.COL634,:NEW.COL634,usrName);
  END IF;

IF NVL(:OLD.COL635,' ') != NVL(:NEW.COL635,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col635'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col635'),:OLD.COL635,:NEW.COL635,usrName);
  END IF;

IF NVL(:OLD.COL636,' ') != NVL(:NEW.COL636,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col636'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col636'),:OLD.COL636,:NEW.COL636,usrName);
  END IF;


IF NVL(:OLD.COL637,' ') != NVL(:NEW.COL637,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col637'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col637'),:OLD.COL637,:NEW.COL637,usrName);
  END IF;

IF NVL(:OLD.COL638,' ') != NVL(:NEW.COL638,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col638'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col638'),:OLD.COL638,:NEW.COL638,usrName);
  END IF;

IF NVL(:OLD.COL639,' ') != NVL(:NEW.COL639,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col639'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col639'),:OLD.COL639,:NEW.COL639,usrName);
  END IF;

IF NVL(:OLD.COL640,' ') != NVL(:NEW.COL640,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col640'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col640'),:OLD.COL640,:NEW.COL640,usrName);
  END IF;

IF NVL(:OLD.COL641,' ') != NVL(:NEW.COL641,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col641'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col641'),:OLD.COL641,:NEW.COL641,usrName);
  END IF;

IF NVL(:OLD.COL642,' ') != NVL(:NEW.COL642,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col642'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col642'),:OLD.COL642,:NEW.COL642,usrName);
  END IF;

IF NVL(:OLD.COL643,' ') != NVL(:NEW.COL643,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col643'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col643'),:OLD.COL643,:NEW.COL643,usrName);
  END IF;

IF NVL(:OLD.COL644,' ') != NVL(:NEW.COL644,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col644'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col644'),:OLD.COL644,:NEW.COL644,usrName);
  END IF;

IF NVL(:OLD.COL645,' ') != NVL(:NEW.COL645,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col645'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col645'),:OLD.COL645,:NEW.COL645,usrName);
  END IF;

IF NVL(:OLD.COL646,' ') != NVL(:NEW.COL646,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col646'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col646'),:OLD.COL646,:NEW.COL646,usrName);
  END IF;

IF NVL(:OLD.COL647,' ') != NVL(:NEW.COL647,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col647'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col647'),:OLD.COL647,:NEW.COL647,usrName);
  END IF;

IF NVL(:OLD.COL648,' ') != NVL(:NEW.COL648,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col648'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col648'),:OLD.COL648,:NEW.COL648,usrName);
  END IF;

IF NVL(:OLD.COL649,' ') != NVL(:NEW.COL649,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col649'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col649'),:OLD.COL649,:NEW.COL649,usrName);
  END IF;

IF NVL(:OLD.COL650,' ') != NVL(:NEW.COL650,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col650'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col650'),:OLD.COL650,:NEW.COL650,usrName);
  END IF;

IF NVL(:OLD.COL651,' ') != NVL(:NEW.COL651,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col651'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col651'),:OLD.COL651,:NEW.COL651,usrName);
  END IF;

IF NVL(:OLD.COL652,' ') != NVL(:NEW.COL652,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col652'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col652'),:OLD.COL652,:NEW.COL652,usrName);
  END IF;

IF NVL(:OLD.COL653,' ') != NVL(:NEW.COL653,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col653'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col653'),:OLD.COL653,:NEW.COL653,usrName);
  END IF;

IF NVL(:OLD.COL654,' ') != NVL(:NEW.COL654,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col654'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col654'),:OLD.COL654,:NEW.COL654,usrName);
  END IF;

IF NVL(:OLD.COL655,' ') != NVL(:NEW.COL655,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col655'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col655'),:OLD.COL655,:NEW.COL655,usrName);
  END IF;

IF NVL(:OLD.COL656,' ') != NVL(:NEW.COL656,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col656'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col656'),:OLD.COL656,:NEW.COL656,usrName);
  END IF;

IF NVL(:OLD.COL657,' ') != NVL(:NEW.COL657,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col657'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col657'),:OLD.COL657,:NEW.COL657,usrName);
  END IF;

IF NVL(:OLD.COL658,' ') != NVL(:NEW.COL658,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col658'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col658'),:OLD.COL658,:NEW.COL658,usrName);
  END IF;

IF NVL(:OLD.COL659,' ') != NVL(:NEW.COL659,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col659'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col659'),:OLD.COL659,:NEW.COL659,usrName);
  END IF;

IF NVL(:OLD.COL660,' ') != NVL(:NEW.COL660,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col660'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col660'),:OLD.COL660,:NEW.COL660,usrName);
  END IF;

IF NVL(:OLD.COL661,' ') != NVL(:NEW.COL661,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col661'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col661'),:OLD.COL661,:NEW.COL661,usrName);
  END IF;

IF NVL(:OLD.COL662,' ') != NVL(:NEW.COL662,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col662'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col662'),:OLD.COL662,:NEW.COL662,usrName);
  END IF;

IF NVL(:OLD.COL663,' ') != NVL(:NEW.COL663,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col663'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col663'),:OLD.COL663,:NEW.COL663,usrName);
  END IF;

IF NVL(:OLD.COL664,' ') != NVL(:NEW.COL664,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col664'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col664'),:OLD.COL664,:NEW.COL664,usrName);
  END IF;

IF NVL(:OLD.COL665,' ') != NVL(:NEW.COL665,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col665'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col665'),:OLD.COL665,:NEW.COL665,usrName);
  END IF;

IF NVL(:OLD.COL666,' ') != NVL(:NEW.COL666,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col666'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col666'),:OLD.COL666,:NEW.COL666,usrName);
  END IF;

IF NVL(:OLD.COL667,' ') != NVL(:NEW.COL667,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col667'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col667'),:OLD.COL667,:NEW.COL667,usrName);
  END IF;

IF NVL(:OLD.COL668,' ') != NVL(:NEW.COL668,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col668'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col668'),:OLD.COL668,:NEW.COL668,usrName);
  END IF;

IF NVL(:OLD.COL669,' ') != NVL(:NEW.COL669,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col669'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col669'),:OLD.COL669,:NEW.COL669,usrName);
  END IF;

IF NVL(:OLD.COL670,' ') != NVL(:NEW.COL670,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col670'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col670'),:OLD.COL670,:NEW.COL670,usrName);
  END IF;

IF NVL(:OLD.COL671,' ') != NVL(:NEW.COL671,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col671'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col671'),:OLD.COL671,:NEW.COL671,usrName);
  END IF;

IF NVL(:OLD.COL672,' ') != NVL(:NEW.COL672,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col672'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col672'),:OLD.COL672,:NEW.COL672,usrName);
  END IF;

IF NVL(:OLD.COL673,' ') != NVL(:NEW.COL673,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col673'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col673'),:OLD.COL673,:NEW.COL673,usrName);
  END IF;

IF NVL(:OLD.COL674,' ') != NVL(:NEW.COL674,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col674'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col674'),:OLD.COL674,:NEW.COL674,usrName);
  END IF;

IF NVL(:OLD.COL675,' ') != NVL(:NEW.COL675,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col675'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col675'),:OLD.COL675,:NEW.COL675,usrName);
  END IF;

IF NVL(:OLD.COL676,' ') != NVL(:NEW.COL676,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col676'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col676'),:OLD.COL676,:NEW.COL676,usrName);
  END IF;

IF NVL(:OLD.COL677,' ') != NVL(:NEW.COL677,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col677'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col677'),:OLD.COL677,:NEW.COL677,usrName);
  END IF;

IF NVL(:OLD.COL678,' ') != NVL(:NEW.COL678,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col678'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col678'),:OLD.COL678,:NEW.COL678,usrName);
  END IF;


IF NVL(:OLD.COL679,' ') != NVL(:NEW.COL679,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col679'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col679'),:OLD.COL679,:NEW.COL679,usrName);
  END IF;

IF NVL(:OLD.COL680,' ') != NVL(:NEW.COL680,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col680'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col680'),:OLD.COL680,:NEW.COL680,usrName);
  END IF;

IF NVL(:OLD.COL681,' ') != NVL(:NEW.COL681,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col681'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col681'),:OLD.COL681,:NEW.COL681,usrName);
  END IF;

IF NVL(:OLD.COL682,' ') != NVL(:NEW.COL682,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col682'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col682'),:OLD.COL682,:NEW.COL682,usrName);
  END IF;

IF NVL(:OLD.COL683,' ') != NVL(:NEW.COL683,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col683'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col683'),:OLD.COL683,:NEW.COL683,usrName);
  END IF;

IF NVL(:OLD.COL684,' ') != NVL(:NEW.COL684,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col684'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col684'),:OLD.COL684,:NEW.COL684,usrName);
  END IF;

IF NVL(:OLD.COL685,' ') != NVL(:NEW.COL685,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col685'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col685'),:OLD.COL685,:NEW.COL685,usrName);
  END IF;

IF NVL(:OLD.COL686,' ') != NVL(:NEW.COL686,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col686'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col686'),:OLD.COL686,:NEW.COL686,usrName);
  END IF;

IF NVL(:OLD.COL687,' ') != NVL(:NEW.COL687,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col687'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col687'),:OLD.COL687,:NEW.COL687,usrName);
  END IF;

IF NVL(:OLD.COL688,' ') != NVL(:NEW.COL688,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col688'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col688'),:OLD.COL688,:NEW.COL688,usrName);
  END IF;

IF NVL(:OLD.COL689,' ') != NVL(:NEW.COL689,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col689'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col689'),:OLD.COL689,:NEW.COL689,usrName);
  END IF;

IF NVL(:OLD.COL690,' ') != NVL(:NEW.COL690,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col690'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col690'),:OLD.COL690,:NEW.COL690,usrName);
  END IF;

IF NVL(:OLD.COL691,' ') != NVL(:NEW.COL691,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col691'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col691'),:OLD.COL691,:NEW.COL691,usrName);
  END IF;

IF NVL(:OLD.COL692,' ') != NVL(:NEW.COL692,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col692'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col692'),:OLD.COL692,:NEW.COL692,usrName);
  END IF;

IF NVL(:OLD.COL693,' ') != NVL(:NEW.COL693,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col693'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col693'),:OLD.COL693,:NEW.COL693,usrName);
  END IF;

IF NVL(:OLD.COL694,' ') != NVL(:NEW.COL694,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col694'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col694'),:OLD.COL694,:NEW.COL694,usrName);
  END IF;

IF NVL(:OLD.COL695,' ') != NVL(:NEW.COL695,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col695'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col695'),:OLD.COL695,:NEW.COL695,usrName);
  END IF;

IF NVL(:OLD.COL696,' ') != NVL(:NEW.COL696,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col696'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col696'),:OLD.COL696,:NEW.COL696,usrName);
  END IF;

IF NVL(:OLD.COL697,' ') != NVL(:NEW.COL697,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col697'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col697'),:OLD.COL697,:NEW.COL697,usrName);
  END IF;

IF NVL(:OLD.COL698,' ') != NVL(:NEW.COL698,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col698'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col698'),:OLD.COL698,:NEW.COL698,usrName);
  END IF;

IF NVL(:OLD.COL699,' ') != NVL(:NEW.COL699,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col699'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col699'),:OLD.COL699,:NEW.COL699,usrName);
  END IF;

IF NVL(:OLD.COL700,' ') != NVL(:NEW.COL700,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col700'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col700'),:OLD.COL700,:NEW.COL700,usrName);
  END IF;

IF NVL(:OLD.COL701,' ') != NVL(:NEW.COL701,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col701'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col701'),:OLD.COL701,:NEW.COL701,usrName);
  END IF;

IF NVL(:OLD.COL702,' ') != NVL(:NEW.COL702,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col702'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col702'),:OLD.COL702,:NEW.COL702,usrName);
  END IF;

IF NVL(:OLD.COL703,' ') != NVL(:NEW.COL703,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col703'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col703'),:OLD.COL703,:NEW.COL703,usrName);
  END IF;

IF NVL(:OLD.COL704,' ') != NVL(:NEW.COL704,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col704'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col704'),:OLD.COL704,:NEW.COL704,usrName);
  END IF;

IF NVL(:OLD.COL705,' ') != NVL(:NEW.COL705,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col705'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col705'),:OLD.COL705,:NEW.COL705,usrName);
  END IF;

IF NVL(:OLD.COL706,' ') != NVL(:NEW.COL706,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col706'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col706'),:OLD.COL706,:NEW.COL706,usrName);
  END IF;

IF NVL(:OLD.COL707,' ') != NVL(:NEW.COL707,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col707'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col707'),:OLD.COL707,:NEW.COL707,usrName);
  END IF;

IF NVL(:OLD.COL708,' ') != NVL(:NEW.COL708,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col708'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col708'),:OLD.COL708,:NEW.COL708,usrName);
  END IF;

IF NVL(:OLD.COL709,' ') != NVL(:NEW.COL709,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col709'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col709'),:OLD.COL709,:NEW.COL709,usrName);
  END IF;

IF NVL(:OLD.COL710,' ') != NVL(:NEW.COL710,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col710'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col710'),:OLD.COL710,:NEW.COL710,usrName);
  END IF;

IF NVL(:OLD.COL711,' ') != NVL(:NEW.COL711,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col711'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col711'),:OLD.COL711,:NEW.COL711,usrName);
  END IF;

IF NVL(:OLD.COL712,' ') != NVL(:NEW.COL712,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col712'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col712'),:OLD.COL712,:NEW.COL712,usrName);
  END IF;

IF NVL(:OLD.COL713,' ') != NVL(:NEW.COL713,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col713'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col713'),:OLD.COL713,:NEW.COL713,usrName);
  END IF;

IF NVL(:OLD.COL714,' ') != NVL(:NEW.COL714,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col714'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col714'),:OLD.COL714,:NEW.COL714,usrName);
  END IF;

IF NVL(:OLD.COL715,' ') != NVL(:NEW.COL715,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col715'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col715'),:OLD.COL715,:NEW.COL715,usrName);
  END IF;

IF NVL(:OLD.COL716,' ') != NVL(:NEW.COL716,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col716'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col716'),:OLD.COL716,:NEW.COL716,usrName);
  END IF;

IF NVL(:OLD.COL717,' ') != NVL(:NEW.COL717,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col717'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col717'),:OLD.COL717,:NEW.COL717,usrName);
  END IF;

IF NVL(:OLD.COL718,' ') != NVL(:NEW.COL718,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col718'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col718'),:OLD.COL718,:NEW.COL718,usrName);
  END IF;

IF NVL(:OLD.COL719,' ') != NVL(:NEW.COL719,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col719'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col719'),:OLD.COL719,:NEW.COL719,usrName);
  END IF;

IF NVL(:OLD.COL720,' ') != NVL(:NEW.COL720,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col720'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col720'),:OLD.COL720,:NEW.COL720,usrName);
  END IF;

IF NVL(:OLD.COL721,' ') != NVL(:NEW.COL721,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col721'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col721'),:OLD.COL721,:NEW.COL721,usrName);
  END IF;

IF NVL(:OLD.COL722,' ') != NVL(:NEW.COL722,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col722'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col722'),:OLD.COL722,:NEW.COL722,usrName);
  END IF;

IF NVL(:OLD.COL723,' ') != NVL(:NEW.COL723,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col723'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col723'),:OLD.COL723,:NEW.COL723,usrName);
  END IF;

IF NVL(:OLD.COL724,' ') != NVL(:NEW.COL724,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col724'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col724'),:OLD.COL724,:NEW.COL724,usrName);
  END IF;

IF NVL(:OLD.COL725,' ') != NVL(:NEW.COL725,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col725'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col725'),:OLD.COL725,:NEW.COL725,usrName);
  END IF;

IF NVL(:OLD.COL726,' ') != NVL(:NEW.COL726,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col726'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col726'),:OLD.COL726,:NEW.COL726,usrName);
  END IF;

IF NVL(:OLD.COL727,' ') != NVL(:NEW.COL727,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col727'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col727'),:OLD.COL727,:NEW.COL727,usrName);
  END IF;

IF NVL(:OLD.COL728,' ') != NVL(:NEW.COL728,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col728'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col728'),:OLD.COL728,:NEW.COL728,usrName);
  END IF;

IF NVL(:OLD.COL729,' ') != NVL(:NEW.COL729,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col729'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col729'),:OLD.COL729,:NEW.COL729,usrName);
  END IF;

IF NVL(:OLD.COL730,' ') != NVL(:NEW.COL730,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col730'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col730'),:OLD.COL730,:NEW.COL730,usrName);
  END IF;

IF NVL(:OLD.COL731,' ') != NVL(:NEW.COL731,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col731'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col731'),:OLD.COL731,:NEW.COL731,usrName);
  END IF;

IF NVL(:OLD.COL732,' ') != NVL(:NEW.COL732,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col732'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col732'),:OLD.COL732,:NEW.COL732,usrName);
  END IF;

IF NVL(:OLD.COL733,' ') != NVL(:NEW.COL733,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col733'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col733'),:OLD.COL733,:NEW.COL733,usrName);
  END IF;

IF NVL(:OLD.COL734,' ') != NVL(:NEW.COL734,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col734'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col734'),:OLD.COL734,:NEW.COL734,usrName);
  END IF;

IF NVL(:OLD.COL735,' ') != NVL(:NEW.COL735,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col735'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col735'),:OLD.COL735,:NEW.COL735,usrName);
  END IF;

IF NVL(:OLD.COL736,' ') != NVL(:NEW.COL736,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col736'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col736'),:OLD.COL736,:NEW.COL736,usrName);
  END IF;

IF NVL(:OLD.COL737,' ') != NVL(:NEW.COL737,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col737'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col737'),:OLD.COL737,:NEW.COL737,usrName);
  END IF;

IF NVL(:OLD.COL738,' ') != NVL(:NEW.COL738,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col738'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col738'),:OLD.COL738,:NEW.COL738,usrName);
  END IF;

IF NVL(:OLD.COL739,' ') != NVL(:NEW.COL739,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col739'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col739'),:OLD.COL739,:NEW.COL739,usrName);
  END IF;

IF NVL(:OLD.COL740,' ') != NVL(:NEW.COL740,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col740'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col740'),:OLD.COL740,:NEW.COL740,usrName);
  END IF;

IF NVL(:OLD.COL741,' ') != NVL(:NEW.COL741,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col741'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col741'),:OLD.COL741,:NEW.COL741,usrName);
  END IF;

IF NVL(:OLD.COL742,' ') != NVL(:NEW.COL742,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col742'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col742'),:OLD.COL742,:NEW.COL742,usrName);
  END IF;

IF NVL(:OLD.COL743,' ') != NVL(:NEW.COL743,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col743'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col743'),:OLD.COL743,:NEW.COL743,usrName);
  END IF;

IF NVL(:OLD.COL744,' ') != NVL(:NEW.COL744,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col744'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col744'),:OLD.COL744,:NEW.COL744,usrName);
  END IF;


IF NVL(:OLD.COL745,' ') != NVL(:NEW.COL745,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col745'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col745'),:OLD.COL745,:NEW.COL745,usrName);
  END IF;

IF NVL(:OLD.COL746,' ') != NVL(:NEW.COL746,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col746'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col746'),:OLD.COL746,:NEW.COL746,usrName);
  END IF;


IF NVL(:OLD.COL747,' ') != NVL(:NEW.COL747,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col747'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col747'),:OLD.COL747,:NEW.COL747,usrName);
  END IF;

IF NVL(:OLD.COL748,' ') != NVL(:NEW.COL748,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col748'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col748'),:OLD.COL748,:NEW.COL748,usrName);
  END IF;

IF NVL(:OLD.COL749,' ') != NVL(:NEW.COL749,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col749'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col749'),:OLD.COL749,:NEW.COL749,usrName);
  END IF;

IF NVL(:OLD.COL750,' ') != NVL(:NEW.COL750,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col750'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col750'),:OLD.COL750,:NEW.COL750,usrName);
  END IF;

IF NVL(:OLD.COL751,' ') != NVL(:NEW.COL751,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col751'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col751'),:OLD.COL751,:NEW.COL751,usrName);
  END IF;

IF NVL(:OLD.COL752,' ') != NVL(:NEW.COL752,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col752'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col752'),:OLD.COL752,:NEW.COL752,usrName);
  END IF;

IF NVL(:OLD.COL753,' ') != NVL(:NEW.COL753,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col753'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col753'),:OLD.COL753,:NEW.COL753,usrName);
  END IF;

IF NVL(:OLD.COL754,' ') != NVL(:NEW.COL754,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col754'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col754'),:OLD.COL754,:NEW.COL754,usrName);
  END IF;

IF NVL(:OLD.COL755,' ') != NVL(:NEW.COL755,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col755'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col755'),:OLD.COL755,:NEW.COL755,usrName);
  END IF;

IF NVL(:OLD.COL756,' ') != NVL(:NEW.COL756,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col756'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col756'),:OLD.COL756,:NEW.COL756,usrName);
  END IF;

IF NVL(:OLD.COL757,' ') != NVL(:NEW.COL757,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col757'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col757'),:OLD.COL757,:NEW.COL757,usrName);
  END IF;

IF NVL(:OLD.COL758,' ') != NVL(:NEW.COL758,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col758'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col758'),:OLD.COL758,:NEW.COL758,usrName);
  END IF;

IF NVL(:OLD.COL759,' ') != NVL(:NEW.COL759,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col759'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col759'),:OLD.COL759,:NEW.COL759,usrName);
  END IF;

IF NVL(:OLD.COL760,' ') != NVL(:NEW.COL760,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col760'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col760'),:OLD.COL760,:NEW.COL760,usrName);
  END IF;

IF NVL(:OLD.COL761,' ') != NVL(:NEW.COL761,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col761'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col761'),:OLD.COL761,:NEW.COL761,usrName);
  END IF;

IF NVL(:OLD.COL762,' ') != NVL(:NEW.COL762,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col762'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col762'),:OLD.COL762,:NEW.COL762,usrName);
  END IF;

IF NVL(:OLD.COL763,' ') != NVL(:NEW.COL763,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col763'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col763'),:OLD.COL763,:NEW.COL763,usrName);
  END IF;

IF NVL(:OLD.COL764,' ') != NVL(:NEW.COL764,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col764'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col764'),:OLD.COL764,:NEW.COL764,usrName);
  END IF;

IF NVL(:OLD.COL765,' ') != NVL(:NEW.COL765,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col765'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col765'),:OLD.COL765,:NEW.COL765,usrName);
  END IF;

IF NVL(:OLD.COL766,' ') != NVL(:NEW.COL766,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col766'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col766'),:OLD.COL766,:NEW.COL766,usrName);
  END IF;

IF NVL(:OLD.COL767,' ') != NVL(:NEW.COL767,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col767'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col767'),:OLD.COL767,:NEW.COL767,usrName);
  END IF;

IF NVL(:OLD.COL768,' ') != NVL(:NEW.COL768,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col768'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col768'),:OLD.COL768,:NEW.COL768,usrName);
  END IF;

IF NVL(:OLD.COL769,' ') != NVL(:NEW.COL769,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col769'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col769'),:OLD.COL769,:NEW.COL769,usrName);
  END IF;

IF NVL(:OLD.COL770,' ') != NVL(:NEW.COL770,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col770'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col770'),:OLD.COL770,:NEW.COL770,usrName);
  END IF;

IF NVL(:OLD.COL771,' ') != NVL(:NEW.COL771,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col771'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col771'),:OLD.COL771,:NEW.COL771,usrName);
  END IF;

IF NVL(:OLD.COL772,' ') != NVL(:NEW.COL772,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col772'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col772'),:OLD.COL772,:NEW.COL772,usrName);
  END IF;

IF NVL(:OLD.COL773,' ') != NVL(:NEW.COL773,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col773'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col773'),:OLD.COL773,:NEW.COL773,usrName);
  END IF;

IF NVL(:OLD.COL774,' ') != NVL(:NEW.COL774,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col774'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col774'),:OLD.COL774,:NEW.COL774,usrName);
  END IF;

IF NVL(:OLD.COL775,' ') != NVL(:NEW.COL775,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col775'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col775'),:OLD.COL775,:NEW.COL775,usrName);
  END IF;

IF NVL(:OLD.COL776,' ') != NVL(:NEW.COL776,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col776'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col776'),:OLD.COL776,:NEW.COL776,usrName);
  END IF;

IF NVL(:OLD.COL777,' ') != NVL(:NEW.COL777,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col777'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col777'),:OLD.COL777,:NEW.COL777,usrName);
  END IF;

IF NVL(:OLD.COL778,' ') != NVL(:NEW.COL778,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col778'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col778'),:OLD.COL778,:NEW.COL778,usrName);
  END IF;

IF NVL(:OLD.COL779,' ') != NVL(:NEW.COL779,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col779'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col779'),:OLD.COL779,:NEW.COL779,usrName);
  END IF;

IF NVL(:OLD.COL780,' ') != NVL(:NEW.COL780,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col780'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col780'),:OLD.COL780,:NEW.COL780,usrName);
  END IF;

IF NVL(:OLD.COL781,' ') != NVL(:NEW.COL781,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col781'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col781'),:OLD.COL781,:NEW.COL781,usrName);
  END IF;

IF NVL(:OLD.COL782,' ') != NVL(:NEW.COL782,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col782'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col782'),:OLD.COL782,:NEW.COL782,usrName);
  END IF;

IF NVL(:OLD.COL783,' ') != NVL(:NEW.COL783,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col783'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col783'),:OLD.COL783,:NEW.COL783,usrName);
  END IF;

IF NVL(:OLD.COL784,' ') != NVL(:NEW.COL784,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col784'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col784'),:OLD.COL784,:NEW.COL784,usrName);
  END IF;

IF NVL(:OLD.COL785,' ') != NVL(:NEW.COL785,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col785'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col785'),:OLD.COL785,:NEW.COL785,usrName);
  END IF;

IF NVL(:OLD.COL786,' ') != NVL(:NEW.COL786,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col786'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col786'),:OLD.COL786,:NEW.COL786,usrName);
  END IF;

IF NVL(:OLD.COL787,' ') != NVL(:NEW.COL787,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col787'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col787'),:OLD.COL787,:NEW.COL787,usrName);
  END IF;

IF NVL(:OLD.COL788,' ') != NVL(:NEW.COL788,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col788'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col788'),:OLD.COL788,:NEW.COL788,usrName);
  END IF;

IF NVL(:OLD.COL789,' ') != NVL(:NEW.COL789,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col789'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col789'),:OLD.COL789,:NEW.COL789,usrName);
  END IF;

IF NVL(:OLD.COL790,' ') != NVL(:NEW.COL790,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col790'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col790'),:OLD.COL790,:NEW.COL790,usrName);
  END IF;

IF NVL(:OLD.COL791,' ') != NVL(:NEW.COL791,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col791'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col791'),:OLD.COL791,:NEW.COL791,usrName);
  END IF;

IF NVL(:OLD.COL792,' ') != NVL(:NEW.COL792,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col792'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col792'),:OLD.COL792,:NEW.COL792,usrName);
  END IF;

IF NVL(:OLD.COL793,' ') != NVL(:NEW.COL793,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col793'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col793'),:OLD.COL793,:NEW.COL793,usrName);
  END IF;

IF NVL(:OLD.COL794,' ') != NVL(:NEW.COL794,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col794'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col794'),:OLD.COL794,:NEW.COL794,usrName);
  END IF;

IF NVL(:OLD.COL795,' ') != NVL(:NEW.COL795,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col795'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col795'),:OLD.COL795,:NEW.COL795,usrName);
  END IF;

IF NVL(:OLD.COL796,' ') != NVL(:NEW.COL796,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col796'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col796'),:OLD.COL796,:NEW.COL796,usrName);
  END IF;

IF NVL(:OLD.COL797,' ') != NVL(:NEW.COL797,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col797'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col797'),:OLD.COL797,:NEW.COL797,usrName);
  END IF;

IF NVL(:OLD.COL798,' ') != NVL(:NEW.COL798,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col798'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col798'),:OLD.COL798,:NEW.COL798,usrName);
  END IF;

IF NVL(:OLD.COL799,' ') != NVL(:NEW.COL799,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col799'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col799'),:OLD.COL799,:NEW.COL799,usrName);
  END IF;

IF NVL(:OLD.COL800,' ') != NVL(:NEW.COL800,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col800'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col800'),:OLD.COL800,:NEW.COL800,usrName);
  END IF;

IF NVL(:OLD.COL801,' ') != NVL(:NEW.COL801,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col801'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col801'),:OLD.COL801,:NEW.COL801,usrName);
  END IF;

IF NVL(:OLD.COL802,' ') != NVL(:NEW.COL802,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col802'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col802'),:OLD.COL802,:NEW.COL802,usrName);
  END IF;

IF NVL(:OLD.COL803,' ') != NVL(:NEW.COL803,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col803'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col803'),:OLD.COL803,:NEW.COL803,usrName);
  END IF;

IF NVL(:OLD.COL804,' ') != NVL(:NEW.COL804,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col804'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col804'),:OLD.COL804,:NEW.COL804,usrName);
  END IF;

IF NVL(:OLD.COL805,' ') != NVL(:NEW.COL805,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col805'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col805'),:OLD.COL805,:NEW.COL805,usrName);
  END IF;

IF NVL(:OLD.COL806,' ') != NVL(:NEW.COL806,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col806'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col806'),:OLD.COL806,:NEW.COL806,usrName);
  END IF;


IF NVL(:OLD.COL807,' ') != NVL(:NEW.COL807,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col807'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col807'),:OLD.COL807,:NEW.COL807,usrName);
  END IF;

IF NVL(:OLD.COL808,' ') != NVL(:NEW.COL808,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col808'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col808'),:OLD.COL808,:NEW.COL808,usrName);
  END IF;

IF NVL(:OLD.COL809,' ') != NVL(:NEW.COL809,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col809'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col809'),:OLD.COL809,:NEW.COL809,usrName);
  END IF;

IF NVL(:OLD.COL810,' ') != NVL(:NEW.COL810,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col810'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col810'),:OLD.COL810,:NEW.COL810,usrName);
  END IF;

IF NVL(:OLD.COL811,' ') != NVL(:NEW.COL811,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col811'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col811'),:OLD.COL811,:NEW.COL811,usrName);
  END IF;

IF NVL(:OLD.COL812,' ') != NVL(:NEW.COL812,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col812'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col812'),:OLD.COL812,:NEW.COL812,usrName);
  END IF;

IF NVL(:OLD.COL813,' ') != NVL(:NEW.COL813,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col813'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col813'),:OLD.COL813,:NEW.COL813,usrName);
  END IF;

IF NVL(:OLD.COL814,' ') != NVL(:NEW.COL814,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col814'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col814'),:OLD.COL814,:NEW.COL814,usrName);
  END IF;


IF NVL(:OLD.COL815,' ') != NVL(:NEW.COL815,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col815'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col815'),:OLD.COL815,:NEW.COL815,usrName);
  END IF;

IF NVL(:OLD.COL816,' ') != NVL(:NEW.COL816,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col816'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col816'),:OLD.COL816,:NEW.COL816,usrName);
  END IF;

IF NVL(:OLD.COL817,' ') != NVL(:NEW.COL817,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col817'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col817'),:OLD.COL817,:NEW.COL817,usrName);
  END IF;

IF NVL(:OLD.COL818,' ') != NVL(:NEW.COL818,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col818'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col818'),:OLD.COL818,:NEW.COL818,usrName);
  END IF;

IF NVL(:OLD.COL819,' ') != NVL(:NEW.COL819,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col819'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col819'),:OLD.COL819,:NEW.COL819,usrName);
  END IF;

IF NVL(:OLD.COL820,' ') != NVL(:NEW.COL820,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col820'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col820'),:OLD.COL820,:NEW.COL820,usrName);
  END IF;

IF NVL(:OLD.COL821,' ') != NVL(:NEW.COL821,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col821'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col821'),:OLD.COL821,:NEW.COL821,usrName);
  END IF;

IF NVL(:OLD.COL822,' ') != NVL(:NEW.COL822,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col822'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col822'),:OLD.COL822,:NEW.COL822,usrName);
  END IF;

IF NVL(:OLD.COL823,' ') != NVL(:NEW.COL823,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col823'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col823'),:OLD.COL823,:NEW.COL823,usrName);
  END IF;

IF NVL(:OLD.COL824,' ') != NVL(:NEW.COL824,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col824'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col824'),:OLD.COL824,:NEW.COL824,usrName);
  END IF;

IF NVL(:OLD.COL825,' ') != NVL(:NEW.COL825,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col825'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col825'),:OLD.COL825,:NEW.COL825,usrName);
  END IF;

IF NVL(:OLD.COL826,' ') != NVL(:NEW.COL826,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col826'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col826'),:OLD.COL826,:NEW.COL826,usrName);
  END IF;

IF NVL(:OLD.COL827,' ') != NVL(:NEW.COL827,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col827'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col827'),:OLD.COL827,:NEW.COL827,usrName);
  END IF;

IF NVL(:OLD.COL828,' ') != NVL(:NEW.COL828,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col828'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col828'),:OLD.COL828,:NEW.COL828,usrName);
  END IF;

IF NVL(:OLD.COL829,' ') != NVL(:NEW.COL829,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col829'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col829'),:OLD.COL829,:NEW.COL829,usrName);
  END IF;

IF NVL(:OLD.COL830,' ') != NVL(:NEW.COL830,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col830'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col830'),:OLD.COL830,:NEW.COL830,usrName);
  END IF;

IF NVL(:OLD.COL831,' ') != NVL(:NEW.COL831,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col831'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col831'),:OLD.COL831,:NEW.COL831,usrName);
  END IF;

IF NVL(:OLD.COL832,' ') != NVL(:NEW.COL832,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col832'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col832'),:OLD.COL832,:NEW.COL832,usrName);
  END IF;

IF NVL(:OLD.COL833,' ') != NVL(:NEW.COL833,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col833'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col833'),:OLD.COL833,:NEW.COL833,usrName);
  END IF;

IF NVL(:OLD.COL834,' ') != NVL(:NEW.COL834,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col834'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col834'),:OLD.COL834,:NEW.COL834,usrName);
  END IF;

IF NVL(:OLD.COL835,' ') != NVL(:NEW.COL835,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col835'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col835'),:OLD.COL835,:NEW.COL835,usrName);
  END IF;

IF NVL(:OLD.COL836,' ') != NVL(:NEW.COL836,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col836'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col836'),:OLD.COL836,:NEW.COL836,usrName);
  END IF;

IF NVL(:OLD.COL837,' ') != NVL(:NEW.COL837,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col837'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col837'),:OLD.COL837,:NEW.COL837,usrName);
  END IF;

IF NVL(:OLD.COL838,' ') != NVL(:NEW.COL838,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col838'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col838'),:OLD.COL838,:NEW.COL838,usrName);
  END IF;

IF NVL(:OLD.COL839,' ') != NVL(:NEW.COL839,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col839'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col839'),:OLD.COL839,:NEW.COL839,usrName);
  END IF;

IF NVL(:OLD.COL840,' ') != NVL(:NEW.COL840,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col840'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col840'),:OLD.COL840,:NEW.COL840,usrName);
  END IF;

IF NVL(:OLD.COL841,' ') != NVL(:NEW.COL841,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col841'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col841'),:OLD.COL841,:NEW.COL841,usrName);
  END IF;

IF NVL(:OLD.COL842,' ') != NVL(:NEW.COL842,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col842'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col842'),:OLD.COL842,:NEW.COL842,usrName);
  END IF;

IF NVL(:OLD.COL843,' ') != NVL(:NEW.COL843,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col843'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col843'),:OLD.COL843,:NEW.COL843,usrName);
  END IF;

IF NVL(:OLD.COL844,' ') != NVL(:NEW.COL844,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col844'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col844'),:OLD.COL844,:NEW.COL844,usrName);
  END IF;

IF NVL(:OLD.COL845,' ') != NVL(:NEW.COL845,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col845'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col845'),:OLD.COL845,:NEW.COL845,usrName);
  END IF;

IF NVL(:OLD.COL846,' ') != NVL(:NEW.COL846,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col846'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col846'),:OLD.COL846,:NEW.COL846,usrName);
  END IF;

IF NVL(:OLD.COL847,' ') != NVL(:NEW.COL847,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col847'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col847'),:OLD.COL847,:NEW.COL847,usrName);
  END IF;

IF NVL(:OLD.COL848,' ') != NVL(:NEW.COL848,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col848'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col848'),:OLD.COL848,:NEW.COL848,usrName);
  END IF;

IF NVL(:OLD.COL849,' ') != NVL(:NEW.COL849,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col849'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col849'),:OLD.COL849,:NEW.COL849,usrName);
  END IF;

IF NVL(:OLD.COL850,' ') != NVL(:NEW.COL850,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col850'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col850'),:OLD.COL850,:NEW.COL850,usrName);
  END IF;

IF NVL(:OLD.COL851,' ') != NVL(:NEW.COL851,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col851'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col851'),:OLD.COL851,:NEW.COL851,usrName);
  END IF;

IF NVL(:OLD.COL852,' ') != NVL(:NEW.COL852,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col852'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col852'),:OLD.COL852,:NEW.COL852,usrName);
  END IF;

IF NVL(:OLD.COL853,' ') != NVL(:NEW.COL853,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col853'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col853'),:OLD.COL853,:NEW.COL853,usrName);
  END IF;

IF NVL(:OLD.COL854,' ') != NVL(:NEW.COL854,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col854'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col854'),:OLD.COL854,:NEW.COL854,usrName);
  END IF;

IF NVL(:OLD.COL855,' ') != NVL(:NEW.COL855,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col855'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col855'),:OLD.COL855,:NEW.COL855,usrName);
  END IF;

IF NVL(:OLD.COL856,' ') != NVL(:NEW.COL856,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col856'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col856'),:OLD.COL856,:NEW.COL856,usrName);
  END IF;

IF NVL(:OLD.COL857,' ') != NVL(:NEW.COL857,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col857'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col857'),:OLD.COL857,:NEW.COL857,usrName);
  END IF;

IF NVL(:OLD.COL858,' ') != NVL(:NEW.COL858,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col858'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col858'),:OLD.COL858,:NEW.COL858,usrName);
  END IF;

IF NVL(:OLD.COL859,' ') != NVL(:NEW.COL859,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col859'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col859'),:OLD.COL859,:NEW.COL859,usrName);
  END IF;

IF NVL(:OLD.COL860,' ') != NVL(:NEW.COL860,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col860'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col860'),:OLD.COL860,:NEW.COL860,usrName);
  END IF;

IF NVL(:OLD.COL861,' ') != NVL(:NEW.COL861,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col861'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col861'),:OLD.COL861,:NEW.COL861,usrName);
  END IF;

IF NVL(:OLD.COL862,' ') != NVL(:NEW.COL862,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col862'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col862'),:OLD.COL862,:NEW.COL862,usrName);
  END IF;

IF NVL(:OLD.COL863,' ') != NVL(:NEW.COL863,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col863'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col863'),:OLD.COL863,:NEW.COL863,usrName);
  END IF;

IF NVL(:OLD.COL864,' ') != NVL(:NEW.COL864,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col864'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col864'),:OLD.COL864,:NEW.COL864,usrName);
  END IF;

IF NVL(:OLD.COL865,' ') != NVL(:NEW.COL865,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col865'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col865'),:OLD.COL865,:NEW.COL865,usrName);
  END IF;

IF NVL(:OLD.COL866,' ') != NVL(:NEW.COL866,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col866'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col866'),:OLD.COL866,:NEW.COL866,usrName);
  END IF;

IF NVL(:OLD.COL867,' ') != NVL(:NEW.COL867,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col867'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col867'),:OLD.COL867,:NEW.COL867,usrName);
  END IF;

IF NVL(:OLD.COL868,' ') != NVL(:NEW.COL868,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col868'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col868'),:OLD.COL868,:NEW.COL868,usrName);
  END IF;

IF NVL(:OLD.COL869,' ') != NVL(:NEW.COL869,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col869'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col869'),:OLD.COL869,:NEW.COL869,usrName);
  END IF;

IF NVL(:OLD.COL870,' ') != NVL(:NEW.COL870,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col870'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col870'),:OLD.COL870,:NEW.COL870,usrName);
  END IF;


IF NVL(:OLD.COL871,' ') != NVL(:NEW.COL871,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col871'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col871'),:OLD.COL871,:NEW.COL871,usrName);
  END IF;

IF NVL(:OLD.COL872,' ') != NVL(:NEW.COL872,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col872'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col872'),:OLD.COL872,:NEW.COL872,usrName);
  END IF;

IF NVL(:OLD.COL873,' ') != NVL(:NEW.COL873,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col873'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col873'),:OLD.COL873,:NEW.COL873,usrName);
  END IF;

IF NVL(:OLD.COL874,' ') != NVL(:NEW.COL874,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col874'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col874'),:OLD.COL874,:NEW.COL874,usrName);
  END IF;

IF NVL(:OLD.COL875,' ') != NVL(:NEW.COL875,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col875'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col875'),:OLD.COL875,:NEW.COL875,usrName);
  END IF;

IF NVL(:OLD.COL876,' ') != NVL(:NEW.COL876,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col876'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col876'),:OLD.COL876,:NEW.COL876,usrName);
  END IF;

IF NVL(:OLD.COL877,' ') != NVL(:NEW.COL877,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col877'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col877'),:OLD.COL877,:NEW.COL877,usrName);
  END IF;

IF NVL(:OLD.COL878,' ') != NVL(:NEW.COL878,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col878'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col878'),:OLD.COL878,:NEW.COL878,usrName);
  END IF;

IF NVL(:OLD.COL879,' ') != NVL(:NEW.COL879,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col879'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col879'),:OLD.COL879,:NEW.COL879,usrName);
  END IF;

IF NVL(:OLD.COL880,' ') != NVL(:NEW.COL880,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col880'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col880'),:OLD.COL880,:NEW.COL880,usrName);
  END IF;

IF NVL(:OLD.COL881,' ') != NVL(:NEW.COL881,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col881'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col881'),:OLD.COL881,:NEW.COL881,usrName);
  END IF;

IF NVL(:OLD.COL882,' ') != NVL(:NEW.COL882,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col882'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col882'),:OLD.COL882,:NEW.COL882,usrName);
  END IF;

IF NVL(:OLD.COL883,' ') != NVL(:NEW.COL883,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col883'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col883'),:OLD.COL883,:NEW.COL883,usrName);
  END IF;

IF NVL(:OLD.COL884,' ') != NVL(:NEW.COL884,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col884'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col884'),:OLD.COL884,:NEW.COL884,usrName);
  END IF;

IF NVL(:OLD.COL885,' ') != NVL(:NEW.COL885,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col885'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col885'),:OLD.COL885,:NEW.COL885,usrName);
  END IF;

IF NVL(:OLD.COL886,' ') != NVL(:NEW.COL886,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col886'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col886'),:OLD.COL886,:NEW.COL886,usrName);
  END IF;

IF NVL(:OLD.COL887,' ') != NVL(:NEW.COL887,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col887'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col887'),:OLD.COL887,:NEW.COL887,usrName);
  END IF;

IF NVL(:OLD.COL888,' ') != NVL(:NEW.COL888,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col888'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col888'),:OLD.COL888,:NEW.COL888,usrName);
  END IF;

IF NVL(:OLD.COL889,' ') != NVL(:NEW.COL889,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col889'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col889'),:OLD.COL889,:NEW.COL889,usrName);
  END IF;

IF NVL(:OLD.COL890,' ') != NVL(:NEW.COL890,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col890'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col890'),:OLD.COL890,:NEW.COL890,usrName);
  END IF;

IF NVL(:OLD.COL891,' ') != NVL(:NEW.COL891,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col891'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col891'),:OLD.COL891,:NEW.COL891,usrName);
  END IF;

IF NVL(:OLD.COL892,' ') != NVL(:NEW.COL892,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col892'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col892'),:OLD.COL892,:NEW.COL892,usrName);
  END IF;

IF NVL(:OLD.COL893,' ') != NVL(:NEW.COL893,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col893'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col893'),:OLD.COL893,:NEW.COL893,usrName);
  END IF;

IF NVL(:OLD.COL894,' ') != NVL(:NEW.COL894,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col894'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col894'),:OLD.COL894,:NEW.COL894,usrName);
  END IF;

IF NVL(:OLD.COL895,' ') != NVL(:NEW.COL895,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col895'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col895'),:OLD.COL895,:NEW.COL895,usrName);
  END IF;

IF NVL(:OLD.COL896,' ') != NVL(:NEW.COL896,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col896'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col896'),:OLD.COL896,:NEW.COL896,usrName);
  END IF;

IF NVL(:OLD.COL897,' ') != NVL(:NEW.COL897,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col897'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col897'),:OLD.COL897,:NEW.COL897,usrName);
  END IF;

IF NVL(:OLD.COL898,' ') != NVL(:NEW.COL898,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col898'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col898'),:OLD.COL898,:NEW.COL898,usrName);
  END IF;

IF NVL(:OLD.COL899,' ') != NVL(:NEW.COL899,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col899'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col899'),:OLD.COL899,:NEW.COL899,usrName);
  END IF;

IF NVL(:OLD.COL900,' ') != NVL(:NEW.COL900,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col900'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col900'),:OLD.COL900,:NEW.COL900,usrName);
  END IF;

IF NVL(:OLD.COL901,' ') != NVL(:NEW.COL901,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col901'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col901'),:OLD.COL901,:NEW.COL901,usrName);
  END IF;

IF NVL(:OLD.COL902,' ') != NVL(:NEW.COL902,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col902'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col902'),:OLD.COL902,:NEW.COL902,usrName);
  END IF;


IF NVL(:OLD.COL903,' ') != NVL(:NEW.COL903,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col903'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col903'),:OLD.COL903,:NEW.COL903,usrName);
  END IF;

IF NVL(:OLD.COL904,' ') != NVL(:NEW.COL904,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col904'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col904'),:OLD.COL904,:NEW.COL904,usrName);
  END IF;

IF NVL(:OLD.COL905,' ') != NVL(:NEW.COL905,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col905'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col905'),:OLD.COL905,:NEW.COL905,usrName);
  END IF;

IF NVL(:OLD.COL906,' ') != NVL(:NEW.COL906,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col906'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col906'),:OLD.COL906,:NEW.COL906,usrName);
  END IF;

IF NVL(:OLD.COL907,' ') != NVL(:NEW.COL907,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col907'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col907'),:OLD.COL907,:NEW.COL907,usrName);
  END IF;

IF NVL(:OLD.COL908,' ') != NVL(:NEW.COL908,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col908'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col908'),:OLD.COL908,:NEW.COL908,usrName);
  END IF;

IF NVL(:OLD.COL909,' ') != NVL(:NEW.COL909,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col909'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col909'),:OLD.COL909,:NEW.COL909,usrName);
  END IF;

IF NVL(:OLD.COL910,' ') != NVL(:NEW.COL910,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col910'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col910'),:OLD.COL910,:NEW.COL910,usrName);
  END IF;

IF NVL(:OLD.COL911,' ') != NVL(:NEW.COL911,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col911'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col911'),:OLD.COL911,:NEW.COL911,usrName);
  END IF;

IF NVL(:OLD.COL912,' ') != NVL(:NEW.COL912,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col912'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col912'),:OLD.COL912,:NEW.COL912,usrName);
  END IF;

IF NVL(:OLD.COL913,' ') != NVL(:NEW.COL913,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col913'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col913'),:OLD.COL913,:NEW.COL913,usrName);
  END IF;

IF NVL(:OLD.COL914,' ') != NVL(:NEW.COL914,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col914'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col914'),:OLD.COL914,:NEW.COL914,usrName);
  END IF;

IF NVL(:OLD.COL915,' ') != NVL(:NEW.COL915,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col915'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col915'),:OLD.COL915,:NEW.COL915,usrName);
  END IF;

IF NVL(:OLD.COL916,' ') != NVL(:NEW.COL916,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col916'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col916'),:OLD.COL916,:NEW.COL916,usrName);
  END IF;

IF NVL(:OLD.COL917,' ') != NVL(:NEW.COL917,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col917'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col917'),:OLD.COL917,:NEW.COL917,usrName);
  END IF;

IF NVL(:OLD.COL918,' ') != NVL(:NEW.COL918,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col918'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col918'),:OLD.COL918,:NEW.COL918,usrName);
  END IF;

IF NVL(:OLD.COL919,' ') != NVL(:NEW.COL919,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col919'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col919'),:OLD.COL919,:NEW.COL919,usrName);
  END IF;

IF NVL(:OLD.COL920,' ') != NVL(:NEW.COL920,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col920'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col920'),:OLD.COL920,:NEW.COL920,usrName);
  END IF;

IF NVL(:OLD.COL921,' ') != NVL(:NEW.COL921,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col921'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col921'),:OLD.COL921,:NEW.COL921,usrName);
  END IF;

IF NVL(:OLD.COL922,' ') != NVL(:NEW.COL922,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col922'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col922'),:OLD.COL922,:NEW.COL922,usrName);
  END IF;

IF NVL(:OLD.COL923,' ') != NVL(:NEW.COL923,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col923'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col923'),:OLD.COL923,:NEW.COL923,usrName);
  END IF;

IF NVL(:OLD.COL924,' ') != NVL(:NEW.COL924,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col924'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col924'),:OLD.COL924,:NEW.COL924,usrName);
  END IF;

IF NVL(:OLD.COL925,' ') != NVL(:NEW.COL925,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col925'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col925'),:OLD.COL925,:NEW.COL925,usrName);
  END IF;

IF NVL(:OLD.COL926,' ') != NVL(:NEW.COL926,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col926'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col926'),:OLD.COL926,:NEW.COL926,usrName);
  END IF;

IF NVL(:OLD.COL927,' ') != NVL(:NEW.COL927,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col927'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col927'),:OLD.COL927,:NEW.COL927,usrName);
  END IF;

IF NVL(:OLD.COL928,' ') != NVL(:NEW.COL928,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col928'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col928'),:OLD.COL928,:NEW.COL928,usrName);
  END IF;

IF NVL(:OLD.COL929,' ') != NVL(:NEW.COL929,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col929'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col929'),:OLD.COL929,:NEW.COL929,usrName);
  END IF;

IF NVL(:OLD.COL930,' ') != NVL(:NEW.COL930,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col930'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col930'),:OLD.COL930,:NEW.COL930,usrName);
  END IF;

IF NVL(:OLD.COL931,' ') != NVL(:NEW.COL931,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col931'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col931'),:OLD.COL931,:NEW.COL931,usrName);
  END IF;

IF NVL(:OLD.COL932,' ') != NVL(:NEW.COL932,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col932'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col932'),:OLD.COL932,:NEW.COL932,usrName);
  END IF;

IF NVL(:OLD.COL933,' ') != NVL(:NEW.COL933,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col933'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col933'),:OLD.COL933,:NEW.COL933,usrName);
  END IF;

IF NVL(:OLD.COL934,' ') != NVL(:NEW.COL934,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col934'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col934'),:OLD.COL934,:NEW.COL934,usrName);
  END IF;

IF NVL(:OLD.COL935,' ') != NVL(:NEW.COL935,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col935'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col935'),:OLD.COL935,:NEW.COL935,usrName);
  END IF;

IF NVL(:OLD.COL936,' ') != NVL(:NEW.COL936,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col936'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col936'),:OLD.COL936,:NEW.COL936,usrName);
  END IF;

IF NVL(:OLD.COL937,' ') != NVL(:NEW.COL937,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col937'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col937'),:OLD.COL937,:NEW.COL937,usrName);
  END IF;

IF NVL(:OLD.COL938,' ') != NVL(:NEW.COL938,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col938'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col938'),:OLD.COL938,:NEW.COL938,usrName);
  END IF;

IF NVL(:OLD.COL939,' ') != NVL(:NEW.COL939,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col939'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col939'),:OLD.COL939,:NEW.COL939,usrName);
  END IF;

IF NVL(:OLD.COL940,' ') != NVL(:NEW.COL940,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col940'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col940'),:OLD.COL940,:NEW.COL940,usrName);
  END IF;


IF NVL(:OLD.COL941,' ') != NVL(:NEW.COL941,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col941'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col941'),:OLD.COL941,:NEW.COL941,usrName);
  END IF;


IF NVL(:OLD.COL942,' ') != NVL(:NEW.COL942,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col942'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col942'),:OLD.COL942,:NEW.COL942,usrName);
  END IF;

IF NVL(:OLD.COL943,' ') != NVL(:NEW.COL943,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col943'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col943'),:OLD.COL943,:NEW.COL943,usrName);
  END IF;

IF NVL(:OLD.COL944,' ') != NVL(:NEW.COL944,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col944'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col944'),:OLD.COL944,:NEW.COL944,usrName);
  END IF;

IF NVL(:OLD.COL945,' ') != NVL(:NEW.COL945,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col945'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col945'),:OLD.COL945,:NEW.COL945,usrName);
  END IF;

IF NVL(:OLD.COL946,' ') != NVL(:NEW.COL946,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col946'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col946'),:OLD.COL946,:NEW.COL946,usrName);
  END IF;

IF NVL(:OLD.COL947,' ') != NVL(:NEW.COL947,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col947'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col947'),:OLD.COL947,:NEW.COL947,usrName);
  END IF;

IF NVL(:OLD.COL948,' ') != NVL(:NEW.COL948,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col948'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col948'),:OLD.COL948,:NEW.COL948,usrName);
  END IF;

IF NVL(:OLD.COL949,' ') != NVL(:NEW.COL949,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col949'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col949'),:OLD.COL949,:NEW.COL949,usrName);
  END IF;

IF NVL(:OLD.COL950,' ') != NVL(:NEW.COL950,' ') THEN
INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,fa_oldvalue,fa_newvalue,fa_modifiedby_name) VALUES (seq_er_formauditcol.NEXTVAL,:OLD.fk_filledform,:OLD.fk_form,:OLD.form_type,
(SELECT mp_systemid FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col950'),SYSDATE,(SELECT mp_dispname FROM ER_MAPFORM m WHERE m.fk_form=:OLD.fk_form AND m.mp_mapcolname='col950'),:OLD.COL950,:NEW.COL950,usrName);
  END IF;

  EXCEPTION
     WHEN OTHERS THEN
	 	  plog.DEBUG(pCTX,'Exception in ER_FORMSLINEAR_AU1 :' || SQLERRM);
       -- Consider logging the error and then re-raise
       RAISE;
END ;
/


