CREATE OR REPLACE TRIGGER "ER_CATLIB_AU1" 
AFTER UPDATE OF RECORD_TYPE
ON ER_CATLIB
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
new.record_type = 'D'
      )
BEGIN

update er_fldlib
set record_type = 'D'
where fk_catlib = :new.pk_catlib and fld_libflag = 'L' ;


END;
/


