create or replace TRIGGER "ERES"."ER_ER_ACCOUNT_BU_LM" 
BEFORE UPDATE ON ER_ACCOUNT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW WHEN (NEW.last_modified_by IS NOT NULL)  --KM-3634
begin
:new.last_modified_date := sysdate ;
 end;
/


