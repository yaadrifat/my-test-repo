CREATE  OR REPLACE TRIGGER ER_ACCOUNT_BI0 BEFORE INSERT ON ER_ACCOUNT        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_ACCOUNT',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'ACCENDDATE', :NEW.ACCENDDATE);
       Audit_Trail.column_insert (raid, 'AC_AUTOGEN_PATIENT', :NEW.AC_AUTOGEN_PATIENT);
       Audit_Trail.column_insert (raid, 'AC_AUTOGEN_STUDY', :NEW.AC_AUTOGEN_STUDY);
       Audit_Trail.column_insert (raid, 'AC_ENABLELDAP', :NEW.AC_ENABLELDAP);
       Audit_Trail.column_insert (raid, 'AC_ENABLEWS', :NEW.AC_ENABLEWS);
       Audit_Trail.column_insert (raid, 'AC_ENDT', :NEW.AC_ENDT);
       Audit_Trail.column_insert (raid, 'AC_KEY', :NEW.AC_KEY);
      -- Audit_Trail.column_insert (raid, 'AC_LOGO', :NEW.AC_LOGO);
       Audit_Trail.column_insert (raid, 'AC_MAILFLAG', :NEW.AC_MAILFLAG);
       Audit_Trail.column_insert (raid, 'AC_MAXSTORAGE', :NEW.AC_MAXSTORAGE);
       Audit_Trail.column_insert (raid, 'AC_MAXUSR', :NEW.AC_MAXUSR);
       Audit_Trail.column_insert (raid, 'AC_MODRIGHT', :NEW.AC_MODRIGHT);
       Audit_Trail.column_insert (raid, 'AC_NAME', :NEW.AC_NAME);
       Audit_Trail.column_insert (raid, 'AC_NOTE', :NEW.AC_NOTE);
       Audit_Trail.column_insert (raid, 'AC_PUBFLAG', :NEW.AC_PUBFLAG);
       Audit_Trail.column_insert (raid, 'AC_STAT', :NEW.AC_STAT);
       Audit_Trail.column_insert (raid, 'AC_STRTDT', :NEW.AC_STRTDT);
       Audit_Trail.column_insert (raid, 'AC_TYPE', :NEW.AC_TYPE);
       Audit_Trail.column_insert (raid, 'AC_USRCREATOR', :NEW.AC_USRCREATOR);
       Audit_Trail.column_insert (raid, 'CREATED_ON', TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_ROLE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_ROLE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_SKIN;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_SKIN', codeList_desc);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'MAX_PORTALS', :NEW.MAX_PORTALS);
       Audit_Trail.column_insert (raid, 'PK_ACCOUNT', :NEW.PK_ACCOUNT);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'SITE_CODE', :NEW.SITE_CODE);
COMMIT;
END;
/