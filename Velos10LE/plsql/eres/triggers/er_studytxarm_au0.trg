CREATE OR REPLACE TRIGGER ER_STUDYTXARM_AU0
AFTER UPDATE
ON ER_STUDYTXARM REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  old_codetype VARCHAR2(200) ;
  new_codetype VARCHAR2(200) ;
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  usr VARCHAR2(100);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  usr := getuser(:NEW.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_STUDYTXARM', :OLD.rid, 'U', usr);
  IF NVL(:OLD.pk_studytxarm,0) !=
     NVL(:NEW.pk_studytxarm,0) THEN
     audit_trail.column_update
       (raid, 'PK_STUDYTXARM',
       :OLD.pk_studytxarm, :NEW.pk_studytxarm);
  END IF;

  IF NVL(:OLD.fk_study,0) !=
     NVL(:NEW.fk_study,0) THEN
     audit_trail.column_update
       (raid, 'FK_STUDY',
       :OLD.fk_study, :NEW.fk_study);
  END IF;
  IF NVL(:OLD.tx_name,' ') !=
     NVL(:NEW.tx_name,' ') THEN
     audit_trail.column_update
       (raid, 'TX_NAME',
       :OLD.tx_name, :NEW.tx_name);
  END IF;
IF NVL(:OLD.tx_drug_info,' ') !=
     NVL(:NEW.tx_drug_info,' ') THEN
     audit_trail.column_update
       (raid, 'TX_DRUG_INFO',
       :OLD.tx_drug_info, :NEW.tx_drug_info);
  END IF;
IF NVL(:OLD.tx_desc,' ') !=
     NVL(:NEW.tx_desc,' ') THEN
     audit_trail.column_update
       (raid, 'TX_DESC',
       :OLD.tx_desc, :NEW.tx_desc);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
    
IF  NVL(:OLD.last_modified_by,0) != NVL(:NEW.last_modified_by,0) THEN
     BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_modby := NULL ;
    END ;
     
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
 END IF;
  IF NVL(:OLD.last_modified_on,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_on,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN

     audit_trail.column_update
       (raid, 'LAST_MODIFIED_ON',
       to_char(:OLD.last_modified_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.created_on,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.created_on,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
              to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.IP_ADD,' ') !=
     NVL(:NEW.IP_ADD,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.IP_ADD, :NEW.IP_ADD);
  END IF;
END;
/


