CREATE OR REPLACE TRIGGER ER_FLDVALIDATE_AU0
AFTER UPDATE OF RID,IP_ADD,FK_FLDLIB,RECORD_TYPE,PK_FLDVALIDATE,FLDVALIDATE_OP1,FLDVALIDATE_OP2,FLDVALIDATE_VAL1,FLDVALIDATE_VAL2,LAST_MODIFIED_BY,FLDVALIDATE_LOGOP1,FLDVALIDATE_LOGOP2,LAST_MODIFIED_DATE,FLDVALIDATE_JAVASCR
ON ER_FLDVALIDATE REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  usr varchar2(100);
  old_modby varchar2(100) ;
  new_modby varchar2(100) ;

begin
  select seq_audit.nextval into raid from dual;

   usr := getuser(:new.last_modified_by);

  audit_trail.record_transaction
    (raid, 'ER_FLDVALIDATE', :old.rid, 'U', usr);

  if nvl(:old.pk_fldvalidate,0) !=
     NVL(:new.pk_fldvalidate,0) then
     audit_trail.column_update
       (raid, 'PK_FLDVALIDATE',
       :old.pk_fldvalidate, :new.pk_fldvalidate);
  end if;
  if nvl(:old.fk_fldlib,0) !=
     NVL(:new.fk_fldlib,0) then
     audit_trail.column_update
       (raid, 'FK_FLDLIB',
       :old.fk_fldlib, :new.fk_fldlib);
  end if;
  if nvl(:old.fldvalidate_op1,' ') !=
     NVL(:new.fldvalidate_op1,' ') then
     audit_trail.column_update
       (raid, 'FLDVALIDATE_OP1',
       :old.fldvalidate_op1, :new.fldvalidate_op1);
  end if;
  if nvl(:old.fldvalidate_val1,' ') !=
     NVL(:new.fldvalidate_val1,' ') then
     audit_trail.column_update
       (raid, 'FLDVALIDATE_VAL1',
       :old.fldvalidate_val1, :new.fldvalidate_val1);
  end if;
  if nvl(:old.fldvalidate_logop1,' ') !=
     NVL(:new.fldvalidate_logop1,' ') then
     audit_trail.column_update
       (raid, 'FLDVALIDATE_LOGOP1',
       :old.fldvalidate_logop1, :new.fldvalidate_logop1);
  end if;
  if nvl(:old.fldvalidate_op2,' ') !=
     NVL(:new.fldvalidate_op2,' ') then
     audit_trail.column_update
       (raid, 'FLDVALIDATE_OP2',
       :old.fldvalidate_op2, :new.fldvalidate_op2);
  end if;
  if nvl(:old.fldvalidate_val2,' ') !=
     NVL(:new.fldvalidate_val2,' ') then
     audit_trail.column_update
       (raid, 'FLDVALIDATE_VAL2',
       :old.fldvalidate_val2, :new.fldvalidate_val2);
  end if;
  if nvl(:old.fldvalidate_logop2,' ') !=
     NVL(:new.fldvalidate_logop2,' ') then
     audit_trail.column_update
       (raid, 'FLDVALIDATE_LOGOP2',
       :old.fldvalidate_logop2, :new.fldvalidate_logop2);
  end if;
  if nvl(:old.fldvalidate_javascr,' ') !=
     NVL(:new.fldvalidate_javascr,' ') then
     audit_trail.column_update
       (raid, 'FLDVALIDATE_JAVASCR',
       :old.fldvalidate_javascr, :new.fldvalidate_javascr);
  end if;
  if nvl(:old.record_type,' ') !=
     NVL(:new.record_type,' ') then
     audit_trail.column_update
       (raid, 'RECORD_TYPE',
       :old.record_type, :new.record_type);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
   Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into old_modby
     from er_user
     where pk_user = :old.last_modified_by ;
     Exception When NO_DATA_FOUND then
      old_modby := null ;
     End ;
    Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into new_modby
     from er_user
     where pk_user = :new.last_modified_by ;
     Exception When NO_DATA_FOUND then
      new_modby := null ;
     End ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  end if;

  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ip_add,' ') !=
     NVL(:new.ip_add,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;

end;
/


