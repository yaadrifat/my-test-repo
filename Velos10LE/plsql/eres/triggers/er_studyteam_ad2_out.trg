CREATE OR REPLACE TRIGGER ERES.ER_STUDYTEAM_AD2_OUT
AFTER DELETE
ON ERES.ER_STUDYTEAM REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE
fkaccount NUMBER;
pkmsg NUMBER;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL  => Plog.LFATAL);
/******************************************************************************
   NAME:       ER_STUDYTEAM_AD2_OUT
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        9/26/2011    Kanwal         1. Created this trigger.

******************************************************************************/
BEGIN
  SELECT fk_account
  INTO fkaccount
  FROM er_user
  WHERE pk_user = NVL(:OLD.LAST_MODIFIED_BY, :OLD.CREATOR);

  PKG_MSG_QUEUE.SP_POPULATE_STUDYTEAM_MSG(
      :OLD.PK_STUDYTEAM,
      fkaccount,
      'D',
      to_char(:OLD.fk_study), :OLD.FK_USER, :OLD.FK_CODELST_TMROLE);
   EXCEPTION
     WHEN OTHERS THEN
       Plog.FATAL(pCTX,'exception in ER_STUDYTEAM_AD2_OUT' || SQLERRM);

END ER_STUDYTEAM_AD2_OUT;
/