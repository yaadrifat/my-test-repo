CREATE OR REPLACE TRIGGER "ER_PATPROT_AU_REV" 
AFTER UPDATE
ON ER_PATPROT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
/* **********************************
   **
   ** Author: Sonia Sahni 05/06/2004
   ** for reverse A2A
   ** Insert a record in er_rev_pendingdata
   *************************************
*/
v_key NUMBER;
v_pk NUMBER;
v_tabname VARCHAR2(50);
v_per NUMBER;
v_account NUMBER;
v_site_code VARCHAR2(255);
v_sponsor_study NUMBER;
v_err VARCHAR2(4000);

v_module_pat VARCHAR2(20);
v_module_commmon VARCHAR2(20);
v_sponsor_account NUMBER;
v_count NUMBER;

BEGIN

v_module_pat := 'pat_enr';
v_module_commmon := 'pat_common';

-- get account information and site code
v_per := :NEW.FK_PER;

  -- get account site code and sponsor study for :new.fk_study

PKG_IMPEX_REVERSE.SP_GET_STUDYSPONSORINFO(:NEW.FK_STUDY, v_sponsor_study , v_site_code, v_sponsor_account, v_err);

IF LENGTH(trim(v_site_code)) >  0 THEN
   IF v_sponsor_study > 0 THEN

--insert data for enrollment

v_tabname := 'er_patprot';
v_pk := :NEW.PK_PATPROT ;

 SELECT pkg_impex_reverse.getRevPendingDataCount(v_module_pat,     :NEW.FK_STUDY,  v_site_code, v_tabname,v_pk)
  INTO v_count FROM dual;

 IF v_count <= 0 THEN

 SELECT SEQ_ER_REV_PENDINGDATA.NEXTVAL
 INTO v_key FROM dual;

  INSERT INTO ER_REV_PENDINGDATA (pk_rp, rp_tablename, rp_tablepk , RP_SITECODE, FK_STUDY,RP_MODULE)
  VALUES (v_key ,v_tabname,v_pk,v_site_code,:NEW.FK_STUDY,v_module_pat);

 END IF; -- end of v_count


--insert data for patient

 v_tabname := 'er_per';
 v_pk := :NEW.FK_PER;

 SELECT pkg_impex_reverse.getRevPendingDataCount(v_module_pat,     :NEW.FK_STUDY,  v_site_code, v_tabname,v_pk)
  INTO v_count FROM dual;

 IF v_count <= 0 THEN

 SELECT SEQ_ER_REV_PENDINGDATA.NEXTVAL
 INTO v_key FROM dual;


 INSERT INTO ER_REV_PENDINGDATA (pk_rp, rp_tablename, rp_tablepk , RP_SITECODE,FK_STUDY ,RP_MODULE)
 VALUES (v_key ,v_tabname,v_pk,v_site_code,:NEW.FK_STUDY , v_module_pat);

 END IF;


--insert data for users

v_tabname := 'er_user';
v_pk := :NEW.FK_USER;

 SELECT pkg_impex_reverse.getRevPendingDataCount(v_module_commmon,       :NEW.FK_STUDY,  v_site_code, v_tabname,v_pk)
  INTO v_count FROM dual;

 IF v_count <= 0 THEN

 SELECT SEQ_ER_REV_PENDINGDATA.NEXTVAL
 INTO v_key FROM dual;


 INSERT INTO ER_REV_PENDINGDATA (pk_rp, rp_tablename, rp_tablepk , RP_SITECODE, FK_STUDY,RP_MODULE)
 VALUES (v_key ,v_tabname,v_pk,v_site_code , :NEW.FK_STUDY, v_module_commmon);

 END IF;

 v_tabname := 'er_user';
 v_pk := :NEW.FK_USERASSTO;

IF :NEW.FK_USERASSTO IS NOT NULL AND :NEW.FK_USERASSTO > 0 THEN
 SELECT pkg_impex_reverse.getRevPendingDataCount(v_module_commmon,       :NEW.FK_STUDY,  v_site_code, v_tabname,v_pk)
 INTO v_count FROM dual;

 IF v_count <= 0 THEN

 	SELECT SEQ_ER_REV_PENDINGDATA.NEXTVAL
	 INTO v_key FROM dual;

	  INSERT INTO ER_REV_PENDINGDATA (pk_rp, rp_tablename, rp_tablepk , RP_SITECODE, FK_STUDY,RP_MODULE)
	   VALUES (v_key ,v_tabname,v_pk,v_site_code,:NEW.FK_STUDY,v_module_commmon );

  END IF; -- for v_count
 END IF ; -- end of null check for :NEW.FK_USERASSTO

 --for physician
 v_tabname := 'er_user';
 v_pk := :NEW.PATPROT_PHYSICIAN;

IF :NEW.PATPROT_PHYSICIAN IS NOT NULL AND :NEW.PATPROT_PHYSICIAN > 0 THEN
 SELECT pkg_impex_reverse.getRevPendingDataCount(v_module_commmon, :NEW.FK_STUDY,  v_site_code, v_tabname,v_pk)
  INTO v_count FROM dual;

 IF v_count <= 0 THEN

 SELECT SEQ_ER_REV_PENDINGDATA.NEXTVAL
 INTO v_key FROM dual;

 INSERT INTO ER_REV_PENDINGDATA (pk_rp, rp_tablename, rp_tablepk , RP_SITECODE, FK_STUDY,RP_MODULE)
 VALUES (v_key ,v_tabname,v_pk,v_site_code,:NEW.FK_STUDY,v_module_commmon );

  END IF; -- for v_count
 END IF ; -- end of null check for :NEW.PATPROT_PHYSICIAN



  END IF; -- for if v_sponsor_study > 0
 END IF; -- for v_site_code
END;
/


