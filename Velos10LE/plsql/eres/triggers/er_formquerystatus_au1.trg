CREATE OR REPLACE TRIGGER ERES."ER_FORMQUERYSTATUS_AU1"
AFTER UPDATE ON ERES.ER_FORMQUERYSTATUS   REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
DECLARE
  v_querymodule_linkedto number;
  v_querymodule number;
BEGIN
  select QUERYMODULE_LINKEDTO, FK_QUERYMODULE into v_querymodule_linkedto, v_querymodule
  from ER_FORMQUERY where pk_formquery = :NEW.FK_FORMQUERY;

  CASE v_querymodule_linkedto
    WHEN 1 THEN
      update ER_PATFORMS set LAST_MODIFIED_DATE = sysdate, 
        LAST_MODIFIED_BY = NVL(:NEW.LAST_MODIFIED_BY, :NEW.CREATOR), RECORD_TYPE = 'M' where PK_PATFORMS = v_querymodule;
    WHEN 2 THEN
      update ER_STUDYFORMS set LAST_MODIFIED_DATE = sysdate, 
        LAST_MODIFIED_BY = NVL(:NEW.LAST_MODIFIED_BY, :NEW.CREATOR), RECORD_TYPE = 'M' where PK_STUDYFORMS = v_querymodule;
    WHEN 3 THEN
      update ER_ACCTFORMS set LAST_MODIFIED_DATE = sysdate, 
        LAST_MODIFIED_BY = NVL(:NEW.LAST_MODIFIED_BY, :NEW.CREATOR), RECORD_TYPE = 'M' where PK_ACCTFORMS = v_querymodule;
    WHEN 4 THEN
      update ER_PATFORMS set LAST_MODIFIED_DATE = sysdate, 
        LAST_MODIFIED_BY = NVL(:NEW.LAST_MODIFIED_BY, :NEW.CREATOR), RECORD_TYPE = 'M' where PK_PATFORMS = v_querymodule;
  ELSE DBMS_OUTPUT.PUT_LINE('Unknown linkedto from type');
  END CASE;

END;
/

