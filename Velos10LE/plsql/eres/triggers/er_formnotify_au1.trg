CREATE OR REPLACE TRIGGER "ER_FORMNOTIFY_AU1" 
AFTER UPDATE
ON ER_FORMNOTIFY
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
BEGIN


update er_formlib
set FORM_XSLREFRESH = 1
where pk_formlib = :new.fk_formlib         ;


END;
/


