CREATE OR REPLACE TRIGGER "ER_USER_BI1" 
BEFORE INSERT
ON ER_USER
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
rand number ;
begin
  if :new.usr_code is null then
    select trunc(dbms_random.value(1,999999999))
      into rand
      from dual ;
    :new.usr_code := substr(:new.usr_lastname,1,1)
         ||substr(:new.usr_lastname,1,1) || to_char(rand);
  end if;
end;
/


