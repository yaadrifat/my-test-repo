CREATE OR REPLACE TRIGGER "ER_USER_AI0" 
AFTER INSERT
ON ER_USER REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  v_pk_bud NUMBER(10);
  v_b_right VARCHAR2(10);

  v_new_fk_site NUMBER;
  v_new_fk_account  NUMBER;
  v_new_pk_user  NUMBER;
  v_new_creator  NUMBER;
  v_new_ip_add  VARCHAR2(15);

BEGIN

v_new_fk_site := :NEW.FK_SITEID;
v_new_fk_account := :NEW.FK_ACCOUNT;
v_new_pk_user :=   :NEW.pk_user;
v_new_creator :=  :NEW.creator;
v_new_ip_add :=   :NEW.ip_add;


FOR i IN (SELECT DISTINCT b.pk_budget,u.BGTUSERS_RIGHTS
          FROM sch_budget b, sch_bgtusers u
          WHERE b.FK_SITE IS NOT NULL AND
          b.FK_SITE = v_new_fk_site AND
          b.BUDGET_RIGHTSCOPE = 'O' AND
          b.BUDGET_SITEFLAG = 1 AND
          u.fk_budget = b.pk_budget AND u.BGTUSERS_TYPE = 'G' UNION
		SELECT DISTINCT b.pk_budget,u.BGTUSERS_RIGHTS
          FROM sch_budget b, sch_bgtusers u
          WHERE b.FK_ACCOUNT IS NOT NULL AND
          b.FK_ACCOUNT = v_new_fk_account AND
          b.BUDGET_RIGHTSCOPE = 'A' AND
          u.fk_budget = b.pk_budget AND u.BGTUSERS_TYPE = 'G'
		)
LOOP
    v_pk_bud :=   i.pk_budget;
    v_b_right := i.BGTUSERS_RIGHTS;
   INSERT INTO sch_bgtusers(PK_BGTUSERS,
                                    FK_BUDGET ,
                                    FK_USER  ,
                                    BGTUSERS_RIGHTS ,
                                    BGTUSERS_TYPE ,
                                    CREATOR,
                                    CREATED_ON ,
                                    IP_ADD )
   VALUES (SEQ_SCH_BGTUSERS.NEXTVAL,
           v_pk_bud,
           v_new_pk_user,
           v_b_right,
           'G',
           v_new_creator,
           SYSDATE,v_new_ip_add )  ;
END LOOP;
END;
/


