CREATE  OR REPLACE TRIGGER ER_SITE_BI0 BEFORE INSERT ON ER_SITE        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_SITE',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON', TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'FK_ACCOUNT', :NEW.FK_ACCOUNT);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_TYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_TYPE', codeList_desc);
       Audit_Trail.column_insert (raid, 'FK_PERADD', :NEW.FK_PERADD);
       Audit_Trail.column_insert (raid, 'GUID', :NEW.GUID);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_SITE', :NEW.PK_SITE);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'SITE_ALTID', :NEW.SITE_ALTID);
       Audit_Trail.column_insert (raid, 'SITE_HIDDEN', :NEW.SITE_HIDDEN);
       Audit_Trail.column_insert (raid, 'SITE_ID', :NEW.SITE_ID);
       Audit_Trail.column_insert (raid, 'SITE_INFO', :NEW.SITE_INFO);
       Audit_Trail.column_insert (raid, 'SITE_NAME', :NEW.SITE_NAME);
       Audit_Trail.column_insert (raid, 'SITE_NOTES', :NEW.SITE_NOTES);
       Audit_Trail.column_insert (raid, 'SITE_PARENT', :NEW.SITE_PARENT);
       Audit_Trail.column_insert (raid, 'SITE_RESTRICT', :NEW.SITE_RESTRICT);
       Audit_Trail.column_insert (raid, 'SITE_SEQ', :NEW.SITE_SEQ);
       Audit_Trail.column_insert (raid, 'SITE_STAT', :NEW.SITE_STAT);
COMMIT;
END;
/
