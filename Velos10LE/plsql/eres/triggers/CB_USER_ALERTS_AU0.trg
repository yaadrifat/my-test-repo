CREATE OR REPLACE TRIGGER CB_USER_ALERTS_AU0  AFTER UPDATE OF 
PK_ALERT,
FK_USER,
FK_CODELST_ALERT,
CREATOR,
CREATED_ON,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
IP_ADD,
DELETEDFLAG,
RID,
EMAIL_FLAG,
NOTIFY_FLAG,
PROMPT_FLAG  ON CB_USER_ALERTS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
   RAID NUMBER(10);
   USR VARCHAR(200); 
   OLD_MODIFIED_BY VARCHAR2(100);
   NEW_MODIFIED_BY VARCHAR2(100);
  
  BEGIN
  SELECT SEQ_AUDIT.NEXTVAL INTO RAID FROM DUAL;
     USR := GETUSER(:NEW.LAST_MODIFIED_BY);
  AUDIT_TRAIL.RECORD_TRANSACTION(RAID, 'CB_USER_ALERTS', :OLD.RID, 'U', USR);    
    
  IF NVL(:OLD.PK_ALERT,0) != NVL(:NEW.PK_ALERT,0) THEN
     AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'PK_ALERT',:OLD.PK_ALERT, :NEW.PK_ALERT);
  END IF;   
    IF NVL(:OLD.FK_USER,0) != NVL(:NEW.FK_USER,0) THEN
   AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'FK_USER', :OLD.FK_USER, :NEW.FK_USER);
    END IF;  
    IF NVL(:OLD.FK_CODELST_ALERT,0) != NVL(:NEW.FK_CODELST_ALERT,0) THEN
   AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'FK_CODELST_ALERT', :OLD.FK_CODELST_ALERT, :NEW.FK_CODELST_ALERT);
    END IF;    
    
  IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
     AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'CREATOR',:OLD.CREATOR, :NEW.CREATOR);
  END IF;  
   IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     AUDIT_TRAIL.COLUMN_UPDATE
       (RAID, 'CREATED_ON',
       TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;  
   
    IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) THEN
       Begin
      Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
      into old_modified_by from er_user  where pk_user = :old.last_modified_by ;
      Exception When NO_DATA_FOUND then
            old_modified_by := null;
    End ;
    Begin
      Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
      into new_modified_by   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
      Exception When NO_DATA_FOUND then
            new_modified_by := null;
    End ;
      audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modified_by, new_modified_by);    
    END IF; 
	 IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     AUDIT_TRAIL.COLUMN_UPDATE
       (RAID, 'LAST_MODIFIED_DATE',
       TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;  
    IF NVL(:OLD.IP_ADD,' ') !=   NVL(:NEW.IP_ADD,' ') THEN
     AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'IP_ADD', :OLD.IP_ADD, :NEW.IP_ADD);
  END IF;  
   IF NVL(:OLD.DELETEDFLAG,' ') != NVL(:NEW.DELETEDFLAG,' ') THEN
     AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'DELETEDFLAG', :OLD.DELETEDFLAG, :NEW.DELETEDFLAG);
    END IF;    
      
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
     AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'RID',:OLD.RID, :NEW.RID);     
    END IF;       
    
    IF NVL(:OLD.EMAIL_FLAG,' ') != NVL(:NEW.EMAIL_FLAG,' ') THEN
   AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'EMAIL_FLAG', :OLD.EMAIL_FLAG, :NEW.EMAIL_FLAG);
    END IF; 
     IF NVL(:OLD.NOTIFY_FLAG,' ') != NVL(:NEW.NOTIFY_FLAG,' ') THEN
   AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'NOTIFY_FLAG', :OLD.NOTIFY_FLAG, :NEW.NOTIFY_FLAG);
    END IF;     
  IF NVL(:OLD.PROMPT_FLAG,' ') != NVL(:NEW.PROMPT_FLAG,' ') THEN
   AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'PROMPT_FLAG', :OLD.PROMPT_FLAG, :NEW.PROMPT_FLAG);
    END IF;    
END ;
/

