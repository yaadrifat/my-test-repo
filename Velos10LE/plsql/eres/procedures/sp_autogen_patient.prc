CREATE OR REPLACE PROCEDURE        "SP_AUTOGEN_PATIENT" (p_account number, p_patientid out varchar2 )
as
v_patientid varchar2(100);
v_sql varchar2(4000);
v_last_id number;
Begin
    select patientid_autogen_sql,(patientid_lastid + 1) as patientid_lastid into v_sql,v_last_id from er_account_settings where fk_account = p_account;
    update er_account_settings set patientid_lastid = v_last_id  where fk_account = p_account;
    execute immediate v_sql into v_patientid;
    p_patientid := v_patientid || v_last_id;
End ;
/


GRANT EXECUTE ON SP_AUTOGEN_PATIENT TO EPAT;

