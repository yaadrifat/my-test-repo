CREATE OR REPLACE PROCEDURE        "SP_COPY_LNKCRFFORMS_FOREVENT" (
   p_calid   IN       NUMBER,
   p_user  IN VARCHAR2,
   p_ip IN VARCHAR2,
   o_ret   OUT      NUMBER)
AS
   /****************************************************************************************************
   ** Procedure to copy event crfs along with forms linked to the event
   ** It is called during copy study protocol calendar process, after the entire calendar and events have been copied
   ** Author Sonika Talwar March 11, 04
   ** Parameter Description
   ** p_calid newly copied calendar to whose events the crf forms have to be linked
   ** p_user the user to whom the copy is being given
   ** p_ip the ip address
   ** o_ret - output of the procedure, 0 for success, -1 for error
   *****************************************************************************************************
   */
   v_arrformid  ARRAY_STRING  :=  ARRAY_STRING();
   v_arrcrfnumber  ARRAY_STRING  :=  ARRAY_STRING();
   v_arrcrfname  ARRAY_STRING  :=  ARRAY_STRING();
   counter NUMBER;
   o_ret_number NUMBER;
   v_fk_account NUMBER;
   v_neweventid NUMBER;
   v_orgid NUMBER;
   BEGIN
     --get the events of the new calendar. Also get the parent_id(org_id of each event). Using the parent_id we can find out
     --the forms associated with the original event and can create their copy in the new event
     FOR i IN (SELECT event_id, org_id
               FROM EVENT_ASSOC
               WHERE  CHAIN_ID = p_calid
			AND DISPLACEMENT > 0
			AND UPPER(EVENT_TYPE) ='A')
	LOOP
       v_neweventid := i.event_id;
	  v_orgid := i.org_id;
	  counter := 0;
       v_arrformid  :=  ARRAY_STRING();
       v_arrcrfnumber :=  ARRAY_STRING();
       v_arrcrfname  :=  ARRAY_STRING();
		 p('v_orgid ' || v_orgid);
		 p('v_neweventid ' || v_neweventid);
      --for each org_id i.e. events from which copy was made, check whether any linked forms exist
	  FOR j IN
	      (SELECT fk_formlib, crflib_number, crflib_name, fk_account
           FROM sch_crflib , er_linkedforms
           WHERE er_linkedforms.fk_event = sch_crflib.fk_events
           AND fk_crf = pk_crflib
           AND fk_events = v_orgid
           AND crflib_formflag = 1)
       LOOP
	      counter := counter +1;
           v_arrcrfnumber.EXTEND;
           v_arrcrfname.EXTEND;
           v_arrformid.EXTEND;
		 v_arrcrfname(counter) := j.crflib_name;
		 v_arrcrfnumber(counter) := j.crflib_number;
           v_arrformid(counter) := j.fk_formlib;
		 v_fk_account := j.fk_account;
		 p('j.fk_formlib ' || j.fk_formlib);
	  END LOOP; --org_id loop
       --copy the linked forms to the new event
       PKG_LNKFORM.SP_LINK_CRFFORMS(v_arrformid,TO_CHAR(v_fk_account), v_arrcrfname,v_arrcrfnumber,p_calid,v_neweventid,TO_CHAR(p_user),p_ip,o_ret_number);
	  IF o_ret_number < 0 THEN
	    o_ret := -1;
         RETURN;
	  END IF;
    END LOOP; --calendar loop
    o_ret := 0;
    END;
/


CREATE SYNONYM ESCH.SP_COPY_LNKCRFFORMS_FOREVENT FOR SP_COPY_LNKCRFFORMS_FOREVENT;


CREATE SYNONYM EPAT.SP_COPY_LNKCRFFORMS_FOREVENT FOR SP_COPY_LNKCRFFORMS_FOREVENT;


GRANT EXECUTE, DEBUG ON SP_COPY_LNKCRFFORMS_FOREVENT TO EPAT;

GRANT EXECUTE, DEBUG ON SP_COPY_LNKCRFFORMS_FOREVENT TO ESCH;

