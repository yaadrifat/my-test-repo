CREATE OR REPLACE PROCEDURE        "SP_VERIFYEXUSER" (
   p_studyid IN VARCHAR2,
   p_userid IN VARCHAR2,
   returnvalue OUT NUMBER
   )
AS
BEGIN
   returnvalue := 0;

            -- To check if the user has been give (V)iew or (M)odify rights previously
            -- SQL will return 3 for View, 4 for modify else 0

            SELECT decode(max(msgcntr_perm),'V',3,'M',4,0)
                  INTO returnvalue
                  FROM er_msgcntr
                 WHERE fk_userx = p_userid
                   AND fk_study = p_studyid;
               dbms_output.put_line('Ret Code 3/4 - return value ' || to_char(returnvalue)) ;

   -- Check if the user has already requested for (V)iew / (M)odify premission for the same study previously
   -- The query will return 6 if View has been requested, 7 if both View and Modify have been requested and
   -- 8 if only modify has been requested

    IF nvl(returnvalue,0) = 0 then
     SELECT decode(max(trim(msgcntr_reqtype)),'V',3,'M',4,0) + decode(min(trim(msgcntr_reqtype)),'V',3,'M',4,0)
       INTO returnvalue
   FROM er_msgcntr
              WHERE fk_userx = p_userid
                AND fk_study = p_studyid
               AND msgcntr_perm is null ;
    END IF ;

               EXCEPTION
                 WHEN NO_DATA_FOUND THEN
                 dbms_output.put_line('Ret Code 3/4 - exception') ;
END   ;
/


CREATE SYNONYM ESCH.SP_VERIFYEXUSER FOR SP_VERIFYEXUSER;


CREATE SYNONYM EPAT.SP_VERIFYEXUSER FOR SP_VERIFYEXUSER;


GRANT EXECUTE, DEBUG ON SP_VERIFYEXUSER TO EPAT;

GRANT EXECUTE, DEBUG ON SP_VERIFYEXUSER TO ESCH;

