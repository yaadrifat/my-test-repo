CREATE OR REPLACE procedure      SP_DELETE_FORM (p_form IN Number)
as
/*
    Deletes all data related to a form.

    Disclaimer:

    1. Does not delete fields. It leaves orphan fields in er_fldlib (though they will not be visible in the application). These can be deleted from a
    separate script. Please note, even deleting a form from the application does not delete these fields.

    2. Does not delete Adhoc queries and Portals that are related with the form - if ANY!

    3. Does not affect following tables:

    -- field library:

    er_fldlib

    --adhoc queries----

    er_dynrep
    er_dynrepfilter
    er_dynrepfilterdt
    er_dynrepview

    --portal

    er_portal_modules
    er_portal_poplevel
    er_portal_logins
    er_portal

------------------************************
to execute from SQLPLUS:

set serveroutput on
exec sp_delete_form(:pk_formlib);

--it will print an error on server output if any

*/
begin
 begin

    delete from er_formslinear where fk_form = p_form;

    delete from er_mapform where fk_form = p_form;

    delete from er_fldactioninfo where fk_fldaction in (select pk_fldaction from er_fldaction where fk_form = p_form);
    delete from er_fldaction where fk_form = p_form;

    delete from er_formgrpacc where fk_formlib = p_form;

    delete from er_formorgacc where fk_formlib = p_form;

    delete from er_formnotify where fk_formlib = p_form;

    delete from er_formquerystatus where fk_formquery in (select pk_formquery from er_formquery
        where  fk_field in (
          select fk_field from er_formfld ,er_formsec where fk_formsec = pk_formsec and
        fk_formlib = p_form
        union
        select fk_field from er_repformfld ,er_formsec where fk_formsec = pk_formsec and
        fk_formlib = p_form    )
                );

    delete from er_formquery
        where  fk_field in (
          select fk_field from er_formfld ,er_formsec where fk_formsec = pk_formsec and
        fk_formlib = p_form
        union
        select fk_field from er_repformfld ,er_formsec where fk_formsec = pk_formsec and
        fk_formlib = p_form
    );



    delete from er_formsharewith where fk_formlib = p_form;

    delete from er_formstat where fk_formlib = p_form;

    delete from er_form_notlog where fk_formlib = p_form;
    delete from er_formauditcol where fk_form = p_form;


    delete from er_patforms where fk_formlib = p_form;
    delete from er_studyforms where fk_formlib = p_form;
    delete from er_acctforms where fk_formlib = p_form;
    delete from er_formaction where formaction_sourceform = p_form;


    delete from er_fldvalidate where  fk_fldlib in (
          select fk_field from er_formfld ,er_formsec where fk_formsec = pk_formsec and
        fk_formlib = p_form
        union
        select fk_field from er_repformfld ,er_formsec where fk_formsec = pk_formsec and
        fk_formlib = p_form    );



    delete from er_repfldvalidate where  fk_field in (
          select fk_field from er_formfld ,er_formsec where fk_formsec = pk_formsec and
        fk_formlib = p_form
        union
        select fk_field from er_repformfld ,er_formsec where fk_formsec = pk_formsec and
        fk_formlib = p_form    );


    delete from er_fldresp where  fk_field in (
          select fk_field from er_formfld ,er_formsec where fk_formsec = pk_formsec and
        fk_formlib = p_form
        union
        select fk_field from er_repformfld ,er_formsec where fk_formsec = pk_formsec and
        fk_formlib = p_form    );


    delete from er_repformfld where fk_formsec in (select pk_formsec from er_formsec where     fk_formlib = p_form);

    delete from er_formfld where fk_formsec in (select pk_formsec from er_formsec where     fk_formlib = p_form);


    delete from er_formsec where fk_formlib = p_form;

 	delete from er_objectshare where object_number = 1 and fk_object = p_form;

    delete from er_formlibver     where fk_formlib = p_form;

 	delete from er_linkedforms   where fk_formlib = p_form;
    delete from er_formlib     where pk_formlib = p_form;

     p('Form Deleted Successfully');

	commit;

 exception when others then
     p('Exception in deleting form' ||  sqlerrm);

 end;

end ;
/


