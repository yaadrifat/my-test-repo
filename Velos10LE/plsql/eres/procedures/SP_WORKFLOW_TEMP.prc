create or replace
PROCEDURE SP_WORKFLOW_TEMP IS
OUTSTR VARCHAR2(100 BYTE);
BEGIN
DBMS_OUTPUT.PUT_LINE('iN bEGIN');
DBMS_OUTPUT.PUT_LINE('BEFORE OPENING CURSOR');
for ORDERS in (SELECT ORDER_ID,ORDER_TYPE FROM ER_ORDER_TEMP)
loop
 dbms_output.put_line( '  inside loop --->' || orders.order_id||'....'||orders.order_type);
 sp_workflow(orders.order_id,orders.order_type,1,outstr);
 DBMS_OUTPUT.PUT_LINE('END IF');
 END LOOP;
 delete from er_order_temp;
END SP_WORKFLOW_TEMP;
/