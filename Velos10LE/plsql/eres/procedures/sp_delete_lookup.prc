CREATE OR REPLACE procedure      SP_DELETE_LOOKUP(p_lkplib IN Number)
as
/*
    Deletes all Lookup Definition Data


------------------************************
to execute from SQLPLUS:

set serveroutput on
exec SP_DELETE_LOOKUP(:p_lkplib);

--it will print an error on server output if any

*/
begin
 begin

    delete from er_lkpviewcol where fk_lkpview in (select pk_lkpview from er_lkpview where fk_lkplib = p_lkplib);

    delete from er_lkpview where fk_lkplib = p_lkplib;

    delete from er_lkpdata where fk_lkplib = p_lkplib;

    delete from er_lkpcol where fk_lkplib = p_lkplib;

    delete from er_lkplib where pk_lkplib = p_lkplib;

     p('Lookup Deleted Successfully');

    commit;

 exception when others then
     p('Exception in deleting lookup' ||  sqlerrm);

 end;

end ;
/


