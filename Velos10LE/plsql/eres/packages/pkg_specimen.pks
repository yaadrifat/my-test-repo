CREATE OR REPLACE PACKAGE        "PKG_SPECIMEN" IS

/* Sonia Abrol  11/26/07
creates a specimen for a form response, the procedure will be used from triggers so it will not have a 'commit'*/

procedure SP_CREATE_FORMRESP_SPEC(p_form IN NUMBER,p_creator IN NUMBER,p_ipadd IN varchar2,p_per In Number,p_study IN NUMBER, o_SPECIMEN OUT NUMBER);

function f_get_specimen_info(p_pk_specimen Number) return Varchar;


pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'Pkg_specimen', pLEVEL  => Plog.LFATAL);

END Pkg_Specimen  ;
/


