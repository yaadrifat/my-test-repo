CREATE OR REPLACE PACKAGE        "PKG_FORMVER" 
AS

PROCEDURE CREATEFORMVERSION (p_form NUMBER);


/* Added by Sonia Abrol, 09/11/06 -  get the form's version number from the filled from ID and from type*/
FUNCTION f_get_versionumber(p_filledformid NUMBER, p_formtype VARCHAR2) RETURN VARCHAR2;

/* Added by Sonia Abrol, 11/21/06 - update the formlib ver pk in formslinear for a form, used in form version migration*/
PROCEDURE SP_MIGRATE_FORMLIBVER( p_form IN VARCHAR2 ,p_formlibver IN VARCHAR2 , o_err OUT VARCHAR2);

/* Added by Sonia Abrol, 12/19/06 - update the data values in multiple choice fields, used in form version migration*/
PROCEDURE SP_MIGRATE_FORM_DATAVAL( p_form IN VARCHAR2);

pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_FORMVER', pLEVEL  => Plog.LFATAL);


END Pkg_Formver;
/


CREATE SYNONYM ESCH.PKG_FORMVER FOR PKG_FORMVER;


CREATE SYNONYM EPAT.PKG_FORMVER FOR PKG_FORMVER;


GRANT EXECUTE, DEBUG ON PKG_FORMVER TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_FORMVER TO ESCH;

