CREATE OR REPLACE PACKAGE        "PKG_VM_RULE" 
AS
   PROCEDURE SP_VM_COUNT_EVENT_DONE_ONE (
      P_STUDY                    NUMBER,
      P_ORG_ID                   NUMBER,
      P_CAL                      NUMBER,
      P_VISIT                    NUMBER,
      P_STDATE                   Date,
      P_ENDDATE                  Date,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   );
   PROCEDURE SP_VM_COUNT_EVENT_DONE_ALL (
      P_STUDY                    NUMBER,
      P_ORG_ID                   NUMBER,
      P_CAL                      NUMBER,
      P_VISIT                    NUMBER,
      P_STDATE                   Date,
      P_ENDDATE                  Date,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   );
   PROCEDURE SP_VM_PAT_CNT_EVENT_DONE_ONE (
      P_STUDY                    NUMBER,
      P_CAL                      NUMBER,
      P_VISIT                    NUMBER,
      P_STDATE                   Date,
      P_ENDDATE                  Date,
      P_PAT                      NUMBER,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   );
   PROCEDURE SP_VM_PATPROT_EVENT_DONE_ONE (
      P_PATPROT                  NUMBER,
      P_ORG_ID                   NUMBER,
      P_CAL                      NUMBER,
      P_VISIT                    NUMBER,
      P_STDATE                   Date,
      P_ENDDATE                  Date,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATE    OUT   Date
   );
   PROCEDURE SP_VM_PAT_CNT_EVENT_DONE_ALL (
      P_STUDY                    NUMBER,
      P_ORG_ID                   NUMBER,
      P_CAL                      NUMBER,
      P_VISIT                    NUMBER,
      P_STDATE                   Date,
      P_ENDDATE                  Date,
      P_PAT                      NUMBER,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   );
   PROCEDURE SP_VM_PATPROT_EVENT_DONE_ALL (
      P_PATPROT                    NUMBER,
      P_CAL                      NUMBER,
      P_VISIT                    NUMBER,
      P_STDATE                   Date,
      P_ENDDATE                  Date,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATE    OUT   Date
   );
   PROCEDURE SP_VIS_RULE_CRFS_COUNT (
      P_STUDY                    NUMBER,
      P_ORG_ID                   NUMBER,
      P_CAL                      NUMBER,
      P_VISIT                    NUMBER,
      P_CRFSTAT_SUBTYP           VARCHAR2,
      P_STDATE                   Date,
      P_ENDDATE                  Date,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   );
   PROCEDURE SP_VIS_RULE_CRFS_PATIENT (
      P_STUDY                    NUMBER,
      P_CAL                      NUMBER,
      P_VISIT                    NUMBER,
      P_PATIENT                  NUMBER,
      P_CRFSTAT_SUBTYP           VARCHAR2,
      P_STDATE                   Date,
      P_ENDDATE                  Date,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   );
   PROCEDURE SP_VIS_RULE_CRFS (
      P_PATPROT                  NUMBER,
      P_PATIENT                  NUMBER,
      P_VISIT                    NUMBER,
      P_CRFSTAT_SUBTYP           VARCHAR2,
      P_STDATE                   Date,
      P_ENDDATE                  Date,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENT_DATE   OUT   Date
   );

   PROCEDURE SP_FORE_VM_CNT_EVENTDONE_ONE (
      P_STUDY                    NUMBER,
      P_ORG_ID                   NUMBER,
      P_CAL                      NUMBER,
      P_VISIT                    NUMBER,
      P_STDATE                   Date,
      P_ENDDATE                  Date,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   );

   PROCEDURE SP_FORE_VIS_RULE_CRFS_CNT (
      P_STUDY                    NUMBER,
	 P_ORG_ID                   NUMBER,
      P_CAL                      NUMBER,
      P_VISIT                    NUMBER,
      P_CRFSTAT_SUBTYP           VARCHAR2,
      P_STDATE                   Date,
      P_ENDDATE                  Date,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   );

END PKG_VM_RULE;
/


CREATE SYNONYM ESCH.PKG_VM_RULE FOR PKG_VM_RULE;


CREATE SYNONYM EPAT.PKG_VM_RULE FOR PKG_VM_RULE;


GRANT EXECUTE, DEBUG ON PKG_VM_RULE TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_VM_RULE TO ESCH;

