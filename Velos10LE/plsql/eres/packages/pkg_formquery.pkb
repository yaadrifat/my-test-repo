CREATE OR REPLACE PACKAGE BODY "PKG_FORMQUERY"
IS
	function F_Is_FQueryCreator(P_FormQueryId Number, P_StudyId Number, P_fqCreatorId Number, P_fqCreatorType VARCHAR2) 
	return number
	IS
	  v_Return Number default 0;
	  Begin
	  
	  CASE (P_fqCreatorType)
	  	  WHEN 'role_All' THEN
			v_Return := 1;
	  	  WHEN 'role_system' THEN
	  	    SELECT COUNT(*) INTO v_Return FROM ER_FORMQUERYSTATUS 
	  	    WHERE FK_FORMQUERY = P_FormQueryId AND FORMQUERY_TYPE =1;
	  	  ELSE
	  	  	--SELECT COUNT(*) INTO v_Return FROM ER_STUDYTEAM WHERE FK_STUDY = P_StudyId AND FK_USER = P_fqCreatorId
		  	---AND P_fqCreatorType = (SELECT CODELST_SUBTYP FROM ER_CODELST WHERE PK_CODELST = FK_CODELST_TMROLE);
			SELECT DECODE (P_fqCreatorType, P_fqCreatorId,1,0) INTO v_Return  FROM dual;
	  END CASE;
	  
	  return v_Return;
	End; -- for function
END;
/

CREATE OR REPLACE SYNONYM EPAT.PKG_FORMQUERY FOR PKG_FORMQUERY;

CREATE OR REPLACE SYNONYM ESCH.PKG_FORMQUERY FOR PKG_FORMQUERY;