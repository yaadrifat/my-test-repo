CREATE OR REPLACE PACKAGE        "PKG_VIE" 
AS
PROCEDURE sp_messages;

pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_VIE', pLEVEL  => Plog.LFATAL);
PROCEDURE SP_ADT(
p_pk_message IN NUMBER,
p_sending_app IN VARCHAR2,
p_event VARCHAR2
);

PROCEDURE SP_PID(
p_pk_msgseg IN NUMBER,
p_status OUT NUMBER,
p_errtxt OUT VARCHAR2,
p_sending_app IN VARCHAR2);

PROCEDURE SP_LAB(
p_pk_message IN NUMBER,
p_sending_app IN VARCHAR2,
p_account NUMBER,
p_site NUMBER
);

PROCEDURE SP_CREATE_SQL(
p_pk_msgseg IN NUMBER,
p_seg_name IN VARCHAR2,
p_update_sql OUT VARCHAR2,
p_update_flag CHAR,
p_insert_col OUT VARCHAR2,
p_errtxt OUT VARCHAR2,
p_status OUT NUMBER,
p_sending_app IN VARCHAR2);

PROCEDURE SP_GETPATIENT(
p_pk_msgseg IN NUMBER,
p_status OUT NUMBER,
p_errtxt OUT VARCHAR2,
p_pk_person OUT NUMBER,
p_match_sql OUT VARCHAR2,
p_account NUMBER,
p_site NUMBER);

END Pkg_Vie;
/


CREATE SYNONYM ESCH.PKG_VIE FOR PKG_VIE;


CREATE SYNONYM EPAT.PKG_VIE FOR PKG_VIE;


GRANT EXECUTE, DEBUG ON PKG_VIE TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_VIE TO ESCH;

