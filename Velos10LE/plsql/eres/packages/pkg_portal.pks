CREATE OR REPLACE PACKAGE        "PKG_PORTAL" AS
/******************************************************************************
   NAME:       PKG_PATIENT
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        8/24/2007 Sonia Abrol             1. Created this package for portal specific Pl/SQL.
******************************************************************************/



   pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_PORTAL', pLEVEL  => Plog.LFATAL);

/* create default logins for the portal*/

  PROCEDURE SP_CREATE_DEFLOGINS(
   p_portal IN Number, p_creator IN NUMBER,p_ip_add in Varchar2, o_ret OUT NUMBER
   );

/* creates default login using the patient MRN if the account is set for automatic login facility*/

  PROCEDURE SP_CREATE_AUTOLOGIN(
   p_login IN Varchar, o_password out Varchar,o_loginflag out Number, o_err OUT Varchar,p_providedpassword In VARCHAR2,
   p_ipadd in varchar2
   );

  procedure sp_create_deflogin(p_calledfrom varchar2, p_patient in number,p_study in number,
  p_site in number,p_usr in number,p_ipadd in Varchar2
  );

END Pkg_Portal;
/


