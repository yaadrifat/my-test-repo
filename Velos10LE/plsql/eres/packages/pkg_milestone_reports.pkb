CREATE OR REPLACE PACKAGE BODY        "PKG_MILESTONE_REPORTS"
AS
   PROCEDURE SP_MS_FINANCIAL (
      P_STUDY            IN       NUMBER,
	 P_ORG_ID           IN       NUMBER,
      P_STDATE_STRING    IN       VARCHAR2,
      P_ENDDATE_STRING   IN       VARCHAR2,
      P_REPORT_TYPE      IN       VARCHAR2,
      O_XML_CLOB         OUT      CLOB
   )
   AS
/*Author: Sajal
  Date: 10th July 2002
  Purpose - Used for Milestones by Quarter report
*/
      V_ACHIVEMENT_DATE_ARRAY    TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_AMOUNT_ARRAY   TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_TYPE_ARRAY     TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_DESC_ARRAY     TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_RULE_ARRAY     TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_COUNT                    NUMBER                   := 0;
      V_REPORT_TYPE_FLAG         NUMBER                   := 0;
      TEMP_CLOB                  CLOB;
      V_TEMPVAR                  VARCHAR2 (32767);
      V_PMT_AMTS                 TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_PMT_DATES                TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_STDATE_STRING            VARCHAR2 (20);
      V_ENDDATE_STRING           VARCHAR2 (20);
--      V_STUDY_NUMBER             VARCHAR2 (50);
	        V_STUDY_NUMBER             VARCHAR2 (100);--JM:120406
      V_STUDY_VERSION            VARCHAR2 (50);
      V_STUDY_TITLE              VARCHAR2 (2000);
      V_STUDY_ACT_DATE           VARCHAR2 (20);
      V_STUDY_CURRENCY           VARCHAR2 (20);
      V_MIN_YEAR                 NUMBER;
      V_MAX_YEAR                 NUMBER;
      V_NO_OF_YRS                NUMBER;
      J                          NUMBER;
      F                          NUMBER;
      V_YR_COUNT                 NUMBER;
      V_RELATIVE_INTERVAL        NUMBER;
      V_TOTAL_INTERVALS          NUMBER;
      V_INTERVAL_ARRAY           TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_VISIT_CNT_ARRAY          TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_EVENT_CNT_ARRAY          TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_PATIENT_CNT_ARRAY        TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_VISIT_AMT_ARRAY          TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_EVENT_AMT_ARRAY          TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_PATIENT_AMT_ARRAY        TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_TOTAL_AMT_ARRAY          TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_PYMT_AMT_ARRAY           TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_PYMT_DISCREPANCY_ARRAY   TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_ACT_DATE_STRING          VARCHAR2 (20);
      V_BEF_ACT_STDATE_STRING    VARCHAR2 (20);
      V_BEF_ACT_ENDDATE_STRING   VARCHAR2 (20);
      V_AFT_ACT_STDATE_STRING    VARCHAR2 (20);
      V_AFT_ACT_ENDDATE_STRING   VARCHAR2 (20);
	 V_ORG_NAME                       VARCHAR2 (50);
	 V_ORG_ID                         NUMBER;
      V_CUR   TYPES.cursorType;
   BEGIN
      SP_FIND_STUDY_ACT_DATES (
         P_STUDY,
         P_STDATE_STRING,
         P_ENDDATE_STRING,
         V_ACT_DATE_STRING,
         V_BEF_ACT_STDATE_STRING,
         V_BEF_ACT_ENDDATE_STRING,
         V_AFT_ACT_STDATE_STRING,
         V_AFT_ACT_ENDDATE_STRING
      );
      V_RELATIVE_INTERVAL := -1;

      SELECT STUDY_NUMBER, STUDY_VER_NO, STUDY_TITLE
        INTO V_STUDY_NUMBER, V_STUDY_VERSION, V_STUDY_TITLE
        FROM ER_STUDY
       WHERE PK_STUDY = P_STUDY;
      SELECT CODELST_SUBTYP
        INTO V_STUDY_CURRENCY
        FROM SCH_CODELST, ER_STUDY
       WHERE FK_CODELST_CURRENCY = PK_CODELST
         AND PK_STUDY = P_STUDY;

      DBMS_LOB.CREATETEMPORARY (O_XML_CLOB, FALSE);
      DBMS_LOB.OPEN (O_XML_CLOB, DBMS_LOB.LOB_READWRITE);
      V_TEMPVAR := '<?xml version="1.0"?><ROWSET>';
      DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);

      V_TEMPVAR := '<STUDY><STUDYNUM>' ||
                   V_STUDY_NUMBER ||
                   '</STUDYNUM><STUDYVER>' ||
                   V_STUDY_VERSION ||
                   '</STUDYVER><STUDYTITLE>' ||
                   V_STUDY_TITLE ||
                   '</STUDYTITLE><ACTIVATIONDATE>' ||
                   V_ACT_DATE_STRING ||
                   '</ACTIVATIONDATE><STUDYCURRENCY>' ||
                   V_STUDY_CURRENCY ||
                   '</STUDYCURRENCY></STUDY>';
      DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);

      IF (V_AFT_ACT_STDATE_STRING <> ' ') THEN


      IF p_org_id > 0 THEN
        OPEN v_cur FOR SELECT DISTINCT fk_site, site_name
                 FROM ER_PATPROT, ER_SITE, ER_PER
                 WHERE fk_study=p_study
                 AND pk_site=fk_site
                 AND pk_per=fk_per
			  AND pk_site = p_org_id;
	 ELSE
        OPEN v_cur FOR SELECT DISTINCT fk_site, site_name
                 FROM ER_PATPROT, ER_SITE, ER_PER
                 WHERE fk_study=p_study
                 AND pk_site=fk_site
                 AND pk_per=fk_per ;
      END IF ;
      LOOP

	 FETCH v_cur INTO v_org_id, v_org_name ;
	 EXIT WHEN v_cur%NOTFOUND;
 	 --reset the totals for each organization
      V_COUNT                  := 0;

      V_INTERVAL_ARRAY := TYPES.SMALL_STRING_ARRAY();
      V_VISIT_CNT_ARRAY            := TYPES.NUMBER_ARRAY ();
      V_EVENT_CNT_ARRAY            := TYPES.NUMBER_ARRAY ();
      V_PATIENT_CNT_ARRAY            := TYPES.NUMBER_ARRAY ();
      V_VISIT_AMT_ARRAY            := TYPES.NUMBER_ARRAY ();
      V_EVENT_AMT_ARRAY            := TYPES.NUMBER_ARRAY ();
      V_PATIENT_AMT_ARRAY            := TYPES.NUMBER_ARRAY ();
      V_TOTAL_AMT_ARRAY            := TYPES.NUMBER_ARRAY ();
      V_PYMT_AMT_ARRAY            := TYPES.NUMBER_ARRAY ();
      V_PYMT_DISCREPANCY_ARRAY    := TYPES.NUMBER_ARRAY ();
      V_ACHIVEMENT_DATE_ARRAY     := TYPES.SMALL_STRING_ARRAY ();


      V_TOTAL_INTERVALS := 0;
	 V_RELATIVE_INTERVAL :=0;

      V_TEMPVAR := '<ORG name="'||V_ORG_NAME||'">';
      DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);


	 V_MIN_YEAR :=0;
      V_MAX_YEAR := 0;
	    P('V_ORG_NAME ' || V_ORG_NAME);

         SP_MILESTONE_STUDY_SPECIFIC (
            P_STUDY,
     	  V_ORG_ID,
            V_AFT_ACT_STDATE_STRING,
            V_AFT_ACT_ENDDATE_STRING,
            V_COUNT,
            V_ACHIVEMENT_DATE_ARRAY,
            V_MILESTONE_AMOUNT_ARRAY,
            V_MILESTONE_TYPE_ARRAY,
            V_MILESTONE_DESC_ARRAY,
            V_MILESTONE_RULE_ARRAY
         );
         V_MIN_YEAR := TO_NUMBER (SUBSTR (V_AFT_ACT_STDATE_STRING, 7, 4));
         V_MAX_YEAR := TO_NUMBER (SUBSTR (V_AFT_ACT_ENDDATE_STRING, 7, 4));

         IF (V_MIN_YEAR IS NULL) THEN
            V_MIN_YEAR := V_MAX_YEAR + 1;
         END IF;

         V_NO_OF_YRS := V_MAX_YEAR - V_MIN_YEAR + 1;
         P ('V_NO_OF_YRS ' || V_NO_OF_YRS);
         V_YR_COUNT := V_MIN_YEAR;
         P ('V_YR_COUNT ' || V_YR_COUNT);
         J := 1;
         IF (P_REPORT_TYPE = 'M') THEN
            V_TOTAL_INTERVALS := 12 * V_NO_OF_YRS;
            --p('V_TOTAL_INTERVALS 1: ' ||V_TOTAL_INTERVALS);
            FOR I IN 1 .. V_NO_OF_YRS
            LOOP
               V_INTERVAL_ARRAY.EXTEND (12);
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 1) :=
                  'January, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 2) :=
                  'February, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 3) := 'March, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 4) := 'April, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 5) := 'May, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 6) := 'June, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 7) := 'July, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 8) :=
                  'August, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 9) :=
                  'September, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 10) :=
                  'October, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 11) :=
                  'November, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 12) :=
                  'December, ' || V_YR_COUNT;
               V_YR_COUNT := V_YR_COUNT + 1;
            END LOOP;
         END IF;
         IF (P_REPORT_TYPE = 'Q') THEN
            V_TOTAL_INTERVALS := 4 * V_NO_OF_YRS;
            FOR I IN 1 .. V_NO_OF_YRS
            LOOP
               V_INTERVAL_ARRAY.EXTEND (4);
               V_INTERVAL_ARRAY ((4 * (I - 1)) + 1) := 'Q1, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((4 * (I - 1)) + 2) := 'Q2, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((4 * (I - 1)) + 3) := 'Q3, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((4 * (I - 1)) + 4) := 'Q4, ' || V_YR_COUNT;
               V_YR_COUNT := V_YR_COUNT + 1;
            END LOOP;
         END IF;
         IF (P_REPORT_TYPE = 'Y') THEN
            V_TOTAL_INTERVALS := V_NO_OF_YRS;
            FOR I IN 1 .. V_NO_OF_YRS
            LOOP
               V_INTERVAL_ARRAY.EXTEND;
               V_INTERVAL_ARRAY (I) := 'Year ' || V_YR_COUNT;
               V_YR_COUNT := V_YR_COUNT + 1;
            END LOOP;
         END IF;

         V_INTERVAL_ARRAY.EXTEND;
         V_VISIT_CNT_ARRAY.EXTEND;
         V_EVENT_CNT_ARRAY.EXTEND;
         V_PATIENT_CNT_ARRAY.EXTEND;
         V_VISIT_AMT_ARRAY.EXTEND;
         V_EVENT_AMT_ARRAY.EXTEND;
         V_PATIENT_AMT_ARRAY.EXTEND;
         V_TOTAL_AMT_ARRAY.EXTEND;
         V_PYMT_AMT_ARRAY.EXTEND;
         V_PYMT_DISCREPANCY_ARRAY.EXTEND;
         V_VISIT_CNT_ARRAY (1) := 0;
         V_EVENT_CNT_ARRAY (1) := 0;
         V_PATIENT_CNT_ARRAY (1) := 0;
         V_VISIT_AMT_ARRAY (1) := 0;
         V_EVENT_AMT_ARRAY (1) := 0;
         V_PATIENT_AMT_ARRAY (1) := 0;
         V_TOTAL_AMT_ARRAY (1) := 0;
         V_PYMT_AMT_ARRAY (1) := 0;
         V_PYMT_DISCREPANCY_ARRAY (1) := 0;
--p('v_total_intervals 2:' ||v_total_intervals);
         V_VISIT_CNT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_EVENT_CNT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_PATIENT_CNT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_VISIT_AMT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_EVENT_AMT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_PATIENT_AMT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_TOTAL_AMT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_PYMT_AMT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_PYMT_DISCREPANCY_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         --p('v_count ' ||v_count);
         IF (P_REPORT_TYPE = 'M') THEN
            FOR I IN 1 .. V_COUNT
            LOOP
--               p('V_ACHIVEMENT_DATE_ARRAY (I)' || V_ACHIVEMENT_DATE_ARRAY (I));
  --             p('V_MIN_YEAR ' ||V_MIN_YEAR);
               V_RELATIVE_INTERVAL :=
                  (12 *
                      (TO_NUMBER (SUBSTR (V_ACHIVEMENT_DATE_ARRAY (I), 7, 4)) -
                       V_MIN_YEAR
                      )
                  ) +
                  TO_NUMBER (SUBSTR (V_ACHIVEMENT_DATE_ARRAY (I), 1, 2));
P('V_RELATIVE_INTERVAL 0: ' ||V_RELATIVE_INTERVAL);
               IF (V_MILESTONE_TYPE_ARRAY (I) = 'PM') THEN
                  V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;
--p('V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) ' ||V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) );
               IF (V_MILESTONE_TYPE_ARRAY (I) = 'EM') THEN
                  V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;
--p('V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) ' ||V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL));
               IF (V_MILESTONE_TYPE_ARRAY (I) = 'VM') THEN
                  V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;
--p('V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) ' ||V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL));
               V_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                  V_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                  TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
            END LOOP;
         END IF;
         IF (P_REPORT_TYPE = 'Q') THEN
            FOR I IN 1 .. V_COUNT
            LOOP
               V_RELATIVE_INTERVAL :=
                  (4 *
                      (TO_NUMBER (SUBSTR (V_ACHIVEMENT_DATE_ARRAY (I), 7, 4)) -
                       V_MIN_YEAR
                      )
                  ) +
                  (TO_NUMBER (
                      SUBSTR (
                         TO_CHAR (
                            (TO_NUMBER (
                                SUBSTR (V_ACHIVEMENT_DATE_ARRAY (I), 1, 2)
                             ) -
                             1
                            ) /
                               3,
                            '0.0'
                         ),
                         2,
                         1
                      )
                   ) +
                   1
                  );
               IF (V_MILESTONE_TYPE_ARRAY (I) = 'PM') THEN
                  V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;
               IF (V_MILESTONE_TYPE_ARRAY (I) = 'EM') THEN
                  V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;
               IF (V_MILESTONE_TYPE_ARRAY (I) = 'VM') THEN
                  V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;
               V_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                  V_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                  TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
            END LOOP;
         END IF;
         IF (P_REPORT_TYPE = 'Y') THEN
            FOR I IN 1 .. V_COUNT
            LOOP
               V_RELATIVE_INTERVAL :=
                  TO_NUMBER (SUBSTR (V_ACHIVEMENT_DATE_ARRAY (I), 7, 4)) -
                  V_MIN_YEAR +
                  1;
               IF (V_MILESTONE_TYPE_ARRAY (I) = 'PM') THEN
                  V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;
               IF (V_MILESTONE_TYPE_ARRAY (I) = 'EM') THEN
                  V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;
               IF (V_MILESTONE_TYPE_ARRAY (I) = 'VM') THEN
                  V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;
               V_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                  V_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                  TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
            END LOOP;
         END IF;
         SP_MILEPAYMENT (
            P_STUDY,
            V_AFT_ACT_STDATE_STRING,
            V_AFT_ACT_ENDDATE_STRING,
            V_COUNT,
            V_PMT_AMTS,
            V_PMT_DATES
         );
--p('414 V_COUNT' || V_COUNT);
         IF (P_REPORT_TYPE = 'M') THEN
            FOR I IN 1 .. V_COUNT
            LOOP
--p('V_PMT_DATES (I)' || V_PMT_DATES (I));
--p('V_MIN_YEAR' || V_MIN_YEAR);
               V_RELATIVE_INTERVAL :=
                  (12 *
                      (TO_NUMBER (
                          SUBSTR (
                             V_PMT_DATES (
                                I
                             ),
                             7,
                             4
                          )
                       ) -
                       V_MIN_YEAR
                      )
                  ) +
                  TO_NUMBER (
                     SUBSTR (
                        V_PMT_DATES (
                           I
                        ),
                        1,
                        2
                     )
                  );
--p('443 V_RELATIVE_INTERVAL' || V_RELATIVE_INTERVAL);
--p('443 V_PYMT_AMT_ARRAY' || V_PYMT_AMT_ARRAY.count);
               V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                  V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                  TO_NUMBER (V_PMT_AMTS (I));
            END LOOP;
         END IF;
         IF (P_REPORT_TYPE = 'Q') THEN
            FOR I IN 1 .. V_COUNT
            LOOP
               V_RELATIVE_INTERVAL :=
                  (4 *
                      (TO_NUMBER (
                          SUBSTR (
                             V_PMT_DATES (
                                I
                             ),
                             7,
                             4
                          )
                       ) -
                       V_MIN_YEAR
                      )
                  ) +
                  (TO_NUMBER (
                      SUBSTR (
                         TO_CHAR (
                            (TO_NUMBER (
                                SUBSTR (
                                   V_PMT_DATES (
                                      I
                                   ),
                                   1,
                                   2
                                )
                             ) -
                             1
                            ) /
                               3,
                            '0.0'
                         ),
                         2,
                         1
                      )
                   ) +
                   1
                  );
               V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                  V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                  TO_NUMBER (V_PMT_AMTS (I));
            END LOOP;
         END IF;
         IF (P_REPORT_TYPE = 'Y') THEN
            FOR I IN 1 .. V_COUNT
            LOOP
               V_RELATIVE_INTERVAL :=
                  TO_NUMBER (SUBSTR (V_PMT_DATES (I), 7, 4)) - V_MIN_YEAR + 1;
               V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                  V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                  TO_NUMBER (V_PMT_AMTS (I));
            END LOOP;
         END IF;
         J := V_TOTAL_INTERVALS + 1;
         V_INTERVAL_ARRAY (J) := 'Total';
--p('V_TOTAL_INTERVALS 3: '||V_TOTAL_INTERVALS);
         FOR I IN 1 .. V_TOTAL_INTERVALS
         LOOP
            V_VISIT_CNT_ARRAY (J) :=
               V_VISIT_CNT_ARRAY (J) + V_VISIT_CNT_ARRAY (I);
            V_EVENT_CNT_ARRAY (J) :=
               V_EVENT_CNT_ARRAY (J) + V_EVENT_CNT_ARRAY (I);
            V_PATIENT_CNT_ARRAY (J) :=
               V_PATIENT_CNT_ARRAY (J) + V_PATIENT_CNT_ARRAY (I);
            V_VISIT_AMT_ARRAY (J) :=
               V_VISIT_AMT_ARRAY (J) + V_VISIT_AMT_ARRAY (I);
            V_EVENT_AMT_ARRAY (J) :=
               V_EVENT_AMT_ARRAY (J) + V_EVENT_AMT_ARRAY (I);
            V_PATIENT_AMT_ARRAY (J) :=
               V_PATIENT_AMT_ARRAY (J) + V_PATIENT_AMT_ARRAY (I);
            V_TOTAL_AMT_ARRAY (J) :=
               V_TOTAL_AMT_ARRAY (J) + V_TOTAL_AMT_ARRAY (I);
            V_PYMT_AMT_ARRAY (J) := V_PYMT_AMT_ARRAY (J) + V_PYMT_AMT_ARRAY (I);
            V_PYMT_DISCREPANCY_ARRAY (I) :=
               V_PYMT_AMT_ARRAY (I) - V_TOTAL_AMT_ARRAY (I);
            V_PYMT_DISCREPANCY_ARRAY (J) :=
               V_PYMT_DISCREPANCY_ARRAY (J) + V_PYMT_DISCREPANCY_ARRAY (I);
         END LOOP;
         V_RELATIVE_INTERVAL := J - 1;
         IF (P_REPORT_TYPE = 'M') THEN
            V_RELATIVE_INTERVAL :=
               (12 *
                   (TO_NUMBER (SUBSTR (V_AFT_ACT_ENDDATE_STRING, 7, 4)) -
                    V_MIN_YEAR
                   )
               ) +
               TO_NUMBER (SUBSTR (V_AFT_ACT_ENDDATE_STRING, 1, 2));
         END IF;
         --p('V_RELATIVE_INTERVAL 1: ' ||V_RELATIVE_INTERVAL);
         IF (P_REPORT_TYPE = 'Q') THEN
            V_RELATIVE_INTERVAL :=
               (4 *
                   (TO_NUMBER (SUBSTR (V_AFT_ACT_ENDDATE_STRING, 7, 4)) -
                    V_MIN_YEAR
                   )
               ) +
               (TO_NUMBER (
                   SUBSTR (
                      TO_CHAR (
                         (TO_NUMBER (SUBSTR (V_AFT_ACT_ENDDATE_STRING, 1, 2)) -
                          1
                         ) /
                            3,
                         '0.0'
                      ),
                      2,
                      1
                   )
                ) +
                1
               );
         END IF;
         IF (P_REPORT_TYPE = 'Y') THEN
            V_RELATIVE_INTERVAL :=
               TO_NUMBER (SUBSTR (V_AFT_ACT_ENDDATE_STRING, 7, 4)) -
               V_MIN_YEAR +
               1;
         END IF;
         FOR I IN 1 .. (J - (V_RELATIVE_INTERVAL + 1))
         LOOP
            FOR K IN (V_RELATIVE_INTERVAL + 1) .. (J - 1)
            LOOP
               V_INTERVAL_ARRAY (K) := V_INTERVAL_ARRAY (K + 1);
               V_VISIT_CNT_ARRAY (K) := V_VISIT_CNT_ARRAY (K + 1);
               V_EVENT_CNT_ARRAY (K) := V_EVENT_CNT_ARRAY (K + 1);
               V_PATIENT_CNT_ARRAY (K) := V_PATIENT_CNT_ARRAY (K + 1);
               V_VISIT_AMT_ARRAY (K) := V_VISIT_AMT_ARRAY (K + 1);
               V_EVENT_AMT_ARRAY (K) := V_EVENT_AMT_ARRAY (K + 1);
               V_PATIENT_AMT_ARRAY (K) := V_PATIENT_AMT_ARRAY (K + 1);
               V_TOTAL_AMT_ARRAY (K) := V_TOTAL_AMT_ARRAY (K + 1);
               V_PYMT_AMT_ARRAY (K) := V_PYMT_AMT_ARRAY (K + 1);
               V_PYMT_DISCREPANCY_ARRAY (K) := V_PYMT_DISCREPANCY_ARRAY (K + 1);
            END LOOP;
         END LOOP;
         V_INTERVAL_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_VISIT_CNT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_EVENT_CNT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_PATIENT_CNT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_VISIT_AMT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_EVENT_AMT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_PATIENT_AMT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_TOTAL_AMT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_PYMT_AMT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_PYMT_DISCREPANCY_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_TOTAL_INTERVALS :=
            V_TOTAL_INTERVALS - (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         J := V_TOTAL_INTERVALS + 1;
         IF (P_REPORT_TYPE = 'M') THEN
            V_RELATIVE_INTERVAL :=
               (12 *
                   (V_MIN_YEAR -
                    TO_NUMBER (SUBSTR (V_AFT_ACT_STDATE_STRING, 7, 4))
                   )
               ) +
               TO_NUMBER (SUBSTR (V_AFT_ACT_STDATE_STRING, 1, 2));
         END IF;
         IF (P_REPORT_TYPE = 'Q') THEN
            V_RELATIVE_INTERVAL :=
               (4 *
                   (V_MIN_YEAR -
                    TO_NUMBER (
                       SUBSTR (
                          V_AFT_ACT_STDATE_STRING,
                          7,
                          4
                       )
                    )
                   )
               ) +
               (TO_NUMBER (
                   SUBSTR (
                      TO_CHAR (
                         (TO_NUMBER (
                             SUBSTR (
                                V_AFT_ACT_STDATE_STRING,
                                1,
                                2
                             )
                          ) -
                          1
                         ) /
                            3,
                         '0.0'
                      ),
                      2,
                      1
                   )
                ) +
                1
               );
         END IF;
         IF (P_REPORT_TYPE = 'Y') THEN
            V_RELATIVE_INTERVAL :=
               V_MIN_YEAR -
               TO_NUMBER (SUBSTR (V_AFT_ACT_STDATE_STRING, 7, 4)) +
               1;
         END IF;
         FOR I IN 1 .. (V_RELATIVE_INTERVAL - 1)
         LOOP
            FOR K IN 1 .. (J - 1)
            LOOP
               V_INTERVAL_ARRAY (K) := V_INTERVAL_ARRAY (K + 1);
               V_VISIT_CNT_ARRAY (K) := V_VISIT_CNT_ARRAY (K + 1);
               V_EVENT_CNT_ARRAY (K) := V_EVENT_CNT_ARRAY (K + 1);
               V_PATIENT_CNT_ARRAY (K) := V_PATIENT_CNT_ARRAY (K + 1);
               V_VISIT_AMT_ARRAY (K) := V_VISIT_AMT_ARRAY (K + 1);
               V_EVENT_AMT_ARRAY (K) := V_EVENT_AMT_ARRAY (K + 1);
               V_PATIENT_AMT_ARRAY (K) := V_PATIENT_AMT_ARRAY (K + 1);
               V_TOTAL_AMT_ARRAY (K) := V_TOTAL_AMT_ARRAY (K + 1);
               V_PYMT_AMT_ARRAY (K) := V_PYMT_AMT_ARRAY (K + 1);
               V_PYMT_DISCREPANCY_ARRAY (K) := V_PYMT_DISCREPANCY_ARRAY (K + 1);
            END LOOP;
         END LOOP;
         V_INTERVAL_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_VISIT_CNT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_EVENT_CNT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_PATIENT_CNT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_VISIT_AMT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_EVENT_AMT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_PATIENT_AMT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_TOTAL_AMT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_PYMT_AMT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_PYMT_DISCREPANCY_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_TOTAL_INTERVALS := V_TOTAL_INTERVALS - (V_RELATIVE_INTERVAL - 1);
         J := J - (V_RELATIVE_INTERVAL - 1);

      SP_PYMTS_BEFORE_STDY_ACT (
         P_STUDY,
         V_BEF_ACT_STDATE_STRING,
         V_BEF_ACT_ENDDATE_STRING,
         P_REPORT_TYPE,
         TEMP_CLOB
      );
      DBMS_LOB.COPY (
         O_XML_CLOB,
         TEMP_CLOB,
         DBMS_LOB.GETLENGTH (TEMP_CLOB),
         DBMS_LOB.GETLENGTH (O_XML_CLOB) + 1,
         1
      );

      V_TEMPVAR := '<INTERVALS>';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
         FOR I IN 1 .. V_TOTAL_INTERVALS
         LOOP
            V_TEMPVAR := '<ROW num="' ||
                         I ||
                         '"><INTERVAL>' ||
                         V_INTERVAL_ARRAY (
                            I
                         ) ||
                         '</INTERVAL><VCNT>' ||
                         V_VISIT_CNT_ARRAY (
                            I
                         ) ||
                         '</VCNT><ECNT>' ||
                         V_EVENT_CNT_ARRAY (
                            I
                         ) ||
                         '</ECNT><PCNT>' ||
                         V_PATIENT_CNT_ARRAY (
                            I
                         ) ||
                         '</PCNT><VAMT>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_VISIT_AMT_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</VAMT><EAMT>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_EVENT_AMT_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</EAMT><PAMT>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_PATIENT_AMT_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</PAMT><TOTAMT>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_TOTAL_AMT_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</TOTAMT><PYMTAMT>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_PYMT_AMT_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</PYMTAMT><DISCREPANCY>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_PYMT_DISCREPANCY_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</DISCREPANCY></ROW>';
            DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
         END LOOP;
         V_TEMPVAR := '</INTERVALS>';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
         V_TEMPVAR := '<TOTAL><INTERVAL>TOTAL</INTERVAL><VCNT>' ||
                      V_VISIT_CNT_ARRAY (V_TOTAL_INTERVALS + 1) ||
                      '</VCNT><ECNT>' ||
                      V_EVENT_CNT_ARRAY (V_TOTAL_INTERVALS + 1) ||
                      '</ECNT><PCNT>' ||
                      V_PATIENT_CNT_ARRAY (V_TOTAL_INTERVALS + 1) ||
                      '</PCNT><VAMT>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_VISIT_AMT_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</VAMT><EAMT>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_EVENT_AMT_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</EAMT><PAMT>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_PATIENT_AMT_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</PAMT><TOTAMT>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_TOTAL_AMT_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</TOTAMT><PYMTAMT>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_PYMT_AMT_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</PYMTAMT><DISCREPANCY>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_PYMT_DISCREPANCY_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</DISCREPANCY></TOTAL>';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
        V_TEMPVAR := '</ORG>';
        DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);

      END LOOP; --organization loopend
    CLOSE v_cur;
   END IF;

      V_TEMPVAR := '</ROWSET>';
      DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
      DBMS_LOB.CLOSE (O_XML_CLOB);


/*
TEST
set serveroutpur on
declare
a clob;
b varchar2(2000);
c number := 0;
begin
PKG_MILESTONE_REPORTS.SP_MS_FINANCIAL(4045,'07/01/2002',
'07/31/2002','M',a);
c := dbms_lob.getlength(a);
dbms_output.put_line(c) ;
DBMS_LOB.CREATETEMPORARY (a, FALSE);
dbms_output.put_line('1') ;
DBMS_LOB.OPEN (a, DBMS_LOB.LOB_READWRITE);
dbms_output.put_line('2') ;
DBMS_LOB.read(a,c,1,b);
dbms_output.put_line('3') ;
DBMS_LOB.CLOSE(a);
dbms_output.put_line('4') ;
dbms_output.put_line(b) ;
Exception when no_data_found then
dbms_output.put_line('EXCEPTION' || c) ;
end ;
**************************************/
   END SP_MS_FINANCIAL;

----------------------------------------------------------------------------------------------------------

   PROCEDURE SP_MS_SUMMARY_STUDY (
      P_STUDY            IN       NUMBER,
	 P_ORG_ID           IN       NUMBER,
      P_STDATE_STRING    IN       VARCHAR2,
      P_ENDDATE_STRING   IN       VARCHAR2,
      O_XML_CLOB         OUT      CLOB
   )
   AS
/*Author: Sajal
  Date: 15th July 2002
  Purpose - Used for Milestones by Quarter report
*/
      V_ACHIVEMENT_DATE_ARRAY          TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_AMOUNT_ARRAY         TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_AMOUNT_TOTAL_ARRAY   TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_TYPE_ARRAY           TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_DESC_ARRAY           TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_RULE_ARRAY           TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_COUNT                          NUMBER                   := 0;
      V_TEMPVAR                        VARCHAR2 (32767);
      V_STDATE_STRING                  VARCHAR2 (20);
      V_ENDDATE_STRING                 VARCHAR2 (20);
--      V_STUDY_NUMBER                   VARCHAR2 (50);
	        V_STUDY_NUMBER                   VARCHAR2 (100);--JM
      V_STUDY_VERSION                  VARCHAR2 (50);
      V_STUDY_TITLE                    VARCHAR2 (2000);
      V_STUDY_ACT_DATE                 VARCHAR2 (20);
      V_STUDY_CURRENCY                 VARCHAR2 (20);
      I                                NUMBER                   := 1;
      F                                NUMBER                  ;
	 U                                NUMBER;
	 J                                NUMBER;
	 K                                NUMBER;
      V_MS_DESC                        VARCHAR2 (1000);
      V_MS_RULE                        VARCHAR2 (1000);
      V_MS_TYPE                        VARCHAR2 (1000);
      V_PM_TOT_AMT                     NUMBER                   := 0;
      V_VM_TOT_AMT                     NUMBER                   := 0;
      V_EM_TOT_AMT                     NUMBER                   := 0;
      V_MS_TOT_AMT                     NUMBER                   := 0;
      V_ACT_DATE_STRING                VARCHAR2 (20);
      V_BEF_ACT_STDATE_STRING          VARCHAR2 (20);
      V_BEF_ACT_ENDDATE_STRING         VARCHAR2 (20);
      V_AFT_ACT_STDATE_STRING          VARCHAR2 (20);
      V_AFT_ACT_ENDDATE_STRING         VARCHAR2 (20);
	 V_ORG_NAME                       VARCHAR2 (50);
	 V_ORG_ID                         NUMBER;
	 V_OLD_ORG_ID                         NUMBER;
      V_CUR   TYPES.cursorType;
  BEGIN
  	   		  BEGIN
      SP_FIND_STUDY_ACT_DATES (
         P_STUDY,
         P_STDATE_STRING,
         P_ENDDATE_STRING,
         V_ACT_DATE_STRING,
         V_BEF_ACT_STDATE_STRING,
         V_BEF_ACT_ENDDATE_STRING,
         V_AFT_ACT_STDATE_STRING,
         V_AFT_ACT_ENDDATE_STRING
      );
      V_MS_DESC := ' ';
      V_MS_RULE := ' ';
      V_MS_TYPE := ' ';

      SELECT STUDY_NUMBER, STUDY_VER_NO, STUDY_TITLE
        INTO V_STUDY_NUMBER, V_STUDY_VERSION, V_STUDY_TITLE
        FROM ER_STUDY
       WHERE PK_STUDY = P_STUDY;
      SELECT CODELST_SUBTYP
        INTO V_STUDY_CURRENCY
        FROM SCH_CODELST, ER_STUDY
       WHERE FK_CODELST_CURRENCY = PK_CODELST
         AND PK_STUDY = P_STUDY;

      DBMS_LOB.CREATETEMPORARY (O_XML_CLOB, FALSE);
      DBMS_LOB.OPEN (O_XML_CLOB, DBMS_LOB.LOB_READWRITE);
      V_TEMPVAR := '<?xml version="1.0"?><ROWSET>';
      DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);

      V_TEMPVAR := '<STUDY><STUDYNUM>' ||
                   V_STUDY_NUMBER ||
                   '</STUDYNUM><STUDYVER>' ||
                   V_STUDY_VERSION ||
                   '</STUDYVER><STUDYTITLE>' ||
                   V_STUDY_TITLE ||
                   '</STUDYTITLE><ACTIVATIONDATE>' ||
                   V_ACT_DATE_STRING ||
                   '</ACTIVATIONDATE><STUDYCURRENCY>' ||
                   V_STUDY_CURRENCY ||
                   '</STUDYCURRENCY></STUDY>';
      DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);

      IF (V_AFT_ACT_STDATE_STRING <> ' ') THEN


      --organization loop, bring data for each organization
      IF p_org_id > 0 THEN
        OPEN v_cur FOR SELECT DISTINCT fk_site, site_name
                 FROM ER_PATPROT, ER_SITE, ER_PER
                 WHERE fk_study=p_study
                 AND pk_site=fk_site
                 AND pk_per=fk_per
			  AND pk_site = p_org_id;
	 ELSE
        OPEN v_cur FOR SELECT DISTINCT fk_site, site_name
                 FROM ER_PATPROT, ER_SITE, ER_PER
                 WHERE fk_study=p_study
                 AND pk_site=fk_site
                 AND pk_per=fk_per ;
      END IF ;
      LOOP

	 FETCH v_cur INTO v_org_id, v_org_name ;
	 EXIT WHEN v_cur%NOTFOUND;

      V_ACHIVEMENT_DATE_ARRAY  := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_AMOUNT_ARRAY  := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_AMOUNT_TOTAL_ARRAY  := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_TYPE_ARRAY   := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_DESC_ARRAY  := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_RULE_ARRAY  := TYPES.SMALL_STRING_ARRAY ();
      V_COUNT                  := 0;
      i:=1;

	 --reset the totals for each organization
      V_PM_TOT_AMT := 0;
      V_EM_TOT_AMT := 0;
      V_VM_TOT_AMT := 0;
      V_MS_TOT_AMT := 0;

 	    V_TEMPVAR := '<ORG name="'||V_ORG_NAME||'">';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);


         SP_MILESTONE_STUDY_SPECIFIC (
            P_STUDY,
		  V_ORG_ID,
            V_AFT_ACT_STDATE_STRING,
            V_AFT_ACT_ENDDATE_STRING,
            V_COUNT,
            V_ACHIVEMENT_DATE_ARRAY,
            V_MILESTONE_AMOUNT_ARRAY,
            V_MILESTONE_TYPE_ARRAY,
            V_MILESTONE_DESC_ARRAY,
            V_MILESTONE_RULE_ARRAY
         );


         FOR K IN 1 .. V_COUNT
         LOOP
            V_MILESTONE_AMOUNT_TOTAL_ARRAY.EXTEND;
         END LOOP;
         WHILE I <= V_COUNT
         LOOP
            IF (   V_MS_DESC <> V_MILESTONE_DESC_ARRAY (I)
                OR V_MS_RULE <> V_MILESTONE_RULE_ARRAY (I)
                OR V_MS_TYPE <> V_MILESTONE_TYPE_ARRAY (I)
			 OR V_ORG_ID <> V_OLD_ORG_ID ) THEN

               V_MS_DESC := V_MILESTONE_DESC_ARRAY (I);
               V_MS_RULE := V_MILESTONE_RULE_ARRAY (I);
               V_MS_TYPE := V_MILESTONE_TYPE_ARRAY (I);
			V_OLD_ORG_ID := V_ORG_ID;

		--	p('V_MS_TYPE ' ||V_MS_TYPE);
               IF (V_MS_TYPE <> 'PM') THEN
                  V_ACHIVEMENT_DATE_ARRAY (I) := '1';
               END IF;
               I := I + 1;
            ELSE
               IF (V_MS_TYPE <> 'PM') THEN
                  V_ACHIVEMENT_DATE_ARRAY (I - 1) :=
                     TO_CHAR (TO_NUMBER (V_ACHIVEMENT_DATE_ARRAY (I - 1)) + 1);
               END IF;
               FOR J IN I .. (V_COUNT - 1)
               LOOP
                  V_ACHIVEMENT_DATE_ARRAY (J) :=
                     V_ACHIVEMENT_DATE_ARRAY (J + 1);
                  V_MILESTONE_AMOUNT_ARRAY (J) :=
                     V_MILESTONE_AMOUNT_ARRAY (J + 1);
                  V_MILESTONE_TYPE_ARRAY (J) := V_MILESTONE_TYPE_ARRAY (J + 1);
                  V_MILESTONE_DESC_ARRAY (J) := V_MILESTONE_DESC_ARRAY (J + 1);
                  V_MILESTONE_RULE_ARRAY (J) := V_MILESTONE_RULE_ARRAY (J + 1);
                  V_MILESTONE_AMOUNT_TOTAL_ARRAY (J) :=
                     V_MILESTONE_AMOUNT_TOTAL_ARRAY (J + 1);
               END LOOP;
               V_ACHIVEMENT_DATE_ARRAY.TRIM;
               V_MILESTONE_AMOUNT_ARRAY.TRIM;
               V_MILESTONE_TYPE_ARRAY.TRIM;
               V_MILESTONE_DESC_ARRAY.TRIM;
               V_MILESTONE_RULE_ARRAY.TRIM;
               V_MILESTONE_AMOUNT_TOTAL_ARRAY.TRIM;
               V_COUNT := V_COUNT - 1;
            END IF;
         END LOOP;
         FOR I IN 1 .. V_COUNT
         LOOP
            IF (V_MILESTONE_TYPE_ARRAY (I) = 'PM') THEN
               V_MILESTONE_AMOUNT_TOTAL_ARRAY (I) :=
                  V_MILESTONE_AMOUNT_ARRAY (I);
               V_PM_TOT_AMT :=
                  V_PM_TOT_AMT + TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
            END IF;
            IF (V_MILESTONE_TYPE_ARRAY (I) = 'EM') THEN
               V_MILESTONE_AMOUNT_TOTAL_ARRAY (I) :=
                  TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I)) *
                     TO_NUMBER (V_ACHIVEMENT_DATE_ARRAY (I));
               V_EM_TOT_AMT :=
                  V_EM_TOT_AMT +
                  (TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I)) *
                      TO_NUMBER (V_ACHIVEMENT_DATE_ARRAY (I))
                  );
            END IF;
            IF (V_MILESTONE_TYPE_ARRAY (I) = 'VM') THEN
		 -- p('V_ACHIVEMENT_DATE_ARRAY ' || V_ACHIVEMENT_DATE_ARRAY (I));
               V_MILESTONE_AMOUNT_TOTAL_ARRAY (I) :=
                  TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I)) *
                     TO_NUMBER (V_ACHIVEMENT_DATE_ARRAY (I));
               V_VM_TOT_AMT :=
                  V_VM_TOT_AMT +
                  (TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I)) *
                      TO_NUMBER (V_ACHIVEMENT_DATE_ARRAY (I))
                  );
            END IF;
            IF (V_MILESTONE_AMOUNT_ARRAY (I) = '0') THEN
               V_MILESTONE_AMOUNT_TOTAL_ARRAY (I) := 'No associated amount';
               V_MILESTONE_AMOUNT_ARRAY (I) := 'No associated amount';
            ELSE
               V_MILESTONE_AMOUNT_TOTAL_ARRAY (I) :=
                  V_STUDY_CURRENCY ||
                  TO_CHAR (
                     TO_NUMBER (V_MILESTONE_AMOUNT_TOTAL_ARRAY (I)),
                     '9999999990.00'
                  );
               V_MILESTONE_AMOUNT_ARRAY (I) :=
                  V_STUDY_CURRENCY ||
                  TO_CHAR (
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I)),
                     '9999999990.00'
                  );
            END IF;
         END LOOP;
         V_MS_TOT_AMT := V_PM_TOT_AMT + V_VM_TOT_AMT + V_EM_TOT_AMT;

         V_TEMPVAR := '<PM>';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);

         FOR U IN 1 .. V_COUNT
         LOOP

            IF (V_MILESTONE_TYPE_ARRAY (U) = 'PM') THEN

               V_TEMPVAR := '<ROW num="' ||
                            U ||
                            '"><DESC>' ||
                            V_MILESTONE_DESC_ARRAY (U) ||
                            '</DESC><RULE>' ||
                            V_MILESTONE_RULE_ARRAY (U) ||
                            '</RULE><ACHIEVEMENT>' ||
                            V_ACHIVEMENT_DATE_ARRAY (U) ||
                            '</ACHIEVEMENT><AMOUNT>' ||
                            V_MILESTONE_AMOUNT_ARRAY (U) ||
                            '</AMOUNT><MSTOTAL>' ||
                            V_MILESTONE_AMOUNT_TOTAL_ARRAY (U) ||
                            '</MSTOTAL></ROW>';
               DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
            END IF;
         END LOOP;
         V_TEMPVAR := '</PM><EM>';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
         FOR U IN 1 .. V_COUNT
         LOOP
            IF (V_MILESTONE_TYPE_ARRAY (U) = 'EM') THEN
               V_TEMPVAR := '<ROW num="' ||
                            U ||
                            '"><DESC>' ||
                            V_MILESTONE_DESC_ARRAY (U) ||
                            '</DESC><RULE>' ||
                            V_MILESTONE_RULE_ARRAY (U) ||
                            '</RULE><ACHIEVEMENT>' ||
                            V_ACHIVEMENT_DATE_ARRAY (U) ||
                            '</ACHIEVEMENT><AMOUNT>' ||
                            V_MILESTONE_AMOUNT_ARRAY (U) ||
                            '</AMOUNT><MSTOTAL>' ||
                            V_MILESTONE_AMOUNT_TOTAL_ARRAY (U) ||
                            '</MSTOTAL></ROW>';
               DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
            END IF;
         END LOOP;
         V_TEMPVAR := '</EM><VM>';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
         FOR U IN 1 .. V_COUNT
         LOOP
            IF (V_MILESTONE_TYPE_ARRAY (U) = 'VM') THEN
               V_TEMPVAR := '<ROW num="' ||
                            I ||
                            '"><DESC>' ||
                            V_MILESTONE_DESC_ARRAY (U) ||
                            '</DESC><RULE>' ||
                            V_MILESTONE_RULE_ARRAY (U) ||
                            '</RULE><ACHIEVEMENT>' ||
                            V_ACHIVEMENT_DATE_ARRAY (U) ||
                            '</ACHIEVEMENT><AMOUNT>' ||
                            V_MILESTONE_AMOUNT_ARRAY (U) ||
                            '</AMOUNT><MSTOTAL>' ||
                            V_MILESTONE_AMOUNT_TOTAL_ARRAY (U) ||
                            '</MSTOTAL></ROW>';
               DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
            END IF;
         END LOOP;
         V_TEMPVAR := '</VM><TOTAL><PM>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (V_PM_TOT_AMT, '9999999990.00') ||
                      '</PM><EM>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (V_EM_TOT_AMT, '9999999990.00') ||
                      '</EM><VM>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (V_VM_TOT_AMT, '9999999990.00') ||
                      '</VM><GRAND>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (V_MS_TOT_AMT, '9999999990.00') ||
                      '</GRAND>' ||
                      '</TOTAL>';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);


        V_TEMPVAR := '</ORG>';
        DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);


        END LOOP; --organization loopend
      CLOSE v_cur;
      END IF;
      V_TEMPVAR := '</ROWSET>';
      DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
      DBMS_LOB.CLOSE (O_XML_CLOB);
	  EXCEPTION WHEN OTHERS THEN
	   RAISE;
	  END;
/*
TEST
set serveroutput on
declare
a clob;
b varchar2(2000);
c number := 0;
begin
PKG_MILESTONE_REPORTS.SP_MS_SUMMARY_STUDY(4045,'07/01/2002',
'07/31/2002',a);
c := dbms_lob.getlength(a);
dbms_output.put_line(c) ;
DBMS_LOB.CREATETEMPORARY (a, FALSE);
dbms_output.put_line('1') ;
DBMS_LOB.OPEN (a, DBMS_LOB.LOB_READWRITE);
dbms_output.put_line('2') ;
DBMS_LOB.read(a,c,1,b);
dbms_output.put_line('3') ;
DBMS_LOB.CLOSE(a);
dbms_output.put_line('4') ;
dbms_output.put_line(b) ;
Exception when no_data_found then
dbms_output.put_line('EXCEPTION' || c) ;
end ;
**************************************/
   END SP_MS_SUMMARY_STUDY;

   PROCEDURE SP_MS_SUMMARY_PATIENT (
      P_STUDY            IN       NUMBER,
      P_PATIENT          IN       NUMBER,
      P_STDATE_STRING    IN       VARCHAR2,
      P_ENDDATE_STRING   IN       VARCHAR2,
 	  P_ORG_ID           IN       NUMBER,
      O_XML_CLOB         OUT      CLOB
   )
   AS
      /* Date : 17th Jul 2002
         Author: Sajal
         Purpose - This is for the Milestone Summary (Patient) Report
      */
--      V_STUDY_NUMBER             VARCHAR2 (50);
	        V_STUDY_NUMBER             VARCHAR2 (100);--JM:
      V_STUDY_VERSION            VARCHAR2 (50);
      V_STUDY_TITLE              VARCHAR2 (2000);
      V_STUDY_ACT_DATE           VARCHAR2 (20);
      V_STUDY_CURRENCY           VARCHAR2 (20);
      V_TEMPVAR                  VARCHAR2 (2000);
      V_STDATE_STRING            VARCHAR2 (20);
      V_ENDDATE_STRING           VARCHAR2 (20);
      V_PATPROT                  NUMBER;
      V_PATIENT                  NUMBER;

	 V_ORGNAME                  VARCHAR2(50);
	 V_ORGID                    NUMBER;
      V_PROTOCOL                 NUMBER                   := 0;
      V_VM_FK_MS                 NUMBER                   := 0;
      V_VM_CODE                  CHAR (15);
      V_VM_VISIT                 NUMBER                   := 0;
      V_VM_CALENDAR              NUMBER                   := 0;
      V_VM_DESC                  VARCHAR2 (200);
      V_VM_RULE                  VARCHAR2 (200);
      V_EM_FK_MS                 NUMBER                   := 0;
      V_EM_CODE                  CHAR (15);
      V_EM_VISIT                 NUMBER                   := 0;
      V_EM_CALENDAR              NUMBER                   := 0;
      V_EM_EVENT                 NUMBER                   := 0;
      V_EM_DESC                  VARCHAR2 (200);
      V_EM_RULE                  VARCHAR2 (200);
      V_VM_PKMS                  TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_VM_RULECODE              TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_VM_RULEVISIT             TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_VM_RULEUSERS             TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_VM_EVENT                 TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_VM_CAL                   TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_VM_RULEDESC              TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_VM_RULEAMT               TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_VM_EVENTDESC             TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_EM_PKMS                  TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_EM_RULECODE              TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_EM_RULEVISIT             TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_EM_RULEUSERS             TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_EM_RULEEVENT             TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_EM_CAL                   TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_EM_RULEDESC              TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_EM_RULEAMT               TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_EM_EVENTDESC             TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_STDATE                   DATE;
      V_ENDDATE                  DATE;
      V_MILESTONEFLAG            NUMBER                   := 0;
      V_ACHIEVEMENT_DATE         DATE;
      J                          NUMBER;
      K                          NUMBER;
	 M                          NUMBER;
      V_PATCODE                  VARCHAR2 (20);
      V_CAL_TITLE                VARCHAR2 (100);
      V_PROT_ST_DT               DATE;
      V_PROT_ENROL_DT            DATE;
      V_COUNT                    NUMBER                   := 0;
      V_AMOUNT                   VARCHAR2 (20);
      V_PAT_CURSOR               TYPES.CURSORTYPE;
      V_DYNAMIC_SQL              VARCHAR2 (1000);
	 V_ORDERBY                  VARCHAR2 (100);
      V_ACT_DATE                 DATE;
      V_PATIENT_ID               TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();

      V_ORG_NAME               TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_ORG_ID               TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_CALENDAR_TITLE           TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_PATPROTS           TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_PROT_START_DATE          TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_PROT_ENROL_DATE          TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MS_DESC                  TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MS_RULE                  TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_SCH_DATE                 TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MS_ACHIEVEMENT           TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MS_ACHIEVEMENT_DATE      TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MS_AMT                   TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_NO_OF_MS                 TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_NO_OF_MS_ACHIEVED        TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_TOTAL_AMT                TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_ACT_DATE_STRING          VARCHAR2 (20);
      V_BEF_ACT_STDATE_STRING    VARCHAR2 (20);
      V_BEF_ACT_ENDDATE_STRING   VARCHAR2 (20);
      V_AFT_ACT_STDATE_STRING    VARCHAR2 (20);
      V_AFT_ACT_ENDDATE_STRING   VARCHAR2 (20);
   BEGIN
      SP_FIND_STUDY_ACT_DATES (
         P_STUDY,
         P_STDATE_STRING,
         P_ENDDATE_STRING,
         V_ACT_DATE_STRING,
         V_BEF_ACT_STDATE_STRING,
         V_BEF_ACT_ENDDATE_STRING,
         V_AFT_ACT_STDATE_STRING,
         V_AFT_ACT_ENDDATE_STRING
      );
      /* get studies for which milestones are defined */
      SELECT STUDY_NUMBER, STUDY_VER_NO, STUDY_TITLE
        INTO V_STUDY_NUMBER, V_STUDY_VERSION, V_STUDY_TITLE
        FROM ER_STUDY
       WHERE PK_STUDY = P_STUDY;
      SELECT CODELST_SUBTYP
        INTO V_STUDY_CURRENCY
        FROM SCH_CODELST, ER_STUDY
       WHERE FK_CODELST_CURRENCY = PK_CODELST
         AND PK_STUDY = P_STUDY;

      IF (V_AFT_ACT_STDATE_STRING <> ' ') THEN
         V_STDATE := TO_DATE (V_AFT_ACT_STDATE_STRING, PKG_DATEUTIL.f_get_dateformat);
         V_ENDDATE := TO_DATE (V_AFT_ACT_ENDDATE_STRING, PKG_DATEUTIL.f_get_dateformat);
         /*get study number*/
         /*get all Visit Milestones for this study*/
         SP_STUDY_MILESTONE_VMEM (
            P_STUDY,
            'VM',
            V_VM_PKMS,
            V_VM_RULECODE,
            V_VM_RULEVISIT,
            V_VM_EVENT,
            V_VM_RULEUSERS,
            V_VM_CAL,
            V_VM_RULEDESC,
            V_VM_RULEAMT,
            V_VM_EVENTDESC
         );
         /*get all Event Milestones for this study*/
         SP_STUDY_MILESTONE_VMEM (
            P_STUDY,
            'EM',
            V_EM_PKMS,
            V_EM_RULECODE,
            V_EM_RULEVISIT,
            V_EM_RULEEVENT,
            V_EM_RULEUSERS,
            V_EM_CAL,
            V_EM_RULEDESC,
            V_EM_RULEAMT,
            V_EM_EVENTDESC
         );

         IF (P_PATIENT = 0) THEN
            V_DYNAMIC_SQL :=
               'select PK_PATPROT, FK_PER, ER_PER.FK_SITE, S.SITE_NAME, FK_PROTOCOL, PER_CODE, NAME, PATPROT_START, PATPROT_ENROLDT from ER_PATPROT, ER_PER, event_assoc, ER_SITE S where FK_STUDY = ' ||
               P_STUDY || ' and PK_PER = FK_PER and FK_PROTOCOL = EVENT_ID AND ER_PER.FK_SITE = S.PK_SITE ' ;
		  V_ORDERBY := ' order by fk_per, PK_PATPROT ';
         ELSE
            V_DYNAMIC_SQL :=
               'select PK_PATPROT, FK_PER, ER_PER.FK_SITE, S.SITE_NAME,  FK_PROTOCOL, PER_CODE, NAME, PATPROT_START, PATPROT_ENROLDT from ER_PATPROT, ER_PER, event_assoc, ER_SITE S where FK_STUDY = ' ||
               P_STUDY ||
               ' and PK_PER = FK_PER and pk_per = ' ||
               P_PATIENT ||
               ' and FK_PROTOCOL = EVENT_ID AND ER_PER.FK_SITE = S.PK_SITE ' ;
         END IF;
	    IF (P_ORG_ID > 0) THEN
            V_DYNAMIC_SQL := V_DYNAMIC_SQL || 'AND S.PK_SITE=' || P_ORG_ID;
	    END IF;
	     V_DYNAMIC_SQL :=  V_DYNAMIC_SQL || V_ORDERBY;

         EXECUTE IMMEDIATE V_DYNAMIC_SQL;
         OPEN V_PAT_CURSOR FOR
            V_DYNAMIC_SQL;
         LOOP
            FETCH V_PAT_CURSOR INTO V_PATPROT,
                                    V_PATIENT,
							 V_ORGID,
							 V_ORGNAME,
                                    V_PROTOCOL,
                                    V_PATCODE,
                                    V_CAL_TITLE,
                                    V_PROT_ST_DT,
                                    V_PROT_ENROL_DT;
            EXIT WHEN V_PAT_CURSOR%NOTFOUND;
            FOR S IN 1 .. V_VM_PKMS.COUNT   --for all visit milestone in study, check for this enrollment
            LOOP   --loop 3, all visit milestones
               V_VM_FK_MS := TO_NUMBER (trim (V_VM_PKMS (S)));
               V_VM_CODE := trim (V_VM_RULECODE (S));
               V_VM_VISIT := TO_NUMBER (trim (V_VM_RULEVISIT (S)));
--p('V_VM_CODE' || V_VM_CODE);
--p('V_PROTOCOL' || V_PROTOCOL);
               V_VM_CALENDAR := TO_NUMBER (trim (V_VM_CAL (S)));
--p('V_VM_CALENDAR' || V_VM_CALENDAR);
               V_VM_RULE := trim (V_VM_RULEDESC (S));
               V_MILESTONEFLAG := 0;
               V_ACHIEVEMENT_DATE := NULL;
               V_AMOUNT := V_VM_RULEAMT (S);
               V_VM_DESC := 'Visit ' || V_VM_VISIT;
               IF (V_PROTOCOL = V_VM_CALENDAR) THEN   -- if milestone calendar equals patprot calendar
                  -- get act schedule date
                  SELECT MIN (ACTUAL_SCHDATE)
                    INTO V_ACT_DATE
                    FROM SCH_EVENTS1
                   WHERE FK_PATPROT = V_PATPROT
                     AND VISIT = V_VM_VISIT;
--p('V_ACT_DATE  ' || V_ENDDATE);
                  IF (trim (V_VM_CODE) = 'vm_4') THEN   -- VM_4 All visit associated CRFs marked 'completed'
                     Pkg_Vm_Rule.SP_VIS_RULE_CRFS (
                        V_PATPROT,
                        V_PATIENT,
                        V_VM_VISIT,
                        'completed',
                        V_STDATE,
                        V_ENDDATE,
                        V_MILESTONEFLAG,
                        V_ACHIEVEMENT_DATE
                     );
                  END IF;
                  IF (trim (V_VM_CODE) = 'vm_5') THEN   -- VM_5 All visit associated CRFs marked 'submitted'
                     Pkg_Vm_Rule.SP_VIS_RULE_CRFS (
                        V_PATPROT,
                        V_PATIENT,
                        V_VM_VISIT,
                        'submitted',
                        V_STDATE,
                        V_ENDDATE,
                        V_MILESTONEFLAG,
                        V_ACHIEVEMENT_DATE
                     );
                  END IF;
                  IF (trim (V_VM_CODE) = 'vm_2') THEN   -- VM_2 All events within the visit are marked as 'Done'
                     Pkg_Vm_Rule.SP_VM_PATPROT_EVENT_DONE_ALL (
                        V_PATPROT,
                        V_PROTOCOL,
                        V_VM_VISIT,
                        V_STDATE,
                        V_ENDDATE,
                        V_MILESTONEFLAG,
                        V_ACHIEVEMENT_DATE
                     );
                  END IF;
                  IF (trim (V_VM_CODE) = 'vm_3') THEN   -- VM_3 At least one event within the visit is marked as 'Done'
                     Pkg_Vm_Rule.SP_VM_PATPROT_EVENT_DONE_ONE (
                        V_PATPROT,
                        V_ORGID,
                        V_PROTOCOL,
                        V_VM_VISIT,
                        V_STDATE,
                        V_ENDDATE,
                        V_MILESTONEFLAG,
                        V_ACHIEVEMENT_DATE
                     );
                  END IF;

                  V_COUNT := V_COUNT + 1;
                  V_PATIENT_ID.EXTEND;

			   V_ORG_NAME.EXTEND;
			   V_ORG_ID.EXTEND;
                  V_CALENDAR_TITLE.EXTEND;
                  V_PATPROTS.EXTEND;
                  V_PROT_START_DATE.EXTEND;
                  V_PROT_ENROL_DATE.EXTEND;
                  V_MS_DESC.EXTEND;
                  V_MS_RULE.EXTEND;
                  V_SCH_DATE.EXTEND;
                  V_MS_ACHIEVEMENT.EXTEND;
                  V_MS_ACHIEVEMENT_DATE.EXTEND;
                  V_MS_AMT.EXTEND;
                  V_NO_OF_MS.EXTEND;
                  V_NO_OF_MS_ACHIEVED.EXTEND;
                  V_TOTAL_AMT.EXTEND;
                  V_PATIENT_ID (V_COUNT) := V_PATCODE;

			   V_ORG_NAME (V_COUNT) := V_ORGNAME;
			   V_ORG_ID (V_COUNT) := V_ORGID;
                  V_CALENDAR_TITLE (V_COUNT) := V_CAL_TITLE;
                  V_PATPROTS(V_COUNT) := V_PATPROT;
                  V_PROT_START_DATE (V_COUNT) :=
                     TO_CHAR (V_PROT_ST_DT, PKG_DATEUTIL.f_get_dateformat);
                  V_PROT_ENROL_DATE (V_COUNT) :=
                     TO_CHAR (V_PROT_ENROL_DT, PKG_DATEUTIL.f_get_dateformat);
                  V_MS_DESC (V_COUNT) := V_VM_DESC;
                  V_MS_RULE (V_COUNT) := V_VM_RULE;
                  V_SCH_DATE (V_COUNT) := TO_CHAR (V_ACT_DATE, PKG_DATEUTIL.f_get_dateformat);
                  IF V_MILESTONEFLAG >= 1 THEN   -- milestone is met
                     V_MS_ACHIEVEMENT (V_COUNT) := 'Yes';
                     V_NO_OF_MS_ACHIEVED (V_COUNT) := 1;
                     V_TOTAL_AMT (V_COUNT) := V_AMOUNT;
                  ELSE
                     V_MS_ACHIEVEMENT (V_COUNT) := 'No';
                     V_NO_OF_MS_ACHIEVED (V_COUNT) := 0;
                     V_TOTAL_AMT (V_COUNT) := 0;
                  END IF;
                  V_MS_ACHIEVEMENT_DATE (V_COUNT) :=
                     TO_CHAR (V_ACHIEVEMENT_DATE, PKG_DATEUTIL.f_get_dateformat);
                  V_MS_AMT (V_COUNT) := V_AMOUNT;
                  V_NO_OF_MS (V_COUNT) := 1;
               END IF;   -- if - for milestone calendar = patprot cal
            END LOOP;   --loop 3 all visit milestones
            FOR S IN 1 .. V_EM_PKMS.COUNT   --for all event milestones in study, check for this enrollment
            LOOP   --loop 4, all event  milestones
               V_EM_FK_MS := TO_NUMBER (trim (V_EM_PKMS (S)));
               V_EM_CODE := trim (V_EM_RULECODE (S));
               V_EM_VISIT := TO_NUMBER (trim (V_EM_RULEVISIT (S)));
               V_EM_CALENDAR := TO_NUMBER (trim (V_EM_CAL (S)));
               V_EM_EVENT := TO_NUMBER (trim (V_EM_RULEEVENT (S)));
               V_EM_RULE := trim (V_EM_RULEDESC (S));
               V_AMOUNT := V_EM_RULEAMT (S);
               V_EM_DESC := 'Event ' ||
                            trim (V_EM_EVENTDESC (S)) ||
                            ' in Visit ' ||
                            V_EM_VISIT;
               V_MILESTONEFLAG := 0;
               V_ACHIEVEMENT_DATE := NULL;
               IF (V_PROTOCOL = V_EM_CALENDAR) THEN   -- if milestone calendar equals patprot calendar
                  SELECT MIN (ACTUAL_SCHDATE)
                    INTO V_ACT_DATE
                    FROM SCH_EVENTS1
                   WHERE FK_PATPROT = V_PATPROT
                     AND VISIT = V_EM_VISIT
                     AND FK_ASSOC = V_EM_EVENT;
                  IF (trim (V_EM_CODE) = 'em_2') THEN
                     Pkg_Em_Rule.SP_EVENT_DONE (
                        V_EM_EVENT,
                        V_PATPROT,
                        V_STDATE,
                        V_ENDDATE,
                        V_MILESTONEFLAG,
                        V_ACHIEVEMENT_DATE
                     );
                  END IF;
                  --p('sp_eve_rule_crf V_ENDDATE ' ||V_ENDDATE);
                  IF (trim (V_EM_CODE) = 'em_3') THEN
                     Pkg_Em_Rule.SP_EVE_RULE_CRF (
                        V_EM_EVENT,
                        'completed',
                        V_PATPROT,
                        V_STDATE,
                        V_ENDDATE,
                        V_ACHIEVEMENT_DATE,
                        V_MILESTONEFLAG
                     );
                  END IF;
                  IF (trim (V_EM_CODE) = 'em_4') THEN
                     Pkg_Em_Rule.SP_EVE_RULE_CRF (
                        V_EM_EVENT,
                        'submitted',
                        V_PATPROT,
                        V_STDATE,
                        V_ENDDATE,
                        V_ACHIEVEMENT_DATE,
                        V_MILESTONEFLAG
                     );
                  END IF;
                  V_COUNT := V_COUNT + 1;
                  V_PATIENT_ID.EXTEND;

			   V_ORG_NAME.EXTEND;
			   V_ORG_ID.EXTEND;
                  V_CALENDAR_TITLE.EXTEND;
                  V_PATPROTS.EXTEND;
                  V_PROT_START_DATE.EXTEND;
                  V_PROT_ENROL_DATE.EXTEND;
                  V_MS_DESC.EXTEND;
                  V_MS_RULE.EXTEND;
                  V_SCH_DATE.EXTEND;
                  V_MS_ACHIEVEMENT.EXTEND;
                  V_MS_ACHIEVEMENT_DATE.EXTEND;
                  V_MS_AMT.EXTEND;
                  V_NO_OF_MS.EXTEND;
                  V_NO_OF_MS_ACHIEVED.EXTEND;
                  V_TOTAL_AMT.EXTEND;
                  V_PATIENT_ID (V_COUNT) := V_PATCODE;


			   V_ORG_NAME (V_COUNT) := V_ORGNAME;

			   V_ORG_ID (V_COUNT) := V_ORGID;
                  V_CALENDAR_TITLE (V_COUNT) := V_CAL_TITLE;
                  V_PATPROTS (V_COUNT) := V_PATPROT;
                  V_PROT_START_DATE (V_COUNT) :=
                     TO_CHAR (V_PROT_ST_DT, PKG_DATEUTIL.f_get_dateformat);
                  V_PROT_ENROL_DATE (V_COUNT) :=
                     TO_CHAR (V_PROT_ENROL_DT, PKG_DATEUTIL.f_get_dateformat);
                  V_MS_DESC (V_COUNT) := V_EM_DESC;
                  V_MS_RULE (V_COUNT) := V_EM_RULE;
                  V_SCH_DATE (V_COUNT) := TO_CHAR (V_ACT_DATE, PKG_DATEUTIL.f_get_dateformat);
                  IF V_MILESTONEFLAG >= 1 THEN   -- milestone is met
                     V_MS_ACHIEVEMENT (V_COUNT) := 'Yes';
                     V_NO_OF_MS_ACHIEVED (V_COUNT) := 1;
                     V_TOTAL_AMT (V_COUNT) := V_AMOUNT;
                  ELSE
                     V_MS_ACHIEVEMENT (V_COUNT) := 'No';
                     V_NO_OF_MS_ACHIEVED (V_COUNT) := 0;
                     V_TOTAL_AMT (V_COUNT) := 0;
                  END IF;
                  V_MS_ACHIEVEMENT_DATE (V_COUNT) :=
                     TO_CHAR (V_ACHIEVEMENT_DATE, PKG_DATEUTIL.f_get_dateformat);
                  V_MS_AMT (V_COUNT) := V_AMOUNT;
                  V_NO_OF_MS (V_COUNT) := 1;
               END IF;   -- if - for milestone calendar = patprot cal
            END LOOP;   --end of loop 4, all event  milestones
         END LOOP;
         CLOSE V_PAT_CURSOR;
         J := 1;
         FOR I IN 2 .. V_COUNT
         LOOP
            IF (    V_PATPROTS (J) = V_PATPROTS (I)) THEN
               V_NO_OF_MS (J) := V_NO_OF_MS (J) + V_NO_OF_MS (I);
               V_NO_OF_MS_ACHIEVED (J) :=
                  V_NO_OF_MS_ACHIEVED (J) + V_NO_OF_MS_ACHIEVED (I);
               V_TOTAL_AMT (J) := V_TOTAL_AMT (J) + V_TOTAL_AMT (I);
            ELSE
               J := I;
            END IF;
         END LOOP;
      END IF;
      DBMS_LOB.CREATETEMPORARY (O_XML_CLOB, FALSE);
      DBMS_LOB.OPEN (O_XML_CLOB, DBMS_LOB.LOB_READWRITE);
      V_TEMPVAR := '<?xml version="1.0"?><ROWSET>';
      DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
      V_TEMPVAR := '<STUDY><STUDYNUM>' ||
                   V_STUDY_NUMBER ||
                   '</STUDYNUM><STUDYVER>' ||
                   V_STUDY_VERSION ||
                   '</STUDYVER><STUDYTITLE>' ||
                   V_STUDY_TITLE ||
                   '</STUDYTITLE><ACTIVATIONDATE>' ||
                   V_ACT_DATE_STRING ||
                   '</ACTIVATIONDATE><STUDYCURRENCY>' ||
                   V_STUDY_CURRENCY ||
                   '</STUDYCURRENCY></STUDY>';
      DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
      IF (V_AFT_ACT_STDATE_STRING <> ' ') THEN
	    V_TEMPVAR := '<PAT>';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
        IF (V_COUNT > 0) THEN
            J := 1;
            K := 1;
		  M := 1;
            FOR I IN 1 .. V_COUNT
            LOOP
               IF (V_MS_ACHIEVEMENT (I) = 'No') THEN
                  V_MS_AMT (I) := '';
               ELSE
                  IF (TO_NUMBER (V_MS_AMT (I)) = 0) THEN
                     V_MS_AMT (I) := 'No associated amount';
                  ELSE
                     V_MS_AMT (I) :=
                        V_STUDY_CURRENCY ||
                        TO_CHAR (
                           TO_NUMBER (LTRIM (RTRIM ((V_MS_AMT (I))))),
                           '9999999990.00'
                        );
                  END IF;
               END IF;

           IF ( V_ORG_ID (I) = V_ORG_ID (M) AND I > 1) THEN
               IF (    V_PATIENT_ID (I) = V_PATIENT_ID (J)
                   AND I > 1) THEN
                  IF (    V_PATPROTS (I) = V_PATPROTS (K)) THEN
                     V_TEMPVAR := '<DESCRIPTION><DESC>' ||
                                  V_MS_DESC (I) ||
                                  '</DESC><RULE>' ||
                                  V_MS_RULE (I) ||
                                  '</RULE><SCH_DATE>' ||
                                  V_SCH_DATE (I) ||
                                  '</SCH_DATE><MS_ACH>' ||
                                  V_MS_ACHIEVEMENT (I) ||
                                  '</MS_ACH><DATE_MS_ACH>' ||
                                  V_MS_ACHIEVEMENT_DATE (I) ||
                                  '</DATE_MS_ACH><MS_AMT>' ||
                                  V_MS_AMT (I) ||
                                  '</MS_AMT></DESCRIPTION>';
                     DBMS_LOB.WRITEAPPEND (
                        O_XML_CLOB,
                        LENGTH (V_TEMPVAR),
                        V_TEMPVAR
                     );
                  ELSE
                     K := I;
                     IF (I > 1) THEN
                        V_TEMPVAR := '</PROTOCOL>';
                        DBMS_LOB.WRITEAPPEND (
                           O_XML_CLOB,
                           LENGTH (V_TEMPVAR),
                           V_TEMPVAR
                        );
                     END IF;
                     V_TEMPVAR := '<PROTOCOL><TITLE>' ||
                                  V_CALENDAR_TITLE (I) ||
                                  '</TITLE><STARTDATE>' ||
                                  V_PROT_START_DATE (I) ||
                                  '</STARTDATE><ENROLDATE>' ||
                                  V_PROT_ENROL_DATE (I) ||
                                  '</ENROLDATE><ACHIEVED>' ||
                                  V_NO_OF_MS_ACHIEVED (I) ||
                                  '</ACHIEVED><TOTAL>' ||
                                  V_NO_OF_MS (I) ||
                                  '</TOTAL><TOTALAMT>' ||
                                  V_STUDY_CURRENCY ||
                                  TO_CHAR (V_TOTAL_AMT (I), '9999999990.00') ||
                                  '</TOTALAMT><DESCRIPTION><DESC>' ||
                                  V_MS_DESC (I) ||
                                  '</DESC><RULE>' ||
                                  V_MS_RULE (I) ||
                                  '</RULE><SCH_DATE>' ||
                                  V_SCH_DATE (I) ||
                                  '</SCH_DATE><MS_ACH>' ||
                                  V_MS_ACHIEVEMENT (I) ||
                                  '</MS_ACH><DATE_MS_ACH>' ||
                                  V_MS_ACHIEVEMENT_DATE (I) ||
                                  '</DATE_MS_ACH><MS_AMT>' ||
                                  V_MS_AMT (I) ||
                                  '</MS_AMT></DESCRIPTION>';
                     DBMS_LOB.WRITEAPPEND (
                        O_XML_CLOB,
                        LENGTH (V_TEMPVAR),
                        V_TEMPVAR
                     );
                  END IF;
               ELSE
                  J := I;
                  K := I;
                  IF (I > 1) THEN
                     V_TEMPVAR := '</PROTOCOL></PATIENT>';
                     DBMS_LOB.WRITEAPPEND (
                        O_XML_CLOB,
                        LENGTH (V_TEMPVAR),
                        V_TEMPVAR
                     );
                  END IF;
                  V_TEMPVAR := '<PATIENT code="' ||
                               V_PATIENT_ID (I) ||
                               '"><PROTOCOL><TITLE>' ||
                               V_CALENDAR_TITLE (I) ||
                               '</TITLE><STARTDATE>' ||
                               V_PROT_START_DATE (I) ||
                               '</STARTDATE><ENROLDATE>' ||
                               V_PROT_ENROL_DATE (I) ||
                               '</ENROLDATE><ACHIEVED>' ||
                               V_NO_OF_MS_ACHIEVED (I) ||
                               '</ACHIEVED><TOTAL>' ||
                               V_NO_OF_MS (I) ||
                               '</TOTAL><TOTALAMT>' ||
                               V_STUDY_CURRENCY ||
                               TO_CHAR (V_TOTAL_AMT (I), '9999999990.00') ||
                               '</TOTALAMT><DESCRIPTION><DESC>' ||
                               V_MS_DESC (I) ||
                               '</DESC><RULE>' ||
                               V_MS_RULE (I) ||
                               '</RULE><SCH_DATE>' ||
                               V_SCH_DATE (I) ||
                               '</SCH_DATE><MS_ACH>' ||
                               V_MS_ACHIEVEMENT (I) ||
                               '</MS_ACH><DATE_MS_ACH>' ||
                               V_MS_ACHIEVEMENT_DATE (I) ||
                               '</DATE_MS_ACH><MS_AMT>' ||
                               V_MS_AMT (I) ||
                               '</MS_AMT></DESCRIPTION>';
                  DBMS_LOB.WRITEAPPEND (
                     O_XML_CLOB,
                     LENGTH (V_TEMPVAR),
                     V_TEMPVAR
                  );
               END IF;

               ELSE
                  J := I;
                  K := I;
			   M := I;
                  IF (I > 1) THEN
                     V_TEMPVAR := '</PROTOCOL></PATIENT></ORG>';
                     DBMS_LOB.WRITEAPPEND (
                        O_XML_CLOB,
                        LENGTH (V_TEMPVAR),
                        V_TEMPVAR
                     );
                  END IF;
                  V_TEMPVAR := '<ORG>' ||
			                '<ORGNAME>'||V_ORG_NAME(I)||'</ORGNAME>' ||
                               '<ORGID>'||V_ORG_ID(I)||'</ORGID>' ||
			                '<PATIENT code="' ||
                               V_PATIENT_ID (I) ||
                               '"><PROTOCOL><TITLE>' ||
                               V_CALENDAR_TITLE (I) ||
                               '</TITLE><STARTDATE>' ||
                               V_PROT_START_DATE (I) ||
                               '</STARTDATE><ENROLDATE>' ||
                               V_PROT_ENROL_DATE (I) ||
                               '</ENROLDATE><ACHIEVED>' ||
                               V_NO_OF_MS_ACHIEVED (I) ||
                               '</ACHIEVED><TOTAL>' ||
                               V_NO_OF_MS (I) ||
                               '</TOTAL><TOTALAMT>' ||
                               V_STUDY_CURRENCY ||
                               TO_CHAR (V_TOTAL_AMT (I), '9999999990.00') ||
                               '</TOTALAMT><DESCRIPTION><DESC>' ||
                               V_MS_DESC (I) ||
                               '</DESC><RULE>' ||
                               V_MS_RULE (I) ||
                               '</RULE><SCH_DATE>' ||
                               V_SCH_DATE (I) ||
                               '</SCH_DATE><MS_ACH>' ||
                               V_MS_ACHIEVEMENT (I) ||
                               '</MS_ACH><DATE_MS_ACH>' ||
                               V_MS_ACHIEVEMENT_DATE (I) ||
                               '</DATE_MS_ACH><MS_AMT>' ||
                               V_MS_AMT (I) ||
                               '</MS_AMT></DESCRIPTION>';
                  DBMS_LOB.WRITEAPPEND (
                     O_XML_CLOB,
                     LENGTH (V_TEMPVAR),
                     V_TEMPVAR
                  );
               END IF;
            END LOOP;
            V_TEMPVAR := '</PROTOCOL></PATIENT></ORG>';
            DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
         END IF;
         V_TEMPVAR := '</PAT>';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
      END IF;
      V_TEMPVAR := '</ROWSET>';
      DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
      DBMS_LOB.CLOSE (O_XML_CLOB);
/*
set serveroutput on
declare
a clob;
b varchar2(2000);
c number := 0;
begin
PKG_MILESTONE_REPORTS.SP_MS_SUMMARY_PATIENT(4045,0,'07/01/2002',
'07/31/2002',a);
c := dbms_lob.getlength(a);
dbms_output.put_line(c) ;
DBMS_LOB.CREATETEMPORARY (a, FALSE);
dbms_output.put_line('1') ;
DBMS_LOB.OPEN (a, DBMS_LOB.LOB_READWRITE);
dbms_output.put_line('2') ;
DBMS_LOB.read(a,c,1,b);
dbms_output.put_line('3') ;
DBMS_LOB.CLOSE(a);
dbms_output.put_line('4') ;
dbms_output.put_line(b) ;
Exception when no_data_found then
dbms_output.put_line('EXCEPTION' || c) ;
end ;
*/
   END SP_MS_SUMMARY_PATIENT;


   PROCEDURE SP_MILESTONE_STUDY_SPECIFIC (
      P_STUDY                    IN       NUMBER,
	 P_ORG_ID                   IN       NUMBER,
      P_STDATE_STRING            IN       VARCHAR2,
      P_ENDDATE_STRING           IN       VARCHAR2,
      O_COUNT                    OUT      NUMBER,
      O_ACHIVEMENT_DATE_ARRAY    OUT      TYPES.SMALL_STRING_ARRAY,
      O_MILESTONE_AMOUNT_ARRAY   OUT      TYPES.SMALL_STRING_ARRAY,
      O_MILESTONE_TYPE_ARRAY     OUT      TYPES.SMALL_STRING_ARRAY,
      O_MILESTONE_DESC_ARRAY     OUT      TYPES.SMALL_STRING_ARRAY,
      O_MILESTONE_RULE_ARRAY     OUT      TYPES.SMALL_STRING_ARRAY
   )
   AS
/*Author: Sajal
  Date: 17th June 2002
  Purpose - Rule: All visit associated CRFs marked p_crfstat_subtyp. To count the number of times this rule has been achieved for a patient
  Input - study id, calendar id, visit number, patient id, CRF status codelst subtype
*/
      V_STDATE             DATE
            := TO_DATE (P_STDATE_STRING, PKG_DATEUTIL.f_get_dateformat);
      V_ENDDATE            DATE
            := TO_DATE (P_ENDDATE_STRING, PKG_DATEUTIL.f_get_dateformat);
      V_COUNT              NUMBER                   := 0;
      V_DATE               DATE                     := pkg_dateUTIL.f_get_null_date_str;
      V_ACHIEVEMENTDATES   TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_PMT_AMTS           TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_PMT_DATES          TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
--      V_STUDY_NUMBER       VARCHAR2 (50);
	        V_STUDY_NUMBER       VARCHAR2 (100);--JM
      V_STUDY_VERSION      VARCHAR2 (50);
      V_STUDY_TITLE        VARCHAR2 (2000);
      V_TEMPVAR            VARCHAR2 (1000);
   BEGIN
      O_ACHIVEMENT_DATE_ARRAY := TYPES.SMALL_STRING_ARRAY ();
      O_MILESTONE_AMOUNT_ARRAY := TYPES.SMALL_STRING_ARRAY ();
      O_MILESTONE_TYPE_ARRAY := TYPES.SMALL_STRING_ARRAY ();
      O_MILESTONE_DESC_ARRAY := TYPES.SMALL_STRING_ARRAY ();
      O_MILESTONE_RULE_ARRAY := TYPES.SMALL_STRING_ARRAY ();
      O_COUNT := 0;
      V_COUNT := 0;
      FOR I IN( SELECT MILESTONE_TYPE, FK_CAL, CODELST_SUBTYP,
                       MILESTONE_AMOUNT, MILESTONE_VISIT, FK_EVENTASSOC,
                       MILESTONE_COUNT, CODELST_DESC, A.NAME CAL_DESC,
                       B.NAME EVENT_DESC
                  FROM ER_MILESTONE,
                       ER_CODELST,
                       EVENT_ASSOC A,
                       EVENT_ASSOC B
                 WHERE FK_STUDY = P_STUDY
                   AND MILESTONE_DELFLAG <> 'Y'
                   AND FK_CODELST_RULE = PK_CODELST
                   AND FK_CAL = A.EVENT_ID (+)
                   AND FK_EVENTASSOC = B.EVENT_ID (+)ORDER BY MILESTONE_TYPE,
                                                              CODELST_SUBTYP,
                                                              FK_CAL,
                                                              MILESTONE_VISIT,
                                                              FK_EVENTASSOC)
      LOOP
         V_ACHIEVEMENTDATES := TYPES.SMALL_STRING_ARRAY ();
         V_COUNT := 0;
         IF (I.MILESTONE_TYPE = 'VM') THEN
            IF (I.CODELST_SUBTYP = 'vm_4') THEN
               Pkg_Vm_Rule.SP_VIS_RULE_CRFS_COUNT (
                  P_STUDY,
			   P_ORG_ID,
                  I.FK_CAL,
                  I.MILESTONE_VISIT,
                  'completed',
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_ACHIEVEMENTDATES
               );
            END IF;
--p('count after SP_VIS_RULE_CRFS_COUNT ' || V_COUNT);
            IF (I.CODELST_SUBTYP = 'vm_5') THEN
               Pkg_Vm_Rule.SP_VIS_RULE_CRFS_COUNT (
                  P_STUDY,
			   P_ORG_ID,
                  I.FK_CAL,
                  I.MILESTONE_VISIT,
                  'submitted',
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_ACHIEVEMENTDATES
               );
            END IF;
            IF (I.CODELST_SUBTYP = 'vm_3') THEN
               Pkg_Vm_Rule.SP_VM_COUNT_EVENT_DONE_ONE (
                  P_STUDY,
			      P_ORG_ID,
                  I.FK_CAL,
                  I.MILESTONE_VISIT,
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_ACHIEVEMENTDATES
               );
            END IF;
            IF (I.CODELST_SUBTYP = 'vm_2') THEN
               Pkg_Vm_Rule.SP_VM_COUNT_EVENT_DONE_ALL (
                  P_STUDY,
			   P_ORG_ID,
                  I.FK_CAL,
                  I.MILESTONE_VISIT,
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_ACHIEVEMENTDATES
               );
            END IF;


         END IF;
         IF (I.MILESTONE_TYPE = 'PM') THEN
            IF (I.CODELST_SUBTYP = 'pm_1') THEN
               Pkg_Pm_Rule.SP_CHK_PAT_RULE_ENR_ACT (
                  P_STUDY,
			   P_ORG_ID,
                  I.MILESTONE_COUNT,
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_DATE
               );
            END IF;


            IF (I.CODELST_SUBTYP = 'pm_2') THEN
               Pkg_Pm_Rule.SP_CHK_PAT_RULE_ENR_ALL (
                  P_STUDY,
			   P_ORG_ID,
                  I.MILESTONE_COUNT,
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_DATE
               );
            END IF;


            IF (I.CODELST_SUBTYP = 'pm_4') THEN
               Pkg_Pm_Rule.SP_RULE_CHK_VIS_DONE_ONE (
                  P_STUDY,
			   P_ORG_ID,
                  I.MILESTONE_COUNT,
                  I.MILESTONE_VISIT,
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_DATE
               );

            END IF;

            IF (I.CODELST_SUBTYP = 'pm_5') THEN
               Pkg_Pm_Rule.SP_RULE_CHK_VIS_DONE_COMPLETE (
                  P_STUDY,
			   P_ORG_ID,
                  I.MILESTONE_COUNT,
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_DATE
               );
            END IF;
            IF (V_COUNT = 1) THEN
               V_ACHIEVEMENTDATES.EXTEND;
               V_ACHIEVEMENTDATES (1) := TO_CHAR (V_DATE, PKG_DATEUTIL.f_get_dateformat);
            END IF;
         END IF;
         IF (I.MILESTONE_TYPE = 'EM') THEN
            IF (I.CODELST_SUBTYP = 'em_2') THEN
               Pkg_Em_Rule.SP_EVENT_DONE_COUNT (
                  P_STUDY,
			   P_ORG_ID,
                  I.FK_CAL,
                  I.FK_EVENTASSOC,
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_ACHIEVEMENTDATES
               );
            END IF;
--p('sp_eve_rule_crf_count V_ENDDATE ' ||V_ENDDATE);
            IF (I.CODELST_SUBTYP = 'em_3') THEN
               Pkg_Em_Rule.SP_EVE_RULE_CRF_COUNT (
                  I.FK_EVENTASSOC,
                  'completed',
                  P_STUDY,
			   P_ORG_ID,
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_ACHIEVEMENTDATES
               );
            END IF;
            IF (I.CODELST_SUBTYP = 'em_4') THEN
               Pkg_Em_Rule.SP_EVE_RULE_CRF_COUNT (
                  I.FK_EVENTASSOC,
                  'submitted',
                  P_STUDY,
			   P_ORG_ID,
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_ACHIEVEMENTDATES
               );
            END IF;
         END IF;
         FOR J IN 1 .. V_COUNT
         LOOP
            O_COUNT := O_COUNT + 1;
            O_ACHIVEMENT_DATE_ARRAY.EXTEND;
            O_MILESTONE_AMOUNT_ARRAY.EXTEND;
            O_MILESTONE_TYPE_ARRAY.EXTEND;
            O_MILESTONE_DESC_ARRAY.EXTEND;
            O_MILESTONE_RULE_ARRAY.EXTEND;
            O_ACHIVEMENT_DATE_ARRAY (O_COUNT) := V_ACHIEVEMENTDATES (J);
            O_MILESTONE_AMOUNT_ARRAY (O_COUNT) := I.MILESTONE_AMOUNT;
            O_MILESTONE_TYPE_ARRAY (O_COUNT) := I.MILESTONE_TYPE;
            O_MILESTONE_RULE_ARRAY (O_COUNT) := I.CODELST_DESC;
            IF (I.MILESTONE_TYPE = 'PM') THEN
               V_TEMPVAR := 'Patient Count ' || I.MILESTONE_COUNT;
            END IF;
            IF (I.MILESTONE_TYPE = 'VM') THEN
               V_TEMPVAR := 'Visit ' ||
                            I.MILESTONE_VISIT ||
                            ' (' ||
                            I.CAL_DESC ||
                            ')';
            END IF;
            IF (I.MILESTONE_TYPE = 'EM') THEN
               V_TEMPVAR := 'Event ' ||
                            I.EVENT_DESC ||
                            ' in Visit ' ||
                            I.MILESTONE_VISIT ||
                            ' (' ||
                            I.CAL_DESC ||
                            ')';
            END IF;
            O_MILESTONE_DESC_ARRAY (O_COUNT) := V_TEMPVAR;
         END LOOP;
      END LOOP;
/*
    DBMS_LOB.CREATETEMPORARY (O_XML_CLOB, FALSE);
      DBMS_LOB.OPEN (O_XML_CLOB, DBMS_LOB.LOB_READWRITE);
      V_TEMPVAR := 'hello';
      DBMS_LOB.WRITEAPPEND (O_XML_CLOB, length (V_TEMPVAR), V_TEMPVAR);
      DBMS_LOB.CLOSE (O_XML_CLOB);
*/
/*
Exception
   When NO_DATA_FOUND then
      O_COUNT := 0;
*/
/**********************************
TEST
declare
O_ACHIVEMENT_DATE_ARRAY TYPES.SMALL_STRING_ARRAY;
O_MILESTONE_AMOUNT_ARRAY TYPES.SMALL_STRING_ARRAY;
O_MILESTONE_TYPE_ARRAY TYPES.SMALL_STRING_ARRAY;
O_MILESTONE_DESC_ARRAY   TYPES.SMALL_STRING_ARRAY;
O_MILESTONE_RULE_ARRAY TYPES.SMALL_STRING_ARRAY;
c number := 0;
begin
PKG_MILESTONE_REPORTS.SP_MILESTONE_STUDY_SPECIFIC(1839,pkg_dateUTIL.f_get_null_date_str,
pkg_dateUTIL.f_get_future_null_date_str, c, O_ACHIVEMENT_DATE_ARRAY, O_MILESTONE_AMOUNT_ARRAY, O_MILESTONE_TYPE_ARRAY, O_MILESTONE_DESC_ARRAY, O_MILESTONE_RULE_ARRAY);
dbms_output.put_line(c) ;
end ;
**************************************/
   END SP_MILESTONE_STUDY_SPECIFIC;
   PROCEDURE SP_MILEPAYMENT (
      P_STUDY                  NUMBER,
      P_STDATE_STRING          VARCHAR2,
      P_ENDDATE_STRING         VARCHAR2,
      O_COUNT            OUT   NUMBER,
      O_AMOUNTS          OUT   TYPES.SMALL_STRING_ARRAY,
      O_PAYMENTDATES     OUT   TYPES.SMALL_STRING_ARRAY
   )
   AS
/*Author: Arvind Kumar
  Date: 28th June 2002
  Returns two arrays of amount and payment date that lies within a particular range and study.
  Input - study id, start date, end date
*/
      V_AMOUNT        NUMBER := 0;
      V_PAYMENTDATE   DATE   := pkg_dateUTIL.f_get_null_date_str;
   BEGIN
      O_COUNT := 0;
      O_PAYMENTDATES := TYPES.SMALL_STRING_ARRAY ();
      O_AMOUNTS := TYPES.SMALL_STRING_ARRAY ();
      P ('2129 P_STUDY' || P_STUDY);
      FOR I IN( SELECT MILEPAYMENT_DATE, MILEPAYMENT_AMT
                  FROM ER_MILEPAYMENT
                 WHERE FK_STUDY = P_STUDY
                   AND TO_DATE (
                          TO_CHAR (MILEPAYMENT_DATE, PKG_DATEUTIL.f_get_dateformat),
                          PKG_DATEUTIL.f_get_dateformat
                       ) >=
                          TO_DATE (P_STDATE_STRING, PKG_DATEUTIL.f_get_dateformat)
                   AND TO_DATE (
                          TO_CHAR (MILEPAYMENT_DATE, PKG_DATEUTIL.f_get_dateformat),
                          PKG_DATEUTIL.f_get_dateformat
                       ) <=
                          TO_DATE (P_ENDDATE_STRING, PKG_DATEUTIL.f_get_dateformat)
                   AND MILEPAYMENT_DELFLAG <> 'Y')
      LOOP
         V_PAYMENTDATE := I.MILEPAYMENT_DATE;
         V_AMOUNT := I.MILEPAYMENT_AMT;
         O_COUNT := O_COUNT + 1;
         O_PAYMENTDATES.EXTEND;
         O_AMOUNTS.EXTEND;
         O_PAYMENTDATES (O_COUNT) := TO_CHAR (V_PAYMENTDATE, PKG_DATEUTIL.f_get_dateformat);
         O_AMOUNTS (O_COUNT) := TO_CHAR (V_AMOUNT);
      END LOOP;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         O_COUNT := 0;
   END SP_MILEPAYMENT;

   PROCEDURE SP_STUDY_MILESTONE_VMEM (
      P_STUDY               NUMBER,
      P_MSTYPE              VARCHAR2,
      O_M_PKMS        OUT   TYPES.SMALL_STRING_ARRAY,
      O_M_RULECODE    OUT   TYPES.SMALL_STRING_ARRAY,
      O_M_RULEVISIT   OUT   TYPES.SMALL_STRING_ARRAY,
      O_M_EVENT       OUT   TYPES.SMALL_STRING_ARRAY,
      O_M_RULEUSERS   OUT   TYPES.SMALL_STRING_ARRAY,
      O_M_RULECAL     OUT   TYPES.SMALL_STRING_ARRAY,
      O_M_RULEDESC    OUT   TYPES.SMALL_STRING_ARRAY,
      O_M_RULEAMT     OUT   TYPES.SMALL_STRING_ARRAY,
      O_M_EVENTDESC   OUT   TYPES.SMALL_STRING_ARRAY
   )
   AS
      /* Date : 19 Jul 2002
         Author: Sajal
         Purpose - GET all P_MSTYPE milestones for a study
      */
      V_COUNT   NUMBER := 0;
   BEGIN
      O_M_PKMS := TYPES.SMALL_STRING_ARRAY ();
      O_M_RULECODE := TYPES.SMALL_STRING_ARRAY ();
      O_M_RULEVISIT := TYPES.SMALL_STRING_ARRAY ();
      O_M_EVENT := TYPES.SMALL_STRING_ARRAY ();
      O_M_RULEUSERS := TYPES.SMALL_STRING_ARRAY ();
      O_M_RULECAL := TYPES.SMALL_STRING_ARRAY ();
      O_M_RULEDESC := TYPES.SMALL_STRING_ARRAY ();
      O_M_RULEAMT := TYPES.SMALL_STRING_ARRAY ();
      O_M_EVENTDESC := TYPES.SMALL_STRING_ARRAY ();
      FOR I IN( SELECT TO_CHAR (M.PK_MILESTONE) PK_MILESTONE,
                       TO_CHAR (M.MILESTONE_VISIT) MILESTONE_VISIT,
                       M.MILESTONE_USERSTO, CD.CODELST_SUBTYP,
                       TO_CHAR (M.FK_EVENTASSOC) MILESTONE_EVENT,
                       FK_CAL MILESTONE_CAL, CD.CODELST_DESC,
                       M.MILESTONE_AMOUNT, EA.NAME EVENT_DESC
                  FROM ER_MILESTONE M, ER_CODELST CD, EVENT_ASSOC EA
                 WHERE FK_STUDY = P_STUDY
                   AND CD.PK_CODELST = M.FK_CODELST_RULE
                   AND trim (CD.CODELST_TYPE) = trim (P_MSTYPE)
                   AND M.MILESTONE_DELFLAG <> 'Y'
                   AND M.FK_EVENTASSOC = EA.EVENT_ID (+))
      LOOP
         V_COUNT := V_COUNT + 1;
         O_M_PKMS.EXTEND;
         O_M_PKMS (V_COUNT) := I.PK_MILESTONE;
         O_M_RULECODE.EXTEND;
         O_M_RULECODE (V_COUNT) := I.CODELST_SUBTYP;
         O_M_RULEVISIT.EXTEND;
         O_M_RULEVISIT (V_COUNT) := I.MILESTONE_VISIT;
         O_M_RULEUSERS.EXTEND;
         O_M_RULEUSERS (V_COUNT) := I.MILESTONE_USERSTO;
         O_M_EVENT.EXTEND;
         O_M_EVENT (V_COUNT) := I.MILESTONE_EVENT;
         O_M_RULECAL.EXTEND;
         O_M_RULECAL (V_COUNT) := I.MILESTONE_CAL;
         O_M_RULEDESC.EXTEND;
         O_M_RULEDESC (V_COUNT) := I.CODELST_DESC;
         O_M_RULEAMT.EXTEND;
         O_M_RULEAMT (V_COUNT) := I.MILESTONE_AMOUNT;
         O_M_EVENTDESC.EXTEND;
         O_M_EVENTDESC (V_COUNT) := I.EVENT_DESC;
      END LOOP;
   END SP_STUDY_MILESTONE_VMEM;

   PROCEDURE SP_PYMTS_BEFORE_STDY_ACT (
      P_STUDY            IN       NUMBER,
      P_STDATE_STRING    IN       VARCHAR2,
      P_STUDY_ACT_DATE   IN       VARCHAR2,
      P_REPORT_TYPE      IN       VARCHAR2,
      O_XML_CLOB         OUT      CLOB
   )
   AS
/*Author: Sajal
  Date: 3rd October 2002
  Purpose - Used for PAYMENTS beofre study activation
*/
      V_COUNT               NUMBER                   := 0;
      V_TEMPVAR             VARCHAR2 (32767);
      V_PMT_AMTS            TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_PMT_DATES           TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_STDATE_STRING       VARCHAR2 (20);
      V_ENDDATE_STRING      VARCHAR2 (20);
      V_STUDY_CURRENCY      VARCHAR2 (20);
      V_MIN_YEAR            NUMBER;
      V_MAX_YEAR            NUMBER;
      V_NO_OF_YRS           NUMBER;
      J                     NUMBER;
      V_YR_COUNT            NUMBER;
      V_RELATIVE_INTERVAL   NUMBER;
      V_TOTAL_INTERVALS     NUMBER;
      V_INTERVAL_ARRAY      TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_PYMT_AMT_ARRAY      TYPES.NUMBER_ARRAY       := TYPES.NUMBER_ARRAY ();
   BEGIN
P ('2287 P_STDATE_STRING' || P_STDATE_STRING);
P ('2288 P_STUDY_ACT_DATE' || P_STUDY_ACT_DATE);
      V_ENDDATE_STRING := P_STUDY_ACT_DATE;
      V_RELATIVE_INTERVAL := -1;
      SELECT NVL (TO_CHAR (MIN (MILEPAYMENT_DATE), PKG_DATEUTIL.f_get_dateformat), '0')
        INTO V_STDATE_STRING
        FROM ER_MILEPAYMENT
       WHERE FK_STUDY = P_STUDY
         AND MILEPAYMENT_DATE < TO_DATE (P_STUDY_ACT_DATE, PKG_DATEUTIL.f_get_dateformat)
         AND milepayment_delflag <> 'Y';
      IF (V_STDATE_STRING <> '0') THEN
         SELECT CODELST_SUBTYP
           INTO V_STUDY_CURRENCY
           FROM SCH_CODELST, ER_STUDY
          WHERE FK_CODELST_CURRENCY = PK_CODELST
            AND PK_STUDY = P_STUDY;
         P ('2291 V_STDATE_STRING' || V_STDATE_STRING);
         P ('2292 V_ENDDATE_STRING' || V_ENDDATE_STRING);
         V_MIN_YEAR := TO_NUMBER (SUBSTR (V_STDATE_STRING, 7, 4));
         V_MAX_YEAR := TO_NUMBER (SUBSTR (V_ENDDATE_STRING, 7, 4));
         P ('2297 V_MIN_YEAR' || V_MIN_YEAR);
         P ('2298 V_MAX_YEAR' || V_MAX_YEAR);
         IF (V_MIN_YEAR IS NULL) THEN
            V_MIN_YEAR := V_MAX_YEAR + 1;
         END IF;
--p('2304 V_MIN_YEAR' || V_MIN_YEAR);
         V_NO_OF_YRS := V_MAX_YEAR - V_MIN_YEAR + 1;
         V_YR_COUNT := V_MIN_YEAR;
         J := 1;
     --    P ('2329 V_YR_COUNT' || V_YR_COUNT);
       --  P ('2329 V_NO_OF_YRS' || V_NO_OF_YRS);
         IF (P_REPORT_TYPE = 'M') THEN
            V_TOTAL_INTERVALS := 12 * V_NO_OF_YRS;
            --p('V_TOTAL_INTERVALS 1: ' ||V_TOTAL_INTERVALS);
            FOR I IN 1 .. V_NO_OF_YRS
            LOOP
               V_INTERVAL_ARRAY.EXTEND (12);
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 1) :=
                  'January, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 2) :=
                  'February, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 3) := 'March, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 4) := 'April, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 5) := 'May, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 6) := 'June, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 7) := 'July, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 8) :=
                  'August, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 9) :=
                  'September, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 10) :=
                  'October, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 11) :=
                  'November, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 12) :=
                  'December, ' || V_YR_COUNT;
               P ('2353 V_YR_COUNT' || V_YR_COUNT);
               V_YR_COUNT := V_YR_COUNT + 1;
            END LOOP;
         END IF;
         IF (P_REPORT_TYPE = 'Q') THEN
            V_TOTAL_INTERVALS := 4 * V_NO_OF_YRS;
            FOR I IN 1 .. V_NO_OF_YRS
            LOOP
               V_INTERVAL_ARRAY.EXTEND (4);
               V_INTERVAL_ARRAY ((4 * (I - 1)) + 1) := 'Q1, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((4 * (I - 1)) + 2) := 'Q2, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((4 * (I - 1)) + 3) := 'Q3, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((4 * (I - 1)) + 4) := 'Q4, ' || V_YR_COUNT;
               V_YR_COUNT := V_YR_COUNT + 1;
            END LOOP;
         END IF;
         IF (P_REPORT_TYPE = 'Y') THEN
            V_TOTAL_INTERVALS := V_NO_OF_YRS;
            FOR I IN 1 .. V_NO_OF_YRS
            LOOP
               V_INTERVAL_ARRAY.EXTEND;
               V_INTERVAL_ARRAY (I) := 'Year ' || V_YR_COUNT;
               V_YR_COUNT := V_YR_COUNT + 1;
            END LOOP;
         END IF;
         V_INTERVAL_ARRAY.EXTEND;
         V_PYMT_AMT_ARRAY.EXTEND;
         IF (V_NO_OF_YRS > 0) THEN
            V_PYMT_AMT_ARRAY (1) := 0;
            V_PYMT_AMT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
            SP_MILEPAYMENT (
               P_STUDY,
               V_STDATE_STRING,
               V_ENDDATE_STRING,
               V_COUNT,
               V_PMT_AMTS,
               V_PMT_DATES
            );
            IF (P_REPORT_TYPE = 'M') THEN
               FOR I IN 1 .. V_COUNT
               LOOP
                  V_RELATIVE_INTERVAL :=
                     (12 *
                         (TO_NUMBER (SUBSTR (V_PMT_DATES (I), 7, 4)) -
                          V_MIN_YEAR
                         )
                     ) +
                     TO_NUMBER (SUBSTR (V_PMT_DATES (I), 1, 2));
                  V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_PMT_AMTS (I));
               END LOOP;
            END IF;
            IF (P_REPORT_TYPE = 'Q') THEN
               FOR I IN 1 .. V_COUNT
               LOOP
                  V_RELATIVE_INTERVAL :=
                     (4 *
                         (TO_NUMBER (
                             SUBSTR (
                                V_PMT_DATES (
                                   I
                                ),
                                7,
                                4
                             )
                          ) -
                          V_MIN_YEAR
                         )
                     ) +
                     (TO_NUMBER (
                         SUBSTR (
                            TO_CHAR (
                               (TO_NUMBER (
                                   SUBSTR (
                                      V_PMT_DATES (
                                         I
                                      ),
                                      1,
                                      2
                                   )
                                ) -
                                1
                               ) /
                                  3,
                               '0.0'
                            ),
                            2,
                            1
                         )
                      ) +
                      1
                     );
                  V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_PMT_AMTS (I));
               END LOOP;
            END IF;
            IF (P_REPORT_TYPE = 'Y') THEN
               FOR I IN 1 .. V_COUNT
               LOOP
                  V_RELATIVE_INTERVAL :=
                     TO_NUMBER (
                        SUBSTR (
                           V_PMT_DATES (
                              I
                           ),
                           7,
                           4
                        )
                     ) -
                     V_MIN_YEAR +
                     1;
                  V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_PMT_AMTS (I));
               END LOOP;
            END IF;
            J := V_TOTAL_INTERVALS + 1;
            V_INTERVAL_ARRAY (J) := 'Total';
--p('V_TOTAL_INTERVALS 3: '||V_TOTAL_INTERVALS);
            FOR I IN 1 .. V_TOTAL_INTERVALS
            LOOP
               V_PYMT_AMT_ARRAY (J) :=
                  V_PYMT_AMT_ARRAY (J) + V_PYMT_AMT_ARRAY (I);
            END LOOP;
            V_RELATIVE_INTERVAL := J - 1;
            IF (P_REPORT_TYPE = 'M') THEN
               V_RELATIVE_INTERVAL :=
                  (12 *
                      (TO_NUMBER (SUBSTR (V_ENDDATE_STRING, 7, 4)) - V_MIN_YEAR)
                  ) +
                  TO_NUMBER (SUBSTR (V_ENDDATE_STRING, 1, 2));
            END IF;
            --p('V_RELATIVE_INTERVAL 1: ' ||V_RELATIVE_INTERVAL);
            IF (P_REPORT_TYPE = 'Q') THEN
               V_RELATIVE_INTERVAL :=
                  (4 *
                      (TO_NUMBER (
                          SUBSTR (
                             V_ENDDATE_STRING,
                             7,
                             4
                          )
                       ) -
                       V_MIN_YEAR
                      )
                  ) +
                  (TO_NUMBER (
                      SUBSTR (
                         TO_CHAR (
                            (TO_NUMBER (
                                SUBSTR (
                                   V_ENDDATE_STRING,
                                   1,
                                   2
                                )
                             ) -
                             1
                            ) /
                               3,
                            '0.0'
                         ),
                         2,
                         1
                      )
                   ) +
                   1
                  );
            END IF;
            IF (P_REPORT_TYPE = 'Y') THEN
               V_RELATIVE_INTERVAL :=
                  TO_NUMBER (SUBSTR (V_ENDDATE_STRING, 7, 4)) - V_MIN_YEAR + 1;
            END IF;
            FOR I IN 1 .. (J - (V_RELATIVE_INTERVAL + 1))
            LOOP
               FOR K IN (V_RELATIVE_INTERVAL + 1) .. (J - 1)
               LOOP
                  V_INTERVAL_ARRAY (K) := V_INTERVAL_ARRAY (K + 1);
                  V_PYMT_AMT_ARRAY (K) := V_PYMT_AMT_ARRAY (K + 1);
               END LOOP;
            END LOOP;
            V_INTERVAL_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
            V_PYMT_AMT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
            V_TOTAL_INTERVALS :=
               V_TOTAL_INTERVALS - (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
            J := V_TOTAL_INTERVALS + 1;
            IF (P_REPORT_TYPE = 'M') THEN
               V_RELATIVE_INTERVAL :=
                  (12 *
                      (V_MIN_YEAR -
                       TO_NUMBER (
                          SUBSTR (
                             V_STDATE_STRING,
                             7,
                             4
                          )
                       )
                      )
                  ) +
                  TO_NUMBER (
                     SUBSTR (
                        V_STDATE_STRING,
                        1,
                        2
                     )
                  );
            END IF;
            IF (P_REPORT_TYPE = 'Q') THEN
               V_RELATIVE_INTERVAL :=
                  (4 *
                      (V_MIN_YEAR -
                       TO_NUMBER (
                          SUBSTR (
                             V_STDATE_STRING,
                             7,
                             4
                          )
                       )
                      )
                  ) +
                  (TO_NUMBER (
                      SUBSTR (
                         TO_CHAR (
                            (TO_NUMBER (
                                SUBSTR (
                                   V_STDATE_STRING,
                                   1,
                                   2
                                )
                             ) -
                             1
                            ) /
                               3,
                            '0.0'
                         ),
                         2,
                         1
                      )
                   ) +
                   1
                  );
            END IF;
            IF (P_REPORT_TYPE = 'Y') THEN
               V_RELATIVE_INTERVAL :=
                  V_MIN_YEAR - TO_NUMBER (SUBSTR (V_STDATE_STRING, 7, 4)) + 1;
            END IF;
            FOR I IN 1 .. (V_RELATIVE_INTERVAL - 1)
            LOOP
               FOR K IN 1 .. (J - 1)
               LOOP
                  V_INTERVAL_ARRAY (K) := V_INTERVAL_ARRAY (K + 1);
                  V_PYMT_AMT_ARRAY (K) := V_PYMT_AMT_ARRAY (K + 1);
               END LOOP;
            END LOOP;
            V_INTERVAL_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
            V_PYMT_AMT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
            V_TOTAL_INTERVALS := V_TOTAL_INTERVALS - (V_RELATIVE_INTERVAL - 1);
            J := J - (V_RELATIVE_INTERVAL - 1);
         END IF;
         DBMS_LOB.CREATETEMPORARY (O_XML_CLOB, FALSE);
         DBMS_LOB.OPEN (O_XML_CLOB, DBMS_LOB.LOB_READWRITE);
         V_TEMPVAR := '<BEFORE_STUDY_ACTIVATION><INTERVALS>';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
         FOR I IN 1 .. V_TOTAL_INTERVALS
         LOOP
            V_TEMPVAR := '<ROW num="' ||
                         I ||
                         '"><INTERVAL>' ||
                         V_INTERVAL_ARRAY (I) ||
                         '</INTERVAL><PYMTAMT>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (V_PYMT_AMT_ARRAY (I), '9999999990.00') ||
                         '</PYMTAMT></ROW>';
            DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
         END LOOP;
         V_TEMPVAR := '</INTERVALS>';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
         V_TEMPVAR := '<TOTAL><INTERVAL>TOTAL</INTERVAL><PYMTAMT>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_PYMT_AMT_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</PYMTAMT></TOTAL></BEFORE_STUDY_ACTIVATION>';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
         DBMS_LOB.CLOSE (O_XML_CLOB);
      ELSE
         DBMS_LOB.CREATETEMPORARY (O_XML_CLOB, FALSE);
         DBMS_LOB.OPEN (O_XML_CLOB, DBMS_LOB.LOB_READWRITE);
         V_TEMPVAR := ' ';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
         DBMS_LOB.CLOSE (O_XML_CLOB);
      END IF;
/*
TEST
set serveroutpur on
declare
a clob;
b varchar2(2000);
c number := 0;
begin
PKG_MILESTONE_REPORTS.SP_MS_FINANCIAL(4045,'07/01/2002',
'07/31/2002','M',a);
c := dbms_lob.getlength(a);
dbms_output.put_line(c) ;
DBMS_LOB.CREATETEMPORARY (a, FALSE);
dbms_output.put_line('1') ;
DBMS_LOB.OPEN (a, DBMS_LOB.LOB_READWRITE);
dbms_output.put_line('2') ;
DBMS_LOB.read(a,c,1,b);
dbms_output.put_line('3') ;
DBMS_LOB.CLOSE(a);
dbms_output.put_line('4') ;
dbms_output.put_line(b) ;
Exception when no_data_found then
dbms_output.put_line('EXCEPTION' || c) ;
end ;
**************************************/
   END SP_PYMTS_BEFORE_STDY_ACT;
   PROCEDURE SP_FIND_STUDY_ACT_DATES (
      P_STUDY                    IN       NUMBER,
      P_STDATE_STRING            IN       VARCHAR2,
      P_ENDDATE_STRING           IN       VARCHAR2,
      O_ACT_DATE_STRING          OUT      VARCHAR2,
      O_BEF_ACT_STDATE_STRING    OUT      VARCHAR2,
      O_BEF_ACT_ENDDATE_STRING   OUT      VARCHAR2,
      O_AFT_ACT_STDATE_STRING    OUT      VARCHAR2,
      O_AFT_ACT_ENDDATE_STRING   OUT      VARCHAR2
   )
   AS
/*Author: Sajal
  Date: 4th October 2002
  Purpose - Used for PAYMENTS beofre study activation
*/
      V_STDATE_STRING    VARCHAR2 (20);
      V_ENDDATE_STRING   VARCHAR2 (20);
   BEGIN
      V_STDATE_STRING := P_STDATE_STRING;
      V_ENDDATE_STRING := P_ENDDATE_STRING;
      IF (    P_STDATE_STRING = pkg_dateUTIL.f_get_null_date_str
          AND P_ENDDATE_STRING = pkg_dateUTIL.f_get_future_null_date_str) THEN
         SELECT TO_CHAR (SYSDATE, PKG_DATEUTIL.f_get_dateformat)
           INTO V_ENDDATE_STRING
           FROM DUAL;
      END IF;
      SELECT NVL (TO_CHAR (STUDY_ACTUALDT, PKG_DATEUTIL.f_get_dateformat), '0')
        INTO O_ACT_DATE_STRING
        FROM ER_STUDY
       WHERE PK_STUDY = P_STUDY;
/*
      select nvl (to_char (min (STUDYSTAT_DATE), PKG_DATEUTIL.f_get_dateformat), '0')
        into O_ACT_DATE_STRING
        from ER_STUDYSTAT
       where FK_STUDY = P_STUDY
         and FK_CODELST_STUDYSTAT = (select PK_CODELST
                                       from ER_CODELST
                                      where CODELST_TYPE = 'studystat'
                                        and CODELST_SUBTYP = 'active');
*/
      IF (O_ACT_DATE_STRING = '0') THEN
         O_ACT_DATE_STRING := ' ';
         O_BEF_ACT_STDATE_STRING := V_STDATE_STRING;
         O_BEF_ACT_ENDDATE_STRING := V_ENDDATE_STRING;
         O_AFT_ACT_STDATE_STRING := ' ';
         O_AFT_ACT_ENDDATE_STRING := ' ';
      ELSE
         IF (TO_DATE (O_ACT_DATE_STRING, PKG_DATEUTIL.f_get_dateformat) >
                TO_DATE (V_ENDDATE_STRING, PKG_DATEUTIL.f_get_dateformat)
            ) THEN
            O_BEF_ACT_STDATE_STRING := V_STDATE_STRING;
            O_BEF_ACT_ENDDATE_STRING := V_ENDDATE_STRING;
            O_AFT_ACT_STDATE_STRING := ' ';
            O_AFT_ACT_ENDDATE_STRING := ' ';
         ELSE
            IF (TO_DATE (O_ACT_DATE_STRING, PKG_DATEUTIL.f_get_dateformat) <
                   TO_DATE (V_STDATE_STRING, PKG_DATEUTIL.f_get_dateformat)
               ) THEN
               O_BEF_ACT_STDATE_STRING := ' ';
               O_BEF_ACT_ENDDATE_STRING := ' ';
               O_AFT_ACT_STDATE_STRING := V_STDATE_STRING;
               O_AFT_ACT_ENDDATE_STRING := V_ENDDATE_STRING;
            ELSE
               O_BEF_ACT_STDATE_STRING := V_STDATE_STRING;
               O_BEF_ACT_ENDDATE_STRING :=
                  TO_CHAR (
                     (TO_DATE (O_ACT_DATE_STRING, PKG_DATEUTIL.f_get_dateformat) - 1),
                     PKG_DATEUTIL.f_get_dateformat
                  );
               O_AFT_ACT_STDATE_STRING := O_ACT_DATE_STRING;
               O_AFT_ACT_ENDDATE_STRING := V_ENDDATE_STRING;
            END IF;
         END IF;
      END IF;
   END SP_FIND_STUDY_ACT_DATES;
-------------------------------------------------------------------------------------------------------------------------------

   PROCEDURE SP_MS_FINANCIAL_PAYMENT (
      P_STUDY            IN       NUMBER,
      P_STDATE_STRING    IN       VARCHAR2,
      P_ENDDATE_STRING   IN       VARCHAR2,
      P_REPORT_TYPE      IN       VARCHAR2,
      O_XML_CLOB         OUT      CLOB
   )
   AS
/*Author: Sajal
  Date: 10th July 2002
  Purpose - Used for Milestones payment reports
*/
      V_ACHIVEMENT_DATE_ARRAY    TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_AMOUNT_ARRAY   TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_TYPE_ARRAY     TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_DESC_ARRAY     TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_RULE_ARRAY     TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();

      V_COUNT                    NUMBER                   := 0;
      V_REPORT_TYPE_FLAG         NUMBER                   := 0;

      TEMP_CLOB                  CLOB;

      V_TEMPVAR                  VARCHAR2 (32767);
      V_PMT_AMTS                 TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_PMT_DATES                TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_STDATE_STRING            VARCHAR2 (20);
      V_ENDDATE_STRING           VARCHAR2 (20);
--      V_STUDY_NUMBER             VARCHAR2 (50);
	  V_STUDY_NUMBER             VARCHAR2 (100);--JM
      V_STUDY_VERSION            VARCHAR2 (50);
      V_STUDY_TITLE              VARCHAR2 (2000);
      V_STUDY_ACT_DATE           VARCHAR2 (20);
      V_STUDY_CURRENCY           VARCHAR2 (20);
      V_MIN_YEAR                 NUMBER;
      V_MAX_YEAR                 NUMBER;
      V_NO_OF_YRS                NUMBER;
      J                          NUMBER;
      V_YR_COUNT                 NUMBER;
      V_RELATIVE_INTERVAL        NUMBER;
      V_TOTAL_INTERVALS          NUMBER;
      V_INTERVAL_ARRAY           TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_VISIT_CNT_ARRAY          TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_EVENT_CNT_ARRAY          TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_PATIENT_CNT_ARRAY        TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_VISIT_AMT_ARRAY          TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_EVENT_AMT_ARRAY          TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_PATIENT_AMT_ARRAY        TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_TOTAL_AMT_ARRAY          TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_PYMT_AMT_ARRAY           TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_PYMT_DISCREPANCY_ARRAY   TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();

      V_ACT_DATE_STRING          VARCHAR2 (20);
      V_BEF_ACT_STDATE_STRING    VARCHAR2 (20);
      V_BEF_ACT_ENDDATE_STRING   VARCHAR2 (20);
      V_AFT_ACT_STDATE_STRING    VARCHAR2 (20);
      V_AFT_ACT_ENDDATE_STRING   VARCHAR2 (20);
   BEGIN
      SP_FIND_STUDY_ACT_DATES (
         P_STUDY,
         P_STDATE_STRING,
         P_ENDDATE_STRING,
         V_ACT_DATE_STRING,
         V_BEF_ACT_STDATE_STRING,
         V_BEF_ACT_ENDDATE_STRING,
         V_AFT_ACT_STDATE_STRING,
         V_AFT_ACT_ENDDATE_STRING
      );
--      V_STDATE_STRING := P_STDATE_STRING;
--      V_ENDDATE_STRING := P_ENDDATE_STRING;
      V_RELATIVE_INTERVAL := -1;

      SELECT STUDY_NUMBER, STUDY_VER_NO, STUDY_TITLE
        INTO V_STUDY_NUMBER, V_STUDY_VERSION, V_STUDY_TITLE
        FROM ER_STUDY
       WHERE PK_STUDY = P_STUDY;

      SELECT CODELST_SUBTYP
        INTO V_STUDY_CURRENCY
        FROM SCH_CODELST, ER_STUDY
       WHERE FK_CODELST_CURRENCY = PK_CODELST
         AND PK_STUDY = P_STUDY;
      P (V_AFT_ACT_STDATE_STRING);

      IF (V_AFT_ACT_STDATE_STRING <> ' ') THEN
         P (V_AFT_ACT_STDATE_STRING);

         SP_MILESTONE_STUDY_SPECIFIC (
            P_STUDY,
            0,
            V_AFT_ACT_STDATE_STRING,
            V_AFT_ACT_ENDDATE_STRING,
            V_COUNT,
            V_ACHIVEMENT_DATE_ARRAY,
            V_MILESTONE_AMOUNT_ARRAY,
            V_MILESTONE_TYPE_ARRAY,
            V_MILESTONE_DESC_ARRAY,
            V_MILESTONE_RULE_ARRAY
         );

         V_MIN_YEAR := TO_NUMBER (SUBSTR (V_AFT_ACT_STDATE_STRING, 7, 4));
         V_MAX_YEAR := TO_NUMBER (SUBSTR (V_AFT_ACT_ENDDATE_STRING, 7, 4));


         IF (V_MIN_YEAR IS NULL) THEN
            V_MIN_YEAR := V_MAX_YEAR + 1;
         END IF;


         V_NO_OF_YRS := V_MAX_YEAR - V_MIN_YEAR + 1;
         P ('V_NO_OF_YRS ' || V_NO_OF_YRS);

         V_YR_COUNT := V_MIN_YEAR;
         P ('V_YR_COUNT ' || V_YR_COUNT);

         J := 1;


         IF (P_REPORT_TYPE = 'M') THEN
            V_TOTAL_INTERVALS := 12 * V_NO_OF_YRS;

            FOR I IN 1 .. V_NO_OF_YRS
            LOOP
               V_INTERVAL_ARRAY.EXTEND (12);
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 1) :=
                  'January, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 2) :=
                  'February, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 3) := 'March, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 4) := 'April, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 5) := 'May, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 6) := 'June, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 7) := 'July, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 8) :=
                  'August, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 9) :=
                  'September, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 10) :=
                  'October, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 11) :=
                  'November, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 12) :=
                  'December, ' || V_YR_COUNT;
               V_YR_COUNT := V_YR_COUNT + 1;
            END LOOP;
         END IF;


         IF (P_REPORT_TYPE = 'Q') THEN
            V_TOTAL_INTERVALS := 4 * V_NO_OF_YRS;


            FOR I IN 1 .. V_NO_OF_YRS
            LOOP
               V_INTERVAL_ARRAY.EXTEND (4);
               V_INTERVAL_ARRAY ((4 * (I - 1)) + 1) := 'Q1, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((4 * (I - 1)) + 2) := 'Q2, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((4 * (I - 1)) + 3) := 'Q3, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((4 * (I - 1)) + 4) := 'Q4, ' || V_YR_COUNT;
               V_YR_COUNT := V_YR_COUNT + 1;
            END LOOP;
         END IF;


         IF (P_REPORT_TYPE = 'Y') THEN
            V_TOTAL_INTERVALS := V_NO_OF_YRS;


            FOR I IN 1 .. V_NO_OF_YRS
            LOOP
               V_INTERVAL_ARRAY.EXTEND;
               V_INTERVAL_ARRAY (I) := 'Year ' || V_YR_COUNT;
               V_YR_COUNT := V_YR_COUNT + 1;
            END LOOP;
         END IF;
         V_INTERVAL_ARRAY.EXTEND;

         V_VISIT_CNT_ARRAY.EXTEND;
         V_EVENT_CNT_ARRAY.EXTEND;
         V_PATIENT_CNT_ARRAY.EXTEND;
         V_VISIT_AMT_ARRAY.EXTEND;
         V_EVENT_AMT_ARRAY.EXTEND;
         V_PATIENT_AMT_ARRAY.EXTEND;
         V_TOTAL_AMT_ARRAY.EXTEND;
         V_PYMT_AMT_ARRAY.EXTEND;
         V_PYMT_DISCREPANCY_ARRAY.EXTEND;

         V_VISIT_CNT_ARRAY (1) := 0;
         V_EVENT_CNT_ARRAY (1) := 0;
         V_PATIENT_CNT_ARRAY (1) := 0;
         V_VISIT_AMT_ARRAY (1) := 0;
         V_EVENT_AMT_ARRAY (1) := 0;
         V_PATIENT_AMT_ARRAY (1) := 0;
         V_TOTAL_AMT_ARRAY (1) := 0;
         V_PYMT_AMT_ARRAY (1) := 0;
         V_PYMT_DISCREPANCY_ARRAY (1) := 0;
--p('v_total_intervals 2:' ||v_total_intervals);
         V_VISIT_CNT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_EVENT_CNT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_PATIENT_CNT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_VISIT_AMT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_EVENT_AMT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_PATIENT_AMT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_TOTAL_AMT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_PYMT_AMT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_PYMT_DISCREPANCY_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);


         IF (P_REPORT_TYPE = 'M') THEN
            FOR I IN 1 .. V_COUNT
            LOOP

               V_RELATIVE_INTERVAL :=
                  (12 *
                      (TO_NUMBER (SUBSTR (V_ACHIVEMENT_DATE_ARRAY (I), 7, 4)) -
                       V_MIN_YEAR
                      )
                  ) +
                  TO_NUMBER (SUBSTR (V_ACHIVEMENT_DATE_ARRAY (I), 1, 2));
--p('V_RELATIVE_INTERVAL 0: ' ||V_RELATIVE_INTERVAL);
               IF (V_MILESTONE_TYPE_ARRAY (I) = 'PM') THEN
                  V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;
--p('V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) ' ||V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) );
               IF (V_MILESTONE_TYPE_ARRAY (I) = 'EM') THEN
                  V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;
--p('V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) ' ||V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL));
               IF (V_MILESTONE_TYPE_ARRAY (I) = 'VM') THEN
                  V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;
--p('V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) ' ||V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL));
               V_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                  V_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                  TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
            END LOOP;
         END IF;


         IF (P_REPORT_TYPE = 'Q') THEN
            FOR I IN 1 .. V_COUNT
            LOOP
               V_RELATIVE_INTERVAL :=
                  (4 *
                      (TO_NUMBER (SUBSTR (V_ACHIVEMENT_DATE_ARRAY (I), 7, 4)) -
                       V_MIN_YEAR
                      )
                  ) +
                  (TO_NUMBER (
                      SUBSTR (
                         TO_CHAR (
                            (TO_NUMBER (
                                SUBSTR (V_ACHIVEMENT_DATE_ARRAY (I), 1, 2)
                             ) -
                             1
                            ) /
                               3,
                            '0.0'
                         ),
                         2,
                         1
                      )
                   ) +
                   1
                  );


               IF (V_MILESTONE_TYPE_ARRAY (I) = 'PM') THEN
                  V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;


               IF (V_MILESTONE_TYPE_ARRAY (I) = 'EM') THEN
                  V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;


               IF (V_MILESTONE_TYPE_ARRAY (I) = 'VM') THEN
                  V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;

               V_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                  V_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                  TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
            END LOOP;
         END IF;


         IF (P_REPORT_TYPE = 'Y') THEN
            FOR I IN 1 .. V_COUNT
            LOOP
               V_RELATIVE_INTERVAL :=
                  TO_NUMBER (SUBSTR (V_ACHIVEMENT_DATE_ARRAY (I), 7, 4)) -
                  V_MIN_YEAR +
                  1;


               IF (V_MILESTONE_TYPE_ARRAY (I) = 'PM') THEN
                  V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;


               IF (V_MILESTONE_TYPE_ARRAY (I) = 'EM') THEN
                  V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;


               IF (V_MILESTONE_TYPE_ARRAY (I) = 'VM') THEN
                  V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;

               V_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                  V_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                  TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
            END LOOP;
         END IF;

         SP_MILEPAYMENT (
            P_STUDY,
            V_AFT_ACT_STDATE_STRING,
            V_AFT_ACT_ENDDATE_STRING,
            V_COUNT,
            V_PMT_AMTS,
            V_PMT_DATES
         );
--p('414 V_COUNT' || V_COUNT);
         IF (P_REPORT_TYPE = 'M') THEN
            FOR I IN 1 .. V_COUNT
            LOOP
--p('V_PMT_DATES (I)' || V_PMT_DATES (I));
--p('V_MIN_YEAR' || V_MIN_YEAR);
               V_RELATIVE_INTERVAL :=
                  (12 *
                      (TO_NUMBER (
                          SUBSTR (
                             V_PMT_DATES (
                                I
                             ),
                             7,
                             4
                          )
                       ) -
                       V_MIN_YEAR
                      )
                  ) +
                  TO_NUMBER (
                     SUBSTR (
                        V_PMT_DATES (
                           I
                        ),
                        1,
                        2
                     )
                  );
--p('443 V_RELATIVE_INTERVAL' || V_RELATIVE_INTERVAL);
--p('443 V_PYMT_AMT_ARRAY' || V_PYMT_AMT_ARRAY.count);
               V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                  V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                  TO_NUMBER (V_PMT_AMTS (I));
            END LOOP;
         END IF;


         IF (P_REPORT_TYPE = 'Q') THEN
            FOR I IN 1 .. V_COUNT
            LOOP
               V_RELATIVE_INTERVAL :=
                  (4 *
                      (TO_NUMBER (
                          SUBSTR (
                             V_PMT_DATES (
                                I
                             ),
                             7,
                             4
                          )
                       ) -
                       V_MIN_YEAR
                      )
                  ) +
                  (TO_NUMBER (
                      SUBSTR (
                         TO_CHAR (
                            (TO_NUMBER (
                                SUBSTR (
                                   V_PMT_DATES (
                                      I
                                   ),
                                   1,
                                   2
                                )
                             ) -
                             1
                            ) /
                               3,
                            '0.0'
                         ),
                         2,
                         1
                      )
                   ) +
                   1
                  );
               V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                  V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                  TO_NUMBER (V_PMT_AMTS (I));
            END LOOP;
         END IF;


         IF (P_REPORT_TYPE = 'Y') THEN
            FOR I IN 1 .. V_COUNT
            LOOP
               V_RELATIVE_INTERVAL :=
                  TO_NUMBER (SUBSTR (V_PMT_DATES (I), 7, 4)) - V_MIN_YEAR + 1;
               V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                  V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                  TO_NUMBER (V_PMT_AMTS (I));
            END LOOP;
         END IF;


         J := V_TOTAL_INTERVALS + 1;

         V_INTERVAL_ARRAY (J) := 'Total';
--p('V_TOTAL_INTERVALS 3: '||V_TOTAL_INTERVALS);
         FOR I IN 1 .. V_TOTAL_INTERVALS
         LOOP
            V_VISIT_CNT_ARRAY (J) :=
               V_VISIT_CNT_ARRAY (J) + V_VISIT_CNT_ARRAY (I);
            V_EVENT_CNT_ARRAY (J) :=
               V_EVENT_CNT_ARRAY (J) + V_EVENT_CNT_ARRAY (I);
            V_PATIENT_CNT_ARRAY (J) :=
               V_PATIENT_CNT_ARRAY (J) + V_PATIENT_CNT_ARRAY (I);
            V_VISIT_AMT_ARRAY (J) :=
               V_VISIT_AMT_ARRAY (J) + V_VISIT_AMT_ARRAY (I);
            V_EVENT_AMT_ARRAY (J) :=
               V_EVENT_AMT_ARRAY (J) + V_EVENT_AMT_ARRAY (I);
            V_PATIENT_AMT_ARRAY (J) :=
               V_PATIENT_AMT_ARRAY (J) + V_PATIENT_AMT_ARRAY (I);
            V_TOTAL_AMT_ARRAY (J) :=
               V_TOTAL_AMT_ARRAY (J) + V_TOTAL_AMT_ARRAY (I);
            V_PYMT_AMT_ARRAY (J) := V_PYMT_AMT_ARRAY (J) + V_PYMT_AMT_ARRAY (I);
            V_PYMT_DISCREPANCY_ARRAY (I) :=
               V_PYMT_AMT_ARRAY (I) - V_TOTAL_AMT_ARRAY (I);
            V_PYMT_DISCREPANCY_ARRAY (J) :=
               V_PYMT_DISCREPANCY_ARRAY (J) + V_PYMT_DISCREPANCY_ARRAY (I);
         END LOOP;


         V_RELATIVE_INTERVAL := J - 1;


         IF (P_REPORT_TYPE = 'M') THEN
            V_RELATIVE_INTERVAL :=
               (12 *
                   (TO_NUMBER (SUBSTR (V_AFT_ACT_ENDDATE_STRING, 7, 4)) -
                    V_MIN_YEAR
                   )
               ) +
               TO_NUMBER (SUBSTR (V_AFT_ACT_ENDDATE_STRING, 1, 2));
         END IF;

         --p('V_RELATIVE_INTERVAL 1: ' ||V_RELATIVE_INTERVAL);
         IF (P_REPORT_TYPE = 'Q') THEN
            V_RELATIVE_INTERVAL :=
               (4 *
                   (TO_NUMBER (SUBSTR (V_AFT_ACT_ENDDATE_STRING, 7, 4)) -
                    V_MIN_YEAR
                   )
               ) +
               (TO_NUMBER (
                   SUBSTR (
                      TO_CHAR (
                         (TO_NUMBER (SUBSTR (V_AFT_ACT_ENDDATE_STRING, 1, 2)) -
                          1
                         ) /
                            3,
                         '0.0'
                      ),
                      2,
                      1
                   )
                ) +
                1
               );
         END IF;


         IF (P_REPORT_TYPE = 'Y') THEN
            V_RELATIVE_INTERVAL :=
               TO_NUMBER (SUBSTR (V_AFT_ACT_ENDDATE_STRING, 7, 4)) -
               V_MIN_YEAR +
               1;
         END IF;


         FOR I IN 1 .. (J - (V_RELATIVE_INTERVAL + 1))
         LOOP
            FOR K IN (V_RELATIVE_INTERVAL + 1) .. (J - 1)
            LOOP
               V_INTERVAL_ARRAY (K) := V_INTERVAL_ARRAY (K + 1);
               V_VISIT_CNT_ARRAY (K) := V_VISIT_CNT_ARRAY (K + 1);
               V_EVENT_CNT_ARRAY (K) := V_EVENT_CNT_ARRAY (K + 1);
               V_PATIENT_CNT_ARRAY (K) := V_PATIENT_CNT_ARRAY (K + 1);
               V_VISIT_AMT_ARRAY (K) := V_VISIT_AMT_ARRAY (K + 1);
               V_EVENT_AMT_ARRAY (K) := V_EVENT_AMT_ARRAY (K + 1);
               V_PATIENT_AMT_ARRAY (K) := V_PATIENT_AMT_ARRAY (K + 1);
               V_TOTAL_AMT_ARRAY (K) := V_TOTAL_AMT_ARRAY (K + 1);
               V_PYMT_AMT_ARRAY (K) := V_PYMT_AMT_ARRAY (K + 1);
               V_PYMT_DISCREPANCY_ARRAY (K) := V_PYMT_DISCREPANCY_ARRAY (K + 1);
            END LOOP;
         END LOOP;


         V_INTERVAL_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_VISIT_CNT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_EVENT_CNT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_PATIENT_CNT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_VISIT_AMT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_EVENT_AMT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_PATIENT_AMT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_TOTAL_AMT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_PYMT_AMT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_PYMT_DISCREPANCY_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_TOTAL_INTERVALS :=
            V_TOTAL_INTERVALS - (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         J := V_TOTAL_INTERVALS + 1;


         IF (P_REPORT_TYPE = 'M') THEN
            V_RELATIVE_INTERVAL :=
               (12 *
                   (V_MIN_YEAR -
                    TO_NUMBER (SUBSTR (V_AFT_ACT_STDATE_STRING, 7, 4))
                   )
               ) +
               TO_NUMBER (SUBSTR (V_AFT_ACT_STDATE_STRING, 1, 2));
         END IF;

         IF (P_REPORT_TYPE = 'Q') THEN
            V_RELATIVE_INTERVAL :=
               (4 *
                   (V_MIN_YEAR -
                    TO_NUMBER (
                       SUBSTR (
                          V_AFT_ACT_STDATE_STRING,
                          7,
                          4
                       )
                    )
                   )
               ) +
               (TO_NUMBER (
                   SUBSTR (
                      TO_CHAR (
                         (TO_NUMBER (
                             SUBSTR (
                                V_AFT_ACT_STDATE_STRING,
                                1,
                                2
                             )
                          ) -
                          1
                         ) /
                            3,
                         '0.0'
                      ),
                      2,
                      1
                   )
                ) +
                1
               );
         END IF;


         IF (P_REPORT_TYPE = 'Y') THEN
            V_RELATIVE_INTERVAL :=
               V_MIN_YEAR -
               TO_NUMBER (SUBSTR (V_AFT_ACT_STDATE_STRING, 7, 4)) +
               1;
         END IF;


         FOR I IN 1 .. (V_RELATIVE_INTERVAL - 1)
         LOOP
            FOR K IN 1 .. (J - 1)
            LOOP
               V_INTERVAL_ARRAY (K) := V_INTERVAL_ARRAY (K + 1);
               V_VISIT_CNT_ARRAY (K) := V_VISIT_CNT_ARRAY (K + 1);
               V_EVENT_CNT_ARRAY (K) := V_EVENT_CNT_ARRAY (K + 1);
               V_PATIENT_CNT_ARRAY (K) := V_PATIENT_CNT_ARRAY (K + 1);
               V_VISIT_AMT_ARRAY (K) := V_VISIT_AMT_ARRAY (K + 1);
               V_EVENT_AMT_ARRAY (K) := V_EVENT_AMT_ARRAY (K + 1);
               V_PATIENT_AMT_ARRAY (K) := V_PATIENT_AMT_ARRAY (K + 1);
               V_TOTAL_AMT_ARRAY (K) := V_TOTAL_AMT_ARRAY (K + 1);
               V_PYMT_AMT_ARRAY (K) := V_PYMT_AMT_ARRAY (K + 1);
               V_PYMT_DISCREPANCY_ARRAY (K) := V_PYMT_DISCREPANCY_ARRAY (K + 1);
            END LOOP;
         END LOOP;


         V_INTERVAL_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_VISIT_CNT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_EVENT_CNT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_PATIENT_CNT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_VISIT_AMT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_EVENT_AMT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_PATIENT_AMT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_TOTAL_AMT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_PYMT_AMT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_PYMT_DISCREPANCY_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);

         V_TOTAL_INTERVALS := V_TOTAL_INTERVALS - (V_RELATIVE_INTERVAL - 1);
         J := J - (V_RELATIVE_INTERVAL - 1);
      END IF;


      DBMS_LOB.CREATETEMPORARY (O_XML_CLOB, FALSE);
      DBMS_LOB.OPEN (O_XML_CLOB, DBMS_LOB.LOB_READWRITE);
      V_TEMPVAR := '<?xml version="1.0"?><ROWSET>';
      DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
      V_TEMPVAR := '<STUDY><STUDYNUM>' ||
                   V_STUDY_NUMBER ||
                   '</STUDYNUM><STUDYVER>' ||
                   V_STUDY_VERSION ||
                   '</STUDYVER><STUDYTITLE>' ||
                   V_STUDY_TITLE ||
                   '</STUDYTITLE><ACTIVATIONDATE>' ||
                   V_ACT_DATE_STRING ||
                   '</ACTIVATIONDATE><STUDYCURRENCY>' ||
                   V_STUDY_CURRENCY ||
                   '</STUDYCURRENCY></STUDY>';
      DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
--p('728 V_BEF_ACT_STDATE_STRING' || V_BEF_ACT_STDATE_STRING);
--p('729 V_BEF_ACT_ENDDATE_STRING' || V_BEF_ACT_ENDDATE_STRING);
     IF(V_BEF_ACT_ENDDATE_STRING <> ' ') THEN
      SP_PYMTS_BEFORE_STDY_ACT (
         P_STUDY,
         V_BEF_ACT_STDATE_STRING,
         V_BEF_ACT_ENDDATE_STRING,
         P_REPORT_TYPE,
         TEMP_CLOB
      );
      DBMS_LOB.COPY (
         O_XML_CLOB,
         TEMP_CLOB,
         DBMS_LOB.GETLENGTH (TEMP_CLOB),
         DBMS_LOB.GETLENGTH (O_XML_CLOB) + 1,
         1
      );
      END IF;


      IF (V_AFT_ACT_STDATE_STRING <> ' ') THEN
         V_TEMPVAR := '<INTERVALS>';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);


         FOR I IN 1 .. V_TOTAL_INTERVALS
         LOOP
            V_TEMPVAR := '<ROW num="' ||
                         I ||
                         '"><INTERVAL>' ||
                         V_INTERVAL_ARRAY (
                            I
                         ) ||
                         '</INTERVAL><VCNT>' ||
                         V_VISIT_CNT_ARRAY (
                            I
                         ) ||
                         '</VCNT><ECNT>' ||
                         V_EVENT_CNT_ARRAY (
                            I
                         ) ||
                         '</ECNT><PCNT>' ||
                         V_PATIENT_CNT_ARRAY (
                            I
                         ) ||
                         '</PCNT><VAMT>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_VISIT_AMT_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</VAMT><EAMT>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_EVENT_AMT_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</EAMT><PAMT>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_PATIENT_AMT_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</PAMT><TOTAMT>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_TOTAL_AMT_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</TOTAMT><PYMTAMT>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_PYMT_AMT_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</PYMTAMT><DISCREPANCY>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_PYMT_DISCREPANCY_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</DISCREPANCY></ROW>';
            DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
         END LOOP;


         V_TEMPVAR := '</INTERVALS>';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
         V_TEMPVAR := '<TOTAL><INTERVAL>TOTAL</INTERVAL><VCNT>' ||
                      V_VISIT_CNT_ARRAY (V_TOTAL_INTERVALS + 1) ||
                      '</VCNT><ECNT>' ||
                      V_EVENT_CNT_ARRAY (V_TOTAL_INTERVALS + 1) ||
                      '</ECNT><PCNT>' ||
                      V_PATIENT_CNT_ARRAY (V_TOTAL_INTERVALS + 1) ||
                      '</PCNT><VAMT>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_VISIT_AMT_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</VAMT><EAMT>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_EVENT_AMT_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</EAMT><PAMT>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_PATIENT_AMT_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</PAMT><TOTAMT>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_TOTAL_AMT_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</TOTAMT><PYMTAMT>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_PYMT_AMT_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</PYMTAMT><DISCREPANCY>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_PYMT_DISCREPANCY_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</DISCREPANCY></TOTAL>';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
      END IF;

      V_TEMPVAR := '</ROWSET>';
      DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
      DBMS_LOB.CLOSE (O_XML_CLOB);


   END SP_MS_FINANCIAL_PAYMENT;



END Pkg_Milestone_Reports;
/


CREATE SYNONYM ESCH.PKG_MILESTONE_REPORTS FOR PKG_MILESTONE_REPORTS;


CREATE SYNONYM EPAT.PKG_MILESTONE_REPORTS FOR PKG_MILESTONE_REPORTS;


GRANT EXECUTE, DEBUG ON PKG_MILESTONE_REPORTS TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_MILESTONE_REPORTS TO ESCH;

