CREATE OR REPLACE PACKAGE BODY      PKG_EIRB AS
/******************************************************************************
   NAME:       PKG_EIRB
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        5/28/2009   Sonia Abrol          1. Created this package.
******************************************************************************/


  PROCEDURE sp_checkSubmission(p_study IN NUMBER, p_submission_board in number,
  o_results OUT  Gk_Cv_Types.GenericCursorType,o_pass_submission OUT Number)
  as
    v_logic_description  varchar2(4000);
    v_logic_sql varchar2(4000);
    v_results t_nested_results_table;

  begin

       select   logic_description  ,logic_sql
       into v_logic_description  ,v_logic_sql
       from er_submission_logic where fk_review_board =  p_submission_board
       order  by logic_sequence;

       if (v_logic_sql is not null and length(v_logic_sql) > 0) then
         -- execute the sql and get results
              execute immediate v_logic_sql into v_results  using p_study;

               OPEN  O_RESULTS
                 FOR
                   SELECT *
         FROM TABLE( CAST(v_results AS t_nested_results_table));


       end if;



  end;

PROCEDURE sp_submit_ongoing(p_study IN NUMBER  ,p_lf_submission_type IN VARCHAR2,p_creator in number,p_ipadd in varchar2,
o_ret OUT NUMBER,p_formstatus in number
  )
  as
    v_SUBMISSION_TYPE  number;

    v_submission_count number;

    v_form_status varchar2(20);
    v_account number;
    v_board_pk number;
    v_pk_submission number;
    v_submission_boardpk number;
    v_submitted_code  number;
    v_saved_submission number;

begin
--check for submission
-- get the submission type mapping for the form submission type

    begin
        select pk_codelst
        into v_SUBMISSION_TYPE
        from er_codelst where codelst_type = 'submission' and
         codelst_subtyp in (select CODELST_SUBMISSION_SUBTYPE from er_irbforms_setup
        where FORM_SUBMISSION_SUBTYPE = p_lf_submission_type and IRB_TAB_SUBTYPE = 'irb_ongoing_menu');
        -- create new submission
    exception when no_data_found then
        v_SUBMISSION_TYPE := 0;
    end;


    begin
        select pk_submission into v_saved_submission
        from er_submission s
        where s.fk_study = p_study and SUBMISSION_STATUS is null and submission_flag = 2 and submission_type=v_SUBMISSION_TYPE;

    exception when no_data_found then
        v_saved_submission := 0;
    end;


        if v_SUBMISSION_TYPE > 0 then

         if v_saved_submission = 0 then

            select seq_ER_SUBMISSION.nextval into v_pk_submission from dual;

            Insert into ER_SUBMISSION
               (PK_SUBMISSION, FK_STUDY, SUBMISSION_FLAG, SUBMISSION_TYPE, SUBMISSION_STATUS,
                CREATED_ON, CREATOR, IP_ADD)
             Values
               (v_pk_submission , p_study, 2, v_SUBMISSION_TYPE, NULL,
                    sysdate,p_creator,p_ipadd );


          else
               v_pk_submission := v_saved_submission;

         end if ; --v_saved_submission =0


             begin
                select f_codelst_id('subm_status','submitted')
                into v_submitted_code
                from dual;
             exception when no_data_found then
                v_submitted_code   :=null;
             end;

             begin
                 select nvl(codelst_subtyp,'')
                 into v_form_status
                 from er_codelst where pk_codelst = p_formstatus;
              exception when no_data_found then
                v_form_status :='';
                end;


            if v_form_status = 'complete' then
                --get default board and make submisson to that board for the account

                    select fk_account into     v_account from er_study where pk_study = p_study ;

                    select pk_review_board into v_board_pk  from er_review_board where fk_account = v_account
                    and nvl(REVIEW_BOARD_DEFAULT ,0) = 1;

                    -- create submission for board

                    select seq_ER_SUBMISSION_BOARD.nextval into v_submission_boardpk from dual;

                    Insert into ER_SUBMISSION_BOARD
                    (PK_SUBMISSION_BOARD, FK_SUBMISSION, FK_REVIEW_BOARD,  CREATED_ON, CREATOR, IP_ADD)
                     Values
                       (v_submission_boardpk, v_pk_submission , v_board_pk, sysdate,p_creator,p_ipadd );


                    --insert board status and overall sttaus

                    Insert into ER_SUBMISSION_STATUS
                       (PK_SUBMISSION_STATUS, FK_SUBMISSION, FK_SUBMISSION_BOARD, SUBMISSION_STATUS, SUBMISSION_STATUS_DATE,
                        SUBMISSION_ENTERED_BY, IS_CURRENT,  CREATED_ON, CREATOR,  IP_ADD)
                     Values
                       (seq_ER_SUBMISSION_STATUS.nextval, v_pk_submission, NULL, v_submitted_code   ,sysdate,
                        p_creator,  1, sysdate, p_creator, p_ipadd);

                            Insert into ER_SUBMISSION_STATUS
                       (PK_SUBMISSION_STATUS, FK_SUBMISSION, FK_SUBMISSION_BOARD, SUBMISSION_STATUS, SUBMISSION_STATUS_DATE,
                        SUBMISSION_ENTERED_BY, IS_CURRENT,  CREATED_ON, CREATOR,  IP_ADD)
                     Values
                       (seq_ER_SUBMISSION_STATUS.nextval, v_pk_submission, v_submission_boardpk, v_submitted_code   ,sysdate,
                        p_creator,  1, sysdate, p_creator, p_ipadd);

                    update er_submission
                    set SUBMISSION_STATUS = v_submitted_code ,last_modified_by =p_creator ,ip_add =p_ipadd ,last_modified_date = sysdate
                    where pK_SUBMISSION=v_pk_submission ;

            end if;

        end if; -- for v_SUBMISSION_TYPE > 0
        o_ret := 0;
  end;

END PKG_EIRB;
/


