create or replace PACKAGE BODY      Pkg_Formrep AS
    PROCEDURE SP_GENFORMLAYOUT(P_FORM IN NUMBER)
	AS
	  v_formtype_disp VARCHAR2(5);
  	  v_formtype VARCHAR2(5);
	  v_success NUMBER;
	  v_html CLOB;
	  BEGIN
		   	--delete existing data , if any Sonia - 7 apr 04

			-- sonia 7th july, do not delete old data, we will update it while generating mapform.

			--DELETE FROM
			--ER_MAPFORM
			--WHERE fk_form = p_form;



			SELECT  trim(LF_DISPLAYTYPE)
			INTO  v_formtype_disp
			FROM ER_LINKEDFORMS
			WHERE fk_formlib = p_form;
			-- call PKG_FORM.SP_CLUBFORMXSL to generate form XML and XSL
			Pkg_Form.SP_CLUBFORMXSL(p_form, v_html);
			IF v_formtype_disp = 'S' OR v_formtype_disp = 'SA' THEN
			    v_formtype := 'S';
			ELSIF  v_formtype_disp = 'SP' OR v_formtype_disp = 'PA' OR v_formtype_disp = 'PS' OR v_formtype_disp = 'PR'  THEN
			    v_formtype := 'P';
			ELSE
				v_formtype := v_formtype_disp;
			END IF;
			SP_GENFORMLAYOUT(P_FORM, v_formtype, V_SUCCESS);
	  END;
	PROCEDURE SP_GENFORMLAYOUT(P_FORM IN NUMBER, P_FORMTYPE IN VARCHAR2, O_SUCCESS OUT VARCHAR2)
	AS
	  v_orig_count NUMBER;
	  v_rep_count NUMBER;
	  v_tot NUMBER;
      i NUMBER;
	  v_name VARCHAR2(2000);
	  v_keyword VARCHAR2(500);
	  v_uid VARCHAR2(250);
	  v_browser VARCHAR2(2);
	  v_type VARCHAR2(2);
	  v_origsysid VARCHAR2(50);
	  v_systemid VARCHAR2(50);
	  v_pkfld NUMBER;
	  v_fldcharsno NUMBER;
	  o_html CLOB;
	  v_formxml sys.XMLTYPE;
	  v_mapcolname VARCHAR(100);
	  v_pksec NUMBER;
	  v_secname VARCHAR2(250);
	  v_count NUMBER;
	  v_fldseq NUMBER;
	  v_prev_mapformcount NUMBER;
	  v_repeatflag  NUMBER;
	  v_insert BOOLEAN ;
	  v_fld_in_mapform NUMBER;
	  v_newver_counter NUMBER;
      v_sysid_count NUMBER;

	BEGIN
    --get the form xml
	SELECT form_xml
	INTO v_formxml
	FROM ER_FORMLIB
	WHERE pk_formlib = p_form;
	-- implement null check , sonia
	IF v_formxml IS NOT NULL THEN
		 -- find total number of fields in XMl
	 	SELECT COUNT(*)
		INTO v_orig_count
		FROM erv_formflds
		WHERE  pk_formlib = p_form AND FLD_TYPE <> 'C' AND FLD_TYPE  <> 'H' ;

		SELECT COUNT(*)
		INTO v_rep_count
		FROM ER_REPFORMFLD, ER_FORMSEC, ER_FLDLIB
		WHERE ER_FORMSEC.fk_formlib = p_form AND
		ER_REPFORMFLD.fk_formsec = pk_formsec AND
		NVL(ER_REPFORMFLD.record_type,'D') <> 'D' AND
		ER_FLDLIB.pk_field = fk_field AND FLD_TYPE <> 'C' AND FLD_TYPE  <> 'H' ;
	 	v_tot := v_rep_count +  v_orig_count;

		-- get v_prev_mapformcount

		SELECT COUNT(*)
		INTO v_prev_mapformcount
		FROM ER_MAPFORM
		WHERE fk_form = p_form;



		--insert into t values('v_tot' || v_tot);
		-- call  PKG_FORM.SP_CLUBFORMXSL to refresh form xsl/xml
		--(dont need it if u execute from process data :
		--PKG_FORM.SP_CLUBFORMXSL(p_form, o_html);
		 v_count := 0;
		 v_newver_counter := v_prev_mapformcount;

			-- iterate throughthe xml
		    FOR i IN 1..v_tot
		      LOOP
			   --substr(sys.xmlType.getStringVal(sys.xmlType.extract(v_formxml,'//rowset/*[' || i || ']/@name')),1,200),
			   --substr(sys.xmlType.getStringVal(sys.xmlType.extract(v_formxml,'//rowset/*[' || i || ']/@keyword')),1,255),
		   	   --substr(sys.xmlType.getStringVal(sys.xmlType.extract(v_formxml,'//rowset/*[' || i || ']/@uid')),1,50),
		   	   --substr(sys.xmlType.getStringVal(sys.xmlType.extract(v_formxml,'//rowset/*[' || i || ']/@browser')),1,2),
		   	   --substr(sys.xmlType.getStringVal(sys.xmlType.extract(v_formxml,'//rowset/*[' || i || ']/@type')),1,2),
		   	   --substr(sys.xmlType.getStringVal(sys.xmlType.extract(v_formxml,'//rowset/*[' || i || ']/@origsysid')),1,50),
		   	   --substr(sys.xmlType.getStringVal(sys.xmlType.extract(v_formxml,'//rowset/*[' || i || ']/@systemid')),1,50)
			   SELECT
			   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@name').getStringVal(),
			   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@keyword').getStringVal(),
		  	   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@uid').getStringVal(),
		   	   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@browser').getStringVal(),
		   	   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@type').getStringVal(),
		   	   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@origsysid').getStringVal(),
		   	   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@systemid').getStringVal(),
		   	   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@pksec').getStringVal(),
		   	   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@secname').getStringVal(),
			   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@pkfld').getStringVal(),
			   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@fldcharsno').getStringVal()
		   	   INTO  v_name, v_keyword, v_uid, v_browser,v_type,v_origsysid,v_systemid,v_pksec,v_secname,v_pkfld,v_fldcharsno
			   FROM dual;

			   --added by sonia, 10/01/04 to un escape special characters in section name
			   v_secname := Pkg_Util.f_unescapeSpecialCharsForXML(v_secname);
			   v_name:=Pkg_Util.f_unescapeSpecialCharsForXML(v_name);
			   v_keyword:=Pkg_Util.f_unescapeSpecialCharsForXML(v_keyword);
			   v_uid:=Pkg_Util.f_unescapeSpecialCharsForXML(v_uid);



			   IF v_type <> 'ML' THEN

			  	  v_insert := FALSE;

			     IF v_prev_mapformcount <= 0 THEN
  				   v_count := v_count + 1;
				   v_mapcolname := 'col' || v_count;
				   v_insert := TRUE;
				  ELSE -- check if the field is new for this version or if it already exists

				  	   SELECT  COUNT(*)
					   INTO v_fld_in_mapform FROM ER_MAPFORM
					   WHERE fk_form = p_form AND MP_PKFLD = v_pkfld;

					   --
					   IF v_fld_in_mapform <= 0 THEN
					       	v_insert := TRUE; -- set insert flag
							v_newver_counter :=  v_newver_counter + 1;
		  				    v_count := v_newver_counter;
							v_mapcolname := 'col' || v_newver_counter;
						ELSE
						   	v_insert := FALSE; -- field exits, do not change sequence, col name just change  other values
					   END IF;


			     END IF;

				 -- get the sequence numbers for the fields, update to mapform

				  BEGIN
	  				 SELECT formfld_seq INTO v_fldseq FROM ER_FORMFLD WHERE fk_field=v_pkfld;
				  EXCEPTION WHEN NO_DATA_FOUND THEN
				  SELECT repformfld_seq INTO v_fldseq FROM ER_REPFORMFLD WHERE fk_field=v_pkfld;
				  END;



                -- changed 06/10/08 for eliminating duplicates in er_mapform

				 IF v_insert = TRUE THEN
                    Plog.DEBUG(pCTX,'p_form='|| p_form||',v_systemid='||v_systemid);
                   select count(*) into v_sysid_count from er_mapform where mp_systemid=v_systemid and fk_form=p_form;
                    Plog.DEBUG(pCTX,'v_sysid_count='||v_sysid_count);
                   IF v_sysid_count=0 THEN
                    Plog.DEBUG(pCTX,'inserting');

				    INSERT INTO ER_MAPFORM  (PK_MP,
				   		  	   			    FK_FORM,
											MP_FORMTYPE,
											MP_MAPCOLNAME,
											MP_SYSTEMID,
											MP_KEYWORD,
											MP_UID,
											MP_DISPNAME,
											MP_ORIGSYSID,
											MP_FLDDATATYPE,
											MP_BROWSER,MP_SEQUENCE,MP_PKSEC,MP_SECNAME,MP_PKFLD,MP_FLDCHARSNO,MP_FLDSEQNO)
				   VALUES (SEQ_ER_MAPFORM.NEXTVAL,p_form,p_formtype,
				   v_mapcolname ,
				   v_systemid,v_keyword,v_uid,v_name,v_origsysid,
				   v_type,TO_CHAR(v_browser),v_count,v_pksec,v_secname,v_pkfld,v_fldcharsno,v_fldseq);
                   COMMIT;
                  END IF;
				 ELSE
                 Plog.DEBUG(pCTX,'updating');
				 	 -- update the old record
					UPDATE ER_MAPFORM
					SET  MP_FORMTYPE = p_formtype,MP_SYSTEMID = v_systemid,	MP_KEYWORD =v_keyword,
						MP_UID = v_uid,	MP_DISPNAME =v_name ,MP_ORIGSYSID = v_origsysid,
						MP_FLDDATATYPE = v_type, MP_BROWSER = TO_CHAR(v_browser),
						MP_PKSEC = v_pksec,
						MP_SECNAME = v_secname,
						MP_FLDCHARSNO = v_fldcharsno,
						MP_FLDSEQNO=v_fldseq
						WHERE 	FK_form = p_form AND
					MP_PKFLD = v_pkfld ;

                    COMMIT;

				 END IF; -- for v_insert = true


			 END IF;
		    END LOOP;

	 ELSE
		 P('NULL XML: Record not processed');
	 END IF; -- end of null check
 END;

 PROCEDURE Sp_Processformdata
  AS
  	v_oldxml sys.XMLTYPE;
	v_newxml sys.XMLTYPE;
	v_filledformpk NUMBER;
	v_form NUMBER;
	v_recordtype CHAR(1);
	v_fftdate DATE;
	v_filledformtype VARCHAR2(10);
	v_creator NUMBER;
	v_lastmodified_by NUMBER;
	v_created_on DATE;
	v_lastmodified_date DATE;
	v_ipadd VARCHAR2(15);
	v_rep_count NUMBER;
	v_orig_count NUMBER;
	v_tot NUMBER;
	v_fldvalue_prefix VARCHAR2(4000);
	v_fldvalue_suffix VARCHAR2(4000);
	v_fldvalue VARCHAR2(32000);
	v_fldvaluedata_prefix VARCHAR2(4000);
	v_fldvaluedata_suffix VARCHAR2(4000);
	v_fldvaluedata VARCHAR2(32000);
	v_ins_sqlstr LONG;
	v_ins_sqlstrdata LONG;
	v_values_sqlstr  VARCHAR2(32000);
	v_values_sqlstrdata  VARCHAR2(32000);
	o_rows NUMBER;
	v_ins_sql  LONG;
	v_ins_sqldata LONG;
	v_linpk NUMBER;
	v_fld_datatype VARCHAR(2);
	v_repcount NUMBER;
	v_respval VARCHAR2(32000);
	v_respdataval VARCHAR2(32000);
	v_mapcount NUMBER;
	o_layout VARCHAR2(100);
	v_pk_fft NUMBER;
	v_id NUMBER;
	v_fk_patprot NUMBER;
	-- for audit taril
v_old_fld_value_prefix VARCHAR2(4000);
	v_old_fld_value_suffix VARCHAR2(4000);
	v_old_fld_value VARCHAR2(32000);
	v_old_fld_valuedata VARCHAR2(32000);
	v_old_respval VARCHAR2(32000);
	v_old_respvaldata VARCHAR2(32000);
	v_old_repcount NUMBER;
	v_fld_systemid VARCHAR2(100);
	v_fld_name VARCHAR2(2000);
	v_modified_by_name VARCHAR2(1000);
	v_fld_count NUMBER;
	v_fldval_for_audit VARCHAR2(32000);

	v_mapform_col VARCHAR2(100);
	v_pkfld NUMBER;

	v_formorigxml sys.XMLTYPE;
	v_form_completed NUMBER;

    v_sysid_orig VARCHAR2(100);

	v_wip NUMBER;

    l_stmt          DBMS_SQL.VARCHAR2S;
    l_cursor        INTEGER  ;
	v_len NUMBER;

	V_COLNAMES            Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
    V_COLVALUES        Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
	V_DUMMYVAL NUMBER;

--	  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_FORMREP_TEMP', pLEVEL  => Plog.LDEBUG);

	BEGIN
	  --change the record_type for this record to 'E'' - for exported
--	  update er_filledformtran
--	  set fft_recordtype  = 'E';
		 --get distinct forms

		 Plog.DEBUG(pCTX,'starting sp_processformdata');

		 SELECT pk_codelst INTO v_wip FROM ER_CODELST WHERE codelst_type = 'fillformstat' AND codelst_subtyp = 'working';

		  FOR j IN (  SELECT DISTINCT FK_FORM, FFT_TYPE,FK_FORMLIBVER FROM ER_FILLEDFORMTRAN )
		  	LOOP
		   		   v_form := j.fk_form;

				   BEGIN

				    Plog.DEBUG(pCTX,'v_form' || v_form||'V_formlibver'||j.fk_formlibver);
				   --get original xml
				   SELECT formlibver_xml
				   INTO    v_formorigxml
				   FROM ER_FORMLIBVER
				   WHERE pk_formlibver = j.fk_formlibver AND fk_formlib = v_form;



				   --see if there is a form layout already created
				   SELECT NVL(COUNT(*),0)
				   INTO v_mapcount
				   FROM ER_MAPFORM
				   WHERE fk_form = v_form;
				   -- if there is no layout, create one
				   IF v_mapcount = 0 THEN
	   			   		 Plog.DEBUG(pCTX,'have to generate mapping');
	   				   Pkg_Formrep.SP_GENFORMLAYOUT(v_form, j.FFT_TYPE, o_layout);
	   	   			   		 Plog.DEBUG(pCTX,'generated mapping');
					END IF;
				   --get field infor for he form
				    SELECT COUNT(*)
					INTO v_orig_count
					FROM erv_formflds
					WHERE  pk_formlib = v_form ;
					SELECT COUNT(*)
					INTO v_rep_count
					FROM ER_REPFORMFLD, ER_FORMSEC
					WHERE ER_FORMSEC.fk_formlib = v_form AND
					ER_REPFORMFLD.fk_formsec = pk_formsec AND
					NVL(ER_REPFORMFLD.record_type,'D') <> 'D';
			   		 Plog.DEBUG(pCTX,'v_rep_count' || v_rep_count || 'v_orig_count' || v_orig_count);

					v_tot := v_rep_count +  v_orig_count;
			   		 Plog.DEBUG(pCTX,'v_tot' || v_tot);
		   		  		 -- get data from er_filledformtran

						 --changed by sonia, 11/02/04, added where clause for formlibver.

						  FOR i IN (  SELECT pk_fft,FK_FILLEDFORM, FK_FORM,  FFT_DATE, FFT_TYPE,  FFT_RECORDTYPE,
									  FFT_OLDXML, FFT_NEWXML, CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD,ID,fk_patprot,
									  form_completed
									  FROM ER_FILLEDFORMTRAN WHERE fk_form = v_form
									  AND fk_formlibver = j.fk_formlibver
									  ORDER BY fft_recordtype DESC,pk_fft ASC)
						  LOOP
						  BEGIN
		  						 V_COLNAMES := Types.SMALL_STRING_ARRAY ();
							     V_COLVALUES := Types.SMALL_STRING_ARRAY ();

								 Plog.DEBUG(pCTX,'Sonia : V_COLNAMES.count' || V_COLNAMES.COUNT);
 								 Plog.DEBUG(pCTX,'Sonia : V_COLVALUES.count' || V_COLVALUES.COUNT);

					  	  	   v_pk_fft := 	  i.pk_fft;

							  Plog.DEBUG(pCTX,' v_pk_fft .. ' ||   v_pk_fft );

							   v_filledformpk := i.FK_FILLEDFORM;

							  plog.DEBUG(pctx,'v_filledformpk .. ' ||   v_filledformpk );

							   v_fftdate := i.FFT_DATE;
							   v_filledformtype := i.fft_type;
							   v_recordtype := i.FFT_RECORDTYPE;
							   v_oldxml := i.FFT_OLDXML;
							   v_newxml := i.FFT_NEWXML;
							   v_creator := i.creator;
							   v_lastmodified_by :=  i.last_modified_by;

							   v_lastmodified_date :=  TO_DATE(TO_CHAR(i.last_modified_date,PKG_DATEUTIL.f_get_datetimeformat),PKG_DATEUTIL.f_get_datetimeformat);
							   v_created_on := TO_DATE(TO_CHAR(i.created_on,PKG_DATEUTIL.f_get_datetimeformat),PKG_DATEUTIL.f_get_datetimeformat);

							   v_ipadd := i.ip_Add;
							   v_id := i.ID;
							   v_fk_patprot := i.fk_patprot;

							   v_form_completed := i.form_completed;

							   IF v_form_completed IS NULL THEN
									   	 v_form_completed := v_wip;
								END IF;


							   SELECT seq_er_formslinear.NEXTVAL
							   INTO v_linpk
							   FROM dual;
								--null check
								IF v_newxml IS NOT NULL THEN
								   IF (v_recordtype = 'M' AND v_oldxml IS NOT NULL) OR (v_recordtype = 'N') THEN
									   IF v_recordtype = 'M' THEN --delete existing record from er_formslinear for this filled form
									   	  DELETE FROM ER_FORMSLINEAR
									   	  WHERE  FK_FILLEDFORM =  v_filledformpk AND FORM_TYPE = v_filledformtype;
										  BEGIN
										      v_modified_by_name := Getuser(v_lastmodified_by);
										      EXCEPTION WHEN NO_DATA_FOUND THEN
										      v_modified_by_name := 'New User' ;
										  END ;
									   END IF;




	   						   	       v_ins_sqlstr := 'INSERT INTO er_formslinear(pk_formslinear,form_completed,created_on,fk_filledform,fk_form,filldate,form_type,export_flag,id,fk_patprot,creator,last_modified_by,last_modified_date ';

									   	  v_values_sqlstr := ' VALUES (:pk_formslinear,:form_completed,:created_on,:fk_filledform,:fk_form,:filldate,:form_type,:export_flag,:id,:fk_patprot ,:creator,:last_modified_by,:last_modified_date' ;


									   --loop through xml and get filled data
									   v_fld_count := 0;
								   		 Plog.DEBUG(pCTX,'v_fld_count' || v_fld_count);
 								   		 Plog.DEBUG(pCTX,'v_tot' || v_tot);
									    FOR k IN 1..v_tot
										    LOOP



											v_fldval_for_audit := '';

											--changed by sonia, 09/08/04, read the attribute from blank xml instead of filledform xml
											--changed by Sonia Abrol, 08/17/05, get system id from blank XML and use this system id to get data from filled form
											SELECT SUBSTR(sys.XMLTYPE.getStringVal(sys.XMLTYPE.EXTRACT(v_formorigxml,'//rowset/*[' || k || ']/@type')),1,3),
										     sys.XMLTYPE.getNumberVal(sys.XMLTYPE.EXTRACT(v_formorigxml,'//rowset/*[' || k || ']/@pkfld')),
											   SUBSTR(sys.XMLTYPE.getStringVal(sys.XMLTYPE.EXTRACT(v_formorigxml,'//rowset/*[' || k || ']/@systemid')),1,50)
											INTO v_fld_datatype, v_pkfld,v_sysid_orig
											FROM dual;


										 IF v_fld_datatype <> 'ML' THEN
										 	   v_fld_count := v_fld_count + 1;
											   v_fldvalue := '';
											   v_fldvaluedata:='';
											IF v_fld_datatype = 'MC' THEN
											-- handle case for checkbox :
											   SELECT NVL(sys.XMLTYPE.getNumberVal(sys.XMLTYPE.EXTRACT(v_newxml,'//rowset/' || v_sysid_orig || '/resp[@checked=1][last()]/@respcount')),0)
											   INTO v_repcount
											   FROM dual;
												--v_repcount := 4;
												FOR ct IN 1..v_repcount
												LOOP
													SELECT SUBSTR(sys.XMLTYPE.getStringVal(sys.XMLTYPE.EXTRACT(v_newxml,'//rowset/' || v_sysid_orig || '/resp[@checked=1]['|| ct ||']/@dispval')),1,4000)
													INTO v_respval
													FROM dual;
													SELECT SUBSTR(sys.XMLTYPE.getStringVal(sys.XMLTYPE.EXTRACT(v_newxml,'//rowset/' || v_sysid_orig || '/resp[@checked=1]['|| ct ||']/@dataval')),1,4000)
													INTO v_respdataval
													FROM dual;
													IF LENGTH(trim(NVL(v_respval,''))) > 0 THEN
														v_fldvalue := v_fldvalue || ',[' || v_respval || ']';
														v_fldvaluedata:=v_fldvaluedata || '[VELCOMMA]' || v_respdataval ;
													END IF;
												END LOOP;
												v_fldvalue := SUBSTR(v_fldvalue,2);
												v_fldvaluedata := SUBSTR(v_fldvaluedata,11);
												IF (LENGTH(v_fldvalue)>0) THEN
												   v_fldval_for_audit := v_fldvalue; --set the original checkbox display val for audit trail
												   IF LENGTH( trim(NVL(v_fldvalue,'') )) > 0  THEN --changed by sonia abrol, 04/21/05, the old condition to check if the field value is empty was not working
												      	  v_fldvalue := v_fldvalue ||'[VELSEP1]'||v_fldvaluedata;
												END IF;
												   --v_fldvalue:=nvl(v_fldvalue,v_fldvalue||'[VELSEP1]'||v_fldvaluedata);
												END IF;
												IF v_recordtype = 'M' THEN
												   v_old_fld_value := '';
												   v_old_fld_valuedata := '';
													SELECT NVL(sys.XMLTYPE.getNumberVal(sys.XMLTYPE.EXTRACT(v_oldxml,'//rowset/' || v_sysid_orig || '/resp[@checked=1][last()]/@respcount')),0)
											   		INTO v_old_repcount
													FROM dual;
													FOR ct IN 1..v_repcount
													LOOP
															SELECT SUBSTR(sys.XMLTYPE.getStringVal(sys.XMLTYPE.EXTRACT(v_oldxml,'//rowset/' || v_sysid_orig || '/resp[@checked=1]['|| ct ||']/@dispval')),1,4000)
															INTO v_old_respval
															FROM dual;
															IF LENGTH(trim(NVL(v_old_respval,''))) > 0 THEN
																v_old_fld_value := v_old_fld_value || ',[' || v_old_respval || ']';
															END IF;
													END LOOP;
													v_old_fld_value := SUBSTR(v_old_fld_value,2);
												END IF;
											ELSE

								--Populate v_fldvalue
											   SELECT NVL(DBMS_LOB.SUBSTR(DECODE (v_fld_datatype,
												'MD',v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@selected="1"]/@dispval').getClobVal() ,
												'MR',v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@checked="1"]/@dispval').getClobVal(),
												v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/text()').getClobVal() ),2000,1),'')
												INTO  v_fldvalue_prefix
												FROM dual;
												SELECT NVL(DBMS_LOB.SUBSTR(DECODE (v_fld_datatype,
												'MD',v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@selected="1"]/@dispval').getClobVal() ,
												'MR',v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@checked="1"]/@dispval').getClobVal(),
												v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/text()').getClobVal() ),2000,2001),'')
												INTO  v_fldvalue_suffix
												FROM dual;
												v_fldvalue:=v_fldvalue_prefix||v_fldvalue_suffix;
												--end v_fldvalue populate

												--populate v_fldvaluedata
											   SELECT NVL(DBMS_LOB.SUBSTR(DECODE (v_fld_datatype,
												'MD',v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@selected="1"]/@dataval').getClobVal() ,
												'MR',v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@checked="1"]/@dataval').getClobVal(),
												v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/text()').getClobVal() ),2000,1),'')
												INTO  v_fldvaluedata_prefix
												FROM dual;
												SELECT NVL(DBMS_LOB.SUBSTR(DECODE (v_fld_datatype,
												'MD',v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@selected="1"]/@dataval').getClobVal() ,
												'MR',v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@checked="1"]/@dataval').getClobVal(),
												v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/text()').getClobVal() ),2000,2001),'')
												INTO  v_fldvaluedata_suffix
												FROM dual;
												v_fldvaluedata:=v_fldvaluedata_prefix||v_fldvaluedata_suffix;
												--end populating v_fldvaluedata
												v_fldval_for_audit := v_fldvalue; --set the original display val for audit trail
												IF v_recordtype = 'M' THEN -- for audit trail
												   v_old_fld_value := '';
												   v_old_fld_valuedata:='';
												    SELECT NVL(DBMS_LOB.SUBSTR(DECODE (v_fld_datatype,
													'MD',v_oldxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@selected="1"]/@dispval').getClobVal() ,
													'MR',v_oldxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@checked="1"]/@dispval').getClobVal(),
													v_oldxml.EXTRACT('//rowset/' || v_sysid_orig || '/text()').getClobVal() ),2000,1),'')
													INTO  v_old_fld_value_prefix
													FROM dual;
													  SELECT NVL(DBMS_LOB.SUBSTR(DECODE (v_fld_datatype,
													'MD',v_oldxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@selected="1"]/@dispval').getClobVal() ,
													'MR',v_oldxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@checked="1"]/@dispval').getClobVal(),
													v_oldxml.EXTRACT('//rowset/' || v_sysid_orig || '/text()').getClobVal() ),2000,2001),'')
													INTO  v_old_fld_value_suffix
													FROM dual;
													v_old_fld_value:=v_old_fld_value_prefix||v_old_fld_value_suffix;

													END IF;
											END IF;
												v_fldvalue := REPLACE(v_fldvalue,'''','''''');
												v_fldvaluedata := REPLACE(v_fldvaluedata,'''','''''');
												v_fldval_for_audit := REPLACE(v_fldval_for_audit,'''','''''');

											 Plog.DEBUG(pCTX,'sonia...1v_fld_datatype '|| v_fld_datatype);
 											 Plog.DEBUG(pCTX,'sonia...2 v_fldvalue'|| v_fldvalue);
  											 Plog.DEBUG(pCTX,'sonia...3 v_fldvaluedata' || v_fldvaluedata);

												IF ((v_fld_datatype='MD') OR (v_fld_datatype='MR')) THEN
												Plog.DEBUG(pCTX,'sonia...4 v_fld_datatype' || v_fld_datatype);
													--v_fldvalue:=nvl(v_fldvalue,v_fldvalue||'[VELSEP1]'||v_fldvaluedata);
													 IF LENGTH( trim(NVL(v_fldvalue,'') )) > 0  THEN --changed by sonia abrol, 04/21/05, the old condition to check if the field value is empty was not working
													  	  v_fldvalue := v_fldvalue ||'[VELSEP1]'||v_fldvaluedata;
			   											 Plog.DEBUG(pCTX,'sonia...5 v_fldvalue'|| v_fldvalue);
													END IF;
												END IF;
											IF v_old_fld_value = 'er_textarea_tag' THEN
												   v_old_fld_value := '';
											END IF;
											IF v_fldvalue = 'er_textarea_tag' THEN
												   v_fldvalue := '';
												   v_fldvaluedata:='';
												   v_fldval_for_audit := '';
											END IF;
											--replace &nbsp; from fld values
											  v_fldvalue := REPLACE (v_fldvalue,'&amp;nbsp;','');
  											  v_fldval_for_audit := REPLACE (v_fldval_for_audit,'&amp;nbsp;','');
											  v_old_fld_value := REPLACE (v_old_fld_value,'&amp;nbsp;','');




											  --escape the special characters from data values
													v_fldvalue:=Pkg_Util.f_unescapeSpecialCharsForXML(v_fldvalue);
													v_fldval_for_audit:=Pkg_Util.f_unescapeSpecialCharsForXML(v_fldval_for_audit);
													v_old_fld_value:=Pkg_Util.f_unescapeSpecialCharsForXML(v_old_fld_value);


												--for audit trail
												IF v_recordtype = 'M' THEN
												   IF NVL(v_old_fld_value,' ') = NVL(v_fldval_for_audit,' ') THEN
												     	v_recordtype := 'M' ;
												   ELSE
		   	  											v_old_fld_value := REPLACE(v_old_fld_value,'''','''''');
														--get information for modified column
														 SELECT NVL(v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/@systemid').getStringVal(),'') ,
														 NVL(v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/@name').getStringVal(),'')
														 INTO  v_fld_systemid,v_fld_name
														 FROM dual;
														-- insert audit record for modified column
														INSERT INTO ER_FORMAUDITCOL( PK_FORMAUDITCOL,
														 FK_FILLEDFORM,
														 FK_FORM,
														 FA_FORMTYPE,
														 FA_SYSTEMID,
														 FA_DATETIME,
														 FA_FLDNAME,
														 FA_OLDVALUE,
														 FA_NEWVALUE,
														 FA_MODIFIEDBY_NAME )
														VALUES(seq_er_formauditcol.NEXTVAL,
														v_filledformpk,
														v_form,
														v_filledformtype,
														v_fld_systemid,
														v_lastmodified_date,-- value from er_filledformtran
														v_fld_name,
														v_old_fld_value,
														v_fldval_for_audit,
														v_modified_by_name);
												   END IF;
												END IF;

												 Plog.DEBUG(pCTX,'v_form'||v_form||'mp_pkfld'||v_pkfld);
											   --Get col name
											    SELECT MP_MAPCOLNAME
											   INTO v_mapform_col
											   FROM ER_MAPFORM
											   WHERE fk_form = v_form AND mp_pkfld = v_pkfld;

												v_ins_sqlstr := v_ins_sqlstr || ',' || v_mapform_col ;
												v_values_sqlstr := v_values_sqlstr || ',:' || v_mapform_col;


												Plog.DEBUG(pCTX,'v_pkfld' || v_pkfld || 'v_pkfld' || 'v_fld_datatype' || v_fld_datatype || '*');
												Plog.DEBUG(pCTX,'k' || k || ' v_fld_count' ||  v_fld_count);

												 V_COLNAMES.EXTEND;
									             V_COLNAMES(v_fld_count) := ':' || v_mapform_col;

   												 V_COLVALUES.EXTEND;
									             V_COLVALUES(v_fld_count) :=  REPLACE(v_fldvalue,'''','''''');

												  Plog.DEBUG(pCTX,'Sonia 3.5: VV_COLNAMES.count' || V_COLNAMES.COUNT);


											END IF ; -- for lookup filter
									 END LOOP; -- for xml
									 	v_ins_sqlstr := v_ins_sqlstr || ')';
										v_values_sqlstr := v_values_sqlstr || ')';

										Plog.DEBUG(pCTX,'v_ins_sqlstr' || v_ins_sqlstr);
										Plog.DEBUG(pCTX,'v_values_sqlstr' || v_values_sqlstr);


									   v_ins_sql := v_ins_sqlstr || v_values_sqlstr;

									   --insert into t values(v_ins_sql);
									   --commit;

								Plog.DEBUG(pCTX,'SQL'||v_ins_sql);
								 --EXECUTE IMMEDIATE v_ins_sql ;
																	   --use dbms_sql:

									   		 --open cursor
									    l_cursor      := DBMS_SQL.OPEN_CURSOR;
									    DBMS_SQL.PARSE( l_cursor,  v_ins_sql , dbms_sql.native );

										-- bind static fields
										DBMS_SQL.BIND_VARIABLE( l_cursor, ':pk_formslinear',v_linpk    );
										DBMS_SQL.BIND_VARIABLE( l_cursor, ':form_completed',  v_form_completed );
										DBMS_SQL.BIND_VARIABLE( l_cursor, ':created_on',  v_created_on   );
										DBMS_SQL.BIND_VARIABLE( l_cursor, ':fk_filledform', v_filledformpk   );
										DBMS_SQL.BIND_VARIABLE( l_cursor, ':fk_form',  v_form  );
										DBMS_SQL.BIND_VARIABLE( l_cursor, ':filldate',  v_fftdate   );
										DBMS_SQL.BIND_VARIABLE( l_cursor, ':form_type',  v_filledformtype  );
										DBMS_SQL.BIND_VARIABLE( l_cursor, ':export_flag', 0  );

										DBMS_SQL.BIND_VARIABLE( l_cursor, ':creator',NVL(v_creator,NULL)   );
										DBMS_SQL.BIND_VARIABLE( l_cursor, ':last_modified_by',  NVL(v_lastmodified_by,NULL) );
										DBMS_SQL.BIND_VARIABLE( l_cursor, ':last_modified_date', i.last_modified_date  );


									   IF v_filledformtype ='A' OR v_filledformtype = 'S' THEN
									   	   IF LENGTH(v_id) > 0 THEN
										   	  --v_values_sqlstr := v_values_sqlstr || ',' || v_id || ', NULL';
										    	DBMS_SQL.BIND_VARIABLE( l_cursor, ':id',    v_id);
												DBMS_SQL.BIND_VARIABLE( l_cursor, ':fk_patprot',  V_DUMMYVAL );
										  ELSE
										   	   -- v_values_sqlstr := v_values_sqlstr || ', NULL, NULL';
												DBMS_SQL.BIND_VARIABLE( l_cursor, ':id',    V_DUMMYVAL);
												DBMS_SQL.BIND_VARIABLE( l_cursor, ':fk_patprot',  V_DUMMYVAL );
										   END IF;
										ELSE
											IF LENGTH(v_id) > 0 THEN
												--v_values_sqlstr := v_values_sqlstr || ',' || v_id ;
												DBMS_SQL.BIND_VARIABLE( l_cursor, ':id',    v_id);
											 ELSE
												 --v_values_sqlstr := v_values_sqlstr || ', NULL';
												  DBMS_SQL.BIND_VARIABLE( l_cursor, ':id',    V_DUMMYVAL);
											END IF;
											IF LENGTH(v_fk_patprot) > 0 THEN
												--v_values_sqlstr := v_values_sqlstr || ',' || v_fk_patprot ;
												DBMS_SQL.BIND_VARIABLE( l_cursor, ':fk_patprot',  v_fk_patprot);
											 ELSE
												-- v_values_sqlstr := v_values_sqlstr || ', NULL';
												 DBMS_SQL.BIND_VARIABLE( l_cursor, ':fk_patprot',  V_DUMMYVAL );
											END IF;
										END IF;

										--bind column variables
											 Plog.DEBUG(pCTX,'Sonia 3: VV_COLNAMES.count' || V_COLNAMES.COUNT);
								 	    FOR cl IN 1..V_COLNAMES.COUNT
								           LOOP -- loop for all message types
	   											 Plog.DEBUG(pCTX,'Sonia 3: VV_COLNAMES.cl' || V_COLNAMES(cl) || '*' ||V_COLVALUES(cl));
												DBMS_SQL.BIND_VARIABLE( l_cursor, V_COLNAMES(cl), V_COLVALUES(cl) );
								          END LOOP;

										 v_len:=DBMS_SQL.EXECUTE(l_cursor);
							  		    DBMS_SQL.CLOSE_CURSOR( l_cursor );

								--Plog.DEBUG(pCTX,'SQL'||v_ins_sql);
								 --dbms_output.put_line('inserted ' || i.FK_FILLEDFORM);
								 ELSE
			 							  Plog.DEBUG(pCTX,'(v_recordtype = M and v_oldxml is null) or (v_recordtype = N) ');
								 END IF; -- null check for v_oldxml and recordtypes
							 ELSE
							 Plog.DEBUG(pCTX,'v_newxml is null');
							 END IF ; -- null check for v_newxml
 							 Plog.DEBUG(pCTX,'finished for  v_pk_fft ' ||  v_pk_fft );

							--  UPDATE ER_FILLEDFORMTRAN
							  --SET fft_recordtype  = 'E' WHERE pk_fft = v_pk_fft ; --mark the record as exported

							    DELETE FROM ER_FILLEDFORMTRAN WHERE pk_fft = v_pk_fft ;
								COMMIT;
								EXCEPTION WHEN OTHERS THEN
								 Plog.DEBUG(pCTX,'SKIPPED:ID='||v_pk_fft || SQLERRM);
								END;

						  END LOOP; -- for filled forms for the selected form

						  EXCEPTION WHEN OTHERS THEN
						 			 Plog.DEBUG(pCTX,'SKIPPED:FORM ID='||v_form || SQLERRM);
						  END;
		    END LOOP; -- for distinct forms

--	  DELETE FROM ER_FILLEDFORMTRAN WHERE fft_recordtype  = 'E';

	  COMMIT;
 	END;

  --KM-#3919
	PROCEDURE SP_GET_DATA_COLUMN_SQL(P_FORM IN NUMBER, P_FORMTYPE IN VARCHAR2, P_FORMLIBVER IN NUMBER, O_LIST OUT CLOB, O_DATACOL_COUNT OUT NUMBER)
	IS
	      v_sql LONG;
	  	  i NUMBER := 1;
		  v_cols NUMBER := 1;
		  v_collist LONG;
		  VALUE Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
	BEGIN
	 SELECT COUNT(*)
	 INTO v_cols
	 FROM ER_MAPFORM WHERE fk_form = p_form;
	 FOR i IN 1..v_cols  LOOP
		  VALUE.EXTEND;
		  VALUE(i) := NULL;
		  v_collist := v_collist || ', col' || i;
	  END LOOP;
	  O_DATACOL_COUNT := v_cols;
   	  v_collist := SUBSTR(v_collist,2);
     -- Build a query
	  IF P_FORMTYPE = 'A' THEN
	        v_sql := 'SELECT ' || v_collist || ', pk_impacctform, nvl(fk_filledform,-1) fk_filledform, fk_account,filldate,form_completed,is_valid,notification_sent,creator,ip_Add FROM er_impacctform WHERE fk_form = ' || p_form || ' and fk_formlibver = '||  P_FORMLIBVER ||' and export_flag = 1 ';
	  ELSIF P_FORMTYPE = 'S' THEN
	        v_sql := 'SELECT ' || v_collist || ', pk_impstudyform, nvl(fk_filledform,-1) fk_filledform, fk_account, fk_study,filldate,form_completed,is_valid,notification_sent,creator,ip_Add FROM er_impstudyform WHERE fk_form = ' || p_form || ' and fk_formlibver = '||  P_FORMLIBVER ||' and export_flag = 1 ';
	  ELSIF P_FORMTYPE = 'P'  THEN
	  		v_sql := 'SELECT ' || v_collist || ', pk_imppatform, nvl(fk_filledform,-1) fk_filledform, fk_account,fk_per,fk_patprot,filldate,form_completed,is_valid,notification_sent,custom_col,creator,ip_Add FROM er_imppatform WHERE fk_form = ' || p_form || ' and fk_formlibver = '||  P_FORMLIBVER ||' and export_flag = 1 ';
	  ELSIF P_FORMTYPE = 'L'  THEN -- from er_formslinear
			v_sql := 'SELECT ' || v_collist || ', PK_FORMSLINEAR, FK_FILLEDFORM, FK_FORM, FILLDATE, FORM_COMPLETED,		IS_VALID, NOTIFICATION_SENT, FORM_TYPE, EXPORT_FLAG, ID, FK_PATPROT  FROM ER_FORMSLINEAR  WHERE fk_form = ' || p_form  ;

	  END IF;
	  o_list := v_sql;
	END;
	PROCEDURE SP_INSERT_ACCTFORM_EXP(p_form IN NUMBER, p_account IN NUMBER, p_formxml IN CLOB,
	p_completed IN NUMBER, p_creator IN NUMBER, p_valid IN NUMBER, p_ipAdd IN VARCHAR2,
	p_datastr IN VARCHAR2, p_datacolStr IN VARCHAR2, p_formlibver IN NUMBER, p_filledform IN NUMBER,  o_ret OUT NUMBER, p_imp IN NUMBER)
	AS
	 /****************************************************************************************************
	   ** Procedure to insert a filled account related form with process_data = 0
	   ** Author: Sonia Sahni 21 Nov 2003
	   ** Input parameter p_form  - Form ID
	   ** Input parameter p_account  - Account
	   ** Input parameter p_formxml - Form xml
	   ** Input parameter p_completed - Form compeletion status
	   ** Input parameter p_creator - Form record creator
	   ** Input parameter p_valid - Form validity flag
	   ** Input parameter p_ipAdd - User's IP ADD
	   ** Output parameter p_ret - Returns primary key of new record
	   **/
	  v_xmlclob    CLOB;
	  v_pkfilledform NUMBER;
	  v_linpk NUMBER;
	  v_ins_sqlstr LONG;
	  v_values_sqlstr LONG;
	  v_lin_sql LONG  ;
	  v_fftdate DATE;
	  v_datastr LONG;
	  v_error VARCHAR2(4000);
	  BEGIN
	  v_xmlclob :=  p_formxml;


	  IF  p_filledform <= 0 THEN
		  SELECT SEQ_ER_ACCTFORMS.NEXTVAL
		  INTO  v_pkfilledform
		  FROM dual;
		  INSERT INTO ER_ACCTFORMS(PK_ACCTFORMS,FK_FORMLIB,FK_ACCOUNT,
					ACCTFORMS_XML,RECORD_TYPE,FORM_COMPLETED,
					CREATOR,ISVALID ,CREATED_ON,IP_ADD,PROCESS_DATA,FK_FORMLIBVER)
		  VALUES ( v_pkfilledform,p_form,p_account,
					XMLTYPE(v_xmlclob),	'N', p_completed ,p_creator,p_valid, SYSDATE, p_ipAdd,2 ,p_formlibver);
			o_ret := v_pkfilledform;
	  ELSE

	  	  v_pkfilledform := p_filledform;

	  	  UPDATE ER_ACCTFORMS
		  SET FK_FORMLIB = p_form,FK_ACCOUNT = p_account,
					ACCTFORMS_XML = XMLTYPE(v_xmlclob),RECORD_TYPE = 'M',FORM_COMPLETED = p_completed ,
					LAST_MODIFIED_BY = p_creator,ISVALID =p_valid ,LAST_MODIFIED_DATE = SYSDATE,
					IP_ADD = p_ipadd,PROCESS_DATA = 2,FK_FORMLIBVER =  p_formlibver
		  WHERE PK_ACCTFORMS = v_pkfilledform;
		  o_ret := 0;
	  END IF;
	  --insert data in er_formslinear for this form, we are bypassing xml processing
  	  IF  p_filledform <= 0 THEN
		  SELECT seq_er_formslinear.NEXTVAL
		  INTO v_linpk
		  FROM dual;
		  SELECT NVL( ACCTFORMS_FILLDATE,SYSDATE)
		  INTO v_fftdate
		  FROM ER_ACCTFORMS
		  WHERE PK_ACCTFORMS = v_pkfilledform;
		  v_datastr := p_datastr;
		  v_datastr := REPLACE(v_datastr,'[*velquote*]','''''');
		  v_ins_sqlstr := 'INSERT INTO er_formslinear(pk_formslinear,id,fk_filledform,fk_form,filldate,form_type,export_flag,creator,created_on,form_completed ' || p_datacolStr || ')';
		  v_values_sqlstr := ' VALUES (' || v_linpk || ' , ' || p_account || ' , ' || v_pkfilledform || ',' || p_form || ',''' || v_fftdate || ''',''A'',0 ,' ||
									   p_creator || ', sysdate,' || p_completed  ||  v_datastr || ')' ;
	  	  v_lin_sql := v_ins_sqlstr || v_values_sqlstr ;
		  EXECUTE IMMEDIATE v_lin_sql ;
	  END IF;
	  UPDATE ER_IMPACCTFORM SET export_flag = 0 WHERE pk_impacctform = p_imp;
	  EXCEPTION  WHEN OTHERS THEN
	  		v_error := SQLERRM;
	  		INSERT INTO ER_IMPLOG VALUES (seq_er_implog.NEXTVAL, 'A',p_imp,v_error,SYSDATE);
	       o_ret := -1;
	       RAISE_APPLICATION_ERROR (-20102, 'Exception occurred in PKG_REPFORM.SP_INSERT_ACCTFORMEXP :'||SQLERRM ||  v_lin_sql);
	  END ; --end of SP_INSERT_ACCTFORM_EXP
	   PROCEDURE SP_INSERT_STUDYFORM_EXP(p_form IN NUMBER, p_study IN NUMBER, p_formxml IN CLOB,
 	 p_completed IN NUMBER, p_creator IN NUMBER, p_valid IN NUMBER, p_ipAdd IN VARCHAR2,  p_datastr IN VARCHAR2,
	 p_datacolStr IN VARCHAR2, p_formlibver IN NUMBER,  p_filledform IN NUMBER, o_ret OUT NUMBER, p_imp IN NUMBER)
	   	AS
	 /****************************************************************************************************
	   ** Procedure to insert a filled study related form with process_data = 0
	   ** Author: Sonia Sahni 21 Nov 2003
	   ** Input parameter p_form  - Form ID
	   ** Input parameter p_account  - Account
	   ** Input parameter p_formxml - Form xml
	   ** Input parameter p_completed - Form compeletion status
	   ** Input parameter p_creator - Form record creator
	   ** Input parameter p_valid - Form validity flag
	   ** Input parameter p_ipAdd - User's IP ADD
	   ** Output parameter p_ret - Returns primary key of new record
	   **/
	  v_xmlclob    CLOB;
	  v_pkfilledform NUMBER;
	  v_linpk NUMBER;
	  v_ins_sqlstr LONG;
	  v_values_sqlstr LONG;
	  v_lin_sql LONG  ;
	  v_fftdate DATE;
	  v_datastr LONG;
	  v_error VARCHAR2(4000);
	  BEGIN
	  v_xmlclob :=  p_formxml;
	  IF  p_filledform <= 0 THEN
		  SELECT SEQ_ER_STUDYFORMS.NEXTVAL
		  INTO  v_pkfilledform
		  FROM dual;
		  INSERT INTO ER_STUDYFORMS(PK_STUDYFORMS,FK_FORMLIB,FK_STUDY,
					STUDYFORMS_XML,RECORD_TYPE,FORM_COMPLETED,
					CREATOR,ISVALID ,CREATED_ON,IP_ADD,PROCESS_DATA,FK_FORMLIBVER)
			VALUES ( v_pkfilledform,p_form,p_study,
					XMLTYPE(v_xmlclob),	'N', p_completed ,p_creator,p_valid, SYSDATE, p_ipAdd,2,p_formlibver );
			o_ret := v_pkfilledform;
	  ELSE
	  	  	UPDATE ER_STUDYFORMS
			SET FK_FORMLIB = p_form,FK_STUDY = p_study,
					STUDYFORMS_XML = XMLTYPE(v_xmlclob),RECORD_TYPE = 'M',FORM_COMPLETED = p_completed,
					LAST_MODIFIED_BY = p_creator,ISVALID = p_valid ,LAST_MODIFIED_DATE = SYSDATE,
					IP_ADD = p_ipAdd,PROCESS_DATA = 2,FK_FORMLIBVER = p_formlibver
			WHERE PK_STUDYFORMS = p_filledform;
			o_ret := 0;
	  END IF;
	  --insert data in er_formslinear for this form, we are bypassing xml processing
	  IF  p_filledform <= 0 THEN
		  SELECT seq_er_formslinear.NEXTVAL
		  INTO v_linpk
		  FROM dual;
		  SELECT NVL( STUDYFORMS_FILLDATE,SYSDATE)
		  INTO v_fftdate
		  FROM ER_STUDYFORMS
		  WHERE PK_STUDYFORMS = v_pkfilledform;
		  v_datastr := p_datastr;
		  v_datastr := REPLACE(v_datastr,'[*velquote*]','''''');
		  v_ins_sqlstr := 'INSERT INTO er_formslinear(pk_formslinear,id,fk_filledform,fk_form,filldate,form_type,export_flag ,creator,created_on,form_completed ' || p_datacolStr || ')';
		  v_values_sqlstr := ' VALUES (' || v_linpk || ' , ' || p_study || ' , ' ||  v_pkfilledform || ',' || p_form || ',''' || v_fftdate || ''',''S'',0 ,' ||
									   p_creator || ', sysdate,' || p_completed  || v_datastr || ')' ;

	  	  v_lin_sql := v_ins_sqlstr || v_values_sqlstr ;
		  EXECUTE IMMEDIATE v_lin_sql ;
	  END IF;
	  UPDATE ER_IMPSTUDYFORM SET export_flag = 0 WHERE pk_impstudyform = p_imp;
	  EXCEPTION  WHEN OTHERS THEN
	  		v_error := SQLERRM;
	  		INSERT INTO ER_IMPLOG VALUES (seq_er_implog.NEXTVAL, 'S',p_imp,v_error,SYSDATE);
	  	   o_ret := -1;
	       RAISE_APPLICATION_ERROR (-20102, 'Exception occurred in PKG_REPFORM.SP_INSERT_STUDYFORMEXP :'||SQLERRM ||  v_lin_sql);
	  END ; --end of SP_INSERT_STUDYFORM_EXP
	 PROCEDURE SP_INSERT_PATFORM_EXP(p_form IN NUMBER, p_pat IN NUMBER, p_patprot IN NUMBER, p_formxml IN CLOB,
 	 p_completed IN NUMBER, p_creator IN NUMBER, p_valid IN NUMBER,
	 p_ipAdd IN VARCHAR2,  p_datastr IN VARCHAR2, p_datacolStr IN VARCHAR2, p_formlibver IN NUMBER,  p_filledform IN NUMBER,
	 p_customCol IN VARCHAR2,o_ret OUT NUMBER, p_imp IN NUMBER)
	   	AS
	 /****************************************************************************************************
	   ** Procedure to insert a filled patient related form with process_data = 0
	   ** Author: Sonia Sahni 21 Nov 2003
	   ** Input parameter p_form  - Form ID
	   ** Input parameter p_account  - Account
	   ** Input parameter p_formxml - Form xml
	   ** Input parameter p_completed - Form compeletion status
	   ** Input parameter p_creator - Form record creator
	   ** Input parameter p_valid - Form validity flag
	   ** Input parameter p_ipAdd - User's IP ADD
	   ** Output parameter p_ret - Returns primary key of new record
	   **/
	  v_xmlclob    CLOB;
	  v_pkfilledform NUMBER;
	  v_linpk NUMBER;
	  v_ins_sqlstr LONG;
	  v_values_sqlstr LONG;
	  v_lin_sql LONG  ;
	  v_fftdate DATE;
	  v_datastr LONG;
	  v_customcount  VARCHAR2(250);
	  v_lmap VARCHAR2(250);
	  v_pkimpmap NUMBER ;
	  v_error VARCHAR2(4000);
	  	  BEGIN
	  v_xmlclob :=  p_formxml;
	    IF p_customcol='[VELDEFAULT]' THEN
	     IF  p_filledform <= 0 THEN
	  	  SELECT SEQ_ER_PATFORMS.NEXTVAL
		  INTO  v_pkfilledform
		  FROM dual;
	  	  INSERT INTO ER_PATFORMS(PK_PATFORMS,FK_FORMLIB,FK_PER,FK_PATPROT,
				PATFORMS_XML,RECORD_TYPE,FORM_COMPLETED,
				CREATOR,ISVALID ,CREATED_ON,IP_ADD,PROCESS_DATA,FK_FORMLIBVER)
			VALUES ( v_pkfilledform,p_form,DECODE(p_pat,0,NULL,p_pat),DECODE(p_patprot,0,NULL,p_patprot),
				XMLTYPE(v_xmlclob),	'N', p_completed ,p_creator,p_valid, SYSDATE, p_ipAdd,2 ,p_formlibver);
		o_ret := v_pkfilledform;
	  ELSE
	  	    UPDATE ER_PATFORMS
			SET FK_FORMLIB = p_form, FK_PER =DECODE(p_pat,0,NULL,p_pat) ,FK_PATPROT = DECODE(p_patprot,0,NULL,p_patprot),
				PATFORMS_XML = XMLTYPE(v_xmlclob),RECORD_TYPE = 'M',FORM_COMPLETED = p_completed,
				LAST_MODIFIED_BY = p_creator,ISVALID = p_valid,LAST_MODIFIED_DATE = SYSDATE,IP_ADD = p_ipAdd,
				PROCESS_DATA = 2,FK_FORMLIBVER = p_formlibver
			WHERE PK_PATFORMS = p_filledform;
			o_ret := 0;
	  END IF;
	  ELSE

	   --check if the record already exist in mapping table
	   SELECT NVL(COUNT(*),0) INTO v_customcount FROM ER_IMPPKMAP WHERE  imppkmap_rmap=p_customCol AND imppkmap_object='er_patforms' ;
	     --end checking
	     IF  v_customcount <= 0 THEN
		 Plog.DEBUG(pCTX,'Insert Statement');
	  	  SELECT SEQ_ER_PATFORMS.NEXTVAL
		  INTO  v_pkfilledform
		  FROM dual;
	  	  INSERT INTO ER_PATFORMS(PK_PATFORMS,FK_FORMLIB,FK_PER,FK_PATPROT,
				PATFORMS_XML,RECORD_TYPE,FORM_COMPLETED,
				CREATOR,ISVALID ,CREATED_ON,IP_ADD,PROCESS_DATA,FK_FORMLIBVER)
			VALUES ( v_pkfilledform,p_form,DECODE(p_pat,0,NULL,p_pat),DECODE(p_patprot,0,NULL,p_patprot),
				XMLTYPE(v_xmlclob),	'N', p_completed ,p_creator,p_valid, SYSDATE, p_ipAdd,2 ,p_formlibver);
		o_ret := v_pkfilledform;
		--update custom mapping table
		  SELECT SEQ_ER_imppkmap.NEXTVAL
		  INTO  v_pkimpmap
		  FROM dual;
		  INSERT INTO ER_IMPPKMAP(pk_imppkmap,imppkmap_object,imppkmap_rmap,imppkmap_lmap) VALUES
		   (v_pkimpmap,'er_patforms',p_customCol,v_pkfilledform);
		 ELSE
		 Plog.DEBUG(pCTX,'Update statement');
		    SELECT imppkmap_lmap INTO v_lmap FROM ER_IMPPKMAP WHERE  imppkmap_rmap=p_customCol AND imppkmap_object='er_patforms';
				   Plog.DEBUG(pCTX,' v_lmap '||v_lmap);
	  	    UPDATE ER_PATFORMS
			SET FK_FORMLIB = p_form, FK_PER =DECODE(p_pat,0,NULL,p_pat) ,FK_PATPROT = DECODE(p_patprot,0,NULL,p_patprot),
				PATFORMS_XML = XMLTYPE(v_xmlclob),RECORD_TYPE = 'M',FORM_COMPLETED = p_completed,
				LAST_MODIFIED_BY = p_creator,ISVALID = p_valid,LAST_MODIFIED_DATE = SYSDATE,IP_ADD = p_ipAdd,
				PROCESS_DATA = 2,FK_FORMLIBVER = p_formlibver
			WHERE PK_PATFORMS = TO_NUMBER(v_lmap);
			o_ret := 0;
	  END IF;

	  END IF; --end if for

	  --insert data in er_formslinear for this form, we are bypassing xml processing

  	  --IF  p_filledform <= 0 THEN
	  IF  o_ret> 0 THEN
	  Plog.DEBUG(pCTX,'Will add in er_formslinear');
		  SELECT seq_er_formslinear.NEXTVAL
		  INTO v_linpk
		  FROM dual;
		  Plog.DEBUG(pCTX,'Get the value');
		  SELECT NVL( PATFORMS_FILLDATE,SYSDATE)
		  INTO v_fftdate
		  FROM ER_PATFORMS
		  WHERE PK_PATFORMS = v_pkfilledform;
		  		  Plog.DEBUG(pCTX,'Get the value after'||v_fftdate );
		  v_datastr := p_datastr;
		  v_datastr := REPLACE(v_datastr,'[*velquote*]','''''');
		  v_ins_sqlstr := 'INSERT INTO er_formslinear(pk_formslinear,id,fk_patprot, fk_filledform,fk_form,filldate,form_type,export_flag ,creator,created_on,form_completed ' || p_datacolStr || ')';
		  v_values_sqlstr := ' VALUES (' || v_linpk || ' , ' || p_pat || ' ,  DECODE('|| p_patprot ||',0,NULL,'|| p_patprot ||') , ' ||   v_pkfilledform || ',' || p_form || ',''' || v_fftdate || ''',''P'',0 ,' ||
									   p_creator || ', sysdate,' || p_completed ||   v_datastr || ')' ;
	  	  v_lin_sql := v_ins_sqlstr || v_values_sqlstr ;
	  	  EXECUTE IMMEDIATE v_lin_sql ;
	  END IF;
	  UPDATE ER_IMPPATFORM SET export_flag = 0 WHERE pk_imppatform = p_imp;
	  EXCEPTION  WHEN OTHERS THEN
	  		v_error := SQLERRM;
	  		INSERT INTO ER_IMPLOG VALUES (seq_er_implog.NEXTVAL, 'P',p_imp,v_error,SYSDATE);
	       o_ret := -1;
	       RAISE_APPLICATION_ERROR (-20102, 'Exception occurred in PKG_REPFORM.SP_INSERT_PATFORMEXP :'||SQLERRM ||  v_lin_sql);
	  END ; --end of SP_INSERT_PATORM_EXP
--not in use now:
PROCEDURE SP_EXPTOFORMXML
IS
    v_cursor NUMBER;
    v_sql LONG;
    v_result NUMBER;
    v_val VARCHAR2(4000);
	i NUMBER := 1;
	v_cols NUMBER := 1;
	v_collist LONG;
	VALUE Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
	v_form NUMBER;
	v_formxml XMLTYPE;
--	v_filledformxml xmltype;
	v_fldtype VARCHAR(3);
	tmpxml  XMLTYPE;
	v_fld_value VARCHAR2(4000);
	v_updatedclob CLOB;
BEGIN
--p('on line 1');
 --get distinct forms
  FOR j IN (  SELECT DISTINCT FK_FORM FROM ER_FORMSLINEAR WHERE export_flag = 1)
  	LOOP
	--p(' on line  2');
   		 v_form := j.fk_form;
		 -- get XML for this form
		 SELECT form_xml
		 INTO v_formxml
		 FROM ER_FORMLIB
		 WHERE pk_formlib = v_form;
		  SELECT COUNT(*)
		  INTO v_cols
		  FROM ER_MAPFORM WHERE fk_form = v_form;
		  FOR i IN 1..v_cols  LOOP
		  	  VALUE.EXTEND;
		      VALUE(i) := NULL;
		 	  v_collist := v_collist || ', col' || i;
		   END LOOP;
   		   v_collist := SUBSTR(v_collist,2);
 		     -- Build a query
		    v_sql := 'SELECT ' || v_collist || ' FROM er_formslinear WHERE fk_form = ' || v_form || ' and export_flag = 1 ';
			--p('v_sql' || v_sql);
		    -- Open the cursor.
		    v_cursor := DBMS_SQL.OPEN_CURSOR;
		    -- Parse the query.
		    DBMS_SQL.PARSE(v_cursor, v_sql, DBMS_SQL.native);
		    -- Set up the columns
			FOR i IN 1..v_cols
			LOOP
			    DBMS_SQL.DEFINE_COLUMN(v_cursor, i, VALUE(i),4000);
			END LOOP;
		    -- Execute the query
		    v_result := DBMS_SQL.EXECUTE(v_cursor);
		    -- Fetch the rows
		    LOOP
		        EXIT WHEN DBMS_SQL.FETCH_ROWS (v_cursor) = 0;
					-- start updating blank xml
					--v_filledformxml := v_formxml;
					tmpxml := v_formxml;
					FOR i IN 1..v_cols
					LOOP
						DBMS_SQL.COLUMN_VALUE(v_cursor, i, VALUE(i));
					END LOOP;
					-- will update all text fields
					sp_updatexml(tmpxml, VALUE, v_updatedclob);
					--insert the record in respective table;
					tmpxml := sys.XMLTYPE.createXML(v_updatedclob);
					INSERT INTO ER_ACCTFORMS ( PK_ACCTFORMS, FK_FORMLIB, FK_ACCOUNT , ACCTFORMS_FILLDATE , ACCTFORMS_XML)
				   VALUES (SEQ_ER_ACCTFORMS.NEXTVAL,v_form,46,SYSDATE,tmpxml);
					--insert a record
		    END LOOP; --for fetching rows for forms
		    -- Close the cursor.
		    DBMS_SQL.CLOSE_CURSOR(v_cursor);
	 --get all export records for this form
	  END LOOP; --for all forms to export
	 COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      DBMS_SQL.CLOSE_CURSOR(v_cursor);
END SP_EXPTOFORMXML;
PROCEDURE sp_updatexml(p_xml sys.XMLTYPE ,p_values Types.SMALL_STRING_ARRAY, o_updatedclob OUT CLOB)
   /****************************************************************************************************
   ** Procedure to get XML dtagram for a field
   ** Author: Sonia Sahni 18th Nov 2003
   ******************************************NOT IN USE
   ********************************************************************************************************/
AS
    v_cnt NUMBER;
	i NUMBER := 1;
	o_clob    CLOB;
	v_doc       dbms_xmldom.DOMDocument;
    ndoc      dbms_xmldom.DOMNode;
    nodelist  dbms_xmldom.DOMNodelist;
	node      dbms_xmldom.DOMNode;
    childnode dbms_xmldom.DOMNode;
	textnode dbms_xmldom.DOMText;
	nodeM dbms_xmldom.DOMNode;
	nodeV dbms_xmldom.DOMNode;
	nodeTemp dbms_xmldom.DOMNode;
    nodelistSize NUMBER := 0;
    nodelistSizeCount NUMBER := 0;
	v_nodename VARCHAR2(50);
	v_counter NUMBER := 1;
	v_fldtype VARCHAR2(3);
	v_domelem dbms_xmldom.DOMElement;
	v_resp_nodelist dbms_xmldom.DOMNodeList;
	v_resp_count NUMBER;
	resp_ctr NUMBER;
	v_resp_elem dbms_xmldom.DOMElement;
	v_resp_dispval VARCHAR2(4000);
	v_resp_val VARCHAR2(4000);
	v_check_list Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
	v_chk_idx NUMBER;
 	--  myParser    dbms_xmlparser.Parser;
    BEGIN
	 v_cnt := p_values.COUNT(); --get the # of elements in array
	-- Create DOMDocument handle:
	 v_doc     := dbms_xmldom.newDOMDocument(p_xml);
     ndoc    := dbms_xmldom.makeNode(v_doc);
	-----------------------------------------------------------
	-- Get First Child Of the Node
	node := dbms_xmldom.getFirstChild( ndoc );
	nodeM := dbms_xmldom.getFirstChild( node );
	--dbms_output.put_line('Node Name <'|| dbms_xmldom.getNodeNAME(nodeM) || '>');
    nodeV := dbms_xmldom.getFirstChild(nodeM);
    v_nodename := dbms_xmldom.getNodeNAME(nodeV);
    IF NVL(LENGTH(trim(v_nodename)),0) = 0 THEN
   	   textnode := dbms_xmldom.createTextNode( v_doc,p_values(1));
	   nodeTemp := dbms_xmldom.appendChild(nodeM,dbms_xmldom.makeNode(textnode));
	   nodeV := dbms_xmldom.getFirstChild(nodeTemp);
      -- v_nodename := dbms_xmldom.getNodeValue(nodeTemp);
	END IF;
	--dbms_output.put_line('Node value <'|| dbms_xmldom.getNodeValue(nodeM) || '>');
	node := nodeM ;
	<<PARESE_RECORD>>
		 LOOP
		 	 IF dbms_xmldom.isNull( node ) = TRUE THEN
			 	EXIT PARESE_RECORD;
			  END IF;
			  v_counter := v_counter + 1;
			  nodeM := dbms_xmldom.getNextSibling( node ); --Get Next Sibbling
			  --dbms_output.put_line('v_counter' || v_counter || 'Node Name <'|| dbms_xmldom.getNodeNAME(nodeM) || '>');
			  nodeV := dbms_xmldom.getFirstChild( nodeM );
			  v_nodename := dbms_xmldom.getNodeNAME(nodeV);
			  --dbms_output.put_line('Node Name <'||  v_nodename  || '>');
			  IF v_nodename = '#text' THEN
			  	 dbms_xmldom.setNodeValue(nodeV,p_values(v_counter));
			  ELSIF NVL(LENGTH(trim(v_nodename)),0) = 0 THEN
			     textnode := dbms_xmldom.createTextNode( v_doc,p_values(v_counter));
				 nodeTemp := dbms_xmldom.appendChild(nodeM,dbms_xmldom.makeNode(textnode));
				 nodeV := dbms_xmldom.getFirstChild(nodeTemp);
				 v_nodename := dbms_xmldom.getNodeValue(nodeTemp);
			  ELSIF v_nodename = 'resp' THEN --its multiple choice
				v_domelem := dbms_xmldom.makeElement(nodeM);
				v_fldtype :=   dbms_xmldom.getAttribute(v_domelem,'type');
				-- make nodes of all resp
				v_resp_nodelist :=	dbms_xmldom.getChildrenByTagName(v_domelem,'resp');
				v_resp_count := dbms_xmldom.getLength(v_resp_nodelist);
				resp_ctr := 0;
				 <<PARSE_RESP>>
				 LOOP
				 	IF resp_ctr = v_resp_count THEN
					  	EXIT PARSE_RESP;
					 END IF;
			  	  	 v_resp_elem :=  dbms_xmldom.makeElement(dbms_xmldom.item(v_resp_nodelist,resp_ctr));
					 v_resp_dispval  :=   dbms_xmldom.getAttribute(v_resp_elem,'dispval');
				  	 v_resp_val  := p_values(v_counter);
					 IF v_fldtype = 'MD' OR v_fldtype = 'MR' THEN --for radio button and dropdown fields
					 	 --p('v_resp_dispval' || v_resp_dispval);
 					 	 --p('v_resp_val' || v_resp_val);
						 IF trim(v_resp_dispval) = trim(v_resp_val) THEN
						 	IF v_fldtype = 'MD' THEN
							    dbms_xmldom.setAttribute(v_resp_elem,'selected','1');
							ELSE
							    dbms_xmldom.setAttribute(v_resp_elem,'checked','1');
							END IF;
						 END IF;
					 ELSIF v_fldtype = 'MC' THEN
						   sp_parse_list (p_values(v_counter), v_check_list);
						   v_chk_idx := v_check_list.FIRST;
						   << display_loop >>
							WHILE v_chk_idx IS NOT NULL LOOP
								 IF '[' || trim(v_resp_dispval) || ']' = trim(v_check_list(v_chk_idx)) THEN
								 	dbms_xmldom.setAttribute(v_resp_elem,'checked','1');
									EXIT display_loop;
								 END IF;
							   -- DBMS_OUTPUT.PUT_LINE('The val ' || v_check_list(v_chk_idx));
							    v_chk_idx := v_check_list.NEXT(v_chk_idx);
							 END LOOP display_loop;
					 END IF;
				  resp_ctr := resp_ctr + 1;
				 END LOOP PARSE_RESP;
			  END IF;
		  --dbms_output.put_line('Node value <'|| dbms_xmldom.getNodeValue(nodeV) || '>');
			  node := nodeM ;
  		 	 IF v_counter = v_cnt THEN
			 	EXIT PARESE_RECORD;
			 END IF;
		 END LOOP PARESE_RECORD;
		  dbms_xmldom.writeToClob(ndoc,o_clob);
		  dbms_xmldom.freeDocument(v_doc); -- Free The Document
	o_updatedclob := o_clob;
	-------------------------------------------------------------
	END;
	PROCEDURE sp_parse_list (p_mainstring VARCHAR2, o_list OUT Types.SMALL_STRING_ARRAY)
	AS
   /****************************************************************************************************
   ** Procedure to parse the checkbox string
   ** Author: Sonia Sahni 19th Nov 2003
   ******************************************NOT IN USE
   ********************************************************************************************************/
	   V_STR      VARCHAR2 (4000) DEFAULT p_mainstring || ',[';
	   V_PARAMS   VARCHAR2 (4001) DEFAULT p_mainstring || ',[';
	   V_POS      NUMBER         := 0;
	   v_count NUMBER;
	   v_list Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
	 BEGIN
	   LOOP
	       V_POS := INSTR (V_STR, ',[');
		   V_PARAMS := SUBSTR (V_STR, 1, V_POS - 1);
	        EXIT WHEN V_PARAMS IS NULL;
				 v_list.EXTEND;
				 v_count := v_list.LAST;
				 v_list(v_count) := V_PARAMS;
	      V_STR := SUBSTR (V_STR, V_POS + 1 );
	   END LOOP ;
	   o_list := v_list;
	END ;
END Pkg_Formrep;

/


CREATE SYNONYM ESCH.PKG_FORMREP FOR PKG_FORMREP;


CREATE SYNONYM EPAT.PKG_FORMREP FOR PKG_FORMREP;


GRANT EXECUTE, DEBUG ON PKG_FORMREP TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_FORMREP TO ESCH;

