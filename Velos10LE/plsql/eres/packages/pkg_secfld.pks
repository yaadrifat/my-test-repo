create or replace PACKAGE        "PKG_SECFLD" 
AS

PROCEDURE SP_UPDATE_SECTION_SEQ(p_formlib_id Number,ret out number);
PROCEDURE SP_UPDATE_FIELD_SEQ(p_formsec_id Number,p_formfld_id Number,p_fld_seq Number, p_usr Number, ret out number);

END PKG_SECFLD;
/


CREATE SYNONYM ESCH.PKG_SECFLD FOR PKG_SECFLD;


CREATE SYNONYM EPAT.PKG_SECFLD FOR PKG_SECFLD;


GRANT EXECUTE, DEBUG ON PKG_SECFLD TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_SECFLD TO ESCH;

