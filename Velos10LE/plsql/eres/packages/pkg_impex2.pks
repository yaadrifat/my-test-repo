CREATE OR REPLACE PACKAGE        "PKG_IMPEX2" IS

  PROCEDURE SP_EXP_ALLPATSTUDYFORMS (
      P_EXPID          NUMBER,
      O_RESFORM     OUT  Gk_Cv_Types.GenericCursorType,
      O_RES_MAPFORM     OUT  Gk_Cv_Types.GenericCursorType,
      O_RES_LINKFORM     OUT  Gk_Cv_Types.GenericCursorType,
	  O_FORMSEC     OUT  Gk_Cv_Types.GenericCursorType,
	  O_FORMFLD     OUT  Gk_Cv_Types.GenericCursorType,
	  O_REPFORMFLD OUT  Gk_Cv_Types.GenericCursorType,
	  O_FLDRESP OUT  Gk_Cv_Types.GenericCursorType,
	  O_FLDVALIDATE OUT  Gk_Cv_Types.GenericCursorType,
	  O_REPFLDVALIDATE OUT  Gk_Cv_Types.GenericCursorType,
	  O_RES_FORMLIBVER OUT  Gk_Cv_Types.GenericCursorType,
	  O_RES_FLDACTION OUT  Gk_Cv_Types.GenericCursorType,
  	  O_RES_FLDACTIONINFO OUT  Gk_Cv_Types.GenericCursorType
  	 );

 PROCEDURE SP_IMP_ALLPATSTUDYFORMS(P_IMPID NUMBER, O_SUCCESS OUT NUMBER);

/******************************************************************************
   NAME:       PKG_IMPEX2
   PURPOSE:    Contains new IMPEX related objects for A2A

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        4/29/2004             1. Created this package.

******************************************************************************/
END Pkg_Impex2;
/


CREATE SYNONYM ESCH.PKG_IMPEX2 FOR PKG_IMPEX2;


CREATE SYNONYM EPAT.PKG_IMPEX2 FOR PKG_IMPEX2;


GRANT EXECUTE, DEBUG ON PKG_IMPEX2 TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_IMPEX2 TO ESCH;

