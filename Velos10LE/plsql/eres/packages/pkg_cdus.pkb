CREATE OR REPLACE PACKAGE BODY        "PKG_CDUS" IS
  PROCEDURE SP_GENERATE_CDUS(p_subm_date DATE , p_start_date DATE, p_cutoff_date DATE, p_user NUMBER, p_study NUMBER)
   AS
--  v_studynumber VARCHAR2(20);
  v_studynumber VARCHAR2(100);--JM:120506
  v_studystatus VARCHAR2(100);
  v_test NUMBER;
  v_studystat_date DATE;
  v_subm_date DATE;
  v_cutoff_date DATE;
  v_studystat CHAR(2);
  v_cdus CLOB;
  v_completer VARCHAR2(200);
  v_correlative VARCHAR2(4000);
  v_corr INT;
  v_patcnt NUMBER;
  v_patients CLOB;
  v_patient_race CLOB;
  v_patient_add_race VARCHAR2(100);
  v_find INT;
  v_patient_add_race_code VARCHAR2(10);
  v_temp VARCHAR2(10);
  v_adverse_events CLOB;
  v_dict_ver NUMBER;
  BEGIN
  	   SELECT study_number INTO v_studynumber FROM ER_STUDY WHERE pk_study = p_study;

--	   select fk_codelst_studystat, studystat_date into v_studystat, v_studystat_date from er_studystat  where fk_study = p_study and studystat_endt is not null;

  	   SELECT (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_studystat),studystat_date INTO v_studystatus,v_studystat_date FROM ER_STUDYSTAT a WHERE fk_study = p_study AND studystat_endt IS NULL;

	   SELECT '"' || usr_lastname || '^' || usr_firstname || '^' || usr_midname || '","' || add_phone || '","' || add_fax || '","' || add_email  || '","' || 'CHANGE_CODE' || '"' INTO v_completer FROM ER_USER, ER_ADD WHERE pk_user = p_user AND fk_peradd = pk_add;

	   v_cdus := v_cdus || '"COLLECTIONS","' || v_studynumber || '",' || TO_CHAR(p_subm_date, 'yyyymmdd') || ',' || TO_CHAR(p_cutoff_date, 'yyyymmdd') || ',' || v_studystatus || TO_CHAR(v_studystat_date, 'yyyymmdd') ||  ',' || v_completer || CHR(13);

	   SELECT COUNT(*) INTO v_corr FROM ER_STUDY WHERE pk_study = p_study AND study_assoc IS NOT NULL;

	   IF v_corr > 0 THEN
	   	  SELECT '"CORRELATIVE_STUDIES","' || study_number || '","' || (SELECT study_number FROM ER_STUDY WHERE pk_study = a.study_assoc) || '?????' || CHR(13) INTO v_correlative FROM ER_STUDY a WHERE pk_study = p_study;
	  	   v_cdus := v_cdus || CHR(13) || v_correlative;
	   END IF;

  	   SELECT COUNT(*) INTO v_patcnt FROM ER_PATPROT WHERE fk_study = p_study AND patprot_stat = 1 AND PATPROT_ENROLDT IS NOT NULL;

	   IF v_patcnt > 0 THEN
	   	  FOR i IN (SELECT patprot_patstdid,person_zip, person_country,person_dob,DECODE((SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_gender),'male','1','female','2','unknown','9') AS gender_code,DECODE((SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_ethnicity),'hispanic','1','nonhispanic','2','notreported','8','unknown','9') AS ethnicity_flag,patprot_enroldt,(SELECT codelst_custom_col FROM ER_CODELST WHERE pk_codelst = fk_codelst_race) AS race_code, person_add_race FROM ER_PATPROT,person WHERE pk_person = fk_per AND fk_study = p_study AND patprot_stat = 1 AND PATPROT_ENROLDT IS NOT NULL)
		  LOOP
		  	  v_patients := v_patients || '"PATIENTS","' || v_studynumber || '","' || i.patprot_patstdid || '","' || i.person_zip || '","' || i.person_country || '","' || TO_CHAR(i.person_dob,'yyyymm') || ',"' || i.gender_code || '","' ||  i.ethnicity_flag || '","?",' || TO_CHAR(i.patprot_enroldt,'yyyymmdd') || CHR(13);

			  IF i.race_code IS NOT NULL THEN
			  	 v_patient_race := v_patient_race || '"PATIENT_RACES","'  || v_studynumber || '","' || i.patprot_patstdid || '", "' || i.race_code || '"' || CHR(13);
			  END IF;
			  IF i.person_add_race IS NOT NULL OR LENGTH(trim(i.person_add_race)) > 0 THEN
			  	 	 v_patient_add_race := i.person_add_race;
					 	LOOP
							v_find := INSTR(v_patient_add_race,',');
	    					IF v_find = 0 THEN
	       					   SELECT codelst_custom_col INTO v_patient_add_race_code FROM ER_CODELST WHERE pk_codelst = TO_NUMBER(v_patient_add_race);
			  		  	 	   v_patient_race := v_patient_race || '"PATIENT_RACES","'  || v_studynumber || '","' || i.patprot_patstdid || '", "' || v_patient_add_race_code || '"' || CHR(13);
		   					   EXIT;
							ELSE
								v_temp := SUBSTR(v_patient_add_race,1,v_find-1) ;
	       					   SELECT codelst_custom_col INTO v_patient_add_race_code FROM ER_CODELST WHERE pk_codelst = TO_NUMBER(v_temp);
			  		  	 	   v_patient_race := v_patient_race || '"PATIENT_RACES","'  || v_studynumber || '","' || i.patprot_patstdid || '", "' || v_patient_add_race_code || '"' || CHR(13);
								v_patient_add_race := SUBSTR(v_patient_add_race,v_find+1);
						   END IF;
						END LOOP;
				 END IF;
		  END LOOP;
	   END IF;

	   SELECT study_advlkp_ver INTO v_dict_ver FROM ER_STUDY WHERE pk_study = p_study;

	   IF v_dict_ver = 1 THEN

	   FOR k IN (SELECT patprot_patstdid,(SELECT custom009 FROM ER_LKPDATA WHERE fk_lkplib = 1 AND trim(custom002) = trim(ae_name) AND TO_NUMBER(custom004) = ae_grade) ae_type_code, ae_grade, DECODE(trim((SELECT codelst_subtyp FROM sch_codelst WHERE pk_codelst = ae_relationship )),'ad_nr','1','ad_unlike','2','ad_pos','3','ad_prob','4','ad_def','5','?') AS ae_attribution_code  FROM sch_adverseve a, ER_PATPROT b WHERE a.fk_study = p_study AND a.fk_study = b.fk_study AND a.fk_per = b.fk_per AND b.patprot_stat = 1 AND PATPROT_ENROLDT IS NOT NULL)
	   LOOP

	   	   v_adverse_events := v_adverse_events || '"ADVERSE_EVENTS","' || v_studynumber   || '","' || k.patprot_patstdid || '",' || '?' || ',' || k.ae_type_code  || ',' || k.ae_grade  || ',"",' || k.ae_attribution_code || ',""' || CHR(13);

	   END LOOP;

	   ELSE
	   FOR k IN (SELECT patprot_patstdid,(SELECT custom010 FROM ER_LKPDATA WHERE fk_lkplib = 3 AND trim(custom009) = trim(ae_name) AND TO_NUMBER(custom004) = ae_grade) ae_type_code, ae_grade, DECODE(trim((SELECT codelst_subtyp FROM sch_codelst WHERE pk_codelst = ae_relationship )),'ad_nr','1','ad_unlike','2','ad_pos','3','ad_prob','4','ad_def','5','?') AS ae_attribution_code  FROM sch_adverseve a, ER_PATPROT b WHERE a.fk_study = p_study AND a.fk_study = b.fk_study AND a.fk_per = b.fk_per AND b.patprot_stat = 1 AND PATPROT_ENROLDT IS NOT NULL)
	   LOOP

	   	   v_adverse_events := v_adverse_events || '"ADVERSE_EVENTS","' || v_studynumber   || '","' || k.patprot_patstdid || '",' || '?' || ',' || k.ae_type_code  || ',' || k.ae_grade  || ',"",' || k.ae_attribution_code || ',""' || CHR(13);

	   END LOOP;
	   END IF;
	   v_cdus := v_cdus || v_patients || v_patient_race || v_adverse_events;

  	   INSERT INTO ER_CDUS (pk_cdus,submission_date,start_date,cutoff_date,fk_user,fk_study,cdus_clob) VALUES  (seq_er_cdus.NEXTVAL, p_subm_date, p_start_date, p_cutoff_date, p_user, p_study, v_cdus);
  END;



END PKG_CDUS;
/


CREATE SYNONYM ESCH.PKG_CDUS FOR PKG_CDUS;


CREATE SYNONYM EPAT.PKG_CDUS FOR PKG_CDUS;


GRANT EXECUTE, DEBUG ON PKG_CDUS TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_CDUS TO ESCH;

