 CREATE OR REPLACE PACKAGE ERES."PKG_LNKFORM" AS
    PROCEDURE SP_LINKFORM(p_account VARCHAR2, p_study ARRAY_STRING, p_forms ARRAY_STRING, p_names ARRAY_STRING,
    p_descs ARRAY_STRING, p_displinks ARRAY_STRING, p_chars ARRAY_STRING, p_orgs ARRAY_STRING, p_grps ARRAY_STRING,
    p_lnkfrom VARCHAR2, p_user VARCHAR2, p_ipadd VARCHAR2, o_ret OUT NUMBER, o_new_forms OUT ARRAY_STRING);
    PROCEDURE SP_FILLED_FORM_NOTIFICATIONS;
    PROCEDURE SP_COPY_MULTIPLE_STUDY_FORMS (p_studyform_ids ARRAY_STRING, p_form_type NUMBER ,
    p_stdacc_id NUMBER, p_user_id NUMBER , p_form_name VARCHAR2,
    p_ip_add VARCHAR2  ,  o_ret_number OUT NUMBER);
    PROCEDURE SP_COPY_MULTIPLE_ACCOUNT_FORMS (p_accountform_ids ARRAY_STRING, p_form_type NUMBER ,
    p_user_id NUMBER , p_form_name VARCHAR2, p_ip_add VARCHAR2, o_ret_number OUT NUMBER) ;

    PROCEDURE SP_FORM_DATA_BROWSER (p_id NUMBER, p_pk_form NUMBER,p_link_from VARCHAR2, p_dt_whereclause VARCHAR2, p_orderby VARCHAR2, p_ordertype VARCHAR2, o_sql OUT VARCHAR2, o_cnt_sql OUT VARCHAR2  , o_xmlSql OUT LONG, o_xml_orderBy OUT VARCHAR2);
    PROCEDURE SP_GET_FORM_RECORDS (p_id NUMBER, p_pk_form NUMBER,p_link_from VARCHAR2, p_dt_whereclause VARCHAR2, p_page NUMBER,  p_recs_per_page NUMBER, p_orderby VARCHAR2, p_ordertype VARCHAR2,o_res OUT Gk_Cv_Types.GenericCursorType, o_rows OUT NUMBER);

    PROCEDURE SP_UPDATE_FORMORGACC (p_pk_lf NUMBER , p_fk_form_id NUMBER , p_created_by NUMBER , p_ip_add VARCHAR2 ,  p_org_ids ARRAY_STRING , p_delorg_ids ARRAY_STRING , o_ret_number OUT NUMBER) ;
    PROCEDURE SP_UPDATE_FORMGRPACC (p_pk_lf NUMBER , p_fk_form_id NUMBER , p_created_by NUMBER , p_ip_add VARCHAR2 , p_grp_ids ARRAY_STRING , p_delgrp_ids ARRAY_STRING , o_ret_number OUT NUMBER) ;
    PROCEDURE SP_LINK_CRFFORMS(p_formIds ARRAY_STRING,p_account VARCHAR2, p_names ARRAY_STRING,p_numbers ARRAY_STRING,p_calendar NUMBER,p_event NUMBER,p_user VARCHAR2,p_ipadd VARCHAR2, o_ret_number OUT NUMBER);
    PROCEDURE SP_SET_OFFLINE_FLAG(p_formId NUMBER,p_flag NUMBER);
    PROCEDURE SP_FORM_ACTIVESTATUS_TASKS(p_form NUMBER, p_migrateFlag IN NUMBER);
    pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_LNKFORM', pLEVEL  => PLOG.LFATAL);
END Pkg_Lnkform;
/


CREATE OR REPLACE SYNONYM ESCH.PKG_LNKFORM FOR PKG_LNKFORM;


CREATE OR REPLACE SYNONYM EPAT.PKG_LNKFORM FOR PKG_LNKFORM;


GRANT EXECUTE, DEBUG ON PKG_LNKFORM TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_LNKFORM TO ESCH;

