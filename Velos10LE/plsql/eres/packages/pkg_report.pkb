CREATE OR REPLACE PACKAGE BODY ERES."PKG_REPORT"
AS
 PROCEDURE Sp_GenReport (
   P_REPID          NUMBER,
   P_ACCID          NUMBER,
   P_PARAM         ARRAY_STRING,
   P_PARAMVAL ARRAY_STRING,
   P_SXML     OUT   CLOB
   )
IS

--   V_STR      VARCHAR2 (2000)DEFAULT P_PARAMS || ':';
   V_SQLSTR   LONG;
  -- V_PARAMS   VARCHAR2 (2000)DEFAULT P_PARAMS || ':';
   V_POS      NUMBER         := 0;
   V_CNT      NUMBER         := 0;
   V_SQLXML   CLOB;
   V_INS      VARCHAR2 (4000);
   V_CID      NUMBER;
   V_CID2      NUMBER;
   v_param VARCHAR2 (255);
   v_param_val CLOB;
   V_GENXML NUMBER;
   v_clobtemp CLOB;
   v_ora_version VARCHAR(256);


BEGIN
/*
Resets any XML options set before
*/
   Xmlgen.RESETOPTIONS;
  -- V_CID := SUBSTR (V_STR, 1, INSTR (V_STR, ':') - 1);
  -- v_cid2 := v_cid ;
   -- For RepId 21va global temp table is populated and then the SQL executed on the temp table.
   IF P_REPID = 21 THEN
   DELETE FROM ER_TMPREP;
   V_INS :=
'insert into er_tmprep (
      EVENT_ID               , --1
 CHAIN_ID               , --2
 EVENT_TYPE             , -- 3
 NAME                   , --4
 NOTES                  , --5
 COST                   , --6
 COST_DESCRIPTION       , --7
 DURATION               , --8
 USER_ID                , --9
 LINKED_URI             , --10
 FUZZY_PERIOD           , --11
 MSG_TO                 , --12
 STATUS                 , -- 13
 DESCRIPTION            , --14
 DISPLACEMENT           , --15
 ORG_ID                 , --16
 EVENT_MSG              , --17
 EVENT_RES              , --18
 CREATED_ON             , --19
 EVENT_FLAG             , --20
 EVENTCOST_DESC         , --21
 EVENTCOST_CURR         , --22
 EVE_VALUE          , --23
 EVENTCOST_VALUE        , --24
 FK_EVENT               , --25
 SUM_EVENT              )   --26
      (
       SELECT
 a.EVENT_ID               ,
        a.CHAIN_ID               ,
        a.EVENT_TYPE             ,
        a.NAME                   ,
        a.NOTES                  , --5
        a.COST                   ,
        a.COST_DESCRIPTION       ,
        a.DURATION               ,
        a.USER_ID                ,
        a.LINKED_URI             , --10
        a.FUZZY_PERIOD           ,
        a.MSG_TO                 ,
        a.STATUS                 ,
        a.DESCRIPTION            ,
        a.DISPLACEMENT           , --15
        a.ORG_ID                 ,
        a.EVENT_MSG              ,
        a.EVENT_RES              ,
        a.CREATED_ON             ,
        a.EVENT_FLAG             , --20
        b.EVENTCOST_DESC ,
 b.EVENTCOST_CURR ,
 b.EVENTCOST_VALUE  EVE_VALUE         , --23
        trim(b.EVENTCOST_CURR ||'' '' ||  b.EVENTCOST_VALUE) EVENTCOST_VALUE , --24
        b.FK_EVENT , --25
        (SELECT      MIN(c.eventcost_curr) || '' '' || SUM(c.EVENTCOST_VALUE)
           FROM erv_evecost c
          WHERE c.fk_event = a.event_id   ) sum_event --25
  FROM erv_eveassoc a , erv_evecost b
 WHERE a.event_id = b.fk_event
   AND a.chain_id =  :1
   AND a.DISPLACEMENT > 0  )';
 EXECUTE IMMEDIATE V_INS
   USING V_CID;
 -- If the report is the protocol calender template
 ELSIF p_repid = 44 THEN
  v_ins := 'insert into er_tmprep
 ( event_id , chain_id , displacement , NAME , duration , fuzzy_period  )
   SELECT a.event_id ,
   a.chain_id ,
   a.displacement ,
   a.NAME ,
   a.duration ,
   a.FUZZY_PERIOD
     FROM erv_eveassoc a
    WHERE a.chain_id = :1 '  ;
-- DBMS_OUTPUT.PUT_LINE (substr(v_ins,-200)) ;
 EXECUTE IMMEDIATE V_INS
  USING v_cid ;
  END IF;
   -- Get the Report SQL from the ER_REPORT Table for the preport id p_repid
   SELECT REP_SQL_CLOB,NVL(GENERATE_XML,1)
     INTO V_SQLSTR,V_GENXML
     FROM ER_REPORT
    WHERE PK_REPORT = P_REPID;
   -- Parse the parameter string passed and separate the comma delimited paramters.
   -- Replace the variables in the SQL with the parameters
/*   LOOP
      EXIT WHEN V_PARAMS IS NULL;
      V_CNT := V_CNT + 1;
      V_POS := INSTR (V_STR, ':');
      V_PARAMS := SUBSTR (V_STR, 1, V_POS - 1);
      V_SQLSTR := REPLACE (V_SQLSTR, '~' || V_CNT, V_PARAMS);
      V_STR := SUBSTR (V_STR, V_POS + 1);
    DBMS_OUTPUT.PUT_LINE (SUBSTR(v_sqlstr,1,200)) ;
      DBMS_OUTPUT.PUT_LINE (SUBSTR(v_sqlstr,-200)) ;
   END LOOP;*/
  /*  ******************************************************************************  /
    Go through the report_SQL ,find the keyword prefixed with ':' and replace them
 with values
  /*  ******************************************************************************  */



     FOR i IN 1 .. p_param.COUNT()
              LOOP
     v_param:=p_param(i);
      v_param_val:=p_paramval(i);

--  INSERT INTO T (c,created_on) VALUES(v_param || ' *** ' || v_param_val,SYSDATE);
--  COMMIT;
 IF (p_param(i) = 'patientId' and NVL(dbms_lob.getLength(p_paramval(i)),0)=0) THEN 
       v_param_val:='[ALL]';
       plog.DEBUG(pCTX,'v_param 000 ' || v_param || ' v_param_val2 [ALL] '||v_param_val);                
      END IF;

     IF (v_param_val='[ALL]') THEN
     -- check if any SQL is defined within the mapping,If not,use the generic one
      SELECT repfiltermap_valuesql INTO v_param_val FROM ER_REPFILTERMAP WHERE repfiltermap_repcat=
      (SELECT (SELECT codelst_custom_col FROM ER_CODELST WHERE codelst_subtyp=rep_type AND codelst_type='report') AS repcat  FROM ER_REPORT WHERE pk_report=p_repid)
      AND fk_repfilter=(SELECT pk_repfilter FROM ER_REPFILTER WHERE repfilter_keyword=v_param);
      --if not defined for mapping
      IF (NVL(dbms_lob.getLength(v_param_val),0)=0)  THEN

       SELECT repfilter_valuesql INTO v_param_val FROM ER_REPFILTER WHERE
       repfilter_keyword=v_param;


      plog.DEBUG(pCTX,'v_param_val2'||v_param_val);
      END IF;
      END IF;



     IF ( INSTR(v_sqlstr,':'||v_param) >0) THEN
     v_sqlstr:=REPLACE(v_sqlstr,':'||v_param,v_param_val);
     END IF;
  END LOOP;

 --Run the Loop again so that value inserted through repfilter_valuesql should be replaced
 -- with value. It's better to run the loop here than to keep running this loop withtin previous loop
 --to replace the values in valueSQL.Since oracle does not support searching a value in a collection
 --we will have to run loop for each value where '[ALL]' was selected
      FOR i IN 1 .. p_param.COUNT()
              LOOP
     v_param:=p_param(i);
      v_param_val:=p_paramval(i);

      IF (p_param(i) = 'patientId' and NVL(dbms_lob.getLength(p_paramval(i)),0)=0) THEN 
       v_param_val:='[ALL]';
           plog.DEBUG(pCTX,'v_param' || v_param || ' v_param_val2 [ALL] '||v_param_val);
           else
            plog.DEBUG(pCTX,'Else v_param' || v_param || ' v_param_val2  '||v_param_val);           
      END IF;
      --Replace any [ALL] values
       IF (v_param_val='[ALL]') THEN
     -- check if any SQL is defined within the mapping,If not,use the generic one
      SELECT repfiltermap_valuesql INTO v_param_val FROM ER_REPFILTERMAP WHERE repfiltermap_repcat=
      (SELECT (SELECT codelst_custom_col FROM ER_CODELST WHERE codelst_subtyp=rep_type AND codelst_type='report') AS repcat  FROM ER_REPORT WHERE pk_report=p_repid)
      AND fk_repfilter=(SELECT pk_repfilter FROM ER_REPFILTER WHERE repfilter_keyword=v_param);
      --if not defined for mapping
      IF (NVL(dbms_lob.getLength(v_param_val),0)=0)  THEN

       SELECT repfilter_valuesql INTO v_param_val FROM ER_REPFILTER WHERE
       repfilter_keyword=v_param;


      plog.DEBUG(pCTX,'v_param_val2'||v_param_val);
      END IF;
      END IF;

      --end Replacing

     IF ( INSTR(v_sqlstr,':'||v_param) >0) THEN
     v_sqlstr:=REPLACE(v_sqlstr,':'||v_param,v_param_val);
     END IF;
  END LOOP;

   --DBMS_OUTPUT.PUT_LINE (substr(v_sqlstr,1,255)) ;
   -- DBMS_OUTPUT.PUT_LINE (substr(v_sqlstr,245,255)) ;
   -- Execute the SQL created, pass it as a variable in the XMLGEN.getXML procedure
   -- This will return the XML for the SQL. Even if the SQL returns no rows the XML will be created


     IF V_GENXML = 1 THEN

  --This need to be fixed and f_doced should be enabled. Rajeev commented this line temporarily to run
  -- a client report.
--   v_sqlstr:=Pkg_Util.f_decode(v_sqlstr);
  v_sqlstr := REPLACE(v_sqlstr, '[VELSQUOTE]','''');

  P_SXML := F_GETXML(V_SQLSTR);


--      P_SXML := REPLACE (P_SXML, '&amp;#', '&#');
--    P_SXML := REPLACE (P_SXML, '<?xml version = ''1.0''?>', '<?xml version = ''1.0''  encoding=''windows-1253''?>');


--     plog.fatal(pctx,'P_SXML' || P_SXML);
 ELSE

  --INSERT INTO T(C) VALUES(v_sqlstr);
  --COMMIT;

--  plog.DEBUG(pctx,'V_SQLSTR' || V_SQLSTR);

EXECUTE IMMEDIATE V_SQLSTR INTO P_SXML;
 END IF;
     DBMS_OUTPUT.PUT_LINE ('XML ' || DBMS_LOB.SUBSTR (P_SXML, 200, 200));

END;

FUNCTION F_Gen_PatDump(p_study VARCHAR2,p_site VARCHAR2,p_patient VARCHAR2,p_txarm VARCHAR2,p_startdate VARCHAR2, p_enddate VARCHAR2)
RETURN  CLOB
AS
v_xml CLOB := EMPTY_CLOB();
v_studynum VARCHAR2(100);
v_studytitle VARCHAR2(4000);
v_site VARCHAR2(100);
v_count NUMBER;
v_temp VARCHAR2(30);
tab_study split_tbl;
tab_org split_tbl;
v_study VARCHAR2(2000);
v_org VARCHAR2(20);
v_counter NUMBER :=1;
v_perCode VARCHAR2(20);
v_perLName VARCHAR2(200);
v_perFName VARCHAR2(200);
v_initials VARCHAR2(10);
v_gender VARCHAR2(50);
v_enrollDt VARCHAR2(20);
v_patStudyId VARCHAR2(20);
v_race VARCHAR2(50);
v_ethnicity VARCHAR2(50);
v_deathDate VARCHAR2(20);
v_enrollPhysn VARCHAR2(200);
v_assndTo VARCHAR2(200);
v_sql VARCHAR2(4000);
v_cur Gk_Cv_Types.GenericCursorType;
v_statcur Gk_Cv_Types.GenericCursorType;
v_statSQL VARCHAR2(4000);
v_perPk NUMBER;
v_statDate VARCHAR2(20);
v_statReason VARCHAR2(4000);
v_statStr CLOB;
v_headerStr VARCHAR2(4000);
v_txarm VARCHAR2(100);
v_where VARCHAR2(4000);
v_add CHAR(1);
v_infVer VARCHAR2(250);
v_txPK NUMBER;
v_off_study_stat NUMBER;
v_off_treat_stat NUMBER;
v_off_study_count NUMBER;
v_off_treat_count NUMBER;
v_tx_desc 		  VARCHAR2(4000);
v_tempStr VARCHAR2(32000);
v_finalclob CLOB := EMPTY_CLOB();

 pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'f_gen_patdump', pLEVEL  => Plog.LDEBUG);
BEGIN

	DBMS_LOB.CREATETEMPORARY(v_xml,TRUE);
	DBMS_LOB.CREATETEMPORARY(v_finalclob,TRUE);


--v_xml := v_xml || '<ROWSET>';
--Create header String
v_add:='Y';
   FOR k IN (SELECT pk_codelst,codelst_subtyp,codelst_desc FROM ER_CODELST WHERE codelst_type='patStatus' ORDER BY codelst_seq )
   LOOP
   IF (LENGTH(v_headerStr)>0)  THEN
   v_headerStr:=v_headerStr||'<name>'||k.codelst_desc||'</name>';
   ELSE
--   v_headerStr:='<'||k.codelst_subtyp||'>'||k.codelst_desc||'</'||k.codelst_subtyp||'>';
   v_headerStr:='<name>'||k.codelst_desc||'</name>';
   END IF;
   END LOOP;
 v_headerStr:='<HEADER>'||v_headerStr||'</HEADER>';
--End header

SELECT Pkg_Util.f_split(p_study) INTO tab_study FROM dual;
plog.DEBUG(pCTX,'v_xml2-'||v_xml);
SELECT Pkg_Util.f_split(p_site) INTO tab_org FROM dual;

 -- get off treatment code
 SELECT Pkg_Util.f_getcodepk ('offtreat', 'patStatus')
 INTO v_off_treat_stat FROM dual;

 -- get off study code
 SELECT Pkg_Util.f_getcodepk ('offstudy', 'patStatus')
 INTO v_off_study_stat FROM dual;

 ----------


 FOR i IN (SELECT COLUMN_VALUE AS study FROM TABLE(tab_study))
 LOOP
 v_study:=i.study;
 plog.DEBUG(pCTX,'study-'||v_study);

  -- get offtreatment count
 SELECT COUNT(DISTINCT fk_per)
 INTO v_off_treat_count
 FROM ER_PATPROT pp, ER_PER WHERE fk_study = v_study AND
   pk_per = fk_per AND fk_site IN (SELECT COLUMN_VALUE AS org FROM TABLE(tab_org))
 AND  patprot_stat=1
 AND  ( (patprot_enroldt BETWEEN  TO_DATE(p_startdate,PKG_DATEUTIL.F_GET_DATEFORMAT)  AND TO_DATE(p_enddate,PKG_DATEUTIL.F_GET_DATEFORMAT)) OR patprot_enroldt IS NULL) AND
 EXISTS (SELECT * FROM ER_PATSTUDYSTAT P WHERE P.fk_per = pp.fk_per AND P.fk_study = pp.fk_study
 AND fk_codelst_stat = v_off_treat_stat);

  -- get off study count  Select count(distinct fk_per)
 SELECT COUNT(DISTINCT fk_per)
 INTO v_off_study_count
 FROM ER_PATPROT pp, ER_PER WHERE fk_study = v_study
 AND  pk_per = fk_per AND fk_site IN (SELECT COLUMN_VALUE AS org FROM TABLE(tab_org))
 AND  patprot_stat=1
 AND  ( (patprot_enroldt BETWEEN  TO_DATE(p_startdate,PKG_DATEUTIL.F_GET_DATEFORMAT)  AND TO_DATE(p_enddate,PKG_DATEUTIL.F_GET_DATEFORMAT)) OR patprot_enroldt IS NULL) AND
 EXISTS (SELECT * FROM ER_PATSTUDYSTAT P WHERE P.fk_per = pp.fk_per AND P.fk_study = pp.fk_study
 AND fk_codelst_stat = v_off_study_stat);



 -----------------------
 FOR j IN (SELECT COLUMN_VALUE AS org FROM TABLE(tab_org))
 LOOP
 v_org:=j.org;
 plog.DEBUG(pCTX,'site-'||v_org);
SELECT study_number, study_title INTO v_studynum, v_studytitle FROM ER_STUDY WHERE pk_study  = v_study;

-- Bug fix for #3526 SM 06/11/08
SELECT replace(site_name, '&', '&amp;') INTO v_site FROM ER_SITE WHERE pk_site =  v_org;

plog.DEBUG(pCTX,'TXARM'||p_txarm||'date'||p_startdate||'date1'||p_enddate);

v_sql:= ' select * from (SELECT  fk_per,person_code AS patient_id,patprot_patstdid AS patient_study_id,person_lname as patient_lname , person_fname AS patient_fname,SUBSTR(Person_fname,1,1) || SUBSTR(person_mname,1,1) || SUBSTR(person_lname,1,1) as initials,f_get_codelstdesc(fk_codelst_gender) as gender,(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_race) AS race,(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_ethnicity) AS ethnicity,
TO_CHAR(person_deathdt,PKG_DATEUTIL.F_GET_DATEFORMAT) AS death_date,(SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = patprot_physician) AS enroll_physn,(SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = fk_userassto) AS usr_assdto,TO_CHAR(patprot_enroldt,PKG_DATEUTIL.F_GET_DATEFORMAT) AS enrolled_on ,'
|| '(SELECT tx_name FROM ER_STUDYTXARM, ER_PATTXARM WHERE pk_studytxarm = fk_studytxarm AND  fk_patprot in (select pk_patprot from er_patprot where fk_per = pk_person and fk_study='||v_study||') AND tx_start_date = (SELECT MIN(tx_start_date) FROM ER_PATTXARM WHERE fk_patprot  in  ( select pk_patprot from er_patprot where fk_per = pk_person and fk_study='||v_study||') ) AND ROWNUM = 1) AS txarm,'
|| '(SELECT fk_studytxarm FROM ER_STUDYTXARM, ER_PATTXARM WHERE pk_studytxarm = fk_studytxarm AND fk_patprot in (select pk_patprot from er_patprot where fk_per = pk_person and fk_study='||v_study||') AND tx_start_date = (SELECT MIN(tx_start_date) FROM ER_PATTXARM WHERE fk_patprot in (select pk_patprot from er_patprot where fk_per = pk_person and fk_study='||v_study||') ) AND ROWNUM = 1) AS fk_studytxarm'
||'  FROM ER_PATPROT ,epat.PERSON  WHERE fk_study='||v_study
|| '  AND pk_person=fk_per AND  fk_site='||v_org || '  AND  patprot_stat=1 and  (patprot_enroldt BETWEEN  to_date('''||p_startdate||''',PKG_DATEUTIL.F_GET_DATEFORMAT)  AND to_date(''' ||p_enddate||''',PKG_DATEUTIL.F_GET_DATEFORMAT) or patprot_enroldt is null))';

--Put txarm filter
v_where:='';

/*
IF (LENGTH(p_txarm) >0) THEN
   IF (LENGTH(v_where)>0) THEN
   v_where:=v_where|| '  and fk_studytxarm  in ('||p_txarm||')';
--      v_where:=v_where|| '  and (fk_studytxarm is null or fk_studytxarm  in ('||p_txarm||'))';
 ELSE
 v_where:='fk_studytxarm  in ('||p_txarm||')';
--   v_where:='fk_studytxarm is null or fk_studytxarm  in ('||p_txarm||')';
 END IF;
END IF;
*/

plog.DEBUG(pCTX,'v_where'||v_where||'v_Add'||v_Add);
--IF ((LENGTH(v_where)>0) AND (v_add='Y') ) THEN
IF ((LENGTH(v_where)>0) ) THEN
v_sql:=v_sql||' where  '||v_where;
--v_add:='N';
END IF;
v_sql:=v_sql|| '  order by enrolled_on desc';

plog.DEBUG(pCTX,'SQL'||v_sql);
OPEN v_cur FOR v_sql;
 LOOP
 FETCH v_cur INTO v_perPk,v_perCode,v_patStudyId,v_perLName,v_perFName,v_initials,v_gender,v_race,v_ethnicity,v_deathDate,v_enrollPhysn,v_assndTo,v_enrollDt,v_txarm,v_txPk;
 EXIT WHEN v_cur %NOTFOUND;
--   v_xml := v_xml || '<ROW num="'||v_counter||'">';

 -- get TX description

 --	v_tempclob :=  empty_clob();
v_tempStr := '';

 	IF (NVL(v_txPk,0) > 0) THEN
	 	SELECT  NVL(tx_desc,'&#xa0;' ) INTO v_tx_desc FROM ER_STUDYTXARM WHERE pk_studytxarm = v_txPk;
	ELSE
		v_tx_desc  := '&#xa0;';
	END IF;

v_tempStr:= v_tempStr || '<ROW num="'||TO_CHAR(v_counter)||'">';
   v_tempStr := v_tempStr  ||'<STUDY_NUM>' || v_studynum ||  '</STUDY_NUM><OFFTREAT_CT>'||v_off_treat_count ||'</OFFTREAT_CT><OFFSTUDY_CT>'||v_off_study_count ||'</OFFSTUDY_CT><SITE>' ||v_site || '</SITE><PATIENT_ID>'||v_perCode||'</PATIENT_ID><PATIENT_STUDY_ID>'||v_patStudyId||'</PATIENT_STUDY_ID>';
  v_tempStr :=v_tempStr ||'<PATIENT_LNAME>'|| NVL(v_perLName, '&#xa0;') ||'</PATIENT_LNAME><PATIENT_FNAME>'|| NVL(v_perFName,'&#xa0;') ||'</PATIENT_FNAME><INITIALS>'|| NVL(v_initials, '&#xa0;') ||'</INITIALS><GENDER>'|| NVL(v_gender, '&#xa0;') ||'</GENDER><TXARM>'||NVL(v_txarm,'&#xa0;')||'</TXARM><TXDESC>'||  v_tx_desc  || '</TXDESC><RACE>'||NVL(v_race,'&#xa0;') ||'</RACE><ETHNICITY>'|| NVL(v_ethnicity,'&#xa0;')||'</ETHNICITY><DEATH_DATE>'|| NVL(v_deathDate, '&#xa0;') ||'</DEATH_DATE><ENROLL_PHYSN>'|| NVL(v_enrollPhysn, '&#xa0;') ||'</ENROLL_PHYSN><USR_ASSDTO>'|| NVL(v_assndTo, '&#xa0;') ||'</USR_ASSDTO>';
   v_counter:=v_counter+1;

   FOR i IN (SELECT pk_codelst,codelst_subtyp,codelst_desc FROM ER_CODELST WHERE codelst_type='patStatus'  ORDER BY codelst_seq)
   LOOP
      v_statStr:='';
   v_statSQL:= 'SELECT  to_char(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) as statDate , (select codelst_desc from er_codelst where pk_codelst=patstudystat_reason) as reason, inform_consent_ver  FROM ER_PATSTUDYSTAT WHERE fk_per='||v_perPk||' and fk_study='||v_study|| ' and fk_codelst_stat='||i.pk_codelst||'  order by patstudystat_date desc'   ;
--   OPEN v_statcur FOR v_statSQL;
   FOR x IN (SELECT  TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) AS statDate , (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst=patstudystat_reason) AS reason, inform_consent_ver  FROM ER_PATSTUDYSTAT WHERE fk_per=v_perPk AND fk_study=v_study AND fk_codelst_stat= i.pk_codelst  ORDER BY patstudystat_date DESC)
   LOOP
--   FETCH v_statcur INTO  v_statDate,v_statReason, v_infVer;
--    EXIT WHEN v_statcur %NOTFOUND;
v_statDate:=x.statDate;
 IF (i.codelst_subtyp='infConsent') THEN
 IF (LENGTH(v_statStr)>0) THEN
-- v_statStr:=v_statStr||' , '||CHR(13)||v_statDate;
 v_statStr:=v_statStr||' , '||CHR(13)|| x.statDate;
 ELSE
-- v_statStr:=v_statDate||' Ver. '||v_infVer||'';
 v_statStr:=v_statDate||' Ver. '|| x.inform_consent_ver||'';
 END IF;
 ELSE
 IF (LENGTH(v_statStr)>0) THEN
-- v_statStr:=v_statStr||' , '||CHR(13)||v_statDate||' '||v_statReason;
 v_statStr:=v_statStr||' , '||CHR(13)||v_statDate||' '||x.reason;
 ELSE
-- v_statStr:=v_statDate||' '||v_statReason;
 v_statStr:=v_statDate||' '||x.reason;
 END IF;
END IF;
   END LOOP; --end loop for status SQL

 v_tempStr  := v_tempStr  ||'<data>'|| NVL(v_statStr,'&#xa0;') ||'</data>';

  END LOOP; -- loop for codelst types

  v_tempStr  :=  v_tempStr || '</ROW>';

   dbms_lob.writeappend( v_xml, LENGTH(v_tempStr), v_tempStr );

 --  plog.DEBUG(pCTX,'v_xml-final'||v_xml);


 END LOOP;

END LOOP;

END LOOP;



   --v_xml :='<ROWSET>' ||v_headerStr|| v_xml || '</ROWSET>';

   dbms_lob.WRITEAPPEND (  v_finalclob, 8, '<ROWSET>' );
   dbms_lob.WRITEAPPEND (  v_finalclob, LENGTH(v_headerStr), v_headerStr );
   dbms_lob.APPEND (  v_finalclob, v_xml);
    dbms_lob.WRITEAPPEND (  v_finalclob, 9, '</ROWSET>' );


  -- plog.DEBUG(pCTX,'v_xml-final2'||v_finalclob);
--    plog.DEBUG(pCTX,'final-'||v_xml);



  RETURN v_finalclob;
END;

FUNCTION F_Gen_UsrProfile(p_user VARCHAR2)
RETURN  CLOB
AS
v_userclob CLOB ;
v_group CLOB ;
v_site CLOB ;
v_username VARCHAR2(200);
v_experience VARCHAR2(4000);
v_phases VARCHAR2(200);
v_jobtype VARCHAR2(4000);
v_special VARCHAR2(4000);
v_address VARCHAR2(4000);
v_city VARCHAR2(200);
v_state VARCHAR2(200);
v_zip VARCHAR2(50);
v_country VARCHAR2(200);
v_phone VARCHAR2(200);
v_email VARCHAR2(4000);
v_groupname VARCHAR2(200);
v_groupdft VARCHAR2(50);
v_sitename VARCHAR2(200);
v_primsite VARCHAR2(50);
v_study CLOB;
v_studypk NUMBER;
v_studynum VARCHAR2(200);
v_studytitle VARCHAR2(4000);
v_teamrole VARCHAR2(200);
v_orgname CLOB;
v_org CLOB;
v_notify CLOB;
v_ntype CLOB;
v_nstudy CLOB;
v_npatient CLOB;
v_nform CLOB;
v_ncalendar CLOB;
v_nvisit CLOB;
v_ndesc CLOB;

v_primary_site NUMBER;
v_user VARCHAR2(200);
--v_finalclob CLOB := EMPTY_CLOB();
v_index_comma NUMBER;
v_account Number;
BEGIN
--temporary fix so that if multiple users are passed, the report does not crash
v_user  := p_user;
v_index_comma  := INSTR(v_user,',');

IF  v_index_comma  > 0 THEN
	v_user  := SUBSTR(v_user,1, (v_index_comma -1)  );
END IF  ;


--collect user data
SELECT usr_firstname || ' ' || usr_lastname,usr_wrkexp,usr_pahseinv,F_Get_Codelstdesc(fk_codelst_jobtype),F_Get_Codelstdesc(fk_codelst_spl),address,add_city,
       add_state,add_zipcode,add_country,add_phone,add_email,fk_siteid,ER_USER.fk_account
INTO v_username,v_experience,v_phases,v_jobtype,v_special,v_address,v_city,v_state,v_zip,v_country,v_phone,v_email,v_primary_site,v_account
FROM ER_USER,ER_ADD
WHERE pk_user = v_user AND pk_add = fk_peradd ;

	  --escape spl chars
	    v_username := Pkg_Util.f_escapespecialcharsforxml (v_username);
	    v_experience := Pkg_Util.f_escapespecialcharsforxml (v_experience);
	    v_address := Pkg_Util.f_escapespecialcharsforxml (v_address);
	    v_email := Pkg_Util.f_escapespecialcharsforxml (v_email);

--open <user> and add user data as attributes
v_userclob := v_userclob || '<user name="' || v_username || '" experience="' || v_experience || '" phases="' || v_phases || '" jobtype="' || v_jobtype || '" special="' || v_special || '" Address="' || v_address || '" city="' || v_city || '" state="' || v_state || '" zip="' || v_zip || '" country="' || v_country || '" phone="' || v_phone || '" email="' || v_email || '"> ' ;

--open <groups>
v_group := v_group || '<groups>' ;

--collect group data
FOR i IN (SELECT grp_name,CASE WHEN (SELECT fk_grp_default FROM ER_USER WHERE fk_user = pk_user)= fk_grp THEN 'Yes' ELSE 'No' END AS grpdef
          FROM ER_USRGRP,ER_GRPS WHERE fk_user = v_user AND fk_grp = pk_grp ORDER BY  1 )
   LOOP
    v_groupname := i.grp_name;

    v_groupdft := i.grpdef;

   v_groupname := Pkg_Util.f_escapespecialcharsforxml (v_groupname);

      --open <group> and add group data as attributes
      v_group := v_group || '<group name="' || v_groupname || '" default="' || v_groupdft || '"></group>'; --close <group>
   END LOOP;

--close <groups>
v_group := v_group || '</groups>';

--open <sites>
v_site := v_site || '<sites>' ;

--collect site data
-- Bug fix for #3015-1 By SM 06/18/08
FOR i IN (SELECT distinct site_name,CASE WHEN (v_primary_site = pk_site) THEN 'Yes' ELSE 'No' END AS sitedef
      FROM ER_STUDY_SITE_RIGHTS,ER_SITE WHERE fk_user = v_user AND fk_site = pk_site AND user_study_site_rights > 0)
    LOOP

   v_sitename := i.site_name;

    v_sitename := Pkg_Util.f_escapespecialcharsforxml (v_sitename);

   v_primsite := i.sitedef;

      --open <site> and add site data as attributes
      v_site:= v_site || '<site name="' || v_sitename || '" primary="' ||  v_primsite || '"/>'; --close <site>
   END LOOP;

--close <sites>
v_site := v_site || '</sites>';

--open <studies>
v_study := v_study || '<studies>';

--collect study data
FOR i IN (SELECT pk_study,study_number,study_title,F_Get_Codelstdesc(fk_codelst_tmrole) strole
          FROM ER_STUDYTEAM,ER_STUDY WHERE fk_account = v_account and pk_study = fk_study AND fk_user = v_user and study_team_usr_type <> 'X'
          union
          SELECT pk_study,study_number,study_title,'Super User'
          from ER_STUDY WHERE fk_account = v_account  and  Pkg_Superuser.F_Is_Superuser(v_user, pk_study) = 1
          ORDER BY  2)
   LOOP

   v_studypk := i.pk_study;
   v_studynum := i.study_number;
   v_studytitle := i.study_title;
   v_teamrole := i.strole;

   v_studytitle := Pkg_Util.f_escapespecialcharsforxml (v_studytitle);
   v_studynum := Pkg_Util.f_escapespecialcharsforxml ( v_studynum);
   v_teamrole := Pkg_Util.f_escapespecialcharsforxml ( v_teamrole);

      --open <study> and add study data as attributes
      v_study:= v_study || '<study number="' || v_studynum || '" title="' || v_studytitle || '" role="' || v_teamrole || '" org="';

      v_orgname := '';

      -- Bug fix for #3015-2 By SM 05/29/08
      if v_teamrole = 'Super User' then
        v_orgname := 'All the Organizations user has access as per User Details page';
      else
        -- Bug fix for #3015-4 By SM 06/18/08
        --collect Site Org Name
        FOR j IN (SELECT distinct site_name,fk_study FROM ER_STUDY_SITE_RIGHTS,ER_SITE WHERE fk_user = v_user AND fk_study = v_studypk AND pk_site = fk_site AND user_study_site_rights > 0 ORDER BY  1)
          LOOP

          v_orgname := v_orgname ||  Pkg_Util.f_escapespecialcharsforxml (j.site_name) || ', ';

          END LOOP;
      end if;

      v_org := trim(TRAILING ',' FROM (trim(TRAILING ' ' FROM v_orgname)));

      --open <org> and add org names
       v_study:= v_study || v_org || '"></study>'; --close <org> --close <study>

      END LOOP;

--close <studies>
v_study:= v_study || '</studies>';

--open <notifications>
v_notify := v_notify || '<notifications>' ;

--collect notification information
FOR i IN (
				SELECT TYPE,study_number   study,
	       NULL AS  patient,(SELECT form_name FROM ER_FORMLIB WHERE pk_formlib = fk_crf) FORM,
	       (SELECT NAME FROM event_assoc WHERE event_id = fk_cal) calendar,(SELECT visit_name FROM sch_protocol_visit WHERE pk_protocol_visit = fk_visit) visit,description
	FROM (
	  SELECT alnot_users users,'Alert' TYPE,fk_study,fk_patprot,fk_protocol fk_cal,NULL fk_crf,NULL fk_visit,
	         (SELECT codelst_desc FROM sch_codelst WHERE pk_codelst = fk_codelst_an) description FROM sch_alertnotify WHERE fk_patprot IS NULL AND alnot_users IS NOT NULL AND   ',' || alnot_users || ',' LIKE '%,' || v_user || ',%'
	  UNION
		    SELECT fn_users,'Form',fk_study,NULL,NULL,fn.fk_formlib,NULL,
	         DECODE(fn_sendtype,'F' ,'First time form is filled' , 'Every time form is filled')
	  FROM ER_FORMNOTIFY fn,ER_LINKEDFORMS sf WHERE fn.fk_formlib = sf.fk_formlib AND  fn_msgtype = 'N'
	  AND fn_users IS NOT NULL AND   ',' || fn_users || ',' LIKE '%,' || v_user || ',%'
	  UNION
	  SELECT milestone_usersto,'Milesone',fk_study,NULL,fk_cal,NULL,NULL,Pkg_Milestone_New.f_getmilestonedesc(pk_milestone) FROM ER_MILESTONE
	  WHERE milestone_usersto IS NOT NULL AND ',' || milestone_usersto || ',' LIKE '%,' || v_user || ',%'
	  UNION
	  SELECT TO_CHAR(eventusr),'Calendar',CASE WHEN event_type = 'P' THEN chain_id ELSE (SELECT ea.chain_id FROM event_assoc ea WHERE e.chain_id = ea.event_id) END,NULL,
	         CASE WHEN event_type = 'P' THEN event_id ELSE (SELECT ea.event_id FROM event_assoc ea WHERE e.chain_id = ea.event_id) END,NULL,fk_visit,
	         CASE WHEN usr_daysbefore IS NULL THEN '' WHEN usr_daysbefore = 0 THEN '' ELSE 'Message sent to user ' || usr_daysbefore || ' days before event: ' || NAME END ||
	         '   ' || CASE WHEN usr_daysafter IS NULL THEN '' WHEN usr_daysafter = 0 THEN '' ELSE 'Message sent to user ' || usr_daysafter || ' days after event: ' || NAME END descript
	  FROM event_assoc e RIGHT JOIN sch_eventusr ON fk_event = event_id WHERE eventusr_type = 'S'
	  AND ',' || eventusr || ',' LIKE '%,' || v_user || ',%'
	  AND (usr_daysbefore IS NOT NULL OR usr_daysafter IS NOT NULL)) a
	LEFT JOIN ER_STUDY ON pk_study = fk_study
ORDER BY TYPE
)
  LOOP
    v_ntype := i.TYPE;
    v_nstudy := CASE WHEN i.study IS NULL THEN NULL ELSE 'Study=' || i.study END;
    v_npatient := CASE WHEN i.patient IS NULL THEN NULL ELSE 'Patient=' || i.patient END;
    v_nform := CASE WHEN i.FORM IS NULL THEN NULL ELSE 'Form=' || i.FORM END;
    v_ncalendar := CASE WHEN i.calendar IS NULL THEN NULL ELSE 'Calendar=' || i.calendar END;
    v_nvisit := CASE WHEN i.visit IS NULL THEN NULL ELSE 'Visit=' || i.visit END;
    v_ndesc := i.description;

	    v_nstudy  := Pkg_Util.f_escapespecialcharsforxml (v_nstudy ) ;
	    v_npatient   := Pkg_Util.f_escapespecialcharsforxml (v_npatient  ) ;
       v_nform    := Pkg_Util.f_escapespecialcharsforxml (   v_nform  ) ;
       v_ncalendar    := Pkg_Util.f_escapespecialcharsforxml (   v_ncalendar ) ;
	    v_nvisit    := Pkg_Util.f_escapespecialcharsforxml (    v_nvisit  ) ;
		 v_ndesc    := Pkg_Util.f_escapespecialcharsforxml (   v_ndesc ) ;

    --open <notify> and add attributes
    v_notify := v_notify || '<notify type="' || v_ntype || '" link="' || v_nstudy || ' ' || v_npatient || ' ' || v_nform || ' ' || v_ncalendar || ' ' || v_nvisit;
    v_notify := v_notify || '" Description="' || v_ndesc || '"/>';

  END LOOP;

  --close <notifications>
  v_notify := v_notify || '</notifications>';

--add <groups>, <sites> and <studies> and close <user>

--v_user := v_user ||     '</user>';

v_userclob := v_userclob || v_group || v_site || v_study || v_notify || '</user>';



  RETURN v_userclob;

END;

END Pkg_Report;
/
