CREATE OR REPLACE PACKAGE BODY        "PKG_IMPEXLOGGING" 
AS
PROCEDURE SP_RECORDLOG (p_expreqid NUMBER, p_logdate DATE, p_severity VARCHAR2, p_messagecode VARCHAR2, p_logdesc VARCHAR2, p_module VARCHAR2, o_success OUT NUMBER )
 AS
  sql_stmt VARCHAR2(4000);
  v_date DATE;

  v_logdata LONG;

--  pCTX PLOG.LOG_CTX := PLOG.init (pSECTION     => 'A2A',
  --                                pLEVEL       => PLOG.LDEBUG);


   BEGIN
	v_date := p_logdate;
	IF v_date IS NULL THEN
		v_date := SYSDATE;
	END IF;
	BEGIN

/*	INSERT INTO er_explogs (PK_LOG, FK_REQLOG, LOG_DATETIME,LOG_SEVERITY,
	LOG_MESSAGECODE,LOG_DESC,LOG_MODULE)
	VALUES (SEQ_ER_EXPLOGS.nextval,p_expreqid,v_date, p_severity,
	p_messagecode,p_logdesc,p_module);
	o_success := 0;
	Commit; */

	--log the record in log file

	--RPAD(NVL(p_messagecode ,' '),10,' ')

	--log the record in log file


	v_logdata := '[VEL]' || p_expreqid || '[VEL]' || v_date || '[VEL]' ||  p_severity || '[VEL]'|| p_messagecode || '[VEL]'
	|| p_logdesc  || '[VEL]'|| p_module || '[VEL]' ;

	--check the p_Severity and use appropriate Plog method.


	--SP_UPDATE_LOGFILE ('A2ALOGS',p_expreqid || '.log', substr(v_logdata,1,4000));

		IF p_severity = 'FATAL' THEN
		   PLog.fatal(PKG_IMPEXLOGGING.pCTXA2A ,v_logdata);
		ELSIF p_severity = 'WARNING' THEN
			PLog.warn(PKG_IMPEXLOGGING.pCTXA2A ,v_logdata);
		ELSE
		 	PLog.DEBUG(PKG_IMPEXLOGGING.pCTXA2A,v_logdata);
		END IF;


	EXCEPTION
	WHEN OTHERS THEN
		o_success := -1;
         		RAISE_APPLICATION_ERROR (
            		-20000,
            		'Unhandled Oracle error ' || SUBSTR (SQLERRM, 1, 100)
         			);
	END;
	END;


PROCEDURE SP_RECORDLOG (p_expreqid NUMBER, p_severity VARCHAR2, p_messagecode VARCHAR2, p_logdesc VARCHAR2, p_module VARCHAR2, p_sitecode VARCHAR2, o_success OUT NUMBER )
 AS
  sql_stmt VARCHAR2(4000);
  v_date DATE;
  v_logdata LONG;

  --pCTX PLOG.LOG_CTX := PLOG.init (pSECTION     => 'A2A',
                                  --pLEVEL       => PLOG.LDEBUG);

   BEGIN

		v_date := SYSDATE;

	BEGIN

	/*	INSERT INTO er_explogs (PK_LOG, FK_REQLOG, LOG_DATETIME,LOG_SEVERITY,
		LOG_MESSAGECODE,LOG_DESC,LOG_MODULE,SITE_CODE)
		VALUES (SEQ_ER_EXPLOGS.nextval,p_expreqid,v_date, p_severity,
		p_messagecode,p_logdesc,p_module, p_sitecode);

		o_success := 0;

	Commit; */

	--log the record in log file


	v_logdata := '[VEL]' || p_expreqid || '[VEL]' || v_date || '[VEL]' ||  p_severity || '[VEL]'|| p_messagecode ||
	 '[VEL]'||	 p_logdesc  || '[VEL]'|| p_module || '[VEL]' || p_sitecode || '[VEL]';

	--check the p_Severity and use appropriate Plog method.


	--SP_UPDATE_LOGFILE ('A2ALOGS',p_expreqid || '.log', substr(v_logdata,1,4000));

		IF UPPER(p_severity) = 'FATAL' THEN
		   PLog.fatal(PKG_IMPEXLOGGING.pCTXA2A ,v_logdata);
		ELSIF UPPER(p_severity) = 'WARNING' THEN
			PLog.warn(PKG_IMPEXLOGGING.pCTXA2A,v_logdata);
		ELSE
		 	PLog.DEBUG(PKG_IMPEXLOGGING.pCTXA2A ,v_logdata);
		END IF;


	EXCEPTION
	WHEN OTHERS THEN
		o_success := -1;
         		RAISE_APPLICATION_ERROR (
            		-20000,
            		'Unhandled Oracle error ' || SUBSTR (SQLERRM, 1, 100)
         			);
	END;
	END;



PROCEDURE SP_RECORDLOG (p_expreqid NUMBER,p_logdesc VARCHAR2, p_module VARCHAR2, o_success OUT NUMBER )
AS
BEGIN
	 SP_RECORDLOG (p_expreqid, SYSDATE, 'Debug', NULL, p_logdesc, p_module, o_success);
END;

PROCEDURE SP_RECORDLOG (p_expreqid NUMBER,p_logdesc VARCHAR2, o_success OUT NUMBER )
AS
BEGIN
	 SP_RECORDLOG (p_expreqid, SYSDATE, 'Debug', NULL, p_logdesc, 'Common', o_success);
END;

PROCEDURE SP_RECORDLOG (p_expreqid NUMBER, p_severity VARCHAR2, p_messagecode VARCHAR2, p_logdesc VARCHAR2, p_module VARCHAR2, o_success OUT NUMBER )
AS
BEGIN
	 SP_RECORDLOG (p_expreqid, SYSDATE, p_severity, p_messagecode, p_logdesc, p_module, o_success);
END;

PROCEDURE SP_RECORDLOG (p_expreqid NUMBER, p_severity VARCHAR2, p_messagecode VARCHAR2, p_logdesc VARCHAR2, o_success OUT NUMBER )
AS
BEGIN
	 SP_RECORDLOG (p_expreqid, SYSDATE, p_severity , p_messagecode , p_logdesc, 'Common', o_success);
END;


PROCEDURE SP_RECORDLOG (p_logdesc VARCHAR2, o_success OUT NUMBER )
AS
BEGIN
	 SP_RECORDLOG (0, SYSDATE, 'debug', 'MSG' , p_logdesc, 'From DB', o_success);
END;

PROCEDURE SP_RECORDLOGSITE (p_expreqid NUMBER,p_logdesc VARCHAR2, p_module VARCHAR2, p_sitecode VARCHAR2, o_success OUT NUMBER )
AS
BEGIN
	 SP_RECORDLOG (p_expreqid, 'Debug', NULL, p_logdesc, p_module, p_sitecode, o_success);
END;

PROCEDURE SP_RECORDLOGSITE (p_expreqid NUMBER,p_logdesc VARCHAR2, p_sitecode VARCHAR2, o_success OUT NUMBER )
AS
BEGIN
	 SP_RECORDLOG (p_expreqid, 'Debug', NULL, p_logdesc, 'Common', p_sitecode, o_success);
END;

PROCEDURE SP_RECORDLOGSITE (p_expreqid NUMBER, p_severity VARCHAR2, p_messagecode VARCHAR2, p_logdesc VARCHAR2, p_sitecode VARCHAR2, o_success OUT NUMBER )
AS
BEGIN
	 SP_RECORDLOG (p_expreqid, p_severity , p_messagecode , p_logdesc, 'Common', p_sitecode, o_success);
END;

PROCEDURE SP_UPDATE_LOGFILE (LOCATION VARCHAR2,filename VARCHAR2, DATA VARCHAR2)
AS
 output_file  UTL_FILE.FILE_TYPE;
BEGIN

	 BEGIN
	  output_file := UTL_FILE.FOPEN (LOCATION,filename, 'a',32000);

   	  UTL_FILE.PUT_LINE (output_file, DATA);
	  UTL_FILE.FCLOSE(output_file);

  	 EXCEPTION WHEN OTHERS THEN
	 		 RAISE_APPLICATION_ERROR (
            		-20000,
            		'Unhandled Oracle error in SP_UPDATE_LOGFILE' || SUBSTR (SQLERRM, 1, 100)
         			);

	 END;


END;

END PKG_IMPEXLOGGING;
/


CREATE SYNONYM ESCH.PKG_IMPEXLOGGING FOR PKG_IMPEXLOGGING;


CREATE SYNONYM EPAT.PKG_IMPEXLOGGING FOR PKG_IMPEXLOGGING;


GRANT EXECUTE, DEBUG ON PKG_IMPEXLOGGING TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_IMPEXLOGGING TO ESCH;

