CREATE OR REPLACE PACKAGE "PKG_FORMQUERY" IS

   /* Sammie Mhalagi  11/11/10*/
   function F_Is_FQueryCreator(P_FormQueryId Number, P_StudyId Number, P_fqCreatorId Number, P_fqCreatorType VARCHAR2) return number;

    pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'Pkg_FormQuery', pLEVEL  => Plog.LFATAL);
END;
/

