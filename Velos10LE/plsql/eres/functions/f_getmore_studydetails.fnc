CREATE OR REPLACE FUNCTION        "F_GETMORE_STUDYDETAILS" (p_study NUMBER)
RETURN VARCHAR2
AS
v_retval VARCHAR2(4000);
BEGIN

-- SELECT codelst_desc, studyid_id FROM (SELECT codelst_desc, studyid_id,codelst_custom_col FROM ER_STUDYID, ER_CODELST WHERE  fk_codelst_idtype = pk_codelst AND fk_study = p_study AND studyid_id IS NOT NULL) WHERE codelst_custom_col IS NULL OR (codelst_custom_col='chkbox' AND studyid_id='Y')

	 FOR i IN (SELECT codelst_desc, studyid_id FROM (SELECT codelst_desc, studyid_id,codelst_custom_col FROM ER_STUDYID, ER_CODELST WHERE  fk_codelst_idtype = pk_codelst AND fk_study = p_study AND studyid_id IS NOT NULL) )
	 LOOP
	 	 v_retval := v_retval || '[' || REPLACE(i.codelst_desc,'&nbsp;','') || ' : ' || i.studyid_id || '];';
	 END LOOP;
	 v_retval := substr(v_retval,1,instr(v_retval,';',-1)-1) ;
	 RETURN v_retval;
END ;
/


CREATE SYNONYM ESCH.F_GETMORE_STUDYDETAILS FOR F_GETMORE_STUDYDETAILS;


CREATE SYNONYM EPAT.F_GETMORE_STUDYDETAILS FOR F_GETMORE_STUDYDETAILS;


GRANT EXECUTE, DEBUG ON F_GETMORE_STUDYDETAILS TO EPAT;

GRANT EXECUTE, DEBUG ON F_GETMORE_STUDYDETAILS TO ESCH;

