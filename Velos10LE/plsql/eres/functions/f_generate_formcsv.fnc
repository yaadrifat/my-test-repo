CREATE OR REPLACE FUNCTION      F_Generate_Formcsv(p_form NUMBER, p_account NUMBER,p_dispordataval NUMBER,p_delimiter VARCHAR2)
RETURN CLOB
-- p_dispordataval 1 - Display value 2 - Data value
IS
v_retval CLOB;
v_colheader_orig VARCHAR2(32000);
v_colheader VARCHAR2(32000);
v_colsql VARCHAR2(32000);
v_formtype CHAR(2);
v_counter NUMBER;
v_find NUMBER;
v_cur Gk_Cv_Types.GenericCursorType;
v_data VARCHAR2(32000);
v_mp_uid VARCHAR2(100);
v_mp_uid_pos NUMBER;
BEGIN
FOR i IN (SELECT mp_mapcolname,mp_uid,mp_flddatatype,mp_sequence,mp_fldseqno,fld_repeatflag
			FROM ER_MAPFORM, ER_FLDLIB
			WHERE pk_field =  mp_pkfld AND fk_form = p_form
			ORDER BY mp_sequence,mp_fldseqno)
LOOP

	v_mp_uid := i.mp_uid;
	LOOP
		v_mp_uid_pos := INSTR(v_mp_uid,'cpy_');

		IF v_mp_uid_pos = 0 THEN
		   EXIT;
		END IF;

		v_mp_uid := SUBSTR(v_mp_uid,INSTR(v_mp_uid,'_',v_mp_uid_pos+4)+1);

	END LOOP;

	v_colheader_orig := v_colheader_orig ||  '"' || v_mp_uid || '"' || p_delimiter ;
	v_counter := 0;
	v_find := 0;
	v_find := INSTR(v_colheader,'"' || v_mp_uid || '"' || p_delimiter);
	IF v_find = 0 OR v_find IS NULL THEN
	   v_colheader := v_colheader ||  '"' || v_mp_uid || '"' || p_delimiter ;
	ELSE
		LOOP
			v_counter := v_counter + 1;
			v_find := INSTR(v_colheader_orig,'"' || v_mp_uid || '"' || p_delimiter,1,v_counter + 1);
			IF v_find = 0 OR v_find IS NULL THEN
			   EXIT;
			END IF;
		END LOOP;
	   v_colheader := v_colheader ||  '"' || v_mp_uid || '_' || TO_CHAR(v_counter - 1) || '"' || p_delimiter;
	END IF;

	CASE i.mp_flddatatype
	WHEN 'ET' THEN 	v_colsql := v_colsql || '''"'' || ' || i.mp_mapcolname || '||''"''' || '||''' || p_delimiter ||'''||';
	WHEN 'ED' THEN v_colsql := v_colsql || i.mp_mapcolname || '||''' || p_delimiter ||'''||';
	WHEN 'EN' THEN v_colsql := v_colsql || i.mp_mapcolname || '||''' || p_delimiter ||'''||';
	WHEN 'MD' THEN
		 	v_colsql := v_colsql || 'F_Gencsv_val(''' || i.mp_flddatatype || ''',' || i.mp_mapcolname || ',' || p_dispordataval || ',''' || p_delimiter || ''')||''' || p_delimiter ||'''||';
	WHEN 'MR' THEN
		 	v_colsql := v_colsql || 'F_Gencsv_val(''' || i.mp_flddatatype || ''',' || i.mp_mapcolname || ',' || p_dispordataval || ',''' || p_delimiter || ''')||''' || p_delimiter ||'''||';
	WHEN 'MC' THEN
		 	v_colsql := v_colsql || 'F_Gencsv_val(''' || i.mp_flddatatype || ''',' || i.mp_mapcolname || ',' || p_dispordataval || ',''' || p_delimiter || ''')||''' || p_delimiter ||'''||';
	END CASE;

END LOOP;


v_colsql := SUBSTR(v_colsql,1,INSTR(v_colsql,'||''' || p_delimiter ||'''||',-1,1)-1);

v_colheader := SUBSTR(v_colheader,1,INSTR(v_colheader,p_delimiter,-1,1)-1);

SELECT lf_displaytype INTO v_formtype FROM ER_LINKEDFORMS WHERE fk_formlib = p_form;

IF v_formtype = 'A' THEN
   v_colheader := '"Form Status"' || p_delimiter || v_colheader || p_delimiter || '"Response ID"' || p_delimiter || '"Create On"' || p_delimiter || '"Creator"' || p_delimiter || '"Last Modified Date"' || p_delimiter || '"Last Modified By"' ;
   v_colsql :=   '''"'' || (select codelst_desc from er_codelst where pk_codelst=form_completed) || ''"' || p_delimiter ||'''||' || v_colsql;
   v_colsql := 'select ' || v_colsql || '|| ''|'' || pk_formslinear || ''|'' || to_char(a.created_on,PKG_DATEUTIL.f_get_datetimeformat) || ''|'' || (select usr_firstname || '' ''  || usr_lastname from er_user where pk_user = a.creator) || ''|'' || to_char(a.last_modified_date,PKG_DATEUTIL.f_get_datetimeformat) || ''|'' || (select usr_firstname || '' ''  || usr_lastname from er_user where pk_user = a.last_modified_by)' || ' from er_formslinear a, er_account where pk_account = id and fk_form = ' || p_form || ' and pk_account = ' || p_account;
END IF;

IF v_formtype = 'S' OR v_formtype = 'SA' THEN
   v_colheader := '"Study Number"' || p_delimiter || '"Form Status"' || p_delimiter || v_colheader || p_delimiter || '"Response ID"' || p_delimiter || '"Create On"' || p_delimiter || '"Creator"' || p_delimiter || '"Last Modified Date"' || p_delimiter || '"Last Modified By"' ;
   v_colsql :=   '''"'' || study_number || ''"' || p_delimiter ||  '"'' || (select codelst_desc from er_codelst where pk_codelst=form_completed) || ''"' || p_delimiter ||'''||' || v_colsql;
   v_colsql := 'select ' || v_colsql || '|| ''|'' || pk_formslinear || ''|'' || to_char(a.created_on,PKG_DATEUTIL.f_get_datetimeformat) || ''|'' || (select usr_firstname || '' ''  || usr_lastname from er_user where pk_user = a.creator) || ''|'' || to_char(a.last_modified_date,PKG_DATEUTIL.f_get_datetimeformat) || ''|'' || (select usr_firstname || '' ''  || usr_lastname from er_user where pk_user = a.last_modified_by)' || ' from er_formslinear a,  er_study where pk_study = id and fk_form = ' || p_form || ' and fk_account = ' || p_account;
END IF;

IF v_formtype = 'PA' THEN
   v_colheader := '"Patient Id"' || p_delimiter || '"Form Status"' || p_delimiter || v_colheader || p_delimiter || '"Response ID"' || p_delimiter || '"Create On"' || p_delimiter || '"Creator"' || p_delimiter || '"Last Modified Date"' || p_delimiter || '"Last Modified By"' ;
   v_colsql :=   '''"'' || per_code || ''"' || p_delimiter   || '"'' || (select codelst_desc from er_codelst where pk_codelst=form_completed) || ''"' || p_delimiter || '''||' || v_colsql;
   v_colsql := 'select ' || v_colsql || '|| ''|'' || pk_formslinear || ''|'' || to_char(a.created_on,PKG_DATEUTIL.f_get_datetimeformat) || ''|'' || (select usr_firstname || '' ''  || usr_lastname from er_user where pk_user = a.creator) || ''|'' || to_char(a.last_modified_date,PKG_DATEUTIL.f_get_datetimeformat) || ''|'' || (select usr_firstname || '' ''  || usr_lastname from er_user where pk_user = a.last_modified_by)' || ' from er_formslinear a,  er_per where pk_per = id and fk_form = ' || p_form || ' and fk_account = ' || p_account;
END IF;

IF v_formtype IN ('PS','SP','PR') THEN
   v_colheader := '"Study Number"' || p_delimiter || '"Patient Id"' || p_delimiter || '"Patient Study Id"' || p_delimiter || '"Form Status"' || p_delimiter || v_colheader || p_delimiter || '"Response ID"' || p_delimiter || '"Create On"' || p_delimiter || '"Creator"' || p_delimiter || '"Last Modified Date"' || p_delimiter || '"Last Modified By"' ;
   v_colsql :=   '''"'' || (select study_number from er_study where pk_study = fk_study) || ''"' || p_delimiter   ||'''||' || '''"'' || per_code || ''"' || p_delimiter   ||'''||' || '''"'' || patprot_patstdid || ''"'  || p_delimiter   ||'''||' || '''"'' || (select codelst_desc from er_codelst where pk_codelst=form_completed) || ''"' || p_delimiter   ||'''||' || v_colsql;
   v_colsql := 'select ' || v_colsql || '|| ''|'' || pk_formslinear || ''|'' || to_char(a.created_on,PKG_DATEUTIL.f_get_datetimeformat) || ''|'' || (select usr_firstname || '' ''  || usr_lastname from er_user where pk_user = a.creator) || ''|'' || to_char(a.last_modified_date,PKG_DATEUTIL.f_get_datetimeformat) || ''|'' || (select usr_firstname || '' ''  || usr_lastname from er_user where pk_user = a.last_modified_by)' || ' from er_formslinear a, er_patprot, er_per where pk_per = fk_per and pk_patprot = fk_patprot and fk_form = ' || TO_CHAR(p_form) || ' and fk_account = ' || TO_CHAR(p_account);
END IF;





v_retval := NULL;



OPEN v_cur FOR v_colsql;
LOOP
FETCH v_cur INTO v_data;
	  EXIT WHEN v_cur%NOTFOUND;
	  v_retval := v_retval || '<CSVDATA>' || REPLACE(REPLACE(REPLACE(v_data,'&amp;','&'),'&','&amp;'),'<',' less than ') || '</CSVDATA>';

END LOOP;

IF v_retval IS NOT NULL THEN
v_retval := '<ROWSET><CSVDATA>' || v_colheader || '</CSVDATA>' || v_retval;
ELSE
v_retval := '<ROWSET>';
END IF;
v_retval := v_retval || '</ROWSET>';






 RETURN v_retval ;
END ;
/


