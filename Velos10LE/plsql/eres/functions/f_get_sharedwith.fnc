CREATE OR REPLACE FUNCTION        "F_GET_SHAREDWITH" (V_VALUE CHAR)
RETURN VARCHAR2 AS V_RETVAL VARCHAR2(50);
BEGIN
  CASE V_VALUE
    WHEN 'A' THEN RETURN 'All Acount Users';
    WHEN 'O' THEN RETURN 'All Users in an Organization';
    WHEN 'G' THEN RETURN 'All Users in a Group';
    WHEN 'S' THEN RETURN 'All Users in Study Team';
    WHEN 'P' THEN RETURN 'Private';
    ELSE RETURN NULL;
  END CASE;
END;
/


