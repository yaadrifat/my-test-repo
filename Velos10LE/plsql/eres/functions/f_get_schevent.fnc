CREATE OR REPLACE FUNCTION        "F_GET_SCHEVENT" (p_pk_sch_events1 NUMBER ) RETURN VARCHAR IS

/*Function to get the event name linked with a schedule record
*Author: Sonia Abrol, on 08/20/06
*/
v_event_name VARCHAR2(100);

BEGIN

	 IF p_pk_sch_events1 > 0 THEN

		BEGIN

		 	SELECT NAME
			INTO v_event_name
			FROM event_assoc WHERE event_id IN (SELECT fk_assoc FROM sch_events1 WHERE LPAD(event_id,10,0) = p_pk_sch_events1);

		EXCEPTION WHEN NO_DATA_FOUND THEN

			v_event_name := '';

		END;

	ELSE
		v_event_name := '';
	END IF;

	RETURN v_event_name;
END   ;
/


