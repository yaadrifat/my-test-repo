CREATE OR REPLACE FUNCTION        "F_GETSPL_ACCESS" (p_spl_access VARCHAR )
RETURN VARCHAR2
AS
v_spl_access VARCHAR2(1000);
v_temp VARCHAR2(50);
v_param VARCHAR2(100) := p_spl_access;
v_find INTEGER;
v_retval VARCHAR2(1000);
BEGIN
	 IF p_spl_access IS NULL THEN
	 	RETURN '';
	 END IF;
	LOOP
		v_find := INSTR(v_param,',');
	    IF v_find = 0 THEN
	       SELECT codelst_desc INTO v_spl_access FROM ER_CODELST WHERE pk_codelst = TO_NUMBER(v_param);
		   v_retval := v_retval || v_spl_access || ', ';
		   EXIT;
		ELSE
			v_temp := SUBSTR(v_param,1,v_find-1) ;
			SELECT codelst_desc INTO v_spl_access FROM ER_CODELST WHERE pk_codelst = TO_NUMBER(v_temp);
			v_retval := v_retval || v_spl_access|| ', ';
			v_param := SUBSTR(v_param,v_find+1);
		END IF;
	END LOOP;
	v_retval := SUBSTR(v_retval,1,LENGTH(v_retval)-2);
	RETURN v_retval;
END ;
/


CREATE SYNONYM ESCH.F_GETSPL_ACCESS FOR F_GETSPL_ACCESS;


CREATE SYNONYM EPAT.F_GETSPL_ACCESS FOR F_GETSPL_ACCESS;


GRANT EXECUTE, DEBUG ON F_GETSPL_ACCESS TO EPAT;

GRANT EXECUTE, DEBUG ON F_GETSPL_ACCESS TO ESCH;

