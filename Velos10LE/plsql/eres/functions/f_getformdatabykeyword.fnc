CREATE OR REPLACE FUNCTION        "F_GETFORMDATABYKEYWORD" (p_date DATE, p_per NUMBER, p_study NUMBER, p_keyword VARCHAR2)
RETURN VARCHAR
IS
v_selectcol VARCHAR2(1000);
v_counter NUMBER := 0;
v_retval VARCHAR2(4000) NULL;
v_form  NUMBER;
v_formname VARCHAR2(50);
v_sqlout VARCHAR2(4000);
BEGIN
  FOR i IN (SELECT fk_form,mp_mapcolname, mp_dispname,mp_flddatatype FROM ER_MAPFORM WHERE fk_form IN (SELECT pk_formlib FROM ER_FORMLIB WHERE form_keyword =p_keyword) AND mp_browser = 1 ORDER BY fk_form)
  LOOP
  	  IF v_form <> i.fk_form AND v_counter <> 0 THEN
	  	  SELECT form_name INTO v_formname FROM ER_FORMLIB WHERE pK_FORMLIB = v_form;
	  	   v_selectcol := 'select ' || v_selectcol || ' from er_formslinear where form_type = ''P'' and fk_filledform = :1';
		  FOR j IN (SELECT PK_PATFORMS  FROM ER_PATFORMS a, ER_PATPROT WHERE pk_patprot = fk_patprot AND fk_study = p_study AND a.fk_per = p_per AND fk_formlib =v_form AND NVL(a.record_type,'N') <> 'D' AND patforms_filldate = p_date)
		  LOOP
  		   	  EXECUTE IMMEDIATE v_selectcol INTO v_sqlout USING j.pk_patforms;
			   v_retval := v_retval || v_formname || 'X@@@XX@@@X' || v_sqlout;
		  END LOOP;
		 v_form := i.fk_form;
		 v_selectcol := '';
		 v_counter := 0;
	  END IF;
  	  v_counter := v_counter + 1;
	  IF v_counter = 1 THEN
	  	 	v_form := i.fk_form;
--  	  	 v_selectcol := v_selectcol || '''' ||i.mp_dispname || ' : '' || ' || 'substr(' || i.mp_mapcolname || ',1,instr(' ||  i.mp_mapcolname || ',[VELSEP1]))' || '|| ''X@@@X''' ;
			 IF i.mp_flddatatype = 'MC' OR  i.mp_flddatatype = 'MR' OR  i.mp_flddatatype = 'MD' THEN
  	  		 	 v_selectcol := v_selectcol || '''' ||i.mp_dispname || ' : '' || substr(' || i.mp_mapcolname || ',1,instr('|| i.mp_mapcolname || ',''[VELSEP1'')-1) || ''X@@@X''' ;
			ELSE
				 v_selectcol := v_selectcol || '''' ||i.mp_dispname || ' : '' || ' || i.mp_mapcolname || ' || ''X@@@X''' ;
			END IF;
	  ELSE
			 IF i.mp_flddatatype = 'MC' OR  i.mp_flddatatype = 'MR' OR  i.mp_flddatatype = 'MD' THEN
  	  		 	 v_selectcol := v_selectcol || ' || ''' || i.mp_dispname || ' : '' || substr(' || i.mp_mapcolname || ',1,instr('|| i.mp_mapcolname || ',''[VELSEP1'')-1) || ''X@@@X''' ;
			ELSE
				 v_selectcol := v_selectcol || ' || ''' ||i.mp_dispname || ' : '' || ' || i.mp_mapcolname || ' || ''X@@@X''' ;
			END IF;
	  END IF;
  END LOOP;
--  	  IF v_retval IS NULL THEN
	  	  SELECT form_name INTO v_formname FROM ER_FORMLIB WHERE pK_FORMLIB = v_form;
	  	   v_selectcol := 'select ' || v_selectcol || ' from er_formslinear where form_type = ''P'' and fk_filledform = :1';
		  FOR j IN (SELECT PK_PATFORMS  FROM ER_PATFORMS a, ER_PATPROT WHERE pk_patprot = fk_patprot AND fk_study = p_study AND a.fk_per = p_per AND fk_formlib =v_form AND NVL(a.record_type,'N') <> 'D' AND patforms_filldate = p_date)
		  LOOP
  		   	  EXECUTE IMMEDIATE v_selectcol INTO v_sqlout USING j.pk_patforms;
			   v_retval := v_retval || v_formname || 'X@@@XX@@@X' || v_sqlout;
		  END LOOP;
		   RETURN v_retval;
--	  END IF;
END;
/


CREATE SYNONYM ESCH.F_GETFORMDATABYKEYWORD FOR F_GETFORMDATABYKEYWORD;


CREATE SYNONYM EPAT.F_GETFORMDATABYKEYWORD FOR F_GETFORMDATABYKEYWORD;


GRANT EXECUTE, DEBUG ON F_GETFORMDATABYKEYWORD TO EPAT;

GRANT EXECUTE, DEBUG ON F_GETFORMDATABYKEYWORD TO ESCH;

