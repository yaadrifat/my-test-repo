CREATE OR REPLACE FUNCTION        "F_NCI_SUM4" (p_account NUMBER,p_startdate VARCHAR2, p_enddate VARCHAR2)
RETURN  CLOB
AS
  v_xml CLOB;
  v_counter NUMBER := 0 ;
  v_startdate DATE;
  v_enddate DATE;
  v_study_title VARCHAR2(200);
BEGIN

	v_xml := v_xml || '<ROWSET>';
	v_startdate := TO_DATE(p_startdate);
	v_enddate := TO_DATE(p_enddate);

 FOR i IN (SELECT * FROM (
SELECT DECODE(trial_type_agent,'Y',1,DECODE(trial_type_nonagent,'Y',2,DECODE(trial_type_epi,'Y',3,DECODE(trial_type_comp,'Y',4)))) AS trial_type_sort,
DECODE(trial_type_agent,'Y','Agent OR Device',DECODE(trial_type_nonagent,'Y','Trials Involving other Interventions',DECODE(trial_type_epi,'Y','Epidemiologic or other Observational Studies',DECODE(trial_type_comp,'Y','Companion, Ancillary or Correlative Studies','NA')))) AS trial_type,
RESEARCH_TYPE,STUDY_TYPE,STUDY_SCOPE,STUDY_SPONSOR,DISEASE_SITE,STUDY_NUMBER,PRIMARY_INVESTIGATOR,STUDY_MAJ_AUTH,TAREA,DATE_OPEN,DATE_CLOSED,STUDY_PHASE,STUDY_TITLE,TARGET,CTR_TO_DATE,CTR_TO_DATE_OTR,CTR_12_MOS, CTR_12_MOS_OTR, CLOSED_DATE
 FROM (
SELECT
DECODE(
	   (SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_type),'corr',
	   		   (SELECT studyid_id FROM ER_STUDYID WHERE fk_study =
			   (SELECT pk_study FROM ER_STUDY WHERE fk_account = p_account AND pk_study = a.study_assoc) AND fk_codelst_idtype =
			   (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'sum4_agent')),
			   (SELECT studyid_id FROM ER_STUDYID WHERE fk_study = a.pk_study AND fk_codelst_idtype =
			   (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'sum4_agent'))) AS trial_type_agent,
DECODE(
	   (SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_type),'corr',
	   		   (SELECT studyid_id FROM ER_STUDYID WHERE fk_study =
			   (SELECT pk_study FROM ER_STUDY WHERE fk_account = p_account AND pk_study = a.study_assoc) AND fk_codelst_idtype =
			   (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'sum4_beh')),
			   (SELECT studyid_id FROM ER_STUDYID WHERE fk_study = a.pk_study AND fk_codelst_idtype =
			   (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'sum4_beh'))) AS trial_type_nonagent,
DECODE(
	   (SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_type),'corr',
	   		   (SELECT studyid_id FROM ER_STUDYID WHERE fk_study =
			   (SELECT pk_study FROM ER_STUDY WHERE fk_account = p_account AND pk_study = a.study_assoc) AND fk_codelst_idtype =
			   (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'sum4_na')),
			   (SELECT studyid_id FROM ER_STUDYID WHERE fk_study = a.pk_study AND fk_codelst_idtype =
			   (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'sum4_na'))) AS trial_type_epi,
DECODE(
	   (SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_type),'corr',
	   		   (SELECT studyid_id FROM ER_STUDYID WHERE fk_study =
			   (SELECT pk_study FROM ER_STUDY WHERE fk_account = p_account AND pk_study = a.study_assoc) AND fk_codelst_idtype =
			   (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'sum4_comp')),
			   (SELECT studyid_id FROM ER_STUDYID WHERE fk_study = a.pk_study AND fk_codelst_idtype =
			   (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'sum4_comp'))) AS trial_type_comp,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_restype) AS research_type,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_type) AS study_type,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_scope) AS study_scope,
study_sponsor,F_Getdis_Site(a.study_disease_site) AS disease_site,
study_number,
(SELECT usr_lastname || ', ' || usr_firstname FROM ER_USER WHERE pk_user = a.study_prinv) || ' ' || study_otherprinv AS PRIMARY_INVESTIGATOR,
study_maj_auth,
(SELECT studyid_id FROM ER_STUDYID WHERE fk_study = a.pk_study AND fk_codelst_idtype = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'sum4_prg')) AS tarea,
TO_CHAR(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) AS date_open,
(CASE WHEN study_end_date >= v_startdate AND study_end_date <= v_enddate
THEN TO_CHAR(study_end_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ELSE NULL
END) AS date_closed,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_phase) AS study_phase,
(SELECT studyid_id FROM ER_STUDYID WHERE fk_study = a.pk_study AND fk_codelst_idtype = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'std_title')) AS study_title,
DECODE((SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_scope),
'std_cen_multi',
				DECODE((SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = (SELECT fk_codelst_studysitetype FROM ER_STUDYSITES WHERE fk_study = a.pk_study AND fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = p_account AND site_stat = 'Y'))),'primary',
				'(' || study_nsamplsize || ') ' || (SELECT studysite_lsamplesize FROM ER_STUDYSITES r WHERE r.fk_study = a.pk_study AND fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = p_account AND site_stat = 'Y') ),
				(SELECT studysite_lsamplesize FROM ER_STUDYSITES r WHERE r.fk_study = a.pk_study AND fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = p_account AND site_stat = 'Y') )),
'std_cen_single',study_nsamplsize,(SELECT studysite_lsamplesize FROM ER_STUDYSITES r WHERE r.fk_study = a.pk_study AND fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = p_account AND site_stat = 'Y') )) AS target,
(SELECT COUNT(*) FROM ER_PATPROT x, ER_PER y, ER_SITE z WHERE y.pk_per = x.fk_per AND z.pk_site = y.fk_site AND x.fk_study = a.pk_study AND z.fk_account = p_account AND site_stat = 'Y' AND PATPROT_ENROLDT IS NOT NULL ) AS ctr_to_date,
(SELECT COUNT(*) FROM ER_PATPROT x, ER_PER y, ER_SITE z WHERE y.pk_per = x.fk_per AND z.pk_site = y.fk_site AND x.fk_study = a.pk_study AND z.fk_account = p_account AND NVL(site_stat,'N') <> 'Y' AND PATPROT_ENROLDT IS NOT NULL ) AS ctr_to_date_otr,
(SELECT COUNT(*) FROM ER_PATPROT x, ER_PER y, ER_SITE z  WHERE y.pk_per = x.fk_per AND z.pk_site = y.fk_site AND x.fk_study = a.pk_study AND z.fk_account = p_account AND site_stat = 'Y' AND patprot_enroldt BETWEEN v_startdate AND v_enddate ) AS ctr_12_mos,
(SELECT COUNT(*) FROM ER_PATPROT x, ER_PER y, ER_SITE z  WHERE y.pk_per = x.fk_per AND z.pk_site = y.fk_site AND x.fk_study = a.pk_study AND z.fk_account = p_account AND NVL(site_stat,'N') <> 'Y' AND patprot_enroldt BETWEEN v_startdate AND v_enddate ) AS ctr_12_mos_otr,
TO_CHAR((SELECT MAX(studystat_date) FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_codelst_studystat IN (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'active_cls' OR codelst_subtyp = 'prmnt_cls')),PKG_DATEUTIL.F_GET_DATEFORMAT) AS closed_date
FROM ER_STUDY a,ER_STUDYSTAT b WHERE a.fk_account = p_account AND b.FK_STUDY = pk_study AND
fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'studystat' AND codelst_subtyp = 'active') AND
(study_end_date IS NULL OR study_end_date >= v_startdate) AND study_actualdt <= v_enddate
)
)
WHERE trial_type <> 'NA'
ORDER BY trial_type_sort,research_type,tarea,primary_investigator)

LOOP

	v_counter := v_counter + 1 ;
	v_xml := v_xml || '<ROW num="' || TO_CHAR(v_counter) || '">';
	v_xml := v_xml || '<TRIAL_TYPE>' || i.TRIAL_TYPE || '</TRIAL_TYPE>';
	v_xml := v_xml || '<RESEARCH_TYPE>' || i.RESEARCH_TYPE || '</RESEARCH_TYPE>';
	v_xml := v_xml || '<STUDY_TYPE>' || i.STUDY_TYPE || '</STUDY_TYPE>';
	v_xml := v_xml || '<STUDY_SCOPE>' || i.STUDY_SCOPE || '</STUDY_SCOPE>';
	v_xml := v_xml || '<STUDY_SPONSOR>' || i.STUDY_SPONSOR || '</STUDY_SPONSOR>';
	v_xml := v_xml || '<DISEASE_SITE>' || i.DISEASE_SITE || '</DISEASE_SITE>';
	v_xml := v_xml || '<STUDY_NUMBER>' || i.STUDY_NUMBER || '</STUDY_NUMBER>';
	v_xml := v_xml || '<PRIMARY_INVESTIGATOR>' || i.PRIMARY_INVESTIGATOR || '</PRIMARY_INVESTIGATOR>';
	v_xml := v_xml || '<STUDY_MAJ_AUTH>' || i.STUDY_MAJ_AUTH || '</STUDY_MAJ_AUTH>';
	v_xml := v_xml || '<TAREA>' || i.TAREA || '</TAREA>';
	v_xml := v_xml || '<DATE_OPEN>' || i.DATE_OPEN || '</DATE_OPEN>';
	v_xml := v_xml || '<DATE_CLOSED>' || i.DATE_CLOSED || '</DATE_CLOSED>';
	v_xml := v_xml || '<STUDY_PHASE>' || i.STUDY_PHASE || '</STUDY_PHASE>';
	v_xml := v_xml || '<STUDY_TITLE>' || i.study_title || '</STUDY_TITLE>';
	v_xml := v_xml || '<TARGET>' || i.TARGET || '</TARGET>';
	v_xml := v_xml || '<CTR_TO_DATE>' || TO_CHAR(i.CTR_TO_DATE) || '</CTR_TO_DATE>';
	v_xml := v_xml || '<CTR_TO_DATE_OTR>' || TO_CHAR(i.CTR_TO_DATE_OTR) || '</CTR_TO_DATE_OTR>';
	v_xml := v_xml || '<CTR_12_MOS>' || TO_CHAR(i.CTR_12_MOS) || '</CTR_12_MOS>';
	v_xml := v_xml || '<CTR_12_MOS_OTR>' || TO_CHAR(i.CTR_12_MOS_OTR) || '</CTR_12_MOS_OTR>';
	v_xml := v_xml || '<CLOSED_DATE>' || TO_CHAR(i.CLOSED_DATE) || '</CLOSED_DATE>';
	v_xml := v_xml || '</ROW>';
END LOOP;
v_xml := v_xml || '</ROWSET>';
RETURN v_xml;
END;
/


CREATE SYNONYM ESCH.F_NCI_SUM4 FOR F_NCI_SUM4;


CREATE SYNONYM EPAT.F_NCI_SUM4 FOR F_NCI_SUM4;


GRANT EXECUTE, DEBUG ON F_NCI_SUM4 TO EPAT;

GRANT EXECUTE, DEBUG ON F_NCI_SUM4 TO ESCH;

