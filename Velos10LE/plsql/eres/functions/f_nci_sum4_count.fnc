CREATE OR REPLACE FUNCTION        "F_NCI_SUM4_COUNT" (p_study NUMBER,p_startdate VARCHAR2, p_enddate VARCHAR2, p_sitestat CHAR)
RETURN  NUMBER
AS
  v_retval NUMBER;
BEGIN

IF p_startdate IS NULL THEN
	SELECT COUNT(*)
	INTO v_retval
	FROM ER_PATPROT, ER_PER, ER_SITE
	WHERE pk_per=fk_per AND
	pk_site=fk_site AND
	fk_study=p_study AND
	NVL(site_stat,'N') = p_sitestat AND
	PATPROT_ENROLDT IS NOT NULL AND patprot_stat = 1;
ELSE
	SELECT COUNT(*)
	INTO v_retval
	FROM ER_PATPROT, ER_PER, ER_SITE
	WHERE pk_per=fk_per AND
	pk_site=fk_site AND
	fk_study=p_study AND
	NVL(site_stat,'N') = p_sitestat AND
	PATPROT_ENROLDT BETWEEN p_startdate AND p_enddate AND patprot_stat = 1;
END IF;

RETURN v_retval;
END;
/


CREATE SYNONYM ESCH.F_NCI_SUM4_COUNT FOR F_NCI_SUM4_COUNT;


CREATE SYNONYM EPAT.F_NCI_SUM4_COUNT FOR F_NCI_SUM4_COUNT;


GRANT EXECUTE, DEBUG ON F_NCI_SUM4_COUNT TO EPAT;

GRANT EXECUTE, DEBUG ON F_NCI_SUM4_COUNT TO ESCH;

