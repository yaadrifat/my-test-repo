CREATE OR REPLACE FUNCTION        "F_CHECK_OUTOFRANGE" (p_start_date Date, p_fuzzy_period varchar)
RETURN  varchar2
as
    l_beforedate date;
    l_afterdate date;
    l_len number;

BEGIN
/*
Author: Sonika Talwar
Date: Dec 24, 2002
Purpose: Used in Protocol compliance report sql (id 84) for displaying the out of range column
If the event completion date does not lie within the startdate  period then its out of range
parameters:
p_start_date Date - event start date
p_fuzzy_period - event fuzzy period
returns:
*/
    select length(p_fuzzy_period) into l_len
    from dual;

            if (substr(p_fuzzy_period,1,1) = '+') then
                l_afterdate := p_start_date + substr(p_fuzzy_period,2,l_len);
			    l_beforedate := p_start_date;
            elsif (substr(p_fuzzy_period,1,1) = '-') then
		      l_beforedate := p_start_date - substr(p_fuzzy_period,2,l_len);
			  l_afterdate := p_start_date;
		  else
                l_afterdate := p_start_date + substr(p_fuzzy_period,1,l_len);
  		      l_beforedate := p_start_date - substr(p_fuzzy_period,1,l_len);
		  end if ;

		  return to_char(l_afterdate,PKG_DATEUTIL.F_GET_DATEFORMAT) || ',' || to_char(l_beforedate,PKG_DATEUTIL.F_GET_DATEFORMAT) ;

END;
/


CREATE SYNONYM ESCH.F_CHECK_OUTOFRANGE FOR F_CHECK_OUTOFRANGE;


CREATE SYNONYM EPAT.F_CHECK_OUTOFRANGE FOR F_CHECK_OUTOFRANGE;


GRANT EXECUTE, DEBUG ON F_CHECK_OUTOFRANGE TO EPAT;

GRANT EXECUTE, DEBUG ON F_CHECK_OUTOFRANGE TO ESCH;

