CREATE OR REPLACE FUNCTION "F_GET_USERSITES" (p_userid NUMBER )
RETURN CLOB
AS
v_usrsite CLOB;
BEGIN
	FOR i IN (SELECT site_name FROM ER_USERSITE,ER_SITE WHERE fk_user = p_userid AND pk_site = fk_site AND usersite_right <> 0)
	LOOP
	    v_usrsite := v_usrsite || i.site_name || ', ';
	END LOOP;
    RETURN SUBSTR(v_usrsite,1,LENGTH(v_usrsite)-2);
END;
/


CREATE SYNONYM ESCH.F_GET_USERSITES FOR F_GET_USERSITES;


CREATE SYNONYM EPAT.F_GET_USERSITES FOR F_GET_USERSITES;


GRANT EXECUTE, DEBUG ON F_GET_USERSITES TO EPAT;

GRANT EXECUTE, DEBUG ON F_GET_USERSITES TO ESCH;

