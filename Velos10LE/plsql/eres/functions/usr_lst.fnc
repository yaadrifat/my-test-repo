CREATE OR REPLACE FUNCTION        "USR_LST" (p_usrid varchar2)
return varchar
is
   V_STR      varchar2 (2001) default p_usrid || ',';
   V_SQLSTR   varchar2 (4000);
   V_PARAMS   varchar2 (2001) default p_usrid || ',';
   V_POS      number         := 0;
   V_CNT      number         := 0;
   v_usrlst varchar2(2000) ;
   v_usr varchar2(70) ;
Begin
   LOOP
       V_CNT := V_CNT + 1;
       V_POS := instr (V_STR, ',');
       V_PARAMS := substr (V_STR, 1, V_POS - 1);
        exit when V_PARAMS Is null;
        select usr_firstname ||' ' || usr_lastname
          into v_usr
         from er_user
        where pk_user = v_params ;
        if v_usrlst is null then
          v_usrlst :=  v_usr ;
        else
          v_usrlst :=  v_usrlst ||';' || v_usr  ;
        end if ;
      V_STR := substr (V_STR, V_POS + 1);
   dbms_output.put_line( 'v_usrlst = '  || v_usrlst ) ;
   END LOOP ;
   dbms_output.put_line( to_char(length(v_usrlst))) ;
  return v_usrlst ;
end ;
/


CREATE SYNONYM ESCH.USR_LST FOR USR_LST;


CREATE SYNONYM EPAT.USR_LST FOR USR_LST;


GRANT EXECUTE, DEBUG ON USR_LST TO EPAT;

GRANT EXECUTE, DEBUG ON USR_LST TO ESCH;

