/********************************************************************/
* Folder: purged
* Created on: 08/31/2010 
* Purpose: This folder will hold all the purged PL-SQL code.
*
* Instructions:
* 1. Only purged code needs to be stored in this folder.
* 2. Purged code should be in original format. File Name should not be changed. (e.g trigger- *.trg, procedure- *.prc)
* 3. Purged code should go in the respective sub-folder. (e.g trigger code in triggers folder, procedure in procedures folder)
*    If the expected sub-folder is not present, please create the folder, add it to CVS. 
*    Commit your purged code files in the newly created sub-folder.
* 4. The PL-SQL files added to the purged folder(i.e. respective sub-folder) need to be removed from the original PL-SQL folder.
* 5. Do not add any PL-SQL code to this folder that you don't wish to purge.
* 6. Purged code can be used in future for reference.
/********************************************************************/