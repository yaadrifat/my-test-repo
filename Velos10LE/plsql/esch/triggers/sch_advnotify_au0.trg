CREATE OR REPLACE TRIGGER SCH_ADVNOTIFY_AU0
  AFTER UPDATE OF
  pk_advnotify,
  fk_adverseve,
  fk_codelst_not_type,
  advnotify_date,
  advnotify_notes,
  creator,
  created_on,
  ip_add,
  advnotify_value,
  rid
  ON sch_advnotify
  FOR EACH ROW
DECLARE
  raid NUMBER(10);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'SCH_ADVNOTIFY', :OLD.rid, 'U', :NEW.LAST_MODIFIED_BY);

  IF NVL(:OLD.pk_advnotify,0) !=
     NVL(:NEW.pk_advnotify,0) THEN
     audit_trail.column_update
       (raid, 'PK_ADVNOTIFY',
       :OLD.pk_advnotify, :NEW.pk_advnotify);
  END IF;
  IF NVL(:OLD.fk_adverseve,0) !=
     NVL(:NEW.fk_adverseve,0) THEN
     audit_trail.column_update
       (raid, 'FK_ADVERSEVE',
       :OLD.fk_adverseve, :NEW.fk_adverseve);
  END IF;
  IF NVL(:OLD.fk_codelst_not_type,0) !=
     NVL(:NEW.fk_codelst_not_type,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_NOT_TYPE',
       :OLD.fk_codelst_not_type, :NEW.fk_codelst_not_type);
  END IF;
  IF NVL(:OLD.advnotify_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.advnotify_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'ADVNOTIFY_DATE',
       to_char(:old.advnotify_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.advnotify_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.advnotify_notes,' ') !=
     NVL(:NEW.advnotify_notes,' ') THEN
     audit_trail.column_update
       (raid, 'ADVNOTIFY_NOTES',
       :OLD.advnotify_notes, :NEW.advnotify_notes);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
  IF NVL(:OLD.advnotify_value,0) !=
     NVL(:NEW.advnotify_value,0) THEN
     audit_trail.column_update
       (raid, 'ADVNOTIFY_VALUE',
       :OLD.advnotify_value, :NEW.advnotify_value);
  END IF;

END;
/


