CREATE OR REPLACE TRIGGER SCH_CODELST_AU0
AFTER UPDATE
ON SCH_CODELST REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_CODELST', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_codelst,0) !=
      NVL(:NEW.pk_codelst,0) THEN
      audit_trail.column_update
        (raid, 'PK_CODELST',
        :OLD.pk_codelst, :NEW.pk_codelst);
   END IF;
   IF NVL(:OLD.codelst_type,' ') !=
      NVL(:NEW.codelst_type,' ') THEN
      audit_trail.column_update
        (raid, 'CODELST_TYPE',
        :OLD.codelst_type, :NEW.codelst_type);
   END IF;
   IF NVL(:OLD.codelst_subtyp,' ') !=
      NVL(:NEW.codelst_subtyp,' ') THEN
      audit_trail.column_update
        (raid, 'CODELST_SUBTYP',
        :OLD.codelst_subtyp, :NEW.codelst_subtyp);
   END IF;
   IF NVL(:OLD.codelst_desc,' ') !=
      NVL(:NEW.codelst_desc,' ') THEN
      audit_trail.column_update
        (raid, 'CODELST_DESC',
        :OLD.codelst_desc, :NEW.codelst_desc);
   END IF;
   IF NVL(:OLD.codelst_hide,' ') !=
      NVL(:NEW.codelst_hide,' ') THEN
      audit_trail.column_update
        (raid, 'CODELST_HIDE',
        :OLD.codelst_hide, :NEW.codelst_hide);
   END IF;
   IF NVL(:OLD.codelst_seq,0) !=
      NVL(:NEW.codelst_seq,0) THEN
      audit_trail.column_update
        (raid, 'CODELST_SEQ',
        :OLD.codelst_seq, :NEW.codelst_seq);
   END IF;
   IF NVL(:OLD.codelst_maint,' ') !=
      NVL(:NEW.codelst_maint,' ') THEN
      audit_trail.column_update
        (raid, 'CODELST_MAINT',
        :OLD.codelst_maint, :NEW.codelst_maint);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
        to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
        to_char(:old.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.fk_account,0) !=
      NVL(:NEW.fk_account,0) THEN
      audit_trail.column_update
        (raid, 'FK_ACCOUNT',
        :OLD.fk_account, :NEW.fk_account);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;

   IF NVL(:OLD.CODELST_CUSTOM_COL1,' ') !=
      NVL(:NEW.CODELST_CUSTOM_COL1,' ') THEN
      audit_trail.column_update
        (raid, 'CODELST_CUSTOM_COL1',
        :OLD.CODELST_CUSTOM_COL1, :NEW.CODELST_CUSTOM_COL1);
   END IF;

   IF NVL(:OLD.CODELST_STUDY_ROLE,' ') !=
      NVL(:NEW.CODELST_STUDY_ROLE,' ') THEN
      audit_trail.column_update
        (raid, 'CODELST_STUDY_ROLE',
        :OLD.CODELST_STUDY_ROLE, :NEW.CODELST_STUDY_ROLE);
   END IF;




END;
/


