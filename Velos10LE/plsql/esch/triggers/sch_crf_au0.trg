CREATE OR REPLACE TRIGGER SCH_CRF_AU0
AFTER UPDATE
ON SCH_CRF REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_CRF', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_crf,0) !=
      NVL(:NEW.pk_crf,0) THEN
      audit_trail.column_update
        (raid, 'PK_CRF',
        :OLD.pk_crf, :NEW.pk_crf);
   END IF;
   IF NVL(:OLD.fk_events1,' ') !=
      NVL(:NEW.fk_events1,' ') THEN
      audit_trail.column_update
        (raid, 'FK_EVENTS1',
        :OLD.fk_events1, :NEW.fk_events1);
   END IF;
   IF NVL(:OLD.crf_number,' ') !=
      NVL(:NEW.crf_number,' ') THEN
      audit_trail.column_update
        (raid, 'CRF_NUMBER',
        :OLD.crf_number, :NEW.crf_number);
   END IF;
   IF NVL(:OLD.crf_name,' ') !=
      NVL(:NEW.crf_name,' ') THEN
      audit_trail.column_update
        (raid, 'CRF_NAME',
        :OLD.crf_name, :NEW.crf_name);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
        to_char(:old.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;
   IF NVL(:OLD.fk_patprot,0) !=
      NVL(:NEW.fk_patprot,0) THEN
      audit_trail.column_update
        (raid, 'FK_PATPROT',
        :OLD.fk_patprot, :NEW.fk_patprot);
   END IF;
   IF NVL(:OLD.crf_flag,' ') !=
      NVL(:NEW.crf_flag,' ') THEN
      audit_trail.column_update
        (raid, 'CRF_FLAG',
        :OLD.crf_flag, :NEW.crf_flag);
   END IF;
   IF NVL(:OLD.crf_delflag,' ') !=
      NVL(:NEW.crf_delflag,' ') THEN
      audit_trail.column_update
        (raid, 'CRF_DELFLAG',
        :OLD.crf_delflag, :NEW.crf_delflag);
   END IF;

END;
/


