CREATE OR REPLACE TRIGGER "SCH_DISPATCHMSG_AU0" 
AFTER UPDATE
ON SCH_DISPATCHMSG REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

 audit_trail.record_transaction
    (raid, 'SCH_DISPATCHMSG', :OLD.rid, 'U');
/*
  if nvl(:old.pk_msg,0) !=
     NVL(:new.pk_msg,0) then
     audit_trail.column_update
       (raid, 'PK_MSG',
       :old.pk_msg, :new.pk_msg);
  end if;

  if nvl(trim(:old.msg_sendon),TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(trim(:new.msg_sendon),TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'MSG_SENDON',
       :old.msg_sendon, :new.msg_sendon);
  end if;
*/

  IF NVL(:OLD.msg_status,0) !=
     NVL(:NEW.msg_status,0) THEN
     audit_trail.column_update
       (raid, 'MSG_STATUS',
       :OLD.msg_status, :NEW.msg_status);
  END IF;

/*
  if nvl(:old.msg_type,' ') !=
     NVL(:new.msg_type,' ') then
     audit_trail.column_update
       (raid, 'MSG_TYPE',
       :old.msg_type, :new.msg_type);
  end if;
  if nvl(:old.fk_event,0) !=
     NVL(:new.fk_event,0) then
     audit_trail.column_update
       (raid, 'FK_EVENT',
       :old.fk_event, :new.fk_event);
  end if;
  if nvl(:old.msg_text,' ') !=
     NVL(:new.msg_text,' ') then
     audit_trail.column_update
       (raid, 'MSG_TEXT',
       :old.msg_text, :new.msg_text);
  end if;
  if nvl(:old.fk_pat,0) !=
     NVL(:new.fk_pat,0) then
     audit_trail.column_update
       (raid, 'FK_PAT',
       :old.fk_pat, :new.fk_pat);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :old.last_modified_by, :new.last_modified_by);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       :old.last_modified_date, :new.last_modified_date);
  end if;
  if nvl(:old.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'CREATED_ON',
       :old.created_on, :new.created_on);

  end if;
  */
END;
/


