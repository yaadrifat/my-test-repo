CREATE OR REPLACE TRIGGER "SCH_EVENTS1_EVENTSTAT_AI"
    AFTER INSERT
    ON SCH_EVENTS1
    FOR EACH ROW
WHEN (
old.start_date_time is null
      )
Begin

 insert into sch_eventstat
   ( PK_EVENTSTAT, EVENTSTAT_DT, EVENTSTAT_NOTES, EVENTSTAT,
     FK_EVENT, CREATOR, LAST_MODIFIED_BY, IP_ADD  )
 values
   ( sch_eventstat_seq.nextval, :new.start_date_time, :new.notes, :new.isconfirmed,
     :new.event_id, :new.creator, :new.last_modified_by, :new.ip_add) ;
end ;
/


