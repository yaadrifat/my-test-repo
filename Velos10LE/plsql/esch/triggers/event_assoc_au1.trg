CREATE OR REPLACE TRIGGER "ESCH"."EVENT_ASSOC_AU1" AFTER UPDATE ON EVENT_ASSOC
FOR EACH ROW
declare
v_return number;
v_pk_lineitem number;

BEGIN
IF  (:OLD.FK_CODELST_CALSTAT <> :NEW.FK_CODELST_CALSTAT) THEN
 INSERT INTO SCH_PROTSTAT (
    PK_PROTSTAT    ,
 FK_EVENT       ,
 --PROTSTAT       ,
 PROTSTAT_DT    ,
 PROTSTAT_BY    ,
 PROTSTAT_NOTE  ,
 CREATOR,   ---Amarnadh
 IP_ADD, FK_CODELST_CALSTAT )
        VALUES
 (sch_protstat_seq.NEXTVAL,
  :NEW.event_id ,
  --:NEW.status ,
  :NEW.status_dt ,
  :NEW.status_changeby ,
  :NEW.notes ,
  :NEW.creator ,
  :NEW.ip_Add, :NEW.FK_CODELST_CALSTAT ) ;
END IF;
    --set default notifications for the protocol
 /*  if ( (:old.status <> :new.status) and :new.status = 'A') then
       pkg_alnot.set_alnot(NULL, :new.CHAIN_ID, :new.EVENT_ID, 'G' , :new.creator, sysdate, :new.ip_add);
  end if; */


 --for default budget:
if (:old.event_type='A' and nvl(:old.fk_visit,0) <> nvl(:NEW.fk_visit,0) and nvl(:old.displacement,-1) <> 0 ) then
    PKG_BGT.sp_addToProtocolBudget( :NEW.event_id,'S',:NEW.chain_id, :NEW.creator , :NEW.ip_Add , v_return , 0 ,:NEW.fk_visit,
    :NEW.name,:NEW.event_cptcode,:NEW.event_line_category,:NEW.description,:NEW.event_sequence);
end if;
begin
    -- get default budget lineitem info for this event
    if (:old.event_type='A' and ( (:NEW.name <> :old.name) or  ( nvl(:NEW.description,' ') <> nvl(:old.description,' ') ) or
       ( nvl(:NEW.event_cptcode,' ') <> nvl(:old.event_cptcode,' ') )  or
       ( nvl(:NEW.event_line_category,0) <> nvl(:old.event_line_category,0) )
       or( nvl(:NEW.event_sequence,0) <> nvl(:old.event_sequence,0) ))
    )then -- only for events
		--update the lineitem
        update sch_lineitem
        set last_modified_by = :NEW.last_modified_by ,last_modified_date = sysdate, IP_ADD = :NEW.ip_add,
        LINEITEM_NAME =  :NEW.name,LINEITEM_DESC= :NEW.description,LINEITEM_CPTCODE = :NEW.event_cptcode,
        lineitem_seq = :NEW.event_sequence
        where pk_lineitem in (select pk_lineitem
        from sch_lineitem l
        where l.fk_event = :old.event_id and l.fk_bgtsection in ( select pk_budgetsec from sch_bgtsection where fk_bgtcal =
        (select pk_bgtcal from sch_bgtcal where bgtcal_protid = :old.chain_id 
        and nvl(BGTCAL_DELFLAG,'Z') <>'Y' and fk_budget = (
        select  pk_budget from sch_budget where budget_calendar = :old.chain_id
        and nvl(BUDGET_DELFLAG,'Z') <>'Y')
        )
        ) ) ;
    end if;
	exception when no_data_found then
    	v_pk_lineitem := 0;
end;
END ;
/


