CREATE OR REPLACE TRIGGER "ESCH"."EVENT_ASSOC_AI1" AFTER INSERT ON EVENT_ASSOC
FOR EACH ROW
DECLARE v_checkactive NUMBER(1) := 0;
         v_study NUMBER;
v_return number;

pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'EVENT_ASSOC_AI1', pLEVEL  => Plog.LFATAL);

BEGIN
 IF UPPER(trim(:NEW.event_type)) = 'P' THEN

   INSERT INTO SCH_PROTSTAT (
     PK_PROTSTAT    ,
     FK_EVENT       ,
     --PROTSTAT       ,
     PROTSTAT_DT    ,
     PROTSTAT_BY    ,
     PROTSTAT_NOTE  ,
     CREATOR        , -- Amarnadh
     IP_ADD,
     FK_CODELST_CALSTAT  )
     VALUES
    (sch_protstat_seq.NEXTVAL,
    :NEW.event_id ,
    --:NEW.status ,
    :NEW.status_dt ,
    :NEW.status_changeby ,
    :NEW.notes ,
    :NEW.creator ,
    :NEW.ip_Add,
    :NEW.FK_CODELST_CALSTAT ) ;


   -- by sonia sahni
   -- insert default notifications if study is already enrolling
   v_study := :NEW.CHAIN_ID;

	/*CCF-FIN1 User will specify the budget template to be used for creating default budget
	Removed PKG_BGT.sp_createProtocolBudget call from here Moved code to Java.
	This is done explicitely as Library calendars will not have default budgets anymore.
	The only place the procedure will be called from is study setup calendars
	*/
END IF;

if (:NEW.event_type='A') then
	begin
	    PKG_BGT.sp_addToProtocolBudget( :NEW.event_id,'S',:NEW.chain_id, :NEW.creator , :NEW.ip_Add , v_return , 0 ,:NEW.fk_visit,
		:NEW.name,:NEW.event_cptcode,:NEW.event_line_category,:NEW.description,:NEW.event_sequence);
	    --, to be added after Manimaran's patch - new.event_line_category
	end;
 end if;

END ;
/


