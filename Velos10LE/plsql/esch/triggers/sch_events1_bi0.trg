CREATE  OR REPLACE TRIGGER SCH_EVENTS1_BI0 BEFORE INSERT ON SCH_EVENTS1        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'SCH_EVENTS1', erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'ACTUAL_SCHDATE', :NEW.ACTUAL_SCHDATE);
       Audit_Trail.column_insert (raid, 'ADVERSE_COUNT', :NEW.ADVERSE_COUNT);
       Audit_Trail.column_insert (raid, 'ALNOT_SENT', :NEW.ALNOT_SENT);
       Audit_Trail.column_insert (raid, 'ATTENDED', :NEW.ATTENDED);
       Audit_Trail.column_insert (raid, 'BOOKEDON', :NEW.BOOKEDON);
       Audit_Trail.column_insert (raid, 'BOOKED_BY', :NEW.BOOKED_BY);
       Audit_Trail.column_insert (raid, 'CHILD_SERVICE_ID', :NEW.CHILD_SERVICE_ID);
       Audit_Trail.column_insert (raid, 'CREATED_ON', :NEW.CREATED_ON);
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'DESCRIPTION', :NEW.DESCRIPTION);
       Audit_Trail.column_insert (raid, 'END_DATE_TIME', :NEW.END_DATE_TIME);
       Audit_Trail.column_insert (raid, 'EVENT_EXEBY', :NEW.EVENT_EXEBY);
       Audit_Trail.column_insert (raid, 'EVENT_EXEON', :NEW.EVENT_EXEON);
       Audit_Trail.column_insert (raid, 'EVENT_ID', :NEW.EVENT_ID);
       Audit_Trail.column_insert (raid, 'EVENT_NOTES', :NEW.EVENT_NOTES);
       Audit_Trail.column_insert (raid, 'EVENT_NUM', :NEW.EVENT_NUM);
       Audit_Trail.column_insert (raid, 'EVENT_SEQUENCE', :NEW.EVENT_SEQUENCE);
       Audit_Trail.column_insert (raid, 'FACILITY_ID', :NEW.FACILITY_ID);
       Audit_Trail.column_insert (raid, 'FK_ASSOC', :NEW.FK_ASSOC);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from  SCH_CODELST where PK_CODELST=:NEW.FK_CODELST_COVERTYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_COVERTYPE', codeList_desc);
       Audit_Trail.column_insert (raid, 'FK_PATPROT', :NEW.FK_PATPROT);
       Audit_Trail.column_insert (raid, 'FK_STUDY', :NEW.FK_STUDY);
       Audit_Trail.column_insert (raid, 'FK_VISIT', :NEW.FK_VISIT);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'ISCONFIRMED', :NEW.ISCONFIRMED);
       Audit_Trail.column_insert (raid, 'ISWAITLISTED', :NEW.ISWAITLISTED);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', :NEW.LAST_MODIFIED_DATE);
       Audit_Trail.column_insert (raid, 'LOCATION_ID', :NEW.LOCATION_ID);
       Audit_Trail.column_insert (raid, 'LOCATION_NAME', :NEW.LOCATION_NAME);
       Audit_Trail.column_insert (raid, 'NOTES', :NEW.NOTES);
       Audit_Trail.column_insert (raid, 'OBJECT_ID', :NEW.OBJECT_ID);
       Audit_Trail.column_insert (raid, 'OBJECT_NAME', :NEW.OBJECT_NAME);
       Audit_Trail.column_insert (raid, 'OBJ_LOCATION_ID', :NEW.OBJ_LOCATION_ID);
       Audit_Trail.column_insert (raid, 'OCCURENCE_ID', :NEW.OCCURENCE_ID);
       Audit_Trail.column_insert (raid, 'PATIENT_ID', :NEW.PATIENT_ID);
       Audit_Trail.column_insert (raid, 'REASON_FOR_COVERAGECHANGE', :NEW.REASON_FOR_COVERAGECHANGE);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'ROLES', :NEW.ROLES);
       Audit_Trail.column_insert (raid, 'SERVICE_ID', :NEW.SERVICE_ID);
       Audit_Trail.column_insert (raid, 'SERVICE_SITE_ID', :NEW.SERVICE_SITE_ID);
       Audit_Trail.column_insert (raid, 'SESSION_ID', :NEW.SESSION_ID);
       Audit_Trail.column_insert (raid, 'SESSION_NAME', :NEW.SESSION_NAME);
       Audit_Trail.column_insert (raid, 'START_DATE_TIME', :NEW.START_DATE_TIME);
       Audit_Trail.column_insert (raid, 'STATUS', :NEW.STATUS);
       Audit_Trail.column_insert (raid, 'SVC_TYPE_ID', :NEW.SVC_TYPE_ID);
       Audit_Trail.column_insert (raid, 'SVC_TYPE_NAME', :NEW.SVC_TYPE_NAME);
       Audit_Trail.column_insert (raid, 'TYPE_ID', :NEW.TYPE_ID);
       Audit_Trail.column_insert (raid, 'USER_NAME', :NEW.USER_NAME);
       Audit_Trail.column_insert (raid, 'VISIT', :NEW.VISIT);
       Audit_Trail.column_insert (raid, 'VISIT_TYPE_ID', :NEW.VISIT_TYPE_ID);
       Audit_Trail.column_insert (raid, 'VISIT_TYPE_NAME', :NEW.VISIT_TYPE_NAME);
COMMIT;
END;
/