CREATE OR REPLACE TRIGGER "SCH_BGTAPNDX_BU0" BEFORE UPDATE ON SCH_BGTAPNDX
FOR EACH ROW
WHEN (
new.last_modified_by is not null
      )
BEGIN
  :NEW.last_modified_date := SYSDATE ;
END;
/


