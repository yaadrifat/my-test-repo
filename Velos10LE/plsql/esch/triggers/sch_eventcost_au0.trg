CREATE OR REPLACE TRIGGER SCH_EVENTCOST_AU0
AFTER UPDATE
ON SCH_EVENTCOST REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_EVENTCOST', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_eventcost,0) !=
      NVL(:NEW.pk_eventcost,0) THEN
      audit_trail.column_update
        (raid, 'PK_EVENTCOST',
        :OLD.pk_eventcost, :NEW.pk_eventcost);
   END IF;
   IF NVL(:OLD.eventcost_value,0) !=
      NVL(:NEW.eventcost_value,0) THEN
      audit_trail.column_update
        (raid, 'EVENTCOST_VALUE',
        :OLD.eventcost_value, :NEW.eventcost_value);
   END IF;
   IF NVL(:OLD.fk_event,0) !=
      NVL(:NEW.fk_event,0) THEN
      audit_trail.column_update
        (raid, 'FK_EVENT',
        :OLD.fk_event, :NEW.fk_event);
   END IF;
   IF NVL(:OLD.fk_cost_desc,0) !=
      NVL(:NEW.fk_cost_desc,0) THEN
      audit_trail.column_update
        (raid, 'FK_COST_DESC',
        :OLD.fk_cost_desc, :NEW.fk_cost_desc);
   END IF;
   IF NVL(:OLD.fk_currency,0) !=
      NVL(:NEW.fk_currency,0) THEN
      audit_trail.column_update
        (raid, 'FK_CURRENCY',
        :OLD.fk_currency, :NEW.fk_currency);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
     to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
    to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;

END;
/


