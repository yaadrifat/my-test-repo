CREATE OR REPLACE TRIGGER SCH_EVENT_CRF_AU0
AFTER UPDATE
ON SCH_EVENT_CRF REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_EVENT_CRF', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_eventcrf,0) !=
      NVL(:NEW.pk_eventcrf,0) THEN
      audit_trail.column_update
        (raid, 'PK_eventCRF',
        :OLD.pk_eventcrf, :NEW.pk_eventcrf);
   END IF;
   IF NVL(:OLD.fk_event,' ') !=
      NVL(:NEW.fk_event,' ') THEN
      audit_trail.column_update
        (raid, 'FK_EVENT',
        :OLD.fk_event, :NEW.fk_event);
   END IF;
   IF NVL(:OLD.fk_form,0) !=
      NVL(:NEW.fk_form,0) THEN
      audit_trail.column_update
        (raid, 'FK_FORM',
        :OLD.fk_form, :NEW.fk_form);
   END IF;
   IF NVL(:OLD.form_type,' ') !=
      NVL(:NEW.form_type,' ') THEN
      audit_trail.column_update
        (raid, 'form_type',
        :OLD.form_type, :NEW.form_type);
   END IF;
   IF NVL(:OLD.other_links,' ') !=
      NVL(:NEW.other_links,' ') THEN
      audit_trail.column_update
        (raid, 'other_links',
        :OLD.other_links, :NEW.other_links);
   END IF;
   IF NVL(:OLD.propagate_from,' ') !=
      NVL(:NEW.propagate_from,' ') THEN
      audit_trail.column_update
        (raid, 'propagate_from',
        :OLD.propagate_from, :NEW.propagate_from);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;

    IF NVL(:OLD.creator,0) !=
      NVL(:NEW.creator,0) THEN
      audit_trail.column_update
        (raid, 'creator',
        :OLD.creator, :NEW.creator);
   END IF;


   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
	to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
	to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;

   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;

END;
/


