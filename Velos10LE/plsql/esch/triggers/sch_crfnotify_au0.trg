CREATE OR REPLACE TRIGGER SCH_CRFNOTIFY_AU0
AFTER UPDATE
ON SCH_CRFNOTIFY REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_CRFNOTIFY', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_crfnot,0) !=
      NVL(:NEW.pk_crfnot,0) THEN
      audit_trail.column_update
        (raid, 'PK_CRFNOT',
        :OLD.pk_crfnot, :NEW.pk_crfnot);
   END IF;
   IF NVL(:OLD.fk_crf,0) !=
      NVL(:NEW.fk_crf,0) THEN
      audit_trail.column_update
        (raid, 'FK_CRF',
        :OLD.fk_crf, :NEW.fk_crf);
   END IF;
   IF NVL(:OLD.fk_codelst_notstat,0) !=
      NVL(:NEW.fk_codelst_notstat,0) THEN
      audit_trail.column_update
        (raid, 'FK_CODELST_NOTSTAT',
        :OLD.fk_codelst_notstat, :NEW.fk_codelst_notstat);
   END IF;
   IF NVL(:OLD.crfnot_usersto,' ') !=
      NVL(:NEW.crfnot_usersto,' ') THEN
      audit_trail.column_update
        (raid, 'CRFNOT_USERSTO',
        :OLD.crfnot_usersto, :NEW.crfnot_usersto);
   END IF;
   IF NVL(:OLD.crfnot_notes,' ') !=
      NVL(:NEW.crfnot_notes,' ') THEN
      audit_trail.column_update
        (raid, 'CRFNOT_NOTES',
        :OLD.crfnot_notes, :NEW.crfnot_notes);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
                    to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
        to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;
   IF NVL(:OLD.fk_study,0) !=
      NVL(:NEW.fk_study,0) THEN
      audit_trail.column_update
        (raid, 'FK_STUDY',
        :OLD.fk_study, :NEW.fk_study);
   END IF;
   IF NVL(:OLD.fk_protocol,0) !=
      NVL(:NEW.fk_protocol,0) THEN
      audit_trail.column_update
        (raid, 'FK_PROTOCOL',
        :OLD.fk_protocol, :NEW.fk_protocol);
   END IF;
   IF NVL(:OLD.fk_patprot,0) !=
      NVL(:NEW.fk_patprot,0) THEN
      audit_trail.column_update
        (raid, 'FK_PATPROT',
        :OLD.fk_patprot, :NEW.fk_patprot);
   END IF;
   IF NVL(:OLD.crfnot_globalflag,' ') !=
      NVL(:NEW.crfnot_globalflag,' ') THEN
      audit_trail.column_update
        (raid, 'CRFNOT_GLOBALFLAG',
        :OLD.crfnot_globalflag, :NEW.crfnot_globalflag);
   END IF;

END;
/


