CREATE OR REPLACE TRIGGER SCH_BGTAPNDX_AU0
AFTER UPDATE OF BGTAPNDX_DESC,BGTAPNDX_TYPE,BGTAPNDX_URI,CREATED_ON,CREATOR,FK_BUDGET,IP_ADD,PK_BGTAPNDX,RID
ON SCH_BGTAPNDX REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction
    (raid, 'SCH_BGTAPNDX', :OLD.rid, 'U', :NEW.LAST_MODIFIED_BY);
  IF NVL(:OLD.pk_bgtapndx,0) !=
     NVL(:NEW.pk_bgtapndx,0) THEN
     audit_trail.column_update
       (raid, 'PK_BGTAPNDX',
       :OLD.pk_bgtapndx, :NEW.pk_bgtapndx);
  END IF;
  IF NVL(:OLD.fk_budget,0) !=
     NVL(:NEW.fk_budget,0) THEN
     audit_trail.column_update
       (raid, 'FK_BUDGET',
       :OLD.fk_budget, :NEW.fk_budget);
  END IF;
  IF NVL(:OLD.bgtapndx_desc,' ') !=
     NVL(:NEW.bgtapndx_desc,' ') THEN
     audit_trail.column_update
       (raid, 'BGTAPNDX_DESC',
       :OLD.bgtapndx_desc, :NEW.bgtapndx_desc);
  END IF;
  IF NVL(:OLD.bgtapndx_uri,' ') !=
     NVL(:NEW.bgtapndx_uri,' ') THEN
     audit_trail.column_update
       (raid, 'BGTAPNDX_URI',
       :OLD.bgtapndx_uri, :NEW.bgtapndx_uri);
  END IF;
  IF NVL(:OLD.bgtapndx_type,' ') !=
     NVL(:NEW.bgtapndx_type,' ') THEN
     audit_trail.column_update
       (raid, 'BGTAPNDX_TYPE',
       :OLD.bgtapndx_type, :NEW.bgtapndx_type);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
END;
/


