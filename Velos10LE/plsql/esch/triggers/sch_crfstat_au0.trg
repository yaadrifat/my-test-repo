CREATE OR REPLACE TRIGGER SCH_CRFSTAT_AU0
AFTER UPDATE
ON SCH_CRFSTAT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_CRFSTAT', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_crfstat,0) !=
      NVL(:NEW.pk_crfstat,0) THEN
      audit_trail.column_update
        (raid, 'PK_CRFSTAT',
        :OLD.pk_crfstat, :NEW.pk_crfstat);
   END IF;
   IF NVL(:OLD.fk_crf,0) !=
      NVL(:NEW.fk_crf,0) THEN
      audit_trail.column_update
        (raid, 'FK_CRF',
        :OLD.fk_crf, :NEW.fk_crf);
   END IF;
   IF NVL(:OLD.fk_codelst_crfstat,0) !=
      NVL(:NEW.fk_codelst_crfstat,0) THEN
      audit_trail.column_update
        (raid, 'FK_CODELST_CRFSTAT',
        :OLD.fk_codelst_crfstat, :NEW.fk_codelst_crfstat);
   END IF;
   IF NVL(:OLD.crfstat_enterby,0) !=
      NVL(:NEW.crfstat_enterby,0) THEN
      audit_trail.column_update
        (raid, 'CRFSTAT_ENTERBY',
        :OLD.crfstat_enterby, :NEW.crfstat_enterby);
   END IF;
   IF NVL(:OLD.crfstat_reviewby,0) !=
      NVL(:NEW.crfstat_reviewby,0) THEN
      audit_trail.column_update
        (raid, 'CRFSTAT_REVIEWBY',
        :OLD.crfstat_reviewby, :NEW.crfstat_reviewby);
   END IF;
   IF NVL(:OLD.crfstat_reviewon,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.crfstat_reviewon,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CRFSTAT_REVIEWON',
        to_char(:OLD.crfstat_reviewon,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.crfstat_reviewon,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.crfstat_sentto,' ') !=
      NVL(:NEW.crfstat_sentto,' ') THEN
      audit_trail.column_update
        (raid, 'CRFSTAT_SENTTO',
        :OLD.crfstat_sentto, :NEW.crfstat_sentto);
   END IF;
   IF NVL(:OLD.crfstat_sentflag,0) !=
      NVL(:NEW.crfstat_sentflag,0) THEN
      audit_trail.column_update
        (raid, 'CRFSTAT_SENTFLAG',
        :OLD.crfstat_sentflag, :NEW.crfstat_sentflag);
   END IF;
   IF NVL(:OLD.crfstat_sentby,0) !=
      NVL(:NEW.crfstat_sentby,0) THEN
      audit_trail.column_update
        (raid, 'CRFSTAT_SENTBY',
        :OLD.crfstat_sentby, :NEW.crfstat_sentby);
   END IF;
   IF NVL(:OLD.crfstat_senton,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.crfstat_senton,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CRFSTAT_SENTON',
        to_char(:OLD.crfstat_senton,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.crfstat_senton,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
        to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
                    to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;

END;
/


