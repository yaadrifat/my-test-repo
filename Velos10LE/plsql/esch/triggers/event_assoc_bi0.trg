CREATE  OR REPLACE TRIGGER EVENT_ASSOC_BI0 BEFORE INSERT ON EVENT_ASSOC        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'EVENT_ASSOC', erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'BUDGET_TEMPLATE', :NEW.BUDGET_TEMPLATE);
       Audit_Trail.column_insert (raid, 'CHAIN_ID', :NEW.CHAIN_ID);
       Audit_Trail.column_insert (raid, 'COST', :NEW.COST);
       Audit_Trail.column_insert (raid, 'COST_DESCRIPTION', :NEW.COST_DESCRIPTION);
       Audit_Trail.column_insert (raid, 'COVERAGE_NOTES', :NEW.COVERAGE_NOTES);
       Audit_Trail.column_insert (raid, 'CREATED_ON', :NEW.CREATED_ON);
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'DESCRIPTION', :NEW.DESCRIPTION);
       Audit_Trail.column_insert (raid, 'DISPLACEMENT', :NEW.DISPLACEMENT);
       Audit_Trail.column_insert (raid, 'DURATION', :NEW.DURATION);
       Audit_Trail.column_insert (raid, 'DURATION_UNIT', :NEW.DURATION_UNIT);
       Audit_Trail.column_insert (raid, 'EVENT_CALASSOCTO', :NEW.EVENT_CALASSOCTO);
       Audit_Trail.column_insert (raid, 'EVENT_CALSCHDATE', :NEW.EVENT_CALSCHDATE);
       Audit_Trail.column_insert (raid, 'EVENT_CATEGORY', :NEW.EVENT_CATEGORY);
       Audit_Trail.column_insert (raid, 'EVENT_CPTCODE', :NEW.EVENT_CPTCODE);
       Audit_Trail.column_insert (raid, 'EVENT_DURATIONAFTER', :NEW.EVENT_DURATIONAFTER);
       Audit_Trail.column_insert (raid, 'EVENT_DURATIONBEFORE', :NEW.EVENT_DURATIONBEFORE);
       Audit_Trail.column_insert (raid, 'EVENT_FLAG', :NEW.EVENT_FLAG);
       Audit_Trail.column_insert (raid, 'EVENT_FUZZYAFTER', :NEW.EVENT_FUZZYAFTER);
       Audit_Trail.column_insert (raid, 'EVENT_ID', :NEW.EVENT_ID);
       Audit_Trail.column_insert (raid, 'EVENT_LIBRARY_TYPE', :NEW.EVENT_LIBRARY_TYPE);
       Audit_Trail.column_insert (raid, 'EVENT_LINE_CATEGORY', :NEW.EVENT_LINE_CATEGORY);
       Audit_Trail.column_insert (raid, 'EVENT_MSG', :NEW.EVENT_MSG);
       Audit_Trail.column_insert (raid, 'EVENT_RES', :NEW.EVENT_RES);
       Audit_Trail.column_insert (raid, 'EVENT_SEQUENCE', :NEW.EVENT_SEQUENCE);
       Audit_Trail.column_insert (raid, 'EVENT_TYPE', :NEW.EVENT_TYPE);
       Audit_Trail.column_insert (raid, 'FACILITY_ID', :NEW.FACILITY_ID);
       Audit_Trail.column_insert (raid, 'FK_CATLIB', :NEW.FK_CATLIB);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from  SCH_CODELST where PK_CODELST=:NEW.FK_CODELST_CALSTAT;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_CALSTAT', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from  SCH_CODELST where PK_CODELST=:NEW.FK_CODELST_COVERTYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_COVERTYPE', codeList_desc);
       Audit_Trail.column_insert (raid, 'FK_VISIT', :NEW.FK_VISIT);
       Audit_Trail.column_insert (raid, 'FUZZY_PERIOD', :NEW.FUZZY_PERIOD);
       Audit_Trail.column_insert (raid, 'HIDE_FLAG', :NEW.HIDE_FLAG);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', :NEW.LAST_MODIFIED_DATE);
       Audit_Trail.column_insert (raid, 'LINKED_URI', :NEW.LINKED_URI);
       Audit_Trail.column_insert (raid, 'MSG_TO', :NEW.MSG_TO);
       Audit_Trail.column_insert (raid, 'NAME', :NEW.NAME);
       Audit_Trail.column_insert (raid, 'NOTES', :NEW.NOTES);
       Audit_Trail.column_insert (raid, 'OFFLINE_FLAG', :NEW.OFFLINE_FLAG);
       Audit_Trail.column_insert (raid, 'ORG_ID', :NEW.ORG_ID);
       Audit_Trail.column_insert (raid, 'ORIG_CAL', :NEW.ORIG_CAL);
       Audit_Trail.column_insert (raid, 'ORIG_EVENT', :NEW.ORIG_EVENT);
       Audit_Trail.column_insert (raid, 'ORIG_STUDY', :NEW.ORIG_STUDY);
       Audit_Trail.column_insert (raid, 'PAT_DAYSAFTER', :NEW.PAT_DAYSAFTER);
       Audit_Trail.column_insert (raid, 'PAT_DAYSBEFORE', :NEW.PAT_DAYSBEFORE);
       Audit_Trail.column_insert (raid, 'PAT_MSGAFTER', :NEW.PAT_MSGAFTER);
       Audit_Trail.column_insert (raid, 'PAT_MSGBEFORE', :NEW.PAT_MSGBEFORE);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'SERVICE_SITE_ID', :NEW.SERVICE_SITE_ID);
       Audit_Trail.column_insert (raid, 'STATUS', :NEW.STATUS);
       Audit_Trail.column_insert (raid, 'STATUS_CHANGEBY', :NEW.STATUS_CHANGEBY);
       Audit_Trail.column_insert (raid, 'STATUS_DT', :NEW.STATUS_DT);
       Audit_Trail.column_insert (raid, 'USER_ID', :NEW.USER_ID);
       Audit_Trail.column_insert (raid, 'USR_DAYSAFTER', :NEW.USR_DAYSAFTER);
       Audit_Trail.column_insert (raid, 'USR_DAYSBEFORE', :NEW.USR_DAYSBEFORE);
       Audit_Trail.column_insert (raid, 'USR_MSGAFTER', :NEW.USR_MSGAFTER);
       Audit_Trail.column_insert (raid, 'USR_MSGBEFORE', :NEW.USR_MSGBEFORE);
       Audit_Trail.column_insert (raid, 'PATSCH_UPDATE', :NEW.PATSCH_UPDATE);
COMMIT;
END;
/