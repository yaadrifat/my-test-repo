CREATE  OR REPLACE TRIGGER SCH_BUDGET_AD0 AFTER DELETE ON SCH_BUDGET        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
   usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
    oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'SCH_BUDGET', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'BUDGET_CALENDAR', :OLD.BUDGET_CALENDAR);
       Audit_Trail.column_delete (raid, 'BUDGET_CALFLAG', :OLD.BUDGET_CALFLAG);
       Audit_Trail.column_delete (raid, 'BUDGET_COMBFLAG', :OLD.BUDGET_COMBFLAG);
       Audit_Trail.column_delete (raid, 'BUDGET_CREATOR', :OLD.BUDGET_CREATOR);
       Audit_Trail.column_delete (raid, 'BUDGET_CURRENCY', :OLD.BUDGET_CURRENCY);
       Audit_Trail.column_delete (raid, 'BUDGET_DELFLAG', :OLD.BUDGET_DELFLAG);
       Audit_Trail.column_delete (raid, 'BUDGET_DESC', :OLD.BUDGET_DESC);
       Audit_Trail.column_delete (raid, 'BUDGET_NAME', :OLD.BUDGET_NAME);
       Audit_Trail.column_delete (raid, 'BUDGET_RIGHTS', :OLD.BUDGET_RIGHTS);
       Audit_Trail.column_delete (raid, 'BUDGET_RIGHTSCOPE', :OLD.BUDGET_RIGHTSCOPE);
       Audit_Trail.column_delete (raid, 'BUDGET_SITEFLAG', :OLD.BUDGET_SITEFLAG);
       Audit_Trail.column_delete (raid, 'BUDGET_STATUS', :OLD.BUDGET_STATUS);
       Audit_Trail.column_delete (raid, 'BUDGET_TEMPLATE', :OLD.BUDGET_TEMPLATE);
       Audit_Trail.column_delete (raid, 'BUDGET_TYPE', :OLD.BUDGET_TYPE);
       Audit_Trail.column_delete (raid, 'BUDGET_VERSION', :OLD.BUDGET_VERSION);
       Audit_Trail.column_delete (raid, 'CREATED_ON', :OLD.CREATED_ON);
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'FK_ACCOUNT', :OLD.FK_ACCOUNT);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from SCH_CODELST where PK_CODELST=:OLD.FK_CODELST_STATUS;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_STATUS', codeList_desc);
       Audit_Trail.column_delete (raid, 'FK_SITE', :OLD.FK_SITE);
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', :OLD.LAST_MODIFIED_DATE);
       Audit_Trail.column_delete (raid, 'PK_BUDGET', :OLD.PK_BUDGET);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/