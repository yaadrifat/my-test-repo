CREATE  OR REPLACE TRIGGER SCH_BGTSECTION_AD0 AFTER DELETE ON SCH_BGTSECTION        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'SCH_BGTSECTION', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'BGTSECTION_DELFLAG', :OLD.BGTSECTION_DELFLAG);
       Audit_Trail.column_delete (raid, 'BGTSECTION_NAME', :OLD.BGTSECTION_NAME);
       Audit_Trail.column_delete (raid, 'BGTSECTION_NOTES', :OLD.BGTSECTION_NOTES);
       Audit_Trail.column_delete (raid, 'BGTSECTION_PATNO', :OLD.BGTSECTION_PATNO);
       Audit_Trail.column_delete (raid, 'BGTSECTION_PERSONLFLAG', :OLD.BGTSECTION_PERSONLFLAG);
       Audit_Trail.column_delete (raid, 'BGTSECTION_SEQUENCE', :OLD.BGTSECTION_SEQUENCE);
       Audit_Trail.column_delete (raid, 'BGTSECTION_TYPE', :OLD.BGTSECTION_TYPE);
       Audit_Trail.column_delete (raid, 'BGTSECTION_VISIT', :OLD.BGTSECTION_VISIT);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'FK_BGTCAL', :OLD.FK_BGTCAL);
       Audit_Trail.column_delete (raid, 'FK_VISIT', :OLD.FK_VISIT);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_BUDGETSEC', :OLD.PK_BUDGETSEC);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'SOC_COST_GRANDTOTAL', :OLD.SOC_COST_GRANDTOTAL);
       Audit_Trail.column_delete (raid, 'SOC_COST_SPONSOR', :OLD.SOC_COST_SPONSOR);
       Audit_Trail.column_delete (raid, 'SOC_COST_TOTAL', :OLD.SOC_COST_TOTAL);
       Audit_Trail.column_delete (raid, 'SOC_COST_VARIANCE', :OLD.SOC_COST_VARIANCE);
       Audit_Trail.column_delete (raid, 'SRT_COST_GRANDTOTAL', :OLD.SRT_COST_GRANDTOTAL);
       Audit_Trail.column_delete (raid, 'SRT_COST_SPONSOR', :OLD.SRT_COST_SPONSOR);
       Audit_Trail.column_delete (raid, 'SRT_COST_TOTAL', :OLD.SRT_COST_TOTAL);
       Audit_Trail.column_delete (raid, 'SRT_COST_VARIANCE', :OLD.SRT_COST_VARIANCE);
COMMIT;
END;
/
