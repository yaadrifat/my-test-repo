CREATE OR REPLACE TRIGGER "SCH_EVRES_TRACK_AU0"
AFTER UPDATE
ON SCH_EVRES_TRACK REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE

  raid NUMBER(10);

  usr VARCHAR2(100);

-- JM: on 02/14/08 for Audit Update

BEGIN

  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  usr := getuser(:NEW.last_modified_by);



  audit_trail.record_transaction

    (raid, 'SCH_EVRES_TRACK', :OLD.rid, 'U', usr );



   IF NVL(:OLD.pk_evres_track,0) !=

      NVL(:NEW.pk_evres_track,0) THEN

      audit_trail.column_update

        (raid, 'PK_EVRES_TRACK',

        :OLD.pk_evres_track, :NEW.pk_evres_track);

   END IF;

   IF NVL(:OLD.fk_eventstat,0) !=

      NVL(:NEW.fk_eventstat,0) THEN

      audit_trail.column_update

        (raid, 'FK_EVENTSTAT',

        :OLD.fk_eventstat, :NEW.fk_eventstat);

   END IF;


   IF NVL(:OLD.fk_codelst_role,0) !=

      NVL(:NEW.fk_codelst_role,0) THEN

      audit_trail.column_update

        (raid, 'fk_codelst_role',

        :OLD.fk_codelst_role, :NEW.fk_codelst_role);

   END IF;

   IF NVL(:OLD.evres_track_duration,' ') !=

      NVL(:NEW.evres_track_duration,' ') THEN

      audit_trail.column_update

        (raid, 'EVRES_TRACK_DURATION',

        :OLD.evres_track_duration, :NEW.evres_track_duration);

   END IF;

   IF NVL(:OLD.rid,0) !=

      NVL(:NEW.rid,0) THEN

      audit_trail.column_update

        (raid, 'RID',

        :OLD.rid, :NEW.rid);

   END IF;

   IF NVL(:OLD.last_modified_by,0) !=

      NVL(:NEW.last_modified_by,0) THEN

      audit_trail.column_update

        (raid, 'LAST_MODIFIED_BY',

        :OLD.last_modified_by, :NEW.last_modified_by);

   END IF;

   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=

      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN

      audit_trail.column_update

        (raid, 'LAST_MODIFIED_DATE',

                    to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));


   END IF;

   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=

      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN

      audit_trail.column_update

        (raid, 'CREATED_ON',

                    to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));


   END IF;



   IF NVL(:OLD.ip_add,' ') !=  NVL(:NEW.ip_add,' ') THEN

      audit_trail.column_update

        (raid, 'IP_ADD',

        :OLD.ip_add, :NEW.ip_add);

   END IF;

END;
/


