CREATE OR REPLACE PROCEDURE        "SP_DELETE_SCHEDULES" (p_ids ARRAY_STRING,usr in number,o_ret OUT NUMBER)
IS
   v_cnt NUMBER;
   i NUMBER;

   v_inStr VARCHAR2(32000);
   v_temp VARCHAR2(32000);
   v_sql VARCHAR2(32000);
   v_sql1 VARCHAR2(32000);
   v_sql2 VARCHAR2(32000);
   v_sql1u VARCHAR2(32000);
   v_sql2u VARCHAR2(32000);
v_sql_msg varchar2(32000);

   eventids NUMBER;

   pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SP_DELETE_SCHEDULES', pLEVEL  => Plog.LFATAL);

BEGIN

 begin
   v_cnt := p_ids.COUNT; --get the # of elements in array

   i:=1;
	WHILE i <= v_cnt
	LOOP
	 	  v_temp:='to_number('''||p_ids(i)||''')';

		IF (LENGTH(v_inStr) > 0) THEN
		v_inStr:=v_inStr||', '||v_temp ;
		ELSE
		v_inStr:= '('||v_temp ;
		END IF;

		i := i+1;
	         END LOOP;

		v_inStr:=v_inStr||')' ;

		v_sql1u := ' update sch_eventstat set LAST_MODIFIED_BY = '||usr||' where fk_event in  (select event_id  from sch_events1 where  fk_patprot in ' || v_inStr || ')' ;
		 EXECUTE IMMEDIATE v_sql1u ;

         v_sql1 := ' delete from  sch_eventstat where fk_event in  (select event_id  from sch_events1 where  fk_patprot in ' || v_inStr || ')' ;
          EXECUTE IMMEDIATE v_sql1 ;

		v_sql2u :='update sch_events1 set LAST_MODIFIED_BY = '||usr||' where  fk_patprot in ' || v_inStr;
		 EXECUTE IMMEDIATE v_sql2u ;
		  
	     v_sql2 :='delete from sch_events1  where  fk_patprot in ' || v_inStr;
	  	  EXECUTE IMMEDIATE v_sql2 ;

		 v_sql_msg := ' delete from sch_msgtran where fk_patprot in ' || v_inStr;
		 EXECUTE IMMEDIATE v_sql_msg  ;

         v_sql:='update  eres.er_patprot
              set fk_protocol = null,patprot_start=null,patprot_schday=null,last_modified_by=' ||usr||
              ' where pk_patprot in ' || v_inStr;

	  EXECUTE IMMEDIATE v_sql ;


      EXCEPTION  WHEN OTHERS THEN
        Plog.fatal('exception in SP_DELETE_SCHEDULES' || sqlerrm);
        o_ret:=-1;
 	end;

      commit;
   o_ret:=0;


 END;
/


