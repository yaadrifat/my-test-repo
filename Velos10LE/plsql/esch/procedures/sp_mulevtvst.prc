CREATE OR REPLACE PROCEDURE ESCH."SP_MULEVTVST" (p_enrollId NUMBER, p_user NUMBER , p_arrEventId ARRAY_STRING,
		p_arrStatusId ARRAY_STRING,p_arrOldStatusId ARRAY_STRING,p_arrExeon ARRAY_STRING, p_arrNote ARRAY_STRING,
		p_arrSOS ARRAY_STRING, p_arrCoverage ARRAY_STRING, p_arrRsnForCoverageChng ARRAY_STRING,
		p_ipadd VARCHAR2,o_ret OUT NUMBER,p_calassoc CHAR)

AS

/****************************************************************************************************
** Procedure to update multiple the status, event done date, notes for events in a schedule
** Author: Anu Khanna 17 March 2004
** Input parameter: patProtId
** Input parameter: userId
** Input parameter: Array of event Ids
** Input parameter: Array of Status ids
** Input parameter: Array of Old Status Ids
** Input parameter: Array of Date valid From
** Input parameter: Array of Notes for each event Id
** Input parameter: Array of SOS for each event Id
** Input parameter: Array of Coverage Type for each event Id
** Input parameter: IP Address
** Output parameter: 0 for successful update, -1 for error
**/
v_cnt NUMBER;
v_visit_num NUMBER;
v_status_id NUMBER;
v_old_status_id NUMBER;
v_exeon DATE;
v_event_id NUMBER;
v_note VARCHAR2(4000);
v_sos NUMBER;
v_coverage NUMBER;
v_arrRsnForCoverageChng varchar2(4000);

i NUMBER;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SP_MULEVTVST', pLEVEL  => Plog.LFATAL);


BEGIN

	v_cnt :=  p_arrEventId.COUNT();

	i:=1;

	WHILE i <= v_cnt LOOP

		v_status_Id := TO_NUMBER(p_arrStatusId(i));
		v_old_status_Id := TO_NUMBER(p_arrOldStatusId(i));
		v_exeon := TO_DATE(p_arrExeon(i),PKG_DATEUTIL.F_GET_DATEFORMAT);
		v_note := p_arrNote(i);
		v_event_id := TO_NUMBER(p_arrEventId(i));
		v_sos := TO_NUMBER(p_arrSOS(i));
		v_coverage := TO_NUMBER(p_arrCoverage(i));
        v_arrRsnForCoverageChng := p_arrRsnForCoverageChng(i);

		BEGIN

			IF (p_calassoc='S') THEN

				UPDATE SCH_EVENTS1
				SET EVENT_EXEON = v_exeon,
				ISCONFIRMED = v_status_id,
				NOTES = v_note,
				SERVICE_SITE_ID = v_sos,
				FK_CODELST_COVERTYPE = v_coverage,
                REASON_FOR_COVERAGECHANGE = v_arrRsnForCoverageChng,
				LAST_MODIFIED_BY = p_user,
				LAST_MODIFIED_DATE = SYSDATE,
				-- added  EVENT_EXEBY to fix CL8390
				EVENT_EXEBY = p_user,
				IP_ADD = p_ipadd
				WHERE FK_STUDY = p_enrollId
				AND EVENT_ID = v_event_id;

			ELSE
				UPDATE SCH_EVENTS1
				SET EVENT_EXEON = v_exeon,
				ISCONFIRMED = v_status_id,
				NOTES = v_note,
                REASON_FOR_COVERAGECHANGE = v_arrRsnForCoverageChng,
				SERVICE_SITE_ID = v_sos,
				FK_CODELST_COVERTYPE = v_coverage,
				LAST_MODIFIED_BY = p_user,
				LAST_MODIFIED_DATE = SYSDATE,
				-- added  EVENT_EXEBY to fix CL8390
				EVENT_EXEBY = p_user,
				IP_ADD = p_ipadd
				WHERE FK_PATPROT = p_enrollId
				AND EVENT_ID = v_event_id;

			END IF;

			EXCEPTION  WHEN OTHERS THEN
			P('ERROR');
			o_ret:=-1;
			RETURN;
		END;

		---JM: 26Aug2008, added for #3816, #3586
		-- SM added  LAST_MODIFIED_BY, LAST_MODIFIED_DATE  to fix Bug4005
		if v_status_Id = v_old_status_id then
			update SCH_EVENTSTAT
			set  EVENTSTAT_DT = v_exeon,
			EVENTSTAT_NOTES =  v_note,
			LAST_MODIFIED_BY = p_user,
			LAST_MODIFIED_DATE = SYSDATE
			where fk_event = lpad(to_char(v_event_id),10,'0')
			and PK_EVENTSTAT = (select max(PK_EVENTSTAT) from SCH_EVENTSTAT where fk_event=lpad(to_char(v_event_id),10,'0'));
		else

			UPDATE sch_eventstat
			SET eventstat_enddt = SYSDATE,
			LAST_MODIFIED_BY = p_user,
			LAST_MODIFIED_DATE = SYSDATE
			WHERE fk_event = lpad(to_char(v_event_id),10,'0')
			AND eventstat_enddt IS NULL ;

			INSERT INTO sch_eventstat
			( PK_EVENTSTAT, EVENTSTAT_DT, EVENTSTAT_NOTES, EVENTSTAT, FK_EVENT, CREATOR, CREATED_ON, IP_ADD  )
			VALUES
			( sch_eventstat_seq.NEXTVAL, v_exeon, v_note, v_status_Id, lpad(to_char(v_event_id),10,'0'), p_user, SYSDATE, p_ipadd) ;
		end if;

		/* Call to SP_EVENTNOTIFY to notify the user through mail for the status change*/

		IF (nvl(p_calassoc,'P') <>'S') THEN
			Sp_Eventnotify(v_event_id,v_status_Id,p_user,p_ipadd,v_old_status_Id);
		END IF;

		i := i + 1;
	END LOOP; --v_cnt loop

	COMMIT;
	o_ret:=0;

END; --end of sp_mulEvtVst

/


CREATE OR REPLACE SYNONYM ERES.SP_MULEVTVST FOR SP_MULEVTVST;


CREATE OR REPLACE SYNONYM EPAT.SP_MULEVTVST FOR SP_MULEVTVST;


GRANT EXECUTE, DEBUG ON SP_MULEVTVST TO EPAT;

GRANT EXECUTE, DEBUG ON SP_MULEVTVST TO ERES;

