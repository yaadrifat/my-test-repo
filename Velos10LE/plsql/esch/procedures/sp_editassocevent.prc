CREATE OR REPLACE PROCEDURE        "SP_EDITASSOCEVENT" (
p_protocol_id IN NUMBER,
p_fromDisp  IN NUMBER,
p_toDisp IN NUMBER
)
AS
/*
This sets the event flag to 1 for all old events during the edit protocol
calender process
*/

v_from NUMBER;
BEGIN
v_from:=p_fromDisp;



IF v_from=1 THEN
UPDATE  EVENT_ASSOC
   SET event_flag = 1
 WHERE chain_id = p_protocol_id
   AND event_type='A'
   AND displacement  <= p_toDisp AND displacement<>0;


ELSE

UPDATE  EVENT_ASSOC
   SET event_flag = 1
 WHERE chain_id = p_protocol_id
   AND event_type='A'
   AND displacement BETWEEN  p_fromDisp AND p_toDisp;
END IF;
COMMIT;
END;
/


CREATE SYNONYM ERES.SP_EDITASSOCEVENT FOR SP_EDITASSOCEVENT;


CREATE SYNONYM EPAT.SP_EDITASSOCEVENT FOR SP_EDITASSOCEVENT;


GRANT EXECUTE, DEBUG ON SP_EDITASSOCEVENT TO EPAT;

GRANT EXECUTE, DEBUG ON SP_EDITASSOCEVENT TO ERES;

