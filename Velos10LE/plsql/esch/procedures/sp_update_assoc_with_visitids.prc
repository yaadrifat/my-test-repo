CREATE OR REPLACE PROCEDURE        "SP_UPDATE_ASSOC_WITH_VISITIDS" (
   P_PROTOCOL      IN       NUMBER,
   P_USER          IN       VARCHAR,
   P_IPADD         IN       VARCHAR
)
AS
/****************************************************************************************************
   ** Procedure to update events in a calendar with visit ids. This is driven by displacement currently.

   ** Parameter Description

   ** P_PROTOCOL     event id of the protocol that is to be copied
   ** P_user         user updating the record.
   ** P_IPADD		 ip address updated from.

   **
   ** Author: Sam Varadarajan, Oct 01, 2004
   **
   ** Modification History
   **
   ** Modified By         Date         Remarks
   ** Sam V	   Oct 01, 2004	 		updating the event records, with correct visit ids.

   *****************************************************************************************************
   */

   V_EVENT_ID           NUMBER;
   V_CHAIN_ID           NUMBER;
   V_EVENT_TYPE         CHAR (1);
   V_DISPLACEMENT		NUMBER;
   V_USER_ID          NUMBER;
   V_EVENT_VISIT_ID	  NUMBER; --SV, 9/30
   V_RET			  NUMBER;

-- The new protocol id generated from the EVENT_ASSOCINIITON_SEQ sequence. This is stored in the
-- p_newProtocol
-- This variable is an OUT parameter
   CURSOR C1
   IS
      SELECT EVENT_ID, DISPLACEMENT,FK_VISIT
        FROM EVENT_ASSOC
       WHERE CHAIN_ID = P_PROTOCOL AND
	   event_type = 'A' AND
	   displacement <> 0 AND fk_visit IS NULL;


BEGIN


   OPEN C1;


   LOOP
      FETCH C1 INTO V_EVENT_ID,
	  		   		V_DISPLACEMENT,
                    V_EVENT_VISIT_ID;

      EXIT WHEN C1%NOTFOUND;

      Sp_Get_Prot_Visit(P_PROTOCOL,V_DISPLACEMENT, P_USER, P_IPADD, v_ret);
	  DBMS_OUTPUT.PUT_LINE('v_ret='||TO_CHAR(v_ret));
	 IF (v_ret > 0) THEN
	 	UPDATE EVENT_ASSOC
		 SET FK_VISIT = v_ret,
		 last_modified_by = P_User,
		 last_Modified_date = SYSDATE,
		 ip_add = p_ipadd
		 WHERE EVENT_ID = V_EVENT_ID;
	 END IF;

   END LOOP;

   CLOSE C1;
/**********************************
TEST
set serveroutput on
declare
i Number;

begin
sp_update_assoc_with_visitids(5128,'1523, '66.237.42.110') ;
end ;
**************************************/

   COMMIT;
END Sp_Update_Assoc_With_Visitids;
/


CREATE SYNONYM ERES.SP_UPDATE_ASSOC_WITH_VISITIDS FOR SP_UPDATE_ASSOC_WITH_VISITIDS;


CREATE SYNONYM EPAT.SP_UPDATE_ASSOC_WITH_VISITIDS FOR SP_UPDATE_ASSOC_WITH_VISITIDS;


GRANT EXECUTE, DEBUG ON SP_UPDATE_ASSOC_WITH_VISITIDS TO EPAT;

GRANT EXECUTE, DEBUG ON SP_UPDATE_ASSOC_WITH_VISITIDS TO ERES;

