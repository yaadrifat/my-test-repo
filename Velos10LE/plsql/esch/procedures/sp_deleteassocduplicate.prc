CREATE OR REPLACE PROCEDURE        "SP_DELETEASSOCDUPLICATE" (
p_protocol_id IN NUMBER
)
AS
/*
pl chk the deleteduplicate proc for process comments
*/
BEGIN
 UPDATE EVENT_ASSOC
    SET EVENT_FLAG = 0
  WHERE event_id IN
    (SELECT de.evt FROM
       (SELECT   COST  , DISPLACEMENT  , COUNT(*) , MIN(event_id) evt,fk_visit
          FROM EVENT_ASSOC
         WHERE chain_id = p_protocol_id
           AND event_type = 'A'
        HAVING COUNT(*) > 1
         GROUP BY COST   , DISPLACEMENT,fk_visit,event_id
       ) de
    )
 ;
 DELETE
   FROM EVENT_ASSOC
  WHERE event_id IN
    (SELECT de.evt FROM
       (SELECT   COST  , DISPLACEMENT  , COUNT(*) , MAX(event_id) evt,fk_visit
          FROM EVENT_ASSOC
         WHERE chain_id = p_protocol_id
           AND event_type = 'A'
        HAVING COUNT(*) > 1
         GROUP BY COST   , DISPLACEMENT,fk_visit,event_id
       ) de
    )
 ;
 DELETE
   FROM EVENT_ASSOC
  WHERE EVENT_FLAG = 1
    AND chain_id= p_protocol_id
    AND event_type='A'
    AND displacement <> 0 ;
COMMIT;
END;
/


CREATE SYNONYM ERES.SP_DELETEASSOCDUPLICATE FOR SP_DELETEASSOCDUPLICATE;


CREATE SYNONYM EPAT.SP_DELETEASSOCDUPLICATE FOR SP_DELETEASSOCDUPLICATE;


GRANT EXECUTE, DEBUG ON SP_DELETEASSOCDUPLICATE TO EPAT;

GRANT EXECUTE, DEBUG ON SP_DELETEASSOCDUPLICATE TO ERES;

