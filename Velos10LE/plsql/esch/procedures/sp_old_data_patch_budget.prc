CREATE OR REPLACE PROCEDURE        "SP_OLD_DATA_PATCH_BUDGET" 
AS
/*
  The stored procedure inserts personnel and default hidden section in old budgets
  June 30, 04
*/

    v_pkbudget NUMBER;
    v_pk_bgtcal NUMBER;
    v_seq NUMBER;
    BEGIN

FOR i IN (SELECT pk_budget
          FROM sch_budget
		WHERE budget_template = (SELECT pk_codelst
		                         FROM sch_codelst
							WHERE codelst_type='budget_template'
                                   AND codelst_subtyp = 'patient_bgt' ))
LOOP
   v_pkbudget := i.pk_budget;
   --get all budget cals
   FOR j IN (SELECT  pk_bgtcal FROM sch_bgtcal WHERE fk_budget=v_pkbudget)
   LOOP
       v_pk_bgtcal := j.pk_bgtcal;

      --get the max section sequence
      SELECT MAX(BGTSECTION_SEQUENCE )
	INTO v_seq
	FROM sch_bgtsection
	WHERE FK_BGTCAL = v_pk_bgtcal ;

      v_seq := v_seq + 1;

--insert personnel section
       INSERT INTO sch_bgtsection (PK_BUDGETSEC  ,
			FK_BGTCAL,
			BGTSECTION_NAME,
			BGTSECTION_DELFLAG,
			BGTSECTION_SEQUENCE    ,
			BGTSECTION_PERSONLFLAG   ,
			BGTSECTION_TYPE
			 )
     VALUES (seq_sch_bgtsection.NEXTVAL,
	 v_pk_bgtcal,
	 'Personnel Cost',
	 'N',
	  v_seq,
	  1,
	  'P');


      v_seq := v_seq + 1;

--insert hidden default section
       INSERT INTO sch_bgtsection (PK_BUDGETSEC  ,
			FK_BGTCAL,
			BGTSECTION_NAME,
			BGTSECTION_DELFLAG,
			BGTSECTION_SEQUENCE    ,
			BGTSECTION_PERSONLFLAG   ,
			BGTSECTION_TYPE
			 )
     VALUES (seq_sch_bgtsection.NEXTVAL,
	 v_pk_bgtcal,
	 'Default Section',
	 'P',
	  v_seq,
	  0,
	  'P');



   END LOOP; --bgtcal loop




END LOOP; --budget loop

COMMIT;

    END;
/


CREATE SYNONYM ERES.SP_OLD_DATA_PATCH_BUDGET FOR SP_OLD_DATA_PATCH_BUDGET;


CREATE SYNONYM EPAT.SP_OLD_DATA_PATCH_BUDGET FOR SP_OLD_DATA_PATCH_BUDGET;


GRANT EXECUTE, DEBUG ON SP_OLD_DATA_PATCH_BUDGET TO EPAT;

GRANT EXECUTE, DEBUG ON SP_OLD_DATA_PATCH_BUDGET TO ERES;

