CREATE OR REPLACE PROCEDURE        "SP_GET_SCH_PAGE_RECORDS" 
 (
  p_page NUMBER,
  p_recs_per_page NUMBER,
  p_sql VARCHAR2,
  p_countsql VARCHAR2,
  o_res OUT Gk_Cv_Types.GenericCursorType,
  o_rows OUT NUMBER
 )
IS
-- Find out the first and last record we want
FirstRec NUMBER;
LastRec NUMBER;
V_POS      NUMBER         := 0;
V_FROM VARCHAR2(500);

  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'schedule', pLEVEL  => Plog.LFATAL);

--V_CTSELECT Varchar2(4000);
BEGIN
SELECT (p_page - 1) * p_recs_per_page
INTO FirstRec
FROM dual;
SELECT p_page * p_recs_per_page + 1
INTO LastRec
FROM dual;
-- Now, return the set of paged records


BEGIN
	OPEN o_res FOR 'select * from ( select a.*, rownum rnum
	                                FROM (' || p_sql ||') a
							  WHERE ROWNUM < ' || LastRec || ' )
				 WHERE rnum > ' || FirstRec ;

				plog.DEBUG(pCtx, 'schedulesql: '  || 'select * from ( select a.*, rownum rnum
	                                FROM (' || p_sql ||') a
							  WHERE ROWNUM < ' || LastRec || ' )
				 WHERE rnum > ' || FirstRec) ;

	--Commented by Sonika on Feb 25,03
	--V_CTSELECT := p_countsql ;
	plog.DEBUG(pCtx, 'pcountsql' || p_countsql);

	EXECUTE IMMEDIATE p_countsql INTO o_rows;
 EXCEPTION WHEN OTHERS THEN

	PLOG.FATAL(pCTX , 'err in Sp_Get_Sch_Page_Records' || SQLERRM);

END;

END Sp_Get_Sch_Page_Records ;
/


CREATE SYNONYM ERES.SP_GET_SCH_PAGE_RECORDS FOR SP_GET_SCH_PAGE_RECORDS;


CREATE SYNONYM EPAT.SP_GET_SCH_PAGE_RECORDS FOR SP_GET_SCH_PAGE_RECORDS;


GRANT EXECUTE, DEBUG ON SP_GET_SCH_PAGE_RECORDS TO EPAT;

GRANT EXECUTE, DEBUG ON SP_GET_SCH_PAGE_RECORDS TO ERES;

