CREATE OR REPLACE PACKAGE        "PKG_ALNOT" 
AS
/***********************************************************************************************
   **
   ** Author: Sonia Sahni 02/01/2002
   **
*/
PROCEDURE set_alnot(
 p_patprot IN NUMBER,
 p_study IN NUMBER ,
 p_protocol IN NUMBER,
 p_globflag IN VARCHAR2,
 p_creator IN NUMBER ,
 p_created_on IN DATE ,
 p_ipadd IN VARCHAR2);

PROCEDURE set_study_alnot( p_study IN NUMBER ,
 p_creator IN NUMBER ,
 p_created_on IN DATE ,
 p_ipadd IN VARCHAR2);

 PROCEDURE apply_alnot(
 p_study IN NUMBER ,
 p_protocol IN NUMBER,
 p_lastmodified_by IN NUMBER ,
 p_lastmodified_on IN DATE ,
 p_ipadd IN VARCHAR2);

 PROCEDURE update_study_alnot(
 p_study IN NUMBER ,
 p_protocol IN NUMBER,
 p_lastmodified_by IN NUMBER ,
 p_ipadd IN VARCHAR2,
 p_globflag IN VARCHAR2,
 p_lockvalue IN NUMBER);


 PROCEDURE copy_crfnotify ( p_FK_CRF IN NUMBER ,
                           p_FK_CODELST_NOTSTAT     IN NUMBER ,
                           p_CRFNOT_USERSTO IN VARCHAR2,
                           p_CREATOR   IN NUMBER ,
                           p_IP_ADD  IN VARCHAR2,
                           p_FK_STUDY IN NUMBER ,
                           p_FK_PROTOCOL IN NUMBER ,
                           p_FK_PATPROT   IN NUMBER ,
                           p_CRFNOT_GLOBALFLAG IN VARCHAR2
 );

  PROCEDURE sp_adversevenotify (
     P_STUDY NUMBER,
     P_PATIENT NUMBER,
	P_ADVTYPE NUMBER,
	P_OUTCOME VARCHAR2,
	P_CREATOR NUMBER,
	P_IPADD VARCHAR2
  ) ;

   PROCEDURE sp_deathnotify (
     P_FKADVERSE NUMBER,
	P_OUTCOME NUMBER,
	P_CREATOR NUMBER,
	P_IPADD VARCHAR2
);


  PROCEDURE sp_advnotify_for_studylevel(
    P_STUDY NUMBER,
	 P_PATIENT NUMBER,
	 P_CREATOR NUMBER,
	P_IPADD VARCHAR2
);

PROCEDURE sp_deathnotify_for_studylevel (
     P_FKADVERSE NUMBER,
	P_CREATOR NUMBER,
	P_IPADD VARCHAR2
);

   PROCEDURE sp_notifyadmin (
     P_USER NUMBER,
	P_IPADD VARCHAR2,
	P_VELOSUSER NUMBER,
	P_FAILDATE VARCHAR2,
     P_FAILTIME VARCHAR2,
	O_MAIL OUT VARCHAR2 ,
     O_TOUSER OUT NUMBER ,
	O_TOUSERMAIL OUT VARCHAR2
);

 pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_ALNOT', pLEVEL  => Plog.LFATAL);

 PROCEDURE set_alertnotify_for_enrollment (p_study NUMBER, p_protocol NUMBER, p_patprot NUMBER, p_creator  NUMBER,
 p_created_on  DATE,p_ip_add VARCHAR2);

END Pkg_Alnot;
/


CREATE SYNONYM ERES.PKG_ALNOT FOR PKG_ALNOT;


CREATE SYNONYM EPAT.PKG_ALNOT FOR PKG_ALNOT;


GRANT EXECUTE, DEBUG ON PKG_ALNOT TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_ALNOT TO ERES;

