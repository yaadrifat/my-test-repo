CREATE OR REPLACE PACKAGE "ESCH"."PKG_PROPGT_EVTUPDATES" IS
PROCEDURE SP_PROPAGATE_EVT_UPDATES(
	p_protocol_id IN NUMBER,
	p_selected_event_id IN NUMBER,
	p_table_name IN VARCHAR2,
	p_primary_keyvalue IN NUMBER,
	p_copy_to_same_visits IN CHAR,
	p_copy_to_same_events IN CHAR,
	p_mode IN CHAR DEFAULT 'M',
	p_calType IN CHAR DEFAULT 'P'
	);
	PROCEDURE sp_update_event_detail(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, source IN VARCHAR2,p_target_eventcopyflag_list IN Types.SMALL_STRING_ARRAY);
	PROCEDURE sp_update_event_message(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, source IN VARCHAR2);
	PROCEDURE sp_add_event_cost(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, p_pk_eventcost IN NUMBER);

	PROCEDURE sp_update_event_cost(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, p_pk_eventcost IN NUMBER, p_mode IN CHAR);
	PROCEDURE sp_delete_event_cost(p_source_event_id IN NUMBER, p_pk_eventcost IN NUMBER, p_mode IN CHAR);

	PROCEDURE sp_add_event_appendix(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, p_pk_docs IN NUMBER);

	PROCEDURE sp_update_event_appendix(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, p_pk_docs IN NUMBER, p_mode IN CHAR);

	PROCEDURE sp_add_event_resource(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, p_pk_eventusr IN NUMBER, p_type CHAR);
	PROCEDURE sp_add_event_crflib(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, pk_crflib IN NUMBER);
	PROCEDURE sp_update_event_crflib(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, pk_crflib IN NUMBER, p_mode IN CHAR);

	PROCEDURE sp_manage_event_user(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, p_pk_eventuser IN NUMBER , p_usertype IN VARCHAR2, p_mode IN CHAR);

	PROCEDURE sp_add_event_crf_form(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array);

	PROCEDURE sp_add_event_kit(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array);
	PROCEDURE sp_delete_event_kit(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array,p_pkeventkit  in number);

pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_PROPGT_EVTUPDATES', pLEVEL  => Plog.LFATAL);

END Pkg_Propgt_Evtupdates;
/


CREATE SYNONYM ERES.PKG_PROPGT_EVTUPDATES FOR PKG_PROPGT_EVTUPDATES;


CREATE SYNONYM EPAT.PKG_PROPGT_EVTUPDATES FOR PKG_PROPGT_EVTUPDATES;


GRANT EXECUTE, DEBUG ON PKG_PROPGT_EVTUPDATES TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_PROPGT_EVTUPDATES TO ERES;

