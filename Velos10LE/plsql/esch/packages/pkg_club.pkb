CREATE OR REPLACE PACKAGE BODY        "PKG_CLUB"
AS
/*
Date : 26 Sept 2002
Author: Sonia Sahni
*/
 PROCEDURE SP_GENCLUBMAIL
 AS
   /*
   Date : 26 Sept 2002
   Author: Sonia Sahni
   Purpose: Wrapper Procedure - 1. Calls all daily procedures
                                2. Generates clubbed mails per user
        3. Transfers mails to sch_dispatch message for eresmailer
  Modified by Sonia Abrol, 03/29/2005, - to filter out emails if user does not have right to the patient's site
   */
 v_user NUMBER ;
 v_prevuser NUMBER;
 v_msgtype VARCHAR2(10);
 v_msgsendon DATE := SYSDATE;
 v_msgtext VARCHAR2(4000);
 v_section VARCHAR2(200);
 v_header VARCHAR2(4000);
 v_footer VARCHAR2(4000);
 v_newtemp VARCHAR2(200);
 v_shownone BOOLEAN := FALSE;
 v_clubmessage clob;
 V_AR_MSGTYPE  Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
 V_AR_MSGSECTION Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();

 V_AR_PMSGTYPE  Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
 V_AR_PMSGSECTION Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();

 V_COUNT NUMBER := 0;
 v_rec_count NUMBER := 0;
 v_patprot NUMBER := 0;
 v_site_right NUMBER := 0;

-- v_study_num VARCHAR2(30);
  v_study_num VARCHAR2(100);--JM:
  v_patcode       VARCHAR2 (25);
  v_actschdate VARCHAR2 (15);

 BEGIN
  -- call daily procedures :

	--Pkg_Milnot.SP_CREATE_MILNOT; -- Event and Visit Milestone Achievement Notifications
    -- Pkg_Milnot.SP_CREATE_PM_MILNOT; -- Patient Milestone Achievement Notifications

     Pkg_Daily.SP_PASTSCHNOTIFY; -- Past Schedule Date Notifications
	 -- tkg commented for fixing the bugzilla issuse #2492
--     Pkg_Daily.SP_CRFTRACKING;  -- CRF Tracking Notifications
  Pkg_Lnkform.SP_FILLED_FORM_NOTIFICATIONS; -- filled forms based notifications

    -- p('all procs executed');
     --get mail header n footer
     v_header  :=    Pkg_Common.SCH_GETMAILMSG('header');
     v_footer  :=    Pkg_Common.SCH_GETMAILMSG('footer');
-- v_newtemp  :=   Pkg_Common.SCH_GETMAILMSG('newline');
   v_newtemp  :=   CHR(13) || '----------------------------------------------------------------------' || CHR(13); -- carriage return character



  -- generate clubbed mails
  --get mail types
  -- modified by gopu for fix the Bugzilla Issue #2492
  FOR j IN (SELECT trim(codelst_subtyp) codelst_subtyp, codelst_desc FROM SCH_CODELST WHERE codelst_type = 'cmail_type'  AND trim(codelst_subtyp)<>'dl_crf' ORDER BY codelst_seq)
   LOOP
      V_COUNT := V_COUNT + 1;
      V_AR_MSGTYPE.EXTEND;
      V_AR_MSGTYPE(V_COUNT) := j.codelst_subtyp;
      V_AR_MSGSECTION.EXTEND;
      V_AR_MSGSECTION(V_COUNT) := j.codelst_desc;
    --  p('mail types - ' || j.codelst_subtyp);
   END LOOP;
   -- get mail types  for patients

   V_COUNT := 0;

   FOR a IN (SELECT trim(codelst_subtyp) codelst_subtyp, codelst_desc FROM SCH_CODELST WHERE codelst_type = 'cmail_type_pat' ORDER BY codelst_seq)
        LOOP
                V_COUNT := V_COUNT + 1;
                V_AR_PMSGTYPE.EXTEND;
                V_AR_PMSGTYPE(V_COUNT) := a.codelst_subtyp;
                V_AR_PMSGSECTION.EXTEND;
                V_AR_PMSGSECTION(V_COUNT) := a.codelst_desc;

        END LOOP;


      -- end of get mail types  for patients

   --when use is application user
      FOR k IN (SELECT DISTINCT TRUNC(msg_sendon) msg_sendon, fk_user FROM SCH_MSGTRAN WHERE TRUNC(msg_sendon) <= TRUNC(SYSDATE) AND msg_status = 0 ORDER BY msg_sendon ) -- get distinct users + msg_sendon combinations
      LOOP

    v_msgsendon := k.msg_sendon;
       v_user := k.fk_user;

         --  p('user ' || v_user);
         -- get header
          v_clubmessage := v_header;
          v_rec_count := 0;

          -- loop for all message types
         FOR s IN 1..V_AR_MSGTYPE.COUNT
           LOOP -- loop for all message types
               v_msgtype  := trim(V_AR_MSGTYPE(s));
               v_section := trim(V_AR_MSGSECTION(s));
     v_shownone := TRUE;
                -- p('IN MSG LOOP' || v_section);
               v_clubmessage := v_clubmessage || v_newtemp || v_section || v_newtemp ;
FOR i IN (SELECT msg_sendon,msg_text,pk_msg,NVL(fk_patprot,0) fk_patprot, fk_study, fk_schevent
                       FROM SCH_MSGTRAN
                        WHERE fk_user = v_user AND TRUNC(msg_sendon) = TRUNC(v_msgsendon) AND msg_status = 0 AND
                        msg_type = v_msgtype
      ORDER BY created_on)
              LOOP
      v_site_right := 0;
     v_patprot := i.fk_patprot;

     IF v_patprot > 0 THEN
     BEGIN
       --get user's rights to the patient's site
      SELECT Pkg_User.f_chk_right_for_patprotsite(v_patprot,v_user)
      INTO v_site_right FROM dual;
      EXCEPTION WHEN NO_DATA_FOUND THEN
         v_site_right := 0;
      END;
    ELSE
        v_site_right  := 1;
    END IF;

      IF v_site_right > 0 THEN --include this in email only if user has rights to the patient's site
             v_rec_count := v_rec_count + 1;
              v_shownone := FALSE;
			  v_msgtext  := i.msg_text;

			  IF v_msgtype = 'ev_usch' THEN --replace placeholders

						   BEGIN
						   			 SELECT study_number INTO v_study_num
						   			 FROM er_study WHERE pk_study = i.fk_study;
							EXCEPTION WHEN NO_DATA_FOUND THEN
									  v_study_num  := '';
							END;

							BEGIN

									SELECT Pkg_Common.sch_getpatcode (v_patprot)
									 INTO v_patcode
									  FROM DUAL;
								EXCEPTION WHEN NO_DATA_FOUND THEN
										  v_patcode := '';

							END;

						  BEGIN
						   			 SELECT NVL(TO_CHAR(actual_schdate,PKG_DATEUTIL.F_GET_DATEFORMAT) ,'') INTO   v_actschdate
						   			 FROM SCH_EVENTS1 WHERE event_id  = i. fk_schevent AND fk_patprot = v_patprot;
							EXCEPTION WHEN NO_DATA_FOUND THEN
									  v_actschdate  := '';
							END;


						   v_msgtext  := REPLACE(v_msgtext,'[*VELSTUDYNUM*]',v_study_num );
   						   v_msgtext  := REPLACE(v_msgtext,'[*VELPATCODE*]',v_patcode );
   						   v_msgtext  := REPLACE(v_msgtext,'[*ACTSCHDATE*]',v_actschdate );

			  END IF;

               v_clubmessage := v_clubmessage || v_newtemp || v_msgtext || v_newtemp;
      END IF;
             --    p('pk_msg ' || i.pk_msg);
              END LOOP;
    IF v_shownone THEN
--	           v_clubmessage := v_clubmessage || v_newtemp || 'None' || v_newtemp;

	           v_clubmessage := v_clubmessage || v_newtemp || 'None' || RPAD(CHR(10),10)|| v_newtemp;
    END IF;
         END LOOP; -- end of loop for all message types
         -- get footer
         v_clubmessage := v_clubmessage || v_newtemp || v_footer;

         IF v_rec_count > 0 THEN
          --insert mail record in sch_dispatch msg
            INSERT INTO SCH_DISPATCHMSG (   PK_MSG,  MSG_SENDON , MSG_STATUS , MSG_TYPE, FK_PAT, MSG_ISCLUBBED, MSG_CLUBTEXT)
            VALUES ( SCH_DISPATCHMSG_SEQ1.NEXTVAL ,v_msgsendon , 0, 'UC',    v_user, 1, v_clubmessage ) ;
         --  p('MAIL INSERTED');
         END IF;
    -- if user is Patient

       -- get header
          v_rec_count := 0;
          v_clubmessage := v_header;
          -- loop for all message types
         FOR s IN 1..V_AR_PMSGTYPE.COUNT
           LOOP -- loop for all message types
               v_msgtype  := trim(V_AR_PMSGTYPE(s));
               v_section := trim(V_AR_PMSGSECTION(s));
     v_shownone := TRUE;
                -- p('IN MSG LOOP' || v_section);
               v_clubmessage := v_clubmessage || v_newtemp || v_section || v_newtemp ;
             FOR b IN (SELECT msg_sendon,msg_text,pk_msg, fk_study, fk_schevent,fk_patprot
                       FROM SCH_MSGTRAN
                        WHERE fk_user = v_user AND TRUNC(msg_sendon) = TRUNC(v_msgsendon) AND msg_status = 0 AND
                        msg_type = v_msgtype ORDER BY created_on)
              LOOP
         v_rec_count := v_rec_count + 1;
                 v_shownone := FALSE;
                 v_msgtext  := b.msg_text;
				 ------------
				 			  IF v_msgtype = 'ev_psch' THEN --replace placeholders

						   BEGIN
						   			 SELECT study_number INTO v_study_num
						   			 FROM er_study WHERE pk_study = b.fk_study;
							EXCEPTION WHEN NO_DATA_FOUND THEN
									  v_study_num  := '';
							END;

							BEGIN

									SELECT Pkg_Common.sch_getpatcode (b.fk_patprot)
									 INTO v_patcode
									  FROM DUAL;
								EXCEPTION WHEN NO_DATA_FOUND THEN
										  v_patcode := '';

							END;

						  BEGIN
						   			 SELECT NVL(TO_CHAR(actual_schdate,PKG_DATEUTIL.F_GET_DATEFORMAT ) ,'') INTO   v_actschdate
						   			 FROM SCH_EVENTS1 WHERE event_id  = b. fk_schevent AND fk_patprot = b.fk_patprot;
							EXCEPTION WHEN NO_DATA_FOUND THEN
									  v_actschdate  := '';
							END;


						   v_msgtext  := REPLACE(v_msgtext,'[*VELSTUDYNUM*]',v_study_num );
   						   v_msgtext  := REPLACE(v_msgtext,'[*VELPATCODE*]',v_patcode );
   						   v_msgtext  := REPLACE(v_msgtext,'[*ACTSCHDATE*]',v_actschdate );

			  END IF;

				 -----------
         v_clubmessage := v_clubmessage || v_newtemp || v_msgtext || v_newtemp;
             --    p('pk_msg ' || i.pk_msg);
              END LOOP;
    IF v_shownone THEN
--           v_clubmessage := v_clubmessage || v_newtemp || 'None' || v_newtemp;

           v_clubmessage := v_clubmessage || v_newtemp || 'None' || RPAD(CHR(10),10)|| v_newtemp;
    END IF;
         END LOOP; -- end of loop for all message types
         -- get footer
         v_clubmessage := v_clubmessage || v_newtemp || v_footer;
             --insert mail record in sch_dispatch msg

    IF v_rec_count > 0 THEN
          INSERT INTO SCH_DISPATCHMSG (  PK_MSG, MSG_SENDON ,  MSG_STATUS  , MSG_TYPE , FK_PAT, MSG_ISCLUBBED, MSG_CLUBTEXT)
             VALUES ( SCH_DISPATCHMSG_SEQ1.NEXTVAL ,  v_msgsendon , 0, 'PC',  v_user, 1, v_clubmessage) ;
            --  p('MAIL INSERTED');
     END IF;

    -- end of patient mail
   END LOOP; -- end loop for get all users

    UPDATE SCH_MSGTRAN
      SET msg_status = 1 WHERE TRUNC(msg_sendon) <= TRUNC(SYSDATE) AND msg_status = 0;
      COMMIT;

 END SP_GENCLUBMAIL;
 /*end of package*/
    END Pkg_Club;
/


CREATE SYNONYM ERES.PKG_CLUB FOR PKG_CLUB;


CREATE SYNONYM EPAT.PKG_CLUB FOR PKG_CLUB;


GRANT EXECUTE, DEBUG ON PKG_CLUB TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_CLUB TO ERES;

