CREATE OR REPLACE FUNCTION        "LST_COSTDESC" (event number ) return varchar is
cursor cur_cost
is
select trim(codelst_desc) event_costdesc
  from sch_eventcost , sch_codelst
 where sch_eventcost.fk_event = event
   and FK_COST_DESC = pk_codelst
   order by event_costdesc desc;

desc_lst varchar2(4000) ;
begin
 for l_rec in cur_cost loop
  desc_lst := l_rec.event_costdesc || ', '|| desc_lst;
 end loop ;
 desc_lst := substr(trim(desc_lst),1,length(trim(desc_lst))-1) ;
return desc_lst ;
end ;
/


CREATE SYNONYM ERES.LST_COSTDESC FOR LST_COSTDESC;


CREATE SYNONYM EPAT.LST_COSTDESC FOR LST_COSTDESC;


GRANT EXECUTE, DEBUG ON LST_COSTDESC TO EPAT;

GRANT EXECUTE, DEBUG ON LST_COSTDESC TO ERES;

