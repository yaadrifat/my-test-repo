CREATE OR REPLACE FUNCTION        "GETDATE" 
return date is
begin
   return sysdate;
end ;
-- END PL/SQL BLOCK (do not remove this line) ----------------------------------
/


CREATE SYNONYM ERES.GETDATE FOR GETDATE;


CREATE SYNONYM EPAT.GETDATE FOR GETDATE;


GRANT EXECUTE, DEBUG ON GETDATE TO EPAT;

GRANT EXECUTE, DEBUG ON GETDATE TO ERES;

