CREATE OR REPLACE FUNCTION        "LRSIZE" ( p_rowid in rowid )
                            return number
    as
        l_size    number;
    begin
        for x in ( select doc_name, doc from sch_docs where rowid = p_rowid )
        loop
            l_size := dbms_lob.getlength( x.doc );
        end loop;

       if ( l_size is null ) then
           raise program_error;
       end if;
       return l_size;
   end;
/


CREATE SYNONYM ERES.LRSIZE FOR LRSIZE;


CREATE SYNONYM EPAT.LRSIZE FOR LRSIZE;


GRANT EXECUTE, DEBUG ON LRSIZE TO EPAT;

GRANT EXECUTE, DEBUG ON LRSIZE TO ERES;

