/////*********This readMe is specific to v11 build #799(compatible with Oracle 12c)**********////
*********************************************************************************************************************************************************************

1. Change requests on "ACC-39863 -- Network Tab"  enhancement: 
	a. The change requests are mentioned at from  points 18-23 in Gap Analysis and Mockups 9-14.
	b. On Velos request We have made a partial release of this task in Build#798. Now this enhancement is completely implemented.
	c. Points covered in this build are : POINT (22).A, POINT (23).B. (i) and (ii), POINT (23).C

2. Form-43402 -- Forms on User, Org and Networks: 
Velos has requested to release this on Priority basis to demonstrate progress to customer.

PLEASE NOTE THAT THIS ENHANCEMENT IS JUST FOR VELOS TO DEMONSTRATE THE PROGRESS.
HE COMPLETE ENHANCEMENT WILL BE DELIVERED LATER.

1. STANDARD ACCOUNT LEVEL ACCESS RIGHTS AND RESTRICTIONS WILL BE APPLICABLE FOR USER AND ORGANIZATION LEVEL FORMS.
2. HERE ARE THE LIST OF ITEMS THAT  ARE NOT COVERED IN THIS BUILD.
    A. Form Association at Network Level.
    B. We didn't implement the form enhancement for Network Tab. so please don't link the form to network on Form Management Tab . Please link the form to USER and Organization only for now.
    C. Delete response functionality : We still need to work on the part of deleting forms response so for now we disabled this functionality. It means, when you click on delete icon for multiple entry form it prompt nothing and for single entry we have hidden the link. Both will be oprational in upcoming Build
    D. Audit link is working, for now we  used the same report id that is used for account form.

    E.This feature have not implemented for Non System User. We will provide this feature in upcoming build.

SO WE REQUEST QA NOT TO VALIDATE  the above features. 




 
 


