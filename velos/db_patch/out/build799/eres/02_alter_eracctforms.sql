
 DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'MOD_PK'
      and table_name = 'ER_ACCTFORMS';

  if (v_column_exists = 0) then
      execute immediate 'alter table ER_ACCTFORMS add MOD_PK  Number';
  end if;
  commit;
end;

/

 DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'ACCT_FORMTYPE'
      and table_name = 'ER_ACCTFORMS';

  if (v_column_exists = 0) then
      execute immediate 'alter table ER_ACCTFORMS ADD ACCT_FORMTYPE  VARCHAR2(50)';
  end if;
  commit;
end;

/
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'ACCT_FORMTYPE'
      and table_name = 'ER_ACCTFORMS';

  if (v_column_exists > 0) then
       execute immediate 'comment on column ER_ACCTFORMS.ACCT_FORMTYPE is ''This column is used for if form is linked to User , Organization and Network. ORG-Organization , USR-User, NTW-Network''';
  end if;
  commit;
end;
/
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'MOD_PK'
      and table_name = 'ER_ACCTFORMS';

  if (v_column_exists > 0) then
       execute immediate 'comment on column ER_ACCTFORMS.MOD_PK is ''This column is used for if form is linked to User , Organization and Network. It will pick the primary key of respective tables.''';
  end if;
  commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,398,2,'02_alter_eracctforms.sql',sysdate,'v11 #799');

commit; 

