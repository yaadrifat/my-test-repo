ALTER TRIGGER ER_LINKEDFORMS_AU0 DISABLE;
/
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'LF_DISPLAYTYPE'
      and table_name = 'ER_LINKEDFORMS';

  if (v_column_exists > 0) then
      execute immediate 'alter table ER_LINKEDFORMS modify LF_DISPLAYTYPE  CHAR(5 byte)';
  end if;
  commit;
end;

/

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'LF_DISPLAYTYPE'
      and table_name = 'ER_LINKEDFORMS';

  if (v_column_exists > 0) then
       execute immediate 'comment on column ER_LINKEDFORMS.LF_DISPLAYTYPE is ''Stores the form display setting (for filling) in the application:S - study LEVEL SP - patient enrolled TO a specific study A- ACCOUNT LEVEL, SA - ALL studies, PA - ALL patients, C- crf , PS - Patient LEVEL (ALL Studies),PR-Patient LEVEL (ALL Studies-RESTRICTED),USR-User Level,ORG-Organization Level,NTW-Network Level'' ';
  end if;
  commit;
end;
/
ALTER TRIGGER ER_LINKEDFORMS_AU0 ENABLE;
/
 
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,398,1,'01_alter_er_linkedforms.sql',sysdate,'v11 #799');

commit; 
  

 
 