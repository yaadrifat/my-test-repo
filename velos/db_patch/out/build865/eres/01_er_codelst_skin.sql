SET DEFINE OFF;
declare
v_table_check number;
v_row_check number;
begin
select count(*) INTO v_table_check from user_tables where table_name='ER_CODELST';
if v_table_check=1
then
    select count(*) into v_row_check from ER_CODELST where codelst_type='skin' and codelst_subtyp='vel_wcg';
    if v_row_check=0
    then
        Insert into ER_CODELST (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_CUSTOM_COL) 
        values (seq_er_codelst.nextval,'skin','vel_wcg','Default WCG','N','deftwcg');
 	commit;
    end if;
end if;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,464,1,'01_er_codelst_skin.sql',sysdate,'v11.1.0 #865');

commit;
