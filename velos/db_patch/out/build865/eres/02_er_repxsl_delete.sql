SET DEFINE OFF;

delete from ER_REPXSL where pk_repxsl in (256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266); 

commit;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,464,2,'02_er_repxsl_delete.sql',sysdate,'v11.1.0 #865');

commit;
/