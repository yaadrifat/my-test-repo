DECLARE
  v_obj_subtyp number := 0;
BEGIN
  SELECT COUNT(*) INTO v_obj_subtyp FROM er_object_settings WHERE object_subtype='new_subm_menu' and object_name='study_menu';
  IF (v_obj_subtyp != 0) THEN
    update er_object_settings 
    set object_sequence=1, object_visible=0 
    where object_subtype='new_subm_menu' and object_name='study_menu';
  END IF;
  commit;
  
  SELECT COUNT(*) INTO v_obj_subtyp FROM er_object_settings WHERE object_subtype='new_menu' and object_name='study_menu';
  IF (v_obj_subtyp != 0) THEN
    update er_object_settings 
    set object_sequence=2
    where object_subtype='new_menu' and object_name='study_menu';
  END IF;
  commit;
  
  SELECT COUNT(*) INTO v_obj_subtyp FROM er_object_settings WHERE object_subtype='open_menu' and object_name='study_menu';
  IF (v_obj_subtyp != 0) THEN
    update er_object_settings 
    set object_sequence=3
    where object_subtype='open_menu' and object_name='study_menu';
  END IF;
  commit;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,349,3,'003_update_er_object_settings.sql',sysdate,'v10 #750');

commit;