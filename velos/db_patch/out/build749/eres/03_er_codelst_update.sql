DECLARE
begin
  update er_codelst 
  set codelst_hide='Y' 
  where codelst_subtyp='dbstudyreview' and codelst_type='newdashboard';
  commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,349,3,'03_er_codelst_update.sql',sysdate,'v10 LE #749');

commit;