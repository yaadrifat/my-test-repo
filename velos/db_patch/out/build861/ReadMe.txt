/////*********This readMe is specific to v11.1.0 build #861(compatible with Oracle 12c)**********////
*******************************************************************************************************************************************************************
1. Regular bug fixing.
2. Following areas are covered for eResearch new skin(UI changes) for WCG theme:
	a)eSample 
	b)Organization
	c)Groups
	d)Portal Admin
	e)Form Management(Main Page)
	f)Account Links
	g)Network(Main Page),Network User documents, Network documents