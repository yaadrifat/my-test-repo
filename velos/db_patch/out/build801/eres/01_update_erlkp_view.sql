DECLARE v_data_exists NUMBER := 0;
BEGIN
 SELECT COUNT(*)
INTO v_data_exists
FROM ER_LKPVIEW
WHERE LKPVIEW_NAME='Patient Study ID'
AND FK_LKPLIB     =  (SELECT PK_LKPLIB  FROM ER_LKPLIB  WHERE LKPTYPE_DESC='Patient Study Status'  AND LKPTYPE_TYPE  ='dyn_p');
  IF (v_data_exists > 0) THEN
      update ER_LKPVIEW set LKPVIEW_FILTER='fk_account=[:ACCID]'
      where LKPVIEW_NAME='Patient Study ID'
      AND FK_LKPLIB     = (SELECT PK_LKPLIB  FROM ER_LKPLIB  WHERE LKPTYPE_DESC='Patient Study Status'  AND LKPTYPE_TYPE  ='dyn_p');
  END IF;
  COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,400,1,'01_update_erlkp_view.sql',sysdate,'v11 #801');
commit; 

