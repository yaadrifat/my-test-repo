set define off;
create or replace procedure "SP_PREPAREDSAMPLE"(
fk_schevents1 in number,
pkStorage in number,
details ARRAY_STRING,
ipadd in varchar2,
pkuser in number
)
IS
tb1 SPLIT_TBL;
v_visit number(20);
v_fksite number(20);
v_pkper number(20);
v_pkstudy number(20);
v_param varchar2(2000);
v_pkcodelst number(20);
v_pkcodelst1 number(20);
v_pkstoragekit number(20);
v_spectype number(20);
v_spectype1 number(20);
v_exptdamount number(20);
v_account number(10);
V_SPEC varchar(50);
v_speciid number(10);
v_count number(10);
v_parentid number(10);
v_exptdunit varchar(20);
v_expspecunit number(20);
v_expspecunt number(20);
v_collection_date date;
v_proc_count number(10):=0;
v_procctype varchar2(1000):=',N';
v_processing varchar2(1000);
v_proctype varchar2(1000);
vStartIdx binary_integer;
vEndIdx   binary_integer;
vCurValue varchar2(1000);
v_countervalue number(10):=0;
v_current number(10):=0;
 vCurValue1 number(10);
 v_exptdamountchild number(10);
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SP_PREPAREDSAMPLE', pLEVEL  => Plog.LFATAL);
begin

Select pk_codelst into v_pkcodelst  from er_codelst where codelst_type='specimen_stat' and codelst_subtyp='Pending';
Select pk_codelst into v_pkcodelst1  from er_codelst where codelst_type='specimen_stat' and codelst_subtyp='Processed';
select pk_codelst into v_expspecunit from er_codelst where codelst_type='spec_q_unit' and codelst_subtyp='GA';
select fk_account into v_account from er_user where pk_user=pkuser;

select fk_visit,(select fk_study from er_study ,er_patprot where pk_study=fk_study and pk_patprot=s1.fk_patprot),
(select pk_per from er_per,er_patprot where pk_per=fk_per and pk_patprot=s1.fk_patprot),
(select fk_site from er_per,er_patprot where pk_per=fk_per and pk_patprot=s1.fk_patprot),ACTUAL_SCHDATE
into v_visit,v_pkstudy,v_pkper ,v_fksite,v_collection_date from sch_events1 s1 where event_id=lpad(fk_schevents1,10,0);
for i in 1.. details.COUNT()
loop
v_param:=details(i);
select pkg_util.f_split(v_param,'_') into tb1 from dual;
v_pkstoragekit:=TO_NUMBER(tb1(1));
v_spectype:=TO_NUMBER(tb1(2));
v_exptdamount:=TO_NUMBER(tb1(3));
v_exptdunit:=tb1(4);
if(v_exptdunit = 'Y') then
v_expspecunt:='';
else
v_expspecunt:=TO_NUMBER(v_exptdunit);

end if;
--plog.FATAL(pCTX, 'v_pkstoragekit:'||v_pkstoragekit ||'v_spectype:' ||v_spectype||'v_exptdamount:'||v_exptdamount);
 select SEQ_ER_SPECIMEN.NEXTVAL into v_parentid from dual;
  select SEQ_ER_SPECIMEN_ID.NEXTVAL into v_speciid from dual;
 INSERT INTO ER_SPECIMEN(PK_SPECIMEN,SPEC_ID,SPEC_TYPE,FK_PER,FK_STUDY,SPEC_COLLECTION_DATE,FK_SITE,CREATOR,CREATED_ON,FK_ACCOUNT,SPEC_EXPECTED_QUANTITY,FK_VISIT,FK_SCH_EVENTS1,IP_ADD,PREPFLAG,SPEC_EXPECTEDQ_UNITS,PREPAREKITID,RID)
 VALUES (v_parentid,v_speciid,v_spectype,v_pkper,v_pkstudy,v_collection_date,v_fksite,pkuser,sysdate,v_account,v_exptdamount,v_visit,fk_schevents1,ipadd,'Y',v_expspecunt,v_pkstoragekit,SEQ_RID.NEXTVAL);
 INSERT INTO ER_SPECIMEN_STATUS (PK_SPECIMEN_STATUS,FK_SPECIMEN,SS_DATE,FK_CODELST_STATUS,FK_STUDY,SS_STATUS_BY,CREATOR,CREATED_ON,IP_ADD,RID)
 VALUES (SEQ_ER_SPECIMEN_STATUS.NEXTVAL,SEQ_ER_SPECIMEN.CURRVAL,sysdate,v_pkcodelst,v_pkstudy,pkuser,pkuser,sysdate,ipadd,SEQ_RID.NEXTVAL);

select def_processing_type into v_processing from ER_STORAGE_KIT where FK_STORAGE=v_pkstoragekit;

v_proctype:=v_processing||v_procctype;
vStartIdx := 0;
vEndIdx   := instr(v_proctype, ',');

while(vEndIdx > 0) loop
    vCurValue := substr(v_proctype, vStartIdx+1, vEndIdx - vStartIdx - 1);
   vCurValue1:=TO_NUMBER(vCurValue);
   plog.FATAL(pCTX, 'vCurValue1'||vCurValue1);
  if v_countervalue!=vCurValue1 then
  plog.FATAL(pCTX, 'else block');
 INSERT INTO ER_SPECIMEN_STATUS (PK_SPECIMEN_STATUS,FK_SPECIMEN,SS_DATE,FK_CODELST_STATUS,FK_STUDY,SS_STATUS_BY,CREATOR,CREATED_ON,IP_ADD,RID,SS_PROC_TYPE,SS_PROC_DATE)
 VALUES (SEQ_ER_SPECIMEN_STATUS.NEXTVAL,SEQ_ER_SPECIMEN.CURRVAL,sysdate,v_pkcodelst1,v_pkstudy,pkuser,pkuser,sysdate,ipadd,SEQ_RID.NEXTVAL,vCurValue1,sysdate);
  end if;
    vStartIdx := vEndIdx;
    vEndIdx := instr(v_proctype, ',', vStartIdx + 1);
  end loop;

v_count:=1;
for j in (select pk_storage from er_storage where fk_storage=v_pkstoragekit)
loop
select def_specimen_type,def_processing_type,DEF_SPEC_QUANTITY into v_spectype1,v_processing,v_exptdamountchild from ER_STORAGE_KIT where FK_STORAGE=j.pk_storage;
V_SPEC:=v_speciid||'_0'||v_count;
INSERT INTO ER_SPECIMEN(PK_SPECIMEN,SPEC_ID,SPEC_TYPE,FK_SPECIMEN,FK_PER,FK_STUDY,SPEC_COLLECTION_DATE,FK_SITE,CREATOR,CREATED_ON,FK_ACCOUNT,SPEC_EXPECTED_QUANTITY,FK_VISIT,FK_SCH_EVENTS1,IP_ADD,SPEC_EXPECTEDQ_UNITS,RID)
VALUES (SEQ_ER_SPECIMEN.NEXTVAL,V_SPEC,v_spectype1,v_parentid,v_pkper,v_pkstudy,v_collection_date,v_fksite,pkuser,sysdate,v_account,v_exptdamountchild,v_visit,fk_schevents1,ipadd,v_expspecunt,SEQ_RID.NEXTVAL);
INSERT INTO ER_SPECIMEN_STATUS (PK_SPECIMEN_STATUS,FK_SPECIMEN,SS_DATE,FK_CODELST_STATUS,FK_STUDY,SS_STATUS_BY,CREATOR,CREATED_ON,IP_ADD,RID)
VALUES (SEQ_ER_SPECIMEN_STATUS.NEXTVAL,SEQ_ER_SPECIMEN.CURRVAL,sysdate,v_pkcodelst,v_pkstudy,pkuser,pkuser,sysdate,ipadd,SEQ_RID.NEXTVAL);
v_proctype:=v_processing||v_procctype;
vStartIdx := 0;
vEndIdx   := instr(v_proctype, ',');

while(vEndIdx > 0) loop
    vCurValue := substr(v_proctype, vStartIdx+1, vEndIdx - vStartIdx - 1);
   vCurValue1:=TO_NUMBER(vCurValue);
   plog.FATAL(pCTX, 'vCurValue1child'||vCurValue1);

  if v_countervalue!=vCurValue1 then

  plog.FATAL(pCTX, 'childelse block');
INSERT INTO ER_SPECIMEN_STATUS (PK_SPECIMEN_STATUS,FK_SPECIMEN,SS_DATE,FK_CODELST_STATUS,FK_STUDY,SS_STATUS_BY,CREATOR,CREATED_ON,IP_ADD,RID,SS_PROC_TYPE,SS_PROC_DATE)
VALUES (SEQ_ER_SPECIMEN_STATUS.NEXTVAL,SEQ_ER_SPECIMEN.CURRVAL,sysdate,v_pkcodelst1,v_pkstudy,pkuser,pkuser,sysdate,ipadd,SEQ_RID.NEXTVAL,vCurValue1,sysdate);
  end if;
    vStartIdx := vEndIdx;
    vEndIdx := instr(v_proctype, ',', vStartIdx + 1);
  end loop;
v_count:=v_count+1;
end loop;
end loop;

end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,434,1,'01_SP_PREPAREDSAMPLE.sql',sysdate,'v11 #835');

commit;	
/