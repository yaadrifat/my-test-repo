set define off;
create or replace PACKAGE        "PKG_BGTUSERS" 
AS

  PROCEDURE sp_addact_users (
     P_BUDGET IN NUMBER,
     P_BGT_RIGHTS IN Varchar2,
     P_USER_TYPE IN Varchar2,
	P_ACCT IN NUMBER,
     P_CREATOR in number ,
     P_CREATED_ON in date ,
     P_IPADD in varchar2
    ) ;


  PROCEDURE sp_delete_bgtusers (
     P_BUDGET IN NUMBER,
     USER_TYPE IN Varchar2,
	 P_lastmod_by IN NUMBER ) ;

  PROCEDURE sp_addsite_users (
     P_BUDGET IN NUMBER,
     P_BGT_RIGHTS IN Varchar2,
     P_USER_TYPE IN Varchar2,
	P_SITE IN NUMBER,
     P_CREATOR in number ,
     P_CREATED_ON in date ,
     P_IPADD in varchar2
    ) ;

  PROCEDURE sp_addteam_users (
     P_BUDGET IN NUMBER,
     P_BGT_RIGHTS IN Varchar2,
     P_USER_TYPE IN Varchar2,
	P_STUDY IN NUMBER,
     P_CREATOR in number ,
     P_CREATED_ON in date ,
     P_IPADD in varchar2
    ) ;

  PROCEDURE sp_update_rights (
     P_BUDGET IN NUMBER,
     P_BGT_RIGHTS IN Varchar2,
     USER_TYPE IN Varchar2,
	P_lastmod_by in number ,
     P_lastmod_date in date ,
     P_IPADD in varchar2
     ) ;

   END PKG_BGTUSERS;
 
/

create or replace PACKAGE BODY        "PKG_BGTUSERS" 
AS
 PROCEDURE sp_addact_users (
     P_BUDGET IN NUMBER,
     P_BGT_RIGHTS IN VARCHAR2,
     P_USER_TYPE IN VARCHAR2,
	P_ACCT IN NUMBER,
     P_CREATOR IN NUMBER ,
     P_CREATED_ON IN DATE ,
     P_IPADD IN VARCHAR2
    )
    AS
/**************************************************************************************************
   **
   ** Author: Sonia Sahni 04/05/2002
   ** Deletes bgt users - revoke access rights for the budget from existing group users
   ** Gives access rights for the budget to all users of an account
*/

BEGIN

    sp_delete_bgtusers (P_BUDGET,P_USER_TYPE,P_CREATOR); --delete existing user access

   INSERT INTO SCH_BGTUSERS ( PK_BGTUSERS, FK_BUDGET, FK_USER , BGTUSERS_RIGHTS ,  BGTUSERS_TYPE ,
    CREATOR  , CREATED_ON , IP_ADD )
   SELECT SEQ_SCH_BGTUSERS.NEXTVAL,P_BUDGET,u.pk_user,P_BGT_RIGHTS,P_USER_TYPE,
    P_CREATOR,sysdate,P_IPADD
   FROM ER_USER u
   WHERE u.fk_Account =  P_ACCT;


END  sp_addact_users ;

PROCEDURE sp_delete_bgtusers (
     P_BUDGET IN NUMBER,
     USER_TYPE IN VARCHAR2,
     P_lastmod_by IN NUMBER )
    AS
/**************************************************************************************************
   **
   ** Author: Sonia Sahni 04/05/2002
   ** Deletes bgt users revoke access rights for the budget from existing group users
   ** Gives access rights for the budget to all users of budget'saccount
*/
BEGIN
Update SCH_BGTUSERS set LAST_MODIFIED_BY=P_lastmod_by WHERE fk_budget = P_BUDGET AND bgtusers_type = USER_TYPE;

DELETE FROM SCH_BGTUSERS
WHERE fk_budget = P_BUDGET AND
bgtusers_type = USER_TYPE;

END  sp_delete_bgtusers ;

 PROCEDURE sp_addsite_users (
     P_BUDGET IN NUMBER,
     P_BGT_RIGHTS IN VARCHAR2,
     P_USER_TYPE IN VARCHAR2,
	P_SITE IN NUMBER,
     P_CREATOR IN NUMBER ,
     P_CREATED_ON IN DATE ,
     P_IPADD IN VARCHAR2
    )
    AS
/**************************************************************************************************
   **
   ** Author: Sonia Sahni 04/05/2002
   ** Deletes bgt users - revoke access rights for the budget from existing group users
   ** Gives access rights for the budget to all users of budget's organisation*/

BEGIN

    sp_delete_bgtusers (P_BUDGET,P_USER_TYPE,P_CREATOR); --delete existing user access

   INSERT INTO SCH_BGTUSERS ( PK_BGTUSERS, FK_BUDGET, FK_USER , BGTUSERS_RIGHTS ,  BGTUSERS_TYPE ,
    CREATOR  , CREATED_ON , IP_ADD )
   SELECT SEQ_SCH_BGTUSERS.NEXTVAL,P_BUDGET,u.pk_user,P_BGT_RIGHTS,P_USER_TYPE,
    P_CREATOR,sysdate,P_IPADD
   FROM ER_USER u
   WHERE u.fk_siteid =  P_SITE;

END  sp_addsite_users ;


   PROCEDURE sp_addteam_users (
     P_BUDGET IN NUMBER,
     P_BGT_RIGHTS IN VARCHAR2,
     P_USER_TYPE IN VARCHAR2,
	P_STUDY IN NUMBER,
     P_CREATOR IN NUMBER ,
     P_CREATED_ON IN DATE ,
     P_IPADD IN VARCHAR2
    )
    AS
/**************************************************************************************************
   **
   ** Author: Sonia Sahni 04/05/2002
   ** Deletes bgt users - revoke access rights for the budget from existing group users
   ** Gives access rights for the budget to all users of budget's study
   ** Modified by Sonia Abrol, 12/21/2006 dont add 'deactivated' team members
   */

BEGIN

    sp_delete_bgtusers (P_BUDGET,P_USER_TYPE,P_CREATOR); --delete existing user access

   INSERT INTO SCH_BGTUSERS ( PK_BGTUSERS, FK_BUDGET, FK_USER , BGTUSERS_RIGHTS ,  BGTUSERS_TYPE ,
    CREATOR  , CREATED_ON , IP_ADD )
   SELECT SEQ_SCH_BGTUSERS.NEXTVAL,P_BUDGET,u.pk_user,P_BGT_RIGHTS,P_USER_TYPE,
    P_CREATOR,sysdate,P_IPADD
   FROM ER_user u, er_study s
   WHERE s.pk_study =  P_STUDY AND u.fk_account = s.fk_account and
   (exists ( select * from er_studyteam st where st.fk_study = p_study and st.fk_user = pk_user and
   NVL(study_team_usr_type,'N') <> 'X')  OR
   Pkg_Superuser.F_Is_Superuser(u.pk_user, s.pk_study) = 1
   );

END  sp_addteam_users ;

PROCEDURE sp_update_rights (
     P_BUDGET IN NUMBER,
     P_BGT_RIGHTS IN VARCHAR2,
     USER_TYPE IN VARCHAR2,
	P_lastmod_by IN NUMBER ,
     P_lastmod_date IN DATE ,
     P_IPADD IN VARCHAR2)
    AS
/**************************************************************************************************
   **
   ** Author: Sonia Sahni 04/05/2002
   ** Updates access rights of budget users (group users)
*/
BEGIN

UPDATE SCH_BGTUSERS
SET BGTUSERS_RIGHTS = P_BGT_RIGHTS,
LAST_MODIFIED_BY = P_lastmod_by , LAST_MODIFIED_DATE = P_lastmod_date, IP_ADD = P_IPADD
WHERE fk_budget = P_BUDGET AND
bgtusers_type = USER_TYPE;

END  sp_update_rights ;

--end of package

END Pkg_Bgtusers;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,462,1,'01_PKG_BGTUSERS.sql',sysdate,'v11.1.0 #863');

Commit;