set define off;

DECLARE
  v_column_exists number;  
BEGIN
	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_PATPROT'
    AND COLUMN_NAME = 'PATPROT_SCHDAY'; 
	
	if (v_column_exists = 1) then
		execute immediate 'ALTER TABLE ERES.ER_PATPROT MODIFY PATPROT_SCHDAY NUMBER(38)';
		dbms_output.put_line('Column PATPROT_SCHDAY is modified to ER_PATPROT');
	else
		dbms_output.put_line('Column PATPROT_SCHDAY does not exists in ER_PATPROT');
	end if;
END;
/
commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,362,1,'01_ALTER_ER_PATPROT.sql',sysdate,'v10 #763');

commit;

