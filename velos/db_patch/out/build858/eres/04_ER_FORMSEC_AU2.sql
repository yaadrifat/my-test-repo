SET DEFINE OFF;
create or replace TRIGGER "ERES"."ER_FORMSEC_AU2" 
AFTER UPDATE OF RECORD_TYPE
ON ER_FORMSEC
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW        WHEN (new.RECORD_TYPE = 'D' ) BEGIN



/*

sets the record_type to 'D' for all the fields in this section

*/

          update er_formfld

		set record_type = 'D', LAST_MODIFIED_BY =:new.LAST_MODIFIED_BY

		where fk_formsec = :new.pk_formsec ;



          update er_formlib

          set FORM_XSLREFRESH  = 1, LAST_MODIFIED_BY =:new.LAST_MODIFIED_BY

          where pk_formlib = :old.fk_formlib ;





	 END;
	 /
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,457,4,'04_ER_FORMSEC_AU2.sql',sysdate,'v11.1.0 #858');
commit;
/