set define off;
create or replace procedure SAVE_NETSTATUS (net_id IN Number,status_Id in number,status_entered_date IN date,notes IN varchar2,o_net_id out number)
is
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SAVE_NETSTATUS', pLEVEL  => Plog.LFATAL);
v_subTyp VARCHAR2(50);
p_parent_id number;
creator_id number;
modified_by number;
created_date date;
ip_addr varchar2(15);

begin

 BEGIN
 Plog.fatal(pCTX,'status_Id:'||status_Id) ;
 select codelst_subtyp into v_subTyp  from er_codelst where pk_codelst=status_Id and codelst_type='networkstat';

Plog.fatal(pCTX,'v_subTyp:'||v_subTyp) ;
Plog.fatal(pCTX,'net_id:'||net_id) ;

SELECT CREATOR,LAST_MODIFIED_BY,CREATED_ON,IP_ADD into creator_id,modified_by,created_date,ip_addr
FROM er_status_history WHERE pk_status=(select max(pk_status) from er_status_history where status_modpk=net_id AND status_modtable='er_nwsites'
AND STATUS_PARENTMODPK IS NULL);

if v_subTyp='pending' then
update er_nwsites set nw_status= status_Id where pk_nwsites=net_id;
else
 for p_net in (SELECT pk_nwsites FROM er_nwsites  START WITH pk_nwsites= net_id CONNECT BY PRIOR pk_nwsites = FK_NWSITES )
 loop
 update er_nwsites set nw_status= status_Id where pk_nwsites=p_net.pk_nwsites;
 if(p_net.pk_nwsites<>net_id) then
 update er_status_history set STATUS_END_DATE=sysdate,LAST_MODIFIED_DATE=sysdate,STATUS_ISCURRENT=0 where STATUS_MODPK=p_net.pk_nwsites and STATUS_END_DATE is null and STATUS_PARENTMODPK IS NULL;
 INSERT INTO er_status_history(PK_STATUS,STATUS_MODPK,STATUS_MODTABLE,FK_CODELST_STAT,STATUS_DATE,CREATOR,RECORD_TYPE,LAST_MODIFIED_BY,CREATED_ON,IP_ADD,STATUS_ISCURRENT)
 VALUES(SEQ_ER_STATUS_HISTORY.nextval,p_net.pk_nwsites,'er_nwsites',status_Id,status_entered_date,creator_id,'N',modified_by,created_date,ip_addr,1);
 end if;

 end loop;
 end if;

if v_subTyp='active' then
 select nvl(fk_nwsites,0) into p_parent_id  from er_nwsites where pk_nwsites=net_id;
 while  p_parent_id <>0
 loop
 update  er_nwsites set nw_status= status_Id where pk_nwsites=p_parent_id;
 update er_status_history set STATUS_END_DATE=sysdate,LAST_MODIFIED_DATE=sysdate,STATUS_ISCURRENT=0 where STATUS_MODPK=p_parent_id and STATUS_END_DATE is null and STATUS_PARENTMODPK IS NULL;
 INSERT INTO er_status_history(PK_STATUS,STATUS_MODPK,STATUS_MODTABLE,FK_CODELST_STAT,STATUS_DATE,CREATOR,RECORD_TYPE,LAST_MODIFIED_BY,CREATED_ON,IP_ADD,STATUS_ISCURRENT)
 VALUES(SEQ_ER_STATUS_HISTORY.nextval,p_parent_id,'er_nwsites',status_Id,status_entered_date,creator_id,'N',modified_by,created_date,ip_addr,1);
 select nvl(fk_nwsites,0) into p_parent_id  from er_nwsites where pk_nwsites=p_parent_id;
 end loop;


 end if;

 o_net_id:=net_id;
 commit;

 END ;

end ;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,401,5,'05_ERESSAVE_NETSTATUS.sql',sysdate,'v11 #802');
commit; 

