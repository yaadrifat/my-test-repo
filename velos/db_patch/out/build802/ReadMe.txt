/////*********This readMe is specific to v11 build #802(compatible with Oracle 12c)**********////
*********************************************************************************************************************************************************************
1. PM team sent a list of items that are to be  incorporated in regular builds.  
2. All these changes are mentioned in the "Network Change Request.docx" 
3. Team has covered 13 of 15 items mentioned in the document in Build#801. 
4. The remaining two items were covered in this build#802
5. The items Covered are marked in YELLOW.
6.  As per Sonia/Umer's request to minimise the efforts done on the Check Submission logic configuration, we have made the changes in couple of
packages and documented the Velos How-To document for Check Submission logic as per the new changes.

To fix struts vulnerability issues New Jars are included in Lib folder[which resides under Velos.ear]. In this context some old Jar files are deleted. 
During the deployment process, if you are replacing the "Velos.ear"  then please note that you need to delete the following jars from the Lib folder. 


Path:-server\standalone\deployments\velos.ear\lib\....

1.commons-fileupload-1.3.jar
2.commons-io-2.0.1.jar
3.commons-lang3-3.1.jar
4.commons-logging-1.1.1.jar
5.freemarker-2.3.19.jar
6.json-lib-2.1-jdk15.jar
7.javassist-3.20.0.GA.jar
8.ognl-3.0.6.jar
9.struts2-convention-plugin-2.3.15.1.jar
10.struts2-core-2.3.15.1.jar
11.xwork-core-2.3.15.1.jar









 
 


