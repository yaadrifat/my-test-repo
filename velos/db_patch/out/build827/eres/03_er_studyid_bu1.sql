SET DEFINE OFF;
create or replace
TRIGGER "ERES"."ER_ER_STUDYID_BU1"
BEFORE UPDATE ON ER_STUDYID
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
begin
	if :old.STUDYID_ID=:new.STUDYID_ID then
:new.LAST_MODIFIED_BY:=null;
:new.LAST_MODIFIED_DATE:=:old.LAST_MODIFIED_DATE;
else
	:new.last_modified_date := sysdate ;
  end if;
 end;
 /
 
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,426,3,'03_er_studyid_bu1.sql',sysdate,'v11 #827');
commit;