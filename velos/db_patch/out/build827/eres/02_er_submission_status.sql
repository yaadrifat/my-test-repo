DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'FILE_DATA'
      and table_name = 'ER_SUBMISSION_STATUS';

  if (v_column_exists = 0) then
      execute immediate 'alter table ER_SUBMISSION_STATUS ADD FILE_DATA  BLOB default empty_blob()';
  end if;
  commit;
end;

/

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'TEMPLATE_FLAG'
      and table_name = 'ER_SUBMISSION_STATUS';

  if (v_column_exists = 0) then
      execute immediate 'alter table ER_SUBMISSION_STATUS ADD TEMPLATE_FLAG  VARCHAR2(5 BYTE)';
  end if;
  commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,426,2,'02_er_submission_status.sql',sysdate,'v11 #827');
commit;

