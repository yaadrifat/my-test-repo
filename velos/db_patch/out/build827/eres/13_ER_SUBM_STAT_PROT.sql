create or replace TRIGGER "ERES"."ER_SUBMISSION_STATUS_AI0_PROT"
AFTER INSERT
ON ERES.ER_SUBMISSION_STATUS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
     WHEN (
NEW.FK_SUBMISSION_BOARD is not null and new.SUBMISSION_STATUS is not null
      )
DECLARE
v_stat NUMBER(10);

o_success NUMBER;
v_stattype number;

v_study_stat_subtyp varchar2(20);
v_stattype_subtype varchar2(20);
v_site number;
v_board_1 varchar2(200);
v_board_2 varchar2(200);
v_subm_new_app varchar2(200);
v_subm_stdamm varchar2(200);
v_subm_stat varchar2(200);
v_subm_stat_appr varchar2(200);
v_review_board_name varchar2(200);
v_submission_type varchar2(200);
v_submission_status varchar2(200);
v_cnt_for_site number;
v_codelst_for_crc number;
v_codelst_for_irb number;
v_codelst_for_crcapp number;
v_study number;
v_site_1 number;
v_apndx_result number;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'ER_SUBMISSION_STATUS_AI0', pLEVEL  => Plog.LDEBUG);

BEGIN


--insert a default status for a new study
SELECT pk_codelst INTO v_codelst_for_crc FROM er_codelst where codelst_type ='studystat' AND codelst_subtyp='crcReturn';

select pk_codelst into v_codelst_for_crcapp from er_codelst where codelst_type ='studystat' and codelst_subtyp='crcApproved';

select pk_codelst into v_codelst_for_irb from er_codelst where codelst_type ='studystat' and codelst_subtyp='irb_add_info';

v_board_1:='Institutional Review Board';

v_board_2:='Clinical Research Committee';

select codelst_desc into v_subm_stat from er_codelst where codelst_type='subm_status' and codelst_subtyp='pi_resp_req';

select codelst_desc into v_subm_stat_appr from er_codelst where codelst_type='subm_status' and codelst_subtyp='approved';

select codelst_desc into v_subm_new_app from er_codelst where codelst_type='submission' and codelst_subtyp='new_app';

select codelst_desc into v_subm_stdamm from er_codelst where codelst_type='submission' and codelst_subtyp='study_amend';

select review_board_name into v_review_board_name
from er_review_board
where pk_review_board = (select fk_review_board from er_submission_board where pk_submission_board = :new.fk_submission_board) ;

select codelst_desc into v_submission_type
from er_codelst where pk_codelst = (select submission_type from er_submission where pk_submission = :new.fk_submission);

select codelst_desc into  v_submission_status from er_codelst where pk_codelst  = :new.SUBMISSION_STATUS ;

select fk_study into v_study from er_submission where pk_submission = :new.fk_submission;

--select fk_site into v_site_1 from er_studystat where fk_study=v_study and current_stat=1;

select count(fk_site) into v_cnt_for_site from er_studystat ss,er_site s where  ss.fk_study=v_study and ss.current_stat=1 and ss.fk_site=s.pk_site and s.site_parent is null;

if(v_cnt_for_site=0) then
  select fk_site into v_site_1 from er_studystat where fk_study=v_study and current_stat=1;
else
  select fk_site into v_site_1 FROM er_studystat WHERE pk_studystat = (select min(pk_studystat) from er_studystat where fk_study=v_study);
end if;

if(v_submission_type=v_subm_new_app and v_submission_status=v_subm_stat) then

update er_studystat
set current_stat = 0
where fk_study =  v_study and current_stat = 1 ;

INSERT INTO ER_STUDYSTAT
                (pk_studystat,fk_user_docby,
                fk_codelst_studystat,
                fk_study,
                fk_site,
                studystat_date,
                creator,
                created_on,
                ip_add,
                current_stat,STUDYSTAT_NOTE)
                VALUES
                (seq_er_studystat.NEXTVAL, :NEW.creator,
                v_codelst_for_crc,
                v_study,
                 v_site_1,
                 --TO_DATE(TO_CHAR(SYSDATE,'mm/dd/yyyy'),'mm/dd/yyyy')
                :NEW.SUBMISSION_STATUS_DATE,
                :NEW.creator,
                SYSDATE,
                :NEW.ip_add, 1,:new.SUBMISSION_NOTES);

/* Copying of documents from old study version to new study version as the study is unlocked */

PKG_STUDYSTAT.SP_COPY_PREV_VERSION_DOCS (v_study,:NEW.creator,'crcReturn',v_apndx_result);

IF v_apndx_result > 0 then
  dbms_output.put_line('Copying of Documents completed successfully');
else
  dbms_output.put_line('Copying of Documents not completed successfully');
end if;

/* Resetting the study locked flag to 0 */
update er_study set STUDY_IS_LOCKED =0 where pk_study=v_study;

end if;
if(v_submission_type=v_subm_stdamm and v_submission_status=v_subm_stat) then

update er_studystat
set current_stat = 0
where fk_study =  v_study and current_stat = 1 ;

INSERT INTO ER_STUDYSTAT
                (pk_studystat,fk_user_docby,
                fk_codelst_studystat,
                fk_study,
                fk_site,
                studystat_date,
                creator,
                created_on,
                ip_add,
                current_stat,STUDYSTAT_NOTE)
                VALUES
                (seq_er_studystat.NEXTVAL, :NEW.creator,
                v_codelst_for_irb,
                v_study,
                 v_site_1,
                -- TO_DATE(TO_CHAR(SYSDATE,'mm/dd/yyyy'),'mm/dd/yyyy')
                :NEW.SUBMISSION_STATUS_DATE,
                :NEW.creator,
                SYSDATE,
                :NEW.ip_add, 1,:new.SUBMISSION_NOTES);

/* Copying of documents from old study version to new study version as the study is unlocked */

PKG_STUDYSTAT.SP_COPY_PREV_VERSION_DOCS (v_study,:NEW.creator,'irb_add_info',v_apndx_result);

IF v_apndx_result > 0 then
  dbms_output.put_line('Copying of Documents completed successfully');
else
  dbms_output.put_line('Copying of Documents not completed successfully');
end if;

/* Resetting the study locked flag to 0 */
update er_study set STUDY_IS_LOCKED =0 where pk_study=v_study;

end if;

if(v_submission_status=v_subm_stat_appr) then
  for flxVersion in (select PAGE_VER, PAGE_MINOR_VER, FK_SUBMISSION from UI_FLX_PAGEVER where PAGE_MOD_TABLE = 'er_study'
			and PAGE_MOD_PK = v_study order by PAGE_VER desc, PAGE_MINOR_VER desc nulls last)
  loop
    update UI_FLX_PAGEVER set PAGE_MOD_STAT_SUBTYP = 'app_CHR' where
			 PAGE_MOD_TABLE = 'er_study' and PAGE_MOD_PK = v_study
			 and PAGE_VER = flxVersion.PAGE_VER and PAGE_MINOR_VER = flxVersion.PAGE_MINOR_VER;
  end loop;
end if;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,426,13,'13_ER_SUBM_STAT_PROT.sql',sysdate,'v11 #827');
commit;