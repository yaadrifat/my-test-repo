SET DEFINE OFF;
DECLARE
  v_cnt NUMBER;
BEGIN
  
  SELECT COUNT(*)
  INTO v_cnt from sch_codelst
  WHERE codelst_type ='fillformstat'
  AND CODELST_SUBTYP='ae_lockdown';
  
  IF(v_cnt           <1) THEN
    INSERT
    INTO sch_codelst
      (
        PK_CODELST,
        CODELST_TYPE,
        CODELST_SUBTYP,
        CODELST_DESC,
        CODELST_HIDE,
        CODELST_SEQ
      )
      VALUES
      (
        SCH_CODELST_SEQ1.nextval,
        'fillformstat',
        'ae_lockdown',
        'Lock Down',
        'N',
        3
      );
  END IF;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,303,1,'01_SCH_CODELST.sql',sysdate,'v9.3.0 #704');

commit;