
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ESCH"."ERV_CALENDAR_TEMPLATE" ("PROT_ID", "PROT_NAME", "PROT_DESC", "COVERAGE_TYPE", "EVENT_CPTCODE", "PROT_DURATION", "VISIT_NAME", "VISIT_DESC", "VISIT_WIN_BEFORE", "VISIT_WIN_AFTER", "VISIT_INTERVAL", "EVENT_NAME", "EVENT_DESC", "EVENT_ID", "EVENT_DURATION", "VWINDOW_BEF", "VWINDOW_AFT", "FUZZY_DURATION_BEF", "FUZZY_DURATION_AFT", "SCH_DATE", "SCH_DATE_GRP", "SCH_DATE_GRP_DISP", "EVENT_COST", "RESOURCES", "APPENDIX", "MESSAGES", "FORMS", "DISPLACEMENT", "INSERT_AFTER", "EVENT_SEQUENCE", "EVENT_TYPE", "HIDE_FLAG", "PK_PROTOCOL_VISIT") AS 
  SELECT x.PROTOCOL AS prot_id,
    a.NAME          AS prot_name,
    a.description   AS prot_desc,
    x.coverage_type AS coverage_type ,
    x.EVENT_CPTCODE AS EVENT_CPTCODE,
    DECODE (duration_unit, 'D', duration, 'W', duration / 7, 'M', duration / 30, 'Y', duration / 365)
    || DECODE (duration_unit, 'D', ' Days', 'W', ' Weeks', 'M', ' Months', 'Y', ' Years') AS prot_duration,
    x.VISIT_NAME,
    x.DESCRIPTION      AS visit_desc,
    x.visit_win_before AS visit_win_before,
    x.visit_win_after  AS visit_win_after,
    CASE
      WHEN x.INTERVAL IS NULL
      THEN 'No Interval Defined'
      ELSE x.INTERVAL
        ||'  '
        || DECODE (x.INTERVAL_after, NULL, '', x.INTERVAL_after
        || ' After')
    END    AS visit_interval,
    x.NAME AS event_name,
    x.event_desc,
    x.event_id,
    x.event_duration,
    x.vwindow_bef,
    x.vwindow_aft,
    x.fuzzy_duration_bef,
    x.fuzzy_duration_aft,
    x.SCH_DATE,
    TO_CHAR (x.SCH_DATE, 'yyyymm')     AS sch_date_grp,
    TO_CHAR (x.SCH_DATE, 'Month yyyy') AS sch_date_grp_disp,
    x.COST                             AS event_cost,
    x.RESOURCES,
    x.APPENDIX,
    x.MESSAGES,
    x.FORMS,
    x.displacement,
    DECODE(x.insert_after,NULL,'',x.insert_after
    || ')'),
    x.event_sequence,
    x.event_type,
    x.hide_flag,
    x.PK_PROTOCOL_VISIT
  FROM EVENT_DEF a,
    (SELECT chain_id AS protocol,
      c.codelst_desc AS coverage_type ,
      EVENT_CPTCODE,
      DECODE (fuzzy_period, '0', '', fuzzy_period
      || ' '
      || DECODE (event_durationbefore, 'H', 'Hours', 'D', 'Days', 'W', 'Weeks', 'M', 'Months')
      || '(BEFORE)') AS vwindow_bef,
      -- Query modified by Gopu to fix the bugzilla issue #2625
      DECODE (TRIM (event_fuzzyafter), '', '', NULL, '', 0, '', event_fuzzyafter
      || ' '
      || DECODE (event_durationafter, 'H', 'Hours', 'D', 'Days', 'W', 'Weeks', 'M', 'Months')
      || '(AFTER)')                                                                                                                AS vwindow_aft,
      DECODE (event_durationbefore, 'D', (fuzzy_period    * 1), 'H', 1, 'W', (fuzzy_period * 7), 'M', (fuzzy_period * 30))         AS fuzzy_duration_bef,
      DECODE (event_durationafter, 'D', (event_fuzzyafter * 1), 'H', 1, 'W', (event_fuzzyafter * 7), 'M', (event_fuzzyafter * 30)) AS fuzzy_duration_aft,
      DECODE (duration, 0, '', duration
      || DECODE (duration_unit, 'D', ' Days', 'W', 'Weeks')) AS event_duration,
      visit_name,
      a.description,
      --KM-#4867
      DECODE(win_before_number, 0,'','','',NULL,'',win_before_number
      || ' '
      || DECODE (win_before_unit, 'D','Day(s)','W','Week(s)','M','Month(s)','Y','Year(s)')
      || '(BEFORE)'
      || DECODE(win_after_number,0,'','','',NULL,'',';')
      || ' ') AS visit_win_before,
      DECODE(win_after_number, 0,'','','',NULL,'',win_after_number
      || ' '
      || DECODE (win_after_unit, 'D','Day(s)','W','Week(s)','M','Month(s','Y','Years')
      || '(AFTER)') AS visit_win_after,
      DECODE (num_months, 0, '', 'Month '
      || num_months
      || ', ')
      || DECODE (num_weeks, 0, '', 'Week '
      || num_weeks
      || ', ')
      || DECODE (num_days, NULL, '', 'Day '
      || num_days) AS INTERVAL,
      DECODE (insert_after_interval, 0, '', '( '
      || insert_after_interval
      || ' '
      || DECODE (Insert_after_interval_unit, 'D', 'Day(s)', 'W', 'Week(s)', 'M', 'Month(s)', 'Y', 'Year(s)'))   AS INTERVAL_after,
      eres.pkg_reschedule.f_get_sch_date(a.fk_protocol,a.pk_protocol_visit,pkg_dateutil.f_get_first_dayofyear())AS sch_date,
      NAME,
      b.description AS event_desc,
      b.event_id,
      lst_cost (event_id)  AS COST,
      lst_usr (event_id)   AS resources,
      lst_doc (event_id)   AS appendix,
      lst_mail (event_id)  AS messages,
      lst_forms (event_id) AS forms,
      (
      CASE
        WHEN (a.NUM_DAYS                                                   = 0
        OR PKG_RESCHEDULE.f_check_dayZero(b.chain_id, a.PK_PROTOCOL_VISIT) = 1)
        THEN 0
        ELSE a.displacement
      END) AS displacement,
      (SELECT visit_name
      FROM SCH_PROTOCOL_VISIT
      WHERE pk_protocol_visit = a.insert_after
      ) AS insert_after,
      event_sequence,
      event_type,
      0 AS hide_flag,
      a.PK_PROTOCOL_VISIT
    FROM SCH_PROTOCOL_VISIT a,
      EVENT_DEF b ,
      sch_codelst c
    WHERE pk_protocol_visit    = fk_visit
    AND b.FK_CODELST_COVERTYPE = c.pk_codelst (+)
    ) x
  WHERE protocol = a.event_id
  UNION ALL
  SELECT x.PROTOCOL AS prot_id,
    a.NAME          AS prot_name,
    a.description   AS prot_desc,
    x.coverage_type AS coverage_type ,
    x.EVENT_CPTCODE AS EVENT_CPTCODE,
    DECODE (duration_unit, 'D', duration, 'W', duration / 7, 'M', duration / 30, 'Y', duration / 365)
    || DECODE (duration_unit, 'D', ' Days', 'W', ' Weeks', 'M', ' Months', 'Y', ' Years') AS prot_duration,
    x.VISIT_NAME,
    x.DESCRIPTION      AS visit_desc,
    x.visit_win_before AS visit_win_before,
    x.visit_win_after  AS visit_win_after,
    CASE
      WHEN x.INTERVAL IS NULL
      THEN 'No Interval Defined'
      ELSE x.INTERVAL
        ||'  '
        || DECODE (x.INTERVAL_after, NULL, '', x.INTERVAL_after
        || ' After')
    END    AS visit_interval,
    x.NAME AS event_name,
    x.event_desc,
    x.event_id,
    x.event_duration,
    x.vwindow_bef,
    x.vwindow_aft,
    x.fuzzy_duration_bef,
    x.fuzzy_duration_aft,
    x.SCH_DATE,
    TO_CHAR (x.SCH_DATE, 'yyyymm')     AS sch_date_grp,
    TO_CHAR (x.SCH_DATE, 'Month yyyy') AS sch_date_grp_disp,
    x.COST                             AS event_cost,
    x.RESOURCES,
    x.APPENDIX,
    x.MESSAGES,
    x.FORMS,
    x.displacement,
    DECODE(x.insert_after,NULL,'',x.insert_after
    || ')'),
    x.event_sequence,
    x.event_type,
    x.hide_flag,
    x.PK_PROTOCOL_VISIT
  FROM EVENT_ASSOC a,
    (SELECT chain_id AS protocol,
      c.codelst_desc AS coverage_type ,
      EVENT_CPTCODE,
      DECODE (fuzzy_period, '0', '', fuzzy_period
      || ' '
      || DECODE (event_durationbefore, 'H', 'Hours', 'D', 'Days', 'W', 'Weeks', 'M', 'Months')
      || '(BEFORE)') AS vwindow_bef,
      -- Query modified by Gopu to fix the bugzilla issue #2625
      DECODE (TRIM (event_fuzzyafter), '', '', NULL, '', 0, '', event_fuzzyafter
      || ' '
      || DECODE (event_durationafter, 'H', 'Hours', 'D', 'Days', 'W', 'Weeks', 'M', 'Months')
      || '(AFTER)')                                                                                                                AS vwindow_aft,
      DECODE (event_durationbefore, 'D', (fuzzy_period    * 1), 'H', 1, 'W', (fuzzy_period * 7), 'M', (fuzzy_period * 30))         AS fuzzy_duration_bef,
      DECODE (event_durationafter, 'D', (event_fuzzyafter * 1), 'H', 1, 'W', (event_fuzzyafter * 7), 'M', (event_fuzzyafter * 30)) AS fuzzy_duration_aft,
      DECODE (duration, 0, '', duration
      || DECODE (duration_unit, 'D', ' Days', 'W', 'Weeks')) AS event_duration,
      visit_name,
      a.description,
      --KM-#4867
      DECODE(win_before_number, 0,'','','',NULL,'',win_before_number
      || ' '
      || DECODE (win_before_unit, 'D','Day(s)','W','Week(s)','M','Month(s)','Y','Year(s)')
      || '(BEFORE)'
      || DECODE(win_after_number,0,'','','',NULL,'',';')
      || ' ') AS visit_win_before,
      DECODE(win_after_number, 0,'','','',NULL,'',win_after_number
      || ' '
      || DECODE (win_after_unit, 'D','Day(s)','W','Week(s)','M','Month(s)','Y','Year(s)')
      || '(AFTER)') AS visit_win_after,
      DECODE (num_months, 0, '', 'Month '
      || num_months
      || ', ')
      || DECODE (num_weeks, 0, '', 'Week '
      || num_weeks
      || ', ')
      || DECODE (num_days, NULL, '', 'Day '
      || num_days) AS INTERVAL,
      DECODE (insert_after_interval, 0, '', '('
      || insert_after_interval
      || ' '
      || DECODE (Insert_after_interval_unit, 'D', 'Day(s)', 'W', 'Week(s)', 'M', 'Month(s)', 'Y', 'Year(s)'))    AS INTERVAL_after,
      eres.pkg_reschedule.f_get_sch_date(a.fk_protocol,a.pk_protocol_visit,pkg_dateutil.f_get_first_dayofyear()) AS sch_date,
      NAME,
      b.description AS event_desc,
      b.event_id,
      lst_cost (event_id)  AS COST,
      lst_usr (event_id)   AS resources,
      lst_doc (event_id)   AS appendix,
      lst_mail (event_id)  AS messages,
      lst_forms (event_id) AS forms,
      (
      CASE
        WHEN (a.NUM_DAYS                                                   = 0
        OR PKG_RESCHEDULE.f_check_dayZero(b.chain_id, a.PK_PROTOCOL_VISIT) = 1)
        THEN 0
        ELSE a.displacement
      END) AS displacement,
      (SELECT visit_name
      FROM SCH_PROTOCOL_VISIT
      WHERE pk_protocol_visit = a.insert_after
      ) AS insert_after,
      event_sequence,
      event_type,
      b.hide_flag,
      a.PK_PROTOCOL_VISIT
    FROM SCH_PROTOCOL_VISIT a,
      EVENT_ASSOC b ,
      sch_codelst c
    WHERE pk_protocol_visit    = fk_visit
    AND b.FK_CODELST_COVERTYPE = c.pk_codelst (+)
    ) x
  WHERE protocol = a.event_id;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,428,1,'01_ERV_CAL_TEMPLATE.sql',sysdate,'v11 #829');

commit;
/