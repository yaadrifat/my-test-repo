DECLARE 
	table_check number;
	row_check number;
	update_sql clob;
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPFILTERMAP';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_repfiltermap
		WHERE repfiltermap_repcat = 'financial'
		and repfiltermap_seq = 1;
		
		IF (row_check > 0) then
		
			update_sql:=
			'<td><DIV id="studyDIV"><Font class=comments><A href="javascript:openLookup(document.reports,''viewId=6013'||'&'||'form=reports'||'&'||'seperator=,'||'&'||'defaultvalue=[SELSTUDYPK]'||'&'||'keyword=selstudyId|STUDY_NUMBER~paramstudyId|LKP_PK|[VELHIDE]'','''')">Select Study</Font></DIV></td><td><DIV id="studydataDIV"> <input TYPE="text" NAME="selstudyId" readonly value="[SELSTUDYNUMBER]"><input TYPE="hidden" NAME="paramstudyId" value="[SELSTUDYPK]"></DIV></td>';
			
			UPDATE ER_REPFILTERMAP
				SET 
					REPFILTERMAP_COLUMN = update_sql
				WHERE 
					repfiltermap_repcat = 'financial' AND
					repfiltermap_seq = 1;
			
		END IF;
		
	END IF;

	COMMIT;

END;
/

	INSERT INTO track_patches
	VALUES(seq_track_patches.nextval,389,1,'01_er_repfiltermap.sql',sysdate,'v11 #790');

	commit; 
