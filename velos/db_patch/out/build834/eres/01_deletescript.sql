set define off;
delete from ER_BULK_UPLOAD_ERRORLOG where FK_BULK_TEMPLATE_DETAIL in (select PK_BULK_TEMPLATE_DETAIL from ER_BULK_TEMPLATE_DETAIL where FILE_FIELD_NAME in ('Calendar Name','Visit','Event','Patient Study ID'));
/
delete from ER_BULK_TEMPLATE_DETAIL where FILE_FIELD_NAME in ('Calendar Name','Visit','Event','Patient Study ID');
/
delete from  ER_BULK_ENTITY_DETAIL where VELOS_FIELD_NAME in ('Calendar Name','Visit','Event','Patient Study ID');
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,433,1,'01_deletescript.sql',sysdate,'v11 #834');

commit;	
/