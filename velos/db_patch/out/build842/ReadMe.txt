/////*********This readMe is specific to v11.1.0 build #842(compatible with Oracle 12c)**********////
*********************************************************************************************************************************************************************
The enhancement released in this build is:

1.Study Network Enhancements.

NOTE: This enhancement is candidate to v11.1.0. This weekly build needs to be deployed on version 11.1.0.