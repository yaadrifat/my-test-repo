set define off;
Declare
v_column_exists number;
Begin
Select count(*) into v_column_exists
  from user_tab_cols where TABLE_NAME = 'ER_STATUS_HISTORY'
  AND COLUMN_NAME = 'STATUS_PARENTMODPK';
  
  IF(v_column_exists > 0) THEN
  execute immediate 'delete from er_status_history where status_modtable=''er_nwsites'' and STATUS_PARENTMODPK is not null';
  execute immediate 'ALTER TABLE ER_STATUS_HISTORY DROP COLUMN STATUS_PARENTMODPK';
  END IF;
  Select count(*) into v_column_exists
  from user_tab_cols where TABLE_NAME = 'ER_STUDY'
  AND COLUMN_NAME = 'STUDY_NETWORK';
  
  IF(v_column_exists > 0) THEN
  execute immediate 'ALTER TABLE ER_STUDY DROP COLUMN STUDY_NETWORK';
  END IF;
  Commit;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,441,1,'01_DROP_COLUMN.SQL',sysdate,'v11.1.0 #842');
commit;

