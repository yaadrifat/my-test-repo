CREATE OR REPLACE FORCE VIEW "ERES"."ERV_ALL_PATIENT_LABS" ("PATIENT_ID", "PATIENT_STUDY_ID", "LAB_DATE", "LAB_NAME", "RESULT", "LONG_TEST_RESULT", "UNIT", "LLN", "ULN", "ACCESSION_NUM", "TOXICITY", "LAB_STATUS", "RESULT_TYPE", "STUDY_NUMBER", "STUDY_PHASE", "NOTES", "FK_PER", "CREATED_ON", "FK_ACCOUNT", "PTLAB_PAT_NAME", "PTLAB_CATEGORY", "CREATOR", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "RID", "PTLAB_RESPONSE_ID", "FK_STUDY") AS 
  SELECT   per_code AS patient_id,
            patprot_patstdid AS patient_study_id,
            TRUNC (test_date) AS lab_date,
            labtest_name AS lab_name,
            test_result AS result,
            test_result_tx AS long_test_result,
            test_unit AS unit,
            test_lln AS lln,
            test_uln AS uln,
            accession_num,
            er_name || NVL2 (er_name, ' - ', '') || er_grade AS toxicity,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_tststat)
               AS lab_status,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_abflag)
               AS result_type,
            NVL ( (SELECT   study_number
                     FROM   ER_STUDY
                    WHERE   pk_study = pp.fk_study), 'No Study Specified')
               AS study_number,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_stdphase)
               AS study_phase,
            notes,
            a.fk_per,
            a.created_on,
            fk_account,
            (SELECT   PERSON_FNAME || ' ' || PERSON_LNAME
               FROM   PERSON
              WHERE   PK_PERSON = PK_PER)
               PTLAB_PAT_NAME,
            (SELECT   GROUP_NAME
               FROM   ER_LABGROUP
              WHERE   PK_LABGROUP = FK_TESTGROUP)
               PTLAB_CATEGORY,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
            A.LAST_MODIFIED_DATE,
            A.RID,
            PK_PATLABS PTLAB_RESPONSE_ID,
            pp.fk_study
     FROM   ER_PATPROT pp,
            ER_PATLABS a,
            ER_LABTEST,
            ER_PER
    WHERE       patprot_stat = 1
            AND pk_per = pp.fk_per
            AND pp.fk_per = a.fk_per
	    AND pp.PK_PATPROT = a.FK_PATPROT
            AND Pk_labtest = fk_testid
 ;

   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."PATIENT_ID" IS 'Patient code for a facility';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."PATIENT_STUDY_ID" IS 'Patients study id';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."LAB_DATE" IS 'Date of the lab test';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."LAB_NAME" IS 'lab test name';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."RESULT" IS 'Lab result';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."LONG_TEST_RESULT" IS 'Lab test result in LONG format';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."UNIT" IS 'lab test unit';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."LLN" IS 'lab test LLN';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."ULN" IS 'lab test ULN';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."ACCESSION_NUM" IS 'Accession number';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."TOXICITY" IS 'Toxicity linked with the test';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."LAB_STATUS" IS 'lab test status';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."RESULT_TYPE" IS 'lab test result type';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."STUDY_NUMBER" IS 'Study number of the study linked with the lab test (if any)';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."STUDY_PHASE" IS 'Study Phase of the study linked with the lab test (if any)';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."NOTES" IS 'Notes for patient labs';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."FK_PER" IS 'Patient Primary Key';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."CREATED_ON" IS 'The date patient lab test record was created';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."FK_ACCOUNT" IS 'Primary key of account labtest is linked with';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."PTLAB_PAT_NAME" IS 'Patient Name';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."PTLAB_CATEGORY" IS 'Lab test category';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."CREATOR" IS 'The user who created the patient labs record';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."LAST_MODIFIED_BY" IS 'The user who last modified the patient labs record';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."LAST_MODIFIED_DATE" IS 'The date patient lab test record was last modified';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."RID" IS 'Database internal RID (used for audit trail)';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."PTLAB_RESPONSE_ID" IS 'Primary key of er_patlabs';
   COMMENT ON COLUMN "ERES"."ERV_ALL_PATIENT_LABS"."FK_STUDY" IS 'Study PK of the study linked with the lab test (if any)';
   COMMENT ON TABLE "ERES"."ERV_ALL_PATIENT_LABS"  IS 'view for patient labs data';
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,403,1,'01_ERV_ALL_PATIENT_LABS.sql',sysdate,'v11 #804');
commit; 