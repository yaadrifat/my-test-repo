﻿/////*********This readMe is specific to v11.1.0 build #860(compatible with Oracle 12c)**********////
*********************************************************************************************************************************************************************

1. Regular bug fixes.
2. Enhancements released in this build are:

I. User More Identifiers.
	The following pages are covered :
		
	1) Advanced Study Search : PI column of the table
	2) Patient Enrolled Browser : Assigned To, Enrolled By and Physician columns of the table
	3) Calendar >> Event Resource : Select User
	4) Calendar >> >> Event Message : Select User
	5) Form Library >> Form Settings >> View List
	6) Form Library >> Form Settings >> Associated Messages Section >> Form Notification >> Select/Deselect User(s)
	7) Financial Search >> Notification >> Select/Deselect User(s)
	8) Financial Search >> Invoice : Created By column of the table
	9) Budget >> Access Rights
	10) Ad-hoc Query : Created by column of the table
	11) Following places at eCompliance module:-
		i)   Protocols >> Manage Meetings >> PI
		ii)  Protocols >> Manage Meetings >> Reviewer Comments Window >> Principal Investigator
		iii) Reviewer's Area >> Review Window >> Principal Investigator
	
II. ACC-46493 -- UI Performance changes
Points Covered:
a) UI – 2: Add pagination to the Manage >> Organizations page. 


Note: This theme is currently under development, please do not test application by applying this theme.
      

   


