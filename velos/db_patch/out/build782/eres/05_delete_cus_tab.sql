set define off;

DECLARE
table_check number;
row_check number;
BEGIN
SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_OBJECT_SETTINGS';
	IF (table_check > 0) then
		select count(*) INTO row_check FROM ER_OBJECT_SETTINGS where object_type='T' and object_subtype='cus_tab' and object_name='study_tab';
	IF (row_check > 0) then
		delete from  er_object_settings
		where object_type='T' and object_subtype='cus_tab' and object_name='study_tab';
	END IF;
	END IF;
	COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,381,5,'05_delete_cus_tab.sql',sysdate,'v10.1 #782');

commit;
