set define off;

DECLARE 
	table_check number;
	row_check number;
  
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_REPORT';

 
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM ER_REPORT
	    WHERE pk_report=294;

    
		IF (row_check > 0) then
		UPDATE 
		ER_REPORT
    SET 
		REP_COLUMNS = 'Study Number, Enrolling Site, Enrolled On, Patient Study ID, Gender, Marital Status, State, Country, Primary Ethnicity, Primary Race, Survival Status'
		WHERE pk_report=294;
		END IF;
	
	END IF;
	
	COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,381,8,'08_update_er_report.sql',sysdate,'v10.1 #782');

commit;
