set define off;
create or replace
FUNCTION        "F_GETDIS_SITE" (p_disease_site VARCHAR )
RETURN VARCHAR2
AS
v_disease_site VARCHAR2(2000);
v_temp VARCHAR2(2000);
v_param VARCHAR2(2000) := p_disease_site;
v_find INTEGER;
v_retval VARCHAR2(2000);
BEGIN
	 IF p_disease_site IS NULL THEN
	 	RETURN '';
	 END IF;
	LOOP
		v_find := INSTR(v_param,',');
	    IF v_find = 0 THEN
	       SELECT codelst_desc INTO v_disease_site FROM ER_CODELST WHERE pk_codelst = TO_NUMBER(v_param);
		   v_retval := v_retval || v_disease_site || ', ';
		   EXIT;
		ELSE
			v_temp := SUBSTR(v_param,1,v_find-1) ;
			SELECT codelst_desc INTO v_disease_site FROM ER_CODELST WHERE pk_codelst = TO_NUMBER(v_temp);
			v_retval := v_retval || v_disease_site || ', ';
			v_param := SUBSTR(v_param,v_find+1);
		END IF;
	END LOOP;
	v_retval := SUBSTR(v_retval,1,LENGTH(v_retval)-2);
	RETURN v_retval;
END ;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,381,2,'02_F_GETDIS_SITE.sql',sysdate,'v10.1 #782');

commit;
