SET define off;
create or replace TRIGGER "ERES"."ER_NWUSERS_ADDNLROLES_AD0" AFTER DELETE ON ER_NWUSERS_ADDNLROLES        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;


    BEGIN
    
    delete from er_status_history where status_modpk=:old.PK_NWUSERS_ADDROLES and status_modtable='er_nwusers_addnlroles';
    
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
       oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_NWUSERS_ADDNLROLES', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'PK_NWUSERS_ADDROLES', :OLD.PK_NWUSERS_ADDROLES);
       Audit_Trail.column_delete (raid, 'FK_NWUSERS', :OLD.FK_NWUSERS);
       Audit_Trail.column_delete (raid, 'NWU_MEMBERADDLTROLE', :OLD.NWU_MEMBERADDLTROLE);
       Audit_Trail.column_delete (raid, 'NWU_ADTSTATUS', :OLD.NWU_ADTSTATUS);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,394,3,'03_ER_NWUSERS_ADDNLROLES_AD0.sql',sysdate,'v11 #795');

commit;