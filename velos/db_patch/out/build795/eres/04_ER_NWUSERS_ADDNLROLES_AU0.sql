set define off;
create or replace
TRIGGER "ERES"."ER_NWUSERS_ADDNLROLES_AU0" AFTER UPDATE ON ER_NWUSERS_ADDNLROLES FOR EACH ROW
DECLARE
  raid NUMBER(10);
  old_codetype VARCHAR2(200) ;
  new_codetype VARCHAR2(200) ;
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  usr VARCHAR2(100);
BEGIN


SELECT seq_audit.NEXTVAL INTO raid FROM dual;

usr := Getuser(:NEW.last_modified_by);

Audit_Trail.record_transaction   (raid, 'ER_NWUSERS_ADDNLROLES', :OLD.rid, 'U', usr);



IF NVL(:OLD.PK_NWUSERS_ADDROLES,0) !=
     NVL(:NEW.PK_NWUSERS_ADDROLES,0) THEN
     Audit_Trail.column_update
       (raid, 'PK_NWUSERS_ADDROLES',
       :OLD.PK_NWUSERS_ADDROLES, :NEW.PK_NWUSERS_ADDROLES);
END IF;

IF NVL(:OLD.FK_NWUSERS,0) !=
     NVL(:NEW.FK_NWUSERS,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_NWUSERS',
       :OLD.FK_NWUSERS, :NEW.FK_NWUSERS);
END IF;


old_modby := '';
new_modby := '';

IF NVL(:OLD.last_modified_by,0) !=    NVL(:NEW.last_modified_by,0) THEN
   BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.last_modified_by;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_modby := NULL;
  END;

 BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.last_modified_by;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_modby := NULL;
 END;
	     Audit_Trail.column_update     (raid, 'LAST_MODIFIED_BY', old_modby, new_modby);
END IF;

IF NVL(:OLD.last_modified_date,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update     (raid, 'LAST_MODIFIED_DATE',    to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
END IF;

IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN Audit_Trail.column_update    (raid, 'RID',  :OLD.rid, :NEW.rid);
END IF;



--JM: 21Feb2008: added auditing for the five columns below..

IF NVL(:OLD.NWU_MEMBERADDLTROLE,0) !=
     NVL(:NEW.NWU_MEMBERADDLTROLE,0) THEN
     Audit_Trail.column_update
       (raid, 'NWU_MEMBERADDLTROLE',
       :OLD.NWU_MEMBERADDLTROLE, :NEW.NWU_MEMBERADDLTROLE);
END IF;


IF NVL(:OLD.NWU_ADTSTATUS,0) !=
     NVL(:NEW.NWU_ADTSTATUS,0) THEN
     Audit_Trail.column_update
       (raid, 'NWU_ADTSTATUS',
       :OLD.NWU_ADTSTATUS, :NEW.NWU_ADTSTATUS);
END IF;


END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,394,4,'04_ER_NWUSERS_ADDNLROLES_AU0.sql',sysdate,'v11 #795');

commit;