set define off;
DECLARE 
v_column_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*)
  INTO v_column_exists
  FROM ER_REPFILTERMAP,ER_REPFILTER
  WHERE  PK_REPFILTER=FK_REPFILTER
  and REPFILTER_KEYWORD='repOrgId';
  
  IF (v_column_exists > 0) THEN
  update ER_REPFILTER set REPFILTER_COLUMN=
	
			'<td><DIV id="repOrgDIV"><FONT class = "comments"><A href="javascript:openLookup(document.reports,''viewId=6014'||'&'||'form=reports'||'&'||'maxselect=1'||'&'||'seperator=,'||'&'||'keyword=selrepOrgId|site_name~paramrepOrgId|pk_site|[VELHIDE]'','''')">Select Reporting Organization</A> </FONT></DIV> </td>
	       	<td><DIV id=''repOrgdataDIV''><Input TYPE="text" NAME="selrepOrgId" SIZE="20" READONLY VALUE="[SESSSITENAME]">
	       	<Input TYPE="hidden" NAME="paramrepOrgId" VALUE=''[SESSSITEID]''></DIV>
	       	</td>'
			
			where  REPFILTER_KEYWORD='repOrgId';
  END IF;
  
  COMMIT;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,437,2,'02_update_repfilter_repOrg.sql',sysdate,'v11 #838');
/
commit;	
/