set define off;
DECLARE 
	table_check number;
	row_check number;
	update_sql clob;

	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_OBJECT_SETTINGS';
	
	IF (table_check > 0) then
	SELECT count(*)
	INTO row_check
	FROM ER_OBJECT_SETTINGS
	WHERE OBJECT_TYPE = 'T' and OBJECT_SUBTYPE='13' and OBJECT_NAME='study_tab';
	IF (row_check = 0) then
		insert into ER_OBJECT_SETTINGS (PK_OBJECT_SETTINGS,OBJECT_TYPE,OBJECT_SUBTYPE,OBJECT_NAME,OBJECT_SEQUENCE,OBJECT_VISIBLE,OBJECT_DISPLAYTEXT,FK_ACCOUNT,OBJECT_URL)
    values(SEQ_ER_OBJECT_SETTINGS.nextval,'T','13','study_tab',13,1,'Study Network',0,'studyNetworkTab.jsp?srcmenu=tdmenubaritem3&selectedTab=13&fromPage=studyNetwork');
	
	END IF;
	END IF;
	COMMIT;

END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,385,20,'20_study_objsettings.sql',sysdate,'v11 #786');

commit;

