set define off;
declare
v_table_check number;
v_row_check number;
v_count number:=0;
begin

select count(*) into v_table_check from user_tables where table_name='ER_BROWSERCONF';
if v_table_check > 0
then
  select count(*) into v_row_check from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='irbSub') and BROWSERCONF_COLNAME='FK_REVIEW_MEETING';
  if v_row_check > 0
  then
  select browserconf_seq into v_count from er_browserconf where fk_browser = (select pk_browser from er_browser where browser_module='irbSub') and BROWSERCONF_COLNAME='FORMS_LINK';
    if v_count>0
	then
    update er_browserconf set BROWSERCONF_SEQ=BROWSERCONF_SEQ+1 where fk_browser=(select pk_browser from er_browser where browser_module ='irbSub') and BROWSERCONF_SEQ>=v_count;
    update er_browserconf set BROWSERCONF_SEQ=v_count,BROWSERCONF_SETTINGS='{"key":"FK_REVIEW_MEETING", "label":"Meeting Date", "format":"meetingLink", "sortable":true,"resizeable":true,"hideable":true}' where fk_browser=(select pk_browser from er_browser where browser_module ='irbSub') and BROWSERCONF_COLNAME='FK_REVIEW_MEETING';
  end if;
  commit;
  end if;
end if;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,385,11,'11_update_browconf.sql',sysdate,'v11 #786');

commit;