Declare
v_column_exists number;
Begin
Select count(*) into v_column_exists
  from user_tab_cols where TABLE_NAME = 'ER_STATUS_HISTORY'
  AND COLUMN_NAME = 'STATUS_PARENTMODPK';
  
  IF(v_column_exists = 0) THEN
  execute immediate 'ALTER TABLE ER_STATUS_HISTORY ADD STATUS_PARENTMODPK NUMBER(38,0)';
  execute immediate 'COMMENT ON COLUMN ER_STATUS_HISTORY.STATUS_PARENTMODPK IS ''This column stores the module PK of parent for which status history record is added''' ;
  END IF;
  Commit;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,385,18,'18_er_status_history.sql',sysdate,'v11 #786');

commit;