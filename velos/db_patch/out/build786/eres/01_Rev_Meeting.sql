 DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'Meeting_Disabled_Flag'
      and table_name = 'ER_REVIEW_MEETING';

  if (v_column_exists = 0) then
      execute immediate 'alter table ER_REVIEW_MEETING add (Meeting_Disabled_Flag varchar2(20))';
  end if;
  commit;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,385,01,'01_Rev_Meeting.sql',sysdate,'v11 #786');

commit;

