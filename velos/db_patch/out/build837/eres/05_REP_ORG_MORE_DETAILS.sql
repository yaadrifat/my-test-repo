  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ERES"."REP_ORG_MORE_DETAILS" ("PK_MOREDETAILS", "FIELD_NAME", "FIELD_VALUE", "CREATED_ON", "CREATOR", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "RID", "RESPONSE_ID", "SITE_NAME", "FK_ACCOUNT") AS 
  SELECT   pk_moredetails,
            (SELECT   codelst_desc
               FROM   er_codelst
              WHERE   pk_codelst = MD_MODELEMENTPK)
               field_name,
            MD_MODELEMENTDATA AS field_value,
            a.CREATED_ON CREATED_ON,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
            a.LAST_MODIFIED_DATE LAST_MODIFIED_DATE,
            A.RID,
            PK_MOREDETAILS RESPONSE_ID,
            s.site_name,
            s.fk_account AS fk_account
     FROM   er_moredetails a, er_site s
    WHERE   a.MD_MODNAME = 'org' AND fk_modpk = s.pk_site
	/
	INSERT INTO track_patches
VALUES(seq_track_patches.nextval,436,5,'05_REP_ORG_MORE_DETAILS.sql',sysdate,'v11 #837');
/
commit;	