  set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LKPLIB where LKPTYPE_DESC='More spec Details' and LKPTYPE_TYPE ='dyn_a';
  if (v_record_exists = 0) then
INSERT
INTO ER_LKPLIB
  (
    PK_LKPLIB,
    LKPTYPE_NAME,
    LKPTYPE_DESC,
    LKPTYPE_TYPE
      )
  VALUES
  (
    SEQ_ER_LKPLIB.nextval,
    'dynReports',
    'More Spec Details',
    'dyn_a'
  );
end if;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,436,6,'06_inser_erlkplib.sql',sysdate,'v11 #837');
/
commit;	