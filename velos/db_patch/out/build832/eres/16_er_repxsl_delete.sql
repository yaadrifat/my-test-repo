SET DEFINE OFF;

delete from ER_REPXSL where pk_repxsl in (180);

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,431,16,'16_er_repxsl_delete.sql',sysdate,'v11 #832');

commit;
/