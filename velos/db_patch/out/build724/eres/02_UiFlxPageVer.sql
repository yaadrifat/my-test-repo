set define off;
set serveroutput on;

DECLARE
  v_item_exists number := 0;  
BEGIN
select count(*) into v_item_exists from user_tab_cols where TABLE_NAME = 'UI_FLX_PAGEVER'
and COLUMN_NAME = 'PAGE_VERDIFF';
if (v_item_exists = 0) then
  execute immediate 'ALTER TABLE ERES.UI_FLX_PAGEVER ADD (PAGE_VERDIFF CLOB)';
  dbms_output.put_line('Col PAGE_VERDIFF added');
else
  dbms_output.put_line('Col PAGE_VERDIFF already exists');
end if;

END;
/

COMMENT ON COLUMN 
ERES.UI_FLX_PAGEVER.PAGE_VERDIFF IS 
'This column stores the json diff of curent and previous version';

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,323,2,'02_UiFlxPageVer.sql',sysdate,'v9.3.0 #724');

commit;
