set define off;
DECLARE 
	table_check number;
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME ='ER_STATUS_HISTORY';
  
  IF (table_check > 0) then
  
    for i in (select PK_NWUSERS,CREATED_ON,NWU_STATUS FROM ER_NWUSERS order by PK_NWUSERS)
    loop
      Insert into ER_STATUS_HISTORY (PK_STATUS,STATUS_MODPK,STATUS_MODTABLE,FK_CODELST_STAT,STATUS_DATE,RECORD_TYPE,CREATED_ON,STATUS_ISCURRENT) values (SEQ_ER_STATUS_HISTORY.nextval,i.PK_NWUSERS,'er_nwusers',i.NWU_STATUS,i.CREATED_ON,'N',sysdate,1);
    end loop;
  END IF;
commit;  
END;
/
INSERT INTO track_patches
	VALUES(seq_track_patches.nextval,388,4,'04_data_patch_er_status_history.sql',sysdate,'v11 #789');

	commit;
