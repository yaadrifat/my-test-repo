set define off;
DECLARE 
	table_check number;
	v_column_check number;
  v_codelstId number;
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME ='ER_NWUSERS';
  
  SELECT PK_CODELST INTO v_codelstId FROM ER_CODELST WHERE CODELST_TYPE='networkuserstat' AND CODELST_SUBTYP='Active';
  
  IF (table_check > 0) then
  SELECT count(*) into v_column_check FROM user_tab_cols where table_name = 'ER_NWUSERS'  and column_name = 'NWU_STATUS';
        
      if v_column_check > 0 then 
       UPDATE ER_NWUSERS SET NWU_STATUS =v_codelstId;
      end if;
    
  END IF;
commit;   
END;
/

INSERT INTO track_patches
	VALUES(seq_track_patches.nextval,388,3,'03_data_patch_er_nwusers.sql',sysdate,'v11 #789');

	commit; 
