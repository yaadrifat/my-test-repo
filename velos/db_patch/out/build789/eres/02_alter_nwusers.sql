set define off;
DECLARE 
	table_check number;
	v_column_check number;
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME ='ER_NWUSERS';
  
  IF (table_check > 0) then
  SELECT count(*) into v_column_check FROM user_tab_cols where table_name = 'ER_NWUSERS'  and column_name = 'NWU_STATUS';
        
      if v_column_check = 0 then 
       execute immediate 'ALTER TABLE ER_NWUSERS ADD NWU_STATUS NUMBER';
       end if;
    
  END IF;
commit;  
END;
/
COMMENT ON COLUMN "ER_NWUSERS"."NWU_STATUS" IS 'PK CODELST TO NETWORK USERS STATUS(networkuserstat)';

INSERT INTO track_patches
	VALUES(seq_track_patches.nextval,388,2,'02_alter_nwusers.sql',sysdate,'v11 #789');

	commit;
