set define off;

DECLARE
    i INTEGER;
BEGIN
    SELECT COUNT(*) INTO i FROM user_indexes WHERE index_name = 'XPK_FORMSTAT';
    IF i > 0 THEN
        EXECUTE IMMEDIATE 'DROP INDEX XPK_FORMSTAT';
    END IF;    
END;
/
CREATE UNIQUE INDEX "ERES"."XPK_FORMSTAT" ON "ERES"."ER_FORMSTAT" ("PK_FORMSTAT") TABLESPACE "ERES_MED_INDX";

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,374,5,'05_XPK_FORMSTAT.sql',sysdate,'v10 #775');

commit;