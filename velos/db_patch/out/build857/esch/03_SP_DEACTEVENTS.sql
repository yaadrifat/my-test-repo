create or replace PROCEDURE        "SP_DEACTEVENTS" (
p_protocol IN NUMBER,
p_patientid IN NUMBER,
p_date IN DATE,
p_type CHAR :='',
p_userid IN NUMBER
)
/**
Procedure to deactivate the schedule of the patient in case of change to date or protocol
**/
AS
   tab_schEventIds tab_numbers;
BEGIN

  IF (p_type='S') THEN
    select event_id
    bulk collect into tab_schEventIds
    from (select event_id FROM SCH_EVENTS1
    WHERE fk_study= (p_patientid)
    AND SESSION_ID= LPAD(TO_CHAR(p_protocol),10,'0') AND status=0);

    DELETE FROM  SCH_DISPATCHMSG
    WHERE fk_schevent IN ( SELECT * from TABLE (tab_schEventIds));

    UPDATE SCH_EVENTS1 SET status = 5, last_modified_date = SYSDATE,last_modified_by= p_userid WHERE event_id IN (SELECT * from TABLE (tab_schEventIds)) ;

    tab_schEventIds.delete;--VIMP
  ELSE
    select event_id
    bulk collect into tab_schEventIds
    from (select event_id FROM SCH_EVENTS1
    WHERE PATIENT_ID= LPAD(TO_CHAR(p_patientid),10,'0') AND
    SESSION_ID= LPAD(TO_CHAR(p_protocol),10,'0') AND bookedon=p_date AND status=0);

    UPDATE SCH_EVENTS1 SET status = 5, last_modified_date = SYSDATE ,last_modified_by= p_userid WHERE event_id IN (SELECT * from TABLE (tab_schEventIds));

    DELETE FROM  SCH_DISPATCHMSG
    WHERE fk_schevent IN (SELECT * from TABLE (tab_schEventIds)) ;

    tab_schEventIds.delete;--VIMP
  END IF;
COMMIT;
END;
/

 
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,456,3,'03_SP_DEACTEVENTS.sql',sysdate,'v11.1.0 #857');
commit;
/