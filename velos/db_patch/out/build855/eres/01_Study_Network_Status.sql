set define Off;
DECLARE
	V_RECORD_EXISTS NUMBER :=0;  
BEGIN
	SELECT COUNT(*) INTO V_RECORD_EXISTS FROM ER_CODELST WHERE CODELST_TYPE = 'sntwStat' AND CODELST_SUBTYP = 'PEND';
	IF ( V_RECORD_EXISTS = 0 ) THEN
		Insert into ER_CODELST (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATED_ON) 
		values (SEQ_ER_CODELST.NEXTVAL,'sntwStat','PEND','Pending','N',SYSDATE);
	END IF;
commit;
END;
/

INSERT INTO track_patches VALUES(seq_track_patches.nextval,454,1,'01_Study_Network_Status.sql',sysdate,'v11.1.0 #855');

commit;
