create or replace PROCEDURE "SP_AUTOGEN_PATIENT" (p_account number, p_patientid out varchar2 )
as
v_patientid varchar2(100);
v_patnumber varchar2(100)  := null ;
v_sql varchar2(4000);
v_last_id number;
v_Count number;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'genReport', pLEVEL  => Plog.LFATAL);
Begin
    WHILE v_patnumber is null
    LOOP
    select patientid_autogen_sql,(patientid_lastid + 1) as patientid_lastid into v_sql,v_last_id from er_account_settings where fk_account = p_account;
	EXECUTE IMMEDIATE v_sql INTO v_patientid;
	SELECT COUNT(*) INTO V_COUNT FROM ER_PER WHERE PER_CODE = RPAD(v_patientid,6,'0') || v_last_id;
    update er_account_settings set patientid_lastid = v_last_id  where fk_account = p_account;
    if(V_COUNT = 0) then
	execute immediate v_sql into v_patnumber;
	v_patnumber:=RPAD(v_patnumber,6,'0');
    p_patientid := v_patnumber || v_last_id;
	END IF;
	END LOOP;
End ;
/
INSERT INTO track_patches VALUES(seq_track_patches.nextval,454,3,'03_SP_AUTOGEN_PATIENT.sql',sysdate,'v11.1.0 #855');

commit;