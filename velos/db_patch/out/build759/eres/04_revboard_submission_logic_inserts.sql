SET DEFINE OFF;

DECLARE
  v_item_exists number := 0;  
BEGIN
  select count(*) into v_item_exists from ER_REVIEW_BOARD where FK_ACCOUNT = <FK_ACCOUNT> and BOARD_GROUP_ACCESS = rowtocol('select pk_grp from er_grps where fk_account=<FK_ACCOUNT>') and REVIEW_BOARD_NAME='Clinical Research Committee';
  if (v_item_exists = 0) then
    Insert into ER_REVIEW_BOARD (PK_REVIEW_BOARD,REVIEW_BOARD_NAME,REVIEW_BOARD_DESCRIPTION,FK_ACCOUNT,BOARD_GROUP_ACCESS,BOARD_MOM_LOGIC,REVIEW_BOARD_DEFAULT,FK_CODELST_REVBOARD) 
    values (SEQ_ER_REVIEW_BOARD.nextval,'Clinical Research Committee','Clinical Research Committee',<FK_ACCOUNT>,rowtocol('select pk_grp from er_grps where fk_account=<FK_ACCOUNT>'),'select PKG_IRBMOM_LOGIC.f_generateMOM(?,?) from dual',1,null);
    commit;
  end if;
end;
/

DECLARE
  v_item_exists number := 0;  
BEGIN
  select count(*) into v_item_exists from ER_SUBMISSION_LOGIC where FK_REVIEW_BOARD = (select PK_REVIEW_BOARD from ER_REVIEW_BOARD where FK_ACCOUNT=<FK_ACCOUNT> and REVIEW_BOARD_NAME='Clinical Research Committee') and FK_ACCOUNT = <FK_ACCOUNT>;
  if (v_item_exists = 0) then
    Insert into ER_SUBMISSION_LOGIC (PK_LOGIC,FK_REVIEW_BOARD,LOGIC_DESCRIPTION,LOGIC_SQL,LOGIC_SEQUENCE,FK_ACCOUNT) 
    values (SEQ_ER_SUBMISSION_LOGIC.nextval,(select PK_REVIEW_BOARD from ER_REVIEW_BOARD where FK_ACCOUNT=<FK_ACCOUNT> and REVIEW_BOARD_NAME='Clinical Research Committee'),'Clinical Research Committee','select pkg_submission_logic.f_check_irb(:studyId) from dual',1,<FK_ACCOUNT>);
    commit;
  end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,358,4,'04_revboard_submission_logic_inserts.sql',sysdate,'v10 #759');

commit;
