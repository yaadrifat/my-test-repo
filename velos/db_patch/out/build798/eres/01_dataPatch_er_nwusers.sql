set define off;
declare
  v_table_exist number;
begin
  select count(*) INTO v_table_exist from user_tables where table_name='ER_NWUSERS';
  
  if v_table_exist > 0
  then
    UPDATE er_nwusers SET NWU_MEMBERTROLE=(SELECT pk_codelst FROM er_codelst WHERE codelst_type='nwusersrole' and codelst_desc=f_codelst_desc(NWU_MEMBERTROLE));
  end if;
  commit;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,397,1,'01_dataPatch_er_nwusers.sql',sysdate,'v11 #798');

commit; 
