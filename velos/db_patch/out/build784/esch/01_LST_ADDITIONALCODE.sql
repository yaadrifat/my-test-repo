create or replace
FUNCTION  "LST_ADDITIONALCODE" (event number ,p_additional_code_type varchar2 ) return clob is
cursor addl_code
is
select trim('[ ' || (select codelst_desc from er_codelst  where pk_codelst = md_modelementpk) || ' - ' || to_clob(md_modelementdata) || ' ]') event_addlcode
from  er_moredetails
where md_modname = p_additional_code_type  and fk_modpk = event;

addl_lst clob;
begin
 for l_rec in addl_code loop
	addl_lst := l_rec.event_addlcode || ', '|| addl_lst ;
 end loop;
  addl_lst := substr(trim(addl_lst),1,length(trim(addl_lst))-1) ;

 --dbms_output.put_line (substr(addl_lst,1,length(addl_lst)-1) ) ;

return addl_lst ;
end ;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,383,1,'01_LST_ADDITIONALCODE.sql',sysdate,'v10.1 #784');

commit;