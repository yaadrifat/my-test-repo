set define off;
declare
	column_check number;
Begin
	select count(*) into column_check from 
	USER_TAB_COLUMNS where table_name='ER_STUDYSITES' and column_name='LAST_MODIFIED_ON';
	
	if(column_check>0) then
		execute immediate 'alter table er_studysites modify(LAST_MODIFIED_ON default null)';
	End if;
commit;
End;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,383,2,'02_ER_STUDYSITES_MODIFY.sql',sysdate,'v10.1 #784');

commit;
