SET define off;
create or replace
TRIGGER "ERES"."ER_NWSITES_AD0" AFTER DELETE ON ER_NWSITES        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;


    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
       oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_NWSITES', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'PK_NWSITES', :OLD.PK_NWSITES);
       Audit_Trail.column_delete (raid, 'FK_NWSITES', :OLD.FK_NWSITES);
       Audit_Trail.column_delete (raid, 'FK_SITE', :OLD.FK_SITE);
       Audit_Trail.column_delete (raid, 'NW_LEVEL', :OLD.NW_LEVEL);
       Audit_Trail.column_delete (raid, 'NW_MEMBERTYPE', :OLD.NW_MEMBERTYPE);
       Audit_Trail.column_delete (raid, 'FK_NWSITES_MAIN', :OLD.FK_NWSITES_MAIN);
       Audit_Trail.column_delete (raid, 'NW_STATUS', :OLD.NW_STATUS);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,378,05,'05_ER_NWSITES_AD0.sql',sysdate,'v10.1 #779');

commit;
