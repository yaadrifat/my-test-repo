set define off;
create or replace
TRIGGER "ESCH"."SCH_DISPATCHMSG_BI0" BEFORE INSERT ON SCH_DISPATCHMSG        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;


  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'SCH_DISPATCHMSG', erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON', :NEW.CREATED_ON);
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'FK_EVENT', :NEW.FK_EVENT);
       Audit_Trail.column_insert (raid, 'FK_PAT', :NEW.FK_PAT);
       Audit_Trail.column_insert (raid, 'FK_PATPROT', :NEW.FK_PATPROT);
       Audit_Trail.column_insert (raid, 'FK_SCHEVENT', :NEW.FK_SCHEVENT);
       Audit_Trail.column_insert (raid, 'FK_STUDY', :NEW.FK_STUDY);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', :NEW.LAST_MODIFIED_DATE);
       Audit_Trail.column_insert (raid, 'MSG_ATTEMPTS', :NEW.MSG_ATTEMPTS);
       --Audit_Trail.column_insert (raid, 'MSG_CLUBTEXT', :NEW.MSG_CLUBTEXT);
       Audit_Trail.column_insert (raid, 'MSG_CLUBTEXT', SUBSTR(:NEW.MSG_CLUBTEXT,0,3600));
       Audit_Trail.column_insert (raid, 'MSG_CLUBTEXT', SUBSTR(:NEW.MSG_CLUBTEXT,3601,7400));
       Audit_Trail.column_insert (raid, 'MSG_ISCLUBBED', :NEW.MSG_ISCLUBBED);
       Audit_Trail.column_insert (raid, 'MSG_SENDON', :NEW.MSG_SENDON);
       Audit_Trail.column_insert (raid, 'MSG_STATUS', :NEW.MSG_STATUS);
       Audit_Trail.column_insert (raid, 'MSG_SUBJECT', :NEW.MSG_SUBJECT);
       Audit_Trail.column_insert (raid, 'MSG_TEXT', :NEW.MSG_TEXT);
       Audit_Trail.column_insert (raid, 'MSG_TYPE', :NEW.MSG_TYPE);
       Audit_Trail.column_insert (raid, 'PK_MSG', :NEW.PK_MSG);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,366,0,'01_SCH_DISPATCHMSG_BI0.sql',sysdate,'v10 #767');

commit;
