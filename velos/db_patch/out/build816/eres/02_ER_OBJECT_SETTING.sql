set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_OBJECT_SETTINGS where OBJECT_NAME='application_menu' and OBJECT_SUBTYPE='ntw_menu';
  if (v_record_exists = 0) then
INSERT INTO ER_OBJECT_SETTINGS 
		(PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
	VALUES(
	SEQ_ER_OBJECT_SETTINGS.nextval , 'M' , 'ntw_menu' , 'application_menu' , (select max(OBJECT_SEQUENCE)+1 from ER_OBJECT_SETTINGS where OBJECT_NAME='application_menu') , 1 , 'Networks' , 0);
 commit;
  end if;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,415,2,'02_ER_OBJECT_SETTING.sql',sysdate,'v11 #816');
commit; 
/