set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
  from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='irbPend') 
  and BROWSERCONF_COLNAME='STATUS_SUBTYP';
  if (v_record_exists = 0) then
 INSERT INTO er_browserconf 
	(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ)
	VALUES(SEQ_ER_BROWSERCONF.nextval ,(select pk_browser from er_browser where browser_module='irbPend'),'STATUS_SUBTYP',(select max(BROWSERCONF_SEQ)+1 from er_browserconf  where fk_browser=(select pk_browser from er_browser where browser_module='irbPend') 
	));
 commit;
 end if;
 end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,415,3,'03_er_browser_conf.sql',sysdate,'v11 #816');
commit; 
/