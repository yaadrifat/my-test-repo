/////*********This readMe is specific to v9.3.0 build #730 and is for PM Team **********////

This Build#730 is a partial release of Jupiter requirements:
1. Form-22618_NO4 
2. Form-22621_No5

Features which have been implemented in Form-22618_NO4:
	1. The 3 panels of the modal window: Only Right and Left Panels have been implemented.
	2. Top Panel of the Modal Window is partially implemented.
	3. Two buttons(Next  and Prev) have been  implemented to navigate through the search result set from previous screen.
	4. Pat Study form and query data implemented  on model window along with functionality of Prev and Next button. 
	

Features which are not implemented in Form-22618_NO4:
	1. Forms and query of   Form Response ,patient Demographics and Adverse Event  .
	2. Color coded icons with status for total query.
	3. Query Status filter on this form response .
	4. Collapse/expand functionality.
	5. Query/Response window opens for the highlighted field.
	6. Monitor view and coordinator view.

	
Features which have been implemented in Form-22621_No5:
	1. Query alerts with different priorities(P1>P2>�>P6) with their related colord images.
	2. When reviewing a form response, users will see the color coded icon next to each form field to know which fields need attention the most (i.e. which fields  
		have the most critical query alerts).
	3. When user checked on the priority(P1,P2,...P6), say P1(red colord image) then user will be able to view only priority P1 query response, it also work on the 
		combination of query alerts and shows their related response.
	4. When user unchecked the all query alerts, all the related color coded icon next to each form field will be appear.
	5. It works with New Searches, say If we use Study filter named 'xyz' and then we get all its related query alert response, and if select any specific/more than 	
		one alert select an query alert P2/P1+P2..etc. then it will show only their related response. 

Features which are not implemented in Form-22621_No5:

	1. OnMouseover functionality on color coded images .
    2. Alignment work on the P1,P2,P3,P4,P5,P6 color coded images.
	3. Color coded images  and their related functionality are not implemented on mockup2, Form panel monitor view, Form panel coordinator view.
	4. Threshold days and age relation on the behalf of Query Status.
	5. Pagination on the behalf of colord image response.
	
Note:-The Edit Icon will open a model window in case of Patient Study Status tab only.