/////*********This readMe is specific to v11 build #797(compatible with Oracle 12c)**********////
*********************************************************************************************************************************************************************
In build#797 we have released e_sign enhancement.  Now the e_sign functionality works in two modes

Mode 1 : Default behavior.   -- No need to configure any thing.

Mode 2 : New behavior (INF-42646 )
To enable this functionality we need to configure in  the following:

1. Go to eres home  and open "eresearch.xml"
2. go to "<applicationDefaults>" tag and  insert following script.

 <eSignConf>userpxd</eSignConf>