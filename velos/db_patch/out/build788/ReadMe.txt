/////*********This readMe is specific to v11 build #788**********////


*********************************************************************************************************************************************************************
VELOS PM team and MAYO has done gap analysis of Network tab enhancement and came up with  change request and new
implementation. there were around 14 items identified. Check the "ACC-39863 -- Network Tab Nov 12 2017 v0.8.docx" for Changes from Gap Analysis section.  In this build we are releasing 7 items  They are listed below.

1. --1.	The Network screen is very slow on refresh
2. --4.	Updatable (Inline editing) fields not apparent
3. --6.	Delete Organization Access
4. --8.	Person-org screen
5. --10.Updating a non-system user attached to an organization
6. --12.Typo on Network-Organization-User screen
7. --13.User Documents

The remaining items 7 items will be covered in the next build.


1. We have also covered the following item on PM team's request:  "implemented Changes related to iSquare so that iSquare doesn't break with the upgrade."



