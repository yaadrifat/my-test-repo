/////*********This readMe is specific to v9.3.0 build #715**********////

1) Lind FT UI configuration
To turn on Lind FT eIRB mode, add this line to
EIRB_MODE=LIND
from configBundle_custom.properties.

This will hide Reviewer's UI and only show PI's UI in eIRB.

2) -- CSS Chnages --
Few CSS, JSP and JS files has been changed to apply UI look as it was done in LindFT

3} The following Enhancements are partially implemented :
1.S-22623_No1 
2.S-22624_No2, 
3. S-22627_No5, 
4. S-22629_No6, 
5. S-22625_No7, 
6. S-22632_No8

4) One of eResearch Customer has requested a change in email's subject name from "Request for change of password/esign" to "Request for change of password/eSign".
No separate bug was opened for this issue. We have resolved the issue and including the same in v9.3 version.



*********************************************************************************************
