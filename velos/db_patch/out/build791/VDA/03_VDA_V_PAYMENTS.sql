  CREATE OR REPLACE FORCE VIEW "VDA"."VDA_V_PAYMENTS" ("STUDY_NUMBER", "STUDY_TITLE", "MSPAY_DATE", "MSPAY_TYPE", "MSPAY_AMT_RECVD", "MSPAY_DESC", "MSPAY_COMMENTS", "PK_MILEPAYMENT", "CREATOR", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "CREATED_ON", "FK_STUDY", "FK_ACCOUNT", "CREATOR_FK", "LAST_MODIFIED_BY_FK", "REC_INV", "REC_INVNUM", "REC_MILE", "REC_MILENUM", "REC_PT", "REC_PTNUM") AS 
  SELECT STUDY_NUMBER,
    STUDY_TITLE,
    MILEPAYMENT_DATE MSPAY_DATE,
    ERES.F_GET_CODELSTDESC (MILEPAYMENT_TYPE) MSPAY_TYPE,
    MILEPAYMENT_AMT MSPAY_AMT_RECVD,
    MILEPAYMENT_DESC MSPAY_DESC,
    MILEPAYMENT_COMMENTS MSPAY_COMMENTS,
    PK_MILEPAYMENT,
    (SELECT USR_FIRSTNAME
      || ' '
      || USR_LASTNAME
    FROM eres.ER_USER
    WHERE PK_USER = x.CREATOR
    ) CREATOR,
    (SELECT USR_FIRSTNAME
      || ' '
      || USR_LASTNAME
    FROM eres.ER_USER
    WHERE PK_USER = x.LAST_MODIFIED_BY
    ) LAST_MODIFIED_BY,
    x.LAST_MODIFIED_DATE,
    x.CREATED_ON,
    FK_STUDY,
    FK_ACCOUNT,
    x.CREATOR          AS CREATOR_FK,
    x.LAST_MODIFIED_BY AS LAST_MODIFIED_BY_FK,
    NVL(
    (SELECT SUM(NVL(mp_amount,0) + NVL(mp_holdback_amount,0))
    FROM eres.er_milepayment_details
    WHERE fk_milepayment = pk_milepayment
    AND mp_linkto_type   ='P'
    AND MP_LEVEL2_ID     > 0
    ), 0) AS rec_inv,
    eres.pkg_util.f_join_clob(cursor(SELECT DISTINCT inv_number
    FROM eres.er_invoice
    WHERE pk_invoice IN
      (SELECT mp_linkto_id
      FROM eres.er_milepayment_details
      WHERE fk_milepayment = pk_milepayment
      AND mp_linkto_type   ='I'
      )
    ),',') AS rec_invnum,
    NVL(
    (SELECT SUM(NVL(mp_amount,0) + NVL(mp_holdback_amount,0))
    FROM eres.er_milepayment_details
    WHERE fk_milepayment = pk_milepayment
    AND mp_linkto_type   ='P'
    AND MP_LEVEL2_ID     = 0
    ), 0) AS rec_mile,
    eres.pkg_util.f_join_clob(cursor(SELECT DISTINCT DBMS_LOB.SUBSTR(milestone_desc_calculated,4000)
    FROM vda.vda_v_milestones
    WHERE pk_milestone IN
      (SELECT mp_linkto_id
      FROM eres.er_milepayment_details
      WHERE fk_milepayment  = pk_milepayment
      AND mp_linkto_type    ='M'
      AND mp_level1_id      = '0'
      AND NVL(mp_amount,0) != 0
      )
    ),',') AS rec_milenum,
    (SELECT SUM(mp_amount)
    FROM eres.er_milepayment_details
    WHERE fk_milepayment = pk_milepayment
    AND mp_linkto_type   ='P'
    AND mp_amount       != '0'
    AND mp_level2_id     = '0'
    ) AS rec_pt,
    eres.pkg_util.f_join_clob(cursor(SELECT DISTINCT msach_ptstudy_id
    FROM vda.vda_v_milestone_achvd_det
    WHERE pk_mileachieved IN
      (SELECT fk_mileachieved
      FROM eres.er_milepayment_details
      WHERE fk_milepayment = pk_milepayment
      AND mp_linkto_type   ='P'
      AND mp_amount       != '0'
      AND mp_level2_id     = '0'
      )
    ),',') AS rec_ptnum
  FROM eres.ER_MILEPAYMENT x,
    eres.ER_STUDY s
  WHERE PK_STUDY           = FK_STUDY
  AND MILEPAYMENT_DELFLAG <> 'Y';
 

   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."STUDY_NUMBER" IS 'The Study Number of the study the payment  is linked with';
 
   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."STUDY_TITLE" IS 'The Study Title of the study the payment is linked with';
 
   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."MSPAY_DATE" IS 'The payment date';
 
   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."MSPAY_TYPE" IS 'The payment type';
 
   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."MSPAY_AMT_RECVD" IS 'The payment amount received';
 
   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."MSPAY_DESC" IS 'The payment description';
 
   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."MSPAY_COMMENTS" IS 'The payment comments';
 
   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."PK_MILEPAYMENT" IS 'The Primary Key of the payment record';
 
   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."CREATOR" IS 'The user who created the record (Audit)';
 
   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."LAST_MODIFIED_BY" IS 'The user who last modified the record(Audit)';
 
   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."LAST_MODIFIED_DATE" IS 'The date the record was last modified on (Audit)';
 
   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."CREATED_ON" IS 'The user who created the record (Audit)';
 
   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."FK_STUDY" IS 'The Foreign Key to the study milestone is linked with';
 
   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."FK_ACCOUNT" IS 'The account the milestone is linked with';
 
   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."CREATOR_FK" IS 'The Key to identify the Creator of the record.';
 
   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."LAST_MODIFIED_BY_FK" IS 'The Key to identify the Last Modified By of the record.';
 
   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."REC_INV" IS 'Amount Reconciled to Invoices from this payment';
 
   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."REC_INVNUM" IS 'Reconciled to Invoices';
 
   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."REC_MILE" IS 'Amount Reconciled to Milestones from this payment';
 
   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."REC_MILENUM" IS 'Milestones reconciled to';
 
   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."REC_PT" IS 'Amount Reconciled to Invoices from this payment';
 
   COMMENT ON COLUMN "VDA"."VDA_V_PAYMENTS"."REC_PTNUM" IS 'Amount Reconciled with Patients';
 
   COMMENT ON TABLE "VDA"."VDA_V_PAYMENTS"  IS 'This view provides access to the payment information for studies';
 
commit;
/