SET DEFINE OFF;

DECLARE
row_count NUMBER;
v_pkrepfilter number :=0;
v_lkpID number :=0;
v_lkpvwID number :=0;
BEGIN

SELECT COUNT(*) INTO row_count FROM ER_LKPFILTER
  WHERE LKPFILTER_KEY   ='formQueryPats';
  
  IF (row_count  < 1) THEN
    INSERT INTO ER_LKPFILTER 
      (PK_LKPFILTER, LKPFILTER_KEY, LKPFILTER_SQL, LKPFILTER_PARAM_TYPE, 
      LKPFILTER_COMMENT)
      VALUES
      (SEQ_ER_LKPFILTER.NEXTVAL,'formQueryPats'
      ,'fk_account=?1 and upper(CURRENT_STAT) = ''YES'' and EXISTS (SELECT * FROM ER_USERSITE  usr, ER_PATFACILITY fac
      WHERE fk_user =?2 AND usersite_right>=4 AND fac.fk_site = erv_patient_study_status.fk_site_enrolling
      AND fac.patfacility_accessright > 0
      AND ( pkg_user.f_chk_right_for_studysite(erv_patient_study_status.PK_STUDY,?2,fac.fk_site) > 0 ) AND Pk_STUDY IN (?3))'
      ,'NUMBER,NUMBER,NUMBER_LIST'
      ,'Patient look up for Forms Query dashboard');
  END IF;
  
  select distinct PK_REPFILTER into v_pkrepfilter from er_repfilter where REPFILTER_KEYWORD ='patientId';

  SELECT PK_LKPLIB INTO v_lkpID
  FROM ER_LKPLIB 
  WHERE LKPTYPE_NAME = 'dynReports' AND LKPTYPE_DESC = 'Patient Study Status';

  SELECT PK_LKPVIEW INTO v_lkpvwID
  FROM ER_LKPVIEW 
  WHERE LKPVIEW_NAME = 'Patient Study ID' AND FK_LKPLIB = v_lkpID;

  update ER_REPFILTERMAP set REPFILTERMAP_COLUMN ='<td><DIV id="patientDIV"><FONT class = "comments"><A href="javascript:void(0);" onClick="return openLookup(document.dashboardpg,''viewId='||v_lkpvwID||'&'||'form=dashboardpg'||'&'||'seperator=,'||'&'||'defaultvalue='||'&'||'keyword=selpatientId|PATIENT_STUDY_ID~parampatientId|LKP_PK|[VELHIDE]'''||','||'''dfilter=formQueryPats'''||')'||'">Select Patient</A> </FONT></DIV></td>	<td><DIV id="patientdataDIV"><Input TYPE="text" NAME="selpatientId" SIZE="50" READONLY value="[ALL]"><Input TYPE="hidden" NAME="parampatientId" value="[ALL]"></DIV>	</td>'
  where FK_REPFILTER=v_pkrepfilter 
  and REPFILTERMAP_DEPENDON ='studyId'
  and lower(REPFILTERMAP_REPCAT) = lower('form query management');
  
END;
/
COMMIT;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,292,5,'05_hotfix2_formQueryFltr.sql',sysdate,'v9.2.0 #693.02');

commit;
