set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v11 #805' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,404,0,'00_er_version.sql',sysdate,'v11 #805');
commit; 