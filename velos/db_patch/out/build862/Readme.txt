/////*********This readMe is specific to v11.1.0 build #862(compatible with Oracle 12c)**********////
*********************************************************************************************************************************************************************

1. Regular bug fixes.
2. Enhancements released in this build are:

	I. User More Identifiers.
		The following pages are covered :	
		1) eCompliance module
		2) Report Module
	
	II.  Following areas are covered for eResearch new skin(UI changes) for WCG theme:
		1. eSample 
		2. Organization,Account Links,Portal Admin,Groups,User,Network
		3. Form Management
		4. Study
		5. Patient
		6. Budget
		7. Library Module
		8. eSample & Storage
		9. Dashboard
		10. Financials
	
	III. ACC-46493 -- UI Performance changes
		Points Covered:
		1. UI – 3: User Page(Redesign with pagination)(refer to ManageUsers4.png )
		2. UI – 4: Manage Network page should NOT load all organizations when initially loading the page.   
