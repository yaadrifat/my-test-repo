--irbPend
UPDATE er_browserconf
SET BROWSERCONF_SETTINGS='{"key":"ENTERED_BY", "label":"Entered By", "sortable":true, "resizeable":true,"format":"enterbyLink","hideable":true}' 
WHERE FK_BROWSER=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbPend') AND BROWSERCONF_COLNAME='ENTERED_BY';
commit;

/
UPDATE er_browserconf
SET BROWSERCONF_SETTINGS='{"key":"STUDY_PRINV_NAME", "label":"Principal Investigator", "sortable":true, "resizeable":true,"hideable":true,"format":"prinvLink"}' 
WHERE FK_BROWSER=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbPend') AND BROWSERCONF_COLNAME='STUDY_PRINV_NAME';
commit;
/

declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbPend') AND BROWSERCONF_COLNAME='PI_DETAILS';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME) values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbPend'),'PI_DETAILS');
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbPend') AND BROWSERCONF_COLNAME='ENTR_BY_DET';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME) values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbPend'),'ENTR_BY_DET');
end if;
commit;
end;
/
--irbReview
UPDATE er_browserconf
SET BROWSERCONF_SETTINGS='{"key":"STUDY_PRINV_NAME", "label":"Principal Investigator", "sortable":true, "resizeable":true,"format":"prinvLink","hideable":true}' 
WHERE FK_BROWSER=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbReview') AND BROWSERCONF_COLNAME='STUDY_PRINV_NAME';
commit;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbReview') AND BROWSERCONF_COLNAME='PI_DETAILS';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME) values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbReview'),'PI_DETAILS');
end if;
commit;
end;
/
--irbSub
UPDATE er_browserconf
SET BROWSERCONF_SETTINGS='{"key":"STUDY_PRINV_NAME", "label":"Principal Investigator", "sortable":true, "resizeable":true,"hideable":true,"format":"prinvLink"}' 
WHERE FK_BROWSER=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbSub') AND BROWSERCONF_COLNAME='STUDY_PRINV_NAME';
commit;
/
UPDATE er_browserconf
SET BROWSERCONF_SETTINGS='{"key":"ASSIGNED_TO", "label":"Assigned To", "sortable":true, "resizeable":true,"hideable":true,"format":"asgntoLink"}' 
WHERE FK_BROWSER=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbSub') AND BROWSERCONF_COLNAME='ASSIGNED_TO';
commit;
/
UPDATE er_browserconf
SET BROWSERCONF_SETTINGS='{"key":"ENTERED_BY", "label":"Entered By", "sortable":true, "resizeable":true,"format":"enterbyLink","hideable":true}' 
WHERE FK_BROWSER=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbSub') AND BROWSERCONF_COLNAME='ENTERED_BY';
commit;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbSub') AND BROWSERCONF_COLNAME='PI_DETAILS';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME) values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbSub'),'PI_DETAILS');
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbSub') AND BROWSERCONF_COLNAME='ENTR_BY_DET';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME) values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbSub'),'ENTR_BY_DET');
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbSub') AND BROWSERCONF_COLNAME='ASGN_TO_DET';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME) values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbSub'),'ASGN_TO_DET');
end if;
commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,461,10,'10_er_browserconf.sql',sysdate,'v11.1.0 #862');

Commit;



