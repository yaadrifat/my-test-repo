set define off;
DECLARE 
	table_check number;
	update_sql clob;
BEGIN
	SELECT COUNT(*) INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_REPORT';

	IF (table_check > 0) then	
	update_sql:='select distinct
b.study_number,
study_division,
study_pi,
study_coordinator,
nvl((select sum(milestone_amt/DECODE((select MSRUL_PT_COUNT from VDA.VDA_V_MILESTONES b where
c.fk_study = b.fk_study AND b.pk_milestone = c.fk_milestone),-1,1,null,1,0,1,
(select MSRUL_PT_COUNT from VDA.VDA_V_MILESTONES b where
c.fk_study = b.fk_study AND b.pk_milestone = c.fk_milestone))) from VDA.vda_v_milestone_achvd_det c where c.fk_study = a.fk_study and payment_subtype != ''pay''),''0'') as milestone_amt,
nvl((select sum(milestone_holdback) from VDA.vda_v_milestone_achvd_det b where b.fk_study = a.fk_study and payment_subtype != ''pay''),''0'') as holdback_amt,
fk_study,
nvl((select sum(amount_invoiced) from er_invoice_detail where er_invoice_detail.fk_study = a.fk_study and detail_type=''H''),''0'') as inv_amt,
(select sum(NVL(mp_amount,0))+sum(NVL(MP_HOLDBACK_AMOUNT,0)) from er_milepayment_details where fk_mileachieved in (select pk_mileachieved from VDA.vda_v_milestone_achvd_det b where
b.fk_study = a.fk_study and payment_subtype != ''pay'' )
and MP_LINKTO_TYPE=''P'') as rec_amt
,
b.USER_DETAILS,
b.CORD_USER_DETAILS
from VDA.vda_v_milestone_achvd_det a, VDA.vda_v_study_summary b
where payment_subtype != ''pay''
and fk_study = pk_study
AND fk_study IN (:studyId)
AND TRUNC(MSACH_ACH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)'
	
	UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=263;
			
	COMMIT;
	END IF;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,461,7,'07_er_report_update_financial.sql',sysdate,'v11.1.0 #862');

commit;