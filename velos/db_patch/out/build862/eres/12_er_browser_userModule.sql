set define off;
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browser where BROWSER_MODULE='usrSearch';

if v_row_count=0
then
insert into er_browser(PK_BROWSER,BROWSER_MODULE,BROWSER_NAME) values(SEQ_ER_BROWSER.nextval,'usrSearch','Users');
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch') AND BROWSERCONF_COLNAME='GRP_NAME';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL) 
values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch'),'GRP_NAME',1,'{"key":"GRP_NAME", "label":"Group Name",  "resizeable":true,"sortable":true}','Group Name');
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch') AND BROWSERCONF_COLNAME='SITE_NAME';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL) 
values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch'),'SITE_NAME',2,'{"key":"SITE_NAME", "label":"Organization",  "resizeable":true,"sortable":true}','Organization');
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch') AND BROWSERCONF_COLNAME='USER_NAME';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL) 
values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch'),'USER_NAME',3,'{"key":"USER_NAME", "label":"User Name","format":"userLink", "resizeable":true,"sortable":true}','User Name');
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch') AND BROWSERCONF_COLNAME='TYPE';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL) 
values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch'),'TYPE',4,'{"key":"TYPE", "label":"Type",  "resizeable":true,"sortable":true}','Type');
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch') AND BROWSERCONF_COLNAME='JOB_TYPE';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL) 
values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch'),'JOB_TYPE',5,'{"key":"JOB_TYPE", "label":"Job Type",  "resizeable":true,"sortable":true}','Job Type');
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch') AND BROWSERCONF_COLNAME='RESET_PASS';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL) 
values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch'),'RESET_PASS',6,'{"key":"RESET_PASS", "label":"Reset Password","format":"resetPass",  "resizeable":true}','Reset Password');
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch') AND BROWSERCONF_COLNAME='RESET_SESSION';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL) 
values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch'),'RESET_SESSION',7,'{"key":"RESET_SESSION", "label":"Reset User Session","format":"resetUserSession",  "resizeable":true}','Reset User Session');
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch') AND BROWSERCONF_COLNAME='USR_STATUS';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL) 
values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch'),'USR_STATUS',8,'{"key":"USR_STATUS", "label":"User Status", "resizeable":true,"sortable":true}','User Status');
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch') AND BROWSERCONF_COLNAME='DEL_LINK';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL) 
values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch'),'DEL_LINK',9,'{"key":"DEL_LINK","format":"delLink", "label":"Delete"}','Delete');
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch') AND BROWSERCONF_COLNAME='PK_USER';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ) 
values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch'),'PK_USER',10);
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch') AND BROWSERCONF_COLNAME='USER_NAME_MOREID';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ) 
values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch'),'USER_NAME_MOREID',11);
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch') AND BROWSERCONF_COLNAME='USR_LINK';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS) 
values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch'),'USR_LINK',12,'{"key":"USR_LINK","format":"usrLink","label":""}');
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch') AND BROWSERCONF_COLNAME='USR_LOGGEDINFLAG';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ) 
values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch'),'USR_LOGGEDINFLAG',13);
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch') AND BROWSERCONF_COLNAME='PK_USRGRP';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ) 
values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='usrSearch'),'PK_USRGRP',14);
end if;
commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,461,12,'12_er_browser_userModule.sql',sysdate,'v11.1.0 #862');

commit;	
