set define off;
DECLARE 
	table_check number;
BEGIN
	SELECT COUNT(*) INTO table_check FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_REPXSL';

	IF (table_check > 0) then		
		delete  from  er_repxsl where pk_repxsl in (138,143,155,280,1,153,278,263,285,293,97,145,273,274,277,282,284,251,252,253);	
		COMMIT;
	END IF;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,461,9,'09_er_repxsl_delete.sql',sysdate,'v11.1.0 #862');

Commit;