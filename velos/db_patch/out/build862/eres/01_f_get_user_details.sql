set define off;
create or replace function f_get_user_details(userPk number) return varchar2 is
v_user_details varchar2(2500);
begin
	SELECT ER_USER.USR_FIRSTNAME
      || ' '
      ||decode(nvl(USR_MIDNAME,''),'',USR_MIDNAME,USR_MIDNAME||' ')
	  ||ER_USER.USR_LASTNAME
	  ||' '||ADD_EMAIL
	  ||' '||USR_LOGNAME into v_user_details
    FROM eres.ER_USER,eres.ER_ADD
    WHERE PK_USER = userPk
	and FK_PERADD=PK_ADD;
return v_user_details;
EXCEPTION
     WHEN OTHERS THEN
     return '';
END f_get_user_details;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,461,1,'01_f_get_user_details.sql',sysdate,'v11.1.0 #862');

commit;