set define off;
DECLARE
  v_cnt NUMBER;
  countFlag NUMBER(5);
 
 BEGIN
    SELECT COUNT(1) INTO countFlag FROM USER_TABLES WHERE TABLE_NAME='ER_CODELST';
    if (countFlag > 0) then
	
		SELECT COUNT(*)  INTO v_cnt from ER_CODELST  WHERE codelst_type ='org'  AND CODELST_SUBTYP='sitename';
		IF(v_cnt <1) THEN				
			INSERT INTO ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1)				
			VALUES(SEQ_ER_CODELST.NEXTVAL,'org','sitename','Site Alternative Name','N',1,'textarea','');
		END IF;	
		
		SELECT COUNT(*)  INTO v_cnt from ER_CODELST  WHERE codelst_type ='org'  AND CODELST_SUBTYP='drugMnftrsite';
		IF(v_cnt <1) THEN  
			INSERT INTO ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
			VALUES(SEQ_ER_CODELST.NEXTVAL,'org','drugMnftrsite','Manufacturer Site','N',2,'checkbox','{chkArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}, {data: ''option4'', display:''Educational Campus (e.g. elementary or secondary school or a university campus''}, {data: ''option5'', display:''Subject''s Home (list information on city; county; state; or country where the home is located below)''}, {data: ''option6'', display:''Internet Research (list website names below)''}, {data: ''option7'', display:''International Site''}, {data: ''option8'', display:''Other''} ]}');
		END IF;		
		
SELECT COUNT(*)  INTO v_cnt from ER_CODELST  WHERE codelst_type ='org'  AND CODELST_SUBTYP='baysianCmpste';
		IF(v_cnt <1) THEN				
			INSERT INTO ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1)				
			VALUES(SEQ_ER_CODELST.NEXTVAL,'org','baysianCmpste','Bayesian component included in Site','N',3,'dropdown','<SELECT id=''alternateId'' NAME=''alternateId''>
<OPTION value = ''>Select an option</OPTION>
<OPTION value = ''Yes''>Yes</OPTION>
<OPTION value = ''No''>No</OPTION>
</SELECT>');
		END IF;			
end if;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,376,1,'01_er_codelst_insert.sql',sysdate,'v10.1 #777');

commit;

