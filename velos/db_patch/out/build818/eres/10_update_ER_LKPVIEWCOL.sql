SET DEFINE OFF;
declare 
PK_STudyTm NUMBER := 0;
pk_mored number:=0;
RRID NUMBER:=0;

BEGIN

select PK_LKPCOL INTO PK_STudyTm from ER_LKPCOL where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Studyteam Details' and LKPTYPE_TYPE='dyn_s') and LKPCOL_KEYWORD ='PK_STUDYTEAM';

select PK_LKPCOL INTO pk_mored from ER_LKPCOL where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Studyteam Details' and LKPTYPE_TYPE='dyn_s') and LKPCOL_KEYWORD ='PK_MOREDETAILS';

select PK_LKPCOL INTO RRID from ER_LKPCOL where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Studyteam Details' and LKPTYPE_TYPE='dyn_s') and LKPCOL_KEYWORD='RID';

IF PK_STudyTm > 0 THEN
update ER_LKPVIEWCOL
SET LKPVIEW_IS_DISPLAY = 'N' where FK_LKPVIEW=(select PK_LKPVIEW from ER_LKPVIEW where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Studyteam Details' and LKPTYPE_TYPE='dyn_s') ) AND FK_LKPCOL=PK_STudyTm;

END IF;
IF pk_mored > 0 THEN
update ER_LKPVIEWCOL
SET LKPVIEW_IS_DISPLAY = 'N' where FK_LKPVIEW=(select PK_LKPVIEW from ER_LKPVIEW where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Studyteam Details' and LKPTYPE_TYPE='dyn_s') ) AND FK_LKPCOL=pk_mored;
END IF;
IF RRID > 0 THEN
update ER_LKPVIEWCOL
SET LKPVIEW_IS_DISPLAY = 'N' where FK_LKPVIEW=(select PK_LKPVIEW from ER_LKPVIEW where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Studyteam Details' and LKPTYPE_TYPE='dyn_s') ) AND FK_LKPCOL=RRID;
END IF;

COMMIT;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,417,10,'10_update_ER_LKPVIEWCOL.sql',sysdate,'v11 #818');
commit;
/