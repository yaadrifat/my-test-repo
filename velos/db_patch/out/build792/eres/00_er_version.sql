set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v11 #792' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,391,0,'00_er_version.sql',sysdate,'v11 #792');

commit; 

