create or replace TRIGGER ERES."ER_BULK_TEMPLATE_MASTER_AU0"
   AFTER UPDATE
   ON ERES.ER_BULK_TEMPLATE_MASTER    FOR EACH ROW
DECLARE
   raid           NUMBER (10);
   new_usr        VARCHAR2 (100);
   old_usr        VARCHAR2 (100);
   usr            VARCHAR2 (100);
   old_modby      VARCHAR2 (100);
   new_modby      VARCHAR2 (100);
   old_codetype   VARCHAR2 (200);
   new_codetype   VARCHAR2 (200);
BEGIN
   SELECT   seq_audit.NEXTVAL INTO raid FROM DUAL;

   usr := getuser (:NEW.last_modified_by);


      audit_trail.record_transaction (raid,
                                      'ER_BULK_TEMPLATE_MASTER',
                                      :OLD.rid,
                                      'U',
                                      usr);

   IF NVL (:OLD.PK_BULK_TEMPLATE_MASTER, 0) != NVL (:NEW.PK_BULK_TEMPLATE_MASTER, 0)
   THEN
      audit_trail.column_update (raid,
                                 'PK_BULK_TEMPLATE_MASTER',
                                 :OLD.PK_BULK_TEMPLATE_MASTER,
                                 :NEW.PK_BULK_TEMPLATE_MASTER);
   END IF;



   IF NVL (:OLD.FK_BULK_ENTITY_MASTER, 0) != NVL (:NEW.FK_BULK_ENTITY_MASTER, 0)
   THEN
      audit_trail.column_update (raid,
                                 'FK_BULK_ENTITY_MASTER',
                                 :OLD.FK_BULK_ENTITY_MASTER,
                                 :NEW.FK_BULK_ENTITY_MASTER);
   END IF;


   IF NVL (:OLD.TEMPLATE_NAME, ' ') !=
         NVL (:NEW.TEMPLATE_NAME, ' ')
   THEN
      audit_trail.column_update (raid,
                                 'TEMPLATE_NAME',
                                 :OLD.TEMPLATE_NAME,
                                 :NEW.TEMPLATE_NAME);
   END IF;




   IF NVL (:OLD.FK_USER, 0) != NVL (:NEW.FK_USER, 0)
   THEN
      audit_trail.column_update (raid,
                                 'FK_USER',
                                 :OLD.FK_USER,
                                 :NEW.FK_USER);
   END IF;


   IF NVL (:OLD.IP_ADD, ' ') != NVL (:NEW.IP_ADD, ' ')
   THEN
      audit_trail.column_update (raid,
                                 'IP_ADD',
                                 :OLD.IP_ADD,
                                 :NEW.IP_ADD);
   END IF;



   IF NVL (:OLD.LAST_MODIFIED_BY, ' ') !=
         NVL (:NEW.LAST_MODIFIED_BY, ' ')
   THEN
      audit_trail.column_update (raid,
                                 'LAST_MODIFIED_BY',
                                 :OLD.LAST_MODIFIED_BY,
                                 :NEW.LAST_MODIFIED_BY);
   END IF;



   IF NVL (:OLD.RID, 0) != NVL (:NEW.RID, 0)
   THEN
      audit_trail.column_update (raid,
                                 'RID',
                                 :OLD.RID,
                                 :NEW.RID);
   END IF;

   IF NVL (:OLD.CREATOR, 0) != NVL (:NEW.CREATOR, 0)
   THEN
      audit_trail.column_update (raid,
                                 'CREATOR',
                                 :OLD.CREATOR,
                                 :NEW.CREATOR);
   END IF;



   IF NVL (
         :OLD.last_modified_date,
         TO_DATE (pkg_dateutil.f_get_future_null_date_str,
                  pkg_dateutil.f_get_dateformat)
      ) !=
         NVL (
            :NEW.last_modified_date,
            TO_DATE (pkg_dateutil.f_get_future_null_date_str,
                     pkg_dateutil.f_get_dateformat)
         )
   THEN
      audit_trail.column_update (raid,
                                 'LAST_MODIFIED_DATE',
                                 :OLD.last_modified_date,
                                 :NEW.last_modified_date);
   END IF;

   IF NVL (
         :OLD.created_on,
         TO_DATE (pkg_dateutil.f_get_future_null_date_str,
                  pkg_dateutil.f_get_dateformat)
      ) !=
         NVL (
            :NEW.created_on,
            TO_DATE (pkg_dateutil.f_get_future_null_date_str,
                     pkg_dateutil.f_get_dateformat)
         )
   THEN
      audit_trail.column_update (raid,
                                 'CREATED_ON',
                                 :OLD.created_on,
                                 :NEW.created_on);
   END IF;
END;
/
	INSERT INTO track_patches
	VALUES(seq_track_patches.nextval,391,7,'07_ER_BULK_TEMPLATE_MASTER_AU0.sql',sysdate,'v11 #792');

	commit; 
