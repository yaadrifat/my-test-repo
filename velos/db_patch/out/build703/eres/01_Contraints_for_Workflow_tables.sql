SET define OFF;
DECLARE
  v_count NUMBER := 0;
BEGIN


  SELECT COUNT(*)
  INTO v_count
  FROM all_constraints cons
  WHERE cons.table_name    = 'ACTIVITY'
  AND cons.constraint_type = 'P';
  IF(v_count=0) THEN
    EXECUTE immediate 'ALTER TABLE ACTIVITY ADD PRIMARY KEY (PK_ACTIVITY)';
  END IF;

  SELECT COUNT(*)
  INTO v_count
  FROM all_constraints cons
  WHERE cons.table_name    = 'TASK'
  AND cons.constraint_type = 'P';
  IF(v_count               =0) THEN
    EXECUTE immediate 'ALTER TABLE TASK ADD PRIMARY KEY (PK_TASKID)';
  END IF;

  SELECT COUNT(*)
  INTO v_count
  FROM all_constraints cons
  WHERE cons.table_name    = 'WORKFLOW_ACTIVITY'
  AND cons.constraint_type = 'P';
  IF(v_count               =0) THEN
    EXECUTE immediate 'ALTER TABLE WORKFLOW_ACTIVITY ADD PRIMARY KEY (PK_WORKFLOWACTIVITY)';
  END IF;

  SELECT COUNT(*)
  INTO v_count
  FROM all_constraints cons
  WHERE cons.table_name    = 'WORKFLOW_INSTANCE'
  AND cons.constraint_type = 'P';
  IF(v_count               =0) THEN
    EXECUTE immediate 'ALTER TABLE WORKFLOW_INSTANCE ADD PRIMARY KEY (PK_WFINSTANCEID)';
  END IF;

  SELECT COUNT(*)
  INTO v_count
  FROM all_constraints cons
  WHERE cons.table_name    = 'WORKFLOW'
  AND cons.constraint_type = 'P';
  IF(v_count               =0) THEN
    EXECUTE immediate 'ALTER TABLE WORKFLOW ADD PRIMARY KEY (PK_WORKFLOW)';
  END IF;

  SELECT COUNT(*)
  INTO v_count
  FROM all_constraints cons
  WHERE cons.table_name    = 'WFACTIVITY_CONDITION'
  AND cons.constraint_type = 'P';
  IF(v_count               =0) THEN
    EXECUTE immediate 'ALTER TABLE WFACTIVITY_CONDITION ADD PRIMARY KEY (PK_WFACONDITION)';
  END IF;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,302,1,'01_Contraints_for_Workflow_tables.sql',sysdate,'v9.3.0 #703');

commit;

