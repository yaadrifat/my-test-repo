SET DEFINE OFF;

delete from ER_REPXSL where pk_repxsl in (1, 5, 66, 97, 98, 121, 126, 127, 251, 252, 253, 254, 255, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 5010); 

commit;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,455,1,'01_er_repxsl_delete.sql',sysdate,'v11.1.0 #856');

commit;
