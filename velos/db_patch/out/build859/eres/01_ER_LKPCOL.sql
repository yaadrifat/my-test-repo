set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
  from ER_LKPCOL where LKPCOL_TABLE='erv_usrprofile' and LKPCOL_KEYWORD='USRMIDNAME' 
  and FK_LKPLIB =(select PK_LKPVIEW from er_lkpview where LKPVIEW_KEYWORD='userProfile');
  
  if (v_record_exists = 0) then
	INSERT INTO ER_LKPCOL
	(
		PK_LKPCOL,
		FK_LKPLIB,
		LKPCOL_NAME,
		LKPCOL_DISPVAL,
		LKPCOL_TABLE,
		LKPCOL_KEYWORD
	)
	VALUES
	(
		SEQ_ER_LKPCOL.nextval,
		(select PK_LKPLIB from ER_LKPLIB where PK_LKPLIB=(SELECT PK_LKPVIEW FROM er_lkpview WHERE LKPVIEW_KEYWORD='userProfile')),
		'usr_midname',
		'Middle Name',
		'erv_usrprofile',
		'USRMIDNAME'
	);
  commit;
  end if;
  
  Select count(*) into v_record_exists
  from ER_LKPCOL where LKPCOL_TABLE='erv_usrprofile' and LKPCOL_KEYWORD='USRLOGNAME' 
  and FK_LKPLIB =(select PK_LKPVIEW from er_lkpview where LKPVIEW_KEYWORD='userProfile');
  
  if (v_record_exists = 0) then
	INSERT INTO ER_LKPCOL
	(
		PK_LKPCOL,
		FK_LKPLIB,
		LKPCOL_NAME,
		LKPCOL_DISPVAL,
		LKPCOL_TABLE,
		LKPCOL_KEYWORD
	)
	VALUES
	(
		SEQ_ER_LKPCOL.nextval,
		(select PK_LKPLIB from ER_LKPLIB where PK_LKPLIB=(SELECT PK_LKPVIEW FROM er_lkpview WHERE LKPVIEW_KEYWORD='userProfile')),
		'usr_logname',
		'Login Name',
		'erv_usrprofile',
		'USRLOGNAME'
	);
  commit;
  end if;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,458,1,'01_ER_LKPCOL.sql',sysdate,'v11.1.0 #859');
commit;
	