set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LKPCOL where LKPCOL_TABLE='REP_PAT_SPEC_MORE_DETAILS' and LKPCOL_KEYWORD='FIELD_NAME' and FK_LKPLIB =(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Specimen Details' and LKPTYPE_TYPE='dyn_p');
  if (v_record_exists = 0) then

INSERT
INTO ER_LKPCOL
  (
    PK_LKPCOL,
    FK_LKPLIB,
    LKPCOL_NAME,
    LKPCOL_DISPVAL,
    LKPCOL_DATATYPE,
    LKPCOL_LEN,
    LKPCOL_TABLE,
    LKPCOL_KEYWORD
  )
  VALUES
  (
    SEQ_ER_LKPCOL.nextval,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Specimen Details' and LKPTYPE_TYPE='dyn_p'),
    'FIELD_NAME',
    'Field Name',
    'varchar',
    NULL,
    'REP_PAT_SPEC_MORE_DETAILS',
    'FIELD_NAME'
  );
  commit;
  end if;
  
  v_record_exists:= 0;
  
  Select count(*) into v_record_exists
    from ER_LKPCOL where LKPCOL_TABLE='REP_PAT_SPEC_MORE_DETAILS' and LKPCOL_KEYWORD='FIELD_VALUE' and FK_LKPLIB =(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Specimen Details' and LKPTYPE_TYPE='dyn_p');
  if (v_record_exists = 0) then
INSERT
INTO ER_LKPCOL
  (
    PK_LKPCOL,
    FK_LKPLIB,
    LKPCOL_NAME,
    LKPCOL_DISPVAL,
    LKPCOL_DATATYPE,
    LKPCOL_LEN,
    LKPCOL_TABLE,
    LKPCOL_KEYWORD
  )
  VALUES
  (
    SEQ_ER_LKPCOL.nextval,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Specimen Details' and LKPTYPE_TYPE='dyn_p'),
    'FIELD_VALUE',
    'Field Value',
    'varchar',
    NULL,
    'REP_PAT_SPEC_MORE_DETAILS',
    'FIELD_VALUE'
  );
commit;
end if;
  
  v_record_exists:= 0;
  
  Select count(*) into v_record_exists
    from ER_LKPCOL where LKPCOL_TABLE='REP_PAT_SPEC_MORE_DETAILS' and LKPCOL_KEYWORD='CREATED_ON' and FK_LKPLIB =(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Specimen Details' and LKPTYPE_TYPE='dyn_p');
  if (v_record_exists = 0) then
  
INSERT
INTO ER_LKPCOL
  (
    PK_LKPCOL,
    FK_LKPLIB,
    LKPCOL_NAME,
    LKPCOL_DISPVAL,
    LKPCOL_DATATYPE,
    LKPCOL_LEN,
    LKPCOL_TABLE,
    LKPCOL_KEYWORD
  )
  VALUES
  (
    SEQ_ER_LKPCOL.nextval,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Specimen Details' and LKPTYPE_TYPE='dyn_p'),
    'CREATED_ON',
    'Created On',
    'date',
    NULL,
    'REP_PAT_SPEC_MORE_DETAILS',
    'CREATED_ON'
  );
commit;
end if;
  
  v_record_exists:= 0;
  
  Select count(*) into v_record_exists
    from ER_LKPCOL where LKPCOL_TABLE='REP_PAT_SPEC_MORE_DETAILS' and LKPCOL_KEYWORD='CREATOR' and FK_LKPLIB =(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Specimen Details' and LKPTYPE_TYPE='dyn_p');
  if (v_record_exists = 0) then
INSERT
INTO ER_LKPCOL
  (
    PK_LKPCOL,
    FK_LKPLIB,
    LKPCOL_NAME,
    LKPCOL_DISPVAL,
    LKPCOL_DATATYPE,
    LKPCOL_LEN,
    LKPCOL_TABLE,
    LKPCOL_KEYWORD
  )
  VALUES
  (
    SEQ_ER_LKPCOL.nextval,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Specimen Details' and LKPTYPE_TYPE='dyn_p'),
    'CREATOR',
    'Creator',
    'varchar',
    NULL,
    'REP_PAT_SPEC_MORE_DETAILS',
    'CREATOR'
  );
commit; 
end if;

  v_record_exists:= 0;

	Select count(*) into v_record_exists
    from ER_LKPCOL where LKPCOL_TABLE='REP_PAT_SPEC_MORE_DETAILS' and LKPCOL_KEYWORD='LAST_MODIFIED_BY' and FK_LKPLIB =(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Specimen Details' and LKPTYPE_TYPE='dyn_p');
  if (v_record_exists = 0) then
INSERT
INTO ER_LKPCOL
  (
    PK_LKPCOL,
    FK_LKPLIB,
    LKPCOL_NAME,
    LKPCOL_DISPVAL,
    LKPCOL_DATATYPE,
    LKPCOL_LEN,
    LKPCOL_TABLE,
    LKPCOL_KEYWORD
  )
  VALUES
  (
    SEQ_ER_LKPCOL.nextval,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Specimen Details' and LKPTYPE_TYPE='dyn_p'),
    'LAST_MODIFIED_BY',
    'Last Modified By',
    'varchar',
    NULL,
    'REP_PAT_SPEC_MORE_DETAILS',
    'LAST_MODIFIED_BY'
  );
commit;
end if;
	
  v_record_exists:= 0;
	
	Select count(*) into v_record_exists
    from ER_LKPCOL where LKPCOL_TABLE='REP_PAT_SPEC_MORE_DETAILS' and LKPCOL_KEYWORD='LAST_MODIFIED_DATE' and FK_LKPLIB =(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Specimen Details' and LKPTYPE_TYPE='dyn_p');
  if (v_record_exists = 0) then
INSERT
INTO ER_LKPCOL
  (
    PK_LKPCOL,
    FK_LKPLIB,
    LKPCOL_NAME,
    LKPCOL_DISPVAL,
    LKPCOL_DATATYPE,
    LKPCOL_LEN,
    LKPCOL_TABLE,
    LKPCOL_KEYWORD
  )
  VALUES
  (
    SEQ_ER_LKPCOL.nextval,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Specimen Details' and LKPTYPE_TYPE='dyn_p'),
    'LAST_MODIFIED_DATE',
    'Last Modified Date',
    'date',
    NULL,
    'REP_PAT_SPEC_MORE_DETAILS',
    'LAST_MODIFIED_DATE'
  );
commit;
end if;
	
	v_record_exists:= 0;
	
	Select count(*) into v_record_exists
    from ER_LKPCOL where LKPCOL_TABLE='REP_PAT_SPEC_MORE_DETAILS' and LKPCOL_KEYWORD='SPEC_RESPONSE_ID' and FK_LKPLIB =(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Specimen Details' and LKPTYPE_TYPE='dyn_p');
  if (v_record_exists = 0) then
INSERT
INTO ER_LKPCOL
  (
    PK_LKPCOL,
    FK_LKPLIB,
    LKPCOL_NAME,
    LKPCOL_DISPVAL,
    LKPCOL_DATATYPE,
    LKPCOL_LEN,
    LKPCOL_TABLE,
    LKPCOL_KEYWORD
  )
  VALUES
  (
    SEQ_ER_LKPCOL.nextval,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Specimen Details' and LKPTYPE_TYPE='dyn_p'),
    'SPEC_RESPONSE_ID',
    'Response ID',
    'number',
    NULL,
    'REP_PAT_SPEC_MORE_DETAILS',
    'SPEC_RESPONSE_ID'
  );
commit;  
end if;
	
  v_record_exists:= 0;
  
  Select count(*) into v_record_exists
    from ER_LKPCOL where LKPCOL_TABLE='REP_PAT_SPEC_MORE_DETAILS' and LKPCOL_KEYWORD='SPEC_ID' and FK_LKPLIB =(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Specimen Details' and LKPTYPE_TYPE='dyn_p');
  if (v_record_exists = 0) then
INSERT
INTO ER_LKPCOL
  (
    PK_LKPCOL,
    FK_LKPLIB,
    LKPCOL_NAME,
    LKPCOL_DISPVAL,
    LKPCOL_DATATYPE,
    LKPCOL_LEN,
    LKPCOL_TABLE,
    LKPCOL_KEYWORD
  )
  VALUES
  (
    SEQ_ER_LKPCOL.nextval,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Specimen Details' and LKPTYPE_TYPE='dyn_p'),
    'SPEC_ID',
    'SPEC ID',
    'varchar',
    NULL,
    'REP_PAT_SPEC_MORE_DETAILS',
    'SPEC_ID'
  );
commit;  
end if;
  
  v_record_exists:= 0;
  
  Select count(*) into v_record_exists
    from ER_LKPCOL where LKPCOL_TABLE='REP_PAT_SPEC_MORE_DETAILS' and LKPCOL_KEYWORD='LKP_PK' and FK_LKPLIB =(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Specimen Details' and LKPTYPE_TYPE='dyn_p');
  if (v_record_exists = 0) then
INSERT
INTO ER_LKPCOL
  (
    PK_LKPCOL,
    FK_LKPLIB,
    LKPCOL_NAME,
    LKPCOL_DISPVAL,
    LKPCOL_DATATYPE,
    LKPCOL_LEN,
    LKPCOL_TABLE,
    LKPCOL_KEYWORD
  )
  VALUES
  (
    SEQ_ER_LKPCOL.nextval,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Specimen Details' and LKPTYPE_TYPE='dyn_p'),
    'FK_PER',
    'fk_per',
    'number',
    NULL,
    'REP_PAT_SPEC_MORE_DETAILS',
    'LKP_PK'
  );
  commit;
end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,443,3,'03_er_lkpcol.sql',sysdate,'v11.1.0 #844');
commit;
/
	