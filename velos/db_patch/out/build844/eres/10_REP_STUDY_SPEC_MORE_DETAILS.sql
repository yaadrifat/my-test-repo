  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ERES"."REP_STUDY_SPEC_MORE_DETAILS" ("PK_MOREDETAILS", "FIELD_NAME", "FIELD_VALUE", "CREATED_ON", "CREATOR", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "RID", "SPEC_RESPONSE_ID", "SPEC_ID", "FK_STUDY","FK_ACCOUNT") AS 
  SELECT   pk_moredetails,
            (SELECT   codelst_desc
               FROM   er_codelst
              WHERE   pk_codelst = MD_MODELEMENTPK)
               field_name,
            MD_MODELEMENTDATA AS field_value,
            a.CREATED_ON CREATED_ON,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
            a.LAST_MODIFIED_DATE LAST_MODIFIED_DATE,
            A.RID,
            PK_MOREDETAILS SPEC_RESPONSE_ID,
            s.SPEC_ID,
            s.fk_study AS fk_study,
			s.fk_account AS fk_account
     FROM   er_moredetails a, er_specimen s
    WHERE   a.MD_MODNAME = 'specimen' AND fk_modpk = s.pk_specimen;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,443,10,'10_REP_STUDY_SPEC_MORE_DETAILS.sql',sysdate,'v11.1.0 #844');
commit;
/