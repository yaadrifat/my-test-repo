/////*********This readMe is specific to v9.3.0 build #708.01**********////
======================================================================================================
QA has identified one  critical issue(bug not reported yet) in the enhancement  REP-22596. We have fixed this issue and  released them as HOTFIX#1 to Build#708.

This hot fix contains following fix:

1.CCSG datatable 4 report is  not fetching any data when accessed from reporting module under 'data safety monitoring' .

The hot fix contains  1 xml file and 3 db patch files.

1.XML file:
	1. server/eresearch/deploy/velos.ear/velos.war/jsp/xml/releaseinfo.xml
2. DB Script files:
    epat:
        1.01_hotfix1_er_version.sql
    eres:
        2.02_hotfix1_er_version.sql
        3.03_hotfix1_er_repfiltermap.sql	
    esch:
        4.01_hotfix1_er_version.sql        
3. Documents
	One technical document on  HowTo  Configuring Study Types is included along with this HotFix.

Steps to deploy the hot fix:
- Stop the eResearch application.
- Execute the DB patch in the 'db_patch' folder on the database. 
- Replace the JSP and XML files on the application server with the files in the 'server' folder.
- Clear the tmp and work folders under JBoss as usual and start the eResearch application.
======================================================================================================
 