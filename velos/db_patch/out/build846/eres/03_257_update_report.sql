DECLARE 
	table_check number;
	row_check number;
	update_sql clob; 
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report = 257;
		
		IF (row_check > 0) then
			update_sql:='select
study_number,
milestone_type,
msach_milestone_desc,
fk_site_enrolling,
enrolling_site,
CASE WHEN milestone_type=''Patient Milestone'' OR milestone_type=''Visit Milestone'' OR milestone_type=''Event Milestone''
THEN
(DECODE(VDA.vda_v_milestone_achvd_det.CALASSOCTO,''S'',NVL(msach_ptstudy_id,'' ''), NVL(msach_ptstudy_id,''(Patient removed from study)''))) 
ELSE
(DECODE(VDA.vda_v_milestone_achvd_det.CALASSOCTO,''S'',NVL(msach_ptstudy_id,'' ''), NVL(msach_ptstudy_id,''-''))) 
END AS msach_ptstudy_id,
--(DECODE(VDA.vda_v_milestone_achvd_det.CALASSOCTO,''S'',NVL(msach_ptstudy_id,'' ''), NVL(msach_ptstudy_id,''(Patient removed from study)''))) msach_ptstudy_id,
TO_CHAR(msach_ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) msach_ach_date,
(milestone_amt/DECODE((select MSRUL_PT_COUNT from VDA.VDA_V_MILESTONES a where
VDA.vda_v_milestone_achvd_det.fk_study = a.fk_study AND a.pk_milestone = VDA.vda_v_milestone_achvd_det.fk_milestone),-1,1,null,1,
0,1,
(select MSRUL_PT_COUNT from VDA.VDA_V_MILESTONES a where
VDA.vda_v_milestone_achvd_det.fk_study = a.fk_study AND a.pk_milestone = VDA.vda_v_milestone_achvd_det.fk_milestone))) milestone_amt,
milestone_holdback,
payment_type,
payment_for,
fk_study,
fk_milestone,
inv_amt,
inv_num,
rec_amt,
payment_dt
from VDA.vda_v_milestone_achvd_det
where PAYMENT_SUBTYPE != ''pay''
AND fk_study IN (:studyId)
AND TRUNC(MSACH_ACH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';

			UPDATE 
				ER_REPORT 
			SET 
			--	REP_SQL=update_sql,
				REP_SQL_CLOB=update_sql				
			WHERE 				
				pk_report=257;
		END IF;
	END IF;		
	
	COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,445,3,'03_257_update_report.sql',sysdate,'v11.1.0 #846');
commit;