  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ERES"."REP_MILESTONE_ACHVD" ("MSACH_RESPONSE_ID", "MSACH_MILESTONE_DESC", "MSACH_PATIENT_ID", "MSACH_STUDY_NUMBER", "MSACH_PTSTUDY_ID", "MSACH_ACH_DATE", "RID", "CREATOR", "CREATED_ON", "LAST_MODIFIED_DATE", "LAST_MODIFIED_BY", "MSACH_IS_COMPLETE", "FK_STUDY", "FK_ACCOUNT", "EVENTSTAT_NOTES", "DATA_ENTRY_DATE", "PAT_FIRSTNAME", "PAT_LASTNAME", "FK_PER") AS 
  SELECT PK_MILEACHIEVED MSACH_RESPONSE_ID,
       Pkg_Milestone_New.f_getMilestoneDesc(FK_MILESTONE) MSACH_MILESTONE_DESC,
       (SELECT PER_CODE FROM ER_PER WHERE PK_PER = a.FK_PER) MSACH_PATIENT_ID,
       (SELECT STUDY_NUMBER FROM ER_STUDY
        WHERE PK_STUDY = a.FK_STUDY) MSACH_STUDY_NUMBER,
        CASE WHEN MILESTONE_TYPE='PM' OR MILESTONE_TYPE='PM' OR MILESTONE_TYPE='PM' 
        Then
       NVL((SELECT PATPROT_PATSTDID FROM ER_PATPROT pp
        WHERE pp.fk_per = a.fk_per AND pp.fk_study = a.fk_study AND pp.patprot_stat = 1 AND ROWNUM < 2), '(Patient removed from study)')
      ELSE 
     NVL((SELECT PATPROT_PATSTDID FROM ER_PATPROT pp
        WHERE pp.fk_per = a.fk_per AND pp.fk_study = a.fk_study AND pp.patprot_stat = 1 AND ROWNUM < 2), '-') 
        END MSACH_PTSTUDY_ID,
        --NVL((SELECT PATPROT_PATSTDID FROM ER_PATPROT pp
        --WHERE pp.fk_per = a.fk_per AND pp.fk_study = a.fk_study AND pp.patprot_stat = 1 AND ROWNUM < 2), '(Patient removed from study)') MSACH_PTSTUDY_ID,
       trunc(ACH_DATE) MSACH_ACH_DATE,a.RID,
       (SELECT usr_firstname || ' ' || usr_lastname FROM ER_USER
        WHERE pk_user = a.CREATOR) creator,a.CREATED_ON,
       a.LAST_MODIFIED_DATE,
       (SELECT usr_firstname || ' ' || usr_lastname FROM ER_USER
        WHERE pk_user = a.LAST_MODIFIED_BY) LAST_MODIFIED_BY,
       F_GET_YESNO(IS_COMPLETE) MSACH_IS_COMPLETE,a.fk_study,
       (SELECT fk_account FROM ER_STUDY WHERE pk_study = a.fk_study) fk_account,
       (select SCH_EVENTSTAT.EVENTSTAT_NOTES from ESCH.SCH_EVENTSTAT where SCH_EVENTSTAT.PK_EVENTSTAT= (select max(PK_EVENTSTAT) from ESCH.SCH_EVENTSTAT where SCH_EVENTSTAT.FK_EVENT in (select event_id from ESCH.SCH_EVENTS1 where FK_ASSOC = m.FK_EVENTASSOC AND fk_visit = m.FK_VISIT))) EVENTSTAT_NOTES,
    (select SCH_EVENTSTAT.created_on from ESCH.SCH_EVENTSTAT where SCH_EVENTSTAT.PK_EVENTSTAT= (select max(PK_EVENTSTAT) from ESCH.SCH_EVENTSTAT where SCH_EVENTSTAT.FK_EVENT in (select event_id from ESCH.SCH_EVENTS1 where FK_ASSOC = m.FK_EVENTASSOC AND fk_visit = m.FK_VISIT))) DATA_ENTRY_DATE,
    (select PERSON_FNAME from epat.person where pk_person=a.FK_PER) PAT_FIRSTNAME,
	(select PERSON_LNAME from epat.person where pk_person=a.FK_PER) PAT_LASTNAME,
  a.FK_PER
FROM ER_MILEACHIEVED a,ER_MILESTONE m  WHERE m.pk_milestone = fk_milestone
  AND m.fk_codelst_milestone_stat =
    (SELECT pk_codelst
    FROM er_codelst
    WHERE codelst_type ='milestone_stat'
    AND codelst_subtyp ='A'
    ) AND IS_COMPLETE = 1;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,445,4,'04_REP_MILESTONE_ACHVD.sql',sysdate,'v11.1.0 #846');
commit;
