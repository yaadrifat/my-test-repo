set define off;
DECLARE 
	table_check number;
	row_check number;
	update_sql clob;

	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_OBJECT_SETTINGS';
	
	IF (table_check > 0) then
		
		insert into ER_OBJECT_SETTINGS (PK_OBJECT_SETTINGS,OBJECT_TYPE,OBJECT_SUBTYPE,OBJECT_NAME,OBJECT_SEQUENCE,OBJECT_VISIBLE,OBJECT_DISPLAYTEXT,FK_ACCOUNT,OBJECT_URL)
    values(SEQ_ER_OBJECT_SETTINGS.nextval,'T','cus_tab','study_tab',13,1,'Custom Tab_1',0,'studyMilestones.jsp?srcmenu=tdmenubaritem3'||chr(38)||'selectedTab=12');
	END IF;

	COMMIT;

END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,380,9,'09_study_cus_tab.sql',sysdate,'v10.1 #781');

commit;