set define off;

DECLARE 
	table_check number;
	row_check number;
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_OBJECT_SETTINGS';

	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_object_settings
	    where object_name='irb_new_tab' and object_subtype='irb_approv_tab';
		IF (row_check > 0) then	
			UPDATE er_object_settings 
			set object_visible=0 
			where object_name='irb_new_tab' and 
			object_subtype='irb_approv_tab';
    END IF;
	END IF;
COMMIT;	
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,380,4,'04_update_ERObjSettings.sql',sysdate,'v10.1 #781');

commit;
