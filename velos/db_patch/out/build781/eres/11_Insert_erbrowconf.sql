set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='irbPend') 
	and BROWSERCONF_COLNAME='DELETE_LINK';

if (v_record_exists = 0) then

insert into er_browserconf
(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS)
VALUES
(SEQ_ER_BROWSERCONF.nextval, (select pk_browser from er_browser where browser_module = 'irbPend'),'DELETE_LINK',(select max(BROWSERCONF_SEQ)+1 from er_browserconf where fk_browser= (select pk_browser from er_browser where browser_module = 'irbPend')),'{"key":"DELETE_LINK","label":"Delete Letter","format":"deleteLink", "sortable":false, "resizeable":true,"hideable":true}');

commit;

  end if;
end;

/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,380,11,'11_Insert_erbrowconf.sql',sysdate,'v10.1 #781');

commit;