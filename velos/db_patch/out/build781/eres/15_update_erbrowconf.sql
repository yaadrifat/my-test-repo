set define off;
 
DECLARE 
	table_check number;
	row_check number;
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_BROWSERCONF';
	
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM ER_BROWSERCONF
	    WHERE  FK_BROWSER=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbPend') AND BROWSERCONF_COLNAME='SUBMISSION_STATUS_DESC';
		
		IF (row_check > 0) then	
			UPDATE er_browserconf
            SET BROWSERCONF_SETTINGS='{"key":"SUBMISSION_STATUS_DESC", "label":"Submission Status","format":"historyLink", "sortable":true, "resizeable":true,"hideable":true}' 
            WHERE  FK_BROWSER=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbPend') AND BROWSERCONF_COLNAME='SUBMISSION_STATUS_DESC';
		END IF;
	
	END IF;

	COMMIT;
	
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,380,15,'15_update_erbrowconf.sql',sysdate,'v10.1 #781');

commit;