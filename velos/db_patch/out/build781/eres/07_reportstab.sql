set define off;
DECLARE 
	table_check number;
	row_check number;

	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_	OBJECT_SETTINGS';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_object_settings
		WHERE object_name = 'milestone_tab' AND 
			  object_subtype = '8';
		
		IF (row_check = 0) then
			INSERT INTO ER_OBJECT_SETTINGS 
				(PK_OBJECT_SETTINGS, 
				OBJECT_TYPE, 
				OBJECT_SUBTYPE, 
				OBJECT_NAME, 
				OBJECT_SEQUENCE, 
				OBJECT_VISIBLE, 
				OBJECT_DISPLAYTEXT, 
				FK_ACCOUNT)
			VALUES
				(SEQ_ER_OBJECT_SETTINGS.NEXTVAL,
				'T',
				'8',
				'milestone_tab',
				(SELECT MAX(OBJECT_SEQUENCE) FROM ER_OBJECT_SETTINGS WHERE OBJECT_NAME = 'milestone_tab')+1,
				1,
				'Reports',
				0);
		END IF;
		
	END IF;

	COMMIT;

END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,380,7,'07_reportstab.sql',sysdate,'v10.1 #781');

commit;


