create or replace
FUNCTION "F_GETCOLORFLAG" ( from_res number,study_id number,linked_to number)
RETURN VARCHAR
IS
TYPE asc_arr_t IS TABLE OF varchar2(4000)  INDEX BY  varchar(100);
v_retvalues VARCHAR2(4000);
 p_query number;
   p1count number;
    p2count number;
     p3count number;
      p4count number;
      p5count number;

       p6count number;
       temp_p1count number;
    temp_p2count number;
     temp_p3count number;
      temp_p4count number;
      temp_p5count number;

       temp_p6count number;
       v_count number;
       color_flag varchar2(100);
status_arr asc_arr_t;
 query_arr asc_arr_t;
 threshold_days number;
 empty_data varchar2(10);
 pk_querystatus number;
 query_status varchar2(100);
 threshold_count number;
 threshold_day_var varchar2(100);
 CHECK_FLAG NUMBER;
 temp_count number;
 --pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'f_getcolorflag', pLEVEL  => Plog.LFATAL);

 -- Author:
 --Vivek Kumar Jha
BEGIN
p6count:=0;
   p5count:=0;
   p4count:=0;
   p3count:=0;
   p2count:=0;
   p1count:=0;
   temp_p1count :=0;
    temp_p2count :=0;
     temp_p3count :=0;
      temp_p4count :=0;
      temp_p5count :=0;

       temp_p6count :=0;
   empty_data:='';
   --dbms_output.put_line('res_id-'||from_res||' '||'study_id-'||study_id||' linked_to'||linked_to);
   --***************************find threshold of study****************************

  -- select count(*) into threshold_count from (select studyId_id from er_studyid where fk_study=study_id and fk_codelst_idtype =(select pk_codelst from er_codelst where codelst_subtyp='thresh_days'));
   --if threshold_count=0 then
   --empty_data:='empty';  --no data found
  -- else

  -- select studyId_id into  threshold_day_var from er_studyid where fk_study=study_id and fk_codelst_idtype =(select pk_codelst from er_codelst where codelst_subtyp='thresh_days');
   -- select is_number(threshold_day_var) INTO CHECK_FLAG FROM dual;
   -- IF CHECK_FLAG=0 THEN
   --  empty_data:='empty';
    -- Plog.fatal(pCTX,'sTRINGdATE:' || empty_data) ;
   -- ELSE
  --  threshold_days:=TO_NUMBER(threshold_day_var);
    --Plog.fatal(pCTX,'tHRESOLDDATA:' || threshold_days) ;
    --dbms_output.put_line(' CONVERT INTO nUMBER FORM STRING :'||threshold_days );
  --  END IF;
  -- end if;


--Plog.fatal(pCTX,'vvvvvvvvvv:' || from_res||' study:'||study_id||' Threshold_day:'||threshold_days) ;

--********************store query status in collection************************************
/*for x in(select codelst_type,codelst_subtyp,codelst_desc from  er_codelst where codelst_type='query_status' )
loop
status_arr(x.codelst_subtyp):=x.codelst_desc;
End loop;*/
--****************************store query Type in collection****************************
/*for x in(select codelst_type,codelst_subtyp, codelst_desc from  er_codelst where codelst_type='form_query' )
loop
query_arr(x.codelst_subtyp):=x.codelst_desc;
End loop;*/
select count(*) into v_count from (select distinct fk_field from  er_formQuery where fk_querymodule=from_res and querymodule_linkedto=linked_to);
--dbms_output.put_line('temp_count-'||v_count);
if v_count>0 then
for fld in (select distinct fk_field from  er_formQuery where fk_querymodule=from_res and querymodule_linkedto=linked_to)

loop
temp_p1count:=0;
temp_p2count:=0;
temp_p3count:=0;
temp_p4count:=0;
temp_p5count:=0;
temp_p6count:=0;
select count(*) into temp_count from er_formquerystatus f_status inner join ( select pk_formquery, max(pk_formquerystatus) AS formquery_status from er_formquery
  inner join er_formquerystatus frm_status on frm_status.fk_formquery=pk_formquery where fk_querymodule=from_res and fk_field=fld.fk_field and querymodule_linkedto=linked_to group by pk_formquery) inn_table  on inn_table.formquery_status=f_status.pk_formquerystatus;


for p_flag in (select f_status.pk_formquerystatus,inn_table.pk_formquery,f_status.fk_codelst_querystatus, (select codelst_subtyp from er_codelst where pk_codelst=f_status.fk_codelst_querystatus) AS status_subtyp, (select codelst_subtyp from er_codelst where pk_codelst=(SELECT fk_codelst_querytype FROM er_formquerystatus  WHERE pk_formquerystatus=(SELECT MIN(pk_formquerystatus) FROM er_formquerystatus WHERE fk_formquery=inn_table.pk_formquery ))) AS query_subtype from er_formquerystatus f_status inner join ( select pk_formquery, max(pk_formquerystatus) AS formquery_status from er_formquery
  inner join er_formquerystatus frm_status on frm_status.fk_formquery=pk_formquery where fk_querymodule=from_res and fk_field=fld.fk_field and querymodule_linkedto=linked_to group by pk_formquery) inn_table  on inn_table.formquery_status=f_status.pk_formquerystatus)
loop
if p_flag.status_subtyp='resolved' then
temp_p6count :=temp_p6count+1;
 elsif p_flag.query_subtype='normal'  and (p_flag.status_subtyp='open'or p_flag.status_subtyp='re-opened') then
 temp_p4count:=temp_p4count+1;
 elsif  p_flag.query_subtype='high_priority'  and (p_flag.status_subtyp='open' or p_flag.status_subtyp='re-opened') then
  temp_p1count:=temp_p1count+1;
  elsif  p_flag.query_subtype='high_priority'  and p_flag.status_subtyp='mon_rev' then
   temp_p2count:=temp_p2count+1;
   elsif  p_flag.query_subtype='normal'  and p_flag.status_subtyp='mon_rev' then
  temp_p3count:=temp_p3count+1;
  else
  temp_p5count:=temp_p5count+1;
end if;
end loop;
if temp_count=temp_p6count then
p6count:=p6count+1;
elsif temp_p1count>=1  then
p1count:=p1count+1;
elsif temp_p1count>=1  then
p1count:=p1count+1;
elsif temp_p2count>=1 then
p2count:=p2count+1;
elsif temp_p3count>=1 then
p3count:=p3count+1;
elsif temp_p4count>=1 then
p4count:=p4count+1;
elsif temp_p5count>=1 then
p5count:=p5count+1;
end if;
end loop;
--****************************count total query of form response*************************


 -- if v_count>0 then

   if p1count>=1  then
   color_flag:='red';
   elsif p2count>=1 then
 color_flag:='orange';
   elsif p3count>=1 then
   color_flag:='yellow';
   elsif p4count>=1 then
   color_flag:='purple';
    elsif p6count=v_count then
   color_flag:='green';
   elsif p5count>=1 then
   color_flag:=' ';
   end if;
  else
  color_flag:='white';
 end if;
  --dbms_output.put_line('color_flag-'||color_flag);
RETURN color_flag;
END;
/



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,372,2,'02_F_GETCOLORFLAG.sql',sysdate,'v10 #773');

commit;