--------------------------------------------------------
--  DDL for Package PKG_WF_GENERIC_RULES
--------------------------------------------------------

CREATE OR REPLACE PACKAGE "ERES"."PKG_WF_GENERIC_RULES" 
IS
  FUNCTION F_studystat_activity(studyId NUMBER, activity_subtype Varchar2) RETURN NUMBER;

  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_WF_GENERIC_RULES', pLEVEL  => Plog.LFATAL);
  
END PKG_WF_GENERIC_RULES;

/

--------------------------------------------------------
--  DDL for Package PKG_WF_GENERIC
--------------------------------------------------------

CREATE OR REPLACE PACKAGE "ERES"."PKG_WF_GENERIC" 
IS


    PROCEDURE SP_CREATE_NEW_WORKFLOW(
      p_workflow_type VARCHAR2, p_workflow_displayname VARCHAR2,
      p_activity_name        VARCHAR2 ,p_activity_displayname        VARCHAR2 , p_activity_sequence number,
      p_activity_trigger Varchar2,p_activity_trigger_code Varchar2 );

  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_WF_GENERIC', pLEVEL  => Plog.LFATAL);
  
END PKG_WF_GENERIC;

/


--------------------------------------------------------
--  DDL for Package Body PKG_WF_GENERIC_RULES
--------------------------------------------------------

CREATE OR REPLACE PACKAGE BODY "ERES"."PKG_WF_GENERIC_RULES" 
AS
--Sonia Abrol, 26th May 2016 to create generic rules for WF activity


  FUNCTION F_studystat_activity(studyId NUMBER,  activity_subtype Varchar2) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  v_form_id  NUMBER := 0;
  v_studyStatusAvail NUMBER :=0;

  BEGIN
   
    select count(*) into v_studyStatusAvail 
    from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = activity_subtype and codelst_hide='N';

    
    IF v_studyStatusAvail =0 then
      v_RETURN_NO := -1; --Status not exists
    else
    
      SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
      SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = activity_subtype
      and codelst_hide='N' );
    
         IF (v_statusExists > 0) THEN
             v_RETURN_NO := 1; --Done
        ELSE
              v_RETURN_NO := 0; --Not Done
        END IF;
    end if;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    
 END; -- end of function



END PKG_WF_GENERIC_RULES;

/


--------------------------------------------------------
--  DDL for Package Body PKG_WF_GENERIC
--------------------------------------------------------

CREATE OR REPLACE PACKAGE BODY "ERES"."PKG_WF_GENERIC" 
AS
  
  --Sonia Abrol, 26th May 2016 to create generic procs to create new workflows

  PROCEDURE SP_CREATE_NEW_WORKFLOW(
      p_workflow_type VARCHAR2,p_workflow_displayname VARCHAR2,
      p_activity_name        VARCHAR2,p_activity_displayname        VARCHAR2, p_activity_sequence number,
      p_activity_trigger Varchar2, p_activity_trigger_code Varchar2 )
    AS
  v_exists NUMBER := 0;
  v_nextVal NUMBER := 0;
  v_PK_WORKFLOW NUMBER := 0;
  v_PK_ACTIVITY NUMBER := 0;
  v_PK_WORKFLOWACTIVITY NUMBER := 0;
  v_activity_logic Varchar2(4000);
  v_wa_name Varchar2(4000);

  begin


           SELECT count(*) INTO v_exists FROM WORKFLOW WHERE WORKFLOW_TYPE = p_workflow_type;


            IF (v_exists = 0) THEN
                v_PK_WORKFLOW := SQ_WORKFLOW.NEXTVAL();

                    Insert into WORKFLOW (PK_WORKFLOW, WORKFLOW_NAME, WORKFLOW_DESC, WORKFLOW_TYPE)
                    values (v_PK_WORKFLOW,p_workflow_displayname,p_workflow_displayname,p_workflow_type);
            
                  COMMIT;
            
            ELSE
                SELECT PK_WORKFLOW INTO v_PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = p_workflow_type;
            
            END IF;

   --Add Activities

   -------------------------------------------------------------------------------------------------------
   --Adding activity  in the activity and workflow_activity table---
   -------------------------------------------------------------------------------------------------------
   SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = p_activity_name;
  
   IF (v_exists = 0) THEN
       v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();

       Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC)
       values (v_PK_ACTIVITY,p_activity_name,p_activity_displayname);

       COMMIT;

   ELSE
       
       SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = p_activity_name;

   END IF;
  
   IF (v_PK_ACTIVITY > 0) THEN
       --Workflow and activity association
       SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
  
       IF (v_exists = 0) THEN
           v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();

           if p_activity_trigger = 'studystatus' THEN

                v_activity_logic := 'SELECT PKG_WF_GENERIC_RULES.F_studystat_activity(:1,''' || p_activity_trigger_code ||  ''') FROM DUAL';

                v_wa_name := 'select codelst_desc from er_codelst where CODELST_TYPE = ''studystat'' AND CODELST_SUBTYP = ''' || p_activity_trigger_code || '''';

           ELSE
                v_activity_logic := 'select 1 from dual';
                 v_wa_name := p_activity_displayname; 

           end if;

           Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY, WA_NAME, FK_WORKFLOWID, FK_ACTIVITYID, WA_SEQUENCE, WA_TERMINATEFLAG, WA_DISPLAYQUERY)
           values (v_PK_WORKFLOWACTIVITY, v_wa_name, v_PK_WORKFLOW, v_PK_ACTIVITY, p_activity_sequence, 0, 
            v_activity_logic);
           
           COMMIT;
       END IF;
   END IF;


  
    END SP_CREATE_NEW_WORKFLOW;

END PKG_WF_GENERIC;

/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,372,3,'03_new_wf_packages.sql',sysdate,'v10 #773');

commit;