set define off;

update er_codelst set codelst_custom_col1 = '{chkArray:' || codelst_custom_col1 || '}'
where codelst_type = 'studyidtype' and codelst_custom_col in ('checkbox', 'chkbox') 
and codelst_custom_col1 like '[%';

update er_codelst set codelst_custom_col1 = '{chkArray:' || codelst_custom_col1 || '}'
where codelst_type = 'user' and codelst_custom_col in ('checkbox', 'chkbox') 
and codelst_custom_col1 like '[%';

update er_codelst set codelst_custom_col1 = '{chkArray:' || codelst_custom_col1 || '}'
where codelst_type = 'evtaddlcode' and codelst_custom_col in ('checkbox', 'chkbox') 
and codelst_custom_col1 like '[%';

update er_codelst set codelst_custom_col1 = '{chkArray:' || codelst_custom_col1 || '}'
where codelst_type = 'advtype' and codelst_custom_col in ('checkbox', 'chkbox') 
and codelst_custom_col1 like '[%';

update er_codelst set codelst_custom_col1 = '{chkArray:' || codelst_custom_col1 || '}'
where codelst_type = 'peridtype' and codelst_custom_col in ('checkbox', 'chkbox') 
and codelst_custom_col1 like '[%';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,315,6,'06_moreDetailsChkBoxes.sql',sysdate,'v9.3.0 #716');

commit;
