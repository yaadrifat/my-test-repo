set define off;
set serveroutput on;

DECLARE
  v_item_exists number := 0;  
BEGIN
select count(*) into v_item_exists from user_tables where TABLE_NAME = 'UI_FLX_PAGEVER';
if (v_item_exists = 0) then
  execute immediate '
CREATE TABLE ERES.UI_FLX_PAGEVER
(
  PAGE_MOD_TABLE  VARCHAR2(255 BYTE),
  PAGE_MOD_PK     NUMBER,
  PAGE_VER        NUMBER                        DEFAULT 1,
  PAGE_DATE       DATE                          DEFAULT sysdate,
  PAGE_ID         VARCHAR2(255 BYTE),
  PAGE_DEF        CLOB,
  PAGE_JS         CLOB,
  PAGE_JSON       CLOB,
  PAGE_HTML       CLOB
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING  
  ';
  execute immediate '
ALTER TABLE ERES.UI_FLX_PAGEVER ADD (
  CONSTRAINT UI_FLX_PAGEVER_U01
 UNIQUE (PAGE_MOD_TABLE, PAGE_MOD_PK, PAGE_ID, PAGE_VER)
    USING INDEX )
  ';
  dbms_output.put_line('Table UI_FLX_PAGEVER created');
else
  dbms_output.put_line('Table UI_FLX_PAGEVER already exists');
end if;

END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,315,5,'05_CREATE_UI_PAGEVER_TABLE.sql',sysdate,'v9.3.0 #716');

commit;
