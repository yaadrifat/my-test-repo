SET DEFINE OFF
DECLARE
v_cnt NUMBER;
BEGIN

SELECT COUNT(*) INTO v_cnt FROM ER_OBJECT_SETTINGS WHERE OBJECT_SUBTYPE='new_dash_menu' AND OBJECT_NAME='report_menu';

IF(v_cnt<1) THEN

INSERT
INTO ER_OBJECT_SETTINGS
  (
    PK_OBJECT_SETTINGS,
    OBJECT_TYPE,
    OBJECT_SUBTYPE,
    OBJECT_NAME,
    OBJECT_SEQUENCE,
    OBJECT_VISIBLE,
    OBJECT_DISPLAYTEXT,
    FK_ACCOUNT
  )
  VALUES
  (
    SEQ_ER_OBJECT_SETTINGS.nextval,
    'M',
    'new_dash_menu',
    'report_menu',
    16,1,
    'New Dashboards',
    0
  );
  Commit;
END IF;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,315,2,'02_er_object_settings.sql',sysdate,'v9.3.0 #716');

commit;
