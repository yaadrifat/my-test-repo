create or replace TRIGGER "ERES"."ER_SUBMISSION_STATUS_AI0" 
AFTER INSERT
ON ERES.ER_SUBMISSION_STATUS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
      WHEN (
NEW.FK_SUBMISSION_BOARD is null and NEW.SUBMISSION_STATUS is not null
      ) DECLARE
v_stat NUMBER(10);

o_success NUMBER;
v_stattype number;
v_subm_type number;
v_sub_codelst number;
v_study_stat_subtyp varchar2(20);
v_stattype_subtype varchar2(20);
v_site number;

BEGIN


--insert a default status for a new study

select pk_codelst into v_subm_type from er_codelst where CODELST_TYPE='submission' and codelst_subtyp ='new_app';

Select submission_type into v_sub_codelst from er_submission where pk_submission = :NEW.FK_SUBMISSION;
 if(v_subm_type = v_sub_codelst) then

 
    SELECT nvl(codelst_custom_col,'')
     INTO v_study_stat_subtyp
     FROM ER_CODELST
    WHERE pk_codelst = :NEW.SUBMISSION_STATUS;


        if nvl(length(v_study_stat_subtyp),0) > 0      then

            begin

                    select pk_codelst,pkg_util.f_getvaluefromdelimitedstring (',' || codelst_custom_col1 || ',',    1,    ','   )
                    into v_stat, v_stattype_subtype
                    from er_codelst
                    where codelst_type='studystat' and codelst_subtyp = v_study_stat_subtyp;


                        begin
                            SELECT pk_codelst INTO v_stattype
                             FROM ER_CODELST
                            WHERE codelst_type='studystat_type'
                            AND codelst_subtyp = v_stattype_subtype;

                        exception when no_data_found then
                            v_stattype := null;
                        end;

            exception when no_data_found then
                v_stat := 0;
            end;


        end if;
end if;

    for i in (select st.fk_site,st.fk_study from er_studysites st ,er_submission s where pk_submission = :new.fk_submission and s.fk_study = st.fk_study )
    loop

      v_site := i.fk_site;

-- current_stat is added by Manimaran for September Enhancement S8.
				update er_studystat
				set current_stat = 0
				where fk_study =  i.fk_study and fk_site = v_site and current_stat = 1 ;


                INSERT INTO ER_STUDYSTAT
                (pk_studystat,fk_user_docby,
                fk_codelst_studystat,
                fk_study,
                fk_site,
                studystat_date,
                creator,
                created_on,
                ip_add,
                current_stat,status_type)
                VALUES
                (seq_er_studystat.NEXTVAL, :NEW.creator,
                v_stat,
                i.fk_study,
                 v_site,
                 TO_DATE(TO_CHAR(SYSDATE,'mm/dd/yyyy'),'mm/dd/yyyy')
                ,
                :NEW.creator,
                SYSDATE,
                :NEW.ip_add, 1,v_stattype);

    end loop;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,405,14,'14_ER_SUBMISSION_STATUS_AI0.sql',sysdate,'v11 #806');
commit; 