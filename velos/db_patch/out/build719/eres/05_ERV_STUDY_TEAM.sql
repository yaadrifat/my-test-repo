CREATE OR REPLACE FORCE VIEW "ERES"."ERV_STUDY_TEAM" ("STUDY_NUMBER", "STUDY_TITLE", "ORGANIZATION", "USER_NAME", "USER_PHONE", "USER_EMAIL", "USER_ADDRESS", "ROLE", "FK_STUDY", "CREATED_ON", "FK_ACCOUNT", "USR_PARENTCHILD", "STEAM_TEAM_RTS", "STEAM_SUPER_USER", "CREATOR", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "RID", "STEAM_RESPONSE_ID", "STUDYTEAM_STATUS")
AS
  SELECT study_number,
    study_title,
    (SELECT site_name FROM ER_SITE WHERE pk_site = fk_siteid
    ) ORGANIZATION,
    (SELECT usr_firstname
      || ' '
      || usr_lastname
    FROM ER_USER
    WHERE pk_user = fk_user
    ) user_name,
    add_phone user_phone,
    add_email user_email,
    address
    || nvl2(address,nvl2(add_city,', ',''),'')
    || add_city
    || nvl2(add_city,nvl2(add_state,', ',''),'')
    || add_state
    || nvl2(add_state,nvl2(add_zipcode,' - ',''),'')
    || add_zipcode AS user_address,
    F_GET_CODELSTDESC(fk_codelst_tmrole) ROLE,
    fk_study,
    ER_STUDYTEAM.CREATED_ON,
    ER_USER.fk_account,
    STUDY_SITEFLAG USR_PARENTCHILD,
    STUDY_TEAM_RIGHTS STEAM_TEAM_RTS,
    STUDY_TEAM_USR_TYPE STEAM_SUPER_USER,
    (SELECT USR_FIRSTNAME
      || ' '
      || USR_LASTNAME
    FROM ER_USER
    WHERE PK_USER = ER_STUDYTEAM.CREATOR
    ) CREATOR,
    (SELECT USR_FIRSTNAME
      || ' '
      || USR_LASTNAME
    FROM ER_USER
    WHERE PK_USER = ER_STUDYTEAM.LAST_MODIFIED_BY
    ) LAST_MODIFIED_BY,
    ER_STUDYTEAM.LAST_MODIFIED_DATE,
    ER_STUDYTEAM.RID,
    PK_STUDYTEAM STEAM_RESPONSE_ID,
    STUDYTEAM_STATUS
  FROM ER_STUDY,
    ER_STUDYTEAM,
    ER_USER,
    ER_ADD
  WHERE ER_STUDY.pk_study = fk_study
  AND pk_user             = fk_user
  AND pk_add              = fk_peradd
    -- AND study_team_usr_type = 'D'
    ;
/	


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,318,5,'05_ERV_STUDY_TEAM.sql',sysdate,'v9.3.0 #719');

commit;