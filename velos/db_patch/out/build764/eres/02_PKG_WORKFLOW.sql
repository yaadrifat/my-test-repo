create or replace PACKAGE "PKG_WORKFLOW"
IS
  PROCEDURE SP_UPDATE_WORKFLOW(
      p_entity_id     NUMBER,
      p_workflow_type VARCHAR2,
      p_userid        NUMBER,
      pendingtasks OUT NUMBER );
   PROCEDURE SP_UPDATE_WORKFLOW(
      p_entity_id     NUMBER,
      p_workflow_type VARCHAR2,
      p_args ARRAY_STRING,
      p_userid        NUMBER,
      pendingtasks OUT NUMBER );
      pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_WORKFLOW', pLEVEL  => Plog.LFATAL);
END PKG_WORKFLOW;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,363,2,'02_PKG_WORKFLOW.sql',sysdate,'v10 #764');

commit;
