DECLARE 
  index_count INTEGER; 
BEGIN 
  SELECT COUNT(*) into index_count FROM user_indexes WHERE
    index_name = 'IDX_FORMSLINEAR_PATPROT' and upper(table_owner) = 'ERES';
  if(index_count = 1) then
      execute immediate 'DROP INDEX ERES.IDX_FORMSLINEAR_PATPROT';
  END IF;
  SELECT COUNT(*) into index_count FROM user_indexes WHERE
    index_name = 'IDX_FORMSLINEAR_PATPROT' and upper(table_owner) = 'ERES';
  if (index_count = 0) then
    execute immediate 'CREATE INDEX ERES.IDX_FORMSLINEAR_PATPROT ON ERES.ER_FORMSLINEAR
      (FK_PATPROT,1)
       LOGGING
       NOPARALLEL';
  	dbms_output.put_line ('Index created IDX_FORMSLINEAR_PATPROT');
	ELSE
		dbms_output.put_line ('Index not created IDX_FORMSLINEAR_PATPROT');
	END IF;

END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,341,3,'03_ER_formsliner_patprot.sql',sysdate,'v9.3.0 #742');

commit;