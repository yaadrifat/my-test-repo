DECLARE
  v_table_exists NUMBER;
BEGIN
  SELECT COUNT(*)
  INTO v_table_exists
  FROM user_tables
  WHERE table_name  ='ER_BROWSERCONF';
  IF v_table_exists > 0 THEN
    UPDATE er_browserconf
    SET BROWSERCONF_SEQ= DECODE(BROWSERCONF_COLNAME,'PERSON_CODE',1,'PAT_STUDYID',2,'STUDY_NUMBER',3,'SITE_NAME',4,'PROTOCOL_NAME',5,'VISIT_NAME',6,'EVENT_SCHDATE_DATESORT',7,'EVENT_NAME',8,'EVENT_STATUS_DESC',9,'START_DATE_TIME',10,'STORAGE_KIT',11,'PREPARED_SAMPLE',12,'FK_STUDY',13,'FK_SITE',14,'FK_SCH_EVENTS1',15,'PK_STORAGE',16,'FK_VISIT',17,'FK_PER',18,'CHECK_DATA',19)
    WHERE fk_browser   =
      (SELECT pk_browser FROM er_browser WHERE browser_module='preparea'
      );
    commit;
  END IF;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,444,1,'01_update_prepAreaSeq',sysdate,'v11.1.0 #845');

commit;	
/
