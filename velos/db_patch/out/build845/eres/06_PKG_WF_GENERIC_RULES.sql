set define off;
create or replace PACKAGE BODY        "PKG_WF_GENERIC_RULES"
AS
--Sonia Abrol, 26th May 2016 to create generic rules for WF activity
--version 1.4


  FUNCTION F_studystat_activity(studyId NUMBER,  activity_subtype Varchar2) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  v_form_id  NUMBER := 0;
  v_studyStatusAvail NUMBER :=0;

  BEGIN

    select count(*) into v_studyStatusAvail
    from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = activity_subtype and codelst_hide='N';


    IF v_studyStatusAvail =0 then
      v_RETURN_NO := -1; --Status not exists
    else

      SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
      SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = activity_subtype
      and codelst_hide='N' );

         IF (v_statusExists > 0) THEN
             v_RETURN_NO := 1; --Done
        ELSE
              v_RETURN_NO := 0; --Not Done
        END IF;
    end if;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;

 END; -- end of function

--Sonia Abrol, 22 June 2016 to create generic study form status based rule for WF activity

FUNCTION F_study_form_activity(studyId NUMBER, formId Number) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  v_comp_formstat_pk number ;
 BEGIN

  begin

    select F_study_form_activity_anystat(studyId , formId , 'complete')
    into v_RETURN_NO
    from dual;

   exception when no_Data_found then
      v_RETURN_NO:= -1;
    end;


    RETURN v_RETURN_NO;
END;


--Sonia Abrol, 15 July 2016 to create generic study form status based rule for WF activity. This will check for the passed
-- status code subtype

FUNCTION F_study_form_activity_anystat(studyId NUMBER, formId Number, responseStatusSubType Varchar2) RETURN NUMBER
 AS
  v_statusExists NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  v_currentStatus number :=0;
  v_lockdown number := 0;
  v_prev_stat_desc varchar2(100):='';
  v_response_exists number;
 BEGIN
 select count(*) into v_response_exists from er_studyforms where fk_study=studyId and FK_FORMLIB=formId and RECORD_TYPE <>'D';
 if v_response_exists != 0 then

	select FORM_COMPLETED into v_currentStatus FROM ER_STUDYFORMS
    WHERE FK_STUDY = studyId
    AND FK_FORMLIB = formId
	AND RECORD_TYPE <>'D';

	select pk_codelst into v_lockdown from er_codelst where codelst_type='fillformstat' and codelst_subtyp='lockdown';
	dbms_output.put_line('lockdown::::::'||v_lockdown);
	if v_currentStatus=v_lockdown then
	dbms_output.put_line('Inside If lockdown');
	--Checking for previous status of locked forms
		select FA_OLDVALUE into v_prev_stat_desc from ER_FORMAUDITCOL where PK_FORMAUDITCOL= (select max(PK_FORMAUDITCOL) from ER_FORMAUDITCOL
		where FK_FORM=formId and FA_SYSTEMID='Form Status' and FA_NEWVALUE=(select codelst_desc from er_codelst where pk_codelst=v_lockdown)
		and FK_FILLEDFORM=(select distinct(pk_studyforms) from er_studyforms where fk_study=studyId
        and FK_FORMLIB=formId and RECORD_TYPE <>'D'));

		dbms_output.put_line('v_prev_stat_desc:::::'||v_prev_stat_desc);
		select 1 into v_statusExists from er_codelst where codelst_desc= v_prev_stat_desc and codelst_type = 'fillformstat' and
		codelst_subtyp=responseStatusSubType and codelst_hide='N';

	else
		SELECT count(*) INTO v_statusExists FROM ER_STUDYFORMS
		WHERE FK_STUDY = studyId
		AND FK_FORMLIB = formId
		AND FORM_COMPLETED in (select pk_codelst from er_Codelst where codelst_type = 'fillformstat' and
				 INSTR('*'||responseStatusSubType ||'*', '*'|| er_Codelst.CODELST_SUBTYP || '*') > 0 )
		AND RECORD_TYPE <>'D';
	end if;
 else
     SELECT count(*) INTO v_statusExists FROM ER_STUDYFORMS
		WHERE FK_STUDY = studyId
		AND FK_FORMLIB = formId
		AND FORM_COMPLETED in (select pk_codelst from er_Codelst where codelst_type = 'fillformstat' and
				 INSTR('*'||responseStatusSubType ||'*', '*'|| er_Codelst.CODELST_SUBTYP || '*') > 0 )
		AND RECORD_TYPE <>'D';
 end if;
    IF v_statusExists > 0 THEN
      v_RETURN_NO := 1; --Done
    END IF;




    RETURN v_RETURN_NO;
END;


--------------

--Sonia Abrol, 15 July 2016 to create generic study form status based rule for WF activity. This will check for the passed
-- status code subtype

 FUNCTION F_study_document_activity(studyId NUMBER,  docCategories Varchar2) RETURN NUMBER
 AS
  v_doc_count NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
 BEGIN

  select count(*)
    into v_doc_count
    from er_studyver v,er_studyapndx x
    where v.fk_study = studyId and
    v.studyver_category in (select pk_codelst from er_codelst where codelst_type = 'studyvercat'
    and  INSTR('*'||docCategories ||'*', '*'|| er_Codelst.CODELST_SUBTYP || '*') > 0 ) and
     x.fk_studyver = pk_studyver and studyapndx_type = 'file'   ;

    IF v_doc_count > 0 THEN
      v_RETURN_NO := 1; --Done
    END IF;


    RETURN v_RETURN_NO;
END;



--------------


--Sonia Abrol, 22 June 2016 to create generic  more study details based rule for WF activity

FUNCTION F_MSD_activity(studyId NUMBER, fieldCodeSubtype String, fieldValue String) RETURN NUMBER
  AS

  v_RETURN_NO NUMBER := 0;
  v_msd_codepk number;
  v_msd_value varchar2(4000) := '';
  v_value_found number :=0;
  v_fieldValue varchar2(4000) := to_char(fieldValue);

 BEGIN

  begin

    select f_codelst_id('studyidtype',fieldCodeSubtype)
    into v_msd_codepk
    from dual;

  exception when no_Data_found then
      v_msd_codepk:= 0;
    end;

  if v_msd_codepk > 0 then

    begin
      select studyid_id
      into v_msd_value
      from er_studyid
      where fk_study = studyId and  fk_codelst_idtype = v_msd_codepk;

   exception when no_Data_found then
		v_msd_value:= '#';

    end;

    begin
	/* Added condition to handle for cases when v_msd_value has single apostrophe symbol on it */
	if instr(v_fieldValue,'''')>0 then
		select v_fieldValue into v_fieldValue from dual;
	end if;

	dbms_output.put_line('v_msd_value::'||v_msd_value);
     -- check if the value passed matches the study MSD value
     if INSTR(v_msd_value,',') =0 then
      select INSTR('*'||v_fieldValue||'*', '*'|| v_msd_value || '*')
      into v_value_found
      from dual;
    else
      select INSTR(v_msd_value,v_fieldValue)
      into v_value_found
      from dual;
    end if;

	if instr(v_fieldValue,'notnull')>0 and instr(v_msd_value,'#')<=0 then
		dbms_output.put_line('Value exists for category');
		v_value_found:=1;
	end if;

    exception when no_Data_found then
      v_value_found:= 0;
    end;

  end if; -- msd code pk
  dbms_output.put_line('v_value_found-->'||v_value_found);
    IF v_value_found > 0 THEN
      v_RETURN_NO := 1; --Done
    END IF;

    RETURN v_RETURN_NO;
END;

function F_2_combination_activity(studyId NUMBER,  fieldtype1 Varchar2, fieldCodeSubtype1 Varchar2, fieldValue1 Varchar2,
 fieldtype2 Varchar2, fieldCodeSubtype2 Varchar2, fieldValue2 Varchar2) return number
 as
 v_condition1 number := 0;
 v_condition2 number := 0;
  v_return number := 0;

begin
    if fieldtype1 = 'studyMSD' then
      select F_MSD_activity(studyId , fieldCodeSubtype1 , fieldValue1)
      into v_condition1
      from dual;

    end if;

    if fieldtype2 = 'studyMSD' then
      select F_MSD_activity(studyId , fieldCodeSubtype2 , fieldValue2)
      into v_condition2
      from dual;

    end if;

    if fieldtype1 = 'studyform' then
      select F_study_form_activity(studyId , fieldCodeSubtype1)
      into v_condition1
      from dual;

    end if;

    if fieldtype2 = 'studyform' then
      select F_study_form_activity(studyId , fieldCodeSubtype2)
      into v_condition2
      from dual;

    end if;

     if fieldtype1 = 'studystatus' then
      select F_studystat_activity(studyId , fieldCodeSubtype1)
      into v_condition1
      from dual;

    end if;

    if fieldtype2 = 'studystatus' then
      select F_studystat_activity(studyId , fieldCodeSubtype2)
      into v_condition2
      from dual;

    end if;


     if v_condition1 = 0 then
       v_return := -1; --not required
    elsif v_condition1 = 1 and v_condition2 = 1 then
       v_return := 1; --done
    else
    v_return := 0; -- not done

    end if;




    return v_return;


end;

function F_multi_combination_activity(studyId NUMBER,    p_activity_trigger Varchar2,
p_activity_trigger_code Varchar2,p_possible_delvalues Varchar2,
      p_notrequired_ifnotmet Varchar2) return number
 as
 v_condition number := 0;
 v_condition2 number := 0;
  v_return number := 0;
  v_notindex number :=0;
  v_notdoneindex number :=0;
 v_reqlater_index number :=0;
 v_activity_trigger_arr TYPES.SMALL_STRING_ARRAY := NEW TYPES.SMALL_STRING_ARRAY();
 v_activity_trigger_code_arr TYPES.SMALL_STRING_ARRAY := NEW TYPES.SMALL_STRING_ARRAY();
v_possible_delvalues_arr TYPES.SMALL_STRING_ARRAY := NEW TYPES.SMALL_STRING_ARRAY();

   v_notrequired_ifnotmet_arr TYPES.SMALL_STRING_ARRAY := NEW TYPES.SMALL_STRING_ARRAY();
  v_counter number := 0;

  v_activity_trigger varchar2(1000);
   v_activity_trigger_code varchar2(1000);
    v_possible_delvalues varchar2(1000);
     v_notrequired_ifnotmet varchar2(1000);


    v_total_conditions number := 0;
   v_total_conditions_metcount number := 0;

   v_not_required Boolean := false;
   v_condition_logic_result Varchar2(1000);


begin

--process condition arrays

      for i in (
    select column_value  from table(pkg_util.f_split(p_activity_trigger,'|') )  )
    loop

        v_counter := v_counter + 1;
        v_activity_trigger_arr.extend;
        v_activity_trigger_arr(v_counter)  := i.column_value;


    end loop;

    v_counter  := 0;

    for i in (
    select column_value  from table(pkg_util.f_split(p_activity_trigger_code,'|') )  )
    loop

        v_counter := v_counter + 1;
        v_activity_trigger_code_arr.extend;
        v_activity_trigger_code_arr(v_counter)  := i.column_value;


    end loop;

    v_counter  := 0;

    for i in (
    select column_value  from table(pkg_util.f_split(p_possible_delvalues,'|') )  )
    loop

        v_counter := v_counter + 1;
        v_possible_delvalues_arr.extend;
        v_possible_delvalues_arr(v_counter)  := i.column_value;


    end loop;

    v_counter  := 0;

    for i in (
    select column_value  from table(pkg_util.f_split(p_notrequired_ifnotmet,'|') )  )
    loop

        v_counter := v_counter + 1;
        v_notrequired_ifnotmet_arr.extend;
        v_notrequired_ifnotmet_arr(v_counter)  := i.column_value;


    end loop;

  -----------------
  v_total_conditions := v_activity_trigger_arr.count;
  v_total_conditions_metcount := 0;

  FOR i in 1 .. v_total_conditions
  LOOP

      v_activity_trigger :=  v_activity_trigger_arr(i);
      v_possible_delvalues :=  v_possible_delvalues_arr(i);
      v_notrequired_ifnotmet :=  v_notrequired_ifnotmet_arr(i);
      v_activity_trigger_code :=  v_activity_trigger_code_arr(i);

        v_condition := 0;
        v_notindex := 0;
        v_notdoneindex := 0;

        --Checking for not equals index
     IF SUBSTR(v_possible_delvalues,1,1)='!' or SUBSTR(v_activity_trigger_code,1,1)='!' then
        v_notindex := 1;

        IF SUBSTR(v_activity_trigger_code,1,1)='!' THEN
          v_activity_trigger_code := SUBSTR(v_activity_trigger_code,2,LENGTH(v_activity_trigger_code)-1);
        END IF;

        IF SUBSTR(v_possible_delvalues,1,1)='!' THEN
          v_possible_delvalues := SUBSTR(v_possible_delvalues,2,LENGTH(v_possible_delvalues)-1);
        END IF;
     END IF;

    --Checking for not done index
     IF SUBSTR(v_possible_delvalues,1,1)='-' or SUBSTR(v_activity_trigger_code,1,1)='-' then
        v_notdoneindex := 1;

        IF SUBSTR(v_activity_trigger_code,1,1)='-' THEN
          v_activity_trigger_code := SUBSTR(v_activity_trigger_code,2,LENGTH(v_activity_trigger_code)-1);
        END IF;

        IF SUBSTR(v_possible_delvalues,1,1)='-' THEN
          v_possible_delvalues := SUBSTR(v_possible_delvalues,2,LENGTH(v_possible_delvalues)-1);
        END IF;
     END IF;

	--Checking for required later index
     IF SUBSTR(v_possible_delvalues,1,1)='?' or SUBSTR(v_activity_trigger_code,1,1)='?' then
        v_reqlater_index := 1;

        IF SUBSTR(v_activity_trigger_code,1,1)='?' THEN
          v_activity_trigger_code := SUBSTR(v_activity_trigger_code,2,LENGTH(v_activity_trigger_code)-1);
        END IF;

        IF SUBSTR(v_possible_delvalues,1,1)='?' THEN
          v_possible_delvalues := SUBSTR(v_possible_delvalues,2,LENGTH(v_possible_delvalues)-1);
        END IF;
     END IF;


      if v_activity_trigger = 'studyMSD' then

      if INSTR(v_activity_trigger_code,'/',-1)>0 or INSTR(v_possible_delvalues,'/',-1)>0 then
      --added functionality for checking OR condition
	  if INSTR(v_activity_trigger_code,'/',-1)>0 then
        for i in (
          select column_value  from table(pkg_util.f_split(v_activity_trigger_code,'/') )  )
          loop

              select F_MSD_activity(studyId , i.column_value , v_possible_delvalues)
              into v_condition
              from dual;

              exit when v_condition=1;
          end loop;
	  else
		for i in (
          select column_value  from table(pkg_util.f_split(v_possible_delvalues,'/') )  )
          loop

              select F_MSD_activity(studyId , v_activity_trigger_code , i.column_value)
              into v_condition
              from dual;

              exit when v_condition=1;
          end loop;
	  end if;
      else
        select F_MSD_activity(studyId , v_activity_trigger_code , v_possible_delvalues)
        into v_condition
        from dual;
      end if;
      end if; --activity code MSD

    if v_activity_trigger = 'studystatus' then

    if INSTR(v_activity_trigger_code,'/',-1)>0 then
      --added functionality for checking OR condition
        for i in (
          select column_value  from table(pkg_util.f_split(v_activity_trigger_code,'/') )  )
          loop

              select F_studystat_activity(studyId , i.column_value)
              into v_condition
              from dual;

              exit when v_condition=1;
          end loop;
      else
      select F_studystat_activity(studyId , v_activity_trigger_code)
      into v_condition
      from dual;
    end if;
    end if;

    if v_activity_trigger = 'activity' then

    if INSTR(v_activity_trigger_code,'/',-1)>0 then
      --added functionality for checking OR condition
        for i in (
          select column_value  from table(pkg_util.f_split(v_activity_trigger_code,'/') )  )
          loop

              select F_check_activity_exists(studyId , i.column_value)
              into v_condition
              from dual;

              exit when v_condition=1;
          end loop;
      else
      select F_check_activity_exists(studyId , v_activity_trigger_code)
      into v_condition
      from dual;
    end if;
    end if;

    if v_activity_trigger = 'studyDoc' then

      if INSTR(v_possible_delvalues,'/',-1)>0 then
      --added functionality for checking OR condition
        for i in (
          select column_value  from table(pkg_util.f_split(v_possible_delvalues,'/') )  )
          loop

              select F_study_document_activity(studyId ,  i.column_value)
              into v_condition
              from dual;

              exit when v_condition=1;
          end loop;
      else
        select F_study_document_activity(studyId ,  v_possible_delvalues)
        into v_condition
        from dual;
        end if;
   end if; --activity code studyDoc- study documents



    if v_activity_trigger = 'studyform' then

      if v_possible_delvalues = null or trim(v_possible_delvalues)='' then
        v_possible_delvalues := 'complete';
      end if;

      if INSTR(v_activity_trigger_code,'/',-1)>0 or INSTR(v_possible_delvalues,'/',-1)>0 then
      --added functionality for checking OR condition
	  if INSTR(v_activity_trigger_code,'/',-1)>0 then
        for i in (
          select column_value  from table(pkg_util.f_split(v_activity_trigger_code,'/') )  )
          loop

              select F_study_form_activity_anystat(studyId , i.column_value,v_possible_delvalues)
              into v_condition
              from dual;

              exit when v_condition=1;
          end loop;
	  else
		for i in (
          select column_value  from table(pkg_util.f_split(v_possible_delvalues,'/') )  )
          loop

              select F_study_form_activity_anystat(studyId , v_activity_trigger_code, i.column_value)
              into v_condition
              from dual;

              exit when v_condition=1;
          end loop;
	  end if;
      else

      select F_study_form_activity_anystat(studyId , v_activity_trigger_code,v_possible_delvalues)
      into v_condition
      from dual;
      end if;

    end if;

    if v_activity_trigger = 'studysummary' then

    if INSTR(v_activity_trigger_code,'/',-1)>0 or INSTR(v_possible_delvalues,'/',-1)>0 then
      --added functionality for checking OR condition
	  if INSTR(v_activity_trigger_code,'/',-1)>0 then
        for i in (
          select column_value  from table(pkg_util.f_split(v_activity_trigger_code,'/') )  )
          loop

              select F_studysummary_activity(studyId , i.column_value,v_possible_delvalues)
              into v_condition
              from dual;

              exit when v_condition=1;
          end loop;
	  else
		for i in (
          select column_value  from table(pkg_util.f_split(v_possible_delvalues,'/') )  )
          loop

              select F_studysummary_activity(studyId , v_activity_trigger_code, i.column_value)
              into v_condition
              from dual;

              exit when v_condition=1;
        end loop;
	  end if;
      else
      select F_studysummary_activity(studyId , v_activity_trigger_code,v_possible_delvalues)
      into v_condition
      from dual;
    end if;
    end if;

     --Check for not equals index
    IF v_notindex!=0 and v_condition=1 then
          v_condition := 0;
      ELSE IF v_notindex!=0 and v_condition=0 then
          v_condition := 1;
          end if;
        end if;

        if v_condition = 0 then
            if v_notrequired_ifnotmet  = '1' then
                v_not_required := true;
            end if; --not required if not met
        end if; -- v_condition is not met = 0
      dbms_output.put_line('v_activity_trigger-->'||v_activity_trigger);
      dbms_output.put_line('v_not_required-->'||v_notrequired_ifnotmet||'v_condition-->'||v_condition);

        --Checking for not done index
      IF v_notdoneindex!=0 and v_condition =1 then
        v_condition :=0;
      END IF;

	  --Checking for required later index
      IF v_reqlater_index != 0 and v_condition =0 then
        return -2;
      END IF;

      if v_not_required then
        return -1;
      end if;

      v_total_conditions_metcount := v_total_conditions_metcount + v_condition;



   END LOOP;


  if v_total_conditions_metcount = v_total_conditions  then
    v_return := 1; --done
  else
    v_return := 0; -- not done

  end if;


    return v_return;


end;


--Sonia Abrol, 22 June 2016 to create generic  more study details based rule for WF activity

  /*

  fieldName : possible values:

                   name of the field in er_study : for all text fields that can be compared from er_study table OR
                                            for all codelist fields in er_study
                   indide : check if indide data exists

  fieldValue : pass '*' delimited value to compare with the field. ignored for indide check

  samples:

  --check indide

     select PKG_WF_GENERIC_RULES.F_studysummary_activity
      (19947 , '', 'indide', '')
      from dual;

   -- check summary codelist field:
      select PKG_WF_GENERIC_RULES.F_studysummary_activity
      (19946 , 'fk_codelst_restype', 'codelist', 'human_subjects*none')
      from dual;

    -- check summary text field:
      select PKG_WF_GENERIC_RULES.F_studysummary_activity
      (19946 , 'study_number', 'string', 'STD02')
      from dual;

  */
FUNCTION F_studysummary_activity(studyId NUMBER, fieldName Varchar2, fieldValue Varchar2) RETURN NUMBER
  AS

  v_RETURN_NO NUMBER := 0;
  v_fld_value varchar2(4000) := '';
  v_value_found number :=0;
  v_sql Varchar2(4000);
  v_value_string varchar2(4000);
  v_code_subtyp varchar2(40);
  fieldCodeSubtype varchar2(4000);

 BEGIN


  begin



   if ( fieldName = 'indide') then
        fieldCodeSubtype := 'indide';
   else

     begin
       select INSTR('*'||pkg_wf_generic_rules.SummaryCodelislFields ||'*', '*'|| upper(fieldName) || '*')
            into v_value_found
            from dual;

            if v_value_found > 0 then
                 fieldCodeSubtype := 'codelist';
            else
                  fieldCodeSubtype := 'string';

            end if;

       exception when no_data_found then
            fieldCodeSubtype := 'string';

     end;

  end if;

   v_value_found:= 0;

   if (fieldCodeSubtype <> 'indide' ) then
	if fieldValue = 'notnull' then
      v_sql := ' select nvl(to_char(' || fieldName || '),''#'') from er_study where pk_study = :p_study';
	else
	  v_sql := ' select to_char(' || fieldName || ') from er_study where pk_study = :p_study';
    end if;
      EXECUTE IMMEDIATE v_sql into v_value_string USING studyId ;

   end if;

    if ( fieldCodeSubtype = 'codelist') and (nvl(v_value_string,0) > 0 ) then
       -- get code subtype and match


      begin
        select codelst_subtyp
        into v_code_subtyp
        from er_codelst where pk_codelst = v_value_string;
      exception when no_data_found then
        v_code_subtyp := '';
      end;



       -- check if the value passed matches the study MSD value
        select INSTR('*'||fieldValue||'*', '*'|| v_code_subtyp || '*')
        into v_value_found
        from dual;

    end if;

    if (fieldCodeSubtype = 'string') then

         -- check if the value passed matches the study MSD value
        select INSTR('*'||fieldValue||'*', '*'|| v_value_string || '*')
        into v_value_found
        from dual;

    end if;

      if (fieldCodeSubtype = 'indide') then

         -- check if the value passed matches the study MSD value
        select count(*)
        into v_value_found
        from er_study_indide where fk_study = studyId;

    end if;

    if instr(fieldValue,'notnull')>0 and instr(v_value_string,'#')<=0 then
		dbms_output.put_line('Value exists for category');
		v_value_found:=1;
	end if;

  exception when others then
      v_value_string:= '';
      v_value_found := 0;
      plog.fatal(pCTX,'F_studysummary_activity, study'|| studyId || ' error:' || sqlerrm);

    end;


    IF v_value_found > 0 THEN
      v_RETURN_NO := 1; --Done
    END IF;

    RETURN v_RETURN_NO;
END;

FUNCTION F_check_activity_exists(studyId NUMBER,  activityName Varchar2) RETURN NUMBER
AS
v_status number:=0;
sql_fetch VARCHAR2(4000):= '';
TYPE CurTyp
IS
  REF
  CURSOR;
  cur_cv CurTyp;
  tmp_rescount number;
BEGIN
  select WA_DISPLAYQUERY into sql_fetch from workflow_activity where FK_ACTIVITYID=(select PK_ACTIVITY from ACTIVITY where ACTIVITY_NAME =activityName);
  sql_fetch := REPLACE( sql_fetch, ':1', studyId );
  IF(LENGTH(sql_fetch) >1 ) THEN
      -- check the post condition status
      OPEN cur_cv FOR sql_fetch;
      LOOP
        FETCH cur_cv INTO tmp_rescount ; -- fetch next row
        EXIT
      WHEN cur_cv%NOTFOUND; -- exit loop when last row is fetched
      dbms_output.put_line('tmp_rescount----->'||tmp_rescount);
      IF(tmp_rescount <= 0) THEN
        v_status := 0;
      ELSE
        v_status := 1;
      END IF;
      END LOOP;
    END IF;
  return v_status;
END;

END PKG_WF_GENERIC_RULES;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,444,6,'06_PKG_WF_GENERIC_RULES.sql',sysdate,'v11.1.0 #845');
commit;