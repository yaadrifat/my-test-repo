/////*********This readMe is specific to v11 build #831(compatible with Oracle 12c)**********////
*********************************************************************************************************************************************************************
1. Regular bug fixing .



Note: We have include a file (Aspose.Words.lic) in ereshome. This is related to the Document Stamping. 

NOTE:
Covered INF-44463:User and Org Appendix Icon enhancement. As per the requirement document, appendix icon should display next to the form icon in Site 
and User details. But When user clicks on the Organization name link from the NETWORKs, Site Details page is appeared as popup. But, it does not show 
any forms Icon (For Organization Forms). There is a defect with id #29219 reported for the same. QA already raise query regarding this defect and 
waiting for PM team to respond. For now we have covered this defect also. If it is not required, we will revert the changes in future.