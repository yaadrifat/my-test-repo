/////*********This readMe is specific to v11 build #828(compatible with Oracle 12c)**********////
*********************************************************************************************************************************************************************
1. Regular bug fixing.

2. In this Build, we have integrated v10 Patch#11.

Note: We need to execute db patches in following order:
1) execute the patch of epat folder.
2)  i) execute all patches of eres folder except xsl folder.
    ii) After that, execute the xsl patches inside the eres folder.
3) execute the patch of esch folder.
4) Lastly, execute the xsl patches inside the db_patch folder.
 


