declare
begin
	delete from er_repxsl where pk_repxsl in (256);
commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,427,6,'06_er_repxsl_delete.sql',sysdate,'v11 #828');

commit;
/
