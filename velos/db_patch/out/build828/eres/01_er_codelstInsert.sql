set define off;
DECLARE 
row_check number;
BEGIN
SELECT count(*)
		INTO row_check
		FROM ER_CODELST
		WHERE CODELST_TYPE = 'specimen' and CODELST_SUBTYP='freeze_time';
		
		IF(row_check=0) then
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (SEQ_ER_CODELST.nextval,null,'specimen','freeze_time','Freeze Time','N',(select nvl(max(codelst_seq)+1,1) from er_codelst where codelst_type='specimen'),null,null,null,null,sysdate,sysdate,null,'time',null,null,null);
End if;

SELECT count(*)
		INTO row_check
		FROM ER_CODELST
		WHERE CODELST_TYPE = 'specimen' and CODELST_SUBTYP='icd_10';
		
		IF(row_check=0) then
		
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (SEQ_ER_CODELST.nextval,null,'specimen','icd_10','ICD10','N',(select nvl(max(codelst_seq)+1,1) from er_codelst where codelst_type='specimen'),null,null,null,null,sysdate,sysdate,null,'lookup','{lookupPK:6510, selection:"multi", mapping:[{source:"ICD_CODE", target:"alternateId"},{source:"SHORTDESC", target:"alternateId"},{source:"LONGDESC", target:"alternateId"}]}',null,null);

End if;
SELECT count(*)
		INTO row_check
		FROM ER_CODELST
		WHERE CODELST_TYPE = 'specimen' and CODELST_SUBTYP='orgdiagdate';
		
		IF(row_check=0) then
		
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (SEQ_ER_CODELST.nextval,null,'specimen','orgdiagdate','Original Diagnosis Date','N',(select nvl(max(codelst_seq)+1,1) from er_codelst where codelst_type='specimen'),null,null,null,null,sysdate,sysdate,null,'date',null,null,null);

End if;

SELECT count(*)
		INTO row_check
		FROM ER_CODELST
		WHERE CODELST_TYPE = 'specimen' and CODELST_SUBTYP='mib';
		
		IF(row_check=0) then
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (SEQ_ER_CODELST.nextval,null,'specimen','mib','MIB','N',(select nvl(max(codelst_seq)+1,1) from er_codelst where codelst_type='specimen'),null,null,null,null,sysdate,sysdate,null,'textarea',null,null,null);

End if;
SELECT count(*)
		INTO row_check
		FROM ER_CODELST
		WHERE CODELST_TYPE = 'specimen' and CODELST_SUBTYP='brainreg';
		
		IF(row_check=0) then
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (SEQ_ER_CODELST.nextval,null,'specimen','brainreg','Brain Region','N',(select nvl(max(codelst_seq)+1,1) from er_codelst where codelst_type='specimen'),null,null,null,null,sysdate,sysdate,null,'checkbox','{chkArray:[{data: ''option1'', display:''Stem''}, {data: ''option2'', display:''Back''}, {data: ''option3'', display:''Caudate''}, {data: ''option4'', display:''Cortex''}, {data: ''option5'', display:"Spinal Cord"}, {data: ''option6'', display:''Ventricle NOS''}, {data: ''option7'', display:''Ventricle Lateral''}, {data: ''option8'', display:''Ventricle 3''},{data: ''option9'', display:''Ventricle 4''} ]}',null,null);

End if;


commit;
End;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,427,1,'01_er_codelstInsert.sql',sysdate,'v11 #828');

commit;
/

