set define off;
Declare
	col_check number;
Begin
	select count(*) into col_check from user_tab_cols
	where column_name='INV_PAYMENT_DUEDATE';
	
	if(col_check=0) Then
		execute immediate 'ALTER TABLE er_invoice ADD INV_PAYMENT_DUEDATE date';
		execute immediate 'COMMENT ON COLUMN "ERES"."ER_INVOICE"."INV_PAYMENT_DUEDATE" IS ''This column stores the invoice PaymentDueDate''';
	End if;
commit;
End;	
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,427,2,'02_alter_er_invoice.sql',sysdate,'v11 #828');

commit;
/