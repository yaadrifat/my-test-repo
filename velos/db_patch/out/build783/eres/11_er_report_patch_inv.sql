DECLARE 
	table_check number;
	row_check number;
	update_sql clob;

BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=128;

		IF (row_check > 0) then
		
			update_sql:='SELECT spec_id, spec_alternate_id,
				spec_original_quantity AS qty,
				(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = spec_quantity_units) AS units,
				(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = spec_type AND codelst_type = ''specimen_type'') AS sp_type,
				(SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study) AS study,
				TO_CHAR(spec_collection_date,PKG_DATEUTIL.F_GET_DATEFORMAT)AS spec_collection_date,
				(SELECT per_code FROM ER_PER WHERE pk_per = fk_per) AS patid,
				DECODE(fk_specimen,NULL,'''' ,''Yes'') AS child_yes,
				(SELECT spec_id FROM ER_SPECIMEN WHERE pk_specimen = a.fk_specimen) AS parent,
				storage_name, storage_id,
				(SELECT storage_name FROM ER_STORAGE WHERE b.fk_storage = pk_storage) AS storage_loc
				FROM ER_SPECIMEN a, ER_STORAGE b
				WHERE pk_storage (+) = a.fk_storage
				AND (('':specTyp''=''NonStudy'' AND fk_study is null AND a.fk_account = :sessAccId) or ('':specTyp''=''Study'' AND fk_study in (:studyId)))
				AND (a.fk_per is not null AND exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = a.fk_per and per.fk_account =:sessAccId
				AND usr.fk_user =:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
				AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
				and a.FK_SITE IN (:orgId)
				order by patid, study, spec_id';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=128;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=129;

		IF (row_check > 0) then
				
				update_sql:='SELECT pk_storage, storage_id,
			decode(s.fk_storage,null,(SELECT b.storage_name FROM ER_STORAGE b WHERE s.pk_storage = b.pk_storage),
			(SELECT b.storage_name FROM ER_STORAGE b WHERE s.fk_storage = b.pk_storage)) as parent, storage_name,
			(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_storage_type AND codelst_type = ''store_type'') AS st_type,
			decode(s.fk_storage,null,pk_storage,s.fk_storage) as fk_storage, storage_cap_number AS cap,
			(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_capacity_units AND codelst_type = ''cap_unit'')  AS unit,
			DECODE((storage_dim1_cellnum ||'' x ''|| storage_dim2_cellnum),'' x '','''',(storage_dim1_cellnum ||'' x ''|| storage_dim2_cellnum)) AS grid,
			(SELECT COUNT(*) FROM ER_STORAGE WHERE fk_storage = s.pk_storage) AS child_storage,
			(SELECT COUNT(*) FROM ER_SPECIMEN WHERE pk_storage = ER_SPECIMEN.fk_storage) AS num_sample,
			((SELECT b.storage_name FROM ER_STORAGE b WHERE s.fk_storage = b.pk_storage) || '''' || storage_name) AS sort_crit,
			ss_start_date,
			(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_storage_status AND codelst_type = ''storage_stat'')  AS status,
			Decode((SELECT usr_firstname ||'' ''|| usr_lastname FROM ER_USER WHERE pk_user = fk_user),null,''No'',(SELECT usr_firstname ||'' ''|| usr_lastname FROM ER_USER WHERE pk_user = fk_user)) AS res_user,
			Decode((SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study),null,''No'',(SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study)) AS res_study
			FROM ER_STORAGE s, ER_STORAGE_STATUS ss
			WHERE s.pk_storage = ss.fk_storage and s.fk_storage is not null
			and s.fk_account =:sessAccId and s.pk_storage in (:storageId)
			AND ss.PK_STORAGE_STATUS in (SELECT max(ss1.PK_STORAGE_STATUS) FROM er_storage_status ss1
			WHERE ss1.FK_STORAGE = s.PK_STORAGE
			AND ss1.SS_START_DATE in (select max(ss2.SS_START_DATE) from er_storage_status ss2
			where ss2.FK_STORAGE = s.PK_STORAGE))
			order by sort_crit';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=129;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=130;

		IF (row_check > 0) then
				
				update_sql:='SELECT spec_id, spec_alternate_id,
			spec_original_quantity AS qty,
			(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = spec_quantity_units) AS units,
			(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = spec_type AND codelst_type = ''specimen_type'') AS sp_type,
			(SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study) AS study,
			TO_CHAR(spec_collection_date,PKG_DATEUTIL.F_GET_DATEFORMAT)AS spec_collection_date,
			(SELECT per_code FROM ER_PER WHERE pk_per = fk_per) AS patid,
			DECODE(fk_specimen,NULL,'''' ,''Yes'') AS child_yes,
			(SELECT spec_id FROM ER_SPECIMEN WHERE pk_specimen = a.fk_specimen) AS parent,
			storage_name, storage_id,
			(SELECT storage_name FROM ER_STORAGE WHERE b.fk_storage = pk_storage) AS storage_loc
			FROM ER_SPECIMEN a, ER_STORAGE b
			WHERE pk_storage (+) = a.fk_storage
			AND (a.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = a.fk_per and per.fk_account =:sessAccId
			AND usr.fk_user =:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
			AND '':specTyp''=''Study'' AND fk_study IN (:studyId)
			AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
			AND a.FK_SITE IN (:orgId)
			order by study, patid, spec_id';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=130;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=136;

		IF (row_check > 0) then
			
				update_sql:='SELECT a.fk_account, a.fk_study, spec_id, spec_alternate_id,
			case when a.fk_study is null then ''[No Study Specified]''
			else (SELECT study_number FROM er_study WHERE pk_study = a.fk_study) end study_number,
			case when (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) is null then ''-''
			else (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) end spec_type,
			TO_CHAR(spec_collection_date,PKG_DATEUTIL.F_GET_DATEFORMAT) spec_collection_date,
			case when (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_anatomic_site) is null then ''-''
			else (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_anatomic_site) end  spec_anatomic_site,
			spec_original_quantity,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_quantity_units) spec_quantity_units
			FROM er_specimen a, er_specimen_status b
			WHERE (('':specTyp''=''NonStudy'' AND a.fk_study is null AND a.fk_account = :sessAccId) or ('':specTyp''=''Study'' AND a.fk_study in (:studyId)))
			AND (a.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = a.fk_per and per.fk_account =:sessAccId
			and usr.fk_user =:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
			AND b.fk_specimen = a.pk_specimen
			AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
			AND b.FK_CODELST_STATUS IN (SELECT pk_codeLst FROM er_codelst WHERE codelst_subtyp=''Depleted'')
			AND b.PK_SPECIMEN_STATUS = (SELECT MAX(c.PK_SPECIMEN_STATUS) FROM er_specimen_status c
			WHERE c.fk_specimen = a.pk_specimen
			AND c.SS_DATE = (SELECT MAX(d.SS_DATE) FROM er_specimen_status d
			WHERE d.fk_specimen = a.pk_specimen))
			AND a.FK_SITE IN (:orgId)
			ORDER BY spec_collection_date';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=136;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=137;

		IF (row_check > 0) then
				
				update_sql:='SELECT case when '':specTyp''=''Study'' then
			pkg_inventory.f_get_specimen_trail(:sessUserId, '':studyId'', TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),'':orgId'')
			end from dual';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=137;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=138;

		IF (row_check > 0) then
				
				update_sql:='SELECT pk_specimen, spec_id, spec_alternate_id, a.fk_study,
			(SELECT p.SPEC_ID FROM ER_SPECIMEN p WHERE p.pk_specimen = a.fk_specimen) PSEC_ID,
			(SELECT count(*) FROM ER_SPECIMEN p WHERE p.fk_specimen = a.pk_specimen) ChildCnt,
			case when a.fk_study is null then ''[No Study Specified]''
			else (SELECT study_number FROM er_study WHERE pk_study = a.fk_study) end study_number,
			b.SS_DATE,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst=b.FK_CODELST_STATUS) as Spec_Staus,
			case when (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) is null then ''-''
			else (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) end spec_type,
			spec_collection_date,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_anatomic_site) spec_anatomic_site,
			spec_original_quantity,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_quantity_units) spec_quantity_units,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst=b.SS_PROC_TYPE) as SS_PROC_TYPE,
			SS_HAND_OFF_DATE, SS_PROC_DATE, SS_QUANTITY,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = SS_QUANTITY_UNITS) SS_QUANTITY_UNITS,
			(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=b.FK_USER_RECEPIENT ) AS SS_RECEPIENT_NAME,
			(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=b.SS_STATUS_BY ) AS SS_STATUSBY_NAME,
			(SELECT STUDY_NUMBER FROM ER_STUDY where pk_study=b.FK_STUDY) AS SS_STUDY_NUMBER
			FROM er_specimen a, er_specimen_status b
			WHERE (('':specTyp''=''NonStudy'' AND a.fk_study is null AND a.fk_account = :sessAccId) or ('':specTyp''=''Study'' AND a.fk_study in (:studyId)))
			AND (a.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = a.fk_per and per.fk_account =:sessAccId
			and usr.fk_user =:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
			AND b.fk_specimen = a.pk_specimen
			AND b.FK_CODELST_STATUS IN (SELECT pk_codeLst FROM er_codelst WHERE codelst_custom_col1=''process'')
			AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
			AND a.FK_SITE IN (:orgId)
			ORDER BY pk_specimen, spec_collection_date';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=138;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=139;

		IF (row_check > 0) then
			
				update_sql:='select s.pk_storage PPK_Storage, s.Storage_id PStorage_id, s.storage_name PStorage_name,
			s.STORAGE_ALTERNALEID PAlt_ID, (SELECT codelst_desc FROM er_codelst WHERE pk_codeLst = s.STORAGE_UNIT_CLASS) PUnit_class,
			(SELECT codelst_desc FROM er_codelst WHERE pk_codeLst=s.FK_CODELST_STORAGE_TYPE) PStorage_type,
			''Occupied'' PStatus,
			sch.pk_storage CPK_Storage, sch.Storage_id CStorage_id, sch.storage_name CStorage_name,
			sch.STORAGE_ALTERNALEID CAlt_ID, (SELECT codelst_desc FROM er_codelst WHERE pk_codeLst=sch.STORAGE_UNIT_CLASS) CUnit_class,
			(SELECT codelst_desc FROM er_codelst WHERE pk_codeLst=sch.FK_CODELST_STORAGE_TYPE) CStorage_type,
			(SELECT codelst_desc FROM er_codelst WHERE pk_codeLst=schs.FK_CODELST_STORAGE_STATUS) CStatus
			from er_storage s, er_storage_status ss, er_storage sch, er_storage_status schs
			WHERE s.pk_storage = ss.fk_storage
			AND ss.FK_CODELST_STORAGE_STATUS IN (SELECT pk_codeLst FROM er_codelst WHERE codelst_subtyp=''Occupied'')
			AND ss.PK_STORAGE_STATUS in (SELECT max(ss1.PK_STORAGE_STATUS) FROM er_storage_status ss1
			WHERE ss1.FK_STORAGE = s.PK_STORAGE
			AND ss1.SS_START_DATE in (select max(ss2.SS_START_DATE) from er_storage_status ss2
			where ss2.FK_STORAGE = s.PK_STORAGE))
			and sch.fk_storage = s.pk_storage and sch.pk_storage = schs.fk_storage
			AND schs.FK_CODELST_STORAGE_STATUS not IN (SELECT pk_codeLst FROM er_codelst WHERE codelst_subtyp=''Occupied'')
			AND schs.PK_STORAGE_STATUS in (SELECT max(schs1.PK_STORAGE_STATUS) FROM er_storage_status schs1
			WHERE schs1.FK_STORAGE = sch.PK_STORAGE
			AND schs1.SS_START_DATE in (select max(schs2.SS_START_DATE) from er_storage_status schs2
			where schs2.FK_STORAGE = sch.PK_STORAGE))
			and s.pk_storage in (:storageId)
			order by s.storage_name,s.Storage_id,sch.storage_name,sch.Storage_id';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=139;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=141;

		IF (row_check > 0) then
				
		update_sql:='select case when lower('':storageId'') like ''select%'' then
		to_clob(''<?xml version="1.0" encoding="UTF-8"?><ROWSET><ROW1><STORE_NAME>Storage filter [ALL] does not apply to this report. Please select a storage in filter.</STORE_NAME></ROW1></ROWSET>'')
		else pkg_inventory.f_get_storage_trail(:sessAccId, '':storageId'')
		end from dual';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=141;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=142;

		IF (row_check > 0) then
				
			update_sql:='select stor.pk_storage,STORAGE_ID,STORAGE_NAME,
		(SELECT p.STORAGE_ID FROM ER_STORAGE p WHERE p.pk_STORAGE = stor.fk_STORAGE) PSTORE_ID,
		(SELECT count(*) FROM ER_STORAGE p WHERE p.fk_STORAGE = stor.pk_storage) StoreChildCnt,
		(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = FK_CODELST_STORAGE_TYPE) STORE_TYPE,spec.pk_specimen,
		SPEC_ID,(SELECT count(*) FROM ER_SPECIMEN p WHERE p.fk_specimen = spec.pk_specimen) SpecChildCnt,
		(SELECT p.SPEC_ID FROM ER_SPECIMEN p WHERE p.pk_specimen = spec.fk_specimen) PSPEC_ID,
		(SELECT study_number FROM ER_STUDY WHERE pk_study = FK_STUDY) STUDY_NUMBER,
		(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) SPEC_TYPE,
		SPEC_ORIGINAL_QUANTITY || '' ''||(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_quantity_units) SPEC_ORIGINAL_QUANTITY
		from er_storage stor, er_specimen spec
		where spec.fk_storage = stor.pk_storage
		and (('':specTyp''=''NonStudy'' AND spec.fk_study is null AND spec.fk_account = :sessAccId) or ('':specTyp''=''Study'' AND spec.fk_study in (:studyId)))
		AND (spec.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = spec.fk_per and per.fk_account =:sessAccId
		and usr.fk_user =:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
		AND stor.pk_storage in (:storageId)
		AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
		AND spec.fk_site IN (:orgId)
		order by storage_id';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=142;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=143;

		IF (row_check > 0) then
				
			update_sql:='SELECT a.fk_account, a.fk_study, spec_id, spec_alternate_id,
		case when a.fk_study is null then ''[No Study Specified]''
		else (SELECT study_number FROM er_study WHERE pk_study = a.fk_study) end study_number,
		case when FK_USER_PATH is null then ''[Not specified]''
		else (select usr_firstName || '' '' || usr_lastname from er_user where pk_user = FK_USER_PATH) end pathologist,
		case when FK_USER_SURG is null then ''[Not specified]''
		else (select usr_firstName || '' '' || usr_lastname from er_user where pk_user = FK_USER_SURG) end Surgeon,
		(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) spec_type,
		TO_CHAR(spec_collection_date,PKG_DATEUTIL.F_GET_DATEFORMAT) spec_collection_date,
		(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_anatomic_site) spec_anatomic_site,
		spec_original_quantity,
		(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_quantity_units) spec_quantity_units
		FROM er_specimen a, er_specimen_status b
		WHERE (('':specTyp''=''NonStudy'' AND a.fk_study is null AND a.fk_account = :sessAccId) or ('':specTyp''=''Study'' AND a.fk_study in (:studyId)))
		AND (a.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = a.fk_per and per.fk_account =:sessAccId
		and usr.fk_user =:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
		AND b.fk_specimen = a.pk_specimen
		AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
		AND b.PK_SPECIMEN_STATUS = (SELECT MAX(c.PK_SPECIMEN_STATUS) FROM er_specimen_status c
		WHERE c.fk_specimen = a.pk_specimen AND c.SS_DATE = (SELECT MAX(d.SS_DATE) FROM er_specimen_status d WHERE d.fk_specimen = a.pk_specimen ))
		and a.FK_SITE IN (:orgId)
		ORDER BY spec_collection_date';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=143;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=144;

		IF (row_check > 0) then
				
			update_sql:='select s.pk_storage PPK_Storage, Storage_id, storage_name,
		s.STORAGE_ALTERNALEID, (SELECT codelst_desc FROM er_codelst WHERE pk_codeLst = s.STORAGE_UNIT_CLASS) STORAGE_UNIT_CLASS,
		(SELECT codelst_desc FROM er_codelst WHERE pk_codeLst=s.FK_CODELST_STORAGE_TYPE) STORAGE_TYPE,
		(SELECT codelst_desc FROM er_codelst WHERE pk_codeLst = ss.FK_CODELST_STORAGE_STATUS) STORAGE_STATUS,
		STORAGE_CAP_NUMBER CAPACITY_SPECIFIED,
		STORAGE_DIM1_CELLNUM * STORAGE_DIM2_CELLNUM CAPACITY_CALCULATED
		from er_storage s, er_storage_status ss
		WHERE s.pk_storage = ss.fk_storage
		AND FK_CODELST_CAPACITY_UNITS in (SELECT pk_codeLst FROM er_codelst WHERE codelst_subtyp =''Items'')
		AND ss.PK_STORAGE_STATUS in (SELECT max(ss1.PK_STORAGE_STATUS) FROM er_storage_status ss1 WHERE ss1.FK_STORAGE = s.PK_STORAGE
		AND ss1.SS_START_DATE in (select max(ss2.SS_START_DATE) FROM er_storage_status ss2 WHERE ss2.FK_STORAGE = s.PK_STORAGE))
		AND STORAGE_CAP_NUMBER <> (STORAGE_DIM1_CELLNUM * STORAGE_DIM2_CELLNUM)
		AND FK_CODELST_CHILD_STYPE is not null
		AND s.pk_storage in (:storageId)
		order by s.storage_name,s.Storage_id';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=144;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=145;

		IF (row_check > 0) then
				
			update_sql:='SELECT pk_specimen, spec_id, spec_alternate_id, a.fk_study,
		(SELECT p.SPEC_ID FROM ER_SPECIMEN p WHERE p.pk_specimen = a.fk_specimen) PSEC_ID,
		(SELECT count(*) FROM ER_SPECIMEN p WHERE p.fk_specimen = a.pk_specimen) ChildCnt,
		case when a.fk_study is null then ''[No Study Specified]''
		else (SELECT study_number FROM er_study WHERE pk_study = a.fk_study) end study_number,
		b.SS_DATE,
		(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst=b.FK_CODELST_STATUS) as Spec_Staus,
		case when (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) is null then ''-''
		else (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) end spec_type,
		spec_collection_date,
		(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_anatomic_site) spec_anatomic_site,
		spec_original_quantity,
		(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_quantity_units) spec_quantity_units,
		(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst=b.SS_PROC_TYPE) as SS_PROC_TYPE,
		SS_HAND_OFF_DATE, SS_PROC_DATE, SS_QUANTITY,
		(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = SS_QUANTITY_UNITS) SS_QUANTITY_UNITS,
		(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=b.FK_USER_RECEPIENT ) AS SS_RECEPIENT_NAME,
		(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=b.SS_STATUS_BY ) AS SS_STATUSBY_NAME,
		(SELECT STUDY_NUMBER FROM ER_STUDY where pk_study=b.FK_STUDY) AS SS_STUDY_NUMBER
		FROM er_specimen a, er_specimen_status b
		WHERE (('':specTyp''=''NonStudy'' AND a.fk_study is null AND a.fk_account = :sessAccId) or ('':specTyp''=''Study'' AND a.fk_study in (:studyId)))
		AND (a.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = a.fk_per and per.fk_account =:sessAccId
		and usr.fk_user =:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
		AND b.fk_specimen = a.pk_specimen
		AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
		AND a.fk_site IN (:orgId)
		ORDER BY pk_specimen, spec_collection_date, SS_PROC_DATE';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=145;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=154;

		IF (row_check > 0) then
				
		update_sql:='SELECT pkg_inventory.f_get_specimen_trail(:sessUserId,''NonStudy'', TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),'':orgId'') from dual';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=154;
		END IF;

		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=155;

		IF (row_check > 0) then
				
			update_sql:='select
		case when (spec.FK_STUDY IS NULL AND SPEC.fk_per IS NOT NULL) THEN ''Patient''
		when (spec.FK_STUDY IS NOT NULL AND SPEC.fk_per IS NULL) THEN ''Study''
		when (spec.FK_STUDY IS NULL AND SPEC.fk_per IS NULL) THEN ''Account''
		when (spec.FK_STUDY IS NOT NULL AND SPEC.fk_per IS NOT NULL) THEN ''Patient Study''
		end as spec_Kind,
		spec.pk_specimen, SPEC_ID,spec_alternate_id,
		(SELECT count(*) FROM ER_SPECIMEN p WHERE p.fk_specimen = spec.pk_specimen) SpecChildCnt,
		(SELECT p.SPEC_ID FROM ER_SPECIMEN p WHERE p.pk_specimen = spec.fk_specimen) PSPEC_ID,
		(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) SPEC_TYPE,
		(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = (SELECT s.FK_CODELST_STATUS FROM er_specimen_status s
		WHERE spec.pk_specimen = s.fk_specimen AND s.PK_SPECIMEN_STATUS = (SELECT MAX(ss.PK_SPECIMEN_STATUS) FROM er_specimen_status ss
		WHERE ss.fk_specimen = spec.pk_specimen AND ss.SS_DATE = (SELECT MAX(ss1.SS_DATE) FROM er_specimen_status ss1
		WHERE ss1.fk_specimen = spec.pk_specimen )))) SPEC_STATUS,
		SPEC_ORIGINAL_QUANTITY || '' ''||(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_quantity_units) SPEC_ORIGINAL_QUANTITY,
		TO_CHAR(spec_collection_date,PKG_DATEUTIL.F_GET_DATEFORMAT)AS spec_collection_date,
		(SELECT site_name FROM ER_SITE WHERE pk_site =  FK_SITE) AS spec_orgn,
		(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = SPEC_ANATOMIC_SITE) SPEC_ANATOMIC_SITE,
		(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = SPEC_TISSUE_SIDE) SPEC_TISSUE_SIDE,
		(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = SPEC_PATHOLOGY_STAT) SPEC_PATHOLOGY_STAT,
		(SELECT study_number FROM ER_STUDY WHERE pk_study = spec.FK_STUDY) STUDY_NUMBER,
		(SELECT per_code FROM ER_PER WHERE pk_per = SPEC.fk_per) AS patid,
		DECODE(spec.fk_specimen,NULL,'''' ,''Yes'') AS child_yes,
		(SELECT spec_id FROM ER_SPECIMEN WHERE pk_specimen = spec.fk_specimen) AS spec_parent,
		stor.pk_storage,STORAGE_ID,STORAGE_NAME,
		(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = FK_CODELST_STORAGE_TYPE) STORE_TYPE,
		(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME  FROM ER_USER where pk_user=spec.FK_USER_PATH ) AS SPEC_PATHOLOGY_NAME,
		(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=spec.FK_USER_SURG ) AS SPEC_SURGEON_NAME,
		(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=spec.FK_USER_COLLTECH ) AS SPEC_COLLTECH_NAME,
		(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=spec.FK_USER_PROCTECH ) AS SPEC_PROCTECH_NAME
		from er_specimen spec, er_storage stor
		where stor.pk_storage(+) = spec.fk_storage
		AND (('':specTyp''=''NonStudy'' AND spec.fk_study is null AND spec.fk_account = :sessAccId) or ('':specTyp''=''Study'' AND spec.fk_study in (:studyId)))
		AND (spec.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = spec.fk_per and per.fk_account =:sessAccId
		and usr.fk_user=:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
		AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
		AND SPEC.FK_SITE IN (:orgId)
		order by storage_id';
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=155;
		END IF;
END IF;		
commit;		
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,11,'11_er_report_patch_inv.sql',sysdate,'v10.1 #783');

commit;
