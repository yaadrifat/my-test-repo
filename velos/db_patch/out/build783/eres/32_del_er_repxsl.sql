set define off;
delete  from  er_repxsl where pk_repxsl in (256,257,258,259,260,261,262,263,264,265,266,272,292);
commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,32,'32_del_er_repxsl.sql',sysdate,'v10.1 #783');

commit; 
 