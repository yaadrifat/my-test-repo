delete  from  er_repxsl where pk_repxsl in (148,500,501);
delete  from er_report where pk_report in (148,500,501);

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,43,'43_delete_from_er_report.sql',sysdate,'v10.1 #783');

commit;
