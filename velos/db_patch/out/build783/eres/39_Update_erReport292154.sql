set Define off;
DECLARE 
	table_check number;
	row_check number;

BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=292;

		IF (row_check > 0) then
UPDATE ER_REPORT SET REP_COLUMNS='Study Number, Enrolling Site, Patient Study ID,Current Status,Patient Study Status, Status Date, Reason' WHERE pk_report=292;					
		END IF; 
	END IF;
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=154;

		IF (row_check > 0) then
UPDATE ER_REPORT SET REP_FILTERBY='Date,Organization' WHERE pk_report=154;					
		END IF; 
	END IF;
	commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,39,'39_Update_erReport292154.sql',sysdate,'v10.1 #783');

commit;
