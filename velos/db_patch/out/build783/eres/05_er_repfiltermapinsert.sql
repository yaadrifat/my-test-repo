Declare
pkRepFilter number;
pkfilterMap number;
row_check number;
Begin
select pk_repfilter into pkRepFilter  from er_repfilter where repfilter_keyword='patStatusId';
select SEQ_ER_REPFILTERMAP.nextval into pkfilterMap from dual;
SELECT count(*)
		INTO row_check
		FROM ER_REPFILTERMAP
		WHERE repfiltermap_repcat='study' and fk_repfilter=pkRepFilter;
if(pkRepFilter>0 and row_check=0) then
insert into er_repfiltermap(PK_REPFILTERMAP,FK_REPFILTER,REPFILTERMAP_REPCAT,REPFILTERMAP_SEQ,REPFILTERMAP_DISPDIV) 
values (pkfilterMap,pkRepFilter,'study',(select max(REPFILTERMAP_SEQ)+1 from er_repfiltermap where repfiltermap_repcat='study'),'patstatusDIV,patstatusdataDIV');
End IF;
commit;
End;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,5,'05_er_repfiltermapinsert.sql',sysdate,'v10.1 #783');

commit;
