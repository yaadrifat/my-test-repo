set define off;
update er_codelst set CODELST_CUSTOM_COL='system administration'  where codelst_type='report' and CODELST_CUSTOM_COL='account';
update er_repfiltermap set REPFILTERMAP_REPCAT='system administration'  where REPFILTERMAP_REPCAT='account';
commit;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,13,'13_RepCodelstchange.sql',sysdate,'v10.1 #783');

commit;