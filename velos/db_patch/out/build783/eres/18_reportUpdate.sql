DECLARE 
	table_check number;
	row_check number;
	update_sql clob;
BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=126;
		
		IF(row_check>0) then
			Update er_report set REP_FILTERAPPLICABLE='date:orgId:sessAccId:studyId' where pk_report=126;
		End if;
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE  pk_report=127;
		IF(row_check>0) then
			Update er_report set REP_FILTERAPPLICABLE='date:orgId:sessAccId:studyId' where pk_report=127;
		End if;	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=281;
    
		IF(row_check>0) then
			Update er_report set rep_filterkeyword='studyId' where pk_report=281;
		End if;
		
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=98;
    
		IF(row_check>0) then
			Update er_report set rep_type='rep_spr' where pk_report=98;
		End if;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=288;
		
		IF(row_check>0) then
			update_sql:='select STUDY_NUMBER,
						SITE_NAME,
						(select distinct PATPROT_PATSTDID from ER_PATPROT where fk_per = patient_pk and fk_study = study_pk and patprot_stat=1) as pat_studyid,
						STUDY_TAREA,
						STUDY_DISEASE_SITE,
						AE_TYPE,
						AE_DESC,
						TO_CHAR(AE_STDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_STDATE,
						TO_CHAR(AE_ENDDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_ENDDATE,
						AE_GRADE,
						AE_NAME,
						MEDDRA,
						DICTIONARY,
						FK_STUDY,
						PATIENT_PK,
						FK_SITE_ENROLLING
						from VDA.VDA_V_AE_ALL_BYSITE
						WHERE fk_site_enrolling IN (:orgId)
						AND TRUNC(AE_STDATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
						AND fk_study IN (:studyId)';
		
			Update er_report set rep_sql_clob=update_sql where pk_report=288;
		End if;
	SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=256;

		IF (row_check > 0) then
			update_sql:='select
 inv_num,
study_number,
milestone_type,
msach_milestone_desc,
fk_site_enrolling,
enrolling_site,
msach_ptstudy_id,
TO_CHAR(msach_ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) msach_ach_date,
milestone_amt,
payment_type,
payment_for,
fk_study,
fk_milestone,
inv_amt,
rec_amt,
payment_dt
from VDA.vda_v_milestone_achvd_det
WHERE fk_study IN (:studyId)
AND TRUNC(MSACH_ACH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=256;					
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=181;
    
		IF(row_check>0) then
			Update er_report set rep_name='Data Table 3',rep_desc='Data Table 3' where pk_report=181;
		End if;
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=182;
    
		IF(row_check>0) then
			Update er_report set rep_name='Data Table 4',rep_desc='Data Table 4' where pk_report=182;
		End if;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report = 254;
		
		IF (row_check > 0) then	
		  
			UPDATE 
				ER_REPORT 
			SET 
				REP_COLUMNS='Study Number, Organization, Local Accrual Goal, Accrual To Date, Accrual Previous Year, Accrual Selected Year, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec'		
			WHERE 				
				pk_report = 254;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report = 121;
		
		IF (row_check > 0) then	
		  
			UPDATE 
				ER_REPORT 
			SET 
				REP_COLUMNS='Study Number, Accrual Goals, Accrual To Date, Accrual Previous Year, Accrual Selected Year, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec'		
			WHERE 				
				pk_report=121;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=279;

		IF (row_check > 0) then
		update_sql:='select
fk_study,
study_number,
enrolling_site_name,
pat_study_id,
calendar_name,
calendar_stat,
visit_name,
evrec_event,
evrec_resource,
TO_CHAR(ptsch_actsch_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ptsch_actsch_date,
ptsch_event_stat,
duration_minutes,
to_char(duration_minutes/60,''9999999999.99'') as duration_hours
from VDA.vda_v_studycal_resource_bypat where ptsch_event_stat_subtype = ''ev_done''
AND  TRUNC(ptsch_actsch_date) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
and fk_study IN (:studyId) ';
UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=279;					
		END IF;
     END IF;		
	
	COMMIT;
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,18,'18_reportUpdate.sql',sysdate,'v10.1 #783');

commit;