DECLARE 
	table_check number;
	row_check number;
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE rep_name = 'Study Patients Demographics' AND
			  pk_report = 294;
		
		IF (row_check > 0) then	
		  
			UPDATE 
				ER_REPORT 
			SET 
				REP_COLUMNS='Study Number, Enrolling Site, Enrolled On, Patient Study ID, Gender, Marital Status, State, Country, Primary Ethnicity, Primary Race, Survival Status'		
			WHERE 				
				pk_report=294 AND 
				rep_name = 'Study Patients Demographics';
		END IF;
		
	END IF;		
	
	COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,38,'38_update_erReport294.sql',sysdate,'v10.1 #783');

commit;
