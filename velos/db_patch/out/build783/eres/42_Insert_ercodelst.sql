set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_0' and CODELST_SUBTYP= 'radiouser1';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_0','radiouser1','Radio User Data1','N',9,'radio','{radioArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}]}');
 end if;



  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_1' and CODELST_SUBTYP= 'radiouser2';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_1','radiouser2','Radio User Data2','N',9,'radio','{radioArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}]}');
end if;



  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_2' and CODELST_SUBTYP= 'radiouser3';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_2','radiouser3','Radio User Data3','N',9,'radio','{radioArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}]}');
  end if; 



  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_3' and CODELST_SUBTYP= 'radiouser4';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_3','radiouser4','Radio User Data4','N',9,'radio','{radioArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}]}');
  
  end if; 



  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_4' and CODELST_SUBTYP= 'radiouser5';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_4','radiouser5','Radio User Data5','N',9,'radio','{radioArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}]}');
  
  end if;



  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_5' and CODELST_SUBTYP= 'radiouser6';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_5','radiouser6','Radio User Data6','N',9,'radio','{radioArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}]}');
  
  end if;

  

  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_6' and CODELST_SUBTYP= 'radiouser7';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_6','radiouser7','Radio User Data7','N',9,'radio','{radioArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}]}');
  
  end if;

  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_6' and CODELST_SUBTYP= 'radiouser7';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_6','radiouser7','Radio User Data7','N',9,'radio','{radioArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}]}');
  
  end if;

  
 

  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='org_0' and CODELST_SUBTYP= 'radioOrg1';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'org_0','radioOrg1','Radio Org Data1','N',9,'radio','{radioArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}]}');
  
  end if;

  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='org_1' and CODELST_SUBTYP= 'radioOrg2';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'org_1','radioOrg2','Radio Org Data2','N',9,'radio','{radioArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}]}');
  
  end if;

  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='org_2' and CODELST_SUBTYP= 'radioOrg3';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'org_2','radioOrg3','Radio Org Data3','N',9,'radio','{radioArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}]}');
  
  end if;

  
 

  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='org_3' and CODELST_SUBTYP= 'radioOrg4';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'org_3','radioOrg4','Radio Org Data4','N',9,'radio','{radioArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}]}');

  end if;

  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='org_4' and CODELST_SUBTYP= 'radioOrg5';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'org_4','radioOrg5','Radio Org Data5','N',9,'radio','{radioArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}]}');
 
  end if;
  
 
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='org_5' and CODELST_SUBTYP= 'radioOrg6';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'org_5','radioOrg6','Radio Org Data6','N',9,'radio','{radioArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}]}');
 
  end if;
  
 
  
 

  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='org_6' and CODELST_SUBTYP= 'radioOrg7';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'org_6','radioOrg7','Radio Org Data7','N',9,'radio','{radioArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}]}');
 
  end if;
  
 
  
 

  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_0' and CODELST_SUBTYP= 'lookupuser1';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_0','lookupuser1','lookup User Data1','N',7,'lookup','{lookupPK:3, selection:"multi", mapping:[{source:"VELSTUDY_NUM", target:"alternateId"},{source:"VELPK|[VELHIDE]", target:"alternateId"}]}');
 
  end if;
  
 
  
 

  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_1' and CODELST_SUBTYP= 'lookupuser2';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_1','lookupuser2','lookup User Data2','N',7,'lookup','{lookupPK:3, selection:"multi", mapping:[{source:"VELSTUDY_NUM", target:"alternateId"},{source:"VELPK|[VELHIDE]", target:"alternateId"}]}');
 
  end if;
  
 
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_2' and CODELST_SUBTYP= 'lookupuser3';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_2','lookupuser3','lookup User Data3','N',7,'lookup','{lookupPK:3, selection:"multi", mapping:[{source:"VELSTUDY_NUM", target:"alternateId"},{source:"VELPK|[VELHIDE]", target:"alternateId"}]}');
 
  end if;
  
    
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_3' and CODELST_SUBTYP= 'lookupuser4';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_3','lookupuser4','lookup User Data4','N',7,'lookup','{lookupPK:3, selection:"multi", mapping:[{source:"VELSTUDY_NUM", target:"alternateId"},{source:"VELPK|[VELHIDE]", target:"alternateId"}]}');
 
  end if;
  
      
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_4' and CODELST_SUBTYP= 'lookupuser5';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_4','lookupuser5','lookup User Data5','N',7,'lookup','{lookupPK:3, selection:"multi", mapping:[{source:"VELSTUDY_NUM", target:"alternateId"},{source:"VELPK|[VELHIDE]", target:"alternateId"}]}');
 
  end if;
  
       
  
 

  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_4' and CODELST_SUBTYP= 'lookupuser5';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_4','lookupuser5','lookup User Data5','N',7,'lookup','{lookupPK:3, selection:"multi", mapping:[{source:"VELSTUDY_NUM", target:"alternateId"},{source:"VELPK|[VELHIDE]", target:"alternateId"}]}');
 
  end if;
  
  
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_5' and CODELST_SUBTYP= 'lookupuser6';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_5','lookupuser6','lookup User Data6','N',7,'lookup','{lookupPK:3, selection:"multi", mapping:[{source:"VELSTUDY_NUM", target:"alternateId"},{source:"VELPK|[VELHIDE]", target:"alternateId"}]}');
  
  end if;
  
  
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_6' and CODELST_SUBTYP= 'lookupuser7';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_6','lookupuser7','lookup User Data7','N',7,'lookup','{lookupPK:3, selection:"multi", mapping:[{source:"VELSTUDY_NUM", target:"alternateId"},{source:"VELPK|[VELHIDE]", target:"alternateId"}]}');
  
  end if;
  
   
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='org_0' and CODELST_SUBTYP= 'lookupOrg1';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'org_0','lookupOrg1','lookup Org Data1','N',7,'lookup','{lookupPK:3, selection:"multi", mapping:[{source:"VELSTUDY_NUM", target:"alternateId"},{source:"VELPK|[VELHIDE]", target:"alternateId"}]}');
 
  end if;
  
  
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='org_1' and CODELST_SUBTYP= 'lookupOrg2';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'org_1','lookupOrg2','lookup Org Data2','N',7,'lookup','{lookupPK:3, selection:"multi", mapping:[{source:"VELSTUDY_NUM", target:"alternateId"},{source:"VELPK|[VELHIDE]", target:"alternateId"}]}');
 
  end if;
  
  
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='org_2' and CODELST_SUBTYP= 'lookupOrg3';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'org_2','lookupOrg3','lookup Org Data3','N',7,'lookup','{lookupPK:3, selection:"multi", mapping:[{source:"VELSTUDY_NUM", target:"alternateId"},{source:"VELPK|[VELHIDE]", target:"alternateId"}]}');
  
  end if;
  
  
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='org_3' and CODELST_SUBTYP= 'lookupOrg4';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'org_3','lookupOrg4','lookup Org Data4','N',7,'lookup','{lookupPK:3, selection:"multi", mapping:[{source:"VELSTUDY_NUM", target:"alternateId"},{source:"VELPK|[VELHIDE]", target:"alternateId"}]}');
 
  end if;
  
 
  
 

  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='org_4' and CODELST_SUBTYP= 'lookupOrg5';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'org_4','lookupOrg5','lookup Org Data5','N',7,'lookup','{lookupPK:3, selection:"multi", mapping:[{source:"VELSTUDY_NUM", target:"alternateId"},{source:"VELPK|[VELHIDE]", target:"alternateId"}]}');
 
  end if;
  
  
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='org_5' and CODELST_SUBTYP= 'lookupOrg6';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'org_5','lookupOrg6','lookup Org Data6','N',7,'lookup','{lookupPK:3, selection:"multi", mapping:[{source:"VELSTUDY_NUM", target:"alternateId"},{source:"VELPK|[VELHIDE]", target:"alternateId"}]}');
 
  end if;
  
   
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='org_6' and CODELST_SUBTYP= 'lookupOrg7';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'org_6','lookupOrg7','lookup Org Data7','N',7,'lookup','{lookupPK:3, selection:"multi", mapping:[{source:"VELSTUDY_NUM", target:"alternateId"},{source:"VELPK|[VELHIDE]", target:"alternateId"}]}');
 
  end if;
  
   
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='org_0' and CODELST_SUBTYP= 'dateOrg1';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'org_0','dateOrg1','Date Org Data1','N',8,'date','');
 
  end if;
  
  
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='org_1' and CODELST_SUBTYP= 'dateOrg2';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'org_1','dateOrg2','Date Org Data2','N',8,'date','');
 
  end if;
  
   
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='org_2' and CODELST_SUBTYP= 'dateOrg3';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'org_2','dateOrg3','Date Org Data3','N',8,'date','');
  
  end if;
  
  
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='org_3' and CODELST_SUBTYP= 'dateOrg4';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'org_3','dateOrg4','Date Org Data4','N',8,'date','');
 
  end if;
  
    
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='org_4' and CODELST_SUBTYP= 'dateOrg5';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'org_4','dateOrg5','Date Org Data5','N',8,'date','');
 
  end if;
  
  
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='org_5' and CODELST_SUBTYP= 'dateOrg6';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'org_5','dateOrg6','Date Org Data6','N',8,'date','');
  
  end if;
  
     
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='org_6' and CODELST_SUBTYP= 'dateOrg7';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'org_6','dateOrg7','Date Org Data7','N',8,'date','');
  
  end if;
  
  
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_0' and CODELST_SUBTYP= 'dateuser1';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_0','dateuser1','Date User Data1','N',8,'date','');
 
  end if;
  
  
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_1' and CODELST_SUBTYP= 'dateuser2';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_1','dateuser2','Date User Data2','N',8,'date','');
 
  end if;
  
  
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_2' and CODELST_SUBTYP= 'dateuser3';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_2','dateuser3','Date User Data3','N',8,'date','');
 
  end if;
  
  
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_3' and CODELST_SUBTYP= 'dateuser4';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_3','dateuser4','Date User Data4','N',8,'date','');
 
  end if;
  
  
  
 

  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_4' and CODELST_SUBTYP= 'dateuser5';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_4','dateuser5','Date User Data5','N',8,'date','');
 
  end if;
  
  
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_5' and CODELST_SUBTYP= 'dateuser6';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_5','dateuser6','Date User Data6','N',8,'date','');

  end if;
  
 
  


  v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='user_6' and CODELST_SUBTYP= 'dateuser7';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'user_6','dateuser7','Date User Data7','N',8,'date','');
  end if;
  
 end;
/

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,42,'42_Insert_ercodelst.sql',sysdate,'v10.1 #783');

commit;
