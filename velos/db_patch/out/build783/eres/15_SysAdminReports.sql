DECLARE 
	table_check number;
	row_check number;
	update_sql clob;

BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=296;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterkeyword
				,rep_filterapplicable
				)
			VALUES(
				 296
				,'Report Access Audit Log'
				,'Report Access Audit Log'
				,0
				,'N'
				,'Module Name, Report Name, Format, Downloaded?, User, Access Date, IP Address'
				,'Date (Access Date)'
				,1
				,'rep_gar'
				,''
				,'[DATEFILTER]MD:studyId:orgId'
				,'date'
				);
				
			update_sql:='select
USR_RPT_LOG_MODNAME,
USR_RPT_LOG_REP_NAME,
USR_RPT_LOG_FORMAT,
USR_RPT_LOG_DOWNLOAD_FLAG,
USER_NAME,
TO_CHAR(USR_RPT_LOG_ACCESSTIME,
 PKG_DATEUTIL.F_GET_DATEFORMAT) USR_RPT_LOG_ACCESSTIME,
IP_ADD
from vda.vda_v_audit_repdownload_log
where TRUNC(USR_RPT_LOG_ACCESSTIME) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=296;				
		END IF;
        
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=4;
		IF (row_check > 0) then
		
		update_sql:='select  USR_LASTNAME, USR_FIRSTNAME, add_phone, add_email,
f_get_usersites(pk_user) AS ORGANIZATION,
 ( SELECT codelst_desc  FROM  ER_CODELST WHERE pk_codelst = FK_CODELST_JOBTYPE) AS jobType,
 ( SELECT codelst_desc  FROM  ER_CODELST WHERE pk_codelst = fk_codelst_spl) AS primary_spl,
f_get_usergrp(pk_user) AS user_groups
 FROM  ER_USER, ER_ADD WHERE  fk_account =~1
 AND  usr_stat = ''A'' AND  fk_peradd = pk_add AND usr_type = ''S'' ORDER
 BY USR_FIRSTNAME,USR_LASTNAME ';
   UPDATE ER_REPORT SET REP_SQL=update_sql WHERE pk_report=4;
	END IF;
	
	SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=120;
		IF (row_check > 0) then

   UPDATE ER_REPORT SET REP_TYPE='rep_gar' WHERE pk_report=120;
	END IF;
 END IF;
 END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,15,'15_SysAdminReports.sql',sysdate,'v10.1 #783');

commit;