set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN	
  Select count(*) into v_record_exists  from SCH_MSGTXT where MSGTXT_TYPE='irb_subtxt';
	if (v_record_exists > 0) then
		UPDATE SCH_MSGTXT 
		set MSGTXT_LONG ='<b>Study#: {VEL_STUDYNUMBER}</b><BR><b>Study Title: {VEL_STUDYTITLE}</b><BR><b>Version: {VEL_STUDYVER}</b><BR><b>Principal Investigator: {VEL_PI}</b><BR><BR><BR><b>Committee Discussion:</b><BR><BR><BR><b>Outcome:</b><BR><BR><BR>'
		where MSGTXT_TYPE='irb_subtxt';
	end if;
	Commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,1,'01_sch_msgtxt_db_patch.sql',sysdate,'v10.1 #783');

commit;
