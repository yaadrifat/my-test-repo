create or replace TRIGGER "ESCH"."SCH_DOCS_AU0" 
AFTER UPDATE
ON SCH_DOCS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);
  oldusr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);
  oldusr := getuser(:NEW.creator);
  if usr = 'ESCH' then
      usr := oldusr;
  end if;

  audit_trail.record_transaction
    (raid, 'SCH_DOCS', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_docs,0) !=
      NVL(:NEW.pk_docs,0) THEN
      audit_trail.column_update
        (raid, 'PK_DOCS',
        :OLD.pk_docs, :NEW.pk_docs);
   END IF;
   IF NVL(:OLD.doc_name,' ') !=
      NVL(:NEW.doc_name,' ') THEN
      audit_trail.column_update
        (raid, 'DOC_NAME',
        :OLD.doc_name, :NEW.doc_name);
   END IF;
   IF NVL(:OLD.doc_desc,' ') !=
      NVL(:NEW.doc_desc,' ') THEN
      audit_trail.column_update
        (raid, 'DOC_DESC',
        :OLD.doc_desc, :NEW.doc_desc);
   END IF;
   IF NVL(:OLD.doc_type,' ') !=
      NVL(:NEW.doc_type,' ') THEN
      audit_trail.column_update
        (raid, 'DOC_TYPE',
        :OLD.doc_type, :NEW.doc_type);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
        to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
        to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;
   IF NVL(:OLD.doc_size,0) !=
      NVL(:NEW.doc_size,0) THEN
      audit_trail.column_update
        (raid, 'DOC_SIZE',
        :OLD.doc_size, :NEW.doc_size);
   END IF;

END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,432,1,'01_SCH_DOCS_AU0.sql',sysdate,'v11 #833');

commit;	
/