declare
v_column_exists number:=0;
begin
select count(*) into v_column_exists from user_tab_cols where column_name='SUBMIT_TO_BOARD' and table_name='ER_SUBMISSION';
if v_column_exists=0 then
execute immediate ('ALTER TABLE ER_SUBMISSION ADD SUBMIT_TO_BOARD VARCHAR2(500)');
end if;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,387,7,'07_alert_submission.sql',sysdate,'v11 #787');

commit;