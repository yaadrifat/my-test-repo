declare
v_column_exists number:=0;
begin
select count(*) into v_column_exists from user_tab_cols where column_name='SUBMIT_TO_BOARD' and table_name='ER_SUBMISSION';
if v_column_exists=0 then
execute immediate ('ALTER TABLE er_per MODIFY per_code varchar2(100)');
end if;
end;
/
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,387,2,'02_er_per_alter.sql',sysdate,'v11 #787');

commit;
