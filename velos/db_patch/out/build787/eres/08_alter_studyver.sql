alter table er_studyver modify (studyver_status varchar2(15));
comment on column er_studyver.studyver_status IS 'This column stores the codelist subtype value of Version status';


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,387,8,'08_alter_studyver.sql',sysdate,'v11 #787');

commit;

