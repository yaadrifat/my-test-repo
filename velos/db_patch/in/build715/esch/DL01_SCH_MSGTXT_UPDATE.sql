Set Define off;

declare
rowExists number := 0;
begin
	select count(*) into rowExists from SCH_MSGTXT where msgtxt_type = 'user_reset';
	if (rowExists != 0) then
		Update SCH_MSGTXT set MSGTXT_SUBJECT = 'Request for change of password/eSign' where msgtxt_type = 'user_reset';
	end if;
	COMMIT;
end;
/