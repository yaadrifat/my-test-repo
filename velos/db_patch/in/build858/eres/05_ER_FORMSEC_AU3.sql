SET DEFINE OFF;
create or replace TRIGGER "ERES"."ER_FORMSEC_AU3" 
AFTER UPDATE OF FORMSEC_SEQ,FORMSEC_NAME
ON ER_FORMSEC
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
v_formlib_id Number ;

Begin

v_formlib_id := :new.fk_formlib ;

update er_formlib
set FORM_XSLREFRESH  = 1, LAST_MODIFIED_BY =:new.LAST_MODIFIED_BY
where pk_formlib = v_formlib_id    ;

END ;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,457,5,'05_ER_FORMSEC_AU3.sql',sysdate,'v11.1.0 #858');
commit;
/