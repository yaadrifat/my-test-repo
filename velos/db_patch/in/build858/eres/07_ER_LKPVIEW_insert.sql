SET DEFINE OFF;

DECLARE
  V_PK_LKPLIB NUMBER :=0;
  v_cnt       NUMBER :=0;
  v_LKPCOL NUMBER :=0;
  v_FK_LKPVIEW NUMBER :=0;
  v_max_cnt NUMBER :=0;
  v_seq NUMBER :=0;
BEGIN
 BEGIN
 select PK_LKPLIB into V_PK_LKPLIB from ER_LKPLIB where LKPTYPE_NAME='dynReports' and LKPTYPE_TYPE='dyn_s' and LKPTYPE_DESC='Study Summary';

 exception when NO_DATA_FOUND then
      V_PK_LKPLIB := 0;
 end; 
	SELECT SEQ_ER_LKPLIB.nextval INTO v_seq FROM dual; 

	SELECT COUNT(*) into v_cnt FROM ER_LKPVIEW WHERE LKPVIEW_KEYWORD='studyPatLookup';

	IF (v_cnt=0) THEN
	Insert into ER_LKPVIEW (PK_LKPVIEW,LKPVIEW_NAME,LKPVIEW_DESC,FK_LKPLIB,LKPVIEW_FILTER,LKPVIEW_KEYWORD) values (v_seq,'Study Lookup','Study Lookup for reports',V_PK_LKPLIB,'erv_study_dyn.fk_account= [:ACCID] AND (Pkg_Util.f_checkStudyTeamRight(pk_study,[:USERID],''STUDYSUM'')) = 1 AND (Pkg_Util.f_checkStudyTeamRight(pk_study,[:USERID],''STUDYMPAT'')) = 1','studyPatLookup');
	END IF;

	
	SELECT COUNT(*) INTO v_cnt FROM ER_LKPVIEWCOL WHERE FK_LKPVIEW in (select PK_LKPVIEW from  ER_LKPVIEW WHERE LKPVIEW_KEYWORD='studyPatLookup');

	IF (v_cnt=0) THEN
	Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select min(pk_lkpcol) from er_lkpcol where LKPCOL_KEYWORD='STUDY_NUMBER' and fk_lkplib=6002),'Y',1,v_seq,'50%','Y');
	Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select min(pk_lkpcol) from er_lkpcol where LKPCOL_KEYWORD='STUDY_TITLE' and fk_lkplib=6002),'Y',1,v_seq,'50%','Y');
	Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select min(pk_lkpcol) from er_lkpcol where LKPCOL_KEYWORD='LKP_PK' and fk_lkplib=6002),'Y',2,v_seq,'5%','N');
	Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select min(pk_lkpcol) from er_lkpcol where LKPCOL_KEYWORD='STUDY_DIVISION' and fk_lkplib=6002),'Y',3,v_seq,'30%','Y');
	Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select min(pk_lkpcol) from er_lkpcol where LKPCOL_KEYWORD='FK_CODELST_TAREA' and fk_lkplib=6002),'Y',4,v_seq,'30%','Y');
	END IF;

	commit; 
	
	SELECT COUNT(*) into v_cnt FROM ER_LKPVIEW WHERE LKPVIEW_KEYWORD='studyLookup';
	IF (v_cnt>0) THEN
	update ER_LKPVIEW set LKPVIEW_FILTER='erv_study_dyn.fk_account= [:ACCID] AND (Pkg_Util.f_checkStudyTeamRight(pk_study,[:USERID],''STUDYSUM'')) = 1' where LKPVIEW_KEYWORD='studyLookup';
	commit;
	END IF;
	
	SELECT COUNT(*) into v_cnt FROM ER_LKPVIEW WHERE LKPVIEW_KEYWORD='userProfile';
	IF (v_cnt>0) THEN
	update ER_LKPVIEW set LKPVIEW_FILTER='usr_account=[:ACCID] and usr_type <> ''X'' and user_hidden <> 1 and site_hidden <> 1' where LKPVIEW_KEYWORD='userProfile';
	commit;
	END IF;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,457,7,'07_ER_LKPVIEW_insert.sql',sysdate,'v11.1.0 #858');
commit;
/