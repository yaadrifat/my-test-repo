set define off;
DECLARE
  v_record_exists NUMBER ;
  v_repfiltermap_colomn CLOB;
  v_repId Number;
  v_lkpId Number;
BEGIN
  select PK_REPFILTER into v_repId from ER_REPFILTER where REPFILTER_KEYWORD='studyId';
  SELECT COUNT(*)
  INTO v_record_exists
  FROM er_repfiltermap
  WHERE repfiltermap_repcat='patient' and FK_REPFILTER=v_repId;
  select PK_LKPVIEW into v_lkpId from ER_LKPVIEW where LKPVIEW_KEYWORD='studyPatLookup';
  
  IF (v_record_exists > 0) THEN
  v_repfiltermap_colomn:='<td><DIV id="studyDIV"><Font class=comments><A href="javascript:openLookup(document.reports,''viewId='||v_lkpId||'&form=reports&seperator=,&defaultvalue=[ALL]&keyword=selstudyId|STUDY_NUMBER~paramstudyId|LKP_PK|[VELHIDE]'','''')">Select Study</Font></DIV></td><td><DIV id="studydataDIV"> <input TYPE="text" NAME="selstudyId" readonly value="[ALL]"><input TYPE="hidden" NAME="paramstudyId" value="[ALL]"></DIV></td>';
    update er_repfiltermap set REPFILTERMAP_COLUMN=v_repfiltermap_colomn,REPFILTERMAP_VALUESQL=' SELECT pk_study FROM ER_STUDY  WHERE  (Pkg_Util.f_checkStudyTeamRight(pk_study,:sessUserId,''STUDYSUM'')) = 1 and  (Pkg_Util.f_checkStudyTeamRight(pk_study,:sessUserId,''STUDYMPAT'')) = 1' where repfiltermap_repcat='patient' and FK_REPFILTER=v_repId;
  END IF;
 commit; 
 
 select PK_REPFILTER into v_repId from er_repfilter where REPFILTER_KEYWORD = 'studyId';
	IF (v_repId != 0) THEN
	UPDATE er_repfilter set REPFILTER_VALUESQL = 'SELECT pk_study FROM ER_STUDY WHERE  (Pkg_Util.f_checkStudyTeamRight(pk_study,:sessUserId,''STUDYSUM'')) = 1'  WHERE PK_REPFILTER = v_repId;
	END IF;
	COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,457,8,'08_er_repfiltermap_update.sql',sysdate,'v11.1.0 #858');
commit;
/