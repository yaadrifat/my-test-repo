set define off;
declare
begin
delete  from  er_repxsl where pk_repxsl in (115,259,260);
commit;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,384,2,'02_er_repxsl.sql',sysdate,'v10.1 #785');

commit;