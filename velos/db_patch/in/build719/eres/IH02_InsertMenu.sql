set define off;
set serveroutput on;

DECLARE
  v_item_exists number := 0;  
BEGIN

select count(*) into v_item_exists from ER_OBJECT_SETTINGS where OBJECT_NAME = 'study_menu' and OBJECT_SUBTYPE = 'new_subm_menu';
if (v_item_exists = 0) then
 Insert into ERES.ER_OBJECT_SETTINGS
   (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
    OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
 Values
   (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'new_subm_menu', 'study_menu', 1, 
    1, 'New Submission', 0);
  commit;
  dbms_output.put_line('One row inserted');
else
  dbms_output.put_line('Row already exists');
end if;

end;
/
