delete from er_browserconf where fk_browser =(select pk_browser from er_browser where browser_module='irbOngoing') and browserconf_colname='AMD_LINK';
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,381,12,'12_delete_amd_column.sql',sysdate,'v10.1 #782');

commit;