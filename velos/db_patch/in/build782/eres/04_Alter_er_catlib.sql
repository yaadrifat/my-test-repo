set define off;
 
DECLARE
	table_check number;
	column_check number;
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_CATLIB';
	
	
	IF (table_check > 0) then
		SELECT COUNT(column_name)
		into column_check
		FROM user_tab_columns
		WHERE table_name='ER_CATLIB'
		AND COLUMN_NAME ='CATLIB_SUBTYPE';
		
		IF (column_check > 0) then	
			--alter table ER_CATLIB modify CATLIB_SUBTYPE VARCHAR2(200 BYTE);
			execute immediate 'alter table ERES.ER_CATLIB modify CATLIB_SUBTYPE VARCHAR2(200 BYTE)';
		END IF;
	
	END IF;

	COMMIT;
	
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,381,4,'04_Alter_er_catlib.sql',sysdate,'v10.1 #782');

commit;
