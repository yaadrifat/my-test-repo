/////*********This readMe is specific to v10.1 build #782	  **********////

The following BUGS  ARE released in the build:

1. eC-40280	Final Outcome Changes   (with latest templete)
2. eC-39891: Protocol Locking & Unlocking
6. Regular bug fixes 88 items.




ITEMS COVERED IN THIS BUILD FOR DEMO PURPOSE AS REQUESTED BY PM TEAM BUT NOT AS OFFICIAL RELEASE:
--------------------------------------------------------------------------------------------------------------------
1.Organization  More details implementation  for each network item.
2. Adding User  to network level and user More details implementation. (User Icon)
3. Document upload facility on network level (Doc Icon)

NOTE:
1. THE REQUIREMENT DOCUMENTS ARE NOT FINALIZED FOR ANY OF THE ITEMS MENTIONED ABOVE.
2. WE HAVE DEVELOPED THE ABOVE FOUR ITEMS BASED ON INITIAL DOCUMENT AND OUR UNDERSTANDING.
3. WE WILL MAKE AN OFFICIAL RELEASE ALL THESE ITEMS ONCE THE REQUIREMENT DOCUMENTS ARE FINALIZED BY PM TEAM.
4. HENCE THESE ITEMS ARE  NOT FOR QA TESTING. 