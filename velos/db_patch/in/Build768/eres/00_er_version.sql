set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v10 #768' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,367,0,'00_er_version.sql',sysdate,'v10 #768');

commit;

