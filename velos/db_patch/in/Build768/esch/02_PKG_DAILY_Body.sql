create or replace
PACKAGE BODY        "PKG_DAILY"
AS

PROCEDURE SP_PASTSCHNOTIFY
AS

/* author : sonia sahni
   date: 12 dec 2001
*/

v_msgsql VARCHAR2(2000) ;
v_notdone NUMBER;

v_alcode NUMBER;

v_patprot NUMBER;
v_alnot_users VARCHAR2(1000);
v_alnot_mobeve VARCHAR2(2000);
v_user_str VARCHAR2(1000);
v_params VARCHAR2(1000);
v_pk_schevent CHAR(10);
v_msg VARCHAR2(4000);

v_cnt NUMBER ;
V_POS		NUMBER :=0 ;

v_patcode VARCHAR2(25);
v_fparam VARCHAR2(4000);
v_eventname VARCHAR2(2000);
v_schdate VARCHAR2(12);
v_visit NUMBER;
v_msgtemplate VARCHAR2(4000);
v_pkalnot NUMBER;

v_cellmsgtemplate VARCHAR2(4000);
v_cellmsg VARCHAR2(4000);

v_msgsendon DATE;
v_sch_eventid CHAR(10);
--v_studynum VARCHAR2(20);
v_studynum VARCHAR2(100);--JM

v_visitname VARCHAR2(200);
/* declare cursor to get past schd notification setting for all current schedules
and respective users, emails.
*/


BEGIN

/*get code for event status 'Not Done'*/
   SELECT pk_codelst
   INTO v_notdone
   FROM SCH_CODELST
   WHERE codelst_subtyp = 'ev_notdone' AND codelst_type = 'eventstatus' ;


  /*get code for alert code for 'send notification for past scheduled date*/

   SELECT pk_codelst
   INTO v_alcode
   FROM SCH_CODELST
   WHERE codelst_subtyp = 'al_pastsch' AND codelst_type = 'sch_alert'   ;


  /* select msgtxt_short
   into v_msg
   from sch_msgtxt
   where msgtxt_type = 'al_pastsch' ;*/

  -- v_msgtemplate  :=    PKG_COMMON.SCH_GETMAILMSG('al_pastsch');

   v_msgtemplate  :=    Pkg_Common.SCH_GETMAILMSG('c_pastsch');

   v_cellmsgtemplate := Pkg_Common.SCH_GETCELLMAILMSG('al_pastsch');

-- 		modified by gopu for fix the Bugzilla Issue #2096
-- Modified by Manimaran to fix Bug #2545.
-- JM: 11Jan2007: Bugzilla issue #2802: CODELST_SUBTYP='lockdown' should replace CODELST_SUBTYP='offstudy'

  FOR  i IN   (SELECT   al.FK_PATPROT,  al.ALNOT_USERS, al.ALNOT_MOBEVE,
               Sch.event_id,Sch.description,TO_CHAR(Sch.ACTUAL_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) ACTUAL_SCHDATE, Sch.visit,
			al.PK_ALNOT, Sch.ACTUAL_SCHDATE + 1 sendondt , spv.VISIT_NAME
               FROM SCH_ALERTNOTIFY al, ER_PATPROT ep, SCH_EVENTS1 Sch,SCH_PROTOCOL_VISIT spv
               WHERE ep.pk_patprot = al.FK_PATPROT AND spv.PK_PROTOCOL_VISIT = Sch.FK_VISIT AND PATPROT_STAT = 1

	       --AND PATPROT_ENROLDT IS NOT NULL
	       AND (SELECT FK_CODELST_STAT FROM ER_PATSTUDYSTAT WHERE FK_PER=EP.FK_PER AND FK_STUDY= EP.FK_STUDY AND PK_PATSTUDYSTAT=(SELECT MAX(PK_PATSTUDYSTAT) FROM ER_PATSTUDYSTAT WHERE FK_PER=EP.FK_PER AND FK_STUDY= EP.FK_STUDY ) )!= (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_SUBTYP='offstudy' AND CODELST_TYPE='patStatus')
	       AND al.FK_CODELST_AN = v_alcode AND
               al.ALNOT_FLAG = 1 AND
               al.ALNOT_TYPE = 'A' AND
               Sch.fk_patprot = al.fk_patprot AND
               Sch.isconfirmed = v_notdone  AND
               TRUNC(Sch.ACTUAL_SCHDATE + 1) = TRUNC(Pkg_Tz.f_getmaxtz(al.ALNOT_USERS)) AND
               al.ALNOT_USERS IS NOT NULL AND
               Sch.alnot_sent = 0
			UNION ALL
               SELECT   al.FK_PATPROT,  al.ALNOT_USERS, al.ALNOT_MOBEVE,
               Sch.event_id,Sch.description,TO_CHAR(Sch.ACTUAL_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) ACTUAL_SCHDATE,
			Sch.visit, al.PK_ALNOT , Sch.ACTUAL_SCHDATE + 1 sendondt , spv.VISIT_NAME
               FROM SCH_ALERTNOTIFY al, ER_PATPROT ep, SCH_EVENTS1 Sch,SCH_PROTOCOL_VISIT spv
               WHERE ep.pk_patprot = al.FK_PATPROT AND spv.PK_PROTOCOL_VISIT = Sch.FK_VISIT AND PATPROT_STAT = 1
	       --AND PATPROT_ENROLDT IS NOT NULL
	       AND (SELECT FK_CODELST_STAT FROM ER_PATSTUDYSTAT WHERE FK_PER=EP.FK_PER AND FK_STUDY= EP.FK_STUDY AND PK_PATSTUDYSTAT=(SELECT MAX(PK_PATSTUDYSTAT) FROM ER_PATSTUDYSTAT WHERE FK_PER=EP.FK_PER AND FK_STUDY= EP.FK_STUDY )) !=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_SUBTYP='offstudy' AND CODELST_TYPE='patStatus')
	       AND al.FK_CODELST_AN = v_alcode AND
               al.ALNOT_FLAG = 1 AND
               al.ALNOT_TYPE = 'A' AND
               Sch.fk_patprot = al.fk_patprot AND
               Sch.isconfirmed = v_notdone   AND
               TRUNC(Sch.ACTUAL_SCHDATE + 1) = TRUNC(SYSDATE) AND
               al.ALNOT_USERS IS NULL AND  al.ALNOT_MOBEVE IS NOT NULL AND
               Sch.alnot_sent = 0
			)
  LOOP
      v_patprot := i.FK_PATPROT;
      v_alnot_users := i.ALNOT_USERS;
      v_alnot_mobeve := i.ALNOT_MOBEVE;
      v_pk_schevent := i.event_id;

      v_user_str    := v_alnot_users || ',' ;
	 v_params := v_alnot_users || ',' ;

      v_eventname := i.description; -- 3rd parameter
      v_schdate := i.ACTUAL_SCHDATE; --4th param
      v_visit := i.visit;

      v_pkalnot :=  i.PK_ALNOT;
	 v_msgsendon := i.sendondt;
      v_sch_eventid :=  i.event_id;

	 SELECT  Pkg_Common.SCH_GETPATCODE(v_patprot)
      INTO v_patcode
	 FROM dual;        -- v_patcode is param 1

	 --get study number

	 SELECT a.STUDY_NUMBER
	 INTO v_studynum
	 FROM er_study a,er_patprot b
	 WHERE b.pk_patprot = v_patprot AND
	 b.fk_study = a.pk_study;

      -- get visit
	 --v_visit := sch_getvisit (v_patprot, v_pk_schevent) ;

--      v_fparam :=  v_studynum  ||'~' || v_patcode ||'~' || NVL(TO_CHAR(v_visit),'Not Known') ||'~'|| v_eventname ||'~'|| v_schdate ;
-- 		modified by gopu for fix the Bugzilla Issue #2096
		IF v_visit = NULL THEN
		   v_visitname :='Not Known';
		ELSE
		   v_visitname := i.visit_name;
		END IF;
	  v_fparam :=  v_studynum  ||'~' || v_patcode ||'~' || v_visitname ||'~'|| v_eventname ||'~'|| v_schdate ;
	 v_msg :=    Pkg_Common.SCH_GETMAIL(v_msgtemplate ,v_fparam);
      v_cellmsg :=    Pkg_Common.SCH_GETMAIL(v_cellmsgtemplate ,v_fparam);


--    	 p('fparam' || v_fparam );

	 --insert record for mobile/pager mail


        IF v_alnot_mobeve IS NOT NULL THEN
--          p('**alnotmobeve' || v_alnot_mobeve);

	   INSERT INTO SCH_DISPATCHMSG (
				PK_MSG                 ,
				MSG_SENDON             ,
				MSG_STATUS             ,
				MSG_TYPE               ,
				FK_SCHEVENT               ,
				MSG_TEXT               ,
				FK_PAT		       ,
				CREATOR                ,
				IP_ADD                 ,
				FK_PATPROT             )
			      VALUES (
			        SCH_DISPATCHMSG_SEQ1.NEXTVAL ,
			   	v_msgsendon,
			 	0,
				'M', --M for mobile :)
				v_pk_schevent ,
				v_cellmsg,
				v_pkalnot,
				NULL,
				NULL,
			     v_patprot) ;
	   END IF;
--    p('**alnotmobeve inserted ' || v_alnot_mobeve);
          LOOP
		    V_CNT := V_CNT + 1;
		    V_POS := INSTR (V_USER_STR, ',');
		    V_PARAMS := SUBSTR (V_USER_STR, 1, V_POS - 1);
		    EXIT WHEN V_PARAMS IS NULL;


              /*
		    chg by sonia sahni -  24 sept 02
		    For consolidated email enhancement
		    mail record will be inserted in sch_msgtran, temp table
		    */

		    INSERT INTO SCH_MSGTRAN (
				PK_MSG                 ,
				MSG_SENDON             ,
				MSG_STATUS             ,
				MSG_TYPE               ,
				FK_SCHEVENT               ,
				MSG_TEXT               ,
				FK_USER		       ,
				FK_PATPROT             )
	         VALUES (
			     SEQ_MSGTRAN.NEXTVAL ,
			   	v_msgsendon,
			 	0,
				'dl_psch',
				v_pk_schevent ,
				v_msg,
				v_params,
			     v_patprot) ;

				V_USER_STR := SUBSTR (V_USER_STR, V_POS + 1);
--			p('DONE');

		END LOOP ; /* end loop for multiple users*/
		UPDATE SCH_EVENTS1
		SET alnot_sent = 1
		WHERE event_id = v_sch_eventid;

  END LOOP;

COMMIT;

END SP_PASTSCHNOTIFY ;


PROCEDURE SP_CRFTRACKING
AS

/* author : sonia sahni
   date: 13 dec 2001
*/

v_msgsql VARCHAR2(2000) ;

v_patprot NUMBER;
v_alnot_users VARCHAR2(1000);
v_alnot_mobeve VARCHAR2(2000);
v_user_str VARCHAR2(1000);
v_params VARCHAR2(1000);
v_pk_schevent CHAR(10);
v_msg VARCHAR2(4000);

v_cnt NUMBER ;
V_POS		NUMBER :=0 ;

v_patcode VARCHAR2(25);
v_sentoname VARCHAR2(100);
v_sentbyname VARCHAR2(100);
v_sentby NUMBER;
v_fparam VARCHAR2(200);
v_eventname VARCHAR2(100);
v_schdate VARCHAR2(12);
v_crfnum VARCHAR2(100);
v_sentondt VARCHAR2(12);
v_visit NUMBER;
v_msgtemplate VARCHAR2(4000);
v_msgtemplatecrf VARCHAR2(4000);
v_msgtemplateother VARCHAR2(4000);
v_formtype VARCHAR(1);

--v_studynum VARCHAR2(20);
v_studynum VARCHAR2(100);--JM:
BEGIN

/*
   select msgtxt_short
   into v_msg
   from sch_msgtxt
   where msgtxt_type = 'al_crftrk' ; */

  -- v_msgtemplatecrf :=    PKG_COMMON.SCH_GETMAILMSG('al_crftrk');
  -- v_msgtemplateother :=    PKG_COMMON.SCH_GETMAILMSG('al_othfrm');

     v_msgtemplatecrf :=    Pkg_Common.SCH_GETMAILMSG('c_crft');
     v_msgtemplateother :=    Pkg_Common.SCH_GETMAILMSG('c_othf');


--   v_msg :=    SCH_GETMAIL(v_msg,'*1*,*2*,*3*,*4*,*5*,*6*,*7*,*8*');


  FOR  i IN   (SELECT  PK_CRF, FK_EVENTS1  ,  CRF_NUMBER , CRF_NAME , FK_CODELST_CRFSTAT,
CRFSTAT_ENTERBY ,CRFSTAT_REVIEWBY  , CRFSTAT_REVIEWON  ,CRFSTAT_SENTTO ,
CRFSTAT_SENTFLAG ,  CRFSTAT_SENTBY , CRFSTAT_SENTON, cd.CODELST_DESC CRFSTAT_DESC,
Sch.FK_PATPROT,Sch.description,TO_CHAR(Sch.ACTUAL_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) ACTUAL_SCHDATE,
Sch.visit, PK_CRFSTAT, crf_flag
  FROM  SCH_CRF crf, SCH_CRFSTAT cstat  , SCH_CODELST cd, SCH_EVENTS1 Sch
 WHERE crf.PK_CRF = cstat.FK_CRF AND
 cstat.CRFSTAT_SENTFLAG = 1 AND
  TRUNC(CRFSTAT_SENTON) = TRUNC(Pkg_Tz.f_getmaxtz(CRFSTAT_SENTTO)) + 1 AND
   Sch.event_id = crf.FK_EVENTS1 AND
   cd.PK_CODELST = cstat.FK_CODELST_CRFSTAT    AND
   cd.codelst_type = 'crfstatus'
  AND  CRFSTAT_SENTTO IS NOT NULL AND crf.crf_flag = 'E'
UNION
SELECT  PK_CRF, NULL AS FK_EVENTS1  ,  CRF_NUMBER , CRF_NAME , FK_CODELST_CRFSTAT,
CRFSTAT_ENTERBY ,CRFSTAT_REVIEWBY  , CRFSTAT_REVIEWON  ,CRFSTAT_SENTTO ,
CRFSTAT_SENTFLAG ,  CRFSTAT_SENTBY , CRFSTAT_SENTON, cd.CODELST_DESC CRFSTAT_DESC,
crf.FK_PATPROT,NULL AS description,NULL AS ACTUAL_SCHDATE,
0 AS visit, PK_CRFSTAT, crf_flag
  FROM  SCH_CRF crf, SCH_CRFSTAT cstat  , SCH_CODELST cd
 WHERE crf.PK_CRF = cstat.FK_CRF AND
 cstat.CRFSTAT_SENTFLAG = 1 AND
  TRUNC(CRFSTAT_SENTON)  = TRUNC(Pkg_Tz.f_getmaxtz(CRFSTAT_SENTTO)) + 1  AND
   cd.PK_CODELST = cstat.FK_CODELST_CRFSTAT    AND
   cd.codelst_type = 'crfstatus'
  AND  CRFSTAT_SENTTO IS NOT NULL AND crf.crf_flag = 'O'
  )
  LOOP

      v_patprot := i.FK_PATPROT;
      v_alnot_users := i.CRFSTAT_SENTTO;
      v_pk_schevent := i.FK_EVENTS1;
      v_user_str    := v_alnot_users || ',' ;
	 v_params := v_alnot_users || ',' ;
      v_sentby := i.CRFSTAT_SENTBY;
      v_eventname := i.description; -- 3rd parameter
      v_schdate := i.ACTUAL_SCHDATE; --4th param
	 v_crfnum := i.CRF_NUMBER; --5th param
      v_visit := i.VISIT;
	 v_formtype := i.crf_flag;
      v_sentondt := i.CRFSTAT_SENTON;

	 --v_visit := sch_getvisit (v_patprot, v_pk_schevent) ; --2 param

	 /*Select  PKG_COMMON.SCH_GETPATCODE(v_patprot), to_char(trunc(sysdate),'mm/dd/yyyy'), usr_lst(v_sentby)
		   into v_patcode,  v_sentondt, v_sentbyname
		   from dual; */

 	  SELECT  Pkg_Common.SCH_GETPATCODE(v_patprot), usr_lst(v_sentby)
	   INTO v_patcode, v_sentbyname
 	   FROM dual;

	   	 --get study number

     	 SELECT a.STUDY_NUMBER
	      INTO v_studynum
     	 FROM er_study a,er_patprot b
	      WHERE b.pk_patprot = v_patprot AND
     	 b.fk_study = a.pk_study;


  --      p('crf ' || i.crf_name) ;
  --    p('fk patprot ' || to_char(i.fk_patprot)) ;
		   -- v_patcode is param 1
   		   -- v_sentbyname is 7thparameter
		   -- v_sentondt is 8th parameter

          LOOP
		    V_CNT := V_CNT + 1;
		    V_POS := INSTR (V_USER_STR, ',');
		    V_PARAMS := SUBSTR (V_USER_STR, 1, V_POS - 1);
		    EXIT WHEN V_PARAMS IS NULL;

           /* calculate parameters */

             SELECT  usr_lst(v_params)
		   INTO v_sentoname
		   FROM dual; -- 6th parameter


		   IF v_formtype = 'O' THEN
		      v_fparam :=  v_studynum  ||'~'|| v_patcode ||'~'|| v_crfnum ||'~'|| v_sentoname ||'~'|| v_sentbyname
                          ||'~' || v_sentondt ;
			 v_msg :=    Pkg_Common.SCH_GETMAIL(v_msgtemplateother,v_fparam);

			 ELSE
                v_fparam :=  v_studynum  ||'~'|| v_patcode ||'~'|| NVL(TO_CHAR(v_visit),'Not Known') || '~'|| v_eventname ||'~'|| v_schdate ||'~'|| v_crfnum ||'~'|| v_sentoname ||'~'|| v_sentbyname
                          ||'~' || v_sentondt ;
  		      v_msg :=    Pkg_Common.SCH_GETMAIL(v_msgtemplatecrf,v_fparam);
		 END IF;



              /*
		    chg by sonia sahni -  24 sept 02
		    For consolidated email enhancement
		    mail record will be inserted in sch_msgtran, temp table
		    */

 			INSERT INTO SCH_MSGTRAN (
				PK_MSG                 ,
				MSG_SENDON             ,
				MSG_STATUS             ,
				MSG_TYPE               ,
				FK_SCHEVENT               ,
				MSG_TEXT               ,
				FK_USER		       ,
				FK_PATPROT             )
			      VALUES (
			      SEQ_MSGTRAN.NEXTVAL ,
   			      i.CRFSTAT_SENTON ,
				 0,
  			 	 'dl_crf',
				v_pk_schevent ,
				v_msg ,
				v_params,
			     v_patprot) ;
			   V_USER_STR := SUBSTR (V_USER_STR, V_POS + 1);
--			p('DONE ' || to_char(v_cnt) );

		END LOOP ; /* end loop for multiple users*/

		-- update crf_sentflag to 2 - for sent mails

		UPDATE SCH_CRFSTAT
		   SET CRFSTAT_SENTFLAG = 2
		WHERE PK_crfstat = i.pk_crfstat ;

  END LOOP;

COMMIT;

END SP_CRFTRACKING;


PROCEDURE SP_CRFTRACKING (P_PKCRFEVSTAT IN   NUMBER,
                          P_FKCRF   IN   NUMBER ,
                          P_CRFSTAT   IN   NUMBER,
                          P_CRFSTATSENTTO         IN VARCHAR2,
                          P_CRFSTATSENTBY          IN   NUMBER,
                          P_CRFSTAT_SENTON         DATE)
AS


/* author : sonia sahni
   date: 13 dec 2001
*/

v_msgsql VARCHAR2(4000) ;

v_patprot NUMBER;
v_alnot_users VARCHAR2(1000);

v_user_str VARCHAR2(1000);
v_params VARCHAR2(1000);
v_pk_schevent CHAR(10);
v_msg VARCHAR2(4000);

v_crfnum VARCHAR2(100);
v_evdate DATE;
v_evname VARCHAR2(200);
v_sentoname VARCHAR2(200);
v_patcode VARCHAR2(25);
v_sentbyname VARCHAR2(100);
v_fparam VARCHAR2(200);
v_sentondt VARCHAR2(24);
v_visit NUMBER;

v_cnt NUMBER ;
V_POS		NUMBER :=0 ;
v_msgtemplate VARCHAR2(4000);
v_formtype VARCHAR2(1);
--v_studynum VARCHAR2(20);
v_studynum VARCHAR2(100);--JM:

BEGIN

SELECT CRF_FLAG, CRF_NUMBER,FK_PATPROT
INTO v_formtype, v_crfnum,v_patprot
FROM SCH_CRF
WHERE  SCH_CRF.PK_CRF = P_FKCRF ;

IF v_formtype = 'E' THEN
 SELECT FK_EVENTS1, ACTUAL_SCHDATE, DESCRIPTION, VISIT
 INTO v_pk_schevent, v_evdate, v_evname, v_visit
 FROM  SCH_CRF, SCH_EVENTS1
 WHERE  SCH_CRF.PK_CRF = P_FKCRF AND
 SCH_CRF.FK_EVENTS1     = SCH_EVENTS1.event_id;
END IF;


             SELECT P.per_code
             INTO v_patcode
             FROM er_patprot e , er_per P
             WHERE e.pk_patprot = v_patprot AND
              P.pk_per = e.fk_per;

--  Select  PKG_COMMON.SCH_GETPATCODE(v_patprot), usr_lst(P_CRFSTATSENTBY)
--		   into v_patcode,  v_sentbyname
--		   from dual;

  SELECT  usr_lst(P_CRFSTATSENTBY)
		   INTO v_sentbyname
		   FROM dual;

	 --get study number

     	 SELECT a.STUDY_NUMBER
	      INTO v_studynum
     	 FROM er_study a,er_patprot b
	      WHERE b.pk_patprot = v_patprot AND
     	 b.fk_study = a.pk_study;



     v_sentondt := P_CRFSTAT_SENTON;


  --get message format

  IF v_formtype = 'O' THEN
     v_msgtemplate :=    Pkg_Common.SCH_GETMAILMSG('al_othfrm');
  ELSE
     v_msgtemplate :=    Pkg_Common.SCH_GETMAILMSG('al_crftrk');
  END IF;

	   --for other forms


	   v_alnot_users := P_CRFSTATSENTTO;
        v_user_str    := v_alnot_users || ',' ;
	   v_params := v_alnot_users || ',' ;

          LOOP
		    V_CNT := V_CNT + 1;
		    V_POS := INSTR (V_USER_STR, ',');
		    V_PARAMS := SUBSTR (V_USER_STR, 1, V_POS - 1);
		    EXIT WHEN V_PARAMS IS NULL;

		   SELECT  usr_lst(v_params)
		   INTO v_sentoname
		   FROM dual; -- 6th parameter

		     IF v_formtype = 'O' THEN
                  v_fparam :=  v_studynum   ||'~'||  v_patcode  ||'~'|| v_crfnum ||'~'|| v_sentoname ||'~'|| v_sentbyname
                          ||'~' || v_sentondt ;
			 ELSE
			   v_fparam :=  v_studynum   ||'~'|| v_patcode ||'~' || NVL(TO_CHAR(v_visit),'Not Known') ||'~'|| v_evname ||'~'|| v_evdate ||'~'|| v_crfnum ||'~'|| v_sentoname ||'~'|| v_sentbyname
                          ||'~' || v_sentondt ;
			 END IF;


		   v_msg :=    Pkg_Common.SCH_GETMAIL(v_msgtemplate,v_fparam);


			INSERT INTO SCH_DISPATCHMSG (
				PK_MSG                 ,
				MSG_SENDON             ,
				MSG_STATUS             ,
				MSG_TYPE               ,
				FK_SCHEVENT               ,
				MSG_TEXT               ,
				FK_PAT		       ,
				CREATOR                ,
				IP_ADD                 ,
				FK_PATPROT             )
			      VALUES (
			        SCH_DISPATCHMSG_SEQ1.NEXTVAL ,
			   	P_CRFSTAT_SENTON,
			 	0,
				'U',
				v_pk_schevent ,
				v_msg ,
				v_params,
				NULL,
				NULL,
			     v_patprot) ;
			   V_USER_STR := SUBSTR (V_USER_STR, V_POS + 1);
			--p('DONE');

		END LOOP ; /* end loop for multiple users*/

--COMMIT;


END SP_CRFTRACKING;

/*end of package*/
END Pkg_Daily;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,368,2,'02_PKG_DAILY_Body.sql',sysdate,'v10 #768');
commit;