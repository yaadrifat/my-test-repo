set define off;
create or replace TRIGGER "ERES"."ER_STUDY_AU0" 
AFTER UPDATE
ON ERES.ER_STUDY
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE

  raid NUMBER(10);

  old_codetype VARCHAR2(200) ;

  new_codetype VARCHAR2(200) ;

  old_modby VARCHAR2(100) ;

  new_modby VARCHAR2(100) ;

  old_site VARCHAR2(50) ;

  new_site VARCHAR2(50) ;

  old_account VARCHAR2(30) ;

  new_account VARCHAR2(30) ;

  usr VARCHAR2(100);

BEGIN

  SELECT seq_audit.NEXTVAL INTO raid FROM dual;



  usr := Getuser(:NEW.last_modified_by);

  --KM-10Jun10-#4835
  IF NVL(:OLD.last_modified_date,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.record_transaction (raid, 'ER_STUDY', :OLD.rid, 'U', usr);
  END IF;


  IF NVL(:OLD.pk_study,0) !=

     NVL(:NEW.pk_study,0) THEN

     Audit_Trail.column_update

       (raid, 'PK_STUDY',

       :OLD.pk_study, :NEW.pk_study);

  END IF;

  IF NVL(:OLD.study_sponsor,' ') !=

     NVL(:NEW.study_sponsor,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_SPONSOR',

       :OLD.study_sponsor, :NEW.study_sponsor);

  END IF;
-- Added By Rohit to Fix Bug No 4900
  IF NVL(:OLD.STUDY_SPONSORID,' ') !=

     NVL(:NEW.STUDY_SPONSORID,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_SPONSORID',

       :OLD.STUDY_SPONSORID, :NEW.STUDY_SPONSORID);

  END IF;

  IF NVL(:OLD.FK_CODELST_SPONSOR,0) !=

     NVL(:NEW.FK_CODELST_SPONSOR,0) THEN

     Audit_Trail.column_update

       (raid, 'FK_CODELST_SPONSOR',

       :OLD.FK_CODELST_SPONSOR, :NEW.FK_CODELST_SPONSOR);

  END IF;

  IF NVL(:OLD.fk_account,0) !=

     NVL(:NEW.fk_account,0) THEN

    BEGIN

     SELECT ac_name INTO old_account

     FROM ER_ACCOUNT WHERE pk_account = :OLD.fk_account ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_account := NULL ;

    END ;

    BEGIN

     SELECT ac_name INTO new_account

     FROM ER_ACCOUNT WHERE pk_account = :NEW.fk_account ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_account := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_ACCOUNT',

	old_account, new_account);

  END IF;

  IF NVL(:OLD.study_contact,' ') !=

     NVL(:NEW.study_contact,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_CONTACT',

       :OLD.study_contact, :NEW.study_contact);

  END IF;

  IF NVL(:OLD.study_pubflag,' ') !=

     NVL(:NEW.study_pubflag,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_PUBFLAG',

       :OLD.study_pubflag, :NEW.study_pubflag);

  END IF;

  IF NVL(:OLD.study_title,' ') !=

     NVL(:NEW.study_title,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_TITLE',

       :OLD.study_title, :NEW.study_title);

  END IF;

  /*
  IF NVL(:OLD.study_obj,' ') !=

     NVL(:NEW.study_obj,' ') THEN

     audit_trail.column_update

       (raid, 'STUDY_OBJ',

       :OLD.study_obj, :NEW.study_obj);

  END IF;

  IF NVL(:OLD.study_sum,' ') !=

     NVL(:NEW.study_sum,' ') THEN

     audit_trail.column_update

       (raid, 'STUDY_SUM',

       :OLD.study_sum, :NEW.study_sum);

  END IF;

  */
  IF NVL(:OLD.study_prodname,' ') !=

     NVL(:NEW.study_prodname,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_PRODNAME',

       :OLD.study_prodname, :NEW.study_prodname);

  END IF;

  IF NVL(:OLD.study_samplsize,0) !=

     NVL(:NEW.study_samplsize,0) THEN

     Audit_Trail.column_update

       (raid, 'STUDY_SAMPLSIZE',

       :OLD.study_samplsize, :NEW.study_samplsize);

  END IF;

  IF NVL(:OLD.study_dur,0) !=

     NVL(:NEW.study_dur,0) THEN

     Audit_Trail.column_update

       (raid, 'STUDY_DUR',

       :OLD.study_dur, :NEW.study_dur);

  END IF;

  IF NVL(:OLD.study_durunit,' ') !=

     NVL(:NEW.study_durunit,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_DURUNIT',

       :OLD.study_durunit, :NEW.study_durunit);

  END IF;

  IF NVL(:OLD.study_estbegindt,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=

     NVL(:NEW.study_estbegindt,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN

     Audit_Trail.column_update

       (raid, 'STUDY_ESTBEGINDT',

       to_char(:OLD.study_estbegindt,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.study_estbegindt,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;

  IF NVL(:OLD.study_actualdt,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=

     NVL(:NEW.study_actualdt,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN

     Audit_Trail.column_update

       (raid, 'STUDY_ACTUALDT',

       to_char(:OLD.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT),to_char(:NEW.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;

  IF NVL(:OLD.study_prinv,' ') !=

     NVL(:NEW.study_prinv,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_PRINV',

       :OLD.study_prinv, :NEW.study_prinv);

  END IF;

  IF NVL(:OLD.study_partcntr,' ') !=

     NVL(:NEW.study_partcntr,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_PARTCNTR',

       :OLD.study_partcntr, :NEW.study_partcntr);

  END IF;

  IF NVL(:OLD.study_keywrds,' ') !=

     NVL(:NEW.study_keywrds,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_KEYWRDS',

       :OLD.study_keywrds, :NEW.study_keywrds);

  END IF;

  IF NVL(:OLD.study_pubcollst,' ') !=

     NVL(:NEW.study_pubcollst,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_PUBCOLLST',

       :OLD.study_pubcollst, :NEW.study_pubcollst);

  END IF;

  IF NVL(:OLD.study_parentid,0) !=

     NVL(:NEW.study_parentid,0) THEN

     Audit_Trail.column_update

       (raid, 'STUDY_PARENTID',

       :OLD.study_parentid, :NEW.study_parentid);

  END IF;

  IF NVL(:OLD.fk_author,0) !=

     NVL(:NEW.fk_author,0) THEN

    BEGIN

     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname

     INTO old_modby

     FROM ER_USER

     WHERE pk_user = :OLD.fk_author ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_modby := NULL ;

    END ;

    BEGIN

     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname

     INTO new_modby

     FROM ER_USER

     WHERE pk_user = :NEW.fk_author ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_modby := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_AUTHOR',

       old_modby, new_modby);

  END IF;

  IF NVL(:OLD.fk_codelst_tarea,0) !=

     NVL(:NEW.fk_codelst_tarea,0) THEN

    BEGIN

	SELECT trim(codelst_desc) INTO old_codetype

	FROM ER_CODELST WHERE pk_codelst =  :OLD.fk_codelst_tarea ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL ;

    END ;

    BEGIN

	SELECT trim(codelst_desc) INTO new_codetype

	FROM ER_CODELST WHERE pk_codelst =  :NEW.fk_codelst_tarea ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_TAREA',

       old_codetype, new_codetype);

  END IF;

  IF NVL(:OLD.fk_codelst_phase,0) !=

     NVL(:NEW.fk_codelst_phase,0) THEN

    BEGIN

	SELECT trim(codelst_desc) INTO old_codetype

	FROM ER_CODELST WHERE pk_codelst =  :OLD.fk_codelst_phase ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL ;

    END ;

    BEGIN

	SELECT trim(codelst_desc) INTO new_codetype

	FROM ER_CODELST WHERE pk_codelst =  :NEW.fk_codelst_phase ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_PHASE',

       old_codetype, new_codetype);

  END IF;

  IF NVL(:OLD.fk_codelst_restype,0) !=

     NVL(:NEW.fk_codelst_restype,0) THEN

    BEGIN

	SELECT trim(codelst_desc) INTO old_codetype

	FROM ER_CODELST WHERE pk_codelst =  :OLD.fk_codelst_restype ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL ;

    END ;

    BEGIN

	SELECT trim(codelst_desc) INTO new_codetype

	FROM ER_CODELST WHERE pk_codelst =  :NEW.fk_codelst_restype ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_RESTYPE',

       old_codetype, new_codetype);

  END IF;
  ------Added By Amarnadh for issue #3208 28th Nov '07
  IF NVL(:OLD.fk_codelst_scope,0) !=

     NVL(:NEW.fk_codelst_scope,0) THEN

    BEGIN

	SELECT trim(codelst_desc) INTO old_codetype

	FROM ER_CODELST WHERE pk_codelst =  :OLD.fk_codelst_scope ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL ;

    END ;

    BEGIN

	SELECT trim(codelst_desc) INTO new_codetype

	FROM ER_CODELST WHERE pk_codelst =  :NEW.fk_codelst_scope ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_SCOPE',

       old_codetype, new_codetype);

  END IF;

  IF NVL(:OLD.fk_codelst_type,0) !=

     NVL(:NEW.fk_codelst_type,0) THEN

   BEGIN

	SELECT trim(codelst_desc) INTO old_codetype

	FROM ER_CODELST WHERE pk_codelst =  :OLD.fk_codelst_type ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL ;

    END ;

    BEGIN

	SELECT trim(codelst_desc) INTO new_codetype

	FROM ER_CODELST WHERE pk_codelst =  :NEW.fk_codelst_type  ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_TYPE',

       old_codetype, new_codetype);

  END IF;

  IF NVL(:OLD.fk_codelst_blind,0) !=

     NVL(:NEW.fk_codelst_blind,0) THEN

    BEGIN

	SELECT trim(codelst_desc) INTO old_codetype

	FROM ER_CODELST WHERE pk_codelst =  :OLD.fk_codelst_blind ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL ;

    END ;

    BEGIN

	SELECT trim(codelst_desc) INTO new_codetype

	FROM ER_CODELST WHERE pk_codelst =  :NEW.fk_codelst_blind ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_BLIND',

       old_codetype, new_codetype);

  END IF;

  IF NVL(:OLD.fk_codelst_random,0) !=

     NVL(:NEW.fk_codelst_random,0) THEN

    BEGIN

	SELECT trim(codelst_desc) INTO old_codetype

	FROM ER_CODELST WHERE pk_codelst =  :OLD.fk_codelst_random ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL ;

    END ;

    BEGIN

	SELECT trim(codelst_desc) INTO new_codetype

	FROM ER_CODELST WHERE pk_codelst =  :NEW.fk_codelst_random ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_RANDOM',

       old_codetype, new_codetype);

  END IF;

  IF NVL(:OLD.rid,0) !=

     NVL(:NEW.rid,0) THEN

     Audit_Trail.column_update

       (raid, 'RID',

       :OLD.rid, :NEW.rid);

  END IF;

  IF NVL(:OLD.study_ver_no,' ') !=

     NVL(:NEW.study_ver_no,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_VER_NO',

       :OLD.study_ver_no, :NEW.study_ver_no);

  END IF;

  IF NVL(:OLD.study_current,' ') !=

     NVL(:NEW.study_current,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_CURRENT',

       :OLD.study_current, :NEW.study_current);

  END IF;

  IF NVL(:OLD.last_modified_by,0) !=

     NVL(:NEW.last_modified_by,0) THEN

    BEGIN

     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname

     INTO old_modby

     FROM ER_USER

     WHERE pk_user = :OLD.last_modified_by ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_modby := NULL ;

    END ;

    BEGIN

     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname

     INTO new_modby

     FROM ER_USER

     WHERE pk_user = :NEW.last_modified_by ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_modby := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'LAST_MODIFIED_BY',

       old_modby, new_modby);

  END IF;

  IF NVL(:OLD.last_modified_date,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=

     NVL(:NEW.last_modified_date,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN

     Audit_Trail.column_update

       (raid, 'LAST_MODIFIED_DATE',

       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;

  IF NVL(:OLD.created_on,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=

     NVL(:NEW.created_on,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN

     Audit_Trail.column_update

       (raid, 'CREATED_ON',

       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;

  IF NVL(:OLD.study_number,' ') !=

     NVL(:NEW.study_number,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_NUMBER',

       :OLD.study_number, :NEW.study_number);

  END IF;

    IF NVL(:OLD.study_info,' ') !=

     NVL(:NEW.study_info,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_INFO',

       :OLD.study_info, :NEW.study_info);

  END IF;

  IF NVL(:OLD.IP_ADD,' ') !=

     NVL(:NEW.IP_ADD,' ') THEN

     Audit_Trail.column_update

       (raid, 'IP_ADD',

       :OLD.IP_ADD, :NEW.IP_ADD);

  END IF;

  IF NVL(:OLD.STUDY_VERPARENT,0) !=

     NVL(:NEW.STUDY_VERPARENT,0) THEN

     Audit_Trail.column_update

       (raid, 'STUDY_VERPARENT',

       :OLD.STUDY_VERPARENT, :NEW.STUDY_VERPARENT);

  END IF;



  IF NVL(:OLD.study_end_date,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=

     NVL(:NEW.study_end_date,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN

     Audit_Trail.column_update

       (raid, 'STUDY_END_DATE',

       to_char(:OLD.study_end_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.study_end_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;

    IF NVL(:OLD.STUDY_DIVISION,0) !=

     NVL(:NEW.STUDY_DIVISION,0) THEN

    BEGIN

	SELECT trim(codelst_desc) INTO old_codetype

	FROM ER_CODELST WHERE pk_codelst =  :OLD.STUDY_DIVISION ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL ;

    END ;

    BEGIN

	SELECT trim(codelst_desc) INTO new_codetype

	FROM ER_CODELST WHERE pk_codelst =  :NEW.STUDY_DIVISION ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'STUDY_DIVISION',

       old_codetype, new_codetype);

  END IF;

  IF NVL(:OLD.STUDY_INVIND_FLAG,0) !=

     NVL(:NEW.STUDY_INVIND_FLAG,0) THEN

     Audit_Trail.column_update

       (raid, 'STUDY_INVIND_FLAG',

       :OLD.STUDY_INVIND_FLAG, :NEW.STUDY_INVIND_FLAG);

  END IF;

 IF NVL(:OLD.STUDY_INVIND_NUMBER,' ') !=

     NVL(:NEW.STUDY_INVIND_NUMBER,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_INVIND_NUMBER',

       :OLD.STUDY_INVIND_NUMBER, :NEW.STUDY_INVIND_NUMBER);

  END IF;

  --JM:added 080808 for issue #3782

  IF NVL(:OLD.STUDY_ADVLKP_VER,0) !=

     NVL(:NEW.STUDY_ADVLKP_VER,0) THEN

     Audit_Trail.column_update

       (raid, 'STUDY_ADVLKP_VER',

       :OLD.STUDY_ADVLKP_VER, :NEW.STUDY_ADVLKP_VER);

  END IF;

  IF NVL(:OLD.STUDY_CREATION_TYPE,' ') !=
     NVL(:NEW.STUDY_CREATION_TYPE,' ') THEN
     Audit_Trail.column_update
       (raid, 'STUDY_CREATION_TYPE',
       :OLD.STUDY_CREATION_TYPE, :NEW.STUDY_CREATION_TYPE);
  END IF;

-- Added By YK DFIN20 Bug#6006
IF NVL(:OLD.FK_CODELST_SET_MILESTATUS,0) !=

     NVL(:NEW.FK_CODELST_SET_MILESTATUS,0) THEN

    BEGIN

	SELECT pk_codelst INTO old_codetype

	FROM ER_CODELST WHERE pk_codelst =  :OLD.FK_CODELST_SET_MILESTATUS ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL ;

    END ;

    BEGIN

	SELECT pk_codelst INTO new_codetype

	FROM ER_CODELST WHERE pk_codelst =  :NEW.FK_CODELST_SET_MILESTATUS ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_SET_MILESTATUS',

       old_codetype, new_codetype);

  END IF;

  -- Ankit CTRP-20527 24-Nov-2011
  IF NVL(:OLD.STUDY_CTRP_REPORTABLE,0) !=
       NVL(:NEW.STUDY_CTRP_REPORTABLE,0) THEN
     Audit_Trail.column_update
       (raid, 'STUDY_CTRP_REPORTABLE',
       :OLD.STUDY_CTRP_REPORTABLE, :NEW.STUDY_CTRP_REPORTABLE);
  END IF;

   -- Ankit INF-22247 27-Feb-2012
  IF NVL(:OLD.FDA_REGULATED_STUDY,0) !=
       NVL(:NEW.FDA_REGULATED_STUDY,0) THEN
     Audit_Trail.column_update
       (raid, 'FDA_REGULATED_STUDY',
       :OLD.FDA_REGULATED_STUDY, :NEW.FDA_REGULATED_STUDY);
  END IF;

   -- CTRP 22471  Raviesh
  IF NVL(:OLD.NCI_TRIAL_IDENTIFIER,0) !=
       NVL(:NEW.NCI_TRIAL_IDENTIFIER,0) THEN
     Audit_Trail.column_update
       (raid, 'NCI_TRIAL_IDENTIFIER',
       :OLD.NCI_TRIAL_IDENTIFIER, :NEW.NCI_TRIAL_IDENTIFIER);
  END IF;

   -- CTRP 22471  Raviesh
  IF NVL(:OLD.NCT_NUMBER,0) !=
       NVL(:NEW.NCT_NUMBER,0) THEN
     Audit_Trail.column_update
       (raid, 'NCT_NUMBER',
       :OLD.NCT_NUMBER, :NEW.NCT_NUMBER);
  END IF;

   -- CTRP 22471  Raviesh
  IF NVL(:OLD.CTRP_ACCRREP_LASTRUN_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=

     NVL(:NEW.CTRP_ACCRREP_LASTRUN_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN

     Audit_Trail.column_update

       (raid, 'CTRP_ACCRREP_LASTRUN_DATE',

       to_char(:OLD.CTRP_ACCRREP_LASTRUN_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.CTRP_ACCRREP_LASTRUN_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT));

  end if;

  -- Ankit Bug-15444 01-May-2013
  if nvl(:old.study_disease_site,' ') !=
       NVL(:NEW.STUDY_DISEASE_SITE,' ') THEN
     Audit_Trail.column_update
       (raid, 'STUDY_DISEASE_SITE',
       :old.STUDY_DISEASE_SITE, :new.STUDY_DISEASE_SITE);
  end if;

  -- Ankit Bug-15444 01-May-2013
  if nvl(:old.study_icdcode1,' ') !=
       NVL(:NEW.STUDY_ICDCODE1,' ') THEN
     Audit_Trail.column_update
       (raid, 'STUDY_ICDCODE1',
       :old.STUDY_ICDCODE1, :new.STUDY_ICDCODE1);
  END IF;

  -- Ankit Bug-15444 01-May-2013
  if nvl(:old.study_icdcode2,' ') !=
       NVL(:NEW.STUDY_ICDCODE2,' ') THEN
     Audit_Trail.column_update
       (raid, 'STUDY_ICDCODE2',
       :old.STUDY_ICDCODE2, :new.STUDY_ICDCODE2);
  end if;

  -- Ankit Bug-15444 01-May-2013
  if nvl(:old.STUDY_ICDCODE3,' ') !=
       NVL(:NEW.STUDY_ICDCODE3,' ') THEN
     Audit_Trail.column_update
       (raid, 'STUDY_ICDCODE3',
       :old.STUDY_ICDCODE3, :new.STUDY_ICDCODE3);
  END IF;

--fixed by JP for Bug# 28004 
IF NVL(:OLD.STUDY_ASSOC,' ') !=

     NVL(:NEW.STUDY_ASSOC,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_ASSOC',

       :OLD.STUDY_ASSOC, :NEW.STUDY_ASSOC);

  END IF;
--Commented by Manimaran to fix the Bug 2743.
/* IF NVL(:OLD.study_obj_clob,' ') !=

     NVL(:NEW.study_obj_clob,' ') THEN

     audit_trail.column_update

       (raid, 'STUDY_OBJ_CLOB',

       dbms_lob.substr(:OLD.study_obj_clob,4000), dbms_lob.substr(:NEW.study_obj_clob,4000));

  END IF;

  IF NVL(:OLD.study_sum_clob,' ') !=

     NVL(:NEW.study_sum_clob,' ') THEN

     audit_trail.column_update

       (raid, 'STUDY_SUM_CLOB',

       dbms_lob.substr(:OLD.study_sum_clob,4000), dbms_lob.substr(:NEW.study_sum_clob,4000));

  END IF;
*/
END;
/
	INSERT INTO track_patches
	VALUES(seq_track_patches.nextval,389,4,'04_ER_STUDY_AU0.sql',sysdate,'v11 #790');

	commit; 
