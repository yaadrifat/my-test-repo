set define off;
DECLARE 
BEGIN 
   FOR i IN ( select pk_nwsites, NW_STATUS,CREATED_ON,CREATOR,IP_ADD  from er_nwsites ) loop    
    INSERT INTO er_status_history
    (PK_STATUS, STATUS_MODPK, STATUS_MODTABLE, FK_CODELST_STAT, STATUS_DATE, CREATOR, RECORD_TYPE, IP_ADD, STATUS_ISCURRENT)
    VALUES (SEQ_ER_STATUS_HISTORY.nextval, i.PK_NWSITES,'er_nwsites', i.NW_STATUS, i.CREATED_ON, i.CREATOR, 'N', i.IP_ADD, 1);
   END LOOP ;
commit;
END; 
/

INSERT INTO track_patches
	VALUES(seq_track_patches.nextval,389,8,'08_statushistory_datapatch.sql',sysdate,'v11 #790');
	commit; 