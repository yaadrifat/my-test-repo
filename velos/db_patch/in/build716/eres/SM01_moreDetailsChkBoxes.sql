set define off;

update er_codelst set codelst_custom_col1 = '{chkArray:' || codelst_custom_col1 || '}'
where codelst_type = 'studyidtype' and codelst_custom_col in ('checkbox', 'chkbox') 
and codelst_custom_col1 like '[%';

update er_codelst set codelst_custom_col1 = '{chkArray:' || codelst_custom_col1 || '}'
where codelst_type = 'user' and codelst_custom_col in ('checkbox', 'chkbox') 
and codelst_custom_col1 like '[%';

update er_codelst set codelst_custom_col1 = '{chkArray:' || codelst_custom_col1 || '}'
where codelst_type = 'evtaddlcode' and codelst_custom_col in ('checkbox', 'chkbox') 
and codelst_custom_col1 like '[%';

update er_codelst set codelst_custom_col1 = '{chkArray:' || codelst_custom_col1 || '}'
where codelst_type = 'advtype' and codelst_custom_col in ('checkbox', 'chkbox') 
and codelst_custom_col1 like '[%';

update er_codelst set codelst_custom_col1 = '{chkArray:' || codelst_custom_col1 || '}'
where codelst_type = 'peridtype' and codelst_custom_col in ('checkbox', 'chkbox') 
and codelst_custom_col1 like '[%';

commit;