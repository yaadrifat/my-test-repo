set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LKPCOL where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Study Summary' and LKPTYPE_TYPE='dyn_s') 
	and LKPCOL_NAME='NCI_TRIAL_IDENTIFIER';
  if (v_record_exists = 0) then
INSERT INTO ER_LKPCOL 
		(PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_TABLE, LKPCOL_KEYWORD)
	 VALUES((select max(PK_LKPCOL)+1 from ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Study Summary' and LKPTYPE_TYPE='dyn_s'),'NCI_TRIAL_IDENTIFIER','NCI_Trial_Identifier','varchar2','erv_study_dyn','NCI_TRIAL_IDENTIFIER');
	 
    commit;
  end if;
end;

/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LKPVIEWCOL where FK_LKPVIEW=(select PK_LKPVIEW from ER_LKPVIEW where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Study Summary' and LKPTYPE_TYPE='dyn_s') and LKPVIEW_NAME='Summary') 
	and FK_LKPCOL=(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME='NCI_TRIAL_IDENTIFIER');
  if (v_record_exists = 0) then
   INSERT INTO ER_LKPVIEWCOL
  (PK_LKPVIEWCOL,FK_LKPCOL, LKPVIEW_SEQ,FK_LKPVIEW, LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY)
  VALUES((select max(PK_LKPVIEWCOL)+1 from ER_LKPVIEWCOL) ,(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME='NCI_TRIAL_IDENTIFIER' and FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Study Summary' and LKPTYPE_TYPE='dyn_s')),(select max(LKPVIEW_SEQ)+1 from ER_LKPVIEWCOL where FK_LKPVIEW=(select PK_LKPVIEW from ER_LKPVIEW where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Study Summary' and LKPTYPE_TYPE='dyn_s') and LKPVIEW_NAME='Summary')),(select PK_LKPVIEW from ER_LKPVIEW where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Study Summary' and LKPTYPE_TYPE='dyn_s') and LKPVIEW_NAME='Summary'),'10%','Y');
 
   commit;
  end if;
end;
  /
  
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,399,11,'11_LKP_Insert.sql',sysdate,'v11 #800');
commit; 
  