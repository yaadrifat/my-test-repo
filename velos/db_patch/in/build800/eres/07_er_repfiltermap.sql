set define off;
DECLARE 
	tab_col_check number;
BEGIN
	SELECT count(*)
	INTO tab_col_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPFILTERMAP' and column_name='REPFILTERMAP_MANDATORY';
	
	IF (tab_col_check > 0) then
		Update ER_REPFILTERMAP set REPFILTERMAP_MANDATORY=null
		where repfiltermap_repcat='biospecimen' and fk_repfilter=(select pk_repfilter from er_repfilter where repfilter_keyword='orgId');
	End if;
commit;
End;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,399,7,'07_er_repfiltermap.sql',sysdate,'v11 #800');

commit; 