create or replace TRIGGER "ERES"."ER_PATPROT_BI1" BEFORE INSERT ON ER_PATPROT        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
   cnt number:=0;
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 select count(*) into cnt from er_patprot where fk_per= :NEW.fk_per and fk_study= :NEW.fk_study and patprot_stat =1;
  IF( cnt > 1 )
  THEN
    RAISE_APPLICATION_ERROR( -20001,'The Patient has already been enrolled ');
  ELSE
    select count(*) into cnt from er_patprot where fk_study= :NEW.fk_study and patprot_patstdid = :NEW.PATPROT_PATSTDID and  patprot_stat =1;
    IF( cnt > 1 )
    THEN
      RAISE_APPLICATION_ERROR( -20001,'The Patient with same patstudyId already exists in study ');
    END IF;
  END IF;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,396,1,'01_ER_PATPROT_BI1.sql',sysdate,'v11 #797');

commit;