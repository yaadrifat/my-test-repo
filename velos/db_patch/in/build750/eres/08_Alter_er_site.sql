set define off;
DECLARE
  v_column_exists number;  
BEGIN
	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_SITE'
    AND COLUMN_NAME = 'NCI_POID';
	
	if (v_column_exists = 0) then
		execute immediate 'ALTER TABLE ERES.ER_SITE ADD (NCI_POID VARCHAR2(50 BYTE))';
		dbms_output.put_line('Column NCI_POID has been added to ER_SITE');
	else
		dbms_output.put_line('Columns are already added in ER_SITE.');
	end if;
END;
/

COMMENT ON COLUMN ERES.ER_SITE.NCI_POID IS 'This column stores NCI PO Organization ID';



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,349,8,'08_Alter_er_site.sql',sysdate,'v10 #750');

commit;