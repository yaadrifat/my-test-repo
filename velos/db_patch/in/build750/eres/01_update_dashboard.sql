update er_ctrltab set ctrl_desc='Dashboard' where ctrl_desc='Query Dashboard' and ctrl_value='QRYDASH';
commit;

update er_ctrltab set ctrl_desc='Query Dashboard' where ctrl_desc='Dashboard' and ctrl_value='DASH';
commit;

update er_ctrltab set ctrl_desc='Query Dashboard' where ctrl_desc='Dashboard' and ctrl_value='MODDASH';
commit;

update er_ctrltab set ctrl_desc='Dashboard' where ctrl_desc='Query Dashboard' and ctrl_value='MODQRYDASH';
commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,349,1,'01_update_dashboard.sql',sysdate,'v10 #750');

commit;