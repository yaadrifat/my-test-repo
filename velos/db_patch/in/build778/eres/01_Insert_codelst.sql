DECLARE 
	table_check number;
	row_check number :=0;

BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_CODELST';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_codelst
		WHERE codelst_type = 'studystat' AND
			  codelst_subtyp = 'Inact';
		
		IF (row_check = 0) then
	
			INSERT INTO ER_CODELST 
						(PK_CODELST, 
						CODELST_TYPE, 
						CODELST_SUBTYP, 
						CODELST_DESC, 
						CODELST_HIDE, 
						CODELST_CUSTOM_COL1,
						CODELST_STUDY_ROLE)
							
			VALUES
				(SEQ_ER_CODELST.nextval, 
				'studystat', 
				'Inact', 
				'Inactive', 
				'N',
				'default', 
				'default_data');
					
		END IF;
	
	END IF;		
	
	COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,377,1,'01_Insert_codelst.sql',sysdate,'v10.1 #778');

commit;
