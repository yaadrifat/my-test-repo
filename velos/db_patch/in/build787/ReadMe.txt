/////*********This readMe is specific to v11 build #787**********////


*********************************************************************************************************************************************************************
A. Study Network tab items:
---------------------------------------
 In Build#786 "Study Network Tab implementation" is released for the first time. In that release we have forgot to mention the uncovered items list . In this build we
have covered all these items. To identify the items released in this build we have highlighted the items in the document with YELLOW color. QA need to re validate these  items again.

There are still few items that are not covered in this document. they are :
1. 9.Filters  &  10. Bulk Edit   & vi.    The workflow panel should appear for this tab. and Under rules section point b. 



B. Velos eResearch Lab Module Fixes and Enhancements for v11 :
-------------------------------------------------------------------------------------------
This Lab enhancement request came from PM team as points wise in a document.  Team has worked on this document and covered  many of these items.
To identify the items released in this build we have highlighted the items in the document with YELLOW color. 

1. Add New Labs > Selection: All points    2. Add New Labs > Lab Data: Point A,D & E   3. Study Patient >> Form Response Browser (Lab View): Points : A & D      
4. Patient Lab Details Screen : Point A

ITEMS NOT COVERED IN THIS BUILD:
1. Add New Labs > Lab Data: Point B & C.    2. Study Patient >> Form Response Browser (Lab View): Points B & C.  3. Patient Lab Details Screen : Point B.


C. Velos eSample Fixes and Enhancements for v11
---------------------------------------------------------------------
This eSample enhancement request came from PM team as points wise in a document.  Team has worked on this document and covered  many of these items.
To identify the items released in this build we have highlighted the items in the document with YELLOW color.

1. Navigation between eSample and Patient tabs:  A & B     2. Specimens > Search:  A C D & E    3. Preparation Area: A B & C    4. Specimens > Label: A B
5.Add Multiple Specimens: A 

ITEMS NOT COVERED IN THIS BUILD:
1.Specimens > Search:   B F & G     2.Preparation Area: D      3. Specimens: A & C   4.Add Multiple Specimens:  B 5.Specimen > Select Location (everywhere the Select Location lookup shows up): A B C D     6.Storage: A 

 
D. Networks Management -- Compared the requirements document and listed down the gaps. 
      I.e.ii -- This field should be an in-line editing field � dropdown field.
      I.e.v -- If an organization is made inactive, all sub-organizations under that organization should be made inactive as well. If the user will make a sub-organization active, the parent organizations will automatically become active.
      I.e.vi -- If an organization is made active, all sub-organizations under that organization will be made active as well.


Bugs Fixed in build 787: 

27761  27766 27769 27770 27772 27773  27786, 27762, 27790 27783

27770 27772 27809 27275 24373 27355 27806 20781 

**********************************************************************************************************************************************************************







