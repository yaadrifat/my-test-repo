ALTER TRIGGER ER_FORMQUERYSTATUS_AU1 DISABLE;
/
ALTER TRIGGER ER_FORMQUERYSTATUS_AI0 DISABLE;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,427,4,'04_disable_trigger.sql',sysdate,'v11 #828');

commit;	
/