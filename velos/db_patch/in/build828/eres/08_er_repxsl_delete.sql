declare
begin
	delete from er_repxsl where pk_repxsl in (30000,30001);
commit;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,427,8,'08_er_repxsl_delete.sql',sysdate,'v11 #828');

commit;
/