set define off;
declare
  v_table_exist number;
begin
  select count(*) INTO v_table_exist from user_tables where table_name='ER_NWSITES';
  
  if v_table_exist > 0
  then
    UPDATE ER_NWSITES SET NW_MEMBERTYPE=(SELECT pk_codelst FROM er_codelst WHERE codelst_type='relnshipTyp' and codelst_desc=f_codelst_desc(NW_MEMBERTYPE));
  end if;
commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,397,2,'02_datapatch_er_nwsites.sql',sysdate,'v11 #798');

commit; 
