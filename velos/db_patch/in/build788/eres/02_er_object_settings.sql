set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
	Select count(*) into v_record_exists
    from ER_OBJECT_SETTINGS where OBJECT_NAME='network_document_tab' and OBJECT_SUBTYPE= '3';
	
	if (v_record_exists = 0) then
		INSERT INTO ER_OBJECT_SETTINGS 
		(PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
		VALUES(
		SEQ_ER_OBJECT_SETTINGS.nextval , 'T' , '3' , 'network_document_tab' , 3, 1 , 'User Network Documents' , 0);
	end if;
	
	Select count(*) into v_record_exists
	from ER_OBJECT_SETTINGS where OBJECT_NAME='network_document_tab' and OBJECT_SUBTYPE= '4'; 
	
	if (v_record_exists = 0) then
		INSERT INTO ER_OBJECT_SETTINGS 
		(PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
		VALUES(
		SEQ_ER_OBJECT_SETTINGS.nextval , 'T' , '4' , 'network_document_tab' , 4, 1 , 'User Level Documents' , 0);
	end if;
commit;
end;
/

	INSERT INTO track_patches
	VALUES(seq_track_patches.nextval,387,2,'02_er_object_settings.sql',sysdate,'v11 #788');
	commit;
