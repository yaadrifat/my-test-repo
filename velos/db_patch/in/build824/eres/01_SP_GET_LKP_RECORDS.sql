set define off;
create or replace PROCEDURE        "SP_GET_LKP_RECORDS" (p_page IN NUMBER, p_rec_per_page IN NUMBER,p_viewId IN NUMBER,p_search IN LONG, p_cursor OUT Gk_Cv_Types.GenericCursorType,p_retrows OUT NUMBER)
    IS
    custom_col SPLIT_TBL;
    v_row_count number;
    v_column_name_ varchar2(500):='';
    v_CODELST_SUBTYP varchar2(500);
    v_CODELST_TYPE varchar2(500);
    custom_col_value varchar2(100);
    l_query LONG := 'select ';
    v_cus_col_name varchar2(4000);
 		v_colname VARCHAR2(4000);
		v_coldisplayName VARCHAR2(100);
		v_collist LONG;
		v_countSQL LONG;
		v_retrows NUMBER;
		v_lkplib NUMBER ;
		v_from VARCHAR2(500);
		v_search LONG;
		v_where LONG;
		v_dispcolcount NUMBER;
		v_orderstr VARCHAR2(100);
		v_dispflag CHAR(1);
		v_colorder_setting NUMBER;
		v_where_order NUMBER;

		/* YK 12Jan- BUG 5713*/
		v_increment NUMBER;	-- Used for loop increment value
		v_increment_value NUMBER; -- Used to store v_orderBy_pos value after casting for comparison
		v_column_name VARCHAR2(500); -- Used to store column name
		v_where_Col VARCHAR2(4000); -- Used to construct where condition on column name
		v_orderBy_pos VARCHAR2(10); -- Used to get the order by position value

		pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'Sp_Get_Lkp_Records', pLEVEL  => Plog.LFATAL);

/*
Parameters:
		   p_page - Page Number for which records are required
		   p_rec_per_page - Number of records per page
		   p_cursor - output type, will hold rows returned
		   p_retrows - output type, returns total number of rows for the query, used by pagination object
*/
 BEGIN
 -- plog.fatal(pctx, 'p_viewId:'|| p_viewId||' p_search:'||p_search);
 -- p_cursor in out cursorType
 -- get column names
 -- get from list
	v_search  := p_search;
	v_dispcolcount := 0;
  v_row_count:=0;
	/* YK 12Jan- BUG 5713*/
	v_increment := 0;
	v_increment_value :=0;
	v_colorder_setting := 0;
	v_colorder_setting := NVL(INSTR(LOWER(p_search),'order by'),0);
  /* YK 12Jan- BUG 5713*/
  IF v_colorder_setting > 0 THEN
  v_orderBy_pos := SUBSTR(p_search, NVL(INSTR(LOWER(p_search),'order by'),0)+9,2);
  v_increment_value := to_number(v_orderBy_pos);
  --plog.fatal(pctx, 'p_viewId********* ' || v_increment_value);
  END IF;

 FOR j IN (SELECT DISTINCT trim(lkpcol_table) AS lkpcol_table FROM ER_LKPCOL a,ER_LKPVIEWCOL b WHERE 	b.fk_lkpcol=a.pk_lkpcol AND b.fk_lkpview=p_viewId)
 LOOP
 v_from := v_from ||','|| j.lkpcol_table;
 END LOOP;
 -- remove  extra ','
 v_from := SUBSTR(v_from, 2, LENGTH(v_from));
 FOR i IN (SELECT a.lkpcol_name,a.lkpcol_dispval,NVL(b.lkpview_seq,0) lkpview_seq ,a.FK_LKPLIB,a.lkpcol_table, lkpview_is_display FROM ER_LKPCOL a,ER_LKPVIEWCOL b WHERE b.fk_lkpcol=a.pk_lkpcol AND b.fk_lkpview=p_viewId   ORDER BY lkpview_seq)
 LOOP
  -- this will get all column rows

  v_colname := i.LKPCOL_NAME;
  v_coldisplayName := i.LKPCOL_DISPVAL;
   plog.fatal(pctx,'v_coldisplayName ' || v_coldisplayName);
  v_lkplib:=i.FK_LKPLIB ;
  /* YK 12Jan- BUG 5713*/
  v_increment := v_increment+1;
  IF v_colorder_setting > 0 THEN
    IF v_increment_value=v_increment THEN
    	/*BUG 5758 union needs column alias in order by clause*/
    	if (INSTR(LOWER(p_search),'union') > 0) then
          v_column_name:= '"'||v_coldisplayName||'"';
      	else
     	  v_column_name:=v_colname;
     	end if;
    END IF;
   END IF;
  v_coldisplayName := SUBSTR(v_coldisplayName,1,30);

  -- generate column list
  v_collist := v_collist || ',' || v_colname  || ' "' || v_coldisplayName ||'"';
  --check if its a visible column
  v_dispflag := i.lkpview_is_display;
  IF v_dispflag = 'Y' AND v_dispcolcount < 4 THEN
    v_orderstr := v_orderstr || ','  || i.lkpview_seq;
	v_dispcolcount := v_dispcolcount + 1;
  END IF;
 END LOOP;
 /*Configuration  NCI Dictionary in er_codelst to cutomize sorting */
 if(v_colorder_setting >0) then

 select lkptype_type,lkpTYPE_version into v_CODELST_TYPE ,v_CODELST_SUBTYP from ER_LKPLIB ,er_lkpview
 where PK_LKPVIEW=p_viewId
and FK_LKPLIB=pk_LKPLIB;

-- select LKPTYPE_VERSION,LKPTYPE_TYPE into v_CODELST_SUBTYP,v_CODELST_TYPE from er_lkplib where pk_lkplib=p_viewId;
 /*Check configuration in er_codelst*/
 begin
 select count(*) into v_row_count from er_codelst where CODELST_TYPE=v_CODELST_TYPE and CODELST_SUBTYP=v_CODELST_SUBTYP;
  EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_row_count := 0;
  end;
  /*Check configuration in er_codelst*/
   plog.fatal(pctx,'v_CODELST_SUBTYP ' || v_CODELST_SUBTYP);
    plog.fatal(pctx,'v_CODELST_TYPE ' || v_CODELST_TYPE);
  plog.fatal(pctx,'v_row_count ' || v_row_count);
 if v_row_count > 0 then
 select  Pkg_Util.f_split(nvl(CODELST_CUSTOM_COL,0),',') into custom_col from er_codelst where CODELST_TYPE=v_CODELST_TYPE and CODELST_SUBTYP=v_CODELST_SUBTYP;
 -- plog.fatal(pctx,'v_column_name ' || v_column_name);
 if(custom_col(1)^=0) then

 --v_column_name:=v_column_name||',';
 for i in 1..custom_col.count
 loop
 if v_increment_value ^= custom_col(i) then
 plog.fatal(pctx,'custom_col ' || custom_col(i));
 begin
SELECT a.lkpcol_name into v_cus_col_name   FROM ER_LKPCOL a,  ER_LKPVIEWCOL b WHERE  b.fk_lkpcol   =a.pk_lkpcol   AND b.fk_lkpview=p_viewId  and b.lkpview_seq=custom_col(i);
 EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_cus_col_name := NULL;
        end;
        if v_cus_col_name is not null then
        v_column_name_:=v_column_name_||v_cus_col_name ||',';
       
       
        end if;
 end if;
 end loop;
 v_column_name_:=SUBSTR(v_column_name_,1,length(v_column_name_)-1);
 end if;
 end if;
 plog.fatal(pctx,'v_column_name_***** ' || v_column_name_);
 end if;
 v_increment:=0; -- reset Loop value
   -- remove first ',' from v_orderstr and add order by
   /* YK 12Jan- BUG 5713*/
   IF v_colorder_setting > 0 THEN
      if v_orderBy_pos != 1  THEN
            if(v_column_name_='' or  v_column_name_ is null) then
           v_orderstr := ' order by lower(' || v_column_name ||')';
           else
            v_orderstr := ' order by lower(' || v_column_name ||')'||','||v_column_name_;
           end if;
      else
         if(v_column_name_='' or  v_column_name_ is null) then
         v_orderstr := ' order by ' || v_column_name ;
           else
           v_orderstr := ' order by ' || v_column_name ||','||v_column_name_;
           
           end if;
           
   END IF;
    ELSE
      v_orderstr := ' order by ' || SUBSTR(v_orderstr,2);
   END IF;

    /*YK Added to add ascending or descending order sorting*/
   IF (INSTR(LOWER(p_search),'asc') > 0) then
      v_orderstr := v_orderstr || ' asc';
      ELSE
      v_orderstr := v_orderstr || ' desc';
   END IF;

   v_collist := SUBSTR(v_collist,2);
   l_query := l_query || v_collist;
   IF INSTR(LOWER(v_from),'er_lkpdata')>0 THEN
	v_where := ' Where  fk_lkplib = '|| v_lkplib ;
 END IF	;
 -- plog.DEBUG(pctx, 'p_search ' || p_search);
 -- plog.DEBUG(pctx, 'v_where 1 ' || v_where);
 IF LENGTH(p_search) >0 THEN
  IF LENGTH(v_where)>0 THEN
  v_where := v_where || p_search ;
  ELSE
  IF INSTR(p_search,'and')>0 THEN
  	 /*BUG 5758 orginal code retained*/
   --  plog.DEBUG(pctx, 'SUBSTR ' || (SUBSTR(p_search,INSTR(p_search,'and')+ 3)));
  	v_where:=	' Where ' || SUBSTR(p_search,INSTR(p_search,'and')+ 3);
  ELSE
    v_where:=p_search;
   -- plog.DEBUG(pctx, 'v_where 2 ' || v_where);
  END IF ;
  END IF;
 END IF;

 IF v_colorder_setting > 0 THEN
     --remove order by
    v_where_order := NVL(INSTR(LOWER(v_where),'order by'),0);
    IF v_where_order > 0 THEN
      v_where := SUBSTR(v_where,1, v_where_order - 1);
    END IF;
    --l_query := l_query || v_orderstr;
 END IF;

 l_query := l_query || ' from  ' ||v_from || v_where ;

 /*order by will slow down count so constructing v_countSQL before*/
 v_countSQL := 'Select count(*) from (' ||l_query||')' ;

 /*BUG 5758, 5846*/
 IF (INSTR(LOWER(l_query),'union') > 0) then
     l_query := 'select * from (' || l_query || ') ';
 END IF;

 --order by needs to be appended in both cases c_colorder_setting <=/> 0
 l_query := l_query || v_orderstr;

--   INSERT INTO T(C) VALUES (l_query);
--COMMIT;
--  v_countSQL := 'Select count(*) from ' ||v_from || v_where ;
-- moved above to avoid order by slowdown v_countSQL := 'Select count(*) from (' ||l_query||')'

 -- we have final query for sp_get_page_records()
 -- Prepare count sql for sp_get_page_records()
--	open p_cursor for l_query;

  plog.fatal(pctx,'LOOKUP SQL YK ' || l_query);
 plog.DEBUG(pctx, 'l_query 2 ' || l_query);
	Sp_Get_Page_Records (p_page,p_rec_per_page, l_query,v_countSQL,p_cursor,p_retrows);
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,423,1,'01_SP_GET_LKP_RECORDS.sql',sysdate,'v11 #824');
commit;
/