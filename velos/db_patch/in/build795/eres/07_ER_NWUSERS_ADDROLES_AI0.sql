set define off;
create or replace
TRIGGER "ERES"."ER_NWUSERS_ADDNLROLES_AI0" AFTER INSERT ON ERES.ER_NWUSERS_ADDNLROLES REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
  pknwuserAddroles number;
  nwuadtStatus  number;
  nwuserCreatedDate date;
  nwuserCreator number;
  nwuserIp varchar2(20);
BEGIN

  pknwuserAddroles:= :NEW.PK_NWUSERS_ADDROLES;
  nwuadtStatus:= :NEW.NWU_ADTSTATUS ;
  nwuserCreatedDate:= :NEW.CREATED_ON;
  nwuserCreator:= :NEW.CREATOR;
  nwuserIp:= :NEW.IP_ADD;
  insert into er_status_history(PK_STATUS,STATUS_MODPK,STATUS_MODTABLE,FK_CODELST_STAT,STATUS_DATE,CREATOR,RECORD_TYPE,IP_ADD,STATUS_ISCURRENT) values(SEQ_ER_STATUS_HISTORY.nextval,pknwuserAddroles,'er_nwusers_addnlroles',nwuadtStatus,nwuserCreatedDate,nwuserCreator,'N',nwuserIp,1);

END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,394,7,'07_ER_NWUSERS_ADDROLES_AI0.sql',sysdate,'v11 #795');

commit;
