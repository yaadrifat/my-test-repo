SET DEFINE OFF;

create or replace TRIGGER "ERES"."ER_SPECIMEN_APPNDX_BU_LM" BEFORE UPDATE ON ER_SPECIMEN_APPNDX
FOR EACH ROW
       WHEN (new.last_modified_by is not null) BEGIN
  :NEW.last_modified_date := SYSDATE ;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,394,11,'11_ER_SPECIMEN_APPNDX_BUO.sql',sysdate,'v11 #795');

commit; 
