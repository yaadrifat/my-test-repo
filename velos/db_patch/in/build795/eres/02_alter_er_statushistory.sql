Set Define Off;
declare
	v_count number :=0;
begin
		Select count(*) into v_count from user_tab_cols where column_name = 'STATUS_MODTABLE' and table_name = 'ER_STATUS_HISTORY';
	if v_count>0 then
	execute immediate 'alter table ER_STATUS_HISTORY modify STATUS_MODTABLE varchar2(50)';
	end if;
end;

/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,394,2,'02_alter_er_statushistory.sql',sysdate,'v11 #795');

commit;