set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v11 #795' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,394,0,'00_er_version.sql',sysdate,'v11 #795');

commit;