set define off;
create or replace
TRIGGER "ERES"."ER_NWUSERS_ADDNLROLES_BI0" BEFORE INSERT ON ER_NWUSERS_ADDNLROLES        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;


  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_NWUSERS_ADDNLROLES',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'PK_NWUSERS_ADDROLES', :NEW.PK_NWUSERS_ADDROLES);
       Audit_Trail.column_insert (raid, 'CREATED_ON', TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'FK_NWUSERS', :NEW.FK_NWUSERS);
       Audit_Trail.column_insert (raid, 'NWU_MEMBERADDLTROLE', :NEW.NWU_MEMBERADDLTROLE);
       Audit_Trail.column_insert (raid, 'NWU_ADTSTATUS', :NEW.NWU_ADTSTATUS);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,394,5,'05_ER_NWUSERS_ADDNLROLES_BI0.sql',sysdate,'v11 #795');

commit;
