set define off;
CREATE OR REPLACE PACKAGE BODY ERES."PKG_LNKFORM" AS
    PROCEDURE SP_LINKFORM(p_account VARCHAR2, p_study ARRAY_STRING, p_forms ARRAY_STRING, p_names ARRAY_STRING,
           p_descs ARRAY_STRING, p_displinks ARRAY_STRING, p_chars ARRAY_STRING, p_orgs ARRAY_STRING,  p_grps ARRAY_STRING,
        p_lnkfrom VARCHAR2, p_user VARCHAR2, p_ipadd VARCHAR2, o_ret OUT NUMBER , o_new_forms OUT ARRAY_STRING)
    AS
    /****************************************************************************************************
    ** Procedure to insert multiple forms for linking with the study or account
    ** all the records are received in the form of arrays.
    ** Author: Sonika Talwar 19th Aug 2003
    ** Input parameter: account Id
    ** Input parameter: Array of study Ids. This array will have only one value in case of linking form with study,
    **                  for the study id to which the forms are being linked. It will have many values incase of account linked forms
    **                  one value corresponding to each form.
    ** Input parameter: Array of form Ids to be linked with study
    ** Input parameter: Array of form names
    ** Input parameter: Array of form descriptions
    ** Input parameter: Array of form display links
    ** Input parameter: Array of form characteristics
    ** Input parameter: Array of form organizations. The organization ids are comma separated in each index of array
    ** Input parameter: Array of form groups. The groupids are comma separated in each index of array
    ** Input parameter: link from (Flag to indicate the location of association:A - From Manage account - manage forms S - From Manage Protocols - setup)
    ** Input parameter: creator of record (userId)
    ** Input parameter: IP Address
    ** Output parameter: 0 for successful insert, -1 for error, -3 if duplicate form name found
    **/
    v_cnt NUMBER;
    i NUMBER;
    v_formid NUMBER;
    v_name VARCHAR2(50);
    v_desc VARCHAR2(255);
    v_displink CHAR(2);
    v_char CHAR(1);
    v_org VARCHAR(4000);
    v_siteid VARCHAR(100);
    v_orgcnt NUMBER;
    v_grp VARCHAR(4000);
    v_grpid VARCHAR(100);
    v_grpcnt NUMBER;
    v_pos NUMBER;
    v_newformid NUMBER;
    v_formtype NUMBER;
    v_study NUMBER;
    v_formarray ARRAY_STRING  := ARRAY_STRING();
    v_new_formarray ARRAY_STRING  := ARRAY_STRING();
    v_fk_account NUMBER;

    BEGIN
        SELECT fk_account INTO v_fk_account FROM ER_USER WHERE
        pk_user = TO_NUMBER(p_user);

        v_cnt := p_forms.COUNT(); --get the # of elements in array
        i:=1;
        v_formarray.EXTEND;
        WHILE i <= v_cnt LOOP
            v_formid := TO_NUMBER(p_forms(i));
            v_name := p_names(i);
            v_name := trim(v_name);

            v_desc := p_descs(i);
            v_displink := p_displinks(i);
            v_char := p_chars(i);
            v_org := p_orgs(i);
            v_grp := p_grps(i);
            v_formarray(1) := p_forms(i);--fill only one form in the array, put always on 1 index
            SELECT fk_catlib
            INTO v_formtype
            FROM ER_FORMLIB
            WHERE pk_formlib =  v_formid;

                        --first create a new copy in the library, then add it to er_linkedforms
            v_study := TO_NUMBER(p_study(i));
            --send the study id for duplicate form name check in study, v_formarray has only one form
            Pkg_Form.SP_COPY_MULTIPLE_FORMS(v_formarray, v_desc, v_formtype , p_lnkfrom ,v_study, TO_NUMBER(p_user) , v_name, v_displink, p_ipadd  ,  v_newformid);

            --id duplicate name found for any form, return
            IF (v_newformid = -3) THEN
                o_ret:=-3;
                ROLLBACK;
                RETURN;
            END IF;

            --sonia sahni 28 may 04 add new form's id to array
            v_new_formarray.EXTEND;
            v_new_formarray(i) := v_newformid;
            BEGIN
                --add the new form to er_linkedforms
                INSERT INTO ER_LINKEDFORMS
                (pk_lf, fk_formlib, lf_displaytype,
                lf_entrychar, fk_account, fk_study,
                lf_lnkfrom, record_type, creator,
                created_on, ip_add)
                VALUES(seq_er_linkedforms.NEXTVAL, v_newformid, v_displink,
                v_char, p_account, v_study,
                p_lnkfrom, 'N', p_user,
                SYSDATE, p_ipadd);
                EXCEPTION  WHEN OTHERS THEN
                P('ERROR');
                o_ret:=-1;
                RETURN;
            END ;--end of insert er_linkedforms
            --multiple organization ids are stored as comma separated for each form
            --separate each id and insert the record in er_formorgacc
            v_orgcnt := 0;
            IF LENGTH(v_org) > 0 THEN
                LOOP
                    v_orgcnt := v_orgcnt + 1;
                    v_pos := INSTR(v_org, ',');
                    IF v_pos > 0 THEN
                        v_siteid := SUBSTR(v_org,1,v_pos-1);
                    END IF;
                    IF v_pos = 0 THEN
                        v_siteid := SUBSTR(v_org,1,LENGTH(v_org));
                    END IF;
                    -- p('v_siteid ' || v_siteid);
                    BEGIN
                        IF (LENGTH(v_siteid)>0) THEN
                            INSERT INTO ER_FORMORGACC (pk_formorgacc, fk_site, fk_formlib,record_type, creator, created_on, ip_add)
                            VALUES(seq_er_formorgacc.NEXTVAL, v_siteid, v_newformid,'N', p_user, SYSDATE, p_ipadd);
                        ELSE
                            v_pos:=0;
                        END IF;
                        EXCEPTION  WHEN OTHERS THEN P('ERROR');
                        o_ret:=-1;
                        RETURN;
                    END ;--end of insert er_formorgacc
                    EXIT WHEN v_pos = 0;
                    v_org := SUBSTR (v_org, v_pos + 1);
                END LOOP; --end of organization loop
            END IF;
            --multiple group ids are stored as comma separated for each form
            --separate each id and insert the record in er_formgrpacc
            v_grpcnt := 0;
            IF LENGTH(v_grp) > 0 THEN
                LOOP
                    v_grpcnt := v_grpcnt + 1;
                    v_pos := INSTR(v_grp, ',');
                    IF v_pos > 0 THEN
                        v_grpid := SUBSTR(v_grp,1,v_pos-1);
                    END IF;
                    IF v_pos = 0 THEN
                        v_grpid := SUBSTR(v_grp,1,LENGTH(v_grp));
                    END IF;
                    BEGIN
                        IF (LENGTH(v_grpid)>0) THEN
                            INSERT INTO ER_FORMGRPACC (pk_formgrpacc, fk_group, fk_formlib,record_type, creator, created_on, ip_add)
                            VALUES(seq_er_formgrpacc.NEXTVAL, v_grpid, v_newformid,'N', p_user, SYSDATE, p_ipadd);
                        ELSE
                            v_pos:=0;
                        END IF;
                        EXCEPTION  WHEN OTHERS THEN P('ERROR');
                        o_ret:=-1;
                        RETURN;
                    END ;--end of insert er_formgrpacc
                    EXIT WHEN v_pos = 0;
                    v_grp := SUBSTR (v_grp, v_pos + 1);
                END LOOP; --end of group loop
            END IF;
            i := i+1;
        END LOOP; --v_cnt loop
        COMMIT;
        o_ret:=0;
        COMMIT;
        o_new_forms := v_new_formarray;
        /**
        Statement For Testing this SP
        call pkg_lnkform.SP_LINKFORM(3459,ARRAY_STRING('7848'),ARRAY_STRING('141','134'),  ARRAY_STRING('Sat1 form','two form'),ARRAY_STRING('desc1','desc2'),ARRAY_STRING('S','S'),ARRAY_STRING('M','M'),ARRAY_STRING('3976,4077','3976,4077'),ARRAY_STRING('1421'),'S','510','12.1.1.1',:a)
        **/
    END; --end of SP_LINKFORM
    PROCEDURE SP_FILLED_FORM_NOTIFICATIONS
    AS
    /****************************************************************************************************
    ** Procedure to transfer filled form notification to sch_msgtran for clubbed mails
    ** Author: Sonia Sahni 21th Aug 2003
    ** Input parameter: p_form - form id
    ** Input parameter: p_msg_sendon
    ** Output Parameter: o_ret - returns success code
    **/
    v_users VARCHAR2(500);
    v_msgtext VARCHAR2(4000);
    v_formname VARCHAR2(50);
    v_form NUMBER;
    v_filldate DATE;
    v_formtype CHAR(1);
    v_updatecount NUMBER;
    v_recordtype CHAR(1);
    v_msgtemp_new_form VARCHAR2(4000);
    v_msgtemp_edit_form VARCHAR2(4000);
    v_msg VARCHAR2(4000);
    v_users_param VARCHAR2(600);
    v_users_str VARCHAR2(600);
    v_fparam VARCHAR2(1000);
    V_POS  NUMBER :=0 ;
    v_formstudy VARCHAR2(100);
    bsend  BOOLEAN ;
    v_percode VARCHAR2(50);
    v_formlinkto_text VARCHAR2(50);
    v_logcount NUMBER;
    v_filledformid NUMBER;
    v_record_status VARCHAR2(10);

    BEGIN
        --get message Templates
        v_msgtemp_new_form  :=  Pkg_Common.SCH_GETMAILMSG('c_forms');

        -- NOT in use, now we have same message    template v_msgtemp_edit_form  :=  Pkg_Common.SCH_GETMAILMSG('c_formedit');

        --get all filled forms for which noifications are pending, loop for all types of filled forms
        FOR j IN  ( SELECT FK_FORMLIB, f.created_on , 'S' FORM_TYPE, RECORD_TYPE, f.LAST_MODIFIED_DATE, 'N/A' code, pk_studyforms id, fk_study AS fk_study
              FROM ER_STUDYFORMS f
        WHERE  NOTIFICATION_SENT = 0 AND NVL(RECORD_TYPE,'D') <> 'D'
        UNION
        SELECT FK_FORMLIB, created_on, 'A' FORM_TYPE, RECORD_TYPE, LAST_MODIFIED_DATE,'N/A' code , pk_acctforms id, 0 AS fk_study
        FROM ER_ACCTFORMS
        WHERE  NOTIFICATION_SENT = 0 AND NVL(RECORD_TYPE,'D') <> 'D'
        UNION
        SELECT FK_FORMLIB, f.created_on , 'P' FORM_TYPE, RECORD_TYPE, f.LAST_MODIFIED_DATE,per_code code, pk_patforms id, NVL( DECODE ( fk_patprot,NULL,0, ( SELECT fk_study FROM ER_PATPROT WHERE pk_patprot = fk_patprot)) , 0) AS fk_study
        FROM ER_PATFORMS f, ER_PER
        WHERE  NOTIFICATION_SENT = 0 AND NVL(RECORD_TYPE,'D') <> 'D' AND pk_per = fk_per
        ORDER BY FORM_TYPE,FK_FORMLIB,RECORD_TYPE DESC,2)
        LOOP

            v_form := j.FK_FORMLIB;
            v_filledformid := j.id;

            v_formtype := j.FORM_TYPE;

            v_recordtype := j.RECORD_TYPE;
            v_percode := j.code;
            v_formstudy := '';

            IF v_recordtype = 'N' THEN
                v_filldate := j.created_on;
                v_record_status := 'New';
            ELSE
                v_filldate := j.last_modified_date;
                v_record_status := 'Modified';
            END IF;

            IF v_formtype = 'P'  OR v_formtype = 'S' THEN
                -- get study number if its a study patient form
                BEGIN

                SELECT study_number INTO v_formstudy
                FROM ER_STUDY
                WHERE pk_study = j.fk_study;
                EXCEPTION WHEN NO_DATA_FOUND THEN
                v_formstudy := 'N/A';
                END ;

            ELSE
                v_formstudy := 'N/A';
            END IF;

            IF v_formtype = 'P'  THEN
                v_formlinkto_text := 'Patient';
            ELSIF v_formtype = 'S'  THEN
                v_formlinkto_text := 'Study';
            ELSIF v_formtype = 'A'  THEN
                v_formlinkto_text := 'Account';
            END IF;

            -- get Notifications defined for a form
            FOR i IN  (SELECT PK_FN,FN_MSGTEXT,FN_SENDTYPE,FN_USERS,FORM_NAME
            FROM ER_FORMNOTIFY, ER_FORMLIB
            WHERE FK_FORMLIB = v_form AND PK_FORMLIB = FK_FORMLIB AND
            FN_MSGTYPE = 'N' AND NVL(ER_FORMNOTIFY.RECORD_TYPE,'D') <> 'D' AND
            NVL(ER_FORMLIB.RECORD_TYPE,'D') <> 'D' AND
            FN_USERS IS NOT NULL )
            LOOP
                v_users := i.FN_USERS;
                v_msgtext := i.FN_MSGTEXT;
                v_formname := i.FORM_NAME;
                v_users_param := v_users || ',' ;
                v_users_str    := v_users || ',' ;
                v_fparam :=  v_formname  ||'~' || TO_CHAR(v_filldate , PKG_DATEUTIL.f_get_datetimeformat ) ||'~' || v_record_status||'~'||   v_formlinkto_text  ||'~'||  v_formstudy  ||'~'||  v_percode   ||'~'||  v_msgtext ;

                -- changed by sonia abrol, 02/06/06, to sned 'first time notification' only when the record is created
                IF i.FN_SENDTYPE = 'F' THEN
                    -- find out if notification for this record is sent from table ER_FORM_NOTLOG
                    BEGIN

                        SELECT COUNT(*)
                        INTO v_logcount
                        FROM ER_FORM_NOTLOG
                        WHERE fk_formlib = v_form AND  fk_filledform = v_filledformid AND fk_formnotify = i.pk_fn;

                        EXCEPTION WHEN OTHERS THEN

                        v_logcount := 0;
                    END ;

                    plog.DEBUG(pCtx,'v_logcount  for notification ' || v_logcount );

                    IF  v_logcount = 0   THEN
                        bsend := TRUE;
                    ELSE
                        bsend := FALSE;
                    END IF;
                ELSE -- for every time message
                     bsend := TRUE;
                END IF;

                IF bsend = TRUE THEN -- send mail
                    IF i.FN_SENDTYPE = 'F' THEN
                        -- INSERT A LOG ENTRY
                        INSERT INTO ER_FORM_NOTLOG(  pk_log, fk_formlib, fk_filledform,  fk_formnotify, sent_on)
                        VALUES(SEQ_ER_FORM_NOTLOG.NEXTVAL,v_form, v_filledformid  ,i.pk_fn, SYSDATE );
                    END IF;

                    v_msg :=    Pkg_Common.SCH_GETMAIL(v_msgtemp_new_form ,v_fparam);
                    -- removed the templates, now only one message template will be used, 06/20/06

                    -- ITERATE FOR NOTIFICATION RECEPIENTS ******************
                    LOOP
                        V_POS := INSTR (v_users_str, ',');
                        v_users_param := SUBSTR (v_users_str, 1, V_POS - 1);
                        EXIT WHEN v_users_param IS NULL;
                        INSERT INTO sch_msgtran (PK_MSG,
                        MSG_SENDON, MSG_STATUS,
                        MSG_TYPE , FK_SCHEVENT,MSG_TEXT,FK_USER,FK_PATPROT)
                        VALUES (SEQ_MSGTRAN.NEXTVAL ,v_filldate, 0, 'c_forms', NULL, v_msg, v_users_param, NULL) ;
                        v_users_str := SUBSTR (v_users_str, V_POS + 1);
                        P('added for form' || v_form || ' user ' || v_users_param);
                     END LOOP ; /* end loop for multiple users*/
                END IF; -- for bsend
                -----*******************************
            END LOOP    ; -- for notifications
        END LOOP; -- for all filled forms
        --update NOTIFICATION_SENT flag in respective form tables
        UPDATE ER_STUDYFORMS
        SET notification_sent = 1
        WHERE notification_sent = 0 AND NVL(RECORD_TYPE,'D') <> 'D';
        UPDATE ER_ACCTFORMS
        SET notification_sent = 1
        WHERE notification_sent = 0 AND NVL(RECORD_TYPE,'D') <> 'D';
        UPDATE ER_PATFORMS
        SET notification_sent = 1
        WHERE notification_sent = 0 AND NVL(RECORD_TYPE,'D') <> 'D';
        UPDATE ER_CRFFORMS
        SET notification_sent = 1
        WHERE notification_sent = 0 AND NVL(RECORD_TYPE,'D') <> 'D';
        COMMIT;
    END;   -- END OF SP_FILLED_FORM_NOTIFICATIONS
    --------------------
    PROCEDURE SP_COPY_MULTIPLE_STUDY_FORMS (p_studyform_ids ARRAY_STRING, p_form_type NUMBER , p_stdacc_id NUMBER, p_user_id NUMBER , p_form_name VARCHAR2, p_ip_add VARCHAR2  ,  o_ret_number OUT NUMBER)
    AS
    /****************************************************************************************************
    ** Procedure merge single/multiple forms linked to study to make a single form
    ** Author: Sonia Kaura 26th Aug 2003
    ** Input parameter: array of the pk of the forms to be merged
    ** Input parameter: the category of the new form
    ** Input parameter: the study pk , the study to which the new form is linked
    ** Input parameter:the user id of user making the change
    ** Input parameter: the form name of the new form
    ** Input parameter: the ip address
    ** Output Parameter: o_ret - returns success code
    **/
    v_formcnt NUMBER;
    v_form_tocopy NUMBER ;
    v_pk_linkedform NUMBER ;
    v_fk_account NUMBER ;
    v_ret NUMBER ;
    BEGIN
        --get the account id from one of the forms given form merger
         v_fk_account := 0 ;
        SELECT fk_account
        INTO v_fk_account
        FROM ER_USER WHERE
        pk_user = p_user_id;
        Pkg_Form.SP_COPY_MULTIPLE_FORMS(p_studyform_ids , NULL, p_form_type , 'S' , p_stdacc_id , p_user_id  , p_form_name , null, p_ip_add   ,  v_ret);
        IF v_ret = -3 THEN
            o_ret_number := -3 ;
            RETURN ;
        END IF ;
        --get the pk of the new entry into er_linkefforms
        SELECT seq_er_linkedforms.NEXTVAL
        INTO v_pk_linkedform
        FROM dual ;
        --we make an entry into ER_LINKEDFORMS for the new form corresponding to the study
        INSERT INTO ER_LINKEDFORMS
        ( PK_LF ,        FK_FORMLIB ,    LF_DISPLAYTYPE , LF_LNKFROM ,
        LF_ENTRYCHAR , FK_ACCOUNT , FK_STUDY ,
        RECORD_TYPE,  CREATOR    , CREATED_ON,  IP_ADD )
        VALUES
        ( v_pk_linkedform, v_ret , 'S' , 'S' ,
        'M' , v_fk_account, p_stdacc_id,
        'N' , p_user_id , SYSDATE ,p_ip_add ) ;
        o_ret_number := v_ret ;
        COMMIT ;
    END ; -- END OF SP_COPY_MULTIPLE_STUDY_FORMS
    ------
    PROCEDURE  SP_COPY_MULTIPLE_ACCOUNT_FORMS (p_accountform_ids ARRAY_STRING, p_form_type NUMBER , p_user_id NUMBER , p_form_name VARCHAR2, p_ip_add VARCHAR2  ,  o_ret_number OUT NUMBER)
    AS
    /****************************************************************************************************
    ** Procedure merge single/multiple forms linked to study to make a single form
    ** Author: Sonia Kaura 27th Aug 2003
    ** Input parameter: array of the pk of the forms to be merged
    ** Input parameter: the category of the new form
    ** Input parameter:the user id of user making the change
    ** Input parameter: the form name of the new form
    ** Input parameter: the ip address
    ** Output Parameter: o_ret - returns success code
    **/
    v_formcnt NUMBER;
    v_form_tocopy NUMBER ;
    v_pk_linkedform NUMBER ;
    v_fk_account NUMBER ;
    v_ret NUMBER ;
    BEGIN
        --get the account id from one of the forms given form merger
         v_fk_account := 0 ;
        SELECT fk_account
        INTO v_fk_account
        FROM ER_USER WHERE
        pk_user = p_user_id;
        Pkg_Form.SP_COPY_MULTIPLE_FORMS(p_accountform_ids , NULL, p_form_type , 'A' , NULL , p_user_id  , p_form_name , null, p_ip_add   ,  v_ret);
        IF v_ret = -3 THEN
            o_ret_number := -3 ;
            RETURN ;
        END IF ;
        --get the pk of the new entry into er_linkefforms
        SELECT seq_er_linkedforms.NEXTVAL
        INTO v_pk_linkedform
        FROM dual ;
        --we make an entry into ER_LINKEDFORMS for the new form corresponding to the study
        INSERT INTO ER_LINKEDFORMS
        ( PK_LF ,        FK_FORMLIB ,    LF_DISPLAYTYPE , LF_LNKFROM ,
        LF_ENTRYCHAR , FK_ACCOUNT ,  RECORD_TYPE,
        CREATOR    , CREATED_ON,  IP_ADD )
        VALUES
        ( v_pk_linkedform, v_ret , 'A' , 'A' ,
        'M' , v_fk_account,  'N' , p_user_id , SYSDATE ,p_ip_add ) ;
        o_ret_number := v_ret ;
        COMMIT ;
    END   ; -- END OF SP_COPY_MULTIPLE_ACCOUNT_FORMS
    ------------------------------------------------------------------------------------------------------------------------------------
    PROCEDURE SP_FORM_DATA_BROWSER (p_id NUMBER, p_pk_form NUMBER,p_link_from VARCHAR2, p_dt_whereclause VARCHAR2, p_orderby VARCHAR2,p_ordertype VARCHAR2,
    o_sql OUT VARCHAR2, o_cnt_sql OUT VARCHAR2 , o_xmlSql OUT LONG, o_xml_orderBy OUT VARCHAR2)
    AS
    /****************************************************************************************************
    ** Procedure to get the data of forms to make the filled form browser based on the fields selected by the user for showing
    ** in browser. Data is extracted from XML of the respective table based on the location of linked form
    ** Author: Sonika Talwar 27th Aug 2003
    ** Input parameter: account id or study id or patient depending upon where its being called from
    ** Input parameter: form id
    ** Input parameter: type of link form A-account, S-study, P-patient, according to this the appropriate table name is made dynamically
    ** Input prameter: date where clause
    ** Input parameter p_oderBy order by column name
    ** Input parameter p_ordertype order type, possible values 'ASC','DESC'
    ** Output parameter: Sql for extracting data from form xml
    ** Output parameter: Sql for getting total # of records for that form
    ** Modified by Sonika on Dec 22, 03 to add the order by clause
    ** Modified by Sonika on March 12, 04 the study patient forms were earlier based on the patprot_id. now, they are linked with
    **                                    the patient_id only.
    ** Modified by Sonika on April 16, 04 to remove deleted records from the query, added form_status in query
    ** Modified by Sonika on April 30, 04 to show default data entry date as the first column
    ** Modified by Sonika on May 21, 04 to get the field name from er_fldlib instead of extracting from xml
    **                 change was required because every version of xml would have different fields
    **                 removed '.extract(''/rowset/' || v_sysid || '/@name'').getStringVal() header'
    ** Modified by Sonia on Oct 26, 04 to handle point n click sorting
    **/
    v_sysid VARCHAR2(500);
    v_xmlsql VARCHAR2(4000);
    v_valsql VARCHAR2(4000);
    v_sql LONG;
    v_unionsql LONG;
    v_cnt NUMBER;
    v_fldname VARCHAR2(2000);
    v_table_name VARCHAR2(100);
    v_xml_colname VARCHAR2(100);
    v_pk_colname VARCHAR2(100);
    v_fld_datatype VARCHAR2(2);
    v_whereclause VARCHAR2(4000);
    v_orderbydefault VARCHAR2(100);
    v_orderby VARCHAR2(100);
    v_fldlinesno NUMBER;
    v_objectsharesql VARCHAR2(4000);
    v_format VARCHAR2(20) := PKG_DATEUTIL.F_GET_DATEFORMAT;

    v_data_column_name VARCHAR2(20);
    v_orderby_column VARCHAR2(20);

    v_sort_onthis_col BOOLEAN := FALSE;
    v_orderby_param VARCHAR2(20);
    v_orderby_column_text VARCHAR2(4000);
    v_link_from VARCHAR(100);
    v_patprot VARCHAR2(20);
    v_index NUMBER;
    v_addl_flds VARCHAR2(4000);
    v_index_sch NUMBER;
    v_sch_eventpk VARCHAR2(20);
    v_patprotstudy number;

    BEGIN
        --Extract the patprotId if embedded in p_link_form
        --patprotId is sent from formfilledstdpatbrowser .jsp to filter records based in study.
        -- changed the following approach to extract sch_events1_pk

        v_link_from:= p_link_from;

        -- plog.debug(pctx,'v_link_from1*' || v_link_from || '*');

        v_index_sch :=INSTR(v_link_from,'[VELSCHPK]');

        IF v_index_sch > 0 THEN
            v_sch_eventpk := SUBSTR(v_link_from,v_index_sch+10);
            v_link_from:=SUBSTR(v_link_from,0,v_index_sch -1);
        END IF;

        v_index:=INSTR(v_link_from,'[VELSEP]');

        IF ( v_index>0) THEN
            v_patprot:=SUBSTR(v_link_from,v_index+8);
            v_link_from:=SUBSTR(v_link_from,0,v_index-1);
        ELSE
            v_link_from:=v_link_from;
        END IF;

        --plog.debug(pctx,'v_link_from*' || v_link_from || '*');

        IF (v_link_from = 'A') THEN
            v_table_name := 'er_acctforms e,er_linkedforms x';
            v_xml_colname := 'e.acctforms_xml';
            v_pk_colname := 'pk_acctforms,acctforms_filldate';
            v_orderbydefault := ' order by pk_acctforms desc, acctforms_filldate desc ' ;
            v_whereclause := ' e.fk_account = ' || p_id|| ' and e.fk_formlib=x.fk_formlib ' ;
            v_addl_flds:=',''  '' as studyNumber';
        ELSIF (v_link_from = 'S') OR (p_link_from = 'SA') THEN
            v_table_name := 'er_studyforms e ,er_linkedforms x';
            v_xml_colname := 'e.studyforms_xml';
            v_pk_colname := 'pk_studyforms,studyforms_filldate';
            v_whereclause := 'e. fk_study = ' || p_id || ' and e.fk_formlib=x.fk_formlib ' ;
            v_orderbydefault  := ' order by pk_studyforms desc, studyforms_filldate desc ' ;
            v_addl_flds:=',(select study_number  from er_study where pk_study=e.fk_study) as studyNumber';
        ELSIF (v_link_from = 'SP') THEN
            v_table_name := 'er_patforms e,er_linkedforms x';
            v_xml_colname := 'e.patforms_xml';
            v_pk_colname := 'pk_patforms, e.patforms_filldate';
            v_addl_flds := ',( SELECT study_number FROM ER_STUDY WHERE pk_study=(SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=e.fk_patprot))  AS studyNumber' ;

            IF (LENGTH(v_patprot)>0) THEN
                --get study
                select fk_study
                into v_patprotstudy
                from ER_PATPROT where pk_patprot = v_patprot;

                v_whereclause := ' e.fk_per = ' || p_id || ' and e.fk_formlib=x.fk_formlib  and e.fk_patprot in ( select pk_patprot from er_patprot pp where pp.fk_study = '||v_patprotstudy
                || ' and pp.fk_per = ' || p_id ||' )' ;
            ELSE
                v_whereclause := ' e.fk_per = ' || p_id || ' and e.fk_formlib=x.fk_formlib and e.fk_patprot is not null ' ;
            END IF;

            IF (LENGTH(v_sch_eventpk)>0) THEN
                v_whereclause := v_whereclause || ' and e.fk_sch_events1 = '  ||   v_sch_eventpk;
            END IF;

            v_orderbydefault := ' order by pk_patforms desc, patforms_filldate desc ' ;
        ELSIF (v_link_from = 'PA' OR  v_link_from = 'PR')THEN
            v_table_name := 'er_patforms e,er_linkedforms x';
            v_xml_colname := 'e.patforms_xml';
            v_pk_colname := 'pk_patforms,patforms_filldate';
            v_whereclause := ' e.fk_per = ' || p_id || ' and e.fk_formlib=x.fk_formlib ';
            v_whereclause := v_whereclause || ' and (((nvl(LF_DISPLAY_INPAT,0)=1) AND e.fk_patprot is null) or (nvl(LF_DISPLAY_INPAT,0)=0))' ;
            v_orderbydefault := ' order by pk_patforms desc, patforms_filldate desc ' ;
            v_addl_flds:=',''  '' as studyNumber';

            IF (LENGTH(v_sch_eventpk)>0) THEN
                v_whereclause := v_whereclause || ' and e.fk_sch_events1 = '  ||   v_sch_eventpk;
            END IF;
        ELSIF (v_link_from = 'C')THEN
            v_table_name := 'er_crfforms e';
            v_xml_colname := 'e.crfforms_xml';
            v_pk_colname := 'pk_crfforms,crfforms_filldate';
            v_whereclause := ' fk_schevent1 = ' || p_id;
            v_orderbydefault := ' order by pk_crfforms desc, crfforms_filldate desc ' ;
            v_addl_flds:=',''  '' as studyNumber';
        END IF;
        v_cnt:=0;

        v_orderby_param := LOWER(p_orderby);

        v_orderby_column := v_orderby_param;

        --get the system_id for the fields whose browser_flag is 1
        --these system ids would be used to make the XPATH for extracting data from XML
        --Default data entry date should be the first column
        --donot show hidden fields

        FOR i IN
        (SELECT a.fk_field, d.fld_systemid, d.fld_datatype, d.fld_linesno, d.fld_name ,
        CASE WHEN  (d.fld_datatype='ED' AND d.FLD_systemID='er_def_date_01') THEN 0 ELSE 1 END AS seq
        FROM ER_FORMFLD a,
        ER_FORMSEC b,
        ER_FORMLIB c,
        ER_FLDLIB d
        WHERE a.formfld_browserflg = 1
        AND a.fk_formsec = b.pk_formsec
        AND b.fk_formlib = c.pk_formlib
        AND d.pk_field = a.fk_field
        AND c.pk_formlib = p_pk_form
        AND a.record_type <> 'D'
        AND b.record_type <> 'D'
        AND d.record_type <> 'D'
        AND NVL(d.fld_isvisible,0) <> 1
        ORDER BY seq, b.formsec_seq,a.formfld_seq )
        LOOP
            v_cnt := v_cnt +1;

            v_data_column_name := 'col' || v_cnt;

            v_sysid := i.fld_systemid;
            v_fld_datatype := i.fld_datatype;
            v_fldlinesno := i.fld_linesno;
            --make the field names to be displayed as headers
            v_fldname := '''' || Pkg_Util.f_escapeSpecialCharsForXML(i.fld_name) || '''';

            IF  LENGTH(NVL(v_orderby_param,'')) > 0 THEN -- there is an order by column specified
                IF  v_data_column_name = v_orderby_param THEN
                    v_sort_onthis_col := TRUE;
                    v_orderby_column := 'sort_' || v_orderby_param;
                END IF;
            END IF;

            --make the sql dynamically using the datatype, system_id, table name and column name
            --edit box type, lookup type
            IF ( v_fld_datatype = 'ET' ) THEN
                IF (v_fldlinesno > 1) THEN --textarea, remove er_textarea_tag(hardcoded value if no data entered in textarea)

                    v_valsql := v_fldname || ' header' || v_cnt || ', decode(' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/text()'').getStringVal(),''er_textarea_tag'','''',' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/text()'').getStringVal()) ' || v_data_column_name ;

                    IF (v_sort_onthis_col) THEN
                        v_orderby_column_text := 'lower(decode(' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/text()'').getStringVal()),''er_textarea_tag'','''',' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/text()'').getStringVal())) ' || v_orderby_column;
                    END IF;
                ELSE
                    v_valsql :=  v_fldname || ' header' || v_cnt || ', ' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/text()'').getStringVal() ' || v_data_column_name;
                    IF (v_sort_onthis_col) THEN
                        v_orderby_column_text := 'lower(' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/text()'').getStringVal()) ' || v_orderby_column;
                    END IF;
                END IF;
            ELSIF ( v_fld_datatype = 'ED' ) THEN
                  v_valsql :=  v_fldname || ' header' || v_cnt || ', ' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/text()'').getStringVal()  ' || v_data_column_name ;

                IF (v_sort_onthis_col) THEN
                    v_orderby_column_text := 'to_date(' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/text()'').getStringVal() ,''' || v_format || ''') ' || v_orderby_column ;
                END IF;
            ELSIF ( v_fld_datatype = 'EN' ) THEN
                v_valsql :=  v_fldname || ' header' || v_cnt || ', ' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/text()'').getStringVal()  ' || v_data_column_name ;

                IF (v_sort_onthis_col) THEN
                    v_orderby_column_text := ' to_number(' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/text()'').getStringVal()) ' || v_orderby_column  ;
                END IF;
            ELSIF ( v_fld_datatype = 'ML') THEN
                v_valsql :=  v_fldname || ' header' || v_cnt || ', ' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/text()'').getStringVal() ' || v_data_column_name ;

                IF (v_sort_onthis_col) THEN
                    v_orderby_column_text := 'lower(' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/text()'').getStringVal()) ' || v_orderby_column ;
                END IF;
            ELSIF (v_fld_datatype = 'MR' ) THEN
                -- radio button
                v_valsql :=  v_fldname || ' header' || v_cnt || ', ' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/resp[@checked="1"]/@dispval'').getStringVal() ' || v_data_column_name ;

                IF (v_sort_onthis_col) THEN
                    v_orderby_column_text := 'lower(' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/resp[@checked="1"]/@dispval'').getStringVal() )' || v_orderby_column ;
                END IF;
            ELSIF ( v_fld_datatype = 'MC' ) THEN
                -- checkbox, for checkbox data is stored in attribute checkboxesdata
                v_valsql :=  v_fldname || ' header' || v_cnt || ', ' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/@checkboxesdata'').getStringVal() ' || v_data_column_name;

                IF (v_sort_onthis_col) THEN
                    v_orderby_column_text :=   'lower(' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/@checkboxesdata'').getStringVal()) ' || v_orderby_column ;
                END IF;
            ELSIF ( v_fld_datatype = 'MD' ) THEN
                -- dropdown
                v_valsql :=  v_fldname || ' header' || v_cnt || ', ' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/resp[@selected="1"]/@dispval'').getStringVal() ' || v_data_column_name ;

                IF (v_sort_onthis_col) THEN
                    v_orderby_column_text :=   'lower(' || v_xml_colname || '.extract(''/rowset/' || v_sysid || '/resp[@selected="1"]/@dispval'').getStringVal()) ' || v_orderby_column;
                END IF;
            END IF ;

            IF (v_sort_onthis_col) THEN
                v_sort_onthis_col := FALSE;
            END IF;

            IF NVL(v_xmlsql,'0') <> '0' THEN
                v_xmlsql := v_xmlsql || ', ' || v_valsql;
            ELSE
                v_xmlsql := v_valsql;
            END IF;
        END LOOP;

        IF LENGTH(trim(v_orderby_column_text)) > 0 THEN
            v_orderby_column_text  := ' ,' || v_orderby_column_text ;
        END IF;

        --Plog.DEBUG(pCTX,'final sql for data browser' || v_xmlsql);

        IF NVL(v_xmlsql,'0') <> '0' THEN
            v_sql := 'select ' || v_pk_colname || ', fk_formlibver, ''Form Status'' header, (select codelst_desc from er_Codelst where pk_codelst=form_completed) as form_status  '||v_addl_flds||'  ' || v_orderby_column_text || '  from '|| v_table_name || ' where e.fk_formlib = ' || p_pk_form || ' and e.record_type <> ''D'' and ' || v_whereclause || REPLACE(p_dt_whereclause,'[VELSEP]','');
            -- If you change v_sql,please change v_unionsql too, this variable is used to implement response filterting security through form management screen
            --because they will be unioned toegther to get the results.
            v_unionsql:='select ' || v_pk_colname || ', fk_formlibver, ''Form Status'' header, (select codelst_desc from er_Codelst where pk_codelst=form_completed) as form_status  '||v_addl_flds||'  ' || v_orderby_column_text ||  ' from '|| v_table_name || ' where e.fk_formlib = ' || p_pk_form || ' and e.record_type <> ''D'' and ' || v_whereclause|| ' and  PK_LF NOT IN'||
             ' ( SELECT  a.fk_object FROM ER_OBJECTSHARE a   WHERE  a.object_number=6    AND a.objectshare_type=''P'') '|| SUBSTR(p_dt_whereclause,0,INSTR(p_dt_whereclause,'[VELSEP]') -1 ) ;
        ELSE
            v_sql := 'select ' || v_pk_colname || ', fk_formlibver, ''Form Status'' header, (select codelst_desc from er_Codelst where pk_codelst=form_completed) as form_status ' ||  v_addl_flds ||'  from '|| v_table_name || ' where e.fk_formlib = ' || p_pk_form || ' and e.record_type <> ''D'' and ' || v_whereclause ||  REPLACE(p_dt_whereclause,'[VELSEP]','') ;
            v_unionsql:='select ' || v_pk_colname || ', fk_formlibver, ''Form Status'' header, (select codelst_desc from er_Codelst where pk_codelst=form_completed) as form_status   '|| v_addl_flds || ' from ' ||v_table_name || ' where e.fk_formlib = ' || p_pk_form || ' and e.record_type <> ''D'' and ' || v_whereclause ||' and  PK_LF NOT IN'||
            ' ( SELECT  a.fk_object FROM ER_OBJECTSHARE a   WHERE  a.object_number=6    AND a.objectshare_type=''P'')  '  || SUBSTR(p_dt_whereclause,0,INSTR(p_dt_whereclause,'[VELSEP]')-1) ;
        END IF;

        IF LENGTH(NVL(v_orderby_param,'')) > 0 THEN
            v_orderby := ' order by ' || v_orderby_column  || ' ' || p_ordertype;

            o_xml_orderBy := v_orderby_column  || ' ' || p_ordertype;
        ELSE
            v_orderby := v_orderbydefault;

            o_xml_orderBy := '';
        END IF;
        v_sql:=v_sql || ' union ' || v_unionsql || v_orderby;

        o_sql := v_sql;
        --o_cnt_sql := 'select count(*) from ' || v_table_name || ' where e. fk_formlib = ' || p_pk_form || ' and e.record_type <> ''D'' and ' || v_whereclause || p_dt_whereclause;
        o_cnt_sql:='select count(*) from (' ||  v_sql ||')';

        --    v_xmlsql is the sql with data columns, we will process them later
        IF NVL(v_xmlsql,'0') <> '0' THEN
            IF NVL( v_orderby_column,'0') <> '0'  AND LOWER(v_orderby_column) <> 'form_status' THEN -- do not include if its form status, otherwise the column will be repeated
                v_xmlsql := v_xmlsql || ','  || v_orderby_column ;
            END IF;
        END IF;

        o_xmlsql := v_xmlsql  ;

        Plog.DEBUG(pCTX,'final sql for data bowser' || v_sql);

    END; --end of SP_FORM_DATA_BROWSER
    --------------------------------------------------------------------------------------------------------------------------------------
    PROCEDURE SP_GET_FORM_RECORDS (p_id NUMBER, p_pk_form NUMBER,p_link_from VARCHAR2 ,p_dt_whereclause VARCHAR2, p_page NUMBER, p_recs_per_page NUMBER, p_orderby VARCHAR2, p_ordertype VARCHAR2, o_res OUT Gk_Cv_Types.GenericCursorType, o_rows OUT NUMBER)
    AS
    /****************************************************************************************************
    ** Procedure to get the data of forms to make paginated form browser. The procedure internally calls SP_FORM_DATA_BROWSER
    ** to get the required sqls and then passes these sqls SP_GET_PAGE_RECORDS
    ** Author: Sonika Talwar 27th Aug 2003
    ** Input parameter: account id or study id or patient or or patprot id depending upon where its being called from
    ** Input parameter: form id
    ** Input parameter: type of link form A-account, S-study, P-patient
    ** Input parameter: date filter where clause
    ** Input parameter: the page # for which data is required
    ** Input parameter: the # of records to be shown in one page
    ** Input parameter p_oderBy order by column name
    ** Input parameter p_ordertype order type, possible values 'ASC','DESC'
    ** Output parameter: cusror of records
    ** Output parameter: Total number of rows retrieved
    ** Modified by Sonia on Oct 26, 04 to handle point n click sorting
    **/
    v_sql VARCHAR2(4000);
    v_cnt_sql VARCHAR2(4000);
    v_xmlsql LONG ;
    v_xmlorder VARCHAR2(4000);
    FirstRec NUMBER;
    LastRec NUMBER;
    v_finalsql LONG;
    v_table_name VARCHAR2(100);
    v_pk_colname  VARCHAR2(100);
    v_orderbydefault VARCHAR2(200);
    v_index NUMBER;
    v_patprot NUMBER;
    v_link_from VARCHAR2(100);
    v_index_sch  NUMBER;

    v_sch_eventpk VARCHAR2(20);
    BEGIN
        --get sql for data extraction from xml and count of records
        SP_FORM_DATA_BROWSER (p_id, p_pk_form,p_link_from,p_dt_whereclause,p_orderby ,p_ordertype , v_sql,v_cnt_sql,v_xmlsql,v_xmlorder);
        --get the records

        v_link_from:= p_link_from;

        v_index_sch :=INSTR(v_link_from,'[VELSCHPK]');

        IF v_index_sch > 0 THEN
            v_sch_eventpk := SUBSTR(v_link_from,v_index_sch+10);
            v_link_from:=SUBSTR(v_link_from,0,v_index_sch -1);
        END IF;

        v_index:=INSTR(v_link_from,'[VELSEP]');

        IF ( v_index>0) THEN
            v_patprot:=SUBSTR(v_link_from,v_index+8);
            v_link_from:=SUBSTR(v_link_from,0,v_index-1);
        ELSE
            v_link_from:=v_link_from;
        END IF;

        Plog.DEBUG(pCTX,'p_link_from' || v_link_from);

        IF (v_link_from = 'A') THEN
            v_table_name := 'er_acctforms e';
            v_pk_colname := 'pk_acctforms';
            v_orderbydefault :=  'pk_acctforms  ' ;
            --v_orderbydefault :=  'acctforms_filldate  ' ;
        ELSIF (v_link_from = 'S') OR (v_link_from = 'SA') THEN
            v_table_name := 'er_studyforms e';
            v_pk_colname := 'pk_studyforms';
            v_orderbydefault :=  'pk_studyforms  ' ;
            --v_orderbydefault := 'studyforms_filldate  ' ;
        ELSIF (v_link_from = 'SP' OR v_link_from = 'PA' OR v_link_from = 'PR' ) THEN
            v_table_name := 'er_patforms e';
            v_pk_colname := 'pk_patforms';
            v_orderbydefault :=  'pk_patforms  ' ;
            --v_orderbydefault := 'patforms_filldate  ' ;
        ELSIF (v_link_from = 'C')THEN
            v_table_name := 'er_crfforms e';
            v_pk_colname := 'pk_crfforms';
            v_orderbydefault :=  'pk_crfforms  ' ;
            --v_orderbydefault := 'crfforms_filldate  ' ;
        END IF;

        -- modified by sonia abrol, 02/21/06,
        -- instead of using Sp_Get_Page_Records, we will use the logic to append to v_sql, and use its output to get xml data
        -- Sp_Get_Page_Records ( p_page,p_recs_per_page,v_sql,v_cnt_sql,o_res,o_rows);

        SELECT (p_page - 1) * p_recs_per_page
        INTO FirstRec
        FROM dual;

        SELECT p_page * p_recs_per_page + 1
        INTO LastRec
        FROM dual;

        IF NVL(v_xmlsql,'0') <> '0' THEN
        v_xmlsql :=   v_xmlsql || ',' ;
        ELSE
        v_xmlsql := '';
        END IF;

        IF (NVL(LENGTH(v_xmlorder),0) < 1) THEN
            v_xmlorder :=  'e.' || v_orderbydefault  || ' desc ';
        ELSE
            v_xmlorder :=  'o.' || v_xmlorder ;
        END IF;

        -- v_orderbydefault  in the following sql is filled form  date. If default orderby is changed, we will have to change here too
        v_finalsql := ' Select  o.'||  v_pk_colname || ' '  || v_pk_colname ||  '  ,o.' ||  v_orderbydefault  || ' '  ||  v_orderbydefault  || ',  o.fk_formlibver fk_formlibver, '  || v_xmlsql ||  '   o.header header,o.form_status form_status,studyNumber,rnum   from  (select * from ( select a.*, rownum rnum     FROM (' || v_sql ||') a   WHERE ROWNUM < ' || LastRec || ' )  WHERE rnum > ' || FirstRec  ||  ' ) o , ' ||
        v_table_name || '  where o.' ||  v_pk_colname || '  = e.' || v_pk_colname || ' Order By  ' || v_xmlorder ;

        Plog.debug(pCTX,v_finalsql);

        OPEN o_res FOR v_finalsql ;

        --insert into T(T1) values(v_finalsql );
        --commit;

        EXECUTE IMMEDIATE v_cnt_sql INTO o_rows;
    END; --end of SP_GET_FORM_RECORDS
    -----------------------------------------------------------------------------------------------------
    PROCEDURE  SP_UPDATE_FORMORGACC (p_pk_lf NUMBER , p_fk_form_id NUMBER , p_created_by NUMBER , p_ip_add VARCHAR2 ,  p_org_ids ARRAY_STRING , p_delorg_ids ARRAY_STRING , o_ret_number OUT NUMBER)
    AS
    /****************************************************************************************************
    ** Procedure update the organization associated with a form
    ** Author: Sonika Talwar 28th Aug 2003
    ** Input parameter: pk of the linked form
    ** Input parameter: pk of the form which is linked to study/account
    ** Input parameter: ids of the organization to which
    ** Output parameter: the success criteria of the procedure
    **/
    v_orgcnt NUMBER ;
    v_pk_formorgacc NUMBER ;
    v_site_toinsert NUMBER ;
    k NUMBER ;
    BEGIN
        -- total number of the organizations deleted
        v_orgcnt := p_delorg_ids.COUNT();
        k := 1 ;
        WHILE k <= v_orgcnt
        LOOP
        DELETE FROM ER_FORMORGACC  WHERE FK_SITE = TO_NUMBER( p_delorg_ids( k ) ) AND pk_formorgacc IN ( SELECT pk_formorgacc  FROM ER_FORMORGACC  forg ,
        ER_SITE , ER_LINKEDFORMS lf WHERE pk_lf = p_pk_lf  AND
        lf.fk_formlib = p_fk_form_id AND forg.fk_site = pk_site
        AND forg.fk_formlib = lf.fk_formlib  ) ;
         k:= k+1 ;
        END LOOP ;

        -- total number of the organizations added
        v_orgcnt := p_org_ids.COUNT();
        k := 1 ;
        WHILE k <= v_orgcnt
        LOOP
            --get the pk for the new form which can be one form or merge of more forms
            SELECT seq_er_formorgacc.NEXTVAL
            INTO v_pk_formorgacc FROM dual ;
            v_site_toinsert := TO_NUMBER( p_org_ids( k ) );
            --insert into the er_formorgacc table
            INSERT INTO ER_FORMORGACC
            ( PK_FORMORGACC   ,FK_SITE   , FK_FORMLIB    ,
            RECORD_TYPE   , CREATOR , CREATED_ON , IP_ADD )
            VALUES
            ( v_pk_formorgacc , v_site_toinsert , p_fk_form_id ,
            'N' ,  p_created_by  , SYSDATE , p_ip_add  ) ;
            k:= k+1 ;
        END LOOP ;
        o_ret_number := 1 ;
    END  ; --end of SP_UPDATE_FORMORGACC
    ------------------------------------------------------------------------------------------------------------------------------------
    PROCEDURE  SP_UPDATE_FORMGRPACC (p_pk_lf NUMBER , p_fk_form_id NUMBER , p_created_by NUMBER , p_ip_add VARCHAR2 , p_grp_ids ARRAY_STRING , p_delgrp_ids ARRAY_STRING , o_ret_number OUT NUMBER)
    AS
    /****************************************************************************************************
    ** Procedure update the Groups associated with a linked form
    ** Author: Sonika Talwar 11th Dec 2003
    ** Input parameter: pk of the linked form
    ** Input parameter: pk of the form which is linked to study/account
    ** Input parameter: ids of the groups
    ** Output parameter: the success criteria of the procedure -1 error, 1 success
    **/
    v_grpcnt NUMBER ;
    v_pk_formgrpacc NUMBER ;
    v_grp_toinsert NUMBER ;
    k NUMBER ;
    BEGIN
        ---- total number of the groups deleted
        v_grpcnt := p_delgrp_ids.COUNT();
        k := 1 ;
            WHILE k <= v_grpcnt
            LOOP
        DELETE FROM ER_FORMGRPACC  WHERE FK_GROUP = TO_NUMBER( p_delgrp_ids( k ) ) AND pk_formgrpacc IN ( SELECT pk_formgrpacc  FROM ER_FORMGRPACC  fgrp ,
        ER_GRPS , ER_LINKEDFORMS lf WHERE pk_lf = p_pk_lf  AND
        lf.fk_formlib = p_fk_form_id AND fgrp.fk_group = pk_grp
        AND fgrp.fk_formlib = lf.fk_formlib  ) ;
        k:= k+1 ;
        END LOOP ;
        
        -- total number of the groups added
        v_grpcnt := p_grp_ids.COUNT();
        k := 1 ;
        BEGIN
            WHILE k <= v_grpcnt
            LOOP
                --get the pk for the new form which can be one form or merge of more forms
                SELECT seq_er_formgrpacc.NEXTVAL
                INTO v_pk_formgrpacc FROM dual ;
                v_grp_toinsert := TO_NUMBER( p_grp_ids( k ) );
                --insert into the er_formgrpacc table
                INSERT INTO ER_FORMGRPACC
                ( PK_FORMGRPACC   ,FK_GROUP   , FK_FORMLIB    ,
                RECORD_TYPE  , CREATOR , CREATED_ON , IP_ADD )
                VALUES
                ( v_pk_formgrpacc , v_grp_toinsert , p_fk_form_id ,
                'N' ,  p_created_by , SYSDATE , p_ip_add  ) ;
                k:= k+1 ;
            END LOOP ;
            EXCEPTION  WHEN OTHERS THEN P('ERROR');
            o_ret_number :=-1;
            RETURN;
        END;
        o_ret_number := 1 ;
    END  ; --end of SP_UPDATE_FORMGRPACC
    ------------------------------------------------------------------------------------------------------------------------------------
    PROCEDURE SP_LINK_CRFFORMS(p_formIds ARRAY_STRING,p_account VARCHAR2, p_names ARRAY_STRING,p_numbers ARRAY_STRING,p_calendar NUMBER, p_event NUMBER,p_user VARCHAR2,p_ipadd VARCHAR2, o_ret_number OUT NUMBER)
    AS
    /****************************************************************************************************
    ** Procedure to store data for forms linked to CRF
    ** Author: Anu Khanna 2/12/2003
    ** Input parameter: Form Ids
    ** Input parameter: account Id
    ** Input parameter: names of the forms to be linked to CRF
    ** Input parameter: crf numbers of the form
    ** Input parameter: calendarId
    ** Input parameter: eventId
    ** Input parameter: creator
    ** Input parameter: Ip Address
    ** Output parameter: returns 0 for success, -1 for error
    ** Modified by Sonika Talwar on March 11, 04, added calendar_id in the input parameter to store in er_linkedforms
    ** Modified by Sonika Talwar on May 27, 04, to create form version when linking forms to crf
    **/
    v_cnt NUMBER;
    v_formId NUMBER;
    v_formtype NUMBER;
    v_entrychar CHAR;
    i NUMBER;
    v_eventid NUMBER;
    v_newcrflibid NUMBER;
    v_newfrmlibid NUMBER;
    v_newlnkfrmid NUMBER;
    v_name VARCHAR2(50);
    v_number VARCHAR2(255);
    v_formdesc VARCHAR2(255);
    v_status NUMBER;
    v_schcnt NUMBER;
    v_ret NUMBER;
    v_arrformid ARRAY_STRING  := ARRAY_STRING();
    BEGIN
        v_cnt := p_names.COUNT();
        i:=1;
        v_eventid := p_event;
        v_arrformid.EXTEND;
        BEGIN
            WHILE i <= v_cnt LOOP
                v_name := p_names(i);
                v_number := p_numbers(i);
                v_formId := p_formIds(i);
                v_arrformid(1) := v_formId;
                --The new crflib id generated from the seq_sch_crflib sequence.
                SELECT seq_sch_crflib.NEXTVAL
                INTO v_newcrflibid
                FROM dual;
                --The new linked form id generated from the seq_er_linkedfroms sequence.
                SELECT seq_er_linkedforms.NEXTVAL
                INTO v_newlnkfrmid
                FROM dual;
                SELECT fk_catlib, form_desc
                INTO v_formtype, v_formdesc
                FROM ER_FORMLIB
                WHERE pk_formlib=v_formId;
                Pkg_Form.SP_COPY_MULTIPLE_FORMS(v_arrformid,v_formdesc,v_formtype,'C',NULL,p_user,v_name,null,p_ipadd,v_ret);
                IF v_ret = -3 THEN
                    o_ret_number := -3 ;
                    RETURN ;
                END IF ;
                --get form entrychar
                SELECT lf_entrychar
                INTO v_entrychar
                FROM ER_LINKEDFORMS
                WHERE fk_formlib = v_formId;
                -- To insert data of the forms linked to CRF to sch_crflib,er_formlib and er_linkedforms.
                INSERT INTO sch_crflib(pk_crflib,fk_events,crflib_number,crflib_name,crflib_flag,
                created_on,creator,ip_add,crflib_formflag)
                VALUES(v_newcrflibid,v_eventid,v_number,v_name,'L',
                SYSDATE,p_user,p_ipadd,1);
                INSERT INTO ER_LINKEDFORMS(pk_lf,fk_formlib,fk_account,lf_displaytype,lf_lnkfrom,lf_entrychar,
                fk_calendar,fk_event,fk_crf,record_type,created_on,creator,ip_add)
                VALUES(v_newlnkfrmid,v_ret,p_account,'C','C',v_entrychar,
                p_calendar,v_eventid,v_newcrflibid,'N',SYSDATE,p_user,p_ipadd);
                --internally create a verison, generate report data and printing xsl
                SP_FORM_ACTIVESTATUS_TASKS(v_ret,0);
                --check if schedule exists for this calendar
                SELECT NVL(COUNT(*),0)
                INTO v_schcnt
                FROM sch_events1
                WHERE session_id = LPAD (TO_CHAR(p_calendar),10,'0')  ;
                P('schedule cnt ' || v_schcnt);

                IF v_schcnt > 0 THEN
                    --if the user adds new crf data of a calendar event associated with a study
                    --then insert new entries for sch_crf table also so that they are reflected in the schedule
                    Pkg_Gensch.sp_transfer_crf_to_schedule (v_newcrflibid,v_eventid,v_number,v_name,p_user, p_ipadd, 1 );
                END IF;
                i := i+1;
            END LOOP;
            EXCEPTION  WHEN OTHERS THEN P('ERROR ' ||SQLERRM);
            o_ret_number:=-1;
            RETURN;
        END;
        o_ret_number:= 1;
        COMMIT;
    END ; --end of SP_LINK_CRFFORMS
    ----------------------------------------------------------------------------------------------
    PROCEDURE SP_SET_OFFLINE_FLAG(p_formId NUMBER, p_flag NUMBER)
    AS
    /****************************************************************************************************
    ** Procedure to set the offline flag, called from trigger er_formlib_au1
    ** Author: Sonika Talwar May 03, 2004
    ** Input parameter: Form Id
    ** Input parameter: value of offline, 1-data was there before the form was made off line for editing.
    **/
    BEGIN
        UPDATE ER_FORMSEC
        SET formsec_offline=p_flag
        WHERE fk_formlib=p_formId;

        UPDATE ER_FORMFLD
        SET formfld_offline = p_flag
        WHERE pk_formfld IN (SELECT pk_formfld
        FROM ER_FORMFLD a, ER_FORMSEC b
        WHERE a.fk_formsec = b.pk_formsec
        AND b.fk_formlib = p_formId);

        UPDATE ER_FLDRESP
        SET fldresp_offline = p_flag
        WHERE pk_fldresp IN (SELECT pk_fldresp
        FROM ER_FORMFLD a, ER_FORMSEC b, ER_FLDRESP c
        WHERE a.fk_formsec = b.pk_formsec
        AND a.fk_field = c.fk_field
        AND b.fk_formlib = p_formId);
    END; --end of SP_SET_OFFLINE_FLAG
    --------------------------------------------------------------------------------------------------------------------------------------

    /* Modified by Sonia Abro, 11/22/06, to handle form version migrationl*/
    PROCEDURE SP_FORM_ACTIVESTATUS_TASKS(p_form NUMBER,  p_migrateFlag IN NUMBER)
    AS
    /****************************************************************************************************
    ** Procedure to perform tasks when form status is changed to active
    ** creates a new verion
    ** generates the Printer Friendly Form XSL
    ** generates form report data
    ** Author: Sonika Talwar May 05, 2004
    ** Input parameter: Form Id
    **/
    v_migration_status NUMBER;
    v_temp NUMBER;
    v_creator NUMBER;
    v_ip VARCHAR2(15);
    v_last_statuspk NUMBER;

    BEGIN
        --generate printer friendly xsl
        Pkg_Filledform.SP_GENERATE_PRINTXSL(p_form);
        --create form report data
        Pkg_Formrep.SP_GENFORMLAYOUT(p_form);
        --create new version
        Pkg_Formver.CREATEFORMVERSION(p_form);

        -- add a new status for migration if the  p_migrateFlag  = 1

        IF  p_migrateFlag  = 1 THEN
            BEGIN
                SELECT pk_codelst
                INTO v_migration_status FROM ER_CODELST
                WHERE trim(codelst_type) = 'frmstat' AND
                trim(codelst_subtyp) = 'M' ;

                UPDATE ER_FORMLIB
                SET form_status = v_migration_status
                WHERE pk_formlib = p_form;

                SELECT creator , ip_add,pk_formstat  INTO v_creator, v_ip, v_last_statuspk
                FROM ER_FORMSTAT
                WHERE fk_formlib = p_form AND FORMSTAT_ENDDATE IS NULL AND ROWNUM < 2;

                UPDATE ER_FORMSTAT
                SET  FORMSTAT_ENDDATE = SYSDATE
                WHERE fk_formlib = p_form AND pk_formstat = v_last_statuspk;


                INSERT INTO ER_FORMSTAT ( PK_FORMSTAT, FK_FORMLIB, FK_CODELST_STAT,
                FORMSTAT_STDATE, FORMSTAT_NOTES, RECORD_TYPE, CREATOR,
                CREATED_ON, IP_ADD,formstat_changedby ) VALUES (
                seq_ER_FORMSTAT.NEXTVAL, P_form, v_migration_status, SYSDATE, 'This status was added by the application when user selected an option to migrate existing form responses to the latest form version',  'N', v_creator,
                SYSDATE, v_ip,v_creator);

                COMMIT;

                EXCEPTION WHEN OTHERS THEN
                plog.fatal(pCtx,'Exception in SP_FORM_ACTIVESTATUS_TASKS : ' || SQLERRM);
            END;
        END IF;
    END; --end of SP_FORM_ACTIVESTATUS_TASKS
END Pkg_Lnkform;
/

CREATE OR REPLACE SYNONYM ESCH.PKG_LNKFORM FOR PKG_LNKFORM;
CREATE OR REPLACE SYNONYM EPAT.PKG_LNKFORM FOR PKG_LNKFORM;

GRANT EXECUTE, DEBUG ON PKG_LNKFORM TO EPAT;
GRANT EXECUTE, DEBUG ON PKG_LNKFORM TO ESCH;