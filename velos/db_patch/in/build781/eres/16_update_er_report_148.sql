set define off;
 update er_report
set REP_SQL_CLOB = ' SELECT ss.pk_submission_status,
TO_CHAR(ss.CREATED_ON, ''dd/mm/yyyy'') CUR_DATE,
ST.STUDY_NUMBER, ST.STUDY_TITLE,
 (select usr_wrkexp || '' '' from er_user where
 pk_user = NVL(ST.STUDY_PRINV, 0))  PER_TITLE,
 (select usr_firstname||'' ''||usr_lastname from er_user where
 pk_user = NVL(ST.STUDY_PRINV, 0)) STUDY_PRINV,
 (select address from er_add era, er_user eru where
 eru.pk_user = NVL(ST.STUDY_PRINV, 0) and eru.fk_peradd = era.pk_add) PER_ADD,
 (select add_city || '' '' from er_add era, er_user eru where
 eru.pk_user = NVL(ST.STUDY_PRINV, 0) and eru.fk_peradd = era.pk_add) PER_CITY,
 (select add_state || '' '' from er_add era, er_user eru where
 eru.pk_user = NVL(ST.STUDY_PRINV, 0) and eru.fk_peradd = era.pk_add) PER_STATE,
 (select add_zipcode from er_add era, er_user eru where
 eru.pk_user = NVL(ST.STUDY_PRINV, 0) and eru.fk_peradd = era.pk_add) PER_ZIPCODE,
 (select site_name from er_site ers, er_user eru where
 eru.pk_user = NVL(ST.STUDY_PRINV, 0) and eru.fk_siteid = ers.pk_site) PER_ORG,
 (select F_CODELST_DESC(fk_codelst_spl) from er_user eru where
 eru.pk_user = NVL(ST.STUDY_PRINV, 0) ) PER_DEPT,
 (select F_CODELST_DESC(ss.submission_status) from dual) submission_status,
 to_char(ss.submission_status_date,pkg_dateutil.f_get_dateformat) submission_status_date,
 (select to_char(meeting_date,pkg_dateutil.f_get_dateformat) from er_review_meeting
 where pk_review_meeting = NVL(esb.fk_review_meeting,0)) meeting_date,
 PK_SUBMISSION_PROVISO,
 FK_USER_ENTEREDBY,usr_lst(FK_USER_ENTEREDBY) User_enteredby_name,
 PROVISO_DATE, PROVISO,
 (select F_CODELST_DESC(esb.submission_review_type) from dual) submission_review_type
 from er_submission sub
 inner join er_study st
 on SUB.FK_STUDY = ST.PK_STUDY
 inner join er_submission_board esb
 on esb.fk_submission = sub.pk_submission
 inner join er_submission_status ss
 on ss.fk_submission = sub.pk_submission and
 ss.fk_submission_board = esb.pk_submission_board
 left outer join er_submission_proviso p
 on (P.FK_SUBMISSION = SUB.PK_SUBMISSION and p.fk_submission_board = :submissionBoardPK and  p.fk_submission_status=ss.pk_submission_status)
 where
 sub.pk_submission = :submissionPK and esb.pk_submission_board = :submissionBoardPK
 and  ss.pk_submission_status<= :pksubmissionStatus
 and ss.pk_submission_status>= :fk_submission_stat
 order by p.PROVISO_DATE desc,
ss.pk_submission_status desc',
REP_FILTERAPPLICABLE='submissionPK:submissionBoardPK:pksubmissionStatus:fk_submission_stat' 
where pk_report=148;
commit;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,380,16,'16_update_er_report_148.sql',sysdate,'v10.1 #781');

commit;