set define off;
declare
begin
ALTER TABLE ERES.ER_OBJECT_SETTINGS
  ADD OBJECT_URL varchar2(100);
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,380,8,'8_obj_settng_add_col.sql',sysdate,'v10.1 #781');

commit;