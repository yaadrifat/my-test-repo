SET DEFINE OFF;
create or replace PROCEDURE SP_INSERTSTUDYNTW(studyId in number,networks in Array_String,userId in number,ip in varchar2) As
i number;
v_pk_stdNet number;
v_count number;
v_study_stat number;
v_affiliated number;
--pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SP_INSERTSTUDYNTW', pLEVEL  => Plog.LFATAL);
BEGIN
	v_count:=networks.count;
	select pk_codelst into v_study_stat from er_codelst where codelst_type='sntwStat' and codelst_subTyp='PEND';
	i:=1;
	while(i<=v_count)loop

		select FK_NWSITES_MAIN into v_affiliated from er_nwsites where pk_nwsites=networks(i);

		SELECT SEQ_ER_STUDYNETWORK.NEXTVAL
		INTO v_pk_stdNet FROM dual ;

		insert into er_studynetwork(PK_STUDYNETWORK,FK_NWSITES,FK_STUDY,CREATOR,CREATED_ON,IP_ADD,AFFILIATED,FK_CODELST_STAT) values(v_pk_stdNet,networks(i),studyId,userId,sysdate,IP,v_affiliated,v_study_stat);
		insert into er_status_history(PK_STATUS,STATUS_MODPK,STATUS_MODTABLE,FK_CODELST_STAT,STATUS_DATE,CREATOR,RECORD_TYPE,IP_ADD,STATUS_ISCURRENT) values(SEQ_ER_STATUS_HISTORY.nextval,v_pk_stdNet,'er_studynetwork',v_study_stat,sysdate,userId,'N',IP,1);
		i:=i+1;
	end loop;
	
END SP_INSERTSTUDYNTW;

/

INSERT INTO track_patches VALUES(seq_track_patches.nextval,454,2,'02_SP_INSERTSTUDYNTW.sql',sysdate,'v11.1.0 #855');

commit;