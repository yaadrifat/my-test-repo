/////*********This readMe is specific to v10.1 build #779	  **********////

The following enhancement is released in the build:

1. Regular bug fixes 39 items.
2. Partial release of Network Tab Enhancement  : ACC-39863 -- Network Tab Feb 03 2017 v0.6.docx

Items that  will be part of Build#779 are :
1.Network tab implementation, with two panes and  switch ON/OFF .  Search functionality on both panes, Audit, Deletion of network items. Facility for the user to change Network type.


Items that will be coverared in future build are :
--------------------------------------------------------------
1. User ICON
2. Document ICON
3. More details implementation  for each network item.
