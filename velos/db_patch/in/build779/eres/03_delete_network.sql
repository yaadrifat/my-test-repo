create or replace
PROCEDURE "DELETE_NETWORK" (p_netId in number,p_userId in varchar2, p_ipAdd in varchar2)
IS

BEGIN
  --delete Network data
  
FOR i IN (SELECT pk_nwsites  FROM ER_NWSITES START WITH pk_nwsites = p_netId CONNECT BY PRIOR pk_nwsites = FK_NWSITES order by pk_nwsites desc) 
LOOP
delete from er_nwsites where pk_nwsites=i.pk_nwsites;
COMMIT;
END LOOP;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,378,03,'03_delete_network.sql',sysdate,'v10.1 #779');

commit;