set define off;
create or replace
TRIGGER "ERES"."ER_NWUSERS_AU0" AFTER UPDATE ON ER_NWUSERS FOR EACH ROW
DECLARE
  raid NUMBER(10);
  old_codetype VARCHAR2(200) ;
  new_codetype VARCHAR2(200) ;
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  usr VARCHAR2(100);
BEGIN


SELECT seq_audit.NEXTVAL INTO raid FROM dual;

usr := Getuser(:NEW.last_modified_by);

Audit_Trail.record_transaction   (raid, 'er_nwusers', :OLD.rid, 'U', usr);



IF NVL(:OLD.PK_NWUSERS,0) !=
     NVL(:NEW.PK_NWUSERS,0) THEN
     Audit_Trail.column_update
       (raid, 'PK_NWUSERS',
       :OLD.PK_NWUSERS, :NEW.PK_NWUSERS);
END IF;

IF NVL(:OLD.FK_NWSITES,' ') !=
     NVL(:NEW.FK_NWSITES,' ') THEN
     Audit_Trail.column_update
       (raid, 'FK_NWSITES',
       :OLD.FK_NWSITES, :NEW.FK_NWSITES);
END IF;

IF NVL(:OLD.FK_USER,' ') !=
     NVL(:NEW.FK_USER,' ') THEN
     Audit_Trail.column_update
       (raid, 'FK_USER',
       :OLD.FK_USER, :NEW.FK_USER);
END IF;

old_modby := '';
new_modby := '';

IF NVL(:OLD.last_modified_by,0) !=    NVL(:NEW.last_modified_by,0) THEN
   BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.last_modified_by;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_modby := NULL;
  END;

 BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.last_modified_by;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_modby := NULL;
 END;
	     Audit_Trail.column_update     (raid, 'LAST_MODIFIED_BY', old_modby, new_modby);
END IF;

IF NVL(:OLD.last_modified_date,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update     (raid, 'LAST_MODIFIED_DATE',    to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
END IF;

IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN Audit_Trail.column_update    (raid, 'RID',  :OLD.rid, :NEW.rid);
END IF;



--JM: 21Feb2008: added auditing for the five columns below..

IF NVL(:OLD.NWU_MEMBERTROLE,' ') !=
     NVL(:NEW.NWU_MEMBERTROLE,' ') THEN
     Audit_Trail.column_update
       (raid, 'NWU_MEMBERTROLE',
       :OLD.NWU_MEMBERTROLE, :NEW.NWU_MEMBERTROLE);
END IF;


IF NVL(:OLD.FK_NWSITES_MAIN,0) !=
     NVL(:NEW.FK_NWSITES_MAIN,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_NWSITES_MAIN',
       :OLD.FK_NWSITES_MAIN, :NEW.FK_NWSITES_MAIN);
END IF;

IF NVL(:OLD.NW_LEVEL,0) !=
     NVL(:NEW.NW_LEVEL,0) THEN
     Audit_Trail.column_update
       (raid, 'NW_LEVEL',
       :OLD.NW_LEVEL, :NEW.NW_LEVEL);
END IF;

END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,378,10,'10_ER_NWUSERS_AU0.sql',sysdate,'v10.1 #779');

commit;
