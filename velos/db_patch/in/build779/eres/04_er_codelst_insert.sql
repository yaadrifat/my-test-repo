set define off;
	
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='networkstat' and CODELST_SUBTYP = 'active';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, LAST_MODIFIED_DATE, CREATED_ON)
	VALUES(
		(SEQ_ER_CODELST.nextval) , NULL, 'networkstat', 'active', 'Active', 'N', 
		1, sysdate,sysdate);
  end if;
  v_record_exists:=0;
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='networkstat' and CODELST_SUBTYP = 'inactive';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, LAST_MODIFIED_DATE, CREATED_ON)
	VALUES(
		(SEQ_ER_CODELST.nextval) , NULL, 'networkstat', 'inactive', 'Inactive', 'N', 
		2, sysdate,sysdate);
  end if;
  v_record_exists:=0;
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='networkstat' and CODELST_SUBTYP = 'pending';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, LAST_MODIFIED_DATE, CREATED_ON)
	VALUES(
		(SEQ_ER_CODELST.nextval) , NULL, 'networkstat', 'pending', 'Pending', 'N', 
		3, sysdate,sysdate);
  end if;
commit;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,378,04,'04_er_codelst_insert.sql',sysdate,'v10.1 #779');

commit;