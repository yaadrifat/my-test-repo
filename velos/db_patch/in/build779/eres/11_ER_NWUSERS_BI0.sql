set define off;
create or replace
TRIGGER "ERES"."ER_NWUSERS_BI0" BEFORE INSERT ON ER_NWUSERS        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;


  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_NWUSERS',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'PK_NWUSERS', :NEW.PK_NWUSERS);
       Audit_Trail.column_insert (raid, 'CREATED_ON', TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'FK_NWSITES', :NEW.FK_NWSITES);
       Audit_Trail.column_insert (raid, 'FK_USER', :NEW.FK_USER);
       Audit_Trail.column_insert (raid, 'NWU_MEMBERTROLE', :NEW.NWU_MEMBERTROLE);
       Audit_Trail.column_insert (raid, 'FK_NWSITES_MAIN', :NEW.FK_NWSITES_MAIN);
       Audit_Trail.column_insert (raid, 'NW_LEVEL', :NEW.NW_LEVEL);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,378,11,'11_ER_NWUSERS_BI0.sql',sysdate,'v10.1 #779');

commit;
