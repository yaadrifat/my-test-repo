set define off;
DECLARE 
	table_check number;
BEGIN
	SELECT COUNT(*) INTO table_check FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_REPXSL';

	IF (table_check > 0) then		
		delete  from  er_repxsl where pk_repxsl in (253);	
		COMMIT;
	END IF;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,462,3,'03_er_repxsl_delete.sql',sysdate,'v11.1.0 #863');

Commit;