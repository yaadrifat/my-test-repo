set define off;
DECLARE 
  index_count INTEGER; 
BEGIN 

  SELECT COUNT(*) into index_count FROM user_indexes WHERE
    index_name = 'IDX_STUDYFKSTUDYUSER' and upper(table_owner) = 'ERES';
  if (index_count = 0) then
execute immediate 'CREATE INDEX "ERES"."IDX_STUDYFKSTUDYUSER" ON "ERES"."ER_STUDY_SITE_RIGHTS" ("FK_STUDY","FK_USER","USER_STUDY_SITE_RIGHTS") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "ERES_USER" ';
  dbms_output.put_line ('Index created IDX_STUDYFKSTUDYUSER');
	ELSE
  dbms_output.put_line ('Index not created IDX_STUDYFKSTUDYUSER');
  END IF;
  
SELECT COUNT(*) into index_count FROM user_indexes WHERE
    index_name = 'INDX_ER_PATPROT_FK_PER' and upper(table_owner) = 'ERES';
  if (index_count = 0) then
execute immediate 'CREATE INDEX "ERES"."INDX_ER_PATPROT_FK_PER" ON "ERES"."ER_PATPROT" ("FK_PER","PATPROT_STAT") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "ERES_USER" ';
  dbms_output.put_line ('Index created INDX_ER_PATPROT_FK_PER');
	ELSE
  dbms_output.put_line ('Index not created INDX_ER_PATPROT_FK_PER');
  END IF;

END;
/

Commit;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,462,1,'01_create_index.sql',sysdate,'v11.1.0 #863');

commit;