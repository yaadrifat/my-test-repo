update er_formslinear set FORM_VERSION=(select formlibver_number from er_formlibver where pk_formlibver=(select fk_formlibver from er_acctforms where pk_acctforms= er_formslinear.FK_FILLEDFORM)) where FK_FILLEDFORM in (select pk_acctforms from ER_ACCTFORMS where ACCT_FORMTYPE='USR' or ACCT_FORMTYPE='NTW' or ACCT_FORMTYPE='ORG') and er_formslinear.FORM_VERSION is  null;
commit;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,462,4,'04_formver_cleanup.sql',sysdate,'v11.1.0 #863');

Commit;