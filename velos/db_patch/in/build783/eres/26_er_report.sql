DECLARE 
	table_check number;
	row_check number;
	update_sql varchar(5000);

BEGIN
	SELECT COUNT(*) INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_REPORT';

	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE rep_name = 'Patient Current Schedule - Upcoming Events' and pk_report=284;

		IF (row_check > 0) then
		
			update_sql:='select
					TO_CHAR(PTSCH_ACTSCH_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) PTSCH_ACTSCH_DATE,
					TO_CHAR(PTSCH_SUGSCH_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) PTSCH_SUGSCH_DATE,
					STUDY_NUMBER,
					ENROLLING_SITE_NAME,
					PAT_STUDY_ID,
					CALENDAR_PK,
					CALENDAR_NAME,
					VISIT_NAME,
					PTSCH_EVENT,
					PTSCH_EVENT_STAT,
					TO_CHAR(EVENTSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) EVENTSTAT_DATE,
					EVENTSTAT_NOTES,
					EVENT_SEQUENCE,
					COVERAGE_TYPE,
					SCHEDULE_STATUS,
					FK_STUDY,
					FK_PER,
					FK_SITE_ENROLLING
					,
					PSTAT_ASSIGNED_TO,
					(SELECT trunc(to_date(ptsch_actsch_date) - sysdate) FROM DUAL) as DIFF
					from vda.vda_v_pat_schedule
					where schedule_status = ''Current'' and ptsch_actsch_date >= trunc(sysdate) and eventstat_date is null
					and fk_study IN (:studyId) and FK_SITE_ENROLLING IN (:orgId) AND TRUNC(PTSCH_ACTSCH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
		
			UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE REP_NAME='Patient Current Schedule - Upcoming Events' AND pk_report=284;		
		END IF; 
	END IF;
COMMIT;
END;
/

DECLARE 
	table_check number;
	row_check number;
	update_sql varchar(5000);

BEGIN
	SELECT COUNT(*) INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_REPORT';

	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE rep_name = 'Patient Current Schedule - Missed Events' and pk_report=285;

		IF (row_check > 0) then
		
			update_sql:='SELECT TO_CHAR(PTSCH_ACTSCH_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) PTSCH_ACTSCH_DATE,
						TO_CHAR(PTSCH_SUGSCH_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) PTSCH_SUGSCH_DATE,
						STUDY_NUMBER,
						ENROLLING_SITE_NAME,
						PAT_STUDY_ID,
						CALENDAR_PK,
						CALENDAR_NAME,
						VISIT_NAME,
						PTSCH_EVENT,
						PTSCH_EVENT_STAT,
						TO_CHAR(EVENTSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) EVENTSTAT_DATE,
						EVENTSTAT_NOTES,
						EVENT_SEQUENCE,
						COVERAGE_TYPE,
						SCHEDULE_STATUS,
						FK_STUDY,
						FK_PER,
						FK_SITE_ENROLLING ,
						PSTAT_ASSIGNED_TO,
						(SELECT trunc(sysdate - to_date(ptsch_actsch_date)) FROM DUAL
						) AS DIFF
					FROM vda.vda_v_pat_schedule
					WHERE schedule_status  = ''Current''
					AND ptsch_actsch_date < TRUNC(sysdate)
					AND eventstat_date    IS NULL
					AND fk_study          IN (:studyId)
					AND FK_SITE_ENROLLING IN (:orgId)
					AND TRUNC(PTSCH_ACTSCH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE REP_NAME='Patient Current Schedule - Missed Events' AND pk_report=285;		
		END IF; 
	END IF;
COMMIT;
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,26,'26_er_report.sql',sysdate,'v10.1 #783');

commit;