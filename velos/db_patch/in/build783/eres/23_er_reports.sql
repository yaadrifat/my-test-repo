DECLARE 
	table_check number;
	row_check number;
	update_sql clob;

BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=258;
		IF (row_check > 0) then
		
		update_sql:='select
study_number,
milestone_type,
msach_milestone_desc,
fk_site_enrolling,
enrolling_site,
msach_ptstudy_id,
TO_CHAR(msach_ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) msach_ach_date,
(milestone_amt/NVL((select MSRUL_PT_COUNT from VDA.VDA_V_MILESTONES a where 
VDA.vda_v_milestone_achvd_det.fk_study = a.fk_study AND a.pk_milestone = VDA.vda_v_milestone_achvd_det.fk_milestone),1)) milestone_amt,
milestone_holdback,
payment_type,
payment_for,
fk_study,
fk_milestone,
inv_amt,
inv_num,
rec_amt,
payment_dt
from VDA.vda_v_milestone_achvd_det
where PAYMENT_SUBTYPE = ''pay''
AND fk_study IN (:studyId)
AND TRUNC(MSACH_ACH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ';
   UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql, REP_COLUMNS='Date Milestone Achievement, Study Number, Payment For, Milestone Description, Enrolling Site, Patient Study ID,  Milestone Amount, Holdback,Payment Date, Reconciled, Outstanding' WHERE pk_report=258;
	END IF;
        
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=257;
		IF (row_check > 0) then
		
		update_sql:='select
study_number,
milestone_type,
msach_milestone_desc,
fk_site_enrolling,
enrolling_site,
msach_ptstudy_id,
TO_CHAR(msach_ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) msach_ach_date,
(milestone_amt/NVL((select MSRUL_PT_COUNT from VDA.VDA_V_MILESTONES a where 
VDA.vda_v_milestone_achvd_det.fk_study = a.fk_study AND a.pk_milestone = VDA.vda_v_milestone_achvd_det.fk_milestone),1)) milestone_amt,
milestone_holdback,
payment_type,
payment_for,
fk_study,
fk_milestone,
inv_amt,
inv_num,
rec_amt,
payment_dt
from VDA.vda_v_milestone_achvd_det
where PAYMENT_SUBTYPE != ''pay''
AND fk_study IN (:studyId)
AND TRUNC(MSACH_ACH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ';
   UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql, REP_COLUMNS='Date Milestone Achievement, Study Number, Milestone Description, Enrolling Site, Patient Study ID,  Milestone Amount, Holdback,Payment Date, Reconciled, Outstanding' WHERE pk_report=257;
	END IF;

	SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=263;
		IF (row_check > 0) then
		
		update_sql:='select  distinct
b.study_number,
study_division,
study_pi,
study_coordinator,
nvl((select sum(msrul_amount) from vda.vda_v_milestones where vda.vda_v_milestones.fk_study = a.fk_study and MSRUL_PAY_SUBTYPE != ''pay''),''0'') as milestone_amt,
nvl((select sum(milestone_holdback) from vda.vda_v_milestones where vda.vda_v_milestones.fk_study = a.fk_study and MSRUL_PAY_SUBTYPE != ''pay''),''0'') as holdback_amt,
fk_study,
nvl((select sum(amount_invoiced) from er_invoice_detail where er_invoice_detail.fk_study = a.fk_study and detail_type=''H''),''0'') as inv_amt,
nvl((select sum(mp_amount)+sum(MP_HOLDBACK_AMOUNT) from er_milepayment_details where fk_milepayment in (select pk_milepayment from er_milepayment where er_milepayment.fk_study = a.fk_study ) and MP_LINKTO_TYPE=''P''),''0'') as rec_amt
from VDA.vda_v_milestone_achvd_det a, VDA.vda_v_study_summary b
where (select MSRUL_PAY_SUBTYPE from vda.vda_v_milestones where vda.vda_v_milestones.pk_milestone = a.fk_milestone) != ''pay''
and fk_study = pk_study
AND fk_study IN (:studyId)
AND TRUNC(MSACH_ACH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ';
   UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=263;
   	END IF;
	SELECT count(*)	INTO row_check	FROM er_report	WHERE pk_report=265;
		IF (row_check > 0) then
			UPDATE ER_REPORT SET REP_COLUMNS='Date Milestone Achievement, Study Number, Milestone Description, Milestone Amount, Holdback,Payment Date, Reconciled, Outstanding' WHERE pk_report=265;					
		END IF;
 END IF;
 END;
 /
 
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,23,'23_er_reports.sql',sysdate,'v10.1 #783');

commit;