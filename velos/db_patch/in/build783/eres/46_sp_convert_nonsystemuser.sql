create or replace
PROCEDURE        "SP_CONVERT_NONSYSTEMUSER" (p_user IN NUMBER,p_group IN NUMBER,p_usrlogname IN VARCHAR2,
p_userpwd IN VARCHAR2,p_useresign IN VARCHAR2, p_useremail VARCHAR2,p_loggedInUser IN NUMBER)
IS
/* Converts a NonSystem user to an 'Active' account user
    By - Sonia
	Date - 10/15/04
	Parameters:

	p_user - The Non System user PK
	p_group - The Goup PK to which the user will be added
	p_usrlogname - The Login name for the user. This should be unique in the database

	If running from SQLPLUS : Before executing the procedure, set option "serveroutput"  on, to see the error messages :

	set serveroutput on

*/
  v_site NUMBER;
  v_success NUMBER;
  v_account NUMBER;
  v_user_count NUMBER;

 BEGIN
  BEGIN
 	   SELECT COUNT(*) INTO v_user_count FROM ER_USER WHERE UPPER(trim(usr_logname)) =  UPPER(trim(p_usrlogname));

	   --JM: shifted up
	   SELECT fk_siteid,fk_account INTO v_site,v_account FROM ER_USER WHERE pk_user = p_user;

    	   IF v_user_count = 0 THEN -- if there is no user with this login name

				 	   UPDATE ER_USER SET
					   usr_sesstime = 30,
					   usr_logname = p_usrlogname,
					   usr_pwd = p_userpwd,
					   fk_grp_default = p_group,
					   usr_pwdxpiry = SYSDATE + 30,
					   usr_pwddays = (select settings_value from er_settings where settings_modname=1 and settings_modnum=v_account
					   			   and settings_keyword='ACC_DAYS_PWD_EXP'),
					   usr_pwdremind = SYSDATE + 30,
					   usr_es = p_useresign,
					   usr_esxpiry =  SYSDATE + 30 ,
					   usr_esdays = (select settings_value from er_settings where settings_modname=1 and settings_modnum=v_account
					   			  and settings_keyword='ACC_DAYS_ESIGN_EXP'),
					   usr_esremind =  SYSDATE + 30,
					   --JM: 10Oct2006
					   fk_timezone = (select settings_value from er_settings where settings_modname=1 and settings_modnum=v_account
					   			  and settings_keyword='ACC_USER_TZ'),
             last_modified_by = p_loggedInUser,      
					   usr_siteflag = 'S',
					   usr_type = 'S'
					   WHERE pk_user = p_user;


				   INSERT INTO ER_USRGRP (pk_usrgrp,fk_user,fk_grp) VALUES (seq_er_usrgrp.NEXTVAL, p_user,p_group);

				   --SELECT fk_siteid,fk_account INTO v_site,v_account FROM ER_USER WHERE pk_user = p_user;

				   Pkg_User.SP_CREATE_USERSITE_DATA (  p_user ,v_account, p_group , NULL, v_site, NULL ,NULL ,'N', v_success );

				   SELECT fk_peradd INTO v_user_count FROM ER_USER WHERE usr_logname = p_usrlogname;

				   IF v_user_count IS NULL THEN
				   	   INSERT INTO ER_ADD (pk_add, add_email) VALUES (seq_er_add.NEXTVAL, p_useremail);
				   ELSE
				   	   UPDATE ER_ADD SET add_email =  p_useremail WHERE pk_add = (SELECT fk_peradd FROM ER_USER WHERE usr_logname = p_usrlogname);
				   END IF;

				  COMMIT;


				ELSE
						 P('User could not be converted. This login name already exists. Please give a different login name and try again.')			;
				END IF;
			EXCEPTION WHEN OTHERS THEN
				 P('Exception:' || SQLERRM);
			END ;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,46,'46_sp_convert_nonsystemuser.sql',sysdate,'v10.1 #783');

commit;
