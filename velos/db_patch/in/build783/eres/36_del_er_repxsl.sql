declare
begin
	delete  from  er_repxsl where pk_repxsl in (253,269,270,271,272);
commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,36,'36_del_er_repxsl.sql',sysdate,'v10.1 #783');

commit;
