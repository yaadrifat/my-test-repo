DECLARE 
	table_check number;
	row_check number;
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE rep_name = 'Admin Schedule Event List' AND
			  pk_report = 5010;
		
		IF (row_check > 0) then	
		  
			UPDATE 
				ER_REPORT 
			SET 
				REP_COLUMNS='Scheduled Date, Study Number, Calendar, Visit, Event, Status, Status Date'		
			WHERE 				
				pk_report=5010 AND 
				rep_name = 'Admin Schedule Event List';
		END IF;
		
	END IF;		
	
	COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,37,'37_update_erReport5010.sql',sysdate,'v10.1 #783');

commit;
