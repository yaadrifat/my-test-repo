set define off;
Declare
	table_check number;
	row_check number;
	update_data varchar2(2000);
	
Begin
	select count(*) into table_check from user_tables 
	where table_name='ER_REPORT';
	
	if(table_check>0) then
		select count(*) into row_check from er_report
		where pk_report=287;
		
		if(row_check>0) then
		
		update_data:='select
study_number,
patient_study_id,
fk_site_enrolling,
(select site_name from er_site where pk_site = fk_site_enrolling) as site,
treatment_name,
drug_info,
TO_CHAR(treatment_status_date,PKG_DATEUTIL.F_GET_DATEFORMAT) treatment_status_date,
TO_CHAR(treatment_end_date,PKG_DATEUTIL.F_GET_DATEFORMAT) treatment_end_date,
treatment_notes,
fk_study,
fk_per
from VDA.vda_v_pat_txarm
where fk_study IN (:studyId)
and fk_site_enrolling IN (:orgId)
AND (TRUNC(treatment_status_date) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
or treatment_status_date is null)';
		
			update er_report set REP_SQL_CLOB=update_data where pk_report=287;
			
		End if;
	End if;
commit;
End;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,41,'41_Update_erReport287.sql',sysdate,'v10.1 #783');

commit;
