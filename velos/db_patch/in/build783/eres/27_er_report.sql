DECLARE 
	table_check number;
	row_check number;
	update_sql varchar2(4000);

BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE PK_REPORT=294 AND REP_TYPE='rep_apr';

		IF (row_check > 0) then
		
			update_sql:='select FK_STUDY,FK_PER,FK_SITE_ENROLLING,STUDY_NUMBER,TO_CHAR(PATPROT_ENROLDT,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_ENROLDT,PSTAT_ENROLL_SITE,
PSTAT_PAT_STUD_ID,pkg_studystat.f_get_patientright(:sessUserId, :orgId, fk_per ) right_mask,
(select PTDEM_GENDER from VDA.VDA_V_PAT_ACCRUAL_ALL where fk_per = VDA.vda_v_pat_accrual.FK_PER and FK_STUDY = VDA.vda_v_pat_accrual.FK_STUDY) as GENDER,
(select PTDEM_MARSTAT from VDA.VDA_V_PAT_ACCRUAL_ALL where fk_per = VDA.vda_v_pat_accrual.FK_PER and FK_STUDY = VDA.vda_v_pat_accrual.FK_STUDY) as MARITAL,
(select PTDEM_EMPLOY from VDA.VDA_V_PAT_ACCRUAL_ALL where fk_per = VDA.vda_v_pat_accrual.FK_PER and FK_STUDY = VDA.vda_v_pat_accrual.FK_STUDY) as EMPLOYMENT,
(select PTDEM_EDUCATION from VDA.VDA_V_PAT_ACCRUAL_ALL where fk_per = VDA.vda_v_pat_accrual.FK_PER and FK_STUDY = VDA.vda_v_pat_accrual.FK_STUDY) as EDUCATION,
(select PTDEM_PRI_ETHNY from VDA.VDA_V_PAT_ACCRUAL_ALL where fk_per = VDA.vda_v_pat_accrual.FK_PER and FK_STUDY = VDA.vda_v_pat_accrual.FK_STUDY) as PRIETHNICITY,
(select PTDEM_PRI_RACE from VDA.VDA_V_PAT_ACCRUAL_ALL where fk_per = VDA.vda_v_pat_accrual.FK_PER and FK_STUDY = VDA.vda_v_pat_accrual.FK_STUDY) as PRIRACE,
(select PTDEM_STATE from VDA.VDA_V_PAT_ACCRUAL_ALL where fk_per = VDA.vda_v_pat_accrual.FK_PER and FK_STUDY = VDA.vda_v_pat_accrual.FK_STUDY) as STATE,
(select PTDEM_COUNTRY from VDA.VDA_V_PAT_ACCRUAL_ALL where fk_per = VDA.vda_v_pat_accrual.FK_PER and FK_STUDY = VDA.vda_v_pat_accrual.FK_STUDY) as COUNTRY,
(select PTDEM_SURVIVSTAT from VDA.VDA_V_PAT_ACCRUAL_ALL where fk_per = VDA.vda_v_pat_accrual.FK_PER and FK_STUDY = VDA.vda_v_pat_accrual.FK_STUDY) as SURVIVAL
from VDA.vda_v_pat_accrual
where fk_study IN (:studyId) and FK_SITE_ENROLLING IN (:orgId) AND TRUNC(PATPROT_ENROLDT) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE PK_REPORT=294 AND REP_TYPE='rep_apr';
		END IF ;
	END IF;
  COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,27,'27_er_report.sql',sysdate,'v10.1 #783');

commit;