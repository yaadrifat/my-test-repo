declare
begin
	delete  from  er_repxsl where pk_repxsl in (121,117);
commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,1,'01_del_Acc_er_repxsl.sql',sysdate,'v10.1 #783');

commit;
