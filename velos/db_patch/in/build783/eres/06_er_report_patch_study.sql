DECLARE 
	table_check number;
	row_check number;
	update_sql clob;
BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=1;

		IF (row_check <> 0) then
			update_sql:='Select
DISTINCT
PK_STUDY,
STUDY_NUMBER,
TO_CHAR(STUDYACTUAL_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDYACTUAL_DATE, STUDY_TITLE, TA, PHASE, RESEARCH_TYPE, STUDY_TYPE, DECODE(PI,NULL,STUDY_OTHERPRINV,PI || 

DECODE(STUDY_OTHERPRINV,NULL,'''',''; '' || STUDY_OTHERPRINV)) AS PI,
(SELECT Pkg_Util.f_join(CURSOR( SELECT  (SELECT site_name FROM ER_SITE WHERE pk_site=a.fk_site) site_name  FROM ER_STUDYSITES a WHERE fk_study=pk_study),'','') FROM 

dual) AS ORGANIZATION,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = study_division) AS division
  FROM erv_studyactive_protocols
  WHERE fk_account = :sessAccId
    AND STUDYACTUAL_DATE <= TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND (studyend_date IS NULL or studyend_date >=  TO_DATE

('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) )
    AND fk_codelst_tarea IN (:tAreaId)
    AND pk_study IN (:studyId)
    AND (study_division IN (:studyDivId) OR study_division IS NULL)
    AND (study_prinv IS NULL OR study_prinv IN (:userId))
    AND EXISTS  (SELECT fk_study FROM ER_STUDYSITES WHERE fk_study=pk_study AND fk_site IN (:orgId) )';
		
		UPDATE ER_REPORT SET REP_NAME='Active Studies (by Timeframe)',REP_DESC='Active Studies (by Timeframe)',REP_COLUMNS='Study Number, Start Date, Title,  Phase, Research Type, Study Type, Division, Therapeutic Area, Organization(s), Principal Investigator',REP_FILTERBY='User (Principal Investigator) </br> Therapeutic Area (Therapeutic Area) </br> Division (Division) </br> Study (Study Number) </br> Organization (Study Site)',REP_FILTERAPPLICABLE='tAreaId:studyId:studyDivId:userId:orgId',REP_SQL_CLOB=update_sql WHERE pk_report=1;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=5010;

		IF (row_check <> 0) then
			update_sql:='SELECT STUDY_NUMBER,FK_STUDY, STUDY_TITLE,TO_CHAR(STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) 

STUDY_ACTUALDT,PROTOCOLID,PROTOCOL_NAME,EVENT_NAME,
  NVL(MONTH,''No Interval Defined'') MONTH, TO_CHAR(ACTUAL_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) st, EVENT_STATUS_DESC as EVENT_STATUS,VISIT_name AS visit,TO_CHAR

(EVENT_EXEON ,PKG_DATEUTIL.F_GET_DATEFORMAT) exe_dt
  FROM erv_adminsch WHERE fk_study IN (:studyId) AND NVL(ACTUAL_SCHDATE,TO_DATE(PKG_DATEUTIL.F_GET_NULL_DATE_STR,PKG_DATEUTIL.F_GET_DATEFORMAT)) BETWEEN TO_DATE

('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',
  PKG_DATEUTIL.F_GET_DATEFORMAT) ORDER BY sort_date,ACTUAL_SCHDATE,study_number';
		
		UPDATE ER_REPORT SET REP_COLUMNS='Scheduled Date, Study Number, Study Start Date, Calendar, Visit, Event, Status, Status Date',REP_FILTERBY='Date 

(Scheduled Date) </br> Study (Study Number)',REP_FILTERAPPLICABLE='date:studyId',REP_SQL_CLOB=update_sql WHERE pk_report=5010;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=5;

		IF (row_check <> 0) then
				update_sql:='select
ER_STUDYSTAT.fk_study,
 (SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study) AS study_number,
 (SELECT site_name FROM ER_SITE WHERE pk_site = fk_site) AS Org,
 TO_CHAR(studystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) AS valid_from,
 (SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = fk_user_docby)AS doc_by,
 codelst_desc,
 DECODE (current_stat,1,''Yes'','''') AS current_stat,
 TO_CHAR(studystat_validt,PKG_DATEUTIL.F_GET_DATEFORMAT) AS valid_to,
 trim(TO_CHAR (studystat_validt, ''MONTH YYYY'')) AS MONTH,
(SELECT sysdate - to_date(studystat_validt) FROM DUAL) as DIFF,
 studystat_note
 FROM ER_CODELST, ER_STUDYSTAT, ER_STUDY, ER_SITE WHERE fk_codelst_studystat IN
(SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''studystat''
AND codelst_subtyp IN (SELECT ctrl_value FROM ER_CTRLTAB WHERE ctrl_key = ''irb_app_stat''))
AND pk_codelst = ER_STUDYSTAT.fk_codelst_studystat AND
ER_STUDYSTAT.fk_study = ER_STUDY.pk_study AND ER_STUDYSTAT.fk_site = ER_SITE.pk_site
AND  fk_study IN (:studyId)  AND
         fk_site IN (:orgId) AND
         studystat_validt BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
ORDER BY study_number, org';
		
		UPDATE ER_REPORT SET REP_NAME='IRB Approval Expiration',REP_DESC='IRB Approval Expiration',REP_COLUMNS='Days Over/Until, Study Number, Organization, Status, Valid From, Valid Until, Documented By,Notes',REP_FILTERBY='Date (Status Valid From) </br> Study (Study Number) </br> Organization (Study Site)',REP_FILTERKEYWORD=':tAreaId',REP_FILTERAPPLICABLE='studyId:orgId:date',REP_SQL_CLOB=update_sql WHERE pk_report=5;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=98;

		IF (row_check <> 0) then	
				update_sql:=' SELECT study_number,
pk_study,
study_title,
DECODE(study_prinv,NULL,DECODE(study_otherprinv,NULL,'''',study_otherprinv),(SELECT usr_lastname || '','' || usr_firstname FROM ER_USER WHERE pk_user = study_prinv) 

|| DECODE(study_otherprinv,NULL,'''',''; '' || study_otherprinv)) AS PI,
(SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = study_coordinator) AS study_contact,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_tarea) AS tarea,
(SELECT TO_CHAR(min(studystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''studystat'' AND codelst_subtyp = 

''active'')
) AS open_for_enroll_dt,
(SELECT TO_CHAR(max(studystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''studystat'' AND codelst_subtyp = 

''active_cls'')
) AS closed_to_accr_dt,
(SELECT TO_CHAR(max(studystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''studystat'' AND codelst_subtyp = 

''prmnt_cls'')
) AS retired_dt,
(SELECT TO_CHAR(max(studystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''studystat'' AND codelst_subtyp = 

(select ctrl_value from ER_CTRLTAB where ctrl_key = ''irb_app_stat''))
) AS chrstat_validfrm_dt,
(SELECT TO_CHAR(max(studystat_validt),PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT  WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''studystat'' AND codelst_subtyp = 

(select ctrl_value from ER_CTRLTAB where ctrl_key = ''irb_app_stat''))
) AS chrstat_validuntil_dt
FROM ER_STUDY a
WHERE fk_account = :sessAccId AND
 ( EXISTS (SELECT fk_study FROM ER_STUDYTEAM WHERE fk_study = pk_study AND fk_user IN (:sessUserId))
     or Pkg_Superuser.F_Is_Superuser(:sessUserId, pk_study) = 1
 )
 AND pk_study IN (:studyId) AND fk_codelst_tarea IN (:tAreaId) AND ((study_division IN (:studyDivId)) OR (study_division IS NULL))
ORDER BY study_number';
		
		UPDATE ER_REPORT SET REP_NAME='Study Summary Timeline',REP_DESC='Study Summary Timeline',REP_COLUMNS='Study Number, Most Recent IRB Approved (Valid From), Most Recent IRB Approved (Valid To), Open to Enrollment, Closed to Enrollment, Study Completed/ Retired',REP_FILTERBY='Therapeutic Area (Therapeutic Area) </br> Division (Division) </br> Study (Study Number)',REP_FILTERAPPLICABLE='studyId:tAreaId:studyDivId',REP_SQL_CLOB=update_sql WHERE pk_report=98;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=273;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 273
				,'Study Status List (All)'
				,'Study Status List (All)'
				,0
				,'N'
				,'Study Number, Study Start Date, Organization, Status Type, Study Status, Current Status, Date From, Date Until, Notes, Documented 

By'
				,'Date (Status Date From) </br> Study (Study Number) </br> Organization (Study Site)'
				,1
				,'rep_spr'
				,''
				,'date:studyId:orgId'
				);
				
				update_sql:='select
STUDY_NUMBER,
TO_CHAR(STUDY_START_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_START_DATE,
SSTAT_SITE_NAME, 
(select codelst_desc from er_codelst WHERE pk_codelst = (select status_type from er_studystat where er_studystat.pk_studystat = VDA.VDA_V_STUDYSTAT.pk_studystat)) as 

STATUSTYPE,
SSTAT_STUDY_STATUS,
TO_CHAR(SSTAT_VALID_FROM,PKG_DATEUTIL.F_GET_DATEFORMAT) SSTAT_VALID_FROM,
TO_CHAR(SSTAT_VALID_UNTIL,PKG_DATEUTIL.F_GET_DATEFORMAT) SSTAT_VALID_UNTIL,
SSTAT_NOTES,
SSTAT_DOCUMNTD_BY,
PK_STUDYSTAT,
SSTAT_FK_SITE,
FK_STUDY,
SSTAT_CURRENT_STAT
from VDA.VDA_V_STUDYSTAT
WHERE  sstat_fk_site IN (:orgId)
 AND  TRUNC(SSTAT_VALID_FROM) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
 AND fk_study IN (:studyId)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=273;		
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=274;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 274
				,'All Studies List'
				,'All Studies List'
				,0
				,'N'
				,'Study Number, Study Title, PI, Coordinator, Division, Therapeutic Area, Phase, Research Type, Study Type, Study Start Date'
				,'Study (Study Number)'
				,1
				,'rep_spr'
				,''
				,'studyId'
				);
				
				update_sql:='select 
PK_STUDY,
STUDY_NUMBER,
STUDY_TITLE,
STUDY_PI,
STUDY_COORDINATOR,
STUDY_DIVISION,
STUDY_TAREA,
STUDY_PHASE,
STUDY_RESEARCH_TYPE,
STUDY_TYPE,
TO_CHAR(STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_ACTUALDT
from VDA.vda_v_study_summary
WHERE  pk_study IN (:studyId)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=274;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=275;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 275
				,'Study Status Key Metrics'
				,'Study Status Key Metrics'
				,0
				,'N'
				,'Study Number, Study Entered On, Initial IRB Approval, Days to IRB Approval, Study Activation Date, Days to Activation, Study Closure 

Date, Days to Closure'
				,'Study (Study Number)'
				,1
				,'rep_spr'
				,''
				,'studyId'
				);
				
				update_sql:='select
STUDY_NUMBER,
STUDY_TITLE,
TO_CHAR(FIRST_NOT_ACTIVE,PKG_DATEUTIL.F_GET_DATEFORMAT) FIRST_NOT_ACTIVE,
TO_CHAR(FIRST_IRB_APPROVED,PKG_DATEUTIL.F_GET_DATEFORMAT) FIRST_IRB_APPROVED,
DAYS_IRB_APPROVAL,
TO_CHAR(FIRST_ACTIVE,PKG_DATEUTIL.F_GET_DATEFORMAT) FIRST_ACTIVE,
DAYS_NOTACTIVE_TO_ACTIVE,
TO_CHAR(LAST_PERM_CLOSURE,PKG_DATEUTIL.F_GET_DATEFORMAT) LAST_PERM_CLOSURE,
DAYS_NOTACTIVE_TO_CLOSURE,
PK_STUDY
from VDA.vda_v_studystatus_metrics
WHERE  pk_study IN (:studyId)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=275;		
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=97;

		IF (row_check <> 0) then	
				update_sql:=' SELECT study_number,
pk_study,
study_title,
DECODE(study_prinv,NULL,DECODE(study_otherprinv,NULL,'''',study_otherprinv),(SELECT usr_lastname || '','' || usr_firstname FROM ER_USER WHERE pk_user = study_prinv) 

|| DECODE(study_otherprinv,NULL,'''',''; '' || study_otherprinv)) AS PI,
(SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = study_coordinator) AS study_contact,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_tarea) AS tarea,
F_Getdis_Site(study_disease_site) AS disease_site,
F_Getlocal_Samplesize(pk_study) AS study_samplsize,
study_nsamplsize,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_phase) AS phase,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_restype) AS restype,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_scope) AS SCOPE,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_type) AS study_type,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_blind) AS blind,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_random) AS RANDOM,
DECODE(fk_codelst_sponsor,NULL,study_sponsor,( SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_sponsor)) study_sponsor,
F_Getmore_Studydetails(pk_study) AS study_details,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = study_division) AS division
FROM ER_STUDY a
WHERE fk_account = :sessAccId AND
 ( EXISTS (SELECT fk_study FROM ER_STUDYTEAM WHERE fk_study = pk_study AND fk_user IN (:sessUserId))
     or Pkg_Superuser.F_Is_Superuser(:sessUserId, pk_study) = 1
 )
 AND pk_study IN (:studyId) AND fk_codelst_tarea IN (:tAreaId) AND ((study_division IN (:studyDivId)) OR (study_division IS NULL))
ORDER BY study_number ';
		
		UPDATE ER_REPORT SET REP_NAME='Study Summary Information',REP_DESC='Study Summary Information',REP_COLUMNS='Study Number, Principal Investigator, Study Contact, Division, Therapeutic Area, Disease Site, Local Sample Site, National Sample Size, Phase, Research Type, Study Scope, Study Type, Binding, Randomization, Sponsored By, Study Details',REP_FILTERBY='Therapeutic Area (Therapeutic Area) </br> Division (Division) </br> Study (Study Number)',REP_FILTERAPPLICABLE='studyId:tAreaId:studyDivId',REP_SQL_CLOB=update_sql WHERE pk_report=97;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=276;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 276
				,'Resource Effort (Not Completed Events)'
				,'Resource Effort (Not Completed Events)'
				,0
				,'N'
				,'Study Number, Organization, Patient Study ID, Calendar, Visit, Event, Resource, Scheduled Date, Duration (hours)'
				,'Date (Scheduled Date) </br> Study (Study Number)'
				,1
				,'rep_spr'
				,''
				,'date:studyId'
				);
				
				update_sql:='select 
fk_study,
study_number,
enrolling_site_name,
pat_study_id,
calendar_name,
calendar_stat,
visit_name,
evrec_event,
evrec_resource,
TO_CHAR(ptsch_actsch_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ptsch_actsch_date,
ptsch_event_stat,
duration_minutes
,
to_char(duration_minutes/60,''9999999999.99'') as duration_hours
from VDA.vda_v_studycal_resource_bypat where ptsch_event_stat != ''Done'' and calendar_stat != ''Deactivated''
AND  TRUNC(ptsch_actsch_date) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
and fk_study IN (:studyId)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=276;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=277;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 277
				,'Study Team Members'
				,'Study Team Members'
				,0
				,'N'
				,'Study Number, Organization, Role, Team Member, Phone, Email,Address, Status on Team'
				,'Study (Study Number)'
				,1
				,'rep_spr'
				,''
				,'studyId'
				);
				
				update_sql:='select 
STUDY_NUMBER,
USER_SITE_NAME,
ROLE,
USER_NAME,
USER_PHONE,
USER_EMAIL,
USER_ADDRESS,
STUDYTEAM_STATUS,
FK_STUDY
from VDA.vda_v_studyteam_members
where fk_study IN (:studyId)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=277;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=278;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 278
				,'Active Studies (by Current Status Flag)'
				,'Active Studies (by Current Status Flag)'
				,0
				,'N'
				,'Study Number,Start Date, Organization, Status Type, Study Status, Date From, Notes, Documented By'
				,'Date (Status Date From) </br> Study (Study Number) </br> Organization (Enrolling Site)'
				,1
				,'rep_spr'
				,''
				,'date:studyId:orgId'
				);
				
				update_sql:='select
STUDY_NUMBER,
TO_CHAR(STUDY_START_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_START_DATE,
SSTAT_SITE_NAME, 
(select codelst_desc from er_codelst WHERE pk_codelst = (select status_type from er_studystat where er_studystat.pk_studystat = VDA.VDA_V_STUDYSTAT.pk_studystat)) as 

STATUSTYPE,
SSTAT_STUDY_STATUS,
TO_CHAR(SSTAT_VALID_FROM,PKG_DATEUTIL.F_GET_DATEFORMAT) SSTAT_VALID_FROM,
TO_CHAR(SSTAT_VALID_UNTIL,PKG_DATEUTIL.F_GET_DATEFORMAT) SSTAT_VALID_UNTIL,
SSTAT_NOTES,
SSTAT_DOCUMNTD_BY,
PK_STUDYSTAT,
SSTAT_FK_SITE,
FK_STUDY,
SSTAT_CURRENT_STAT
from VDA.VDA_V_STUDYSTAT
where pk_studystat = (select pk_studystat from er_studystat where fk_codelst_studystat = (select pk_codelst from er_codelst where codelst_type = ''studystat'' and 

codelst_subtyp = ''active'') and er_studystat.fk_study = VDA.VDA_V_STUDYSTAT.fk_study and current_stat=''1'' )  
AND  sstat_fk_site IN (:orgId)
AND  TRUNC(SSTAT_VALID_FROM) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
AND fk_study IN (:studyId)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=278;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=279;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 279
				,'Resource Effort (Completed Events)'
				,'Resource Effort (Completed Events)'
				,0
				,'N'
				,'Study Number, Organization, Patient Study ID, Calendar, Visit, Event, Resource, Scheduled Date, Duration (hours)'
				,'Date (Scheduled Date) </br> Study (Study Number)'
				,1
				,'rep_spr'
				,''
				,'date:studyId'
				);
				
				update_sql:='select 
fk_study,
study_number,
enrolling_site_name,
pat_study_id,
calendar_name,
calendar_stat,
visit_name,
evrec_event,
evrec_resource,
TO_CHAR(ptsch_actsch_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ptsch_actsch_date,
ptsch_event_stat,
duration_minutes,
to_char(duration_minutes/60,''9999999999.99'') as duration_hours
from VDA.vda_v_studycal_resource_bypat where ptsch_event_stat = ''Done''
AND  TRUNC(ptsch_actsch_date) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
and fk_study IN (:studyId)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=279;		
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=280;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 280
				,'Study Status List (Current)'
				,'Study Status List (Current)'
				,0
				,'N'
				,'Study Number, Study Start Date, Organization, Status Type, Study Status, Date From, Date Until, Notes, Documented By'
				,'Date (Status Date From) </br> Study (Study Number) </br> Organization (Study Site)'
				,1
				,'rep_spr'
				,''
				,'date:studyId'
				);
				
				update_sql:='select
STUDY_NUMBER,
TO_CHAR(STUDY_START_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_START_DATE,
SSTAT_SITE_NAME, 
(select codelst_desc from er_codelst WHERE pk_codelst = (select status_type from er_studystat where er_studystat.pk_studystat = VDA.VDA_V_STUDYSTAT.pk_studystat)) as 

STATUSTYPE,
SSTAT_STUDY_STATUS,
TO_CHAR(SSTAT_VALID_FROM,PKG_DATEUTIL.F_GET_DATEFORMAT) SSTAT_VALID_FROM,
TO_CHAR(SSTAT_VALID_UNTIL,PKG_DATEUTIL.F_GET_DATEFORMAT) SSTAT_VALID_UNTIL,
SSTAT_NOTES,
SSTAT_DOCUMNTD_BY,
PK_STUDYSTAT,
SSTAT_FK_SITE,
FK_STUDY,
SSTAT_CURRENT_STAT
from VDA.VDA_V_STUDYSTAT
where SSTAT_CURRENT_STAT =''Yes''
AND  sstat_fk_site IN (:orgId)
 AND  TRUNC(SSTAT_VALID_FROM) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
 AND fk_study IN (:studyId)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=280;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=94;

		IF (row_check <> 0) then
				update_sql:='SELECT  year1,DECODE(month1,''01'',''January'',''02'',''February'',''03'',''March'',''04'',''April'',''05'',''May'',''06'',''June'',''07'',''July'',''08'',''August'',''09'',''September'',''10'',''October'',''11'',''November'',''December'') AS month1,
       F_Get_Pateve(sunday,'':orgId'',:studyId) AS sunday,
       F_Get_Pateve(monday,'':orgId'',:studyId) AS monday,
       F_Get_Pateve(tuesday,'':orgId'',:studyId) AS tuesday,
       F_Get_Pateve(wednesday,'':orgId'',:studyId) AS wednesday,
       F_Get_Pateve(thursday,'':orgId'',:studyId) AS thursday,
       F_Get_Pateve(friday,'':orgId'',:studyId) AS friday,
       F_Get_Pateve(saturday,'':orgId'',:studyId) AS saturday FROM (
SELECT year1,month1,week1,MAX(sunday) AS SUNDAY, MAX(monday) AS MONDAY, MAX(tuesday) AS TUESDAY, MAX(wednesday) AS WEDNESDAY, MAX(thursday) AS THURSDAY, MAX(friday) AS FRIDAY, MAX(saturday) AS SATURDAY FROM (
SELECT year1,month1,week1,date1, TO_CHAR(date1,''d''),DECODE(TO_CHAR(date1,''d''),1,date1,'''') AS Sunday,
                              DECODE(TO_CHAR(date1,''d''),2,date1,NULL) AS Monday,
                              DECODE(TO_CHAR(date1,''d''),3,date1,NULL) AS Tuesday,
                              DECODE(TO_CHAR(date1,''d''),4,date1,NULL) AS Wednesday,
                              DECODE(TO_CHAR(date1,''d''),5,date1,NULL) AS Thursday,
                              DECODE(TO_CHAR(date1,''d''),6,date1,NULL) AS Friday,
                              DECODE(TO_CHAR(date1,''d''),7,date1,NULL) AS Saturday
                           FROM (
   SELECT TO_CHAR(dt,''yyyy'') AS year1, TO_CHAR(dt,''MM'') AS month1, TO_CHAR(dt+1,''IW'') AS week1 , dt AS date1
   FROM
           (SELECT TO_DATE(TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)) + ROWNUM - 1 AS dt
         FROM ALL_OBJECTS
         WHERE ROWNUM <= TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) - TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) + 1)
         ) GROUP BY year1,week1,month1, date1
) GROUP BY year1,month1,week1
 ORDER BY year1,month1,week1
) ';
		
		UPDATE ER_REPORT SET REP_NAME='Study Visit Calendar (Per Study)',REP_DESC='Study Visit Calendar (Per Study)',REP_COLUMNS='Study Number, Calendar by Month, Patient Study ID, Visit(s), Event(s)',REP_FILTERBY='Date (Visit Date) </br> Study (Study Number)  </br> Organization (Enrolling Site)',REP_FILTERKEYWORD='[DATEFILTER]YM:studyId:orgId',REP_FILTERAPPLICABLE='orgId:studyId:date',REP_SQL_CLOB=update_sql WHERE pk_report=94;		
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=116;

		IF (row_check <> 0) then	
				update_sql:='SELECT (SELECT study_number FROM er_study WHERE pk_study = fk_study) AS study_number,
(SELECT study_title FROM ER_STUDY WHERE pk_study = fk_study) AS study_title,
TO_CHAR((SELECT study_actualdt FROM ER_STUDY WHERE pk_study = fk_study),PKG_DATEUTIL.F_GET_DATEFORMAT) AS study_startdate,
site_name, race,SUM(race_count) AS race_count,
SUM(DECODE(gender,''female'',race_count,0)) AS female_count,
SUM(DECODE(gender,''male'',race_count,0)) AS male_count,
SUM(DECODE(gender,''other'',race_count,0)) AS other_count,
SUM(DECODE(gender,''unknown'',race_count,0)) AS unknown_count,
SUM(DECODE(gender,''zzzz'',race_count,0)) AS gen_ne_count,
SUM(DECODE(ethnicity,''hispanic'',race_count,0)) AS hispanic_count,
SUM(DECODE(ethnicity,''nonhispanic'',race_count,0)) AS nonhispanic_count,
SUM(DECODE(ethnicity,''notreported'',race_count,0)) AS notreported_count,
SUM(DECODE(ethnicity,''unknown'',race_count,0)) AS eunknown_count,
SUM(DECODE(ethnicity,''zzzz'',race_count,0)) AS eth_ne_count
FROM (
SELECT fk_study,
(SELECT site_name FROM ER_SITE WHERE pk_site = fk_site) AS site_name,
DECODE(fk_codelst_race,NULL,''zzzz'',(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_race)) AS race,
DECODE(fk_codelst_gender,NULL,''zzzz'',(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_gender)) AS gender,
DECODE(fk_codelst_ethnicity,NULL,''zzzz'',(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_ethnicity)) AS ethnicity,
COUNT(fk_codelst_race || ''*'' || fk_codelst_gender || ''*'' || fk_codelst_ethnicity) AS race_count
FROM EPAT.person, ER_PATPROT
WHERE pk_person = fk_per AND
fk_study in (:studyId) AND
patprot_enroldt BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND
patprot_stat = 1
GROUP BY
GROUPING SETS ( (fk_study,fk_site,fk_codelst_race,fk_codelst_gender,fk_codelst_ethnicity))
ORDER BY fk_site, fk_codelst_race)
GROUP BY fk_study,site_name, race
ORDER BY site_name, race';
		
		UPDATE ER_REPORT SET REP_NAME='Study Enrollment by Race/Gender (Per Study)',REP_DESC='Study Enrollment by Race/Gender (Per 

Study)',REP_COLUMNS='Enrolling Site, Race, Count, Female, Male, other, Unknown ,Hispanic or Latino, Non- Hispanic, Not Reported, Unknown',REP_FILTERBY='Date (Enrollment Date) </br> Study (Study Number)',REP_FILTERAPPLICABLE='studyId:date',REP_SQL_CLOB=update_sql WHERE pk_report=116;		
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=281;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 281
				,'Study Status and Accrual Overview (Per Study)'
				,'Study Status and Accrual Overview (Per Study)'
				,0
				,'N'
				,'Study Number, Title, PI, Division, Phase, IRB Approval Date, Open to Enrollment Date, Closed to Enrollment Date, Current Status, 

Current Accrual % of Target, Accrual Per Enrolling Site by Status'
				,'Study (Study Number)'
				,1
				,'rep_pat_onestd'
				,''
				,'studyId'
				);
				
				update_sql:=' SELECT 
a.study_number,
a.pk_study,
a.study_title,
DECODE(study_prinv,NULL,DECODE(study_otherprinv,NULL,'''',study_otherprinv),(SELECT usr_lastname || '','' || usr_firstname FROM ER_USER WHERE pk_user = study_prinv) 

|| DECODE(study_otherprinv,NULL,'''',''; '' || study_otherprinv)) AS PI,
F_Getlocal_Samplesize(a.pk_study) AS study_samplsize,
study_nsamplsize,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = study_division) AS division,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_phase) AS phase,
(SELECT TO_CHAR(max(studystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''studystat'' AND codelst_subtyp = 

''active_cls'')
) AS closed_to_accr_dt,
(SELECT TO_CHAR(max(studystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''studystat'' AND codelst_subtyp = 

(select ctrl_value from ER_CTRLTAB where ctrl_key = ''irb_app_stat''))
) AS chrstat_validfrm_dt,
(SELECT TO_CHAR(max(studystat_validt),PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT  WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''studystat'' AND codelst_subtyp = 

(select ctrl_value from ER_CTRLTAB where ctrl_key = ''irb_app_stat''))
) AS chrstat_validuntil_dt,
(select distinct sstat_study_status from vda.vda_v_studystat where sstat_current_stat = ''Yes'' and fk_study = a.pk_study) as currentstat,
TO_CHAR(first_irb_approved,PKG_DATEUTIL.F_GET_DATEFORMAT) first_irb_approved,
TO_CHAR(first_active,PKG_DATEUTIL.F_GET_DATEFORMAT) first_active,
TO_CHAR(last_perm_closure,PKG_DATEUTIL.F_GET_DATEFORMAT) last_perm_closure,
days_irb_approval, days_notactive_to_active,
(SELECT site_name FROM ER_SITE WHERE pk_site = fk_site) AS site_name,
(SELECT TO_CHAR(MIN(patstudystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT x,ER_PER y WHERE pk_per = fk_per AND a.pk_study = x.fk_study AND y.fk_site 

= c.fk_site AND  fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''enrolled'')) AS  first_enrolled_date,
(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PER y WHERE pk_per = fk_per AND a.pk_study = x.fk_study AND y.fk_site = c.fk_site AND  fk_codelst_stat = (SELECT pk_codelst 

FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''enrolled'')) AS  tot_enrolled,
(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PER y WHERE pk_per = fk_per AND a.pk_study = x.fk_study AND y.fk_site = c.fk_site AND  fk_codelst_stat = (SELECT pk_codelst 

FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''screening'')) AS  tot_screened,
(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PER y WHERE pk_per = fk_per AND a.pk_study = x.fk_study AND y.fk_site = c.fk_site AND  fk_codelst_stat = (SELECT pk_codelst 

FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''scrfail'')) AS  tot_screenfail,
(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PER y WHERE pk_per = fk_per AND a.pk_study = x.fk_study AND y.fk_site = c.fk_site AND  current_stat = 1 AND fk_codelst_stat 

= (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''followup'')) AS  tot_followup,
(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PER y WHERE pk_per = fk_per AND a.pk_study = x.fk_study AND y.fk_site = c.fk_site AND  current_stat = 1 AND fk_codelst_stat 

= (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offstudy'')) AS  tot_offstudy,
(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PER y WHERE pk_per = fk_per AND a.pk_study = x.fk_study AND y.fk_site = c.fk_site AND  current_stat = 1 AND fk_codelst_stat 

= (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offtreat'')) AS  tot_offtreat
FROM ER_STUDY a, vda.vda_v_studystatus_metrics b, ER_STUDYSITES c
where a.pk_study = b.pk_study and a.pk_study = fk_study
AND a.pk_study IN (:studyId)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=281;
		END IF;
    END IF;
    
COMMIT;
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,6,'06_er_report_patch_study.sql',sysdate,'v10.1 #783');

commit;

