set define off;
declare
	table_check number;
	row_check number;

Begin
	select count(*) into table_check 
	from user_tables where table_name='ER_REPORT';
	
	if(table_check>0) then
		select count(*) into row_check 
		from er_report where pk_report=271;
		
		if(row_check>0) then
			update er_report set REP_FILTERBY='Date (Date of Service) </br> Study (Study Number)' where pk_report=271;
		end if;
		
		select count(*) into row_check
		from er_report where pk_report=272;
		
		if(row_check>0) then
			update er_report set REP_FILTERBY='Date (Date of Service) </br> Study (Study Number)' where pk_report=272;
		end if;	
	end if;
commit;
end;
/			
	
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,40,'40_Update_erReport271272.sql',sysdate,'v10.1 #783');

commit;
