set define off;

update er_report set rep_hide ='Y' where pk_report in (6,7,8,10,11,13,15,16,23,24,45,48,67,68,70,76,79,82,84,85,86,88,89,91,92,103,113,114,119,122,123,134,135);

delete from er_repxsl where pk_repxsl = 277;
commit;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,12,'12_Hidereports.sql',sysdate,'v10.1 #783');

commit;