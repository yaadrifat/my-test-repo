/////*********This readMe is specific to v10.1 build #783	  **********////

The following BUGS  ARE released in the build:

1. ACC-39863 -- Network Tab
        	1. Organization  More details implementation  for each network item.
	2. Adding User  to network level and user More details implementation. (User Icon)
	3. Document upload facility on network level (Doc Icon)
2.  eC-40173 -- Versioning 
3. V10 Report package integration.
4. Regular bug fixes 54 items.



