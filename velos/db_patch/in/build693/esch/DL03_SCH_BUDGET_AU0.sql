create or replace
TRIGGER "ESCH"."SCH_BUDGET_AU0" 
AFTER UPDATE OF RID,IP_ADD,CREATOR,FK_SITE,FK_STUDY,PK_BUDGET,FK_ACCOUNT,BUDGET_DESC,BUDGET_NAME,BUDGET_RIGHTS,BUDGET_STATUS,BUDGET_CALFLAG,BUDGET_CREATOR,BUDGET_DELFLAG,BUDGET_VERSION,BUDGET_CURRENCY,BUDGET_SITEFLAG,BUDGET_TEMPLATE,LAST_MODIFIED_BY,BUDGET_RIGHTSCOPE
ON ESCH.SCH_BUDGET REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR(2000);
  
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  
    BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.LAST_MODIFIED_BY ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;

  audit_trail.record_transaction
    (raid, 'SCH_BUDGET', :OLD.rid, 'U', usr);

  IF NVL(:OLD.pk_budget,0) !=
     NVL(:NEW.pk_budget,0) THEN
     audit_trail.column_update
       (raid, 'PK_BUDGET',
       :OLD.pk_budget, :NEW.pk_budget);
  END IF;
  IF NVL(:OLD.budget_name,' ') !=
     NVL(:NEW.budget_name,' ') THEN
     audit_trail.column_update
       (raid, 'BUDGET_NAME',
       :OLD.budget_name, :NEW.budget_name);
  END IF;
  IF NVL(:OLD.budget_version,' ') !=
     NVL(:NEW.budget_version,' ') THEN
     audit_trail.column_update
       (raid, 'BUDGET_VERSION',
       :OLD.budget_version, :NEW.budget_version);
  END IF;
  IF NVL(:OLD.budget_desc,' ') !=
     NVL(:NEW.budget_desc,' ') THEN
     audit_trail.column_update
       (raid, 'BUDGET_DESC',
       :OLD.budget_desc, :NEW.budget_desc);
  END IF;
  IF NVL(:OLD.budget_creator,0) !=
     NVL(:NEW.budget_creator,0) THEN
     audit_trail.column_update
       (raid, 'BUDGET_CREATOR',
       :OLD.budget_creator, :NEW.budget_creator);
  END IF;
  IF NVL(:OLD.budget_template,0) !=
     NVL(:NEW.budget_template,0) THEN
     audit_trail.column_update
       (raid, 'BUDGET_TEMPLATE',
       :OLD.budget_template, :NEW.budget_template);
  END IF;
  IF NVL(:OLD.budget_status,' ') !=
     NVL(:NEW.budget_status,' ') THEN
     audit_trail.column_update
       (raid, 'BUDGET_STATUS',
       :OLD.budget_status, :NEW.budget_status);
  END IF;
  IF NVL(:OLD.budget_currency,0) !=
     NVL(:NEW.budget_currency,0) THEN
     audit_trail.column_update
       (raid, 'BUDGET_CURRENCY',
       :OLD.budget_currency, :NEW.budget_currency);
  END IF;
  IF NVL(:OLD.budget_siteflag,' ') !=
     NVL(:NEW.budget_siteflag,' ') THEN
     audit_trail.column_update
       (raid, 'BUDGET_SITEFLAG',
       :OLD.budget_siteflag, :NEW.budget_siteflag);
  END IF;
  IF NVL(:OLD.budget_calflag,' ') !=
     NVL(:NEW.budget_calflag,' ') THEN
     audit_trail.column_update
       (raid, 'BUDGET_CALFLAG',
       :OLD.budget_calflag, :NEW.budget_calflag);
  END IF;
   IF NVL(:OLD.budget_delflag,' ') !=
     NVL(:NEW.budget_delflag,' ') THEN
     audit_trail.column_update
       (raid, 'BUDGET_DELFLAG',
       :OLD.budget_delflag, :NEW.budget_delflag);
  END IF;
  IF NVL(:OLD.fk_study,0) !=
     NVL(:NEW.fk_study,0) THEN
     audit_trail.column_update
       (raid, 'FK_STUDY',
       :OLD.fk_study, :NEW.fk_study);
  END IF;
  IF NVL(:OLD.fk_account,0) !=
     NVL(:NEW.fk_account,0) THEN
     audit_trail.column_update
       (raid, 'FK_ACCOUNT',
       :OLD.fk_account, :NEW.fk_account);
  END IF;
  IF NVL(:OLD.fk_site,0) !=
     NVL(:NEW.fk_site,0) THEN
     audit_trail.column_update
       (raid, 'FK_SITE',
       :OLD.fk_site, :NEW.fk_site);
  END IF;
  IF NVL(:OLD.budget_rightscope,' ') !=
     NVL(:NEW.budget_rightscope,' ') THEN
     audit_trail.column_update
       (raid, 'BUDGET_RIGHTSCOPE',
       :OLD.budget_rightscope, :NEW.budget_rightscope);
  END IF;
  IF NVL(:OLD.budget_rights,' ') !=
     NVL(:NEW.budget_rights,' ') THEN
     audit_trail.column_update
       (raid, 'BUDGET_RIGHTS',
       :OLD.budget_rights, :NEW.budget_rights);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;

  IF NVL(:OLD.FK_CODELST_STATUS,0) !=
     NVL(:NEW.FK_CODELST_STATUS,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_STATUS',
       :OLD.FK_CODELST_STATUS, :NEW.FK_CODELST_STATUS);
  END IF;

  IF NVL(:OLD.BUDGET_COMBFLAG,' ') !=
     NVL(:NEW.BUDGET_COMBFLAG,' ') THEN
     audit_trail.column_update
       (raid, 'BUDGET_COMBFLAG',
       :OLD.BUDGET_COMBFLAG, :NEW.BUDGET_COMBFLAG);
  END IF;
END;
/