set define off;

delete from er_repxsl where pk_repxsl = 158;

commit;

update ER_REPORT
set REP_SQL = 'select pk_formquery, pk_patforms, pk_patprot, FK_FORMLIBVER, pk_study, study_number, fk_site,
(select site_name from er_site where pk_site = fk_site) as site_name,fk_per,PATSTDID,
VISIT_NAME, EVENT_ID, event_name,
formcount, fk_form, form_name, fmod, TO_CHAR(fmod, pkg_dateUtil.f_get_datetimeformat) as umod
from er_study, (select
0 as pk_formquery, pk_patforms, pk_patprot, FK_FORMLIBVER, g.fk_study, f.fk_per, PATPROT_PATSTDID as PATSTDID, FK_SITE_ENROLLING as fk_site,
(select j.EVENT_ID from esch.sch_events1 h, esch.event_assoc j
where j.event_id = h.fk_assoc AND f.FK_SCH_EVENTS1 = f_to_number(h.EVENT_ID) AND g.pk_patprot = h.fk_patprot AND h.status = 0) AS EVENT_ID,
(select j.NAME from esch.sch_events1 h, esch.event_assoc j
where j.event_id = h.fk_assoc AND f.FK_SCH_EVENTS1 = f_to_number(h.EVENT_ID) AND g.pk_patprot = h.fk_patprot AND h.status = 0) AS event_name,
(select VISIT_NO from esch.sch_events1 h, esch.event_assoc j, esch.SCH_PROTOCOL_VISIT k
where j.event_id = h.fk_assoc AND f.FK_SCH_EVENTS1 = f_to_number(h.EVENT_ID)
AND h.fk_visit = k.PK_PROTOCOL_VISIT AND j.fk_visit = k.PK_PROTOCOL_VISIT) AS VISIT_NO,
(select VISIT_NAME from esch.sch_events1 h, esch.event_assoc j, esch.SCH_PROTOCOL_VISIT k
where j.event_id = h.fk_assoc AND f.FK_SCH_EVENTS1 = f_to_number(h.EVENT_ID)
AND h.fk_visit = k.PK_PROTOCOL_VISIT AND j.fk_visit = k.PK_PROTOCOL_VISIT) AS VISIT_NAME,
(select count(*) from ER_PATFORMS pf where pk_formlib = pf.fk_formlib AND pf.fk_per = f.fk_per) formcount,
pk_formlib as fk_form, form_name,
case
  when (f.CREATED_ON is not null and f.LAST_MODIFIED_DATE is null) then f.CREATED_ON
  else f.LAST_MODIFIED_DATE
end As fmod,
f.PATFORMS_FILLDATE
FROM ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g
WHERE pk_formlib = f.fk_formlib AND g.pk_patprot = f.fk_patprot
AND f.fk_per = g.fk_per AND f.RECORD_TYPE <> ''D''
AND e.pk_formlib NOT IN (SELECT lf.fk_formlib FROM er_linkedforms lf WHERE e.pk_formlib = lf.fk_formlib AND (lf.RECORD_TYPE = ''D'' OR LF_HIDE = 1)
  AND (select count(1) from er_patforms pf2 where pf2.RECORD_TYPE <> ''D'' and pf2.FK_FORMLIB = lf.fk_formlib and pf2.fk_patprot = g.pk_patprot) < 1)
AND e.pk_formlib NOT IN (SELECT LF.FK_FORMLIB FROM ER_SETTINGS, ER_LINKEDFORMS lf WHERE
  SETTINGS_KEYWORD = ''FORM_HIDE'' AND SETTINGS_MODNAME = 3
  AND SETTINGS_MODNUM = g.fk_study AND SETTINGS_VALUE = lf.pk_lf 
  AND (select count(1) from er_patforms pf2 where pf2.RECORD_TYPE <> ''D'' and pf2.FK_FORMLIB = lf.fk_formlib and pf2.fk_patprot = g.pk_patprot) < 1)
AND FORM_COMPLETED = :statId
order by PATFORMS_FILLDATE desc nulls last, fmod desc) aa where er_study.pk_study = aa.fk_study
AND pk_study in (:studyId)
AND (pkg_util.f_getStudyRight(pkg_studystat.F_GET_USERRIGHTS_FOR_STUDY(:sessUserId,:studyId),(select CTRL_SEQ from er_ctrltab where CTRL_KEY = ''study_rights'' and upper(ctrl_value) = ''STUDYMPAT'')) > 0)
AND FK_PER > 0 AND (0 < pkg_user.f_chk_studyright_using_pat(FK_PER,FK_STUDY,:sessUserId))
AND fmod between TO_DATE('':fromDate'', pkg_dateUtil.f_get_dateformat) and TO_DATE('':toDate'', pkg_dateUtil.f_get_dateformat)'
,
REP_SQL_CLOB = 'select pk_formquery, pk_patforms, pk_patprot, FK_FORMLIBVER, pk_study, study_number, fk_site,
(select site_name from er_site where pk_site = fk_site) as site_name,fk_per,PATSTDID,
VISIT_NAME, EVENT_ID, event_name,
formcount, fk_form, form_name, fmod, TO_CHAR(fmod, pkg_dateUtil.f_get_datetimeformat) as umod
from er_study, (select
0 as pk_formquery, pk_patforms, pk_patprot, FK_FORMLIBVER, g.fk_study, f.fk_per, PATPROT_PATSTDID as PATSTDID, FK_SITE_ENROLLING as fk_site,
(select j.EVENT_ID from esch.sch_events1 h, esch.event_assoc j
where j.event_id = h.fk_assoc AND f.FK_SCH_EVENTS1 = f_to_number(h.EVENT_ID) AND g.pk_patprot = h.fk_patprot AND h.status = 0) AS EVENT_ID,
(select j.NAME from esch.sch_events1 h, esch.event_assoc j
where j.event_id = h.fk_assoc AND f.FK_SCH_EVENTS1 = f_to_number(h.EVENT_ID) AND g.pk_patprot = h.fk_patprot AND h.status = 0) AS event_name,
(select VISIT_NO from esch.sch_events1 h, esch.event_assoc j, esch.SCH_PROTOCOL_VISIT k
where j.event_id = h.fk_assoc AND f.FK_SCH_EVENTS1 = f_to_number(h.EVENT_ID)
AND h.fk_visit = k.PK_PROTOCOL_VISIT AND j.fk_visit = k.PK_PROTOCOL_VISIT) AS VISIT_NO,
(select VISIT_NAME from esch.sch_events1 h, esch.event_assoc j, esch.SCH_PROTOCOL_VISIT k
where j.event_id = h.fk_assoc AND f.FK_SCH_EVENTS1 = f_to_number(h.EVENT_ID)
AND h.fk_visit = k.PK_PROTOCOL_VISIT AND j.fk_visit = k.PK_PROTOCOL_VISIT) AS VISIT_NAME,
(select count(*) from ER_PATFORMS pf where pk_formlib = pf.fk_formlib AND pf.fk_per = f.fk_per) formcount,
pk_formlib as fk_form, form_name,
case
  when (f.CREATED_ON is not null and f.LAST_MODIFIED_DATE is null) then f.CREATED_ON
  else f.LAST_MODIFIED_DATE
end As fmod,
f.PATFORMS_FILLDATE
FROM ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g
WHERE pk_formlib = f.fk_formlib AND g.pk_patprot = f.fk_patprot
AND f.fk_per = g.fk_per AND f.RECORD_TYPE <> ''D''
AND e.pk_formlib NOT IN (SELECT lf.fk_formlib FROM er_linkedforms lf WHERE e.pk_formlib = lf.fk_formlib AND (lf.RECORD_TYPE = ''D'' OR LF_HIDE = 1)
  AND (select count(1) from er_patforms pf2 where pf2.RECORD_TYPE <> ''D'' and pf2.FK_FORMLIB = lf.fk_formlib and pf2.fk_patprot = g.pk_patprot) < 1)
AND e.pk_formlib NOT IN (SELECT LF.FK_FORMLIB FROM ER_SETTINGS, ER_LINKEDFORMS lf WHERE
  SETTINGS_KEYWORD = ''FORM_HIDE'' AND SETTINGS_MODNAME = 3
  AND SETTINGS_MODNUM = g.fk_study AND SETTINGS_VALUE = lf.pk_lf 
  AND (select count(1) from er_patforms pf2 where pf2.RECORD_TYPE <> ''D'' and pf2.FK_FORMLIB = lf.fk_formlib and pf2.fk_patprot = g.pk_patprot) < 1)
AND FORM_COMPLETED = :statId
order by PATFORMS_FILLDATE desc nulls last, fmod desc) aa where er_study.pk_study = aa.fk_study
AND pk_study in (:studyId)
AND (pkg_util.f_getStudyRight(pkg_studystat.F_GET_USERRIGHTS_FOR_STUDY(:sessUserId,:studyId),(select CTRL_SEQ from er_ctrltab where CTRL_KEY = ''study_rights'' and upper(ctrl_value) = ''STUDYMPAT'')) > 0)
AND FK_PER > 0 AND (0 < pkg_user.f_chk_studyright_using_pat(FK_PER,FK_STUDY,:sessUserId))
AND fmod between TO_DATE('':fromDate'', pkg_dateUtil.f_get_dateformat) and TO_DATE('':toDate'', pkg_dateUtil.f_get_dateformat)' where PK_REPORT = 158;

commit;
