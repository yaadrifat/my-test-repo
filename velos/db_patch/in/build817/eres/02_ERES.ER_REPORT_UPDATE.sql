set define off;
declare
	update_sql varchar2(4000);
	table_check number;

begin
	SELECT count(*)
	INTO table_check FROM user_tables
	WHERE TABLE_NAME = 'ER_REPORT';
	
	if(table_check>0) then
		update_sql:='Select
DISTINCT
PK_STUDY,
STUDY_NUMBER,
TO_CHAR(STUDYACTUAL_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDYACTUAL_DATE, STUDY_TITLE, TA, PHASE, RESEARCH_TYPE, STUDY_TYPE, DECODE(PI,NULL,STUDY_OTHERPRINV,PI ||

DECODE(STUDY_OTHERPRINV,NULL,'''',''; '' || STUDY_OTHERPRINV)) AS PI,
(SELECT Pkg_Util.f_join(CURSOR( SELECT  (SELECT site_name FROM ER_SITE WHERE pk_site=a.fk_site) site_name  FROM ER_STUDYSITES a WHERE fk_study=pk_study),'','') FROM

dual) AS ORGANIZATION,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = study_division) AS division
  FROM erv_studyactive_protocols
  WHERE fk_account = :sessAccId
    AND STUDYACTUAL_DATE <= TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND (studyend_date IS NULL or studyend_date >=  TO_DATE

('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) )
    AND fk_codelst_tarea IN (:tAreaId)
    AND pk_study IN (:studyId)
    AND (study_division IN (:studyDivId))
    AND (study_prinv IS NULL OR study_prinv IN (:userId))
    AND EXISTS  (SELECT fk_study FROM ER_STUDYSITES WHERE fk_study=pk_study AND fk_site IN (:orgId) )';
	
		update er_report set rep_sql=update_sql where pk_report=1;
		
		update_sql:=' SELECT study_number,
pk_study,
study_title,
DECODE(study_prinv,NULL,DECODE(study_otherprinv,NULL,'''',study_otherprinv),(SELECT usr_lastname || '','' || usr_firstname FROM ER_USER WHERE pk_user = study_prinv)

|| DECODE(study_otherprinv,NULL,'''',''; '' || study_otherprinv)) AS PI,
(SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = study_coordinator) AS study_contact,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_tarea) AS tarea,
F_Getdis_Site(study_disease_site) AS disease_site,
F_Getlocal_Samplesize(pk_study) AS study_samplsize,
study_nsamplsize,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_phase) AS phase,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_restype) AS restype,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_scope) AS SCOPE,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_type) AS study_type,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_blind) AS blind,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_random) AS RANDOM,
DECODE(fk_codelst_sponsor,NULL,study_sponsor,( SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_sponsor)) study_sponsor,
F_Getmore_Studydetails(pk_study) AS study_details,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = study_division) AS division
FROM ER_STUDY a
WHERE fk_account = :sessAccId AND
 ( EXISTS (SELECT fk_study FROM ER_STUDYTEAM WHERE fk_study = pk_study AND fk_user IN (:sessUserId))
     or Pkg_Superuser.F_Is_Superuser(:sessUserId, pk_study) = 1
 )
 AND pk_study IN (:studyId) AND fk_codelst_tarea IN (:tAreaId) AND ((study_division IN (:studyDivId)))
ORDER BY study_number ';
		
		update er_report set rep_sql=update_sql where pk_report=97;
		
		update_sql:=' SELECT study_number,
pk_study,
study_title,
DECODE(study_prinv,NULL,DECODE(study_otherprinv,NULL,'''',study_otherprinv),(SELECT usr_lastname || '','' || usr_firstname FROM ER_USER WHERE pk_user = study_prinv)
|| DECODE(study_otherprinv,NULL,'''',''; '' || study_otherprinv)) AS PI,
(SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = study_coordinator) AS study_contact,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_tarea) AS tarea,
(SELECT TO_CHAR(min(studystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''studystat'' AND codelst_subtyp =''active'')
) AS open_for_enroll_dt,
(SELECT TO_CHAR(max(studystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''studystat'' AND codelst_subtyp =''active_cls'')
) AS closed_to_accr_dt,
(SELECT TO_CHAR(max(studystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''studystat'' AND codelst_subtyp =''prmnt_cls'')
) AS retired_dt,
(SELECT TO_CHAR(max(studystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''studystat'' AND codelst_subtyp =
(select ctrl_value from ER_CTRLTAB where ctrl_key = ''irb_app_stat''))
) AS chrstat_validfrm_dt,
(SELECT TO_CHAR(max(studystat_validt),PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT  WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''studystat'' AND codelst_subtyp =
(select ctrl_value from ER_CTRLTAB where ctrl_key = ''irb_app_stat''))
) AS chrstat_validuntil_dt
FROM ER_STUDY a
WHERE fk_account = :sessAccId AND
 ( EXISTS (SELECT fk_study FROM ER_STUDYTEAM WHERE fk_study = pk_study AND fk_user IN (:sessUserId))
     or Pkg_Superuser.F_Is_Superuser(:sessUserId, pk_study) = 1
 )
 AND pk_study IN (:studyId) AND fk_codelst_tarea IN (:tAreaId) AND ((study_division in (:studyDivId)))
ORDER BY study_number';
		
		update er_report set rep_sql=update_sql where pk_report=98;
	
	End if;
commit;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,416,2,'02_ERES.ER_REPORT_UPDATE.sql',sysdate,'v11 #817');
commit;
/