grant select on ESCH.SCH_ADVERSEVE to eres with grant option;
grant select on ESCH.SCH_CODELST to eres with grant option;

/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,416,1,'01_grant_esch.sql',sysdate,'v11 #817');

commit;
/