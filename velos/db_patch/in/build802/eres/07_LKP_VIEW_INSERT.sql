DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LKPCOL where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Organizations' and LKPTYPE_TYPE='dyn_a') 
	and LKPCOL_NAME='NCI_PO_ID';
  if (v_record_exists = 0) then
INSERT INTO ER_LKPCOL 
		(PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_TABLE, LKPCOL_KEYWORD)
	 VALUES((select max(PK_LKPCOL)+1 from ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Organizations' and LKPTYPE_TYPE='dyn_a'),'NCI_PO_ID','NCI PO-ID','varchar','REP_ACCNT_ORGS','NCI_PO_ID');
	 
    commit;
  end if;
end;

/

 DECLARE
  v_record_exists number := 0;  
BEGIN
 Select count(*) into v_record_exists
    from ER_LKPVIEWCOL where FK_LKPVIEW=(select PK_LKPVIEW from ER_LKPVIEW where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Organizations' and LKPTYPE_TYPE='dyn_a') and LKPVIEW_NAME='Organizations') 
	and FK_LKPCOL=(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME='NCI_PO_ID' and FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Organizations' and LKPTYPE_TYPE='dyn_a'));
    if (v_record_exists = 0) then
   INSERT INTO ER_LKPVIEWCOL
  (PK_LKPVIEWCOL,FK_LKPCOL, LKPVIEW_SEQ,FK_LKPVIEW, LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY)
  VALUES((select max(PK_LKPVIEWCOL)+1 from ER_LKPVIEWCOL) ,(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME='NCI_PO_ID' and FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Organizations' and LKPTYPE_TYPE='dyn_a')),(select max(LKPVIEW_SEQ)+1 from ER_LKPVIEWCOL where FK_LKPVIEW=(select PK_LKPVIEW from ER_LKPVIEW where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Organizations' and LKPTYPE_TYPE='dyn_a') and LKPVIEW_NAME='Organizations')),(select PK_LKPVIEW from ER_LKPVIEW where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Organizations' and LKPTYPE_TYPE='dyn_a') and LKPVIEW_NAME='Organizations'),'10%','Y');
 
   commit;
  end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,401,7,'07_LKP_VIEW_INSERT.sql',sysdate,'v11 #802');
commit; 

