/////*********This readMe is specific to v11 build #791**********////
*********************************************************************************************************************************************************************

1. Velos eresearch application is made compatible with Oracle 12c with  build, i.e. V11Build#791.

2. Please go through the following steps before deployment of this build in your environment. ,

3. For Upgrading Oracle10g or 11g to Oracle12c please follow the instructions below. 

	   
4) Change the JAR "ojdbc7-12.1.0.1.jar" at the following locations:-

	a) Go to JBOSS_HOME (Wildlfy server main directory)
	
	b) Go to this location : JBOSS_HOME\modules\system\layers\base\com\oracle\main . 
	
	c) Here  delete the existing Oracle jar file : it could be "classes10G.jar" or "classes10G-10.2.0.jar" . Delete This jar.
	
	d) Place the "ojdbc7-12.1.0.1.jar" in the same location i.e.: JBOSS_HOME\modules\system\layers\base\com\oracle\main\...
	
	e) Now Make changes in module.xml  which is on the same folder i.e.: JBOSS_HOME\modules\system\layers\base\com\oracle\main\module.xml
	
	f) Here at  line number 3  mention the  "ojdbc7-12.1.0.1.jar" name  under Path variable like below 
 
	***************************************************************************	
	<module xmlns="urn:jboss:module:1.1" name="com.oracle">
		<resources>
			<resource-root path="ojdbc7-12.1.0.1.jar"/>
		</resources>
		
		<dependencies>
			<module name="javax.api"/>
			<module name="javax.transaction.api"/>
		</dependencies>
	</module>

	**************************************************************************
	g ) Save the file
	
5) Go to path "JBOSS_HOME/bin" 
	a) Edit the standalone.bat file
	b) Append the following script in standalone.bat file:  [set "JAVA_OPTS=-Doracle.jdbc.autoCommitSpecCompliant=false"]
	c ) Save the file


