set define off;

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "VDA"."VDA_V_STUDYTEAM_MEMBERS" ("STUDY_NUMBER", "STUDY_TITLE", "USER_SITE_NAME", "USER_NAME", "USER_PHONE", "USER_EMAIL", "USER_ADDRESS", "ROLE", "FK_STUDY", "CREATED_ON", "FK_ACCOUNT", "CREATOR", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "RID", "PK_STUDYTEAM", "STUDYTEAM_STATUS", "LAST_MODIFIED_BY_FK", "CREATOR_FK", "USR_DETAILS") AS 
  SELECT study_number,
    study_title,
    (SELECT site_name FROM eres.ER_SITE WHERE pk_site = fk_siteid
    ) ORGANIZATION,
    (SELECT usr_firstname
      || ' '
      || usr_lastname
    FROM eres.ER_USER
    WHERE pk_user = fk_user
    ) user_name,
    add_phone user_phone,
    add_email user_email,
    address
    || NVL2 (address, NVL2 (add_city, ', ', ''), '')
    || add_city
    || NVL2 (add_city, NVL2 (add_state, ', ', ''), '')
    || add_state
    || NVL2 (add_state, NVL2 (add_zipcode, ' - ', ''), '')
    || add_zipcode AS user_address,
    eres.F_GET_CODELSTDESC (fk_codelst_tmrole) ROLE,
    fk_study,
    ER_STUDYTEAM.CREATED_ON,
    ER_USER.fk_account,
    (SELECT USR_FIRSTNAME
      || ' '
      || USR_LASTNAME
    FROM eres.ER_USER
    WHERE PK_USER = ER_STUDYTEAM.CREATOR
    ) CREATOR,
    (SELECT USR_FIRSTNAME
      || ' '
      || USR_LASTNAME
    FROM eres.ER_USER
    WHERE PK_USER = ER_STUDYTEAM.LAST_MODIFIED_BY
    ) LAST_MODIFIED_BY,
    ER_STUDYTEAM.LAST_MODIFIED_DATE,
    ER_STUDYTEAM.RID,
    PK_STUDYTEAM STEAM_RESPONSE_ID,
    STUDYTEAM_STATUS,
    ER_STUDYTEAM.LAST_MODIFIED_BY AS LAST_MODIFIED_BY_FK,
    ER_STUDYTEAM.CREATOR          AS CREATOR_FK,
	eres.f_get_user_details(fk_user) USR_DETAILS
  FROM eres.ER_STUDY,
    eres.ER_STUDYTEAM,
    eres.ER_USER,
    eres.ER_ADD
  WHERE ER_STUDY.pk_study = fk_study
  AND pk_user             = fk_user
  AND pk_add              = fk_peradd
  AND study_team_usr_type = 'D';

  
  COMMENT ON COLUMN "VDA"."VDA_V_STUDYTEAM_MEMBERS"."STUDY_NUMBER" IS 'The Study Number';
   COMMENT ON COLUMN "VDA"."VDA_V_STUDYTEAM_MEMBERS"."STUDY_TITLE" IS 'The Study Title';
   COMMENT ON COLUMN "VDA"."VDA_V_STUDYTEAM_MEMBERS"."USER_SITE_NAME" IS 'The Site or Organization the
team member is linked with';
   COMMENT ON COLUMN "VDA"."VDA_V_STUDYTEAM_MEMBERS"."USER_NAME" IS 'The Study team member name';
   COMMENT ON COLUMN "VDA"."VDA_V_STUDYTEAM_MEMBERS"."USER_PHONE" IS 'The Study Team member''s Phone
number information';
   COMMENT ON COLUMN "VDA"."VDA_V_STUDYTEAM_MEMBERS"."USER_EMAIL" IS 'The Study Team member''s email
information';
   COMMENT ON COLUMN "VDA"."VDA_V_STUDYTEAM_MEMBERS"."USER_ADDRESS" IS 'The Study Team member''s
address information';
   COMMENT ON COLUMN "VDA"."VDA_V_STUDYTEAM_MEMBERS"."ROLE" IS 'The Study Team member''s Role
information';
   COMMENT ON COLUMN "VDA"."VDA_V_STUDYTEAM_MEMBERS"."FK_STUDY" IS 'The FK to the study';
   COMMENT ON COLUMN "VDA"."VDA_V_STUDYTEAM_MEMBERS"."CREATED_ON" IS 'The date the record was created
on (Audit)';
   COMMENT ON COLUMN "VDA"."VDA_V_STUDYTEAM_MEMBERS"."FK_ACCOUNT" IS 'The account the study  is linked
with';
   COMMENT ON COLUMN "VDA"."VDA_V_STUDYTEAM_MEMBERS"."CREATOR" IS 'The user who created the record
(Audit)';
   COMMENT ON COLUMN "VDA"."VDA_V_STUDYTEAM_MEMBERS"."LAST_MODIFIED_BY" IS 'The user who last modified
the record(Audit)';
   COMMENT ON COLUMN "VDA"."VDA_V_STUDYTEAM_MEMBERS"."LAST_MODIFIED_DATE" IS 'The date the record was
last modified on (Audit)';
   COMMENT ON COLUMN "VDA"."VDA_V_STUDYTEAM_MEMBERS"."RID" IS 'The unique Row ID for Audit';
   COMMENT ON COLUMN "VDA"."VDA_V_STUDYTEAM_MEMBERS"."PK_STUDYTEAM" IS 'The Primary Key of the Study
Team record';
   COMMENT ON COLUMN "VDA"."VDA_V_STUDYTEAM_MEMBERS"."STUDYTEAM_STATUS" IS 'The Study Team member''s
status in the team';
   COMMENT ON COLUMN "VDA"."VDA_V_STUDYTEAM_MEMBERS"."LAST_MODIFIED_BY_FK" IS 'The Key to identify the
Last Modified By of the record.';
   COMMENT ON COLUMN "VDA"."VDA_V_STUDYTEAM_MEMBERS"."CREATOR_FK" IS 'The Key to identify the Creator
of the record.';
   COMMENT ON COLUMN "VDA"."VDA_V_STUDYTEAM_MEMBERS"."USR_DETAILS" IS 'This is used to show User more details';
   COMMENT ON TABLE "VDA"."VDA_V_STUDYTEAM_MEMBERS"  IS 'This view provides access to study team
member information for all the studies';
/