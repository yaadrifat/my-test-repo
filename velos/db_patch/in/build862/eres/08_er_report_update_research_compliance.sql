DECLARE 
	table_check number;
	update_sql clob; 
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
	
			update_sql:='select st.pk_study, st.study_number, st.study_title,
(select usr_firstname||'' ''||usr_lastname from er_user where pk_user = st.study_prinv) study_prinv,
f_get_user_details(st.study_prinv) USR_DETAILS,
st.study_division,
sub.pk_submission,
(select codelst_desc from er_codelst where pk_codelst = sub.submission_type) submission_type,
sb.pk_submission_board, rb.review_board_name, ss.pk_submission_status,
(select codelst_desc from er_codelst where pk_codelst = ss.submission_status) submission_status,
TO_CHAR(trunc(ss.submission_status_date),PKG_DATEUTIL.F_GET_DATEFORMAT) submission_status_date
,p.proviso
from er_submission_status ss
inner join er_submission sub on ss.fk_submission = sub.pk_submission
inner join er_submission_board sb on ss.fk_submission_board = sb.pk_submission_board
inner join er_study st on sub.fk_study = st.pk_study
inner join er_review_board rb on rb.pk_review_board = sb.fk_review_board
left outer join er_submission_proviso p on p.fk_submission = sub.pk_submission and p.fk_submission_board = sb.pk_submission_board
where
st.fk_account = :sessAccId
and st.pk_study in (:studyId)
and submission_status_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
order by st.study_number, sub.pk_submission, sb.pk_submission_board, trunc(ss.submission_status_date) desc,
ss.pk_submission_status desc
';

			UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=153;
		COMMIT;
	END IF;		
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,461,8,'08_er_report_update_research_compliance.sql',sysdate,'v11.1.0 #862');

Commit;


