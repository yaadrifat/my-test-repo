set define off;
DECLARE 
	table_check number;
	update_sql clob;
BEGIN
	SELECT COUNT(*) INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_REPORT';

	IF (table_check > 0) then		
			
			update_sql:='select * from (select
			case when (spec.FK_STUDY IS NULL AND SPEC.fk_per IS NOT NULL) THEN ''Patient''
			when (spec.FK_STUDY IS NOT NULL AND SPEC.fk_per IS NULL) THEN ''Study''
			when (spec.FK_STUDY IS NULL AND SPEC.fk_per IS NULL) THEN ''Account''
			when (spec.FK_STUDY IS NOT NULL AND SPEC.fk_per IS NOT NULL) THEN ''Patient Study''
			end as spec_Kind,
			nvl(SPEC.FK_SITE,-1) As fk_site,
			spec.pk_specimen, SPEC_ID,spec_alternate_id,
			(SELECT count(*) FROM ER_SPECIMEN p WHERE p.fk_specimen = spec.pk_specimen) SpecChildCnt,
			(SELECT p.SPEC_ID FROM ER_SPECIMEN p WHERE p.pk_specimen = spec.fk_specimen) PSPEC_ID,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) SPEC_TYPE,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = (SELECT s.FK_CODELST_STATUS FROM er_specimen_status s
			WHERE spec.pk_specimen = s.fk_specimen AND s.PK_SPECIMEN_STATUS = (SELECT MAX(ss.PK_SPECIMEN_STATUS) FROM er_specimen_status ss
			WHERE ss.fk_specimen = spec.pk_specimen AND ss.SS_DATE = (SELECT MAX(ss1.SS_DATE) FROM er_specimen_status ss1
			WHERE ss1.fk_specimen = spec.pk_specimen )))) SPEC_STATUS,
			SPEC_ORIGINAL_QUANTITY || '' ''||(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_quantity_units) SPEC_ORIGINAL_QUANTITY,
			TO_CHAR(spec_collection_date,PKG_DATEUTIL.F_GET_DATEFORMAT)AS spec_collection_date,
			(SELECT site_name FROM ER_SITE WHERE pk_site =  FK_SITE) AS spec_orgn,
			(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = SPEC_ANATOMIC_SITE) SPEC_ANATOMIC_SITE,
			(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = SPEC_TISSUE_SIDE) SPEC_TISSUE_SIDE,
			(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = SPEC_PATHOLOGY_STAT) SPEC_PATHOLOGY_STAT,
			(SELECT study_number FROM ER_STUDY WHERE pk_study = spec.FK_STUDY) STUDY_NUMBER,
			(SELECT per_code FROM ER_PER WHERE pk_per = SPEC.fk_per) AS patid,
			DECODE(spec.fk_specimen,NULL,'''' ,''Yes'') AS child_yes,
			(SELECT spec_id FROM ER_SPECIMEN WHERE pk_specimen = spec.fk_specimen) AS spec_parent,
			stor.pk_storage,STORAGE_ID,STORAGE_NAME,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = FK_CODELST_STORAGE_TYPE) STORE_TYPE,
			(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME  FROM ER_USER where pk_user=spec.FK_USER_PATH ) AS SPEC_PATHOLOGY_NAME,
			(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=spec.FK_USER_SURG ) AS SPEC_SURGEON_NAME,
			(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=spec.FK_USER_COLLTECH ) AS SPEC_COLLTECH_NAME,
			(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=spec.FK_USER_PROCTECH ) AS SPEC_PROCTECH_NAME,
			f_get_user_details(spec.FK_USER_PATH) as PATHOLOGY_NAME_DETAILS,
			f_get_user_details(spec.FK_USER_SURG) as SURGEON_NAME_DETAILS,
			f_get_user_details(spec.FK_USER_COLLTECH) as COLLTECH_NAME_DETAILS,
			f_get_user_details(spec.FK_USER_PROCTECH) as PROCTECH_NAME_DETAILS
			from er_specimen spec, er_storage stor
			where stor.pk_storage(+) = spec.fk_storage
			AND (('':specTyp''=''NonStudy'' AND spec.fk_study is null AND spec.fk_account = :sessAccId) or ('':specTyp''=''Study'' AND spec.fk_study in (:studyId)))
			AND (spec.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = spec.fk_per and per.fk_account =:sessAccId
			and usr.fk_user=:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
			AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
			)
			where FK_SITE IN (:orgId)
			order by storage_id';
			
			UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=155;
			
			update_sql:='select * from (SELECT pk_specimen, spec_id, spec_alternate_id, a.fk_study,
			nvl(a.FK_SITE,-1) As fk_site,
			(SELECT p.SPEC_ID FROM ER_SPECIMEN p WHERE p.pk_specimen = a.fk_specimen) PSEC_ID,
			(SELECT count(*) FROM ER_SPECIMEN p WHERE p.fk_specimen = a.pk_specimen) ChildCnt,
			case when a.fk_study is null then ''[No Study Specified]''
			else (SELECT study_number FROM er_study WHERE pk_study = a.fk_study) end study_number,
			b.SS_DATE,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst=b.FK_CODELST_STATUS) as Spec_Staus,
			case when (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) is null then ''-''
			else (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) end spec_type,
			spec_collection_date,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_anatomic_site) spec_anatomic_site,
			spec_original_quantity,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_quantity_units) spec_quantity_units,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst=b.SS_PROC_TYPE) as SS_PROC_TYPE,
			SS_HAND_OFF_DATE, SS_PROC_DATE, SS_QUANTITY,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = SS_QUANTITY_UNITS) SS_QUANTITY_UNITS,
			(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=b.FK_USER_RECEPIENT ) AS SS_RECEPIENT_NAME,
			(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=b.SS_STATUS_BY ) AS SS_STATUSBY_NAME,
			(SELECT STUDY_NUMBER FROM ER_STUDY where pk_study=b.FK_STUDY) AS SS_STUDY_NUMBER,
			f_get_user_details(b.FK_USER_RECEPIENT) as RECEPIENT_NAME_DETAILS,
			f_get_user_details(b.SS_STATUS_BY) as STATUSBY_NAME_DETAILS
			FROM er_specimen a, er_specimen_status b
			WHERE (('':specTyp''=''NonStudy'' AND a.fk_study is null AND a.fk_account = :sessAccId) or ('':specTyp''=''Study'' AND a.fk_study in (:studyId)))
			AND (a.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = a.fk_per and per.fk_account =:sessAccId
			and usr.fk_user =:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
			AND b.fk_specimen = a.pk_specimen
			AND b.FK_CODELST_STATUS IN (SELECT pk_codeLst FROM er_codelst WHERE codelst_custom_col1=''process'')
			AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
			)
			where FK_SITE IN (:orgId)
			ORDER BY pk_specimen, spec_collection_date';
			
			UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=138;
			
			update_sql:='select * from (SELECT a.fk_account, a.fk_study, spec_id, spec_alternate_id,
				nvl(a.FK_SITE,-1) As fk_site,
				case when a.fk_study is null then ''[No Study Specified]''
				else (SELECT study_number FROM er_study WHERE pk_study = a.fk_study) end study_number,
				case when FK_USER_PATH is null then ''[Not specified]''
				else (select usr_firstName || '' '' || usr_lastname from er_user where pk_user = FK_USER_PATH) end pathologist,
				case when FK_USER_SURG is null then ''[Not specified]''
				else (select usr_firstName || '' '' || usr_lastname from er_user where pk_user = FK_USER_SURG) end Surgeon,
				(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) spec_type,
				TO_CHAR(spec_collection_date,PKG_DATEUTIL.F_GET_DATEFORMAT) spec_collection_date,
				(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_anatomic_site) spec_anatomic_site,
				spec_original_quantity,
				(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_quantity_units) spec_quantity_units,
				f_get_user_details(FK_USER_PATH) as pathologist_details,
				f_get_user_details(FK_USER_SURG) as Surgeon_details
				FROM er_specimen a, er_specimen_status b
				WHERE (('':specTyp''=''NonStudy'' AND a.fk_study is null AND a.fk_account = :sessAccId) or ('':specTyp''=''Study'' AND a.fk_study in (:studyId)))
				AND (a.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = a.fk_per and per.fk_account =:sessAccId
				and usr.fk_user =:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
				AND b.fk_specimen = a.pk_specimen
				AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
				AND b.PK_SPECIMEN_STATUS = (SELECT MAX(c.PK_SPECIMEN_STATUS) FROM er_specimen_status c
				WHERE c.fk_specimen = a.pk_specimen AND c.SS_DATE = (SELECT MAX(d.SS_DATE) FROM er_specimen_status d WHERE d.fk_specimen = a.pk_specimen ))
				)
				where FK_SITE IN (:orgId)
				ORDER BY spec_collection_date';
				
			UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=143;
			
			update_sql:='select * from (SELECT pk_specimen, spec_id, spec_alternate_id, a.fk_study,
			nvl(a.fk_site,-1) As fk_site,
			(SELECT p.SPEC_ID FROM ER_SPECIMEN p WHERE p.pk_specimen = a.fk_specimen) PSEC_ID,
			(SELECT count(*) FROM ER_SPECIMEN p WHERE p.fk_specimen = a.pk_specimen) ChildCnt,
			case when a.fk_study is null then ''[No Study Specified]''
			else (SELECT study_number FROM er_study WHERE pk_study = a.fk_study) end study_number,
			b.SS_DATE,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst=b.FK_CODELST_STATUS) as Spec_Staus,
			case when (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) is null then ''-''
			else (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) end spec_type,
			spec_collection_date,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_anatomic_site) spec_anatomic_site,
			spec_original_quantity,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_quantity_units) spec_quantity_units,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst=b.SS_PROC_TYPE) as SS_PROC_TYPE,
			SS_HAND_OFF_DATE, SS_PROC_DATE, SS_QUANTITY,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = SS_QUANTITY_UNITS) SS_QUANTITY_UNITS,
			(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=b.FK_USER_RECEPIENT ) AS SS_RECEPIENT_NAME,
			(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=b.SS_STATUS_BY ) AS SS_STATUSBY_NAME,
			(SELECT STUDY_NUMBER FROM ER_STUDY where pk_study=b.FK_STUDY) AS SS_STUDY_NUMBER,
			f_get_user_details(b.SS_STATUS_BY) AS USER_DETAILS
			FROM er_specimen a, er_specimen_status b
			WHERE (('':specTyp''=''NonStudy'' AND a.fk_study is null AND a.fk_account = :sessAccId) or ('':specTyp''=''Study'' AND a.fk_study in (:studyId)))
			AND (a.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = a.fk_per and per.fk_account =:sessAccId
			and usr.fk_user =:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
			AND b.fk_specimen = a.pk_specimen
			AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
			)
			where fk_site IN (:orgId)
			ORDER BY pk_specimen, spec_collection_date, SS_PROC_DATE';
			
			
			UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=145;

		COMMIT;
	END IF;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,461,4,'04_er_report_update_biospecimen.sql',sysdate,'v11.1.0 #862');

commit;