DECLARE 
	table_check number;
	update_sql clob;
BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then

			update_sql:='Select
DISTINCT
PK_STUDY,
STUDY_NUMBER,
USR_DETAILS,
TO_CHAR(STUDYACTUAL_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDYACTUAL_DATE, STUDY_TITLE, TA, PHASE, RESEARCH_TYPE, STUDY_TYPE, DECODE(PI,NULL,STUDY_OTHERPRINV,PI ||

DECODE(STUDY_OTHERPRINV,NULL,'''',''; '' || STUDY_OTHERPRINV)) AS PI,
(SELECT Pkg_Util.f_join(CURSOR( SELECT  (SELECT site_name FROM ER_SITE WHERE pk_site=a.fk_site) site_name  FROM ER_STUDYSITES a WHERE fk_study=pk_study),'','') FROM

dual) AS ORGANIZATION,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = study_division) AS division
  FROM erv_studyactive_protocols
  WHERE fk_account = :sessAccId
    AND STUDYACTUAL_DATE <= TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND (studyend_date IS NULL or studyend_date >=  TO_DATE

('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) )
    AND fk_codelst_tarea IN (:tAreaId)
    AND pk_study IN (:studyId)
    AND (study_division IN (:studyDivId) OR study_division IS NULL)
    AND (study_prinv IS NULL OR study_prinv IN (:userId))
    AND EXISTS  (SELECT fk_study FROM ER_STUDYSITES WHERE fk_study=pk_study AND fk_site IN (:orgId) )';

			UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=1;
			
		update_sql:='select
STUDY_NUMBER,
TO_CHAR(STUDY_START_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_START_DATE,
SSTAT_SITE_NAME,
(select codelst_desc from er_codelst WHERE pk_codelst = (select status_type from er_studystat where er_studystat.pk_studystat = VDA.VDA_V_STUDYSTAT.pk_studystat)) as
STATUSTYPE,
SSTAT_STUDY_STATUS,
TO_CHAR(SSTAT_VALID_FROM,PKG_DATEUTIL.F_GET_DATEFORMAT) SSTAT_VALID_FROM,
TO_CHAR(SSTAT_VALID_UNTIL,PKG_DATEUTIL.F_GET_DATEFORMAT) SSTAT_VALID_UNTIL,
SSTAT_NOTES,
SSTAT_DOCUMNTD_BY,
PK_STUDYSTAT,
SSTAT_FK_SITE,
FK_STUDY,
SSTAT_CURRENT_STAT,
USR_DETAILS
from VDA.VDA_V_STUDYSTAT
where SSTAT_CURRENT_STAT =''Yes''
AND  sstat_fk_site IN (:orgId)
 AND  TRUNC(SSTAT_VALID_FROM) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
 AND fk_study IN (:studyId)';
		
			UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=280;
			
			update_sql:='select
STUDY_NUMBER,
TO_CHAR(STUDY_START_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_START_DATE,
SSTAT_SITE_NAME,
(select codelst_desc from er_codelst WHERE pk_codelst = (select status_type from er_studystat where er_studystat.pk_studystat = VDA.VDA_V_STUDYSTAT.pk_studystat)) as

STATUSTYPE,
SSTAT_STUDY_STATUS,
TO_CHAR(SSTAT_VALID_FROM,PKG_DATEUTIL.F_GET_DATEFORMAT) SSTAT_VALID_FROM,
TO_CHAR(SSTAT_VALID_UNTIL,PKG_DATEUTIL.F_GET_DATEFORMAT) SSTAT_VALID_UNTIL,
SSTAT_NOTES,
SSTAT_DOCUMNTD_BY,
USR_DETAILS,
PK_STUDYSTAT,
SSTAT_FK_SITE,
FK_STUDY,
SSTAT_CURRENT_STAT
from VDA.VDA_V_STUDYSTAT
where pk_studystat = (select pk_studystat from er_studystat where fk_codelst_studystat = (select pk_codelst from er_codelst where codelst_type = ''studystat'' and

codelst_subtyp = ''active'') and er_studystat.fk_study = VDA.VDA_V_STUDYSTAT.fk_study and current_stat=''1'' )
AND  sstat_fk_site IN (:orgId)
AND  TRUNC(SSTAT_VALID_FROM) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
AND fk_study IN (:studyId)';

			UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=278;
			
			update_sql:='select
			STUDY_NUMBER,
			TO_CHAR(STUDY_START_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_START_DATE,
			SSTAT_SITE_NAME,
			(select codelst_desc from er_codelst WHERE pk_codelst = (select status_type from er_studystat where er_studystat.pk_studystat = VDA.VDA_V_STUDYSTAT.pk_studystat)) as

			STATUSTYPE,
			SSTAT_STUDY_STATUS,
			TO_CHAR(SSTAT_VALID_FROM,PKG_DATEUTIL.F_GET_DATEFORMAT) SSTAT_VALID_FROM,
			TO_CHAR(SSTAT_VALID_UNTIL,PKG_DATEUTIL.F_GET_DATEFORMAT) SSTAT_VALID_UNTIL,
			SSTAT_NOTES,
			SSTAT_DOCUMNTD_BY,
			PK_STUDYSTAT,
			SSTAT_FK_SITE,
			FK_STUDY,
			SSTAT_CURRENT_STAT,
			USR_DETAILS
			from VDA.VDA_V_STUDYSTAT
			WHERE  sstat_fk_site IN (:orgId)
			 AND  TRUNC(SSTAT_VALID_FROM) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
			 AND fk_study IN (:studyId)';			 
			
			UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=273;
			
			update_sql:='select
			PK_STUDY,
			STUDY_NUMBER,
			STUDY_TITLE,
			STUDY_PI,
			STUDY_COORDINATOR,
			STUDY_DIVISION,
			STUDY_TAREA,
			STUDY_PHASE,
			STUDY_RESEARCH_TYPE,
			STUDY_TYPE,
			TO_CHAR(STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_ACTUALDT,
			USER_DETAILS
			from VDA.vda_v_study_summary
			WHERE  pk_study IN (:studyId)';
			
			UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=274;
			
			update_sql:='SELECT study_number,
			pk_study,
			study_title,
			DECODE(study_prinv,NULL,DECODE(study_otherprinv,NULL,'''',study_otherprinv),(SELECT usr_lastname || '','' || usr_firstname FROM ER_USER WHERE pk_user = study_prinv)
			|| DECODE(study_otherprinv,NULL,'''',''; '' || study_otherprinv)) AS PI,
			(SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = study_coordinator) AS study_contact,
			(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_tarea) AS tarea,
			F_Getdis_Site(study_disease_site) AS disease_site,
			F_Getlocal_Samplesize(pk_study) AS study_samplsize,
			study_nsamplsize,
			(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_phase) AS phase,
			(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_restype) AS restype,
			(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_scope) AS SCOPE,
			(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_type) AS study_type,
			(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_blind) AS blind,
			(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_random) AS RANDOM,
			DECODE(fk_codelst_sponsor,NULL,study_sponsor,( SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_sponsor)) study_sponsor,
			F_Getmore_Studydetails(pk_study) AS study_details,
			(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = study_division) AS division,
			f_get_user_details(study_prinv) AS USER_DETAILS
			FROM ER_STUDY a
			WHERE fk_account = :sessAccId AND
			 ( EXISTS (SELECT fk_study FROM ER_STUDYTEAM WHERE fk_study = pk_study AND fk_user IN (:sessUserId))
				 or Pkg_Superuser.F_Is_Superuser(:sessUserId, pk_study) = 1
			 )
			 AND pk_study IN (:studyId) AND fk_codelst_tarea IN (:tAreaId) AND ((study_division IN (:studyDivId)) OR (study_division IS NULL))
			ORDER BY study_number ';

			UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=97;
			
			update_sql:='select
STUDY_NUMBER,
USER_SITE_NAME,
ROLE,
USER_NAME,
USER_PHONE,
USER_EMAIL,
USER_ADDRESS,
STUDYTEAM_STATUS,
USR_DETAILS,
FK_STUDY
from VDA.vda_v_studyteam_members
where fk_study IN (:studyId)';
		
			UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=277;
			
		COMMIT;
	END IF;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,461,5,'05_er_report_update_study.sql',sysdate,'v11.1.0 #862');

Commit;
