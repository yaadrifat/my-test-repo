set define off;
Declare
Begin
	execute immediate 'GRANT execute ON eres.f_get_user_details TO vda';
End;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,461,2,'02_grant_f_get_user_details.sql',sysdate,'v11.1.0 #862');

commit;