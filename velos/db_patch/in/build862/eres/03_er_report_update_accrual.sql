set define off;
DECLARE 
	table_check number;
	update_sql clob;
BEGIN
	SELECT COUNT(*) INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_REPORT';

	IF (table_check > 0) then		
			
			update_sql:='select FK_STUDY,
		(select STUDY_NUMBER from vda.vda_v_study_summary where pk_study = fk_study) as STUDY_NUMBER,
		(select STUDY_DIVISION from vda.vda_v_study_summary where pk_study = fk_study) as STUDY_DIVISION,
		(select STUDY_TAREA from vda.vda_v_study_summary where pk_study = fk_study) as STUDY_TAREA,
		(select STUDY_PHASE from vda.vda_v_study_summary where pk_study = fk_study) as STUDY_PHASE,
		(select STUDY_PI from vda.vda_v_study_summary where pk_study = fk_study) as STUDY_PI,
		FK_SITE_ENROLLING,
		PSTAT_ENROLL_SITE,
		PATPROT_ENROLL_YEAR,
		ACCRUAL_COUNT,
		(select USER_DETAILS from vda.vda_v_study_summary where pk_study = fk_study) as USER_DETAILS
		from VDA.VDA_V_PAT_ACCRUAL_YEAR
		WHERE fk_site_enrolling IN (:orgId) AND fk_study IN (:studyId)';
		
			UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=251;
			
			update_sql:='SELECT
		study_number,
		pk_study,
		study_title,
		(select study_PI from vda.vda_v_study_summary where vda.vda_v_study_summary.pk_study = b.pk_study) as STUDY_PI,
		(select study_division from vda.vda_v_study_summary where vda.vda_v_study_summary.pk_study = b.pk_study) as STUDY_DIVISION,
		(select study_tarea from vda.vda_v_study_summary where vda.vda_v_study_summary.pk_study = b.pk_study) as STUDY_TAREA,
		TO_CHAR((select study_actualdt from vda.vda_v_study_summary where vda.vda_v_study_summary.pk_study = b.pk_study) ,PKG_DATEUTIL.F_GET_DATEFORMAT) as STUDY_ACTUALDT,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study ) AS total_acr,
		(SELECT sysdate - to_date(study_actualdt) FROM DUAL) as DIFF,
		study_nsamplsize AS study_nsamplsize,
		(select USER_DETAILS from vda.vda_v_study_summary where vda.vda_v_study_summary.pk_study = b.pk_study) user_details
		FROM ER_STUDY b
		WHERE (SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study ) = 0 AND study_actualdt is not null AND pk_study IN (:studyId)';
			
			UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=252;
			
			update_sql:='select
		STUDY_NUMBER,
		(select study_division from VDA.VDA_V_STUDY_SUMMARY where VDA.VDA_V_STUDY_SUMMARY.pk_study = VDA.VDA_V_PAT_ACCRUAL.fk_study) as STUDY_DIVISION,
		(select study_phase from VDA.VDA_V_STUDY_SUMMARY where VDA.VDA_V_STUDY_SUMMARY.pk_study = VDA.VDA_V_PAT_ACCRUAL.fk_study) as STUDY_PHASE,
		(select study_pi from VDA.VDA_V_STUDY_SUMMARY where VDA.VDA_V_STUDY_SUMMARY.pk_study = VDA.VDA_V_PAT_ACCRUAL.fk_study) as STUDY_PI,
		TO_CHAR((select study_actualdt from VDA.VDA_V_STUDY_SUMMARY where VDA.VDA_V_STUDY_SUMMARY.pk_study = VDA.VDA_V_PAT_ACCRUAL.fk_study),PKG_DATEUTIL.F_GET_DATEFORMAT) as STUDY_ACTUALDT,
		TO_CHAR((select last_perm_closure from vda.vda_v_studystatus_metrics where vda.vda_v_studystatus_metrics.pk_study = VDA.VDA_V_PAT_ACCRUAL.fk_study),PKG_DATEUTIL.F_GET_DATEFORMAT) as STUDY_CLOSEDT,
		PSTAT_ENROLL_SITE,
		PSTAT_PAT_STUD_ID,
		PSTAT_ENROLLED_BY,
		TO_CHAR(PATPROT_ENROLDT,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_ENROLDT,
		(select patprot_enroll_year from VDA.VDA_V_PAT_ACCRUAL_ALL where VDA.VDA_V_PAT_ACCRUAL_ALL.fk_study = VDA.VDA_V_PAT_ACCRUAL.fk_study and VDA.VDA_V_PAT_ACCRUAL_ALL.fk_per = VDA.VDA_V_PAT_ACCRUAL.fk_per) as ENROLL_YEAR,
		PSTAT_ASSIGNED_TO,
		PSTAT_PHYSICIAN,
		FK_STUDY,
		FK_SITE_ENROLLING,
		(select USER_DETAILS from vda.vda_v_study_summary where vda.vda_v_study_summary.pk_study = VDA.VDA_V_PAT_ACCRUAL.fk_study) user_details
		from VDA.VDA_V_PAT_ACCRUAL
		WHERE fk_site_enrolling IN (:orgId)
		AND TRUNC(PATPROT_ENROLDT) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
		AND fk_study IN (:studyId)
		order by ENROLL_YEAR asc';
			
			UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=253;

		COMMIT;
	END IF;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,461,3,'03_er_report_update_accrual.sql',sysdate,'v11.1.0 #862');

commit;