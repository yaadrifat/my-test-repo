create or replace
FUNCTION f_get_event_sequence_new(p_visit_id NUMBER, p_event_name varchar2,  p_table varchar2,p_cat_name varchar2)
 RETURN Number AS
 v_retval number;
 v_count number;
 max_seq number;
 v_ret number;
 v_sql varchar2(1000);
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'f_get_event_sequence_new', pLEVEL  => Plog.LFATAL);

 BEGIN
   execute immediate 'select count(*) from ' ||p_table || ' where trim(lower(name))= '''||trim(lower(p_event_name))||''' and fk_visit= ' || p_visit_id
   || ' and EVENT_TYPE=''A'' AND trim(lower(EVENT_CATEGORY))='''||trim(lower(p_cat_name))||''''
   into v_count ;

   if (v_count>0) then
    v_ret := -3;
   RETURN v_ret;

   else
    --KM-09Oct09
    execute immediate 'select max(EVENT_SEQUENCE) from ' ||p_table|| ' where fk_visit = ' ||p_visit_id ||' and (DISPLACEMENT <> 0 or DISPLACEMENT IS NULL) '
    || ' and EVENT_TYPE=''A'''
    into max_seq ;

    v_ret := max_seq;

  end if;

    return v_ret;



end ;

/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,369,1,'01_f_geteventsequencenew.sql',sysdate,'v10 #770');

commit;

