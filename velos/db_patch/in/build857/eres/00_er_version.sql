set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v11.1.0 #857' 
where CTRL_KEY = 'app_version' ; 

commit;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,456,0,'00_er_version.sql',sysdate,'v11.1.0 #857');

commit;	
