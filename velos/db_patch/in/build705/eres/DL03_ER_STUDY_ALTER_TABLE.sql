SET define OFF;
DECLARE
  v_column_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*)
  INTO v_column_exists
  FROM user_tab_cols
  WHERE column_name   = 'CCSG_REPORTABLE_STUDY'
  AND table_name      = 'ER_STUDY';
  IF (V_COLUMN_EXISTS = 0) THEN
    EXECUTE immediate 'ALTER TABLE ERES.ER_STUDY ADD (CCSG_REPORTABLE_STUDY CHAR(1 BYTE));    
COMMENT ON COLUMN ER_STUDY.CCSG_REPORTABLE_STUDY  
IS    
''Identifies CCSG Reportable study flag 1-Yes, 0-No''';
  END IF; 

v_column_exists := 0;
SELECT COUNT(*)
  INTO v_column_exists
  FROM user_tab_cols
  WHERE column_name   = 'FK_CODELST_PURPOSE'
  AND table_name      = 'ER_STUDY';
  IF (V_COLUMN_EXISTS = 0) THEN
    EXECUTE immediate 'ALTER TABLE ERES.ER_STUDY ADD (FK_CODELST_PURPOSE NUMBER);    
COMMENT ON COLUMN ER_STUDY.FK_CODELST_PURPOSE  
IS    
''This column is for storing the Primary Purpose  of the Study. This is FK to table ER_CODELST.''';
  END IF;
END;
/