set define off;
UPDATE er_browserconf
SET BROWSERCONF_SETTINGS='{"key":"PI_NAME", "label":"Principal Investigator", "sortable":true, "resizeable":true,"hideable":true,"format":"piLink"}' 
WHERE FK_BROWSER=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='advSearch') AND BROWSERCONF_COLNAME='PI_NAME';
commit;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='advSearch') AND BROWSERCONF_COLNAME='PI_DETAILS';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME) values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='advSearch'),'PI_DETAILS');
end if;
commit;
end;
/
UPDATE er_browserconf
SET BROWSERCONF_SETTINGS='{"key":"ASSIGNEDTO_NAME", "label":"Assigned To", "sortable":true, "resizeable":true,"hideable":true, "format":"asgnLink"}' 
WHERE FK_BROWSER=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='studyPatient') AND BROWSERCONF_COLNAME='ASSIGNEDTO_NAME';
/
UPDATE er_browserconf
SET BROWSERCONF_SETTINGS='{"key":"PHYSICIAN_NAME", "label":"Physician", "sortable":true, "resizeable":true,"hideable":true, "format":"phyLink"}' 
WHERE FK_BROWSER=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='studyPatient') AND BROWSERCONF_COLNAME='PHYSICIAN_NAME';
/
UPDATE er_browserconf
SET BROWSERCONF_SETTINGS='{"key":"ENROLLEDBY_NAME", "label":"Enrolled By", "sortable":true, "resizeable":true,"hideable":true, "format":"enrolLink"}' 
WHERE FK_BROWSER=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='studyPatient') AND BROWSERCONF_COLNAME='ENROLLEDBY_NAME';
commit;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='studyPatient') AND BROWSERCONF_COLNAME='ENROL_BY';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME) values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='studyPatient'),'ENROL_BY');
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='studyPatient') AND BROWSERCONF_COLNAME='ASGN_TO';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME) values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='studyPatient'),'ASGN_TO');
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='studyPatient') AND BROWSERCONF_COLNAME='PHYSICIAN';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME) values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='studyPatient'),'PHYSICIAN');
end if;
commit;
end;
/
UPDATE er_browserconf
SET BROWSERCONF_SETTINGS='{"key":"STUDY_PRINV", "label":"PI", "sortable":true, "resizeable":true,"hideable":true,"format":"prinvLink"}' 
WHERE FK_BROWSER=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbMeeting') AND BROWSERCONF_COLNAME='STUDY_PRINV';
commit;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbMeeting') AND BROWSERCONF_COLNAME='STUDY_PRINV_DETAILS';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME) values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='irbMeeting'),'STUDY_PRINV_DETAILS');
end if;
commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,459,2,'02_er_browserconf_UserPatch.sql',sysdate,'v11.1.0 #860');

commit;	