set define off;
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browser where BROWSER_MODULE='orgSearch';

if v_row_count=0
then
insert into er_browser(PK_BROWSER,BROWSER_MODULE,BROWSER_NAME) values(SEQ_ER_BROWSER.nextval,'orgSearch','Organizations');
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='orgSearch') AND BROWSERCONF_COLNAME='ORG_NAME';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL) 
values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='orgSearch'),'ORG_NAME',1,'{"key":"ORG_NAME", "label":"Organization Name", "format":"orgLink",  "resizeable":true,"sortable":true,"hideable":true}','Organization Name');
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='orgSearch') AND BROWSERCONF_COLNAME='ORG_TYPE';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL) 
values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='orgSearch'),'ORG_TYPE',2,'{"key":"ORG_TYPE", "label":"Type",  "resizeable":true,"sortable":true,"hideable":true}','Type');
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='orgSearch') AND BROWSERCONF_COLNAME='PARENT_ORG';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL) 
values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='orgSearch'),'PARENT_ORG',3,'{"key":"PARENT_ORG", "label":"Parent Organization",  "resizeable":true,"sortable":true,"hideable":true}','Parent Organization');
end if;
commit;
end;
/
declare
v_row_count number;
begin

select count(*) into v_row_count from er_browserconf where fk_browser=(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='orgSearch') AND BROWSERCONF_COLNAME='PK_SITE';

if v_row_count=0
then
insert into er_browserconf(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ) 
values(SEQ_ER_BROWSERCONF.nextval,(SELECT PK_BROWSER FROM ER_BROWSER WHERE BROWSER_MODULE='orgSearch'),'PK_SITE',4);
end if;
commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,459,1,'01_er_browserconf_SitePatch.sql',sysdate,'v11.1.0 #860');

commit;	