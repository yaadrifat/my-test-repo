set define off;
DECLARE
  v_column_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*)
  INTO v_column_exists
  FROM user_tab_cols
  WHERE column_name   = 'anatomic_site'
  AND table_name      = 'ER_PATPROT';
  IF (v_column_exists = 0) THEN
  EXECUTE immediate 'ALTER TABLE ERES.ER_PATPROT ADD (anatomic_site varchar2(500))';
  EXECUTE immediate 'COMMENT ON COLUMN ER_PATPROT.Anatomic_SITE  
  IS    
  ''This column is used to store the display value for the column Anatomic Site.''';
  END IF;
END;
/