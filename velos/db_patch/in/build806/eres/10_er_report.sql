set define off;
DECLARE 
	table_check number;
	row_check number;
	update_sql varchar(5000);

BEGIN
	SELECT COUNT(*) INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_REPORT';

	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE rep_name = 'Research Ticket Items (Per Patient)' and pk_report=269;

		IF (row_check > 0) then
		
			update_sql:='select distinct
es.study_number as study_number,
es.study_title as Study_title,se.fk_patprot                 AS pk_patprot,
pkg_phi.f_get_patientright(:sessUserId, (select fk_grp_default from er_user where pk_user=:sessUserId), (select fk_per from er_patprot where pk_patprot=se.fk_patprot ) ) right_mask,
visit_name,
se.event_id,
se.reason_for_coveragechange,
(select sum(eventcost_value) from sch_eventcost where fk_cost_desc = (select pk_codelst from sch_codelst where codelst_type = ''cost_desc'' and codelst_subtyp = ''res_cost'') and fk_event = se.fk_assoc) as cost,
(select usr_firstname || '' '' || usr_lastname from er_user where pk_user = es.study_coordinator) as UserName,
(select add_phone from er_add where pk_add = (select fk_peradd from er_user where pk_user = es.study_coordinator)) as Contact,
(select name from event_assoc where event_id = se.fk_assoc) as Event_Name,
(select codelst_desc from sch_codelst where pk_codelst = se.fk_codelst_covertype) as CoverageType,
(select event_cptcode from event_assoc where event_id = se.fk_assoc) as cptcode,
(select eventstat_dt from sch_eventstat where pk_eventstat = (select max(pk_eventstat) from sch_eventstat where fk_event = se.event_id and eventstat = (select pk_codelst from sch_codelst where codelst_type=''eventstatus'' and codelst_subtyp = ''ev_done''))) as ServiceDate,
(select codelst_desc from er_codelst where pk_codelst = es.fk_codelst_tarea) as Department,
(SELECT u.USR_FIRSTNAME || '' ''|| u.USR_LASTNAME FROM er_study,er_user u WHERE pk_study = ea.chain_id AND er_study.STUDY_PRINV=u.pk_user
  ) AS principal_investigator,
study_contact,
(select person_fname  || '' ''|| person_lname from er_patprot ep, esch.person p where fk_study =  :studyId  and p.pk_person = ep.fk_per and p.pk_person = :patientId and ep.patprot_stat = 1) as PatName,
(select pat_facilityid from er_patprot ep, esch.person p where fk_study = :studyId and p.pk_person = ep.fk_per and p.pk_person =  :patientId  and ep.patprot_stat = 1) as PatFacilityId
from event_assoc ea, er_study es, sch_protocol_visit spv, sch_events1 se, sch_eventstat sev
WHERE ea.event_id = spv.fk_protocol and
spv.pk_protocol_visit = se.fk_visit and
sev.fk_event = se.event_id and
(select EVENTSTAT_DT from sch_eventstat where PK_EVENTSTAT = (select max(PK_EVENTSTAT) from sch_eventstat where fk_event = se.event_id)) >= TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and
(select EVENTSTAT_DT from sch_eventstat where PK_EVENTSTAT = (select max(PK_EVENTSTAT) from sch_eventstat where fk_event = se.event_id)) <= TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and
eventstat = (select pk_codelst from sch_codelst where codelst_type=''eventstatus'' and codelst_subtyp = ''ev_done'') and
se.fk_codelst_covertype = (select pk_codelst from sch_codelst where codelst_type=''coverage_type'' and codelst_subtyp = ''S'') and
pk_study IN (:studyId)  and
se.fk_patprot IN (select pk_patprot from er_patprot ep, esch.person p where fk_study = :studyId  and p.pk_person = ep.fk_per and p.pk_person = :patientId  and ep.patprot_stat = 1)';
		
			UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE REP_NAME='Research Ticket Items (Per Patient)' AND pk_report=269;		
		END IF; 
	END IF;
END;
/
DECLARE 
	table_check number;
	row_check number;
	update_sql varchar(5000);

BEGIN
	SELECT COUNT(*) INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_REPORT';

	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE rep_name = 'Routine Care Events (Per Patient)' and pk_report=270;

		IF (row_check > 0) then
		
			update_sql:='select distinct
es.study_number as study_number,
es.study_title as Study_title,
se.fk_patprot                 AS pk_patprot,
visit_name,
se.event_id,
se.reason_for_coveragechange,
(select usr_firstname || '' '' || usr_lastname from er_user where pk_user = es.study_coordinator) as UserName,
(select add_phone from er_add where pk_add = (select fk_peradd from er_user where pk_user = es.study_coordinator)) as Contact,
(select name from event_assoc where event_id = se.fk_assoc) as Event_Name,
(select codelst_desc from sch_codelst where pk_codelst = se.fk_codelst_covertype) as CoverageType,
(select event_cptcode from event_assoc where event_id = se.fk_assoc) as cptcode,
(select eventstat_dt from sch_eventstat where pk_eventstat = (select max(pk_eventstat) from sch_eventstat where fk_event = se.event_id and eventstat = (select pk_codelst from sch_codelst where codelst_type=''eventstatus'' and codelst_subtyp = ''ev_done''))) as ServiceDate,
(select codelst_desc from er_codelst where pk_codelst = es.fk_codelst_tarea) as Department,
(SELECT u.USR_FIRSTNAME || '' ''|| u.USR_LASTNAME FROM er_study,er_user u WHERE pk_study = ea.chain_id AND er_study.STUDY_PRINV=u.pk_user
  ) AS principal_investigator,
study_contact,
(select person_fname  || '' ''|| person_lname from er_patprot ep, esch.person p where fk_study =  :studyId  and p.pk_person = ep.fk_per and p.pk_person = :patientId and ep.patprot_stat = 1) as PatName,
(select pat_facilityid from er_patprot ep, esch.person p where fk_study = :studyId and p.pk_person = ep.fk_per and p.pk_person =  :patientId  and ep.patprot_stat = 1) as PatFacilityId,
pkg_phi.f_get_patientright(:sessUserId, (select fk_grp_default from er_user where pk_user=:sessUserId),  (SELECT fk_per
    FROM er_patprot
    WHERE pk_patprot =se.fk_patprot
  
    ) ) right_mask
from event_assoc ea, er_study es, sch_protocol_visit spv, sch_events1 se, sch_eventstat sev
WHERE ea.event_id = spv.fk_protocol and
spv.pk_protocol_visit = se.fk_visit and
sev.fk_event = se.event_id and
(select EVENTSTAT_DT from sch_eventstat where PK_EVENTSTAT = (select max(PK_EVENTSTAT) from sch_eventstat where fk_event = se.event_id)) >= TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and
(select EVENTSTAT_DT from sch_eventstat where PK_EVENTSTAT = (select max(PK_EVENTSTAT) from sch_eventstat where fk_event = se.event_id)) <= TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and
eventstat = (select pk_codelst from sch_codelst where codelst_type=''eventstatus'' and codelst_subtyp = ''ev_done'') and
se.fk_codelst_covertype = (select pk_codelst from sch_codelst where codelst_type=''coverage_type'' and codelst_subtyp = ''M'') and
pk_study IN (:studyId)  and
se.fk_patprot IN (select pk_patprot from er_patprot ep, esch.person p where fk_study = :studyId  and p.pk_person = ep.fk_per and p.pk_person = :patientId  and ep.patprot_stat = 1)';
		
			UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE REP_NAME='Routine Care Events (Per Patient)' AND pk_report=270;		
		END IF; 
	END IF;
END;
/
DECLARE 
	table_check number;
	row_check number;
	update_sql varchar(5000);

BEGIN
	SELECT COUNT(*) INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_REPORT';

	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE rep_name = 'Research Ticket Items (Per Study)' and pk_report=271;

		IF (row_check > 0) then
		
			update_sql:='select distinct
es.study_number as study_number,
es.study_title as Study_title,
se.fk_patprot                 AS pk_patprot,
visit_name,
se.event_id,
se.reason_for_coveragechange,
(select sum(eventcost_value) from sch_eventcost where fk_cost_desc = (select pk_codelst from sch_codelst where codelst_type = ''cost_desc'' and codelst_subtyp = ''res_cost'') and fk_event = se.fk_assoc) as cost,
(select usr_firstname || '' '' || usr_lastname from er_user where pk_user = es.study_coordinator) as UserName,
(select add_phone from er_add where pk_add = (select fk_peradd from er_user where pk_user = es.study_coordinator)) as Contact,
(select name from event_assoc where event_id = se.fk_assoc) as Event_Name,
(select codelst_desc from sch_codelst where pk_codelst = se.fk_codelst_covertype) as CoverageType,
(select event_cptcode from event_assoc where event_id = se.fk_assoc) as cptcode,
(select eventstat_dt from sch_eventstat where pk_eventstat = (select max(pk_eventstat) from sch_eventstat where fk_event = se.event_id and eventstat = (select pk_codelst from sch_codelst where codelst_type=''eventstatus'' and codelst_subtyp = ''ev_done''))) as ServiceDate,
(select codelst_desc from er_codelst where pk_codelst = es.fk_codelst_tarea) as Department,
(SELECT u.USR_FIRSTNAME || '' ''|| u.USR_LASTNAME FROM er_study,er_user u WHERE pk_study = ea.chain_id AND er_study.STUDY_PRINV=u.pk_user
  ) AS principal_investigator,
study_contact,
(select person_fname  || '' ''|| person_lname from esch.person p where p.pk_person IN (select fk_per from er_patprot where fk_study IN (:studyId) and pk_patprot = se.fk_patprot)) as PatName,
(select pat_facilityid from esch.person p where p.pk_person IN (select fk_per from er_patprot where fk_study IN (:studyId) and pk_patprot = se.fk_patprot)) as PatFacilityId,
pkg_phi.f_get_patientright(:sessUserId, (select fk_grp_default from er_user where pk_user=:sessUserId),  (SELECT fk_per
    FROM er_patprot
    WHERE pk_patprot =se.fk_patprot
  
    ) ) right_mask
from event_assoc ea, er_study es, sch_protocol_visit spv, sch_events1 se, sch_eventstat sev
WHERE ea.event_id = spv.fk_protocol and
spv.pk_protocol_visit = se.fk_visit and
sev.fk_event = se.event_id and
(select EVENTSTAT_DT from sch_eventstat where PK_EVENTSTAT = (select max(PK_EVENTSTAT) from sch_eventstat where fk_event = se.event_id)) >= TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and
(select EVENTSTAT_DT from sch_eventstat where PK_EVENTSTAT = (select max(PK_EVENTSTAT) from sch_eventstat where fk_event = se.event_id)) <= TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and
eventstat = (select pk_codelst from sch_codelst where codelst_type=''eventstatus'' and codelst_subtyp = ''ev_done'') and
se.fk_codelst_covertype = (select pk_codelst from sch_codelst where codelst_type=''coverage_type'' and codelst_subtyp = ''S'') and
pk_study IN (:studyId)  and
se.fk_patprot IN (select pk_patprot from er_patprot ep, esch.person p where fk_study IN (:studyId)   and p.pk_person = ep.fk_per and ep.patprot_stat = 1)
order by PatFacilityId, ServiceDate';
		
			UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE REP_NAME='Research Ticket Items (Per Study)' AND pk_report=271;		
		END IF; 
	END IF;
END;
/
DECLARE 
	table_check number;
	row_check number;
	update_sql varchar(5000);

BEGIN
	SELECT COUNT(*) INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_REPORT';

	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE rep_name = 'Routine Care Events (Per Study)' and pk_report=272;

		IF (row_check > 0) then
		
			update_sql:='select distinct
es.study_number as study_number,
es.study_title as Study_title,
se.fk_patprot AS pk_patprot,
visit_name,
se.event_id,
se.reason_for_coveragechange,
(select usr_firstname || '' '' || usr_lastname from er_user where pk_user = es.study_coordinator) as UserName,
(select add_phone from er_add where pk_add = (select fk_peradd from er_user where pk_user = es.study_coordinator)) as Contact,
(select name from event_assoc where event_id = se.fk_assoc) as Event_Name,
(select codelst_desc from sch_codelst where pk_codelst = se.fk_codelst_covertype) as CoverageType,
(select event_cptcode from event_assoc where event_id = se.fk_assoc) as cptcode,
(select eventstat_dt from sch_eventstat where pk_eventstat = (select max(pk_eventstat) from sch_eventstat where fk_event = se.event_id and eventstat = (select pk_codelst from sch_codelst where codelst_type=''eventstatus'' and codelst_subtyp = ''ev_done''))) as ServiceDate,
(select codelst_desc from er_codelst where pk_codelst = es.fk_codelst_tarea) as Department,
(SELECT u.USR_FIRSTNAME || '' ''|| u.USR_LASTNAME FROM er_study,er_user u WHERE pk_study = ea.chain_id AND er_study.STUDY_PRINV=u.pk_user
  ) AS principal_investigator,
study_contact,
(select person_fname  || '' ''|| person_lname from esch.person p where p.pk_person IN (select fk_per from er_patprot where fk_study IN (:studyId) and pk_patprot = se.fk_patprot)) as PatName,
(select pat_facilityid from esch.person p where p.pk_person IN (select fk_per from er_patprot where fk_study IN (:studyId) and pk_patprot = se.fk_patprot)) as PatFacilityId,
 pkg_phi.f_get_patientright(:sessUserId, (select fk_grp_default from er_user where pk_user=:sessUserId),  (SELECT fk_per
    FROM er_patprot
    WHERE pk_patprot =se.fk_patprot
  
    ) ) right_mask
from event_assoc ea, er_study es, sch_protocol_visit spv, sch_events1 se, sch_eventstat sev
WHERE ea.event_id = spv.fk_protocol and
spv.pk_protocol_visit = se.fk_visit and
sev.fk_event = se.event_id and
(select EVENTSTAT_DT from sch_eventstat where PK_EVENTSTAT = (select max(PK_EVENTSTAT) from sch_eventstat where fk_event = se.event_id)) >= TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and
(select EVENTSTAT_DT from sch_eventstat where PK_EVENTSTAT = (select max(PK_EVENTSTAT) from sch_eventstat where fk_event = se.event_id)) <= TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and
eventstat = (select pk_codelst from sch_codelst where codelst_type=''eventstatus'' and codelst_subtyp = ''ev_done'') and
se.fk_codelst_covertype = (select pk_codelst from sch_codelst where codelst_type=''coverage_type'' and codelst_subtyp = ''M'') and
pk_study IN (:studyId)  and
se.fk_patprot IN (select pk_patprot from er_patprot ep, esch.person p where fk_study IN (:studyId)   and p.pk_person = ep.fk_per and ep.patprot_stat = 1)
order by PatFacilityId, ServiceDate';
		
			UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE REP_NAME='Routine Care Events (Per Study)' AND pk_report=272;		
		END IF; 
	END IF;
END;
/
delete from er_repxsl where pk_repxsl in (269,270,271,272);
COMMIT;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,405,10,'10_er_report.sql',sysdate,'v11 #806');
commit; 