set define off;
 update er_report
set REP_SQL_CLOB = 'select inv_number, int_acc_num,
(select study_number from er_study where pk_study = a.fk_study) as studynum,
(select nct_number from er_study where pk_study = a.fk_study) as nctnum,
(select study_title from er_study where pk_study = a.fk_study) as studytitle,
to_char(inv_date,PKG_DATEUTIL.F_GET_DATEFORMAT) inv_date,
DECODE(trim(inv_payunit), ''D'' ,  to_char(to_date(to_char(inv_date, PKG_DATEUTIL.F_GET_DATEFORMAT), PKG_DATEUTIL.F_GET_DATEFORMAT) + inv_payment_dueby , PKG_DATEUTIL.F_GET_DATEFORMAT ),
                          ''W'' ,  to_char(to_date(to_char(inv_date, PKG_DATEUTIL.F_GET_DATEFORMAT), PKG_DATEUTIL.F_GET_DATEFORMAT) + inv_payment_dueby * 7 , PKG_DATEUTIL.F_GET_DATEFORMAT ),
                          ''M'' ,  to_char(add_months(to_date(inv_date , PKG_DATEUTIL.F_GET_DATEFORMAT) , inv_payment_dueby),PKG_DATEUTIL.F_GET_DATEFORMAT),
                                 to_char(add_months(to_date(inv_date , PKG_DATEUTIL.F_GET_DATEFORMAT) , inv_payment_dueby * 12),PKG_DATEUTIL.F_GET_DATEFORMAT)
) as duedays,
(SELECT SITE_NAME FROM ER_SITE, ER_USER WHERE PK_USER = INV_ADDRESSEDTO and PK_SITE = FK_SITEID) AS orgto,
(SELECT USR_FIRSTNAME || '' '' || USR_LASTNAME FROM ER_USER WHERE PK_USER = INV_ADDRESSEDTO) AS addto,
(SELECT address FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = INV_ADDRESSEDTO) AS addto_add,
(SELECT add_city || '' '' || add_state FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = INV_ADDRESSEDTO) AS addto_add1,
(SELECT add_country || '' '' || add_zipcode FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = INV_ADDRESSEDTO) AS addto_add2,
(SELECT SITE_NAME FROM ER_SITE, ER_USER WHERE PK_USER = inv_sentfrom and PK_SITE = FK_SITEID) AS orgfrom,
(SELECT USR_FIRSTNAME || '' '' || USR_LASTNAME FROM ER_USER WHERE PK_USER = inv_sentfrom) AS sentfrom,
(SELECT address FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = inv_sentfrom) AS sentfrom_add,
(SELECT add_city || '' '' || add_state FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = inv_sentfrom) AS sentfrom_add1,
(SELECT add_country || '' '' || add_zipcode FROM ER_ADD, ER_USER WHERE pk_add = fk_peradd AND PK_USER = inv_sentfrom) AS sentfrom_add2,
NVL((SELECT patprot_patstdid FROM ER_PATPROT WHERE ER_PATPROT.fk_study = a.fk_study AND ER_PATPROT.fk_per = b.fk_per AND patprot_stat = 1,''(Patient removed from study)'') fk_per,
milestones_achieved,
to_char(achieved_on,PKG_DATEUTIL.F_GET_DATEFORMAT) achieved_on,
amount_due,
round(amount_invoiced,2) amount_invoiced, round(amount_holdback,2) amount_holdback, detail_type, display_detail,
inv_notes,
milestone_type AS TYPE,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = milestone_status) AS status,
(SELECT name FROM esch.event_assoc WHERE event_id = fk_cal) AS cal,
(SELECT name FROM esch.event_assoc WHERE event_id = fk_eventassoc) AS event,
(SELECT visit_name FROM esch.sch_protocol_visit WHERE pk_protocol_visit = fk_visit) AS visit,
NVL(trim((SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = milestone_status) || '' '' || (SELECT visit_name FROM esch.sch_protocol_visit WHERE pk_protocol_visit = fk_visit)),''All'')  || ''; ''
  ||
  (SELECT name FROM esch.EVENT_ASSOC WHERE EVENT_ID = FK_EVENTASSOC
  ) AS milestone_type, fk_per as pk_per
FROM ER_INVOICE a, ER_INVOICE_DETAIL b, er_milestone c
WHERE fk_inv = pk_invoice
and fk_milestone = pk_milestone
and pk_invoice = ~1 and nvl(b.fk_per,0) <> 0 and milestone_type <> ''SM'''
where pk_report=131;
commit;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,405,1,'01_Er_Report_Update_131.sql',sysdate,'v11 #806');
commit; 