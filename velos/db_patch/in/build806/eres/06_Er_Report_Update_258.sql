set define off;
 update er_report
set REP_SQL_CLOB ='select
study_number,
milestone_type,
msach_milestone_desc,
fk_site_enrolling,
enrolling_site,
NVL(msach_ptstudy_id,''(Patient removed from study)'') msach_ptstudy_id,
TO_CHAR(msach_ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) msach_ach_date,
(milestone_amt/DECODE((select MSRUL_PT_COUNT from VDA.VDA_V_MILESTONES a where
VDA.vda_v_milestone_achvd_det.fk_study = a.fk_study AND a.pk_milestone = VDA.vda_v_milestone_achvd_det.fk_milestone),-1,1,null,1,
(select MSRUL_PT_COUNT from VDA.VDA_V_MILESTONES a where
VDA.vda_v_milestone_achvd_det.fk_study = a.fk_study AND a.pk_milestone = VDA.vda_v_milestone_achvd_det.fk_milestone))) milestone_amt,
milestone_holdback,
payment_type,
payment_for,
fk_study,
fk_milestone,
inv_amt,
inv_num,
rec_amt,
payment_dt
from VDA.vda_v_milestone_achvd_det
where PAYMENT_SUBTYPE = ''pay''
AND fk_study IN (:studyId)
AND TRUNC(MSACH_ACH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)'
where pk_report=258;
commit;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,405,6,'06_Er_Report_Update_258.sql',sysdate,'v11 #806');
commit; 
