create or replace
FUNCTION        "F_GETMORE_STUDYDETAILS" (p_study NUMBER)
RETURN clob 
AS
v_retval clob;

BEGIN

-- SELECT codelst_desc, studyid_id FROM (SELECT codelst_desc, studyid_id,codelst_custom_col FROM ER_STUDYID, ER_CODELST WHERE  fk_codelst_idtype = pk_codelst AND fk_study = p_study AND studyid_id IS NOT NULL) WHERE codelst_custom_col IS NULL OR (codelst_custom_col='chkbox' AND studyid_id='Y')

	 FOR i IN (SELECT codelst_desc, studyid_id FROM (SELECT codelst_desc, studyid_id,codelst_custom_col FROM ER_STUDYID, ER_CODELST WHERE  fk_codelst_idtype = pk_codelst AND fk_study = p_study AND studyid_id IS NOT NULL) )
	 LOOP
	 	 v_retval := v_retval || '[' || REPLACE(i.codelst_desc,'&nbsp;','') || ' : ' || i.studyid_id || '];';
	 END LOOP;
	 v_retval := substr(v_retval,1,instr(v_retval,';',-1)-1) ;

	 RETURN v_retval;
END ;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,383,8,'08_F_GETMORE_STUDYDETAILS.sql',sysdate,'v10.1 #784');

commit;
