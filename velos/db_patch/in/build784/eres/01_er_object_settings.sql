DECLARE 
	table_check number;
	row_check number;
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_OBJECT_SETTINGS';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_object_settings
		WHERE object_name = 'acct_tab' AND
			  object_sequence = 7;
		
		IF (row_check > 0) then	
		  
			UPDATE 
				ER_OBJECT_SETTINGS
			SET 
				OBJECT_DISPLAYTEXT='Networks'		
			WHERE object_name = 'acct_tab' AND
			  object_sequence = 7;
		END IF;
		
	END IF;		
	
	COMMIT;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,383,1,'01_er_object_settings.sql',sysdate,'v10.1 #784');

commit;
