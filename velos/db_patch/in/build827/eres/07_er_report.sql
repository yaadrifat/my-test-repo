SET DEFINE OFF;
DECLARE 
	table_check number;
	row_check number;

BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE rep_name = 'Study Submission History(All)';

		IF (row_check = 0) then
			Insert into ER_REPORT (PK_REPORT,REP_NAME,REP_DESC,FK_ACCOUNT,REP_HIDE,REP_COLUMNS,REP_FILTERBY,GENERATE_XML,REP_TYPE,REP_FILTERAPPLICABLE)
			values (30000,'Study Submission History(All)','Study Submission History(All)',0,'N','Review Board Name, Study Number, Study Title,Submission Type, Submission Status, Submission Date ','Study',1,'rep_irb','date:studyId');
		END IF;
	END IF;
  COMMIT;
  END;
 /
DECLARE 
	table_check number;
	row_check number;

BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE rep_name = 'Study Submission History(Current)';

		IF (row_check = 0) then
			Insert into ER_REPORT (PK_REPORT,REP_NAME,REP_DESC,FK_ACCOUNT,REP_HIDE,REP_COLUMNS,REP_FILTERBY,GENERATE_XML,REP_TYPE,REP_FILTERAPPLICABLE) 
			values (30001,'Study Submission History(Current)','Study Submission History(Current)',0,'N','Review Board Name, Study Number, Study Title,Submission Type, Submission Status, Submission Date ','Study',1,'rep_irb','date:studyId');
		END IF;
	END IF;
  COMMIT;
  END;
  /
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,426,7,'07_er_report.sql',sysdate,'v11 #827');
commit;