set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v11 #801' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,400,0,'00_er_version.sql',sysdate,'v11 #801');
commit; 

