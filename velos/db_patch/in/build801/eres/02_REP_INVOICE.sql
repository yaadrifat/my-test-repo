CREATE OR REPLACE  VIEW "ERES"."REP_INVOICE" ("MSINV_STUDY_NUM", "MSINV_STUDY_TITLE", "MSINV_NUMBER", "MSINV_DATE", "MSINV_RANGE_TYPE", "MSINV_DATE_RANGE", "MSINV_NOTES", "MSINV_PAY_DUE", "MSINV_ADDRESSED_TO", "MSINV_SENT_FROM", "MSINV_HEADER", "MSINV_FOOTER", "MSINV_UNINV_OR_ALL", "FK_ACCOUNT", "MSINV_RESPONSE_ID", "RID", "CREATOR", "CREATED_ON", "LAST_MODIFIED_DATE", "LAST_MODIFIED_BY", "FK_STUDY", "INV_TOTAL") AS 
  SELECT   (SELECT   STUDY_NUMBER
               FROM   ER_STUDY
              WHERE   PK_STUDY = FK_STUDY)
               MSINV_STUDY_NUM,
            (SELECT   STUDY_TITLE
               FROM   ER_STUDY
              WHERE   PK_STUDY = FK_STUDY)
               MSINV_STUDY_TITLE,
            INV_NUMBER MSINV_NUMBER,
            INV_DATE MSINV_DATE,
            F_GET_INTERVAL (INV_INTERVALTYPE) MSINV_RANGE_TYPE,
               TO_CHAR (INV_INTERVALFROM, PKG_DATEUTIL.f_get_dateformat)
            || ' - '
            || TO_CHAR (INV_INTERVALTO, PKG_DATEUTIL.f_get_dateformat)
               AS MSINV_DATE_RANGE,
            INV_NOTES MSINV_NOTES,
            (TRUNC (INV_DATE) + F_CALC_DAYS (INV_PAYUNIT, INV_PAYMENT_DUEBY))
               AS MSINV_PAY_DUE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = INV_ADDRESSEDTO)
               MSINV_ADDRESSED_TO,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = INV_SENTFROM)
               MSINV_SENT_FROM,
            INV_HEADER MSINV_HEADER,
            INV_FOOTER MSINV_FOOTER,
            F_GET_INV_MSTATTYPE (INV_MILESTATUSTYPE) MSINV_UNINV_OR_ALL,
            (SELECT   FK_ACCOUNT
               FROM   ER_STUDY
              WHERE   PK_STUDY = FK_STUDY)
               FK_ACCOUNT,
            PK_INVOICE MSINV_RESPONSE_ID,
            RID,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = ER_INVOICE.CREATOR)
               CREATOR,
            CREATED_ON,
            LAST_MODIFIED_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = ER_INVOICE.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            FK_STUDY,
            (NVL ( (SELECT   SUM (amount_invoiced)
                      FROM   ER_INVOICE_DETAIL
                     WHERE   fk_inv = pk_invoice AND fk_per = 0 and DETAIL_TYPE='H'), 0))
               INV_TOTAL
     FROM   ER_INVOICE
 ;
 /
 

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,400,2,'02_REP_INVOICE.sql',sysdate,'v11 #801');
commit; 
