set define off;
DECLARE 
v_column_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*)
  INTO v_column_exists
  FROM ER_REPFILTERMAP,ER_REPFILTER
  WHERE REPFILTERMAP_REPCAT='patient'
  and PK_REPFILTER=FK_REPFILTER
  and REPFILTER_KEYWORD='patStatusId';
  
  IF (v_column_exists > 0) THEN
  update ER_REPFILTER set REPFILTER_COLUMN='<td><DIV id="patstatusDIV"><FONT class = "comments"><A href="javascript:void(0);" onClick="return openLookup(document.reports,''viewId=6011'||chr(38)||'form=reports'||chr(38)||'seperator=,'||chr(38)||'defaultvalue=[ALL]'||chr(38)||'keyword=selpatStatusId|CODEDESC~parampatStatusId|CODEPK|[VELHIDE]'',''dfilter=codelst_type=\''patStatus\'' and codelst_hide=\''N\'' '')">Select Patient Study Status</A> </FONT></DIV></td><td><DIV id="patstatusdataDIV"><Input TYPE="text" NAME="selpatStatusId" SIZE="20" READONLY VALUE="[ALL]"><Input TYPE="hidden" NAME="parampatStatusId" VALUE="[ALL]"></DIV></td>' 
  where  REPFILTER_KEYWORD='patStatusId';
  END IF;
  
  COMMIT;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,422,2,'02_update_repfilter.sql',sysdate,'v11 #823');
commit;
/
