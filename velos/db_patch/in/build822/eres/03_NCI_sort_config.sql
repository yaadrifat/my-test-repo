set define off;
DECLARE
countFlag number;
v_cnt number;
begin
 SELECT COUNT(1) INTO countFlag FROM USER_TABLES WHERE TABLE_NAME='ER_CODELST';
    if (countFlag > 0) then
      SELECT COUNT(*)  INTO v_cnt from er_codelst  WHERE codelst_type ='NCI' and CODELST_SUBTYP=2.0;
      if v_cnt=0 then
      insert into er_codelst (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_CUSTOM_COL) 
	  values(seq_er_codelst.nextval,'NCI','2.0','CTC NCI v2.0','1,2,4');
end if;
SELECT COUNT(*)  INTO v_cnt from er_codelst  WHERE codelst_type ='NCI' and CODELST_SUBTYP=3.0;
      if v_cnt=0 then
      insert into er_codelst (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_CUSTOM_COL) 
	  values(seq_er_codelst.nextval,'NCI','3.0','CTC NCI v3.0','1,2,4');
end if;
SELECT COUNT(*)  INTO v_cnt from er_codelst  WHERE codelst_type ='NCI' and CODELST_SUBTYP=4.0;
      if v_cnt=0 then
      insert into er_codelst (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_CUSTOM_COL) 
	  values(seq_er_codelst.nextval,'NCI','4.0','CTC NCI v4.01','1,2,3,5');
end if;
SELECT COUNT(*)  INTO v_cnt from er_codelst  WHERE codelst_type ='NCI' and CODELST_SUBTYP=5.0;
      if v_cnt=0 then
      insert into er_codelst (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_CUSTOM_COL) 
	  values(seq_er_codelst.nextval,'NCI','5.0','CTCAE v5.0','1,2,3,4');
end if;

end if;
commit;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,421,3,'03_NCI_sort_config',sysdate,'v11 #822');
commit;
/