SET DEFINE OFF;
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ERES"."ERV_STUDYTEAM_MORE_DETAILS" ("PK_MOREDETAILS", "PK_STUDYTEAM", "STUDYTEAM_STATUS", "FIELD_NAME", "FIELD_VALUE", "CREATED_ON", "CREATOR", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "RID", "RESPONSE_ID") AS 
  SELECT   pk_moredetails,PK_STUDYTEAM,STUDYTEAM_STATUS,
            (SELECT   codelst_desc
               FROM   er_codelst
              WHERE   pk_codelst = MD_MODELEMENTPK)
               field_name,
            MD_MODELEMENTDATA AS field_value,
            a.CREATED_ON CREATED_ON,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
            a.LAST_MODIFIED_DATE LAST_MODIFIED_DATE,
            A.RID,
            PK_MOREDETAILS RESPONSE_ID
  FROM  er_moredetails a, ER_STUDYTEAM t
  WHERE a.MD_MODNAME = 'studyteam' AND fk_modpk = t.PK_STUDYTEAM;
  
  Commit
  /
  INSERT INTO track_patches
VALUES(seq_track_patches.nextval,417,11,'11_update_ERV_STUDYTEAM_MORE_DETAILS.sql',sysdate,'v11 #818');
commit;
/