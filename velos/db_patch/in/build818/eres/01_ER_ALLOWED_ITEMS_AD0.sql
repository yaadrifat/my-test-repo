SET DEFINE OFF;
create or replace TRIGGER "ERES"."ER_ALLOWED_ITEMS_AD0"
AFTER DELETE
ON ERES.ER_ALLOWED_ITEMS
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(2000);
  usr VARCHAR(2000);
  oldusr VARCHAR(100);
begin

 usr := getuser(:old.last_modified_by);
 
  oldusr :=getuser(:old.CREATOR);
  
  select seq_audit.nextval into raid from dual;


if usr != null OR usr != ''
  then
      audit_trail.record_transaction
    (raid, 'ER_ALLOWED_ITEMS', :old.rid, 'D', usr);
  else
      audit_trail.record_transaction
    (raid, 'ER_ALLOWED_ITEMS', :old.rid, 'D', oldusr);
  end if;


    
    
        Audit_Trail.column_delete (raid, 'FK_STORAGE', :OLD.FK_STORAGE);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', :OLD.LAST_MODIFIED_BY);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_AI', :OLD.PK_AI);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
    
      
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,417,1,'01_ER_ALLOWED_ITEMS_AD0.sql',sysdate,'v11 #818');
commit;
/