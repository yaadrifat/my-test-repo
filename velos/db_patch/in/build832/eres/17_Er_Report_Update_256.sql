SET DEFINE OFF;
DECLARE 
	table_check number;
	row_check number;

BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=256;

		IF (row_check = 1) then
			UPDATE er_report SET rep_sql_clob = 'select
 inv_num,
study_number,
milestone_type,
msach_milestone_desc,
fk_site_enrolling,
enrolling_site,
msach_ptstudy_id msach_ptstudy_id,
TO_CHAR(msach_ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) msach_ach_date,
milestone_amt,
payment_type,
payment_for,
fk_study,
fk_milestone,
inv_amt,
rec_amt,
payment_dt
from VDA.vda_v_milestone_achvd_det
WHERE fk_study IN (:studyId)
AND TRUNC(MSACH_ACH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)'
  WHERE pk_report=256;
    
		END IF;
	END IF;
  COMMIT;
  END;
  /
  
  INSERT INTO track_patches
VALUES(seq_track_patches.nextval,431,17,'17_Er_Report_Update_256.sql',sysdate,'v11 #832');

COMMIT;
/
