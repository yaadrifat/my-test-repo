DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'IS_CURRENT_FINAL'
      and table_name = 'ER_SUBMISSION_STATUS';

  if (v_column_exists = 0) then
      execute immediate 'alter table ER_SUBMISSION_STATUS add (IS_CURRENT_FINAL number)';
  end if;
  commit;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,385,10,'10_Alter_substatus.sql',sysdate,'v11 #786');

commit;
