set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
  from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='irbPend') 
  and BROWSERCONF_COLNAME='STUDY_PRINV_NAME';
  if (v_record_exists = 0) then
 INSERT INTO er_browserconf 
	(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL)
	VALUES(SEQ_ER_BROWSERCONF.nextval ,(select pk_browser from er_browser where browser_module='irbPend'),'STUDY_PRINV_NAME',(select max(BROWSERCONF_SEQ)+1 from er_browserconf  where fk_browser=(select pk_browser from er_browser where browser_module='irbPend')),'{"key":"STUDY_PRINV_NAME", "label":"Principal Investigator", "sortable":true, "resizeable":true,"hideable":true}','Principal Investigator');

 end if;
 commit;
 end;
/
  
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
  from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='irbSub') 
  and BROWSERCONF_COLNAME='REVIEW_TYPE';
  if (v_record_exists = 0) then
 INSERT INTO er_browserconf 
	(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ)
	VALUES(SEQ_ER_BROWSERCONF.nextval ,(select pk_browser from er_browser where browser_module='irbSub'),'REVIEW_TYPE',(select max(BROWSERCONF_SEQ)+1 from er_browserconf  where fk_browser=(select pk_browser from er_browser where browser_module='irbSub')));
 
 end if;
 commit;
 end;
/
 
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
  from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='irbSub') 
  and BROWSERCONF_COLNAME='FK_REVIEW_MEETING';
  if (v_record_exists = 0) then
 INSERT INTO er_browserconf 
	(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL)
	VALUES(SEQ_ER_BROWSERCONF.nextval ,(select pk_browser from er_browser where browser_module='irbSub'),'FK_REVIEW_MEETING',(select max(BROWSERCONF_SEQ)+1 from er_browserconf  where fk_browser=(select pk_browser from er_browser where browser_module='irbSub')),'{"key":"FK_REVIEW_MEETING", "label":"Meeting Date",  "sortable":true, "resizeable":true,"hideable":true}','MEETING DATE');
 
 end if;
 commit;
 end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
  from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='irbSub') 
  and BROWSERCONF_COLNAME='MEETING_DATE';
  if (v_record_exists = 0) then
 INSERT INTO er_browserconf 
	(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ)
	VALUES(SEQ_ER_BROWSERCONF.nextval ,(select pk_browser from er_browser where browser_module='irbSub'),'MEETING_DATE',(select max(BROWSERCONF_SEQ)+1 from er_browserconf  where fk_browser=(select pk_browser from er_browser where browser_module='irbSub')));
 
 end if;
 commit;
 end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
  from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='irbSub') 
  and BROWSERCONF_COLNAME='PK_SUBMISSION_STATUS';
  if (v_record_exists = 0) then
 INSERT INTO er_browserconf 
	(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ)
	VALUES(SEQ_ER_BROWSERCONF.nextval ,(select pk_browser from er_browser where browser_module='irbSub'),'PK_SUBMISSION_STATUS',(select max(BROWSERCONF_SEQ)+1 from er_browserconf  where fk_browser=(select pk_browser from er_browser where browser_module='irbSub')));
 
 end if;
 commit;
 end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
  from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='irbReview') 
  and BROWSERCONF_COLNAME='PK_SUBMISSION_STATUS';
  if (v_record_exists = 0) then
 INSERT INTO er_browserconf 
	(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ)
	VALUES(SEQ_ER_BROWSERCONF.nextval ,(select pk_browser from er_browser where browser_module='irbReview'),'PK_SUBMISSION_STATUS',(select max(BROWSERCONF_SEQ)+1 from er_browserconf  where fk_browser=(select pk_browser from er_browser where browser_module='irbReview')));
 
 end if;
 commit;
 end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
  from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='irbMeeting') 
  and BROWSERCONF_COLNAME='PK_SUBMISSION_STATUS';
  if (v_record_exists = 0) then
 INSERT INTO er_browserconf 
	(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ)
	VALUES(SEQ_ER_BROWSERCONF.nextval ,(select pk_browser from er_browser where browser_module='irbMeeting'),'PK_SUBMISSION_STATUS',(select max(BROWSERCONF_SEQ)+1 from er_browserconf  where fk_browser=(select pk_browser from er_browser where browser_module='irbMeeting')));
 
 end if;
 commit;
 end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,385,02,'02_Inst_browconf.sql',sysdate,'v11 #786');

commit;
 
 