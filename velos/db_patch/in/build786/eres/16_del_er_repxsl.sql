declare
begin
	delete  from  er_repxsl where pk_repxsl in (269,270,271,272);
commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,385,16,'16_del_er_repxsl.sql',sysdate,'v11 #786');

commit;
