set define off;
update er_codelst set codelst_hide='Y' where codelst_type ='skin' and codelst_subtyp <> 'vel_default';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,349,1,'01_update_er_codelst.sql',sysdate,'v10 LE #749');

commit;

