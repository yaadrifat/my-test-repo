insert into er_ctrltab 
	(
	pk_ctrltab,
	ctrl_value, 
	ctrl_desc, 
	ctrl_key, 
	ctrl_seq
	) 
	values
	(
	SEQ_ER_CTRLTAB.nextval,
	'MODQRYDASH',
	'Query Dashboard',
	'module',
	(select max(ctrl_seq) from  er_ctrltab where ctrl_key='module') + 1
	);
commit;


INSERT INTO er_ctrltab
  (
    pk_ctrltab,
    ctrl_value,
    ctrl_desc,
    ctrl_key,
    ctrl_seq
  )
  VALUES
  (
    SEQ_ER_CTRLTAB.nextval,
    'QRYDASH',
    'Query Dashboard',
    'app_rights',
    (select max(ctrl_seq) from  er_ctrltab where ctrl_key='app_rights')+1
  );
commit;

update er_account set ac_modright= ac_modright || 0  ;
commit;

update er_grps set grp_rights=grp_rights||0  ;
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,349,2,'02_update_er_ctrltab.sql',sysdate,'v10 LE #749');

commit;





