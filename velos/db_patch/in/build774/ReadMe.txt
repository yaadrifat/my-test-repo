/////*********This readMe is specific to v10 build #774	  **********////

For UCSD Calendar issues. (Ticket:38134)

This no particular test case to start with. But we need to manufacture one scenario to  replicate the issues.

During Developer investigation , we found that ORG_ID (Event_def, EVENT_Assoc)  of Calendar records was found zero"0" , for come of the calendars. Due to this the sequence at event sequence tab on  select event page is giving  "wrong sequence numbers".  Here we suspect that this ORG_ID is having ZERO value for the records that are being transferred from eTools.
Now to replicate the bug , you need to make changes for ORG_ID Column in Event_def &  EVENT_Assoc  to ZERO for Calendar records and see the effect on event sequence tab   

A document sent by Client is under Doc folder for reference. 




