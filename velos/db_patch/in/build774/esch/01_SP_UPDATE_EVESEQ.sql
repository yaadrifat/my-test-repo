create or replace
PROCEDURE      "SP_UPDATE_EVESEQ" (p_calId in NUMBER, p_arrEventId ARRAY_STRING,
		p_arrEveNewSeq ARRAY_STRING,p_calassoc in char,p_ipadd in varchar,p_user in number, o_ret OUT NUMBER)
AS
v_cnt NUMBER;
v_event_id NUMBER;
v_eveNew_seq NUMBER;
i NUMBER;
TYPE asc_arr_t IS TABLE OF varchar2(32000)  INDEX BY  varchar(100);
v_updEveIdsArr asc_arr_t;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SP_UPDATE_EVESEQ', pLEVEL  => Plog.LFATAL);
BEGIN

	v_cnt :=  p_arrEventId.COUNT();
	i:=1;
	WHILE i <= v_cnt LOOP
		v_event_id := TO_NUMBER(p_arrEventId(i));
		BEGIN

			IF (p_calassoc='P' or p_calassoc='L') THEN
     select ERES.pkg_util.f_join(Cursor(select event_id from event_def where chain_id=p_calId and event_type<>'P' and cost=(select cost from event_def where event_id=v_event_id)),',') 
     into v_updEveIdsArr(i) from dual;
     ELSE
    select ERES.pkg_util.f_join(Cursor(select event_id from event_assoc where chain_id=p_calId and event_type<>'P' and cost=(select cost from event_assoc where event_id=v_event_id)),',') 
     into v_updEveIdsArr(i) from dual;
			END IF;

			EXCEPTION  WHEN OTHERS THEN
			P('ERROR');
			o_ret:=-1;
			RETURN;
		END;
		i := i + 1;
	END LOOP; --v_cnt loop
  i:=1;
  WHILE i <= v_cnt LOOP
  v_eveNew_seq := TO_NUMBER(p_arrEveNewSeq(i));
  BEGIN
  IF (p_calassoc='P' or p_calassoc='L') THEN
  --plog.fatal(pCTX,'SP_UPDATE_EVESEQ1>>'||p_user);
  --plog.fatal(pCTX,'SP_UPDATE_EVESEQ2>>'||v_updEveIdsArr(i));
  execute immediate 'update event_def set COST='||v_eveNew_seq||',LAST_MODIFIED_BY = '||p_user||' where event_type<>''P''  and  chain_id='||p_calId||' and event_id in ('||v_updEveIdsArr(i)||')';
  else
  execute immediate 'update event_assoc set COST='||v_eveNew_seq||',LAST_MODIFIED_BY = '||p_user||' where event_type<>''P''  and  chain_id='||p_calId||' and event_id in ('||v_updEveIdsArr(i)||')';
  END IF;
  EXCEPTION  WHEN OTHERS THEN
			P('ERROR');
			o_ret:=-1;
			RETURN;
		END;
    i := i + 1;
  END LOOP; --v_cnt loop
	COMMIT;
	o_ret:=0;

END; --end of SP_UPDATE_EVESEQ
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,373,1,'01_SP_UPDATE_EVESEQ.sql',sysdate,'v10 #774');

commit;


