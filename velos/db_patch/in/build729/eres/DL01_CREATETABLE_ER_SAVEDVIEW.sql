set define off;

DECLARE
  v_tab_exists number := 0;  
BEGIN
	
	Select count(*) into v_tab_exists from tabs  where TABLE_NAME = 'ER_SAVEDVIEW';
	if v_tab_exists = 0 then
	
	execute immediate 'CREATE TABLE ER_SAVEDVIEW(
	PK_SAVEDVIEW NUMBER(10,0),
	SAVEDVIEW_FILTERS VARCHAR2(4000 BYTE),
          SAVEDVIEW_CRITERIA VARCHAR2(2000 BYTE),
	SAVEDVIEW_ISDEFAULT CHAR(1 BYTE),
	SAVEDVIEW_MODULE VARCHAR2(100 BYTE),
	FK_USER NUMBER(10,0),
	CREATOR	NUMBER(10,0),
	CREATED_ON DATE DEFAULT sysdate,
	LAST_MODIFIED_BY NUMBER(10,0),
	LAST_MODIFIED_DATE DATE DEFAULT sysdate,
	RID NUMBER(10,0))';

execute immediate 'COMMENT ON TABLE ER_SAVEDVIEW IS ''Stores Dashboard View''';
execute immediate 'COMMENT ON COLUMN ER_SAVEDVIEW.PK_SAVEDVIEW IS ''Primary key''';
execute immediate 'COMMENT ON COLUMN ER_SAVEDVIEW.SAVEDVIEW_FILTERS IS ''Search Filters saved as request String''';
execute immediate 'COMMENT ON COLUMN ER_SAVEDVIEW.SAVEDVIEW_CRITERIA IS ''Search Criteria saved as request String''';
execute immediate 'COMMENT ON COLUMN ER_SAVEDVIEW.SAVEDVIEW_ISDEFAULT IS ''Whether this view is applied by default';
execute immediate 'COMMENT ON COLUMN ER_SAVEDVIEW.SAVEDVIEW_MODULE IS ''Dashboard to which this view is saved for e.g. form response,adverse event etc.''';
execute immediate 'COMMENT ON COLUMN ER_SAVEDVIEW.FK_USER IS ''Stores reference to er_user to indicates for which user view saved''';
execute immediate 'COMMENT ON COLUMN ER_SAVEDVIEW.CREATOR IS ''Uses for Audit trail. Stores who created the record''';
execute immediate 'COMMENT ON COLUMN ER_SAVEDVIEW.LAST_MODIFIED_BY IS ''Uses for Audit trail. Stores who modified the record recently''';
execute immediate 'COMMENT ON COLUMN ER_SAVEDVIEW.LAST_MODIFIED_DATE IS ''Uses for Audit trail. Stores when modified the record recently''';
execute immediate 'COMMENT ON COLUMN ER_SAVEDVIEW.CREATED_ON IS ''Uses for Audit trail. Stores when created the record''';
execute immediate 'COMMENT ON COLUMN ER_SAVEDVIEW.RID IS ''Uses for Audit trail. Stores sequence generated unique value''';

execute immediate 'CREATE SEQUENCE SEQ_ER_SAVEDVIEW MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE';
	else
		p('no action needed. Form response Compare utility patch exists');
	end if;	
end; 
/