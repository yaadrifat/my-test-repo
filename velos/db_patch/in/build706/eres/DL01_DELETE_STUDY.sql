  CREATE OR REPLACE PROCEDURE "ERES"."DELETE_STUDY" (p_study IN NUMBER, p_userId in varchar2, p_ipAdd in varchar2)
IS

/* Modified by Sonia Abrol, to reduce the data count in linked forms when form data is deleted*/

 BEGIN
  --delete patient study data


DELETE FROM SCH_ADVERSEVE WHERE FK_STUDY =  p_study ;

DELETE FROM SCH_ALERTNOTIFY WHERE FK_PATPROT IN
(SELECT pk_patprot FROM ER_PATPROT WHERE FK_STUDY = p_study);

DELETE FROM ER_FORMSLINEAR WHERE form_type = 'P' AND fk_filledform IN (SELECT pk_patforms FROM ER_PATFORMS WHERE FK_PATPROT IN
(SELECT pk_patprot FROM ER_PATPROT WHERE FK_STUDY = p_study));


DELETE FROM ER_PATFORMS WHERE FK_PATPROT IN
(SELECT pk_patprot FROM ER_PATPROT WHERE FK_STUDY = p_study);


DELETE FROM ER_FORMSLINEAR WHERE form_type = 'S' AND fk_filledform IN (SELECT pk_studyforms FROM ER_STUDYFORMS WHERE fk_study = p_study);

FOR i IN (SELECT fk_formlib, COUNT(*) ct FROM ER_STUDYFORMS WHERE fk_study = p_study AND NVL(record_type,'N') <> 'D'  GROUP BY fk_formlib)
LOOP

   UPDATE ER_LINKEDFORMS
   SET lf_datacnt = (NVL(lf_datacnt,0) - i.ct) WHERE fk_formlib = i.fk_formlib;

END LOOP;

DELETE FROM ER_STUDYFORMS WHERE fk_study = p_study;


 DELETE FROM SCH_CRFNOTIFY
        WHERE fk_crf IN
         (SELECT pk_crf FROM SCH_CRF
           WHERE FK_PATPROT IN
              (SELECT pk_patprot FROM
               ER_PATPROT WHERE FK_STUDY = p_study)
           );

     DELETE FROM SCH_CRFSTAT
         WHERE fk_crf IN
          (SELECT pk_crf FROM SCH_CRF
            WHERE FK_PATPROT IN
               (SELECT pk_patprot FROM
                ER_PATPROT WHERE FK_STUDY = p_study)
               );

    DELETE FROM SCH_CRF
        WHERE FK_PATPROT IN
          (SELECT pk_patprot FROM
           ER_PATPROT WHERE FK_STUDY = p_study);

    DELETE FROM SCH_EVENTS1
          WHERE FK_PATPROT IN
              (SELECT pk_patprot FROM
               ER_PATPROT WHERE FK_STUDY = p_study);


   DELETE FROM SCH_BGTAPNDX
   WHERE fk_budget IN (SELECT pk_budget FROM
                     SCH_BUDGET WHERE fk_study = p_study );


   DELETE FROM SCH_LINEITEM WHERE
    FK_BGTSECTION IN (SELECT PK_BUDGETSEC FROM SCH_BGTSECTION
   WHERE fk_bgtcal IN ( SELECT PK_BGTCAL FROM SCH_BGTCAL
                       WHERE fk_budget IN
                          (SELECT pk_budget FROM
                     SCH_BUDGET WHERE fk_study = p_study )));



   DELETE FROM SCH_BGTSECTION
   WHERE fk_bgtcal IN ( SELECT PK_BGTCAL FROM SCH_BGTCAL
                       WHERE fk_budget IN
                          (SELECT pk_budget FROM
                     SCH_BUDGET WHERE fk_study = p_study ));

   DELETE FROM SCH_BGTCAL
                       WHERE fk_budget IN
                          (SELECT pk_budget FROM
                     SCH_BUDGET WHERE fk_study = p_study );

   DELETE FROM SCH_BGTUSERS
                       WHERE fk_budget IN
                          (SELECT pk_budget FROM
                     SCH_BUDGET WHERE fk_study = p_study );



   DELETE FROM SCH_BUDGET WHERE fk_study = p_study;

--delete study protocol event data

-- delete event cost
DELETE FROM SCH_EVENTCOST
WHERE fk_event IN (SELECT event_id FROM EVENT_ASSOC
                   WHERE CHAIN_ID IN
                           (SELECT event_id
                            FROM EVENT_ASSOC
                            WHERE CHAIN_ID = p_study AND
                            EVENT_TYPE='P'));


-- delete event crf

DELETE FROM SCH_CRFLIB
WHERE fk_events IN (SELECT event_id FROM EVENT_ASSOC
                   WHERE CHAIN_ID IN
                           (SELECT event_id
                            FROM EVENT_ASSOC
                            WHERE CHAIN_ID = p_study AND
                            EVENT_TYPE='P'));


-- delete event user

DELETE FROM SCH_EVENTUSR
WHERE fk_event IN (SELECT event_id FROM EVENT_ASSOC
                   WHERE CHAIN_ID IN
                           (SELECT event_id
                            FROM EVENT_ASSOC
                            WHERE CHAIN_ID = p_study AND
                           EVENT_TYPE='P'));


-- delete event doc - doc master data
DELETE FROM SCH_EVENTDOC
WHERE fk_event IN (SELECT event_id FROM
                   EVENT_ASSOC
                   WHERE CHAIN_ID IN
                           (SELECT event_id
                            FROM EVENT_ASSOC
                            WHERE CHAIN_ID = p_study AND
                            EVENT_TYPE='P'));


-- delete event doc - doc data
DELETE FROM SCH_DOCS
WHERE pk_docs IN (SELECT pk_docs FROM SCH_EVENTDOC WHERE
                 fk_event IN (SELECT event_id FROM EVENT_ASSOC
                   WHERE CHAIN_ID IN
                           (SELECT event_id
                            FROM EVENT_ASSOC
                            WHERE CHAIN_ID = p_study AND
                            EVENT_TYPE='P')));




   DELETE FROM SCH_DISPATCHMSG
   WHERE fk_event IN (SELECT event_id FROM EVENT_ASSOC
                         WHERE CHAIN_ID IN
                           (SELECT event_id
                            FROM EVENT_ASSOC
                            WHERE CHAIN_ID = p_study AND
                            EVENT_TYPE='P'));

   DELETE FROM SCH_DISPATCHMSG
   WHERE fk_study = p_study;


--delete all study protocol events

--set the notification status to -2

UPDATE SCH_MSGTRAN SET msg_status=-2 WHERE fk_protocol  IN (SELECT event_id FROM  event_assoc WHERE CHAIN_ID = p_study AND
EVENT_TYPE='P' ) ;


DELETE FROM EVENT_ASSOC
WHERE CHAIN_ID IN (SELECT event_id FROM EVENT_ASSOC
                   WHERE CHAIN_ID = p_study AND
                   EVENT_TYPE='P');

DELETE FROM SCH_PROTSTAT
WHERE  FK_EVENT  IN (SELECT event_id FROM EVENT_ASSOC
                   WHERE CHAIN_ID = p_study AND
                   EVENT_TYPE='P');


--delete study protocol

DELETE FROM EVENT_ASSOC
WHERE CHAIN_ID = p_study AND
EVENT_TYPE='P';

--delete study data

 DELETE FROM ER_MSGCNTR WHERE fk_study = p_study;

 DELETE FROM ER_MILEAPNDX  WHERE fk_study = p_study;
 DELETE FROM ER_MILENOTLOG WHERE fk_study = p_study;
 DELETE FROM ER_MILEPAYMENT WHERE fk_study = p_study;
 DELETE FROM ER_MILESTONE WHERE fk_study = p_study;

 DELETE FROM ER_PATSTUDYSTAT
  WHERE fk_study = p_study;

 DELETE FROM ER_PATLABS WHERE fk_patprot IN (SELECT pk_patprot FROM ER_PATPROT WHERE fk_study  = p_study) ;

 DELETE FROM ER_PATLABS WHERE fk_study = p_study ;

  DELETE FROM ER_PATPROT
    WHERE fk_study  = p_study;

  DELETE FROM ER_SAVEDREP  WHERE fk_study = p_study;

  DELETE FROM ER_STUDYAPNDX  WHERE fk_studyver IN
 (SELECT pk_studyver FROM ER_STUDYVER WHERE fk_study = p_study);

  DELETE FROM ER_STUDYSEC  WHERE fk_studyver IN
 (SELECT pk_studyver FROM ER_STUDYVER WHERE fk_study = p_study);

  DELETE FROM ER_STATUS_HISTORY WHERE status_modtable ='er_studyver' AND
  status_modpk IN ( SELECT pk_studyver FROM ER_STUDYVER WHERE fk_study = p_study);

  DELETE FROM ER_STUDYVER  WHERE fk_study = p_study;

  DELETE FROM ER_STUDYID  WHERE fk_study = p_study;

  DELETE FROM ER_STUDYSTAT  WHERE fk_study = p_study;
  DELETE FROM ER_STUDYTEAM  WHERE fk_study = p_study;


  --JM: 08Nov2006
  DELETE FROM ER_STATUS_HISTORY WHERE status_modtable ='er_studyteam' AND
  status_modpk IN (SELECT pk_studyteam FROM ER_STUDYTEAM WHERE fk_study = p_study);
  -----/////////

  DELETE FROM ER_STUDYVIEW WHERE fk_study = p_study;

--bug fix #2322
  DELETE FROM ER_LINKEDFORMS WHERE fk_study=p_study;

  DELETE FROM ER_STUDYSITES_APNDX WHERE  fk_studysites IN (SELECT pk_studysites FROM ER_STUDYSITES WHERE fk_study=p_study);

  DELETE FROM ER_STUDYSITES WHERE fk_study=p_study;

  DELETE FROM ER_STUDYTXARM WHERE fk_study=p_study;

  DELETE FROM ER_STUDY_SITE_RIGHTS WHERE fk_study=p_study;

  --delete portals linked with teh study

  for l in (select pk_portal from ER_PORTAL where fk_study = p_study)
  LOOP
  	sp_delete_portal(l.pk_portal, p_userId, p_ipAdd);
  end loop;

  --JM: 24Dec2009: #4172 Delete the er_Settings table data
  DELETE FROM ER_SETTINGS WHERE SETTINGS_MODNUM = P_STUDY;

  --Ankit: 24-Nov-2011 CTRP-20527
  DELETE FROM ER_STUDY_NIH_GRANT WHERE FK_STUDY=P_STUDY;
  DELETE FROM ER_STUDY_INDIDE WHERE FK_STUDY=p_study;

--finally!!!
  DELETE FROM ER_STUDY WHERE pk_study = p_study;

  COMMIT;
END;
/

CREATE OR REPLACE SYNONYM ESCH.DELETE_STUDY FOR DELETE_STUDY;

CREATE OR REPLACE SYNONYM EPAT.DELETE_STUDY FOR DELETE_STUDY;

GRANT EXECUTE, DEBUG ON DELETE_STUDY TO EPAT;

GRANT EXECUTE, DEBUG ON DELETE_STUDY TO ESCH; 

