set define off;
Declare
  v_record_exists number := 0;
begin
	Select count(*) into v_record_exists FROM ER_LKPVIEWCOL WHERE 
	FK_LKPCOL = (SELECT PK_LKPCOL FROM ER_LKPCOL WHERE LKPCOL_KEYWORD='USRMIDNAME' AND LKPCOL_NAME='usr_midname')
	AND FK_LKPVIEW = (SELECT PK_LKPVIEW FROM er_lkpview WHERE LKPVIEW_KEYWORD='userProfile');
	
	if (v_record_exists = 0) then
	INSERT INTO ER_LKPVIEWCOL
	(
		PK_LKPVIEWCOL,
		FK_LKPCOL,
		LKPVIEW_IS_SEARCH,
		LKPVIEW_SEQ,
		FK_LKPVIEW,
		LKPVIEW_DISPLEN,
		LKPVIEW_IS_DISPLAY
	)
	VALUES
	(
		(select max(PK_LKPVIEWCOL)+1 from ER_LKPVIEWCOL),
		(SELECT PK_LKPCOL FROM ER_LKPCOL WHERE LKPCOL_KEYWORD='USRMIDNAME' AND LKPCOL_NAME='usr_midname'
		AND FK_LKPLIB = (SELECT PK_LKPVIEW FROM er_lkpview WHERE LKPVIEW_KEYWORD='userProfile')),
		'N',
		(Select max(lkpview_seq)+1 from ER_LKPVIEWCOL where 
		 FK_LKPVIEW = (SELECT PK_LKPVIEW FROM er_lkpview WHERE LKPVIEW_KEYWORD='userProfile')),
		(SELECT PK_LKPVIEW FROM ER_LKPVIEW WHERE LKPVIEW_KEYWORD='userProfile'),
		'25%',
		'N'
	);
	commit;
	end if;

	Select count(*) into v_record_exists FROM ER_LKPVIEWCOL WHERE 
	FK_LKPCOL = (SELECT PK_LKPCOL FROM ER_LKPCOL WHERE LKPCOL_KEYWORD='USRLOGNAME' AND LKPCOL_NAME='usr_logname')
	AND FK_LKPVIEW = (SELECT PK_LKPVIEW FROM er_lkpview WHERE LKPVIEW_KEYWORD='userProfile');

	if (v_record_exists = 0) then
	INSERT INTO ER_LKPVIEWCOL
	(
		PK_LKPVIEWCOL,
		FK_LKPCOL,
		LKPVIEW_IS_SEARCH,
		LKPVIEW_SEQ,
		FK_LKPVIEW,
		LKPVIEW_DISPLEN,
		LKPVIEW_IS_DISPLAY
	)
	VALUES
	(
		(select max(PK_LKPVIEWCOL)+1 from ER_LKPVIEWCOL),
		(SELECT PK_LKPCOL FROM ER_LKPCOL WHERE LKPCOL_KEYWORD='USRLOGNAME' AND LKPCOL_NAME='usr_logname'
		AND FK_LKPLIB = (SELECT PK_LKPVIEW FROM er_lkpview WHERE LKPVIEW_KEYWORD='userProfile')),
		'N',
		(Select max(lkpview_seq)+1 from ER_LKPVIEWCOL where 
		 FK_LKPVIEW = (SELECT PK_LKPVIEW FROM er_lkpview WHERE LKPVIEW_KEYWORD='userProfile')),
		(SELECT PK_LKPVIEW FROM ER_LKPVIEW WHERE LKPVIEW_KEYWORD='userProfile'),
		'25%',
		'N'
	);
	commit;
	end if;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,458,2,'02_ER_LKPVIEWCOL.sql',sysdate,'v11.1.0 #859');
commit;	