/////*********This readMe is specific to v11.1.0 build #859(compatible with Oracle 12c)**********////
*********************************************************************************************************************************************************************

1. Regular bug fixes.
2. The enhancement released in this build are:

	I. User More Identifiers May 06 2019 v0.5.docx.
		Points Covered:
		a) User search page (usersearchdetails.jsp)
		b) On Pages mentioned in mockup1 i.e. 
			i) Manage Account >> Active Account Users
			ii) Manage Account >> Non System Users
			iii) Manage Account >> Blocked/Deactivated Users
		
		c) Page mentioned in mockup2 i.e. 
			i) "Assign Users to Team" page on Study Team tab.
		
		d) Page mentioned in mockup3 i.e.
			i) "Network User Search" page (autocomplete search)
			ii) "Manage Inventory >> Specimen Details" page (autocomplete search)
			
		e) User lookup in reports.
	
	II. ACC-46493 -- UI Performance changes May 1 2019 v0.2.docx
		Points Covered:
		a) UI – 1:Include Middle Name field in User Details. 
	
	III. ViewAndDelete-Achievments_UI_Changes.docx
		 a) "Financials >> Open >> View/Delete Achievements" page (UI changes)
	
	IV. We have introduced new theme i.e. "Default WCG".


Note: This theme is currently under development, please do not test application by appying this theme.
      Please make sure that the db patches must be executed in sequence mentioned on them.

   


