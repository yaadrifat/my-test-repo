set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v11.1.0 #856' 
where CTRL_KEY = 'app_version' ; 

commit;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,455,0,'00_er_version.sql',sysdate,'v11.1.0 #856');
commit;
