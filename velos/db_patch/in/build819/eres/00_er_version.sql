set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v11 #819' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,418,0,'00_er_version.sql',sysdate,'v11 #819');
commit;
/