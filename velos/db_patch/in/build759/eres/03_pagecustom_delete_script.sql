--deleted custom field entriews for enabling the study status fields 

DECLARE
  v_item_exists number := 0;  
BEGIN
  select count(*) into v_item_exists from er_pagecustom where fk_account=(select fk_account from er_site where site_name='ORGNAME') and pagecustom_page='studystatus';
  if (v_item_exists = 1) then
    delete from er_pagecustomflds where fk_pagecustom=(select pk_pagecustom from er_pagecustom where fk_account=(select fk_account from er_site where site_name='ORGNAME') and pagecustom_page='studystatus');
    delete from er_pagecustom where fk_account=(select fk_account from er_site where site_name='ORGNAME') and pagecustom_page='studystatus';
    commit;
  end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,358,3,'03_pagecustom_delete_script.sql',sysdate,'v10 #759');

commit;