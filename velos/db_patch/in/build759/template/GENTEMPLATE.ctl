LOAD DATA INFILE * INTO TABLE ui_flx_page
APPEND
FIELDS TERMINATED BY ','
(PAGE_ID,
js_file filler char,
"PAGE_DEF" LOBFILE (js_file) TERMINATED BY EOF NULLIF JS_FILE = 'NONE',
json_file filler char,
"PAGE_JS" LOBFILE (json_file) TERMINATED BY EOF NULLIF JSON_FILE = 'NONE')
BEGINDATA
protocol,page_def.json,page_js.js