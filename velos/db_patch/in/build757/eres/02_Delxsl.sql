set define off;

delete from er_repxsl where fk_report in (110,159);
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,356,2,'02_Delxsl.sql',sysdate,'v10 #757');
commit;