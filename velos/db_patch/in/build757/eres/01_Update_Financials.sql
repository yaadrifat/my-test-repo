set define off;
DECLARE
  v_obj_subtyp number := 0;
  
BEGIN
  SELECT COUNT(*) INTO v_obj_subtyp 
  FROM er_object_settings 
  WHERE object_subtype='milestone_menu' and object_name='manage_accnt';
  
  IF (v_obj_subtyp != 0) THEN
    update er_object_settings 
    set object_displaytext='Financials' 
    where object_name='manage_accnt' and object_subtype='milestone_menu';
  END IF;
  commit;
  
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,356,1,'01_Update_Financials.sql',sysdate,'v10 #757');

commit;

