set define off;
DECLARE
  v_column_exists number;

BEGIN
  Select count(*) into v_column_exists
  from user_tab_cols
  where TABLE_NAME = 'EVENT_ASSOC'
  AND COLUMN_NAME = 'EVENT_CPTCODE';
    
  IF(v_column_exists = 0) THEN
    execute immediate 'alter table event_assoc modify event_cptcode varchar2(4000 BYTE)';
  END IF;
  commit;
  
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,356,3,'03_alter_eventassoccpt4code.sql',sysdate,'v10 #757');

commit;

