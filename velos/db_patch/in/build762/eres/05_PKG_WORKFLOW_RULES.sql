create or replace
PACKAGE        "PKG_WORKFLOW_RULES"
IS
  FUNCTION F_studyInit_protDef(studyId NUMBER) RETURN NUMBER;

  FUNCTION F_studyInit_crcReview(studyId NUMBER) RETURN NUMBER;

  FUNCTION F_studyInit_irbReview(studyId NUMBER) RETURN NUMBER;
  
  FUNCTION F_studyActive_Enrolling(studyId NUMBER) RETURN NUMBER;

  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_WORKFLOW', pLEVEL  => Plog.LFATAL);
END PKG_WORKFLOW_RULES;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,361,5,'05_PKG_WORKFLOW_RULES.sql',sysdate,'v10 #762');

commit;