create or replace TRIGGER "ERES"."ER_NWUSERS_AI0" AFTER INSERT ON ERES.ER_NWUSERS REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
  nwuserID number;
  nwuserStatus number;
  nwuserCreatedDate date;
  nwuserCreator number;
  nwuserIp varchar2(20);
BEGIN
  
  nwuserID:= :NEW.PK_NWUSERS;
  nwuserStatus:= :NEW.NWU_STATUS;
  nwuserCreatedDate:= :NEW.CREATED_ON;
  nwuserCreator:= :NEW.CREATOR;
  nwuserIp:= :NEW.IP_ADD;
  insert into er_status_history(PK_STATUS,STATUS_MODPK,STATUS_MODTABLE,FK_CODELST_STAT,STATUS_DATE,CREATOR,RECORD_TYPE,IP_ADD,STATUS_ISCURRENT) values(SEQ_ER_STATUS_HISTORY.nextval,nwuserID,'er_nwusers',nwuserStatus,nwuserCreatedDate,nwuserCreator,'N',nwuserIp,1);
  
END;
/
INSERT INTO track_patches
	VALUES(seq_track_patches.nextval,388,6,'06_er_nwusers_ai0.sql',sysdate,'v11 #789');

	commit;
