/////*********This readMe is specific to v11.1.0 build #847(compatible with Oracle 12c)**********////
*********************************************************************************************************************************************************************
1. Regular bug fixing.

2. We have made changes in antisamy-highsecurity.xml file  to increase the length of input field. Recently we have faced one issue on sanbox env ,Size of input length on sanbox env was greater than default length in antisamy-highsecurity.xml.
   So we have enabled the maxInputSize and set the length  10000000 in antisamy-highsecurity.xml. 
   Please include the antisamy-highsecurity.xml file in conf folder of running wildfly server.
