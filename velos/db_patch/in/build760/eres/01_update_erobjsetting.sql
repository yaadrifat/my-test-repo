set define off;
DECLARE
  v_obj_subtyp number := 0;
  
BEGIN
  SELECT COUNT(*) INTO v_obj_subtyp 
  FROM er_object_settings 
  WHERE object_subtype='1' and object_type = 'T' and object_name='milestone_tab';
  
  IF (v_obj_subtyp != 0) THEN
    update er_object_settings 
    set object_displaytext='Milestones' 
    where object_name='milestone_tab' and object_type = 'T' and object_subtype='1';
  END IF;
  commit;
  
END;
/

DECLARE
  v_obj_subtyp number := 0;
  
BEGIN
  SELECT COUNT(*) INTO v_obj_subtyp 
  FROM er_object_settings 
  WHERE object_subtype='12' and object_type = 'T' and object_name='study_tab';
  
  IF (v_obj_subtyp != 0) THEN
    update er_object_settings 
    set object_displaytext='Milestones' 
    where object_name='study_tab' and object_type = 'T' and object_subtype='12';
  END IF;
  commit;
  
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,359,1,'01_update_erobjsetting.sql',sysdate,'v10 #760');

commit;
