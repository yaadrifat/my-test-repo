DECLARE
  v_column_exists number;

BEGIN
  Select count(*) into v_column_exists
  from user_tab_cols
  where TABLE_NAME = 'EVENT_DEF'
  AND COLUMN_NAME = 'EVENT_CPTCODE';
    
  IF(v_column_exists != 0) THEN
    execute immediate 'alter table event_def modify event_cptcode varchar2(4000 BYTE)';
  END IF;
  commit;
  
END;
/


DECLARE
  v_column_exists number;

BEGIN
  Select count(*) into v_column_exists
  from user_tab_cols
  where TABLE_NAME = 'EVENT_ASSOC'
  AND COLUMN_NAME = 'EVENT_CPTCODE';
    
  IF(v_column_exists != 0) THEN
    execute immediate 'alter table event_assoc modify event_cptcode varchar2(4000 BYTE)';
  END IF;
  commit;
  
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,357,1,'01_updatecpt4code.sql',sysdate,'v10 #758');

commit;




