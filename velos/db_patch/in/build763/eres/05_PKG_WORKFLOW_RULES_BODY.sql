create or replace
PACKAGE BODY        "PKG_WORKFLOW_RULES"
AS
  FUNCTION F_studyInit_protDef(studyId NUMBER) RETURN NUMBER
  AS
  v_RETURN_NO NUMBER := 0;
  BEGIN
    SELECT COUNT(*) INTO v_RETURN_NO from ER_STUDY where PK_STUDY= studyId;
    RETURN v_RETURN_NO;
  END;

  FUNCTION F_studyInit_irbReview(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  v_form_id  NUMBER := 0;
  v_studyStatusAvail NUMBER :=0;

  BEGIN
  
    select count(*) into v_studyStatusAvail from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'app_CHR' and codelst_hide='N';
  
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'app_CHR');
    IF v_studyStatusAvail =0 then
      v_RETURN_NO := -1; --Status not exists
    else
    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;
    end if;

    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;

  FUNCTION F_studyActive_Enrolling(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  v_form_id  NUMBER := 0;
  v_studyStatusAvail NUMBER :=0;

  BEGIN
  
    select count(*) into v_studyStatusAvail from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'active' and codelst_hide='N';
  
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'active');
    IF v_studyStatusAvail =0 then
      v_RETURN_NO := -1; --Status not exists
    else
    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;
    end if;
dbms_output.put_line('v_RETURN_NO:::'||v_RETURN_NO);
    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;
  
  FUNCTION F_study_complete(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  v_form_id  NUMBER := 0;
  v_studyStatusAvail NUMBER :=0;

  BEGIN
  
    select count(*) into v_studyStatusAvail from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'prmnt_cls' and codelst_hide='N';
  
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'prmnt_cls');
    IF v_studyStatusAvail =0 then
      v_RETURN_NO := -1; --Status not exists
    else
    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;
    end if;
dbms_output.put_line('v_RETURN_NO:::'||v_RETURN_NO);
    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;
  
  FUNCTION F_CDA_InReview(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  v_form_id  NUMBER := 0;
  v_studyStatusAvail NUMBER :=0;

  BEGIN
  
    select count(*) into v_studyStatusAvail from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'cdainrev' and codelst_hide='N';
  
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'cdainrev');
    IF v_studyStatusAvail =0 then
      v_RETURN_NO := -1; --Status not exists
    else
    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;
    end if;
dbms_output.put_line('v_RETURN_NO:::'||v_RETURN_NO);
    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;
  
  FUNCTION F_CDA_Signoff(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  v_form_id  NUMBER := 0;
  v_studyStatusAvail NUMBER :=0;

  BEGIN
  
    select count(*) into v_studyStatusAvail from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'cdasignoff' and codelst_hide='N';
  
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'cdasignoff');
    IF v_studyStatusAvail =0 then
      v_RETURN_NO := -1; --Status not exists
    else
    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;
    end if;
dbms_output.put_line('v_RETURN_NO:::'||v_RETURN_NO);
    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;
  
  FUNCTION F_CA_InReview(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  v_form_id  NUMBER := 0;
  v_studyStatusAvail NUMBER :=0;

  BEGIN
  
    select count(*) into v_studyStatusAvail from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'cainrev' and codelst_hide='N';
  
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'cainrev');
    IF v_studyStatusAvail =0 then
      v_RETURN_NO := -1; --Status not exists
    else
    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;
    end if;
dbms_output.put_line('v_RETURN_NO:::'||v_RETURN_NO);
    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;
  
  FUNCTION F_CA_Signoff(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  v_form_id  NUMBER := 0;
  v_studyStatusAvail NUMBER :=0;

  BEGIN
  
    select count(*) into v_studyStatusAvail from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'casignoff' and codelst_hide='N';
  
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'casignoff');
    IF v_studyStatusAvail =0 then
      v_RETURN_NO := -1; --Status not exists
    else
    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;
    end if;
dbms_output.put_line('v_RETURN_NO:::'||v_RETURN_NO);
    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;
  
  FUNCTION F_Budget_InReview(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  v_form_id  NUMBER := 0;
  v_studyStatusAvail NUMBER :=0;

  BEGIN
  
    select count(*) into v_studyStatusAvail from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'bgtInreview' and codelst_hide='N';
  
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'bgtInreview');
    IF v_studyStatusAvail =0 then
      v_RETURN_NO := -1; --Status not exists
    else
    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;
    end if;
dbms_output.put_line('v_RETURN_NO:::'||v_RETURN_NO);
    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;
  
  FUNCTION F_Budget_Signoff(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  v_form_id  NUMBER := 0;
  v_studyStatusAvail NUMBER :=0;

  BEGIN
  
    select count(*) into v_studyStatusAvail from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'bgtsignoff' and codelst_hide='N';
  
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'bgtsignoff');
    IF v_studyStatusAvail =0 then
      v_RETURN_NO := -1; --Status not exists
    else
    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;
    end if;
dbms_output.put_line('v_RETURN_NO:::'||v_RETURN_NO);
    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;
  
  FUNCTION F_IRB_InitSubmission(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  v_form_id  NUMBER := 0;
  v_studyStatusAvail NUMBER :=0;

  BEGIN
  
    select count(*) into v_studyStatusAvail from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_init_subm' and codelst_hide='N';
  
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_init_subm');
    IF v_studyStatusAvail =0 then
      v_RETURN_NO := -1; --Status not exists
    else
    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;
    end if;
dbms_output.put_line('v_RETURN_NO:::'||v_RETURN_NO);
    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;
  
  FUNCTION F_Study_Enrollment(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;
  v_RETURN_NO NUMBER := 0;
  v_form_id  NUMBER := 0;
  v_studyStatusAvail NUMBER :=0;

  BEGIN
  
    select count(*) into v_studyStatusAvail from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'active_cls' and codelst_hide='N';
  
    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'active_cls');
    IF v_studyStatusAvail =0 then
      v_RETURN_NO := -1; --Status not exists
    else
    IF (v_statusExists > 0) THEN
      v_RETURN_NO := 1; --Done
    ELSE
      v_RETURN_NO := 0; --Not Done
    END IF;
    end if;
dbms_output.put_line('v_RETURN_NO:::'||v_RETURN_NO);
    RETURN v_RETURN_NO;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      v_RETURN_NO := 0; --Not Done
      RETURN 0;
    END;
  END;
  
  FUNCTION F_studyInit_crcReview(studyId NUMBER) RETURN NUMBER
  AS
  v_statusExists NUMBER := 0;
  v_isPiMajAuthor NUMBER := 0;

  v_RETURN_NO NUMBER := 0;
  BEGIN
  /*commented CRC workflow logic as it not needed on generic workflow*/
--    SELECT count(*) INTO v_statusExists FROM ER_STUDYSTAT WHERE FK_STUDY = studyId AND FK_CODELST_STUDYSTAT = (
--    SELECT PK_CODELST from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcApproved');
--
--    IF (v_statusExists > 0) THEN
--      v_RETURN_NO := 1; --Done
--    ELSE
--      v_RETURN_NO := 0; --Not Done
--    END IF;
--
--    RETURN v_RETURN_NO;
--
--  EXCEPTION WHEN NO_DATA_FOUND THEN
--    BEGIN
--      v_RETURN_NO := 0; --Not Done
--      RETURN 0;
--    END;
return -1;
  END;
  
END PKG_WORKFLOW_RULES;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,362,5,'05_PKG_WORKFLOW_RULES.sql',sysdate,'v10 #763');

commit;