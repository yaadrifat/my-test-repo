set define off;

declare
v_exists NUMBER := 0;
v_nextVal NUMBER := 0;
v_PK_WORKFLOW NUMBER := 0;
v_PK_ACTIVITY NUMBER := 0;
v_PK_WORKFLOWACTIVITY NUMBER := 0;
begin
   SELECT count(*) INTO v_exists FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_CLOSURE';
	
   IF (v_exists = 0) THEN
       v_PK_WORKFLOW := SQ_WORKFLOW.NEXTVAL();

       Insert into WORKFLOW (PK_WORKFLOW, WORKFLOW_NAME, WORKFLOW_DESC, WORKFLOW_TYPE)
       values (v_PK_WORKFLOW,'Study Closure','Study Closure','STUDY_CLOSURE');
       COMMIT;
   ELSE
       SELECT PK_WORKFLOW INTO v_PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_CLOSURE';
   END IF;

   -------------------------------------------------------------------------------------------------------
   --Adding activity named "Study Completed" in the activity and workflow_activity table---
   -------------------------------------------------------------------------------------------------------
   SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = 'prmnt_cls';
	
   IF (v_exists = 0) THEN
       v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();

       Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC)
       values (v_PK_ACTIVITY,'prmnt_cls',(select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'prmnt_cls' and codelst_hide='N'));
       COMMIT;
   ELSE
       SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'prmnt_cls';
   END IF;
	
   IF (v_PK_ACTIVITY > 0) THEN
       --Workflow and activity association
       SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
	
       IF (v_exists = 0) THEN
           v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();

           Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY, WA_NAME, FK_WORKFLOWID, FK_ACTIVITYID, WA_SEQUENCE, WA_TERMINATEFLAG, WA_DISPLAYQUERY)
           values (v_PK_WORKFLOWACTIVITY, (select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'prmnt_cls' and codelst_hide='N'), v_PK_WORKFLOW, v_PK_ACTIVITY, 1, 0, 'SELECT PKG_WORKFLOW_RULES.F_study_complete(:1) FROM DUAL');
           COMMIT;
       END IF;
   END IF;
   
    SELECT PK_WORKFLOW INTO v_PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_INITIATION';
    
       -------------------------------------------------------------------------------------------------------
   --Adding activity named "CDA In Review" in the activity and workflow_activity table---
   -------------------------------------------------------------------------------------------------------
   SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = 'cdainrev';
	
   IF (v_exists = 0) THEN
       v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();

       Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC)
       values (v_PK_ACTIVITY,'cdainrev',(select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'cdainrev' and codelst_hide='N'));
       COMMIT;
   ELSE
       SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'cdainrev';
   END IF;
	
   IF (v_PK_ACTIVITY > 0) THEN
       --Workflow and activity association
       SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
	
       IF (v_exists = 0) THEN
           v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();

           Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY, WA_NAME, FK_WORKFLOWID, FK_ACTIVITYID, WA_SEQUENCE, WA_TERMINATEFLAG, WA_DISPLAYQUERY)
           values (v_PK_WORKFLOWACTIVITY, (select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'cdainrev' and codelst_hide='N'), v_PK_WORKFLOW, v_PK_ACTIVITY, 3, 0, 'SELECT PKG_WORKFLOW_RULES.F_CDA_InReview(:1) FROM DUAL');
           COMMIT;
       END IF;
   END IF;
   
      -------------------------------------------------------------------------------------------------------
   --Adding activity named "CDA Signoff" in the activity and workflow_activity table---
   -------------------------------------------------------------------------------------------------------
   SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = 'cdasignoff';
	
   IF (v_exists = 0) THEN
       v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();

       Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC)
       values (v_PK_ACTIVITY,'cdasignoff',(select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'cdasignoff' and codelst_hide='N'));
       COMMIT;
   ELSE
       SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'cdasignoff';
   END IF;
	
   IF (v_PK_ACTIVITY > 0) THEN
       --Workflow and activity association
       SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
	
       IF (v_exists = 0) THEN
           v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();

           Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY, WA_NAME, FK_WORKFLOWID, FK_ACTIVITYID, WA_SEQUENCE, WA_TERMINATEFLAG, WA_DISPLAYQUERY)
           values (v_PK_WORKFLOWACTIVITY, (select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'cdasignoff' and codelst_hide='N'), v_PK_WORKFLOW, v_PK_ACTIVITY, 4, 0, 'SELECT PKG_WORKFLOW_RULES.F_CDA_Signoff(:1) FROM DUAL');
           COMMIT;
       END IF;
   END IF;
   
   -------------------------------------------------------------------------------------------------------
   --Adding activity named "CA In Review" in the activity and workflow_activity table---
   -------------------------------------------------------------------------------------------------------
   SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = 'cainrev';
	
   IF (v_exists = 0) THEN
       v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();

       Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC)
       values (v_PK_ACTIVITY,'cainrev',(select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'cainrev' and codelst_hide='N'));
       COMMIT;
   ELSE
       SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'cainrev';
   END IF;
	
   IF (v_PK_ACTIVITY > 0) THEN
       --Workflow and activity association
       SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
	
       IF (v_exists = 0) THEN
           v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();

           Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY, WA_NAME, FK_WORKFLOWID, FK_ACTIVITYID, WA_SEQUENCE, WA_TERMINATEFLAG, WA_DISPLAYQUERY)
           values (v_PK_WORKFLOWACTIVITY, (select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'cainrev' and codelst_hide='N'), v_PK_WORKFLOW, v_PK_ACTIVITY, 5, 0, 'SELECT PKG_WORKFLOW_RULES.F_CA_InReview(:1) FROM DUAL');
           COMMIT;
       END IF;
   END IF;
   
   -------------------------------------------------------------------------------------------------------
   --Adding activity named "CA Signoff" in the activity and workflow_activity table---
   -------------------------------------------------------------------------------------------------------
   SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = 'casignoff';
	
   IF (v_exists = 0) THEN
       v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();

       Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC)
       values (v_PK_ACTIVITY,'casignoff',(select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'casignoff' and codelst_hide='N'));
       COMMIT;
   ELSE
       SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'casignoff';
   END IF;
	
   IF (v_PK_ACTIVITY > 0) THEN
       --Workflow and activity association
       SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
	
       IF (v_exists = 0) THEN
           v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();

           Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY, WA_NAME, FK_WORKFLOWID, FK_ACTIVITYID, WA_SEQUENCE, WA_TERMINATEFLAG, WA_DISPLAYQUERY)
           values (v_PK_WORKFLOWACTIVITY, (select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'casignoff' and codelst_hide='N'), v_PK_WORKFLOW, v_PK_ACTIVITY, 6, 0, 'SELECT PKG_WORKFLOW_RULES.F_CA_Signoff(:1) FROM DUAL');
           COMMIT;
       END IF;
   END IF;
   
   -------------------------------------------------------------------------------------------------------
   --Adding activity named "Budget In Review" in the activity and workflow_activity table---
   -------------------------------------------------------------------------------------------------------
   SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = 'bgtInreview';
	
   IF (v_exists = 0) THEN
       v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();

       Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC)
       values (v_PK_ACTIVITY,'bgtInreview',(select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'bgtInreview' and codelst_hide='N'));
       COMMIT;
   ELSE
       SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'bgtInreview';
   END IF;
	
   IF (v_PK_ACTIVITY > 0) THEN
       --Workflow and activity association
       SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
	
       IF (v_exists = 0) THEN
           v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();

           Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY, WA_NAME, FK_WORKFLOWID, FK_ACTIVITYID, WA_SEQUENCE, WA_TERMINATEFLAG, WA_DISPLAYQUERY)
           values (v_PK_WORKFLOWACTIVITY, (select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'bgtInreview' and codelst_hide='N'), v_PK_WORKFLOW, v_PK_ACTIVITY, 7, 0, 'SELECT PKG_WORKFLOW_RULES.F_Budget_InReview(:1) FROM DUAL');
           COMMIT;
       END IF;
   END IF;
   
   -------------------------------------------------------------------------------------------------------
   --Adding activity named "Budget Signoff" in the activity and workflow_activity table---
   -------------------------------------------------------------------------------------------------------
   SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = 'bgtsignoff';
	
   IF (v_exists = 0) THEN
       v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();

       Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC)
       values (v_PK_ACTIVITY,'bgtsignoff',(select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'bgtsignoff' and codelst_hide='N'));
       COMMIT;
   ELSE
       SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'bgtsignoff';
   END IF;
	
   IF (v_PK_ACTIVITY > 0) THEN
       --Workflow and activity association
       SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
	
       IF (v_exists = 0) THEN
           v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();

           Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY, WA_NAME, FK_WORKFLOWID, FK_ACTIVITYID, WA_SEQUENCE, WA_TERMINATEFLAG, WA_DISPLAYQUERY)
           values (v_PK_WORKFLOWACTIVITY, (select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'bgtsignoff' and codelst_hide='N'), v_PK_WORKFLOW, v_PK_ACTIVITY, 8, 0, 'SELECT PKG_WORKFLOW_RULES.F_Budget_Signoff(:1) FROM DUAL');
           COMMIT;
       END IF;
   END IF;
   
   -------------------------------------------------------------------------------------------------------
   --Adding activity named "IRB - Submission - Initial Submission" in the activity and workflow_activity table---
   -------------------------------------------------------------------------------------------------------
   SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = 'irb_init_subm';
	
   IF (v_exists = 0) THEN
       v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();

       Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC)
       values (v_PK_ACTIVITY,'irb_init_subm',(select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_init_subm' and codelst_hide='N'));
       COMMIT;
   ELSE
       SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'irb_init_subm';
   END IF;
	
   IF (v_PK_ACTIVITY > 0) THEN
       --Workflow and activity association
       SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
	
       IF (v_exists = 0) THEN
           v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();

           Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY, WA_NAME, FK_WORKFLOWID, FK_ACTIVITYID, WA_SEQUENCE, WA_TERMINATEFLAG, WA_DISPLAYQUERY)
           values (v_PK_WORKFLOWACTIVITY, (select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_init_subm' and codelst_hide='N'), v_PK_WORKFLOW, v_PK_ACTIVITY, 9, 0, 'SELECT PKG_WORKFLOW_RULES.F_IRB_InitSubmission(:1) FROM DUAL');
           COMMIT;
       END IF;
   END IF;
   
   SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'IRB_Review';
   
   if (v_PK_ACTIVITY > 0) then
      update workflow_activity set wa_sequence=10 where FK_ACTIVITYID = v_PK_ACTIVITY;
      commit;
   end if;
   
   SELECT PK_WORKFLOW INTO v_PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_ACTIVATION';
    
   -------------------------------------------------------------------------------------------------------
   --Adding activity named "Active/ Closed to Enrollment" in the activity and workflow_activity table---
   -------------------------------------------------------------------------------------------------------
   SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = 'active_cls';
	
   IF (v_exists = 0) THEN
       v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();

       Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC)
       values (v_PK_ACTIVITY,'active_cls',(select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'active_cls' and codelst_hide='N'));
       COMMIT;
   ELSE
       SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'active_cls';
   END IF;
	
   IF (v_PK_ACTIVITY > 0) THEN
       --Workflow and activity association
       SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
	
       IF (v_exists = 0) THEN
           v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();

           Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY, WA_NAME, FK_WORKFLOWID, FK_ACTIVITYID, WA_SEQUENCE, WA_TERMINATEFLAG, WA_DISPLAYQUERY)
           values (v_PK_WORKFLOWACTIVITY, (select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'active_cls' and codelst_hide='N'), v_PK_WORKFLOW, v_PK_ACTIVITY, 2, 0, 'SELECT PKG_WORKFLOW_RULES.F_Study_Enrollment(:1) FROM DUAL');
           COMMIT;
       END IF;
   END IF;
   
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,362,3,'03_Workflow_Creation_script.sql',sysdate,'v10 #763');

commit;