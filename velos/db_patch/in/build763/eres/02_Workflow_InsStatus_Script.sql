
--cdainrev

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'cdainrev';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'cdainrev', 'CDA In Review', 'default','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--cdasignoff

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'cdasignoff';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'cdasignoff', 'CDA Signoff', 'default','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--cainrev

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'cainrev';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'cainrev', 'CA In Review', 'default','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--casignoff

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'casignoff';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'casignoff', 'CA Signoff', 'default','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--bgtInreview

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'bgtInreview';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'bgtInreview', 'Budget In Review', 'default','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--bgtsignoff

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'bgtsignoff';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'bgtsignoff', 'Budget Signoff', 'default','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,362,2,'02_Workflow_InsStatus_Script.sql',sysdate,'v10 #763');

commit;