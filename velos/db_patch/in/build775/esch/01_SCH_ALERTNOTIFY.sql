set define off;
DECLARE
  v_column_exists number;

BEGIN
  Select count(*) into v_column_exists
  from user_tab_cols
  where TABLE_NAME = 'SCH_ALERTNOTIFY'
  AND COLUMN_NAME = 'ALNOT_MOBEVE';
   
  IF(v_column_exists != 0) THEN
    execute immediate 'alter table SCH_ALERTNOTIFY modify ALNOT_MOBEVE varchar2(4000 BYTE)';
  END IF;
  commit;
 
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,374,1,'01_SCH_ALERTNOTIFY.sql',sysdate,'v10 #775');

commit;