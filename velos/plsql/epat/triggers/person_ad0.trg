CREATE  OR REPLACE TRIGGER PERSON_AD0 AFTER DELETE ON PERSON        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
   usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
  oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
     oldUsr := 'New User' ;
      END;

  Audit_Trail.record_transaction (raid, 'PERSON', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CAUSE_OF_DEATH_OTHER', :OLD.CAUSE_OF_DEATH_OTHER);
       Audit_Trail.column_delete (raid, 'CREATED_ON', :OLD.CREATED_ON);
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'FK_ACCOUNT', :OLD.FK_ACCOUNT);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_BLOODGRP;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_BLOODGRP', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_CITIZEN;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_CITIZEN', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_EDU;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_EDU', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_EMPLOYMENT;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_EMPLOYMENT', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_ETHNICITY;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_ETHNICITY', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_GENDER;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_GENDER', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_MARITAL;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_MARITAL', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_NATIONALITY;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_NATIONALITY', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_PAT_DTH_CAUSE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_PAT_DTH_CAUSE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_PRILANG;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_PRILANG', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_PSTAT;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_PSTAT', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_RACE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_RACE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_RELIGION;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_RELIGION', codeList_desc);
       Audit_Trail.column_delete (raid, 'FK_CODLST_NTYPE', :OLD.FK_CODLST_NTYPE);
       Audit_Trail.column_delete (raid, 'FK_PERSON_MOTHER', :OLD.FK_PERSON_MOTHER);
       Audit_Trail.column_delete (raid, 'FK_SITE', :OLD.FK_SITE);
       Audit_Trail.column_delete (raid, 'FK_TIMEZONE', :OLD.FK_TIMEZONE);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', :OLD.LAST_MODIFIED_DATE);
       Audit_Trail.column_delete (raid, 'PAT_FACILITYID', :OLD.PAT_FACILITYID);
       Audit_Trail.column_delete (raid, 'PERSON_ADDRESS1', :OLD.PERSON_ADDRESS1);
       Audit_Trail.column_delete (raid, 'PERSON_ADDRESS2', :OLD.PERSON_ADDRESS2);
       Audit_Trail.column_delete (raid, 'PERSON_ADD_ETHNICITY', :OLD.PERSON_ADD_ETHNICITY);
       Audit_Trail.column_delete (raid, 'PERSON_ADD_RACE', :OLD.PERSON_ADD_RACE);
       Audit_Trail.column_delete (raid, 'PERSON_AKA', :OLD.PERSON_AKA);
       Audit_Trail.column_delete (raid, 'PERSON_ALTID', :OLD.PERSON_ALTID);
       Audit_Trail.column_delete (raid, 'PERSON_BIRTH_PLACE', :OLD.PERSON_BIRTH_PLACE);
       Audit_Trail.column_delete (raid, 'PERSON_BPHONE', :OLD.PERSON_BPHONE);
       Audit_Trail.column_delete (raid, 'PERSON_CITY', :OLD.PERSON_CITY);
       Audit_Trail.column_delete (raid, 'PERSON_CODE', :OLD.PERSON_CODE);
       Audit_Trail.column_delete (raid, 'PERSON_COUNTRY', :OLD.PERSON_COUNTRY);
       Audit_Trail.column_delete (raid, 'PERSON_COUNTY', :OLD.PERSON_COUNTY);
       Audit_Trail.column_delete (raid, 'PERSON_DEATHDT', :OLD.PERSON_DEATHDT);
       Audit_Trail.column_delete (raid, 'PERSON_DEGREE', :OLD.PERSON_DEGREE);
       Audit_Trail.column_delete (raid, 'PERSON_DOB', :OLD.PERSON_DOB);
       Audit_Trail.column_delete (raid, 'PERSON_DRIV_LIC', :OLD.PERSON_DRIV_LIC);
       Audit_Trail.column_delete (raid, 'PERSON_EMAIL', :OLD.PERSON_EMAIL);
       Audit_Trail.column_delete (raid, 'PERSON_ETHGRP', :OLD.PERSON_ETHGRP);
       Audit_Trail.column_delete (raid, 'PERSON_FNAME', :OLD.PERSON_FNAME);
       Audit_Trail.column_delete (raid, 'PERSON_HPHONE', :OLD.PERSON_HPHONE);
       Audit_Trail.column_delete (raid, 'PERSON_LNAME', :OLD.PERSON_LNAME);
       Audit_Trail.column_delete (raid, 'PERSON_MILVET', :OLD.PERSON_MILVET);
       Audit_Trail.column_delete (raid, 'PERSON_MNAME', :OLD.PERSON_MNAME);
       Audit_Trail.column_delete (raid, 'PERSON_MOTHER_NAME', :OLD.PERSON_MOTHER_NAME);
       Audit_Trail.column_delete (raid, 'PERSON_MULTI_BIRTH', :OLD.PERSON_MULTI_BIRTH);
       Audit_Trail.column_delete (raid, 'PERSON_NOTES', :OLD.PERSON_NOTES);
       Audit_Trail.column_delete (raid, 'PERSON_NOTES_CLOB', :OLD.PERSON_NOTES_CLOB);
       Audit_Trail.column_delete (raid, 'PERSON_ORGOTHER', :OLD.PERSON_ORGOTHER);
       Audit_Trail.column_delete (raid, 'PERSON_PHYOTHER', :OLD.PERSON_PHYOTHER);
       Audit_Trail.column_delete (raid, 'PERSON_PREFIX', :OLD.PERSON_PREFIX);
       Audit_Trail.column_delete (raid, 'PERSON_REGBY', :OLD.PERSON_REGBY);
       Audit_Trail.column_delete (raid, 'PERSON_REGDATE', :OLD.PERSON_REGDATE);
       Audit_Trail.column_delete (raid, 'PERSON_SPLACCESS', :OLD.PERSON_SPLACCESS);
       Audit_Trail.column_delete (raid, 'PERSON_SSN', :OLD.PERSON_SSN);
       Audit_Trail.column_delete (raid, 'PERSON_STATE', :OLD.PERSON_STATE);
       Audit_Trail.column_delete (raid, 'PERSON_SUFFIX', :OLD.PERSON_SUFFIX);
       Audit_Trail.column_delete (raid, 'PERSON_ZIP', :OLD.PERSON_ZIP);
       Audit_Trail.column_delete (raid, 'PK_PERSON', :OLD.PK_PERSON);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/
