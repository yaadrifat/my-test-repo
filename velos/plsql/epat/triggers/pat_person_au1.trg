CREATE OR REPLACE TRIGGER "PAT_PERSON_AU1" 
AFTER UPDATE
ON PERSON REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
/******************************************************************************
   NAME:       Vishal
   PURPOSE:    Update records in er_per after modify in person table

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        5/3/2005             1. Created this trigger.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     ER_PERSON_AU1
      Sysdate:         5/3/2005
      Date and Time:   5/3/2005, 5:13:01 PM, and 5/3/2005 5:13:01 PM
******************************************************************************/
BEGIN
   tmpVar := 0;

   UPDATE ER_PER SET PER_CODE=:NEW.PERSON_CODE, FK_ACCOUNT=:NEW.FK_ACCOUNT, FK_SITE=:NEW.FK_SITE,
     LAST_MODIFIED_BY=:NEW.LAST_MODIFIED_BY,IP_ADD=:NEW.IP_ADD,LAST_MODIFIED_DATE=:NEW.LAST_MODIFIED_DATE,
	FK_TIMEZONE=:NEW.FK_TIMEZONE
	WHERE pk_per=:OLD.pk_person	;

   EXCEPTION
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;
END ;
/


