CREATE OR REPLACE PROCEDURE        "SP_EDITPROEVENT" (
p_protocol_id IN NUMBER,
p_fromDisp IN NUMBER,
p_toDisp IN NUMBER
)
AS
/*
This sets the event flag to 1 for all old events during the edit protocol
calender process
*/
v_from NUMBER;
  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SP_EDITPROEVENT', pLEVEL  => Plog.LFATAL);

BEGIN

v_from := p_fromDisp;

plog.DEBUG(pctx, 'v_from' || v_from);

IF v_from = 1 THEN

UPDATE  EVENT_DEF
   SET event_flag = 1
 WHERE chain_id = p_protocol_id
   AND event_type='A'
   AND displacement <= p_toDisp AND displacement  <> 0;


ELSE

UPDATE  EVENT_DEF
   SET event_flag = 1
 WHERE chain_id = p_protocol_id
   AND event_type='A'
   AND displacement BETWEEN  p_fromDisp AND p_toDisp;
END IF;
COMMIT;
END;
/


CREATE SYNONYM ERES.SP_EDITPROEVENT FOR SP_EDITPROEVENT;


CREATE SYNONYM EPAT.SP_EDITPROEVENT FOR SP_EDITPROEVENT;


GRANT EXECUTE, DEBUG ON SP_EDITPROEVENT TO EPAT;

GRANT EXECUTE, DEBUG ON SP_EDITPROEVENT TO ERES;

