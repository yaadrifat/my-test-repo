create or replace
PROCEDURE      "SP_EVENTNOTIFY" (
   P_EVENT          NUMBER,
   P_EVENTSTAT      NUMBER,
   P_CREATOR        NUMBER,
   P_IPADD          VARCHAR2,
   P_OLDEVENTSTAT   NUMBER
)
AS
/*
This procedure will move the records to despatcher table to send mails in case of change in event status
P_EVENT is event id
P_EVENTNAME is event NAME
P_EVENTSTAT is status of the event when changed
P_PATPROT is the patient enrolled to Protocol
P_CREATOR is for auditing that who changed event status
P_IPADD is the IP address from where the Event status was changed
Modified By         Date      Remarks
Sonia               12 Dec    To move the records to despatcher for further action to be taken
Sonia               28 Dec    To get visit from sch_events1 instead of calculating dynamically
Sonia ABrol  03/29/05 - to get visit name from the new table; to restrict sending the email to user's who do not have access to patient's site
*/
   V_EVENTFLAG       NUMBER;
   V_POS1            NUMBER         := 0;
   V_USERS           VARCHAR2 (1000);
   V_MSG             VARCHAR2 (4000);
   V_STR             VARCHAR2 (4000);
   V_PARAMS          VARCHAR2 (4000);
   V_CNT             NUMBER;

   V_EVENTNAME       VARCHAR2 (4000);--KM-15Sep09
   V_PATCODE         VARCHAR2 (25);
   V_FPARAM          VARCHAR2 (4000); --KM
   --V_FPARAM        VARCHAR2 (200);
   V_SCHDATE         VARCHAR2 (12) ;
   V_VISIT           NUMBER;
   V_PATPROT         NUMBER     ;
   V_MSGTEMPLATE     VARCHAR2 (4000);
   V_OLDEVSTATNAME   VARCHAR2 (300);
   V_NEWEVSTATNAME   VARCHAR2 (300);
   V_EVENTNAME_TRUNC VARCHAR2 (4000);

   v_pat_site NUMBER;
   v_pk_per NUMBER;
   v_study NUMBER;
   v_right NUMBER := 0;
   v_visit_name VARCHAR2(50);
   v_fk_protocol number;
     pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SP_EVENTNOTIFY', pLEVEL  => Plog.LFATAL);

BEGIN

       SELECT fk_patprot, ev.description, TO_CHAR(ACTUAL_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT), visit, visit_name, fk_protocol
     INTO v_patprot, v_eventname ,v_schdate ,V_VISIT,v_visit_name, v_fk_protocol
     FROM SCH_EVENTS1 ev,SCH_PROTOCOL_VISIT
    WHERE event_id = LPAD(p_event,10,0)  AND pk_protocol_visit = fk_visit ;

    BEGIN
    SELECT fk_study ,fk_per
    INTO v_study,v_pk_per
    FROM er_patprot WHERE pk_patprot = v_patprot;

    EXCEPTION
         WHEN NO_DATA_FOUND THEN
            --V_EVENTFLAG := 0;
            RETURN;
      END;


      SELECT fk_site
    INTO v_pat_site
    FROM er_per WHERE pk_per =v_pk_per;




   SELECT Pkg_Common.SCH_GETPATCODE (V_PATPROT)
     INTO V_PATCODE
     FROM DUAL;   -- v_patcode is param 1

   --V_VISIT := SCH_GETVISIT (V_PATPROT, lpad (P_EVENT, 10, 0));   -- get visit

   SELECT CODELST_DESC
     INTO V_OLDEVSTATNAME
     FROM SCH_CODELST
    WHERE PK_CODELST = P_OLDEVENTSTAT
      AND CODELST_TYPE = 'eventstatus';

   SELECT CODELST_DESC
     INTO V_NEWEVSTATNAME
     FROM SCH_CODELST
    WHERE PK_CODELST = P_EVENTSTAT
      AND CODELST_TYPE = 'eventstatus';

   V_MSGTEMPLATE := Pkg_Common.SCH_GETMAILMSG ('eventstat');
   --KM-16Sep09
   v_eventname_trunc := SUBSTR(v_eventname,1,500)  || '...';
   --'B_Value' here stands for Blank value or null
   V_FPARAM := V_PATCODE ||
               '~' ||
               NVL(TO_CHAR(v_visit_name),'Not Known') ||
               '~' ||
               v_eventname_trunc ||
               '~' ||
               NVL(V_SCHDATE,'B_Value') ||
               '~' ||
               V_OLDEVSTATNAME ||
               '~' ||
               V_NEWEVSTATNAME;
      V_MSG := Pkg_Common.SCH_GETMAIL (V_MSGTEMPLATE, V_FPARAM);

plog.debug(pctx,'gotmail');

   FOR I IN 1 .. 2
   LOOP
      V_POS1 := 0;
      V_USERS := NULL;
      V_EVENTFLAG := 0;


      IF I = 2 THEN
         V_EVENTNAME := 'All';
      END IF;

      BEGIN
         SELECT ALNOT_FLAG, ALNOT_USERS
           INTO V_EVENTFLAG, V_USERS
           FROM SCH_ALERTNOTIFY
          WHERE FK_study = v_study and  alnot_globalflag = 'G' and alnot_type = 'N' and fk_patprot is null
             and FK_CODELST_AN = P_EVENTSTAT
            AND ALNOT_MOBEVE = V_EVENTNAME and fk_protocol = v_fk_protocol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_EVENTFLAG := 0;
      END;

    plog.debug(pctx,'V_EVENTFLAG' || V_EVENTFLAG);


      IF V_EVENTFLAG = 1 THEN
         /* Get the message */

         /*select msgtxt_short
           into v_msg
           from sch_msgtxt
          where msgtxt_type = 'eventstat' ; */

         /*Move the records to despatcher to send email to the users*/
         V_STR := V_USERS || ',';
         V_PARAMS := V_USERS || ',';


         LOOP
            V_CNT := V_CNT + 1;
            V_POS1 := INSTR (V_STR, ',');
            V_PARAMS := SUBSTR (V_STR, 1, V_POS1 - 1);
            EXIT WHEN V_PARAMS IS NULL;

            --check for email recepient user's rights on patient's organization

            BEGIN
                        SELECT      pkg_user.f_chk_right_for_studysite(v_study ,v_params,v_pat_site )
                        INTO v_right FROM dual;
            EXCEPTION WHEN OTHERS THEN
                        v_right := 0;
                        plog.debug(pctx,'v_right - exce' || v_right);
            END;

            plog.debug(pctx,'V_right' || v_right);

            --KM-#3891
              IF ((v_right >  0) and (p_eventstat <> p_oldeventstat) ) THEN
                        INSERT INTO SCH_DISPATCHMSG
                        (     PK_MSG,    MSG_SENDON,    MSG_STATUS,   MSG_TYPE,   FK_SCHEVENT,      MSG_TEXT,      FK_PAT,
                                       CREATOR,    IP_ADD,   FK_PATPROT      )
                 VALUES(    SCH_DISPATCHMSG_SEQ1.NEXTVAL,  SYSDATE,   0,  'U',P_EVENT,  V_MSG,   V_PARAMS,
                    P_CREATOR,   P_IPADD,  v_patprot   );

                    plog.debug(pctx,'insrted' );
            END IF;

            V_STR := SUBSTR (V_STR, V_POS1 + 1);
         END LOOP;
      END IF;
   END LOOP;
commit;

END;

/


CREATE OR REPLACE SYNONYM ERES.SP_EVENTNOTIFY FOR SP_EVENTNOTIFY;


CREATE OR REPLACE SYNONYM EPAT.SP_EVENTNOTIFY FOR SP_EVENTNOTIFY;


GRANT EXECUTE, DEBUG ON SP_EVENTNOTIFY TO EPAT;

GRANT EXECUTE, DEBUG ON SP_EVENTNOTIFY TO ERES;

