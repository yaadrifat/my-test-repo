CREATE OR REPLACE PROCEDURE        "GETCONFLICTSEXTID" ( PatID char, StDate date, EnDate date
)
as
event_id char(10);
description char(10);
start_date_time date;
end_date_time date;
begin
  select event_id,description,start_date_time,end_date_time into event_id,description ,start_date_time ,end_date_time from sch_events1 where patient_id=patID and(not((end_date_time<=StDate) or(start_date_time>=EnDate)));
end;
/


CREATE SYNONYM ERES.GETCONFLICTSEXTID FOR GETCONFLICTSEXTID;


CREATE SYNONYM EPAT.GETCONFLICTSEXTID FOR GETCONFLICTSEXTID;


GRANT EXECUTE, DEBUG ON GETCONFLICTSEXTID TO EPAT;

GRANT EXECUTE, DEBUG ON GETCONFLICTSEXTID TO ERES;

