CREATE OR REPLACE FUNCTION        "LST_USR" (event NUMBER ) RETURN VARCHAR IS
CURSOR cur_usr
IS
 SELECT usr_lastname ||' ' || usr_firstname usr_name
   FROM sch_eventusr, er_user
  WHERE sch_eventusr.fk_event = event
    AND EVENTUSR_TYPE = 'U'
    AND EVENTUSR = pk_user ;

CURSOR cur_role
IS
 SELECT codelst_desc
   FROM sch_eventusr, er_codelst
  WHERE sch_eventusr.fk_event = event
    AND EVENTUSR_TYPE = 'R'
    AND EVENTUSR = pk_codelst
    AND codelst_type = 'role'  ;

usr_lst VARCHAR2(4000) ;
v_chkcomma VARCHAR2(1);
BEGIN
 FOR l_rec IN cur_usr LOOP
  usr_lst :=   l_rec.usr_name || ', '|| usr_lst ;
 END LOOP ;
  usr_lst := SUBSTR(trim(usr_lst),1,LENGTH(trim(usr_lst))-1) ;
  FOR l_rec1 IN cur_role LOOP
  usr_lst :=   l_rec1.codelst_desc || ', '|| usr_lst ;
 END LOOP ;

 v_chkcomma :=  SUBSTR(trim(usr_lst),LENGTH(trim(usr_lst)),LENGTH(trim(usr_lst))) ;
 --if last character is , remove it
 IF (v_chkcomma = ',') THEN
   usr_lst := SUBSTR(trim(usr_lst),1,LENGTH(trim(usr_lst))-1) ;
 END IF;
 DBMS_OUTPUT.PUT_LINE (SUBSTR(usr_lst,1,LENGTH(usr_lst)-1) ) ;
RETURN usr_lst ;
END ;
/


CREATE SYNONYM ERES.LST_USR FOR LST_USR;


CREATE SYNONYM EPAT.LST_USR FOR LST_USR;


GRANT EXECUTE, DEBUG ON LST_USR TO EPAT;

GRANT EXECUTE, DEBUG ON LST_USR TO ERES;

