CREATE  OR REPLACE TRIGGER SCH_CRF_AD0 AFTER DELETE ON SCH_CRF        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'SCH_CRF', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'CRF_CURSTAT', :OLD.CRF_CURSTAT);
       Audit_Trail.column_delete (raid, 'CRF_DELFLAG', :OLD.CRF_DELFLAG);
       Audit_Trail.column_delete (raid, 'CRF_FLAG', :OLD.CRF_FLAG);
       Audit_Trail.column_delete (raid, 'CRF_NAME', :OLD.CRF_NAME);
       Audit_Trail.column_delete (raid, 'CRF_NUMBER', :OLD.CRF_NUMBER);
       Audit_Trail.column_delete (raid, 'CRF_ORIGLIBID', :OLD.CRF_ORIGLIBID);
       Audit_Trail.column_delete (raid, 'FK_EVENTS1', :OLD.FK_EVENTS1);
       Audit_Trail.column_delete (raid, 'FK_LF', :OLD.FK_LF);
       Audit_Trail.column_delete (raid, 'FK_PATPROT', :OLD.FK_PATPROT);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'LF_ENTRYCHAR', :OLD.LF_ENTRYCHAR);
       Audit_Trail.column_delete (raid, 'PK_CRF', :OLD.PK_CRF);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/
