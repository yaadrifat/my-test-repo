CREATE OR REPLACE TRIGGER "SCH_ADVINFO_AI0" 
AFTER INSERT
ON SCH_ADVINFO REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
  out_type VARCHAR2(15);
  v_death NUMBER;
  v_death_date DATE;
  v_per NUMBER;
BEGIN
 BEGIN
   SELECT trim(codelst_subtyp)
   INTO out_type
   FROM SCH_CODELST
   WHERE codelst_type = 'outcome' AND
   pk_codelst = :NEW.FK_CODELST_INFO;
 EXCEPTION WHEN NO_DATA_FOUND THEN
	  out_type  := '' ;
 END ;
 IF out_type = 'al_death' AND :NEW.advinfo_value = 1 THEN
	-- get pk codelst for death status code
	SELECT pk_codelst
	INTO v_death
	FROM ER_CODELST
	WHERE trim(codelst_subtyp) = 'dead' AND trim(codelst_type)='patient_status';
	--get the death date if entered; update patient status and death date
	SELECT ae_outdate, fk_per
	INTO  v_death_date, v_per
	FROM SCH_ADVERSEVE
	WHERE pk_adveve = :NEW.fk_adverse;
	-- update patient data;
	UPDATE person
	SET person_deathdt = v_death_date, fk_codelst_pstat = v_death
	WHERE pk_person = v_per;
	Pkg_Alnot.sp_deathnotify (:NEW.FK_ADVERSE , :NEW.FK_CODELST_INFO   ,
        	                  :NEW.CREATOR ,   :NEW.IP_ADD) ;


  -- call study specific death notification - sonia abrol, 08/29/06
    Pkg_Alnot.sp_deathnotify_for_studylevel (:NEW.FK_ADVERSE ,
        	                  :NEW.CREATOR ,   :NEW.IP_ADD);

  END IF;
END;
/


