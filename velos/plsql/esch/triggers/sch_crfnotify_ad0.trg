CREATE  OR REPLACE TRIGGER SCH_CRFNOTIFY_AD0 AFTER DELETE ON SCH_CRFNOTIFY        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
   usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
    oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'SCH_CRFNOTIFY', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON', :OLD.CREATED_ON);
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'CRFNOTIFYCODELSTNOTSTATID', :OLD.CRFNOTIFYCODELSTNOTSTATID);
       Audit_Trail.column_delete (raid, 'CRFNOT_GLOBALFLAG', :OLD.CRFNOT_GLOBALFLAG);
       Audit_Trail.column_delete (raid, 'CRFNOT_NOTES', :OLD.CRFNOT_NOTES);
       Audit_Trail.column_delete (raid, 'CRFNOT_USERSTO', :OLD.CRFNOT_USERSTO);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from SCH_CODELST where PK_CODELST=:OLD.FK_CODELST_NOTSTAT;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_NOTSTAT', codeList_desc);
       Audit_Trail.column_delete (raid, 'FK_CRF', :OLD.FK_CRF);
       Audit_Trail.column_delete (raid, 'FK_PATPROT', :OLD.FK_PATPROT);
       Audit_Trail.column_delete (raid, 'FK_PROTOCOL', :OLD.FK_PROTOCOL);
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', :OLD.LAST_MODIFIED_DATE);
       Audit_Trail.column_delete (raid, 'PK_CRFNOT', :OLD.PK_CRFNOT);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/