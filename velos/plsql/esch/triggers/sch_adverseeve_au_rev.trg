CREATE OR REPLACE TRIGGER "SCH_ADVERSEEVE_AU_REV" 
AFTER UPDATE
ON SCH_ADVERSEVE REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
/* **********************************
   **
   ** Author: Sonia Sahni 05/04/2004
   ** for reverse A2A
   ** Insert a record in er_rev_pendingdata
   *************************************
*/
v_key NUMBER;
v_pk NUMBER;
v_tabname VARCHAR2(50);
v_per NUMBER;
v_account NUMBER;
v_site_code VARCHAR2(255);
v_sponsor_study NUMBER;
v_err VARCHAR2(4000);
v_module_name VARCHAR2(20);
v_module_commmon VARCHAR2(20);
v_sponsor_account NUMBER;
v_site_study NUMBER;
v_adv_formstatus_pk NUMBER := -1;
v_formstatus_subtype_setting VARCHAR2(20);

BEGIN
 -- get study information
v_site_study := :NEW.fk_study;
IF v_site_study IS NOT NULL THEN
  -- check if its a networked study and data needs to be sent back
  PKG_IMPEX_REVERSE.SP_GET_STUDYSPONSORINFO(v_site_study, v_sponsor_study , v_site_code, v_sponsor_account, v_err);
  IF LENGTH(trim(v_site_code)) >  0 AND v_sponsor_study > 0 THEN

  -- get the adverse event form status setting
  BEGIN
   SELECT sys.XMLTYPE.EXTRACT(EXPORT_SETTINGS,'/modules/adverse/formStatusSubType/text()').getStringVal()
   INTO v_formstatus_subtype_setting
   FROM er_rev_studysponsor
   WHERE  FK_SITESTUDY   = v_site_study AND SITE_CODE = v_site_code;

   -- check adverse event from status

   SELECT pkg_impex.getSchCodePk (v_formstatus_subtype_setting, 'fillformstat')
   INTO v_adv_formstatus_pk
   FROM dual;

  EXCEPTION WHEN OTHERS THEN
  	v_formstatus_subtype_setting := '';
	v_adv_formstatus_pk := -1;
  END ;


  IF  v_adv_formstatus_pk <= 0 OR v_adv_formstatus_pk = :NEW.FORM_STATUS THEN

  --insert data for patient adverse event
   v_tabname := 'sch_adverseve';
   v_module_name := 'pat_adverse';
   v_module_commmon := 'pat_common';

   v_pk := :NEW.PK_ADVEVE ;
   SELECT SEQ_ER_REV_PENDINGDATA.NEXTVAL
   INTO v_key FROM dual;
   INSERT INTO er_rev_pendingdata (pk_rp, rp_tablename, rp_tablepk,  RP_SITECODE,FK_STUDY,RP_MODULE)
   VALUES (v_key ,v_tabname,v_pk,v_site_code,v_site_study,v_module_name);

	   --insert data for users
	 SELECT SEQ_ER_REV_PENDINGDATA.NEXTVAL
	 INTO v_key FROM dual;
	 v_tabname := 'er_user';
	 v_pk := :NEW.AE_ENTERBY;

	 INSERT INTO er_rev_pendingdata (pk_rp, rp_tablename, rp_tablepk , RP_SITECODE, FK_STUDY,RP_MODULE)
	 VALUES (v_key ,v_tabname,v_pk,v_site_code , v_site_study, v_module_commmon);

	    END IF; -- for adverse event from status
  END IF; -- for v_site_code
 END IF; --for fk_study
END;
/


