CREATE  OR REPLACE TRIGGER SCH_ALERTNOTIFY_BI0 BEFORE INSERT ON SCH_ALERTNOTIFY        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'SCH_ALERTNOTIFY', erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'ALNOT_FLAG', :NEW.ALNOT_FLAG);
       Audit_Trail.column_insert (raid, 'ALNOT_GLOBALFLAG', :NEW.ALNOT_GLOBALFLAG);
       Audit_Trail.column_insert (raid, 'ALNOT_MOBEVE', :NEW.ALNOT_MOBEVE);
       Audit_Trail.column_insert (raid, 'ALNOT_TYPE', :NEW.ALNOT_TYPE);
       Audit_Trail.column_insert (raid, 'ALNOT_USERS', :NEW.ALNOT_USERS);
       Audit_Trail.column_insert (raid, 'CREATED_ON', :NEW.CREATED_ON);
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from  SCH_CODELST where PK_CODELST=:NEW.FK_CODELST_AN;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_AN', codeList_desc);
       Audit_Trail.column_insert (raid, 'FK_PATPROT', :NEW.FK_PATPROT);
       Audit_Trail.column_insert (raid, 'FK_PROTOCOL', :NEW.FK_PROTOCOL);
       Audit_Trail.column_insert (raid, 'FK_STUDY', :NEW.FK_STUDY);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', :NEW.LAST_MODIFIED_DATE);
       Audit_Trail.column_insert (raid, 'PK_ALNOT', :NEW.PK_ALNOT);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/