create or replace
TRIGGER "SCH_PROTOCOL_VISIT_BIO"
BEFORE INSERT
ON SCH_PROTOCOL_VISIT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  USR VARCHAR2(70);
  v_new_creator NUMBER;
  insert_data CLOB;
BEGIN
 BEGIN
   v_new_creator := :NEW.creator;
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' ||usr_firstname
   INTO usr FROM er_user WHERE pk_user = v_new_creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
  USR := 'New User' ;
 END ;
  SELECT TRUNC(seq_rid.NEXTVAL) INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
    insert_data:= :NEW.PK_PROTOCOL_VISIT||'|'||:NEW.FK_PROTOCOL||'|'||:NEW.VISIT_NO||'|'||
          :NEW.VISIT_NAME||'|'||:NEW.DESCRIPTION||'|'|| :NEW.DISPLACEMENT||'|'||
          :NEW.NUM_MONTHS||'|'|| :NEW.NUM_WEEKS||'|'|| :NEW.NUM_DAYS||'|'|| :NEW.INSERT_AFTER||'|'||
          :NEW.RID||'|'||  :NEW.CREATOR||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD ||'|'|| :NEW.INSERT_AFTER_INTERVAL||'|'||
	  :NEW.INSERT_AFTER_INTERVAL_UNIT||'|'|| :NEW.PROTOCOL_TYPE||'|'|| :NEW.VISIT_TYPE ||'|'|| :NEW.OFFLINE_FLAG ||'|'|| :NEW.HIDE_FLAG
    ||'|'|| :NEW.NO_INTERVAL_FLAG ||'|'|| :NEW.WIN_BEFORE_NUMBER ||'|'|| :NEW.WIN_BEFORE_UNIT ||'|'|| :NEW.WIN_AFTER_NUMBER ||'|'|| :NEW.WIN_AFTER_UNIT ;


     INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
  audit_trail.record_transaction(raid, 'SCH_PROTOCOL_VISIT', erid, 'I', USR );
END;
/