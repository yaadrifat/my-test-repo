CREATE  OR REPLACE TRIGGER SCH_PROTOCOL_VISIT_BI0 BEFORE INSERT ON SCH_PROTOCOL_VISIT        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;

 
    BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
  Audit_Trail.record_transaction (raid, 'SCH_PROTOCOL_VISIT', erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON', :NEW.CREATED_ON);
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'DESCRIPTION', :NEW.DESCRIPTION);
       Audit_Trail.column_insert (raid, 'DISPLACEMENT', :NEW.DISPLACEMENT);
       Audit_Trail.column_insert (raid, 'FK_PROTOCOL', :NEW.FK_PROTOCOL);
       Audit_Trail.column_insert (raid, 'HIDE_FLAG', :NEW.HIDE_FLAG);
       Audit_Trail.column_insert (raid, 'INSERT_AFTER', :NEW.INSERT_AFTER);
       Audit_Trail.column_insert (raid, 'INSERT_AFTER_INTERVAL', :NEW.INSERT_AFTER_INTERVAL);
       Audit_Trail.column_insert (raid, 'INSERT_AFTER_INTERVAL_UNIT', :NEW.INSERT_AFTER_INTERVAL_UNIT);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', :NEW.LAST_MODIFIED_DATE);
       Audit_Trail.column_insert (raid, 'NO_INTERVAL_FLAG', :NEW.NO_INTERVAL_FLAG);
       Audit_Trail.column_insert (raid, 'NUM_DAYS', :NEW.NUM_DAYS);
       Audit_Trail.column_insert (raid, 'NUM_MONTHS', :NEW.NUM_MONTHS);
       Audit_Trail.column_insert (raid, 'NUM_WEEKS', :NEW.NUM_WEEKS);
       Audit_Trail.column_insert (raid, 'OFFLINE_FLAG', :NEW.OFFLINE_FLAG);
       Audit_Trail.column_insert (raid, 'PK_PROTOCOL_VISIT', :NEW.PK_PROTOCOL_VISIT);
       Audit_Trail.column_insert (raid, 'PROTOCOL_TYPE', :NEW.PROTOCOL_TYPE);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'VISIT_NAME', :NEW.VISIT_NAME);
       Audit_Trail.column_insert (raid, 'VISIT_NO', :NEW.VISIT_NO);
       Audit_Trail.column_insert (raid, 'VISIT_TYPE', :NEW.VISIT_TYPE);
       Audit_Trail.column_insert (raid, 'WIN_AFTER_NUMBER', :NEW.WIN_AFTER_NUMBER);
       Audit_Trail.column_insert (raid, 'WIN_AFTER_UNIT', :NEW.WIN_AFTER_UNIT);
       Audit_Trail.column_insert (raid, 'WIN_BEFORE_NUMBER', :NEW.WIN_BEFORE_NUMBER);
       Audit_Trail.column_insert (raid, 'WIN_BEFORE_UNIT', :NEW.WIN_BEFORE_UNIT);
COMMIT;
END;
/