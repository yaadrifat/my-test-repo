CREATE  OR REPLACE TRIGGER SCH_LINEITEM_BI0 BEFORE INSERT ON SCH_LINEITEM        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'SCH_LINEITEM', erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON', :NEW.CREATED_ON);
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'FK_BGTSECTION', :NEW.FK_BGTSECTION);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from  SCH_CODELST where PK_CODELST=:NEW.FK_CODELST_CATEGORY;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_CATEGORY', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from  SCH_CODELST where PK_CODELST=:NEW.FK_CODELST_COST_TYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_COST_TYPE', codeList_desc);
       Audit_Trail.column_insert (raid, 'FK_EVENT', :NEW.FK_EVENT);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', :NEW.LAST_MODIFIED_DATE);
       Audit_Trail.column_insert (raid, 'LINEITEM_APPLYINDIRECTS', :NEW.LINEITEM_APPLYINDIRECTS);
       Audit_Trail.column_insert (raid, 'LINEITEM_APPLYINFUTURE', :NEW.LINEITEM_APPLYINFUTURE);
       Audit_Trail.column_insert (raid, 'LINEITEM_CDM', :NEW.LINEITEM_CDM);
       Audit_Trail.column_insert (raid, 'LINEITEM_CLINICNOFUNIT', :NEW.LINEITEM_CLINICNOFUNIT);
       Audit_Trail.column_insert (raid, 'LINEITEM_CPTCODE', :NEW.LINEITEM_CPTCODE);
       Audit_Trail.column_insert (raid, 'LINEITEM_DELFLAG', :NEW.LINEITEM_DELFLAG);
       Audit_Trail.column_insert (raid, 'LINEITEM_DESC', :NEW.LINEITEM_DESC);
       Audit_Trail.column_insert (raid, 'LINEITEM_INCOSTDISC', :NEW.LINEITEM_INCOSTDISC);
       Audit_Trail.column_insert (raid, 'LINEITEM_INPERSEC', :NEW.LINEITEM_INPERSEC);
       Audit_Trail.column_insert (raid, 'LINEITEM_INVCOST', :NEW.LINEITEM_INVCOST);
       Audit_Trail.column_insert (raid, 'LINEITEM_NAME', :NEW.LINEITEM_NAME);
       Audit_Trail.column_insert (raid, 'LINEITEM_NOTES', :NEW.LINEITEM_NOTES);
       Audit_Trail.column_insert (raid, 'LINEITEM_OTHERCOST', :NEW.LINEITEM_OTHERCOST);
       Audit_Trail.column_insert (raid, 'LINEITEM_PARENTID', :NEW.LINEITEM_PARENTID);
       Audit_Trail.column_insert (raid, 'LINEITEM_REPEAT', :NEW.LINEITEM_REPEAT);
       Audit_Trail.column_insert (raid, 'LINEITEM_RESCOST', :NEW.LINEITEM_RESCOST);
       Audit_Trail.column_insert (raid, 'LINEITEM_SEQ', :NEW.LINEITEM_SEQ);
       Audit_Trail.column_insert (raid, 'LINEITEM_SPONSORAMOUNT', :NEW.LINEITEM_SPONSORAMOUNT);
       Audit_Trail.column_insert (raid, 'LINEITEM_SPONSORUNIT', :NEW.LINEITEM_SPONSORUNIT);
       Audit_Trail.column_insert (raid, 'LINEITEM_STDCARECOST', :NEW.LINEITEM_STDCARECOST);
       Audit_Trail.column_insert (raid, 'LINEITEM_TMID', :NEW.LINEITEM_TMID);
       Audit_Trail.column_insert (raid, 'LINEITEM_TOTALCOST', :NEW.LINEITEM_TOTALCOST);
       Audit_Trail.column_insert (raid, 'LINEITEM_VARIANCE', :NEW.LINEITEM_VARIANCE);
       Audit_Trail.column_insert (raid, 'PK_LINEITEM', :NEW.PK_LINEITEM);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'SUBCOST_ITEM_FLAG', :NEW.SUBCOST_ITEM_FLAG);
       Audit_Trail.column_insert (raid, 'LINEITEM_APPLYPATIENTCOUNT', :NEW.LINEITEM_APPLYPATIENTCOUNT);
COMMIT;
END;
/