CREATE  OR REPLACE TRIGGER SCH_CRFSTAT_AD0 AFTER DELETE ON SCH_CRFSTAT        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
   usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
    oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'SCH_CRFSTAT', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON', :OLD.CREATED_ON);
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'CRFSTAT_ENTERBY', :OLD.CRFSTAT_ENTERBY);
       Audit_Trail.column_delete (raid, 'CRFSTAT_REVIEWBY', :OLD.CRFSTAT_REVIEWBY);
       Audit_Trail.column_delete (raid, 'CRFSTAT_REVIEWON', :OLD.CRFSTAT_REVIEWON);
       Audit_Trail.column_delete (raid, 'CRFSTAT_SENTBY', :OLD.CRFSTAT_SENTBY);
       Audit_Trail.column_delete (raid, 'CRFSTAT_SENTFLAG', :OLD.CRFSTAT_SENTFLAG);
       Audit_Trail.column_delete (raid, 'CRFSTAT_SENTON', :OLD.CRFSTAT_SENTON);
       Audit_Trail.column_delete (raid, 'CRFSTAT_SENTTO', :OLD.CRFSTAT_SENTTO);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from SCH_CODELST where PK_CODELST=:OLD.FK_CODELST_CRFSTAT;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_CRFSTAT', codeList_desc);
       Audit_Trail.column_delete (raid, 'FK_CRF', :OLD.FK_CRF);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', :OLD.LAST_MODIFIED_DATE);
       Audit_Trail.column_delete (raid, 'PK_CRFSTAT', :OLD.PK_CRFSTAT);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/