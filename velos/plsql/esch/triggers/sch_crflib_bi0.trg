CREATE  OR REPLACE TRIGGER SCH_CRFLIB_BI0 BEFORE INSERT ON SCH_CRFLIB        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'SCH_CRFLIB', erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'CRFLIB_FLAG', :NEW.CRFLIB_FLAG);
       Audit_Trail.column_insert (raid, 'CRFLIB_FORMFLAG', :NEW.CRFLIB_FORMFLAG);
       Audit_Trail.column_insert (raid, 'CRFLIB_NAME', :NEW.CRFLIB_NAME);
       Audit_Trail.column_insert (raid, 'CRFLIB_NUMBER', :NEW.CRFLIB_NUMBER);
       Audit_Trail.column_insert (raid, 'FK_EVENTS', :NEW.FK_EVENTS);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_CRFLIB', :NEW.PK_CRFLIB);
       Audit_Trail.column_insert (raid, 'PROPAGATE_FROM', :NEW.PROPAGATE_FROM);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/
