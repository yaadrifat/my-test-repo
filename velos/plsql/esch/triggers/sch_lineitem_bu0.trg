CREATE OR REPLACE TRIGGER "SCH_LINEITEM_BU0" BEFORE UPDATE ON SCH_LINEITEM
FOR EACH ROW
WHEN (
new.last_modified_by is not null
      )
BEGIN
  :NEW.last_modified_date := SYSDATE ;
END;
/


