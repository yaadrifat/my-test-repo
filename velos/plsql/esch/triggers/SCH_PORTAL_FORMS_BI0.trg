CREATE  OR REPLACE TRIGGER SCH_PORTAL_FORMS_BI0 BEFORE INSERT ON SCH_PORTAL_FORMS        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'SCH_PORTAL_FORMS', erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON', :NEW.CREATED_ON);
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'FK_CALENDAR', :NEW.FK_CALENDAR);
       Audit_Trail.column_insert (raid, 'FK_EVENT', :NEW.FK_EVENT);
       Audit_Trail.column_insert (raid, 'FK_FORM', :NEW.FK_FORM);
       Audit_Trail.column_insert (raid, 'FK_PORTAL', :NEW.FK_PORTAL);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', :NEW.LAST_MODIFIED_DATE);
       Audit_Trail.column_insert (raid, 'PK_PF', :NEW.PK_PF);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/