CREATE  OR REPLACE TRIGGER SCH_BGTCAL_AD0 AFTER DELETE ON SCH_BGTCAL        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'SCH_BGTCAL', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'BGTCAL_CLINICFLAG', :OLD.BGTCAL_CLINICFLAG);
       Audit_Trail.column_delete (raid, 'BGTCAL_CLINICOHEAD', :OLD.BGTCAL_CLINICOHEAD);
       Audit_Trail.column_delete (raid, 'BGTCAL_DELFLAG', :OLD.BGTCAL_DELFLAG);
       Audit_Trail.column_delete (raid, 'BGTCAL_DISCOUNT', :OLD.BGTCAL_DISCOUNT);
       Audit_Trail.column_delete (raid, 'BGTCAL_DISCOUNTFLAG', :OLD.BGTCAL_DISCOUNTFLAG);
       Audit_Trail.column_delete (raid, 'BGTCAL_EXCLDSOCFLAG', :OLD.BGTCAL_EXCLDSOCFLAG);
       Audit_Trail.column_delete (raid, 'BGTCAL_FRGBENEFIT', :OLD.BGTCAL_FRGBENEFIT);
       Audit_Trail.column_delete (raid, 'BGTCAL_FRGFLAG', :OLD.BGTCAL_FRGFLAG);
       Audit_Trail.column_delete (raid, 'BGTCAL_INDIRECTCOST', :OLD.BGTCAL_INDIRECTCOST);
       Audit_Trail.column_delete (raid, 'BGTCAL_PROTID', :OLD.BGTCAL_PROTID);
       Audit_Trail.column_delete (raid, 'BGTCAL_PROTTYPE', :OLD.BGTCAL_PROTTYPE);
       Audit_Trail.column_delete (raid, 'BGTCAL_SPONSORFLAG', :OLD.BGTCAL_SPONSORFLAG);
       Audit_Trail.column_delete (raid, 'BGTCAL_SPONSOROHEAD', :OLD.BGTCAL_SPONSOROHEAD);
       Audit_Trail.column_delete (raid, 'BGTCAL_SP_OVERHEAD', :OLD.BGTCAL_SP_OVERHEAD);
       Audit_Trail.column_delete (raid, 'BGTCAL_SP_OVERHEAD_FLAG', :OLD.BGTCAL_SP_OVERHEAD_FLAG);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'FK_BUDGET', :OLD.FK_BUDGET);
       Audit_Trail.column_delete (raid, 'GRAND_DISC_COST', :OLD.GRAND_DISC_COST);
       Audit_Trail.column_delete (raid, 'GRAND_DISC_TOTALCOST', :OLD.GRAND_DISC_TOTALCOST);
       Audit_Trail.column_delete (raid, 'GRAND_FRINGE_COST', :OLD.GRAND_FRINGE_COST);
       Audit_Trail.column_delete (raid, 'GRAND_FRINGE_TOTALCOST', :OLD.GRAND_FRINGE_TOTALCOST);
       Audit_Trail.column_delete (raid, 'GRAND_IND_COST', :OLD.GRAND_IND_COST);
       Audit_Trail.column_delete (raid, 'GRAND_IND_TOTALCOST', :OLD.GRAND_IND_TOTALCOST);
       Audit_Trail.column_delete (raid, 'GRAND_RES_COST', :OLD.GRAND_RES_COST);
       Audit_Trail.column_delete (raid, 'GRAND_RES_SPONSOR', :OLD.GRAND_RES_SPONSOR);
       Audit_Trail.column_delete (raid, 'GRAND_RES_TOTALCOST', :OLD.GRAND_RES_TOTALCOST);
       Audit_Trail.column_delete (raid, 'GRAND_RES_VARIANCE', :OLD.GRAND_RES_VARIANCE);
       Audit_Trail.column_delete (raid, 'GRAND_SALARY_COST', :OLD.GRAND_SALARY_COST);
       Audit_Trail.column_delete (raid, 'GRAND_SALARY_TOTALCOST', :OLD.GRAND_SALARY_TOTALCOST);
       Audit_Trail.column_delete (raid, 'GRAND_SOC_COST', :OLD.GRAND_SOC_COST);
       Audit_Trail.column_delete (raid, 'GRAND_SOC_SPONSOR', :OLD.GRAND_SOC_SPONSOR);
       Audit_Trail.column_delete (raid, 'GRAND_SOC_TOTALCOST', :OLD.GRAND_SOC_TOTALCOST);
       Audit_Trail.column_delete (raid, 'GRAND_SOC_VARIANCE', :OLD.GRAND_SOC_VARIANCE);
       Audit_Trail.column_delete (raid, 'GRAND_TOTAL_COST', :OLD.GRAND_TOTAL_COST);
       Audit_Trail.column_delete (raid, 'GRAND_TOTAL_SPONSOR', :OLD.GRAND_TOTAL_SPONSOR);
       Audit_Trail.column_delete (raid, 'GRAND_TOTAL_TOTALCOST', :OLD.GRAND_TOTAL_TOTALCOST);
       Audit_Trail.column_delete (raid, 'GRAND_TOTAL_VARIANCE', :OLD.GRAND_TOTAL_VARIANCE);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_BGTCAL', :OLD.PK_BGTCAL);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/
