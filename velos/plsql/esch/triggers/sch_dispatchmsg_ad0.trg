CREATE  OR REPLACE TRIGGER SCH_DISPATCHMSG_AD0 AFTER DELETE ON SCH_DISPATCHMSG        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'SCH_DISPATCHMSG', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'FK_EVENT', :OLD.FK_EVENT);
       Audit_Trail.column_delete (raid, 'FK_PAT', :OLD.FK_PAT);
       Audit_Trail.column_delete (raid, 'FK_PATPROT', :OLD.FK_PATPROT);
       Audit_Trail.column_delete (raid, 'FK_SCHEVENT', :OLD.FK_SCHEVENT);
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'MSG_ATTEMPTS', :OLD.MSG_ATTEMPTS);
       Audit_Trail.column_delete (raid, 'MSG_CLUBTEXT', :OLD.MSG_CLUBTEXT);
       Audit_Trail.column_delete (raid, 'MSG_ISCLUBBED', :OLD.MSG_ISCLUBBED);
       Audit_Trail.column_delete (raid, 'MSG_SENDON', :OLD.MSG_SENDON);
       Audit_Trail.column_delete (raid, 'MSG_STATUS', :OLD.MSG_STATUS);
       Audit_Trail.column_delete (raid, 'MSG_SUBJECT', :OLD.MSG_SUBJECT);
       Audit_Trail.column_delete (raid, 'MSG_TEXT', :OLD.MSG_TEXT);
       Audit_Trail.column_delete (raid, 'MSG_TYPE', :OLD.MSG_TYPE);
       Audit_Trail.column_delete (raid, 'PK_MSG', :OLD.PK_MSG);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/
