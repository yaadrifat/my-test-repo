CREATE  OR REPLACE TRIGGER SCH_EVENTS1_AD0 AFTER DELETE ON SCH_EVENTS1        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
   usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
    oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'SCH_EVENTS1', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'ACTUAL_SCHDATE', :OLD.ACTUAL_SCHDATE);
       Audit_Trail.column_delete (raid, 'ADVERSE_COUNT', :OLD.ADVERSE_COUNT);
       Audit_Trail.column_delete (raid, 'ALNOT_SENT', :OLD.ALNOT_SENT);
       Audit_Trail.column_delete (raid, 'ATTENDED', :OLD.ATTENDED);
       Audit_Trail.column_delete (raid, 'BOOKEDON', :OLD.BOOKEDON);
       Audit_Trail.column_delete (raid, 'BOOKED_BY', :OLD.BOOKED_BY);
       Audit_Trail.column_delete (raid, 'CHILD_SERVICE_ID', :OLD.CHILD_SERVICE_ID);
       Audit_Trail.column_delete (raid, 'CREATED_ON', :OLD.CREATED_ON);
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'DESCRIPTION', :OLD.DESCRIPTION);
       Audit_Trail.column_delete (raid, 'END_DATE_TIME', :OLD.END_DATE_TIME);
       Audit_Trail.column_delete (raid, 'EVENT_EXEBY', :OLD.EVENT_EXEBY);
       Audit_Trail.column_delete (raid, 'EVENT_EXEON', :OLD.EVENT_EXEON);
       Audit_Trail.column_delete (raid, 'EVENT_ID', :OLD.EVENT_ID);
       Audit_Trail.column_delete (raid, 'EVENT_NOTES', :OLD.EVENT_NOTES);
       Audit_Trail.column_delete (raid, 'EVENT_NUM', :OLD.EVENT_NUM);
       Audit_Trail.column_delete (raid, 'EVENT_SEQUENCE', :OLD.EVENT_SEQUENCE);
       Audit_Trail.column_delete (raid, 'FACILITY_ID', :OLD.FACILITY_ID);
       Audit_Trail.column_delete (raid, 'FK_ASSOC', :OLD.FK_ASSOC);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from SCH_CODELST where PK_CODELST=:OLD.FK_CODELST_COVERTYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_COVERTYPE', codeList_desc);
       Audit_Trail.column_delete (raid, 'FK_PATPROT', :OLD.FK_PATPROT);
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'FK_VISIT', :OLD.FK_VISIT);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'ISCONFIRMED', :OLD.ISCONFIRMED);
       Audit_Trail.column_delete (raid, 'ISWAITLISTED', :OLD.ISWAITLISTED);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', :OLD.LAST_MODIFIED_DATE);
       Audit_Trail.column_delete (raid, 'LOCATION_ID', :OLD.LOCATION_ID);
       Audit_Trail.column_delete (raid, 'LOCATION_NAME', :OLD.LOCATION_NAME);
       Audit_Trail.column_delete (raid, 'NOTES', :OLD.NOTES);
       Audit_Trail.column_delete (raid, 'OBJECT_ID', :OLD.OBJECT_ID);
       Audit_Trail.column_delete (raid, 'OBJECT_NAME', :OLD.OBJECT_NAME);
       Audit_Trail.column_delete (raid, 'OBJ_LOCATION_ID', :OLD.OBJ_LOCATION_ID);
       Audit_Trail.column_delete (raid, 'OCCURENCE_ID', :OLD.OCCURENCE_ID);
       Audit_Trail.column_delete (raid, 'PATIENT_ID', :OLD.PATIENT_ID);
       Audit_Trail.column_delete (raid, 'REASON_FOR_COVERAGECHANGE', :OLD.REASON_FOR_COVERAGECHANGE);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'ROLES', :OLD.ROLES);
       Audit_Trail.column_delete (raid, 'SERVICE_ID', :OLD.SERVICE_ID);
       Audit_Trail.column_delete (raid, 'SERVICE_SITE_ID', :OLD.SERVICE_SITE_ID);
       Audit_Trail.column_delete (raid, 'SESSION_ID', :OLD.SESSION_ID);
       Audit_Trail.column_delete (raid, 'SESSION_NAME', :OLD.SESSION_NAME);
       Audit_Trail.column_delete (raid, 'START_DATE_TIME', :OLD.START_DATE_TIME);
       Audit_Trail.column_delete (raid, 'STATUS', :OLD.STATUS);
       Audit_Trail.column_delete (raid, 'SVC_TYPE_ID', :OLD.SVC_TYPE_ID);
       Audit_Trail.column_delete (raid, 'SVC_TYPE_NAME', :OLD.SVC_TYPE_NAME);
       Audit_Trail.column_delete (raid, 'TYPE_ID', :OLD.TYPE_ID);
       Audit_Trail.column_delete (raid, 'USER_NAME', :OLD.USER_NAME);
       Audit_Trail.column_delete (raid, 'VISIT', :OLD.VISIT);
       Audit_Trail.column_delete (raid, 'VISIT_TYPE_ID', :OLD.VISIT_TYPE_ID);
       Audit_Trail.column_delete (raid, 'VISIT_TYPE_NAME', :OLD.VISIT_TYPE_NAME);
COMMIT;
END;
/