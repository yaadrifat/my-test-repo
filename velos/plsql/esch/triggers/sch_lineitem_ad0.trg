CREATE  OR REPLACE TRIGGER SCH_LINEITEM_AD0 AFTER DELETE ON SCH_LINEITEM        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
   usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
    oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'SCH_LINEITEM', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON', :OLD.CREATED_ON);
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'FK_BGTSECTION', :OLD.FK_BGTSECTION);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from SCH_CODELST where PK_CODELST=:OLD.FK_CODELST_CATEGORY;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_CATEGORY', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from SCH_CODELST where PK_CODELST=:OLD.FK_CODELST_COST_TYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_COST_TYPE', codeList_desc);
       Audit_Trail.column_delete (raid, 'FK_EVENT', :OLD.FK_EVENT);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', :OLD.LAST_MODIFIED_DATE);
       Audit_Trail.column_delete (raid, 'LINEITEM_APPLYINDIRECTS', :OLD.LINEITEM_APPLYINDIRECTS);
       Audit_Trail.column_delete (raid, 'LINEITEM_APPLYINFUTURE', :OLD.LINEITEM_APPLYINFUTURE);
       Audit_Trail.column_delete (raid, 'LINEITEM_CDM', :OLD.LINEITEM_CDM);
       Audit_Trail.column_delete (raid, 'LINEITEM_CLINICNOFUNIT', :OLD.LINEITEM_CLINICNOFUNIT);
       Audit_Trail.column_delete (raid, 'LINEITEM_CPTCODE', :OLD.LINEITEM_CPTCODE);
       Audit_Trail.column_delete (raid, 'LINEITEM_DELFLAG', :OLD.LINEITEM_DELFLAG);
       Audit_Trail.column_delete (raid, 'LINEITEM_DESC', :OLD.LINEITEM_DESC);
       Audit_Trail.column_delete (raid, 'LINEITEM_INCOSTDISC', :OLD.LINEITEM_INCOSTDISC);
       Audit_Trail.column_delete (raid, 'LINEITEM_INPERSEC', :OLD.LINEITEM_INPERSEC);
       Audit_Trail.column_delete (raid, 'LINEITEM_INVCOST', :OLD.LINEITEM_INVCOST);
       Audit_Trail.column_delete (raid, 'LINEITEM_NAME', :OLD.LINEITEM_NAME);
       Audit_Trail.column_delete (raid, 'LINEITEM_NOTES', :OLD.LINEITEM_NOTES);
       Audit_Trail.column_delete (raid, 'LINEITEM_OTHERCOST', :OLD.LINEITEM_OTHERCOST);
       Audit_Trail.column_delete (raid, 'LINEITEM_PARENTID', :OLD.LINEITEM_PARENTID);
       Audit_Trail.column_delete (raid, 'LINEITEM_REPEAT', :OLD.LINEITEM_REPEAT);
       Audit_Trail.column_delete (raid, 'LINEITEM_RESCOST', :OLD.LINEITEM_RESCOST);
       Audit_Trail.column_delete (raid, 'LINEITEM_SEQ', :OLD.LINEITEM_SEQ);
       Audit_Trail.column_delete (raid, 'LINEITEM_SPONSORAMOUNT', :OLD.LINEITEM_SPONSORAMOUNT);
       Audit_Trail.column_delete (raid, 'LINEITEM_SPONSORUNIT', :OLD.LINEITEM_SPONSORUNIT);
       Audit_Trail.column_delete (raid, 'LINEITEM_STDCARECOST', :OLD.LINEITEM_STDCARECOST);
       Audit_Trail.column_delete (raid, 'LINEITEM_TMID', :OLD.LINEITEM_TMID);
       Audit_Trail.column_delete (raid, 'LINEITEM_TOTALCOST', :OLD.LINEITEM_TOTALCOST);
       Audit_Trail.column_delete (raid, 'LINEITEM_VARIANCE', :OLD.LINEITEM_VARIANCE);
       Audit_Trail.column_delete (raid, 'PK_LINEITEM', :OLD.PK_LINEITEM);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'SUBCOST_ITEM_FLAG', :OLD.SUBCOST_ITEM_FLAG);
       Audit_Trail.column_delete (raid, 'LINEITEM_APPLYPATIENTCOUNT', :OLD.LINEITEM_APPLYPATIENTCOUNT);
COMMIT;
END;
/