CREATE  OR REPLACE TRIGGER SCH_BUDGET_BI0 BEFORE INSERT ON SCH_BUDGET        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'SCH_BUDGET', erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'BUDGET_CALENDAR', :NEW.BUDGET_CALENDAR);
       Audit_Trail.column_insert (raid, 'BUDGET_CALFLAG', :NEW.BUDGET_CALFLAG);
       Audit_Trail.column_insert (raid, 'BUDGET_COMBFLAG', :NEW.BUDGET_COMBFLAG);
       Audit_Trail.column_insert (raid, 'BUDGET_CREATOR', :NEW.BUDGET_CREATOR);
       Audit_Trail.column_insert (raid, 'BUDGET_CURRENCY', :NEW.BUDGET_CURRENCY);
       Audit_Trail.column_insert (raid, 'BUDGET_DELFLAG', :NEW.BUDGET_DELFLAG);
       Audit_Trail.column_insert (raid, 'BUDGET_DESC', :NEW.BUDGET_DESC);
       Audit_Trail.column_insert (raid, 'BUDGET_NAME', :NEW.BUDGET_NAME);
       Audit_Trail.column_insert (raid, 'BUDGET_RIGHTS', :NEW.BUDGET_RIGHTS);
       Audit_Trail.column_insert (raid, 'BUDGET_RIGHTSCOPE', :NEW.BUDGET_RIGHTSCOPE);
       Audit_Trail.column_insert (raid, 'BUDGET_SITEFLAG', :NEW.BUDGET_SITEFLAG);
       Audit_Trail.column_insert (raid, 'BUDGET_STATUS', :NEW.BUDGET_STATUS);
       Audit_Trail.column_insert (raid, 'BUDGET_TEMPLATE', :NEW.BUDGET_TEMPLATE);
       Audit_Trail.column_insert (raid, 'BUDGET_TYPE', :NEW.BUDGET_TYPE);
       Audit_Trail.column_insert (raid, 'BUDGET_VERSION', :NEW.BUDGET_VERSION);
       Audit_Trail.column_insert (raid, 'CREATED_ON', :NEW.CREATED_ON);
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'FK_ACCOUNT', :NEW.FK_ACCOUNT);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from  SCH_CODELST where PK_CODELST=:NEW.FK_CODELST_STATUS;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_STATUS', codeList_desc);
       Audit_Trail.column_insert (raid, 'FK_SITE', :NEW.FK_SITE);
       Audit_Trail.column_insert (raid, 'FK_STUDY', :NEW.FK_STUDY);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', :NEW.LAST_MODIFIED_DATE);
       Audit_Trail.column_insert (raid, 'PK_BUDGET', :NEW.PK_BUDGET);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/