CREATE  OR REPLACE TRIGGER SCH_BGTSECTION_BI0 BEFORE INSERT ON SCH_BGTSECTION        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'SCH_BGTSECTION', erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'BGTSECTION_DELFLAG', :NEW.BGTSECTION_DELFLAG);
       Audit_Trail.column_insert (raid, 'BGTSECTION_NAME', :NEW.BGTSECTION_NAME);
       Audit_Trail.column_insert (raid, 'BGTSECTION_NOTES', :NEW.BGTSECTION_NOTES);
       Audit_Trail.column_insert (raid, 'BGTSECTION_PATNO', :NEW.BGTSECTION_PATNO);
       Audit_Trail.column_insert (raid, 'BGTSECTION_PERSONLFLAG', :NEW.BGTSECTION_PERSONLFLAG);
       Audit_Trail.column_insert (raid, 'BGTSECTION_SEQUENCE', :NEW.BGTSECTION_SEQUENCE);
       Audit_Trail.column_insert (raid, 'BGTSECTION_TYPE', :NEW.BGTSECTION_TYPE);
       Audit_Trail.column_insert (raid, 'BGTSECTION_VISIT', :NEW.BGTSECTION_VISIT);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'FK_BGTCAL', :NEW.FK_BGTCAL);
       Audit_Trail.column_insert (raid, 'FK_VISIT', :NEW.FK_VISIT);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_BUDGETSEC', :NEW.PK_BUDGETSEC);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'SOC_COST_GRANDTOTAL', :NEW.SOC_COST_GRANDTOTAL);
       Audit_Trail.column_insert (raid, 'SOC_COST_SPONSOR', :NEW.SOC_COST_SPONSOR);
       Audit_Trail.column_insert (raid, 'SOC_COST_TOTAL', :NEW.SOC_COST_TOTAL);
       Audit_Trail.column_insert (raid, 'SOC_COST_VARIANCE', :NEW.SOC_COST_VARIANCE);
       Audit_Trail.column_insert (raid, 'SRT_COST_GRANDTOTAL', :NEW.SRT_COST_GRANDTOTAL);
       Audit_Trail.column_insert (raid, 'SRT_COST_SPONSOR', :NEW.SRT_COST_SPONSOR);
       Audit_Trail.column_insert (raid, 'SRT_COST_TOTAL', :NEW.SRT_COST_TOTAL);
       Audit_Trail.column_insert (raid, 'SRT_COST_VARIANCE', :NEW.SRT_COST_VARIANCE);
COMMIT;
END;
/
