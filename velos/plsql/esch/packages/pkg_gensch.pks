CREATE OR REPLACE PACKAGE        "PKG_GENSCH" IS

PROCEDURE deprecated_SP_GENCH  (
   P_PROTOCOL    IN   NUMBER,
   P_PATIENTID   IN   NUMBER,
   P_DATE        IN   DATE,
   P_PATPROTID   IN   NUMBER ,
   P_DAYS        IN   NUMBER ,
   P_DMLBY       IN   NUMBER ,
   P_IPADD       IN   VARCHAR2
);

PROCEDURE SP_ADDMSG (
  P_PATIENTID   IN   NUMBER,
   p_study       IN   NUMBER,
   P_DMLBY       IN   NUMBER,
   P_IPADD       IN   VARCHAR2,
   p_patprot	 IN   NUMBER
 );

PROCEDURE SP_UPDATE_ACTDT(
p_oldactdt IN VARCHAR,
p_newactdt IN VARCHAR,
p_event_id IN NUMBER,
p_fk_patprot IN NUMBER,
p_flag IN NUMBER,
p_ip IN VARCHAR,
p_modby IN NUMBER,
p_calassoc IN CHAR,
p_synch_sugdate IN NUMBER
);


PROCEDURE SP_CHGMSGSTAT(
p_fk_patprot IN NUMBER,
p_flag IN NUMBER,
p_msgtype IN VARCHAR,
p_ip IN VARCHAR,
p_modby IN NUMBER
);

PROCEDURE SP_COPYNOTIFYSETTING(
p_old_fkpatprot IN NUMBER,
p_new_fkpatprot IN NUMBER,
p_copyflag IN NUMBER,
p_creator IN NUMBER,
p_ip IN VARCHAR

);

PROCEDURE SP_SETMAILSTAT(
p_patprot IN NUMBER,
p_modifiedby IN NUMBER,
p_ip IN VARCHAR
);


-- for clubbed notifications

PROCEDURE SP_ADDMSG_TO_TRAN (
P_PATIENTID   IN   NUMBER,
p_study       IN   NUMBER,
P_DMLBY       IN   NUMBER,
P_IPADD       IN   VARCHAR2,
p_patprot	 IN   NUMBER
);




  PROCEDURE SP_CHGMSGSTAT_TRAN (
   p_fk_patprot IN NUMBER,
   p_flag IN NUMBER,
   p_msgtype IN VARCHAR,
   p_ip IN VARCHAR,
   p_modby IN NUMBER
  );

  PROCEDURE SP_DELETE_STUDY_CALENDAR (p_study IN NUMBER, p_cal IN NUMBER, o_ret OUT NUMBER) ;

 PROCEDURE  sp_get_schedule_page_records
 ( p_page NUMBER, p_recs_per_page NUMBER, p_schsql VARCHAR2, p_countsql VARCHAR2, p_schwhere VARCHAR2,
   o_schres OUT Gk_Cv_Types.GenericCursorType, o_schrows OUT NUMBER, o_crfres OUT Gk_Cv_Types.GenericCursorType
  );


  PROCEDURE  sp_transfer_crf_to_schedule
 ( p_crf NUMBER, p_event NUMBER, p_crflib_number VARCHAR2,p_crflib_name VARCHAR2, p_creator NUMBER,p_ipadd VARCHAR2 , p_formflag NUMBER);


 PROCEDURE  sp_update_msgstatus_discdt(
   studyId     NUMBER,
   patProtPK   NUMBER
   );

  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'pkg_gensch', pLEVEL  => Plog.LDEBUG);

     /* Created by Sonia Abrol, 08/24/06, return a comma separated string for patient from counts for each status the forms were answered*/
     FUNCTION f_crfforms_resp_stat(p_from NUMBER ,p_patprot NUMBER, p_event_id NUMBER) RETURN VARCHAR2;

     FUNCTION f_get_event_roles(event_id NUMBER) RETURN VARCHAR2;

	 /* Created by Sonia Abrol, 08/25/06, checks for all CRfs linked with a Patient schedule PK's schedule record
	 If all CRFs are filled atleast once and if  the event
	 is still marked 'not done', it will return 1.
	 If the event already has a status return 0
	 If there are events is still not done'  but there are no linked CRfs, return 0
	 If all CRFs are not marked done, it will return 0
	 	 */



	 FUNCTION f_getCRFEventMarkDoneFlag(p_fk_events1 NUMBER) RETURN NUMBER;

		 /* Procedure to refresh notifications for existing schedules
	 Sonia Abrol, 10/30/06*/
	 PROCEDURE sp_refresh_cal_messages (
      p_calendar IN NUMBER,
	  p_study IN NUMBER,
      p_dmlby       IN   NUMBER,
      p_ipadd       IN   VARCHAR2,
	  o_ret OUT VARCHAR2
   );

   /*Procedure to create Admin schedule emails */

   PROCEDURE sp_admin_msg_to_tran (
      p_protocol  IN   NUMBER,
      p_study       IN   NUMBER,
      p_dmlby       IN   NUMBER,
      p_ipadd       IN   VARCHAR2
   );


-- JM: 20Jun2008, #3540---------------------------------------------------------------------
PROCEDURE sp_addmsg_to_tran_per_event (
p_eventid IN NUMBER,
P_PATIENTID   IN   NUMBER,
p_study       IN   NUMBER,
P_DMLBY       IN   NUMBER,
P_IPADD       IN   VARCHAR2,
p_patprot	 IN   NUMBER
);


PROCEDURE sp_setmailstat_per_event(
p_eventid IN NUMBER,
p_patprot IN NUMBER,
p_modifiedby IN NUMBER,
p_ip IN VARCHAR
);

PROCEDURE sp_chgmsgstat_tran_event (
   p_eventid IN NUMBER,
   p_fk_patprot IN NUMBER,
   p_flag IN NUMBER,
   p_msgtype IN VARCHAR,
   p_ip IN VARCHAR,
   p_modby IN NUMBER
  );

-- JM: 20Jun2008, #3540------------------------------------------------------------

END Pkg_Gensch ;
/


CREATE SYNONYM ERES.PKG_GENSCH FOR PKG_GENSCH;


CREATE SYNONYM EPAT.PKG_GENSCH FOR PKG_GENSCH;


GRANT EXECUTE, DEBUG ON PKG_GENSCH TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_GENSCH TO ERES;

