set define off;
CREATE OR REPLACE PACKAGE ERES."PKG_FORM" 
AS
    PROCEDURE SP_INSERT_FLDRESPONSES(p_field VARCHAR2, p_resp_seqs ARRAY_STRING, p_resp_dispvals ARRAY_STRING,
                   p_resp_datavals ARRAY_STRING, p_resp_scores ARRAY_STRING, p_resp_isdefaults ARRAY_STRING,
			    p_record_types ARRAY_STRING, p_user VARCHAR2, p_ipadd VARCHAR2, o_ret OUT NUMBER);
    PROCEDURE SP_UPDATE_FLDRESPONSES(p_pk_fieldresps ARRAY_STRING, p_resp_seqs ARRAY_STRING, p_resp_dispvals ARRAY_STRING,
                   p_resp_datavals ARRAY_STRING, p_resp_scores ARRAY_STRING, p_resp_isdefaults ARRAY_STRING,
			    p_record_types ARRAY_STRING, p_user VARCHAR2, p_ipadd VARCHAR2, o_ret OUT NUMBER);
    PROCEDURE SP_COPYFIELD_TOLIB(p_pk_field NUMBER,p_libflag CHAR, p_user VARCHAR2, p_ipadd VARCHAR2,o_ret OUT NUMBER);
    PROCEDURE SP_FORMSHAREWITH(p_formid NUMBER, p_pk_sharewith_ids VARCHAR2,
			    p_sharewith_type CHAR, p_user VARCHAR2, p_ipadd VARCHAR2, p_mode VARCHAR2, o_ret OUT NUMBER);
    PROCEDURE SP_GETFIELDXML(p_pk_field NUMBER, p_fldname VARCHAR2, p_fldkeyword VARCHAR2, p_flduniqueid VARCHAR2, p_browserflag NUMBER, p_systemid VARCHAR2, p_fldtype CHAR , p_fld_datatype VARCHAR2, p_fld_defresp VARCHAR2, p_colcount NUMBER,p_fldlinesno NUMBER,p_fld_origsysid IN VARCHAR2, p_pksec NUMBER,p_secname VARCHAR2,p_fld_charsno NUMBER,p_sortorder VARCHAR2,o_xml OUT CLOB);
    FUNCTION F_GENERATEFORM_HTML (p_formid NUMBER) RETURN CLOB ;
    PROCEDURE SP_FORMDEFAULTDATA ( p_pk_form  IN NUMBER , o_out_field   OUT NUMBER , o_out_formfld OUT NUMBER  );
	PROCEDURE SP_SAVEFORM (p_formid NUMBER, p_param ARRAY_STRING, p_paramvalues ARRAY_STRING );
	PROCEDURE SP_COPY_MULTIPLE_FLDS_FROM_LIB (p_section STRING, p_field_ids ARRAY_STRING, p_user VARCHAR2, p_ipadd VARCHAR2, o_ret OUT NUMBER, o_pk_formfld OUT ARRAY_STRING, o_pk_fldlib OUT ARRAY_STRING);

	PROCEDURE SP_GETFILLEDFORM_HTML(p_pkfilledform IN NUMBER, p_formdisplaytype IN VARCHAR2, o_xml OUT CLOB, o_formstat OUT NUMBER,o_xsl OUT CLOB,o_specimen OUT Number);

	PROCEDURE SP_COPYFIELD_TOFORM(p_pk_formfld NUMBER, p_formsec NUMBER, p_field NUMBER, p_user VARCHAR2, p_ipadd VARCHAR2, o_ret_pkField OUT NUMBER, o_ret_pkFormFld OUT NUMBER );

	PROCEDURE SP_INSERT_PAT_FORM(p_form IN NUMBER, p_patient IN NUMBER,p_patprot IN NUMBER, p_formxml IN CLOB,
	p_completed IN NUMBER, p_creator IN NUMBER, p_valid IN NUMBER, p_ipAdd IN VARCHAR2, p_disptype IN VARCHAR2 , o_ret OUT NUMBER, p_schevent IN NUMBER
	,p_specimen IN VARCHAR2);

	PROCEDURE SP_INSERT_STUDY_FORM(p_form IN NUMBER, p_study IN NUMBER, p_formxml IN CLOB,
	p_completed IN NUMBER, p_creator IN NUMBER, p_valid IN NUMBER, p_ipAdd IN VARCHAR2, o_ret OUT NUMBER,p_specimen IN VARCHAR2);

	PROCEDURE SP_INSERT_ACCT_FORM(p_form IN NUMBER, p_account IN NUMBER, p_formxml IN CLOB,
	p_completed IN NUMBER, p_creator IN NUMBER, p_valid IN NUMBER, p_ipAdd IN VARCHAR2, o_ret OUT NUMBER,p_specimen IN VARCHAR2);

	PROCEDURE SP_REPEAT_FIELD(p_pk_field NUMBER, p_user VARCHAR2, p_ipadd VARCHAR2,o_ret OUT NUMBER);
	PROCEDURE SP_UPDATE_FILLEDFORM(p_pkfilledform IN NUMBER, p_formxml IN CLOB,
	p_completed IN NUMBER, p_modifiedby IN NUMBER, p_valid IN NUMBER, p_ipAdd IN VARCHAR2, p_disptype IN VARCHAR2 , o_ret OUT NUMBER);
     PROCEDURE SP_SETFORMFLD_BROWSERFLAG(SETBROWSER_ONE VARCHAR2, SETBROWSER_ZERO VARCHAR2, LAST_MODBY NUMBER, RECORD_TYPE VARCHAR2, p_ipadd VARCHAR2, o_ret OUT NUMBER);
	PROCEDURE SP_CLUBFORMXSL(p_formid NUMBER, o_html OUT CLOB);
	PROCEDURE SP_FLD_COPY  (p_org_field_lib_id NUMBER , p_new_field_lib_id NUMBER , o_ret OUT NUMBER);
    PROCEDURE SP_COPY_MULTIPLE_FORMS(p_form_ids ARRAY_STRING,p_form_desc VARCHAR2, p_form_type NUMBER , p_linked_form_flag CHAR , p_stdacc_id NUMBER, p_user_id NUMBER , p_form_name VARCHAR2, p_disp_type VARCHAR2, p_ip_add VARCHAR2  ,  o_ret_number OUT NUMBER);
	PROCEDURE SP_COPYFLD_FOR_FORM ( p_org_field_lib_id NUMBER , p_formsec NUMBER , p_user_id NUMBER ,p_ip_add VARCHAR2  , p_account_id NUMBER ,p_formcnt NUMBER, o_ret OUT NUMBER, o_newsys OUT VARCHAR2) ;
	PROCEDURE SP_GET_SECTION_HEADER (p_formid NUMBER, p_section NUMBER, o_xsl OUT CLOB);
	PROCEDURE SP_POPMESSAGE (p_formlib_id NUMBER, o_popmsg_f OUT VARCHAR , o_popmsg_e OUT VARCHAR );
	PROCEDURE SP_DEF_SEC_DELETE (p_pk_formsec NUMBER,p_fk_formlib NUMBER,p_user VARCHAR2,p_ipadd VARCHAR2,o_ret OUT NUMBER);
	PROCEDURE SP_SEARCH_IN_XML (p_xml CLOB, p_searchparam VARCHAR2, o_value OUT STRING );
    PROCEDURE SP_COPYLOOKUPFLD_FOR_FORM ( p_org_field_lib_id NUMBER , p_formsec NUMBER , p_user_id NUMBER , p_ip_add VARCHAR2  , p_account_id NUMBER ,p_formcnt NUMBER ,p_oldfldxml CLOB, o_ret OUT NUMBER, o_newsys OUT VARCHAR2 );
	PROCEDURE testxml;
	PROCEDURE SP_GET_FORM_HTML (p_formid NUMBER, o_html OUT CLOB );
	PROCEDURE SP_INSERT_CRF_FORM(p_form IN NUMBER, p_formxml IN CLOB,p_schevent IN NUMBER, p_fkcrf IN NUMBER, p_completed IN NUMBER, p_creator IN NUMBER, p_valid IN NUMBER, p_ipAdd IN VARCHAR2, o_ret OUT NUMBER);
	PROCEDURE SP_UPDT_INSRT_FORSTATUSCHNG(p_formLibId IN NUMBER, p_status IN NUMBER, p_creator IN NUMBER, p_ipAdd IN VARCHAR2, o_ret OUT NUMBER);
	 FUNCTION f_cut_fieldlabel(P_FLDXSL VARCHAR2,P_FLDTYPE VARCHAR2,P_FLDDATATYPE VARCHAR2, P_FLDALIGN VARCHAR2,P_PKFIELD NUMBER) RETURN VARCHAR2 ;

	PROCEDURE SP_REPLACE_SYSIDS (p_oldsys VARCHAR2, p_newsys VARCHAR2,
	 io_formxsl IN OUT CLOB , io_formxml IN OUT CLOB , io_formviewxsl IN OUT CLOB ,io_formcustomjs IN OUT CLOB, io_activejs  IN OUT CLOB  );

	pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_FORM', pLEVEL  => Plog.LFATAL);

	/* Replace the old system ids with new system ids in Repeat Fields*/
	PROCEDURE SP_REPLACE_REPEAT_SYSIDS (p_oldfld NUMBER, p_newfld NUMBER,
	 io_formxsl IN OUT CLOB , io_formxml IN OUT CLOB , io_formviewxsl IN OUT CLOB ,io_formcustomjs IN OUT CLOB, io_activejs  IN OUT CLOB  );

     FUNCTION f_getFormAction(P_FORM NUMBER) RETURN clob;

    END Pkg_Form;
/

CREATE OR REPLACE SYNONYM ESCH.PKG_FORM FOR PKG_FORM;


CREATE OR REPLACE SYNONYM EPAT.PKG_FORM FOR PKG_FORM;


GRANT EXECUTE, DEBUG ON PKG_FORM TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_FORM TO ESCH;

