CREATE OR REPLACE PACKAGE BODY        "PKG_MILE_FORECAST"
AS

 PROCEDURE SP_MS_FORECAST (
      P_STUDY            IN       NUMBER,
      P_STDATE_STRING    IN       VARCHAR2,
      P_ENDDATE_STRING   IN       VARCHAR2,
      P_REPORT_TYPE      IN       VARCHAR2,
      O_XML_CLOB         OUT      CLOB
   )
   AS
 /***************************************************************************************************
   ** Procedure to generate Milestone Forecast (Projection) Reports
   ** Author: Sonika Talwar June 07, 2004
   ** Input parameter: Study Id
   ** Input parameter: Start Date
   ** Input parameter: End Date
   ** Input parameter: Report type, M-Month, Q-Quarter, Y-Year
   ** Output parameter: data in xml format
   **/

      V_ACHIVEMENT_DATE_ARRAY    TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_FORE_ACHIEVE_DATE_ARRAY    TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_AMOUNT_ARRAY   TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_TYPE_ARRAY     TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_DESC_ARRAY     TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_MILESTONE_RULE_ARRAY     TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();

      V_FORE_MILE_AMOUNT_ARRAY  TYPES.SMALL_STRING_ARRAY  := TYPES.SMALL_STRING_ARRAY ();
      V_FORE_MILE_TYPE_ARRAY TYPES.SMALL_STRING_ARRAY  := TYPES.SMALL_STRING_ARRAY ();
      V_FORE_MILE_DESC_ARRAY TYPES.SMALL_STRING_ARRAY  := TYPES.SMALL_STRING_ARRAY ();
      V_FORE_MILE_RULE_ARRAY TYPES.SMALL_STRING_ARRAY  := TYPES.SMALL_STRING_ARRAY ();


      V_COUNT                    NUMBER                   := 0;
      V_FORE_COUNT                    NUMBER                   := 0;
      V_REPORT_TYPE_FLAG         NUMBER                   := 0;

      TEMP_CLOB                  CLOB;

      V_TEMPVAR                  VARCHAR2 (32767);
      V_PMT_AMTS                 TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_PMT_DATES                TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_STDATE_STRING            VARCHAR2 (20);
      V_ENDDATE_STRING           VARCHAR2 (20);
--      V_STUDY_NUMBER             VARCHAR2 (50);
	        V_STUDY_NUMBER             VARCHAR2 (100);--JM:
      V_STUDY_VERSION            VARCHAR2 (50);
      V_STUDY_TITLE              VARCHAR2 (2000);
      V_STUDY_ACT_DATE           VARCHAR2 (20);
      V_STUDY_CURRENCY           VARCHAR2 (20);
      V_MIN_YEAR                 NUMBER;
      V_MAX_YEAR                 NUMBER;
      V_NO_OF_YRS                NUMBER;
      J                          NUMBER;
      V_YR_COUNT                 NUMBER;
      V_RELATIVE_INTERVAL        NUMBER;
      V_TOTAL_INTERVALS          NUMBER;
      V_INTERVAL_ARRAY           TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_VISIT_CNT_ARRAY          TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_EVENT_CNT_ARRAY          TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_PATIENT_CNT_ARRAY        TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_VISIT_AMT_ARRAY          TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_EVENT_AMT_ARRAY          TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_PATIENT_AMT_ARRAY        TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_TOTAL_AMT_ARRAY          TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_PYMT_AMT_ARRAY           TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_PYMT_DISCREPANCY_ARRAY   TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();


      V_FORE_VISIT_CNT_ARRAY          TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_FORE_EVENT_CNT_ARRAY          TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_FORE_PATIENT_CNT_ARRAY        TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_FORE_VISIT_AMT_ARRAY          TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_FORE_EVENT_AMT_ARRAY          TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_FORE_PATIENT_AMT_ARRAY        TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();
      V_FORE_TOTAL_AMT_ARRAY          TYPES.NUMBER_ARRAY
            := TYPES.NUMBER_ARRAY ();

      V_ACT_DATE_STRING          VARCHAR2 (20);
      V_BEF_ACT_STDATE_STRING    VARCHAR2 (20);
      V_BEF_ACT_ENDDATE_STRING   VARCHAR2 (20);
      V_AFT_ACT_STDATE_STRING    VARCHAR2 (20);
      V_AFT_ACT_ENDDATE_STRING   VARCHAR2 (20);
   BEGIN
      PKG_MILESTONE_REPORTS.SP_FIND_STUDY_ACT_DATES (
         P_STUDY,
         P_STDATE_STRING,
         P_ENDDATE_STRING,
         V_ACT_DATE_STRING,
         V_BEF_ACT_STDATE_STRING,
         V_BEF_ACT_ENDDATE_STRING,
         V_AFT_ACT_STDATE_STRING,
         V_AFT_ACT_ENDDATE_STRING
      );
      V_RELATIVE_INTERVAL := -1;

      SELECT STUDY_NUMBER, STUDY_VER_NO, STUDY_TITLE
        INTO V_STUDY_NUMBER, V_STUDY_VERSION, V_STUDY_TITLE
        FROM ER_STUDY
       WHERE PK_STUDY = P_STUDY;

      SELECT CODELST_SUBTYP
        INTO V_STUDY_CURRENCY
        FROM SCH_CODELST, ER_STUDY
       WHERE FK_CODELST_CURRENCY = PK_CODELST
         AND PK_STUDY = P_STUDY;
      P (V_AFT_ACT_STDATE_STRING);

      IF (V_AFT_ACT_STDATE_STRING <> ' ') THEN
         P (V_AFT_ACT_STDATE_STRING);

         PKG_MILE_FORECAST.SP_MILEFORE_STUDY_SPECIFIC (
            P_STUDY,
            0,
            V_AFT_ACT_STDATE_STRING,
            V_AFT_ACT_ENDDATE_STRING,
            V_COUNT,
		  V_FORE_COUNT,
            V_ACHIVEMENT_DATE_ARRAY,
            V_MILESTONE_AMOUNT_ARRAY,
            V_MILESTONE_TYPE_ARRAY,
            V_MILESTONE_DESC_ARRAY,
            V_MILESTONE_RULE_ARRAY,
 		  V_FORE_ACHIEVE_DATE_ARRAY,
            V_FORE_MILE_AMOUNT_ARRAY ,
            V_FORE_MILE_TYPE_ARRAY ,
            V_FORE_MILE_DESC_ARRAY ,
            V_FORE_MILE_RULE_ARRAY
         );


         V_MIN_YEAR := TO_NUMBER (SUBSTR (V_AFT_ACT_STDATE_STRING, 7, 4));
         V_MAX_YEAR := TO_NUMBER (SUBSTR (V_AFT_ACT_ENDDATE_STRING, 7, 4));


         IF (V_MIN_YEAR IS NULL) THEN
            V_MIN_YEAR := V_MAX_YEAR + 1;
         END IF;


         V_NO_OF_YRS := V_MAX_YEAR - V_MIN_YEAR + 1;

         V_YR_COUNT := V_MIN_YEAR;

         J := 1;


         IF (P_REPORT_TYPE = 'M') THEN
            V_TOTAL_INTERVALS := 12 * V_NO_OF_YRS;

            FOR I IN 1 .. V_NO_OF_YRS
            LOOP
               V_INTERVAL_ARRAY.EXTEND (12);
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 1) :=
                  'January, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 2) :=
                  'February, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 3) := 'March, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 4) := 'April, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 5) := 'May, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 6) := 'June, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 7) := 'July, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 8) :=
                  'August, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 9) :=
                  'September, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 10) :=
                  'October, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 11) :=
                  'November, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((12 * (I - 1)) + 12) :=
                  'December, ' || V_YR_COUNT;
               V_YR_COUNT := V_YR_COUNT + 1;
            END LOOP;
         END IF;


         IF (P_REPORT_TYPE = 'Q') THEN
            V_TOTAL_INTERVALS := 4 * V_NO_OF_YRS;


            FOR I IN 1 .. V_NO_OF_YRS
            LOOP
               V_INTERVAL_ARRAY.EXTEND (4);
               V_INTERVAL_ARRAY ((4 * (I - 1)) + 1) := 'Q1, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((4 * (I - 1)) + 2) := 'Q2, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((4 * (I - 1)) + 3) := 'Q3, ' || V_YR_COUNT;
               V_INTERVAL_ARRAY ((4 * (I - 1)) + 4) := 'Q4, ' || V_YR_COUNT;
               V_YR_COUNT := V_YR_COUNT + 1;
            END LOOP;
         END IF;


         IF (P_REPORT_TYPE = 'Y') THEN
            V_TOTAL_INTERVALS := V_NO_OF_YRS;


            FOR I IN 1 .. V_NO_OF_YRS
            LOOP
               V_INTERVAL_ARRAY.EXTEND;
               V_INTERVAL_ARRAY (I) := 'Year ' || V_YR_COUNT;
               V_YR_COUNT := V_YR_COUNT + 1;
            END LOOP;
         END IF;
         V_INTERVAL_ARRAY.EXTEND;

         V_VISIT_CNT_ARRAY.EXTEND;
         V_EVENT_CNT_ARRAY.EXTEND;
         V_PATIENT_CNT_ARRAY.EXTEND;
         V_VISIT_AMT_ARRAY.EXTEND;
         V_EVENT_AMT_ARRAY.EXTEND;
         V_PATIENT_AMT_ARRAY.EXTEND;
         V_TOTAL_AMT_ARRAY.EXTEND;
         V_PYMT_AMT_ARRAY.EXTEND;
         V_PYMT_DISCREPANCY_ARRAY.EXTEND;


         V_FORE_VISIT_CNT_ARRAY.EXTEND;
         V_FORE_EVENT_CNT_ARRAY.EXTEND;
         V_FORE_PATIENT_CNT_ARRAY.EXTEND;
         V_FORE_VISIT_AMT_ARRAY.EXTEND;
         V_FORE_EVENT_AMT_ARRAY.EXTEND;
         V_FORE_PATIENT_AMT_ARRAY.EXTEND;
         V_FORE_TOTAL_AMT_ARRAY.EXTEND;

         V_VISIT_CNT_ARRAY (1) := 0;
         V_EVENT_CNT_ARRAY (1) := 0;
         V_PATIENT_CNT_ARRAY (1) := 0;
         V_VISIT_AMT_ARRAY (1) := 0;
         V_EVENT_AMT_ARRAY (1) := 0;
         V_PATIENT_AMT_ARRAY (1) := 0;
         V_TOTAL_AMT_ARRAY (1) := 0;
         V_PYMT_AMT_ARRAY (1) := 0;
         V_PYMT_DISCREPANCY_ARRAY (1) := 0;

         V_FORE_VISIT_CNT_ARRAY (1) := 0;
         V_FORE_EVENT_CNT_ARRAY (1) := 0;
         V_FORE_PATIENT_CNT_ARRAY (1) := 0;
         V_FORE_VISIT_AMT_ARRAY (1) := 0;
         V_FORE_EVENT_AMT_ARRAY (1) := 0;
         V_FORE_PATIENT_AMT_ARRAY (1) := 0;
         V_FORE_TOTAL_AMT_ARRAY (1) := 0;


         V_VISIT_CNT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_EVENT_CNT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_PATIENT_CNT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_VISIT_AMT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_EVENT_AMT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_PATIENT_AMT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_TOTAL_AMT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_PYMT_AMT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_PYMT_DISCREPANCY_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);

         V_FORE_VISIT_CNT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_FORE_EVENT_CNT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_FORE_PATIENT_CNT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_FORE_VISIT_AMT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_FORE_EVENT_AMT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_FORE_PATIENT_AMT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);
         V_FORE_TOTAL_AMT_ARRAY.EXTEND (V_TOTAL_INTERVALS, 1);


         IF (P_REPORT_TYPE = 'M') THEN
            FOR I IN 1 .. V_COUNT
            LOOP
               V_RELATIVE_INTERVAL :=
                  (12 *
                      (TO_NUMBER (SUBSTR (V_ACHIVEMENT_DATE_ARRAY (I), 7, 4)) -
                       V_MIN_YEAR
                      )
                  ) +
                  TO_NUMBER (SUBSTR (V_ACHIVEMENT_DATE_ARRAY (I), 1, 2));

               IF (V_MILESTONE_TYPE_ARRAY (I) = 'PM') THEN
                  V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;

               IF (V_MILESTONE_TYPE_ARRAY (I) = 'EM') THEN
                  V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;

               IF (V_MILESTONE_TYPE_ARRAY (I) = 'VM') THEN
                  V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;

               V_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                  V_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                  TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
            END LOOP; --END V_VOUNT


--P('SONIKA V_FORE_COUNT ' || V_FORE_COUNT);

            --forecast loop
            FOR I IN 1 .. V_FORE_COUNT
            LOOP

               V_RELATIVE_INTERVAL :=
                  (12 *
                      (TO_NUMBER (SUBSTR (V_FORE_ACHIEVE_DATE_ARRAY (I), 7, 4)) -
                       V_MIN_YEAR
                      )
                  ) +
                  TO_NUMBER (SUBSTR (V_FORE_ACHIEVE_DATE_ARRAY (I), 1, 2));

               IF (V_FORE_MILE_TYPE_ARRAY (I) = 'PM') THEN
                  V_FORE_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=    V_FORE_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_FORE_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                   V_FORE_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +         TO_NUMBER (V_FORE_MILE_AMOUNT_ARRAY (I));
               END IF;

               IF (V_FORE_MILE_TYPE_ARRAY (I) = 'EM') THEN
                  V_FORE_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_FORE_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_FORE_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_FORE_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_FORE_MILE_AMOUNT_ARRAY (I));
               END IF;

--p('V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) ' ||V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL));
               IF (V_FORE_MILE_TYPE_ARRAY (I) = 'VM') THEN

                  V_FORE_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_FORE_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_FORE_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_FORE_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_FORE_MILE_AMOUNT_ARRAY (I));
               END IF;
--p('SONIKA FORECAST V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) ' ||V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL));
               V_FORE_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                  V_FORE_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                  TO_NUMBER (V_FORE_MILE_AMOUNT_ARRAY (I));

           END LOOP; --end v_forecount

         END IF;


         IF (P_REPORT_TYPE = 'Q') THEN
            FOR I IN 1 .. V_COUNT
            LOOP
               V_RELATIVE_INTERVAL :=
                  (4 *
                      (TO_NUMBER (SUBSTR (V_ACHIVEMENT_DATE_ARRAY (I), 7, 4)) -
                       V_MIN_YEAR
                      )
                  ) +
                  (TO_NUMBER (
                      SUBSTR (
                         TO_CHAR (
                            (TO_NUMBER (
                                SUBSTR (V_ACHIVEMENT_DATE_ARRAY (I), 1, 2)
                             ) -
                             1
                            ) /
                               3,
                            '0.0'
                         ),
                         2,
                         1
                      )
                   ) +
                   1
                  );


               IF (V_MILESTONE_TYPE_ARRAY (I) = 'PM') THEN
                  V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;


               IF (V_MILESTONE_TYPE_ARRAY (I) = 'EM') THEN
                  V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;


               IF (V_MILESTONE_TYPE_ARRAY (I) = 'VM') THEN
                  V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;

               V_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                  V_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                  TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
            END LOOP; --v_count loop



            FOR I IN 1 .. V_FORE_COUNT
            LOOP
               V_RELATIVE_INTERVAL :=
                  (4 *
                      (TO_NUMBER (SUBSTR (V_FORE_ACHIEVE_DATE_ARRAY (I), 7, 4)) -
                       V_MIN_YEAR
                      )
                  ) +
                  (TO_NUMBER (
                      SUBSTR (
                         TO_CHAR (
                            (TO_NUMBER (
                                SUBSTR (V_FORE_ACHIEVE_DATE_ARRAY (I), 1, 2)
                             ) -
                             1
                            ) /
                               3,
                            '0.0'
                         ),
                         2,
                         1
                      )
                   ) +
                   1
                  );

               IF (V_FORE_MILE_TYPE_ARRAY (I) = 'PM') THEN
                  V_FORE_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=    V_FORE_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_FORE_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                   V_FORE_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +         TO_NUMBER (V_FORE_MILE_AMOUNT_ARRAY (I));
               END IF;

               IF (V_FORE_MILE_TYPE_ARRAY (I) = 'EM') THEN
                  V_FORE_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_FORE_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_FORE_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_FORE_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_FORE_MILE_AMOUNT_ARRAY (I));
               END IF;


               IF (V_FORE_MILE_TYPE_ARRAY (I) = 'VM') THEN
                  V_FORE_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) := V_FORE_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_FORE_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                   V_FORE_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) + TO_NUMBER (V_FORE_MILE_AMOUNT_ARRAY (I));
               END IF;

               V_FORE_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                  V_FORE_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                  TO_NUMBER (V_FORE_MILE_AMOUNT_ARRAY (I));

            END LOOP; --v_fore_count loop


         END IF;


         IF (P_REPORT_TYPE = 'Y') THEN
            FOR I IN 1 .. V_COUNT
            LOOP
               V_RELATIVE_INTERVAL :=
                  TO_NUMBER (SUBSTR (V_ACHIVEMENT_DATE_ARRAY (I), 7, 4)) -
                  V_MIN_YEAR +
                  1;


               IF (V_MILESTONE_TYPE_ARRAY (I) = 'PM') THEN
                  V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;


               IF (V_MILESTONE_TYPE_ARRAY (I) = 'EM') THEN
                  V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;


               IF (V_MILESTONE_TYPE_ARRAY (I) = 'VM') THEN
                  V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
               END IF;

               V_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                  V_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                  TO_NUMBER (V_MILESTONE_AMOUNT_ARRAY (I));
            END LOOP; --v_count loop


            FOR I IN 1 .. V_FORE_COUNT
            LOOP
               V_RELATIVE_INTERVAL :=
                  TO_NUMBER (SUBSTR (V_FORE_ACHIEVE_DATE_ARRAY (I), 7, 4)) -
                  V_MIN_YEAR +
                  1;

               IF (V_FORE_MILE_TYPE_ARRAY (I) = 'PM') THEN
                  V_FORE_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=    V_FORE_PATIENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_FORE_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                   V_FORE_PATIENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +         TO_NUMBER (V_FORE_MILE_AMOUNT_ARRAY (I));
               END IF;

               IF (V_FORE_MILE_TYPE_ARRAY (I) = 'EM') THEN
                  V_FORE_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_FORE_EVENT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_FORE_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                     V_FORE_EVENT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                     TO_NUMBER (V_FORE_MILE_AMOUNT_ARRAY (I));
               END IF;


               IF (V_FORE_MILE_TYPE_ARRAY (I) = 'VM') THEN
                  V_FORE_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) := V_FORE_VISIT_CNT_ARRAY (V_RELATIVE_INTERVAL) + 1;
                  V_FORE_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                   V_FORE_VISIT_AMT_ARRAY (V_RELATIVE_INTERVAL) + TO_NUMBER (V_FORE_MILE_AMOUNT_ARRAY (I));
               END IF;

               V_FORE_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                  V_FORE_TOTAL_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                  TO_NUMBER (V_FORE_MILE_AMOUNT_ARRAY (I));
            END LOOP; --forecount

         END IF; --year

         PKG_MILESTONE_REPORTS.SP_MILEPAYMENT (
            P_STUDY,
            V_AFT_ACT_STDATE_STRING,
            V_AFT_ACT_ENDDATE_STRING,
            V_COUNT,
            V_PMT_AMTS,
            V_PMT_DATES
         );

         IF (P_REPORT_TYPE = 'M') THEN
            FOR I IN 1 .. V_COUNT
            LOOP
               V_RELATIVE_INTERVAL :=
                  (12 *
                      (TO_NUMBER (
                          SUBSTR (
                             V_PMT_DATES (
                                I
                             ),
                             7,
                             4
                          )
                       ) -
                       V_MIN_YEAR
                      )
                  ) +
                  TO_NUMBER (
                     SUBSTR (
                        V_PMT_DATES (
                           I
                        ),
                        1,
                        2
                     )
                  );
               V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                  V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                  TO_NUMBER (V_PMT_AMTS (I));
            END LOOP;
         END IF;


         IF (P_REPORT_TYPE = 'Q') THEN
            FOR I IN 1 .. V_COUNT
            LOOP
               V_RELATIVE_INTERVAL :=
                  (4 *
                      (TO_NUMBER (
                          SUBSTR (
                             V_PMT_DATES (
                                I
                             ),
                             7,
                             4
                          )
                       ) -
                       V_MIN_YEAR
                      )
                  ) +
                  (TO_NUMBER (
                      SUBSTR (
                         TO_CHAR (
                            (TO_NUMBER (
                                SUBSTR (
                                   V_PMT_DATES (
                                      I
                                   ),
                                   1,
                                   2
                                )
                             ) -
                             1
                            ) /
                               3,
                            '0.0'
                         ),
                         2,
                         1
                      )
                   ) +
                   1
                  );
               V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                  V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                  TO_NUMBER (V_PMT_AMTS (I));
            END LOOP;
         END IF;


         IF (P_REPORT_TYPE = 'Y') THEN
            FOR I IN 1 .. V_COUNT
            LOOP
               V_RELATIVE_INTERVAL :=
                  TO_NUMBER (SUBSTR (V_PMT_DATES (I), 7, 4)) - V_MIN_YEAR + 1;
               V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) :=
                  V_PYMT_AMT_ARRAY (V_RELATIVE_INTERVAL) +
                  TO_NUMBER (V_PMT_AMTS (I));
            END LOOP;
         END IF;


         J := V_TOTAL_INTERVALS + 1;

         V_INTERVAL_ARRAY (J) := 'Total';

         FOR I IN 1 .. V_TOTAL_INTERVALS
         LOOP
            V_VISIT_CNT_ARRAY (J) :=
               V_VISIT_CNT_ARRAY (J) + V_VISIT_CNT_ARRAY (I);
            V_EVENT_CNT_ARRAY (J) :=
               V_EVENT_CNT_ARRAY (J) + V_EVENT_CNT_ARRAY (I);
            V_PATIENT_CNT_ARRAY (J) :=
               V_PATIENT_CNT_ARRAY (J) + V_PATIENT_CNT_ARRAY (I);
            V_VISIT_AMT_ARRAY (J) :=
               V_VISIT_AMT_ARRAY (J) + V_VISIT_AMT_ARRAY (I);
            V_EVENT_AMT_ARRAY (J) :=
               V_EVENT_AMT_ARRAY (J) + V_EVENT_AMT_ARRAY (I);
            V_PATIENT_AMT_ARRAY (J) :=
               V_PATIENT_AMT_ARRAY (J) + V_PATIENT_AMT_ARRAY (I);
            V_TOTAL_AMT_ARRAY (J) :=
               V_TOTAL_AMT_ARRAY (J) + V_TOTAL_AMT_ARRAY (I);
            V_PYMT_AMT_ARRAY (J) := V_PYMT_AMT_ARRAY (J) + V_PYMT_AMT_ARRAY (I);
            V_PYMT_DISCREPANCY_ARRAY (I) :=
               V_PYMT_AMT_ARRAY (I) - V_TOTAL_AMT_ARRAY (I);
            V_PYMT_DISCREPANCY_ARRAY (J) :=
               V_PYMT_DISCREPANCY_ARRAY (J) + V_PYMT_DISCREPANCY_ARRAY (I);

            V_FORE_VISIT_CNT_ARRAY (J) :=
               V_FORE_VISIT_CNT_ARRAY (J) + V_FORE_VISIT_CNT_ARRAY (I);
            V_FORE_EVENT_CNT_ARRAY (J) :=
               V_FORE_EVENT_CNT_ARRAY (J) + V_FORE_EVENT_CNT_ARRAY (I);
            V_FORE_PATIENT_CNT_ARRAY (J) :=
               V_FORE_PATIENT_CNT_ARRAY (J) + V_FORE_PATIENT_CNT_ARRAY (I);
            V_FORE_VISIT_AMT_ARRAY (J) :=
               V_FORE_VISIT_AMT_ARRAY (J) + V_FORE_VISIT_AMT_ARRAY (I);
            V_FORE_EVENT_AMT_ARRAY (J) :=
               V_FORE_EVENT_AMT_ARRAY (J) + V_FORE_EVENT_AMT_ARRAY (I);
            V_FORE_PATIENT_AMT_ARRAY (J) :=
               V_FORE_PATIENT_AMT_ARRAY (J) + V_FORE_PATIENT_AMT_ARRAY (I);
            V_FORE_TOTAL_AMT_ARRAY (J) :=
               V_FORE_TOTAL_AMT_ARRAY (J) + V_FORE_TOTAL_AMT_ARRAY (I);

         END LOOP;


         V_RELATIVE_INTERVAL := J - 1;


         IF (P_REPORT_TYPE = 'M') THEN
            V_RELATIVE_INTERVAL :=
               (12 *
                   (TO_NUMBER (SUBSTR (V_AFT_ACT_ENDDATE_STRING, 7, 4)) -
                    V_MIN_YEAR
                   )
               ) +
               TO_NUMBER (SUBSTR (V_AFT_ACT_ENDDATE_STRING, 1, 2));
         END IF;


         IF (P_REPORT_TYPE = 'Q') THEN
            V_RELATIVE_INTERVAL :=
               (4 *
                   (TO_NUMBER (SUBSTR (V_AFT_ACT_ENDDATE_STRING, 7, 4)) -
                    V_MIN_YEAR
                   )
               ) +
               (TO_NUMBER (
                   SUBSTR (
                      TO_CHAR (
                         (TO_NUMBER (SUBSTR (V_AFT_ACT_ENDDATE_STRING, 1, 2)) -
                          1
                         ) /
                            3,
                         '0.0'
                      ),
                      2,
                      1
                   )
                ) +
                1
               );
         END IF;


         IF (P_REPORT_TYPE = 'Y') THEN
            V_RELATIVE_INTERVAL :=
               TO_NUMBER (SUBSTR (V_AFT_ACT_ENDDATE_STRING, 7, 4)) -
               V_MIN_YEAR +
               1;
         END IF;


         FOR I IN 1 .. (J - (V_RELATIVE_INTERVAL + 1))
         LOOP
            FOR K IN (V_RELATIVE_INTERVAL + 1) .. (J - 1)
            LOOP
               V_INTERVAL_ARRAY (K) := V_INTERVAL_ARRAY (K + 1);
               V_VISIT_CNT_ARRAY (K) := V_VISIT_CNT_ARRAY (K + 1);
               V_EVENT_CNT_ARRAY (K) := V_EVENT_CNT_ARRAY (K + 1);
               V_PATIENT_CNT_ARRAY (K) := V_PATIENT_CNT_ARRAY (K + 1);
               V_VISIT_AMT_ARRAY (K) := V_VISIT_AMT_ARRAY (K + 1);
               V_EVENT_AMT_ARRAY (K) := V_EVENT_AMT_ARRAY (K + 1);
               V_PATIENT_AMT_ARRAY (K) := V_PATIENT_AMT_ARRAY (K + 1);
               V_TOTAL_AMT_ARRAY (K) := V_TOTAL_AMT_ARRAY (K + 1);
               V_PYMT_AMT_ARRAY (K) := V_PYMT_AMT_ARRAY (K + 1);
               V_PYMT_DISCREPANCY_ARRAY (K) := V_PYMT_DISCREPANCY_ARRAY (K + 1);



               V_FORE_VISIT_CNT_ARRAY (K) := V_FORE_VISIT_CNT_ARRAY (K + 1);
               V_FORE_EVENT_CNT_ARRAY (K) := V_FORE_EVENT_CNT_ARRAY (K + 1);
               V_FORE_PATIENT_CNT_ARRAY (K) := V_FORE_PATIENT_CNT_ARRAY (K + 1);
               V_FORE_VISIT_AMT_ARRAY (K) := V_FORE_VISIT_AMT_ARRAY (K + 1);
               V_FORE_EVENT_AMT_ARRAY (K) := V_FORE_EVENT_AMT_ARRAY (K + 1);
               V_FORE_PATIENT_AMT_ARRAY (K) := V_FORE_PATIENT_AMT_ARRAY (K + 1);
               V_FORE_TOTAL_AMT_ARRAY (K) := V_FORE_TOTAL_AMT_ARRAY (K + 1);


            END LOOP;
         END LOOP;


         V_INTERVAL_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_VISIT_CNT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_EVENT_CNT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_PATIENT_CNT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_VISIT_AMT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_EVENT_AMT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_PATIENT_AMT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_TOTAL_AMT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_PYMT_AMT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_PYMT_DISCREPANCY_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);

         V_FORE_VISIT_CNT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_FORE_EVENT_CNT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_FORE_PATIENT_CNT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_FORE_VISIT_AMT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_FORE_EVENT_AMT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_FORE_PATIENT_AMT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         V_FORE_TOTAL_AMT_ARRAY.TRIM (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);

         V_TOTAL_INTERVALS :=
            V_TOTAL_INTERVALS - (V_TOTAL_INTERVALS - V_RELATIVE_INTERVAL);
         J := V_TOTAL_INTERVALS + 1;


         IF (P_REPORT_TYPE = 'M') THEN
            V_RELATIVE_INTERVAL :=
               (12 *
                   (V_MIN_YEAR -
                    TO_NUMBER (SUBSTR (V_AFT_ACT_STDATE_STRING, 7, 4))
                   )
               ) +
               TO_NUMBER (SUBSTR (V_AFT_ACT_STDATE_STRING, 1, 2));
         END IF;

         IF (P_REPORT_TYPE = 'Q') THEN
            V_RELATIVE_INTERVAL :=
               (4 *
                   (V_MIN_YEAR -
                    TO_NUMBER (
                       SUBSTR (
                          V_AFT_ACT_STDATE_STRING,
                          7,
                          4
                       )
                    )
                   )
               ) +
               (TO_NUMBER (
                   SUBSTR (
                      TO_CHAR (
                         (TO_NUMBER (
                             SUBSTR (
                                V_AFT_ACT_STDATE_STRING,
                                1,
                                2
                             )
                          ) -
                          1
                         ) /
                            3,
                         '0.0'
                      ),
                      2,
                      1
                   )
                ) +
                1
               );
         END IF;


         IF (P_REPORT_TYPE = 'Y') THEN
            V_RELATIVE_INTERVAL :=
               V_MIN_YEAR -
               TO_NUMBER (SUBSTR (V_AFT_ACT_STDATE_STRING, 7, 4)) +
               1;
         END IF;


         FOR I IN 1 .. (V_RELATIVE_INTERVAL - 1)
         LOOP
            FOR K IN 1 .. (J - 1)
            LOOP
               V_INTERVAL_ARRAY (K) := V_INTERVAL_ARRAY (K + 1);
               V_VISIT_CNT_ARRAY (K) := V_VISIT_CNT_ARRAY (K + 1);
               V_EVENT_CNT_ARRAY (K) := V_EVENT_CNT_ARRAY (K + 1);
               V_PATIENT_CNT_ARRAY (K) := V_PATIENT_CNT_ARRAY (K + 1);
               V_VISIT_AMT_ARRAY (K) := V_VISIT_AMT_ARRAY (K + 1);
               V_EVENT_AMT_ARRAY (K) := V_EVENT_AMT_ARRAY (K + 1);
               V_PATIENT_AMT_ARRAY (K) := V_PATIENT_AMT_ARRAY (K + 1);
               V_TOTAL_AMT_ARRAY (K) := V_TOTAL_AMT_ARRAY (K + 1);
               V_PYMT_AMT_ARRAY (K) := V_PYMT_AMT_ARRAY (K + 1);
               V_PYMT_DISCREPANCY_ARRAY (K) := V_PYMT_DISCREPANCY_ARRAY (K + 1);


               V_FORE_VISIT_CNT_ARRAY (K) := V_FORE_VISIT_CNT_ARRAY (K + 1);
               V_FORE_EVENT_CNT_ARRAY (K) := V_FORE_EVENT_CNT_ARRAY (K + 1);
               V_FORE_PATIENT_CNT_ARRAY (K) := V_FORE_PATIENT_CNT_ARRAY (K + 1);
               V_FORE_VISIT_AMT_ARRAY (K) := V_FORE_VISIT_AMT_ARRAY (K + 1);
               V_FORE_EVENT_AMT_ARRAY (K) := V_FORE_EVENT_AMT_ARRAY (K + 1);
               V_FORE_PATIENT_AMT_ARRAY (K) := V_FORE_PATIENT_AMT_ARRAY (K + 1);
               V_FORE_TOTAL_AMT_ARRAY (K) := V_FORE_TOTAL_AMT_ARRAY (K + 1);

            END LOOP;
         END LOOP;


         V_INTERVAL_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_VISIT_CNT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_EVENT_CNT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_PATIENT_CNT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_VISIT_AMT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_EVENT_AMT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_PATIENT_AMT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_TOTAL_AMT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_PYMT_AMT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_PYMT_DISCREPANCY_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);


	    V_FORE_VISIT_CNT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_FORE_EVENT_CNT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_FORE_PATIENT_CNT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_FORE_VISIT_AMT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_FORE_EVENT_AMT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_FORE_PATIENT_AMT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);
         V_FORE_TOTAL_AMT_ARRAY.TRIM (V_RELATIVE_INTERVAL - 1);


         V_TOTAL_INTERVALS := V_TOTAL_INTERVALS - (V_RELATIVE_INTERVAL - 1);
         J := J - (V_RELATIVE_INTERVAL - 1);
      END IF;


      DBMS_LOB.CREATETEMPORARY (O_XML_CLOB, FALSE);
      DBMS_LOB.OPEN (O_XML_CLOB, DBMS_LOB.LOB_READWRITE);
      V_TEMPVAR := '<?xml version="1.0"?><?xml:stylesheet type="text/xsl"?><ROWSET>';
      DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
      V_TEMPVAR := '<STUDY><STUDYNUM>' ||
                   V_STUDY_NUMBER ||
                   '</STUDYNUM><STUDYVER>' ||
                   V_STUDY_VERSION ||
                   '</STUDYVER><STUDYTITLE>' ||
                   V_STUDY_TITLE ||
                   '</STUDYTITLE><ACTIVATIONDATE>' ||
                   V_ACT_DATE_STRING ||
                   '</ACTIVATIONDATE><STUDYCURRENCY>' ||
                   V_STUDY_CURRENCY ||
                   '</STUDYCURRENCY></STUDY>';
      DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
--p('728 V_BEF_ACT_STDATE_STRING' || V_BEF_ACT_STDATE_STRING);
--p('729 V_BEF_ACT_ENDDATE_STRING' || V_BEF_ACT_ENDDATE_STRING);

    IF(V_BEF_ACT_ENDDATE_STRING <> ' ') THEN
      PKG_MILESTONE_REPORTS.SP_PYMTS_BEFORE_STDY_ACT (
         P_STUDY,
         V_BEF_ACT_STDATE_STRING,
         V_BEF_ACT_ENDDATE_STRING,
         P_REPORT_TYPE,
         TEMP_CLOB
      );
      DBMS_LOB.COPY (
         O_XML_CLOB,
         TEMP_CLOB,
         DBMS_LOB.GETLENGTH (TEMP_CLOB),
         DBMS_LOB.GETLENGTH (O_XML_CLOB) + 1,
         1
      );
      END IF;


      IF (V_AFT_ACT_STDATE_STRING <> ' ') THEN
         V_TEMPVAR := '<INTERVALS>';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);


         FOR I IN 1 .. V_TOTAL_INTERVALS
         LOOP
            V_TEMPVAR := '<ROW num="' ||
                         I ||
                         '"><INTERVAL>' ||
                         V_INTERVAL_ARRAY (
                            I
                         ) ||
                         '</INTERVAL><VCNT>' ||
                         V_VISIT_CNT_ARRAY (
                            I
                         ) ||
                         '</VCNT><ECNT>' ||
                         V_EVENT_CNT_ARRAY (
                            I
                         ) ||
                         '</ECNT><PCNT>' ||
                         V_PATIENT_CNT_ARRAY (
                            I
                         ) ||
                         '</PCNT><VAMT>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_VISIT_AMT_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</VAMT><EAMT>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_EVENT_AMT_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</EAMT><PAMT>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_PATIENT_AMT_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</PAMT><TOTAMT>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_TOTAL_AMT_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</TOTAMT><PYMTAMT>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_PYMT_AMT_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</PYMTAMT><DISCREPANCY>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_PYMT_DISCREPANCY_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</DISCREPANCY></ROW>';
            DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
         END LOOP;


         V_TEMPVAR := '</INTERVALS>';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
         V_TEMPVAR := '<TOTAL><INTERVAL>TOTAL</INTERVAL><VCNT>' ||
                      V_VISIT_CNT_ARRAY (V_TOTAL_INTERVALS + 1) ||
                      '</VCNT><ECNT>' ||
                      V_EVENT_CNT_ARRAY (V_TOTAL_INTERVALS + 1) ||
                      '</ECNT><PCNT>' ||
                      V_PATIENT_CNT_ARRAY (V_TOTAL_INTERVALS + 1) ||
                      '</PCNT><VAMT>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_VISIT_AMT_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</VAMT><EAMT>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_EVENT_AMT_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</EAMT><PAMT>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_PATIENT_AMT_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</PAMT><TOTAMT>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_TOTAL_AMT_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</TOTAMT><PYMTAMT>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_PYMT_AMT_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</PYMTAMT><DISCREPANCY>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_PYMT_DISCREPANCY_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</DISCREPANCY></TOTAL>';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);



   --for forecast stalwar
         V_TEMPVAR := '<FORE_INTERVALS>';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);

         FOR I IN 1 .. V_TOTAL_INTERVALS
         LOOP
            V_TEMPVAR := '<ROW num="' ||
                         I ||
                         '"><INTERVAL>' ||
                         V_INTERVAL_ARRAY (
                            I
                         ) ||
                         '</INTERVAL><VCNT>' ||
                         V_FORE_VISIT_CNT_ARRAY (
                            I
                         ) ||
                         '</VCNT><ECNT>' ||
                         V_FORE_EVENT_CNT_ARRAY (
                            I
                         ) ||
                         '</ECNT><PCNT>' ||
                         V_FORE_PATIENT_CNT_ARRAY (
                            I
                         ) ||
                         '</PCNT><VAMT>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_FORE_VISIT_AMT_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</VAMT><EAMT>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_FORE_EVENT_AMT_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</EAMT><PAMT>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_FORE_PATIENT_AMT_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</PAMT><TOTAMT>' ||
                         V_STUDY_CURRENCY ||
                         TO_CHAR (
                            V_FORE_TOTAL_AMT_ARRAY (
                               I
                            ),
                            '9999999990.00'
                         ) ||
                         '</TOTAMT>' ||
                         '</ROW>';
            DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
         END LOOP;


         V_TEMPVAR := '</FORE_INTERVALS>';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
         V_TEMPVAR := '<FORE_TOTAL><INTERVAL>FORECAST TOTAL</INTERVAL><VCNT>' ||
                      V_FORE_VISIT_CNT_ARRAY (V_TOTAL_INTERVALS + 1) ||
                      '</VCNT><ECNT>' ||
                      V_FORE_EVENT_CNT_ARRAY (V_TOTAL_INTERVALS + 1) ||
                      '</ECNT><PCNT>' ||
                      V_FORE_PATIENT_CNT_ARRAY (V_TOTAL_INTERVALS + 1) ||
                      '</PCNT><VAMT>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_FORE_VISIT_AMT_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</VAMT><EAMT>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_FORE_EVENT_AMT_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</EAMT><PAMT>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_FORE_PATIENT_AMT_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</PAMT><TOTAMT>' ||
                      V_STUDY_CURRENCY ||
                      TO_CHAR (
                         V_FORE_TOTAL_AMT_ARRAY (V_TOTAL_INTERVALS + 1),
                         '9999999990.00'
                      ) ||
                      '</TOTAMT> </FORE_TOTAL>';
         DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);










      END IF;

      V_TEMPVAR := '</ROWSET>';
      DBMS_LOB.WRITEAPPEND (O_XML_CLOB, LENGTH (V_TEMPVAR), V_TEMPVAR);
      DBMS_LOB.CLOSE (O_XML_CLOB);


   END SP_MS_FORECAST;


---------------------------------------------------------------------------------------------------------------------------
   PROCEDURE SP_MILEFORE_STUDY_SPECIFIC (
      P_STUDY                    IN       NUMBER,
	 P_ORG_ID                   IN       NUMBER,
      P_STDATE_STRING            IN       VARCHAR2,
      P_ENDDATE_STRING           IN       VARCHAR2,
      O_COUNT OUT NUMBER,
	 O_FORE_COUNT OUT NUMBER ,
      O_ACHIVEMENT_DATE_ARRAY OUT TYPES.SMALL_STRING_ARRAY,
      O_MILESTONE_AMOUNT_ARRAY   OUT      TYPES.SMALL_STRING_ARRAY,
      O_MILESTONE_TYPE_ARRAY     OUT      TYPES.SMALL_STRING_ARRAY,
      O_MILESTONE_DESC_ARRAY     OUT      TYPES.SMALL_STRING_ARRAY,
      O_MILESTONE_RULE_ARRAY     OUT      TYPES.SMALL_STRING_ARRAY,
      O_FORE_ACHIEVE_DATE_ARRAY OUT TYPES.SMALL_STRING_ARRAY,
      O_FORE_MILE_AMOUNT_ARRAY OUT TYPES.SMALL_STRING_ARRAY,
      O_FORE_MILE_TYPE_ARRAY OUT TYPES.SMALL_STRING_ARRAY,
      O_FORE_MILE_DESC_ARRAY OUT TYPES.SMALL_STRING_ARRAY,
      O_FORE_MILE_RULE_ARRAY OUT TYPES.SMALL_STRING_ARRAY

   )
   AS

      V_STDATE             DATE
            := TO_DATE (P_STDATE_STRING, PKG_DATEUTIL.F_GET_DATEFORMAT);
      V_ENDDATE            DATE
            := TO_DATE (P_ENDDATE_STRING, PKG_DATEUTIL.F_GET_DATEFORMAT);
      V_COUNT              NUMBER                   := 0;
      V_FORE_DATE               DATE                     := pkg_dateutil.f_get_null_date_str;
      V_FORE_COUNT              NUMBER                   := 0;
      V_DATE               DATE                     := pkg_dateutil.f_get_null_date_str;
      V_FORE_ACHIEVEDATES   TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();

      V_ACHIEVEMENTDATES   TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_PMT_AMTS           TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_PMT_DATES          TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
--      V_STUDY_NUMBER       VARCHAR2 (50);
	        V_STUDY_NUMBER       VARCHAR2 (100);--JM:
      V_STUDY_VERSION      VARCHAR2 (50);
      V_STUDY_TITLE        VARCHAR2 (2000);
      V_TEMPVAR            VARCHAR2 (1000);
   BEGIN
      O_ACHIVEMENT_DATE_ARRAY := TYPES.SMALL_STRING_ARRAY ();
      O_MILESTONE_AMOUNT_ARRAY := TYPES.SMALL_STRING_ARRAY ();
      O_MILESTONE_TYPE_ARRAY := TYPES.SMALL_STRING_ARRAY ();
      O_MILESTONE_DESC_ARRAY := TYPES.SMALL_STRING_ARRAY ();
      O_MILESTONE_RULE_ARRAY := TYPES.SMALL_STRING_ARRAY ();

      O_FORE_ACHIEVE_DATE_ARRAY := TYPES.SMALL_STRING_ARRAY ();
      O_FORE_MILE_AMOUNT_ARRAY := TYPES.SMALL_STRING_ARRAY ();
      O_FORE_MILE_TYPE_ARRAY := TYPES.SMALL_STRING_ARRAY ();
      O_FORE_MILE_DESC_ARRAY := TYPES.SMALL_STRING_ARRAY ();
      O_FORE_MILE_RULE_ARRAY := TYPES.SMALL_STRING_ARRAY ();

      O_COUNT := 0;
      V_COUNT := 0;
      O_FORE_COUNT := 0;
      V_FORE_COUNT := 0;
      FOR I IN( SELECT MILESTONE_TYPE, FK_CAL, CODELST_SUBTYP,
                       MILESTONE_AMOUNT, MILESTONE_VISIT, FK_EVENTASSOC,
                       MILESTONE_COUNT, CODELST_DESC, A.NAME CAL_DESC,
                       B.NAME EVENT_DESC
                  FROM ER_MILESTONE,
                       ER_CODELST,
                       EVENT_ASSOC A,
                       EVENT_ASSOC B
                 WHERE FK_STUDY = P_STUDY
                   AND MILESTONE_DELFLAG <> 'Y'
                   AND FK_CODELST_RULE = PK_CODELST
                   AND FK_CAL = A.EVENT_ID (+)
                   AND FK_EVENTASSOC = B.EVENT_ID (+)ORDER BY MILESTONE_TYPE,
                                                              CODELST_SUBTYP,
                                                              FK_CAL,
                                                              MILESTONE_VISIT,
                                                              FK_EVENTASSOC)
      LOOP
         V_ACHIEVEMENTDATES := TYPES.SMALL_STRING_ARRAY ();
         V_FORE_ACHIEVEDATES := TYPES.SMALL_STRING_ARRAY ();

         V_COUNT := 0;
	    V_FORE_COUNT :=0;

         IF (I.MILESTONE_TYPE = 'VM') THEN
            IF (I.CODELST_SUBTYP = 'vm_4') THEN
               PKG_VM_RULE.SP_VIS_RULE_CRFS_COUNT (
                  P_STUDY,
			   P_ORG_ID,
                  I.FK_CAL,
                  I.MILESTONE_VISIT,
                  'completed',
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_ACHIEVEMENTDATES
               );


		   --forecast
              PKG_VM_RULE.SP_FORE_VIS_RULE_CRFS_CNT (
                  P_STUDY,
			   P_ORG_ID,
                  I.FK_CAL,
                  I.MILESTONE_VISIT,
                  'completed',
                  V_STDATE,
                  V_ENDDATE,
                  V_FORE_COUNT,
                  V_FORE_ACHIEVEDATES
		   );

            END IF;
--p('count after SP_VIS_RULE_CRFS_COUNT ' || V_COUNT);
            IF (I.CODELST_SUBTYP = 'vm_5') THEN
               PKG_VM_RULE.SP_VIS_RULE_CRFS_COUNT (
                  P_STUDY,
			   P_ORG_ID,
                  I.FK_CAL,
                  I.MILESTONE_VISIT,
                  'submitted',
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_ACHIEVEMENTDATES
               );

		  --forecast
              PKG_VM_RULE.SP_FORE_VIS_RULE_CRFS_CNT (
                  P_STUDY,
			   P_ORG_ID,
                  I.FK_CAL,
                  I.MILESTONE_VISIT,
                  'submitted',
                  V_STDATE,
                  V_ENDDATE,
                  V_FORE_COUNT,
                  V_FORE_ACHIEVEDATES
		   );


            END IF;

            IF (I.CODELST_SUBTYP = 'vm_3') THEN
               PKG_VM_RULE.SP_VM_COUNT_EVENT_DONE_ONE (
                  P_STUDY,
			      P_ORG_ID,
                  I.FK_CAL,
                  I.MILESTONE_VISIT,
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_ACHIEVEMENTDATES
               );

		    --forecast
              PKG_VM_RULE.SP_FORE_VM_CNT_EVENTDONE_ONE (
                  P_STUDY,
		        P_ORG_ID,
                  I.FK_CAL,
                  I.MILESTONE_VISIT,
                  V_STDATE,
                  V_ENDDATE,
                  V_FORE_COUNT,
                  V_FORE_ACHIEVEDATES
               );

            END IF;

            IF (I.CODELST_SUBTYP = 'vm_2') THEN
               PKG_VM_RULE.SP_VM_COUNT_EVENT_DONE_ALL (
                  P_STUDY,
			   P_ORG_ID,
                  I.FK_CAL,
                  I.MILESTONE_VISIT,
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_ACHIEVEMENTDATES
               );

      	    --forecast
              PKG_VM_RULE.SP_FORE_VM_CNT_EVENTDONE_ONE (
                  P_STUDY,
		        P_ORG_ID,
                  I.FK_CAL,
                  I.MILESTONE_VISIT,
                  V_STDATE,
                  V_ENDDATE,
                  V_FORE_COUNT,
                  V_FORE_ACHIEVEDATES
               );

            END IF;


         END IF;
         IF (I.MILESTONE_TYPE = 'PM') THEN
            IF (I.CODELST_SUBTYP = 'pm_1') THEN
               PKG_PM_RULE.SP_CHK_PAT_RULE_ENR_ACT (
                  P_STUDY,
			   P_ORG_ID,
                  I.MILESTONE_COUNT,
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_DATE
               );
            END IF;
            IF (I.CODELST_SUBTYP = 'pm_2') THEN
               PKG_PM_RULE.SP_CHK_PAT_RULE_ENR_ALL (
                  P_STUDY,
			   P_ORG_ID,
                  I.MILESTONE_COUNT,
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_DATE
               );
            END IF;
            IF (I.CODELST_SUBTYP = 'pm_4') THEN
               PKG_PM_RULE.SP_RULE_CHK_VIS_DONE_ONE (
                  P_STUDY,
			   P_ORG_ID,
                  I.MILESTONE_COUNT,
                  I.MILESTONE_VISIT,
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_DATE
               );

			--stalwar

               PKG_PM_RULE.SP_FORECAST_VIS_EVENT_DONE_ONE (
                  P_STUDY,
			   P_ORG_ID,
                  I.MILESTONE_COUNT,
                  I.MILESTONE_VISIT,
                  V_STDATE,
                  V_ENDDATE,
                  V_FORE_COUNT,
                  V_FORE_DATE
                 );
     --       p('sonika V_FORE_COUNT pm4 ' || V_FORE_COUNT);
       --      p('sonika V_FORE_date pm4 ' || V_FORE_DATE);

            END IF;

            IF (I.CODELST_SUBTYP = 'pm_5') THEN
               PKG_PM_RULE.SP_RULE_CHK_VIS_DONE_COMPLETE (
                  P_STUDY,
			   P_ORG_ID,
                  I.MILESTONE_COUNT,
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_DATE
               );

               PKG_PM_RULE.SP_FORECAST_VIS_EVENT_DONE_ONE (
                  P_STUDY,
			   P_ORG_ID,
                  I.MILESTONE_COUNT,
                  I.MILESTONE_VISIT,
                  V_STDATE,
                  V_ENDDATE,
                  V_FORE_COUNT,
                  V_FORE_DATE
                 );
            END IF;

            IF (V_COUNT = 1) THEN
               V_ACHIEVEMENTDATES.EXTEND;
               V_ACHIEVEMENTDATES (1) := TO_CHAR (V_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT);
            END IF;

            IF (V_FORE_COUNT = 1) THEN
               V_FORE_ACHIEVEDATES.EXTEND;
               V_FORE_ACHIEVEDATES (1) := TO_CHAR (V_FORE_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT);
            END IF;

         END IF;
         IF (I.MILESTONE_TYPE = 'EM') THEN
            IF (I.CODELST_SUBTYP = 'em_2') THEN
               PKG_EM_RULE.SP_EVENT_DONE_COUNT (
                  P_STUDY,
			   P_ORG_ID,
                  I.FK_CAL,
                  I.FK_EVENTASSOC,
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_ACHIEVEMENTDATES
               );

            --forecast
               PKG_EM_RULE.SP_FORE_EVENT_DONE_COUNT (
                  P_STUDY,
			   P_ORG_ID,
                  I.FK_CAL,
                  I.FK_EVENTASSOC,
                  V_STDATE,
                  V_ENDDATE,
                  V_FORE_COUNT,
                  V_FORE_ACHIEVEDATES
               );
            p('sonika V_FORE_COUNT em2 ' || V_FORE_COUNT);

            END IF;



--p('sp_eve_rule_crf_count V_ENDDATE ' ||V_ENDDATE);
            IF (I.CODELST_SUBTYP = 'em_3') THEN
               PKG_EM_RULE.SP_EVE_RULE_CRF_COUNT (
                  I.FK_EVENTASSOC,
                  'completed',
                  P_STUDY,
			   P_ORG_ID,
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_ACHIEVEMENTDATES
               );
            END IF;
            IF (I.CODELST_SUBTYP = 'em_4') THEN
               PKG_EM_RULE.SP_EVE_RULE_CRF_COUNT (
                  I.FK_EVENTASSOC,
                  'submitted',
                  P_STUDY,
			   P_ORG_ID,
                  V_STDATE,
                  V_ENDDATE,
                  V_COUNT,
                  V_ACHIEVEMENTDATES
               );
            END IF;
         END IF;

         FOR J IN 1 .. V_COUNT
         LOOP
            O_COUNT := O_COUNT + 1;
            O_ACHIVEMENT_DATE_ARRAY.EXTEND;
            O_MILESTONE_AMOUNT_ARRAY.EXTEND;
            O_MILESTONE_TYPE_ARRAY.EXTEND;
            O_MILESTONE_DESC_ARRAY.EXTEND;
            O_MILESTONE_RULE_ARRAY.EXTEND;
            O_ACHIVEMENT_DATE_ARRAY (O_COUNT) := V_ACHIEVEMENTDATES (J);
            O_MILESTONE_AMOUNT_ARRAY (O_COUNT) := I.MILESTONE_AMOUNT;
            O_MILESTONE_TYPE_ARRAY (O_COUNT) := I.MILESTONE_TYPE;
            O_MILESTONE_RULE_ARRAY (O_COUNT) := I.CODELST_DESC;

            IF (I.MILESTONE_TYPE = 'PM') THEN
               V_TEMPVAR := 'Patient Count ' || I.MILESTONE_COUNT;
            END IF;
            IF (I.MILESTONE_TYPE = 'VM') THEN
               V_TEMPVAR := 'Visit ' ||
                            I.MILESTONE_VISIT ||
                            ' (' ||
                            I.CAL_DESC ||
                            ')';
            END IF;
            IF (I.MILESTONE_TYPE = 'EM') THEN
               V_TEMPVAR := 'Event ' ||
                            I.EVENT_DESC ||
                            ' in Visit ' ||
                            I.MILESTONE_VISIT ||
                            ' (' ||
                            I.CAL_DESC ||
                            ')';
            END IF;
            O_MILESTONE_DESC_ARRAY (O_COUNT) := V_TEMPVAR;
         END LOOP;

         FOR J IN 1 .. V_FORE_COUNT
         LOOP
            O_FORE_COUNT := O_FORE_COUNT + 1;
            O_FORE_ACHIEVE_DATE_ARRAY.EXTEND;
            O_FORE_MILE_AMOUNT_ARRAY.EXTEND;
            O_FORE_MILE_TYPE_ARRAY.EXTEND;
            O_FORE_MILE_DESC_ARRAY.EXTEND;
            O_FORE_MILE_RULE_ARRAY.EXTEND;

            O_FORE_ACHIEVE_DATE_ARRAY (O_FORE_COUNT) := V_FORE_ACHIEVEDATES (J);
            O_FORE_MILE_AMOUNT_ARRAY (O_FORE_COUNT) := I.MILESTONE_AMOUNT;
            O_FORE_MILE_TYPE_ARRAY (O_FORE_COUNT) := I.MILESTONE_TYPE;
            O_FORE_MILE_RULE_ARRAY (O_FORE_COUNT) := I.CODELST_DESC;

            IF (I.MILESTONE_TYPE = 'PM') THEN
               V_TEMPVAR := 'Patient Count ' || I.MILESTONE_COUNT;
            END IF;
            IF (I.MILESTONE_TYPE = 'VM') THEN
               V_TEMPVAR := 'Visit ' ||
                            I.MILESTONE_VISIT ||
                            ' (' ||
                            I.CAL_DESC ||
                            ')';
            END IF;
            IF (I.MILESTONE_TYPE = 'EM') THEN
               V_TEMPVAR := 'Event ' ||
                            I.EVENT_DESC ||
                            ' in Visit ' ||
                            I.MILESTONE_VISIT ||
                            ' (' ||
                            I.CAL_DESC ||
                            ')';
            END IF;
            O_FORE_MILE_DESC_ARRAY (O_FORE_COUNT) := V_TEMPVAR;
         END LOOP;

      END LOOP; --MAIN




/**********************************
TEST
declare
O_ACHIVEMENT_DATE_ARRAY TYPES.SMALL_STRING_ARRAY;
O_MILESTONE_AMOUNT_ARRAY TYPES.SMALL_STRING_ARRAY;
O_MILESTONE_TYPE_ARRAY TYPES.SMALL_STRING_ARRAY;
O_MILESTONE_DESC_ARRAY   TYPES.SMALL_STRING_ARRAY;
O_MILESTONE_RULE_ARRAY TYPES.SMALL_STRING_ARRAY;
c number := 0;
begin
PKG_MILESTONE_REPORTS.SP_MILESTONE_STUDY_SPECIFIC(1839,'01/01/1900',
'01/01/3000', c, O_ACHIVEMENT_DATE_ARRAY, O_MILESTONE_AMOUNT_ARRAY, O_MILESTONE_TYPE_ARRAY, O_MILESTONE_DESC_ARRAY, O_MILESTONE_RULE_ARRAY);
dbms_output.put_line(c) ;
end ;
**************************************/
   END SP_MILEFORE_STUDY_SPECIFIC;






END PKG_MILE_FORECAST;
/


CREATE SYNONYM ESCH.PKG_MILE_FORECAST FOR PKG_MILE_FORECAST;


CREATE SYNONYM EPAT.PKG_MILE_FORECAST FOR PKG_MILE_FORECAST;


GRANT EXECUTE, DEBUG ON PKG_MILE_FORECAST TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_MILE_FORECAST TO ESCH;

