create or replace PACKAGE      "PKG_STORAGE" AS
/******************************************************************************
   NAME:       PKG_STORAGE
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        10/11/2007   Khader              1. Created this package for storage specific Pl/SQL.
******************************************************************************/

   v_id NUMBER;
   indx NUMBER := 1;
   cntr NUMBER := 1;
   x    NUMBER := 0;
   y    NUMBER := 0;

   v_childFetchedList ARRAY_STRING     :=  ARRAY_STRING();
   v_finalDelIdList   ARRAY_STRING     :=  ARRAY_STRING();
   childFetchFlag boolean := true;
   finalDelFlag boolean :=true;
   childFetchedFlag boolean := true;
   /* deletes the  child records recursively for the parent key passed*/
  PROCEDURE sp_delete_storages_parent_chld(p_ids ARRAY_STRING,del_flag in NUMBER, usr in number, o_ret OUT NUMBER);--KM
  /* deletes records in all parent and child tables based on the primary key */
  PROCEDURE SP_DELETE_STORAGES(p_ids ARRAY_STRING, usr in number, o_ret OUT NUMBER);
  /* get all the child and sub sub child storage ids*/
  PROCEDURE sp_find_all_child_storage_ids(storageId IN VARCHAR2, o_return OUT CLOB); --JM:28Dec2007
  /* to take required number of copies of storage unit/template  */
  PROCEDURE sp_copy_storages(strg_pks array_string,copy_counts array_string,strg_ids array_string, strg_names array_string, templates array_string, usr varchar2, ipAdd varchar2,o_ret OUT NUMBER); --KM:25Jan2008

  /* copies children / frandchildren of a storage. uses recursion */
  procedure sp_copy_child_storage(p_pk_storge_old Number,p_pk_storage_new Number,p_usr varchar2, p_ipAdd varchar2,o_ret_new OUT NUMBER);

  procedure sp_create_default_storagestat(p_pk_storge_old Number,p_pk_storage_new Number,p_usr varchar2, p_ipAdd varchar2,o_ret_new OUT NUMBER);

  /* copies allowed contents of a storage*/
  procedure sp_copy_allowed_contents(p_pk_storge_old Number,p_pk_storage_new Number,p_template_flag Number, p_usr varchar2, p_ipAdd varchar2,o_ret_new OUT NUMBER);

  /* calculate the number of children and follow the first child's children all the way down */
  procedure sp_get_firstborn_descendants(i_pk_storage IN Number,o_level OUT NUMBER,o_pk_down OUT VARCHAR2);

  /* procedure to be used recursively by sp_num_firstborn_desc */
  procedure sp_get_firstborn_recursive(i_pk_storage IN Number,o_next_pk OUT NUMBER,o_ret_number OUT NUMBER);
  
  /* Reset Dimensions and storage capacty of parent node whose all childs are being deleted*/
  /*-------------add by shyam sunder kushwaha on 14-Aug-2014 to resolve bug#19636 ----------*/
  procedure sp_reset_dim_capacity(parent_ids ARRAY_STRING,usr in number);


  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'Pkg_storage', pLEVEL  => Plog.LFATAL);

  END Pkg_Storage;

  /

  CREATE OR REPLACE SYNONYM ESCH.Pkg_Storage FOR Pkg_Storage;


  CREATE OR REPLACE SYNONYM EPAT.Pkg_Storage FOR Pkg_Storage;


  GRANT EXECUTE, DEBUG ON Pkg_Storage TO EPAT;

  GRANT EXECUTE, DEBUG ON Pkg_Storage TO ESCH;