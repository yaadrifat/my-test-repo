CREATE OR REPLACE PACKAGE BODY        "PKG_SPECIMEN" 
IS
 	/* Sonia Abrol  11/26/07
	creates a specimen for a form response, the procedure will be used from triggers so it will not have a 'commit'*/

	procedure SP_CREATE_FORMRESP_SPEC(p_form IN NUMBER,p_creator IN NUMBER,p_ipadd IN varchar2,p_per In Number,p_study IN NUMBER, o_SPECIMEN OUT NUMBER)
	AS
		v_disp_in_spec Number;
		v_fk_specimen Number;
		v_account Number;
		v_spec_type Number;
		v_status Number;
	BEGIN
		--check if the form is set to display in specimen management

		select nvl(lf_display_inspec,0), fk_Account
		into v_disp_in_spec , v_account
		from ER_LINKEDFORMS where fk_formlib = p_form;

		if (v_disp_in_spec = 1) then --create  a specimen

			begin
				select seq_er_specimen.nextval
				into v_fk_specimen
				from dual;

				select Pkg_Util.f_getcodepk('Cell','specimen_type')
				into v_spec_type
				from dual;


				select Pkg_Util.f_getcodepk('Collected','specimen_stat')
				into v_status
				from dual;

				Insert into ER_SPECIMEN
			   (PK_SPECIMEN, SPEC_ID, SPEC_DESCRIPTION, SPEC_TYPE, CREATOR, CREATED_ON, IP_ADD, FK_ACCOUNT,FK_PER,FK_STUDY,
			   spec_collection_date)
		 		Values (v_fk_specimen , seq_er_specimen_id.nextval, 'Created from form response',v_spec_type , p_creator, sysdate, p_ipadd,v_account,
		 		p_per,p_study,sysdate);

		 		Insert into ER_SPECIMEN_STATUS(pk_specimen_status,fk_specimen,ss_date,fk_codelst_status,creator,created_on,ip_add)
		 		values (seq_er_specimen_status.nextval,v_fk_specimen,sysdate,v_status,p_creator,sysdate,p_ipadd);

		 		--create specimen status
			exception when OTHERS then
				v_fk_specimen  := null;
			end ;
		else
				v_fk_specimen := null;

		end if;

		o_SPECIMEN := v_fk_specimen ;

	END;

	function f_get_specimen_info(p_pk_specimen Number) return Varchar
	is
	v_specid Varchar2(100);
	begin
		if (nvl(p_pk_specimen,0) = 0) then
			return '';
		end if;

		select spec_id
		into v_specid
		from er_specimen where pk_specimen = p_pk_specimen;

		return v_specid ;
	end;

END Pkg_Specimen;
/


