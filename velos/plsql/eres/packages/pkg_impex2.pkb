CREATE OR REPLACE PACKAGE BODY        "PKG_IMPEX2" AS

  PROCEDURE SP_EXP_ALLPATSTUDYFORMS (
      P_EXPID          NUMBER,
      O_RESFORM     OUT  Gk_Cv_Types.GenericCursorType,
      O_RES_MAPFORM     OUT  Gk_Cv_Types.GenericCursorType,
      O_RES_LINKFORM     OUT  Gk_Cv_Types.GenericCursorType,
	  O_FORMSEC     OUT  Gk_Cv_Types.GenericCursorType,
	  O_FORMFLD     OUT  Gk_Cv_Types.GenericCursorType,
	  O_REPFORMFLD OUT  Gk_Cv_Types.GenericCursorType,
	  O_FLDRESP OUT  Gk_Cv_Types.GenericCursorType,
	  O_FLDVALIDATE OUT  Gk_Cv_Types.GenericCursorType,
	  O_REPFLDVALIDATE OUT  Gk_Cv_Types.GenericCursorType,
	  O_RES_FORMLIBVER OUT  Gk_Cv_Types.GenericCursorType,
	  O_RES_FLDACTION OUT  Gk_Cv_Types.GenericCursorType,
  	  O_RES_FLDACTIONINFO OUT  Gk_Cv_Types.GenericCursorType
  	 )
	 /* Author: Sonia Sahni
	    Date: 29rd April 2004
		Purpose - Returns a ref cursors for all patient study forms data (not linked to any specific study)
     */
   AS

	v_account VARCHAR2(4000);

	v_active_formstat NUMBER;

	v_form_ids LONG;
	v_form_count NUMBER;

	v_sql_formlib LONG;
	v_sql_mapform LONG;
	v_sql_linkform LONG;
	v_sql_mapform_count VARCHAR2(4000);
	v_sql_form_refresh LONG;

	v_sql_form_select VARCHAR2(4000);
	v_sql_mapform_select VARCHAR2(4000);
	v_sql_linkform_select VARCHAR2(4000);
	v_sql_form_refresh_select VARCHAR2(4000);

	v_sql_mapform_count_select VARCHAR2(4000);



	v_missingmap_cur Gk_Cv_Types.GenericCursorType;
	v_refreshform_cur Gk_Cv_Types.GenericCursorType;
	V_FORMPK_CUR Gk_Cv_Types.GenericCursorType;

	v_formpk_list LONG;
	v_formpk NUMBER;

	v_missingform NUMBER;
	v_refreshform NUMBER;
	v_refreshformclob CLOB;

	v_formtype CHAR(2);
	v_success NUMBER;

	v_expdetails CLOB;
	v_modkeys TYPES.SMALL_STRING_ARRAY := NEW TYPES.SMALL_STRING_ARRAY();

	v_formpk_select LONG;
	v_formpk_sql LONG;

	V_FORMSEC     LONG;
	V_FORMFLD     LONG;
	V_REPFORMFLD LONG;
	V_FLDRESP LONG;
	V_FLDVALIDATE LONG;
	V_REPFLDVALIDATE LONG;
	V_FLDACTION LONG;
	V_FLDACTIONINFO LONG;

	v_sql_formlib_ver_select LONG;
	v_sql_formlib_ver  LONG;


    BEGIN
 		 --get export request

		 Pkg_Impex.SP_GET_REQDETAILS(P_EXPID ,v_expdetails);

		 --find out the study id from the export request


		 Pkg_Impexcommon.SP_GETMODULEKEYS(sys.XMLTYPE.createXML(v_expdetails),'sp_patform',v_modkeys,v_success );

		 -- find out versions to be exported

		 v_form_count := v_modkeys.COUNT();

		 IF v_form_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_form_ids);
		 END IF;


		 -- get v_account from the export request

   		 Pkg_Impexcommon.SP_GETELEMENT_VALUES (v_expdetails,'account',v_modkeys);
		 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_account);


		 -- find out formsto be exported


		 --get 'Aative' Status'for form

		 SELECT pk_codelst
		 INTO v_active_formstat
		 FROM ER_CODELST
		 WHERE trim(codelst_type) = 'frmstat' AND trim(codelst_subtyp) = 'A';



		 -- generate form mapping if it does not exist


		 v_sql_form_select := ' Select e.pk_formlib pk_formlib, e.fk_catlib fk_catlib,e.form_name form_name, e.form_desc form_desc,''A'' form_status_subtype, e.form_linkto form_linkto,
		 				   	    e.form_xsl form_xsl, e.form_xslrefresh form_xslrefresh, e.form_viewxsl form_viewxsl, e.form_xml.getClobVal() form_xml, e.form_sharedwith,
								e.FORM_CUSTOM_JS, e.FORM_KEYWORD ,e.FORM_ACTIVATION_JS ';

		 v_sql_mapform_select:= ' Select pk_mp , fk_form , mp_formtype , mp_mapcolname , mp_systemid , mp_keyword , mp_uid ,
		 						mp_dispname , mp_origsysid,
		 						mp_flddatatype , mp_browser , mp_sequence';

		 v_sql_linkform_select := 'Select pk_lf, fk_formlib, lf_displaytype , lf_entrychar , er_linkedforms.fk_account fk_account, er_linkedforms.fk_study fk_study,
		 					    lf_lnkfrom , fk_calendar , fk_event , fk_crf,
								LF_DATACNT, LF_HIDE, LF_SEQ, LF_DISPLAY_INPAT ';

    	 --v_sql_mapform_count_select := 'Select pk_formlib, LF_DISPLAYTYPE ';

		 --v_sql_form_refresh_select := 'Select pk_formlib ' ;

		 v_formpk_select := 'Select pk_formlib ' ;

		 v_sql_formlib_ver_select := 'Select a.PK_FORMLIBVER,  a.FK_FORMLIB, a.FORMLIBVER_NUMBER,  a.FORMLIBVER_NOTES,    a.FORMLIBVER_DATE,
							 a.FORMLIBVER_XML.getClobVal() FORMLIBVER_XML, a.FORMLIBVER_XSL,  a.FORMLIBVER_VIEWXSL ';


		 IF v_form_count > 1 THEN

		    v_sql_formlib := v_sql_form_select || ' from er_formlib e where e.pk_formlib in (' || v_form_ids || ')';

			v_sql_linkform := v_sql_linkform_select || ' from er_linkedforms where fk_formlib in (' || v_form_ids || ')';

			v_sql_mapform := v_sql_mapform_select || ' from er_mapform where fk_form in (' || v_form_ids || ')';

			/*v_sql_mapform_count := v_sql_mapform_count_select || ' from er_formlib Where
			pk_formlib in (' || v_form_ids || ') and not exists (select 1 from er_mapform where
					   fk_form = pk_formlib)';

			v_sql_form_refresh  := v_sql_form_refresh_select || ' from er_formlib e where e.pk_formlib in (' || v_form_ids || ') and FORM_XSLREFRESH = 1'; */

			v_sql_formlib_ver := v_sql_formlib_ver_select || ' from er_formlibver a, (select max(i.pk_formlibver) pk , i.fk_formlib from er_formlibver i  where i.fk_FORMLIB in (' || v_form_ids || ') group by fk_formlib) ids where a.fk_FORMLIB in (' || v_form_ids || ') and pk_formlibver = ids.pk' ;

		 ELSIF v_form_count = 1 THEN

		    v_sql_formlib := v_sql_form_select || ' from er_formlib e where e.pk_formlib = ' || v_form_ids ;

 		    v_sql_linkform := v_sql_linkform_select || ' from er_linkedforms where fk_formlib = ' || v_form_ids ;

			v_sql_mapform := v_sql_mapform_select || ' from er_mapform where fk_form = '|| v_form_ids ;

			v_sql_formlib_ver := v_sql_formlib_ver_select || ' from er_formlibver a where a.fk_FORMLIB = '|| v_form_ids || ' and pk_formlibver = ( select max(pk_formlibver) from er_formlibver where fk_FORMLIB = '|| v_form_ids ||  ') ';

			/*v_sql_mapform_count := v_sql_mapform_count_select || ' from er_formlib Where
			pk_formlib = ' || v_form_ids || ' and not exists (select 1 from er_mapform where
					   fk_form = pk_formlib)';

		   v_sql_form_refresh  := v_sql_form_refresh_select || ' from er_formlib e where e.pk_formlib = ' || v_form_ids || ' and FORM_XSLREFRESH = 1'; */

		 ELSIF v_form_count = 0 THEN	--get all sorts study forms

		    v_sql_formlib := v_sql_form_select || ' from er_formlib e , er_linkedforms	 where e.FK_ACCOUNT = '
			                 || v_account || ' and fk_formlib = e.pk_formlib and (LF_DISPLAYTYPE = ''PS'' OR  LF_DISPLAYTYPE = ''PR'')  and  nvl(er_linkedforms.record_type,''N'') <> ''D''
						 AND	 e.FORM_STATUS  = ' || v_active_formstat  ;


  		    v_sql_linkform := v_sql_linkform_select || ' from er_formlib e ,er_linkedforms  where
				   e.FK_ACCOUNT = '  || v_account || ' AND fk_formlib = e.pk_formlib AND (LF_DISPLAYTYPE = ''PS'' OR  LF_DISPLAYTYPE = ''PR'')  AND  NVL(ER_LINKEDFORMS.record_type,''N'') <> ''D''
						 AND	 e.FORM_STATUS  = ' || v_active_formstat ;

			v_sql_mapform := v_sql_mapform_select || ' from er_formlib e ,er_mapform, er_linkedforms  where e.FK_ACCOUNT = '
						  || v_account || ' and fk_formlib = e.pk_formlib and (LF_DISPLAYTYPE = ''PS'' OR LF_DISPLAYTYPE = ''PR'')  and  nvl(er_linkedforms.record_type,''N'') <> ''D''
						 AND	 e.FORM_STATUS  = ' || v_active_formstat || ' AND   fk_formlib = fk_form ';

			/*v_sql_mapform_count := v_sql_mapform_count_select || ' from er_formlib, er_linkedforms
				where (( er_linkedforms.fk_study = ' || v_study || ' and LF_DISPLAYTYPE in (''S'',''SP'')) OR
				 (LF_DISPLAYTYPE in (''SA'',''PS'') and er_linkedforms.fk_account = ' || v_account || ')) AND
				  fk_formlib = pk_formlib and nvl(er_formlib.record_type,''N'') <> ''D'' and FORM_STATUS  = ' || v_active_formstat || ' and not exists (select 1 from er_mapform where
					   fk_form = pk_formlib)';

			v_sql_form_refresh  := v_sql_form_refresh_select || ' from er_formlib e , er_linkedforms  where (( er_linkedforms.fk_study = ' || v_study
				|| ' and LF_DISPLAYTYPE in (''S'',''SP'')) OR
				 (LF_DISPLAYTYPE in (''SA'',''PS'') and er_linkedforms.fk_account = ' || v_account || ')) AND
				  fk_formlib = e.pk_formlib and e.FORM_STATUS  = ' || v_active_formstat ||
				  ' and e.FORM_XSLREFRESH = 1 and nvl(e.record_type,''N'') <> ''D''' ; */

			v_formpk_sql := v_formpk_select || 	' from er_formlib e , er_linkedforms  where  e.FK_ACCOUNT = '
			                 || v_account || ' and fk_formlib = e.pk_formlib and (LF_DISPLAYTYPE = ''PS'' OR LF_DISPLAYTYPE = ''PR'' )  and  nvl(er_linkedforms.record_type,''N'') <> ''D''
						 AND e.FORM_STATUS  = ' || v_active_formstat;

			 v_sql_formlib_ver := v_sql_formlib_ver_select || ' from  er_formlibver a  ,  (select max(pk_formlibver) pk, a.fk_formlib  FROM ER_FORMLIBVER a, ER_FORMLIB e , ER_LINKEDFORMS
				WHERE e.FK_ACCOUNT = ' || v_account || ' AND ER_LINKEDFORMS.fk_formlib = e.pk_formlib AND
				 (LF_DISPLAYTYPE = ''PS'' OR LF_DISPLAYTYPE = ''PR'')  AND   e.FORM_STATUS  = ' || v_active_formstat || ' AND
				 NVL(ER_LINKEDFORMS.record_type,''N'') <> ''D'' AND   ER_LINKEDFORMS.fk_formlib = a.fk_FORMLIB  GROUP BY a.fk_formlib)  dat WHERE a.pk_formlibver = dat.pk  ';

		 END IF;

		 -- generate data for er_mapform for forms that do not have this data ready
		 Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> PKG_IMPEX2.SP_EXP_ALLPATSTUDYFORMS : v_sql_formlib_ver' ||  v_sql_formlib_ver, v_success );
		 Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> PKG_IMPEX2.SP_EXP_ALLPATSTUDYFORMS : v_sql_mapform_count' || v_sql_mapform_count , v_success );

		 --execute

    	 OPEN O_RESFORM FOR v_sql_formlib ;
		 OPEN O_RES_MAPFORM FOR v_sql_mapform ;
 		 OPEN O_RES_LINKFORM FOR v_sql_linkform ;
 		 OPEN O_RES_FORMLIBVER FOR v_sql_formlib_ver;


		 -- execute the form id sql to export form data

		 IF v_form_count = 0 THEN
			 OPEN V_FORMPK_CUR FOR v_formpk_sql;
			 LOOP
			 	 FETCH V_FORMPK_CUR INTO v_formpk;
				 EXIT WHEN V_FORMPK_CUR %NOTFOUND;
				  	 v_formpk_list := v_formpk_list || ',' || v_formpk;
			 END LOOP;

			 v_formpk_list := SUBSTR(v_formpk_list,2);

			 CLOSE V_FORMPK_CUR;
		ELSE
			 v_formpk_list := v_form_ids;

		END IF;

		 -- get SQLs for the form field data

	 	Pkg_Impexforms.SP_GET_FORMFLD_EXP_SQLS(v_formpk_list,V_FORMSEC ,V_FORMFLD,V_REPFORMFLD ,V_FLDRESP ,V_FLDVALIDATE ,
			V_REPFLDVALIDATE,V_FLDACTION , V_FLDACTIONINFO  );

		 --execute

		  OPEN O_FORMSEC  FOR  V_FORMSEC;
	      OPEN O_FORMFLD     FOR  V_FORMFLD;
		  OPEN O_REPFORMFLD FOR V_REPFORMFLD;
	  	  OPEN O_FLDRESP FOR V_FLDRESP;
	  	  OPEN O_FLDVALIDATE FOR V_FLDVALIDATE;
	  	  OPEN O_REPFLDVALIDATE FOR V_REPFLDVALIDATE;
		  OPEN O_RES_FLDACTION FOR V_FLDACTION;
		  OPEN O_RES_FLDACTIONINFO FOR V_FLDACTIONINFO;


    END SP_EXP_ALLPATSTUDYFORMS;

------------------------------------------------------------------

 PROCEDURE SP_IMP_ALLPATSTUDYFORMS(P_IMPID NUMBER, O_SUCCESS OUT NUMBER)
  AS
  /** Author : Sonia Sahni
  	  Date: 04/29/04
	  Purpose: Import patient study forms data from temp tables
	  Note:
  */

  v_form_cur Gk_Cv_Types.GenericCursorType;
 -- v_mapform_cur gk_cv_types.GenericCursorType;
  v_lf_cur Gk_Cv_Types.GenericCursorType;
  v_formlibver_cur Gk_Cv_Types.GenericCursorType;

  v_modkeys TYPES.SMALL_STRING_ARRAY := NEW TYPES.SMALL_STRING_ARRAY();

  v_orig_form NUMBER;
  v_new_form NUMBER;

  v_formname VARCHAR2(50);
  v_formdesc VARCHAR2(255);
  v_formstatus NUMBER;
  v_codelst_formstat VARCHAR2(15);
  v_formlinkto CHAR(1);
  v_formxsl CLOB;
  v_form_xslrefresh NUMBER;
  v_form_viewxsl CLOB;
  v_formxml CLOB;
  v_sharedwith CHAR(1);

  v_orig_mapform NUMBER;
  v_new_mapform NUMBER;
  v_mpformtype  VARCHAR2(2);

  v_mapcolname VARCHAR2(100);
  v_mpsystemid  VARCHAR2(100);
  v_mpkeyword VARCHAR2(255);
  v_mpuid VARCHAR2(100);
  v_mpdispname VARCHAR2(500);
  v_mporigsysid VARCHAR2(100);
  v_mpflddatatype VARCHAR2(2);
  v_mpbrowser VARCHAR2(2);
  v_mpsequence NUMBER;



  v_err VARCHAR2(4000);

  v_form_sql LONG;
--  v_mapform_sql Long;
  v_lf_sql LONG;

  v_success NUMBER;

  V_EXPID NUMBER;
  V_expdatils CLOB;

  v_sitecode VARCHAR2(10);
  v_originating_sitecode VARCHAR2(10);
  v_count NUMBER;

  v_creator VARCHAR2(50);
  v_ipAdd VARCHAR2(15);

  v_orig_lf NUMBER;
  v_new_lf NUMBER;

  v_lfdisplaytype CHAR(2);
  v_lfentrychar CHAR(1);
  v_lfaccount NUMBER;
  v_lffkstudy NUMBER;
  v_lflnkfrom CHAR(1);


  v_account VARCHAR2(100);

  o_out_field NUMBER;
  o_out_formfld NUMBER;

  v_form_xmltype sys.XMLTYPE;

  v_formlibver_sql LONG;
  v_orig_formlibver NUMBER;
  v_new_formlibver NUMBER;
  v_formlibver_number VARCHAR2(25);
  v_formlibver_notes VARCHAR2(2000);
  v_formlibver_date DATE;
  v_formlibver_xml sys.XMLTYPE;
  v_formlibver_xmlclob CLOB;
  v_formlibver_xsl CLOB;
  v_formlibver_viewxsl CLOB;

  v_form_keyword VARCHAR2(50);
  v_form_custom_js CLOB;
  v_form_activation_js CLOB;

  v_lf_datacnt NUMBER;
  v_lf_hide NUMBER;
  v_lf_seq NUMBER;
  v_lf_display_inpat NUMBER;
  v_is_library_form Number := 0;

  v_importedforms TYPES.SMALL_STRING_ARRAY := NEW TYPES.SMALL_STRING_ARRAY();

  BEGIN
  	   -- get import request details

	   Pkg_Impex.SP_GET_IMPREQDETAILS(P_IMPID, V_expdatils,v_expid);
	   Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'targetSite',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_sitecode);
		END IF;


		--get originating site_code

		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'originatingSiteCode',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_originating_sitecode);
		END IF;

		--get creator

		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'impUserId',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,	v_creator);
		END IF;

		-- get ipAdd


		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'impIpAdd',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,	v_ipAdd);
		END IF;

		--get import account

		Pkg_Impexcommon.SP_GETELEMENT_VALUES (V_expdatils,'impAccountId',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_account);
		END IF;


		-- get data from the form temp table


		v_form_sql := 'Select   PK_FORMLIB, FORM_NAME , FORM_DESC , FORM_LINKTO,
				    FORM_XSL, FORM_XSLREFRESH, FORM_VIEWXSL , FORM_XML, form_status_subtype, form_sharedwith,
					FORM_CUSTOM_JS, FORM_KEYWORD ,FORM_ACTIVATION_JS
		 	      FROM IMPSP_PATFORM0';



		v_lf_sql := ' Select PK_LF , FK_FORMLIB, LF_DISPLAYTYPE, LF_ENTRYCHAR, FK_ACCOUNT, FK_STUDY,
 				 	 LF_LNKFROM,
					 LF_DATACNT, LF_HIDE, LF_SEQ, LF_DISPLAY_INPAT
					 FROM IMPSP_PATFORM2';

		v_formlibver_sql := 'Select PK_FORMLIBVER,  FK_FORMLIB, FORMLIBVER_NUMBER,  FORMLIBVER_NOTES,
					 FORMLIBVER_DATE, FORMLIBVER_XML , FORMLIBVER_XSL,  FORMLIBVER_VIEWXSL 	FROM IMPSP_PATFORM9';

		-- import form

	 OPEN v_form_cur FOR v_form_sql;
	 	 LOOP

		 	 FETCH v_form_cur INTO  v_orig_form,v_formname,v_formdesc, v_formlinkto,v_formxsl,
				  	v_form_xslrefresh, v_form_viewxsl, v_formxml ,v_codelst_formstat, v_sharedwith,
					v_form_custom_js,v_form_keyword,v_form_activation_js ;

			 EXIT WHEN v_form_cur %NOTFOUND;

 			 SELECT	Pkg_Impex.getCodePk (v_codelst_formstat, 'frmstat')
			 INTO v_formstatus
			 FROM dual;

			 if (v_formlinkto = 'L') then
			 	v_is_library_form := 1;
			 end if;

			 IF v_formstatus = -1 THEN
			 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX2.SP_IMP_ALLPATSTUDYFORMS - Code not found for codelst_type : frmstat, codelst_subtyp : ' || v_codelst_formstat, v_success );
				v_formstatus  := NULL;
			 END IF;



			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_form, 'er_formlib',v_sitecode, v_new_form,v_err,v_originating_sitecode);

			-- if v_orig_form <= 0 then
			  --	  PKG_IMPEXLOGGING.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX2.SP_IMP_STUDYVER Could not create study version, parent Study not found. original PK_STUDY : ' || v_orig_study, v_success );
			 --else

			  IF v_new_form = 0 THEN -- new record

					 SELECT seq_er_formlib.NEXTVAL
						INTO v_new_form	FROM dual;

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX2.SP_IMP_ALLPATSTUDYFORMS - New Form record with original PK:' || v_orig_form , v_success );

						Pkg_Impexcommon.SP_SET_SITEPK(v_orig_form, 'er_formlib',v_sitecode,  v_new_form,v_err,v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX2.SP_IMP_ALLPATSTUDYFORMS Could not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN

							v_form_xmltype := NULL;

							BEGIN
								IF v_formxml IS NOT NULL THEN
							   	   SELECT XMLTYPE(v_formxml)
								   INTO v_form_xmltype FROM dual;
							    END IF;
							EXCEPTION WHEN OTHERS THEN
								 v_form_xmltype := NULL;
							END;

							 INSERT INTO ER_FORMLIB ( PK_FORMLIB , FORM_NAME ,  FORM_DESC,
            				 			 			 FORM_LINKTO,  FORM_XSL ,
													 FORM_XSLREFRESH , FORM_XML,
													 CREATOR, CREATED_ON,IP_ADD, FORM_STATUS, FK_ACCOUNT, RECORD_TYPE,
													 FORM_SHAREDWITH ,FORM_CUSTOM_JS,
													 FORM_KEYWORD, FORM_ACTIVATION_JS )
							 VALUES (v_new_form,v_formname,v_formdesc, v_formlinkto,v_formxsl,
							 	  	1, v_form_xmltype ,
								    v_creator, SYSDATE,v_ipAdd , v_formstatus, v_account,'N',v_sharedwith,
									v_form_custom_js,v_form_keyword,v_form_activation_js) ;

							 INSERT INTO ER_FORMSTAT(pk_formstat,fk_formlib,fk_codelst_stat,formstat_stdate,
							 formstat_changedby,record_type,creator,created_on,ip_add)
							 VALUES(seq_er_formstat.NEXTVAL,v_new_form, v_formstatus,SYSDATE,v_creator,'N',v_creator,
							 SYSDATE,v_ipAdd );

							 if v_is_library_form  = 1 then -- generate librray share with data for all acc users
							 	 Sp_Objectsharewith( v_new_form,'1', TO_CHAR(v_account)  ,'A', TO_CHAR(v_creator) , v_ipAdd , v_success );
							 end if;

							  --add the new form id to v_importedforms array, these forms will be processed for new xsl,xmls, versions, etc
							 v_importedforms.EXTEND;
							 v_importedforms(v_importedforms.LAST) := v_new_form;



							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX2.SP_IMP_ALLPATSTUDYFORMS:Could not create Form record for Sponsor PK ' || v_orig_form || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					 ELSIF v_new_form > 0 THEN -- old record

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX2.SP_IMP_ALLPATSTUDYFORMS - Old Form record with original PK:' || v_orig_form, v_success );

						v_form_xmltype := NULL;

						BEGIN
							IF v_formxml IS NOT NULL THEN
						   	   SELECT XMLTYPE(v_formxml)
							   INTO v_form_xmltype FROM dual;
						    END IF;
						EXCEPTION WHEN OTHERS THEN
								 v_form_xmltype := NULL;
						END;



						UPDATE ER_FORMLIB
						SET FORM_NAME = v_formname,  FORM_DESC = v_formdesc, FORM_LINKTO = v_formlinkto,
						  FORM_XSL = v_formxsl , FORM_XSLREFRESH = 1,
						   FORM_VIEWXSL = NULL , FORM_XML =  v_form_xmltype ,
							LAST_MODIFIED_BY = v_creator ,
							LAST_MODIFIED_DATE = SYSDATE ,IP_ADD = v_ipAdd,
							FORM_STATUS = v_formstatus ,FK_ACCOUNT = v_account, RECORD_TYPE = 'M',
							FORM_SHAREDWITH = v_sharedwith ,FORM_CUSTOM_JS = v_form_custom_js,
							 FORM_KEYWORD = v_form_keyword, FORM_ACTIVATION_JS = v_form_activation_js
					    WHERE pk_formlib = v_new_form;

					    if v_is_library_form  = 1 then -- generate librray share with data for all acc users
							 	 Sp_Objectsharewith( v_new_form,'1', TO_CHAR(v_account)  ,'A', TO_CHAR(v_creator) , v_ipAdd , v_success );
						end if;

						--add the new form id to v_importedforms array, these forms will be processed for new xsl,xmls, versions, etc
						 v_importedforms.EXTEND;
						 v_importedforms(v_importedforms.LAST) := v_new_form;


					 ELSIF v_new_form = -1 THEN

					  	 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX2.SP_IMP_ALLPATSTUDYFORMS:' || v_err, v_success );

					END IF;

		 END LOOP;

	 CLOSE v_form_cur ;

	 -- import Linked forms records

	  	 OPEN v_lf_cur FOR v_lf_sql;

		 LOOP
		 	 FETCH v_lf_cur INTO v_orig_lf,v_orig_form, v_lfdisplaytype, v_lfentrychar, v_lfaccount, v_lffkstudy,
			  v_lflnkfrom , v_lf_datacnt ,  v_lf_hide ,  v_lf_seq, v_lf_display_inpat;

			 EXIT WHEN v_lf_cur  %NOTFOUND;

 			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_lf, 'er_linkedforms',v_sitecode, v_new_lf,v_err,v_originating_sitecode);
			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_form, 'er_formlib',v_sitecode, v_new_form,v_err,v_originating_sitecode);


			 IF LENGTH(trim(NVL(v_lfaccount,''))) > 0 THEN
			 	v_lfaccount := v_account;
			 ELSE
			 	 v_lfaccount := NULL;
			 END IF;



			 IF v_new_form <= 0 THEN
			  	  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX2.SP_IMP_ALLPATSTUDYFORMS Could not create linked form record, parent Form not found. original PK_FORMLIB : ' || v_orig_form, v_success );

			 ELSE

			 	 IF v_new_lf = 0 THEN -- new record
		  		 	 SELECT seq_er_linkedforms.NEXTVAL
						INTO v_new_lf	FROM dual;

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX2.SP_IMP_ALLPATSTUDYFORMS - New Linked Forms record with original PK:' || v_orig_lf, v_success );

						Pkg_Impexcommon.SP_SET_SITEPK(v_orig_lf, 'er_linkedforms',v_sitecode,  v_new_lf,v_err,v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX2.SP_IMP_ALLPATSTUDYFORMS Could not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN

							 --PKG_IMPEXLOGGING.SP_RECORDLOG (V_EXPID,'FATAL','DATA','IMPEX--> PKG_IMPEX.SP_IMP_FORMS before insert v_new_study' || v_new_study, v_success );

							 INSERT INTO ER_LINKEDFORMS (PK_LF , FK_FORMLIB, LF_DISPLAYTYPE, LF_ENTRYCHAR, FK_ACCOUNT,
							 LF_LNKFROM ,  CREATOR , CREATED_ON , IP_ADD , RECORD_TYPE ,
							  LF_DATACNT, LF_HIDE, LF_SEQ, LF_DISPLAY_INPAT )
							 VALUES (v_new_lf, v_new_form, v_lfdisplaytype, v_lfentrychar, v_lfaccount ,
					 			  v_lflnkfrom,  v_creator, SYSDATE,v_ipAdd,'N',
								  v_lf_datacnt ,  v_lf_hide ,  v_lf_seq, v_lf_display_inpat ) ;

							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX2.SP_IMP_ALLPATSTUDYFORMS:Could not create Linked Forms record for Sponsor PK ' || v_orig_lf || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					 ELSIF v_new_lf > 0 THEN -- old record

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX2.SP_IMP_ALLPATSTUDYFORMS - Old Linked Forms record with original PK:' || v_orig_lf , v_success );

						UPDATE ER_LINKEDFORMS
						SET LF_DISPLAYTYPE = v_lfdisplaytype, LF_ENTRYCHAR =  v_lfentrychar, FK_ACCOUNT =  v_lfaccount ,
							 LF_LNKFROM = v_lflnkfrom,
						LAST_MODIFIED_BY = v_creator , LAST_MODIFIED_DATE = SYSDATE,IP_ADD = v_ipAdd,
						 RECORD_TYPE = 'M' , LF_DATACNT = v_lf_datacnt, LF_HIDE = v_lf_hide , LF_SEQ = v_lf_seq,
						  LF_DISPLAY_INPAT = v_lf_display_inpat
					    WHERE pk_lf  = v_new_lf;

					 ELSIF v_new_lf = -1 THEN

					  	 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX2.SP_IMP_ALLPATSTUDYFORMS:' || v_err, v_success );

					 END IF;
			END IF; -- for parent study, forms
		 END LOOP;

	 CLOSE v_lf_cur;

	 OPEN v_formlibver_cur FOR v_formlibver_sql;
		 LOOP

		 	 FETCH v_formlibver_cur INTO v_orig_formlibver,v_orig_form,v_formlibver_number, v_formlibver_notes, v_formlibver_date,
			 v_formlibver_xmlclob, v_formlibver_xsl,v_formlibver_viewxsl	;

			 EXIT WHEN v_formlibver_cur  %NOTFOUND;

 			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_formlibver, 'er_formlibver',v_sitecode, v_new_formlibver,v_err,v_originating_sitecode);
			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_form, 'er_formlib',v_sitecode, v_new_form,v_err,v_originating_sitecode);

		--	 PKG_IMPEXLOGGING.SP_RECORDLOG (V_EXPID,'FATAL','DATA','IMPEX--> PKG_IMPEX2.SP_IMP_FORMS v_lffkstudy' || v_lffkstudy, v_success );

			 IF v_new_form <= 0 THEN
			  	  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','FK MISSING','IMPEX--> PKG_IMPEX2.SP_IMP_ALLPATSTUDYFORMS Could not create formlib version record, parent Form not found. original PK_FORMLIB : ' || v_orig_form, v_success );
			 ELSE

				--prepare xmltype

				v_formlibver_xml := NULL;

					BEGIN
						IF v_formlibver_xmlclob IS NOT NULL THEN
				   	       SELECT XMLTYPE(v_formlibver_xmlclob)
						    INTO v_formlibver_xml FROM dual;
					    END IF;
					EXCEPTION WHEN OTHERS THEN
						  v_formlibver_xml := NULL;
					END;


			 	 IF v_new_formlibver = 0 THEN -- new record

		  		 	 SELECT seq_er_formlibver.NEXTVAL
						INTO v_new_formlibver FROM dual;

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX2.SP_IMP_ALLPATSTUDYFORMS - New Formlib version record with original PK:' || v_orig_formlibver, v_success );

						Pkg_Impexcommon.SP_SET_SITEPK(v_orig_formlibver, 'er_formlibver',v_sitecode,  v_new_formlibver,v_err,v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX2.ALLPATSTUDYFORMS Could not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN


							  INSERT INTO ER_FORMLIBVER (PK_FORMLIBVER,  FK_FORMLIB,  FORMLIBVER_NUMBER,  FORMLIBVER_NOTES,
							  FORMLIBVER_DATE,  FORMLIBVER_XML,   FORMLIBVER_XSL,   FORMLIBVER_VIEWXSL,
							  CREATOR, RECORD_TYPE, CREATED_ON ,IP_ADD )
							 VALUES (v_new_formlibver,v_new_form, v_formlibver_number, v_formlibver_notes, v_formlibver_date,
							 v_formlibver_xml,v_formlibver_xsl,v_formlibver_viewxsl,
					 			 v_creator, 'N' ,SYSDATE,v_ipAdd) ;

							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX2.ALLPATSTUDYFORMS:Could not create FormLib ver record for Sponsor PK ' || v_orig_formlibver || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					 ELSIF v_new_formlibver > 0 THEN -- old record

					 	Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'debug','MSG','IMPEX--> PKG_IMPEX2.ALLPATSTUDYFORMS - Old Formlib ver record with original PK:' || v_orig_formlibver , v_success );

						UPDATE ER_FORMLIBVER
						SET  FK_FORMLIB = v_new_form,  FORMLIBVER_NUMBER = v_formlibver_number ,  FORMLIBVER_NOTES =  v_formlibver_notes,
							  FORMLIBVER_DATE = v_formlibver_date,  FORMLIBVER_XML = v_formlibver_xml,
							  FORMLIBVER_XSL = v_formlibver_xsl,   FORMLIBVER_VIEWXSL = v_formlibver_viewxsl,
							  RECORD_TYPE = 'M', LAST_MODIFIED_BY = v_creator , LAST_MODIFIED_DATE = SYSDATE,IP_ADD = v_ipAdd
						WHERE PK_FORMLIBVER = v_new_formlibver;

					 ELSIF v_new_formlibver = -1 THEN

					  	 Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX2.ALLPATSTUDYFORMS:' || v_err, v_success );

					 END IF;
			END IF; -- for parent study, forms
		 END LOOP;

	 CLOSE v_formlibver_cur;

	 ----------------


	 -- import form fields

	  V_ERR := '';

	  Pkg_Impexforms.SP_IMP_FORMFLDS('sp_patform',V_EXPID,V_expdatils, V_ERR , O_SUCCESS);

	  IF LENGTH(trim(v_err)) > 0 THEN
		  Pkg_Impexlogging.SP_RECORDLOG (V_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEX2.SP_IMP_ALLPATSTUDYFORMS Could not import form fields data :' || v_err, v_success );
		  END IF;

	  -- after complete forms import, regenerate mpform,xsls, xmls for each form
	  -- sonia, 12/22/04
	  if (v_is_library_form <> 1) then
	  	Pkg_Impexforms.sp_regen_formstruct(V_EXPID, v_importedforms,'PKG_IMPEX2.SP_IMP_ALLPATSTUDYFORMS');
	  end if;


	COMMIT;

  END SP_IMP_ALLPATSTUDYFORMS;

END Pkg_Impex2;
--------------------------------------------------------------------
/


CREATE SYNONYM ESCH.PKG_IMPEX2 FOR PKG_IMPEX2;


CREATE SYNONYM EPAT.PKG_IMPEX2 FOR PKG_IMPEX2;


GRANT EXECUTE, DEBUG ON PKG_IMPEX2 TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_IMPEX2 TO ESCH;

