CREATE OR REPLACE PACKAGE        "PKG_ADV" IS

  PROCEDURE SP_CALC_TOXICITY(p_toxicity_group NUMBER, p_labdate VARCHAR2, p_unit VARCHAR2,
        p_labsite NUMBER, p_result VARCHAR2, p_lln NUMBER,p_uln NUMBER,p_usegivenrange NUMBER, p_nci_ver VARCHAR2, p_patient NUMBER,  o_grade OUT NUMBER, o_ulnused OUT NUMBER, o_llnused OUT  NUMBER) ;


  PROCEDURE SP_GET_NCILIB(p_nci_ver VARCHAR2 , o_lib OUT gk_cv_types.GenericCursorType);

  PROCEDURE SP_GET_TOXICITY_GRADE_LIST(p_nci_ver VARCHAR2 , p_toxicity_group NUMBER , o_lib OUT gk_cv_types.GenericCursorType);

FUNCTION   f_get_adv_subtypes(p_subtype VARCHAR2) RETURN  split_tbl;

END PKG_ADV;
/


CREATE SYNONYM ESCH.PKG_ADV FOR PKG_ADV;


CREATE SYNONYM EPAT.PKG_ADV FOR PKG_ADV;


GRANT EXECUTE, DEBUG ON PKG_ADV TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_ADV TO ESCH;

