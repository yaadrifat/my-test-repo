/* Formatted on 2/9/2010 1:39:15 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_ALLUSERSITES_DISTINCT
(
   PK_SITE,
   SITE_NAME,
   FK_USER,
   SITE_HIDDEN
)
AS
   SELECT   DISTINCT pk_site,
                     site_name,
                     fk_user,
                     site_hidden
     FROM   erv_allusersites;


