/* Formatted on 2/9/2010 1:39:43 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_STUDYSUM
(
   PK_STUDY,
   STUDY_PUBCOLLST,
   STUDY_NUMBER,
   STUDY_TITLE,
   STUDY_VER_NO,
   PK_STUDYVER,
   STUDY_OBJ,
   STUDY_SUM,
   STUDY_PRODNAME,
   STUDY_TAREA,
   STUDY_SAMPLSIZE,
   STUDY_DURATION,
   STUDY_ESTBEGINDT,
   STUDY_PHASE,
   STUDY_RESTYPE,
   STUDY_TYPE,
   STUDY_BLIND,
   STUDY_RANDOM,
   STUDY_SPONSOR,
   STUDY_CONTACT,
   STUDY_INFO,
   STUDY_PARTCNTR,
   STUDY_KEYWRDS,
   PK_STUDYAPNDX,
   FK_STUDY,
   STUDYAPNDX_URI,
   STUDYAPNDX_DESC,
   STUDYAPNDX_FILE,
   STUDYAPNDX_TYPE,
   RECTYPE,
   BIT1,
   BIT2,
   BIT3,
   FK_AUTHOR,
   STUDYSEC_CONTENTS,
   STUDYSEC_CONTENTS2,
   STUDYSEC_CONTENTS3,
   STUDYSEC_CONTENTS4,
   STUDYSEC_CONTENTS5,
   STUDYSEC_NUM,
   STUDY_END_DATE,
   STUDY_ACTUALDT,
   STUDYSEC_TEXT,
   FK_CODELST_SPONSOR
)
AS
   SELECT   a.pk_study,                                                    --1
            a.study_pubcollst,                                             --2
            a.study_number study_number,                                   --3
            a.study_title study_title,                                     --4
            c.studyver_number,                                            --4a
            c.pk_studyver,                                                --4b
            a.STUDY_OBJ_CLOB study_obj,                                   -- 5
            a.STUDY_SUM_CLOB study_sum,                                    --6
            a.STUDY_PRODNAME study_prodname,                               --7
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = a.fk_codelst_tarea)
               study_tarea,                                                --8
            a.STUDY_SAMPLSIZE study_samplsize,                             --9
            a.STUDY_DUR || ' ' || a.STUDY_DURUNIT study_duration,         --10
            a.STUDY_ESTBEGINDT study_estbegindt,                          --11
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = a.fk_codelst_phase)
               study_phase,                                               --12
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = FK_CODELST_restype)
               study_restype,                                           -- 12a
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = FK_CODELST_TYPE)
               study_type,                                                --13
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = FK_CODELST_BLIND)
               study_blind,                                               --14
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = FK_CODELST_RANDOM)
               study_random,                                              --15
            a.STUDY_SPONSOR,                                              --16
            a.STUDY_CONTACT,                                             -- 17
            a.STUDY_INFO,                                                -- 18
            a.STUDY_PARTCNTR,                                            -- 19
            a.STUDY_KEYWRDS,                                             -- 20
            b.pk_studyapndx,                                              --21
            b.fk_study,                                                  -- 22
            b.studyapndx_uri,                                            -- 23
            b.studyapndx_desc,                                           -- 24
            b.studyapndx_file x,                                         -- 25
            b.studyapndx_type,                                           -- 26
            'A' rectype,                                                  --27
            SUBSTR (STUDY_PUBCOLLST, 1, 1) bit1,                          --28
            SUBSTR (STUDY_PUBCOLLST, 2, 1) bit2,                          --29
            SUBSTR (STUDY_PUBCOLLST, 3, 1) bit3,                          --30
            (SELECT   USR_LASTNAME || ', ' || USR_FIRSTNAME
               FROM   ER_USER
              WHERE   pk_user = a.fk_author)
               fk_author,                                                -- 31
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            a.study_end_date,
            a.STUDY_ACTUALDT,
            NULL AS STUDYSEC_TEXT,
            a.fk_codelst_sponsor
     FROM   ER_STUDY a, ER_STUDYAPNDX b, ER_STUDYVER c
    WHERE       a.pk_study = c.fk_study(+)
            AND c.pk_studyver = b.fk_studyver(+)
            AND (STUDYAPNDX_PUBFLAG = 'Y' OR STUDYAPNDX_PUBFLAG IS NULL)
   UNION ALL
   SELECT   a.pk_study,                                                    --1
            a.study_pubcollst,                                             --2
            a.study_number study_number,                                   --3
            a.study_title study_title,                                     --4
            b.studyver_number,                                            --4a
            b.pk_studyver,                                                --4b
            a.STUDY_OBJ_CLOB study_obj,                                   -- 5
            a.STUDY_SUM_CLOB study_sum,                                    --6
            a.STUDY_PRODNAME study_prodname,                               --7
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = a.fk_codelst_tarea)
               study_tarea,                                                --8
            a.STUDY_SAMPLSIZE study_samplsize,                             --9
            a.STUDY_DUR || ' ' || a.STUDY_DURUNIT study_duration,         --10
            a.STUDY_ESTBEGINDT study_estbegindt,                          --11
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = a.fk_codelst_phase)
               study_phase,                                               --12
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = FK_CODELST_restype)
               study_restype,                                           -- 12a
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = FK_CODELST_TYPE)
               study_type,                                                --13
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = FK_CODELST_BLIND)
               study_blind,                                               --14
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = FK_CODELST_RANDOM)
               study_random,                                              --15
            a.STUDY_SPONSOR,                                              --16
            a.STUDY_CONTACT,                                             -- 17
            a.STUDY_INFO,                                                -- 18
            a.STUDY_PARTCNTR,                                            -- 19
            a.STUDY_KEYWRDS,                                             -- 20
            TO_NUMBER (NULL) pk_studysec,                                -- 21
            TO_NUMBER (NULL) fk_study,                                   -- 22
            NULL studysec_name,                                          -- 23
            NULL studysec_pubflag,                                       -- 24
            NULL x,                                                      -- 25
            NULL AS sc,                                                  -- 26
            'S' rectype,                                                  --27
            SUBSTR (STUDY_PUBCOLLST, 1, 1) bit1,                         -- 28
            SUBSTR (STUDY_PUBCOLLST, 2, 1) bit2,                          --29
            SUBSTR (STUDY_PUBCOLLST, 3, 1) bit3,                          --30
            (SELECT   USR_LASTNAME || ', ' || USR_FIRSTNAME
               FROM   ER_USER
              WHERE   pk_user = a.fk_author)
               fk_author,                                                -- 31
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            a.study_end_date,
            a.STUDY_ACTUALDT,
            NULL AS STUDYSEC_TEXT,
            a.fk_codelst_sponsor
     FROM   ER_STUDY a, ER_STUDYVER b
    WHERE   a.pk_study = b.fk_study(+)
            AND NOT EXISTS
                  (SELECT   1
                     FROM   ER_STUDYSEC
                    WHERE   fk_studyver = b.pk_studyver
                            AND (STUDYSEC_PUBFLAG = 'Y'
                                 OR STUDYSEC_PUBFLAG IS NULL))
   UNION ALL
   SELECT   a.pk_study,                                                    --1
            a.study_pubcollst,                                             --2
            a.study_number study_number,                                   --3
            a.study_title study_title,                                     --4
            c.studyver_number,                                            --4a
            c.pk_studyver,                                                --4b
            a.STUDY_OBJ_CLOB study_obj,                                   -- 5
            a.STUDY_SUM_CLOB study_sum,                                    --6
            a.STUDY_PRODNAME study_prodname,                               --7
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = a.fk_codelst_tarea)
               study_tarea,                                                --8
            a.STUDY_SAMPLSIZE study_samplsize,                             --9
            a.STUDY_DUR || ' ' || a.STUDY_DURUNIT study_duration,         --10
            a.STUDY_ESTBEGINDT study_estbegindt,                          --11
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = a.fk_codelst_phase)
               study_phase,                                               --12
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = FK_CODELST_restype)
               study_restype,                                           -- 12a
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = FK_CODELST_TYPE)
               study_type,                                                --13
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = FK_CODELST_BLIND)
               study_blind,                                               --14
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = FK_CODELST_RANDOM)
               study_random,                                              --15
            a.STUDY_SPONSOR,                                              --16
            a.STUDY_CONTACT,                                             -- 17
            a.STUDY_INFO,                                                -- 18
            a.STUDY_PARTCNTR,                                            -- 19
            a.STUDY_KEYWRDS,                                             -- 20
            b.pk_studysec,                                               -- 21
            b.fk_study,                                                  -- 22
            b.studysec_name,                                             -- 23
            b.studysec_pubflag,                                          -- 24
            TO_CHAR (b.studysec_seq) x,                                  -- 25
            NULL AS sc,                                                  -- 26
            'S' rectype,                                                  --27
            SUBSTR (STUDY_PUBCOLLST, 1, 1) bit1,                         -- 28
            SUBSTR (STUDY_PUBCOLLST, 2, 1) bit2,                          --29
            SUBSTR (STUDY_PUBCOLLST, 3, 1) bit3,                          --30
            (SELECT   USR_LASTNAME || ', ' || USR_FIRSTNAME
               FROM   ER_USER
              WHERE   pk_user = a.fk_author)
               fk_author,                                                -- 33
            TRIM (b.studysec_contents),
            TRIM (b.studysec_contents2),
            TRIM (b.studysec_contents3),
            TRIM (b.studysec_contents4),
            TRIM (b.studysec_contents5),
            TRIM (b.studysec_num),
            a.study_end_date,
            a.STUDY_ACTUALDT,
            STUDYSEC_TEXT,
            a.fk_codelst_sponsor
     FROM   ER_STUDY a, ER_STUDYSEC b, ER_STUDYVER c
    WHERE       a.pk_study = c.fk_study
            AND b.fk_studyver = c.pk_studyver
            AND (STUDYSEC_PUBFLAG = 'Y' OR STUDYSEC_PUBFLAG IS NULL)
   ORDER BY   x ASC;


CREATE SYNONYM ESCH.ERV_STUDYSUM FOR ERV_STUDYSUM;


CREATE SYNONYM EPAT.ERV_STUDYSUM FOR ERV_STUDYSUM;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_STUDYSUM TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_STUDYSUM TO ESCH;

