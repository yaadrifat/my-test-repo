/* Formatted on 2/9/2010 1:39:47 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_ACCNT_USERS
(
   ACUSR_TYPE,
   ACUSR_GRP,
   ACUSR_SITE,
   ACUSR_NAME,
   ACUSR_JOBTYPE,
   ACUSR_ADDRESS,
   ACUSR_CITY,
   ACUSR_STATE,
   ACUSR_ZIP,
   ACUSR_COUNTRY,
   ACUSR_PHONE,
   ACUSR_EMAIL,
   ACUSR_TIMEZONE,
   ACUSR_SPECIALTY,
   ACUSR_WRKEXP,
   ACUSR_PHASEINV,
   ACUSR_DEFAULTGRP,
   ACUSR_USRNAME,
   ACUSR_STATUS,
   ACUSR_ACCOUNT,
   CREATED_ON,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATOR,
   RID,
   ACUSR_RESPONSE_ID,
   FK_ACCOUNT
)
AS
   SELECT   F_Get_Usrtype (USR_TYPE) ACUSR_TYPE,
            FK_GRP_DEFAULT ACUSR_GRP,
            (SELECT   SITE_NAME
               FROM   ER_SITE
              WHERE   PK_SITE = FK_SITEID)
               ACUSR_SITE,
            o.USR_FIRSTNAME || ' ' || o.USR_LASTNAME AS ACUSR_NAME,
            F_Get_Codelstdesc (FK_CODELST_JOBTYPE) ACUSR_JOBTYPE,
            (SELECT   ADDRESS
               FROM   ER_ADD
              WHERE   PK_ADD = FK_PERADD)
               ACUSR_ADDRESS,
            (SELECT   ADD_CITY
               FROM   ER_ADD
              WHERE   PK_ADD = FK_PERADD)
               ACUSR_CITY,
            (SELECT   ADD_STATE
               FROM   ER_ADD
              WHERE   PK_ADD = FK_PERADD)
               ACUSR_STATE,
            (SELECT   ADD_ZIPCODE
               FROM   ER_ADD
              WHERE   PK_ADD = FK_PERADD)
               ACUSR_ZIP,
            (SELECT   ADD_COUNTRY
               FROM   ER_ADD
              WHERE   PK_ADD = FK_PERADD)
               ACUSR_COUNTRY,
            (SELECT   ADD_PHONE
               FROM   ER_ADD
              WHERE   PK_ADD = FK_PERADD)
               ACUSR_PHONE,
            (SELECT   ADD_EMAIL
               FROM   ER_ADD
              WHERE   PK_ADD = FK_PERADD)
               ACUSR_EMAIL,
            (SELECT   TZ_DESCRIPTION
               FROM   SCH_TIMEZONES
              WHERE   PK_TZ = FK_TIMEZONE)
               ACUSR_TIMEZONE,
            F_Get_Codelstdesc (FK_CODELST_SPL) ACUSR_SPECIALTY,
            USR_WRKEXP ACUSR_WRKEXP,
            USR_PAHSEINV ACUSR_PHASEINV,
            (SELECT   GRP_NAME
               FROM   ER_GRPS
              WHERE   PK_GRP = FK_GRP_DEFAULT)
               ACUSR_DEFAULTGRP,
            USR_LOGNAME ACUSR_USRNAME,
            DECODE (USR_STAT,
                    'A', 'Active',
                    'B', 'Blocked',
                    'D', 'Deactivated',
                    'Other')
               ACUSR_STATUS,
            (SELECT   AC_NAME
               FROM   ER_ACCOUNT
              WHERE   PK_ACCOUNT = FK_ACCOUNT)
               ACUSR_ACCOUNT,
            CREATED_ON CREATED_ON,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER i
              WHERE   i.PK_USER = o.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            LAST_MODIFIED_DATE LAST_MODIFIED_DATE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER i
              WHERE   i.PK_USER = o.CREATOR)
               CREATOR,
            RID,
            PK_USER ACUSR_RESPONSE_ID,
            fk_account
     FROM   ER_USER o
    WHERE   USR_TYPE <> 'X';


