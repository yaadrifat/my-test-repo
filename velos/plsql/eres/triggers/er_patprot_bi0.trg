CREATE  OR REPLACE TRIGGER ER_PATPROT_BI0 BEFORE INSERT ON ER_PATPROT        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_PATPROT',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON', TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'DATE_OF_DEATH', :NEW.DATE_OF_DEATH);
       Audit_Trail.column_insert (raid, 'DEATH_STD_RLTD_OTHER', :NEW.DEATH_STD_RLTD_OTHER);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELSTLOC;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELSTLOC', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_PTST_DTH_STDREL;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_PTST_DTH_STDREL', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_PTST_EVAL;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_PTST_EVAL', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_PTST_EVAL_FLAG;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_PTST_EVAL_FLAG', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_PTST_INEVAL;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_PTST_INEVAL', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_PTST_SURVIVAL;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_PTST_SURVIVAL', codeList_desc);
       Audit_Trail.column_insert (raid, 'FK_PER', :NEW.FK_PER);
       Audit_Trail.column_insert (raid, 'FK_PROTOCOL', :NEW.FK_PROTOCOL);
       Audit_Trail.column_insert (raid, 'FK_SITE_ENROLLING', :NEW.FK_SITE_ENROLLING);
       Audit_Trail.column_insert (raid, 'FK_STUDY', :NEW.FK_STUDY);
       Audit_Trail.column_insert (raid, 'FK_TIMEZONE', :NEW.FK_TIMEZONE);
       Audit_Trail.column_insert (raid, 'FK_USER', :NEW.FK_USER);
       Audit_Trail.column_insert (raid, 'FK_USERASSTO', :NEW.FK_USERASSTO);
       Audit_Trail.column_insert (raid, 'INFORM_CONSENT_VER', :NEW.INFORM_CONSENT_VER);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'NEXT_FOLLOWUP_ON', :NEW.NEXT_FOLLOWUP_ON);
       Audit_Trail.column_insert (raid, 'PATPROT_CONSIGN', :NEW.PATPROT_CONSIGN);
       Audit_Trail.column_insert (raid, 'PATPROT_CONSIGNDT', :NEW.PATPROT_CONSIGNDT);
       Audit_Trail.column_insert (raid, 'PATPROT_DISCDT', :NEW.PATPROT_DISCDT);
       Audit_Trail.column_insert (raid, 'PATPROT_ENROLDT', :NEW.PATPROT_ENROLDT);
       Audit_Trail.column_insert (raid, 'PATPROT_NOTES', :NEW.PATPROT_NOTES);
       Audit_Trail.column_insert (raid, 'PATPROT_PATSTDID', :NEW.PATPROT_PATSTDID);
       Audit_Trail.column_insert (raid, 'PATPROT_PHYSICIAN', :NEW.PATPROT_PHYSICIAN);
       Audit_Trail.column_insert (raid, 'PATPROT_RANDOM', :NEW.PATPROT_RANDOM);
       Audit_Trail.column_insert (raid, 'PATPROT_REASON', :NEW.PATPROT_REASON);
       Audit_Trail.column_insert (raid, 'PATPROT_RESIGNDT1', :NEW.PATPROT_RESIGNDT1);
       Audit_Trail.column_insert (raid, 'PATPROT_RESIGNDT2', :NEW.PATPROT_RESIGNDT2);
       Audit_Trail.column_insert (raid, 'PATPROT_SCHDAY', :NEW.PATPROT_SCHDAY);
       Audit_Trail.column_insert (raid, 'PATPROT_START', :NEW.PATPROT_START);
       Audit_Trail.column_insert (raid, 'PATPROT_STAT', :NEW.PATPROT_STAT);
       Audit_Trail.column_insert (raid, 'PATPROT_TREATINGORG', :NEW.PATPROT_TREATINGORG);
       Audit_Trail.column_insert (raid, 'PK_PATPROT', :NEW.PK_PATPROT);
       Audit_Trail.column_insert (raid, 'PATPROT_DISEASE_CODE', :NEW.PATPROT_DISEASE_CODE);
       Audit_Trail.column_insert (raid, 'DMGRPH_REPORTABLE', :NEW.DMGRPH_REPORTABLE);
	   Audit_Trail.column_insert (raid, 'PATPROT_OTHR_DIS_CODE', :NEW.PATPROT_OTHR_DIS_CODE);
       Audit_Trail.column_insert (raid, 'PATPROT_MORE_DIS_CODE1', :NEW.PATPROT_MORE_DIS_CODE1);
	   Audit_Trail.column_insert (raid, 'PATPROT_MORE_DIS_CODE2', :NEW.PATPROT_MORE_DIS_CODE2);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/