CREATE  OR REPLACE TRIGGER ER_PORTAL_AD0 AFTER DELETE ON ER_PORTAL        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_PORTAL', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'FK_ACCOUNT', :OLD.FK_ACCOUNT);
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_PORTAL', :OLD.PK_PORTAL);
       Audit_Trail.column_delete (raid, 'PORTAL_AUDITUSER', :OLD.PORTAL_AUDITUSER);
       Audit_Trail.column_delete (raid, 'PORTAL_BGCOLOR', :OLD.PORTAL_BGCOLOR);
       Audit_Trail.column_delete (raid, 'PORTAL_CONSENTING_FORM', :OLD.PORTAL_CONSENTING_FORM);
       Audit_Trail.column_delete (raid, 'PORTAL_CREATED_BY', :OLD.PORTAL_CREATED_BY);
       Audit_Trail.column_delete (raid, 'PORTAL_CREATELOGINS', :OLD.PORTAL_CREATELOGINS);
       Audit_Trail.column_delete (raid, 'PORTAL_DEFAULTPASS', :OLD.PORTAL_DEFAULTPASS);
       Audit_Trail.column_delete (raid, 'PORTAL_DESC', :OLD.PORTAL_DESC);
       Audit_Trail.column_delete (raid, 'PORTAL_DISP_TYPE', :OLD.PORTAL_DISP_TYPE);
       Audit_Trail.column_delete (raid, 'PORTAL_FOOTER', :OLD.PORTAL_FOOTER);
       Audit_Trail.column_delete (raid, 'PORTAL_HEADER', :OLD.PORTAL_HEADER);
       Audit_Trail.column_delete (raid, 'PORTAL_LEVEL', :OLD.PORTAL_LEVEL);
       Audit_Trail.column_delete (raid, 'PORTAL_NAME', :OLD.PORTAL_NAME);
       Audit_Trail.column_delete (raid, 'PORTAL_NOTIFICATION_FLAG', :OLD.PORTAL_NOTIFICATION_FLAG);
       Audit_Trail.column_delete (raid, 'PORTAL_SELFLOGOUT', :OLD.PORTAL_SELFLOGOUT);
       Audit_Trail.column_delete (raid, 'PORTAL_STATUS', :OLD.PORTAL_STATUS);
       Audit_Trail.column_delete (raid, 'PORTAL_TEXTCOLOR', :OLD.PORTAL_TEXTCOLOR);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/
