CREATE  OR REPLACE TRIGGER ER_DYNREP_BI0 BEFORE INSERT ON ER_DYNREP        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_DYNREP',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'DYNREP_DESC', :NEW.DYNREP_DESC);
       Audit_Trail.column_insert (raid, 'DYNREP_FILTER', :NEW.DYNREP_FILTER);
       Audit_Trail.column_insert (raid, 'DYNREP_FORMLIST', :NEW.DYNREP_FORMLIST);
       Audit_Trail.column_insert (raid, 'DYNREP_FTRNAME', :NEW.DYNREP_FTRNAME);
       Audit_Trail.column_insert (raid, 'DYNREP_HDRNAME', :NEW.DYNREP_HDRNAME);
       Audit_Trail.column_insert (raid, 'DYNREP_NAME', :NEW.DYNREP_NAME);
       Audit_Trail.column_insert (raid, 'DYNREP_ORDERBY', :NEW.DYNREP_ORDERBY);
       Audit_Trail.column_insert (raid, 'DYNREP_SHAREWITH', :NEW.DYNREP_SHAREWITH);
       Audit_Trail.column_insert (raid, 'DYNREP_TYPE', :NEW.DYNREP_TYPE);
       Audit_Trail.column_insert (raid, 'DYNREP_USEDATAVAL', :NEW.DYNREP_USEDATAVAL);
       Audit_Trail.column_insert (raid, 'DYNREP_USEUNIQUEID', :NEW.DYNREP_USEUNIQUEID);
       Audit_Trail.column_insert (raid, 'FK_ACCOUNT', :NEW.FK_ACCOUNT);
       Audit_Trail.column_insert (raid, 'FK_FORM', :NEW.FK_FORM);
       Audit_Trail.column_insert (raid, 'FK_PERSON', :NEW.FK_PERSON);
       Audit_Trail.column_insert (raid, 'FK_STUDY', :NEW.FK_STUDY);
       Audit_Trail.column_insert (raid, 'FK_USER', :NEW.FK_USER);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_DYNREP', :NEW.PK_DYNREP);
       Audit_Trail.column_insert (raid, 'REPORT_TYPE', :NEW.REPORT_TYPE);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/
