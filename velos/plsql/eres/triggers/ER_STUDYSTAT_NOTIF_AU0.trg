create or replace
TRIGGER ER_STUDYSTAT_NOTIF_AU0
AFTER UPDATE
ON ER_STUDYSTAT_NOTIF REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  usr VARCHAR2(100);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  usr := getuser(:NEW.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_STUDYSTAT_NOTIF', :OLD.rid, 'U', usr);
  IF NVL(:OLD.pk_studystat_notif,0) !=
     NVL(:NEW.pk_studystat_notif,0) THEN
     audit_trail.column_update
       (raid, 'PK_STUDYSTAT_NOTIF',
       :OLD.pk_studystat_notif, :NEW.pk_studystat_notif);
  END IF;
  IF NVL(:OLD.fk_codelst_studystat,0) !=
     NVL(:NEW.fk_codelst_studystat,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_STUDYSTAT',
       :OLD.fk_codelst_studystat, :NEW.fk_codelst_studystat);
  END IF;
  IF NVL(:OLD.fk_account,0) !=
     NVL(:NEW.fk_account,0) THEN
     audit_trail.column_update
       (raid, 'FK_ACCOUNT',
       :OLD.fk_account, :NEW.fk_account);
  END IF;  
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.fk_user,0) !=
     NVL(:NEW.fk_user,0) THEN
     audit_trail.column_update
       (raid, 'FK_USER',
       :OLD.fk_user, :NEW.fk_user);
  END IF;
  IF NVL(:OLD.fk_siteid,0) !=
     NVL(:NEW.fk_siteid,0) THEN
     audit_trail.column_update
       (raid, 'FK_SITEID',
       :OLD.fk_siteid, :NEW.fk_siteid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_modby := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
              to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.IP_ADD,' ') !=
     NVL(:NEW.IP_ADD,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.IP_ADD, :NEW.IP_ADD);
  END IF;
END;