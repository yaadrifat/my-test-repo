CREATE OR REPLACE TRIGGER ERES.ER_STUDYTEAM_AI1_OUT
AFTER INSERT
ON ERES.ER_STUDYTEAM REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE
fkaccount NUMBER;
pkmsg NUMBER;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL  => Plog.LFATAL);
/******************************************************************************
   NAME:
   PURPOSE:OutBound messages for study Team insert

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        9/26/2011    Kanwal           1. Created this trigger.
******************************************************************************/
BEGIN
  SELECT fk_account
  INTO fkaccount
  FROM er_user
  WHERE pk_user = NVL(:NEW.LAST_MODIFIED_BY, :NEW.CREATOR);

  PKG_MSG_QUEUE.SP_POPULATE_STUDYTEAM_MSG(
      :NEW.PK_STUDYTEAM,
      fkaccount,
      'I',
      to_char(:NEW.fk_study), :NEW.FK_USER,  :NEW.FK_CODELST_TMROLE);
   EXCEPTION
     WHEN OTHERS THEN
       Plog.FATAL(pCTX,'exception in ER_STUDYTEAM_AI1_OUT' || SQLERRM);
END ;
/
