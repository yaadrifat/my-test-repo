CREATE  OR REPLACE TRIGGER ER_FORMLIB_AD0 AFTER DELETE ON ER_FORMLIB        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
  oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
       oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_FORMLIB', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'FK_ACCOUNT', :OLD.FK_ACCOUNT);
       Audit_Trail.column_delete (raid, 'FK_CATLIB', :OLD.FK_CATLIB);
       Audit_Trail.column_delete (raid, 'FLD_FONTSIZE', :OLD.FLD_FONTSIZE);
       Audit_Trail.column_delete (raid, 'FORM_ACTIVATION_JS', :OLD.FORM_ACTIVATION_JS);
       Audit_Trail.column_delete (raid, 'FORM_ALIGN', :OLD.FORM_ALIGN);
       Audit_Trail.column_delete (raid, 'FORM_BGCOLOR', :OLD.FORM_BGCOLOR);
       Audit_Trail.column_delete (raid, 'FORM_BOLD', :OLD.FORM_BOLD);
       Audit_Trail.column_delete (raid, 'FORM_COLOR', :OLD.FORM_COLOR);
       Audit_Trail.column_delete (raid, 'FORM_CUSTOM_JS', :OLD.FORM_CUSTOM_JS);
       Audit_Trail.column_delete (raid, 'FORM_DESC', :OLD.FORM_DESC);
       Audit_Trail.column_delete (raid, 'FORM_ESIGNREQ', :OLD.FORM_ESIGNREQ);
       Audit_Trail.column_delete (raid, 'FORM_FILLFLAG', :OLD.FORM_FILLFLAG);
       Audit_Trail.column_delete (raid, 'FORM_FONT', :OLD.FORM_FONT);
       Audit_Trail.column_delete (raid, 'FORM_ITALICS', :OLD.FORM_ITALICS);
       Audit_Trail.column_delete (raid, 'FORM_KEYWORD', :OLD.FORM_KEYWORD);
       Audit_Trail.column_delete (raid, 'FORM_LINKTO', :OLD.FORM_LINKTO);
       Audit_Trail.column_delete (raid, 'FORM_NAME', :OLD.FORM_NAME);
       Audit_Trail.column_delete (raid, 'FORM_NEXT', :OLD.FORM_NEXT);
       Audit_Trail.column_delete (raid, 'FORM_NEXTMODE', :OLD.FORM_NEXTMODE);
       Audit_Trail.column_delete (raid, 'FORM_SAVEFORMAT', :OLD.FORM_SAVEFORMAT);
       Audit_Trail.column_delete (raid, 'FORM_SHAREDWITH', :OLD.FORM_SHAREDWITH);
       Audit_Trail.column_delete (raid, 'FORM_STATUS', :OLD.FORM_STATUS);
       Audit_Trail.column_delete (raid, 'FORM_UNDERLINE', :OLD.FORM_UNDERLINE);
      -- Audit_Trail.column_delete (raid, 'FORM_VIEWXSL', :OLD.FORM_VIEWXSL);
      -- Audit_Trail.column_delete (raid, 'FORM_XML', :OLD.FORM_XML);
      -- Audit_Trail.column_delete (raid, 'FORM_XSL', :OLD.FORM_XSL);
       Audit_Trail.column_delete (raid, 'FORM_XSLREFRESH', :OLD.FORM_XSLREFRESH);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_FORMLIB', :OLD.PK_FORMLIB);
       Audit_Trail.column_delete (raid, 'RECORD_TYPE', :OLD.RECORD_TYPE);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID); 

COMMIT;
END;
/