CREATE  OR REPLACE TRIGGER ER_SPECIMEN_STATUS_AD0 AFTER DELETE ON ER_SPECIMEN_STATUS        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
  oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
       oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_SPECIMEN_STATUS', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_STATUS;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_STATUS', codeList_desc);
       Audit_Trail.column_delete (raid, 'FK_SPECIMEN', :OLD.FK_SPECIMEN);
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'FK_USER_RECEPIENT', :OLD.FK_USER_RECEPIENT);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_SPECIMEN_STATUS', :OLD.PK_SPECIMEN_STATUS);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'SS_ACTION', :OLD.SS_ACTION);
       Audit_Trail.column_delete (raid, 'SS_DATE', :OLD.SS_DATE);
       Audit_Trail.column_delete (raid, 'SS_HAND_OFF_DATE', :OLD.SS_HAND_OFF_DATE);
       Audit_Trail.column_delete (raid, 'SS_NOTES', :OLD.SS_NOTES);
       Audit_Trail.column_delete (raid, 'SS_PROC_DATE', :OLD.SS_PROC_DATE);
       Audit_Trail.column_delete (raid, 'SS_PROC_TYPE', :OLD.SS_PROC_TYPE);
       Audit_Trail.column_delete (raid, 'SS_QUANTITY', :OLD.SS_QUANTITY);
       Audit_Trail.column_delete (raid, 'SS_QUANTITY_UNITS', :OLD.SS_QUANTITY_UNITS);
       Audit_Trail.column_delete (raid, 'SS_STATUS_BY', :OLD.SS_STATUS_BY);
       Audit_Trail.column_delete (raid, 'SS_TRACKING_NUMBER', :OLD.SS_TRACKING_NUMBER);
COMMIT;
END;
/