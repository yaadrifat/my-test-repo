CREATE  OR REPLACE TRIGGER ER_DYNREPVIEW_BI0 BEFORE INSERT ON ER_DYNREPVIEW        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_DYNREPVIEW',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CALCPER', :NEW.CALCPER);
       Audit_Trail.column_insert (raid, 'CALCSUM', :NEW.CALCSUM);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'DYNREPVIEW_COL', :NEW.DYNREPVIEW_COL);
       Audit_Trail.column_insert (raid, 'DYNREPVIEW_COLDATATYPE', :NEW.DYNREPVIEW_COLDATATYPE);
       Audit_Trail.column_insert (raid, 'DYNREPVIEW_COLNAME', :NEW.DYNREPVIEW_COLNAME);
       Audit_Trail.column_insert (raid, 'DYNREPVIEW_DISPNAME', :NEW.DYNREPVIEW_DISPNAME);
       Audit_Trail.column_insert (raid, 'DYNREPVIEW_FORMAT', :NEW.DYNREPVIEW_FORMAT);
       Audit_Trail.column_insert (raid, 'DYNREPVIEW_SEQ', :NEW.DYNREPVIEW_SEQ);
       Audit_Trail.column_insert (raid, 'DYNREPVIEW_WIDTH', :NEW.DYNREPVIEW_WIDTH);
       Audit_Trail.column_insert (raid, 'FK_DYNREP', :NEW.FK_DYNREP);
       Audit_Trail.column_insert (raid, 'FK_FORM', :NEW.FK_FORM);
       Audit_Trail.column_insert (raid, 'FORM_TYPE', :NEW.FORM_TYPE);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_DYNREPVIEW', :NEW.PK_DYNREPVIEW);
       Audit_Trail.column_insert (raid, 'REPEAT_NUMBER', :NEW.REPEAT_NUMBER);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'USEDATAVAL', :NEW.USEDATAVAL);
       Audit_Trail.column_insert (raid, 'USEUNIQUEID', :NEW.USEUNIQUEID);
COMMIT;
END;
/
