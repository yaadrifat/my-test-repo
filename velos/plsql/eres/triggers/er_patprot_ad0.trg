CREATE  OR REPLACE TRIGGER ER_PATPROT_AD0 AFTER DELETE ON ER_PATPROT        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
  oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
       oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_PATPROT', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'DATE_OF_DEATH', :OLD.DATE_OF_DEATH);
       Audit_Trail.column_delete (raid, 'DEATH_STD_RLTD_OTHER', :OLD.DEATH_STD_RLTD_OTHER);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELSTLOC;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELSTLOC', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_PTST_DTH_STDREL;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_PTST_DTH_STDREL', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_PTST_EVAL;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_PTST_EVAL', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_PTST_EVAL_FLAG;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_PTST_EVAL_FLAG', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_PTST_INEVAL;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_PTST_INEVAL', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_PTST_SURVIVAL;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_PTST_SURVIVAL', codeList_desc);
       Audit_Trail.column_delete (raid, 'FK_PER', :OLD.FK_PER);
       Audit_Trail.column_delete (raid, 'FK_PROTOCOL', :OLD.FK_PROTOCOL);
       Audit_Trail.column_delete (raid, 'FK_SITE_ENROLLING', :OLD.FK_SITE_ENROLLING);
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'FK_TIMEZONE', :OLD.FK_TIMEZONE);
       Audit_Trail.column_delete (raid, 'FK_USER', :OLD.FK_USER);
       Audit_Trail.column_delete (raid, 'FK_USERASSTO', :OLD.FK_USERASSTO);
       Audit_Trail.column_delete (raid, 'INFORM_CONSENT_VER', :OLD.INFORM_CONSENT_VER);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'NEXT_FOLLOWUP_ON', :OLD.NEXT_FOLLOWUP_ON);
       Audit_Trail.column_delete (raid, 'PATPROT_CONSIGN', :OLD.PATPROT_CONSIGN);
       Audit_Trail.column_delete (raid, 'PATPROT_CONSIGNDT', :OLD.PATPROT_CONSIGNDT);
       Audit_Trail.column_delete (raid, 'PATPROT_DISCDT', :OLD.PATPROT_DISCDT);
       Audit_Trail.column_delete (raid, 'PATPROT_ENROLDT', :OLD.PATPROT_ENROLDT);
       Audit_Trail.column_delete (raid, 'PATPROT_NOTES', :OLD.PATPROT_NOTES);
       Audit_Trail.column_delete (raid, 'PATPROT_PATSTDID', :OLD.PATPROT_PATSTDID);
       Audit_Trail.column_delete (raid, 'PATPROT_PHYSICIAN', :OLD.PATPROT_PHYSICIAN);
       Audit_Trail.column_delete (raid, 'PATPROT_RANDOM', :OLD.PATPROT_RANDOM);
       Audit_Trail.column_delete (raid, 'PATPROT_REASON', :OLD.PATPROT_REASON);
       Audit_Trail.column_delete (raid, 'PATPROT_RESIGNDT1', :OLD.PATPROT_RESIGNDT1);
       Audit_Trail.column_delete (raid, 'PATPROT_RESIGNDT2', :OLD.PATPROT_RESIGNDT2);
       Audit_Trail.column_delete (raid, 'PATPROT_SCHDAY', :OLD.PATPROT_SCHDAY);
       Audit_Trail.column_delete (raid, 'PATPROT_START', :OLD.PATPROT_START);
       Audit_Trail.column_delete (raid, 'PATPROT_STAT', :OLD.PATPROT_STAT);
       Audit_Trail.column_delete (raid, 'PATPROT_TREATINGORG', :OLD.PATPROT_TREATINGORG);
       Audit_Trail.column_delete (raid, 'PK_PATPROT', :OLD.PK_PATPROT);
       Audit_Trail.column_delete (raid, 'PATPROT_DISEASE_CODE', :OLD.PATPROT_DISEASE_CODE);
       Audit_Trail.column_delete (raid, 'DMGRPH_REPORTABLE', :OLD.DMGRPH_REPORTABLE);
	   Audit_Trail.column_delete (raid, 'PATPROT_OTHR_DIS_CODE', :OLD.PATPROT_OTHR_DIS_CODE);
	   Audit_Trail.column_delete (raid, 'PATPROT_MORE_DIS_CODE1', :OLD.PATPROT_MORE_DIS_CODE1);
	   Audit_Trail.column_delete (raid, 'PATPROT_MORE_DIS_CODE2', :OLD.PATPROT_MORE_DIS_CODE2);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/