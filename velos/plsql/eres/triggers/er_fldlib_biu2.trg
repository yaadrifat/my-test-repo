CREATE OR REPLACE TRIGGER "ER_FLDLIB_BIU2" 
BEFORE INSERT OR UPDATE OF FLD_LENGTH,FLD_CHARSNO,FLD_LINESNO,FLD_DATATYPE
ON ER_FLDLIB
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
v_length NUMBER;
v_linesno NUMBER;
v_charsno NUMBER;
v_field NUMBER;
v_fld_datatype Varchar2(2);
begin

/*
 * To set the default values of length, lines no and character nos of the field if the user doesn't enter them
*/

 --Find the data type,length ,lines no and character nos of the field
v_length := :new.fld_length;
v_linesno := :new.fld_linesno;
v_charsno := :new.fld_charsno;
v_fld_datatype := :new.fld_datatype;
v_field := :new.pk_field;
--If edit box type is TEXT and if any of the fields out of length,characters or lines are null set them to respective default
--values
if (v_fld_datatype='ET') then
-- set default value of length to 20 if it is null
    if ((nvl(length(v_length),0)=0) or (v_length is null))  then
         :new.fld_length := 20;
    end if;
-- set default value of lines to 1 if it is null
    if ((length(v_linesno)=0) or (v_linesno is null)) then
       :new.fld_linesno := 1;
    end if;
-- set default value of characters to 100 if it is null
    if ((length(v_charsno)=0) or (v_charsno is null))  then
        :new.fld_charsno := 100;
    end if ;
end if;
--If edit box type is Number and if any of the fields out of length,characters or lines are null set them to respective
--default values
if (v_fld_datatype='EN') then
 -- set default value of length to 20 if it is null
    if ((nvl(length(v_length),0)=0) or (v_length is null))  then
         :new.fld_length := 20;
    end if;
-- set default value of characters to 100 if it is null
    if ((length(v_charsno)=0) or (v_charsno is null))  then
        :new.fld_charsno := 100;
    end if ;
end if;
end;
/


