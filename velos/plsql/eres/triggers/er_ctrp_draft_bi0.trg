CREATE  OR REPLACE TRIGGER ER_CTRP_DRAFT_BI0 BEFORE INSERT ON ER_CTRP_DRAFT        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_CTRP_DRAFT',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'AMEND_DATE', :NEW.AMEND_DATE);
       Audit_Trail.column_insert (raid, 'AMEND_NUMBER', :NEW.AMEND_NUMBER);
       Audit_Trail.column_insert (raid, 'CREATED_ON', TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'CTRP_DRAFT_TYPE', :NEW.CTRP_DRAFT_TYPE);
       Audit_Trail.column_insert (raid, 'CTRP_STUDYACC_CLOSED_DATE', :NEW.CTRP_STUDYACC_CLOSED_DATE);
       Audit_Trail.column_insert (raid, 'CTRP_STUDYACC_OPEN_DATE', :NEW.CTRP_STUDYACC_OPEN_DATE);
       Audit_Trail.column_insert (raid, 'CTRP_STUDY_COMP_DATE', :NEW.CTRP_STUDY_COMP_DATE);
       Audit_Trail.column_insert (raid, 'CTRP_STUDY_START_DATE', :NEW.CTRP_STUDY_START_DATE);
       Audit_Trail.column_insert (raid, 'CTRP_STUDY_STATUS_DATE', :NEW.CTRP_STUDY_STATUS_DATE);
       Audit_Trail.column_insert (raid, 'CTRP_STUDY_STOP_REASON', :NEW.CTRP_STUDY_STOP_REASON);
       Audit_Trail.column_insert (raid, 'DELAY_POST_INDICATOR', :NEW.DELAY_POST_INDICATOR);
       Audit_Trail.column_insert (raid, 'DELETED_FLAG', :NEW.DELETED_FLAG);
       Audit_Trail.column_insert (raid, 'DM_APPOINT_INDICATOR', :NEW.DM_APPOINT_INDICATOR);
       Audit_Trail.column_insert (raid, 'DRAFT_PILOT', :NEW.DRAFT_PILOT);
       Audit_Trail.column_insert (raid, 'DRAFT_XML_REQD', :NEW.DRAFT_XML_REQD);
       Audit_Trail.column_insert (raid, 'FDA_INTVEN_INDICATOR', :NEW.FDA_INTVEN_INDICATOR);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_CTRP_STUDY_STATUS;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_CTRP_STUDY_STATUS', codeList_desc);
BEGIN
codeList_desc := :NEW.FK_CODELST_DISEASE_SITE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_DISEASE_SITE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_INTERVENTION_TYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_INTERVENTION_TYPE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_PHASE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_PHASE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_SUBMISSION_TYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_SUBMISSION_TYPE', codeList_desc);
       Audit_Trail.column_insert (raid, 'FK_STUDY', :NEW.FK_STUDY);
       Audit_Trail.column_insert (raid, 'FK_TRIAL_OWNER', :NEW.FK_TRIAL_OWNER);
       Audit_Trail.column_insert (raid, 'INTERVENTION_NAME', :NEW.INTERVENTION_NAME);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'IS_COMP_ACTUAL', :NEW.IS_COMP_ACTUAL);
       Audit_Trail.column_insert (raid, 'IS_START_ACTUAL', :NEW.IS_START_ACTUAL);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'LEAD_ORG_CTRPID', :NEW.LEAD_ORG_CTRPID);
       Audit_Trail.column_insert (raid, 'LEAD_ORG_FK_ADD', :NEW.LEAD_ORG_FK_ADD);
       Audit_Trail.column_insert (raid, 'LEAD_ORG_FK_SITE', :NEW.LEAD_ORG_FK_SITE);
       Audit_Trail.column_insert (raid, 'LEAD_ORG_STUDYID', :NEW.LEAD_ORG_STUDYID);
       Audit_Trail.column_insert (raid, 'LEAD_ORG_TYPE', :NEW.LEAD_ORG_TYPE);
       Audit_Trail.column_insert (raid, 'NCI_STUDYID', :NEW.NCI_STUDYID);
       Audit_Trail.column_insert (raid, 'NCT_NUMBER', :NEW.NCT_NUMBER);
       Audit_Trail.column_insert (raid, 'OTHER_NUMBER', :NEW.OTHER_NUMBER);
       Audit_Trail.column_insert (raid, 'OVERSIGHT_COUNTRY', :NEW.OVERSIGHT_COUNTRY);
       Audit_Trail.column_insert (raid, 'OVERSIGHT_ORGANIZATION', :NEW.OVERSIGHT_ORGANIZATION);
       Audit_Trail.column_insert (raid, 'PI_CTRPID', :NEW.PI_CTRPID);
       Audit_Trail.column_insert (raid, 'PI_FIRST_NAME', :NEW.PI_FIRST_NAME);
       Audit_Trail.column_insert (raid, 'PI_FK_ADD', :NEW.PI_FK_ADD);
       Audit_Trail.column_insert (raid, 'PI_FK_USER', :NEW.PI_FK_USER);
       Audit_Trail.column_insert (raid, 'PI_LAST_NAME', :NEW.PI_LAST_NAME);
       Audit_Trail.column_insert (raid, 'PK_CTRP_DRAFT', :NEW.PK_CTRP_DRAFT);
       Audit_Trail.column_insert (raid, 'RESPONSIBLE_PARTY', :NEW.RESPONSIBLE_PARTY);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'RP_EMAIL', :NEW.RP_EMAIL);
       Audit_Trail.column_insert (raid, 'RP_EXTN', :NEW.RP_EXTN);
       Audit_Trail.column_insert (raid, 'RP_PHONE', :NEW.RP_PHONE);
       Audit_Trail.column_insert (raid, 'RP_SPONSOR_CONTACT_ID', :NEW.RP_SPONSOR_CONTACT_ID);
       Audit_Trail.column_insert (raid, 'RP_SPONSOR_CONTACT_TYPE', :NEW.RP_SPONSOR_CONTACT_TYPE);
       Audit_Trail.column_insert (raid, 'SECTION_801_INDICATOR', :NEW.SECTION_801_INDICATOR);
       Audit_Trail.column_insert (raid, 'SITE_TARGET_ACCRUAL', :NEW.SITE_TARGET_ACCRUAL);
       Audit_Trail.column_insert (raid, 'SPONSOR_CTRPID', :NEW.SPONSOR_CTRPID);
       Audit_Trail.column_insert (raid, 'SPONSOR_FK_ADD', :NEW.SPONSOR_FK_ADD);
       Audit_Trail.column_insert (raid, 'SPONSOR_FK_SITE', :NEW.SPONSOR_FK_SITE);
       Audit_Trail.column_insert (raid, 'SPONSOR_GENERIC_TITLE', :NEW.SPONSOR_GENERIC_TITLE);
       Audit_Trail.column_insert (raid, 'SPONSOR_PERSONAL_FIRST_NAME', :NEW.SPONSOR_PERSONAL_FIRST_NAME);
       Audit_Trail.column_insert (raid, 'SPONSOR_PERSONAL_FK_ADD', :NEW.SPONSOR_PERSONAL_FK_ADD);
       Audit_Trail.column_insert (raid, 'SPONSOR_PERSONAL_FK_USER', :NEW.SPONSOR_PERSONAL_FK_USER);
       Audit_Trail.column_insert (raid, 'SPONSOR_PERSONAL_LAST_NAME', :NEW.SPONSOR_PERSONAL_LAST_NAME);
       Audit_Trail.column_insert (raid, 'SPONSOR_PROGRAM_CODE', :NEW.SPONSOR_PROGRAM_CODE);
       Audit_Trail.column_insert (raid, 'STUDY_PURPOSE', :NEW.STUDY_PURPOSE);
       Audit_Trail.column_insert (raid, 'STUDY_PURPOSE_OTHER', :NEW.STUDY_PURPOSE_OTHER);
       Audit_Trail.column_insert (raid, 'STUDY_TYPE', :NEW.STUDY_TYPE);
       Audit_Trail.column_insert (raid, 'SUBMIT_NCI_DESIGNATED', :NEW.SUBMIT_NCI_DESIGNATED);
       Audit_Trail.column_insert (raid, 'SUBMIT_ORG_CTRPID', :NEW.SUBMIT_ORG_CTRPID);
       Audit_Trail.column_insert (raid, 'SUBMIT_ORG_FK_ADD', :NEW.SUBMIT_ORG_FK_ADD);
       Audit_Trail.column_insert (raid, 'SUBMIT_ORG_FK_SITE', :NEW.SUBMIT_ORG_FK_SITE);
       Audit_Trail.column_insert (raid, 'SUBMIT_ORG_STUDYID', :NEW.SUBMIT_ORG_STUDYID);
       Audit_Trail.column_insert (raid, 'SUBMIT_ORG_TYPE', :NEW.SUBMIT_ORG_TYPE);
       Audit_Trail.column_insert (raid, 'SUMM4_SPONSOR_FK_ADD', :NEW.SUMM4_SPONSOR_FK_ADD);
       Audit_Trail.column_insert (raid, 'SUMM4_SPONSOR_FK_SITE', :NEW.SUMM4_SPONSOR_FK_SITE);
       Audit_Trail.column_insert (raid, 'SUMM4_SPONSOR_ID', :NEW.SUMM4_SPONSOR_ID);
       Audit_Trail.column_insert (raid, 'SUMM4_SPONSOR_TYPE', :NEW.SUMM4_SPONSOR_TYPE);
COMMIT;
END;
/