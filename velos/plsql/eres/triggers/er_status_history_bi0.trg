CREATE  OR REPLACE TRIGGER ER_STATUS_HISTORY_BI0 BEFORE INSERT ON ER_STATUS_HISTORY        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_STATUS_HISTORY',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON', TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_STAT;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_STAT', codeList_desc);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_STATUS', :NEW.PK_STATUS);
       Audit_Trail.column_insert (raid, 'RECORD_TYPE', :NEW.RECORD_TYPE);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'STATUSENDDATE', :NEW.STATUSENDDATE);
       Audit_Trail.column_insert (raid, 'STATUS_CUSTOM1', :NEW.STATUS_CUSTOM1);
       Audit_Trail.column_insert (raid, 'STATUS_DATE', :NEW.STATUS_DATE);
       Audit_Trail.column_insert (raid, 'STATUS_END_DATE', :NEW.STATUS_END_DATE);
       Audit_Trail.column_insert (raid, 'STATUS_ENTERED_BY', :NEW.STATUS_ENTERED_BY);
       Audit_Trail.column_insert (raid, 'STATUS_ISCURRENT', :NEW.STATUS_ISCURRENT);
       Audit_Trail.column_insert (raid, 'STATUS_MODPK', :NEW.STATUS_MODPK);
       Audit_Trail.column_insert (raid, 'STATUS_MODTABLE', :NEW.STATUS_MODTABLE);
       Audit_Trail.column_insert (raid, 'STATUS_NOTES', :NEW.STATUS_NOTES);
COMMIT;
END;
/
