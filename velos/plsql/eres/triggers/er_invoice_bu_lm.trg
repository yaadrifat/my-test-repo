CREATE OR REPLACE TRIGGER "ER_INVOICE_BU_LM" 
	BEFORE UPDATE ON ER_INVOICE REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
BEGIN
		:NEW.last_modified_date := SYSDATE ;
END;
/


