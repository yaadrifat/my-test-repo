CREATE  OR REPLACE TRIGGER ER_DYNREP_AD0 AFTER DELETE ON ER_DYNREP        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_DYNREP', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'DYNREP_DESC', :OLD.DYNREP_DESC);
       Audit_Trail.column_delete (raid, 'DYNREP_FILTER', :OLD.DYNREP_FILTER);
       Audit_Trail.column_delete (raid, 'DYNREP_FORMLIST', :OLD.DYNREP_FORMLIST);
       Audit_Trail.column_delete (raid, 'DYNREP_FTRNAME', :OLD.DYNREP_FTRNAME);
       Audit_Trail.column_delete (raid, 'DYNREP_HDRNAME', :OLD.DYNREP_HDRNAME);
       Audit_Trail.column_delete (raid, 'DYNREP_NAME', :OLD.DYNREP_NAME);
       Audit_Trail.column_delete (raid, 'DYNREP_ORDERBY', :OLD.DYNREP_ORDERBY);
       Audit_Trail.column_delete (raid, 'DYNREP_SHAREWITH', :OLD.DYNREP_SHAREWITH);
       Audit_Trail.column_delete (raid, 'DYNREP_TYPE', :OLD.DYNREP_TYPE);
       Audit_Trail.column_delete (raid, 'DYNREP_USEDATAVAL', :OLD.DYNREP_USEDATAVAL);
       Audit_Trail.column_delete (raid, 'DYNREP_USEUNIQUEID', :OLD.DYNREP_USEUNIQUEID);
       Audit_Trail.column_delete (raid, 'FK_ACCOUNT', :OLD.FK_ACCOUNT);
       Audit_Trail.column_delete (raid, 'FK_FORM', :OLD.FK_FORM);
       Audit_Trail.column_delete (raid, 'FK_PERSON', :OLD.FK_PERSON);
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'FK_USER', :OLD.FK_USER);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_DYNREP', :OLD.PK_DYNREP);
       Audit_Trail.column_delete (raid, 'REPORT_TYPE', :OLD.REPORT_TYPE);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/
