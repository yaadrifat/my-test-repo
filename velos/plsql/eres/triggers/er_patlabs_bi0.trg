CREATE  OR REPLACE TRIGGER ER_PATLABS_BI0 BEFORE INSERT ON ER_PATLABS        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_PATLABS',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'ACCESSION_NUM', :NEW.ACCESSION_NUM);
       Audit_Trail.column_insert (raid, 'CMS_APPROVED_LAB', :NEW.CMS_APPROVED_LAB);
       Audit_Trail.column_insert (raid, 'CREATED_ON', TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'CUSTOM001', :NEW.CUSTOM001);
       Audit_Trail.column_insert (raid, 'CUSTOM002', :NEW.CUSTOM002);
       Audit_Trail.column_insert (raid, 'CUSTOM003', :NEW.CUSTOM003);
       Audit_Trail.column_insert (raid, 'CUSTOM004', :NEW.CUSTOM004);
       Audit_Trail.column_insert (raid, 'CUSTOM005', :NEW.CUSTOM005);
       Audit_Trail.column_insert (raid, 'CUSTOM006', :NEW.CUSTOM006);
       Audit_Trail.column_insert (raid, 'CUSTOM007', :NEW.CUSTOM007);
       Audit_Trail.column_insert (raid, 'CUSTOM008', :NEW.CUSTOM008);
       Audit_Trail.column_insert (raid, 'CUSTOM009', :NEW.CUSTOM009);
       Audit_Trail.column_insert (raid, 'CUSTOM010', :NEW.CUSTOM010);
       Audit_Trail.column_insert (raid, 'ER_GRADE', :NEW.ER_GRADE);
       Audit_Trail.column_insert (raid, 'ER_NAME', :NEW.ER_NAME);
       Audit_Trail.column_insert (raid, 'FDA_LICENSED_LAB', :NEW.FDA_LICENSED_LAB);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_ABFLAG;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_ABFLAG', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_STDPHASE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_STDPHASE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_TSTSTAT;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_TSTSTAT', codeList_desc);
       Audit_Trail.column_insert (raid, 'FK_EVENT_ID', :NEW.FK_EVENT_ID);
       Audit_Trail.column_insert (raid, 'FK_PATPROT', :NEW.FK_PATPROT);
       Audit_Trail.column_insert (raid, 'FK_PER', :NEW.FK_PER);
       Audit_Trail.column_insert (raid, 'FK_SITE', :NEW.FK_SITE);
       Audit_Trail.column_insert (raid, 'FK_SPECIMEN', :NEW.FK_SPECIMEN);
       Audit_Trail.column_insert (raid, 'FK_STUDY', :NEW.FK_STUDY);
       Audit_Trail.column_insert (raid, 'FK_TESTGROUP', :NEW.FK_TESTGROUP);
       Audit_Trail.column_insert (raid, 'FK_TESTID', :NEW.FK_TESTID);
       Audit_Trail.column_insert (raid, 'FK_TEST_METHOD', :NEW.FK_TEST_METHOD);
       Audit_Trail.column_insert (raid, 'FK_TEST_OUTCOME', :NEW.FK_TEST_OUTCOME);
       Audit_Trail.column_insert (raid, 'FK_TEST_REASON', :NEW.FK_TEST_REASON);
       Audit_Trail.column_insert (raid, 'FK_TEST_SPECIMEN', :NEW.FK_TEST_SPECIMEN);
       Audit_Trail.column_insert (raid, 'FK_TIMING_OF_TEST', :NEW.FK_TIMING_OF_TEST);
       Audit_Trail.column_insert (raid, 'FT_TEST_SOURCE', :NEW.FT_TEST_SOURCE);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'NOTES', :NEW.NOTES);
       Audit_Trail.column_insert (raid, 'PK_PATLABS', :NEW.PK_PATLABS);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'TEST_DATE', :NEW.TEST_DATE);
       Audit_Trail.column_insert (raid, 'TEST_LLN', :NEW.TEST_LLN);
       Audit_Trail.column_insert (raid, 'TEST_RANGE', :NEW.TEST_RANGE);
       Audit_Trail.column_insert (raid, 'TEST_RESULT', :NEW.TEST_RESULT);
       Audit_Trail.column_insert (raid, 'TEST_RESULT_TX', :NEW.TEST_RESULT_TX);
       Audit_Trail.column_insert (raid, 'TEST_TYPE', :NEW.TEST_TYPE);
       Audit_Trail.column_insert (raid, 'TEST_ULN', :NEW.TEST_ULN);
       Audit_Trail.column_insert (raid, 'TEST_UNIT', :NEW.TEST_UNIT);
COMMIT;
END;
/