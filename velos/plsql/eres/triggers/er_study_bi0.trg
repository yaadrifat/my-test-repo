CREATE  OR REPLACE TRIGGER ER_STUDY_BI0 BEFORE INSERT ON ER_STUDY        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_STUDY',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON', TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'FDA_REGULATED_STUDY', :NEW.FDA_REGULATED_STUDY);
       Audit_Trail.column_insert (raid, 'FK_ACCOUNT', :NEW.FK_ACCOUNT);
       Audit_Trail.column_insert (raid, 'FK_AUTHOR', :NEW.FK_AUTHOR);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_BLIND;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_BLIND', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_CURRENCY;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_CURRENCY', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_PHASE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_PHASE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_RANDOM;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_RANDOM', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_RESTYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_RESTYPE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_SCOPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_SCOPE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_SET_MILESTATUS;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_SET_MILESTATUS', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_SPONSOR;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_SPONSOR', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_TAREA;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_TAREA', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_TYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_TYPE', codeList_desc);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_STUDY', :NEW.PK_STUDY);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'STUDY_ACTUALDT', :NEW.STUDY_ACTUALDT);
       Audit_Trail.column_insert (raid, 'STUDY_ADVLKP_VER', :NEW.STUDY_ADVLKP_VER);
       Audit_Trail.column_insert (raid, 'STUDY_ASSOC', :NEW.STUDY_ASSOC);
       Audit_Trail.column_insert (raid, 'STUDY_CONTACT', :NEW.STUDY_CONTACT);
       Audit_Trail.column_insert (raid, 'STUDY_COORDINATOR', :NEW.STUDY_COORDINATOR);
       Audit_Trail.column_insert (raid, 'STUDY_CREATION_TYPE', :NEW.STUDY_CREATION_TYPE);
       Audit_Trail.column_insert (raid, 'STUDY_CTRP_REPORTABLE', :NEW.STUDY_CTRP_REPORTABLE);
       Audit_Trail.column_insert (raid, 'STUDY_CURRENT', :NEW.STUDY_CURRENT);
       Audit_Trail.column_insert (raid, 'STUDY_DISEASE_SITE', :NEW.STUDY_DISEASE_SITE);
       Audit_Trail.column_insert (raid, 'STUDY_DIVISION', :NEW.STUDY_DIVISION);
       Audit_Trail.column_insert (raid, 'STUDY_DUR', :NEW.STUDY_DUR);
       Audit_Trail.column_insert (raid, 'STUDY_DURUNIT', :NEW.STUDY_DURUNIT);
       Audit_Trail.column_insert (raid, 'STUDY_END_DATE', :NEW.STUDY_END_DATE);
       Audit_Trail.column_insert (raid, 'STUDY_ESTBEGINDT', :NEW.STUDY_ESTBEGINDT);
       Audit_Trail.column_insert (raid, 'STUDY_ICDCODE1', :NEW.STUDY_ICDCODE1);
       Audit_Trail.column_insert (raid, 'STUDY_ICDCODE2', :NEW.STUDY_ICDCODE2);
       Audit_Trail.column_insert (raid, 'STUDY_ICDCODE3', :NEW.STUDY_ICDCODE3);
       Audit_Trail.column_insert (raid, 'STUDY_INFO', :NEW.STUDY_INFO);
       Audit_Trail.column_insert (raid, 'STUDY_INVIND_FLAG', :NEW.STUDY_INVIND_FLAG);
       Audit_Trail.column_insert (raid, 'STUDY_INVIND_NUMBER', :NEW.STUDY_INVIND_NUMBER);
       Audit_Trail.column_insert (raid, 'STUDY_KEYWRDS', :NEW.STUDY_KEYWRDS);
       Audit_Trail.column_insert (raid, 'STUDY_MAJ_AUTH', :NEW.STUDY_MAJ_AUTH);
       Audit_Trail.column_insert (raid, 'STUDY_NSAMPLSIZE', :NEW.STUDY_NSAMPLSIZE);
       Audit_Trail.column_insert (raid, 'STUDY_NUMBER', :NEW.STUDY_NUMBER);
       Audit_Trail.column_insert (raid, 'STUDY_OBJ', :NEW.STUDY_OBJ);
       Audit_Trail.column_insert (raid, 'STUDY_OBJ_CLOB', :NEW.STUDY_OBJ_CLOB);
       Audit_Trail.column_insert (raid, 'STUDY_OTHERPRINV', :NEW.STUDY_OTHERPRINV);
       Audit_Trail.column_insert (raid, 'STUDY_PARENTID', :NEW.STUDY_PARENTID);
       Audit_Trail.column_insert (raid, 'STUDY_PARTCNTR', :NEW.STUDY_PARTCNTR);
       Audit_Trail.column_insert (raid, 'STUDY_PRINV', :NEW.STUDY_PRINV);
       Audit_Trail.column_insert (raid, 'STUDY_PRODNAME', :NEW.STUDY_PRODNAME);
       Audit_Trail.column_insert (raid, 'STUDY_PUBCOLLST', :NEW.STUDY_PUBCOLLST);
       Audit_Trail.column_insert (raid, 'STUDY_PUBFLAG', :NEW.STUDY_PUBFLAG);
       Audit_Trail.column_insert (raid, 'STUDY_SAMPLSIZE', :NEW.STUDY_SAMPLSIZE);
       Audit_Trail.column_insert (raid, 'STUDY_SPONSOR', :NEW.STUDY_SPONSOR);
       Audit_Trail.column_insert (raid, 'STUDY_SPONSORID', :NEW.STUDY_SPONSORID);
       Audit_Trail.column_insert (raid, 'STUDY_SUM', :NEW.STUDY_SUM);
       Audit_Trail.column_insert (raid, 'STUDY_SUM_CLOB', :NEW.STUDY_SUM_CLOB);
       Audit_Trail.column_insert (raid, 'STUDY_TITLE', :NEW.STUDY_TITLE);
       Audit_Trail.column_insert (raid, 'STUDY_VERPARENT', :NEW.STUDY_VERPARENT);
       Audit_Trail.column_insert (raid, 'STUDY_VER_NO', :NEW.STUDY_VER_NO);
COMMIT;
END;
/
