CREATE OR REPLACE TRIGGER er_milepayment_details_AU0
AFTER UPDATE
ON ER_MILEPAYMENT_DETAILS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  old_codetype VARCHAR2(200) ;
  new_codetype VARCHAR2(200) ;
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  usr VARCHAR2(100);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  usr := Getuser(:NEW.last_modified_by);

  Audit_Trail.record_transaction
    (raid, 'er_milepayment_details', :OLD.rid, 'U', usr);


  IF NVL(:OLD.pk_mp_details ,0) !=
     NVL(:NEW.pk_mp_details ,0) THEN
     Audit_Trail.column_update
       (raid, 'pk_mp_details',
       :OLD.pk_mp_details , :NEW.pk_mp_details );
  END IF;

   IF NVL(:OLD.fk_milepayment ,0) !=
     NVL(:NEW.fk_milepayment ,0) THEN
     Audit_Trail.column_update
       (raid, 'fk_milepayment ',
       :OLD.fk_milepayment , :NEW.fk_milepayment );
  END IF;
    IF NVL(:OLD.mp_linkto_id ,0) !=
     NVL(:NEW.mp_linkto_id ,0) THEN
     Audit_Trail.column_update
       (raid, 'mp_linkto_id ',
       :OLD.mp_linkto_id , :NEW.mp_linkto_id );
  END IF;

  IF NVL(:OLD.mp_amount,0) !=
     NVL(:NEW.mp_amount,0) THEN
     Audit_Trail.column_update
       (raid, 'mp_amount',
       :OLD.mp_amount, :NEW.mp_amount);
  END IF;
   IF NVL(:OLD.mp_linkto_type,' ') !=
     NVL(:NEW.mp_linkto_type,' ') THEN
     Audit_Trail.column_update
       (raid, 'mp_linkto_type',
       :OLD.mp_linkto_type, :NEW.mp_linkto_type);
  END IF;

 IF NVL(:OLD.FK_MILEACHIEVED,0) !=
     NVL(:NEW.FK_MILEACHIEVED,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_MILEACHIEVED',
       :OLD.FK_MILEACHIEVED, :NEW.FK_MILEACHIEVED);
  END IF;

IF NVL(:OLD.MP_HOLDBACK_AMOUNT,0) !=
     NVL(:NEW.MP_HOLDBACK_AMOUNT,0) THEN
     Audit_Trail.column_update
       (raid, 'MP_HOLDBACK_AMOUNT',
       :OLD.MP_HOLDBACK_AMOUNT, :NEW.MP_HOLDBACK_AMOUNT);
  END IF;

old_modby := '';
new_modby := '';

  IF NVL(:OLD.last_modified_by,0) !=    NVL(:NEW.last_modified_by,0) THEN
      BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_modby := NULL ;
    END ;
	     Audit_Trail.column_update     (raid, 'LAST_MODIFIED_BY', old_modby, new_modby);
  END IF;

  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update     (raid, 'LAST_MODIFIED_DATE',    to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=     NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update (raid, 'CREATED_ON',       to_char(:old.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN Audit_Trail.column_update    (raid, 'RID',  :OLD.rid, :NEW.rid);
  END IF;

END;
/


