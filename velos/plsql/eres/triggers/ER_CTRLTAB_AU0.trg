create or replace
TRIGGER ER_CTRLTAB_AU0
AFTER UPDATE
ON ER_CTRLTAB REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  usr VARCHAR2(100);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  usr := getuser(:NEW.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_CTRLTAB', :OLD.rid, 'U', usr);
  IF NVL(:OLD.pk_ctrltab,0) !=
     NVL(:NEW.pk_ctrltab,0) THEN
     audit_trail.column_update
       (raid, 'PK_CTRLTAB',
       :OLD.pk_ctrltab, :NEW.pk_ctrltab);
  END IF;
  IF NVL(:OLD.ctrl_value,0) !=
     NVL(:NEW.ctrl_value,0) THEN
     audit_trail.column_update
       (raid, 'CTRL_VALUE',
       :OLD.ctrl_value, :NEW.ctrl_value);
  END IF;
  IF NVL(:OLD.ctrl_desc,0) !=
     NVL(:NEW.ctrl_desc,0) THEN
     audit_trail.column_update
       (raid, 'CTRL_DESC',
       :OLD.ctrl_desc, :NEW.ctrl_desc);
  END IF;  
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.ctrl_key,0) !=
     NVL(:NEW.ctrl_key,0) THEN
     audit_trail.column_update
       (raid, 'CTRL_KEY',
       :OLD.ctrl_key, :NEW.ctrl_key);
  END IF;
  IF NVL(:OLD.ctrl_seq,0) !=
     NVL(:NEW.ctrl_seq,0) THEN
     audit_trail.column_update
       (raid, 'CTRL_SEQ',
       :OLD.ctrl_seq, :NEW.ctrl_seq);
  END IF;
  IF NVL(:OLD.ctrl_custom_col1,0) !=
     NVL(:NEW.ctrl_custom_col1,0) THEN
     audit_trail.column_update
       (raid, 'CTRL_CUSTOM_COL1',
       :OLD.ctrl_custom_col1, :NEW.ctrl_custom_col1);
  END IF;
  IF NVL(:OLD.ctrl_custom_col2,0) !=
     NVL(:NEW.ctrl_custom_col2,0) THEN
     audit_trail.column_update
       (raid, 'CTRL_CUSTOM_COL1',
       :OLD.ctrl_custom_col2, :NEW.ctrl_custom_col2);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_modby := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
              to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.IP_ADD,' ') !=
     NVL(:NEW.IP_ADD,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.IP_ADD, :NEW.IP_ADD);
  END IF;
END;