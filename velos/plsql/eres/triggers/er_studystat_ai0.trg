CREATE OR REPLACE TRIGGER ER_STUDYSTAT_AI0
BEFORE INSERT
ON ER_STUDYSTAT REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
declare v_checkactive number(10);
      v_checkclose number(10);
      v_checkapproved number(10);

BEGIN

-- this part is handled via JSP now
--update the end date of previous status with the start date of current status

/*  update er_studystat
    set STUDYSTAT_ENDT = :new.studystat_date
     where fk_study = :new.fk_study
     and STUDYSTAT_ENDT is null; */


--update study start date if the status is active
select count(*) into v_checkactive
    from er_codelst
    where :new.fk_codelst_studystat = pk_codelst
    and codelst_type='studystat'
    and codelst_subtyp='active';


if (v_checkactive = 1) then
    update er_study
    set study_actualdt = :new.studystat_date,
    last_modified_by = :new.creator
    where pk_study = :new.fk_study
    and study_actualdt is null;
end if;


--update study end date if the status is permanent closure
select count(*) into v_checkclose
    from er_codelst
    where :new.fk_codelst_studystat = pk_codelst
    and codelst_type='studystat'
    and codelst_subtyp='prmnt_cls';


if (v_checkclose = 1) then
    update er_study
    set study_end_date = :new.studystat_date,
    last_modified_by = :new.creator
    where pk_study = :new.fk_study
    and study_end_date is null;
end if;


END;
/