CREATE  OR REPLACE TRIGGER ER_STUDYVER_BI0 BEFORE INSERT ON ER_STUDYVER        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_STUDYVER',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'FK_STUDY', :NEW.FK_STUDY);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'ORIG_STUDY', :NEW.ORIG_STUDY);
       Audit_Trail.column_insert (raid, 'PK_STUDYVER', :NEW.PK_STUDYVER);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'STUDYVER_CATEGORY', :NEW.STUDYVER_CATEGORY);
       Audit_Trail.column_insert (raid, 'STUDYVER_DATE', :NEW.STUDYVER_DATE);
       Audit_Trail.column_insert (raid, 'STUDYVER_NOTES', :NEW.STUDYVER_NOTES);
       Audit_Trail.column_insert (raid, 'STUDYVER_NUMBER', :NEW.STUDYVER_NUMBER);
       Audit_Trail.column_insert (raid, 'STUDYVER_STATUS', :NEW.STUDYVER_STATUS);
       Audit_Trail.column_insert (raid, 'STUDYVER_TYPE', :NEW.STUDYVER_TYPE);
COMMIT;
END;
/
