CREATE  OR REPLACE TRIGGER ER_FORMFLD_AD0 AFTER DELETE ON ER_FORMFLD        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_FORMFLD', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'FK_FIELD', :OLD.FK_FIELD);
       Audit_Trail.column_delete (raid, 'FK_FORMSEC', :OLD.FK_FORMSEC);
       Audit_Trail.column_delete (raid, 'FORMFLD_BROWSERFLG', :OLD.FORMFLD_BROWSERFLG);
       Audit_Trail.column_delete (raid, 'FORMFLD_FOREIGNKEY', :OLD.FORMFLD_FOREIGNKEY);
       Audit_Trail.column_delete (raid, 'FORMFLD_FOREIGNKEYFLAG', :OLD.FORMFLD_FOREIGNKEYFLAG);
       Audit_Trail.column_delete (raid, 'FORMFLD_JAVASCR', :OLD.FORMFLD_JAVASCR);
       Audit_Trail.column_delete (raid, 'FORMFLD_MANDATORY', :OLD.FORMFLD_MANDATORY);
       Audit_Trail.column_delete (raid, 'FORMFLD_OFFLINE', :OLD.FORMFLD_OFFLINE);
       Audit_Trail.column_delete (raid, 'FORMFLD_SEQ', :OLD.FORMFLD_SEQ);
       Audit_Trail.column_delete (raid, 'FORMFLD_XSL', :OLD.FORMFLD_XSL);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_FORMFLD', :OLD.PK_FORMFLD);
       Audit_Trail.column_delete (raid, 'RECORD_TYPE', :OLD.RECORD_TYPE);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/
