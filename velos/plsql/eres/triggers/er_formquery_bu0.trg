CREATE OR REPLACE TRIGGER "ERES"."ER_FORMQUERY_BU0" BEFORE UPDATE ON ER_FORMQUERY

FOR EACH ROW 
  WHEN (new.last_modified_by is not null) 
begin
 :new.last_modified_date := sysdate;
end;
/
