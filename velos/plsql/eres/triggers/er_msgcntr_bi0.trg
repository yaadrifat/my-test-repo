CREATE  OR REPLACE TRIGGER ER_MSGCNTR_BI0 BEFORE INSERT ON ER_MSGCNTR        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_MSGCNTR',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'FK_STUDY', :NEW.FK_STUDY);
       Audit_Trail.column_insert (raid, 'FK_STUDYVIEW', :NEW.FK_STUDYVIEW);
       Audit_Trail.column_insert (raid, 'FK_USERTO', :NEW.FK_USERTO);
       Audit_Trail.column_insert (raid, 'FK_USERX', :NEW.FK_USERX);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'MSGCNTR_PERM', :NEW.MSGCNTR_PERM);
       Audit_Trail.column_insert (raid, 'MSGCNTR_REQTYPE', :NEW.MSGCNTR_REQTYPE);
       Audit_Trail.column_insert (raid, 'MSGCNTR_STAT', :NEW.MSGCNTR_STAT);
       Audit_Trail.column_insert (raid, 'MSGCNTR_TEXT', :NEW.MSGCNTR_TEXT);
       Audit_Trail.column_insert (raid, 'PK_MSGCNTR', :NEW.PK_MSGCNTR);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/
