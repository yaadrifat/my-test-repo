CREATE  OR REPLACE TRIGGER ER_STUDYSITES_BI BEFORE INSERT ON ER_STUDYSITES        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;
 
 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_STUDYSITES',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'ENR_NOTIFYTO', :NEW.ENR_NOTIFYTO);
       Audit_Trail.column_insert (raid, 'ENR_STAT_NOTIFYTO', :NEW.ENR_STAT_NOTIFYTO); 
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_STUDYSITETYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_STUDYSITETYPE', codeList_desc);
       Audit_Trail.column_insert (raid, 'FK_SITE', :NEW.FK_SITE);
       Audit_Trail.column_insert (raid, 'FK_STUDY', :NEW.FK_STUDY);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'IS_REVIEWBASED_ENR', :NEW.IS_REVIEWBASED_ENR);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_ON', :NEW.LAST_MODIFIED_ON);
       Audit_Trail.column_insert (raid, 'PK_STUDYSITES', :NEW.PK_STUDYSITES);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'STUDYSITE_ENRCOUNT', :NEW.STUDYSITE_ENRCOUNT);
       Audit_Trail.column_insert (raid, 'STUDYSITE_LSAMPLESIZE', :NEW.STUDYSITE_LSAMPLESIZE);
       Audit_Trail.column_insert (raid, 'STUDYSITE_PUBLIC', :NEW.STUDYSITE_PUBLIC);
       Audit_Trail.column_insert (raid, 'STUDYSITE_REPINCLUDE', :NEW.STUDYSITE_REPINCLUDE);
COMMIT;
END;
/
