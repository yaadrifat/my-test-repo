
  CREATE OR REPLACE TRIGGER "ERES"."ER_PATSTUDYSTAT_BU0" 
BEFORE UPDATE ON ER_PATSTUDYSTAT REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW


when (new.last_modified_by is not null) begin
:new.last_modified_date := sysdate ; 


end;

/