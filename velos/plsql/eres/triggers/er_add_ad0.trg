CREATE  OR REPLACE TRIGGER ER_ADD_AD0 AFTER DELETE ON ER_ADD        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
  oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
       oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_ADD', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'ADDRESS', :OLD.ADDRESS);
       Audit_Trail.column_delete (raid, 'ADDRESS2', :OLD.ADDRESS2);
       Audit_Trail.column_delete (raid, 'ADD_CITY', :OLD.ADD_CITY);
       Audit_Trail.column_delete (raid, 'ADD_COUNTRY', :OLD.ADD_COUNTRY);
       Audit_Trail.column_delete (raid, 'ADD_COUNTY', :OLD.ADD_COUNTY);
       Audit_Trail.column_delete (raid, 'ADD_CREATOR', :OLD.ADD_CREATOR);
       Audit_Trail.column_delete (raid, 'ADD_EFF_DT', :OLD.ADD_EFF_DT);
       Audit_Trail.column_delete (raid, 'ADD_EMAIL', :OLD.ADD_EMAIL);
       Audit_Trail.column_delete (raid, 'ADD_EXT', :OLD.ADD_EXT);
       Audit_Trail.column_delete (raid, 'ADD_FAX', :OLD.ADD_FAX);
       Audit_Trail.column_delete (raid, 'ADD_MOBILE', :OLD.ADD_MOBILE);
       Audit_Trail.column_delete (raid, 'ADD_PAGER', :OLD.ADD_PAGER);
       Audit_Trail.column_delete (raid, 'ADD_PHONE', :OLD.ADD_PHONE);
       Audit_Trail.column_delete (raid, 'ADD_STATE', :OLD.ADD_STATE);
       Audit_Trail.column_delete (raid, 'ADD_TTY', :OLD.ADD_TTY);
       Audit_Trail.column_delete (raid, 'ADD_URL', :OLD.ADD_URL);
       Audit_Trail.column_delete (raid, 'ADD_ZIPCODE', :OLD.ADD_ZIPCODE);
       Audit_Trail.column_delete (raid, 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_ADDTYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_ADDTYPE', codeList_desc);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_ADD', :OLD.PK_ADD);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/