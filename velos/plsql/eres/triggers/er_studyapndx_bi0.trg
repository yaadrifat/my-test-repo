CREATE  OR REPLACE TRIGGER ER_STUDYAPNDX_BI0 BEFORE INSERT ON ER_STUDYAPNDX        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_STUDYAPNDX',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'FK_STUDY', :NEW.FK_STUDY);
       Audit_Trail.column_insert (raid, 'FK_STUDYVER', :NEW.FK_STUDYVER);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_STUDYAPNDX', :NEW.PK_STUDYAPNDX);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'STUDYAPNDX_DESC', :NEW.STUDYAPNDX_DESC);
       Audit_Trail.column_insert (raid, 'STUDYAPNDX_FILE', :NEW.STUDYAPNDX_FILE);
       --Audit_Trail.column_insert (raid, 'STUDYAPNDX_FILEOBJ', :NEW.STUDYAPNDX_FILEOBJ);
       Audit_Trail.column_insert (raid, 'STUDYAPNDX_FILESIZE', :NEW.STUDYAPNDX_FILESIZE);
       Audit_Trail.column_insert (raid, 'STUDYAPNDX_PUBFLAG', :NEW.STUDYAPNDX_PUBFLAG);
       Audit_Trail.column_insert (raid, 'STUDYAPNDX_TYPE', :NEW.STUDYAPNDX_TYPE);
       Audit_Trail.column_insert (raid, 'STUDYAPNDX_URI', :NEW.STUDYAPNDX_URI);
COMMIT;
END;
/
