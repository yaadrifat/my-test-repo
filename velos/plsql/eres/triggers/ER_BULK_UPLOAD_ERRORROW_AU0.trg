create or replace
TRIGGER ERES."ER_BULK_UPLOAD_ERRORROW_AU0"
   AFTER UPDATE
   ON ERES.ER_BULK_UPLOAD_ERRORROW    FOR EACH ROW
DECLARE
   raid           NUMBER (10);
   new_usr        VARCHAR2 (100);
   old_usr        VARCHAR2 (100);
   usr            VARCHAR2 (100);
   old_modby      VARCHAR2 (100);
   new_modby      VARCHAR2 (100);
   old_codetype   VARCHAR2 (200);
   new_codetype   VARCHAR2 (200);
BEGIN
   SELECT   seq_audit.NEXTVAL INTO raid FROM DUAL;

   usr := getuser (:NEW.last_modified_by);


   IF NVL (
         :OLD.last_modified_date,
         TO_DATE (pkg_dateutil.f_get_future_null_date_str,
                  pkg_dateutil.f_get_dateformat)
      ) !=
         NVL (
            :NEW.last_modified_date,
            TO_DATE (pkg_dateutil.f_get_future_null_date_str,
                     pkg_dateutil.f_get_dateformat)
         )
   THEN
      audit_trail.record_transaction (raid,
                                      'ER_BULK_UPLOAD_ERRORROW',
                                      :OLD.rid,
                                      'U',
                                      usr);
   END IF;

   IF NVL (:OLD.PK_BULK_UPLOAD_ERRORROW, 0) != NVL (:NEW.PK_BULK_UPLOAD_ERRORROW, 0)
   THEN
      audit_trail.column_update (raid,
                                 'PK_BULK_UPLOAD_ERRORROW',
                                 :OLD.PK_BULK_UPLOAD_ERRORROW,
                                 :NEW.PK_BULK_UPLOAD_ERRORROW);
   END IF;



   IF NVL (:OLD.FK_BULK_UPLOAD, 0) != NVL (:NEW.FK_BULK_UPLOAD, 0)
   THEN
      audit_trail.column_update (raid,
                                 'FK_BULK_UPLOAD',
                                 :OLD.FK_BULK_UPLOAD,
                                 :NEW.FK_BULK_UPLOAD);
   END IF;


   IF NVL (:OLD.FILE_ROW_NUMBER, 0) !=
         NVL (:NEW.FILE_ROW_NUMBER, 0)
   THEN
      audit_trail.column_update (raid,
                                 'FILE_ROW_NUMBER',
                                 :OLD.FILE_ROW_NUMBER,
                                 :NEW.FILE_ROW_NUMBER);
   END IF;




   IF NVL (:OLD.FILE_HEADER_ROWDATA, ' ') != NVL (:NEW.FILE_HEADER_ROWDATA, ' ')
   THEN
      audit_trail.column_update (raid,
                                 'FILE_HEADER_ROWDATA',
                                 :OLD.FILE_HEADER_ROWDATA,
                                 :NEW.FILE_HEADER_ROWDATA);
   END IF;

   IF NVL (:OLD.FILE_ERROR_ROWDATA, ' ') != NVL (:NEW.FILE_ERROR_ROWDATA, ' ')
   THEN
      audit_trail.column_update (raid,
                                 'FILE_ERROR_ROWDATA',
                                 :OLD.FILE_ERROR_ROWDATA,
                                 :NEW.FILE_ERROR_ROWDATA);
   END IF;


   IF NVL (:OLD.RID, 0) != NVL (:NEW.RID, 0)
   THEN
      audit_trail.column_update (raid,
                                 'RID',
                                 :OLD.RID,
                                 :NEW.RID);
   END IF;

   IF NVL (:OLD.CREATOR, 0) !=
         NVL (:NEW.CREATOR, 0)
   THEN
      audit_trail.column_update (raid,
                                 'CREATOR',
                                 :OLD.CREATOR,
                                 :NEW.CREATOR);
   END IF;

   IF NVL (:OLD.LAST_MODIFIED_BY, ' ') !=
         NVL (:NEW.LAST_MODIFIED_BY, ' ')
   THEN
      audit_trail.column_update (raid,
                                 'LAST_MODIFIED_BY',
                                 :OLD.LAST_MODIFIED_BY,
                                 :NEW.LAST_MODIFIED_BY);
   END IF;




   IF NVL (
         :OLD.last_modified_date,
         TO_DATE (pkg_dateutil.f_get_future_null_date_str,
                  pkg_dateutil.f_get_dateformat)
      ) !=
         NVL (
            :NEW.last_modified_date,
            TO_DATE (pkg_dateutil.f_get_future_null_date_str,
                     pkg_dateutil.f_get_dateformat)
         )
   THEN
      audit_trail.column_update (raid,
                                 'LAST_MODIFIED_DATE',
                                 :OLD.last_modified_date,
                                 :NEW.last_modified_date);
   END IF;

   IF NVL (
         :OLD.created_on,
         TO_DATE (pkg_dateutil.f_get_future_null_date_str,
                  pkg_dateutil.f_get_dateformat)
      ) !=
         NVL (
            :NEW.created_on,
            TO_DATE (pkg_dateutil.f_get_future_null_date_str,
                     pkg_dateutil.f_get_dateformat)
         )
   THEN
      audit_trail.column_update (raid,
                                 'CREATED_ON',
                                 :OLD.created_on,
                                 :NEW.created_on);
   END IF;
END;