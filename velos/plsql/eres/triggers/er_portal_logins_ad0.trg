CREATE  OR REPLACE TRIGGER ER_PORTAL_LOGINS_AD0 AFTER DELETE ON ER_PORTAL_LOGINS        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_PORTAL_LOGINS', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'FK_PORTAL', :OLD.FK_PORTAL);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_PORTAL_LOGIN', :OLD.PK_PORTAL_LOGIN);
       Audit_Trail.column_delete (raid, 'PL_ID', :OLD.PL_ID);
       Audit_Trail.column_delete (raid, 'PL_ID_TYPE', :OLD.PL_ID_TYPE);
       Audit_Trail.column_delete (raid, 'PL_LOGIN', :OLD.PL_LOGIN);
       Audit_Trail.column_delete (raid, 'PL_LOGOUT_TIME', :OLD.PL_LOGOUT_TIME);
       Audit_Trail.column_delete (raid, 'PL_PASSWORD', :OLD.PL_PASSWORD);
       Audit_Trail.column_delete (raid, 'PL_STATUS', :OLD.PL_STATUS);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/
