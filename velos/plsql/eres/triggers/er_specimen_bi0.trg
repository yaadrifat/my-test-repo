CREATE  OR REPLACE TRIGGER ER_SPECIMEN_BI0 BEFORE INSERT ON ER_SPECIMEN        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_SPECIMEN',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'FK_ACCOUNT', :NEW.FK_ACCOUNT);
       Audit_Trail.column_insert (raid, 'FK_PER', :NEW.FK_PER);
       Audit_Trail.column_insert (raid, 'FK_SCH_EVENTS1', :NEW.FK_SCH_EVENTS1);
       Audit_Trail.column_insert (raid, 'FK_SITE', :NEW.FK_SITE);
       Audit_Trail.column_insert (raid, 'FK_SPECIMEN', :NEW.FK_SPECIMEN);
       Audit_Trail.column_insert (raid, 'FK_STORAGE', :NEW.FK_STORAGE);
       Audit_Trail.column_insert (raid, 'FK_STORAGE_KIT', :NEW.FK_STORAGE_KIT);
       Audit_Trail.column_insert (raid, 'FK_STORAGE_KIT_COMPONENT', :NEW.FK_STORAGE_KIT_COMPONENT);
       Audit_Trail.column_insert (raid, 'FK_STUDY', :NEW.FK_STUDY);
       Audit_Trail.column_insert (raid, 'FK_USER_COLLTECH', :NEW.FK_USER_COLLTECH);
       Audit_Trail.column_insert (raid, 'FK_USER_PATH', :NEW.FK_USER_PATH);
       Audit_Trail.column_insert (raid, 'FK_USER_PROCTECH', :NEW.FK_USER_PROCTECH);
       Audit_Trail.column_insert (raid, 'FK_USER_SURG', :NEW.FK_USER_SURG);
       Audit_Trail.column_insert (raid, 'FK_VISIT', :NEW.FK_VISIT);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_SPECIMEN', :NEW.PK_SPECIMEN);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'SPEC_ALTERNATE_ID', :NEW.SPEC_ALTERNATE_ID);
       Audit_Trail.column_insert (raid, 'SPEC_ANATOMIC_SITE', :NEW.SPEC_ANATOMIC_SITE);
       Audit_Trail.column_insert (raid, 'SPEC_BASE_ORIGQ_UNITS', :NEW.SPEC_BASE_ORIGQ_UNITS);
       Audit_Trail.column_insert (raid, 'SPEC_BASE_ORIG_QUANTITY', :NEW.SPEC_BASE_ORIG_QUANTITY);
       Audit_Trail.column_insert (raid, 'SPEC_COLLECTION_DATE', :NEW.SPEC_COLLECTION_DATE);
       Audit_Trail.column_insert (raid, 'SPEC_DESCRIPTION', :NEW.SPEC_DESCRIPTION);
       Audit_Trail.column_insert (raid, 'SPEC_DISPOSITION', :NEW.SPEC_DISPOSITION);
       Audit_Trail.column_insert (raid, 'SPEC_DNA_SAM_AVAILCODE', :NEW.SPEC_DNA_SAM_AVAILCODE);
       Audit_Trail.column_insert (raid, 'SPEC_ENVT_CONS', :NEW.SPEC_ENVT_CONS);
       Audit_Trail.column_insert (raid, 'SPEC_EXPECTEDQ_UNITS', :NEW.SPEC_EXPECTEDQ_UNITS);
       Audit_Trail.column_insert (raid, 'SPEC_EXPECTED_QUANTITY', :NEW.SPEC_EXPECTED_QUANTITY);
       Audit_Trail.column_insert (raid, 'SPEC_FREEZE_TIME', :NEW.SPEC_FREEZE_TIME);
       Audit_Trail.column_insert (raid, 'SPEC_ID', :NEW.SPEC_ID);
       Audit_Trail.column_insert (raid, 'SPEC_NOTES', :NEW.SPEC_NOTES);
       Audit_Trail.column_insert (raid, 'SPEC_ORIGINAL_QUANTITY', :NEW.SPEC_ORIGINAL_QUANTITY);
       Audit_Trail.column_insert (raid, 'SPEC_OTH_SAM_AVAILCODE', :NEW.SPEC_OTH_SAM_AVAILCODE);
       Audit_Trail.column_insert (raid, 'SPEC_OWNER_FK_USER', :NEW.SPEC_OWNER_FK_USER);
       Audit_Trail.column_insert (raid, 'SPEC_PATHOLOGY_STAT', :NEW.SPEC_PATHOLOGY_STAT);
       Audit_Trail.column_insert (raid, 'SPEC_PROCESSING_TYPE', :NEW.SPEC_PROCESSING_TYPE);
       Audit_Trail.column_insert (raid, 'SPEC_QCSAMPLEIND', :NEW.SPEC_QCSAMPLEIND);
       Audit_Trail.column_insert (raid, 'SPEC_QUANTITY_UNITS', :NEW.SPEC_QUANTITY_UNITS);
       Audit_Trail.column_insert (raid, 'SPEC_REMOVAL_TIME', :NEW.SPEC_REMOVAL_TIME);
       Audit_Trail.column_insert (raid, 'SPEC_SEQUENCE', :NEW.SPEC_SEQUENCE);
       Audit_Trail.column_insert (raid, 'SPEC_STORAGE_TEMP', :NEW.SPEC_STORAGE_TEMP);
       Audit_Trail.column_insert (raid, 'SPEC_STORAGE_TEMP_UNIT', :NEW.SPEC_STORAGE_TEMP_UNIT);
       Audit_Trail.column_insert (raid, 'SPEC_TISSUE_SIDE', :NEW.SPEC_TISSUE_SIDE);
       Audit_Trail.column_insert (raid, 'SPEC_TYPE', :NEW.SPEC_TYPE);
COMMIT;
END;
/
