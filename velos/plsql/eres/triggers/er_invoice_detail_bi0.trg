CREATE  OR REPLACE TRIGGER ER_INVOICE_DETAIL_BI0 BEFORE INSERT ON ER_INVOICE_DETAIL        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_INVOICE_DETAIL',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'ACHIEVED_ON', :NEW.ACHIEVED_ON);
       Audit_Trail.column_insert (raid, 'AMOUNT_DUE', :NEW.AMOUNT_DUE);
       Audit_Trail.column_insert (raid, 'AMOUNT_INVOICED', :NEW.AMOUNT_INVOICED);
       Audit_Trail.column_insert (raid, 'AMOUNT_HOLDBACK', :NEW.AMOUNT_HOLDBACK);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'DETAIL_TYPE', :NEW.DETAIL_TYPE);
       Audit_Trail.column_insert (raid, 'DISPLAY_DETAIL', :NEW.DISPLAY_DETAIL);
       Audit_Trail.column_insert (raid, 'FK_INV', :NEW.FK_INV);
       Audit_Trail.column_insert (raid, 'FK_MILEACHIEVED', :NEW.FK_MILEACHIEVED);
       Audit_Trail.column_insert (raid, 'FK_MILESTONE', :NEW.FK_MILESTONE);
       Audit_Trail.column_insert (raid, 'FK_PATPROT', :NEW.FK_PATPROT);
       Audit_Trail.column_insert (raid, 'FK_PER', :NEW.FK_PER);
       Audit_Trail.column_insert (raid, 'FK_STUDY', :NEW.FK_STUDY);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'MILESTONES_ACHIEVED', :NEW.MILESTONES_ACHIEVED);
       Audit_Trail.column_insert (raid, 'PAYMENT_DUEDATE', :NEW.PAYMENT_DUEDATE);
       Audit_Trail.column_insert (raid, 'PK_INVDETAIL', :NEW.PK_INVDETAIL);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/
