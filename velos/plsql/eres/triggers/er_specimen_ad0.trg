CREATE  OR REPLACE TRIGGER ER_SPECIMEN_AD0 AFTER DELETE ON ER_SPECIMEN        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_SPECIMEN', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'FK_ACCOUNT', :OLD.FK_ACCOUNT);
       Audit_Trail.column_delete (raid, 'FK_PER', :OLD.FK_PER);
       Audit_Trail.column_delete (raid, 'FK_SCH_EVENTS1', :OLD.FK_SCH_EVENTS1);
       Audit_Trail.column_delete (raid, 'FK_SITE', :OLD.FK_SITE);
       Audit_Trail.column_delete (raid, 'FK_SPECIMEN', :OLD.FK_SPECIMEN);
       Audit_Trail.column_delete (raid, 'FK_STORAGE', :OLD.FK_STORAGE);
       Audit_Trail.column_delete (raid, 'FK_STORAGE_KIT', :OLD.FK_STORAGE_KIT);
       Audit_Trail.column_delete (raid, 'FK_STORAGE_KIT_COMPONENT', :OLD.FK_STORAGE_KIT_COMPONENT);
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'FK_USER_COLLTECH', :OLD.FK_USER_COLLTECH);
       Audit_Trail.column_delete (raid, 'FK_USER_PATH', :OLD.FK_USER_PATH);
       Audit_Trail.column_delete (raid, 'FK_USER_PROCTECH', :OLD.FK_USER_PROCTECH);
       Audit_Trail.column_delete (raid, 'FK_USER_SURG', :OLD.FK_USER_SURG);
       Audit_Trail.column_delete (raid, 'FK_VISIT', :OLD.FK_VISIT);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_SPECIMEN', :OLD.PK_SPECIMEN);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'SPEC_ALTERNATE_ID', :OLD.SPEC_ALTERNATE_ID);
       Audit_Trail.column_delete (raid, 'SPEC_ANATOMIC_SITE', :OLD.SPEC_ANATOMIC_SITE);
       Audit_Trail.column_delete (raid, 'SPEC_BASE_ORIGQ_UNITS', :OLD.SPEC_BASE_ORIGQ_UNITS);
       Audit_Trail.column_delete (raid, 'SPEC_BASE_ORIG_QUANTITY', :OLD.SPEC_BASE_ORIG_QUANTITY);
       Audit_Trail.column_delete (raid, 'SPEC_COLLECTION_DATE', :OLD.SPEC_COLLECTION_DATE);
       Audit_Trail.column_delete (raid, 'SPEC_DESCRIPTION', :OLD.SPEC_DESCRIPTION);
       Audit_Trail.column_delete (raid, 'SPEC_DISPOSITION', :OLD.SPEC_DISPOSITION);
       Audit_Trail.column_delete (raid, 'SPEC_DNA_SAM_AVAILCODE', :OLD.SPEC_DNA_SAM_AVAILCODE);
       Audit_Trail.column_delete (raid, 'SPEC_ENVT_CONS', :OLD.SPEC_ENVT_CONS);
       Audit_Trail.column_delete (raid, 'SPEC_EXPECTEDQ_UNITS', :OLD.SPEC_EXPECTEDQ_UNITS);
       Audit_Trail.column_delete (raid, 'SPEC_EXPECTED_QUANTITY', :OLD.SPEC_EXPECTED_QUANTITY);
       Audit_Trail.column_delete (raid, 'SPEC_FREEZE_TIME', :OLD.SPEC_FREEZE_TIME);
       Audit_Trail.column_delete (raid, 'SPEC_ID', :OLD.SPEC_ID);
       Audit_Trail.column_delete (raid, 'SPEC_NOTES', :OLD.SPEC_NOTES);
       Audit_Trail.column_delete (raid, 'SPEC_ORIGINAL_QUANTITY', :OLD.SPEC_ORIGINAL_QUANTITY);
       Audit_Trail.column_delete (raid, 'SPEC_OTH_SAM_AVAILCODE', :OLD.SPEC_OTH_SAM_AVAILCODE);
       Audit_Trail.column_delete (raid, 'SPEC_OWNER_FK_USER', :OLD.SPEC_OWNER_FK_USER);
       Audit_Trail.column_delete (raid, 'SPEC_PATHOLOGY_STAT', :OLD.SPEC_PATHOLOGY_STAT);
       Audit_Trail.column_delete (raid, 'SPEC_PROCESSING_TYPE', :OLD.SPEC_PROCESSING_TYPE);
       Audit_Trail.column_delete (raid, 'SPEC_QCSAMPLEIND', :OLD.SPEC_QCSAMPLEIND);
       Audit_Trail.column_delete (raid, 'SPEC_QUANTITY_UNITS', :OLD.SPEC_QUANTITY_UNITS);
       Audit_Trail.column_delete (raid, 'SPEC_REMOVAL_TIME', :OLD.SPEC_REMOVAL_TIME);
       Audit_Trail.column_delete (raid, 'SPEC_SEQUENCE', :OLD.SPEC_SEQUENCE);
       Audit_Trail.column_delete (raid, 'SPEC_STORAGE_TEMP', :OLD.SPEC_STORAGE_TEMP);
       Audit_Trail.column_delete (raid, 'SPEC_STORAGE_TEMP_UNIT', :OLD.SPEC_STORAGE_TEMP_UNIT);
       Audit_Trail.column_delete (raid, 'SPEC_TISSUE_SIDE', :OLD.SPEC_TISSUE_SIDE);
       Audit_Trail.column_delete (raid, 'SPEC_TYPE', :OLD.SPEC_TYPE);
COMMIT;
END;
/
