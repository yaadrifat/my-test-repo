CREATE  OR REPLACE TRIGGER ER_ADD_BI0 BEFORE INSERT ON ER_ADD        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_ADD',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'ADDRESS', :NEW.ADDRESS);
       Audit_Trail.column_insert (raid, 'ADDRESS2', :NEW.ADDRESS2);
       Audit_Trail.column_insert (raid, 'ADD_CITY', :NEW.ADD_CITY);
       Audit_Trail.column_insert (raid, 'ADD_COUNTRY', :NEW.ADD_COUNTRY);
       Audit_Trail.column_insert (raid, 'ADD_COUNTY', :NEW.ADD_COUNTY);
       Audit_Trail.column_insert (raid, 'ADD_CREATOR', :NEW.ADD_CREATOR);
       Audit_Trail.column_insert (raid, 'ADD_EFF_DT', :NEW.ADD_EFF_DT);
       Audit_Trail.column_insert (raid, 'ADD_EMAIL', :NEW.ADD_EMAIL);
       Audit_Trail.column_insert (raid, 'ADD_EXT', :NEW.ADD_EXT);
       Audit_Trail.column_insert (raid, 'ADD_FAX', :NEW.ADD_FAX);
       Audit_Trail.column_insert (raid, 'ADD_MOBILE', :NEW.ADD_MOBILE);
       Audit_Trail.column_insert (raid, 'ADD_PAGER', :NEW.ADD_PAGER);
       Audit_Trail.column_insert (raid, 'ADD_PHONE', :NEW.ADD_PHONE);
       Audit_Trail.column_insert (raid, 'ADD_STATE', :NEW.ADD_STATE);
       Audit_Trail.column_insert (raid, 'ADD_TTY', :NEW.ADD_TTY);
       Audit_Trail.column_insert (raid, 'ADD_URL', :NEW.ADD_URL);
       Audit_Trail.column_insert (raid, 'ADD_ZIPCODE', :NEW.ADD_ZIPCODE);
       Audit_Trail.column_insert (raid, 'CREATED_ON', TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_ADDTYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_ADDTYPE', codeList_desc);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_ADD', :NEW.PK_ADD);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/