CREATE  OR REPLACE TRIGGER ER_CTRP_DRAFT_AD0 AFTER DELETE ON ER_CTRP_DRAFT        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
  oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
       oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_CTRP_DRAFT', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'AMEND_DATE', :OLD.AMEND_DATE);
       Audit_Trail.column_delete (raid, 'AMEND_NUMBER', :OLD.AMEND_NUMBER);
       Audit_Trail.column_delete (raid, 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'CTRP_DRAFT_TYPE', :OLD.CTRP_DRAFT_TYPE);
       Audit_Trail.column_delete (raid, 'CTRP_STUDYACC_CLOSED_DATE', :OLD.CTRP_STUDYACC_CLOSED_DATE);
       Audit_Trail.column_delete (raid, 'CTRP_STUDYACC_OPEN_DATE', :OLD.CTRP_STUDYACC_OPEN_DATE);
       Audit_Trail.column_delete (raid, 'CTRP_STUDY_COMP_DATE', :OLD.CTRP_STUDY_COMP_DATE);
       Audit_Trail.column_delete (raid, 'CTRP_STUDY_START_DATE', :OLD.CTRP_STUDY_START_DATE);
       Audit_Trail.column_delete (raid, 'CTRP_STUDY_STATUS_DATE', :OLD.CTRP_STUDY_STATUS_DATE);
       Audit_Trail.column_delete (raid, 'CTRP_STUDY_STOP_REASON', :OLD.CTRP_STUDY_STOP_REASON);
       Audit_Trail.column_delete (raid, 'DELAY_POST_INDICATOR', :OLD.DELAY_POST_INDICATOR);
       Audit_Trail.column_delete (raid, 'DELETED_FLAG', :OLD.DELETED_FLAG);
       Audit_Trail.column_delete (raid, 'DM_APPOINT_INDICATOR', :OLD.DM_APPOINT_INDICATOR);
       Audit_Trail.column_delete (raid, 'DRAFT_PILOT', :OLD.DRAFT_PILOT);
       Audit_Trail.column_delete (raid, 'DRAFT_XML_REQD', :OLD.DRAFT_XML_REQD);
       Audit_Trail.column_delete (raid, 'FDA_INTVEN_INDICATOR', :OLD.FDA_INTVEN_INDICATOR);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_CTRP_STUDY_STATUS;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_CTRP_STUDY_STATUS', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_DISEASE_SITE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_DISEASE_SITE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_INTERVENTION_TYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_INTERVENTION_TYPE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_PHASE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_PHASE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_SUBMISSION_TYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_SUBMISSION_TYPE', codeList_desc);
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'FK_TRIAL_OWNER', :OLD.FK_TRIAL_OWNER);
       Audit_Trail.column_delete (raid, 'INTERVENTION_NAME', :OLD.INTERVENTION_NAME);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'IS_COMP_ACTUAL', :OLD.IS_COMP_ACTUAL);
       Audit_Trail.column_delete (raid, 'IS_START_ACTUAL', :OLD.IS_START_ACTUAL);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'LEAD_ORG_CTRPID', :OLD.LEAD_ORG_CTRPID);
       Audit_Trail.column_delete (raid, 'LEAD_ORG_FK_ADD', :OLD.LEAD_ORG_FK_ADD);
       Audit_Trail.column_delete (raid, 'LEAD_ORG_FK_SITE', :OLD.LEAD_ORG_FK_SITE);
       Audit_Trail.column_delete (raid, 'LEAD_ORG_STUDYID', :OLD.LEAD_ORG_STUDYID);
       Audit_Trail.column_delete (raid, 'LEAD_ORG_TYPE', :OLD.LEAD_ORG_TYPE);
       Audit_Trail.column_delete (raid, 'NCI_STUDYID', :OLD.NCI_STUDYID);
       Audit_Trail.column_delete (raid, 'NCT_NUMBER', :OLD.NCT_NUMBER);
       Audit_Trail.column_delete (raid, 'OTHER_NUMBER', :OLD.OTHER_NUMBER);
       Audit_Trail.column_delete (raid, 'OVERSIGHT_COUNTRY', :OLD.OVERSIGHT_COUNTRY);
       Audit_Trail.column_delete (raid, 'OVERSIGHT_ORGANIZATION', :OLD.OVERSIGHT_ORGANIZATION);
       Audit_Trail.column_delete (raid, 'PI_CTRPID', :OLD.PI_CTRPID);
       Audit_Trail.column_delete (raid, 'PI_FIRST_NAME', :OLD.PI_FIRST_NAME);
       Audit_Trail.column_delete (raid, 'PI_FK_ADD', :OLD.PI_FK_ADD);
       Audit_Trail.column_delete (raid, 'PI_FK_USER', :OLD.PI_FK_USER);
       Audit_Trail.column_delete (raid, 'PI_LAST_NAME', :OLD.PI_LAST_NAME);
       Audit_Trail.column_delete (raid, 'PK_CTRP_DRAFT', :OLD.PK_CTRP_DRAFT);
       Audit_Trail.column_delete (raid, 'RESPONSIBLE_PARTY', :OLD.RESPONSIBLE_PARTY);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'RP_EMAIL', :OLD.RP_EMAIL);
       Audit_Trail.column_delete (raid, 'RP_EXTN', :OLD.RP_EXTN);
       Audit_Trail.column_delete (raid, 'RP_PHONE', :OLD.RP_PHONE);
       Audit_Trail.column_delete (raid, 'RP_SPONSOR_CONTACT_ID', :OLD.RP_SPONSOR_CONTACT_ID);
       Audit_Trail.column_delete (raid, 'RP_SPONSOR_CONTACT_TYPE', :OLD.RP_SPONSOR_CONTACT_TYPE);
       Audit_Trail.column_delete (raid, 'SECTION_801_INDICATOR', :OLD.SECTION_801_INDICATOR);
       Audit_Trail.column_delete (raid, 'SITE_TARGET_ACCRUAL', :OLD.SITE_TARGET_ACCRUAL);
       Audit_Trail.column_delete (raid, 'SPONSOR_CTRPID', :OLD.SPONSOR_CTRPID);
       Audit_Trail.column_delete (raid, 'SPONSOR_FK_ADD', :OLD.SPONSOR_FK_ADD);
       Audit_Trail.column_delete (raid, 'SPONSOR_FK_SITE', :OLD.SPONSOR_FK_SITE);
       Audit_Trail.column_delete (raid, 'SPONSOR_GENERIC_TITLE', :OLD.SPONSOR_GENERIC_TITLE);
       Audit_Trail.column_delete (raid, 'SPONSOR_PERSONAL_FIRST_NAME', :OLD.SPONSOR_PERSONAL_FIRST_NAME);
       Audit_Trail.column_delete (raid, 'SPONSOR_PERSONAL_FK_ADD', :OLD.SPONSOR_PERSONAL_FK_ADD);
       Audit_Trail.column_delete (raid, 'SPONSOR_PERSONAL_FK_USER', :OLD.SPONSOR_PERSONAL_FK_USER);
       Audit_Trail.column_delete (raid, 'SPONSOR_PERSONAL_LAST_NAME', :OLD.SPONSOR_PERSONAL_LAST_NAME);
       Audit_Trail.column_delete (raid, 'SPONSOR_PROGRAM_CODE', :OLD.SPONSOR_PROGRAM_CODE);
       Audit_Trail.column_delete (raid, 'STUDY_PURPOSE', :OLD.STUDY_PURPOSE);
       Audit_Trail.column_delete (raid, 'STUDY_PURPOSE_OTHER', :OLD.STUDY_PURPOSE_OTHER);
       Audit_Trail.column_delete (raid, 'STUDY_TYPE', :OLD.STUDY_TYPE);
       Audit_Trail.column_delete (raid, 'SUBMIT_NCI_DESIGNATED', :OLD.SUBMIT_NCI_DESIGNATED);
       Audit_Trail.column_delete (raid, 'SUBMIT_ORG_CTRPID', :OLD.SUBMIT_ORG_CTRPID);
       Audit_Trail.column_delete (raid, 'SUBMIT_ORG_FK_ADD', :OLD.SUBMIT_ORG_FK_ADD);
       Audit_Trail.column_delete (raid, 'SUBMIT_ORG_FK_SITE', :OLD.SUBMIT_ORG_FK_SITE);
       Audit_Trail.column_delete (raid, 'SUBMIT_ORG_STUDYID', :OLD.SUBMIT_ORG_STUDYID);
       Audit_Trail.column_delete (raid, 'SUBMIT_ORG_TYPE', :OLD.SUBMIT_ORG_TYPE);
       Audit_Trail.column_delete (raid, 'SUMM4_SPONSOR_FK_ADD', :OLD.SUMM4_SPONSOR_FK_ADD);
       Audit_Trail.column_delete (raid, 'SUMM4_SPONSOR_FK_SITE', :OLD.SUMM4_SPONSOR_FK_SITE);
       Audit_Trail.column_delete (raid, 'SUMM4_SPONSOR_ID', :OLD.SUMM4_SPONSOR_ID);
       Audit_Trail.column_delete (raid, 'SUMM4_SPONSOR_TYPE', :OLD.SUMM4_SPONSOR_TYPE);
COMMIT;
END;
/
