CREATE  OR REPLACE TRIGGER ER_STUDYSEC_AD0 AFTER DELETE ON ER_STUDYSEC        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_STUDYSEC', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'FK_STUDYVER', :OLD.FK_STUDYVER);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_STUDYSEC', :OLD.PK_STUDYSEC);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'STUDYSEC_CONTENTS', :OLD.STUDYSEC_CONTENTS);
       Audit_Trail.column_delete (raid, 'STUDYSEC_CONTENTS2', :OLD.STUDYSEC_CONTENTS2);
       Audit_Trail.column_delete (raid, 'STUDYSEC_CONTENTS3', :OLD.STUDYSEC_CONTENTS3);
       Audit_Trail.column_delete (raid, 'STUDYSEC_CONTENTS4', :OLD.STUDYSEC_CONTENTS4);
       Audit_Trail.column_delete (raid, 'STUDYSEC_CONTENTS5', :OLD.STUDYSEC_CONTENTS5);
       Audit_Trail.column_delete (raid, 'STUDYSEC_NAME', :OLD.STUDYSEC_NAME);
       Audit_Trail.column_delete (raid, 'STUDYSEC_NUM', :OLD.STUDYSEC_NUM);
       Audit_Trail.column_delete (raid, 'STUDYSEC_PUBFLAG', :OLD.STUDYSEC_PUBFLAG);
       Audit_Trail.column_delete (raid, 'STUDYSEC_SEQ', :OLD.STUDYSEC_SEQ);
      -- Audit_Trail.column_delete (raid, 'STUDYSEC_TEXT', :OLD.STUDYSEC_TEXT);
COMMIT;
END;
/
