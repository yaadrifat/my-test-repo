create or replace
TRIGGER ERES.ER_STUDYVER_AU0
  after update of
  pk_studyver,
  fk_study,
  studyver_number,
  studyver_status,
  studyver_notes,
  orig_study,
  creator,
  created_on,
  ip_add,
  studyver_date,
  studyver_category,
  studyver_type,
  rid
  on er_studyver
  for each row
declare
  raid number(10);
  usr varchar(200);
begin
  select seq_audit.nextval into raid from dual;
  usr := Getuser(:NEW.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_STUDYVER', :old.rid, 'U', usr);

  if nvl(:old.pk_studyver,0) !=
     NVL(:new.pk_studyver,0) then
     audit_trail.column_update
       (raid, 'PK_STUDYVER',
       :old.pk_studyver, :new.pk_studyver);
  end if;
  if nvl(:old.fk_study,0) !=
     NVL(:new.fk_study,0) then
     audit_trail.column_update
       (raid, 'FK_STUDY',
       :old.fk_study, :new.fk_study);
  end if;
  if nvl(:old.studyver_number,' ') !=
     NVL(:new.studyver_number,' ') then
     audit_trail.column_update
       (raid, 'STUDYVER_NUMBER',
       :old.studyver_number, :new.studyver_number);
  end if;
  if nvl(:old.studyver_status,' ') !=
     NVL(:new.studyver_status,' ') then
     audit_trail.column_update
       (raid, 'STUDYVER_STATUS',
       :old.studyver_status, :new.studyver_status);
  end if;
  if nvl(:old.studyver_notes,' ') !=
     NVL(:new.studyver_notes,' ') then
     audit_trail.column_update
       (raid, 'STUDYVER_NOTES',
       :old.studyver_notes, :new.studyver_notes);
  end if;
  
  if nvl(:old.studyver_category,0) !=
     NVL(:new.studyver_category,0) then
     audit_trail.column_update
       (raid, 'STUDYVER_CATEGORY',
       :old.studyver_category, :new.studyver_category);
  end if;
  
 if nvl(:old.studyver_type,0) !=
     NVL(:new.studyver_type,0) then
     audit_trail.column_update
       (raid, 'STUDYVER_TYPE',
       :old.studyver_type, :new.studyver_type);
  end if;
  
 if nvl(:old.studyver_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.studyver_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'STUDYVER_DATE',
          to_char(:OLD.studyver_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.studyver_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  --PK-Bug 4773:  STUDYVER_CATEGORY,STUDYVER_TYPE,STUDYVER_DATE if condition added 
  
  if nvl(:old.orig_study,0) !=
     NVL(:new.orig_study,0) then
     audit_trail.column_update
       (raid, 'ORIG_STUDY',
       :old.orig_study, :new.orig_study);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :old.last_modified_by, :new.last_modified_by);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ip_add,' ') !=
     NVL(:new.ip_add,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
  end if;

end;
/