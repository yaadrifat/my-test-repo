CREATE  OR REPLACE TRIGGER ER_PATSTUDYSTAT_AD0 AFTER DELETE ON ER_PATSTUDYSTAT        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
  oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
       oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_PATSTUDYSTAT', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'CURRENT_STAT', :OLD.CURRENT_STAT);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_STAT;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_STAT', codeList_desc);
       Audit_Trail.column_delete (raid, 'FK_PER', :OLD.FK_PER);
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'INFORM_CONSENT_VER', :OLD.INFORM_CONSENT_VER);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'NEXT_FOLLOWUP_ON', :OLD.NEXT_FOLLOWUP_ON);
       Audit_Trail.column_delete (raid, 'PATSTUDYSTAT_DATE', :OLD.PATSTUDYSTAT_DATE);
       Audit_Trail.column_delete (raid, 'PATSTUDYSTAT_ENDT', :OLD.PATSTUDYSTAT_ENDT);
       Audit_Trail.column_delete (raid, 'PATSTUDYSTAT_NOTE', :OLD.PATSTUDYSTAT_NOTE);
       Audit_Trail.column_delete (raid, 'PATSTUDYSTAT_REASON', :OLD.PATSTUDYSTAT_REASON);
       Audit_Trail.column_delete (raid, 'PK_PATSTUDYSTAT', :OLD.PK_PATSTUDYSTAT);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'SCREENED_BY', :OLD.SCREENED_BY);
       Audit_Trail.column_delete (raid, 'SCREENING_OUTCOME', :OLD.SCREENING_OUTCOME);
       Audit_Trail.column_delete (raid, 'SCREEN_NUMBER', :OLD.SCREEN_NUMBER);
COMMIT;
END;
/