CREATE  OR REPLACE TRIGGER ER_MILEPAYMENT_AD0 AFTER DELETE ON ER_MILEPAYMENT        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_MILEPAYMENT', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'FK_MILESTONE', :OLD.FK_MILESTONE);
       Audit_Trail.column_delete (raid, 'FK_STUDY', :OLD.FK_STUDY);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'MILEPAYMENT_AMT', :OLD.MILEPAYMENT_AMT);
       Audit_Trail.column_delete (raid, 'MILEPAYMENT_COMMENTS', :OLD.MILEPAYMENT_COMMENTS);
       Audit_Trail.column_delete (raid, 'MILEPAYMENT_DATE', :OLD.MILEPAYMENT_DATE);
       Audit_Trail.column_delete (raid, 'MILEPAYMENT_DELFLAG', :OLD.MILEPAYMENT_DELFLAG);
       Audit_Trail.column_delete (raid, 'MILEPAYMENT_DESC', :OLD.MILEPAYMENT_DESC);
       Audit_Trail.column_delete (raid, 'MILEPAYMENT_TYPE', :OLD.MILEPAYMENT_TYPE);
       Audit_Trail.column_delete (raid, 'PK_MILEPAYMENT', :OLD.PK_MILEPAYMENT);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/
