CREATE  OR REPLACE TRIGGER ER_STUDY_AD0 AFTER DELETE ON ER_STUDY        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
  oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;

            BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR;
      EXCEPTION WHEN NO_DATA_FOUND THEN
       oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_STUDY', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr);
       Audit_Trail.column_delete (raid, 'FDA_REGULATED_STUDY', :OLD.FDA_REGULATED_STUDY);
       Audit_Trail.column_delete (raid, 'FK_ACCOUNT', :OLD.FK_ACCOUNT);
       Audit_Trail.column_delete (raid, 'FK_AUTHOR', :OLD.FK_AUTHOR);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_BLIND;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_BLIND', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_CURRENCY;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_CURRENCY', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_PHASE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_PHASE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_RANDOM;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_RANDOM', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_RESTYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_RESTYPE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_SCOPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_SCOPE', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_SET_MILESTATUS;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_SET_MILESTATUS', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_SPONSOR;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_SPONSOR', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_TAREA;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_TAREA', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:OLD.FK_CODELST_TYPE;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_delete (raid, 'FK_CODELST_TYPE', codeList_desc);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_STUDY', :OLD.PK_STUDY);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
       Audit_Trail.column_delete (raid, 'STUDY_ACTUALDT', :OLD.STUDY_ACTUALDT);
       Audit_Trail.column_delete (raid, 'STUDY_ADVLKP_VER', :OLD.STUDY_ADVLKP_VER);
       Audit_Trail.column_delete (raid, 'STUDY_ASSOC', :OLD.STUDY_ASSOC);
       Audit_Trail.column_delete (raid, 'STUDY_CONTACT', :OLD.STUDY_CONTACT);
       Audit_Trail.column_delete (raid, 'STUDY_COORDINATOR', :OLD.STUDY_COORDINATOR);
       Audit_Trail.column_delete (raid, 'STUDY_CREATION_TYPE', :OLD.STUDY_CREATION_TYPE);
       Audit_Trail.column_delete (raid, 'STUDY_CTRP_REPORTABLE', :OLD.STUDY_CTRP_REPORTABLE);
       Audit_Trail.column_delete (raid, 'STUDY_CURRENT', :OLD.STUDY_CURRENT);
       Audit_Trail.column_delete (raid, 'STUDY_DISEASE_SITE', :OLD.STUDY_DISEASE_SITE);
       Audit_Trail.column_delete (raid, 'STUDY_DIVISION', :OLD.STUDY_DIVISION);
       Audit_Trail.column_delete (raid, 'STUDY_DUR', :OLD.STUDY_DUR);
       Audit_Trail.column_delete (raid, 'STUDY_DURUNIT', :OLD.STUDY_DURUNIT);
       Audit_Trail.column_delete (raid, 'STUDY_END_DATE', :OLD.STUDY_END_DATE);
       Audit_Trail.column_delete (raid, 'STUDY_ESTBEGINDT', :OLD.STUDY_ESTBEGINDT);
       Audit_Trail.column_delete (raid, 'STUDY_ICDCODE1', :OLD.STUDY_ICDCODE1);
       Audit_Trail.column_delete (raid, 'STUDY_ICDCODE2', :OLD.STUDY_ICDCODE2);
       Audit_Trail.column_delete (raid, 'STUDY_ICDCODE3', :OLD.STUDY_ICDCODE3);
       Audit_Trail.column_delete (raid, 'STUDY_INFO', :OLD.STUDY_INFO);
       Audit_Trail.column_delete (raid, 'STUDY_INVIND_FLAG', :OLD.STUDY_INVIND_FLAG);
       Audit_Trail.column_delete (raid, 'STUDY_INVIND_NUMBER', :OLD.STUDY_INVIND_NUMBER);
       Audit_Trail.column_delete (raid, 'STUDY_KEYWRDS', :OLD.STUDY_KEYWRDS);
       Audit_Trail.column_delete (raid, 'STUDY_MAJ_AUTH', :OLD.STUDY_MAJ_AUTH);
       Audit_Trail.column_delete (raid, 'STUDY_NSAMPLSIZE', :OLD.STUDY_NSAMPLSIZE);
       Audit_Trail.column_delete (raid, 'STUDY_NUMBER', :OLD.STUDY_NUMBER);
       Audit_Trail.column_delete (raid, 'STUDY_OBJ', :OLD.STUDY_OBJ);
       Audit_Trail.column_delete (raid, 'STUDY_OBJ_CLOB', :OLD.STUDY_OBJ_CLOB);
       Audit_Trail.column_delete (raid, 'STUDY_OTHERPRINV', :OLD.STUDY_OTHERPRINV);
       Audit_Trail.column_delete (raid, 'STUDY_PARENTID', :OLD.STUDY_PARENTID);
       Audit_Trail.column_delete (raid, 'STUDY_PARTCNTR', :OLD.STUDY_PARTCNTR);
       Audit_Trail.column_delete (raid, 'STUDY_PRINV', :OLD.STUDY_PRINV);
       Audit_Trail.column_delete (raid, 'STUDY_PRODNAME', :OLD.STUDY_PRODNAME);
       Audit_Trail.column_delete (raid, 'STUDY_PUBCOLLST', :OLD.STUDY_PUBCOLLST);
       Audit_Trail.column_delete (raid, 'STUDY_PUBFLAG', :OLD.STUDY_PUBFLAG);
       Audit_Trail.column_delete (raid, 'STUDY_SAMPLSIZE', :OLD.STUDY_SAMPLSIZE);
       Audit_Trail.column_delete (raid, 'STUDY_SPONSOR', :OLD.STUDY_SPONSOR);
       Audit_Trail.column_delete (raid, 'STUDY_SPONSORID', :OLD.STUDY_SPONSORID);
       Audit_Trail.column_delete (raid, 'STUDY_SUM', :OLD.STUDY_SUM);
       Audit_Trail.column_delete (raid, 'STUDY_SUM_CLOB', :OLD.STUDY_SUM_CLOB);
       Audit_Trail.column_delete (raid, 'STUDY_TITLE', :OLD.STUDY_TITLE);
       Audit_Trail.column_delete (raid, 'STUDY_VERPARENT', :OLD.STUDY_VERPARENT);
       Audit_Trail.column_delete (raid, 'STUDY_VER_NO', :OLD.STUDY_VER_NO);
COMMIT;
END;
/
