create or replace
TRIGGER ER_STUDYSITES_APNDX_BI0 BEFORE INSERT ON ER_STUDYSITES_APNDX        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;
 
 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_STUDYSITES_APNDX',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'APNDX_DESCRIPTION', :NEW.APNDX_DESCRIPTION);
    -- Audit_Trail.column_insert (raid, 'APNDX_FILE', :NEW.APNDX_FILE);
       Audit_Trail.column_insert (raid, 'APNDX_FILESIZE', :NEW.APNDX_FILESIZE);
       Audit_Trail.column_insert (raid, 'APNDX_NAME', :NEW.APNDX_NAME);
       Audit_Trail.column_insert (raid, 'APNDX_TYPE', :NEW.APNDX_TYPE);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'FK_STUDYSITES', :NEW.FK_STUDYSITES);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_ON', :NEW.LAST_MODIFIED_ON);
       Audit_Trail.column_insert (raid, 'PK_STUDYSITES_APNDX', :NEW.PK_STUDYSITES_APNDX);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/