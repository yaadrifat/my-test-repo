CREATE OR REPLACE TRIGGER ERES.ER_STUDYSTAT_AI1_OUT
BEFORE INSERT ON ERES.ER_STUDYSTAT FOR EACH ROW
DECLARE
fkaccount NUMBER;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL  => Plog.LFATAL);
BEGIN
  SELECT fk_account
  INTO fkaccount
  FROM er_user
  WHERE pk_user = :NEW.creator;
  PKG_MSG_QUEUE.SP_POPULATE_STUDY_STATUS_MSG (
      :NEW.pk_studystat,
      fkaccount,
      'I',
      to_char(:NEW.fk_study),
      :NEW.FK_CODELST_STUDYSTAT
   );

  EXCEPTION  WHEN OTHERS THEN
  Plog.FATAL(pCTX,'exception in ER_STUDYSTAT_AI1_OUT ' || SQLERRM);
END;
/
