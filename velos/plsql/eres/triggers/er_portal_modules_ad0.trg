CREATE  OR REPLACE TRIGGER ER_PORTAL_MODULES_AD0 AFTER DELETE ON ER_PORTAL_MODULES        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    oldUsr VARCHAR(2000);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 oldUsr := 'New User' ;

 
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO usr FROM er_user
      WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      usr := 'New User' ;
      END;
    BEGIN
     SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
       INTO oldUsr FROM er_user
      WHERE pk_user = :OLD.CREATOR ;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      oldUsr := 'New User' ;
      END;
  Audit_Trail.record_transaction (raid, 'ER_PORTAL_MODULES', :OLD.rid, 'D', usr);
       Audit_Trail.column_delete (raid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'CREATOR', oldUsr );
       Audit_Trail.column_delete (raid, 'FK_ID', :OLD.FK_ID);
       Audit_Trail.column_delete (raid, 'FK_PORTAL', :OLD.FK_PORTAL);
       Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', usr );
       Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_delete (raid, 'PK_PORTAL_MODULES', :OLD.PK_PORTAL_MODULES);
       Audit_Trail.column_delete (raid, 'PM_ALIGN', :OLD.PM_ALIGN);
       Audit_Trail.column_delete (raid, 'PM_FORM_AFTER_RESP', :OLD.PM_FORM_AFTER_RESP);
       Audit_Trail.column_delete (raid, 'PM_FROM', :OLD.PM_FROM);
       Audit_Trail.column_delete (raid, 'PM_FROM_UNIT', :OLD.PM_FROM_UNIT);
       Audit_Trail.column_delete (raid, 'PM_TO', :OLD.PM_TO);
       Audit_Trail.column_delete (raid, 'PM_TO_UNIT', :OLD.PM_TO_UNIT);
       Audit_Trail.column_delete (raid, 'PM_TYPE', :OLD.PM_TYPE);
       Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
COMMIT;
END;
/
