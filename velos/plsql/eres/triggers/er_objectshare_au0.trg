CREATE OR REPLACE TRIGGER ER_OBJECTSHARE_AU0
AFTER UPDATE OF RID,IP_ADD,CREATOR,FK_OBJECT,CREATED_ON,RECORD_TYPE,OBJECT_NUMBER,PK_OBJECTSHARE,OBJECTSHARE_TYPE,FK_OBJECTSHARE_ID
ON ER_OBJECTSHARE REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);

   usr VARCHAR2(100);

   old_modby VARCHAR2(100);

   new_modby VARCHAR2(100);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

   usr := Getuser(:NEW.last_modified_by);

  Audit_Trail.record_transaction
    (raid, 'ER_OBJECTSHARE', :OLD.rid, 'U', usr);

  IF NVL(:OLD.pk_objectshare,0) !=
     NVL(:NEW.pk_objectshare,0) THEN
     Audit_Trail.column_update
       (raid, 'PK_OBJECTSHARE',
       :OLD.pk_objectshare, :NEW.pk_objectshare);
  END IF;
  IF NVL(:OLD.object_number,0) !=
     NVL(:NEW.object_number,0) THEN
     Audit_Trail.column_update
       (raid, 'OBJECT_NUMBER',
       :OLD.object_number, :NEW.object_number);
  END IF;
  IF NVL(:OLD.fk_object,0) !=
     NVL(:NEW.fk_object,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_OBJECT',
       :OLD.fk_object, :NEW.fk_object);
  END IF;
  IF NVL(:OLD.fk_objectshare_id,0) !=
     NVL(:NEW.fk_objectshare_id,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_OBJECTSHARE_ID',
       :OLD.fk_objectshare_id, :NEW.fk_objectshare_id);
  END IF;
  IF NVL(:OLD.objectshare_type,' ') !=
     NVL(:NEW.objectshare_type,' ') THEN
     Audit_Trail.column_update
       (raid, 'OBJECTSHARE_TYPE',
       :OLD.objectshare_type, :NEW.objectshare_type);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     Audit_Trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.record_type,' ') !=
     NVL(:NEW.record_type,' ') THEN
     Audit_Trail.column_update
       (raid, 'RECORD_TYPE',
       :OLD.record_type, :NEW.record_type);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
  	BEGIN
		SELECT  TO_CHAR(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
		INTO old_modby FROM ER_USER  WHERE pk_user = :OLD.last_modified_by ;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			old_modby := NULL;
	END ;
	BEGIN
		SELECT  TO_CHAR(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
		INTO new_modby   FROM ER_USER   WHERE pk_user = :NEW.LAST_MODIFIED_BY ;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			new_modby := NULL;
	 END ;
	Audit_Trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
              to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     Audit_Trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;

END;
/


