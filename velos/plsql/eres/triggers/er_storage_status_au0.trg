create or replace TRIGGER ERES.ER_STORAGE_STATUS_AU0 AFTER UPDATE ON ERES.ER_STORAGE_STATUS FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;

BEGIN

  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);
  --KM-08Jun10-#3897
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.record_transaction
      (raid, 'ER_STORAGE_STATUS', :OLD.rid, 'U', usr);
  end if;

  IF NVL(:OLD.PK_STORAGE_STATUS,0) !=
     NVL(:NEW.PK_STORAGE_STATUS,0) THEN
     audit_trail.column_update
       (raid, 'PK_STORAGE_STATUS',
       :OLD.PK_STORAGE_STATUS, :NEW.PK_STORAGE_STATUS);
  END IF;

  IF NVL(:OLD.FK_STORAGE,0) !=
     NVL(:NEW.FK_STORAGE,0) THEN
     audit_trail.column_update
       (raid, 'FK_STORAGE',
       :OLD.FK_STORAGE, :NEW.FK_STORAGE);
  END IF;

  IF NVL(:OLD.SS_START_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.SS_START_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'SS_START_DATE',
       to_char(:OLD.SS_START_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.SS_START_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.FK_CODELST_STORAGE_STATUS,0) !=
     NVL(:NEW.FK_CODELST_STORAGE_STATUS,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_STORAGE_STATUS',
       :OLD.FK_CODELST_STORAGE_STATUS, :NEW.FK_CODELST_STORAGE_STATUS);
  END IF;

  --KM-3171--Removed Storage status notes

  IF NVL(:OLD.FK_USER,0) !=
     NVL(:NEW.FK_USER,0) THEN
     audit_trail.column_update
       (raid, 'FK_USER',
       :OLD.FK_USER, :NEW.FK_USER);
  END IF;

  IF NVL(:OLD.SS_TRACKING_NUMBER,' ') !=
     NVL(:NEW.SS_TRACKING_NUMBER,' ') THEN
     audit_trail.column_update
       (raid, 'SS_TRACKING_NUMBER',
       :OLD.SS_TRACKING_NUMBER, :NEW.SS_TRACKING_NUMBER);
  END IF;

  IF NVL(:OLD.FK_STUDY,0) !=
     NVL(:NEW.FK_STUDY,0) THEN
     audit_trail.column_update
       (raid, 'FK_STUDY',
       :OLD.FK_STUDY, :NEW.FK_STUDY);
  END IF;

  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;

  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM er_user
     WHERE pk_user = :OLD.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM er_user
     WHERE pk_user = :NEW.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_modby := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  END IF;

  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
END;

/


