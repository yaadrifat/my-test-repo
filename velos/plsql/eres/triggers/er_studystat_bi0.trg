CREATE  OR REPLACE TRIGGER ER_STUDYSTAT_BI0 BEFORE INSERT ON ER_STUDYSTAT        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_STUDYSTAT',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON', TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'CURRENT_STAT', :NEW.CURRENT_STAT);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_APRNO;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_APRNO', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_APRSTAT;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_APRSTAT', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_REVBOARD;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_REVBOARD', codeList_desc);
BEGIN
SELECT to_char(PK_CODELST)||', '||CODELST_DESC into codeList_desc from ER_CODELST where PK_CODELST=:NEW.FK_CODELST_STUDYSTAT;
EXCEPTION WHEN NO_DATA_FOUND THEN
      codeList_desc := '';
END;
       Audit_Trail.column_insert (raid, 'FK_CODELST_STUDYSTAT', codeList_desc);
       Audit_Trail.column_insert (raid, 'FK_SITE', :NEW.FK_SITE);
       Audit_Trail.column_insert (raid, 'FK_STUDY', :NEW.FK_STUDY);
       Audit_Trail.column_insert (raid, 'FK_USER_DOCBY', :NEW.FK_USER_DOCBY);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'OUTCOME', :NEW.OUTCOME);
       Audit_Trail.column_insert (raid, 'PK_STUDYSTAT', :NEW.PK_STUDYSTAT);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
       Audit_Trail.column_insert (raid, 'STATUS_TYPE', :NEW.STATUS_TYPE);
       Audit_Trail.column_insert (raid, 'STUDYSTAT_ASSIGNEDTO', :NEW.STUDYSTAT_ASSIGNEDTO);
       Audit_Trail.column_insert (raid, 'STUDYSTAT_DATE', :NEW.STUDYSTAT_DATE);
       Audit_Trail.column_insert (raid, 'STUDYSTAT_ENDT', :NEW.STUDYSTAT_ENDT);
       Audit_Trail.column_insert (raid, 'STUDYSTAT_HSPN', :NEW.STUDYSTAT_HSPN);
       Audit_Trail.column_insert (raid, 'STUDYSTAT_MEETDT', :NEW.STUDYSTAT_MEETDT);
       Audit_Trail.column_insert (raid, 'STUDYSTAT_NOTE', :NEW.STUDYSTAT_NOTE);
       Audit_Trail.column_insert (raid, 'STUDYSTAT_VALIDT', :NEW.STUDYSTAT_VALIDT);
COMMIT;
END;
/
