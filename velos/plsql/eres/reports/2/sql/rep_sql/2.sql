Select
  usr_lastname,
  usr_firstname,
  study_number,
  study_title,
  TO_CHAR(STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDYACTUAL_DATE,
( SELECT codelst_desc
    FROM er_codelst
   WHERE pk_codelst = er_study.fk_codelst_tarea
) AS tarea,
( SELECT codelst_desc
    FROM er_codelst
   WHERE pk_codelst = er_studyteam.FK_CODELST_TMROLE
) AS teamRole
  FROM er_study, er_studyteam, er_user
 WHERE er_user.pk_user = ~1
   AND er_studyteam.fk_user = ~1
   AND er_study.pk_study = er_studyteam.fk_study
 AND NVL(er_study.STUDY_ACTUALDT,SYSDATE) BETWEEN '~2' AND '~3'
AND er_study.study_verparent IS NULL AND
study_team_usr_type = 'D'
