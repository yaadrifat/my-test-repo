select s.pk_storage PPK_Storage, s.Storage_id PStorage_id, s.storage_name PStorage_name,
s.STORAGE_ALTERNALEID PAlt_ID, (SELECT codelst_desc FROM er_codelst WHERE pk_codeLst = s.STORAGE_UNIT_CLASS) PUnit_class,
(SELECT codelst_desc FROM er_codelst WHERE pk_codeLst=s.FK_CODELST_STORAGE_TYPE) PStorage_type,
'Occupied' PStatus,
sch.pk_storage CPK_Storage, sch.Storage_id CStorage_id, sch.storage_name CStorage_name,
sch.STORAGE_ALTERNALEID CAlt_ID, (SELECT codelst_desc FROM er_codelst WHERE pk_codeLst=sch.STORAGE_UNIT_CLASS) CUnit_class,
(SELECT codelst_desc FROM er_codelst WHERE pk_codeLst=sch.FK_CODELST_STORAGE_TYPE) CStorage_type,
(SELECT codelst_desc FROM er_codelst WHERE pk_codeLst=schs.FK_CODELST_STORAGE_STATUS) CStatus
from er_storage s, er_storage_status ss, er_storage sch, er_storage_status schs
WHERE s.pk_storage = ss.fk_storage
AND ss.FK_CODELST_STORAGE_STATUS IN (SELECT pk_codeLst FROM er_codelst WHERE codelst_subtyp='Occupied')
AND ss.PK_STORAGE_STATUS in (SELECT max(ss1.PK_STORAGE_STATUS) FROM er_storage_status ss1
WHERE ss1.FK_STORAGE = s.PK_STORAGE
AND ss1.SS_START_DATE in (select max(ss2.SS_START_DATE) from er_storage_status ss2
where ss2.FK_STORAGE = s.PK_STORAGE))
and sch.fk_storage = s.pk_storage and sch.pk_storage = schs.fk_storage
AND schs.FK_CODELST_STORAGE_STATUS not IN (SELECT pk_codeLst FROM er_codelst WHERE codelst_subtyp='Occupied')
AND schs.PK_STORAGE_STATUS in (SELECT max(schs1.PK_STORAGE_STATUS) FROM er_storage_status schs1
WHERE schs1.FK_STORAGE = sch.PK_STORAGE
AND schs1.SS_START_DATE in (select max(schs2.SS_START_DATE) from er_storage_status schs2
where schs2.FK_STORAGE = sch.PK_STORAGE))
and s.pk_storage in (:storageId)
order by s.storage_name,s.Storage_id,sch.storage_name,sch.Storage_id
