 Select pk_study, study_number, study_title, study_obj, study_sum,study_ver_no,
   study_prodname, study_tarea, study_samplsize,study_duration,
to_char(STUDY_ESTBEGINDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_ESTBEGINDT,
  study_phase,study_type, study_blind, study_random,
  decode(fk_codelst_sponsor,null,study_sponsor,( select codelst_desc from er_codelst where pk_codelst = fk_codelst_sponsor)) STUDY_SPONSOR,
  STUDY_CONTACT,
  STUDY_INFO, STUDY_PARTCNTR, STUDY_KEYWRDS,pk_studyapndx, fk_study , studyapndx_uri , studyapndx_desc,studyapndx_file, studyapndx_type, rectype, bit1,bit2, bit3,
   fk_author, study_restype,   STUDYSEC_CONTENTS,STUDYSEC_CONTENTS2,STUDYSEC_CONTENTS3,
  STUDYSEC_CONTENTS4, STUDYSEC_CONTENTS5,STUDYSEC_NUM,studysec_text from erv_studysum where
   pk_study in  (:studyId) order by pk_studyver
