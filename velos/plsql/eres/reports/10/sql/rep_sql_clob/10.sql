select
PERSON_CODE  ,
PAT_STUDYID,
PERSON_NAME  ,
STUDY_NUMBER  ,
STUDY_TITLE   ,
TO_CHAR(STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_ACTUALDT,
TO_CHAR(PATPROT_ENROLDT,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_ENROLDT ,
PROTOCOLID   ,
PROTOCOL_NAME ,
TO_CHAR(PATPROT_START,PKG_DATEUTIL.F_GET_DATEFORMAT)  PATPROT_START ,
EVENT_NAME   ,
MONTH   ,
TO_CHAR(EVENT_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) st,
EVENT_STATUS_DESC AS EVENT_STATUS,
  FK_PATPROT,visit_name AS VISIT
FROM   erv_patsch
WHERE  fk_per IN (:patientId)  AND
       fk_study IN (:studyId) AND
NVL(EVENT_SCHDATE, TO_DATE(PKG_DATEUTIL.F_GET_NULL_DATE_STR,PKG_DATEUTIL.F_GET_DATEFORMAT)) BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT) and
SCHEVE_STATUS = 0
ORDER BY EVENT_SCHDATE  ,event_sequence
