SELECT study_number,
study_title,DECODE(F_Get_Sum4_Data(a.pk_study,'sum4_agent'),'Y','Agent OR Device',
					DECODE(F_Get_Sum4_Data(a.pk_study,'sum4_beh'),'Y','Trials Involving other Interventions',
					DECODE(F_Get_Sum4_Data(a.pk_study,'sum4_na'),'Y','Epidemiologic OR other Observational Studies',
					DECODE(F_Get_Sum4_Data(a.pk_study,'sum4_comp') ,'Y','Companion, ANCILLARY OR Correlative Studies','')))) AS trial_type,
F_Get_Codelstdesc(a.fk_codelst_restype) AS research_type,F_Get_Codelstdesc(a.fk_codelst_type) AS study_type,
F_Get_Codelstdesc(a.fk_codelst_scope) AS study_scope,F_Get_Codelstdesc(a.fk_codelst_tarea) AS study_tarea,
F_Get_Sum4_Data(a.pk_study,'sum4_prg') AS program_code,decode(fk_codelst_sponsor,null,study_sponsor,( select codelst_desc from er_codelst where pk_codelst = fk_codelst_sponsor)) study_sponsor,F_Getdis_Site(a.study_disease_site) AS disease_site,
DECODE(study_prinv,NULL,DECODE(study_otherprinv,NULL,'',study_otherprinv),(SELECT usr_lastname || ',' || usr_firstname FROM ER_USER WHERE pk_user = study_prinv) || DECODE(study_otherprinv,NULL,'','; ' || study_otherprinv))  AS PRINCIPAL_INVESTIGATOR,
F_Get_Codelstdesc(a.fk_codelst_phase) AS study_phase,study_samplsize AS target,TO_CHAR(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt,  TO_CHAR(study_end_date,PKG_DATEUTIL.F_GET_DATEFORMAT) study_end_date,
(SELECT COUNT(*) FROM ER_PATPROT,person WHERE fk_study = a.pk_study AND patprot_stat = 1 AND PATPROT_ENROLDT IS NOT NULL AND pk_person = fk_per AND fk_site IN (:orgId)  ) AS ctr_to_date,
(SELECT COUNT(*) FROM ER_PATPROT,person WHERE fk_study = a.pk_study AND patprot_stat = 1 AND patprot_enroldt BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
AND pk_person = fk_per AND fk_site IN (:orgId)  ) AS ctr_12_mos,
race_ne,race_white,race_asian,race_notrep,race_indala,race_blkafr,race_hwnisl,race_unknown,ethnicity_ne,ethnicity_hispanic,ethnicity_unknown,ethnicity_nonhispanic,ethnicity_notreported,gender_ne,gender_male,gender_female,gender_other,gender_unknown
FROM (SELECT fk_study,SUM(race_ne) AS race_ne,SUM(race_white) AS race_white,SUM(race_asian) AS race_asian,SUM(race_notrep) AS  race_notrep,SUM(race_indala) AS race_indala,SUM(race_blkafr) AS race_blkafr,
SUM(race_hwnisl) AS race_hwnisl,SUM(race_unknown) AS race_unknown,SUM(ethnicity_ne) AS ethnicity_ne,SUM(ethnicity_hispanic) AS ethnicity_hispanic,SUM(ethnicity_unknown) AS ethnicity_unknown,
SUM(ethnicity_nonhispanic) AS ethnicity_nonhispanic,SUM(ethnicity_notreported) AS ethnicity_notreported,
SUM(gender_ne) AS gender_ne,SUM(gender_male) AS gender_male,SUM(gender_female) AS gender_female,SUM(gender_other) AS gender_other,
SUM(gender_unknown) AS gender_unknown FROM (SELECT fk_study, DECODE(race,NULL,1,0) AS race_ne,DECODE(race,'race_white',1,0) AS race_white,DECODE(race,'race_asian',1,0) AS race_asian,
DECODE(race,'race_notrep',1,0) AS race_notrep, DECODE(race,'race_indala',1,0) AS race_indala, DECODE(race,'race_blkafr',1,0) AS race_blkafr,
DECODE(race,'race_hwnisl',1,0) AS race_hwnisl, DECODE(race,'race_unknown',1,0) AS race_unknown,DECODE(ethnicity,NULL,1,0) AS ethnicity_ne,
DECODE(ethnicity,'hispanic',1,0) AS ethnicity_hispanic, DECODE(ethnicity,'Unknown',1,0) AS ethnicity_unknown,DECODE(ethnicity,'nonhispanic',1,0) AS ethnicity_nonhispanic,DECODE(ethnicity,'notreported',1,0) AS ethnicity_notreported,
DECODE(gender,NULL,1,0) AS gender_ne, DECODE(gender,'male',1,0) AS gender_male,
DECODE(gender,'female',1,0) AS gender_female,
DECODE(gender,'other',1,0) AS gender_other,
DECODE(gender,'Unknown',1,0) AS gender_unknown FROM  (
SELECT fk_study,
(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_race) AS race,
(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_gender) AS gender,
(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_ethnicity) AS ethnicity
FROM EPAT.person, ER_PATPROT a
WHERE pk_person = fk_per AND fk_site IN (:orgId) AND
patprot_enroldt BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND
patprot_stat = 1
AND fk_study IN(SELECT pk_study FROM ER_STUDY WHERE pk_study IN (SELECT  DISTINCT fk_study FROM ER_STUDYTEAM WHERE
fk_codelst_tmrole =(SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='role_prin')
AND fk_user IN (:userId) OR STUDY_PRINV IN (:userId) OR STUDY_PRINV IS NULL ))
)) GROUP BY fk_study
), ER_STUDY a WHERE pk_study = fk_study AND fk_account = :sessAccId
AND fk_study IN (:studyId) AND fk_codelst_tarea IN (:tAreaId) AND ((study_division IN (:studyDivId)) OR (study_division IS NULL))
ORDER BY study_number