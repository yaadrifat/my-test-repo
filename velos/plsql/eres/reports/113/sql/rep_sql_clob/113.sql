 SELECT * FROM (
SELECT DECODE(trial_type_agent,'Y',1,DECODE(trial_type_nonagent,'Y',2,DECODE(trial_type_epi,'Y',3,DECODE(trial_type_comp,'Y',4)))) AS trial_type_sort,
DECODE(trial_type_agent,'Y','Agent OR Device',DECODE(trial_type_nonagent,'Y','Trials Involving other Interventions',DECODE(trial_type_epi,'Y','Epidemiologic OR other Observational Studies',DECODE(trial_type_comp,'Y','Companion, ANCILLARY OR Correlative Studies','NA')))) AS trial_type,
RESEARCH_TYPE,STUDY_TYPE,STUDY_SCOPE,STUDY_SPONSOR,DISEASE_SITE,STUDY_NUMBER,PRIMARY_INVESTIGATOR,STUDY_MAJ_AUTH,TAREA,DATE_OPEN,DATE_CLOSED,STUDY_PHASE,STUDY_TITLE,TARGET,CTR_TO_DATE,CTR_TO_DATE_OTR,CTR_12_MOS, CTR_12_MOS_OTR, CLOSED_DATE
 FROM (
SELECT
F_Get_Sum4_Data(a.pk_study,'sum4_agent') AS trial_type_agent,
F_Get_Sum4_Data(a.pk_study,'sum4_beh') AS trial_type_nonagent,
F_Get_Sum4_Data(a.pk_study,'sum4_na') AS trial_type_epi,
F_Get_Sum4_Data(a.pk_study,'sum4_comp') AS trial_type_comp,
F_Get_Codelstdesc(a.fk_codelst_restype) AS research_type,
F_Get_Codelstdesc(a.fk_codelst_type) AS study_type,
F_Get_Codelstdesc(a.fk_codelst_scope) AS study_scope,
decode(fk_codelst_sponsor,null,study_sponsor,( select codelst_desc from er_codelst where pk_codelst = fk_codelst_sponsor)) study_sponsor
,DECODE(INSTR(a.study_disease_site,','),0,(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.study_disease_site),'Multiple')  AS disease_site,
study_number,
(SELECT usr_lastname || ', ' || usr_firstname FROM ER_USER WHERE pk_user = a.study_prinv) || ' ' || study_otherprinv AS PRIMARY_INVESTIGATOR,
study_maj_auth, (SELECT studyid_id FROM ER_STUDYID WHERE fk_study = a.pk_study AND fk_codelst_idtype = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'sum4_prg')) AS tarea,(SELECT TO_CHAR(studystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT
WHERE fk_study = a.pk_study AND  fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = a.fk_account AND site_stat = 'Y') AND
fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'studystat' AND codelst_subtyp = 'active')
AND studystat_date = (SELECT MIN(studystat_date)
		 	 FROM ER_STUDYSTAT
			 WHERE fk_study = a.pk_study AND
			fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = a.fk_account AND site_stat = 'Y') AND
			fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'studystat' AND codelst_subtyp = 'active')) and rownum = 1)
 AS date_open,
(SELECT TO_CHAR(studystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT
WHERE fk_study = a.pk_study AND
fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = a.fk_account AND site_stat = 'Y') AND
fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'studystat' AND codelst_subtyp = 'prmnt_cls')
AND studystat_date = (SELECT MIN(studystat_date)
		 	 FROM ER_STUDYSTAT
			 WHERE fk_study = a.pk_study AND
			fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = a.fk_account AND site_stat = 'Y') AND
			fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'studystat' AND codelst_subtyp = 'prmnt_cls')) and rownum = 1)
AS date_closed,
F_Get_Codelstdesc(a.fk_codelst_phase) AS study_phase,
decode((SELECT count(*) FROM ER_STUDYID WHERE fk_study = a.pk_study AND fk_codelst_idtype = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'studyidtype' AND codelst_subtyp = 'std_title')),0,study_title,(SELECT studyid_id FROM ER_STUDYID WHERE fk_study = a.pk_study AND fk_codelst_idtype = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'studyidtype' AND codelst_subtyp = 'std_title'))) AS study_title,

DECODE((SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_scope),
'std_cen_multi','(' || study_nsamplsize || ') ' || (SELECT studysite_lsamplesize FROM ER_STUDYSITES r WHERE r.fk_study = a.pk_study AND fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = :sessAccId AND site_stat = 'Y') ),
(SELECT studysite_lsamplesize FROM ER_STUDYSITES r WHERE r.fk_study = a.pk_study AND fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = :sessAccId AND site_stat = 'Y') )) AS target,
F_Nci_Sum4_Count(a.pk_study,NULL,NULL,'Y') AS ctr_to_date, F_Nci_Sum4_Count(a.pk_study,NULL,NULL,'N') AS ctr_to_date_otr,F_Nci_Sum4_Count(a.pk_study,TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT),TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),'Y') AS ctr_12_mos,
F_Nci_Sum4_Count(a.pk_study,TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT),TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),'N') AS ctr_12_mos_otr,
(SELECT TO_CHAR(studystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT)  FROM ER_STUDYSTAT
WHERE fk_study = a.pk_study AND
fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = a.fk_account AND site_stat = 'Y') AND
fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'studystat' AND codelst_subtyp = 'active_cls')
AND studystat_date = (SELECT MIN(studystat_date)
		 	 FROM ER_STUDYSTAT

			 WHERE fk_study = a.pk_study AND
			fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = a.fk_account AND site_stat = 'Y') AND
			fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'studystat' AND codelst_subtyp = 'active_cls')) and rownum = 1)
 AS closed_date
FROM ER_STUDY a, ER_STUDYSTAT
WHERE pk_study = fk_study AND
a.fk_account = :sessAccId AND
fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type =  'studystat' AND codelst_subtyp = 'active') AND
TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) <= NVL((SELECT MIN(studystat_date) FROM ER_STUDYSTAT WHERE fk_study = pk_study AND fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = :sessAccId AND site_stat = 'Y') AND fk_codelst_studystat IN (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type =  'studystat' AND (codelst_subtyp = 'prmnt_cls' OR codelst_subtyp = 'active_cls'))),TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT)) AND
TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT) >= (SELECT MIN(studystat_date) FROM ER_STUDYSTAT WHERE fk_study = pk_study AND fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = :sessAccId AND site_stat = 'Y') AND fk_codelst_studystat IN (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type =  'studystat' AND codelst_subtyp = 'active'))
)
)
WHERE trial_type <> 'NA'
ORDER BY trial_type_sort,research_type,tarea,primary_investigator

