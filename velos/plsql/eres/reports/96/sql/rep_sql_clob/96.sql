SELECT (SELECT study_number FROM ER_STUDY WHERE pk_study = ~4) AS study_number,
(SELECT study_title FROM ER_STUDY WHERE pk_study = ~4) AS study_title,
(SELECT  per_code FROM ER_PER WHERE pk_per = ~1) AS per_code,
(SELECT  DISTINCT patprot_patstdid FROM ER_PATPROT WHERE fk_per = ~1 AND fk_study = ~4) AS patprot_patstdid,
(SELECT  DISTINCT  TO_CHAR(patprot_enroldt,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATPROT WHERE fk_per = ~1 AND fk_study = ~4) AS  patprot_enroldt,
TO_CHAR(test_date,PKG_DATEUTIL.F_GET_DATEFORMAT) AS test_date, psa, testosterone, adverse_event, treatment FROM  (
SELECT test_date, F_Getlabresult('psa',test_date,~1, ~4) AS PSA, F_Getlabresult('TEST',test_date,~1, ~4) AS TESTOSTERONE, F_Getae(test_date,~1, ~4) AS ADVERSE_EVENT,
F_Getformdatabykeyword(test_date, ~1, ~4, 'TREATMENT') AS Treatment
FROM (
SELECT test_date
FROM ER_PATLABS, ER_LABTEST
WHERE
pk_labtest = fk_testid AND
LOWER(labtest_shortname) IN ('psa','TEST') AND
test_date BETWEEN '~2' AND '~3' AND
fk_per = ~1 AND
fk_study = ~4
UNION
SELECT ae_stdate
FROM ESCH.sch_adverseve
WHERE
fk_per = ~1 AND
fk_study = ~4 AND
ae_stdate BETWEEN '~2' AND '~3'
UNION
SELECT patforms_filldate
FROM ER_PATFORMS a, ER_PATPROT, ER_FORMLIB
WHERE patforms_filldate BETWEEN '~2' AND '~3' AND
a.fk_per = ~1 AND
pk_patprot = fk_patprot AND
fk_study = ~4 AND
pk_formlib = fk_formlib AND
form_keyword ='TREATMENT'
)
)