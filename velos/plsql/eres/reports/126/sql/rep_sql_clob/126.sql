SELECT (SELECT per_code FROM ER_PER WHERE pk_per = g.fk_per) AS patient_id,
patprot_patstdid AS patient_study_id,
(SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study) AS study_number,
form_name,
SUM(DECODE(fk_codelst_querystatus,(select pk_codelst from er_codelst where codelst_type = 'query_status' and codelst_subtyp = 'open'),1,0)) AS open_count,
SUM(DECODE(fk_codelst_querystatus,(select pk_codelst from er_codelst where codelst_type = 'query_status' and codelst_subtyp = 'resolved'),1,0)) AS resolved_count,
SUM(DECODE(fk_codelst_querystatus,(select pk_codelst from er_codelst where codelst_type = 'query_status' and codelst_subtyp = 're-opened'),1,0)) AS reopen_count,
SUM(DECODE(fk_codelst_querystatus,(select pk_codelst from er_codelst where codelst_type = 'query_status' and codelst_subtyp = 'closed'),1,0)) AS closed_count
FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g
WHERE a.pk_formquery = b.fk_formquery
AND b.PK_FORMQUERYSTATUS = (select max(b1.PK_FORMQUERYSTATUS) from ER_FORMQUERYSTATUS b1
where a.pk_formquery = b1.fk_formquery
AND trim(b1.ENTERED_ON) = (select max(trim(b2.ENTERED_ON)) from ER_FORMQUERYSTATUS b2
where a.pk_formquery = b2.fk_formquery))
AND a.fk_field = c.fk_field
AND d.pk_formsec = c.fk_formsec
AND e.pk_formlib = d.fk_formlib AND f.RECORD_TYPE <> 'D'
AND pk_formlib = f.fk_formlib
AND pk_patforms = fk_querymodule
AND pk_patprot = fk_patprot
AND fk_study IN (:studyId) AND g.fk_per in (:patientId)
and g.fk_site_enrolling in (:orgId)
and (a.created_on BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT) )
GROUP BY fk_study,g.fk_per,patprot_patstdid,form_name
