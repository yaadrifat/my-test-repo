CREATE OR REPLACE
PROCEDURE SP_DELETE_PORTAL(portalId in number, p_userId in varchar2, p_ipAdd in varchar2)
is
BEGIN
  update ER_STATUS_HISTORY set LAST_MODIFIED_BY=p_userId, IP_ADD=p_ipAdd, LAST_MODIFIED_DATE=SYSDATE where status_modpk = portalId;
   DELETE FROM ER_STATUS_HISTORY WHERE STATUS_MODPK = PORTALID;
  update ER_PORTAL_POPLEVEL set LAST_MODIFIED_BY=p_userId, IP_ADD=p_ipAdd, LAST_MODIFIED_DATE=SYSDATE where fk_portal=portalId;
   delete from ER_PORTAL_POPLEVEL where fk_portal=portalId;

--modified by Sonia Abrol, 7/23/07, delete records from logins as well as portal design

   --delete from portal logins
   update er_portal_logins set LAST_MODIFIED_BY=p_userId, IP_ADD=p_ipAdd, LAST_MODIFIED_DATE=SYSDATE where fk_portal=portalId;
   delete from er_portal_logins where fk_portal = portalId;

   --delete from portal modules/portal design
   update er_portal_modules set LAST_MODIFIED_BY=p_userId, IP_ADD=p_ipAdd, LAST_MODIFIED_DATE=SYSDATE where fk_portal=portalId;   
   delete from er_portal_modules where fk_portal = portalId;

   update ER_PORTAL set LAST_MODIFIED_BY=p_userId, IP_ADD=p_ipAdd, LAST_MODIFIED_DATE=SYSDATE where pk_portal=portalId;   
   delete from ER_PORTAL where pk_portal = portalId;


   --delete from SCH_PORTAL_FORMS table
   update SCH_PORTAL_FORMS set LAST_MODIFIED_BY=p_userId, IP_ADD=p_ipAdd, LAST_MODIFIED_DATE=SYSDATE where fk_portal=portalId;   
   delete from SCH_PORTAL_FORMS where fk_portal = portalId;

end;
/