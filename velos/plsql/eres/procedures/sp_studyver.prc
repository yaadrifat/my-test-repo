CREATE OR REPLACE PROCEDURE        "SP_STUDYVER" (
   P_STUDY      IN       NUMBER,
   P_XUSER      IN       NUMBER,
   P_NEWSTUDY   OUT      NUMBER
)
/*
Changed the study parent - now it is the same as the study from which we copy the study from
10sept2002 added currency to copied study
*/
AS
   V_NEWSTUDY     NUMBER;
   V_DT           CHAR (12);
   V_ACCOUNT      NUMBER;
   V_CNT          NUMBER;
   V_RIGHTS       VARCHAR2 (20);
   V_REQ          VARCHAR2 (10);
   V_RIGHT        VARCHAR2 (200);
--   V_STUDYNUM     VARCHAR2 (20);
      V_STUDYNUM     VARCHAR2 (100);--JM: 120406
   V_STUDYTITLE   VARCHAR2 (1000);

   CURSOR CUR_STUDY_RIGTH
   IS
      SELECT trim (CTRL_VALUE)
        FROM ER_CTRLTAB
       WHERE CTRL_KEY = 'study_rights'
       ORDER BY CTRL_SEQ DESC;
BEGIN
   DBMS_OUTPUT.PUT_LINE ('Study to copy ' || TO_CHAR (P_STUDY));
   -- the DateTime format that will be appended to the file names in the Study Appendix where
   -- the apndx type is 'file'. This is an OUT parameter
   SELECT TO_CHAR (SYSDATE, 'DDMMYYHHMISS')
     INTO V_DT
     FROM DUAL;
   -- The new study id generated from the seq_er_study sequence. This is stored in the p_newstudy
   -- variable to be used for all the INSERT/SELECT. This is an OUT parameter
   SELECT SEQ_ER_STUDY.NEXTVAL
     INTO P_NEWSTUDY
     FROM DUAL;

   DBMS_OUTPUT.PUT_LINE ('new study  ' || TO_CHAR (P_NEWSTUDY));


   BEGIN
      -- Make a Copy of the study
      --     These columns have ben updated with the same values for all INSERTs
      -- CREATOR                 the external user id is updated here
      -- LAST_MODIFIED_BY        the external user id is updated here
      -- LAST_MODIFIED_DATE      the current sysdate
      -- CREATED_ON              the current sysdate
      -- The first insert, to copy the study details
      -- PK_STUDY    is geneated
      -- FK_SITE_SPONSOR      the site sponsor is updated to null
      -- FK_ACCOUNT              the account id is updated to the account of the xternal user
      -- FK_USER_CONTACT         the user contact is nullified
      -- STUDY_PARENTID          the study parent is the id of the original study
      -- FK_AUTHOR               the author of the new study is the external user
      BEGIN
         INSERT INTO ER_STUDY
                     (PK_STUDY,
                      STUDY_SPONSOR,
                      FK_ACCOUNT,
                      STUDY_CONTACT,
                      STUDY_PUBFLAG,
                      STUDY_TITLE,
                      STUDY_OBJ_CLOB,
                      STUDY_SUM_CLOB,
                      STUDY_PRODNAME,
                      STUDY_SAMPLSIZE,
                      STUDY_DUR,
                      STUDY_DURUNIT,
                      STUDY_ESTBEGINDT,
                      STUDY_ACTUALDT,
                      STUDY_PRINV,
                      STUDY_PARTCNTR,
                      STUDY_KEYWRDS,
                      STUDY_PUBCOLLST,
                      STUDY_PARENTID,
                      FK_AUTHOR,
                      FK_CODELST_TAREA,
                      FK_CODELST_PHASE,
                      FK_CODELST_BLIND,
                      FK_CODELST_RANDOM,
                      CREATOR,
                      STUDY_VER_NO,
                      STUDY_CURRENT,
                      LAST_MODIFIED_BY,
                      LAST_MODIFIED_DATE,
                      CREATED_ON,
                      STUDY_NUMBER,
                      FK_CODELST_RESTYPE,
                      FK_CODELST_TYPE,
                      STUDY_INFO,
                      IP_ADD,
                      STUDY_VERPARENT, FK_CODELST_CURRENCY,
					  study_advlkp_ver,
					  study_maj_auth,study_disease_site,fk_codelst_scope,study_assoc
                     )
                 SELECT P_NEWSTUDY, STUDY_SPONSOR, FK_ACCOUNT,
                        STUDY_CONTACT, 'N', STUDY_TITLE, STUDY_OBJ_CLOB,
                        STUDY_SUM_CLOB, STUDY_PRODNAME, STUDY_SAMPLSIZE,
                        STUDY_DUR, STUDY_DURUNIT, STUDY_ESTBEGINDT,
                        NULL, STUDY_PRINV, STUDY_PARTCNTR,
                        STUDY_KEYWRDS, STUDY_PUBCOLLST, STUDY_PARENTID , FK_AUTHOR,
                        FK_CODELST_TAREA, FK_CODELST_PHASE,
                        FK_CODELST_BLIND, FK_CODELST_RANDOM, P_XUSER,
                        NULL, STUDY_CURRENT, P_XUSER, SYSDATE, SYSDATE,
                        STUDY_NUMBER, FK_CODELST_RESTYPE, FK_CODELST_TYPE,
                        STUDY_INFO, IP_ADD, P_STUDY,FK_CODELST_CURRENCY,
						study_advlkp_ver,
					  study_maj_auth,study_disease_site,fk_codelst_scope,study_assoc
                   FROM ER_STUDY
                  WHERE PK_STUDY = P_STUDY;


         IF SQL%FOUND THEN
            DBMS_OUTPUT.PUT_LINE ('Insert into study team ' || TO_CHAR (1));
         ELSE
            DBMS_OUTPUT.PUT_LINE ('Error in sharing study. ');
            RAISE_APPLICATION_ERROR (
               -20000,
               'Error in sharing study. If error persists Pl. Check with Sys Admin'
            );
         END IF;
      END;

      -- The second insert for all the study sections
      --    PK_STUDYSEC            the Primary key is generated
      -- FK_STUDY               the study id is updated with the new study id
      INSERT INTO ER_STUDYSEC
                  (PK_STUDYSEC,
                   FK_STUDY,
                   STUDYSEC_NAME,
                   STUDYSEC_NUM,
                   STUDYSEC_TEXT,
                   STUDYSEC_PUBFLAG,
                   STUDYSEC_SEQ,
                   CREATOR,
                   LAST_MODIFIED_BY,
                   LAST_MODIFIED_DATE,
                   CREATED_ON
                  )
              SELECT SEQ_ER_STUDYSEC.NEXTVAL, P_NEWSTUDY, STUDYSEC_NAME,STUDYSEC_NUM,
                     STUDYSEC_TEXT, 'N', STUDYSEC_SEQ, CREATOR,
                     LAST_MODIFIED_BY, SYSDATE, SYSDATE
                FROM ER_STUDYSEC
               WHERE FK_STUDY = P_STUDY;

      --    PK_STUDYAPNDX           the pk is generated from seq_er_studyapndx
      --    FK_STUDY                the new study id is updated here
      --    STUDYAPNDX_FILE         the FILE name is changed for all appendix where type is 'file'
      --             a 'ddmmyyhhmiss' format has been appended to the file name
      INSERT INTO ER_STUDYAPNDX
                  (PK_STUDYAPNDX,
                   FK_STUDY,
                   STUDYAPNDX_URI,
                   STUDYAPNDX_DESC,
                   STUDYAPNDX_PUBFLAG,
                   CREATOR,
                   LAST_MODIFIED_BY,
                   LAST_MODIFIED_DATE,
                   CREATED_ON,
                   STUDYAPNDX_FILE,
                   STUDYAPNDX_TYPE,
                   STUDYAPNDX_FILEOBJ
                  )
              SELECT SEQ_ER_STUDYAPNDX.NEXTVAL, P_NEWSTUDY, STUDYAPNDX_URI,
                     STUDYAPNDX_DESC, STUDYAPNDX_PUBFLAG, CREATOR,
                     LAST_MODIFIED_BY, SYSDATE, SYSDATE,
                     DECODE (
                        trim (STUDYAPNDX_TYPE),
                        'file', SUBSTR (
                                   STUDYAPNDX_URI,
                                   1,
                                   (INSTR (STUDYAPNDX_URI, '.', -1) - 1)
                                ) ||
                                V_DT ||
                                SUBSTR (
                                   STUDYAPNDX_URI,
                                   INSTR (STUDYAPNDX_URI, '.', -1)
                                ),
                        STUDYAPNDX_URI
                     ),
                     STUDYAPNDX_TYPE, STUDYAPNDX_FILEOBJ
                FROM ER_STUDYAPNDX
               WHERE FK_STUDY = P_STUDY;


      INSERT INTO ER_STUDYTEAM
                  (PK_STUDYTEAM,
                   FK_CODELST_TMROLE,
                   FK_USER,
                   FK_STUDY,
                   CREATOR,
                   LAST_MODIFIED_BY,
                   LAST_MODIFIED_DATE,
                   CREATED_ON,
                   STUDY_TEAM_RIGHTS,
                   IP_ADD,
		   study_team_usr_type
                  )
              (SELECT SEQ_ER_STUDYTEAM.NEXTVAL, FK_CODELST_TMROLE, FK_USER,
                      p_newstudy, P_XUSER, P_XUSER, SYSDATE, SYSDATE,
                      STUDY_TEAM_RIGHTS, IP_ADD,study_team_usr_type
                 FROM ER_STUDYTEAM
                WHERE FK_STUDY = P_STUDY);

      -- ADD the protocol copy procedure here
      -- This procedure is owned by the eSch schema, thus we will use the DB Link to call the proc
      SP_CPSTUDYPROT (P_STUDY, P_NEWSTUDY, P_XUSER);
   END;
END;
/


CREATE SYNONYM ESCH.SP_STUDYVER FOR SP_STUDYVER;


CREATE SYNONYM EPAT.SP_STUDYVER FOR SP_STUDYVER;


GRANT EXECUTE, DEBUG ON SP_STUDYVER TO EPAT;

GRANT EXECUTE, DEBUG ON SP_STUDYVER TO ESCH;

