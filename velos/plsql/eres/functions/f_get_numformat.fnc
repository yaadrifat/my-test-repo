create or replace
FUNCTION f_get_numformat  return Varchar2
IS
   v_nf varchar2(200);
   v_decsym CHAR(1);
   v_grpsym CHAR(2);
   pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'F_GET_NUMFORMAT', pLEVEL  => Plog.LFATAL);
    begin

	SELECT CODELST_DESC INTO v_decsym FROM ER_CODELST WHERE codelst_type='num_format' AND codelst_subtyp ='decimal_symbol';
    SELECT CODELST_DESC INTO v_grpsym FROM ER_CODELST WHERE codelst_type='num_format' AND codelst_subtyp ='group_symbol';

        if (v_decsym is null or length(v_decsym)=0 ) then
            v_decsym := '.';
        end if;
        if (v_grpsym is null or length(v_grpsym)=0 ) then
            v_grpsym := ',';
        end if;
	v_nf:= q'[nls_numeric_characters = ']';
	v_nf:= v_nf || v_decsym || v_grpsym || q'[']';
    v_nf:=REPLACE(v_nf,CHR(49824),CHR(32));
     return v_nf;
END f_get_numformat;
/

CREATE OR REPLACE SYNONYM ESCH.f_get_numformat FOR f_get_numformat;
CREATE OR REPLACE SYNONYM EPAT.f_get_numformat FOR f_get_numformat;

GRANT EXECUTE, DEBUG ON f_get_numformat TO EPAT;
GRANT EXECUTE, DEBUG ON f_get_numformat TO ESCH;
