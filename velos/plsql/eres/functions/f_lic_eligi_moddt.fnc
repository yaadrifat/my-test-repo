create or replace function f_lic_eligi_moddt(cordid number,status_type varchar2) 
  return varchar2
  is
  v_modi_dt varchar2(50);
  begin
   select TO_CHAR(MAX(CREATED_ON),'Mon DD, YYYY') into v_modi_dt FROM CB_ENTITY_STATUS WHERE ENTITY_ID = cordid AND STATUS_TYPE_CODE= status_type;
   return v_modi_dt;
end;
/