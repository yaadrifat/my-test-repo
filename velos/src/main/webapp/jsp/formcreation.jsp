<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="jqueryUtils.jsp"></jsp:include>
<jsp:include page="velos_includes.jsp"></jsp:include>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT Language="javascript">

function checkPermission(pageRight)
{
   return f_check_perm(pageRight,'N');
}
 function  validate(formobj){

    checkQuote="N";
    if(formobj.calledFrom.value == "St" || formobj.calledFrom.value == "A")
	 {

	 preStatus = formobj.preStatus.value;
	 activeId = formobj.activeId.value;
	 offlineId = formobj.offlineId.value;
	 deactivateId = formobj.deactivateId.value;
	 lockdownId = formobj.lockdownId.value;
	 wipId = formobj.wipId.value;

	migrationId = formobj.migrationId.value;

	var selIndex;

	selIndex =  formobj.cmbStatus.selectedIndex;
	var status = formobj.cmbStatus.options[selIndex].value;

	if(migrationId == status)
	{
		alert("<%=MC.M_CntSelStat_SetAppResp%>");/*alert("You cannot select this status. This status can be only set by the application for the purpose of form responses' migration.");*****/
		formobj.cmbStatus.value = preStatus;
		return false;
	}


	// When form status change from 'Active'
	if(preStatus == activeId &&  status == wipId)
	{
		alert("<%=MC.M_CntChg_ActvToWorkInPgress%>");/*alert("You cannot change status from 'Active' to 'Work in progress'");*****/
		formobj.cmbStatus.value = activeId;
		return false;
	}


	// when form status change from offline for editing
	if(preStatus == offlineId && status == wipId)
	{
		alert("<%=MC.M_CntChgStat_WorkPgress%>");/*alert("You cannot change status from 'Offline for Editing' to 'Work in Progress'");*****/
		formobj.cmbStatus.value = offlineId;
		return false;
	}


	if(preStatus == offlineId && status == deactivateId)
	{
		alert("<%=MC.M_CntChg_OfflineToDeac%>");/*alert("You cannot change status from 'Offline for Editing' to 'Deactivated'");*****/
		formobj.cmbStatus.value = offlineId;
		return false;
	}


	if(preStatus == offlineId && status == lockdownId)
	{
		alert("<%=MC.M_CntChg_OfflineToLkdwn%>");/*alert("You cannot change status from 'Offline for Editing' to 'Lockdown'");*****/
		formobj.cmbStatus.value = offlineId;
		return false;
	}

// when form status change from lockdown
	if(preStatus == lockdownId && status == wipId)
	{
		alert("<%=MC.M_CntChg_LkdwnToWorkInPgress%>");/*alert("You cannot change status from 'Lockdown' to 'Work in Progress'");*****/
		formobj.cmbStatus.value = lockdownId;
		return false;
	}


	if(preStatus == lockdownId && status == deactivateId)
	{
		alert("<%=MC.M_CntChgStat_FromLkdwn%>");/*alert("You cannot change status from 'Lockdown' to 'Deactivated'");*****/
		formobj.cmbStatus.value = lockdownId;
		return false;
	}


	if(preStatus == lockdownId && status == offlineId)
	{
		alert("<%=MC.M_CntChg_FromLkdwnToOffline%>");/*alert("You cannot change status from 'Lockdown' to 'Offline for Editing'");*****/
		formobj.cmbStatus.value = lockdownId;
		return false;
	}

	if(preStatus == wipId && status == offlineId)
		 {
		alert("<%=MC.M_CntChg_OfflineEdit%>");/*alert("You cannot change status from 'Work in Progress' to 'Offline for Editing'");*****/
		formobj.cmbStatus.value = wipId;
		return false;
		 }

	if(preStatus == wipId && status == lockdownId)
		 {
		   alert("<%=MC.M_CntChg_WorkInPgressToLkdwn%>");/*alert("You cannot change status from 'Work in Progress' to 'Lockdown'");*****/
		   formobj.cmbStatus.value = wipId;
		return false;
		 }
}
     /* Bug#10963 09-Aug-2012 -Sudhir*/
     //if (!(allsplcharcheck(formobj.txtName.value))) return false;
	 if (!(validate_col('Form Name',formobj.txtName))) return false;
	 if((formobj.calledFrom.value!="A") && (formobj.calledFrom.value!="St")){
	 if (!(validate_col('Form Type',formobj.formType))) return false;
	 }

	 if (!(validate_col('Form Status',formobj.cmbStatus))) return false;

	 if (!(validate_col('e-Signature',formobj.eSign))) return false;

<%-- 	 if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
    	} --%>

   }

/* function openExistingFormWin(formobj)
{
		param1="copyFormFromLib.jsp?srcmenu=tdmenubaritem4&selectedTab=3";
		windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=300");
		windowName.focus();
}*/

/***** @Gyanendra update for Bug #20544 *****/
 
var textEventLimitFunction = {		
		fixTextAreas:{}
	};

     textEventLimitFunction.fixTextAreas = function (frm) {
	
	limitChars('txtAreaDesc', 4000, 'countxtAreaDesc');
	
	$j("#txtAreaDesc").keyup(function(){
		var divId = 'coun'+this.id;
		limitChars(this.id, 4000, divId);
	});	
};

$j(document).ready(function(){	
	
	textEventLimitFunction.fixTextAreas();
		
});

/***** End update @Gyanendra *****/


</SCRIPT>

<% String src;
String selectedTab;
String calledFrom;
String formId="" ; // null;
src= request.getParameter("srcmenu");
selectedTab = request.getParameter("selectedTab");
calledFrom=request.getParameter("calledFrom");


formId = request.getParameter("formLibId");
String mode=request.getParameter("mode");

/*if(formId!=null)
{
 mode="M";
}*/
//out.println("formId"+formId);

%>
<body>
<title><%=MC.M_Frm_DefineFrm%><%--Forms >> Define the Form*****--%> </title>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>


<br>
<jsp:useBean id="formlibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="catLibJB" scope="request"  class="com.velos.eres.web.catLib.CatLibJB"/>

<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.studyRights.StudyRightsJB"
%>
<DIV class="BrowserTopn" id="div1">


<%
 HttpSession tSession = request.getSession(true);
 int accountId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));
 int formLibId=0;
 String name="";
 String desc="";
 String type="";
 String status="";
// String sharedWith="";
 //String grpids="";
 //String shrdWith="";
 //String grpNames="";
 //String shrdWithNames="";
 //String shrdWithIds="";
 String statusPullDn="";
 String codeStatus="";
 int pageRight=0;

 int groupLen=0;
 int count=0;
 //String selNames="";%>
<%

 if (sessionmaint.isValidSession(tSession))
	{
		String userIdFromSession = (String) tSession.getValue("userId");
		String lnkFrom="";
		lnkFrom = request.getParameter("lnkFrom");

		if(lnkFrom==null)
		{lnkFrom="-";}

		String stdId=request.getParameter("studyId");
	    int studyId= EJBUtil.stringToNum(stdId);
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		ArrayList secIds=null;
		ArrayList repeatFields=null;
		FormLibDao formLibDao = new FormLibDao();
		formLibDao = formlibB.getSectionFieldInfo(StringUtil.stringToNum(formId));	
		secIds = formLibDao.getSecIds();
		repeatFields = formLibDao.getRepeatFields();
		if( calledFrom.equals("A")){ //from account form management
	 	    if (lnkFrom.equals("S"))
	 	      {
		 		 ArrayList tId ;
				 tId = new ArrayList();
				 if(studyId >0)
				 {
				 	TeamDao teamDao = new TeamDao();
					teamDao.getTeamRights(studyId,EJBUtil.stringToNum(userIdFromSession));
					tId = teamDao.getTeamIds();
					if (tId.size() <=0)
					 {
						pageRight  = 0;
					  } else
					  {
					  	StudyRightsJB stdRights = new StudyRightsJB();
						stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));

							ArrayList teamRights ;
							teamRights  = new ArrayList();
							teamRights = teamDao.getTeamRights();

							stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
							stdRights.loadStudyRights();


						if ((stdRights.getFtrRights().size()) == 0)
						{
						  pageRight= 0;
						} else
						{
							pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
						}
					  }
			    }
			 }
    	 	else
    	    {
    	  		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));
    		 }
	  	}

	 	if( calledFrom.equals("L")) //form library
	 	{
			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));
	 	}

		if( calledFrom.equals("St")) //study
	 	{
			StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
	  		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
		}




 if (pageRight >=4) ///flag added by salil
	{

		if( (mode == null)  || (mode.equals("null")))
			{

			 mode="M";
			}

		String accId=(String)tSession.getAttribute("accountId");
		String usrId = (String) tSession.getValue("userId");
		int iaccId=EJBUtil.stringToNum(accId);
		String catLibType="T" ;
		CatLibDao catLibDao= new CatLibDao();
		catLibDao = catLibJB.getAllCategories(iaccId,catLibType);
		ArrayList ids= new ArrayList();
		ArrayList names= new ArrayList();
		String pullDown;
		int rows;
		rows=catLibDao.getRows();
		ids = catLibDao.getCatLibIds();
		names= catLibDao.getCatLibNames();
		pullDown=EJBUtil.createPullDown("formType", 0, ids, names);

		CodeDao cDao = new CodeDao();
		if(calledFrom.equals("L")){
		cDao.getCodeValues("frmlibstat");
		}
		if(calledFrom.equals("St") || calledFrom.equals("A")){

		cDao.getCodeValues("frmstat");
		}
		// change  by salil
		CodeDao cd = new CodeDao();
		int cdId=0;
		cdId=cd.getCodeId("frmlibstat","W");

		//	end change by  salil
		statusPullDn = cDao.toPullDown("cmbStatus",cdId, false);


		int wipId=0;
		int activeId = 0;
		int lockdownId = 0;
		int deactivateId = 0;
		int offlineId = 0 ;
		int istatus =0 ;
		int freezeId = 0;
		int migrationId = 0;

        String inputClass = "inpDefault";
		String makeReadOnly = "";
		String makeGrey = "Black";
		String textGrey = "textDefault";
		String esigCheck = " CHECKED ";
		String sSigFlag = "1";
	  boolean showSubmit = true;

if(mode.equals("M"))
		{
		formLibId = EJBUtil.stringToNum(formId);

		formlibB.setFormLibId(formLibId);

		formlibB.getFormLibDetails();
		name=formlibB.getFormLibName();

		sSigFlag = formlibB.getEsignRequired();

		if (StringUtil.isEmpty(sSigFlag))
		{
			sSigFlag = "0";
		}

		if (sSigFlag.equals("0"))
		{
			esigCheck = "";
		}

		desc=formlibB.getFormLibDesc();
		desc = (   desc  == null      )?"":(  desc ) ;


		type=formlibB.getCatLibId();
		/*if(calledFrom.equals("L"))
			{
			shrdWithNames=formlibB.getShrdWithNames();
			shrdWithIds=formlibB.getShrdWithIds();
			sharedWith=formlibB.getFormLibSharedWith();
			}*/

		UserDao usrDao=new UserDao();
		UserDao userDao=new UserDao();
		pullDown=EJBUtil.createPullDown("formType", EJBUtil.stringToNum(type), ids, names);
		status=formlibB.getFormLibStatus();
		if(calledFrom.equals("L")){
		statusPullDn = cDao.toPullDown("cmbStatus", EJBUtil.stringToNum(status), false);
		}
		else
			{
			statusPullDn = cDao.toPullDown("cmbStatus", EJBUtil.stringToNum(status));
			}

		istatus = EJBUtil.stringToNum(status);


		if(calledFrom.equals("L"))
		{
			wipId=cd.getCodeId("frmlibstat","W");
			freezeId=cd.getCodeId("frmlibstat","F");
		}
		else if(calledFrom.equals("St") || calledFrom.equals("A"))
		{

			wipId=cd.getCodeId("frmstat","W");
			activeId=cd.getCodeId("frmstat","A");
			lockdownId = cd.getCodeId("frmstat","L");
			deactivateId = cd.getCodeId("frmstat","D");
			offlineId = cd.getCodeId("frmstat","O");
			migrationId = cd.getCodeId("frmstat","M");
		}



		if(wipId==istatus)
			{
			codeStatus = "WIP";
			showSubmit = true;
			}
		else if(activeId==istatus)
			{
				codeStatus = "Active";
				showSubmit = true;
			}
		else if(lockdownId == istatus)
			{
				codeStatus = "Lockdown";
				showSubmit = true;
			}
		else if(deactivateId == istatus)
			{
				codeStatus = "Deactivated";
				showSubmit = true;
			}
		else if(offlineId == istatus)
			{
				codeStatus = "Offline";
				showSubmit = true;
			}
		else if(freezeId == istatus)
			{
				codeStatus = "Freeze";
				showSubmit = true;
			}
	else if(migrationId == istatus)
			{
				codeStatus = "migration";
				showSubmit = true;
			}
			else
			{
				showSubmit = true;
			}

		}

		// modified by gopu on 29th April 2005 for fixing the bug # 2136
		if(!calledFrom.equals("L") && (codeStatus.equals("Offline") || codeStatus.equals("Active")|| codeStatus.equals("Lockdown"))   ){

					makeReadOnly = "readonly";
					inputClass = "inpGrey";
					makeGrey = "Grey";
					textGrey = "textGrey";

		} else if(calledFrom.equals("L") && codeStatus.equals("Freeze")){
					makeReadOnly = "readonly";
					inputClass = "inpGrey";
					//	makeGrey = "Grey";
					textGrey = "textGrey";
		}
		int count1=0;
		String tempRepeatField1="";
		secIds = formLibDao.getSecIds();
		int FLDcount=secIds.size();
		
		for(count1=0;count1<secIds.size();count1++)
		{
			tempRepeatField1=(repeatFields.get(count1)).toString();
			FLDcount+=Integer.parseInt(tempRepeatField1);
			
		}
		if (! codeStatus.equals("migration"))
		{
		%>
			<jsp:include page="formtabs.jsp" flush="true">
			<jsp:param name="formId" value="<%=formId%>"/>
			<jsp:param name="mode" value="<%=mode%>"/>
			<jsp:param name="codeStatus" value="<%=codeStatus%>"/>
			<jsp:param name="calledFrom" value="<%=calledFrom%>"/>
			<jsp:param name="lnkFrom" value="<%=lnkFrom%>"/>
			<jsp:param name="studyId" value="<%=studyId%>"/>
			<jsp:param name="formCount" value="<%=FLDcount%>"/>
			</jsp:include>
</div>

<SCRIPT LANGUAGE="JavaScript">


var screenWidth = screen.width;
var screenHeight = screen.height;
if(screenWidth>1280 || screenHeight>1024)
	document.write('<DIV class="BrowserBotN BrowserBotN_FLsetting_1 custom-BrowserBotN_FLsetting_1" id="div2" style="height: 70%">')
else
	document.write('<DIV class="BrowserBotN BrowserBotN_FLsetting_1 custom-BrowserBotN_FLsetting_1" id="div2" style="height: 70%"> ')


</SCRIPT>

			<BR>

<%
	}
if(!mode.equals("N")){
if(calledFrom.equals("L") && !codeStatus.equals("WIP")){%>
 <P class = "defComments"><FONT class="Mandatory"><%=MC.M_AnyChgNotSvd_FrzFrm%><%--Any changes made will not be saved for a Frozen form*****--%> </FONT></P>



<%}if((calledFrom.equals("St")||calledFrom.equals("A")) && codeStatus.equals("Deactivated"))
	{%>
 <P class = "defComments"><FONT class="Mandatory"><%=MC.M_ChgNotSvd_ForDeacFrm%><%--Any changes made will not be saved for Deactivated form*****--%> </FONT></P>
	<%}
if (codeStatus.equals("Offline")) {%>
 	 <P class="defComments"><FONT class="Mandatory"> <%=MC.M_OfflineChg_ApplicabNewFrm%><%--Some changes made in an Offline for Editing form may be applicable only to new forms and not to previously answered forms.*****--%></FONT></P>
	 <%}
}

//if(!calledFrom.equals("St")){out.println("codeStatus:"+codeStatus);%>
<!-- <A href="copyFormFromLib.jsp?srcmenu=tdmenubaritem4&selectedTab=3&codeStatus=<%=codeStatus%>&calledFrom=<%=calledFrom%>" onClick="return checkPermission(<%=pageRight%>)">Copy an Existing Form</A> -->
<%//}%>

<!-- modified by gopu on 29th April 2005 for fixing the bug # 2136 -->
<Form name="formlib" id="formlibid" class="custom-design-form" method="post" action="updateLibForm.jsp?calledFrom=<%=calledFrom%>&codeStatus=<%=codeStatus%>" onsubmit="if (validate(document.formlib)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<input type="hidden" name="selUsrId" value=<%=usrId%>>
<input type="hidden" name="selAccId" value=<%=accId%>>
<input type="hidden" name="FLDcount" value=<%=FLDcount%>>

<input type="hidden" name="srcmenu" value=<%=src%>>
<input type="hidden" name="selectedTab" value=<%=selectedTab%>>
<input type="hidden" name="codeStatus" value=<%=codeStatus%>>
<input type="hidden" name="calledFrom" value=<%=calledFrom%>>
<input type="hidden" name="lnkFrom" value=<%=lnkFrom%>>
<input type="hidden" name="studyId" value=<%=studyId%>>

<input type="hidden" name="activeId" value=<%=activeId%>>
<input type="hidden" name="offlineId" value=<%=offlineId%>>
<input type="hidden" name="deactivateId" value=<%=deactivateId%>>
<input type="hidden" name="lockdownId" value=<%=lockdownId%>>
<input type="hidden" name="wipId" value=<%=wipId%>>
<input type="hidden" name="preStatus" value=<%=istatus%>>
<input type="hidden" name="migrationId" value=<%=migrationId%>>
<input type="hidden" name="lfPk" value="<%=StringUtil.htmlEncodeXss(request.getParameter("lfPk"))%>">

 <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl midAlign">
      <tr>
		  <td width="20%"><Font class="<%=makeGrey%>"><%=LC.L_Frm_Name%><%--Form Name*****--%></Font>
		   <FONT class="Mandatory">* </FONT> </td>

		   <td >
		   <input class="<%=inputClass%>" type="text" name="txtName" size = 60 MAXLENGTH = 50 value="<%=name%>" <%=makeReadOnly%>>
		   </td>
	  </tr>
	 <tr>            <!-- @Gyanendra update for bug #4421 -->
		<td><br><Font class="<%=makeGrey%>"><%=LC.L_Form_Desc%><%--Form Description*****--%></Font> </td>
		<td   class="textareaheight"><br><TEXTAREA class=<%=textGrey%> name="txtAreaDesc" id="txtAreaDesc" rows=3 cols="50" MAXLENGTH=4000 <%=makeReadOnly%>><%=desc%></TEXTAREA>
		<font class="Mandatory"><div id="countxtAreaDesc"><%=MC.M_Limit4000_CharLeft %></div></font></td>
	  </tr>
	                <!-- @Gyanendra update for bug #4421 end -->
	<%
	   if( !calledFrom.equals("A") && !calledFrom.equals("St")){%>
	 <tr>
      <td ><br><%=LC.L_Form_Type%><%--Form Type*****--%><FONT class="Mandatory">* </FONT> </td>
	  <td ><br><%=pullDown%> </td>
	 </tr>
	<%}%>
	<tr>
     <td><br><%=LC.L_Form_Status%><%--Form Status*****--%> 
		  <FONT class="Mandatory">* </FONT> </td>
	  <td><br><%=statusPullDn%>&nbsp;&nbsp;&nbsp;<!-- <A href="">What does this mean?</A> --></td>
	  </tr>
	</table>
	  <br>
	 <table width="99%" cellspacing="0" cellpadding="0" border="0" class="midAlign">

<% if(!codeStatus.equals("Deactivated")){
	if((mode.equals("N") && (pageRight==5 || pageRight==7)) || (mode.equals("M") && (pageRight==6 || pageRight==7) && codeStatus.equals("WIP")) || (mode.equals("M") && (pageRight==6 || pageRight==7) && (calledFrom.equals("St") || calledFrom.equals("A")) ) )
	//if((pageRight == 5 && mode.equals("N")) || (pageRight == 6 && mode.equals("M")) || (pageRight == 7) && //(codeStatus.equals("WIP") && mode.equals("M")))
	{
		//if(calledFrom.equals("A") && ((lnkFrom.equals("S") && (pageRight==4 || pageRight==5)) || (lnkFrom.equals("A") && (pageRight==4 || pageRight==5)))) {
			//if(calledFrom.equals("A")){%>
			<!-- <tr>
			<td>
			<button onclick="window.history.back(); return false;"><%=LC.L_Back%></button>
				</td>
				</tr> --><%//}
			//}
		//else{%>

		<%	if (codeStatus.equals("Offline"))
		{ %>

				<tr><td colspan=3><input type="checkbox" name="migrateForm" value="1"><%=MC.M_MigrateResp_StatActive%><%--Migrate the existing form responses to the latest version (applicable when you change status to 'Active')*****--%>
				<P class="defComments"><FONT class="Mandatory"><%=MC.M_ChangedVal_ChoiceVersion%><%--If you changed 'Data Value(s)' in any Multiple Choice field, please migrate the existing form responses to the latest version*****--%></FONT></P>
				</td></tr>


		<% } else
		{
			%>
				<input type="hidden" name="migrateForm" value="0">

			<%
		}%>

		<tr><td colspan=3><input type="checkbox" name="esignReq" value="1" <%=esigCheck %>><%=MC.M_EsignMdtry_FrmResp%><%--e-Signature is Mandatory for this form's responses*****--%>	</td></tr>
    <tr>
	   <td>


		<%
			//KM-#4259

			String showSubmitParam = "";
			if (! codeStatus.equals("migration") && ( showSubmit))
			{		showSubmitParam = "Y";	}
			else
			{  showSubmitParam = "N"; }

		%>

		<jsp:include page="submitBar.jsp" flush="true">
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="formlibid"/>
				<jsp:param name="showDiscard" value="N"/>
				<jsp:param name="showSubmit" value="<%=showSubmitParam%>"/>
		</jsp:include>
   </td>
   </tr>

	   <input name="mode" type=hidden value=<%=mode%>></td>
	  <input name="formLibId" type=hidden value=<%=formLibId%>>


		   <%
		}
	}%>
		</table>

		<% if ( codeStatus.equals("migration"))
		{%>
			<p class = "redMessage"><%=MC.M_AppMigrating_FrmResps%><%--The application is migrating the form's responses to the latest Version. The form cannot be edited at this point.*****--%></p>
	<% }
		if (codeStatus.equals("Active"))
		{
			%>
			<p class = "redMessage"><%=MC.M_ChgFrmStat_FrmStatLnk%><%--To change the form status, please go back to the browser and click on the Form Status Link*****--%></p>

			<%

		}
	 %>
	<%if(calledFrom.equals("St"))
			{%>
		<A href="studyprotocols.jsp?srcmenu=tdmenubaritem3&selectedTab=7&mode=<%=mode%>&studyId=<%=studyId%>&calledFrom=<%=calledFrom%>"><%=MC.M_BackTo_StdSetupPage%><%--Back to the <%=LC.Std_Study%> Setup page*****--%></A>
		<%}
	 if(calledFrom.equals("L"))
		{%>
		<A href ="formLibraryBrowser.jsp?selectedTab=3&mode=final&srcmenu=<%=src%>"><%=MC.M_BackTo_FrmLib%><%--Back to the Form Library*****--%> </A>
		<%}
		 if(calledFrom.equals("A"))
		{%>
		<A href ="accountforms.jsp?selectedTab=&srcmenu=tdMenuBarItem2"><%=MC.M_BackTo_MgmtBrowser%><%--Back to the Form Management browser*****--%></A>
		<%}%>
  </form>


	 <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right
	}
else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div><div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/></div>

</body>

</html>


