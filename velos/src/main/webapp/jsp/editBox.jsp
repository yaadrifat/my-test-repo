<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Fld_EditBox%><%--Field Edit Box*****--%> </title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %><%@page import="com.velos.eres.service.util.*"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="include.jsp" flush="true"/>
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>
<script type="text/javascript" src="./js/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
     var fck;
     var oFCKeditor;
     checkQuote='N';
     function init()
      {
       
      /*oFCKeditor = new FCKeditor("nameta") ;
      oFCKeditor.BasePath = './FCKeditor/';
      oFCKeditor.Width = editorWidth ;
      oFCKeditor.Height = editorHeight ; // 400 pixels
      oFCKeditor.ToolbarSet="Velos";
      //oFCKeditor.Config['SkinPath'] = '/FCKeditor/editor/skins/office2003/' ;
      oFCKeditor.ReplaceTextarea() ;*/
      document.editBoxField.category.focus();		
      }
       function processFormat(formobj,mode)
      {
       
      	if (mode=="D")
	{
	 if (formobj.nameta.value.length>0)
	 formobj.nameta.value="[VELDISABLE]"+formobj.nameta.value;
	 //showHide("eformat","S");
	 //showHide("dformat","H");
	 document.poplayer=document.getElementById("dformat");
	 document.poplayer.innerHTML="<A href='javascript:void(0)' onClick='processFormat(document.editBoxField,\"E\")'><img src=\"./images/enableformat.gif\" alt=\"<%=LC.L_Enable_Formatting%><%--Enable Formatting*****--%>\" border = 0 align=absbotton></A>";
	 
	} else if (mode=="R")
	{
	   if (confirm("<%=MC.M_RemFmt_ForFldName%>"))/*if (confirm("Remove formatting for field name?"))*****/
	   {
	    formobj.nameta.value="";
	    
	   }
	} else if(mode=="E")
	{
	 formobj.nameta.value=replaceSubstring(formobj.nameta.value,"[VELDISABLE]","");
	 //showHide("eformat","H");
	 //showHide("dformat","S");
	 document.poplayer=document.getElementById("dformat");
	 document.poplayer.innerHTML="<A href='javascript:void(0)' onClick='processFormat(document.editBoxField,\"D\")'><img src=\"./images/disableformat.gif\" alt=\"<%=LC.L_Disable_Formatting%><%--Disable Formatting*****--%>\" border = 0 align=absbotton></A>";
	}
	
      }
    </script>
<script type="text/javascript">
     overRideChar("#");
   overRideChar("&");
   overRideFld("name");
   var editor;
    function FCKeditor_OnComplete(oFCKeditor)
{
	 fck= CKEDITOR.instances.nameta.getData();
	 
}
  </script>




<SCRIPT Language="javascript">
function toRefresh(formobj)
{
		if(formobj.toRefresh.value=="yes")
		{
			window.opener.location.reload();
		}
}

function findCheckBox(formobj)
{
	//Replace the <textarea id="editor1"> with a CKEditor
	//instance, using default configuration.

	CKEDITOR.replace( 'editor',{
		toolbar : 'Basic',
		height: 260,
		width:'140%',
	});
	for(i=0;i<formobj.editBoxType.length;i++)
	{
		if(formobj.editBoxType[i].checked)
		{
			if(formobj.editBoxType[i].value=="ET")
			{
				formobj.lines.readOnly=false;
				formobj.lengthOf.readOnly=false;
				formobj.characters.readOnly=false;
				formobj.defResponse1.readOnly=false;
				
				formobj.greaterless1.disabled=true;
				formobj.andor.disabled=true;
				formobj.greaterless2.disabled=true;
				
				formobj.val1.readOnly=true;
				formobj.val2.readOnly=true;	 
				formobj.numformat.disabled=true;
				formobj.overRideFormat.disabled=true;			
				formobj.overRideRange.disabled=true;
		
			
				formobj.datefld.disabled=true;
				formobj.datefld.checked=false;
				formobj.defaultdate.disabled=true;
				formobj.defaultdate.checked=false;
				
				formobj.overRideDate.disabled = true;
				formobj.overRideDate.checked = false;
			}
			else if(formobj.editBoxType[i].value=="EN")
			{
				formobj.lines.readOnly=true;
				formobj.lengthOf.readOnly=false;
				formobj.characters.readOnly=false;
				formobj.defResponse1.readOnly=false;
				
				formobj.greaterless1.disabled=false;
				formobj.andor.disabled=false;
				formobj.greaterless2.disabled=false;
				formobj.val1.readOnly=false;
				formobj.val2.readOnly=false;	 
				formobj.numformat.disabled=false;	
				formobj.overRideFormat.disabled=false;			
				formobj.overRideRange.disabled=false;

				formobj.datefld.disabled=true;
				formobj.datefld.checked=false;
				
				formobj.defaultdate.disabled=true;
				formobj.defaultdate.checked=false;
				
				formobj.overRideDate.disabled = true;
				formobj.overRideDate.checked = false;
			}
			else if(formobj.editBoxType[i].value=="ED")
			{
				formobj.lines.readOnly=true;
				formobj.lengthOf.readOnly=true;
				formobj.characters.readOnly=true;
				formobj.defResponse1.readOnly=true;
				
				formobj.greaterless1.disabled=true;
				formobj.andor.disabled=true;
				formobj.greaterless2.disabled=true;
				formobj.val1.readOnly=true;
				formobj.val2.readOnly=true;	 
				
				formobj.numformat.disabled=true;
				formobj.overRideFormat.disabled=true;			
				formobj.overRideRange.disabled=true;
			
				formobj.datefld.disabled=false;
				formobj.defaultdate.disabled=false;
				formobj.overRideDate.disabled = false;

		
			}
		}
	}
	
}

 function  validate(formobj)
 {
 	
  	if (   !(validate_col ('  Category', formobj.category)  )  )  return false
 	
	//if (   !(validate_col_spl ('  Field Display Name', formobj.name)  )  )  return false

    //if (!(validate_col('Field Name',formobj.name)))  return false;
	 
	 	 
	 if (checkChar(formobj.name.value,"'"))
	 {
	   alert("Special Character(') not allowed for this Field");
	   formobj.name.focus();
	    return false;
	 }
	 
	 if ((formobj.name.value.length)>2000)
	{
	  alert("<%=MC.M_FldNameMax2000_CrtCont%>");/*alert("'Field Name' length exceeds the maximum allowed limit of 2000 characters.\n Please correct to continue.");*****/
	  formobj.name.focus();
	  return false;
	}
	
	/*if (formobj.nameta.value.length>0)
	formobj.nameta.value=htmlEncode(formobj.nameta.value);*/
	
	if (   !(validate_col_spl ('Field Name', formobj.name)  )  )  return false
	if (   !(validate_col_spl ('  Field Uniqueid', formobj.uniqueId)  )  )  return false
	
	if ((fnTrimSpaces((formobj.uniqueId.value).toLowerCase())=='er_def_date_01') && (formobj.prevfldid.value!='er_def_date_01'))
	{
	  alert("<%=MC.M_ErDefDt01_SysKwrdFld%>");/*alert("'er_def_date_01' is a system reserved keyword. Please specify another Field Id.");*****/
	  formobj.uniqueId.focus();
	  return false;
	}

	if(!( validateDataSize(formobj.instructions,1000,'Field Help'))) return false
		
	//if (   !(validateDataRange (formobj.uniqueId.value, '>',20,'or','>',40)  )  ){ return false}
	
	
	
	if (! (splcharcheck(formobj.keyword.value)))
	{
		  formobj.keyword.focus();
		  return false;
	}
	
	if (! (checkquote(formobj.instructions.value)))
	{
		  formobj.instructions.focus();
		  return false;
	}
	 if (! (splcharcheckForXSL(formobj.instructions.value)))
	{
		  formobj.instructions.focus();
		  return false;
	} 
    
	
	if(formobj.editBoxType[2].checked)
	 {
	 if(!formobj.datefld.checked && formobj.overRideDate.checked)
	 {
	 	  alert("<%=MC.M_PlsChkDtFld_Overridden%>");/*alert("Please check the Date Field first which needs to be overridden");*****/
		  return false;
	 }
	 
	 
		if(formobj.datefld.checked)
		 {
		   formobj.hdndatefld.value= "1";
		 }
		 else {
		   formobj.hdndatefld.value= "0";
		 }
		 
		  if(formobj.overRideDate.checked)	
		formobj.hdnOverRideDate.value="1";		
		else
		formobj.hdnOverRideDate.value="0";
		
		if(formobj.defaultdate.checked)
		 {
		   formobj.hdndefaultdate.value= "1";
		 }
		 else {
		   formobj.hdndefaultdate.value= "0";
		 }
	 }

	if (   !(validateNumFormat(formobj.numformat) ) ) return false
	
	if(formobj.numformat.value== "" && formobj.overRideFormat.checked)
	{
	  alert("<%=MC.M_EtrNumFldFrmt_Overridden%>");/*alert("Please enter Number Field Format first which needs to be overridden");*****/
	  return false;
	}	
	
		
	if(formobj.overRideFormat.checked)	
			formobj.hdnOverRideFormat.value="1";		
		else
			formobj.hdnOverRideFormat.value="0";
		
		if(formobj.overRideRange.checked)	
			formobj.hdnOverRideRange.value="1";		
		else
			formobj.hdnOverRideRange.value="0";

	if(formobj.val1.value!="" && formobj.val2.value!="" && formobj.editBoxType[2].checked)
	 {
			formobj.greaterless1.disabled=false;
			formobj.andor.disabled=false;
			formobj.greaterless2.disabled=false;

	 }

if(formobj.val1.value!="" && formobj.greaterless1.value=="")
	 {
		 
		 alert("<%=MC.M_Selc_AnOpt%>");/*alert("Please select an option");*****/
		 formobj.greaterless1.focus();
		 return false;
	 }
	 if(formobj.val2.value!="" && formobj.greaterless2.value=="")
	 {	
		 alert("<%=MC.M_Selc_AnOpt%>");/*alert("Please select an option");*****/
		 formobj.greaterless2.focus();
		 return false;
	 }

	if(formobj.val1.value=="" && formobj.greaterless1.value!="")
	 {	
		 alert("<%=MC.M_PlsEnterValue%>");/*alert("Please enter Value");*****/
		 formobj.val1.focus();
		 return false;
	 }
	 if(formobj.val2.value=="" && formobj.greaterless2.value!="")
	 {	
		 alert("<%=MC.M_PlsEnterValue%>");/*alert("Please enter Value");*****/
		 formobj.val2.focus();
		 return false;
	 }

	 if(formobj.val1.value!="" && formobj.greaterless1.value!="" && formobj.val2.value!="" && formobj.greaterless2.value!="" && formobj.andor.value=="")
	 {	
		 alert("<%=MC.M_Selc_AnOpt%>");/*alert("Please select an option");*****/
		 formobj.andor.focus();
		 return false;
	 }
	 if(formobj.andor.value!="")
	 {	 
		if(formobj.greaterless1.value=="")
		 {
			alert("<%=MC.M_Selc_AnOpt%>");/*alert("Please select an option");*****/
			formobj.greaterless1.focus();
			return false;
		 }
		 if(formobj.greaterless2.value=="")
		 {
			 alert("<%=MC.M_Selc_AnOpt%>");/*alert("Please select an option");*****/
			 formobj.greaterless2.focus();
			 return false;
		 }
		 if(formobj.val1.value=="")
		 {
			 alert("<%=MC.M_PlsEnterValue%>");/*alert("Please enter Value");*****/
			 formobj.val1.focus();
			 return false;
		 }
		 if(formobj.val2.value=="")
		 {
			 alert("<%=MC.M_PlsEnterValue%>");/*alert("Please enter Value");*****/
			formobj.val2.focus();
			return false;
		 }
	 }
	 
	 
	/* if(formobj.val1.value!="" || formobj.val2.value!="")
	 {
		 if(formobj.greaterless1.value=="")
		 {
			 alert("Please select an option");
			 formobj.greaterless1.focus();
			 return false;
		 }
		 if(formobj.andor.value=="")
		 {
			 alert("Please select an option");
			 formobj.andor.focus();
			 return false;
		 }
		 if(formobj.greaterless2.value=="")
		 {
			 alert("Please select an option");
			 formobj.greaterless2.focus();
			 return false;
		 }
		 if( formobj.val1.value=="")
		 {
			 alert("Please enter Value");
			 formobj.val1.focus();
			 return false;
		 }
		if(formobj.val2.value=="")
		 {
			 alert("Please enter Value");
			 formobj.val2.focus();
			 return false;
		 }

	 }*/
	 if(isNaN(formobj.val1.value) == true) 
	{
		alert("<%=MC.M_NumValues_ForResps%>");/*alert("Enter only numeric values for responses.");*****/
		formobj.val1.focus();
		 return false;
   	}

	if(isNaN(formobj.val2.value) == true) 
	{
		alert("<%=MC.M_NumValues_ForResps%>");/*alert("Enter only numeric values for responses.");*****/
		formobj.val2.focus();
		 return false;
   	}
	 
if(formobj.val1.value =="" && formobj.val2.value =="" && formobj.editBoxType[1].checked && (formobj.overRideRange.checked))
	{
	  alert("<%=MC.M_PlsEtrValues_Overridden%>");/*alert("Please enter values in Response first which needs to be overridden");*****/
	  return false;
	
	} 		


     if (  !(validate_col('e-Signature',formobj.eSign)     )   ) return false



	/*if(isNaN(formobj.eSign.value) == true) 
	{
		alert*/<%--("<%=MC.M_IncorrEsign_EtrAgain%>");--%>
		/*alert("Incorrect e-Signature. Please enter again");
		formobj.eSign.focus();
		 return false;
   	}*/

	
	if ( formobj.editBoxType[0].checked)   
	{
		
		if(   (isNaN(formobj.lengthOf.value)== true)    )
		{
			alert("<%=MC.M_LthFld_EtrValid%>");/*alert("The length field takes a Number input. Please enter a valid Number");*****/
			formobj.lengthOf.focus();
			 return false;
			
		}   
			
		

		else if     (     (isNaN(formobj.lines.value)== true)  )
		{
			alert("<%=MC.M_LineNumInput_EtrValid%>");/*alert("The lines field takes a Number input.Please enter a valid Number");*****/
			formobj.lines.focus();
			  return false;
			
		}

		else if (      (isNaN(formobj.characters.value)== true)   )
		{  
			 alert("<%=MC.M_CharsFld_NumInput%>");/*alert("The characters field takes a Number input.Please enter a valid Number");*****/
		 	 formobj.characters.focus();
			  return false;
			
		}

	} 
	
	
	if    (   ( formobj.editBoxType[1].checked  )    )     
	{
			if(   (isNaN(formobj.lengthOf.value)== true)    )
			{
				alert("<%=MC.M_LthFld_EtrValid%>");/*alert("The length field takes a Number input. Please enter a valid Number");*****/
				formobj.lengthOf.focus();
				 return false;
				
			}   	
			if  ( isNaN(formobj.characters.value)== true)
			{
				alert("<%=MC.M_CharsFld_NumInput%>");/*alert("The characters field takes a Number input.Please enter a valid Number");*****/
				formobj.characters.focus();
				return false;
				
			}
			
			if  ( isNaN(formobj.defResponse1.value)== true)
			{
				alert("<%=MC.M_RespNumFld_CntText%>");/*alert("This default response of number input field cannot be text.Please enter a valid Number");*****/
				formobj.defResponse1.focus();
				return false;
				
			}
			
			/*if  ( isNaN(formobj.lines.value)== true)
			{
				alert("This lines field takes a number input.Please enter a valid Number");
				formobj.lines.focus();
				return false;
				
			}*/
			
	}  
	
	return true;
  }
  
  //Function to validate the radio button input boxes
  function disablingField(formobj,val, flag)
  {

	if ( flag == "C" )
	{		
	  	if  (( val) =="EN"  )
		{
		
			formobj.lines.readOnly=true;
			formobj.lengthOf.readOnly=false;
			formobj.characters.readOnly=false;
			formobj.defResponse1.readOnly=false;
			formobj.lines.value="";
			formobj.lengthOf.value="20";
			formobj.characters.value="100";

			formobj.greaterless1.disabled=false;
			formobj.andor.disabled=false;
			formobj.greaterless2.disabled=false;
			formobj.val1.readOnly=false;
			formobj.val2.readOnly=false;	 
			formobj.numformat.disabled=false;	
			
			formobj.overRideFormat.disabled=false;
			formobj.overRideRange.disabled=false;
			formobj.datefld.disabled=true;
			formobj.datefld.checked=false;
			formobj.defaultdate.disabled=true;
			formobj.defaultdate.checked=false;
			formobj.overRideDate.disabled = true;
			formobj.overRideDate.checked = false;
		
		}
	
		if (val=="ED") 
		{
				
			//formobj.defResponse2.readOnly=false;
		
			formobj.lines.readOnly=true;
			formobj.lengthOf.readOnly=true;
			formobj.characters.readOnly=true;
			formobj.defResponse1.readOnly=true;
			formobj.lines.value="";
			formobj.lengthOf.value="";
			formobj.characters.value="";
			formobj.defResponse1.value="";

			formobj.greaterless1.disabled=true;
			formobj.andor.disabled=true;
			formobj.greaterless2.disabled=true;
			formobj.greaterless1.value="";
			formobj.greaterless2.value="";
			formobj.val1.value="";
			formobj.andor.value="";
			formobj.val2.value="";
			formobj.val1.readOnly=true;
			formobj.val2.readOnly=true;	
			formobj.numformat.value="";
			formobj.numformat.disabled=true;
			formobj.overRideFormat.disabled=true;			
			formobj.overRideRange.disabled=true;
			formobj.overRideFormat.checked=false;			
			formobj.overRideRange.checked=false;
			formobj.datefld.disabled=false;
			formobj.defaultdate.disabled=false;
			formobj.overRideDate.disabled = false;


		

		}   
	
		if  (val =="ET"  )
		{
					
		
			formobj.lines.readOnly=false;
			formobj.lengthOf.readOnly=false;
			formobj.characters.readOnly=false;
			formobj.defResponse1.readOnly=false;
			formobj.lines.value="1";
			formobj.lengthOf.value="20";
			formobj.characters.value="100";

			formobj.greaterless1.disabled=true;
			formobj.andor.disabled=true;
			formobj.greaterless2.disabled=true;
			formobj.greaterless1.value="";
			formobj.greaterless2.value="";
			formobj.val1.value="";
			formobj.andor.value="";
			formobj.val2.value="";
			formobj.val1.readOnly=true;
			formobj.val2.readOnly=true;	 
			formobj.numformat.disabled=true;
			formobj.numformat.value="";
			formobj.overRideFormat.disabled=true;			
			formobj.overRideRange.disabled=true;
			formobj.overRideFormat.checked=false;			
			formobj.overRideRange.checked=false;
			formobj.datefld.disabled=true;
			formobj.datefld.checked=false;
			formobj.defaultdate.disabled=true;
			formobj.defaultdate.checked=false;
			formobj.overRideDate.disabled = true;
			formobj.overRideDate.checked = false;

		}

	}	
	
	if ( flag == "M")
	{
			  	if  (( val) =="EN"  )
		{
		
			formobj.lines.readOnly=true;
			formobj.lengthOf.readOnly=false;
			formobj.characters.readOnly=false;
			formobj.defResponse1.readOnly=false;
			formobj.lines.value="";

			formobj.greaterless1.disabled=false;
			formobj.andor.disabled=false;
			formobj.greaterless2.disabled=false;
			formobj.val1.readOnly=false;
			formobj.val2.readOnly=false;	 
			formobj.numformat.disabled=false;
			formobj.overRideFormat.disabled=false;			
			formobj.overRideRange.disabled=false;
			formobj.datefld.disabled=true;
			formobj.datefld.checked=false;
			formobj.defaultdate.disabled=true;
			formobj.defaultdate.checked=false;
			formobj.overRideDate.disabled = true;
			formobj.overRideDate.checked = false;

		}
	
		if (val=="ED") 
		{
				
			//formobj.defResponse2.readOnly=false;
		
			formobj.lines.readOnly=true;
			formobj.lengthOf.readOnly=true;
			formobj.characters.readOnly=true;
			formobj.defResponse1.readOnly=true;
			formobj.lines.value="";
			formobj.lengthOf.value="";
			formobj.characters.value="";
			formobj.defResponse1.value="";

			formobj.greaterless1.disabled=true;
			formobj.andor.disabled=true;
			formobj.greaterless2.disabled=true;
			formobj.greaterless1.value="";
			formobj.greaterless2.value="";
			formobj.val1.value="";
			formobj.andor.value="";
			formobj.val2.value="";
			formobj.val1.readOnly=true;
			formobj.val2.readOnly=true;	 
			formobj.numformat.value="";
			formobj.numformat.disabled=true;
			formobj.overRideFormat.disabled=true;			
			formobj.overRideRange.disabled=true;
			formobj.overRideFormat.checked=false;			
			formobj.overRideRange.checked=false;
			formobj.datefld.disabled=false;
			formobj.defaultdate.disabled=false;
			formobj.overRideDate.disabled = false;

			
	
		}   
	
		if  (val =="ET"  )
		{
					
		
			formobj.lines.readOnly=false;
			formobj.lengthOf.readOnly=false;
			formobj.characters.readOnly=false;
			formobj.defResponse1.readOnly=false;

			formobj.greaterless1.disabled=true;
			formobj.andor.disabled=true;
			formobj.greaterless2.disabled=true;
			formobj.greaterless1.value="";
			formobj.greaterless2.value="";
			formobj.val1.value="";
			formobj.andor.value="";
			formobj.val2.value="";
			formobj.val1.readOnly=true;
			formobj.val2.readOnly=true;	
			formobj.numformat.value="";
			formobj.numformat.disabled=true;
			formobj.overRideFormat.disabled=true;			
			formobj.overRideRange.disabled=true;
			formobj.overRideFormat.checked=false;			
			formobj.overRideRange.checked=false;
			formobj.datefld.disabled=true;
			formobj.datefld.checked=false;
			formobj.defaultdate.disabled=true;
			formobj.defaultdate.checked=false;
			formobj.overRideDate.disabled = true;
			formobj.overRideDate.checked = false;
			

		}


	
	}
	formobj.category.focus();
}
 // BK:#4960 TO MAKE ENTER KEY WORKING FOR FORM SUBMISSION	
  function fKeyUp(e)
  {
     var KeyID = (window.event) ? event.keyCode : e.keyCode;

     switch(KeyID)

     {
    
         case 13:
        	 if(!document.editBoxField.instructions.hasFocus) {        	
	        	 if (validate(document.editBoxField)== false) {        		 
	            	 setValidateFlag('false'); return false; 
	            } 
	             else {             
	                  setValidateFlag('true'); 
	                document.forms["editBoxField"].submit();
	                return true;               
	            }
        	 }    
             
        break;
     }
  }
  	
	
  
 

</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="catLibJB" scope="request"  class="com.velos.eres.web.catLib.CatLibJB"/>
<jsp:useBean id="fldVldJB" scope="request"  class="com.velos.eres.web.fldValidate.FldValidateJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.fldValidate.impl.FldValidateBean"%>

<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll" onLoad="findCheckBox(document.editBoxField);" onUnLoad="toRefresh(document.editBoxField);">
<%} else {%>
<body onLoad="findCheckBox(document.editBoxField);" onUnLoad="toRefresh(document.editBoxField);">

<%}%>
<%--For Bug# 8205:Yogendra--%>
<jsp:include page="popupPanel.jsp" flush="true"/>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div> 
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<DIV class="popDefault" id="div1"> 
 <%

	HttpSession tSession = request.getSession(true);
	int pageRight = 0;	
	
   if (sessionmaint.isValidSession(tSession))
   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
<%
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFLDLIB"));
	if (pageRight >=4)
	{
	
		String mode=request.getParameter("mode");
		
		String fieldLibId="";
		String fldCategory="";
		String fldName="";
		String fldNameFormat="";
		String formatDisable="N";
		String fldUniqId="";
		String fldKeyword="";
		String fldInstructions="";
		String fldDesc="";
		String fldType="";
		String fldDataType="ET";
		String fldExpLabel="";

		
		
		String fldLength="20";
		String fldLinesNo="1";
		String fldCharsNo="100";
		String fldDefResp1="";
		//String fldDefResp2="";
		String fldDefResp="";

		String fldValidateOp1 =  "";
		String fldValidateVal1 =  "";
		String fldValidateLogOp1 = ""; 
		String fldValidateOp2 =  ""; 
		String fldValidateVal2 =  "";	
		
		String formatChk = "0";
		String dateChk = "0",defaultDateCheck="0";
		
		String rangeChk = "0";
				
			
				
		String accountId=(String)tSession.getAttribute("accountId");
		int tempAccountId=EJBUtil.stringToNum(accountId);
		String catLibType="C" ;

		CatLibDao catLibDao= new CatLibDao();
		catLibDao = catLibJB.getAllCategories(tempAccountId,catLibType);
		ArrayList id= new ArrayList();
		ArrayList desc= new ArrayList();
		String pullDown;
		int rows;
		int fldValId = 0;
		String dateCheck="";
		String numformat="";
		rows=catLibDao.getRows();
		
		id = catLibDao.getCatLibIds();
		desc= catLibDao.getCatLibNames();
		pullDown=EJBUtil.createPullDown("category", 0, id, desc);
		pullDown = pullDown.replaceFirst("WIDTH:177px","WIDTH:250px");
		
		String toRefresh = request.getParameter("toRefresh");
		if(toRefresh==null)
		{
			toRefresh = "";
		}
		if  (   (mode.equals("M") )   ||  (mode.equals("RO" ) )  )
		{ 
				
				
				fieldLibId=request.getParameter("fieldLibId");
			
				fieldLibJB.setFieldLibId(Integer.parseInt(fieldLibId));
				fieldLibJB.getFieldLibDetails();
				fldExpLabel = ((fieldLibJB.getExpLabel())== null)?"":(fieldLibJB.getExpLabel()) ; 
										
				FldValidateBean fldValSk = fieldLibJB.getFldValidateSk();
				
				if (fldValSk!=null){
				fldValId = fldValSk.getFldValidateId();
								
				}
				if ((fldValSk!=null) && ( fldValSk.getFldValidateVal1() !=null || fldValSk.getFldValidateVal2() !=null)) 
				{
				
				
					
				fldValidateOp1 =  fldValSk.getFldValidateOp1() ;
				fldValidateVal1 =  fldValSk.getFldValidateVal1() ;
				fldValidateLogOp1 = fldValSk.getFldValidateLogOp1(); 
				fldValidateOp2 =  fldValSk.getFldValidateOp2() ; 
				fldValidateVal2 =  fldValSk.getFldValidateVal2() ;
					if(fldValidateOp1!=null){
					fldValidateOp1=fldValidateOp1.trim();
					}
					else{
					fldValidateOp1="";
					}
					if(fldValidateOp2!=null){
					fldValidateOp2=fldValidateOp2.trim();
					}
					else{
					fldValidateOp2="";
					}
					if(fldValidateLogOp1==null)
						fldValidateLogOp1="";
					if(fldValidateVal1==null)
						fldValidateVal1="";
					if(fldValidateVal2==null)
						fldValidateVal2="";
					
				
				}

				

				fldCategory=fieldLibJB.getCatLibId();
				
				rangeChk = ((fieldLibJB.getOverRideRange()) == null)?"0":(fieldLibJB.getOverRideRange()) ;
				dateChk = ((fieldLibJB.getOverRideDate()) == null)?"0":(fieldLibJB.getOverRideDate()) ;
				formatChk = ((fieldLibJB.getOverRideFormat()) == null)?"0":(fieldLibJB.getOverRideFormat()) ;
				
				pullDown=EJBUtil.createPullDown("category", EJBUtil.stringToNum(fldCategory), id, desc);
				pullDown = pullDown.replaceFirst("WIDTH:177px","WIDTH:250px");
				fldName=fieldLibJB.getFldName();
				fldNameFormat=fieldLibJB.getFldNameFormat();
				fldNameFormat=(fldNameFormat==null)?"":fldNameFormat;
				
				if (fldNameFormat.length()>0)
				{
				 if (fldNameFormat.indexOf("[VELDISABLE]")>=0)
				 formatDisable="Y";
				}
				
				
				fldUniqId=fieldLibJB.getFldUniqId();
								
				fldType=fieldLibJB.getFldType();
				fldDataType=fieldLibJB.getFldDataType();
				
				numformat  = fieldLibJB.getFldFormat();
				if(numformat == null)
					numformat ="";
				if(!fldUniqId.equals("er_def_date_01"))
				{
				dateCheck = fieldLibJB.getTodayCheck();
				}
				if(dateCheck==null)
				{
					dateCheck="";
				}

				if (fldDataType == null)
					fldDataType = "";
				
				fldKeyword = ((fieldLibJB.getFldKeyword()) == null)?"":(fieldLibJB.getFldKeyword()) ;	
				fldDefResp = ((fieldLibJB.getFldDefResp()) == null)?"":(fieldLibJB.getFldDefResp()) ;					
				
				fldInstructions = ((fieldLibJB.getFldInstructions()) == null)?"":(fieldLibJB.getFldInstructions()) ;		
				fldDesc = ((fieldLibJB.getFldDesc()) == null)?"":(fieldLibJB.getFldDesc());	
				
				if (fldDataType.equals("ED"))
				{
					if (fldDefResp.equals("[VELSYSTEMDATE]"))
					 defaultDateCheck="1";
				}	
				
				
				if ( ( fldDataType.equals ( "EN") )  ||  (fldDataType.equals("ET") )   )
				{
					fldDefResp1= fldDefResp  ;  
					fldLength = ((fieldLibJB.getFldLength()) == null)?"":(fieldLibJB.getFldLength()) ;	
					fldCharsNo = ((fieldLibJB.getFldCharsNo()) == null)?"":(fieldLibJB.getFldCharsNo()) ;	
					if ( fldDataType.equals("ET"))
					{
						 fldLinesNo = ((fieldLibJB.getFldLinesNo()) == null)?"":(fieldLibJB.getFldLinesNo()) ;		
					}
				}
		
				
		}
	
%>

	
	
    <Form name="editBoxField" id="editBoxFldFrm" class="custom-design-form
    " method="post" action="editBoxSubmit.jsp" onsubmit="if (validate(document.editBoxField)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" onkeyup=" return fKeyUp(e);">
	<Input type="hidden" name="mode" value=<%=mode%> />
	<Input type="hidden" name="fieldLibId" value=<%=fieldLibId%> />
	<Input type="hidden" name="hdndatefld"/> 
	<Input type="hidden" name="hdndefaultdate"/>
	<Input type="hidden" name="hdnOverRideDate"/> 
	<Input type="hidden" name="hdnOverRideFormat"/> 
	<Input type="hidden" name="hdnOverRideRange"/> 
	<Input type="hidden" name="prevfldid" value="<%=fldUniqId%>"/>
	<Input type="hidden" name="fldValId" value="<%=fldValId%>"/> <!--KV:BugNo.5213 -->
	<Input type="hidden" name="toRefresh" value="<%=toRefresh%>"/>
	
	<table width="90%" cellspacing="1" cellpadding="2" border="0">
		<tr height="25"><td colspan="3"><P class="sectionHeadings"> <%=MC.M_FldTyp_EdtBox_Upper%><%--FIELD TYPE:  EDIT BOX*****--%> </P> </td></tr>
    <tr class="browserEvenRow"> 
      <td width="20%"><%=LC.L_Category%><%--Category*****--%> <FONT class="Mandatory">* </FONT></td>
        <td width="60%"><%=pullDown%> </td>
	  </tr>



	  <tr class="browserEvenRow">
		<td width= "45%"> <%=LC.L_Fld_Name%><%--Field Name*****--%> <FONT class="Mandatory">* </FONT></td>
		<td width="30%">
		 
			<!-- Commented and Modified by Gopu to fix the bugzilla Issues #2595 -->
			<!--input type="text" name="name" size = 40 MAXLENGTH = 50 value="<%=fldName%>"--> 
			<input type="text" name="name" size = 60 MAXLENGTH = 1000 value="<%=fldName%>"/> 
		<input name="nameta" type="hidden" value="<%=fldNameFormat%>"/>
		</td>

	<td bgcolor="#cccccc"><A href="javascript:void(0);" onClick="openPopupEditor('editBoxField','name','nameta','Velos',2000)"><img src="./images/format.gif" alt="<%=LC.L_Launch_Editor %><%-- Launch Editor*****--%>" border = 0 ></A>
	<% if (fldNameFormat.length()>0)
	{
	if (formatDisable.equals("N")){%>
	<DIV id="dformat"><A href="javascript:void(0);" onClick="processFormat(document.editBoxField,'D')"><img src="./images/disableformat.gif" alt="<%=LC.L_Disable_Formatting %><%-- Disable Formatting*****--%>" border = 0 ></A></DIV>
	<%} else if (formatDisable.equals("Y")){%>
	<DIV id="dformat"><A href="javascript:void(0);" onClick="processFormat(document.editBoxField,'E')"><img src="./images/enableformat.gif" alt="<%=LC.L_Enable_Formatting %><%-- Enable Formatting*****--%>" border = 0 ></A></DIV>
	<%}%>
	<A href="javascript:void(0);" onClick="processFormat(document.editBoxField,'R')"><img src="./images/removeformat.gif" alt="<%=LC.L_Rem_Format %><%-- Remove Formatting*****--%>" border = 0 ></A>
	<%}%>		
	</td>
	</tr>

		<tr class="browserEvenRow">
		<td width="20%"> <%=LC.L_Fld_Id%><%--Field ID*****--%> <FONT class="Mandatory">* </FONT></td>
		<td width ="20%"><input type="text" name="uniqueId" size = 40 MAXLENGTH = 50 value="<%=fldUniqId%>"></td>
		</tr>

		<tr class="browserEvenRow">
			<td width="30%" >&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Edit_BoxType%><%--Edit Box Type*****--%></td>
           <td>
			<% if (fldDataType.equals("ET"))
			 { %>
			<input type="radio" name="editBoxType" value="ET"  onclick=  "disablingField(document.editBoxField,'ET','C')" CHECKED/> <%=LC.L_Text%><%--Text*****--%>
			<input type="radio" name="editBoxType" value="EN"  onclick=  "disablingField(document.editBoxField,'EN','C')"/> <%=LC.L_Number%><%--Number*****--%>
			<input type="radio" name="editBoxType" value="ED"  onclick=  "disablingField(document.editBoxField,'ED','C')"/> <%=LC.L_Date%><%--Date*****--%>
			<% }  
			 if (fldDataType.equals("EN")) 
			{ %>	
			<input type="radio" name="editBoxType" value="ET"  onclick=  "disablingField(document.editBoxField,'ET','C')" /> <%=LC.L_Text%><%--Text*****--%>
			<input type="radio" name="editBoxType" value="EN"  onclick=  "disablingField(document.editBoxField,'EN','C') "CHECKED/> <%=LC.L_Number%><%--Number*****--%>
			<input type="radio" name="editBoxType" value="ED"  onclick=  "disablingField(document.editBoxField,'ED','C') "/> <%=LC.L_Date%><%--Date*****--%>
			<% }
			if (fldDataType.equals("ED")) 
			{ %>	
			<input type="radio" name="editBoxType" value="ET"  onclick=  "disablingField(document.editBoxField,'ET','C')" /> <%=LC.L_Text%><%--Text*****--%>
			<input type="radio" name="editBoxType" value="EN"  onclick=  "disablingField(document.editBoxField,'EN','C') "/> <%=LC.L_Number%><%--Number*****--%>
			<input type="radio" name="editBoxType" value="ED"  onclick=  "disablingField(document.editBoxField,'ED','C') " CHECKED/> <%=LC.L_Date%><%--Date*****--%>
			<% }%>
			</td>
		</tr>	

	<tr class="browserEvenRow">
		 <td>&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Fld_Label%><%--Field Label*****--%></td>
         <td>
			  <%if(fldExpLabel.equals("1")){%>
		  <input type="checkBox" name="expLabel" value="<%=fldExpLabel%>" checked/><%=LC.L_Expand_FldLabel%><%--Expand Field Label*****--%>
			  <%}else{%>
		  <input type="checkBox" name="expLabel" value="<%=fldExpLabel%>" /><%=LC.L_Expand_FldLabel%><%--Expand Field Label*****--%>
			  <%}%>
           </td>
		</tr>

		 <tr class="browserEvenRow">
		<td width= "20%">&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Fld_Desc%><%--Field Description*****--%></td>
		<td width="30%"><input type="text" name="description" size = "60" MAXLENGTH = 500 value="<%=fldDesc%>"> </td>
		</tr>
		<tr class="browserEvenRow">
		<td width="20%"> &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Search_Keyword_S%><%--Search Keyword(s)*****--%> <img src="../images/jpg/help.jpg" onmouseover="return overlib('<%=MC.M_EtrOneOrMore_Kwrd%><%--Enter one or more keywords separated by a comma for use in querying.*****--%>',CAPTION,'<%=LC.L_Search_Keyword_S%><%--Search Keyword(s)*****--%>');" onmouseout="return nd();"></td>
		<td width ="20%"><input type="text" name="keyword" size = "60" MAXLENGTH = 255 value="<%=fldKeyword%>">
		
		</td>
			<tr class="browserEvenRow"><td ><label><%=MC.M_FldHelp_OnMouse%><%--Field Help <I> (On Mouse Over)*****--%></I></label></td>
	  	<td > <textarea name="instructions" cols="40" rows="3"  MAXLENGTH = 500 onfocus="this.hasFocus=true;" onblur="this.hasFocus=false;" ><%=fldInstructions%></textarea></td></tr>
		 	
		<tr>
			<td colspan="2"><P class="defComments"> <BR><%=MC.M_ForTxt_BoxOrNumFld%><%--For Text Box/Number Field*****--%> </P></td></tr>
			<tr  ><td width="10%"> &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Fld_Length%><%--Field Length*****--%> </td>
			<td><input type="text" name="lengthOf" size =10 MAXLENGTH = 25 value="<%=fldLength%>"/></td>
		</tr>	
			<tr>
				<td width="20%"> &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Number_OfLines%><%--Number of Lines*****--%> </td>
				<td><input type="text" name="lines" size = 10 MAXLENGTH = 25 value="<%=fldLinesNo%>"/></td>
	 		</tr>
			<tr  >
				<td width="24%"> &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_MaxChar_Numb%><%--Max. Characters/Numbers*****--%> </td>
				<td ><input type="text" name="characters" size =10 MAXLENGTH = 25  value="<%=fldCharsNo%>"/></td>
	 		</tr>
			 <tr> 
				<td width="20%"> &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Default_Resp%><%--Default Response*****--%>  </td>
				<td><input type="text" name="defResponse1" size = 60 MAXLENGTH = 50  value="<%=fldDefResp1%>"/>	</td>
	 		</tr>			
		
		<tr>
		<td width="30%"><P class="defComments"> <BR><%=LC.L_For_DateFld%><%--For Date Field*****--%> </P></td></tr><tr class="browserEvenRow">
		<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<%if(mode.equals("N")){%> 
		<input type="checkBox" name="datefld" disabled/>
		<%}
		else{
		if(dateCheck.equals("1")){%>
		 <input type="checkBox" name="datefld"  checked/>
		<%}else{%>
		<input type="checkBox" name="datefld" />
		<%}
		}%><%=MC.M_DtCnt_FutDt%><%--Date cannot be a future date*****--%>
	 	</td>
	 	</tr>
		<tr class="browserEvenRow">
		<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 		<%if(mode.equals("N")){%> 
	 			<input type="checkBox" name="overRideDate" disabled/>
	 		<%}
		 else{
		 	 if(dateChk.equals("1")){%>
		 	 <input type="checkBox" name="overRideDate" value="1" checked/>
		 	 	 <%}else{%>
		 	 <input type="checkBox" name="overRideDate" value="0"/>
	 		
	 		<%}}%>
			<%=LC.L_Override_DtValidation%><%--Override Date validation*****--%>	 		
	 		</td></tr>
			<tr class="browserEvenRow">
		<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<%if(mode.equals("N")){%> 
		<input type="checkBox" name="defaultdate" disabled/>
		<%}
		else{
		if(defaultDateCheck.equals("1")){%>
		 <input type="checkBox" name="defaultdate"  checked/>
		<%}else{%>
		<input type="checkBox" name="defaultdate" />
		<%}
		}%>
		<%=MC.M_DefDt_ToCurDt%><%--Default date to current date*****--%>
		</td>
	 	</tr>

		<tr>
		<td width="30%"><P class="defComments"> <BR><%=LC.L_For_NumberFld%><%--For Number Field*****--%> </P></td></tr>		
		<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Format%><%--Format*****--%> <img src="../images/jpg/help.jpg" onmouseover="return overlib('<%=MC.M_SpecifyFrmt_MaxNum_CanEtr%><%--Specify the format and maximum numbers that can be entered e.g. ### or ##.# etc. where `#` signifies a number and `.` signifies a decimal point.*****--%>',CAPTION,'<%=LC.L_Num_Format%><%--Number Format*****--%>');" onmouseout="return nd();">
		</td><td>
		<%if(mode.equals("N")){ 
			if (fldDataType.equals("ET"))
			{%>
		<input type="text" name="numformat" size = 15 MAXLENGTH = 25 disabled/>
			<%}}else{%>
		<input type="text" name="numformat" size = 15 MAXLENGTH = 25 value="<%=numformat%>"/> <!--KV:BugNo.5213 -->
				<%}%>
		 </td></tr><tr>
		 <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		 	 	<%if (fldDataType.equals("ET"))
			{%>
			 <input type="checkbox" name = "overRideFormat" disabled/>
			 	 <%}else{
			 	 if(formatChk.equals("1")){%>
			 <input type="checkbox" name = "overRideFormat" value="1" checked/>
			 	 <%}else{%>
			 	 <input type="checkbox" name = "overRideFormat" value="0"/>
			 <%}}%>
			 <%=MC.M_ORideNum_FmtValid%><%--Override Number Format Validation*****--%>
			 </td>	
	 	</tr>
 		  <tr>
			  	  <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			 	<%if(mode.equals("N")){ 
			if   (fldDataType.equals("ET"))
			{%>
				<input type="checkbox" name="overRideRange" disabled/>
			<%}}else{
					if(rangeChk.equals("1")){%>
				<input type="checkbox" name="overRideRange" value="1" checked/>
					<%}else{%>
				<input type="checkbox" name="overRideRange" value="0"/>
				
				<%}}%>
				<%=MC.M_ORideNum_RngValid%><%--Override Number Range Validation*****--%>
                 </td>
			</tr>		
			</table>


		<table width="98%">
			<tr>
			<td width="35%">&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Response_ShouldBe%><%--Response Should Be*****--%></td>
			<%if(mode.equals("N")){ 
			if (  fldDataType.equals("ET"))
			{%>
			<td ><select NAME="greaterless1" disabled>
			<option value=""> <%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>
			<option VALUE=">" ><%=LC.L_Greater_Than%><%--Greater Than*****--%> </option>
            <option VALUE="<"><%=LC.L_Less_Than%><%--Less Than*****--%> </option>
			<option VALUE=">="><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%></option>
			<option VALUE="<="><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%> </option>
			</select>
			</td>
			<td width="8%"><input type="text" name="val1" size=8 readOnly></td>
			 <td width="57%"><select NAME="andor" disabled>
			 <option value=""> <%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>
			 <option VALUE="and" ><%=LC.L_And%><%--And*****--%></option>
             <option VALUE="or"><%=LC.L_Or%><%--Or*****--%></option>
			</select>
			</td>
			</tr>
			</table>
			<table width="100%">
			<tr>
			<td width="40%"></td>
			<td ><select NAME="greaterless2" disabled>
			 <option value=""> <%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>
			<option VALUE=">"><%=LC.L_Greater_Than%><%--Greater Than*****--%></option>
            <option VALUE="<"><%=LC.L_Less_Than%><%--Less Than*****--%></option>
			<option VALUE=">="><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%></option>
			<option VALUE="<="><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%> </option>
			</select>
			</td>
			<td width="60%" align=left><input type="text" name="val2" size=8 readOnly></td>			 
			</tr>
			</table>



			<%}}else{
			%>
			<td><select NAME="greaterless1">
			   <option value=""> <%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>
			<%if(fldValidateOp1.equals(">")){%>	
			   <option VALUE=">" selected><%=LC.L_Greater_Than%><%--Greater Than*****--%></option>
               <option VALUE="<"><%=LC.L_Less_Than%><%--Less Than*****--%> </option>
			   <option VALUE=">="><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%></option>
			   <option VALUE="<="><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%> </option>
			<%}else if(fldValidateOp1.equals("<")){%>
			   <option VALUE=">"><%=LC.L_Greater_Than%><%--Greater Than*****--%></option>
               <option VALUE="<" selected><%=LC.L_Lesser_Than%><%--Lesser Than*****--%></option>
			   <option VALUE=">="><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%></option>
			   <option VALUE="<="><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%> </option>
			<%}else if(fldValidateOp1.equals(">=")){%>
			  <option VALUE=">"><%=LC.L_Greater_Than%><%--Greater Than*****--%></option>
               <option VALUE="<"><%=LC.L_Less_Than%><%--Less Than*****--%></option>
			   <option VALUE=">=" selected><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%></option>
			   <option VALUE="<="><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%> </option>
			<%}else if(fldValidateOp1.equals("<=")){%>
			<option VALUE=">" ><%=LC.L_Greater_Than%><%--Greater Than*****--%></option>
               <option VALUE="<"><%=LC.L_Less_Than%><%--Less Than*****--%></option>
			   <option VALUE=">="><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%></option>
			   <option VALUE="<=" selected><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%></option>
			<%}else {%>		
				<option VALUE=">" ><%=LC.L_Greater_Than%><%--Greater Than*****--%></option>
               <option VALUE="<"><%=LC.L_Less_Than%><%--Less Than*****--%></option>
			   <option VALUE=">="><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%></option>
			   <option VALUE="<=" ><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%></option>
				<%}%>		   
			</select></td>
			<td width="8%"><input type="text" name="val1" size=8 value="<%=fldValidateVal1%>"></td>
			<td width="57%"><select NAME="andor">
			 <option value=""> <%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>
	 		<%if(fldValidateLogOp1.equals("and")){%>
			  <option VALUE="and" selected ><%=LC.L_And%><%--And*****--%></option>
               <option VALUE="or"><%=LC.L_Or%><%--Or*****--%></option>
				  <%}else if(fldValidateLogOp1.equals("or")){%>
				 <option VALUE="and" ><%=LC.L_And%><%--And*****--%></option>
               <option VALUE="or" selected ><%=LC.L_Or%><%--Or*****--%></option>
				  <%}else{%>
				<option VALUE="and" ><%=LC.L_And%><%--And*****--%></option>
               <option VALUE="or"><%=LC.L_Or%><%--Or*****--%></option>
				<%}%>
          </select></td>
			</tr>
			</table>
			<table width="100%">
			<tr>
			<td width="40%"></td>
			<td ><select NAME="greaterless2">
			 <option value=""> <%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>
			<%if(fldValidateOp2.equals(">")){%>
               <option VALUE=">" selected><%=LC.L_Greater_Than%><%--Greater Than*****--%></option>
               <option VALUE="<"><%=LC.L_Less_Than%><%--Less Than*****--%></option>
			   <option VALUE=">="><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%></option>
			   <option VALUE="<="><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%></option>
			<%}else if(fldValidateOp2.equals("<")){%>
			 <option VALUE=">"><%=LC.L_Greater_Than%><%--Greater Than*****--%></option>
               <option VALUE="<" selected><%=LC.L_Lesser_Than%><%--Lesser Than*****--%>
			   <option VALUE=">="><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%>
			   <option VALUE="<="><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%>
			<%}else if(fldValidateOp2.equals(">=")){%>
			 <option VALUE=">" ><%=LC.L_Greater_Than%><%--Greater Than*****--%>
               <option VALUE="<"><%=LC.L_Less_Than%><%--Less Than*****--%>
			   <option VALUE=">=" selected><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%>
			   <option VALUE="<="><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%>
			<%}else if(fldValidateOp2.equals("<=")){%>
			 <option VALUE=">" ><%=LC.L_Greater_Than%><%--Greater Than*****--%>
               <option VALUE="<"><%=LC.L_Less_Than%><%--Less Than*****--%>
			   <option VALUE=">="><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%>
			   <option VALUE="<=" selected><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%>
			<%}
			else {%>		
			    <option VALUE=">" ><%=LC.L_Greater_Than%><%--Greater Than*****--%></option>
               <option VALUE="<"><%=LC.L_Less_Than%><%--Less Than*****--%></option>
			   <option VALUE=">="><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%></option>
			   <option VALUE="<=" ><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%></option>
				<%}%>
          </select></td>
			<td width="60%" align=left><input type="text" name="val2" size=8 value="<%=fldValidateVal2%>"></td>
			<!-- <td width="5%" align=right><input type="checkBox" name="overridden"></td>
			<td width="77%"><%=LC.L_CanBe_Overridden%><%--Can be Overridden*****--%></td> -->
			</tr>
			</table>
			
<%}%>
  	
	<% //BK:#4945 Moved this if block from outside of form tag.	
	if( mode.equals("M")  )
	{
		
		if ( ( fldDataType.equals ( "EN") )  ||  (fldDataType.equals("ET") )   )
		{
				if ( fldDataType.equals("ET"))
				{
%>
					<SCRIPT>						 
					 disablingField(document.editBoxField,"ET","M")
				 	
					</SCRIPT>
<%
				}
				else
				{
%>
						<SCRIPT>										
						disablingField(document.editBoxField,"EN","M")
						</SCRIPT>
<%	
				}
		}
		else
		{
%>
						<SCRIPT>										
						disablingField(document.editBoxField,"ED","M")
						
						</SCRIPT>
<%				
		}
	} // end of if for mode etc
	if (!mode.equals("RO") && ((pageRight == 5 && mode.equals("N")) || (pageRight == 6 && mode.equals("M")) || pageRight == 7))
 	{   %>
	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="editBoxFldFrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

	<%}%>
     <br>

 	 <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
 	 <td align=right> 

      </td> 
      </tr>
  </table>

  </Form>

 

<%
	} //end of if body for page right
	else
	{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%

	} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

}

%>
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</DIV>
</body>

</html>


		
	
		
		
		
			
		
		
		
	




