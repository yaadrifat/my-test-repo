<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=MC.M_EvtLib_EvtCrf%><%--Event Library >> Event Crf*****--%></title>
<!-- Akshi:Added for bug #6948 -->
<%-- Commented by Yogendra Pratap Singh : Bug# 6927 and 6947--%>
<%--<link type="text/css" href="./styles/bethematch/common.css" rel="STYLESHEET">
<link type="text/css" href="./styles/ns_gt1024.css" rel="STYLESHEET">--%>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script>


function openCRFWin(formRight,pageRight,eventId,srcmenu,duration,protocolId,calledFrom,fromPage,calStatus,eventmode,displayType,displayDur,crflibId, eventName,calAssoc)
{
  //KM-#3207
eventName=decodeString(eventName);
  if (f_check_perm(pageRight,'E') == false) {
	  return false;
  }
/*if(pageRight==4 || pageRight==6)
	{
		alert("New Permission Denied");
		return false;
	} */
 else if(formRight=='1')
	  	{/*param1="tempcrflib.jsp?mode=N&crflibmode=N&eventId="+eventId+"&srcmenu="+srcmenu+"&duration="+duration+"&protocolId="+protocolId+"&calledFrom="+calledFrom+"&fromPage="+fromPage+"&calStatus="+calStatus+"&eventmode="+eventmode+"&displayType="+displayType+"&displayDur="+displayDur+"&crflibId="+crflibId;
		windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=450,height=400,top=250 ,left=300");
		windowName.focus();*/
		param1="popuplnkfrmstudy.jsp?selectedTab=1&mode=M&calStatus="+calStatus+"&eventId="+eventId+"&protocolId="+protocolId+"&calledFrom="+calledFrom+"&fromPage="+fromPage+"&calassoc"+calAssoc;
		windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=500,height=400,top=250 ,left=300");
		windowName.focus()

		}

	else
	{
		param1="popupaddcrf.jsp?selectedTab=2&mode=N&crflibmode=N&eventId="+eventId+"&srcmenu="+srcmenu+"&duration="+duration+"&protocolId="+protocolId+"&calledFrom="+calledFrom+"&fromPage="+fromPage+"&calStatus="+calStatus+"&eventmode="+eventmode+"&displayType="+displayType+"&displayDur="+displayDur+"&crflibId="+crflibId+ "&formRight="+formRight+"&calassoc"+calAssoc;
		//JM: 11Jul2008, #3436
		if (calledFrom=='L'){
			windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=550,height=400,top=250 ,left=300");
		}
		else if (calledFrom=='P' || calledFrom=='S'){
			windowName= window.open(param1,"_self","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=450,height=400,top=250 ,left=300");
		}
		windowName.focus();

	}

}

//KM-4267
function openLinkform(formobj,pageRight,eventId,protocolId,studyId,calName,fromPage,calledFrom) {
	//KM-#3207
	if (f_check_perm(pageRight,'E') == false) {
	  return false;
	}
    formsp=formobj.formSP.value;
    formps=formobj.formPS.value;
    patLinks=formobj.patLinks.value;
    windowName = window.open("linkforms.jsp?eventId="+eventId+"&protocolId="+protocolId+"&studyId="+studyId+"&calName="+calName+"&formSP="+formsp+"&formPS="+formps+"&patLinks="+patLinks + "&fromPage=" + fromPage + "&calledFrom=" + calledFrom,"patient","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=600,height=500,left=300,top=300");
	windowName.focus();
}


</script>
<%  if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt") )

{ %>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>

<% } //end for if ((request.getParameter("fromPage")).equals("selectEvent"))

%>

<SCRIPT language="javascript">

var screenWidth = screen.width;
var screenHeight = screen.height;

function confirmBox(fileName,pgRight) {



	if (f_check_perm(pgRight,'E') == true) {



		var paramArray = [fileName];
		if (confirm(getLocalizedMessageString("M_Del_FrmCrfList",paramArray)))/*if (confirm("Delete " + fileName + " from CRF List?"))****/ {

		    return true;}

		else

		{

			return false;

		}

	} else {

		return false;

	}

}



</SCRIPT>



</head>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.esch.business.common.EventInfoDao,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.linkedForms.impl.LinkedFormsBean,com.velos.esch.web.eventCrf.EventCrfJB,com.velos.esch.business.common.CrfDao"%>

<jsp:useBean id="crflibB" scope="request" class="com.velos.esch.web.crflib.CrflibJB"/>

<jsp:useBean id="codelstB" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>

<jsp:useBean id="linkedFormsB" scope="request"  class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>



<% String src;



src= request.getParameter("srcmenu");
//SV, commented 10/28/04, eventname handling added
//String eventName = request.getParameter("eventName");
//eventName = StringUtil.encodeString(eventName);
String eventName ="";

%>


<%-- Modified by Yogendra Pratap Singh : Bug# 6927 and 6947--%>
<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt") ){%>
<jsp:include page="include.jsp" flush="true"/>
<%}else{

%>

<jsp:include page="panel.jsp" flush="true">



<jsp:param name="src" value="<%=src%>"/>



</jsp:include>

<%}%>





<body id="forms">

<%  if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt") ){%>

		<DIV class="popDefault" id="div1" style="height: 95%;">

	<%	}

else { %>

<DIV class="browserDefault" id="div1" style="height: 90%;">

<%}%>



<%

	int pageRight = 0;

	String duration = request.getParameter("duration");

	String protocolId = request.getParameter("protocolId");

	String calledFrom = request.getParameter("calledFrom");

	String eventId = request.getParameter("eventId");

	String mode = request.getParameter("mode");

	String fromPage = request.getParameter("fromPage");

	String calStatus = request.getParameter("calStatus");

	String eventmode = request.getParameter("eventmode");

	String displayDur=request.getParameter("displayDur");

	String displayType=request.getParameter("displayType");

	String calAssoc=request.getParameter("calassoc");
    	calAssoc=(calAssoc==null)?"":calAssoc;

	String formRight="0";
	String m;
	String lfDataCnt = "";
	if (fromPage.equals("eventlibrary")){
	  m="N";
	}
	else
	{
	if (mode.equals("N")){m="N";}else{m="E";}
	}


	//KM
	if (calledFrom.equals("P")||calledFrom.equals("L"))
	{
		  eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
		  eventdefB.getEventdefDetails();
		  eventName = eventdefB.getName();
	}else{	
		  eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
		  eventassocB.getEventAssocDetails();     
		  eventName = eventassocB.getName(); 
	 }			

 

	HttpSession tSession = request.getSession(true);



	if (sessionmaint.isValidSession(tSession))

	{

	   int count = 0;



	   if (calledFrom.equals("S")) {

   	   	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");

	  	   if ((stdRights.getFtrRights().size()) == 0){

			 	pageRight= 0;

		   }else{
				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
    	   }


		} else if (calledFrom.equals("P")) {

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
		}

		else if(calledFrom.equals("L")){

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("EVLIB"));

		}





	String uName = (String) tSession.getValue("userName");

	EventInfoDao eventinfoDao = crflibB.getEventCrfs(EJBUtil.stringToNum(eventId));
	ArrayList crflibIds= eventinfoDao.getCrflibIds() ;
	ArrayList crflibNumbers= eventinfoDao.getCrflibNumbers() ;
	ArrayList crflibNames = eventinfoDao.getCrflibNames();
	ArrayList crfFormFlags = eventinfoDao.getCrflibFormFlags();
	String crflibId= "";
	String crflibNumber= "";
	String crflibName = "";
	String crfFormFlag = "";
	int crfLen = crflibIds.size();
	String studyId="";
	String calName = "";
	if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary")  )
	{
	%>
	<!-- P class="sectionHeadings"> Event Library >> Event CRF  </P-->
	<%
	}
	else
	{
		calName = (String) tSession.getValue("protocolname");
	%>
	<P class="sectionHeadings"><%Object[] arguments = {calName}; %>
	    <%=VelosResourceBundle.getMessageString("M_PcolCal_EvtCrf",arguments)%><%--Protocol Calendar [ <%=calName%> ] >> Event CRF*****--%> </P>
	<%
	}
%>
<jsp:include page="eventtabs.jsp" flush="true">
<jsp:param name="duration" value="<%=duration%>"/>
<jsp:param name="protocolId" value="<%=protocolId%>"/>
<jsp:param name="calledFrom" value="<%=calledFrom%>"/>
<jsp:param name="fromPage" value="<%=fromPage%>"/>
<jsp:param name="mode" value="<%=mode%>"/>
<jsp:param name="calStatus" value="<%=calStatus%>"/>
<jsp:param name="displayDur" value="<%=displayDur%>"/>
<jsp:param name="displayType" value="<%=displayType%>"/>
<jsp:param name="eventId" value="<%=eventId%>"/>
<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="calassoc" value="<%=calAssoc%>"/>
</jsp:include>

<%
if(eventId == "" || eventId == null || eventId.equals("null") || eventId.equals("")) {
	%>
	  <jsp:include page="eventDoesNotExist.jsp" flush="true"/>
  <%
	}else{

  %>

<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024){
		if (navigator.userAgent.indexOf("MSIE") == -1){
			document.write('<DIV class="tabDefBotN" id="div2" style="height:58%; top:90px;">') 
		}else{
			document.write('<DIV class="tabDefBotN" id="div2" style="height:82.5%; top:65px;">')
		}
	}else{
		if (navigator.userAgent.indexOf("MSIE") == -1){
			document.write('<DIV class="tabDefBotN" id="div2" style="height:83%; top:92px;">')
		}else{
			document.write('<DIV class="tabDefBotN" id="div2" style="height:82.5%; top:100px;">')
		}
	}	
</SCRIPT>


<form name="crflib" method="POST">

<%
if (pageRight == 4) {

%>

   <P class = "defComments"><FONT class="Mandatory"><%=MC.M_OnlyViewPerm_ForEvt%><%--You have only View permission for the Event.*****--%></Font></P>

<%}%>





<TABLE width="90%">

   <tr>

	<td></td>

<%if(crfLen!=0){%>
	<td> <P class = "defComments"><%=LC.L_Assoc_Crfs%><%--Associated CRFs are*****--%> </P> </td>

<%}
else{%>
<td> <P class = "defComments" align=center> <%=MC.M_NoRecordsPresent%><%--No Records Present*****--%></p></td>
	<%}%>
<td colspan=2>

	<%

	if (calledFrom.equals("S")) {
       		studyId =(String)tSession.getValue("studyId");
	}
     	if (calledFrom.equals("P") || calledFrom.equals("S")){
     		calName = (String) tSession.getValue("protocolname");
			calName = StringUtil.encodeString(calName);
	}
     //CR Comment String studyId=(String) tSession.getValue("studyId");
	/*if ((calStatus.equals("A")) || (calStatus.equals("F")))

	{

	}else

	{*/%>
	 <P class = "defComments" align=center> 
		<A href="#" onClick="openCRFWin('<%=formRight%>','<%=pageRight%>','<%=eventId%>','<%=src%>','<%=duration%>','<%=protocolId%>','<%=calledFrom%>','<%=fromPage%>','<%=calStatus%>','<%=eventmode%>','<%=displayType%>','<%=displayDur%>','<%=crflibId%>','<%=StringUtil.encodeString(eventName)%>','<%=calAssoc%>')"><%=LC.L_Add_Crf%><%--Add CRF*****--%></A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<!--KM-#4267-->
		<A href="#" onClick="openLinkform(document.crflib,'<%=pageRight%>','<%=eventId%>','<%=protocolId%>','<%=studyId%>','<%=calName%>','<%=fromPage%>','<%=calledFrom%>')"><%=LC.L_Link_Forms%><%--Link Forms*****--%></A>
	</P>
	<%

	//}

	%>

	</td>

   </tr>

</TABLE>

<TABLE width="90%">


<%if(crfLen==0){%>

<%}else{%>
<tr>

          <th width="50%"> <%=LC.L_Crf_Number%><%--CRF Number*****--%> </th>

		  <th width="45%"> <%=LC.L_Crf_Name%><%--CRF Name*****--%> </th>

          <th><%=LC.L_Delete%><%--Delete*****--%></th>

</tr>





<%

   for(int i=0;i<crflibIds.size(); i++) {



	crflibId = (String)crflibIds .get(i);

	crflibNumber= (String)crflibNumbers.get(i);

	crflibName= (String)crflibNames.get(i);

	crfFormFlag = (String)crfFormFlags.get(i);








	if(crflibNumber==null)
	   {
		crflibNumber="-";
	   }

  	if ((i%2)==0) {



  %>

      <tr class="browserEvenRow">

        <%



		}



		else{



  %>

      <tr class="browserOddRow">

        <%



		}



  %>

	<td width="35%">

	<%=crflibNumber%>

	</td>



	<td width="45%"><A HREF="crflib.jsp?selectedTab=6&crflibmode=M&crflibId=<%=crflibId %>&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&calassoc=<%=calAssoc%>" onClick="return f_check_perm(<%=pageRight%>,'E')"> <%=crflibName%></A></td>

	<td width=10%>


	<A HREF="deletecrf.jsp?crfId=<%=crflibId%>&delMode=initial&crfmode=D&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&crfFormFlag=<%=crfFormFlag%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&calassoc=<%=calAssoc%>" onClick="return confirmBox('<%=crflibName%>',<%=pageRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A>


	</td>

   </tr>

<%

   }

%>

</TABLE>

<%}%>

<br>
	<table width="45%" cellspacing="0" cellpadding="0" border=0 >
			<tr>
          		<th  align="left" width="43%"> <%=LC.L_Associated_Forms%><%--Associated Forms*****--%> </th>
          		<!-- <th  align="left"><%=LC.L_Delete%><%--Delete*****--%></th>-->
			</tr>

			<tr>
				<td  align="center" width="10%"><b><%=LC.L_Frm_Name%><%--Form Name*****--%></b></td>
			</tr>
			<tr><td>&nbsp;</td></tr><br>
		</table>
<%

		String dispType="SP_PS";
		String prevDispType="";
		ArrayList formNamesArrLst = null;
		ArrayList formIdsArrLst=null;
		ArrayList formTypesArrLst=null;
		EventCrfJB eventCrfJB=new EventCrfJB();
		CrfDao crfDao=(CrfDao)eventCrfJB.getEventCrfForms(EJBUtil.stringToNum(eventId),dispType);
		formNamesArrLst = crfDao.getFormName();
		formIdsArrLst=crfDao.getFormId();
		formTypesArrLst=crfDao.getCrfFormType();
		int formcount = formNamesArrLst.size();
		String formName = "";
		String formId="";
		String formTypeLink="";
		String formSP="";
		String formPS="";

		int restrictedFormCount = 0;
		int allStudyFormCount = 0;

		if(formcount==0)
		{
		%>
		<table>
				<tr>
					<td  width="10%" align="left">
					<b><%=MC.M_FrmLnkPats_ToStd%><%--Forms Linked to <%=LC.Pat_Patients%> of this <%=LC.Std_Study%>*****--%></b>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
		</table>
		<br>
		<table>
					<tr>
						<td width="10%" align="left">
						<b><%=MC.M_FrmLkdPat_Std%><%--Forms Linked to <%=LC.Pat_Patient%>(All <%=LC.Std_Studies%>)*****--%></b>
						</td>
					</tr>
		</table>
		<br>
		<table>
					<tr>
						<td  width="10%" align="left">
						<b><%=MC.M_FrmLkdPat_StdRest%><%--Forms Linked to <%=LC.Pat_Patient%>(All <%=LC.Std_Studies%> - Restricted)*****--%></b>
						</td>
					</tr>
		</table>

		<%
		}
		for (int j=0 ; j<formcount; j++)
		{
		formName =((formNamesArrLst.get(j))==null)?"":formNamesArrLst.get(j).toString();
		formId=((formIdsArrLst.get(j)) == null)?"":formIdsArrLst.get(j).toString();
		formTypeLink=((formTypesArrLst.get(j))==null)?"":formTypesArrLst.get(j).toString();

		 if(formTypeLink.equals("PR") )
		 {
			restrictedFormCount = restrictedFormCount + 1;
		 }

		 if(formTypeLink.equals("PS") )
		 {
			allStudyFormCount  = allStudyFormCount  + 1;
		 }



		if (!formTypeLink.equals(prevDispType)){
			prevDispType=formTypeLink;
			if (formTypeLink.equals("SP") || j ==0){
	%>

			<table>
				<tr>
					<td width="10%" align="left">
					<b><%=MC.M_FrmLnkPats_ToStd%><%--Forms Linked to <%=LC.Pat_Patients%> of this <%=LC.Std_Study%>*****--%></b>
					<%  if(calledFrom.equals("L") || calledFrom.equals("P")){ %>
						<BR> &nbsp;&nbsp;&nbsp; <font class="mandatory">***** <%=LC.L_Not_Applicab%><%--Not Applicable*****--%> *****</font>
					<% } %>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
			</table>
			<% }  if(formTypeLink.equals("PS") ){
			%>
			<table>
					<tr>
						<td  width="10%" align="left">
						<b><%=MC.M_FrmLkdPat_Std%><%--Forms Linked to <%=LC.Pat_Patient%>(All <%=LC.Std_Studies%>)*****--%></b>
						</td>
					</tr>
			</table>
			<% }
			 if(formTypeLink.equals("PR") ){

				if (allStudyFormCount ==0)
				{

				%>

				<BR>
					<table>
					<tr>
						<td width="10%" align="left">
						<b><%=MC.M_FrmLkdPat_Std%><%--Forms Linked to <%=LC.Pat_Patient%>(All <%=LC.Std_Studies%>)*****--%></b>
						</td>
					</tr>
					</table>

				<BR>

				<%

				}

			%>
			<table>
					<tr>
						<td width="10%" align="left">
						<b><%=MC.M_FrmLkdPat_StdRest%><%--Forms Linked to <%=LC.Pat_Patient%>(All <%=LC.Std_Studies%> - Restricted)*****--%></b>
						</td>
					</tr>
			</table>
			<% }
		}

		if (formTypeLink.equals("SP"))
			formSP=formSP+formId+',';
		else
			formPS=formPS+formId+',';

%>

		<table  width="45%" cellspacing="0" cellpadding="0" border="1" >
		<%				if ((j%2)==0) {	%>

					<tr class="browserEvenRow">

					<%
					}
					else
					{ %>

				  <tr class="browserOddRow">
					<%
					}
		%>
		<td  width="50%" align="left">&nbsp;&nbsp;<%=formName%></td>
	</tr>
	 </table>
	<%
	}


	if (restrictedFormCount == 0 && formcount > 0)
	{
	if ( allStudyFormCount == 0)
	{
			%>

				<BR>
					<table>
					<tr>
						<td  width="10%" align="left">
						<b><%=MC.M_FrmLkdPat_Std%><%--Forms Linked to <%=LC.Pat_Patient%>(All <%=LC.Std_Studies%>)*****--%></b>
						</td>
					</tr>
					</table>

				<BR>


			<%

	}
	%>
	<br>
			<table>
					<tr>
						<td  width="10%" align="left">
						<b><%=MC.M_FrmLkdPat_StdRest%><%--Forms Linked to <%=LC.Pat_Patient%>(All <%=LC.Std_Studies%> - Restricted)*****--%></b>
						</td>
					</tr>
			</table>
	<%

	}
	  %>



<%

	ArrayList otherLinks = null;
	ArrayList formtypesOthers=null;
	dispType="link";
	crfDao=(CrfDao)eventCrfJB.getEventCrfForms(EJBUtil.stringToNum(eventId),dispType);
	otherLinks=crfDao.getOtherLinks();
	formtypesOthers=crfDao.getCrfFormType();
	int n=otherLinks.size();
	String otherLink="";
	String formTypeAllStud="";
	String patLinks="";



  %>
  <br>


	 <br>
	<table>
		<tr width="60%" align="left"><td><b><%=LC.L_Patient_Links%><%--<%=LC.Pat_Patient%> Links*****--%></b></td></tr>
	</table>

<table width="45%" cellspacing="0" cellpadding="0" border=1>

<%
for (int i=0;i<n;i++){
otherLink =((otherLinks.get(i))==null)?"":otherLinks.get(i).toString();
formTypeAllStud=((formtypesOthers.get(i))==null)?"":formtypesOthers.get(i).toString();
if( "N".equals(CFG.LAB_MODE) && "Labs".equalsIgnoreCase(otherLink) && "labs".equalsIgnoreCase(formTypeAllStud) ){/*Fixed for FR-550_All references to Adverse Events and Labs shall be removed from the Link Forms page(Display only when CFG.LAB_MODE='Y' configured in configuration file)*/
continue;
}/*End of FR-550_All references to Adverse Events and Labs shall be removed from the Link Forms page(Display only when CFG.LAB_MODE='Y' configured in configuration file)*/
else if("N".equals(CFG.ADV_EVENT) && "Adverse Events".equalsIgnoreCase(otherLink) && "adv".equalsIgnoreCase(formTypeAllStud) ){/*Fixed for FR-550_All references to Adverse Events and Labs shall be removed from the Link Forms page(Display only when CFG.ADV_EVENT='Y' configured in configuration file)*/
continue;
}/*End of FR-550_All references to Adverse Events and Labs shall be removed from the Link Forms page(Display only when CFG.ADV_EVENT='Y' configured in configuration file)*/

patLinks=patLinks+formTypeAllStud+",";

%>
	<tr class="browserEvenRow" width="40%" align="left"><td> <%=otherLink%></td></tr>
<%
}
%>
</table>
<table>

<tr>

  <td>

    <%

  	if (fromPage.equals("selectEvent")) {
         %>

		<A type="submit" href="selecteventus.jsp?fromPage=NewEvent"><%=LC.L_Back%></A>

 <%

 	}

 %>
<td></td>
  <%

  	if (fromPage.equals("eventbrowser")) {

  %>

		<A type="submit" href="eventbrowser.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>"><%=LC.L_Back%></A>

 <%

 	}

  	if (fromPage.equals("eventlibrary")) {

 %>

		<A type="submit" href="eventlibrary.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>"><%=LC.L_Back%></A>

 <%

 	}

  	//if (fromPage.equals("fetchProt")) {

  %>



		<!-- <A href="fetchProt.jsp?srcmenu=<%=src%>&selectedTab=4&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&pageNo=1&displayType=<%=displayType%>&displayDur=<%=displayDur%>" >Back to Protocol Calendar : Customize Event Details</A>	 -->

 <%

 	//}

 %>



 </td>

</tr>

</table>

<input type="hidden" name=formSP value="<%=formSP%>">
<input type="hidden" name=formPS value="<%=formPS%>">
<input type="hidden" name=patLinks value="<%=patLinks%>">
</form>
</div>



<%

	}//event

} else {  //else of if body for session



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%



}



%>



  <%if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt") ){}

else {

%>

</DIV>

<div class ="mainMenu" id = "emenu">

<jsp:include page="getmenu.jsp" flush="true"/>

</div>

	<% }%>
<!-- Akshi:Added for bug #6948 -->
</div>
  <div class = "myHomebottomPanel">

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>
</body>
<script>
function removecssfile(filename, filetype){
	 var targetelement=(filetype=="css")? "link" : "none" //determine element type to create nodelist from
	 var targetattr=(filetype=="css")? "href" : "none" //determine corresponding attribute to test for
	 var allsuspects=document.getElementsByTagName(targetelement)
	 for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
	  if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(filename)!=-1)
	   allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
	 }
	}
	removecssfile("velos_popup.css", "css") //remove all occurences "velos_popup.css" on page
</script>
</html>





