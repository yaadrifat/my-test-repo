<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>

<%@page import="java.math.BigDecimal"%><HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="calendar.js"></SCRIPT>

<SCRIPT  Language="JavaScript1.2">

	function fclose_to_role()
	{
		//window.opener.location.reload();
			setTimeout("self.close()",1000);
	}

	function toggleSelect(formobj,fld)
	{
		 selcount = parseInt(formobj.selectedCount.value);

	    if (fld.checked)
	    {
	    	selcount = selcount + 1;
	    }
	    else
	    {
	    	selcount = selcount - 1;
	    }

	    formobj.selectedCount.value = selcount;

	}

 	function setSelected(formobj,counter,totaldetail,isChecked)
	{

		var selcount ;
		var finalCount;
		var k;

		finalCount = parseInt(counter) + parseInt(totaldetail);

		selcount = parseInt(formobj.selectedCount.value);

			if (isChecked)
			{
				for (k=counter;k<finalCount;k++)
				{
					formobj.mileDetailCheck[k].checked = true;
					selcount = selcount + 1;
				}
			}
			else
			{
				for (k=counter;k<finalCount;k++)
				{
					formobj.mileDetailCheck[k].checked = false;
					selcount = selcount - 1;
				}
			}




		formobj.selectedCount.value = selcount;

	}


  function validate(formobj)
  {


  	  	var selcount;

  	  	selcount = parseInt(formobj.selectedCount.value );

  	  	if (selcount <= 0)
  	  	{
  	  		alert("<%=MC.M_PlsSel_AchvRec%>");/*alert("Please select atleast one Achievement Record");*****/
  	  		return false;
  	  	}

	  	return true;
  }





</SCRIPT>

</HEAD>

<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
  <jsp:useBean id="statusB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>
  <jsp:useBean id="mJB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>

  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,java.text.*"%>
  <%@ page import = "com.aithent.audittrail.reports.AuditUtils"%>
  <%

	String src = null;

	String eSign = request.getParameter("eSign");
	String mode = request.getParameter("mode");

	String studyId = request.getParameter("studyId");

	if (StringUtil.isEmpty(mode))
	{
		mode = "N";
	}


HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
<%
//   	String oldESign = (String) tSession.getValue("eSign");
	String eventId = (String) tSession.getValue("eventId");

//	if(!oldESign.equals(eSign))
//	{
%>

<%
//	}
//	else   	{

	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");



%>

  <!-- Get the achieved milestones -->
 <%
 	if (mode.equals("N"))
 	{
	 int totalmilecount  = 0;
	 int totalCounter = 0;
	 double mileAmountForGroupMilestones = 0.00;
	 int iPayCount = 0;
	 int iInvCount = 0;

 	%>

 	<DIV class="popDefault"  >
 	<p class="sectionHeadings"><%=LC.L_ViewOrDel_Achv%><%--View / Delete Achievements*****--%></p>

 	<table>
 		<tr><td><font class="mandatory"><%=MC.M_AchvRecDel_1Cnt2Inc%><%--An Achievement record cannot be deleted if: <br>1. It belongs to a Milestone rule with value of 'count' field more than 1 <br>2. It has been included in an Invoice or Payment record.*****--%>
 							  </font> </td></tr>
 	</table>

	 	<form name="delAch" id="delAchMSfrm" action="deleteAchMS.jsp" method="post" onSubmit="ripLocaleFromAll(); if (validate(document.delAch)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >
		<input type="hidden" name="studyId" value="<%=studyId%>"/>


	 		<table width="100%">
			<tr>

	        <th><%=LC.L_Select%><%--Select*****--%></th>
	        <th><%=LC.L_Milestone%><%--Milestone*****--%></th>
	        <th><%=LC.L_Ach_Date%><%--Count*****--%> </th>
	        <th><%=LC.L_Count%><%--Count*****--%> </th>
	        <th><%=LC.L_Mstone_Amt%><%--Milestone Amount*****--%></th>

	      </tr>

 	<%
 		// iterate through 5 milestone types

 		String[] arMilestoneType =  new String[4];
 		arMilestoneType[0] = "PM";
 		arMilestoneType[1] = "VM";
 		arMilestoneType[2] = "EM";
 		arMilestoneType[3] = "SM";
		//arMilestoneType[4] = "AM";//KM


 		String[] arMilestoneTypeHeader =  new String[4];
 		arMilestoneTypeHeader[0] = LC.L_Pat_StatusMstones/*Patient Status Milestones*****/;
 		arMilestoneTypeHeader[1] = LC.L_Visit_Mstones/*Visit Milestones*****/;
 		arMilestoneTypeHeader[2] = LC.L_Evt_Mstones/*Event Milestones*****/;
 		arMilestoneTypeHeader[3] = LC.L_Std_Mstones/*Study Milestones*****/;
 		//arMilestoneTypeHeader[4] = "Additional Milestones";


 		String displayMilestoneHeader ="";


 		for (int t = 0; t <arMilestoneType.length;t++)
 		{


 			%>

 			<tr >
		        <td colspan = 5><p class="sectionHeadings"><%=arMilestoneTypeHeader[t]%></p></td>
		     </tr>

		 	<%
	 		MileAchievedDao mile = new MileAchievedDao();

	 		MilestoneDao milestoneDao = new MilestoneDao();
	 		ArrayList arMilestoneIds = new ArrayList();
	 		ArrayList arMilestoneRuleDescs = new ArrayList();
	 		ArrayList arMilestoneAchievedCounts = new ArrayList();
	 		ArrayList arCountPerMilestone = new ArrayList();
	 		ArrayList arMileAmounts = new ArrayList();
	 		ArrayList arMileTotalInvoicedAmounts = new ArrayList();



	 		String mileStoneId = "";
	 		Hashtable htAchieved = new Hashtable();
	 		Hashtable moreParam = new Hashtable();

	 		int milecount = 0;
	 		int mileDetailCount = 0;
	 		String mileAchCount = "0";
	 		String mileDesc = "";
	 		String mileAchDate="";
		  	int countPerMilestone  = 0;

		   double calcAmount = 0.00;
		   double mileAmount = 0.00;
		   BigDecimal calcAmounts = new BigDecimal("0.0");
		   BigDecimal mileAmt = new BigDecimal("0.0");
	 		mile = mJB.getALLAchievedMilestones(EJBUtil.stringToNum(studyId ), arMilestoneType[t],moreParam);


			milestoneDao = mile.getMilestoneDao();



	 		htAchieved = mile.getHtAchieved();
 	 		arMilestoneIds = mile.getMilestone();

		    arMilestoneRuleDescs = mile.getMilestoneDesc();
			arMilestoneAchievedCounts = milestoneDao.getMilestoneAchievedCounts();
	 		arCountPerMilestone  = milestoneDao.getMilestoneCounts();
	 		arMileAmounts =  milestoneDao.getMilestoneAmounts();




	 		if (arMilestoneIds != null)
	 		{
	 			milecount = arMilestoneIds.size();

	 		}
	 		else
	 		{
		 		milecount  = 0;
	 		}

	 		totalmilecount = totalmilecount + milecount;

	 		// for each milestone, get the milestone achieved details

	 		for (int i =0; i < milecount ; i++)
	 		{


	 			mileDesc = (String ) arMilestoneRuleDescs.get(i);
	 			mileStoneId =  (String)arMilestoneIds.get(i) ;

				MileAchievedDao ach = new MileAchievedDao();
				ArrayList patients = new ArrayList();
				ArrayList ids = new ArrayList();
				ArrayList patientCodes = new ArrayList();
				ArrayList achievedOn = new ArrayList();

				ArrayList arInvCount =  new ArrayList();
				ArrayList arPayCount =  new ArrayList();



				if (htAchieved.containsKey(mileStoneId))
				{

				countPerMilestone =  EJBUtil.stringToNum((String) arCountPerMilestone.get(i));
				mileAchCount = (String) arMilestoneAchievedCounts.get(i);
				mileAmount = Double.parseDouble((String) arMileAmounts.get(i));
				mileAmt = new BigDecimal(mileAmount);



				if (countPerMilestone > 1)
				{
			 		mileAmountForGroupMilestones = mileAmount/countPerMilestone;
			 		mileAmt = new BigDecimal(mileAmountForGroupMilestones);
			 	}

					// 16Mar2011 @Ankit #5924
					if(mileAchCount==null)
					{
						mileAchCount="0";
					}
					ach = (MileAchievedDao) htAchieved.get(mileStoneId);
					ids = ach.getId();
					patients = ach.getPatient();
					mileDetailCount = ids.size();
					patientCodes = ach.getPatientCode();
					calcAmount = EJBUtil.stringToNum(mileAchCount) * mileAmount;
					calcAmounts=new BigDecimal(calcAmount);
					achievedOn = ach.getAchievedOn();
					arInvCount = ach.getArInvCount();
					arPayCount = ach.getArPaymentCount();

					if (ids.size() == 1 && StringUtil.isEmpty((String)ids.get(0)))
					{
					  mileAchCount = "0";
					  mileDetailCount = 0;
					calcAmount = 0.00;
					calcAmounts=new BigDecimal(calcAmount);
					}

				}
				else
				{
					mileAchCount = "0";
					mileDetailCount = 0;

					if(!arMilestoneType[t].equals("AM"))
					{
						calcAmount = 0.00;
						calcAmounts=new BigDecimal(calcAmount);
					}
					else
					{
						calcAmount = mileAmount;
						calcAmounts=new BigDecimal(calcAmount);
					}


				}


	 			%>
	 			<tr>
		 			<td>
		 			<%
		 				//added 1==2 so that checkbox is never shows, will impement this later
		 			if (mileDetailCount > 1 && countPerMilestone <= 1 && 1==2) {%>
		 			 <input type="checkbox" onClick= "setSelected(document.delAch,'<%=totalCounter%>','<%=mileDetailCount%>',this.checked )" name="selectMilestone" value="<%=mileStoneId%>"/>

		 			<% } %>
		 			</td>
		 			<td><b><%=mileDesc %></b></td>
		 			<td>&nbsp;</td>
		 			<td><%=mileAchCount%></td>


				 	<td><span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=calcAmounts%></span>
				 		<input type= "hidden"  name="mileStoneId"  value="<%=mileStoneId%>" />
				 		<input type= "hidden"  name="countPerMilestone"  value="<%=countPerMilestone%>" />
				 		<input type= "hidden"  name="mileAmountDue"  value="<%=calcAmount%>" />
				 		<input type= "hidden"  name="mileAchCount"  value="<%=mileAchCount%>" />
 				 	 	<input type= "hidden"  name="mileDetailCount"  value="<%=mileDetailCount%>" />
			 			<input type= "hidden"  name="mileAmountForGroupMilestones"  value="<%=mileAmountForGroupMilestones%>" />


				 	</td>

	 			</tr>


	 			<%
	 				for (int k = 0; k < mileDetailCount; k++)
	 				{

	 					iPayCount = ((Integer)arPayCount.get(k)).intValue();
	 					iInvCount = ((Integer)arInvCount.get(k)).intValue();

	 				%>

	  				<input type="hidden" name="mileDetFKAchieved<%=mileStoneId%>"  value="<%=(String)ids.get(k)%>" />



		 				<%
						if(k%2==0){
					%>
						<TR class="browserEvenRow">
					<%
					}else{
					%>
						<TR class="browserOddRow">
					<%
					}
					%>
					 <td>
					 <% if ( countPerMilestone <= 1 && iInvCount == 0 && iPayCount ==0) { %>
					     <input type="checkbox" name="mileDetailCheck"   onClick="toggleSelect(document.delAch,this);" value="<%= (String)ids.get(k)%>" />

					   <%
					   		totalCounter = totalCounter + 1;
					   } %>
					 </td>
		 			<td>&nbsp;</td>
		 			<td><%= (String) achievedOn.get(k)%></td>
		 			<td>&nbsp;&nbsp;&nbsp;<%= (String) patientCodes.get(k)%></td>	 			
		 			<td><span class="numberfield" data-unitsymbol="" data-formatas="currency">
		 			<%
				 		if (countPerMilestone <= 1)
				 		{
				 		%>
				 			<%=mileAmt%>

				 		<%
				 		}
				 		else { %>

					 		<%=mileAmt%>

				 		<%}

				 	%>
				 	</span></td>

				  		 			</tr>


	 				<%

	 				}


	 			// end of for
	 		}
	 	}	// for milestone types


 %>
 </table>

<P>
<br>
<input type= "hidden"  name="totalmilecount"  value="<%=totalmilecount%>" />
<input type= "hidden"  name="selectedCount"  value="0" />
<input type= "hidden"  name="mode"  value="F" />
<input type= "hidden"  name="totalDetailCounter"  value="<%=totalCounter%>" />



</P>
	<jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="N"/>
			<jsp:param name="formID" value="delAchMSfrm"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>

</form>
</div>
<%

 	}  //mode = N
 	else //final mode
 	{
 		int totalDetailCounter = 0;

 		totalDetailCounter = EJBUtil.stringToNum( request.getParameter("totalDetailCounter") );

 		String selectedACH[] = null ;

 		if (totalDetailCounter == 1)
 		{
 			selectedACH= new String[1];
			selectedACH[0] = request.getParameter("mileDetailCheck");

 		}
 		else if (totalDetailCounter > 1)
 		{
 			selectedACH	= request.getParameterValues("mileDetailCheck");
 		}

 		 int ret = 0;

 		 if (selectedACH != null && selectedACH.length > 0) //delete!
 		 {
 		 	//Modified for INF-18183 ::: Ankit
 		 	ret = mJB.deleteMSAch(selectedACH, AuditUtils.createArgs(session,"",LC.L_Milestones));
 		 }

 		 if (ret == 0)
 		 {
 		   %>
 		   <br><br><br><br><br> <p class = "successfulmsg" align = center>  <%=MC.M_DataWas_DelSucc%><%--Data was deleted successfully*****--%> </p>
 		   <script>
 		     fclose_to_role();
 		 	 window.opener.location.reload();
 		   </script>
 		   <%
 		 }
 		 else
 		 { %>
 		    <br><br><br><br><br> <p class = "successfulmsg" align = center>  <%=MC.M_DataNotDel_Succ%><%--Data was not deleted successfully*****--%> </p>

 		 <%

 		 }




 	}

//}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

<script>

</script>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</HTML>





