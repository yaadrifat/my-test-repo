<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.velos.eres.service.util.*"%><html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<title><%=LC.L_Activity_Rpt%><%-- Activity Report*****--%></title>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT language="Javascript">
function chrome_datepick(){
	var eResYearRange = "-"+YEAR_LOWER_BOUND+":"+"+"+YEAR_UPPER_BOUND ;
	 $.datepicker({ 
		 	changeMonth: true,
			changeYear: true,
			yearRange: eResYearRange,
			showButtonPanel: true,
			prevText: '',
			nextText: '',
			closeText: L_Clear,
			currentText: L_Today,
			dateFormat: jQueryDateFormat 
	    }); 
}
function fdateValidate(frmobj){

	var sUsrAg = navigator.userAgent;
	if (frmobj.filterType[0].checked == true)
	{
		if (sUsrAg.indexOf("Chrome") > -1) {
			chrome_datepick(); 
		}
		frmobj.submit();
		window.close();
	}
	else 
	{
		if (frmobj.dateFrom.value == ""){
			alert("<%=MC.M_Etr_FromDate%>");/*alert("Please enter 'From Date'.");*****/
			return false;
		}

		if (frmobj.dateTo.value == ""){
			alert("<%=MC.M_PlsEtr_ToDate%>");/*alert("Please enter 'To Date'.");*****/
			return false;
		}
		if (sUsrAg.indexOf("Chrome") > -1) {
			chrome_datepick();
		}
		frmobj.submit();
		window.close();
	} //end else
}
</SCRIPT>
	

<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<body>
	<%/* SV, 8/24/04, fix for bug 1673, _new is not a keyword, _blank is and it opens new window instance each time which is what we want for reports.*/%>
<%-- INF-20084 Datepicker-- AGodara --%>
	<jsp:include page="jqueryUtils.jsp" flush="true"></jsp:include>
	<DIV class="popDefault">
    <Form name="results" method="post" action="repRetrieve.jsp" target="_blank">
    
	<%HttpSession tSession = request.getSession(true);
	  	if (sessionmaint.isValidSession(tSession))
		{
	%>
			<Input type=hidden name="repName" value="Activity Report">
			<Input type=hidden name="repId" value="104"> 

			<table width="100%" border="0" cellspacing="0" cellpadding="0" border="0" class="basetbl midAlign outline">
				<tr><td><Font class="reportText"><%=LC.L_Date_Filter%><%-- Date Filter*****--%>:</font></td></tr>
				<tr>
				<td colspan="2">
					<input type="radio" name="filterType" value="1" checked><%=LC.L_All%><%-- All*****--%><BR>
					<input type="radio" name="filterType" value="4"><%=LC.L_Date_Range%><%-- Date Range*****--%>
				</td>
				</tr>
				<tr>
<%-- INF-20084 Datepicker-- AGodara --%>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_From_Date%><%-- From Date*****--%>:</td>
					<td><input type="text" name="dateFrom" class="datefield" READONLY></td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_To_Date%><%-- To Date*****--%>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td><input type="text"  name="dateTo" class="datefield" READONLY></td>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<br>
					<button onClick="return fdateValidate(document.results);"><%=LC.L_Display%></button> 
					</td>
				</tr>
			</table>



	</Form>
	</DIV>
	<%}//end of if body for session
		else{
	%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}

%>
</body>

</html>
