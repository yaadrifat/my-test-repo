<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="calendar.js"></SCRIPT>

<SCRIPT  Language="JavaScript1.2">
</SCRIPT>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>

<BODY>
  <jsp:useBean id="InvB" scope="request" class="com.velos.eres.web.invoice.InvoiceJB"/>
  <jsp:useBean id="InvDetB" scope="request" class="com.velos.eres.web.invoice.InvoiceDetailJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
  <%

	String src = null;

	
	 
	
	
HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))

   {
   
   		String oldESign = (String) tSession.getValue("eSign");
		String eSign = request.getParameter("eSign");
		
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%  

	if(!oldESign.equals(eSign)) 
	{
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} 
	else 
  	{
  	 
	   	
	
		
		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");
		
		String invPk = request.getParameter("invPk");
		
		 
		String arInvDetailPks[]  = null;
		String arMileAmountInvoiced[] = null;
		String arMileAmountHoldback[] = null;
                String arMileAmountHoldbackNew[] = {"0.00"};
		 String invDetailPK = "";
		 String amountInvoiced = ""; 
		 String amountHoldback = ""; 
		
		int totalDetailCount = 0;
		InvoiceDetailDao inv = new InvoiceDetailDao();
		
		 totalDetailCount = EJBUtil.stringToNum(request.getParameter("totalDetailCnt"));
		
		String invNum = request.getParameter("invNumber");
		String studyNum = request.getParameter("studyNumber");
		String invNumber = studyNum+"-"+ invNum.trim();
		String paymentDueIn = request.getParameter("paymentDueIn");
		//String paymentDueUnit = request.getParameter("paymentDueUnit");
		String addressedTo = request.getParameter("addressedTo");
		String sentFrom = request.getParameter("sentFrom");
		String invHeader = request.getParameter("invHeader");
		String invFooter = request.getParameter("invFooter");
		String invDate = request.getParameter("invDate");
		String intAccNum = request.getParameter("intAccNum");//JM:
		String invNotes = request.getParameter("invNotes");
		String arr="0.00";	
		if (totalDetailCount > 1)
		{
			arMileAmountInvoiced = request.getParameterValues("amountInvoiced");
			arMileAmountHoldback = request.getParameterValues("amountHoldback");
                        for(int i=0;i<totalDetailCount;i++)
			{
				try{
					if(null == arMileAmountHoldback){
						arMileAmountHoldback[i] = arr;//arMileAmountHoldbackNew;
					}
				}
				catch(Exception e){ System.out.println("@99 e===>>>"+e); }
			}
			arInvDetailPks = request.getParameterValues("invDetailPk")	;
			 
		}
		else
		{
			amountInvoiced = request.getParameter("amountInvoiced");
	 		arMileAmountInvoiced = new String[1];
			arMileAmountInvoiced[0] = amountInvoiced;
			
			amountHoldback = request.getParameter("amountHoldback");
			arMileAmountHoldback = new String[1];
			arMileAmountHoldback[0] = amountHoldback;
			
			invDetailPK = request.getParameter("invDetailPk")	;
			arInvDetailPks = new String[1];
			arInvDetailPks[0] = invDetailPK;
	  
		}
		
		for (int i = 0; i< totalDetailCount ; i++)
		{
		 	 	//set the default record for the invoice
		 	 	//Yogendra Pratap : Bug# 8764 :Date 28 Mar 2012
                                try{
		 	 		if(null == arMileAmountHoldback){
						arMileAmountHoldback[i] = arr;
					}
			 	inv.setAmountInvoiced(InvB.replaceValues(arMileAmountInvoiced[i],",","")); 
		 	 	inv.setAmountHoldback(InvB.replaceValues(arMileAmountHoldback[i],",",""));
				inv.setId(arInvDetailPks[i]) ; 
				inv.setLastModifiedBy(usr);
				inv.setIPAdd(ipAdd);
				inv.setInvPk(invPk);
				} 
		 	 	catch(Exception e){ System.out.println("Exception is"+e); }
		  
		} // for loop for total milestones
		
		int ret = 0;
		
		//update invoice data
				
				InvB.setId(EJBUtil.stringToNum(invPk));
				InvB.getInvoiceDetails();
				
				InvB.setInvAddressedto(addressedTo);
				InvB.setInvDate(invDate); 
				InvB.setInvFooter(invFooter) ;
				InvB.setInvHeader(invHeader) ;
				InvB.setInvNumber(invNumber) ;
				InvB.setInvSentFrom(sentFrom) ;
				InvB.setInvPayDueDate(paymentDueIn);
				//InvB.setInvPayUnit(paymentDueUnit);
				InvB.setLastModifiedBy(usr);
				InvB.setIPAdd(ipAdd);
				InvB.setInvNotes(invNotes);
				InvB.setIntnlAccNum(intAccNum);
				InvB.updateInvoice();
				
		ret = InvDetB.updateInvoiceDetails(inv);
		
		if (ret >= 0)
		{
		
		
	%>
	<BR><BR><BR><BR>
		<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
		
		<script>
			window.opener.location.reload();
		</script>
			
		<META HTTP-EQUIV=Refresh CONTENT="1; URL=viewInvoice.jsp?invPk=<%=invPk%>">
		<%
		} %>
	</div>
	<%

	} //for esign
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

<script>

</script>

</HTML>





