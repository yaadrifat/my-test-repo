<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<%
boolean isIrb = "irb_rpt_tab".equals(request.getParameter("selectedTab")) ? true : false;
if (isIrb) {
%>
<title><%=MC.M_ResComp_AppRep%><%--Research Compliance >> New Application >> Reports*****--%></title>
<% } else { %>
<title><%=LC.L_Study_Reports%> Display<%--Study >> Reports*****--%></title>
<% } %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT>
function fMakeVisible(type){
	typ = document.all("year");
	typ.style.visibility = "visible";
}


function fValidate(formobj){
	reportChecked=false;
	for(i=0;i<formobj.reportNumber.length;i++){
		sel = formobj.reportNumber[i].checked;
		if (formobj.reportNumber[i].checked){
	   		reportChecked=true;
		}
	}
	if (reportChecked==false) {
		alert("<%=MC.M_Selc_Rpt%>");/*alert("Please select a Report");*****/
		return false;
	}

	reportNo = formobj.repId.value;


	switch (reportNo) {
		case "44": //Protocol Calendar Template
			if (formobj.protId.value == "") {
				alert("<%=MC.M_Selc_PcolCal%>");/*alert("Please select a Protocol Calendar");*****/
				return false;
			}
		break;

		case "21": //Estimated Study Budget
			if (formobj.protId.value == "") {
				alert("<%=MC.M_Selc_PcolCal%>");/*alert("Please select a Protocol Calendar");*****/
				return false;
			}
		break;
	}
	return true;
}


function fSetId(ddtype,formobj){
	if (ddtype == "report") {//report Id and name are concatenated by %.Need to separate them
		for(i=0;i<formobj.reportNumber.length;i++)	{
			if (formobj.reportNumber[i].checked){
				lsReport = formobj.reportNumber[i].value;
				ind = lsReport.indexOf("%");
				formobj.repId.value = lsReport.substring(0,ind);
				formobj.repName.value = lsReport.substring(ind+1,lsReport.length);
				break;
			}
		}
	}
}

</SCRIPT>

<SCRIPT language="JavaScript1.1">
function openProtWindow(formobj) {
	lstudyPk = formobj.studyPk.value;
	rpType='all';
	windowName=window.open("protocolPopup.jsp?studyPk="+lstudyPk+"&reportType="+rpType,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");
	windowName.focus();
}

</SCRIPT>


</head>



<% String src="";
String from = "report";
src= request.getParameter("srcmenu");

	String studyIdForTabs = "";
 	studyIdForTabs = request.getParameter("studyId");



%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.section.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.studyRights.StudyRightsJB"%>

<jsp:useBean id="repdao" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao1" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>


<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<script language="JavaScript" src="overlib.js" type="text/javascript"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js" type="text/javascript"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js" type="text/javascript"><!-- overLIB (c) Erik Bosrup --></script>

<body style="overflow:hidden;">

<DIV class="BrowserTopn" id="divtab">
<% String includeTabsJsp = isIrb ? ("LIND".equals(CFG.EIRB_MODE) ? "ecompNewTabs.jsp" : "irbnewtabs.jsp") : "studytabs.jsp"; %>
<jsp:include page="<%=includeTabsJsp%>" flush="true">
<jsp:param name="from" value="<%=from%>"/>
	<jsp:param name="studyId" value="<%=studyIdForTabs%>"/>
</jsp:include>
</DIV>
<br>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<%
String stid= request.getParameter("studyId");
if(stid==null || stid=="" || "0".equals(stid)){%>
   <DIV class="BrowserBotN BrowserBotN_RC_1" id="div1">
	<%} else {%>
	<% if("Y".equals(CFG.Workflows_Enabled) && (stid!=null && !stid.equals("") && !stid.equals("0"))){ %>
			<DIV class="BrowserBotN BrowserBotN_S_3 workflowDivBig" id="div1">
	<% } else{ %>
			<DIV class="BrowserBotN BrowserBotN_S_3" id="div1">
	<% } %>
<%}

   HttpSession tSession = request.getSession(true);

   if (sessionmaint.isValidSession(tSession))
   {
   		int pageRight = 0;
    	String study = (String) tSession.getValue("studyId");
		String studyNumber = (String) tSession.getValue("studyNo");
        if(study == "" || study == null || "0".equals(study)||study.equalsIgnoreCase("")) {
%>
  <jsp:include page="studyDoesNotExist.jsp" flush="true"/>
  <%
	 	  } else {

	    	StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
			if ((stdRights.getFtrRights().size()) == 0){
		 		pageRight= 0;
			   }else{
				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYREP"));
	   		}

	if (pageRight >= 4)
   {

	String uName =(String) tSession.getValue("userName");
	String userId = (String) tSession.getValue("userId");
 	String acc = (String) tSession.getValue("accountId");
	String tab = request.getParameter("selectedTab");
	String studyPk = request.getParameter("studyId");
	String repId = request.getParameter("repId");
	String repName = request.getParameter("repName");
	int counter=0;
	String ver = "";

	%>
	<span><%=repName %>: <%=studyNumber %></span>
		<!-- include report Display-->
		<jsp:include page="reportDisplay.jsp" flush="true">
		<jsp:param name="repId" value="<%=repId%>"/>
		<jsp:param name="repName" value="<%=repName%>"/>
		<jsp:param name="paramstudyId" value="<%=study%>"/>
		<jsp:param name="repcat" value="<%=LC.L_Study_Lower%>"/>
		<jsp:param name="calledfrom" value="study"/>
		<jsp:param name="fltrChk" value="1"/>
		<jsp:param name="dnldChk" value="1"/>
		<jsp:param name="dateFrom" value=""/>
		<jsp:param name="dateTo" value=""/>
		</jsp:include>


<%
	} else {      //end of else body for page right
%>

		<jsp:include page="accessdenied.jsp" flush="true"/>
<%
	}
	}
} //end of if session times out

else

{

%>

<jsp:include page="timeout.html" flush="true"/>


<%

}

%>


<div>

<jsp:include page="bottompanel.jsp" flush="true"/>


</div>


</div>



<DIV class="mainMenu" id = "emenu">

  <jsp:include page="getmenu.jsp" flush="true"/>

</DIV>



</body>

</html>
