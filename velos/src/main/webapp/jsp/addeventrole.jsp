<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<%
	String fromPage = request.getParameter("fromPage");

	if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary") || fromPage.equals("patientschedule") ) {		
	%>
		<title> <%=MC.M_EvtLib_ResrcEvtRole%><%--Event Library >> Event Resource >> Event Role*****--%> </title>
	<%
	} else {%>
		<title><%=LC.L_Evt_RolePage%><%--Event Role Page*****--%></title>	
	<%
	}
%>


<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.StringUtil" %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



</head>

<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt"))

{ %>

<!-- YK 05Aug11: Removed for Bug #6728 already using Whichcss_skin.js in panel.jsp -->
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>



 <% } //end for if ((request.getParameter("fromPage")).equals("selectEvent"))



	%>



<SCRIPT Language="javascript">

function changeValue(rownum,role,formobj)
	{

	  selrow = rownum ;

	  if (formobj.roleId[selrow].checked)

		{

			formobj.selRole[selrow].value = role;
		}

	  else

		{
			formobj.selRole[selrow].value = "0";
		}

	}




 function  validate(formobj){
	mycount = 0;
 	cnt = formobj.selRole.length;

for(count = 0; count<cnt ; count++){
	if(formobj.roleId[count].checked){
	 mycount++;
	}
}



if(mycount == 0){
	alert("<%=MC.M_SelcAtleast_OneRole%>");/*alert("Please select atleast one Role");*****/
	return false;
}
	for(i = 0; i<cnt ; i++)
	{
		id = formobj.selRole[i].value;

		if ( id > 0)
		{

				if(isNaN(formobj.durationDD[i].value) == true)
				{
				alert("<%=MC.M_InvalidNum_EtrAgain%>");/*alert("Invalid Number.Please enter again");*****/
				formobj.durationDD[i].focus();
				return false;
				}

				if(isNaN(formobj.durationHH[i].value) == true)
				{
				alert("<%=MC.M_InvalidNum_EtrAgain%>");/*alert("Invalid Number.Please enter again");*****/
				formobj.durationHH[i].focus();
				return false;
				}

				if(isNaN(formobj.durationMM[i].value) == true)
				{
				alert("<%=MC.M_InvalidNum_EtrAgain%>");/*alert("Invalid Number.Please enter again");*****/
				formobj.durationMM[i].focus();
				return false;
				}

				if(isNaN(formobj.durationSS[i].value) == true)
				{
				alert("<%=MC.M_InvalidNum_EtrAgain%>");/*alert("Invalid Number.Please enter again");*****/
				formobj.durationSS[i].focus();
				return false;
				}

		}
		else
		{

				durationDD = parseInt(  (formobj.durationDD[i].value)  ) ;
				durationHH = parseInt(  (formobj.durationHH[i].value ) ) ;
				durationMM =parseInt( ( formobj.durationMM[i].value ) ) ;
				durationSS	= parseInt( (  formobj.durationSS[i].value) ) ;

				dd = false ; hh = false ; mm=false ; ss = false ;
				ddO = false ; hhO = false ; mmO =false ; ssO = false ;

				if (    isNaN(durationDD)   )
				{

					if (  !formobj.durationDD[i].value=="" )
					{
						alert("<%=MC.M_InvalidNum_EtrAgain%>");/*alert("Invalid Number.Please enter again");*****/
						formobj.durationDD[i].focus();
						 return false ;
					 }
					dd = true;


				}
				if (   isNaN(durationHH)  )
				{
					if (  !formobj.durationHH[i].value=="" )
					{
						alert("<%=MC.M_InvalidNum_EtrAgain%>");/*alert("Invalid Number.Please enter again");*****/
						formobj.durationHH[i].focus();
						return false ;
					}
					hh = true ;

				}

				if ( isNaN( durationMM  ) )
				{

					if (  !formobj.durationMM[i].value=="" )
					{
						alert("<%=MC.M_InvalidNum_EtrAgain%>");/*alert("Invalid Number.Please enter again");*****/
						formobj.durationMM[i].focus();
						return false ;
					}

					mm = true ;

				}
				if ( isNaN(  durationSS  ) )
				{

					if (  !formobj.durationSS[i].value=="" )
					{
						alert("<%=MC.M_InvalidNum_EtrAgain%>");/*alert("Invalid Number.Please enter again");*****/
						formobj.durationSS[i].focus();
						return false ;
					}

					ss = true ;

				}

			////////////////
				if ( durationDD > 0)
				{
					ddO = true;

				}
				if ( durationHH > 00)
				{
					hhO = true ;
				}
				if ( durationMM > 00  )
				{
					mmO = true ;
				}
				if ( durationSS > 00)
				{
					ssO= true ;
				}

			///////////



				if  (  ddO || hhO || mmO ||   ssO ||   (  ! ( formobj.notes[i].value==""  ) )       )
				{

				alert("Please check the role");
				formobj.roleId[i].focus( );
				return false;

				}

				if  (     (dd || !ddO)  && (hh || !hhO) && ( mm || !mmO )  && ( ss || !ssO )  )
				{


				}


			} //else of id ==0


	}



	if (!(validate_col('e-Signature',formobj.eSign))) return false



<%-- 	if(isNaN(formobj.eSign.value) == true) {

	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

	formobj.eSign.focus();

	return false;

   } --%>



}

</SCRIPT>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.EventuserDao"%>

<jsp:useBean id="EventuserB" scope="request" class="com.velos.esch.web.eventuser.EventuserJB"/>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>


<% String src;



src= request.getParameter("srcmenu");

//String eventName = request.getParameter("eventName");
String eventName = "";//KM


%>



<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){
%>

<jsp:include page="include.jsp" flush="true"/>
<%


}

 else{

%>

<jsp:include page="panel.jsp" flush="true">



<jsp:param name="src" value="<%=src%>"/>



</jsp:include>

<%}%>



<body >

<%if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){%>

		<DIV class="popDefault" id="div1">
		<br>
	<%	}

else { %>

<DIV class="formDefault" id="div1">

<%}%>

<%

	String duration = request.getParameter("duration");

	String protocolId = request.getParameter("protocolId");

	String calledFrom = request.getParameter("calledFrom");

	String eventId = request.getParameter("eventId");

	String mode = request.getParameter("mode");

	String calStatus = request.getParameter("calStatus");

	String eventmode = request.getParameter("eventmode");

	String displayDur=request.getParameter("displayDur");

	String displayType=request.getParameter("displayType");



	HttpSession tSession = request.getSession(true);



	if (sessionmaint.isValidSession(tSession))

	{

	   String uName = (String) tSession.getValue("userName");

	   EventuserDao evd = EventuserB.getAllRolesForEvent(EJBUtil.stringToNum(eventId));

	   ArrayList cDescs = evd.getResourceName();
	   ArrayList cIds = evd.getUserIds();
	   ArrayList cSels = evd.getEventUserIds();
   	   ArrayList cDuration = evd.getDuration();
   	   ArrayList cNotes = evd.getNotes();

	   int cRows = cIds.size();

	   String cDesc = "";
	   int cId = 0;
	   int cSel = 0;
	   String cRoleDur = null;
	   String cRoleNotes = null;

	   String calAssoc=request.getParameter("calassoc");
	    calAssoc=(calAssoc==null)?"":calAssoc;

		//KM
		if (calledFrom.equals("P")||calledFrom.equals("L"))
		{
       	  eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
       	  eventdefB.getEventdefDetails();
          eventName = eventdefB.getName();
//JM: 12Nov2009: #4399
	  	  eventName = (eventName==null)?"":eventName;
	  	  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
	  		eventName=eventName.substring(0,1000);
	  	  }

		}else{
       	  eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
          eventassocB.getEventAssocDetails();
       	  eventName = eventassocB.getName();
//JM: 12Nov2009: #4399
	  	  eventName = (eventName==null)?"":eventName;
	  	  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
	  		eventName=eventName.substring(0,1000);
	  	  }

		}

	if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary")  )
	{
	%>
	<!-- P class="sectionHeadings"> Event Library >> Event Resource >> Event Role  </P-->
	<%
	}else{
		String calName = (String) tSession.getValue("protocolname");
	%>
	<P class="sectionHeadings"> <%Object[] arguments = {calName}; %><%=VelosResourceBundle.getMessageString("M_PcolCalEvt_EvtRole",arguments)%><%--Protocol Calendar [ <%=calName%> ] >> Event Resource >> Event Role*****--%> </P>
	<%}%>

<jsp:include page="eventtabs.jsp" flush="true">
<jsp:param name="eventmode" value="<%=eventmode%>"/>
<jsp:param name="duration" value="<%=duration%>"/>
<jsp:param name="protocolId" value="<%=protocolId%>"/>
<jsp:param name="calledFrom" value="<%=calledFrom%>"/>
<jsp:param name="fromPage" value="<%=fromPage%>"/>
<jsp:param name="mode" value="<%=mode%>"/>
<jsp:param name="calStatus" value="<%=calStatus%>"/>
<jsp:param name="displayDur" value="<%=displayDur%>"/>
<jsp:param name="displayType" value="<%=displayType%>"/>
<jsp:param name="eventId" value="<%=eventId%>"/>
<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="calassoc" value="<%=calAssoc%>"/>
<jsp:param name="eventName" value="<%=StringUtil.encodeString(eventName)%>"/>
<jsp:param name="selectedTab" value="5"/>
 </jsp:include>

<form name="addevent" id="addevtrole" METHOD=POST action="rolesave.jsp?eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&calassoc=<%=calAssoc%>" onSubmit = "if (validate(document.addevent)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
<table width="100%" cellpadding="0" cellspacing="0" class="basetbl midalign" >
   <tr>
   	<th width="38%"> <%=LC.L_Role%><%--Role*****--%> <FONT class="Mandatory">* </FONT></th>
	<th width="40%"> <%=LC.L_Duration_DdHhMmSs%><%--Duration (DD:HH:MM:SS)*****--%> </th>
	<th width="22%"> <%=LC.L_Notes%><%--Notes*****--%> </th>
   </tr>

<%
	// change by salil
	int sidx = 0;
	// end change by salil
	int fidx = 0;
	int midx = 0;
	int lidx = 0;

    for(int i=0 ; i < cRows ; i++) {


 	// change by salil
    String ddval = "00";
	// end change by salil

	String hhval = "00";
   	String mmval = "00";
   	String ssval = "00";

	cDesc = (String) cDescs.get(i);
	cSel = EJBUtil.stringToNum(((String) cSels.get(i)));
    cId =  EJBUtil.stringToNum(((String) cIds.get(i)));
	cRoleDur = (String) cDuration.get(i);
	cRoleNotes = (String) cNotes.get(i);



	if (!(cRoleDur == null) && ! (cRoleDur.equals("null")) && cRoleDur.trim().length() > 0)
	{


		// get the dd, hh, mm, ss values

		fidx = 	cRoleDur.indexOf(":");
		midx = 	cRoleDur.indexOf(":",fidx+1);
		lidx = 	cRoleDur.lastIndexOf(":");

		ddval = cRoleDur.substring(0,fidx);
		hhval = cRoleDur.substring(fidx+1,midx);
		mmval = cRoleDur.substring(midx + 1,lidx);
		ssval = cRoleDur.substring(lidx+1);



	}

	if ((cRoleNotes == null) || (cRoleNotes.equals("null")) || (cRoleNotes.length() == 0))
	{
		cRoleNotes = "";
	}

%>

   <tr>
<%

	if(cSel == 0)
	{
%>
	<td class=tdDefault><input type="hidden" name="selRole" value=0>
	<input type="checkbox" name="roleId" value=<%=cId%> onclick="changeValue(<%=i%>, <%=cId%>, document.addevent)">
<%
	} else
	{
%>
	<td class=tdDefault> <input type="hidden" name="selRole" value=<%=cId%>>
	<input type="checkbox" name="roleId" value=<%=cId%> checked onclick="changeValue(<%=i%>, <%=cId%>, document.addevent)">


<%
	}

%>
 <%=cDesc%> </td>
<td class=tdDefault>
<input type="text" name="durationDD" maxlength = 2 SIZE = 2 value="<%=ddval%>">:
<input type="text" name="durationHH" maxlength = 2 SIZE = 2 value="<%=hhval%>">:
<input type="text" name="durationMM" maxlength = 2 SIZE = 2 value="<%=mmval%>">:
<input type="text" name="durationSS" maxlength = 2 SIZE =2 value="<%=ssval%>"></td>
<td class=tdDefault><input type="text" name="notes" maxlength = 1000  SIZE =70 value="<%=cRoleNotes%>"></td>
</tr>

<%}%>

</table>

<jsp:include page="propagateEventUpdate.jsp" flush="true">
<jsp:param name="fromPage" value="<%=fromPage%>"/>
<jsp:param name="formName" value="addevent"/>
<jsp:param name="eventName" value="<%=eventName%>"/>
</jsp:include>
<br>

<table width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<%-- fix for bug #1578 - added back button; moved it up in line with submit button --%>
	 <td bgcolor="<%=StringUtil.eSignBgcolor%>" align="center">
	 	<%-- fix for bug #10480 - added type="button" --%>
		<button type="button" onclick="window.history.back();"><%=LC.L_Back%></button>
	 </td>
     <td bgcolor="<%=StringUtil.eSignBgcolor%>" width="800" align="left">
     <jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="addevtrole"/>
		<jsp:param name="showDiscard" value="N"/>
        <jsp:param name="noBR" value="Y"/>
     </jsp:include>
     </td>
	</tr>
</table>
</form>

<%

} else {  //else of if body for session



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%



}



%>



  <%if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){}

else {
%>
</div>

<div class ="mainMenu" id = "emenu">

<jsp:include page="getmenu.jsp" flush="true"/>

</div>

<% }%>
 <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</form>
</DIV>
</body>

<%-- SV. 8/2/04, now that back button is always there, this is not necessary --%>
<%--if ((request.getParameter("fromPage")).equals("selectEvent")){ --%>
<%-- <button onclick="window.history.back();"><%=LC.L_Back%></button> <%}%> --%>



</html>




