<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventuser" scope="request" class="com.velos.esch.web.eventuser.EventuserJB"/>
<jsp:useBean id="budgetUsersB" scope="request" class="com.velos.esch.web.budgetUsers.BudgetUsersJB"/> 
<%@ page language = "java" import = "com.velos.esch.service.util.EJBUtil,com.velos.esch.web.eventuser.EventuserJB,com.velos.eres.service.util.*"%>

<%

String src = request.getParameter("srcmenu");
String duration = request.getParameter("duration");
String protocolId = request.getParameter("protocolId");
String calledFrom = request.getParameter("calledFrom");
String eventId = request.getParameter("eventId");
String mode = request.getParameter("mode");
String fromPage = request.getParameter("fromPage");
String userType = request.getParameter("userType");
String calStatus = request.getParameter("calStatus");
String eSign = request.getParameter("eSign");
String budgetId = request.getParameter("budgetId");
String propagationType = "";
String propagateInVisitFlag = request.getParameter("propagateInVisitFlag"); //SV, 9/16/04, cal-enh-05
String propagateInEventFlag = request.getParameter("propagateInEventFlag");
String calAssoc=request.getParameter("calassoc");
calAssoc=(calAssoc==null)?"":calAssoc;





int rows = 0;
rows = EJBUtil.stringToNum((String) request.getParameter("checkedrows")); 
String[] userIdArray = new String[100];
ArrayList userIds  = new ArrayList();
String userId = "";
String userName = "";
String selParam = "";
int pos = 0;

if(rows > 1) {
   userIdArray = request.getParameterValues("userId");
} else {
   selParam = request.getParameter("userId");
   pos = selParam.indexOf("*");
   userId = selParam.substring(0,pos);
   userName = selParam.substring(pos+1);
}

HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession)) {	
//JM: 16/09/05 	
	//String oldESign = (String) tSession.getValue("eSign");


	//if(!oldESign.equals(eSign)) {
//%-->
//  <jsp:include page="incorrectesign.jsp" flush="true"/> 
//<%
	//} else {
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");
	if(rows > 0) {
	   if(rows > 1) {
		for(int i =0; i<rows; i++) {
			selParam = userIdArray[i];
			pos = selParam.indexOf("*");
			userId = selParam.substring(0,pos);		   
	   	    userIds.add(userId);
			if(userName==null || userName==""){
				userName = selParam.substring(pos + 1);
			} else {
				userName = userName + ";" + selParam.substring(pos + 1);
			} 
		}
   	   } else {
	   	userIds.add(userId);
   	   }
	 
	   if (userType.equals("B")) { //from Budget
    	   budgetUsersB.setBgtUsers(userIds);
	 	   budgetUsersB.setBgtUsersType("I"); //individual user
 		   budgetUsersB.setBgtUsersBudget(budgetId);
		   budgetUsersB.setBgtUsersRights("000");
	 	   budgetUsersB.setCreator(usr); 
 		   budgetUsersB.setIpAdd(ipAdd);
		   budgetUsersB.setBudgetUsersDetails();	   
	   } else {	
    	   eventuser.setUserIds(userIds);
	 	   eventuser.setUserType(userType);
 		   eventuser.setEventId(eventId);
	 	   eventuser.setCreator(usr); 
 		   eventuser.setIpAdd(ipAdd);
		   eventuser.setEventuserDetails();
		}
	   
	   	// Cal-enh - 05, now check if the user wishes to propagate these changes to other events.
		if ((propagateInVisitFlag == null) || (propagateInVisitFlag.equals("")))
			propagateInVisitFlag = "N";

		if ((propagateInEventFlag == null) || (propagateInEventFlag.equals("")))
			propagateInEventFlag = "N";
		
		if ( (propagateInVisitFlag.equals("Y")) || (propagateInEventFlag.equals("Y"))) {
//SV, 10/28/04, changed parm "P" to calledFrom
			
			if (userType.equals("S"))
			{
				propagationType = "EVENT_MSG_USER";
			}
			else if (userType.equals("U"))
			{
				propagationType = "EVENT_USER_USER";
			}
			
	   		eventuser.propagateEventuser(EJBUtil.stringToNum(protocolId), propagateInVisitFlag, propagateInEventFlag, "N", calledFrom, propagationType);  
		}

	
		%>
		
	
		<form name = "usersave" method="POST">
		<table width=100%>
		<br><br><br><br><br><p class = "sectionHeadings" align = center><%=MC.M_Data_SvdSucc%><%-- Data was saved successfully.*****--%></p>
		
		<input type=hidden name="userName" value="<%=userName%>">
		<tr>
			<td align=center>
			<%
			if (userType.equals("S")) {
			%>
			<Script>	
				 	if (document.all)
					{
					window.opener.document.eventmessage.msgUsers.value = window.opener.document.eventmessage.msgUsers.value +  usersave.userName.value;
					}
				else
				{
				formobj = document.usersave;
				if (((navigator.appVersion).substring(0,1)) == '5') {
				 
				window.opener.document.eventmessage.msgUsers.value = window.opener.document.eventmessage.msgUsers.value + formobj.userName.value;
			} else {
				window.opener.document.div1.document.eventmessage.msgUsers.value = window.opener.document.div1.document.eventmessage.msgUsers.value + formobj.userName.value;
			}
			}
	
			self.close();

			</Script>
			<%
			} else {
			%>
		   
				<script>
				  window.opener.location.reload();
				  setTimeout("self.close()",1000);
				 </script>
				 			
			<%}
			%>
			</td>
		</tr>
		
		</table>
		</form>
	<%
	} //end of if for rows>0
	%>
<%
//JM}//end of if for eSign check
}//end of if body for session
else
{
%>
	<jsp:include page="timeout.html" flush="true"/> 
  	<%
}
%>

</BODY>
</HTML>


