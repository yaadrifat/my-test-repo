<%--
// Reset a logged-in user's current session.
// - The user has to be already logged in.
// - Only reset the calling user's session.
// - CSRFToken has to be valid for the request to be accepted.
 --%>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="tokenJB" scope="request" class="com.velos.eres.web.userSession.UserSessionTokenJB"/>
<%@page import="com.velos.login.LoginJB, com.velos.eres.service.util.StringUtil" %>
<%@page import="java.util.Date, com.velos.eres.service.util.DateUtil" %>
<%@page import="com.velos.eres.web.userSession.UserSessionTokenJB, com.velos.login.jwt.*" %>
<%!
private static final String USER_ID_STR = "UserID";
private static final String ZERO_STR = "0";
private static final String EMPTY_STR = "";
private static final String HOMEPAGE_JSP_STR = "myHome.jsp";
private static final String ACCOUNT_FREEZE_STR = "accountfreeze.jsp";
%>
<%
	HttpSession tSession = request.getSession(false);
    if (tSession == null) { return; }
    String password="";
	Integer userID = (Integer)tSession.getAttribute(USER_ID_STR);
	 password=(String)tSession.getAttribute("password");
	 request.setAttribute("pass", password);
    if (userID == null || userID < 1) { return; }
	userB.setUserId(userID);
	userB.getUserDetails();
	userB.setUserLoginFlag(ZERO_STR);
	userB.setUserCurrntSsID(EMPTY_STR);
	userB.updateUser();
	
	/* custom code start */
	String username = (String)tSession.getAttribute("username");
	//String password = (String)tSession.getAttribute("password");
    String jwttoken = (String)tSession.getAttribute("jwttoken");


	/* custom code end */
	
    tSession.invalidate();
    tSession = null;
	
	/* custom code start */
	tSession = request.getSession(true);
			
tSession.setAttribute("login", username);   // AVSP - Aug01'2016 - attribute set for project Fabric --> Only for Proof of concept
	tSession.setAttribute("password", password );  // AVSP - Aug01'2016  -  attribute set for project Fabric -->  Only for Proof of concept
	System.out.println("jwttoken generated - session was reset " + AuthHelper.createJsonWebToken(username, password, Long.parseLong("2"))); 
	tSession.setAttribute("jwttoken", AuthHelper.createJsonWebToken(username, password, Long.parseLong("2")) );
	
		/* custom code end */
	
    userB.getUserDetails();
    LoginJB loginJB = new LoginJB(request, userB);
    loginJB.fillSessionDataAfterLoginConfirmation();
    
	
	if ("A".equals(userB.getUserStatus())) {
		String eSignExp1=userB.getUserESignExpiryDate();
		if (StringUtil.isEmpty(eSignExp1)) eSignExp1= DateUtil.getFormattedDateString("9999","01","01");
		eSignExp1= DateUtil.dateToString(DateUtil.stringToDate(eSignExp1, null));

		String pwdExp1=userB.getUserPwdExpiryDate();
		if (StringUtil.isEmpty(pwdExp1)) pwdExp1=DateUtil.getFormattedDateString("9999","01","01");
		pwdExp1= DateUtil.dateToString(DateUtil.stringToDate(pwdExp1, null));

		Date eSignExpDate1 = DateUtil.stringToDate(eSignExp1,null);
		Date pwdExpDate1 = DateUtil.stringToDate(pwdExp1,null);
		Date d1 = new Date();
		if(d1.after(eSignExpDate1) || d1.after(pwdExpDate1)) {
%>
<form name="redirectForm" action="pwdexpired.jsp" method="post">
<input name="<%=UserSessionTokenJB.CSRF_TOKEN%>" value="<%=tokenJB.createTokenForUser(userID)%>">
</form>
<script>
document.redirectForm.submit();
</script>
<%			
			return;
		}
	} else if ("B".equals(userB.getUserStatus())) { // This probably will not happen but added here just in case
		response.sendRedirect(ACCOUNT_FREEZE_STR);
		return;
	}
    
	String homepage = codeLst.getCodeCustomCol(StringUtil.stringToNum(userB.getUserTheme()));
	if (StringUtil.isEmpty(homepage)) {
		homepage = HOMEPAGE_JSP_STR;
	}
	response.sendRedirect(homepage);
%>
