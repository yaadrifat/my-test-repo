<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<HTML>
<HEAD>
<title><%=LC.L_Inv_Creation%><%--Invoice Creation*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="include.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="calendar.js"></SCRIPT>

<SCRIPT LANGUAGE="javascript">
$j(function(){

	// add multiple select / deselect functionality
	$j(".allcovTypeOpt").click(function () {
		  //$j('.covTypeOpt').attr('checked', this.checked);
		$j(".covTypeOpt").removeAttr("checked");
	});
 
	// if any of checkboxes other than All is selected, uncheck All checkbox
	$j(".covTypeOpt").click(function(){ 
		if($j(".covTypeOpt").length == $j(".covTypeOpt:checked").length) {
			//$j(".allcovTypeOpt").attr("checked", "checked");
		} else {
			$j(".allcovTypeOpt").removeAttr("checked");
		} 
	});
        });
</SCRIPT>

</HEAD>

<script>

	jQuery().ready(function($) {
		// Assinging current date as dafult value to Invoice date
		jQuery('input[name=invDate]').datepicker( "setDate" , "new Date(jQueryDateFormat)" );
	});
	
function setMainDateFilter(formobj)
{
    var dtFilterFrom;
    var dtFilterTo;
    var yr;
    var month;
    var monthYr;
    var lastDayOfMonth;	
    var monthPrefix = "";
    		

	if (formobj.dateRangeType[0].checked) //ALL
	{
		dtFilterFrom = "";
		dtFilterTo = ""; 

	}
		else if (formobj.dateRangeType[1].checked) //Year Filter
	{
		
		
		yr = formobj.yearFilter.value;

		if (yr == '0')
		{
			alert("<%=MC.M_Selc_AnYear%>");/*alert("Please select an Year");*****/
			formobj.yearFilter.focus()			
			return false;
		}

		dtFilterFrom = getFormattedDateString(  yr,  "01",  "01");
		dtFilterTo = getFormattedDateString(  yr,  "31",  "12");

	}
	else if (formobj.dateRangeType[2].checked) //Month Filter
	{

		monthYr = formobj.monthFilterYear.value;			
		month =  formobj.monthFilter.value;			

		if (month == '0')
		{
			alert("<%=MC.M_Selc_Month%>");/*alert("Please select a Month");*****/
			formobj.monthFilter.focus()			
			return false;
		}

		if (monthYr == '0')
		{
			alert("<%=MC.M_Selc_AnYear%>");/*alert("Please select an Year");*****/
			formobj.monthFilterYear.focus()			
			return false;
		}
		lastDayOfMonth = getLastDayOfMonth(month,monthYr);
		
		if (month > 0 && month <10)
		{
			monthPrefix = "0";
		}		
		else
		{
			monthPrefix = "";
		}
		dtFilterFrom =  getFormattedDateString(  monthYr,  "01",  month );  
		dtFilterTo =  getFormattedDateString(  monthYr,  lastDayOfMonth,  month );   


	} else if (formobj.dateRangeType[3].checked) //date Range Filter
	{

		dtFilterFrom = formobj.drDateFrom.value;
		dtFilterTo = formobj.drDateTo.value;
		
		if(dtFilterFrom ==''||dtFilterFrom ==null){
			alert("<%=MC.M_SelFromDt_ForRangeFilter%>");/*alert("Please select a 'From Date' for the Date Range filter");*****/
			return false;
		}
		if(dtFilterTo ==''||dtFilterTo  ==null){
			alert("<%=MC.M_SelToDt_ForRangeFilter%>");/*alert("Please select a 'To Date' for the Date Range filter");*****/
			return false;
		}
		
	}


	formobj.dtFilterDateFrom.value = dtFilterFrom;
	formobj.dtFilterDateTo.value = dtFilterTo;
	
	return true;

}
/* FIN-22382_0926012 Enhancement 31-Oct-2012 -Sudhir*/
/*This function is used to check invoice is existed or not*/
function checkValidInvc() {
	var flag=true;

	if($j("#invNumber").val()!=""){
			if(($j("#invNumber").val()).indexOf("%")!= -1){
				var paramArray = ["%"];
				alert(getLocalizedMessageString("M_NoSplCharAllowed", paramArray));
				$j("#invNumber").focus();
				return false;
		 	}
			if(($j("#invNumber").val()).indexOf("\"")!= -1){
				var paramArray = ["\""];
				alert(getLocalizedMessageString("M_NoSplCharAllowed", paramArray));
				$j("#invNumber").focus();
				return false;
		 	}

			$j.ajax({
			url:'checkInvoiceDetails.jsp',
			type: "POST",
			async:false,
			data:"studyNumber="+$j("#studyNumber").val()+"&invNumber="+$j("#invNumber").val()+"&calledFrom=invoice_step1",
			success:(function (data){
				var response=$j(data);
				var message = response.filter('#resultMsg').val();				
				var invNumber = response.filter('#invNumber').val();
				if(message=="true"){
					alert(M_ExstngInvcNumberPlsModifyOrLeaveBlank);/*Existing invoice number. Please modify the invoice number or leave the number field blank and try again.*****/
					$j("#invNumber").focus();
					flag= false;
				}
				else{
					$j('#invNumbers').val(invNumber);
					flag= true;
				}
	
			}),
			error:function(response) { alert(data); return false; }
		});	
	}	
return 	flag;
}//checkValidInvc function closed

function validate(formobj) {

	//if (!(validate_col('<%=LC.L_Pyment_DueIn%>',formobj.paymentDueIn))) return false;
	
	//if(!(allSpclcharcheck(formobj.paymentDueIn.value ,  formobj.paymentDueIn)))return false;
	if(!(allSpclcharcheck(formobj.invNumber.value ,  formobj.invNumber)))return false;

    if(formobj.invNumber.value.indexOf("-")!= -1) {
    	/*alert("No Special Character( $ ) Allowed ");*****/
    	var paramArray = ["-"];
    	alert(getLocalizedMessageString("M_NoSplCharAllowed", paramArray));
    	formobj.invNumber.focus();
        return false;
        }
    if(formobj.invNumber.value.indexOf("+")!= -1) {
    	/*alert("No Special Character( $ ) Allowed ");*****/
    	var paramArray = ["+"];
    	alert(getLocalizedMessageString("M_NoSplCharAllowed", paramArray));
    	formobj.invNumber.focus();
        return false;
        }
	if(checkValidInvc()==false)return false;
    /*if(!(isInteger(formobj.paymentDueIn.value))){
		alert(M_PymntDueDtMustNum_ReEtr);
		formobj.paymentDueIn.focus();
		return false;
	};*/
	
	//if (!(checkGreaterThanZero(formobj.paymentDueIn.value ,  formobj.paymentDueIn, M_PymntDueDtMust_GtZero)))return false;

	if ((formobj.paymentDueIn.value != null && formobj.paymentDueIn.value !='') && (formobj.invDate.value != null && formobj.invDate.value !='')){
		if (CompareDates(formobj.invDate.value,formobj.paymentDueIn.value,'>')){
			alert ("Invoice Date should not be greater than Payment Due Date");/*alert ("Invoice Date should not be less than Payemnt Due Date");*****/
			formobj.paymentDueIn.focus();
			return false;
		}
	}
	
/* FIN-22382_0926012 Enhancement 31-Oct-2012 -Sudhir*/
	//if($j("#invNumber").val()!=''){

	//}
	if ( setMainDateFilter(formobj) == false ) 
	return false;
	
    //mode = formobj.mode.value;
	//04Apr2011 @Ankit #5771
	/*if (!(validate_col('Invoice Number',formobj.invNumber))) return false;@Ankit#5771*/
	
	//if (!(isIntegerAndNotBlank(formobj.paymentDueIn.value))){
		//alert('<%=MC.M_Etr_ValidNum%>');
		//formobj.paymentDueIn.focus();
		//return false;
	//}

	if (!(validate_col('<%=LC.L_Receivable_Status%>',formobj.milestoneReceivableStatus))) return false;
	if (!(validate_col('<%=LC.L_By_InvDate%>',formobj.invDate))) return false;
	
	 //added for date field validation
	 if (!(validate_date(formobj.invDate))) return false;
		 
//	if (!(validate_col('e-Signature',formobj.eSign))) return false;
//  if(isNaN(formobj.eSign.value) == true) {
//		alert("Incorrect e-Signature. Please enter again");
//		formobj.eSign.focus();
//		formobj.subVal.value="Submit";
//		return false;
//	   }

return true;
} 
</script>
<% String src= (String)request.getParameter("srcmenu"); %>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
  
<BODY>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div> 
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<%@page import="com.velos.esch.business.common.SchCodeDao"%>
<jsp:useBean id="invB" scope="request" class="com.velos.eres.web.invoice.InvoiceJB"/>
<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="studySiteB" scope="request" class="com.velos.eres.web.studySite.StudySiteJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>


<%
  HttpSession tSession = request.getSession(true); 
  String studyId = "";
  String mode = "";

 if (sessionmaint.isValidSession(tSession))
	{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
		//04Apr2011 @Ankit #5780 Desc:-This attribute is removed on updateinvoice.jsp
		session.setAttribute("flag","true");

		studyId = request.getParameter("studyId");
	
		studyB.setId(EJBUtil.stringToNum(studyId ));
		studyB.getStudyDetails();

		String selectedTab = request.getParameter("selectedTab");
//JM: added on 17Nov2006, 04Apr2011 @Ankit #5771

//String invNumber = invB.getInvoiceNumber();


	String acc = (String) tSession.getValue("accountId");
	int accId = 0;

	
		accId = EJBUtil.stringToNum(acc);	
	 String yrFilterDD = ""; String monthFilterDD = ""; String monthFilterYearDD = "";
	 String selMonth = ""; String selMonthYear = ""; String selYear="";
	 String defDateFilterDateFrom  = ""; String defDateFilterDateTo  = "";

	 
	
		yrFilterDD = DateUtil.getYearDropDown("yearFilter",selYear,false);
		monthFilterDD = DateUtil.getMonthDropDown("monthFilter",selMonth,false);
		monthFilterYearDD = DateUtil.getYearDropDown("monthFilterYear",selMonthYear,false);
		
		String defDateFilterType = "A";
		String userId = "";
		
		userId = (String) tSession.getValue("userId");
		
		///commented, will be implemented after the initial release
		/*StudySiteDao ssDao2 = new StudySiteDao();
		ssDao2	= studySiteB.getSitesForStatAndEnrolledPat(EJBUtil.stringToNum(studyId), EJBUtil.stringToNum(userId), accId);
		ArrayList arrSites = ssDao2.getSiteIds();
		ArrayList arrSiteNames = ssDao2.getSiteNames();
		String dSites = "";
		
		dSites = EJBUtil.createPullDownOrg("dSites",0,arrSites,arrSiteNames); */
	
		String dSites = "<input name=\"dSites\" value=\"0\" type = \"hidden\" >";
		
		SchCodeDao schDao = new SchCodeDao();
		schDao.getCodeValues("coverage_type");
		ArrayList covSubTypeList = schDao.getCSubType();
		ArrayList covDescList = schDao.getCDesc();
		ArrayList<String> covCodeHide = schDao.getCodeHide();
		StringBuffer sb1 = new StringBuffer();
		for (int iX=0; iX<covSubTypeList.size(); iX++) {
		    String covSub1 = ((String)covSubTypeList.get(iX)).replaceAll("'", "\\\\'");
		    String covDesc1 = ((String)covDescList.get(iX)).replaceAll("'", "\\\\'");
		    String covIsHidden = covCodeHide.get(iX);
		    if(StringUtil.isEmpty(covIsHidden))covIsHidden="N";
			if (covSub1 == null || covDesc1 == null) { continue; }
			if(covIsHidden.equalsIgnoreCase("Y")){
				sb1.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append(" ["+LC.L_Hidden+"]").append("<br/>");
			}else{
				sb1.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append("<br/>");
			}
		}
		String covTypeInfo = sb1.toString();
		
%>
<DIV  class="BrowserTopN" id="div1"> 
	<jsp:include page="milestonetabs.jsp" flush="true">
	<jsp:param name="studyId" value="<%=studyId%>"/>
	<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
	</jsp:include>
</DIV>

<DIV class="BrowserBotN BrowserBotN_M_2" id="div1"> 
<Form name="inv" id="invstep1" action="generateInvoice.jsp" method="post" onSubmit="if (validate(document.inv)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >
<P class="comments"><%=MC.M_EtrDets_ToCreateYourInv%><%--Enter the following details to create your Invoice*****--%>:</P>

<div style="padding: 2px; width:100%">
<table border="0" style="width: 100%">
<tbody>
<tr>
<td width="60%">
<table>
 <tr> 
 	<td class=tdDefault width="20%"><%=LC.L_Study_Number%><%--Study Number*****--%></td>
	 <td class=tdDefault>
		<%= studyB.getStudyNumber()%>
		<!-- FIN-22382_0926012 Enhancement 31-Oct-2012 -Sudhir-->
		<input type="hidden" id="studyNumber" name="studyNumber" value="<%= studyB.getStudyNumber()%>" />
	</td>
</tr> 
<!-- FIN-22585 Enhancement 11-Sep-2013 -Parminder Singh-->
<tr> 
 	<td class=tdDefault width="20%"><%=LC.CTRP_DraftNCTNumber%><%--Study Number*****--%></td>
	 <td class=tdDefault>
		<%= (studyB.getNctNumber()!=null)?studyB.getNctNumber():""%>
	</td>
</tr> 
 <!--<tr> 
 	<td class=tdDefault>Study Title</td>
	 <td class=tdDefault>
		<%= studyB.getStudyTitle()%>
	</td>
</tr> -->

 <tr>
 <!-- 04-04-2011 @Ankit #5771 -->
 	<td class=tdDefault><%=LC.L_Inv_Number%><%--Invoice Number*****--%> <!-- <FONT class="Mandatory">* </FONT> -->(<%=LC.L_Sys_GeneratedIfLeftBlank%><%--system-generated*****--%>)</td>
	 <td>
		<%= studyB.getStudyNumber()%>&nbsp;-&nbsp;<input type=text name="invNumber" id="invNumber" value="" size=10 maxlength=50 class="leftAlign" >
		<input type="hidden" name="invNumbers" id="invNumbers" value="" size=10 maxlength=50 class="leftAlign" >
	</td>
</tr> 
<tr>
	<td class=tdDefault><%=LC.L_Invoice_Date%><%--Invoice Date*****--%></td>
	<td><input type="text" name="invDate" class="datefield" size = 15 MAXLENGTH = 50></td>
</tr> 
<!--
	<tr> 
 	<td  class=tdDefault>Organization</td>
	<td>

	</td>
	 
</tr>  -->
	
		<%=dSites%>	
	
 <tr> 
 	<td class=tdDefault><%=LC.L_Payment_Due+" "+LC.L_Date%><%--Payment Due Date*****  <FONT class="Mandatory">* </FONT>--%></td>
	 <td>
		<input type=text name="paymentDueIn" class="datefield" size=15 maxlength=50>
		<%--<Select name="paymentDueUnit">
			<option value="D" SELECTED><%=LC.L_Day_S%>Day(s)*****</option>
			<option value="W" ><%=LC.L_Week_S%>Week(s)*****</option>
			<option value="M" ><%=LC.L_Month_S%>Month(s)*****</option>
			<option value="Y" ><%=LC.L_Year_S%>Year(s)*****</option>
		</Select>--%>
	 </td> 
 </tr>
	<tr>
	<td class=tdDefault ><%=LC.L_Invoice_To%><%--Invoice To*****--%></td>
	<td>
	<input type=hidden name=addressedTo value=''>
          <input type=text name=addressedToName size=30 value='' readonly>
          <A HREF=# onClick=openCommonUserSearchWindow("inv","addressedTo","addressedToName") ><%=LC.L_Select_User%><%--Select User*****--%></A> 
    </td>
	
	</tr>
	<tr>
	<td class=tdDefault><%=LC.L_Payment_To%><%--Payment To*****--%></td>
	<td>
	<input type=hidden name=sentFrom value=''>
          <input type=text name=sentFromName size=30 value='' readonly>
          <A HREF=# onClick=openCommonUserSearchWindow("inv","sentFrom","sentFromName") ><%=LC.L_Select_User%><%--Select User*****--%></A> 
    </td>
	</tr>
	</table>
	</td>
	<td width="40%"><table>
	<%=MC.M_InvoiceMilestonesBy%><%--Invoice Milestones by--%>&nbsp;
	<a href="javascript:void(0)" onmouseover="return overlib('<%=covTypeInfo%>',CAPTION,'<%=LC.L_Coverage_TypeLegend%><%-- Coverage Type Legend*****--%>');" onmouseout="return nd();"><%=MC.M_CoverageType%><%-- Coverage Type*****--%></a>
	<br/><%=schDao.toPullDownCodeListchckBox("coverage_type")%>
	</table>
	</td>
	</tr>
	</tbody>
	</table>
	</div>
	<!--JM: 13NOV06 -->
	
	<!--  FIN-22338
	
	<tr>
	<td class= tdDefault> <%=LC.L_Internal_Acc%><%--Internal Account*****--%> # </td>
	<td><input type ="text" name="intAccNum" size=15 maxlength =60></td>
	</tr>
	<tr>
	<td class=tdDefault><%=LC.L_Header%><%--Header*****--%></td>
	<td>
		<Textarea name=invHeader rows=2 cols=75></textarea>
    </td>
	</tr>
	<tr>
		<td class=tdDefault><%=LC.L_Footer%><%--Footer*****--%></td>
	<td>
		<Textarea name=invFooter rows=2 cols=75></textarea>
    </td>
	</tr>
	
	-->	
	<div style="padding: 2px; width:100%">
	<table border="0" style="width: 100%">
	<tbody>
	<tr>
	<td width="100%">
	<table>
	<tr>
	
	<td  class=tdDefault style="width: 19.3%"><%=LC.L_Note%><%--Note*****--%></td>
	<td >	
		<textarea name=invNotes rows=2 cols=75  onfocus="if(this.value == '<%=MC.M_Prmpt_Pymnt_Appr%>') { this.value = ''; }" 
		onblur="if(this.value == '') {this.value = '<%=MC.M_Prmpt_Pymnt_Appr%>'}"><%=MC.M_Prmpt_Pymnt_Appr%></textarea>
    </td>
	</tr>
<tr><td colspan="2">
	<table width="100%">
	<tr>
	<td class=tdDefault style="width: 20.3%"><%=LC.L_Display%><%--Display*****--%>  <!-- <FONT class="Mandatory">* </FONT> --></td>
		<td width="20%">
		<Select name="milestoneReceivableStatus">
			<option value="A" selected="selected"><%=LC.L_All_Receivables%><%--All Receivables*****--%></option>
			<!--<option value="OR" >Outstanding Receivables</option>-->
			<option value="UI" ><%=LC.L_Uninv_Receivables%><%--Un-invoiced Receivables*****--%></option>
			<!--<option value=""><%//=LC.L_Select_AnOption%><%--Select an Option*****--%></option>-->
			<option value="IN" ><%=LC.L_Invoiceables%><%--Invoiceables*****--%></option>
		</Select>
	</td>
	
		<td class=tdDefault width="15%" align="center"><%=LC.L_For_Dates%><%--For Dates*****--%>:</td>
		<td class=tdDefault width="50%"><input type = "radio" name="dateRangeType" value="A" 
		<%if(StringUtil.isEmpty(defDateFilterType) || defDateFilterType.equals("A")) { %> CHECKED <% } %> ><i><%=LC.L_All%><%--All*****--%></i>
		
		&nbsp;&nbsp;&nbsp;<input type = "radio" name="dateRangeType" value="Y"
		<% if( defDateFilterType.equals("Y")) { %> CHECKED <% } %> ><i> <%=LC.L_Year%><%--Year*****--%> </i><%=yrFilterDD%>
		
		&nbsp;&nbsp;&nbsp;<input type = "radio" name="dateRangeType" value="M"
			<% if( defDateFilterType.equals("M")) { %> CHECKED <% } %> > <i><%=LC.L_Month%><%--Month*****--%> </i><%=monthFilterDD%>&nbsp;<%=monthFilterYearDD%>

		<BR><BR><input type = "radio" name="dateRangeType" value="DR"
			<% if( defDateFilterType.equals("DR")) { %> CHECKED <% } %>	><i> <%=LC.L_DateRange_Frm%><%--Date Range: From*****--%></i>
<%-- INF-20084 Datepicker-- AGodara --%>		
			<Input type=text name="drDateFrom" class="datefield" READONLY SIZE=7 
				<% if( defDateFilterType.equals("DR")) { %> value = <%=defDateFilterDateFrom%>  <% } %>> 
				
			 &nbsp;<i><%=LC.L_To%><%--To*****--%> </i>&nbsp;
			<Input type=text name="drDateTo" class="datefield" READONLY SIZE=7
				<% if( defDateFilterType.equals("DR")) { %> value = <%=defDateFilterDateTo %>  <% } %> > 
		
		</td>
	</tr>
	</table>
</td>
</tr>		

<!--Added by Manimaran for Enh.#FIN12 --Requirement Changed-->
<!-- tr><td class= tdDefault> <input type="checkbox" name="includeHeader" value=""> Include Account Specific Header </td> </tr-->
<!--  tr><td class= tdDefault> <input type="checkbox" name="includeFooter" value=""> Include Account Specific Footer </td> </tr-->

<Input type="hidden" name="dtFilterDateFrom" SIZE=10 value=<%=defDateFilterDateFrom%>> 
<Input type="hidden" name="dtFilterDateTo" SIZE=10 value=<%=defDateFilterDateTo%>> 
 

<INPUT id="mode" name="mode" TYPE="hidden" value="<%=mode%>"/> 
<INPUT id="studyId" name="studyId" TYPE="hidden"  value="<%=studyId%>"/> 
<INPUT id="selectedTab" name="selectedTab" TYPE="hidden" value="<%=selectedTab%>"/>
<INPUT id="srcmenu" name="srcmenu" TYPE="hidden" value="<%=src%>"/>
</table>
</td>
</tr>
</tbody>

</TABLE>

<table><tr><td><FONT class="Mandatory"><%//=MC.M_InvItem_MstoneRule%><%--Note: To ensure inclusion of partially invoiced line items using the 'Un-invoiced Receivables' option, please ensure that these Milestone Rules do not have a Count greater than 1 AND always select the 'Detail View' option when including them in your invoice*****--%></FONT>
</td></tr></table>
<!-- Please select option 'All Receivables' if you have compound Milestones with a value of > 1 in 'count' field OR if you have selected 'High Level' option for the milestone previously. -->

</table>
</div>
 

<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="N"/>
		<jsp:param name="formID" value="invstep1"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>


   <input type="hidden" name="subVal" value="Sumbit">
</Form>

<%
	}
 else{%>
 <jsp:include page="timeout.html" flush="true"/> 
 <%}%>

 <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</DIV>
<div class ="mainMenu" id="emenu"> 
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</BODY>


