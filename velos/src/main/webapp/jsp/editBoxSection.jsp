<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="jqueryUtils.jsp"></jsp:include>
<jsp:include page="velos_includes.jsp"></jsp:include>
<jsp:include page="localization.jsp" flush="true" />
<html>
<head>
<title><%=LC.L_Fld_EditBox%><%--Field Edit Box*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*"%><%@page
	import="com.velos.eres.service.util.*"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="include.jsp" flush="true" />

</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>
<%--For Bug# 8205:Yogendra--%>
<jsp:include page="popupPanel.jsp" flush="true" />
<div id="overDiv"
	style="position: absolute; visibility: hidden; z-index: 1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<!-- <script type="text/javascript" src="./FCKeditor/fckeditor.js"></script> -->

<script type="text/javascript">
	checkQuote="N";
	var fck;
	var oFCKeditor;

   function init()
      {
       
      /*oFCKeditor = new FCKeditor("nameta") ;
      oFCKeditor.BasePath = './FCKeditor/';
      oFCKeditor.Width = editorWidth ;
      oFCKeditor.Height = editorHeight ; // 400 pixels
      oFCKeditor.ToolbarSet="Velos";
      //oFCKeditor.Config['SkinPath'] = '/FCKeditor/editor/skins/office2003/' ;
      oFCKeditor.ReplaceTextarea() ;*/
      if (document.editBoxField.section.type!="hidden")
      document.editBoxField.section.focus();
      else 
      document.editBoxField.sequence.focus();
      
      }
      function processFormat(formobj,mode)
      {
       
      	if (mode=="D")
	{
	 if (formobj.nameta.value.length>0)
	 formobj.nameta.value="[VELDISABLE]"+formobj.nameta.value;
	 //showHide("eformat","S");
	 //showHide("dformat","H");
	 document.poplayer=document.getElementById("dformat");
	 document.poplayer.innerHTML="<A href='javascript:void(0)' onClick='processFormat(document.editBoxField,\"E\")'><img src=\"./images/enableformat.gif\" alt=\"<%=LC.L_Enable_Formatting%><%--Enable Formatting*****--%>\" border = 0 align=absbotton></A>";
	 
	} else if (mode=="R")
	{
	   if (confirm("<%=MC.M_RemFmt_ForFldName%>"))/*if (confirm("Remove formatting for field name?"))*****/
	   {
	    formobj.nameta.value="";
	    
	   }
	} else if(mode=="E")
	{
	 formobj.nameta.value=replaceSubstring(formobj.nameta.value,"[VELDISABLE]","");
	 //showHide("eformat","H");
	 //showHide("dformat","S");
	 document.poplayer=document.getElementById("dformat");
	 document.poplayer.innerHTML="<A href='javascript:void(0)' onClick='processFormat(document.editBoxField,\"D\")'><img src=\"./images/disableformat.gif\" alt=\"<%=LC.L_Disable_Formatting%><%--Disable Formatting*****--%>\" border = 0 align=absbotton></A>";
	}
	
      }


</script>
<script type="text/javascript">
   overRideChar("#");
   overRideChar("&");
   overRideFld("name");
   var editor;
  function FCKeditor_OnComplete(oFCKeditor)
{
	 fck= FCKeditorAPI.GetInstance('nameta');
	 
}

  </script>


<SCRIPT Language="javascript1.1">
function findCheckBox(formobj)
{
	init();
	
	for(i=0;i<formobj.editBoxType.length;i++)
	{
		if(formobj.editBoxType[i].checked)
		{	
			if(formobj.editBoxType[i].value=="ET")
			{	
				formobj.lines.readOnly=false;
				formobj.lengthOf.readOnly=false;
				formobj.characters.readOnly=false;
				formobj.defResponse1.readOnly=false;
				
				formobj.greaterless1.disabled=true;
				formobj.andor.disabled=true;
				formobj.greaterless2.disabled=true;
				
				formobj.val1.readOnly=true;
				formobj.val2.readOnly=true;	 
				formobj.numformat.disabled=true;
				formobj.overRideFormat.disabled=true;			
			    formobj.overRideRange.disabled=true;
			
				formobj.datefld.disabled=true;
				formobj.datefld.checked=false;
				formobj.defaultdate.disabled=true;
				formobj.defaultdate.checked=false;
				formobj.overRideDate.disabled = true;
				formobj.overRideDate.checked = false;
			}
			else if(formobj.editBoxType[i].value=="EN")
			{	
				formobj.lines.readOnly=true;
				formobj.lengthOf.readOnly=false;
				formobj.characters.readOnly=false;
				formobj.defResponse1.readOnly=false;
				
				formobj.greaterless1.disabled=false;
				formobj.andor.disabled=false;
				formobj.greaterless2.disabled=false;
				formobj.val1.readOnly=false;
				formobj.val2.readOnly=false;	 
				formobj.numformat.disabled=false;
				formobj.overRideFormat.disabled=false;			
			    formobj.overRideRange.disabled=false;
				formobj.datefld.disabled=true;
				formobj.datefld.checked=false;
				
				formobj.defaultdate.disabled=true;
				formobj.defaultdate.checked=false;
				
				formobj.overRideDate.disabled = true;
				formobj.overRideDate.checked = false;
			}
			else if(formobj.editBoxType[i].value=="ED")
			{	
				formobj.lines.readOnly=true;
				formobj.lengthOf.readOnly=true;
				formobj.characters.readOnly=true;
				formobj.defResponse1.readOnly=true;
				
				formobj.greaterless1.disabled=true;
				formobj.andor.disabled=true;
				formobj.greaterless2.disabled=true;
				formobj.val1.readOnly=true;
				formobj.val2.readOnly=true;	 
				
				formobj.numformat.disabled=true;
				
				formobj.overRideFormat.disabled=true;			
			    formobj.overRideRange.disabled=true;
				formobj.datefld.disabled=false;
				formobj.defaultdate.disabled=false;
				
				formobj.overRideDate.disabled = false;
			
			}
		}
	}
	
}

 function  validate(formobj)
 {
	 if(formobj.uniqueId.value == "er_def_date_01")
	 {		
		 
	 }
	 else{
		 
	 if (   !(validate_col_spl ('  Field Uniqueid', formobj.uniqueId)  )  )  return false
	 }
	
	 if ((fnTrimSpaces((formobj.uniqueId.value).toLowerCase())=='er_def_date_01') && (formobj.prevfldid.value!='er_def_date_01'))
	{
	  alert("<%=MC.M_ErDefDt01_SysKwrdFld%>");/*alert("'er_def_date_01' is a system reserved keyword. Please specify another Field Id.");*****/
	  formobj.uniqueId.focus();
	  return false;
	}
	
	if (   !(validate_col ('  Section', formobj.section)  )  )  return false
	if (   !(validate_col ('  Sequence', formobj.sequence)  )  )  return false
	
	var valFlag = allfldquotecheck(formobj);
	if(valFlag!=undefined && !valFlag==true){
		return false;
	}
	
	
	//replace special chracters with html codes
	
	
	
		
	//end replacing
	
       /*if (   !(validate_col_spl ('  Field Display Name', formobj.name)  )  )
	{
	 formobj.name.focus();
	 return false
	}*/
	
	if (!(validate_col('Field Display Name',formobj.name)))  return false;
	 	 
	 
	/* if (checkChar(formobj.name.value,"'"))
	 {
	    alert("Special Character(') not allowed for this Field");
	    formobj.name.focus();
	    return false;
	 }*/
	
		
	if ((formobj.name.value.length)>2000)
	{
	  alert("<%=MC.M_FldDispName2000_CrtCont%>");/* alert("'Field Display Name' length exceeds the maximum allowed limit of 2000 characters.\n Please correct to continue.");*****/
	  formobj.name.focus();
	  return false;
	}
	
	
	/*if (formobj.nameta.value.length>0)
	formobj.nameta.value=htmlEncode(formobj.nameta.value);*/
	
	
	
	// @Gyanendra commented for bug #4421 if(!( validateDataSize(formobj.instructions,1000,'Field Help'))) return false
		
	if (! (splcharcheck(formobj.keyword.value)))
	{
		  formobj.keyword.focus();
		  return false;
	}
	
	if (! (checkquote(formobj.instructions.value)))
	{
		  formobj.instructions.focus();
		  return false;
	}
	
	if (! (splcharcheckForXSL(formobj.instructions.value)))
	{
		  formobj.instructions.focus();
		  return false;
	}

	if(!(formobj.mandatory.checked) && formobj.overRideMandatory.checked)
	{
	  alert("<%=MC.M_ChkMndtFldFirst_ToOverride%>");/*alert("Please check Mandatory field first which needs to be overridden");*****/
	  return false;	
	}
	
	if(formobj.overRideMandatory.checked)
	{	
		formobj.overRideMandatory.value="1";
		}			
	else {
		formobj.overRideMandatory.value="0";
		}
	
	

	if(formobj.editBoxType[2].checked)
	 {
	 
	  if((formobj.overRideDate.checked)	&& !(formobj.datefld.checked)){
	  alert("<%=MC.M_PlsChkDtFld_Overridden%>");/*alert("Please check the Date Field first which needs to be overridden");*****/
	  return false;
	  } 
		if(formobj.datefld.checked)
		 {
		   formobj.hdndatefld.value= "1";
		 }
		 else {
		   formobj.hdndatefld.value= "0";
		 }
		 
		 if(formobj.overRideDate.checked)	
		formobj.hdnOverRideDate.value="1";		
		else
		formobj.hdnOverRideDate.value="0";
		
		if(formobj.defaultdate.checked)
		 {
		   formobj.hdndefaultdate.value= "1";
		 }
		 else {
		   formobj.hdndefaultdate.value= "0";
		 }
	 
	 }

	if (   !(validateNumFormat(formobj.numformat) ) ) return false
	
	if(formobj.numformat.value== "" && formobj.overRideFormat.checked)
	{
	  alert("<%=MC.M_EtrNumFldFrmt_Overridden%>");/*alert("Please enter Number Field Format first which needs to be overridden");*****/
	  return false;
	}


	 if(formobj.overRideFormat.checked)	
			formobj.hdnOverRideFormat.value="1";		
		else
			formobj.hdnOverRideFormat.value="0";
		
		if(formobj.overRideRange.checked)	
			formobj.hdnOverRideRange.value="1";		
		else
			formobj.hdnOverRideRange.value="0";
			
		
			
		
	if(formobj.val1.value!="" && formobj.val2.value!="" && formobj.editBoxType[2].checked)
	 {
			formobj.greaterless1.disabled=false;
			formobj.andor.disabled=false;
			formobj.greaterless2.disabled=false;

	 }
         
	if(formobj.val1.value!="" && formobj.greaterless1.value=="")
	 {
		 
		 alert("<%=MC.M_Selc_AnOpt%>.");/*alert("Please select an option");*****/
		 formobj.greaterless1.focus();
		 return false;
	 }
	 if(formobj.val2.value!="" && formobj.greaterless2.value=="")
	 {	
		 alert("<%=MC.M_Selc_AnOpt%>");/*alert("Please select an option");*****/
		 formobj.greaterless2.focus();
		 return false;
	 }

	if(formobj.val1.value=="" && formobj.greaterless1.value!="")
	 {	
		 alert("<%=MC.M_PlsEnterValue%>");/*alert("Please enter Value");*****/
		 formobj.val1.focus();
		 return false;
	 }
	 if(formobj.val2.value=="" && formobj.greaterless2.value!="")
	 {	
		 alert("<%=MC.M_PlsEnterValue%>");/*alert("Please enter Value");*****/
		 formobj.val2.focus();
		 return false;
	 }

	  if(formobj.val1.value!="" && formobj.greaterless1.value!="" && formobj.val2.value!="" && formobj.greaterless2.value!="" && formobj.andor.value=="")
	 {	
		  alert("<%=MC.M_Selc_AnOpt%>");/*alert("Please select an option");*****/
		 formobj.andor.focus();
		 return false;
	 }
	 if(formobj.andor.value!="")
	 {	 
		if(formobj.greaterless1.value=="")
		 {
			alert("<%=MC.M_Selc_AnOpt%>");/*alert("Please select an option");*****/
			formobj.greaterless1.focus();
			return false;
		 }
		 if(formobj.greaterless2.value=="")
		 {
			 alert("<%=MC.M_Selc_AnOpt%>");/*alert("Please select an option");*****/
			 formobj.greaterless2.focus();
			 return false;
		 }
		 if(formobj.val1.value=="")
		 {
			 alert("<%=MC.M_PlsEnterValue%>");/*alert("Please enter Value");*****/
			 formobj.val1.focus();
			 return false;
		 }
		 if(formobj.val2.value=="")
		 {
			 alert("<%=MC.M_PlsEnterValue%>");/*alert("Please enter Value");*****/
			formobj.val2.focus();
			return false;
		 }
	 }

if(formobj.val1.value =="" && formobj.val2.value =="" && formobj.editBoxType[1].checked && (formobj.overRideRange.checked))
	{
	  alert("<%=MC.M_PlsEtrValues_Overridden%>");/*alert("Please enter values in Response first which needs to be overridden");*****/
	  return false;
	
	} 		
	/*if(formobj.val1.value!="" || formobj.val2.value!="")
	 { 


		 if(formobj.greaterless1.value=="")
		 {
			 alert("Please select an option");
			 formobj.greaterless1.focus();
			 return false;
		 }
		 if(formobj.andor.value=="")
		 {
			 alert("Please select an option");
			 formobj.andor.focus();
			 return false;
		 }
		 if(formobj.greaterless2.value=="")
		 {
			 alert("Please select an option");
			 formobj.greaterless2.focus();
			 return false;
		 }
		 if( formobj.val1.value=="")
		 {
			 alert("Please enter Value");
			 formobj.val1.focus();
			 return false;
		 }
		if(formobj.val2.value=="")
		 {
			 alert("Please enter Value");
			 formobj.val2.focus();
			 return false;
		 }

	 }*/
	 
	 // options 'hide display label' and 'field alignment top can not be selected together
	 //KM-to fix Bug 2284 
	 if (formobj.expLabel.value!=1){
             if(formobj.hideLabel.checked && formobj.align[3].checked)
	     {	
		 alert("<%=MC.M_HideFldLabel_CntSetAlign%>");/*alert("When option 'Hide Field Label' is selected, you can not set 'Align (Field Display Name)' to 'Top'. Please choose one of these options.");*****/
		 formobj.hideLabel.focus();
		 return false;
	 }
      }	 

	 	if(formobj.codeStatus.value != "WIP"){	
	 	 	if (  !(validate_col('e-Signature',formobj.eSign)     )   ) return false
	 		 
// 	 		if(isNaN(formobj.eSign.value) == true) 
// 	 			{
<%-- 	 			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/ --%>
// 	 			formobj.eSign.focus();
// 	 			 return false;
// 	 	   		}
	 	 	}
	
	if(isNaN(formobj.sequence.value) == true) 
	{
		alert("<%=MC.M_SecSeqHasToNum_ReEtr%>");/*alert("Section Sequence has to be a number. Please enter again.");*****/
		formobj.sequence.focus();
		 return false;
   	}

	if(isNaN(formobj.labelDisplayWidth.value) == true) 
      {
        alert("<%=MC.M_LabelDispWidth_HasToNum%>");/*alert("Label Display Width has to be a number. Please enter a valid number.");*****/
        formobj.labelDisplayWidth.focus();
        return false;
       }    

	if(isNaN(formobj.val1.value) == true) 
	{
		alert("<%=MC.M_NumValues_ForResps%>");/*alert("Enter only numeric values for responses.");*****/
		formobj.val1.focus();
		 return false;
   	}

	if(isNaN(formobj.val2.value) == true) 
	{
		alert("<%=MC.M_NumValues_ForResps%>");/*alert("Enter only numeric values for responses.");*****/
		formobj.val2.focus();
		 return false;
   	}
	if ( formobj.editBoxType[0].checked)   
	{
		
		if(   (isNaN(formobj.lengthOf.value)== true)    )
		{
			alert("<%=MC.M_LthFld_EtrValid%>");/*alert("The length field takes a Number input. Please enter a valid Number.");*****/
			formobj.lengthOf.focus();
			 return false;
			
		}   
		else if     (     (isNaN(formobj.lines.value)== true)  )
		{
			alert("<%=MC.M_LineNumInput_EtrValid%>");/*alert("The lines field takes a Number input.Please enter a valid Number.");*****/
			formobj.lines.focus();
			  return false;
			
		}
			
		else if (      (isNaN(formobj.characters.value)== true)   )
		{  
			alert("<%=MC.M_CharsFld_NumInput%>");/*alert("The characters field takes a Number input.Please enter a valid Number.");*****/
		 	 formobj.characters.focus();
			  return false;
			
		}

		

	}
	
	
	if    (   ( formobj.editBoxType[1].checked  )    )     
	{
			if(   (isNaN(formobj.lengthOf.value)== true)    )
			{
				alert("<%=MC.M_LthFld_EtrValid%>");/*alert("The length field takes a Number input. Please enter a valid Number");*****/
				formobj.lengthOf.focus();
				 return false;
				
			}   	
			if  ( isNaN(formobj.characters.value)== true)
			{
				alert("<%=MC.M_CharFld_EtrValid%>");/*alert("This characters field takes a number input.Please enter a valid Number.");*****/
				formobj.characters.focus();
				return false;
				
			}
			
			if  ( isNaN(formobj.defResponse1.value)== true)
			{
				alert("<%=MC.M_RespNumFld_CntText%>");/*alert("This default response of number input field cannot be text.Please enter a valid Number.");*****/
				formobj.defResponse1.focus();
				return false;
				
			}
			
			/*if  ( isNaN(formobj.lines.value)== true)
			{
				alert("This lines field takes a number input.Please enter a valid Number.");
				formobj.lines.focus();
				return false;
				
			}*/
			
	}  
	
	return true;
	
	
  }
  
  //Function to validate the radio button input boxes
    function disablingField(formobj,val, flag)
  {
  		
	if ( flag == "C" )
	{		
			
	  	if  (( val) =="EN"  )
		{
			formobj.lines.readOnly=true;
			formobj.lengthOf.readOnly=false;
			formobj.characters.readOnly=false;
			formobj.defResponse1.readOnly=false;
			formobj.defResponse1.value="";
			formobj.lines.value="";
			formobj.lengthOf.value="20";
			formobj.characters.value="100";
			
			formobj.greaterless1.disabled=false;
			formobj.andor.disabled=false;
			formobj.greaterless2.disabled=false;
			formobj.val1.readOnly=false;
			formobj.val2.readOnly=false;
			formobj.numformat.disabled=false;
			formobj.overRideFormat.disabled=false;			
			formobj.overRideRange.disabled=false;
			formobj.datefld.disabled=true;
			formobj.datefld.checked=false;
			formobj.defaultdate.disabled=true;
			formobj.defaultdate.checked=false;
     		formobj.overRideDate.disabled = true;
			formobj.overRideDate.checked = false;
		}
	
		if (val=="ED") 
		{
			
			//formobj.defResponse2.readOnly=false;
		    formobj.lines.readOnly=true;
			formobj.lengthOf.readOnly=true;
			formobj.characters.readOnly=true;
			formobj.defResponse1.readOnly=true;
		
			formobj.lines.value="";
			formobj.lengthOf.value="";
			formobj.characters.value="";
			formobj.defResponse1.value="";
		
			
			formobj.greaterless1.disabled=true;
			formobj.andor.disabled=true;
			formobj.greaterless2.disabled=true;
			formobj.greaterless1.value="";
			formobj.greaterless2.value="";
			formobj.val1.value="";
			formobj.andor.value="";
			formobj.val2.value="";
			formobj.val1.readOnly=true;
			formobj.val2.readOnly=true;	 
			formobj.numformat.value="";
			formobj.numformat.disabled=true;
			formobj.overRideFormat.disabled=true;			
			formobj.overRideRange.disabled=true;
			formobj.overRideFormat.checked=false;			
			formobj.overRideRange.checked=false;
			formobj.datefld.disabled=false;
			formobj.defaultdate.disabled=false;
			
			formobj.overRideDate.disabled = false;


			


		 
		}   


	
		if  (val =="ET"  )
		{
				
			formobj.lines.readOnly=false;
			formobj.lengthOf.readOnly=false;
			formobj.characters.readOnly=false;
			formobj.defResponse1.readOnly=false;
			formobj.lines.value="1";
			formobj.lengthOf.value="20";
			formobj.characters.value="100";

			formobj.greaterless1.disabled=true;
			formobj.andor.disabled=true;
			formobj.greaterless2.disabled=true;
			formobj.greaterless1.value="";
			formobj.greaterless2.value="";
			formobj.val1.value="";
			formobj.andor.value="";
			formobj.val2.value="";
			formobj.val1.readOnly=true;
			formobj.val2.readOnly=true;
			formobj.numformat.value="";
			formobj.numformat.disabled=true;	
			formobj.overRideFormat.disabled=true;			
			formobj.overRideRange.disabled=true;
			formobj.overRideFormat.checked=false;			
			formobj.overRideRange.checked=false;	
			formobj.datefld.disabled=true;
            formobj.datefld.checked=false;
	    
	    formobj.defaultdate.disabled=true;
            formobj.defaultdate.checked=false;
			
			formobj.overRideDate.disabled = true;
			formobj.overRideDate.checked = false;
			
		}

	}	
	
	if ( flag == "M")
	{
			  	if  (( val) =="EN"  )
		{
			formobj.lines.readOnly=true;
			formobj.lengthOf.readOnly=false;
			formobj.characters.readOnly=false;
			formobj.defResponse1.readOnly=false;
			formobj.lines.value="";

			
			formobj.greaterless1.disabled=false;
			formobj.andor.disabled=false;
			formobj.greaterless2.disabled=false;
			formobj.val1.readOnly=false;
			formobj.val2.readOnly=false;	 
			formobj.numformat.disabled=false;
			formobj.overRideFormat.disabled=false;			
			formobj.overRideRange.disabled=false;
			
			formobj.datefld.disabled=true;
			formobj.datefld.checked=false;
			
			formobj.defaultdate.disabled=true;
			formobj.defaultdate.checked=false;
			
			formobj.overRideDate.disabled = true;
			formobj.overRideDate.checked = false;
			formobj.overRideRange.disabled=false;

		}
	
		if (val=="ED") 
		{
				
			//formobj.defResponse2.readOnly=false;
		
			formobj.lines.readOnly=true;
			formobj.lengthOf.readOnly=true;
			formobj.characters.readOnly=true;
			formobj.defResponse1.readOnly=true;
			formobj.lines.value="";
			formobj.lengthOf.value="";
			formobj.characters.value="";
			formobj.defResponse1.value="";

			
			formobj.greaterless1.disabled=true;
			formobj.andor.disabled=true;
			formobj.greaterless2.disabled=true;
			formobj.greaterless1.value="";
			formobj.andor.value="";
			formobj.greaterless2.value="";
			formobj.val1.value="";
			formobj.val2.value="";
			formobj.val1.readOnly=true;
			formobj.val2.readOnly=true;	 
			formobj.numformat.value="";
			formobj.numformat.disabled=true;
			formobj.overRideFormat.disabled=true;			
			formobj.overRideRange.disabled=true;
			formobj.overRideFormat.checked=false;			
			formobj.overRideRange.checked=false;
			formobj.datefld.disabled=false;
			formobj.defaultdate.disabled=false;
			formobj.overRideDate.disabled = false;

			
	

		
		}   
	
		if  (val =="ET"  )
		{
					
		
			formobj.lines.readOnly=false;
			formobj.lengthOf.readOnly=false;
			formobj.characters.readOnly=false;
			formobj.defResponse1.readOnly=false;

			
			formobj.greaterless1.disabled=true;
			formobj.andor.disabled=true;
			formobj.greaterless2.disabled=true;
			formobj.greaterless1.value="";
			formobj.andor.value="";
			formobj.greaterless2.value="";
			formobj.val1.value="";
			formobj.val2.value="";
			formobj.val1.readOnly=true;
			formobj.val2.readOnly=true;
			formobj.numformat.value="";
			formobj.numformat.disabled=true;	
			formobj.overRideFormat.disabled=true;			
			formobj.overRideRange.disabled=true;
			formobj.overRideFormat.checked=false;			
			formobj.overRideRange.checked=false;
			formobj.datefld.disabled=true;
			formobj.datefld.checked=false;
			formobj.defaultdate.disabled=true;
			formobj.defaultdate.checked=false;
			//formobj.overRideRange.disabled=true;
			//formobj.overRideRange.checked=false;
			//formobj.overRideRange.disabled=false;
			formobj.overRideDate.disabled = true;
			formobj.overRideDate.checked = false;

		}

	
	}
	
	
}

 // BK:#4960 TO MAKE ENTER KEY WORKING FOR FORM SUBMISSION	
    function fKeyUp(e)
    {
    	if (document.getElementById("submit_btn")==null){
    		return false;
    	}  
       var KeyID = (window.event) ? event.keyCode : e.keyCode;

       switch(KeyID)

       {
      
           case 13:
          	 if(!document.editBoxField.instructions.hasFocus) {        	
  	        	 if (validate(document.editBoxField)== false) {        		 
  	            	 setValidateFlag('false'); return false; 
  	            } 
  	             else {             
  	                  setValidateFlag('true'); 
  	                document.forms["editBoxField"].submit();
  	                return true;               
  	            }
          	 }    
               
          break;
       }
    }

    /***** @Gyanendra update for Bug#20544 *****/
    
    var textEventLimitFunction = {		
    		fixTextAreas:{}
    	};

         textEventLimitFunction.fixTextAreas = function (frm) {
    	
    	limitChars('fldinst', 2000, 'countfldinst');
    	
    	$j("#fldinst").keyup(function(){
    		var divId = 'count'+this.id;
    		limitChars(this.id, 2000, divId);
    	});	
    };

    $j(document).ready(function(){	
    	
    	textEventLimitFunction.fixTextAreas();
    		
    });

    /***** End update @Gyanendra *****/

</SCRIPT>


<jsp:useBean id="sessionmaint" scope="session"
	class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="fieldLibJB" scope="request"
	class="com.velos.eres.web.fieldLib.FieldLibJB" />
<jsp:useBean id="catLibJB" scope="request"
	class="com.velos.eres.web.catLib.CatLibJB" />
<jsp:useBean id="formSecJB" scope="request"
	class="com.velos.eres.web.formSec.FormSecJB" />
<jsp:useBean id="formFieldJB" scope="request"
	class="com.velos.eres.web.formField.FormFieldJB" />
<jsp:useBean id="fldVldJB" scope="request"
	class="com.velos.eres.web.fldValidate.FldValidateJB" />

<%@ page language="java"
	import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.fldValidate.impl.FldValidateBean,com.velos.eres.web.studyRights.StudyRightsJB"%>

<%
	int ienet = 2;
	String agent1 = request.getHeader("USER-AGENT");
	if (agent1 != null && agent1.indexOf("MSIE") != -1)
		ienet = 0; //IE
	else
		ienet = 1;

	if (ienet == 0) {
%>
<body style="overflow: scroll"
	onLoad="findCheckBox(document.editBoxField);">
<%
	} else {
%>
<body onLoad="findCheckBox(document.editBoxField);">
<%
	}
%>
<DIV class="popDefault" id="div1">
<%
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession)) {
%> <jsp:include page="sessionlogging.jsp" flush="true" /> <jsp:include
	page="include.jsp" flush="true" /> <%
 	int pageRight = 0;
 		GrpRightsJB grpRights = (GrpRightsJB) tSession
 				.getValue("GRights");
 		String calledFrom = request.getParameter("calledFrom");
 		String lnkFrom = request.getParameter("lnkFrom");
		String codeStatus = request.getParameter("codeStatus");
		System.out.println("codeStatus Fetched Value is : " + codeStatus);
 		if (lnkFrom == null) {
 			lnkFrom = "-";
 		}

 		String stdId = request.getParameter("studyId");
 		int studyId = EJBUtil.stringToNum(stdId);

 		String userIdFromSession = (String) tSession.getValue("userId");

 		if (calledFrom.equals("A")) { //from account form management
 			if (lnkFrom.equals("S")) {
 				ArrayList tId;
 				tId = new ArrayList();
 				if (studyId > 0) {
 					TeamDao teamDao = new TeamDao();
 					teamDao.getTeamRights(studyId, EJBUtil
 							.stringToNum(userIdFromSession));
 					tId = teamDao.getTeamIds();
 					if (tId.size() <= 0) {
 						pageRight = 0;
 					} else {
 						StudyRightsJB stdRights = new StudyRightsJB();
 						stdRights.setId(EJBUtil.stringToNum(tId.get(0)
 								.toString()));

 						ArrayList teamRights;
 						teamRights = new ArrayList();
 						teamRights = teamDao.getTeamRights();

 						stdRights
 								.setSuperRightsStringForStudy((String) teamRights
 										.get(0));
 						stdRights.loadStudyRights();

 						if ((stdRights.getFtrRights().size()) == 0) {
 							pageRight = 0;
 						} else {
 							pageRight = Integer
 									.parseInt(stdRights
 											.getFtrRightsByValue("STUDYFRMMANAG"));
 						}
 					}
 				}
 			} else {
 				pageRight = Integer.parseInt(grpRights
 						.getFtrRightsByValue("MACCFRMS"));
 			}
 		}

 		if (calledFrom.equals("L")) //form library
 		{
 			pageRight = Integer.parseInt(grpRights
 					.getFtrRightsByValue("MFRMLIB"));
 		}

 		if (calledFrom.equals("St")) //study
 		{
 			StudyRightsJB stdRights = (StudyRightsJB) tSession
 					.getValue("studyRights");
 			pageRight = Integer.parseInt(stdRights
 					.getFtrRightsByValue("STUDYFRMMANAG"));
 		}

 		if (pageRight >= 4) {

 			String mode = request.getParameter("mode");

 			String fieldLibId = "";
 			//String fldCategory="";
 			String fldName = "";
 			String fldNameFormat = "";
 			String formatDisable = "N";
 			String fldUniqId = "";
 			String fldKeyword = "";
 			String fldInstructions = "";
 			String fldDesc = "";
 			String fldType = "";
 			String fldDataType = "ET";
 			String dateCheck = "";
 			String numformat = "";

 			// THE STYLE OBJECT WHICH TAKES VALUES OF ATTRIBUTES OF THE FIELD  
 			// LIKE BOLD, SAMELINE, ALIGN , ITALICS
 			Style aStyle = new Style();
 			String formId = "";
 			String formSecId = "";
 			String fldMandatory = "";
 			String fldSeq = "";
 			String fldAlign = "";
 			String samLinePrevFld = "";
 			String fldBold = "";
 			String fldItalics = "";
 			String formFldId = "";
 			String mandatoryChk = "0";
 			String formatChk = "0";
 			String dateChk = "0", defaultDateCheck = "0";
 			String rangeChk = "0";

 			String fldLength = "20";
 			String fldLinesNo = "1";
 			String fldCharsNo = "100";
 			String fldDefResp1 = "";

 			String fldDefResp = "";

 			String fldValidateOp1 = "";
 			String fldValidateVal1 = "";
 			String fldValidateLogOp1 = "";
 			String fldValidateOp2 = "";
 			String fldValidateVal2 = "";
 			String fldExpLabel = "";
 			String fldIsVisible = "";
 			String offlineFlag = "";

 			String hideLabel = "";
 			String labelDisplayWidth = "";
 			String fldIsRO = "";

 			int fldValId = 0;

 			formId = request.getParameter("formId");
 			String accountId = (String) tSession
 					.getAttribute("accountId");
 			int tempAccountId = EJBUtil.stringToNum(accountId);
 			String catLibType = "C";

 			CatLibDao catLibDao = new CatLibDao();
 			catLibDao = catLibJB.getAllCategories(tempAccountId,
 					catLibType);
 			//ArrayList id= new ArrayList();
 			//ArrayList desc= new ArrayList();
 			ArrayList idSec = new ArrayList();
 			ArrayList descSec = new ArrayList();
 			//String pullDown;
 			//int rows;
 			//rows=catLibDao.getRows();
 			//id = catLibDao.getCatLibIds();
 			//desc= catLibDao.getCatLibNames();
 			//pullDown=EJBUtil.createPullDown("category", 0, id, desc);

 			String pullDownSection = "";
 			FormSecDao formSecDao = new FormSecDao();
 			formSecDao = formSecJB.getAllSectionsOfAForm(EJBUtil
 					.stringToNum(formId));
 			idSec = formSecDao.getFormSecId();
 			descSec = formSecDao.getFormSecName();

 			pullDownSection = EJBUtil.createPullDown("section", 0,
 					idSec, descSec);

 			String inputClass = "inpDefault";
 			String makeReadOnly = "";
 			String makeGrey = "Black";
 			String makeDisabled = "";
 			if (mode.equals("M")) {
 				formId = request.getParameter("formId");
 				formFldId = request.getParameter("formFldId");
 				formFieldJB.setFormFieldId(Integer.parseInt(formFldId));

 				formFieldJB.getFormFieldDetails();
 				fieldLibId = formFieldJB.getFieldId();
 				offlineFlag = formFieldJB.getOfflineFlag();
 				if (offlineFlag == null) {
 					offlineFlag = "";
 				}

 				fieldLibJB.setFieldLibId(Integer.parseInt(fieldLibId));

 				fieldLibJB.getFieldLibDetails();

 				fldExpLabel = ((fieldLibJB.getExpLabel()) == null)
 						? ""
 						: (fieldLibJB.getExpLabel());

 				hideLabel = ((fieldLibJB.getFldHideLabel()) == null)
 						? ""
 						: (fieldLibJB.getFldHideLabel());
 				labelDisplayWidth = ((fieldLibJB.getFldDisplayWidth()) == null)
 						? ""
 						: (fieldLibJB.getFldDisplayWidth());

 				fldSeq = ((formFieldJB.getFormFldSeq()) == null)
 						? ""
 						: (formFieldJB.getFormFldSeq());

 				fldIsVisible = ((fieldLibJB.getFldIsVisible()) == null)
 						? "0"
 						: (fieldLibJB.getFldIsVisible());
 				fldIsRO = ((fieldLibJB.getFldIsRO()) == null)
 						? "0"
 						: (fieldLibJB.getFldIsRO());

 				if (codeStatus.equals("Offline")
 						&& !offlineFlag.equals("0")) {

 					makeReadOnly = "readonly";
 					inputClass = "inpGrey";
 					makeGrey = "Grey";
 					makeDisabled = "disabled";
 				}

 				FldValidateBean fldValSk = fieldLibJB
 						.getFldValidateSk();

 				if (fldValSk != null) {
 					fldValId = fldValSk.getFldValidateId();
 				}

 				if ((fldValSk != null)
 						&& (fldValSk.getFldValidateVal1() != null || fldValSk
 								.getFldValidateVal2() != null)) {
 					fldValidateOp1 = fldValSk.getFldValidateOp1();
 					fldValidateVal1 = fldValSk.getFldValidateVal1();
 					fldValidateLogOp1 = fldValSk.getFldValidateLogOp1();
 					fldValidateOp2 = fldValSk.getFldValidateOp2();
 					fldValidateVal2 = fldValSk.getFldValidateVal2();

 					if (fldValidateOp1 != null) {
 						fldValidateOp1 = fldValidateOp1.trim();
 					} else {
 						fldValidateOp1 = "";
 					}
 					if (fldValidateOp2 != null) {
 						fldValidateOp2 = fldValidateOp2.trim();
 					} else {
 						fldValidateOp2 = "";
 					}
 					if (fldValidateLogOp1 == null)
 						fldValidateLogOp1 = "";
 					if (fldValidateVal1 == null)
 						fldValidateVal1 = "";
 					if (fldValidateVal2 == null)
 						fldValidateVal2 = "";
 				}

 				formSecId = formFieldJB.getFormSecId();

 				fldMandatory = ((formFieldJB.getFormFldMandatory()) == null)
 						? ""
 						: (formFieldJB.getFormFldMandatory());
 				//TO GIVE A SINGLE SECTION IN EDIT MODE
 				ArrayList idSecNew = new ArrayList();
 				ArrayList descSecNew = new ArrayList();
 				int secId = EJBUtil.stringToNum(formSecId);
 				int j = 0;
 				for (j = 0; j < idSec.size(); j++) {
 					int secIdNew = ((Integer) idSec.get(j)).intValue();
 					if (secIdNew == secId) {
 						idSecNew.add((Integer) idSec.get(j));
 						descSecNew.add((String) descSec.get(j));
 						break;
 					}
 				}

 				/// TO MAKE A READONLY BOX FOR NOT LETTING THE EDITING OF THE SECTION FOR FIELD IN EDIT MODE 
 				String sectionN = (String) descSecNew.get(0);
 				pullDownSection = "<label>"
 						+ sectionN
 						+ "</label>"
 						+ "<Input type=\"hidden\" name=\"section\" value="
 						+ (Integer) idSecNew.get(0) + " >  ";

 				//pullDownSection=EJBUtil.createPullDown("section", secId,idSecNew, descSecNew   );

 				fldSeq = ((formFieldJB.getFormFldSeq()) == null)
 						? ""
 						: (formFieldJB.getFormFldSeq());

 				//  WHILE GETTING THE STYLE WE CHECK ITS VALUES TO DISPLAY APPROPRIATELY
 				aStyle = fieldLibJB.getAStyle();
 				if (aStyle != null) {

 					fldAlign = ((aStyle.getAlign()) == null)
 							? "-"
 							: (aStyle.getAlign());
 					fldBold = ((aStyle.getBold()) == null)
 							? "-1"
 							: (aStyle.getBold());
 					fldItalics = ((aStyle.getItalics()) == null)
 							? "-1"
 							: (aStyle.getItalics());
 					samLinePrevFld = ((aStyle.getSameLine()) == null)
 							? "-1"
 							: (aStyle.getSameLine());
 				}

 				fldName = fieldLibJB.getFldName();
 				fldNameFormat = StringUtil.unEscapeSpecialCharHTML(fieldLibJB.getFldNameFormat());
 				fldNameFormat = (fldNameFormat == null)
 						? ""
 						: StringUtil.encodeString(fldNameFormat.replace("&amp;#39;","\'"));

 				if (fldNameFormat.length() > 0) {
 					if (fldNameFormat.indexOf("[VELDISABLE]") >= 0)
 						formatDisable = "Y";
 				}

 				fldUniqId = fieldLibJB.getFldUniqId();
 				numformat = fieldLibJB.getFldFormat();

 				if (numformat == null)
 					numformat = "";

 				//commented by Sonia Abrol 12/13/05 
 				//if(!fldUniqId.equals("er_def_date_01"))

 				dateCheck = fieldLibJB.getTodayCheck();

 				if (dateCheck == null)
 					dateCheck = "";

 				fldKeyword = ((fieldLibJB.getFldKeyword()) == null)
 						? ""
 						: (fieldLibJB.getFldKeyword());

 				fldType = fieldLibJB.getFldType();
 				fldDataType = fieldLibJB.getFldDataType();
 				if (fldDataType == null)
 					fldDataType = "";

 				fldDefResp = ((fieldLibJB.getFldDefResp()) == null)
 						? ""
 						: (fieldLibJB.getFldDefResp());
 				fldInstructions = ((fieldLibJB.getFldInstructions()) == null)
 						? ""
 						: (fieldLibJB.getFldInstructions());
 				fldDesc = ((fieldLibJB.getFldDesc()) == null)
 						? ""
 						: (fieldLibJB.getFldDesc());

 				if (fldDataType.equals("ED")) {
 					if (fldDefResp.equals("[VELSYSTEMDATE]"))
 						defaultDateCheck = "1";
 				}
 				mandatoryChk = ((fieldLibJB.getOverRideMandatory()) == null)
 						? "0"
 						: (fieldLibJB.getOverRideMandatory());
 				rangeChk = ((fieldLibJB.getOverRideRange()) == null)
 						? "0"
 						: (fieldLibJB.getOverRideRange());
 				dateChk = ((fieldLibJB.getOverRideDate()) == null)
 						? "0"
 						: (fieldLibJB.getOverRideDate());
 				formatChk = ((fieldLibJB.getOverRideFormat()) == null)
 						? "0"
 						: (fieldLibJB.getOverRideFormat());

 				if ((fldDataType.equals("EN"))
 						|| (fldDataType.equals("ET"))) {
 					fldDefResp1 = fldDefResp;
 					fldLength = ((fieldLibJB.getFldLength()) == null)
 							? ""
 							: (fieldLibJB.getFldLength());
 					fldCharsNo = ((fieldLibJB.getFldCharsNo()) == null)
 							? ""
 							: (fieldLibJB.getFldCharsNo());
 					fldLinesNo = "";
 					if (fldDataType.equals("ET")) {
 						fldLinesNo = ((fieldLibJB.getFldLinesNo()) == null)
 								? ""
 								: (fieldLibJB.getFldLinesNo());

 					}
 				}

 			}
 %>

<Form name="editBoxField" id="editBoxFieldFrm" method="post"
	action="editBoxSectionSubmit.jsp"
	onSubmit="if (validate(document.editBoxField)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}"
	onkeyup="return fKeyUp(e);"><!--   	<table width="80%" cellspacing="0" cellpadding="0" border="0">
  	
		 <%//if(fldUniqId.equals("er_def_date_01")){%>
		 
<tr>
			<td><br> <P class = "defComments"><FONT class="Mandatory">You cannot edit Data Entry Date type field</p> </td>
		 </tr>
		 <tr><td></td></tr> 
		 
		 <%//}%>
	 </table>
			 <br> --> <Input type="hidden" name="mode" value=<%=mode%> /> <Input
	type="hidden" name="formId" value=<%=formId%> /> <Input type="hidden"
	name="fieldLibId" value=<%=fieldLibId%> /> <Input type="hidden"
	name="formFldId" value=<%=formFldId%> /> <Input type="hidden"
	name="fldValidateId" value="<%=fldValId%>" /> <!--KV:BugNo.5213 --> <Input
	type="hidden" name="hdndatefld" /> <Input type="hidden"
	name="hdndefaultdate" /> <Input type="hidden" name="hdnOverRideDate" />
<Input type="hidden" name="hdnOverRideFormat" /> <Input type="hidden"
	name="hdnOverRideRange" /> <Input type="hidden"
	name="hdnOverRideMandatory" /> <Input type="hidden" name="prevfldid"
	value="<%=fldUniqId%>" />


<table cellspacing="1" cellpadding="2" border="0">
	<tr height="25">
		<td colspan="3">
		<P class="sectionHeadings"><%=MC.M_FldTyp_EdtBox%><%--Field type: Edit box*****--%>
		</P>
		</td>
	</tr>
	<tr class="browserEvenRow">
		<td width="25%"><%=LC.L_Sections%><%--Section*****--%><FONT
			class="Mandatory">* </FONT></td>
		<td width="65%"><%=pullDownSection%></td>
       <td></td>

	</tr>

	<tr class="browserEvenRow">
		<td><%=LC.L_Sequence%><%--Sequence*****--%><FONT
			class="Mandatory">* </FONT></td>
		<td><input type="text" name="sequence" size=10 MAXLENGTH=10
			value="<%=fldSeq%>"></td>
			<td></td>
	</tr>

	<!--	</table>
		<table width="485" cellspacing="0" cellpadding="0" border="0"  > -->



	<!--	</table>	
	<table width="80%" cellspacing="0" cellpadding="0" border="0"  > -->
	<tr class="browserEvenRow">
		<td width = "35%"><%=LC.L_Fld_Name%><%--Field Name*****--%> <FONT
			class="Mandatory">* </FONT></td>
		<!-- Modified by Gopu to fix the bugzilla Issue # 2595> -->
		<td><input type="text" name="name" size="60" maxlength="1000"
			value="<%=fldName%>"></td>
		<!-- Commented by Gopu to fix the bugzilla Issue # 2595>
		<!--<td><textarea id="name" name="name"  rows="" cols="200" style="width:400;height:100;"><%=fldName%></textarea> -->

		<input type="hidden" name="nameta" size=40 MAXLENGTH=100
			value="<%=fldNameFormat%>">
		<td><A href="javascript:void(0);" onClick="openPopupEditor('editBoxField','name','nameta','Velos',2000)"><img src="./images/format.gif" alt="<%=LC.L_Launch_Editor %><%-- Launch Editor*****--%>" border = 0 ></A>
		<%
			if (fldNameFormat.length() > 0) {
						if (formatDisable.equals("N")) {
		%> <BR>
		<DIV id="dformat"><A href="javascript:void(0);"
			onClick="processFormat(document.editBoxField,'D')"><img
			src="./images/disableformat.gif"
			alt="<%=LC.L_Disable_Formatting%><%-- Disable Formatting*****--%>"
			border=0></A></DIV>
		<%
			} else if (formatDisable.equals("Y")) {
		%> <BR>
		<DIV id="dformat"><A href="javascript:void(0);"
			onClick="processFormat(document.editBoxField,'E')"><img
			src="./images/enableformat.gif"
			alt="<%=LC.L_Enable_Formatting%><%-- Enable Formatting*****--%>"
			border=0></A></DIV>
		<%
			}
		%> <A href="javascript:void(0);"
			onClick="processFormat(document.editBoxField,'R')"><img
			src="./images/removeformat.gif"
			alt="<%=LC.L_Rem_Format%><%-- Remove Formatting*****--%>" border=0></A>
		<%
			}
		%>
		</td>


	</tr>

	<tr class="browserEvenRow">
		<td><Font class="<%=makeGrey%>"> <%=LC.L_Fld_Id%><%--Field ID*****--%>
		</font><FONT class="Mandatory">* </FONT></td>
		<td>
		<%
			if (fldUniqId.equals("er_def_date_01")) {
		%> <input type="text"
			name="uniqueId" size=50 MAXLENGTH=50 value="<%=fldUniqId%>" readonly>
		<%
			} else {
		%> <input type="text" name="uniqueId" size=50 MAXLENGTH=50
			value="<%=fldUniqId%>" <%=makeReadOnly%>>
		<%
			}
		%>
		</td>
       <td></td>

	</tr>

	<tr class="browserEvenRow">
		<td><Font class="<%=makeGrey%>">&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Edit_BoxType%><%--Edit Box Type*****--%></font></td>
		<td>
		<%
			if (fldUniqId.equals("er_def_date_01")) {
		%> <input type="radio"
			name="editBoxType" value="ET" disabled> <%=LC.L_Text%><%--Text*****--%>
		<input type="radio" name="editBoxType" value="EN" disabled> <%=LC.L_Number%><%--Number*****--%>
		<input type="radio" name="editBoxType" value="ED" checked disabled>
		<%=LC.L_Date%><%--Date*****--%> <%
 	} else {
 				if (fldDataType.equals("ET")) {
 %> <input type="radio" name="editBoxType" value="ET"
			onclick="disablingField(document.editBoxField,'ET','C')" CHECKED
			<%=makeDisabled%>> <Font class="<%=makeGrey%>"><%=LC.L_Text%><%--Text*****--%></font>
		<input type="radio" name="editBoxType" value="EN"
			onclick="disablingField(document.editBoxField,'EN','C') "
			<%=makeDisabled%>> <Font class="<%=makeGrey%>"> <%=LC.L_Number%><%--Number*****--%></font>
		<input type="radio" name="editBoxType" value="ED"
			onclick="disablingField(document.editBoxField,'ED','C') "
			<%=makeDisabled%>><Font class="<%=makeGrey%>"><%=LC.L_Date%><%--Date*****--%></font>
		<%
			}
		%> <%
 	if (fldDataType.equals("EN")) {
 %> <input type="radio" name="editBoxType" value="ET"
			onclick="disablingField(document.editBoxField,'ET','C')"
			<%=makeDisabled%>><Font class="<%=makeGrey%>"> <%=LC.L_Text%><%--Text*****--%></font>
		<input type="radio" name="editBoxType" value="EN"
			onclick="disablingField(document.editBoxField,'EN','C') " CHECKED
			<%=makeDisabled%>><Font class="<%=makeGrey%>"> <%=LC.L_Number%><%--Number*****--%></font>
		<input type="radio" name="editBoxType" value="ED"
			onclick="disablingField(document.editBoxField,'ED','C') "
			<%=makeDisabled%>><Font class="<%=makeGrey%>"><%=LC.L_Date%><%--Date*****--%></font>
		<%
			}
		%> <%
 	if (fldDataType.equals("ED")) {
 %> <input type="radio" name="editBoxType" value="ET"
			onclick="disablingField(document.editBoxField,'ET','C')"
			<%=makeDisabled%>><Font class="<%=makeGrey%>"> <%=LC.L_Text%><%--Text*****--%></font>
		<input type="radio" name="editBoxType" value="EN"
			onclick="disablingField(document.editBoxField,'EN','C') "
			<%=makeDisabled%>><Font class="<%=makeGrey%>"> <%=LC.L_Number%><%--Number*****--%></font>
		<input type="radio" name="editBoxType" value="ED"
			onclick="disablingField(document.editBoxField,'ED','C') " CHECKED
			<%=makeDisabled%>><Font class="<%=makeGrey%>"> <%=LC.L_Date%><%--Date*****--%></font>
		<%
			}
					}
		%>
		</td>
		<td></td>
	</tr>
	<tr class="browserEvenRow">
		<td>&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Fld_Chars%><%--Field Characteristics*****--%></td>
		<td><input type="radio" name="isvisible" value="0"
			<%if (fldIsVisible.equals("0") || fldIsVisible.equals("")) {%> checked
			<%}%>><%=LC.L_Visible%><%--Visible*****--%> <input
			type="radio" name="isvisible" value="1"
			<%if (fldIsVisible.equals("1")) {%> checked
			<%}
					if (fldUniqId.equals("er_def_date_01")) {%> disabled <%}%> /><%=LC.L_Hide%><%--Hide*****--%>
		<input type="radio" name="isvisible" value="2"
			<%if (fldIsVisible.equals("2")) {%> checked
			<%}
					if (fldUniqId.equals("er_def_date_01")) {%> disabled <%}%> /><%=LC.L_Disabled%><%--Disable*****--%>
		<input type="radio" name="isvisible" value="3"
			<%if (fldIsRO.equals("1")) {%> checked <%}%> /><%=LC.L_Read_Only%><%--Read Only*****--%></td>
			<td></td>
	</tr>

	<tr class="browserEvenRow">
		<td>&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Label_DispWidth%><%--Label Display Width*****--%></td>
		<td><input type="text" name="labelDisplayWidth" size=3
			MAXLENGTH=3 value="<%=labelDisplayWidth%>">&nbsp;% <%
 	if (fldExpLabel.equals("1")) {
 %> <input type="checkBox"
			name="expLabel" value="<%=fldExpLabel%>" checked><%=LC.L_Expand_Label%><%--Expand Label*****--%>

		<%
			} else {
		%> <input type="checkBox" name="expLabel"
			value="<%=fldExpLabel%>"><%=LC.L_Expand_Label%><%--Expand Label*****--%>

		<%
			if (hideLabel.equals("1")) {
		%> <input type="checkBox" name="hideLabel"
			value="1" checked><%=LC.L_Hide_Label%><%--Hide Label*****--%>
		<%
			} else {
		%> <input type="checkBox" name="hideLabel" value="1"><%=LC.L_Hide_Label%><%--Hide Label*****--%>
		<%
			}
		%> <%
 	}
 %> &nbsp;&nbsp; <%
 	if ((fldBold.equals("-1")) || (fldBold.equals("0"))
 					|| (fldBold.equals(""))) {
 %> <input type="checkbox" name="bold" value="1"><%=LC.L_Bold%><%--Bold*****--%>
		<%
			}
		%> <%
 	if (fldBold.equals("1")) {
 %> <input type="checkbox" name="bold" value="1" CHECKED><%=LC.L_Bold%><%--Bold*****--%>
		<%
			}
		%> &nbsp;&nbsp; <%
 	if ((fldItalics.equals("-1")) || (fldItalics.equals("0"))
 					|| (fldItalics.equals(""))) {
 %> <input type="checkbox" name="italics" value="1"><%=LC.L_Italics%><%--Italics*****--%>
		<%
			}
		%> <%
 	if (fldItalics.equals("1")) {
 %> <input type="checkbox" name="italics" value="1" CHECKED><%=LC.L_Italics%><%--Italics*****--%>
		<%
			}
		%>
		</td>
		<td></td>
	</tr>
	<tr class="browserEvenRow">
		<td>&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Fld_Desc%><%--Field Description*****--%></td>
		<td><input type="text" name="description" size=60 MAXLENGTH=500
			value="<%=fldDesc%>"></td>
			<td></td>
	</tr>
	<tr class="browserEvenRow">
		<td><font class="<%=makeGrey%>">&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Search_Keyword_S%><%--Search Keyword(s)*****--%></font>

		<img src="../images/jpg/help.jpg"
			onmouseover="return overlib('<%=MC.M_EtrOneOrMore_Kwrd%><%--Enter one or more keywords separated by a comma for use in querying.*****--%>',CAPTION,'<%=LC.L_Keyword_Help%><%--Keyword Help*****--%>');"
			onmouseout="return nd();"></td>
		<td><input type="text" name="keyword" size=60 MAXLENGTH=255
			value="<%=fldKeyword%>" <%=makeReadOnly%> /></td>
			<td></td>




		<!--	</table>
	<br>
	
	
	<table width="40%" cellspacing="0" cellpadding="0" border="0"  align="right" > -->

		<tr class="browserEvenRow">
			<td><label>&nbsp;&nbsp;&nbsp;&nbsp;<%=MC.M_FldHelp_OnMouse%><%--Field Help <I> (On Mouse Over)</I>*****--%></label></td>
			<!--	  	</tr>
		 <tr> -->
		 
		 <!-- ************ @Gyanendra Update for Bug #4421 ************/ -->
			   <td><textarea id="fldinst" name="instructions" cols="40"
				  rows="3" MAXLENGTH=2000 onfocus="this.hasFocus=true;"
				  onblur="this.hasFocus=false;" ><%=fldInstructions%></textarea>
				  <font class="Mandatory"><div id="countfldinst"><%=MC.M_Limit2000_CharLeft %></div>
			 </td>
			 <td></td>
		 </tr>
         <!-- ************ Update for Bug #4421 end************/ -->
       
		<!--	</table>
	<table  width="50%" cellspacing="0" cellpadding="0" border="0" > -->
		<%
			//Commented as per UCSF requirements
		%>
		<%
			//Now uncommented as per 19th April enhancements
		%>
		<tr class="browserEvenRow">
			<td>&nbsp;&nbsp;&nbsp;&nbsp; <%=MC.M_AlignFld_DispName%><%--Align (Field Display Name)*****--%>
			</td>
			<td>
			<%
				if ((fldAlign.equals("left")) || (fldAlign.equals(""))) {
			%> <input type="radio" name="align" value="left" onclick=" " CHECKED />
			<%=LC.L_Left%><%--Left*****--%> <input type="radio" name="align"
				value="right" onclick=" " /> <%=LC.L_Right%><%--Right*****--%> <input
				type="radio" name="align" value="center" onclick=" " /> <%=LC.L_Center%><%--Center*****--%>
			<input type="radio" name="align" value="top" onclick=" " /> <%=LC.L_Top%><%--Top*****--%>
			<%
				}
						if (fldAlign.equals("right")) {
			%> <input type="radio" name="align" value="left" onclick=" " /> <%=LC.L_Left%><%--Left*****--%>
			<input type="radio" name="align" value="right" onclick=" " CHECKED />
			<%=LC.L_Right%><%--Right*****--%> <input type="radio" name="align"
				value="center" onclick=" " /> <%=LC.L_Center%><%--Center*****--%> <input
				type="radio" name="align" value="top" onclick=" " /> <%=LC.L_Top%><%--Top*****--%>
			<%
				}
						if (fldAlign.equals("center")) {
			%> <input type="radio" name="align" value="left" onclick=" " /> <%=LC.L_Left%><%--Left*****--%>
			<input type="radio" name="align" value="right" onclick=" " /> <%=LC.L_Right%><%--Right*****--%>
			<input type="radio" name="align" value="center" onclick=" " CHECKED />
			<%=LC.L_Center%><%--Center*****--%> <input type="radio" name="align"
				value="top" onclick=" " /> <%=LC.L_Top%><%--Top*****--%> <%
 	}
 			if (fldAlign.equals("top")) {
 %> <input type="radio" name="align" value="left" onclick=" " /> <%=LC.L_Left%><%--Left*****--%>
			<input type="radio" name="align" value="right" onclick=" " /> <%=LC.L_Right%><%--Right*****--%>
			<input type="radio" name="align" value="center" onclick=" " /> <%=LC.L_Center%><%--Center*****--%>
			<input type="radio" name="align" value="top" onclick=" " CHECKED /> <%=LC.L_Top%><%--Top*****--%>
			<%
				}
						if (fldAlign.equals("-")) {
			%> <input type="radio" name="align" value="left" onclick=" " /> <%=LC.L_Left%><%--Left*****--%>
			<input type="radio" name="align" value="right" onclick=" " /> <%=LC.L_Right%><%--Right*****--%>
			<input type="radio" name="align" value="center" onclick=" " /> <%=LC.L_Center%><%--Center*****--%>
			<input type="radio" name="align" value="top" onclick=" " /> <%=LC.L_Top%><%--Top*****--%>
			<%
				}
			%> <!--		 </table>
		 <table width="55%"> --> <%
 	if ((samLinePrevFld.equals("-1"))
 					|| (samLinePrevFld.equals("0"))
 					|| (samLinePrevFld.equals(""))) {
 %> <input type="checkbox" name="sameLine" value="1" /><%=MC.M_SameLine_AsPrevFld%><%--Same line as previous field*****--%>
			<%
				}
			%> <%
 	if (samLinePrevFld.equals("1")) {
 %> <input type="checkbox" name="sameLine" value="1" CHECKED /><%=MC.M_SameLine_AsPrevFld%><%--Same line as previous field*****--%>
			<%
				}
			%>
			</td>
			<td></td>
		</tr>
		<!--	</table>
	<br> <br>
	
	<table width="90%" cellspacing="0" cellpadding="0" border="0" > -->
</table>
<br>
<table width="100%" cellspacing="1" cellpadding="1" border="0">
	<tr>
		<td><p class="sectionHeadingsFrm"><%=LC.L_Fld_Validations_Upper%></p><%--FIELD VALIDATIONS*****--%>
		</td>
		<td colspan="2">
		<%
			if ((fldMandatory.equals("-1"))
							|| (fldMandatory.equals("0"))
							|| (fldMandatory.equals(""))) {
		%> <input type="checkbox" name="mandatory" value="1"><%=LC.L_Mandatory%><%--Mandatory*****--%>
		<%
			}
		%> <%
 	if (fldMandatory.equals("1")) {
 %> <input type="checkbox" name="mandatory" value="1" CHECKED
			<%if (fldUniqId.equals("er_def_date_01")) {%> disabled <%}%>><%=LC.L_Mandatory%><%--Mandatory*****--%>


		<%
			}
		%> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; <%
 	if (mandatoryChk.equals("1")) {
 %>
		<input type="checkBox" name="overRideMandatory" value="1" checked><%=LC.L_Override_MandatoryValidation%><%--Override Mandatory Validation*****--%>
		<%
			} else {
		%> <input type="checkBox" name="overRideMandatory" value="0"
			<%if (fldUniqId.equals("er_def_date_01")) {%> disabled <%}%>><%=LC.L_Override_MandatoryValidation%><%--Override Mandatory Validation*****--%>
		<%
			}
		%>
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td width="25%"> <%=MC.M_ForTxt_BoxOrNumFld%><%--For Text Box/Number Field*****--%>
		</td>
		<td width="10%"><%=LC.L_Fld_Length%><%--Field Length*****--%></td>
		<td><input type="text" name="lengthOf" size=10 MAXLENGTH=25
			value="<%=fldLength%>"></td>
	</tr>
	<tr>
		<td width="20%"></td>
		<td width="20%"><%=LC.L_Number_OfLines%><%--Number of Lines*****--%>
		</td>
		<td><input type="text" name="lines" size=10 MAXLENGTH=25
			value="<%=fldLinesNo%>"></td>
	</tr>
	<tr>
		<td width="20%"></td>
		<td width="24%"><%=LC.L_MaxCharOrNumb%><%--Maximum Characters/Numbers*****--%>
		</td>
		<td><input type="text" name="characters" size=10 MAXLENGTH=25
			value="<%=fldCharsNo%>"></td>
	</tr>
	<tr>
		<td width="20%"></td>
		<td width="20%"><%=LC.L_Default_Resp%><%--Default Response*****--%>
		</td>
		<td><input type="text" name="defResponse1" size=35 MAXLENGTH=50
			value="<%=fldDefResp1%>"></td>
	</tr>


	<table width="100%">
		<br>
		<tr class="browserEvenRow">
			<td width="30%"> <%=LC.L_For_DateFld%><%--For Date Field*****--%>
			</td>
			<%
				if (mode.equals("N")) {
			%>
			<td width="3%"><input type="checkBox" name="datefld" disabled></td>
			<%
				} else {
							if (dateCheck.equals("1")) {
			%>
			<td width="3%"><input type="checkBox" name="datefld" checked></td>
			<%
				} else {
			%>
			<td width="3%"><input type="checkBox" name="datefld"></td>
			<%
				}
						}
			%>

			<td width="67%"><%=MC.M_DtCnt_FutDt%><%--Date cannot be a future date*****--%></td>
		</tr>
		<tr class="browserEvenRow">
			<td width="30%"></td>
			<%
				if (mode.equals("N")) {
			%>
			<td width="3%"><input type="checkBox" name="overRideDate"
				disabled></td>
			<%
				} else {
							if (dateChk.equals("1")) {
			%>
			<td width="3%"><input type="checkBox" name="overRideDate"
				value="1" checked></td>
			<%
				} else {
			%>
			<td width="3%"><input type="checkBox" name="overRideDate"
				value="0"></td>

			<%
				}
						}
			%>
			<td width="67%"><%=LC.L_Override_DtValidation%><%--Override Date validation*****--%></td>

		</tr>
		<tr class="browserEvenRow">
			<td width="30%"></td>
			<%
				if (mode.equals("N")) {
			%>
			<td width="3%"><input type="checkBox" name="defaultdate"
				disabled></td>
			<%
				} else {
							if (defaultDateCheck.equals("1")) {
			%>
			<td width="3%"><input type="checkBox" name="defaultdate" checked></td>
			<%
				} else {
			%>
			<td width="3%"><input type="checkBox" name="defaultdate"></td>
			<%
				}
						}
			%>

			<td width="67%"><%=MC.M_DefDt_ToCurDt%><%--Default date to current date*****--%></td>
		</tr>
	</table>
	<table width="100%">
		<br>
		<tr>
			<td width="25%"> <%=LC.L_For_NumberFld%><%--For Number Field*****--%>
			</td>
			<td>
			<%
				if (mode.equals("N")) {
							if (fldDataType.equals("ET")) {
			%> <input type="text" name="numformat" size=15 MAXLENGTH=25 disabled>
			<%
				}
						} else {
			%> <input type="text" name="numformat" size=15 MAXLENGTH=25
				value=<%=numformat%>> <%
 	}
 %>
			</td>
			<td><%=MC.M_ORideNum_FmtValid%><%--Override Number Format Validation*****--%>
			&nbsp;&nbsp; <%
 	if (fldDataType.equals("ET")) {
 %> <input type="checkbox" name="overRideFormat" disabled> <%
 	} else {
 				if (formatChk.equals("1")) {
 %> <input type="checkbox"
				name="overRideFormat" value="1" checked> <%
 	} else {
 %> <input
				type="checkbox" name="overRideFormat" value="0"> <%
 	}
 			}
 %>
			</td>


		</tr>

	</table>
	<table >
		<tr>
			<td width="25%"></td>
			<td>
			<P class="defComments custom-not-defComments custom-defComments"><%=MC.M_SpecifyFrmt_MaxNum_CanEtr%><%--Specify the format and maximum numbers that can be entered e.g. ### or ##.# etc. where '#' signifies a number and '.' signifies a decimal point*****--%></P>
			</td>

		</tr>
	</table>
	<table width="100%">
		<tr>
			<td width="30%"></td>
			<td width="25%"><%=LC.L_Response_ShouldBe%><%--Response Should Be*****--%></td>
			<%
				if (mode.equals("N")) {
							if (fldDataType.equals("ET")) {
			%>
			<td><select NAME="greaterless1" disabled>
				<option value=""><%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>
				<option VALUE=">"><%=LC.L_Greater_Than%><%--Greater Than*****--%></option>
				<option
					VALUE="<"><%=LC.L_Less_Than%><%--Less Than*****--%></option>
			<option VALUE=">="><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%></option>
			<option VALUE="<="><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%> </option>
			</select>
			</td>
			<td width="8%"><input type="text" name="val1" size=8 readOnly></td>
			 <td width="57%"><select NAME="andor" disabled>
			 <option value=""> <%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>
			 <option VALUE="and" ><%=LC.L_And%><%--And*****--%></option>
             <option VALUE="or"><%=LC.L_Or%><%--Or*****--%></option>
			</select>
			</td>
			</tr>
			</table>
			<table width="100%">
			<tr>
			<td width="52%"></td>
			<td ><select NAME="greaterless2" disabled>
			 <option value=""> <%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>
			<option VALUE=">"><%=LC.L_Greater_Than%><%--Greater Than*****--%></option>
            <option VALUE="<"><%=LC.L_Less_Than%><%--Less Than*****--%></option>
			<option VALUE=">="><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%></option>
			<option VALUE="<="><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%> </option>
			</select>
			</td>
			<td width="60%" align=left><input type="text" name="val2" size=8 readOnly></td>
			 </td>
			</tr>
			</table>



			<%}
					} else {%>
			<td><select NAME="greaterless1">
			   <option value=""> <%=LC.L_Select_AnOption%><%--Select an Option*****--%>
			<%if (fldValidateOp1.equals(">")) {%>	
			   <option VALUE=">" selected><%=LC.L_Greater_Than%><%--Greater Than*****--%></option>
               <option VALUE="<"><%=LC.L_Less_Than%><%--Less Than*****--%></option>
			   <option VALUE=">="><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%></option>
			   <option VALUE="<="><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%> </option>
			<%} else if (fldValidateOp1.equals("<")) {%>
			   <option VALUE=">"><%=LC.L_Greater_Than%><%--Greater Than*****--%></option>
               <option VALUE="<" selected><%=LC.L_Lesser_Than%><%--Lesser Than*****--%></option>
			   <option VALUE=">="><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%></option>
			   <option VALUE="<="><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%> </option>
			<%} else if (fldValidateOp1.equals(">=")) {%>
			  <option VALUE=">"><%=LC.L_Greater_Than%><%--Greater Than*****--%>
               <option VALUE="<"><%=LC.L_Less_Than%><%--Less Than*****--%></option>
			   <option VALUE=">=" selected><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%></option>
			   <option VALUE="<="><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%> </option>
			<%} else if (fldValidateOp1.equals("<=")) {%>
			<option VALUE=">" ><%=LC.L_Greater_Than%><%--Greater Than*****--%></option>
               <option VALUE="<"><%=LC.L_Less_Than%><%--Less Than*****--%></option>
			   <option VALUE=">="><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%></option>
			   <option VALUE="<=" selected><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%></option>
			<%} else {%>		
				<option VALUE=">" ><%=LC.L_Greater_Than%><%--Greater Than*****--%></option>
               <option VALUE="<"><%=LC.L_Less_Than%><%--Less Than*****--%></option>
			   <option VALUE=">="><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%></option>
			   <option VALUE="<=" ><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%></option>
				<%}%>		   
			</select></td>
			<td width="8%"><input type="text" name="val1" size=8 value="<%=fldValidateVal1%>"></td>
			<td width="57%"><select NAME="andor">
			 <option value=""> <%=LC.L_Select_AnOption%><%--Select an Option*****--%>
			<%if (fldValidateLogOp1.equals("and")) {%>
			  <option VALUE="and" selected ><%=LC.L_And%><%--And*****--%></option>
               <option VALUE="or"><%=LC.L_Or%><%--Or*****--%></option>
				  <%} else if (fldValidateLogOp1.equals("or")) {%>
				 <option VALUE="and" ><%=LC.L_And%><%--And*****--%></option>
               <option VALUE="or" selected ><%=LC.L_Or%><%--Or*****--%></option>
				  <%} else {%>
				<option VALUE="and" ><%=LC.L_And%><%--And*****--%></option>
               <option VALUE="or"><%=LC.L_Or%><%--Or*****--%></option>
				<%}%>
          </select></td>
			</tr>
			</table>
			<table width="100%">
			<tr>
			<td width="40%"></td>
			<td ><select NAME="greaterless2">
			 <option value=""> <%=LC.L_Select_AnOption%><%--Select an Option*****--%>
			<%if (fldValidateOp2.equals(">")) {%>
               <option VALUE=">" selected><%=LC.L_Greater_Than%><%--Greater Than*****--%></option>
               <option VALUE="<"><%=LC.L_Less_Than%><%--Less Than*****--%></option>
			   <option VALUE=">="><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%></option>
			   <option VALUE="<="><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%></option>
			<%} else if (fldValidateOp2.equals("<")) {%>
			 <option VALUE=">"><%=LC.L_Greater_Than%><%--Greater Than*****--%></option>
               <option VALUE="<" selected><%=LC.L_Lesser_Than%><%--Lesser Than*****--%></option>
			   <option VALUE=">="><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%></option>
			   <option VALUE="<="><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%></option>
			<%} else if (fldValidateOp2.equals(">=")) {%>
			 <option VALUE=">" ><%=LC.L_Greater_Than%><%--Greater Than*****--%></option>
               <option VALUE="<"><%=LC.L_Less_Than%><%--Less Than*****--%></option>
			   <option VALUE=">=" selected><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%></option>
			   <option VALUE="<="><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%></option>
			<%} else if (fldValidateOp2.equals("<=")) {%>
			 <option VALUE=">" ><%=LC.L_Greater_Than%></option>
               <option VALUE="<"><%=LC.L_Less_Than%><%--Less Than*****--%></option>
			   <option VALUE=">="><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%></option>
			   <option VALUE="<=" selected><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%></option>
			<%} else {%>		
				<option VALUE=">" ><%=LC.L_Greater_Than%><%--Greater Than*****--%></option>
               <option VALUE="<"><%=LC.L_Less_Than%><%--Less Than*****--%></option>
			   <option VALUE=">="><%=MC.M_Greater_ThanEqualTo%><%--Greater Than Equal to*****--%></option>
			   <option VALUE="<=" ><%=MC.M_LessThan_EqualTo%><%--Less Than Equal to*****--%></option>
				<%}%>
          </select></td>
			<td width="60%" align=left><input type="text" name="val2" size=8 value="<%=fldValidateVal2%>"></td>
			<!-- <td width="5%" align=right><input type="checkBox" name="overridden"></td>
			<td width="77%"><%=LC.L_CanBe_Overridden%><%--Can be Overridden*****--%></td> -->
			</tr>
<%}%>
			
			 
			</table>
		   <table width="98%">
		   		  <tr>
			  	  <td width="50%" align="right"><%=MC.M_ORideNum_RngValid%><%--Override Number Range Validation*****--%></td>
			 	<%if (mode.equals("N")) {
						if (fldDataType.equals("ET")) {%>
				<td><input type="checkbox" name="overRideRange" disabled/></td>
			<%}
					} else {
						if (rangeChk.equals("1")) {%>
				<td><input type="checkbox" name="overRideRange" value="1" checked/></td>
					<%} else {%>
				<td><input type="checkbox" name="overRideRange" value="0"/></td>
				
				<%}
					}%>
			</tr>		
				</table>
		   		

<%//BK:#4945 Moved this if block from outside of form tag.	
					if (mode.equals("M")) {
						if ((fldDataType.equals("EN"))
								|| (fldDataType.equals("ET"))) {
							if (fldDataType.equals("ET")) {%>
							<SCRIPT>						 
							 disablingField(document.editBoxField,"ET", "M")
						 	
							</SCRIPT>
 <%} else {%>
								<SCRIPT>										
								disablingField(document.editBoxField,"EN", "M")
								</SCRIPT>
<%}
						} else {%>
								<SCRIPT>										
								disablingField(document.editBoxField,"ED", "M")
								
								</SCRIPT>
<%}
					} // end of if for mode etc%>		
	
  	
	<br>
		<%//if(!fldUniqId.equals("er_def_date_01")){

					//KM-to fix Bug2265
					if (((pageRight == 6 && mode.equals("M"))
							|| (pageRight == 5 && mode.equals("N")) || pageRight == 7)
							|| mode.equals("N")) {
						if ((calledFrom.equals("L") && !codeStatus
								.equals("WIP"))
								|| ((calledFrom.equals("St") || calledFrom
										.equals("A")) && (codeStatus
										.equals("Lockdown")
										|| codeStatus.equals("Deactivated") || codeStatus
										.equals("Active")))) {
						} else {
							if(codeStatus.equals("WIP")){
						System.out.println("Code Status - > " + codeStatus);
						%>
						<table align="center"><tr>
	  <td align="center">
				<button type="submit" id="submit_btn" ondblclick="return false"><%=LC.L_Submit%></button>
			</td> 
	  </tr></table>
     
<%}else{ 
	System.out.println("Code Status - > " + codeStatus);
%>
	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="editBoxFieldFrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include> 
<br>

 	 <table width="100%" cellspacing="0" cellpadding="0">
      <td align=right> 

      </td> 
      </tr>
  </table>
		  <%}
				}			
					}%>
		<input type="hidden" name="codeStatus" value=<%=codeStatus%>>
		
		<%if ((makeDisabled.equals("disabled"))
							|| (fldUniqId.equals("er_def_date_01"))) {%>		
				<input type ="hidden" name="editBoxType" value="<%=fldDataType%>" />
		<%}%>
		<%if (fldUniqId.equals("er_def_date_01")) {%>					
			<input type ="hidden"  name ="mandatory" value="<%=fldMandatory%>" />
			<input type ="hidden" name="isvisible" value="<%=fldIsVisible%>" />			
		<%}%>
	
	
	
  </Form>
  

<%} //end of if body for page right
				else {%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%} //end of else body for page right

			}//end of if body for session

			else

			{%>
  <jsp:include page="timeout.html" flush="true"/>
<%}%>
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</body>

</html>