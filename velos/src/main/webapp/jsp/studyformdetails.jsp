<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page import="com.velos.eres.business.common.FormFieldDao,com.velos.eres.business.common.FormQueryDao,com.velos.eres.business.common.StudyDao,com.velos.eres.business.common.LinkedFormsDao,com.velos.eres.widget.business.common.UIFlxPageDao,com.velos.eres.widget.business.common.ResubmDraftDao"%>
<%@page import="java.util.ArrayList,java.util.HashMap,org.json.*"%>
<html style="overflow-y:hidden;">  
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%
boolean isIrb = "irb_form_tab".equals(request.getParameter("selectedTab")) ? true : false;
%>

<title><%=LC.L_Std_Form%><%-- <%=LC.Std_Study%> Form*****--%></title>
<style type="text/css">
.ui-dialog .ui-dialog-titlebar-close span
{
padding : 0 0 0 0 ;
}
.marker{
background-color:Yellow;}
</style>

<%@ page import="com.velos.eres.service.util.LC, com.velos.eres.service.util.CFG" %>
<%HttpSession tSessionISQ = request.getSession(true);%>
<%!
private final static String isqNotConfigured = "[ISQ_URL_NOT_CONFIGURED]";
private final static String isqNoBeachheads = "{}";
%>
<% if(!isqNotConfigured.equals(CFG.ISQ_URL) && !isqNoBeachheads.equals(CFG.ISQ_BEACHHEADS)){ %>
<script type="text/javascript" src="./js/velosCustom/isquare.js"></script>
<script type="text/javascript">
isquare.isqObj = {
	isquareURL: '<%=CFG.ISQ_URL%>',
	isquareToken: '<%=CFG.ISQ_TOKEN%>',
	location: 'saveSuccessFormResponse',
	isquareBeachheads: '<%=CFG.ISQ_BEACHHEADS%>',
	isquareParams: {
		loggedInUser: <%=(String) tSessionISQ.getAttribute("userId")%>
	}
};
</script>
<%}%>
<SCRIPT>


function openUserSearch(formobj,colName) {
//alert(formobj);
//alert(colName);
  //  windowName=window.open("formusersearch.jsp?formobj="+formobj+"&colName="+colName,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
//	windowName.focus();
}
// Added the parameter 'formLibVerNumber' for to fix the bugzilla issue #2786
function openPrintWin(formId,filledFormId,studyId,formLibVerNumber) {
   //windowName=window.open("formprint.jsp?formId="+formId+"&filledFormId="+filledFormId+"&studyId="+studyId+"&formLibVerNumber="+formLibVerNumber+"&linkFrom=S","Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
   //windowName.focus();
   
   var params = { 'formId' : formId, 'filledFormId': filledFormId,'studyId' :studyId,'formLibVerNumber' :formLibVerNumber,'linkFrom' : 'S'};
   
   var form = document.createElement("form");
   form.setAttribute("method", "post");
   form.setAttribute("action", "formprint.jsp");
   form.setAttribute("target", "Information1");
   for (var i in params)
   {
     if (params.hasOwnProperty(i))
     {
       var input = document.createElement('input');
       input.type = 'hidden';
       input.name = i;
       input.value = params[i];
       form.appendChild(input);
     }
   }
   document.body.appendChild(form);
   window.open(" ", "Information1", "toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
   form.submit();
   document.body.removeChild(form);
}

function openQueries(formId,filledFormId,studyId) {
	  windowName=window.open("addeditquery.jsp?studyId="+studyId+"&formId="+formId+"&filledFormId="+filledFormId+"&from=2","Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
   windowName.focus();

	  var timer = setInterval(function() {   
		    if(windowName.closed) {  
		        clearInterval(timer);  
		        location.reload();
		        parent.preview1.location.reload();
		    }  
		}, 500);

}

function  openReport(formobj,reportId,reportName,repFilledFormId,act,displayMode){
	/*formobj.repId.value = reportId;
	formobj.repName.value = reportName;
	formobj.repFilledFormId.value = repFilledFormId;
	formobj.filterType.value = "A";*/

	formobj.action="repRetrieve.jsp?repId="+reportId+"&repName="+reportName+"&repFilledFormId="+repFilledFormId+"&filterType=A&displayMode="+displayMode;
	formobj.target="_new";
	formobj.submit();
	formobj.action=act;
	formobj.target="";
	}
function confirmBox(pageRight,isLockdown,formStatusDesc)
{
	if (f_check_perm(pageRight,'E') == true)
		{
			msg="<%=LC.L_Del_ThisResponse%>";/*msg="Delete this Response?";*****/
			if(isLockdown==true){
				var paramArray = [formStatusDesc];
				alert(getLocalizedMessageString("M_CantDelete_LockDownFrmResp",paramArray));/*Lockdown status form response cannot be deleted.*****/
			      return false;}
			  else{
				if (confirm(msg)) 
				{
				   return true;
				}
				else
				{
				   return false;
				}
			  }
		}
		else
		{
			return false;
		}
}

</SCRIPT>

</head>

<%
String filledFormId="";
String teamRole="";
String src= request.getParameter("srcmenu");
String selectedTab = StringUtil.htmlEncodeXss(request.getParameter("selectedTab"));
String studyId = request.getParameter("studyId");
String formIdStr=request.getParameter("formId");
formIdStr=(formIdStr==null)?"":formIdStr;
int formId = EJBUtil.stringToNum(formIdStr);
String from = "formdetails";
String dformPullDown="";
String entryChar=request.getParameter("entryChar");
String formFillDt = request.getParameter("formFillDt");
String formPullDown = request.getParameter("formPullDown");
String fldMode = request.getParameter("fldMode");
String eSignRequired = "";

String refreshPanel=request.getParameter("refreshPanel");
refreshPanel=(refreshPanel!=null)?refreshPanel:"";
if(refreshPanel.equals("yes"))
{
%>
<script>
parent.preview1.location.reload();
</script>
<%
}

String calledFromForm = "";
       calledFromForm = request.getParameter("calledFromForm");

      if (StringUtil.isEmpty(calledFromForm))
      {
     	calledFromForm = "";
      }

   String showPanel= "";
       showPanel= request.getParameter("showPanel");

      if (StringUtil.isEmpty(showPanel))
      {
     	showPanel= "true";
      }

		 String formCategory = request.getParameter("formCategory");
		 String submissionType = request.getParameter("submissionType");

    if (StringUtil.isEmpty(formCategory))
			 {
			 	 formCategory="";
			 }

			 if (StringUtil.isEmpty(submissionType))
			 {
			 	 submissionType="";
			 }




   String outputTarget = ""; //if a target name is passed, open the new and edit page in this target
	   outputTarget = request.getParameter("outputTarget");

	 if (StringUtil.isEmpty(outputTarget))
	 {
	 	outputTarget = "";
	 }

	 String specimenPk = "";
    specimenPk = request.getParameter("specimenPk");

     if (StringUtil.isEmpty(specimenPk))
	 {
	 	specimenPk = "";
	 }


   if (calledFromForm.equals("") && showPanel.equals("true"))
   {

%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
 <% }
 else
 { %>
 	<jsp:include page="include.jsp" flush="true"/>

 	<%
 }
 %>
<script>
	 checkQuote="N";
	 </script>
<%@page language = "java" import="java.util.*,com.velos.eres.business.common.FormLibDao,com.velos.eres.business.common.SaveFormDao,com.velos.eres.business.common.LinkedFormsDao,com.velos.eres.service.util.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.user.UserJB,java.util.*,com.velos.eres.business.common.CodeDao" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="formLibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id="userSite" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" /> <!--KM-->
<jsp:useBean id="studyJB" scope="session" class="com.velos.eres.web.study.StudyJB" />
<body style="overflow:auto;">
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>

<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<br>
<%
if (calledFromForm.equals("") && showPanel.equals("true"))
   {
%>
<div class="BrowserTopn" id="divtab">

<% String includeTabsJsp = isIrb ? "irbnewtabs.jsp" : "studytabs.jsp"; 
System.out.println("studyId::"+studyId);
%>
<jsp:include page="<%=includeTabsJsp%>" flush="true">
<jsp:param name="from" value="<%=from%>"/>
<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
<jsp:param name="studyId" value="<%=studyId%>"/>
</jsp:include>
</div>

<script>
var screenWidth = screen.width;
var screenHeight = screen.height;

<% if("Y".equals(CFG.Workflows_Enabled)&& (studyId!=null && !studyId.equals("") && !studyId.equals("0"))){ %>
if(screenWidth>1280 || screenHeight>1024){
	document.write("<div class='BrowserBotN BrowserBotN_S_3 workflowDivBig' id='div1'>");
}else{
	document.write("<div class='BrowserBotN BrowserBotN_S_3 workflowDivSmall' id='div1'>");
}
<% } else{ %>

	document.write("<div class='BrowserBotN BrowserBotN_S_3' id='div1'>");

<% } %>
</script>

<%
   }else{
%>
<div>
<%}%>
<%
filledFormId = request.getParameter("filledFormId");
HttpSession tSession = request.getSession(true);
String dashBoard=request.getParameter("dashBoard");
String clickedBy=request.getParameter("clickedBy");
 if (sessionmaint.isValidSession(tSession))
 {
	 //EDC_AT4 extn
	 if (studyId == null || studyId.equals("null")){
	 	tSession.setAttribute("studyIdForFormQueries","");	
	 }else{
	 	tSession.setAttribute("studyIdForFormQueries",studyId);
	 }
	 
 	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
	if(stdRights==null&& "jupiter".equals(dashBoard)){
		System.out.println("stdRights is null");
		studyJB.setId(StringUtil.stringToNum(studyId));
		studyJB.getStudyDetails();
		String mode = "M";
		String createType = studyJB.getStudyCreationType();
		HashMap sysDataMap = studyJB.getStudySysData(request);
		if (null == sysDataMap)
			return;
		String autoGenStudy = (String) sysDataMap.get("autoGenStudy");
		//tSession.setAttribute("autoGenStudyForJS",autoGenStudy);
		//tSession.setAttribute("studyNo",sysDataMap.get("studyNo"));
		
		String roleCodePk = (String) sysDataMap.get("studyRoleCodePk");
		//tSession.setAttribute("studyRoleCodePk",roleCodePk);

		stdRights = (StudyRightsJB) sysDataMap.get("studyRights");
		//tSession.setAttribute("studyRights",stdRights);

		int stdRight = StringUtil.stringToNum((sysDataMap.get("stdSummRight").toString()));
		//tSession.setAttribute("studyRightForJS",stdRight);
	}
	ArrayList<String> wfFormIds = new ArrayList<String>();
	StudyDao sDao= new StudyDao();
	LinkedFormsDao lfDao = new LinkedFormsDao();
	if("LIND".equals(CFG.EIRB_MODE)){
		String pageFormResponses = "";
		// Here retrieve the latest flx page version form responses
		filledFormId = request.getParameter("filledFormId");
		UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
		ResubmDraftDao resubmDraftDao = new ResubmDraftDao();
		String flxFullVersion = null;
		String resubmFullVersion = null;
		// -1 indicates that highest version is unknown to calling page hence find it first
		uiFlxPageDao.getHighestFlexPageFullVersion("er_study", EJBUtil.stringToNum(studyId));
		flxFullVersion = uiFlxPageDao.getMajorVer() + "." + uiFlxPageDao.getMinorVer();
		resubmDraftDao.getHighestResubmDraftFullVersion("er_study", EJBUtil.stringToNum(studyId));
		resubmFullVersion = resubmDraftDao.getMajorVer() + "." + resubmDraftDao.getMinorVer();
		if(EJBUtil.stringToFloat(resubmFullVersion) > EJBUtil.stringToFloat(flxFullVersion)){
			pageFormResponses = resubmDraftDao.getResubmDraftFormResponses("er_study", EJBUtil.stringToNum(studyId), resubmFullVersion);
		}else{
			pageFormResponses = uiFlxPageDao.getPageVersionFormResponses("er_study", EJBUtil.stringToNum(studyId), flxFullVersion);
		}
		JSONObject jsFormRespJSON = null;
		try {
			if(!StringUtil.isEmpty(pageFormResponses)){
				jsFormRespJSON = new JSONObject(pageFormResponses);
				Iterator formRespIterator = jsFormRespJSON.keys();
				while (formRespIterator.hasNext()) {
					String formNameKey = (formRespIterator.next()).toString();
					JSONObject jsFormRespObj =  jsFormRespJSON.getJSONObject(formNameKey);
					String formRespId = jsFormRespObj.getString("formRespId");
					if (StringUtil.isEmpty(formRespId) || wfFormIds.contains(filledFormId)){ continue; }
					if(filledFormId!=null && !filledFormId.equals("") && EJBUtil.stringToNum(filledFormId)<=EJBUtil.stringToNum(formRespId)){
						wfFormIds.add(filledFormId);
					}
				}
			}
		}catch (JSONException e) {
			e.printStackTrace();
		}
	}
 	String findStr = "";
 	String findStr1="";
	StringBuffer sbuffer = null;
	int len = 0;
	int pos = 0;
	int pageRight = 0;
	int study_acc_form_right = 0,formRights=0;
	int study_team_form_access_right =0;
	int study_team_manage_pat=0;

	String studyRoleCodePk;

	studyRoleCodePk = (String) tSession.getValue("studyRoleCodePk");

	if (StringUtil.isEmpty(studyRoleCodePk))
	{
		studyRoleCodePk = "";
	}

	Hashtable htMoreParam = new Hashtable();

	htMoreParam.put("teamRolePK",studyRoleCodePk);

    lnkformB.findByFormId((formId));
	String formLinkedFrom = lnkformB.getLnkFrom();
	String formDispType = lnkformB.getLFDisplayType();
	if (formDispType==null) formDispType="-";
	formDispType = formDispType.trim();

	if (formLinkedFrom==null) formLinkedFrom="-";

	if (StringUtil.isEmpty(outputTarget))
	 {


			if (formLinkedFrom.equals("S"))
			{

		    	pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMACC"));
		    } else if (formLinkedFrom.equals("A"))
			{
		    	 if (formDispType.equals("S")) { //specific study form

		    		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMACC"));
				 } else if (formDispType.equals("SA")) { //all study forms
				 	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("STUDYFRMSACC"));
				 }
			}



			if ((stdRights.getFtrRights().size()) == 0){
			 	study_team_form_access_right= 0;
			}else{
			 study_acc_form_right = Integer.parseInt(grpRights.getFtrRightsByValue("STUDYFRMSACC"));
			 study_team_manage_pat = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));
			 study_team_form_access_right=Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMACC"));
			}

			formRights = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));

	 }
	 else if(outputTarget.equals("specFormIds")) //bypass all access rights
	 {
	 		pageRight = 7;
	 		study_acc_form_right = 7;
	 		study_team_form_access_right = 7;
	 		formRights = 7;
	 }
	 
	//Bug# 6638
	String accessRight = "";
	if(isAccessibleFor(study_team_form_access_right,'E')){
		if(isAccessibleFor(pageRight,'E')){
			accessRight="E";
		}else if(isAccessibleFor(pageRight,'V') || isAccessibleFor(pageRight,'N')){
			accessRight="V";
		}
	}else{
		if(isAccessibleFor(study_team_form_access_right,'E')){
			accessRight="E";
		}else if(isAccessibleFor(study_team_form_access_right,'V') || isAccessibleFor(study_team_form_access_right,'N')){
			accessRight="V";
		}
	}
	String strFormId = request.getParameter("formPullDown");
    String formLibVer = request.getParameter("formLibVer");
 // -- Start of Bug 14656 fix
   	// Make sure that formId is a valid Account Form ID
	//accFormId = request.getParameter("formId");
 
 	//int formIdstr=0;
	StringTokenizer strTokenizers = new StringTokenizer(strFormId,"*");
	//formIdstr = StringUtil.stringToNum(strTokenizers.nextToken());
	int studyFormIdInt = -1;
	if (!StringUtil.isEmpty(formIdStr)) {
		UserJB userB1 = (UserJB) tSession.getAttribute("currentUser");
		/*Comment for Bug# 16271*/
		/*LinkedFormsDao lnkFrmDao1 = lnkformB.getStudyForms(
				StringUtil.stringToNum((String)tSession.getAttribute("accountId")), 
				StringUtil.stringToNum((String)studyId),
				StringUtil.stringToNum((String)tSession.getAttribute("userId")),
				StringUtil.stringToNum(userB1.getUserSiteId()));*/
		/*Added for Bug# 16271 to get all the form of study*/				
		LinkedFormsDao lnkFrmDao1 = lnkformB.getStudyForms(
				StringUtil.stringToNum((String)tSession.getAttribute("accountId")),
				StringUtil.stringToNum((String)studyId),
				StringUtil.stringToNum((String)tSession.getAttribute("userId")),
				StringUtil.stringToNum(userB1.getUserSiteId()), 
		         		study_acc_form_right,  
		         		study_team_form_access_right, 
		         		isIrb, 
		         		submissionType, 
		         		formCategory);
		boolean isValidAccountFormId = false;
		try { studyFormIdInt = Integer.parseInt(formIdStr); } catch(Exception e) {}
		for (Object myFormId : lnkFrmDao1.getFormId()) {
			int myFormIdInt = ((Integer) myFormId).intValue();
			if (studyFormIdInt == myFormIdInt ) {
				isValidAccountFormId = true;
				break;
			}
		}
		if (!isValidAccountFormId) {
			pageRight = 0; // Invalid formId in request parameter => reject
		}
	}
	
	/*if (!StringUtil.isEmpty(formIdStr)) {
		UserJB userB1 = (UserJB) tSession.getAttribute("currentUser");
		LinkedFormsDao lnkFrmDao1 = lnkformB.getStudyLinkedForms(
				StringUtil.stringToNum((String)studyId));
		boolean isValidAccountFormId = false;
		try { studyFormIdInt = Integer.parseInt(formIdStr); } catch(Exception e) {}
		for (Object myFormId : lnkFrmDao1.getFormId()) {
			int myFormIdInt = ((Integer) myFormId).intValue();
			if (studyFormIdInt == myFormIdInt && (studyFormIdInt==formIdstr)) {
				isValidAccountFormId = true;
				break;
			}
		}
		if (!isValidAccountFormId) {
			pageRight = 0; // Invalid formId in request parameter => reject
		}
	}*/
	
	
	// -- End of Bug 14656 fix
	
	// -- Start of Bug 14659 fix
	// Make sure the formId and formLibVer combination is valid
	//int studyFormIdInt = formId;
	if (!StringUtil.isEmpty(formLibVer)) {
		FormLibDao formLibDao1 = new FormLibDao();
		int fkFormLib = formLibDao1.getFkFormLibForPkFormLibVer(StringUtil.stringToNum(formLibVer));
		if (studyFormIdInt > 0 && studyFormIdInt != fkFormLib) {
			pageRight = 0; // Invalid formId and formLibVer combination in request parameters => reject
		}
	}
	// -- End of Bug 14659 fix
	
	session.setAttribute("formQueryRight",""+pageRight);
	int orgRight  = 0;
	if (!StringUtil.isEmpty(formIdStr)) {
		UserJB userB1 = (UserJB) tSession.getAttribute("currentUser");
	 orgRight = userSite.getRightForUserSite(StringUtil.stringToNum((String)tSession.getAttribute("userId")), 
			 	StringUtil.stringToNum(userB1.getUserSiteId()));
	}
	
	if (! lnkformB.hasFormAccess(formId, StringUtil.stringToNum((String)tSession.getAttribute("userId"))))    			
	{
		//user does not have access rights to this form
		study_acc_form_right = 0;
		study_team_form_access_right = 0;
	}
	
   	int filledFormUserAccessInt = lnkformB.getFilledFormUserAccess(
    		StringUtil.stringToNum(request.getParameter("filledFormId")),
    		StringUtil.stringToNum(request.getParameter("formId")),
    		StringUtil.stringToNum((String)tSession.getAttribute("userId")) );
    
	if (pageRight>=4 && filledFormUserAccessInt > 0 || orgRight>0 )
	{

       String mode = request.getParameter("mode");
       SaveFormDao saveDao= new SaveFormDao();
       String filledFromStatValue ="";
       //Create the dropdown here for mahi enhancement
       ArrayList arrFrmNames = null;
		 ArrayList arrNumEntries = null;
 		 ArrayList arrFrmIds = null;
		 ArrayList arrEntryChar = null;
		 ArrayList arrFrmInfo = new ArrayList();
		 String frmName = "";
		  String numEntries = "";
		 String frmInfo= "";
		  String firstFormInfo = "";

  	    

	 	 String accId=(String)tSession.getAttribute("accountId");
		 int iaccId=EJBUtil.stringToNum(accId);
 	     String userId = (String) tSession.getValue("userId");
	     UserJB userB = (UserJB) tSession.getValue("currentUser");
   	     String siteId = userB.getUserSiteId();
	 	 int istudyId = EJBUtil.stringToNum(studyId);
		 boolean userHasFormAccess = false;

		 if (study_acc_form_right>=4 || study_team_form_access_right>=0)
		{
		 LinkedFormsDao lnkFrmDao = new LinkedFormsDao();
		 lnkFrmDao = lnkformB.getStudyForms(iaccId,istudyId, EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId),
		  study_acc_form_right,  study_team_form_access_right,isIrb, submissionType, formCategory);


		 arrFrmIds = lnkFrmDao.getFormId();
		 arrFrmNames = lnkFrmDao.getFormName();
		 arrEntryChar = lnkFrmDao.getFormEntryChar();
		 arrNumEntries = lnkFrmDao.getFormSavedEntriesNum();

		 if (arrFrmIds.size() > 0) {
    		 if (strFormId==null) {
    		 	formId = EJBUtil.stringToNum(arrFrmIds.get(0).toString());
    			entryChar = arrEntryChar.get(0).toString();
    			numEntries = arrNumEntries.get(0).toString();
    			firstFormInfo = arrFrmIds.get(0).toString() + "*"+ entryChar + "*" + arrNumEntries.get(0).toString();
    		 }
    		 else {
    		 	 StringTokenizer strTokenizer = new StringTokenizer(strFormId,"*");
    		 	 if (calledFromForm.equals("") && showPanel.equals("true"))
    	 		 {

	    	 	     formId = EJBUtil.stringToNum(strTokenizer.nextToken());
	    			 entryChar = strTokenizer.nextToken();
	    		     numEntries = strTokenizer.nextToken();
	    			 firstFormInfo = strFormId;
	    		}
	    		else
    			 {
    			 	formId = EJBUtil.stringToNum(strTokenizer.nextToken());
    			 	entryChar = strTokenizer.nextToken();

    			 	lnkformB.findByFormId(formId );
		   		 	numEntries = lnkformB.getLfDataCnt();
    			 	//get the number of times the form was answered
    			 	//prepare strFormId again
    			 	strFormId = formId + "*" + entryChar + "*" + numEntries;
    			 	firstFormInfo = strFormId;

    			 }
    		 }
    		 
    		 for (int i=0;i<arrFrmIds.size();i++)
    		 {  //store the formId, entryChar and num Entries separated with a *
    		  	 frmInfo = arrFrmIds.get(i).toString() + "*"+ arrEntryChar.get(i).toString() + "*" + arrNumEntries.get(i).toString();
    		 	 arrFrmInfo.add(frmInfo);
    		 	 if ((Integer)arrFrmIds.get(i) == StringUtil.stringToNum(request.getParameter("formId"))) {
    		 		userHasFormAccess = true;
    		 	 }
    		 }


    	 	 dformPullDown = EJBUtil.createPullDownWithStrNoSelect("formPullDown",firstFormInfo, arrFrmInfo , arrFrmNames);
		 }
		 }
		 if (!userHasFormAccess) {
%>
<jsp:include page="accessdenied.jsp" flush="true"/>
<div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</html>
<%
			return;
		 }
		 
		 
    		 StringBuffer formBuffer = new StringBuffer(dformPullDown);

			 //submit the form when the user selects the form name
			 /* if (calledFromForm.equals(""))
			 {
			 	formBuffer.replace(0,7,"<SELECT onChange=\"document.studyform.submit();\"");
			 }
			 else
			 {
			 //	formBuffer.replace(0,7,"<SELECT DISABLED onChange=\"document.studyform.submit();\"");
			 }*/

			 dformPullDown = formBuffer.toString();
       //end mahi enhancements

        filledFormId = request.getParameter("filledFormId");
       String formDispLocation = request.getParameter("formDispLocation");
       String formHtml = "";
	   String name="";
	   String formLibVerNumber="";
	   int fillFormStat=0;


	   formLibB.setFormLibId((formId));
	   formLibB.getFormLibDetails();
	   name=formLibB.getFormLibName();

	  eSignRequired =formLibB.getEsignRequired();

	   if (StringUtil.isEmpty(eSignRequired))
	   {
	   		eSignRequired = "1";
	   }

       String formStatus = formLibB.getFormLibStatus();
	   //Added by Manimaran for November Enhancement F6
	   //JM: 122206
	   //formLibVerNumber = formLibB.getFormLibVersionNumber(EJBUtil.stringToNum(formLibVer));
		//added by IA 11.05.2006
		FormLibDao fd = new FormLibDao();
		if ((formLibVer==null)|| formLibVer.equals("")){

			formLibVer = fd.getLatestFormLibVersionPK(EJBUtil.stringToNum(formIdStr));

		}
		//end added

	   formLibVerNumber = formLibB.getFormLibVersionNumber(EJBUtil.stringToNum(formLibVer));


	   CodeDao cdao = new CodeDao();
	   /******** find study team role for DashBoard(jupiter) only******* Begin */
	   if("jupiter".equals(dashBoard)|| "mouseright".equals(clickedBy)){
			
			int codeLstPk=cdao.getStudyRolePK(StringUtil.stringToNum(studyId),StringUtil.stringToNum((String)tSession.getAttribute("userId")));
			studyRoleCodePk=(codeLstPk==0)?"":StringUtil.integerToString(codeLstPk);
			tSession.setAttribute("studyRoleCodePk", studyRoleCodePk);
			//studyRoleCodePk=StringUtil.integerToString(codeLstPk);
			htMoreParam.put("teamRolePK",studyRoleCodePk);
		}
	   /*******find study team role for DashBoard(jupiter) only********** End*/
	   cdao.getCodeValuesById(EJBUtil.stringToNum(formStatus));
	   ArrayList arrSybType = cdao.getCSubType();
	   String statSubType = arrSybType.get(0).toString();
		%>
		<form name="studyform" id="studyForm" action="formfilledstudybrowser.jsp" method="post">
		<input type="hidden" name="srcmenu" value=<%=src%>>
     		<input type="hidden" name="selectedTab" value="<%=selectedTab%>">
     		<input type="hidden" name="studyId" value=<%=studyId%>>
     		<input type="hidden" id="formRights" value=<%=formRights%>>
    	<%  if ("true".equals(request.getParameter("irbReviewForm"))) { %>
    		<input type="hidden" name="irbReviewForm" value="true">
    	<%  }  %>

		<table width="100%" border="0"  >
		<% if (calledFromForm.equals("") && showPanel.equals("true"))
		{
			%>

			<tr bgcolor="#dcdcdc" class="custom-bg-color">
			<td width="60%" align="right" class="custom-center-text">
			<span><%=LC.L_Jump_ToForm%><%--Jump to Form*****--%>: <%=dformPullDown%></span></td>
			<td >
			<button type="submit" onclick="javascript:fn_nextpage(document.studyform.formPullDown.value) ;"><%=LC.L_Go%></button>
			</td></tr>
			<tr height="5"><td></td></tr>
			</table>
			<script>
			</script>
	<table width="100%">
			<% } 
			if (mode.equals("N"))
       		 {
			formHtml =  lnkformB.getFormHtmlforPreview((formId),htMoreParam);

			System.out.println("******************formHtml (N): - "+ formHtml);
			
			
        		}
        		else
        			{
        			saveDao=lnkformB.getFilledFormHtml(EJBUtil.stringToNum(filledFormId),formDispLocation,"Y",htMoreParam);
			formHtml =  saveDao.getHtmlStr();
			
			//System.out.println(" my form data "+filledFormId+" :: ******************formHtml (M): - "+ formHtml);
        		}
			CodeDao codeLstStatus = new CodeDao();
        			filledFromStatValue = codeLstStatus.getCodeDescription(saveDao.getFilledFormStat());
        			
        			System.out.println("******************filledFromStatValue (M): - "+ filledFromStatValue);
        			
        			%>
		<tr bgcolor="#dcdcdc" class="custom-bg-color">
			 <td width="50%"><!--KM-to fix the Bug 2809 -->
			 	 <b> <%=LC.L_Open_FormName%><%--Open Form Name*****--%>: </b><%=name%>&nbsp;&nbsp;
			 </td>
			 <td width="50%">
			 	 <%if (mode.equals("M")){
			 	 if("jupiter".equals(dashBoard)){%>
		 			<A style="display:none" onclick="openQueries(<%=formId%>,<%=filledFormId%>,<%=studyId%>)" href=#> <%=LC.L_AddOrEdit_Queries%><%--Add/Edit Queries*****--%></A>
			 <%}else{%>
			 	<A onclick="openQueries(<%=formId%>,<%=filledFormId%>,<%=studyId%>)" href=#> <%=LC.L_AddOrEdit_Queries%><%--Add/Edit Queries*****--%></A>
			 <%} %>&nbsp;
			 <%}%>


		 		<A onclick="openPrintWin(<%=formId%>,<%=filledFormId%>,<%=studyId%>,<%=formLibVerNumber%>)" href=#><img src="./images/printer.gif" title="<%=LC.L_Printer_FriendlyFormat%><%--Printer Friendly Format*****--%>" alt="<%=LC.L_Printer_FriendlyFormat%><%--Printer Friendly Format*****--%>" align="absmiddle" border="0"></img></A>&nbsp;
				<%if (entryChar.equals("E") && (!StringUtil.isEmpty(filledFormId)) ){%>
		 		 <A href="#" onclick="openReport(document.studyform,'101','Form Audit Trail','<%=filledFormId%>','formfilledstudybrowser.jsp?srcmenu=tdMenuBarItem5&page=1','')" title="<%=LC.L_ViewAud_Trial%><%--View Audit Trail*****--%>"><%=LC.L_Audit%><%--Audit*****--%></A>
		 		 &nbsp; <A href="#" onclick="openReport(document.studyform,'101','Track Changes','<%=filledFormId%>','formfilledstudybrowser.jsp?srcmenu=tdMenuBarItem5&page=1','<%=accessRight%>')" title="<%=LC.L_Track_Changes%><%--Track Changes*****--%>"><%=LC.L_Track_Changes%><%--Track Changes*****--%></A>
				<%if(wfFormIds.size()>0 && sDao.isStudyLocked(EJBUtil.stringToNum(studyId)) && lfDao.checkIsIRBform(formId)==1){
			    	if(filledFormId!=null && !filledFormId.equals("") && wfFormIds.contains(filledFormId)){
						
					}else{%>
						&nbsp;<A onclick="return confirmBox(<%=pageRight%>,<%=saveDao.isLocked()%>,'<%=filledFromStatValue%>');" href="formrespdelete.jsp?srcmenu=<%=src%>&calledFromForm=<%=calledFromForm%>&formCategory=<%=formCategory%>&submissionType=<%=submissionType%>&showPanel=<%=showPanel%>&selectedTab=<%=selectedTab%>&formId=<%=formId%>&formLibVer=<%=formLibVer%>&mode=M&studyId=<%=studyId%>&formDispLocation=S&filledFormId=<%=filledFormId%>&calledFrom=S&entryChar=<%=entryChar%>&formFillDt=<%=formFillDt%>&formPullDown=<%=formPullDown%>&outputTarget=<%=outputTarget%>"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" /></A>
					<%}
				}else{%>
					&nbsp;<A onclick="return confirmBox(<%=pageRight%>,<%=saveDao.isLocked()%>,'<%=filledFromStatValue%>');" href="formrespdelete.jsp?srcmenu=<%=src%>&calledFromForm=<%=calledFromForm%>&formCategory=<%=formCategory%>&submissionType=<%=submissionType%>&showPanel=<%=showPanel%>&selectedTab=<%=selectedTab%>&formId=<%=formId%>&formLibVer=<%=formLibVer%>&mode=M&studyId=<%=studyId%>&formDispLocation=S&filledFormId=<%=filledFormId%>&calledFrom=S&entryChar=<%=entryChar%>&formFillDt=<%=formFillDt%>&formPullDown=<%=formPullDown%>&outputTarget=<%=outputTarget%>"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" /></A>
				<%}
			}%>

			 </td>
			 
			 
		</tr>

		<!--Added by Manimaran for the November Enhancement F6-->
		<!--tr>
		<td width="40%">
			 	 <b> Version Number: <%=formLibVerNumber%></b>
		</td>
		</tr-->
		</table>
		</form>
		<script>
				function fn_nextpage(val)
		{
				 document.studyform.submit();
		}
		</script>
			<%
       
		//Replace Submit image with button
		System.out.println("PageRight-"+pageRight+" mode-"+mode+" entryChar-"+entryChar+" statSubType-"+statSubType+" saveDao.isLocked()-"+saveDao.isLocked()+" isSoftLock-"+saveDao.isSoftLock()+" isAllowedUnlock()-"+saveDao.isAllowedUnlock()+ "studyRoleCodePk- "+studyRoleCodePk);
		findStr ="<input id=\"submit_btn\" border=\"0\" align=\"absmiddle\" src=\"../images/jpg/Submit.gif\" type=\"image\">";
		findStr1="<input type=\"image\" src=\"../images/jpg/Submit.gif\" align=\"absmiddle\" border=\"0\" id=\"submit_btn\">";
        if(formHtml.indexOf(findStr)>-1) {
		len = findStr.length();
	    pos = formHtml.lastIndexOf(findStr);
	}else{
		len = findStr1.length();
	    pos = formHtml.lastIndexOf(findStr1);
	}
	    if (pos > 0)
	    {
		    sbuffer = new StringBuffer(formHtml);
		    if(pageRight==4  || (pageRight == 6 && mode.equals("N")) || (pageRight < 6 && mode.equals("M")) || (entryChar.equals("O")&& mode.equals("M")) || (statSubType.equals("L")) ||(saveDao.isLocked()) || (saveDao.isSoftLock())&& !(saveDao.isAllowedUnlock())){
		    	sbuffer.replace(pos,pos+len,"");
		    }else{
		    	if(wfFormIds.size()>0 && sDao.isStudyLocked(EJBUtil.stringToNum(studyId)) && lfDao.checkIsIRBform(formId)==1){
			    	if(filledFormId!=null && !filledFormId.equals("") && wfFormIds.contains(filledFormId)){
			    		sbuffer.replace(pos,pos+len,"");
			    		sbuffer.append("<table><tr><td width=\"20%\" align=\"center\"><B>"+LC.L_EC_CannotSubmitFormResponse+"</B></td></tr></table>");
			    	}else{
			    		sbuffer.replace(pos,pos+len,"<button type='submit'>"+LC.L_Submit+"</button>");
			    	}
		    	}else{
		    		sbuffer.replace(pos,pos+len,"<button type='submit'>"+LC.L_Submit+"</button>");
		    	}
		    }
		    formHtml = sbuffer.toString();
	    }
	    
	    findStr = "name=\"eSign\"";
	    pos = formHtml.lastIndexOf(findStr);
	    if(pos > 0)
	    {
	    	sbuffer = new StringBuffer(formHtml);
	    	sbuffer.insert(pos-1," autocomplete=\"off\" ");
	    	formHtml = sbuffer.toString();  	    	
	    }
	    
	    findStr ="cannot be greater than today's date";
  	    len = findStr.length();
  	    pos = formHtml.lastIndexOf(findStr);

  	    if (pos > 0)
  	    {
  		    sbuffer = new StringBuffer(formHtml);
			sbuffer.replace(pos,pos+len,"cannot be a future date.");
  		    formHtml = sbuffer.toString();
  	    }
  	  if ("true".equals(request.getParameter("irbReviewForm"))) { 
  	    findStr="<span id=\"eSignMessage\""+"></span>";  	
  		formHtml=formHtml.replace(findStr,"</br><span id=\"eSignMessage\""+"></span>");
  		}
  	  
  	
		//in case of view right, remove the submit button
		//in case of lockdown status, remove the submit button
		//in case of lockdown status, remove the submit button
		if(pageRight==4  || (pageRight == 6 && mode.equals("N")) || (pageRight < 6 && mode.equals("M")) || (entryChar.equals("O")&& mode.equals("M")) || (statSubType.equals("L")) ||(saveDao.isLocked()) || saveDao.isSoftLock() && !saveDao.isAllowedUnlock()){

		    // Disable the Form tag for issue 4632
		    formHtml = formHtml.replaceAll("onSubmit=\"return validationWithAjax\\(\\)\"","")
			   .replaceAll("action=\"updateFormData.jsp\"","action=\"javascript:void(0)\"")
			   .replaceAll("formobj.action=\"querymodal.jsp\"","formobj.action=\"javascript:void(0)\"");
		    
		   
		    

			//Added by Manimaran for the issue 2941
			sbuffer = new StringBuffer(formHtml);
			sbuffer.append("<table><tr><td>"+LC.L_Form_VersionNumber+": "+formLibVerNumber+"</td></tr></table>");/*sbuffer.append("<table><tr><td>Form Version Number: "+formLibVerNumber+"</td></tr></table>");*****/
			sbuffer.append("<input type=\"hidden\" id=\"formId\" name=\"formId\" value='"+formId+"'/>");
			formHtml = sbuffer.toString();

		} else {

		   findStr = "</Form>";
		   len = findStr.length();
		   pos = formHtml.lastIndexOf(findStr);
		   sbuffer = new StringBuffer(formHtml);
		   StringBuffer paramBuffer = new StringBuffer();

		  //  by salil on 13 oct 2003 to see if the check box type multiple choice exists
    	  //  or not and to handle the case
	     // when no option is selected in a check box type multiple choice the element name does not
	  	 // be  carried forward to the next page hence the "chkexists"  variable is used to see
		 // if the page has a multiple choice field or not .

		   String chkexists ="";
		   String tochk="type=\"checkbox\"";
		   if(formHtml.indexOf(tochk) == -1 )
		   {
		     chkexists ="N";
		   }
		   else
		   {
		   	 chkexists ="Y";
		   }

		   paramBuffer.append("<input type=\"hidden\" name=\"formDispLocation\" value='"+formDispLocation+"'/>");
		   paramBuffer.append("<input type=\"hidden\" name=\"formStudy\" value='"+studyId+"'/>");
		   paramBuffer.append("<input type=\"hidden\" name=\"formFillMode\" value='"+mode+"'/>");
		   paramBuffer.append("<input type=\"hidden\" id=\"formId\" name=\"formId\" value='"+formId+"'/>");
		   paramBuffer.append("<input type=\"hidden\" name=\"srcmenu\" value='"+src+"'/>");
		   paramBuffer.append("<input type=\"hidden\" name=\"formFillDt\" value='"+formFillDt+"'/>");
		   paramBuffer.append("<input type=\"hidden\" name=\"formPullDown\" value='"+formPullDown+"'/>");
		   paramBuffer.append("<input type=\"hidden\" name=\"selectedTab\" value='"+selectedTab+"'/>");
		   paramBuffer.append("<input type=\"hidden\" name=\"calledFromForm\" value='"+calledFromForm+"'/>");
           paramBuffer.append("<input type=\"hidden\" name=\"fldMode\" value='"+fldMode+"'/>");
			paramBuffer.append("<input type=\"hidden\" name=\"showPanel\" value='"+showPanel+"'/>");
			paramBuffer.append("<input type=\"hidden\" name=\"submissionType\" value='"+submissionType+"'/>");
			paramBuffer.append("<input type=\"hidden\" name=\"formCategory\" value='"+formCategory+"'/>");
			if ("true".equals(request.getParameter("irbReviewForm"))) { 
		 	paramBuffer.append("<input type=\"hidden\" name=\"irbReviewForm\" value=\"true\">");
    		
    	  }  
			paramBuffer.append("<br><table><tr><td>Form Version Number: "+formLibVerNumber+"</td></tr></table>");
			paramBuffer.append("<input type=\"hidden\" name=\"outputTarget\" value='"+outputTarget+"'/>");
			paramBuffer.append("<input type=\"hidden\" name=\"specimenPk\" value='"+specimenPk+"'/>");
			paramBuffer.append("<input type=\"hidden\" name=\"eSignRequired\" value='"+eSignRequired+"'/>");


			if (!mode.equals("N"))
		    {
		    	paramBuffer.append("<input type=\"hidden\" name=\"formFilledFormId\" value='"+filledFormId+"'/>");
		    	paramBuffer.append("<input type=\"hidden\" name=\"formLibVer\" value='"+formLibVer+"'/>");

			}
			//also append the paramBuffer
		   sbuffer.replace(pos,pos+len,"<input type=\"hidden\" name=\"mode\" value='"+mode+"'/> <input type=\"hidden\" name=\"chkexists\" value='"+chkexists+"'/>"+paramBuffer.toString()+" </Form>");

		   //sbuffer.replace(pos,pos+len,"<input type=\"hidden\" name=\"mode\" value='"+mode+"'/>  </Form>");
		   formHtml = sbuffer.toString();
		}
 	%>
<% 
    /* String str="id=\"er_def_date_01\"";
     System.out.println("str-"+str);
     int indexof=formHtml.indexOf("id=\"er_def_date_01\"");
     System.out.println("str-"+indexof);
     String str1=formHtml.substring(indexof+str.length(),formHtml.length()-1);
     System.out.println("str-"+str1);
     int indexof1=str1.indexOf(">");
     System.out.println("index of-"+indexof);
     String str2=formHtml.substring(0,indexof1+indexof+str.length()+1);
     String str3=formHtml.substring(str2.length(),formHtml.length()-1);
     System.out.println("str2-"+str3);
     StringBuffer sb=new StringBuffer(str2);
     sb.append("<span class=\"queryStatusFlagDlg\"><img src=\"./images/redflag.jpg\"></span>");
     
     System.out.println("str2-"+str2);
     formHtml=sb.toString()+str3;*/
     %>
   
    <!--  To check whether Its jupiter Screen or not?
          Put Span in every field.
          Date:24th April 2015
          Created By:Bindu.
      -->
     

     <% 

     if("jupiter".equals(dashBoard))
     {
    	FormFieldDao fldDao=new  FormFieldDao();
    	fldDao.getFormfieldDetails(formId);
    	ArrayList formFieldIds=fldDao.getFormFldId();
        ArrayList fldSystemId= fldDao.getFldSystemId();
        ArrayList formFldLinesNos=fldDao.getFormFldLinesNos();
    	ArrayList formFldDataType=fldDao.getFormFldDataType();
    	CodeDao cd2 = new CodeDao();
     	cd2.getCodeValues("form_query");
     	ArrayList subType=new ArrayList();
     	ArrayList subDesc=new ArrayList();
     	subType=cd2.getCSubType();
     	subDesc=cd2.getCDesc();
     	JSONObject formQueryJson = new JSONObject();
     	JSONObject formStatusJson = new JSONObject();
     	for(int i=0;i<cd2.getCRows();i++){
     		formQueryJson.put(subType.get(i).toString(),subDesc.get(i));	
     	}
     	CodeDao cd = new CodeDao();
     	cd.getCodeValues("query_status");
     	subType=cd.getCSubType();
     	subDesc=cd.getCDesc();
     	for(int i=0;i<cd.getCRows();i++){
     		formStatusJson.put(subType.get(i).toString(),subDesc.get(i));	
     	}
     	FormQueryDao fqDao=null;
    	for(int i=0;i<fldSystemId.size();i++){
    		int findField=formHtml.indexOf(fldSystemId.get(i).toString());
         	if(findField>=0){//in case of you delete the field from form
    	String strId="id=\"";
    	strId=strId+fldSystemId.get(i).toString();
     	String formFldDType=formFldDataType.get(i)==null?"":formFldDataType.get(i).toString();
     	int linesNo=formFldLinesNos.get(i)==null?0:Integer.parseInt(formFldLinesNos.get(i).toString());   	 	
    	
    	if("MR".equals(formFldDType))
     	{
     		strId=strId+"_span";
     		int start=formHtml.indexOf(strId);
     		if(start>-1){
    		int clospan=formHtml.indexOf("<tr>",start);
    		int firstclosetd=formHtml.lastIndexOf("</td>",clospan);
     		String str4=formHtml.substring(0,firstclosetd);
     		String str5=formHtml.substring(firstclosetd,formHtml.length());
     		String rep="&nbsp;23153Flg";
     		formHtml=str4+rep+str5;	
     		}
     	}
     	
     	if("MC".equals(formFldDType))
     	{
     		//strId=strId+"_id";
     		strId=strId+"_span";
     		int start=formHtml.indexOf(strId);
     		if(start>-1){
    		int clospan=formHtml.indexOf("<tr>",start);
    		int firstclosetd=formHtml.lastIndexOf("</td>",clospan);
     		String str4=formHtml.substring(0,firstclosetd);
     		String str5=formHtml.substring(firstclosetd,formHtml.length());
     		String rep="&nbsp;23153Flg";
     		formHtml=str4+rep+str5;
     		}
     	}
    	 	
    	strId=strId+"\"";
        int indexof=formHtml.indexOf(strId);
        String str1=formHtml.substring(indexof+strId.length(),formHtml.length());
        String indxCen="";
        int indexof1=0;
        if(formFldDType!=""){
        if("MD".equals(formFldDType)){
        	indxCen="</select>";
       	 indexof1=str1.indexOf(indxCen);
       }else if("MR".equals(formFldDType)){
    	   indxCen="</span>";
    	   indexof1=str1.indexOf(indxCen);
       }else if("MC".equals(formFldDType)){
    	   indxCen="</label>";
    	   indexof1=str1.indexOf(indxCen);
       }
       else if("ET".equals(formFldDType)&& linesNo>1){
    	   indxCen="</textarea>";
    	   indexof1=str1.indexOf(indxCen);
       }
        else{
    	    indxCen=">";
         indexof1=str1.indexOf(indxCen);
       }
        }
        String str2=formHtml.substring(0,indexof1+indexof+strId.length()+indxCen.length());
        String str3=formHtml.substring(str2.length(),formHtml.length());
        StringBuffer sb=new StringBuffer(str2);
        String statusFlag="";
            	
    	int studyforms=2;
    	fqDao=new FormQueryDao();
    	fqDao.getQueryTypeAndStatus(formFieldIds.get(i).toString(),StringUtil.stringToNum(request.getParameter("filledFormId")),studyforms);
    	ArrayList queryTypes=fqDao.getQueryType();
    	ArrayList queryStatus=fqDao.getQueryStatus();
    	ArrayList formqueryIds=fqDao.getFormQueryIds();
    	ArrayList queryFieldIds=fqDao.getFieldIds();
    	
    	String queryStatusStr="";
    	String queryTypeStr="";	
    	int redflag=0,orangeflag=0,yellowflag=0,blueflag=0,greenflag=0;
    	
    	if(queryFieldIds.size()>0){
    		
    		for(int count=0;count<queryTypes.size();count++){
    				
    				if((formStatusJson.get("open").toString().equalsIgnoreCase(queryStatus.get(count).toString())||formStatusJson.get("re-opened").toString().equalsIgnoreCase(queryStatus.get(count).toString())) && queryTypes.get(count).toString().equalsIgnoreCase(formQueryJson.get("high_priority").toString())){
    					redflag=count+1;
    					}
    				else if(formStatusJson.get("mon_rev").toString().equalsIgnoreCase(queryStatus.get(count).toString()) && formQueryJson.get("high_priority").toString().equalsIgnoreCase(queryTypes.get(count).toString())){
    					orangeflag=count+1;
    					}
    				else if(formStatusJson.get("mon_rev").toString().equalsIgnoreCase(queryStatus.get(count).toString()) && formQueryJson.get("normal").toString().equalsIgnoreCase(queryTypes.get(count).toString())){
    					yellowflag=count+1;
    					}
    				else if((formStatusJson.get("open").toString().equalsIgnoreCase(queryStatus.get(count).toString())|| formStatusJson.get("re-opened").toString().equalsIgnoreCase(queryStatus.get(count).toString())) && queryTypes.get(count).toString().equalsIgnoreCase(formQueryJson.get("normal").toString())){
    					blueflag=count+1;
    					}
    				else if(formStatusJson.get("resolved").toString().equalsIgnoreCase(queryStatus.get(count).toString())){
    					greenflag=count+1;
    					}
    			}
    				
    				if(redflag!=0){
    					queryTypeStr=queryTypes.get(redflag-1).toString();
    					queryStatusStr=queryStatus.get(redflag-1).toString();
    		    	}
    				else if(redflag==0 && orangeflag!=0){
    		    		queryTypeStr=queryTypes.get(orangeflag-1).toString();
    					queryStatusStr=queryStatus.get(orangeflag-1).toString();
    		    	}
    				else if(redflag==0 && orangeflag==0 && yellowflag!=0){
    		    		queryTypeStr=queryTypes.get(yellowflag-1).toString();
    					queryStatusStr=queryStatus.get(yellowflag-1).toString();
    		    	}
    				else if(redflag==0 && orangeflag==0 && yellowflag==0 && blueflag!=0){
    		    		queryTypeStr=queryTypes.get(blueflag-1).toString();
    					queryStatusStr=queryStatus.get(blueflag-1).toString();
    		    	}
    				else if(redflag==0 && orangeflag==0 && yellowflag==0 && blueflag==0 && greenflag!=0){
    		    		queryTypeStr=queryTypes.get(greenflag-1).toString();
    					queryStatusStr=queryStatus.get(greenflag-1).toString();
    		    	}
     		
        	if(formFieldIds.get(i).equals(queryFieldIds.get(0))){
        		if((formStatusJson.get("open").toString().equalsIgnoreCase(queryStatusStr)||formStatusJson.get("re-opened").toString().equalsIgnoreCase(queryStatusStr))&& formQueryJson.get("high_priority").toString().equalsIgnoreCase(queryTypeStr)){
        			if(study_acc_form_right>5 && study_team_form_access_right>5)
        			{
        			statusFlag="<span class='queryStatusFlagDlg' id=\""+fldSystemId.get(i)+"[Velsep]"+formqueryIds.get(0)+"\"><img border='0'onclick='openQueryDialog(\""+fldSystemId.get(i).toString()+"\",\""+studyId+"\")' src='./images/redflag.jpg'></span>";
        		}
        			else
        			{statusFlag="<span class='queryStatusFlagDlg' id=\""+fldSystemId.get(i)+"[Velsep]"+formqueryIds.get(0)+"\"><img border='0' src='./images/redflag.jpg'></span>";}
        		}else if(formStatusJson.get("mon_rev").toString().equalsIgnoreCase(queryStatusStr)&&formQueryJson.get("normal").toString().equalsIgnoreCase(queryTypeStr)){
        			if(study_acc_form_right>5 && study_team_form_access_right>5)
        			{
        			statusFlag="<span class='queryStatusFlagDlg' id=\""+fldSystemId.get(i)+"[Velsep]"+formqueryIds.get(0)+"\"><img border='0'onclick='openQueryDialog(\""+fldSystemId.get(i).toString()+"\",\""+studyId+"\")' src='./images/yellowflg.jpg'></span>";
        			}
        			else
        			{
        				statusFlag="<span class='queryStatusFlagDlg' id=\""+fldSystemId.get(i)+"[Velsep]"+formqueryIds.get(0)+"\"><img border='0' src='./images/yellowflg.jpg'></span>";
        			}
        		}else if(formStatusJson.get("mon_rev").toString().equalsIgnoreCase(queryStatusStr)&&formQueryJson.get("high_priority").toString().equalsIgnoreCase(queryTypeStr)){
        			if(study_acc_form_right>5 && study_team_form_access_right>5)
        			{
        			statusFlag="<span class='queryStatusFlagDlg' id=\""+fldSystemId.get(i)+"[Velsep]"+formqueryIds.get(0)+"\"><img border='0'onclick='openQueryDialog(\""+fldSystemId.get(i).toString()+"\",\""+studyId+"\")' src='./images/orangflg.jpg'></span>";
        			}
        			else
        			{
        				statusFlag="<span class='queryStatusFlagDlg' id=\""+fldSystemId.get(i)+"[Velsep]"+formqueryIds.get(0)+"\"><img border='0' src='./images/orangflg.jpg'></span>";
        			}
        		}else if((formStatusJson.get("open").toString().equalsIgnoreCase(queryStatusStr)||formStatusJson.get("re-opened").toString().equalsIgnoreCase(queryStatusStr)) && formQueryJson.get("normal").toString().equalsIgnoreCase(queryTypeStr)){
        			if(study_acc_form_right>5 && study_team_form_access_right>5)
        			{
        			statusFlag="<span class='queryStatusFlagDlg' id=\""+fldSystemId.get(i)+"[Velsep]"+formqueryIds.get(0)+"\"><img border='0'onclick='openQueryDialog(\""+fldSystemId.get(i).toString()+"\",\""+studyId+"\")' src='./images/purplflg.jpg'></span>";
        		}
        			else{
        				statusFlag="<span class='queryStatusFlagDlg' id=\""+fldSystemId.get(i)+"[Velsep]"+formqueryIds.get(0)+"\"><img border='0' src='./images/purplflg.jpg'></span>";
        				}
        			}
        		}else if(formStatusJson.get("resolved").toString().equalsIgnoreCase(queryStatusStr)){
        			if(study_acc_form_right>5 && study_team_form_access_right>5)
        			{
        			statusFlag="<span class='queryStatusFlagDlg' id=\""+fldSystemId.get(i)+"[Velsep]"+formqueryIds.get(0)+"\"><img border='0'onclick='openQueryDialog(\""+fldSystemId.get(i).toString()+"\",\""+studyId+"\")' src='./images/greenflg.jpg'></span>";
        		}
        			else
        			{
        				statusFlag="<span class='queryStatusFlagDlg' id=\""+fldSystemId.get(i)+"[Velsep]"+formqueryIds.get(0)+"\"><img border='0' src='./images/greenflg.jpg'></span>";
        			}
        	}
       
    	}
        if(statusFlag.equals("") && formFldDType!=""){
        	if(study_acc_form_right>5 && study_team_form_access_right>5)
			{
        	statusFlag="<span class='queryStatusFlagDlg'><img border='0'onclick='openQueryDialog(\""+fldSystemId.get(i).toString()+"\",\""+studyId+"\",\"queryWin\")' src='./images/white.jpg'></span>";
        }
        	else
        	{
        		statusFlag="<span class='queryStatusFlagDlg'><img border='0' src='./images/white.jpg'></span>";	
        	}
        }
        if((!"MR".equals(formFldDType))&&(!"MC".equals(formFldDType)))
     	{
     		sb.append(statusFlag);
     	}
       // sb.append("<span class='queryStatusFlagDlg'><img border='0'onclick='openQueryDialog(\""+fldSystemId.get(i).toString()+"\","+"\""+studyId+"\")' src='./images/redflag.jpg'></span>");
       formHtml=sb.toString()+str3;
       formHtml =  formHtml.replace("23153Flg",statusFlag);
        }
    	}
     //  formHtml = formHtml.replaceAll("<tr></tr>" ,"");
      //formHtml = formHtml.replaceAll("(\\r|\\n|\\r\\n)+", "");
     
     //  formHtml = formHtml.replaceAll("<span>" ,"<span style='float:left;'>");
     
   // formHtml = formHtml.replaceAll("</span></td></tr>" ,"<span class='queryStatusFlagDlg'><img border='0' src='./images/redflag.jpg'></span></td></tr>");
    	
			String stdyid=request.getParameter("studyId");
			String p_key=request.getParameter("pkey");
			String frmid=request.getParameter("formId");
			String frmlibver=request.getParameter("formLibVer");
			String filldfrmid=request.getParameter("filledFormId");
			String frmpulldwn=request.getParameter("formPullDown");
			String entrychr=request.getParameter("entryChar");
			String statDesc=request.getParameter("statDesc");
			String statid=request.getParameter("statid");
			String ptprotId=request.getParameter("patProtId");
			String patientCde=request.getParameter("patientCode");
			String patstdyid = request.getParameter("patStudyId");

   			int formEndTagIndex=formHtml.indexOf("</Form>");
    	  	String htmlBeforeFormEndTag="";
    	   	String htmlAfterFormEndTag="";
    	    htmlBeforeFormEndTag=formHtml.substring(0,formEndTagIndex);
    	    htmlAfterFormEndTag=formHtml.substring(formEndTagIndex,formHtml.length());
    	    String hiddenStr="";
    	    hiddenStr=hiddenStr.concat("<input name='dashBoard' type='hidden' value='").concat(dashBoard+"'>");
    	    hiddenStr=hiddenStr.concat("<input name='patStdyId' type='hidden' value='").concat(patstdyid+"'>");
    	    hiddenStr=hiddenStr.concat("<input name='StdyId' type='hidden' value='").concat(stdyid+"'>");
    	    hiddenStr=hiddenStr.concat("<input name='pKey' type='hidden' value='").concat(p_key+"'>");
    	    hiddenStr=hiddenStr.concat("<input name='frmId' type='hidden' value='").concat(frmid+"'>");
    	    hiddenStr=hiddenStr.concat("<input name='frmLibVer' type='hidden' value='").concat(frmlibver+"'>");
    	    hiddenStr=hiddenStr.concat("<input name='fillFrmId' type='hidden' value='").concat(filldfrmid+"'>");
    	    hiddenStr=hiddenStr.concat("<input name='frmPulldwn' type='hidden' value='").concat(frmpulldwn+"'>");
    	    hiddenStr=hiddenStr.concat("<input name='entryChr' type='hidden' value='").concat(entrychr+"'>");
    	    hiddenStr=hiddenStr.concat("<input name='patPrtId' type='hidden' value='").concat(ptprotId+"'>");
  
    	    formHtml=htmlBeforeFormEndTag+hiddenStr+htmlAfterFormEndTag;
     }
    
     %>
     <%if(saveDao.isMonitor()){
	  if("jupiter".equals(dashBoard)){
		  formHtml=formHtml.replaceAll("hasDatepicker","");
		  teamRole="role_monit";
	  }
  }%>
  <%=formHtml%>
 <%

	} //end of if body for page right

	else
	{
%>
    <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

  	} //end of else body for page right

  } //end of if for session

 else
 {  //else of if body for session
  %>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
 %>
<br>
<div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
<%if (calledFromForm.equals("")&& showPanel.equals("true") )
   {
%>

<div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>


<%
	}
if ("true".equals(request.getParameter("irbReviewForm"))) { %>
<style>
Table.dynFormTable TD{
padding:0px 0px 0px 0px;
}
</style>
<%} %>
<script>
elementOverride=(document.getElementById("override_count"));
if (!(typeof(elementOverride)=="undefined") && (elementOverride!=null) ) {
if ((document.getElementById("override_count").value)>0) setDisableSubmit('false');
}
linkFormSubmit('fillform');</script>

<%
if ( eSignRequired.equals("0") )
{
  %>
  	<script>
	  	elemeSign = (document.getElementById("eSign"));
		elemeSignLabel = (document.getElementById("eSignLabel"));

		if (!(typeof(elemeSign)=="undefined") && (elemeSign!=null) ) {
			elemeSign.style.visibility = "hidden";
			elemeSign.value="11";
		}

		if (!(typeof(elemeSignLabel)=="undefined") && (elemeSignLabel!=null) ) {

			elemeSignLabel.style.visibility = "hidden";
		}

		
	</script>
  <%

}
%>
   


<div id="queryStatusDialog" style="display: none">

	<div id='queryStatusDialogHtml'></div>
</div>

<script type="text/javascript">
var jq = jQuery.noConflict();
jq("#queryStatusDialog").dialog({
		modal:true,
	      autoOpen: false,
	      width : '410',
		  height:'175'    
      });
	
	function openQueryDialog(fieldSysId,studyId,windowType){
		jq.ajax({	  
	   		url: "fieldQueryHistory.jsp?filedId="+fieldSysId+"&windowType="+windowType+"&studyId="+studyId+"&responseId="+"<%=filledFormId%>",   		
			success: function(data) {	
	   			jq('#queryStatusDialog').dialog( "open" );			
	   			jq("#queryStatusDialogHtml").html(data);			
	   			jq('#queryStatusDialog').dialog( "open" );		
			}
					
	    	});
	}
	function saveQuery(fieldSysId){
		var fieldId='';
		var responseId="<%=filledFormId%>";
		var queryStatus=jq("#cmbStatus").val();
		var queryTypeId=jq("#cmbQueryResp").val();
		var instructions=jq("#instructions").val();
		var index=fieldSysId.indexOf("er_def_date_01");	
		if(index>=0){
			jq.ajax({
				 url: '/velos/jsp/findFieldDetails',
				 type: 'GET',
			     global: true,
			     dataType: 'json',
			     async: false,
			     data:{
				 'fieldId':fieldSysId,
				 'filledFormId':responseId,
				 'formType':'S'
			     },
			     cache: false,
			     success: function (data) {
			    	 fieldId=data[0].er_def_date_01;
			     },
			     error: function (){
				        alert("error");
			        }
			});	
			
		}else{
		var fieldIdSegmentArr=fieldSysId.split('_');
		fieldId=fieldIdSegmentArr[2];
		}
		var formQueryType=2;
		var calledFrom=2;
		
		if(queryStatus==''){
			alert("Select Status");
		}
		else if(queryTypeId==''){
			alert("Select Query Type");
		}else{
			jq.ajax({
			url: '/velos/jsp/patStudyQuerySave',
		    type: 'GET',
	        global: true,
	        dataType: 'json',
	        async: false,
	        data: {
	        'fieldId':fieldId,
	        'formQueryType':formQueryType,
	        'calledFrom':calledFrom,
	        'filledFormId':responseId,
	        'queryStatus':queryStatus,
	        'queryTypeId':queryTypeId,
	        'instructions':instructions
		},
		cache: true,
        success: function (data) {
	       
			jq('#queryStatusDialog').dialog('close');  
		        location.reload();
		        parent.preview1.location.reload();
        },
        error: function (){
	        alert("error");
        }
		});
		}
	}
function saveQueryAndResponse(fieldSysId){
	var fieldStr='';
	var formQueryId='';
	var formQueryType=3;
	var instructions='';
	formQueryId =jQuery("input:radio[name=queryRadio]:checked").val();
	var queryStatus=jq("#cmbStatus").val();
	instructions=jq("#instructions").val();
	if(queryStatus!=''){
		if(formQueryId!=undefined){	
			jq.ajax({
		    url: '/velos/jsp/saveQueryResponse',
		    type: 'GET',
	        global: true,
	        dataType: 'json',
	        async: false,
	        data: {
		    'formQueryId':formQueryId,
		    'queryStatus':queryStatus,
		    'formQueryType':formQueryType,
		    'instructions':instructions
	        },
	        cache: true,
	        success: function (data) {
		        if(data[0].rowImpact==0){
			        alert("data  not Saved Successfully");
		        }else{
		        	jq('#queryStatusDialog').dialog('close');  
			        location.reload();
			        parent.preview1.location.reload();
		        }
	        },
	        error: function (){
		        alert("error");
	        }
	});
		}else{alert("Select Any one Selection");}
	}else{
		alert("Select Status");
	}
}
function formRespFldReadOnly(){
	jq('#fillform input').attr('readonly', 'readonly');
	jq('#eSign').attr('readonly', false);
	jq(":radio").on("click", function(){
		  return false;
		});
	jq(":checkbox").on("click", function(){
		  return false;
		});
	jq('input[type="text"], textarea').attr('readonly','readonly');

	jq("#fillform").find("select").each(function (i) {
		var selectId=jq(this).attr('id');
		if(selectId !='er_def_formstat'){
			jq(this).prop('disabled','disabled');
		}
		
	});
 }

if('<%=teamRole%>'=='role_monit'){
	formRespFldReadOnly();	
}
	

</script>
</body>



</html>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>
