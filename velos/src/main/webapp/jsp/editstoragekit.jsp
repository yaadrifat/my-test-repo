<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<%
String mode= "";
mode = request.getParameter("mode");

 String dProcSteps="";
 String dStorageCateg = "", dStorType="", dSpecimenType="", dSpecQuantUnit="",  dDisposition="" ,dStoreChildType="";
 CodeDao cdSpecProcSteps = new CodeDao();//Processing steps
 cdSpecProcSteps.getCodeValues("spec_proctype");
 dProcSteps = cdSpecProcSteps.toPullDown("processingsteps");

 CodeDao cdStorageType = new CodeDao();//storage Type
 cdStorageType.getCodeValuesSaveKit("store_type");
 dStorType = cdStorageType.toPullDown("storageType");

 CodeDao cdSpecType = new CodeDao();//Specimen Type
 cdSpecType.getCodeValues("specimen_type");
 dSpecimenType = cdSpecType.toPullDown("specimentype");

 CodeDao cdSpecQntUnit = new CodeDao();//Specimen Quantity unit
 cdSpecQntUnit.getCodeValues("spec_q_unit");
 dSpecQuantUnit = cdSpecQntUnit.toPullDown("specimenqunit");

 CodeDao cdDisposition = new CodeDao();//Disposition
 cdDisposition.getCodeValues("specdisposition");
 dDisposition = cdDisposition.toPullDown("disposition");

 //JM: 09Oct2009: #4311: to fetch the Enviornmental Constraints in Storage Kit Details from the er_codelst table
 CodeDao cdEcons = new CodeDao();
 cdEcons.getCodeValues("envt_cons");
 ArrayList codelstDescs=cdEcons.getCDesc();
 String codelstdesc="";
 int length=codelstDescs.size();
 String genStatus = "";

%>

<head>
	<title> <%=MC.M_MngInv_StrgKitDets%><%--Manage Inventory >> Storage Kit Details*****--%></title>
	 <SCRIPT LANGUAGE = "JavaScript">
		var screenWidth = screen.width;
		var screenHeight = screen.height;
	    var specimenValues ;
	    var storageValues ;

		function validateName(storageName)
		{
			if(storageName.value == ''||storageName.value==null )
			{
				alert("<%=MC.M_StorName_CntBlank%>");/*alert("Storage Name cannot be left blank");*****/
				storageName.focus();
				return false;
			}
			if (isWhitespace(storageName.value))
			{
				alert("<%=MC.M_Etr_ValidData%>");/*alert("Please enter valid data");*****/
				storageName.focus();
				return false;

			}

		}




</SCRIPT>
 </head>
 <body  >
<SCRIPT language="javascript">

function  validate(formobj){

	 var  rowIds= formobj.rowId.value;

     if (!(validate_col('Storage Name',formobj.storageKitName))) return false;

     if (!(validate_col('Storage Kit Type',formobj.storeChildType))) return false;
     
// 	 if(isNaN(formobj.eSign.value) == true) {
<%-- 		 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/ --%>
// 		formobj.eSign.focus();
// 		return false;
//      }

	/* Bug#6320 10-Sep-2012 -Sudhir*/
	for(rw=0;rw<rowIds;rw++) {
		var kitCompNames = document.getElementsByName("kitCompName");			 
		if (!(validate_col('Storage Name',kitCompNames[rw]))) return false;

		 var strgTyp= document.getElementsByName("storageType");	
		if (!(validate_col('Storage Kit',strgTyp[rw]))) return false;

		var spcmnTyp= document.getElementsByName("specimentype");	
		if (!(validate_col('Specimen Type',spcmnTyp[rw]))) return false;
		
	}

   //if (!(validate_col('e-Signature',formobj.eSign))) return false;

      //KM-#5698
      var totalComp=0;
      if (typeof(formobj.kitCompName)!="undefined") //AK:Fixed Bug#6298 (24thMay2011)
	   totalComp = formobj.kitCompName.length;

      if (totalComp==1){

      	 
		  kit_comp_name = formobj.kitCompName.value;
	      kit_strType = formobj.storageType.value;
	      //Fixed bug 6030 mandatory validation for specimenType,BK
	      kit_strSpecimenType = formobj.specimentype.value;

	      kit_quantity = formobj.quantity.value;
		  kit_specimenqunit = formobj.specimenqunit.value;

	      if ((kit_comp_name !='' || kit_strType !='' || kit_strSpecimenType != '')) {

				if(kit_comp_name==''){alert("<%=MC.M_Etr_StorKitName%>")/*alert("Please enter Storage Kit Name")*****/; formobj.kitCompName.focus(); return false;}
				if(kit_strType==''){alert("<%=MC.M_Etr_StorKitType%>")/*alert("Please enter Storage Kit Type")*****/; formobj.storageType.focus();return false;}
				if(kit_strSpecimenType==''){alert("<%=MC.M_PlsEtr_SpmenType%>")/*alert("Please enter Specimen Type")*****/; formobj.specimentype.focus();return false;}
		  }
	      
	      if(kit_quantity!='' && kit_specimenqunit==''){
	    	  alert("<%=MC.M_Etr_StrKitQtyUnit%>")/*alert("Please enter Storage Kit Quantity Unit")*****/; formobj.specimenqunit.focus(); return false;
	      }

		  if ((kit_quantity !='' || kit_specimenqunit !='')) {

				if(kit_quantity==''){alert("<%=MC.M_Etr_StorKitQty%>")/*alert("Please enter Storage Kit Quantity")*****/; formobj.quantity.focus(); return false;}
				if((kit_comp_name !='' || kit_strType !='') && parseFloat(formobj.quantity.value)!= '0' && kit_specimenqunit==''){alert("<%=MC.M_Etr_StrKitQtyUnit%>")/*alert("Please enter Storage Kit Quantity Unit")*****/; formobj.specimenqunit.focus(); return false;}

		  }


      }else if (totalComp>1){

      for(i=0;i<totalComp;i++) {

		  kit_comp_name = formobj.kitCompName[i].value;
	      kit_strType = formobj.storageType[i].value;

	      kit_quantity = formobj.quantity[i].value;
		  kit_specimenqunit = formobj.specimenqunit[i].value;
		  kit_strSpecimenType = formobj.specimentype[i].value;		
	      if ((kit_comp_name !='' || kit_strType !='' || kit_strSpecimenType != '')) {

				if(kit_comp_name==''){alert("<%=MC.M_Etr_StorKitName%>")/*alert("Please enter Storage Kit Name")*****/; formobj.kitCompName[i].focus(); return false;}
				if(kit_strType==''){alert("<%=MC.M_Etr_StorKitType%>")/*alert("Please enter Storage Kit Type")*****/; formobj.storageType[i].focus();return false;}
				if(kit_strSpecimenType==''){alert("<%=MC.M_PlsEtr_SpmenType%>")/*alert("Please enter Specimen Type")*****/; formobj.specimentype[i].focus();return false;}
		  }
	      
	      if(kit_quantity!='' && kit_specimenqunit==''){
	    	  alert("<%=MC.M_Etr_StrKitQtyUnit%>")/*alert("Please enter Storage Kit Quantity Unit")*****/; formobj.specimenqunit[i].focus(); return false;
	      }

		  if ((kit_quantity !='' || kit_specimenqunit !='')) {

				if(kit_quantity==''){alert("<%=MC.M_Etr_StorKitQty%>")/*alert("Please enter Storage Kit Quantity")*****/; formobj.quantity[i].focus(); return false;}
				if((kit_comp_name !='' || kit_strType !='') && parseFloat(formobj.quantity[i].value)!= '0' && kit_specimenqunit==''){alert("<%=MC.M_Etr_StrKitQtyUnit%>")/*alert("Please enter Storage Kit Quantity Unit")*****/; formobj.specimenqunit[i].focus(); return false;}

		  }
      }
      }//totalComp>1
      
     if (!(validate_col('e-Signature',formobj.eSign))) return false;
      
     //Fixed by Ashu Bug No:5587
     var returnValue=quantityVallidation(formobj);
       return returnValue;


} //end of validate method


function indivChek(formobj){
 	var numCompRowExists = 0;
	numCompRowExists = formobj.totalExtgRows.value



	for (var i=0; i<numCompRowExists; i++){
	var strEnvtChkd="";
	var checkmed = "";

		if (numCompRowExists==1){
			if (formobj.checkme0.checked ==true){
		 	strEnvtChkd +="1";
			}else if (formobj.checkme0.checked ==false){
			strEnvtChkd +="0";
			}

			if (formobj.checkme1.checked ==true){
		 	strEnvtChkd +="1";
			}else if (formobj.checkme1.checked ==false){
			strEnvtChkd +="0";
			}

			if (formobj.checkme2.checked ==true){
		 	strEnvtChkd +="1";
			}else if (formobj.checkme2.checked ==false){
			strEnvtChkd +="0";
			}

			formobj.envtStringChecked.value = strEnvtChkd;
		}else{
			if (formobj.checkme0[i].checked ==true){
		 	strEnvtChkd +="1";
			}else if (formobj.checkme0[i].checked ==false){
			strEnvtChkd +="0";
			}

			if (formobj.checkme1[i].checked ==true){
		 	strEnvtChkd +="1";
			}else if (formobj.checkme1[i].checked ==false){
			strEnvtChkd +="0";
			}

			if (formobj.checkme2[i].checked ==true){
		 	strEnvtChkd +="1";
			}else if (formobj.checkme2[i].checked ==false){
			strEnvtChkd +="0";
			}

			formobj.envtStringChecked[i].value = strEnvtChkd;

		}
	}

return true;

}

function fnChk(formobj){

 	var numCompRowExists = 0;
	numCompRowExists = formobj.totalExtgRows.value


	for (var i=0; i<numCompRowExists; i++){

	 	if (numCompRowExists==1){
		 	if (formobj.checkCopyChild.checked){
		 		formobj.checkCopyChildStr.value = "1";
			}else{
				formobj.checkCopyChildStr.value = "0";
			}

	 	}else{
		 	if (formobj.checkCopyChild[i].checked){
		 		formobj.checkCopyChildStr[i].value = "1";
			}else{
				formobj.checkCopyChildStr[i].value = "0";
			}

	 	}
	}
}


function fnAddDropDown(formobj, turn){



	var numCompRowExists = 0;
	numCompRowExists = formobj.totalExtgRows.value

	var baseRowNum = 0;
	baseRowNum = formobj.baseOrigRows.value


	var addedNum = 0;
	if (numCompRowExists==1){
		addedNum = formobj.numDropDownPerRow.value
	}else{
		addedNum = formobj.numDropDownPerRow[turn].value
	}

    //for (var f=0; f<numCompRowExists; f++){


	    var myRow = eval("document.getElementById(\"myTable"+turn+"\").insertRow(addedNum)");
	    var myCell = myRow.insertCell(0);
		myCell.innerHTML = "<%=dProcSteps%>";
		myCell = myRow.insertCell(1);
		myCell.innerHTML = "<input type='text' name='sequence' size='3'/>";

		addedNum ++
		if (numCompRowExists==1){
			formobj.numDropDownPerRow.value=addedNum;
		}else{
			formobj.numDropDownPerRow[turn].value=addedNum;
		}
		//break;
	//}

}





function fnAddMoreComponentRows(formobj, exits){

	/* Bug#6320 10-Sep-2012 -Sudhir*/
	var rowId = formobj.rowId.value;

//how many new row do you want to add in the existing rows of the table
	var newAddCount = formobj.addmore.value
	var numCompRowExists = 0;
	//KM-#5698
	if (typeof(formobj.kitCompName)!="undefined") //AK:Fixed Bug#6264 (18thMay2011)
	numCompRowExists =	formobj.kitCompName.length;
	
	for (var f=0; f<newAddCount; f++){

	    //cTurn is component turn used for the hidden check boxes for deletion operation
	    var cTurn= (numCompRowExists+eval((formobj.generalCnt.value))+2)

	    var myRow = document.getElementById("myMainTable").insertRow(-1);  // #6267 @Ankit May-18-2011

	    var myCell = myRow.insertCell(0);
		myCell.innerHTML = '<tr id="myiRow'+numCompRowExists+'" > <td><input type="text" name="kitCompName" value="" size="10"/></td>';
	    //myCell.innerHTML = "<tr id='myiRow"+numCompRowExists+"' > <td><input type='text' name='kitCompName' value='' size='10'/></td>";

		myCell = myRow.insertCell(1);
		myCell.innerHTML = "<td class=tdDefault><%=dStorType%></td>";
		myCell = myRow.insertCell(2);
		myCell.innerHTML = "<td class=tdDefault><%=dSpecimenType%></td>";
		myCell = myRow.insertCell(3);
		myCell.innerHTML = "<td><input type='text' name='quantity' value='' size='05'/></td>";

		myCell = myRow.insertCell(4);
		myCell.innerHTML = "<td class=tdDefault><%=dSpecQuantUnit%></td>";

		myCell = myRow.insertCell(5);
		myCell.innerHTML = '<td><table>'
		+ '	<tr><td>&nbsp;&nbsp;'
		+ '	<a href=# onClick="fnAddDropDown(document.storagekit, ('+numCompRowExists+'));"><%=LC.L_Add_More%><%--Add More*****--%></a>'
		+ '	</td></tr>'
		+ '	</table>'
		+ '	<table id="myTable'+numCompRowExists+'" border="0">'
		+ '	<tr><td class="tdDefault">'+"<%=dProcSteps%>"+'</td>'
		+ '	<td><input type="text" name="sequence" value="1" size="03"></td>'
		+ '	</tr>'
		+ '	<tr><td class="tdDefault">'+"<%=dProcSteps%>"+'</td>'
		+ '	<td><input type="text" name="sequence" value="2" size="03"></td>'
		+ '	</tr></table>'
		+ '	</td><input type="hidden" name="numDropDownPerRow" value="2">';

		myCell = myRow.insertCell(6);
		myCell.innerHTML = "<td class=tdDefault><%=dDisposition%></td>";
		myCell = myRow.insertCell(7);
		myCell.innerHTML =
		"   <td><input type='checkbox' name='checkme0' onClick='indivChek(document.storagekit)'>"+'<%=(String)codelstDescs.get(0)%>'+" <br>"
		+ " <input type='checkbox' name='checkme1' onClick='indivChek(document.storagekit)'>"+'<%=(String)codelstDescs.get(1)%>'+" <br>"
		+ " <input type='checkbox' name='checkme2' onClick='indivChek(document.storagekit)'>"+'<%=(String)codelstDescs.get(2)%>'+"  <br></td>"
		+ " <input type='hidden' name='envtStringChecked'>";


		myCell = myRow.insertCell(8);
		myCell.innerHTML = "<td><input type='text' name='numSpecChildren' value='0' size='03'/></td>";
		myCell = myRow.insertCell(9);
		//Fixed by Ashu Bug No:5584
		myCell.innerHTML = "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"



		myCell = myRow.insertCell(10);
		myCell.innerHTML = '<td width="2%"><A href="#" onClick ="return fnDeleteRowComponentRows(document.storagekit,  '+(numCompRowExists+eval((formobj.generalCnt.value))+1)+')"><img src="./images/delete.gif" border="0" align="left"/></A></td></tr>';
		myCell.align='center';

		myCell = myRow.insertCell(11);
		var element1 = document.createElement("input");
    	element1.type = "checkbox";
    	element1.name="hiddenChk";
    	element1.id="hiddenChk_"+cTurn;
		element1.style.display='none';
    	myCell.appendChild(element1);

		numCompRowExists++
		formobj.totalExtgRows.value = numCompRowExists;
		/* Bug#6320 10-Sep-2012 -Sudhir*/
		rowId++;
	}
	/* Bug#6320 10-Sep-2012 -Sudhir*/
	formobj.rowId.value =rowId;
}

//Fixed by Ashu Bug No:5587
//Added method for quantity validation while editing the storage kit.
function quantityVallidation(formobj){
 	var numCompRowExists = 0;
 	var rowCount=0;
 	numCompRowExists = formobj.totalExtgRows.value;
 	if(numCompRowExists==1){ //AK: Fixed BUG#6266 (May 24,2011)
 	 	if((formobj.childCount!=null) && (formobj.childCount)!="undefined"){ 
 		childrenCount =parseInt(formobj.childCount.value);
 		var temQty=0.0;
		var totalQty=0.0;
			temQty=parseFloat(formobj.quantity.value);
			kit_comp_name = formobj.kitCompName.value;
		    if(childrenCount>0){
			    var loopCount=rowCount+childrenCount+1;
		    	for(k=rowCount+1;k<loopCount;k++){
		    		totalQty=totalQty+ parseFloat(formobj.quantity[k].value);
		        }
		    	rowCount=rowCount+childrenCount;
		    	if(totalQty>temQty){
		    			var paramArray = [kit_comp_name];
		    			alert(getLocalizedMessageString("M_TotQtyChildComp_LtEqComp",paramArray));/*alert ("Total quantity of children components should be less than or equal to '"+kit_comp_name+"' component.");*****/
				    	return false;
			   	}
        	}
 	 	}
 	}
 	else if(parseInt(numCompRowExists)>1){
		for(rowCount=0;rowCount<numCompRowExists;rowCount++) {
			if((formobj.childCount[rowCount])!=null && (formobj.childCount[rowCount])!="undefined"){ //AK: Fixed BUG#6266 (May 17,2011)
				childrenCount =parseInt(formobj.childCount[rowCount].value);
				var temQty=0.0;
				var totalQty=0.0;
					temQty=parseFloat(formobj.quantity[rowCount].value);
					kit_comp_name = formobj.kitCompName[rowCount].value;
				    if(childrenCount>0){
					    var loopCount=rowCount+childrenCount+1;
				    	for(k=rowCount+1;k<loopCount;k++){
				    		totalQty=totalQty+ parseFloat(formobj.quantity[k].value);
				        }
				    	rowCount=rowCount+childrenCount;
				    	if(totalQty>temQty){
				    		var paramArray = [kit_comp_name];
			    			alert(getLocalizedMessageString("M_TotQtyChildComp_LtEqComp",paramArray));/*alert ("Total quantity of children components should be less than or equal to '"+kit_comp_name+"' component.");*****/
						    	return false;
					   	}
			        }		    	
			 }
		}
 	}
	return true;
}		



function fnDeleteRowComponentRows(formobj, turn){

	var numCompRowExists = 0;
	numCompRowExists = formobj.totalExtgRows.value


	turn = turn+1;
	document.getElementById('hiddenChk_'+turn).checked='true';

	/* Bug#6320 10-Sep-2012 -Sudhir*/
	var rowId = formobj.rowId.value ;

	var inIndex = eval(formobj.baseOrigRows.value)+ eval(formobj.generalCnt.value)+1


	var delSelected = 0;
	 try {
            var table = document.getElementById("myMainTable");
            var rowCount = table.rows.length;


            for(var i=inIndex; i<rowCount; i++) {



                var row = table.rows[i];
                var chkbox = row.cells[11].childNodes[0];

                if(null != chkbox && true == chkbox.checked ) {
               	//alert("hare rama hare rama rama rama hare hare hare krishna hare krishan kishna krishna hare hare")

                    table.deleteRow(i);
                    rowCount--;
					/* Bug#6320 10-Sep-2012 -Sudhir*/
					rowId = rowCount;
					rowId--;
                    i--;
                    numCompRowExists--;
					formobj.totalExtgRows.value = numCompRowExists;
					delSelected=1;

                }


	    	}
			/* Bug#6320 10-Sep-2012 -Sudhir*/
			formobj.rowId.value = rowId;
	 }catch(e) {
	        alert(e);
	 }

}//end fn




//Added By Bikash: Function to set attribute of the first Child to all child of a particular component
//Oct 2010
	function updateAll(formobj,childCount,rowCount,z){	
		var loopCount = rowCount + childCount - 1;
		var firstValue = rowCount - 1;
	//getting the value of first child	
		tempIdsStOrages = formobj.idsStOrages[firstValue].value;
		tempIdsKits = formobj.idsKits[firstValue].value;
		tempStorageType = formobj.storageType[firstValue].value;
		tempSpecimentype = formobj.specimentype[firstValue].value;
		tempQuantity = formobj.quantity[firstValue].value;
		//Fixed by Ashu Bug No:5585 
		tempEnvtStringChecked = formobj.envtStringChecked[firstValue].value;
		tempSpecimenqunit = formobj.specimenqunit[firstValue].value;	
		tempDivProcess = document.getElementById('divprocess'+firstValue).getElementsByTagName("select");	
		tempDivSeq = document.getElementById('divprocess'+firstValue).getElementsByTagName("input");	
		tempLength = tempDivProcess.length;
		tempCheck = document.getElementById('divcheck'+firstValue).getElementsByTagName("input");
		tempCheckLength = tempCheck.length;	
 		tempDisposition = formobj.disposition[firstValue].value;
 	//setting the values in all child by looping through
		for(i = rowCount; i<loopCount; i++) {		
	//drop down values
			formobj.storageType[i].value = tempStorageType;
			formobj.specimentype[i].value = tempSpecimentype;
			formobj.quantity[i].value = tempQuantity;
			formobj.specimenqunit[i].value = tempSpecimenqunit;
			//Fixed by Ashu Bug No:5585 
			formobj.envtStringChecked[i].value=tempEnvtStringChecked;
			var tem = document.getElementById('divprocess'+i).getElementsByTagName("select");
			var temSeq = document.getElementById('divprocess'+i).getElementsByTagName("input");
			var tempCheckin = document.getElementById('divcheck'+i).getElementsByTagName("input"); 			 
	 			for(addrowindex = tem.length;addrowindex < tempLength ;addrowindex++) {
	 				fnAddDropDown(formobj, i);
	 			} 			
		  for(j=0;j<=tempLength-1;j++) {
	 			 tem[j].value = tempDivProcess[j].value;
		 		 temSeq[j].value = tempDivSeq[j].value;		 
			  }
  		for(j=0;j<=tempCheckLength-1;j++) {
  					tempCheckin[j].checked = tempCheck[j].checked;
  			  }
  			formobj.disposition[i].value = tempDisposition;
  		}	
	  }

</SCRIPT>


<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="srctdmenubaritem5"/>
</jsp:include>


<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.StringUtil,com.velos.eres.business.common.*,com.velos.eres.web.storageAllowedItems.* ,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.storageKit.StorageKitJB, com.velos.eres.web.storage.StorageJB, com.velos.eres.web.storageStatus.StorageStatusJB"%>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="storageB" scope="request" class="com.velos.eres.web.storage.StorageJB"/>
<jsp:useBean id="storageKitB" scope="request" class="com.velos.eres.web.storage.StorageJB"/>
<jsp:useBean id="StorageStatJB" scope="request" class="com.velos.eres.web.storageStatus.StorageStatusJB"/>
<jsp:useBean id="stdJB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="usrJB" scope="request" class="com.velos.eres.web.user.UserJB"/>




<div class="tabDefTopN" id="div1" >
<%

HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {

	 int pageRight = 0;

	 GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");

	 pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSTORAGE"));

String selectedTab = request.getParameter("selectedTab");

String pkStorageKit="";
String storageId = "";
String storageName = "";
String storageType = "";
String notes ="";

String  user = "", userName ="";

String mainFKStorage = "";
String mainParentStorageName = "";

String acc = (String) tSession.getValue("accountId");


String chld1_pkStorage = "";
String chld1_pkStorageKit = "";

ArrayList chld1Pk_StorageKits = null;

ArrayList chld1_spec_types = null;
ArrayList chld1_proc_types = null;
ArrayList  chld1_proc_typSeqs = null;
ArrayList  chld1_quants = null;
ArrayList  chld1_quant_units = null;
ArrayList  chld1_disposns = null;
ArrayList  chld1_envt_consts = null;

String chld1_spec_type = "";
String chld1_proc_type = "";
String chld1_proc_typSeq = "";
String chld1_quant = "";
String chld1_quant_unit = "";
String chld1_disposn = "";
String chld1_envt_const = "";


String chld2_pkStorage = "";
String chld2_pkStorageKit = "";

ArrayList chld2Pk_StorageKits = null;
ArrayList chld2_spec_types = new ArrayList();
ArrayList chld2_proc_types = new ArrayList();
ArrayList  chld2_proc_typSeqs = new ArrayList();
ArrayList  chld2_quants = new ArrayList();
ArrayList  chld2_quant_units = new ArrayList();
ArrayList  chld2_disposns = new ArrayList();
ArrayList  chld2_envt_consts = new ArrayList();

String chld2_spec_type = "";
String chld2_proc_type = "";
String chld2_proc_typSeq = "";
String chld2_quant = "";
String chld2_quant_unit = "";
String chld2_disposn = "";
String chld2_envt_const = "";

String ParentKitName= "";

 CodeDao cdStorCateg= new CodeDao();//category
 cdStorCateg.getCodeValues("storecategory");
 dStorageCateg = cdStorCateg.toPullDown("storecate");
 String storecateg = "";
 //Added by Bikash for Invp 2.8 Storage Child Type 
 
 CodeDao cdStoreChildType= new CodeDao();//child  storage type
 //removed kit from kit storage type
 cdStoreChildType.getCodeValuesSaveKit("store_type");
 dStoreChildType = cdStoreChildType.toPullDown("storeChildType");
 String storeChildType = "";

String add_more = request.getParameter("addmore");
add_more = (add_more==null)?"1":add_more;


String counter_more = request.getParameter("counterBox");
counter_more = (counter_more==null)?"":counter_more;


String counter_more1 = request.getParameter("counterBox");
counter_more1 = (counter_more1==null)?"":counter_more1;

pkStorageKit = request.getParameter("pkstoragekit");

   if(mode.equals("M")) {

	storageB.setPkStorage(EJBUtil.stringToNum(pkStorageKit));
	storageB.getStorageDetails();


	storageId = storageB.getStorageId();
	storageName = storageB.getStorageName();
	storageType = storageB.getFkCodelstStorageType();
	storecateg = storageB.getStorageKitCategory();

	notes = storageB.getStorageNotes();
	mainFKStorage = storageB.getFkStorage();
		
	storeChildType = storageB.getFkCodelstChildStype();

}



StorageDao storDao = storageKitB.getAllChildren(EJBUtil.stringToNum(pkStorageKit));
ArrayList pk_Storages = storDao.getPkStorages();
ArrayList kit_names = storDao.getStorageNames();
ArrayList kit_storage_types = storDao.getFkCodelstStorageTypes();
ArrayList fk_Storages = storDao.getFkStorages();



int len = pk_Storages.size();

int generalCnt = 0;

%>

<table width="100%" cellspacing="0" cellpadding="0" border="0" >

	<tr><td colspan = "4">
	<jsp:include page="inventorytabs.jsp" flush="true">
		<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
		</jsp:include>
	</td>
	</tr>
</table>

</div>
<div class="tabDefBotN" id="div2">

<Form name="storagekit" id="storagekitid" method="post" action="updatestoragekit.jsp" onSubmit="if (validate(document.storagekit)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

 <input type="hidden" name="mode" value=<%=mode%>>
 <input type="hidden" name="pkStorageKit" value=<%=pkStorageKit%>>





<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tr>
	<td colspan="2"><%=MC.M_StrKitIdBlank_ForAutoGenr%><%--Leave 'Storage Kit ID' field blank for system auto-generated ID*****--%></td>
	</tr>

	<tr>
	<td width="50%">
		<table>
		<tr>
		<td class=tdDefault width="30%"><b><%=LC.L_Storage_KitId%><%--Storage Kit ID*****--%></b></td>
		<td class=tdDefault><Input type = "text" name="storageKitId"  value="<%=storageId%>" size=15 MAXLENGTH =100></td>

		</td>


		<%
			dStorageCateg = cdStorCateg.toPullDown("storecate",EJBUtil.stringToNum(storecateg));
		%>
		<td class=tdDefault align="center"><%=LC.L_Category%><%--Category*****--%></td>
		<td class=tdDefault><%=dStorageCateg%></td>



		</tr>
		<tr>
		<td class=tdDefault align="center"><%=LC.L_Storage_KitName%><%--Storage Kit Name*****--%><FONT class="Mandatory">* </FONT></td>
		<td class=tdDefault ><Input type = "text" name="storageKitName"   value="<%=storageName%>" size=25 MAXLENGTH = 250 align = right></td>
		<!--Added by Bikash Invp 2.8 change-->
		<%
		dStoreChildType = cdStoreChildType.toPullDown("storeChildType",EJBUtil.stringToNum(storeChildType));
		%>
		<td class=tdDefault align="center"><%=LC.L_Storage_KitType%><%--Storage Kit Type*****--%><FONT class="Mandatory">* </FONT></td> <!-- AK:FixedBug#5816 (16Mar11) -->
		<td class=tdDefault><%=dStoreChildType%></td>
		</tr>

</table>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<br>

<input type="hidden" name="counterBox" value="" size="2">
<tr>
<td ><P class = "sectionHeadings"> <%=LC.L_Kit_Components%><%--Kit Components*****--%></p></td>
<td>
<a href=# onClick="return fnAddMoreComponentRows(document.storagekit, <%=len%>);"><img border="0" src="../images/add.png" title="<%=LC.L_Add%><%--Add*****--%>"/></a>
&nbsp;<input type="text" name="addmore" value="<%=add_more%>" size="2">&nbsp;
<a href=# onClick="return fnAddMoreComponentRows(document.storagekit, <%=len%>);"><%=LC.L_More_Components%><%--More Component(s)*****--%></a></td>

</tr>

</table>
<table class=tableDefault id="myMainTable" width="100%" border=0 >
<TR id="myiRow0">
 	 <th width=10% align =center><%=LC.L_Name%><%--Name*****--%><FONT class="Mandatory">* </FONT></th>
     <th width=10% align =center><%=LC.L_Storage_Type%><%--Storage Type*****--%><FONT class="Mandatory">* </FONT></th>
     <th width=10%  align =center><%=LC.L_Specimen_Type%><%--Specimen Type*****--%><FONT class="Mandatory">* </FONT></th>
     <th width=12% align =center colspan=2><%=LC.L_Quantity%><%--Quantity*****--%></th>

     <th width=22% align =center ><%=LC.L_Processing_Steps%><%--Processing Steps*****--%> &nbsp; <%=LC.L_Seq%><%--Seq*****--%>#</th>
      <!--
      <th width=05%  align =left >Seq#</th>
      -->

 	 <th width=15%  align =center ><%=LC.L_Disposition%><%--Disposition*****--%></th>
 	 <th width=13%  align =center ><%=LC.L_Environmental_Consts%><%--Environmental Constraints*****--%></th>
 	 <th width=5%  align =center >&nbsp;&nbsp;&nbsp;</th>
 	 <th width=5%  align =center colspan=1 >&nbsp;&nbsp;</th>

 	 <th width=5%  align =center ><%=LC.L_Delete%></th>

<!--for deleting extra rows those added in the mode=M hiddenChk is used-->
<INPUT type="checkbox" name="hiddenChk" style="visibility:hidden"/>
</TR>

<%
int baseOginalRows = 0;
int rowCount = 0;
baseOginalRows = 2;

String numExi = "";
numExi = request.getParameter("totalExtgRows");
numExi = (numExi ==null)?"2":numExi ;


%>

<input type = "hidden" name="totalExtgRows" value=<%=len%>>

<input type = "hidden" name="baseOrigRows" value=<%=len%>>

<input type = "hidden" name="oldExtgRows" value=<%=len%>>
<%

for (int n=0 ; n < len; n ++){

	if ((fk_Storages.get(n).toString()).equals(pkStorageKit)){
      rowCount ++;
//find out the details based upon the parameter and assign to display in edit mode

		StorageJB strChld1B = new StorageJB();

		chld1_pkStorage  = pk_Storages.get(n).toString();
		strChld1B.setPkStorage(EJBUtil.stringToNum(chld1_pkStorage));
		strChld1B.getStorageDetails();

		storageName = strChld1B.getStorageName();
		ParentKitName = strChld1B.getStorageName();
		storageType = strChld1B.getFkCodelstStorageType();


		StorageKitJB strKitBChld1 = new StorageKitJB();
		StorageKitDao skChld1Dao = new StorageKitDao();
		 skChld1Dao= strKitBChld1.getStorageKitAttributes(EJBUtil.stringToNum(chld1_pkStorage));


		//skChld1Dao.getFkStorages();
		chld1Pk_StorageKits = skChld1Dao.getPkStorageKits();//need for edit mode calculation
		chld1_spec_types = skChld1Dao.getDefSpecimenTypes();
		chld1_proc_types = skChld1Dao.getDefProcTypes();//',' separated values
		chld1_proc_typSeqs = skChld1Dao.getDefProcSeqs();//',' separated values
		chld1_quants = skChld1Dao.getDefSpecQuants();
		chld1_quant_units = skChld1Dao.getDefSpecQuantUnits();
		chld1_disposns =  skChld1Dao.getKitSpecDispositions();
		chld1_envt_consts = skChld1Dao.getKitSpecEnvtCons();//Not ',' separated values


		chld1_pkStorageKit=((chld1Pk_StorageKits.get(0)) == null)?"":(chld1Pk_StorageKits.get(0)).toString();
		chld1_spec_type=((chld1_spec_types.get(0)) == null)?"":(chld1_spec_types.get(0)).toString();
		chld1_quant=((chld1_quants.get(0)) == null)?"":(chld1_quants.get(0)).toString();
		chld1_quant_unit=((chld1_quant_units.get(0)) == null)?"":(chld1_quant_units.get(0)).toString();
		chld1_proc_type=((chld1_proc_types.get(0)) == null)?"":(chld1_proc_types.get(0)).toString();
		chld1_proc_typSeq=((chld1_proc_typSeqs.get(0)) == null)?"":(chld1_proc_typSeqs.get(0)).toString();
		chld1_disposn=((chld1_disposns.get(0)) == null)?"":(chld1_disposns.get(0)).toString();
		chld1_envt_const=((chld1_envt_consts.get(0)) == null)?"":(chld1_envt_consts.get(0)).toString();


if (n>0){
generalCnt++;
%>



<!--Level-1 kits will lie here-->





<TR>
<TD colspan=10><hr/><INPUT type="checkbox" name="hiddenChk" style="visibility:hidden"/></TD>
</TR>


<%
//System.out.println("********n****"+n+ ":::"+ generalCnt + "::"+pk_Storages.get(n).toString());
}%>

<tr id="myiRow<%=n+1%>" >

<td>
<input type="text" name="kitCompName" value="<%=storageName%>" size="10">
</td>

<input type="hidden" name="idsStOrages" value="<%=pk_Storages.get(n).toString()%>" >
<input type="hidden" name="idsKits" value="<%=chld1_pkStorageKit%>" >


<% if (mode.equals("N")){ %>
	<td class=tdDefault><%=dStorType%></td>
<%}else {
	dStorType = cdStorageType.toPullDown("storageType",EJBUtil.stringToNum(storageType));
%>
	<td class=tdDefault><%=dStorType%></td>
<%}%>

<% if (mode.equals("N")){ %>
	<td class=tdDefault><%=dSpecimenType%></td>
<%}else {
	dSpecimenType = cdSpecType.toPullDown("specimentype",EJBUtil.stringToNum(chld1_spec_type));
%>
	<td class=tdDefault><%=dSpecimenType%></td>
<%}%>
<td>
<input type="text" name="quantity" value="<%=chld1_quant%>" size="05">
</td>
<% if (mode.equals("N")){ %>
	<td class=tdDefault><%=dSpecQuantUnit%></td>
<%}else {
	dSpecQuantUnit = cdSpecQntUnit.toPullDown("specimenqunit",EJBUtil.stringToNum(chld1_quant_unit));
%>
	<td class=tdDefault><%=dSpecQuantUnit%></td>
<%}%>

<!--Processing Steps and sequence----------------->
<%



%>
<td>
<table>
<tr>
<td>&nbsp;&nbsp;<a href=# onClick="fnAddDropDown(document.storagekit, '<%=n%>')" ><%=LC.L_Add_More%><%--Add More*****--%></a>
</td>
</tr>
</table>

<%
String baseVal="2";
baseVal = request.getParameter("numDropDownPerRow");
baseVal = (baseVal ==null)?"2":baseVal ;

StringTokenizer idDropDownSt=new StringTokenizer(chld1_proc_type,",");
StringTokenizer idSeqSt=new StringTokenizer(chld1_proc_typSeq,",");


int indivIds =idDropDownSt.countTokens();
String procTypeStr = "" ;
String procSeq= "";
String envtConsStr= "";


if (indivIds==0){
	baseVal = baseVal;
}else{
	baseVal=indivIds+"";
}

%>
<input type="hidden" name="numDropDownPerRow" value="<%=baseVal%>">
<DIV id="divprocess<%=rowCount-1%>">
<table id="myTable<%=n%>" name="myTable<%=n%>" border="0">
<%



for (int q= 0 ; q<EJBUtil.stringToNum(baseVal); q++){


	procTypeStr = idDropDownSt.nextToken();
	procSeq = idSeqSt.nextToken();
	procSeq= (procSeq.equals("0"))?"":procSeq;





%>

<tr><!-- id="tr1"--removed-->

	<% if (mode.equals("N")){ %>
		<td class=tdDefault><%=dProcSteps%></td>

	<%}else {
		dProcSteps = cdSpecProcSteps.toPullDown("processingsteps",EJBUtil.stringToNum(procTypeStr.toString()));
	%>
		<td class=tdDefault><%=dProcSteps%></td>
	<%}
	%>


	<td>
	<input type="text" name="sequence" value="<%=procSeq%>" size="03">
	</td>
</tr>
<%
}%>
</table>
</Div>
</td>



<!--Disposition----------------->
<% if (mode.equals("N")){ %>
		<td class=tdDefault><%=dDisposition%></td>

<%}else {
		dDisposition = cdDisposition.toPullDown("disposition",EJBUtil.stringToNum(chld1_disposn));
	%>
		<td class=tdDefault><%=dDisposition%></td>
<%}
%>
<td>
<div id="divcheck<%=rowCount-1%>">
<%
	for (int i=0;i<length; i++) {
	codelstdesc=(String)  codelstDescs.get(i);
	genStatus = chld1_envt_const.charAt(i)+"";
	genStatus = (genStatus.equals("0"))?"":"checked";

%>

<input type="checkbox" name="checkme<%=i%>" id="checkme<%=i%>" onClick="indivChek(document.storagekit)" <%=genStatus%>> <%=codelstdesc%><br>

<%
	}
%>
</div>
</td>

<input type="hidden" name="envtStringChecked" value="<%=chld1_envt_const.charAt(0)%><%=chld1_envt_const.charAt(1)%><%=chld1_envt_const.charAt(2)%>" >


<td>

</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

</td>
<!--
<td width="10%"><A href="#" onClick ="return fnDeleteRowComponentRows(document.storagekit, '<%=n%>');"><img src="./images/delete.gif" border="0" align="left"/></A></td>
-->
<td>
<input type="hidden" name="numSpecChildren" value="" size="03">
</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="hidden" name="checkCopyChild" id="checkCopyChild" onClick="fnChk(document.storagekit)">
<input type="hidden" name="checkCopyChildStr">


<INPUT type="checkbox" name="hiddenChk" style="visibility:hidden"/>


</td>



</tr>



<%}

int dispCounter = 0;
int childCount = 0;
int cnt = 0;
while(cnt<len){
	if ((pk_Storages.get(n).toString()).equals(fk_Storages.get(cnt).toString())){
		childCount++;
	}
		cnt ++;
	
}
%>
<input type="hidden" name="childCount" value="<%=childCount%>">
<%
//-----------------------Level-2 kit will lie here------------------------------------------
	for (int z=1 ; z < len; z ++){

		if ((pk_Storages.get(n).toString()).equals(fk_Storages.get(z).toString())){
    rowCount ++;
	dispCounter++;
		//find out the details based upon the parameter and assign to display in edit mode




		StorageJB strchld2B = new StorageJB();


		 chld2_pkStorage  = pk_Storages.get(z).toString();

		strchld2B.setPkStorage(EJBUtil.stringToNum(chld2_pkStorage));
		strchld2B.getStorageDetails();

		storageName = strchld2B.getStorageName();
		storageType = strchld2B.getFkCodelstStorageType();


		StorageKitJB strKitBchld2 = new StorageKitJB();
		StorageKitDao skChld2Dao = new StorageKitDao();
			skChld2Dao = strKitBchld2.getStorageKitAttributes(EJBUtil.stringToNum(chld2_pkStorage));




		chld2Pk_StorageKits = skChld2Dao.getPkStorageKits();
		chld2_spec_types = skChld2Dao.getDefSpecimenTypes();
		chld2_proc_types = skChld2Dao.getDefProcTypes();
		chld2_proc_typSeqs = skChld2Dao.getDefProcSeqs();
		chld2_quants = skChld2Dao.getDefSpecQuants();
		chld2_quant_units = skChld2Dao.getDefSpecQuantUnits();
		chld2_disposns =  skChld2Dao.getKitSpecDispositions();
		chld2_envt_consts =  skChld2Dao.getKitSpecEnvtCons();


		chld2_pkStorageKit=((chld2Pk_StorageKits.get(0)) == null)?"":(chld2Pk_StorageKits.get(0)).toString();
		chld2_spec_type=((chld2_spec_types.get(0)) == null)?"":(chld2_spec_types.get(0)).toString();
		chld2_quant=((chld2_quants.get(0)) == null)?"":(chld2_quants.get(0)).toString();
		chld2_quant_unit=((chld2_quant_units.get(0)) == null)?"":(chld2_quant_units.get(0)).toString();
		chld2_proc_type=((chld2_proc_types.get(0)) == null)?"":(chld2_proc_types.get(0)).toString();
		chld2_proc_typSeq=((chld2_proc_typSeqs.get(0)) == null)?"":(chld2_proc_typSeqs.get(0)).toString();
		chld2_disposn=((chld2_disposns.get(0)) == null)?"":(chld2_disposns.get(0)).toString();
		chld2_envt_const=((chld2_envt_consts.get(0)) == null)?"":(chld2_envt_consts.get(0)).toString();



		%><!----------------------------level-2 children will go here----------------------------------------->
<%if (dispCounter==1){%>
<td colspan="50%">
<p class = "defComments"> <%=LC.L_Child_SpecimenFor%><%--Child Specimen for*****--%> <%=ParentKitName%></p>
<INPUT type="checkbox" name="hiddenChk" style="visibility:hidden"/>
</td>



<%
generalCnt++;

}

//System.out.println("********z****"+z+ ":::"+ generalCnt + "::"+pk_Storages.get(z).toString());

%>
<tr id="myiRow<%=z%>" class="custom-transparent" bgcolor="#ccccc0">

<td>
<input type="text" name="kitCompName" value="<%=storageName%>" size="10">
</td>

<input type="hidden" name="idsStOrages" value="<%=pk_Storages.get(z).toString()%>" >
<input type="hidden" name="idsKits" value="<%=chld2_pkStorageKit%>" >

<% if (mode.equals("N")){ %>
	<td class=tdDefault><%=dStorType%></td>
<%}else {
	dStorType = cdStorageType.toPullDown("storageType",EJBUtil.stringToNum(storageType));
%>
	<td class=tdDefault><%=dStorType%></td>
<%}%>

<% if (mode.equals("N")){ %>
	<td class=tdDefault><%=dSpecimenType%></td>
<%}else {
	dSpecimenType = cdSpecType.toPullDown("specimentype",EJBUtil.stringToNum(chld2_spec_type));
%>
	<td class=tdDefault><%=dSpecimenType%></td>
<%}%>
<td>
<input type="text" name="quantity" value="<%=chld2_quant%>" size="05">
</td>
<% if (mode.equals("N")){ %>
	<td class=tdDefault><%=dSpecQuantUnit%></td>
<%}else {
	dSpecQuantUnit = cdSpecQntUnit.toPullDown("specimenqunit",EJBUtil.stringToNum(chld2_quant_unit));
%>
	<td class=tdDefault><%=dSpecQuantUnit%></td>
<%}%>

<!--Processing Steps and sequence----------------->
<td>
<table>
<tr>
<td>&nbsp;&nbsp;<a href=# onClick="fnAddDropDown(document.storagekit, '<%=z%>')" ><%=LC.L_Add_More%><%--Add More*****--%></a>
</td>
</tr>
</table>
<%

String baseVal="2";

baseVal = request.getParameter("numDropDownPerRow");
baseVal = (baseVal ==null)?"2":baseVal ;

StringTokenizer idDropDownSt=new StringTokenizer(chld2_proc_type,",");
StringTokenizer idSeqSt=new StringTokenizer(chld2_proc_typSeq,",");


int indivIds =idDropDownSt.countTokens();
String procTypeStr = "" ;
String procSeq= "";
String envtConsStr= "";


if (indivIds==0){
	baseVal = baseVal;
}else{
	baseVal=indivIds+"";
}

%>
<input type="hidden" name="numDropDownPerRow" value="<%=baseVal%>">
<DIV id = "divprocess<%=rowCount-1%>">
<table id="myTable<%=z%>" name="myTable<%=z%>" border="0">
<%
for (int q= 0 ; q<EJBUtil.stringToNum(baseVal); q++){


	procTypeStr = idDropDownSt.nextToken();
	procSeq = idSeqSt.nextToken();

%>


<tr> <!--id="tr1" removed-->
	<% if (mode.equals("N")){ %>
		<td class=tdDefault><%=dProcSteps%></td>

	<%}else {
		dProcSteps = cdSpecProcSteps.toPullDown("processingsteps",EJBUtil.stringToNum(procTypeStr.toString()));
	%>
		<td class=tdDefault><%=dProcSteps%></td>
	<%}
	%>


	<td>
	<input type="text" name="sequence" value="<%=procSeq%>" size="03">
	</td>
</tr>
<%}%>
</table>
</DIV>
</td>


<!--Disposition----------------->
<% if (mode.equals("N")){ %>
		<td class=tdDefault><%=dDisposition%></td>

<%}else {
		dDisposition = cdDisposition.toPullDown("disposition",EJBUtil.stringToNum(chld2_disposn));
	%>
		<td class=tdDefault><%=dDisposition%></td>
<%}

%>
<td>
<div id="divcheck<%=rowCount-1%>">
<%
	for (int i=0;i<length; i++) {
	codelstdesc=(String)  codelstDescs.get(i);

	genStatus = chld2_envt_const.charAt(i)+"";
	genStatus = (genStatus.equals("0"))?"":"checked";

%>

<input type="checkbox" name="checkme<%=i%>" id="checkme<%=i%>" onClick="indivChek(document.storagekit)" <%=genStatus%>> <%=codelstdesc%><br>

<%
	}
%>
</div>
</td>
<%if (dispCounter==1 && childCount > 1){%>
<!-- Fixed by Ashu Bug No:5586-->
<td colspan="4"><a href=# onclick="updateAll(document.storagekit,<%=childCount%>,<%=rowCount%>,<%=z%>)"><%=LC.L_Set_ToAll%><%--Set To All*****--%></a></td>
<%
}
else{%>
	<td colspan="4">&nbsp;</td>
<%}
%>
<input type="hidden" name="envtStringChecked" value="<%=chld2_envt_const.charAt(0)%><%=chld2_envt_const.charAt(1)%><%=chld2_envt_const.charAt(2)%>" >

<!--<td>
</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td>
-->

<td>
<input type="hidden" name="numSpecChildren" value="" size="03">
</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="hidden" name="checkCopyChild" id="checkCopyChild" onClick="fnChk(document.storagekit)">
<input type="hidden" name="checkCopyChildStr">

<INPUT type="checkbox" name="hiddenChk" style="visibility:hidden"/>
</td>




<!--
<td width="10%"><A href="#" onClick ="return fnDeleteRowComponentRows(document.storagekit, '<%=z%>');"><img src="./images/delete.gif" border="0" align="left"/></A></td>
-->


</tr>

		<%
		}




	}




}


%>
<input type="hidden" name="generalCnt" value="<%=generalCnt%>" >

<Input type = "hidden" name="lengthOfLoop" value="<%=len%>">

<Input type = "hidden" name="storageId" >
<Input type = "hidden" name="storageName">

</table>
<table>
<tr>
<td width="50%">
	<table>
	<tr>
	<td width="30%"><b><%=LC.L_Notes%><%--Notes*****--%> </b></td>
	<td class=tdDefault><textarea name="notes" rows=4 cols=30 MAXLENGTH = 20000><%=notes%></textarea></td>
	</tr>
	</table>
</td>
</tr>
</table>

<%

 if ( pageRight >=5  )
 {
%>
<BR>
<table width="98%" cellspacing="0" cellpadding="0">
<tr valign="center">
			<td width=35%>&nbsp;</td>
			<td>
<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="N"/>
		<jsp:param name="formID" value="storagekitid"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
</td>
		</tr>
		</table>
<% } %>
<!-- Bug#6320 10-Sep-2012 -Sudhir-->
<Input type = "hidden" name="rowId" value="<%=len%>" >
</Form>
<%
}//end of if body for session
	else {
%>
  <jsp:include page="timeout.html" flush="true"/>
<% } %>
	<div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>
</body>

</html>
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		$j("#div2").attr("style","height:80%;");
</SCRIPT>