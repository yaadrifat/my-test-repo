<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList,java.util.HashMap,org.json.*"%>
<%@page import="com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
<jsp:useBean id="sessionmaint" scope="session"
	class="com.velos.eres.service.util.SessionMaint" />
	<jsp:useBean id="ntDao" scope="request"
	class="com.velos.eres.business.common.NetworkDao" />
	<jsp:useBean id="usrDao" scope="request"
	class="com.velos.eres.business.common.UserDao" />
	<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	HttpSession tSession = request.getSession(true);
	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN,
				"SESSION_TIME_OUT");
		JSONObject jsObj = new JSONObject();
		jsObj.put("error", new Integer(-1));
		jsObj
				.put("errorMsg", MC.M_UsrNot_LoggedIn/*User is not logged in.*****/);
		out.println(jsObj.toString());
		return;
	}
	int accountId=0;
	 String  searchBy  = request.getParameter("term");
	 String networkId=(request.getParameter("networkId")==null)?"":request.getParameter("networkId");
	 String searchNetwork=(request.getParameter("searchNetwork")==null)?"":request.getParameter("searchNetwork");
	 String studyId=(request.getParameter("studyId")==null)?"":request.getParameter("studyId");
	 String acc = (String) tSession.getValue("accountId");
		accountId = EJBUtil.stringToNum(acc);
	 JSONArray maplist = new JSONArray();
	 if(!studyId.equals(""))
		maplist= ntDao.autocompleteStudyNetwork(searchBy,EJBUtil.stringToNum(acc),EJBUtil.stringToNum(studyId));
	 else if(searchNetwork.equals("true"))
		maplist= ntDao.autocompleteNetworks(searchBy,EJBUtil.stringToNum(acc),networkId);
	 else if(networkId.equals(""))
	 {
		 maplist= usrDao.autocompleteUsers(searchBy,EJBUtil.stringToNum(acc));
	 }
	 else
	 	maplist= ntDao.autocompleteUsersNetwork(searchBy,EJBUtil.stringToNum(acc),EJBUtil.stringToNum(networkId));
	 out.println(maplist.toString());
	 %>