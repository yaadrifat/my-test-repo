<!-- 
This JSP will be included in gadgetHome.jsp. All non-local javascript variables and functions 
will be global in the mother JSP. Therefore, make sure to prefix all non-local variables
with the gadget ID. All functions should be namespaced to start with gadget ID.
 -->

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%> 
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
if (!sessionmaint.isValidSession(request.getSession(false))) {
	return;
}
%>
<script>
// Declare all functions here
var gadgetSample2 = {
	acctLinks: {
		data: {},
		deleteLink: {},  // do the actual delete
		formatAcctLinkHTML: {},
		reloadAcctLink: {},
		saveNewLink: {}
	},
	myLinks: {
		data: {},
		deleteLink: {},  // do the actual delete
		formatMyLinkHTML: {},
		reloadMyLink: {},
		formatData: {},
		saveNewLink: {}
	},
	bothLinks: { // functions shared between myLinks and acctLinks
		truncateLinkName: {},
		screenActionForLinks: {},
		openNewLink: {},
		addNewLink: {},
		cancelNewLink: {},
		editLink: {},
		delLink: {},  // Open delete dialog
		clearNewLink: {},
		clearNewLinkErrMsg: {},
		clearLinkESign: {},
		clearLinkDeleteESign: {},
		validateNewLink: {},
		validateDeleteLink: {}
	},
	screenAction: {}, // add actions to the entire screen of this gadget
	submitStudy: {},
	submitStudyAdvanced: {},
	submitPatient: {},
	submitPatientStudy: {},
	beforeSubmitStudy:{},
	beforeSubmitPatient: {},
	beforeSubmitPatientStudy: {}
};

// Implement the functions below
gadgetSample2.myLinks.reloadMyLink = function() {
	try {
    	jQuery.ajax({
    		url:'gadgetSample2_getMyLinks',
    		async: true,
    		cache: false,
    		success: function(resp) {
    			gadgetSample2.myLinks.data.array = resp.array;
    			gadgetSample2.myLinks.data.addMore = resp.addMore;
    			gadgetSample2.myLinks.formatMyLinkHTML();
    			gadgetSample2.bothLinks.screenActionForLinks('myLink');
    		}
    	});
	} catch(e) {}
}
gadgetSample2.myLinks.formatMyLinkHTML = function () {
	var str1 = '<table width="100%" cellspacing="0" >';
	for(var iX=0; iX<gadgetSample2.myLinks.data.array.length; iX++) {
		if (iX > 9) { break; }
		str1 += "<tr id='gadgetSample2_myLink-row-"+iX+"'><td>";
		str1 += "<a target=\"_blank\" id=\"gadgetSample2_myLink"+iX+"\"";
		str1 += " href=\""+gadgetSample2.myLinks.data.array[iX].uri+"\"";
		str1 += " title=\""+gadgetSample2.myLinks.data.array[iX].uri+"\">";
		str1 += gadgetSample2.bothLinks.truncateLinkName(gadgetSample2.myLinks.data.array[iX].desc);
		str1 += "</td><td nowrap='nowrap' align='right'>";
		str1 += "<a style=\"opacity:0; filter:alpha(opacity=0);\" href=\"javascript:gadgetSample2.bothLinks.editLink('myLinks',";
		str1 += (iX+1);
		str1 += ");\" id=\"gadgetSample2.myLinks.editMyLink"+iX+"\">";
		str1 += "<img id='gadgetSample2_myLink-edt-img-"+iX;
		str1 += "' class='headerImage' style='border:none; opacity:0; filter:alpha(opacity=0);' title='";
		str1 += "Edit Link #".replace("\'", "&#39;")+(iX+1);
		str1 += "' src='./images/edit.gif' border=0 />";
		str1 += "</a>&nbsp;";
		str1 += "<a style=\"opacity:0; filter:alpha(opacity=0);\" href=\"javascript:gadgetSample2.bothLinks.delLink('myLinks',";
		str1 += (iX+1);
		str1 += ");\" id=\"gadgetSample2_delMyLink"+iX+"\">";
		str1 += "<img id='gadgetSample2_myLink-del-img-"+iX;
		str1 += "' class='headerImage' style='border:none; opacity:0; filter:alpha(opacity=0);' title='";
		str1 += "Delete Link #".replace("\'", "&#39;")+(iX+1);
		str1 += "' src='./images/delete.gif' border=0 />";
		str1 += "</a>";
		str1 += "</td></tr>";
	}
	if (gadgetSample2.myLinks.data.addMore == 'true') {
		str1 += "<tr><td colspan='2' align='right'><a href='ulinkBrowser.jsp?srcmenu=tdMenuBarItem13&selectedTab=2'";
		str1 += " title='"+"More My Links"+"'>";
		str1 += "More My Links"+"</a>";
		str1 += "</td></tr>";
	}
	str1 += '</table>';
	$('gadgetSample2_myLinksHTML').innerHTML = str1;
}
gadgetSample2.myLinks.formatData = function() {
	var j = {};
	j.myLinksLinkUrl = jQuery.trim($('gadgetSample2JB.myLinksLinkUrl').value);
	j.myLinksLinkSection = jQuery.trim($('gadgetSample2JB.myLinksLinkSection').value);
	j.myLinksLinkId = jQuery.trim($('gadgetSample2JB.myLinksLinkId').value);
	j.myLinksLinkDisplay = jQuery.trim($('gadgetSample2JB.myLinksLinkDisplay').value);
	j.eSign = jQuery.trim($('gadgetSample2JB.myLinksESign').value);
	if ($('gadgetSample2JB.myLinksLinkUrl').value)
		$('gadgetSample2JB.myLinksLinkUrl').value = jQuery.trim($('gadgetSample2JB.myLinksLinkUrl').value);
	if ($('gadgetSample2JB.myLinksLinkSection').value)
		$('gadgetSample2JB.myLinksLinkSection').value = jQuery.trim($('gadgetSample2JB.myLinksLinkSection').value);
	if ($('gadgetSample2JB.myLinksLinkId').value) 
		$('gadgetSample2JB.myLinksLinkId').value = jQuery.trim($('gadgetSample2JB.myLinksLinkId').value);
	if ($('gadgetSample2JB.myLinksLinkDisplay').value)
		$('gadgetSample2JB.myLinksLinkDisplay').value = jQuery.trim($('gadgetSample2JB.myLinksLinkDisplay').value);
	if ($('gadgetSample2JB.myLinksESign').value)
		$('gadgetSample2JB.myLinksESign').value = jQuery.trim($('gadgetSample2JB.myLinksESign').value);
	return j;
}
gadgetSample2.myLinks.saveNewLink = function() {
	if (!gadgetSample2.bothLinks.validateNewLink('myLinks')) { return false; }
	try {
		YUI().use('io', 'json', 'json-parse', function(Y) {
			var formData = Y.JSON.stringify(gadgetSample2.myLinks.formatData());
        	Y.io('gadgetSample2_saveMyLink', {
            	method: 'POST',
            	data: $j('#gadgetSample2_formNewLink').serialize(),
            	sync: true,
            	on: {
                	success: function (tranId, o) {
                    	var json = Y.JSON.parse(o.responseText);
                    	if (json.inputErrors) {
                    		var thisValidationId = 'gadgetSample2_newLink';
                    		validationMessages[thisValidationId] = {};
                    		var myValidations = [];
                    		for (var key in json.inputErrors) {
                    			var errorArray = json.inputErrors[key];
                    			for (var iX=0; iX<errorArray.length; iX++) {
                    				myValidations.push(errorArray[iX]);
                    			}
                    			if (myValidations.length > 0) {
                    				validationMessages[thisValidationId][key]
                    					= myValidations;
                    			}
                    		}
                    		formatErrMsgGlobal(thisValidationId);
                    		$j('#gadgetSample2_newMyLinkButtonRow').show();
                    	}  else if (json.operationErrors) {
                    		var myMessage = '';
                    		if (json.operationErrors) {
                    			myMessage = json.operationErrors[0];
                    		}
                    		$('gadgetSample2_formNewLink_resultMsg').innerHTML = wrapDisplayMessage(myMessage); 
                        	$j('#gadgetSample2_newMyLinkButtonRow').show();
                    	} else {
                    		$j('#gadgetSample2_newMyLinkButtonRow').hide();
                    		$('gadgetSample2_formNewLink_resultMsg').innerHTML = wrapDisplayMessage('Data saved successfully.');
                    		setTimeout(function () {
                    			$j("#gadgetSample2_myLinksNewLink").slideUp(200).fadeOut(200);
                    			gadgetSample2.myLinks.reloadMyLink();
                        		$('gadgetSample2_formNewLink_resultMsg').innerHTML = '';
                        		$j('#gadgetSample2_newMyLinkButtonRow').show();
                        		gadgetSample2.bothLinks.clearNewLink('myLinks');
                        		gadgetSample2.bothLinks.clearLinkESign('myLinks');
                    		}, 500);
                    	}
                    },
                    failure: function (tranId, o) {
                		$j('#gadgetSample2_newMyLinkButtonRow').show();
                		$('gadgetSample2_formNewLink_resultMsg').innerHTML = wrapDisplayMessage(M_CommErr_CnctVelos);
                    }
                }
            });
		});
	} catch(e) {}
}
gadgetSample2.myLinks.deleteLink = function() {
	if (!gadgetSample2.bothLinks.validateDeleteLink('myLinks')) { return false; }
	var thisJ = {}
	thisJ.myLinksLinkId = $('gadgetSample2JB.myLinksDeleteLinkId').value;
	thisJ.eSign = $('gadgetSample2JB.myLinksDeleteESign').value;
	try {
		YUI().use('io', 'json', 'json-parse', function(Y) {
			var formData = Y.JSON.stringify(thisJ);
        	Y.io('gadgetSample2_deleteMyLink', {
            	method: 'POST',
            	data: $j('#gadgetSample2_formDeleteLink').serialize(),
            	sync: true,
            	on: {
                	success: function (tranId, o) {
                    	var json = Y.JSON.parse(o.responseText);
                    	if (json.inputErrors) {
                    		var thisValidationId = 'gadgetSample2.myLinks.deleteLink';
                    		validationMessages[thisValidationId] = {};
                    		var myValidations = [];
                    		for (var key in json.inputErrors) {
                    			var errorArray = json.inputErrors[key];
                    			for (var iX=0; iX<errorArray.length; iX++) {
                    				myValidations.push(errorArray[iX]);
                    			}
                    			if (myValidations.length > 0) {
                    				validationMessages[thisValidationId][key]
                    					= myValidations;
                    			}
                    		}
                    		formatErrMsgGlobal(thisValidationId);
                        	$j('#gadgetSample2_deleteMyLinkButtonRow').show();
                    	} else if (json.operationErrors) {
                    		var myMessage = '';
                    		if (json.operationErrors) {
                    			myMessage = json.operationErrors[0];
                    		}
                    		$('gadgetSample2_formDeleteLink_resultMsg').innerHTML = wrapDisplayMessage(myMessage); 
                        	$j('#gadgetSample2_deleteMyLinkButtonRow').show();
                    	} else {
                    		$j('#gadgetSample2_deleteMyLinkButtonRow').hide();
                    		$('gadgetSample2_formDeleteLink_resultMsg').innerHTML = wrapDisplayMessage('Data Deleted Successfully!');
                    		setTimeout(function () {
                    			$j("#gadgetSample2_myLinksNewLink").slideUp(200).fadeOut(200);
                    			gadgetSample2.myLinks.reloadMyLink();
                    		}, 500);
                    		setTimeout(function () {
                        		$j('#gadgetSample2_deleteMyLinkButtonRow').show();
                        		$('gadgetSample2_formDeleteLink_resultMsg').innerHTML = '';
                        		gadgetSample2.bothLinks.clearNewLink('myLinks');
                        		gadgetSample2.bothLinks.clearLinkDeleteESign('myLinks');
                    		}, 1000);
                    	}
                    },
                    failure: function (tranId, o) {
                		$('gadgetSample2_formDeleteLink_resultMsg').innerHTML = wrapDisplayMessage(M_CommErr_CnctVelos);
                    	$j('#gadgetSample2_deleteMyLinkButtonRow').show();
                    }
                }
            });
		});
	} catch(e) {}
}

gadgetSample2.acctLinks.reloadAcctLink = function () {
	try {
    	jQuery.ajax({
    		url:'gadgetSample2_getAcctLinks',
    		async: true,
    		cache: false,
    		success: function(resp) {
    			gadgetSample2.acctLinks.data.array = resp.array;
    			gadgetSample2.acctLinks.data.addMore = resp.addMore;
    			gadgetSample2.acctLinks.data.access = resp.access;
    			gadgetSample2.acctLinks.formatAcctLinkHTML();
    			gadgetSample2.bothLinks.screenActionForLinks('acctLink');
    		}
    	});
	} catch(e) {}
}
gadgetSample2.acctLinks.formatAcctLinkHTML = function () {
	var str1 = '<table width="100%" cellspacing="0" >';
	for(var iX=0; iX<gadgetSample2.acctLinks.data.array.length; iX++) {
		if (iX > 9) { break; }
		str1 += "<tr id='gadgetSample2_acctLink-row-"+iX+"'><td>";
		str1 += "<a target=\"_blank\" id=\"gadgetSample2_acctLink"+iX+"\"";
		str1 += " href=\""+gadgetSample2.acctLinks.data.array[iX].uri+"\"";
		str1 += " title=\""+gadgetSample2.acctLinks.data.array[iX].uri+"\">";
		str1 += gadgetSample2.bothLinks.truncateLinkName(gadgetSample2.acctLinks.data.array[iX].desc);
		str1 += "</td><td width='10%' nowrap='nowrap' align='right'>";
		if (gadgetSample2.acctLinks.data.access != '6' && gadgetSample2.acctLinks.data.access != '7') {
			str1 += "</td></tr>"; continue;
		}
		str1 += "<a style=\"opacity:0; filter:alpha(opacity=0);\" href=\"javascript:gadgetSample2.bothLinks.editLink('acctLinks',";
		str1 += (iX+1);
		str1 += ");\" id=\"gadgetSample2.acctLinks.editAcctLink"+iX+"\">";
		str1 += "<img id='gadgetSample2_acctLink-edt-img-"+iX;
		str1 += "' class='headerImage' style='border:none; opacity:0; filter:alpha(opacity=0);' title='";
		str1 += "Edit Link #".replace("\'", "&#39;")+(iX+1);
		str1 += "' src='./images/edit.gif' border=0 />";
		str1 += "</a>&nbsp;";
		str1 += "<a style=\"opacity:0; filter:alpha(opacity=0);\" href=\"javascript:gadgetSample2.bothLinks.delLink('acctLinks',";
		str1 += (iX+1);
		str1 += ");\" id=\"gadgetSample2_delAcctLink"+iX+"\">";
		str1 += "<img id='gadgetSample2_acctLink-del-img-"+iX;
		str1 += "' class='headerImage' style='border:none; opacity:0; filter:alpha(opacity=0);' title='";
		str1 += "Delete Link #".replace("\'", "&#39;")+(iX+1);
		str1 += "' src='./images/delete.gif' border=0 />";
		str1 += "</a>";
		str1 += "</td></tr>";
	}
	if (gadgetSample2.acctLinks.data.addMore == 'true') {
		str1 += "<tr><td colspan='2' align='right'><a href='accountlinkbrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=4'";
		str1 += " title='"+"More Account Links"+"'>";
		str1 += "More Account Links"+"</a>";
		str1 += "</td></tr>";
	}
	str1 += '</table>';
	$('gadgetSample2_acctLinksHTML').innerHTML = str1;
}
gadgetSample2.acctLinks.deleteLink = function() {
	if (!gadgetSample2.bothLinks.validateDeleteLink('acctLinks')) { return false; }
	try {
		YUI().use('io', 'json', 'json-parse', function(Y) {
        	Y.io('gadgetSample2_deleteAcctLink', {
            	method: 'POST',
            	data: $j('#gadgetSample2_formDeleteAcctLink').serialize(),
            	sync: true,
            	on: {
                	success: function (tranId, o) {
                    	var json = Y.JSON.parse(o.responseText);
                    	if (json.inputErrors) {
                    		var thisValidationId = 'gadgetSample2.acctLinks.deleteLink';
                    		validationMessages[thisValidationId] = {};
                    		var myValidations = [];
                    		for (var key in json.inputErrors) {
                    			var errorArray = json.inputErrors[key];
                    			for (var iX=0; iX<errorArray.length; iX++) {
                    				myValidations.push(errorArray[iX]);
                    			}
                    			if (myValidations.length > 0) {
                    				validationMessages[thisValidationId][key]
                    					= myValidations;
                    			}
                    		}
                    		formatErrMsgGlobal(thisValidationId);
                        	$j('#gadgetSample2_deleteAcctLinkButtonRow').show();
                    	} else if (json.operationErrors) {
                    		var myMessage = '';
                    		if (json.operationErrors) {
                    			myMessage = json.operationErrors[0];
                    		}
                    		$('gadgetSample2_formDeleteAcctLink_resultMsg').innerHTML = wrapDisplayMessage(myMessage); 
                        	$j('#gadgetSample2_deleteAcctLinkButtonRow').show();
                    	} else {
                    		$j('#gadgetSample2_deleteAcctLinkButtonRow').hide();
                    		$('gadgetSample2_formDeleteAcctLink_resultMsg').innerHTML = wrapDisplayMessage('Data Deleted Successfully!');
                    		setTimeout(function () {
                    			$j("#gadgetSample2_acctLinksNewLink").slideUp(200).fadeOut(200);
                    			gadgetSample2.acctLinks.reloadAcctLink();
                    		}, 500);
                    		setTimeout(function () {
                        		$j('#gadgetSample2_deleteAcctLinkButtonRow').show();
                        		$('gadgetSample2_formDeleteAcctLink_resultMsg').innerHTML = '';
                        		gadgetSample2.bothLinks.clearNewLink('acctLinks');
                        		gadgetSample2.bothLinks.clearLinkDeleteESign('acctLinks');
                    		}, 1000);
                    	}
                    },
                    failure: function (tranId, o) {
                		$('gadgetSample2_formDeleteAcctLink_resultMsg').innerHTML = wrapDisplayMessage(M_CommErr_CnctVelos);
                    	$j('#gadgetSample2_deleteAcctLinkButtonRow').show();
                    }
                }
            });
		});
	} catch(e) {}
}
gadgetSample2.acctLinks.saveNewLink = function() {
	if (!gadgetSample2.bothLinks.validateNewLink('acctLinks')) { return false; }
	try {
		YUI().use('io', 'json', 'json-parse', function(Y) {
        	Y.io('gadgetSample2_saveAcctLink', {
            	method: 'POST',
            	data: $j('#gadgetSample2_formNewAcctLink').serialize(),
            	sync: true,
            	on: {
                	success: function (tranId, o) {
                    	var json = Y.JSON.parse(o.responseText);
                    	if (json.inputErrors) {
                    		var thisValidationId = 'gadgetSample2.acctLinks.newLink';
                    		validationMessages[thisValidationId] = {};
                    		var myValidations = [];
                    		for (var key in json.inputErrors) {
                    			var errorArray = json.inputErrors[key];
                    			for (var iX=0; iX<errorArray.length; iX++) {
                    				myValidations.push(errorArray[iX]);
                    			}
                    			if (myValidations.length > 0) {
                    				validationMessages[thisValidationId][key]
                    					= myValidations;
                    			}
                    		}
                    		formatErrMsgGlobal(thisValidationId);
                    		$j('#gadgetSample2_newAcctLinkButtonRow').show();
                    	}  else if (json.operationErrors) {
                    		var myMessage = '';
                    		if (json.operationErrors) {
                    			myMessage = json.operationErrors[0];
                    		}
                    		$('gadgetSample2_formNewAcctLink_resultMsg').innerHTML = wrapDisplayMessage(myMessage); 
                        	$j('#gadgetSample2_newAcctLinkButtonRow').show();
                    	} else {
                    		$j('#gadgetSample2_newAcctLinkButtonRow').hide();
                    		$('gadgetSample2_formNewAcctLink_resultMsg').innerHTML = wrapDisplayMessage('Data saved successfully.');
                    		setTimeout(function () {
                    			$j("#gadgetSample2_acctLinksNewLink").slideUp(200).fadeOut(200);
                    			gadgetSample2.acctLinks.reloadAcctLink();
                        		$('gadgetSample2_formNewAcctLink_resultMsg').innerHTML = '';
                        		$j('#gadgetSample2_newAcctLinkButtonRow').show();
                        		gadgetSample2.bothLinks.clearNewLink('acctLinks');
                        		gadgetSample2.bothLinks.clearLinkESign('acctLinks');
                    		}, 500);
                    	}
                    },
                    failure: function (tranId, o) {
                		$j('#gadgetSample2_newAcctLinkButtonRow').show();
                		$('gadgetSample2_formNewAcctLink_resultMsg').innerHTML = wrapDisplayMessage(M_CommErr_CnctVelos);
                    }
                }
            });
		});
	} catch(e) {}
}

gadgetSample2.bothLinks.truncateLinkName = function(str) {
	if (!str) { return ''; }
	var newText = truncateLongName(str, 25);
	if (str.length > 25) {
		newText = htmlEncode(newText)+"</a>&nbsp;<img title='"+htmlEncode(str)
				+ "' src='./images/More.png' style='width:20px; height:10px;' border=0 />";
	} else {
		newText = htmlEncode(newText)+"</a>";
	}
	return newText;
}
gadgetSample2.bothLinks.openNewLink = function(whichLink) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return; }
	$j("#gadgetSample2_"+whichLink+"NewLink").slideDown(200).fadeIn(200);
	gadgetSample2.bothLinks.clearNewLinkErrMsg(whichLink);
	gadgetSample2.bothLinks.clearLinkESign(whichLink);
}
gadgetSample2.bothLinks.cancelNewLink = function(whichLink) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return; }
	gadgetSample2.bothLinks.clearNewLink(whichLink);
	gadgetSample2.bothLinks.clearLinkESign(whichLink);
	gadgetSample2.bothLinks.clearLinkDeleteESign(whichLink);
	$j("#gadgetSample2_"+whichLink+"NewLink").slideUp(200).fadeOut(200);
}
gadgetSample2.bothLinks.screenActionForLinks = function(whichLink) {
	if (whichLink != "acctLink" && whichLink != "myLink") { return; }
	YUI().use('node', 'event-mouseenter', function(Y) {
		var onMouseoverDelLink = function(e) {
			var trNum = -1;
			if (e.target.get('tagName') == 'TD') {
				var pos1 = ('gadgetSample2_'+whichLink+'-row-').length;
				trNum = e.target.get('parentNode').get('id').substring(pos1,pos1+1);
			} else if (e.target.get('tagName') == 'A') {
				var pos1 = ('gadgetSample2_'+whichLink+'-row-').length;
				trNum = e.target.get('parentNode').get('parentNode').get('id').substring(pos1,pos1+1);
			} else if (e.target.get('tagName') == 'IMG') {
				var pos1 = ('gadgetSample2_'+whichLink+'-edt-img-').length;
				trNum = e.target.get('id').substring(pos1,pos1+1);
			}
			if (trNum == -1) { return; }
			$j('#gadgetSample2_'+whichLink+'-row-'+trNum).addClass('gdt-highlighted-row');
			if (!Y.one('#gadgetSample2_'+whichLink+'-edt-img-'+trNum)) { return; }
			Y.one('#gadgetSample2_'+whichLink+'-edt-img-'+trNum).setStyle('opacity', '1');
			Y.one('#gadgetSample2_'+whichLink+'-edt-img-'+trNum).get('parentNode').setStyle('opacity', '1');
			Y.one('#gadgetSample2_'+whichLink+'-del-img-'+trNum).setStyle('opacity', '1');
			Y.one('#gadgetSample2_'+whichLink+'-del-img-'+trNum).get('parentNode').setStyle('opacity', '1');
		}
		var onMouseoutDelLink = function(e) {
			var trNum = -1;
			if (e.target.get('tagName') == 'TD') {
				var pos1 = ('gadgetSample2_'+whichLink+'-row-').length;
				trNum = e.target.get('parentNode').get('id').substring(pos1,pos1+1);
			} else if (e.target.get('tagName') == 'A') {
				var pos1 = ('gadgetSample2_'+whichLink+'-row-').length;
				trNum = e.target.get('parentNode').get('parentNode').get('id').substring(pos1,pos1+1);
			} else if (e.target.get('tagName') == 'IMG') {
				var pos1 = ('gadgetSample2_'+whichLink+'-edt-img-').length;
				trNum = e.target.get('id').substring(pos1,pos1+1);
			}
			if (trNum == -1) { return; }
			$j('#gadgetSample2_'+whichLink+'-row-'+trNum).removeClass('gdt-highlighted-row');
			if (!Y.one('#gadgetSample2_'+whichLink+'-edt-img-'+trNum)) { return; }
			Y.one('#gadgetSample2_'+whichLink+'-edt-img-'+trNum).setStyle('opacity', '0');
			Y.one('#gadgetSample2_'+whichLink+'-edt-img-'+trNum).get('parentNode').setStyle('opacity', '0');
			Y.one('#gadgetSample2_'+whichLink+'-del-img-'+trNum).setStyle('opacity', '0');
			Y.one('#gadgetSample2_'+whichLink+'-del-img-'+trNum).get('parentNode').setStyle('opacity', '0');
		}
		for(var iX=0; iX<10; iX++) {
			if (!Y.one('#gadgetSample2_'+whichLink+'-row-'+iX)) { continue; }
			Y.one('#gadgetSample2_'+whichLink+'-row-'+iX).on('mousemove', onMouseoverDelLink);
			Y.one('#gadgetSample2_'+whichLink+'-row-'+iX).on('mouseenter', onMouseoverDelLink);
			Y.one('#gadgetSample2_'+whichLink+'-row-'+iX).on('mouseover', onMouseoverDelLink);
			Y.one('#gadgetSample2_'+whichLink+'-row-'+iX).on('mouseleave', onMouseoutDelLink);
			Y.one('#gadgetSample2_'+whichLink+'-row-'+iX).on('mouseout', onMouseoutDelLink);
			var whichLinkFirstCap = whichLink.charAt(0).toUpperCase()+whichLink.substring(1);
			if (!Y.one('#gadgetSample2_del'+whichLinkFirstCap+iX)) { continue; }
			Y.one('#gadgetSample2_del'+whichLinkFirstCap+iX).setStyle('opacity', '0');
			Y.one('#gadgetSample2_del'+whichLinkFirstCap+iX).setStyle('opacity', '0');
		}
	});
}
gadgetSample2.bothLinks.editLink = function(whichLink, num) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return; }
	gadgetSample2.bothLinks.openNewLink(whichLink);
	$j('#gadgetSample2_'+whichLink+'FormNewLinkHeader').hide();
	$j('#gadgetSample2_'+whichLink+'FormEditLinkHeader').show();
	$j('#gadgetSample2_'+whichLink+'FormDeleteLinkDiv').hide();
	$('gadgetSample2_'+whichLink+'FormEditLinkHeadderLabel').innerHTML = '<b>Edit Link #'+num+'</b>';
	$j('#gadgetSample2_'+whichLink+'FormNewLinkDiv').slideDown(200).fadeIn(200);
	$('gadgetSample2JB.'+whichLink+'LinkDisplay').focus();
	$('gadgetSample2JB.'+whichLink+'LinkUrl').value = gadgetSample2[whichLink].data.array[num-1].uri;
	$('gadgetSample2JB.'+whichLink+'LinkDisplay').value = gadgetSample2[whichLink].data.array[num-1].desc;
	$('gadgetSample2JB.'+whichLink+'LinkSection').value = gadgetSample2[whichLink].data.array[num-1].section ?
			gadgetSample2[whichLink].data.array[num-1].section : '';
	$('gadgetSample2JB.'+whichLink+'LinkId').value = gadgetSample2[whichLink].data.array[num-1].lnkid;
}
gadgetSample2.bothLinks.delLink = function(whichLink, num) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return; }
	gadgetSample2.bothLinks.openNewLink(whichLink);
	var moreString = '';
	if (gadgetSample2[whichLink].data.array[num-1].desc.length > 19) { moreString = '...'; }
	$j('#gadgetSample2_'+whichLink+'FormNewLinkDiv').hide();
	$j('#gadgetSample2_'+whichLink+'FormDeleteLinkDiv').slideDown(200).fadeIn(200);
	gadgetSample2.bothLinks.clearLinkDeleteESign(whichLink);
	$('gadgetSample2_'+whichLink+'FormDeleteLinkHeadderLabel').innerHTML = '<b>Delete Link #'+num+': '+
		htmlEncode(truncateLongName(gadgetSample2[whichLink].data.array[num-1].desc, 19))+
		moreString+'</b>';
	$('gadgetSample2JB.'+whichLink+'DeleteLinkId').value = gadgetSample2[whichLink].data.array[num-1].lnkid;
	$('gadgetSample2JB.'+whichLink+'DeleteESign').focus();
}
gadgetSample2.bothLinks.addNewLink = function(whichLink) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return; }
	gadgetSample2.bothLinks.openNewLink(whichLink);
	$j('#gadgetSample2_'+whichLink+'FormDeleteLinkDiv').slideUp(200).fadeOut(200);
	$j('#gadgetSample2_'+whichLink+'FormNewLinkDiv').slideDown(200).fadeIn(200);
	$j('#gadgetSample2_'+whichLink+'FormNewLinkHeader').show();
	$j('#gadgetSample2_'+whichLink+'FormEditLinkHeader').hide();
	gadgetSample2.bothLinks.clearNewLink(whichLink);
	gadgetSample2.bothLinks.clearLinkESign(whichLink);
	$('gadgetSample2JB.'+whichLink+'LinkDisplay').focus();
}
gadgetSample2.bothLinks.clearNewLink = function(whichLink) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return; }
	$('gadgetSample2JB.'+whichLink+'LinkUrl').value = '';
	$('gadgetSample2JB.'+whichLink+'LinkDisplay').value = '';
	$('gadgetSample2JB.'+whichLink+'LinkSection').value = '';
	$('gadgetSample2JB.'+whichLink+'ESign').value = '';
	$('gadgetSample2JB.'+whichLink+'DeleteESign').value = '';
	$('gadgetSample2JB.'+whichLink+'DeleteLinkId').value = '';
	$('gadgetSample2JB.'+whichLink+'LinkId').value = '';
	gadgetSample2.bothLinks.clearNewLinkErrMsg(whichLink);
};
gadgetSample2.bothLinks.clearNewLinkErrMsg = function(whichLink) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return; }
	if ($('gadgetSample2JB.'+whichLink+'LinkUrl_error')) { $('gadgetSample2JB.'+whichLink+'LinkUrl_error').innerHTML = ''; }
	if ($('gadgetSample2JB.'+whichLink+'LinkDisplay_error')) { $('gadgetSample2JB.'+whichLink+'LinkDisplay_error').innerHTML = ''; }
	if ($('gadgetSample2JB.'+whichLink+'LinkSection_error')) { $('gadgetSample2JB.'+whichLink+'LinkSection_error').innerHTML = ''; }
	if ($('gadgetSample2JB.'+whichLink+'ESign_error')) { $('gadgetSample2JB.'+whichLink+'ESign_error').innerHTML = ''; }
	if ($('gadgetSample2JB.'+whichLink+'DeleteESign_error')) { $('gadgetSample2JB.'+whichLink+'DeleteESign_error').innerHTML = ''; }
	if (whichLink == "myLinks") {
		if ($('gadgetSample2_formNewLink_resultMsg')) { $('gadgetSample2_formNewLink_resultMsg').innerHTML = ''; }
		if ($('gadgetSample2_formDeleteLink_resultMsg')) { $('gadgetSample2_formDeleteLink_resultMsg').innerHTML = ''; }
	} else {
		if ($('gadgetSample2_formNewAcctLink_resultMsg')) { $('gadgetSample2_formNewAcctLink_resultMsg').innerHTML = ''; }
		if ($('gadgetSample2_formDeleteAcctLink_resultMsg')) { $('gadgetSample2_formDeleteAcctLink_resultMsg').innerHTML = ''; }
	}
};
gadgetSample2.bothLinks.clearLinkESign = function(whichLink) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return; }
	if ($('gadgetSample2JB.'+whichLink+'ESign_eSign')) {
		$('gadgetSample2JB.'+whichLink+'ESign_eSign').innerHTML = ''; 
		$j('#gadgetSample2JB\\.'+whichLink+'ESign_eSign').removeClass('validation-fail');
		$j('#gadgetSample2JB\\.'+whichLink+'ESign_eSign').removeClass('validation-pass');
	}
	if ($('gadgetSample2JB.'+whichLink+'ESign')) { $('gadgetSample2JB.'+whichLink+'ESign').value = ''; }
}
gadgetSample2.bothLinks.clearLinkDeleteESign = function(whichLink) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return; }
	if ($('gadgetSample2JB.'+whichLink+'DeleteESign_eSign')) {
		$('gadgetSample2JB.'+whichLink+'DeleteESign_eSign').innerHTML = ''; 
		$j('#gadgetSample2JB\\.'+whichLink+'DeleteESign_eSign').removeClass('validation-fail');
		$j('#gadgetSample2JB\\.'+whichLink+'DeleteESign_eSign').removeClass('validation-pass');
	}
	$('gadgetSample2JB.'+whichLink+'DeleteESign').value = '';
}
gadgetSample2.bothLinks.validateDeleteLink = function(whichLink) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return false; }
	var thisValidationId = 'gadgetSample2.'+whichLink+'.deleteLink';
	validationMessages[thisValidationId] = {};
	gadgetSample2.bothLinks.clearNewLinkErrMsg(whichLink);
	gadgetSample2.bothLinks.clearLinkESign(whichLink); // clear the other e-sign
	
	var myLinksESValidations = [];
	var myLinksESKey = 'gadgetSample2JB.'+whichLink+'DeleteESign';
	if (!$(myLinksESKey).value) {
		myLinksESValidations.push('This field is required.');
	}
	if (myLinksESValidations.length > 0) {
		validationMessages[thisValidationId][myLinksESKey] = myLinksESValidations;
	}
	
	return formatErrMsgGlobal(thisValidationId);
}
gadgetSample2.bothLinks.validateNewLink = function(whichLink) {
	if (whichLink != "acctLinks" && whichLink != "myLinks") { return false; }
	var thisValidationId = 'gadgetSample2.'+whichLink+'.newLink';
	validationMessages[thisValidationId] = {};
	gadgetSample2.bothLinks.clearNewLinkErrMsg(whichLink);
	gadgetSample2.bothLinks.clearLinkDeleteESign(whichLink); // clear the other e-sign
	
	var myLinksDisplayValidations = []; // The word my in variable name here means local variable
	var myLinksDisplayKey = 'gadgetSample2JB.'+whichLink+'LinkDisplay';
	if ($(myLinksDisplayKey).value) {
		$(myLinksDisplayKey).value = jQuery.trim($(myLinksDisplayKey).value);
	}
	if (!$(myLinksDisplayKey).value || $(myLinksDisplayKey).value.length == 0) {
		myLinksDisplayValidations.push('This field is required.');
	}
	if (myLinksDisplayValidations.length > 0) {
		validationMessages[thisValidationId][myLinksDisplayKey] = myLinksDisplayValidations;
	}

	var myLinksUrlValidations = [];
	var myLinksUrlKey = 'gadgetSample2JB.'+whichLink+'LinkUrl';
	if ($(myLinksUrlKey).value) {
		$(myLinksUrlKey).value = jQuery.trim($(myLinksUrlKey).value);
	}
	if (!$(myLinksUrlKey).value || $(myLinksUrlKey).value.length == 0) {
		myLinksUrlValidations.push('This field is required.');
	}
	if (myLinksUrlValidations.length > 0) {
		validationMessages[thisValidationId][myLinksUrlKey] = myLinksUrlValidations;
	}
	
	var myLinksSectionValidations = [];
	var myLinksSectionKey = 'gadgetSample2JB.'+whichLink+'LinkSection';
	/*
	if ($(myLinksSectionKey).value) {
		$(myLinksSectionKey).value = jQuery.trim($(myLinksSectionKey).value);
	}
	if (!$(myLinksSectionKey).value) {
		myLinksSectionValidations.push('This field is required.');
	}
	*/
	if (myLinksSectionValidations.length > 0) {
		validationMessages[thisValidationId][myLinksSectionKey] = myLinksSectionValidations;
	}
	
	
	var myLinksESValidations = [];
	var myLinksESKey = 'gadgetSample2JB.'+whichLink+'ESign';
	if (!$(myLinksESKey).value) {
		myLinksESValidations.push('This field is required.');
	}
	if (myLinksESValidations.length > 0) {
		validationMessages[thisValidationId][myLinksESKey] = myLinksESValidations;
	}
	
	return formatErrMsgGlobal(thisValidationId);
}

gadgetSample2.screenAction = function() {
	YUI({
	    gallery: 'gallery-2012.06.20-20-07'
	}).use('gallery-placeholder', 'node', function(Y) {
		var addToolTipToButton = function(buttonId) {
			Y.one(buttonId).on('mousemove', onMousemoveToolTip);
			Y.one(buttonId).on('mouseleave', onMouseleaveToolTip);
			Y.one(buttonId).on('mouseout', onMouseleaveToolTip);
		};
		Y.one('#gadgetSample2_study').plug(Y.Plugin.Placeholder, {
			text : '<%=MC.M_StdTitle_Kword%>',
			hideOnFocus : true
		});
		Y.one('#gadgetSample2_patient').plug(Y.Plugin.Placeholder, {
			text : 'Patient ID',
			hideOnFocus : true
		});
		Y.one('#gadgetSample2_patientStudy').plug(Y.Plugin.Placeholder, {
			text : 'Patient Study ID',
			hideOnFocus : true
		});
		
		addToolTipToButton('#gadgetSample2_a_help');
		addToolTipToButton('#gadgetSample2_studySearchButton');
		addToolTipToButton('#gadgetSample2_studyAdvancedSearchButton');
		addToolTipToButton('#gadgetSample2_patientSearchButton');
		addToolTipToButton('#gadgetSample2_patientStudySearchButton');
	});
	
	$j("#gadgetSample2_studySearchButton").button();
	$j("#gadgetSample2_patientSearchButton").button();
	$j("#gadgetSample2_patientStudySearchButton").button();
	$j("#gadgetSample2_studyAdvancedSearchButton").button();
	
	$j("#gadgetSample2_studySearchButton").click(function() {
		gadgetSample2.submitStudy();
	});
	$j("#gadgetSample2_patientSearchButton").click(function() {
		gadgetSample2.submitPatient();
	});
	$j("#gadgetSample2_patientStudySearchButton").click(function() {
		gadgetSample2.submitPatientStudy();
	});
	$j("#gadgetSample2_studyAdvancedSearchButton").click(function() {
		gadgetSample2.submitStudyAdvanced();
	});
	
	var setIdToSelectStudy = function() {
		var mySelect = document.getElementsByName("gadgetSample2JB.studyForPatStudySearch")[0]
		mySelect.id = 'gadgetSample2JB.studyForPatStudySearch';
		$j("#gadgetSample2JB\\.studyForPatStudySearch").addClass("gdt-placeholder-select-study");
		
		var mySelect1 = document.getElementsByName("gadgetSample2JB.studyForPatSearch")[0]
		mySelect1.id = 'gadgetSample2JB.studyForPatSearch';
		$j("#gadgetSample2JB\\.studyForPatSearch").addClass("gdt-placeholder-select-study");
	}();

	var setPlaceholderToSelectStudy = function() {
		$j(".gdt-placeholder-select-study").each(function() {
			$j(this).css('width','177px');
			var opts = $j(this).attr('options')
			for (var iX=0; iX<opts.length; iX++) {
				if (opts[iX].value == "" || opts[iX].value == "0") {
					opts[iX].style.color = 'gray';
					opts[iX].text = "Study Number";
				} else { opts[iX].style.color = 'black'; }
			}
		});
		$j(".gdt-placeholder-select-study").change(function() {
			if($j(this).val() == "" || $j(this).val() == "0") {
				$j(this).css('color','gray');
			} else $j(this).css('color','black');
		});
		$j(".gdt-placeholder-select-study").change();
	}();
	
	$j("#gadgetSample2_patientOnStudyTurnedOn").change(function() {
		if ($j(this).attr('checked')) {
			$j("#gadgetSample2JB\\.studyForPatSearch").show("blind");
			$('gadgetSample2_formPatient').action = 'studypatients.jsp?srcmenu=tdmenubaritem5&selectedTab=2&openMode=F';
		} else {
			$j("#gadgetSample2JB\\.studyForPatSearch").hide("blind");
			$('gadgetSample2_formPatient').action = 'allPatient.jsp?srcmenu=tdmenubaritem5&selectedTab=1&searchFrom=initial';
		}
	});
	if ($j("#gadgetSample2_patientOnStudyTurnedOn")) {
		if ($j("#gadgetSample2_patientOnStudyTurnedOn").attr('checked')) {
			$j("#gadgetSample2JB\\.studyForPatSearch").show();
		} else {
			$j("#gadgetSample2JB\\.studyForPatSearch").hide();
		}
	}

	// --- My Links section starts here
	$j("#gadgetSample2_saveNewMyLinkButton").button();
	$j("#gadgetSample2_cancelNewMyLinkButton").button();
	$j("#gadgetSample2_deleteMyLinkButton").button();
	$j("#gadgetSample2_cancelMyLinkButton").button();
	
	$j("#gadgetSample2_saveNewMyLinkButton").click(function() {
		gadgetSample2.myLinks.saveNewLink();
	});
	$j("#gadgetSample2_cancelNewMyLinkButton").click(function() {
		gadgetSample2.bothLinks.cancelNewLink('myLinks');
	});
	$j("#gadgetSample2_deleteMyLinkButton").click(function() {
		gadgetSample2.myLinks.deleteLink();
	});
	$j("#gadgetSample2_cancelMyLinkButton").click(function() {
		gadgetSample2.bothLinks.cancelNewLink('myLinks');
	});
	gadgetSample2.myLinks.reloadMyLink();
	// --- My Links section ends here
	
	// --- Acct Links section starts here
	$j("#gadgetSample2_saveNewAcctLinkButton").button();
	$j("#gadgetSample2_cancelNewAcctLinkButton").button();
	$j("#gadgetSample2_deleteAcctLinkButton").button();
	$j("#gadgetSample2_cancelAcctLinkButton").button();
	
	$j("#gadgetSample2_saveNewAcctLinkButton").click(function() {
		gadgetSample2.acctLinks.saveNewLink();
	});
	$j("#gadgetSample2_cancelNewAcctLinkButton").click(function() {
		gadgetSample2.bothLinks.cancelNewLink("acctLinks");
	});
	$j("#gadgetSample2_deleteAcctLinkButton").click(function() {
		gadgetSample2.acctLinks.deleteLink();
	});
	$j("#gadgetSample2_cancelAcctLinkButton").click(function() {
		gadgetSample2.bothLinks.cancelNewLink("acctLinks");
	});
	gadgetSample2.acctLinks.reloadAcctLink();
	// --- Acct Links section ends here

};
gadgetSample2.beforeSubmitStudy = function(formobj) {
	try {
		if (!$j("#gadgetSample2_study").hasClass('yui3-placeholder-text')) {
			formobj.searchCriteria.value = formobj.gadgetSample2_study.value;
		}
	} catch(e) {
		// console.log('In gadgetSample2.beforeSubmitStudy:'+e);
	}
}
gadgetSample2.submitStudy = function() {
	try {
		if (!$j("#gadgetSample2_study").hasClass('yui3-placeholder-text')) {
			var mySearchCriteria = $j("#gadgetSample2_formStudy").children('#searchCriteria');
			if (mySearchCriteria && mySearchCriteria[0]) {
				mySearchCriteria[0].value = $j("#gadgetSample2_study").val();
			}
		}
		$("gadgetSample2_formStudy").action = "studybrowserpg.jsp";
		$("gadgetSample2_formStudy").method = "POST";
		$("gadgetSample2_formStudy").submit();
	} catch(e) {
		// console.log('In gadgetSample2.submitStudy:'+e);
	}
}
gadgetSample2.submitStudyAdvanced = function() {
	try {
		if (!$j("#gadgetSample2_study").hasClass('yui3-placeholder-text')) {
			var mySearchCriteria = $j("#gadgetSample2_formStudy").children('#searchCriteria');
			if (mySearchCriteria && mySearchCriteria[0]) {
				mySearchCriteria[0].value = $j("#gadgetSample2_study").val();
			}
		}
		$("gadgetSample2_formStudy").action = "advStudysearchpg.jsp";
		$("gadgetSample2_formStudy").method = "POST";
		$("gadgetSample2_formStudy").submit();
	} catch(e) {
		// console.log('In gadgetSample2.submitStudyAdvanced:'+e);
	}
}
gadgetSample2.beforeSubmitPatient = function(formobj) {
	if (!$j("#gadgetSample2_patientOnStudyTurnedOn")) { return false; }
	if ($j("#gadgetSample2_patientOnStudyTurnedOn").attr('checked')) {
		try {
			var selectedStudyVal = formobj['gadgetSample2JB.studyForPatSearch'].value
			formobj.studyId.value = selectedStudyVal;
			if (!$j("#gadgetSample2_patient").hasClass('yui3-placeholder-text')) {
				formobj.patientid.value = formobj.gadgetSample2_patient.value;
			}
		} catch(e) {
			// console.log('In gadgetSample2.beforeSubmitPatient:'+e);
		}
	} else {
		try {
			if (!$j("#gadgetSample2_patient").hasClass('yui3-placeholder-text')) {
				formobj.searchPatient.value = formobj.gadgetSample2_patient.value;
			}
		} catch(e) {
			// console.log('In gadgetSample2.beforeSubmitPatient:'+e);
		}
	}
	return true;
}
gadgetSample2.submitPatient = function() {
	if (!$j("#gadgetSample2_patientOnStudyTurnedOn")) { return; }
	if ($j("#gadgetSample2_patientOnStudyTurnedOn").attr('checked')) {
		try {
			var selectedStudyVal = $('gadgetSample2JB.studyForPatSearch').value
			var mySearchStudy = $j("#gadgetSample2_formPatient").children('#studyId');
			if (mySearchStudy && mySearchStudy[0]) {
				mySearchStudy[0].value = selectedStudyVal;
			}
			if (!$j("#gadgetSample2_patient").hasClass('yui3-placeholder-text')) {
				var mySearchCriteria = $j("#gadgetSample2_formPatient").children('#patientid');
				if (mySearchCriteria && mySearchCriteria[0]) {
					mySearchCriteria[0].value = $j("#gadgetSample2_patient").val();
				}
			}
			$('gadgetSample2_formPatient').action = 'studypatients.jsp?srcmenu=tdmenubaritem5&selectedTab=2&openMode=F';
		} catch(e) {
			// console.log('In gadgetSample2.submitPatient:'+e);
			return;
		}
	} else {
		try {
			if (!$j("#gadgetSample2_patient").hasClass('yui3-placeholder-text')) {
				var mySearchCriteria = $j("#gadgetSample2_formPatient").children('#searchPatient');
				if (mySearchCriteria && mySearchCriteria[0]) {
					mySearchCriteria[0].value = $j("#gadgetSample2_patient").val();
				}
			}
			$("gadgetSample2_formPatient").action = "allPatient.jsp?srcmenu=tdmenubaritem5&selectedTab=1&searchFrom=initial";
		} catch(e) {
			// console.log('In gadgetSample2.submitPatient:'+e);
			return;
		}
	}
	$("gadgetSample2_formPatient").method = "POST";
	$("gadgetSample2_formPatient").submit();
}
gadgetSample2.beforeSubmitPatientStudy = function(formobj) {
	try {
		var selectedStudyVal = formobj['gadgetSample2JB.studyForPatStudySearch'].value
		formobj.studyId.value = selectedStudyVal;
		if (!$j("#gadgetSample2_patientStudy").hasClass('yui3-placeholder-text')) {
			formobj.pstudyid.value = formobj.gadgetSample2_patientStudy.value;
		}
	} catch(e) {
		// console.log('In gadgetSample2.beforeSubmitPatientStudy:'+e);
		return false;
	}
	return true;
}
gadgetSample2.submitPatientStudy = function() {
	try {
		var selectedStudyVal = $('gadgetSample2JB.studyForPatStudySearch').value
		var mySearchStudy = $j("#gadgetSample2_formPatientStudy").children('#studyId');
		if (mySearchStudy && mySearchStudy[0]) {
			mySearchStudy[0].value = selectedStudyVal;
		}
		if (!$j("#gadgetSample2_patientStudy").hasClass('yui3-placeholder-text')) {
			var mySearchCriteria = $j("#gadgetSample2_formPatientStudy").children('#pstudyid');
			if (mySearchCriteria && mySearchCriteria[0]) {
				mySearchCriteria[0].value = $j("#gadgetSample2_patientStudy").val();
			}
		}
		$("gadgetSample2_formPatientStudy").action = "studypatients.jsp?srcmenu=tdmenubaritem5&selectedTab=2&openMode=F";
		$("gadgetSample2_formPatientStudy").method = "POST";
		$("gadgetSample2_formPatientStudy").submit();
	} catch(e) {
		alert(e);
		// console.log('In gadgetSample2.submitPatientStudy:'+e);
	}
}

screenActionRegistry['gadgetSample2'] = gadgetSample2.screenAction;
</script>
