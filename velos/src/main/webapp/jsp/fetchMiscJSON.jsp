<%--
This JSP is intended to be used with getlookupmodal.jsp. When the user makes a selection 
from the lookup result, the callback function fetchLookupDetails() gets called. This callback
function is supposed call this JSP to fetch the details.
 --%>

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList,java.util.HashMap,org.json.*"%>
<%@page import="com.velos.eres.business.common.SiteDao,com.velos.eres.business.common.UserDao"%>
<%@page import="com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
<jsp:useBean id="addressB" scope="request" class="com.velos.eres.web.address.AddressJB"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>

<%!
private static final String ORG_DETAILS = "orgDetails";
private static final String USER_DETAILS = "userDetails";
%>

<%
    request.setCharacterEncoding("UTF-8");
    response.setContentType("application/json");
    // response.setContentType("text/html");

	HttpSession tSession = request.getSession(true);
    JSONObject jsObj = new JSONObject();
    if (!sessionmaint.isValidSession(tSession)) {
        // Session is invalid; print an error and exit
        response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
        jsObj.put("error", new Integer(-1));
        jsObj.put("errorMsg", MC.M_UsrNot_LoggedIn);
	    out.println(jsObj.toString());
        return;
    }
    
	String reqType = request.getParameter("reqType");
	if(reqType == null || "".equals(reqType) || reqType.equals("null") || reqType.equals("")) {
	    // A valid reqType is required; print an error and exit
        jsObj.put("error", new Integer(-2));
        jsObj.put("errorMsg", MC.Lkp_ReqTypeMissing);
	    out.println(jsObj.toString());
	    return;
	}
	
    // Define page-wide variables here
    StringBuffer sb = new StringBuffer();
    
    // Handle various reqType here
	if (ORG_DETAILS.equals(reqType)) {
		int siteId = StringUtil.stringToNum(request.getParameter("siteId"));
		if (siteId < 1) {
	        jsObj.put("error", new Integer(-2));
	        jsObj.put("errorMsg", MC.Lkp_InvalidInputParam);
		    out.println(jsObj.toString());
		    return;
		}
		
		// Check authorization
		SiteDao siteDao = siteB.getSitesByUser(
				StringUtil.stringToNum((String)tSession.getAttribute("userId")));
		ArrayList<Integer> siteList = siteDao.getSiteIds();
		boolean siteAccessible = false;
		for (int iX=0; iX<siteList.size(); iX++) {
			if ((siteList.get(iX)).intValue() == siteId) {
				siteAccessible = true;
				break;
			}
		}
		if (!siteAccessible) {
			jsObj.put("error", new Integer(-4));
			jsObj.put("errorMsg", LC.L_Access_Forbidden);
		    out.println(jsObj.toString());
		    return;
		}
		
		// Authroziation passed; get details
		siteB.setSiteId(siteId);
		siteB.getSiteDetails();
		int siteAddId= StringUtil.stringToNum(siteB.getSitePerAdd()); 
		addressB.setAddId(siteAddId);
		addressB.getAddressDetails();
		jsObj.put("addId", siteAddId);
		jsObj.put("name", siteB.getSiteName());
		jsObj.put("street", addressB.getAddPri());
		jsObj.put("city", addressB.getAddCity());
		jsObj.put("state", addressB.getAddState());
		jsObj.put("zip", addressB.getAddZip());
		jsObj.put("country", addressB.getAddCountry());
		jsObj.put("email", addressB.getAddEmail());
		jsObj.put("phone", addressB.getAddPhone());
		jsObj.put("tty", StringUtil.trueValue(addressB.getAddTty()));
		jsObj.put("fax", StringUtil.trueValue(addressB.getAddFax()));
		jsObj.put("url", StringUtil.trueValue(addressB.getAddUrl()));
	    jsObj.put("error", new Integer(0));
		out.println(jsObj.toString());
		return;
	}
	if (USER_DETAILS.equals(reqType)) {
		int userId = StringUtil.stringToNum(request.getParameter("userId"));
		if (userId < 1) {
	        jsObj.put("error", new Integer(-2));
	        jsObj.put("errorMsg", MC.Lkp_InvalidInputParam);
		    out.println(jsObj.toString());
		    return;
		}
		
		// Check authorization
		UserDao userDao = userB.getAccountUsersDetailsForLookup(StringUtil.stringToNum((String)tSession.getAttribute("accountId")));
		ArrayList<Integer> userIdList = userDao.getUsrIds();
		boolean userAccessible = false;
		for (int iX=0; iX<userIdList.size(); iX++) {
			if ((userIdList.get(iX)).intValue() == userId) {
				userAccessible = true;
				break;
			}
		}
		if (!userAccessible) {
			jsObj.put("error", new Integer(-4));
			jsObj.put("errorMsg", LC.L_Access_Forbidden);
		    out.println(jsObj.toString());
		    return;
		}
		
		// Authroziation passed; get details
		userB.setUserId(userId);
		userB.getUserDetails();
		jsObj.put("userId", userId);
		jsObj.put("firstName", userB.getUserFirstName());
		jsObj.put("middleName", userB.getUserMidName());
		jsObj.put("lastName", userB.getUserLastName());
		addressB.setAddId(StringUtil.stringToNum(userB.getUserPerAddressId()));
		addressB.getAddressDetails();
		jsObj.put("addId", userB.getUserPerAddressId());
		jsObj.put("streetAddress", addressB.getAddPri());
		jsObj.put("city", addressB.getAddCity());
		jsObj.put("state", addressB.getAddState());
		jsObj.put("zip", addressB.getAddZip());
		jsObj.put("country", addressB.getAddCountry());
		jsObj.put("email", addressB.getAddEmail());
		jsObj.put("phone", addressB.getAddPhone());
		jsObj.put("tty", StringUtil.trueValue(addressB.getAddTty()));
		jsObj.put("fax", StringUtil.trueValue(addressB.getAddFax()));
		jsObj.put("url", StringUtil.trueValue(addressB.getAddUrl()));
	    jsObj.put("error", new Integer(0));
		out.println(jsObj.toString());
		return;
	}
    
    jsObj.put("error", new Integer(-3));
    jsObj.put("errorMsg", MC.Lkp_InvalidReqType);
	out.println(jsObj.toString());
    // <html><head></head><body></body></html>
%>
