<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:useBean id="tokenJB" scope="request" class="com.velos.eres.web.userSession.UserSessionTokenJB"/>
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%><%@page import="com.velos.eres.service.util.*"%>
<html>

<head>

<title><%=MC.M_MngAcc_Org%><%--Manage Account >> Organizations*****--%></title>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
#serverpagination table {
    width: 98%;
}
</style>
</head>
<body onload="refreshPaginator();onLoad();">


<% 
String module="orgSearch" ;

String refreshBool = "false";

String src;
src= request.getParameter("srcmenu");
String calledFrom=request.getParameter("calledFrom");
HttpSession tSession = request.getSession(true); 

if(!"networkTabs".equals(calledFrom)){
%>


<jsp:include page="panel.jsp" flush="true" > 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   
<jsp:include page="ui-include.jsp" flush="true"/>
<script> 

var screenWidth = screen.width;
var screenHeight = screen.height;

function setFilterText(form){

	  <%-- form.searchByOrgName.value=<%=request.getParameter("searchByOrgName")==null?"":request.getParameter("searchByOrgName")%>; --%>  

	  if (form.siteType[form.siteType.selectedIndex].value != "")
	  form.drpdwnSiteType.value=form.siteType[form.siteType.selectedIndex].text ;

}


//modified on 29th March for organization sorting, bug #2072
function setOrder(formObj,orderBy) //orderBy column number 
{

	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";

	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
	}

	orderType=formObj.orderType.value;
	formObj.orderBy.value = orderBy;
	lsrc = formObj.srcmenu.value;	
	formObj.action="sitebrowser.jsp?mode=M&srcmenu="+lsrc+"&orderBy="+orderBy+"&orderType="+orderType;		
	setFilterText(formObj);
	formObj.submit(); 	

}


var paginate_org;     
$E.addListener(window, "load", function() {
	$('CSRFToken').value="<%=StringUtil.trueValue(tokenJB.createTokenForUser((String)tSession.getAttribute("userId")==null?0:StringUtil.stringToNum((String)tSession.getAttribute("userId"))))%>";
	paginate_org=new VELOS.Paginator('orgSearch',{
				sortDir:"asc", 
				sortKey:"ORG_NAME",
				defaultParam:"userId,accountId,grpId",
				filterParam:"orderBy,orderType,siteType,searchByOrgName,CSRFToken",
				dataTable:'serverpagination',
				navigation: true,
				rowSelection:[25,50,100,250]
				});
	paginate_org.runFilter();
	//elem = document.getElementById("yui-gen2")
				
				 });
	

function openOrgWin(siteId,src)
{
	   windowName = window.open("sitedetails.jsp?siteId="+siteId+"&mode=M&srcmenu="+src,"orgWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=900,height=700,left=10,top=100");
	   windowName.focus();
}
				 
				 
orgLink=function(elCell, oRecord, oColumn, oData)
{
var htmlStr="";
var siteId=oRecord.getData("PK_SITE");
var siteName=oRecord.getData("ORG_NAME");
var src='<%=src%>';
if (!siteId) siteId="";
//jQuery(".yui-dt0-col-ORG_NAME").css("width","50%");

	htmlStr="<A HREF=sitedetails.jsp?siteId="+siteId+"&mode=M&srcmenu="+src+">"+siteName+"</A> ";
	elCell.innerHTML=htmlStr;
}



<%}%>

</script>


<body class="yui-skin-sam">
<%if(!"networkTabs".equals(calledFrom)){ %>
<DIV class="tabDefTopN" id="divTab"> 
	<jsp:include page="accounttabs.jsp" flush="true"> 
	<jsp:param name="selectedTab" value="1"/>
	</jsp:include>
</DIV>
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="tabDefBotN" id="div1" style="height:80%;">')
	else
		document.write('<DIV class="tabDefBotN" id="div1">')
</SCRIPT>				

<Form name="sitebrowser" method="post" action="sitebrowser.jsp" onsubmit="setFilterText(document.specimen);" flush="true">

  <%
}
   

   if (sessionmaint.isValidSession(tSession))

	{

	String uName = (String) tSession.getValue("userName");

	 int pageRight = 0;

	   GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		

   pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSITES"));

 if (pageRight > 0 )

	{
		


	   int accountId=0;   

//modified on 29th March for organization sorting, bug #2072

		String orderBy="";
		orderBy=request.getParameter("orderBy");

		if (orderBy==null) orderBy="SITE_NAME";
				String orderType = "";
				orderType = request.getParameter("orderType");
		if (orderType == null)
			{
				orderType = "asc";
			} 
		
		String searchby="";
		searchby=(request.getParameter("searchby")==null)?"":request.getParameter("searchby");
		searchby=StringUtil.escapeSpecialCharSQL(searchby);

	   String acc = (String) tSession.getValue("accountId");

	   accountId = EJBUtil.stringToNum(acc);
	   String searchByOrgName="";
	   
	   searchByOrgName=request.getParameter("searchByOrgName")==null?"":request.getParameter("searchByOrgName");

	   String drpdwnSiteType="";
		CodeDao cdType = new CodeDao();
		cdType.getCodeValues("site_type");
		cdType.setCType("site_type");
		
		String ddSiteType =	request.getParameter("siteType")==null?"":request.getParameter("siteType");

		if (ddSiteType.equals("")){
			drpdwnSiteType=cdType.toPullDown("siteType");
		}
		else{
			drpdwnSiteType=cdType.toPullDown("siteType",EJBUtil.stringToNum(ddSiteType),true);
		}

	  // SiteDao siteDao = siteB.getByAccountId(accountId);


	  //modified on 29th March for organization sorting, bug 2072
	  SiteDao siteDao = new SiteDao();
	  if("networkTabs".equals(calledFrom)){
		 // siteDao.getOrgValues(orderBy,orderType,searchby.trim(),accountId);
	  }else
	   	  siteDao = siteB.getByAccountId(accountId,orderBy,orderType,ddSiteType,searchByOrgName);

	   ArrayList siteIds = siteDao.getSiteIds(); 

	   ArrayList siteTypes = siteDao.getSiteTypes();
	   ArrayList siteTypeIds = siteDao.getSiteTypeIds();

	   ArrayList siteNames = siteDao.getSiteNames();
	   ArrayList ctepIds = siteDao.getCtepIds();
	   ArrayList siteInfos = siteDao.getSiteInfos();

	   ArrayList siteParents = siteDao.getSiteParents();

	   ArrayList siteStats = siteDao.getSiteStats();



	   String siteType = null;

	   String siteName = null;
	   String ctepid = null;
	   String siteInfo = null;

	   String siteParent = null;

	   String siteStat = null;


	   int siteId=0;
	   int siteTypeId=0;


	   int len = siteIds.size();

	   int counter = 0;
	   CodeDao cd1 = new CodeDao();
	   cd1.getCodeValues("site_type");
	   String dSiteType="";

%>

  
 <%if(!"networkTabs".equals(calledFrom)){ %>
<input type="hidden" name="orderType" value="<%=orderType%>"> 
<input type="hidden" name="srcmenu" value="<%=src%>">
<input type="hidden" name="orderBy" value="<%=orderBy%>">
    <table width="100%" cellspacing="0" cellpadding="0" border=0 >
      <tr valign="center"> 
        <td width="20%"> 
          <P class="sectionheadings"><%=MC.M_Cur_AddedOrgs%><%--Currently added Organizations are*****--%>:</P>
        </td>
        <td width="7%"> <b><%=LC.L_Search_By%></b></td>
        <td width="10%"><%=LC.L_Organization_Name%>:</td>
        <td width="6%"><input type="text" name="searchByOrgName" id="searchByOrgName" value="<%=searchByOrgName%>"/></td>
        <td width="10%"><%=LC.L_Organization_Type%>:</td>
        <td width="10%"><%=drpdwnSiteType%></td>
        <td width="10%"><button type="submit" onClick="paginate_org.runFilter()"><%=LC.L_Search%></button></td>
        <td width="20%" valign="center" align="right"> 
           <A  class="rhsFont" href="sitedetails.jsp?mode=N&srcmenu=<%=src%>"><%=MC.M_AddNew_Org_Upper%><%--ADD A NEW ORGANIZATION*****--%> </A>  
        </td>
      </tr>
    </table>
    
    <div id="serverpagination" class="custom-table-center" style="width:99%;"></div>
    <input type="hidden" name="siteType" value="<%=ddSiteType%>">
   <input type="hidden" id="moduleName"  name="moduleName" value="Organization">
<input type="hidden" id="CSRFToken" name="CSRFToken">
    
    <%}else{
    
    	
      	String pagenum = "";
    		int curPage = 1;
    		long startPage = 1;
    		String stPage;
    		long cntr = 0;
    		pagenum = request.getParameter("page");

    		if (pagenum == null)
    		{
    			pagenum = "1";
    		}
    		curPage = EJBUtil.stringToNum(pagenum);
          long rowsPerPage=0;
    		long totalPages=0;	
    		long rowsReturned = 0;
    		long showPages = 0;
    			
    	   boolean hasMore = false;
    	   boolean hasPrevious = false;
    	   long firstRec = 0;
    	   long lastRec = 0;	   
    	   long totalRows = 0;	   	   
    	   String formSql=""; 
    	   String countSql="";
    	   String sortOrderType="";
    		//majumdar changed the following line to test for pagination
    		rowsPerPage =  Configuration.MOREBROWSERROWS ;
    		totalPages =Configuration.PAGEPERBROWSER ; 
    		formSql="select b.PK_SITE,(select CODELST_DESC from er_codelst where PK_CODELST = b.FK_CODELST_TYPE) as SITE_TYPE, b.FK_CODELST_TYPE FK_CODELST_TYPE, b.SITE_NAME, b.CTEP_ID, b.site_id from ER_SITE b where b.FK_ACCOUNT ="+accountId+" and SITE_HIDDEN <> 1  and lower(SITE_NAME) like lower('%"+(StringUtil.unescapeSpecialCharSQL(searchby.trim())).replaceAll("%","/%").replaceAll("_","/_")+"%') escape '/' or (select lower(CODELST_DESC) from er_codelst where PK_CODELST = b.FK_CODELST_TYPE and b.FK_ACCOUNT ="+accountId+" ) like lower('%"+(StringUtil.unescapeSpecialCharSQL(searchby.trim())).replaceAll("%","/%").replaceAll("_","/_")+"%') escape '/' or lower(CTEP_ID) like lower('%"+(StringUtil.unescapeSpecialCharSQL(searchby.trim())).replaceAll("%","/%").replaceAll("_","/_")+"%') escape '/'";  
    		String count1 = "select count(*) from  ( " ;
    		String count2 = ")"  ;
    		countSql = count1 + formSql + count2 ;
    		/* if(!"SITE_NAME".equals(orderBy)){
    			sortOrderType=" lower( SITE_NAME ) "+orderType+",lower("+orderBy+") "+ orderType;
    		}else
    		{ */
    		sortOrderType=" lower("+ orderBy+" ) "+ orderType;
    		/* } */
         BrowserRows br = new BrowserRows();
         br.getPageRows(curPage,rowsPerPage,formSql ,totalPages,countSql,sortOrderType,"");
     	   rowsReturned = br.getRowReturned();
    	   showPages = br.getShowPages();
    	   startPage = br.getStartPage();
    	   hasMore = br.getHasMore();
    	   hasPrevious = br.getHasPrevious();	 
    	   totalRows = br.getTotalRows();	   
    	   firstRec = br.getFirstRec();
    	   lastRec = br.getLastRec();
    	
    	
    	
    	
    
    %>
    <div id="mydiv">
    <table width="100%" class="outline midAlign"  id="table1" style="margin: 0px 0px 0px 0px;">
    
      <tr> 
		<!--//modified on 29th March for organization sorting, bug #2072 -->
	<%--if(!"networkTabs".equals(calledFrom)){ --%>
        <%-- <th width="40%" onClick="setOrder(document.sitebrowser,'lower(SITE_NAME)')"> <%=LC.L_Organization_Name%>Organization Name***** &loz;</th>
        <th width="20%" onClick="setOrder(document.sitebrowser,'lower(SITE_TYPE)')"> <%=LC.L_Type%>Type***** &loz;</th>
        <th width="40%" onClick="setOrder(document.sitebrowser,'lower(PARENT_SITE)')"> <%=LC.L_Parent_Org%>Parent Organization***** &loz;</th>
         --%><%--else{--%>
        <th width="5%" onClick=""></th>
        <th width="40%" onClick="setOrderNetPage(document.networkTabs,'SITE_NAME')"> <%=LC.L_Organization_Name%><%--Organization Name*****--%> &loz;</th>
        <th width="33%" onClick="setOrderNetPage(document.networkTabs,'SITE_TYPE')"> <%=LC.L_Organization_Type%><%--Type*****--%> &loz;</th>
        <th width="30%" onClick="setOrderNetPage(document.networkTabs,'CTEP_ID')"> <%=LC.L_CTEP_ID%><%--Type*****--%> &loz;</th>
        <%--} --%>
      </tr>
      <%

    for(counter = 1;counter<=rowsReturned;counter++)

	{	siteId=EJBUtil.stringToNum(((br.getBValues(counter,"PK_SITE")) == null)?"-":(br.getBValues(counter,"PK_SITE")).toString());
	    siteType=((br.getBValues(counter,"SITE_TYPE")) == null)?"-":(br.getBValues(counter,"SITE_TYPE")).toString();
		siteTypeId=EJBUtil.stringToNum(((br.getBValues(counter,"FK_CODELST_TYPE")) == null)?"-":(br.getBValues(counter,"FK_CODELST_TYPE")).toString());
		siteName=((br.getBValues(counter,"SITE_NAME")) == null)?"-":(br.getBValues(counter,"SITE_NAME")).toString();
		ctepid=((br.getBValues(counter,"CTEP_ID")) == null)?"-":(br.getBValues(counter,"CTEP_ID")).toString();
		dSiteType = cd1.toPullDown("siteType_"+siteId, siteTypeId,"style='display:none;'");
		if(!"networkTabs".equals(calledFrom)){
		siteParent=((siteParents.get(counter)) ==null)?"-":(siteParents.get(counter)).toString();



		siteStat=((siteStats.get(counter)) == null)?"-":(siteStats.get(counter)).toString();
		if (siteStat.equals("A")){

		   siteStat = "Active";

		}

		else if (siteStat.equals("I")){

		   siteStat = "InActive";

		}

		}
		if ((counter%2)==0) {  %>
      <tr class="browserEvenRow" id="<%=siteId%>" > 
        <%}	else{ %>

      <tr class="browserEvenRow" id="<%=siteId%>" > 
        <%

		}

  %>
  <%if("networkTabs".equals(calledFrom)){ %>
  <td><input value="<%=siteId%>" type="checkbox"  name="selectNetwork"></td>
  <%}%>
        <td> 
        				<%if(ctepid.equals("-")) {%>
        					<A href="../jsp/sitedetails.jsp?siteId=<%=siteId%>&mode=M&srcmenu=<%=src%>">
        				<%}else{%>
        					<A href="../jsp/sitedetails.jsp?siteId=<%=siteId%>&mode=M&srcmenu=<%=src%>" title="<%= ctepid%>">
        				<%}%>
          						<%= siteName%> 
          					</A>
          				</td>
        				<td> <span><%= siteType%></span><%=dSiteType%> </td>
        				<%if("networkTabs".equals(calledFrom)){ %>
        				<td> <span><%= ctepid%></span> </td>
        				<%} %>
        <%if(!"networkTabs".equals(calledFrom)){ %>
        <td> <%= siteParent%> </td>
        <%} %>
      </tr>

	   <%

		}

%>
    </table>
    <input type="hidden" id="currp" name="currp" value="<%=curPage %>" />
    <table width="98%" cellspacing="0" cellpadding="0" class="midalign">
		<tr valign="top"><td>
		<% if (totalRows > 0) 
		{ Object[] arguments = {firstRec,lastRec,totalRows};		
		%>
					<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
		<%} else {%>
			<font class="recNumber"><BR><b><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></b></font>	
		<%}%>	
	   </td></tr>
	</table>
    	<div align="center" class="midalign">
	<%
		if (curPage==1) startPage=1;
	    for ( counter = 1; counter <= showPages;counter++)
		{
		
		 
   			cntr = (startPage - 1) + counter;
	 		if ((counter == 1) && (hasPrevious))
			{
	 			long prev=0;
	 			prev=cntr-1;
			  %>
				
			  	<A href="#" onclick ="pagetraverse('1',<%= prev%>);">
			  	<%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%>  </A>
				&nbsp;&nbsp;&nbsp;&nbsp;
					
				<%
  			}	%>
		
		<%
 		 if (curPage  == cntr)
		 {	 
	    	 %>	   
				<FONT class = "pageNumber"><%= cntr %></Font>
       		<%
       	}else
        {	%>		
		<A href="#" onclick ="pagetraverse('1',<%= cntr%>);"><%= cntr%></A>
       <%}%>
	
		<%	}
		if (hasMore)
		{ 
			long next=cntr+1;
		%>
		
	   &nbsp;&nbsp;&nbsp;&nbsp;
	   <A href="#" onclick ="pagetraverse('1',<%= next%>);"> <%=LC.L_Next%><%--Next*****--%> <%=totalPages%></A>
		<%	}	%>
	</div>
    <%}%>
    <% if(!"networkTabs".equals(calledFrom)){%>
    </div>
    <div class="tmpHeight"></div>
    <input type="hidden" name="drpdwnSiteType">
  </Form>
  <%
    	}
	} //end of if body for page right
 
 

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}
if(!"networkTabs".equals(calledFrom)){
%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id="emenu"> 
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>
<%} %>
</body>

</html>

