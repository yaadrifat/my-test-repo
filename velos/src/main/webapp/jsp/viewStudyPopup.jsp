<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<title><%=LC.L_Study%><%-- <%=LC.Std_Study%>*****--%></title>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<%-- Nicholas : Start --%>
	<%--<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>--%>
	<%-- Nicholas : End --%>

	<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.UserSiteDao"%>
<%@ page import="com.velos.eres.service.util.*"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
	<%-- Nicholas : Start --%>
	<jsp:include page="skinChoser.jsp" flush="true"/>
	<Link Rel=STYLESHEET HREF='./styles/velos_popup.css' type=text/css>
	<%-- Nicholas : End --%>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
</head>>
<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1)
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>

<DIV class="popDefault" >

<P class="sectionHeadings"> <%=LC.L_FrmLib_Std%><%-- Form Library >> <%=LC.Std_Study%>*****--%> </P>
<%
	HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
{
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	ArrayList arrIds=null;
	ArrayList arrFirstNames=null;
	ArrayList arrLastNames=null;
	ArrayList arrStudyNumbers=null;
	ArrayList arrMidNames=null;
	ArrayList arrLogNames=null;
	ArrayList arrEmails=null;
	String studyIds=request.getParameter("studyIds");
	String usrLastName="";
	String usrFirstName="";
	String studyNumber="";
	String usrMidName="";
	String usrLogname="";
	String usrEmail="";
	String userDetails="";

	UserDao usrDao=new UserDao();
	usrDao.getStudyUsers(studyIds);
	arrIds=usrDao.getUsrIds();
	arrLastNames=usrDao.getUsrLastNames();
	arrFirstNames=usrDao.getUsrFirstNames();
	arrMidNames=usrDao.getUsrMidNames();
	arrLogNames=usrDao.getUsrLogNames();
	arrEmails=usrDao.getUsrPerAdds();
	arrStudyNumbers = usrDao.getUsrStudyNumbers();
	int rows = usrDao.getCRows();

	if(rows==0)
	{%>
	<P class="defComments"><%=MC.M_NoRecordsFound%><%-- No records found*****--%></P>

	<%}
else{%>
	<table width="95%" cellspacing="0" cellpadding="0" border="0" >
    <tr class="browserOddRow">
    <th  width="25%"> <%=LC.L_Study%><%-- <%=LC.Std_Study%>*****--%></th>
    <th   width="25%"> <%=LC.L_User_Name%><%-- User Name*****--%> </th>

    </tr>
<%

	for(int i = 0 ; i < rows ; i++)
	{

		studyNumber= arrStudyNumbers.get(i).toString();

		if ( i > 0)
		{
			if ( studyNumber.equals(arrStudyNumbers.get(i-1).toString() ) )
			{
				studyNumber="";
			}
		}

		usrLastName=((arrLastNames.get(i)) == null)?"-":(arrLastNames.get(i)).toString();

		usrFirstName=((arrFirstNames.get(i))==null)?"-":(arrFirstNames.get(i)).toString();
		
		usrMidName=((arrMidNames.get(i))==null)?"":(arrMidNames.get(i)).toString();
		
		usrLogname=((arrLogNames.get(i))==null)?"":(arrLogNames.get(i)).toString();
		
		usrEmail=((arrEmails.get(i))==null)?"":(arrEmails.get(i)).toString();
		
		userDetails=usrFirstName+" "+usrMidName+" "+usrLastName+" "+usrEmail+" "+usrLogname;

		if ((i%2)==0)
		{

%>
      <tr class="browserEvenRow">
<%
		}

	   else
	   {
%>
      <tr class="browserOddRow">
<%

	   }
%>
	<td ><b><%= studyNumber%><b></td>

	<td >	<%=usrFirstName%>&nbsp;<%= usrLastName%> <a href="#"><img src="./images/View.gif" border="0" align="left" style="float: right;" onmouseover="return overlib(htmlEncode('<%=userDetails.replace("'","\\'") %>'),ABOVE,CAPTION,'');" onmouseout="nd();" > </a> </td>
	  </tr>
<%
		}//end of for statement
}//end of else
%>	</table>
<%
}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</div>
</body>

</html>

