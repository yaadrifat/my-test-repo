<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Update_FormMsgs%><%--Update Form Messages*****--%> </title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</head>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="formNotifyJB" scope="request"  class="com.velos.eres.web.formNotify.FormNotifyJB"/>
<jsp:include page="include.jsp" flush="true"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<body>
<DIV id="div1"> 

<%
	HttpSession tSession = request.getSession(true);
	String codeStatus = request.getParameter("codeStatus");
	if (sessionmaint.isValidSession(tSession))
	{
		
%>
	<jsp:include page="sessionlogging.jsp" flush="true"/>
   
<%		String eSign= request.getParameter("eSign");
		String oldESign = (String) tSession.getValue("eSign");
	  	if((codeStatus.equals("WIP")) || ((!codeStatus.equals("WIP") && !codeStatus.equals("")) && (oldESign.equals(eSign)))){
				
		int errorCode=0;
		String formNotifyId="";
		String formLibId="";
	
		String msgType;
		String msgSendType;
		String userlist;
		String messageText;
		
		String creator=(String) tSession.getAttribute("userId");
		String ipAdd=(String)tSession.getAttribute("ipAdd");
		String mode=request.getParameter("mode");
		formLibId=request.getParameter("formLibId");
		
		String recordType=mode;  
		
	
	
		msgType=request.getParameter("msgType");
		
		msgSendType=request.getParameter("msgSendType");
	
		
		userlist=request.getParameter("userlist");	
		if(msgType.equals("P")){
		
			userlist = "";
		}
		messageText=request.getParameter("messageText");
		messageText=(messageText==null)?"":messageText;
		
		if (messageText.length()>0)
			messageText=StringUtil.replace(messageText,"\r\n"," ");
			
		formNotifyJB.setFormLibId(formLibId);
		formNotifyJB.setMsgType(msgType);
		formNotifyJB.setMsgText(messageText);
		formNotifyJB.setMsgSendType(msgSendType);
		formNotifyJB.setUserList(userlist);
		formNotifyJB.setCreator(creator);
		formNotifyJB.setIpAdd(ipAdd);
		
		
		formNotifyJB.setRecordType(recordType);

		// Set the form Notify State Keeper
		if (mode.equals("N"))
		{
			
			 errorCode = formNotifyJB.setFormNotifyDetails();
		}

		else if (mode.equals("M"))
		{
			
			formNotifyId = request.getParameter("formNotifyId");
			formNotifyJB.setFormNotifyId(EJBUtil.stringToNum(formNotifyId));
			formNotifyJB.getFormNotifyDetails();
			formNotifyJB.setMsgType(msgType);
			formNotifyJB.setMsgText(messageText);
			formNotifyJB.setMsgSendType(msgSendType);
			formNotifyJB.setUserList(userlist);
			formNotifyJB.setIpAdd(ipAdd);
			formNotifyJB.setRecordType(recordType);
			formNotifyJB.setModifiedBy(creator);
			formNotifyJB.updateFormNotify();
		}
		
		
		if ( errorCode == -2)
		{
%>

	  <br>
	  <br>
	  <br>
	  <P class ="sectionHeadings" align="center" ><%=MC.M_Data_NotSvdSucc%><%--Data  not  saved succesfully*****--%> </P>

<%
		
	
		} //end of if for Error Code

		else

		{
%>	
		<br>
		<br>
		<br>
		<P class ="sectionHeadings" align = "center"><%=MC.M_Data_SavedSucc%><%--Data  saved succesfully*****--%> </P>
		
    	<script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
		</script>
		
<%
		}//end of else for Error Code Msg Display
	}//end of if old esign 
	
	else{ %>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%			}//end of else of incorrect of esign
     }//end of if body for session 	 

else{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%
	
	} //end of else of the Session 
 

%>

</body>

</html>
