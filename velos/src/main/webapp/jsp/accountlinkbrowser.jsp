<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.velos.eres.service.util.LC"%>
<%@page import="com.velos.eres.service.util.MC"%>
<html>

<head>

     <title><%=MC.M_MngAcc_AccLnks%><%--Manage Account >> Account Links***** --%></title>


<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT language="javascript">

function confirmBox(ln,pgRight) {
	if (f_check_perm(pgRight,'E') == true) {

		var paramArray = [decodeString(ln)];
		if (confirm(getLocalizedMessageString("M_Del_FrmAccLnk",paramArray))) {/*if (confirm("Delete " + decodeString(ln) + " from Account Links?")) {// BUG # 4843: Fixed by Prabhat  *****/
			return true;
		} else	{
			return false;
		}
	} else { 
		return false;
	}	
}
function openWin() {

      windowName = window.open("","Information","toolbar=yes,location=yes,scrollbars=yes,resizable=yes,menubar=yes,width=600,height=300")
	windowName.focus();

;}

</SCRIPT> 


<% String src;

src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   

<body>

<DIV class="tabDefTopN" id = "div1"> 

	<jsp:include page="accounttabs.jsp" flush="true"> 
	<jsp:param name="selectedTab" value="4"/>
	</jsp:include>
</DIV>			
<DIV class="tabDefBotN" id="div1"> 

  <jsp:useBean id="ulinkB" scope="request" class="com.velos.eres.web.ulink.ULinkJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
  <%

 HttpSession tSession = request.getSession(true); 

	if (sessionmaint.isValidSession(tSession))

	{



	 String uName = (String) tSession.getValue("userName");

 	 String tab = request.getParameter("selectedTab");


	   int pageRight = 0;	

	   GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		

	   pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MACCLINKS"));

	 if (pageRight > 0 )

		{



%>
  <%

   String accId ;   



	accId = (String) tSession.getValue("accountId");

//   out.println(accId);


   UsrLinkDao usrLinkDao = ulinkB.getULinkValuesByAccountId(EJBUtil.stringToNum(accId),"all");



   ArrayList lnksIds = usrLinkDao.getLnksIds(); 

   ArrayList lnksUris = usrLinkDao.getLnksUris();

   ArrayList lnksDescs = usrLinkDao.getLnksDescs();

   ArrayList lnksGrps = usrLinkDao.getLnksGrpNames();
	
   ArrayList lnkTypes= usrLinkDao.getLnksSubTypes();
 



   String lnkUri = null;

   String lnkDesc = null;

   int lnkId = 0;

   String lnkGrp = null;		

   String oldGrp = null;	
   String lnkType="";	
   String prevlnkType="";	
   
   String accountLinkMsg=/*"The list below displays the links to be included on the <b> Homepage </b>of all the users within your account:";*****/MC.M_ListHpage_UserAcc+":";
   
   String adverseEventMsg=/*"The list below displays the links to be included on the <b> Adverse Event </b> Page of all the users  within your account:";*****/MC.M_ListAdvEvt_UserAcc+":";

 	



   int len = lnksIds.size();

   int counter = 0;

%>
  

  <Form name="usrlnkbrowser" method="post" action="" onsubmit="">
    <Input type="hidden" name="selectedTab" value="<%=tab%>">
<%	
if(len == 0){	
%>
      <table width="100%"  cellpadding="0" border=0 >
  	<tr>
		<td>
	       <p> <%-- No links exist currently.*****--%> <%=MC.M_No_LnksExistCur%> <A href="link.jsp?mode=N&srcmenu=<%=src%>&selectedTab=<%=tab%>&lnkType=account"><%-- ADD A NEW LINK*****--%><%=MC.M_AddNew_Link_Upper%></A> </p>
		</td>
		<td colspan="3"></td>
	</tr>
  </table>
<%
}else{ //else for if to check the len==0
%>
  <table width="100%"  cellpadding="0" border=0 >

      <%

    for(counter = 0;counter<len;counter++)

	{		

		lnkUri=((lnksUris.get(counter))==null)?"-":(lnksUris.get(counter)).toString();

		lnkDesc=((lnksDescs.get(counter))==null)?"-":(lnksDescs.get(counter)).toString();

		lnkGrp = ((lnksGrps.get(counter))==null)?"-":(lnksGrps.get(counter)).toString();

		lnkId =  ((Integer)lnksIds.get(counter)).intValue();

		lnkType= ((lnkTypes.get(counter))==null)?"-":(lnkTypes.get(counter)).toString();
	 	lnkType=lnkType.trim(); 





	if(!(prevlnkType.equals(lnkType)))
	{ %>
    <tr > 


      <td colspan=3> 	

	 <% if((lnkType.equals("lnk_gen"))) {%>
        <P class = "defComments"><%=accountLinkMsg%></P>
	<%}%>

	 <% if(lnkType.equals("lnk_adv") && "Y".equalsIgnoreCase(CFG.ADV_EVENT)) {%>
        <P class = "defComments"><%=adverseEventMsg%></P>
	<%}%>

      </td>
      <td colspan="2"></td>
     </tr>
    
 <% if (lnkType.equals("lnk_gen")) {%>
  <tr>
      <td colspan=3 align="center"> 
      <% if("N".equalsIgnoreCase(CFG.ADV_EVENT))
      {%>
        <p> <A href="link.jsp?mode=N&srcmenu=<%=src%>&selectedTab=<%=tab%>&lnkType=account"><%--ADD A NEW LINK*****--%><%=MC.M_AddNew_Link_Upper%></A> </p>
   <%} %>
      </td>
		<td colspan="2">&nbsp;</td>
    </tr>
      <tr> 
        <th width="25%" align="center"> <%--Section Heading*****--%><%=LC.L_Sec_Heading%></th>
        <th width="30%" align="center"> <%--Link Description*****--%><%=LC.L_Link_Desc%> </th>
        <th width="35%" align="center"> <%--Link URL*****--%> <%=LC.L_Link_Url%> </th>
	  <th width="10%" align="center"><%=LC.L_Delete%><%--Delete*****--%></th>
      </tr>
      
      <%} else if(lnkType.equals("lnk_adv") && "Y".equalsIgnoreCase(CFG.ADV_EVENT)) {%>
       <tr>
      <td colspan=3 align="center"> 
        <p> <A href="link.jsp?mode=N&srcmenu=<%=src%>&selectedTab=<%=tab%>&lnkType=account"><%--ADD A NEW LINK*****--%><%=MC.M_AddNew_Link_Upper%></A> </p>
   
      </td>
		<td colspan="2">&nbsp;</td>
    </tr>
            <tr> 
        <th width="25%" align="center"> <%--Section Heading*****--%><%=LC.L_Sec_Heading%></th>
        <th width="30%" align="center"> <%--Link Description*****--%><%=LC.L_Link_Desc%> </th>
        <th width="35%" align="center"> <%--Link URL*****--%> <%=LC.L_Link_Url%> </th>
	  <th width="10%" align="center"><%=LC.L_Delete%><%--Delete*****--%></th>
      </tr>      
      <%} %>
	<%  } %>


	  <% if (lnkType.equals("lnk_gen")) {%>
	  <%	if ((counter%2)==0) {	%>
      <tr class="browserEvenRow"> 
        <%

		}

		else{

	  %>
      <tr class="browserOddRow"> 
        <%

		}

	  %>
        <td > 
          <% if (!lnkGrp.equals(oldGrp)){ %>
          <%= lnkGrp%> 
          <%}%>
        </td>
        <td align="center"> <A href = "link.jsp?mode=M&lnkId=<%= lnkId%>&srcmenu=<%=src%>&selectedTab=<%=tab%>&lnkType=account"><%= lnkDesc%></A> 
        </td>
        <td align="center"> <A href="<%=lnkUri%>" target = "Information" onClick="openWin()"><%=lnkUri%></A> 
        </td>
       <!-- Changed By PK Bug #4017 --> 
       <td align="center"><A href="deletelnk.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&mode=a&lnkId=<%=lnkId%>" onClick="return confirmBox('<%=StringUtil.encodeString(lnkUri)%>',<%=pageRight%>);" ><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/></A></td>
      </tr>
      <%} else if(lnkType.equals("lnk_adv") && "Y".equalsIgnoreCase(CFG.ADV_EVENT)) { %>
      <%	if ((counter%2)==0) {	%>
      <tr class="browserEvenRow"> 
        <%
		}
		else{
		%>
      <tr class="browserOddRow"> 
        <%

		}
	  %>
        <td > 
          <% if (!lnkGrp.equals(oldGrp)){ %>
          <%= lnkGrp%> 
          <%}%></td><td align="center"> <A href = "link.jsp?mode=M&lnkId=<%= lnkId%>&srcmenu=<%=src%>&selectedTab=<%=tab%>&lnkType=account"><%= lnkDesc%></A> 
        </td>
        <td align="center"> <A href="<%=lnkUri%>" target = "Information" onClick="openWin()"><%=lnkUri%></A> 
        </td>
       <!-- Changed By PK Bug #4017 --> 
       <td align="center"><A href="deletelnk.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&mode=a&lnkId=<%=lnkId%>" onClick="return confirmBox('<%=StringUtil.encodeString(lnkUri)%>',<%=pageRight%>);" ><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/></A></td>
      </tr>      
      <%} %>
      <%

	oldGrp = lnkGrp;
prevlnkType=lnkType;
 		}

%>
      <tr> 
        <td width="130" height=20></td>
        <td width=22 height=20></td>
        <td colspan="2">&nbsp;</td>
      </tr>
    </table>
<%
} //end for if to check the len==0
%>	
  </Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu" > 
  <jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>

