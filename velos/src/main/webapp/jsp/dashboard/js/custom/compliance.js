compliance = {
    renderPage: {},
    renderStatusFilters: {},
    setUpStdAutoComplete: {},
    renderDataTable: {},
    renderReviewTypeDD: {},
    renderSubmissionTypeDD: {}
}

compliance.renderPage = function() {
	$.ajax({
    	  url: urls.compliance.template,success: function( data ) {
    		 
			var template = Handlebars.compile(data);
    		var html = template(LC);
    		//console.log(html);
    		$("#pageContent").html(html);
            
    		$('#statusFiltersDIV, #searchFiltersDIV').accordion({
                collapsible: true,
                heightStyle: 'content',
                animate: false
            });
    		 compliance.renderStatusFilters();
             compliance.setUpStdAutoComplete();
             //compliance.renderReviewTypeDD();
             //compliance.renderSubmissionTypeDD();
             compliance.renderDataTable();
    	}
   });
    
	
	/*$("#pageContent").load(
        urls.compliance.template, function() {
            $('#statusFiltersDIV, #searchFiltersDIV').accordion({
                collapsible: true,
                heightStyle: 'content',
                animate: false
            });
            compliance.renderStatusFilters();
            compliance.setUpStdAutoComplete();
            //compliance.renderReviewTypeDD();
            //compliance.renderSubmissionTypeDD();
            compliance.renderDataTable();
        });
	*/
}

compliance.renderStatusFilters = function() {
    dashboardUtils
        .fetchCodeLstItems(
            'studystat',
            function(jsonData) {
                var html = '';
                html += '<label><input checked="false" type="checkbox" id="all-checkbox" name="all-checkbox" class="all-checkbox" value="all"></input>Select All/ Unselect All</label>';
                $.each(jsonData,
                    function() {
                        html += '<label style="background-color:' + this.colorcode + '" ><input checked="true" type="checkbox" id="' + this.PK_CODELST + '" name="statues" class="velos-checkbox" value="'+this.PK_CODELST+'"></input>' + this.CODELST_DESC + '</label><br/>'
                    });
                $('#statusFiltersContentDIV').append(html);
            });
    $(".velos-checkbox").change(function(){
    	$("#complianceTable").dataTable().fnDestroy();    
    	$("#complianceTable").empty();
    	addnodes();
    	compliance.renderDataTable();
    });
    
    $(".all-checkbox").change(function(){
    	if($('#all-checkbox').is(':checked'))
    		{
    			$(".velos-checkbox:checkbox").prop('checked',true);
    		}
    	else
    		{
    			$(".velos-checkbox:checkbox").prop('checked',false);
    		}
    	$("#complianceTable").dataTable().fnDestroy();    
    	$("#complianceTable").empty();
    	addnodes();
    	compliance.renderDataTable();
    });
}

compliance.setUpStdAutoComplete = function() {
    $("#selectedStudies").select2({
        placeholder: "Search a Study",
        minimumInputLength: 1,
        multiple: true,
        ajax: {
            url: urls.compliance.studyAutoComplete,
            dataType: 'json',
            allowClear: true,
            results: function(data, page) {
                return {
                    results: data
                };
            }
        }
    });
}

compliance.renderReviewTypeDD = function() {
    dashboardUtils
        .fetchCodeLstItems(
            'revType',
            function(jsonData) {
                var html = '<label><select id="dashboardTypeDD"><options><option>Select an option</option>';
                $.each(jsonData, function() {
                    html += '<option data-codelstSubType="' + this.CODELST_SUBTYP + '" value="' + this.PK_CODELST + '">' + this.CODELST_DESC + '</option>'
                });
                html += '</options><select>Review Type</label>';
                $('#reviewTypeDIV').append(html);
            });
}

compliance.renderSubmissionTypeDD = function() {
    dashboardUtils
        .fetchCodeLstItems(
            'submission',
            function(jsonData) {
                var html = '<label><select id="dashboardTypeDD"><options><option>Select an option</option>';
                $.each(jsonData, function() {
                    html += '<option data-codelstSubType="' + this.CODELST_SUBTYP + '" value="' + this.PK_CODELST + '">' + this.CODELST_DESC + '</option>'
                });
                html += '</options><select>Submission Type<label>';
                $('#submissionTypeDIV').append(html);
            });
}

jQuery.fn.dataTableExt.oSort['custom_us_date-asc'] = function(x, y) {
    var xVal = getCustomUsDateValue(x);
    var yVal = getCustomUsDateValue(y);
 
    if (xVal < yVal) {
        return -1;
    } else if (xVal > yVal) {
        return 1;
    } else {
        return 0;
    }
}
 
jQuery.fn.dataTableExt.oSort['custom_us_date-desc'] = function(x, y) {
    var xVal = getCustomUsDateValue(x);
    var yVal = getCustomUsDateValue(y);
 
    if (xVal < yVal) {
        return 1;
    } else if (xVal > yVal) {
        return -1;
    } else {
        return 0;
    }
}
 
function getCustomUsDateValue(strDate) {
	
	strDate = getDate(strDate);
	
    var frDate = strDate.split('-');
     
    var x = (frDate[2] + frDate[0] + frDate[1]);
   
    x = x * 1;
 
    return x;
}

function getDate(str)
{
    var arr = str.split("-");
    var months = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];

    var month = months.indexOf(arr[0].toLowerCase()) + 1;
    if(month < 10)
    	month = "0"+month;

    return (month + "-" +arr[1] + "-" + arr[2]);
}

jQuery.fn.dataTableExt.oSort['custom_days-asc'] = function(x, y) {
    var xVal = getCustomDaysValue(x);
    var yVal = getCustomDaysValue(y);
 
    if (xVal < yVal) {
        return -1;
    } else if (xVal > yVal) {
        return 1;
    } else {
        return 0;
    }
}
 
jQuery.fn.dataTableExt.oSort['custom_days-desc'] = function(x, y) {
    var xVal = getCustomDaysValue(x);
    var yVal = getCustomDaysValue(y);
 
    if (xVal < yVal) {
        return 1;
    } else if (xVal > yVal) {
        return -1;
    } else {
        return 0;
    }
}

function getCustomDaysValue(strDate) {
    var frDate = strDate.split(' ');
     
    var x = (frDate[0]);
    x = x * 1;
 
    return x;
}

compliance.renderDataTable = function(dataSet) {
	
	
	$(".selectAllQueries").on('change',function(){
		
		if($(this).checked){
			//console.log('ckeck all');
		}else{
			//console.log('cunckeck all');
		}
		
	});
	
	$.ajax({
        url: urls.compliance.fetchSubmissionDataTable,
        type: 'POST',
        data: $('#statusForm').serialize(),
        global: true,
        dataType: 'json',
        async: false,
        cache: true,
        success: function(data) {
            $('#complianceTable')
                .dataTable({
                	dom: 'TC<"clear">lfrtip',
                    tableTools: {
                        "sSwfPath": "/velos/jsp/dashboard/js/extensions/swf/copy_csv_xls_pdf.swf"
                    }, "aaSorting": [],
                    "data": data,
                    "order": [[ 2, "desc" ]],
                    "aoColumns": [{
                        "mData": "submission_status_desc"
                    }, {
                        "mData": "colorcode"
                    },{
                        "mData": "submission_status_date",
                        	"sType": "custom_us_date"
                        
                    }, {
                        "mData": "submission_since",
                    	"sType": "custom_days"
                    }, {
                        "mData": "study_title"
                    }, {
                        "mData": "study_number"
                    },{
                        "mData": "current_version"
                    }, {
                        "mData": "submission_type_desc"
                    }, {
                        "mData": "submission_board_name"
                    },{
                        "mData": "created_on",
                    	"sType": "custom_us_date"
                    },{
                        "mData": "submitted_date",
                    	"sType": "custom_us_date"
                    },{
                        "mData": "approved_date",
                    	"sType": "custom_us_date"
                    },{
                        "mData": "total_elapsed_days",
                    	"sType": "custom_days"
                    }/*,{
                        "mData": "checkboxPlaceHolder"
                    }*/ ],
                    "columnDefs": [{
                            "targets": [0],
                            "createdCell": function(td, cellData, rowData, row, col) {
                    			$(td).css('background-color', rowData.colorcode);
                    			$(td).css({"border": ".5px solid","border-radius":"3px","-moz-border-radius":"3px","padding":"3px"});
                    		}
                        },{
                        	"targets": [1],
                            "visible": false
                        },{

                        	"targets": [5],
                        	"createdCell": function(td, cellData, rowData, row, col) {
                        		var htmlStr="";
                        		
                        		var study=rowData.pk_study;
                        		var studyNumber=rowData.study_number;
                        		var submTypeSubtype=rowData.submission_type_subtype;
                        		var submTypeDesc=rowData.submission_type_desc;
                        		if (!study) study="";
                        		if (!studyNumber) studyNumber="";
                        		htmlStr="<A href='flexStudyScreenLindFt?srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=M&studyId="+study+"&submissionType="+submTypeSubtype+"'>"
                        		    +studyNumber+"</A> ";
                        		$(td).html(htmlStr);
                        	
                        	}
                        
                        },{

                        	"targets": [4],
                        	"createdCell": function(td, cellData, rowData, row, col) {
                        		var htmlStr="";
                        		var studyTitle=rowData.study_title;
                        		var reg_exp = /\'/g;
                        		if (!studyTitle) { studyTitle = ''; }
                        		studyTitle = studyTitle.replace(reg_exp, "\\'");
                        		reg_exp = /[\r\n|\n]/g;
                        		studyTitle = studyTitle.replace(reg_exp, " ");
                        		htmlStr="<a href=\"#\" onmouseover=\"return overlib('"+studyTitle+"',CAPTION,'"+L_Study_Title+"');\" onmouseout=\"return nd();\"><img border=\"0\" src=\"./images/More_info_icon_grey_ square_24x24.png\" /></a>";
                        		
                        		$(td).html(htmlStr);
                        		
                        	}                        
                        },
                        {
                        	"targets": [8],
                            "visible": false
                        }/*,
                        {
                        	"targets": [13],
                        	 "orderable": false,
                        	"createdCell": function(td, cellData, rowData, row, col) {
                    			$(td).html('<input type="checkbox" name="selectQuery" class="selectQueries"/>');
                    			$(td).css("text-align","center");
                            }
                        }*/
                    ]
                });
        },
        error: function(xhr, status, errorThrown) {
            //console.log('errorThrown  ' + errorThrown);
            return null;
        }
    });
	
	

}

