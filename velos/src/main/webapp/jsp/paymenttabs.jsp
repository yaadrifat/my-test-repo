<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<%@ page import="com.velos.eres.service.util.EJBUtil" %>
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings,java.util.*"%><%@page import="com.velos.eres.service.util.*"%>
<jsp:include page="skinChoser.jsp" flush="true"/> 
<%
String mode="N";
String study="";
int studyId = 0;	
String selclass="";
String paymentPk = "";
String tab= request.getParameter("selectedTab");
study=request.getParameter("studyId");
paymentPk = request.getParameter("paymentPk");
studyId = EJBUtil.stringToNum(study);

String right = "";

right =  request.getParameter("pR");



String uName="";
String studyNumber = "";
String studyTitle ="";

HttpSession tSession = request.getSession(true); 

String acc = (String) tSession.getValue("accountId");

ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
ArrayList tabList = objCache.getAccountObjects(EJBUtil.stringToNum(acc), "payment_tab");

if (sessionmaint.isValidSession(tSession))
{
	uName = (String) tSession.getValue("userName");
	
	studyB.setId(studyId);
	studyB.getStudyDetails();
	studyNumber = studyB.getStudyNumber();
	studyTitle = studyB.getStudyTitle();	

	if (studyTitle==null) studyTitle="-";
}

out.print("<Input type=\"hidden\" name=\"mode\" value=\"" +mode+"\">");

%>

<DIV>
<table  cellspacing="0" cellpadding="0" border="0"> 
	<tr>

	<%

	for (int iX=0; iX<tabList.size(); iX++) {
	ObjectSettings settings = (ObjectSettings)tabList.get(iX);

	if ("0".equals(settings.getObjVisible())) {
		continue;
	}

	boolean showThisTab = false;
	if ("1".equals(settings.getObjSubType())) {
		 showThisTab = true; 
	} 
	else if ("2".equals(settings.getObjSubType())) {
		showThisTab = true; 
		
	}
	else if ("3".equals(settings.getObjSubType())) {
		showThisTab = true; 
	}
    else {
		showThisTab = true;
	}

	if (!showThisTab) { continue; } 


	if (tab == null) { 
		selclass = "unselectedTab";
	} else if (tab.equals(settings.getObjSubType())) {
		selclass = "selectedTab";
	} else {
		selclass = "unselectedTab";
	}
	 %>	

	 <td  valign="TOP">			
			<table class="<%=selclass%>"  cellspacing="0" cellpadding="0" border="0">
				<tr>
				<!--  class="<%=selclass%>" rowspan="3" valign="top" >
						<img src="../images/leftgreytab.gif" height=20 border=0 alt="">
					</td>  --> 
					<td class="<%=selclass%>">
					<% if ("1".equals(settings.getObjSubType())) { %>

				<a href="linkInvPayBrowser.jsp?selectedTab=1&studyId=<%=studyId%>&paymentPk=<%=paymentPk%>&pR=<%=right%>"> <%=settings.getObjDispTxt()%></a>
					<%} else if ("2".equals(settings.getObjSubType())) {%>	
					<a href="linkMilestonePayBrowser.jsp?selectedTab=2&studyId=<%=studyId%>&paymentPk=<%=paymentPk %>&pR=<%=right%>"><%=settings.getObjDispTxt()%></a>	
					<%} else if ("3".equals(settings.getObjSubType())) {%>
						<a href="linkBudgetPayBrowser.jsp?selectedTab=3&studyId=<%=studyId%>&paymentPk=<%=paymentPk %>&pR=<%=right%>"><%=settings.getObjDispTxt()%></a>
					<%}%>

					</td> 
				<!--     <td class="<%=selclass%>" rowspan="3" valign="top">
						<img src="../images/rightgreytab.gif"  height=20 border=0 alt="">
			        </td>  --> 
			  	</tr> 
		   	</table> 
        </td> 

		<%}%>
			
			</tr>
   <!--  <tr>
     <td colspan=5 height=10></td>
  </tr>  --> 
</table>
<table  class="tabBorder" width="99%" height="5"><tr><td></td></tr></table>
    <table width="100%" cellspacing="0" cellpadding="0" class= "patHeader" >
	<tr>
	<%Object[] arguments = {studyNumber};%>
		<td style="font-weight: normal"><%=VelosResourceBundle.getLabelString("L_StdNum_Dyna",arguments)%><%--Study Number:<%=studyNumber%>*****--%>&nbsp;&nbsp;&nbsp;&nbsp;
		<!--Study Title:<%=studyTitle%>-->
		</td>
	</tr>
	
</table>


</DIV>
