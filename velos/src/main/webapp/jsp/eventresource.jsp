<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=MC.M_EvtLib_EvtResrc%><%-- Event Library >> Event Resource*****--%></title>
<!-- Akshi:Added for bug #6948 -->
<%-- Commented by Yogendra Pratap Singh : Bug# 6927 and 6947--%>
<%-- <link type="text/css" href="./styles/bethematch/common.css" rel="STYLESHEET">
<link type="text/css" href="./styles/ns_gt1024.css" rel="STYLESHEET">--%>


<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,java.lang.reflect.*" %>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt"))

{ %>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>

 <% } //end for if ((request.getParameter("fromPage")).equals("selectEvent"))
	%>

<SCRIPT language="javascript">

var screenWidth = screen.width;
var screenHeight = screen.height;

function confirmBox(fileName,pgRight) {

	if (f_check_perm(pgRight,'E') == true) {
		var paramArray = [fileName];
		if (confirm(getLocalizedMessageString("L_Del_FromEvt",paramArray))) /*if (confirm("Delete " + fileName + " from Events?")) {*****/{

		    return true;}

		else

		{

		return false;

		}

	} else {

		return false;

	}

}

function confirmBoxKit(name,pgRight,pk,formObj) {

	if (f_check_perm(pgRight,'E') == true) {
		var paramArray = [name];
		if (confirm(getLocalizedMessageString("L_Del_FromEvt",paramArray))) /*if (confirm("Delete " + name + " from Event?")) {*****/{
		leventId = formObj.eventId.value;
 		lprotocolId = formObj.protocolId.value;

		leventName = formObj.eventName.value;
		lfromPage = formObj.fromPage.value;
		lcalledFrom = formObj.calledFrom.value;

		 windowName=window.open("deleteEventKit.jsp?delMode=initial&eventkitpk="+pk+"&eventId="+leventId+"&protocolId="+lprotocolId+"&eventName="+leventName+"&fromPage="+lfromPage+"&calledFrom="+lcalledFrom,"Kit","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");


		windowName.focus();
		    return true;



		    }

		else

		{

		return false;

		}

	} else {

		return false;

	}

}


function openkitWindow(pgRight, formObj,ch) {

	if (f_check_perm(pgRight,ch) == true) {

		leventId = formObj.eventId.value;
 		lprotocolId = formObj.protocolId.value;

		leventName = formObj.eventName.value;
		lfromPage = formObj.fromPage.value;
		lcalledFrom = formObj.calledFrom.value;

		 windowName=window.open("addeventkit.jsp?eventId="+leventId+"&protocolId="+lprotocolId+"&eventName="+leventName+"&fromPage="+lfromPage+"&calledFrom="+lcalledFrom,"Kit","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");


		windowName.focus();

	} else {

		return false;

	}

}


function openUserWindow(pgRight, formObj,ch) {

	if (f_check_perm(pgRight,ch) == true) {

		leventId = formObj.eventId.value;

		luserType = formObj.userType.value;

		lsrcmenu = formObj.srcmenu.value;

		lduration = formObj.duration.value;

		lprotocolId = formObj.protocolId.value;

		lcalledFrom = formObj.calledFrom.value;

		lmode = formObj.mode.value;

		lfromPage = formObj.fromPage.value;

		leventmode = formObj.eventmode.value;

		lcalStatus = formObj.calStatus.value;

		leventName = formObj.eventName.value;

		lcalAssoc=formObj.calassoc.value;

		//if (lfromPage=="selectEvent") {
		//JM: 01Jul2008, #3436
		if (lfromPage=="fetchProt") {
		    windowName=window.open("addeventuser.jsp?budgetId=&eventId="+leventId+"&userType="+luserType+"&srcmenu="+lsrcmenu+"&duration="+lduration+"&protocolId="+lprotocolId+"&calledFrom="+lcalledFrom+"&mode="+lmode+"&fromPage="+lfromPage+"&eventmode="+leventmode+"&calStatus="+lcalStatus+"&eventName="+leventName+"&calassoc"+lcalAssoc,"Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
		} else {
			windowName=window.open("addeventuser.jsp?budgetId=&eventId="+leventId+"&userType="+luserType+"&srcmenu="+lsrcmenu+"&duration="+lduration+"&protocolId="+lprotocolId+"&calledFrom="+lcalledFrom+"&mode="+lmode+"&fromPage="+lfromPage+"&eventmode="+leventmode+"&calStatus="+lcalStatus+"&eventName="+leventName+"&calassoc"+lcalAssoc,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
		}
		
		windowName.focus();		

	} else {

		return false;

	}

}



</SCRIPT>



</head>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.service.util.StringUtil,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.esch.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>

<jsp:useBean id="eventuser" scope="request" class="com.velos.esch.web.eventuser.EventuserJB"/>
<jsp:useBean id="eventkitB" scope="request" class="com.velos.esch.web.eventKit.EventKitJB"/>


<% String src;



src= request.getParameter("srcmenu");
//SV, commented on 10/28/04, added eventname, part of cal-enh
String eventName = request.getParameter("eventName");
eventName = StringUtil.encodeString(eventName);
//Modified by Yogendra Pratap Singh : Bug# 6927 and 6947
if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){%>
	<jsp:include page="include.jsp" flush="true"/>
<%} else{

%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<%}%>





<body id="forms">


<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){%>

		<DIV class="popDefault" id="div1" style="height: 95%;">

	<%	}

else { %>

<DIV class="browserDefault" id="div1">

<%}%>



<%

	int pageRight = 0;

	String duration = request.getParameter("duration");

	String protocolId = request.getParameter("protocolId");

	String calledFrom = request.getParameter("calledFrom");

	String eventId = request.getParameter("eventId");

	String mode = request.getParameter("mode");

	String fromPage = request.getParameter("fromPage");

	String calStatus = request.getParameter("calStatus");

	String eventmode = request.getParameter("eventmode");

	String displayDur=request.getParameter("displayDur");

	String displayType=request.getParameter("displayType");

    String m;

    String calAssoc=request.getParameter("calassoc");
    calAssoc=(calAssoc==null)?"":calAssoc;


%>

	<form name = "hiddenparam" method="POST">

	<input type = hidden value ="<%=eventId%>" name = eventId>

	<input type = hidden value = "U" name = userType>

	<input type = hidden value ="<%=src%>" name = srcmenu>

	<input type = hidden value ="<%=duration%>" name = duration>

	<input type = hidden value ="<%=protocolId%>" name = protocolId>

	<input type = hidden value ="<%=calledFrom%>" name = calledFrom>

	<input type = hidden value ="<%=mode%>" name = mode>

	<input type = hidden value ="<%=fromPage%>" name = fromPage>

	<input type = hidden value ="<%=calStatus%>" name = calStatus>

	<input type = hidden value ="<%=eventmode%>" name = eventmode>

	<input type = hidden value ="<%=eventName%>" name = eventName>

	<input type = hidden value ="<%=calAssoc%>" name = calassoc>


	<input type=hidden name=displayDur value=<%=displayDur%>>

	<input type=hidden name=displayType value=<%=displayType%>>

	</form>



	<%
	HttpSession tSession = request.getSession(true);
	int storageRight = 0; int specRight = 0;
	if (sessionmaint.isValidSession(tSession))

	{
	
		String sessionInventoryRight  ="";
		sessionInventoryRight  = (String) tSession.getAttribute("sessionInventoryRight");
		
		if (StringUtil.isEmpty(sessionInventoryRight))
		{
			sessionInventoryRight= "0";
		} 	 
		/*YK 14Sep2011 : Fixed for Bug#6979*/
		if ("1".equals(sessionInventoryRight)){
		GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getAttribute("GRights");
		storageRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MSTORAGE"));
		}
	   if (calledFrom.equals("S")) {

   	   	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");

	  	   if ((stdRights.getFtrRights().size()) == 0){

			 	pageRight= 0;

		   }else{

				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));

    	   }


		} else if (calledFrom.equals("P")) {

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
		}

		else if(calledFrom.equals("L")){

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("EVLIB"));

		}



	   int count = 0;

	   String uName = (String) tSession.getValue("userName");

	   //get event roles


	   EventuserDao evd = eventuser.getEventRoles(EJBUtil.stringToNum(eventId));
	   EventuserDao evUsers = eventuser.getEventUsers(EJBUtil.stringToNum(eventId));


	   ArrayList eventUserIds = evd.getEventUserIds();
	   ArrayList userTypes = evd.getUserTypes();
	   ArrayList userIds = evd.getUserIds();
   	   ArrayList eventUserDesc = evd.getResourceName();
	   ArrayList eventDuration = evd.getDuration();
	   ArrayList eventNotes = evd.getNotes();

	   String eventUserId = null;



	%>


<%

	String calName = "";

	if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary") || fromPage.equals("patientschedule") )

	{

%>
	<!--P class="sectionHeadings"> Protocol Calendar >> Event Resource </P-->

<%

	}

	else

	{

		calName = (String) tSession.getValue("protocolname");



%>

	<P class="sectionHeadings"> <%Object[] arguments = {calName}; %>
	    <%=VelosResourceBundle.getMessageString("M_PcolCal_EvtRsrc",arguments)%><%-- Protocol Calendar [ <%=calName%> ] >>  Event Resource*****--%>  </P>





<%

	}



%>



<jsp:include page="eventtabs.jsp" flush="true">

<jsp:param name="duration" value="<%=duration%>"/>

<jsp:param name="protocolId" value="<%=protocolId%>"/>

<jsp:param name="calledFrom" value="<%=calledFrom%>"/>

<jsp:param name="fromPage" value="<%=fromPage%>"/>

<jsp:param name="mode" value="<%=mode%>"/>

<jsp:param name="calStatus" value="<%=calStatus%>"/>

<jsp:param name="displayDur" value="<%=displayDur%>"/>

<jsp:param name="displayType" value="<%=displayType%>"/>

<jsp:param name="eventId" value="<%=eventId%>"/>
<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="calassoc" value="<%=calAssoc%>"/>

</jsp:include>



<%


	if (fromPage.equals("eventlibrary")){
	  m="N";
	}
	else
	{
	if (mode.equals("N")){m="N";}else{m="E";}
	}


if(eventId == "" || eventId == null || eventId.equals("null") || eventId.equals("")) {

	%>

	  <jsp:include page="eventDoesNotExist.jsp" flush="true"/>

  <%

	}else {

%>
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024){
		if (navigator.userAgent.indexOf("MSIE") == -1){
			document.write('<DIV class="tabDefBotN" id="div2" style="height:58%; top:90px;">') 
		}else{
			document.write('<DIV class="tabDefBotN" id="div2" style="height:82.5%; top:100px;">')
		}
	}else{
		if (navigator.userAgent.indexOf("MSIE") == -1){
			document.write('<DIV class="tabDefBotN" id="div2" style="height:83%; top:92px;">')
		}else{
			document.write('<DIV class="tabDefBotN" id="div2" style="height:82.5%; top:100px;">')
		}
	}	
</SCRIPT>

<%





if(fromPage.equals("eventbrowser")) {

%>



<form METHOD=POST action="eventbrowser.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calassoc=<%=calAssoc%>">



<%

} else {

%>

<form METHOD=POST action="fetchProt.jsp?protocolId=<%=protocolId%>&srcmenu=<%=src%>&selectedTab=4&mode=<%=mode%>&calledFrom=<%=calledFrom%>&calStatus=<%=calStatus%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&calassoc=<%=calAssoc%>">



<%

}

%>



<%

//if ((calStatus.equals("A")) || (calStatus.equals("F"))){%>
<!--
          <P class = "defComments"><FONT class="Mandatory">Changes cannot be made to Event Interval and Event Details for an Active/Frozen Protocol</Font></P> -->

<%//}



if (pageRight == 4) {

%>

   <P class = "defComments"><FONT class="Mandatory"><%=MC.M_OnlyViewPerm_ForEvt%><%-- You have only View permission for the Event.*****--%></Font></P>

<%}%>



<TABLE width="90%">

   <tr>

 	<td width=60% colspan=2> <P class = "defComments"><%=MC.M_ResrcLnk_ThisEvt%><%-- Resources linked with this Event*****--%>: </P> </td>



<%

	/*if ((calStatus.equals("A")) || (calStatus.equals("F")))

	{

	}else

		{*/%>



	<td width=15% align=right><P class = "defComments"> <A href="addeventrole.jsp?eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>" onClick="return f_check_perm(<%=pageRight%>,'<%=m%>')"> <%=MC.M_SelcOrEdt_RoleType%><%-- Select/Edit Role type*****--%></A> </P></td>

	<td width=15% align=right><P class = "defComments"> <A href=# onClick=openUserWindow(<%=pageRight%>,document.hiddenparam,'<%=m%>')><%=LC.L_Select_User%><%-- Select user*****--%></A></P></td>

    <% boolean showStorageKit = false; // For the time being, hide any reference to storage kit until it's ready.
    /*YK 14Sep2011 : Fixed for Bug#6979*/
    if (storageRight>=4)     //Akshi :Fixed for Bug#6979
    {
    	showStorageKit = true; 
    }
    // Set this to true to show storage kit again. %>
	<% if (sessionInventoryRight.equals("1") && showStorageKit) { %>
	<td width=15% align=right><P class = "defComments"> <A href=# onClick=openkitWindow(<%=pageRight%>,document.hiddenparam,'<%=m%>')><%=LC.L_Select_StorageKit%><%-- Select Storage Kit*****--%></A></P></td>
	<% } %>



	<%

	//}

	%>



<!--<td width=15% align=right> <A href="eventusersearch.jsp?userType=U&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>">Select user</A> 	</td> -->

   </tr>

</TABLE>

<Br>

	<TABLE width="100%" class = "defComments">

   <tr>

	<th width = "40%"><%=LC.L_Role_Types%><%-- Role Types*****--%></th>
	<th width = "25%"><%=LC.L_Duration%><%-- Duration*****--%></th>
	<th width = "35%"><%=LC.L_Notes%><%-- Notes*****--%></th>
	<th width = "10%"><%=LC.L_Delete%><%--Delete*****--%></th>

   </tr>

<%

	count = 0;

	for(int i=0;i<eventUserIds.size(); i++)
	{

		if ((count%2)==0) {	%>

      	<tr class="browserEvenRow">

        <%
		}
		else
		{ %>

      <tr class="browserOddRow">
        <%
		}

		count++;
  %>

	<td width = "40%">
	<%=eventUserDesc.get(i)%>
	</td>

	<td width = "20%">
	<%=eventDuration.get(i) == null ? "-" : eventDuration.get(i)%>
	</td>

	<td width = "30%">
	<%=eventNotes.get(i) == null ? "-" : eventNotes.get(i)%>
	</td>

	<td width = "10%">



	<A HREF="eventuserdelete.jsp?eventuserId=<%= eventUserIds.get(i) %>&eventId=<%=eventId%>&srcmenu=<%=src%>&eventmode=<%=eventmode%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>&userType=R"  onClick="return confirmBox('<%=eventUserDesc.get(i)%>',<%=pageRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A>

	</td>

   </tr>

<%



	}

%>

</table>
<br>

<%

	   ArrayList evUserIds = evUsers.getEventUserIds();
	   ArrayList evuser = evUsers.getUserIds();
   	   ArrayList evUserDesc = evUsers.getResourceName();
	   ArrayList evDuration = evUsers.getDuration();
	   ArrayList evNotes = evUsers.getNotes();

	   String evUserName = null;
	   String evUserId = null;

%>


	<TABLE width="100%">

   <tr>

	<th width = "98%"><%=LC.L_Users%><%-- Users*****--%></th>
	<!--  commented by gopu to fix Bug #2373 -->
	<th width = "10%"><%=LC.L_Delete%><%--Delete*****--%></th>

 <%

 %>



   </tr>

<%

	count = 0;

	for(int i=0;i<evUserIds.size(); i++)
	{

		if ((count%2)==0) {	%>

      	<tr class="browserEvenRow">

        <%
		}
		else
		{ %>

      <tr class="browserOddRow">
        <%
		}

		count++;
  %>

	<td width = "80%">
	<%=evUserDesc.get(i)%>
	</td>

	<td width = "20%">

	<A HREF="eventuserdelete.jsp?eventuserId=<%= evUserIds.get(i) %>&eventId=<%=eventId%>&srcmenu=<%=src%>&eventmode=<%=eventmode%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&calassoc=<%=calAssoc%>&eventName=<%=eventName%>&userType=U"  onClick="return confirmBox('<%= StringUtil.escapeSpecialCharJS((String)evUserDesc.get(i)) %>',<%=pageRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A>


	</td>

   </tr>

<%



	}

%>

</table>
<br>


<!-- end of resources code-->

<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>

  <td>

    <%

  	if (fromPage.equals("selectEvent")) {

         %>

		<A href="selecteventus.jsp?fromPage=NewEvent" type="submit"><%=LC.L_Back%></A>

 <%

 	}

 %>

  <%

  	if (fromPage.equals("eventbrowser")) {

  %>

		<A href=eventbrowser.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>  type="submit"><%=LC.L_Back%></A>

 <%

 	}

  	if (fromPage.equals("eventlibrary")) {

 %>

		<A href=eventlibrary.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%> type="submit"><%=LC.L_Back%></A>

 <%

 	}

  	//if (fromPage.equals("fetchProt")) {

  %>

		<!--<A href="fetchProt.jsp?srcmenu=<%=src%>&selectedTab=4&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&pageNo=1&displayType=<%=displayType%>&displayDur=<%=displayDur%>"  type="submit"><%=LC.L_Back%></A>-->

		<!-- <A href="fetchProt.jsp?srcmenu=<%=src%>&selectedTab=4&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&pageNo=1&displayType=<%=displayType%>&displayDur=<%=displayDur%>" >Back to Protocol Calendar : Customize Event Details</A>	 -->

 <%

 	//}

 %>



 </td>

</tr>

</table>

<%
 if (sessionInventoryRight.equals("1") && showStorageKit) { 
	  //get storage Kits
	  EventKitDao evk = new EventKitDao();
	
	  ArrayList arKNames = new ArrayList();
	  ArrayList arKIds = new ArrayList();
	  ArrayList arKPks = new ArrayList();
	  ArrayList arKIds1 = new ArrayList();
	
	
	  int kitcount = 0;
	
	  evk  = eventkitB.getEventStorageKits(EJBUtil.stringToNum(eventId));
	
	
	   arKIds =  evk.getStorageKits();
	   arKIds1 = evk.getStorageKitsIds();
	   arKNames =    evk.getStorageKitNames();
	   arKPks = evk.getIds();
	
	   if (arKIds != null)
	   {
	   	kitcount = arKIds.size();
	   }
	
	%>
	
	<P class = "defComments"><%=MC.M_StorKit_LnkToEvt%><%-- Storage Kits linked with this Event*****--%>: </P>
	<TABLE cellpadding="0" cellspacing="0" border="0" width="100%">
	 <tr>
	
		<th width = "40%"><%=LC.L_Storage_KitId%><%-- Storage Kit ID*****--%></th>
		<th width = "50%"><%=LC.L_Storage_KitName%><%-- Storage Kit Name*****--%></th>
		<th width = "50%"><%=LC.L_Delete%><%--Delete*****--%></th>
	   </tr>
	
	   	<% for ( int j = 0; j<kitcount ; j++ )
	   	{
	        if ((j%2)==0) {	%>
	
	      		<tr class="browserEvenRow">
	
	        <%
			}
			else
			{ %>
	
	      		<tr class="browserOddRow">
	        <%
			}
	      	%>
	
	 		<td width=40% > <%= (String)arKIds1.get(j) %> </td>
	 		<td width=50% > <%= (String)arKNames.get(j) %> </td>
	 		<td width = "10%">
	
	
	
		    <A HREF="#"  onClick="return confirmBoxKit('<%=(String)arKNames.get(j)%>',<%=pageRight%>,<%=(String)arKPks.get(j)%>,document.hiddenparam)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A>
	
		</td>
	    </tr>
	
	    <% } %>
	
	 </TABLE>
 <% } %>	

</form>

</div>

<%

	} //event



} else {  //else of if body for session



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%



}



%>



 <%if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){}
else {

%>

  


</div>

<div class ="mainMenu" id = "emenu">

<jsp:include page="getmenu.jsp" flush="true"/>

</div>

	<% }%>
<!-- Akshi:Added for bug #6948 -->
</div>
<div class = "myHomebottomPanel">

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>

</body>

<script>
function removecssfile(filename, filetype){
	 var targetelement=(filetype=="css")? "link" : "none" //determine element type to create nodelist from
	 var targetattr=(filetype=="css")? "href" : "none" //determine corresponding attribute to test for
	 var allsuspects=document.getElementsByTagName(targetelement)
	 for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
	  if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(filename)!=-1)
	   allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
	 }
	}
	removecssfile("velos_popup.css", "css") //remove all occurences "velos_popup.css" on page
</script>

</html>





