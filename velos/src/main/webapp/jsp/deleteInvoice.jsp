<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Del_Invoice%><%--Delete Invoice*****--%></title>

<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>

<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

<%-- 	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   } --%>
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.*,com.aithent.audittrail.reports.AuditUtils"%>
<jsp:useBean id="InvB" scope="request" class="com.velos.eres.web.invoice.InvoiceJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 

<% String src;
	src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<BODY> 
<br>

<DIV class="formDefault" id="div1">
<% 
String invoiceId= "";
String selectedTab = request.getParameter("selectedTab");
		
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{

	invoiceId= request.getParameter("invoiceId");

	String studyId = request.getParameter("studyId");



	int ret=0;
		
	String delMode=request.getParameter("delMode");
	
	if (delMode == null) {
		delMode="final";
%>
	<FORM name="invdelete" id="delInvId" method="post" action="deleteInvoice.jsp" onSubmit="if (validate(document.invdelete)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>

	<P class="defComments"><%=MC.M_Etr_Esign_ToProc%><%--Please enter e-Signature to proceed*****--%></P>
		
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="delInvId"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>

	<input type="hidden" name="delMode" value="<%=delMode%>">
	<input type="hidden" name="srcmenu" value="<%=src%>">
	<input type="hidden" name="invoiceId" value="<%=invoiceId%>">
	<input type="hidden" name="studyId" value="<%=studyId%>">  
	<input type="hidden" name="selectedTab" value="<%=selectedTab%>">
	

	</FORM>
<%
	} else {%>
   <%

			String eSign = request.getParameter("eSign");	
            invoiceId= request.getParameter("invoiceId");			
            studyId=request.getParameter("studyId");			
            selectedTab=request.getParameter("selectedTab");		
            src=	request.getParameter("srcmenu");		
            delMode=null;
			String oldESign = (String) tSession.getValue("eSign");

			if(!oldESign.equals(eSign)) {
%>        
	<br> <br> 	<br> <br> 	<br> <br> 	<br> <br> 	<br> <br> 

 <table width=100%>

 

<tr>

<td align=center>
<jsp:include page="incEsignPwd.jsp" flush="true"/>
</td>

</tr>

<tr height=20></tr>

<tr>

<td align=center>

<!--		<A href="#" onclick="window.history.back();"><img src="../images/jpg/Back.gif" align="absmiddle"  border=0> </A>-->


</td>		

</tr>		

</table>		
   <table>	<tr><td width="85%"></td><td>&nbsp;&nbsp;
<button tabindex=2 onclick="history.go(-1);"><%=LC.L_Back%></button>	
</td></tr>	</table>


	
          
 		  
         
<%
			} else {

			InvB.setId(EJBUtil.stringToNum(invoiceId));
			// Modified for INF-18183 ::: AGodara 
			ret = InvB.deleteInvoice(AuditUtils.createArgs(tSession,"",LC.L_Milestones));


			if (ret==-3) {%>
				<br><br><br><br><br> <p class = "successfulmsg" align = center>  <%=MC.M_DelRelatedPayment_OrInv%><%--This Invoice is linked with one or more payments. Please delete the related payment(s) and then try to delete this Invoice.*****--%> </p>			
				<META HTTP-EQUIV=Refresh CONTENT="5; URL=invoicebrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">				
			<%}else if (ret==-2) {%>
				<br><br><br><br><br> <p class = "successfulmsg" align = center>  <%=MC.M_InvCnt_DelSucc%><%--Invoice could not be deleted successfully.*****--%> </p>			
			<%}else { %>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Inv_DelSucc%><%--Invoice deleted successfully.*****--%> </p>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=invoicebrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">				
				 
		<%

			}
			
	
			} //end esign
			
			

	} //end of delMode
	
  }//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %> <div> 

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>

</DIV>  


<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/> 
</div>

</body>
</HTML>


