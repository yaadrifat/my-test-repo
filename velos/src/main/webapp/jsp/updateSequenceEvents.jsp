<%--PCAL-20461 For Updating Changes in Database for Events Sequence & Hide/Unhide Changes--%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>

<%@page import="com.velos.esch.business.common.EventdefDao"%>
<%@page import="com.velos.esch.business.common.EventAssocDao"%><jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
 <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
  <%

	HttpSession tSession = request.getSession(true);

    if (sessionmaint.isValidSession(tSession)) {
	
	String sequenceShuffleData = request.getParameter("sequenceShuffleData");	
	String calledFrom = request.getParameter("calledFrom");
	String protId= request.getParameter("protocolId");
	String userId = (String) tSession.getValue("userId");
	String ipAdd = (String) tSession.getValue("ipAdd");
	int orgId=0;	
	String eventOldSeq = "";
	String chainId="";
	EventdefDao evtdef=new EventdefDao();
	EventAssocDao eventassc =new EventAssocDao();
	calledFrom = calledFrom.trim();
	int setId = 0;
	JSONArray shuffleEvent;
	JSONObject shuffleEventUpdate = null;
	if (sequenceShuffleData.length() > 0) {
		try{
			shuffleEvent = new JSONArray(sequenceShuffleData);
			int eventLen = shuffleEvent.length();
	  		String[] eventIds = new String[eventLen];
	  		String[] eventNewSeqs = new String[eventLen];
		  	if (calledFrom.equals("P")  || calledFrom.equals("L")){
	    		for(int iS=0;iS<eventLen;iS++)
				{
					shuffleEventUpdate = shuffleEvent.getJSONObject(iS);
					eventIds[iS]=shuffleEventUpdate.getString("eventID");
					eventNewSeqs[iS]=shuffleEventUpdate.getString("eventSeqNew");
				}
	    		setId=evtdef.setEventsCost(protId,eventIds,eventNewSeqs,calledFrom,userId,ipAdd);
	    	}else if (calledFrom.equals("S")){
	    		for(int iS=0;iS<eventLen;iS++)
				{
					shuffleEventUpdate = shuffleEvent.getJSONObject(iS);
					eventIds[iS]=shuffleEventUpdate.getString("eventID");
					eventNewSeqs[iS]=shuffleEventUpdate.getString("eventSeqNew");
				}
	    		setId=eventassc.setEventsCost(protId,eventIds,eventNewSeqs,calledFrom,userId,ipAdd);
			}
		if(setId==0)
    		out.print(LC.L_Successful);/*out.print("Successfull");*****/
    	else
    		out.print("");/*out.print("Unsuccessfull");*****/
	}catch(Exception exp)
	{
		out.print(LC.L_Error);/*out.print("Error");*****/
    	System.out.print("Exception Caught in Updating Events Sequence : "+exp);
	}
	}
	
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>





