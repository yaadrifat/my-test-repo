<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC"%>
<%@page import="com.velos.eres.service.util.MC"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>

<%@page import="com.velos.eres.business.common.*,java.util.*"%>
<%@ page language = "java" import="com.velos.eres.service.util.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.velos.eres.service.util.Configuration"%><jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="tokenJB" scope="request" class="com.velos.eres.web.userSession.UserSessionTokenJB"/>
<%@page import="com.velos.eres.service.util.EJBUtil"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:include page="ui-include.jsp" flush="true"/> 
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}

private String cutOffdate() {
	
	String reformattedStr="";
	try{
	HashMap<String, String> cutOffDate = new HashMap<String, String>();
	// map to store the q
	cutOffDate.put("03","31");
	cutOffDate.put("06","30");
	cutOffDate.put("09","30");
	cutOffDate.put("12","31");
	Calendar currentCal = Calendar.getInstance();
	String quarterDate = "";
	String quarterMonth = "";
	int date = currentCal.get(Calendar.DATE) ;
	int month = (currentCal.get(Calendar.MONTH) + 1);
	int year = currentCal.get(Calendar.YEAR);
	
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 0);
        cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
        Date lastDateOfPreviousMonth = cal.getTime();
         
        int lastDate = lastDateOfPreviousMonth.getDate();
       
        if(date<lastDate)
        {
            month=month-1;
        }
        
	int prevQuarter = (month / 3); 
	switch(prevQuarter) {
	    case 3 : 
	    	quarterMonth="09";
	        // return September 30
	    	break;
	    case 2 :
	    	quarterMonth="06";
	        // return June 30
	    	break;
	    case 1 :
	    	quarterMonth="03";
	    	break;
	        // return March 31
	    case 0 : default :
	    	quarterMonth="12";
	    	break;
	        // return December 31
	}
	if(month<=3 && date<31  ){
		year=(year-1);
		quarterMonth="12";
	}
        
	
	quarterDate= cutOffDate.get(quarterMonth);
	
	String dateFinal =year+""+quarterMonth+""+quarterDate;
	
	SimpleDateFormat fromUser = new SimpleDateFormat("yyyyMMdd");
	SimpleDateFormat myFormat = new SimpleDateFormat("MM/dd/yyyy");

	reformattedStr = myFormat.format(fromUser.parse(dateFinal));
	
            System.out.println("reformattedStr "+reformattedStr);
	
	}catch (Exception e){
		 System.out.print("Exception"+e);
	}	
	return reformattedStr.toString();
}

%>
<%
// The only scriptlet in this JSP
	HttpSession mySession = request.getSession(true);
int ctrpAccrulRights=0;
int pageRight = 0;
try {
	if (!sessionmaint.isValidSession(request.getSession(false))) { return; }
	GrpRightsJB grpRights = (GrpRightsJB) request.getSession(false).getAttribute("GRights");
	if (grpRights == null) { return; }
	ctrpAccrulRights=Integer.parseInt(grpRights.getFtrRightsByValue("REGREPORT_CTRP"));
	if (Integer.parseInt(grpRights.getFtrRightsByValue("REGREPORT_CTRP")) < 4) {
		out.println("<div><p>&nbsp;</p><p class='sectionHeadings' align='center'>"+LC.L_Access_Forbidden+"</p></div>");
		return;
	}
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));
	// Added Condition For  Study Team Rights Bug#15565 
	acmod.getControlValues("study_rights","STUDYMPAT");
	ArrayList aRightSeq = acmod.getCSeq();
	String rightSeq = aRightSeq.get(0).toString();
	int iRightSeq = EJBUtil.stringToNum(rightSeq);
%>

<script>
var L_Access_Rights = "<%=LC.L_Access_Rights%>";
var L_Valid_Esign ="<%=LC.L_Valid_Esign%>";
var L_Invalid_Esign ="<%=LC.L_Invalid_Esign%>";
var CTRP_DraftTrialSubmCategory = "<%=LC.CTRP_DraftTrialSubmCategory%>";
var L_Create="<%=LC.L_Create%>";
var checkedStudyIds="";
var researchTypes="";
var managePatRights="";
var patValidationFlag=0;
var downloadAccrualData = "<%=LC.L_Down_Accrul_Data%>";
function checkSearch(){ 
	paginate_study.runFilter();
	checkedStudyIds="";
	managePatRights="";
	patValidationFlag=0;
}

function selectedStudyIds(status,researchType,managePatRight){
	if(status.checked){
		if(checkedStudyIds==""){
			checkedStudyIds=$j.trim(status.value)+"~"+$j.trim(researchType)+",";
			managePatRights=$j.trim(managePatRight)+",";
		}else{
			checkedStudyIds=$j.trim(checkedStudyIds)+$j.trim(status.value)+"~"+$j.trim(researchType)+",";
			managePatRights=$j.trim(managePatRights)+$j.trim(managePatRight)+",";
		}
	}
	else{
		var replaceStr = $j.trim(status.value)+"~"+$j.trim(researchType)+",";
		checkedStudyIds = replaceCheckedStudytIds($j.trim(checkedStudyIds), replaceStr, '');
		$j.trim(checkedStudyIds);

		var repStr = $j.trim(managePatRight)+",";
		managePatRights = replaceCheckedStudytIds($j.trim(managePatRights), repStr, '');
		$j.trim(managePatRights);
	}
}

function replaceCheckedStudytIds(allSelectedStudytIds, replace, replaceWithStr) {
	  return allSelectedStudytIds.replace(new RegExp(replace, 'g'),replaceWithStr);
}

studyCheckBox = function(elCell, oRecord, oColumn, oData) {
	var data=oData;
		var record=oRecord;
		var htmlStr="";
		var fkStudy=oRecord.getData("PK_STUDY");
		var resType=oRecord.getData("RESEARCH_TYPE");
		var sTeamRight=oRecord.getData("STUDY_TEAM_RIGHTS");
 		sTeamRight=((sTeamRight==null) || (!sTeamRight) )?"":sTeamRight;
		var seq=parseInt($j('#rightSeq').val());
		if (!seq) seq=0;
		var mProtRight=0;
		var mPatRight=0;
		var managePatRightSeq =<%=rightSeq%>;
		if (sTeamRight.length>seq){
 			mProtRight=parseInt(sTeamRight.substring(seq-1,seq));
 			mProtRight=((mProtRight==null) || (!mProtRight))?"":mProtRight;

 			mPatRight=parseInt(sTeamRight.substring(managePatRightSeq-1,managePatRightSeq));
 			mPatRight=((mPatRight==null) || (!mPatRight))?"":mPatRight;
		}
		if (f_check_perm_noAlert(mProtRight,'E')){
    		 htmlStr="<input type='checkbox' name='studycheckboxes' id='studycheckboxes' onclick=\"selectedStudyIds(this,'"+resType+"','"+mPatRight+"')\" value='"+fkStudy+"'>";
		}else{
		 	htmlStr="<a href=\"javascript:void(0);\" style=\"text-decoration:none;\"onmouseover=\"return overlib('"+M_InsuffAccRgt_CnctSysAdmin+"',CAPTION,'"+ CTRP_DraftTrialSubmCategory +"');\" onmouseout=\"return nd();\">&nbsp;<img src=\"./images/ctrp_images/Frozen.png\" style=\"border-style: none;\" /></a>";
		 	htmlStr +="<input type='checkbox' disabled name='studycheckboxes' id='studycheckboxes' value='"+record.getData("PK_STUDY")+"'>";
    	}
		elCell.innerHTML =htmlStr;
};

statusLink = function(elCell, oRecord, oColumn, oData){
	var record=oRecord;
  		var htmlStr="";
  		var cl="";
		var status=record.getData("STATUS"); 
		var nt=record.getData("STUDYSTAT_NOTE");
  		nt=((nt==null) || (!nt) )?"":nt;
  		nt=htmlEncode(nt);
  		nt = escapeSingleQuoteEtc(nt);
  		var tt = "<tr><td><font size=2><%=LC.L_Note%>:</font><font size=1>"+nt+"</font></td></tr><tr><td><font size=2><%=LC.L_Organization%>:</font><font size=1>"+record.getData("SITE_NAME")+"</font></td></tr>";
  		if(status!="..."){
      		htmlStr="<A href=\"studystatusbrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=3&studyId="+record.getData("PK_STUDY")   
  		+ "\" onmouseover=\"return overlib('"+tt+"',CAPTION,'"+record.getData("STUDYSTAT_DATE_DATESORT")+"');\" onmouseout=\"return nd();\"><FONT class=\""+cl+"\">"+status+"</FONT> </A>";
  		}else{
  			htmlStr=status;
      	}
  		elCell.innerHTML=htmlStr;
};
  
titleLink = function(elCell, oRecord, oColumn, oData){
	var record=oRecord;
    var htmlStr="";
	var title = record.getData("STUDY_TITLE"); 
	title = ((title==null) || (!title) )? "-":title;
	title = htmlEncode(title);
	title = escapeSingleQuoteEtc(title);
	if(title.length>50){
		var studyTitle=title.substring(0,50);
		htmlStr = studyTitle+"&nbsp;<img class=\"asIsImage\" src=\"./images/More.png\" border=\"0\" onmouseover=\"return overlib(\'"+title+"\',CAPTION,'<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>' ,LEFT , ABOVE);\" onmouseout=\"return nd();\">";
	}else{
		htmlStr=title;
	}
	elCell.innerHTML = htmlStr;
 };

studyLink = function(elCell, oRecord, oColumn, oData) {
	var data=oData;
		var record=oRecord;
 		var htmlStr="";
 		$j( "#dialog" ).hide();
 		$j( "#studiesValPassDialog" ).hide();
 		$j( "#dialogSelOneTypeStd" ).hide();
 		$j( "#dialogFilenameTooLong" ).hide();
 		$j('#ctrpAccrualOpts').show();
 		$j( "#inSufficientRights" ).hide();
 		
 		var studyNumber=record.getData("STUDY_NUMBER");
 		if(studyNumber.length>50){
 			studyNumber=studyNumber.substring(0,50);
 			htmlStr="<a href=\"study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId="+record.getData("PK_STUDY")+"\"><img src=\"../images/jpg/study.gif\" alt=\"<%=LC.L_Std_Admin%><%--<%=LC.Std_Study%> Administration*****--%>\" border=0></a> &nbsp;"+studyNumber+"&nbsp;<img class=\"asIsImage\" src=\"./images/More.png\" style=\"border-style: none;\" onmouseover=\"return overlib(\'"+record.getData("STUDY_NUMBER")+"\',CAPTION,'<%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Title*****--%>' ,LEFT , ABOVE);\" onmouseout=\"return nd();\"/>"; 			
		}else{
			htmlStr="<a href=\"study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId="+record.getData("PK_STUDY")+"\"><img src=\"../images/jpg/study.gif\" alt=\"<%=LC.L_Std_Admin%><%--<%=LC.Std_Study%> Administration*****--%>\" border=0></a> &nbsp;"+studyNumber;
			}
 		var sTeamRight=oRecord.getData("STUDY_TEAM_RIGHTS");
 		sTeamRight=((sTeamRight==null) || (!sTeamRight) )?"":sTeamRight;
 		var seq=parseInt($j('#rightSeq').val());
		if (!seq) seq=0;
		var mPatRight=0;
		if (sTeamRight.length>seq)
		{
 		mPatRight=parseInt(sTeamRight.substring(seq-1,seq));
 		mPatRight=((mPatRight==null) || (!mPatRight) )?"":mPatRight;
		}
		elCell.innerHTML =htmlStr;
};
validateAccrualsLink = function(elCell, oRecord, oColumn, oData) {
	var data=oData;
	var htmlStr="";
	var pkStudy=oRecord.getData("PK_STUDY");
	var researchType=oRecord.getData("RESEARCH_TYPE")==null?"":oRecord.getData("RESEARCH_TYPE");
	// Added Condition For  Study Team Rights Bug#15565 
	var sTeamRight=oRecord.getData("STUDY_TEAM_RIGHTS");
	sTeamRight=((sTeamRight==null) || (!sTeamRight) )?"":sTeamRight;
	var managePatRightSeq =<%=rightSeq%>;
	vmPatRight = 0;
	if (sTeamRight.length>managePatRightSeq){
		vmPatRight=parseInt(sTeamRight.substring(managePatRightSeq-1,managePatRightSeq));
		vmPatRight=((vmPatRight==null) || (!vmPatRight))?"":vmPatRight;
	}

	
	<%if ( isAccessibleFor(ctrpAccrulRights, 'E') && pageRight >= 4 ){ %>
	htmlStr = "<div align=\"center\"><a href=\"javascript:void(0)\" onclick=\"checkForStudyRightSingle('validate',"+pkStudy+",'"+researchType+"','"+vmPatRight+"');\" style=\"text-decoration:none;\"><img title=\"Validate\" style=\"border-style: none;\" src=\"./images/ctrp_images/validate.png\"></a></div>";
	<%}else{%>
	htmlStr="";
	<%}%>
	elCell.innerHTML =htmlStr;
};
// Added Condition For  Study Team Rights Bug#15565 
function checkForStudyRightSingle(type,pkStudy,researchType,patMangRigts){
	if( patMangRigts < 4 ){
		$j( "#inSufficientRightsSingle" ).show();
		$j( "#inSufficientRightsSingle" ).html("You do not have enough patients rights for this Study");
		$j( "#inSufficientRightsSingle" ).dialog({ resizable: false, height:'auto',
			 modal: true,
			 maxWidth: 300,
		     overflow:scroll,
		     buttons: {
		     	"Close": function() {
			         $j( this ).dialog( "close" );
			    }
	     	}
		});
		return false;
	}else{
		addCuttOffdate(type,pkStudy,researchType);
	}
}
function checkForSelectedStudyRight(type,pkStudy,researchType){
	
	//alert(managePatRights.length);
	 if(managePatRights.length > 0){
		var notSuffRightFlag = false;
		var singlePatRightsArr = managePatRights.split(",");
		for(var k=0; k<singlePatRightsArr.length;k++){
			if(k!=(singlePatRightsArr.length-1)){
				if(singlePatRightsArr[k] < 4 
						|| singlePatRightsArr[k]==''){
					notSuffRightFlag = true;
					break;
				}
			}
		}
		if(notSuffRightFlag){

			$j( "#inSufficientRights" ).show();
			$j( "#inSufficientRights" ).html("You do not have enough patients rights for one or more studies");
			$j( "#inSufficientRights" ).dialog({ resizable: false, height:'auto',
				 modal: true,
				 maxWidth: 300,
			     overflow:scroll,
			     buttons: {
			     	"Continue Download": function() {
			      		//processAction('download','test');
			      		addCuttOffdate(type,pkStudy,researchType);
				         $j( this ).dialog( "close" );
				     },
			     	"Close": function() {
				         $j( this ).dialog( "close" );
				     }
		     	}
			});
			return false;
		}else{
			addCuttOffdate(type,pkStudy,researchType);
		}
	}else{
		addCuttOffdate(type,pkStudy,researchType);
		
	}

}

function addCuttOffdate(type,pkStudy,researchType){
	if(type=="download" && checkedStudyIds.length<=0){
		$j( "#dialog" ).show();
		 $j( "#dialog" ).dialog({
			 resizable: false,
		     height:'auto',
		     maxWidth: 300,
		     overflow:scroll,
		     modal: true,
		     buttons: {
			     "Close": function() {
			         $j( this ).dialog( "close" );
			     }
	    	}
		});
			return;
	}
	$j("#cutOffDate").val('<%=cutOffdate()%>');
	$j( "#subjectAccrualCutOffDate" ).dialog({ resizable: false, height:'auto',maxWidth: 250,  modal: true,
		closeOnEscape: true,overflow: scroll,
		buttons: { 
			"Continue": function(){
				var cutOffDate=$j("#cutOffDate").val();
				if(cutOffDate==null || cutOffDate=="" || cutOffDate.length<=0){
					$j("#cuttOffError").show();
				}else{
					$j( this ).dialog( "close" );
					if(type=="download"){
					processAction('download');
					}
					if(type=="validate"){
					  validateStdAccr(pkStudy,researchType,cutOffDate);
					}
				}
			},
			"Cancel": function() {
					$j("#cuttOffError").hide();
			       			$j( this ).dialog( "close" ); 
			}
		     	 
     	         },
     	         close: function(ev, ui) {
     	        	$j("#cuttOffError").hide();
     	        	$j(this).dialog('destroy');  }
	
	});
	
}
function validateStdAccr(pkStudy,researchType,cutOffDate){

	var urlParam = "cutOffDate="+cutOffDate+"&pkStudy="+pkStudy+"~"+researchType+",";
	openMessageDialog('validationProgressMsg');
		$j.ajax({
			url:'validateStudyAccruals',
			data:urlParam,		
			success:(function (data){
				var userDataMap = data.userDataMap;
				var validationStrPat=userDataMap.validationStrPat;
				var validationStrStd=userDataMap.validationStrStd;
				var validationStrIndus=userDataMap.validationStrIndus;
				var indusPatEnrSite=userDataMap.indusPatEnrSite;
				var result=userDataMap.result;

				var fileName=userDataMap.fileName;
				closeMessageDialog('validationProgressMsg');

				if(result=="0"){
					$j( "#stdValidationSuccDialog" ).show();
					$j( "#stdValidationSuccDialog" ).dialog({ resizable: false, height:'auto',maxWidth: 250,  modal: true,
					     buttons: { "Close": function() {
					         $j( this ).dialog( "close" );
					         }					     
						}
					});
				}else{
					if(researchType==("Industrial")){

						var valStr="<p><b>"+M_CTRP_ACCR_Val_Fail_Rea+"</b><p>";
						
						
						if(typeof(validationStrIndus) != 'undefined' ||typeof(indusPatEnrSite) != 'undefined'){

							if(typeof(validationStrIndus) != 'undefined'){
								valStr = valStr +"<p>" +validationStrIndus+"</p>";
							}
														
							if(typeof(indusPatEnrSite) != 'undefined'){
								var patEnrSite  = indusPatEnrSite.split(",");
								
								$j.each(patEnrSite, function() {
									
								   var patData = this.split(":");
								   var patStdId = patData[0];
								   var siteName = patData[1];
								   var paramArray = [patStdId, siteName];
								   valStr = valStr + "<p>"+ getLocalizedMessageString('M_PatEnrSite_NotOnStdTeam',paramArray)+"</p>";							   
								});
							}
							$j("#induaStdValFailDialog").html(valStr);
						if($j.browser.msie){
							$j( "#induaStdValFailDialog" ).dialog({ resizable: false, height:'auto',maxWidth: 250,  modal: true,
								closeOnEscape: true,overflow: scroll,
								buttons: {
									 "<%=LC.L_Close%>" : function() {
										 deleteErrorList(fileName,1);
									     },
									     "<%=LC.L_Save%>": function() {
									    	 downloadErrorList(fileName,0);
									         //$j( this ).dialog( "close" );
									     }
							    	}
							     ,close: function(ev, ui) {$j(this).dialog('destroy');  }
							
							});
						}else{
							
							$j( "#induaStdValFailDialog" ).dialog({ resizable: false, height:'auto',maxWidth: 250,  modal: true,
								closeOnEscape: true,overflow: scroll,
								 buttons: {
									 "<%=LC.L_Close%>" : function() {
										 deleteErrorList(fileName,1);
									     },
									     "<%=LC.L_Save%>": function() {
									    	 downloadErrorList(fileName,0);
									         //$j( this ).dialog( "close" );
									     }
							    	}
					     	,close: function(ev, ui) {$j(this).dialog('destroy');  }
							});
						}
						}
					}else{
						

						var valStr = "<p><b>"+M_CTRP_ACCR_Val_Fail_Rea+"</b><p>";
						
						if(typeof(indusPatEnrSite) != 'undefined'){
							var patEnrSite  = indusPatEnrSite.split(",");
							$j.each(patEnrSite, function() {
							   var patData = this.split(":");
							   var patStdId = patData[0];
							   var siteName = patData[1];
							   var paramArray = [patStdId, siteName];
							   valStr = valStr + "<p>"+ getLocalizedMessageString('M_PatEnrSite_NotOnStdTeam',paramArray)+"</p>";							   
							});
						}

						
						valStr = valStr + "<table class='basetbl'>";



						if(typeof(validationStrStd) != 'undefined'){
							valStr = valStr + "<tr><td colspan='2'>"+validationStrStd+"</td></tr>";
						} else if(typeof(validationStrPat) != 'undefined'){
							var patientsData = validationStrPat.split("~");
							valStr = valStr + "<tr><th>"+L_CTRP_ACCR_Pat_Id+"</th><th>"+L_CTRP_ACCR_Mis_Flds+"</th></tr>";
							$j.each(patientsData, function() {
							   var patData = this.split(":");
							   var patID = patData[0];
							   var patMsg = patData[1];
							   valStr = valStr + "<tr><td>"+patID+"</td><td>"+patMsg+"</td></tr>";
							});
						}
						valStr = valStr + "</table>";
						$j( "#nonInduaStdValFailDialog" ).html(valStr);
						
						if($j.browser.msie){
							$j( "#nonInduaStdValFailDialog" ).dialog({ resizable: false, height:'auto', maxWidth: 450,  modal: true,
								closeOnEscape: true,overflow: scroll,
								buttons: {
									 "<%=LC.L_Close%>" : function() {
										 deleteErrorList(fileName,1);
									     },
									     "<%=LC.L_Save%>": function() {
									    	 downloadErrorList(fileName,0);
									         //$j( this ).dialog( "close" );
									     }
							    	},close: function(ev, ui) {$j(this).dialog('destroy');  }
							});
						}else{
							$j( "#nonInduaStdValFailDialog" ).dialog({ resizable: false, height:'auto', maxWidth: 450,width:'auto',  modal: true,
								closeOnEscape: true,overflow: scroll,
								buttons: {
									 "<%=LC.L_Close%>" : function() {
										 deleteErrorList(fileName,1);
									     },
									     "<%=LC.L_Save%>": function() {
									    	 downloadErrorList(fileName,0);
									         //$j( this ).dialog( "close" );
									     }
							    	},close: function(ev, ui) {$j(this).dialog('destroy');  }
							});
						}
					}
				}
			})		
	});
}

$E.addListener(window, "load", function() {
	$('CSRFToken').value="<%=StringUtil.trueValue(tokenJB.createTokenForUser((String)mySession.getAttribute("userId")==null?0:StringUtil.stringToNum((String)mySession.getAttribute("userId"))))%>";
	paginate_study=new VELOS.Paginator('ctrpAccrualBrowser',{
					rowsPerPage  : 10,
	 				sortDir:"asc", 
					sortKey:"STUDY_NUMBER",
					defaultParam:"userId,accountId,grpId,superuserRights",
					filterParam:"searchCriteria,nciTrialIden,protocolStatus,CSRFToken",
					dataTable:'serverpagination',
					rowSelection:[5,10,15,25,50,75,100],
					navigation: true,
					rowsPerPage:10
					});
	checkSearch();
	}

)  
function processAction(type){	
	if(type=="download"){
		if(checkedStudyIds.length>0){
			var indusFlag =	false;
			var nonIndusFlag =	false;
			document.getElementById('studyIds').value=checkedStudyIds;
			var cutOffDate = document.getElementById('cutOffDate').value;
			var resTypeArr = new Array();
			var studyIdsArr = new Array();
			var singleDataArr = checkedStudyIds.split(",");
			for(var k=0; k<singleDataArr.length;k++){
				var arraySize = singleDataArr.length;
				var dataArr = singleDataArr[k].split("~");
				studyIdsArr[k]=dataArr[0];
				resTypeArr[k]=dataArr[1];
			}
			if($j.inArray("Industrial", resTypeArr)!=-1){
				indusFlag = true;
			}
			if($j.inArray("Other", resTypeArr)!=-1 || $j.inArray("Institutional", resTypeArr)!=-1 || $j.inArray("National", resTypeArr)!=-1){
				nonIndusFlag = true;
			}
			if (indusFlag && nonIndusFlag){
				$j( "#dialogSelOneTypeStd" ).show();
				$j( "#dialogSelOneTypeStd" ).dialog({ resizable: false, height:'auto',
					 modal: true,
					 maxWidth: 300,
				     overflow:scroll,
				     buttons: { "Close": function() {
					         $j( this ).dialog( "close" );
					     }
			     	}
				});
				return false;
			}
			var urlParam = "cutOffDate="+cutOffDate+"&selectedStudyIds="+checkedStudyIds;
			openMessageDialog('validationProgressMsg');
			$j.ajax({
				url:"ctrpPatientAccural",
				data:urlParam,
				success:onSuccessDataDownload
			});
		}else{
			$j( "#dialog" ).show();
			 $j( "#dialog" ).dialog({
				 resizable: false,
			     height:'auto',
			     maxWidth: 300,
			     overflow:scroll,
			     modal: true,
			     buttons: {
				     "Close": function() {
				         $j( this ).dialog( "close" );
				     }
		     	}
			});
		}
	}
}

function onSuccessDataDownload(data) {
	closeMessageDialog('validationProgressMsg');
	var userDataMap = data.userDataMap;
	var zipFileName = userDataMap.zipFileName;
	var studyIdList= userDataMap.studyIdList;
	var result = userDataMap.result;
	var validationStr = userDataMap.validationStr;
	if(zipFileName=='fileNameTooLong'){
		$j( "#dialogFilenameTooLong" ).show();
		$j( "#dialogFilenameTooLong" ).dialog({ resizable: false, height:'auto',
			 modal: true,
			 maxWidth: 300,
		     overflow:scroll,
		     buttons: { "Close": function() {
			         $j( this ).dialog( "close" );
			     }
	     	}
		});
		return false;
	}
	if(result=="0"){
		$j( "#studiesValPassDialog" ).show();
		$j( "#studiesValPassDialog" ).dialog({
			 resizable: false,
		     height:'auto',
		     maxWidth: 300,
		     overflow:scroll,
		     modal: true,
		     buttons: {
			 "<%=LC.L_Down_Accrul_Data%>" : function() {
			         downloadPatientAccrual(zipFileName,result,studyIdList,0);
			     },
			     "<%=LC.L_Cancel%>": function() {
			    	 deletePatientAccrual(zipFileName,result,studyIdList,1);
			         //$j( this ).dialog( "close" );
			     }
	    	}
		});
	}else{
		if(typeof(validationStr)!= 'undefined'){

			var valStr = "<p>"+M_CTRP_ACCR_Val_Fail+"<p><table class='basetbl'><tr><td>"+validationStr+"</td></tr></table>";
			$j( "#studiesValFailDialog" ).html(valStr);

			if($j.browser.msie){
				$j( "#studiesValFailDialog" ).dialog({
					 resizable: true,closeOnEscape: true,overflow: scroll,
				     height:'auto',
				     maxWidth: 300,
				     overflow:scroll,
				     modal: true,
				     buttons: {
					 "<%=LC.L_Down_Accrul_Data%>" : function() {
					         downloadPatientAccrual(zipFileName,result,studyIdList,0);
					     },
					       "<%=LC.L_Cancel%>": function() {
					    	 deletePatientAccrual(zipFileName,result,studyIdList,1);
					       	}
			    	},close: function(ev, ui) {$j(this).dialog('destroy');  }
				});


			}else{

				$j( "#studiesValFailDialog" ).dialog({
					 resizable: true,closeOnEscape: true,overflow: scroll,
				     height:'auto',
				     maxWidth: 300,
				     overflow:scroll,
				     modal: true,
				     buttons: {
					 "<%=LC.L_Down_Accrul_Data%>" : function() {
					         downloadPatientAccrual(zipFileName,result,studyIdList,0);
					     },
					       "<%=LC.L_Cancel%>": function() {
					    	 deletePatientAccrual(zipFileName,result,studyIdList,1);
					       	}
			    	},close: function(ev, ui) {$j(this).dialog('destroy');  }
				});
			}
		}
	}
}

function handleFailure() {
	closeMessageDialog('validationProgressMsg');
}

// Genric function to open a dialog with supplied Id
function openMessageDialog(divId)
{
	var messageHgth = navigator.userAgent.indexOf("MSIE") != -1 ? 130 : 70;
	jQuery("#"+divId).dialog({
		height: messageHgth,maxWidth: 250,position: 'center',resizable: false,modal: true,autoOpen: false
    }).siblings('.ui-dialog-titlebar').remove();
	jQuery("#"+divId).dialog("open");
}
//Genric function to close a dialog with supplied Id
function closeMessageDialog(divId)
{
	jQuery("#"+divId).dialog("close");
}

//To Download the ZIP File.
function downloadPatientAccrual(zipFileName,result,studyIdList,delFlag){
	document.getctrpAccrualBrowser.zipFileName.value=zipFileName;
	document.getctrpAccrualBrowser.result.value=result;
	document.getctrpAccrualBrowser.downloadedStudyIds.value=studyIdList;
	document.getctrpAccrualBrowser.delFlag.value=delFlag;
	document.getctrpAccrualBrowser.cutOffDateForPat.value=$j("#cutOffDate").val();
	document.getctrpAccrualBrowser.action="patStdAccrualDwnload"; 
	document.getctrpAccrualBrowser.method="POST";
	document.getctrpAccrualBrowser.submit();
	clearValues();
	closeMessageDialog('studiesValFailDialog');
	closeMessageDialog('studiesValPassDialog');
	
	closeMessageDialog("studiesValPassDialog");
	paginate_study.runFilter();
}

//To Delete the ZIP if User Press Cancel
function deletePatientAccrual(zipFileName,result,studyIdList,delFlag){
	var urlParam = "zipFileName="+zipFileName+"&downloadedStudyIds="+studyIdList+"&result=1&delFlag="+delFlag;
	$j.ajax({
		url:'patStdAccrualDwnload',
		type: "POST",
		async:false,
		data:urlParam,		
		success:(function (data){
			closeMessageDialog('validationProgressMsg');
		})
	});
	closeMessageDialog('studiesValFailDialog');
	closeMessageDialog('studiesValPassDialog');

}

function clearValues(){
	checkedStudyIds="";
}

resType=	function(elCell, oRecord, oColumn, oData)
{
	var record=oRecord;
		var htmlStr="";
	var title = record.getData("RESEARCH_TYPE"); 
	var researchSubType=record.getData("RESEARCH_SUB_TYPE")==null?"":record.getData("RESEARCH_SUB_TYPE");
	title = ((title==null) || (!title) )? "-":title;
	title = htmlEncode(title);
	title = escapeSingleQuoteEtc(title);
	var color;
	//Bug#7936
	if (researchSubType == "other" || researchSubType == "insti" || researchSubType == "national"){
	color="green";			
	}else if(researchSubType == "indus"){
		color="blue";
	}else{
	 color="red";
	 }
	htmlStr = "<font color="+color+">"+title+"</font>"; 
	elCell.innerHTML = htmlStr;
}

//To Download the error File.
function downloadErrorList(errorListFileName,delFlag){

	document.getctrpAccrualBrowser.errorListFileName.value=errorListFileName;
	document.getctrpAccrualBrowser.delFlag.value=delFlag;
	document.getctrpAccrualBrowser.action="downloadAccErrorList"; 
	document.getctrpAccrualBrowser.method="POST";
	document.getctrpAccrualBrowser.submit();
	closeMessageDialog('nonInduaStdValFailDialog');
	closeMessageDialog('induaStdValFailDialog');
}

//To Delete the error file if User Press Cancel
function deleteErrorList(errorListFileName,delFlag){
	var urlParam = "errorListFileName="+errorListFileName+"&delFlag="+delFlag;
	$j.ajax({
		url:'downloadAccErrorList',
		type: "POST",
		async:false,
		data:urlParam,		
		success:(function (data){
			closeMessageDialog('validationProgressMsg');
		})
	});
	closeMessageDialog('nonInduaStdValFailDialog');
	closeMessageDialog('induaStdValFailDialog');
}

</script>
<body  class="yui-skin-sam" style="overflow: hidden;">
<div style="margin-top: 10px;">
<s:form method="post" action="ctrpAccrualBrowser" name="getctrpAccrualBrowser" id="getctrpAccrualBrowser" onsubmit="return false;">

<br>
<s:hidden id="accountId" name="accountId" value="%{ctrpAccrualJB.accountId}"></s:hidden>
<s:hidden id="studyIds" name="studyIds" ></s:hidden>
<s:hidden id="userId" name="userId" value="%{ctrpAccrualJB.userIdS}"></s:hidden>
<s:hidden id="grpId" name="grpId" value="%{ctrpAccrualJB.grpId}"></s:hidden>
<s:hidden id="groupName" name="groupName" value="%{ctrpAccrualJB.groupName}"></s:hidden>
<s:hidden id="rightSeq" name="rightSeq" value="%{ctrpAccrualJB.rightSeq}"></s:hidden>
<s:hidden id="superuserRights" name="superuserRights" value="%{ctrpAccrualJB.superuserRights}"></s:hidden>
<s:hidden name="zipFileName" value=""></s:hidden>
<s:hidden name="errorListFileName" value=""></s:hidden>
<s:hidden name="cutOffDateForPat" value=""></s:hidden>
<s:hidden name="result" value=""></s:hidden>
<s:hidden name="downloadedStudyIds" value=""></s:hidden>
<s:hidden name="delFlag" value=""></s:hidden>
<s:hidden name="moduleName" id="moduleName" value="ctrpaccrualbrowser"></s:hidden>
 <s:hidden id="CSRFToken" name="CSRFToken"></s:hidden>
<table class="basetbl" cellpadding="0" cellspacing="0" width="99%">
<tr>
	<td align="left" width="12%" colspan="6" >&nbsp;<%=LC.L_Search_By %>:</td>
</tr>
<tr>	
	<td width="12%" id="lookupid" align="right"><%=LC.L_Study_Number%></td>
	<td>
		<input type="text" id="searchCriteria" name="searchCriteria" /></input>
	</td>
	<td width="10%" align="right">&nbsp;<%=LC.CTRP_DraftNCITrialId %></td>
	<td>
		<s:textfield id="nciTrialIden" name="nciTrialIden" maxlength="35"></s:textfield>
	</td>
	<td width="16%" align="right">&nbsp;<%=LC.L_Study_Status %></td>
	<td>
		<s:property escape="false" value="ctrpAccrualJB.protocolStatus" />
	</td>
</tr>
<tr>	
	<td colspan="6" align="right" style="padding-right: 10px">
	<button type="submit" onclick="checkSearch();"><%=LC.L_Search %></button>
	</td>
</tr>
</table>
<hr/>
<div id="ctrpAccrualOpts" style="display: none; margin-left: 10px;">
<%if (isAccessibleFor(ctrpAccrulRights, 'N') && pageRight >= 4 ){ %>
<%=LC.L_To_Selected %>
	<a href="javascript:void(0)" id="save_changes" onclick="checkForSelectedStudyRight('download','','');"><%=LC.L_Download%></a>
<%} %>
</div>

<div id="dialog" title="<%=LC.L_Subj_Accrual %>" style="display: none;">
  <p><div class="bd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; overflow: visible; height:3em;"><br /><%=MC.M_PlsSel_OneStd %></div></p>
</div>

<div id="dialogSelOneTypeStd" title="<%=LC.L_Subj_Accrual %>" style="display: none; height: 80.2667px;">
  <p><div class="bd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; overflow: visible; height:3em;"><br /><%=MC.M_SelOneTypeStd %></div></p>
</div>
<div id="dialogFilenameTooLong" title="<%=LC.L_Subj_Accrual %>" style="display: none; height: 80.2667px;">
  <p><div class="bd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; overflow: visible; height:3em;"><br /><%=MC.M_Filename_Too_Long %></div></p>
</div>
<div id="studiesValFailDialog" title="<%=LC.L_Subj_Accrual_Vldtn %>" style="display: none;"></div>
<div id="studiesValPassDialog" title="<%=LC.L_Subj_Accrual_Vldtn %>" style="display: none;">
  <div class="bd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; overflow: visible; height:3em;"><br /><%=MC.M_CTRP_ACCR_ALLSTDVAL_PASS %></div>
</div>
 <div id="subjectAccrualCutOffDate" title="<%=LC.L_Subj_Accrual_Vldtn %>" style="display: none;">
<%=LC.L_Enter_CutOff_Date %> : <input type="text" id="cutOffDate" name="cutOffDate" class="cusdatefield" size ="20" MAXLENGTH ="20" readonly>
<BR/>
<BR/>
<span id="cuttOffError" id="cuttOffError" class='basetbl' style="color:red;display:none;"><%=MC.M_CTRP_CutOff_Date %></span>
</div>
<br>
<div id="serverpagination" style="margin-top: 10px; margin-left: 5px; " >
</div>

<div id="inSufficientRights" title="<%=LC.L_Subj_Accrual_Vldtn %>" style="display: none;">
  <div class="bd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; overflow: visible; height:3em;"><br /></div>
</div>
<div id="inSufficientRightsSingle" title="<%=LC.L_Subj_Accrual_Vldtn %>" style="display: none;">
  <div class="bd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; overflow: visible; height:3em;"><br /></div>
</div>
</s:form>
</div>
<div id='validationProgressMsg' style="display:none;"><p class="sectionHeadings" align="center"> <%=LC.L_PleaseWait_Dots %><%-- Please wait...*****--%> </p></div>

<div id="stdValidationSuccDialog" title="<%=LC.L_Subj_Accrual_Vldtn %>" style="display: none;">
  <div class="bd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; overflow: visible; height:3em;"><br/><%=MC.M_CTRP_ACCR_STDVAL_PASS %></div>
</div>
<div id="induaStdValFailDialog" title="<%=LC.L_Subj_Accrual_Vldtn %>" style="display: none;"></div>

<div id="nonInduaStdValFailDialog" title="<%=LC.L_Subj_Accrual_Vldtn %>" style="display: none;"></div>
</body>
<%} catch(Exception e) { return; }
%> 