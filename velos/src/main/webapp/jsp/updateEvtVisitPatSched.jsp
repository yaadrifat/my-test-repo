<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@page import="com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.MC"%>
<%@page import="com.velos.esch.business.common.EventResourceDao,com.velos.esch.web.eventresource.EventResourceJB"%>
<%@page import="com.velos.esch.web.eventdef.EventdefJB,com.velos.esch.web.eventassoc.EventAssocJB"%>
<%@page import="java.util.ArrayList,java.util.Enumeration,com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.LC"%>
<%@page import="com.velos.esch.audit.web.AuditRowEschJB"%>

<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	HttpSession tSession = request.getSession(true);
	JSONObject jsObj = new JSONObject();
	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
		out.println(jsObj.toString());
		return;
	}
	
	String oldESign = (String) tSession.getAttribute("eSign");
	String eSign = request.getParameter("eSign");
	if (!oldESign.equals(eSign)) {
		jsObj.put("result", -2);
		jsObj.put("resultMsg", MC.M_EtrWrongEsign_TryAgain);/*jsObj.put("resultMsg", "You entered a wrong e-signature. Please try again.");*****/
		out.println(jsObj.toString());
		return;
	}
	
	String ipAdd = (String) tSession.getAttribute("ipAdd");
    String userId = (String) tSession.getAttribute("userId");
	String accountId = (String) tSession.getAttribute("accountId");

	String enrolledId = request.getParameter("enrolledId");
	int returnVal = -1;
	String paramName = null;
	Enumeration paramNames = request.getParameterNames();
	ArrayList selEventList = new ArrayList();
	while(paramNames.hasMoreElements()) {
	    paramName = (String)paramNames.nextElement();
	    if (paramName == null) { continue; } 
	    if (paramName.startsWith("selEvent")) {
	        selEventList.add(paramName.substring(8));
	    }
	}
	
	if (selEventList.size() == 0) {
		jsObj.put("result", -3);
		jsObj.put("resultMsg", MC.M_No_OperPerformed);/*jsObj.put("resultMsg", "No operations were performed");*****/
		out.println(jsObj.toString());
		return;
	}
	
    try {
        ArrayList rowNumList = new ArrayList();
        ArrayList eventIdList = new ArrayList();
        ArrayList eventIdListTemp = new ArrayList();
        ArrayList eventStatusList = new ArrayList();
        ArrayList oldEventStatusList = new ArrayList();
        ArrayList statusValidList = new ArrayList();
        ArrayList covTypeList = new ArrayList();
        ArrayList reasonList = new ArrayList();
        ArrayList notesList = new ArrayList();
        ArrayList serviceSiteList = new ArrayList();
		for (int iX=0; iX<selEventList.size(); iX++) {
		    String rowNum = (String)selEventList.get(iX);
		    rowNumList.add(rowNum);
		    eventIdList.add(StringUtil.trueValue(request.getParameter("eventId"+rowNum)));
		    eventIdListTemp.add("LPAD("+StringUtil.trueValue(request.getParameter("eventId"+rowNum))+",10,0)");
		    eventStatusList.add(StringUtil.trueValue(request.getParameter("eventStatus"+rowNum)));
		    oldEventStatusList.add(StringUtil.trueValue(request.getParameter("oldStatus"+rowNum)));
		    statusValidList.add(StringUtil.trueValue(request.getParameter("statusValid"+rowNum)));
		    covTypeList.add(StringUtil.trueValue(request.getParameter("covType"+rowNum)));
		    reasonList.add(StringUtil.trueValue(request.getParameter("reason"+rowNum)));
		    notesList.add(StringUtil.trueValue(request.getParameter("notes"+rowNum)));
		    serviceSiteList.add(StringUtil.trueValue(request.getParameter("serviceSite"+rowNum)));
		}
		String[] templateObj = {};
		String[] eventIdArray = (String[]) eventIdList.toArray(templateObj);
		String[] eventIdArrayTemp = (String[]) eventIdListTemp.toArray(templateObj);
		String[] eventStatusArray = (String[]) eventStatusList.toArray(templateObj);
		String[] oldEventStatusArray = (String[]) oldEventStatusList.toArray(templateObj);
		String[] statusValidArray = (String[]) statusValidList.toArray(templateObj);
		String[] covTypeArray = (String[]) covTypeList.toArray(templateObj);
		String[] reasonArray = (String[]) reasonList.toArray(templateObj);
		String[] notesArray = (String[]) notesList.toArray(templateObj);
		String[] serviceSiteArray = (String[]) serviceSiteList.toArray(templateObj);
		/*
	    StringBuffer sb = new StringBuffer();
		for (int iX=0; iX<eventIdArray.length; iX++) {
		    sb.append("\neventId=").append(eventIdArray[iX]).append("\n");
		    sb.append("eventStatus=").append(eventStatusArray[iX]).append("\n");
		    sb.append("oldEventStatus=").append(oldEventStatusArray[iX]).append("\n");
		    sb.append("statusValid=").append(statusValidArray[iX]).append("\n");
		    sb.append("covType=").append(covTypeArray[iX]).append("\n");
		    sb.append("reason=").append(reasonArray[iX]).append("\n");
		    sb.append("notes=").append(notesArray[iX]).append("\n");
		    sb.append("serviceSite=").append(serviceSiteArray[iX]).append("\n");
		}
		System.out.println(sb);
		*/
		returnVal = eventdefB.saveMultipleEventsVisits(EJBUtil.stringToNum(enrolledId), EJBUtil.stringToNum(userId),
		        eventIdArray, eventStatusArray, oldEventStatusArray, statusValidArray, notesArray, serviceSiteArray,
		        covTypeArray, reasonArray, ipAdd);
		// System.out.println("returnVal="+returnVal);
	ArrayList frtrue=new ArrayList();
	ArrayList falsev=new ArrayList();
		int index =0;
		boolean exccurr;
		
		String remarks = request.getParameter("remarks");
		AuditRowEschJB schAuditJB = new AuditRowEschJB();
				 
		if (!StringUtil.isEmpty(remarks)){
		schAuditJB.setReasonForChangeOfScheduleEvent(eventIdArrayTemp, StringUtil.stringToNum(userId), remarks);
			for(int i=0 ; i<eventIdArray.length ; i++)
			 {
				String statId = eventStatusArray[i];
				String oldStatId = oldEventStatusArray[i];
			//int evtId = StringUtil.stringToNum(eventIdArray[i]);
			boolean excludeCurrent = (!statId.equals(oldStatId))?true:false;
				
				if(excludeCurrent){
					frtrue.add(StringUtil.trueValue(eventIdArrayTemp[i]));
				}
				else{
					falsev.add(StringUtil.trueValue(eventIdArrayTemp[i]));
				}
				index++;
			 }
			if (frtrue.size()>0){
				schAuditJB.setReasonForChangeOfScheduleEventStatus(frtrue, StringUtil.stringToNum(userId), 
								remarks, true);
			}
			if(falsev.size()>0) {
				schAuditJB.setReasonForChangeOfScheduleEventStatus(falsev, StringUtil.stringToNum(userId), 
								remarks, false);	
			}
		 }
		// Copy-pasted from mulEventsSave.jsp for SCH_EVRES_TRACK 
		for(int g=0 ; g<eventIdArray.length; g++)	 { //for-loop1 //how many events
			String eventId = eventIdArray[g];
			ArrayList evtStats = null;
			ArrayList cDescs = null;
			ArrayList cIds = null;
			ArrayList cDuration = null;

			String evtStat = "";
			String cDesc = "";
			int cId = 0;
			String cRoleDur = null;
			
			ArrayList evDuration = new ArrayList();
			String selTrackingId ="";
			String durationStr = null;
			String evtResTrackId = "";
			int eventStatusId = 0;

			EventResourceJB eventResB = new EventResourceJB();
			EventResourceDao evResDao = eventResB.getAllRolesForEvent(EJBUtil.stringToNum(eventId));

			ArrayList evtResTrackIds = evResDao.getEvtResTrackIds();
			evtStats = evResDao.getEvtStats();
			cIds = evResDao.getEvtRoles();
			cDuration = evResDao.getEvtTrackDurations();
			cDescs = evResDao.getResourceNames();
			int cRows = cIds.size();
			int fidx = 0;
			int midx = 0;
			int lidx = 0;

			for (int f=0; f<cRows; f++){ //how many roles associated -- for-loop2
				String ddval = "00";
				String hhval = "00";
				String mmval = "00";
				String ssval = "00";
				
				cDesc = (String) cDescs.get(f);
				cId =  EJBUtil.stringToNum(((String) cIds.get(f)));
				cRoleDur = (String) cDuration.get(f);
				evtResTrackId = (String) evtResTrackIds.get(f);

				//avoid duplicate roles, Modified for INF-18183 ::: Ankit
				eventResB.removeEventResources(EJBUtil.stringToNum(evtResTrackId), AuditUtils.createArgs(session,"",LC.L_Mng_Pats));

				if (!(cRoleDur == null) && ! (cRoleDur.equals("null")) && cRoleDur.trim().length() > 0) {
					// get the dd, hh, mm, ss values
					fidx = 	cRoleDur.indexOf(":");
					midx = 	cRoleDur.indexOf(":",fidx+1);
					lidx = 	cRoleDur.lastIndexOf(":");
					
					ddval = cRoleDur.substring(0,fidx);
					hhval = cRoleDur.substring(fidx+1,midx);
					mmval = cRoleDur.substring(midx + 1,lidx);
					ssval = cRoleDur.substring(lidx+1);
					
					if ( ddval.trim().length() == 0 ) ddval = "00";
					if ( hhval.trim().length() == 0 ) hhval = "00";
					if ( mmval.trim().length() == 0 ) mmval = "00";
					if ( ssval.trim().length() == 0 ) ssval = "00";

					durationStr = ((ddval.trim().length() == 1) ?  "0" + ddval : ddval) + ":" +((hhval.trim().length() == 1) ?  "0" + hhval : hhval) + ":" + ((mmval.trim().length() == 1) ?  "0" + mmval : mmval) + ":" + ((ssval.trim().length() == 1) ?  "0" + ssval : ssval);
					evDuration.add(durationStr);
				}
			} //end of for-loop2

			eventStatusId  = eventResB.getStatusIdOfTheEvent(EJBUtil.stringToNum(eventId));
			
			eventResB.setEvtStat("" + eventStatusId);
			eventResB.setEvtRoles(cIds);
			eventResB.setEvtTrackDurations(evDuration);
			eventResB.setCreator(userId);
			eventResB.setIpAdd(ipAdd);
			eventResB.setEventResourceDetails();
			
		} //end of for-loop1
		// end of copy-paste from mulEventsSave.jsp for SCH_EVRES_TRACK
	} catch(Exception e) {
		jsObj.put("result", -4);
		jsObj.put("resultMsg", e);
		out.println(jsObj.toString());
		return;
	}
	
	if (returnVal < 0) {
		jsObj.put("result", returnVal);
		jsObj.put("resultMsg",MC.M_DataCnt_Svd);/*jsObj.put("resultMsg", "Data could not be saved.");*****/
		out.println(jsObj.toString());
		return;
	}

    jsObj.put("result", 0);
    jsObj.put("resultMsg",MC.M_Changes_SavedSucc);/*jsObj.put("resultMsg", "Changes saved successfully");*****/
	out.println(jsObj.toString());
%>
