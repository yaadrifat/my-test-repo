<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<title><%=LC.L_Selection%><%--Selection*****--%></title>
<html>
<head>
<style>
html,body{
    overflow: hidden !important;
}
</style>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.service.util.*,java.util.ArrayList,java.util.List,java.util.StringTokenizer,java.util.Arrays"%>
<jsp:include page="include.jsp" flush="true"/>
	<title><%=MC.M_NoTitleYet%><%--No title yet*****--%>!</title>
<script language="JavaScript">
<!-- 
ok=1
function onClose()
{ 

//if (ok)
if (window.screenTop>10000)
{
var paramArray = [document.select];
alert(getLocalizedMessageString("L_ClsCln",paramArray));/*alert("close and clean" + document.select);*****/
document.select.action="cleansession.jsp?event=multilookup";
var paramArray = [document.select.action];
alert(getLocalizedMessageString("L_ClsCln",paramArray));/*alert("close and clean" + document.select.action);*****/
window.open("cleansession.jsp?event=multilookup","","toolbar=no,resizable=no,scrollbars=no,menubar=no,status=yes,width=0,height=0 top=-100,left=-100 0,");
//windowname=window.open("Pcategory.jsp?from=lookup"+val,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100 0, ");
document.select.submit();
}
}
// -->
//window.onUnload=onClose


</script>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

function uncheckAll(formobj){
for(var i=0;i<formobj.set.length;i++)
{
  formobj.set[i].checked=false;
}
}
function checkAll(formobj){
for(var i=0;i<formobj.set.length;i++)
{
  formobj.set[i].checked=true;
}
}


function setParent(formobj)
{
 var count=formobj.count.value;
 var counter=0;
 maxSelect=formobj.maxSelect.value;
 if (maxSelect.length>0)
  maxSelect=parseInt(maxSelect);
  else maxSelect=0;
 maxSelect=formobj.maxSelect.value;
 columnStr=formobj.columnStr.value;
 var arrayofColumn=columnStr.split("[VELSEP]");
 var arrayofValue;
 var form=formobj.form.value;
 var seperator=formobj.seperator.value;
 var pForm=this.parent.opener.document.forms[form]; 
 
 if (count==1)
 {   
 prevSel=formobj.prevSel.value;
   if (formobj.set.checked)
   {
     arrayofValue=prevSel.split("[VELSEP]");
     
    for (var i=0;i<arrayofColumn.length;i++)
    {
     pForm.elements[arrayofColumn[i]].value=arrayofValue[i];
     pForm.elements[arrayofColumn[i]].focus();
    }
   }
   else
   {
    
    for (var i=0;i<arrayofColumn.length;i++)
    {
    	if (formobj.defaultvalue.value == '[ALL]'){
	    	pForm.elements[arrayofColumn[i]].value=formobj.defaultvalue.value;
    	} else {
			pForm.elements[arrayofColumn[i]].value='';
	    }
    }
   }
   
 }
 else if (count>1)
 { 
	// run a loop to restrict the selection by maxSelect
	
	if (maxSelect!=0) { // 0 means unlimited selection
		for (var i=0;i<count;i++)
		{
			if (formobj.set[i].checked) counter++;
		}
		if (counter>maxSelect)
		{
			var paramArray = [maxSelect,(counter-maxSelect)];
			alert(getLocalizedMessageString("M_MaxSelAlwd_DeselSel",paramArray))/*alert("Maximum of " + maxSelect + " selection(s) allowed.Please deselect " + (counter-maxSelect) + " selection(s).")*****/
			return false;
		}
	}

        //arrayofValue=new Array((formobj.prevSel[0].value.split("[VELSEP]")).length);
	var tempStr="";
	
	if (!typeof(pForm))		this.parent.close();
	 
	
	for (var j=0;j<arrayofColumn.length;j++)
	{
		tempStr="";
		
		for (var i=0;i<count;i++)
		{
			if (formobj.set[i].checked)
			{
				prevSel=formobj.prevSel[i].value;
				tempArray=prevSel.split("[VELSEP]");

				if (typeof(tempArray[j])!="undefined"){
					tempStr = (tempStr.length==0)? tempArray[j] : tempStr+seperator+tempArray[j]; // YK 12MAY2011 Bug#6209
				}
			} else {
				continue;
			}
		}

		if (tempStr.length>0){
		  	if (typeof(pForm.elements[arrayofColumn[j]])!="undefined"){
	      		pForm.elements[arrayofColumn[j]].value=tempStr;
		  	}
	    } else {
		    if (typeof(pForm.elements[arrayofColumn[j]])!="undefined"){
		    	//Bug #15404
		    	if (formobj.defaultvalue.value == '[ALL]'){
		    		pForm.elements[arrayofColumn[j]].value=formobj.defaultvalue.value;
		    	} else {
		    		pForm.elements[arrayofColumn[j]].value='';
			    }
		    }
	    }
	 }
	// this.parent.close();
 }//end if count==1
 else 
 {
	  if (formobj.defaultvalue.value.length>0) 
	  {
		 for (var j=0;j<arrayofColumn.length;j++)
			{ 
			  if (typeof(pForm.elements[arrayofColumn[j]])!="undefined")
			      pForm.elements[arrayofColumn[j]].value=formobj.defaultvalue.value;
			}
	 }
 }
 this.parent.close();
}

function checkValue(formobj,counter)
{
 count=formobj.count.value;
 if (count==1)
 {
  if (formobj.set.checked) 
    formobj.setValue.value="Y";
    else 
    formobj.setValue.value="N";
 }
 else 
 {
  if (formobj.set[counter].checked) 
    formobj.setValue[counter].value="Y";
    else 
    formobj.setValue[counter].value="N";
  
 }
 
}
 
	
</SCRIPT>

</head>

<%HttpSession tSession=request.getSession(true);
if (sessionmaint.isValidSession(tSession)) 
{
int count=0;

count = EJBUtil.stringToNum(request.getParameter("count"));


String oddRow="";
String checkString = "";
String[] tempArray,dispArray=null;
ArrayList urlFields=new ArrayList(),
          fieldDisp=new ArrayList();
ArrayList valueList=null,currSelList=new ArrayList();
ArrayList tempList = new ArrayList();
ArrayList currentValueList = new ArrayList();

//Get the newly selected values
String newSel= request.getParameter("newSel");
newSel=(newSel==null)?"":newSel;

//Get vales for maximum number of selections
String maxSelect= request.getParameter("maxselect");
maxSelect=(maxSelect==null)?"":maxSelect;

//get the header and process the header
String headerStr= request.getParameter("headerStr");
headerStr=(headerStr==null)?"":headerStr;

//get the header and process the column
String columnStr= request.getParameter("columnStr");
columnStr=(columnStr==null)?"":columnStr;

//This value stores the hide/unhide status of a column 
String displayStr= request.getParameter("displayStr");
displayStr=(displayStr==null)?"":displayStr;


//get the header and process the form variale
String form= request.getParameter("form");
form=(form==null)?"":form;

//get the seperator and process the form variale
String seperator= request.getParameter("seperator");
seperator=(seperator==null)?";":seperator;
if (seperator.length()==0) seperator=";";

int len=0;

String keyword=request.getParameter("keyword");
keyword=(keyword==null)?"":keyword;


// this variable verifies if the selection frame need to be updated
// from opener window. This should be active/blank when the window opens
//after first time,this should never be blank
String refresh=request.getParameter("refresh");
refresh=(refresh==null)?"":refresh;



String urlStr=request.getParameter("urlStr");
urlStr=(urlStr==null)?"":urlStr;


String defaultValue=request.getParameter("defaultvalue");
defaultValue=(defaultValue==null)?"":defaultValue;




System.out.println("URLSTR"+urlStr);
if (urlStr.length()>0){

valueList=new ArrayList(Arrays.asList(StringUtil.strSplit(urlStr,"[VELFIELDSEP]",false)));


String tempStr="";
//extract the values from urlStr
System.out.println("VaLUELIST"+valueList);
for (int i=0;i<valueList.size();i++)
{	
  tempStr=(String)valueList.get(i);
	if(!(tempStr.equalsIgnoreCase("[ALL]"))){
		tempStr=(tempStr==null)?"":tempStr;
		tempArray=StringUtil.strSplit(tempStr,"[VELSEP]",false);
		for (int j=0;j<tempArray.length;j++)
		{
			if (j+1>tempList.size()) {
				tempList.add(tempArray[j]);
			}
	   		else
				tempList.set(j,(String)tempList.get(j)+ "[VELSEP]" + tempArray[j]);
	   	}
	}
}

}
//end extraction 

//Process current selection from variable newSel
System.out.println(newSel.indexOf("[VELRECSEP]"));

	currSelList=new ArrayList(Arrays.asList(StringUtil.strSplit(newSel,"[VELRECSEP]",false)));

//end current selection processing

//extract the values from keyword String
if (columnStr.length()==0) 
{
if (keyword.length()>0)
{
StringTokenizer st = new StringTokenizer(keyword,"~");
StringTokenizer st_sep;
	     while (st.hasMoreTokens()) 
	     {
	      	 st_sep= new StringTokenizer(st.nextToken(),"|");
		 urlFields.add(st_sep.nextToken());
		 st_sep.nextToken();
		 // This token specifies if the column is hidden on the selection screen
		 if (st_sep.hasMoreTokens())
		 fieldDisp.add(st_sep.nextToken());
		 else 
		 fieldDisp.add("[VELSHOW]");
	     }
	String tempStr="";
	
	
	 
	String tmpValue="";
	for (int i=0;i<urlFields.size();i++)
	{
	  if (columnStr.length()==0)  columnStr=(String)urlFields.get(i);
	   else  columnStr=columnStr+"[VELSEP]" +(String)urlFields.get(i);
	   
	   tmpValue=(String)fieldDisp.get(i);
	   if  (displayStr.length()==0)
	   displayStr=tmpValue;
	  else
	  displayStr=displayStr+"[VELSEP]"+tmpValue;
	
	}
	
}
}

 String[] currSel=new String[0];
//	 StringUtil.strSplit(newSel,"[VELSEP]",false);
 String[] valueArray=request.getParameterValues("prevSel"); 
 String[] setValue=request.getParameterValues("setValue");
 
 //Parse the displayStr to enable hide/unhide of a selected row column
   
   if (displayStr.length()>0)
   {
    dispArray=StringUtil.strSplit(displayStr,"[VELSEP]",false);
   }
   

%>
<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1) 
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {	
%>
<body style="overflow:auto;" class="not-body">
<%
	} else {
%>
<body class="not-body">
<%
	}
%>

<form name="select" method="post" id="valSelFrm" onSubmit ="if (setParent(document.select)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
<input type="hidden" name="newSel">
<input type="hidden" name="headerStr" value="<%=headerStr%>">
<input type="hidden" name="columnStr" value="<%=columnStr%>">
<input type="hidden" name="displayStr" value="<%=displayStr%>">
<input type="hidden" name="form" value="<%=form%>">
<input type="hidden" name="seperator" value="<%=seperator%>">
<input type="hidden" name="urlStr" value="">
<input type="hidden" name="refresh" value="<%=refresh%>">
<input type="hidden" name="maxSelect" value="<%=maxSelect%>">
<input type="hidden" name="defaultvalue" value="<%=defaultValue%>">

<table>

<tr>
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="N"/>
		<jsp:param name="formID" value="valSelFrm"/>
		<jsp:param name="showDiscard" value="LKP"/>
</jsp:include>
</tr>

<%if (maxSelect.length()>0){ Object[] arguments = {maxSelect}; %>
<tr><td colspan=2><p><font color="red" size="1"><%=VelosResourceBundle.getMessageString("M_MaxSel_Allwd",arguments) %><%--Maximum <%=maxSelect%> selection(s) allowed.*****--%></font></p></td>
<%}%></tr>
</table>
<br>
<table width="100%" class="custom-table-no-border">
	<div id="links">
	</div>
	
    <!--<%//if (headerStr.length()>0){%>
    <tr class="popupHeader"> 
    <th width="5%"></th>
    <%//tempArray=StringUtil.strSplit(headerStr,"[VELSEP]",false);
    //for (int i=0;i<tempArray.length;i++){
    %>
    <th><%//=tempArray[i]%></th>
    <%//}%>
    </tr>
    <%//}%>-->
  
  
   
   
 
  <!-- Parse the Already selected values to display them  -->
  <%if (tempList.size()>0){
      System.out.println("TEMPLIST-"+tempList);
   for (int i=0;i<tempList.size();i++)
  {
	   if(!(tempList.get(i).toString().equalsIgnoreCase("[ALL]"))){
   tempArray=StringUtil.strSplit((String)tempList.get(i),"[VELSEP]",false);
   
  if ((i%2)==0) {
  oddRow="N";
  %>
    <!--<tr class="browserEvenRow">-->
    <tr class="browserEvenRow">
<%
	 		   }else{
			   oddRow="Y";

%>

      <tr class="browserOddRow">
      <%}%> 
<input type="hidden" name="prevSel" value="<%=StringUtil.escapeSpecialCharHTML((String)tempList.get(i))%>">
<td><input type="checkbox" name="set" checked onClick="checkValue(document.select,<%=i%>)"></td>
<input type="hidden" name="setValue" value="Y">
<%for (int j=0;j<tempArray.length;j++)
{
 if (dispArray!=null)
 {
 if (!(dispArray[j]).equals("[VELHIDE]"))
 {
%>

<td><%=tempArray[j]%></td>

<%}
  }
  else{%>
  <td><%=tempArray[j]%></td>

<%}}%>
</tr>
  
  <%}} //end for loop
  }//end if templist.size
  
  
    
if (valueArray!=null)
for (int i=0;i<valueArray.length;i++)
{
   tempArray=StringUtil.strSplit(valueArray[i],"[VELSEP]",false);
   currentValueList.add(StringUtil.arrayToString(tempArray,""));

 if ((i%2)==0) {
 oddRow="N";
%>

      <!--<tr class="browserEvenRow">-->
    <tr class="browserEvenRow">

<%

	 		   }else{
			   oddRow="Y";

%>

      <tr class="browserOddRow"><%}%> 
<input type="hidden" name="prevSel" value="<%=StringUtil.escapeSpecialCharHTML(valueArray[i])%>">
<%if (setValue[i].equals("Y")){%>
<td><input type="checkbox" name="set" checked onClick="checkValue(document.select,<%=i%>)"></td>
<input type="hidden" name="setValue" value="Y">
<%} else if (setValue[i].equals("N")){ %>
<td><input type="checkbox" name="set" onClick="checkValue(document.select,<%=i%>)"></td>
<input type="hidden" name="setValue" value="N">
<%}%>

<%for (int j=0;j<tempArray.length;j++){
if (dispArray!=null)
 {
 if (!(dispArray[j]).equals("[VELHIDE]"))
 {
	 
	
%>

<td><%=tempArray[j]%></td>

<%}
  }
  else{%>
  <td><%=tempArray[j]%></td>

<%}
}%>
</tr>
<%}


if (count <= 0)
{

if (valueArray!=null){
 count=valueArray.length+1+tempList.size();  
 }else if (currSelList.size()>0){
count=tempList.size()+currSelList.size();
  }else{
count=tempList.size();
  } 

}
%>







<%len=currSelList.size();
if (len>0){%>


<%}%>
<%for (int selCount=0;selCount<currSelList.size();selCount++)
{ 
	currSel=StringUtil.strSplit((String)currSelList.get(selCount),"[VELSEP]",false);
	 checkString = StringUtil.arrayToString(currSel,"");
	//Defect Fix #3997 BK 12/05/2010
    if(!currentValueList.contains(checkString)){
    	/*Adding to the pile of already checked or unchecked list of selections*/
    	int addToChkBoxArrayIndx = selCount+count-1;
		%>
		<%if (oddRow.equals("Y")){
			oddRow="N";
		%>
		<tr class="browserEvenRow">
		<% }else  if(oddRow.equals("N") || oddRow.equals("") ) {
			oddRow="Y";
		%>
	    <tr class="browserOddRow">
		<%}%>
<input type="hidden" name="prevSel" value="<%=StringUtil.escapeSpecialCharHTML((String)currSelList.get(selCount))%>">
<td><input type="checkbox" name="set" checked onClick="checkValue(document.select,<%=addToChkBoxArrayIndx%>)"></td>
<input type="hidden" name="setValue" value="Y">	
<%for(int k=0;k<currSel.length;k++)
 {
if (dispArray!=null)
 {
	System.out.println("SelCount"+selCount+"k="+k+" currSel.length="+currSel.length);
 if (!(dispArray[k]).equals("[VELHIDE]") )
 {
	  System.out.println(currSel[k]);	  
%>

<td><%=currSel[k]%></td>

<%}
  }
  else{%>
  <td><%=currSel[k]%></td>

<%}}}
	else{
		count--;
	}
		}%>
</tr>
<% //show message if selection is blank
if ((count==0) && (refresh.equals("N"))) {%>
<br><br>
<P class="defComments"><%=MC.M_NoRowsSelected%><%--No Rows Selected.*****--%></P>
<%}%>
<input type="hidden" name="count" value=<%=count%>>
</table>
<SCRIPT LANGUAGE="JavaScript" defer="defer">
//alert(this.parent.opener.document.forms[0].elements["data"].value);
if (this.document.forms[1].refresh.value!="N") 
{
var tempStr="";
var urlStr="";
var colValue="";
var columnStr= this.parent.frames["selectFrame"].document.forms[1].columnStr.value;
var arrayOfColumns=columnStr.split("[VELSEP]");
for (var count=0;count<arrayOfColumns.length;count++)
{
	if (typeof(this.parent.opener.document.forms[this.document.forms[1].form.value].elements[arrayOfColumns[count]])=="undefined")
    {
		alert("<%=MC.M_InvalidCol_VelosSuppWin%>");/*alert("Invalid column name. Please contact Velos customer support.\n Window will close.");*****/
		this.parent.close();
    }
     
  	colValue=this.parent.opener.document.forms[this.document.forms[1].form.value].elements[arrayOfColumns[count]].value;
	if (this.document.forms[1].defaultvalue.value.length>0)
	{
		//FIX #6790 added and clause
		//FIX #6305 Uncommented following line
		if (colValue==this.document.forms[1].defaultvalue.value 
				&& this.document.forms[1].defaultvalue.value == '[ALL]'){
    		continue;
		}
	}
	<%if(!form.equalsIgnoreCase("adverseEventScreenForm")){%>
	rExp=new RegExp(this.document.forms[1].seperator.value,"gi");
	colValue=colValue.replace(rExp,"[VELSEP]");
	<%}%>

    if (tempStr.length==0)
		tempStr=colValue;
	else
		tempStr=tempStr+"[VELFIELDSEP]"+colValue;
}
//if (tempStr.length>0)
//{
this.parent.frames["selectFrame"].document.forms[1].urlStr.value=tempStr;
this.document.forms[1].refresh.value="N";
this.parent.frames["selectFrame"].document.forms[1].submit();
//}

}
/*if (this.parent.document.forms[0].urlStr.value.length>0)
{
this.parent.frames[0].document.forms[0].urlStr.value=this.parent.document.forms[0].urlStr.value;
this.parent.document.forms[0].urlStr.value="";
}
if (this.parent.frames[0].document.forms[0].urlStr.value.length>0)
 this.parent.frames[0].document.forms[0].submit();*/
 
 
var count=this.parent.frames["selectFrame"].document.forms[1].count.value;
var mydiv = document.getElementById("links");
if (count>0){
   
//  mydiv.innerHTML='<tr><td><A href="#" onClick="uncheckAll(document.select)">Remove All</A>&nbsp;&nbsp;<A href="#" onClick="checkAll(document.select)">Select All</A></td> </tr>'

  }

</script>
</form>
<%
}else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}
%>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</html>
