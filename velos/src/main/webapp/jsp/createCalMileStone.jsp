<%--
* File Name 	: createCalMileStone.jsp 
* Created on	: 2/24/11
* Created By	: YogeshKumar/AshuKhatkar
* Enhancement	: 8_10.0DFIV12 
* Purpose		: Create Milestone from the Event-Visit Grid.
*
*
--%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%><%@page import="com.velos.eres.service.util.LC"%><%@page import="com.velos.eres.service.util.MC"%><%@page import="com.velos.eres.service.util.Configuration"%>
<%@page import="com.velos.esch.web.eventdef.EventdefJB,com.velos.esch.web.eventassoc.EventAssocJB"%>
<%@page import="java.util.ArrayList"%>


<%@page import="java.util.HashMap"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="com.velos.eres.business.common.CodeDao"%>
<%@page import="com.velos.esch.business.common.SchCodeDao"%><jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="MsB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>


<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	HttpSession tSession = request.getSession(true);
	JSONObject jsObj = new JSONObject();
	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn/*"User is not logged in."*****/);
		out.println(jsObj.toString());
		return;
	}
	
	String oldESign = (String) tSession.getAttribute("eSign");
	String eSign = request.getParameter("eSign");
	if (!oldESign.equals(eSign)) {
		jsObj.put("result", -4);
		if ("userpxd".equals(Configuration.eSignConf)) {
			jsObj.put("resultMsg", MC.M_EtrWrgPassword_Svg/*"You entered a wrong e-Password. Please try saving again. "*****/);
		}
		else
		{
		jsObj.put("resultMsg", MC.M_EtrWrgEsign_Svg/*"You entered a wrong e-signature. Please try saving again. "*****/);
		}
		out.println(jsObj.toString());
		return;
	}
	
	String ipAdd = (String) tSession.getAttribute("ipAdd");
    String userId = (String) tSession.getAttribute("userId");
	String[] ops= request.getParameterValues("ops");
	String[] visitSel= request.getParameterValues("visitSel");
 	String protId = request.getParameter("calProtocolId");
	String studyID = request.getParameter("studyId");
	
	if (ops == null || ops.length == 0) {
		jsObj.put("result", -2);
		jsObj.put("resultMsg", MC.M_No_OperPerformed/*"No operations were performed"*****/);
		out.println(jsObj.toString());
		return;
	}
	int ret = 0;
	int count=1;
	 try {
    	HashMap visitMap= new HashMap();
    	HashMap visiteventMap= new HashMap();
    	CodeDao cdStat = new CodeDao();
    	SchCodeDao schCode = new SchCodeDao();
		int mileStoneStat = cdStat.getCodeId("milestone_stat","WIP"); // Work in Progress
		int mileStonePayType= cdStat.getCodeId("milepaytype","rec"); // Receivable 	
		int eventMileStoneRule= cdStat.getCodeId("EM","em_2"); //Event is marked as 
		int visitMileStoneRule= cdStat.getCodeId("VM","vm_2"); // All events within the visit are  marked as -
		int visitMileStoneRule2= cdStat.getCodeId("VM","vm_3"); // At least one event is marked as - 
		int eventStatus= schCode.getCodeId("eventstatus","ev_done"); // Done
	
    		for (int iX=0; iX<ops.length; iX++) {
    			String[] part = ops[iX].split("\\|");
			if (part == null || part.length < 4) { continue; }
			String vId = part[1].substring(part[1].indexOf("v")+1);
			
			if(part[1].indexOf("e")>-1){
				
				String eId = part[1].substring(1,part[1].indexOf("v"));
				if(visiteventMap.containsKey(vId)){ /* YK 16MAR Bug #5919*/
				visiteventMap.put(vId,""+visitMileStoneRule);
				}else {
					visiteventMap.put(vId,""+visitMileStoneRule2);
				}
				//Added here for EM
				MsB.setMilestoneType("EM");
				MsB.setMilestoneStudyId(studyID);				
				MsB.setMilestoneCalId(protId);			
				MsB.setMilestoneRuleId(""+eventMileStoneRule);	
				MsB.setMilestoneAmount("0");
				MsB.setMilestoneDelFlag("N");	
				MsB.setMilestoneEvent(eId);
				MsB.setCreator(userId);
			    MsB.setIpAdd(ipAdd);
				MsB.setMilestoneStatFK(""+mileStoneStat);
			    MsB.setMilestonePayType(""+mileStonePayType) ; 
			    MsB.setMilestoneEventStatus(""+eventStatus);
			    MsB.setMilestoneVisitFK(vId);
			    MsB.setMilestoneCount("-1"); //AK:Fixed Bug#5971 30thMar11
				ret = MsB.setMilestoneDetails();	
				}
			
		}
    		 /* YK 16MAR Bug #5919*/
    		for(int iY=0;iY<visitSel.length;iY++)
    		{
    			if(!visitSel[iY].equalsIgnoreCase("N"))
    			{
    				String vId = visitSel[iY];
    				 if(visiteventMap.size()>0)
				      {
    				 Set visiteventList= visiteventMap.entrySet();
    				 Iterator itr = visiteventList.iterator();
    				 while(itr.hasNext()){
    				      Map.Entry mapList = (Map.Entry)itr.next();
    				      String visit=mapList.getKey().toString();
    				      String rule= mapList.getValue().toString();
    				   	  if(vId.equalsIgnoreCase(visit)){ /*YK 28APR2011 Bug#6146*/
    				      visitMap.put(visit,""+rule);
    				   	  }
    					 }
    				}
    				if(!visitMap.containsKey(vId)) 
    				{
    					visitMap.put(vId,""+visitMileStoneRule2);
    				}
				     
    				
    			}
    		}
    		 /* YK 16MAR Bug #5919*/
    		 Set keyList= visitMap.entrySet();
			 Iterator i = keyList.iterator();
			//Added here for VM
			    while(i.hasNext()){
			      Map.Entry mapList = (Map.Entry)i.next();
			      String vId=mapList.getKey().toString();
			      String rule= mapList.getValue().toString();
				  	MsB.setMilestoneType("VM");
					MsB.setMilestoneStudyId(studyID);				
					MsB.setMilestoneCalId(protId);			
					MsB.setMilestoneRuleId(rule);	
					MsB.setMilestoneAmount("0");
					MsB.setMilestoneDelFlag("N");	
					MsB.setMilestoneEvent(""); //YK 29Mar2011: BUG#5984
					MsB.setCreator(userId);
				    MsB.setIpAdd(ipAdd);
					MsB.setMilestoneStatFK(""+mileStoneStat);
				    MsB.setMilestonePayType(""+mileStonePayType) ; 
				    MsB.setMilestoneEventStatus(""+eventStatus);
				    MsB.setMilestoneVisitFK(vId);
				    MsB.setMilestoneCount("-1"); //AK:Fixed Bug#5971 30thMar11
					ret = MsB.setMilestoneDetails();
			    }
			
	} catch(Exception e) {
		jsObj.put("result", -3);
		jsObj.put("resultMsg", e);
		out.println(jsObj.toString());
		return;
	}
    
    jsObj.put("result", 0);
    jsObj.put("resultMsg",MC.M_MstonesCreated_Succ+"<br>"+MC.M_ClkMstone_CompleteDets);//FIN-20635 Enhancement : Akshi
	out.println(jsObj.toString());
%>
