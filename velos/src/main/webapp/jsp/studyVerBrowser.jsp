<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.services.map.ObjectMap, com.velos.services.map.ObjectMapService"%>
<%@page import="com.velos.services.util.ServicesUtil" %>
<%@page import="com.velos.eres.web.submission.SubmissionStatusJB" %>
<%@page import="com.velos.eres.widget.business.common.UIFlxPageDao"%>
<%@page import="com.velos.eres.business.common.StudyVerDao"%>
<%@page import="com.velos.eres.business.common.StudyDao"%>
<jsp:useBean id="complianceJB" scope="request" class="com.velos.eres.compliance.web.ComplianceJB"/>
<jsp:useBean id="studyJB" scope="session" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="statHistoryB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<%HttpServletResponse httpResponse = (HttpServletResponse) response;
httpResponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1
httpResponse.setHeader("Pragma", "no-cache"); // HTTP 1.0
httpResponse.setDateHeader("Expires", 0); %>
<html>
<head>

<%!
static final String STR_TRUE = "true", STR_FALSE = "false";
%>
<% 
String studyOID = null;
String isPopupWin = "0";
String mode= request.getParameter("mode")==null?"":request.getParameter("mode");
if(request.getParameter("isPopupWin")!=null){
	isPopupWin = request.getParameter("isPopupWin");
	System.out.println("isPopupWin---->"+isPopupWin);
}

boolean isIrb = "irb_upload_tab".equals(request.getParameter("selectedTab")) ? true : false;
String includeTabsJsp = isIrb ? ("LIND".equals(CFG.EIRB_MODE) ? "ecompNewTabs.jsp" : "irbnewtabs.jsp") : "studytabs.jsp";
String jspName = isIrb
	? (("LIND".equals(CFG.EIRB_MODE)) ? "studyAttachmentsLindFt" : "studyVerBrowser.jsp")
	: (("LIND".equals(CFG.EIRB_MODE)) ? "studyAttachments" : "studyVerBrowser.jsp");

%>
<title><%=LC.L_Std_Ver%><%--<%=LC.L_Study%> >> Versions*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<% String src;
src= request.getParameter("srcmenu");
%>
<%if (!"LIND".equals(CFG.EIRB_MODE) || isPopupWin.equals("1")){%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="isPopupWin" value="<%=isPopupWin%>"/>
</jsp:include>
<%}
String calledFrom=request.getParameter("calledFrom")==null?"":request.getParameter("calledFrom");
%>
<SCRIPT language="javascript">
var windowName;
var screenWidth = screen.width;
var screenHeight = screen.height;
$j(document).ready(function() {
	var tbodyHeight = (screenHeight > 768)? 200 : 150;
	if($j(document).find("#versionsTbl > tbody").hasClass("NoScrollCont")){
		if(screenWidth > 1280 && !($j(document).find("#versionsTbl > tbody").hasClass("isPopupWin"))){
			$j("tbody.NoScrollCont").css({"width":"1160px"});
		}
		if(screenWidth <= 1280 && !($j(document).find("#versionsTbl > tbody").hasClass("isPopupWin"))){
			$j("tbody.NoScrollCont").css({"width":"1080px"});
		}
	}else{
		$j(document).find("#versionsTbl > tbody").addClass("scrollContent");
		$j("tbody.scrollContent").css({"height":""+tbodyHeight});
		$j("tbody.scrollContent").css({"overflow-y":"auto"});
		$j("tbody.scrollContent").css({"overflow-x":"hidden"});
		if(screenWidth > 1280 && !($j(document).find("#versionsTbl > tbody").hasClass("isPopupWin"))){
			$j("tbody.scrollContent").css({"width":"100%"});
		}
		if(screenWidth <= 1280 && !($j(document).find("#versionsTbl > tbody").hasClass("isPopupWin"))){
			$j("tbody.scrollContent").css({"width":"100%"});
		}
	}
	/* if(screenWidth > 1280 && !($j(document).find("#versionsTbl > tbody").hasClass("isPopupWin"))){
		$j(document).find("#studyverbrowserSearh").css({"width":"1160px"});
	}
	if(screenWidth <= 1280 && !($j(document).find("#versionsTbl > tbody").hasClass("isPopupWin"))){
		$j(document).find("#studyverbrowserSearh").css({"width":"1080px"});
	} */
	if(screenWidth > 1280){
		$j("#dVersionStatus").css({"width":"200px"});
		$j("#dStudyvertype").css({"width":"200px"});
		$j("#dStudyvercat").css({"width":"200px"});
	}else{
		$j("#dVersionStatus").css({"width":"180px"});
		$j("#dStudyvertype").css({"width":"180px"});
		$j("#dStudyvercat").css({"width":"180px"});
	}
	if("LIND"=="<%=CFG.EIRB_MODE%>" && "N"=="<%=CFG. Workflows_Enabled%>" && "eComp"=="<%=calledFrom%>"){				
		$j("#versionSearchTbl").css({marginLeft:"-15%",width:"115%"});
		$j("#versionsTbl").css({marginLeft:"-15%",width:"115%"});
	}
});

var popCatNewLink = function popCatNewLink(oid) {
	window.open("<%=CFG.EIRB_CAT_NEW_LINK%>"+oid);
};

var popCatViewLink = function (oid) {
	window.open("<%=CFG.EIRB_CAT_VIEW_LINK%>"+oid);
};

function setOrder(formobj,orderBy)
{

	if (formobj.orderType.value=="desc") {
		formobj.orderType.value= "asc";
	} else 	if (formobj.orderType.value=="asc") {
		formobj.orderType.value= "desc";
	}

	formobj.orderBy.value= orderBy;


	formobj.submit();
}

function closeChildWindow() {
	if (windowName != null) {
		windowName.close();
	}
}

function confirmBox(ver,pgRight,num,category) {
	if (windowName != null) {
		windowName.close();
	}

	if (f_check_perm(pgRight,'E') == true) {
		if (num == 1)
		{
			//Added by Ganapathy on 04-15-05
			//alert("The default version can not be deleted.");
			alert("<%=MC.M_ThisVerStd_YouNotDel%>");/*alert("This is the only version left for this <%=LC.Std_Study_Lower%>. You can not delete this version.");*****/
			return false;
		}
		var paramArray = null;
		if (category) {
			paramArray = [category];
			msg = "Delete "+category+" category?";
		} else {
			paramArray = [ver];
			msg=getLocalizedMessageString("L_Del_StdVer",paramArray);/*msg="Delete <%=LC.L_Study%> Version " + ver+ "?";*****/
		}
		if (confirm(msg))
		{
    		return true;
		}
		else
		{
			return false;
		}
	} else {
		return false;
	}
}


function openWin(pgRight,mode,src,tab,studyVerId)
{	if (f_check_perm(pgRight,'E') == true) {
		windowName= window.open("studyver.jsp?mode=" + mode + "&srcmenu=" + src + "&selectedTab=" + tab + "&studyVerId=" +  studyVerId,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=500,height=300");
		windowName.focus();
	}
}
function openNewVersionWin(pgRight,mode,src,tab,studyId)
{	if (f_check_perm(pgRight,'N') == true) {
		windowName= window.open("studyver.jsp?mode=" + mode + "&srcmenu=" + src + "&selectedTab=" + tab + "&studyId=" +  studyId,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=500,height=300");
		windowName.focus();
	}
}

function openNewMultiVersionWin(pgRight,pageRightApndx,mode,src,tab,studyId)
{	if (f_check_perm(pgRight,'N') == true && f_check_perm(pageRightApndx,'N') == true) {
		windowName= window.open("appendix_file_multi.jsp?mode=" + mode + "&srcmenu=" + src + "&selectedTab=" + tab + "&studyId=" +  studyId,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=1150,height=400");
		windowName.focus();
	}
}

function openWinStatus(pgRight,mode,studyVerId,verNumber,statusId,studyId,fullVerNo)
{
	var checkType;

	if (mode=='N')
		checkType = 'N';
	else
		checkType = 'E';

	var otherParam;
	<% String sectionHeading = isIrb ? LC.L_Status_Dets/*"Status Details"*****/ : MC.M_MngPcolVer_StatDets/*"Manage Protocols >> Versions >> Status Details"*****/; %>
	otherParam = "&moduleTable=er_studyver&statusCodeType=versionStatus&sectionHeading=<%=sectionHeading%>&statusId=" + statusId;
		  if (f_check_perm(pgRight,checkType) == true) {
		//JM: 031006: if condition changed
		if (verNumber!='')
		windowName= window.open("editstatus.jsp?mode=" + mode + "&modulePk=" +  studyVerId + "&verNumber=" + verNumber + "&studyId=" + studyId + "&fullVerNo=" + fullVerNo + otherParam,"statusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=500,height=300");
		else
		windowName= window.open("editstatus.jsp?mode=" + mode + "&modulePk=" +  studyVerId + "&studyId=" + studyId + "&fullVerNo=" + fullVerNo + otherParam,"statusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=500,height=300");
		windowName.focus();
	}
}

</SCRIPT>
<body>

<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<%
	String from = "version";
	String tab = request.getParameter("selectedTab");

	String studyIdForTabs = "";
 	studyIdForTabs = request.getParameter("studyId");
	if (studyIdForTabs == null) {
 		if (request.getSession(false) != null) {
 			studyIdForTabs = String.valueOf(request.getSession(false).getAttribute("studyId"));
 		}
 	}
 	
%>
<% if (!"LIND".equals(CFG.EIRB_MODE)) {%>
<DIV class="BrowserTopn" id = "divtab">
<jsp:include page="<%=includeTabsJsp%>" flush="true">
				<jsp:param name="from" value="<%=from%>"/>
				<jsp:param name="selectedTab" value="<%=tab%>"/>
				<jsp:param name="studyId" value="<%=studyIdForTabs%>"/>
</jsp:include>
</DIV>
<%}%>
<%
HttpSession tSession = request.getSession(true);


%>
<%
String stid= request.getParameter("studyId");
System.out.println("stid:::"+stid);
if("LIND".equals(CFG.EIRB_MODE) && !"N".equals(request.getParameter("mode"))){%>

<% } else if(stid==null || stid=="" || "0".equals(stid)) {%>
<SCRIPT LANGUAGE="JavaScript">
if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="BrowserBotN BrowserBotN_RC_1" id="div1"  style="height:75%;position:relative;top:60px;border:0">')            //Done by Siddharth for #20709 bug
	else
		document.write('<DIV class="BrowserBotN BrowserBotN_RC_1" id="div1">')
</SCRIPT>
	<%} else {%>
<SCRIPT LANGUAGE="JavaScript">
/* Condition to check configuration for workflow enabled */
<% if("Y".equals(CFG.Workflows_Enabled)&& (stid!=null && !stid.equals("") && !stid.equals("0")) && !"LIND".equals(CFG.EIRB_MODE)){ %>
		if(screenWidth>1280 || screenHeight>1024)
			document.write('<DIV class="BrowserBotN BrowserBotN_S_3 workflowDivBig" id="div1" style="height:75%;position:relative;top:75px;border:0;">')			//Done by Siddharth for #20709 bug
		else
			document.write('<DIV class="BrowserBotN BrowserBotN_S_3 workflowDivSmall" id="div1" style="height:75%;" >')
<% } else{ %>
		if(screenWidth>1280 || screenHeight>1024)
			document.write('<DIV class="BrowserBotN BrowserBotN_S_3" id="div1" style="height:75%;position:relative;top:60px;border:0;">')			//Done by Siddharth for #20709 bug
		else
			document.write('<DIV class="BrowserBotN BrowserBotN_S_3" id="div1" style="height:75%;">')

<% } %>
	</SCRIPT>
<%}%>
	<jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="appendixB" scope="request" class="com.velos.eres.web.appendix.AppendixJB"/>
	<%@ page language = "java" import = "com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
	<%

	if (sessionmaint.isValidSession(tSession))
	{
		String studyId ="";
		if(isPopupWin.equals("1")){
			studyId = request.getParameter("studyId");
		}else{
			studyId = (String) tSession.getValue("studyId");
		}
		
		
		String usrId ;
		usrId = (String) tSession.getValue("userId");

		String uName = (String) tSession.getValue("userName");
		String defUserGroup = (String) tSession.getAttribute("defUserGroup");

		double pageRight = 0;
		int pageRightApndx = 0;

        if(studyId == "" || studyId == null || "0".equals(studyId.trim()) || studyId.equalsIgnoreCase("")) {
	%>
	  <jsp:include page="studyDoesNotExist.jsp" flush="true"/>
	  <%
	  } else {
			UIFlxPageDao uiFlxPage = new UIFlxPageDao();
		  	ArrayList<String> lockedVersions = uiFlxPage.getAllLockedStudyVersions(StringUtil.stringToNum(studyId));
		  	for (String version: lockedVersions){
		  		System.out.println("Locked Version::::"+version);
		  	}
	    	StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
			if ((stdRights.getFtrRights().size()) == 0){
		 		pageRight= 0;
			}else{
				pageRight = Double.parseDouble(stdRights.getFtrRightsByValue("STUDYVER"));
				pageRightApndx = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYAPNDX"));
	   		}
		// change by salil  moving the closing bracket of else
	     ///change by salil on 15-sep-2003

		// int viewVer=0;
		 //int  secRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSEC"));
		// int  apndxRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYAPNDX"));

		// if(secRight >=4 && apndxRight >=4){ viewVer = 4;}

	   if ( pageRight > 0 )
		{
		    // Get or create OID of study to be passed to CAT 
			ObjectMapService objectMapService = ServicesUtil.getObjectMapService();
			if("LIND".equals(CFG.EIRB_MODE)) {
				ObjectMap objectMap = objectMapService.getOrCreateObjectMapFromPK("er_study", StringUtil.stringToInteger(studyIdForTabs));
				//System.out.println("Study OID="+objectMap.getOID());
				studyOID = objectMap.getOID();
		    }


		//JM: 11/17/2005
			String orderType = "";
			orderType = request.getParameter("orderType");
			//if (orderType==null) orderType="";

			if (EJBUtil.isEmpty(orderType))
			{
			orderType = "desc";
			}

			String orderBy = "";
			orderBy = request.getParameter("orderBy");
			if (orderBy==null)		orderBy="";
			if (EJBUtil.isEmpty(orderBy))
			orderBy = "PK_STUDYVER";






			String ddStudyvercat = "" ;
			CodeDao cd1=new CodeDao();
			cd1.getCodeValues("studyvercat");
			cd1.setCType("studyvercat");
	        cd1.setForGroup(defUserGroup);


			String studyvercat=request.getParameter("dStudyvercat");
			if (studyvercat==null) studyvercat="";


			if (studyvercat.equals("")){
			ddStudyvercat=cd1.toPullDown("dStudyvercat");
			}
			else{
			ddStudyvercat=cd1.toPullDown("dStudyvercat",EJBUtil.stringToNum(studyvercat),true);
			}

			String ddStudyvertype = "" ;
			CodeDao cd2=new CodeDao();
			cd2.getCodeValues("studyvertype");

			cd2.setCType("studyvertype");
	        cd2.setForGroup(defUserGroup);

			String studyvertype=request.getParameter("dStudyvertype");
			if (studyvertype==null) studyvertype="";

			if (studyvertype.equals("")){
			ddStudyvertype=cd2.toPullDown("dStudyvertype");
			}
			else{
			ddStudyvertype=cd2.toPullDown("dStudyvertype",EJBUtil.stringToNum(studyvertype),true);
			}


			String ddVersionStatus = "" ;
			CodeDao cd3=new CodeDao();
			cd3.getCodeValues("versionStatus");
			cd3.setCType("versionStatus");
	        cd3.setForGroup(defUserGroup);


			String versionStatus=request.getParameter("dVersionStatus");
			if (versionStatus==null) versionStatus="";

			if (versionStatus.equals("")){
			ddVersionStatus=cd3.toPullDown("dVersionStatus");
			}
			else{
			ddVersionStatus=cd3.toPullDown("dVersionStatus",EJBUtil.stringToNum(versionStatus),true);
			}

			String	dStudyvercat =	request.getParameter("dStudyvercat");
			String dStudyvertype =  request.getParameter("dStudyvertype");
			String dVersionStatus = request.getParameter("dVersionStatus");







			String studyVerNumber = request.getParameter("studyVerNumber");
			if (studyVerNumber==null) studyVerNumber="";

			String verCat = request.getParameter("dStudyvercat");
			if (verCat==null) verCat="";
			String verType = request.getParameter("dStudyvertype");
			if (verType==null) verType="";

			String verStat = request.getParameter("dVersionStatus");
			if (verStat==null) verStat="";


	%>
	<%if ("LIND".equals(CFG.EIRB_MODE) && !isPopupWin.equals("1")){ %>
		<Form name="studyverbrowserSearh" id="studyverbrowserSearh" method="post" action="<%=jspName%>?selectedTab=irb_upload_tab&mode=M&srcmenu=tdmenubaritem3&studyId=<%=studyId%>" onsubmit="">
	<%} else { %>
		<Form name="studyverbrowserSearh" id="studyverbrowserSearh" method="post" action="studyVerBrowser.jsp?studyId=<%=studyId%>" onsubmit="">
		<%if(isPopupWin.equals("1")){ %>
					<Input type="hidden" name="mode" value="N">
					<input type="hidden" name="isPopupWin" value=<%=isPopupWin %>>
					<input type="hidden" name="studyId" value=<%=studyIdForTabs %>>
				<%}%>
	<%} %>
				<Input type="hidden" name="selectedTab" value="<%=tab%>">

<div class="tmpHeight">
<%if(isPopupWin.equals("1")){
	String studyNumber=null;
    String studyTitle=null;
    String versionNum = "0";
    
    if(!studyId.equals("")){
    	studyJB.setId(EJBUtil.stringToNum(studyId));
    	studyJB.getStudyDetails();
		studyNumber = studyJB.getStudyNumber();
		studyTitle = studyJB.getStudyTitle();
		UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
		uiFlxPageDao.getHighestFlexPageFullVersion("er_study", Integer.parseInt(studyId));

		int versionMajor = uiFlxPageDao.getMajorVer();
		versionNum = String.valueOf(versionMajor);
		if (versionMajor > 0){
			versionNum += ".";
			int versionMinor = uiFlxPageDao.getMinorVer();
			versionNum += (versionMinor == 0)? "00" : ((versionMinor > 9)? versionMinor : "0" + versionMinor);
		}
    }
%>
<script language=javascript>
		var study_Title = htmlEncode('<%=studyTitle%>');
</script>
<h5><b>
	<%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>:<%=studyNumber %>&nbsp;&nbsp;&nbsp;&nbsp;
	<%=LC.L_Version%><%--Version*****--%>:<%=versionNum %>&nbsp;&nbsp;&nbsp;&nbsp;
	<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>:<img src="./images/View.gif" onmouseover="return overlib(study_Title,CAPTION,'<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>' ,RIGHT , BELOW );" onmouseout="return nd();" > &nbsp;&nbsp;&nbsp;&nbsp;
</b></h5>
<%} %>
</div><br>
<table id="versionSearchTbl" border="0" cellspacing="0" cellpadding="0" class="basetbl outline midAlign">
	<tr height="20">
	<td colspan="9"> <b> <%=LC.L_Search_By%><%--Search By*****--%></b></td>
	</tr>
	<tr >
	<!--Mukul: To fix Bug 4274  -->
	<td >  <%=LC.L_Version%><%--Version*****--%> &#35; : </td>
	<td >	<% if (studyVerNumber.equals("")) {%>
					<Input type=text name="studyVerNumber" size=5 value="">
					<%}else{%>
					<Input type=text name="studyVerNumber" size=5 value="<%=studyVerNumber%>">
					<%}%>
	</td>
	<td align="right"> <%=LC.L_Category%><%--Category*****--%>:&nbsp;   </td>
	<td >     <%=ddStudyvercat%>  </td>

	<td  align="right"> <%=LC.L_Type%><%--Type*****--%>:&nbsp; </td>
	<td  >          <%=ddStudyvertype%>
	</td>
	<%if ("LIND".equals(CFG.EIRB_MODE)){ %>
		<td  align="right"> <%=LC.L_Attachment_Status%><%--Status*****--%>:&nbsp;</td>
	<% } else { %>
		<td  align="right"> <%=LC.L_Status%><%--Status*****--%>:&nbsp;</td>
	<% } %>
	<td  >  <%=ddVersionStatus%></td>
	<td valign="bottom">
					<button type="submit"><%=LC.L_Search%></button>
	      			</td>

				</tr>
				</table>

				</form>


	<%
//+++++++++++++++++++++++++++++++++++++JM pagination+++++++++++++++++++++++++++++++++++++++++++++//


		int curPage = 0;
		long startPage = 1;
		String stPage;
		long cntr = 0;
		String pagenum  = request.getParameter("page");


		if (pagenum == null)
		{
			pagenum = "1";
		}

		curPage = EJBUtil.stringToNum(pagenum);

		String count1 = "";
		String count2 = "";
		String countSql = "";
		String formSql = "";


		String sWhere = "";
        String vcWhere = "";
        String vtWhere = "";
        String vstWhere = "";
        String obWhere = "";
        String otWhere = "";


        StringBuffer sqlBuffer = new StringBuffer();
       // int intStudyId =EJBUtil.stringToNum(studyId);





	 	String mysqlSelect = "SELECT PK_STUDYVER,"
                    + " (select count(*) from er_studysec where fk_studyver = pk_studyver ) secCount, "
                    + " (select count(*) from er_studyapndx where fk_studyver = pk_studyver) apndxCount, "
                    + " FK_STUDY,"
                    + " STUDYVER_NUMBER,studyver_date,TO_CHAR(STUDYVER_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) as STUDYVER_DATE_STR,"
                    + " (select codelst_desc from er_codelst where pk_codelst=a.STUDYVER_CATEGORY) as STUDYVER_CATEGORY,"
                    + " (select codelst_desc from er_codelst where pk_codelst=a.STUDYVER_TYPE) as STUDYVER_TYPE,"
                    + " STUDYVER_NOTES,"
                    + " CREATOR,"
                    + " IP_ADD, CODELST_DESC STATUS_CODEDESC, CODELST_SUBTYP STATUS_CODESUBTYP,PK_STATUS "
                    + " FROM ER_STUDYVER a , ( Select CODELST_DESC, CODELST_SUBTYP, PK_STATUS,STATUS_MODPK, FK_CODELST_STAT  from "
                    + " er_studyver i, ER_STATUS_HISTORY, er_codelst c Where i.fk_study = "+EJBUtil.stringToNum(studyId)
                    + " and i.PK_STUDYVER =  STATUS_MODPK and STATUS_MODTABLE =  'er_studyver'  "
                    + " and STATUS_END_DATE is null and c.pk_codelst = FK_CODELST_STAT ) stat "
                    + " WHERE FK_STUDY = " + EJBUtil.stringToNum(studyId)
                    + " and STATUS_MODPK (+) = pk_studyver "
                    + " and STUDYVER_NUMBER <> '0.00'";


				if ((studyVerNumber!= null) && (! studyVerNumber.trim().equals("") && ! studyVerNumber.trim().equals("null")))
				{
					sWhere = " and upper(a.STUDYVER_NUMBER) like upper('%";
					sWhere+=studyVerNumber.trim();
					sWhere+="%')";
				}

				vcWhere = " and a.STUDYVER_CATEGORY not in (select fk_codelst from er_codelst_hide where fk_grp = " +defUserGroup +" and codelst_type='studyvercat' and codelst_hide_table  = 1)";

				if ((verCat!= null) && (! verCat.trim().equals("") && ! verCat.trim().equals("null")))
				{
					vcWhere = vcWhere  + " and upper(a.STUDYVER_CATEGORY) = upper(";
					vcWhere+=verCat;
					vcWhere+=")";
				}



				vtWhere = " and ( a.STUDYVER_TYPE is null or a.STUDYVER_TYPE not in (select fk_codelst from er_codelst_hide where fk_grp = " +defUserGroup +" and codelst_type='studyvertype' and codelst_hide_table  = 1) ) ";

				if ((verType!= null) && (! verType.trim().equals("") && ! verType.trim().equals("null")))
				{
					vtWhere = vtWhere + " and a.STUDYVER_TYPE = UPPER(" ;
					vtWhere+=verType;
					vtWhere+=")";
				}

				vstWhere = " and FK_CODELST_STAT not in (select fk_codelst from er_codelst_hide where fk_grp = " +defUserGroup +" and codelst_type='versionStatus' and codelst_hide_table  = 1)";

				if ((verStat!= null) && (! verStat.trim().equals("") && ! verStat.trim().equals("null")))
				{
					vstWhere = vstWhere + " and FK_CODELST_STAT = UPPER(" ;
					vstWhere+=verStat;
					vstWhere+=")";
				}
				if ((orderBy!= null) && (! orderBy.trim().equals("") && ! orderBy.trim().equals("null")))
				{
					obWhere = " order by " +orderBy+ " " + orderType;

				}
			//	else{
			//
			//		obWhere = " order by PK_STUDYVER DESC ";
			//	}
				
				System.out.println("mysqlSelect--->"+mysqlSelect);

				sqlBuffer.append(mysqlSelect);

	            if(sWhere!=null){
	            	sqlBuffer.append(sWhere);
	            }
	            if(vcWhere !=null){
	            	sqlBuffer.append(vcWhere );
	            }
	            if(vtWhere!=null){
	            	sqlBuffer.append(vtWhere);
	            }
	            if(vstWhere !=null){
	            	sqlBuffer.append(vstWhere );
	            }
	            if(orderBy!=null){
	            	sqlBuffer.append(obWhere);
	            }


	            formSql=sqlBuffer.toString();
	            //out.println(formSql);
				System.out.println("formSql--->"+formSql);

				count1 = "select count(*) from  ( " ;
				count2 = ")"  ;
				countSql = count1 + formSql + count2 ;

				long rowsPerPage=0;
				long totalPages=0;
				long rowsReturned = 0;
				long showPages = 0;

			    boolean hasMore = false;
			    boolean hasPrevious = false;
			    long firstRec = 0;
			    long lastRec = 0;
			    long totalRows = 0;
			    long actualRec=0;

			    String type ="" ;
			    String type1="";

			    rowsPerPage =  Configuration.MOREBROWSERROWS ;
			    totalPages =Configuration.PAGEPERBROWSER ;



		        BrowserRows br = new BrowserRows();
		        br.getPageRows(curPage,rowsPerPage,formSql ,totalPages,countSql,orderBy,orderType);
		   	    rowsReturned = br.getRowReturned();
			    showPages = br.getShowPages();
			    startPage = br.getStartPage();
			    hasMore = br.getHasMore();
			    hasPrevious = br.getHasPrevious();
			    totalRows = br.getTotalRows();
			    firstRec = br.getFirstRec();
			    lastRec = br.getLastRec();

				String verStatus = null;
				String popCrfString = "";
				AppendixDao appendixDao =new AppendixDao();
				AppendixDao appendixDao1 =new AppendixDao();
				int counter = 0;
//+++++++++++++++++++++++++++++++++++++JM pagination+++++++++++++++++++++++++++++++++++++++++++++//


			%>


<div class="tmpHeight"></div>
			<table width="100%" cellspacing="0" cellpadding="0" border=0 > 
			<tr >
			<td>&nbsp;</td>
				<td width = "30%" class="lhsFont">
					<P class = "sectionheadings"> <%=MC.M_AssocVer_DocuList%><%--Associated Versions/Documents Listed Below*****--%></P>
			    </td>

			    <% if(!"LIND".equals(CFG.EIRB_MODE)) { %>
			    <td align="center" width="20%">
					<A href=# onclick="openNewVersionWin(<%=pageRight%>,'N','<%=src%>','<%=tab%>','<%=studyId%>')"><%=LC.L_Add_NewVersion_Upper%><%--ADD NEW VERSION*****--%></A>
			    </td>
			    <% } %>
                <td width="50%">&nbsp;
                <% if("LIND".equals(CFG.EIRB_MODE)) { %>
					<% if (!"TBD".equals(CFG.EIRB_CAT_NEW_LINK)) { %>
					<A href=# onclick="popCatNewLink('<%=studyOID%>')" ><%=LC.L_EIRB_CAT_NEW %><%--Create New Informed Consent Document*****--%></A>&nbsp;&nbsp;
					<% }  %>
					<% if (!"TBD".equals(CFG.EIRB_CAT_VIEW_LINK)) { %>
					<A href=# onclick="popCatViewLink('<%=studyOID%>')" ><%=LC.L_EIRB_CAT_VIEW %><%--View Informed Consent Documents*****--%></A>&nbsp;&nbsp;
					<% }  %>
				<%} %>
<% 
if ("LIND".equals(CFG.EIRB_MODE)){ 
int accId = StringUtil.stringToNum((String)tSession.getAttribute("accountId"));
HashMap<String, Object> paramMap = new HashMap<String, Object>();
paramMap.put("userId", (usrId.equals(null) ? null : Integer.valueOf(usrId)));
paramMap.put("accountId", accId);
paramMap.put("studyId", (studyId.equals(null) ? null : Integer.valueOf(studyId)));	
complianceJB.retrieveLockStudyFlag(paramMap);
System.out.println("usrId::"+usrId);
System.out.println("accountId::"+accId);
System.out.println("studyId::"+studyId);
//First check whether the study should be frozen based on study status history

String isSubmittedWithNoAck = null;

//Third check whether the study was recently submitted or resubmitted based on submission status
 boolean isIrbApproved = complianceJB.getIsIrbApproved();
 /*if (!isIrbApproved) {
	UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
	int submStatusCodelstPk = uiFlxPageDao.getFlexLatestSubmissionStatus(StringUtil.stringToNum(studyId));
	if (submStatusCodelstPk > 0) {
		CodeDao submCodeDao = new CodeDao();
		String submStatusSubtype = submCodeDao.getCodeSubtype(submStatusCodelstPk);
		if ("submitted".equals(submStatusSubtype) || "resubmitted".equals(submStatusSubtype)) {
			SubmissionStatusJB submissionStatusJB = new SubmissionStatusJB();
			submissionStatusJB.getSubmissionStatusDetails(uiFlxPageDao.getSubmissionStatusPk());
			Date submStatDate = submissionStatusJB.getSubmissionStatusDate();
			long timeDiff = System.currentTimeMillis() - submStatDate.getTime();
			System.out.println("timeDiff="+timeDiff);
			isSubmittedWithNoAck = (timeDiff < 600000L)? STR_FALSE : STR_TRUE;
		}
	}
}

//Second check whether the study should be frozen based on a flag which is set by the versioning & freezing logic
if (!STR_TRUE.equals(isSubmittedWithNoAck)) {
	if (studyJB != null && "1".equals(studyJB.getStudyIsLocked())) {
		isFrozen = true;
	}
} else if (STR_TRUE.equals(isSubmittedWithNoAck)) {
	isFrozen = false;
} */



%>
<A href=# onclick="openNewMultiVersionWin(<%=pageRight%>,<%=pageRightApndx%>,'N','<%=src%>','<%=tab%>','<%=studyId%>')"><%=MC.M_AddNew_VerOrDocu_Upper%><%--ADD NEW DOCUMENT*****--%></A>
                </td>
				
				<%  } else { %>
                <A href=# onclick="openNewMultiVersionWin(<%=pageRight%>,<%=pageRightApndx%>,'N','<%=src%>','<%=tab%>','<%=studyId%>')"><%=MC.M_AddNew_VerOrDocu_Upper%><%--ADD NEW DOCUMENT*****--%></A>
                </td>
                <% } %>
                
		    </tr>
			</table>
	<%if ("LIND".equals(CFG.EIRB_MODE) && isPopupWin.equals("0")){ %>
	<form name="studyScreenForm">
		<input type="hidden" name="formStudy" value="<%=studyId %>" />
	</form>
	<Form name="studyverbrowser" method="post" action="<%=jspName%>?selectedTab=irb_upload_tab&mode=M&srcmenu=tdmenubaritem3&studyId=<%=studyId%>" onsubmit="">
	<%} else { %>
	<Form name="studyverbrowser" method="post" action="studyVerBrowser.jsp" onsubmit="">
	<%} %>
				<Input type="hidden" name="selectedTab" value="<%=tab%>">
				<Input type="hidden" name="orderType" value="<%=orderType%>">
				<Input type="hidden" name="orderBy" value="<%=orderBy%>">
				<Input type="hidden" name="page" value="<%=pagenum%>">
				
				<input type="hidden" name="rowsReturned" value=<%=rowsReturned%>>
				<input type="hidden" name="verCat" value=<%=verCat%>>
				<input type="hidden" name="verType" value=<%=verType%>>
				<input type="hidden" name="verStat" value=<%=verStat%>>


				<input type="hidden" name="dStudyvercat" value=<%=dStudyvercat%>>
				<input type="hidden" name="dStudyvertype" value=<%=dStudyvertype%>>
				<input type="hidden" name="dVersionStatus" value=<%=dVersionStatus%>>
				<input type="hidden" name="studyVerNumber" value=<%=studyVerNumber%>>
				<input type="hidden" name="studyId" value=<%=studyIdForTabs%>>
				<%if(isPopupWin.equals("1")){ %>
					<Input type="hidden" name="mode" value="N">
					<input type="hidden" name="isPopupWin" value=<%=isPopupWin %>>
				<%}else{ %>
					<Input type="hidden" name="mode" value="M">
				<%} %>
				<table id="versionsTbl" class="outline midAlign custom-center-Align" width="99%" cellpadding="0" cellspacing="0" >
					<tr>
						<th width="10%" onClick="setOrder(document.studyverbrowser,'lower(STUDYVER_NUMBER)')" ><%=LC.L_Version%><%--Version*****--%> &#35; &loz;</th>	<!--Mukul: To fix Bug 4274  -->
						<% if("LIND".equals(CFG.EIRB_MODE)) {  %>
						<% } else { %>
							<th width="12%" onClick="setOrder(document.studyverbrowser,'STUDYVER_DATE')"> <%=LC.L_Version_Date%><%--Version Date*****--%> &loz;</th>
						<% } %>
						<th width="15%" onClick="setOrder(document.studyverbrowser,'lower(STUDYVER_CATEGORY)')"> <%=LC.L_Category%><%--Category*****--%> &loz;</th>
						<th width="10%" onClick="setOrder(document.studyverbrowser,'lower(STUDYVER_TYPE)')"> <%=LC.L_Type%><%--Type*****--%> &loz; </th>
						<% if("LIND".equals(CFG.EIRB_MODE)) {  %>
							<th width="12%" style="display:none" onClick="setOrder(document.studyverbrowser,'lower(SECCOUNT)')"> <%=LC.L_Section%><%--Section*****--%> &loz;</th>
						<% } else { %>
							<th width="12%" onClick="setOrder(document.studyverbrowser,'lower(SECCOUNT)')"> <%=LC.L_Section%><%--Section*****--%> &loz;</th>
						<% } %>	
						<th width="15%" onClick="setOrder(document.studyverbrowser,'lower(APNDXCOUNT)')"> <%=LC.L_Appendix%><%--Appendix*****--%> &loz; </th>
						<% if("LIND".equals(CFG.EIRB_MODE)) {  %>
							<th width="15%" onClick="setOrder(document.studyverbrowser,'lower(STATUS_CODEDESC)')"> <%=LC.L_Attachment_Status%><%--Attachment Status*****--%> &loz; </th>
						<% } else { %>
							<th width="15%" onClick="setOrder(document.studyverbrowser,'lower(STATUS_CODEDESC)')"> <%=LC.L_Status%><%--Status*****--%> &loz; </th>
						<% } %>	
						<th width="5%"><%=LC.L_Delete%><%--Delete*****--%></th>
						<% if(!"LIND".equals(CFG.EIRB_MODE)) {  %>
						<th width="4%"><%=LC.L_Copy%><%--Copy*****--%></th>
						<% } %>	
					</tr>
					<%




int i = 0;

	for(i = 1 ; i <=rowsReturned ; i++){


			int studyVerId = EJBUtil.stringToNum(br.getBValues(i,"PK_STUDYVER"));
			String studyVerStudyId = br.getBValues(i,"FK_STUDY");//
						  studyVerStudyId=(studyVerStudyId==null)?"":studyVerStudyId;
			String verNumber = br.getBValues(i,"STUDYVER_NUMBER");
			//if(verNumber.equals("0") || verNumber.equals("0.00")) { continue; }
			
			verNumber=(verNumber==null)?"":verNumber;
			String statusSubType = br.getBValues(i,"STATUS_CODESUBTYP");
						  statusSubType=(statusSubType==null)?"":statusSubType;
			String statusDesc = br.getBValues(i,"STATUS_CODEDESC");
						  statusDesc=(statusDesc==null)?"":statusDesc;
			String statusPk = br.getBValues(i,"PK_STATUS");
						  statusPk=(statusPk==null)?"":statusPk;
			String studyVerNote = br.getBValues(i,"STUDYVER_NOTES");//
						  studyVerNote=(studyVerNote==null)?"":studyVerNote;
			int    secCount = EJBUtil.stringToNum(br.getBValues(i,"secCount"));
			int    apndxCount = EJBUtil.stringToNum(br.getBValues(i,"apndxCount"));


			String studyVerDate	  =	br.getBValues(i,"STUDYVER_DATE_str");
						  studyVerDate=(studyVerDate==null)?"-":studyVerDate;
			String studyVerCat = br.getBValues(i,"STUDYVER_CATEGORY");
						  studyVerCat=(studyVerCat==null)?"-":studyVerCat;
			String studyVerType = br.getBValues(i,"STUDYVER_TYPE");
						  studyVerType=(studyVerType==null)?"-":studyVerType;



		    appendixDao=appendixB.getByStudyIdAndType(studyVerId, "url");
			ArrayList appendixFile_Uris = appendixDao.getAppendixFile_Uris();


			appendixDao1=appendixB.getByStudyIdAndType(studyVerId, "file");
			ArrayList appendixFile_Uris1 = appendixDao1.getAppendixFile_Uris();
			int uriLen = appendixFile_Uris.size();
			int uriLen1 = appendixFile_Uris1.size();
			    popCrfString = "";
			    popCrfString = popCrfString+"<table width=300px cellspacing=0 cellpadding 0>";	

				if(uriLen==0)
						{
							popCrfString = popCrfString + "<tr><td colspan=2 align=left><B>"+LC.L_NoUrls_Attch+"</B><br></td><tr>";/*popCrfString = popCrfString + "<B>No URLs attached</B>";*****/
						}
				else{
					popCrfString = popCrfString + "<tr><td colspan=2 align=left>&#9679;&nbsp;<B>"+LC.L_My_Urls+"</B><br></td><tr>";/*popCrfString = popCrfString + "<LI><B>My URLs</B><UL>";*****/
						for (int k=0;k<uriLen;k++) {
							popCrfString = popCrfString + "<tr><td align=right width=15px >&nbsp;&nbsp;&nbsp;&#9675;</td><td align=left>" + appendixFile_Uris.get(k) + "</td></tr>";
						}
 	    			}
					if(uriLen1==0)
						{
						if(uriLen!=0)
								popCrfString = popCrfString + "" ;
							popCrfString = popCrfString + "<tr><td colspan=2 align=left><br><B>"+MC.M_NoFilesAttached+"</B><br></td><tr>";/*popCrfString = popCrfString + "<br><B>No Files attached</B>";*****/
						}
						else{
							if(uriLen!=0)
								popCrfString = popCrfString + "" ;

				popCrfString = popCrfString + "<tr><td colspan=2 align=left>&#9679;&nbsp;<B>"+LC.L_My_Files+"</B><br></td><tr>" ;/*popCrfString = popCrfString + "<LI><B>My Files</B><UL>" ;*****/
				for(int k1=0;k1<uriLen1;k1++){
					popCrfString = popCrfString + "<tr><td align=right width=15px >&nbsp;&nbsp;&nbsp;&#9675;</td><td align=left>" + appendixFile_Uris1.get(k1) + "</td></tr>";
				}
						}
				popCrfString = popCrfString+"</table>";
				//popCrfString = popCrfString + "</UL></UL>";



						if(statusSubType.equals("F")) {
							verStatus = "Freeze";
						} else
						{
						 verStatus = "";
						}


						if ((i%2)==0) {
						%>
							<tr class="browserEvenRow">
				        <%
						}
						else{
						%>
						    <tr class="browserOddRow">
				        <%
						}
						%>

						<% if("LIND".equals(CFG.EIRB_MODE)) {%>
							<td ><%= verNumber%></td>
						<% } else { %>
							<td width="150"><A href="#" onclick="openWin(<%=pageRight%>,'M','<%=src%>','<%=tab%>','<%=studyVerId%>')" ><%= verNumber%></A>
							</td>
						<% } %>
						<% if("LIND".equals(CFG.EIRB_MODE)) {  %>
						<% } else { %>
							<td width="150"><%=studyVerDate%>
						<% } %>
						</td>

						<td width="150"><%=studyVerCat%>
						</td>

						<td width="150"><%=studyVerType%>
						</td>

						<% if("LIND".equals(CFG.EIRB_MODE)) {  %>
        					<td width="150" style="display:none"> <A href = "sectionBrowserNew.jsp?mode=M&srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>"> 
        					<%Object[] arguments1 = {secCount}; %>
	    					<%=VelosResourceBundle.getLabelString("L_Sec_Dyn",arguments1)%><%--Sections (<%=secCount%>)*****--%></A>
				        	</td>
						<% } else  { %>
							<td width="150"> <A href = "sectionBrowserNew.jsp?mode=M&srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>"> 
        					<%Object[] arguments1 = {secCount}; %>
	    					<%=VelosResourceBundle.getLabelString("L_Sec_Dyn",arguments1)%><%--Sections (<%=secCount%>)*****--%></A>
				        	</td>
				        <% } %>
				        
        				<td width="150"> <A href = "appendixbrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>&studyId=<%=studyId%>&isPopupWin=<%=isPopupWin %>&statusPk=<%=statusPk %>&statusDesc=<%=statusDesc %>" onmouseover="return overlib('<%=popCrfString.replaceAll("'","\\\\'")%>',CAPTION,'<%=LC.L_Attachments%><%-- Attachments*****--%>');" onmouseout="return nd();">
         				<%Object[] arguments2 = {apndxCount}; %>
	    				<%=VelosResourceBundle.getLabelString("L_Attch_Dyn",arguments2)%><%--Attachments (<%=apndxCount%>)*****--%> </A>
				        </td>
						<td>

						<!--//JM: 031006: verNumber not passing from the link-->
						<!--A href="#" onclick="openWinStatus(<%--=pageRight--%>,'M','<%--=studyVerId--%>','<%--=verNumber--%>','<%--=statusPk--%>')"><%--= statusDesc--%></A-->
						<% // Change by Elayaperumal on 06-15-24 for Protocol Build issue #20722
							if(verStatus.equals("Freeze") || isPopupWin.equals("1"))
							{%> 
							 <B><%= statusDesc%></B>
							<%}
						 else  {%>
							<A href="#" onclick="openWinStatus(<%=pageRight%>,'M','<%=studyVerId%>','','<%=statusPk%>','<%=studyIdForTabs%>','<%=verNumber%>')"><%= statusDesc%></A>
							<% }%>
						<%-- <% // Added by Ganapathy on 04-15-05
							if(verStatus.equals("Freeze") || isPopupWin.equals("1"))
							{}
						 else  {%> --%>

						 <!--<A href="#" onclick="openWinStatus(<%--=pageRight--%>,'N','<%--=studyVerId--%>', '<%--=StringUtil.encodeString(verNumber)--%>','0')">C</A>-->
						  <A href="#" onclick="openWinStatus(<%=pageRight%>,'N','<%=studyVerId%>', '','0','<%=studyIdForTabs%>','<%=verNumber%>')"><img border="0" title="<%=LC.L_Edit%>" alt="<%=LC.L_Edit%>" src="./images/edit.gif" ><%//=LC.L_Edit %><%-- Edit*****--%><%//=LC.L_C%><%--C*****--%></A>
						<%--  <% }%> --%>
						<!--//JM: 031006: StringUtil.encodeString() method applied on version number-->
						<%-- <% if (!"LIND".equals(CFG.EIRB_MODE) || !"Freeze".equals(verStatus)) { %>
						<%if(!isPopupWin.equals("1")){ %> --%>
						<A href="showVersionHistory.jsp?modulePk=<%=studyVerId%>&pageRight=<%=pageRight%>&srcmenu=<%=src%>&studyId=<%=studyIdForTabs%>&selectedTab=<%=tab%>&verNumber=<%=StringUtil.encodeString(verNumber)%>&page=<%=page%>&from=verHistory&fromjsp=showVersionHistory.jsp&isPopupWin=<%=isPopupWin%>&statusDesc=<%=statusDesc %>"><img border="0" title="<%=LC.L_History%>" alt="<%=LC.L_History%>" src="./images/History.gif" ><%--H*****--%></A>
						<%-- <% }} %> --%>
						</td>
						<td align="center"><!--Change by Ganapathy on 04-15-05
						//JM: 12Dec2006, Changed
						-->
						<!--<A href="studyversiondelete.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>&delMode=null" onClick="return confirmBox('<%=verNumber%>',<%=pageRight%>,<%=counter%>);" ><img src="./images/delete.gif" border="0"/></A>-->
						<% if ((!"LIND".equals(CFG.EIRB_MODE) || !"Freeze".equals(verStatus))){%>
						
						<% if ("LIND".equals(CFG.EIRB_MODE)) {
							System.out.println("verNumber------>"+verNumber);
							if (lockedVersions!=null && lockedVersions.size()>0 && !lockedVersions.contains(verNumber)){
						%>
						<A href="studyversiondelete.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>&delMode=null&from=<%=from%>&studyId=<%=studyIdForTabs %>&isPopupWin=<%=isPopupWin%>&mode=<%=mode%>" onClick="return confirmBox('<%=verNumber%>',<%=pageRight%>,<%=rowsReturned%>,'<%=studyVerCat%>');" ><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" /></A>
						    <% }else{
						    CodeDao freezeCode = new CodeDao();
						    StatusHistoryDao shDao = new StatusHistoryDao();
						    shDao = statHistoryB.getStatusHistoryInfo(studyVerId, "er_studyver");
						    boolean freezeFlag = false;
						    if(shDao.getCodelstIds()!=null && shDao.getCodelstIds().size()>0){
						     freezeFlag = shDao.getCodelstIds().contains(String.valueOf(freezeCode.getCodeId("versionStatus","F")));
						    }
						    System.out.println("studyVerId::::"+studyVerId);
						    System.out.println("freezeFlag::::"+freezeFlag);
						    if (freezeFlag){
						    %>
						    -
						    <%}else{%>
						    		<A href="studyversiondelete.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>&delMode=null&isPopupWin=<%=isPopupWin%>&from=<%=from%>&studyId=<%=studyIdForTabs %>&mode=<%=mode%>" onClick="return confirmBox('<%=verNumber%>',<%=pageRight%>,<%=totalRows%>);" ><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" /></A>
						    <%} 
						    }}else { 
						    %>
						<A href="studyversiondelete.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>&delMode=null&from=<%=from%>&studyId=<%=studyIdForTabs %>" onClick="return confirmBox('<%=verNumber%>',<%=pageRight%>,<%=totalRows%>);" ><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" /></A>
						    <% } %>
						<% } else { %>
						-
						<% } %>
						</td>
						<% if (!"LIND".equals(CFG.EIRB_MODE)) { %>
						<td align="center">
						<A href="copystudyversion.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>&copyMode=" onClick="closeChildWindow();return f_check_perm(<%=pageRight%>,'N');"><img border="0" title="<%=LC.L_Copy%>" src="../images/copy.png"/></A>
						</td>
						<% } %>

					</tr>
				    <%

			 		} //end of for loop

					%>

				</table>
				<%if (isPopupWin.equals("1")){ %>
					<script>
				
						$j(document).find("#versionsTbl > tbody").addClass("isPopupWin");
					</script>
				<%} %>
			<% if (rowsReturned == 0){%>
				<script>
				
				$j(document).find("#versionsTbl > tbody").addClass("NoScrollCont");
				</script>
			<%} %>	
<!--/////////////////////////JM pagination///////////////////////////////////////////////////-->
		<table >
		<tr><td>
		<% if (rowsReturned > 0)
		{
			Object[] arguments = {firstRec,lastRec,totalRows};
		%>
					<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
		<%} else {%>
			<font class="recNumber"> <%=MC.M_NoSrchResMatch_Criteria%><%--No search results matching given criteria found*****--%></font>
		<%}%>
		</td></tr>
		</table>
		<table align=center>
			<tr>
				<%	if (curPage==1) startPage=1;

				for ( int count = 1; count <= showPages;count++)
				{

		   				cntr = (startPage - 1) + count;

		   				if ((count == 1) && (hasPrevious))
						{
		   					if(isPopupWin.equals("1")){%>
		   						<td colspan = 2>
								<!-- km-to fix Bug 2442 -->
							  	<A href="studyVerBrowser.jsp?srcmenu=<%=src%>&isPopupWin=<%=isPopupWin %>&studyId=<%=studyId %>&page=<%=cntr-1%>&studyVerNumber=<%=studyVerNumber%>&dStudyvercat=<%=dStudyvercat%>&dStudyvertype=<%=dStudyvertype%>&dVersionStatus=<%=dVersionStatus%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&selectedTab=<%=tab%>&mode=N">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> > </A>
								&nbsp;&nbsp;&nbsp;&nbsp;
								</td>
		   					<%}else{%>
								<td colspan = 2>
								<!-- km-to fix Bug 2442 -->
							  	<A href="<%=jspName%>?srcmenu=<%=src%>&isPopupWin=<%=isPopupWin %>&studyId=<%=studyId %>&page=<%=cntr-1%>&studyVerNumber=<%=studyVerNumber%>&dStudyvercat=<%=dStudyvercat%>&dStudyvertype=<%=dStudyvertype%>&dVersionStatus=<%=dVersionStatus%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&selectedTab=<%=tab%>&mode=\"M\"">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> > </A>
								&nbsp;&nbsp;&nbsp;&nbsp;
								</td>
				
				<%		}}
				%>
					<td>
				<%
		 		 		if (curPage  == cntr)
				 		{
			    %>
								<FONT class = "pageNumber"><%= cntr %></Font>
		       	<%		}else
		       			{
		       			if(isPopupWin.equals("1")){%>
		       				<A href="studyVerBrowser.jsp?srcmenu=<%=src%>&isPopupWin=<%=isPopupWin %>&page=<%=cntr%>&studyVerNumber=<%=studyVerNumber%>&studyId=<%=studyId %>&dStudyvercat=<%=dStudyvercat%>&dStudyvertype=<%=dStudyvertype%>&dVersionStatus=<%=dVersionStatus%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&selectedTab=<%=tab%>&mode=N"><%= cntr%></A>
		       			<%}else{%>
		       				
								<A href="<%=jspName%>?srcmenu=<%=src%>&isPopupWin=<%=isPopupWin %>&page=<%=cntr%>&studyVerNumber=<%=studyVerNumber%>&studyId=<%=studyId %>&dStudyvercat=<%=dStudyvercat%>&dStudyvertype=<%=dStudyvertype%>&dVersionStatus=<%=dVersionStatus%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&selectedTab=<%=tab%>&mode=\"M\""><%= cntr%></A>
		        
		        <%
		       			}
		        }
		        %>
					</td>
				<%
				}

				if (hasMore)
				{
					if(isPopupWin.equals("1")){
				
					%>
					<td colspan = 3 align = center>&nbsp;&nbsp;&nbsp;&nbsp;
				    <A href="studyVerBrowser.jsp?mode=N&isPopupWin=<%=isPopupWin %>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&studyId=<%=studyId %>	">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
					</td>
				<%}else{%>
					<td colspan = 3 align = center>&nbsp;&nbsp;&nbsp;&nbsp;
					<A href="<%=jspName%>?srcmenu=<%=src%>&isPopupWin=<%=isPopupWin %>&page=<%=cntr+1%>&studyId=<%=studyId %>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&mode=\"M\"">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%> > </A>
					</td>
					<%}
				}
				%>

	   		</tr>
	    </table>

<!--///////////////////////////JM pagination/////////////////////////////////////////////////-->
<%if(isPopupWin.equals("1")){ %>
<br><div style="text-align:center;">
<button type="button" style="width:05%; position:relative; margin:auto; text-align: center; background-color: #5c5c5c; font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:12px; color:#fff; border-radius:3px 3px 3px 3px;" onclick="window.close()"><font color="white"><%=LC.L_Close %></font></button>
</div>
<%} %>

	</Form>
<%
		} //end of if body for page right
		else
		{
		%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
		<%
		} //end of else body for page right
		}// end of else of study check
	}//end of if body for session
	else
	{
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>
	<div>
    	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
<%if(!"LIND".equals(CFG.EIRB_MODE)){ %>
</div>
<%} %>
<div class ="mainMenu" id = "emenu" >
  <jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>

</html>