<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="lab" scope="request" class="com.velos.eres.web.lab.LabJB"/>
<jsp:useBean id ="auditRowEresJB" scope="session" class="com.velos.eres.audit.web.AuditRowEresJB"/>
<jsp:useBean id ="audittrails" scope="session" class="com.aithent.audittrail.reports.AuditUtils"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,com.velos.eres.business.pref.*,com.velos.eres.business.person.*"%>
<%@page import="com.velos.eres.audit.web.AuditRowEresJB"%>
<%
String eSign = request.getParameter("eSign");

HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))

{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String oldESign = (String) tSession.getValue("eSign");
	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	} else {
 	//String studyId = (String) tSession.getValue("studyId");
	String studyId = request.getParameter("studyId");
	studyId=(studyId==null)?"":studyId;
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = null;
	usr = (String) tSession.getValue("userId");
	String fromLab=request.getParameter("fromlab");
	fromLab=(fromLab==null)?"":fromLab;
// dSAVE DATA
//declare variables
  int labPK  = 0;
  String mode = null;
//Declare varibale
	 int rid=0;
	 int id=0,pers=0;
	 String testId="";
	 String perId="";
	 String testDate="";
	 String testResult="";
	 String testType="";
	 String testUnit="";
	 String testRange="";
	 String testLLN="";
	 String testULN="";
	 String siteId="";
	 String testStatus="";
	 String accnNum="";
	 String testgroupId="";
	 String abnrFlag="";
	 String patprotId="";
	 String eventId="";
	 String creator="";
	 String modifiedBy="";
	 String lResult="" ;
	 String notes="";
	 String grade="",patientCode="";
	 String advName="",testcatId="";
	 String selstudyId="",stdPhase="";
	 String windowType="";
	 String[] saLabDates=null;
	 String[] saLabIds=null;
	 String[] saAccnNum=null;
	String[] saLabResults=null;
	String[] saTestTypes=null;
	String[] saLabUnits=null;
	String[] saLabLlns= null;
	String[] saLabUlns=null;
	String[] saLabStatus=null;
	String[] saLabAbnrs=null;
	String[] saLabCatIds=null;
	String[] saLabAdvNames=null;
	String[] saLabGrades=null;
	String[] saLabNotes=null;
	String[] saLabLResults=null;
	String[] saStdPhase=null;
	String src = request.getParameter("srcmenu");
	mode = request.getParameter("mode");
	String pages= request.getParameter("page");
	String pkey = request.getParameter("pkey");
	patprotId=request.getParameter("patProtId");
	patientCode=request.getParameter("patientCode");
	selstudyId=request.getParameter("selstudyId");
	selstudyId=(selstudyId==null)?"":selstudyId;
	windowType=(request.getParameter("windowType")==null)?"":request.getParameter("windowType");

//JM: 07Jul2009: #INVP2.11
	String specimenPk=request.getParameter("specimenPk");
	specimenPk=(specimenPk==null)?"":specimenPk;

	String calldFrom=request.getParameter("calledFromPg");
	calldFrom=(calldFrom==null)?"":calldFrom;
	String FromPatPage = request.getParameter("FromPatPage");
	if(FromPatPage==null){
		FromPatPage="";
	}
	String patientCode1=request.getParameter("patientCode");
	if(patientCode1==null){
		patientCode1="";
	}

	if (patientCode==null) patientCode="";
	 if (mode.equals("N")){
	  saLabDates=request.getParameterValues("labdate");
	  saLabIds=request.getParameterValues("labpk");
	  saLabCatIds=request.getParameterValues("testcatId");
	  saAccnNum=request.getParameterValues("accnumber");
	  saLabResults=request.getParameterValues("tresult");
	  saTestTypes=request.getParameterValues("testtype");
	  saLabUnits=request.getParameterValues("unit");
	  saLabLlns=request.getParameterValues("LLN");
	  saLabUlns=request.getParameterValues("ULN");
	  saLabStatus=request.getParameterValues("tstatus");
	  saLabAbnrs=request.getParameterValues("abnresult");
	  saLabAdvNames=request.getParameterValues("advName");
	  saLabGrades=request.getParameterValues("grade");
	  saLabNotes=request.getParameterValues("notes");
	  saLabLResults=request.getParameterValues("longresult");
	  saStdPhase=request.getParameterValues("stdPhase");
	  for (int i=0;i<saStdPhase.length;i++){
	  System.out.println("saStdPhase"+saStdPhase[i]);

	  }
	 }
	 else
	 {
		testId= request.getParameter("testId");
		testDate=request.getParameter("labdate");
		accnNum= request.getParameter("accnumber");
		testcatId= request.getParameter("testcatId");
		testResult= request.getParameter("tresult");
		testUnit= request.getParameter("unit");
		testLLN= request.getParameter("LLN");
		testULN= request.getParameter("ULN");
		lResult = request.getParameter("longresult");
		testStatus= request.getParameter("tstatus");
		abnrFlag= request.getParameter("abnresult");
		advName= request.getParameter("advName");
		grade= request.getParameter("grade");
		notes= request.getParameter("notes");
		testType=request.getParameter("testtype");
		stdPhase=request.getParameter("stdPhase");
		stdPhase=(stdPhase==null)?"":stdPhase;

	}
// got values
	int saved = 0;
	if (mode.equals("M"))
	{
		labPK = EJBUtil.stringToNum(request.getParameter("labpk"));
		lab.setId(labPK);
		lab.getLabDetails();
	 	lab.setTestId(testId);
		lab.setPerId(pkey);
		lab.setTestDate(testDate);
		lab.setTestResult(testResult);
		lab.setTestType(testType);
		lab.setTestUnit(testUnit);
		lab.setTestRange(testRange);
		lab.setTestLLN(testLLN);
		lab.setTestULN(testULN);
		lab.setSiteId(siteId);
		lab.setTestStatus(testStatus);
		lab.setAccnNum(accnNum);
		lab.setTestgroupId(testcatId);
		lab.setAbnrFlag(abnrFlag);
		lab.setEventId(eventId);
		lab.setErName(advName);
		lab.setErGrade(grade);
		lab.setStdPhase(stdPhase);
		lab.setStudyId(selstudyId);
		lab.setPatprotId(patprotId);
		lab.setModifiedBy(usr);
		lab.setIpAdd(ipAdd);
		//lab.setSpecimenId(specimenPk);
		saved = lab.updateLab();
		if (saved==0){ 
			if((!(notes.equals(""))) || (!(lResult.equals("")))){
				saved=lab.updateClobData(labPK,lResult,notes); 
				
				rid = auditRowEresJB.getRidForDeleteOperation(labPK, EJBUtil.stringToNum(usr), "ER_PATLABS", "PK_PATLABS");
				audittrails.deleteRaidList("eres", rid, usr, "U");
			}
		}
		 if (saved<0) saved=-2; else saved=0;

		String remarks = request.getParameter("remarks");
		if (saved >= 0){
			if (!StringUtil.isEmpty(remarks)){
				AuditRowEresJB auditJB = new AuditRowEresJB();
				auditJB.setReasonForChangeOfPatLab(labPK, StringUtil.stringToNum(usr), remarks);
			}
		}
	}
	else
	{
		LabDao labDao=new LabDao();
		labDao.setPkey(pkey);
		labDao.setSiteId(siteId);
		labDao.setPatprotId(patprotId);
		labDao.setStudyId(selstudyId);
		labDao.setUser(usr);
		labDao.setIpAdd(ipAdd);
		labDao.setSaLabDates(saLabDates);
		labDao.setSaLabIds(saLabIds);
		labDao.setSaAccnNums(saAccnNum);
		labDao.setSaLabResults(saLabResults);
		labDao.setSaTestTypes(saTestTypes);
		labDao.setSaLabUnits(saLabUnits);
		labDao.setSaLabLlns(saLabLlns);
		labDao.setSaLabUlns(saLabUlns);
		labDao.setSaLabStatus(saLabStatus);
		labDao.setSaLabAbnrs(saLabAbnrs);
		labDao.setSaLabCatIds(saLabCatIds);
		labDao.setSaLabAdvNames(saLabAdvNames);
		labDao.setSaLabGrades(saLabGrades);
		labDao.setSaLabLResults(saLabLResults);
		labDao.setSaLabNotes(saLabNotes);
		labDao.setSaStdPhase(saStdPhase);
		labDao.setSpecimenId(specimenPk);
		//lab.setCreator(usr);
		//lab.setIpAdd(ipAdd);
		saved = lab.insertLab(labDao);
		}

  if (saved >= 0)
 {

%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p>
<%if (fromLab.equals("pat")){
if(windowType.equals("child")){
		%>
			<script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
			</script>
		<%}else{%>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=formfilledpatbrowser.jsp?mode=initial&selectedTab=11&srcmenu=<%=src%>&patProtId=<%=patprotId%>&pkey=<%=pkey%>&page=<%=pages%>&patientCode=<%=patientCode%>&formPullDown=lab&fromlab=<%=fromLab%>">
<%}}else if (fromLab.equals("patstd")){
		if(windowType.equals("child")){
		%>
			<script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
			</script>
		<%}else{%>
			<META HTTP-EQUIV=Refresh CONTENT="1; URL=formfilledstdpatbrowser.jsp?mode=initial&selectedTab=8&srcmenu=<%=src%>&patProtId=<%=patprotId%>&pkey=<%=pkey%>&page=<%=pages%>&patientCode=<%=patientCode%>&studyId=<%=studyId%>&formPullDown=lab&fromlab=<%=fromLab%>">
		<%}
}else if (calldFrom.equals("specimen")){
		if(windowType.equals("child")){%>
			<script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
			</script>
		<%}else{%>
			<META HTTP-EQUIV=Refresh CONTENT="1; URL=specimenlabbrowser.jsp?mode=M&selectedTab=1&srcmenu=<%=src%>&patProtId=<%=patprotId%>&pkey=<%=pkey%>&page=<%=pages%>&patientCode=<%=patientCode%>&studyId=<%=studyId%>&formPullDown=spec&fromlab=<%=fromLab%>&specimenPk=<%=specimenPk%>&calledFromPg=<%=calldFrom%>&FromPatPage=<%=FromPatPage %>&patientCode=<%=patientCode1 %>">
		<%}
}
}
else if (saved==-2)
{
%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_ProblStr_LongResFld %><%-- Problem in storing Long Results and Notes fields.*****--%></p>
<%if (fromLab.equals("pat")){%>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=formfilledpatbrowser.jsp?mode=initial&selectedTab=11&srcmenu=<%=src%>&patProtId=<%=patprotId%>&pkey=<%=pkey%>&page=<%=pages%>&patientCode=<%=patientCode%>&formPullDown=lab&fromlab=<%=fromLab%>">
<%} else if (fromLab.equals("patstd")){%>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=formfilledstdpatbrowser.jsp?mode=initial&selectedTab=8&srcmenu=<%=src%>&patProtId=<%=patprotId%>&pkey=<%=pkey%>&page=<%=pages%>&patientCode=<%=patientCode%>&studyId=<%=studyId%>&formPullDown=lab&fromlab=<%=fromLab%>">
<%}else if (calldFrom.equals("specimen")){%>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=specimenlabbrowser.jsp?mode=M&selectedTab=1&srcmenu=<%=src%>&patProtId=<%=patprotId%>&pkey=<%=pkey%>&page=<%=pages%>&patientCode=<%=patientCode%>&studyId=<%=studyId%>&formPullDown=spec&fromlab=<%=fromLab%>&specimenPk=<%=specimenPk%>&calledFromPg=<%=calldFrom%>&FromPatPage=<%=FromPatPage %>&patientCode=<%=patientCode1 %>">

<%
}
}
else
{
%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_DataCnt_Svd %><%-- Data could not be saved.*****--%></p>
<%if (fromLab.equals("pat")){%>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=formfilledpatbrowser.jsp?mode=initial&selectedTab=11&srcmenu=<%=src%>&patProtId=<%=patprotId%>&pkey=<%=pkey%>&page=<%=pages%>&patientCode=<%=patientCode%>&formPullDown=lab&fromlab=<%=fromLab%>">
<%} else if (fromLab.equals("patstd")){%>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=formfilledstdpatbrowser.jsp?mode=initial&selectedTab=10&srcmenu=<%=src%>&patProtId=<%=patprotId%>&pkey=<%=pkey%>&page=<%=pages%>&patientCode=<%=patientCode%>&studyId=<%=studyId%>&formPullDown=lab&fromlab=<%=fromLab%>">
<%}else if (calldFrom.equals("specimen")){%>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=specimenlabbrowser.jsp?mode=M&selectedTab=1&srcmenu=<%=src%>&patProtId=<%=patprotId%>&pkey=<%=pkey%>&page=<%=pages%>&patientCode=<%=patientCode%>&studyId=<%=studyId%>&formPullDown=lab&fromlab=<%=fromLab%>&specimenPk=<%=specimenPk%>&calledFromPg=<%=calldFrom%>&FromPatPage=<%=FromPatPage %>&patientCode=<%=patientCode1 %>">

<%
}
}
}//end of if for eSign check
}//end of if body for session
else
{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</BODY>
</HTML>

