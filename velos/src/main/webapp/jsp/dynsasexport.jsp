<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title><%=LC.L_Form_RptPreview%><%--Form Report Preview*****--%></title>
	<%@ page language = "java" import = "java.io.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB,java.lang.reflect.*"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />
	<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
	<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
	<jsp:useBean id="sasB" scope="page" class="com.velos.eres.web.common.CommonJB" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
</head>
<% String sql="",tempStr="",sqlStr="",name="",selValue="",fmt="";
   int strlen=0,firstpos=-1,secondpos=-1,patId=0,studyId=0;
   	int seq=0;
	String mode = request.getParameter("mode");
	String value="",patientID="",studyNumber="",orderStr="",order="",filter="",dataOrder="",fltrDD="",mapSecId="",prevSecId="";
	String formName="";
	StringBuffer htmlString=new StringBuffer();
	int personPk=0,studyPk=0,repId=0,index=-1;
	DynRepDao dynDao=new DynRepDao();
	DynRepDao dynDaoFltr=new DynRepDao();
	ArrayList selectedfld=null;
	ArrayList dataList=new ArrayList() ;
	ArrayList tempList=new ArrayList();
	ArrayList lfltrIds= new ArrayList();
	ArrayList lfltrNames= new ArrayList();
	ArrayList lfltrStrs= new ArrayList();
	ArrayList mapSecNames=new ArrayList();
	ArrayList mapSecIds=new ArrayList();
	ArrayList repFldNames=new ArrayList();
	ArrayList secFldCols=new ArrayList();
	ArrayList repFldNamesSeq=new ArrayList();
	ArrayList tempVal=new ArrayList();
	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))
	{ 
	String repIdStr=request.getParameter("repId");
	int userId=EJBUtil.stringToNum((String)tSession.getAttribute("userId"));
	 fmt =request.getParameter("exp");
	if (fmt==null) fmt="";
	if (repIdStr==null) repIdStr="";
	 HashMap FldAttr=new HashMap();
	 HashMap TypeAttr=new HashMap();
	 HashMap FltrAttr=new HashMap();
	 HashMap SortAttr= new HashMap();
	 HashMap RepAttr= new HashMap();
	 HashMap attributes;
	 //This is done to remove the session varibale from previous report.
	 attributes=(HashMap)tSession.getAttribute("attributes");
	 if (attributes==null) attributes=new HashMap();
	 if (attributes.isEmpty()){
	 repId=(new Integer(repIdStr)).intValue();
	if (repId<= 0)
	 {
	   %>
	   <script>window.close();</script>
	  <%}
	dynDao=dynrepB.populateHash(repIdStr);
	attributes=dynDao.getAttributes();
	tSession.setAttribute("attributes",attributes);
	 }
	 if (attributes.containsKey("FldAttr")) FldAttr=(HashMap)attributes.get("FldAttr");
	 if (attributes.containsKey("TypeAttr")) TypeAttr=(HashMap)attributes.get("TypeAttr");
	 if (attributes.containsKey("FltrAttr")) FltrAttr=(HashMap)attributes.get("FltrAttr");
	 if (attributes.containsKey("SortAttr")) SortAttr=(HashMap)attributes.get("SortAttr");
	 if (attributes.containsKey("RepAttr")) RepAttr=(HashMap)attributes.get("RepAttr");
	 
	/*String[] fldCol=request.getParameterValues("fldCol");
	ArrayList fldCols=EJBUtil.strArrToArrayList(fldCol);
	String[] fldName=request.getParameterValues("fldName");
	ArrayList fldNames=EJBUtil.strArrToArrayList(fldName);
	String[] fldDispName=request.getParameterValues("fldDispName");
	String[] fldOrder=request.getParameterValues("fldOrder");
	String[] fldWidth=request.getParameterValues("fldWidth");*/
	ArrayList fldCols=new ArrayList();
	ArrayList fldNames=new ArrayList();
	ArrayList fldDispName=new ArrayList();
	ArrayList fldWidth=new ArrayList();
	if (FldAttr.containsKey("sessCol")) fldCols=(ArrayList)FldAttr.get("sessCol");
	if (FldAttr.containsKey("sessColName")) fldNames=(ArrayList)FldAttr.get("sessColName");
	if (FldAttr.containsKey("sessColDisp")) fldDispName=(ArrayList)FldAttr.get("sessColDisp");
	if (FldAttr.containsKey("sessColWidth")) fldWidth=(ArrayList)FldAttr.get("sessColWidth");
	
	filter=request.getParameter("filter");
	filter=(filter==null)?"":filter;
	if (filter.length()==0){
	if (FltrAttr.containsKey("filter")) filter=(String)FltrAttr.get("filter");
	if (filter==null) filter="";
	}
	filter=StringUtil.decodeString(filter);
	String formType=(String)TypeAttr.get("dynType");
	formType=(formType==null)?"":formType;
	formType=(formType=="study")?"S":formType;
	formType=(formType=="pat")?"P":formType;
	orderStr=request.getParameter("order");
	if (orderStr==null) orderStr="";
	if (orderStr.length()==0){
	if (SortAttr.containsKey("repOrder")) dataOrder=(String)SortAttr.get("repOrder");
	if (dataOrder==null) dataOrder="";
	if (dataOrder.length()>0){
	dataOrder=StringUtil.replace(dataOrder,"[$$]",",");
	dataOrder=StringUtil.replace(dataOrder,"[$]"," ");
	orderStr=" order by "+ dataOrder;
	}
	}
	String repHeader=request.getParameter("repHeader");
	String repFooter=request.getParameter("repFooter");
	if (repHeader==null) repHeader="";
	if (repHeader.length()==0){
	if (RepAttr.containsKey("repHdr")) repHeader=(String)RepAttr.get("repHdr");
	if (repHeader==null) repHeader="";
	}
	if (repFooter==null) repFooter="";
	if (repFooter.length()==0){
	if (RepAttr.containsKey("repFtr")) repFooter=(String)RepAttr.get("repFtr");
	if (repFooter==null) repFooter="";
	}
	String formId=request.getParameter("formId");
	if (formId==null) formId="";
	if (formId.length()==0) {
	if (TypeAttr.containsKey("formId")) formId=(String)TypeAttr.get("formId");
	}
	
	for (int i=0;i<fldCols.size();i++){
	  tempStr=(String)fldCols.get(i) ;
	  name=(String)fldNames.get(i);
	   if (name==null) name="";
	  if (name.length()>=32) name=name.substring(0,30);
	  if (name.indexOf("?")>=0) 
	   name=name.replace('?','q');
	   if (name.indexOf("\\")>=0)
	   name=name.replace('\\','S');
	  /*if (order.length()>0) 
	    if (orderStr.length()>0){
	    orderStr=orderStr + " , \"" + name+"\" " + order;
	    }
	    else{
	    orderStr="\""+name+ "\" " + order;
	    }*/
	  if (tempStr.indexOf("|")>=0){
	  secFldCols.add(tempStr);
	  repFldNames.add((String)fldDispName.get(i));
	  repFldNamesSeq.add(new Integer(i));
	  tempStr=tempStr.replace('|',',');
	  
	  tempStr=StringUtil.replaceAll(tempStr, ",", "||'[VELSEP]'||");
	  
	    if (sqlStr.length()==0)
	  	sqlStr=" ("+tempStr+") as \"" + name+ "\"" ;
		 else
		sqlStr=sqlStr+",  ("+tempStr+") as \"" + name +"\"";
	}
	else{
		if (sqlStr.length()==0)  sqlStr=tempStr +" as \"" +name +"\"";
		else sqlStr=sqlStr + ", " + tempStr + " as \"" + name+"\"";
	}
		
		 
	}
	filter=(filter.equals("undefined")?"":filter);
	 if (formType.equals("S"))
	if ((filter.trim()).length()>0){
	filter=StringUtil.decodeString(filter);
	sql =" select ID,fk_patprot as PATPROT, (SELECT study_number FROM ER_STUDY WHERE pk_study=ID) AS study_num, " + sqlStr + " from er_formslinear where fk_form="+formId +"  and " + filter  ;
	}else {
	sql =" select ID,fk_patprot as PATPROT,(SELECT study_number FROM ER_STUDY WHERE pk_study=ID) AS study_num, " + sqlStr + " from er_formslinear where fk_form="+formId ;
	}
	
		
	if (formType.equals("P"))
	if ((filter.trim()).length()>0){
	filter=StringUtil.decodeString(filter);
	sql =" select ID,(select NVL(patprot_patstdid,'<I>(Patient removed from study)</I>') from er_patprot where pk_patprot = a.fk_patprot) as PATPROT,(SELECT per_code FROM ER_PER WHERE pk_per=ID ) pat_code, " + sqlStr + " from er_formslinear a where fk_form="+formId +"  and " + filter  ;
	} else {
	sql =" select ID,(select NVL(patprot_patstdid,'<I>(Patient removed from study)</I>') from er_patprot where pk_patprot = a.fk_patprot) as PATPROT,(SELECT per_code FROM ER_PER WHERE pk_per=ID ) pat_code, " + sqlStr + " from er_formslinear a where fk_form="+formId  ;
	}
	//attach patient /study id criteira
	if (formType.equals("P")) {
	/* Attach Organizartion check */
	 sql = sql + " AND ID IN (  SELECT pk_per FROM ER_PER c ,ER_USERSITE d  WHERE c.fk_site = d. fk_site AND d. fk_user=" + userId+
	 " AND d.usersite_right>0 )";
	if (TypeAttr.containsKey("perId")) patId=EJBUtil.stringToNum((String)TypeAttr.get("perId"));
		if (TypeAttr.containsKey("studyId")) studyId=EJBUtil.stringToNum((String)TypeAttr.get("studyId"));
	if (patId>0)	sql = sql+" and Id="+patId ; 
	else if (studyId>0)	sql=sql +" and Id in (select fk_per from er_patprot where fk_study="+studyId+ " and patprot_stat=1)";
	
	/*put a check to restrcit data for those studies which user don't have access to*/
        //sql=sql + " AND " + userId +" IN ( SELECT fk_user FROM ER_STUDYTEAM WHERE fk_user="+ userId +" AND " +  
	//" fk_study=(SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=a.fk_patprot) " +  
	//"  AND ((Pkg_Util.f_checkRight(study_team_rights,'STUDYFRMACC','study_rights')=1)  OR (study_team_usr_type='S')))";
	//sql=sql + " AND " + userId +" IN ( SELECT fk_user FROM ER_STUDYTEAM WHERE fk_user="+ userId +" AND " +  
	//" ((a.fk_patprot is null) or (fk_study=(SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=a.fk_patprot))) " +  
	//"  AND ((Pkg_Util.f_checkRight(study_team_rights,'STUDYFRMACC','study_rights')=1)  OR (study_team_usr_type='S')))";
	sql=sql + " AND ((a.fk_patprot is null) or (" + userId +" IN ( SELECT fk_user FROM ER_STUDYTEAM WHERE fk_user="+ userId +" AND " +  
	" ((fk_study=(SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=a.fk_patprot))) " +  
	"  AND ((Pkg_Util.f_checkRight(study_team_rights,'STUDYFRMACC','study_rights')=1)  OR (study_team_usr_type='S')))))";
	}
	if (formType.equals("S")) {
	if (TypeAttr.containsKey("studyId")) studyId=EJBUtil.stringToNum((String)TypeAttr.get("studyId"));
	if (studyId>0) {	
	sql = sql+" and Id="+studyId;
	}
	else 
	{ 
	 /* block studies to which user does not have access*/
	 sql=sql+" AND "+ userId +" IN ( SELECT fk_user FROM ER_STUDYTEAM WHERE fk_user="+ userId + " and fk_study=ID " + 
	 " AND ((Pkg_Util.f_checkRight(study_team_rights,'STUDYFRMACC','study_rights')=1)  OR (study_team_usr_type='S'))) " ;
	} 
	}
	
	//end attach criteria
	if (orderStr.length()>0)	sql =sql+ "  " + orderStr ;
	else {
	if (formType.equals("P")) sql =sql+ "  " + "order by pat_code" ;
	else if (formType.equals("S")) sql =sql+ "  " + "order by study_num" ;
	 }
	
	//sql="select "+ sqlStr+" from er_formslinear where fk_form="+formId; 
	System.out.println(sql+formType);
	//dynDao=dynrepB.getReportData(sql,(( String [] ) fldNames.toArray ( new String [ fldNames.size() ] )),formType);
	//dynDao.getReportData(sql,fldName);
	SasExportDao sas=sasB.getSasHandle("adhoc",EJBUtil.stringToNum(repIdStr));
	
	/*Class cl = sas.getClass();
	
	System.out.println("************" + cl.getName());
	
	Method [] mdArr = cl.getMethods();
	
	for (int o = 0; o < mdArr.length ; o++)
	{
		Method md = mdArr[o];
		System.out.println("************Method : " + o + "*" + md.toString());
	}*/
	sas.GenerateStructure();
	sas.setFormId(formId);
	sas.setFormName(formName);
	sas.GenerateData(sql,(( String [] ) fldNames.toArray ( new String [ fldNames.size() ] )),formType);
	VelosWriter vWrite=new VelosWriter();
	vWrite.GetFileWriter("SAS",".sas");
	vWrite.writeFile("options validvarname=any;\n");
	vWrite.writeFile((sas.getProcFormat()).toString());
	vWrite.writeFile("DATA ERES;\n");
	vWrite.writeFile((sas.getFormatCommand()).toString());
	vWrite.writeFile((sas.getLabelCommand()).toString());
	vWrite.writeFile("INFILE DATALINES DSD DLM='|' TRUNCOVER;\n");
	vWrite.writeFile((sas.getProcInput()).toString() +"\n");
	vWrite.writeFile("DATALINES;\n");
	vWrite.writeFile((sas.getProcData()).toString());
	vWrite.closeFile();
	String downloadPath="";
	downloadPath=vWrite.getDownloadStr();
	System.out.println("Path to Open the file is"+downloadPath);%>
	<META HTTP-EQUIV=Refresh CONTENT="0; URL=<%=downloadPath%>">
	<p class="sectionHeading"><%=LC.L_Sas_Exported%><%--SAS Exported*****--%></p>	
	<%		
	//end retrieve section
	 

} else {%>  //else of if body for session
	<jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%
}
%>
</body>
</html>
