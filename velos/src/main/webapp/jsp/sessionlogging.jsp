<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page import="javax.servlet.http.HttpUtils,java.text.SimpleDateFormat,java.util.*,com.velos.eres.service.util.*" %>
	
<%
	HttpSession tSession = request.getSession(false);
	if(tSession != null) {
		String modRight = (String) tSession.getValue("modRight");
		int modlen = modRight.length();
		com.velos.eres.business.common.CtrlDao acmod = new com.velos.eres.business.common.CtrlDao();
		acmod.getControlValues("module");
		ArrayList acmodfeature =  acmod.getCValue();
		ArrayList acmodftrSeq = acmod.getCSeq();
		int sessLogSeq = acmodfeature.indexOf("SESSLOG");
		sessLogSeq	= ((Integer) acmodftrSeq.get(sessLogSeq)).intValue();
		char sessLogAppRight = modRight.charAt(sessLogSeq - 1);
		if (String.valueOf(sessLogAppRight).compareTo("1") == 0 && sessLogAppRight > 0)
		{
			String pkUserSession = (String) tSession.getValue("pkUserSession");
			javax.servlet.http.HttpUtils a = new HttpUtils();			
	   		String queryString = request.getQueryString();
   			StringBuffer requestURL = a.getRequestURL(request);
   			String completeURL = requestURL + "?" + queryString;
   			Long entityId = null;
   			String entityType = null;
   			String eventCode = null;
   			String entityIdentifier = null;
   			if(request.getParameter("entityId")!=null && request.getParameter("entityId").toString()!=""
   				&& !request.getParameter("entityId").equals("") && !request.getParameter("entityId").equals("null")
				&& request.getParameter("entityId")!="null"){
               entityId = Long.parseLong(request.getParameter("entityId").toString());
   			}
   			if(request.getParameter("entityType")!=null && request.getParameter("entityType").toString()!=""
   				&& !request.getParameter("entityType").equals("") && !request.getParameter("entityType").equals("null")
				&& request.getParameter("entityType")!="null"){
   				entityType = request.getParameter("entityType").toString();
   			}
   			if(request.getParameter("entityIdentifier")!=null && request.getParameter("entityIdentifier").toString()!=""
   				&& !request.getParameter("entityIdentifier").equals("") && !request.getParameter("entityIdentifier").equals("null")
				&& request.getParameter("entityIdentifier")!="null"){
   				entityIdentifier = request.getParameter("entityIdentifier").toString();
   			}
   			if(request.getParameter("eventCode")!=null && request.getParameter("eventCode").toString()!=""
   				&& !request.getParameter("eventCode").equals("") && !request.getParameter("eventCode").equals("null")
				&& request.getParameter("eventCode")!="null"){
   				eventCode = request.getParameter("eventCode").toString();
   			}
			//Rohit  Bug #2848	
			//String dateFormat = Configuration.getAppDateFormat();
			//Calendar cal =  Calendar.getInstance();
			//SimpleDateFormat sdf = new SimpleDateFormat (dateFormat+" hh:mm:ss a" ); 
			//String accessDateTime = sdf.format(cal.getTime()) ;
			
			String accessDateTime = DateUtil.getCurrentDateTime();

			com.velos.eres.web.userSessionLog.UserSessionLogJB userSessionLogJB = new com.velos.eres.web.userSessionLog.UserSessionLogJB();
			userSessionLogJB.setUserSession(pkUserSession);
			userSessionLogJB.setUslURL(completeURL);
			userSessionLogJB.setUslAccessTime(accessDateTime);
			if(entityType!=null && entityId!=null && entityIdentifier!=null && eventCode!=null 
					&& entityType!="" && entityIdentifier!="" && eventCode!=""){
				userSessionLogJB.setEntityId(entityId);
				userSessionLogJB.setEntityType(entityType);
				userSessionLogJB.setEntityIdentifier(entityIdentifier);
				userSessionLogJB.setEventCode(eventCode);
			}
			userSessionLogJB.setUserSessionLogDetails();
			
		}
	}
%>			



		
