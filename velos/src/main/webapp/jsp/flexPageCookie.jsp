<%@page import="java.net.URLDecoder"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList,java.util.HashMap,org.json.*"%>
<%@page import="com.velos.eres.widget.business.common.UIFlxPageDao" %>
<%@page import="com.velos.eres.service.util.StringUtil"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%!
static final String COOKIE = "cookie";
static final String SETTINGS = "settings";
static final String TYPE = "type";
static final String USER_ID = "userId";
static final String TYPE_GET_COOKIE = "typeGetCookie";
static final String TYPE_SAVE_COOKIE = "typeSaveCookie";
static final String TYPE_GET_SETTINGS = "typeGetSettings";
static final String TYPE_SAVE_SETTINGS = "typeSaveSettings";
%>
<%
HttpSession tSession = request.getSession(true); 
if (!sessionmaint.isValidSession(tSession)) {
	return;
}
String pageId = request.getParameter("pageId");

String userId = (String)tSession.getAttribute(USER_ID);
if (StringUtil.isEmpty(userId) || StringUtil.isEmpty(pageId)){
	return;
}

UIFlxPageDao UIFlxPageDao = new UIFlxPageDao();
JSONObject jsObj = new JSONObject();
if (TYPE_GET_COOKIE.equals(request.getParameter(TYPE))) {
	jsObj = UIFlxPageDao.flexPageData(pageId);
	out.println(UIFlxPageDao.getFlxPageCookie(jsObj));
	return;
}
if (TYPE_GET_SETTINGS.equals(request.getParameter(TYPE))) {
	//out.println(UIFlxPageDao.getGadgetInstanceSettings(userId));
	return;
}
if (TYPE_SAVE_SETTINGS.equals(request.getParameter(TYPE))) {
	String settings = request.getParameter(SETTINGS);
	HashMap<String, String> hash = new HashMap<String, String>();
	//hash.put(UIFlxPageDao.GADGET_SETTINGS, settings);
	//UIFlxPageDao.setGadgetInstanceSettings(userId, hash);
	return;
}
if (TYPE_SAVE_COOKIE.equals(request.getParameter(TYPE))) {
	String cookie = request.getParameter(COOKIE);
	cookie=URLDecoder.decode(cookie);
	if (cookie == null || cookie.length() < 1) {
		return;
	}
	HashMap<String, String> hash = new HashMap<String, String>();
	hash.put(UIFlxPageDao.FLX_PAGE_COOKIE, cookie);
	UIFlxPageDao.updateFlexPageCookie(pageId, hash);
	return;
}
%>
