<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html style="overflow:hidden">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=11" /> 
<jsp:include page="localization.jsp" flush="true"/>
<title><%=LC.L_Cal_MngVsts%><%--Calendar >> Manage Visits*****--%></title>
<%@ page import ="com.velos.eres.service.util.*,java.util.*, java.io.*, org.w3c.dom.*"%>
<%@ page language = "java" import ="com.velos.eres.service.util.StringUtil,com.velos.esch.business.common.*,java.net.URLEncoder,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB"%>
<jsp:useBean id="sess" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8">
<style>
.yui-skin-sam .yui-dt th.hiddenHeader{ 
	display: none;
}
.yui-skin-sam .yui-dt th.yui-dt-hidden,
.yui-skin-sam .yui-dt tr.yui-dt-odd .yui-dt-hidden,
.yui-skin-sam .yui-dt tr.yui-dt-even .yui-dt-hidden {
border:0px;
}
</style>
<%
String src;
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<jsp:include page="ui-include.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<script Language="javascript">

var screenWidth = screen.width;
var screenHeight = screen.height;

function validateNumber(evt){
	if(evt.shiftKey) return false;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	//delete, backspace & arrow keys
	if (charCode == 8 || charCode == 46 || charCode == 37 || charCode == 39)
		return true;

	if (charCode > 31 && (!((47 < charCode && charCode  < 58) || (95 < charCode &&  charCode < 106))))
		return false;
	return true;
}
</script>
</head>
<body class="yui-skin-sam yui-dt yui-dt-liner" style="overflow:hidden;">
<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1100;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<%
HttpSession tSession = request.getSession(true);
StringBuffer urlParamSB = new StringBuffer();
String protocolId=request.getParameter("protocolId");
String calStatus="";
int pageRight=0;

if (sess.isValidSession(tSession)){
	String duration= "";
	String newDuration = "";

	newDuration = (String) tSession.getAttribute("newduration");

	if (StringUtil.isEmpty(newDuration))
	{
		duration =   request.getParameter("duration");
	}
	else
	{
		duration = newDuration ;
	}
	String calledFrom = request.getParameter("calledFrom");
	String tab = request.getParameter("selectedTab");
	String calAssoc=request.getParameter("calassoc");
	calAssoc=(calAssoc==null)?"":calAssoc;
	//Get calender status Description
	calStatus=request.getParameter("calStatus");
	 String calStatDesc = "";
	   SchCodeDao schcodedao = new SchCodeDao();	   
	   
	   if (calledFrom.equals("P") || calledFrom.equals("L")) {
		   calStatDesc = schcodedao.getCodeDescription(schcodedao.getCodeId("calStatLib",calStatus));
	   }else{
		   calStatDesc = schcodedao.getCodeDescription(schcodedao.getCodeId("calStatStd",calStatus));
	   }
	   
	// Fetching the Page rights
	
	if (calledFrom.equals("S")) {
	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
	   if ((stdRights.getFtrRights().size()) == 0){
			pageRight= 0;
	   }else{
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
	   }
	} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
	}
    urlParamSB = new StringBuffer();
	urlParamSB.append("protocolId=").append(StringUtil.htmlEncodeXss(protocolId))
	.append("&calledFrom=").append(StringUtil.htmlEncodeXss(calledFrom))
	.append("&duration=").append(StringUtil.htmlEncodeXss(duration))
	.append("&pageRight=").append(pageRight)
	.append("&fromPage=").append(StringUtil.htmlEncodeXss(request.getParameter("fromPage")))
	.append("&calassoc=").append(StringUtil.htmlEncodeXss(calAssoc))
	.append("&calstatus=").append(StringUtil.htmlEncodeXss(request.getParameter("calStatus")))
	.append("&mode=").append(StringUtil.htmlEncodeXss(request.getParameter("mode")))
	.append("&calProtocolId=").append(StringUtil.htmlEncodeXss(protocolId))
	;

%>
<script type="text/javascript">
//Added method to implement YUI for 'Manager Visits' tab of study.
function reloadManageVisitSGrid() {
	
	$("manageVisitsGrid").innerHTML = "";
	YAHOO.example.manageVisitsGrid = function() {
		var args = {
			urlParams: "<%=urlParamSB.toString()%>",
			dataTable: "manageVisitsGrid"
		};
	
    	myManageVisitsGrid = new VELOS.manageVisitsGrid('fetchVisitsJSON.jsp', args);
    	myManageVisitsGrid.startRequest();
    }();
}
YAHOO.util.Event.addListener(window, "load", function() {
	reloadManageVisitSGrid();
});
</script>
	<DIV class="BrowserTopn" id = "div1">
		<jsp:include page="protocoltabs.jsp" flush="true">
		<jsp:param name="calassoc" value="<%=StringUtil.htmlEncodeXss(calAssoc)%>" />
		<jsp:param name="selectedTab" value="<%=StringUtil.htmlEncodeXss(tab)%>" />
		</jsp:include>
	</DIV>
	<% if(protocolId == "" || protocolId == null || protocolId.equals("null") || protocolId.equals("")) {
		 %>
		<DIV class="BrowserBotN BrowserBotN_CL_1">
		  <jsp:include page="calDoesNotExist.jsp" flush="true"/>
		</DIV>
		  <%

	}else { %>
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="BrowserBotN BrowserBotN_CL_1" id="div2" style="height:77%">')
	else
		document.write('<DIV class="BrowserBotN BrowserBotN_CL_1" id="div2">')
</SCRIPT>
<input type="hidden" id ="pageRight" name="pageRight" value="<%=pageRight%>"/>
<table width="99%" cellspacing="0" cellpadding="0" border="0" class="midalign">
<tr height="5"></tr>
<tr> 
<td width="15%">
        <%if ((calStatus.equals("A")) || (calStatus.equals("F"))|| (calStatus.equals("D")))
		{%>
	       <FONT class="Mandatory"><%=MC.M_ChgCnt_IntDetsCalStat%><%--Changes cannot be made to Visit Interval and Visit Details for a Calendar with status*****--%> <%=calStatDesc %></Font>&nbsp;&nbsp;&nbsp;</td>
		<%} else{%>
			<P class="defComments custom-defComments"><%=LC.L_Cal_Status%><%--Calendar Status*****--%>: <%=calStatDesc%></P>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		  <%if (isAccessibleFor(pageRight, 'N')||isAccessibleFor(pageRight, 'E')){%> 
  			</td>
  			<td width="10%"><FONT class='Mandatory'>(<%=MC.M_ClkRow_ToEdt%><%--Click row to edit*****--%>)</FONT>&nbsp;&nbsp;&nbsp;&nbsp;</td>
  		<%}%>
		<%}%>
	<td width="15%">
		<button id="save_changes" name="save_changes" type="submit"
		<%if ((!isAccessibleFor(pageRight, 'N')) && (!isAccessibleFor(pageRight, 'E'))){%>
			disabled style="visibility:hidden"
		<%} %>
		<%if ((calStatus.equals("A")) || (calStatus.equals("F"))|| (calStatus.equals("D")))
		{%>
			disabled style="visibility:hidden"
		<%} %>
    	onclick="if(f_check_perm_noAlert(<%=pageRight%>,'N') || f_check_perm_noAlert(<%=pageRight%>,'E')){VELOS.manageVisitsGrid.saveDialog(event);}"><%=LC.L_Preview_AndSave%><%--Preview and Save*****--%></button>
   </td>
<td align="right">
     <%if ((isAccessibleFor(pageRight, 'N'))){%>
      <%if (!(calStatus.equals("A") || calStatus.equals("F") || calStatus.equals("D"))){%>
  		<%=LC.L_Add%><%--Add*****--%> &nbsp;<input id="rowCount" type="text" value="0" size="2" maxlength="2" class="index"
  		onkeydown = "return validateNumber(event);"/> <%=LC.L_New_Visit_S%><%--New Visit(s)*****--%>
		<span>
			<img id="addRows" name="addRows" title="<%=LC.L_Add%>" src="../images/add.png"
			<%
			String noTimePointdefined = schcodedao.getCodeDescription(schcodedao.getCodeId("timepointtype", "NTPD"));
			int ntpd = schcodedao.getCodeId("timepointtype", "NTPD");
			%>
	    	onclick="if (f_check_perm(<%=pageRight%>,'N')) {if(eval(document.getElementById('rowCount').value) > 20){ alert('<%=MC.M_AddMax_20Rows%><%--Please add maximum 20 rows at a time.*****--%>'); return;} if(eval(document.getElementById('rowCount').value) > 0){ VELOS.manageVisitsGrid.addRows('<%=noTimePointdefined %>','<%=ntpd %>'); }}"/> 
		</span> 
		<%} %>
		<span width="50%">&nbsp;</span>
			<%} %>
  	</td>
 </tr>
<tr height="5"></tr>
</table>
<input type="hidden" id="calassoc" value="<%=calAssoc%>"/>
<input type="hidden" id="calledFrom" value="<%=calledFrom%>"/>
<hr></hr>
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<div id="mvgrid" style="height: 82%; width:100%;">')
	else
		document.write('<div id="mvgrid" style="height: 88%; width:100%;">')
</SCRIPT>

<div id="manageVisitsGrid" style="padding-bottom:10px; width:100%;"></div>
</div>
</DIV>
<%

}
}
//end of session
else{
%>

<jsp:include page="timeout.html" flush='true'></jsp:include>
<%}%>

	<div class = "myHomebottomPanel">
	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</body>
<script>
//to Set the grid scroll bars
$j(document).ready(function(){
    $j('#mvgrid').css({'overflow':'auto'});
});
</script>
</html><%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>
