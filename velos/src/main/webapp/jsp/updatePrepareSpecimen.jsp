<%@page import="com.velos.eres.business.common.SpecimenDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=MC.M_Updating_PrepareSpecimen%><%--Updating Prepare Specimen*****--%></title>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<body>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 
  <%@page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.web.specimen.*,java.util.*"%>

  <%

	String eSign = request.getParameter("eSign");
   String selPks="";
	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))

   {
     %>
     <jsp:include page="sessionlogging.jsp" flush="true"/>

     <%

   	String oldESign = (String) tSession.getValue("eSign");

	if((!oldESign.equals(eSign)) && (request.getParameter("prepflag")==null?"":request.getParameter("prepflag")).equals(""))
	{
%>      
       <!-- Ashu:Fixed bug#5826 (11Mar2011).-->
  	   <p class = "successfulmsg" align = center><font color = #FF0000> <%=MC.M_Etr_WrongEsign%><%--You have entered a wrong e-Signature.*****--%></font> </p>

<%
	}
	else if(!(request.getParameter("prepflag")==null?"":request.getParameter("prepflag")).equals("")){
		String fk_schevents1=request.getParameter("fk_schevents1")==null?"":request.getParameter("fk_schevents1");
		String pkStorageKit=request.getParameter("pkStorageKit")==null?"":request.getParameter("pkStorageKit");
		String  fieldActionId=request.getParameter("fieldActionId")==null?"":request.getParameter("fieldActionId");
		String ipAdd = (String) tSession.getValue("ipAdd");
	    String usr = (String) tSession.getValue("userId");
	    String  fieldActionIdAp=request.getParameter("fieldActionIdAp")==null?"":request.getParameter("fieldActionIdAp");
		System.out.println("fieldActionIdAp="+fieldActionIdAp);
	    SpecimenDao prepSample=new SpecimenDao();
				
		int ret = prepSample.prepareSample(EJBUtil.stringToNum(fk_schevents1),EJBUtil.stringToNum(pkStorageKit),fieldActionId,ipAdd,EJBUtil.stringToNum(usr) );
		if(!fieldActionIdAp.equals("")){
			prepSample.updateColNdCurQty(fieldActionIdAp,fk_schevents1);
		}
		if (ret == 0) { %>
	      <p class = "successfulmsg" align = center> <%=MC.M_Specimen_PreparedSucc%><%--Specimen Prepared Successfully*****--%> 
	      <% } else if(ret == -2) { %>
		  <p class = "successfulmsg" align = center> <%=MC.M_Spmen_NotPrepared%><%--Specimen Not Prepared Successfully*****--%> </p>
	      <SCRIPT>
				window.opener.location.reload();
	           	setTimeout("self.close()",1000);
	      </SCRIPT>
	<%
		  }
	}
	else
  	{
		HashMap params = new HashMap();				
		params.put("userId", StringUtil.trueValue((String)tSession.getValue("userId")));
		params.put("ipAdd", StringUtil.trueValue((String)tSession.getValue("ipAdd")));			
		ManageCart.paramMap = params;		
		String ipAdd = (String) tSession.getValue("ipAdd");
		String printIndices = request.getParameter("printIndices");
		String indices[] = StringUtil.strSplit(printIndices,"[INDEXSEP]");
    	String usr = null;
    	int ret= -2;
	    usr = (String) tSession.getValue("userId");
		String accId = (String) tSession.getValue("accountId");		
		params.put("accountId", StringUtil.trueValue(accId));
		PreparationCartDto preparationCartDto = (PreparationCartDto)tSession.getAttribute("aPreparationCartDto");
		if(preparationCartDto !=null && preparationCartDto.getPkStorageList().size()!=0){			 		 				 
				 selPks = ManageCart.prepareSpecimen(preparationCartDto,Arrays.asList(indices));			
			 }		
		ret=1;		 	
	   if (ret > 0) { %>
      <p class = "successfulmsg" align = center> <%=MC.M_Specimen_PreparedSucc%><%--Specimen Prepared Successfully*****--%> 
      <% if (indices !=null && indices.length >0 )  {%>
     <A href="#" onClick="return printLabelWin('<%=selPks%>');">&nbsp;<%=LC.L_Print_Labels%><%--Print Labels*****--%></A> </p><%} %>
     <input type="hidden" value="<%=selPks%>" name="paramVals"><!--  YK 16MAR Enhancement No.3(Print Label Link Error) -->
      <% } else if(ret == -2) { %>
	  <p class = "successfulmsg" align = center> <%=MC.M_Spmen_NotPrepared%><%--Specimen Not Prepared Successfully*****--%> </p>
      <SCRIPT>
			//window.opener.location.reload();
           	//setTimeout("self.close()",1000);
      </SCRIPT>

<%
	  }
%>
<%	  
  }
   }	
else{
%>	
<jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</BODY>
</HTML>