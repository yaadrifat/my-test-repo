##### Moment Js #####
Parse, validate, manipulate, and display dates in JavaScript.

Source: http://momentjs.com/

Documentation: http://momentjs.com/docs/#/use-it/other/

License: MIT license