var milestoneRowChangeList = [];
var milestoneRowChangeListRemainder = null;
var milestoneDataChangeList = null;
var milePurgeList = [];
var purgedMileArray = null;
var purgedMileArrayRemainder = null;
var milestoneArray = null;
var mileDescArray = null; //milestone description
var incomingUrlParams = null;
var calStatus = null;
var lastTimeSubmitted = 0;
var horizontalPage = 0;
var mileCount = 0;
var firstAMSeq = 0;
var lastAMSeq = 0;
var showChangeRowsContentHTML='';
var myPMGrid, myVMGrid, myEMGrid, mySMGrid, myAMGrid,myPMParentGrid, myVMParentGrid, myEMParentGrid, mySMParentGrid, myAMParentGrid;
var myMileTypeJSON;

var myMiletypeIds = [];
var myMiletypes = [];

var myPaytypeIds = [];
var myPaytypes = [];
var myPayforIds = [];
var myPayfors = [];
var myMileStatusIds = [];
var myMileStatuses = [];

var myMileDifStatusIds = [];
var myMileDifStatuses = [];
var myMileStatusIdsRl = [];
var myMileStatusesRl = [];


var mileStatusSubTypes = [];

var allMileStatusIds = [];
var allMileStatusDesc = [];

var allMileStatus;
var allMileStatusOpen=[];
var depIds = []; 
var depNames =[];

var VMvisitIds = []; 
var VMvisitNames =[];

var myEMruleIds = [];
var myEMrules = [];
var myVMruleIds = [];
var myVMrules = [];
var patStatus = [];
var patStatusIds = [];
var studyStatus = [];
var studyStatusIds = [];
var eventStatus = [];
var eventStatusIds = []; 
//Parminder Singh : Bug#14939
var mileIdStatusPM = [];
var mileIdStatusVM = [];
var mileIdStatusEM = [];
var mileIdStatusSM = [];
var mileIdStatusAM = [];

var mileFieldsList = [];

var myCalendarIds = [];
var myCalendars = [];
var myCalendarTypes = [];
var myVisitIds = [];
var myVisits = [];
var myEventIds = [];
var myEvents = [];

var myFieldArray = [];
var myColumnDefs = [];

var myParentFieldArray = [];
var myParentColumnDefs = [];


var updateRows = [];
var updateRowsRemainder = null;

var pgRight=0;

var respJSON;
var defaultPayTypeId, defaultPayType;
//INF-22500: Yogendra
var selectedGridPM;
var selectedGridVM;
var selectedGridEM;
var selectedGridSM;
var selectedGridAM;
var selectAllFlag=false;
//APR-13-11,BK ,FIXED #6016
var PMrecordNum,VMrecordNum,EMrecordNum,SMrecordNum,AMrecordNum;
//INF-22500
var optedGridPM;
var optedGridVM;
var optedGridEM;
var optedGridSM;
var optedGridAM;
var numCheckValid = false;
var valValid = false;
function includeJS(file)
{
  var script  = document.createElement('script');
  script.src  = file;
  script.type = 'text/javascript';
  script.defer = true;
  document.getElementsByTagName('head').item(0).appendChild(script);
}
includeJS('js/velos/velosUtil.js');

//IE does not support Array indexOf(); handle it here
if (Array.prototype.indexOf === undefined) {
	Array.prototype.indexOf = function(myObj, startIndex) {
		for (var iX = (startIndex || 0), jX = this.length; iX < jX; iX++) {
			if (this[iX] === myObj) { return iX; }
		}
		return -1;
	};
}

var Total_Chg=L_Total_Chg;
var ErrGetResp_CnctVelos=M_ErrGetResp_CnctVelos;
var ValEtrFwg_2Dgtholdback=M_ValEtrFwg_2Dgtholdback;
var ValEtrFwg_11DgtPatCnt=M_ValEtrFwg_11DgtPatCnt;
var ValEtrInt_11DgtLessValid=M_ValEtrInt_11DgtLessValid;
var ValEtrFwg_AmtFmt11Dgt=M_ValEtrFwg_AmtFmt11Dgt;
var ValEtrCrit_MstoneDescValid=	M_ValEtrCrit_MstoneDescValid;
var ValEtrCrit_4kCharPlsEtr=M_ValEtrCrit_4kCharPlsEtr;
var Preview_AndSave=L_Preview_AndSave;
var CommErr_CnctVelos=M_CommErr_CnctVelos;
var PlsEnterEsign=M_PlsEnterEsign;
var CldNtCont_InvldIndx=M_CldNtCont_InvldIndx;
var ThereNoChg_Save=M_ThereNoChg_Save;
var Esignature=L_Esignature;
var Valid_Esign=L_Valid_Esign;
var Invalid_Esign=L_Invalid_Esign;
var CnfrmStdLvl_Change=M_CnfrmStdLvl_Change;
var ValEtrFwg_11DgtLessValid=M_ValEtrFwg_11DgtLessValid;
var Pat_StatusMstones=L_Pat_StatusMstones;
var PleaseWait_Dots=L_PleaseWait_Dots;
var LoadingPlsWait=L_LoadingPlsWait;
var Visit_Mstones=L_Visit_Mstones;
var Evt_Mstones=L_Evt_Mstones;
var StdPat_Mstone=L_StdPat_Mstone;
var Addl_Mstones=L_Addl_Mstones;
var CnfrmMstone_Change=M_CnfrmMstone_Change;
var Pat_Count=L_Pat_Count;
var Patient_Status=L_Patient_Status;
var Mstone_Status=L_Mstone_Status;
var Calendar=L_Calendar;
var Visit=L_Visit;
var Event=L_Event;
var Milestone_Rule=L_Milestone_Rule;
var Event_Status=L_Event_Status;
var Study_Status=L_Study_Status;
var Milestone_Description=L_Milestone_Description;
var Additional=L_Additional;
var NoRec_ChldGrid = M_NoRec_ChldGrid;
var PleaseSelectMile = M_PleaseSelectMile;
var saveDropDown=true;
var globalTd;
var globalColumnKey;
var	globalORecord;
var	globalrecnum;

var holdbackToAll=0;
// Define VELOS.milestoneGrid object. "VELOS" is a namespace so that the object can be called globally
VELOS.milestoneGrid = function (url, args) {	
	pgRight = parseInt(YAHOO.util.Dom.get("pageRight").value);
	this.url = url;  // save it in this.url; later it will be sent in startRequest()
	incomingUrlParams = args.urlParams; // this will be sent to updateEvtVisits.jsp
	if (incomingUrlParams) {
		incomingUrlParams = incomingUrlParams.replace("'", "\"");
	}

	showPanel(); // Show the wait panel; this will be hidden later

	// Define handleSuccess() of VELOS.milestoneGrid object. This will be added to the callback object to be
	// used when the call is processed successfully.
	this.handleSuccess = function(o) {		
		/* Response Object Fields:
		 * o.tId, o.status, o.statusText, o.getResponseHeader[],
		 * o.getAllResponseHeaders, o.responseText, o.responseXML, o.argument
		 */
        this.dataTable = args.dataTable ? args.dataTable : 'serverpagination'; // name of the div to hold milestoneGrid
        holdbackToAll = args.holdbackToAll ? args.holdbackToAll : 0;    //Holdback Value - FIN-22301 : Raviesh
        var pMileTypeId;
        pMileTypeId = args.pMileTypeId;
        var isSearch=args.isSearch=='Y' ?true:false;
        var clickedOnMile=args.clickedOnMile=='Y'?true:false;
		var respJ = null; // response in JSON format
		try {	
			respJ = $J.parse(o.responseText);
			respJSON = respJ;
		} catch(e) {
			alert(ErrGetResp_CnctVelos);/*alert('Error getting response from server. Please contact Velos Support.');*****/
			return;
		}
		if (respJ.error < 0) {
			var paramArray = [respJ.errorMsg];
			alert(getLocalizedMessageString("L_Error_Msg",paramArray));/*alert('Error Msg: '+respJ.errorMsg);*****/
			return;
		}

		myMileTypeJSON = respJ.mileTypeJSON;
		myMileTypeIds = respJ.milestoneTypeId;
		myMileTypes = respJ.milestoneType;
		
		defaultPayTypeId = respJ.defaultPayTypeId;
		defaultPayType = respJ.defaultPayType;	
		
		myCalendarIds = respJ.studyProtId;
		myCalendars = respJ.studyProtocols;
		myCalendarTypes = respJ.studyProtTypes;
		
		myVisitIds = respJ.studyVisitId;
		myVisits = respJ.studyVisits;
		myEventIds = respJ.studyEventId;
		myEvents = respJ.studyEvents;
		
		myPaytypesIds = respJ.milePayTypeId;
		myPaytypes = respJ.milePayType;
		myPayforIds = respJ.milePayForId;
		myPayfors = respJ.milePayFor;
		myMileStatusIds = respJ.mileStatusId;
		myMileStatuses = respJ.mileStatus;

		myMileStatusIdsRl = respJ.mileStatusIdRl;
		myMileStatusesRl = respJ.mileStatusRl;
		
		
		mileStatusSubTypes = respJ.mileStatusSubType;
		
		allMileStatus = respJ.jsAllMilestoneStats;
		myEMruleIds = respJ.EM_mileRuleId;
		myEMrules =  respJ.EM_mileRule;
		myVMruleIds = respJ.VM_mileRuleId;
		myVMrules = respJ.VM_mileRule;
		patStatusIds = respJ.patStatusId;
		patStatus = respJ.patStatus;
		studyStatusIds = respJ.studyStatusId;
		studyStatus = respJ.studyStatus;
		eventStatusIds = respJ.eventStatusId;
		eventStatus = respJ.eventStatus;
		
	    mileCount = respJ.mileCount;	
	    
	    mileCount = respJ.mileCount;
	    if(allMileStatusOpen.length>0){
	    	for(var iX=0;iX<allMileStatus.length;iX++){
	    	var count = 0;
	    	var newMileStat;
	    	var newMileId =allMileStatus[iX].mileStatusId;
	    	for(var iM=0;iM<allMileStatusOpen.length;iM++){
				var prevMileId =allMileStatusOpen[iM].mileStatusId;
				if(newMileId==prevMileId){
					count++;
					newMileStat = allMileStatus[iX].mileStatuses;
					break;
					}
	    		}
	    	if(count>0){
	    		allMileStatusOpen[iM].mileStatuses = newMileStat;
	    		}
	    	else{
	    		allMileStatusOpen.push(allMileStatus[iX]);
	    		}
	    	}
	    }
	    else
	    {
	    	for(var iX=0;iX<allMileStatus.length;iX++){
	    		allMileStatusOpen.push(allMileStatus[iX]);
	    	}
	    }

		var maxWidth = 800;
		var maxHeight = 800;
		/*YK(05Aug11): Fix for Bug #6760 to resolve width issue*/
		if (screen.availWidth >= 800 && screen.availWidth < 900) { maxWidth = 800; }
		else if (screen.availWidth >= 1000 && screen.availWidth < 1100) { maxWidth = 900; }
		else if (screen.availWidth >= 1100 && screen.availWidth < 1290) { maxWidth = 1180; }
		else if (screen.availWidth >= 1290 ) { maxWidth = 1280; }

		if (screen.availHeight >= 700 && screen.availHeight < 900) { maxHeight = 320; }
		else if (screen.availHeight >= 900 && screen.availHeight < 1000) { maxHeight = 480; }

		
		var calcHeight =0; 
		var pmMileIdCells;var amMileIdCells;var vmMileIdCells;var emMileIdCells;var smMileIdCells;
		var pmParentMileIdCells;var amParentMileIdCells;var vmParentMileIdCells;var emParentMileIdCells;var smParentMileIdCells;
		/*YK 03Aug11:Code removed (reloadMilestoneGrid('ALL')) for  Enhancement FIN-20073, Now not in use*/
		
		//Modified to get Milestone Power Bar Grid values - INF-22500 : Raviesh
		mileTypeId = pMileTypeId;
		if (mileTypeId=='PM'){			
			colArray = respJ.PM_colArray;
			colArrayParent = respJ.parentPMColArray;
			dataArray = respJ.PM_dataArray;
			dataParentArray = respJ.parentPMDataArray;
			PMrecordNum = respJ.PMmileCount;
			//alert("PM case:"+recordNum);
		} else if (mileTypeId == 'VM'){
			colArray = respJ.VM_colArray;
			colArrayParent = respJ.parentVMColArray;
			dataArray = respJ.VM_dataArray;
			dataParentArray = respJ.parentVMDataArray;
			VMrecordNum = respJ.VMmileCount;
		} else if (mileTypeId == 'EM'){
			colArray = respJ.EM_colArray;
			colArrayParent = respJ.parentEMColArray;
			dataArray = respJ.EM_dataArray;
			dataParentArray = respJ.parentEMDataArray;
			EMrecordNum = respJ.EMmileCount;
		} else if (mileTypeId == 'SM'){
			colArray = respJ.SM_colArray;
			colArrayParent = respJ.parentSMColArray;
			dataArray = respJ.SM_dataArray;
			dataParentArray = respJ.parentSMDataArray;
			SMrecordNum = respJ.SMmileCount;
		} else if (mileTypeId == 'AM'){
			colArray = respJ.AM_colArray;
			colArrayParent = respJ.parentAMColArray;
			dataArray = respJ.AM_dataArray;
			dataParentArray = respJ.parentAMDataArray;
			AMrecordNum = respJ.AMmileCount;
		}
		
		
		 //added because we need to flush out all the changes when user click on search
		if(isSearch){
			milestoneRowChangeList = [];
			purgedMileArray = [];
			updateRows =[];
		}
		var otherRegularExp =  new RegExp('mileSeq[0-9]+$');
		for (oKey in purgedMileArrayRemainder){
			if (oKey.match(otherRegularExp) && purgedMileArrayRemainder[oKey] != undefined) {
				purgedMileArray[oKey] = purgedMileArrayRemainder[oKey];
			}
		}
		for (oKey in milestoneRowChangeListRemainder){
			if (oKey.match(otherRegularExp) && milestoneRowChangeListRemainder[oKey] != undefined) {
				milestoneRowChangeList[oKey] = milestoneRowChangeListRemainder[oKey];
			}
		}
		for (oKey in updateRowsRemainder){
			if (oKey.match(otherRegularExp) && updateRowsRemainder[oKey] != undefined) {
				updateRows[oKey] = updateRowsRemainder[oKey];
			}
		}
		
		if (colArray){
		
			calcHeight = dataArray.length*40 + 40;
			if (calcHeight > maxHeight) { calcHeight = maxHeight; }	
			
			myColumnDefs = VELOS.milestoneGrid.processColumns(mileTypeId, respJ, colArray);
			
			var myDataSource = new YAHOO.util.DataSource(dataArray);
			myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
			myDataSource.responseSchema = {
				fields: myFieldArray
			};
			var myGrid = new YAHOO.widget.DataTable(
				mileTypeId +'_'+ this.dataTable,
				myColumnDefs, myDataSource,
				{
					width:maxWidth+"px", 
					sortedBy : {
				        key: "recNum",
				        dir: YAHOO.widget.DataTable.CLASS_ASC
				    }
					//height:calcHeight+"px",
					//caption:'my'+mileTypeId+'Grid'
					//caption:"DataTable Caption",
					//scrollable:true
				}
			);
			var tabLength= myGrid.getRecordSet().getLength();

		

			if (mileTypeId=='PM'){			
				myPMGrid = myGrid;
				selectedGridPM=myGrid;
				pmMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myGrid.getTableEl());	
				VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(pmMileIdCells,mileTypeId);
				//FIN-19817 27-Sep-2012 Hari
				VELOS.formatFields('amount',pmMileIdCells);
				//VELOS.formatFields('limit',pmMileIdCells);
				
				
				if(holdbackToAll>0)
					VELOS.applyHoldBack(myGrid,pmMileIdCells,holdbackToAll);
				VELOS.formatHoldback('holdBack',pmMileIdCells);
				myPMGrid.focus();
			} else if (mileTypeId == 'VM'){
				myVMGrid = myGrid;
				selectedGridVM=myGrid;
				vmMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myGrid.getTableEl());
				VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(vmMileIdCells,mileTypeId);
				//FIN-19817 27-Sep-2012 Hari
				VELOS.formatFields('amount',vmMileIdCells);
				//VELOS.formatFields('limit',vmMileIdCells);
				if(holdbackToAll>0)
					VELOS.applyHoldBack(myGrid,vmMileIdCells,holdbackToAll);
				VELOS.formatHoldback('holdBack',vmMileIdCells);
				myVMGrid.focus();
			} else if (mileTypeId == 'EM'){
				myEMGrid = myGrid;
				selectedGridEM=myGrid;
				emMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myGrid.getTableEl());
				VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(emMileIdCells,mileTypeId);
				//FIN-19817 27-Sep-2012 Hari
				VELOS.formatFields('amount',emMileIdCells);
				//VELOS.formatFields('limit',emMileIdCells);
				if(holdbackToAll>0)
					VELOS.applyHoldBack(myGrid,emMileIdCells,holdbackToAll);
				VELOS.formatHoldback('holdBack',emMileIdCells);
				myEMGrid.focus();
			} else if (mileTypeId == 'SM'){
				mySMGrid = myGrid;
				selectedGridSM=myGrid;
				smMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myGrid.getTableEl());
				VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(smMileIdCells,mileTypeId);
				//FIN-19817 27-Sep-2012 Hari
				VELOS.formatFields('amount',smMileIdCells);
				//VELOS.formatFields('limit',smMileIdCells);
				if(holdbackToAll>0)
					VELOS.applyHoldBack(myGrid,smMileIdCells,holdbackToAll);
				VELOS.formatHoldback('holdBack',smMileIdCells);
				mySMGrid.focus();
			} else if (mileTypeId == 'AM'){
				myAMGrid = myGrid;
				selectedGridAM=myGrid;
				amMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myGrid.getTableEl());
				VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(amMileIdCells,mileTypeId);
				//FIN-19817 27-Sep-2012 Hari
				VELOS.formatFields('amount',amMileIdCells);
				//VELOS.formatFields('limit',amMileIdCells);
				if(holdbackToAll>0)
					VELOS.applyHoldBack(myGrid,amMileIdCells,holdbackToAll);
				VELOS.formatHoldback('holdBack',amMileIdCells);
				myAMGrid.focus();
			}		
	  }
		
		//Added for Milestone Power Bar Grid - INF-22500 : Raviesh
		if (colArrayParent){
			
			calcHeight = dataParentArray.length*40 + 40;
			if (calcHeight > maxHeight) { calcHeight = maxHeight; }	
			
			mileTypeId = mileTypeId + "_Parent";
			
			myParentColumnDefs = VELOS.milestoneGrid.processParentColumns(mileTypeId, respJ, colArrayParent);
			
			var myParentDataSource = new YAHOO.util.DataSource(dataParentArray);
			myParentDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
			myParentDataSource.responseSchema = {
				fields: myParentFieldArray
			};			
			
			var myParentGrid = new YAHOO.widget.DataTable(
					mileTypeId +'_'+ this.dataTable,
					myParentColumnDefs, myParentDataSource,
					{
						width:maxWidth+"px"//, 
						//height:calcHeight+"px",
						//caption:'my'+mileTypeId+'Grid'
						//caption:"DataTable Caption",
						//scrollable:true
					}
				);	

			if(mileTypeId== 'PM_Parent'){
				myPMParentGrid = myParentGrid;
				optedGridPM = myParentGrid;
				pmParentMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myParentGrid.getTableEl());
				VELOS.formatFields('amount',pmParentMileIdCells);
				//VELOS.formatFields('limit',pmParentMileIdCells);
				myPMParentGrid.focus();
			}
			else if(mileTypeId== 'VM_Parent'){
				myVMParentGrid = myParentGrid;
				optedGridVM = myParentGrid;
				vmParentMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myParentGrid.getTableEl());
				myVMParentGrid.focus();
			}
			else if(mileTypeId== 'EM_Parent'){
				myEMParentGrid = myParentGrid;
				optedGridEM = myParentGrid;
				emParentMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myParentGrid.getTableEl());
				myEMParentGrid.focus();
			}
			else if(mileTypeId== 'SM_Parent'){
				mySMParentGrid = myParentGrid;
				optedGridSM = myParentGrid;
				smParentMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myParentGrid.getTableEl());
				mySMParentGrid.focus();
			}
			else if(mileTypeId== 'AM_Parent'){
				myAMParentGrid = myParentGrid;
				optedGridAM = myParentGrid;
				amParentMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myParentGrid.getTableEl());
				myAMParentGrid.focus();
			}
	  }

		
	//FIX #6283
	if (YAHOO.util.Dom.get("PM_rowCount")){
		YAHOO.util.Dom.get("PM_rowCount").value="0";
	}
	if (YAHOO.util.Dom.get("VM_rowCount")){
		YAHOO.util.Dom.get("VM_rowCount").value="0";
	}
	if (YAHOO.util.Dom.get("EM_rowCount")){
		YAHOO.util.Dom.get("EM_rowCount").value="0";
	}
	if (YAHOO.util.Dom.get("SM_rowCount")){
		YAHOO.util.Dom.get("SM_rowCount").value="0";
	}
	if (YAHOO.util.Dom.get("AM_rowCount")){
		YAHOO.util.Dom.get("AM_rowCount").value="0";
	}
	
		
		mileIdArray = []; 	mileSeqArray = []; 		checkItemArray = [];
		milestoneArray = [];		
		delButtonArray = []; mileDescArray = [];
		
		if (!purgedMileArray) purgedMileArray = [];
		if (!milestoneRowChangeList) milestoneRowChangeList = [];
		if (!updateRows) updateRows = [];
		
		milestoneRowChangeListRemainder =[]; purgedMileArrayRemainder = [];
		updateRowsRemainder =[];

		var onMouseOver = function(oArgs) {
			this.onEventHighlightRow(oArgs);
		}
		
		var onMouseOut = function(oArgs) {
			this.onEventUnhighlightRow(oArgs);
			return nd();
		}

		// I subscribe to the cellClickEvent to respond to the action icons and, if none, to pop up the cell editor
		var onCellClick = function(oArgs) {
			var target = oArgs.target;
			var column = this.getColumn(target);
			var oRecord = this.getRecord(target);
			var mileSeq = oRecord.getData('mileSeq');
			if(column.key == 'patCount' || column.key == 'limit' || column.key == 'holdBack' || column.key == 'holdBack_parent' || column.key == 'amount')
				numCheckValid = true;
			//for INF-22500
			var mileId =0;
			if(oRecord.getData('mileId')!=undefined)
			{mileId = parseInt(oRecord.getData('mileId'));}
			var mileTypeId = oRecord.getData('mileTypeId');
			var mileStatusId = oRecord.getData('mileStaticStatusId');
			var mileStatusSubType ="";

			// Bug #15722 :AGodara
			var recNum;
			if (column.key == 'datefrom' || column.key == 'dateto'){
				recNum = oRecord.getData('recNum');
				globalColumnKey = column.key;
				globalORecord = oRecord;
				globalTd = target;
				globalrecnum = recNum;
			}

			for (var ind=0; ind < myMileStatusIds.length; ind++){
				if (myMileStatusIds[ind] == mileStatusId){
					mileStatusSubType = mileStatusSubTypes[ind];
					ind = myMileStatusIds.length;
				}
			}

			//Bug#14942 Fixed : Raviesh
			if (column.key == 'recNum' || column.key == 'mileType')
				return;
			if (mileTypeId == 'VM' || mileTypeId == 'EM'){
			/*Ankit 16Aug2011 Bug#6788: 'Patient Count' and 'Patient Status' columns are not allowed to edit*/
				if (column.key == 'calendar' || column.key == 'visit' || column.key == 'event' ){
					if (mileId != 0){
						return;
					}
					var mileFrom = $j("#mileFrom").val();
					if(mileFrom=="manageMilestone" && column.key == 'calendar'){
						return;
					}
				}
				if (column.key == 'patCount' || column.key == 'patStatus'){
					if (oRecord.getData('calendarType')=='A')
						return;
					if (mileId != 0)
						return;
				}
			}
			if(mileTypeId==undefined)
			{
				if (column.key == 'patCount' || column.key == 'patStatus'){
					if (oRecord.getData('calendarType')=='A')
						return;
				}
			}
			switch(column.formatter){
			case 'customInteger':
				column.formatter = function(el, oRecord, oColumn, oData) {
					var mileTypeId = oRecord.getData('mileTypeId');
					//Modified for Milestone Power Bar Grid - INF-22500 : Raviesh
					var myGrid = (mileTypeId=='PM')? myPMGrid :
						(mileTypeId=='VM')? myVMGrid :
						(mileTypeId=='EM')? myEMGrid :
						(mileTypeId=='SM')? mySMGrid :
						(mileTypeId=='AM')? myAMGrid :
						(mileTypeId=='PM_Parent')? myPMParentGrid :
						(mileTypeId=='VM_Parent')? myVMParentGrid :
						(mileTypeId=='EM_Parent')? myEMParentGrid :
						(mileTypeId=='SM_Parent')? mySMParentGrid : myAMParentGrid;
					
					//precautionary change
					if (oData != '0'){
						if (oData != null && oData != ''){
							if (/^[0]+/g.test(oData)){//FIX #6272
								oData = oData.replace(/^[0]+/g,""); //remove leading zeros
							}
						}
					}
					//FIX #6048
					if ((oColumn.key == 'patCount' || oColumn.key == 'limit' || oColumn.key == 'holdBack' || oColumn.key == 'holdBack_parent')
						&& (oData == null || oData == '')){
						valValid = false;
						if (oColumn.key == 'patCount')
							oData = '';
						if (oColumn.key == 'limit')
							oData = '';
						if (oColumn.key == 'holdBack')
							oData = '0';
						if (oColumn.key == 'holdBack_parent')
							oData = '0';
						if (myGrid && myGrid._oCellEditor){
							myGrid._oCellEditor.value = oData;
						}
						el.innerHTML = oData;
						oRecord.setData(oColumn.key, oData);
						return;
					}
					if(oData == ''){
						if(oColumn.key != 'limit')
								oData ='0'; //If empty set 0
					}
					if (oData == '0'){
						valValid = false;							
						el.innerHTML = oData;
						oRecord.setData.oColumn= el.innerHTML;
						if (myGrid && myGrid._oCellEditor){
							myGrid._oCellEditor.value = oData;
						}
					} else {
						switch(oColumn.key){
							case 'patCount':
								if (/^-{0,1}\d{0,10}$/.test(oData) && oData >= -1){
									valValid = false;
									el.innerHTML = oData;
									oRecord.setData(oColumn.key, oData);
									if (myGrid && myGrid._oCellEditor){
										myGrid._oCellEditor.value = oData;
									}
								}else{
									if (myGrid && myGrid._oCellEditor){
										myGrid._oCellEditor.value = null;
									}
									valValid = true;
									alert(ValEtrFwg_11DgtPatCnt);/*alert("The value entered does not pass the following criteria:\n \n -'Patient Count' should be an integer.\n -'Patient Count' should be less than 11 digits.\n -'Patient Count' should not be less than -1.\n \nPlease enter a valid value.");*****/
								}
								break;
							case 'limit':
								if (/^\d{0,10}$/.test(oData)){
									valValid = false;
									el.innerHTML = oData;
									oRecord.setData(oColumn.key, oData);
									if (myGrid && myGrid._oCellEditor){
										myGrid._oCellEditor.value = oData;
									}
								}else{
									if (myGrid && myGrid._oCellEditor){
										myGrid._oCellEditor.value = null;
									}
									valValid = true;
									alert(ValEtrFwg_11DgtLessValid);/*alert("The value entered does not pass the following criteria:\n \n -'Limit' should be an integer.\n -'Limit' should be less than 11 digits.\n -'Limit' should not be less than 0.\n \nPlease enter a valid value.");*****/
								}
								break;
							case 'holdBack':
								if (/^100$|^\d{0,2}(\.\d{1,2})? *%?$/.test(oData)){
									valValid = false;
									el.innerHTML = oData;
									oRecord.setData(oColumn.key, oData);
									if (myGrid && myGrid._oCellEditor){
										myGrid._oCellEditor.value = oData;
									}
								}else{
									if (myGrid && myGrid._oCellEditor){
										myGrid._oCellEditor.value = null;
									}
									valValid = true;
									alert(ValEtrFwg_2Dgtholdback);/*alert("The value entered does not pass the following criteria:\n \n -'Patient Count' should be an integer.\n -'Patient Count' should be less than 11 digits.\n -'Patient Count' should not be less than -1.\n \nPlease enter a valid value.");*****/
								}
								break;
							case 'holdBack_parent':
								if (/^100$|^\d{0,2}(\.\d{1,2})? *%?$/.test(oData)){
									valValid = false;
									el.innerHTML = oData;
									oRecord.setData(oColumn.key, oData);
									if (myGrid && myGrid._oCellEditor){
										myGrid._oCellEditor.value = oData;
									}
								}else{
									if (myGrid && myGrid._oCellEditor){
										myGrid._oCellEditor.value = null;
									}
									valValid = true;
									alert(ValEtrFwg_2Dgtholdback);/*alert("The value entered does not pass the following criteria:\n \n -'Patient Count' should be an integer.\n -'Patient Count' should be less than 11 digits.\n -'Patient Count' should not be less than -1.\n \nPlease enter a valid value.");*****/
								}
								break;
							default:
								if (/^\d{0,11}$/.test(oData)){
									el.innerHTML = oData;
									oRecord.setData(oColumn.key, oData);
									if (myGrid && myGrid._oCellEditor){
										myGrid._oCellEditor.value = oData;
									}
								}else{
									if (myGrid && myGrid._oCellEditor){
										myGrid._oCellEditor.value = null;
									}
									alert(ValEtrInt_11DgtLessValid);/*alert("The value entered does not pass the following criteria:\n \n -'' should be an integer.\n -'' should be less than 11 digits.\n -'' should not be less than -1.\n \nPlease enter a valid value.");*****/
								}
								break;
						}
					}
				};
				if(mileTypeId!=undefined && mileTypeId.indexOf('Parent')==-1){
					if (mileId == 0){
						if (f_check_perm(pgRight,'N')){
							this.onEventShowCellEditor(oArgs);
						}
					}else{
						//FIX #6329
						if (f_check_perm(pgRight,'E') && (mileStatusSubType != 'A' && mileStatusSubType != 'IA')){
							this.onEventShowCellEditor(oArgs);
						}
					}
				}
				else
				{
					this.onEventShowCellEditor(oArgs);
				}
				return;
				break;
			case 'customNumber':
				column.formatter = function(el, oRecord, oColumn, oData) {									
					var mileTypeId = oRecord.getData('mileTypeId');
					//Modified for Milestone Power Bar Grid - INF-22500 : Raviesh
					var myGrid = (mileTypeId=='PM')? myPMGrid :
						(mileTypeId=='VM')? myVMGrid :
						(mileTypeId=='EM')? myEMGrid :
						(mileTypeId=='SM')? mySMGrid :
						(mileTypeId=='AM')? myAMGrid :
						(mileTypeId=='PM_Parent')? myPMParentGrid :
						(mileTypeId=='VM_Parent')? myVMParentGrid :
						(mileTypeId=='EM_Parent')? myEMParentGrid :
						(mileTypeId=='SM_Parent')? mySMParentGrid : myAMParentGrid;
					
					if (oData != '0'){
						if (oData != '' || oData != null){
							if (/^[0]+/g.test(oData)){//FIX #6272
								oData = oData.replace(/^[0]+/g,""); //remove leading zeros
							}
						}
					}
					if (/^\.\d{0,2}?$/.test(oData)){ // If no zero before decimal add one
						oData = "0" + oData;
					}

					if(oData == '') {
						valValid = false;
						oData ='0'; //If empty set 0
						//alert("oData empty");
					}
					if (oData == '0'){
						valValid = false;
						el.innerHTML = oData;
						oRecord.setData(oColumn.key, oData);
						if (myGrid && myGrid._oCellEditor){
							myGrid._oCellEditor.value = oData;
						}
						$j(el).formatCurrency({
				            region:appNumberLocale
				                   });
					} else {
						if (/^\d{0,11}([.]\d{0,2})?$/.test(oData)){
							valValid = false;
							el.innerHTML = oData;
							oRecord.setData(oColumn.key, oData);
							if (myGrid && myGrid._oCellEditor){
								myGrid._oCellEditor.value = oData;
							}
							$j(el).formatCurrency({
					            region:appNumberLocale
					                   });
						}else{
							if (myGrid && myGrid._oCellEditor){
								myGrid._oCellEditor.value = null;
							}
							valValid = true;
							alert(ValEtrFwg_AmtFmt11Dgt);/*alert("The value entered does not pass the following criteria:\n \n -'Amount' should be a number.\n -'Amount' should conform to format - ###########.## \n    i.e. max 11 digits followed by max 2 decimal places.\n \nPlease enter a valid value.");*****/
						}
					}
				};
				if(mileTypeId!=undefined && mileTypeId.indexOf('Parent')==-1){
					if (mileId == 0){
						if (f_check_perm(pgRight,'N')){
							this.onEventShowCellEditor(oArgs);
						}
					}else{
						//FIX #6329
						if (f_check_perm(pgRight,'E') && (mileStatusSubType != 'A' && mileStatusSubType != 'IA')){
							this.onEventShowCellEditor(oArgs);
						}
					}
				}
				else
				{
					this.onEventShowCellEditor(oArgs);
				}
				return;
				break;
			case 'customVarchar':
				column.formatter = function(el, oRecord, oColumn, oData) {
					var mileTypeId = oRecord.getData('mileTypeId');
					//Modified for Milestone Power Bar Grid - INF-22500 : Raviesh
					var myGrid = (mileTypeId=='PM')? myPMGrid :
						(mileTypeId=='VM')? myVMGrid :
						(mileTypeId=='EM')? myEMGrid :
						(mileTypeId=='SM')? mySMGrid :
						(mileTypeId=='AM')? myAMGrid :
						(mileTypeId=='PM_Parent')? myPMParentGrid :
						(mileTypeId=='VM_Parent')? myVMParentGrid :
						(mileTypeId=='EM_Parent')? myEMParentGrid :
						(mileTypeId=='SM_Parent')? mySMParentGrid : myAMParentGrid;
					
					if (/['"|]+/g.test(oData)){							
						//this._oCellEditor.value = null;
						alert(ValEtrCrit_MstoneDescValid);/*alert("The value entered does not pass the following criterion:\n \n - 'Milestone Description' should not contain following special characters: pipe(|), single quote('), double quote(\").\n \nPlease enter a valid value.");*****/
						return;
					}
					
					if (oData.length < 4001){							
						el.innerHTML = oData;
						oRecord.setData(oColumn.key, VELOS.milestoneGrid.decodeData(oData));							
						
						return;
					}else{	
						if (myGrid && myGrid._oCellEditor){
							myGrid._oCellEditor.value = null;
						}
						alert(ValEtrCrit_4kCharPlsEtr);/*alert("The value entered does not pass the following criterion:\n \n -'Milestone Description' length should not be greater than 4000 characters.\n \nPlease enter a valid value.");*****/
						return;
					}
					
				};
				//FIX #6329
				if (mileId == 0 || (f_check_perm(pgRight,'E') && (mileStatusSubType != 'A' && mileStatusSubType != 'IA'))){
					this.onEventShowCellEditor(oArgs);
				}
				return;
				break;
			default:
				//for checkItem access rights are checked in itemAll()
				if(mileTypeId!=undefined && mileTypeId.indexOf('Parent')==-1){
				if (mileId == 0){ //New Record
					if (f_check_perm(pgRight,'N')){
						if (column.key != 'mileType'){
							if (column.key == 'mileStatus'){
								/* Loads Milestone status for each Milestone **/
										allMileStatusDesc = myMileStatusesRl;
										allMileStatusIds = myMileStatusIdsRl;
								column.editor.dropdownOptions = allMileStatusDesc;
							}
							if (column.key == 'visit'){	
								VELOS.milestoneGrid.updateDependent(column,oRecord,'calendarId','c',respJ);	
							}
							else if (column.key == 'event'){									
								VELOS.milestoneGrid.updateDependent(column,oRecord,'visitId','v',respJ);
							}
							else if (column.key == 'evtStatus'){	
								 var temp = oRecord.getData('mileRule');
								  if(temp == 'On Scheduled Date' ){									 								
									  //APR-13-11,BK ,FIXED #6017
									  return;
								  }
								  else{									  
									  column.editor.dropdownOptions =  respJ.eventStatus;
								  }								 
							}
							// If no action is given, I try to edit it
							//FIX #6033, #6275
							if (column.editor instanceof YAHOO.widget.DropdownCellEditor){
								if (column.editor.dropdownOptions != null){
									column.editor.render();
									this.onEventShowCellEditor(oArgs);
								}
							}else{
								if (column.editor instanceof YAHOO.widget.TextboxCellEditor){
									this.onEventShowCellEditor(oArgs);
								}
							}
							return;
						}						
					}
				}else{//Existing Record
					if (f_check_perm(pgRight,'E')){
						if (column.key == 'mileStatus'){
							var status = oRecord.getData('mileStaticStatusId');
							var mileAchievedCount = oRecord.getData('mileAchievedCount');
							/* Loads Milestone status for each Milestone **/
							for (var iM=0; iM<allMileStatusOpen.length; iM++){
								var selMileId =allMileStatusOpen[iM].mileStatusId;
									if(mileId==selMileId){
										allMileStatusDesc = allMileStatusOpen[iM].mileStatuses.codeDesc;
										allMileStatusIds = allMileStatusOpen[iM].mileStatuses.codeId;
									}
								
							}
							column.editor.dropdownOptions = allMileStatusDesc;
							column.editor.render();
							this.onEventShowCellEditor(oArgs);	
							return;
						}
						if (column.key != 'mileType'){
							//FIX #6329
							if (mileStatusSubType != 'A' && mileStatusSubType != 'IA'){								
								if (column.key == 'visit'){						
									VELOS.milestoneGrid.updateDependent(column,oRecord,'calendarId','c',respJ);	
								}
								if (column.key == 'event'){								
									VELOS.milestoneGrid.updateDependent(column,oRecord,'visitId','v',respJ);						
								}
								if (column.key == 'evtStatus'){	
									 var temp = oRecord.getData('mileRule');
									  if(temp == 'On Scheduled Date' ){									 								
										  column.editor.dropdownOptions =  [''];
										  column.editor.render(); 
										  return;
									  }
									  else{									  
										  column.editor.dropdownOptions =  respJ.eventStatus;
									  }
								}
								//FIX #6033, #6275
								if (column.editor instanceof YAHOO.widget.DropdownCellEditor){
									if (column.editor.dropdownOptions != null){
										column.editor.render();
										this.onEventShowCellEditor(oArgs);
									}
								}else{
									if (column.editor instanceof YAHOO.widget.TextboxCellEditor){
										this.onEventShowCellEditor(oArgs);
									}
								}
								return;
							} else {
								if (column.key == 'payFor'){
									if (column.editor.dropdownOptions != null){
										column.editor.render();
										this.onEventShowCellEditor(oArgs);
									}
									return;	
								}
							}
						}
						//Fixed Bug#14858 (FIN-22301)
						if (column.key == 'holdBack'){
							var isReconciledAmount=oRecord.getData('isReconciledAmount');
							var mileAchievedCount = oRecord.getData('mileAchievedCount');
							var isInvoiceGenerated=oRecord.getData('isInvoiceGenerated');
							if (mileStatusSubType != 'IA'){
								if(isInvoiceGenerated=='0' && isReconciledAmount=='0'){
									column.editor.render();
									this.onEventShowCellEditor(oArgs);
								}
							}
						}
						if (column.key == 'dateto'){
							//var isReconciledAmount=oRecord.getData('isReconciledAmount');
							var mileAchievedCount = oRecord.getData('mileAchievedCount');
							//var isInvoiceGenerated=oRecord.getData('isInvoiceGenerated');
							if (mileStatusSubType != 'IA'){
								//if(isInvoiceGenerated=='0' && isReconciledAmount=='0'){
									column.editor.render();
									this.onEventShowCellEditor(oArgs);
								//}
							}
						}
					}
				}
				}else{
					if (mileId == 0){ //For Power Bar
						if (column.key != 'mileType'){
							if (column.key == 'mileStatus'){
								/* Loads Milestone status for each Milestone **/
										allMileStatusDesc = myMileStatusesRl;
										allMileStatusIds = myMileStatusIdsRl;
								column.editor.dropdownOptions = allMileStatusDesc;
							}
							if (column.key == 'visit'){	
								VELOS.milestoneGrid.updateDependent(column,oRecord,'calendarId','c',respJ);	
							}
							else if (column.key == 'event'){									
								VELOS.milestoneGrid.updateDependent(column,oRecord,'visitId','v',respJ);
							}
							else if (column.key == 'evtStatus'){	
								 var temp = oRecord.getData('mileRule');
								  if(temp == 'On Scheduled Date' ){									 								
									  //APR-13-11,BK ,FIXED #6017
									  return;
								  }
								  else{									  
									  column.editor.dropdownOptions =  respJ.eventStatus;
								  }								 
							}
							// If no action is given, I try to edit it
							//FIX #6033, #6275
							if (column.editor instanceof YAHOO.widget.DropdownCellEditor){
								if (column.editor.dropdownOptions != null){
									column.editor.render();
									this.onEventShowCellEditor(oArgs);
								}
							}else{
								if (column.editor instanceof YAHOO.widget.TextboxCellEditor){
									this.onEventShowCellEditor(oArgs);
								}
							}
							return;
						}						
					}
					
					
				}
				
			}
		};
		
		
		var onCellSave = function(oArgs) {
			if(valValid == false)
			numCheckValid = false;
			var elCell = oArgs.editor.getTdEl();
			var oOldData = oArgs.oldData;
			var oNewData = oArgs.newData;			
			var oRecord = this.getRecord(elCell);			
			var column =  this.getColumn(elCell);
			var mileSeq = oRecord.getData('mileSeq');
			var recNum = oRecord.getData('recNum');
			//INF-22500
			var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
			var parentDiv = new YAHOO.util.Element($D.getAncestorByTagName(parent, 'div'));
			var parentId = parentDiv.get('id');
			//Bug #10465 : AGodara
			if(saveDropDown==false){
				oRecord.setData(column.key, oArgs.oNewData);
				this.onEventShowCellEditor({target:elCell}); 
				saveDropDown=true;
				return false;
			}
			
			mileTypeId = oRecord.getData('mileTypeId');
			if (column.key == 'payType'){					
				VELOS.milestoneGrid.setDropDownId(this,elCell,myPaytypesIds,myPaytypes);
			}
			else if(column.key == 'payFor'){
				VELOS.milestoneGrid.setDropDownId(this,elCell,myPayforIds,myPayfors);
			}
			else if(column.key == 'mileStatus'){
				VELOS.milestoneGrid.setDropDownId(this,elCell,allMileStatusIds,allMileStatusDesc);
			}
			else if(column.key == 'patStatus'){
				VELOS.milestoneGrid.setDropDownId(this,elCell,patStatusIds,patStatus);
			}
			else if(column.key == 'stdStatus'){
				VELOS.milestoneGrid.setDropDownId(this,elCell,studyStatusIds,studyStatus);
			}
			else if(column.key == 'evtStatus'){
				VELOS.milestoneGrid.setDropDownId(this,elCell,eventStatusIds,eventStatus);
			}	
			else if (column.key  == 'mileRule'){
				if (mileTypeId == 'EM'|| parentId=='EM_Parent_milestonegrid')
					VELOS.milestoneGrid.setDropDownId(this,elCell,myEMruleIds,myEMrules);
				
				if (mileTypeId == 'VM'|| parentId=='VM_Parent_milestonegrid')
					VELOS.milestoneGrid.setDropDownId(this,elCell,myVMruleIds,myVMrules);
				
				//FIX #6050
				if(oNewData == "On Scheduled Date"){
					VELOS.milestoneGrid.eraseDependent(this,elCell,'evtStatus','');
					VELOS.milestoneGrid.eraseDependent(this,elCell,'evtStatusId',0);
				}
			}
			else if (column.key == 'calendar'){	
				VELOS.milestoneGrid.setDropDownId(this,elCell,myCalendarIds,myCalendars);
				if(oNewData == '***Patient Calendars***' || oNewData == '***Admin Calendars***' ){	
					VELOS.milestoneGrid.eraseDependent(this,elCell,column.key,'');
				}
				if (oOldData != oNewData ){				
					VELOS.milestoneGrid.eraseDependent(this,elCell,'visit','');					
					VELOS.milestoneGrid.eraseDependent(this,elCell,'visitId',0);					
					if(mileTypeId == 'EM'){
						VELOS.milestoneGrid.eraseDependent(this,elCell,'event','');					
						VELOS.milestoneGrid.eraseDependent(this,elCell,'eventId',0);
					}
				}
				if (oRecord.getData('calendarType')=='A'){
					VELOS.milestoneGrid.eraseDependent(this,elCell,'patCount',1);
					VELOS.milestoneGrid.eraseDependent(this,elCell,'patStatus','');
					VELOS.milestoneGrid.eraseDependent(this,elCell,'patStatusId','0');
				}
			}
			else if(column.key == 'visit'){				 
			 if(mileTypeId == 'VM'){
				VELOS.milestoneGrid.setDropDownId(this,elCell,VMvisitIds,VMvisitNames);
				}
				else{
				VELOS.milestoneGrid.setDropDownId(this,elCell,depIds,depNames);
				}	
				if (oOldData != oNewData){				
					VELOS.milestoneGrid.eraseDependent(this,elCell,'event','');
					VELOS.milestoneGrid.eraseDependent(this,elCell,'eventId',0);
				}		
			}
			else if(column.key == 'event'){
				VELOS.milestoneGrid.setDropDownId(this,elCell,depIds,depNames);
			}
			else{				
				var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
				var Td = parent.getElementsByClassName('yui-dt-col-'+column.key, 'td')[0];
				var El = new YAHOO.util.Element(Td);
				var oData = oNewData;
				//oData is null if formatter encounters error
				if (oData != null){					
					oData = VELOS.milestoneGrid.encodeData(oData);
					El.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = oData;
					oRecord.setData(column.key, oNewData);
				}
				else if(oData == null){					
					oData = oOldData;
					oRecord.setData(column.key, oData);
					this.onEventShowCellEditor({target:elCell});
				}
				
				var nodes = parent.getElementsByClassName('yui-dt-col-amount', 'td')[0];
				$j(jQuery(nodes).children()).formatCurrency({
		            region:appNumberLocale
		         });
				/*var limitNodes = parent.getElementsByClassName('yui-dt-col-limit', 'td')[0];
				$j(jQuery(limitNodes).children()).formatCurrency({
		            region:appNumberLocale
		         });*/
				
					if(parentId=='PM_Parent_milestonegrid' || parentId=='EM_Parent_milestonegrid' || parentId=='VM_Parent_milestonegrid' || parentId=='SM_Parent_milestonegrid' || parentId=='AM_Parent_milestonegrid')
					{	if(column.key == 'holdBack_parent')
						{
						var holdBackNodes = parent.getElementsByClassName('yui-dt-col-holdBack_parent', 'td')[0];
						var holdBackEl = new YAHOO.util.Element(holdBackNodes);
						var holdBackValue = holdBackEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
						$j(holdBackEl.getElementsByClassName('yui-dt-liner', 'div')[0]).formatCurrency({
					        region:appNumberLocale
					    });
						var holdBackValueDiv = holdBackEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
						if(oNewData>100 || oNewData<0)
						{
						holdBackValue = YAHOO.util.Number.format(0.00, twoDecimalNumber);
						oArgs.oldData=holdBackValue;
						holdBackEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML=holdBackValueDiv + '%';
						oRecord.setData('holdBack_parent',holdBackValue);
						}
						else{
							if(holdBackValue<100 && holdBackValue>=0){
								holdBackValue = YAHOO.util.Number.format(holdBackValue, twoDecimalNumber);
								holdBackEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML=holdBackValueDiv + '%';
								oRecord.setData('holdBack_parent',holdBackValue);
							}else{
								holdBackValue='&nbsp';
								holdBackEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML=holdBackValue;	
								oRecord.setData('holdBack_parent','');
							}
						}//Parminder Singh : Bug#14845
						}
					}
					else
					{	if(column.key == 'holdBack')
						{
						var holdBackNodes = parent.getElementsByClassName('yui-dt-col-holdBack', 'td')[0];
						var holdBackEl = new YAHOO.util.Element(holdBackNodes);
						$j(holdBackEl.getElementsByClassName('yui-dt-liner', 'div')[0]).formatCurrency({
					        region:appNumberLocale
					    });
						var holdBackValue = holdBackEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
						holdBackValue = holdBackValue + '%'; 
						holdBackEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML=holdBackValue;
						}
					}
				
				
				
				/*if (itemLabelArray['e'+itemSeq] == undefined){
					if (itemName!= undefined && itemName!=null && itemName!=''){
						itemLabelArray['e'+itemSeq] = itemName;
					}
				}else{
					itemLabelArray['e'+itemSeq] = itemName;
				}*/
			}			
			if(oNewData == L_Select_AnOption){				
				VELOS.milestoneGrid.eraseDependent(this,elCell,column.key,'');
			}
			//FIX #6249
			if (oOldData != oNewData){
				if(!((oOldData == null || oOldData == '') && (oNewData == L_Select_AnOption
						|| oNewData == '' || oNewData==null ))){
					VELOS.milestoneGrid.auditAction(mileTypeId, mileSeq, 'Updated', recNum);
				}
			}
			//BUG
			if(mileTypeId=='PM'){
				VELOS.milestoneGrid.flushRecordList(mileTypeId,mileSeq,oOldData,oNewData,parent.get('id'),column.key);
			}
			else if(mileTypeId=='VM'){
				VELOS.milestoneGrid.flushRecordList(mileTypeId,mileSeq,oOldData,oNewData,parent.get('id'),column.key);
			}
			else if(mileTypeId=='EM'){
				VELOS.milestoneGrid.flushRecordList(mileTypeId,mileSeq,oOldData,oNewData,parent.get('id'),column.key);
			}
			else if(mileTypeId=='SM'){
				VELOS.milestoneGrid.flushRecordList(mileTypeId,mileSeq,oOldData,oNewData,parent.get('id'),column.key);
			}
			else{
				VELOS.milestoneGrid.flushRecordList(mileTypeId,mileSeq,oOldData,oNewData,parent.get('id'),column.key);
			}
		};	
		
		
		onCheckClick = function(oArgs){
			var elCheckbox = oArgs.target;
			var oRecord = this.getRecord(elCheckbox);
			var column = this.getColumn(elCheckbox);			
			var mileSeq = oRecord.getData('mileSeq');
			var mileId = parseInt(oRecord.getData('mileId'));
			var mileTypeId = oRecord.getData('mileTypeId');
			var rowNum = oRecord.getData('recNum');
			if (purgedMileArray==null) purgedMileArray =[];
			if (mileId == 0) {
				if (!f_check_perm_noAlert(pgRight, 'N')) {
					elCheckbox.checked = (!elCheckbox.checked);
					return;
				}
			} else {
				if (!f_check_perm_noAlert(pgRight, 'E')) {
					elCheckbox.checked = (!elCheckbox.checked);
					return;
				}
			}
			//INF-22500
			var selectAllFlag = false;
			if(column.action.substring(0,9)=='selectAll'){
				selectAllFlag = true;
				if(elCheckbox.checked){
					YAHOO.util.Dom.addClass(elCheckbox, 'selSelected');
				}else{
					YAHOO.util.Dom.removeClass(elCheckbox, 'selSelected');
				}
				
			}
			if(!selectAllFlag){
				oRecord.setData('delete_'+mileTypeId, ""+elCheckbox.checked);
			}
			if(!selectAllFlag){
				if(elCheckbox.checked){
					var paramArray = [rowNum];
						purgedMileArray[mileTypeId+'mileSeq'+mileSeq]=getLocalizedMessageString("L_Del_Row",paramArray)/*'Deleted Row #'+rowNum+''*****/;
						if (oRecord.getData('mileId')!="0"){
							var data = oRecord.getData('mileId');				
							milePurgeList[mileTypeId+'mileSeq'+mileSeq] = data;						
						}
						YAHOO.util.Dom.addClass(elCheckbox, 'delSelected');
				    }
					else{
						YAHOO.util.Dom.removeClass(elCheckbox, 'delSelected');
						purgedMileArray[mileTypeId+'mileSeq'+mileSeq]= undefined;				
						milePurgeList[mileTypeId+'mileSeq'+mileSeq] = undefined;
					}	
			}
		}
		
		var onHeaderClick = function(oArgs) {
			var myGrid;
			if (this == myPMGrid){
				myGrid = this;
				pmMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myGrid.getTableEl());
				VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(pmMileIdCells,'PM');
				myPMGrid.focus();
			}
			if (this == myVMGrid){
				myGrid = this;
				vmMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myGrid.getTableEl());
				VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(vmMileIdCells,'VM');
				myVMGrid.focus();
			}
			if (this == myEMGrid){
				myGrid = this;
				emMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myGrid.getTableEl());
				VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(emMileIdCells,'EM');
				myEMGrid.focus();
			}
			if (this == mySMGrid){
				myGrid = this;
				smMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myGrid.getTableEl());
				VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(smMileIdCells,'SM');
				mySMGrid.focus();
			}
			if (this == myAMGrid){
				myGrid = this;
				amMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myGrid.getTableEl());
				VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(amMileIdCells,'AM');
				myAMGrid.focus();
			}
			
			var otherRegularExp =  new RegExp('%$');
			jQuery("td.yui-dt-col-holdBack >div.yui-dt-liner ").each(function(){
				if(this.innerHTML.match(otherRegularExp)==null )
				jQuery(this).text(YAHOO.util.Number.format(this.innerHTML, twoDecimalNumber)+'%');
			});
			
		};
		var handleFocus = function (myGrid, target){
			var oColumn = myGrid.getColumn(target);
			var key = oColumn.key;
			var oRecord = myGrid.getRecord(target);
			var focusMe;
			if(!target.disabled){
				if (key.match(/^v[0-9]+$/)){
					//focus on the checkbox
					var targetEl = new YAHOO.util.Element(target);
					var parentDiv = targetEl.getElementsByClassName('yui-dt-liner', 'div')[0];
					var parentDivEl = new YAHOO.util.Element(parentDiv);
					var chkBox = parentDivEl.getElementsByClassName('yui-dt-checkbox', 'input')[0];
					focusMe = chkBox;
					
				}
				if (key == 'delete_'+mileTypeId){//INF-22500
					//focus on the checkbox
					var targetEl = new YAHOO.util.Element(target);
					var parentDiv = targetEl.getElementsByClassName('yui-dt-liner', 'div')[0];
					var parentDivEl = new YAHOO.util.Element(parentDiv);
					var chkBox = parentDivEl.getElementsByClassName('yui-dt-checkbox', 'input')[0];
					focusMe = chkBox;					
				}
			}
			if (focusMe && !focusMe.disabled){
				myGrid.saveCellEditor();
				target.onkeydown = function(e) {
					e = e || window.event;
					var charCode = e.keyCode || e.which;
					switch (charCode) {
					case 9:
						YAHOO.util.Event.stopEvent(e);
						if (e.shiftKey) {
							editPrevious(myGrid, target);
						} else {
							editNext(myGrid, target);
						}
						break;
					}
				}
				focusMe.tabindex = -1;
				focusMe.keydown = target.keydown;
				focusMe.focus();
			}
			return;
		}
		var editNext = function(myGrid, cell) {
			cell = myGrid.getNextTdEl(cell);
			while (cell && !myGrid.getColumn(cell).editor) {
				cell = myGrid.getNextTdEl(cell);
			}
			if (cell) {
				//Enabling/disabling time-point child columns
				var oRecord = myGrid.getRecord(cell);
				var tMileId = oRecord.getData('mileId');
				var tMileTypeId = oRecord.getData('mileTypeId');
				var tMileStatusId = oRecord.getData('mileStaticStatusId');
				var tMileStatusSubType = mileStatusSubTypes[$j.inArray(tMileStatusId,myMileStatusIds)];
				var returnVal = VELOS.milestoneGrid.enableDisableChildColumns(myGrid.getColumn(cell),tMileStatusSubType,oRecord,respJ);
				if (returnVal < 1) {
					editNext(myGrid, cell);
				}else{
					var oColumn = myGrid.getColumn(cell);
					var justFocus = (oColumn.madeUp==true
					|| oColumn.editor._sType=='checkbox')?true:false;
					if (justFocus){
						handleFocus(myGrid, cell);
					}else {
						YAHOO.util.UserAction.click(cell);
					}
				}
			}
		};
		var editPrevious = function(myGrid, cell) {
			cell = myGrid.getPreviousTdEl(cell);
			while (cell && !myGrid.getColumn(cell).editor) {
				cell = myGrid.getPreviousTdEl(cell);
			}
			if (cell) {
				//Enabling/disabling time-point child columns
				var oRecord = myGrid.getRecord(cell);
				var tMileId = oRecord.getData('mileId');
				var tMileTypeId = oRecord.getData('mileTypeId');
				var tMileStatusId = oRecord.getData('mileStaticStatusId');
				var tMileStatusSubType = mileStatusSubTypes[$j.inArray(tMileStatusId,myMileStatusIds)];
				var returnVal = VELOS.milestoneGrid.enableDisableChildColumns(myGrid.getColumn(cell),tMileStatusSubType,oRecord,respJ);
				if (returnVal < 1) {
					editPrevious(myGrid, cell);
				}else{
					var oColumn = myGrid.getColumn(cell);
					var justFocus = (oColumn.madeUp==true
					|| oColumn.editor._sType=='checkbox')?true:false;
					if (justFocus){
						handleFocus(myGrid, cell);
					}else {
						YAHOO.util.UserAction.click(cell);
					}
				}
			}
		};
		
		//SM:Introduced a common function to avoid repeated Tabbing code
		var onEditorKeyDown = function(oArgs) {
			var isIe = jQuery.browser.msie;
			var myGrid = this;
			if (myGrid){
				var ed = myGrid._oCellEditor;  // Should be: oArgs.editor, see: http://yuilibrary.com/projects/yui2/ticket/2513909
				var ev = oArgs.event;
				var KEY = YAHOO.util.KeyListener.KEY;
				var Textbox = YAHOO.widget.TextboxCellEditor;
				var Textarea = YAHOO.widget.TextareaCellEditor;
				if (ed){
					var cell = ed.getTdEl();
					var oColumn = ed.getColumn();
					var row,rec;
					//Bug #10465 : AGodara
					if(ed instanceof YAHOO.widget.DropdownCellEditor && isIe == true){
						switch (ev.keyCode) {
						case KEY.TAB:
							YAHOO.util.Event.stopEvent(ev);
							ed.save();
							if (ev.shiftKey) {
								editPrevious(myGrid, cell);
							} else {
								editNext(myGrid, cell);
							}
							break;
						case KEY.ENTER:
							YAHOO.util.Event.stopEvent(ev);
							ed.save();
							break;
						default :
							saveDropDown=false;
							break;
						}						
					}else{
						switch (ev.keyCode) {
						case KEY.TAB:
							YAHOO.util.Event.stopEvent(ev);
							ed.save();
							if (ev.shiftKey) {
								editPrevious(myGrid, cell);
							} else {
								editNext(myGrid, cell);
							}
							break;
						}
					}
				}
			}
		};
		
		if (pMileTypeId == 'ALL' || pMileTypeId == 'PM'){
			if(myPMGrid != null){
				myPMGrid.subscribe('cellClickEvent', onCellClick);
				myPMGrid.subscribe("editorSaveEvent", onCellSave);
				myPMGrid.subscribe("checkboxClickEvent", onCheckClick);
				myPMGrid.subscribe("rowMouseoverEvent", onMouseOver);
				myPMGrid.subscribe("rowMouseoutEvent", onMouseOut);
				myPMGrid.subscribe("rowClickEvent", myPMGrid.onEventSelectRow);
				myPMGrid.subscribe("headerCellClickEvent", onHeaderClick);
				//TAB key implementation Bug#6182 Date:30-05-2012 Ankit
				myPMGrid.subscribe("editorKeydownEvent",onEditorKeyDown);
			}
			//Added to subscribe Milestone Power Bar Grid Events - INF-22500 : Raviesh
			if(myPMParentGrid != null){
				myPMParentGrid.subscribe('cellClickEvent', onCellClick);
				myPMParentGrid.subscribe("editorSaveEvent", onCellSave);
				myPMParentGrid.subscribe("rowMouseoverEvent", onMouseOver);
				myPMParentGrid.subscribe("rowMouseoutEvent", onMouseOut)
				myPMParentGrid.subscribe("rowClickEvent", myPMParentGrid.onEventSelectRow);
				myPMParentGrid.subscribe("headerCellClickEvent", onHeaderClick);
				myPMParentGrid.subscribe("editorKeydownEvent",onEditorKeyDown);
			}
		}
		if (pMileTypeId == 'ALL' || pMileTypeId == 'VM'){
			if(myVMGrid != null){
				myVMGrid.subscribe('cellClickEvent', onCellClick);
				myVMGrid.subscribe("editorSaveEvent", onCellSave);
				myVMGrid.subscribe("checkboxClickEvent", onCheckClick);
				myVMGrid.subscribe("rowMouseoverEvent", onMouseOver);
				myVMGrid.subscribe("rowMouseoutEvent", onMouseOut);
				myVMGrid.subscribe("rowClickEvent", myVMGrid.onEventSelectRow);
				myVMGrid.subscribe("headerCellClickEvent", onHeaderClick);
				//TAB key implementation Bug#6182 Date:30-05-2012 Ankit
				myVMGrid.subscribe("editorKeydownEvent",onEditorKeyDown);
			}
			//Added to subscribe Milestone Power Bar Grid Events - INF-22500 : Raviesh
			if(myVMParentGrid != null){
				myVMParentGrid.subscribe('cellClickEvent', onCellClick);
				myVMParentGrid.subscribe("editorSaveEvent", onCellSave);
				myVMParentGrid.subscribe("rowMouseoverEvent", onMouseOver);
				myVMParentGrid.subscribe("rowMouseoutEvent", onMouseOut)
				myVMParentGrid.subscribe("rowClickEvent", myVMParentGrid.onEventSelectRow);
				myVMParentGrid.subscribe("headerCellClickEvent", onHeaderClick);
				myVMParentGrid.subscribe("editorKeydownEvent",onEditorKeyDown);
			}
		}
		if (pMileTypeId == 'ALL' || pMileTypeId == 'EM'){
			if(myEMGrid != null){
				myEMGrid.subscribe('cellClickEvent', onCellClick);
				myEMGrid.subscribe("editorSaveEvent", onCellSave);
				myEMGrid.subscribe("checkboxClickEvent", onCheckClick);
				myEMGrid.subscribe("rowMouseoverEvent", onMouseOver);
				myEMGrid.subscribe("rowMouseoutEvent", onMouseOut)
				myEMGrid.subscribe("rowClickEvent", myEMGrid.onEventSelectRow);
				myEMGrid.subscribe("headerCellClickEvent", onHeaderClick);
				//TAB key implementation Bug#6182 Date:30-05-2012 Ankit
				myEMGrid.subscribe("editorKeydownEvent",onEditorKeyDown);
			}
			//Added to subscribe Milestone Power Bar Grid Events - INF-22500 : Raviesh
			if(myEMParentGrid != null){
				myEMParentGrid.subscribe('cellClickEvent', onCellClick);
				myEMParentGrid.subscribe("editorSaveEvent", onCellSave);
				myEMParentGrid.subscribe("rowMouseoverEvent", onMouseOver);
				myEMParentGrid.subscribe("rowMouseoutEvent", onMouseOut)
				myEMParentGrid.subscribe("rowClickEvent", myEMParentGrid.onEventSelectRow);
				myEMParentGrid.subscribe("headerCellClickEvent", onHeaderClick);
				myEMParentGrid.subscribe("editorKeydownEvent",onEditorKeyDown);
			}
		}
		if (pMileTypeId == 'ALL' || pMileTypeId == 'SM'){
			if(mySMGrid != null){
				mySMGrid.subscribe('cellClickEvent', onCellClick);
				mySMGrid.subscribe("editorSaveEvent", onCellSave);
				mySMGrid.subscribe("checkboxClickEvent", onCheckClick);
				mySMGrid.subscribe("rowMouseoverEvent", onMouseOver);
				mySMGrid.subscribe("rowMouseoutEvent", onMouseOut)
				mySMGrid.subscribe("rowClickEvent", mySMGrid.onEventSelectRow);
				mySMGrid.subscribe("headerCellClickEvent", onHeaderClick);
				//TAB key implementation Bug#6182 Date:30-05-2012 Ankit
				mySMGrid.subscribe("editorKeydownEvent",onEditorKeyDown);
			}
			//Added to subscribe Milestone Power Bar Grid Events - INF-22500 : Raviesh
			if(mySMParentGrid != null){
				mySMParentGrid.subscribe('cellClickEvent', onCellClick);
				mySMParentGrid.subscribe("editorSaveEvent", onCellSave);
				mySMParentGrid.subscribe("rowMouseoverEvent", onMouseOver);
				mySMParentGrid.subscribe("rowMouseoutEvent", onMouseOut)
				mySMParentGrid.subscribe("rowClickEvent", mySMParentGrid.onEventSelectRow);
				mySMParentGrid.subscribe("headerCellClickEvent", onHeaderClick);
				mySMParentGrid.subscribe("editorKeydownEvent",onEditorKeyDown);
			}
		}
		if (pMileTypeId == 'ALL' || pMileTypeId == 'AM'){
			if(myAMGrid != null){
				myAMGrid.subscribe('cellClickEvent', onCellClick);
				myAMGrid.subscribe("editorSaveEvent", onCellSave);
				myAMGrid.subscribe("checkboxClickEvent", onCheckClick);
				myAMGrid.subscribe("rowMouseoverEvent", onMouseOver);
				myAMGrid.subscribe("rowMouseoutEvent", onMouseOut)
				myAMGrid.subscribe("rowClickEvent", myAMGrid.onEventSelectRow);
				myAMGrid.subscribe("headerCellClickEvent", onHeaderClick);
				//TAB key implementation Bug#6182 Date:30-05-2012 Ankit
				myAMGrid.subscribe("editorKeydownEvent",onEditorKeyDown);
			}
			//Added to subscribe Milestone Power Bar Grid Events - INF-22500 : Raviesh
			if(myAMParentGrid != null){
				myAMParentGrid.subscribe('cellClickEvent', onCellClick);
				myAMParentGrid.subscribe("editorSaveEvent", onCellSave);
				myAMParentGrid.subscribe("rowMouseoverEvent", onMouseOver);
				myAMParentGrid.subscribe("rowMouseoutEvent", onMouseOut)
				myAMParentGrid.subscribe("rowClickEvent", myAMParentGrid.onEventSelectRow);
				myAMParentGrid.subscribe("headerCellClickEvent", onHeaderClick);
				myAMParentGrid.subscribe("editorKeydownEvent",onEditorKeyDown);
			}
		}
		
	/*	//addRowsClick was defined here
		if ($('PM_addRows')){
			var btn = new YAHOO.widget.Button("PM_addRows");
			btn.on("click", function () {VELOS.milestoneGrid.addRows(myPMGrid, 'PM',respJ);},this,true);
		}
		if ($('VM_addRows')){
			var btn = new YAHOO.widget.Button("VM_addRows");
			btn.on("click", function () {VELOS.milestoneGrid.addRows(myVMGrid, 'VM',respJ);},this,true);
		}
		if ($('EM_addRows')){		
			var btn = new YAHOO.widget.Button("EM_addRows");
			btn.on("click", function () {VELOS.milestoneGrid.addRows(myEMGrid, 'EM',respJ);},this,true);
		}
		if ($('SM_addRows')){
			var btn = new YAHOO.widget.Button("SM_addRows");
			btn.on("click", function () {VELOS.milestoneGrid.addRows(mySMGrid, 'SM',respJ);},this,true);
		}
		if ($('AM_addRows')){
			var btn = new YAHOO.widget.Button("AM_addRows");
			btn.on("click", function () {VELOS.milestoneGrid.addRows(myAMGrid, 'AM',respJ);},this,true);
		}*/
		
		if (!$('save_changes')) {
			var dt = $(this.dataTable);
			var saveDiv = document.createElement('div');
			saveDiv.innerHTML = "<br/><table border=0 width='100%'><tbody><tr align='left'>" +
				"<td width='350'></td>"+
				"<td align='left'><input onclick='VELOS.milestoneGrid.saveDialog();' type='button' id='save_changes' name='save_changes' value='"+Preview_AndSave/*Preview and Save*****/+"'/></td>"+
				"</td></tr></tbody></table>";
			$D.insertAfter(saveDiv, dt);

		}

		calStatus = respJ.calStatus;
		var cells = null;
		if (calStatus == 'A' || calStatus == 'D' || calStatus == 'F' || calStatus == 'R') {
			cells = $D.getElementsByClassName('yui-dt-checkbox', 'input', myPMGrid.getTableEl());
			for (var iX = 0; iX < cells.length; iX++) {
				cells[iX].disabled = true;
			}
			if ($('save_changes')) { $('save_changes').disabled = true; }
		}

		hidePanel();
		//For Collapsing the Grids which do no have Data
		// For INF-22500 (Milestone Merging) By Yogendra  Paratp
		if(!clickedOnMile){
		if(dataArray.length==0 ){
			if(mileTypeId=='PM_Parent'){
				document.getElementById("PM_milestonegrid").innerHTML="";
				jQuery( "#PatientMileStone" ).accordion({
					autoHeight: false, collapsible: true, active: false, fillSpace: false, animated: false
				});
			}else if(mileTypeId=='EM_Parent'){
				document.getElementById("EM_milestonegrid").innerHTML="";
				jQuery( "#EventMilestone" ).accordion({
					autoHeight: false, collapsible: true, active: false, fillSpace: false, animated: false
				});
			}else if(mileTypeId=='AM_Parent'){
				document.getElementById("AM_milestonegrid").innerHTML="";
				jQuery( "#AdditionalMilestone" ).accordion({
					autoHeight: false, collapsible: true, active: false, fillSpace: false, animated: false
				});
			}else if(mileTypeId=='SM_Parent'){
				document.getElementById("SM_milestonegrid").innerHTML="";
				jQuery( "#StudyMilestone" ).accordion({
					autoHeight: false, collapsible: true, active: false, fillSpace: false, animated: false
				});
			}else if(mileTypeId=='VM_Parent'){
				document.getElementById("VM_milestonegrid").innerHTML="";
				jQuery( "#VisitMilestone" ).accordion({
					autoHeight: false, collapsible: true, active: false, fillSpace: false, animated: false
				});
			}
		}
		}
		// For INF-22500 (Milestone Merging) By Yogendra Pratap [End]
	};
	this.handleFailure = function(o) {
		alert(CommErr_CnctVelos);/*alert('Communication Error. Please contact Velos Support');*****/
		hidePanel();
	};
	this.cArgs = [];
	this.callback = {
		success:this.handleSuccess,
		failure:this.handleFailure,
		argument:this.cArgs
	};
	this.initConfigs(args);
	
}

// TAB key implementation Bug#6182,#10074,#10077 Date:30-05-2012 Ankit 
VELOS.milestoneGrid.enableDisableChildColumns = function (oColumn,tMileStatusSubType,oRecord,respJ){
	var key = oColumn.key;
	var tMileId = oRecord.getData('mileId');
	var tMileTypeId = oRecord.getData('mileTypeId');
	var calTyp = oRecord.getData('calendarType');
	var cal = oRecord.getData('calendar');
	if (key == 'delete_'+mileTypeId) { 
		//alert(YAHOO.lang.dump(oRecord));
		var parent = new YAHOO.util.Element(oRecord._sId);
		var target = parent.getElementsByClassName('yui-dt-col-delete_'+mileTypeId, 'td')[0];
		var targetEl = new YAHOO.util.Element(target);
		var parentDiv = targetEl.getElementsByClassName('yui-dt-liner', 'div')[0];
		var parentDivEl = new YAHOO.util.Element(parentDiv);
		var chkBox = parentDivEl.getElementsByClassName('yui-dt-checkbox', 'input')[0];
		if (chkBox && !chkBox.disabled){
			return 1;
		} else return 0; 
	}

	if ((tMileId != 0) && (tMileTypeId == 'PM') && (tMileStatusSubType == 'A' || tMileStatusSubType == 'IA')){
		if ((key == 'mileType') ||
			(key == 'patCount') ||
			(key == 'patStatus') ||
			(key == 'amount') ||
			(key == 'limit') ||
			(key == 'payType')||(key == 'datefrom') ||(key == 'dateto'))
			return 0;
	}
	if (tMileTypeId == 'VM' || tMileTypeId == 'EM')
	{
		if (key == 'patCount' || key == 'patStatus'){
			if (calTyp == 'A')
				return 0;
			if (tMileId != 0)
				return 0;
		}
	}
	
	if ((tMileId != 0) && (tMileTypeId == 'VM') && (tMileStatusSubType == 'A' || tMileStatusSubType == 'IA')){
		if ((key == 'calendar') ||
			(key == 'visit') ||
			(key == 'mileRule') ||
			(key == 'evtStatus') ||
			(key == 'patCount') ||
			(key == 'patStatus') ||
			(key == 'amount') ||
			(key == 'limit') ||
			(key == 'payType')|| (key == 'datefrom') ||(key == 'dateto'))
			return 0;
	}
	if ((tMileId != 0) && (tMileTypeId == 'VM') && (tMileStatusSubType == 'WIP' ||(tMileStatusSubType!='IA' || tMileStatusSubType != 'A'))){
		if ((key == 'calendar') ||
			(key == 'visit'))
			return 0;
	}
	if((tMileId == 0) && (tMileTypeId == 'VM') && (key == 'visit')){
			var tCalendar = oRecord.getData('calendar');
			if(tCalendar == '')
				return 0;
	}
	if ((tMileId != 0) && (tMileTypeId == 'EM') && (tMileStatusSubType == 'A' || tMileStatusSubType == 'IA')){
		if ((key == 'calendar') ||
			(key == 'visit') ||
			(key == 'event') ||
			(key == 'mileRule') ||
			(key == 'evtStatus') ||
			(key == 'patCount') ||
			(key == 'patStatus') ||
			(key == 'amount') ||
			(key == 'limit') ||
			(key == 'payType')|| (key == 'datefrom') ||(key == 'dateto'))
			return 0;
	}
	if ((tMileId != 0) && (tMileTypeId == 'EM') && (tMileStatusSubType == 'WIP' ||(tMileStatusSubType!='IA' || tMileStatusSubType != 'A'))){
		if ((key == 'calendar') ||
			(key == 'visit') ||
			(key == 'event'))
			return 0;
	}
	if((tMileId == 0) && (tMileTypeId == 'EM') && (key == 'visit' || key == 'event')){
			var tCalendar = oRecord.getData('calendar');
			var depCount = 0;
			if(tCalendar == '')
				return 0;
			if (key == 'visit'){
				depCount = VELOS.milestoneGrid.checkDependent(oColumn,oRecord,'calendarId','c',respJ);
			}
			if (key == 'event'){
				var tVisit = oRecord.getData('visit');
				if(tVisit != ''){
					depCount = VELOS.milestoneGrid.checkDependent(oColumn,oRecord,'visitId','v',respJ);
				}
			}
			if(depCount == 0)
				return 0;
	}
	if ((tMileId != 0) && (tMileTypeId == 'SM') && (tMileStatusSubType == 'A' || tMileStatusSubType == 'IA')){
		if ((key == 'mileType') ||
			(key == 'patCount') ||
			(key == 'stdStatus') ||
			(key == 'patStatus') ||
			(key == 'amount') ||
			(key == 'limit') ||
			(key == 'payType') || (key == 'datefrom') ||(key == 'dateto'))
			return 0;
	}
	if ((tMileId != 0) && (tMileTypeId == 'AM') && (tMileStatusSubType == 'A' || tMileStatusSubType == 'IA')){
		if ((key == 'mileDesc') ||
			(key == 'amount') ||
			(key == 'payType'))
			return 0;
	}
	if ((key == 'recNum')||(key == 'mileType')){
		return 0;
	}
	if (key == 'evtStatus' && (oRecord.getData('mileRule') == 'On Scheduled Date'))	{
		return 0;
	}
	return 1;
};
VELOS.milestoneGrid.checkDependent = function (columnObject,recordObject,dependent,depValue,respJ){
    var dependentId = recordObject.getData(dependent);
    var depArray = respJ[depValue+dependentId];
    depIds = [];
    VMvisitIds = [];
    depNames = [];
    VMvisitNames = [];   
    for (var iL=0; iL<depArray.length; iL++){
        var depJSON = depArray[iL];
        for(key in depJSON){
            depIds[iL] = key;
            VMvisitIds[iL] = key;
            depNames[iL] = depJSON[key];
            VMvisitNames[iL] = depJSON[key];
        }
    }
    return (depNames.length);
}
VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones = function (mileIdCells,mileTypeId){
	for (var iX=0; iX<mileIdCells.length; iX++) {
		var el = new YAHOO.util.Element(mileIdCells[iX]);
		var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
		//FIN-19817 27-Sep-2012 Hari
		var nodes = parent.getElementsByClassName('yui-dt-col-amount', 'td')[0];
		$j(jQuery(nodes).children()).formatCurrency({
            region:appNumberLocale
                     });
		/*var limitNodes = parent.getElementsByClassName('yui-dt-col-limit', 'td')[0];
		$j(jQuery(limitNodes).children()).formatCurrency({
            region:appNumberLocale
                     });*/
		
		var mileStatusIdTd = parent.getElementsByClassName('yui-dt-col-mileStatusId', 'td')[0];
		var mileStatusIdEl = new YAHOO.util.Element(mileStatusIdTd);
		var mileStatusId = mileStatusIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
		var mileStatusSubType ="";
		
		for (var ind=0; ind < myMileStatusIds.length; ind++){
			if (myMileStatusIds[ind] == mileStatusId){
				mileStatusSubType = mileStatusSubTypes[ind];
				ind = myMileStatusIds.length;
			}
		}
		
		var mileAchievedCountTd = parent.getElementsByClassName('yui-dt-col-mileAchievedCount', 'td')[0];
		var mileAchievedCountEl = new YAHOO.util.Element(mileAchievedCountTd);
		var mileAchievedCount = mileAchievedCountEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
		
		var isInvGeneratedTd = parent.getElementsByClassName('yui-dt-col-isInvoiceGenerated', 'td')[0];
		var isInvGeneratedEl = new YAHOO.util.Element(isInvGeneratedTd);
		var isInvGenerated = isInvGeneratedEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
		var isReconAmountTd = parent.getElementsByClassName('yui-dt-col-isReconciledAmount', 'td')[0];
		var isReconAmountEl = new YAHOO.util.Element(isReconAmountTd);
		var isReconAmount = isReconAmountEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
	
		//alert('Row #'+iX+' mileStatusSubType:'+mileStatusSubType+' mileAchievedCount:'+mileAchievedCount);
		if ((mileStatusSubType == 'A' && (mileAchievedCount != '0' || (isInvGenerated!= '0' || isReconAmount!= '0'))) || mileStatusSubType == 'IA'){
			var delButtonTd = parent.getElementsByClassName('yui-dt-col-delete_'+mileTypeId, 'td')[0];
			var delButtonEl = new YAHOO.util.Element(delButtonTd);
			var delButton = delButtonEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			delButtonEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML ="";
			//disabled Select check box when milestone Active and achieved and Inactive INF-22500
			var selButtonTd = parent.getElementsByClassName('yui-dt-col-selectAll_'+mileTypeId, 'td')[0];
			var selButtonEl = new YAHOO.util.Element(selButtonTd);
			var selButton = selButtonEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			selButtonEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML ="";
		}
	}
}

VELOS.milestoneGrid.updateDependent = function (columnObject,recordObject,dependent,depValue,respJ){
	var dependentId = recordObject.getData(dependent);		
	var depArray = respJ[depValue+dependentId];
	depIds = [];
	VMvisitIds = [];
	depNames = [];
	VMvisitNames = [];	
	
	for (var iL=0; iL<depArray.length; iL++){
		var depJSON = depArray[iL];
		for(key in depJSON){
			depIds[iL] = key;
			VMvisitIds[iL] = key;
			depNames[iL] = depJSON[key];
			VMvisitNames[iL] = depJSON[key];
		}
	}
	VMvisitIds.push("-1");
	VMvisitNames.push("All");
	var mileTypeId = recordObject.getData('mileTypeId');
	 if(mileTypeId == 'VM' && columnObject.key == 'visit'){
	 	//FIX #6033, #6275
		VMvisitNames = (VMvisitNames.length > 0)? VMvisitNames:null;
		columnObject.editor.dropdownOptions = VMvisitNames;
	 }
	 else{ 
		//FIX #6033, #6275
		depNames = (depNames.length > 0)? depNames:null;
		columnObject.editor.dropdownOptions = depNames;
	 }
	//columnObject.editor.render();
	//alert('rendered');
}

VELOS.milestoneGrid.eraseDependent = function (myGrid,elCell,dependent,depValue){
	var oRecord = myGrid.getRecord(elCell);
	var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
	var depTd = parent.getElementsByClassName('yui-dt-col-'+dependent, 'td')[0];	
	var depEl = new YAHOO.util.Element(depTd);	
	var dep = depEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;	
	oRecord.setData(dependent, depValue);
	depEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = depValue;
}

VELOS.milestoneGrid.setDropDownId = function (myGrid,elCell,myIds,myTypes){		
	
	var oRecord = myGrid.getRecord(elCell);			
	var oColumn =  myGrid.getColumn(elCell);	

	for (var j=0; j<myIds.length; j++){
		if (myTypes[j] == oRecord.getData(oColumn.key)) break;
	}

	oRecord.setData(oColumn.key +'Id', myIds[j]);
	
	var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
	var idTd = parent.getElementsByClassName('yui-dt-col-'+oColumn.key+'Id', 'td')[0];

	var idEl = new YAHOO.util.Element(idTd);

	var Id = idEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
	idEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = myIds[j];
	
	if(oColumn.key == 'calendar'){
		oRecord.setData(oColumn.key +'Type', myCalendarTypes[j]);
		var idTd = parent.getElementsByClassName('yui-dt-col-'+oColumn.key+'Type', 'td')[0];

		var idEl = new YAHOO.util.Element(idTd);

		var Id = idEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
		idEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = myCalendarTypes[j];
	}
};

VELOS.milestoneGrid.sortNumber = function (a, b, desc, field) {
	a = a.getData(field);
	if (/<[^>]+>/.test(a)){		
		a = a.replace(/<[^>]+>/, '');
	}
	b = b.getData(field);
	if (/<[^>]+>/.test(b)){
		b = b.replace(/<[^>]+>/, '');
	}
    
	// Deal with empty values   
	if(!YAHOO.lang.isValue(a)) {   
		return (!YAHOO.lang.isValue(b)) ? 0 : 1;   
	}   
	else if(!YAHOO.lang.isValue(b)) {   
		return -1;   
	}   
	
	a = parseFloat(a);
	b = parseFloat(b);
	    
	return YAHOO.util.Sort.compare(a, b, desc);   
}; 

VELOS.milestoneGrid.processColumns = function (mileTypeId,respJ,colArray){
	myFieldArray = [];
	myColumnDefs = [];
	var isIe = jQuery.browser.msie;	
	if (colArray) {
		var tempColumnDefs =[];
		myColumnDefs = colArray;
		tempColumnDefs = colArray;
		for (var iX=0; iX<myColumnDefs.length; iX++) {

			var indx = myColumnDefs[iX].newIndex; 
			if(mileTypeId == 'PM'){
				myColumnDefs[iX].width = (myColumnDefs[iX].key == 'recNum')?(isIe==true?55:25):
					(myColumnDefs[iX].key == 'mileType')?(isIe==true?110:80):
					(myColumnDefs[iX].key == 'patCount')?(isIe==true?80:50):
					(myColumnDefs[iX].key == 'patStatus')?(isIe==true?90:50):
					(myColumnDefs[iX].key == 'limit')?(isIe==true?60:40):(myColumnDefs[iX].key == 'payType')?(isIe==true?90:60):
					(myColumnDefs[iX].key == 'payFor')?(isIe==true?90:60):(isIe==true?80:50);
			}else if(mileTypeId == 'VM'){
			myColumnDefs[iX].width = (myColumnDefs[iX].key == 'recNum')?(isIe==true?55:25):
					(myColumnDefs[iX].key == 'mileRule')?(isIe==true?80:100):
					(myColumnDefs[iX].key == 'mileDesc')? (isIe==true?80:150):
					(myColumnDefs[iX].key == 'limit')?(isIe==true?60:40):
					(myColumnDefs[iX].key == 'patStatus')? (isIe==true?80:50):(myColumnDefs[iX].key == 'patCount') ? (isIe==true?80:50):(isIe==true?80:60);
			}else if(mileTypeId == 'EM'){
				myColumnDefs[iX].width = (myColumnDefs[iX].key == 'recNum')?(isIe==true?55:25):
					(myColumnDefs[iX].key == 'mileRule')?(isIe==true?80:100):(myColumnDefs[iX].key == 'mileDesc')? (isIe==true?80:150):
					(myColumnDefs[iX].key == 'patStatus')? (isIe==true?80:40) :(myColumnDefs[iX].key == 'event') ? (isIe==true?80:40): (myColumnDefs[iX].key == 'patCount') ? (isIe==true?80:40):(isIe==true?80:60);
			}else if(mileTypeId == 'SM'){
				myColumnDefs[iX].width = (myColumnDefs[iX].key == 'recNum')?(isIe==true?55:25):
					(myColumnDefs[iX].key == 'mileType')?(isIe==true?110:80):
					(myColumnDefs[iX].key == 'stdStatus')?(isIe==true?110:80):
					(myColumnDefs[iX].key == 'limit')?(isIe==true?60:40):
					(myColumnDefs[iX].key == 'payType')?(isIe==true?90:60):
					(myColumnDefs[iX].key == 'payFor')?(isIe==true?90:60):(isIe==true?100:70);
				}else{
				myColumnDefs[iX].width = (myColumnDefs[iX].key == 'recNum')?(isIe==true?55:25):
					(myColumnDefs[iX].key == 'mileRule') ?150:(myColumnDefs[iX].key == 'mileDesc')? 180:
					(myColumnDefs[iX].key == 'patStatus' || myColumnDefs[iX].key == 'patCount') ? 110:100;
			}
			myColumnDefs[iX].resizeable = true;
			//Fix #5968: making columns sortable
			myColumnDefs[iX].sortable = true;
			if (myColumnDefs[iX].type=="number" || myColumnDefs[iX].type=="integer"){
				myColumnDefs[iX].sortOptions = {sortFunction:VELOS.milestoneGrid.sortNumber};
			}

			if (myColumnDefs[iX].key == 'delete_'+mileTypeId) {
				myColumnDefs[iX].madeUp = true;
			}

			var ddArray = null;
			if (myColumnDefs[iX].key == 'mileType'){
				ddArray = respJ.milestoneType;
				myColumnDefs[iX].sortable = false;
			}
			if (myColumnDefs[iX].key == 'calendar')
				ddArray = respJ.studyProtocols;
			if (myColumnDefs[iX].key == 'visit')			
				ddArray = respJ.studyVisits;				
			if (myColumnDefs[iX].key == 'event')
				ddArray = respJ.studyEvents;
			if (myColumnDefs[iX].key == 'mileStatus'){
				ddArray = respJ.mileStatus;				
			}
			if (myColumnDefs[iX].key == 'payType')
				ddArray = respJ.milePayType;
			if (myColumnDefs[iX].key == 'payFor')
				ddArray = respJ.milePayFor;
			if (myColumnDefs[iX].key == 'patStatus')
				ddArray = respJ.patStatus;
			if (myColumnDefs[iX].key == 'evtStatus')
				ddArray = respJ.eventStatus;			
			if (myColumnDefs[iX].key == 'stdStatus')
					ddArray = respJ.studyStatus;
			if (myColumnDefs[iX].key == 'mileRule' && mileTypeId == 'EM')
				ddArray = respJ.EM_mileRule;	
			if (myColumnDefs[iX].key == 'mileRule' && mileTypeId == 'VM')
				ddArray = respJ.VM_mileRule;			
			
			if (ddArray != null){
				myColumnDefs[iX].editor = new YAHOO.widget.DropdownCellEditor({dropdownOptions:ddArray,disableBtns:true});	
			}

			if (myColumnDefs[iX].type!=undefined){
				var type= myColumnDefs[iX].type;
			 	var typeInfo = VELOS.milestoneGrid.types[type];
			 	if (typeInfo.parser!=undefined)
			 		myColumnDefs[iX].parser=typeInfo.parser;
			 	if (myColumnDefs[iX].formatter == undefined){
			 		if (typeInfo.formatter!=undefined)
			 			myColumnDefs[iX].formatter=typeInfo.formatter;
			 	}
			 	if (typeInfo.stringer!=undefined)
			 		myColumnDefs[iX].stringer=typeInfo.stringer;
				if (typeInfo.editorOptions!=undefined)
					myColumnDefs[iX].editor=new YAHOO.widget.TextboxCellEditor(typeInfo.editorOptions);
			}

			var fieldElem = [];
			/* FIN-22373 11-Oct-2012 -Sudhir*/
			if (myColumnDefs[iX].key == 'datefrom' || myColumnDefs[iX].key == 'dateto'){			
				myColumnDefs[iX].className = 'msGridDatefield';	
				myColumnDefs[iX].width = (isIe==true?80:60);
			}
		if (myColumnDefs[iX].key && myColumnDefs[iX].key == 'delete_'+mileTypeId) {
				myColumnDefs[iX].formatter = 'checkbox';
				myColumnDefs[iX].sortable = false;	
				myColumnDefs[iX].editor = 'checkbox';
				myColumnDefs[iX].width = (isIe==true?40:30);
				fieldElem['parser'] = 'checkbox';
			}
		//INF-22500
		if (myColumnDefs[iX].key && myColumnDefs[iX].key == 'selectAll_'+mileTypeId) {
			myColumnDefs[iX].formatter = 'checkbox';
			myColumnDefs[iX].sortable = false;	
			myColumnDefs[iX].editor = 'checkbox';
			myColumnDefs[iX].width = (isIe==true?40:30);
			myColumnDefs[iX].className="selectedCheckBox"
			fieldElem['parser'] = 'checkbox';
		}
			fieldElem['key'] = myColumnDefs[iX].key;
			myFieldArray.push(fieldElem);						
		}
	}
	return myColumnDefs;
	
}

//Added to Process Milestone Power Bar Grid Columns - INF-22500 : Raviesh
VELOS.milestoneGrid.processParentColumns = function (mileTypeId,respJ,colArrayParent){
	myParentFieldArray = [];
	myParentColumnDefs = [];
	var isIe = jQuery.browser.msie;	
	if (colArrayParent) {
		var tempParentColumnDefs =[];
		myParentColumnDefs = colArrayParent;
		tempParentColumnDefs = colArrayParent;
		for (var iX=0; iX<myParentColumnDefs.length; iX++) {

			var indx = myParentColumnDefs[iX].newIndex; 
			if(mileTypeId == 'VM_Parent'){
				myParentColumnDefs[iX].width = (myParentColumnDefs[iX].key == 'mileRule') ?
					(isIe==true?80:100):(myParentColumnDefs[iX].key == 'mileDesc')? (isIe==true?80:150):
				(myParentColumnDefs[iX].key == 'delete_'+mileTypeId)? 56:(myParentColumnDefs[iX].key == 'recNum')? 60:
				(myParentColumnDefs[iX].key == 'patStatus')? (isIe==true?80:50):(myParentColumnDefs[iX].key == 'patCount') ? (isIe==true?80:50):(isIe==true?80:60);
			}else if(mileTypeId == 'EM_Parent'){
				myParentColumnDefs[iX].width = (myParentColumnDefs[iX].key == 'mileRule') ?
						(isIe==true?80:100):(myParentColumnDefs[iX].key == 'mileDesc')? (isIe==true?80:150):
					(myParentColumnDefs[iX].key == 'delete_'+mileTypeId)? 56:(myParentColumnDefs[iX].key == 'recNum')? 60:
					(myParentColumnDefs[iX].key == 'patStatus')? (isIe==true?80:40) :(myParentColumnDefs[iX].key == 'event') ? (isIe==true?80:40): (myParentColumnDefs[iX].key == 'patCount') ? (isIe==true?80:40):(isIe==true?80:60);
				}else{
				myParentColumnDefs[iX].width = (myParentColumnDefs[iX].key == 'mileRule') ?
						150:(myParentColumnDefs[iX].key == 'mileDesc')? 200:
						(myParentColumnDefs[iX].key == 'delete_'+mileTypeId)? 56:(myParentColumnDefs[iX].key == 'recNum')? 60:
						(myParentColumnDefs[iX].key == 'patStatus' || myParentColumnDefs[iX].key == 'patCount') ? 110:100;
			}
			myParentColumnDefs[iX].resizeable = true;

			if (myParentColumnDefs[iX].type=="number" || myParentColumnDefs[iX].type=="integer"){
				myParentColumnDefs[iX].sortOptions = {sortFunction:VELOS.milestoneGrid.sortNumber};
			}

			if (myParentColumnDefs[iX].key == 'delete_'+mileTypeId) {
				myParentColumnDefs[iX].madeUp = true;
			}

			var ddArray = null;
			if (myParentColumnDefs[iX].key == 'mileType'){
				ddArray = respJ.milestoneType;
				myParentColumnDefs[iX].sortable = false;
			}
			if (myParentColumnDefs[iX].key == 'calendar')
				ddArray = respJ.studyProtocols;
			if (myParentColumnDefs[iX].key == 'visit')			
				ddArray = respJ.studyVisits;				
			if (myParentColumnDefs[iX].key == 'event')
				ddArray = respJ.studyEvents;
			if (myParentColumnDefs[iX].key == 'mileStatus'){
				ddArray = respJ.mileStatus;
			}
			if (myParentColumnDefs[iX].key == 'payType')
				ddArray = respJ.milePayType;
			if (myParentColumnDefs[iX].key == 'payFor')
				ddArray = respJ.milePayFor;
			if (myParentColumnDefs[iX].key == 'patStatus')
				ddArray = respJ.patStatus;
			if (myParentColumnDefs[iX].key == 'evtStatus')
				ddArray = respJ.eventStatus;			
			if (myParentColumnDefs[iX].key == 'stdStatus')
					ddArray = respJ.studyStatus;
			if (myParentColumnDefs[iX].key == 'mileRule' && mileTypeId == 'EM_Parent')
				ddArray = respJ.EM_mileRule;	
			if (myParentColumnDefs[iX].key == 'mileRule' && mileTypeId == 'VM_Parent')
				ddArray = respJ.VM_mileRule;			
			
			if (ddArray != null){
				myParentColumnDefs[iX].editor = new YAHOO.widget.DropdownCellEditor({dropdownOptions:ddArray,disableBtns:true});	
			}

			if (myParentColumnDefs[iX].type!=undefined){
				var type= myParentColumnDefs[iX].type;
			 	var typeInfo = VELOS.milestoneGrid.types[type];
			 	if (typeInfo.parser!=undefined)
			 		myParentColumnDefs[iX].parser=typeInfo.parser;
			 	if (myParentColumnDefs[iX].formatter == undefined){
			 		if (typeInfo.formatter!=undefined)
			 			myParentColumnDefs[iX].formatter=typeInfo.formatter;
			 	}
			 	if (typeInfo.stringer!=undefined)
			 		myParentColumnDefs[iX].stringer=typeInfo.stringer;
				if (typeInfo.editorOptions!=undefined)
					myParentColumnDefs[iX].editor=new YAHOO.widget.TextboxCellEditor(typeInfo.editorOptions);
			}

			var fieldElem = [];

			if (myParentColumnDefs[iX].key && myParentColumnDefs[iX].key == 'delete') {
				myParentColumnDefs[iX].formatter = 'checkbox';
				myParentColumnDefs[iX].editor = 'checkbox';
				fieldElem['parser'] = 'checkbox';
			} 
			fieldElem['key'] = myParentColumnDefs[iX].key;
			myParentFieldArray.push(fieldElem);						
		}
	}
	return myParentColumnDefs;
	
}

VELOS.milestoneGrid.updateMilestoneChangeList = function(pMileType,myGrid){
	//alert("Inside:1");
	var arrRecords = myGrid.getRecordSet();
	milestoneDataChangeList = [];

	var data = yuiDataTabletoJSON(myGrid);			
	milestoneDataChangeList[pMileType+'mileSeq'+mileSeq] = data;
}

VELOS.milestoneGrid.auditAction = function(mileTypeId, mileSeq, action, rowNum){
	if (milestoneRowChangeList == null)milestoneRowChangeList = [];
	
	var change=milestoneRowChangeList[mileTypeId+'mileSeq'+mileSeq];
	var prevAct ='';

	if(change == undefined){
		if(action=='Updated'){
			var paramArray = [rowNum];
			milestoneRowChangeList[mileTypeId+'mileSeq'+mileSeq] =getLocalizedMessageString("L_Update_Row",paramArray) /*'Updated Row #'+rowNum+''*****/;
			if (updateRows == null) updateRows = [];
			updateRows[mileTypeId+'mileSeq'+mileSeq] = rowNum;
		}else{
			var paramArray = [rowNum];
			milestoneRowChangeList[mileTypeId+'mileSeq'+mileSeq] = getLocalizedMessageString("L_Del_Row",paramArray)/*'Deleted Row #'+rowNum+''*****/;
		}
	}else{
		var pos =change.indexOf(' ');
		if (pos>0){
			prevAct = change.slice(0,pos);
		}else{
			prevAct = change;
		}

		if(prevAct=='Added'){
			if(action!='Updated'){
				var paramArray = [action,rowNum];
				milestoneRowChangeList[mileTypeId+'mileSeq'+mileSeq] = getLocalizedMessageString("L_Added_Row",paramArray)/*'Added/'+ action + ' Row #'+rowNum+''*****/;
			}
		}else if(prevAct=='Added/Updated'){
			if(action=='Updated'){
				var paramArray = [rowNum];
				milestoneRowChangeList[mileTypeId+'mileSeq'+mileSeq] =getLocalizedMessageString("L_AddUpdt_Row",paramArray) /*'Added/Updated Row #'+rowNum+''*****/;
			}else{
				var paramArray = [rowNum];
				milestoneRowChangeList[mileTypeId+'mileSeq'+mileSeq] = getLocalizedMessageString("M_AddUpdtDel_Row",paramArray)/*'Added/Updated/Deleted Row #'+rowNum+''*****/;
			}
		}else if(prevAct=='Deleted'){
			//This case is not valid
		}else if(prevAct=='Updated'){
			var paramArray = [rowNum];
			milestoneRowChangeList[mileTypeId+'mileSeq'+mileSeq] =getLocalizedMessageString("L_Update_Row",paramArray)/* 'Updated Row #'+rowNum+''*****/;
		}
	}
}

//Apply attributes to the newly created rows

VELOS.milestoneGrid.types = {
	'varchar':{
		editor:'textbox',
		editorOptions:{disableBtns:true},
		formatter:'varchar'
	},
	'date': {
		parser:'date',
		formatter:'date',
		editor:'date',
		className:'msGridDatefield',/* FIN-22373 11-Oct-2012 -Sudhir*/
		// I don't want the calendar to have the Ok-Cancel buttons below
		editorOptions:{
			disableBtns:true,
			validator: YAHOO.widget.DataTable.validateDate
		},
		// I want to send the dates to my server in the same format that it sends it to me
		// so I use this stringer to convert data on the way from the client to the server
		stringer: function (date) {
			return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() +
				' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
		}
	},
	integer: {
		formatter:'integer',
		parser:'number',
		// I am using my own RegularExpressionCellEditor, defined below
		editor:'regexp',
		// I'm only accepting digits (probably this data type should be called integer)
		editorOptions: {
			disableBtns:true,
			regExp:'^\\d{0,11}$'
			//validator: YAHOO.widget.DataTable.validateNumber
		}
	},
	number: {
		formatter:'number',
		parser:'number',
		// I am using my own RegularExpressionCellEditor, defined below
		editor:'regexp',
		// I'm only accepting digits (probably this data type should be called integer)
		editorOptions: {
			disableBtns:true,
			regExp:'^\\d{0,11}([.,]\\d{0,2})?$'
			//regExp:'^\\d{11}$',
			//validator: YAHOO.widget.DataTable.validateNumber
		}
	},
	currency: {
		parser:'number',
		formatter:'currency',
		editor:'regexp',
		// for currency I accept numbers with up to two decimals, with dot or comma as a separator
		// It won't accept currency signs and thousands separators really messes things up
		editorOptions: {
			disableBtns:true,
			regExp:'^\\d*([.,]\\d{0,2})?$',
			validator: function (value) {
				return parseFloat(value.replace(',','.'));
			}
		},
		// When I pop up the cell editor, if I don't change the dots into commas, the whole thing will look inconsistent.
		cellEditorFormatter: function(value) {
			return ('' + value).replace('.',',');
		},
		disableBtns:true
	},
	// This is the default, do-nothing data type.
	// There is no need to parse, format or stringify anything and if an editor is needed, the textbox editor will have to do.
	string: {
		editor:'textbox'
	}
};

VELOS.milestoneGrid.fnOnceEnterKeyPress = function(e) {
	try {
        if (e.keyCode == 13 || e.keyCode == 10) {
			if ('' == document.getElementById('eSignMessage').innerHTML) {
        		alert(PlsEnterEsign);/*alert('Please enter e-Signature.');*****/
        		return false;
			}
        	if (L_Valid_Esign != document.getElementById('eSignMessage').innerHTML) {
        		alert(document.getElementById('eSignMessage').innerHTML);
        		return false;
        	}
            var thisTimeSubmitted = new Date();
            if (!lastTimeSubmitted) { return true; }
            if (!thisTimeSubmitted) { return true; }
            if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
                return false;
            }
            lastTimeSubmitted = thisTimeSubmitted;
        }
	} catch(e) {}
	return true;
}


VELOS.milestoneGrid.validate = function(pMileType,myGrid,mileCount,totalCount) {
	totalCount++;
	var arrRecords = myGrid.getRecordSet();
	var mileSeq;
	var oRecord;	
	var deleteInfo
	var flag_invalid=0;	
	var showRecordNum='';
	var count=0;
	var alertdateDialog=null;
		if (!$('alertdateDialog')) {
				var alertdateDialog = document.createElement('div');
				alertdateDialog.setAttribute('id', 'alertdateDialog');
				alertdateDialog.innerHTML = '<div class="bd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; overflow: visible;"><br />&nbsp;'+M_DtToNotLessThan_DtFrom+'</div><div class="bd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; overflow: visible;" id="showChangeContent"></div>';
				$D.insertAfter(alertdateDialog, $('save_changes'));
			}
		var myDateAlertDialog = new YAHOO.widget.Dialog('alertdateDialog',
				{
			visible:false, fixedcenter:true, modal:true, resizeable:true,
			draggable:"true", autofillheight:"body", constraintoviewport:false
				});
		
		$('alertdateDialog').style.width = '26em';
		var handleCancel = function(e) {
			myDateAlertDialog.cancel();
			$('showChangeContent').innerHTML='';
			
		};
		var myButtons = [{ text: "Close", handler: handleCancel }] ;
		myDateAlertDialog.cfg.queueProperty("buttons", myButtons);
		myDateAlertDialog.render(document.body);
	if(pMileType == 'PM'){
		var patStatus;
		for(rec=0; rec < arrRecords.getLength(); rec++){
			oRecord = arrRecords.getRecord(rec);
			mileSeq = oRecord.getData('mileSeq');
			var patStatus = oRecord.getData('patStatus');	
			var mileStatus = oRecord.getData('mileStatus');
			deleteInfo = purgedMileArray[pMileType+'mileSeq'+mileSeq];			
			//alert("desc  "+patStatus+"  seq "+mileSeq+" deleteinfo:" +deleteInfo);  
			/* FIN-22373 11-Oct-2012 -Sudhir*/
			var datefrom = oRecord.getData('datefrom');	
			var dateto = oRecord.getData('dateto');
			var recNum = oRecord.getData('recNum');
			if(deleteInfo == undefined){
				if((datefrom!=null || datefrom!="undefined") && (dateto !=null || dateto!="undefined")){
					if(CompareDates(datefrom,dateto)){
						showRecordNum +='&nbsp;'+ 'Row # '+recNum+'<br/>';
						count++;
					}
				}
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'patCount', Pat_Count))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'patCount', 'Patient Count'))*****/
					return false;
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'patStatus', Patient_Status))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'patStatus', 'Patient Status'))*****/
					return false;
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileStatus', Mstone_Status))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileStatus', 'Milestone Status'))*****/
					return false;
			}
		}
		if(count>0){
			var paramArray = [Pat_StatusMstones];
			showChangeRowsContentHTML+= '&nbsp;'+getLocalizedMessageString("L_For_Cl",paramArray)/*'For '+ mileArr[mileCount].myGridName + ''*****/+':<br/>';
			showChangeRowsContentHTML+=showRecordNum;
			$('showChangeContent').innerHTML = showChangeRowsContentHTML;
			if(mileCount==1)
			showChangeRowsContentHTML = '';
			showRecordNum = '';
			count = 0;
			if(mileCount==1){
			myDateAlertDialog.show();
			return false;}
		}
		if(totalCount==mileCount && showChangeRowsContentHTML!='')
		{
			$('showChangeContent').innerHTML = showChangeRowsContentHTML;
			showChangeRowsContentHTML = '';
			myDateAlertDialog.show();
			return false;
		}
		return true;
	}
	else if(pMileType == 'VM' || pMileType == 'EM'){
		var patStatus;
		for(rec=0; rec < arrRecords.getLength(); rec++){
			oRecord = arrRecords.getRecord(rec);
			mileSeq = oRecord.getData('mileSeq');
			deleteInfo = purgedMileArray[pMileType+'mileSeq'+mileSeq];			
			//alert("1  "+evtStatus+"  2 "+event+" 3:" +deleteInfo + "4: " +calendar+ "5:" +visit +"6:" +mileRule +"7:" +mileStatus ); 
			/* FIN-22373 11-Oct-2012 -Sudhir*/
			var datefrom = oRecord.getData('datefrom');	
			var dateto = oRecord.getData('dateto');
			var recNum = oRecord.getData('recNum');
			if(deleteInfo == undefined){
				if((datefrom!=null || datefrom!="undefined") && (dateto !=null || dateto!="undefined")){
					if(CompareDates(datefrom,dateto)){
						showRecordNum +='&nbsp;'+ 'Row # '+recNum+'<br/>';
						count++;
					}
				}
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'calendar', Calendar))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'calendar', 'Calendar'))*****/
					return false;
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'visit', Visit))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'visit', 'Visit'))*****/
					return false;
				if(pMileType == 'EM'){
					if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'event', Event))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'event', 'Event'))*****/
						return false;
				}
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileRule', Milestone_Rule))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileRule', 'Milestone Rule'))*****/
					return false;
				
				 if(oRecord.getData('mileRule') != 'On Scheduled Date' ){		
					if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'evtStatus', Event_Status))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'evtStatus', 'Event Status'))*****/
						return false;
				 }
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileStatus', Mstone_Status))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileStatus', 'Milestone Status'))*****/
					return false;
			}
		}
		if(count>0){
			if(pMileType=='VM')
				var paramArray = [Visit_Mstones];
			else
				var paramArray = [Evt_Mstones];
			showChangeRowsContentHTML+= '&nbsp;'+getLocalizedMessageString("L_For_Cl",paramArray)/*'For '+ mileArr[mileCount].myGridName + ''*****/+':<br/>';
			showChangeRowsContentHTML+=showRecordNum;
			$('showChangeContent').innerHTML = showChangeRowsContentHTML;
			if(mileCount==1)
			showChangeRowsContentHTML = '';
			showRecordNum = '';
			count = 0;
			if(mileCount==1){
			myDateAlertDialog.show();
			return false;}
		}
		if(totalCount==mileCount && showChangeRowsContentHTML!='')
		{
			$('showChangeContent').innerHTML = showChangeRowsContentHTML;
			showChangeRowsContentHTML = '';
			myDateAlertDialog.show();
			return false;
		}
		return true;
		
	}
	else if(pMileType == 'SM'){
		var patStatus;
		for(rec=0; rec < arrRecords.getLength(); rec++){
			oRecord = arrRecords.getRecord(rec);
			mileSeq = oRecord.getData('mileSeq');
			deleteInfo = purgedMileArray[pMileType+'mileSeq'+mileSeq];			
			//alert("desc  "+stdStatus+"  seq "+mileSeq+" deleteinfo:" +deleteInfo+" mileStatus:" +mileStatus);  
			/* FIN-22373 11-Oct-2012 -Sudhir*/
			var datefrom = oRecord.getData('datefrom');	
			var dateto = oRecord.getData('dateto');	
			var recNum = oRecord.getData('recNum');
			if(deleteInfo == undefined){
				if((datefrom!=null || datefrom!="undefined") && (dateto !=null || dateto!="undefined")){
					if(CompareDates(datefrom,dateto)){
						showRecordNum +='&nbsp;'+ 'Row # '+recNum+'<br/>';
						count++;
					}
				}
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'stdStatus', Study_Status))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'stdStatus', 'Study Status'))*****/
					return false;
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileStatus', Mstone_Status))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileStatus', 'Milestone Status'))*****/
					return false;
			}
		}
		if(count>0){
			var paramArray = [StdPat_Mstone];
			showChangeRowsContentHTML+= '&nbsp;'+getLocalizedMessageString("L_For_Cl",paramArray)/*'For '+ mileArr[mileCount].myGridName + ''*****/+':<br/>';
			showChangeRowsContentHTML+=showRecordNum;
			$('showChangeContent').innerHTML = showChangeRowsContentHTML;
			if(mileCount==1)
			showChangeRowsContentHTML = '';
			showRecordNum = '';
			count = 0;
			if(mileCount==1){
			myDateAlertDialog.show();
			return false;}
		}
		if(totalCount==mileCount && showChangeRowsContentHTML!='')
		{
			$('showChangeContent').innerHTML = showChangeRowsContentHTML;
			showChangeRowsContentHTML = '';
			myDateAlertDialog.show();
			return false;
		}
		return true;
	}
	else if(pMileType == 'AM'){
		var desc='';	
		for(rec=0; rec < arrRecords.getLength(); rec++){
			oRecord = arrRecords.getRecord(rec);
			mileSeq = oRecord.getData('mileSeq');
			deleteInfo = purgedMileArray[pMileType+'mileSeq'+mileSeq];
			if(deleteInfo == undefined){
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileDesc', Milestone_Description))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileDesc', 'Milestone Description'))*****/
					return false;
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileStatus', Mstone_Status))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileStatus', 'Milestone Status'))*****/
					return false;
			}
		}
		if(totalCount==mileCount && showChangeRowsContentHTML!='')
		{
			$('showChangeContent').innerHTML = showChangeRowsContentHTML;
			showChangeRowsContentHTML = '';
			myDateAlertDialog.show();
			return false;
		}
		return true;
	}
}

VELOS.milestoneGrid.validateColumnContent = function(recCount, oRecord, columnKey, columnLabel) {
	var columnContent = oRecord.getData(columnKey);
	
	if(columnContent == undefined || columnContent.length == 0){
		var paramArray = [columnLabel,(recCount+1)];
		alert(getLocalizedMessageString("M_EtrFor_Row",paramArray));/*alert("Enter "+columnLabel+" for row #" + (recCount+1));*****/
		return false;
 	}
	return true;
}
//APR-13-11,BK ,FIXED #6016
VELOS.milestoneGrid.addRows = function(mileTypeId) {
	
	var mileFrom = $j("#mileFrom").val();
	var protocolId	="";
	var calendar	="";
	if(mileFrom=="manageMilestone"){
		var srchStrCalName = $j("#strCalName").val();
		protocolId	= 	GetQueryStringParams("protocolId");
		calendar	=	srchStrCalName;//GetQueryStringParams("calName");
	}
	var recordNum;
	var mileType;
	if(f_check_perm(pgRight,'N')){	
	if (mileTypeId == "PM"){
		mileType = Patient_Status;/*mileType = "Patient Status";*****/
		recordNum = PMrecordNum;
	} else if (mileTypeId == "VM"){				
		mileType = Visit;/*mileType = "Visit";*****/
		recordNum = VMrecordNum;
	} else if (mileTypeId == "EM"){
		mileType = Event;/*mileType = "Event";*****/
		recordNum = EMrecordNum;
	} else if (mileTypeId == "SM"){
		mileType = Study_Status;/*mileType = "Study Status";*****/
		recordNum = SMrecordNum;
	} else if (mileTypeId == "AM"){
		mileType = Additional;/*mileType = "Additional";*****/
		recordNum = AMrecordNum;
	}
	//alert(recordNum);	
	var cntFld = ""+mileTypeId+"_rowCount";	
	var count = eval(YAHOO.util.Dom.get(cntFld).value);	
	if(YAHOO.lang.isNumber(count)) {
		if (count > 0){//FIX #6281
			var myArray = [];
			for(var i=0;i<count;i++) {
				mileCount++;recordNum++;
				
				// Modified By Parminder Singh Bug 10672#
				while(milestoneRowChangeList[mileTypeId+'mileSeq'+mileCount]!=undefined)
				{
				milestoneRowChangeList[mileTypeId+'mileSeq'+mileCount]=milestoneRowChangeList[mileTypeId+'mileSeq'+mileCount];
				mileCount++;
				}
				
				//FIX #6048
				/*var dataJSON = {"mileSeq":mileCount,"mileTypeId":mileTypeId,"mileType":mileType,
						"amount":0,"limit":'',"mileId":0,"patCount":'-1',
						"evtStatus":'',"evtStatusId":0,
						"patStatus":'',"patStatusId":0,
						"mileAchievedCount":0,"mileDesc":'',"mileStaticStatusId":0,
						"payTypeId":defaultPayTypeId, "payType":defaultPayType,"payForId":0, "payFor":'',
						"recNum":recordNum};*/
				//Modified for Holdback Implementation - FIN-22301 : Raviesh
				if (mileTypeId == "PM"){
					var dataJSON = {"mileSeq":mileCount,"mileTypeId":mileTypeId,"mileType":mileType,
						"amount":0,"holdBack":0.00,"limit":'',"mileId":0,"patCount":'1',"patStatus":'',"patStatusId":0,
						"mileStatus":'',"mileAchievedCount":0,"isReconciledAmount":0,"isInvoiceGenerated":0,"mileStaticStatusId":0,
						"payTypeId":defaultPayTypeId, "payType":defaultPayType,"payForId":0, "payFor":'',
						"recNum":recordNum};
				} else if (mileTypeId == "VM"){				
					var dataJSON = {"mileSeq":mileCount,"mileTypeId":mileTypeId,"mileType":mileType,
						"amount":0,"holdBack":0.00,"limit":'',"mileId":0,"patCount":'1',
						"evtStatus":'',"evtStatusId":0,
						"patStatus":'',"patStatusId":0,
						"calendarId":protocolId ,"calendar":calendar,
						"mileStatus":'',"mileAchievedCount":0,"isReconciledAmount":0,"isInvoiceGenerated":0,"mileStaticStatusId":0,
						"payTypeId":defaultPayTypeId, "payType":defaultPayType,"payForId":0, "payFor":'',
						"recNum":recordNum};
				} else if (mileTypeId == "EM"){
					var dataJSON = {"mileSeq":mileCount,"mileTypeId":mileTypeId,"mileType":mileType,
						"amount":0,"holdBack":0.00,"limit":'',"mileId":0,"patCount":'1',
						"evtStatus":'',"evtStatusId":0,
						"patStatus":'',"patStatusId":0,
						"calendarId":protocolId ,"calendar":calendar,
						"mileStatus":'',"mileAchievedCount":0,"isReconciledAmount":0,"isInvoiceGenerated":0,"mileStaticStatusId":0,
						"payTypeId":defaultPayTypeId, "payType":defaultPayType,"payForId":0, "payFor":'',
						"recNum":recordNum};
				} else if (mileTypeId == "SM"){
					var dataJSON = {"mileSeq":mileCount,"mileTypeId":mileTypeId,"mileType":mileType,
						"amount":0,"holdBack":0.00,"limit":'',"mileId":0,
						"mileStatus":'',"mileAchievedCount":0,"isReconciledAmount":0,"isInvoiceGenerated":0,"mileStaticStatusId":0,
						"payTypeId":defaultPayTypeId, "payType":defaultPayType,"payForId":0, "payFor":'',
						"recNum":recordNum};
				} else if (mileTypeId == "AM"){
					var dataJSON = {"mileSeq":mileCount,"mileTypeId":mileTypeId,"mileType":mileType,
						"amount":0,"holdBack":0.00,"limit":'',"mileId":0,
						"mileStatus":'',"mileAchievedCount":0,"isReconciledAmount":0,"isInvoiceGenerated":0,"mileDesc":'',"mileStaticStatusId":0,
						"payTypeId":defaultPayTypeId, "payType":defaultPayType,"payForId":0, "payFor":'',
						"recNum":recordNum};
				}
				var record = YAHOO.widget.DataTable._cloneObject(dataJSON);
				record.row = i;			
				myArray.push(record);
				if(milestoneRowChangeList==null) milestoneRowChangeList =[];
				var paramArray = [recordNum];
				milestoneRowChangeList[mileTypeId+'mileSeq'+mileCount] = getLocalizedMessageString("L_Add_Row",paramArray)/*'Added Row #'+recordNum;*****/;		
			}
			if (mileTypeId == "PM"){
				PMrecordNum = recordNum;
				myPMGrid.addRows(myArray);
			}
			else if(mileTypeId == "VM"){
				VMrecordNum = recordNum;
				myVMGrid.addRows(myArray);
			}
			else if(mileTypeId == "EM"){
				EMrecordNum = recordNum;
				myEMGrid.addRows(myArray);
			}
			else if(mileTypeId == "SM"){
				SMrecordNum = recordNum;
				mySMGrid.addRows(myArray);
			}
			else{
				AMrecordNum = recordNum;
				myAMGrid.addRows(myArray);
			}
			//myGrid.addRows(myArray);
			//if (count>0){
				//VELOS.milestoneGrid.applyRowAttribs(myGrid,count);
			//}
			YAHOO.util.Dom.get(cntFld).value = "0";		
			return;
		}
	 }
	YAHOO.log(CldNtCont_InvldIndx);/*YAHOO.log("Could not continue due to invalid index.");*****/
	YAHOO.util.Dom.get(mileTypeId+"_rowCount").value = "0";
 }	
}
var isSaveAll=false;
VELOS.milestoneGrid.saveDialogForAll=function(){
	isSaveAll=true;
	var accorClass='ui-accordion-header ui-helper-reset ui-state-active ui-corner-top';
	var mileArr=[{id:'#PatientMileStone',grid:selectedGridPM,mileType:"PM",myGridName:Pat_StatusMstones},
	             {id:'#VisitMilestone',grid:selectedGridVM,mileType:"VM",myGridName:Visit_Mstones},
	             {id:'#EventMilestone',grid:selectedGridEM,mileType:"EM",myGridName:Evt_Mstones},
	             {id:'#StudyMilestone',grid:selectedGridSM,mileType:"SM",myGridName:StdPat_Mstone},
	             {id:'#AdditionalMilestone',grid:selectedGridAM,mileType:"AM",myGridName:Addl_Mstones}];

	var datatoSaveForMile=[];
	for(var mile=0; mile< mileArr.length;mile++){
		var milestone=mileArr[mile];
		var isOpen=jQuery("#mile"+milestone.mileType).hasClass(accorClass);
		if(isOpen){
			datatoSaveForMile.push(milestone)	
		}
	}
	VELOS.milestoneGrid.saveDialog('',datatoSaveForMile);
	isSaveAll=false;
	
}

VELOS.milestoneGrid.saveDialog = function(pMileTypeId,datatoSaveForMile) {
	var myGrid = (pMileTypeId=='PM')? myPMGrid :
		(pMileTypeId=='VM')? myVMGrid :
		(pMileTypeId=='EM')? myEMGrid :
		(pMileTypeId=='SM')? mySMGrid : myAMGrid;
	var myGridName = (pMileTypeId=='PM')? Pat_StatusMstones/*'Patient Status Milestones'*****/ :
		(pMileTypeId=='VM')?  Visit_Mstones/*'Visit Milestones'*****/ :
		(pMileTypeId=='EM')?  Evt_Mstones/*'Event Milestones' *****/:
		(pMileTypeId=='SM')?  StdPat_Mstone/*'Study Status Milestones'******/ : Addl_Mstones/*'Additional Milestones'*****/;
	var regularExp;
	var otherRegularExp =  new RegExp('mileSeq[0-9]+$');
	
	//INF--22500
	if (pMileTypeId=='PM'){	myGrid = selectedGridPM;	}
	if (pMileTypeId=='VM'){	myGrid = selectedGridVM;	}
	if (pMileTypeId=='EM'){	myGrid = selectedGridEM;	}
	if (pMileTypeId=='SM'){	myGrid = selectedGridSM;	}
	if (pMileTypeId=='AM'){	myGrid = selectedGridAM;	}
	
	var mileArr=isSaveAll?datatoSaveForMile:[{grid:myGrid,mileType:pMileTypeId,myGridName:myGridName}];
	var changeRowsContentHTML='';
	var changeFormContentHTML='';
	var totalChanges = 0;	var saveDialog=null;	var noChange = true; var valid1=false; var alertmsg='';
	var totalMilestones=mileArr.length;
	for(var mileCount=0;mileCount<mileArr.length;mileCount++){
		mileArr[mileCount].grid.saveCellEditor();
		regularExp =  new RegExp('^'+mileArr[mileCount].mileType+'mileSeq[0-9]+$'); 
   if(numCheckValid == false){
	   valid1=VELOS.milestoneGrid.validate(mileArr[mileCount].mileType,mileArr[mileCount].grid,mileArr.length,mileCount);
	if(valid1){
		if(mileCount==0){
		 saveDialog = document.createElement('div');
		if (!$('saveDialog')) {
			saveDialog.setAttribute('id', 'saveDialog');
			saveDialog.innerHTML  = '<div class="hd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; width=\"23em\"">'+CnfrmMstone_Change/*Confirm Milestone Changes*****/+'</div>';
			saveDialog.innerHTML += '<div class="bd" id="insertForm" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; overflow: visible;"></div>';
			$D.insertAfter(saveDialog, $('save_changes'));
			
		}
		$('insertForm').innerHTML = '<form id="dialogForm" method="POST" action="updateMilestone.jsp?'+incomingUrlParams+'"><div id="changeRowsContent"></div><br/><div id="insertFormContent"></div></form>';
		}
		var numChanges = 0;
	
		var updateJSON = "[" ; 
		var mileVal ="";
		var mileValArr = {};
		var rows = 0;
		var maxChangesDisplayedBeforeScroll = 25;
		var subContent='';	
		for (oKey in milestoneRowChangeList) {
			var mileTypeId = oKey.slice(0,oKey.indexOf('mileSeq'));
		
			if (mileTypeId == mileArr[mileCount].mileType){
				if (oKey.match(regularExp) && milestoneRowChangeList[oKey] != undefined) {
					noChange = false;
					if(purgedMileArray[oKey]==undefined){
						var mileSeq = oKey.slice(oKey.indexOf('mileSeq')+7);
						subContent += milestoneRowChangeList[oKey]+'<br/>';
						numChanges++;
						if(updateRows[oKey] != undefined){
							if(rows != 0){
								updateJSON += ",";
							}						
							updateJSON += "{\""+rows+"\":\""+updateRows[oKey]+"\"}";
							rows ++;
						}
					}
				}
			}
		}
		updateJSON += "]";
		for (oKey in purgedMileArray) {
			var mileTypeId = oKey.slice(0,oKey.indexOf('mileSeq'));
			if (mileTypeId == mileArr[mileCount].mileType){
				if (oKey.match(regularExp) && purgedMileArray[oKey] != undefined) {
					noChange = false;
					var mileSeq = oKey.slice(oKey.indexOf('mileSeq')+7);
					subContent += purgedMileArray[oKey]+'<br/>';
					numChanges++;
				}
			}
		}
		if (numChanges > 0){
			var paramArray = [mileArr[mileCount].myGridName];
			if(numChanges>0){
			 changeRowsContentHTML+= '<b>'+getLocalizedMessageString("L_For_Cl",paramArray)/*'For '+ mileArr[mileCount].myGridName + ''*****/+':</b><br/>' + $('changeRowsContent').innerHTML;
			 changeRowsContentHTML+=subContent;
			 mileArr[mileCount].grid.sortColumn(mileArr[mileCount].grid.getColumn('recNum'),"yui-dt-asc");
			 for (oKey in purgedMileArray) {
					var mileTypeId = oKey.slice(0,oKey.indexOf('mileSeq'));
					if (mileTypeId == mileArr[mileCount].mileType){
						if (purgedMileArray[oKey] == undefined) {
							var mileSeq = oKey.slice(oKey.indexOf('mileSeq')+7);
							var Parent = $D.getElementsByClassName('yui-dt-col-mileSeq', 'td', mileArr[mileCount].grid.getTableEl());
							for (var iX=0; iX<Parent.length; iX++) {
								var el = new YAHOO.util.Element(Parent[iX]);
								var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
								var mileSeqIdTd = parent.getElementsByClassName('yui-dt-col-mileSeq', 'td')[0];
								var mileSeqIdEl = new YAHOO.util.Element(mileSeqIdTd);
								var mileSeqId = mileSeqIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
								var mileIdTd = parent.getElementsByClassName('yui-dt-col-mileId', 'td')[0];
								var mileIdEl = new YAHOO.util.Element(mileIdTd);
								var mileId = mileIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
								if(mileSeqId==mileSeq && mileId>=0)
								{
									var checkboxTd = parent.getElementsByClassName('yui-dt-col-delete_'+mileTypeId, 'td')[0];
									var checkboxEl = new YAHOO.util.Element(checkboxTd);
									var parentDiv = checkboxEl.getElementsByClassName('yui-dt-liner', 'div')[0];
									var parentDivEl = new YAHOO.util.Element(parentDiv);
									var chkBox = parentDivEl.getElementsByClassName('yui-dt-checkbox', 'input')[0];
									chkBox.checked = (!chkBox.checked)
								}
							}
						}
					}
				}
			var otherRegularExp =  new RegExp('%$');
			jQuery("td.yui-dt-col-holdBack >div.yui-dt-liner ").each(function(){
				if(this.innerHTML.match(otherRegularExp)==null )
				jQuery(this).text(YAHOO.util.Number.format(this.innerHTML, twoDecimalNumber)+'%');
			});
			var mileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', mileArr[mileCount].grid.getTableEl());
			VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(mileIdCells,mileArr[mileCount].mileType);		
			var mileValString = yuiDataTabletoJSON(mileArr[mileCount].grid);
			mileValString=mileValString.replace(/\\/g, "");
			mileValArr = JSON.parse(mileValString);
			for(var i = 0; i < mileValArr.length; i++) {
				if(mileValArr[i]["mileTypeId"]!='AM'){
				if(mileValArr[i]["datefrom"]==''){
					mileValArr[i]["datefrom"]='undefined';
					//delete mileValArr[i]["datefrom"];
				}
				else if(mileValArr[i]["datefrom"].indexOf("/") !== -1)
					mileValArr[i]["datefrom"]='velquote'+mileValArr[i]["datefrom"]+'velquote';
				if(mileValArr[i]["dateto"]=='')
					mileValArr[i]["dateto"]='undefined';
					//delete mileValArr[i]["dateto"];
				else if(mileValArr[i]["dateto"].indexOf("/") !== -1)
					mileValArr[i]["dateto"]='velquote'+mileValArr[i]["dateto"]+'velquote';
				}
				if(mileValArr[i]["patStatus"]=='')
					delete mileValArr[i]["patStatus"];
				else {
					if(mileValArr[i]["patStatus"] && (mileValArr[i]["patStatus"].indexOf(" ") !== -1 || mileValArr[i]["patStatus"].indexOf("\\") !== -1 || mileValArr[i]["patStatus"].indexOf("/") !== -1))
					mileValArr[i]["patStatus"]='velquote'+mileValArr[i]["patStatus"]+'velquote';
				}
				if(mileValArr[i]["limit"]=='')
					mileValArr[i]["limit"]=null;
				if(mileValArr[i]["payFor"]=='')
					delete mileValArr[i]["payFor"];
				else {
					if(mileValArr[i]["payFor"] && (mileValArr[i]["payFor"].indexOf(" ") !== -1 || mileValArr[i]["payFor"].indexOf("\\") !== -1 || mileValArr[i]["payFor"].indexOf("/") !== -1))
					mileValArr[i]["payFor"]='velquote'+mileValArr[i]["payFor"]+'velquote';
				}
				if(mileValArr[i]["evtStatus"]=='')
					delete mileValArr[i]["evtStatus"];
				else {
					if(mileValArr[i]["evtStatus"] && (mileValArr[i]["evtStatus"].indexOf(" ") !== -1 || mileValArr[i]["evtStatus"].indexOf("\\") !== -1 || mileValArr[i]["evtStatus"].indexOf("/") !== -1))
					mileValArr[i]["evtStatus"]='velquote'+mileValArr[i]["evtStatus"]+'velquote';
				}
				if(mileValArr[i]["mileDesc"]){
				if(mileValArr[i]["mileDesc"].indexOf(" ") !== -1 || mileValArr[i]["mileDesc"].indexOf("\\") !== -1 || mileValArr[i]["mileDesc"].indexOf("/") !== -1)
					mileValArr[i]["mileDesc"]='velquote'+mileValArr[i]["mileDesc"]+'velquote';
			     }
				if(mileValArr[i]["stdStatus"] && (mileValArr[i]["stdStatus"].indexOf(" ") !== -1 || mileValArr[i]["stdStatus"].indexOf("\\") !== -1 || mileValArr[i]["stdStatus"].indexOf("/") !== -1))
					mileValArr[i]["stdStatus"]='velquote'+mileValArr[i]["stdStatus"]+'velquote';
			    delete mileValArr[i]["recNum"];
			    delete mileValArr[i]["mileSeq"];
			    delete mileValArr[i]["isReconciledAmount"];
			    delete mileValArr[i]["isInvoiceGenerated"];
			    delete mileValArr[i]["mileAchievedCount"];
			    delete mileValArr[i]["event"];
			    delete mileValArr[i]["calendar"];
			    delete mileValArr[i]["visit"];
			    delete mileValArr[i]["mileRule"];
			    delete mileValArr[i]["mileType"];
			}
			mileVal = JSON.stringify(mileValArr);
			mileVal = htmlEncode(mileVal);
			updateJSON = htmlEncode(updateJSON);
			//changeFormContentHTML += '<input type="hidden" name="mileType" value="'+mileVal+'" />';
			changeFormContentHTML += '<input type="hidden" name="myMilestoneGrid" value="'+mileVal+'" />';
			changeFormContentHTML += '<input type="hidden" name="updateInfo" value="'+updateJSON+'" />';
			}
		}
		totalChanges+=numChanges;
		if (totalChanges < maxChangesDisplayedBeforeScroll) {
			$('insertForm').style.width = null;
			$('insertForm').style.height = null;
			$('insertForm').style.overflow = 'visible';
		} else {
			$('insertForm').style.width = '51em';
			$('insertForm').style.height = '35em';
			$('insertForm').style.overflow = 'scroll';
		}
		if($('insertFormContentOne')){
			$('insertFormContentOne').innerHTML =""; //YK: 26APR2011 Bug#6008 
		}
		var myDialog = new YAHOO.widget.Dialog('saveDialog',
				{
					visible:false, fixedcenter:true, modal:true, resizeable:true,
					draggable:"true", autofillheight:"body", constraintoviewport:false
				});
		var handleCancel = function(e) {
			myDialog.cancel();
		};
		var handleSubmit = function(e) {
			try {
				if ('' == document.getElementById('eSignMessage').innerHTML) {
					alert(PlsEnterEsign);/*alert('Please enter e-Signature.');*****/
	        		return false;
				}
		        var thisTimeSubmitted = new Date();
		        if (lastTimeSubmitted && thisTimeSubmitted) {
		        	if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) { return false; }
			        lastTimeSubmitted = thisTimeSubmitted;
		        }
			} catch(e) {}
			myDialog.submit();
		};
		var onButtonsReady = function() {
		    if (!$E.getListeners($('dialog_submit'))) { $E.addListener($('dialog_submit'), 'click', handleSubmit, false, true); }
		    if (!$E.getListeners($('dialog_cancel'))) { $E.addListener($('dialog_cancel'), 'click', handleCancel); }
		}
		YAHOO.util.Event.onContentReady("saveDialog", onButtonsReady);
		var myButtons = noChange ?
			[   { text: getLocalizedMessageString("L_Close"), handler: handleCancel } ] :
			[
				{ text: getLocalizedMessageString('L_Save'),   handler: handleSubmit },
				{ text: getLocalizedMessageString('L_Cancel'), handler: handleCancel }
			];
		var onSuccess = function(o) {
			hideTransitPanel();
			if(document.getElementById("saveDialog")!=undefined){
				var saveDialog = document.getElementById("saveDialog");
				saveDialog.parentNode.removeChild(saveDialog);
			}
			var respJ = null; // response in JSON format
			try {
				respJ = $J.parse(o.responseText);
			} catch(e) {
				// Log error here
				return;
			}
			if (respJ.result == 0) {
				//Parminder Singh : Bug#15506
				for (oKey in milestoneRowChangeList) {
					var mileTypeId = oKey.slice(0,oKey.indexOf('mileSeq'));
					if (mileTypeId == pMileTypeId){			
						milestoneRowChangeList[oKey] = undefined;
						milestoneRowChangeListRemainder[oKey] = undefined;
					}
				}
				for (oKey in purgedMileArray) {
					var mileTypeId = oKey.slice(0,oKey.indexOf('mileSeq'));
					if (mileTypeId == pMileTypeId){
						purgedMileArray[oKey] = undefined;
						purgedMileArrayRemainder[oKey] = undefined;
					}
				}
				// alert("Your data was successfully submitted. The response was: " + respJ.resultMsg);
				showFlashPanel(respJ.resultMsg);
				//Very Important Fix #6184, 6273, 6274
				if(mileArr[mileCount]!=undefined)
				VELOS.milestoneGrid.flushListAndResetRemainder(mileArr[mileCount].mileType);
				//Parminder Singh : Bug#14939, #15508
				var milestoneStatusVal 	= 	$j.trim($j("#milestoneStat").val());
				var milestPayTypeVal 	= 	$j.trim($j("#milestPayType").val());
				var calNameVal 			=	$j.trim($j("#calName").val());
				var visitNameVal 		=	$j.trim($j("#visitName").val());
				var evtNameVal 			=	$j.trim($j("#evtName").val());
				var datefrom			= 	$j.trim($j("#datefrom").val());
				var dateto				= 	$j.trim($j("#dateto").val());
				var inactiveFlag 		= 	chkExcludeMilestone();
				if(pMileTypeId=='PM'){
					mileIdStatusPM = [];
					setTimeout(function() {reloadMilestoneGridBySearch('PM',milestoneStatusVal,milestPayTypeVal,calNameVal,visitNameVal,evtNameVal,'Y',inactiveFlag,datefrom,dateto) }, 1500);}
				else if(pMileTypeId=='VM'){
					mileIdStatusVM = [];
					setTimeout(function() {reloadMilestoneGridBySearch('VM',milestoneStatusVal,milestPayTypeVal,calNameVal,visitNameVal,evtNameVal,'Y',inactiveFlag,datefrom,dateto) }, 1500);}
				else if(pMileTypeId=='EM'){
					mileIdStatusEM = [];
					setTimeout(function() {reloadMilestoneGridBySearch('EM',milestoneStatusVal,milestPayTypeVal,calNameVal,visitNameVal,evtNameVal,'Y',inactiveFlag,datefrom,dateto) }, 1500);}
				else if(pMileTypeId=='SM'){
					mileIdStatusSM = [];
					setTimeout(function() {reloadMilestoneGridBySearch('SM',milestoneStatusVal,milestPayTypeVal,calNameVal,visitNameVal,evtNameVal,'Y',inactiveFlag,datefrom,dateto) }, 1500);}
				else {
					mileIdStatusAM = [];
					if(pMileTypeId=='AM'){
						setTimeout(function() {reloadMilestoneGridBySearch('AM',milestoneStatusVal,milestPayTypeVal,calNameVal,visitNameVal,evtNameVal,'Y',inactiveFlag,datefrom,dateto) }, 1500);
						}
					else{
						if (window.parent.searchMilestones('')) { setTimeout('window.parent.searchMilestones(\'\');', 1500); }}
				     }
			} else {
				var paramArray = [respJ.resultMsg];
				alert(getLocalizedMessageString("L_Error_Cl",paramArray));/*alert("Error: " + respJ.resultMsg);*****/
			}
		};
		var onFailure = function(o) {
			hideTransitPanel();
			
			var paramArray = [o.status];
			alert(getLocalizedMessageString("M_CommErr_CnctVelosSupp",paramArray));/*alert("Communication Error. Please contact Velos Support: " + o.status);*****/
		};
		var onStart = function(o) {
			showTransitPanel(PleaseWait_Dots);/*showTransitPanel('Please wait...');*****/
		}
		myDialog.callback.customevents = { onStart:onStart };
		myDialog.callback.success = onSuccess;
		myDialog.callback.failure = onFailure;
		myDialog.cfg.queueProperty("buttons", myButtons);
		myDialog.render(document.body);
		if(mileCount== totalMilestones-1){
			if (noChange) {
				changeFormContentHTML += '<table width="220px"><tr><td style="font-size:8pt;">&nbsp;&nbsp;'+ThereNoChg_Save/*There are no changes to save.*****/+'&nbsp;&nbsp;</td></tr></table>';
				noChange = true;
			} else {

					
					if(M_eSignConfig=='userpxd'){
						changeFormContentHTML +=Total_Chg/*Total changes*****/+': '+totalChanges+'<br/><table><tr><td width="150px"><span id="eSignMessage" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"></span></td>'+
						'<td style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;">'+'e-Password'/*e-Signature*****/+'<FONT class="Mandatory">*</FONT>&nbsp</td>'+'<td><input type="password" name="eSign" id="eSign"  autocomplete="off" style="background:url(./images/ic_lock_outline_gray_24dp.png) no-repeat; padding-left: 25px;" class="input21" onkeyup="ajaxvalidate(\'pass:\'+this.id,-1,\'eSignMessage\',\'Valid-Password\',\'Invalid-Password\',\'sessUserId\')"'+
						' />&nbsp;</td></tr></table>';
					}else{
						changeFormContentHTML +=Total_Chg/*Total changes*****/+': '+totalChanges+'<br/><table><tr><td width="150px"><span id="eSignMessage" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"></span></td>'+
						'<td style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;">'+Esignature/*e-Signature*****/+'<FONT class="Mandatory">*</FONT>&nbsp</td>'+'<td><input type="password" name="eSign" id="eSign" maxlength="8" autocomplete="off"  onkeyup="ajaxvalidate(\'misc:\'+this.id,4,\'eSignMessage\',\''+Valid_Esign/*Valid e-Sign*****/+'\',\''+Invalid_Esign/*Invalid e-Sign*****/+'\',\'sessUserId\')" '+
						' onkeypress="return VELOS.milestoneGrid.fnOnceEnterKeyPress(event)" '+
						' />&nbsp;</td></tr></table>';





					}
					
					
					
					
					
				

				changeFormContentHTML  +='<input id="changeCount" name="changeCount" type="hidden" value="'+totalChanges+'"/>';
				if(valid1==true){
					alertmsg='<div class="bd alertmsg" id="alertmsg" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; overflow: visible;">'+M_DelAch_UsingBtn+'</div>';
					}
			}
			$('changeRowsContent').innerHTML=changeRowsContentHTML;
			$('insertFormContent').innerHTML=changeFormContentHTML;
			myDialog.show();
			if($j("#alertmsg").length>=0){
				$j(".alertmsg").remove();
				$j(".ft").after(alertmsg);
			}
			alertmsg='';
			changeRowsContentHTML='';
			changeFormContentHTML='';
		}
		if (document.getElementById('eSign')) { document.getElementById('eSign').focus(); }
	
		var showFlashPanel = function(msg) {
			// Initialize the temporary Panel to display while waiting for external content to load
			if (!(VELOS.flashPanel)) {
				VELOS.flashPanel =
					new YAHOO.widget.Panel("flashPanel",
						{ width:"240px",
						  fixedcenter:true,
						  close:false,
						  draggable:false,
						  zindex:4,
						  modal:true,
						  visible:false
						}
					);
			}
			VELOS.flashPanel.setHeader("");
			VELOS.flashPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"><b>'+msg+'</b></td></tr><table>');
			VELOS.flashPanel.render(document.body);
			VELOS.flashPanel.show();
			setTimeout('VELOS.flashPanel.hide();', 1500);
		}
	
		var showTransitPanel = function(msg) {
			if (!VELOS.transitPanel) {
				VELOS.transitPanel =
					new YAHOO.widget.Panel("transitPanel",
						{ width:"240px",
						  fixedcenter:true,
						  close:false,
						  draggable:false,
						  zindex:4,
						  modal:true,
						  visible:false
						}
					);
			}
			VELOS.transitPanel.setHeader("");
			VELOS.transitPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"><b>'+msg+'</b></td></tr><table>');
			VELOS.transitPanel.render(document.body);
			VELOS.transitPanel.show();
		}
	
		var hideTransitPanel = function() {
			if (VELOS.transitPanel) { VELOS.transitPanel.hide(); }
		}
	}
	else{
		if($j(".alertmsg").length>=0)
		$j("#alertmsg").remove();
	}
   }}
	//For Bug#14742
	$j(function() {
		if ($j("input:submit")){
			$j( "input:submit").button();
		}
		if ($j("a:submit")){
			$j( "a:submit").button();
		}
		if ($j("button")){
			$j("button").button();
		}
	});	
	
}

VELOS.milestoneGrid.decodeData = function(oData) {
	oData = oData.replace(/&lt;+/g,"<"); //replace less than
	oData = oData.replace(/&gt;+/g,">"); //replace greater than
	oData = oData.replace(/&amp;+/g,"&"); //replace &
	return oData;
}

VELOS.milestoneGrid.encodeData = function(oData) {	
	oData = oData.replace(/[&]+/g,"&amp;"); //replace &
	oData = oData.replace(/[<]+/g,"&lt;"); //replace less than
	oData = oData.replace(/[>]+/g,"&gt;"); //replace greater than	
	return oData;
}

VELOS.milestoneGrid.prototype.initConfigs = function(args) {
	if (!args) { return false; }
	if (args.constructor != Object) { return false; }
	if (args.urlParams) { this.urlParams = args.urlParams; }
	else { this.dataTable = 'serverpagination'; }
	if ((args.success) && (typeof args.success == 'function')) {
		this.handleSuccess = args.success;
	}
	if ((args.failure) && (typeof args.failure == 'function')) {
		this.handleSuccess=args.success;
	}
	return true;
}

VELOS.milestoneGrid.flushListAndResetRemainder = function (pMileTypeId){
	var regularExp =  new RegExp('^'+pMileTypeId+'mileSeq[0-9]+$'); 
	var otherRegularExp =  new RegExp('mileSeq[0-9]+$');
	
	for (oKey in milestoneRowChangeList) {
		var mileTypeId = oKey.slice(0,oKey.indexOf('mileSeq'));
		if (mileTypeId == pMileTypeId){			
			milestoneRowChangeList[oKey] = undefined;
			milestoneRowChangeListRemainder[oKey] = undefined;
		}else{
			if (oKey.match(otherRegularExp) && milestoneRowChangeList[oKey] != undefined) {
				milestoneRowChangeListRemainder[oKey] = milestoneRowChangeList[oKey];
			}
		}
	}
				
	for (oKey in purgedMileArray) {
		var mileTypeId = oKey.slice(0,oKey.indexOf('mileSeq'));
		if (mileTypeId == pMileTypeId){
			purgedMileArray[oKey] = undefined;
			purgedMileArrayRemainder[oKey] = undefined;
		}else{
			if (oKey.match(otherRegularExp) && purgedMileArray[oKey] != undefined) {
				purgedMileArrayRemainder[oKey] = purgedMileArray[oKey];
			}
		}
	}
	
	//FIX #6286
	for (oKey in updateRows) {
		var mileTypeId = oKey.slice(0,oKey.indexOf('mileSeq'));
		if (mileTypeId == pMileTypeId){
			updateRows[oKey] = undefined;
			updateRowsRemainder[oKey] = undefined;
		}else{
			if (oKey.match(otherRegularExp) && updateRows[oKey] != undefined) {
				updateRowsRemainder[oKey] = updateRows[oKey];
			}
		}
	}		
}

VELOS.milestoneGrid.flushRecordList = function (pMileTypeId,mileSeq,oOldData,oNewData,parentId,columnKey){
	var fieldExists = false;
	var count = 0;
	var undefCount=0;
	for(okey1 in mileFieldsList){
		if(!(mileFieldsList[okey1]==undefined) && okey1==parentId+"-"+columnKey){
			fieldExists = true;
			if(mileFieldsList[parentId+"-"+columnKey] == oNewData){
				mileFieldsList[parentId+"-"+columnKey] = undefined;	
			}
		}
	}
	if(fieldExists==false){
		if(oOldData==null){
			oOldData = '';
		}	
		if(columnKey == 'payFor')
			mileFieldsList[parentId+"-"+columnKey]= L_Select_AnOption;
		else
			mileFieldsList[parentId+"-"+columnKey]= oOldData;
	}
	
	for(okey1 in mileFieldsList){
		var index = okey1.indexOf(parentId); 
		if(index!=-1){count++;
		if(mileFieldsList[okey1]==undefined)
			undefCount++;
			}
		}
	if(count == undefCount && count!=0){
		for (oKey in milestoneRowChangeList) {
			var findRecord = oKey.indexOf(pMileTypeId+'mileSeq'+mileSeq);
			if (findRecord != -1){			
				milestoneRowChangeList[oKey] = undefined;
				milestoneRowChangeListRemainder[oKey] = undefined;
			}
		}
	}//added for restricting save dialog while no changes made into the date field.
	if(columnKey=="datefrom" || columnKey=="dateto"){
		if (oOldData == oNewData){
			/*for (oKey in milestoneRowChangeList) {
				var findRecord = oKey.indexOf(pMileTypeId+'mileSeq'+mileSeq);
				if (findRecord != -1){			
					milestoneRowChangeList[oKey] = undefined;
					milestoneRowChangeListRemainder[oKey] = undefined;
					
				}
			}*/
		}
	}
	
}

/*YK 29Mar2011 -DFIN20 */
VELOS.milestoneGrid.setStudyMileStat = function(studyID,studyName,mileStatus,statusDesc ) {
	var noChange = true;
		
			if(mileStatus.length>0 && mileStatus !="")
			{
				noChange=false;
			}
			
			 
			setStudyMileStatus="keyword=setStudyMileStatus&studyId="+studyID+"&setMileStat="+mileStatus;
			     var setStatus = document.createElement('div');
					if (!$('setStatus')) {
						setStatus.setAttribute('id', 'setStatus');
						setStatus.innerHTML  = '<div class="hd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; width=\"23em\"">'+CnfrmStdLvl_Change/*Confirm Study Level Milestone Status Changes*****/+'</div>';
						setStatus.innerHTML += '<div class="bd" id="insertFormOne" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; overflow: visible;"></div>';
						$D.insertAfter(setStatus, $('save_changes'));
					}
					$('insertFormOne').innerHTML = '<form id="dialogForm" method="POST" action="setStudyMileStatus.jsp?'+setStudyMileStatus+'"><div id="insertFormContentOne"></div></form>';
					if (noChange) {
						$('insertFormOne').style.width = '30em';
						$('insertFormOne').style.height = null;
						$('insertFormOne').style.overflow = 'visible';
					} else {
						$('insertFormOne').style.width = '40em';
						$('insertFormOne').style.height = null;
						$('insertFormOne').style.overflow = 'visible';
					}
				
					if (noChange) {
						$('insertFormContentOne').innerHTML += '<table width="220px"><tr><td style="font-size:8pt;">&nbsp;&nbsp;'+ThereNoChg_Save/*There are no changes to save.*****/+'&nbsp;&nbsp;</td></tr></table>';
						noChange = true;
					} else {
						var paramArray = [statusDesc,studyName];
						$('insertFormContentOne').innerHTML =getLocalizedMessageString("M_SetStdMstone_ForStd",paramArray)/*'Set Study Milestone Status to:"<b>'+statusDesc+'</b>" for Study :"<b>'+studyName+'</b>"'*****/; 
						$('insertFormContentOne').innerHTML += '<br/><table align=left><tr><td width="150px"><span id="eSignMessage" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"></span></td>'+
							'<td style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;">'+Esignature/*e-Signature*****/+' <FONT class="Mandatory">*</FONT>&nbsp</td>'+
							'<td><input type="password" name="eSign" id="eSign" maxlength="8" autocomplete="off" '+
							' onkeyup="ajaxvalidate(\'misc:\'+this.id,4,\'eSignMessage\',\''+Valid_Esign/*Valid e-Sign*****/+'\',\''+Invalid_Esign/*Invalid e-Sign*****/+'\',\'sessUserId\')" '+
							' onkeypress="return VELOS.milestoneGrid.fnOnceEnterKeyPress(event)" '+
							' />&nbsp;</td></tr></table>';
						
					}
					if($('insertFormContent')){
						$('insertFormContent').innerHTML =""; //YK: 26APR2011 Bug#6008 
						}
					
					var myDialog = new YAHOO.widget.Dialog('setStatus',
							{
								visible:false, fixedcenter:true, modal:true, resizeable:true,
								draggable:"true", autofillheight:"body", constraintoviewport:false
							});
					var handleCancel = function(e) {
						myDialog.cancel();
					};
					var handleSubmit = function(e) {
						try {
							if ('' == document.getElementById('eSignMessage').innerHTML) {
								alert(PlsEnterEsign);/*alert('Please enter e-Signature.');*****/
				        		return false;
							}
					        var thisTimeSubmitted = new Date();
					        if (lastTimeSubmitted && thisTimeSubmitted) {
					        	if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) { return false; }
						        lastTimeSubmitted = thisTimeSubmitted;
					        }
						} catch(e) {}
						myDialog.submit();
					};
					var onButtonsReady = function() {
					    if (!$E.getListeners($('dialog_submit'))) { $E.addListener($('dialog_submit'), 'click', handleSubmit, false, true); }
					    if (!$E.getListeners($('dialog_cancel'))) { $E.addListener($('dialog_cancel'), 'click', handleCancel); }
					}
					YAHOO.util.Event.onContentReady("setStatus", onButtonsReady);
					var myButtons = noChange ?
						[   { text: "Close", handler: handleCancel } ] :
						[
							{ text: "Save",   handler: handleSubmit },
							{ text: "Cancel", handler: handleCancel }
						];
					var onSuccess = function(o) {
						hideTransitPanel();
						var respJ = null; // response in JSON format
						try {
							respJ = $J.parse(o.responseText);
						} catch(e) {
							// Log error here
							return;
						}
						if (respJ.result == 0) {
							// alert("Your data was successfully submitted. The response was: " + respJ.resultMsg);
							showFlashPanel(respJ.resultMsg);
							setTimeout('window.location.reload();', 1500); 
						} else {
							alert("Error: " + respJ.resultMsg);
						}
					};
					var onFailure = function(o) {
						hideTransitPanel();
						var paramArray = [o.status];
						alert(getLocalizedMessageString("M_CommErr_CnctVelosSupp",paramArray));/*alert("Communication Error. Please contact Velos Support: " + o.status);*****/
					};
					var onStart = function(o) {
						showTransitPanel(PleaseWait_Dots);/*showTransitPanel('Please wait...');*****/
					}
					myDialog.callback.customevents = { onStart:onStart };
					myDialog.callback.success = onSuccess;
					myDialog.callback.failure = onFailure;
					myDialog.cfg.queueProperty("buttons", myButtons);
					myDialog.render(document.body);
					myDialog.show();
					if (document.getElementById('eSign')) { document.getElementById('eSign').focus(); }
				
					var showFlashPanel = function(msg) {
						// Initialize the temporary Panel to display while waiting for external content to load
						if (!(VELOS.flashPanel)) {
							VELOS.flashPanel =
								new YAHOO.widget.Panel("flashPanel",
									{ width:"240px",
									  fixedcenter:true,
									  close:false,
									  draggable:false,
									  zindex:4,
									  modal:true,
									  visible:false
									}
								);
						}
						VELOS.flashPanel.setHeader("");
						VELOS.flashPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"><b>'+msg+'</b></td></tr><table>');
						VELOS.flashPanel.render(document.body);
						VELOS.flashPanel.show();
						setTimeout('VELOS.flashPanel.hide();', 1500);
					}
				
					var showTransitPanel = function(msg) {
						if (!VELOS.transitPanel) {
							VELOS.transitPanel =
								new YAHOO.widget.Panel("transitPanel",
									{ width:"240px",
									  fixedcenter:true,
									  close:false,
									  draggable:false,
									  zindex:4,
									  modal:true,
									  visible:false
									}
								);
						}
						VELOS.transitPanel.setHeader("");
						VELOS.transitPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"><b>'+msg+'</b></td></tr><table>');
						VELOS.transitPanel.render(document.body);
						VELOS.transitPanel.show();
					}
				
					var hideTransitPanel = function() {
						if (VELOS.transitPanel) { VELOS.transitPanel.hide(); }
					}
			
	$j(function() {
		if ($j("input:submit")){
			$j( "input:submit").button();
		}
		if ($j("a:submit")){
			$j( "a:submit").button();
		}
		if ($j("button")){
			$j("button").button();
		}
	});	
}
/*YK 29Mar2011 -DFIN20 */

VELOS.milestoneGrid.prototype.startRequest = function() {
	//alert("bk alert 15 start of function startrequest");
	$C.asyncRequest(
		'POST',
		this.url,
		this.callback,
		this.urlParams
	);
	//alert("bk alert 15 end of function startrequest");
}

VELOS.milestoneGrid.prototype.render = function() {
	this.startRequest();
}

VELOS.milestoneGrid.prototype.loadingPanel = function() {
	// Initialize the temporary Panel to display while waiting for external content to load
	if (!(VELOS.wait)) {
		VELOS.wait =
			new YAHOO.widget.Panel("wait",
				{ width:"240px",
				  fixedcenter:true,
				  close:false,
				  draggable:false,
				  zindex:4,
				  modal:true,
				  visible:false
				}
			);

		VELOS.wait.setHeader(LoadingPlsWait);/*VELOS.wait.setHeader("Loading, please wait...");*****/
		VELOS.wait.setBody('<img class="asIsImage_progbar" src="../images/jpg/loading_pg.gif" />');
		VELOS.wait.render(document.body);
	}
}

VELOS.milestoneGrid.prototype.showPanel = function () {
	if (!(VELOS.wait)) { loadingPanel(); }
	VELOS.wait.show();
}

VELOS.milestoneGrid.prototype.hidePanel = function() {
	if (VELOS.wait) { VELOS.wait.hide(); }
}

/**
 * This function is used to format the fields in YUI Grid 
 * @param fieldName e.g amount,limit
 * @param mileIdCells
 * @author Hari
 * @since FIN-19817 27-Sep-2012
 */
VELOS.formatFields=function(fieldName,mileIdCells){
	for (var iX=0; iX<mileIdCells.length; iX++) {
		var el = new YAHOO.util.Element(mileIdCells[iX]);
		var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
		var nodes = parent.getElementsByClassName('yui-dt-col-'+fieldName, 'td')[0];
		$j(jQuery(nodes).children()).formatCurrency({
            region:appNumberLocale
                     });
	}

}
VELOS.formatHoldback=function(fieldName,mileIdCells){
for (var iX=0; iX<mileIdCells.length; iX++) {		
	var el = new YAHOO.util.Element(mileIdCells[iX]);
	var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
	var holdBackTd = parent.getElementsByClassName('yui-dt-col-'+fieldName, 'td')[0];
                if(holdBackTd!=undefined && holdBackTd!=''){
	var holdBackEl = new YAHOO.util.Element(holdBackTd);
	$j(holdBackEl.getElementsByClassName('yui-dt-liner', 'div')[0]).formatCurrency({
        region:appNumberLocale
    });
	var holdBackValue = holdBackEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
	holdBackValue = holdBackValue + '%'; 
	holdBackEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML=holdBackValue;
                 }
		}
}

/**
 * This function is used to apply Holdback to Active Accordian
 * @param mileVal,holdbackApplyToAll
 * @author Raviesh
 * @Enhancement FIN-22301
 */
VELOS.applyHoldBackToActiveAccordian=function(mileVal,holdbackApplyToAll){
	var holdbackForPowerGrid=0;
	var myParentColumnDefs='',myParentDataSource='';
	var myParentGrid = jQuery("#"+mileVal+"_milestonegrid tr[id*=yui-rec]");
	var holdbackValueOfPowerGrid=jQuery("#"+mileVal+"_Parent_milestonegrid td[class*=yui-dt-col-holdBack_parent]")[0].firstChild.innerHTML;
	var  myGrid = (mileVal=='PM')? selectedGridPM:
		(mileVal=='VM')? selectedGridVM:
		(mileVal=='EM')? selectedGridEM:
		(mileVal=='SM')? selectedGridSM : selectedGridAM;
		var  recordSet=myGrid.getRecordSet(); 
	for(var i=0;i<myParentGrid.length ;i++){
		var el = new YAHOO.util.Element(myParentGrid[i]);
		var oRecord = recordSet.getRecord(i);
   		var isSelectedRow =jQuery(myParentGrid[i]).children('[class*="selectedCheckBox "]').children('[class="yui-dt-liner"]');
		var statusNodes=oRecord.getData('mileStatus');
		var mileType=oRecord.getData('mileTypeId');
		var isReconciledAmt=oRecord.getData('isReconciledAmount');
		var isInvoiceGenerated=oRecord.getData('isInvoiceGenerated');
		var elm=oRecord;
		var isMileAchvCount=oRecord.getData('mileAchievedCount');
		var oChildMileId = oRecord.getData('mileId');
		var oChildmileSeq = oRecord.getData('mileSeq');
		var oChildRowNum=oRecord.getData('recNum')!=undefined?oRecord.getData('recNum'):i;
		var mileStatusId = oRecord.getData('mileStaticStatusId');
		var mileStatusSubType ="";
		
		for (var ind=0; ind < myMileStatusIds.length; ind++){
			if (myMileStatusIds[ind] == mileStatusId){
				mileStatusSubType = mileStatusSubTypes[ind];
				ind = myMileStatusIds.length;
			}
		}
	if(holdbackValueOfPowerGrid!=undefined && holdbackValueOfPowerGrid!='' && holdbackValueOfPowerGrid >0)
	{
		if(isSelectedRow!=undefined && isSelectedRow.hasClass('yui-dt-checkbox selSelected')){
			VELOS.setHoldBackValueToAll(oChildMileId,oChildmileSeq,oChildRowNum,myGrid,oRecord,mileStatusSubType,elm,holdbackValueOfPowerGrid,mileType,isReconciledAmt,isInvoiceGenerated,isMileAchvCount,true);
		}else{
			VELOS.setHoldBackValueToAll(oChildMileId,oChildmileSeq,oChildRowNum,myGrid,oRecord,mileStatusSubType,elm,holdbackApplyToAll,mileType,isReconciledAmt,isInvoiceGenerated,isMileAchvCount,true);
		}
	}else{
			VELOS.setHoldBackValueToAll(oChildMileId,oChildmileSeq,oChildRowNum,myGrid,oRecord,mileStatusSubType,elm,holdbackApplyToAll,mileType,isReconciledAmt,isInvoiceGenerated,isMileAchvCount,true);		
	}
	}
}

/**
 * This function is used to set Holdback value to All Milestones
 * @author Raviesh
 * @Enhancement FIN-22301
 */
VELOS.setHoldBackValueToAll=function(oChildMileId,oChildmileSeq,oChildRowNum,myGrid,oRecord,mileStatusSubType,elm,holdback,mileType,isReconciledAmt,isInvoiceGenerated,isMileAchvCount,isOpen){
	var updateCell=false;
	var sign =isOpen == true?'':'%';
	if(mileStatusSubType!="A" && mileStatusSubType!="IA"){
		updateCell=true;
	}else if(mileStatusSubType=="A" && ( isReconciledAmt=="0" || isInvoiceGenerated=="0")){
		updateCell=true;
	}
	if(mileStatusSubType=="A" && (isReconciledAmt!="0" || isInvoiceGenerated!="0")){
		updateCell=false;
	}
	
	if(updateCell){
		if(oChildMileId>0){
			VELOS.milestoneGrid.auditAction(mileType, oChildmileSeq, 'Updated', oChildRowNum);
		}/*else{
			VELOS.milestoneGrid.auditAction(mileType, oChildmileSeq, 'Added', oChildRowNum);
		}*/
		if(isOpen){
		myGrid.updateCell(oRecord, 'holdBack', YAHOO.util.Number.format(holdback, twoDecimalNumber), false);
		var otherRegularExp =  new RegExp('%$');
		jQuery("td.yui-dt-col-holdBack >div.yui-dt-liner ").each(function(){
			jQuery(this).formatCurrency({
	            region:appNumberLocale
            });
			if(this.innerHTML.match(otherRegularExp)==null )
			jQuery(this).text(this.innerHTML+'%');
		});
		}
		else
		myGrid.updateCell(oRecord, 'holdBack', YAHOO.util.Number.format(holdback, twoDecimalNumber), false);			
		oRecord.setData('holdBack',holdback);
	}
	
}

/**
 * This function is used to apply Holdback value to All Milestones
 * @author Raviesh
 * @Enhancement FIN-22301
 */
VELOS.applyHoldBack=function(myGrid,mileIdCells,holdbackToAll){
	var t=0;
	recordSet = myGrid.getRecordSet();
	
	for (var iX=0; iX<mileIdCells.length; iX++) {
		var el = new YAHOO.util.Element(mileIdCells[iX]);
		var oRecord = recordSet.getRecord(iX);
		var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
		var mileType=parent.getElementsByClassName('yui-dt-col-mileTypeId', 'td')[0].firstChild.innerHTML;
		var statusNodes=parent.getElementsByClassName('yui-dt-col-mileStatus', 'td')[0].firstChild.innerHTML;
		var isMileAchvCount= parent.getElementsByClassName('yui-dt-col-mileAchievedCount', 'td')[0].firstChild.innerHTML;
		var holBacknodes = parent.getElementsByClassName('yui-dt-col-holdBack', 'td')[0];
		var isInvoiceGenerated = parent.getElementsByClassName('yui-dt-col-isInvoiceGenerated', 'td')[0].firstChild.innerHTML;
		var isReconciledAmt = parent.getElementsByClassName('yui-dt-col-isReconciledAmount', 'td')[0].firstChild.innerHTML;
		var checkbox=parent.getElementsByClassName('selectedCheckBox', 'td')[0].firstChild;
		var oChildMileId = oRecord.getData('mileId');
		var oChildmileSeq = oRecord.getData('mileSeq');
		var oChildRowNum=oRecord.getData('recNum')!=undefined?oRecord.getData('recNum'):i;
		var mileStatusId = oRecord.getData('mileStaticStatusId');
		var mileStatusSubType ="";
		
		for (var ind=0; ind < myMileStatusIds.length; ind++){
			if (myMileStatusIds[ind] == mileStatusId){
				mileStatusSubType = mileStatusSubTypes[ind];
				ind = myMileStatusIds.length;
			}
		}
		VELOS.setHoldBackValueToAll(oChildMileId,oChildmileSeq,oChildRowNum,myGrid,oRecord,mileStatusSubType,holBacknodes.firstChild,holdbackToAll,mileType,isReconciledAmt,isInvoiceGenerated,isMileAchvCount,false);
		
	}
}

/* FIN-22373 11-Oct-2012 -Sudhir*/
// Extend textbox cell editor input box 
/*
* Extend properties of TextBoxEditor for customization.
* Here used to implement Datepicker inside YUI Datagrid.
*/
YAHOO.widget.TextboxCellEditor.prototype.move =  function() {
   //   this.textbox.style.width = this.getTdEl().offsetWidth + "px";
   var offsetWidth = this.getTdEl().offsetWidth;
   this.textbox.className = ((YAHOO.util.Dom.hasClass(this.getTdEl(), 'msGridDatefield'))) ? "msGridDatefield" :"";
   YAHOO.widget.TextboxCellEditor.superclass.move.call(this);
   openCal();
};



/**
 * This function is used to set Milestone Power Bar Grid values to selected Milestones
 * @author Raviesh
 * @Enhancement INF-22500
 */
VELOS.milestoneGrid.setToSelected = function(pMileTypeId){
	var myParentGrid = (pMileTypeId=='PM')? optedGridPM :
		(pMileTypeId=='VM')? optedGridVM :
		(pMileTypeId=='EM')? optedGridEM :
		(pMileTypeId=='SM')? optedGridSM : optedGridAM;
	
	var myChildGrid = (pMileTypeId=='PM')? selectedGridPM :
		(pMileTypeId=='VM')? selectedGridVM :
		(pMileTypeId=='EM')? selectedGridEM :
		(pMileTypeId=='SM')? selectedGridSM : selectedGridAM;
	
	var arrRecords = myChildGrid.getRecordSet();
	if (arrRecords.getLength() < 1) {
		alert(NoRec_ChldGrid);/* alert("No records found in Child Grid");**** */
		return;
	}
	else{
		var checkFound=false;
	    var selColEls = $D.getElementsByClassName('yui-dt-col-selectAll_'+pMileTypeId, 'td');
		for (var iX=0; iX<selColEls.length; iX++) {
			var el = new YAHOO.util.Element(selColEls[iX]);
			var oRecord = arrRecords.getRecord(iX);
			var input = el.getElementsByClassName('yui-dt-checkbox', 'input')[0];
			if (input==undefined || input.disabled) { continue; }
			if(input.checked){
				checkFound=true;
				break;
			}
		}
		if(checkFound){
		VELOS.milestoneGrid.setToAllColumnContent(myParentGrid,myChildGrid,pMileTypeId);
		}else{
			//alert(PleaseSelectMile);
			//Tarun - Alert Dialog added - Bug14850
			var alertDialog = null;
			if (!$('alertDialog')) {
					var alertDialog = document.createElement('div');
					alertDialog.setAttribute('id', 'alertDialog');
					alertDialog.innerHTML = '<div class="bd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; overflow: visible; height:3em;"><br />&nbsp;&nbsp;'+PleaseSelectMile+'</div></div>';
					$D.insertAfter(alertDialog, $('save_changes'));
				}
			var myAlertDialog = new YAHOO.widget.Dialog('alertDialog',
					{
						visible:true, fixedcenter:true, modal:true, resizeable:true,
						draggable:"true", autofillheight:"body", constraintoviewport:false
					});
			
			$('alertDialog').style.width = '20em';
			$('alertDialog').style.height = '3em';
			
			var handleCancel = function(e) {
				myAlertDialog.cancel();
			}
			var myButtons = [{ text: "Close", handler: handleCancel }] ;
			myAlertDialog.cfg.queueProperty("buttons", myButtons);
			myAlertDialog.render(document.body);
			myAlertDialog.show();
		}
	}	
	$j(function() {
		if ($j("input:submit")){
			$j( "input:submit").button();
		}
		if ($j("a:submit")){
			$j( "a:submit").button();
		}
		if ($j("button")){
			$j("button").button();
		}
	});	
}

/**
 * This function is used to set Milestone Power Bar Grid values to selected Milestones Columns
 * @author Raviesh
 * @Enhancement INF-22500
 */
VELOS.milestoneGrid.setToAllColumnContent = function(myParentGrid,myChildGrid,pMileTypeId){
	var amountMileIdCells;
	var mileType_Val = pMileTypeId
	var arrParRecords = myParentGrid.getRecordSet();
	oParentRecord = arrParRecords.getRecord(0);
	var oOldData = myChildGrid.oldData;
	var oNewData = myChildGrid.newData;
	var patCount=oParentRecord.getData("patCount");
    var patStatus=oParentRecord.getData("patStatus");
    var patStatusId=oParentRecord.getData("patStatusId");
	var amount=oParentRecord.getData("amount");
	var holdBack=oParentRecord.getData("holdBack_parent");
    var limit=oParentRecord.getData("limit");
    var payType=oParentRecord.getData("payType");
    var payFor=oParentRecord.getData("payFor");
    var mileStatus=oParentRecord.getData("mileStatus");
    var payTypeId=oParentRecord.getData("payTypeId");
    var payForId=oParentRecord.getData("payForId");
    var mileStatusId=oParentRecord.getData("mileStatusId");    
    var selColEls = $D.getElementsByClassName('yui-dt-col-selectAll_'+mileType_Val, 'td');
    var recordSet,dataJSON;
    
    recordSet = (mileType_Val=='PM')? selectedGridPM.getRecordSet() :
		(mileType_Val=='VM')? selectedGridVM.getRecordSet() :
		(mileType_Val=='EM')? selectedGridEM.getRecordSet() :
		(mileType_Val=='SM')? selectedGridSM.getRecordSet() : selectedGridAM.getRecordSet();
		//Parminder Singh : Bug#14939
		if(mileIdStatusPM.length<=0 && mileType_Val=='PM'){
		for (var iX=0; iX<selColEls.length; iX++) {
			var oRecord = recordSet.getRecord(iX);
			var mileStatusID=oRecord.getData("mileStatusId");
			var mileID=oRecord.getData("mileId");
			for (var ind=0; ind < myMileStatusIds.length; ind++){
				if (myMileStatusIds[ind] == mileStatusID && mileID>0){
					mileIdStatusPM.push(mileStatusSubTypes[ind]);
					ind = myMileStatusIds.length;
						}
					}
			}
		}else if(mileIdStatusVM.length<=0 && mileType_Val=='VM'){
			for (var iX=0; iX<selColEls.length; iX++) {
				var oRecord = recordSet.getRecord(iX);
				var mileID=oRecord.getData("mileId");
				var mileStatusID=oRecord.getData("mileStatusId");
				for (var ind=0; ind < myMileStatusIds.length; ind++){
					if (myMileStatusIds[ind] == mileStatusID && mileID>0){
						mileIdStatusVM.push(mileStatusSubTypes[ind]);
						ind = myMileStatusIds.length;
							}
						}
				}
		}else if(mileIdStatusEM.length<=0 && mileType_Val=='EM'){
			for (var iX=0; iX<selColEls.length; iX++) {
				var oRecord = recordSet.getRecord(iX);
				var mileStatusID=oRecord.getData("mileStatusId");
				var mileID=oRecord.getData("mileId");
				for (var ind=0; ind < myMileStatusIds.length; ind++){
					if (myMileStatusIds[ind] == mileStatusID && mileID>0){
						mileIdStatusEM.push(mileStatusSubTypes[ind]);
						ind = myMileStatusIds.length;
							}
						}
				}
		}else if(mileIdStatusSM.length<=0 && mileType_Val=='SM'){
			for (var iX=0; iX<selColEls.length; iX++) {
				var oRecord = recordSet.getRecord(iX);
				var mileStatusID=oRecord.getData("mileStatusId");
				var mileID=oRecord.getData("mileId");
				for (var ind=0; ind < myMileStatusIds.length; ind++){
					if (myMileStatusIds[ind] == mileStatusID && mileID>0){
						mileIdStatusSM.push(mileStatusSubTypes[ind]);
						ind = myMileStatusIds.length;
							}
						}
				}
		}else if(mileIdStatusAM.length<=0 && mileType_Val=='AM'){
			for (var iX=0; iX<selColEls.length; iX++) {
				var oRecord = recordSet.getRecord(iX);
				var mileStatusID=oRecord.getData("mileStatusId");
				var mileID=oRecord.getData("mileId");
				for (var ind=0; ind < myMileStatusIds.length; ind++){
					if (myMileStatusIds[ind] == mileStatusID && mileID>0){
						mileIdStatusAM.push(mileStatusSubTypes[ind]);
						ind = myMileStatusIds.length;
							}
						}
				}
		}
		else
		{}
		
		for (var iX=0; iX<selColEls.length; iX++) {
			var el = new YAHOO.util.Element(selColEls[iX]);
			var oRecord = recordSet.getRecord(iX);
			var oChildMileId = oRecord.getData('mileId');
			var oChildmileSeq = oRecord.getData('mileSeq');
			var oChildRowNum = 	oRecord.getData('recNum');
			var input = el.getElementsByClassName('yui-dt-checkbox', 'input')[0];
			if (input==undefined || input.disabled) { continue; }

			if(input.checked){
				/* var mileStatusSubType ="";
				   var mileStatusID=oRecord.getData("mileStatusId");
				for (var ind=0; ind < myMileStatusIds.length; ind++){
					if (myMileStatusIds[ind] == mileStatusID){
						mileStatusSubType = mileStatusSubTypes[ind];
						ind = myMileStatusIds.length;
						//alert(mileStatusSubType+" "+myMileStatusIds[ind]+" "+mileStatusID+" "+mileStatusSubType);
						}
					}
				alert("mileStatusSubType::"+mileIdStatusPM[iX]);*/
				var mileStatusSubType ="";
				if(mileType_Val=='PM')
				{
					mileStatusSubType = mileIdStatusPM[iX];
				}else if(mileType_Val=='VM'){
					mileStatusSubType = mileIdStatusVM[iX];
				}else if(mileType_Val=='EM'){
					mileStatusSubType = mileIdStatusEM[iX];
				}else if(mileType_Val=='SM'){
					mileStatusSubType = mileIdStatusSM[iX];
				}else{
					mileStatusSubType = mileIdStatusAM[iX];
				}
				
				if(checkForBlankAndNullValues(amount) && mileStatusSubType != 'A'){
				    myChildGrid.updateCell(oRecord, 'amount', amount, false);
					oRecord.setData('amount',amount);
					amountMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myChildGrid.getTableEl());	
					VELOS.formatFields('amount',amountMileIdCells);
				}
				if(checkForBlankAndNullValues(holdBack)){
					var hldBck=YAHOO.util.Number.format(holdBack, twoDecimalNumber);
					myChildGrid.updateCell(oRecord, 'holdBack', hldBck+"%", false);
					oRecord.setData('holdBack',hldBck);
				}
				if(checkForBlankAndNullValues(limit) && mileStatusSubType != 'A'){
					myChildGrid.updateCell(oRecord, 'limit', limit, false);
					oRecord.setData('limit',limit);
				}
				if(checkForBlankAndNullValues(payType) && mileStatusSubType != 'A'){
					myChildGrid.updateCell(oRecord, 'payType', payType, false);
					oRecord.setData('payType',payType);
					myChildGrid.updateCell(oRecord, 'payTypeId', payTypeId, false);
					oRecord.setData('payTypeId',payTypeId);
				}
				if(checkForBlankAndNullValues(payFor)){
					myChildGrid.updateCell(oRecord, 'payFor', payFor, false);
					oRecord.setData('payFor',payFor);
					myChildGrid.updateCell(oRecord, 'payForId', payForId, false);
					oRecord.setData('payForId',payForId);
				}
				if(checkForBlankAndNullValues(mileStatus)){
					myChildGrid.updateCell(oRecord, 'mileStatus', mileStatus, false);
					oRecord.setData('mileStatus',mileStatus);
					myChildGrid.updateCell(oRecord, 'mileStatusId', mileStatusId, false);
					oRecord.setData('mileStatusId',mileStatusId);
				}
				if(mileType_Val=='PM' || mileType_Val=='VM' || mileType_Val=='EM'){
				    if(mileType_Val=='PM'){
				    	if(checkForBlankAndNullValues(patCount) && mileStatusSubType != 'A'){
				    		myChildGrid.updateCell(oRecord, 'patCount', patCount, false);
				    		oRecord.setData('patCount',patCount);
				    	}
				    	if(checkForBlankAndNullValues(patStatus) && mileStatusSubType != 'A'){
							myChildGrid.updateCell(oRecord, 'patStatusId', patStatusId, false);
							oRecord.setData('patStatusId',patStatusId);
							myChildGrid.updateCell(oRecord, 'patStatus', patStatus, false);
							oRecord.setData('patStatus',patStatus);
				    	}
				    	if(oRecord.getData('mileId')>0){
				    	if(amount!='' || holdBack!=null || limit!='' || payType!='' || payFor!='' || mileStatus!='' || patCount!='' || patStatus!='')
							VELOS.milestoneGrid.auditAction(mileType_Val, oChildmileSeq, 'Updated', oChildRowNum);
				    	}
				    }else if(mileType_Val=='VM' || mileType_Val=='EM'){
				    	if(oRecord.getData('mileId')==0 || oRecord.getData('mileId')==undefined){
				    		if(checkForBlankAndNullValues(patCount) && mileStatusSubType != 'A'){
				    			myChildGrid.updateCell(oRecord, 'patCount', patCount, false);
				    			oRecord.setData('patCount',patCount);
				    		}
				    		if(checkForBlankAndNullValues(patStatus) && mileStatusSubType != 'A'){
								myChildGrid.updateCell(oRecord, 'patStatusId', patStatusId, false);
								oRecord.setData('patStatusId',patStatusId);
								myChildGrid.updateCell(oRecord, 'patStatus', patStatus, false);
								oRecord.setData('patStatus',patStatus);
				    		}
				    	}
				    }
				   
				}
				if(mileType_Val=='VM' || mileType_Val=='EM'){
					var calendarType=oParentRecord.getData("calendarType");
					var calendarId=oParentRecord.getData("calendarId");
					var calendar=oParentRecord.getData("calendar");
					var visit=oParentRecord.getData("visit");
					var visitId=oParentRecord.getData("visitId");
					var mileRule=oParentRecord.getData("mileRule");
					var mileRuleId=oParentRecord.getData("mileRuleId");
					var evtStatus=oParentRecord.getData("evtStatus");
					var evtStatusId=oParentRecord.getData("evtStatusId");
					if(oRecord.getData('mileId')==0 || oRecord.getData('mileId')==undefined){
						if(checkForBlankAndNullValues(calendar)){
							myChildGrid.updateCell(oRecord, 'calendarType', calendarType, false);
							oRecord.setData('calendarType',calendarType);
							myChildGrid.updateCell(oRecord, 'calendarId', calendarId, false);
							oRecord.setData('calendarId',calendarId);
							myChildGrid.updateCell(oRecord, 'calendar', calendar, false);
							oRecord.setData('calendar',calendar);
						}
						if(checkForBlankAndNullValues(visit)){
							myChildGrid.updateCell(oRecord, 'visit', visit, false);
							oRecord.setData('visit',visit);
							myChildGrid.updateCell(oRecord, 'visitId', visitId, false);
							oRecord.setData('visitId',visitId);
						}
					}
					if(checkForBlankAndNullValues(mileRule) && mileStatusSubType != 'A'){
						myChildGrid.updateCell(oRecord, 'mileRule', mileRule, false);
						oRecord.setData('mileRule',mileRule);
						myChildGrid.updateCell(oRecord, 'mileRuleId', mileRuleId, false);
						oRecord.setData('mileRuleId',mileRuleId);
						//Bug# 14846
						if(mileRule == "On Scheduled Date"){
							myChildGrid.updateCell(oRecord, 'evtStatus', '', false);
							oRecord.setData('evtStatus','');
							myChildGrid.updateCell(oRecord, 'evtStatusId', 0, false);
							oRecord.setData('evtStatusId',0);
						}
					}
					if(checkForBlankAndNullValues(evtStatus) && mileStatusSubType != 'A'){
						myChildGrid.updateCell(oRecord, 'evtStatus', evtStatus, false);
						oRecord.setData('evtStatus',evtStatus);
						myChildGrid.updateCell(oRecord, 'evtStatusId', evtStatusId, false);
						oRecord.setData('evtStatusId',evtStatusId);
					}
					if(oRecord.getData('mileId')>0 && mileType_Val=='VM'){
						if(calendar!='' || visit!='' || mileRule!='' || evtStatus!='' || amount!='' || holdBack!=null || limit!='' || payType!='' || payFor!='' || mileStatus!='' || patCount!='' || patStatus!='')
						VELOS.milestoneGrid.auditAction(mileType_Val, oChildmileSeq, 'Updated', oChildRowNum);
					}
				}
				if(mileType_Val=='EM'){
					var event=oParentRecord.getData("event");
					var eventId=oParentRecord.getData("eventId");
					if(oRecord.getData('mileId')==0 || oRecord.getData('mileId')==undefined){
						if(checkForBlankAndNullValues(event)){
							myChildGrid.updateCell(oRecord, 'event', event, false);
							oRecord.setData('event',event);
							myChildGrid.updateCell(oRecord, 'eventId', eventId, false);
							oRecord.setData('eventId',eventId);
						}
					}
					if(oRecord.getData('mileId')>0){
						if(calendar!='' || visit!='' || event!='' || mileRule!='' || evtStatus!='' || amount!='' || holdBack!=null || limit!='' || payType!='' || payFor!='' || mileStatus!='' || patCount!='' || patStatus!='')
						VELOS.milestoneGrid.auditAction(mileType_Val, oChildmileSeq, 'Updated', oChildRowNum);
					}
				}
				if(mileType_Val=='SM'){
					var stdStatus=oParentRecord.getData("stdStatus");
					var stdStatusId=oParentRecord.getData("stdStatusId");
					if(checkForBlankAndNullValues(stdStatus) && mileStatusSubType != 'A'){
						myChildGrid.updateCell(oRecord, 'stdStatus', stdStatus, false);
						oRecord.setData('stdStatus',stdStatus);
						myChildGrid.updateCell(oRecord, 'stdStatusId', stdStatusId, false);
						oRecord.setData('stdStatusId',stdStatusId);
					}
					if(oRecord.getData('mileId')>0){	
						if(amount!='' || holdBack!=null || limit!='' || payType!='' || payFor!='' || mileStatus!='' || stdStatus!='')
						VELOS.milestoneGrid.auditAction(mileType_Val, oChildmileSeq, 'Updated', oChildRowNum);
					}
				}
				if(mileType_Val=='AM'){
					var mileDesc=oParentRecord.getData("mileDesc");
					if(checkForBlankAndNullValues(mileDesc) && mileStatusSubType != 'A'){
						myChildGrid.updateCell(oRecord, 'mileDesc', mileDesc, false);
						oRecord.setData('mileDesc',mileDesc);
					}
					if(oRecord.getData('mileId')>0){
						if(amount!='' || holdBack!=null || payType!='' || payFor!='' || mileStatus!='' || mileDesc!='')
						VELOS.milestoneGrid.auditAction(mileType_Val, oChildmileSeq, 'Updated', oChildRowNum);
					}
				}		
			}
	  }
}

//Select All for INF-22500 (Milestone Merging) By Yogendra  Pratap
VELOS.milestoneGrid.selectAll = function(mileType_Val) {
	var selColEls = $D.getElementsByClassName('yui-dt-col-selectAll_'+mileType_Val, 'td');
	var recordSet;
	if (mileType_Val=='PM'){	recordSet = selectedGridPM.getRecordSet();	}
	if (mileType_Val=='VM'){	recordSet = selectedGridVM.getRecordSet();	}
	if (mileType_Val=='EM'){	recordSet = selectedGridEM.getRecordSet();	}
	if (mileType_Val=='SM'){	recordSet = selectedGridSM.getRecordSet();	}
	if (mileType_Val=='AM'){	recordSet = selectedGridAM.getRecordSet();	}

	for (var iX=0; iX<selColEls.length; iX++) {
		var el = new YAHOO.util.Element(selColEls[iX]);
		var oRecord = recordSet.getRecord(iX);
		var input = el.getElementsByClassName('yui-dt-checkbox', 'input')[0];
		if (input==undefined || input.disabled) { continue; }
		
		
		var mileSeq = oRecord.getData('mileSeq');
		var mileId = parseInt(oRecord.getData('mileId'));
		var mileTypeId = oRecord.getData('mileTypeId');
		var rowNum = oRecord.getData('recNum');
		
		if (mileId == 0) {
			if (!f_check_perm_noAlert(pgRight, 'N')) {
				//input.checked = (!input.checked);
				continue;
			}
		} else {
			if (!f_check_perm_noAlert(pgRight, 'E')) {
				//input.checked = (!input.checked);
				continue;
			}
		}			
		var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
		var selectTd = parent.getElementsByClassName('yui-dt-col-selectAll_'+mileType_Val, 'td')[0];
		var selectEl = new YAHOO.util.Element(selectTd);
		var selectId = selectEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
		var mileSeq = oRecord.getData('mileSeq');
		//var oKey = mileTypeId+'mileSeq'+mileSeq;
		if (input.checked != document.getElementById('selectAll_'+mileType_Val).checked) {
			selectAllFlag=true;
			input.checked = document.getElementById('selectAll_'+mileType_Val).checked;
		}
	}
}

//Bulk Delete for INF-22500 (Milestone Merging) By Yogendra  Pratap
VELOS.milestoneGrid.deleteAll = function(mileType_Val) {
	var delColEls = $D.getElementsByClassName('yui-dt-col-delete_'+mileType_Val, 'td');
	var recordSet;
	if (mileType_Val=='PM'){	recordSet = selectedGridPM.getRecordSet();	}
	if (mileType_Val=='VM'){	recordSet = selectedGridVM.getRecordSet();	}
	if (mileType_Val=='EM'){	recordSet = selectedGridEM.getRecordSet();	}
	if (mileType_Val=='SM'){	recordSet = selectedGridSM.getRecordSet();	}
	if (mileType_Val=='AM'){	recordSet = selectedGridAM.getRecordSet();	}
	
	for (var iX=0; iX<delColEls.length; iX++) {
		var el = new YAHOO.util.Element(delColEls[iX]);
		var oRecord = recordSet.getRecord(iX);
		var input = el.getElementsByClassName('yui-dt-checkbox', 'input')[0];
		if (input==undefined || input.disabled) { continue; }
		
		var mileSeq = oRecord.getData('mileSeq');
		var mileId = parseInt(oRecord.getData('mileId'));
		var mileTypeId = oRecord.getData('mileTypeId');
		var rowNum = oRecord.getData('recNum');
		if (purgedMileArray==null) purgedMileArray =[];
		if (mileId == 0) {
			if (!f_check_perm_noAlert(pgRight, 'N')) {
				//input.checked = (!input.checked);
				continue;
			}
		} else {
			if (!f_check_perm_noAlert(pgRight, 'E')) {
				//input.checked = (!input.checked);
				continue;
			}
		}			
		oRecord.setData('delete_'+mileType_Val, ""+document.getElementById('deleteSelected_'+mileType_Val).checked);
		if(document.getElementById('deleteSelected_'+mileType_Val).checked){
		var paramArray = [rowNum];
		purgedMileArray[mileTypeId+'mileSeq'+mileSeq]=getLocalizedMessageString("L_Del_Row",paramArray)/*'Deleted Row #'+rowNum+''*****/;
		if (oRecord.getData('mileId')!="0"){
			var data = oRecord.getData('mileId');				
			milePurgeList[mileTypeId+'mileSeq'+mileSeq] = data;	
		}
	    }else{
			purgedMileArray[mileTypeId+'mileSeq'+mileSeq]= undefined;				
			milePurgeList[mileTypeId+'mileSeq'+mileSeq] = undefined;
			
		}		
		var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
		var delTd = parent.getElementsByClassName('yui-dt-col-delete_'+mileType_Val, 'td')[0];
		var delEl = new YAHOO.util.Element(delTd);
		var deleteId = delEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
		var mileSeq = oRecord.getData('mileSeq');
		var oKey = mileTypeId+'mileSeq'+mileSeq;
		if (input.checked != document.getElementById('deleteSelected_'+mileType_Val).checked) {
			selectAllFlag=true;
			input.checked = document.getElementById('deleteSelected_'+mileType_Val).checked;
		}
	}
}
//INF-22500
function GetQueryStringParams(sParam){
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++){
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam){
            return sParameterName[1];
        }
    }
}
//Added for INF-22500 Bug#(14713 and 14717)
function checkForBlankAndNullValues(orgValue){
	if(orgValue==undefined){
		return false;
	}
	orgValue = $j.trim(orgValue);
	if(orgValue=='' || orgValue==null
			|| orgValue=='null' ){
		return false
	}
	return true;
}

function currdate(){
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();
if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} today = mm+'/'+dd+'/'+yyyy;
return today;
}