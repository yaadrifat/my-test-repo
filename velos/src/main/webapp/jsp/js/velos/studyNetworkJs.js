
var selNetWorkIds=[];
var showPannnel;
function fetchNetworkRows(networkId)
{
	var moreParam ='{"calledFrom":"networkLookup"'+',"networkId":"'+networkId+'"}';
	var rowCount = $j('#table2 tr').length;
	var pgRight= $j("#pageRight").val();
	if(networkId!='null'){
		selectedNtwId=networkId;
	}
	var urlCall="networksearch.jsp?&calledFrom=networkLookup&src=networkTabs&pageRight="+pgRight+"&expandNtw="+'&moreParam='+moreParam+"&rowCount="+rowCount+'&parentNtwId='+selectedNtwId+'&network_flag=';

	jQuery.ajax({
		url: urlCall,
		cache: false,
		type: "POST",
		async:false,
		success : function (data) 
		{
			$j("#table2").find("tr:gt(0)").remove();
			//$j("#row td").css("background-color","#2ECCFA");
			$j("#row").closest( "tr" ).after(data);
			//hideTableRowSortable();
			//defaultNetworkDisplay();
		}
	});
	
}
function defaultNetworkDisplay(){
    var limitLevel=1;
    var currentLevel=0;
	 var table = document.getElementById("table2");
	  $j("#table2 tr").each(function(i,row){
		   
	      var rowId=row.id;
	      var splitId='';
	      if(rowId !=''){
	    		splitId=rowId.split('_');
    		if(splitId[2]<=limitLevel){
    			currentLevel=splitId[2];
    		var _img=$j("#"+rowId).find('img:first');
    		var tdhtml=$j("#"+rowId).children('td:first').html();
    		if(_img.attr('src')=='./images/formright.gif' && currentLevel<limitLevel){
    			_img.attr('src','./images/formdown.gif');
    		}
    		$j("#"+rowId).show();
	    		}
	      }
	});
  }

function getPadding(padding){
	
	var padding1;
	var valueOfPadding=padding
	if(valueOfPadding!=undefined){
	valueOfPadding=valueOfPadding.trim();
	var indexPadding=valueOfPadding.indexOf("padding-left:");
	var firstIndOrgn=valueOfPadding.indexOf("%;",indexPadding);
	if(indexPadding>=0)
	{
    	padding1=valueOfPadding.substring(indexPadding+13,firstIndOrgn);
    	padding1=parseInt(padding1);
	}
  }
  return padding1;
}


 function showHide(event){
	var testing=$j(event).parent().attr("style");
	var rowCount = $j('#table2 tr').length;
	var index1=$j(event).closest('tr').index()+1;
	var parentPadding=getPadding(testing);
	var testpad;
	var childPadding;
	if($j(event).attr('src')=='./images/formdown.gif'){
		expandNtw.pop($j(event).closest('tr').attr('id'));
		$j(event).attr('src','./images/formright.gif');
				while(index1<rowCount){
					testpad=$j('#table2 tr:eq(' + (index1)+ ')').find("td:eq(1)").attr('style');
					childPadding=getPadding(testpad);
					if(childPadding>parentPadding){
						$j('#table2 tr:eq(' + (index1)+ ')').hide();
						}
					else{
							return;
						}
					index1++;
					}
		}
	else{
		expandNtw.push($j(event).closest('tr').attr('id'));
		$j(event).attr('src','./images/formdown.gif');
		var img='';
		var comparePadding;
		 while(index1<rowCount){
			testpad=$j('#table2 tr:eq(' + (index1)+ ')').find("td:eq(1)").attr('style');
			childPadding=getPadding(testpad);
			if(img=='./images/formright.gif'){
				while(comparePadding<childPadding){
					index1++;
					testpad=$j('#table2 tr:eq(' + (index1)+ ')').find("td:eq(1)").attr('style');
					childPadding=getPadding(testpad);
					}			
				}
			if(childPadding>parentPadding){
					$j('#table2 tr:eq(' + (index1)+ ')').show();	
				}
			else{
					return;
				}
			comparePadding=childPadding;
			img=$j('#table2 tr:eq(' + (index1)+ ')').find("td:eq(1)").find('img').attr('src');
			index1++;
			}
	}
}

function validateLookupSubmit(){
	var netIds =[];
	var _count =0;	
	var studyId=$j('#studyId').val();
	$j.each($j("input[name='selectedntw']:checked"), function(){
		_netArray=$j(this).val().split('_');
		if(_netArray[1]==0)
			netIds.push(_netArray[0])
			else
		     netIds.push(_netArray[2]);
		_count=_count+1;
		
	
	});
	if (_count==0){
		alert(M_Slct_Ntwrk); //Please select any network to submit
		return false;
	}
	var urlCall="";
	urlCall="saveNetwork.jsp?studyId="+studyId+"&studyNetworkId="+netIds+"&calledFrom=studynetworkTabs";
	jQuery.ajax({
		url: urlCall,
		cache: false,
		type: "POST",
		async:false,
		success : function (data) 
		{
			
			$j('#dialog-message').dialog({ modal: true, width:400,
				open: function(event, ui) {
			        setTimeout(function(){
			            $j('#dialog-message').dialog('close'); 
			            
			           opener.location.reload();
			           self.close ();
			        }, 2000);
			    }	
			});
			
		},error: function (xhr, status, errorThrown) {
            console.log('errorThrown  ' + errorThrown);
            $j('#submitFailedDialog').dialog({
				modal:true,
				closeText: '',
				close: function() {
					$j("#submitFailedDialog" ).dialog("destroy");
				}
			});
        }
	
	});	
	
}
