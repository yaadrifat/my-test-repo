/**
 * @ngdoc service
 * @name myApp:eRes_Study
 *
 * @description
 *
 *
 * */
angular.module('myApp')
    .service('eRes_Study',
        ['$http', '$q',
            function($http, $q){

    this.getStudyByName = function(studyName) {
        // <%--Angular REST request--%>
        // <%--Create post request--%>
        var req = {
            method: 'POST',
            url: 'https://bizdev3.veloseresearch.com:55555/study',
            headers: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            data:{
                StudyIdentifier: {
                    studyNumber:studyName
                }
            }
        };

        var deferred  = $q.defer();
        $http(req).success(function(response){ //success
            deferred.resolve(response);
        }).error(function(response){ //error
            deferred.reject(response.status);
        });
        return deferred.promise;
    };
}]);

