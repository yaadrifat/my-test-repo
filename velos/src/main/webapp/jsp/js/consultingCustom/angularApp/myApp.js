/*Main angular application:
* Will use separate files for each JSP page as the controller for
* that particular JSP page.
*
* All Controllers will be linked back to the same application named
* myApp
* */
angular.module('myApp',
    ['ngRoute','ui.bootstrap',
        //UI Grid API
        'ui.grid',
        'ui.grid.expandable',
        'ui.grid.cellNav',
        'ui.grid.edit',
        'ui.grid.saveState',
        'ui.grid.pinning',
        'ui.grid.moveColumns',
        'ui.grid.autoResize',
        'ui.grid.resizeColumns',
        'ui.grid.selection',
        'ui.grid.grouping',
        'ui.grid.exporter',
        'ui.grid.pagination',
        //Google Chart
        'googlechart',
        /*For some reason if importer is listed before
        * exporter the export options dont seem to show up on the menu....
        * dont ask me why...*/
        'ui.grid.rowEdit','ui.grid.importer',
        //UI-Select dropdown
        //'ui.select', 'ngSanitize'
    ])
//    .directive('uiSelectWrap', uiSelectWrap)
    //URL location functions
    .config(function($locationProvider) {
        // $locationProvider.html5Mode(true).hashPrefix('?');
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    })
     /*Routing Configuration, not is use yet...but will have to be
    explored for future use*/
    .config(['$routeProvider',
        function($routeProvider) {
            $routeProvider.
            when('/', {
                templateUrl: 'angularHome.jsp',
                controller: 'MainCtrl'
            }).
            when('#/Import/Demographics', {
                templateUrl: 'importDemographics.jsp',
                controller: 'ImportDemographicsCtrl'
            }).
            when('#/Financials/:StudyOID/:CalendarOID',{
                templateUrl: 'StudyFinancials.jsp',
                controller: 'StudyFinancialsCtrl'
            }).
            otherwise({
                redirectTo: '/'
            });
        }]);

/*

/!**
 * http://brianhann.com/ui-grid-and-dropdowns/
 * Directive for UI-Select to disappeare when user
 * clicks elsewhere on the page.
 *
 * Check the reference url for more details.
 * *!/
uiSelectWrap.$inject = ['$document', 'uiGridEditConstants'];
function uiSelectWrap($document, uiGridEditConstants) {
    return function link($scope, $elm, $attr) {
        $document.on('click', docClick);

        function docClick(evt) {
            if ($(evt.target).closest('.ui-select-container').size() === 0) {
                $scope.$emit(uiGridEditConstants.events.END_CELL_EDIT);
                $document.off('click', docClick);
            }
        }
    };
}
*/
