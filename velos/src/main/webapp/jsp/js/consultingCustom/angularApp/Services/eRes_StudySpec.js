describe('Service: myApp.eRes_Study', function () {

    // load the service's module
    beforeEach(module('myApp'));

    // instantiate service
    var service;

    //update the injection
    beforeEach(inject(function (_eRes_Study_) {
        service = _eRes_Study_;
    }));

    /**
     * @description
     * Sample test case to check if the service is injected properly
     * */
    it('should be injected and defined', function () {
        expect(service).toBeDefined();
    });
});
