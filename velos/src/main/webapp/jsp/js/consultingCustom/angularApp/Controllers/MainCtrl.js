/**
 * @ngdoc controller
 * @name myApp:MainCtrl
 *
 * @description
 *
 *
 * @requires $scope
 * */
angular.module('myApp')
    .controller('MainCtrl', ['$scope', '$http',
        '$window', '$templateCache','eRes_Study',
        function ($scope, $http, $window, eRes_Study, $templateCache) {

            $scope.studyName = "DemoStudy";
            $scope.gridOptions = {
                enableSorting: true,
                enableCellEditOnFocus: true,
                enableGridMenu: true,
                columnDefs: [
                    {field: 'currentForStudy', name: 'Current?'},
                    {
                        name: 'Documented By',
                        cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.documentedBy.lastName}},{{row.entity.documentedBy.firstName}}</div>'
                    },
                    {field: 'organizationId.siteName', name: 'organization'},
                    {
                        name: 'Status', cellTemplate: '' +
                    '<div class="ui-grid-cell-contents">' +
                    '{{row.entity.status.description}}' +
                    '({{row.entity.statusType.description}})</div>'
                    },
                    {field: 'validFromDate', name: 'Valid From'}

                ]
            };

            //Setting up function to navigate to new page on button click
            $scope.go = function (path) {
                $window.location.href = path;
                //$location.path( path );
            };


            //Method attached to button click event (ng-click)
            $scope.getStudy = function () {
                // <%--Angular REST request--%>
                // <%--Create post request--%>
                //need to replace static code with angular service
                var req = {
                 method: 'POST',
                 url: 'https://bizdev3.veloseresearch.com:55555/study/getStudy',
                 headers: {
                 'Content-Type': 'application/json;charset=UTF-8'
                 },
                 data:{
                 StudyIdentifier: {
                 studyNumber:$scope.studyName
                 }
                 }
                 };

                $http(req).then(function (response) { //success
                    $scope.gridOptions.data = response.data.Study.studyStatuses.studyStatus;
                    $scope.status = response.status;
                    $scope.studyDetails = response.data;
                    // Get the Calendar List
                    $scope.getStudyCalendarList();
                }, function (response) { //error
                    $scope.status = response.status;
                });
            };

            $scope.getStudyCalendarList = function () {
                // <%--Angular REST request--%>
                // <%--Create post request--%>
                var req = {
                    method: 'POST',
                    url: 'https://bizdev3.veloseresearch.com:55555/study/StudyCalendarList',
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8'
                    },
                    data: {
                        StudyIdentifier: {
                            studyNumber: $scope.studyDetails.Study.studyNumber,
                            OID: $scope.studyDetails.Study.studyIdentifier.OID
                        }
                    }
                }

                $http(req).then(function (response) { //success
                    $scope.calendarList = response.data.StudyCalendarList.studyCalendars;
                }, function (response) { //error
                    $scope.status = "CalendarList:" + response.status;
                });
            };
        }]);