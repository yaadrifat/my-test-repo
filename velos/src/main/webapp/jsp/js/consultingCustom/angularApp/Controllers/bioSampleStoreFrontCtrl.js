/**
 * @ngdoc controller
 * @name Controllers:bioSampleStoreFrontCtrl
 *
 * @description
 *
 *
 * @requires $scope
 * */
angular.module('myApp')
    .controller('bioSampleStoreFrontCtrl', ['$scope', '$http',
        '$location',
        '$interval', '$q', '$templateCache',
        'uiGridGroupingConstants',
        'uiGridConstants',
        function ($scope, $http, $location, $interval, $q, $templateCache, uiGridGroupingConstants, uiGridConstants) {


            //Reset the sample request count on page load.
            $scope.BioSampleRequestCount = 0;

            /**
             * Use the following object for retreiving a set number of rows from
             * PT
             * @type {{startRow: number, numberOfRows: number}}
             */
            var paginationOptions = {
                startRow: 1,
                numberOfRows: 50
            };

            /**
             * Use for filter options from PT web service
             */
            var ptFilterOptions = {
                organization: '',
                pathStatus: '',
                anatomicalSite: '',
                sampleType: '',
                sampleStatus: ''
            };

            /**
             * Create angular grid options and settings
             *
             * @type {Array}
             */
            $scope.data = [];
            $scope.gridOptions = {
                enableGridMenu: true,
                enableFiltering: true,
                enableSorting: true,
                enableCellEditOnFocus: true,
                exporterMenuPdf: false,
                paginationPageSizes: [100, 500, 1000],
                paginationPageSize: 25,
                useExternalPagination: true,
                //useExternalFiltering: true,
                importerDataAddCallback: function (grid, newObjects) {
                    $scope.data = $scope.data.concat(newObjects);
                },
                onRegisterApi: function (gridApi) {
                    $scope.bioSamplegridApi = gridApi;

                    gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);

                    //Load the data the folloing method will populate $scope.gridOptions.data
                    getBioSamples();

                    //Load pagination strategy
                    gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                        paginationOptions.startRow = newPage;
                        paginationOptions.numberOfRows = pageSize;
                        getBioSamples();
                    });

                    //Filter options
                    gridApi.core.on.filterChanged($scope, function () {
                        var grid = this.grid;

                        //should pt need to be called?
                        var callPtService = false;

                        //Spec_Organization - filter on organization
                        if (ptFilterOptions.organization != $scope.bioSamplegridApi.grid.columns[2].filters[0].term) {
                            ptFilterOptions.organization = $scope.bioSamplegridApi.grid.columns[2].filters[0].term;
                            callPtService = true;
                        }


                        //SpecimenType
                        if (ptFilterOptions.sampleType != $scope.bioSamplegridApi.grid.columns[7].filters[0].term) {
                            ptFilterOptions.sampleType = $scope.bioSamplegridApi.grid.columns[7].filters[0].term;
                            callPtService = true;
                        }

                        //Anatomical Site
                        if (ptFilterOptions.anatomicalSite != $scope.bioSamplegridApi.grid.columns[9].filters[0].term) {
                            ptFilterOptions.anatomicalSite = $scope.bioSamplegridApi.grid.columns[9].filters[0].term;
                            callPtService = true;
                        }


                        //Pathological Status
                        if (ptFilterOptions.pathStatus != $scope.bioSamplegridApi.grid.columns[10].filters[0].term) {
                            ptFilterOptions.pathStatus = $scope.bioSamplegridApi.grid.columns[10].filters[0].term;
                            callPtService = true;
                        }

                        //If any of the filters changed then call PT
                        if (callPtService) {
                            getBioSamples();
                        }
                    });

                },
                columnDefs: [
                    {
                        field: 'SPEC_ORGANIZATION', name: 'Organization',
                        width: 130,
                        minWidth: 100,
                        resizable: true,
                        visible: true,
                        //grouping: {groupPriority: 0},
                        filter: {
                            type: uiGridConstants.filter.SELECT,
                            selectOptions: [
                                {value: 'Akron Childrens Hospital', label: 'Akron Childrens Hospital'},
                                {value: 'Baylor College of Medicine', label: 'Baylor College of Medicine'},
                                {
                                    value: 'Bleeding & Clotting Disorders Institute',
                                    label: 'Bleeding & Clotting Disorders Institute'
                                },
                                {value: 'BloodCenter of Wisconsin', label: 'BloodCenter of Wisconsin'},
                                {value: 'Center 1 - UCSD', label: 'Center 1 - UCSD'},
                                {
                                    value: 'Childrens Hospital & Research Center Oakland',
                                    label: 'Childrens Hospital & Research Center Oakland'
                                },
                                {value: 'Childrens Hospital of Chicago', label: 'Childrens Hospital of Chicago'},
                                {
                                    value: 'Childrens Hospital of the Kings Daughters',
                                    label: 'Childrens Hospital of the Kings Daughters'
                                },
                                {value: 'Cincinnati Childrens Hospital', label: 'Cincinnati Childrens Hospital'},
                                {value: 'Cornell University', label: 'Cornell University'},
                                {
                                    value: 'Drexel University College of Medicine',
                                    label: 'Drexel University College of Medicine'
                                },
                                {
                                    value: 'Emory University School of Medicine',
                                    label: 'Emory University School of Medicine'
                                },
                                {
                                    value: 'Indiana Hemophilia & Thrombosis Center, Inc',
                                    label: 'Indiana Hemophilia & Thrombosis Center, Inc'
                                },
                                {value: 'Jewish Hospital', label: 'Jewish Hospital'},
                                {value: 'Johns Hopkins Childrens Center', label: 'Johns Hopkins Childrens Center'},
                                {
                                    value: 'Loma Linda University Medical Center',
                                    label: 'Loma Linda University Medical Center'
                                },
                                {value: 'Michigan State University', label: 'Michigan State University'},
                                {
                                    value: 'Mountain States Regional Hem. and Thrombosis Ctr.',
                                    label: 'Mountain States Regional Hem. and Thrombosis Ctr.'
                                },
                                {value: 'NVCI', label: 'NVCI'},
                                {value: 'Nationwide Childrens Hospital', label: 'Nationwide Childrens Hospital'},
                                {
                                    value: 'Newark Beth Israel Medical Center',
                                    label: 'Newark Beth Israel Medical Center'
                                },
                                {value: 'Northwestern University', label: 'Northwestern University'},
                                {value: 'Norton Hospital', label: 'Norton Hospital'},
                                {value: 'ORGNAME', label: 'ORGNAME'},
                                {
                                    value: 'Oregon Health & Science University',
                                    label: 'Oregon Health & Science University'
                                },
                                {value: 'Queens University', label: 'Queens University'},
                                {value: 'Rochester General Hospital', label: 'Rochester General Hospital'},
                                {value: 'Rush Univeristy Medical Center', label: 'Rush Univeristy Medical Center'},
                                {value: 'Toledo Childrens Hospital', label: 'Toledo Childrens Hospital'},
                                {
                                    value: 'Tulane University Health Sciences Center',
                                    label: 'Tulane University Health Sciences Center'
                                },
                                {value: 'UCSD', label: 'UCSD'},
                                {
                                    value: 'Univ. of Texas Health Science Center at Houston',
                                    label: 'Univ. of Texas Health Science Center at Houston'
                                },
                                {
                                    value: 'Univeristy of North Carolina at Chapel Hill',
                                    label: 'Univeristy of North Carolina at Chapel Hill'
                                },
                                {value: 'University Hospital', label: 'University Hospital'},
                                {value: 'University of Iowa', label: 'University of Iowa'},
                                {
                                    value: 'University of Louisville - Brown Cancer Center',
                                    label: 'University of Louisville - Brown Cancer Center'
                                },
                                {value: 'University of Pennsylvania', label: 'University of Pennsylvania'},
                                {value: 'University of Pittsburgh', label: 'University of Pittsburgh'},
                                {
                                    value: 'University of Texas Southwestern Medical Center',
                                    label: 'University of Texas Southwestern Medical Center'
                                },
                                {value: 'University of Wisconsin Madison', label: 'University of Wisconsin Madison'},
                                {value: 'Vanderbilt University', label: 'Vanderbilt University'},
                                {value: 'Wayne State University', label: 'Wayne State University'},
                                {value: 'White Plains Hospital', label: 'White Plains Hospital'}
                            ]
                        }
                    },
                    {
                        field: 'CONVERGE_SITE_ID', name: 'Converge SiteId', visible: false,
                        minWidth: 100,
                        resizable: true
                    },
                    {
                        field: 'RECID', name: 'RecId', visible: false,
                        minWidth: 100,
                        resizable: true
                    },
                    {
                        field: 'SPEC_ID', name: 'Sample Id', width: 200, minWidth: 100,
                        resizable: true,
                        visible: false
                    },
                    {
                        field: 'PARENT_SPEC_ID', name: 'Parent Sample Id',
                        cellTooltip: function (row, col) {
                            return 'Name: ' + row.entity.PARENT_SPEC_ID;
                        },
                        minWidth: 100,
                        resizable: true,
                        visible: false
                    },
                    {
                        field: 'SPEC_TYPE', name: 'Type', minWidth: 170,
                        resizable: true,
                        enableCellEditOnFocus: false,
                        //filterHeaderTemplate:'htmlTemplates/ui-selectDropDown',
                        /*filterHeaderTemplate: '<ui-select-wrap>' +
                         '<ui-select ng-model="colFilter.term" class="ui-select-container"' +
                         'append-to-body="true">' +
                         '<ui-select-match placeholder="Choose...">{{ item }}</ui-select-match>' +
                         '<ui-select-choices repeat="item in col.filters.Options">' +
                         '<span>{{ item.value }}</span>' +
                         '</ui-select-choices>' +
                         '</ui-select>' +
                         '</ui-select-wrap>',*/
                        filter: {
                            type: uiGridConstants.filter.SELECT,
                            selectOptions: [
                                //Options: [
                                {value: 'ACD Tube', label: 'ACD Tube'},
                                {value: 'BAL', label: 'BAL'},
                                {value: 'Bacterial Isolates', label: 'Bacterial Isolates'},
                                {value: 'Biospecimen', label: 'Biospecimen'},
                                {value: 'Blood', label: 'Blood'},
                                {value: 'Blood Spot', label: 'Blood Spot'},
                                {value: 'Body Cavity Fluid', label: 'Body Cavity Fluid'},
                                {value: 'Bone Marrow Aspirate', label: 'Bone Marrow Aspirate'},
                                {value: 'Breast Milk', label: 'Breast Milk'},
                                {value: 'Breath Condensate', label: 'Breath Condensate'},
                                {value: 'Buccual Swab', label: 'Buccual Swab'},
                                {value: 'Buffy Coat', label: 'Buffy Coat'},
                                {value: 'CPT Tube', label: 'CPT Tube'},
                                {value: 'CSF', label: 'CSF'},
                                {value: 'Cell', label: 'Cell'},
                                {value: 'Coagulated Blood', label: 'Coagulated Blood'},
                                {value: 'DNA', label: 'DNA'},
                                {value: 'Fixed Tissue', label: 'Fixed Tissue'},
                                {value: 'Fixed Tissue Block', label: 'Fixed Tissue Block'},
                                {value: 'Fixed Tissue Slide', label: 'Fixed Tissue Slide'},
                                {value: 'Fresh Tissue', label: 'Fresh Tissue'},
                                {value: 'Frozen Cell Pellet', label: 'Frozen Cell Pellet'},
                                {value: 'Frozen Tissue', label: 'Frozen Tissue'},
                                {value: 'Lavage', label: 'Lavage'},
                                {value: 'Nasal Swab', label: 'Nasal Swab'},
                                {value: 'Non-Viable Cell Aliquot', label: 'Non-Viable Cell Aliquot'},
                                {value: 'PBMC', label: 'PBMC'},
                                {value: 'PCR Product', label: 'PCR Product'},
                                {value: 'Plasma', label: 'Plasma'},
                                {value: 'Plasma Aliquot', label: 'Plasma Aliquot'},
                                {value: 'Protein', label: 'Protein'},
                                {value: 'RNA', label: 'RNA'},
                                {value: 'Saliva', label: 'Saliva'},
                                {value: 'Segment', label: 'Segment'},
                                {value: 'Serum', label: 'Serum'},
                                {value: 'Serum Aliquot', label: 'Serum Aliquot'},
                                {value: 'Stool', label: 'Stool'},
                                {value: 'Synovial Fluid', label: 'Synovial Fluid'},
                                {value: 'Tissue', label: 'Tissue'},
                                {value: 'Urine', label: 'Urine'},
                                {value: 'Viable Cell Aliquot', label: 'Viable Cell Aliquot'},
                                {value: 'Whole Blood', label: 'Whole Blood'},
                                {value: 'Xenograft', label: 'Xenograft'},
                                {value: 'cDNA', label: 'cDNA'}
                            ]
                            //disableCancelFilterButton: true
                        },
                        cellTooltip: function (row, col) {
                            return 'Organization:' +
                                row.entity.SPEC_ORGANIZATION +
                                '\nOwner Name:' +
                                row.entity.OWNER +
                                '\nSample Id: ' +
                                row.entity.SPEC_ID +
                                '\nParent Sample Id:' +
                                row.entity.PARENT_SPEC_ID
                        },
                        cellTemplate: '<div class="ui-grid-cell-contents">' +
                        '<span> {{COL_FIELD}} ' +
                        '<span class="pull-right">' +
                        '<button type="button" class="glyphicon glyphicon-filter" ' +
                        'aria-hidden="true"' +
                        'ng-click="grid.appScope.setSampleTypeFilter(row.entity)"></button></a>' +
                        '</span></span>' +
                        '</div>'
                    },
                    {
                        field: 'SPEC_DESCRIPTION', name: 'Description',
                        minWidth: 100,
                        resizable: true,
                        visible: false

                    },
                    {
                        field: 'SPEC_ANATOMIC_SITE',
                        name: 'Anatomic Site',
                        width: 150,
                        minWidth: 100,
                        resizable: true,
                        cellTooltip: function (row, col) {
                            return 'Name: ' + row.entity.SPEC_ORGANIZATION;
                        },
                        filter: {
                            type: uiGridConstants.filter.SELECT,
                            selectOptions: [
                                {value: 'Abdominal', label: 'Abdominal'},
                                {value: 'Adrenal Gland, Left', label: 'Adrenal Gland, Left'},
                                {value: 'Adrenal Gland, Right', label: 'Adrenal Gland, Right'},
                                {value: 'Adrenal gland', label: 'Adrenal gland'},
                                {value: 'Anus', label: 'Anus'},
                                {value: 'Appendix', label: 'Appendix'},
                                {value: 'Base of tongue, NOS', label: 'Base of tongue, NOS'},
                                {value: 'Bladder', label: 'Bladder'},
                                {value: 'Body of pancreas', label: 'Body of pancreas'},
                                {value: 'Bones', label: 'Bones'},
                                {value: 'Bowel peritoneum/serosa', label: 'Bowel peritoneum/serosa'},
                                {value: 'Brain', label: 'Brain'},
                                {value: 'Breast', label: 'Breast'},
                                {value: 'Breast, Left', label: 'Breast, Left'},
                                {value: 'Breast, NOS', label: 'Breast, NOS'},
                                {value: 'Breast, Right', label: 'Breast, Right'},
                                {value: 'Cervix', label: 'Cervix'},
                                {value: 'Colon', label: 'Colon'},
                                {
                                    value: 'Connective, Subcutaneous and other soft tissues of head, face, and neck',
                                    label: 'Connective, Subcutaneous and other soft tissues of head, face, and neck'
                                },
                                {
                                    value: 'Connective, Subcutaneous and other soft tissues of lower limb and hip',
                                    label: 'Connective, Subcutaneous and other soft tissues of lower limb and hip'
                                },
                                {value: 'Cul-de-sac', label: 'Cul-de-sac'},
                                {value: 'Diaphragm', label: 'Diaphragm'},
                                {value: 'Duodenum', label: 'Duodenum'},
                                {value: 'Elbow, Left', label: 'Elbow, Left'},
                                {value: 'Endometrium', label: 'Endometrium'},
                                {value: 'Esophagus', label: 'Esophagus'},
                                {value: 'Eye, Left (OS)', label: 'Eye, Left (OS)'},
                                {value: 'Fallopian tube', label: 'Fallopian tube'},
                                {value: 'Gallbladder', label: 'Gallbladder'},
                                {value: 'Head of pancreas', label: 'Head of pancreas'},
                                {value: 'Head, face or neck, NOS', label: 'Head, face or neck, NOS'},
                                {value: 'Hip, Left', label: 'Hip, Left'},
                                {value: 'Ileum', label: 'Ileum'},
                                {value: 'Intestine, Small', label: 'Intestine, Small'},
                                {value: 'Kidney', label: 'Kidney'},
                                {value: 'Kidney, Left', label: 'Kidney, Left'},
                                {value: 'Kidney, NOS', label: 'Kidney, NOS'},
                                {value: 'Kidney, Right', label: 'Kidney, Right'},
                                {value: 'Knee, Left', label: 'Knee, Left'},
                                {value: 'Knee, Right', label: 'Knee, Right'},
                                {value: 'Larynx', label: 'Larynx'},
                                {value: 'Lingual tonsil', label: 'Lingual tonsil'},
                                {value: 'Lip, Lower', label: 'Lip, Lower'},
                                {value: 'Liver', label: 'Liver'},
                                {value: 'Lower lobe, lung', label: 'Lower lobe, lung'},
                                {value: 'Lower-outer quadrant of breast', label: 'Lower-outer quadrant of breast'},
                                {value: 'Lung', label: 'Lung'},
                                {value: 'Lung, Left Lower lobe ', label: 'Lung, Left Lower lobe '},
                                {value: 'Lung, Left Upper lobe ', label: 'Lung, Left Upper lobe '},
                                {value: 'Lung, NOS', label: 'Lung, NOS'},
                                {value: 'Lung, Right Lower Lobe', label: 'Lung, Right Lower Lobe'},
                                {value: 'Lung, Right Middle Lobe', label: 'Lung, Right Middle Lobe'},
                                {value: 'Lung, Right Upper Lobe', label: 'Lung, Right Upper Lobe'},
                                {value: 'Lymph node', label: 'Lymph node'},
                                {
                                    value: 'Lymph nodes of head, face and neck',
                                    label: 'Lymph nodes of head, face and neck'
                                },
                                {
                                    value: 'Lymph nodes of inguinal region or leg',
                                    label: 'Lymph nodes of inguinal region or leg'
                                },
                                {value: 'Mesentery', label: 'Mesentery'},
                                {value: 'NERVE', label: 'NERVE'},
                                {value: 'Neck', label: 'Neck'},
                                {value: 'Nose', label: 'Nose'},
                                {value: 'Not specified', label: 'Not specified'},
                                {value: 'Omentum', label: 'Omentum'},
                                {value: 'Oral', label: 'Oral'},
                                {value: 'Other', label: 'Other'},
                                {value: 'Other, specify:', label: 'Other, specify:'},
                                {value: 'Ovary', label: 'Ovary'},
                                {value: 'Palate, NOS', label: 'Palate, NOS'},
                                {value: 'Pancreas', label: 'Pancreas'},
                                {value: 'Pancreas, NOS', label: 'Pancreas, NOS'},
                                {value: 'Paracolic gutter', label: 'Paracolic gutter'},
                                {value: 'Parotid Gland', label: 'Parotid Gland'},
                                {value: 'Parotid gland', label: 'Parotid gland'},
                                {value: 'Pelvic wall', label: 'Pelvic wall'},
                                {value: 'Pelvis, NOS', label: 'Pelvis, NOS'},
                                {value: 'Penis', label: 'Penis'},
                                {value: 'Peritoneum', label: 'Peritoneum'},
                                {value: 'Peritoneum/uterine serosa', label: 'Peritoneum/uterine serosa'},
                                {value: 'Prostate', label: 'Prostate'},
                                {value: 'Rectosigmoid', label: 'Rectosigmoid'},
                                {value: 'Rectum', label: 'Rectum'},
                                {value: 'Renal pelvis', label: 'Renal pelvis'},
                                {value: 'Retroperitoneal', label: 'Retroperitoneal'},
                                {value: 'Salivary Gland', label: 'Salivary Gland'},
                                {value: 'Skin', label: 'Skin'},
                                {value: 'Skin of lower limb and hip', label: 'Skin of lower limb and hip'},
                                {
                                    value: 'Skin of other and unspecified parts of face',
                                    label: 'Skin of other and unspecified parts of face'
                                },
                                {value: 'Skin of scalp and neck', label: 'Skin of scalp and neck'},
                                {value: 'Skin of trunk', label: 'Skin of trunk'},
                                {value: 'Skin, NOS', label: 'Skin, NOS'},
                                {value: 'Small Intestine', label: 'Small Intestine'},
                                {value: 'Spleen', label: 'Spleen'},
                                {value: 'Stomach', label: 'Stomach'},
                                {value: 'Stomach, NOS', label: 'Stomach, NOS'},
                                {value: 'Submandibular gland', label: 'Submandibular gland'},
                                {value: 'Testis', label: 'Testis'},
                                {
                                    value: '========= Lip, oral cavity and pharynx ==========',
                                    label: '========= Lip, oral cavity and pharynx =========='
                                }
                            ]
                        }
                    },
                    {
                        field: 'SPEC_PATHOLOGY_STAT', name: 'Pathological Status',
                        minWidth: 100,
                        resizable: true,
                        filter: {
                            type: uiGridConstants.filter.SELECT,
                            selectOptions: [
                                {value: 'Adrenal Cancer History', label: 'Adrenal Cancer History'},
                                {
                                    value: 'Adrenal gland - Non-Neoplastic/benign',
                                    label: 'Adrenal gland - Non-Neoplastic/benign'
                                },
                                {value: 'Anal Cancer History', label: 'Anal Cancer History'},
                                {value: 'Anus - Non-Neoplastic/benign', label: 'Anus - Non-Neoplastic/benign'},
                                {
                                    value: 'Appendix  - Non-Neoplastic/benign',
                                    label: 'Appendix  - Non-Neoplastic/benign'
                                },
                                {value: 'Appendix Cancer History', label: 'Appendix Cancer History'},
                                {value: 'Basal Cell Cancer History - Skin', label: 'Basal Cell Cancer History - Skin'},
                                {
                                    value: 'Bile Duct - Non-Neoplastic/benign',
                                    label: 'Bile Duct - Non-Neoplastic/benign'
                                },
                                {value: 'Bile Duct Cancer History', label: 'Bile Duct Cancer History'},
                                {value: 'Bladder Cancer History', label: 'Bladder Cancer History'},
                                {
                                    value: 'Blood - Use Hematologic Cancer History',
                                    label: 'Blood - Use Hematologic Cancer History'
                                },
                                {value: 'Bone  - Non-Neoplastic/benign', label: 'Bone  - Non-Neoplastic/benign'},
                                {value: 'Bone Cancer History', label: 'Bone Cancer History'},
                                {value: 'Brain Cancer History', label: 'Brain Cancer History'},
                                {value: 'Breast Cancer History', label: 'Breast Cancer History'},
                                {value: 'Bronchial Cancer History', label: 'Bronchial Cancer History'},
                                {
                                    value: 'Carcinoma of Unknown Primary, History',
                                    label: 'Carcinoma of Unknown Primary, History'
                                },
                                {value: 'Cardiac Cancer History', label: 'Cardiac Cancer History'},
                                {
                                    value: 'Central Nervous System, other than Brain, Cancer History',
                                    label: 'Central Nervous System, other than Brain, Cancer History'
                                },
                                {value: 'Cervical Cancer History', label: 'Cervical Cancer History'},
                                {value: 'Clin Path Dx Match Bio Path Dx', label: 'Clin Path Dx Match Bio Path Dx'},
                                {value: 'Colon Cancer History', label: 'Colon Cancer History'},
                                {value: 'Colorectal Cancer History', label: 'Colorectal Cancer History'},
                                {value: 'DCIS - Breast', label: 'DCIS - Breast'},
                                {value: 'Diagnostic', label: 'Diagnostic'},
                                {value: 'Endocrine Cancer History', label: 'Endocrine Cancer History'},
                                {value: 'Endometrial Cancer History', label: 'Endometrial Cancer History'},
                                {value: 'Esophageal Cancer History', label: 'Esophageal Cancer History'},
                                {
                                    value: 'Fallopian Tube - Non-Neoplastic/benign',
                                    label: 'Fallopian Tube - Non-Neoplastic/benign'
                                },
                                {
                                    value: 'Gallbladder - Non-Neoplastic/benign',
                                    label: 'Gallbladder - Non-Neoplastic/benign'
                                },
                                {value: 'Gallbladder Cancer History', label: 'Gallbladder Cancer History'},
                                {value: 'Gastric - Use Stomach', label: 'Gastric - Use Stomach'},
                                {
                                    value: 'Gastrointestinal Stromal Tumor History',
                                    label: 'Gastrointestinal Stromal Tumor History'
                                },
                                {value: 'Head and Neck Cancer History', label: 'Head and Neck Cancer History'},
                                {value: 'Hematologic Cancer History', label: 'Hematologic Cancer History'},
                                {value: 'Hypopharyngeal Cancer History', label: 'Hypopharyngeal Cancer History'},
                                {value: 'IPMN - Pancreas', label: 'IPMN - Pancreas'},
                                {
                                    value: 'Kidney (Use Renal) Renal Cancer History',
                                    label: 'Kidney (Use Renal) Renal Cancer History'
                                },
                                {value: 'Laryngeal Cancer History', label: 'Laryngeal Cancer History'},
                                {value: 'Leukemia History', label: 'Leukemia History'},
                                {value: 'Lip Cancer History', label: 'Lip Cancer History'},
                                {value: 'Lung Cancer History', label: 'Lung Cancer History'},
                                {value: 'Lymphoma History', label: 'Lymphoma History'},
                                {value: 'Malignant', label: 'Malignant'},
                                {value: 'Malignant, Invasive', label: 'Malignant, Invasive'},
                                {value: 'Melanoma, Skin Cancer History', label: 'Melanoma, Skin Cancer History'},
                                {value: 'Mesothelioma History', label: 'Mesothelioma History'},
                                {value: 'Metastatic', label: 'Metastatic'},
                                {value: 'Mouth - Use Oral Cancer History', label: 'Mouth - Use Oral Cancer History'},
                                {value: 'Nasal Cavity Cancer History', label: 'Nasal Cavity Cancer History'},
                                {value: 'Nerve - Non-Neoplastic/benign', label: 'Nerve - Non-Neoplastic/benign'},
                                {value: 'Nerve Cancer', label: 'Nerve Cancer'},
                                {value: 'Non-Diagnostic', label: 'Non-Diagnostic'},
                                {value: 'Non-Malignant', label: 'Non-Malignant'},
                                {value: 'Non-Malignant, Diseased', label: 'Non-Malignant, Diseased'},
                                {value: 'Normal/Control', label: 'Normal/Control'},
                                {value: 'Not Specified', label: 'Not Specified'},
                                {value: 'Oral Cancer History', label: 'Oral Cancer History'},
                                {value: 'Oropharyngeal Cancer History', label: 'Oropharyngeal Cancer History'},
                                {
                                    value: 'Oropharynx - Non-Neoplastic/benign',
                                    label: 'Oropharynx - Non-Neoplastic/benign'
                                },
                                {value: 'Ovarian Cancer History', label: 'Ovarian Cancer History'},
                                {value: 'Ovary - Non-Neoplastic/benign', label: 'Ovary - Non-Neoplastic/benign'},
                                {value: 'Pancreatic Cancer History', label: 'Pancreatic Cancer History'},
                                {value: 'Penis Cancer History', label: 'Penis Cancer History'},
                                {value: 'Pharyngeal Cancer History', label: 'Pharyngeal Cancer History'},
                                {value: 'Pituitary Cancer History', label: 'Pituitary Cancer History'},
                                {value: 'Pre-Malignant', label: 'Pre-Malignant'},
                                {value: 'Prostate - Non-Neoplastic/benign', label: 'Prostate - Non-Neoplastic/benign'},
                                {value: 'Prostate Cancer History', label: 'Prostate Cancer History'},
                                {value: 'Rectal Cancer History', label: 'Rectal Cancer History'},
                                {value: 'Renal Cancer History', label: 'Renal Cancer History'},
                                {
                                    value: 'Salivary Gland - Non-Neoplastic/benign',
                                    label: 'Salivary Gland - Non-Neoplastic/benign'
                                },
                                {value: 'Salivary Gland Cancer History', label: 'Salivary Gland Cancer History'},
                                {value: 'Sarcoma', label: 'Sarcoma'},
                                {value: 'Skin (see specific diagnosis)', label: 'Skin (see specific diagnosis)'},
                                {value: 'Skin - Non-Neoplastic/benign', label: 'Skin - Non-Neoplastic/benign'},
                                {
                                    value: 'Soft Tissue - Non-Neoplastic/benign',
                                    label: 'Soft Tissue - Non-Neoplastic/benign'
                                },
                                {
                                    value: 'Squamous Cell Carcinoma Skin Cancer History',
                                    label: 'Squamous Cell Carcinoma Skin Cancer History'
                                },
                                {value: 'Stomach - Non-Neoplastic/benign', label: 'Stomach - Non-Neoplastic/benign'},
                                {value: 'Stomach/Gastric Cancer History', label: 'Stomach/Gastric Cancer History'},
                                {value: 'Testicular Cancer History', label: 'Testicular Cancer History'},
                                {value: 'Thymoma Cancer History', label: 'Thymoma Cancer History'},
                                {value: 'Thyroid Cancer History', label: 'Thyroid Cancer History'},
                                {value: 'Thyroid Nodule', label: 'Thyroid Nodule'},
                                {value: 'Tongue Cancer History', label: 'Tongue Cancer History'},
                                {value: 'Tonsillar Cancer History', label: 'Tonsillar Cancer History'},
                                {value: 'Ureteral Cancer History', label: 'Ureteral Cancer History'},
                                {
                                    value: 'Uterine Cancer - Use Endometrial Cancer History',
                                    label: 'Uterine Cancer - Use Endometrial Cancer History'
                                },
                                {
                                    value: 'Uterus: Endometrium - Non-Neoplastic/benign',
                                    label: 'Uterus: Endometrium - Non-Neoplastic/benign'
                                },
                                {value: 'Vaginal Cancer History', label: 'Vaginal Cancer History'},
                                {value: 'Vulvar Cancer History', label: 'Vulvar Cancer History'}
                            ]
                        }
                    },
                    {
                        field: 'COLL_QTY_UNITS', name: 'Collected Amount',
                        minWidth: 100,
                        visible: true,
                        resizable: true,
                        cellTemplate: '<div class="ui-grid-cell-contents">' +
                        '{{row.entity.COLL_SPEC_QTY}}' + ' ' + '{{row.entity.COLL_QTY_UNITS}}' +
                        '</div>',
                        treeAggregationType: uiGridGroupingConstants.aggregation.SUM
                    },
                    {
                        field: 'COLL_SPEC_QTY', name: 'Collected Amount Units', visible: false,
                        minWidth: 100,
                        resizable: true,
                        treeAggregationType: uiGridGroupingConstants.aggregation.SUM
                    },
                    {
                        field: 'CURRENT_SPEC_QTY', name: 'Current Amount',
                        type: 'number',
                        minWidth: 100,
                        cellTemplate: '<div class="ui-grid-cell-contents">' +
                        '{{row.entity.CURRENT_SPEC_QTY}}' + ' ' + '{{row.entity.CURRENT_SPEC_UNITS}}' +
                        '</div>',
                        filters: [{
                            //condition: uiGridConstants.filter.GREATER_THAN,
                            placeholder: 'amt > x',
                            condition: function (searchTerm, cellValue) {
                                if (cellValue != undefined) {
                                    /* alert("value=" +cellValue.PK_CODELST_CATEGORY.description + " Search: " + searchTerm
                                     );*/
                                    if (parseInt(cellValue) > searchTerm) {
                                        return cellValue;
                                    }
                                }
                            }

                        }
                        ],
                        resizable: true,
                        /*cellTemplate: '<div class="ui-grid-cell-contents">' +
                         '{{row.entity.CURRENT_SPEC_QTY}}' + ' ' + '{{row.entity.CURRENT_SPEC_QTY_UNITS}}' +
                         '</div>',*/
                        cellClass: function (grid, row, col, rowIndex, colIndex) {
                            //var val = grid.getCellValue(row, col);
                            //Check if the collected quatity is > required and color green
                            // == color orange, < color pink
                            if (row.entity.CURRENT_SPEC_QTY > row.entity.COLL_SPEC_QTY) {
                                //Take note that I created a .grid .ui-grid-row .green css entry for this to work....
                                return 'green';
                            }
                            else if (row.entity.CURRENT_SPEC_QTY < row.entity.COLL_SPEC_QTY) {
                                return 'pink';
                            }
                            else if (row.entity.CURRENT_SPEC_QTY == row.entity.COLL_SPEC_QTY) {
                                return 'yellow';
                            }
                            else {
                                return 'grey';
                            }
                        },
                        treeAggregationType: uiGridGroupingConstants.aggregation.SUM
                    },
                    {
                        field: 'CURRENT_SPEC_QTY_UNITS', name: 'Current Amount Units', visible: false,
                        minWidth: 100,
                        resizable: true
                    },
                    {
                        field: 'STATUS_DESC', name: 'Current Status',
                        minWidth: 100,
                        resizable: true,
                        enableCellEditOnFocus: false,
                        cellTemplate: '<div class="ui-grid-cell-contents">' +
                        '<span> {{COL_FIELD}} ' +
                        '<span class="pull-right">' +
                        '<button type="button" class="glyphicon glyphicon-filter" ' +
                        'aria-hidden="true"' +
                        'ng-click="grid.appScope.setSampleStatusFilter(row.entity)"></button></a>' +
                        '</span></span>' +
                        '</div>'
                    },
                    {
                        field: 'OWNER', name: 'Sample Owner',
                        minWidth: 100,
                        resizable: true,
                        visible: false
                    }
                ]
                //data: 'data'
            };

            //Create angular grid for eSample form response list
            $scope.sampleFormListdata = [];
            $scope.sampleFormListGridOptions = {
                enableGridMenu: true,
                enableFiltering: true,
                enableSorting: true,
                exporterMenuPdf: false,
                paginationPageSizes: [10, 25, 50],
                paginationPageSize: 5,
                onRegisterApi: function (gridApi) {
                    $scope.sampleFormListGridApi = gridApi;

                    //Load the data the folloing method will populate $scope.gridOptions.data
                    getBioSamplesRequestList();
                },
                columnDefs: [
                    {
                        field: 'SPEC_ORGANIZATION', name: 'Organization',
                        width: 130,
                        minWidth: 100,
                        resizable: true,
                        visible: true,
                        //grouping: {groupPriority: 0},
                        filter: {
                            type: uiGridConstants.filter.SELECT,
                            selectOptions: [
                                {value: 'Akron Childrens Hospital', label: 'Akron Childrens Hospital'},
                                {value: 'Baylor College of Medicine', label: 'Baylor College of Medicine'},
                                {
                                    value: 'Bleeding & Clotting Disorders Institute',
                                    label: 'Bleeding & Clotting Disorders Institute'
                                },
                                {value: 'BloodCenter of Wisconsin', label: 'BloodCenter of Wisconsin'},
                                {value: 'Center 1 - UCSD', label: 'Center 1 - UCSD'},
                                {
                                    value: 'Childrens Hospital & Research Center Oakland',
                                    label: 'Childrens Hospital & Research Center Oakland'
                                },
                                {value: 'Childrens Hospital of Chicago', label: 'Childrens Hospital of Chicago'},
                                {
                                    value: 'Childrens Hospital of the Kings Daughters',
                                    label: 'Childrens Hospital of the Kings Daughters'
                                },
                                {value: 'Cincinnati Childrens Hospital', label: 'Cincinnati Childrens Hospital'},
                                {value: 'Cornell University', label: 'Cornell University'},
                                {
                                    value: 'Drexel University College of Medicine',
                                    label: 'Drexel University College of Medicine'
                                },
                                {
                                    value: 'Emory University School of Medicine',
                                    label: 'Emory University School of Medicine'
                                },
                                {
                                    value: 'Indiana Hemophilia & Thrombosis Center, Inc',
                                    label: 'Indiana Hemophilia & Thrombosis Center, Inc'
                                },
                                {value: 'Jewish Hospital', label: 'Jewish Hospital'},
                                {value: 'Johns Hopkins Childrens Center', label: 'Johns Hopkins Childrens Center'},
                                {
                                    value: 'Loma Linda University Medical Center',
                                    label: 'Loma Linda University Medical Center'
                                },
                                {value: 'Michigan State University', label: 'Michigan State University'},
                                {
                                    value: 'Mountain States Regional Hem. and Thrombosis Ctr.',
                                    label: 'Mountain States Regional Hem. and Thrombosis Ctr.'
                                },
                                {value: 'NVCI', label: 'NVCI'},
                                {value: 'Nationwide Childrens Hospital', label: 'Nationwide Childrens Hospital'},
                                {
                                    value: 'Newark Beth Israel Medical Center',
                                    label: 'Newark Beth Israel Medical Center'
                                },
                                {value: 'Northwestern University', label: 'Northwestern University'},
                                {value: 'Norton Hospital', label: 'Norton Hospital'},
                                {value: 'ORGNAME', label: 'ORGNAME'},
                                {
                                    value: 'Oregon Health & Science University',
                                    label: 'Oregon Health & Science University'
                                },
                                {value: 'Queens University', label: 'Queens University'},
                                {value: 'Rochester General Hospital', label: 'Rochester General Hospital'},
                                {value: 'Rush Univeristy Medical Center', label: 'Rush Univeristy Medical Center'},
                                {value: 'Toledo Childrens Hospital', label: 'Toledo Childrens Hospital'},
                                {
                                    value: 'Tulane University Health Sciences Center',
                                    label: 'Tulane University Health Sciences Center'
                                },
                                {value: 'UCSD', label: 'UCSD'},
                                {
                                    value: 'Univ. of Texas Health Science Center at Houston',
                                    label: 'Univ. of Texas Health Science Center at Houston'
                                },
                                {
                                    value: 'Univeristy of North Carolina at Chapel Hill',
                                    label: 'Univeristy of North Carolina at Chapel Hill'
                                },
                                {value: 'University Hospital', label: 'University Hospital'},
                                {value: 'University of Iowa', label: 'University of Iowa'},
                                {
                                    value: 'University of Louisville - Brown Cancer Center',
                                    label: 'University of Louisville - Brown Cancer Center'
                                },
                                {value: 'University of Pennsylvania', label: 'University of Pennsylvania'},
                                {value: 'University of Pittsburgh', label: 'University of Pittsburgh'},
                                {
                                    value: 'University of Texas Southwestern Medical Center',
                                    label: 'University of Texas Southwestern Medical Center'
                                },
                                {value: 'University of Wisconsin Madison', label: 'University of Wisconsin Madison'},
                                {value: 'Vanderbilt University', label: 'Vanderbilt University'},
                                {value: 'Wayne State University', label: 'Wayne State University'},
                                {value: 'White Plains Hospital', label: 'White Plains Hospital'}
                            ]
                        }
                    },
                    {
                        field: 'SPEC_TYPE', name: 'Type', minWidth: 170,
                        resizable: true,
                        enableCellEditOnFocus: false,
                        filter: {
                            type: uiGridConstants.filter.SELECT,
                            selectOptions: [
                                //Options: [
                                {value: 'ACD Tube', label: 'ACD Tube'},
                                {value: 'BAL', label: 'BAL'},
                                {value: 'Bacterial Isolates', label: 'Bacterial Isolates'},
                                {value: 'Biospecimen', label: 'Biospecimen'},
                                {value: 'Blood', label: 'Blood'},
                                {value: 'Blood Spot', label: 'Blood Spot'},
                                {value: 'Body Cavity Fluid', label: 'Body Cavity Fluid'},
                                {value: 'Bone Marrow Aspirate', label: 'Bone Marrow Aspirate'},
                                {value: 'Breast Milk', label: 'Breast Milk'},
                                {value: 'Breath Condensate', label: 'Breath Condensate'},
                                {value: 'Buccual Swab', label: 'Buccual Swab'},
                                {value: 'Buffy Coat', label: 'Buffy Coat'},
                                {value: 'CPT Tube', label: 'CPT Tube'},
                                {value: 'CSF', label: 'CSF'},
                                {value: 'Cell', label: 'Cell'},
                                {value: 'Coagulated Blood', label: 'Coagulated Blood'},
                                {value: 'DNA', label: 'DNA'},
                                {value: 'Fixed Tissue', label: 'Fixed Tissue'},
                                {value: 'Fixed Tissue Block', label: 'Fixed Tissue Block'},
                                {value: 'Fixed Tissue Slide', label: 'Fixed Tissue Slide'},
                                {value: 'Fresh Tissue', label: 'Fresh Tissue'},
                                {value: 'Frozen Cell Pellet', label: 'Frozen Cell Pellet'},
                                {value: 'Frozen Tissue', label: 'Frozen Tissue'},
                                {value: 'Lavage', label: 'Lavage'},
                                {value: 'Nasal Swab', label: 'Nasal Swab'},
                                {value: 'Non-Viable Cell Aliquot', label: 'Non-Viable Cell Aliquot'},
                                {value: 'PBMC', label: 'PBMC'},
                                {value: 'PCR Product', label: 'PCR Product'},
                                {value: 'Plasma', label: 'Plasma'},
                                {value: 'Plasma Aliquot', label: 'Plasma Aliquot'},
                                {value: 'Protein', label: 'Protein'},
                                {value: 'RNA', label: 'RNA'},
                                {value: 'Saliva', label: 'Saliva'},
                                {value: 'Segment', label: 'Segment'},
                                {value: 'Serum', label: 'Serum'},
                                {value: 'Serum Aliquot', label: 'Serum Aliquot'},
                                {value: 'Stool', label: 'Stool'},
                                {value: 'Synovial Fluid', label: 'Synovial Fluid'},
                                {value: 'Tissue', label: 'Tissue'},
                                {value: 'Urine', label: 'Urine'},
                                {value: 'Viable Cell Aliquot', label: 'Viable Cell Aliquot'},
                                {value: 'Whole Blood', label: 'Whole Blood'},
                                {value: 'Xenograft', label: 'Xenograft'},
                                {value: 'cDNA', label: 'cDNA'}
                            ]
                            //disableCancelFilterButton: true
                        },
                        cellTooltip: function (row, col) {
                            return 'Organization:' +
                                row.entity.SPEC_ORGANIZATION +
                                '\nOwner Name:' +
                                row.entity.OWNER +
                                '\nSample Id: ' +
                                row.entity.SPEC_ID +
                                '\nParent Sample Id:' +
                                row.entity.PARENT_SPEC_ID
                        }
                    },
                    {
                        field: 'SPEC_DESCRIPTION', name: 'Description',
                        minWidth: 100,
                        resizable: true,
                        visible: false

                    },
                    {
                        field: 'SPEC_ANATOMIC_SITE',
                        name: 'Anatomic Site',
                        width: 150,
                        minWidth: 100,
                        resizable: true,
                        cellTooltip: function (row, col) {
                            return 'Name: ' + row.entity.SPEC_ORGANIZATION;
                        },
                        filter: {
                            type: uiGridConstants.filter.SELECT,
                            selectOptions: [
                                {value: 'Abdominal', label: 'Abdominal'},
                                {value: 'Adrenal Gland, Left', label: 'Adrenal Gland, Left'},
                                {value: 'Adrenal Gland, Right', label: 'Adrenal Gland, Right'},
                                {value: 'Adrenal gland', label: 'Adrenal gland'},
                                {value: 'Anus', label: 'Anus'},
                                {value: 'Appendix', label: 'Appendix'},
                                {value: 'Base of tongue, NOS', label: 'Base of tongue, NOS'},
                                {value: 'Bladder', label: 'Bladder'},
                                {value: 'Body of pancreas', label: 'Body of pancreas'},
                                {value: 'Bones', label: 'Bones'},
                                {value: 'Bowel peritoneum/serosa', label: 'Bowel peritoneum/serosa'},
                                {value: 'Brain', label: 'Brain'},
                                {value: 'Breast', label: 'Breast'},
                                {value: 'Breast, Left', label: 'Breast, Left'},
                                {value: 'Breast, NOS', label: 'Breast, NOS'},
                                {value: 'Breast, Right', label: 'Breast, Right'},
                                {value: 'Cervix', label: 'Cervix'},
                                {value: 'Colon', label: 'Colon'},
                                {
                                    value: 'Connective, Subcutaneous and other soft tissues of head, face, and neck',
                                    label: 'Connective, Subcutaneous and other soft tissues of head, face, and neck'
                                },
                                {
                                    value: 'Connective, Subcutaneous and other soft tissues of lower limb and hip',
                                    label: 'Connective, Subcutaneous and other soft tissues of lower limb and hip'
                                },
                                {value: 'Cul-de-sac', label: 'Cul-de-sac'},
                                {value: 'Diaphragm', label: 'Diaphragm'},
                                {value: 'Duodenum', label: 'Duodenum'},
                                {value: 'Elbow, Left', label: 'Elbow, Left'},
                                {value: 'Endometrium', label: 'Endometrium'},
                                {value: 'Esophagus', label: 'Esophagus'},
                                {value: 'Eye, Left (OS)', label: 'Eye, Left (OS)'},
                                {value: 'Fallopian tube', label: 'Fallopian tube'},
                                {value: 'Gallbladder', label: 'Gallbladder'},
                                {value: 'Head of pancreas', label: 'Head of pancreas'},
                                {value: 'Head, face or neck, NOS', label: 'Head, face or neck, NOS'},
                                {value: 'Hip, Left', label: 'Hip, Left'},
                                {value: 'Ileum', label: 'Ileum'},
                                {value: 'Intestine, Small', label: 'Intestine, Small'},
                                {value: 'Kidney', label: 'Kidney'},
                                {value: 'Kidney, Left', label: 'Kidney, Left'},
                                {value: 'Kidney, NOS', label: 'Kidney, NOS'},
                                {value: 'Kidney, Right', label: 'Kidney, Right'},
                                {value: 'Knee, Left', label: 'Knee, Left'},
                                {value: 'Knee, Right', label: 'Knee, Right'},
                                {value: 'Larynx', label: 'Larynx'},
                                {value: 'Lingual tonsil', label: 'Lingual tonsil'},
                                {value: 'Lip, Lower', label: 'Lip, Lower'},
                                {value: 'Liver', label: 'Liver'},
                                {value: 'Lower lobe, lung', label: 'Lower lobe, lung'},
                                {value: 'Lower-outer quadrant of breast', label: 'Lower-outer quadrant of breast'},
                                {value: 'Lung', label: 'Lung'},
                                {value: 'Lung, Left Lower lobe ', label: 'Lung, Left Lower lobe '},
                                {value: 'Lung, Left Upper lobe ', label: 'Lung, Left Upper lobe '},
                                {value: 'Lung, NOS', label: 'Lung, NOS'},
                                {value: 'Lung, Right Lower Lobe', label: 'Lung, Right Lower Lobe'},
                                {value: 'Lung, Right Middle Lobe', label: 'Lung, Right Middle Lobe'},
                                {value: 'Lung, Right Upper Lobe', label: 'Lung, Right Upper Lobe'},
                                {value: 'Lymph node', label: 'Lymph node'},
                                {
                                    value: 'Lymph nodes of head, face and neck',
                                    label: 'Lymph nodes of head, face and neck'
                                },
                                {
                                    value: 'Lymph nodes of inguinal region or leg',
                                    label: 'Lymph nodes of inguinal region or leg'
                                },
                                {value: 'Mesentery', label: 'Mesentery'},
                                {value: 'NERVE', label: 'NERVE'},
                                {value: 'Neck', label: 'Neck'},
                                {value: 'Nose', label: 'Nose'},
                                {value: 'Not specified', label: 'Not specified'},
                                {value: 'Omentum', label: 'Omentum'},
                                {value: 'Oral', label: 'Oral'},
                                {value: 'Other', label: 'Other'},
                                {value: 'Other, specify:', label: 'Other, specify:'},
                                {value: 'Ovary', label: 'Ovary'},
                                {value: 'Palate, NOS', label: 'Palate, NOS'},
                                {value: 'Pancreas', label: 'Pancreas'},
                                {value: 'Pancreas, NOS', label: 'Pancreas, NOS'},
                                {value: 'Paracolic gutter', label: 'Paracolic gutter'},
                                {value: 'Parotid Gland', label: 'Parotid Gland'},
                                {value: 'Parotid gland', label: 'Parotid gland'},
                                {value: 'Pelvic wall', label: 'Pelvic wall'},
                                {value: 'Pelvis, NOS', label: 'Pelvis, NOS'},
                                {value: 'Penis', label: 'Penis'},
                                {value: 'Peritoneum', label: 'Peritoneum'},
                                {value: 'Peritoneum/uterine serosa', label: 'Peritoneum/uterine serosa'},
                                {value: 'Prostate', label: 'Prostate'},
                                {value: 'Rectosigmoid', label: 'Rectosigmoid'},
                                {value: 'Rectum', label: 'Rectum'},
                                {value: 'Renal pelvis', label: 'Renal pelvis'},
                                {value: 'Retroperitoneal', label: 'Retroperitoneal'},
                                {value: 'Salivary Gland', label: 'Salivary Gland'},
                                {value: 'Skin', label: 'Skin'},
                                {value: 'Skin of lower limb and hip', label: 'Skin of lower limb and hip'},
                                {
                                    value: 'Skin of other and unspecified parts of face',
                                    label: 'Skin of other and unspecified parts of face'
                                },
                                {value: 'Skin of scalp and neck', label: 'Skin of scalp and neck'},
                                {value: 'Skin of trunk', label: 'Skin of trunk'},
                                {value: 'Skin, NOS', label: 'Skin, NOS'},
                                {value: 'Small Intestine', label: 'Small Intestine'},
                                {value: 'Spleen', label: 'Spleen'},
                                {value: 'Stomach', label: 'Stomach'},
                                {value: 'Stomach, NOS', label: 'Stomach, NOS'},
                                {value: 'Submandibular gland', label: 'Submandibular gland'},
                                {value: 'Testis', label: 'Testis'},
                                {
                                    value: '========= Lip, oral cavity and pharynx ==========',
                                    label: '========= Lip, oral cavity and pharynx =========='
                                }
                            ]
                        }
                    },
                    {
                        field: 'SPEC_PATHOLOGY_STAT', name: 'Pathological Status',
                        minWidth: 100,
                        resizable: true
                    },
                    {
                        field: 'COLL_QTY_UNITS', name: 'Collected Amount',
                        minWidth: 100,
                        visible: true,
                        resizable: true,
                        treeAggregationType: uiGridGroupingConstants.aggregation.SUM
                    },
                    {
                        field: 'CURRENT_SPEC_QTY', name: 'Current Amount',
                        type: 'number',
                        minWidth: 100,
                        cellTemplate: '<div class="ui-grid-cell-contents">' +
                        '{{row.entity.CURRENT_SPEC_QTY}}' + ' ' + '{{row.entity.CURRENT_SPEC_UNITS}}' +
                        '</div>',
                        filters: [{
                            //condition: uiGridConstants.filter.GREATER_THAN,
                            placeholder: 'amt > x',
                            condition: function (searchTerm, cellValue) {
                                if (cellValue != undefined) {
                                    /* alert("value=" +cellValue.PK_CODELST_CATEGORY.description + " Search: " + searchTerm
                                     );*/
                                    if (parseInt(cellValue) > searchTerm) {
                                        return cellValue;
                                    }
                                }
                            }

                        }
                        ],
                        resizable: true,
                        /*cellTemplate: '<div class="ui-grid-cell-contents">' +
                         '{{row.entity.CURRENT_SPEC_QTY}}' + ' ' + '{{row.entity.CURRENT_SPEC_QTY_UNITS}}' +
                         '</div>',*/
                        cellClass: function (grid, row, col, rowIndex, colIndex) {
                            //var val = grid.getCellValue(row, col);
                            //Check if the collected quatity is > required and color green
                            // == color orange, < color pink
                            if (row.entity.CURRENT_SPEC_QTY > row.entity.COLL_SPEC_QTY) {
                                //Take note that I created a .grid .ui-grid-row .green css entry for this to work....
                                return 'green';
                            }
                            else if (row.entity.CURRENT_SPEC_QTY < row.entity.COLL_SPEC_QTY) {
                                return 'pink';
                            }
                            else if (row.entity.CURRENT_SPEC_QTY == row.entity.COLL_SPEC_QTY) {
                                return 'yellow';
                            }
                            else {
                                return 'grey';
                            }
                        },
                        treeAggregationType: uiGridGroupingConstants.aggregation.SUM
                    },
                    {
                        field: 'OWNER', name: 'Sample Owner',
                        minWidth: 100,
                        resizable: true,
                        visible: false
                    }
                ]
            };


            //REST call to get biosample data
            var getBioSamples = function () {

                //Create loading modal
                $scope.eSampleGridLoading = $scope.gridOptions.data && 'Paging...' || 'Loading Data...';

                //Get study Details
                // <%--Angular REST request--%>
                // <%--Create post request--%>
                //need to replace static code with angular service
                var req = {
                    method: 'POST',
                    url: 'https://bizdev3.veloseresearch.com:55555/PTReports/getBioSpecimen',
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8'
                    },
                    data: {
                        Biospecimen: {
                            CONVERGE_SITE_ID: "?",
                            RECID: "?",
                            SPEC_ID: "?",
                            PARENT_SPEC_ID: "?",
                            SPEC_TYPE: ptFilterOptions.sampleType,
                            SPEC_DESCRIPTION: "?",
                            SPEC_ANATOMIC_SITE: ptFilterOptions.anatomicalSite,
                            SPEC_PATHOLOGY_STAT: ptFilterOptions.pathStatus,
                            COLL_SPEC_QTY: "?",
                            COLL_QTY_UNITS: "?",
                            CURRENT_SPEC_QTY: "?",
                            CURRENT_SPEC_QTY_UNITS: "?",
                            STATUS_DESC: ptFilterOptions.sampleStatus,
                            SPEC_ORGANIZATION: ptFilterOptions.organization,
                            OWNER: "?",
                            DateRange: {
                                CREATED_ON_GENERAL: {
                                    Start: "?",
                                    End: "?"
                                },
                                LAST_MODIFIED_DATE_GENERAL: {
                                    Start: "?",
                                    End: "?"
                                }
                            },
                            ResultRange: {
                                start: paginationOptions.startRow,
                                noofrows: paginationOptions.numberOfRows
                            }
                        }
                    }
                };

                $http(req)
                    .then(function (response) { //success

                            //Check if there is a valid sample present
                            if (response.data.BiospecimenData.BiospecimenList) {
                                var bioSpecimenList = response.data.BiospecimenData.BiospecimenList.Biospecimen;

                                //Mark the total number of rows that was obtained
                                $scope.gridOptions.totalItems = response.data.BiospecimenData.NoOfRecords - 1;
                                //Mark the first row
                                var firstRow = (paginationOptions.startRow - 1) * paginationOptions.numberOfRows;

                                //Populate table as soon as the initial data is obtained
                                $scope.gridOptions.data = bioSpecimenList; //bioSpecimenList.slice(firstRow, firstRow + paginationOptions.numberOfRows);

                                $scope.soapStatus = "Web Service response: " + response.statusText
                                    + " @" + new Date().toLocaleDateString()
                                    + " " + new Date().toLocaleTimeString()
                                    + "";
                                $scope.bioSamplegridApi.core.refresh();
                            }
                            else  //No results present
                            {
                                //Mark the total number of rows that was obtained
                                $scope.gridOptions.totalItems = 0;
                                //Mark the first row
                                var firstRow = 0;

                                //Populate table as soon as the initial data is obtained
                                $scope.gridOptions.data = null; //bioSpecimenList.slice(firstRow, firstRow + paginationOptions.numberOfRows);

                                $scope.soapStatus = "Web Service response: " + response.statusText
                                    + " @" + new Date().toLocaleDateString()
                                    + " " + new Date().toLocaleTimeString()
                                    + "";
                                $scope.bioSamplegridApi.core.refresh();
                            }


                        }, function (response) { //error
                            $scope.status = response.status;
                        }
                    )
                    .finally(function () {
                        $scope.eSampleGridLoading = false;
                    });
            };


            //REST call to get biosample request form list from eResearch
            var getBioSamplesRequestList = function () {

                //Create loading modal
                $scope.eSampleFormListGridLoading = $scope.sampleFormListGridOptions.data && 'Paging...' || 'Loading Data...';

                //Get eSample Request list forms
                var req = {
                    method: 'POST',
                    url: 'https://bizdev3.veloseresearch.com:55555/FormResponse/bizdev3/AccountFormResponseList',
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8'
                    },
                    data: {
                        FormIdentifier: {
                            PK: 567 //BioSampleRequest_v2.0 BioSample Request form id
                        },
                        pageNumber: "1",
                        pageSize: "100"
                    }
                };

                $http(req)
                    .then(function (response) { //success

                        //reset sample response count
                        $scope.BioSampleRequestCount = 0;

                            //Assign the web service returned data to local variable
                            var bioSampleFormList = response.data.accountFormResponse.accountFormResponses;

                            //Loop through each growth details form response and construct rows for grid
                            $.each(bioSampleFormList, function (key, value) {
                                //Use underscore indexBy function to index by uniqueID since the form field ids are unique for this project.
                                var formFields = _.indexBy(value.formFieldResponses.field, 'uniqueID');

                                //Populate table as soon as the initial data is obtained
                                $scope.sampleFormListGridOptions.data.push({
                                        "SPEC_ORGANIZATION": formFields.Organization.value,
                                        "SPEC_TYPE": formFields['Sample Type'].value,
                                        "SPEC_DESCRIPTION": formFields.SPEC_DESCRIPTION.value,
                                        "SPEC_ANATOMIC_SITE": formFields.SPEC_ANATOMIC_SITE.value,
                                        "SPEC_PATHOLOGY_STAT": formFields.SPEC_PATHOLOGY_STAT.value,
                                        "COLL_QTY_UNITS": formFields.COLL_QTY_UNITS.value,
                                        "CURRENT_SPEC_QTY": formFields.CURRENT_SPEC_QTY.value,
                                        "OWNER": formFields.OWNER.value
                                    });

                                $scope.BioSampleRequestCount += 1;
                            });

                            $scope.soapStatus = "BioSample request form count: " + response.data
                                    .accountFormResponse.recordCount;
                            $scope.bioSamplegridApi.core.refresh();

                        }, function (response) { //error
                            $scope.status = "eResearch is busy try again. ";
                        }
                    )
                    .finally(function () {
                        $scope.eSampleFormListGridLoading = false;
                    });
            };


            //Set type filter for external filtering
            $scope.setSampleTypeFilter = function (rowEntity) {
                ptFilterOptions.sampleType = rowEntity.SPEC_TYPE;

                //Attach label to filter list
                var filterList = angular.element(document.querySelector('#filterList'));

                //Create label
                var typeFilterLabel = angular.element(
                    '<h4><span id="bioSampleTypeFilter" class="label label-info" ng-click="removeTypeFilter()">' +
                    '<span>' + rowEntity.SPEC_TYPE + '</span>' +
                    /*'<span id="removeTypeFilterBtn" type="button"' +
                     'class="glyphicon glyphicon-remove"' +
                     'ng-click="removeTypeFilter()"></span>'+*/
                    '</span></h4>');
                angular.element(typeFilterLabel).appendTo(filterList);

                getBioSamples();
            };

            //remove type filter
            $scope.removeTypeFilter = function () {
                //find label to filter
                var typeFilter = angular.element(document.querySelector('#bioSampleTypeFilter'));

                ptFilterOptions.sampleType = '';
                typeFilter.remove();
                getBioSamples();
            };

            //Set type filter for external filtering
            $scope.setSampleStatusFilter = function (rowEntity) {
                ptFilterOptions.sampleStatus = rowEntity.STATUS_DESC;

                //Attach label to filter list
                var filterList = angular.element(document.querySelector('#filterList'));

                //Create label
                var typeFilterLabel = angular.element(
                    '<h4><span id="bioSampleStatusFilter" class="label label-info">' +
                    '<span>' + rowEntity.STATUS_DESC + '</span>' +
                    '</span></h4>');
                angular.element(typeFilterLabel).appendTo(filterList);

                getBioSamples();
            };

            //remove type filter
            $scope.removeStatusFilter = function () {
                //find label to filter
                var typeFilter = angular.element(document.querySelector('#bioSampleStatusFilter'));

                ptFilterOptions.sampleStatus = '';
                typeFilter.remove();
                getBioSamples();
            };

            //Remove filters
            $scope.RemoveFilters = function () {
                $scope.removeStatusFilter();
                $scope.removeTypeFilter();

                //clear all table filters
                $scope.bioSamplegridApi.grid.clearAllFilters();
            };

            //Clear all filters
            var clearAllPTFilters = function () {
                ptFilterOptions.sampleType = '';
                ptFilterOptions.sampleStatus = '';
                getBioSamples();
            };

            //Save grid display options
            $scope.saveState = function () {
                $scope.bioSampleGridState = $scope.bioSamplegridApi.saveState.save();
            };

            //Restore grid display options from save
            $scope.restoreState = function () {
                $scope.bioSamplegridApi.saveState.restore($scope, $scope.bioSampleGridState);
            };

            //Get all the selected rows in the table and create form responses in eresearch
            $scope.CreateBioSampleRequestForms = function () {
                var selectedSamples = $scope.bioSamplegridApi.selection.getSelectedRows();
                //Loop to the list of selected rows on the grid.
                angular.forEach(selectedSamples, function (value, key) {

                    //Create webservice request
                    /*
                     * {
                     "soapenv:Envelope": {
                     "-xmlns:soapenv": "http://schemas.xmlsoap.org/soap/envelope/",
                     "-xmlns:ser": "http://velos.com/services/",
                     "soapenv:Body": {
                     "ser:createAccountFormResponse": {
                     "AccountFormResponse": {
                     "formFieldResponses": {
                     "field": [
                     {
                     fieldIdentifier: {
                     OID: "4a0fced7-627e-4a77-9dd5-3f9ec4f83163",
                     PK: "16977"
                     },
                     systemID: "er_def_date_01",
                     uniqueID: "er_def_date_01",
                     value: "04/07/2017"
                     },
                     {
                     fieldIdentifier: {
                     OID: "ea5d25c0-6e9d-4a09-9452-377a0a549ede",
                     PK: "16979"
                     },
                     uniqueID: "fld10050_13845_16978",
                     value: "Plasma"
                     }
                     ]
                     },
                     "formFillDt": "2017-07-04",
                     "formId": { PK: "566" },
                     "formStatus": {
                     "code": "working",
                     "description": "Work In Progress",
                     "type": "fillformstat"
                     },
                     "accountIdentifier": { PK: "50" }
                     }
                     }
                     }
                     }
                     }
                     * */
                    var data =
                    {
                        AccountFormResponse: {
                            formFieldResponses: {
                                field: [
                                    {
                                        fieldIdentifier: {
                                            OID: "89db61e7-b50a-45f9-bda4-16dcbba60cc3",
                                            PK: "17002"
                                        },
                                        systemID: "er_def_date_01",
                                        uniqueID: "er_def_date_01",
                                        value: moment().format("MM/DD/YYYY") //date format "04/10/2017"
                                    },
                                    {
                                        fieldIdentifier: {
                                            OID: "f1e809ae-075b-4108-aeba-95338cab2e58",
                                            PK: "17003"
                                        },
                                        systemID: "fld10050_13869_17003",
                                        uniqueID: "CONVERGE_SITE_ID",
                                        value: value.CONVERGE_SITE_ID
                                    },
                                    {
                                        fieldIdentifier: {
                                            OID: "755f44a2-8c96-4a05-ad59-92b076b129be",
                                            PK: "17004"
                                        },
                                        systemID: "fld10050_13870_17004",
                                        uniqueID: "Sample Type",
                                        value: value.SPEC_TYPE
                                    },
                                    {
                                        fieldIdentifier: {
                                            OID: "2e8c2b8f-c33e-4f9b-bc5b-cb75a8eb2d5e",
                                            PK: "16995"
                                        },
                                        systemID: "fld10050_13862_16995",
                                        uniqueID: "Organization",
                                        value: value.SPEC_ORGANIZATION
                                    },
                                    {
                                        fieldIdentifier: {
                                            OID: "a435d77b-6566-47c3-8176-110041ed428b",
                                            PK: "16996"
                                        },
                                        systemID: "fld10050_13863_16996",
                                        uniqueID: "SPEC_TYPE",
                                        value: value.SPEC_TYPE
                                    },
                                    {
                                        fieldIdentifier: {
                                            OID: "346a0e8f-cce5-4c58-aa38-9af6a9dd4de8",
                                            PK: "16997"
                                        },
                                        systemID: "fld10050_13864_16997",
                                        uniqueID: "SPEC_DESCRIPTION",
                                        value: value.SPEC_DESCRIPTION
                                    },
                                    {
                                        fieldIdentifier: {
                                            OID: "30b779b4-b917-47a4-86c2-8e6f945ea740",
                                            PK: "16998"
                                        },
                                        systemID: "fld10050_13865_16998",
                                        uniqueID: "SPEC_ANATOMIC_SITE",
                                        value: value.SPEC_ANATOMIC_SITE
                                    },
                                    {
                                        fieldIdentifier: {
                                            OID: "f409b1e7-8706-494b-b77b-28b608bf009f",
                                            PK: "16999"
                                        },
                                        systemID: "fld10050_13866_16999",
                                        uniqueID: "SPEC_PATHOLOGY_STAT",
                                        value: value.SPEC_PATHOLOGY_STAT
                                    },
                                    {
                                        fieldIdentifier: {
                                            OID: "dcee82e7-4f60-4774-8dbf-085ff9b96704",
                                            PK: "17000"
                                        },
                                        systemID: "fld10050_13867_17000",
                                        uniqueID: "COLL_QTY_UNITS",
                                        value: value.COLL_SPEC_QTY
                                    },
                                    {
                                        fieldIdentifier: {
                                            OID: "4704a723-7f4d-4974-ac69-aa1901260235",
                                            PK: "17001"
                                        },
                                        systemID: "fld10050_13868_17001",
                                        uniqueID: "CURRENT_SPEC_QTY",
                                        value: value.CURRENT_SPEC_QTY
                                    },
                                    {
                                        fieldIdentifier: {
                                            OID: "3b873a07-bfa6-417f-9e0b-8458ef8c2edf",
                                            PK: "17005"
                                        },
                                        systemID: "fld10050_13871_17005",
                                        uniqueID: "OWNER",
                                        value: value.OWNER
                                    }
                                ]
                            },
                            formFillDt: moment().format("YYYY-MM-DD"), //date format "2017-07-04",
                            formId: {PK: "567"},
                            formStatus: {
                                code: "working",
                                description: "Work In Progress",
                                type: "fillformstat"
                            },
                            accountIdentifier: {PK: "50"}
                        }
                    };


                    //Call REST Node server to get Form response list
                    $.ajax({
                        type: 'POST',
                        url: 'https://bizdev3.veloseresearch.com:55555/FormResponse/createAccountFormResponse',
                        data: JSON.stringify(data),
                        contentType: "application/json",
                        //dataType: 'json',
                        success: function (data) {
                            $scope.BioSampleRequestCount += 1;
                            $scope.soapStatus = data.Response.results.result["0"].action + " Success (" +
                                "FormId:" + data.Response.results.result["0"].objectId.PK + ")";

                            //Refresh form request list
                            getBioSamplesRequestList();
                        },
                        error: function (data) {
                            $scope.soapStatus1 = data.Response.results.result["0"].action;
                        }
                    });

                });
            };

        }]);
