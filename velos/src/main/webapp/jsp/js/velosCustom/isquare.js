var isquare = {
	setLocationIndex: {},
	addParam: {},
	createBeachhead: {},
	addOneMoreBeachhead: {}
};

isquare.setLocationIndex = function (locationIndex){
	if (locationIndex == undefined || locationIndex < 0){
		return;
	}
	isquare.isqObj.locationIndex = locationIndex;
};

isquare.addParam = function (paramKey, paramValue){
	if (!paramKey || paramKey.length <= 0){
		return;
	}
	isquare.isqObj.isquareParams[paramKey] = paramValue;
};

isquare.createBeachhead = function (args){
	if (isquare.requireAdded){
		isquare.addOneMoreBeachhead(args);
		return;
	}

	var isquareToken = args.isquareToken;
	if (!isquareToken || isquareToken.length < 1){ return false; }

	var beacHeadId = '';
	var location = args.location;
	var isqBeachheads = args.isquareBeachheads;
	var params = args.isquareParams; 
	var locIndex = args.locationIndex;
	locIndex = (!locIndex)? 0 : locIndex;
	
	var beachHeadArray = null;
	try {
		var locationObj = JSON.parse(isqBeachheads);
		//console.log ('locationObj '+locationObj);
		beachHeadArray = locationObj[location];
		var beachHeadObj = beachHeadArray[locIndex];
		//console.log ('beachHeadArray '+beachHeadArray);
		beacHeadId = beachHeadObj["_id"]; 
	} catch (e) {
		//console.log(isqBeachheads + ' ' + e);
	}

	//console.log('beacHeadId '+ beacHeadId);
	if (!beacHeadId && beacHeadId.length < 1){ return false; }

	if (beacHeadId && beacHeadId.length > 0){
		var isquareURL = args.isquareURL;	
		var sessUserId = params["loggedInUser"];
		if (!sessUserId || sessUserId.length < 1) { return false; }
	
		var scriptTag = document.createElement('script');
		scriptTag.onload = function() {
			define('global', {
				bHeadId: beacHeadId,
				app: 'VelosER',
				mainTarget: location,
				params: params,
				loggedInUser: sessUserId
			});
		};
		scriptTag.id = beacHeadId;
		scriptTag.type = "text/javascript";
		scriptTag.src = isquareURL+"/VelosER/"+args.isquareToken+"/require";
		scriptTag.setAttribute("data-main", isquareURL+"/VelosER/"+args.isquareToken+"/main");
		document.getElementsByTagName('head')[0].appendChild(scriptTag);
		isquare.requireAdded = true;
	}
};

isquare.addOneMoreBeachhead = function(args){
	if (!isquare.requireAdded){
		isquare.createBeachhead(args);
		return;
	}

	var isquareToken = args.isquareToken;
	if (!isquareToken || isquareToken.length < 1){ return false; }

	var beacHeadId = '';
	var location = args.location;
	var isqBeachheads = args.isquareBeachheads;
	var params = args.isquareParams; 
	var locIndex = args.locationIndex;
	
	var beachHeadArray = null;
	try {
		var locationObj = JSON.parse(isqBeachheads);
		//console.log ('locationObj '+locationObj);
		beachHeadArray = locationObj[location];
		var beachHeadObj = beachHeadArray[locIndex];
		//console.log ('beachHeadArray '+beachHeadArray);
		beacHeadId = beachHeadObj["_id"]; 
	} catch (e) {
		//console.log(isqBeachheads + ' ' + e);
	}

	//console.log('beacHeadId '+ beacHeadId);
	if (!beacHeadId && beacHeadId.length < 1){ return false; }

	if (beacHeadId && beacHeadId.length > 0){
		var sessUserId = params["loggedInUser"];
		if (!sessUserId || sessUserId.length < 1) { return false; }

		require.undef('global');
		define('global', {
		  bHeadId: beacHeadId,
		  app: 'VelosER',
	      mainTarget: location,
	      params: params,
	      loggedInUser: sessUserId
	    });
		require(['global', 'router/MainBeachHeadRouter'], function(global, MainBeachHeadRouter){
			//console.log(global.bHeadId);
			if (global.bHeadId){
				MainBeachHeadRouter.initialize(global.bHeadId);
			}
		});
	}
};