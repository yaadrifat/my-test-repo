<%@page import="antlr.StringUtils"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.velos.eres.business.common.CodeDao"%>
<%@page import="com.velos.eres.web.network.NetworkJB"%>
<%@page import="com.velos.eres.service.util.*"%>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="nwDao" scope="session" class="com.velos.eres.business.common.NetworkDao"/>
<jsp:useBean id ="auditRowEresJB" scope="session" class="com.velos.eres.audit.web.AuditRowEresJB"/>
<jsp:useBean id ="audittrails" scope="session" class="com.aithent.audittrail.reports.AuditUtils"/>
<jsp:useBean id ="userBean" scope="session" class="com.velos.eres.business.user.impl.UserBean"/>
<jsp:useBean id ="userJB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html");
	HttpSession tSession = request.getSession(false);
	//JSONObject jsObj = new JSONObject();
	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		//jsObj.put("result", -1);
		//jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
   	%>
		<input type="hidden" name="resultMsg" id="resultMsg" value="<%=MC.M_UsrNot_LoggedIn %>"/>    	
	<%
		//return;
	}
	int accountId=0;
	String acc = (String) tSession.getValue("accountId");
	accountId = EJBUtil.stringToNum(acc);
	String userId = (String) tSession.getValue("userId");
	 userJB.setUserId(StringUtil.stringToNum(userId));
	 userBean= userJB.getUserDetails();
	int pkuser = EJBUtil.stringToNum(userId);
	String ipAdd = (String) tSession.getValue("ipAdd");
	String networkId=request.getParameter("netWorkId");
	String operation=request.getParameter("operation");
	String assignUserId=request.getParameter("assignUserId");
	String nwPkUser=request.getParameter("nwPkUser");
	String trole=request.getParameter("trole");
	String networkUser= request.getParameter("networkUser")==null?"":request.getParameter("networkUser");
	boolean flag=true;	
   int success = 0;
   int additonalcheck=0;
   String additonalcheckVal="";
   int rid = 0;
   int ridSH = 0;
	try { 
		if("inserAddnl".equals(operation)||"fetch".equals(operation)){
			if("inserAddnl".equals(operation)){
			additonalcheck = nwDao.additonalCheckFlag(EJBUtil.stringToNum(networkUser),ipAdd,pkuser);
			}else{
				additonalcheckVal = nwDao.additonalCheckfetch(EJBUtil.stringToNum(networkUser));
			}
			%>
			<input type="hidden" name="additonalcheck" id="additonalcheck" value="<%=additonalcheck %>"/>
			<input type="hidden" name="additonalcheckVal" id="additonalcheckVal" value="<%=additonalcheckVal %>"/>
			
			<%
		}
		else{
		if("I".equals(operation)){
			success=nwDao.saveUserNetwork(EJBUtil.stringToNum(networkId), EJBUtil.stringToNum(assignUserId),EJBUtil.stringToNum(userId));
		}else if("U".equals(operation)){ 
			success=nwDao.updateUserNetwork(EJBUtil.stringToNum(nwPkUser), EJBUtil.stringToNum(trole), EJBUtil.stringToNum(userId));	
		}else if("AU".equals(operation)){ 
			success=nwDao.updateUserAdditional(EJBUtil.stringToNum(nwPkUser), EJBUtil.stringToNum(trole), EJBUtil.stringToNum(userId));	
		}else if("AD".equals(operation)){ 
			success=nwDao.updateUserNetworkaD(EJBUtil.stringToNum(nwPkUser), trole, EJBUtil.stringToNum(userId));
		}else if("DAddRole".equals(operation)){
			ridSH=nwDao.deleteStatusHistoryEnteries(EJBUtil.stringToNum(nwPkUser), "er_nwusers_addnlroles");
			if(ridSH > 0){
    			audittrails.updateAuditROw("eres", userBean.getUserId()+", "+userBean.getUserFirstName()+", "+userBean.getUserLastName(), StringUtil.integerToString(ridSH), "D");
    		}
	
			rid = auditRowEresJB.getRidForDeleteOperation(EJBUtil.stringToNum(nwPkUser), EJBUtil.stringToNum(userId), "ER_NWUSERS_ADDNLROLES", "PK_NWUSERS_ADDROLES");
			success=nwDao.deleteUserNetworkaDAddRole(EJBUtil.stringToNum(nwPkUser));
		}else{
			success=nwDao.deleteUserNetwork(EJBUtil.stringToNum(nwPkUser),EJBUtil.stringToNum(userId),ipAdd);
		}
		
		if(success>0){
    		flag=false;
			
    		if(rid > 0){
    			audittrails.updateAuditROw("eres", userBean.getUserId()+", "+userBean.getUserFirstName()+", "+userBean.getUserLastName(), StringUtil.integerToString(rid), "D");
    		}
    	
		%>
				<input type="hidden" name="resultMsg" id="resultMsg" value="<%=flag %>"/>    	
				
		<%
		}}}catch(Exception e){%>
	<input type="hidden" name="resultMsg" id="resultMsg" value="<%=flag %>"/> 
	<input type="hidden" name="additonalcheck" id="additonalcheck" value="<%=additonalcheck %>"/> 
	
	<% }%>
	