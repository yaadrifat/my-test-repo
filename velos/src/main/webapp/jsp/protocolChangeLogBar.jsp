<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*,com.velos.eres.service.util.*" %>
	<%
		String formID = "";
		String displayESign = "";
		String showDiscard = "";
		String showSubmit = "";
		String noBR = "";
		String eSignLabel="";
		eSignLabel=LC.L_Esignature;
        if ("userpxd".equals(Configuration.eSignConf)) {
	     eSignLabel="e-Password";
		}
		formID = request.getParameter("formID");
		displayESign = request.getParameter("displayESign");
		showDiscard= request.getParameter("showDiscard");
		showSubmit = request.getParameter("showSubmit");
		noBR = request.getParameter("noBR");
		    
		if (showSubmit == null)
			showSubmit = "";

		if (StringUtil.isEmpty(displayESign))
		{
			displayESign="Y";
		}

		if (StringUtil.isEmpty(showDiscard))
		{
			showDiscard="N";
		}

		if (!showDiscard.equals("LKP") && !("Y".equals(noBR))) {
		%>
	    <%}%>
<script>
var lastTimeSubmitted = 0;
function fnOnceEnterKeyPress(e) {
	try {
        if (e.keyCode == 13 || e.keyCode == 10) {
            var thisTimeSubmitted = new Date();
            if (!lastTimeSubmitted) { return true; }
            if (!thisTimeSubmitted) { return true; }
            if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
                return false;
            }
            lastTimeSubmitted = thisTimeSubmitted;
        }
	} catch(e) {}
	return true;
}

function fnClickOnce(e) {
	try {
		<%if(formID!=null && formID.equalsIgnoreCase("addevtvisitfrm")){%>
			if (validate(document.addeventvisit)== false) {
				setValidateFlag('false'); return false; 
			} else {
				setValidateFlag('true'); return true;
			}
		<%}%>
        var thisTimeSubmitted = new Date();
        if (!lastTimeSubmitted) { return true; }
        if (!thisTimeSubmitted) { return true; }
        if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
            return false;
        }
        lastTimeSubmitted = thisTimeSubmitted;
	} catch(e) {}
    return true;
}

</script>


<table id="submitBarTable" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	  <% if (displayESign.equals("Y")) { %>
	  <td align="right">  	
		 <span id="eSignMessage"></span>
		 <%=eSignLabel %><%-- e-Signature*****--%> <FONT class="Mandatory">* </FONT>&nbsp;
	   </td>
	  <% } %>

	  <% if (displayESign.equals("Y")) { %>
	  <td>
		<%-- <input type="password" name="eSign" id="eSign" maxlength="8"
		   onkeyup="ajaxvalidate('misc:'+this.id,4,'eSignMessage','Valid e-Sign','Invalid e-Sign','sessUserId')" onkeypress="return fnOnceEnterKeyPress(event)">*****--%>
		   
		    <%if ("userpxd".equals(Configuration.eSignConf)) {%>
		   <input type="password" name="eSign" id="eSign" style="background:url(./images/ic_lock_outline_gray_24dp.png) no-repeat; padding-left: 25px;" class="input21" autocomplete="off" onkeyup="ajaxvalidate('pass:'+this.id,-1,'eSignMessage','Valid-Password','Invalid-Password','sessUserId')" 
		   >
		   <%}else{ %>
		   <input type="password" name="eSign" id="eSign" maxlength="8" autocomplete="off"
		   onkeyup="ajaxvalidate('misc:'+this.id,4,'eSignMessage','<%=LC.L_Valid_Esign %>','<%=LC.L_Invalid_Esign %>','sessUserId')" onkeypress="return fnOnceEnterKeyPress(event)">
		   <%} %>
		   
		   
		
	  </td>
	  <% } %>
	  <td width="2%" >&nbsp;&nbsp;</td>
	  <td>
		<button type="button" id="saveDraftBtn" onclick="return fnClickOnce(event)" ondblclick="return false"><%=LC.L_Save%> Draft</button>
	  </td>
	  <td width="2%" >&nbsp;&nbsp;</td>
	  <td>
		<button type="button" id="unfreezeProtocolBtn" onclick="return fnClickOnce(event)" ondblclick="return false">Unfreeze Protocol</button>
	  </td>
	  <td width="2%" >&nbsp;&nbsp;</td>
	  <% if(!showSubmit.equals("N")) {%>
	  <td>
		<button type="submit" id="submit_btn" onclick="return fnClickOnce(event)" ondblclick="return false"><%=LC.L_Submit%> Protocol</button>
	     
	  </td>
	  <%}%>
  </tr>
  <tr><td colspan="8" align="center"><p><font size="3" color="red"><B>Upload "Informed Consent" document, if applicable.</B></font></p></td></tr>
</table>

	  <script>
	  <%if(formID!=null && !(formID.equalsIgnoreCase("addevtvisitfrm"))){%>
	  	linkFormSubmit('<%=formID%>');
	  <%}%>

	  </script>
