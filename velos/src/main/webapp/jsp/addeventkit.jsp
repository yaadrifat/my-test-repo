<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>


<title><%=LC.L_Evt_StorageKit%><%--Event Storage Kit*****--%></title>




<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">

function openLocLookup(formobj) {


	formobj.target="Lookup";
	formobj.method="post";
	formobj.action="multilookup.jsp?viewNameKeyword=keyword_storages&form=kit&seperator=,"+
                  "&keyword=location|STORAGE_NAME~mainFKStorage|PK_STORAGE|[VELHIDE]&filter=[VELGET:dfilter]";

	formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');
	if (formWin && !formWin.closed) formWin.focus();
	formobj.submit();
	formobj.target="_self";
	formobj.action="addeventkit.jsp";
	void(0);


}


//JM: 09NOV2009: #4046
function validate(formobj){


    if (formobj.location.value ==""){

    	alert("<%=MC.M_SelKit_BeforeSbmt%>");/*alert("Please select Storage Kit before submitting");*****/

		return false;

	} else {

		return true;

	}

}

</SCRIPT>



</head>







<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.*,com.velos.eres.service.util.*"%>

<jsp:useBean id="eventkitB" scope="request" class="com.velos.esch.web.eventKit.EventKitJB"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>


<% String src;

src= request.getParameter("srcmenu");

String eventName = "";



int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>



<DIV class="popDefault"  id="div1">



<%

	String protocolId = request.getParameter("protocolId");

	String eventId = request.getParameter("eventId");
	String fromPage= request.getParameter("fromPage");
	String mode = request.getParameter("mode");
	String calledFrom = request.getParameter("calledFrom");

	if (StringUtil.isEmpty(mode))
	{
		mode = "I";
	}



	HttpSession tSession = request.getSession(true);



	if (sessionmaint.isValidSession(tSession))
	{
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	 <jsp:include page="include.jsp" flush="true"/>

<%
	    String creator=(String) tSession.getAttribute("userId");
	    String accId = (String) tSession.getValue("accountId");
	    String ipAdd = (String) tSession.getValue("ipAdd");


		if (calledFrom.equals("P")||calledFrom.equals("L"))
		{
			  eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
			  eventdefB.getEventdefDetails();
			  eventName = eventdefB.getName();
//JM: 12Nov2009: #4399
		  	  eventName = (eventName==null)?"":eventName;
		  	  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
		  		eventName=eventName.substring(0,1000);
		  	  }
		}else{
			  eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
			  eventassocB.getEventAssocDetails();
			  eventName = eventassocB.getName();
//JM: 12Nov2009: #4399
		  	  eventName = (eventName==null)?"":eventName;
		  	  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
		  		eventName=eventName.substring(0,1000);
		  	  }

		 }




	if (mode.equals("I"))
	{
 %>

 <form name="kit" id="evtKitFrm" METHOD="POST" action="addeventkit.jsp" onsubmit="if (validate(document.kit)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

	<table >
		<tr>
			<td class=tdDefault><%=LC.L_Storage_Kit%><%--Storage Kit*****--%>:</td>
			<td><Input type="text" name="location" value = "" size="25" MAXLENGTH="250" READONLY="true"></td>
			<td>
				<Input type = "hidden" name="mainFKStorage" value = "" size=25 MAXLENGTH = 250 align = "left">
				<A  href="javascript:void(0);" onClick="return openLocLookup(document.kit)" ><%=LC.L_Select_StorageKit%><%--Select Storage Kit*****--%></A>&nbsp;
				<!--<A href="#" onClick="setValue(document.kit)"> Remove</A> -->
			</td>
		</tr>
	</table>

<Input type = "hidden" name="mode" value = "F" />
<Input type = "hidden" name="eventId" value = "<%=eventId%>" />
<Input type = "hidden" name="calledFrom" value = "<%=calledFrom%>" />
<Input type = "hidden" name="protocolId" value = "<%=protocolId%>" />
<Input type = "hidden" name="dfilter" value = "addeventkit|<%=eventId%>" />


<jsp:include page="propagateEventUpdate.jsp">
<jsp:param name="fromPage" value="<%=fromPage%>"/>
<jsp:param name="formName" value="kit"/>
<jsp:param name="eventName" value="<%=eventName%>"/>
</jsp:include>


<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="N"/>
		<jsp:param name="formID" value="evtKitFrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>




  </form>

  <%

	}
	else //final mode, save data
	{
	  String[] strStorages = null;

	  String mainFKStorage = request.getParameter("mainFKStorage");
	  int returnVal = 0;

	  	String propagateInVisitFlag = request.getParameter("propagateInVisitFlag");
	String propagateInEventFlag = request.getParameter("propagateInEventFlag");


	  if (! StringUtil.isEmpty(mainFKStorage))
	  {
	  		mainFKStorage = "," + mainFKStorage + ",";

	       strStorages = StringUtil.strSplit(mainFKStorage, ",");



    		eventkitB.setFkEvent(EJBUtil.stringToNum(eventId));


    	 	eventkitB.setCreator(creator);
    	 	eventkitB.setIpAdd(ipAdd);

    	 	eventkitB.setEventStorageKit("0"); //since we are passing multiple ids in an array

    	 	returnVal = eventkitB.setEventKitDetails(strStorages);

    	 	if ((propagateInVisitFlag == null) || (propagateInVisitFlag.equals("")))
			propagateInVisitFlag = "N";

	     	if ((propagateInEventFlag == null) || (propagateInEventFlag.equals("")))
			propagateInEventFlag = "N";

			if ( (propagateInVisitFlag.equals("Y")) || (propagateInEventFlag.equals("Y"))) {

				eventdefB.propagateEventUpdates(EJBUtil.stringToNum(protocolId), EJBUtil.stringToNum(eventId), "SCH_EVENT_KIT", 0, propagateInVisitFlag, propagateInEventFlag, "N", calledFrom);
			}


	  }

	  if (returnVal == 0)
	  { %>

	  	<br><br><br><br><br><p class = "sectionHeadings" align = center><%=MC.M_Data_SvdSucc%><%--Data was saved successfully.***** --%></p>

	  <%
	  }
	  else
	  {
	   %>
	   <br><br><br><br><br><p class = "sectionHeadings" align = center><%=MC.M_DataNotSvd_Succ%><%--Data was not saved successfully.*****--%></p>
	   <%
	  }

	  %>
	  		<script>

				  window.opener.location.reload();

				  setTimeout("self.close()",1000);

				 </script>


	  <%
	}

}//end of if body for session

else
{



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%



}



%>


</div>
</body>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</html>
