<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import="com.velos.eres.service.util.*,java.util.*"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings"%>


<%!
  private String getHrefBySubtype(String subtype) {
      if (subtype == null) { return "#"; }

	  if ("1".equals(subtype)) {
          return "allPatient.jsp?searchFrom=initial&selectedTab=1&srcmenu=tdmenubaritem5";
      }
	  if ("2".equals(subtype)) {
          return "studypatients.jsp?srcmenu=tdmenubaritem5&selectedTab=2&studyId=&patid=&patstatus=&openMode=F";
      }
	  if ("3".equals(subtype)) {
          return "allSchedules.jsp?srcmenu=tdmenubaritem5&selectedTab=3&studyId=&patid=&patstatus=&openMode=F";
      }
	  
      return "#";
  }
%>

<%
  String selclass;
  String studyId="";	
  String study="";	
  String tab= request.getParameter("selectedTab");
  HttpSession tSession = request.getSession(true);
  int protocolManagementRightInt = 0;
  String acc = (String) tSession.getValue("accountId");
   
  if (sessionmaint.isValidSession(tSession))
  {
  
  	String protocolManagementRight = (String) tSession.getValue("protocolManagementRight");
	protocolManagementRightInt = EJBUtil.stringToNum(protocolManagementRight);
	
	//study = (String) tSession.getValue("studyId");
	//if (!study.equals("")) mode="M";

 }
 
	ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
	ArrayList tabList = objCache.getAccountObjects(EJBUtil.stringToNum(acc), "mgpat_tab");
%>
<!-- <table  class= "selectedTab" cellspacing="0" cellpadding="0" border="0">   --> 
<table  cellspacing="0" cellpadding="0" border="0">
	<tr>
	
	<%
			for (int iX=0; iX<tabList.size(); iX++) {
            ObjectSettings settings = (ObjectSettings)tabList.get(iX);

    		if ("0".equals(settings.getObjVisible())) {
    		    continue;
    		}

            boolean showThisTab = false;
    		if ("1".equals(settings.getObjSubType())) {
    		     showThisTab = true; 
    		} 
			else if ("2".equals(settings.getObjSubType())) {
				if (protocolManagementRightInt > 0) {
					showThisTab = true; 
				}
			}else if ("3".equals(settings.getObjSubType())) {
				if (protocolManagementRightInt > 0) {
					showThisTab = true; 
				}
			}
			else {
    		    showThisTab = true;
    		}

            if (!showThisTab) { continue; } 

		
			if (tab == null) { 
                selclass = "unselectedTab";
    		} else if (tab.equals(settings.getObjSubType())) {
                selclass = "selectedTab";
            } else {
                selclass = "unselectedTab";
            }
	 %>

			<td  valign="TOP">

			<table class="<%=selclass%>"  cellspacing="0" cellpadding="0"  border="0">
			<tr>
		<!-- 	<td rowspan="3" valign="top" wclassth="7">
			<img src="../images/leftgreytab.gif" wclassth=8 height=20 border=0 alt=""></td> <!--  -->
			<td>
				<a href="<%=getHrefBySubtype(settings.getObjSubType())%>"><%=settings.getObjDispTxt()%>
				</a>
			</td> 
		<!-- 	<td rowspan="3" valign="top" wclassth="7">
				<img src="../images/rightgreytab.gif" wclassth=7 height=20 border=0 alt="">
			</td> -->
			</tr> 
			</table> 
			</td> 
	
	 <%
        } // End of tabList loop
      %>



	

<%--	<td  valign="TOP">
	<% if (tab.equals("3"))
    	{
     		selclass= "selectedTab";
	    } 
	    else
	    {
			selclass = "unselectedTab";
		}%>
	<table class="<%=selclass%>"  cellspacing="0" cellpadding="0" border="0">
	<tr>
   	<td rowspan="3" valign="top" wclassth="7">
	<img src="../images/leftgreytab.gif" wclassth=8 height=20 border=0 alt=""></td> 
	<td>
		<a href="#">Screened
		</a>
	</td> 
    <td rowspan="3" valign="top" wclassth="7">
		<img src="../images/rightgreytab.gif" wclassth=7 height=20 border=0 alt="">
    </td>
   	</tr> 
   	</table> 
    </td>
--%>	

	</tr> 
<!--      <tr>
     <td colspan=5 height=10></td>
     </tr>  --> 
     </table>
<table class="tabBorder" width="99%" height="5"><tr><td></td></tr></table>