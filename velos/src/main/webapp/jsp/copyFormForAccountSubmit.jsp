<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<!--Bug#9911 24-May-2012 -Sudhir-->
<jsp:include page="include.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<BODY>
<jsp:useBean id="linkedFormsJB" scope="request"  class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import ="com.velos.eres.business.common.*,java.util.*, com.velos.eres.service.util.*" %>
<%
 int iret = -1;
 int iupret = 0;
 int iformLibId=0;
 String mode =request.getParameter("mode");
 String eSign = request.getParameter("eSign");
 String formLibId =request.getParameter("formLibId");
 String studyId =request.getParameter("studyId");
 String[] selectedForms = null ;
 selectedForms = request.getParameterValues("selectedForms");
 String src =request.getParameter("srcmenu");
 HttpSession tSession = request.getSession(true); 
 String accId = (String) tSession.getValue("accountId");

 String ipAdd = (String) tSession.getValue("ipAdd");
 String usr = (String) tSession.getValue("userId");

 if (sessionmaint.isValidSession(tSession)) {

%>
	
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%	
String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign)) {

%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} 
else
	{

	String formName=StringUtil.stripScript(request.getParameter("formName"));
	
	formName = formName.trim();
	
	String formType=request.getParameter("newFormType");
		
	//copyFormsForStudy(  String[] idArray, String formType ,String studyId,  String formName ,String creator, String ipAdd)
   
    iret = linkedFormsJB.copyFormsForAccount( selectedForms , formType   , formName , usr ,   ipAdd ) ;
   	iformLibId = iret ;
	
	
	/*out.println("SONIA KKKKKKK"+iformLibId);
	out.println(" sfdgdgdF " +selectedForms[0]);
	out.println(" sfdgdgdF " +selectedForms[1]);
	out.println(" sfdgdgd 12--"+ formType);
	out.println(" sfdgdgd 13--"+ usr);  */ 
	
	
	if ((iupret == -2) || (iret == -3) || (iret == 0) ) {
		 if (iret == -3 )
			{%>
			<br><br><br><br><br>
			<p class = "successfulmsg" align = center > <%=MC.M_UnqFrmName_EtrUnqFrmName%><%--The unique form name already exists. Please enter a different unique form name*****--%></p>
			<center><button type="button" tabindex = 2 onClick="history.go(-1);"><%=LC.L_Back %></button>
		<%
		} else {
		%> 
		<br><br><br><br><br>
			<p class = "successfulmsg" align = center> <%=MC.M_Data_NotSvdSucc%><%--Data not saved successfully*****--%> </p>
			<button type="button" onclick="window.history.back();"><%=LC.L_Back%></button></p>
		<% return;
		} 
	}
	else {%>
		<br><br><br><br><br><p class = "successfulmsg" align = center>
		<%=MC.M_Data_SavedSucc%><%--Data saved successfully*****--%></p>	
		<script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
		</script>			
				
	<%}
	}
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
<jsp:include page="bottompanel.jsp" flush="true"/>
</BODY>

</HTML>

