<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>

<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252">

<TITLE><%=LC.L_Benefits %><%-- Benefits*****--%></TITLE>
<%@ page import="com.velos.eres.service.util.*"%>

</HEAD>

<SCRIPT>

	function openwin() {

      window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")

;}

</SCRIPT>



<BODY style = "overflow:auto">



<Link Rel=STYLESHEET HREF="common.css" type=text/css>



<DIV class="staticDefault">


<br><br>
<table width="80%" cellspacing="0" cellpadding="0" border=0 align=center>

<tr>

<td width="10%">

</td>

<td width="60%">

<img src="../images/resorg.gif" width="300" height="200" align="left"></img>

</tr>

</table>

<DIV style="margin-left:500px;margin-top:-50px;position:absolute">

	<table  cellspacing="0" cellpadding="0" border=0 align=center>

	<tr>

	<td id="researchHeadings" align="center"><%=MC.M_HowdoesMy_ResOrg %><%-- How does my research organization*****--%>

	</td>

	</tr>

	<tr>

	<td height="5">

	</td>

	</tr>

	<tr>

	<td id="researchHeadings" align="center"> <%=LC.L_Benefit %><%-- benefit?*****--%>

	</td>

	</tr>

	</table>

</DIV>

&nbsp;

&nbsp;



<table width="80%" cellspacing="0" cellpadding="0" border=0 align=center>

<tr>

<td height="30">

</td>

</tr>

<tr>

<td><%=MC.M_VelosNewTech_ClinicTrails %><%-- With increasing emphasis being placed on completing a clinical trial efficiently and on time, research organizations are constantly on the lookout for newer technologies that will help them streamline processes and coordinate activities. However, the advancing use of technology in the research industry has succeeded in creating a difficult situation with investigators having to use several different products for different trials and as a result, desperately needing a centralized point of access to all of their research-based information. Velos eResearch provides

focal point.*****--%>

</td>

</tr>

</table>



<table width="80%" cellspacing="0" cellpadding="0" border=0 align=center>

<tr>

<td height="20">

</td>

</tr>

<tr>

<td width="60%">

<%=MC.M_NewDrugDevlop_Take10Years %><%-- For new drug development, the cycle from the laboratory to the market takes an average of 10 years and*****--%>

</td>

<td id="researchHeadings" width="40%" align="center">

"<%=LC.L_Time_IsMoney %><%-- Time is Money*****--%>"

<td rowspan=3> <img src="../images/resmoney.gif" width="150" height="200" align="right"></img>

</td>

</tr>

<tr>

<td colspan="2">

<%=MC.M_ProductDelay_BringLoss %><%-- costs a pharmaceutical company an estimated $500 million. Each day of delay in bringing a pharmaceutical product to market results in an average loss of approximately $1 million in revenues. At Velos, we believe the opportunity for radical improvement lies in supplying the infrastructure that supports the investigator activities that lead up to data submission.  In doing so, Velos eResearch provides the opportunity to*****--%>:

<UL>

<UL>

<P ALIGN="JUSTIFY"><LI><%=MC.M_ReduceCost_TrialExec %><%-- Reduce cost through more efficient trial execution.*****--%> </LI></P>

<P ALIGN="JUSTIFY"><LI><%=MC.M_IncRevCap_ResVol %><%-- Increase revenue capture and research volume.*****--%></LI></P>

<P ALIGN="JUSTIFY"><LI><%=MC.M_ImprovePosIn_ResChain %><%-- Improve position in the research value chain.*****--%> </LI></P></UL>

</UL>

<%=MC.M_Together_CreateResOrg %><%-- Together, these capabilities can create enormous value for any research program or organization.*****--%>

</table>



<table width="80%" cellspacing="0" cellpadding="0" border=0 align=center>

<tr>

<td height="20">

</td>

</tr>

<tr>

<td id="researchHeadings" width="40%" align="center">

<%=MC.M_WhatVelosEres_Offer %><%-- What does Velos eResearch Offer?*****--%>

</td>



<td width="60%">

<%=MC.M_VeResApp_CharsAdva %><%-- Velos eResearch is an application that aids in knowledge and process management and leads to improved research quality, process efficiency and increased rate of successfully completed trials.*****--%>

</td>

</tr>

<tr>

<td colspan="2">

<%=MC.M_Org_OffersBenifit %><%-- To an organization actively involved in research, it offers the following product features and benefits*****--%>:

</td>

</tr>

<tr>

<td height="20">

</td>

</tr>

<tr>

</table>



<TABLE BORDER =1 CELLSPACING=1 CELLPADDING=7 WIDTH=80% align="center">

<TR><TD WIDTH="50%" VALIGN="TOP" BGCOLOR="#ffffff">

<P ALIGN="CENTER"> </p>

<b><%=LC.L_Features %><%-- Features*****--%></b>

<P ALIGN="CENTER"> </p>

<b> <%=LC.L_Centralization %><%-- Centralization*****--%> </B>

<UL>
<LI><%=MC.M_SnglAcc_ForStdDataResNews %><%-- Single access personalized account for <%=LC.Std_Study_Lower%> data and research news*****--%></LI>
</UL>



<B><%=LC.L_Content %><%-- Content*****--%></B>

<UL><LI><%=LC.L_Std_SummaryListing %><%-- <%=LC.Std_Study%> summary listing*****--%></LI>

<LI><%=MC.M_AcesCtrl_DetStdPcol %><%-- Access controlled detailed <%=LC.Std_Study_Lower%> protocol*****--%></LI>

<LI><%=LC.L_Auto_StdMgmt %><%-- Automated <%=LC.Std_Study_Lower%> management*****--%></LI>

<LI><%=MC.M_Personalized_FavLnk %><%-- Personalized list of favorite links*****--%></LI></UL>





<B><%=LC.L_Connectivity %><%-- Connectivity*****--%>
</B>

<UL><LI><%=LC.L_Network_OfResearchers %><%-- Network of researchers*****--%></LI>

<LI><%=MC.M_EfctInvst_RectProc %><%-- Efficient investigator recruitment process*****--%></LI>

<LI><%=MC.M_CommStd_PcolTeam %><%-- Communication with <%=LC.Std_Study_Lower%> protocol creator and team members*****--%></LI></UL>






<B><P>&nbsp;</B></TD>

<TD WIDTH="50%" VALIGN="TOP" BGCOLOR="#ffffff">

<P ALIGN="CENTER"></P>

<b><%=LC.L_Benefits %><%-- Benefits*****--%></b>

<P ALIGN="CENTER"></P>


<b><%=LC.L_Quality %><%-- Quality*****--%> </B>

<UL>

<LI><%=LC.L_Increased_PcolCompliance %><%-- Increased protocol compliance*****--%></LI>

<LI><%=LC.L_Consistent_PcolExec %><%-- Consistent protocol execution*****--%></LI>

<LI><%=MC.M_MonitoringStd_AcrossOrg %><%-- Real time monitoring of <%=LC.Std_Study_Lower%> across organizations*****--%></LI></UL>



<B><%=LC.L_Time %><%-- Time*****--%></B>

<UL>

<LI><%=MC.M_Quick_StartTrial %><%-- Quick start to trial*****--%></LI>

<LI><%=MC.M_DistOf_PcolAndAmendments %><%-- Quick distribution of protocol and amendments*****--%></LI>

<LI><%=LC.L_Auto_StdMgmt %><%-- Automated <%=LC.Std_Study_Lower%> management*****--%></LI>

<LI><%=MC.M_CentralAces_ToData %><%-- Centralized access to data*****--%></LI>

<LI><%=MC.M_Efficient_SecureComm %><%-- Efficient and secure communication*****--%></LI></UL>


<B><%=LC.L_Revenue%><%-- Revenue*****--%></B>


<UL>
<LI><%=MC.M_AcesTo_MoreTrials %><%-- Access to more trials*****--%></LI>
<LI><%=MC.M_Ablty_CdtCoord_Trials %><%-- Ability to conduct and coordinate more trials simultaneously*****--%></LI>
<LI><%=MC.M_ImprPos_ResChain %><%-- Improves position in the research value chain*****--%></LI></UL>

<B></B></FONT></TD>

</TR>

</TABLE>





<table width="80%" cellspacing="0" cellpadding="0" border=0 align=center>

<tr>

<td height="20">

</td>

</tr>

<tr>

<td width="35%">

</td>

<td>

<%=MC.M_ToCreate_Acc %><%-- To create an account*****--%> <A href="register.jsp"><font size = 2><%=LC.L_Reg_Now %><%-- Register Now*****--%></font></A>,

</td>

</tr>

<td colspan="2" align="center">

<%=LC.L_Or %><%-- Or*****--%> <A href="contactus.jsp" target="Information" onClick="openwin()"> <font size = 2><%=LC.L_Contact_Us %><%-- Contact Us*****--%></font></A> <%=MC.M_MoreServInfo_ProdServ %><%-- for more information regarding our product and services*****--%>

</td>

</tr>

</table>

<div>

<jsp:include page="bottompanel.jsp" flush="true">

</jsp:include>

</div>



</BODY>



</DIV>



</HTML>

