<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title> <%=LC.L_Budget_Appendix%><%--Budget >> Appendix*****--%> </title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@ page import="com.velos.eres.service.util.*" %>

 
<SCRIPT language="javascript">

function confirmBox(fileName,pgRight) {
	if (f_check_perm(pgRight,'E') == true) {
		var paramArray = [fileName];
		if (confirm(getLocalizedMessageString("L_Del_FrmBgt",paramArray))) {/*if (confirm("Delete " + fileName + " from Budget?")) {*****/
		    return true;}
		else
		{
			return false;
		}
	} else {
		return false;
	}
}


function fdownload(formobj,pk,filename,dnldurl)
{
	formobj.file.value = filename;
	formobj.pkValue.value = pk ;
	formobj.dnldurl.value=dnldurl;
	formobj.moduleName.value="Budget";
	formobj.action="postFileDownload.jsp";
	//formobj.action=dnldurl;
	
	formobj.target = "_filedownload";
	formobj.method = "POST";
	formobj.submit();
}

</SCRIPT> 

</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.Configuration,com.velos.esch.service.util.EJBUtil,com.velos.esch.business.common.BgtApndxDao,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.business.common.BudgetDao"%>
<jsp:useBean id="bgtApndxB" scope="request" class="com.velos.esch.web.bgtApndx.BgtApndxJB"/>
<jsp:useBean id="budgetB" scope="page" class="com.velos.esch.web.budget.BudgetJB"/>

<% String src;
src= request.getParameter("srcmenu");
String budgetTemplate = request.getParameter("budgetTemplate");
String mode = request.getParameter("mode");
int studyId= EJBUtil.stringToNum(request.getParameter("studyId"));
String includedIn = StringUtil.trueValue(request.getParameter("includedIn"));
String 	bottomdivClass="tabDefBotN";
if ("S".equals(budgetTemplate)) { %>
	<jsp:include page="panel.jsp" flush="true"> 
	<jsp:param name="src" value="<%=src%>"/>
	</jsp:include>   
<% } else { 
	bottomdivClass="popDefault";
	%>
    <jsp:include page="include.jsp" flush="true"/> 
<% } %>

<body>
<% 
	if ("S".equals(budgetTemplate)) { 
%>
<DIV class="tabDefTopN" id="div1">
<jsp:include page="budgettabs.jsp" flush="true">
<jsp:param name="budgetTemplate" value="<%=budgetTemplate%>"/>
<jsp:param name="mode" value="<%=mode%>"/>
<jsp:param name="studyId" value="<%=studyId%>"/>
</jsp:include>
</DIV>
<%  
    }
	int pageRight = 0;
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{
 		String budgetId=request.getParameter("budgetId");
		int iBugtId= EJBUtil.stringToNum(budgetId);
		if(iBugtId==0 ) {
		  
		%>
			<jsp:include page="budgetDoesNotExist.jsp" flush="true"/>
		<%

		   } else { 
			
			String fromRt = request.getParameter("fromRt");
			if (fromRt==null || fromRt.equals("null")) fromRt="";
			%>
			<%
			if (fromRt.equals("")){
				GrpRightsJB bRights = (GrpRightsJB) tSession.getAttribute("BRights");		
				pageRight = Integer.parseInt(bRights.getFtrRightsByValue("BGTAPNDX"));
			} else{
				pageRight = EJBUtil.stringToNum(fromRt);
			}
%>			 
	<Input type="hidden" name="fromRt" value="<%=pageRight%>"/>

	<!--Marked as "overflow" for the bug id 24107-->
	<DIV class="<%=bottomdivClass%>" id="div2" style="overflow:auto">
<%
		if (pageRight > 0 )
		{
	
			
			String bgtStatDesc ="";
			
			String space = request.getParameter("outOfSpace");
			
			String selectedTab=request.getParameter("selectedTab");

			
			BudgetDao budgetDao = budgetB.getBudgetInfo(EJBUtil.stringToNum(budgetId));
			ArrayList bgtNames = budgetDao.getBgtNames(); 
			ArrayList bgtStudyNumbers = budgetDao.getBgtStudyNumbers();
			ArrayList bgtSites = budgetDao.getBgtSites();
			ArrayList bgtStats = budgetDao.getBgtStats();
	        String bgtName = (String) bgtNames.get(0);
         	String bgtStudyNumber = (String) bgtStudyNumbers.get(0);
        	String bgtSiteName = (String) bgtSites.get(0);
            String bgtStat = (String) bgtStats.get(0);
            //Ashu:Added for :BUG#5362
            bgtStatDesc = (String) budgetDao.getBgtCodeListStatusesDescs().get(0);
            
			bgtStudyNumber = (bgtStudyNumber == null || bgtStudyNumber.equals(""))?"-":bgtStudyNumber;
			bgtSiteName = (bgtSiteName == null || bgtSiteName.equals(""))?"-":bgtSiteName;			
			bgtName = (bgtName == null || bgtName.equals(""))?"-":bgtName;
			bgtName = StringUtil.stripScript(bgtName);
			space = (space==null)?"":space.toString();
			String incorrectFile = request.getParameter("incorrectFile");
			incorrectFile = (incorrectFile==null)?"":incorrectFile.toString();			
			%>
						<BR>
<table width="98%" cellspacing="0" cellpadding="0">
	<tr>
		<td>
	    <table width="75%" cellspacing="0" cellpadding="0">
	    <tr> 
			<td class=tdDefault width="50%"><B> <%=LC.L_Budget_Name%><%--Budget Name*****--%>:</B>&nbsp;&nbsp;<%=bgtName%></td>
		</tr>
	    <tr> 
			<td class=tdDefault width="50%"><B> <%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number--%>:</B>&nbsp;&nbsp;<%=bgtStudyNumber%></td>
		</tr>			
	    <tr> 
	        <td class=tdDefault width="50%"><B> <%=LC.L_Organization%><%--Organization*****--%>:</B>&nbsp;&nbsp;<%=bgtSiteName%></td>
		</tr>										
		</table> 
		<%if ((!(bgtStat ==null)) && (bgtStat.equals("F") || bgtStat.equals("T"))) { Object[] arguments = {bgtStatDesc}; %>
	<P class = "defComments"><FONT class="Mandatory"><%=VelosResourceBundle.getMessageString("M_BgtStat_CntChgBgt",arguments)%><%--Budget Status is '<%=bgtStatDesc%>'. You cannot make any changes to the budget.*****--%></Font></P>
		<%}%>

			<%
			if(space.equals("1")) {
			%>
				<br><br><br><br>
				<table width=100%>
				<tr><td align=center>
				<p class = "sectionHeadings">

				<%=MC.M_UploadSpace_ContAdmin%><%--The space allotted to you for file uploading has exhausted. Please contact Velos Administrator.*****--%> 

				</p>
				</td>
				</tr>

				<tr height=20></tr>
				<tr>
				<td align=center>
			<button onClick="window.history.go(-2);"><%=LC.L_Back%></button>
			</td>		
			</tr>		
		</table>		
<%		
				}else if(incorrectFile.equals("1")) {
%>
<br><br><br><br>
<table width=100%>
<tr>
<td align=center>

<p class = "sectionHeadings">

<%=MC.M_FileUpldEmpty_IncorrPath%><%--The file that you are trying to upload is either empty or the path specified is incorrect.*****--%>  

</p>
</td>
</tr>

<tr height=20></tr>
<tr>
<td align=center>

		<button onClick="window.history.go(-1);"><%=LC.L_Back%></button>
</td>		
</tr>		
</table>		


<%		
	} 
				else 
				{ //else of if for space.equals("1")

		
		   	int count = 0;
		   	ArrayList bgtApndxIds=null;
			ArrayList bgtApndxDescs=null; 
			ArrayList bgtApndxUris=null; 
			ArrayList bgtApndxTypes=null;
		    int bgtApndxId=0;
     	    String bgtApndxDesc="";
    	    String bgtApndxUri="";	
   	       String bgtApndxType = "";			
    %>
	<form METHOD=POST action="" name="budget">
<%
if (pageRight == 4) {
%>
   <P class = "defComments"><FONT class="Mandatory"><%=MC.M_YouOnly_ViewPerm%><%--You have only View permission*****--%></Font></P>
<%}

    BgtApndxDao bgtApndxDaoFile = bgtApndxB.getBgtApndxFiles(EJBUtil.stringToNum(budgetId));
		
	bgtApndxIds = bgtApndxDaoFile.getBgtApndxIds();
	bgtApndxDescs = bgtApndxDaoFile.getBgtApndxDescs();
	bgtApndxUris = bgtApndxDaoFile.getBgtApndxUris();
	bgtApndxTypes = bgtApndxDaoFile.getBgtApndxTypes();
%>
<!-- <P class = "defComments"><%=MC.M_FlwWebPgDocu_LnkThisBgt%><%--The following web pages and documents are linked to this budget*****--%>:<br> </P>

<TABLE width="90%">
<tr>
<th align = left><%=LC.L_My_Links%><%--My Links*****--%></th>
</tr>
<tr>
<td>
<P class = "defComments"><%=MC.M_ListimpLink_DelAppxWin%><%--You can list your important links. Give full path of your URLs.
<br><br> Select [Edit] against the name of a link to edit its details. Click on the name of the link to open it in a new browser window.*****--%></P>
</td>
</tr>
</table> -->
<TABLE WIDTH="100%">  
<TR>

<TD WIDTH="50%" valign="top">
 <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midalign" >
      <tr> 
        <th width="100%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);" > <%=LC.L_My_Files%><%-- My Files*****--%> </th>
      </tr>
     
      <tr> 
        <td> 
 <%if(! ( bgtStat.equals("F") || bgtStat.equals("T") )) {%>	
	<A href="budgetfile.jsp?bgtId=<%=budgetId%>&mode=N&selectedTab=<%=selectedTab%>&srcmenu=<%=src%>&budgetTemplate=<%=budgetTemplate%>&budgetMode=<%=mode%>&fromRt=<%=fromRt%>" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=MC.M_ClickHere_UploadNewFile%><%--Upload Document*****--%></A>
<%}%>
        </td>
      </tr>
    </table>
    <div style="width:99%; max-height:240px; overflow:auto">
	<TABLE cellspacing="0" cellpadding="0" class="basetbl outline midalign">
	<tr>
		<th width="30%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_File_Name%><%-- File name*****--%></th>
		<th width="60%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Description%><%-- Description*****--%></th>
		<th width="5%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Edit%><%-- Edit*****--%></th>
		<th width="5%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Delete%><%-- Delete*****--%></th>
	</tr>   
<%
	String dnld;
	com.aithent.file.uploadDownload.Configuration.readSettings("eres");
	com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "milestone");
	dnld=com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET ;
	String modDnld = "";
 
   for(int i=0;i<bgtApndxIds.size(); i++) {
	bgtApndxId= ((Integer)bgtApndxIds.get(i)).intValue();
	bgtApndxDesc= (String)bgtApndxDescs.get(i); 
	bgtApndxUri = (String)bgtApndxUris.get(i);
	
	modDnld = dnld + "?file=" + StringUtil.encodeString(bgtApndxUri) ;	
	 
	if ((i%2)!=0) {
  %>
      <tr id="browserEvenRow"> 
        <%
		}
		else{
  %>
      <tr id="browserOddRow"> 
        <%
		}
  %>
	<% %>
	<td width="30%">
	<span style="overflow: hidden; width: 100px; word-wrap: break-word; display: block;">
	<A style="color:#3B9EC6" href="#" onClick="fdownload(document.budget,<%=bgtApndxId%>,'<%=StringUtil.encodeString(bgtApndxUri)%>','<%=dnld%>');return false;" >
	 
	 
	<%=bgtApndxUri %></A>
	</span>
	 </td>
	<td width="60%">
	<span style="overflow: hidden; width: 230px; word-wrap: break-word; display: block;">
	 <%=bgtApndxDesc%> 
	 </span>
	</td>
	
	<td width=5%>
<%if(!(bgtStat.equals("F") || bgtStat.equals("T")) ) {%>	  
	<A HREF="budgetfile.jsp?bgtId=<%=budgetId%>&mode=M&apndxId=<%=bgtApndxId%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&budgetTemplate=<%=budgetTemplate%>&budgetMode=<%=mode%>&fromRt=<%=fromRt%>" onClick="return f_check_perm(<%=pageRight%>,'E')"><img border="0" title="<%=LC.L_Edit%>" alt="<%=LC.L_Edit%>" src="./images/edit.gif" ><%//=LC.L_Edit%><%--Edit*****--%></A>
<%}%>	
	</td>
	<td width=5%>
<%if(!(bgtStat.equals("F") || bgtStat.equals("T")) ) {%>
	<A HREF="budgetapndxdelete.jsp?budgetApndxId=<%=bgtApndxId%>&budgetId=<%=budgetId%>&delMode=null&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&budgetTemplate=<%=budgetTemplate%>&budgetMode=<%=mode%>&fromRt=<%=fromRt%>" onClick="return confirmBox('<%=bgtApndxUri%>',<%=pageRight%>)"><img title="<%=LC.L_Delete%>" src="./images/delete.gif" border="0" align="left"/></A>
<%}%>	
	</td>
	
   </tr>
<%
   }
    BgtApndxDao bgtApndxDao = bgtApndxB.getBgtApndxUrls(EJBUtil.stringToNum(budgetId));
		
	bgtApndxIds = bgtApndxDao.getBgtApndxIds();
	bgtApndxDescs = bgtApndxDao.getBgtApndxDescs();
	bgtApndxUris = bgtApndxDao.getBgtApndxUris();
	bgtApndxTypes = bgtApndxDao.getBgtApndxTypes();
%>
</TABLE>
</div>
</TD>
<TD WIDTH="50%" valign="top">
<table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midalign" >
     <!-- <tr > 
        <td width = "100%"> 
          <P class = "defComments"><%=MC.M_PcolAppx_AsFollows%><%-- Protocol Appendix is as follows*****--%>:</P>
        </td>
      </tr>-->
      <tr> 
        <th width="100%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"> <%=LC.L_My_Links%><%-- My Links*****--%> </th>
      </tr>
      
      <tr> 
        <td> 
		    
	 <%if(!(bgtStat.equals("F") || bgtStat.equals("T"))) {%>	      
	 <A href="addbudgeturl.jsp?budgetId=<%=budgetId%>&mode=N&srcmenu=<%=src%>&budgetApndxId=&selectedTab=<%=selectedTab%>&budgetTemplate=<%=budgetTemplate%>&budgetMode=<%=mode%>&fromRt=<%=fromRt%>" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=MC.M_ClickHere_AddNewLink%><%--Add New URL*****--%></A>
<%}%>	  
        </td>
      </tr>
    </table>
     <div style="width:99%; max-height:240px; overflow:auto">
	<TABLE width=100%;  cellspacing="0" cellpadding="0" table-layout="fixed"class="basetbl outline midalign">
	<tr>
		<th width="35%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Url_Upper%><%-- URL*****--%></th>
		<th width="35%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Description%><%-- Description*****--%></th>
		<th width="15%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Edit%><%-- Edit*****--%></th>
		<th width="15%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Delete%><%-- Delete*****--%></th>
   </tr>
<% 
   for(int i=0;i<bgtApndxIds.size(); i++) {

	bgtApndxId= ((Integer)bgtApndxIds.get(i)).intValue();
	bgtApndxDesc= (String)bgtApndxDescs.get(i); 
	bgtApndxUri = (String)bgtApndxUris.get(i); 
	if ((i%2)!=0) {
  %>
      <tr id="browserEvenRow"> 
        <%
		}
		else{
  %>
      <tr id="browserOddRow"> 
        <%
		}
  %>
	<td width=35%>
	<span style="overflow: hidden; width: 100px; word-wrap: break-word; display: block;">
	<A style="color:#3B9EC6"  href=<%=bgtApndxUri%> target="_blank"><%=bgtApndxUri%></A>
	</span>
	</td>
	
	<td width=35%>
	<span style="overflow: hidden; width: 230px; word-wrap: break-word; display: block;">
	 <%=bgtApndxDesc%>
	 </span>
	  </td>
	<td width=15%>
<%if(!(bgtStat.equals("F") || bgtStat.equals("T") )) {%>	  	
	<A HREF="addbudgeturl.jsp?budgetId=<%=budgetId%>&mode=M&srcmenu=<%=src%>&budgetApndxId=<%=bgtApndxId%>&selectedTab=<%=selectedTab%>&budgetTemplate=<%=budgetTemplate%>&budgetMode=<%=mode%>&fromRt=<%=fromRt%>" onClick="return f_check_perm(<%=pageRight%>,'E')"><img border="0" title="<%=LC.L_Edit%>" alt="<%=LC.L_Edit%>" src="./images/edit.gif" ><%//=LC.L_Edit%><%--Edit*****--%></A>
<%}%>	
	 </td>
	<td width=15%>
<%if(! (bgtStat.equals("F") || bgtStat.equals("T") )) {%>	  
	<A HREF="budgetapndxdelete.jsp?budgetApndxId=<%=bgtApndxId%>&budgetId=<%=budgetId%>&delMode=null&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&budgetTemplate=<%=budgetTemplate%>&budgetMode=<%=mode%>&fromRt=<%=fromRt%>" onClick="return confirmBox('<%=bgtApndxUri%>',<%=pageRight%>)"><img title="<%=LC.L_Delete%>" src="./images/delete.gif" border="0" align="left"/></A>
<%}%>	
	</td>
   </tr>
<%
   }
%>		
		
</TABLE>
</div>
</TD>
</TR>
</TABLE>
<br>
<%
  if(budgetTemplate.equals("P") || budgetTemplate.equals("C") ){
%>
  <table width="100%" cellspacing="0" cellpadding="0">
  <tr>
      <td align=center>
	  	<button onClick="javascript:window.self.close();"><%=LC.L_Close%></button>
      </td>
  </tr>
  </table>
<%
  }
%>

 <!--  following fields are required for file download -->

	<input type="hidden" name="tableName" value="sch_bgtapndx">
    <input type="hidden" name="columnName" value="BGTAPNDX_FILEOBJ">
    <input type="hidden" name="pkColumnName" value="PK_BGTAPNDX">
    <input type="hidden" name="module" value="budget">
    <input type="hidden" name="db" value="sch">
    <input type="hidden" name="pkValue" value="">
    <input type="hidden" name="file" value="">
    <input type="hidden" name="dnldurl" value="">
    <input type="hidden" name="moduleName" value="">

</form>

<%
}// end of if for space

} //end of if body for page right


else
 {
	%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%
		
 } //end of else body for page right  

}//if check for budget exist
} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>
<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>


