<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Copy_Bgt%><%--Copy Budget*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT Language="javascript">

 function  validate(formobj){
    if (!(validate_col('budget Name',formobj.budname))) return false
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	<%-- if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
     } --%>
   }

</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="budget" scope="request" class="com.velos.esch.web.budget.BudgetJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.business.common.*"%>


<% String src;

  src= request.getParameter("srcmenu");

  int ienet = 2;
  int proceed = 1;
  int ret = 0;
  String agent1 ;
  agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1) 
     ienet = 0; //IE
    else
  ienet = 1;


%>

<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   


<body>
<br>
<DIV class="tabDefBotN" id="div2">
  <%
  HttpSession tSession = request.getSession(true); 
    

	if (sessionmaint.isValidSession(tSession))
	{
		String budgetName ="";
		String budgetVer = "";
    	String budgetId =request.getParameter("budgetId");
		String sAcc= (String) tSession.getValue("accountId");
		int bgtId=EJBUtil.stringToNum(budgetId);		
		budget.setBudgetId(bgtId);
		budget.getBudgetDetails();
		budgetName=budget.getBudgetName();
		budgetName = StringUtil.stripScript(budgetName);
		String from = request.getParameter("from");
// 		String calledFrom = request.getParameter("calledFrom");
        String ipAdd = (String) tSession.getValue("ipAdd");
//		calledFrom = "s";
    	     	
        int length = 0;

		String uName = (String) tSession.getValue("userName");
		String uId = (String) tSession.getValue("userId");
		int userId = EJBUtil.stringToNum(uId);
		Object[] arguments = {"<FONT class='Mandatory'>"+budgetName+"</FONT>"};
%>
		<P class = "userName"><%= uName %></p>
<Form name="copybudget" id="cpybgdtfrm" method="post" action="updatecopybudget.jsp" onsubmit="if (validate(document.copybudget)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >		
<P class="sectionHeadingsFrm" align="center"> <b> <%=VelosResourceBundle.getMessageString("M_BgtWill_BeCopied",arguments)%><%--The Budget <FONT class="Mandatory"> '<%=budgetName%>' </FONT> will be copied.*****--%> </b></P>

<Form name="select" method="post" action="copybudget.jsp?from=final&srcmenu=<%=src%>" onsubmit="">
<input type="hidden" name="budsel" value="<%=budgetId%>" >
	<input type="hidden" name="srcmenu" value="<%=src%>" >
<table width=98%>
	<tr>
	<td width="50%" align="right"> <%=MC.M_EtrName_ForNewBgt%><%--Please enter a name for the new Budget*****--%> <FONT class="Mandatory">* </FONT>  &nbsp:&nbsp;</td>
	<td> <input type="text" name="budname"> </td>
	</tr>
	
	<tr>
	<td align="right"> <%=MC.M_EtrVerFor_NewBgt%><%--Please enter a Version Number for the new Budget*****--%>  &nbsp:&nbsp;</td>
	<td> <input type="text" name="budver"> </td>
	</tr>

<P class = "defComments" align="center">
<FONT class="Mandatory">
<%=MC.M_NotAllAppx_FileCopied%><%--Please note that all the appendix files will not be copied if the size of the files exceeds the space allocated for uploading files for your account.*****--%>   
</FONT></P>

</table>
<BR>
	<BR>
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="cpybgdtfrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
</form>

<%
} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>
<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>


