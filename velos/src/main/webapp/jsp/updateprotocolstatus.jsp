<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="alnot" scope="request" class="com.velos.esch.web.alertNotify.AlertNotifyJB"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

  <%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*, com.velos.esch.business.common.SchCodeDao"%>

<%
	HttpSession tSession = request.getSession(true);
	int ret = 0;

	int eventId=0;
	String name ="";
	String description ="";
	String eventType="";
	String duration = "";
	String durationUnit="";
	String status="";
	String mode = "";
	String msg="";
	String userId="";
	String accId="";
	String src = null;
	String calledFrom = "";
	String Notes="";
	String offlineFalg ="";//JM: 23Apr2008, added for, Enh. #C9
	String StatusDate="";
	String ChangedById="";
	int ret2 = 0;

 String eSign = request.getParameter("eSign");

     if (sessionmaint.isValidSession(tSession)) {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>

<%
	 	String oldESign = (String) tSession.getValue("eSign");
		if(!oldESign.equals(eSign)) {
		%>
		  <jsp:include page="incorrectesign.jsp" flush="true"/>
		<%
		} else {
         	String ipAdd = (String) tSession.getValue("ipAdd");
         	String usr = null;
         	usr = (String) tSession.getValue("userId");

         	userId = (String) (tSession.getValue("userId"));
         	accId = (String) (tSession.getValue("accountId"));
         	String studyId = (String) (tSession.getValue("studyId"));


			//JM: 28Apr2008, added for, Enh. #C9
         	String checkedVal = request.getParameter("patSchChk");
         	checkedVal = (checkedVal == null)?"":checkedVal ;

         	String calendarId = request.getParameter("protocolId");


			String dateVal = request.getParameter("dateTxt");
         	dateVal = (dateVal==null)?"":dateVal;

			Date dt = null;
			dt = DateUtil.stringToDate(dateVal,"");
			java.sql.Date dtVal = DateUtil.dateToSqlDate(dt);



         	String oldStat = "";

         	src = request.getParameter("srcmenu");

         	eventType = "P";

         	mode = request.getParameter("mode");
         	mode = (mode==null)?"":mode;
         	calledFrom = request.getParameter("calledFrom");

         	status = request.getParameter("calStatus");
         	
        	// For PCAL-22446 Enhancement By Parminder Singh
        	String patSchUpdateStatus="";
        	if(request.getParameterValues("statusflag")!=null)
        	{
        	String statusflag[]=request.getParameterValues("statusflag");
        	for(int i=0;i<statusflag.length;i++){
        	if(patSchUpdateStatus=="")
        		patSchUpdateStatus+=statusflag[i];
        	else
        		patSchUpdateStatus=patSchUpdateStatus+","+statusflag[i];
        	
        	}
        	}else{
        		patSchUpdateStatus="X";
        	}
         	
         	//JM: 08FEB2011, #D-FIN9
         	SchCodeDao cd = new SchCodeDao();         	
         	int statCode=cd.getCodeId("calStatStd",status);

         	String oldStatus= request.getParameter("prevStat");

         	//JM: 05May2008, added for, Enh. #C9

			String selStat ="";
			if (oldStatus.equals("O")){

				selStat = request.getParameter("patstat");
				selStat = (selStat==null)?"":selStat;


			}


         	eventId = EJBUtil.stringToNum(request.getParameter("protocolId"));
         	name = request.getParameter("protocolName");
         	description = request.getParameter("desc");
         	duration = request.getParameter("durationNum");

         	Notes=request.getParameter("Notes");



         	StatusDate=request.getParameter("StatusDate");
         	ChangedById=request.getParameter("ChangedById");

         	String  calAssoc=request.getParameter("calassoc");
    		calAssoc=(calAssoc==null)?"":calAssoc;

         	eventassocB.setEvent_id(eventId);

         	eventassocB.getEventAssocDetails();

         	
         	//JM: 08FEB2011, #D-FIN9
    		//oldStat =	eventassocB.getStatus();	
    		SchCodeDao cd1 = new SchCodeDao();		
    		oldStat = cd1.getCodeSubtype(EJBUtil.stringToNum(eventassocB.getStatCode()));
    		

         	eventassocB.setName(name);
         	eventassocB.setDescription(description);
         	eventassocB.setEvent_type(eventType);
         	eventassocB.setUser_id(accId);
         	
         	//JM: 01FEB2011: Enh-#D-FIN9         	
         	eventassocB.setStatCode(""+statCode);
         	
         	eventassocB.setDuration(duration);
         	eventassocB.setEvent_id(eventId);
         	eventassocB.setModifiedBy(usr);
         	eventassocB.setIpAdd(ipAdd);
         	eventassocB.setStatusDate(StatusDate);
         	eventassocB.setStatusUser(ChangedById);
         	eventassocB.setNotes(Notes);
         	eventassocB.setCost("11");
         	 if (mode.equals("N"))
  		  		 {
        	eventassocB.setCreator(userId);/*Bug# 10139 : Date 6th July 2012 : Yogendra Pratap */
  		 		 }
         	eventassocB.setPatSchUpdate(patSchUpdateStatus);//For PCAL-22446 Enhancement By Parminder Singh


         	ret = eventassocB.updateEventAssoc();





		//JM: 28Apr2008, added for, Enh. #C9
			if(oldStatus.equals("O") && status.equals("A") && ( !checkedVal.equals("0") && !checkedVal.equals("") )){
         		eventassocB.updatePatientSchedulesNow(EJBUtil.stringToNum(calendarId),EJBUtil.stringToNum(checkedVal), EJBUtil.stringToNum(selStat), dtVal, EJBUtil.stringToNum(userId), ipAdd);
        	}



			//JM: 23Apr2008, added for, Enh. #C9
			//if Calendar status is made Offline for editing then offlineFalg="1" else "0"
         	if(oldStatus.equals("A") && status.equals("O")){
         	 offlineFalg="1";
         	 eventassocB.updateOfflnFlgForEventsVisits(eventId, offlineFalg);
         	}else if(oldStatus.equals("O") && status.equals("A")){
         	 offlineFalg="0";
         	 eventassocB.updateOfflnFlgForEventsVisits(eventId, offlineFalg);
         	}else if(oldStatus.equals("O") && status.equals("D")){
         	 offlineFalg="0";
         	 eventassocB.updateOfflnFlgForEventsVisits(eventId, offlineFalg);
         	}




         //	out.print(ret);
         	   if (ret == 0) {

         	   	//if protocol status is changed

         	   		//JM: 22May2008 issue #3496
         	   		//if ( (! oldStat.equals(status)) && status.equals("A"))
         	   		if ( (! oldStat.equals(status)) && status.equals("A") && !oldStatus.equals("O")){
	         	   		System.out.println("###### oldStat" +  oldStat + " ##status" + status);
    	     	   		ret2 = alnot.setDefStudyAlnot(studyId,request.getParameter("protocolId"),ChangedById,ipAdd);
    	     		}
         	   		msg = MC.M_Pcol_CalSvdSucc;/*msg = "Protocol Calendar saved successfully";*****/

         	   } else {

         		msg = MC.M_Pcol_CalNotSvd;/*msg = "Protocol Calendar not saved";*****/

         	   }
         	if (calAssoc.equals("S"))
         	{
         
         %>
         

<br>

<br>

<br>

<br>

<br>

<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
           <META HTTP-EQUIV=Refresh CONTENT="0;URL=studyadmincal.jsp?srcmenu=<%=src%>&selectedTab=10&studyId=<%=studyId%>">
		<%} else {%>
		

<br>

<br>

<br>

<br>

<br>

<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
		  <META HTTP-EQUIV=Refresh CONTENT="0;URL=studyprotocols.jsp?&mode=M&srcmenu=<%=src%>&selectedTab=7&studyId=<%=studyId%>">
		<%}
         	
}//end of if body for session
}//end of if body for session

else
{
%>

  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</BODY>
</HTML>





