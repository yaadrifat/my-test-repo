<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="patFacility" scope="request" class="com.velos.eres.web.patFacility.PatFacilityJB"/>
<jsp:useBean id="prefB" scope="request" class="com.velos.eres.web.pref.PrefJB"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,com.velos.eres.business.pref.impl.*,com.velos.eres.business.person.impl.*"%>


<%

String eSign = request.getParameter("eSign");

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))

{
%>

<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String oldESign = (String) tSession.getValue("eSign");





	if(!oldESign.equals(eSign)) {

%>


	<jsp:include page="incorrectesign.jsp" flush="true"/> 
<%

	} else {

 	

	String ipAdd = (String) tSession.getValue("ipAdd");

	String usr = null;

	usr = (String) tSession.getValue("userId");

	


  int personPK  = 0;	

  String mode = null;

  String organization = "";

  String regBy = "";

  String account = "";

  String phyOther="";

  String specialityIds = "";
  String patientFacilityId = "";
  String patientFacilityIdold=request.getParameter("patientFacilityIdold")==null?"":request.getParameter("patientFacilityIdold");
  int patFacilityPK = 0;
  String regdate = "";
  String accessFlag = "";
  int savedKey = 0;
  String isDefault = "";
  boolean codeExists= false;
  int saved = 0;
	
	organization = request.getParameter("patorganization");
	organization=(organization==null)?"":organization;

   
 
 
      mode = request.getParameter("mode");
	   
	 String patientId = request.getParameter("patientId");
  	patientFacilityId = request.getParameter("patFacilityID");
  	patientFacilityId = patientFacilityId.trim();
  	String facilitycheck=CFG.PATFACILITY_CHECK;
  	
  	if(mode.equals("N")){
  		if(facilitycheck.equals("Enabled")){
  		codeExists = prefB.patientFacilityCodeExists(patientFacilityId,"");
		if(codeExists){
			
		%>
			<br><br><br><br><br>
			<p class = "successfulmsg" align = center> <%=MC.M_PatIdFacExst %><%-- The Patient Facility ID you have entered already exists. Please click on back and enter a new ID.*****--%></p>
			<p align = center>
			<button type="button" onclick="window.history.back();"><%=LC.L_Back%></button>
			</p>
			<script>
						if (!(typeof(document.getElementById("notification"))=="undefined") && (document.getElementById("notification")!=null))	
							toggleNotification("off","");
				</script>
		<% saved = -1;
			return;
		}
  		
  	}}
  	if(mode.equals("M")){
  		if(facilitycheck.equals("Enabled")){
  		if(!patientFacilityIdold.equals(patientFacilityId)){
  		codeExists = prefB.patientFacilityCodeExists(patientFacilityId,"");
		if(codeExists){
			
		%>
			<br><br><br><br><br>
			<p class = "successfulmsg" align = center> <%=MC.M_PatIdFacExst %><%-- The Patient Facility ID you have entered already exists. Please click on back and enter a new ID.*****--%></p>
			<p align = center>
			<button type="button" onclick="window.history.back();"><%=LC.L_Back%></button>
			</p>
			<script>
						if (!(typeof(document.getElementById("notification"))=="undefined") && (document.getElementById("notification")!=null))	
							toggleNotification("off","");
				</script>
		<% saved = -1;
			return;
		}
  		
  	}}}
  	
	regBy = request.getParameter("patregby");
	phyOther=request.getParameter("phyOther");
   	regdate = request.getParameter("regdate");
	accessFlag = request.getParameter("accessFlag");
	isDefault =  request.getParameter("isDefault");
   
   specialityIds = request.getParameter("selSpecialityIds");
   
    if(StringUtil.isEmpty(regBy ))
    {
    	regBy = "";
    }
      
	if(StringUtil.isEmpty(regdate))
    {
    	regdate = "";
    }
	   
   if(StringUtil.isEmpty(specialityIds))
   {
       specialityIds="";
   }
   
   if (organization.equals("")) organization = null; 

  
	if (mode.equals("M"))
	{

		patFacilityPK  = EJBUtil.stringToNum(request.getParameter("patFacilityPK"));

		patFacility.setId(patFacilityPK );
	
        patFacility.getPatFacilityDetails();

	}

	    patFacility.setPatientFacilityId(patientFacilityId); 
		patFacility.setPatientPK(patientId);
		patFacility.setPatientSite(organization);
		patFacility.setRegDate(regdate);
		patFacility.setRegisteredBy(regBy);
		patFacility.setRegisteredByOther(phyOther);
		patFacility.setPatSpecialtylAccess(specialityIds);
		patFacility.setPatAccessFlag(accessFlag);
		//out.println("accessFlag" + accessFlag);
	    patFacility.setIsDefault(isDefault);
		patFacility.setIPAdd(ipAdd);		 
	
		if (mode.equals("M"))

		{

		patFacility.setLastModifiedBy(usr);

		saved = patFacility.updatePatFacility();

		  

	}else

	{


		patFacility.setCreator(usr);

		patFacility.setPatFacilityDetails();
		savedKey = patFacility.getId(); 
		
	if (savedKey > 0)

		  {
			saved = 0;
		 }

		else

		{
			saved = -1;
		}
	}



	  if (saved == 0)


	  {
%>


<br>

<br> 
 
<br>

<br>

<br>

<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p>

<%

}

else

{

%>

<br>

<br>

<br>

<br>

<br>

<p class = "successfulmsg" align = center> <%=MC.M_DataCnt_Svd %><%-- Data could not be saved.*****--%></p>



<%

}
%>
<script>
	var formObj = window.opener.document.patient;

	if (!formObj) formObj = window.opener.document.patientDemogForm;
	formObj.action="patientdetails.jsp" ;
	formObj.submit();
	setTimeout("self.close()",1000); 
</script>
<%

}//end of if for eSign check

}//end of if body for session



else



{



%>



  <jsp:include page="timeout.html" flush="true"/>
  <%



}



%>





</BODY>

</HTML>












