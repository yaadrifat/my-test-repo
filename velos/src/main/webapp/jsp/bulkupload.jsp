<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
	<title>Bulk Upload</title>
	<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
</head>
 <script type="text/javascript">
var defaultHeight = navigator.userAgent.indexOf("MSIE") != -1 ? 650 : 500;
function openSequenceDialog() //YK: Added for PCAL-20461 - Opens the Dialog Window
{
		jQuery("#embedDataHere").dialog({ 
		height: defaultHeight,width: 700,position: 'center' ,modal: true,
		closeText:'',
		buttons: [
		          {
		              text: L_Close,
		              click: function() { 
		        	  $j(this).dialog('destroy');
		              }
		          }
		      ],
		close: function() { $j(this).dialog('destroy'); },
		beforeClose: function(event, ui) { if(!checkBeforeClose()){ return false;} }
        });
  
}
function loadModifySequenceDialog(logPk,totReds,succRec,unSuccRec) {
		    jQuery('#embedDataHere').load("bulkUpladViewLogs.jsp?logPk="+logPk+"&totReds="+totReds+"&succRec="+succRec+"&unSuccRec="+unSuccRec);
		    openSequenceDialog(); 
			}
function checkBeforeClose()
{
	return true;
}
        function doSubmit(csvFile,FromPatPage,patientCode,patientId){
		if(csvFile=="New")
		{
		winOpen =window.open("bulkfileuploaddetail.jsp?FromPatPage="+FromPatPage+"&patientCode="+patientCode+"&pkey="+patientId,'_self',false) 
		winOpen.focus();
		}
		else if(csvFile=="Map")
		{
		winOpen =window.open("bulkuploaddetails.jsp?FromPatPage="+FromPatPage+"&patientCode="+patientCode+"&pkey="+patientId,'_self',false) 
		winOpen.focus();
		}
        }
        </script>
<% String src;
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="tdmenubaritem6"/>
</jsp:include>
<jsp:include page="ui-include.jsp" flush="true"/>
<%@page import="com.velos.eres.bulkupload.business.*,java.util.*,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*,com.velos.eres.service.util.ES"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<body class="yui-skin-sam yui-dt yui-dt-liner" style="overflow: hidden">
<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1100;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
{
String userId="";
String accountId="";
ArrayList<String>pkBulkUpload=null;
ArrayList<String> fileName=null;
ArrayList<String> totRecord=null;
ArrayList<String> sucRecord=null;
ArrayList<String> unsucRecord=null;
ArrayList<String> createdOn=null;
ArrayList<String> usrFirstName=null;
ArrayList<String> usrMidName=null;
ArrayList<String> usrLastName=null;
userId = (String) tSession.getValue("userId");
accountId = (String) tSession.getValue("accountId");
String FromPatPage=request.getParameter("FromPatPage");
if(FromPatPage==null){FromPatPage="";}
String patientCode=request.getParameter("patientCode");
if(patientCode==null){patientCode="";}
String patientId=request.getParameter("pkey");
if(patientId==null){patientId="";}
String userName="";
bulkUploadDao bukUpldDao=new bulkUploadDao();
bukUpldDao.getUploadHistory(accountId);
pkBulkUpload=bukUpldDao.getPkBulkUpload();
fileName=bukUpldDao.getFileName();
totRecord=bukUpldDao.getTotRecord();
sucRecord=bukUpldDao.getSucRecord();
unsucRecord=bukUpldDao.getUnsucRecord();
createdOn=bukUpldDao.getCreatedOn();
usrFirstName=bukUpldDao.getUsrFirstName();
usrMidName=bukUpldDao.getUsrMidName();
usrLastName=bukUpldDao.getUsrLastName();
%>
<% if(FromPatPage.equals("patient")  || FromPatPage.equals("patientEnroll")){%>
<DIV class="BrowserTopn" id="divTab">
<jsp:include page="patienttabs.jsp" flush="true">
<jsp:param name="selectedTab" value="12"/>
<jsp:param name="pkey" value="<%=patientId%>"/>
<jsp:param name="page" value="<%=FromPatPage%>"/>
<jsp:param name="page" value="<%=patientCode%>"/>
</jsp:include>
</Div>
<%} else{%>
<DIV class="tabDefTopN" id="div1">
		<jsp:include page="inventorytabs.jsp" flush="true">
		<jsp:param name="selectedTab" value="1"/>
		</jsp:include>
</DIV>
<% }%>
<DIV class="tabFormTopN tabFormTopN_PAS"  id="div2" style="overflow:visible" width="99%;">
<table class="basetbl outline midAlign lhsFont" width="100%" cellspacing="0" cellpadding="0" border="0" >
      
      <tr >
		<td ><b><font size="3" class="sectionHeadingsFrm"><%=ES.ES_BulkUpld_Area%></font></b></td>
        
        <td colspan="2" ><button type="submit" onclick="doSubmit('New','<%=FromPatPage %>','<%=patientCode %>','<%=patientId %>');"><%=ES.ES_StrtNew_Upload%></button><button type="submit" onclick="doSubmit('Map','<%=FromPatPage %>','<%=patientCode %>','<%=patientId %>');"><%=ES.ES_Mang_MApp%></button></td>
        </tr>
		   
</table>
<BR>
		
	 <%if(!pkBulkUpload.isEmpty()){
	 %>		
	<SCRIPT LANGUAGE="JavaScript">
		var isIE = jQuery.browser.msie;
		var screenWidth = screen.width;
		var screenHeight = screen.height;
		if(screenWidth>1280 || screenHeight>1024)
		{
			if(isIE==true)
			{
				document.write('<div id="divId1" class="BrowserBotN BrowserBotN_MI_4" style="height:260%; top:70px;">');
			}
			else
			{
				document.write('<div id="divId1" class="BrowserBotN BrowserBotN_MI_4" style="height:300%; top:70px;" >');
			}
		}
		else
		{
			if(isIE==true)
			{
				document.write('<div id="divId1" class="BrowserBotN BrowserBotN_MI_4" style="height:400%; top:70px;">');
			}
			else
			{
				document.write('<div id="divId1" class="BrowserBotN BrowserBotN_MI_4" style="height:450%; top:70px;" >');
			}
		}
	</SCRIPT>	
	<table class="basetbl outline midAlign lhsFont custom-table-center" style="margin:0 0 0 0;" width="100%" cellspacing="0" cellpadding="0" border="0" >	
	<tr >
		<td style="padding: 10px 0 10px 10px;" colspan="4"><b><font size="3" class="sectionHeadingsFrm"><%=ES.ES_Upld_Histy%></br></font></b></td>		
     </tr>   
		<tr>
		<th>#</th>
		<th><%=ES.ES_DateOf_Upld%></th>
		<th><%=ES.ES_Upld_By%></th>
		<th><%=ES.ES_Upld_Log%></th>
		</tr>
	   <%for (int errSize=0;errSize<pkBulkUpload.size();errSize++)  
	   {
	   if(!usrFirstName.isEmpty())
		{
    	if(!StringUtil.isEmpty(usrFirstName.get(errSize)))
    		userName=usrFirstName.get(errSize);
		}
		if(!usrMidName.isEmpty())
		{
    	if(!StringUtil.isEmpty(usrMidName.get(errSize)))
    		userName=userName+" "+usrMidName.get(errSize);
		}
		if(!usrLastName.isEmpty())
		{
    	if(!StringUtil.isEmpty(usrLastName.get(errSize)))
    		userName=userName+" "+usrLastName.get(errSize);
		}
	   %>
		<tr  >
	     <td><%=pkBulkUpload.get(errSize)%></td>
		 <td><%=DateUtil.dateToString(java.sql.Date.valueOf(createdOn.get(errSize).toString().substring(0,10)))%>&nbsp;<%=createdOn.get(errSize).toString().substring(11,19)%></td>
		 <td><%=userName%></td>
		 <%
		 String pkblk=pkBulkUpload.get(errSize);
		 String crted=createdOn.get(errSize);
		 String toRec=totRecord.get(errSize);
		 String sucRec=sucRecord.get(errSize);
		 String unSuccRec=unsucRecord.get(errSize);
		 %>
		 <td  align="center"><A href="#" onClick="loadModifySequenceDialog('<%=pkblk%>','<%=toRec%>','<%=sucRec%>','<%=unSuccRec%>')">
		 <img src="../images/jpg/preview.gif" title="<%=ES.ES_Preview%>" border="0"></A></td>
         </tr>
	  <%}%>
	  </table>
	  </div>
	   <%}%>
		 
		  
		      
      
	  
	  </div>
	   <div id="editVisitDiv" title="Upload Data" style="display:none" class="velos-yui-dt-container ui-gadget">
<div id='embedDataHere'>

</div>
</div>
	  <div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
	
<%
} //end of if session times out
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

} //end of else body for session time out
%>
</body>

</html>
