<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.*"%>

<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="java.util.Date"%><jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="auditB" scope="request" class="com.velos.eres.audit.web.AuditRowEresJB"/>

<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html");
	HttpSession tSession = request.getSession(true);
	//JSONObject jsObj = new JSONObject();
	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		//jsObj.put("result", -1);
		//jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
   	%>
		<input type="hidden" name="resultMsg" id="resultMsg" value="<%=MC.M_UsrNot_LoggedIn %>"/>    	
	<%
		//return;
	}
	
	

	boolean flag=true;
    try {    	
    	String action = "",tableName = "";
    	String usr = null;
    	 usr = (String) tSession.getValue("userId");
    	 UserJB userB = new UserJB();
    	 userB.setUserId(StringUtil.stringToNum(usr));
    	 userB.getUserDetails();
    	 int userId = StringUtil.stringToNum(usr);
    	 int siteId = StringUtil.stringToNum(userB.getUserSiteId());
    	 int groupId = StringUtil.stringToNum(userB.getUserGrpDefault());
    	 String uFName = userB.getUserFirstName();
    	 String uLName = userB.getUserLastName();
    	tableName = request.getParameter("tableName");
    	action = request.getParameter("action");
    	auditB.setAction(action);
    	auditB.setTableName(tableName);
    	auditB.setRID(99999999);
    	auditB.setUserName(usr+", "+uLName+", "+uFName);
    	auditB.setTimeStamp(new Date());
    	auditB.setAuditRowDetails();    	
		%>
				<input type="hidden" name="resultMsg" id="resultMsg" value="<%=flag %>"/> 
		<%			
    		
	} catch(Exception e) {
    	%>
    		<input type="hidden" name="resultMsg" id="resultMsg" value="<%=flag %>"/>    	
    	<%
		//return ;
	}
%>
	