<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.velos.eres.service.util.LC"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE><%=LC.L_Adverse_Event%><%--Adverse Event*****--%></TITLE>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<script>
function SendBack(grade,formobj,flag,versionnum){
 
	var pform = formobj.parentform.value;
 	var gradefld = formobj.gradefld.value;
 	var dispfield = formobj.dispfield.value;
 	var advnamefld = formobj.advnamefld.value;
 	var advGradeDesc = formobj.advGradeDesc.value;
 	
 //	 var advDictionary = formobj.advDictionary.value;
 	var event = formobj.advName.value;
 	
 	var currentDictSettingValue = formobj.currentDictSettingValue.value;
 	
 	var advDictionaryField = formobj.advDictionaryField.value;

	if(flag=='remove'&& advnamefld=='advName') {
		window.opener.document.forms[pform].elements['dictValSel'].value='';
		window.opener.document.forms[pform].elements['aeCategory'].value='';
		window.opener.document.forms[pform].elements['aeToxicity'].value='';
		window.opener.document.forms[pform].elements['aeToxicityDesc'].value='';
		window.opener.document.forms[pform].elements['MedDRAcode'].value='';
	
		
		window.opener.document.forms[pform].elements['aeGradeDesc'].value=""; 
		
		window.opener.document.forms[pform].elements[gradefld].value=""; 
		window.opener.document.forms[pform].elements[advnamefld].value=""; 
		window.opener.document.forms[pform].elements[dispfield].value= "";
		window.opener.document.forms[pform].elements[showfield].value= "";
		window.opener.document.forms[pform].elements[advDictionaryField].value= "";	
		
	} else {
		
		if(advnamefld=='advName'){
		window.opener.document.forms[pform].elements['dictValSel'].value='';
		window.opener.document.forms[pform].elements['aeCategory'].value='';
		window.opener.document.forms[pform].elements['aeToxicity'].value='';
		window.opener.document.forms[pform].elements['aeToxicityDesc'].value='';
		window.opener.document.forms[pform].elements['MedDRAcode'].value='';
		}
		
		var gradeDesc = '';
		switch(grade)
		{
		case "1":
			  gradeDesc = "<%=LC.L_Mild%>";
			  break;
		case "2":
			gradeDesc = "<%=LC.L_Moderate%>";
			  break;
		case "3":
			  gradeDesc = "<%=LC.L_Severe%>";
			  break;
		case "4":
			gradeDesc = "<%=MC.M_Life_ThreatDisabling%>";
			  break;
		case "5":
			gradeDesc = "<%=MC.M_Dth_RelToAE%>";
		  break;
		}
		if(advnamefld=='advName'){
		window.opener.document.forms[pform].elements['aeGradeDesc'].value=gradeDesc; 
		
		window.opener.document.forms[pform].elements[gradefld].value=grade; 
		window.opener.document.forms[pform].elements[advnamefld].value=event;
		window.opener.document.forms[pform].elements[advDictionaryField].value= currentDictSettingValue;
		}else
		{
			if(flag=='remove'){window.opener.document.forms[pform].elements[dispfield].value= "";}
			else
		{
			window.opener.document.forms[pform].elements[dispfield].value="<%=LC.L_Grade%> : " +  grade + " " + event ;
			window.opener.document.forms[pform].elements[advnamefld].value=event;
			window.opener.document.forms[pform].elements[gradefld].value=grade; 
			window.opener.document.forms[pform].elements[advGradeDesc].value=gradeDesc;
			
			 
		}
			 
		}
		/* commented by Amarnadh for #3253*/
		/*Added by Gopu to fix the bugzilla Issues #2640 */
		// window.opener.document.forms[pform].elements["advDictionary"].value="Free Text Entry"; 	
		
	}
	this.close();	 
}
</script>
<BODY>
<%@ page language = "java" import = "com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession)){
		String parentform = request.getParameter("parentform");
		String gradefld = request.getParameter("gradefld");
		String advnamefld =	request.getParameter("advnamefld");
		String dispfield =	request.getParameter("dispfield");
		String currentDictSettingValue = request.getParameter("currentDictSettingValue");
		String advDictionaryField = request.getParameter("advDictionaryField");
		String advGradeDesc = request.getParameter("advGradeDesc")==null?"":request.getParameter("advGradeDesc");
		
	%>
		<jsp:include page="sessionlogging.jsp" flush="true"/> 
		<BR>	
		<form name="dictSetting" METHOD="POST">		
			<INPUT type=hidden name= parentform value =<%=parentform%>>		
			<INPUT type=hidden name= gradefld value = <%=gradefld%>>		
			<INPUT type=hidden name= dispfield value = <%=dispfield%>>				   
			<INPUT type=hidden name= advnamefld value = <%=advnamefld%>>
			<INPUT type=hidden name= currentDictSettingValue value = '<%=currentDictSettingValue%>'>
			<INPUT type=hidden name= advDictionaryField value = <%=advDictionaryField%>>
			<INPUT type=hidden name= advGradeDesc value = <%=advGradeDesc%>>
									   			
		<table class="basetbl" width="100%" >
			<tr>
				<td ><%=LC.L_Adv_EvtName%><%--Adverse Event Name*****--%></td>
					<td><Input type="text" name="advName" size="30" maxlength="500">
				</td> 
			</tr>
		</table>	
		<P class="sectionHeadings"><%=LC.L_Adv_EvtGrade%><%--Adverse Event Grade*****--%></P>
		<p class="defcomments"><A href="#" onClick="SendBack('', document.dictSetting, 'remove')"><%=MC.M_RemAldy_AdvEvtGrade%><%--Remove Already Selected Adverse Event Grade*****--%></A></p>
		<BR>		
		<table width="100%" border="0" class="basetbl"> 
			<tr class="browserOddRow"> 
				<td ><%=LC.L_Grade1%><%--Grade 1*****--%></td> <td ><%=LC.L_Mild%><%--Mild*****--%></td> 
				<td >
						
<!--<A HREF="#" onClick="sortThis(this.form, 'PERSONS.last_name, PERSONS.first_name')">Name</A>-->

						<A href="#" Onclick="SendBack('1', document.dictSetting, '' )"><%=LC.L_Select%><%--Select*****--%></A>
				</td>     	
			</tr>		
			<tr class="browserEvenRow"> 
				<td ><%=LC.L_Grade2%><%--Grade 2*****--%></td> <td ><%=LC.L_Moderate%><%--Moderate*****--%></td> 
				<td >
					<A href="#" Onclick="SendBack('2', document.dictSetting,'')"><%=LC.L_Select%><%--Select*****--%></A>
				</td>     	
			</tr>
			<tr class="browserOddRow"> 
				<td ><%=LC.L_Grade3%><%--Grade 3*****--%></td> <td ><%=LC.L_Severe%><%--Severe*****--%></td> 
				<td >
					<A href="#" Onclick="SendBack('3', document.dictSetting,'' )"><%=LC.L_Select%><%--Select*****--%></A>
				</td>     	
			</tr>
			<tr class="browserEvenRow"> 
				<td ><%=LC.L_Grade4%><%--Grade 4*****--%></td> <td ><%=MC.M_Life_ThreatDisabling%><%--Life-threatening or Disabling*****--%></td> 
				<td >
					<A href="#" Onclick="SendBack('4', document.dictSetting,'' )"><%=LC.L_Select%><%--Select*****--%></A>
				</td>     	
			</tr>	
			<tr class="browserOddRow"> 
				<td ><%=LC.L_Grade5%><%--Grade 5*****--%></td> <td ><%=MC.M_Dth_RelToAE%><%--Death related to AE*****--%></td> 
				<td >
					<A href="#" Onclick="SendBack('5', document.dictSetting,'' )"><%=LC.L_Select%><%--Select*****--%></A>
				</td>     	
			</tr>		
		</table>	
			<br>
	</form>	
	<%
	} //end of if for session
	else{%>
	<jsp:include page="timeout_childwindow.jsp" flush="true"/> 
	<%}%>
</BODY>
</HTML>
