<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!--Code for bug id:- #18691 -->
	<title><%=MC.M_MngPat_EnrlPat.substring(0,19)%>New<%--Manage Patients >> Enroll Patient*****--%></title>
</head>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@ page import="com.velos.eres.web.user.ConfigFacade,com.velos.eres.web.user.ConfigObject, com.velos.eres.web.user.ConfigDetailsObject"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB, com.velos.eres.web.user.UserJB,com.velos.remoteservice.demographics.*"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />

<% String src;
src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
String page1 = request.getParameter("page");
int pkey = StringUtil.stringToNum(request.getParameter("pkey"));
String patientID = null; 

String stid= request.getParameter("studyId");

HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))	{
	String userIdFromSession = (String) tSession.getAttribute("userId");
//userBFromSession = (UserJB) tSession.getAttribute("currentUser");

	if (pkey!=0) {
		person.setPersonPKId(pkey);
		person.getPersonDetails();
		patientID = person.getPersonPId();
		patientID = (patientID==null)?"":patientID;
		String prevPatientId=patientID;
		int patStatusId= StringUtil.stringToNum(person.getPersonStatus());
		String organization = person.getPersonLocation() ;
	
		String specialityIds = person.getPersonSplAccess();
		String specialityNames="";
		String splAccessRight= "0";
		if(specialityIds==null || specialityIds.equals("")){
			specialityNames="";
			splAccessRight="1";
		}
		organization = (organization==null)?"":organization;
	
		String patPrimOrg=organization;
		siteB.setSiteId(StringUtil.stringToNum(organization)) ;
		siteB.getSiteDetails();
		String patOrg = siteB.getSiteName();
		//check whether the user has right to edit the patients of the patient org
		int orgRight = userSiteB.getUserPatientFacilityRight(StringUtil.stringToNum(userIdFromSession),pkey);
	}

	//look for a the remote service. If a remote service exists, it's
	//return values will override the values of this page. If it does not
	//exist, we will use default behavior.
	IDemographicsService demographicsService = DemographicsServiceFactory.getService();
	
	boolean isRemoteDemoInstalled = (demographicsService == null) ? true : false;
%>

<%--Include JSP --%>
<jsp:include page="patientEnrollScreenInclude.jsp" flush="true"/>
<%----%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
<%
String onLoadStr ="";
if (isRemoteDemoInstalled) onLoadStr = "onload=\"fetchData()\"";
String enrollId = (String) tSession.getAttribute("enrollId");
%>

<body <%=onLoadStr%>>
<DIV class="BrowserTopn" id="divTab">
</DIV>
<DIV class="BrowserBotN BrowserBotN_S_5" id="div1" style="top: 85px">
<%	
	if(page1.equals("enrollPatientsearch")){
		String patCode=request.getParameter("patCode");
	}
	String mode = request.getParameter("mode");
	int orgRight = 0;

	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getAttribute("GRights");
	int pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));
	%>
	<%if ((mode.equals("M") && (StringUtil.isAccessibleFor(pageRight, 'E')  && StringUtil.isAccessibleFor(orgRight, 'E'))) 
			|| (mode.equals("N") && StringUtil.isAccessibleFor(pageRight, 'N'))){ %>
	  <form name="patientEnrollForm" class="custom-design-form" id="patientEnrollForm" method="post" action="#" onSubmit="return patientEnrollScreenFunctions.validatePatientEnrollScreen();">
		<%
	  	boolean hasAccess = false;
		String uName = (String) tSession.getAttribute("userName");
		String acc = (String) tSession.getAttribute("accountId");
		 %>
		<div style="border:1">
		<div id="patientSection1" name="patientSection">
			<div id="patientTab1content" onclick="toggleDiv('patientTab1')" class="portlet-header portletstatus 
					ui-widget ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" style="background-color:#CCCCCC;">
				<span class="ui-icon ui-icon-triangle-1-s"></span><%=LC.L_Patient_Details%>
			</div>
			<div id="patientTab1" style="width:99%">
				<jsp:include page="patientdetailsquick.jsp" >
					<jsp:param name="pkey" value=""/>
					<jsp:param name="srcmenu" value="<%=src%>"/>
					<jsp:param name="selectedTab" value="<%=selectedTab%>"/>				
					<jsp:param name="mode" value="<%=mode%>"/>
					<jsp:param name="page" value="patient"/>
					<jsp:param name="includeMode" value="Y"/>
				</jsp:include>
			</div>
		</div>
		<div id="patientSection2" name="patientSection">
			<div id="patientTab2content" onclick="toggleDiv('patientTab2');" class="portlet-header portletstatus 
					ui-widget ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" style="background-color:#CCCCCC;">
				<span class="ui-icon ui-icon-triangle-1-s"></span>
				<%=LC.L_More_PatDets%>
			</div>
			<div id='patientTab2' style="width:99%">
				<jsp:include page="morePerDetails.jsp" >
					<jsp:param name="perId" value="<%=pkey%>"/>
				</jsp:include>
			</div>
		</div>
		</div>
		<br>
		<%if ((mode.equals("M") && (StringUtil.isAccessibleFor(pageRight, 'E')  && StringUtil.isAccessibleFor(orgRight, 'E'))) 
			|| (mode.equals("N") && StringUtil.isAccessibleFor(pageRight, 'N'))){ %>
		<jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="studyForm"/>
			<jsp:param name="showDiscard" value="N"/>
		</jsp:include> 
		<%} %>
		<div style="height: 20px;"></div>
		<div class = "myHomebottomPanel">
		    <jsp:include page="bottompanel.jsp" flush="true"/>
		</div>
		<div class ="mainMenu" id="emenu">
		  	<jsp:include page="getmenu.jsp" flush="true"/>
		</div>
		<div id="popMeUp" name="popMeUp"></div>
	</form>
		  
		  <%
		} //end of if body for page right
		else{
		%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
		<%
		} //end of else body for page right
	}//end of if body for session
	else {%>
		<jsp:include page="timeout.html" flush="true"/>
	<%}%>
<div>
	<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
<DIV id="wsDIV" class="pageInst" >
</DIV>
</DIV>
</body>
</html>

