<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<script>
var studySubmScreenFunctions = {
	formObj: {},
	
	setDDFlex: {},
	expandAll: {},
	collapseAll: {},
	toggleExpandCollapseAllClick: {}
};
jQuery(document).ready(function() {
	studySubmScreenFunctions.formObj = document.studyScreenForm;
});
</script>
<script>

studySubmScreenFunctions.expandAll = function (){
	//$j('#toggleExpandCollapseAll').attr('src', './images/collapseIcon.png');
	$j('#toggleExpandCollapseAll').attr('title', "Collapse All");

	$j(".ui-accordion-content").show();	
	var activeSection = "";
	for (var indx=pageSectionRegistry.length; indx > 0; indx--){
		var sectionNo = indx;
		sectionHeaderClick(sectionNo);
		if($j('#sectionHeader'+ sectionNo).hasClass("activeSection")){
			activeSection = '#sectionBody'+ sectionNo;
		}
		var triangleSpan = $j('#sectionHeader'+ sectionNo).find('span.ui-icon-triangle-1-e');
		triangleSpan.removeClass('ui-icon-triangle-1-e');
		triangleSpan.addClass('ui-icon-triangle-1-s');
	}

	var inHTML = $j('#toggleExpandCollapseAll').html();
	inHTML = inHTML.replace("Expand", "Collapse");
	$j('#toggleExpandCollapseAll').html(inHTML);
	
	setTimeout(function(){
		if(activeSection != ""){
			$j(activeSection).find("table").each(function(){
				if($j(this).hasClass("basetbl")){
					$j(this).find(":input").each(function(){
						$j(this).trigger('focus');
						return false;
					});
				}
			});
		}
		else{
			$j('#sectionBody1').find("table").each(function(){
				if($j(this).hasClass("basetbl")){
					$j(this).find(":input").each(function(){
						$j(this).trigger('focus');
						return false;
					});
				}
			});
		}
	},2000);
	
	return false;
};

studySubmScreenFunctions.collapseAll = function (){
	//$j('#toggleExpandCollapseAll').attr('src', './images/expandIcon.png');
	$j("#studyScreenForm .activeSection ").each(function(){
		$j(this).removeClass("activeSection");
	});
	$j('#toggleExpandCollapseAll').attr('title', "Expand All");	

	$j(".ui-accordion-content").hide();
	$j('#sectionHead1').focus();

	for (var indx=pageSectionRegistry.length; indx > 0; indx--){
		var sectionNo = indx;

		var triangleSpan = $j('#sectionHeader'+ sectionNo).find('span.ui-icon-triangle-1-s');
		triangleSpan.removeClass('ui-icon-triangle-1-s');
		triangleSpan.addClass('ui-icon-triangle-1-e');
	}

	var inHTML = $j('#toggleExpandCollapseAll').html();
	inHTML = inHTML.replace("Collapse", "Expand");
	$j('#toggleExpandCollapseAll').html(inHTML);

	return false;
};

studySubmScreenFunctions.toggleExpandCollapseAllClick = function (){
	//if ( $j('#toggleExpandCollapseAll').attr('src') == './images/expandIcon.png' ) {
	if ( $j('#toggleExpandCollapseAll').attr('title') == "Expand All" ) {
		studySubmScreenFunctions.expandAll();
	} else {
		studySubmScreenFunctions.collapseAll();
	}
	
	return false;
};

studySubmScreenFunctions.setDDFlex = function(iElementId){
	if (!document.getElementsByName("ddlist"+iElementId)[0]) return;
	var optvalue=(document.getElementsByName("ddlist"+iElementId)[0]).value;
	if (optvalue.length>0)
	{
		var arrayofStrings=optvalue.split("||");
		if (arrayofStrings.length>1) {
			for (var j=0;j<arrayofStrings.length;j++)
			{
				var ddStr=arrayofStrings[j];
				var arrayofDD=ddStr.split(":");
				var ddcount=arrayofDD[0];
				var selvalue=arrayofDD[1];
				var ddFld = studySubmScreenFunctions.formObj['alternateId'+ddcount];
				// ddFld.id = ddFld.name;
				if (ddFld && ddFld.options) {
					var opt = ddFld.options;
					for (var i=0;i<opt.length;i++){
						if (opt[i].value==selvalue){
							ddFld.selectedIndex=i ;
						}
					}
				}
			}
		} else {
			var ddStr=arrayofStrings[0];
			var arrayofDD=ddStr.split(":");
			var ddcount=arrayofDD[0];
			var selvalue=arrayofDD[1];
			var ddFld = studySubmScreenFunctions.formObj['alternateId'+ddcount];
			// ddFld.id = ddFld.name;
			if (ddFld && ddFld.options) {
			 	var opt = ddFld.options; 
				if (opt == undefined)
			    	opt = ddFld.options; 
				for (var i=0;i<opt.length;i++){
					if (opt[i].value==selvalue){
						ddFld.selectedIndex=i ;
					}
				}
			}
		}// end else
	}//optvalue.length>0	
};
</script>

