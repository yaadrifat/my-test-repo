<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*"%>
<jsp:useBean id="storageB" scope="request" class="com.velos.eres.web.storage.StorageJB"/>
<head>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<style>
div #load {
    padding: 0 10px;
    font-size: 93%;
     line-height: 2;
    font-weight: bold;
    color: #000;
    background-color:#ded7d7;
}
</style>
<script>
function callOverlib(id,name,subtype,status) {
	return overlib(
			'<tr><td><font size=2><%=LC.L_Storage_Id%><%--Storage ID*****--%>: </font></td>'+
            '<td><font size=2>'+id+'</font></td></tr>'+
            '<tr><td><font size=2><%=LC.L_Name%><%--Name*****--%>: </font></td>'+
            '<td><font size=2>'+name+'</font></td></tr>'+
            '<tr><td><font size=2><%=LC.L_Type%><%--Type*****--%>: </font></td>'+
            '<td><font size=2>'+subtype+'</font></td></tr>'+
            '<tr><td><font size=2><%=LC.L_Status%><%--Status*****--%>: </font></td>'+
            '<td><font size=2>'+status+'</font></td></tr>',ABOVE);
}
function handleKeyPress(e,formobj)
{
	if (!e) e = window.event;
	if (e && e.keyCode == 13)
		{
			// handle Enter key
			// return false; here to cancel the event
			callAjaxGetStorages(formobj)
		}
}


	function onSubmit()
	{
	 return false;
	}
	
    function Functionpkstorage()
	{
	document.search.PKStorageID.value='';
    }
	function selectStorage(locName,cx,cy,pk)
	
	{	
	frmName = document.search;
	 if(frmName.callingform.value != "")
	{
		genOpenerFormName = frmName.callingform.value;
		
		locationFldName = frmName.locationFldName.value;
		

		coordXFldName = frmName.coordXFldName.value;

		coordYFldName = frmName.coordYFldName.value;

		storagePKFldName = frmName.storagePKFldName.value;
		


		window.opener.document.forms[genOpenerFormName].elements[locationFldName].value = locName;
		window.opener.document.forms[genOpenerFormName].elements[coordXFldName].value = cx;
		window.opener.document.forms[genOpenerFormName].elements[coordYFldName].value = cy;

		window.opener.document.forms[genOpenerFormName].elements[storagePKFldName].value = pk;

		self.close();
		return;
	}
	//
	}

	function selectStorage4Specimen(locName,pk)
	{
	frmName = document.search;
	if(frmName.callingform.value != "")
	{
		genOpenerFormName = frmName.callingform.value;
		locationFldName = frmName.locationFldName.value;
	    storagePKFldName = frmName.storagePKFldName.value;
	    window.opener.document.forms[genOpenerFormName].elements[locationFldName].value = locName;
		window.opener.document.forms[genOpenerFormName].elements[storagePKFldName].value = pk;
		

		self.close();
		return;
	}
	
	}

	function callAjaxGetStorages(formobj){
		showviewcheck=formobj.showGridView.checked;
		name = formobj.storageName.value;
		id = formobj.storageID.value;
	    gridFor = formobj.gridFor.value;
	    storageType = formobj.storageType.value;
	    srchLocFlag = formobj.srchLocFlag.value;
	    specimenType=formobj.specimenType.value;
	    storageStatus=formobj.storageStatus.value;
	    selStudyIds = formobj.selStudyIds.value;
	    PKStorageID = formobj.PKStorageID.value;
	 	orderBy = formobj.orderBy.value;
	 	orderType = formobj.orderType.value;
	    page=formobj.page.value;
	    selStudy = formobj.selStudy.value;
	    callingform=formobj.callingform.value;
	    locationFldName = formobj.locationFldName.value;
		storagePKFldName = formobj.storagePKFldName.value;
		storageview = formobj.storageview.value;
		coordXFldName = formobj.coordXFldName.value;
		coordYFldName = formobj.coordYFldName.value;
		
		
		if(showviewcheck==true){
		   showviewcheck="on";
	   }
		   else{
			showviewcheck="";
	   }

 	   new VELOS.ajaxObject("getStorageGrid.jsp", {
   		urlData:"dispType=B&storageId="+id+"&name="+name+"&gridFor="+gridFor+"&storageType="+storageType
   		       +"&pkStorage="+formobj.pkStorage.value+"&srchLocFlag="+srchLocFlag+"&showviewcheck="+showviewcheck+"&specimenType="+specimenType+"&storageStatus="+storageStatus+"&selStudyIds="+selStudyIds+"&PKStorageID="+PKStorageID+"&page="+page+"&selStudy="+selStudy+"&callingform="+callingform+"&locationFldName="+locationFldName+"&storagePKFldName="+storagePKFldName+"&storageview="+storageview+"&coordYFldName="+coordYFldName+"&coordXFldName="+coordXFldName+"&orderBy="+orderBy+"&orderType="+orderType,
		   reqType:"POST",
		   outputElement: "span_storage" }

   ).startRequest();

    storage_path.innerHTML = "";
    span_storagechild.innerHTML = "";
    formobj.childspanname.value="";

       if (formobj.pkStorage.value == "0" ) { return; }
       callAjaxGetStorageParents(formobj);
  	  var elem = document.getElementById("span_storagechild");
   }

   function callAjaxGetStorageParents(formobj) {
	   storageview = formobj.storageview.value;
       var childspanname = "span_storagechild";
 	   new VELOS.ajaxObject("getStorageGrid.jsp", {
 	   		urlData:"child="+formobj.pkStorage.value+"&dispType=G&explorer=1&gridFor="+formobj.gridFor.value+"&storageview="+storageview,
 			   reqType:"POST",
 			   outputElement: childspanname }
 	       ).startRequest();
   }

   function callAjaxGetStorageChildren(parent,parentname,gridFor,grandparent,
		   topStorage,topStorageName,level,column,row){
	   callAjaxGetStorageChildren(parent,parentname,gridFor,grandparent,
			   topStorage,topStorageName,level,column,row, null);
   }
   function callAjaxGetStorageChildren(parent,parentname,gridFor,grandparent,
		   topStorage,topStorageName,level,column,row, explorer){
       // alert('parent='+parent+' gridFor='+gridFor+' grandparent='+grandparent+' topStorage='+topStorage+' topStorageName='+topStorageName+
       //        ' level='+level+' column='+column+' explorer='+explorer);
	   var curdivs;
	   storageview = document.search.storageview.value;
       for (var iY=(level+1); iY<702; iY++) {
	       var elem = document.getElementById("span_storagechild"+iY);
	       if (elem == null) { break; }
		   elem.innerHTML = "";
       }
       var childspanname = "span_storagechild" + (level+1);

	   if (parent == topStorage) {
		   storage_path.innerHTML = "";
		   span_storagechild.innerHTML = "";
       }
       oldchHTML = span_storagechild.innerHTML;
 	   // span_storagechild.innerHTML = oldchHTML + "<span ID=\""+childspanname+"\" style=\"float:left;position:relative;margin-left: -20px;\"></span>";
	   span_storagechild.innerHTML = oldchHTML + "<span ID=\""+childspanname+"\" style=\"float:left;\"></span>";

	   srchLocFlag = document.search.srchLocFlag.value;
	   showviewcheck=document.search.showGridView.checked;
	   if(showviewcheck==true){
		   showviewcheck="on";
	   }
		   else{
			showviewcheck="";
	   } 
	   var explorerParameter = explorer == null ? "" : "&explorer="+explorer;
 	   new VELOS.ajaxObject("getStorageGrid.jsp", {
   		urlData:"parent="+parent+"&dispType=G&gridFor="+gridFor+"&parentName="+parentname+
   		        "&pkTopStorage="+topStorage+"&topStorageName="+topStorageName+explorerParameter+"&srchLocFlag="+srchLocFlag+"&showviewcheck="+showviewcheck+"&storageview="+storageview,
		   reqType:"POST",
		   outputElement: childspanname }
       ).startRequest();
 	   var eachTd = null;
       for (var iY=1; iY<702; iY++) {
 	       eachTd = document.getElementById("td_level"+level+"_col1_row"+iY);
 	       if (eachTd == null) { break; }
           for (var iX=1; iX<702; iX++) {
     	       eachTd = document.getElementById("td_level"+level+"_col"+iX+"_row"+iY);
     	       if (eachTd == null) { break; }
     	       if (iY %2 == 1) eachTd.style.backgroundColor = "#DADADA";
     	       else eachTd.style.backgroundColor = "#F7F7F7";
           }
       }
       if (row == null) row = 1;
 	   var td = document.getElementById("td_level"+level+"_col"+column+"_row"+row);
 	   if (td != null) {
 	      td.style.backgroundColor = "#FDFDDE";
 	   }
   }
   function goToDetailInParent(pkStorage) {
	   window.opener.location.href = 'storageunitdetails.jsp?srcmenu=tdmenubaritem6&selectedTab=2&mode=M&pkStorage='+pkStorage;
	   setTimeout('self.close()',1000);
   }
   function openStudyWindow(formobj) {
		formobj.target="Lookup";
		formobj.method="post";
		formobj.action="multilookup.jsp?viewId=6013&form=search&seperator=,"+
	                  "&keyword=selStudy|STUDY_NUMBER~selStudyIds|LKP_PK|[VELHIDE]&maxselect=1";

		formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');

		if (formWin && !formWin.closed) formWin.focus();
		formobj.submit();
		formobj.target="_self";
		formobj.action="searchLocation.jsp";
		void(0);
	}

function setOrder(formObj,orderBy,orderType)
{
	var lorderType;
	if (orderType=="asc") {
		lorderType="desc";
	} else 	if (orderType=="desc") {
		lorderType="asc";
	} 
	 srchLocFlag = formObj.srchLocFlag.value;
	 gridFor=formObj.gridFor.value;
	 form=formObj.callingform.value;
	 locationFldName = formObj.locationFldName.value;
	 storagePKFldName = formObj.storagePKFldName.value;
	 coordXFldName = formObj.coordXFldName.value;
	 coordYFldName = formObj.coordYFldName.value;
	 page=formObj.page.value;
	formObj.action="searchLocation.jsp?page="+page+"&srchLocFlag="+srchLocFlag+"&gridFor="+gridFor+"&form="+form+"&locationName="+locationFldName+"&storagepk="+storagePKFldName+"&coordx="+coordXFldName+"&coordy="+coordYFldName+"&PKStorageID="+PKStorageID+"&orderBy="+orderBy+"&orderType="+lorderType;
	formObj.submit();
			
	
}
</script>
<jsp:include page="include.jsp" flush="true"/>
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<%
String dspecimenType= "";
String strCoordinateX = "";
String strCoordinateY = "";
String mainFKStorage = "";
String dstorageStatus = "";
String strgeName = "";
String pagenum 	         = request.getParameter("page");
String orderBy=request.getParameter("orderBy")==null?"":request.getParameter("orderBy");
String orderType=request.getParameter("orderType")==null?"":request.getParameter("orderType");
System.out.println("order by"+orderBy);
System.out.println("orderType  "+orderType);
String dstorageType = null;
String storageNme = request.getParameter("storageName")==null?"":request.getParameter("storageName");
String specimenTyp = request.getParameter("specimenType")==null?"":request.getParameter("specimenType");
String storageStat= request.getParameter("storageStatus")==null?"":request.getParameter("storageStatus");
String studyId = request.getParameter("selStudyIds")==null?"":request.getParameter("selStudyIds");
String spSrchLocFlag = request.getParameter("srchLocFlag")==null?"":request.getParameter("srchLocFlag");
String studyNumber = request.getParameter("selStudy")==null?"":request.getParameter("selStudy");
String formName = request.getParameter("form")==null?"":request.getParameter("form");
String locationFldName = request.getParameter("locationName")==null?"":request.getParameter("locationName");
String coordXFldName= request.getParameter("coordx")==null?"":request.getParameter("coordx");
String coordYFldName = request.getParameter("coordy")==null?"":request.getParameter("coordy");
String storagePKFldName = request.getParameter("storagepk")==null?"":request.getParameter("storagepk");
String storageTyp = request.getParameter("storageType")==null?"":request.getParameter("storageType");
String PKStorageID = request.getParameter("PKStorageID")==null?"":request.getParameter("PKStorageID");
String storageId = request.getParameter("storageId")==null?"":request.getParameter("storageId");
String pkStorage = request.getParameter("pkStorage");
String gridFor = request.getParameter("gridFor");
String storageview = request.getParameter("storageview")==null?"":request.getParameter("storageview");//flag for storage hierachy view
spSrchLocFlag = (spSrchLocFlag==null)?"":spSrchLocFlag;


if (StringUtil.isEmpty(gridFor))
	{
		gridFor = "ST";
	}

if (StringUtil.isEmpty(pkStorage)) 
{ 
	pkStorage = "0";
}
	
if(formName.equals("storageunit") && !pkStorage.equals("0")){
		storageB.setPkStorage(EJBUtil.stringToNum(pkStorage));
		storageB.getStorageDetails();
		strgeName = storageB.getStorageName();
		}

CodeDao cd = new CodeDao();

cd.getCodeValues("storage_stat");
if(storageStat.equals(""))
    {
	 dstorageStatus=cd.toPullDown("storageStatus");
    }
	 else
    {
	 dstorageStatus=cd.toPullDown("storageStatus",StringUtil.stringToNum(storageStat),true);
	 }

cd = new CodeDao();
cd.getCodeValues("specimen_type");
if (specimenTyp.equals(""))
    {
     dspecimenType=cd.toPullDown("specimenType");
    }
    else
    {   
     dspecimenType=cd.toPullDown("specimenType",StringUtil.stringToNum(specimenTyp),true);
    }  

cd = new CodeDao();
cd.getCodeValuesSaveKit("store_type");    
  if (StringUtil.isEmpty(storageTyp)) {
	     storageTyp = ""; 
	    }
    if (storageTyp.equals("")) {
    	
        dstorageType=cd.toPullDown("storageType");
    } 
    else 
   {
       dstorageType=cd.toPullDown("storageType",EJBUtil.stringToNum(storageTyp),true);
   }


  boolean isHierarchyView = false;
 
  
	

%>

<% if ("0".equals(pkStorage)) { %>
<title><%=LC.L_Search_StorageLocation%><%--Search Storage Location*****--%></title>
<% } else {
       isHierarchyView = true;
%>
<title><%=LC.L_Storage_HierarchyView%><%--Storage Hierarchy View*****--%></title>
<% }  %>
</head>
<script>
var isIe = jQuery.browser.msie;
if (isIe==true)
    document.write('<body style="overflow: auto">');
else
	document.write('<body style="overflow: visible">');
</script>

<form name="search" method="POST">

<table id="searchBar" width="100%" cellspacing="0" cellpadding="0" border="0"  class="basetbl outline midalign">

<tr>
	<td><%=LC.L_Storage_Id%><%--Storage ID*****--%></td><td><Input type = "text" name="storageID"  onkeypress="return handleKeyPress(event,document.search);" value = "<%=storageId%>" size=18 align = right>
	</td>
	<td><%=LC.L_Storage_Type%><%--Storage Type*****--%></td><td><%=dstorageType%>
	</td>
	<td><%=LC.L_Storage_Name%><%--Storage Name*****--%></td><td><Input type = "text" name="storageName" onkeypress="return handleKeyPress(event,document.search);" value = "<%=storageNme%>" size=18 align = right>
	</td>
</tr>	

<tr>
	<td><%=LC.L_Allowed_Specimens%><%--Allowed Specimens*****--%>:</td><td ><%=dspecimenType%></td>
	<td ><%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%>:</td>
	<td ><Input type = "text" name="selStudy"  value="<%=studyNumber%>" size=15 readonly >
	<A href="#" onClick="openStudyWindow(document.search)" ><%=LC.L_Select%><%--Select*****--%></A>
	<input type="hidden" name="selStudyIds" value="<%=studyId%>">
	</td>
    <td><%=LC.L_Status%><%--Status*****--%>:</td><td ><%=dstorageStatus%></td>
</tr>
		
<tr>
	<td>
		<Input type = "hidden" name="mainFKStorage" value = "<%=mainFKStorage%>" size=25 MAXLENGTH = 250 align = right>
    	<Input type = "hidden" name="strCoordinateX" value = "<%=strCoordinateX%>" size=25 MAXLENGTH = 250 align = right>
		<Input type = "hidden" name="strCoordinateY" value = "<%=strCoordinateY%>" size=25 MAXLENGTH = 250 align = right>
	</td>


<td colspan=4 align="right"> <input type="checkbox" name="showGridView" > <%=MC.M_Show_InGridView%><%--Show in Grid View*****--%>
</td>
<td><button type="button" class="custom-button" onclick="Functionpkstorage(); callAjaxGetStorages(document.search);"><%=LC.L_Search%></button></td>
	
</tr>
</table>

<% if (!"0".equals(pkStorage)) { %>
    <script>
    document.getElementById('searchBar').style.display="none";
    </script>
<% }  %>

<Input type = "hidden" name="gridFor"  value = "<%=gridFor%>" size=15 align = right>
<Input  id="textpath" type = "hidden" name="childspanname"  value = "" size=115 align = right>
<Input  type = "hidden" name="callingform"  value = "<%=formName%>" size=115 align = right>
<Input  type = "hidden" name="locationFldName"  value = "<%=locationFldName%>" size=115 align = right>
<Input  type = "hidden" name="coordXFldName"  value = "<%=coordXFldName%>" size=115 align = right>
<Input  type = "hidden" name="coordYFldName"  value = "<%=coordYFldName%>" size=115 align = right>
<Input  type = "hidden" name="storagePKFldName"  value = "<%=storagePKFldName%>" size=115 align = right>
<Input  type = "hidden" name="pkStorage"  value = "<%=pkStorage%>" size=115 align = right>
<Input  type = "hidden" name="PKStorageID"  value = "<%=PKStorageID%>" size=115 align = right>
<Input  type = "hidden" name="orderBy"  value = "<%=orderBy%>"  /> 
<Input  type = "hidden" name="orderType"  value = "<%=orderType%>"  /> 
<Input  type = "hidden" name="srchLocFlag" value="<%=spSrchLocFlag%>">
<Input  type = "hidden" name="page" value="<%=pagenum%>">
<Input  type = "hidden" name="storageview" value="<%=storageview%>">



<BR><BR>
<span ID="storage_path" ></span>
<% if(formName.equals("storageunit") && !pkStorage.equals("0")) { %>
<table class="tableDefault"><tr><th><%=strgeName%></th></tr></table>
<% } %>
<% if(!isHierarchyView) { %>
<div ID="span_storage" style="float:left;overflow:auto;width:100%;height:70%;" >

<div style="visibility: visible; left: 38%; top: 35%; z-index: 4;border:1px solid black;height: 8%;width:25%;position:absolute;">
        <div id="load" style="cursor: auto;">Please Wait...</div>
        <div style="margin-top:4%"><img class="asIsImage_progbar" src="../images/jpg/loading_pg.gif"></div>
</div>

</div>
<% }  %>

<span ID="span_storagechild" style="overflow:auto;float:left;width:100%;height:75%"></span>

</form>
<div>

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>
  <script>

  	callAjaxGetStorages(window.document.search);
  </script>
</body>

</html>
