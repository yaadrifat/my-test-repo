<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.eres.web.studyRights.StudyRightsJB"%>
<%@page import="com.velos.eres.service.util.EJBUtil"%>
<%@page import="com.velos.esch.web.protvisit.ProtVisitCoverageJB"%>
<%@page import="com.velos.esch.service.util.Rlog"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.json.JSONObject,org.json.JSONArray,org.json.JSONException,com.velos.eres.service.util.*"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
response.setDateHeader("Expires", 0);
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {
	ProtVisitCoverageJB pvCoverageJB = new ProtVisitCoverageJB();
	String protocolId = request.getParameter("protocolId");
	String calledFrom = request.getParameter("calledFrom");
	String calStatus=  request.getParameter("calstatus");
	String requestURL = StringUtil.encodeString(request.getRequestURL().toString());
	String queryString = StringUtil.encodeString(request.getQueryString());
	String eventName = StringUtil.htmlEncodeXss(request.getParameter("eventName"));
	eventName = eventName.trim();
    int pageRight = 0;
    int finDetRight = 0;
	if (calledFrom.equals("S")) {
		StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
		if ((stdRights.getFtrRights().size()) == 0){
			pageRight= 0;
			finDetRight = 0;
		} else {
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
			finDetRight=Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFIN"));
		}
	} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
		finDetRight = pageRight;
	}
	if (pageRight > 0) {
		String jsString = null;
		JSONObject jsObj = null;
		try {
		    jsString = pvCoverageJB.fetchCoverageJSON(protocolId, calledFrom, finDetRight,(String)tSession.getAttribute("accountId"),eventName);
			jsObj = new JSONObject(jsString);
		} catch(Exception e) {
		    Rlog.fatal("fetchCoverageExport", "Error while calling fetchCoverageJSON "+e);
		}
		if (jsObj == null) { jsObj = new JSONObject(); }
		String calName = (String)jsObj.get("protocolName");
		JSONArray colArray = (JSONArray)jsObj.get("colArray");
		JSONArray dataArray = (JSONArray)jsObj.get("dataArray");
		JSONArray covSubTypes = (JSONArray)jsObj.get("covSubTypes");
		JSONArray covPks = (JSONArray)jsObj.get("covPks");
		String myCoverageOption="";
		myCoverageOption+="<option value=\"\">"+"</option>";
		for (int iX=0; iX<covSubTypes.length(); iX++) {
			 String keySub = (String)((JSONObject)covSubTypes.get(iX)).get("key");
			Integer keyPk =(Integer)((JSONObject)covPks.get(iX)).get("key");
			 myCoverageOption+="<option value=\""+keyPk.intValue()+"\">"+keySub+"</option>";
		}
		String format = request.getParameter("format");
		String filename = null;
		String contentApp = null;		
		StringBuffer sb = new StringBuffer();
		

		ArrayList visitIdList = new ArrayList();
		    sb.append("<TABLE class='viewallcass'>");
		    if (colArray.length() > 0) {
		        sb.append("<tr>");
		       for (int iX=0; iX<colArray.length(); iX++) {
		            String key = (String)((JSONObject)colArray.get(iX)).get("key");
		            String hideExport = 
		            ((JSONObject)colArray.get(iX)).has("hideExport")?
		            		(String)((JSONObject)colArray.get(iX)).get("hideExport") : "false";

		            if ("eventId".equals(key)) { continue; }
		            if ("true".equals(hideExport)) { continue; }
		            
		            sb.append("<th class='yui-skin-sam' style='border-right: 1px solid gray;'>").append(((JSONObject)colArray.get(iX)).get("label"));
		            if (key.startsWith("v")) { 
		            	String visit=key.substring(1);
		            	String onChangeMethod="onChange=\""+"VELOS.coverageGrid.htmlSaveCoverageTypeByVisit("+visit+");"+"\"";
		            
		            	String covSelect="<select id=\""+"all_"+key+"\""+" name=\"all_"+key+"\""+onChangeMethod+">"+myCoverageOption+"</select>";
		            
		            	sb.append(covSelect);
		            	visitIdList.add(key); 
		            }
		            sb.append("</th>");
		        }
		        sb.append("</tr>");
		    }
		    int noteCounter = 0;
		    ArrayList notesList = new ArrayList();
		    if (dataArray.length() > 0) {
		    for (int iX=0; iX<dataArray.length(); iX++) {
		        JSONObject rowData = (JSONObject)dataArray.get(iX);
		        if ((iX%2)==0) {
		        	sb.append("<tr onclick = 'VELOS.coverageGrid.rowDialog("+ rowData.get("eventId")+")' class=\"browserEvenRow\">");
		      		}
		      		else{
		      	sb.append("<tr onclick = 'VELOS.coverageGrid.rowDialog("+ rowData.get("eventId")+")' class=\"browserOddRow\">");
		 	}
			String onChangeMethod="onChange=\""+"VELOS.coverageGrid.htmlSaveCoverageTypeByEvent("+rowData.get("eventId")+");"+"\"";
		        String covSelect="<span style=\""+"float:right\">"+"<select onmousedown=\"VELOS.coverageGrid.getEventDown()\" id=\""+"all_"+rowData.get("eventId")+"\""+" name=\"all_"+rowData.get("eventId")+"\""+onChangeMethod+">"+myCoverageOption+"</select></span>";
			    sb.append("<td style='border-right: 1px solid gray;'><span onmouseout='return nd();' onmouseover='return VELOS.coverageGrid.getEventMouseOver("+ rowData.get("eventId") +");'><p class='setwidth'>").append(rowData.get("event")).append(covSelect).append("</p></span></td>");
			    sb.append("<td style='border-right: 1px solid gray;' onmouseout='return nd();' onmouseover='return VELOS.coverageGrid.getEventMouseOver("+ rowData.get("eventId") +");'>").append(rowData.get("eventCPT")).append("</td>");
			    for (int iV=0; iV<visitIdList.size(); iV++) {
			        sb.append("<td style='border-right: 1px solid gray;' onmouseout='return nd();' onmouseover='return VELOS.coverageGrid.getEventMouseOver("+ rowData.get("eventId") +");'>");
			        sb.append("<span onmouseout='return nd();' onmouseover='return VELOS.coverageGrid.getEventMouseOver("+ rowData.get("eventId") +");'>");
			        String cellData = null;
			        try { cellData = (String)rowData.get((String)visitIdList.get(iV)); } catch(JSONException e) {}
			        if (cellData != null) {
			            if (!cellData.startsWith("<b>"+LC.L_X/*X*****/+"</b>")) { cellData = "<b>"+LC.L_X/*X*****/+"</b> "+cellData;}
			            sb.append(cellData);
			        }
			        String notesData = null;
			        try { notesData = (String)rowData.get("notes_"+visitIdList.get(iV)); } catch(JSONException e) {}
			        if (notesData != null && notesData.trim().length() > 0) {
			            sb.append(" <img class=\"headerImage\" align=\"bottom\" src=\"images/clip.jpg\"/><sup>").append(++noteCounter).append("</sup>");
			            notesList.add(notesData);
			        }
			        if (cellData == null && (notesData == null || notesData.trim().length() == 0)) {
			            sb.append("&nbsp;");
			        }
			        sb.append("</span></td>");
			    }
		        sb.append("<td style='border-right: 1px solid gray;'>").append(rowData.get("eventCovNotes")).append("</td>");
		        sb.append("</tr>");
		    }
		    sb.append("</TABLE>");
		    if (notesList.size() > 0) {
		        sb.append("<br/><Table border=\"0%\">");
		        sb.append("<tr><td colspan=\"2\"><img align=\"bottom\" src=\"images/clip.jpg\" width=\"17\" height=\"15\"/></td></tr>");
		        for (int iX=0; iX<notesList.size(); iX++) {
		            sb.append("<tr valign='top'><td align='left' colspan=\"2\">").append(iX+1).append(" &ndash; ");
		            sb.append((String)notesList.get(iX)).append("</td></tr>");
		        }
		        sb.append("</table>");
		    }
		    }
		    else{
		    	sb.append("<tr><td colspan="+visitIdList.size()+3+"> No Records Found</td></tr>");	
		    }

		out.println(sb.toString().trim());
%>
<input type=hidden id="calledFrom" name="calledFrom" value="<%=calledFrom%>"/>
<input type=hidden id="calstatus" name="calstatus" value="<%=calStatus%>"/>
<input type=hidden id="eventName" name="eventName" value="<%=eventName%>"/>
<input type=hidden id="protocolId" name="protocolId" value="<%=protocolId%>"/>

<%
	} // end of pageRight
	else {
	%>
		<jsp:include page="accessdenied.jsp" flush="true"/>
	<%
	} // end of else of pageRight
} else { // else of valid session
%>
<jsp:include page="timeout.html" flush="true"/>
<%
} // end of else of valid session
%>
<div> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>
<div class ="mainMenu" id = "emenu" > 
  <jsp:include page="getmenu.jsp" flush="true"/> 
</div>

