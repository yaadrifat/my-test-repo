
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss_skin.js"></SCRIPT>
<%
	String isPPortal = null;
	if ("Y".equals(request.getParameter("isPPortal"))) { isPPortal = "Y"; }
%>
<jsp:include page="sessionwarning.jsp" flush="true">
	<jsp:param name="isPPortal" value="<%=isPPortal%>" />
</jsp:include>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="codeLst" scope="session" class="com.velos.eres.web.codelst.CodelstJB"/>
<%@ page language = "java" import = "java.util.*,com.velos.eres.web.user.UserJB, com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB"%>


<%
HttpSession tSession = request.getSession(true);
tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))
	{
   UserJB userB1 = (UserJB) tSession.getValue("currentUser");
   int userId=userB1.getUserId(); 
//////////////////Added by IG to handle skin in the acc_name column er_account table
	String accName = (String) tSession.getValue("accName");
	String accId = (String) tSession.getValue("accountId");
	accName=(accName==null)?"default":accName;
	String accSkinId = "";
	String usersSkinId = "";
	String userSkin = "";
	String accSkin = "";
	String skin = "default";
	accSkinId = 	(String) tSession.getValue("accSkin");
//	userB1.setUserId(userId);
//	userB1.getUserDetails();
	usersSkinId = userB1.getUserSkin();
	userSkin= codeLst.getCodeCustomCol(EJBUtil.stringToNum(usersSkinId) );
	accSkin= codeLst.getCodeCustomCol(EJBUtil.stringToNum(accSkinId) );
	if (accSkin == null || accSkin.equals("") || accSkin.equals("default") ){
		accSkin = "accSkin";
	}
	else{
		skin = accSkin;
	}

	if (userSkin != null && !userSkin.equals("") ){
		skin = userSkin;
	}
	//	System.out.println("*******skinname...."+skin);
%>
<script> whichcss_skin("<%=skin%>");</script>
<%}%>

</body>
</html>
