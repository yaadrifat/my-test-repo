<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
	<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
	<jsp:useBean id="statusB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="portalB" scope="request" class="com.velos.eres.web.portal.PortalJB"/>
	<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.user.UserJB"%>
<%
	String src = null;
	src = request.getParameter("srcmenu");
	String eSign = request.getParameter("eSign");	
	String mode = request.getParameter("mode");
	String moduleTable = request.getParameter("moduleTable");
	String modulePk = request.getParameter("modulePk");
	String status = request.getParameter("status");
	String endStatusDate = request.getParameter("EndStatusDate");
	if(!endStatusDate.equals("")){
		endStatusDate+=" "+StatusHistoryDao.getCurrTime();
	}
	String portalStat=request.getParameter("portalStat");	
		
	String isCurrent=request.getParameter("isCurrent");
	int isCurrentStat=EJBUtil.stringToNum(isCurrent);
	String currentStatId=request.getParameter("currentStatId");

	


	if (mode.equals("M")) {
   	    status = request.getParameter("statCode");
	}
	
	ArrayList subTypeStatus =null;
	CodeDao cdao = new CodeDao();
	cdao.getCodeValuesById(EJBUtil.stringToNum(status));
	subTypeStatus = (cdao.getCSubType());
	String subTypeStat = subTypeStatus.get(0).toString();	
	portalStat = subTypeStat;
	
	/////
	
	
	System.out.println("portalStat" + portalStat);
	
        


	String statusDate = request.getParameter("statusDate")+" "+StatusHistoryDao.getCurrTime();
	String notes = request.getParameter("notes");		
	if (EJBUtil.isEmpty(mode))
	    mode = "N";
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession)) {
%>
	    <jsp:include page="sessionlogging.jsp" flush="true"/> 
<%   
   	String oldESign = (String) tSession.getValue("eSign");	
	if(!oldESign.equals(eSign)) {
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
			String ipAdd = (String) tSession.getValue("ipAdd");
			String usr = (String) tSession.getValue("userId");
			String accountId = (String) tSession.getValue("accountId");
			
			String statusId = request.getParameter("statusId");
			String auditUser = ""; 
			
			statusB.setStatusId(EJBUtil.stringToNum(statusId));
			
			statusB.getStatusHistoryDetails();
			if (mode.equals("N")) {
				//Modified By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.
				if (isCurrentStat==1){
				   	statusB.setIsCurrentStat("0");
					statusB.setModifiedBy(usr);
				}
				statusB.setStatusEndDate(endStatusDate);
				statusB.updateStatusHistoryWithEndDate();	
				if(!statusB.getModifiedBy().equals("")){
					statusB.setModifiedBy("");
				}

				if ( isCurrentStat == 1 ){
				
				portalB.setPortalId(EJBUtil.stringToNum(modulePk));
				portalB.getPortalDetails();
				portalB.setPortalStatus(portalStat);
				
				if (portalStat.equals("A")) //if status is Active
				{
				//get the audit user, if there is no audit user created, create an audut user for the portal
				
					auditUser = portalB.getPortalAuditUser();
					
					if (StringUtil.isEmpty(auditUser))
					{
						//create a new user
						
						UserJB ujb = new UserJB();
						
						ujb.setUserAccountId(accountId);
				        ujb.setUserLastName("Audit User");
				        ujb.setUserFirstName("Portal");
				        ujb.setUserSessionTime("15");
				        ujb.setUserStatus("A");
				        ujb.setCreator(usr);
				        ujb.setIpAdd(ipAdd);
				        ujb.setUserSiteFlag("0");
				        ujb.setUserType("P");
	        			ujb.setUserDetails();
	     				
	     				auditUser = String.valueOf(ujb.getUserId());
	     				portalB.setPortalAuditUser(auditUser); 
						
					}
						
				}

				portalB.setModifiedBy(usr); //Added By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.
				portalB.updatePortal();
				
				
				}
				
				
			}
			statusB.setIsCurrentStat(isCurrent);
			statusB.setStatusModuleId(modulePk);
			statusB.setStatusModuleTable(moduleTable);
			statusB.setStatusCodelstId(status);
			statusB.setStatusStartDate(statusDate);
			statusB.setStatusNotes(notes);
			statusB.setIpAdd(ipAdd);
			if (mode.equals("N")) {
				statusB.setCreator(usr);
				statusB.setStatusEndDate("");
				statusB.setStatusHistoryDetailsWithEndDate();			
			} else if (mode.equals("M")) {
				statusB.setRecordType("M");	
				statusB.setModifiedBy(usr);
				statusB.updateStatusHistoryWithEndDate();
				
				
				/*if ((isCurrentStat==1) && (!statusId.equals(currentStatId))) {
					statusB.setStatusId(EJBUtil.stringToNum(currentStatId));
						statusB.getStatusHistoryDetails();
						statusB.setIsCurrentStat("0");
						statusB.updateStatusHistoryWithEndDate();
						portalB.setPortalId(EJBUtil.stringToNum(modulePk));
						portalB.getPortalDetails();
						portalB.setPortalStatus(portalStat);
						portalB.updatePortal();
				}*/
				
								
			}	
%>
			  <br><br><br><br><br>
				 <p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
			  <script>
					window.opener.location.reload();
					setTimeout("self.close()",1000);
			  </script>	  
		<%
		}//end of if for eSign check
	}//end of if body for session
	else {
%>
	  <jsp:include page="timeout.html" flush="true"/>
<%
	}
%>
</BODY>
</HTML>





