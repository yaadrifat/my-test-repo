<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.LC"%>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<title><%=MC.M_Del_StdTreatArm%><%--Delete <%=LC.L_Study%> Treatment Arm*****--%></title>
<jsp:include page="popupJS.js" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

<%-- 	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   } --%>
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.EJBUtil"%>
<jsp:useBean id="studytxarmJB" scope="request" class="com.velos.eres.web.studyTXArm.StudyTXArmJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:include page="include.jsp" flush="true"/>
<BODY>
<br>

<DIV class="popDefault" id="div1">
<%
	String studytxArmId= "";


HttpSession tSession = request.getSession(true);

 if (sessionmaint.isValidSession(tSession))	{
		studytxArmId= request.getParameter("studytxArmId");

		int ret=0;
		String delMode=request.getParameter("delMode");

		if (EJBUtil.isEmpty(delMode)) {
			delMode="final";
%>

	<FORM name="studytxarmdelete" id="stdtxarm" method="post" action="studytxarmdelete.jsp" onSubmit="if (validate(document.studytxarmdelete)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

	<P class="defComments"><%=MC.M_Etr_Esign_ToProc%><%--Please enter e-Signature to proceed*****--%></P>


	<jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="stdtxarm"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>


 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="studytxArmId" value="<%=studytxArmId%>">

	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
 		  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
			} else {
			studytxarmJB.setStudyTXArmId(Integer.parseInt(studytxArmId));
			studytxarmJB.getStudyTXArmDetails();
			
			//Modified for INF-18183 ::: Akshi
			ret = studytxarmJB.removeStudyTXArm(AuditUtils.createArgs(session,"",LC.L_Study));     //Akshi: Fixed for Bug #7604
			%>
				<br><br><br><br><br><br><br>
			<TABLE width="550" border = "0">
			 <tr>
				<td align="center">
				<%

			if (ret == -1) {%>
			 <p class = "successfulmsg"> <%=MC.M_DataCnt_DelSucc%><%--Data not could not be deleted successfully*****--%> </p>
			<%}
			else { %>
			 <p class = "successfulmsg"> <%=MC.M_Data_DelSucc%><%--Data deleted successfully*****--%> </p>
			<%}
			%>

				</td>
			   </tr>
			 </table>
				<% if (ret >= 0)
					{%>
				  <script>
						window.opener.location.reload();
						setTimeout("self.close()",1000);
				  </script>
					<%
					} // end of if status got deleted
				  else
					 {
					 	%>
					 	<TABLE width="550" border = "0">
			 			<tr>
							<td align="center">
								<button onClick="window.self.close();"><%=LC.L_Close%></button>
							</td>
			   			</tr>
			 			</table>
						<%

					 }


			} //end esign
	} //end of delMode
  }//end of if body for session
else { %>
 <jsp:include page="timeout_childwindow.jsp" flush="true"/>
 <% } %>


</DIV>

<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</HTML>


