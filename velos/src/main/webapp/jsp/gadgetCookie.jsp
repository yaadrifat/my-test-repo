<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList,java.util.HashMap,org.json.*"%>
<%@page import="com.velos.eres.widget.business.common.UIGadgetDao,com.velos.eres.service.util.StringUtil" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%!
static final String COOKIE = "cookie";
static final String SETTINGS = "settings";
static final String TYPE = "type";
static final String USER_ID = "userId";
static final String TYPE_GET_COOKIE = "typeGetCookie";
static final String TYPE_SAVE_COOKIE = "typeSaveCookie";
static final String TYPE_GET_SETTINGS = "typeGetSettings";
static final String TYPE_SAVE_SETTINGS = "typeSaveSettings";
%>
<%
HttpSession tSession = request.getSession(true); 
if (!sessionmaint.isValidSession(tSession)) {
	return;
}
UIGadgetDao uiGadgetDao = new UIGadgetDao();
String userId = (String)tSession.getAttribute(USER_ID);
if (TYPE_GET_COOKIE.equals(request.getParameter(TYPE))) {
	out.println(uiGadgetDao.getGadgetInstanceCookie(userId));
	return;
}
if (TYPE_GET_SETTINGS.equals(request.getParameter(TYPE))) {
	out.println(uiGadgetDao.getGadgetInstanceSettings(userId));
	return;
}
if (TYPE_SAVE_SETTINGS.equals(request.getParameter(TYPE))) {
	String settings = request.getParameter(SETTINGS);
	HashMap<String, String> hash = new HashMap<String, String>();
	hash.put(UIGadgetDao.GADGET_SETTINGS, settings);
	uiGadgetDao.setGadgetInstanceSettings(userId, hash);
	return;
}
if (TYPE_SAVE_COOKIE.equals(request.getParameter(TYPE))) {
	String cookie = request.getParameter(COOKIE);
	if (cookie == null || cookie.length() < 1) {
		return;
	}
	HashMap<String, String> hash = new HashMap<String, String>();
	hash.put(UIGadgetDao.GADGET_COOKIE, StringUtil.decodeString(cookie));
	uiGadgetDao.setGadgetInstanceCookie(userId, hash);
	return;
}
%>
