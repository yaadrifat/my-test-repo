<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Mstone_Rpt%><%--Milestone Report*****--%></title>

<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="savedRepB" scope="request" class="com.velos.eres.web.savedRep.SavedRepJB"/>

<body>

<%@page language="java" import="com.velos.eres.business.common.SavedRepDao,com.velos.eres.service.util.EJBUtil"%> <%@page import="com.velos.eres.service.util.LC"%>



<%
int savedRepId = EJBUtil.stringToNum(request.getParameter("savedRepId"));
String repHtml = "";
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	SavedRepDao sDao = savedRepB.getReportContent(savedRepId);
	repHtml = sDao.getReportContent(savedRepId);
	out.println(repHtml);
} else {
%>
	<jsp:include page="timeout.htm" flush="true"/> 

<%}
%>

</body>
</html>
