<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<html>
<head>
	<%
	String dispPage=request.getParameter("page");
	if (dispPage == null) dispPage = "";

	if (dispPage.equals("patient")){
	%>
	<title><%=LC.L_Patient_Labs%><%--<%=LC.Pat_Patient%> Labs*****--%></title>
	<%}else {%>
	<title><%=LC.L_Specimen_Labs%><%--Specimen Labs*****--%></title>
	<%}%>


<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="studyProt" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="labJB" scope="request" class="com.velos.eres.web.lab.LabJB"/>

</head>


<SCRIPT LANGUAGE="JavaScript">
window.name = "main";
var isIe = jQuery.browser.msie;
	
function openwindow(val){

windowname=window.open("Pcategory.jsp?from=lookup"+val,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100 0, ");
windowname.focus();

}
function openlookup(){
windowname=window.open("getlookup.jsp?viewId=1&form=patlab&dfilter=&keyword=labname|category~testId|grade" ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100 0, ");
windowname.focus();
}
function openwindowcalc(formobj,nciVersion,pkey,count)
{
	 totcount=formobj.arraylen.value;
	 if (totcount>1){
	 url="&parentform=patlab&gradefld=grade[" +count+"]&advnamefld=advName["+count+"]&dispfield=toxicity["+count+"]";
	 //url="&parentform=patlab&gradefld=grade&advnamefld=advName&dispfield=toxicity";

	  }else{
	  url="&parentform=patlab&gradefld=grade&advnamefld=advName&dispfield=toxicity";
	 }

	windowname=window.open("getAdvToxicity.jsp?nciVersion=" + nciVersion + "&pkey=" + pkey + url ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=500,height=350 top=120,left=200 0, ");
	//windowname=window.open("getAdvToxicity.jsp?nciVersion=" + nciVersion ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=500,height=350 top=120,left=200 0, ");
	windowname.focus();

}
function opendetail(formobj,count){
	status=formobj.tstatus.value;
	totcount=formobj.arraylen.value;
	if (totcount>1){
	lln=formobj.LLN[count].value;
	uln=formobj.ULN[count].value;
	accnum=formobj.accnumber[count].value;
	//notes=formobj.notes[count].value;

	} else if(totcount==1){
	lln=formobj.LLN.value;
	uln=formobj.ULN.value;
	accnum=formobj.accnumber.value;
	//notes=formobj.notes.value;
	}

	//notes=encodeString(notes);
	//formobj.hidenotes.value=notes;
	windowname=window.open("labdetails.jsp?status=" + status+"&lln="+lln+"&uln="+uln+"&accnum="+accnum+"&count="+count+"&totcount="+totcount,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=600,height=350 top=120,left=200 0, ");
	windowname.focus();
}
function opentextresult(formobj,count){
	totcount=formobj.arraylen.value;
	if (totcount>1){
	lresult=formobj.longresult[count].value;
	}else{
	 lresult=formobj.longresult.value;
	}
	//lresult=encodeString(lresult);
	formobj.hidelongresult.value=lresult;
	windowname=window.open("labtextresult.jsp?count="+count+"&totcount="+totcount  ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=600,height=350 top=120,left=200 0, ");
	windowname.focus();



}


function openLabSelect(formobj){
	groupId=formobj.groupName.value;
	mode=formobj.mode.value;
	tab=formobj.selectedTab.value;
	src=formobj.srcmenu.value;
	pkey=formobj.pkey.value;
	patprot=formobj.patProtId.value;
	page=formobj.page.value;
	if (formobj.groupName.value.length=0) {
	alert("<%=MC.M_Selc_Cat%>");/*alet("Please select a category");*****/
	return;
	}
	windowname=window.open("labSelection.jsp?groupName=" +groupId+"&mode=initial&pkey="+pkey+"&patProtId="+patprot+"&srcmenu="+src+"&selectedTab="+tab+"&page="+page ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=870,height=600 top=40,left=50 0, ");
	windowname.focus();

}
function setAllValues(formobj,totcount){
	date=formobj.date.value;
	
	abnormalResult=document.getElementById("selectAllAbnresult").value;
	stdPhase=document.getElementById("selectAllstdPhase").value;
	var labdates="";
	
	if(date.length==0 ){
		if (totcount>1){
		for(k=0;k<totcount;k++){
			labdates=formobj.labdate[k].value;
			if(labdates==""){
			paramArray = ["<%=LC.L_Date%>"];
			alert(getLocalizedMessageString("M_Select_Val_First",paramArray));
			return ;
		}
	}}else{
		
		var labdate=formobj.labdate.value;
		if(labdates==""){
			paramArray = ["<%=LC.L_Date%>"];
			alert(getLocalizedMessageString("M_Select_Val_First",paramArray));
			return ;
		}
	}
	}
	if (totcount>1){
	for(i=0;i<totcount;i++){
	if(formobj.date.value!=''){
	formobj.labdate[i].value=formobj.date.value;
	}
	formobj.abnresult[i].value=abnormalResult;
	formobj.stdPhase[i].value=stdPhase;
	}
	}else{
	if(formobj.date.value!=''){
	formobj.labdate.value=formobj.date.value;
	}
	formobj.abnresult.value=abnormalResult;
	formobj.stdPhase.value=stdPhase;
	}
	
}


function validate(formobj){
 formobj.mode.value="N";
 count=formobj.arraylen.value;
 totcount=formobj.arraylen.value;
if (!(validate_col('e-Signature',formobj.eSign))) return false
<%-- if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   } --%>
 if (totcount>1){
 for(i=0;i<count;i++){

	if (formobj.labdate[i].value.length==0){
	var paramArray = [(i+1)];
	alert(getLocalizedMessageString("M_EtrVldDt_ResRow",paramArray));/*alert("Please enter a Valid Date for Lab Result row# "+ (i+1));*****/
	formobj.labdate[i].focus();
	return false;
	}

   if(isNaN(formobj.tresult[i].value) == true) {
    var paramArray = [(i+1)];
    alert(getLocalizedMessageString("M_InvdRes_PlsLongData",paramArray));/*alert("Invalid Test Results for Lab Result row# "+ (i+1) + ".Please use Long Test results for alpha-numeric data.");*****/
	formobj.tresult[i].focus();
	return false;
   }
 if (formobj.tresult[i].value.length>0){
 	if (formobj.longresult[i].value.length>0){
 	var paramArray = [(i+1)];
 	alert(getLocalizedMessageString("M_TestRsltLong_LabRsltRow",paramArray));/*alert("Test Results entry not allowed in 'Test Results' and 'Long Test Results' simultaneously. Please use one of them for test results in Lab Result row# "+(i+1)+".");*****/
	formobj.tresult[i].focus();
	return false;
	}
}

 if (formobj.tresult[i].value.length>0)
  {
          formobj.testtype[i].value="NM";
	  }
      else {
      formobj.testtype[i].value="TX";
      }
}
}else {
  if (formobj.labdate.value.length==0){
  alert("<%=MC.M_Etr_ValidDt%>");/*alert("Please enter a Valid Date");*****/
  formobj.labdate.focus();
  return false;
  }
  if(isNaN(formobj.tresult.value) == true) {
	alert("<%=MC.M_InvalidTestRes_UseLngANum%>");/*alert("Invalid Test Results.Please use Long Test results for alpha-numeric data.");*****/
	formobj.tresult.focus();
	return false;
   }
 if (formobj.tresult.value.length>0){
 	if (formobj.longresult.value.length>0){
	alert("<%=MC.M_ResultsEntry_NotAlw%> ");/*alert("Test Results entry not allowed in 'Test Results' and 'Long Test Results' simultaneously. Please use one of them for results. ");*****/
	formobj.tresult.focus();
	return false;
	}
}
if (formobj.tresult.value.length>0)
  {
          formobj.testtype.value="NM";
	  }
      else {
      formobj.testtype.value="TX";
      }

} //end of totcount>1

 }



</script>
<% String src="",groupNamePullDown="",mode = "",unitStr="",defaultStr="";
ArrayList selectedLab=null;
boolean defaultLoaded=false;
ArrayList tempUnits=new ArrayList(),
	  defaultUnits=new ArrayList();%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<%
HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
	{
  			int personPK = 0,len=0,strlen=0,firstpos=0,secondpos=0,thirdpos=0,pos=0,tempValLen=0;
  			String patientId = "",groupId="";
			String studyDD= "";
			String dob = "";
			String gender = "";
			String genderId = "";
			String yob = "";
			String age = "",temp="",labName="",labpk="",catName="",catId="",tempval="",unit="",nciId="",gradeCalc="",stdPhaseDD="";
			ArrayList labUnits=null;
			ArrayList arStudyIds=new ArrayList();
			ArrayList arStudyNums=new ArrayList();
			mode=request.getParameter("mode");
			LabDao labdao=new LabDao();
			labdao=labJB.groupPullDown(groupId);
			groupNamePullDown=labdao.getPullDown();
			Calendar cal1 = new GregorianCalendar();
			String abnresult="",tstatus="";
			personPK = EJBUtil.stringToNum(request.getParameter("pkey"));
			String pkey = request.getParameter("pkey");
			person.setPersonPKId(personPK);
		 	person.getPersonDetails();
			patientId = person.getPersonPId();
			genderId = person.getPersonGender();
			dob = person.getPersonDob();
			gender = codeLst.getCodeDescription(EJBUtil.stringToNum(genderId));
			if (gender==null){ gender=""; }

		// modified by Gopu on 18th March for fixing the bug No.2063

			if(!(dob== null) && !(dob.equals(""))){
				yob = dob.substring(6,10);
				age = (new Integer(cal1.get(Calendar.YEAR) - EJBUtil.stringToNum(yob))).toString();
			} else {
				age = "Unknown";
			}
			src= request.getParameter("srcmenu");
			System.out.println("src" + src);
			String selectedTab = request.getParameter("selectedTab");

			if (mode.equals("select")){
			String select[]=request.getParameterValues("select");
			String unitTest[]=request.getParameterValues("testId");
			selectedLab=EJBUtil.strArrToArrayList(select);
			labdao=labJB.retLabUnit(unitTest);
			labUnits=labdao.getLabUnits();
			if (labUnits!=null) System.out.println("length of unitsa"+labUnits.size()); else
			System.out.println("nothing in here");
			}
			if (selectedLab!=null) len=selectedLab.size();


	String uName = (String) tSession.getValue("userName");
	String userIdFromSession = (String) tSession.getValue("userId");
	String acc = (String) tSession.getValue("accountId");
	String patientCode = request.getParameter("patientCode");
	String patProtId=request.getParameter("patProtId");
	patProtId=(patProtId.equals("null"))?"":patProtId;
	patProtId=(patProtId==null)?"":patProtId;
	String fromLab=request.getParameter("fromlab");
	String FromPatPage = request.getParameter("FromPatPage");
	if(FromPatPage==null){
		FromPatPage="";
	}
	String patientCode1=request.getParameter("patientCode");
	if(patientCode1==null){
		patientCode1="";
	}

	fromLab=(fromLab==null)?"":fromLab;
	String pages=request.getParameter("page");
	if (pages == null)
		pages = "";

	PatStudyStatDao stDao=studyB.getPatientStudies(personPK,EJBUtil.stringToNum(userIdFromSession));
	arStudyIds=stDao.getStudyIds();
	arStudyNums=stDao.getStudyNums();

	//String patientName = request.getParameter("patientName");
	String studyId = request.getParameter("studyId");
	if(studyId == null || studyId.equals("null")){
		studyId = (String) tSession.getValue("studyId");
	} else {

		tSession.putValue("studyId",studyId);

	}
	studyId=(studyId==null)?"":studyId;

	studyDD = EJBUtil.createPullDown("selstudyId", EJBUtil.stringToNum(studyId), arStudyIds , arStudyNums );



	/*String studyTitle = "";

	String studyNumber = "";



	String estatDesc="";

	estatDesc = request.getParameter("statDesc");



    String estatid="";

	estatid = request.getParameter("statid");



	studyB.setId(EJBUtil.stringToNum(studyId));



      studyB.getStudyDetails();

      studyTitle = studyB.getStudyTitle();

	studyNumber = studyB.getStudyNumber();*/
	CodeDao cdLabs = new CodeDao();
	CodeDao cdLabs1=new CodeDao();
	CodeDao cdLabs2= new CodeDao();
	cdLabs.getCodeValues("abflag");
	abnresult=cdLabs.toPullDown("abnresult");
	cdLabs1.getCodeValues("tststat");
	tstatus=cdLabs1.toPullDown("tstatus");
	cdLabs2.getCodeValues("stdphase");
	stdPhaseDD=cdLabs2.toPullDown("stdPhase");

	//JM: 07Jul2009: #INVP2.11
	String specimenPk=request.getParameter("specimenPk");
		specimenPk=(specimenPk==null)?"":specimenPk;


		String calldFrom=request.getParameter("calledFromPg");
		calldFrom=(calldFrom==null)?"":calldFrom;


%>


<body>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<%

if ( calldFrom.equals("specimen")){%>
<% if(FromPatPage.equals("patient")  || FromPatPage.equals("patientEnroll")){%>

<jsp:include page="patienttabs.jsp" flush="true">
<jsp:param name="selectedTab" value="12"/>
<jsp:param name="page" value="<%=FromPatPage%>"/>
<jsp:param name="page" value="<%=patientCode%>"/>
<jsp:param name="pkey" value="<%=patientId%>"/>
</jsp:include>
<%}else{ %>
<!--JM: 07Jul2009: #INVP2.11-->
<DIV class="browserDefault" id="div1">
<table width="99%" cellspacing="0" cellpadding="0" border="0" >

		<tr>
		<td colspan = "4">
		<P class="sectionHeadings"> <%=MC.M_MngInv_SpmenLabs%><%--Manage Inventory >> Specimen >> Labs*****--%><BR> </P>
		</td></tr>
		<tr><td colspan = "4">
		<jsp:include page="inventorytabs.jsp" flush="true">
			<jsp:param name="selectedTab" value="1"/>
			<jsp:param name="specimenPk" value="<%=specimenPk%>" />
			</jsp:include>
		</td>
		</tr>
	</table>
</div>
<%} %>
<!--JM: 07Jul2009: #INVP2.11-->
<%}else{%>

<DIV class="BrowserTopn"  id="div1">
<jsp:include page="patienttabs.jsp" flush="true">
		<jsp:param name="pkey" value="<%=pkey%>"/>
		<jsp:param name="patientCode" value="<%=patientCode%>"/>
</jsp:include>

</div>
<%}%>
<SCRIPT LANGUAGE="JavaScript">
	if(isIe)
		document.write('<DIV class="BrowserBotN"  id="div2" style="top:146px;height:65%">');
	else
		document.write('<DIV class="BrowserBotN"  id="div2" style="top:146px;height:70%">');
</SCRIPT>

<form name="patlab" id="patlabdata" method="post"  action="updatelab.jsp" onSubmit="if (validate(document.patlab)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<input type="hidden" name="pkey" value="<%=pkey%>">
<input type="hidden" name="patProtId" value=<%=patProtId%>>
<input type="hidden" name="srcmenu" value=<%=src%>>
<input type="hidden" name="mode" value="<%=mode%>">
<input type="hidden" name="selectedTab" value="<%=selectedTab%>">
<input type="hidden" name="statusDD" value="<%=tstatus%>">
<input type="hidden" name="studyId" value="<%=studyId%>">
<input type="hidden" name="hidelongresult" value="">
<input type="hidden" name="hidenotes" value="">
<input type="hidden" name="page" value="<%=pages%>">
<input type="hidden" name="fromlab" value="<%=fromLab%>">
<input name="arraylen" type="hidden" value="<%=len%>">
<input name="patientCode" type="hidden" value="<%=patientCode%>">

<input type="hidden" name="specimenPk" value="<%=specimenPk%>">
<input type="hidden" name="calledFromPg" value="<%=calldFrom%>">
<input type="hidden" name="FromPatPage" value="<%=FromPatPage%>">
<input type="hidden" name="patientCode" value="<%=patientCode1%>">




<%

 if (mode.equals("initial")){

%>

<table><tr><td><%=groupNamePullDown%></td><td><button type="submit" onClick="openLabSelect(document.patlab)"><%=LC.L_Search%></button></td></tr></table>

<%}else{%>
<%-- INF-20084 Datepicker-- AGodara --%>
<table width="100%">
	<tr height="8"><td></td></tr>
	<tr>
	<td width="5%" align="center"><b><%=LC.L_Select_Date%><%--Select Date*****--%></b></td>
	<td width="5%"><Input type="text" name="date" size="8" readonly class="datefield"></td>
	<td align="center" width="10%"><b><%=LC.L_Abnormal_Res%><%--Abnormal Result*****--%></b></td>
	<td width="10%"><%=abnresult.replace("abnresult","selectAllAbnresult")%></td>
	<td align="center" width="10%"><b><%=LC.L_Std_Phase%><%--Study Phase*****--%></b></td>
	<td width="10%"><%=stdPhaseDD.replace("stdPhase","selectAllstdPhase")%></td>
	<td width="40%">
	&nbsp;<% String Hyper_Link = "<A href=# onClick=\"setAllValues(document.patlab,"+len+")\"> "+LC.L_Click_Here_Lower+"</A>"; Object[] arguments = {Hyper_Link}; %>
	<%=VelosResourceBundle.getMessageString("M_ClkToSetDt_LabRec",arguments) %><%--and&nbsp;<A href=# onClick="setAllValues(document.patlab,<%=len%>)"> click here</A> to set this Date for all Lab records below*****--%>
</td>
<td width="25%" align="center"><%=MC.M_Lnk_RecToStd%></td>
<td width="5%"><%--Link records to Study*****--%><%=studyDD%></td>
</tr>
	<tr height="8"><td></td></tr>
	</table>
<table width="100%">
<tr><th width="10%"><%=LC.L_Date%><%--Date*****--%> <FONT class="Mandatory">* </FONT></td>
<th width="15%"><%=LC.L_Lab_Name%><%--Lab Name*****--%> <FONT class="Mandatory">* </FONT></td>
<th width="10%"><%=LC.L_Test_Result%><%--Test Result*****--%> </td><th width="10%"><%=LC.L_Units%><%--Units*****--%> </td><th width="10%"><%=LC.L_Abnormal_Res%><%--Abnormal Result*****--%></td>
<th width="10%"><%=LC.L_Std_Phase%><%--Study Phase*****--%></th>
<th width="20%"><%=LC.L_Notes%></th>
<th width="5%"></th>
</tr>


<%for(int i=0;i<len;i++){%>
<tr>
<%-- INF-20084 Datepicker-- AGodara --%>
		<td width="10%" align="center"><input type="text" name="labdate" size="10" readonly class="datefield"></td>
		<%    temp=StringUtil.decodeString(((String)selectedLab.get(i)));
			StringTokenizer st = new StringTokenizer(temp,";");
			 /*strlen = (temp).length();
			 firstpos = (temp).indexOf(";",1);
			 labpk = (temp).substring(0,firstpos);
			 secondpos = (temp).indexOf(";",firstpos+1);
			 labName = (temp).substring(firstpos+1,secondpos);
			 thirdpos=(temp).indexOf(";",secondpos+1);
			 catName = (temp).substring(secondpos+1,thirdpos);
			 catId=(temp).substring(thirdpos+1,strlen);*/
			 labpk=st.nextToken();
			 labName=st.nextToken();
			 catName=st.nextToken();
			 catId=st.nextToken();
			 nciId=st.nextToken();
			 gradeCalc=st.nextToken();
		if (nciId.equals("-"))  nciId="";
		if (gradeCalc.equals("-")) gradeCalc="";

						 %>
<td width="15%" align="center">
<input type="text" name="labname" size="30" value="<%=labName%>" readonly>
<input type="hidden" name="labpk" value="<%=labpk%>">
<input type="hidden" name="testcatId" size="15" value="<%=catId%>">
<input type="hidden" name="nciId" size="15" value="<%=nciId%>">
<input type="hidden" name="gradeCalc" size="15" value="<%=gradeCalc%>">

<!--<A href=# onClick="openlookup();">Lookup</A>-->
</td>
<% if (labUnits!=null){

	tempUnits.clear();
	for (int k=0;k<labUnits.size();k++){
         tempval=(String)labUnits.get(k);
	  tempValLen=tempval.length();
	 pos=tempval.indexOf(";",1);
	 if (tempval.substring(0,pos).equals(labpk))
		 tempUnits.add(tempval.substring(pos+1,tempValLen));
	if ((defaultLoaded)){} else {
	if (tempval.substring(0,pos).equals("0")) {
		 defaultUnits.add(tempval.substring(pos+1,tempValLen));

	}
	}
  }
   if ((tempUnits.size())>0)
   unitStr=EJBUtil.createPullDownWithStr("unit", unit, tempUnits, tempUnits);
   else if  (defaultUnits.size()>0) {
   if (defaultLoaded){}
   else {
   defaultStr=EJBUtil.createPullDownWithStr("unit", unit, defaultUnits, defaultUnits);

   defaultLoaded=true;

   }
   unitStr=defaultStr;
   }


}



%>

<td width="10%" align="center"><input type="text" name="tresult" size="10">
<A href="javascript:void(0);" onmouseover="return overlib('<%=LC.L_Long_Results%><%--Long Results*****--%>');"  onClick="opentextresult(document.patlab,<%=i%>)" onmouseout="return nd();"><%=LC.L_L%><%--L*****--%></A></td>
<!--<input type="text" name="unit" size="3">-->
<td width="10%"><%=unitStr%></td>

<!--<td colspan="8" ><TEXTAREA name="longresult" cols="60" rows="4"></TEXTAREA></td>-->
<td width="10%"><%=abnresult%><!--<input type="text" name="abnresult" size="15">--></td>
<td width="10%">
<%=stdPhaseDD%>
<!--<input type="text" name="toxicity" size="10"><A href=# onClick="openwindowcalc(document.patlab,'2.0',<%=pkey%>,<%=i%>);">Cal</A>-->
</td>
<td width="20%"><input type="text" name="notes" size="40"></td>
<td width="5%">

<A href="javascript:void(0);"  onmouseover="return overlib('<%=LC.L_More%><%--More*****--%>');"  onClick="opendetail(document.patlab,<%=i%>)" onmouseout="return nd();"><%=LC.L_M%><%--M*****--%></A>
</td>

<input type = "hidden" maxLength="30" size="15" name="grade" value= "" >
<input type = "hidden" maxLength="200" size="15" name="advName" value= "" >
<!-- <input type="hidden" name="notes" value=""> -->
<input type="hidden" name="longresult" value="">
<input type="hidden" name="LLN" size="3"><input type="hidden" name="ULN" size="3">
<input type="hidden" name="accnumber" size="8">
<input type="hidden" name="tstatus" value="">
<input type="hidden" name="testtype" value="">
<!--<td colspan="8"><TEXTAREA name="notes" cols="60" rows="4"></TEXTAREA>-->
</td></tr><%}%>

</table>
<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="patlabdata"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
<%}%>

</form>

	</DIV>
<DIV class="mainMenu" id = "emenu">

  <jsp:include page="getmenu.jsp" flush="true"/>

</DIV>



		<%}
	else { %>

<jsp:include page="timeout.html" flush="true"/>

<%

}

%>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</body>
</html>

