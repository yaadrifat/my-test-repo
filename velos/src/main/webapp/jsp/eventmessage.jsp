<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=MC.M_EvtLib_EvtMsg%><%-- Event Library >> Event Message*****--%></title>


<!-- Virendra: Fixed Bug no. 4736, added import com.velos.eres.service.util.StringUtil  -->
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.StringUtil,com.velos.esch.business.common.SchCodeDao" %>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



<%
	//SV, 11/05, added eventbrowser to the list
	if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt") || (request.getParameter("fromPage")).equals("eventbrowser"))

{ %>

 <SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>

 <% } //end for if ((request.getParameter("fromPage")).equals("selectEvent"))
	%>

<SCRIPT language="JavaScript1.1">

function openUserWindow(formobj,pgRight) {

	if (f_check_perm(pgRight,'E') == true) {

		leventId = formobj.eventId.value;

		luserType = formobj.userType.value;

		lsrcmenu = formobj.srcmenu.value;

		lduration = formobj.duration.value;

		lprotocolId = formobj.protocolId.value;

		lcalledFrom = formobj.calledFrom.value;

		lmode = formobj.mode.value;

		lfromPage = formobj.fromPage.value;

		lcalStatus = formobj.calStatus.value;

		var leventName = formobj.eventName.value;


   // 	windowName=window.open("eventusersearch.jsp?&eventId="+leventId+"&userType="+luserType+"&srcmenu="+lsrcmenu+"&duration="+lduration+"&protocolId="+lprotocolId+"&calledFrom="+lcalledFrom+"&mode="+lmode+"&fromPage="+lfromPage,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");

   //   windowName.focus();



      var windowName = "addeventuser.jsp?budgetId=&eventId="+leventId+"&userType="+luserType+"&srcmenu="+lsrcmenu+"&duration="+lduration+"&protocolId="+lprotocolId+"&calledFrom="+lcalledFrom+"&mode="+lmode+"&fromPage="+lfromPage
      windowName += "&eventName="+leventName;
      window.open(windowName,"SelectUser","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")

      //windowName.focus();



	}else {

		return false;

	}

}



function openUserWindowfordeletion(formobj,pgRight) {

	if (f_check_perm(pgRight,'E') == true) {

		leventId = formobj.eventId.value;

		luserType = formobj.userType.value;

		lsrcmenu = formobj.srcmenu.value;

		lduration = formobj.duration.value;

		lprotocolId = formobj.protocolId.value;

		lcalledFrom = formobj.calledFrom.value;

		lmode = formobj.mode.value;

		lfromPage = formobj.fromPage.value;

		lcalStatus = formobj.calStatus.value;
		leventName = formobj.eventName.value;


	    windowName=window.open("eventmessageusers.jsp?&eventId="+leventId+"&userType="+luserType+"&srcmenu="+lsrcmenu+"&duration="+lduration+"&protocolId="+lprotocolId+"&calledFrom="+lcalledFrom+"&mode="+lmode+"&fromPage="+lfromPage+"&eventName="+leventName,"RemoveUser","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");

		windowName.focus();

	}else {

		return false;

	}

}



 function  validate(formobj){



	if (!(validate_col('e-Signature',formobj.eSign))) return false


	if (!typeof(formobj.eventMsgDaysBpat)=="undefined") {
	 if (!(isInteger(formobj.eventMsgDaysBpat.value))){

		 alert("<%=LC.L_Invalid_Day%>");/*alert("Invalid Day");*****/

		formobj.eventMsgDaysBpat.focus();

	 	 return false;

	 }
	 }
	if (!typeof(formobj.eventMsgDaysApat)=="undefined") {
 	 if (!(isInteger(formobj.eventMsgDaysApat.value))){

 		alert("<%=LC.L_Invalid_Day%>");/*alert("Invalid Day");*****/

		formobj.eventMsgDaysApat.focus();

	 	 return false;

	 }
}


 	 if (!(isInteger(formobj.eventMsgDaysBuser.value))){

 		alert("<%=LC.L_Invalid_Day%>");/*alert("Invalid Day");*****/

		formobj.eventMsgDaysBuser.focus();

	 	 return false;

	 }

 	 if (!(isInteger(formobj.eventMsgDaysAuser.value))){

 		alert("<%=LC.L_Invalid_Day%>");/*alert("Invalid Day");*****/

		formobj.eventMsgDaysAuser.focus();

	 	 return false;

	 }



/*	if(isNaN(formobj.eSign.value) == true) {

	alert*/<%--("<%=MC.M_IncorrEsign_EtrAgain%>");--%>
	/*alert("Incorrect e-Signature. Please enter again");

	formobj.eSign.focus();

	return false;

   }*/





  }

</SCRIPT>

</head>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.business.common.EventInfoDao,com.velos.eres.web.studyRights.StudyRightsJB"%>

<jsp:useBean id="codelstB" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>


<% String src;



src= request.getParameter("srcmenu");
String eventName = "";
//String eventName = request.getParameter("eventName");
//eventName = StringUtil.encodeString(eventName);


%>



<%
//SV, 11/05, added eventbrowser to the list.
//if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")|| (request.getParameter("fromPage")).equals("eventbrowser")){}
if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){



%>

<jsp:include page="include.jsp" flush="true"/>
<%

}
 else{


%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>



<%}%>

<body id="forms">

<%
//if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")|| (request.getParameter("fromPage")).equals("eventbrowser")){

	
    if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){
%>
		<DIV class="popDefault" id="div1">
		<!--DIV class="formDefaultpopup" id="div1"-->
		<br>
	<%	}

else { %>

<DIV class="formDefault_event" id="div1">

<%}%>

<%

	int pageRight = 0;

	String eventmode = request.getParameter("eventmode");

	String duration = request.getParameter("duration");

	String protocolId = request.getParameter("protocolId");

	String calledFrom = request.getParameter("calledFrom");

	String fromPage = request.getParameter("fromPage");

	String mode = request.getParameter("mode");

	String calStatus= request.getParameter("calStatus");

	String displayDur=request.getParameter("displayDur");

	String displayType=request.getParameter("displayType");

  	String eventId = request.getParameter("eventId");

 	String categoryId = "";


	String eventDesc = "";



	String eventMsgDaysBpat = "";

  	String eventMsgBpat = "";

	String eventMsgDaysApat = "";

  	String eventMsgApat = "";

	String eventMsgDaysBuser = "";

	String eventMsgBuser = "";

	String eventMsgDaysAuser = "";

	String eventMsgAuser = "";



	String eventMsgBAuser = "";

	String eventMsgBApat = "";



	String userIdList ="";
	//Virendra: Fixed Bug no. 4736,added variable addAnotherWidth
	int addAnotherWidth = 0;



	HttpSession tSession = request.getSession(true);

	String users ="";



	if (sessionmaint.isValidSession(tSession))	{


	    String calAssoc = request.getParameter("calassoc");
        calAssoc = (calAssoc == null) ? "" : calAssoc;


		if (calledFrom.equals("S")) {

   	   	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");

	  	   if ((stdRights.getFtrRights().size()) == 0){

			 	pageRight= 0;

		   }else{

				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));

    	   }

		} else if (calledFrom.equals("P")) {

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
		}

		else if(calledFrom.equals("L")){

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("EVLIB"));

		}



		String uName = (String) tSession.getValue("userName");


		String calName = "";

		if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary") || fromPage.equals("patientschedule") )
		{
		%>	<%
		} else{
			calName = (String) tSession.getValue("protocolname");
		%>
			<P class="sectionHeadings">  <%Object[] arguments = {calName}; %><%=VelosResourceBundle.getMessageString("M_PcolCal_EvtMsg",arguments)%><%--Protocol Calendar [ <%=calName%> ] >>  Event Message*****--%>  </P>
		<%
		 }

//KM
	if (calledFrom.equals("P")||calledFrom.equals("L"))
    {
       	  eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
       	  eventdefB.getEventdefDetails();
          eventName = eventdefB.getName();
//JM: 12Nov2009: #4399
		  eventName = (eventName==null)?"":eventName;
		  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
 	      	eventName=eventName.substring(0,1000);
          }

    }else{
       	  eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
          eventassocB.getEventAssocDetails();
       	  eventName = eventassocB.getName();		 
//JM: 12Nov2009: #4399
		  eventName = (eventName==null)?"":eventName;
		  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
 	      	eventName=eventName.substring(0,1000);
          }

    }


	%>

<jsp:include page="eventtabs.jsp" flush="true">

<jsp:param name="eventmode" value="<%=eventmode%>"/>

<jsp:param name="duration" value="<%=duration%>"/>

<jsp:param name="protocolId" value="<%=protocolId%>"/>

<jsp:param name="calledFrom" value="<%=calledFrom%>"/>

<jsp:param name="fromPage" value="<%=fromPage%>"/>

<jsp:param name="mode" value="<%=mode%>"/>

<jsp:param name="calStatus" value="<%=calStatus%>"/>

<jsp:param name="displayDur" value="<%=displayDur%>"/>

<jsp:param name="displayType" value="<%=displayType%>"/>

<jsp:param name="eventId" value="<%=eventId%>"/>

<jsp:param name="src" value="<%=src%>"/>

<jsp:param name="eventName" value="<%=eventName%>"/>

<jsp:param name="calassoc" value="<%=calAssoc%>"/>


 </jsp:include>

	<%

	 if(eventId == "" || eventId == null || eventId.equals("null") || eventId.equals("")) {
	%>

	  <jsp:include page="eventDoesNotExist.jsp" flush="true"/>

  <%

	}else {

//SVC, 10/31/04, same problem as in eventdetails.jsp
	//		 if ((calledFrom.equals("P")) || (calledFrom.equals("L")) || ((calledFrom.equals("S") && (fromPage.equals("eventbrowser")) || fromPage.equals("selectEvent") )) )
//question to ask, what scenario in a study calendar, will we bring up messages from protocol calendar??? To me it sounds like none
if ((calledFrom.equals("P")) || (calledFrom.equals("L")) || (calledFrom.equals("S") &&  fromPage.equals("selectEvent") ))
		{
				eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
				eventdefB.getEventdefDetails();
			}else {
				eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
				eventassocB.getEventAssocDetails();
			}

		if(eventmode.equals("M")) {




//SV, 10/31/04?? one more time, which scenario in study calendar do we bring in protocol calendar's message??
// if ((calledFrom.equals("P")) || (calledFrom.equals("L")) || ((calledFrom.equals("S") && (fromPage.equals("eventbrowser")) || fromPage.equals("selectEvent") )) )
 if ((calledFrom.equals("P")) || (calledFrom.equals("L")) || (calledFrom.equals("S") &&  fromPage.equals("selectEvent") ) )
			{

  		        eventMsgDaysBpat = eventdefB.getPatDaysBefore();
				eventMsgDaysApat = eventdefB.getPatDaysAfter();
       		    eventMsgDaysBuser = eventdefB.getUsrDaysBefore();
				eventMsgDaysAuser = eventdefB.getUsrDaysAfter();
		   }else{

				eventMsgDaysBpat = eventassocB.getPatDaysBefore();
    		    eventMsgDaysApat = eventassocB.getPatDaysAfter();
    		    eventMsgDaysBuser = eventassocB.getUsrDaysBefore();
		        eventMsgDaysAuser = eventassocB.getUsrDaysAfter();

		   }



			  if (eventMsgDaysBpat==null) {

				  eventMsgDaysBpat="";

			  }

  			  if (eventMsgDaysApat==null) {

				  eventMsgDaysApat="";

			  }

			  if (eventMsgDaysBuser==null) {

				  eventMsgDaysBuser="";

			  }

			  if (eventMsgDaysAuser==null) {

				  eventMsgDaysAuser="";

			  }





		  if (!((eventMsgDaysBpat==null) || (eventMsgDaysBpat==""))) {

			if(eventMsgDaysBpat.substring(0,1).equals("+")) {

			   eventMsgBApat = "+";

			   eventMsgDaysBpat =eventMsgDaysBpat.substring(1,eventMsgDaysBpat.length());

			} else if(eventMsgDaysBpat.substring(0,1).equals("-")) {

			   eventMsgBApat = "-";

			   eventMsgDaysBpat =eventMsgDaysBpat.substring(1,eventMsgDaysBpat.length());

			} else {

			   eventMsgBApat = "";

			}

  		   }



		  if (!((eventMsgDaysApat==null) || (eventMsgDaysApat==""))) {

			if(eventMsgDaysApat.substring(0,1).equals("+")) {

			   eventMsgBApat = "+";

			   eventMsgDaysApat =eventMsgDaysApat.substring(1,eventMsgDaysApat.length());

			} else if(eventMsgDaysApat.substring(0,1).equals("-")) {

			   eventMsgBApat= "-";

			   eventMsgDaysApat =eventMsgDaysApat.substring(1,eventMsgDaysApat.length());

			} else {

			   eventMsgBApat = "";

			}

  		   }



		  if (!((eventMsgDaysBuser==null) || (eventMsgDaysBuser==""))) {

			if(eventMsgDaysBuser.substring(0,1).equals("+")) {

			   eventMsgBAuser = "+";

			   eventMsgDaysBuser =eventMsgDaysBuser.substring(1,eventMsgDaysBuser.length());

			} else if(eventMsgDaysBuser.substring(0,1).equals("-")) {

			   eventMsgBAuser = "-";

			   eventMsgDaysBuser =eventMsgDaysBuser.substring(1,eventMsgDaysBuser.length());

			} else {

			   eventMsgBAuser = "";

			}

  		   }



		  if (!((eventMsgDaysAuser==null) || (eventMsgDaysAuser==""))) {

			if(eventMsgDaysAuser.substring(0,1).equals("+")) {

			   eventMsgBAuser = "+";

			   eventMsgDaysAuser =eventMsgDaysAuser.substring(1,eventMsgDaysAuser.length());

			} else if(eventMsgDaysAuser.substring(0,1).equals("-")) {

			   eventMsgBAuser = "-";

			   eventMsgDaysAuser =eventMsgDaysAuser.substring(1,eventMsgDaysAuser.length());

			} else {

			   eventMsgBAuser = "";

			}

  		   }



//SV, 11/05, bug 1827, the message "disappeared" in a study calendar and also showed up as 1's in customization screen.			if ((calledFrom.equals("P")) || (calledFrom.equals("L")) || ((calledFrom.equals("S") && (fromPage.equals("eventbrowser")) || fromPage.equals("selectEvent") )) ){
			if ((calledFrom.equals("P")) || (calledFrom.equals("L")) || (calledFrom.equals("S") &&  fromPage.equals("selectEvent") ) ){




  	   	      eventMsgBpat = eventdefB.getPatMsgBefore();

  	   	      eventMsgApat = eventdefB.getPatMsgAfter();

  	   	      eventMsgBuser = eventdefB.getUsrMsgBefore();

  	   	      eventMsgAuser = eventdefB.getUsrMsgAfter();

			} else {

			  eventMsgBpat = eventassocB.getPatMsgBefore();

  	   	      eventMsgApat = eventassocB.getPatMsgAfter();

 	   	      eventMsgBuser = eventassocB.getUsrMsgBefore();

  	   	      eventMsgAuser = eventassocB.getUsrMsgAfter();

			}



			  if (eventMsgBpat==null) {

				  eventMsgBpat="";

			  }

  			  if (eventMsgApat==null) {

				  eventMsgApat="";

			  }

			  if (eventMsgBuser==null) {

				  eventMsgBuser="";

			  }

			  if (eventMsgAuser==null) {

				  eventMsgAuser="";

			  }





		} else {

		   categoryId = request.getParameter("categoryId");

		}



	   int count = 0;

	   EventInfoDao eventinfoDao = eventdefB.getEventDefInfo(EJBUtil.stringToNum(eventId));

	   ArrayList eventUserIds = eventinfoDao.getEventUserIds();

	   ArrayList userTypes = eventinfoDao.getUserTypes();

	   ArrayList userIds = eventinfoDao.getUserIds();

	   CodeDao codeDao = codelstB.getDescForEventUsers(userIds, userTypes);

	   ArrayList eventUserDesc = codeDao.getCDesc();
	   
	   
	    
	   
	 //JM: 22Feb2011: #5857
	   String calStatDesc_A = "";
	   String calStatDesc_F = "";

	   SchCodeDao scho = new SchCodeDao();
	   
	   calStatDesc_A = scho.getCodeDescription(scho.getCodeId("calStatStd","A"));
	   calStatDesc_F = scho.getCodeDescription(scho.getCodeId("calStatLib","F")); 

%>



<form name="eventmessage" id="eventmsgfrm" METHOD=POST action=eventmessagesave.jsp?srcmenu=<%=src%>&eventmode=<%=eventmode%>&eventId=<%=eventId%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%> onsubmit="if (validate(document.eventmessage)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">



<Input type=hidden name=eventMsgBApat value=<%=eventMsgBApat%>>

<Input type=hidden name=calassoc value=<%=calAssoc%>>

<Input type=hidden name=eventMsgBApat value=<%=eventMsgBAuser%>>

<%

if (pageRight == 4) {
%>
   <P class = "defComments_txt"><FONT class="Mandatory"><%=MC.M_OnlyViewPerm_ForEvt%><%-- You have only View permission for the Event.*****--%></FONT></P>
<%} else {Object[] arguments1 = {calStatDesc_A,calStatDesc_F};%>
	<P class = "defComments_txt"><FONT class="Mandatory"><%=VelosResourceBundle.getMessageString("M_ClkSbmtBtn_CalMsgPrev",arguments1)%><%--Please remember to click Submit at the bottom of the page to save data.
	 In case of Calendars with status <%=calStatDesc_A%>/<%=calStatDesc_F%> , click on the 'Refresh Messages' icon on the Calendar browser to apply changes to previously generated schedules.*****--%></FONT></P>
<%}%>

<TABLE width="99%" cellspacing="0" cellpadding="0" class="basetbl">

<% if (!calAssoc.equals("S")){%>

   <tr>

	<td width="25%" > <P class="defcomments"><%=MC.M_Send_MsgToPat%><%--Send Message to <%=LC.Pat_Patient%>*****--%></P></td>

	<td width="25%" ><INPUT NAME="eventMsgDaysBpat" value="<%=eventMsgDaysBpat%>" TYPE=TEXT SIZE=5> <%=LC.L_Days_Before%><%-- Days before*****--%></td>

	<td width="50%" class="textareaheight"> <TEXTAREA name="eventMsgBpat" rows=3 cols=40><%=eventMsgBpat%></TEXTAREA> </td>

   </tr>



   <tr>

	<td></td>

	<td><INPUT NAME="eventMsgDaysApat" value="<%=eventMsgDaysApat%>" TYPE=TEXT SIZE=5> <%=LC.L_Days_After%><%-- Days after*****--%></td>

	<td class="textareaheight"> <TEXTAREA name="eventMsgApat" rows=3 cols=40><%=eventMsgApat%></TEXTAREA> </td>

   </tr>

	<tr height="5">

	 <td></td>

 	 <td></td>

 	 <td></td>

	</tr>
<%} %>
   <tr>

   	<td > <P class="defcomments"><%=MC.M_Send_MsgToUser%><%-- Send Message to User*****--%></P></td>

   	<td >

	<%

	/*if ((calStatus.equals("A")) || (calStatus.equals("F")))

	{

	}else

		{*/%>

	 <A href=# onClick=openUserWindow(document.eventmessage,<%=pageRight%>)><%=LC.L_Select_User_S%><%-- Select User(s)*****--%></A>

	 <%

	 //}

	 %>

	  </td>

<%

	for(int i=0;i<userIds.size(); i++) {

	   if(userTypes.get(i).equals("S")) {

		users = users + eventUserDesc.get(i) +";" ;

		userIdList = userIdList +userIds.get(i) +";" ;

	   }

	}

%>

	<td> <TEXTAREA name="msgUsers" rows=2 cols=40 READONLY><%=users%></TEXTAREA> </td>

   </tr>

   <input type=hidden name = userIds value="<%=userIdList%>">

   <input type=hidden name = userType value="S">

   <input type=hidden name = eventId value="<%=eventId%>">

   <input type=hidden name = srcmenu value="<%=src%>">

   <input type=hidden name = duration value="<%=duration%>">

   <input type=hidden name = protocolId value="<%=protocolId%>">

   <input type=hidden name = calledFrom value="<%=calledFrom%>">

   <input type=hidden name = mode value="<%=mode%>">

   <input type=hidden name = fromPage value="<%=fromPage%>">

   <input type=hidden name = calStatus value="<%=calStatus%>">

   <input type=hidden name=displayDur value=<%=displayDur%>>

   <input type=hidden name=displayType value=<%=displayType%>>
   <input type=hidden name=eventName value="<%=eventName%>">

   <tr>

   	<td></td>

	<td >

	<%

	/*if ((calStatus.equals("A")) || (calStatus.equals("F")))

	{

	}else

		{*/%>

	 <A href=# onclick= openUserWindowfordeletion(document.eventmessage,<%=pageRight%>)><%=LC.L_Rem_User_S%><%-- Remove User(s)*****--%></A>

	<%

	//}

	%>

	</td>

	<td></td>

   </tr>



   <tr>

   <td></td>

	<td><INPUT NAME="eventMsgDaysBuser" value="<%=eventMsgDaysBuser%>" TYPE=TEXT SIZE=5> <%=LC.L_Days_Before%><%-- Days before*****--%></td>

	<td class="textareaheight"> <TEXTAREA name="eventMsgBuser" rows=3 cols=40><%=eventMsgBuser%></TEXTAREA> </td>

   </tr>



   <tr>

	<td></td>

	<td class=tdDefault><INPUT NAME="eventMsgDaysAuser" value="<%=eventMsgDaysAuser%>" TYPE=TEXT SIZE=5> <%=LC.L_Days_After%><%-- Days after*****--%></td>

	<td class="textareaheight"> <TEXTAREA name="eventMsgAuser" rows=3 cols=40 ><%=eventMsgAuser%></TEXTAREA> </td>

   </tr>

 </Table>



<Br>



<%

//if ((calStatus.equals("A")) || (calStatus.equals("F")))

//{

//}else

//{%>

<jsp:include page="propagateEventUpdate.jsp" flush="true">
<jsp:param name="fromPage" value="<%=fromPage%>"/>
<jsp:param name="formName" value="eventmessage"/>
<jsp:param name="eventName" value="<%=eventName%>"/>

</jsp:include>




<table width=100% cellspacing="0" cellpadding="0">
  <tr align="center">
  <td >
   <%
  	if (fromPage.equals("selectEvent")) {%>
		<A href=selecteventus.jsp?fromPage=NewEvent  type="submit"><%=LC.L_Back%></A>
  <%}
 	if (fromPage.equals("eventbrowser")) {%>
		<A href="eventbrowser.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>" type="submit"><%=LC.L_Back%></A>
  <%}
  	if (fromPage.equals("eventlibrary")) {%>
		<A href="eventlibrary.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>"  type="submit"><%=LC.L_Back%></A>
  <%}

  //	if (fromPage.equals("fetchProt")) {
  %>
		<!--<A href="fetchProt.jsp?srcmenu=<%=src%>&selectedTab=4&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&pageNo=1&displayType=<%=displayType%>&displayDur=<%=displayDur%>"  type="submit"><%=LC.L_Back%></A>-->

		<!-- <A href="fetchProt.jsp?srcmenu=<%=src%>&selectedTab=4&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&pageNo=1&displayType=<%=displayType%>&displayDur=<%=displayDur%>" >Back to Protocol Calendar : Customize Event Details</A>		 -->
 <%
 	//}

 %>
 </td>
  <%
  //KM-#4260
  if ((mode.equals("M") && pageRight >=6) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 )) ) {
   %>
   <!-- Virendra: Fixed Bug no. 4736, added td with addAnotherWidth-->
  <td valign="baseline" align="left" width="85%" <%=addAnotherWidth > 0 ? "" : "colspan=5" %>>
		<jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="eventmsgfrm"/>
			<jsp:param name="showDiscard" value="N"/>
			<jsp:param name="showSubmit" value="Y"/>
			<jsp:param name="noBR" value="Y"/>
		</jsp:include>
  </td>
  
  <%} %>
  </tr>
</table>

<%//}%>

</form>



<%

	} //event



} else {  //else of if body for session



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%



}



%>



  <%
  //SV, 11/05 added event browser to the list.
  if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt") || (request.getParameter("fromPage")).equals("eventbrowser")){}

else {

%>





</div>

<div class ="mainMenu" id = "emenu">

<jsp:include page="getmenu.jsp" flush="true"/>

</div>

	<% }%>
 <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</body>

</html>

