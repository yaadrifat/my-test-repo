<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Form_StatusHistory%><%--Form Status History*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@ page import="com.velos.eres.business.common.*, com.velos.eres.service.util.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
	<%-- Nicholas : Start --%>
	<jsp:include page="skinChoser.jsp" flush="true"/>
	<%-- Nicholas : End --%>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
	<%-- Nicholas : Start --%>
	<%--<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>--%>
	<%-- Nicholas : End --%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="formStatB" scope="request" class="com.velos.eres.web.formStat.FormStatJB"/>
<jsp:useBean id="linkedFormsJB" scope="request"  class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="formLibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>

<body>
<SCRIPT LANGUAGE="JavaScript">
var screenHeight=screen.height;
var screenWidth=screen.width;
	if(screenWidth==1280)
	{
		if (navigator.userAgent.indexOf("MSIE") > -1)	
			document.write('<DIV class="popDefault" id="div1" style="overflow:scroll; height:97%">');
		else
			document.write('<DIV class="popDefault" id="div1" style="overflow:scroll; height:96%">');
	}
	else if(screenWidth==1360)
	{
		if (navigator.userAgent.indexOf("MSIE") > -1)
			document.write('<DIV class="popDefault" id="div1" style="overflow:scroll; height:97%">');
		else
			document.write('<DIV class="popDefault" id="div1" style="overflow:scroll; height:96%">');
	}
</SCRIPT>
<!-- <DIV class="popDefault" id="div1" style="overflow:scroll; height:90%">-->
  <%
	String formId = "";
	String formName = "";
	HttpSession tSession = request.getSession(true);     
	if (sessionmaint.isValidSession(tSession))
	{
 %>
 <jsp:include page="sessionlogging.jsp" flush="true"/> 
<%
	  formId = request.getParameter("formId");
	
	  formLibB.setFormLibId(EJBUtil.stringToNum(formId));
	   formLibB.getFormLibDetails();
	   formName = formLibB.getFormLibName();		
	  String linkedFormIdStr  =request.getParameter("linkedFormId") ;
  	 
	  String linkFrom = request.getParameter("linkFrom");
	    
	  int linkedFormId = EJBUtil.stringToNum( linkedFormIdStr);
  	  String studyId= "";
	  int studyIdi  = 0 ;
	 LinkedFormsDao linkedFormsDao = new LinkedFormsDao( ); 
	 linkedFormsJB.setLinkedFormId(linkedFormId) ;
	 linkedFormsJB.getLinkedFormDetails(); 
	 
	 linkedFormsDao =  linkedFormsJB.getLinkedFormsDao(); 
	 studyId = linkedFormsJB.getStudyId() ;
	 studyIdi =  EJBUtil.stringToNum(studyId) ;
	 String formLinkedFrom = linkedFormsJB.getLnkFrom();	 
	 String userIdFromSession = (String) tSession.getValue("userId");
	
		FormStatusDao formStatusDao = new FormStatusDao();
		formStatusDao=formStatB.getFormStatusHistory(EJBUtil.stringToNum(formId));
 		ArrayList stats = new ArrayList(); 
		ArrayList notes = new ArrayList();
		ArrayList stDate = new ArrayList();
		ArrayList endDate = new ArrayList();
		stDate = formStatusDao.getFormStDate();
		endDate = formStatusDao.getFormEndDate();
		notes = formStatusDao.getFormNotes();
		stats = formStatusDao.getFormStatus();
		int len= formStatusDao.getCRows();
%>
<%
		String note = "";
		String status = "";
		String sDate = "";
		String eDate = "";
	
	 //   if (flag > 0) 
		//{
		  if (linkFrom.equals("S")) {
		  %>
		   <p class="sectionHeadings"> <%=MC.M_FrmStatHist_StdAcc%><%--Form Status History >> <%=LC.Std_Study%> Account*****--%> </p>
		   <% }else {%>	
		   <p class="sectionHeadings"> <%=MC.M_FrmStatHist_AccFrmMgmt%><%--Form Status History >> Manage Account >> Form Management*****--%> </p>
		   <%}%>		
 
		<P><%=LC.L_Frm_Name%><%--Form Name*****--%> : <%=formName%></P> 
		
		 <Form name="formstathistory" method="post" action="" onsubmit="">		 
         <table width="100%" cellspacing="0" cellpadding="0" border="0" >
		 <tr > 
	  	 <td width = "70%"> 
	     <P class="defcomments custom-defcomments custom-not-defcomments"><%=MC.M_ListDisp_FrmStatHist%><%--The list below displays form status history.*****--%></P>
		 </td>
		 </tr>
	     </table>

    	<table width="99%" class="custom-table-center" cellspacing="0" cellpadding="0" border="0">
       	<tr> 
       	<th width="20%"> <%=LC.L_Start_Date%><%--Start Date*****--%></th>
        <th width="20%"> <%=LC.L_End_Date%><%--End Date*****--%> </th>
        <th width="20%"> <%=LC.L_Status%><%--Status*****--%> </th>
        <th width="20%"> <%=LC.L_Notes%><%--Notes*****--%> </th>
	    </tr>

       <%
		    for(int counter = 0;counter<len;counter++)
			{	
				sDate=((stDate.get(counter)) == null)?"-":(stDate.get(counter)).toString();
				eDate=((endDate.get(counter)) == null)?"-":(endDate.get(counter)).toString();
				status=((stats.get(counter)) == null)?"-":(stats.get(counter)).toString();
				note=((notes.get(counter)) == null)?"-":(notes.get(counter)).toString();

				if ((counter%2)==0) {
  		%>
      		<tr class="browserEvenRow"> 
        <%
				}
				else{
  		%>
      		<tr class="browserOddRow"> 
        <%
		}
 		 %>

        <td><%=sDate%> </td>
     	<td><%=eDate%></td>   
     	<td><%=status%></td>   
     	<td><%=note%></td>   

      </tr>
      <%
		}
	  %>

  <tr>
 <td align=center colspan="4" class="custom-no-border"> 

		<button onclick="self.close()"><%=LC.L_Close%></button>

</td> 



</tr>

    </table>

  </Form>



  <%



	//} //end of flag



//else



//{



%>


  <%



//} //end of else flag



}//end of if body for session



else



{



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%



}



%>

  <div> 

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>

</div>






</body>



</html>



