<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Select_Ntwrks%><%--Select Networks*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<SCRIPT LANGUAGE="JavaScript" SRC="js/velos/studyNetworkJs.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<style type="text/css">
 .ui-autocomplete {
    cursor: default;
    position: absolute;
    background: #FAFAFA;
    max-height: 400px;
    overflow-y: auto;
    overflow-x: hidden;
    }
  .ui-widget-content a text{
   color: :#FAFAFA
	}
	
  #table2 td{
  border: 1px solid #dddddd;
  }
  
  #table2 tr{
  height:25px;
  }
	#table3 td{
  border: 1px solid #dddddd;
  }
  
  #table3 tr{
  height:25px;
  }

 </style>
</head>

<jsp:include page="popupPanel.jsp" flush="true"/>
<script type="text/javascript">
var selNetWorkIds=[];
var expandNtw=[];
$j(function() {	
	 $j('#sub_btn').hide();
	
	$j("#displayNetwork").autocomplete({

	      source : function(request, response) {
	          $j.ajax({
	               url : "userNetworkAutoCompleteJson.jsp?searchNetwork=true&networkId",
	               type : "POST",
	               autofocus:true,

	               data : {
	              	 term : request.term
	               },		               
	               dataType : "json",
	               success: function(data){
	                   response( $j.map( data, function( item ) {
	                       return {
	                           label: item.network_name,
	                           networkId: item.pk_nwsites     // EDIT
	                       }
	                   }));
	                },
	                
	        });
	     },
	     select : function(event, ui) {
	        selectedNtwId=ui.item.networkId;
	  	   	fetchNetworkRows(selectedNtwId);
	         $j("#displayNetwork").val('');
	         
	         return false;
	     }
	  });

});

function networkSiteByName1(event){
	if (event.keyCode != 37 && event.keyCode != 39 && event.keyCode != 36 && event.keyCode != 38 && event.keyCode != 100 && event.keyCode != 102 && event.keyCode != 104) {
	var searchNetWorkByName1=document.getElementById("searchNetWorkByName1").value;
	var moreParam='';
	if(searchNetWorkByName1!=""){
		moreParam ='{"searchByName":'+'"'+searchNetWorkByName1+'"'+',"calledFrom":"networkLookup"'+',"filter":"name"'+',"networkId":"'+selectedNtwId+'"}';
	}else{
		moreParam ='{"calledFrom":"networkLookup"'+',"filter":"name"'+',"networkId":"'+selectedNtwId+'"}';
	}
	var pageRight=document.getElementById("pageRight").value;
	//alert("param-"+param);
	var urlCall="networksearch.jsp?&calledFrom=networkLookup&src=networkTabs&searchByName="+searchNetWorkByName1+"&pageRight="+pageRight+"&expandNtw="+expandNtw+'&moreParam='+moreParam+'&parentNtwId='+selectedNtwId;
	jQuery.ajax({
		url: urlCall,
		cache: false,
		type: "POST",
		async:false,
		success : function (data) 
		{
			//alert(data);
			$j("#table2").find("tr:gt(0)").remove();
			$j("#row").closest( "tr" ).after(data);
			var new1=($j("#searchNetWorkByName1").val(searchNetWorkByName1));
			var setselection=new1.val().length * 2;
			new1.focus();
			new1[0].setSelectionRange(setselection,setselection);
			//$j("#searchNetWorkByName1").focus();
			hideTableRowSortable();
		}
	});	
	}
}
	
function participationType(event){
	if (event.keyCode != 37 && event.keyCode != 39 && event.keyCode != 36 && event.keyCode != 38 && event.keyCode != 100 && event.keyCode != 102 && event.keyCode != 104) {
	var searchNetWorkByparticipationType=document.getElementById("searchNetWorkByparticipationType").value;
	var moreParam='';
	if(searchNetWorkByparticipationType!=""){
		moreParam ='{"searchByName":'+'"'+searchNetWorkByparticipationType+'"'+',"calledFrom":"networkLookup"'+',"filter":"type"'+',"networkId":"'+selectedNtwId+'"}';	
	}else{
		moreParam ='{"calledFrom":"networkLookup"'+',"filter":"type"'+',"networkId":"'+selectedNtwId+'"}';
	}
	var pageRight=document.getElementById("pageRight").value;
	//alert("param-"+param);
	var urlCall="networksearch.jsp?&calledFrom=networkLookup&src=networkTabs&searchByName="+searchNetWorkByparticipationType+"&pageRight="+pageRight+"&expandNtw="+expandNtw+'&moreParam='+moreParam+'&parentNtwId='+selectedNtwId;
	jQuery.ajax({
		url: urlCall,
		cache: false,
		type: "POST",
		async:false,
		success : function (data) 
		{
			//alert(data);
			$j("#table2").find("tr:gt(0)").remove();
			$j("#row").closest( "tr" ).after(data);
			var new1=($j("#searchNetWorkByparticipationType").val(searchNetWorkByparticipationType));
			var setselection=new1.val().length * 2;
			new1.focus();
			new1[0].setSelectionRange(setselection,setselection);
			//$j("#searchNetWorkByparticipationType").focus();
			hideTableRowSortable();
		}
	});
	}	
	}
	
function NetWorkByStatus(event){
	if (event.keyCode != 37 && event.keyCode != 39 && event.keyCode != 36 && event.keyCode != 38 && event.keyCode != 100 && event.keyCode != 102 && event.keyCode != 104) {
		var searchNetWorkByStatus=document.getElementById("searchNetWorkByStatus").value;
		var moreParam='';
		if(searchNetWorkByStatus!=""){
			moreParam ='{"searchByName":'+'"'+searchNetWorkByStatus+'"'+',"calledFrom":"networkLookup"'+',"filter":"status"'+',"networkId":"'+selectedNtwId+'"}';
		}else{
			moreParam ='{"calledFrom":"networkLookup"'+',"filter":"status"'+',"networkId":"'+selectedNtwId+'"}';
		}
		var pageRight=document.getElementById("pageRight").value;
		//alert("param-"+param);
		var urlCall="networksearch.jsp?&calledFrom=networkLookup&src=networkTabs&searchByName="+searchNetWorkByStatus+"&pageRight="+pageRight+"&expandNtw="+expandNtw+'&moreParam='+moreParam+'&parentNtwId='+selectedNtwId;
		
		
		jQuery.ajax({
			url: urlCall,
			cache: false,
			type: "POST",
			async:false,
			success : function (data) 
			{
				//alert(data);
				$j("#table2").find("tr:gt(0)").remove();
				$j("#row").closest( "tr" ).after(data);
				var new1=($j("#searchNetWorkByStatus").val(searchNetWorkByStatus));
				var setselection=new1.val().length * 2;
				new1.focus();
				new1[0].setSelectionRange(setselection,setselection);
				//$j("#searchNetWorkByStatus").focus();
				hideTableRowSortable();
			}
		});	
	}
		}


function validate_selectntw(event,networkId){
    
    var _row=$j('#table3 tr').length;
    var _html=$j('#selNtw').html();
	var _netArray='';
    var _addRow=false;
    var studyNetIds = window.opener.document.getElementById("addedNetIds").value;
    _netArray=$j(event).attr("value").split('||');
    
    
    if(event.checked){
		if(studyNetIds.indexOf(networkId)>=0){
			alert("<%=MC.M_Ntwrk_Added%>");
			event.checked=false;
			return false;
		}
		
		if(jQuery.inArray(_netArray[0],selNetWorkIds)<0){
			selNetWorkIds.push(_netArray[0]);
			_addRow=true;
		}
		
		if (_addRow){
		$j('#table3 tr:last').after('<tr ><td width="5%"><input type="checkbox" style="display:none" checked id="'+_netArray[0]+'_hidden" name="selectedntw" value="'+_netArray[0]+'"/></td>'+
				'<td width="55%">'+_netArray[1]+'</td><td width="25%">'+_netArray[2]+'</td width="15%"><td>'+_netArray[3]+'</td><td>'+"<img src=\"./images/delete.gif\" style=\"cursor:pointer\" onClick=\"deleteStudyNtwk(this.id,'"+_netArray[0]+"')\" id=\"DelNtw_"+_netArray[0]+"\"  title=\""+LC.L_Delete+"\" border=\"0\"/>"+'</td></tr>');
			_addRow=false;
		}
		$j("#table3").css("display", "block");
		$j('#sub_btn').show();
    }
    else{
    	
    	var index = selNetWorkIds.indexOf(_netArray[0]);
    	if (index > -1) {
    		selNetWorkIds.splice(index, 1);
      	}
    	deleteStudyNtwk(_netArray[0]+'_hidden',_netArray[0]);
    }
}

function deleteStudyNtwk(e,netId){
 $j("#"+e).closest('tr').remove();
	$j('input:checkbox[id='+netId+']').attr('checked', false);
	var index = selNetWorkIds.indexOf(netId);
	if (index > -1) {
		selNetWorkIds.splice(index, 1);
  	}

}

/* function checkAll(status) {
	var checkboxes;
	 if (status.checked) {
		 checkboxes = document.getElementsByName("ntwchecked");
		 for(var i=0, n=checkboxes.length;i<n;i++) {
			 checkboxes[i].checked=true; 
		 }
	   }
	 else{
		checkboxes = document.getElementsByName("ntwchecked");
		 for(var i=0, n=checkboxes.length;i<n;i++) {
			 checkboxes[i].checked=false; 
		 }
	 }
} */

</script>

<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1) 
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {	
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>
<%@ page language = "java" import = "com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*"%>

<% int pageRight = 0;
   HttpSession tSession = request.getSession(true); 
   
   if (sessionmaint.isValidSession(tSession))

 {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<div style="display:none;" id="dialog-message" title="">
  <br>
  <p><%=LC.L_Please_Wait %><img style="width:200px;" src="../images/jpg/loading_pg.gif"/></p>
</div>
<div id="submitFailedDialog" title="<%=LC.L_Error%>" class="comments" style="display:none; overflow:auto">
	<table align="center">
		<tr><td height="20px">&nbsp;</td></tr>
		<tr align="center">
			<td id="errorMessageTD"></td>
		</tr>
		<tr><td height="20px">&nbsp;</td></tr>
		<tr align="center">
			<td>
				<button onclick='$j("#submitFailedDialog").dialog("close");'><%=LC.L_Close%></button>
			</td>
		</tr>
	</table>
</div>
<%
String studyId=null;
   int accId = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));
   GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
   pageRight = EJBUtil.stringToNum(request.getParameter("pageRight"));
   studyId=request.getParameter("studyId")==null?"":request.getParameter("studyId");

     

  // pageRight = 7;
 if (pageRight >= 4 )
	{
 %>

<body style="overflow:scroll;"> 
<table style="top:10%; width:100%;">
<tr>
<td style="width:30%;font-size:12px;font-family:sans-serif;"><b><%=LC.L_SlctNtwrk_ToDisplay %></b></td>
<td style="width:70%"><input size="60" placeholder="Display Network" id="displayNetwork"  class="search-table midAlign" type="text" style="border-radius:5px;border: 2px solid #aaa;padding:3px 3px 3px 0px;background-position:right;background-image: url('../images/jpg/search_pg.png');background-repeat: no-repeat;"/></td>
</tr>
</table>
<br>
<div id="divRight" style="height:50%; overflow:auto;">
		<table width="100%" cellspacing="0" cellpadding="0" border="0" id="table2" class="outline">
			<tr class = "popupHeader" id="row">
				<th width="5%"><!-- <input type="checkbox" name="selectAll" id="selectAll" onchange="checkAll(this);" value="" /> --></th>
				<th width="55%" onClick="setOrder(document.networkTabs,'lower(SITE_NAME)')"> <%=LC.L_Organization_Name%><%--Organization Name*****--%></th>
        		<th width="25%" onClick="setOrder(document.networkTabs,'lower(SITE_TYPE)')"> <%=LC.L_Relation_Type%><%--Relationship Type***** --%></th>
        		<th width="15%"> <%=LC.L_Network_Status%><%---Network Status***** --%></th>
			</tr>
		</table>
</div>
<div>
<input type="hidden" name ="studyId" id="studyId" value="<%= studyId%>" />
<input type="hidden" name ="pageRight" id="pageRight" value="<%= pageRight%>" />
<hr/>
  <br/>
  <div id="selNtw" align="centre">
  <table width="100%" style="display:none" cellspacing="0" cellpadding="0" border="0" id="table3" class="outline">
  <tr class = "popupHeader" id="row">
				<th width="5%"></th>
				<th width="55%" onClick="setOrder(document.networkTabs,'lower(SITE_NAME)')"> <%=LC.L_Organization_Name%><%--Organization Name*****--%></th>
        		<th width="25%" onClick="setOrder(document.networkTabs,'lower(SITE_TYPE)')"> <%=LC.L_Relation_Type%><%--Relationship Type*****--%></th>
        		<th width="15%"> <%=LC.L_Network_Status%><%--Network Status*****--%></th>
        		<th width="15%"> <%=LC.L_Delete%><%--Delete*****--%></th>
        		</tr>
  </table>
  </div>
  <div  style="margin-left: 50%;">
  <br/>
  	<button type="button" id="sub_btn" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" name="browse" onclick="return validateLookupSubmit();"><%=LC.L_Submit %></button>
  
  </div>

</div>

 <% 
	
	} //end of if body for page right
else
{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
 <%
 } //end of else body for page right
}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
<%
}
%>
 <jsp:include page="ui-include.jsp" flush="true"></jsp:include>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</body>


</html>
