<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
	<body>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,javax.xml.transform.TransformerFactory,javax.xml.transform.Transformer,javax.xml.transform.Source,javax.xml.transform.stream.StreamSource,javax.xml.transform.stream.StreamResult,javax.xml.transform.TransformerException,javax.xml.transform.TransformerConfigurationException,java.io.*,java.net.URL"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	
<%
	HttpSession tSession = request.getSession(true);
    if (null == request.getHeader("referer")) {
%>
<h1><%=LC.L_Forbidden%><%--Forbidden*****--%></h1>
<p><%=MC.M_NotPerm_ToAcesServer%><%--You do not have permission to access this server or file.*****--%></p>
</body>
</html>
<%
        return;
    }

	  if (sessionmaint.isValidSession(tSession))
	{
	String htmlFile=request.getParameter("htmlFile");
	String filePath=request.getParameter("filePath");
	String fileDnPath=request.getParameter("fileDnPath");
	Integer repId = new Integer((request.getParameter("repId")==null) ? "0" : request.getParameter("repId"));
	String calledfrom = request.getParameter("calledfrom");
	if(EJBUtil.isEmpty(calledfrom)){
		calledfrom = LC.L_Rpt_Central;
	}
	String requestURL 	= StringUtil.encodeString(request.getRequestURL().toString());
	String queryString 	= StringUtil.encodeString(request.getQueryString());
	String AppndAddress = requestURL.substring(0,requestURL.indexOf("velos")+5);
	Calendar now = Calendar.getInstance();
	String wordFileName="reportword"+"["+ System.currentTimeMillis() + "].doc" ;
	String wordFile=filePath + "/" + wordFileName ;

	try
	{	
		File f=new File(htmlFile);
		int len=(int)f.length();
		StringBuffer fileStrbuf=new StringBuffer();
		BufferedReader in = new BufferedReader(
		   new InputStreamReader(
                      new FileInputStream(f), "UTF8"));
		        
		String fileStr;
		      
		while ((fileStr = in.readLine()) != null) {
			fileStrbuf.append(fileStr);
		}
		in.close();
		fileStr = fileStrbuf.toString();
		//out.println(len);

		/*FileInputStream fIn=new FileInputStream (f);
		byte[] b=new byte[(len+2)];
		int i =fIn.read(b, 0, len);
		System.out.println(i);
		fIn.close();
		String fileStr=new String(b);*/
		fileStr=fileStr.trim();
		//fileStr = fileStr.replaceFirst("../images/Velos_Invoice_Logo.jpg",AppndAddress+"/images/Velos_Invoice_Logo.jpg");
		//out.println(fileStr.length());
		if(null!=fileStr && fileStr.indexOf("./styles/common.css")!=-1){
			fileStr = fileStr.replace("./styles/common.css", "");
		}
//		out.println(fileStr);

		int pos = fileStr.indexOf("text/html");

		StringBuffer fileStrBuff=new StringBuffer(fileStr);
		fileStrBuff.replace(pos, pos+9, "application/ms-word");
		fileStr=fileStrBuff.toString();

//sajal

		/*int pos1 = fileStr.indexOf("<SCRIPT LANGUAGE");
		int pos2 = fileStr.indexOf("</SCRIPT>");
		if ((pos1>=0) && (pos2>=0))
		fileStrBuff.replace(pos1, pos2+9, "");
		fileStr=fileStrBuff.toString();*/	
		int pos2=-1;
		int pos1 = fileStr.indexOf("<link");
		if (pos1>=0) 
		 pos2 = fileStr.indexOf(">",pos1);
		if ((pos1>=0) && (pos2>=0))
		fileStrBuff.replace(pos1, pos2+1, "");

//sajal

		while(fileStr.indexOf("<A") > 0){
			pos2=-1;
			pos1 = fileStr.indexOf("<A");
			if (pos1>=0) 
				 pos2 = fileStr.indexOf(">",pos1);
			if ((pos1>=0) && (pos2>=0))
				fileStrBuff.replace(pos1, pos2+1, "");
			fileStr=fileStrBuff.toString();
			
			pos2=-1;
			pos1 = fileStr.indexOf("</A");
			if (pos1>=0) 
				 pos2 = fileStr.indexOf(">",pos1);
			if ((pos1>=0) && (pos2>=0))
				fileStrBuff.replace(pos1, pos2+1, "");
			fileStr=fileStrBuff.toString();
		}

//		out.println(fileStr);

		/*b=fileStr.getBytes();
		len=b.length;
		FileOutputStream fOut=new FileOutputStream (wordFile);	
		fOut.write(b,0,len);
		fOut.close();*/
		
		Writer writer  = null;
		OutputStream outputStream = new FileOutputStream(wordFile);
		writer  = new OutputStreamWriter(outputStream,"UTF8");
		writer.write(fileStr, 0, fileStr.length());

		writer.close();
		}
		catch (Exception e)
		{
			System.out.println("Error in Creation of Word file" + e + e.getMessage());
		}
	
		String path=fileDnPath + "?file=" + wordFileName +"&mod=R&repId="+repId+"&moduleName="+calledfrom+"&filePath="+StringUtil.encodeString(filePath)+"&requestURL="+requestURL+"&queryString="+queryString;
		
%>

	<form name="dummy" method="post" action="<%=path%>" target="_self">
		<input type ="hidden" name="d" />
	</form>

<script>
	document.dummy.submit();
</script>
	
	<% } //session
		%> 
	
	
</body>
	</html>