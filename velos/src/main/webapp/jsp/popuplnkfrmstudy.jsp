<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>

<title><%=LC.L_Add_NewCrf%><%--Add New CRF*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT Language="javascript">



 function  validate(formobj){
	num=formobj.len.value;

	formNames = new Array();
	formIds = new Array();
	//alert("num"+num);
	var j=0;
	var count=0;
	if(num==1)
	 {
	numTxt= formobj.crfNumbers;
	numVal = formobj.crfNumbers.value;
	formIds=formobj.formIds.value;

		 if((formobj.chkcrf.checked) && (numVal==""))
			 {
				alert("<%=MC.M_Etr_CrfNum%>");/*alert("Please enter CRF Number");*****/
				numTxt.focus();
				return false;

			 }
			 if(!(formobj.chkcrf.checked) && (numVal==""))
			 {
				alert("<%=MC.M_PlsSelCrf_Name%>");/*alert("Please select the CRF Name");*****/
				//formobj.crfNumbers[i].focus();
				return false;

			 }
			 if(!(formobj.chkcrf.checked) && (numVal!=""))
			 {
				 alert("<%=MC.M_PlsSelCrf_Name%>");/*alert("Please select the CRF Name");*****/
				//formobj.crfNumbers[i].focus();
				return false;

			 }
	 }
	else
	 {
		for(i=0;i<num;i++)
		 {

			 if((formobj.chkcrf[i].checked) && (formobj.crfNumbers[i].value==null || formobj.crfNumbers[i].value==""))
			 {
				alert("<%=MC.M_Etr_CrfNum%>");/*alert("Please enter Crf Number");*****/
				formobj.crfNumbers[i].focus();
				return false;

			 }
			 if(!(formobj.chkcrf[i].checked) && (formobj.crfNumbers[i].value!=null && formobj.crfNumbers[i].value!=""))
			 {
				alert("<%=MC.M_PlsSelFrm_EtrCrf%>");/*alert("Please select the Form for which you have entered CRF number");*****/
				//formobj.crfNumbers[i].focus();
				return false;

			 }
		 }


	for(i=0;i<num;i++)
	 {
		 if(formobj.chkcrf[i].checked)
		 {
		   //alert("inside chkcrf");
		   //alert("arrformNames[i]"+formobj.crfNames[i].value);
		   formNames[j]=formobj.crfNames[i].value;
		   formIds[j]=formobj.formIds[i].value;
		   j++;
		   count++;
		 }
	 }

	 if(count==0)
	 {
	   alert("<%=MC.M_Selc_CrfName%>");/*alert("Please Select CRF Name");*****/
	   return false;
	 }
	}



	 if (!(validate_col('e-Signature',formobj.eSign))) return false

	<%-- if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   	} --%>

	formobj.crfNames.value= formNames;
	formobj.formIds.value=formIds;

   }

</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="linkedformsB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,java.util.*,java.io.*,org.w3c.dom.*"%>
<%@ page import="com.velos.eres.service.util.*"%>


<% String src;

src= request.getParameter("srcmenu");

%>
<body style="overflow:scroll">
<br>
<DIV class="popDefault" id="div1">


  <%

	HttpSession tSession = request.getSession(true);
	String mode=request.getParameter("mode");
	String selectedTab=request.getParameter("selectedTab");
	String eventmode = request.getParameter("eventmode");
	String duration = request.getParameter("duration");
	String protocolId = request.getParameter("protocolId");
	String calledFrom = request.getParameter("calledFrom");
	String fromPage = request.getParameter("fromPage");
	String calStatus= request.getParameter("calStatus");

	String displayDur=request.getParameter("displayDur");
	String displayType=request.getParameter("displayType");
  	String eventId = request.getParameter("eventId");
	String crflibmode=request.getParameter("crflibmode");
	String crflibId=request.getParameter("crflibId");
	int pageRight = 0;


	if (sessionmaint.isValidSession(tSession))
	{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%


		String studyId = (String) tSession.getValue("studyId");
		String accId=(String)tSession.getAttribute("accountId");

		int istudyId = EJBUtil.stringToNum(studyId);
		int iaccId = EJBUtil.stringToNum(accId);
		ArrayList arrformNames = new ArrayList();
		ArrayList arrformIds = new ArrayList();
		ArrayList arrfkEvent = new ArrayList();

		LinkedFormsDao lnkformsDao = new LinkedFormsDao();
		lnkformsDao=linkedformsB.getStudyForms(iaccId,istudyId,0,0);
		arrformNames = lnkformsDao.getFormName();
		arrformIds = lnkformsDao.getFormId();
		int len = arrformNames.size();
		String crfName="";

		String calName = "";
		if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary") || fromPage.equals("patientschedule") )

		{

	%>


	<%

		} else

		{

		calName = (String) tSession.getValue("protocolname");

	%>

	<P class="sectionHeadings"> <%Object[] arguments = {calName}; %>
	    <%=VelosResourceBundle.getMessageString("M_PcolCal_AddCrf",arguments)%><%-- Protocol Calendar [ <%=calName%> ] >> Add Crf*****--%> </P>

	<%

		 }
	%>

<% if (calledFrom.equals("S")) {%>
<jsp:include page="crftabs.jsp" flush="true">
<jsp:param name="mode" value="<%=mode%>"/>
<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
<jsp:param name="calStatus" value="<%=calStatus%>"/>
<jsp:param name="calledFrom" value="<%=calledFrom%>"/>
<jsp:param name="eventId" value="<%=eventId%>"/>
</jsp:include>
<%}%>

  <Form name="studylnkforms" method="post" action="popuplnkfrmstudysave.jsp?eventId=<%=eventId%>" onsubmit="return validate(document.studylnkforms)" >

<% if(len==0){%>
<P class = "defComments"><FONT class="Mandatory"><%=MC.M_NoFrms_AssocToStd%><%--No Forms Associated to this <%=LC.Std_Study%>*****--%></FONT></P>
  <%}
  else
	{%>

  <table width="65%" cellspacing="0" cellpadding="0">
      <tr>
        <td width="10%"><b><%=LC.L_Select%><%--Select*****--%> <FONT class="Mandatory">* </FONT></b></td>
		<td width="25%"><b><%=LC.L_Crf_Name%><%--CRF Name*****--%></b></td>
		<td width="30%"><b><%=LC.L_Crf_Number%><%--CRF Number*****--%> <FONT class="Mandatory">* </FONT></b></td>
	  </tr>
	  <tr>
	  <%

	  for(int i=0;i<len;i++)
		{
	  crfName = arrformNames.get(i).toString();


	  %>
	  <tr>
	  <td><input type=checkBox name=chkcrf value="<%=crfName%>"></td>
	  <td><%=arrformNames.get(i)%></td>
	  <input type="hidden" name="crfNames" value="<%=arrformNames.get(i)%>"  >
	  <input type="hidden" name="formIds" value="<%=arrformIds.get(i)%>">
	  <td><input type=text name=crfNumbers></td>

	  </tr>
  <%}%>

<%String[] str=request.getParameterValues("crfNames");
		%>
  </table>
  <br>

<table width="60%">
<tr>
	   <td>
		<%=LC.L_Esignature%><%--e-Signature*****--%> <FONT class="Mandatory">* </FONT>
	   </td>
	   <td>
		<input type="password" name="eSign" maxlength="8" autocomplete="off">
		</td><td>
		<input type="image" src="../images/jpg/Submit.gif" align="absmiddle" border="0" >
	   </td>
</tr>
</table>
	  <%}%>
	<input type="hidden" name="mode"  value = <%=mode%> >
	<input type="hidden" name="eventmode" value = <%=eventmode%> >
	<input type="hidden" name="duration"  value = <%=duration%> >
	<input type="hidden" name="mode"  value = <%=mode%> >
	<input type="hidden" name="arrformNames"  value = <%=arrformNames%> >
	<input type="hidden" name="len"  value = <%=len%> >
	<input type="hidden" name="selForms"  >
	<input type="hidden" name="protocolId" value= <%=protocolId%>>
    <input type="hidden" name="calledFrom" value= <%=calledFrom%>>
    <input type="hidden" name="calStatus" value= <%=calStatus%>>
	<input type="hidden" name="fromPage" value= <%=fromPage%>>





</Form>
  <%



}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>


</body>

</html>


