<%--
* File Name 	: setStudyMileStatus.jsp
* Created on	: 24/03/2011
* Created By	: Yogesh Kumar (YK)
* Enhancement	: 8_10.0DFIN20 
* Purpose		: Set Study level Milstone status and update individual Milestone status.
*
*
--%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@page import="com.velos.esch.web.eventdef.EventdefJB,com.velos.esch.web.eventassoc.EventAssocJB"%>
<%@page import="java.util.ArrayList"%>


<%@page import="java.util.HashMap"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="com.velos.eres.business.common.CodeDao"%>
<%@page import="com.velos.eres.web.milestone.MilestoneJB,com.velos.eres.service.util.*"%>
<%@page import="com.velos.eres.business.common.MilestoneDao"%>
<%@page import="com.velos.eres.web.study.StudyJB"%>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="milestoneB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />


<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	HttpSession tSession = request.getSession(true);
	JSONObject jsObj = new JSONObject();
	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn/*"User is logged in."*****/);
		out.println(jsObj.toString());
		return;
	}
	
	String oldESign = (String) tSession.getAttribute("eSign");
	String eSign = request.getParameter("eSign");
	if (!oldESign.equals(eSign)) {
		jsObj.put("result", -4);
		if ("userpxd".equals(Configuration.eSignConf)) {
			jsObj.put("resultMsg", MC.M_EtrWrgPassword_Svg/*"You entered a wrong e-Password. Please try saving again. "*****/);
		}
		else
		{
		jsObj.put("resultMsg", MC.M_EtrWrgEsign_Svg/*"You entered a wrong e-signature. Please try saving again. "*****/);
		}
		out.println(jsObj.toString());
		return;
	}
	
	String ipAdd = (String) tSession.getAttribute("ipAdd");
    String userId = (String) tSession.getAttribute("userId");
    String accountId=(String) tSession.getValue("accountId");
    int fkAccount= Integer.parseInt(accountId);
    
 	String setMileStat = request.getParameter("setMileStat");
 	String studyID = request.getParameter("studyId");
 	int studyId= Integer.parseInt(studyID);
	CodeDao cdMsPayDao = new CodeDao();
	MilestoneDao mileDao = new MilestoneDao();
	ArrayList milestonePKIds = null;
	int ret=0;
	try {
		 int mileStatus= cdMsPayDao.getCodeId("mile_setstat","approved"); //Approved Status from Codelist Bug #
		 if(setMileStat==null || setMileStat.length()==0)
		 {
			jsObj.put("result", -2);
			jsObj.put("resultMsg", MC.M_No_OperPerformed/*"No operations were performed"*****/);
			out.println(jsObj.toString());
			return;
		 }
		 StudyJB stdJb =new StudyJB(); 
		 stdJb.setId(studyId);
		 stdJb.getStudyDetails();
		 stdJb.setIpAdd(ipAdd);
		 stdJb.setModifiedBy(userId);
		 stdJb.setMilestoneSetStatus(setMileStat);
		 ret = stdJb.updateStudy(); /*Study updated with Study Level Milestone*/
		 if (ret==-2) {
			 
			    jsObj.put("result", ret);
			 	jsObj.put("resultMsg", MC.M_StdMstoneStat_NotUpdt/*"Study Milestone status not Updated"*****/);
				out.println(jsObj.toString());
				
		  }
		 if(setMileStat.equalsIgnoreCase(String.valueOf(mileStatus)))  /*Check condition for 'Approved' Study Level Milestone status*/
		 { 
			 int setMileStoneStatus= cdMsPayDao.getCodeId("milestone_stat","A");
			 mileDao = milestoneB.getStudyMileStoneForSetStatus(studyId);
			 milestonePKIds=mileDao.getMilestoneIds();
			 int noOfMilestones = milestonePKIds == null ? 0 : milestonePKIds.size();  
			 for (int iX=0; iX<milestonePKIds.size(); iX++) {   	
				if (((Integer)milestonePKIds.get(iX)).intValue() < 1) { continue; }
				MilestoneJB mlJB= new MilestoneJB();
				
				mlJB.setId(((Integer)milestonePKIds.get(iX)).intValue());					
				mlJB.getMilestoneDetails();
				mlJB.setIpAdd(ipAdd);
				mlJB.setModifiedBy(userId);
				mlJB.setMilestoneStatFK(String.valueOf(setMileStoneStatus));
				ret = mlJB.updateMilestone();		/*Individual Milestones Updated with Status as Active*/
				} 
			 if (ret==-2) {
				 
				    jsObj.put("result", ret);
				 	jsObj.put("resultMsg", MC.M_MstoneStat_NotUpdt/*"Milestone status not Updated"*****/);
					out.println(jsObj.toString());
					
			  }
			}
	  	
	} catch(Exception e) {
		jsObj.put("result", -3);
		jsObj.put("resultMsg", e);
		out.println(jsObj.toString());
		return;
	}
	
    jsObj.put("result", 0);
    jsObj.put("resultMsg", MC.M_StdMstone_Updt/*"Study Level Milestone Status updated successfully"*****/);
	out.println(jsObj.toString());
%>
