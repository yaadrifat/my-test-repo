<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Define_InterFldAction%><%--Define Inter-Field Action*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<jsp:include page="include.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


<SCRIPT Language="javascript">

 function  validate(formobj)
 {
				 
	if (  !(validate_col('\'Select Source Fields\'',formobj.dispsourcevalue)     )   ) return false
	if (  !(validate_col('\'Select Calculation Type\'',formobj.actionType)     )   ) return false
	if (  !(validate_col('\'Select Target Fields\'',formobj.disptargetvalue)     )   ) return false
 	if(formobj.codeStatus.value != "WIP"){	
 	 	if (  !(validate_col('e-Signature',formobj.eSign)     )   ) return false
 		 
 		<%-- if(isNaN(formobj.eSign.value) == true) 
 			{
 			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
 			formobj.eSign.focus();
 			 return false;
 	   		} --%>
 	 	}

}

function populateKeyWordParams(formobj)
{
	var value ;
	value = formobj.sourceFldDD.options[formobj.sourceFldDD.selectedIndex].value;
	var fldId , sysid;
	fld = value.substring(0, value.indexOf("[VELSEP]"));
	sysid = value.substring(value.indexOf("[VELSEP]")+8,value.length);
	
	formobj.velsource.value = sysid;
	formobj.velsourcefldid.value = fld;
	
}

function openTarget(formobj)
{
		sourceFormName = formobj.name;
		targetDispFld = "disptargetvalue";
		targetIdFld = "veltargetid";
		targetKeywordFld = "veltarget";
		formId = formobj.formId.value;
		paramStr="selectActionTargetFields.jsp?formId="+formId +"&sourceFormName="+sourceFormName+"&targetDispFld="+targetDispFld+"&targetIdFld="+targetIdFld + "&targetKeywordFld=" + targetKeywordFld;
		
		openPopWindow(paramStr);
}

function openSource(formobj)
{
		sourceFormName = formobj.name;
		sourceDispFld = "dispsourcevalue";
		sourceIdFld = "velsourcefldid";
		sourceKeywordFld = "velsource";
		formId = formobj.formId.value;
		paramStr="selectActionTargetFields.jsp?formId="+formId +"&sourceFormName="+sourceFormName+"&targetDispFld="+sourceDispFld+"&targetIdFld="+sourceIdFld + "&targetKeywordFld=" + sourceKeywordFld;
		
		openPopWindow(paramStr);
}


function openPopWindow(paramStr)
{
	windowName=window.open(paramStr,"FieldAction","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=450 ,top=200 ,left=250");
	windowName.focus();
	
}


</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.fieldAction.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.tags.*"%>
<%@ page language = "java" import = "com.velos.eres.web.fieldLib.FieldActionJB"%><%@page import="com.velos.eres.service.util.*"%>

<body>
<DIV class="popDefault" id="div1"> 
 <%

	HttpSession tSession = request.getSession(true);
	int pageRight = 0;	
	
	FieldActionJB fldActionJB = new FieldActionJB();

	FieldActionDao fldActionDaoInfo = new FieldActionDao();
	
	FieldLibDao fldlibDao = new FieldLibDao();
	
	FieldActionStateKeeper fsk = new FieldActionStateKeeper(); 	

	VFieldAction vfldAction = new VFieldAction ();
	
   if (sessionmaint.isValidSession(tSession))
   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	
<%
	 
	 // get page right
			
	 String pageRightParam = request.getParameter("pgrt");		
	 pageRight = EJBUtil.stringToNum(pageRightParam);
	 
     

	if (pageRight >= 4)
		
	{
		String mode="";
		String codeStatus = request.getParameter("codeStatus");
		mode=request.getParameter("mode");
		String formId = request.getParameter("formId");
		String fieldAction = request.getParameter("fldActionId");
		
		String pullDownSourceField = "";
		ArrayList sourcefieldIds = new ArrayList();
		ArrayList sourcefieldNames = new ArrayList();
		ArrayList sourcefieldSystemIds = new ArrayList();
		
		Hashtable htActionInfo = new Hashtable();
		String fldActionCondition = "";
		String fldAction  = "";
		String selectedSourceFld  = "";
		int selectedSourceFldInt = 0;				
				
		String selectedVeltarget = "";
		String selectedDisptargetvalue = "";
        String selectedVeltargetid = ""; 
		int fieldActionInt = 0;
		
		String selectedVelSource = "";
		String selectedActionType = "";
		
		String  selectedVelSourceFldId = "";
		String selectedDispsourcevalue = "";

		

		//get source form fields
			
		fldlibDao = fldActionJB.getFieldsForFieldAction(EJBUtil.stringToNum(formId));
		
		if (EJBUtil.isEmpty(mode) )
		{	mode="N";
		}

		if (EJBUtil.isEmpty(fieldAction))
		{
			fieldAction="";
		}	
		
		fieldActionInt = EJBUtil.stringToNum(fieldAction);
			
		if (mode.equals("M"))
		{  
			ArrayList arVal = new ArrayList();
			String keywordval = "";
			
			fsk = fldActionDaoInfo.getFieldActionDetails(fieldActionInt);
			
				//get data
				 selectedSourceFld = fsk.getFldId();
				 selectedActionType = fsk.getFldActionType();
				
				 htActionInfo = fsk.getFldActionInfo();			
				 selectedSourceFldInt = EJBUtil.stringToNum(selectedSourceFld);
				
				//get selected responses /conditions for the action field
				
				if (htActionInfo.containsKey("[VELTARGETID]"))	
				{
					
					arVal = (ArrayList) htActionInfo.get("[VELTARGETID]");
					for (int i = 0; i < arVal.size(); i++)
					{
						keywordval	= (String) arVal.get(i);
						if (! EJBUtil.isEmpty(keywordval))
						{	
							selectedVeltargetid = selectedVeltargetid + "[VELSEP]" + keywordval;
						}	
					}		
					if (selectedVeltargetid.length() > 0)
					{
						selectedVeltargetid= selectedVeltargetid + "[VELSEP]";
					}	
				
				}		
				if (htActionInfo.containsKey("[VELTARGET]"))	
				{
					
					arVal = (ArrayList) htActionInfo.get("[VELTARGET]");
					for (int i = 0; i < arVal.size(); i++)
					{
						keywordval	= (String) arVal.get(i);
						if (! EJBUtil.isEmpty(keywordval))
						{	
							selectedVeltarget = selectedVeltarget + "[VELSEP]" + keywordval;
						}	
					}		
					if (selectedVeltarget.length() > 0)
					{
						selectedVeltarget= selectedVeltarget + "[VELSEP]";
					}	
				
				}
				
			selectedDisptargetvalue = vfldAction.getSingleFieldActionKeywordField(fieldActionInt,"[VELTARGETID]");
			selectedDispsourcevalue = vfldAction.getSingleFieldActionKeywordField(fieldActionInt,"[VELSOURCEFLDID]");
			
									
			if (htActionInfo.containsKey("[VELSOURCE]"))	
				{
					arVal = (ArrayList) htActionInfo.get("[VELSOURCE]");
					for (int i = 0; i < arVal.size(); i++)
					{
						keywordval	= (String) arVal.get(i);
						if (! EJBUtil.isEmpty(keywordval))
						{	
							selectedVelSource = selectedVelSource + "[VELSEP]" + keywordval;
						}	
					}		
					if (selectedVelSource.length() > 0)
					{
						selectedVelSource= selectedVelSource + "[VELSEP]";
					}	
				}	
			if (htActionInfo.containsKey("[VELSOURCEFLDID]"))	
				{
					arVal = (ArrayList) htActionInfo.get("[VELSOURCEFLDID]");
					for (int i = 0; i < arVal.size(); i++)
					{
						keywordval	= (String) arVal.get(i);
						if (! EJBUtil.isEmpty(keywordval))
						{	
							selectedVelSourceFldId   = selectedVelSourceFldId  + "[VELSEP]" + keywordval;
						}	
					}		
					if (selectedVelSourceFldId.length() > 0)
					{
						selectedVelSourceFldId  = selectedVelSourceFldId   + "[VELSEP]";
					}	
							
				}			
				
				
		}
		sourcefieldIds = fldlibDao.getFieldLibId();
		sourcefieldNames = fldlibDao.getFldName();
		sourcefieldSystemIds = fldlibDao.getFldSysID();
		
		String sourceFldName = "";
		String sourceFldSystemId = "";
		Integer sourceFldId = null;
		int intFldId  = 0;	

		StringBuffer sbSource = new StringBuffer();
		
		///////////////////////////////////////////
		
		/*sbSource.append("<SELECT NAME=\"sourceFldDD\" onChange=\"populateKeyWordParams(document.fldAction)\">") ;

		for (int counter = 0; counter <= sourcefieldIds.size() -1 ; counter++)
		{
			sourceFldId = (Integer) sourcefieldIds.get(counter);
			intFldId =  sourceFldId.intValue() ;
			sourceFldSystemId = ( String) sourcefieldSystemIds.get(counter); 
			sourceFldName = (String) sourcefieldNames.get(counter); 
			
			if (selectedSourceFldInt == intFldId )
			{	

				sbSource.append("<OPTION SELECTED value = \""+ intFldId + "[VELSEP]" + sourceFldSystemId +"\">" + sourceFldName + "</OPTION>");			
			}	
			else
			{	
				sbSource.append("<OPTION value = "+ intFldId + "[VELSEP]" + sourceFldSystemId +">" + sourceFldName + "</OPTION>");			
			 }				
			
		}
			if (mode.equals("N"))
			{	
				sbSource.append("<OPTION value ='' SELECTED>Select an Option</OPTION>");			
			}	
			sbSource.append("</SELECT>");

		*/
			
		StringBuffer sbActionType = new StringBuffer();
		sbActionType.append("<SELECT NAME=\"actionType\">") ;
		
//		if (selectedVelReadonlyOrDisable.equals()
	
	
		String [] arrActionType = {"tempAdd","tempProduct","tempDiff","tempDivision","tempAvg",""};
		String [] arrActionTypeDisp = {LC.L_Sum_Of,LC.L_Product_Of,LC.L_Difference_Of,LC.L_Division_Of,LC.L_Average_Of,LC.L_Select_AnOption};/*String [] arrActionTypeDisp = {"Sum of","Product of","Difference of","Division of","Average of","Select an Option"};*****/
		
		String actionTypeVal = "";
		String actionTypeValDisp = "";
		
		for (int i=0; i < arrActionType.length; i++)
		{
			actionTypeVal = arrActionType[i];
			actionTypeValDisp = arrActionTypeDisp[i];
			
			if (actionTypeVal.equals(selectedActionType))
			{
				sbActionType.append("<OPTION value = \""+ actionTypeVal+"\" SELECTED>"+actionTypeValDisp+"</OPTION>");
			}
			else
			{
				sbActionType.append("<OPTION value = \""+ actionTypeVal+"\">"+ actionTypeValDisp+"</OPTION>");
			}	
		}
		
		sbActionType.append("</SELECT>");
			
%>

    <Form name="fldAction" id="calcFldActionFrm" method="post" action="updateFieldAction.jsp" onsubmit="if (validate(document.fldAction)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	
	<table width="99%" cellspacing="0" cellpadding="0" border="0">
  	<tr>
		<td ><P class="sectionHeadings"> <%=LC.L_Define_FldCalcu%><%--Define Field Calculation*****--%> </P> </td>		
	 </tr>   
	 </table>
	 <br>
 
	
	<Input type="hidden" name="mode" value=<%=mode%>>
	<Input type="hidden" name="formId" value=<%=formId%>>
	<Input type="hidden" name="codeStatus" value=<%=codeStatus%>>  
	<Input type="hidden" name="fieldActionId" value=<%=fieldAction%>>  

	<!--<Input type="hidden" name="actionType" value="tempGrey">  -->

	<!--<Input type="hidden" name="velsource" value="<%=selectedVelSource %>">  
	<Input type="hidden" name="velsourcefldid" value="<%=selectedSourceFldInt%>">  	 -->

	<table width="99%" cellspacing="2" cellpadding="2" border="0" class="basetbl midalign" >
	 <tr> 
      <td width="30%"><%=LC.L_Select_SourceFlds%><%--Select Source Fields*****--%><FONT class="Mandatory">* </FONT></td>
	       <!--<td width="60%"><%=sbSource.toString()%> </td>-->
          <td width="69%"><Input type="hidden" name="velsource" value="<%=selectedVelSource%>">
        <Input type="text" name="dispsourcevalue" READONLY value="<%=selectedDispsourcevalue%>">
        <Input type="hidden" name="velsourcefldid"  value="<%=selectedVelSourceFldId%>">
        <A href= "#" onClick="openSource(document.fldAction)"><%=LC.L_Select%><%--Select*****--%></A>
        </td>
        	
	  </tr>
  	 <tr> 
      <td ><%=LC.L_Select_CalcuType%><%--Select Calculation Type*****--%><FONT class="Mandatory">* </FONT></td>
        <td ><%=sbActionType.toString()%> </td>
	  </tr>  
  	 <tr> 
      <td ><%=LC.L_Select_TargetFlds%><%--Select Target Fields*****--%> <FONT class="Mandatory">* </FONT></td>
        <td ><Input type="hidden" name="veltarget" value="<%=selectedVeltarget%>">
        <Input type="text" name="disptargetvalue" READONLY value="<%=selectedDisptargetvalue%>">
        <Input type="hidden" name="veltargetid"  value="<%=selectedVeltargetid%>">
        <A href= "#" onClick="openTarget(document.fldAction)"><%=LC.L_Select%><%--Select*****--%></A>
        </td>
	  </tr>  
  </table>
  	  <br>

<% if (pageRight >= 6) { if(codeStatus.equals("WIP")){
						%>
						<table align="center"><tr>
	  <td align="center"><br />
				<button type="submit" id="submit_btn" ondblclick="return false"><%=LC.L_Submit%></button>
			</td> 
	  </tr></table>
     
<%}else{ 
%>

	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="editBoxFieldFrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include> 

<br>
	<% } } %>
		
  </Form>
  
<%
	} //end of if body for page right
	else
	{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%

	} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

}

%>
 <div class = "myHomebottomPanel"> 
 <jsp:include page="bottompanel.jsp" flush="true"/>
 </div>
</body>

</html>

