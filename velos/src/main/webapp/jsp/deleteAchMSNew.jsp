<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<jsp:include page="velos_includes.jsp"></jsp:include>
<%@page import="java.math.BigDecimal"%>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="js/jquery/common/scripts.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="./js/jquery/jquery.dataTables.js"></SCRIPT>
<!-- ONLY FOR NEW DESIGN -->
<link rel="stylesheet" href="./styles/font-awesome.min.css">
<style>
.custom-search-section input {
    height: 16px !important;
}
.custom-search-section {
    float: right;
}
.custom-inner-details {
    clear: both;
}
.custom-search-section {
    position: relative;
}
.custom-search-section i {
    float: right;
    position: absolute;
    right: 0px;
}
.custom-search-section input {
    height: 16px;
    background: #cccccc47;
}
.custom-search-section i {
    float: right;
    position: absolute;
    right: 11px;
    font-size: 16px;
    top: 6px;
    color: #1ab5da;
}
.custom-tab-content {
    border: 1px solid #ccc;
    box-shadow: 0px 4px 10px #ccc;
}
.custom-checkbox-section.hidden {
    width: 21px;
}
.browserEvenRow td:last-child, .browserOddRow td:last-child, .table-subheading td:last-child {
    text-align: right;
    padding-right: 40px !important;
}
.browserEvenRow td:first-child, .browserOddRow td:first-child {
    padding-left: 116px;
}
.mstone-id {
    text-align: right;
    padding-right: 35px !important;
}
.table-subheading .numberfield {
    padding: 5px 7px;
    background: #106cb7;
    color: #fff;
    border-radius: 5px;
    padding-left: 10px;
    padding-right: 10px;
}
.custom-tab-heading {
    float: left;
    padding: 9px 1px;
    margin-right: 22px;
    cursor: pointer;
}
.milestone-id {
    width: 144px;
}
.patient-id {
    text-align: left;
}
tr.browserEvenRow td,.browserOddRow td {
    vertical-align: middle;
    padding-top: 10px !important;
    padding-bottom: 10px !important;
    padding-left: 0px;
    padding-right: 0px;
}
.custom-tab-heading-active{
    background-color: transparent;
    color: #3ab54a;
    padding-left: 0px;
    padding-right: 0px;
}
.custom-tab-heading-active {
    border-bottom: 3px solid #3ab54a;
}
.custom-tab-content {
    clear: both;
}
.custom-tab-content {
    display: none;
}
.custom-tab-content-active {
    display: block;
}
.custom-tab-section:after{ content:"";display:block;clear:both;}
.custom-tab-content table {
    border: 0px;
    border-spacing: 0px;
}
.custom-tab-content tbody th {
    background: #5a5a5a !important;
    border: 0px;
    line-height: 12px;
    color: #fff !important;
    font-weight: normal !important;
    border: 0px !important;
}
.custom-tab-content td {
    border: 0px;
}
.custom-tab-content table {
    width: 100%;
}

.table-subheading td:first-child:after {
    content: "";
    position: absolute;
    background: #106cb7;
    width: 10px;
    left: 0px;
    top: 0px;
    bottom: 0px;
}
tr.table-subheading td {
    background: #ece9e9;
}
.custom-tab-content table td{ padding: 10px;}
span.count-bg {
    background: #106cb7;
    color: #fff;
    padding: 4px 8px;
    border-radius: 5px;
    font-size: 12px;
    letter-spacing: 0.5px;
    margin-left: 11px;
}
tr.table-subheading td:first-child {
    position: relative;
    padding-left: 20px;
}
tr.browserEvenRow, tr.browserEvenRow td {
    background: transparent !important;
    border: 0px;
    border-top: 1px solid #ccc;
    border-bottom: 1px solid #ccc;
}
span.custom-checkbox {
    border: 1px solid #9a9a9a;
    width: 14px;
    height: 14px;
    display: block;
}
.custom-checkbox-section {
        position: relative;
    float: left;
    margin-right: 39px;
    height: 16px;
    margin-top: 2px;
}
span.custom-checkbox {
    position: absolute;
    top: 0px;
}
.custom-checkbox-section input {
    position: relative;
    z-index: 99;
    /* opacity: 0; */
}
.custom-checkbox-section input {
    position: relative;
    z-index: 99;
    width: 16px;
    height: 20px;
    left: 0px;
    margin-top: -2px;
    margin-left: 0px;
    opacity: 0;
}
.custom-checkbox-section input:checked + span:after {
    content: "\2713";
    display: block;
    width: 20px;
    height: 20px;
    font-size: 19px;
    line-height: 11px;
    color: #106cb7;
    font-weight: bold;
}
.browserEvenRow td {
    border-right: 0px !important;
    border-left: 0px !important;
}
.main-heading {
    font-size: 17px;
    font-weight: bold;
    font-family: Arial, Helvetica, sans-serif;
    margin-bottom: 0px;
}
.custom-inner-details {
    background: #03a9f447;
    padding: 4px;
}
.custom-inner-details button {
    margin-left: 12px;
    border: 0px;
}
span.custom-cancelSelect {
    font-size: 18px;
    font-weight: bold;
    cursor: pointer;
}
.custom-inner-details{ display:none;}
html, body {
    height: 100%;
    overflow: auto;
    margin: 0px;
}
.custom-container{max-width:1170px; margin:0 auto;}
.custom-tab-content tbody th {
    padding: 10px;
}
.custom-search-section input {
    float: right;
    border-radius: 0px;
    padding-left: 12px;
    margin-bottom: 10px;
}
.custom-search-section input {
    float: right;
    margin-bottom: 10px;
    padding-left: 12px;
    height: inherit;
    padding: 2px 13px;
}
</style>
<!-- ONLY FOR NEW DESIGN END  -->



<SCRIPT  Language="JavaScript1.2">

	function fclose_to_role()
	{
		//window.opener.location.reload();
			setTimeout("self.close()",1000);
	}

	function toggleSelect(formobj,fld)
	{
		 selcount = parseInt(formobj.selectedCount.value);

	    if (fld.checked)
	    {
	    	selcount = selcount + 1;
	    }
	    else
	    {
	    	selcount = selcount - 1;
	    }
	//alert("selcount="+selcount);
	   if(selcount>0)
		{
		   $j('.custom-inner-details').show();
		}else
	    {
			$j('.custom-inner-details').hide();
	    }	
		$j('#tab2tr1td1 b').html(+selcount+' Items Selected');
	    formobj.selectedCount.value = selcount;

	}

 	function setSelected(formobj,counter,totaldetail,isChecked)
	{

		var selcount ;
		var finalCount;
		var k;

		finalCount = parseInt(counter) + parseInt(totaldetail);

		selcount = parseInt(formobj.selectedCount.value);

			if (isChecked)
			{
				for (k=counter;k<finalCount;k++)
				{
					formobj.mileDetailCheck[k].checked = true;
					selcount = selcount + 1;
				}
			}
			else
			{
				for (k=counter;k<finalCount;k++)
				{
					formobj.mileDetailCheck[k].checked = false;
					selcount = selcount - 1;
				}
			}


			alert("selcount1="+selcount);

		formobj.selectedCount.value = selcount;

	}

  function deleteSelected()
  {
	  return confirm('Are you sure you want to delete this record ?');

  }
</SCRIPT>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<body>
<jsp:useBean id="statusB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>
  <jsp:useBean id="mJB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>
  <jsp:useBean id ="userBean" scope="session" class="com.velos.eres.business.user.impl.UserBean"/>
  <jsp:useBean id ="userJB" scope="session" class="com.velos.eres.web.user.UserJB"/>

  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,java.text.*"%>
  <%@ page import = "com.aithent.audittrail.reports.AuditUtils"%>
  <%--
  String st="<script>document.writeln(dd)</script>";
  out.println("value="+st);
--%>
<%

	String src = null;

	String eSign = request.getParameter("eSign");
	String mode = request.getParameter("mode");

	String studyId = request.getParameter("studyId");

	if (StringUtil.isEmpty(mode))
	{
		mode = "N";
	}

	String srchAchvHd = request.getParameter("srchAchvHd")==null?"":request.getParameter("srchAchvHd");
	System.out.println(srchAchvHd);

HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
<%
//   	String oldESign = (String) tSession.getValue("eSign");
	String eventId = (String) tSession.getValue("eventId");

//	if(!oldESign.equals(eSign))
//	{
%>

<%
//	}
//	else   	{

	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");



%>

  <!-- Get the achieved milestones -->
 <%
 	if (mode.equals("N"))
 	{
	 int totalmilecount  = 0;
	 int totalCounter = 0;
	 double mileAmountForGroupMilestones = 0.00;
	 int iPayCount = 0;
	 int iInvCount = 0;
	 int achvdMileCount = 0;

 	%>

<!--popDefault -->
<DIV id="popDefault" class=""  >
<div class="custom-container">
<!--class="sectionHeadings" -->
 	<p id="sectionHeadings" class="main-heading"><%=LC.L_Achievements%><%--Achievements*****--%></p>

 	<table id="tab1" width="100%">
 		<tr id="tab1tr1">
 		<td id="tab1tr1td1">
 		<font class="mandatory"><%=MC.M_AchvRemove%><%--An achievement can be removed if it belongs to a milestone rule with a count of <1 and it has not been included in an Invoice or Payment record.*****--%>
 		</font>
 		</td>
 		</tr>
 	</table>
<form name="delAch" id="delAchMSfrm" action="deleteAchMSNew.jsp" method="post"onSubmit=" ripLocaleFromAll(); "  onkeydown="return event.key != 'Enter';" flush="true" >
		
		<!-- Table Top Section Start -->
		<div class="custom-table-details">
		   <!-- Search Section Start -->
		   <div class="custom-table-search">
		   <input type="hidden" id="studyId" name="studyId" value="<%=studyId%>"/>
		   </div>
		   <!-- Search Section End  -->
		</div>
		<!-- Table Top Section Start End -->
		
 	<%
 		// iterate through 5 milestone types

 		String[] arMilestoneType =  new String[4];
 		arMilestoneType[0] = "PM";
 		arMilestoneType[1] = "VM";
 		arMilestoneType[2] = "EM";
 		arMilestoneType[3] = "SM";
		//arMilestoneType[4] = "AM";//KM


 		String[] arMilestoneTypeHeader =  new String[4];
 		arMilestoneTypeHeader[0] = LC.L_Pat_StatusMstones/*Patient Status Milestones*****/;
 		arMilestoneTypeHeader[1] = LC.L_Visit_Mstones/*Visit Milestones*****/;
 		arMilestoneTypeHeader[2] = LC.L_Evt_Mstones/*Event Milestones*****/;
 		arMilestoneTypeHeader[3] = LC.L_Std_Mstones/*Study Milestones*****/;
 		//arMilestoneTypeHeader[4] = "Additional Milestones";


 		String displayMilestoneHeader ="";
        %>
        <!-- Tab Heading Start -->
 		<div class="custom-tab-section">
        <%
 		// Custom Tab Heading Start
 		for (int t = 0; t <arMilestoneType.length;t++)
 		{
 			String tabHeadingActive;
 			if(t==0)
 			{
 			 tabHeadingActive = "custom-tab-heading-active";
 			}else
 			{
 			  tabHeadingActive = "";
 			}	
 		%>
 		<!-- class="portlet-header portletstatus ui-widget ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" -->
				<div id="mileAchvTabcontent<%=t%>" data-tab-heading="<%=t%>" onclick="toggleDiv('mileAchvTab<%=t%>');" class="custom-tab-heading <%=tabHeadingActive%>" >
				<%=arMilestoneTypeHeader[t]%>
				</div>
	
 		<%
 		// Custom Tab Heading End 
 		}
 		%>
 			</div>		
			<!-- Tab Heading End -->
	  <div class="custom-search-section">
	   	<input type="text" name="srchAchv" id="srchAchv" placeholder="Search achievements"  value="" size="25" onkeypress="searchAchv()"/>
 	   <input type="hidden" name="srchAchvHd" id="srchAchvHd" />
 	   <i class="fa fa-search" aria-hidden="true"></i>
	  </div>	
	  <!-- Table other details start -->
		   <div class="custom-inner-details">
		     <!-- Inner Table  -->
		    <table id="tab2" width="100%">
	 		<tr id="tab2tr1">
	 		<td id="tab2tr1td1"><b>0 Item Selected</b> <button id="delSelect" type="submit"  onClick="deleteSelected();"><i class="fa fa-times-circle-o" aria-hidden="true"></i> <%=LC.L_Del_Selected%></button></td>
			<td id="tab2tr1td3">
			<span class="custom-cancelSelect">&#10005;</span>
			</td>
	 		</tr>
	      </table>
		     <!-- Inner Table End -->
		   </div>
		   <!-- Table other details end -->	
 		<div class="custom-tab-content-section">
 		<% 
 		for (int t = 0; t <arMilestoneType.length;t++)
 		{

	 		MileAchievedDao mile = new MileAchievedDao();
	 		String tabContentActive;
 			if(t==0)
 			{
 				tabContentActive = "custom-tab-content-active";
 			}else
 			{
 				tabContentActive = "";
 			}	
	 		%>
	 		<!--  portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all -->
	 		<div id="mileAchvTabDIV<%=t%>"  class="custom-tab-content-<%=t%> custom-tab-content <%=tabContentActive%>">
	 			
	 		<%
	 		MilestoneDao milestoneDao = new MilestoneDao();
	 		ArrayList arMilestoneIds = new ArrayList();
	 		ArrayList arMilestoneRuleDescs = new ArrayList();
	 		ArrayList arMilestoneAchievedCounts = new ArrayList();
	 		ArrayList arCountPerMilestone = new ArrayList();
	 		ArrayList arMileAmounts = new ArrayList();
	 		ArrayList arMileTotalInvoicedAmounts = new ArrayList();



	 		String mileStoneId = "";
	 		Hashtable htAchieved = new Hashtable();
	 		Hashtable moreParam = new Hashtable();
			
	 		int milecount = 0;
	 		int mileDetailCount = 0;
	 		String mileAchCount = "0";
	 		String mileDesc = "";
	 		String prevMileDesc="";
	 		int mileDescCount=0;
	 		String mileAchDate="";
		  	int countPerMilestone  = 0;
		  	
			
		   double calcAmount = 0.00;
		   double mileAmount = 0.00;
		   BigDecimal calcAmounts = new BigDecimal("0.0");
		   BigDecimal mileAmt = new BigDecimal("0.0");
	 		mile = mJB.getALLAchievedMilestones(EJBUtil.stringToNum(studyId ), arMilestoneType[t],moreParam);


			milestoneDao = mile.getMilestoneDao();



	 		htAchieved = mile.getHtAchieved();
 	 		arMilestoneIds = mile.getMilestone();

		    arMilestoneRuleDescs = mile.getMilestoneDesc();
			arMilestoneAchievedCounts = milestoneDao.getMilestoneAchievedCounts();
	 		arCountPerMilestone  = milestoneDao.getMilestoneCounts();
	 		arMileAmounts =  milestoneDao.getMilestoneAmounts();




	 		if (arMilestoneIds != null)
	 		{
	 			milecount = arMilestoneIds.size();

	 		}
	 		else
	 		{
		 		milecount  = 0;
	 		}

	 		totalmilecount = totalmilecount + milecount;

	 		// for each milestone, get the milestone achieved details %>
	 		<!-- Tab Content Start Here  -->
	 		<div id="mileAchvTab<%=t%>">
	 		         			<table>
	 								<tr>

	        <%-- <th>=LC.L_Select--%><%--Select*****</th> --%>
	        <th id="tab2th1" class="milestone-id"><%=LC.L_Milestone%><%--Milestone*****--%></th>
	      <%  if(!arMilestoneType[t].equals("SM"))
	      {%>
	      <th id="tab2th2" class="patient-id"><%=LC.L_Patient_Id%><%--L_Patient_Id***** --%></th>
	      <% } else { %>
	         <th id="tab2th2" class="patient-id"><%=LC.L_Ach_Date%><%--Achievement Date*****--%> </th>
	        <%} %>
	        <%-- <th>=LC.L_Ach_Date--%><%--Count***** </th> --%>
	        <%-- <th><%=LC.L_Count%>Count***** </th> --%>
	        <th id="tab2th3" class="mstone-id"><%=LC.L_Mstone_Amt%><%--Milestone Amount*****--%></th>

	      </tr>
	 			</table>
 			<% 
	 		for (int i =0; i < milecount ; i++)
	 		{
	 			mileDesc = (String ) arMilestoneRuleDescs.get(i);
	 			
	 			mileStoneId =  (String)arMilestoneIds.get(i) ;

				MileAchievedDao ach = new MileAchievedDao();
				ArrayList patients = new ArrayList();
				ArrayList ids = new ArrayList();
				ArrayList patientCodes = new ArrayList();
				ArrayList achievedOn = new ArrayList();

				ArrayList arInvCount =  new ArrayList();
				ArrayList arPayCount =  new ArrayList();



				if (htAchieved.containsKey(mileStoneId))
				{

				countPerMilestone =  EJBUtil.stringToNum((String) arCountPerMilestone.get(i));
				mileAchCount = (String) arMilestoneAchievedCounts.get(i);
				mileAmount = Double.parseDouble((String) arMileAmounts.get(i));
				mileAmt = new BigDecimal(mileAmount);



				if (countPerMilestone > 1)
				{
			 		mileAmountForGroupMilestones = mileAmount/countPerMilestone;
			 		mileAmt = new BigDecimal(mileAmountForGroupMilestones);
			 	}

					// 16Mar2011 @Ankit #5924
					if(mileAchCount==null)
					{
						mileAchCount="0";
					}
					ach = (MileAchievedDao) htAchieved.get(mileStoneId);
					ids = ach.getId();
					patients = ach.getPatient();
					mileDetailCount = ids.size();
					patientCodes = ach.getPatientCode();
					calcAmount = EJBUtil.stringToNum(mileAchCount) * mileAmount;
					calcAmounts=new BigDecimal(calcAmount);
					achievedOn = ach.getAchievedOn();
					arInvCount = ach.getArInvCount();
					arPayCount = ach.getArPaymentCount();

					if (ids.size() == 1 && StringUtil.isEmpty((String)ids.get(0)))
					{
					  mileAchCount = "0";
					  mileDetailCount = 0;
					calcAmount = 0.00;
					calcAmounts=new BigDecimal(calcAmount);
					}

				}
				else
				{
					mileAchCount = "0";
					mileDetailCount = 0;

					if(!arMilestoneType[t].equals("AM"))
					{
						calcAmount = 0.00;
						calcAmounts=new BigDecimal(calcAmount);
					}
					else
					{
						calcAmount = mileAmount;
						calcAmounts=new BigDecimal(calcAmount);
					}


				}%>
	
	 			<table id="tab3<%=i%>" width="100%" border="1px;">
	 			
	 			<% if(!mileDesc.equals(prevMileDesc) || prevMileDesc.equals("")){
	 				prevMileDesc = mileDesc;
	 				achvdMileCount=0;
	 					}%>
	 			
	 			<tr id="tab3<%=i%>tr2" class="table-subheading">
		 			<td id="tab3<%=i%>tr2td1"><b><%=mileDesc %></b>
		 			<span id="div<%=t%><%=i%>tab3<%=i%>tr1td1" class="achvMileCnt"><%=arMilestoneAchievedCounts.get(i) %></span>
		 			<%
		 				//added 1==2 so that checkbox is never shows, will impement this later
		 			if (mileDetailCount > 1 && countPerMilestone <= 1 && 1==2) {%>
		 			 <input type="checkbox" onClick= "setSelected(document.delAch,'<%=totalCounter%>','<%=mileDetailCount%>',this.checked )" name="selectMilestone" value="<%=mileStoneId%>"/>

		 			<% }else{ %>
		 			&nbsp;
		 			<%} %>
		 			</td>
		 			<td id="tab3<%=i%>tr2td2">&nbsp;</td>
				 	<td id="tab3<%=i%>tr2td3"><span id="tab3<%=i%>tr2td3span<%=i%>" class="numberfield" data-unitsymbol="" data-formatas="currency"><%=calcAmounts%></span>
				 		<input type= "hidden"  name="mileStoneId"  value="<%=mileStoneId%>" />
				 		<input type= "hidden"  name="countPerMilestone"  value="<%=countPerMilestone%>" />
				 		<input type= "hidden"  name="mileAmountDue"  value="<%=calcAmount%>" />
				 		<input type= "hidden"  name="mileAchCount"  value="<%=mileAchCount%>" />
 				 	 	<input type= "hidden"  name="mileDetailCount"  value="<%=mileDetailCount%>" />
			 			<input type= "hidden"  name="mileAmountForGroupMilestones"  value="<%=mileAmountForGroupMilestones%>" />
				 	</td>

	 			</tr>


	 			<%
	 				for (int k = 0; k < mileDetailCount; k++)
	 				{

	 					iPayCount = ((Integer)arPayCount.get(k)).intValue();
	 					iInvCount = ((Integer)arInvCount.get(k)).intValue();

	 				%>

	  				<input type="hidden" name="mileDetFKAchieved<%=mileStoneId%>" id="mileDetFKAchieved<%=mileStoneId%>"  value="<%=(String)ids.get(k)%>" />



		 				<%
						if(k%2==0){
					%>
						<TR id="tab3<%=i%>tr3" class="browserEvenRow">
					<%
					}else{
					%>
						<TR id="tab3<%=i%>tr3" class="browserOddRow">
					<%
					}
					%>
					 <td id="tab3<%=i%>tr3td1" colspan="2">
					 <% if ( countPerMilestone <= 1 && iInvCount == 0 && iPayCount ==0) { %>
					   <div class="custom-checkbox-section">
					     <input type="checkbox" name="mileDetailCheck"  id="mileDetailCheck" onClick="toggleSelect(document.delAch,this);" value="<%= (String)ids.get(k)%>" />
                         <span class="custom-checkbox"></span>
                       </div>  
					   <%
					   		totalCounter = totalCounter + 1;
					   }else{ %>
					     <div class="custom-checkbox-section hidden">     
                       </div> 
					   <%} %>
					   <%= (String) patientCodes.get(k)%>
					 </td>			
		 			<td id="tab3<%=i%>tr3td3"><span id="tab3<%=i%>tr3td3span<%=i%>" class="numberfield" data-unitsymbol="" data-formatas="currency">
		 			<% achvdMileCount=achvdMileCount+1;
				 		if (countPerMilestone <= 1)
				 		{
				 		%>
				 			<%=mileAmt%>

				 		<%
				 		}
				 		else { %>

					 		<%=mileAmt%>

				 		<%}

				 	%>
				 	</span></td>

				  		 			</tr>


	 				<%

	 				}

					%>
					</table>
					<script>
		   	 		//$j(document).ready(function(){
						//$j("#div<%=t%><%=i%>tab3<%=i%>tr1td1").html(<%=achvdMileCount%>);
					//});
				</script>
					<% 
	 			// end of for
	 		}%></div>
	 		<!-- Tab Content Start End  -->
				</div>
				<%  }	// for milestone types


 %>
</div>

<P>
<br>
<input type= "hidden"  name="totalmilecount" id="totalmilecount"  value="<%=totalmilecount%>" />
<input type= "hidden"  name="selectedCount" id="selectedCount"  value="0" />
<input type= "hidden"  name="mode" id="mode"  value="F" />
<input type= "hidden"  name="totalDetailCounter" id="totalDetailCounter"  value="<%=totalCounter%>" />



</P>
	

</form>
</div>
</div>
<%

 	}  //mode = N
 	else //final mode
 	{
 		int totalDetailCounter = 0;

 		totalDetailCounter = EJBUtil.stringToNum( request.getParameter("totalDetailCounter") );

 		String selectedACH[] = null ;

 		if (totalDetailCounter == 1)
 		{
 			selectedACH= new String[1];
			selectedACH[0] = request.getParameter("mileDetailCheck");

 		}
 		else if (totalDetailCounter > 1)
 		{
 			selectedACH	= request.getParameterValues("mileDetailCheck");
 		}

 		 int ret = 0;
 		String condition="";
    	String ridVal = "";
 		for(int i=0; i<selectedACH.length; i++){
      		condition = condition + ", " + selectedACH[i];
		 }
		condition = "pk_mileachieved IN ("+condition.substring(1)+")";
		Hashtable<String, ArrayList<String>> rowValues = AuditUtils.getRowValues("ER_MILEACHIEVED",condition,"eres");/*Fetches the RID from the given table */
		ArrayList<String> rowRID  = (ArrayList<String>)rowValues.get(AuditUtils.ROW_RID_KEY);
		String userFullName= "";
		userJB.setUserId(StringUtil.stringToNum(usr));
		userBean = userJB.getUserDetails();
		userFullName = usr + ", " + userBean.getUserLastName() + ", " + userBean.getUserFirstName();

 		 if (selectedACH != null && selectedACH.length > 0) //delete!
 		 {
 		 	//Modified for INF-18183 ::: Ankit
 		 	ret = mJB.deleteMSAch(selectedACH, AuditUtils.createArgs(session,"",LC.L_Milestones));
 		 }

 		 if (ret == 0)
 		 {
 			for(int i=0; i<rowRID.size(); i++)
				{
 				ridVal = rowRID.get(i);
 				AuditUtils.updateAuditROw("eres", userFullName, ridVal, "D");
				}
 		   %>
 		   <br><br><br><br><br> <p class = "successfulmsg" align = center>  <%=MC.M_DataWas_DelSucc%><%--Data was deleted successfully*****--%> </p>
 		   <script>
 		     fclose_to_role();
 		 	 window.opener.location.reload();
 		   </script>
 		   <%
 		 }
 		 else
 		 { %>
 		    <br><br><br><br><br> <p class = "successfulmsg" align = center>  <%=MC.M_DataNotDel_Succ%><%--Data was not deleted successfully*****--%> </p>

 		 <%

 		 }
 	}

//}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

<script>
<!---// ONLY for NEW Mockup ---->
$j(document).ready(function(){
// Tab Heading
	$j('.custom-tab-heading').click(function(){
		  $j('div').removeClass('custom-tab-heading-active');
		  $j('div').removeClass('custom-tab-content-active');
		  $j(this).addClass('custom-tab-heading-active');
		  var tabContentActive = $j(this).attr('data-tab-heading');
		  //alert(tabContentActive);
		  $j('.custom-tab-content-'+tabContentActive).addClass('custom-tab-content-active');
	});
		});
// Tab Heading End	
 
<!----// ONLY FOR NEW MOCK ------>
</script>
<div id="myHomebottomPanel" class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</HTML>
