<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page
	import="com.velos.eres.service.util.*,java.util.ArrayList,java.util.HashMap,org.json.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.*,com.velos.eres.service.util.StringUtil"%>
<%@page import="org.json.JSONArray"%>

<%@page import="java.util.Collections"%><jsp:useBean id="sessionmaint" scope="session"
	class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="protVisitB" scope="request"
	class="com.velos.esch.web.protvisit.ProtVisitJB" />
<jsp:useBean id="visitdao" scope="request"
	class="com.velos.esch.business.common.ProtVisitDao" />
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"></jsp:useBean>
<jsp:useBean id="assoclistdao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>

<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	HttpSession tSession = request.getSession(true);

	JSONObject jsObj = new JSONObject();
	if (!sessionmaint.isValidSession(tSession)) {
		response.sendError(HttpServletResponse.SC_FORBIDDEN,
				"SESSION_TIME_OUT");

		jsObj.put("error", new Integer(-1));
		jsObj.put("errorMsg", MC.M_UsrNot_LoggedIn/*User is not logged in.*****/);
		out.println(jsObj.toString());
		return;
	}
	//Validate the protocal ID
	String protocolId = request.getParameter("protocolId");
	String calStatus = request.getParameter("calstatus");

	if (protocolId == "" || protocolId == null
			|| protocolId.equals("null") || protocolId.equals("")) {
		// A valid protocol ID is required; print an error and exit
		jsObj.put("error", new Integer(-2));
		jsObj.put("errorMsg", MC.M_PcolId_Invalid/*Protocol ID is invalid.*****/);
		out.println(jsObj.toString());
		return;
	}
	ArrayList ids = new ArrayList();
	ArrayList descs = new ArrayList();
	String calStatDesc = ""; 
	SchCodeDao schcodedao = new SchCodeDao();

	SchCodeDao timePointDao = new SchCodeDao();
	String noTimePointdefined = "", fixedPointdefined = "", depnPointdefined = "";
	int ntpd=0, ftp=0, dtp = 0;
	
	try{
		timePointDao.getCodeValues("timepointtype");
		ArrayList pkList = timePointDao.getCId();
		ArrayList descList = timePointDao.getCDesc();
		jsObj.put("timePointListPk", pkList);
		jsObj.put("timePointListDesc", descList);
		
		for (int indx=0; indx < pkList.size(); indx++){
			String subType = (timePointDao.getCSubType()).get(indx).toString();
			if (!StringUtil.isEmpty(subType)){
				if ("NTPD".equals(subType)){
					noTimePointdefined = descList.get(indx).toString();
					ntpd = StringUtil.stringToNum(pkList.get(indx).toString());
				}
				if ("FTP".equals(subType)){
					fixedPointdefined = descList.get(indx).toString();
					ftp = StringUtil.stringToNum(pkList.get(indx).toString());
				}
				if ("DTP".equals(subType)){
					depnPointdefined = descList.get(indx).toString();
					dtp = StringUtil.stringToNum(pkList.get(indx).toString());
				}
			}	
		}
	}catch(Exception exp)
	{
	 exp.printStackTrace();	
	}
	jsObj.put("noTimePointdefined", ntpd);
	jsObj.put("fixedPointdefined", ftp);
	jsObj.put("depnPointdefined", dtp);
	String calledFrom = request.getParameter("calledFrom");
	String calAssoc = request.getParameter("calassoc");
	//End here protocal validation

	String userIdFromSession = (String) tSession.getValue("userId");
	String accId = (String) tSession.getValue("accountId");
	String duration = (String) request.getParameter("duration");
	String fromPage = (String)request.getParameter("fromPage");
	boolean newAccessRight=false;
	String pageRight = (String)request.getParameter("pageRight");
	if (isAccessibleFor(EJBUtil.stringToNum(pageRight),'N')){
		newAccessRight=true;
	}
	//Set calender related information

	jsObj.put("protocolId", protocolId);
	jsObj.put("calStatus", calStatus);
	jsObj.put("duration", duration);

	SchCodeDao schCalStatus = new SchCodeDao();
	String offlineStatusDesc = "";
	offlineStatusDesc = schCalStatus.getCodeDescription(schCalStatus.getCodeId("calStatStd", "O"));
	jsObj.put("offlineStatusDesc", offlineStatusDesc);
	
	String calenderId =(String)session.getAttribute("wrongVisitCalender");
	calenderId=calenderId==null? "":calenderId;
	jsObj.put("wrongVisitCalender", calenderId);
	
	ArrayList wrongVisitArray = new ArrayList();
	wrongVisitArray =(ArrayList)session.getAttribute("wrongVisitArray");
	wrongVisitArray=wrongVisitArray==null? new ArrayList():wrongVisitArray;

	jsObj.put("wrongVisitArray", wrongVisitArray);
	
	ArrayList wrongVisitNameArray = new ArrayList();
	wrongVisitNameArray =(ArrayList)session.getAttribute("wrongVisitNameArray");
	wrongVisitNameArray=wrongVisitNameArray==null? new ArrayList():wrongVisitNameArray;

	jsObj.put("wrongVisitNameArray", wrongVisitNameArray);
	//End here calender setting values
	ArrayList fieldsList = new ArrayList();
	ArrayList durUnitAList = new ArrayList();
	durUnitAList.add(0,LC.L_Days/*Days*****/);
	durUnitAList.add(LC.L_Weeks/*Weeks*****/); //Ak:Fixed BUG#7438
	durUnitAList.add(LC.L_Months/*Months*****/);
	durUnitAList.add(LC.L_Years/*Years*****/);

	jsObj.put("durUnitAList", durUnitAList);
	jsObj.put("durUnitBList", durUnitAList);

	ArrayList intervalUnitList = new ArrayList();
	intervalUnitList.add(0, LC.L_Select_AnOption/*Select an Option*****/);
	intervalUnitList.add(LC.L_Days);
	intervalUnitList.add(LC.L_Weeks);
	intervalUnitList.add(LC.L_Months);
	jsObj.put("intervalUnitList", intervalUnitList);

	JSONArray jsColArray = new JSONArray();
	{

		JSONObject jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "visitSeq");// SM:Bug#9308
		jsObjTemp1.put("label",LC.L_Serial/*visitSeq*****/+" #");
		//jsObjTemp1.put("hidden", "true");
		fieldsList.add("visitSeq");
		jsColArray.put(jsObjTemp1);

		jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "copyVisit");
	   	jsObjTemp1.put("label",LC.L_Copy);  //Ak:Added localization support for PCAL-20801
    	jsObjTemp1.put("action", "copy");
    	if(!newAccessRight || calStatus.equals("F")||calStatus.equals("A")||calStatus.equals("D")){
    		jsObjTemp1.put("hidden", "true"); 
    	}
    	fieldsList.add("copyVisit");
    	jsColArray.put(jsObjTemp1);

		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "visitNumber");
		jsObjTemp1.put("label",LC.L_Visitnumber/*visitNumber*****/);
		jsObjTemp1.put("hidden", "true");
		fieldsList.add("visitNumber");
		jsColArray.put(jsObjTemp1);
		
		jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "delete");
    	jsObjTemp1.put("label", LC.L_Delete/*Delete*****/);
    	if(calStatus.equals("F")||calStatus.equals("A")||calStatus.equals("D")){
    		jsObjTemp1.put("hidden", "true"); 
    	}
    	jsObjTemp1.put("action", "delete");
    	fieldsList.add("delete");
    	jsColArray.put(jsObjTemp1);
 

    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "hideFlag");
    	jsObjTemp1.put("label", LC.L_HideOrUnhide/*Hide/Unhide*****/);
     	if(!(calStatus.equals("O"))){
    	  jsObjTemp1.put("hidden", "true"); 
      	}
    	fieldsList.add("hideFlag");
    	jsObjTemp1.put("action", "hide");
    	jsColArray.put(jsObjTemp1);
    	
    		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "visitName");
		jsObjTemp1.put("label",	LC.L_Visit_Name/*Visit Name*****/+" <FONT class='mandatory'><sup>*</sup></FONT>");			
		jsObjTemp1.put("type", "varchar");
		jsObjTemp1.put("formatter", "customVarchar");
		fieldsList.add("visitName");
		jsColArray.put(jsObjTemp1);

		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "description");
		jsObjTemp1.put("label", LC.L_Description/*Description*****/);
		jsObjTemp1.put("type", "varchar");
		jsObjTemp1.put("formatter", "customVarchar");
		fieldsList.add("description");
		jsColArray.put(jsObjTemp1);
		
		
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "offlineFlag");
		jsObjTemp1.put("label", LC.L_Offlineflag/*offlineFlag*****/);
		jsObjTemp1.put("hidden", "true");
		fieldsList.add("offlineFlag");
		jsColArray.put(jsObjTemp1);
		
		//Nesting of header 'Visit Window'

		//Children of before visit window

		JSONArray jsBeforeChildArray = new JSONArray();
		JSONObject jsBeforeChildObj = new JSONObject();
		jsBeforeChildObj.put("key", "beforenum");
		jsBeforeChildObj.put("className", "hiddenHeader");//AK:Fixed BUG#7006
		jsBeforeChildObj.put("type", "integer");
		jsBeforeChildObj.put("formatter", "customInteger");
		fieldsList.add("beforenum");
		jsBeforeChildArray.put(jsBeforeChildObj);

		jsBeforeChildObj = new JSONObject();
		jsBeforeChildObj.put("key", "durUnitA");
		jsBeforeChildObj.put("className", "hiddenHeader"); //AK:Fixed BUG#7006
		fieldsList.add("durUnitA");
		jsBeforeChildArray.put(jsBeforeChildObj);

		///Before tab
		JSONArray jsChildrenArray = new JSONArray();
		JSONObject jsChildObj = new JSONObject();
		jsChildObj.put("key", "beforeLabel");
		jsChildObj.put("label", LC.L_Before/*Before*****/);
		jsChildObj.put("children", jsBeforeChildArray);
		jsChildrenArray.put(jsChildObj);

		//children of'After' visit window

		JSONArray jsAfterChildArray = new JSONArray();
		JSONObject jsAfterChildObj = new JSONObject();
		jsAfterChildObj.put("key", "afternum");
		jsAfterChildObj.put("className", "hiddenHeader"); //AK:Fixed BUG#7006
		jsAfterChildObj.put("type", "integer");
		jsAfterChildObj.put("formatter", "customInteger");
		fieldsList.add("afternum");
		jsAfterChildArray.put(jsAfterChildObj);

		jsAfterChildObj = new JSONObject();
		jsAfterChildObj.put("key", "durUnitB");
		jsAfterChildObj.put("className", "hiddenHeader"); //AK:Fixed BUG#7006
		fieldsList.add("durUnitB");
		jsAfterChildArray.put(jsAfterChildObj);

		//Children of After tab
		jsChildObj = new JSONObject();
		jsChildObj.put("key", "afterLabel");
		jsChildObj.put("label", LC.L_After/*After*****/);
		jsChildObj.put("children", jsAfterChildArray);
		jsChildrenArray.put(jsChildObj);

		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "visitWindow");
		jsObjTemp1.put("label",LC.L_Visit_Window/*Visit Window*****/);
		fieldsList.add("visitWindow");
		jsObjTemp1.put("children", jsChildrenArray);
		jsColArray.put(jsObjTemp1);
        
        
        		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "visitId");
		jsObjTemp1.put("label", LC.L_VisitID/*visitId*****/);
		jsObjTemp1.put("hidden", "true");
		fieldsList.add("visitId");
		jsColArray.put(jsObjTemp1);
		
		
		//Header define for no Interval
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "noInterval");
		jsObjTemp1.put("label",LC.L_Time_Point_Type/*No Interval*****/);
		fieldsList.add("noInterval");
		jsColArray.put(jsObjTemp1);
		
		//Set a hidden field for noIntervalId
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "noIntervalId");
		jsObjTemp1.put("label","No Interval Id"/*noIntervalId*****/);
		jsObjTemp1.put("hidden", "true");
		fieldsList.add("noIntervalId");
		jsColArray.put(jsObjTemp1);
		
		//Set a hidden field for NoTimePointdefined
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "noTimePointId");
		jsObjTemp1.put("label","No Time Point Id"/*NoTimePointdefined*****/);
		jsObjTemp1.put("hidden", "true");
		fieldsList.add("noTimePointId");
		jsColArray.put(jsObjTemp1);
		
		//Declare children for label 'Interval A'

		jsChildrenArray = new JSONArray();
		jsChildObj = new JSONObject();
		jsChildObj.put("key", "months");
		jsChildObj.put("label", LC.L_Month/*Month*****/);
		jsChildObj.put("type", "integer");
		jsChildObj.put("formatter", "customInteger");
		fieldsList.add("months");
		jsChildrenArray.put(jsChildObj);

		jsChildObj = new JSONObject();
		jsChildObj.put("key", "weeks");
		jsChildObj.put("label",LC.L_Week/*Week*****/);
		jsChildObj.put("type", "integer");
		jsChildObj.put("formatter", "customInteger");
		fieldsList.add("weeks");
		jsChildrenArray.put(jsChildObj);

		jsChildObj = new JSONObject();
		jsChildObj.put("key", "days");
		jsChildObj.put("type", "integer");
		jsChildObj.put("formatter", "customInteger");
		jsChildObj.put("label",LC.L_Day/*Day*****/);
		fieldsList.add("days");
		jsChildrenArray.put(jsChildObj);

		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "intervalA");
		jsObjTemp1.put("label",LC.L_FixedTime_Point/*Interval A*****/);
		jsObjTemp1.put("children", jsChildrenArray);
		jsColArray.put(jsObjTemp1);

		//Define header for Interval B

		jsChildrenArray = new JSONArray();
		jsChildObj = new JSONObject();
		jsChildObj.put("key", "insertAfterInterval");
		jsChildObj.put("label", LC.L_Interval/*Interval*****/);
		fieldsList.add("insertAfterInterval");
		jsChildObj.put("type", "integer");
		jsChildObj.put("formatter", "customInteger");
		jsChildrenArray.put(jsChildObj);

		jsChildObj = new JSONObject();
		jsChildObj.put("key", "intervalUnit");
		fieldsList.add("intervalUnit");
		jsChildObj.put("label",LC.L_Mwd/*M/W/D*****/);
		//fieldsList.add("intervalUnit");
		jsChildrenArray.put(jsChildObj);

		jsChildObj = new JSONObject();
		jsChildObj.put("key", "afterText");
		fieldsList.add("afterText");
		jsChildObj.put("label", " ");
		jsChildrenArray.put(jsChildObj);

		jsChildObj = new JSONObject();
		jsChildObj.put("key", "insertAfter");
		fieldsList.add("insertAfter");
		jsChildObj.put("label", LC.L_Visit_Name/*Visit Name*****/);
		jsChildrenArray.put(jsChildObj);

		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "intervalB");
		jsObjTemp1.put("label",LC.L_DepndTime_Point/*Interval B*****/+" <BR/>("+MC.M_PlsSv_VstSpec/*Please save visits before specifying*****/+")");
		jsObjTemp1.put("className", "myfield");
		jsObjTemp1.put("children", jsChildrenArray);
		jsColArray.put(jsObjTemp1);
		
		//Header define for Interval in days #Fix 7009 ,BK,OCT/17/2011
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "interval");
		jsObjTemp1.put("label",LC.L_CalculatedTime_Point/*"Interval"/*****/);
		fieldsList.add("interval");
		jsColArray.put(jsObjTemp1);
		
		//Set a hidden field for InsertAfterId
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "insertAfterId");
		jsObjTemp1.put("label",LC.L_InsertafterID/*insertAfterId*****/);
		jsObjTemp1.put("hidden", "true");
		fieldsList.add("insertAfterId");
		jsColArray.put(jsObjTemp1);
		
		//Set a hidden field for isFakeVisit
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "isFakeVisit");
		jsObjTemp1.put("label","isFakeVisit");
		jsObjTemp1.put("hidden", "true");
		fieldsList.add("isFakeVisit");
		jsColArray.put(jsObjTemp1);
		
		//Set a hidden field for real Fake Visit
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "isRealFakeVisit");
		jsObjTemp1.put("label","isRealFakeVisit");
		jsObjTemp1.put("hidden", "true");
		fieldsList.add("isRealFakeVisit");
		jsColArray.put(jsObjTemp1);
		
		//AK:Added for enhancement PCAL-20801
		jsObjTemp1 = new JSONObject(); 
		jsObjTemp1.put("key", "fromCopyVisitId");
		jsObjTemp1.put("hidden", "true");
		fieldsList.add("fromCopyVisitId");
		jsColArray.put(jsObjTemp1);
		
	

	}
	int visitSeq = 0;
	//Set fields array 
	jsObj.put("fieldsArray", fieldsList);
  
	//Ak:Added for Enhancement:20353,20354 Calender status Description
	if (calledFrom.equals("P") || calledFrom.equals("L")) {
				calStatDesc = schcodedao.getCodeDescription(schcodedao
						.getCodeId("calStatLib", calStatus));
	} else {
				calStatDesc = schcodedao.getCodeDescription(schcodedao
						.getCodeId("calStatStd", calStatus));
	}
	jsObj.put("calStatDesc",calStatDesc);
	//List of Datatable data
	ArrayList dataList = new ArrayList();

	//Fetching Visit Data
	ArrayList visitIds = null;
	ArrayList visitNames = null;
	ArrayList displacements = null;
	ArrayList numOfDaysList = null;
	ArrayList numOfWeekList = null;
	ArrayList numOfMonthsList = null;
	ArrayList intervalList = new ArrayList();
	String visitName = null;
	ArrayList insertAftVisitPks = new ArrayList();
	ArrayList insertAftVisitIds = new ArrayList();
	ArrayList insertAftVisitNames = new ArrayList();
	visitdao = protVisitB.getProtocolVisits(EJBUtil
			.stringToNum(protocolId));
	visitIds = visitdao.getVisit_ids();
	visitNames = visitdao.getNames();
	displacements = visitdao.getDisplacements();
	numOfDaysList = visitdao.getDays();
	numOfWeekList=visitdao.getWeeks();  //Ak:Foxed BUG#8166
	numOfMonthsList=visitdao.getMonths();
	ArrayList indexArray= new ArrayList();
	
	//Set the value of Data
	for (int j = 0; j < visitIds.size(); j++) {
		String visitId = null;
		String visitname = null;
		String displacement = "";
		String numOfDays = "";
		String insertAfterId = "";
		Integer numOfWeeks =0;
		Integer numOfMonths =0;
		Boolean weekMonthFlag= false;
		Integer reCalDisplacment=0;
		displacement = displacements.get(j).toString();
		visitId = visitIds.get(j).toString();
		visitname = visitNames.get(j).toString();
		if (numOfDaysList.get(j) != null)
			numOfDays = numOfDaysList.get(j).toString();
		
		//AK:Added for the bug#8166
		if(Integer.parseInt(displacements.get(j).toString())==1){
			if (numOfWeekList.get(j) != null){
				weekMonthFlag=true;
				numOfWeeks = Integer.parseInt(numOfWeekList.get(j).toString());
			}
			if (numOfMonthsList.get(j) != null){
				weekMonthFlag=true;
				numOfMonths = Integer.parseInt(numOfMonthsList.get(j).toString());
			}
			if(weekMonthFlag){
				if (numOfMonths >0) reCalDisplacment+= (numOfMonths -1)*30;
				if (numOfWeeks > 0) reCalDisplacment+= (numOfWeeks -1)*7;
				reCalDisplacment += Integer.parseInt(numOfDays);  	
			}
		}
		//End here BUG#7469 fix
		String tempDisp = "";
		if ("0".equals(numOfDays)) {
			tempDisp = "0";
		} else if ("0".equals(displacement)) {
			tempDisp = "";
		} else {
			tempDisp = displacement;
		}
		if(displacement.equals("0")){
			indexArray.add(j);
		}
		String optVal ="";
		insertAftVisitPks.add(visitId);
		insertAftVisitNames.add(visitname);
		insertAfterId = visitId + "/" + tempDisp;
		insertAftVisitIds.add(insertAfterId);
		optVal = insertAfterId.substring(0, insertAfterId
				.indexOf("/"));
		
		//added for calculation interval interval
		if(Integer.parseInt(displacements.get(j).toString()) != 1){ //Ak:Fixed BUG#7684
			intervalList.add(displacements.get(j));
		}else if(Integer.parseInt(displacements.get(j).toString()) == 1 && Integer.parseInt(numOfDaysList.get(j).toString()) != 0 && weekMonthFlag ){ //Ak:Fixed BUG#8166
			intervalList.add(reCalDisplacment);
		} 
		else if(Integer.parseInt(displacements.get(j).toString()) == 1 && Integer.parseInt(numOfDaysList.get(j).toString()) != 0){ //Ak:Fixed BUG#7469
			intervalList.add(displacements.get(j));
		}
		else{
			intervalList.add(numOfDaysList.get(j));
		}

	}
	//Set the Dynamic array of interval unit for saved visit.
	ArrayList intervalUnitUpdatedList = new ArrayList();
	intervalUnitUpdatedList.add(LC.L_Days);
	intervalUnitUpdatedList.add(LC.L_Weeks);
	intervalUnitUpdatedList.add(LC.L_Months);
	
	if ((calledFrom.equals("P")) || (calledFrom.equals("L"))
			|| ((calledFrom.equals("S")))) {
		for (int i = 0; i < visitIds.size(); i++) {

			String insertAfterName = "";
			String insertAfterId = "";
			String isFakeVisit = "";
			String isRealFakeVisit = "";
			String visitId = null;
			int visitNumber = 0;
			HashMap visitMap = new HashMap();
			String vName = "";
			String description = "";
			boolean noIntervalVal = false;
			boolean deleteFlag = false;
			int months = 0, weeks = 0;
			String intervalInDays = "";
			Integer days = null;
			Integer insertAfter = 0;
			String monthStr = "";
			String weekStr = "";
			String dayStr = "";
			String intervalStr = "";
			int interval = 0;
			String intervalUnit = "";
			String durationBefore = "";
			String durationUnitBefore = "";
			String durationAfter = "";
			String durationUnitAfter = "";
			String displacement = "";
			String numOfDays = "";
			int hideFlag= 0;
			int offlineFlag = 0;
			vName = visitNames.get(i).toString();
			visitId = visitIds.get(i).toString();

			displacement = displacements.get(i).toString();
			if (numOfDaysList.get(i) != null)
				numOfDays = numOfDaysList.get(i).toString();
			//calculation interval in days, #7009
			if (intervalList.get(i) != null)
				
			protVisitB.setVisit_id(EJBUtil.stringToNum(visitId));
			protVisitB.getProtVisitDetails();
			visitSeq++;
			String noInterval = protVisitB.getIntervalFlag();
			int intervalNo= Integer.parseInt(noInterval);
			noIntervalVal = (noInterval.equals(String.valueOf(ntpd)) || noInterval.equals("1")) ? true : false;
			if(!noIntervalVal){
				Object[] params = {intervalList.get(i).toString()}; 
				intervalInDays = VelosResourceBundle.getLabelString("L_Day_Param",params);
			}
			else{
				intervalInDays = LC.L_No_Interval;
			}
      //Fixed #7454,BK,10/25/2011
			description = protVisitB.getDescription();
			description = (description == null) ? "" : (description);

			months = protVisitB.getMonths();
			weeks = protVisitB.getWeeks();
			days = protVisitB.getDays();
			visitNumber = protVisitB.getVisit_no();

			insertAfter = protVisitB.getInsertAfter();
			String noIntervalDesc="";
//Fixed Bug 7109
			if (insertAfter != null && insertAfter > 0) {
				if(intervalNo==dtp || intervalNo==0)
				{noInterval=String.valueOf(dtp);}
				String tempdisplacement = "";
				String tempnumOfDays = "";
				monthStr = "";
				weekStr = "";
				dayStr = "";
				intervalStr = new Integer(protVisitB
						.getInsertAfterInterval()).toString();
				intervalUnit = protVisitB.getInsertAfterIntervalUnit();
				int index1 = visitIds.indexOf(insertAfter);
				insertAfterName = (String) visitNames.get(index1);

				//Set the value
				tempdisplacement = displacements.get(index1).toString();

				if (numOfDaysList.get(index1) != null)

					tempnumOfDays = numOfDaysList.get(index1)
							.toString();
				
				//Set the insertAfterIds
				String tempDisp;
				if ("0".equals(tempnumOfDays)) {
					tempDisp = "0";
				} else if ("0".equals(tempdisplacement)) {
					tempDisp = "";
				} else {
					tempDisp = tempdisplacement;
				}

				insertAfterId = insertAfter + "/" + tempDisp;

			}else {
				
				if(intervalNo==1){
					//Yogendra : Bug# 9089 :02/April/2012
				
					if(calenderId.equalsIgnoreCase(protocolId) && (wrongVisitArray.contains(EJBUtil.stringToNum(visitId)) || wrongVisitNameArray.contains(vName)))
					{
					  noInterval=String.valueOf(dtp);
					  JSONArray visitData = (JSONArray)session.getAttribute("wrongVisitData");
					  visitData=visitData==null? new JSONArray():visitData;
						String tempdisplacement = "";
						String tempnumOfDays = "";
						monthStr = "";
						weekStr = "";
						dayStr = "";
					  for(int vd=0;vd<visitData.length();vd++){
						  JSONObject jsObjTemp1= visitData.getJSONObject(vd);
						String wrngVisitId=jsObjTemp1.getString("visitId");
						wrngVisitId.trim();
						String wrngVisitName=jsObjTemp1.getString("visitName");
						wrngVisitName.trim();
						if(wrngVisitId.equals(visitId) || wrngVisitName.equals(vName)){
							intervalStr = jsObjTemp1.getString("insertAfterInterval");
							intervalUnit = jsObjTemp1.getString("intervalUnit");
							insertAfterName=jsObjTemp1.getString("insertAfterVisit");;
							intervalInDays = LC.L_No_Interval;
							insertAfterId="-1/-1";
							String tempVisitId = "-1";
							int index = visitNames.indexOf(insertAfterName);
							if (index >= 0){
								tempVisitId = (visitIds.get(index)).toString();
							}
							isFakeVisit="true/"+insertAfterName+"/"+tempVisitId;
							isRealFakeVisit="true";
						}
					   }
					}else 
					{
					  noInterval=String.valueOf(ntpd);
					  String tempdisplacement = "";
					  String tempnumOfDays = "";
					  monthStr = "";
					  weekStr = "";
					  dayStr = "";
					  intervalStr = "";
					  intervalUnit = "";
					  insertAfterName="";
					  intervalInDays =LC.L_No_Interval;;
					  insertAfterId="";
	    				}
					
				}else{
					noInterval=String.valueOf(ftp);
					if (months > 0)
						monthStr = new Integer(months).toString();
	
					if (weeks > 0)
						weekStr = new Integer(weeks).toString();
					if (days != null)
						dayStr = days.toString();
					intervalStr = "";
					intervalUnit = "";
				}

			}
			
			if(noInterval.equals(String.valueOf(ntpd))){
				noIntervalDesc=noTimePointdefined;
			}else if(noInterval.equals(String.valueOf(ftp))){
			 	noIntervalDesc =fixedPointdefined;
			}else if(noInterval.equals(String.valueOf(dtp))){
				noIntervalDesc =depnPointdefined;
			}
			durationBefore = protVisitB.getDurationBefore();
			durationBefore = (durationBefore == null) ? "0"
					: (durationBefore);
			durationUnitBefore = protVisitB.getDurationUnitBefore();
			durationUnitBefore = (durationUnitBefore == null) ? ""
					: (durationUnitBefore);
			durationAfter = protVisitB.getDurationAfter();
			durationAfter = (durationAfter == null) ? "0"
					: (durationAfter);
			durationUnitAfter = protVisitB.getDurationUnitAfter();
			durationUnitAfter = (durationUnitAfter == null) ? ""
					: (durationUnitAfter);

			if (durationUnitBefore.equals("D"))
				durationUnitBefore = LC.L_Days/*Days*****/;
			if (durationUnitBefore.equals("W"))
				durationUnitBefore = LC.L_Weeks/*Weeks*****/;
			if (durationUnitBefore.equals("M"))
				durationUnitBefore = LC.L_Months/*Months*****/; //Ak:Fixed BUG#7438
			if (durationUnitBefore.equals("Y"))
				durationUnitBefore = LC.L_Years/*Years*****/;

			if (durationUnitAfter.equals("D"))
				durationUnitAfter = LC.L_Days/*Days*****/;
			if (durationUnitAfter.equals("W"))
				durationUnitAfter = LC.L_Weeks/*Weeks*****/;
			if (durationUnitAfter.equals("M"))
				durationUnitAfter = LC.L_Months/*Months*****/;
			if (durationUnitAfter.equals("Y"))
				durationUnitAfter = LC.L_Years/*Years*****/;

			if (intervalUnit == null) { intervalUnit = ""; }
			if (intervalUnit.equals("D"))
				intervalUnit = LC.L_Days/*Days*****/;
			if (intervalUnit.equals("W"))
				intervalUnit = LC.L_Weeks/*Weeks*****/;
			if (intervalUnit.equals("M"))
				intervalUnit = LC.L_Months/*Months*****/;
			if (intervalUnit.equals("Y"))
				intervalUnit = LC.L_Years/*Years*****/;

			if(calledFrom.equals("S")){ //AK:Added for Enhancement:20353,20354
				hideFlag=protVisitB.getHideFlag()==null?0:protVisitB.getHideFlag();
				offlineFlag=protVisitB.getOfflineFlag()==null?0:protVisitB.getOfflineFlag();	
			}
			
			//Set the InsertAfter fields values and Ids

			JSONArray jsVisits = new JSONArray();
			if(insertAfterName.equals("")){
				JSONObject jsObjvisit = new JSONObject();
				jsObjvisit.put("0", LC.L_Select_AnOption/*Select an Option*****/);
				jsVisits.put(jsObjvisit);
			}

			for (int iVisit = 0; iVisit < visitIds.size(); iVisit++) {
				String tempVisitId = visitIds.get(iVisit).toString();
				String tempDisplacement = displacements.get(iVisit).toString();
				if (!tempVisitId.equals(visitId)) {
					JSONObject jsObjvisit = new JSONObject();
					if(!tempDisplacement.equals("0")){
					jsObjvisit.put(insertAftVisitIds.get(iVisit)
							.toString(), (String) insertAftVisitNames
							.get(iVisit));
					jsVisits.put(jsObjvisit);
					}
				}
			}
			JSONArray fkvisitData = (JSONArray)session.getAttribute("wrongVisitData");
			fkvisitData=fkvisitData==null? new JSONArray():fkvisitData;
			if(calenderId.equalsIgnoreCase(protocolId)){
			  for(int vd=0;vd<fkvisitData.length();vd++){
				JSONObject jsObjTemp1= fkvisitData.getJSONObject(vd);
				String wrngVisitName=jsObjTemp1.getString("visitName");
				wrngVisitName.trim();
				if(!wrngVisitName.equals(vName)){
					JSONObject jsObjNovisit = new JSONObject();
					jsObjNovisit.put("-1/-1",wrngVisitName);
					jsVisits.put(jsObjNovisit);
				}
				
			   }
			  }
			JSONObject jsObjNovisit = new JSONObject();
			jsObjNovisit.put("-1/-1", LC.L_VisitNot_Listed/*Visit Not Listed*****/);
			jsVisits.put(jsObjNovisit);
			JSONObject jsFirstVisit = new JSONObject();
			jsFirstVisit.put("-7/-7",LC.L_First_Visit);
			jsVisits.put(jsFirstVisit);
			JSONObject jsPreviousVisit = new JSONObject();
			jsPreviousVisit.put("-9/-9",LC.L_Previous_Visit);
			jsVisits.put(jsPreviousVisit);
			jsObj.put("v" + visitId, jsVisits);
			//Set the value of Interval unit
			if(!intervalUnit.equals("")){
				jsObj.put("i"+ visitId,intervalUnitUpdatedList);

			}
			else{	
				jsObj.put("i"+ visitId,intervalUnitList);
			}
			
			visitMap.put("visitName", vName);
			visitMap.put("description", description);
			visitMap.put("noInterval", noIntervalDesc);
			visitMap.put("noIntervalId", noInterval);
			visitMap.put("noTimePointId", ntpd);
			visitMap.put("months", monthStr);
			visitMap.put("weeks", weekStr);
			visitMap.put("days", dayStr);
			visitMap.put("beforenum", durationBefore);
			visitMap.put("durUnitA", durationUnitBefore);
			visitMap.put("afternum", durationAfter);
			visitMap.put("durUnitB", durationUnitAfter);
			visitMap.put("afterText", LC.L_After);
			visitMap.put("insertAfter", insertAfterName);
			visitMap.put("insertAfterId", insertAfterId);
			visitMap.put("isFakeVisit", isFakeVisit);
			visitMap.put("isRealFakeVisit", isRealFakeVisit);
			visitMap.put("insertAfterInterval", intervalStr);
			visitMap.put("intervalUnit", intervalUnit);
			visitMap.put("visitId", visitId);
			visitMap.put("visitSeq", visitSeq);

			visitMap.put("visitNumber", visitNumber);
			visitMap.put("offlineFlag", offlineFlag);//Ak:Added for Enhancement:20353,20354
			visitMap.put("hideFlag", hideFlag);
			visitMap.put("delete",deleteFlag);
			visitMap.put("interval",intervalInDays);

			dataList.add(visitMap);

		}
	}
	//Storing visit sequence
	jsObj.put("visitSeq", visitSeq);
	Collections.sort(indexArray);
	JSONArray visitData = (JSONArray)session.getAttribute("wrongVisitData");
	visitData=visitData==null? new JSONArray():visitData;
	if(calenderId.equalsIgnoreCase(protocolId)){
	  for(int vd=0;vd<visitData.length();vd++){
		JSONObject jsObjTemp1= visitData.getJSONObject(vd);
		String wrngVisitPk = jsObjTemp1.getString("visitId");
		insertAftVisitPks.add(wrngVisitPk);
		String wrngVisitName=jsObjTemp1.getString("visitName");
		wrngVisitName.trim();
		insertAftVisitIds.add("-1/-1");
		insertAftVisitNames.add(wrngVisitName);
		
	   }
	}
	//Fixing PKs of all the newly added visits
	for(int iL = 0; iL < insertAftVisitPks.size(); iL++){
		String iLVisitPK = (String)insertAftVisitPks.get(iL);
		if (!"0".equals(iLVisitPK)){
			String iLVisitName = (String)insertAftVisitNames.get(iL);

			for(int iM = 0; iM < insertAftVisitPks.size(); iM++){
				if (iL != iM){ 
					String iMVisitPK = (String)insertAftVisitPks.get(iM);
					if ("0".equals(iMVisitPK)){
						String iMVisitName = (String)insertAftVisitNames.get(iM);
						if (iLVisitName.equals(iMVisitName)){
							insertAftVisitPks.set(iM,iLVisitPK);
						}
					}
				}
			}
		}
	}
	//List of the existing Visit Ids and Names
	for(int iN =(indexArray.size()-1) ;iN>=0;iN--){
		int inx = Integer.parseInt(indexArray.get(iN).toString());
		insertAftVisitPks.remove(inx);
		insertAftVisitIds.remove(inx);
		insertAftVisitNames.remove(inx);
	}
	insertAftVisitPks.add(0, "0");
	insertAftVisitIds.add(0, "0");
	insertAftVisitNames.add(0, LC.L_Select_AnOption/*Select an Option*****/);
	insertAftVisitPks.add("-1");
	insertAftVisitIds.add("-1/-1");
	insertAftVisitNames.add(LC.L_VisitNot_Listed/*Visit Not Listed*****/);
	insertAftVisitPks.add("-7");
	insertAftVisitIds.add("-7/-7");
	insertAftVisitNames.add(LC.L_First_Visit/*First Visit*****/);
	insertAftVisitPks.add("-9");
	insertAftVisitIds.add("-9/-9");
	insertAftVisitNames.add(LC.L_Previous_Visit/*Previous Visit*****/);
	jsObj.put("insertAftVisitPks", insertAftVisitPks);
	jsObj.put("insertAftVisitIds", insertAftVisitIds);
	jsObj.put("insertAftVisitNames", insertAftVisitNames);

	//End visiting Data here
	JSONArray jsVisitsData = new JSONArray();
	for (int iX = 0; iX < dataList.size(); iX++) {
		jsVisitsData.put(EJBUtil.hashMapToJSON((HashMap) dataList
				.get(iX)));
	}
	
	jsObj.put("dataArray", jsVisitsData);
	jsObj.put("colArray", jsColArray);

	out.println(jsObj.toString());
	
%>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>

