<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<!-- YK 05Aug11: Removed for Bug #6728 already using Whichcss_skin.js in panel.jsp -->
<SCRIPT>

function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	<%-- if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   } --%>
}
</SCRIPT>

 

</HEAD>

<% String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<BODY>
<DIV class="formDefault" id="div1">

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="addressB" scope="page" class="com.velos.eres.web.address.AddressJB" />
<jsp:useBean id ="auditRowEresJB" scope="session" class="com.velos.eres.audit.web.AuditRowEresJB"/>
<jsp:useBean id ="audittrails" scope="session" class="com.aithent.audittrail.reports.AuditUtils"/>
 
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*"%>
<%
 HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))
   {   
    boolean bReturn = false;
    String userId=request.getParameter("userId");
    String userType = request.getParameter("userType");
   
    int user = EJBUtil.stringToNum(userId);
   
    userB.setUserId(user);
    userB.getUserDetails();
    
    String acc = (String) tSession.getValue("accountId");
    String uStat = userB.getUserStatus();
    if (uStat==null) uStat = "-";
  
    int rid = 0; //Added By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.
 
 // if the user tries to reactivate a deactivated user, then check the # of active and blocked user
 // should be less than the max users for an account
 
 //Added by Ganapathy on 02/08/05 for finding out total number of active user
 
 
	
	String viewList="AA";
	
	   
	   
	  String accId = (String) tSession.getValue("accountId");
	   	
	
		 UserDao userDao= new UserDao();

		if(viewList.equals("AA"))
		 {
			userDao = userB.getAccountUsers(Integer.parseInt(accId),"A","","");//KM-#U10
		 }
		 

 	 			
    int counter = 0;
    
	ArrayList uIds = userDao.getUsrIds();
		
	int lenUsers=uIds.size();

// Added by gopu to fix the bugzilla issues #1873 on 14th March 2006

	boolean cntExceeded = false;
	UserDao userDao1 = new UserDao();
	userDao1 = userB.getUsersSelectively(Integer.parseInt(accId),"'D','B'","","");//KM-#U10
	//userDao1 = userB.getUsersSelectively(Integer.parseInt(accId),"'B'");

	ArrayList stats = userDao1.getUsrStats();
	int countBD= stats.size();

	int counterB=0;
	int counterD=0;

	String stat = null ;

	for (int j=0 ; j<countBD ; j++){
	
		stat = stats.get(j).toString();	 
		if(stat.equals("B")){
			counterB= counterB +1 ;							
		} else
		{
			counterD= counterD +1;
			
		}

	}
//////////////


	
	int countUsers = 0;
	int outLoop=0;
	int inLoop=0;
	
	if(viewList.equals("AA")){
		for(outLoop=0;outLoop<lenUsers;outLoop++)
			 {
				int count=0;
				
				for(inLoop=0;inLoop<outLoop;inLoop++)
				 {
				   if(uIds.get(inLoop).equals(uIds.get(outLoop)))
					 {
						count ++;				 
					 }				 
				 }			 
				 if(count==0)
				 {	
					countUsers ++;
				 }
			 }
	
			 	
 
 }
 //
if (uStat.equals("D")) {
	 int totalUsersAllowed = EJBUtil.stringToNum((String) tSession.getValue("totalUsersAllowed"));
	 int existingUsers = 0;
	 existingUsers = userB.getUsersInAccount(EJBUtil.stringToNum(acc));
	
	
	// if(totalUsersAllowed > 0 && totalUsersAllowed <= existingUsers){
	
	 if(totalUsersAllowed > 0 && totalUsersAllowed <=countUsers){
	 
%>	 
       <br><br><br><br>
       <table width=100%>
       <tr>
       <td align=center>
       
       <p class = "sectionHeadings">    
       <%=MC.M_NumUsr_VelosAdmin%><%--The number of users you can activate in this account has exhausted. Please contact the Velos Administrator.*****--%>     
       </p>
       </td>
       </tr>
       
       <tr height=20></tr>
       <tr>
       <td align=center>    
      		<button onclick="window.history.back();"><%=LC.L_Back%></button>
       </td>		
       </tr>		
       </table>			 
<%
  	   bReturn = true;	 
	}
 } 
 
if (bReturn == false) {  

 String supportEmail = Configuration.SUPPORTEMAIL;
 String ipAdd = (String) tSession.getValue("ipAdd");
 // Added By Amarnadh for issue #3232
 String usr1 = null;
 usr1 = (String) tSession.getValue("userId");
		 
 String userName = (String) tSession.getAttribute("userName"); //Added By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.

 String userPass="";
 String usereSign="";
 String messageSubtext="";
 String addressId="";
 
 String siteUrl = ""; 

 String messageFrom = "";
 String messageText = "";
 String messageSubject = "";
 String messageHeader = "";
 String messageFooter = "";		
 String completeMessage = "";
 String smtpHost = "";
 String domain = "";
 String messageTo = "";
 int rows = 0;
 String	usrEmail="";
 String userLoginName ="";

 int ret =0;
 double randomNumber=Math.random();
 String randomNum=String.valueOf(randomNumber);


 String delMode=request.getParameter("delMode");
 String userMailStatus = "";
 int metaTime = 1;

// For the bugzilla issues #2093 on Feb 2005 by gopu
	rows = 0;
	CtrlDao urlCtrl = new CtrlDao();	
	urlCtrl.getControlValues("site_url");
	rows = urlCtrl.getCRows();
   	if (rows > 0)
	   {
	   	siteUrl = (String) urlCtrl.getCValue().get(0);
	   }
// End





if (delMode.equals("null")) 
	{
	delMode="final";

%>	

<FORM name="activateuser" id="actuserfrm" method="post" action="activateuser.jsp?userType=<%=userType%>" onSubmit="if (validate(document.activateuser)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
  	 <input type="hidden" name="userId" value="<%=userId%>">

	<BR><BR><BR>
	<!-- Added by gopu to fix the bugzilla issues #1873 on 14th March 2006 -->
	<%
	

	int totalUsersAllowed = EJBUtil.stringToNum((String) tSession.getValue("totalUsersAllowed"));
	int existingUsers = 0;

	//existingUsers = userB.getActiveUsersInAccount(EJBUtil.stringToNum(acc));
	//Added by gopu to fix the bugzilla issues #1873 on 25th April 2006
	existingUsers = userB.getActiveUsersInAccount(EJBUtil.stringToNum(acc));
	//existingUsers = ( lenUsers + counterB );
	
	if (uStat.equals("D")){
	if (totalUsersAllowed > 0 && totalUsersAllowed <= existingUsers) {
	  	 cntExceeded = true;

	}
	}
	else if (uStat.equals("B")){

	existingUsers = existingUsers -counterB;

	if (totalUsersAllowed > 0 && totalUsersAllowed <= existingUsers) {
		cntExceeded = true;
	
	} 
	}
	
	if (cntExceeded == true ){%>
	<P class="defComments"> <%=MC.M_NumUsr_CnctVelos%><%--The number of users you can activate in this account has exhausted. You cannot activate user. Please contact the Velos Administrator*****--%> 
	</P>	
   
   	  <tr height=20></tr>
      <tr>
      <td align=center width=50%>   
     		<button onclick="window.history.back();"><%=LC.L_Back%></button>
      </td>		
      </tr>	
       	
       	
	
	<%} else{
	

	%>

	<P class="defComments"><%=MC.M_Etr_Esign_ToProc%><%--Please enter e-Signature to proceed*****--%></P>
		
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="actuserfrm"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>

<%}%>
	</FORM>
<%
	} else {
		String eSign = request.getParameter("eSign");	
		String oldESign = (String) tSession.getValue("eSign"); 
		if(!oldESign.equals(eSign)) 
		{
%>
 		 <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
		 } else {
		
      addressId=userB.getUserPerAddressId();
      addressB.setAddId(EJBUtil.stringToNum(addressId));
      addressB.getAddressDetails();
      usrEmail=addressB.getAddEmail();
      userLoginName =userB.getUserLoginName();
     
      userPass=randomNum.substring(2,10);
      usereSign=randomNum.substring(11,15);    
      userB.setUserPwd(Security.encryptSHA(userPass));
      userB.setUserESign(usereSign);
      userB.setUserPwdExpiryDate(DateUtil.dateToString(DateUtil.addDays((new Date()),-1)));
  	  userB.setUserESignExpiryDate(DateUtil.dateToString(DateUtil.addDays((new Date()),-1)));
      userB.setIpAdd(ipAdd);
	  userB.setModifiedBy(usr1); // Amarnadh
      userB.setUserStatus("A");
      userB.setUserAttemptCount("0");
      
      //Modified By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.
      rid = auditRowEresJB.getRidForDeleteOperation(user, EJBUtil.stringToNum(usr1), "ER_USER", "PK_USER");
      ret = userB.updateUser();	
      
      if(ret>=0){
    	  if(rid > 0){
    			audittrails.updateAuditROw("eres", EJBUtil.stringToNum(usr1)+", "+userName.substring(userName.indexOf(" ")+1,userName.length())+", "+userName.substring(0,userName.indexOf(" ")), StringUtil.integerToString(rid), "U");
    		}
      }
     

	
	messageTo = usrEmail;

	CtrlDao userCtrl = new CtrlDao();	
	userCtrl.getControlValues("eresuser");
	//APR-04-2011,MSG-TEMPLAGE FOR USER INFO V-CTMS-1
	int output = userB.notifyUserResetInfo(userPass,siteUrl,usereSign,userId,userLoginName,"R",usr1);  //Modified By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.
	/*rows = userCtrl.getCRows();
	if (rows > 0)
	   {
	   	messageFrom = (String) userCtrl.getCDesc().get(0);
	   }
	
	messageSubject = "Request for change of password/esign";	
	messageHeader ="\nDear Velos eResearch Member," ; 
	messageSubtext=" \nNew Password :   " + userPass +  "\nNew e-Signature :   " + usereSign;
    messageText = "\n\nAs per your request your Password/e-Signature has been reset. Your new Password/e-Signature is:\n\nLogin ID           :    " + userLoginName + messageSubtext + "\nApplication URL : " + siteUrl + "\n\nYou may of course change your Password/e-Signature at any time should you have any concerns regarding the privacy of your information. To change your Password/e-Signature, you will need to login with the above ID , go to the Personalize Account section and then change your Password/e-Signature.\n\nTo begin using the Velos eResearch system, go to "+ siteUrl +", enter your login ID and password and click on the Login button.\n **** Important ****\nPlease remember that the above information needs to be kept in a secure place. Any one with access to the above information can access your account information.\n\nIf you have any questions or feedback, please feel free to contact us at " + supportEmail + ".\n\n Velos eResearch Customer Support.\n\nVelos, Inc.\n2201 Walnut Avenue, Suite 208\nFremont, CA. 94538" ;
	messageFooter = "\n\nThis email transmission and any documents, files or previous email messages attached to it may contain information that is confidential or legally privileged. If you are not the intended recipient or a person responsible for delivering this transmission to the intended recipient you are hereby notified that you must not read this transmission and that any disclosure, copying, printing, distribution or use of this transmission is strictly prohibited. If you have received this transmission in error, please immediately notify the sender by telephone or return email and delete the original transmission and its attachments without reading or saving in any manner." ;

	completeMessage = messageHeader  + messageText + messageFooter ;

	try{
		
   	   	VMailer vm = new VMailer();
    	VMailMessage msgObj = new VMailMessage();
    				
		msgObj.setMessageFrom(messageFrom);
		msgObj.setMessageFromDescription("Velos eResearch Customer Services");
		msgObj.setMessageTo(messageTo);
		msgObj.setMessageSubject(messageSubject);
		msgObj.setMessageSentDate(new Date());
		msgObj.setMessageText(completeMessage);

		vm.setVMailMessage(msgObj);
		userMailStatus = vm.sendMail(); 							
	  }
	  catch(Exception ex)
      {
     		  userMailStatus = ex.toString();
       }*/

%>
<br><br><br><br><br>
 <%
  if ( output >= 0 )
  {
  	metaTime = 1;
	
  %>
	<p class = "sectionHeadings" align = center> <%=MC.M_DataSvdSucc_NotficSent%><%--Data was saved successfully and Notification sent.*****--%> </p>

 <%
	 } else
	{
	 	metaTime = 10;
     %>
		<p class = "redMessage" align = center> <%=MC.M_DataSvdSucc_CustSuppMsg%><%--Data was saved successfully but there was an error in sending notification to the user.<br>Please contact Customer Support with the following message*****--%>:<BR><%=userMailStatus%></p>

	 <%
  		}
	%>	 	
		<META HTTP-EQUIV=Refresh CONTENT="<%=metaTime%>; URL=accountbrowser.jsp?srcmenu=<%=src%>&cmbViewList=<%=userType%>" >
  	<%
 	
	 } //end esign
	} //end of delMode	

	} //end of usr_stat D
	
	}//end of if body for session
	else
	{
	%>
	<jsp:include page="timeout.html" flush="true"/>
	<%
	}
%>

 <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</BODY>

</HTML>
