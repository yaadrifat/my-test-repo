<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Alerts_Notifications%><%--Alerts and Notifications*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT language="JavaScript1.1">
function  validate(formobj){    
    if (!(validate_col('e-Signature',formobj.eSign))) return false
<%-- 	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
     } --%>
}
function openwin1(formobj,counter) {
	  var names = formobj.enteredByName[counter].value;
	  var ids = formobj.enteredByIds[counter].value;
	  var windowToOpen = "multipleusersearchdetails.jsp?fname=&lname=&from=alertnotify&mode=initial&ids=" + ids + "&names=" + names + "&rownum=" + counter;
      window.open(windowToOpen,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400")
;}
</SCRIPT>
</head>
<% String src="";
src= request.getParameter("srcmenu");
%>	
<jsp:include page="panel.jsp" flush="true"> 
	<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="crfNotifyB" scope="request" class="com.velos.esch.web.crfNotify.CrfNotifyJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,com.velos.eres.service.util.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.DateUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*"%>

<body>
<br>
<DIV class="browserDefault" id="div1"> 
  <%
   HttpSession tSession = request.getSession(true); 
   if (sessionmaint.isValidSession(tSession))
   {
	String mode="N";//request.getParameter("mode");
	String patientId = "";
	String globalFlag = "G";
	String protocolId="";
	String selectedTab ;
	String from;
	   
	Calendar cal1 = new GregorianCalendar();
	
	int pageRight = 0; 
	protocolId=request.getParameter("protocolId");
	String studyId = request.getParameter("studyId");
	from = request.getParameter("from");
    selectedTab = request.getParameter("selectedTab");
%>
	<jsp:include page="studytabs.jsp" flush="true"> 
		<jsp:param name="from" value="<%=from%>"/> 
		<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
		<jsp:param name="studyId" value="<%=studyId%>"/>
	</jsp:include>
	
<%	stdRights =(StudyRightsJB) tSession.getValue("studyRights");	 
	if ((stdRights.getFtrRights().size()) == 0){
 		pageRight= 0;
		}else{
		pageRight = 	Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
		}
	if (pageRight >= 4 )
	  {
	%>  	  
<br>
<table class=tableDefault  width="100%" border=0>
<tr>
	<td width= 40%>
		<p class = "sectionHeadings" ><%=LC.L_Crf_Notifications%><%--CRF Notifications*****--%> </p>
	</td>
<td width=60% align=right>
	 <a href="studycrfnot.jsp?srcmenu=tdMenuBarItem5&mode=N&studyId=<%=studyId%>&globalFlag=<%=globalFlag%>&protocolId=<%=protocolId%>&selectedTab=<%=selectedTab%>&from=<%=from%>" onClick="return f_check_perm(<%=pageRight%>,'N');"> <%=MC.M_Add_AnotherCrfNotfic%><%--Add Another CRF Notification*****--%></a>
</td>
</tr>
</table>
<TABLE  width="95%">
  <TR>
  	<th width=25% ><%=LC.L_For_CrfStatus%><%--For CRF Status*****--%> </th>
	<th width=40% ><%=LC.L_Notify_Users%><%--Notify Users*****--%> </th>
	<th width=30% ><%=LC.L_Crf%><%--CRF*****--%></th>
  </TR>
<%  
  CrfNotifyDao crfNotifyDao = new CrfNotifyDao();
  crfNotifyDao = crfNotifyB.getCrfNotifyValues(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(protocolId));
  
  ArrayList crfNotIds = null;
  ArrayList crfNotStats = null;
  ArrayList crfNotUsers = null; 
  ArrayList crfNumbers = null;
  ArrayList crfIds = null;
  
  crfNotIds = crfNotifyDao.getCrfNotIds();
  crfNotStats = crfNotifyDao.getCrfNotStats();
  crfNotUsers = crfNotifyDao.getCrfNotUsers();
  crfNumbers = crfNotifyDao.getCrfNumbers();
  crfIds = crfNotifyDao.getCrfNotCrfIds();
  int lenCrf = crfNotifyDao.getCRows();
	for(int i=0; i<lenCrf ; i++) 
	{
		String crfNotId = ((Integer) crfNotIds.get(i)).toString();
		String crfNotStat = (String)crfNotStats.get(i); 
		String crfNotUser = (String) crfNotUsers.get(i);
		String crfNumber = (String) crfNumbers.get(i);
		String crfId = (String) crfIds.get(i);
	
		crfNotId = (crfNotId == null)?"-":crfNotId;
		crfNotStat = (crfNotStat == null)?"-":crfNotStat;
		crfNotUser = (crfNotUser == null)?"-":crfNotUser;
		crfNumber = (crfNumber == null)?"All":crfNumber;
		crfId = (crfId == null)?"":crfId;
		
	
	if(i%2==0){ 
%>	
	<TR class="browserEvenRow">	
<%
	}else{
%>
	<TR class="browserOddRow">
<% 
	} 
%>

	<td class=tdDefault align="center"><A HREF="studycrfnot.jsp?srcmenu=<%=src%>&crfNotifyId=<%=crfNotId%>&globalFlag=<%=globalFlag%>&mode=M&studyId=<%=studyId%>&protocolId=<%=protocolId%>&selectedTab=<%=selectedTab%>&from=<%=from%>" onClick="return f_check_perm(<%=pageRight%>,'E');"> <%=crfNotStat%></A></td> 	
	<td class=tdDefault align="center"><%=crfNotUser%></td>
	<td class=tdDefault align="center"><%=crfNumber%></td>
</tr>
		
<%	
	}
%>
  
</TABLE>

<table width=100%>
	<tr>
      <td align=center>
		<A href="studynotification.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>" type="submit"><%=LC.L_Back%></A>	
      </td>
	 </tr>
</table>


<%
	}//end of page right
} //end of if session times out
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

} //end of else body for page right

%>
   <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>   
</div>
</body>
</html>
