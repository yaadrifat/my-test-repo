<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@page import="java.util.ArrayList,java.util.HashMap,org.json.*"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%> 
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
HttpSession tSession = request.getSession(false); 
if (!sessionmaint.isValidSession(tSession)) {
	return;
}
%>
<div>
    <table style="width:100%">
        <tr>
            <td colspan="2">
            <table style="width:100%">
                <tr>
                    <td>
                    <form name="gadgetSample2_formStudy" id="gadgetSample2_formStudy" 
                            onsubmit="gadgetSample2.beforeSubmitStudy(this);"
                            method="POST" action="studybrowserpg.jsp">
                        <input type="text" class="gdt-input-text" size="40" 
                                name="gadgetSample2_study" id="gadgetSample2_study" placeholder="<%=MC.M_StdTitle_Kword%>"/>
                        <input type="hidden" name="searchCriteria" id="searchCriteria" />
                        <button type="button" name="gadgetSample2_studySearchButton" id="gadgetSample2_studySearchButton" 
                                class="gadgetSample2_studySearchButton">Search</button>
                        <button type="button" name="gadgetSample2_studyAdvancedSearchButton" id="gadgetSample2_studyAdvancedSearchButton" 
                                class="gadgetSample2_studyAdvancedSearchButton">Advanced Search</button>
                    </form>
                    </td>
                </tr>
                <tr>
                    <td>
                    <form name="gadgetSample2_formPatient" id="gadgetSample2_formPatient" 
                            onsubmit="return gadgetSample2.beforeSubmitPatient(this);"
                            method="POST" action="allPatient.jsp?srcmenu=tdmenubaritem5&selectedTab=1&searchFrom=initial">
                        <input type="text" class="gdt-input-text" size="20" maxlength="100" 
                                name="gadgetSample2_patient" id="gadgetSample2_patient" placeholder="Patient ID"/>
                        <input type="hidden" name="studyId" id="studyId" />
                        <input type="hidden" name="patientid" id="patientid" />
                        <input type="hidden" name="searchPatient" id="searchPatient" />
                        on&nbsp;study
                        <s:checkbox id="gadgetSample2_patientOnStudyTurnedOn" name="gadgetSample2_patientOnStudyTurnedOn" />
                        <s:property escape="false" value="gadgetSample2JB.studyMenuForPatSearch" />
                        <button type="button" name="gadgetSample2_patientSearchButton" id="gadgetSample2_patientSearchButton" 
                		        class="gadgetSample2_patientSearchButton">Search</button>
                    </form>
                    </td>
                </tr>
                <tr>
                    <td>
                    <form name="gadgetSample2_formPatientStudy" id="gadgetSample2_formPatientStudy" 
                            onsubmit="return gadgetSample2.beforeSubmitPatientStudy(this);"
                            method="POST" action="studypatients.jsp?srcmenu=tdmenubaritem5&selectedTab=2&openMode=F">
                        <input type="text" class="gdt-input-text" size="20" maxlength="100" 
                                name="gadgetSample2_patientStudy" id="gadgetSample2_patientStudy" placeholder="Patient Study ID"/>
                        <input type="hidden" name="studyId" id="studyId" />
                        <input type="hidden" name="pstudyid" id="pstudyid" />
                        on&nbsp;study
                        <input type="checkbox" style="visibility:hidden" />
                        <s:property escape="false" value="gadgetSample2JB.studyMenuForPatStudySearch" />
                        <button type="button" name="gadgetSample2_patientStudySearchButton" id="gadgetSample2_patientStudySearchButton" 
                        		class="gadgetSample2_patientStudySearchButton">Search</button>
                    </form>
                    </td>
                </tr>
            </table>
            </td>
        </tr>
        <tr valign="top">
            <td width="50%">
                <table class="gdt-table" style="width:100%;">
                    <tr>
                        <td>
                        <table class="gdt-header-row" style="width:100%;">
                            <tr>
                            <td>My Links</td>
                            <td align="right" valign="middle">
                                <a href="ulinkBrowser.jsp?srcmenu=tdMenuBarItem13&selectedTab=2" >All<!-- <img 
                                    id="editMyLinksImg" title="Jump to Edit Links" align="middle" align="middle"
                                    src="./images/edit.gif" style="height:18px; width:18px; border:none; padding:1px"
                                /> --></a>
                                &nbsp;
                                <a href="javascript:gadgetSample2.bothLinks.addNewLink('myLinks');" >
                                    <img id="addMyLinksImg" title="Add New Link" align="middle" align="middle"
                                            src="./images/Add.gif" style="height:18px; width:18px; border:none; padding:1px"/>
                                </a>
                            </td>
                        </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <div id="gadgetSample2_myLinksNewLink" style="display:none">
                            <div id="gadgetSample2_myLinksFormNewLinkDiv">
                            <s:form name="gadgetSample2_formNewLink" id="gadgetSample2_formNewLink" 
                                    onsubmit="return false;">
                            <table id="gadgetSample2_myLinksFormNewLinkTable" class="gdt-table" style="width:100%;">
                            <tr>
                                <td>
                                    <table id="gadgetSample2_myLinksFormNewLinkHeader" style="width:100%;">
                                    <tr>
                                        <td><b>Add New Link</b></td>
                                        <td align="right"><a href="linklist.jsp?mode=N&srcmenu=tdMenuBarItem13&selectedTab=2&lnkType=user">Add Multiple</a></td>
                                    </tr>
                                    </table>
                                    <table id="gadgetSample2_myLinksFormEditLinkHeader" style="width:100%; display:none">
                                    <tr>
                                        <td><div id='gadgetSample2_myLinksFormEditLinkHeadderLabel'></div></td>
                                        <td align="right"></td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div id="gadgetSample2JB.myLinksLinkDisplay_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <label for="gadgetSample2JB.myLinksLinkDisplay">Display Text</label><FONT class="Mandatory"> *</FONT><br/>
                                
                                <s:textfield id="gadgetSample2JB.myLinksLinkDisplay" cssClass="gdt-input-text" 
                                        name="gadgetSample2JB.myLinksLinkDisplay" type="text" size="30" maxlength="100" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div id="gadgetSample2JB.myLinksLinkUrl_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <label for="gadgetSample2JB.myLinksLinkUrl">URL</label><FONT class="Mandatory"> *</FONT><br/>
                                <s:textfield id="gadgetSample2JB.myLinksLinkUrl" cssClass="gdt-input-text" 
                                        name="gadgetSample2JB.myLinksLinkUrl" type="text" size="30" maxlength="100" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div id="gadgetSample2JB.myLinksLinkSection_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <label for="gadgetSample2JB.myLinksLinkSection">Section</label><br/>
                                <s:textfield id="gadgetSample2JB.myLinksLinkSection" cssClass="gdt-input-text" 
                                        name="gadgetSample2JB.myLinksLinkSection" type="text" size="30" maxlength="100" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div id="gadgetSample2JB.myLinksESign_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <label for="gadgetSample2JB.myLinksESign">e-Signature</label><FONT class="Mandatory"> *</FONT><br/>
                                <s:password id="gadgetSample2JB.myLinksESign" cssClass="gdt-input-password" 
                                        name="gadgetSample2JB.myLinksESign" size="30" maxlength="8" 
                                        onkeyup="replicateESign(this.id);" />
                                <s:hidden id="gadgetSample2JB.myLinksLinkId" 
                                        name="gadgetSample2JB.myLinksLinkId" />
                                </td>
                            </tr>
                            <tr>
                                <td><div id="gadgetSample2_formNewLink_resultMsg"></div></td>
                            </tr>
                            <tr>
                                <td>
                                    <span id="gadgetSample2JB.myLinksESign_eSign"></span>
                                </td>
                            </tr>
                            <tr id="gadgetSample2_newMyLinkButtonRow" align="right">
                                <td>
                                
                                <button type="submit" id="gadgetSample2_saveNewMyLinkButton" 
             	                        class="gadgetSample2_saveNewMyLinkButton">Save</button>&nbsp;
                                <button type="button" id="gadgetSample2_cancelNewMyLinkButton" 
             	                        class="gadgetSample2_cancelNewMyLinkButton">Cancel</button>
                                </td>
                            </tr>
                            </table>
                            </s:form>
                            </div>
                            <div style="height:5px;"></div>
                            <div id="gadgetSample2_myLinksFormDeleteLinkDiv">
                            <s:form name="gadgetSample2_formDeleteLink" id="gadgetSample2_formDeleteLink" 
                                    onsubmit="gadgetSample2.myLinks.deleteLink(); return false;">
                            <table id="gadgetSample2_myLinksFormDeleteLinkTable" class="gdt-table" style="width:100%;">
                            <tr>
                                <td>
                                    <table style="width:100%; ">
                                    <tr>
                                        <td><div id='gadgetSample2_myLinksFormDeleteLinkHeadderLabel'></div></td>
                                        <td align="right"></td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div id="gadgetSample2JB.myLinksDeleteESign_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td><div id="gadgetSample2_formDeleteLink_resultMsg"></div></td>
                            </tr>
                            <tr>
                                <td>
                                <label for="gadgetSample2JB.myLinksDeleteLinkId">e-Signature</label><FONT class="Mandatory"> *</FONT><br/>
                                <s:hidden id="gadgetSample2JB.myLinksDeleteLinkId" 
                                        name="gadgetSample2JB.myLinksDeleteLinkId" />
                                <s:password id="gadgetSample2JB.myLinksDeleteESign" cssClass="gdt-input-password" 
                                        name="gadgetSample2JB.myLinksDeleteESign" size="30" maxlength="8" 
                                        onkeyup="replicateESign(this.id);" />
                                </td>
                            </tr>
                            <tr id="gadgetSample2_deleteMyLinkButtonRow" align="right">
                                <td>
                                <span id="gadgetSample2JB.myLinksDeleteESign_eSign" style="font-size:8pt;"></span>&nbsp;&nbsp;
                                <button type="button" id="gadgetSample2_deleteMyLinkButton" 
             	                        class="gadgetSample2_deleteMyLinkButton">Delete</button>&nbsp;
                                <button type="button" id="gadgetSample2_cancelMyLinkButton" 
             	                        class="gadgetSample2_cancelMyLinkButton">Cancel</button>
                                </td>
                            </tr>
                            </table>
                            </s:form>
                            </div>
                        </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <div id="gadgetSample2_myLinksHTML"></div>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="50%">
                <table class="gdt-table" style="width:100%;">
                    <tr>
                        <td>
                        <table class="gdt-header-row" style="width:100%;">
                            <tr>
                            <td>Account Links</td>
                            <td align="right" valign="middle">
                                <a href="accountlinkbrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=4" >All<!-- <img 
                                    id="editMyLinksImg" title="Jump to Edit Links" align="middle" align="middle"
                                    src="./images/edit.gif" style="height:18px; width:18px; border:none; padding:1px"
                                /> --></a>
                                &nbsp;
                                <s:if test="%{gadgetSample2JB.hasAcctLinksNewRights}" >
                                <a href="javascript:gadgetSample2.bothLinks.addNewLink('acctLinks');" >
                                    <img id="addMyLinksImg" title="Add New Link" align="middle" align="middle"
                                            src="./images/Add.gif" style="height:18px; width:18px; border:none; padding:1px"/>
                                </a>
                                </s:if>
                            </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <div id="gadgetSample2_acctLinksNewLink" style="display:none">
                            <div id="gadgetSample2_acctLinksFormNewLinkDiv">
                            <s:form name="gadgetSample2_formNewAcctLink" id="gadgetSample2_formNewAcctLink" 
                                    onsubmit="return false;">
                            <table id="gadgetSample2_acctLinksFormNewLinkTable" class="gdt-table" style="width:100%;">
                            <tr>
                                <td>
                                    <table id="gadgetSample2_acctLinksFormNewLinkHeader" style="width:100%;">
                                    <tr>
                                        <td><b>Add New Link</b></td>
                                        <td align="right"></td>
                                    </tr>
                                    </table>
                                    <table id="gadgetSample2_acctLinksFormEditLinkHeader" style="width:100%; display:none">
                                    <tr>
                                        <td><div id='gadgetSample2_acctLinksFormEditLinkHeadderLabel'></div></td>
                                        <td align="right"></td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div id="gadgetSample2JB.acctLinksLinkDisplay_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <label for="gadgetSample2JB.acctLinksLinkDisplay">Display Text</label><FONT class="Mandatory"> *</FONT><br/>
                                
                                <s:textfield id="gadgetSample2JB.acctLinksLinkDisplay" cssClass="gdt-input-text"
                                        name="gadgetSample2JB.acctLinksLinkDisplay" type="text" size="30" maxlength="100" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div id="gadgetSample2JB.acctLinksLinkUrl_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <label for="gadgetSample2JB.acctLinksLinkUrl">URL</label><FONT class="Mandatory"> *</FONT><br/>
                                <s:textfield id="gadgetSample2JB.acctLinksLinkUrl" cssClass="gdt-input-text"
                                        name="gadgetSample2JB.acctLinksLinkUrl" type="text" size="30" maxlength="100" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div id="gadgetSample2JB.acctLinksLinkSection_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <label for="gadgetSample2JB.acctLinksLinkSection">Section</label><br/>
                                <s:textfield id="gadgetSample2JB.acctLinksLinkSection" cssClass="gdt-input-text"
                                        name="gadgetSample2JB.acctLinksLinkSection" type="text" size="30" maxlength="100" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div id="gadgetSample2JB.acctLinksESign_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <label for="gadgetSample2JB.acctLinksESign">e-Signature</label><FONT class="Mandatory"> *</FONT><br/>
                                <s:password id="gadgetSample2JB.acctLinksESign" cssClass="gdt-input-password" 
                                        name="gadgetSample2JB.acctLinksESign" size="30" maxlength="8" 
                                        onkeyup="replicateESign(this.id);" />
                                <s:hidden id="gadgetSample2JB.acctLinksLinkId" 
                                        name="gadgetSample2JB.acctLinksLinkId" />
                                </td>
                            </tr>
                            <tr>
                                <td><div id="gadgetSample2_formNewAcctLink_resultMsg"></div></td>
                            </tr>
                            <tr>
                                <td>
                                    <span id="gadgetSample2JB.acctLinksESign_eSign"></span>
                                </td>
                            </tr>
                            <tr id="gadgetSample2_newAcctLinkButtonRow" align="right">
                                <td>
                                
                                <button type="submit" id="gadgetSample2_saveNewAcctLinkButton" 
             	                        class="gadgetSample2_saveNewAcctLinkButton">Save</button>&nbsp;
                                <button type="button" id="gadgetSample2_cancelNewAcctLinkButton" 
             	                        class="gadgetSample2_cancelNewAcctLinkButton">Cancel</button>
                                </td>
                            </tr>
                            </table>
                            </s:form>
                            </div>
                            <div style="height:5px;"></div>
                            <div id="gadgetSample2_acctLinksFormDeleteLinkDiv">
                            <s:form name="gadgetSample2_formDeleteAcctLink" id="gadgetSample2_formDeleteAcctLink" 
                                    onsubmit="gadgetSample2.acctLinks.deleteLink(); return false;">
                            <table id="gadgetSample2_acctLinksFormDeleteLinkTable" class="gdt-table" style="width:100%;">
                            <tr>
                                <td>
                                    <table style="width:100%; ">
                                    <tr>
                                        <td><div id='gadgetSample2_acctLinksFormDeleteLinkHeadderLabel'></div></td>
                                        <td align="right"></td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <div id="gadgetSample2JB.acctLinksDeleteESign_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td><div id="gadgetSample2_formDeleteAcctLink_resultMsg"></div></td>
                            </tr>
                            <tr>
                                <td>
                                <label for="gadgetSample2JB.acctLinksDeleteLinkId">e-Signature</label><FONT class="Mandatory"> *</FONT><br/>
                                <s:hidden id="gadgetSample2JB.acctLinksDeleteLinkId" 
                                        name="gadgetSample2JB.acctLinksDeleteLinkId" />
                                <s:password id="gadgetSample2JB.acctLinksDeleteESign" cssClass="gdt-input-password" 
                                        name="gadgetSample2JB.acctLinksDeleteESign" size="30" maxlength="8" 
                                        onkeyup="replicateESign(this.id);" />
                                </td>
                            </tr>
                            <tr id="gadgetSample2_deleteAcctLinkButtonRow" align="right">
                                <td>
                                <span id="gadgetSample2JB.acctLinksDeleteESign_eSign" style="font-size:8pt;"></span>&nbsp;&nbsp;
                                <button type="button" id="gadgetSample2_deleteAcctLinkButton" 
             	                        class="gadgetSample2_deleteAcctLinkButton">Delete</button>&nbsp;
                                <button type="button" id="gadgetSample2_cancelAcctLinkButton" 
             	                        class="gadgetSample2_cancelAcctLinkButton">Cancel</button>
                                </td>
                            </tr>
                            </table>
                            </s:form>
                            </div>
                        </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <div id="gadgetSample2_acctLinksHTML"></div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div id="gadgetSample2_end"></div>
</div>
