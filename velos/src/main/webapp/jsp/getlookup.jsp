<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%!
private String htmlEncode(String str) {
    return StringUtil.htmlEncodeXss(StringUtil.trueValue(str))
    .replaceAll("\\(","&#40;").replaceAll("\\)","&#41;");
}
private String htmlDecode(String str) {
    return StringUtil.htmlDecode(StringUtil.trueValue(str))
    .replaceAll("&#40;","\\(").replaceAll("&#41;","\\)").replaceAll("&quot;","\"");
}
private static final String LAST_CRITERIA = "LAST_CRITERIA";
private static final String LAST_SEARCH_STR = "LAST_SEARCH_STR";
private static final String CURROUT = "currout";
%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Lookup%><%--Lookup*****--%></title>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<script>
//Added for  Bug 14033 - #CTRP-22496 - Hari Shankar Maurya 
jQuery(window).load(function(){
	jQuery("body").css("position","relative");
}); 
function opensvwin(id) {
       windowName = window.open("studyview.jsp?studyVPk="+ id,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
	 windowName.focus();
;}
function validate(formobj){
	value=formobj.lookup_column.value;
	if (value!="0") {
  		if (formobj.search_data.value.length==0)  {
  			alert("<%=MC.M_EtrValueFor_ContSrch%>");/*alert("Please enter value for search criteria to continue.");*****/
   			formobj.search_data.focus();
   			return false;  
  		}
   	}
}
function createString(formobj,reset){  
	var lcriteria = formobj.criteria_str.value;	
	if (lcriteria=="None") lcriteria="";	
		var lstr;
	if (formobj.lookup_filter.value=="entlookup"){
		formobj.search.value="";
		lcriteria=""; 
		formobj.criteria_str.value=lcriteria;
		formobj.submit();  
	}	  
	if (formobj.search_data.value.length>0)  { 
		if (formobj.lookup_column.value!="0"){
			var paramArray = [formobj.lookup_column.options[formobj.lookup_column.selectedIndex].text,formobj.scriteria.options[formobj.scriteria.selectedIndex].text,formobj.search_data.value];			
			lstr="["+getLocalizedMessageString("L_012",paramArray)+"]" ;	
			if (lcriteria.length>0){ 
				var paramArray1 = [lcriteria,formobj.lookup_column.options[formobj.lookup_column.selectedIndex].text,formobj.scriteria.options[formobj.scriteria.selectedIndex].text,formobj.search_data.value];
				lcriteria=getLocalizedMessageString("L_And_123",paramArray1); 
			} else {
				lcriteria = lstr; 
			}
		}   
	}	
	formobj.criteria_str.value=lcriteria;                                                
}
 //orderBy column number
 function setOrder(formObj,orderBy){
	var lorderType,lsearch_data,lscriteria;
	var llook_column;	
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";		
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";	
	}
	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;
	lsearch_data=formObj.search_data_val.value; 
	llook_column=formObj.lookup_column_val.value; 
	lscriteria=formObj.scriteria_val.value;		formObj.action="getlookup.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType+"&lookup_column="+llook_column+"&scriteria="+lscriteria+"&search_data="+lsearch_data;
	formObj.submit(); 
}
/*function setValue(formobj,disp,data){
	disp=decodeString(disp);
	data=decodeString(data);
	var lform=fnTrimSpaces(formobj.form.value);
	var  lfdatacolumn=fnTrimSpaces(formobj.fdatacolumn.value);
	var  lfdispcolumn=fnTrimSpaces(formobj.fdispcolumn.value);
	ldata=lform+"."+lfdatacolumn+".value";
	ldisp=lform+"."+lfdispcolumn+".value";
	ldata=data;
	ldisp=disp;
    	var fcalculate = fnTrimSpaces(formobj.fcalculate.value);
    	if  (fcalculate != "") {
		var fcalcexpr = fnTrimSpaces(formobj.fcalcexpr.value);    	
		fcalcexpr = fcalcexpr.replace('fdispcolumn',disp);
		fcalcexpr = fcalcexpr.replace('fdatacolumn',data);		
	    	window.opener.document.forms[lform].elements[fcalculate].value=fcalcexp
	}
       
	window.opener.document.forms[lform].elements[lfdatacolumn].value=data;
	window.opener.document.forms[lform].elements[lfdispcolumn].value=disp;
	
	this.close();
	//   	window.opener.document.testlookup.data.value=data ; 
	//	window.opener.document.testlookup.disp.value=disp;
	
}*/
function setValue(formobj,count,flag){	
	var outputStr;
    //outputStr=decodeString(outputStr);    
    	if (!(flag=="remove")) {
    		if (formobj.totalrows.value==1)
    			outputStr=decodeString(formobj.totStr.value);
        	if (formobj.totalrows.value>1){
     			outputStr=decodeString(formobj.totStr[count-1].value);
    		}
     	} else if (flag=="remove")
      		outputStr=decodeString(formobj.outputString.value);    
   		var valueStr="",keyword="";
   		var lform=fnTrimSpaces(formobj.form.value);
   		//alert(outputStr);
   		arrayStr=outputStr.split("[end]");
   		//alert(arrayStr.length);
   		var data,lfdatacolumn,startIndex,endIndex,index,tempKey,tempvalue,tempdata,advDictionary;  
   		for (var i=0;i<arrayStr.length;i++){
   			if (flag=="remove"){
   				startIndex=arrayStr[i].indexOf("[field]")+ 7;
  				endIndex=arrayStr[i].indexOf("[keyword]");
  				lfdatacolumn=arrayStr[i].substring(startIndex,endIndex);
  				window.opener.document.forms[lform].elements[lfdatacolumn].value="";
  				this.close();
   			}else{
   				//alert(arrayStr[i].substring(7,11));    				
				data="";
				startIndex=arrayStr[i].indexOf("[field]")+ 7;
				endIndex=arrayStr[i].indexOf("[keyword]");
				lfdatacolumn=arrayStr[i].substring(startIndex,endIndex);
				startIndex=endIndex+9;
				endIndex=arrayStr[i].indexOf("[dbcol]");
				keyword=arrayStr[i].substring(startIndex,endIndex);	
	       		if (keyword.indexOf("[VELEXPR]")>=0){
	 				valuearray=keyword.split("*");
					for (var j=1;j<valuearray.length;j++){
						valuearray[j]=fnTrimSpaces(valuearray[j].replace('[',''));
						valuearray[j]=fnTrimSpaces(valuearray[j].replace(']',''));
						if ((valuearray[j].indexOf("=")) >=0) 
							tempKey=valuearray[j].substring(0,valuearray[j].indexOf("="));
						else tempKey=valuearray[j];							tempvalue=valuearray[j].substring(valuearray[j].indexOf("=")+1,valuearray[j].length);
							//alert(tempKey+"and"+tempvalue);
						if (tempKey=="VELSTR")     data=data+tempvalue;
						if (tempKey=="VELSPACE") data=data+' ';
						if (tempKey=="VELCRET") data=data+"\r";
						if (tempKey=="VELNLINE") data=data+"\n";
						if (tempKey=="VELKEYWORD"){
							index=valueStr.indexOf(tempvalue);							tempdata=valueStr.substring(index+tempvalue.length+1,valueStr.indexOf("]",index));
							data=data+tempdata;
						}	
					}
				} else {
					startIndex=arrayStr[i].indexOf("[value]")+7 ;
					endIndex=arrayStr[i].length ;
					data=arrayStr[i].substring(startIndex,endIndex);
				}
				
				if (data=="null") data="";
				//alert(data);
				var fld = window.opener.document.forms[lform].elements[lfdatacolumn];
	  			if (fld != undefined) {					
					window.opener.document.forms[lform].elements[lfdatacolumn].value=data;
	  			}  
				valueStr=valueStr+"["+keyword+"="+data+"]";
   			}
   		}    

	// Added by gopu to fix the bugzilla issue #2640
	if(window.opener.document.advevent) {
		window.opener.document.advevent.advDictionary.value=document.lookuppg.viewname.value;
	}
	if (flag != "remove"){
    	this.close();
	}
//   	window.opener.document.testlookup.data.value=data ; 
//	window.opener.document.testlookup.disp.value=disp;	
}

function showoutput(form,count) {
	alert(form.totstr[count].value);
}
</script>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
	<SCRIPT LANGUAGE="JavaScript">
		if ((document.layers) || (navigator.appName == 'Netscape') || (navigator.appName == 'Opera')){  
			if (screen.width=='800'){
				document.write("<Link Rel=STYLESHEET HREF='commonNS86.css' type=text/css>");
			} else if (screen.width=='1024'){		
				document.write("<Link Rel=STYLESHEET HREF='commonNS.css' type=text/css>");
			} else {
				document.write("<Link Rel=STYLESHEET HREF='commonNSgt1024.css' type=text/css>");
			}
		}
		if (document.all) { 
			if (screen.width=='800'){
				document.write("<Link Rel=STYLESHEET HREF='common86.css' type=text/css>");
			}else if (screen.width=='1024'){
				document.write("<Link Rel=STYLESHEET HREF='common.css' type=text/css>");
			}else {
				document.write("<Link Rel=STYLESHEET HREF='commongt1024.css' type=text/css>");
			}
		}	
	</SCRIPT>	
</script>
<Link Rel=STYLESHEET HREF='./styles/yuilookandfeel/yuilookandfeel.css' type=text/css> <%-- YK 27Dec - UI-1 - Requirement --%>

<%
	String src="tdMenuBarItem3";
%>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "java.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,java.sql.*,com.velos.eres.business.common.*,com.velos.eres.web.user.UserJB"%>
<%
	int ienet = 2;
	String agent1 = request.getHeader("USER-AGENT");
   	if (agent1 != null && agent1.indexOf("MSIE") != -1) {
     		ienet = 0; //IE
    	} else {
		ienet = 1;
	}
	if(ienet == 0) {	
%>
		<body class="not-body" style="overflow:scroll;">
	
<%
	} else {
%>
	<body class="not-body" style="position:relative">
<%
	}
%>
<br>
<%
	String pagenum = "";
	
	String ignoreRights = "";
	
	int curPage = 0;
	long startPage = 1;
	long cntr = 0;
	String pagenumView = "";
	int curPageView = 0;
	long startPageView = 1;
	long cntrView = 0;
	String tempSearch="";
	LookupDao lookupDao = new LookupDao();
	String ddlookup ="";
	pagenum = request.getParameter("page");
	if (pagenum == null){
		pagenum = "1";
	}
	curPage = EJBUtil.stringToNum(pagenum);
	String orderBy = "";
	orderBy = request.getParameter("orderBy");
	orderBy = FilterUtil.sanitizeTextForSQL(orderBy);
	String orderType = "";
	orderType = request.getParameter("orderType");
	orderType = FilterUtil.sanitizeTextForSQL(orderType);
	if (orderType == null){
		orderType = "desc"; /*YK 03JAN -  BUG # 5710*/
	}
	if (orderBy==null){
		orderBy="";
	}
	pagenumView = request.getParameter("pageView");
	if (pagenumView == null){
		pagenumView = "1";
	}
	curPageView = EJBUtil.stringToNum(pagenumView);
	String orderByView = "";
	orderByView = request.getParameter("orderByView");
	String orderTypeView = "";
	orderTypeView = request.getParameter("orderTypeView");
	if (orderTypeView == null){
		orderTypeView = "desc"; /*YK 03JAN -  BUG # 5710*/
	}
	String orderBy_str="";
	if (!orderBy.equals("") ){
		orderBy_str = " order by " + orderBy+" " +orderType; 
	}else {
		orderBy_str =  "";
	}
	String search=request.getParameter("search"); 
	if (search==null) search=""; else search=search.replace('~','%');
	search = FilterUtil.sanitizeTextForSQL(search);
	String formname=request.getParameter("form");
	String fDatacolumn=request.getParameter("fdatacolumn");
	String fDispcolumn=request.getParameter("fdispcolumn");
	String fCalculate = request.getParameter("fcalculate");
	String fCalcexpr = request.getParameter("fcalcexpr");
	String fkeyword_str=request.getParameter("keyword");
	String dfilter=FilterUtil.validateDFilter(request);
	%>
	<%if ("1=2".equals(dfilter)){%>
	<div class="">
	<jsp:include page="accessdenied.jsp" flush="true"/>
	</div>
	<%return;
	}%>
	<%
	String dfilterReq=request.getParameter("dfilter");
	dfilterReq = (null == dfilterReq)? "" : dfilterReq;
	if (dfilter!=null){
		if (dfilter.length()>0){
			dfilter=dfilter.replace('~','%');
		}
	} //to be check and remove unwanted bracket
	String searchCriteria = request.getParameter("searchCriteria");
	if (searchCriteria==null) {
		searchCriteria="";
	}
	String search_data=request.getParameter("search_data");
	String search_data_req=request.getParameter("search_data");
	String search_data_val=request.getParameter("search_data_val");
	if ((search_data_req == null || search_data_req.length() == 0)
	        && search_data_val != null && search_data_val.length() > 0) {
	    search_data_req = htmlDecode(search_data_val);
	    search_data = htmlDecode(search_data_val);
	}
	if (search_data==null) search_data="";
	search_data=search_data.toLowerCase();
	search_data=FilterUtil.sanitizeText(search_data);
	String scriteria=request.getParameter("scriteria");
	if (scriteria==null) scriteria="";
	String lookup_column="",dfilter_search="",search_str="",criteria_str="";	
	String lookup_column_val=request.getParameter("lookup_column");  
	criteria_str=request.getParameter("criteria_str");
	if (criteria_str==null) criteria_str="";
	if (criteria_str.length() == 0) {
	    search_data_req = search_data = "";
	}
	HttpSession tSession = request.getSession(true); 
	int vlookup=0;
	if (sessionmaint.isValidSession(tSession)) {	
		String userId = (String) tSession.getValue("userId");
		String accountId=(String) tSession.getValue("accountId");
		UserJB user = (UserJB) tSession.getValue("currentUser");	
		String orgId=user.getUserSiteId();;
		orgId=(orgId==null)?"":orgId;
		if (lookup_column_val!=null){
			if ((lookup_column_val.equals("0")) || (lookup_column_val.length()==0)){
				vlookup=0;
			}else{				vlookup=EJBUtil.stringToNum((request.getParameter("lookup_column")).substring(0,((request.getParameter("lookup_column")).indexOf("~"))));
			} 	
		}
	int pageRight = 0,index=-1;
	String outputStr="",tempOutputStr="",viewName="";
	
	ignoreRights = (String) tSession.getValue("pp_ignoreAllRights") ;
	
	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
	
	if (grpRights!=null)
	{		
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
	}
	
	//for patient portal
	 if (StringUtil.isEmpty(ignoreRights))
		{
			ignoreRights = "false";
		}
	if (ignoreRights.equals("true"))
		{
			pageRight = 7;
			accountId=(String) tSession.getValue("pp_accountId");
		}
		
	String viewId=(request.getParameter("viewId"));
	String protocolid=(request.getParameter("protocolid"));
	viewId=(viewId==null)?"":(viewId);
	String viewNameKeyword = request.getParameter("viewNameKeyword");
	if (!StringUtil.isEmpty(viewNameKeyword)) {
		String viewIdNew = lookupDao.getViewIdByViewKeyword(viewNameKeyword);
		if (StringUtil.stringToNum(viewIdNew) > 0) {
			viewId = viewIdNew;
		}
	}
	if (viewId.length()==0)	{
		viewName=request.getParameter("viewName");
		System.out.println("viewName"+viewName);		
		viewId=lookupDao.getViewId(viewName);
	}
	lookupDao.getViewDetail(EJBUtil.stringToNum(viewId));	
	String viewFilter=lookupDao.getViewFilter();
	viewFilter=(viewFilter==null)?"":viewFilter;
	System.out.println("INDEX"+viewFilter.indexOf("[:ACCID]"));
	if (viewFilter.indexOf("[:ACCID]")>=0) 
		viewFilter=StringUtil.replace(viewFilter,"[:ACCID]",accountId);
	if (viewFilter.indexOf("[:USERID]")>=0) 
		viewFilter=StringUtil.replace(viewFilter,"[:USERID]",userId);
	if (viewFilter.indexOf("[:ORGID]")>=0) 
		viewFilter=StringUtil.replace(viewFilter,"[:ORGID]",orgId);	
	if (viewFilter.indexOf("[:PROTID]")>=0) 
		viewFilter=StringUtil.replace(viewFilter,"[:PROTID]",protocolid);	
	//System.out.println("viewid in getlookup.jsp"+viewId);
	lookupDao.getLookupView(viewId,"lookup_column",vlookup,"");
	lookupDao.getReturnValues(viewId);
	ddlookup = lookupDao.getLookupDropDown();
	String retDisp=lookupDao.getRetDispValue();
	String retData=lookupDao.getRetDataValue();
	ArrayList lKeyword=lookupDao.getLViewRetKeywords();
	ArrayList lColumns=lookupDao.getLViewRetColumns();
	ArrayList lkpCollen=lookupDao.getLViewColLen();
	ArrayList lkpIsDisp=lookupDao.getLViewIsDisplay();
	ArrayList retKeyword=new ArrayList();
	ArrayList retFields=new ArrayList();
	ArrayList urlKeywords=new ArrayList();
	ArrayList urlFields=new ArrayList();
	ArrayList dbKeywords=lookupDao.getLViewRetKeywords();
	ArrayList dbColumns=lookupDao.getLViewRetColumns();
	ArrayList genOutput=new ArrayList();
	ArrayList tempCol=new ArrayList();
	//populate return keywords arraylist
	StringTokenizer st_sep=null;
	StringTokenizer st = new StringTokenizer(fkeyword_str,"~");
	     while (st.hasMoreTokens()) {
	      	 st_sep= new StringTokenizer(st.nextToken(),"|");
		 urlFields.add(st_sep.nextToken());
		 urlKeywords.add((String)st_sep.nextToken());		 
	     }
	 for (int i=0;i<urlKeywords.size();i++)	{
	  //System.out.println(i + (String)urlKeywords.get(i));	
		if ((dbKeywords.contains(urlKeywords.get(i))) || (((String)urlKeywords.get(i))).indexOf("[VELEXPR]") >= 0){
			index=dbKeywords.indexOf(urlKeywords.get(i));
			//System.out.println(index);
			if (index>=0 ){
			outputStr="[field]"+urlFields.get(i)+"[keyword]"+urlKeywords.get(i)+"[dbcol]"+dbColumns.get(index);
			//System.out.println("adding value"+dbColumns.get(index));
			tempCol.add(((String)dbColumns.get(index)).toLowerCase());
			} else {
			outputStr="[field]"+urlFields.get(i)+"[keyword]"+urlKeywords.get(i)+"[dbcol]"+"[VELEXPR]";
			tempCol.add("[VELEXPR]");
				}  
			genOutput.add(outputStr);
		}	  
	 }
	 //create seach string
	if (lookup_column_val!=null){
		if ((lookup_column_val.equals("0")) || (lookup_column_val=="")) {}else{
			lookup_column=lookup_column_val.substring(((request.getParameter("lookup_column")).indexOf("~"))+1);
		}
	}
	if ((lookup_column_val!=null)&& (search_data.length()>0)){			
		if ((lookup_column_val.equals("0")) || (lookup_column_val=="")) {}
		else if (lookup_column.equals("[VELALL]")){
			String tempStr="";
			ArrayList tempList=lookupDao.getLViewColumns();
		if (search.length()>0) search="("+search+")";
			tempSearch="";
		for (int i=0;i<tempList.size();i++){ 
	 		tempStr=(String)tempList.get(i);
			if (scriteria.equals("contains")) {	
	 			if (tempSearch.length()>0) {
					tempSearch= tempSearch + "  or (lower("+tempStr+") like  lower('%"+search_data.trim() +"%'))";
				}else{
	 				tempSearch=" (lower("+tempStr+") like  lower('%"+search_data.trim() +"%'))";}
				}
			if (scriteria.equals("isequalto")){	
	 			if(tempSearch.length()>0) {
					tempSearch= tempSearch + " or (lower(" + tempStr+") =  lower('"+search_data.trim() +"'))"; 
				}else{
	 				tempSearch = " (lower(" + tempStr+") =  lower('"+search_data.trim() +"'))";
	 			}
			}
			if (scriteria.equals("start"))	{ 
				if(tempSearch.length()>0){
	  				tempSearch=tempSearch + " or (lower(" + tempStr+") like  lower('"+search_data.trim() +"%'))" ; 
				} else {
	  				tempSearch= "  (lower(" + tempStr+") like  lower('"+search_data.trim() +"%'))" ; 
	  			}
			}
			if (scriteria.equals("ends")){	
	 			if(tempSearch.length()>0) {
					tempSearch= tempSearch + " or (lower(" + tempStr+") like lower('%"+search_data.trim() +"'))" ;
				} else {
					tempSearch = " (lower(" + tempStr+") like lower('%"+search_data.trim() +"'))" ;
				}
			}	
	}//end for loop of templist	
	if (tempSearch.length()>0) tempSearch="("+tempSearch+")";
		}//end check for  (lookup_column_val.equals("[VELALL]"))
	else{
		if (scriteria.equals("contains")){	
			if (search.length()>0) {
				search= search + "  and lower("+lookup_column+") like  lower('%"+search_data.trim() +"%')";
			}else{
				search=" lower("+lookup_column+") like  lower('%"+search_data.trim() +"%')";
			}
		}
		if (scriteria.equals("isequalto")){	
			if(search.length()>0) {
				search= search + " and lower(" + lookup_column+") =  lower('"+search_data.trim() +"')"; 
			}else{
				search = " lower(" + lookup_column+") =  lower('"+search_data.trim() +"')";}
		}
		if (scriteria.equals("start"))	{ 
			if(search.length()>0){
				search=search + " and lower(" + lookup_column+") like  lower('"+search_data.trim() +"%')" ; 
			} else {
				search= "  lower(" + lookup_column+") like  lower('"+search_data.trim() +"%')" ; 
			}
		}
		if (scriteria.equals("ends"))	{	
			if(search.length()>0) {
				search= search + " and lower(" + lookup_column+") like lower('%"+search_data.trim() +"')" ;
			} else {
				search = " lower(" + lookup_column+") like lower('%"+search_data.trim() +"')" ;
			}
		}
	}
}
		
 	search=(search==null)?"":search;	   
  	if (search.length()>0){
		if (tempSearch.length()>0)
	    		search=search+" and "+tempSearch;
  	  	} else {
	   	if (tempSearch.length()>0)
	      		search=tempSearch;      
	   	}     
  		if (search.length()>0) search_str=" and " + search;
  	if (viewFilter.length()>0)
  		search_str=search_str+ " and " + viewFilter;
	if (CURROUT.equals(request.getParameter("lookup_filter")) ||
	        criteria_str.indexOf("] AND [") > -1) {
  	    String lastCriteria = StringUtil.trueValue((String)tSession.getAttribute(LAST_CRITERIA));
  	    if (criteria_str.indexOf(lastCriteria) > -1) {
  	        if (criteria_str.equals(lastCriteria)) {
  	        	search_str = StringUtil.trueValue((String)tSession.getAttribute(LAST_SEARCH_STR));
  	        } else {
  	        	search_str += StringUtil.trueValue((String)tSession.getAttribute(LAST_SEARCH_STR));
  	        }
  	    }
  	}
  	if (criteria_str.length() > 0) {
		tSession.setAttribute(LAST_CRITERIA, criteria_str);
		tSession.setAttribute(LAST_SEARCH_STR, search_str);
  	}
	System.out.println("after complete"+search_str);
	if (dfilter == null || dfilter.length() == 0){
		dfilter_search=search_str+orderBy_str;
	} else {
 		if (dfilter.length()>0){
 			dfilter_search=" and "+dfilter+search_str+orderBy_str; 	  
		}
	}
	 //end creating
	long rowsPerPage=0;
   	long totalPages=0;	
	long rowsReturned = 0;
	long showPages = 0;
	long totalRows = 0;	   
	long firstRec = 0;
	long lastRec = 0;	   
	ArrayList bColumns;
	String studyNumber = null;
	String studyTitle = null;
	String studyPhase = null;
	String studyStatus = null;
	String statusSubType = null;
	String studyStatusNote = null;
	String studyStatusDate = null;
	int studyId=0,dynsize=0;
	boolean hasMore = false;
	boolean hasPrevious = false;	
	String  study_verparent = "",retDispVal="",retDataVal="";
	rowsPerPage =  Configuration.ROWSPERBROWSERPAGE ;
	totalPages =Configuration.PAGEPERBROWSER ; 
	BrowserRows br = new BrowserRows();	     
	rowsPerPage=100;
	br.getPageRows(curPage,rowsPerPage,totalPages,orderBy,orderType,EJBUtil.stringToNum(viewId),dfilter_search);
	bColumns=br.getBColumns();	
	dynsize=(100/(bColumns.size()-1));
	rowsReturned = br.getRowReturned();
	showPages = br.getShowPages();
	startPage = br.getStartPage();
	hasMore = br.getHasMore();
	hasPrevious = br.getHasPrevious();
	totalRows = br.getTotalRows();	    
	firstRec = br.getFirstRec();
	lastRec = br.getLastRec();	  	  
%>  
<Form name="lookuppg" method=post onsubmit="createString(document.lookuppg,'N');" action="getlookup.jsp?srcmenu=<%=src%>&page=1">
	<P class="sectionHeadings">&nbsp;&nbsp; <%=lookupDao.getViewName()%> </P>
	<input name="viewname" type="hidden" value="<%=lookupDao.getViewName()%>"/>
	<table WIDTH="100%" CELLSPACING="0" CELLPADDING="0" border="0" class="basetbl">
		<tr>
			<td><%=LC.L_Search%><%--Search *****--%></td>
			<td><select size="1" name="lookup_filter"><option selected value="entlookup"><%=LC.L_Entire_Lookup%><%-- Entire Lookup*****--%></option>
				<option value="currout"><%=LC.L_Cur_Output%><%-- Current Output*****--%></option></select></td>
			<td><P><%=ddlookup%></td>
 			<td><select size="1" name="scriteria">
		  		<option value="contains" <%if (scriteria.equals("contains")){%> Selected <%}%>><%=LC.L_Contains%><%-- Contains*****--%></option>
  				<option value="isequalto" <%if (scriteria.equals("isequalto")){%> Selected <%}%>><%=LC.L_Exactly_Matches%><%-- Exactly Matches*****--%></option>
			  	<option value="start" <%if (scriteria.equals("start")){%> Selected <%}%>><%=LC.L_Begins_With%><%-- Begins with*****--%></option>
			  	<option value="ends" <%if (scriteria.equals("ends")){%> Selected <%}%>><%=LC.L_Ends_With%><%-- Ends with*****--%></option>
			  	</select>
			</td>	
			<td width="5"><input name="search_data" type="text" ></td>
			<td>
				<P><button type="submit" onClick="return validate(document.lookuppg)"><%=LC.L_Search%></button></P>
			</td>
			<td>
				<A href="#" onClick="setOrder(document.lookuppg,'')"><%=LC.L_Reset_Sort%><%-- Reset Sort*****--%></A>
			</td>
	  	</tr>		

		<tr>
        		<%if (criteria_str.length()==0) criteria_str="None";%>	
        		<input name="criteria_str" type="hidden" value="<%=htmlEncode(criteria_str)%>">	
        	<tr>
        		<td colspan="7"><%=LC.L_Filter_Criteria%><%-- Filter Criteria*****--%>: <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=htmlEncode(criteria_str).replaceAll("&lt;/b&gt;","</b>").replaceAll("&lt;b&gt;","<b>")%>
			</td>
		</tr>	

		<tr>
			<td colspan="5">
				<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
				<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
			<%} else {%>
				<font class="recNumber"><%=MC.M_NoRecordsFound%><%-- No Records Found*****--%></font>	
			<%}%>
			</td>
			<td  colspan="5">
			<!--Added by Gopu to fix the bugzilla Issue #2640 -->
				<A href="#" onClick="setValue(document.lookuppg,'','remove')"><%=LC.L_Rem_SelectedRecord%><%-- Remove Selected Record*****--%></A>
				</td>
		</tr>
	</table>    	
	<Input type="hidden" name="srcmenu" value="<%=src%>">
	<Input type="hidden" name="page" value="<%=curPage%>">
 	<%-- YK 27Dec - UI-1 - Requirement--%>		
	<%
	orderType=orderType.equals("")?"desc":orderType;
	%>
	<Input type="hidden" name="orderBy" value="<%=orderBy%>">
	<Input type="hidden" name="orderType" value="<%=orderType%>">
	<Input type="hidden" name="totalrows" value="<%=totalRows%>">	
    	<input name="viewId" type="hidden" value="<%=viewId%>">
    	<input name="protocolid" type="hidden" value="<%=protocolid%>">
    	<input name="keyword" type="hidden" value="<%=fkeyword_str%>">
    	<% if (lookup_column_val==null) lookup_column_val="";%>
    	<input name="lookup_column_val" type="hidden" value="<%=lookup_column_val%>">	
    	<input name="search_data_val" type="hidden" value="<%=htmlEncode(search_data_req)%>">		
    	<input name="scriteria_val" type="hidden" value="<%=scriteria%>">		
    	<input name="form" type="hidden" value="<%=formname%>">			
    	<input name="fdatacolumn" type="hidden" value="<%=fDatacolumn%>">		
    	<input name="fdispcolumn" type="hidden" value="<%=fDispcolumn%>">		
   	<input name="dfilter" type="hidden" value="<%=StringUtil.htmlEncodeXss(dfilterReq).replace('%','~').replaceAll("\\(","&#40;").replaceAll("\\)","&#41;")%>">			
	<input name="search" type="hidden" value="">
    	<input name="fcalculate" type="hidden" value="<%=fCalculate%>">			
    	<input name="fcalcexpr" type="hidden" value="<%=fCalcexpr%>">
    	
    	<%-- YK 27Dec - UI-1 - Requirement Starts --%>			
    <table width="100%" border="0" cellspacing="0" class="custom-table-head" cellpadding="0"  >
     		<tr > 
        		<th width="5%" class="normalcolNo" height="20"><%=LC.L_Select%><%--Select*****--%></th>
       	<%	
       	String arrowImg="";
        String title="";
        String imageTitle="";
       	for (int count_column=0;count_column<(bColumns.size()-1);count_column++){
       	  		if (lkpIsDisp.get(count_column).equals("Y")){
       	  		/*YK 03JAN -  BUG # 5710*/
       	       		if(orderBy.equals(""))
       	       		{
       	       			orderBy=String.valueOf(count_column+1);
       	       			%>
       	       			<script language="JavaScript">
       	       			
       	       			setOrder(document.lookuppg,<%=count_column+1%>);
       	       			</script>
       	       			<%
       	       		}
       	  		  if(orderBy.equals(String.valueOf(count_column+1))){
             			arrowImg=(orderType.equals("desc"))?"./images/yuilookandfeel/sortdescending.png":"./images/yuilookandfeel/sortascending.png";
             			imageTitle=(orderType.equals("desc"))?LC.L_SortAscending/*Sort Ascending******/:LC.L_SortDescending/*"Sort Descending"*****/;
             			title=(orderType.equals("desc"))?MC.M_Click_SortAscend/*Click to sort ascending******/:MC.M_Click_SortDescend/*"Click to sort descending"*****/;
       	  			
        %>
        		<th class="selectedcol"  title="<%=title %>" width="<%=lkpCollen.get(count_column)%>" onClick="setOrder(document.lookuppg,'<%=count_column+1%>' )"><%=(String)bColumns.get(count_column)%> <img src="<%=arrowImg%>" title="<%=imageTitle%>" alt="<%=LC.L_Arrow_Lower %><%-- arrow*****--%>"/></th>
  			<%}else{
  				
  				%>
  						<th class="normalcol"   width="<%=lkpCollen.get(count_column)%>" onClick="setOrder(document.lookuppg,'<%=count_column+1%>' )"><%=(String)bColumns.get(count_column)%> </th>
  				<%

  			}
       	  		  }
    		}	
   
     	%>     
      		</tr>	  
      	<%
      	 String normalrowCss="";
        String selectedColCss="";
    		for(int counter = 1;counter<=rowsReturned;counter++){	
			outputStr="";	
			index=-1;
		
			if ((counter%2)==0) 
				{
					normalrowCss="normalinfoeven";
					selectedColCss="selectedinfoeven";
				}
				else{
					normalrowCss="normalinfoodd";
				       selectedColCss="selectedinfoodd";
					}%>
				<tr class="normalinfo" height="25">
				<td  class="<%= normalrowCss%>"  ><!--<A href="#" onclick="setValue(document.lookuppg,'<%=retDispVal%>','<%=retDataVal%>')">Select</A>-->
				<!--Added by Gopu to fix the bugzilla Issue #2640 -->
				<A type="submit" onclick="setValue(document.lookuppg,'<%=counter%>','')"><%=LC.L_Select%></A> <%-- YK 03JAN -  BUG # 5709 --%>
			<%-- YK 27Dec - UI-1 - Requirement Ends --%>
      		</td>
<%
	for (int i=0;i<(tempCol.size());i++){
  		index=(bColumns).indexOf(((String)tempCol.get(i)).toUpperCase());	
		if (index>=0) {
			if (outputStr!=null){
				if (outputStr.length()>0)	
				outputStr=outputStr+"[end]"+genOutput.get(i) + "[value]"+(String)br.getBValues(counter,(String)bColumns.get(index));
				else outputStr=genOutput.get(i) + "[value]"+(String)br.getBValues(counter,(String)bColumns.get(index));
			}		
		}	
	}	
        for (int i=0;i<(bColumns.size()-1);i++){
  	  	  String tempStr=(String)br.getBValues(counter,(String)bColumns.get(i));
  	  	  if (tempStr!=null && !tempStr.equals("null")){ //KM-03/10/2008
		  	tempStr=(String)br.getBValues(counter,(String)bColumns.get(i));
		  	
		  } else {	
		  	tempStr="-";
		  }  
      		//check if the colums is defined hidden,hide it if it is
  		if (lkpIsDisp.get(i).equals("Y")){ 
  			/*YK 27Dec - UI-1 - Requirement Starts*/
				if(orderBy.equals(String.valueOf(i+1))){
				%>	 
				
				<td class="<%= selectedColCss%>" width="<%=lkpCollen.get(i)%>"> <%=tempStr%> </td>
				<%
				}else{
				%>	 
				
				<td class="<%= normalrowCss%>"  width="<%=lkpCollen.get(i)%>"> <%=tempStr%> </td>
				
				<%}
  			/* YK 27Dec - UI-1 - Requirement Ends */
     		} else {%>
     			<Input name="<%=(String)bColumns.get(i)%>" type="hidden" value="<%=tempStr%>">
     		<%} 
     	} //end for (int i=0;i<(bColumns.size()-1);i++)
     	tempOutputStr="";
     	for (int z=0;z<tempCol.size();z++){
     	//System.out.println(tempCol.get(z));
     		if (tempCol.get(z).equals("[VELEXPR]")) {	 
			if (tempOutputStr!=null){
				if (tempOutputStr.length()>0) 
					tempOutputStr=tempOutputStr+"[end]"+genOutput.get(z)+"[value]"+"";
				else 
					tempOutputStr="[end]"+genOutput.get(z)+"[value]"+"";
			}
	//System.out.println(tempOutputStr);
		}
        }     
     	retDataVal=StringUtil.encodeString(retDataVal);  
     	retDispVal=StringUtil.encodeString(retDispVal);
	outputStr=outputStr + tempOutputStr;
	outputStr=StringUtil.encodeString(outputStr);
	//System.out.println(outputStr);
     	
      %> 
      
      <input type="hidden" value="<%=outputStr%>" name="totStr">
      </tr>
     
      <%
		}
%>
    </table>
		<input type="hidden" name="outputString" value='<%=outputStr%>'> 	
	<table>
		<tr>
			<td>
				<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
					<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
				<%} else {%>
					<font class="recNumber"><%=MC.M_NoRecordsFound%><%-- No Records Found*****--%></font>	
				<%}%>	
			</td>
		</tr>
	</table>	
	<table align=center>
		<tr>
			<% if (curPage==1) startPage=1;
    		for (int count = 1; count <= showPages;count++)	{
   			cntr = (startPage - 1) + count;
		if ((count == 1) && (hasPrevious))	{
%>
			<td colspan = 2>
				<A style="font-size:10pt;font-weight:bold;" href="getlookup.jsp?search=<%=search.replace('%','~')%>&criteria_str=<%=criteria_str%>&scriteria=<%=scriteria%>&lookup_column=<%=lookup_column_val%>&srcmenu=<%=src%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&viewId=<%=viewId%>&dfilter=<%=StringUtil.htmlEncodeXss(dfilterReq).replace('%','~').replaceAll("\\(","&#40;").replaceAll("\\)","&#41;")%>&form=<%=formname%>&fdatacolumn=<%=fDatacolumn%>&fdispcolumn=<%=fDispcolumn%>&keyword=<%=fkeyword_str%>">< <%=LC.L_Previous%><%-- Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
			</td>	
<%
		 }
%>
			<td>
<%
		 if (curPage  == cntr)	 {
%>	   
		<FONT class = "pageNumber"><%= cntr %></Font>
<%      } else {
%>		
		
	<A style="font-size:10pt;font-weight:bold;" href="getlookup.jsp?search=<%=search.replace('%','~')%>&criteria_str=<%=criteria_str%>&scriteria=<%=scriteria%>&lookup_column=<%=lookup_column_val%>&srcmenu=<%=src%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&viewId=<%=viewId%>&dfilter=<%=StringUtil.htmlEncodeXss(dfilterReq).replace('%','~').replaceAll("\\(","&#40;").replaceAll("\\)","&#41;")%>&form=<%=formname%>&fdatacolumn=<%=fDatacolumn%>&fdispcolumn=<%=fDispcolumn%>&keyword=<%=fkeyword_str%>"><%= cntr%>
	</A>
<%	}	
%>
	</td>
<%
  }
	if (hasMore){   
%>
	<td colspan = 3 align = center>
  	&nbsp;&nbsp;&nbsp;&nbsp;
		<A style="font-size:10pt;font-weight:bold;" href="getlookup.jsp?search=<%=search.replace('%','~')%>&criteria_str=<%=criteria_str%>&scriteria=<%=scriteria%>&lookup_column=<%=lookup_column_val%>&srcmenu=<%=src%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&viewId=<%=viewId%>&dfilter=<%=StringUtil.htmlEncodeXss(dfilterReq).replace('%','~').replaceAll("\\(","&#40;").replaceAll("\\)","&#41;")%>&form=<%=formname%>&fdatacolumn=<%=fDatacolumn%>&fdispcolumn=<%=fDispcolumn%>&keyword=<%=fkeyword_str%>">< <%=LC.L_Next%><%-- Next*****--%> <%=totalPages%>></A>
	</td>	
	<% } %>
   </tr>
  </table>
    
<%
	
%>	  </Form>
<script>
	document.lookuppg.search_data.focus();
</script>
<%
	}//end of if body for session
	else {
%>
	<jsp:include page="timeout.html" flush="true"/>
	<% } %>
<br>
<div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</html>
