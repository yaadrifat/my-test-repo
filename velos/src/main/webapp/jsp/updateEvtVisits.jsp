<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@page import="com.velos.eres.service.util.MC,com.velos.esch.business.common.EventdefDao,com.velos.esch.business.common.EventAssocDao,com.velos.esch.web.eventdef.EventdefJB,com.velos.esch.web.eventassoc.EventAssocJB,com.velos.eres.service.util.EJBUtil,com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.LC,com.velos.eres.service.util.Configuration"%>
<%@page import="com.velos.eres.service.util.StringUtil"%>
<%@page import="java.util.ArrayList, java.util.HashMap"%>
<%@ page import="java.util.Hashtable" %>

<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id ="userBean" scope="session" class="com.velos.eres.business.user.impl.UserBean"/>
<jsp:useBean id ="userJB" scope="session" class="com.velos.eres.web.user.UserJB"/>

<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	HttpSession tSession = request.getSession(true);
	String checkAllSave=request.getParameter("checkAllSave");
	String ipAdd = (String) tSession.getAttribute("ipAdd");
    String userId = (String) tSession.getAttribute("userId");
	String accountId = (String) tSession.getAttribute("accountId");
	String protId = request.getParameter("calProtocolId");
	String calledFrom = request.getParameter("calledfrom");
	String userFullName = "";
	userJB.setUserId(StringUtil.stringToNum(userId));
	userBean= userJB.getUserDetails();
	userFullName = userId+", "+userBean.getUserLastName()+", "+userBean.getUserFirstName();
	int updateAudit_row =0;
	int myEventDefId = 0;
	String pkEvtDefVal ="",ridEvtDefVal="";
	
	if("checkAllSave".equals(checkAllSave)){
		ArrayList event_ids=new ArrayList();
		ArrayList hideFlags = new ArrayList();
		ArrayList event_names=new ArrayList();
		ArrayList<String> visit_ids=new ArrayList<String>();
		ArrayList<String> categoryNames=new ArrayList<String>();
		ArrayList<String> updatedListHide1 = new ArrayList<String>();
		ArrayList<String> displacement = new ArrayList<String>();
		ArrayList<String> eventIds = new ArrayList<String>();
		 ArrayList deleteList1 = new ArrayList();
		String tname="";
		String localvr="abc";
		String cldForm=request.getParameter("cldForm");
		String operation=request.getParameter("operation");
		ArrayList<String> updatedListSeq1 = new ArrayList<String>();
		System.out.println("save vertical found ");
		String visitId=request.getParameter("visitId");
		String myDisplacements=request.getParameter("myDisplacements");
		String eventId=request.getParameter("eventId");
		
		String vId = visitId.substring(visitId.indexOf("v")+1);
		EventdefDao eventDefDao=new EventdefDao();
		EventAssocDao eventAssocDao=new EventAssocDao();
		if("event".equals(request.getParameter("saveType"))){
			if("P".equals(cldForm) || "L".equals(cldForm)){
				tname="EVENT_DEF";
			eventDefDao.getEventListForSaveInEventVisitGrid(StringUtil.stringToNum(protId),StringUtil.stringToNum(vId),operation);
		 event_ids=eventDefDao.getEvent_ids();
		 event_names=eventDefDao.getCosts();
		 categoryNames=eventDefDao.getEventCategory();
		 for(int i=0;i<event_ids.size();i++){
			 visit_ids.add(vId)	; 
			 updatedListHide1.add("undefined");
			 displacement.add(myDisplacements);
			
		 }
			}else{
				tname="event_assoc";
				eventAssocDao.getEventListForSaveInEventVisitGridFromStudy(StringUtil.stringToNum(protId),StringUtil.stringToNum(vId),operation);
				event_ids=eventAssocDao.getEvent_ids();
				event_names=eventAssocDao.getCosts();
				categoryNames=eventAssocDao.getEventCategory();
				hideFlags=eventAssocDao.getFlags();
				 for(int i=0;i<event_ids.size();i++){
					 visit_ids.add(vId)	; 
					 String flag="";
					 if(hideFlags.get(i)==null){
						 flag="0" ;
						 updatedListHide1.add(flag);
					 }else{
						 updatedListHide1.add(hideFlags.get(i).toString()); 
					 }
					
					 displacement.add(myDisplacements);
					
				 }
				
			}
		}else{
			eventDefDao.getVisitListForSaveInEventVisitGrid(StringUtil.stringToNum(protId),StringUtil.stringToNum(eventId),operation);
		}
		
		
		HashMap maxEvtSeqMap1 = new HashMap();
		for(int i=0;i<event_ids.size();i++){
		if ("A".equals(request.getParameter("operation"))) { // Add operation
			int maxSeqVal = -1;
			if (maxEvtSeqMap1.get(visit_ids.get(i)) == null) {
				maxSeqVal = eventdefB.getMaxSeqForVisitEvents(
			          Integer.parseInt(vId), 
			          event_names.get(i).toString(), tname,categoryNames.get(i).toString());
				if (maxSeqVal < 0) {
					
					continue;
				}
				maxEvtSeqMap1.put(visit_ids.get(i), ++maxSeqVal);
			} else {
				maxSeqVal = (Integer)maxEvtSeqMap1.get(visit_ids.get(i));
				maxEvtSeqMap1.put(visit_ids.get(i), ++maxSeqVal);
			}
			String seq = ""+maxSeqVal;
			
			
			updatedListSeq1.add(seq);
			
			eventIds.add(event_ids.get(i).toString());
			
		}
		else if ("D".equals(operation)) { 
			Integer myEvtId = new Integer(
	                eventdefB.fetchEventIdByVisitAndDisplacement(event_ids.get(i).toString(), tname, visit_ids.get(i), myDisplacements));
	        if (myEvtId < 1) {
	        	myEvtId = new Integer(eventdefB.fetchEventIdByVisit(event_ids.get(i).toString(), tname, visit_ids.get(i)));
	        }
	        deleteList1.add(String.valueOf(myEvtId));
		}
		 }
		
		
		if (eventIds.size() > 0) {
			if("P".equals(calledFrom) || "L".equals(calledFrom)){
			String [] blankArray1 = {};	
			EventdefJB evtdefB = new EventdefJB();
			int ret = evtdefB.createEventsDef(
					eventIds.toArray(blankArray1),
					visit_ids.toArray(blankArray1),
					updatedListSeq1.toArray(blankArray1),
					displacement.toArray(blankArray1),
					updatedListHide1.toArray(blankArray1),
   					StringUtil.stringToNum(userId), ipAdd);
   			if (ret < 0) {
   				throw new Exception(MC.M_EvntVisitConstraintViolation);
   			}
			}else{
				EventAssocJB evtassB = new EventAssocJB();
       			String [] blankArray = {};
       			int ret = evtassB.createEventsAssoc(
       					eventIds.toArray(blankArray),
       					visit_ids.toArray(blankArray),
       					updatedListSeq1.toArray(blankArray),
       					displacement.toArray(blankArray),
       					updatedListHide1.toArray(blankArray),
       					StringUtil.stringToNum(userId), ipAdd);
       			if (ret < 0) {
       				throw new Exception(MC.M_EvntVisitConstraintViolation);
       			}
			}
		}
		
		if (deleteList1.size() > 0) {
			boolean hasInvalidId = false;
			for (int iX=0; iX<deleteList1.size(); iX++) {
				if (deleteList1.get(iX) == null) {
					hasInvalidId = true;
					break;
				}
				try {
					int myEventId = Integer.parseInt((String)deleteList1.get(iX));
					if (myEventId == 0) {
						hasInvalidId = true;
						break;
					}
				} catch(NumberFormatException e) {
					hasInvalidId = true;
					break;
				}
			}
		    String[] flags = new String[deleteList1.size()];
		    String[] idArray = new String[deleteList1.size()];
		    for (int iX=0; iX<flags.length; iX++) {
		        flags[iX] = "0";
		    }
		    //KM-#5949
		    //Modified for INF-18183 ::: Akshi
		    String moduleName = calledFrom.equals("P")? LC.L_Cal_Lib : LC.L_Study_Calendar;
		    for (int iX=0; iX<deleteList1.size(); iX++) {
		    	myEventDefId = Integer.parseInt((String)deleteList1.get(iX));
			}
			Hashtable<String, ArrayList<String>> getEvtDefRowValues = AuditUtils.getRowValues("EVENT_DEF","EVENT_ID ="+myEventDefId,"esch");/*Fetches the RID/PK_VALUE*/
			ArrayList<String> rowEvtDefPK  = (ArrayList<String>)getEvtDefRowValues.get(AuditUtils.ROW_PK_KEY);
			ArrayList<String> rowEvtDefRID  = (ArrayList<String>)getEvtDefRowValues.get(AuditUtils.ROW_RID_KEY);
			int delResult = eventdefB.deleteEvtOrVisits((String[])deleteList1.toArray(idArray), 
		            tname, flags,EJBUtil.stringToNum(userId),AuditUtils.createArgs(session,"",moduleName));
			for(int i=0; i<rowEvtDefPK.size(); i++)
			 {
 				pkEvtDefVal = rowEvtDefPK.get(i);
 				ridEvtDefVal = rowEvtDefRID.get(i);
 				updateAudit_row = AuditUtils.updateAuditROw("esch", userFullName, ridEvtDefVal, "D");
			 }
		    if (delResult < 0) { throw new Exception(MC.M_EvntVisitCouldNotDelete); }
			if (hasInvalidId) { throw new Exception(MC.M_EvntVisitDisplacementInvalidId); }
		}
		
		System.out.println("VisitId-"+vId);
		//eventDefDao.saveVisitToEvent(protId,vId,myDisplacements);
		return;
	}
	JSONObject jsObj = new JSONObject();
	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
		out.println(jsObj.toString());
		return;
	}
	
	String oldESign = (String) tSession.getAttribute("eSign");
	String eSign = request.getParameter("eSign");
	if (!oldESign.equals(eSign)) {
		jsObj.put("result", -4);
		if ("userpxd".equals(Configuration.eSignConf)) {
			jsObj.put("resultMsg", MC.M_EtrWrgPassword_Svg/*"You entered a wrong e-Password. Please try saving again. "*****/);
		}
		else
		{
		jsObj.put("resultMsg", MC.M_EtrWrgEsign_Svg/*"You entered a wrong e-signature. Please try saving again. "*****/);
		}
		out.println(jsObj.toString());
		return;
	}
	
	

	String mode=request.getParameter("mode");
	String duration=request.getParameter("duration");
	String calstatus=request.getParameter("calstatus");
	String calassoc=request.getParameter("calassoc");
	String[] ops= request.getParameterValues("ops");
	String[] visitHide= request.getParameterValues("visitHide");//YK: Fix for Bug#7264
 	//String calledFrom = request.getParameter("calledfrom");
	//String protId = request.getParameter("calProtocolId");
	String tableName = request.getParameter("tableName");
	String test1 = request.getParameter("test1");

	/*
	StringBuffer sb = new StringBuffer();
	sb.append("\n");
	sb.append("eSign=").append(eSign).append("\n");
	sb.append("mode=").append(mode).append("\n");
	sb.append("duration=").append(duration).append("\n");
	sb.append("calstatus=").append(calstatus).append("\n");
	sb.append("calassoc=").append(calassoc).append("\n");
	sb.append("calledfrom=").append(calledFrom).append("\n");
	sb.append("calProtocolId=").append(protId).append("\n");
	sb.append("tableName=").append(tableName).append("\n");
	for(int iX=0; iX<ops.length; iX++) {
		sb.append("ops[").append(iX).append("]=").append(ops[iX]).append("\n");
   	}
	System.out.println(sb);
	*/
	
	if (ops == null || ops.length == 0) {
		jsObj.put("result", -2);
		jsObj.put("resultMsg", MC.M_No_OperPerformed);/*jsObj.put("resultMsg", "No operations were performed");*****/
		out.println(jsObj.toString());
		return;
	}
	
        StringBuffer evntVistWrongAssoc = new StringBuffer();
    try {
        ArrayList deleteList = new ArrayList();
        ArrayList<String> updatedListEId = new ArrayList<String>();
        ArrayList<String> updatedListVId = new ArrayList<String>();
        ArrayList<String> updatedListSeq = new ArrayList<String>();
        ArrayList<String> updatedListDisp = new ArrayList<String>();
        ArrayList<String> updatedListHide = new ArrayList<String>();
   		System.out.println("Calculating in JSP...");
		HashMap maxEvtSeqMap = new HashMap();
       	for (int iX=0; iX<ops.length; iX++) {
			String[] part = StringUtil.decodeString(ops[iX].replaceAll("(\\r|\\n|\\r\\n)+", "")).replaceAll("[MOVER]", "onmouseover").replaceAll("[MOUT]", "onmouseout").split(java.util.regex.Pattern.quote("||"));
			if (part == null || part.length < 4) { continue; }
			String vId = part[1].substring(part[1].indexOf("v")+1);
			String eId = part[1].substring(1,part[1].indexOf("v"));
			String eventName = part[3];
			String visitName = part[4];
			String catName=part[5];
			String evntVisitAssoc=eventName+" => "+visitName;
			int calcId = Integer.parseInt(protId);
			//YK: Fix for Bug#7264
			String visitId="";
			String hideFlag="";
			for (int iY=0; iY<visitHide.length; iY++) {
				String[] visitFlagHide = visitHide[iY].split(java.util.regex.Pattern.quote("||"));
				visitId= visitFlagHide[0];
				if(vId.equals(visitId))
				{
					hideFlag=visitFlagHide[1];
					break;
				}
			}
			String displacement = part[2];
			if ("A".equals(part[0])) { // Add operation
				int maxSeqVal = -1;
				if (maxEvtSeqMap.get(vId) == null) {
					maxSeqVal = eventdefB.getMaxSeqForVisitEvents(
				          Integer.parseInt(vId), 
				          part[3], tableName,catName);
					if (maxSeqVal < 0) {
						evntVistWrongAssoc.append(evntVisitAssoc+"<br>");
						continue;
					}
					maxEvtSeqMap.put(vId, ++maxSeqVal);
				} else {
					maxSeqVal = (Integer)maxEvtSeqMap.get(vId);
					maxEvtSeqMap.put(vId, ++maxSeqVal);
				}
				String seq = ""+maxSeqVal;
				
			    updatedListDisp.add(displacement);
				updatedListEId.add(String.valueOf(Integer.parseInt(eId)));
				updatedListVId.add(vId);
				updatedListSeq.add(seq);
				updatedListHide.add(hideFlag);

			} else if ("D".equals(part[0])) { // Delete operation
			    Integer myEvtId = new Integer(
		                eventdefB.fetchEventIdByVisitAndDisplacement(eId, tableName, vId, displacement));
		        if (myEvtId < 1) {
		        	myEvtId = new Integer(eventdefB.fetchEventIdByVisit(eId, tableName, vId));
		        }
			    deleteList.add(String.valueOf(myEvtId));
			}
		}
       	if (updatedListEId.size() > 0) {
       		System.out.println("Handing off to Stored Procedure...");
       		if ("S".equals(calledFrom)) {
       			EventAssocJB evtassB = new EventAssocJB();
       			String [] blankArray = {};
       			int ret = evtassB.createEventsAssoc(
       					updatedListEId.toArray(blankArray),
       					updatedListVId.toArray(blankArray),
       					updatedListSeq.toArray(blankArray),
       					updatedListDisp.toArray(blankArray),
       					updatedListHide.toArray(blankArray),
       					StringUtil.stringToNum(userId), ipAdd);
       			if (ret < 0) {
       				throw new Exception(MC.M_EvntVisitConstraintViolation);
       			}
       		} else if ("P".equals(calledFrom) || "L".equals(calledFrom)) {
       			EventdefJB evtdefB = new EventdefJB();
       			String [] blankArray = {};
       			int ret = evtdefB.createEventsDef(
       					updatedListEId.toArray(blankArray),
       					updatedListVId.toArray(blankArray),
       					updatedListSeq.toArray(blankArray),
       					updatedListDisp.toArray(blankArray),
       					updatedListHide.toArray(blankArray),
       					StringUtil.stringToNum(userId), ipAdd);
       			if (ret < 0) {
       				throw new Exception(MC.M_EvntVisitConstraintViolation);
       			}
       		}
       	}
		if (deleteList.size() > 0) {
			boolean hasInvalidId = false;
			for (int iX=0; iX<deleteList.size(); iX++) {
				if (deleteList.get(iX) == null) {
					hasInvalidId = true;
					break;
				}
				try {
					int myEventId = Integer.parseInt((String)deleteList.get(iX));
					if (myEventId == 0) {
						hasInvalidId = true;
						break;
					}
				} catch(NumberFormatException e) {
					hasInvalidId = true;
					break;
				}
			}
		    String[] flags = new String[deleteList.size()];
		    String[] idArray = new String[deleteList.size()];
		    for (int iX=0; iX<flags.length; iX++) {
		        flags[iX] = "0";
		    }
		    //KM-#5949
		    //Modified for INF-18183 ::: Akshi
		    String moduleName = calledFrom.equals("P")? LC.L_Cal_Lib : LC.L_Study_Calendar;
			for (int iX=0; iX<deleteList.size(); iX++) {
		    	myEventDefId = Integer.parseInt((String)deleteList.get(iX));
			}
		    Hashtable<String, ArrayList<String>> getEvtDefRowValues = AuditUtils.getRowValues("EVENT_DEF","EVENT_ID ="+myEventDefId,"esch");/*Fetches the RID/PK_VALUE*/
			ArrayList<String> rowEvtDefPK  = (ArrayList<String>)getEvtDefRowValues.get(AuditUtils.ROW_PK_KEY);
			ArrayList<String> rowEvtDefRID  = (ArrayList<String>)getEvtDefRowValues.get(AuditUtils.ROW_RID_KEY);
			int delResult = eventdefB.deleteEvtOrVisits((String[])deleteList.toArray(idArray), 
		            tableName, flags,EJBUtil.stringToNum(userId),AuditUtils.createArgs(session,"",moduleName));
			for(int i=0; i<rowEvtDefPK.size(); i++)
			 {
				pkEvtDefVal = rowEvtDefPK.get(i);
				ridEvtDefVal = rowEvtDefRID.get(i);
				updateAudit_row = AuditUtils.updateAuditROw("esch", userFullName, ridEvtDefVal, "D");
			 }
		    if (delResult < 0) { throw new Exception(MC.M_EvntVisitCouldNotDelete); }
			if (hasInvalidId) { throw new Exception(MC.M_EvntVisitDisplacementInvalidId); }
		}
	} catch(Exception e) {
		jsObj.put("result", -3);
		jsObj.put("resultMsg", e.getMessage());
		out.println(jsObj.toString());
		return;
	}
	
	int length = evntVistWrongAssoc.length();
	String finalMeSg = MC.M_Changes_SavedSucc;
	if(evntVistWrongAssoc!=null && length>0)
	{
		finalMeSg+="<br><br>"+MC.M_EvntVisitDupAssoc+"<br>"+evntVistWrongAssoc.toString();
	}
	
    jsObj.put("result", 0);
    jsObj.put("resultMsg", finalMeSg); /*jsObj.put("resultMsg", "Changes saved successfully");*****/
	out.println(jsObj.toString());
%>
