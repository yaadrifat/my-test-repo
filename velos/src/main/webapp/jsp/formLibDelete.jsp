<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Form_Delete%><%--Form Delete*****--%></title>


<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	<%-- if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   } --%>
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*,com.aithent.audittrail.reports.AuditUtils"%>
<jsp:useBean id="formLibJB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 

<% String src;
	src= request.getParameter("srcmenu");
	String formStatus = request.getParameter("formStatusInt");
	int formStatusInt = EJBUtil.stringToNum(request.getParameter("formStatusInt"));
	CodeDao cd = new CodeDao();
	String frmStat =cd.getCodeSubtype(formStatusInt);
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<BODY> 
<br>

<DIV class="formDefault" id="div1">
<% 
	
	String selectedTab="";
	String mode= "";
	String formId="";
	int ret=0;
	HttpSession tSession = request.getSession(true); 
 	if (sessionmaint.isValidSession(tSession))
	{ 		
		mode= request.getParameter("mode");
		if (mode==null) mode="initial";
		selectedTab= request.getParameter("selectedTab");
		formId = request.getParameter("formId");		
%>
		<FORM name="deleteForm" id="delFormFrmId" method="post" action="formLibDelete.jsp" onSubmit="if (validate(document.deleteForm)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
		<br><br>
		<%  if ( mode.equals("initial") && !frmStat.equals("W") )
			{ %>     
			<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	
		</table>
		
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="delFormFrmId"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
		
				<Input type="hidden" name="mode" value="final" >
				<Input type="hidden" name="formId" value="<%=formId%>" >
				<Input type="hidden" name="srcmenu" value="<%=src%>" >
				<Input type="hidden" name="selectedTab" value="<%=selectedTab%>" >
				<input type="hidden" name="formStatusInt" value="<%=formStatusInt%>">
		</FORM>
		<%  
			}
		else
		{
				String eSign = request.getParameter("eSign");	
				String oldESign = (String) tSession.getValue("eSign");
				if((frmStat.equals("W")) || ((!frmStat.equals("W") && !frmStat.equals("")) && (oldESign.equals(eSign)))){

					formId=  request.getParameter("formId");	
					formLibJB.setFormLibId(Integer.parseInt(formId));
					formLibJB.getFormLibDetails();				
					formLibJB.setRecordType("D");
					// Modified for INF-18183 ::: AGodara 
					ret = formLibJB.updateFormLib(AuditUtils.createArgs(tSession,"",LC.L_Form_Lib));
		
					if (ret==-2) 
					{%>	

						<br><br><br><br><br><br>
	
						<table width=100%>
						<tr>
						<td align=center><p class = "successfulmsg"><%=MC.M_ErrCnt_Del%><%--Error cannot be  be deleted.*****--%></p>
					<%} else 
					{ %>
						<br><br><br><br><br><br>
						<P class="successfulmsg" align="center"><%=MC.M_Data_DelSucc%><%--Data deleted successfully.*****--%> </P>
												
						<META HTTP-EQUIV=Refresh CONTENT="1; URL=formLibraryBrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>" >
					<%}%>
					
			 			</td>
						
						</table>				
						
				<%}else{%>
				<jsp:include page="incorrectesign.jsp" flush="true"/>
		<%}//end esign
		}  

	}
		
	else
	{ %> 
	 	<jsp:include page="timeout.html" flush="true"/> 
 <% } %>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</DIV>
<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/> 
</div>
</body>
</HTML>


