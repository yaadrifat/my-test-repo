<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_EditCalcu_Attributes%><%--Edit Calculation Attributes*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />

<%@ page import="com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.business.common.*,java.math.BigDecimal
"%>

</head>
   	<jsp:include page="include.jsp" flush="true"/> 
   	<jsp:include page="ui-include.jsp" flush="true"></jsp:include>

 <%  
%>


<SCRIPT Language="javascript">
	 

 function  validate(formobj){

	   if(isNaN(formobj.fringeBenefit.value) == true) {
		alert("<%=MC.M_EtrValid_NumValue%> ");/*alert("Please enter a valid numeric value ");*****/
		formobj.fringeBenefit.focus();
		return false;
		}
		
	if(isNaN(formobj.costDiscount.value) == true) {
		alert("<%=MC.M_EtrValid_NumValue%> ");/*alert("Please enter a valid numeric value ");*****/
		formobj.costDiscount.focus();
		return false;
		}
	
	if(isNaN(formobj.sponsorOHead.value) == true) {
		alert("<%=MC.M_EtrValid_NumValue%> ");/*alert("Please enter a valid numeric value ");*****/
		formobj.sponsorOHead.focus();
		return false;
		}
	
		
		
   }
   
  
</SCRIPT>

<body >

<jsp:useBean id="budgetB" scope="session" class="com.velos.esch.web.budget.BudgetJB"/>
<jsp:useBean id="budgetcalB" scope="request" class="com.velos.esch.web.budgetcal.BudgetcalJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
 
<br>
 
  <%

	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
		{	
		
		
	  
	String bgtcalId = request.getParameter("bgtcalId");		
		
	String bgtId=request.getParameter("budgetId");
	String repType=request.getParameter("repType");
	
	String oldSOCExludeFlag = "";
	oldSOCExludeFlag = request.getParameter("oldSOCExludeFlag");
	
	String allowSubmit =  request.getParameter("allowSubmit");
	
	if (StringUtil.isEmpty(allowSubmit))
	{
		allowSubmit="1";
	}
	
	String mode = request.getParameter("mode");
	
	if (StringUtil.isEmpty(mode))
	{
		mode="E";
	}
	
	if (mode.equals("E"))
	{
		String bgtStatCode = request.getParameter("bgtStatCode");
		String bgtStatDec ="";
		
		if (EJBUtil.isEmpty(bgtStatCode))
			bgtStatCode ="W";
		
		 if(bgtStatCode.equals("F")) {
			bgtStatDec = LC.L_Freeze/*"Freeze"*****/;
			} else if(bgtStatCode.equals("W")) {
			bgtStatDec = LC.L_Work_InProgress/*"Work in Progress"*****/;
			}else if(bgtStatCode.equals("T")) {
			bgtStatDec = LC.L_Template/*"Template"*****/;
			}
	 
	       
		
		
		%>

		<div class="popDefault " id="div2">	

	  <%  
	         
		int budgetId=EJBUtil.stringToNum(bgtId);	
		String budgetname=""; 	
		
		BudgetDao budgetDao = budgetB.getBudgetInfo(budgetId);
		ArrayList bgtStats = budgetDao.getBgtStats();
		ArrayList bgtSites = budgetDao.getBgtSites();
		
		ArrayList bgtCombFlags = budgetDao.getBgtCombBgtFlags();
		String bgtCombFlag = (String) bgtCombFlags.get(0);
		bgtCombFlag = (bgtCombFlag== null)?"": bgtCombFlag;
		
		if (bgtCombFlag.equals("") && bgtcalId.equals("0")){
			//Stand Alone Budget Combined View
			bgtCombFlag ="Y";
		}
		
		ArrayList bgtStudyNumbers = budgetDao.getBgtStudyNumbers();
		String bgtStudyNumber = (String) bgtStudyNumbers.get(0);
		String bgtSite = (String) bgtSites.get(0);
		
		BudgetcalDao budgetcalDao = new BudgetcalDao();
		
		String calName = "";
		ArrayList bgtcalNames = new ArrayList();
		
	if (! bgtCombFlag.equals("Y")){
		budgetcalDao = budgetcalB.getBgtcalDetail(EJBUtil.stringToNum(bgtcalId));
		bgtcalNames= budgetcalDao.getBgtcalProtNames();
		
		calName= (String) bgtcalNames.get(0);	
	}else{
		//bgtCombFlag.equals("Y") 
		//For Combined Budgets and Stand Alone budget Combined View
		budgetcalDao = budgetcalB.getAllBgtCals(budgetId);
		ArrayList bgtcalIds = new ArrayList();
		bgtcalIds = budgetcalDao.getBgtcalIds();
		bgtcalNames= budgetcalDao.getBgtcalProtNames();
		
		bgtcalId = bgtcalIds.get(0).toString();
		
		for(int indx=0; indx < bgtcalNames.size(); indx++){
			if (!EJBUtil.isEmpty(calName)){
				calName = calName + ", "+ (String) bgtcalNames.get(indx);
			}else calName = (String) bgtcalNames.get(indx);
		}
	}
	calName = (calName == null || calName.equals(""))?"-":calName;
	
	budgetcalB.setBudgetcalId(EJBUtil.stringToNum(bgtcalId));
	budgetcalB.getBudgetcalDetails();

	oldSOCExludeFlag = budgetcalB.getBudgetExcldSOCFlag();

	if (StringUtil.isEmpty(oldSOCExludeFlag))
	{
		oldSOCExludeFlag="0";
	}
	
		budgetB.setBudgetId(budgetId);
		budgetB.getBudgetDetails();
		budgetname=budgetB.getBudgetName();
		
		budgetname = (budgetname == null || budgetname.equals(""))?"-":budgetname;
		budgetname = StringUtil.stripScript(budgetname);
		bgtStudyNumber = (bgtStudyNumber == null || bgtStudyNumber.equals(""))?"-":bgtStudyNumber;
		
		bgtSite = (bgtSite == null || bgtSite.equals(""))?"-":bgtSite;
		 
		String lineitemInCostDisc = "0";
		String appIndirects = "0";
		String appFringe = "0";
		
		String lineitemInCostDiscAmount="0";
		String appIndirectsAmount = "0";
		String appFringeAmount = "0";
		String checkDiscount="";
		String checkMarkup="";
		String checkNoMarkupDisc="";
		String excludeSOCApply="0";
		
		appIndirects =	budgetcalB.getSpFlag();
		appIndirectsAmount = budgetcalB.getSpOverHead();
			
		if (StringUtil.isEmpty(appIndirectsAmount))
		{
			appIndirectsAmount = "0";
		}	
		
		appFringeAmount=	budgetcalB.getBudgetFrgBenefit();
		appFringe =	budgetcalB.getBudgetFrgFlag();
		
		if (StringUtil.isEmpty(appFringeAmount))
		{
			appFringeAmount = "0";
		}	
		
			
		lineitemInCostDiscAmount =	budgetcalB.getBudgetDiscount();
		lineitemInCostDisc=	budgetcalB.getBudgetDiscountFlag();
		
		if (StringUtil.isEmpty(lineitemInCostDiscAmount))
		{
			lineitemInCostDiscAmount = "0";
		}	
		
		
		if (StringUtil.isEmpty(lineitemInCostDisc))
		{
			lineitemInCostDisc="0";
		}
		
		if (StringUtil.isEmpty(appIndirects))
		{
			appIndirects="0";
		}
		
		if (StringUtil.isEmpty(appFringe))
		{
			appFringe="0";
		}	
		
		excludeSOCApply = budgetcalB.getBudgetExcldSOCFlag();
	 	
	 	
		if (StringUtil.isEmpty(excludeSOCApply))
		{
			excludeSOCApply="0";
		}	
	 	
	%>
	  

	<Form name="budget" method="post" id="budget" action="editBudgetCalAttributes.jsp" onSubmit="if (validate(document.budget)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
		
	<p class="sectionHeadings"><%=LC.L_EditCalcu_Attributes%><%--Edit Calculation Attributes*****--%></p>	
		
	<table width=100%>
	<tr>
	<td class=tdDefault width = 20% ><B><%=LC.L_Budget_Name%><%--Budget Name*****--%>: </B>  <%=budgetname%></td>
	</tr>

	<tr>
	<td class=tdDefault width = 20% ><B><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>: </B>   <%=bgtStudyNumber%></td>
	</tr>
	<tr>
	<td class=tdDefault width = 20% ><B><%=LC.L_Organizations%><%--Organization*****--%>: </B> <%=bgtSite%></td>
	</tr>
	<tr>
		<td class=tdDefault width = 20% ><B>
		<%if(calName.equals("None")){%>
			<%=MC.M_NoCalSelected%><%--No Calendar Selected*****--%></B>
		<%}else{%>
			<%=LC.L_Calendar%><%--Calendar*****--%><%if (bgtCombFlag.equals("Y")){ %>(<%=LC.L_S_Lower%><%--s*****--%>)<%}%>: </B> <%=calName%>
		<%}%>
		</td>	
	</tr>
	</table>
	<br>
	<%
					  
		if ((!(bgtStatCode ==null)) && (bgtStatCode.equals("F") || bgtStatCode.equals("T")) ) 
			{ Object[] arguments = {bgtStatDec};
			%>
				 <table width="600" cellspacing="2" cellpadding="2">
				 	<tr>
						<td>
						    	<P class = "defComments"><FONT class="Mandatory"><%=VelosResourceBundle.getMessageString("M_BgtStat_CntChgBgt",arguments)%><%--Budget Status is '<%=bgtStatDec%>'. You cannot make any changes to the budget.*****--%></Font></P>
						</td>
					</tr>
				</table>
			<%
			}

	%>

	<table width="100%">
			<tr>
			<td class=tdDefault width=20%><B> <%=LC.L_Fringe_Benefit%><%--Fringe Benefit*****--%> </B></td>
			<td class=tdDefault width=20%><B> <input type=text	name="fringeBenefit" class="rightAlign" value="<%=appFringeAmount%>"
					maxlength=5 size=3>&nbsp;%&nbsp;
	</td>
	 	<td class=tdDefault width=50%>
	 <%
		if(appFringe.equals("1")) {
	%> <input type="checkbox" name="fringeBenefitApply" checked>&nbsp;&nbsp;
			<%
		} else {
	%> <input type=checkbox name="fringeBenefitApply">&nbsp;&nbsp; <%
		}
	 if(repType.equalsIgnoreCase("cov_type")){
     SchCodeDao sch=new SchCodeDao();
     int salcalc= sch.getCodeId("category", "ctgry_sclass");
     String salcalc_desc= sch.getCodeDescription(salcalc);
     int salfac= sch.getCodeId("category", "ctgry_sfaculty");
     String salfac_desc= sch.getCodeDescription(salfac);
     Object[] arguments1 = {salcalc_desc,salfac_desc};
	%> <%=VelosResourceBundle.getMessageString("M_Apply_SalCalcSalFac",arguments1)%><%--Apply to all Salary-Classified and Salary-Faculty*****--%> 
	<%}else{ %>
	    <%=MC.M_Apply_PersonnelCosts%><%--Apply to all Personnel Costs*****--%>
	<%} %>
	</B>
		</td>
	 </tr>
		<tr>
			<td class=tdDefault%><B> <%=LC.L_CostDiscOrMarkup%><%--Cost Discount/Markup*****--%> </B></td>
			<td class=tdDefault%>
				<B>
					<input type=text name="costDiscount" class="rightAlign" value="<%=lineitemInCostDiscAmount%>" maxlength=5 size=3>&nbsp;%&nbsp;
	</td>
	<td class=tdDefault width=50%>
	<%
		if(lineitemInCostDisc.equals("1")) {
			
			checkDiscount="CHECKED";
		}
		else if (lineitemInCostDisc.equals("2")) {
			checkMarkup="CHECKED";
		}	
		else
		{
			checkNoMarkupDisc	="CHECKED";
		}
		
	%>
			<input type="radio" name="costDiscountApply" value="1" <%=checkDiscount%>  >&nbsp;&nbsp;<%=MC.M_Discount_ToSeltems%><%--Apply Discount to selected line items*****--%>
			<br><input type="radio" name="costDiscountApply" value="2" <%=checkMarkup%>  >&nbsp;&nbsp;<%=MC.M_ApplyMarkup_ToSelItem%><%--Apply Markup to selected line items*****--%>
			<br><input type="radio" name="costDiscountApply" value="0" <%=checkNoMarkupDisc%>  >&nbsp;&nbsp;<%=MC.M_NoDiscountTo_SelItems%><%--Do not Apply Discount/Markup to selected line items*****--%>
			</B>
		</td>
			 
		</tr>

		<tr>
			<td class=tdDefault><B> <%=LC.L_Indirects%><%--Indirects*****--%> </B></td>
			<td class=tdDefault><B> <input type=text name=sponsorOHead	class="rightAlign" value="<%=appIndirectsAmount%>" maxlength=5 size=3>&nbsp;%&nbsp;
		</td>
			<td class=tdDefault width=50%>
			<%
		if(appIndirects.equals("N")) {
	%> <input type=checkbox name=sponsorOHeadApply>&nbsp;&nbsp; <%
		} else {
	%> <input type=checkbox name=sponsorOHeadApply checked>&nbsp;&nbsp; <%
		}
	%> <%=MC.M_ApplySelc_LineItems%><%--Apply to selected line items*****--%> </B></td>
			 
		</tr>

<tr>
		<td class=tdDefault colspan=2><B> <%=MC.M_ExcludeLine_FromTot%><%--Exclude SOC line items from Totals*****--%> </B>

		<%
	if(excludeSOCApply.equals("0")) {
%> <input type="checkbox" value="1" name="excludeSOCApply"> <%
	} else {
%> <input type="checkbox" value="1" name="excludeSOCApply" checked> <%
	}%></td>
		

	</tr>

	 

	</table>


	<input type="hidden" name="bgtcalId" MAXLENGTH = 15 value="<%=bgtcalId%>">
	<input type="hidden" name="budgetId" MAXLENGTH = 15 value="<%=budgetId%>">
	<input type="hidden" name="bgtCombFlag" MAXLENGTH = 15 value="<%=bgtCombFlag%>">
	<input type="hidden" name="oldSOCExludeFlag" MAXLENGTH = 15 value="<%=oldSOCExludeFlag%>">

	<input type="hidden" name="bgtStatCode" value=<%=bgtStatCode%>>
	<input type="hidden" name="mode" value="F">
		


	<%
	if (!(bgtStatCode.equals("F") || bgtStatCode.equals("T") )) 
		{
			if (allowSubmit.equals("1"))
			{
	%>
	<BR>
		<BR>
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="N"/>
			<jsp:param name="formID" value="budget"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
	<%
			} //allowSubmut==1
	  }
	%>
		</Form>

		<%

}// mode
	else if (mode.equals("F"))
	{
		//save
		
		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");
		
		int budgetId=EJBUtil.stringToNum(bgtId);
		String bgtCombFlag = request.getParameter("bgtCombFlag");	
		
		int output = 0;
		
		String sponsorOHead = request.getParameter("sponsorOHead");
 		
 		if (StringUtil.isEmpty(sponsorOHead))
		{
			sponsorOHead="0";
		}
		
 		String sponsorOHeadApply = request.getParameter("sponsorOHeadApply");

		String excludeSOCApply = request.getParameter("excludeSOCApply");

		if(excludeSOCApply == null)
		excludeSOCApply = "0";
		if(excludeSOCApply.equals("on") || excludeSOCApply.equals("1"))
		excludeSOCApply = "1";
		else
		excludeSOCApply = "0";

		sponsorOHeadApply = (sponsorOHeadApply == null)?"N":"Y";

	 
		String fringeBenefit = request.getParameter("fringeBenefit");

		if (StringUtil.isEmpty(fringeBenefit))
		{
			fringeBenefit="0";
		}
		String fringeBenefitApply = request.getParameter("fringeBenefitApply");

		fringeBenefitApply = (fringeBenefitApply == null)?"0":"1";


		String costDiscount = request.getParameter("costDiscount");
		
		if (StringUtil.isEmpty(costDiscount))
		{
			costDiscount="0";
		}


		String costDiscountApply = request.getParameter("costDiscountApply");

		costDiscountApply = (costDiscountApply == null)?"0":costDiscountApply;
		
		budgetcalB.setBudgetcalId(EJBUtil.stringToNum(bgtcalId));
		budgetcalB.getBudgetcalDetails();

		budgetcalB.setSpOverHead(sponsorOHead); 
		budgetcalB.setSpFlag(sponsorOHeadApply);			 
		budgetcalB.setBudgetFrgBenefit(fringeBenefit);
		budgetcalB.setBudgetFrgFlag(fringeBenefitApply);
		budgetcalB.setBudgetDiscount(costDiscount);
		budgetcalB.setBudgetDiscountFlag(costDiscountApply);			
		budgetcalB.setBudgetExcldSOCFlag(excludeSOCApply);
		
		budgetcalB.setModifiedBy(usr);
		budgetcalB.setIpAdd(ipAdd);
		
		if (bgtCombFlag.equals("Y")){
			BudgetcalDao budgetcalDao = new BudgetcalDao();
			output = budgetcalDao.updateAllBgtCalendars(budgetId,budgetcalB); 
			
			if (! oldSOCExludeFlag.equals(excludeSOCApply))
			{
				System.out.println(EJBUtil.stringToNum(excludeSOCApply));
				
				budgetcalDao.updateOtherCostToExSOCAllCals(budgetId, EJBUtil.stringToNum(excludeSOCApply),usr,ipAdd);
			}
		
		}else{
		
			output = budgetcalB.updateBudgetcal();
			
			if (! oldSOCExludeFlag.equals(excludeSOCApply))
			{
				System.out.println(EJBUtil.stringToNum(excludeSOCApply));
				
				budgetcalB.updateOtherCostToExcludeSOC(bgtcalId, EJBUtil.stringToNum(excludeSOCApply),usr,ipAdd);
			}
			
		}
			
		
		if(output == -2) {
%>
<br><br><br><br><br><p class="sectionHeadings" align=center><%=MC.M_DataCnt_Svd%><%--Data could not be saved*****--%></p>

<%
		} else {
%>
<br><br><br><br><br><p class="sectionHeadings" align=center><%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%></p>
	<script>
		 
		window.opener.location.reload();
		setTimeout("self.close()",1000);
		
	</script>
<%
		}
		
		////////////////////////
	}	
}//end of if body for session

else

{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}

%>
  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div> 
	
</body>
</html>
