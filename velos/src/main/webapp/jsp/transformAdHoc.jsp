<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,javax.xml.transform.TransformerFactory,javax.xml.transform.Transformer,javax.xml.transform.Source,javax.xml.transform.stream.StreamSource,javax.xml.transform.stream.StreamResult,javax.xml.transform.TransformerException,javax.xml.transform.TransformerConfigurationException,java.io.*,java.text.*,java.net.URL,com.velos.eres.web.user.UserJB"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%
	   String xmlString = "";
	   String xslFileName = "";
	   String xslSrc ="";
	   String filePath = "";
	   TimeZone tz = TimeZone.getDefault();
	   HttpSession tSession = request.getSession(true);
	   
	   
	if (sessionmaint.isValidSession(tSession))
	{ 
	   String uName =(String) tSession.getValue("userName");
	   String repDate="";
	   	   
	   String calledfrom = request.getParameter("calledfrom");
	   if(EJBUtil.isEmpty(calledfrom)){
		   calledfrom = LC.L_Rpt_Central;
		}
	   int repId = Integer.parseInt( (request.getParameter("repId")==null) ? "0" : request.getParameter("repId"));
	   
		repDate = DateUtil.getCurrentDateTime();
	     
	   	
	   //for downloads
	   	Configuration.readAppendixParam(Configuration.ERES_HOME + "eresearch.xml");
	 	com.aithent.file.uploadDownload.Configuration.readSettings("eres");
	 	com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "study");		  
		String fileDnPath = com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET ;
	
		Configuration.readReportParam(Configuration.ERES_HOME + "eresearch.xml");
		filePath = Configuration.REPDWNLDPATH;
		Calendar now = Calendar.getInstance();
		//make the file name
		String fileName="reporthtml"+"["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "].html" ;
		//make the complete file name
		String htmlFile = filePath + "/"+fileName;
		
	   
	   ///////
	   
		
		xmlString = request.getParameter("xmlString"); //get xml string
		xmlString=StringUtil.decodeString(xmlString);
		xmlString=xmlString.replaceAll("&", "&amp;");
		xslFileName = request.getParameter("xslFileName"); //get xsl file name
		xslSrc = request.getParameter("xslSrc"); //get xsl source type
		
		String repHeader = "";
		String repFooter = "";
		
		repHeader = request.getParameter("repHeader"); //get repHeader
		repFooter = request.getParameter("repFooter"); //get repFooter
		
		TransformerFactory tFactory = TransformerFactory.newInstance();
		
		Reader mR=new StringReader(xmlString); 
		Reader mR2=new StringReader(xmlString); 
		//Reader sR=new StringReader(xsl);
		
		
		 
		Source xmlSource=new StreamSource(mR);
		Source xmlSource2=new StreamSource(mR2);
		
		if (xslSrc.equals("file"))
		{	
			System.out.println("fil:" + xslFileName);
			File fo1=new File(xslFileName);
			Source xslSource = new StreamSource(fo1);
			// Generate the transformer.
			Transformer transformer = tFactory.newTransformer(xslSource);
			
			FileOutputStream fhtml=new FileOutputStream(htmlFile);
			
			transformer.setParameter("tz", tz.getDisplayName());
			transformer.setParameter("repBy",uName);
			transformer.setParameter("repDate",repDate);
				transformer.setParameter("repTitle",repHeader);
				transformer.setParameter("repFooter",repFooter);
			
			transformer.setOutputProperty("encoding", "UTF-8");
			
	  		transformer.transform(xmlSource, new StreamResult(fhtml));
	  		fhtml.close();
	  		
  			/*Changes for: INF-22402 Date:10/04/2012 By:Yogendra Pratap Singh*/
  			String wordLink = "repGetWord.jsp?htmlFile=" + htmlFile +"&fileDnPath="+fileDnPath+"&filePath="+filePath+"&repId="+repId+"&calledfrom="+calledfrom;
			String excelLink = "repGetExcel.jsp?htmlFile=" + htmlFile +"&fileDnPath="+fileDnPath+"&filePath="+filePath+"&repId="+repId+"&calledfrom="+calledfrom;
			String printLink = "repGetHtml.jsp?htmlFileName=" + fileName +"&fileDnPath="+fileDnPath+"&filePath="+filePath+"&repId="+repId+"&calledfrom="+calledfrom;
			
			%>
			
	 
			
			<table width="100%"><tr class="reportGreyRow"><td  class="reportPanel"><%=MC.M_Download_ReportIn%><%--Download the report in*****--%>: <A target="_new" href="<%=wordLink%>"><img border="0" title="<%=LC.L_Word_Format%>" alt="<%=LC.L_Word_Format%>" src="./images/word.GIF" /><%//=LC.L_Word_Format%><%--Word Format*****--%></A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<A target="_new" href="<%=excelLink%>"><img border="0" title="<%=LC.L_Excel_Format%>" alt="<%=LC.L_Excel_Format%>" src="./images/excel.GIF" /><%//=LC.L_Excel_Format%><%--Excel Format*****--%></A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<A target="_new" href="<%=printLink%>"><img border="0" title="<%=LC.L_Printer_FriendlyFormat%>" alt="<%=LC.L_Printer_FriendlyFormat%>" src="./images/printer.gif"/></A>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr></table>
			
			<%
			
			//generate output on screen
			File fo2=new File(xslFileName);
			Source xslSource2 = new StreamSource(fo2);
			Transformer transformer2 = tFactory.newTransformer(xslSource2);
			
			transformer2.setParameter("tz", tz.getDisplayName());
			transformer2.setParameter("repBy",uName);
			transformer2.setParameter("repDate",repDate);
				transformer2.setParameter("repTitle",repHeader);
				transformer2.setParameter("repFooter",repFooter);
				transformer2.setOutputProperty("encoding", "UTF-8");
				System.out.println("encoding set");
				
			transformer2.transform(xmlSource2, new StreamResult(out));
	  	
		}
		
		/*Changes for: INF-22402 Date:10/04/2012 By:Yogendra Pratap Singh*/
		if(EJBUtil.isEmpty(calledfrom)){
			calledfrom = LC.L_Rpt_Central;
		}
		int downloadFlag=1;
		String downloadFormat = LC.L_Linear_Format;
		%>
		<jsp:include page="bottompanel.jsp" flush="true"/>
		<jsp:include page="userReportLogging.jsp" flush="true">
			<jsp:param 	value="<%=repId %>" 		name="repId"/>
			<jsp:param 	value="<%=fileName %>" 		name="fileName"/>
			<jsp:param 	value="<%=filePath %>"		name="filePath"/>
			<jsp:param 	value="<%=calledfrom%>"		name="moduleName"/>
			<jsp:param 	value="<%=xmlString %>" 	name="repXml"/>
			<jsp:param 	value="<%=downloadFlag%>"	name="downloadFlag"/>
			<jsp:param value="<%=downloadFormat%>"  name="downloadFormat"/>
		</jsp:include>
		
	<%} //valid session		
			
	%>