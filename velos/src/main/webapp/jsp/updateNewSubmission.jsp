<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML> 

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
	<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:include page="include.jsp" flush="true"/>
	<jsp:useBean id="submB" scope="request" class="com.velos.eres.web.submission.SubmissionJB"/>
	<jsp:useBean id="submBoardB" scope="request" class="com.velos.eres.web.submission.SubmissionBoardJB"/>
	<jsp:useBean id="submStatusB" scope="request" class="com.velos.eres.web.submission.SubmissionStatusJB"/>

	<%@ page language="java" import="com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*,com.velos.eres.web.account.AccountJB,com.velos.esch.business.common.*,com.velos.eres.compliance.web.*,org.json.*,org.apache.commons.lang.StringEscapeUtils"%>
	<%@ page import="com.velos.eres.web.study.StudyJB,com.velos.eres.web.studyId.StudyIdJB" %>
	<%@ page import="com.velos.eres.widget.business.common.UIFlxPageDao,com.velos.eres.widget.business.common.ResubmDraftDao,com.velos.eres.widget.service.util.FlxPageArchive" %>
	<%@page import="com.velos.eres.web.intercept.InterceptorJB"%>
	<%@page import="com.velos.eres.web.studyVer.StudyVerJB,com.velos.eres.business.common.StudyVerDao" %>
	<%@page import="com.velos.eres.web.statusHistory.StatusHistoryJB" %>
	<%@page import="com.velos.eres.web.studyStatus.StudyStatusJB" %>
	<%@page import="com.velos.eres.compliance.web.ComplianceJB" %>

	<%!
	private static final String DEFAULT_DUMMY_CLASS = "[Config_Submission_Interceptor_Class]";
	private static final String VERSION_STATUS = "versionStatus";
	private static final String LETTER_F = "F";
	private static final String ER_STUDYVER_TABLE = "er_studyver";
	%>

	<%
	 EIRBDao eirbDao = new EIRBDao();
  	HttpSession tSession = request.getSession(true);
  	String accId = (String) tSession.getValue("accountId");
  	int iaccId = EJBUtil.stringToNum(accId);
  	String studyId = (String) tSession.getValue("studyId");
  	String grpId = (String) tSession.getValue("defUserGroup");
  	String ipAdd = (String) tSession.getValue("ipAdd");
  	String usr = (String) tSession.getValue("userId");
  	int iusr = EJBUtil.stringToNum(usr);
  	int newSubmStatusId = 0;
  	String fieldDetails = StringEscapeUtils.unescapeHtml(StringUtil.htmlEncodeXss(request.getParameter("fieldDetails")));

	String attachFieldDetails = StringEscapeUtils.unescapeHtml(StringUtil.htmlEncodeXss(request.getParameter("attachFieldDetails")));
	
	String formRespDetails = StringEscapeUtils.unescapeHtml(StringUtil.htmlEncodeXss(request.getParameter("formRespDetails")));
	if(attachFieldDetails == null) {
	 attachFieldDetails="[]";
	}
	if(formRespDetails == null) {
		formRespDetails="[]";
	}

	fieldDetails= StringUtil.decodeString(fieldDetails);
	attachFieldDetails=StringUtil.decodeString(attachFieldDetails);
	formRespDetails= StringUtil.decodeString(formRespDetails);
	
	String comments = StringEscapeUtils.unescapeHtml(StringUtil.htmlEncodeXss(request.getParameter("comments")));

	String src = StringUtil.htmlEncodeXss(request.getParameter("srcmenu"));
	String selectedTab = StringUtil.htmlEncodeXss(request.getParameter("selectedTab"));
	String eSign = request.getParameter("eSign");
	if (sessionmaint.isValidSession(tSession)) {
	%>
	<jsp:include page="sessionlogging.jsp" flush="true"/>
	<%
	    String oldESign = (String) tSession.getValue("eSign");
		if (!oldESign.equals(eSign)) {
	%>
	 		<jsp:include page="incorrectesign.jsp" flush="true"/>
	<%
	} else {
	    UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
	    int version = uiFlxPageDao.getHighestFlexPageVersion("er_study", Integer.valueOf(studyId));

	    HashMap<String, Object> paramMap = new HashMap<String, Object>();
	    int ret = 0;
	    if (version > 0) {
			//Save re-submission draft first
		    paramMap.put("request", request);
		    ResubmDraftDao resubmDraftDao = new ResubmDraftDao();
		    ret = resubmDraftDao.saveResubmissionDraft(paramMap);
	    } else { ret = 1; }
	    if( ret < 0 ) {
%>
  <br><br><br><br>
  <p class = "successfulmsg" align = center><%= MC.M_Err_SavingData/*"There was an error while saving data"*****/%></p>
  <%if("LIND".equals(CFG.EIRB_MODE)){ %>
  <META HTTP-EQUIV=Refresh CONTENT="3; URL=studyCheckNSubmit?mode=M&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">
  <%}else{ %>
  <META HTTP-EQUIV=Refresh CONTENT="3; URL=irbnewcheck.jsp?mode=M&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">
  <%} %>
</BODY>
</HTML>
<%          
            return; // An error occurred; let's get out of here
        }

		// Create a new submission status bean for submission
		paramMap.put("userId", (usr.equals(null) ? null : Integer.valueOf(usr)));
		paramMap.put("accountId", (accId.equals(null) ? null : Integer.valueOf(accId)));
		paramMap.put("studyId", (studyId.equals(null) ? null : Integer.valueOf(studyId)));

		// Implement Flex Study part here
		StudyJB studyJB = new StudyJB();
		studyJB.setId(StringUtil.stringToNum(studyId));
		studyJB.getStudyDetails();
		String defUserGroup = (String) tSession.getAttribute("defUserGroup");
		StudyIdJB studyIdJB = new StudyIdJB();
		StudyIdDao sidDao = new StudyIdDao();
		sidDao = studyIdJB.getStudyIds(
				StringUtil.stringToNum(studyId), defUserGroup);

		FlxPageArchive archive = new FlxPageArchive();
		String fullVersion = archive.createArchive(studyJB, sidDao, paramMap);
	    double versionNum = Double.valueOf(fullVersion).doubleValue();
	    // Freeze all categories of this version of this study
	    StudyVerJB studyVerJB = new StudyVerJB();
	    StudyVerDao studyVerDao = studyVerJB.getAllVers(StringUtil.stringToNum(studyId));
	    ArrayList studyVerIds = studyVerDao.getStudyVerIds();
	    ArrayList studyVerNumbers = studyVerDao.getStudyVerNumbers();
	    // Get a codelst item for Freeze status
	    CodeDao statDao = new CodeDao();
	    int freezeStat = statDao.getCodeId(VERSION_STATUS, LETTER_F);
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(Configuration.getAppDateFormat()+" HH:mm:ss");
	    for (int iX = 0; iX < studyVerNumbers.size(); iX++) {
	    	String svNumStr = (String)studyVerNumbers.get(iX);
	    	double svNum = Double.valueOf(svNumStr).doubleValue();
	    	double vNum = Double.valueOf(versionNum).doubleValue();
	    	if( versionNum > 0 ) {
    			if( svNum == vNum) {
		    		// add a Freeze row to ER_STATUS_HISTORY
		    		StatusHistoryJB statusHistoryJB = new StatusHistoryJB();
		    		statusHistoryJB.setStatusModuleTable(ER_STUDYVER_TABLE);
		    		int modulePk = (Integer)studyVerIds.get(iX);
		    		statusHistoryJB.setStatusModuleId(String.valueOf(modulePk));
		    		statusHistoryJB.setStatusCodelstId(String.valueOf(freezeStat));
		    		statusHistoryJB.setStatusStartDate(sdf.format(cal.getTime()));
			    	statusHistoryJB.setStatusEnteredBy(usr);
			    	statusHistoryJB.setCreator(usr);
			   		statusHistoryJB.setStatusHistoryDetails();
			   		// add version status "F" in er_studyver table
			   		studyVerDao.updateVersionStatus(svNumStr, Integer.parseInt(studyId), modulePk, "F",Integer.parseInt(usr));			   		
	    		}
		   	}
		}

    	/* Add form locking code here. */
    	
    	
	    //Locking the Study as Version is frozen
	    StudyDao studyDao = new StudyDao();
	    studyDao.lockUnlockStudy(EJBUtil.stringToNum(studyId), 1);
		String resultMsg = null;
		if (!"0".equals(fullVersion)) {
			resultMsg = MC.M_Data_SvdSucc/*"Data was saved successfully"*****/;
		} else {
			resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
		}
	    // Create a new submission bean
        submB.setStudy(studyId);
	    submB.setIpAdd(ipAdd);
	    submB.setCreator(usr);
	    submB.setLastModifiedBy(usr);
	    int newSubmId = submB.createSubmission();

        if( newSubmId > 0 ) {
            resultMsg = MC.M_Data_SvdSucc/*"Data was saved successfully"*****/;
        } else {
            resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
%>
  <br><br><br><br>
  <p class = "successfulmsg" align = center><%=resultMsg%></p>
  <%if("LIND".equals(CFG.EIRB_MODE)){ %>
  <META HTTP-EQUIV=Refresh CONTENT="3; URL=studyCheckNSubmit?mode=M&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">
  <%}else{ %>
  <META HTTP-EQUIV=Refresh CONTENT="3; URL=irbnewcheck.jsp?mode=M&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">
  <%} %>
</BODY>
</HTML>
<%          
            return; // An error occurred; let's get out of here
        }

        // Create a new submission status bean for submission
        EIRBDao eIrbDao = new EIRBDao();
        eIrbDao.getCurrentOverallStatus(EJBUtil.stringToNum(studyId), newSubmId);
        String currentOverallStatus = eIrbDao.getCurrentOverallStatus();
        // If the overall status does not already exist for this study, create a new one
        if (currentOverallStatus == null || currentOverallStatus.length() == 0) {
            submStatusB.setFkSubmission(String.valueOf(newSubmId));
            submStatusB.setFkSubmissionBoard(null); // For overall status, this is null
            //submStatusB.setSubmissionEnteredBy(usr);
            submStatusB.setCreator(usr);
            submStatusB.setLastModifiedBy(usr);
            submStatusB.setIpAdd(ipAdd);
            submStatusB.setSubmissionStatusDate(null);
            submStatusB.setSubmissionAssignedTo(null);
            submStatusB.setSubmissionCompletedBy(null);
            submStatusB.setSubmissionNotes(null);
         //   submStatusB.setSubmissionStatusBySubtype("submitted");

            newSubmStatusId = submStatusB.createSubmissionStatus();
            if( newSubmStatusId < 1 ) {
                resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
%>
  <br><br><br><br>
  <p class = "successfulmsg" align = center><%=resultMsg%></p>
  <%if("LIND".equals(CFG.EIRB_MODE)){ %>
  <META HTTP-EQUIV=Refresh CONTENT="3; URL=studyCheckNSubmit?mode=M&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">
  <%}else{ %>
  <META HTTP-EQUIV=Refresh CONTENT="3; URL=irbnewcheck.jsp?mode=M&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">
  <%} %>
</BODY>
</HTML>
<%          
                return; // An error occurred; let's get out of here
            }
        }
        
	    // Figure out which board was selected
        eIrbDao.getReviewBoards(EJBUtil.stringToNum(accId),EJBUtil.stringToNum(grpId));
        ArrayList boardNameList = eIrbDao.getBoardNameList();
        ArrayList boardIdList = eIrbDao.getBoardIdList();
        ArrayList submitToList = new ArrayList<String>();
        int pkSubmission = submB.getNewAmendSubmissionByFkStudy(studyId);
        if(pkSubmission > 0){
        	submitToList.addAll(eirbDao.getSubmitToBoardList(pkSubmission));
        }
        boolean noBoardIsSelected = true;
	    for (int iBoard=0; iBoard<boardIdList.size(); iBoard++) {
	    	if (!"on".equals(request.getParameter("submitTo"+iBoard))) { 
	    		if("LIND".equals(CFG.EIRB_MODE) && submitToList.size()>0){
	    			if(!submitToList.contains(boardIdList.get(iBoard))){
	    				continue;
	    			}
	    		}else{
	        		continue;
	    		}
	    	}
	    	noBoardIsSelected = false;
	    }
	    for (int iBoard=0; iBoard<boardIdList.size(); iBoard++) {
	    	if ("LIND".equals(CFG.EIRB_MODE) && noBoardIsSelected) {
	    		if (iBoard > 0) {
	    			break;
	    		}
	    	} else if (!"on".equals(request.getParameter("submitTo"+iBoard))) {
	    		if("LIND".equals(CFG.EIRB_MODE) && submitToList.size()>0){
	    			if(!submitToList.contains(boardIdList.get(iBoard))){
	    				continue;
	    			}
	    		}else{
	        		continue;
	    		}
	        }
			// Create a new submission_board bean for each board selected
		    submBoardB.setFkSubmission(String.valueOf(newSubmId));
		    submBoardB.setFkReviewBoard((String) boardIdList.get(iBoard));
		    submBoardB.setCreator(usr);
		    submBoardB.setLastModifiedBy(usr);
		    submBoardB.setIpAdd(ipAdd);
		    submBoardB.setFkReviewMeeting(null);
		    submBoardB.setSubmissionReviewer(null);
		    submBoardB.setSubmissionReviewType(null);
		    
		    Hashtable ht = submBoardB.createSubmissionBoard();
		    
		    int newSubmBoardId = ((Integer)ht.get("id")).intValue(); 
		    if (newSubmBoardId < 1) {
		        resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
		        break;
		    }
		    
		    // Create a new submission status bean for this submission_board
		    if (request.getParameter("from")!=null && !request.getParameter("from").equals("resub_draft")){
            submStatusB.setFkSubmission(String.valueOf(newSubmId));
            submStatusB.setFkSubmissionBoard(String.valueOf(newSubmBoardId));
            submStatusB.setSubmissionEnteredBy(usr);
            submStatusB.setCreator(usr);
            submStatusB.setLastModifiedBy(usr);
            submStatusB.setIpAdd(ipAdd);
            submStatusB.setSubmissionStatusDate(null);
            submStatusB.setSubmissionAssignedTo(null);
            submStatusB.setSubmissionCompletedBy(null);
            submStatusB.setSubmissionNotes(null);
		    if (ht.containsKey("previouslySubmitted")) {
		        submStatusB.setSubmissionStatusBySubtype("resubmitted");
		    } else {
		        submStatusB.setSubmissionStatusBySubtype("submitted");
		    }
            
            int newSubmStatusIdForBoard = submStatusB.createSubmissionStatus();
            if(newSubmStatusIdForBoard>1){
            	eirbDao.updateIscurrentForFinalOutcomeTab(newSubmId,newSubmBoardId);
                }
            if( newSubmStatusIdForBoard < 1 ) {
                resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
                break;
            }
            }else{
		    	System.out.println("Inside else");
		    	int submissionPK = submB.getNewAmendSubmissionByFkStudy(studyId);
		    	CodeDao codePiRespReq = new CodeDao();
		    	String oldCurrentStatus = submStatusB.getPreviousCurrentSubmissionStatus(submissionPK,newSubmBoardId); 
	    		int currentStatusPK = submStatusB.getCurrentSubmissionStatusPK(submissionPK, newSubmBoardId);
	    		submStatusB.getSubmissionStatusDetails(currentStatusPK);
	    		System.out.println("submissionPK---->"+submissionPK);
	    		System.out.println("newSubmBoardId---->"+newSubmBoardId);
	    		System.out.println("currentStatusPK---->"+currentStatusPK);
	    		int currentStatus=0;
	    		if(currentStatusPK > 0){
	    			currentStatus=submStatusB.getFkSubmissionStatus();
	    		}
		    	if(codePiRespReq.getCodeId("subm_status","pi_resp_req")==currentStatus){
		    		
		    		String statusDate = DateUtil.dateToString(Calendar.getInstance().getTime());
		    	    
		    		String piRespondedStatus = "";
		    		 
		    		    
		    	    CodeDao codeDao = new CodeDao();
		         	piRespondedStatus =  String.valueOf(codeDao.getCodeId("subm_status", "pi_respond"));
		         	
		    	    
		    		// set to subm status bean and create new status for PI responded
		            submStatusB.setFkSubmission(String.valueOf(newSubmId));
            		submStatusB.setFkSubmissionBoard(String.valueOf(newSubmBoardId));
		            submStatusB.setFkSubmissionStatus(piRespondedStatus);
		            submStatusB.setSubmissionEnteredBy(usr);
		            submStatusB.setSubmissionStatusDate(statusDate);
		            submStatusB.setSubmissionNotes(comments);
		            submStatusB.setCreator(usr);
		            submStatusB.setLastModifiedBy(usr);
		            submStatusB.setIpAdd(ipAdd);
		            if(submStatusB.getSubmissionAssignedTo()!=null){
		            	submStatusB.setSubmissionAssignedTo(submStatusB.getSubmissionAssignedTo());
		            }
		            submStatusB.setSubmissionCompletedBy(null);
		            int draftSubmStatusId = submStatusB.createSubmissionStatus();
		            
		    		if (draftSubmStatusId < 1) {
		                resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
		                %>
		                <jsp:include page="errorback.jsp" flush="true"><jsp:param name="errorMsg" value="<%=resultMsg%>"/></jsp:include>
		                </BODY></HTML>
		                <% return; // An error occurred; let's get out of here
		            } // End of error
		            
		            
		            // set to subm status bean and create new
		            submStatusB.setFkSubmission(String.valueOf(newSubmId));
            		submStatusB.setFkSubmissionBoard(String.valueOf(newSubmBoardId));
		            submStatusB.setFkSubmissionStatus(oldCurrentStatus);
		            submStatusB.setIsCurrent(1);
		            submStatusB.setSubmissionEnteredBy(usr);
		            submStatusB.setSubmissionStatusDate(statusDate);
		            submStatusB.setSubmissionNotes(null);
		            submStatusB.setCreator(usr);
		            submStatusB.setLastModifiedBy(usr);
		            submStatusB.setIpAdd(ipAdd);
		            if(submStatusB.getSubmissionAssignedTo()!=null){
		            	submStatusB.setSubmissionAssignedTo(submStatusB.getSubmissionAssignedTo());
		            }
		            submStatusB.setSubmissionCompletedBy(null);
		            int draftSubmStatusId1 = submStatusB.createSubmissionStatus();
		            
		    		if (draftSubmStatusId1 < 1) {
		                resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
		                %>
		                <jsp:include page="errorback.jsp" flush="true"><jsp:param name="errorMsg" value="<%=resultMsg%>"/></jsp:include>
		                </BODY></HTML>
		                <% return; // An error occurred; let's get out of here
		            } // End of error
		            
		            
		            if (resultMsg == null) { resultMsg = MC.M_Data_SvdSucc/*"Data was saved successfully"*****/; }
		            
		    	}else{
		    		
		    		submStatusB.setFkSubmission(String.valueOf(newSubmId));
		            submStatusB.setFkSubmissionBoard(String.valueOf(newSubmBoardId));
		            submStatusB.setSubmissionEnteredBy(usr);
		            submStatusB.setCreator(usr);
		            submStatusB.setLastModifiedBy(usr);
		            submStatusB.setIpAdd(ipAdd);
		            submStatusB.setSubmissionStatusDate(null);
		            submStatusB.setSubmissionAssignedTo(null);
		            submStatusB.setSubmissionCompletedBy(null);
		            submStatusB.setSubmissionNotes(null);
		            
				    if (ht.containsKey("previouslySubmitted")) { 
				    	
				        submStatusB.setSubmissionStatusBySubtype("resubmitted");
				        
				    } else {
				        submStatusB.setSubmissionStatusBySubtype("submitted");
				    }
		            
		            int newSubmStatusIdForBoard = submStatusB.createSubmissionStatus();
		            if(newSubmStatusIdForBoard>1){
		            	eirbDao.updateIscurrentForFinalOutcomeTab(newSubmId,newSubmBoardId);
		                }
		            if( newSubmStatusIdForBoard < 1 ) {
		                resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
		                break;
		            }
		            if(newSubmId > 0){
		            	CodeDao cdao = new CodeDao();
		            	int amendType=cdao.getCodeId("submission", "study_amend");
		            	int tmpnewSubmId = submB.updateAmendFlagSubmission(newSubmId,amendType);
		            	System.out.println("newSubmId:::::"+newSubmId);
		            	System.out.println("tmpnewSubmId:::::"+tmpnewSubmId);
		            	if (newSubmId!=tmpnewSubmId) {
			                resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
			                %>
			                <jsp:include page="errorback.jsp" flush="true"><jsp:param name="errorMsg" value="<%=resultMsg%>"/></jsp:include>
			                </BODY></HTML>
			                <% return; // An error occurred; let's get out of here
			            } // End of error
		            }
		    	}
		    }
	    } // End of board loop

		paramMap.put("attachFieldDetails", attachFieldDetails);
		
		
	    // Implement Flex Study part here

	    /* CodeDao studyStatCodeDao = new CodeDao();
	    int submittingCodeId = studyStatCodeDao.getCodeId("studystat", "submitting");
	    StudyStatusJB studyStatusJB = new StudyStatusJB();
	    if (submittingCodeId > 0) {
	        studyStatusJB.addStudyStatus(studyId, usr, submittingCodeId);
	    } */
	    
	    String fieldDetailsString = "[]"; 
        if (fieldDetails != null) {
                JSONArray fieldDetailsArray = new JSONArray(
                                fieldDetails);
                int noOfFields = fieldDetailsArray.length();
                for (int i = 0; i < noOfFields; i++) {
                        fieldDetailsArray.getJSONObject(i).remove(
                                        "fieldLabel");
                        fieldDetailsArray.getJSONObject(i).remove(
                                        "fieldRow");
                }
                fieldDetailsString = fieldDetailsArray.toString();
        }

        uiFlxPageDao.setComments(comments);
        uiFlxPageDao.updateCurrentPageVersionChanges(
                                "er_study", studyJB.getId(),
                                fieldDetailsString, newSubmId);
        
        uiFlxPageDao.updateAttachPageDiffFlexPageVersion(studyJB.getId(),attachFieldDetails);
        System.out.println("formRespDetails----------==========="+formRespDetails);
        uiFlxPageDao.updateFormPageDiffFlexPageVersion(studyJB.getId(), formRespDetails);//saving Form response log
        uiFlxPageDao.updateStatOfLatestVersion("er_study", studyJB.getId(), "submitting"); // marker to show this version was submitted
	    
		// -- Add a hook for customization for after a submission update succeeds
		try {
			if (!StringUtil.isEmpty(CFG.Config_Submission_Interceptor_Class) &&
					!DEFAULT_DUMMY_CLASS.equals(CFG.Config_Submission_Interceptor_Class)) {
				Class clazz = Class.forName(CFG.Config_Submission_Interceptor_Class);
				Object obj = clazz.newInstance();
				Object[] args = new Object[3];
				args[0] = (Object)request;
				args[1] = studyJB;
				args[2] = fullVersion;
				((InterceptorJB) obj).handle(args);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		// -- End of hook
        String nextScreen = "LIND".equals(CFG.EIRB_MODE) ? "flexStudyScreen" : "irbnewcheck.jsp";
%>
  <br><br><br><br>
  <p class = "successfulmsg" align = center><%=resultMsg%></p>
  <META HTTP-EQUIV=Refresh CONTENT="1; URL=<%=nextScreen%>?mode=M&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>&autoPopulate=Y">
<%        
    }// end of if body for e-sign
  } //end of if body for valid session
  else
  {
  %>
  <jsp:include page="timeout.html" flush="true"/>
  <%
  }
%>

</BODY>
</HTML>
