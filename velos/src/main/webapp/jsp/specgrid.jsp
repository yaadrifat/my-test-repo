<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<jsp:include page="include.jsp" flush="true"/>
<script>
dojo.require("dojo.widget.*");
	dojo.require("dojo.widget.TreeV3");
	dojo.require("dojo.widget.TreeNodeV3");
	dojo.require("dojo.widget.TreeBasicControllerV3");
	dojo.hostenv.writeIncludes();

function getElementsByClassName(oElm, strTagName, strClassName){
	var arrElements = (strTagName == "*" && oElm.all)? oElm.all : oElm.getElementsByTagName(strTagName);
	var arrReturnElements = new Array();
	strClassName = strClassName.replace(/-/g, "\-");
	var oRegExp = new RegExp("(^|\s)" + strClassName + "(\s|$)");
	var oElement;
	for(var i=0; i<arrElements.length; i++){
		oElement = arrElements[i];
		if(oRegExp.test(oElement.className)){
			arrReturnElements.push(oElement);
		}
	}
	return (arrReturnElements)
}

function deSelectAll()
{
arrayElements=getElementsByClassName(document, "*", "selected");
for (i=0;i<arrayElements.length;i++)
{
 arrayElements[i].className="avail"; 
}
}	
	
function Toggle(elementId)
{
 if (document.getElementById(elementId).className=="taken") return false;
 deSelectAll();
  if (document.getElementById(elementId).className=="selected"){
    document.getElementById(elementId).className="avail";
    
    }
   else{
   document.getElementById(elementId).className="selected";
   }
    
  
}
function createByName(nodeName) {
		var tree = dojo.widget.byId('tree')
		var name=document.getElementById('nodeName').value;
		if ((name=="undefined") || (name.length==0))
		{
		name="";
		}
		if (name.length==0) return false; 
		arrayOfLocations=name.split(":");
		var data;
		for (i=0;i<arrayOfLocations.length;i++){
		
		data = {widgetName:'dojo:TreeNodeV3', title:"<span style='color:blue;font-style:italic'><img src='../images/jpg/treenode_node.gif' border='0'/> "+arrayOfLocations[i]+"</span>"}
		if (i>0) {
		prevData=dojo.widget.byId('controller').createChild(prevData,0,data)
		}else
		{
		prevData=dojo.widget.byId('controller').createChild(tree,0,data)
		}
		
		}
	
	}
</script>
<style type="text/css">

td.avail {cursor: pointer;width:50px;height:50px}
td.taken {background-color:#999999;width:50px;height:50px}
td.selected {background-color: red;width:50px;height:50px}
</style>

<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Grid%><%--Grid*****--%></title>
</head>
<body>
<div dojoType="TreeBasicControllerV3" widgetId="controller"></div>	

<div dojoType="TreeV3" listeners="controller" widgetId="tree">
	
</div>

<%String[] alphaSeqArray={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ"};
ArrayList alphaSeq=new ArrayList(Arrays.asList(alphaSeqArray));
int dimA=EJBUtil.stringToNum(request.getParameter("dimA"));
int dimB=EJBUtil.stringToNum(request.getParameter("dimB"));
String nameConvA=request.getParameter("nameConvA");
String nameConvB=request.getParameter("nameConvB");
String posA=request.getParameter("posA");
String posB=request.getParameter("posB");
String location=request.getParameter("location");
String boxString="";
//posA="LR";
//nameConvA="azz";
//dimB=8;
//dimA=7;
%>
<input id="nodeName" value='<%=location%>' type="hidden">
<%String[] takenArray={"A2","B3","D1","G4"};
ArrayList takenStorage=new ArrayList(Arrays.asList(takenArray));
if (posA.equals("LR")) {%>
	<table border="1" id="box">
	
	<%if (nameConvA.equals("azz")) {
		for (int i=0;i<dimB;i++) {
	
		%>
		<tr>	
		<%for(int j=0;j<alphaSeq.size();j++)
		{
		boxString=(String)alphaSeq.get(j)+i;	
		if (j>=dimA) break;
		if (takenStorage.indexOf(boxString)>=0) {
		%>
	<td class="taken"  id=<%=boxString%> onClick="Toggle('<%=boxString%>')">
		<%} else {%>
		<td id=<%=boxString%> class="avail" onClick="Toggle('<%=boxString%>')">
		<%} %>
		<%=alphaSeq.get(j)%>(<%=i+1 %>)
		</td>
			
	<%	}
		%>
		</tr>
	<%	}
		
	}
	%>
	
	</table>
<%}
	

%>




</body>
<script>
dojo.addOnLoad(createByName)
</script>
<div> 
		<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</html>