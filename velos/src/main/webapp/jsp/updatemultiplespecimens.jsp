<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@page import="java.math.BigDecimal,java.util.ArrayList,com.velos.esch.service.util.EJBUtil"%>
<%@page import="com.velos.eres.web.specimen.SpecimenJB,com.velos.eres.web.specimenStatus.SpecimenStatusJB,com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*"%>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	HttpSession tSession = request.getSession(true);
	JSONObject jsObj = new JSONObject();
	String mySpecimenGrid;
	String createInfo;
	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN,
				"SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn/*"User is not logged in."*****/);
		out.println(jsObj.toString());
		return;
	}

	String oldESign = (String) tSession.getAttribute("eSign");
	String eSign = request.getParameter("eSign");
	if (!oldESign.equals(eSign)) {
		jsObj.put("result", -4);
		if ("userpxd".equals(Configuration.eSignConf)) {
			jsObj.put("resultMsg", MC.M_EtrWrgPassword_Svg/*"You entered a wrong e-Password. Please try saving again. "*****/);
		}
		else
		{
		jsObj.put("resultMsg", MC.M_EtrWrgEsign_Svg/*"You entered a wrong e-signature. Please try saving again. "*****/);
		}
		out.println(jsObj.toString());
		return;
	}
	String ipAdd = (String) tSession.getAttribute("ipAdd");
	String userId = (String) tSession.getAttribute("userId");
	String accountId = (String) tSession.getAttribute("accountId");
	String mode = request.getParameter("mode");
	String duration = request.getParameter("duration");
	String[] ops = request.getParameterValues("ops");
	String calledFrom = request.getParameter("calledfrom");
	mySpecimenGrid = request.getParameter("mySpecimenGrid");
	createInfo = request.getParameter("createInfo");

	String specIdSeq = "";
	String statDt = "";
	String collectedOn="";
	//String tableName = request.getParameter("tableName");
	//String[] purgeItems = request.getParameterValues("purgedItems");
	int changeCount = EJBUtil.stringToNum(request
			.getParameter("changeCount"));
	boolean deleteFlag = false, createFlag = false;
	//today
	Date date = new java.util.Date();
	String sysDate = DateUtil.dateToString(date);
	sysDate = sysDate + " " + "00:00:00";

	int printme = -1;
	int specStatRet = -1;

	//pk for default Specimen Status
	CodeDao cdSpecStat = new CodeDao();
	int fkCodelstSpecimenStat = cdSpecStat.getCodeId("specimen_stat",
			"Collected");

	//calculating the flag for create,update and delete operations		*****

	if (!"[]".equals(createInfo) && !StringUtil.isEmpty(createInfo)) {
		JSONArray updateArray = new JSONArray(createInfo);
		createFlag = true;
		//System.out.println("updateArray----" + updateArray);
	}

	mySpecimenGrid = StringUtil.replaceAll(mySpecimenGrid, "null", "\"\"");
	mySpecimenGrid = StringUtil.replaceAll(mySpecimenGrid, "velquote", "\"");
	mySpecimenGrid = StringUtil.replaceAll(mySpecimenGrid, "velbslash", "/");
	mySpecimenGrid = mySpecimenGrid.replaceAll("(\\r|\\n|\\r\\n)+", "");
//	System.out.println("mySpecimenGrid------" + mySpecimenGrid);
	JSONArray specimenArray = new JSONArray(mySpecimenGrid);
	SpecimenJB specJB;
	JSONObject dataRecord = null;
	for (int i = 0; i < specimenArray.length(); ++i) {
		dataRecord = specimenArray.getJSONObject(i);
		specJB = new SpecimenJB();
		specJB.setFkAccount(accountId);
		specIdSeq = specJB.getSpecimenIdAuto(dataRecord
				.getString("tempStudyIds"), dataRecord
				.getString("tempPatientIds"));
		if (!StringUtil.isEmpty(dataRecord.getString("specimenId"))) {
			specJB.setSpecimenId(dataRecord.getString("specimenId"));
		} else {
			specJB.setSpecimenId(specIdSeq);
		}
		specJB.setSpecType(dataRecord.getString("specTypeId"));
		specJB.setSpecDesc(dataRecord.getString("description"));
		specJB.setFkStudy(dataRecord.getString("tempStudyIds"));
		specJB.setFkPer(dataRecord.getString("tempPatientIds"));
		specJB.setFkSite(dataRecord.getString("orgMdtryId"));
		statDt = dataRecord.getString("tempStatDt");
		collectedOn=dataRecord.getString("tempStatDt");
		if(StringUtil.isEmpty(statDt)){
			String vardate=DateUtil.getFromDateStringForNullDate();
			vardate=vardate + " " + "00:00:00";
			specJB.setSpecRemovalDatetime(vardate);
			specJB.setSpecFreezeDatetime(vardate);
			
		}else{
			String SpecRemovalDatetime=statDt + " " + "00:00:00";
			specJB.setSpecRemovalDatetime(SpecRemovalDatetime);
			specJB.setSpecFreezeDatetime(SpecRemovalDatetime);
			statDt=statDt;
		} 

		if (StringUtil.isEmpty(statDt)) {
			statDt = sysDate;
		} else {
			statDt = statDt + " " + "00:00:00";
		}
		if (StringUtil.isEmpty(collectedOn)) {
			collectedOn = sysDate;
		} else {
			collectedOn = collectedOn + " " + "00:00:00";
		}
		//specJB.setSpecCollDate(statDt);
		//specJB.setSpecOrgQnty(dataRecord.getString("quantity"));
		specJB.setSpecExpectQuant(dataRecord.getString("quantity"));
		specJB.setSpecExpectedQUnits(dataRecord.getString("unitId"));
		specJB.setSpecBaseOrigQUnit(dataRecord.getString("unitId"));
		specJB.setSpectQuntyUnits(dataRecord.getString("unitId"));		
		//specJB.setFkStorage(dataRecord.getString("patientIds"));
		specJB.setSpecAltId(dataRecord.getString("altId"));
		specJB.setCreator(userId);
		specJB.setIpAdd(ipAdd);
		specJB.setFkAccount(accountId);
		specJB.setSpecCollDate(collectedOn);
		printme = specJB.setSpecimenDetails();

		SpecimenStatusJB specStatB = new SpecimenStatusJB();
		CodeDao statCodeDao = new CodeDao();

		specStatB.setFkSpecimen(printme + "");
		specStatB.setSsDate(statDt);

		if (!dataRecord.getString("specStat").equals("")) {
			specStatB.setFkCodelstSpecimenStat(dataRecord
					.getString("specStatId"));
		} else {
			specStatB.setFkCodelstSpecimenStat(fkCodelstSpecimenStat
					+ "");//default
		}
		specStatB.setSsQuantity("");
		specStatB.setSsQuantityUnits("");
		specStatB.setSsAction("");
		specStatB.setFkStudy(dataRecord.getString("tempStudyIds"));
		specStatB.setFkUserRecpt("");
		specStatB.setSsTrackingNum("");
		specStatB.setSsNote("");
		specStatB.setSsStatusBy(userId);
		specStatB.setCreator(userId);
		specStatB.setIpAdd(ipAdd);
		specStatRet = specStatB.setSpecimenStatusDetails();
		//JM: 18Nov2009: #4487: if process related status entered then default checked in status also should be entered
		if (("process").equals(statCodeDao.getCodeCustomCol1(EJBUtil
				.stringToNum(dataRecord.getString("specStatId"))))) {

			specStatB = new SpecimenStatusJB();

			specStatB.setFkSpecimen(printme + "");
			specStatB.setSsDate(statDt);
			specStatB.setFkCodelstSpecimenStat(fkCodelstSpecimenStat
					+ "");
			specStatB.setSsQuantity("");
			specStatB.setSsQuantityUnits("");
			specStatB.setSsAction("");
			specStatB.setFkStudy(dataRecord.getString("tempStudyIds"));
			specStatB.setFkUserRecpt("");
			specStatB.setSsTrackingNum("");
			specStatB.setSsNote("");
			specStatB.setSsStatusBy(userId);
			specStatB.setCreator(userId);
			specStatB.setIpAdd(ipAdd);
			specStatRet = specStatB.setSpecimenStatusDetails();
		}

	}
	if (printme >= 0) {
		jsObj.put("result", 0);
		jsObj.put("resultMsg", MC.M_Changes_SavedSucc/*"Changes saved successfully"*****/);
		out.println(jsObj.toString());
	} else if (printme == -3) {
		jsObj.put("result", -3);
		jsObj
				.put("resultMsg",
						MC.M_SpmenIdExist_ProvideDiff/*"Some Specimen ID(s) already exist, Please provide different ID(s)"*****/);
		out.println(jsObj.toString());
	} else {
		jsObj.put("result", 0);
		jsObj.put("resultMsg", MC.M_DataCnt_Svd/*"Data could not be saved"*****/);
		out.println(jsObj.toString());
	}
%>
