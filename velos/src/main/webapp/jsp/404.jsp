<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Http404NotFound %></title>
<style>
body {
  font-family:"Segoe UI", "verdana", "arial"; COLOR: #575757;
}
H1 {
  margin-top:7px;
  margin-bottom: 4px;
  color: #4465a2;
  font-size: 1.1em;
  vertical-align: bottom;
  font-weight: normal;
}
H3 {
  margin-top: 10px;
  margin-bottom: 1px;
  font-size: 0.9em;
  font-weight: normal;
}
UL {
  line-height: 1.3em;
  font-size: 0.9em;
}
.errorCode {
  color: #787878;
  font-size: 0.8em;
  font-weight: normal;
}
.divider {
  border-bottom: #b6bcc6 1px solid;
}

</style>
</head>
<body>
<table width="730" border="0" cellSpacing="0" cellPadding="0">
<tbody>
<tr>
<td width="*" align="left" id="mainTitleAlign" vAlign="middle">
<h1>
<%=LC.L_Http404WebpageNotFound %>
</h1>
</td>
</tr>
<tr>
<td align="right" class="errorCode" >
<%=LC.L_Http404 %>
<div class="divider" />
</td>
</tr>
<tr></tr>
<tr>
<td align="left" vAlign="top">
<h3><%=LC.L_Http404MostLikely %></h3>
</td>
</tr>
<tr>
<td>
<ul>
<li><%=LC.L_Http404TypingError %></li>
<li><%=LC.L_Http404LinkOutOfDate %></li>
</ul>
</td>
</tr>
</tbody>
</table>
</body>
</html>
