<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page language="java" import="com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings"%>  
<jsp:include page="ui-include.jsp" flush="true"/> 
<jsp:useBean id="tokenJB" scope="request" class="com.velos.eres.web.userSession.UserSessionTokenJB"/>
<%
String moduleName  = "irbMeeting";
String meetingDate = StringUtil.htmlEncodeXss(request.getParameter("meetingDate"));
String reviewBoard = StringUtil.htmlEncodeXss(request.getParameter("reviewBoard"));
HttpSession tSession = request.getSession(true);
%>

<script>
var paginate_study;

	function checkSubmissionRight(mode)
  		{
  			var elem = document.getElementById("pageRight");
			var submissionBrowserRight ;
			 
			 if (elem != null)
			 {
			 	submissionBrowserRight = elem.value;
			 }
			 
			 
		 	if (f_check_perm(submissionBrowserRight,mode) == false)	{
				return false;
			}
  		}
   
  	

		formsLink=function(elCell, oRecord, oColumn, oData)
		{
		var htmlStr="";
		var study=oRecord.getData("PK_STUDY");
		var pkSubmission = oRecord.getData("PK_SUBMISSION");
		var submissionBoardPK = oRecord.getData("PK_SUBMISSION_BOARD");
		var appSubmissionType	= oRecord.getData("SUBMISSION_TYPE_SUBTYPE");
		var pksubmissionStatus  = oRecord.getData("PK_SUBMISSION_STATUS");
			
		if (!study) study="";
		
 		htmlStr="<A title=\"<%=LC.L_Form%><%--Form*****--%>\" HREF=\"javascript:void(0);\" onclick=\"openFormsWin('"+ $('tabsubtype').value+"', "+study+","+pkSubmission+","+submissionBoardPK+",'"+appSubmissionType+"', "+pksubmissionStatus+")\">"+
		"<img src=\"./images/note.gif\" border=\"0\" align=\"left\"/></A> ";
 		elCell.innerHTML=htmlStr;
		}
		
		reviewLink=function(elCell, oRecord, oColumn, oData)
		{
		var htmlStr="";
		var study=oRecord.getData("PK_STUDY");
		var pkSubmission = oRecord.getData("PK_SUBMISSION");
		var submissionBoardPK = oRecord.getData("PK_SUBMISSION_BOARD");
		var appSubmissionType	= oRecord.getData("SUBMISSION_TYPE_SUBTYPE");
		/////////////
		 var submission_review_subtype = oRecord.getData("SUBMISSION_REVIEW_SUBTYPE");
		
		 var pksubmissionStatus  = oRecord.getData("PK_SUBMISSION_STATUS");
		
		var tabsubtypeforreview = "irb_revdummy_tab"; // dummy tab actually does not exist
		
		if (submission_review_subtype == "rev_full")
		{
			tabsubtypeforreview = "irb_revfull_tab";
		}	
		 		 
		if (submission_review_subtype == "rev_anc")
		{
			tabsubtypeforreview = "irb_revanc_tab";
		}
		if (submission_review_subtype == "rev_exe")
		{
			tabsubtypeforreview = "irb_revexem_tab";
		} 
  		
  		if (submission_review_subtype == "rev_exp")
		{
			tabsubtypeforreview = "irb_revexp_tab";
		} 
  		////////////////	
		if (!study) study="";
		
 		htmlStr="<A title=\"<%=LC.L_Review%><%--Review*****--%>\" HREF=\"javascript:void(0);\" onclick=\"openReviewWin('"+  tabsubtypeforreview+"', "+study+","+pkSubmission+","+submissionBoardPK+",'"+appSubmissionType+"', "+pksubmissionStatus+")\">"+
		"<img src=\"./images/CommentField.gif\" border=\"0\" align=\"left\"/></A> ";
 		elCell.innerHTML=htmlStr;
		}

function confirmBoxTopic(topic){
		 if (checkSubmissionRight('E') == false)
			 {
			 	return false;
			 }
var paramArray = [topic];	
if (confirm(getLocalizedMessageString("M_WantDel_MtgTopic",paramArray)))/*if (confirm("Do you want to delete Meeting Topic: "+ topic+"?" ))*****/ 
return true;
else
return false;
   
}

      
      		
function checkSearch(formobj)
{ 
	paginate_study.render();
 	paginate_study.runFilter();
}

//Added by Aakriti to implement user details mouse over link
prinvLink = function(elCell, oRecord, oColumn, oData)
{
	var piName=oRecord.getData("STUDY_PRINV");
	var piDetails=oRecord.getData("STUDY_PRINV_DETAILS");
	
	//jQuery(".yui-dt-col-PI_NAME").css("width","15%");
	if(piName!='' && piName!=null){
		piDetails=escapeSingleQuoteEtc(piDetails);
			elCell.innerHTML=piName+"<a href=\"#\"><img src=\"./images/View.gif\" border=\"0\" align=\"left\" style=\"float: right;\" onmouseover=\"return overlib(htmlEncode('"+piDetails+"'),ABOVE,CAPTION,'');\" onmouseout=\"nd();\" > </a>";
	}
}


        
 $E.addListener(window, "load", function() { 
	 $('CSRFToken').value="<%=StringUtil.trueValue(tokenJB.createTokenForUser((String)tSession.getAttribute("userId")==null?0:StringUtil.stringToNum((String)tSession.getAttribute("userId"))))%>";
    paginate_study=new VELOS.Paginator('<%=moduleName%>',{
 				sortDir:"asc", 
				sortKey:"STUDY_NUMBER",
				defaultParam:"userId,accountId,grpId,tabsubtype,meetingDate,meetingId,CSRFToken",
				filterParam:"meetingDate",
				dataTable:'serverpagination_study',
				searchEnable:false,
				rowSelection:[5,10,15,25,50,75,100]
				});
	paginate_study.render(); 
	paginate_study.runFilter();
	elem = document.getElementById("yui-gen2")
			 }
				);

 $E.addListener(window, "load", function() { 
	 $('CSRFToken').value="<%=StringUtil.trueValue(tokenJB.createTokenForUser((String)tSession.getAttribute("userId")==null?0:StringUtil.stringToNum((String)tSession.getAttribute("userId"))))%>";
	    paginate_study=new VELOS.Paginator('<%=moduleName%>Topic',{
	 				sortDir:"asc", 
					sortKey:"TOPIC_NUMBER",
					defaultParam:"userId,accountId,grpId,tabsubtype,meetingDate,meetingId,CSRFToken",
					filterParam:"meetingDate,CSRFToken",
					dataTable:'serverpagination_topic',
					saveEnable:false,
					searchEnable:false,
					rowSelection:[5,10,15,25,50,75,100]
					});
		paginate_study.render(); 
		paginate_study.runFilter();
		elem = document.getElementById("yui-gen2")
				 }
					);

		titleLink=function(elCell, oRecord, oColumn, oData)
		{
		var studyTitle=oRecord.getData("STUDY_TITLE");
		var reg_exp = /\'/g;
		studyTitle = studyTitle.replace(reg_exp, "\\'");
		reg_exp = /[\r\n|\n]/g;
		studyTitle = studyTitle.replace(reg_exp, " ");
 		elCell.innerHTML="<a href=\"#\" onmouseover=\"return overlib('"+studyTitle+"',CAPTION,'<%=LC.L_Study_Title%><%--Study Title*****--%>');\" onmouseout=\"return nd();\"><img border=\"0\" src=\"./images/View.gif\" title=\"<%=LC.L_View%><%--View*****--%>\"/></a>";
		}

		topicLink=function(elCell, oRecord, oColumn, oData)
		{
		var meetingTopic=oRecord.getData("MEETING_TOPIC");
		if (meetingTopic == null) { meetingTopic = ''; }
		var reg_exp = /\'/g;
		var reg_dbqt = /\"/g;
		var reg_lt  = /&lt;/g;
		var reg_gt  = /&gt;/g;
		var reg_cr  = /\r/g;
		var reg_lf  = /\n/g;
		meetingTopic = meetingTopic.replace(reg_exp, "\\'").replace(reg_dbqt, "&quot;").replace(reg_lt, "<").replace(reg_gt, ">")
        .replace(reg_cr, "").replace(reg_lf, "<br/>");
 		elCell.innerHTML="<a href=\"#\" onmouseover=\"return overlib('"+meetingTopic+"',CAPTION,'<%=LC.L_Meeting_Topic%><%--Meeting Topic*****--%>');\" onmouseout=\"return nd();\"><%=LC.L_View_Topic%><%--View Topic*****--%></a>";
		}

		topicNumberLink=function(elCell, oRecord, oColumn, oData)
		{
		var topicNumber=oRecord.getData("TOPIC_NUMBER");
		if (topicNumber == null) { topicNumber = ''; }
		var pkTopic=oRecord.getData("PK_REVIEW_MEETING_TOPIC");
 		elCell.innerHTML="<A title=\"<%=LC.L_Action%><%--Action*****--%>\" HREF=\"javascript:void(0);\" onclick=\"openMeetingTopicEdit('"+pkTopic+"')\">"+topicNumber+"</a>"
		}

		studyNumber=function(elCell, oRecord, oColumn, oData)
		{
		var htmlStr="";
		var study=oRecord.getData("PK_STUDY");
		var studyNumber=oRecord.getData("STUDY_NUMBER");
		var submTypeSubtype=oRecord.getData("SUBMISSION_TYPE_SUBTYPE");
		
		if (!study) study="";
		if (!studyNumber) studyNumber="";

        if ('new_app' == submTypeSubtype) {
        	<%if("LIND".equalsIgnoreCase(CFG.EIRB_MODE)){%>
 		    htmlStr="<A onclick=\"return checkSubmissionRight('E');\" HREF=\"study.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=M&studyId="+study+"\">"
		        +studyNumber+"</A> ";
		     <%} else{%>
		     htmlStr="<A onclick=\"return checkSubmissionRight('E');\" HREF=\"irbnewinit.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=M&studyId="+study+"\">"
		        +studyNumber+"</A> ";
		     <%}%>
        } else {
 		    htmlStr="<A onclick=\"return checkSubmissionRight('E');\" HREF=\"study.jsp?srcmenu=tdMenuBarItem3&selectedTab=1&mode=M&studyId="+study+"\">"
	            +studyNumber+"</A> ";
        }
 		elCell.innerHTML=htmlStr;
		}
	 
		
		amdLink=function(elCell, oRecord, oColumn, oData) {
			formsLink(elCell, oRecord, oColumn, oData, 'study_amend');
		}
		probLink=function(elCell, oRecord, oColumn, oData) {
			formsLink(elCell, oRecord, oColumn, oData, 'prob_rpt');
		}
		contLink=function(elCell, oRecord, oColumn, oData) {
			formsLink(elCell, oRecord, oColumn, oData, 'cont_rev');
		}
		closLink=function(elCell, oRecord, oColumn, oData) {
			formsLink(elCell, oRecord, oColumn, oData, 'closure');
		}

			
		function openFormsWin(tabsubtype,study,appSubmissionType)
		{
			
			 if (checkSubmissionRight('E') == false)
			 {
			 	return false;
			 }
	
			   windowName = window.open("irbForms.jsp?appSubmissionType="+appSubmissionType+"&tabsubtype="+tabsubtype+"&studyId="+study+"&hiddenflag=N","formsWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=800,left=400,top=400");
			   windowName.focus();
  		}
		function openMeetingTopicEdit(pkTopic)
		{
			if (checkSubmissionRight('E') == false)
			 {
			 	return false;
			 }
	
			   var revBoard = document.subcommon.reviewBoard.options[document.subcommon.reviewBoard.selectedIndex].value;
			   windowName = window.open("meetingTopic.jsp?pkTopic="+pkTopic+"&meetingId="+document.subcommon.meetingId.value+"&reviewBoard="+revBoard,"topicWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=400,top=400");
			   windowName.focus();
  		}


		function openMeetingMom()
		{
			if (checkSubmissionRight('E') == false)
			 {
			 	return false;
			 }
			 
			  var revBoard = document.subcommon.reviewBoard.options[document.subcommon.reviewBoard.selectedIndex].value;
			  var meetingDatePK = document.subcommon.meetingDate.options[document.subcommon.meetingDate.selectedIndex].value;
			  
			   windowName = window.open("irbMom.jsp?revBoard="+revBoard+"&meetingDatePK="+meetingDatePK,"momWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=750,height=650,left=20,top=20");
			   windowName.focus();
			   
		}
		
		function openReviewWin(selectedTab,study,pkSubmission,submissionBoardPK,appSubmissionType,pksubmissionStatus)
		{
					 if (checkSubmissionRight('E') == false)
			 {
			 	return false;
			 }
	
			   var w = screen.availWidth-100;
			   windowName = window.open("irbReview.jsp?appSubmissionType="+appSubmissionType+"&studyId="+study+"&submissionPK="+pkSubmission+"&submissionBoardPK="+ submissionBoardPK+"&tabsubtype="+selectedTab+"&hiddenflag=N&provisoflag=N&newprovisoflag=N&pksubmissionStatus="+pksubmissionStatus,"revWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width="+w+",height=600,left=10,top=100");
			   windowName.focus();
  		}
  		 	 		 	 
			 		 	 
		function openFormsWin(tabsubtype,study,pkSubmission,submissionBoardPK,appSubmissionType,pksubmissionStatus)
		{
			
			 		 if (checkSubmissionRight('E') == false)
			 {
			 	return false;
			 }
	
			   windowName = window.open("irbForms.jsp?appSubmissionType="+appSubmissionType+"&tabsubtype="+tabsubtype+"&studyId="+study+"&submissionPK="+pkSubmission+"&submissionBoardPK="+ submissionBoardPK+"&hiddenflag=N&meetingpage=N&pksubmissionStatus="+pksubmissionStatus,"formsWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=800,left=400,top=400");
			   windowName.focus();
  		}


	
		delLink=function(elCell, oRecord, oColumn, oData)
		{
			var htmlStr="";
		 
		 	var pkTopic=oRecord.getData("PK_REVIEW_MEETING_TOPIC");
		 	
		 	var topicNumber=oRecord.getData("TOPIC_NUMBER");
		if (topicNumber == null) { topicNumber = ''; }
		
			htmlStr="<A title=\"<%=LC.L_Delete%><%--Delete*****--%>\" href=\"deleteMeetingTopic.jsp?pk="+pkTopic+ "&meetingId="+document.subcommon.meetingId.value+"&reviewBoard="+<%=reviewBoard%>+"\" onclick=\"return confirmBoxTopic('"+ topicNumber +"');\">"+
			"<img src=\"./images/delete.gif\" border=\"0\" align=\"left\"/></A>";
		 
	 
		    elCell.innerHTML=htmlStr;
		}

        function getIEVersionNumber() {
            var ua = navigator.userAgent;
            var MSIEOffset = ua.indexOf("MSIE ");
            if (MSIEOffset == -1) {
                return 0;
            } else {
                return parseFloat(ua.substring(MSIEOffset + 5, ua.indexOf(";", MSIEOffset)));
            }
        }

</script>

<body class="yui-skin-sam">

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/> 
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/><!--km-->
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/><!--km--> 
   <%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.service.util.StringUtil,java.sql.*,com.velos.eres.business.common.*"%>  

  
 

<%

 //HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession)) {
    String userId ="";
    int meetingGrpRight  = 0;
    
    
    GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	meetingGrpRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_MM"));

    
    userId = (String) tSession.getValue("userId");
    String defUserGroup = (String) tSession.getAttribute("defUserGroup");
    String accountId = (String) tSession.getValue("accountId");
    String tabsubtype  = StringUtil.htmlEncodeXss(request.getParameter("tabsubtype"));
    String meetingId = StringUtil.htmlEncodeXss(request.getParameter("meetingId"));
    
    // Get review boards
    EIRBDao eIrbDao = new EIRBDao();
    ArrayList boardIdList = eIrbDao.getBoardIdList();
    eIrbDao.getReviewBoards(EJBUtil.stringToNum(accountId), EJBUtil.stringToNum(defUserGroup));
    String script = "onChange='changeMeetingDates(document.subcommon)'";
    String boardMenu = eIrbDao.getBoardDropDown(request.getParameter("reviewBoard"), script);
    ReviewMeetingDao reviewMeetingDao = new ReviewMeetingDao();
    reviewMeetingDao.getReviewMeetings(EJBUtil.stringToNum(accountId), 
            EJBUtil.stringToNum(defUserGroup), boardIdList);
    
    //Added by Manimaran to give access right for default admin group to the delete link 
	int usrId = EJBUtil.stringToNum(userId);
	userB.setUserId(usrId);
	userB.getUserDetails();
	
	 int pageRight = 0;
     pageRight = meetingGrpRight;

    if (pageRight > 0 )    {
    
    %>
        
  <Form name="subcommon" method="POST" onsubmit="checkSearch(this);">
    
    <input type="hidden" id="accountId" value="<%=accountId%>">
    <input type="hidden" id="userId" value="<%=userId%>">
    <input type="hidden" id="grpId" value="<%=defUserGroup%>">
    <input type="hidden" id="tabsubtype" value="<%=tabsubtype%>">
    <input type="hidden" id="meetingId" value="<%=meetingId%>">
 	 <input type="hidden" id="pageRight" value="<%=pageRight%>">	
     <input type="hidden" id="CSRFToken" name="CSRFToken">
    <p class="sectionHeadings wcg-sectionHeadings">
    <table cellspacing="0" cellpadding="0" border="0" width="99%" class="basetbl outline midalign">
        	<tr height="5"></tr>
    	<tr >
    		<td>
    			&nbsp;&nbsp;&nbsp;<%=LC.L_Select_ReviewBoard%><%--Select Review Board*****--%>
    		</td>
    		<td>
    		    &nbsp;&nbsp;&nbsp;<%=boardMenu%>
      	    </td>
    		<td>
    		    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Select_MeetingDate%><%--Select Meeting Date*****--%>
      	    </td>
    		<td>
    		    &nbsp;&nbsp;&nbsp;<select name="meetingDate" onChange="document.subcommon.submit();"></select>
      	    </td>
<script>
    var selIndex = 0;
    for(var i = 0; i<document.subcommon.reviewBoard.options.length; i++) {
        if (document.subcommon.reviewBoard.options[i].value == 
                "<%=StringUtil.htmlEncodeXss(request.getParameter("reviewBoard"))%>") {
            selIndex = i;
            break;
        }
    }

    var meetingdb = new Object();
<%  for (int iBoard=0; iBoard<boardIdList.size(); iBoard++) {  %>
        meetingdb[<%=iBoard%>] = [<%=reviewMeetingDao.getMeetingValueTextPairs((String)boardIdList.get(iBoard))%>];
<%  }  %>
    document.subcommon.meetingDate.options.length = 0;
    var defaultdb = meetingdb[selIndex];
    var selMeetingIndex = 0;
    if (defaultdb != null) {
	    for (var i = 0; i < defaultdb.length; i++) {
		    if (i==0) { document.subcommon.meetingId.value = defaultdb[i].value; }
		    if (defaultdb[i].value ==
			    "<%=StringUtil.htmlEncodeXss(request.getParameter("meetingDate"))%>") { 
			    document.subcommon.meetingDate.options[i] = new Option(defaultdb[i].text, defaultdb[i].value, true, true);
			    document.subcommon.meetingId.value = defaultdb[i].value;
			    selMeetingIndex = i;
		    } else {
			    document.subcommon.meetingDate.options[i] = new Option(defaultdb[i].text, defaultdb[i].value);
		    }
	    }
	    var ieVer = getIEVersionNumber();
	    if (ieVer != 0 && ieVer < 7 && document.subcommon.meetingDate.options != null
	    	    && document.subcommon.meetingDate.options.length > selMeetingIndex) {
	        document.subcommon.meetingDate.options[selMeetingIndex].selected = true;
	    }
    }
    function changeMeetingDates(formobj)
    {
	    document.subcommon.meetingDate.options.length = 0;
	    var db = meetingdb[formobj.reviewBoard.selectedIndex];
	    if (db != null) {
		    for (var i = 0; i < db.length; i++) {
			    document.subcommon.meetingDate.options[i] = new Option(db[i].text, db[i].value);
		    }
	    }
    	formobj.submit();
    }
  	function openMeetingTopic() {
  		
  		if (checkSubmissionRight('N') == false)
			 {
			 	return false;
			 }
	
		   windowName = window.open("meetingTopic.jsp?meetingId="+document.subcommon.meetingId.value+"&reviewBoard="+document.subcommon.reviewBoard.value,"meetingTopic","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=400,top=400");
		   windowName.focus();
  	}
  		
</script>
      	    <td>&nbsp;&nbsp;&nbsp; <!--   bug no 4209 <button type="submit" onClick="document.subcommon.submit();"><%=LC.L_Search%></button> ...-->	 </td>
    	</tr>
    	</table>	  
    </p>
    			<br>
    						
        <div >
    <div id="serverpagination_study" ></div>
    </div>
    
    <br>
    <P class="sectionHeadings">&nbsp;</P>
    <table cellspacing="0" cellpadding="0" border="0" width="99%" class="basetbl outline midalign">
    	<tr height="5"></tr>
        <tr align="right">
            <td width="70%" align="left"><%=LC.L_Meeting_Topics%><%--Meeting Topics*****--%></td>
    		<td>
    			&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="openMeetingTopic();"><%=LC.L_Add_MeetingTopic%><%--Add Meeting Topic*****--%></a>
    		</td>
    		<td>
    		    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="openMeetingMom();"><%=LC.L_Compile_MeetingMinutes%><%--Compile Meeting Minutes*****--%></a>
      	    </td>
        </tr>
     	<tr height="5"></tr>
    </table>
<br>

    <div id="serverpagination_topic" ></div>
    </div>
    
  </Form>

<script>
	 //	alert(paginate_study);
		//if (paginate_study) paginate_study.runFilter();		

	</script>
    <%
    } //end of if body for page right
    else{
%>
    <%@include file="accessdenied.jsp" %>
<%
    } //end of else body for page right
}//end of if body for session
else {
%>
    
    <jsp:include page="timeout.html" flush="true"/>
<%}
%>
<div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>