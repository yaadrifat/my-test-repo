<%@page import="com.velos.eres.service.util.StringUtil"%>
<%@page import="com.velos.eres.web.appendix.AppendixJB"%>
<%@page import="com.velos.eres.web.studyRights.StudyRightsJB"%>
<%@page import="java.util.HashMap"%>
<%@page import="org.json.*"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%!
static final String successMsg = "Attachment fixed successfully!";
static final String failureMsg = "An error occurred. Please try again. If the problem persists, please fix the attachment manually.";
static final String noActionMsg1 = "The attachment ID is invalid; no action was taken.";
static final String noActionMsg2 = "The attachment was not empty; no action was taken.";
static final String noActionMsg3 = "No replacement attachment found; no action was taken.";
static final String noActionMsg4 = "User is not authorized; no action was taken.";
%>
<%
	HttpSession tSession = request.getSession(false);
	if (!sessionmaint.isValidSession(tSession)) {
		return;
	}
	JSONObject jsObj = new JSONObject();
	String apndxId = request.getParameter("apndxId");
	if (StringUtil.stringToNum(apndxId) < 1) {
		try {
			jsObj.put("message", noActionMsg1);
		} catch(Exception e) {}
		out.println(jsObj.toString());
		return;
	}
	System.out.println("Fixing apndxId="+apndxId);
	int apndxIdInt = StringUtil.stringToNum(apndxId);
	AppendixJB appendixJB = new AppendixJB();
	appendixJB.setId(apndxIdInt);
	HashMap<String, Object> apndxData = appendixJB.checkApndxData();
	if (apndxData == null || apndxData.isEmpty()) {
		try {
			jsObj.put("message", noActionMsg1);
		} catch(Exception e) {}
		out.println(jsObj.toString());
		return;
	}
	double pageRight = 0d;
	int pageRightApndx = 0;
	StudyRightsJB stdRights =(StudyRightsJB) tSession.getAttribute("studyRights");
	if (stdRights == null || (stdRights.getFtrRights().size()) == 0) {
		pageRight = 0d;
	} else {
		pageRight = Double.parseDouble(stdRights.getFtrRightsByValue("STUDYVER"));
		pageRightApndx = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYAPNDX"));
	}
	if (pageRight < 1.0d || pageRightApndx < 1) {
		try {
			jsObj.put("message", noActionMsg4);
		} catch(Exception e) {}
		out.println(jsObj.toString());
		return;
	}

	if ((Integer)apndxData.get("len") > 0) {
		try {
			jsObj.put("message", noActionMsg2);
		} catch(Exception e) {}
		out.println(jsObj.toString());
		return;
	}
	try {
		int result = appendixJB.fixAttachment(
				(String)  apndxData.get("filename"),
				(Integer) apndxData.get("fkStudy"),
				(Integer) apndxData.get("category"));
		if (result > 0) {
			jsObj.put("message", successMsg);
		} else if (result == 0) {
			jsObj.put("message", noActionMsg3);
		} else {
			jsObj.put("message", failureMsg);
		}
	} catch(Exception e) {
		jsObj.put("message", failureMsg);
		e.printStackTrace();
	}
	out.println(jsObj.toString());
%>
