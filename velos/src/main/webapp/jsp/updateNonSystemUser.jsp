<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<%@ page language = "java" import = "com.velos.eres.web.moreDetails.*" %>

<HTML>
 
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT>  

	function confirmContinue(formobj, mode,name) {
		
			formobj.action="updateNonSystemUser.jsp";					
			
			//KM-Modified for User Name and Email duplicate checking.
			
			if (mode =='M'){					
			
    			if (name == 'UserName')
					formobj.countOfUserMod.value= "1";
    			else
					formobj.countOfEmailMod.value= "1";

			}
			else{

				if (name == 'UserName')
					formobj.countOfUserNew.value= "1";
				else 
					formobj.countOfEmailNew.value= "1";
			}
    		
    		
    		
    		formobj.submit();    		   		
    		
	}

</SCRIPT>


</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id="usr" scope="request" class="com.velos.eres.web.accountWrapper.AccountWrapperJB"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="usertemp" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="userBUpdate" scope="request" class="com.velos.eres.web.user.UserJB"/>


<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,javax.mail.*, javax.mail.internet.*, javax.activation.*,java.text.*,com.velos.eres.service.util.*"%>
<%
String pname=request.getParameter("pname");

if (pname == null)
pname = "null";

int ret = 0; 

String usrFirstName,usrMidName, usrLastName, usrJobType, usrPrimarySpeciality,usrWrkExp,usrPhaseInv ;

String  usrSiteName, usrGroup, usrLogin, usrPass, usrAddress, usrCity, usrState;

String usrZip, usrEmail, usrCountry, usrPhone,userAddId, usrESign;
String userId = null;

String mode, userSecretQues, userSecretAns,src;

 String messageFrom = "";
 String messageText = "";
 String messageSubject = "";
 String messageHeader = "";
 String messageFooter = "";		
 String completeMessage = "";
 String smtpHost = "";
 String domain = "";
 String messageTo = "";
 int rows = 0;
 String uStat = "";
 String pwdXpDate="";
 String esignXpDate="";
 String usr_session = "";
 String timeZone="";
 String messageSubtext = "";
 String userResetPass = ""; 
 String usereResetSign = "";
 String prevStatus = ""; 
 String accsites="";
 String usrCode = "";
 String nLevel= "";
 String networkUsrId = "";



	int countUserNames = 0; 
	int countUserMails = 0;
	
	  String deactivateFromTeam = request.getParameter("deactivateFromTeam");
    
    if (StringUtil.isEmpty(deactivateFromTeam))
    {
     deactivateFromTeam = "0";
    } 
	


String eSign = request.getParameter("eSign");
	
mode = request.getParameter("mode");


src = request.getParameter("srcmenu");

usrCode = request.getParameter("userCode");
usrCode = (   usrCode  == null      )?"":(  usrCode ) ;

// modified by gopu to fix the issues #1917 for trimming leading and trailing spaces
usrFirstName = request.getParameter("userFirstName").trim();
usrFirstName = (   usrFirstName  == null      )?"":(  usrFirstName ) ;

usrMidName = request.getParameter("userMidName").trim();
usrMidName = (usrMidName == null)?"":(usrMidName);


usrLastName = request.getParameter("userLastName").trim();
usrLastName = (   usrLastName  == null      )?"":(  usrLastName ) ;

usrJobType = request.getParameter("jobType");
usrJobType = (   usrJobType  == null      )?"":(  usrJobType ) ;

usrPrimarySpeciality = request.getParameter("primarySpeciality");
usrPrimarySpeciality = (   usrPrimarySpeciality  == null      )?"":(  usrPrimarySpeciality ) ;


usrSiteName = request.getParameter("accsites");
usrSiteName = (   usrSiteName  == null      )?"":(  usrSiteName ) ;

usrAddress = request.getParameter("userAddress");
usrAddress = (   usrAddress  == null      )?"":(  usrAddress ) ;	

usrCity = request.getParameter("userCity");
usrCity = (   usrCity  == null      )?"":(  usrCity ) ;

usrState = request.getParameter("userState");
usrState = (   usrState  == null      )?"":(  usrState ) ;

usrZip = request.getParameter("userZip");
usrZip = (   usrZip  == null      )?"":(  usrZip ) ;	

usrPhone = request.getParameter("userPhone");
usrPhone = (   usrPhone  == null      )?"":(  usrPhone ) ;	

usrEmail = request.getParameter("userEmail");
usrEmail = (   usrEmail  == null      )?"":(  usrEmail ) ;	

usrCountry = request.getParameter("userCountry");
usrCountry = (   usrCountry  == null      )?"":(  usrCountry ) ;	

usr_session = request.getParameter("userSession");

accsites = request.getParameter("userSiteId");

uStat = request.getParameter("userStatus");//KM

String from = request.getParameter("from")==null?"":request.getParameter("from");
nLevel = (request.getParameter("nLevel")==null)?"":request.getParameter("nLevel");
networkUsrId = (request.getParameter("networkUsrId")==null)?"0":request.getParameter("networkUsrId");



 HttpSession tSession = request.getSession(true); 
 String ipAdd = (String) tSession.getValue("ipAdd");
 String usr1 = null;
 usr1 = (String) tSession.getValue("userId");

if (sessionmaint.isValidSession(tSession))
   {	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {

 String accId = (String) tSession.getValue("accountId");

 String userIdMod = request.getParameter("userId");

if (mode.equals("M"))

{

	userId = request.getParameter("userId");	

	usr.setUserId(userId);
	usr.populateUser();

}

 
 
 



usr.setUserLastName(usrLastName);
usr.setUserMidName(usrMidName);
usr.setUserFirstName(usrFirstName);

usr.setUserCodelstJobtype(usrJobType);

usr.setUserCodelstSpl(usrPrimarySpeciality);

usr.setUserSiteId(usrSiteName);

usr.setUserType("N");



usr.setUserAccountId(accId);




if (pname.equals("null")) {
	//usr.setUserStatus("A");
	usr.setUserStatus(uStat);//KM 

	
}


usr.setAddPriUser(usrAddress);

usr.setAddCityUser(usrCity);

usr.setAddStateUser(usrState);

usr.setAddZipUser(usrZip);

usr.setAddCountryUser(usrCountry);

usr.setAddPhoneUser(usrPhone);
usr.setAddEmailUser(usrEmail);


int count =0;


userAddId = request.getParameter("userAddId"); 

String userHidden = request.getParameter("userHidden");


if (userHidden != null) {
	if(!userHidden.equals("0")) //KM-22Aug08
	userHidden = "1";
}
else
	userHidden = "0";


//KM-Moved up here.
String flagUserNew = request.getParameter("countOfUserNew");
flagUserNew = (flagUserNew==null)?"":flagUserNew;			
		
String flagEmailNew = request.getParameter("countOfEmailNew");
flagEmailNew = (flagEmailNew==null)?"":flagEmailNew;

String flagUserMod = request.getParameter("countOfUserMod");
flagUserMod = (flagUserMod==null)?"":flagUserMod;

String flagEmailMod = request.getParameter("countOfEmailMod");
flagEmailMod = (flagEmailMod==null)?"":flagEmailMod;

%>

<Form name = "nsuserdetails" method="post" action="updateNonSystemUser.jsp"  onsubmit = "" >	

	<input type="hidden" name="eSign" size = 20  value = "<%=eSign%>" >
	<input type="hidden" name="mode" size = 20  value = "<%=mode%>" >
	<input type="hidden" name="srcmenu" size = 20  value = "<%=src%>" >	
	<input type="hidden" name="from" size = 20  value = "<%=from%>" >
	<input type="hidden" name="userCode" size = 20  value = "<%=usrCode%>" >
	<input type="hidden" name="userFirstName" size = 20  value = "<%=usrFirstName%>" >
	<input type="hidden" name="userMidName" size = 20  value = "<%=usrMidName%>" >
	<input type="hidden" name="userLastName" size = 20  value = "<%=usrLastName%>" >	
	<input type="hidden" name="jobType" size = 20  value = <%=usrJobType%> >
	<input type="hidden" name="primarySpeciality" size = 20  value = <%=usrPrimarySpeciality%> >
	<input type="hidden" name="accsites" size = 20  value = <%=usrSiteName%> >
	
	<input type="hidden" name="userAddress" size = 20  value = "<%=usrAddress%>" >
	<input type="hidden" name="userCity" size = 20  value = "<%=usrCity%>" >
	<input type="hidden" name="userState" size = 20  value = "<%=usrState%>" >	
	<input type="hidden" name="userZip" size = 20  value = "<%=usrZip%>" >
	<input type="hidden" name="userEmail" size = 20  value = "<%=usrEmail%>" >
	<input type="hidden" name="userCountry" size = 20  value = "<%=usrCountry%>" >
	<input type="hidden" name="userSession" size = 20  value = "<%=usr_session%>" >
	<input type="hidden" name="userSiteId" size = 20  value = <%=accsites%> >
	<input type="hidden" name="userId" size = 20  value = <%=userIdMod%> >
	
	<input type="hidden" name="userAddId" size = 20  value = <%=userAddId%> >
	<input type="hidden" name="userPhone" size = 20  value = "<%=usrPhone%>" >
	<input type="hidden" name="userStatus" size=20 value ="<%=uStat%>">  <!-- KM -040408-->
	
	
	<!--KM-Modified on Aug12,2008 -->

	<input type="hidden" name="countOfUserMod"  value="<%=flagUserMod%>">
	<input type="hidden" name="countOfEmailMod" value="<%=flagEmailMod%>" >
	<input type="hidden" name="countOfUserNew" value="<%=flagUserNew%>">
	<input type="hidden" name="countOfEmailNew" value="<%=flagEmailNew%>">
	<input type="hidden" name="deactivateFromTeam" value=<%=deactivateFromTeam%> >
	<input type="hidden" name="userHidden" value = "<%=userHidden%>" >  <!--KM- Added -->
 		

<%
if (mode.equals("M"))

{


	
				countUserNames = userB.getCount(usrLastName, usrFirstName, accId, mode, userId );
				countUserMails = userB.getCount(usrEmail, accId, mode, userId  );	
				
//				String flagUserMod = request.getParameter("countOfUserMod");
//				flagUserMod = (flagUserMod==null)?"":flagUserMod;
//				
//				String flagEmailMod = request.getParameter("countOfEmailMod");
//				flagEmailMod = (flagEmailMod==null)?"":flagEmailMod;
				
				if(countUserNames>0 && flagUserMod.equals("")&&false) {
				
				
				%>
				
				
					<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>
					<p class = "successfulmsg" align = center> <%=MC.M_UsrNameAldy_WantOverride%><%--The User Name you have entered already exists. Do you want to over-ride?*****--%></p>					
										
						<table align=center>
							<tr width="100%"> 
								<td width="100%" > 
									<button name="Continue" onClick="return confirmContinue(document.nsuserdetails, 'M','UserName');" >
										<%=LC.L_Ok_Upper%><%--OK--%>
									</button>
									<button onClick="window.history.back();" name="return" type="button">
										<%=LC.L_Cancel%><%--Cancel--%>
									</button>
								</td>
							</tr>
						</table>	
						
				
					
					<%
					
					return;
					
					
					} 
					
					if(countUserMails>0 && flagEmailMod.equals("")&&false) {



				%>
				
					<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>
					<p class = "successfulmsg" align = center> <%=MC.M_UsrEmailAldy_WantOverride%><%--The User Email ID you have entered already exists. Do you want to over-ride?*****--%></p>					
						<table align=center>
							<tr width="100%"> 
								<td width="100%" > 
									<button name="Continue" onClick="return confirmContinue(document.nsuserdetails, 'M','Email');" >
										<%=LC.L_Ok_Upper%><%--OK--%>
									</button>
									<button onClick="window.history.back();" name="return" type="button">
										<%=LC.L_Cancel%><%--Cancel--%>
									</button>
								</td>
							</tr>
						</table>
					<%
					return;
				}
//if the user changes the primary organization, set the user site flag to S - User has access to only specified organizations
    if (!(accsites.equals(usrSiteName))) {%>
    
	<%
	   usr.setUserSiteFlag("S");
    }
   
	//userAddId = request.getParameter("userAddId"); 

	usr.setUserId(userId);	
	usr.setUserPerAddressId(userAddId );

	usr.setModifiedBy(usr1);
	usr.setIpAdd(ipAdd);
	usr.setUserType("N");
	usr.setUserHidden(userHidden);//KM
	usertemp.setUserId(EJBUtil.stringToNum(userId));
	usertemp.getUserDetails();		
	ret = usr.updateUser();	
	
	if (ret >= 0 && deactivateFromTeam.equals("1"))
				{
					userBUpdate.deactivateUser(userId,usr1,ipAdd);
				}
	
	
	%>
	
	<BR>
	<BR>
	<BR>
	<BR>
	<BR>
	<BR>
	<BR>
	<BR>
	<BR>
	

<%
	

	} else {
	
	usr.setCreator(usr1);
	
	usr.setIpAdd(ipAdd);
	
	usr.setUserHidden(userHidden);//KM

	//JM: 12Dec2007:	
		
		countUserNames = userB.getCount(usrLastName, usrFirstName, accId, mode, "");		
		countUserMails = userB.getCount(usrEmail, accId, mode ,"" );

//		String flagUserNew = request.getParameter("countOfUserNew");
//		flagUserNew = (flagUserNew==null)?"":flagUserNew;			
//				
//		String flagEmailNew = request.getParameter("countOfEmailNew");
//		flagEmailNew = (flagEmailNew==null)?"":flagEmailNew;

		if(countUserNames>0 && flagUserNew.equals("")&&false) {


				%>
				
					<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>
					<p class = "successfulmsg" align = center> <%=MC.M_UsrNameAldy_WantOverride%><%--The User Name you have entered already exists. Do you want to over-ride?*****--%></p>
					    <table align=center>
							<tr width="100%"> 
								<td width="100%" > 
									<button name="Continue" onClick="return confirmContinue(document.nsuserdetails, 'N','UserName');" >
										<%=LC.L_Ok_Upper%><%--OK--%>
									</button>
									<button onClick="window.history.back();" name="return" type="button">
										<%=LC.L_Cancel%><%--Cancel--%>
									</button>
								</td>
							</tr>
						</table>					
					<%
					return;
				
				
		} else if(countUserMails>0 && flagEmailNew.equals("")&&false) {


				%>
				
					<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>
					<p class = "successfulmsg" align = center> <%=MC.M_UsrEmailAldy_WantOverride%><%--The User Email ID you have entered already exists. Do you want to over-ride?*****--%></p>
						<table align=center>
							<tr width="100%"> 
								<td width="100%" >
									<button name="Continue" onClick="return confirmContinue(document.nsuserdetails, 'N','Email');" >
										<%=LC.L_Ok_Upper%><%--OK--%>
									</button>
									<button onClick="window.history.back();" name="return" type="button">
										<%=LC.L_Cancel%><%--Cancel--%>
									</button>
								</td>
							</tr>
						</table>					
					<%
					return;
		} else {


		ret = usr.createUser();
		}
	
	}

MoreDetailsJB mdJB = new MoreDetailsJB();
mdJB.saveMoredetails("user",request,usr.getUserId(),usr1);

if(!"".equals(nLevel) && mode.equals("M")){
	   mdJB.saveMoredetails("user_"+nLevel,request,networkUsrId,usr1);
}

if (ret ==0) {
%>
<br>
<br>
<br>

<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
<% 

} //end of if for count
String viewList= "NS";

if("networkTabs".equals(from)){%>
<script type="text/javascript">
 window.opener.location.reload();
	setTimeout("self.close()",500);
	</script>
	<%}else{
%>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=accountbrowser.jsp?srcmenu=<%=src%>&cmbViewList=<%=viewList%>">
<%}

}// end of if body for e-sign 
%>
	</Form>
	<%
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</BODY>

</HTML>
