<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.velos.eres.service.util.LC" %>

<%--Include JSPs --%>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<jsp:include page="velos_includes.jsp"></jsp:include>

<jsp:include page="patientDetailsQuickInclude.jsp" flush="true"/>
<jsp:include page="morePerDetailsInclude.jsp" flush="true"/>
<%----%>
<div id="submitFailedDialog" title="<%=LC.L_Error%>" class="comments" style="display:none; overflow:auto">
	<table align="center">
		<tr><td height="20px">&nbsp;</td></tr>
		<tr align="center">
			<td id="errorMessageTD"></td>
		</tr>
		<tr><td height="20px">&nbsp;</td></tr>
		<tr align="center">
			<td>
				<button onclick='$j("#submitFailedDialog").dialog("close");'><%=LC.L_Close%></button>
			</td>
		</tr>
	</table>
</div>
<script>
var patientEnrollScreenFunctions = {
	validatePatientEnrollScreen: {}
};

</script>

<script type="text/javascript">
var isValidatedForm = false;
patientEnrollScreenFunctions.validatePatientEnrollScreen = function() {
	//Call all the validate functions one-by-one
	if(!patientDetailsQuickFunctions.validate()){
		return false;
	}

	if (!morePatientDetFunctions.validate()){ 
		return false;
	}
	
	if (isValidatedForm) { return false; }
	isValidatedForm = true;
	
	$j.post('updatePatientEnrollScreen',
		$j('#patientEnrollForm').serialize(),
		function(data) {
			var errorMap = data.errorMap;
			var hasErrors = false;
			for (var key in errorMap) {
				hasErrors = true;
				isValidatedForm = false;
				$j('#errorMessageTD').html(errorMap[key]);
				break;
			}
			if (hasErrors) {
				$j('#submitFailedDialog').dialog({
					modal:true,
					closeText: '',
					close: function() {
						$j("#submitFailedDialog" ).dialog("destroy");
					}
				});
			} else {
				$("patientEnrollForm").onSubmit=null;
				$("patientEnrollForm").action ="genericMessage.jsp?messageKey=M_Data_SvdSucc&id=6";
				$("patientEnrollForm").submit();
				$("patientEnrollForm").action = '#';
				$("patientEnrollForm").onSubmit = "return patientEnrollScreenFunctions.validatePatientEnrollScreen();";
			}
		}
	);
	
	/*$("patientEnrollForm").onSubmit=null;
	$("patientEnrollForm").action ="updatePatientEnrollScreen";
	$("patientEnrollForm").submit();
	$("patientEnrollForm").action = '#';
	$("patientEnrollForm").onSubmit = "return patientEnrollScreenFunctions.validatePatientEnrollScreen();";*/

	return false;
};
</script>


<%--Custom JS inclusion --%>
<script type="text/javascript" src="./js/velosCustom/patientEnrollScreen.js"></script>
<%----%>