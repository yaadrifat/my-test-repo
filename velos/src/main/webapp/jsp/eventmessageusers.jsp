<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:useBean id="userjB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="addressUserB" scope="page" class="com.velos.eres.web.address.AddressJB" />
<html>

<head>
<style>
.img-right img {
    float: right;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	

<%

String userType = request.getParameter("userType");

if (userType.equals("B")) {%> 

<title><%=LC.L_Bgt_Users%><%--Budget Users*****--%></title>

<%} else {%>

<title><%=LC.L_Evt_UserPage%><%--Event User Page*****--%></title>

<%}%>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>



<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>



<SCRIPT language="JavaScript1.2">

function setVals(formObj) {


    
	if (!validate(formObj))

	{

	return false;

	}

	else

	{

	var usersDel="", userlist="";

	totrows = formObj.totrows.value;

	if (totrows == '1'){

		if (document.all) {

			if (formObj.del.checked) usersDel = formObj.userId.value +";" ;

			else {

				

			userlist = formObj.userName.value +";";} 
			

		}

		else

		{
            
			if (!(formObj.del.checked))		 userlist = formObj.userName.value +";";  
			          

			else  {
			     	usersDel = formObj.userId.value +";" ; }	}		

	}else{  

		for (cnt=0; cnt< totrows; cnt++){

			if (formObj.del[cnt].checked) usersDel = usersDel + formObj.userId[cnt].value +";" ;

			else userlist = userlist + formObj.userName[cnt].value +";";

		}

	}

	formObj.delusers.value = usersDel;

 	formObj.userlist.value = userlist;

    
	//formObj.submit();

	return true;

	}

}



function  validate(formobj){    

  

     if (!(validate_col('e-Signature',formobj.eSign))) return false



<%-- 	if(isNaN(formobj.eSign.value) == true) {

		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

	formobj.eSign.focus();

	return false;

     } --%>

	 return true;

	 

}

</Script>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>



</head>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.EventInfoDao,com.velos.eres.service.util.*"%>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="codelstB" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>

<jsp:useBean id ="budgetUsersB" scope="request" class="com.velos.esch.web.budgetUsers.BudgetUsersJB"/>

<jsp:useBean id="budgetUsersDao" scope="request" class="com.velos.esch.business.common.BudgetUsersDao"/>


<jsp:include page="include.jsp" flush="true"/>


<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>





<% 

	String src = request.getParameter("srcmenu");

	String duration = request.getParameter("duration");   

	String protocolId = request.getParameter("protocolId");   

	String calledFrom = request.getParameter("calledFrom");   

	String eventId = request.getParameter("eventId");

	String mode = request.getParameter("mode");

	String fromPage = request.getParameter("fromPage");

	

	String budgetId =  request.getParameter("budgetId");
	String eventName = "";
	
	eventName =  request.getParameter("eventName");

%>

	

<body>

<DIV id="div1"> 

<%





	HttpSession tSession = request.getSession(true); 



	if (sessionmaint.isValidSession(tSession))

	{
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<% 	
	   String uName = (String) tSession.getValue("userName");



  	   ArrayList eventUserIds = null;

	   ArrayList userIds = null;

	   ArrayList userTypes = null;

	   ArrayList eventUserDesc = null;

	   

	   ArrayList bgtUserIds=null; 

	   ArrayList bgtUsrLastNames=null;

	   ArrayList bgtUsrFirstNames=null;

		  	   

	   int rows = 0;



	   String userId = "";

	   String userLName = "";	

	   String userFName = "";	

	   String userName = "";	

	   String bgtUserId = "";

   	   String eventUserId = "";

	   String usrType = "";
	   String firstname = "";
	   String lastname = "";
	   String midname = ""	;
	   String logname = ""		;
	   String email = "";
	   String userMoreIdDetails = "";
	   

	   if (userType.equals("B")) { //called from Budget

  		  budgetUsersDao= budgetUsersB.getUsersInBudget(EJBUtil.stringToNum(budgetId));

		  bgtUserIds=budgetUsersDao.getBgtUsers(); 

		  bgtUsrLastNames=budgetUsersDao.getUsrLastNames();

		  bgtUsrFirstNames=budgetUsersDao.getUsrFirstNames();

		  rows = bgtUserIds.size();

		}

		else {

		   EventInfoDao eventinfoDao = eventdefB.getEventDefInfo(EJBUtil.stringToNum(eventId));

  		   eventUserIds = eventinfoDao.getEventUserIds();

		   userIds = eventinfoDao.getUserIds();

		   userTypes = eventinfoDao.getUserTypes();

		   CodeDao codeDao = new CodeDao(); 

		   codeDao = codelstB.getDescForEventUsers(userIds, userTypes);

		   eventUserDesc = codeDao.getCDesc();
		   

		   rows = eventUserDesc.size();

		}



%>  

<%if(userType.equals("B")) {%>

   <P class="sectionHeadings"> <%=MC.M_BgtAccRgt_UsrBgt%><%--Budget Access Rights >> Users for Budget*****--%> </P>

<%

} else {
	if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary")  )
	{
	%>
	<P class="sectionHeadings"> <%=MC.M_EvtLibEvt_UsrMail%><%--Event Library >> Event Message >> Users for Mail*****--%> </P>
	<%
	} else {
		String calName = (String) tSession.getValue("protocolname");
		Object[] arguments = {calName};
	%>
	<P class="sectionHeadings"> <%=VelosResourceBundle.getMessageString("M_PcolCalEvtMsg_UsrMail",arguments)%><%--Protocol Calendar [ <%=calName%> ] >> Event Message >> Users for Mail*****--%> </P>
	<%}
}%>



<%

if(rows > 0) {

%>

  <Form name="user" id="evtMsgUser" method="post" action="removemsgusers.jsp" onSubmit="if (setVals(document.user)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

   <P class="defComments"> <%=MC.M_SelUsr_WantToDel%><%--Select the users you want to delete.*****--%></P>



   <Input type=hidden name=userlist>

   <Input type=hidden name=delusers>

	  

    <table width="80%" cellspacing="0" cellpadding="0" border=0 >

      <tr> 

        <th width="80%"> <%=LC.L_User_Name%><%--User Name*****--%> </th>

        <th width="5%"> </th>

      </tr>



<%

	int cnt =0;

   for(int i = 0;i<rows;i++){

     if (userType.equals("B")) { //from Budget

	   bgtUserId = (String) bgtUserIds.get(i); 	

	   userLName =(String) bgtUsrLastNames.get(i);

   	   userFName =(String) bgtUsrFirstNames.get(i);

	   userName = userFName + " " +userLName;

		/*For User More Information Idenifier */
	    userjB.setUserId(EJBUtil.stringToNum(bgtUserId));
		userjB.getUserDetails();
		firstname=userjB.getUserFirstName();
		lastname=userjB.getUserLastName();
		midname=userjB.getUserMidName()==null?"":userjB.getUserMidName();
		logname=userjB.getUserLoginName();
		addressUserB.setAddId(EJBUtil.stringToNum(userjB.getUserPerAddressId()));
		addressUserB.getAddressDetails();
		email = addressUserB.getAddEmail();
		email = ( email == null )?"":(email);//JM
		userMoreIdDetails = firstname +" "+ midname +" "+ lastname +" "+ email+" "+logname;
	/*For User More Information Idenifier */
	
		if ((cnt%2)==0) { 

 %>

	      <tr class="browserEvenRow"> 

  <%

		}

		else{

%>

      <tr class="browserOddRow"> 

<%

		}

  %>

       <td class="img-right" > <%=userName%><img src="./images/View.gif" onmouseover="return overlib(htmlEncode('<%=userMoreIdDetails.replace("'", "\\'")%>'),ABOVE,CAPTION,'');" onmouseout="return nd();" border="0" align="left" /></td>

		<td><Input type="checkbox" name=del></td>

		<Input type="hidden" name=userId value=<%=bgtUserId%>>

		<Input type="hidden" name=userName value="<%=userName%>"> 

      </tr>

<%

	 cnt ++;

	 } else {

	   eventUserId = (String) eventUserIds.get(i); 	

	   userId = (String) userIds.get(i);
	   
	/*For User More Information Idenifier */
	    userjB.setUserId(EJBUtil.stringToNum(userId));
		userjB.getUserDetails();
		firstname=userjB.getUserFirstName();
		lastname=userjB.getUserLastName();
		midname=userjB.getUserMidName()==null?"":userjB.getUserMidName();
		logname=userjB.getUserLoginName();
		addressUserB.setAddId(EJBUtil.stringToNum(userjB.getUserPerAddressId()));
		addressUserB.getAddressDetails();
		email = addressUserB.getAddEmail();
		email = ( email == null )?"":(email);//JM
		userMoreIdDetails = firstname +" "+ midname +" "+ lastname +" "+ email+" "+logname;
	/*For User More Information Idenifier */
		
	   usrType = (String) userTypes.get(i);

	   userName	=(String) eventUserDesc.get(i);

	  if (usrType.equals("S")){

		if ((cnt%2)==0) { 

 %>

	      <tr class="browserEvenRow"> 

  <%

		}

		else{

%>

      <tr class="browserOddRow"> 

<%

		}

  %>

       <td  class="img-right" > <%=userName%> <img src="./images/View.gif" onmouseover="return overlib(htmlEncode('<%=userMoreIdDetails.replace("'", "\\'")%>'),ABOVE,CAPTION,'');" onmouseout="return nd();" border="0" align="left" /> </td>

		<td><Input type="checkbox" name=del></td>

		<Input type="hidden" name=userId value=<%=eventUserId%>>

		<Input type="hidden" name=userName value="<%=userName%>"> 

      </tr>

<%

	cnt ++;

	}

	}

   } //end for



%>

   <Input type=hidden name=totrows value=<%=cnt%>>
      

   <tr height=10><td colspan=2 height=10></td></tr>

 </table>

<Input type=hidden name=fromPage value=<%=fromPage%>>
<Input type=hidden name=protocolId value=<%=protocolId%>>
<Input type=hidden name=eventId value=<%=eventId%>>

	
<%  if(! userType.equals("B")) { %>

		<jsp:include page="propagateEventUpdate.jsp" flush="true"> 
		
		<jsp:param name="fromPage" value="<%=fromPage%>"/>
		
		<jsp:param name="formName" value="user"/>
		
		<jsp:param name="eventName" value="<%=eventName%>"/>
		
		</jsp:include>   
<% } %>

<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="evtMsgUser"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
  </Form>

<%

} else { // else of if for rows>0

%>

<P class = "defComments"> 

<%=MC.M_NoUsrAttached_ToEvt%><%--No users currently attached to this event*****--%>

</P> 

  <%

} // end of if for rows>0

}//end of if body for session



else



{



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%



}



%>



</div>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</body>

</html>


 




