<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.velos.eres.service.util.CFG"%>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_StdGtSumm%><%--Study >> Summary*****--%></title>

<style>
span.errorMessage { color:red; height:1.5em; text-align:right; }
</style>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="accountB" scope="page" class="com.velos.eres.web.account.AccountJB" />
<jsp:useBean id="studyJB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB" %>

<!-- KM : import the objects for customized field -->
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>

<%
HttpSession tSession = request.getSession(true);
if (!sessionmaint.isValidSession(tSession))
	return;
%>

</head>
<% String src;
src= request.getParameter("srcmenu");
String mode ="";
mode = request.getParameter("mode");
String from = "default";

int studyId = 0;
if (mode.equals("M")) {
   	studyId = StringUtil.stringToNum(request.getParameter("studyId"));
}
String selectedTab = request.getParameter("selectedTab");
%>
<%

int pageRight = 0;
int stdRight =0;

HashMap sysDataMap = studyJB.getStudySysData(request);
if (null == sysDataMap)
	return;	

String autoGenStudy = (String) sysDataMap.get("autoGenStudy");
tSession.setAttribute("autoGenStudyForJS",autoGenStudy);

int grpRight = 0;
grpRight = StringUtil.stringToNum(sysDataMap.get("grpRight").toString());
pageRight = StringUtil.stringToNum(sysDataMap.get("pageRight").toString());

tSession.setAttribute("studyId",sysDataMap.get("studyId"));
tSession.setAttribute("studyNo",sysDataMap.get("studyNo"));

if (mode.equals("M"))
{
	String roleCodePk = (String) sysDataMap.get("studyRoleCodePk");
	tSession.setAttribute("studyRoleCodePk",roleCodePk);

	StudyRightsJB stdRights = (StudyRightsJB) sysDataMap.get("studyRights");
	tSession.setAttribute("studyRights",stdRights);
	if(sysDataMap.get("stdSummRight")!=null){
	stdRight = StringUtil.stringToNum((sysDataMap.get("stdSummRight").toString()));
	tSession.setAttribute("studyRightForJS",stdRight);
	}
	pageRight = StringUtil.stringToNum((sysDataMap.get("pageRight").toString()));

} else {
	tSession.setAttribute("StudyRights",null);
}

%>

<%--Include JSP --%>
<jsp:include page="studyScreenInclude.jsp" flush="true"/>
<%----%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<jsp:include page="ui-include.jsp" flush="true"></jsp:include>

<body style="overflow:hidden;">
<%
tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
{
	String accId = (String) tSession.getAttribute("accountId");

	String stid= request.getParameter("studyId");
%>	
<DIV class="BrowserTopn" id="divTab">
	<jsp:include page="studytabs.jsp" flush="true">
	<jsp:param name="from" value="<%=from%>"/>
	<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
	<jsp:param name="studyId" value="<%=studyId%>"/>
	</jsp:include>
</DIV>
<SCRIPT LANGUAGE="JavaScript">
var screenWidth = screen.width;
var screenHeight = screen.height;
<%if(stid==null || stid==""){%>
<!-- Bug#15375 Fix for 1360*768 resolution- Tarun Kumar -->
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="BrowserBotN BrowserBotN_S_1" id="div1"  style="overflow-x:hidden; height:77%;">')
	else
		document.write('<DIV class="BrowserBotN BrowserBotN_S_1" id="div1"  style="overflow-x:hidden;">')
<%} else { System.out.println("Inside else");%>

/* Condition to check configuration for workflow enabled */
<% if("Y".equals(CFG.Workflows_Enabled)&& (stid!=null && !stid.equals("") && !stid.equals("0")) && !("LIND".equals(CFG.EIRB_MODE) || "Y".equals(CFG. FLEX_MODE))){ %>
		if(screenWidth>1280 || screenHeight>1024)
			document.write('<DIV class="BrowserBotN BrowserBotN_S_3 workflowDivBig" id="div1" style="overflow-x:hidden; height:77%;" >')
		else
			document.write('<DIV class="BrowserBotN BrowserBotN_S_3 workflowDivSmall" id="div1" style="overflow-x:hidden;" >')
<% } else{ %>
		if(screenWidth>1280 || screenHeight>1024)
			document.write('<DIV class="BrowserBotN BrowserBotN_S_3" id="div1" style="overflow-x:hidden; height:77%;" >')
		else
			document.write('<DIV class="BrowserBotN BrowserBotN_S_3" id="div1" style="overflow-x:hidden;" >')
<% } %>
	
<%}%>
</SCRIPT>
<%if ((mode.equals("M") && (StringUtil.isAccessibleFor(pageRight, 'E')  || StringUtil.isAccessibleFor(pageRight, 'V'))) 
			|| (mode.equals("N") && StringUtil.isAccessibleFor(pageRight, 'N'))){ %>
<form name="studyScreenForm" id="studyScreenForm" method="post" action="#" onSubmit="return studyScreenFunctions.validateStudyScreen();">
<input type="hidden" name="formStudy" value="<%=studyId %>" />
<%
  	boolean hasAccess = false;
	String uName = (String) tSession.getAttribute("userName");
	String userIdFromSession = (String) tSession.getAttribute("userId");
	String acc = (String) tSession.getAttribute("accountId");
	 %>
	<br>
	<div style="border:1">
	<div id="studySection1" name="studySection" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
		<div id="studyTab1content" onclick="toggleDiv('studyTab1')" class="portlet-header portletstatus ui-widget 
		ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" style="background-color:#CCCCCC;">
			<span class="ui-icon ui-icon-triangle-1-s"></span>
			<%=LC.L_StdSummary%>
		</div>		
		<div id="studyTab1" style="width:99%">
			<jsp:include page="studySummary.jsp" >
				<jsp:param name="studyId" value="<%=studyId%>"/>
				<jsp:param name="srcmenu" value="<%=src%>"/>
				<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
				<jsp:param name="from" value="1"/>
				<jsp:param name="includeMode" value="Y"/>
			</jsp:include>
		</div>
	</div>
	<div id="studySection2" name="studySection" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
		<div id="studyTab2content" onclick="toggleDiv('studyTab2');" class="portlet-header portletstatus ui-widget 
		ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" style="background-color:#CCCCCC;">
			<span class="ui-icon ui-icon-triangle-1-s"></span>
			<%=LC.L_More_StdDets%>
		</div>
		<div id='studyTab2' style="width:99%">
			<jsp:include page="newStudyIds.jsp" >
				<jsp:param name="studyId" value="<%=studyId%>"/>
				<jsp:param name="studyRights" value="<%=stdRight%>"/>
				<jsp:param name="srcmenu" value="<%=src%>"/>
				<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
			</jsp:include>
		</div>
	</div>
	</div>
	<%if ((mode.equals("N") && StringUtil.isAccessibleFor(pageRight, 'N')) || (mode.equals("M") && StringUtil.isAccessibleFor(pageRight, 'E'))){%>
	<br>
	<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="studyForm"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
	<%}%>
	<div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
	<div class ="mainMenu" id="emenu">
	  	<jsp:include page="getmenu.jsp" flush="true"/>
	</div>
</form>
</DIV>
<%
		} //end of if body for page right
		else{
		%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
		<%
	}
}//end of if body for session
  	else
{
%>
<jsp:include page="studytabs.jsp" flush="true">
<jsp:param name="from" value='<%=from%>'/>
<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
<jsp:param name="studyId" value="<%=studyId%>"/>
</jsp:include>

<jsp:include page="timeout.html" flush="true"/>
<%
}
%>
 <div>
 <jsp:include page="bottompanel.jsp" flush="true"/>
 </div>


</body>
</html>

