<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id="repB" scope="request" class="com.velos.eres.web.report.ReportJBNew"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<%-- Nicholas : Start --%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%-- Nicholas : End --%>
<style>
html, body { overflow: visible; }
</style>

<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,javax.xml.transform.TransformerFactory,javax.xml.transform.Transformer,javax.xml.transform.Source,javax.xml.transform.stream.StreamSource,javax.xml.transform.stream.StreamResult,javax.xml.transform.TransformerException,javax.xml.transform.TransformerConfigurationException,java.io.*,java.text.*,java.net.URL,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.*"%>

<%
	String repDate="";
	String repTitle="&nbsp;";
	String coverage_type_label = "";
	String repName=request.getParameter("repName");
	String dtFrom=request.getParameter("dateFrom");
	String dtTo=request.getParameter("dateTo");
	String selSiteIds = request.getParameter("selSiteIds");
	String budgetPk = request.getParameter("budgetPk");
	
	String calledfrom = request.getParameter("calledfrom");
	/*Custom label processing*/
	int startIndx =0;
	int stopIndx =0;
	String labelKeyword ="";
	String labelString ="";
	String messageKeyword ="";
	String messageString ="";
	String interimXSL = "";
	String messageKey ="";
	String tobeReplaced ="";
	String messageParaKeyword []=null;
	String displayMode = request.getParameter("displayMode");	
	if (StringUtil.isEmpty(displayMode))
	{
		displayMode = "";
	}
	
	String repArgs = "";
	String repArgsDisplay = "";
	String monthString = null;
	String filterType = request.getParameter("filterType");
	
	String month = request.getParameter("month");
	String year = null;
	String format = "";
	
	if(filterType == null) filterType = "";	

if(filterType.equals("2")) { //Year
		year = request.getParameter("year");
		if (!(year.equals(""))) {
 			
			dtFrom = DateUtil.getFormattedDateString(year,"01","01");
			dtTo = DateUtil.getFormattedDateString(year,"31","12");
		}
	}

	if(filterType.equals("3")) { //Month
		year = request.getParameter("year1");
		
		if (!(year.equals(""))) {
 			dtFrom = DateUtil.getFormattedDateString(year,"01",month);
			 
		}
 		
		dtTo = DateUtil.getFormattedDateString(year,String.valueOf(DateUtil.getMonthMaxNumberOfDays(month,year)),month);
	}

	if(filterType.equals("1")) { //All Dates
 		
		dtFrom = DateUtil.getFromDateStringForNullDate();
		dtTo = DateUtil.getToDateStringForNullDate();
	}
	
 	
	if(dtFrom == null) dtFrom = "";
	dtFrom = dtFrom.trim();			

	if (!(dtFrom.equals(""))) {
		 
		if(filterType.equals("1")) { //All Dates	
			repArgsDisplay = LC.L_All;/*repArgsDisplay = "ALL";*****/	
		}
		else {
			repArgsDisplay = dtFrom +" "+LC.L_To+" " + dtTo ;/*repArgsDisplay = dtFrom + " To " + dtTo ;*****/			
		}

		repArgs= dtFrom + ":" + dtTo;
	
	}

	int repId = EJBUtil.stringToNum(request.getParameter("repId"));

	int repXslId = 0;

	String dispStyle=request.getParameter("dispStyle");	
	String studyPk=request.getParameter("studyPk");
	String protId=request.getParameter("protId");
	String formId=request.getParameter("formId");
	
	//only added for report 107
	String showSelect = request.getParameter("showSelect");
	String mockSchCondition = "T";
	
	if (StringUtil.isEmpty(showSelect))
	{
		showSelect = "0";
	}
	
	if (showSelect.equals("1"))
	{
			mockSchCondition = "F"; 
	}
	else
	{
			mockSchCondition = "T";
	}
	
%>
<%		
	String patid=request.getParameter("id");	


	String params="";	

	String hdr_file_name="";
	String ftr_file_name="";
	String filePath="";
	String hdrfile="";
	String ftrfile="";	
	String hdrFilePath="";
	String ftrFilePath="";
	String fileName="";
	String xml=null;
	boolean formReport = false;

	HttpSession tSession = request.getSession(true); 


   if (sessionmaint.isValidSession(tSession))
   {
	try{	   
%>
<jsp:include page="sessionlogging.jsp" flush="true"/> 

<%
	String uName =(String) tSession.getValue("userName");	 
	int acc = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));
	String accountId= (String) (tSession.getValue("accountId"));
	int userId = EJBUtil.stringToNum((String) (tSession.getValue("userId")));
	userB = (UserJB)tSession.getValue("currentUser");
	String FilledFormId = "";
%>
<input type="hidden" id="sessUserId" value="<%=userId%>">
<input type="hidden" id="sessAccId" value="<%=accountId%>">
<%
	int pageRight = 0; 
	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		
    pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("REPORTS"));
   
    	
	switch (repId) {
	
		case 1: //Active/Enrolling Protocols 
			params = (accountId + ":" + repArgs ) ;
			repXslId = repId;
			break;

		case 2: //Protocols by user
			String uId=request.getParameter("id"); 
			params = (uId + ":" + repArgs ) ;
			repXslId = repId;
			break;
			
		case 4: //All Account Users
			params = (accountId) ;
			repXslId = repId;
			repArgsDisplay = "";			
			break;			
			
		case 5: //Protocols by Therapeutic Area 
			String tArea=request.getParameter("tArea");			
			params = (accountId + ":" + tArea + ":" + repArgs ) ;
			repXslId = repId;
			break;						

		case 6: //Public Protocols
			params = (accountId + ":" + repArgs) ;
			repXslId = repId;
			break;
			
		case 7: //Protocols by Organization
			String orgn=request.getParameter("orgn");			
			params = orgn + ":" + repArgs;
			repXslId = repId;
			break;
							
		case 8: //Complete Protocol	
			params = studyPk ;
			repXslId = repId;
			break;									
			
		case 11: //Patient Events (Done)

			params = (patid + ":" + studyPk + ":" + repArgs) ;
			repXslId = repId;
			break;

		case 10: //Patient Schedule

			params = (patid + ":" + studyPk + ":" + repArgs) ;
			repXslId = repId;
			break;

		case 93: //Patient Timeline

			params = (patid + ":" + studyPk + ":" + repArgs) ;
			repXslId = repId;
			break;

		case 94: //Study Visit Calendar
			
			params = (studyPk + ":" + repArgs + ":" + selSiteIds  ) ;
			repXslId = repId;
			break;

		case 95: //Patient Visit Calendar
			params = (patid + ":" + repArgs + ":" + studyPk) ;
			repXslId = repId;
			break;

		case 96: //Patient Visit Calendar
			params = (patid + ":" + repArgs + ":" + studyPk) ;
			repXslId = repId;
			break;

		case 97: //Study Dump
			params = (accountId + ":" + userId) ;
			repXslId = repId;
			break;

		case 98: //Study Dump in Data Safety Monitoring
			params = (accountId + ":" + userId) ;
			repXslId = repId;
			break;

		case 103: //Patient Dump in Data Safety Monitoring
			params = (userId + ":" + repArgs) ;
			repXslId = repId;
			break;

		case 99: //Form Audit - Patient
			FilledFormId=request.getParameter("repFilledFormId");	
			params = FilledFormId ;
			repXslId = repId;
			formReport= true;
			break;

		case 100: //Form Audit - AccountForms 
			FilledFormId=request.getParameter("repFilledFormId");	
			params = FilledFormId ;
			repXslId = 99;
			formReport=true;
			break;

		case 101: //Form Audit - StudyForms
			FilledFormId=request.getParameter("repFilledFormId");	
			params = FilledFormId ;
			repXslId = 99;
			formReport=true;
			break;

		case 102: //Form Audit - CRFForms
			FilledFormId=request.getParameter("repFilledFormId");	
			params = FilledFormId ;
			repXslId = 99;
			formReport=true;
			break;

		case 104: //Study Dump in Data Safety Monitoring
			params = (accountId + ":" + repArgs) ;
			repXslId = repId;
			break;

		case 106: //Protocol Calendar Template
			params = (protId) ;
			repXslId = repId;
			coverage_type_label=VelosResourceBundle.getLabelString("L_Coverage_Type");
			break;			
		case 107: //Mock Schedule
			params = (protId) ;
			repXslId = repId;
			coverage_type_label=VelosResourceBundle.getLabelString("L_Coverage_Type");
			break;			
		case 108: //Budget by Category
			params = (budgetPk + ":" + protId);
			repXslId = repId;
			break;									
			
		case 109: //Budget by Standard of Care
			params = (budgetPk + ":" + protId);
			repXslId = repId;
			break;									
		case 110: //Budget by Visit
			params = (budgetPk + ":" + protId);
			repXslId = repId;
			break;									
		case 124: //Budget - Overview
			params = (budgetPk + ":" + protId);
			repXslId = repId;
			break;									
		case 125: //Form Metadata
			params = (accountId + ":" + formId);
			repXslId = repId;
			formReport=true;
			break;									
		case 111: //Study Budget by Section
			params = (budgetPk);
			repXslId = repId;
			break;									
		case 112: //Study Budget by Category
			params = (budgetPk);
			repXslId = repId;
			break;									
		case 12: // Patient Events (To Be Done)

			params = (patid + ":" + studyPk + ":" + repArgs) ;
//out.println("params " + params + " repId " + repId);
			repXslId = repId;
			break;

		case 13: //All Patients Event List 			
			if (dispStyle.equals("P")) { //patient wise
				params = (studyPk + ":" + repArgs + ":" + "Person_code: start_date_time:"+selSiteIds) ;
				repXslId = repId;
			}
			else if (dispStyle.equals("D")) { //datewise
				params = (studyPk + ":" + repArgs + ":" + "start_date_time:Person_code:"+selSiteIds) ;
				repXslId = repId+1;
			}
			break;

			
		case 15: //Summary for Public Protocol	
			params = studyPk ;
			repXslId = repId;
			break;									
					
		case 16: //All Patients Enrolled to a Study
		params = (studyPk + ":" + repArgs + ":" + selSiteIds) ;
		if (dispStyle.equals("P")) {
			repXslId = repId;
		}
		else if (dispStyle.equals("D")) {
			repXslId = repId+1;
		}

		//out.println("params " + params + " repId " + repId);
			
		break;

		case 21: //Estimated Study Budget Report
			params = (protId + ":" + studyPk + ":" + repArgs) ;
//out.println("params " + params + " repId " + repId);
			repXslId = repId;
			break;
			
		case 23: //Study Team	
			params = studyPk ;
			repXslId = repId;
			break;									

		case 24: //Protocol Tracking	
			params = studyPk ;
			repXslId = repId;
			break;									
							
		case 25: //Assigned External Users	
			params = studyPk ;
			repXslId = repId;
			break;									
				
		case 43: //User Profile
			String selUser=request.getParameter("id");
			params = (selUser) ;
			repXslId = repId;
			break;			

		case 44: //Protocol Calendar Template
			params = (protId + ":" + studyPk) ;
//out.println("params " + params + " repId " + repId);
			repXslId = repId;
			
			break;			

		case 89: //All Patient Associated Cost(By Cost Type)
		case 90: //All Patient Associated Cost(Bill)
			params = (protId + ":" + repArgs + ":" + selSiteIds) ;
			repXslId = repId;
			break;
			
		case 45: //All Patients Associated Cost(Done Events)
			params = (protId + ":" + repArgs + ":" + selSiteIds) ;
			if (dispStyle.equals("D")) { //date wise
				repXslId = repId;
			}
			else if (dispStyle.equals("P")) {//patient wise
				repXslId = repId+1;
			}

			break;			

		case 47: //Patient Associated Cost(Done Events)
			params = (protId + ":" + repArgs + ":" + patid) ;
//out.println("params " + params + " repId " + repId);
			repXslId = repId;
			break;			
			
		case 65: //Patient Event/CRF Status
		case 66: //Patient CRF Tracking
			params = (patid + ":" + studyPk + ":" + repArgs) ;
			repXslId = repId;
			break;	
		case 105:
			params = (studyPk + ":" + protId + ":" + selSiteIds ) ;
			repXslId = repId;
			break;	
			
		case 79: //Study Enrollment (All Sites)
			params = studyPk + ":" + repArgs;
			repXslId = repId; 
			break;						
		case 80: //Study Enrollment by Race/Gender -- Not in use
		case 116: //Study Enrollment by Race/Gender	
			params = studyPk + ":" + repArgs;
			repXslId = repId; 
			break;						
		case 117: //Enrollment Summary by Race/Gender/Ethnicity
			params = accountId + ":" + userId + ":" + repArgs ;
			repXslId = repId;
			break;
		case 81: //Adverse Event Tracking
			params = studyPk + ":" + repArgs;
			repXslId = repId; 
			break;						
		case 82: //Active Enrolling Protocols
			params = (accountId + ":" + repArgs ) ;			
			repXslId = 1; //Same report is being shown at two places, one xsl
			break;
		case 83: //Study Enrollment (Site Specific)
			orgn=request.getParameter("orgId");	
			params = (studyPk + ":" + repArgs + ":" + orgn) ;
			repXslId = repId; //Same report is being shown at two places, one xsl
			break;
		case 84: //Protocol Compliance (Site Specific)
			orgn=request.getParameter("orgId");	
			params = studyPk + ":" +  orgn + ":" + repArgs;
			repXslId = repId; 
			break;						
		case 86: //Deaths by Study
		case 85: //Adverse Events by Study
			orgn=request.getParameter("orgId");	
			params = orgn + ":" +studyPk+ ":" + repArgs;
			repXslId = repId; 
			break;		
		case 115: //Adverse Events Summary
			orgn=request.getParameter("orgId");	
			params = studyPk+ ":" + orgn + ":" +repArgs;
			repXslId = repId; 
			break;		
		case 87: //Protocols by Organization							
			orgn=request.getParameter("orgId");	
			params = orgn + ":" + repArgs;
			repXslId = 7; 
			break;
		case 88: //Adverse Events by Patient 
			params = (studyPk + ":" + patid + ":" + repArgs) ;
			repXslId = repId;
			break;
		case 91: //Summary 3 - Accrual to Therapeutic Protocols 
			params = (accountId + ":" + repArgs ) ;
			repXslId = repId;
			break;
		case 113: //Summary 4 - Clinical Research Protocols Multi-site
			params = (accountId + ":" + repArgs ) ;
			repXslId = repId;
			break;
		case 114: //NCI Summary 3
			params = (accountId + ":" + repArgs ) ;
			repXslId = repId;
			break;
		case 92: //Summary 4 - Clinical Research Protocols
			params = (accountId + ":" + repArgs ) ;
			repXslId = repId;
			break;
			
		case 5000: //External Adverse Event Log Report
			params = (studyPk + ":" + request.getParameter("id") + ":" + repArgs ) ;
			repXslId = repId;
			break;
		case 5001: //EMSI
			params = acc + ":" + (request.getParameter("param1") + ":" + request.getParameter("param2")+ ":" + request.getParameter("yalewindow")+ ":" + request.getParameter("emsiwindow"))  ;
			repXslId = repId;
			break;

		case 5002: //Yale
			params = acc + ":" + (request.getParameter("param1") + ":" + request.getParameter("param2") + ":" + request.getParameter("param3") + ":" + request.getParameter("yalewindow")+ ":" + request.getParameter("emsiwindow"))  ;
			repXslId = repId;
			break;

		case 5005: //1 Month Turnover Report
			params = (accountId + ":" + repArgs ) ;
			repXslId = repId;
			break;

		case 5004: //6 Month Turnover Report
			params = (accountId + ":" + repArgs ) ;
			repXslId = repId;
			break;

		case 5006: //Yale patients 12 m
			params = (accountId + ":" + repArgs ) ;
			repXslId = repId;
			break;

		case 118: //Form CSV
			params = request.getParameter("formId") + ":" + acc + ":" + request.getParameter("mode") + ":" + request.getParameter("delimiter")  ;
			repXslId = repId;
			formReport=true;
			break;
		case 5010 : //Complete Protocol	
			params = studyPk ;
			repXslId = repId;
			break;									
		

		default:out.println(LC.L_Default_Lower);/*default:out.println("default");*****/	
	}
	
	Calendar now = Calendar.getInstance();
	repDate = DateUtil.getCurrentDate() ;
	
	ReportDaoNew rD =new ReportDaoNew();
	
	//The parameters are separated by :
	//They are segregated in the stored procedure SP_GENXML and then passed to report sql
//System.out.println("**************" + params);	 

	rD=repB.getRepXml(repId,acc,params);
	
	ArrayList repXml = rD.getRepXml();
	if (repXml == null || repXml.size() == 0) { Object[] arguments = {repName}; //no data found
%>
		<br><br><br><br><br><br><br><br><br>
		<P class="sectionHeadings">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=VelosResourceBundle.getMessageString("M_NoDataFnd_Rpt",arguments)%><%--No data found for the Report '<%=repName%>'.*****--%></P>
<%		throw new Exception(VelosResourceBundle.getMessageString("M_NoDataFnd_Rpt",arguments));
	}
//System.out.println("SONIKA "+params);	
//System.out.println("===================================================");	 
//System.out.println(repXml);	 
//System.out.println("===================================================");	 

	rD=repB.getRepXsl(repXslId);
	ArrayList repXsl = rD.getXsls();
		
	Object temp;
	
	String xsl=null;
	

	temp=repXml.get(0);				
	
	if (!(temp == null)) 
	{
		xml=temp.toString();
		//replace the encoded characters
		xml = StringUtil.replace(xml,"&amp;#","&#");
		
	}
	else
	{
		out.println(MC.M_ErrIn_GetXml);/*out.println("Error in getting XML");*****/
		Rlog.fatal("Report","ERROR in getting XML");			
		throw new Exception(MC.M_ErrIn_GetXml);
	}
	
    try{	
		temp=repXsl.get(0);
	} catch (Exception e) {
		out.println(MC.M_ErrIn_GetXsl);/*out.println("Error in getting XSL.");*****/
		Rlog.fatal("Report","ERROR in getting XSL. Record for report " +repId +"does not exist in database.");
		throw new Exception(MC.M_ErrIn_GetXsl);
	}	
	
	if (!(temp == null))	{
		xsl=temp.toString();
		if("userpxd".equals(Configuration.eSignConf)){
			xsl=xsl.replace("VELLABEL[L_Esignature]", "e-Password");
			System.out.println("index-"+xsl.indexOf("VELLABEL[L_Esignature]"));
			xsl=xsl.replaceAll("<input type=\"password\" autocomplete=\"off\" name=\"eSign\" id=\"eSign\" maxlength=\"8\"", "<input type=\"password\" name=\"eSign\" id=\"eSign\" style=\"background:url(./images/ic_lock_outline_gray_24dp.png) no-repeat; padding-left: 25px;background-color:white;\" class=\"input21\" autocomplete=\"off\"");
			xsl=xsl.replace("onkeyup=\"ajaxvalidate('misc:'+this.id,4,'eSignMessage','VELLABEL[L_Valid_Esign]','VELLABEL[L_Invalid_Esign]','sessUserId')\"", "onkeyup=\"ajaxvalidate('pass:'+this.id,-1,'eSignMessage','Valid-Password','Invalid-Password','sessUserId')\"");
		}
	}
	else
	{
		out.println(MC.M_ErrIn_GetXsl);/*out.println("Error in getting XSL.");*****/
		Rlog.fatal("Report","ERROR in getting XSL. Record for report " +repId +"does not exist in database.");
		throw new Exception(MC.M_ErrIn_GetXsl);
	}
	
	if ((xsl.length())==0) {
		out.println(MC.M_ErrIn_GetXsl);/*out.println("Error in getting XSL.");*****/
		Rlog.fatal("Report","ERROR in getting XSL. Empty XSL found in database for report " +repId);
		throw new Exception(MC.M_ErrIn_GetXsl);
	} 

//out.println(protId + "<br>"); 
//out.println(patid + "<br>");
//out.println(studyPk + "<br>");

//out.println(repArgs + "<br>");
//out.println("params " + params + " repId " + repId);

	if ((xml.length())==34) { //no data found
%>
	
		<br><br><br><br><br><br><br><br><br>
		<P class="sectionHeadings">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=MC.M_NoData_ForRpt%><%--No data found for the Report*****--%> '<%=repName%>'.</P>				
<%		throw new Exception(MC.M_NoData_ForRpt);
	}
	
	
	//get hdr and ftr
	//Changed By Deepali
	rD=repB.getRepHdFtr(repId,acc,userId);
	
	byte[] hdrByteArray=rD.getHdrFile();
	byte[] ftrByteArray=rD.getFtrFile();
	String hdrflag, ftrflag;	
	hdr_file_name="temph["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "].jpg";
	Configuration.readReportParam(Configuration.ERES_HOME + "eresearch.xml");
	filePath = Configuration.REPTEMPFILEPATH;
	
	hdrfile=filePath+ "/" + hdr_file_name;
	ftr_file_name="tempf["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "].jpg";
	ftrfile=filePath+ "/" + ftr_file_name;

	hdrFilePath="../temp/"+hdr_file_name;
	ftrFilePath="../temp/"+ftr_file_name;
//out.println(params);		
	//check for byte array
	if (!(hdrByteArray ==null)) 
	{
		hdrflag="1";
		ByteArrayInputStream fin=new ByteArrayInputStream(hdrByteArray);
					
		BufferedInputStream fbin=new BufferedInputStream(fin);
		
		File fo=new File(hdrfile);

		FileOutputStream fout = new FileOutputStream(fo);
		Rlog.debug("3","after output stream");		
		int c ;
		while ((c = fbin.read()) != -1){
				fout.write(c);
			}
		fbin.close();
		fout.close();
		}	
	else
	{
		hdrflag="0";	
	}
		
		
		//check for length of byte array
	if (!(ftrByteArray ==null))
		{	
		ftrflag="1";
		ByteArrayInputStream fin1=new ByteArrayInputStream(ftrByteArray);
		BufferedInputStream fbin1=new BufferedInputStream(fin1);
		File fo1=new File(ftrfile);
		FileOutputStream fout1 = new FileOutputStream(fo1);
		int c1 ;
		while ((c1 = fbin1.read()) != -1){

			fout1.write(c1);
		}
		
		fbin1.close();
		fout1.close();
	}
	else
	{
		ftrflag="0";	
	}

		
	//get the folder name
	Configuration.readAppendixParam(Configuration.ERES_HOME + "eresearch.xml");
	com.aithent.file.uploadDownload.Configuration.readSettings("eres");
	com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "study");		  
	String fileDnPath = com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET ;
	
	Configuration.readReportParam(Configuration.ERES_HOME + "eresearch.xml");
	filePath = Configuration.REPDWNLDPATH;
	Rlog.debug("report", filePath);
	//make the file name
	fileName="reporthtml"+"["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "].html" ;
	//make the complete file name
	String htmlFile = filePath + "/"+fileName;
	response.setContentType("text/html");    

	interimXSL = xsl;
	
	try{
		while(interimXSL.contains("VELLABEL[")){
			startIndx = interimXSL.indexOf("VELLABEL[") + "VELLABEL[".length();
			stopIndx = interimXSL.indexOf("]",startIndx);
			labelKeyword = interimXSL.substring(startIndx, stopIndx);
			labelString = LC.getLabelByKey(labelKeyword);
			interimXSL=StringUtil.replaceAll(interimXSL, "VELLABEL["+labelKeyword+"]", labelString);
		}
		
		while(interimXSL.contains("VELMESSGE[")){
			startIndx = interimXSL.indexOf("VELMESSGE[") + "VELMESSGE[".length();
			stopIndx = interimXSL.indexOf("]",startIndx);
			messageKeyword = interimXSL.substring(startIndx, stopIndx);
			messageString = MC.getMessageByKey(messageKeyword);
			interimXSL=StringUtil.replaceAll(interimXSL, "VELMESSGE["+messageKeyword+"]", messageString);
		}
		while(interimXSL.contains("VELPARAMESSGE[")){
			startIndx = interimXSL.indexOf("VELPARAMESSGE[") + "VELPARAMESSGE[".length();
			stopIndx = interimXSL.indexOf("]",startIndx);
			messageKeyword = interimXSL.substring(startIndx, stopIndx);
			messageKey=messageKeyword.substring(0, messageKeyword.indexOf("||"));
			messageKey=messageKey.trim();
			messageParaKeyword=messageKeyword.substring(messageKeyword.indexOf("||")+2).split(java.util.regex.Pattern.quote("||"));
			messageString = VelosResourceBundle.getMessageString(messageKey,messageParaKeyword);
			interimXSL=StringUtil.replaceAll(interimXSL, "VELPARAMESSGE["+messageKeyword+"]", messageString);
		}
		if(!interimXSL.contains("word.GIF")){
			if(interimXSL.contains(LC.L_Word_Format)){
				tobeReplaced = "<img border=\"0\" title=\""+LC.L_Word_Format+"\" alt=\""+LC.L_Word_Format+"\" src=\"./images/word.GIF\"></img>";
				interimXSL=StringUtil.replace(interimXSL, LC.L_Word_Format, tobeReplaced);
			}
			if(interimXSL.contains(LC.L_Excel_Format)){
				tobeReplaced = "<img border=\"0\" title=\""+LC.L_Excel_Format+"\" alt=\""+LC.L_Excel_Format+"\" src=\"./images/excel.GIF\"></img>";
				interimXSL=StringUtil.replace(interimXSL,LC.L_Excel_Format, tobeReplaced);
			}
			if(interimXSL.contains(LC.L_Printer_FriendlyFormat)){
				tobeReplaced = "<img border=\"0\" src=\"./images/printer.gif\" title=\""+LC.L_Printer_FriendlyFormat+"\" alt=\""+LC.L_Printer_FriendlyFormat+"\"></img>";
				interimXSL=StringUtil.replace(interimXSL, LC.L_Printer_FriendlyFormat, tobeReplaced);
			}
		}

		xsl = interimXSL;
	}catch (Exception e)
	{
  		out.write(MC.M_Err_ReplacingLabel+": " + e.getMessage());/*out.write("Error in replacing label string: " + e.getMessage());*****/
 	}

	 try
    {	
	
		//first save the output in html file
		TransformerFactory tFactory1 = TransformerFactory.newInstance();
		Reader mR1=new StringReader(xml); 
		Reader sR1=new StringReader(xsl); 
		Source xmlSource1=new StreamSource(mR1);
		Source xslSource1 = new StreamSource(sR1);

 		Transformer transformer1 = tFactory1.newTransformer(xslSource1);
		//Set the params
		transformer1.setParameter("hdrFileName", hdrFilePath);
		transformer1.setParameter("ftrFileName", ftrFilePath);
		transformer1.setParameter("repTitle",repTitle);
		transformer1.setParameter("repName",repName);
		transformer1.setParameter("repBy",uName);
		transformer1.setParameter("repDate",repDate);
		transformer1.setParameter("argsStr",repArgsDisplay);
		transformer1.setParameter("cond","F");
		transformer1.setParameter("wd","");
		transformer1.setParameter("xd","");
		transformer1.setParameter("hd", "");
		transformer1.setParameter("hdrflag", hdrflag);
		transformer1.setParameter("ftrflag", ftrflag);
		transformer1.setParameter("dl", "");
		//Rohit - SW-FIN3C
		transformer1.setParameter("coverage_type_label", coverage_type_label);
		transformer1.setParameter("mode",displayMode);
		transformer1.setOutputProperty("encoding", "UTF-8");		
	
		// Perform the transformation, sending the output to html file
/*SV, 8/26, transformer didn't seem to close the stream properly that, next jsp (repGetExcel.jsp) kept getting 0(zero) bytes for file length,
	even though the file existed and was openable outside the application. So, fix is to, open and close file outside of transformer.
		
	  	transformer1.transform(xmlSource1, new StreamResult(htmlFile));
*/
		FileOutputStream fhtml=new FileOutputStream(htmlFile);
	  	transformer1.transform(xmlSource1, new StreamResult(fhtml));
	  	fhtml.close();
		//now send it to console			
	  	TransformerFactory tFactory = TransformerFactory.newInstance();
		Reader mR=new StringReader(xml); 
		Reader sR=new StringReader(xsl); 
		Source xmlSource=new StreamSource(mR);
		Source xslSource = new StreamSource(sR);
		// Generate the transformer.
			Transformer transformer = tFactory.newTransformer(xslSource);
		/*Changes for: INF-22402 Date:10/04/2012 By:Yogendra Pratap Singh*/
		String wordLink = "repGetWord.jsp?htmlFile=" + htmlFile +"&fileDnPath="+fileDnPath+"&filePath="+filePath+"&repId="+repId+"&calledfrom="+calledfrom;
		String excelLink = "repGetExcel.jsp?htmlFile=" + htmlFile +"&fileDnPath="+fileDnPath+"&filePath="+filePath+"&repId="+repId+"&calledfrom="+calledfrom;
		String printLink = "repGetHtml.jsp?htmlFileName=" + fileName +"&fileDnPath="+fileDnPath+"&filePath="+filePath+"&repId="+repId+"&calledfrom="+calledfrom;
		if (pageRight < 6)
		{
			String strRightMsg = "javascript:alert(&quot;"+MC.M_NoRgtTo_DldRpt+"&quot;);";/*String strRightMsg = "javascript:alert(&quot;You do not have access rights to download the report&quot;);";*****/
			excelLink = strRightMsg;
			printLink = strRightMsg;
		}		
		
		//Set the param for header and footer
		transformer.setParameter("hdrFileName", hdrFilePath);
		transformer.setParameter("ftrFileName", ftrFilePath);
		transformer.setParameter("repTitle",repTitle);
		transformer.setParameter("repName",repName);
		transformer.setParameter("repBy",uName);
		transformer.setParameter("repDate",repDate);
		transformer.setParameter("argsStr",repArgsDisplay);
		transformer.setParameter("cond","T");
		transformer.setParameter("wd",wordLink);
		transformer.setParameter("xd",excelLink);
		transformer.setParameter("hd",printLink);
		transformer.setParameter("hdrflag", hdrflag);
		transformer.setParameter("ftrflag", ftrflag);
		transformer.setParameter("dl", fileDnPath);
		//Rohit - SW-FIN3C
		transformer.setParameter("coverage_type_label", coverage_type_label);
		transformer.setParameter("studyApndxParam", "?tableName=ER_STUDYAPNDX&columnName=STUDYAPNDX_FILEOBJ&pkColumnName=PK_STUDYAPNDX&module=study&db=eres&pkValue=");
		transformer.setParameter("mode",displayMode);
		transformer.setOutputProperty("encoding", "UTF-8");
		
		//only for report 107, mock schedule
		
	  if (repId == 107)
		{
		 	transformer.setParameter("showSelect", showSelect);
		 	transformer.setParameter("cond",mockSchCondition);
		 
		} 
						  	  
		// Perform the transformation, sending the output to the response.
      	transformer.transform(xmlSource, new StreamResult(out));
			
    }
	catch (Exception e)
	{
  		out.write(e.getMessage());
 	}
	}catch(Exception exp){
		Rlog.error("report",exp.getMessage());
		return;
	}finally{
	
	/*Changes for: INF-22402 Date:10/04/2012 By:Yogendra Pratap Singh*/
	if(EJBUtil.isEmpty(calledfrom)){
		calledfrom = LC.L_Rpt_Central;
	}
	int downloadFlag=0;
	String downloadFormat = null;
	if(fileName.indexOf("xls")!=-1){
		downloadFormat = LC.L_Excel_Format;
		downloadFlag = 1;
	}else if(fileName.indexOf("doc")!=-1){
		downloadFormat = LC.L_Word_Format;
		downloadFlag = 1;
	}else if(!calledfrom.equalsIgnoreCase(LC.L_Calendar) 
			&& !calledfrom.equalsIgnoreCase("Patient")
			&& fileName.indexOf("html")!=-1){
		downloadFormat = LC.L_Printer_Friendly;
		downloadFlag = 1;
	}
	if(!formReport){
%>
	<jsp:include page="userReportLogging.jsp" flush="true">
		<jsp:param 	value="<%=repId %>" 		name="repId"/>
		<jsp:param 	value="<%=fileName%>" 		name="fileName"/>
		<jsp:param 	value="<%=filePath %>"		name="filePath"/>
		<jsp:param 	value="<%=calledfrom%>"		name="moduleName"/>
		<jsp:param 	value="<%=xml%>" 			name="repXml"/>
		<jsp:param 	value="<%=downloadFlag%>"	name="downloadFlag"/>
		<jsp:param value="<%=downloadFormat %>" name="downloadFormat"/>
	</jsp:include>
	<form id="hiddenFldForm" style="display:none;">
			<input type = "hidden" value="<%=repId %>" id="repId" name="repId"/>
			<input type = "hidden" value="<%=fileName%>" id="fileName" name="fileName"/>
			<input type = "hidden" value="<%=filePath %>" id="filePath"	name="filePath"/>
			<input type = "hidden" value="<%=calledfrom%>" id="moduleName" name="moduleName"/>
			<input type = "hidden" id="downloadFlag" name="downloadFlag"/>
			<input type = "hidden" id="downloadFormat" name="downloadFormat"/>
			<input type="hidden" id="export" name="export" value="false"></input>
			<input type="hidden" id="csvFrmt" name="csvFrmt" value="<%=LC.L_CSV_Format%>"/>
			<input type="hidden" id="excelFrmt" name="excelFrmt" value="<%=LC.L_Excel_Format%>"/>
			<input type="hidden" id="pdfFrmt" name="pdfFrmt" value="<%=LC.L_Pdf%>"/>
			<input type="hidden" id="print" name="print" value="<%=LC.L_Printer_FriendlyFormat%>"/>
			<input type="hidden" id="copy" name="copy" value="<%=LC.L_Copy%>"/>
			<input type="hidden" id="repLogUrl" name="repLogUrl"/>
		</form>
<%	}
	}
} //end of if session times out

else

{

%>

<jsp:include page="timeout_childwindow.jsp" flush="true"/> 


<%

} 

%>
<script type="text/javascript">
	window.onload = function() {
		$j('.buttons-copy').click(function(){
			logRepExport($j('#copy').val());
		});
		
		$j('.buttons-csv').click(function(){
			logRepExport($j('#csvFrmt').val());
		});
		
		$j('.buttons-excel').click(function(){
			logRepExport($j('#excelFrmt').val());
		});
		
		$j('.buttons-pdf').click(function(){
			logRepExport($j('#pdfFrmt').val());
		});
		$j
		('.buttons-print').click(function(){
			logRepExport($j('#print').val());
		});
	}
	function logRepExport(downloadFrmt){
		$j('#export').val('true');
		$j('#repLogUrl').val(window.location.href);
		$j('#downloadFlag').val('1');
		$j('#downloadFormat').val(downloadFrmt);
		$j.ajax({
			url: "userReportLogging.jsp",
			type: "POST",
			data: $j('#hiddenFldForm').serialize(),
			success: function(result){
				$j('#export').val('false');
				$j('#repLogUrl').val('');
				$j('#downloadFlag').val('0');
				$j('#downloadFormat').val('');
			},
			error: function(jqXHR, exception){
			}
		});
	}
</script>