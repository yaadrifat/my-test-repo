<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
String src = request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
String mode = request.getParameter("mode");
String includeMode = request.getParameter("includeMode");
%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.eres.service.util.StringUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB" %>
<!-- KM : import the objects for customized field -->
<%@ page import="com.velos.eres.web.user.ConfigFacade,com.velos.eres.web.user.ConfigObject, com.velos.eres.web.user.ConfigDetailsObject,com.velos.eres.business.common.CommonDAO"%>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC,com.velos.eres.business.studyNIHGrant.impl.StudyNIHGrantBean" %>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="studyAssocB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="accountB" scope="page" class="com.velos.eres.web.account.AccountJB" />
<jsp:useBean id="studyNIHGrantB"  scope="request" class="com.velos.eres.web.studyNIHGrant.StudyNIHGrantJB"/>

<SCRIPT LANGUAGE="JavaScript">

$j(window).load(function () {
		if ($j("#ctrp").is(':checked'))
			     $j("#NCIRow").show();
	});

function fnAddNciNctRow(){
	   if ($j("#ctrp").is(':checked'))
	     $j("#NCIRow").show();
	   else
	     $j("#NCIRow").hide();
}
	
</Script>

<%
String from = "default";

int studyId = 0;
if (mode.equals("M")) {
    	studyId = StringUtil.stringToNum(request.getParameter("studyId"));
	}
%>
<%

  	boolean hasAccess = false;
	HttpSession tSession = request.getSession(true);
	String uName = (String) tSession.getAttribute("userName");
	String userIdFromSession = (String) tSession.getAttribute("userId");
	String acc = (String) tSession.getAttribute("accountId");
	if (sessionmaint.isValidSession(tSession))
	{
	  	String defUserGroup = (String) tSession.getAttribute("defUserGroup");

		int accId = StringUtil.stringToNum(acc);
		int pageRight = 0;
		int stdRight =0;

		StudyDao stDao=new StudyDao();
		CtrlDao ctrlValue=new CtrlDao();//km
		CodeDao cdTArea = new CodeDao();
		CodeDao cdPhase = new CodeDao();
		CodeDao cdResType = new CodeDao();
		CodeDao cdPurpose = new CodeDao();
		CodeDao cdBlinding = new CodeDao();
		CodeDao cdRandom = new CodeDao();
		CodeDao cdType = new CodeDao();
		CodeDao cdScope=new CodeDao();
		CodeDao cdDivision = new CodeDao();
		CodeDao cdSponsorName = new CodeDao();//gopu
		String studyNumber = "" ;
		String studyTitle = "" ;
		
		String nciTrialIdentifier = "";
		String nctNumber = "";

		String studyObjective = "" ;
		String studySummary = "" ;
		String studyProduct = "" ;
		String studyDivision = "";
		String studyPurpose = "";
		String studyTArea = "" ;
		String studySize = "0" ;
		String studyDuration = "0" ;
		String studyDurationUnit = "" ;
		String studyEstBgnDate = "" ;
		String studyPhase = "" ;
		String studyResType = "" ;
		String studyType = "" ;
		String studyBlinding = "" ;
		String studyRandom = "" ;
		String studySponsorId = "" ;
		String studySponsorName = ""; //gopu
		String studySponsorIdInfo=""; //gopu
		String studyContactId = "" ;
		String studyOtherInfo = "" ;
		String studyPartCenters = "" ;
		String studyKeywrds = "" ;
		String studyAuthor = "" ;
		String studyAuthorName = "" ;
		String studyPrinv = "" ;
		String studyCo="";
		String studyCoName="";
		String studyPrinvName = "" ;
		String majAuthor="";
		String studyScope="";
		String studyAssoc="";
		String studyAssocNum="";
		String disSiteid="";
		String disSitename="";
		String studyPubCol = "00000";
		String studyVerParent = "";
		String studyInvFlag = "";
		String studyInvNum = "";
		String prinvOther="",nStudySize="0";
		String lastName  ="";
		String firstName = "";
		String ICDCode1 = "";//km
		String ICDCode2 = "";
		String studyDiv = "";
		String studyPurpse = "";
		//KM
		String disableStr ="";
		String readOnlyStr ="";
		String ctrpRepFlag =""; //CTRP-20527 22-Nov-2011 @Ankit
		String fdaRegStdFlag =""; //INF-22247 27-Feb-2012 @Ankit
		String ccsgdtFlag ="";
		/*String ICDCode3 = "";*/

		StringBuffer studyContact = new StringBuffer();
		StringBuffer studySponsor = new StringBuffer();
		StringBuffer dataManager = new StringBuffer();
		StringBuffer durationUnit = new StringBuffer();
		int counter = 0;
		int userId = 0;
		int siteId = 0;
		SiteDao siteDao = new SiteDao();
		// Added Column customization
		ArrayList cValue = new ArrayList();
		ArrayList cCustom1 = new ArrayList();
		String lkpId = "";
		String lkpColumn = "";
		
		ctrlValue.getControlValues("icd");//km
		cValue=ctrlValue.getCValue();
		cCustom1=ctrlValue.getCCustom1();
		if((cValue!=null && cValue.size()>0) && (cCustom1!=null && cCustom1.size()>0))
		{
			lkpId = (String)cValue.get(0);
			lkpColumn = (String)cCustom1.get(0);
		}
		
		
		//Added for INF-22311 : Raviesh
		String publicVsNonpublicStr = "";
		String fdaRegulatedStr = "";
		String ctrpReportableStr = "";
		publicVsNonpublicStr = MC.M_PublicVsNonPublicContent;
		fdaRegulatedStr = MC.M_FDARegulatedStudyContent;
		ctrpReportableStr = MC.M_CTRPReportableContent;


		//Added by Manimaran for the Enh.#GL9

		ConfigFacade cFacade=ConfigFacade.getConfigFacade();
		HashMap hashPgCustFld = new HashMap();

		ConfigDetailsObject cdoPgField = cFacade.populateObject(Integer.parseInt(acc), "studysummary");

		if (cdoPgField.getPcfField()!= null && cdoPgField.getPcfField().size() > 0) {
			for (int i=0;i<cdoPgField.getPcfField().size();i++){
				hashPgCustFld.put((String)cdoPgField.getPcfField().get(i), String.valueOf(i));

	       }
	    }

		if (mode.equals("M")){
			studyB.setId(studyId);
			studyB.getStudyDetails();
		}

		HashMap sysDataMap = studyB.getStudySysData(request);
		if (null == sysDataMap)
			return;

		String autoGenStudy = (String) sysDataMap.get("autoGenStudy");

		int grpRight = 0;
		grpRight = StringUtil.stringToNum(sysDataMap.get("grpRight").toString());
		
		pageRight = StringUtil.stringToNum(sysDataMap.get("pageRight").toString());
		
		tSession.setAttribute("studyId",sysDataMap.get("studyId"));
		tSession.setAttribute("studyNo",sysDataMap.get("studyNo"));
		
		if (mode.equals("M"))
		{
			String roleCodePk = (String) sysDataMap.get("studyRoleCodePk");
			tSession.setAttribute("studyRoleCodePk",roleCodePk);

			stdRights = (StudyRightsJB) sysDataMap.get("studyRights");
			tSession.setAttribute("studyRights",stdRights);

			stdRight = StringUtil.stringToNum((sysDataMap.get("stdSummRight").toString()));
			tSession.setAttribute("studyRightForJS",stdRight);

			pageRight = StringUtil.stringToNum((sysDataMap.get("pageRight").toString()));

			studyNumber = (String) sysDataMap.get("studyNo");
			studyVerParent = (String) sysDataMap.get("studyVerParent");
			studyPurpse = (String) sysDataMap.get("studyPurpse");
			studyDiv = (String) sysDataMap.get("studyDiv");
			studyInvFlag = (String) sysDataMap.get("studyInvFlag");
			studyInvNum = (String) sysDataMap.get("studyInvNum");
			ctrpRepFlag = (String) sysDataMap.get("ctrpRepFlag");
			fdaRegStdFlag = (String) sysDataMap.get("fdaRegStdFlag");
			ccsgdtFlag = (String) sysDataMap.get("ccsgdtFlag");

			tSession.setAttribute("studyVerParent",sysDataMap.get("studyVerParent"));

			String studyT = (String) sysDataMap.get("studyTArea");
			tSession.setAttribute("studyTArea",studyT);
		} else {
			tSession.setAttribute("StudyRights",null);
			tSession.setAttribute("studyRightForJS",pageRight);
		}

		
		if (((mode.equals("M") && StringUtil.isAccessibleFor(pageRight, 'V')) || (mode.equals("N") && StringUtil.isAccessibleFor(pageRight, 'N'))) 
		&& ((mode.equals("M") && StringUtil.isAccessibleFor(grpRight, 'V')) || (mode.equals("N") && StringUtil.isAccessibleFor(grpRight, 'N'))))
		{
			if ( ("M".equals(mode) && pageRight > 5) ||
			     ("N".equals(mode) && (pageRight == 5 || pageRight == 7) ) )
			{
			 	hasAccess = true;
			}
			//cdTArea.getCodeValues("tarea");

			cdTArea.getCodeValuesForCustom1("tarea",studyDiv);
			cdTArea.setCType("tarea");
	        cdTArea.setForGroup(defUserGroup);

			cdDivision.getCodeValues("study_division");
			cdDivision.setCType("study_division");
	        cdDivision.setForGroup(defUserGroup);

	        cdPurpose.getCodeValues("studyPurpose");
	        cdPurpose.setCType("studyPurpose");
	        cdPurpose.setForGroup(defUserGroup);

			cdPhase.getCodeValues("phase");
			cdPhase.setCType("phase");
	        cdPhase.setForGroup(defUserGroup);

			cdResType.getCodeValues("research_type");
			cdResType.setCType("research_type");
	        cdResType.setForGroup(defUserGroup);

			cdBlinding.getCodeValues("blinding");
			cdBlinding.setCType("blinding");
	        cdBlinding.setForGroup(defUserGroup);

			cdRandom.getCodeValues("randomization");
			cdRandom.setCType("randomization");
	        cdRandom.setForGroup(defUserGroup);

			cdType.getCodeValues("study_type");
			cdType.setCType("study_type");
	        cdType.setForGroup(defUserGroup);

			cdScope.getCodeValues("studyscope");
			cdScope.setCType("studyscope");
	        cdScope.setForGroup(defUserGroup);

			cdSponsorName.getCodeValues("sponsor"); //gopu
			cdSponsorName.setCType("sponsor");
	        cdSponsorName.setForGroup(defUserGroup);

	        String publishRadioAtt ="";
	        String divAtt ="";
	        String purposeAtt ="";
			String tareaAtt ="";
			String phaseAtt ="";
			String researchAtt ="";
			String scopeAtt ="";
			String typeAtt ="";
			String blindingAtt ="";
			String randomAtt = "";
			String spnameAtt = "";
			String hideStr = "display:none";
			String emptyStr = "";

			if (hashPgCustFld.containsKey("publishRadio")) {
				int fldPublishRadio = Integer.parseInt((String)hashPgCustFld.get("publishRadio"));
				publishRadioAtt = ((String)cdoPgField.getPcfAttribute().get(fldPublishRadio));
				if(publishRadioAtt == null) publishRadioAtt = emptyStr;
			}
		    if (hashPgCustFld.containsKey("division")) {
				int fldNumDivision = Integer.parseInt((String)hashPgCustFld.get("division"));
				divAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDivision));
				if(divAtt == null) divAtt ="";
		    }
		    
		    if (hashPgCustFld.containsKey("purpose")) {
				int fldNumPurpose = Integer.parseInt((String)hashPgCustFld.get("purpose"));
				purposeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumPurpose));
				if(purposeAtt == null) purposeAtt ="";
		    }

			if (hashPgCustFld.containsKey("therapeuticarea")) {
				int fldNumTarea = Integer.parseInt((String)hashPgCustFld.get("therapeuticarea"));
				tareaAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumTarea));
				if(tareaAtt == null) tareaAtt ="";
		    }

			if (hashPgCustFld.containsKey("stdphase")) {
				int fldNumPhase = Integer.parseInt((String)hashPgCustFld.get("stdphase"));
				phaseAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumPhase));
				if (phaseAtt == null) phaseAtt ="";
		    }


			if (hashPgCustFld.containsKey("researchtype")) {
				int fldNumResearch = Integer.parseInt((String)hashPgCustFld.get("researchtype"));
				researchAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumResearch));
				if (researchAtt == null) researchAtt ="";
		    }

			if (hashPgCustFld.containsKey("studyscope")) {
				int fldNumScope = Integer.parseInt((String)hashPgCustFld.get("studyscope"));
				scopeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumScope));
				if (scopeAtt == null) scopeAtt ="";
		    }


			if (hashPgCustFld.containsKey("studytype")) {
				int fldNumType = Integer.parseInt((String)hashPgCustFld.get("studytype"));
				typeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumType));
				if (typeAtt == null) typeAtt ="";
		    }


			if (hashPgCustFld.containsKey("blinding")) {
				int fldNumBlinding = Integer.parseInt((String)hashPgCustFld.get("blinding"));
				blindingAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumBlinding));
				if (blindingAtt == null) blindingAtt ="";
		    }


			if (hashPgCustFld.containsKey("randomization")) {
				int fldNumRandom = Integer.parseInt((String)hashPgCustFld.get("randomization"));
				randomAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumRandom));
				if (randomAtt == null) randomAtt ="";
		    }


			if (hashPgCustFld.containsKey("sponsorname")) {
				int fldNumSpname = Integer.parseInt((String)hashPgCustFld.get("sponsorname"));
				spnameAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSpname));
				if (spnameAtt == null) spnameAtt ="";
		    }

			if (mode.equals("M")) {
%>
<%--
				String studyT = "";
				studyB.setId(studyId);
				studyB.getStudyDetails();
				studyNumber = studyB.getStudyNumber();
				studyVerParent = studyB.getStudyVersionParent();

				if (studyVerParent == null)
					studyVerParent = "";

				tSession.setAttribute("studyId",request.getParameter("studyId"));
				tSession.setAttribute("studyNo",studyNumber);

				tSession.setAttribute("studyVerParent",studyVerParent);

				studyT = studyB.getStudyTArea();
				studyDiv = studyB.getStudyDivision();
				studyInvFlag = studyB.getStudyInvIndIdeFlag();
				studyInvNum = studyB.getStudyIndIdeNum();

				if (studyT == null)
					studyT = "";
				if(studyDiv == null)
					studyDiv  = "";
				if(studyInvNum == null)
					studyInvNum = "";
				tSession.setAttribute("studyTArea",studyT);
--%>
<%
						//stdRights.setId(studyId);
							studyTitle = studyB.getStudyTitle();
							if(studyTitle == null) studyTitle = ""; //KM
							
							
							//nciTrialIdentifier Added for CTRP-22471 : Raviesh							
							nciTrialIdentifier = studyB.getNciTrialIdentifier();
							if(nciTrialIdentifier == null) nciTrialIdentifier = "";
							
							//nctNumber Added for CTRP-22471 : Raviesh
							nctNumber = studyB.getNctNumber();
							if(nctNumber == null) nctNumber = "";
							

							//	studyTitle = StringUtil.htmlDecode(studyTitle);

							studyObjective = studyB.getStudyObjective();
							studyObjective = (   studyObjective  == null      )?"":(  studyObjective ) ;

							studySummary = studyB.getStudySummary();
							studySummary = (   studySummary  == null      )?"":(  studySummary ) ;

							studyProduct = studyB.getStudyProduct();
							studyProduct = (   studyProduct  == null      )?"":(  studyProduct ) ;

							//studySize = studyB.getStudySize();
							//studySize = (   studySize  == null      )?"":(  studySize ) ;
							nStudySize=studyB.getStudyNSamplSize();
							nStudySize=(nStudySize==null)?"":nStudySize;
							studyDuration = studyB.getStudyDuration();
							studyDuration=(StringUtil.stringToNum(studyDuration) == 0)?"0":studyDuration;

							studyDurationUnit = studyB.getStudyDurationUnit();
							studyEstBgnDate = studyB.getStudyEstBeginDate();

							studyOtherInfo = studyB.getStudyInfo();
							studyOtherInfo = (   studyOtherInfo  == null      )?"":(  studyOtherInfo ) ;

							//studyPartCenters = studyB.getStudyPartCenters();
							//studyPartCenters = (   studyPartCenters  == null      )?"":(  studyPartCenters ) ;

							studyKeywrds = studyB.getStudyKeywords();
							studyKeywrds = (   studyKeywrds  == null      )?"":(  studyKeywrds ) ;

							studySponsorId = studyB.getStudySponsor();
							studySponsorId = (   studySponsorId  == null      )?"":(  studySponsorId ) ;

							//gopu
							studySponsorName = studyB.getStudySponsorName();
							studySponsorName = (   studySponsorName  == null      )?"":(  studySponsorName ) ;

							studySponsorIdInfo = studyB.getStudySponsorIdInfo();
							studySponsorIdInfo = (   studySponsorIdInfo  == null      )?"":(  studySponsorIdInfo ) ;

							studyContactId = studyB.getStudyContact();
							studyContactId = (   studyContactId  == null      )?"":(  studyContactId ) ;

							studyAuthor = studyB.getStudyAuthor();
							if(studyAuthor == null)	studyAuthor = "";

							studyPrinv = studyB.getStudyPrimInv();
							if(studyPrinv==null) studyPrinv="";

							//Study Coordinator
							studyCo=studyB.getStudyCoordinator();
							studyCo=(studyCo==null)?"":studyCo;

							prinvOther=studyB.getStudyOtherPrinv();
							if (prinvOther==null) prinvOther="";
							majAuthor=studyB.getMajAuthor();
							majAuthor=(majAuthor==null)?"":(majAuthor);

							studyAssoc=studyB.getStudyAssoc();
							studyAssoc=(studyAssoc==null)?"":(studyAssoc);

							disSiteid=studyB.getDisSite();
							disSiteid=(disSiteid==null)?"":(disSiteid);
							disSitename=stDao.getDiseaseSite(StringUtil.integerToString(new Integer(studyId)));
							disSitename=(disSitename==null)?"":(disSitename);
							tSession.setAttribute("studyDataMgr",studyAuthor);
							ICDCode1=studyB.getStudyICDCode1();//km
							ICDCode1=(ICDCode1==null)?"":(ICDCode1);
							ICDCode2=studyB.getStudyICDCode2();
							ICDCode2=(ICDCode2==null)?"":(ICDCode2);
							/*ICDCode3=studyB.getStudyICDCode3();
							ICDCode3=(ICDCode3==null)?"":(ICDCode3);*/

							userB.setUserId(StringUtil.stringToNum(studyAuthor));
							userB.getUserDetails();
							firstName  = userB.getUserFirstName();
							lastName = userB.getUserLastName();
							if(firstName == null)
								firstName  = "";
							if(lastName == null)
								lastName  = "";

							studyAuthorName = firstName+" " +lastName ;

							//if (! StringUtil.isEmpty(studyPrinv) )
							if(studyPrinv.length()==0 || studyPrinv.equals("0"))
								studyPrinvName="";
							else {
								userB.setUserId(StringUtil.stringToNum(studyPrinv));
								userB.getUserDetails();
								studyPrinvName =  userB.getUserFirstName() + " " +userB.getUserLastName();
							}

							//if (! StringUtil.isEmpty(studyPrinv) )
							if(studyCo.length()==0)
								studyCoName="";
							else {
								userB.setUserId(StringUtil.stringToNum(studyCo));
								userB.getUserDetails();
								studyCoName =  userB.getUserFirstName() + " " +userB.getUserLastName();
							}

							studyPubCol = studyB.getStudyPubCol();

							if (tareaAtt.equals("1") || tareaAtt.equals("2"))
								studyTArea = cdTArea.toPullDown("studyTArea",StringUtil.stringToNum(studyB.getStudyTArea()) ,"disabled");
							else
								studyTArea = cdTArea.toPullDown("studyTArea",StringUtil.stringToNum(studyB.getStudyTArea()));

							if (divAtt.equals("1") || divAtt.equals("2"))
								studyDivision = cdDivision.toPullDown("studyDivision",StringUtil.stringToNum(studyDiv), " onChange='studyFunctions.callAjaxGetTareaDD(studyFunctions.formObj); studyFunctions.setValueCCSGReport(studyFunctions.formObj);' disabled");
							else
							    studyDivision = cdDivision.toPullDown("studyDivision",StringUtil.stringToNum(studyDiv), " onChange='studyFunctions.callAjaxGetTareaDD(studyFunctions.formObj); studyFunctions.setValueCCSGReport(studyFunctions.formObj)'");
							
							if (purposeAtt.equals("1") || purposeAtt.equals("2"))
								studyPurpose = cdPurpose.toPullDown("studyPurpose",StringUtil.stringToNum(studyPurpse)," disabled");
							else
								studyPurpose = cdPurpose.toPullDown("studyPurpose",StringUtil.stringToNum(studyPurpse));

							if (phaseAtt.equals("1") || phaseAtt.equals("2"))
								studyPhase = cdPhase.toPullDown("studyPhase",StringUtil.stringToNum(studyB.getStudyPhase()),"disabled");
							else
								studyPhase = cdPhase.toPullDown("studyPhase",StringUtil.stringToNum(studyB.getStudyPhase()));

							if (researchAtt.equals("1") || researchAtt.equals("2"))
								studyResType = cdResType.toPullDown("studyResType",StringUtil.stringToNum(studyB.getStudyResType()),"disabled");
							else
								studyResType = cdResType.toPullDown("studyResType",StringUtil.stringToNum(studyB.getStudyResType()));

							if (blindingAtt.equals("1") || blindingAtt.equals("2"))
								studyBlinding = cdBlinding.toPullDown("studyBlinding",StringUtil.stringToNum(studyB.getStudyBlinding()),"disabled");
							else
								studyBlinding = cdBlinding.toPullDown("studyBlinding",StringUtil.stringToNum(studyB.getStudyBlinding()));

							if (randomAtt.equals("1") || randomAtt.equals("2"))
								studyRandom = cdRandom.toPullDown("studyRandom",StringUtil.stringToNum(studyB.getStudyRandom()),"disabled");
							else
								studyRandom = cdRandom.toPullDown("studyRandom",StringUtil.stringToNum(studyB.getStudyRandom()));

							if (spnameAtt.equals("1") || spnameAtt.equals("2"))
								studySponsorName = cdSponsorName.toPullDown("sponsor",StringUtil.stringToNum(studyB.getStudySponsorName()),"disabled");
							else
								studySponsorName = cdSponsorName.toPullDown("sponsor",StringUtil.stringToNum(studyB.getStudySponsorName()),true);

							if (typeAtt.equals("1") || typeAtt.equals("2"))
								studyType = cdType.toPullDown("studyType",StringUtil.stringToNum(studyB.getStudyType()),"disabled");
							else
								studyType = cdType.toPullDown("studyType",StringUtil.stringToNum(studyB.getStudyType()));

							if (scopeAtt.equals("1") || scopeAtt.equals("2"))
								studyScope=cdScope.toPullDown("studyScope",StringUtil.stringToNum(studyB.getStudyScope()),"disabled");
							else
								studyScope=cdScope.toPullDown("studyScope",StringUtil.stringToNum(studyB.getStudyScope()));

							if (studyAssoc.length()>0){
								studyAssocB.setId(StringUtil.stringToNum(studyAssoc));
								studyAssocB.getStudyDetails();
								studyAssocNum = studyAssocB.getStudyNumber();
							}
					   	} else {
							if (tareaAtt.equals("1") || tareaAtt.equals("2"))
								studyTArea = cdTArea.toPullDown("studyTArea","disabled");
							else
								studyTArea = cdTArea.toPullDown("studyTArea");

							if (divAtt.equals("1") || divAtt.equals("2"))
								studyDivision = cdDivision.toPullDown("studyDivision",0, " onChange='studyFunctions.callAjaxGetTareaDD(studyFunctions.formObj); studyFunctions.setValueCCSGReport(studyFunctions.formObj);' disabled");
							else
							    studyDivision = cdDivision.toPullDown("studyDivision",0, " onChange='studyFunctions.callAjaxGetTareaDD(studyFunctions.formObj); studyFunctions.setValueCCSGReport(studyFunctions.formObj);'");
							
							if (purposeAtt.equals("1") || purposeAtt.equals("2"))
								studyPurpose = cdPurpose.toPullDown("studyPurpose",0,"disabled");
							else
								studyPurpose = cdPurpose.toPullDown("studyPurpose",0);

							if (phaseAtt.equals("1") || phaseAtt.equals("2"))
								studyPhase = cdPhase.toPullDown("studyPhase","disabled");
							else
							    studyPhase = cdPhase.toPullDown("studyPhase");

							if (researchAtt.equals("1") || researchAtt.equals("2"))
								studyResType = cdResType.toPullDown("studyResType","disabled");
							else
								studyResType = cdResType.toPullDown("studyResType");

							if (blindingAtt.equals("1") || blindingAtt.equals("2"))
								studyBlinding = cdBlinding.toPullDown("studyBlinding","disabled");
							else
								studyBlinding = cdBlinding.toPullDown("studyBlinding");

							if (randomAtt.equals("1") || randomAtt.equals("2"))
								studyRandom = cdRandom.toPullDown("studyRandom","disabled");
							else
								studyRandom = cdRandom.toPullDown("studyRandom");

							if (spnameAtt.equals("1") || spnameAtt.equals("2"))
								studySponsorName = cdSponsorName.toPullDown("sponsor","disabled");
							else
								studySponsorName = cdSponsorName.toPullDown("sponsor");

							if (typeAtt.equals("1") || typeAtt.equals("2"))
								studyType = cdType.toPullDown("studyType","disabled");
							else
								studyType = cdType.toPullDown("studyType");

							if (scopeAtt.equals("1") || scopeAtt.equals("2"))
								studyScope=cdScope.toPullDown("studyScope","disabled");
							else
								studyScope=cdScope.toPullDown("studyScope");

							studyAuthor = userIdFromSession;
							userB.setUserId(StringUtil.stringToNum(studyAuthor));
							userB.getUserDetails();
							firstName  = userB.getUserFirstName();
							lastName = userB.getUserLastName();
							if(firstName == null)
								firstName  = "";
							if(lastName == null)
								lastName  = "";
							studyAuthorName = firstName+" " + lastName;

							//studyPrinv = userIdFromSession;
							//studyPrinvName = userB.getUserLastName() + ", " + userB.getUserFirstName();
						}
						char[] stdSecRights= null;
						if (StringUtil.isEmpty(studyPubCol)) studyPubCol="1111";
						stdSecRights= studyPubCol.toCharArray();
						int totSec = studyPubCol.length();
						int secCount=0;

						from = "default";
			%>
			    <input type="hidden" name="srcmenu" size = 20  value = <%=src%> >
			    <input type="hidden" name="selectedTab" size = 20  value = <%=selectedTab%> >
			    <input type="hidden" name="totPubSections" size = 20  value=4>
			    <input type="hidden" name="delStrs" >

		    <%if (mode.equals("M"))
			{%>
			    <input type="hidden" id="studyId" name="studyId" size = 20  value = <%=studyId%> >
				<input type="hidden" name="studyVerParent" size = 20  value = <%=studyVerParent%>>
				
		 	<% } %>
				<input type="hidden" id="mode" name="mode" size="20"  value = <%=mode%> >
			    <P class="sectionHeadingsFrm custom-study"><%=LC.L_Std_Info%><%--Study Information*****--%></P>
			    <table width="100%" cellspacing="2" cellpadding="2" border="0" class="basetbl">
					<tr>
						<td width="40%">
							<table>
								<tr>									
									<td colspan =2>
										<%
										boolean displayCopyStudyLink = true;
										if (hashPgCustFld.containsKey("copyStudyLink")) {
											int fldCopyStudyLink= Integer.parseInt((String)hashPgCustFld.get("copyStudyLink"));
											String copyStudyLinkAtt = ((String)cdoPgField.getPcfAttribute().get(fldCopyStudyLink));
											if (copyStudyLinkAtt.equals("0")) {displayCopyStudyLink = false;}										
										} 
										%>
										<%if (displayCopyStudyLink){ %>
										<A id="copyStudyLink" name="copyStudyLink" href="copystudy.jsp?srcmenu=<%=src%>&studymode=<%=mode%>&fromstudyId=<%=studyId%>&fromMode=initial" onClick="return f_check_perm(<%=grpRight%>,'N')"><%=MC.M_CpyExtg_Std%><%--COPY AN EXISTING STUDY*****--%></A>
										<%} %>
									</td>
								</tr>		
				
								<tr>
								<%
				
								if (hashPgCustFld.containsKey("studyentby")) {
				
								int fldNumStudyBy = Integer.parseInt((String)hashPgCustFld.get("studyentby"));
								String studyByMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStudyBy));
								String studyByLable = ((String)cdoPgField.getPcfLabel().get(fldNumStudyBy));
								String studyByAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStudyBy));
				
								disableStr ="";
								readOnlyStr ="";
				
								if(studyByAtt == null) studyByAtt ="";
								if(studyByMand == null) studyByMand ="";
				
								if(!studyByAtt.equals("0")) {
				
								if(studyByLable !=null){
								%>
								<td width="20%">
								 <%=studyByLable%> 
								<%} else {%> <td width="20%">
								<!--b>Study Entered By</b-->
								<%=LC.L_Std_EnteredBy%><%--Study Entered By*****--%>
								<% }
				
				
							   if (studyByMand.equals("1")) {
								%>
							   <FONT class="Mandatory" id="pgcustomstudyentby">* </FONT>
							   <% }
							   %>
				
							   <%if(studyByAtt.equals("1")) {
								   disableStr = "disabled class='readonly-input'"; }
							   %>
							  </td>
							  <td width="30%">
							  <input type=hidden name="dataManager" value='<%=studyAuthor%>' <%=disableStr%> >
							  <input type=text name="dataManagerName" value="<%=studyAuthorName%>" readonly <%=disableStr%>>
				
							  <%if(!studyByAtt.equals("1") && !studyByAtt.equals("2")) { %>
							  <A id='study_selectLink' HREF=# onClick="studyFunctions.openwin1('study')"><%=LC.L_Select_User%><%--SELECT USER*****--%></A>
							  <%}%>
				
							  </td>
				
								<% } else { %>
							   <input type=hidden name="dataManager" value='<%=studyAuthor%>'>   <!-- KM- for PK value in case of hidden-->
				
								<%}}  else {%>
				
								<td width="20%"><%=LC.L_Std_EnteredBy%><%--Study Entered By*****--%> <FONT class="Mandatory" id="mandstudyent">* </FONT>
										</td>
										<td width="30%">
											<input type=hidden name="dataManager" value='<%=studyAuthor%>'>
											<input type=text name="dataManagerName" value="<%=studyAuthorName%>" readonly>
											<A id='study_selectLink' HREF=# onClick="studyFunctions.openwin1('study')"><%=LC.L_Select_User%><%--SELECT USER*****--%></A>
										</td>
				
							 <%}%>
							</tr>
				
				
								<tr>
								<%
								if (hashPgCustFld.containsKey("pinvestigator")) {
				
								int fldNumPi = Integer.parseInt((String)hashPgCustFld.get("pinvestigator"));
								String pIMand = ((String)cdoPgField.getPcfMandatory().get(fldNumPi));
								String pILable = ((String)cdoPgField.getPcfLabel().get(fldNumPi));
								String pIAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumPi));
				
								disableStr ="";
								readOnlyStr ="";
				
								if(pIAtt == null) pIAtt ="";
								if(pIMand == null) pIMand ="";
				
								if(!pIAtt.equals("0")) {
				
								if(pILable !=null){
								%>
								<td width="20%">
								 <%=pILable%> 
								<%} else {%> <td width="20%">
								<%=LC.L_Principal_Investigator%><%--Principal Investigator*****--%>
								<% }
				
				
							   if (pIMand.equals("1")) {
								%>
							   <FONT class="Mandatory" id="pgcustompi">* </FONT>
							   <% }
							   %>
				
							   <%if(pIAtt.equals("1")) {
								   disableStr = "disabled class='readonly-input'"; }
				
							   %>
							  </td>
							  <td width="30%">
							  <input type=hidden name="prinInv" value='<%=studyPrinv%>'>
							  <input type=text name="prinInvName" value="<%=studyPrinvName%>" readonly <%=disableStr%>>
				
							   <%if(!pIAtt.equals("1") && !pIAtt.equals("2")) {%>
							  <A id='studyinv_selectLink' HREF=# ><%=LC.L_Select_User%><%--SELECT USER*****--%></A>
							   <%}%>
							  </td>
				
								<% } else { %>
								 <input type=hidden name="prinInv" value='<%=studyPrinv%>'>
				
								<% } }  else {%>
				
								<td width="20%"> <%=LC.L_Principal_Investigator%><%--Principal Investigator*****--%>
								 </td>
								 <td width="30%">
											<input type=hidden name="prinInv" value='<%=studyPrinv%>'>
											<input type=text name="prinInvName" value="<%=studyPrinvName%>" readonly>
											<A id='studyinv_selectLink' HREF=# ><%=LC.L_Select_User%><%--SELECT USER*****--%></A>
				
							 <%}%>
							 </tr>
				
				
								<tr>
								<%
								if (hashPgCustFld.containsKey("ifotheruser")) {
				
								int fldNumOtherUser = Integer.parseInt((String)hashPgCustFld.get("ifotheruser"));
								String othUserMand = ((String)cdoPgField.getPcfMandatory().get(fldNumOtherUser));
								String othUserLable = ((String)cdoPgField.getPcfLabel().get(fldNumOtherUser));
								String othUserAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumOtherUser));
				
								disableStr ="";
								readOnlyStr ="";
				
								if(othUserAtt == null) othUserAtt ="";
								if(othUserMand == null) othUserMand ="";
				
								if(!othUserAtt.equals("0")) {
				
								if(othUserLable !=null){
								%>
								<td width="20%">
								<%=othUserLable%>
								<%} else {%> <td width="20%">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_If_Other%><%--IF OTHER*****--%>
								<% }
				
				
							   if (othUserMand.equals("1")) {
								%>
							   <FONT class="Mandatory" id="pgcustomifotheruser">* </FONT>
							   <% }
							   %>
				
							   <%if(othUserAtt.equals("1")) {
								   disableStr = "disabled class='readonly-input'"; }
								 else if (othUserAtt.equals("2") ) {
								   readOnlyStr = "readonly"; }
							   %>
							  </td>
							  <td width="30%"> <input type="text" name="prinvOther" id="prinvIfOther" value="<%=prinvOther%>"  <%=disableStr%>  <%=readOnlyStr%>  ></td>
				
								<% } else { %>
								<input type="hidden" name="prinvOther" id="prinvIfOther"  value="<%=prinvOther%>" >
				
								<% } }  else {%>
				
								<td width="20%"> <%=LC.L_If_Other%><%--IF OTHER*****--%></td>
									<td width="30%"><input type="text" name="prinvOther" id="prinvIfOther" value="<%=prinvOther%>"></td>
							 <%}%>
							 </tr>
				
				
								<tr>
								<%
				
								if (hashPgCustFld.containsKey("studycontact")) {
				
								int fldNumStudyCont = Integer.parseInt((String)hashPgCustFld.get("studycontact"));
								String StudyContMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStudyCont));
								String StudyContLable = ((String)cdoPgField.getPcfLabel().get(fldNumStudyCont));
								String StudyContAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStudyCont));
				
								disableStr ="";
								readOnlyStr ="";
				
								if(StudyContAtt == null) StudyContAtt ="";
								if(StudyContMand == null) StudyContMand ="";
				
								if(!StudyContAtt.equals("0")) {
				
								if(StudyContLable !=null){
								%>
								<td width="20%">
								 <%=StudyContLable%> 
								<%} else {%> <td width="20%">
								<%=LC.L_Study_Contact%><%--Study Contact*****--%> 
								<% }
				
							   if (StudyContMand.equals("1")) {
								%>
							   <FONT class="Mandatory" id="pgcustomstudycont">* </FONT>
							   <% }
							   %>
				
							   <%if(StudyContAtt.equals("1")) {
								   disableStr = "disabled class='readonly-input'"; }
							   %>
							  </td>
							  <td width="30%">
									<input type=hidden name="studyco" value='<%=studyCo%>'>
									<input type=text name="studycoName" value="<%=studyCoName%>" readonly  <%=disableStr%> >
				
									 <%if(!StudyContAtt.equals("1") && !StudyContAtt.equals("2")) {%>
									<A id='studyco_selectLink' HREF=# onClick="studyFunctions.openwin1('studyco')"><%=LC.L_Select_User%><%--SELECT USER*****--%></A>
									<%}%>
				
				
							  </td>
				
								<% } else {%>
								<input type=hidden name="studyco" value='<%=studyCo%>'>
								<% }}  else {%>
				
								<td width="20%"> <%=LC.L_Study_Contact%><%--Study Contact*****--%> </td>
								<td width="30%">
									<input type=hidden name="studyco" value='<%=studyCo%>'>
									<input type=text name="studycoName" value="<%=studyCoName%>" readonly>
									<A id='studyco_selectLink' HREF=# onClick="studyFunctions.openwin1('studyco')"><%=LC.L_Select_User%><%--SELECT USER*****--%></A>
								</td>
							 <%}%>
							</tr>
							</table>
							
					</td>
					<td width="40%">
						<table width="80%">
							<tr>
						<td>
					<%if (majAuthor.equals("Y")){%>
					<input type="checkbox" id="cbauth" name="cbauth" onClick="studyFunctions.setValue(studyFunctions.formObj)" checked>
					<%}else{%>
					<input type="checkbox" id="cbauth" name="cbauth" onClick="studyFunctions.setValue(studyFunctions.formObj)">
					<%}%>
					<%=MC.M_PInvestigator_MajorAuth%><%--Principal Investigator was a major author/initiator of this study?*****--%></td>
					<input type="hidden" name="author" value="<%=majAuthor%>">

					</tr>
					</tr>

					<tr >
						<!-- CTRP-20527 22-Nov-2011 @Ankit -->
						<td>
							<input type="checkbox" name="ctrp" id="ctrp" <%if(ctrpRepFlag.equals("1")){%>checked onclick="return false" onkeydown="return false"<%}%><%else{%>onclick="fnAddNciNctRow();"<%}%>></input>
							<%=LC.L_Ctrp_Reportable%>
							&nbsp; <A href="#"><img src="../images/jpg/help.jpg" border="0" onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS(ctrpReportableStr)%>',CAPTION,'<%=LC.L_Ctrp_Reportable%> <%=LC.L_Study%>',RIGHT, ABOVE,OFFSETY,70,WRAP);"	onmouseout="return nd();" alt=""></img></A>
						<input type='hidden' value='ctrpReptFlag' name='ctrpReportFlag'/>
						</td>
					</tr>
					<tr >
						<!-- INF-22247 27-Feb-2012 @Ankit -->
						<td>
						<%if(fdaRegStdFlag.equals("1")){%>
							&nbsp;<%=LC.L_StudyMarkedAsFDARegulated%>
							<input type="hidden" name="fda" value="on" >
						<%}else{%>
							<input type="checkbox" name="fda">
							<%=LC.L_FdaRegulatedStudy%>
							&nbsp; <A href="#">
							<img src="../images/jpg/help.jpg" border="0" onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS(fdaRegulatedStr)%>',CAPTION,'<%=LC.L_FdaRegulatedStudy%>',RIGHT, ABOVE,OFFSETY,70,WRAP);" onmouseout="return nd();" alt=""></img></A>
						<%}%>
							<input type='hidden' value='fdaFlag' name='fdaRegFlag'/>
						</td>
					</tr>					
						</table>
					</td>
				</tr>
				</table>
				
			   <!-- Fixed Bug#7882 : Raviesh -->
			   <table width="100%" cellspacing="2" cellpadding="2" border="0" class="basetbl midAlign">
					<tr>					
						<td colspan=2 align="left">
						<br/>
						<!-- Modified for Bug#7962 : Raviesh -->
						<!-- Modified for Bug#7969 : Akshi -->
							<jsp:include page="ctrpDraftSectINDIDE.jsp">
								<jsp:param value="<%=studyId%>" name="studyId"/>
								<jsp:param value="<%=studyInvNum%>" name="studyInvNum"/>
								<jsp:param value="<%=studyInvFlag%>" name="studyInvFlag"/>
								<jsp:param value="editable" name="mode"/>
							</jsp:include>					
						</td>
				    </tr>				
				</table>
				<P class="sectionHeadingsFrm"> <%=LC.L_Std_Definition%><%--Study Definition*****--%> </P>
			    <table width="100%" cellspacing="2" cellpadding="2" border="0" class="stdfldheight basetbl midAlign" >



				<!--tr>
						<% if (autoGenStudy.equals("1")){ %>
						<td width="20%"> Study Number  <FONT class="Mandatory">* </FONT> (system-generated) </td>
						<%}else { %>
				        <td width="20%"> Study Number <FONT class="Mandatory">* </FONT> </td>
				        <%} %>
				        <td>
				       <!-- <input type="text" name="studyNumber" size = 15 MAXLENGTH = 20 value="<%=studyNumber%>" dojoType="ValidationTextBox" required="true" >-->
   				        <!--input type="text" name="studyNumber" size = 40 MAXLENGTH = 100 value="<%=studyNumber%>" dojoType="ValidationTextBox" required="true" >
    					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href="#" onClick="studyFunctions.openwinstudyid('<%=studyId%>','<%=mode%>',<%=stdRight%>);"><img src="../images/jpg/preview.gif" border="0"> MORE STUDY DETAILS</A>
				        </td>
				</tr-->

				
			<tr>
				<%

				if (hashPgCustFld.containsKey("studynumber")) {

				int fldNumStudyNum = Integer.parseInt((String)hashPgCustFld.get("studynumber"));
				String StudyNumMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStudyNum));
				String StudyNumLable = ((String)cdoPgField.getPcfLabel().get(fldNumStudyNum));
				String StudyNumAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStudyNum));

				disableStr ="";
				readOnlyStr ="";

				if(StudyNumAtt == null) StudyNumAtt ="";
  				if(StudyNumMand == null) StudyNumMand ="";
				%>
				<%if(!StudyNumAtt.equals("0")) {%>
				<td width="20%">
				<label id="studyNumber_Label"> 
				<%if(StudyNumLable !=null){%>
				<%=StudyNumLable%>
				<%} else {%>
				  <%=LC.L_Study_Number%><%--Study Number*****--%>
				<% }%>
				</label> 
				<%if (StudyNumMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomstudynum">* </FONT>
		 	   <% }
				//KM:31July08
				if (autoGenStudy.equals("1") && mode.equals("N")) {
			   %>
			   (system-generated)
			   <%} if(StudyNumAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (StudyNumAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>
			  <td>
			  			<!-- Changes By Sudhir on 05-Apr-2012 for Bug #9121-->
   				        <input type="text" id="studyNumber" name="studyNumber" size = 40 MAXLENGTH = 100   <%=disableStr%> <%=readOnlyStr%>  value="<%=studyNumber%>" >
   				        <!-- Changes By Sudhir on 05-Apr-2012 for Bug #9121-->
   				        <input type="hidden" id="studyNumberOld" name="studyNumberOld" value="<%=studyNumber%>">
				        </td>

				<% } else { %>

				 <input type="hidden" id="studyNumber" name="studyNumber" size = 40 MAXLENGTH = 100   value="<%=studyNumber%>" >
				 <input type="hidden" id="studyNumberOld" name="studyNumberOld" value="<%=studyNumber%>">
				<% }}  else {%>

				<%

				//Modified by Manimaran on July31,08
				if (autoGenStudy.equals("1")){ %>

						<td width="20%"><label id="studyNumber_Label"><%=LC.L_Study_Number%><%--Study Number*****--%></label> <FONT class="Mandatory" id="mandsnumber">* </FONT> <% if (mode.equals("N")) { %> (<%=LC.L_SysHypenGen%><%--system-generated*****--%>) <%}%> </td>
						<%}else { %>
				        <td width="20%"><label id="studyNumber_Label"><%=LC.L_Study_Number%><%--Study Number*****--%></label> <FONT class="Mandatory" id="mandsnumber">* </FONT> </td>
				        <%} %>
				        <td>
				       <!-- <input type="text" name="studyNumber" size = 15 MAXLENGTH = 20 value="<%=studyNumber%>" dojoType="ValidationTextBox" required="true" >-->
				       <!-- Changes By Sudhir on 05-Apr-2012 for Bug #9121-->
   				        <input type="text" id="studyNumber" name="studyNumber" size = 40 MAXLENGTH = 100 value="<%=studyNumber%>" >
   				        <!-- Changes By Sudhir on 05-Apr-2012 for Bug #9121-->
   				        <input type="hidden" id="studyNumberOld" name="studyNumberOld" value="<%=studyNumber%>">
 		         </td>
			 <%}%>
				</tr>


				<tr>
				<%
				if (hashPgCustFld.containsKey("studytitle")) {

				int fldNumStudyTitle = Integer.parseInt((String)hashPgCustFld.get("studytitle"));
				String stdTitleMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStudyTitle));
				String stdTitleLable = ((String)cdoPgField.getPcfLabel().get(fldNumStudyTitle));
				String stdTitleAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStudyTitle));

				disableStr ="";
				readOnlyStr ="";

				if(stdTitleAtt == null) stdTitleAtt ="";
  				if(stdTitleMand == null) stdTitleMand ="";

				if(!stdTitleAtt.equals("0")) {

				if(stdTitleLable !=null){
				%>
				<td width="20%">
				 <%=stdTitleLable%>
				<%} else {%> <td width="20%">
				<%=LC.L_Title%><%--Title*****--%>
				<% }


			   if (stdTitleMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomstudytitle">* </FONT>
		 	   <% }
			   %>

			   <%if(stdTitleAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (stdTitleAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

              <td class="textareaheight"> <TextArea  id="studyTitle" name="studyTitle" rows=4 cols=93 MAXLENGTH = 1000  <%=disableStr%>  <%=readOnlyStr%> ><%=studyTitle%></TextArea> </td>

			  <% } else { %>
			   <TextArea  id="studyTitle" name="studyTitle"  Style = "display:none; visibility:hidden"  rows=4 cols=93 MAXLENGTH = 1000><%=studyTitle%></TextArea>

			 <% }}  else {%>

				<td width="20%"><%=LC.L_Title%><%--Title*****--%> <FONT class="Mandatory" id="mandtitle">* </FONT> </td>
			        <td  class="textareaheight">
				        <TextArea   id="studyTitle" name="studyTitle" rows=4 cols=93 MAXLENGTH = 1000  ><%=studyTitle%></TextArea>
			        </td>
			 <%}%>
			 </tr>



				<tr>
				<%
				if (hashPgCustFld.containsKey("objective")) {

				int fldNumObjective = Integer.parseInt((String)hashPgCustFld.get("objective"));
				String objectiveMand = ((String)cdoPgField.getPcfMandatory().get(fldNumObjective));
				String objectiveLable = ((String)cdoPgField.getPcfLabel().get(fldNumObjective));
				String objectiveAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumObjective));

				disableStr ="";
				readOnlyStr ="";

				if(objectiveAtt == null) objectiveAtt ="";
  				if(objectiveMand == null) objectiveMand ="";

				if(!objectiveAtt.equals("0")) {

				if(objectiveLable !=null){
				%>
				<td width="20%">
				 <%=objectiveLable%>
				<%} else {%> <td width="20%">
				<%=LC.L_Objective%><%--Objective*****--%>
				<% }


			   if (objectiveMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomobjective">* </FONT>
		 	   <% }
			   %>

			   <%if(objectiveAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (objectiveAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

			  <td  class="textareaheight">  <TextArea name="studyObjective" rows=3 cols=93  <%=disableStr%>  <%=readOnlyStr%> ><%=studyObjective%></TextArea>  </td>

			  <% } else { %>

			  <TextArea name="studyObjective" rows=3  Style = "display:none; visibility:hidden" cols=93 > <%=studyObjective%> </TextArea>

			  <% } }  else {%>

				<td width="20%"><%=LC.L_Objective%><%--Objective*****--%></td>
			     <td  class="textareaheight">
						<!--Modified by Manimaran to fix the Bug2741 -->
					    <TextArea   name="studyObjective" rows=3 cols=93  ><%=studyObjective%></TextArea>
	 	        </td>
			 <%}%>
			 </tr>


				<tr>
				<%
				if (hashPgCustFld.containsKey("summary")) {

				int fldNumSummary = Integer.parseInt((String)hashPgCustFld.get("summary"));
				String summaryMand = ((String)cdoPgField.getPcfMandatory().get(fldNumSummary));
				String summaryLable = ((String)cdoPgField.getPcfLabel().get(fldNumSummary));
				String summaryAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSummary));

				disableStr ="";
				readOnlyStr ="";

				if(summaryAtt == null) summaryAtt ="";
  				if(summaryMand == null) summaryMand ="";

				if(!summaryAtt.equals("0")) {

				if(summaryLable !=null){
				%>
				<td width="20%">
				 <%=summaryLable%>
				<%} else {%> <td width="20%">
				<%=LC.L_Summary%><%--Summary*****--%>
				<% }


			   if (summaryMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomsummary">* </FONT>
		 	   <% }
			   %>

			   <%if(summaryAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (summaryAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

			  <td  class="textareaheight"> 	<TextArea name="studySummary" rows=3 cols=93  <%=disableStr%>  <%=readOnlyStr%> ><%=studySummary%></TextArea>  </td>

			  <% } else { %>

			  <TextArea   name="studySummary" rows=3 Style = "display:none; visibility:hidden" cols=93 ><%=studySummary%></TextArea>

			  <% }}  else {%>

			     <td width="20%"><%=LC.L_Summary%><%--Summary*****--%></td>
		          <td  class="textareaheight">
				    <!--Modified by Manimaran to fix the Bug2741 -->
					<TextArea   name="studySummary" rows=3 cols=93><%=studySummary%></TextArea>
			      </td>
			 <%}%>
			 </tr>
			 
			 
			 
			 <%--[Start] CTRP-22471 : Rows added for storing NCI Trial Identifier and NCT Number : Raviesh --%>
			 
			 <tr id="NCIRow" style="display: none;">
				<%
				if (hashPgCustFld.containsKey("NCITrialIdentifier")) {

				int fldNumNCITrialIdentifier = Integer.parseInt((String)hashPgCustFld.get("NCITrialIdentifier"));
				String stdNCITrialIdentifierMand = ((String)cdoPgField.getPcfMandatory().get(fldNumNCITrialIdentifier));
				String stdNCITrialIdentifierLabel = ((String)cdoPgField.getPcfLabel().get(fldNumNCITrialIdentifier));
				String stdNCITrialIdentifierAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumNCITrialIdentifier));

				disableStr ="";
				readOnlyStr ="";

				if(stdNCITrialIdentifierAtt == null) stdNCITrialIdentifierAtt ="";
  				if(stdNCITrialIdentifierMand == null) stdNCITrialIdentifierMand ="";

				if(!stdNCITrialIdentifierAtt.equals("0")) {

				if(stdNCITrialIdentifierLabel !=null){
				%>
				<td width="20%">
				 <%=stdNCITrialIdentifierLabel%>
				<%} else {%> <td width="20%">
				<%=LC.CTRP_DraftNCITrialId%><%--NCI Trial Identifier*****--%>
				<% }


			   if (stdNCITrialIdentifierMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomstudyncitrialidentifier">* </FONT>
		 	   <% }
			   %>

			   <%if(stdNCITrialIdentifierAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (stdNCITrialIdentifierAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

              <td>
             	 <input type="text" name="NCITrialIdentifier" size = 40 MAXLENGTH = 35   <%=disableStr%> <%=readOnlyStr%>  value="<%=nciTrialIdentifier%>" />
              </td>

			  <% } else { %>
			  <input type="text" name="NCITrialIdentifier" style="display:none;" size = 40 MAXLENGTH = 35 value="<%=nciTrialIdentifier%>" />

			 <% }}  else {%>

				<td width="20%"><%=LC.CTRP_DraftNCITrialId%><%--NCI Trial Identifier*****--%></td>
			        <td>
			        <input type="text" name="NCITrialIdentifier" size = 40 MAXLENGTH = 35 value="<%=nciTrialIdentifier%>" />
			        </td>
			 <%}%>
			 </tr>
			 

			<tr>
				<%
				if (hashPgCustFld.containsKey("NCTNumber")) {

				int fldNumNctNumber = Integer.parseInt((String)hashPgCustFld.get("NCTNumber"));
				String stdNctNumberMand = ((String)cdoPgField.getPcfMandatory().get(fldNumNctNumber));
				String stdNctNumberLabel = ((String)cdoPgField.getPcfLabel().get(fldNumNctNumber));
				String stdNctNumberAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumNctNumber));

				disableStr ="";
				readOnlyStr ="";

				if(stdNctNumberAtt == null) stdNctNumberAtt ="";
  				if(stdNctNumberMand == null) stdNctNumberMand ="";

				if(!stdNctNumberAtt.equals("0")) {

				if(stdNctNumberLabel !=null){
				%>
				<td width="20%">
				 <%=stdNctNumberLabel%>
				<%} else {%> <td width="20%">
				<%=LC.CTRP_DraftNCTNumber%><%--NCT Number*****--%>
				<% }


			   if (stdNctNumberMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomstudynctnumber">* </FONT>
		 	   <% }
			   %>

			   <%if(stdNctNumberAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (stdNctNumberAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

              <td>
              	<input type="text" name="nctNumber" id="nctNumber" size = 40 MAXLENGTH = 100 <%=disableStr%>  <%=readOnlyStr%> value="<%=nctNumber%>" />
              </td>

			  <% } else { %>
				<input type="text" name="nctNumber" id="nctNumber" style="display:none; visibility: hidden;" size = 40 MAXLENGTH = 100 value="<%=nctNumber%>" />
			 <% }}  else {%>

				<td width="20%"><%=LC.CTRP_DraftNCTNumber%><%--NCT Number*****--%></td>
			        <td>
			        	<input type="text" name="nctNumber" id="nctNumber" size = 40 MAXLENGTH = 100 value="<%=nctNumber%>" />
			        </td>
			 <%}%>
			 </tr>
			 
			 <%--[END] CTRP-22471 : Rows added for storing NCI Trial Identifier and NCT Number : Raviesh --%>
				 <tr style="<%=((StringUtil.isEmpty(publishRadioAtt))? emptyStr : hideStr)%>">
			        <td colspan=2 align="center"> <%=MC.M_DoYouWant_InfoToPublic%><%--Do you want information in this section to be available to the public?*****--%>
				&nbsp;&nbsp;
				        <% if (stdSecRights[secCount] == '1') { %>
					        <input type="Radio" name="studyPubFlag1" value='1' Checked><%=LC.L_Yes%><%--Yes*****--%>
					        <input type="Radio" name="studyPubFlag1" value='0' ><%=LC.L_No%><%--No*****--%>
				        <%} else if (stdSecRights[secCount] == '0') {%>
					        <input type="Radio" name="studyPubFlag1" value='1'><%=LC.L_Yes%><%--Yes*****--%>
					        <input type="Radio" name="studyPubFlag1" value='0' Checked><%=LC.L_No%><%--No*****--%>
				       <%} else {%>
					        <input type="Radio" name="studyPubFlag1" value='1'><%=LC.L_Yes%><%--Yes*****--%>
					        <input type="Radio" name="studyPubFlag1" value='0'><%=LC.L_No%><%--No*****--%>
				        <%} secCount++; %>
				        <input type=hidden name=studyPubFlag1 value=''>
						&nbsp; <A href="#"><img src="../images/jpg/help.jpg" border="0"	onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS(publicVsNonpublicStr)%>',CAPTION,'<%=LC.L_PublicNonPublicInfo%>',RIGHT, ABOVE,OFFSETY,70,WRAP);" onmouseout="return nd();" alt=""></img></A>
					</td>
				</tr>
		    </table>

				<P class = "sectionHeadingsFrm"> <%=LC.L_Study_Details%><%--Study Details*****--%> </P>
				<table width="100%" cellspacing="2" cellpadding="2" border="0" class="basetbl midAlign">

<tr>

       <%if (hashPgCustFld.containsKey("purpose")) {
			int fldNumPurpose = Integer.parseInt((String)hashPgCustFld.get("purpose"));
			String PurposeMand = ((String)cdoPgField.getPcfMandatory().get(fldNumPurpose));
			String PurposeLable = ((String)cdoPgField.getPcfLabel().get(fldNumPurpose));
			purposeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumPurpose));

			if(purposeAtt == null) purposeAtt ="";
			if(PurposeMand == null) PurposeMand ="";

			if(!purposeAtt.equals("0")) {
			if(PurposeLable !=null){
			%><td width="20%">
			<%=PurposeLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Primary_Purpose%><%--Primary Purpose*****--%>
			<%}

			if (PurposeMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompurpose">* </FONT>
			<% }
		 %>
      </td>
      <td>
		<%=studyPurpose%>
		<%if(purposeAtt.equals("1")) {%>
			<input type="hidden" name="studyPurpose" value=""> 
		<%} else if(purposeAtt.equals("2")) {%><!--KM-->
	 		<input type="hidden" name="studyPurpose" value="<%=studyPurpse%>">
		 <%}%>
     </td>

	 <%} else { %>

	 <input type="hidden" name="studyPurpose" value=<%=studyPurpse%> >

	 <%}} else {%>
	  <td width="20%"> <%=LC.L_Primary_Purpose%><%--Primary Purpose*****--%> </td>
	  <td> <%=studyPurpose%> </td>

	 <% }%>

     </tr>
				<tr>
				<%
				if (hashPgCustFld.containsKey("agentordevice")) {

				int fldNumAgentDev = Integer.parseInt((String)hashPgCustFld.get("agentordevice"));
				String agentMand = ((String)cdoPgField.getPcfMandatory().get(fldNumAgentDev));
				String agentLable = ((String)cdoPgField.getPcfLabel().get(fldNumAgentDev));
				String agentAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumAgentDev));

				disableStr ="";
				readOnlyStr ="";

				if(agentAtt == null) agentAtt ="";
  				if(agentMand == null) agentMand ="";

				if(!agentAtt.equals("0")) {

				if(agentLable !=null){
				%>
				<td width="20%">
				<%=agentLable%>
				<%} else {%> <td width="20%">
				 <%=LC.L_AgentOrDevice%><%--Agent/Device*****--%>
				<% }


			   if (agentMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomagent">* </FONT>
		 	   <% }
			   %>

			   <%if(agentAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (agentAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

			  <td>  <input type="text" name="studyProduct" size = 40 MAXLENGTH = 100  <%=disableStr%>  <%=readOnlyStr%>   value="<%=studyProduct%>" >    </td>

			  <% }  else { %>

			  <input type="hidden" name="studyProduct" size = 40 MAXLENGTH = 100  value="<%=studyProduct%>" >

			 <% } }  else {%>

			 <td width="20%"> <%=LC.L_AgentOrDevice%><%--Agent/Device*****--%>  </td>
			 <td>
			       <input type="text" name="studyProduct" size = 40 MAXLENGTH = 100 value="<%=studyProduct%>" >
			 </td>
			 <%}%>
			 </tr>



		<tr>

       <%if (hashPgCustFld.containsKey("division")) {
			int fldNumDiv = Integer.parseInt((String)hashPgCustFld.get("division"));
			String divMand = ((String)cdoPgField.getPcfMandatory().get(fldNumDiv));
			String divLable = ((String)cdoPgField.getPcfLabel().get(fldNumDiv));
			divAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDiv));

			if(divAtt == null) divAtt ="";
			if(divMand == null) divMand ="";

			if(!divAtt.equals("0")) {
			if(divLable !=null){
			%><td width="20%">
			<%=divLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Division%><%--Division*****--%>
			<%}

			if (divMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomdivision">* </FONT>
			<% }
		 %>
      </td>
      <td>
		<%=studyDivision%>
		<%if(divAtt.equals("1")) {%>
			<input type="hidden" name="studyDivision" value=""> 
		<%} else if(divAtt.equals("2")) {%><!--KM-->
	 		<input type="hidden" name="studyDivision" value="<%=studyDiv%>">
		 <%}%>
     </td>

	 <%} else { %>

	 <input type="hidden" name="studyDivision" value=<%=studyDiv%> >

	 <%}} else {%>
	  <td width="20%"> <%=LC.L_Division%><%--Division*****--%> </td>
	  <td width="31%"> <%=studyDivision%> </td>

	 <% }%>
	 	<td>
		<input type="checkbox" name="ccsgdt" id="ccsgdt" <%if(ccsgdtFlag.equals("1")){%>checked<%}%>><%=LC.L_CCSG_DT_Reportable%></input>
		<input type='hidden' value='CcsgRprtFlag' name='CcsgReportFlag'/>
		</td>
     </tr>



     <tr>

	 <%     if (hashPgCustFld.containsKey("therapeuticarea")) {
			int fldNumTarea = Integer.parseInt((String)hashPgCustFld.get("therapeuticarea"));
			String tareaMand = ((String)cdoPgField.getPcfMandatory().get(fldNumTarea));
			String tareaLable = ((String)cdoPgField.getPcfLabel().get(fldNumTarea));
			tareaAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumTarea));

			if(tareaAtt == null) tareaAtt ="";
			if(tareaMand == null) tareaMand ="";

			if(!tareaAtt.equals("0")) {
			if(tareaLable !=null){
			%><td width="20%">
			<%=tareaLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Therapeutic_Area%><%--Therapeutic Area*****--%>
			<%}

			if (tareaMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomtarea">* </FONT>
			<% }
	  %>
      </td>
      <td>
      	 <span id = "span_tarea"> <%=studyTArea%> </span>
		 <%if(tareaAtt.equals("1")) {%>
		 	<input type="hidden" name="studyTArea" value="">
		 <%} else if(tareaAtt.equals("2")){%>
		 	<input type="hidden" name="studyTArea" value="<%=studyB.getStudyTArea()%>">
		 <%}%>
      </td>

	  <%} else if(tareaAtt.equals("0")) {%>

	  <input type="hidden" name="studyTArea" value="<%=studyB.getStudyTArea()%>"  > <!--KM-->

	 <%}} else {%>
	   <td width="20%"> <%=LC.L_Therapeutic_Area%><%--Therapeutic Area*****--%> <FONT class="Mandatory" id="mandtarea">* </FONT> </td>
	   <td><span id = "span_tarea"> <%=studyTArea%> </span></td>
	 <% }%>

	 </tr>


				<tr>
				<%
				if (hashPgCustFld.containsKey("diseasesite")) {

				int fldNumDiseaseSite = Integer.parseInt((String)hashPgCustFld.get("diseasesite"));
				String disSiteMand = ((String)cdoPgField.getPcfMandatory().get(fldNumDiseaseSite));
				String disSiteLable = ((String)cdoPgField.getPcfLabel().get(fldNumDiseaseSite));
				String disSiteAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDiseaseSite));

				disableStr ="";
				readOnlyStr ="";

				if(disSiteAtt == null) disSiteAtt ="";
  				if(disSiteMand == null) disSiteMand ="";

				if(!disSiteAtt.equals("0")) {

				if(disSiteLable !=null){
				%>
				<td width="20%">
				<%=disSiteLable%>
				<%} else {%> <td width="20%">
				 <%=LC.L_Disease_Site%><%--Disease Site*****--%>
				<% }


			   if (disSiteMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomdissite">* </FONT>
		 	   <% }
			   %>

			   <%if(disSiteAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }

  		       %>
 			  </td>


			  <td><input type="text" id="disSitename" name="disSitename" size="20" value="<%=StringUtil.escapeSpecialCharHTML(disSitename)%>" readonly <%=disableStr%>>&nbsp;
			  <%if(!disSiteAtt.equals("1") && !disSiteAtt.equals("2")) { %>
			<a href="#" onClick="studyFunctions.selectDisease(studyFunctions.formObj)"><%=LC.L_Select_DiseaseSites%><%--SELECT DISEASE SITE(S)*****--%></a></td>
			  <input type="hidden" id="disSiteid" name="disSiteid" value="<%=disSiteid%>">

			  <% }} else { %>
			 <input type="hidden" id="disSiteid" name="disSiteid" value="<%=disSiteid%>">

			 <% } }  else { %>

			  <td width="20%"> <%=LC.L_Disease_Site%><%--Disease Site*****--%> </td>
			  <td><input type="text" id="disSitename" name="disSitename" size="20" value="<%=StringUtil.escapeSpecialCharHTML(disSitename)%>" readonly>&nbsp;&nbsp;<a href="#" onClick="studyFunctions.selectDisease(studyFunctions.formObj)"><%=LC.L_Select_DiseaseSites%><%--SELECT DISEASE SITE(S)*****--%></a>  </td>
			 	<input type="hidden" id="disSiteid" name="disSiteid" value="<%=disSiteid%>">
			 <%}%>
			 </tr>



			<tr>
				<%
				if (hashPgCustFld.containsKey("specificsites")) {

				int fldNumSpecsites = Integer.parseInt((String)hashPgCustFld.get("specificsites"));
				String specSiteMand = ((String)cdoPgField.getPcfMandatory().get(fldNumSpecsites));
				String specSiteLable = ((String)cdoPgField.getPcfLabel().get(fldNumSpecsites));
				String specSiteAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSpecsites));

				disableStr ="";
				readOnlyStr ="";

				if(specSiteAtt == null) specSiteAtt ="";
  				if(specSiteMand == null) specSiteMand ="";

				if(!specSiteAtt.equals("0")) {

				if(specSiteLable !=null){
				%>
				<td width="20%">
				<%=specSiteLable%>
				<%} else {%> <td width="20%">
				 <%=LC.L_Specific_Sites%><%--Specific Sites*****--%>
				<% }


			   if (specSiteMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomspecsite">* </FONT>
		 	   <% }
			   %>

			   <%if(specSiteAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
 		       %>
 			  </td>

				<td><input type="text" name="ICDcode1" size="20" value="<%=ICDCode1%>" readonly  <%=disableStr%> >&nbsp;&nbsp;


				<%if(!specSiteAtt.equals("1") && !specSiteAtt.equals("2")) {%>
				<a href="#" onClick="return studyFunctions.openWinICD('ICDcode1','<%=lkpId%>','<%=lkpColumn%>')"><%=LC.L_Select_Site1%><%--SELECT SITE 1*****--%></a>
   			    <%}%>


			   </td></tr>
			   <tr>
					<td width="20%">&nbsp;</td>
				<td><input type="text" name="ICDcode2" size="20"
					value="<%=ICDCode2%>" readonly  <%=disableStr%> >&nbsp;&nbsp;

				<%if(!specSiteAtt.equals("1") && !specSiteAtt.equals("2")) {%>
				<a href="#" onClick="return studyFunctions.openWinICD('ICDcode2','<%=lkpId%>','<%=lkpColumn%>')"><%=LC.L_Select_Site2%><%--SELECT SITE 2*****--%></a></td>
				 <%}%>


			  <% } else { %>
			  <input type="hidden" name="ICDcode1" size="20" value="<%=ICDCode1%>" >
			  <input type="hidden" name="ICDcode2" size="20" value="<%=ICDCode2%>" >

			 <% }}  else {%>

					  <!--Added by Manimaran for September Enhancement2 to select the ICD codes.-->
					<td width="20%"> <%=LC.L_Specific_Sites%><%--Specific Sites*****--%> </td>
					<td><input type="text" name="ICDcode1" size="20" value="<%=ICDCode1%>" readonly>&nbsp;&nbsp;<a href="#" onClick="return studyFunctions.openWinICD('ICDcode1','<%=lkpId%>','<%=lkpColumn%>')"><%=LC.L_Select_Site1%><%--SELECT SITE 1*****--%></a>
				    </td></tr>
			 <tr>

					<td width="20%">&nbsp;</td>
					<td><input type="text" name="ICDcode2" size="20"
					value="<%=ICDCode2%>" readonly>&nbsp;&nbsp;<a href="#" onClick="return studyFunctions.openWinICD('ICDcode2','<%=lkpId%>','<%=lkpColumn%>')"><%=LC.L_Select_Site2%><%--SELECT SITE 2*****--%></a></td>

			 <%}%>
			 </tr>

			 <!--<tr>
				    	<td width="200">ICD-9 Code</td>
				        <td><input type="text" name="ICDcode1" size="10" " >
				        <input type="text" name="ICDcode2" size="10" " >
				        <input type="text" name="ICDcode3" size="10" " >
				        <a href="#" onClick="">Select ICD-9</a></td>
				    </tr>-->
				    <!--<tr>
						<td width="20%">Local Sample Size </td>
				        <td>
					        <input type="text" name="studySize" size = 15 value="<%=studySize%>" >
				        </td>
   	         </tr>-->



			<tr>
				<%
				if (hashPgCustFld.containsKey("nss")) {

				int fldNumNss = Integer.parseInt((String)hashPgCustFld.get("nss"));
				String nssMand = ((String)cdoPgField.getPcfMandatory().get(fldNumNss));
				String nssLable = ((String)cdoPgField.getPcfLabel().get(fldNumNss));
				String nssAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumNss));

				disableStr ="";
				readOnlyStr ="";

				if(nssAtt == null) nssAtt ="";
  				if(nssMand == null) nssMand ="";

				if(!nssAtt.equals("0")) {

				if(nssLable !=null){
				%>
				<td width="20%">
				<%=nssLable%>
				<%} else {%> <td width="20%">
				<%=LC.L_National_SampleSize%><%--National Sample Size*****--%>
				<% }


			   if (nssMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomnss">* </FONT>
		 	   <% }
			   %>

			   <%if(nssAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (nssAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>


			  <td>  <input type="text" name="nStudySize" size = 20 maxlength="10" value="<%=nStudySize%>"  <%=readOnlyStr%> <%=disableStr%> >&nbsp; <%-- YK BUG#5564 --%>

		      <%if(!nssAtt.equals("1") && !nssAtt.equals("2")) {%>
			  <A id="nssLink" href="#" onClick="studyFunctions.openLSampleSize('<%=studyId%>','<%=mode%>');"><%=LC.L_Local_SampleSize%><%--LOCAL SAMPLE SIZE*****--%></A></td>
			  <%}%>

			  <% } else {  %>

			  <input type="hidden" name="nStudySize" size = 20 value="<%=nStudySize%>" >

			  <% }}  else {%>

			    <td width="20%"><%=LC.L_National_SampleSize%><%--National Sample Size*****--%> </td>
				<td>
					        <input type="text" name="nStudySize" size = 20 maxlength="10" value="<%=nStudySize%>" >&nbsp; <%-- YK BUG#5564 --%>
				        <A id="nssLink" href="#" onClick="studyFunctions.openLSampleSize('<%=studyId%>','<%=mode%>');"><%=LC.L_Local_SampleSize%><%--LOCAL SAMPLE SIZE*****--%></A></td>
			 <%}%>
			 </tr>



				<%

				String durationAtt = "";
				if (hashPgCustFld.containsKey("studyduration")) {
				int fldNumDuration = Integer.parseInt((String)hashPgCustFld.get("studyduration"));
				durationAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDuration));
				if (durationAtt == null) durationAtt ="";
				}

				%>

				 <%
					if(durationAtt.equals("1"))
				 		durationUnit.append("<SELECT NAME=durationUnit disabled id=durunit>") ;
				    else
				 		durationUnit.append("<SELECT NAME=durationUnit id=durunit>") ;


					if(studyDurationUnit != null && (!studyDurationUnit.equals("null")) ){

						if (studyDurationUnit.equals("days")) {
							durationUnit.append("<OPTION value='days' SELECTED>"+LC.L_Days_Lower/*days*****/+"</OPTION>");
						} else {
							durationUnit.append("<OPTION value = 'days'>"+LC.L_Days_Lower/*days*****/+"</OPTION>");
						}
						if (studyDurationUnit.equals("weeks")) {
							durationUnit.append("<OPTION value = 'weeks' SELECTED>"+LC.L_Weeks_Lower/*weeks*****/+"</OPTION>");
						} else {
							durationUnit.append("<OPTION value ='weeks'>"+LC.L_Weeks_Lower/*weeks*****/+"</OPTION>");
						}
						if (studyDurationUnit.equals("months")) {
							durationUnit.append("<OPTION value ='months' SELECTED>"+LC.L_Months_Lower/*months*****/+"</OPTION>");
						} else {
							durationUnit.append("<OPTION value ='months'>"+LC.L_Months_Lower/*months*****/+"</OPTION>");
						}
						if (studyDurationUnit.equals("years")) {
							durationUnit.append("<OPTION value ='years' SELECTED>"+LC.L_Years_Lower/*years*****/+"</OPTION>");
						} else {
							durationUnit.append("<OPTION value ='years'>"+LC.L_Years_Lower/*years*****/+"</OPTION>");
						}
						if (studyDurationUnit.equals("")) {
							durationUnit.append("<OPTION value='' SELECTED> "+LC.L_Select_AnOption/*Select an Option*****/+" </OPTION>");
						}
					} else {
						durationUnit.append("<OPTION value = days>"+LC.L_Days_Lower/*days*****/+"</OPTION>");
						durationUnit.append("<OPTION value = weeks>"+LC.L_Weeks_Lower/*weeks*****/+"</OPTION>");
						durationUnit.append("<OPTION value = months>"+LC.L_Months_Lower/*months*****/+"</OPTION>");
						durationUnit.append("<OPTION value = years>"+LC.L_Years_Lower/*years*****/+"</OPTION>");
						durationUnit.append("<OPTION value='' SELECTED> "+LC.L_Select_AnOption/*Select an Option*****/+" </OPTION>");
					}
					durationUnit.append("</SELECT>");
					%>



					<tr>
				<%
				if (hashPgCustFld.containsKey("studyduration")) {

				int fldNumDuration = Integer.parseInt((String)hashPgCustFld.get("studyduration"));
				String durationMand = ((String)cdoPgField.getPcfMandatory().get(fldNumDuration));
				String durationLable = ((String)cdoPgField.getPcfLabel().get(fldNumDuration));
				durationAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDuration));

				disableStr ="";
				readOnlyStr ="";

				if(durationAtt == null) durationAtt ="";
  				if(durationMand == null) durationMand ="";

				if(!durationAtt.equals("0")) {

				if(durationLable !=null){
				%>
				<td width="20%">
				<%=durationLable%>
				<%} else {%> <td width="20%">
				 <%=LC.L_Study_Duration%><%--Study Duration*****--%>
				<% }


			   if (durationMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomduration">* </FONT>
		 	   <% }
			   %>

			   <%if(durationAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (durationAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>


			  <td>
 	          <input type="text" name="studyDuration" id="stdDuration" size = 20 MAXLENGTH = 10    <%=disableStr%>  <%=readOnlyStr%>   value="<%=studyDuration%>" >
			  <%=durationUnit%>
			  </td>

			  <% } else { %>

	  	      <input type="hidden" name="studyDuration" size = 20 MAXLENGTH = 10  value="<%=studyDuration%>" >
			  <input type="hidden" name="durationUnit"  value= "<%=studyDurationUnit%>" >


			 <% }}  else {%>

			   <td width="20%"> <%=LC.L_Study_Duration%><%--Study Duration*****--%> </td>
			   <td>
				        <input type="text" name="studyDuration" id="stdDuration" size = 20 MAXLENGTH = 10 value="<%=studyDuration%>" >
				        <%=durationUnit%>
				</td>
			 <%}%>
			 </tr>


				<tr>
				<%
				if (hashPgCustFld.containsKey("estimatedbegindt")) {

				int fldNumEstDt = Integer.parseInt((String)hashPgCustFld.get("estimatedbegindt"));
				String estDtMand = ((String)cdoPgField.getPcfMandatory().get(fldNumEstDt));
				String estDtLable = ((String)cdoPgField.getPcfLabel().get(fldNumEstDt));
				String estDtAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumEstDt));

				disableStr ="";
				readOnlyStr ="";

				if(estDtAtt == null) estDtAtt ="";
  				if(estDtMand == null) estDtMand ="";

				if(!estDtAtt.equals("0")) {

				if(estDtLable !=null){
				%>
				<td width="20%">
				<%=estDtLable%>
				<%} else {%> <td width="20%">
				 <%=LC.L_Estimated_BeginDate%><%--Estimated Begin Date*****--%>
				<% }


			   if (estDtMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomestdt">* </FONT>
		 	   <% }
			   %>

			   <%if(estDtAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (estDtAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			</td>
			<%-- INF-20084 Datepicker-- AGodara --%>
			<td colspan=1>
			<%if (StringUtil.isEmpty(estDtAtt)){ %>
				<input type="text" id="studyEstBgnDate" name="studyEstBgnDate" class="datefield" size = 20 MAXLENGTH = 20 value ="<%=studyEstBgnDate%>">
			<%} else if(estDtAtt.equals("1") || estDtAtt.equals("2")) {%>
				<input type="text" id="studyEstBgnDate" name="studyEstBgnDate" <%=disableStr%> <%=readOnlyStr%> size = 20 MAXLENGTH = 20 value="<%=studyEstBgnDate%>">
			<%}%>
			</td>
		  <%}else { %>
				<input type="hidden" id="studyEstBgnDate" name="studyEstBgnDate" size = 20  MAXLENGTH = 20 value="<%=studyEstBgnDate%>">
		<%}}else{%>
				<td width="20%"> <%=LC.L_Estimated_BeginDate%><%--Estimated Begin Date*****--%></td>
			<td colspan=1>
				<input type="text" id="studyEstBgnDate" name="studyEstBgnDate" class="datefield" size = 20 MAXLENGTH = 20 value="<%=studyEstBgnDate%>" READONLY>
			</td>
		<%}%>
		 </tr>



<!--					<tr height="10">
					    <td></td>
						<td>
							<A href=# onClick="return fnShowCalendar(studyFunctions.studyEstBgnDate)"><input type="image" src="./images/calendar.jpg" align="absmiddle" border="0"></A>
					    </td>
					</tr> -->
					<tr style="<%=((StringUtil.isEmpty(publishRadioAtt))? emptyStr : hideStr)%>">
						<td colspan=2 align="center"> <%=MC.M_DoYouWant_InfoToPublic%><%--Do you want information in this section to be available to the public?*****--%>
					&nbsp;&nbsp;
					        <% if (stdSecRights[secCount] == '1') { %>
						        <input type="Radio" name="studyPubFlag2" value='1' Checked><%=LC.L_Yes%><%--Yes*****--%>
						        <input type="Radio" name="studyPubFlag2" value='0'><%=LC.L_No%><%--No*****--%>
					        <%} else if (stdSecRights[secCount] == '0') {  %>
						        <input type="Radio" name="studyPubFlag2" value='1'><%=LC.L_Yes%><%--Yes*****--%>
						        <input type="Radio" name="studyPubFlag2" value='0' Checked><%=LC.L_No%><%--No*****--%>
					       <% } else {%>
						        <input type="Radio" name="studyPubFlag2" value='1'><%=LC.L_Yes%><%--Yes*****--%>
					    	    <input type="Radio" name="studyPubFlag2" value='0'><%=LC.L_No%><%--No*****--%>
					        <%} secCount++; %>
      					        <input type=hidden name=studyPubFlag2 value=''>
							&nbsp; <A href="#"><img src="../images/jpg/help.jpg" border="0"	onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS(publicVsNonpublicStr)%>',CAPTION,'<%=LC.L_PublicNonPublicInfo%>',RIGHT, ABOVE,OFFSETY,70,WRAP);" onmouseout="return nd();" alt=""></img></A>
						</td>
			      	</tr>
	    		</table>
    			<P class = "sectionHeadingsFrm"> <%=LC.L_Study_Design%><%--Study Design*****--%> </P>
			    <table width="100%" cellspacing="2" cellpadding="2" border="0" class="basetbl midAlign">


				<tr>

			   <%if (hashPgCustFld.containsKey("stdphase")) {
					int fldNumPhase = Integer.parseInt((String)hashPgCustFld.get("stdphase"));
					String phaseMand = ((String)cdoPgField.getPcfMandatory().get(fldNumPhase));
					String phaseLable = ((String)cdoPgField.getPcfLabel().get(fldNumPhase));
					phaseAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumPhase));

					if(phaseAtt == null) phaseAtt ="";
					if(phaseMand == null) phaseMand ="";

					if(!phaseAtt.equals("0")) {
					if(phaseLable !=null){
					%><td width="20%">
					<%=phaseLable%>
					<%} else {%> <td width="20%">
					<%=LC.L_Phase%><%--Phase*****--%>
					<%}

					if (phaseMand.equals("1")) {
						%>
					   <FONT class="Mandatory" id="pgcustomphase">* </FONT>
					<% }
				   %>
			    </td>
			   <td>
				<%=studyPhase%>
				<%if(phaseAtt.equals("1")) {%>
					<input type="hidden" name="studyPhase" value="">
				<%} else if(phaseAtt.equals("2")) {%>
					<input type="hidden" name="studyPhase" value="<%=studyB.getStudyPhase()%>">
				<%}%>
			   </td>

			   <%} else {%>

			  <input type="hidden" name="studyPhase" value="<%=studyB.getStudyPhase()%>">


			  <%}} else {%>
				 <td width="20%"> <%=LC.L_Phase%><%--Phase*****--%> <FONT class="Mandatory" id="mandphase">* </FONT> </td>
				 <td> <%=studyPhase%> </td>
			  <% }%>

			  </tr>


		<tr>

        <%if (hashPgCustFld.containsKey("researchtype")) {
	  		int fldNumResearch = Integer.parseInt((String)hashPgCustFld.get("researchtype"));
			String researchMand = ((String)cdoPgField.getPcfMandatory().get(fldNumResearch));
			String researchLable = ((String)cdoPgField.getPcfLabel().get(fldNumResearch));
			researchAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumResearch));

			if(researchAtt == null) researchAtt ="";
			if(researchMand == null) researchMand ="";

			if(!researchAtt.equals("0")) {
			if(researchLable !=null){
			%><td width="20%">
			<%=researchLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Research_Type%><%--Research Type*****--%>
			<%}

			if (researchMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomresearchtype">* </FONT>
			<% }
		 %>
        </td>
        <td>
		  <%=studyResType%>
		  <%if(researchAtt.equals("1")) {%>
		  	<input type="hidden" name="studyResType" value="">
		  <%} else if(researchAtt.equals("2")) {%>
			<input type="hidden" name="studyResType" value="<%=studyB.getStudyResType()%>"> 
		  <%}%>
        </td>

		 <%} else  {%>

		 <input type="hidden" name="studyResType" value="<%=studyB.getStudyResType()%>"> <!--KM-->


		 <%}} else {%>
		 <td width="20%"> <%=LC.L_Research_Type%><%--Research Type*****--%> </td>
		 <td> <%=studyResType%> </td>

		 <% }%>

		 </tr>


		<tr>

        <%if (hashPgCustFld.containsKey("studyscope")) {
	  		int fldNumScope = Integer.parseInt((String)hashPgCustFld.get("studyscope"));
			String scopeMand = ((String)cdoPgField.getPcfMandatory().get(fldNumScope));
			String scopeLable = ((String)cdoPgField.getPcfLabel().get(fldNumScope));
			scopeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumScope));

			if(scopeAtt == null) scopeAtt ="";
			if(scopeMand == null) scopeMand ="";

			if(!scopeAtt.equals("0")) {
			if(scopeLable !=null){
			%><td width="20%">
			<%=scopeLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Study_Scope%><%--Study Scope*****--%>
			<%}

			if (scopeMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomscope">* </FONT>
			<% }
		 %>
        </td>
        <td>
		  <%=studyScope%>
		  <%if(scopeAtt.equals("1")) {%>
		  	<input type="hidden" name="studyScope" value="">
		  <%} else if(scopeAtt.equals("2")) {%>
		  	<input type="hidden" name="studyScope" value=<%=studyB.getStudyScope()%> >
		  <%} %>
        </td>

		 <%} else if(scopeAtt.equals("0")) {%>

		 <input type="hidden" name="studyScope" value=<%=studyB.getStudyScope()%> > <!--KM-->


		 <%}} else {%>
		 <td width="20%"> <%=LC.L_Study_Scope%><%--Study Scope*****--%></td>
		 <td><%=studyScope%></td>
		 <% }%>

		 </tr>



		<tr>

        <%if (hashPgCustFld.containsKey("studytype")) {
	  		int fldNumType = Integer.parseInt((String)hashPgCustFld.get("studytype"));
			String typeMand = ((String)cdoPgField.getPcfMandatory().get(fldNumType));
			String typeLable = ((String)cdoPgField.getPcfLabel().get(fldNumType));
			typeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumType));

			if(typeAtt == null) typeAtt ="";
			if(typeMand == null) typeMand ="";

			if(!typeAtt.equals("0")) {
			if(typeLable !=null){
			%><td width="20%">
			<%=typeLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Study_Type%><%--Study Type*****--%>
			<%}

			if (typeMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomtype">* </FONT>
			<% }
		 %>
        </td>
        <td>
		  <%=studyType%>
		  <%if(typeAtt.equals("1")) {%>
		  	<input type="hidden" name="studyType" value="">
		  <%} else if(typeAtt.equals("2")) {%>
		  	<input type="hidden" name="studyType" value="<%=StringUtil.stringToNum(studyB.getStudyType())%>" >
		  <%} %>
        </td>

		 <%} else {%>

		 <input type="hidden" name="studyType" value="<%=StringUtil.stringToNum(studyB.getStudyType())%>" >

		 <%}} else {%>
		<td width="20%"> <%=LC.L_Study_Type%><%--Study Type*****--%> </td>
		<td> <%=studyType%> </td>
		 <% }%>

		 </tr>



				<tr>
				<%
				if (hashPgCustFld.containsKey("studylinkedto")) {

				int fldNumStdLinkedTo = Integer.parseInt((String)hashPgCustFld.get("studylinkedto"));
				String stdLinkedToMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStdLinkedTo));
				String stdLinkedToLable = ((String)cdoPgField.getPcfLabel().get(fldNumStdLinkedTo));
				String stdLinkedToAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStdLinkedTo));

				disableStr ="";
				readOnlyStr ="";

				if(stdLinkedToAtt == null) stdLinkedToAtt ="";
  				if(stdLinkedToMand == null) stdLinkedToMand ="";

				if(!stdLinkedToAtt.equals("0")) {

				if(stdLinkedToLable !=null){
				%>
				<td width="20%">
				<%=stdLinkedToLable%>
				<%} else {%> <td width="20%">
				 <%=LC.L_Std_Linked_To%><%--Study Linked To*****--%>
				<% }


			   if (stdLinkedToMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomstdlinkedto">* </FONT>
		 	   <% }
			   %>

			   <%if(stdLinkedToAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }

  		       %>
 			  </td>


			  <td><Input type="text" name="studyAssocNum" size="10" value="<%=studyAssocNum%>" readonly  <%=disableStr%> >

			  <%if(!stdLinkedToAtt.equals("1") && !stdLinkedToAtt.equals("2")) {%>
			  <A href=# onClick="studyFunctions.openLookup(<%=accId%>)"><%=LC.L_Select%><%--Select*****--%></A></td>
			  <Input type="hidden" name="studyAssoc" size="20" value="<%=studyAssoc%>">

			  <% }} else { %>
			  <Input type="hidden" name="studyAssocNum" size="10" value="<%=studyAssocNum%>" >
			  <Input type="hidden" name="studyAssoc" size="20" value="<%=studyAssoc%>">

			 <% } }  else {%>

				<td width="20%"><%=LC.L_Std_Linked_To%><%--Study Linked To*****--%></td><td><Input type="text" name="studyAssocNum" size="10" value="<%=studyAssocNum%>" readonly><A href=# onClick="studyFunctions.openLookup(<%=accId%>)"><%=LC.L_Select%><%--Select*****--%></A></td>
				<Input type="hidden" name="studyAssoc" size="20" value="<%=studyAssoc%>">

			 <%}%>
			 </tr>




        <tr>

        <%if (hashPgCustFld.containsKey("blinding")) {
	  		int fldNumBlinding = Integer.parseInt((String)hashPgCustFld.get("blinding"));
			String blindingMand = ((String)cdoPgField.getPcfMandatory().get(fldNumBlinding));
			String blindingLable = ((String)cdoPgField.getPcfLabel().get(fldNumBlinding));
			blindingAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumBlinding));

			if(blindingAtt == null) blindingAtt ="";
			if(blindingMand == null) blindingMand ="";

			String blinding = studyB.getStudyBlinding();
		   	blinding = StringUtil.isEmpty(blinding)? "" : blinding;
		   				
			if(!blindingAtt.equals("0")) {
			if(blindingLable !=null){
			%><td width="20%">
			<%=blindingLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Blinding%><%--Blinding*****--%>
			<%}

			if (blindingMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomblinding">* </FONT>
			<% }
		 %>
        </td>
        <td>
		   <%=studyBlinding%>
		   <%if(blindingAtt.equals("1")) {%>
		   	<input type="hidden" name="studyBlinding" value="">
		   <%} else if (blindingAtt.equals("2")){%>
		   	<input type="hidden" name="studyBlinding" value=<%=blinding%> >
		   <%} %>
        </td>

		 <%} else  {%>

		 <input type="hidden" name="studyBlinding" value=<%=blinding%> >

		 <%}} else {%>
		<td width="20%"> <%=LC.L_Blinding%><%--Blinding*****--%> </td>
		<td> <%=studyBlinding%> </td>
		 <% }%>

		 </tr>




		 <tr>

        <%if (hashPgCustFld.containsKey("randomization")) {
	  		int fldNumRandom = Integer.parseInt((String)hashPgCustFld.get("randomization"));
			String randomMand = ((String)cdoPgField.getPcfMandatory().get(fldNumRandom));
			String randomLable = ((String)cdoPgField.getPcfLabel().get(fldNumRandom));
			randomAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumRandom));

			if(randomAtt == null) randomAtt ="";
			if(randomMand == null) randomMand ="";

			if(!randomAtt.equals("0")) {
			if(randomLable !=null){
			%><td width="20%">
			<%=randomLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Randomization%><%--Randomization*****--%>
			<%}

			if (randomMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomrandom">* </FONT>
			<% }
		 %>
        </td>
        <td>
		    <%=studyRandom%>
		    <%
		    	studyRandom = studyB.getStudyRandom();
		    	studyRandom = (StringUtil.isEmpty(studyRandom)) ?  "" : studyRandom;
		    %>
		    <%if(randomAtt.equals("1")) {%>
		    	<input type="hidden" name="studyRandom" value="">
		    <%} else if (randomAtt.equals("2")){%>
		    	<input type="hidden" name="studyRandom" value="<%=studyRandom%>" >
		    <%} %>
        </td>

		 <%} else  {%>
 			<%
 				studyRandom = studyB.getStudyRandom();
		    	studyRandom = (StringUtil.isEmpty(studyRandom)) ?  "" : studyRandom;
		    %>
		 <input type="hidden" name="studyRandom" value="<%=studyRandom%>" >

		 <%}} else {%>
		<td width="20%"> <%=LC.L_Randomization%><%--Randomization*****--%> </td>
		<td> <%=studyRandom%> </td>
		 <% }%>

		 </tr>

					<tr style="<%=((StringUtil.isEmpty(publishRadioAtt))? emptyStr : hideStr)%>">
				        <td colspan=2 align="center"> <%=MC.M_DoYouWant_InfoToPublic%><%--Do you want information in this section to be available to the public?*****--%>
					&nbsp;&nbsp;
					        <% if (stdSecRights[secCount] == '1') { %>
						        <input type="Radio" name="studyPubFlag3" value='1' Checked><%=LC.L_Yes%><%--Yes*****--%>
						        <input type="Radio" name="studyPubFlag3" value='0'><%=LC.L_No%><%--No*****--%>
					        <%} else if (stdSecRights[secCount] == '0') {  %>
						        <input type="Radio" name="studyPubFlag3" value='1'><%=LC.L_Yes%><%--Yes*****--%>
						        <input type="Radio" name="studyPubFlag3" value='0' Checked><%=LC.L_No%><%--No*****--%>
					       <% } else {%>
						        <input type="Radio" name="studyPubFlag3" value='1'><%=LC.L_Yes%><%--Yes*****--%>
					    	    <input type="Radio" name="studyPubFlag3" value='0'><%=LC.L_No%><%--No*****--%>
					        <%} secCount++; %>
					        	<input type=hidden name=studyPubFlag3 value=''>
							&nbsp; <A href="#"><img src="../images/jpg/help.jpg" border="0"	onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS(publicVsNonpublicStr)%>',CAPTION,'<%=LC.L_PublicNonPublicInfo%>',RIGHT, ABOVE,OFFSETY,70,WRAP);" onmouseout="return nd();" alt=""></img></A>
						</td>
					</tr>
				</table>
			    <P class = "sectionHeadingsFrm"> <%=LC.L_Sponsor_Information%><%--Sponsor Information*****--%> </P>
			    <%
				siteDao.getSiteValues(StringUtil.stringToNum(acc));
				counter = 0;
				%>
			    <table width="100%" cellspacing="2" cellpadding="2" border="0" class="basetbl midAlign">
			<tr>

		<%


		//JM: 20Apr2010: Enh:SW-FIN6

		String sponsorNameId = "";
		sponsorNameId = studyB.getStudySponsorName();
		sponsorNameId=(sponsorNameId==null)?"":sponsorNameId;


		int fldSpnameLkp = 0;
		String spnameLkpMand = "";
		String spnameLkpLabel = "";
		String spnameLkpAtt = "";




		if (hashPgCustFld.containsKey("sponsorlookup")) {
	  		fldSpnameLkp = Integer.parseInt((String)hashPgCustFld.get("sponsorlookup"));
			spnameLkpAtt = ((String)cdoPgField.getPcfAttribute().get(fldSpnameLkp));
			if(spnameLkpAtt == null) spnameLkpAtt ="";
		}


		if (hashPgCustFld.containsKey("sponsorname")) {
	  		int fldNumSpname = Integer.parseInt((String)hashPgCustFld.get("sponsorname"));
			String spnameMand = ((String)cdoPgField.getPcfMandatory().get(fldNumSpname));
			String spnameLable = ((String)cdoPgField.getPcfLabel().get(fldNumSpname));
			spnameAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSpname));


			if(spnameAtt == null) spnameAtt ="";
			if(spnameMand == null) spnameMand ="";

			if(!spnameAtt.equals("0")) {
				if(spnameLable !=null){
				%><td width="20%">
				 <%=spnameLable%> 
				<%} else {%> <td width="20%">
				 <%=LC.L_Sponsor_Name%><%--Sponsor Name*****--%> 
				<%}

				if (spnameMand.equals("1")) {
					%>
				   <FONT class="Mandatory" id="pgcustomspname">* </FONT>
				<%}%>
         		</td>
		        <td>
				   <%=studySponsorName%>
				   <%if(spnameAtt.equals("1")) {%>
				   		<input type="hidden" name="sponsor" value="">
				   <%} else if (randomAtt.equals("2")){%>
				    	<input type="hidden" name="sponsor" value="<%=studyB.getStudySponsorName()%>" >
				   <%} %>
		        </td>
		        <input type="hidden" name="fromSponsor" value="DD">

			<%}else{%>

				<input type="hidden" name="sponsor" value='<%=sponsorNameId%>' >

		 	<%}

		} else {%>
			<td width="20%"> <%=LC.L_Sponsor_Name%><%--Sponsor Name*****--%>  </td>
			<td> <%=studySponsorName%></td>
			<input type="hidden" name="fromSponsor" value="DD">
		<%
		}%>

	   </tr>



	<tr><%




		//JM: 20Apr2010: Enh:SW-FIN6: to add the Sponosor look up
		if ( (spnameAtt.equals("")||spnameAtt.equals("1")||spnameAtt.equals("2")) && (spnameLkpAtt.equals("1") || spnameLkpAtt.equals("")|| spnameLkpAtt.equals("2"))) {

			//Do not show sponsor look up ..........

		}else {


			//show sponsor look up ..........

			if (hashPgCustFld.containsKey("sponsorlookup")) {

				spnameLkpMand = ((String)cdoPgField.getPcfMandatory().get(fldSpnameLkp));
				spnameLkpLabel = ((String)cdoPgField.getPcfLabel().get(fldSpnameLkp));

				if(spnameLkpAtt == null) spnameLkpAtt ="";
				if(spnameLkpMand == null) spnameLkpMand ="";

				if(!spnameLkpAtt.equals("0")) {
					if(spnameLkpLabel !=null){%>
						<td width="20%">
						 <%=spnameLkpLabel%>
					<%
					} else {%>
						<td width="20%">
						 <%=LC.L_Sponsor_Name%><%--Sponsor Name*****--%> 
					<%
					}

					if (spnameLkpMand.equals("1")) {%>
					   <FONT class="Mandatory" id="pgcustomspname1">* </FONT>
					<%
					}
				  	%>
		         		</td>
		           <td>
				   <input type="text" name="sponsor1"   value='<%=(sponsorNameId.length()>0)?cdSponsorName.getCodeDesc(StringUtil.stringToNum(sponsorNameId)):""%>' readonly>
				   <%
				   if(!spnameLkpAtt.equals("1") && !spnameLkpAtt.equals("2")) {%>
						<A href="#" onClick="studyFunctions.openLookupSpnsr(studyFunctions.formObj)"><%=LC.L_Select_Sponsor%><%--Select Sponsor*****--%> </A>
						<td><input type="hidden" name="sponsorpk" value='<%=sponsorNameId%>'></td>
		   		   <%
		   		   }
		   		   if( spnameLkpAtt.equals("1") || spnameLkpAtt.equals("2")) {%>
		   		   		<input type="hidden" name="sponsorpk" value='<%=sponsorNameId%>'> <%}%>
		           </td>
		           <input type="hidden" name="fromSponsor" value="LKP">

			 	<%
			 	}else{%>
			 		<input type="hidden" name="sponsorpk" value='<%=sponsorNameId%>' >

			 	<%
			 	}

			} else {%>
				<td width="20%"> <%=LC.L_Sponsor_Name%><%--Sponsor Name*****--%>  </td>
				<td>
				<input type="text" name="sponsor1"   value='<%=(sponsorNameId.length()>0)?cdSponsorName.getCodeDesc(StringUtil.stringToNum(sponsorNameId)):""%>' readonly>
				<A href="#" onClick="studyFunctions.openLookupSpnsr(studyFunctions.formObj)"><%=LC.L_Select_Sponsor%><%--Select Sponsor*****--%></A>
				<input type="hidden" name="sponsorpk" value='<%=sponsorNameId%>'>
				<input type="hidden" name="fromSponsor" value="LKP">
				</td>
			<%
			}

		}//end of else part
		%>

		</tr>



				<tr>
				<%
				if (hashPgCustFld.containsKey("ifothersponsor")) {

				int fldNumOthSponsor = Integer.parseInt((String)hashPgCustFld.get("ifothersponsor"));
				String othSponsorMand = ((String)cdoPgField.getPcfMandatory().get(fldNumOthSponsor));
				String othSponsorLable = ((String)cdoPgField.getPcfLabel().get(fldNumOthSponsor));
				String othSponsorAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumOthSponsor));

				disableStr ="";
				readOnlyStr ="";

				if(othSponsorAtt == null) othSponsorAtt ="";
  				if(othSponsorMand == null) othSponsorMand ="";

				if(!othSponsorAtt.equals("0")) {

				if(othSponsorLable !=null){
				%>
				<td width="20%">
				<%=othSponsorLable%>
				<%} else {%> <td width="20%">
				 <%=LC.L_If_Other%><%--If Other*****--%>
				<% }


			   if (othSponsorMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomothsponsor">* </FONT>
		 	   <% }
			   %>

			   <%if(othSponsorAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (othSponsorAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

			   <td>
				<Input type=text id="ifothersponsor" name="studySponsor"  <%=disableStr%> <%=readOnlyStr%>  value="<%=studySponsorId%>"  >
			   </td>

			  <% } else { %>

			  <Input type=hidden id="ifothersponsor" name="studySponsor"   value="<%=studySponsorId%>"  >

 			  <% } }  else {%>

				 <td width="20%"> <%=LC.L_If_Other%><%--If Other*****--%> </td>
    				<td>
						<Input type=text id="ifothersponsor" name="studySponsor" value="<%=studySponsorId%>" >
				 </td>

			 <%}%>
			 </tr>




				<tr>
				<%
				if (hashPgCustFld.containsKey("sponsorid")) {

				int fldNumSponsorId = Integer.parseInt((String)hashPgCustFld.get("sponsorid"));
				String sponsorIdMand = ((String)cdoPgField.getPcfMandatory().get(fldNumSponsorId));
				String sponsorIdLable = ((String)cdoPgField.getPcfLabel().get(fldNumSponsorId));
				String sponsorIdAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSponsorId));

				disableStr ="";
				readOnlyStr ="";

				if(sponsorIdAtt == null) sponsorIdAtt ="";
  				if(sponsorIdMand == null) sponsorIdMand ="";

				if(!sponsorIdAtt.equals("0")) {

				if(sponsorIdLable !=null){
				%>
				<td width="20%">
				<%=sponsorIdLable%>
				<%} else {%> <td width="20%">
				 <%=LC.L_Sponsor_Id%><%--Sponsor ID*****--%>
				<% }


			   if (sponsorIdMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomsponsorid">* </FONT>
		 	   <% }
			   %>

			   <%if(sponsorIdAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (sponsorIdAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

			   <td>  <input type="text" name="studySponsorIdInfo"  maxlength="50"  <%=disableStr%> <%=readOnlyStr%> value="<%=studySponsorIdInfo%>">  </td>

			  <% } else { %>

			   <input type="hidden" name="studySponsorIdInfo"  maxlength="50"  value="<%=studySponsorIdInfo%>">

			  <% } }  else {%>

				 <td width="20%"> <%=LC.L_Sponsor_Id%><%--Sponsor ID*****--%></td>
						<td>   <!--Modified by Manimaran based on Code review -->
							<input type="text" name="studySponsorIdInfo"  maxlength="50" value="<%=studySponsorIdInfo%>">
						</td>

			 <%}%>
			 </tr>



			<tr>
				<%
				if (hashPgCustFld.containsKey("contact")) {

				int fldNumContact = Integer.parseInt((String)hashPgCustFld.get("contact"));
				String contactMand = ((String)cdoPgField.getPcfMandatory().get(fldNumContact));
				String contactLable = ((String)cdoPgField.getPcfLabel().get(fldNumContact));
				String contactAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumContact));

				disableStr ="";
				readOnlyStr ="";

				if(contactAtt == null) contactAtt ="";
  				if(contactMand == null) contactMand ="";

				if(!contactAtt.equals("0")) {

				if(contactLable !=null){
				%>
				<td width="20%">
				<%=contactLable%>
				<%} else {%> <td width="20%">
				<%=LC.L_Contact%><%--Contact*****--%>
				<% }


			   if (contactMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomcontact">* </FONT>
		 	   <% }
			   %>

			   <%if(contactAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (contactAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

			  <td> 	<Input type=text name="studyContact" value="<%=studyContactId%>" <%=disableStr%> <%=readOnlyStr%> >   </td>

			  <% } else { %>

			  <Input type=hidden name="studyContact" value="<%=studyContactId%>" >


			 <% } }  else {%>

				 <td width="20%"> <%=LC.L_Contact%><%--Contact*****--%> </td>
				  <td>
							<Input type=text name="studyContact" value="<%=studyContactId%>" >
				  </td>

			 <%}%>
			 </tr>



			<tr>
				<%
				if (hashPgCustFld.containsKey("othinfo")) {

				int fldNumOthInfo = Integer.parseInt((String)hashPgCustFld.get("othinfo"));
				String othInfoMand = ((String)cdoPgField.getPcfMandatory().get(fldNumOthInfo));
				String othInfoLable = ((String)cdoPgField.getPcfLabel().get(fldNumOthInfo));
				String othInfoAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumOthInfo));

				disableStr ="";
				readOnlyStr ="";

				if(othInfoAtt == null) othInfoAtt ="";
  				if(othInfoMand == null) othInfoMand ="";

				if(!othInfoAtt.equals("0")) {

				if(othInfoLable !=null){
				%>
				<td width="20%">
				<%=othInfoLable%>
				<%} else {%> <td width="20%">
				<%=LC.L_Other_Information%><%--Other Information*****--%>
				<% }


			   if (othInfoMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomothinfo">* </FONT>
		 	   <% }
			   %>

			   <%if(othInfoAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (othInfoAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

			  <td> 	<TextArea name="studyOtherInfo" rows=3 cols=50 MAXLENGTH = 2000 <%=disableStr%> <%=readOnlyStr%>  ><%=studyOtherInfo%></TextArea>	     </td>


			  <% } else { %>
			  <TextArea name="studyOtherInfo" rows=3 cols=50 Style = "display:none; visibility:hidden" MAXLENGTH = 2000  ><%=studyOtherInfo%></TextArea>

			  <% } }  else {%>

				<td width="20%"> <%=LC.L_Other_Information%><%--Other Information*****--%> </td>
			        <td>
							<TextArea name="studyOtherInfo"  rows=3 cols=50 MAXLENGTH = 2000 ><%=studyOtherInfo%></TextArea>
			    </td>

			 <%}%>
			 </tr>
			 <tr>					
					<td colspan=2 align="left">
					<br/>
					<!-- Modified for Bug#7962 : Raviesh -->	
					<!-- Modified for Bug#7969 : Akshi -->					   
								<jsp:include page="ctrpDraftSectNihGrant.jsp" flush="true">
									<jsp:param name="pageMode" value="E"/>
			  						<jsp:param name="studyId" value="<%=studyId%>"/> 				
								</jsp:include>	
					</td>			   			
					
			</tr>

					<tr style="<%=((StringUtil.isEmpty(publishRadioAtt))? emptyStr : hideStr)%>">
				        <td colspan=2  rowspan=3 align="center"> <%=MC.M_DoYouWant_InfoToPublic%><%--Do you want information in this section to be available to the public?*****--%>
					&nbsp;&nbsp;
					        <% if (stdSecRights[secCount] == '1') {%>
						        <input type="Radio" name="studyPubFlag4" value='1' Checked><%=LC.L_Yes%><%--Yes*****--%>
						        <input type="Radio" name="studyPubFlag4" value='0'><%=LC.L_No%><%--No*****--%>
					        <%} else if (stdSecRights[secCount] == '0') {%>
						        <input type="Radio" name="studyPubFlag4" value='1'><%=LC.L_Yes%><%--Yes*****--%>
						        <input type="Radio" name="studyPubFlag4" value='0' Checked><%=LC.L_No%><%--No*****--%>
					       <% } else {%>
						        <input type="Radio" name="studyPubFlag4" value='1'><%=LC.L_Yes%><%--Yes*****--%>
					    	    <input type="Radio" name="studyPubFlag4" value='0'><%=LC.L_No%><%--No*****--%>
					        <%} secCount++; %>
   					        <input type=hidden name=studyPubFlag4 value=''>
							&nbsp; <A href="#"><img src="../images/jpg/help.jpg" border="0" onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS(publicVsNonpublicStr)%>',CAPTION,'<%=LC.L_PublicNonPublicInfo%>',RIGHT, ABOVE,OFFSETY,70,WRAP);" onmouseout="return nd();" alt=""></img></A>
						</td>
					</tr>
		    	</table>

						<!--
						<P class = "sectionHeadings"> Participating Centers </P> -->
	            	<!-- <tr>
	              		<td width="300"> Participating Centers </td>
	              		<td>
	                		<TextArea name="studyPartCenters" rows=3 cols=50 MAXLENGTH = 1000 ><%=studyPartCenters%></TextArea>
	              		</td>
	            	</tr>
					<tr>
	              		<td width="500" colspan=2> Do you want information in this section to be available to the public?
						</td>
					</tr>
			        <tr>
				        <td colspan=2>  -->
	              			<% //if (stdSecRights[secCount] == '1') {//Public %>
	              				<!-- <input type="Radio" name="studyPubFlag5" value='1' Checked>Yes
				            <%//} else if (stdSecRights[secCount] == '0') { //Non Public %>
					            <!-- <input type="Radio" name="studyPubFlag5" value='1' >Yes
					            <input type="Radio" name="studyPubFlag5" value='0' Checked>No  -->
				            <% //} else {%>
							    <!-- <input type="Radio" name="studyPubFlag5" value='1'>Yes
					            <input type="Radio" name="studyPubFlag5" value='0'>No  -->
				            <%//} secCount++;%>
	            			<!-- &nbsp; <A href="pubvsnonpub.htm" target="Information" onClick="studyFunctions.openwin()">What is Public vs Non Public information?</A>
						</td>
					</tr> -->

			  		<!--<tr><td height =20></td>
					</tr>-->



				<tr>
				<%
				if (hashPgCustFld.containsKey("keywords")) {

				int fldNumKeywords = Integer.parseInt((String)hashPgCustFld.get("keywords"));
				String keywordsMand = ((String)cdoPgField.getPcfMandatory().get(fldNumKeywords));
				String keywordsLable = ((String)cdoPgField.getPcfLabel().get(fldNumKeywords));
				String keywordsAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumKeywords));

				disableStr ="";
				readOnlyStr ="";

				if(keywordsAtt == null) keywordsAtt ="";
  				if(keywordsMand == null) keywordsMand ="";

				if(!keywordsAtt.equals("0")) {
				%>
			<P class = "sectionHeadingsFrm"> <%=LC.L_Keywords%><%--Keywords*****--%></P>
				<table width="80%" cellspacing="2" cellpadding="2" border="0" class="basetbl midAlign">	
				<tr>
				<%

				if(keywordsLable !=null){
				%>
				<td width="25%">
				<%=keywordsLable%>
				<%} else {%> <td width="25%">
				<%=LC.L_Keywords%><%--Keywords*****--%>
				<% }


			   if (keywordsMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomkeywords">* </FONT>
		 	   <% }
			   %>

			   <%if(keywordsAtt.equals("1")) {
			       disableStr = "disabled class='readonly-input'"; }
		         else if (keywordsAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>

			  <td  class="textareaheight">  <TextArea name="studyKeywrds" rows=2 cols = 50  MAXLENGTH = 500 <%=disableStr%> <%=readOnlyStr%> ><%=studyKeywrds%></TextArea>    </td>

			 </tr>
			<tr>
	            <td></td>
       			<td ><p class="defComments"><%=MC.M_TipEtrSpfcSrch_SeprComma%><%--Tip: Enter specific words, which will help to search this trial. For Example leukemia,. You can add more than one keyword separated by a ',' (comma)*****--%>
				 </p></td>
	        </tr>
	        </table>
			  <% } else { %>
			  <TextArea name="studyKeywrds" rows=2 cols = 50  style="display:none; visibility:hidden"  MAXLENGTH = 500><%=studyKeywrds%></TextArea>


			 <% } 
				
			}  else {%>
		<P class = "sectionHeadingsFrm"> <%=LC.L_Keywords%><%--Keywords*****--%></P>
			<table width="80%" cellspacing="2" cellpadding="2" border="0" class="basetbl midAlign">
				<tr>	
				<td width="25%"> <%=LC.L_Keywords%><%--Keywords*****--%> </td>
	        	<td  class="textareaheight">
				     <TextArea class="textareaheight" name="studyKeywrds" rows=2 cols = 50  MAXLENGTH = 500><%=studyKeywrds%></TextArea>
			    </td>
			 </tr>
			<tr>
	            <td></td>
       			<td ><p class="defComments"><%=MC.M_TipEtrSpfcSrch_SeprComma%><%--Tip: Enter specific words, which will help to search this trial. For Example leukemia,. You can add more than one keyword separated by a ',' (comma)*****--%>
				 </p></td>
	        </tr>
	        </table>
			 <%}%>
		<%
		}else {
		//KM-#4099. Though the tab is hidden, we should not show blank page
		%>
			 <jsp:include page="accessdenied.jsp" flush="true"/>
			<%
		}
	}//end of if body for session
   	else
	{
	%>
	<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>

