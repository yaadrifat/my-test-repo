<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<%@ page import="com.velos.eres.service.util.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>

<script>
</script>


<body>

<title><%=LC.L_PatStd_Frm%><%--<%=LC.Pat_Patient%> <%=LC.Std_Study%> Form*****--%></title>


<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.user.UserJB" %>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<% String src;

src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");

String statDesc=request.getParameter("statDesc");
String statid=request.getParameter("statid");
String patProtId=request.getParameter("patProtId");
String patientCode=request.getParameter("patientCode");
String mode = request.getParameter("mode");
int patId=EJBUtil.stringToNum(request.getParameter("pkey"));


%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<br>
<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"
%>

<%
 HttpSession tSession = request.getSession(true);


  if (sessionmaint.isValidSession(tSession))
  {
	int pageRight = 0;
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("PATFRMSACC"));
	String modRight = (String) tSession.getValue("modRight");
	int patProfileSeq = 0, formLibSeq = 0;

	String study = request.getParameter("studyId");
	if(study == null || study.equals("null"))
	{
		study = (String) tSession.getValue("studyId");
	}
	else
	{
		tSession.setAttribute("studyId",study);
	}

	int frmId=0;
	String frmName = "";
	String desc = "";
	String lastEntryDate = "";
	String numEntries = "";
	String entryChar="";
	String protocolId = "";


 	// To check for the account level rights
	 acmod.getControlValues("module");
	 ArrayList acmodfeature =  acmod.getCValue();
	 ArrayList acmodftrSeq = acmod.getCSeq();
	 char formLibAppRight = '0';
	 char patProfileAppRight = '0';
	 patProfileSeq = acmodfeature.indexOf("MODPATPROF");
	 formLibSeq = acmodfeature.indexOf("MODFORMS");
	 formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
	 patProfileSeq = ((Integer) acmodftrSeq.get(patProfileSeq)).intValue();
	 formLibAppRight = modRight.charAt(formLibSeq - 1);
	 patProfileAppRight = modRight.charAt(patProfileSeq - 1);

	if (((String.valueOf(formLibAppRight).compareTo("1") == 0) || (String.valueOf(patProfileAppRight).compareTo("1") == 0)) &&(pageRight >=4))
	{
		 ArrayList arrFrmNames = null;
		 ArrayList arrDesc = null;
		 ArrayList arrLastEntryDate = null;
		 ArrayList arrNumEntries = null;
		 ArrayList arrFrmIds = null;
		  //by salil
		 ArrayList arrEntryChar=null;
		 String accId=(String)tSession.getAttribute("accountId");
		 String studyId= request.getParameter("studyId");
		 int ipatProtId = EJBUtil.stringToNum(patProtId);
		 int istudyId = EJBUtil.stringToNum(studyId);
		 int iaccId=EJBUtil.stringToNum(accId);

	  	 String userId = (String) tSession.getValue("userId");
	     UserJB userB = (UserJB) tSession.getValue("currentUser");
   	     String siteId = userB.getUserSiteId();

		 LinkedFormsDao lnkFrmDao = new LinkedFormsDao();
		 lnkFrmDao = lnkformB.getPatientStudyForms(iaccId,ipatProtId,istudyId, EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId));
		 arrFrmIds = lnkFrmDao.getFormId();
		 arrFrmNames = lnkFrmDao.getFormName();
		 arrDesc = lnkFrmDao.getFormDescription();
		 arrEntryChar = lnkFrmDao.getFormEntryChar();
		 arrLastEntryDate = lnkFrmDao.getFormLastEntryDate();
		 arrNumEntries = lnkFrmDao.getFormSavedEntriesNum();
		 int len = arrFrmNames.size();
		 String enrollId =(String) tSession.getValue("enrollId");

		patEnrollB.setPatProtId(EJBUtil.stringToNum(enrollId));
		patEnrollB.getPatProtDetails();
		protocolId =patEnrollB.getPatProtProtocolId();

		String protName = "";
		if(protocolId != null) {
		eventAssocB.setEvent_id(EJBUtil.stringToNum(protocolId));
		eventAssocB.getEventAssocDetails();
		protName = 	eventAssocB.getName();
		}

		String studyTitle = "";
		String studyNumber = "";
		studyB.setId(EJBUtil.stringToNum(studyId));

		studyB.getStudyDetails();
		studyTitle = studyB.getStudyTitle();
		studyNumber = studyB.getStudyNumber();


 %>

<DIV  class="browserDefault" id="div1">
<P class="sectionHeadings"> <%=LC.L_MngPat_Forms %><%-- Manage <%=LC.Pat_Patients%> >> Forms*****--%> </P>
<jsp:include page="patienttabs.jsp" flush="true">
<jsp:param name="pkey" value="<%=patId%>"/>
<jsp:param name="statDesc" value="<%=statDesc%>"/>
<jsp:param name="statid" value="<%=statid%>"/>
<jsp:param name="patProtId" value="<%=patProtId%>"/>
<jsp:param name="patientCode" value="<%=patientCode%>"/>
<jsp:param name="fromPage" value=""/>
<jsp:param name="page" value="patientEnroll"/>
</jsp:include>
<br>
<%if (!(patProtId.equals(""))) {%>
<table width=100%>
<tr>
	<td class=tdDefault width = 20% ><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>:</td><td class=tdDefault><%=studyNumber%></td>
</tr>
<tr>
	<td class=tdDefault><%=LC.L_Study_Title%><%-- <%=LC.Std_Study%> Title*****--%>:</td><td class=tdDefault><%=studyTitle%></td>
</tr>
	<%if(protocolId != null) {%>
	<tr>
 	<td class=tdDefault ><%=LC.L_Protocol_Calendar%><%-- Protocol Calendar*****--%>:</td><td class=tdDefault><%=protName%></td>
 </tr>
	<%}%>
 </table>



 <Form name="formlib" action="formLibraryBrowser.jsp"  method="post">

 <table width="100%" cellspacing="1" cellpadding="0" >
					<tr>
				        <th width="20%"><%=LC.L_Frm_Name%><%--Form Name*****--%></th>
				        <th width="25%"><%=LC.L_Description%><%--Description*****--%> </th>
						<th width="15%"><%=LC.L_Last_EntryOn%><%--Last Entry On*****--%> </th>
						<th width="20%"><%=LC.L_Number_OfEntries%><%--Number of Entries*****--%></th>
					</tr>
	 <%
	 int counter=0;
	 int i=0;
	for(counter=0;counter<len;counter++)
		{
		frmId = ((Integer)arrFrmIds.get(counter)).intValue();
		entryChar=(String)arrEntryChar.get(counter);
		frmName=((arrFrmNames.get(counter)) == null)?"-":(arrFrmNames.get(counter)).toString();
		desc=((arrDesc.get(counter)) == null)?"-":(arrDesc.get(counter)).toString();
		lastEntryDate=((arrLastEntryDate.get(counter))== null)?"-":(arrLastEntryDate.get(counter)).toString();
		numEntries=((arrNumEntries.get(counter))== null)?"-":(arrNumEntries.get(counter)).toString();
		int lk = Integer.parseInt(numEntries);

			if ((i%2)==0)
				{i++;%>
				<tr class="browserEvenRow">
				<%}else
				 {i++;%>
				 <tr class="browserOddRow">
					<%}
		        if( lk==0){%>
				   <td width="10%"><A href="patstudyformdetails.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&formId=<%=frmId%>&mode=N&formDispLocation=SP&pkey=<%=patId%>&patProtId=<%=patProtId%>&studyId=<%=studyId%>&calledFrom=S&statDesc=<%=statDesc%>&statid=<%=statid%>&patientCode=<%=patientCode%>&entryChar=<%=entryChar%>" ><%=frmName%></A></td>
				<%}
				 else
				 {%>
					<td width="10%"><A href="formfilledstdpatbrowser.jsp?formId=<%=frmId%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&pkey=<%=patId%>&mode=<%=mode%>&patProtId=<%=patProtId%>&studyId=<%=studyId%>&calledFrom=S&statDesc=<%=statDesc%>&statid=<%=statid%>&patientCode=<%=patientCode%>&entryChar=<%=entryChar%>"><%=frmName%></A></td>
				<%}%>

				<!--<td width="10%"><A href="formfilledstdpatbrowser.jsp?formId=<%=frmId%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&pkey=<%=patId%>&mode=<%=mode%>&patProtId=<%=patProtId%>&studyId=<%=studyId%>&calledFrom=S&statDesc=<%=statDesc%>&statid=<%=statid%>&patientCode=<%=patientCode%>"><%=frmName%></A></td>-->

				<td width="8%"><%=desc%></td>
				<%if(lastEntryDate!="-"){%>
				<td width="13%"><%=lastEntryDate%></td>
					<%}else{%>
				<td width="13%"><%=lastEntryDate%></td>
					<%}%>
				<td width="13%"><%=numEntries%></td>

		</tr>

		<%}%>


	</table>
	 <%

 } // if patProtId
  else {%>
  <P class="defComments"><%=MC.M_PatNotEnrl_EnrlPat%><%-- This <%=LC.Pat_Patient_Lower%> has not been enrolled yet. Please enroll <%=LC.Pat_Patient_Lower%> before answering Forms.*****--%></P>
  <%}

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right
}
else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div><div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/></div>

</body>

</html>



















