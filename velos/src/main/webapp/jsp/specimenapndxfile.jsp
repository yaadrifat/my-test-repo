<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<title><%=LC.L_File_Dets%><%--File Details*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>
</HEAD>


<% String src;
src= request.getParameter("srcmenu");
%>


<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>



<SCRIPT Language="javascript">

 function  validate(formobj){

	 mode = formobj.mode.value;
	 
	 if(mode == 'N') {
	  desc=formobj.desc;
	 }
	 if(mode == 'M'){
	 desc=formobj.specimenApndxDesc; 
	 }

	 if(desc.value.length>500){
    	 alert("<%=MC.M_ShortDesc_MaxCharsAllwd%>");/*alert(" 'Short Description' exceeded maximum number of characters allowed.");*****/
    	 desc.focus();
	     return false;
     } 
     if (!(checkquote(desc.value))) return false 
     
	 if(mode == 'N') {

	   if (!(validate_col('DESC',formobj.desc))) return false;
	   if (!(validate_col('FILE',formobj.name))) return false;


	 }
	  if(mode == 'M') {
	      if (!(validate_col('DESC',formobj.specimenApndxDesc))) return false;


	 }

     if (!(validate_col('e-Sign',formobj.eSign))) return false

	 <%-- if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
		} --%>
 }


function openwin() {

      window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=600,height=450")

;}

</SCRIPT>

<BODY>
<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="specimenApndxB" scope="page" class="com.velos.eres.web.specimenApndx.SpecimenApndxJB"/>

<%@ page  language = "java" import="com.velos.esch.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*"%>
<br>
<DIV class="formDefault" id="div1">

<P class="sectionHeadings"> <%=LC.L_Specimen_File%><%--Specimen >> File*****--%> </P>
  <%

HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))

{
	String accId = (String) tSession.getAttribute("accountId");
	String userId = (String) tSession.getAttribute("userId");
	String uName = (String) tSession.getAttribute("userName");
        String specId = request.getParameter("pkId");
	String specimenId = request.getParameter("specimenId");
	String ipAdd = (String) tSession.getAttribute("ipAdd");
	String mode = (String) request.getParameter("mode");
	String selectedTab = request.getParameter("selectedTab");
	String specimenTemplate=request.getParameter("specimenTemplate");
	String specimenMode =request.getParameter("specimenMode");
	String accMaxStorage = (String) tSession.getAttribute("accMaxStorage");
	String pkId = request.getParameter("pkId");
	CtrlDao ctrlDao = new CtrlDao();
	long freeSpace = ctrlDao.getFreeSpace(EJBUtil.stringToNum(accId));
	String specApndxDesc = "" ;
	String specApndxId = "";

	String FromPatPage=request.getParameter("FromPatPage");
	if(FromPatPage==null)
	{
		FromPatPage="";
	}
	String patientCode=request.getParameter("patientCode");
	if(patientCode==null){
		patientCode="";
	}
	
	
	//JM: 21Jul2009: issue no-4174
	String perId = "", stdId = "";
	perId = request.getParameter("pkey");
 	perId = (perId==null)?"":perId;

	stdId = request.getParameter("studyId");
	stdId = (stdId==null)?"":stdId;



%>

  <%
  if (mode.equals("N"))
  {

	com.aithent.file.uploadDownload.Configuration.readSettings("eres");
	com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml","specimen");
	String upld=com.aithent.file.uploadDownload.Configuration.UPLOADSERVLET;

   	%>

  <form name=upload id="uploadspecfile" action=<%=upld%> METHOD=POST ENCTYPE=multipart/form-data onSubmit=" if (validate(document.upload)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
  <%
  }
  else
  {	        specimenApndxB.setSpecimenApndxId(EJBUtil.stringToNum(specimenId))	;
		specimenApndxB.getSpecimenApndxDetails();
		specApndxDesc = specimenApndxB.getSpecApndxDesc();
		specApndxId = Integer.toString(specimenApndxB.getSpecimenApndxId());

  %>
  <form name="upload" id="uploadspecfile" action="specimenfileupdate.jsp" METHOD=POST onSubmit=" if (validate(document.upload)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
  <%
  }
   %>
    <table width="100%" >
      <tr>
        <td>
		<%
		if (mode.equals("N"))
		  {
	   %>
          <P class = "defComments"> <%=MC.M_AddDocu_SpmenAppdx%><%--Add documents/forms to your specimen's Appendix.*****--%>
		  </P>
		 <%
			}
		 else
		  {
		%>
	      <P class = "defComments"> <%=LC.L_Edit_ShortDesc%><%--Edit Short Description*****--%>
		  </P>

		<%
		  }
		%>

        </td>
      </tr>
    </table>
    <table width="100%" >
	  <%
	  if (mode.equals("N"))
	  {
	   %>
      <tr>
        <td width="35%"> <%=LC.L_File%><%--File*****--%> <FONT class="Mandatory">* </FONT> </td>
        <td width="65%">
          <input type =file name=name size=40 >
		  <input type=hidden name=db value='eres'>
		  <input type=hidden name=module value='specimen'>
		  <input type=hidden name=tableName value='ER_SPECIMEN_APPNDX'>
		  <input type=hidden name=columnName value='SA_FILEOBJ'>
		  <input type=hidden name=pkValue value='0'>
		  <input type=hidden name=pkColumnName value='PK_SPECIMEN_APPNDX'>
		  <!-- Ankit: Bug#6859  17-Aug-2011 -->
		  <input type=hidden name=nextPage value='../../velos/jsp/specimenapndx.jsp?&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&specId=<%=specId%>&pkId=<%=specId%>&pkey=<%=perId%>&studyId=<%=stdId%>&FromPatPage=<%=FromPatPage%>&patientCode=<%=StringUtil.encodeString(patientCode)%>'>

	    </td>
      </tr>
      <tr>
        <td width="35%"> </td>
        <td width="65%">
          <P class="defComments_txt"> <%=MC.M_Specify_FullFilePath%><%--Specify full path of the file.*****--%> </P>
        </td>
      </tr>
	  <%
	  }
	  %>
      <tr>
        <td width="35%"> <%=LC.L_Short_Desc%><%--Short Description*****--%> <FONT class="Mandatory" >* </FONT>
        </td>
        <td width="65%">
		<%
		if (mode.equals("N"))
		  {
	   %>
		 <!--   <input type=text name=desc MAXLENGTH=250 size=40> -->
		    <TextArea type=text name=desc row=3 cols=50 ></TextArea>
		<%
			}
		 else
		  {
		%>
		    <%-- <input type=text name=specimenApndxDesc MAXLENGTH=250 size=40 value='<%=specApndxDesc%>'> --%>
		    <TextArea type=text name=specimenApndxDesc row=3 cols=50 ><%=specApndxDesc%></TextArea>
		<%
		  }
		%>

        </td>
      </tr>
      <tr>
        <td width="35%"> </td>
        <td width="65%">
          <P class="defComments_txt"> <%=MC.M_ShortDescFile_500CharMax%><%--Give a short description of your file (500 char  max.)*****--%> </P>
        </td>
      </tr>
    </table>
     	<input type=hidden name=accId value=<%=accId%>>
  	<input type=hidden name=userId value=<%=userId%>>
	<input type=hidden name=ipAdd value=<%=ipAdd%>>
	<input type=hidden name=accountId value=<%=accId%>>
	<input type=hidden name=maxFileSize value=<%=freeSpace%>>
	<input type=hidden name=accMaxStorage value=<%=accMaxStorage%>>
	<input type="hidden" name="type" value='F'>
        <input type="hidden" name="mode" value=<%=mode%>>
	<input type="hidden" name="srcmenu" value=<%=src%>>
	<input type="hidden" name="specimenTemplate" value=<%=specimenTemplate%>>
	<input type="hidden" name="specimenMode" value=<%=specimenMode%>>
	<input type="hidden" name="specId" value=<%=pkId%>>
  	<input type="hidden" name="selectedTab" value="<%=selectedTab%>">
	<input type="hidden" name="pkId" value="<%=pkId%>">
	<input type="hidden" name="specimenId" value="<%=specimenId%>">

	<input type="hidden" name="pkey" value="<%=perId%>">
	<input type="hidden" name="studyId" value="<%=stdId%>">
	<input type="hidden" name="FromPatPage" value="<%=FromPatPage%>">
	<input type="hidden" name="patientCode" value="<%=patientCode%>">

    <%
	  if (mode.equals("M"))
	  {

    %>
	   <input type="hidden" name="specId" value=<%=specimenId%>>
 	<%
	   }
	%>


	<BR>
    <jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="uploadspecfile"/>
			<jsp:param name="showDiscard" value="N"/>
    </jsp:include>
	 <table width="100%" >
      <tr>
        <td > <BR>
          <P class = "defComments"> <FONT class="Mandatory">* </FONT> <%=LC.L_Indicates_MandatoryFlds%><%--Indicates
            mandatory fields*****--%> </P>
        </td>
      </tr>
    </table>
  </form>
  <%

}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
 <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<DIV class="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>
</body>

</html>
