<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ page import="com.velos.eres.gems.business.*"%>
<%@page import="com.velos.eres.widget.service.util.FlxPageCache"%>
<%@page import="org.apache.axis.types.Entities"%>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC, com.velos.eres.service.util.WorkflowUtil" %>
<%@ page import="com.velos.eres.service.util.StringUtil" %>
<%@page import="com.velos.eres.web.study.StudyJB"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.service.util.CFG"%>
<%
HttpSession tSession = request.getSession(false);
if (!sessionmaint.isValidSession(tSession))
	return;

if (!"Y".equals(CFG.Workflows_Enabled)) {
	return;
}

String wfPageId = (String) request.getParameter("wfPageId");
String flexMode = CFG.FLEX_MODE;
System.out.println("flexMode:::"+flexMode);

String from = "";
if(request.getParameter("fromTab")!=null && !request.getParameter("fromTab").equals("")){
	from=request.getParameter("fromTab");
}
//System.out.println("pid::::"+wfPageId+"::StudyID:::"+request.getParameter("studyId"));
int entityId = 0;
String workflowTypes = null, workflowFormName = null;

if (!StringUtil.isEmpty(wfPageId)) {
	//entityId = WorkflowUtil.getEntityId(wfPageId, tSession);
	workflowTypes = WorkflowUtil.getWorkflowTypes(wfPageId);
	workflowFormName = WorkflowUtil.getWorkflowFormName(wfPageId);
	workflowFormName = (StringUtil.isEmpty(workflowFormName)? "" : workflowFormName);
}
if(request.getParameter("studyId")!=null && !request.getParameter("studyId").equals("0") && !request.getParameter("studyId").equals("0")){
entityId = StringUtil.stringToNum(request.getParameter("studyId"));
}else{
if (!"LIND".equals(CFG.EIRB_MODE)){	
	entityId = WorkflowUtil.getEntityId(wfPageId, tSession);
}
else{
	entityId = StringUtil.stringToNum(request.getParameter("studyId"));
}
}
/* if("0".equals(studyId)) {
	entityId=0;
} */

if (entityId < 1 || StringUtil.isEmpty(workflowTypes)){
	return;
}
//System.out.println("WF Enabled::::"+CFG.Workflows_Enabled+"::::entityId::::"+entityId+":::workflowTypes:::"+workflowTypes+"::::workflowFormName:::"+workflowFormName);
%>
<%if ("LIND".equals(CFG.EIRB_MODE) || "Y".equals(flexMode)){ %>
  <%if ("Y".equals(CFG.Workflows_Enabled)) {%>
	<jsp:include page="workflows.jsp" flush="true">
		<jsp:param name="entityId" value="<%=entityId%>" />
		<jsp:param name="workflowTypes" value="<%=workflowTypes%>" />
		<jsp:param name="params" value="<%=entityId%>" />
		<jsp:param name="wfPageId" value="<%=wfPageId%>" />
		<jsp:param name="from" value="<%=from %>" />
	</jsp:include>
<%}%>
 <% } 
 
 if (!"LIND".equals(CFG.EIRB_MODE)){ %>
 <script>

$j(document).ready(function() {
//alert("Inside ready");
var entityId = $j("#studyEntId").val();
var workflowTypes = $j("#wfTypes").val();
var params = $j("#studyEntId").val();
var wfPageId = $j("#wfPgId").val();

	var url = "workflows.jsp?entityId="+entityId+"&workflowTypes="+workflowTypes+"&params="+params+"&wfPageId="+wfPageId;
	$j("#workflowContainer").load(url, function(){ });
	
	//alert("Inside ready"+url);	
	/* $j.ajax({
        type: "POST",
        url: "workflows.jsp?entityId="+entityId+"&workflowTypes="+workflowTypes+"&params="+params+"&wfPageId="+wfPageId,
        async:false,
        success: function (result, status, error){
        	
        	$j("#modelPopup1").html(result);
        	
        	       	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	}); */
	
	
	
});
</script>
<%} %>
<form id="tempform">
<input type="hidden" id="studyEntId" value="<%=entityId %>"/>
<input type="hidden" id="wfTypes" value="<%=workflowTypes %>"/>
<input type="hidden" id="wfPgId" value="<%=wfPageId %>"/>
</form>