<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page language = "java" import = "com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,java.net.URLEncoder,com.velos.eres.service.util.LC"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings,java.util.*"%>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<%
  String selclass;

  String studyId="";
  String eventId="";
  String protocolId="";
  String study="";
  String eventName="";
  String eventNameURL="";
  String calledFrom = (request.getParameter("calledFrom")==null)?"":request.getParameter("calledFrom");
  String studyNo="";

  String tab= request.getParameter("selectedTab");

  HttpSession tSession = request.getSession(true);
  String acc = (String) tSession.getValue("accountId");

  ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
  ArrayList tabList=null;
  if(!(request.getParameter("calledFrom").equals("NetWork")))
  	 tabList = objCache.getAccountObjects(EJBUtil.stringToNum(acc), "event_tab");

  if (sessionmaint.isValidSession(tSession))

  {
  	String eventmode = request.getParameter("eventmode");
	String duration = request.getParameter("duration");
	protocolId = request.getParameter("protocolId");
	String networkFlag = (request.getParameter("networkFlag")==null)?"":request.getParameter("networkFlag");
	String networkId = (request.getParameter("networkId")==null)?"":request.getParameter("networkId");
	String siteId = (request.getParameter("siteId")==null)?"":request.getParameter("siteId");
	String userPk = (request.getParameter("userPk")==null)?"":request.getParameter("userPk");
	String mainNetworkId = (request.getParameter("mainNetworkId")==null)?"":request.getParameter("mainNetworkId");
	String fromPage =request.getParameter("fromPage");
	String mode = request.getParameter("mode");
	String calStatus= request.getParameter("calStatus");
	eventId=request.getParameter("eventId");
	String displayDur=request.getParameter("displayDur");
	String displayType=request.getParameter("displayType");
	String src = request.getParameter("src");

	String calAssoc = request.getParameter("calassoc");
	calAssoc=(calAssoc==null)?"":calAssoc;

	if(!calledFrom.equals("NetWork")){
		
   if (calledFrom.equals("P")||calledFrom.equals("L") ||(calledFrom.equals("S") && fromPage.equals("selectEvent"))){
       	  eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
       	  eventdefB.getEventdefDetails();
          eventName = eventdefB.getName();
          eventName = (eventName==null)?"":eventName;
//JM: 12Nov2009: #4399
		  if (!eventName.equals("")){
		    if (eventName.length()>1000){//for the old existing event names > 1000 or larger
 	      		eventName=eventName.substring(0,1000);
          	}
          }

    }else{
       	  eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
          eventassocB.getEventAssocDetails();
       	  eventName = eventassocB.getName();
       	  eventName = (eventName==null)?"":eventName;
//JM: 12Nov2009: #4399
		   if (!eventName.equals("")){
		    if (eventName.length()>1000){//for the old existing event names > 1000 or larger
 	      		eventName=eventName.substring(0,1000);
          	}
           }

     }

     //encode Event Name for any special chracter in event name
     eventName=(eventName==null)?"":eventName;

     eventNameURL = URLEncoder.encode(eventName,"UTF-8");

  	if (mode == null){
	 	mode = "N";
	}else if (mode.equals("M")){
  		 eventId= request.getParameter("eventId");
    }



	out.print("<Form method=\"POST\"><Input type=\"hidden\" name=\"mode\" value=\"" +mode+"\"> </Form>");
	String uName = (String) tSession.getValue("userName");
  }
%>

<table cellspacing="0" cellpadding="0" border="0">
<tr>

	<%
	if((calledFrom.equals("NetWork"))){
		
		 int pageRight=EJBUtil.stringToNum(request.getParameter("pageRight")==null?"":request.getParameter("pageRight"));
		 
		 tabList = objCache.getAccountObjects(EJBUtil.stringToNum(acc), "network_document_tab");
		 
		 for (int iX=0; iX<tabList.size(); iX++) {
				ObjectSettings settings = (ObjectSettings)tabList.get(iX);
				
				if ("0".equals(settings.getObjVisible())) {
					continue;
				}
				boolean showThisTab = false;
				
				 if(networkFlag.equalsIgnoreCase("Org_level_doc") || networkFlag.equalsIgnoreCase("Org_doc")){
				if ("1".equals(settings.getObjSubType())) {
					 showThisTab = true;
				}
				else if ("2".equals(settings.getObjSubType())) {
					showThisTab = true;
				}
				 }else{
					 
					 if ("3".equals(settings.getObjSubType())) {
						 showThisTab = true;
					}
					else if ("4".equals(settings.getObjSubType())) {
						showThisTab = true;
					} 
				 }
		 
				if (!showThisTab) { continue; }
				
				if (tab == null) {
					selclass = "unselectedTab";
				} else if (tab.equals(settings.getObjSubType())) {
					selclass = "selectedTab";
				} else {
					selclass = "unselectedTab";
				}
	%>
	<td  valign="TOP">
			<table class="<%=selclass%>"  cellspacing="0" cellpadding="0"  border="0">
			<tr>
				<td><%
				if(networkFlag.equalsIgnoreCase("Org_level_doc") || networkFlag.equalsIgnoreCase("Org_doc")){ %>
				<%if ("1".equals(settings.getObjSubType())) {%>
					<a href="eventappendix.jsp?siteId=<%=siteId%>&networkId=<%=networkId%>&mainNetworkId=<%=mainNetworkId%>&calledFrom=<%=calledFrom%>&fromPage=<%=fromPage%>&userPk=<%=userPk%>&pageRight=<%=pageRight%>&networkFlag=Org_doc&selectedTab=1"><%=settings.getObjDispTxt()%>
					</a>
				<%}else if("2".equals(settings.getObjSubType())){%>
					<a href="eventappendix.jsp?networkId=<%=networkId%>&siteId=<%=siteId%>&mainNetworkId=<%=mainNetworkId%>&calledFrom=<%=calledFrom%>&fromPage=<%=fromPage%>&userPk=<%=userPk%>&pageRight=<%=pageRight%>&networkFlag=Org_level_doc&selectedTab=2"><%=settings.getObjDispTxt()%>
					</a>
				<%}}else{%>
				<%if ("3".equals(settings.getObjSubType())) {%>
					<a href="eventappendix.jsp?siteId=<%=siteId%>&networkId=<%=networkId%>&mainNetworkId=<%=mainNetworkId%>&calledFrom=<%=calledFrom%>&fromPage=<%=fromPage%>&userPk=<%=userPk%>&pageRight=<%=pageRight%>&networkFlag=Network_User_doc&selectedTab=3"><%=settings.getObjDispTxt()%>
					</a>
				<%}else if("4".equals(settings.getObjSubType())){%>
					<a href="eventappendix.jsp?networkId=<%=networkId%>&siteId=<%=siteId%>&mainNetworkId=<%=mainNetworkId%>&calledFrom=<%=calledFrom%>&fromPage=<%=fromPage%>&userPk=<%=userPk%>&pageRight=<%=pageRight%>&networkFlag=User_doc&selectedTab=4"><%=settings.getObjDispTxt()%>
					</a>
				<%} }%>
				</td>
			</tr>
	   	</table>
	</td>
	<% 
		 }}
	
	else{
	for (int iX=0; iX<tabList.size(); iX++) {
	ObjectSettings settings = (ObjectSettings)tabList.get(iX);

	if ("0".equals(settings.getObjVisible())) {
		continue;
	}

	boolean showThisTab = false;
	if ("1".equals(settings.getObjSubType())) {
		 showThisTab = true;
	}
	else if ("2".equals(settings.getObjSubType())) {
		showThisTab = true;

	}
	else if ("3".equals(settings.getObjSubType())) {
		showThisTab = true;

		if (calledFrom.equals("P") || calledFrom.equals("S"))
		{
			// showThisTab = false; // do not show cost when calld from calendar

		}
	}
	else if ("4".equals(settings.getObjSubType())) {
		showThisTab = true;
	}
	else if ("5".equals(settings.getObjSubType())) {
		showThisTab = true;
	}
	else if ("6".equals(settings.getObjSubType())) {
	   if (calAssoc.equals("S"))
	   {
	   }else{
		showThisTab = true;
	}}
    else {
		showThisTab = true;
	}

	if (!showThisTab) { continue; }


	if (tab == null) {
		selclass = "unselectedTab";
	} else if (tab.equals(settings.getObjSubType())) {
		selclass = "selectedTab";
	} else {
		selclass = "unselectedTab";
	}
	 %>


	<td  valign="TOP">
			<table class="<%=selclass%>"  cellspacing="0" cellpadding="0"  border="0">
			<tr> 	
				<td>

				<%if ("1".equals(settings.getObjSubType())) {%>
					<a href="eventdetails.jsp?eventmode=<%=eventmode%>&mode=<%=mode%>&duration=<%=duration%>&srcmenu=<%=src%>&selectedTab=1&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&displayType=<%=displayType%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>"><%=settings.getObjDispTxt()%>
					</a>
				<% } else if ("2".equals(settings.getObjSubType())) {%>
					<a href="eventmessage.jsp?eventmode=<%=eventmode%>&mode=<%=mode%>&duration=<%=duration%>&srcmenu=<%=src%>&selectedTab=2&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>"><%=settings.getObjDispTxt()%>
					</a>

				<%} else if ("3".equals(settings.getObjSubType())) {%>
					<a href="eventcost.jsp?eventmode=<%=eventmode%>&mode=<%=mode%>&duration=<%=duration%>&srcmenu=<%=src%>&selectedTab=3&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>"><%=settings.getObjDispTxt()%> </a>
				<%} else if ("4".equals(settings.getObjSubType())) {%>
					<a href="eventappendix.jsp?eventmode=<%=eventmode%>&mode=<%=mode%>&duration=<%=duration%>&srcmenu=<%=src%>&selectedTab=4&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>"><%=settings.getObjDispTxt()%>
					</a>
				<%} else if ("5".equals(settings.getObjSubType())) {%>
					<a href="eventresource.jsp?eventmode=<%=eventmode%>&mode=<%=mode%>&duration=<%=duration%>&srcmenu=<%=src%>&selectedTab=5&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>"><%=settings.getObjDispTxt()%>
					</a>
				<%} else if ("6".equals(settings.getObjSubType())) {
						if (calAssoc.equals("S"))   { }else{
					%>
					<a href="crflibbrowser.jsp?eventmode=<%=eventmode%>&mode=<%=mode%>&duration=<%=duration%>&srcmenu=<%=src%>&selectedTab=6&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>"><%=settings.getObjDispTxt()%>
					</a>

				<%}}%>

				</td>
		   	</tr>
	   	</table>
	</td>
	<%}
	}%>

	</tr>
</table>
<table class="tabBorder" width="99%" height="5"><tr><td></td></tr></table>
<%

	//KM
	if(!calledFrom.equals("NetWork")){
if ( (!(tab.equals("1"))) && (!(eventName==null)) ) {
	String OverlibParameter="";
	OverlibParameter=",WRAP, BORDER,2, CENTER,BELOW,OFFSETY,10,STATUS,'Draggable with overflow scrollbar, caption and Close link',";

%>
<table>
<tr><td>
<!--Code for bug id 23989-->
<P class="sectionHeadings"> <%=LC.L_Event_Name%><%--Event Name*****--%> :<%if(eventName.length()>50){ %><%=StringUtil.decodeString(eventName).substring(0,50)%><span onmouseover="return overlib(htmlEncode('<%=StringUtil.decodeString(eventName).replaceAll("\n", "").replaceAll("\r", "")%>')<%=OverlibParameter %>CAPTION,'<%=LC.L_Event_Name%><%--<%=LC.Event_Name%> Name*****--%>');" onmouseout="return nd();">...</span><%}else{ %><%=eventName%><%} %></P>
</td></tr>
</table>
<%}
}%>
    <%}%>
