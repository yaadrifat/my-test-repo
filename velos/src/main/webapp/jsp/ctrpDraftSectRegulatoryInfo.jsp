<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC" %>

<script type="text/JavaScript">
//to get dynamic drop down based upon the selected country
function callAjaxGetOversightOrgDD(){
	
	var selectedIndex =document.getElementById("ctrpDraftJB.oversightCountry").selectedIndex;
	   selval = document.getElementById("ctrpDraftJB.oversightCountry").options[selectedIndex].value;

 	   new VELOS.ajaxObject("getCodeDropDown.jsp", {
   		urlData:"selectedVal="+selval + "&ddName=ctrpDraftJB.oversightOrganization&codeType=ctrpOverAuth&ddType=child",
		   reqType:"POST",
		   outputElement: "span_oversightOrganization" }

   ).startRequest();

}

	$j(document).ready(function(){
		
		regulatoryInformationSectAction();
		$j('#ctrpDraftNonIndustrialForm_ctrpDraftJB_draftXmlRequired1').change(function() {
			regulatoryInformationSectAction();		});
		$j('#ctrpDraftNonIndustrialForm_ctrpDraftJB_draftXmlRequired0').change(function() {
			regulatoryInformationSectAction();		});

		$j('#ctrpDraftJB\\.section801Indicator').change(function() {
			regulatoryInformationSectAction();		});

		$j('#ctrpDraftJB\\.fdaIntvenIndicator').change(function() {
			regulatoryInformationSectAction();		});
		

	});

	function regulatoryInformationSectAction() {
		if (!$j('#ctrpDraftNonIndustrialForm_ctrpDraftJB_draftXmlRequired1')) { return; }
		if($j('#ctrpDraftNonIndustrialForm_ctrpDraftJB_draftXmlRequired1').is(':checked')){
			$j(':input', '#ctrp_draft10').removeAttr('disabled');
			$j('#ctrp_draft10_section').show();

			var fdaIntvenIndicator = document.getElementById('ctrpDraftJB.fdaIntvenIndicator').value;
			if(fdaIntvenIndicator==1){
				($j('#section801_Indicator_rowId')).show();
				var section801Indicator = document.getElementById('ctrpDraftJB.section801Indicator').value;
				if(section801Indicator==1){
					$j('#delayPost_Indicator_rowId').show();
				}else{
					$j('#delayPost_Indicator_rowId').hide();
				}
			}else{	
				$j('#delayPost_Indicator_rowId').hide();
				$j('#section801_Indicator_rowId').hide();
			}
		}else{
			$j(':input', '#ctrp_draft10').attr('disabled', 'disabled');
			$j('#ctrp_draft10_section').hide();
		}
	}
</script>
<table>
	<tr>
		<td align="right" width="20%">
			<%=LC.CTRP_DraftTrialAuthorityCountry%>
			<span id="ctrpDraftJB.oversightCountry_error" class="errorMessage" style="display:none;"></span>
		</td>
		<td width="1%" valign="top">
			<FONT id="oversight_Country_Menu_asterisk" class="Mandatory">* </FONT>
		</td>
		<td valign="top">
			<s:property escape="false" value="ctrpDraftJB.oversightCountryMenu" />
		</td>
	</tr>
	<tr>
		<td align="right">
			<%=LC.CTRP_DraftTrialAuthorityOrgName%>
			<span id="ctrpDraftJB.oversightOrganization_error" class="errorMessage" style="display:none;"></span>
		</td>
		<td valign="top">
			<FONT id="oversight_org_Menu_asterisk" class="Mandatory">* </FONT>
		</td>
		<td valign="top">
			<span id="span_oversightOrganization">
				<s:property escape="false" value="ctrpDraftJB.oversightOrgMenu" />
			</span>
		</td>
	</tr>
	<tr>
		<td align="right">
			<%=LC.CTRP_DraftFdaRegIntvnIndicator%>
			<span id="ctrpDraftJB.fdaIntvenIndicator_error" class="errorMessage" style="display:none;"></span>
		</td>
		<td valign="top">
			<FONT id="fda_Intven_Indicator_asterisk" class="Mandatory">* </FONT>
		</td>
		<td valign="top">
			<s:select name="ctrpDraftJB.fdaIntvenIndicator" id="ctrpDraftJB.fdaIntvenIndicator" list="ctrpDraftJB.fdaIntvenIndicatorList" 
					listKey="radioCode" listValue="radioDisplay" value="ctrpDraftJB.fdaIntvenIndicator" headerKey="-1" headerValue=""/>
		</td>
	</tr>
	<tr id="section801_Indicator_rowId" style="display:none">
		<td align="right">
			<%=LC.CTRP_DraftSection801Indicator%>
			<span id="ctrpDraftJB.section801Indicator_error" class="errorMessage" style="display:none;"></span>
		</td>
		<td valign="top">
			<FONT id="section801_Indicator_asterisk" class="Mandatory">* </FONT>
		</td>
		<td valign="top">
			<s:select name="ctrpDraftJB.section801Indicator" id="ctrpDraftJB.section801Indicator" list="ctrpDraftJB.section801IndicatorList" 
					listKey="radioCode" listValue="radioDisplay" value="ctrpDraftJB.section801Indicator" headerKey="-1" headerValue=""/>
		</td>
	</tr>
	<tr id="delayPost_Indicator_rowId" style="display:none">
		<td align="right">
			<%=LC.CTRP_DraftDelayPostIndicator%>
			<span id="ctrpDraftJB.delayPostIndicator_error" class="errorMessage" style="display:none;"></span>
		</td>
		<td valign="top">
			<FONT id="delayPost_Indicator_asterisk" class="Mandatory">* </FONT>
		</td>
		<td valign="top">
			<s:select name="ctrpDraftJB.delayPostIndicator" id="ctrpDraftJB.delayPostIndicator" list="ctrpDraftJB.delayPostIndicatorList" 
					listKey="radioCode" listValue="radioDisplay" value="ctrpDraftJB.delayPostIndicator" headerKey="-1" headerValue=""/>
		</td>
	</tr>
	<tr>
		<td align="right"><%=LC.CTRP_DraftDMAppointIndicator%></td>
		<td valign="top">
			&nbsp;&nbsp;
		</td>
		<td valign="top">
			<s:select name="ctrpDraftJB.dmAppointIndicator" id="ctrpDraftJB.dmAppointIndicator" list="ctrpDraftJB.dmAppointIndicatorList" 
					listKey="radioCode" listValue="radioDisplay" value="ctrpDraftJB.dmAppointIndicator" headerKey="-1" headerValue=""/>
		</td>
	</tr>
</table>