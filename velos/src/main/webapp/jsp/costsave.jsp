<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>
</head>

<%  
String fromPage = request.getParameter("fromPage");
if ((fromPage.equals("selectEvent")) || (fromPage.equals("fetchProt")))
{ %>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<!-- YK 05Aug11: Removed for Bug #6728 already using Whichcss_skin.js in panel.jsp -->
	 
 <% } //end for if ((request.getParameter("fromPage")).equals("selectEvent"))
 %>

<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	<%-- if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   } --%>
}
</SCRIPT> 

<%@page import="com.aithent.audittrail.reports.AuditUtils"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventcostB" scope="request" class="com.velos.esch.web.eventcost.EventcostJB"/>
<%@ page language = "java" import = "com.velos.esch.service.util.EJBUtil,com.velos.eres.service.util.StringUtil"%>

<% String src;
	src= request.getParameter("srcmenu");
%>

<% if (fromPage.equals("selectEvent") || fromPage.equals("fetchProt")){
    %>
    <jsp:include page="include.jsp" flush="true"/>
    <%
    } else {
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   
<%  }  %>

<body>
<br>

<%  
if ((fromPage.equals("selectEvent")) || (fromPage.equals("fetchProt"))){%>
 <DIV class="popDefault" id="div1"> 
<%	} 
else { %>
 <DIV class="formDefault" id="div1">  
<%}%>	


<%
String duration = request.getParameter("duration");
String protocolId = request.getParameter("protocolId");
String calledFrom = request.getParameter("calledFrom");
String eventId = request.getParameter("eventId");
String costmode = request.getParameter("costmode");
String mode = request.getParameter("mode");
String calStatus = request.getParameter("calStatus");
String eventmode = request.getParameter("eventmode");
String displayDur=request.getParameter("displayDur");
String displayType=request.getParameter("displayType");	  
String eSign = request.getParameter("eSign");
 
String costId = "";
String propagateInVisitFlag = request.getParameter("propagateInVisitFlag"); //SV, 10/12/04, cal-enh-05
String propagateInEventFlag = request.getParameter("propagateInEventFlag");

String eventName = request.getParameter("eventName");
eventName = StringUtil.encodeString(eventName);

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession)) {
    
    String calAssoc = request.getParameter("calassoc");
    calAssoc = (calAssoc == null) ? "" : calAssoc;
    
    System.out.println("calAssoc" + calAssoc);
    
	String oldESign = (String) tSession.getValue("eSign");
	if(!oldESign.equals(eSign) && !costmode.equals("D")) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
   String ipAdd = (String) tSession.getValue("ipAdd");
   String usr = (String) tSession.getValue("userId");

   String cost = request.getParameter("cost");
   String costDesc = request.getParameter("costDesc");
   String cur = request.getParameter("cur");
	
   if(costmode.equals("D")) {
	costId = request.getParameter("costId");
		String delMode=request.getParameter("delMode");
		if (delMode==null) {
			delMode="final";
%>
	<FORM name="deleteFile" id="delcostfile" method="post" action="costsave.jsp" onSubmit="if (validate(document.deleteFile)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>	
	
		<jsp:include page="propagateEventUpdate.jsp" flush="true"> 
		
		<jsp:param name="fromPage" value="<%=fromPage%>"/>
		
		<jsp:param name="formName" value="deleteFile"/>
		
		<jsp:param name="eventName" value="<%=eventName%>"/>
		
		</jsp:include>   
		
	<P class="defComments"><%=MC.M_EtrEsign_CostDel%><%--Please enter e-Signature to proceed with Cost Delete.*****--%></P>	
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="delcostfile"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
  	 <input type="hidden" name="duration" value="<%=duration%>">
  	 <input type="hidden" name="protocolId" value="<%=protocolId%>">
  	 <input type="hidden" name="calledFrom" value="<%=calledFrom%>">
  	 <input type="hidden" name="eventId" value="<%=eventId%>">
  	 <input type="hidden" name="costmode" value="<%=costmode%>">
  	 <input type="hidden" name="mode" value="<%=mode%>">
  	 <input type="hidden" name="fromPage" value="<%=fromPage%>">
  	 <input type="hidden" name="calStatus" value="<%=calStatus%>">
   	 <input type="hidden" name="eventmode" value="<%=eventmode%>">
   	 <input type="hidden" name="displayDur" value="<%=displayDur%>">
   	 <input type="hidden" name="displayType" value="<%=displayType%>">
   	 <input type="hidden" name="costId" value="<%=costId%>">
   	 <input type="hidden" name="eventName" value="<%=eventName%>">
   	 <input type="hidden" name="calassoc" value="<%=calAssoc%>">
 
	</FORM>
<%
		} else {
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {
			
			
				if ((propagateInVisitFlag == null) || (propagateInVisitFlag.equals("")))

					propagateInVisitFlag = "N";

				if ((propagateInEventFlag == null) || (propagateInEventFlag.equals("")))
	
				propagateInEventFlag = "N";
			
				 
				 if ( (propagateInVisitFlag.equals("Y")) || (propagateInEventFlag.equals("Y"))) 
				 {
					eventcostB.setEventcostId(EJBUtil.stringToNum(costId));
					eventcostB.setEventId(eventId);
					System.out.println("costId delete" + costId);
					System.out.println("eventId " + eventId);
			
					eventcostB.propagateEventcost(EJBUtil.stringToNum(protocolId), propagateInVisitFlag, propagateInEventFlag, "D", calledFrom);  
				}
				else
				{
					eventcostB.setEventcostId(EJBUtil.stringToNum(costId));
					eventcostB.setEventId(eventId);
					System.out.println("costId delete" + costId);
					System.out.println("eventId " + eventId);
				 	//eventcostB.removeEventcost(EJBUtil.stringToNum(costId));
				 	// Modified for INF-18183 ::: Raviesh
					eventcostB.removeEventcost(EJBUtil.stringToNum(eventId), EJBUtil.stringToNum(costId), "D",AuditUtils.createArgs(session,"",LC.L_Evt_Lib));  
				}	
				
				
				
%>

<br><br><br><br><br>
<p class = "sectionHeadings" align = center><%=MC.M_Data_DelSucc%><%--Data deleted successfully.*****--%></p>
<%-- Commented for Bug# 16450-(Script error is displayed on deleting cost) 
<script>
window.opener.location.reload();
</script>
--%>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=eventcost.jsp?selectedTab=3&srcmenu=<%=src%>&duration=<%=duration%>&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>">
<%
			}
		}
   } else {
	if(costmode.equals("M")) {
	   costId = request.getParameter("costId");
	   eventcostB.setEventcostId(EJBUtil.stringToNum(costId));
	   eventcostB.getEventcostDetails();
   	}

   	eventcostB.setEventcostDescId(costDesc); 
	eventcostB.setEventcostValue(cost);
   	eventcostB.setEventId(eventId);
	eventcostB.setCurrencyId(cur);
	eventcostB.setIpAdd(ipAdd);   

   	if(costmode.equals("M")) {
	   eventcostB.setModifiedBy(usr);
		// as usual, update the record.
	   eventcostB.updateEventcost();  
	} else {
	   eventcostB.setCreator(usr); 
	   eventcostB.setEventcostDetails();  
   	}
	// Cal-enh - 05, now check if the user wishes to propagate these changes to other events.
		if ((propagateInVisitFlag == null) || (propagateInVisitFlag.equals("")))
			propagateInVisitFlag = "N";

		if ((propagateInEventFlag == null) || (propagateInEventFlag.equals("")))
			propagateInEventFlag = "N";
		
		if ( (propagateInVisitFlag.equals("Y")) || (propagateInEventFlag.equals("Y"))) {
			//SV, 10/28/04, changed "P" to calledFrom; "M" - modify/update, "P" - protocol/library calendar.			
			
			eventcostB.setEventcostId(EJBUtil.stringToNum(costId));
			eventcostB.setEventId(eventId);
			
			System.out.println("costId new" + costId);
			System.out.println("eventId new" + eventId);
			
			eventcostB.propagateEventcost(EJBUtil.stringToNum(protocolId), propagateInVisitFlag, propagateInEventFlag, "M", calledFrom);  
		}

%>

<br><br><br><br><br>
<p class = "sectionHeadings" align = center><%=MC.M_Data_SavedSucc%><%--Data saved successfully.*****--%></p>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=eventcost.jsp?selectedTab=3&srcmenu=<%=src%>&duration=<%=duration%>&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>">
<%

   }
   
%>

<!--<META HTTP-EQUIV=Refresh CONTENT="1; URL=addeventinfo.jsp?srcmenu=<%=src%>&duration=<%=duration%>&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>">-->
<%
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}

if ((fromPage.equals("selectEvent")) || (fromPage.equals("fetchProt")))
{ } else {%>
</div>
<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>
<%}%>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</BODY>
</HTML>


