<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_FldEdt_BoxSubmit%><%--Field -Edit Box Submit*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="formSecJB" scope="request"  class="com.velos.eres.web.formSec.FormSecJB"/>
<jsp:useBean id="formFieldJB" scope="request"  class="com.velos.eres.web.formField.FormFieldJB"/>
<jsp:useBean id="fldValidateJB" scope="request"  class="com.velos.eres.web.fldValidate.FldValidateJB"/>





<body><%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB, com.velos.eres.business.fieldLib.impl.FieldLibBean, com.velos.eres.business.fldValidate.impl.FldValidateBean"%>
<DIV class="popDefault" id="div1"> 

<%
	HttpSession tSession = request.getSession(true); 
	String codeStatus = request.getParameter("codeStatus");
	if (sessionmaint.isValidSession(tSession))
	{
		
%>
	
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   
		String eSign= request.getParameter("eSign");
    	String oldESign = (String) tSession.getValue("eSign");
		   if((codeStatus.equals("WIP")) || ((!codeStatus.equals("WIP") && !codeStatus.equals("")) && (oldESign.equals(eSign)))){
			
			int errorCode=0;
			String mode=request.getParameter("mode");
			String fieldLibId="";
			//String fldCategory="";
			String fldName="";
			String fldNameFormat="";
			String fldExpLabel = "";
			String fldUniqId="";
			String fldKeyword="";
			String fldInstructions="";
			String fldDesc="";
			String fldType="";
			String fldDataType="";
			String fldIsRO="";
		
			String fldLength="";
			String fldLinesNo="";
			String fldCharsNo="";
			String fldDefResp="";
			String accountId="";
			String fldLibFlag="F"; // received from session in future 
			String fldBrowserFlg="0" ;
			String fldIsVisible ="";
			
			// THE STYLE OBJECT WHICH TAKES VALUES OF ATTRIBUTES OF THE FIELD  
			// LIKE BOLD, SAMELINE, ALIGN , ITALICS
			Style aStyle = new Style();
			String formId="";
			String formFldId="";
			String formSecId="";
			String fldMandatory="";
			String fldSeq="";
			String fldAlign="";
			String samLinePrevFld="";
			String fldBold="";
			String fldItalics="";
		
			String creator="";
			String ipAdd="";
			String dateCheck ="",defaultDate="";
			String numformat ="";

			String Glt1 = "";
			String Glt2 = "";
			String val1 = "";
			String val2 = "";
			String andOr = "";
			String fldValidateId="";
						
			String hideLabel = "";
			String labelDisplayWidth= "";
		
		
			FieldLibBean fieldLsk = new FieldLibBean();
			creator=(String) tSession.getAttribute("userId");
			ipAdd=(String)tSession.getAttribute("ipAdd");
	     	accountId=(String)tSession.getAttribute("accountId");
			
			mode=request.getParameter("mode");
			String recordType=mode;  
			
	
			formId=request.getParameter("formId");
			fieldLibId=request.getParameter("fieldLibId");
			//fldCategory=request.getParameter("category");
			fldName=request.getParameter("name");
			
			fldNameFormat=StringUtil.decodeString(request.getParameter("nameta"));

			fldNameFormat=StringUtil.decodeString(fldNameFormat);
			
			fldNameFormat=(fldNameFormat==null)?"":fldNameFormat;
			
			fldExpLabel = request.getParameter("expLabel");
			
			fldUniqId= request.getParameter("uniqueId");
			
			fldKeyword=request.getParameter("keyword");
			fldInstructions=request.getParameter("instructions");
			fldDesc=request.getParameter("description");
			fldType="E"; //edit box type to be entered by us 
			
			fldLength=request.getParameter("lengthOf");
			fldCharsNo=request.getParameter("characters");
			fldLinesNo=request.getParameter("lines");
			formSecId=request.getParameter("section");
			fldSeq=request.getParameter("sequence");
		
			fldMandatory=request.getParameter("mandatory");	
			
			fldDataType = request.getParameter("editBoxType");
	
			
			String mandatoryChk = request.getParameter("overRideMandatory");
			if(mandatoryChk == null || mandatoryChk.equals(""))
				mandatoryChk = "0";
			String dateChk = request.getParameter("hdnOverRideDate");
			
				if(dateChk == null || dateChk.equals(""))
				dateChk = "0";
			String formatChk = request.getParameter("hdnOverRideFormat");
				if(formatChk == null || formatChk.equals(""))
				formatChk = "0";
			String rangeChk = request.getParameter("hdnOverRideRange");
				if(rangeChk == null || rangeChk.equals(""))
				rangeChk = "0";
				
					
			fldAlign=request.getParameter("align");
			samLinePrevFld=request.getParameter("sameLine");
			fldBold=request.getParameter("bold");
			fldItalics=request.getParameter("italics");
			dateCheck = request.getParameter("hdndatefld");
			
			defaultDate=request.getParameter("hdndefaultdate");
			defaultDate=(defaultDate==null)?"0":defaultDate;
			
		
			
			fldValidateId = request.getParameter("fldValidateId");
			Glt1 = StringUtil.htmlDecode(request.getParameter("greaterless1"));
			
			Glt2 = StringUtil.htmlDecode(request.getParameter("greaterless2"));
			val1 = request.getParameter("val1");
			val2 = request.getParameter("val2");
			andOr = request.getParameter("andor");
			numformat = request.getParameter("numformat");	
			fldIsVisible = request.getParameter("isvisible");
			
			hideLabel = request.getParameter("hideLabel");
			if(StringUtil.isEmpty(hideLabel)){
				hideLabel="0";
			}
			labelDisplayWidth = request.getParameter("labelDisplayWidth");
				
			if(numformat!=null)
			{
				numformat = numformat.trim();
			}
			else
		    {
				numformat="";
		    }
			if(Glt1==null)
				Glt1="";
			if(Glt2==null)
				Glt2="";
			if(val1==null)
				val1="";
			if(val2==null)
				val2="";
			if(andOr==null)
				andOr="";
			if(numformat==null)
				numformat="";
			if(dateCheck==null)
				dateCheck="";

			//  We SET THE STYLE WE CHECK ITS VALUES TO BEFORE SENDING TO THE DB
			aStyle.setAlign(fldAlign);
			if(fldMandatory==null){
			fldMandatory="0";
			}
			
			if (fldBold==null)
			aStyle.setBold("0");
			else if (fldBold.equals("1"))
			aStyle.setBold("1");

			if (fldItalics==null)  
			aStyle.setItalics("0");
			else if (fldItalics.equals("1"))
			aStyle.setItalics("1");
			
			
			if (samLinePrevFld==null)
			aStyle.setSameLine("0");
			else if (samLinePrevFld.equals("1"))
			aStyle.setSameLine("1");
	
			if(fldExpLabel==null){
				aStyle.setExpLabel("0");
				fldExpLabel="0";
			}
			else
		   {
				aStyle.setExpLabel("1");
				fldExpLabel="1";
		   }
		   
			if (fldDataType.equals("ED"))
			{
				//fldDefResp=request.getParameter("defResponse2");
				fldDefResp="";
				//date field should not be readonly
				fieldLsk.setFldIsRO("0") ;
			}
			
			/*if(fldIsVisible.equals("1"))
		   {
				//aStyle.setColor("lightgrey");
				fieldLsk.setFldIsRO("1") ;

		   }*/
		   
		   if(fldIsVisible.equals("2"))
		   {
				aStyle.setColor("lightgrey");
		   }
		   
		   if(fldIsVisible.equals("3")) //for readonly
		   {
		   		fieldLsk.setFldIsRO("1") ;
		   }else
		   	   {
		   	   		fieldLsk.setFldIsRO("0") ;
		   	   }
			
			fldDefResp=request.getParameter("defResponse1");
			fldDefResp=(fldDefResp==null)?"":fldDefResp;
			
			if (fldDataType.equals("ED"))
			{
			 if (defaultDate.equals("1"))
			   fldDefResp="[VELSYSTEMDATE]"; 
			} 
						
			if (!(fldDataType.equals("ED")) && (fldDefResp.equals("[VELSYSTEMDATE]")))
			{
				fldDefResp="";
			}
			
			
			
			fieldLsk.setFormId(formId );
			//fieldLsk.setCatLibId(fldCategory );
			fieldLsk.setFldName(fldName);
			fieldLsk.setFldNameFormat(fldNameFormat);
			fieldLsk.setFldUniqId(fldUniqId);
			fieldLsk.setFldKeyword(fldKeyword);
			fieldLsk.setFldInstructions(fldInstructions);
			fieldLsk.setFldDesc(fldDesc);
			fieldLsk.setFldType(fldType);
			fieldLsk.setFldDataType(fldDataType);
			fieldLsk.setFldLength(fldLength);
			fieldLsk.setFldLinesNo(fldLinesNo);
			fieldLsk.setFldCharsNo(fldCharsNo);
			fieldLsk.setFldDefResp(fldDefResp);
			fieldLsk.setIpAdd(ipAdd);
			fieldLsk.setRecordType(recordType);
			fieldLsk.setAccountId(accountId);
			fieldLsk.setLibFlag(fldLibFlag);
			fieldLsk.setTodayCheck(dateCheck);
			fieldLsk.setFldFormat(numformat);	
			fieldLsk.setFormFldBrowserFlg(fldBrowserFlg);
			fieldLsk.setExpLabel(fldExpLabel);
			fieldLsk.setFldIsVisible(fldIsVisible);
			
			
			

			fieldLsk.setFormSecId(formSecId );
			fieldLsk.setFormFldSeq( fldSeq );
			fieldLsk.setFormFldMandatory( fldMandatory );
			
			fieldLsk.setOverRideMandatory(mandatoryChk);
			fieldLsk.setOverRideFormat(formatChk);
			fieldLsk.setOverRideRange(rangeChk);
			fieldLsk.setOverRideDate(dateChk);
			
		
			//fieldLsk.setFormFldBrowserFlg( formFldBrowserFlg );
			
			fieldLsk.setAStyle(aStyle);
						
			FldValidateBean fldValsk = new FldValidateBean();
			fldValsk.setFldLibId(fieldLibId);
			fldValsk.setFldValidateOp1(Glt1);
			fldValsk.setFldValidateVal1(val1);
			fldValsk.setFldValidateLogOp1(andOr);
			fldValsk.setFldValidateOp2(Glt2);
			fldValsk.setFldValidateVal2(val2);
			fldValsk.setRecordType(recordType); 
			fldValsk.setIpAdd(ipAdd);

			fieldLsk.setFldValidateBean(fldValsk);
			
			
			fieldLsk.setFldHideLabel(hideLabel);
			fieldLsk.setFldDisplayWidth(labelDisplayWidth);

			if (mode.equals("N"))
			{
			fldValsk.setCreator(creator);
			}
			
			
			if (mode.equals("N"))
			{
				
				fieldLsk.setCreator(creator);
				errorCode=fieldLibJB.insertToFormField(fieldLsk);
				
			}

			else if (mode.equals("M"))
			{
			
		
				
				formFldId=request.getParameter("formFldId");
				formFieldJB.setFormFieldId(EJBUtil.stringToNum(formFldId));
				formFieldJB.getFormFieldDetails();
				fieldLibId=formFieldJB.getFieldId();
							
				fieldLibJB.setFieldLibId(EJBUtil.stringToNum(fieldLibId) );
				fieldLibJB.getFieldLibDetails();
				
				fieldLsk  = fieldLibJB.createFieldLibStateKeeper();

				fldValsk.setFldValidateId(EJBUtil.stringToNum(fldValidateId));
				//KM-#3634
				fldValsk.setModifiedBy(creator);
				fieldLsk.setFldValidateBean(fldValsk);
								

				fieldLsk.setFormId(formId );
				fieldLsk.setFormFldId(formFldId );
				//fieldLsk.setCatLibId(fldCategory );
				fieldLsk.setFldName(fldName);
				fieldLsk.setFldNameFormat(fldNameFormat);
				fieldLsk.setFldUniqId(fldUniqId);
				fieldLsk.setFldKeyword(fldKeyword);
				fieldLsk.setFldInstructions(fldInstructions);
				fieldLsk.setFldDesc(fldDesc);
				fieldLsk.setFldType(fldType);
				fieldLsk.setFldDataType(fldDataType);
				fieldLsk.setFldLength(fldLength);
				fieldLsk.setFldLinesNo(fldLinesNo);
				fieldLsk.setFldCharsNo(fldCharsNo);
				fieldLsk.setFldDefResp(fldDefResp);
				fieldLsk.setIpAdd(ipAdd);
				fieldLsk.setRecordType(recordType);
				fieldLsk.setAccountId(accountId);
				fieldLsk.setLibFlag(fldLibFlag);
				fieldLsk.setTodayCheck(dateCheck);
				fieldLsk.setModifiedBy(creator);
				fieldLsk.setFormSecId(formSecId );
				fieldLsk.setFormFldSeq( fldSeq );
				fieldLsk.setFldFormat(numformat);
				fieldLsk.setFldIsVisible(fldIsVisible);
				if(fldIsVisible.equals("2"))
				   {									
						
						aStyle.setColor("lightgrey");
						//fieldLsk.setFldIsRO("1") ;

				   }
				 else{
						aStyle.setColor("");
				}
			
				if(fldIsVisible.equals("3")) //for readonly
		   		{
		   			fieldLsk.setFldIsRO("1") ;
		   		}else
		   			{
		   				fieldLsk.setFldIsRO("0") ;
		   			}
		
				
				fieldLsk.setFormFldMandatory( fldMandatory );
				
				fieldLsk.setOverRideMandatory(mandatoryChk);
				fieldLsk.setOverRideFormat(formatChk);
				fieldLsk.setOverRideRange(rangeChk);
				fieldLsk.setOverRideDate(dateChk);
				
				fieldLsk.setExpLabel( fldExpLabel );
				
				fieldLsk.setAStyle(aStyle);
				fieldLsk.setFldHideLabel(hideLabel);
				fieldLsk.setFldDisplayWidth(labelDisplayWidth);

				
		    	errorCode=fieldLibJB.updateToFormField(fieldLsk);
				
		 }

		
		if ( errorCode == -2 || errorCode == -3 )
		{
%>
  	
<%             if (errorCode == -3 )
				{
%>
				
				<br><br><br><br><br>
				<p class = "successfulmsg" > <%=MC.M_FldIdExst_EtrDiff%><%--The Field ID already exists. Please enter a different Field ID*****--%></p>
				<button tabindex=2 onClick="history.go(-1);"><%=LC.L_Back%></button>
				
				
<%
				} else 
%>				
				<br><br><br><br><br><p class = "successfulmsg" align = center>		
	 			<p class = "successfulmsg" ><%=MC.M_Data_NotSvdSucc%><%--Data not saved successfully*****--%> </p>

<%
		
		} //end of if for Error Code

		else

		{
%>	
	
		
	<br><br><br><br><br>
		<p class = "successfulmsg" align = center><%=MC.M_Data_SavedSucc%><%--Data Saved Successfully*****--%> </p>
        <script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
		</script> 

<%
		}//end of else for Error Code Msg Display
	}//end of if old esign
	else{  
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%	  
			}//end of else of incorrect of esign
     }//end of if body for session 	
	
	else
	{

%>
		  <jsp:include page="timeout.html" flush="true"/>
<%
	
} //end of else of the Session 
 

%>

</body>

</html>
