<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.velos.eres.widget.business.common.UIFlxPageDao" %>
<%@ page import="com.velos.eres.business.common.StudyVerDao" %>
<jsp:include page="localization.jsp" flush="true"/> 
<HTML>
<HEAD>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<%
boolean isIrb = "irb_upload_tab".equals(request.getParameter("selectedTab")) ? true : false; 
if (isIrb) {
	if("LIND".equals(CFG.EIRB_MODE)){%>
	<title><%= MC.M_ResProtoApp_UploadDoc %></title>	
	<%}else{%>
<title><%=MC.M_ResCompApp_UploadDoc %><%-- Research Compliance >> New Application >> Upload Documents*****--%></title>
<%} %>
<% } else { %>
<TITLE><%=LC.L_Study_Appendix %><%-- <%=LC.Std_Study%> >> Appendix*****--%></TITLE>
<% } %>

 

</HEAD>

<% String src;

src= request.getParameter("srcmenu");
String from = "appendixver";
String isPopupWin = "0";
	if(request.getParameter("isPopupWin")!=null){
		isPopupWin = request.getParameter("isPopupWin");
	}

%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="isPopupWin" value="<%=isPopupWin%>"/>
<jsp:param name="src" value="<%=src%>"/>
</jsp:include> 

<SCRIPT language="JavaScript1.1">

var screenWidth = screen.width;
var screenHeight = screen.height;

	function openWin() {

      windowName = window.open("","Information","toolbar=yes,location=yes,scrollbars=yes,resizable=yes,menubar=yes,width=600,height=300")
	windowName.focus();

;}


function fdownload(formobj,pk,filename,dnldurl,statuspk,studyno,statusDesc)
{
	formobj.file.value = filename;
	formobj.pkValue.value = pk ;
	formobj.dnldurl.value=dnldurl;
	formobj.moduleName.value="Study";
	formobj.action="postFileDownload.jsp?statuspk="+statuspk+"&studyno="+studyno+"&statusDesc="+statusDesc;
	//formobj.action=dnldurl;
	
	formobj.target = "_filedownload";
	formobj.method = "POST";
	formobj.submit();
}

</SCRIPT> 



<SCRIPT language="JavaScript1.1">

	function openWin1() {

      windowName=window.open("#","Information","toolbar=no,location=no,scrollbars=yes,resizable=yes,menubar=no,width=600,height=300")
	windowName.focus();

;}

</SCRIPT>

 

<SCRIPT language="javascript">

function confirmBox(fileName,pgRight) {
	if (f_check_perm(pgRight,'E') == true) {
		var paramArray = [decodeString(fileName)];
		if (confirm(getLocalizedMessageString("L_Del_FrmApdx",paramArray))) {/*if (confirm("Delete " + decodeString(fileName) + " from Appendix?")) {*****/
		    return true;
		} else	{
			return false;
		}
	} else {
		return false;
	}		
}

</SCRIPT> 

  
<body>
<jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>
<jsp:useBean id="appendixB" scope="request" class="com.velos.eres.web.appendix.AppendixJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="statHistoryB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.CFG"%>
<BR>


  <%

   HttpSession tSession = request.getSession(true); 

   if (sessionmaint.isValidSession(tSession))

	{
       String includeTabsJsp = isIrb ? ("LIND".equals(CFG.EIRB_MODE) ? "ecompNewTabs.jsp" : "irbnewtabs.jsp") : "studytabs.jsp";
       String studyIdForTabs = "0";
       if(request.getParameter("studyId")!=null && !request.getParameter("studyId").equals("")){
       		studyIdForTabs = request.getParameter("studyId");
       }
       
       String uName = (String) tSession.getValue("userName");

   	String sessStudyId = "";
   		

   	String studyNo = "";
   	if(isPopupWin.equals("1")){
   		if(request.getParameter("studyId")!=null){
   			sessStudyId = request.getParameter("studyId");
   		}
   		studyB.setId(EJBUtil.stringToNum(sessStudyId));
   		studyB.getStudyDetails();
   		studyNo = studyB.getStudyNumber();
   	}else{
   		
   		sessStudyId=(String) tSession.getValue("studyId");
   		studyNo=(String) tSession.getValue("studyNo");
   	}
   	int studyVerId = EJBUtil.stringToNum(request.getParameter("studyVerId"));
   	studyVerB.setStudyVerId(studyVerId);
   	studyVerB.getStudyVerDetails();
   	String statusPk=request.getParameter("statusPk")==null?"":request.getParameter("statusPk");
   	String statusDesc=request.getParameter("statusDesc")==null?"":request.getParameter("statusDesc");
   	int studyVerCat = Integer.parseInt(studyVerB.getStudyVercat());
   	
   	UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
	uiFlxPageDao.getHighestFlexPageFullVersion("er_study", Integer.parseInt(studyIdForTabs));

	int versionMajor = uiFlxPageDao.getMajorVer();
	String versionNum = String.valueOf(versionMajor);
	
	if (versionMajor > 0){
		versionNum += ".";
		int versionMinor = uiFlxPageDao.getMinorVer();
		versionNum += (versionMinor == 0)? "00" : ((versionMinor > 9)? versionMinor : "0" + versionMinor);
	}
   	
   	String verNumber = "0";
   	verNumber=studyVerB.getStudyVerNumber();
   	if(!"LIND".equals(CFG.EIRB_MODE) && !verNumber.equals("0")){
   		verNumber+=".00";
   	}
   	CodeDao freezeCode = new CodeDao();
   	int pkfreezeCode = freezeCode.getCodeId("versionStatus","F");
   	String freezeDesc = freezeCode.getCodeDesc(pkfreezeCode);
    StatusHistoryDao shDao = new StatusHistoryDao();
    shDao = statHistoryB.getStatusHistoryInfo(studyVerId, "er_studyver");
    boolean freezeFlag = false;
    
    UIFlxPageDao uiFlxPage = new UIFlxPageDao();
  	ArrayList<String> lockedVersions = uiFlxPage.getAllLockedStudyVersions(EJBUtil.stringToNum(studyIdForTabs));
  	for (String version: lockedVersions){
  		System.out.println("Locked Version::::"+version);
  	}
  	if (lockedVersions.contains(versionNum) || statusDesc.equals(freezeDesc)){
	    freezeFlag = shDao.getCodelstIds().contains(String.valueOf(pkfreezeCode));
  	}
  	System.out.println("versionNum::::"+versionNum);
    System.out.println("freezeFlag::::"+freezeFlag);
   	if(!isPopupWin.equals("1")){
	%>

	<DIV class="BrowserTopn" id="div1"> 
		  <jsp:include page="<%=includeTabsJsp%>" flush="true">
		  <jsp:param name="from" value="<%=from%>"/> 
		  <jsp:param name="studyId" value="<%=studyIdForTabs%>"/>
	  </jsp:include>
	</DIV>
<%}
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;  
   	if (isPopupWin.equals("1")){  
	String studyNumber=null;
    String studyTitle=null;
	if(!studyIdForTabs.equals("")){
		studyB.setId(EJBUtil.stringToNum(studyIdForTabs));
		studyB.getStudyDetails();
		studyNumber = studyB.getStudyNumber();
		studyTitle = studyB.getStudyTitle();
    }
	if (ienet == 0) {%>
		<table width="100%"><tr><td>
		<DIV  id="div2" style="margin-top:6%;">
	<%}else{ %>
		<table width="100%"><tr><td width="75%">
		<DIV  id="div2" style="margin-top:4%;">
	<%} %>
<SCRIPT LANGUAGE="JavaScript">
	var study_Title = htmlEncode('<%=studyTitle%>');
</SCRIPT>
<h5><b>
<%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>:<%=studyNumber %>&nbsp;&nbsp;&nbsp;&nbsp;
<%=LC.L_Version%><%--Version*****--%>:<%=versionNum %>&nbsp;&nbsp;&nbsp;&nbsp;
<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>:<img src="./images/View.gif" onmouseover="return overlib(study_Title,CAPTION,'<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>' ,RIGHT , BELOW );" onmouseout="return nd();" > &nbsp;&nbsp;&nbsp;&nbsp;
</b></h5></DIV></td><td width="25%"><a href="studyVerBrowser.jsp?mode=N&isPopupWin=1&studyId=<%=studyIdForTabs%>"><%=LC.L_Back_to_Attach%><%--Back to Attachment Versions*****--%></a></td></tr></table>
<% }%>

<!-- Bug#15424 Fix for 1360*768 resolution- Tarun Kumar -->	

<SCRIPT LANGUAGE="JavaScript">
/* Condition to check configuration for workflow enabled */
<% if("Y".equals(CFG.Workflows_Enabled)&& (studyIdForTabs!=null && !studyIdForTabs.equals("") && !studyIdForTabs.equals("0"))){ %>
		if(screenWidth>1280 || screenHeight>1024)
			document.write('<DIV class="BrowserBotN  BrowserBotN_S_3 workflowDivBig" id = "div1" style = "height:76%;">')
		else
			document.write('<DIV class="BrowserBotN  BrowserBotN_S_3 workflowDivSmall" id = "div1" >')
<% } else{ %>
		if(screenWidth>1280 || screenHeight>1024)
			<%if(isIrb){ %>
			document.write('<DIV class="BrowserBotN  BrowserBotN_S_3" id = "div1" style = "height:76%;margin-top:30px;">')
			<%}else{%>
			document.write('<DIV class="BrowserBotN  BrowserBotN_S_3" id = "div1" style = "height:76%;">')
			<%}%>
		else
			<%if(isIrb){ %>
			document.write('<DIV class="BrowserBotN  BrowserBotN_S_3"  id = "div1" style="margin-top:30px;">')
			<%}else{%>
			document.write('<DIV class="BrowserBotN  BrowserBotN_S_3"  id = "div1">')
			<%}%>
<% } %>
</SCRIPT>

		<%

	
	String verStatus = "";//studyVerB.getStudyVerStatus();
	
	
	verStatus = statHistoryB.getLatestStatus(studyVerId,"er_studyver");
	if(verStatus == null)
		verStatus = "W";

	String tab="";

      tab = request.getParameter("selectedTab");

	String space = request.getParameter("outOfSpace");
	space = (space==null)?"":space.toString();

	String incorrectFile = request.getParameter("incorrectFile");
	incorrectFile = (incorrectFile==null)?"":incorrectFile.toString();		
	if(space.equals("1")) {
%>
<br><br><br><br>
<table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl">
<tr>
<td align=center>
<p class = "sectionHeadings">
<%=MC.M_UploadSpace_ContAdmin%><%-- The space allotted to you for file uploading has exhausted. Please contact Velos Administrator.*****--%> 
</p>
</td>
</tr>

<tr height=20></tr>
<tr>
<td align=center>

		<button onclick="window.history.go(-2);"><%=LC.L_Back%></button>
</td>		
</tr>		
</table>		


<%		
	} else if(incorrectFile.equals("1")) {
%>
<br><br><br><br>
<table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl">
<tr>
<td align=center>

<p class = "sectionHeadings">

<%=MC.M_FileUpldEmpty_IncorrPath%><%-- The file that you are trying to upload is either empty or the path specified is incorrect.*****--%>  

</p>
</td>
</tr>

<tr height=20></tr>
<tr>
<td align=center>

		<button onclick="window.history.go(-1);"><%=LC.L_Back%></button>
</td>		
</tr>		
</table>		


<%		
	} else { //else of if for incorrectFile.equals("1")


	if(sessStudyId == "" || sessStudyId == null) {

%>
  <jsp:include page="studyDoesNotExist.jsp" flush="true"/>
  <%

   } else {   		
	   int studyId = EJBUtil.stringToNum(sessStudyId);
   	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");	 
	   int pageRight =0;	
   	   if ((stdRights.getFtrRights().size()) == 0){
	 	pageRight= 0;
	   }else{
		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYAPNDX"));
   	   }
    	   if (pageRight > 0 )
   	   {

		int stId=EJBUtil.stringToNum(sessStudyId);
		AppendixDao appendixDao =new AppendixDao();
		appendixDao=appendixB.getByStudyIdAndType(studyVerId, "url");

		ArrayList appendixIds = appendixDao.getAppendixIds(); 
		ArrayList appendixDescriptions = appendixDao.getAppendixDescriptions();
		ArrayList appendixTypes = appendixDao.getAppendixTypes();
		ArrayList appendixFile_Uris = appendixDao.getAppendixFile_Uris();
		
		ArrayList appendixFiles = appendixDao.getAppendixFiles();

		ArrayList appendixStudyIds = appendixDao.getAppendixStudyIds();
		
		ArrayList appendixStudyVers = appendixDao.getAppendixStudyVers();
		
		ArrayList appendixPubFlags =appendixDao.getAppendixPubFlags();
		


		String appendixId=null;

		int appId;
	
		String appendixDescription=null;

		String appendixType=null;

		String appendixFile_Uri = null;

		String appendixFile = null;

		String appendixStudy = null;
		
		String appendixStudyVer = null;		

		String appendixPubFlag = null;	

		int len = appendixIds.size();
	   	int counter = 0;
		Object temp;
		
	%>
	<table width="100%"><tr><td align="center">
	<b><%=LC.L_Version_Number%><%-- Version Number*****--%>: <%=verNumber%> </b>&nbsp;&nbsp;
	<%
	if ("LIND".equals(CFG.EIRB_MODE)){
		if (freezeFlag && !verStatus.equals("V") && !verStatus.equals("NP")){
			%>
			<FONT class="Mandatory"><%=MC.M_VerNum_Frozen%><%-- You cannot make any changes in a Freeze Version.*****--%></Font>
	<%}
	}else{
	 if (verStatus.equals("F")) {
	 %>
      <FONT class="Mandatory"><%=MC.M_CntChg_InFreezeVer%><%-- You cannot make any changes in a Freeze Version.*****--%></Font>
	 <%}}%>
	</td></tr></table>
  <Form name="apendixbrowser" method="post" action="" onsubmit="">
<TABLE WIDTH="100%">  
<TR>

<TD WIDTH="50%" valign="top">

    <%

	AppendixDao appendixDao1 =new AppendixDao();

	appendixDao1=appendixB.getByStudyIdAndType(studyVerId, "file");

	ArrayList appendixIds1 = appendixDao1.getAppendixIds(); 

	ArrayList appendixDescriptions1 = appendixDao1.getAppendixDescriptions();


	ArrayList appendixTypes1 = appendixDao1.getAppendixTypes();

	ArrayList appendixFile_Uris1 = appendixDao1.getAppendixFile_Uris();
	
	ArrayList appendixFiles1 = appendixDao1.getAppendixFiles();
	

	ArrayList appendixStudyIds1 = appendixDao1.getAppendixStudyIds();

	ArrayList appendixStudyVers1 = appendixDao1.getAppendixStudyVers();
	
	ArrayList appendixPubFlags1 =appendixDao1.getAppendixPubFlags();



	String appendixId1=null;

	String appendixDescription1=null;

	String appendixType1=null;

	String appendixFile_Uri1 = null;

	String appendixFile1 = null;

	String appendixStudy1 = null;
	String appendixStudyVer1 = null;
	String appendixPubFlag1 = null;	

	

	int len1 = appendixIds1.size();



   	int counter1 = 0;



	Object temp1;

%>



    
    <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midalign" >
      <tr> 
        <th width="100%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"> <%=LC.L_My_Files%><%-- My Files*****--%> </th>
      </tr>
     
      <tr> 
        <td> 
         
	    <% if ("LIND".equals(CFG.EIRB_MODE)){
			if (freezeFlag && !verStatus.equals("V") && !verStatus.equals("NP")){%>
				
			<%}else{%>
				<A href="appendix_file.jsp?srcmenu=<%=src%>&studyId=<%=sessStudyId %>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>&studyVerNumber=<%=versionNum %>&isPopupWin=<%=isPopupWin %>&statusPk=<%=statusPk %>&statusDesc=<%=statusDesc %>" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=MC.M_ClickHere_UploadNewFile%></A><%--CLICK HERE  to upload a new File.*****--%>
			<%}
			}else{
	    if (!verStatus.equals("F")) {%>
      
		  <A href="appendix_file.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>&statusPk=<%=statusPk %>&statusDesc=<%=statusDesc %>" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=MC.M_ClickHere_UploadNewFile%></A><%--CLICK HERE  to upload a new File.*****--%> 
        </td>
      </tr>
	  <%}}%>
      
    </table>
    <div style="width:100.5%; max-height:340px; overflow:auto">
    <table cellspacing="0" cellpadding="0" border="0"  class="basetbl outline midalign">
	<th width="30%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_File_Name%><%-- File name*****--%></th>
	<th width="60%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Description%><%-- Description*****--%></th>
	<th width="5%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Edit%><%-- Edit*****--%></th>
	<th width="8%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Delete%><%-- Delete*****--%></th>
      <%

	for(counter1 = 0;counter1<len1;counter1++)

	{	

		temp1=appendixIds1.get(counter1);			

		if (!(temp1 ==null))

			appendixId1=temp1.toString();

			appId=EJBUtil.stringToNum(appendixId1);	

		temp1=appendixDescriptions1.get(counter1);			

		if (!(temp1 ==null))

			appendixDescription1=temp1.toString();

		temp1=appendixFile_Uris1.get(counter1);	

		if (!(temp1 ==null))

			appendixFile_Uri1 = temp1.toString();

		temp1=appendixFiles1.get(counter1);	

		if (!(temp1 ==null))

			appendixFile1 = temp1.toString();	

		temp1=appendixStudyVers1.get(counter1);

		if (!(temp1 ==null))	

			appendixStudyVer1 = temp1.toString();

		temp1=appendixPubFlags1.get(counter1);

		if (!(temp1 ==null))	

			appendixPubFlag1 = temp1.toString(); 			 

	

		if ((counter1%2)==0) {

		%>
      <tr class="browserEvenRow"> 
        <%

			}

		else{

		%>
      <tr class="browserOddRow"> 
        <%

			}

		%>
        <td width=40%> 
          <% 	String dnld;
	com.aithent.file.uploadDownload.Configuration.readSettings("eres");
	com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "study");		  
	dnld=com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET ;
	//SV, 7/27/04, fix for bug #1201, where a file with embedded # sign will cause browser to look for a HTML section tag. 
	//Fix: Encode the file name here and servlets decodes it later. 
	String modDnld = dnld + "?file=" + StringUtil.encodeString(appendixFile_Uri1) ;	
	%>
<span style="overflow: hidden; width: 150px; word-wrap: break-word; display: block;">
          <A href="#" onClick="fdownload(document.apendixbrowser,<%=appId%>,'<%=StringUtil.encodeString(appendixFile_Uri1)%>','<%=dnld%>','<%=statusPk %>','<%=studyNo %>','<%=statusDesc %>');return false;" >
          
            
          
          <%=appendixFile_Uri1%>
           
          </A></span> </td>
		<td width="50%">
		<span style="overflow: hidden; width: 230px; word-wrap: break-word; display: block;">
		<%=appendixDescription1%>
		</span>
		</td>
  		<td width=5%>
		  <% if ("LIND".equals(CFG.EIRB_MODE)){
				if (!freezeFlag){%>
					<A href="appendix_file_edit.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&appndxId=<%=appId%>&studyVerId=<%=studyVerId%>&studyId=<%=studyIdForTabs %>&isPopupWin=<%=isPopupWin %>&statusPk=<%=statusPk %>&statusDesc=<%=statusDesc %>" onClick="return f_check_perm(<%=pageRight%>,'E')" ><img src="./images/edit.gif" title="<%=LC.L_Edit %>" border="0"/></A>
				<%}
		  }else{
			if (!verStatus.equals("F")) {%> 
				<A href="appendix_file_edit.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&appndxId=<%=appId%>&studyVerId=<%=studyVerId%>&studyId=<%=studyIdForTabs %>&statusPk=<%=statusPk %>&statusDesc=<%=statusDesc %>" onClick="return f_check_perm(<%=pageRight%>,'E')" ><img src="./images/edit.gif" title="<%=LC.L_Edit %>" border="0"/></A>
		  <%}}%> 
        </td>  
        <td width=5%  align="center">
        <% if ("LIND".equals(CFG.EIRB_MODE)){
				if (!freezeFlag){%>
					<A href="appendix_file_delete.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&appId=<%=appId%>&studyVerId=<%=studyVerId%>&studyId=<%=studyIdForTabs %>&isPopupWin=<%=isPopupWin %>&statusPk=<%=statusPk %>&statusDesc=<%=statusDesc %>" onClick="return confirmBox('<%=appendixFile_Uri1%>',<%=pageRight%>);" ><img src="./images/delete.gif" title="<%=LC.L_Delete %>" border="0"/></A>
				<%}
		  }else{
			if (!verStatus.equals("F")) {%> 
				<A href="appendix_file_delete.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&appId=<%=appId%>&studyVerId=<%=studyVerId%>&studyId=<%=studyIdForTabs %>&statusPk=<%=statusPk %>&statusDesc=<%=statusDesc %>" onClick="return confirmBox('<%=appendixFile_Uri1%>',<%=pageRight%>);" ><img src="./images/delete.gif" title="<%=LC.L_Delete %>" border="0"/></A>
		  <%}}%> 
        </td>
      </tr>
      <%			
		}
	%>
    </table>
</div>
</TD>
<TD WIDTH="50%" valign="top">


    <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midalign" >
     <!-- <tr > 
        <td width = "100%"> 
          <P class = "defComments"><%=MC.M_PcolAppx_AsFollows%><%-- Protocol Appendix is as follows*****--%>:</P>
        </td>
      </tr>-->
      <tr> 
        <th width="100%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"> <%=LC.L_My_Links%><%-- My Links*****--%> </th>
      </tr>
      
      <tr> 
        <td> 
		   <% if ("LIND".equals(CFG.EIRB_MODE)){
				if (!freezeFlag){%>
					<A href="appendix_url.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&isPopupWin=<%=isPopupWin%>&studyId=<%=studyIdForTabs %>&studyVerId=<%=studyVerId%>&statusPk=<%=statusPk %>&statusDesc=<%=statusDesc %>" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=MC.M_ClickHere_AddNewLink%><%--CLICK HERE</A> to add a new Link.*****--%>
				<%}
		  }else{
			if (!verStatus.equals("F")) {%> 
				<A href="appendix_url.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>&statusPk=<%=statusPk %>&statusDesc=<%=statusDesc %>" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=MC.M_ClickHere_AddNewLink%><%--CLICK HERE</A> to add a new Link.*****--%>
		  <%}}%> 
        </td>
      </tr>

      
     
    </table>
    <div style="width:100.5%; max-height:340px; overflow:auto">
    <table  cellspacing="0" cellpadding="0" border="0" class="basetbl outline midalign">
	<th width="40%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Url_Upper%><%-- URL*****--%></th>
	<th width="50%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Description%><%-- Description*****--%></th>
	<th width="5%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Edit%><%-- Edit*****--%></th>
	<th width="5%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Delete%><%-- Delete*****--%></th>
      <%

	for(counter = 0;counter<len;counter++)
	{	
		temp=appendixIds.get(counter);			

		if (!(temp ==null))

			appendixId=temp.toString();

			appId=EJBUtil.stringToNum(appendixId); 	

		temp=appendixDescriptions.get(counter);			

		if (!(temp ==null))

			appendixDescription=temp.toString();

		temp=appendixFile_Uris.get(counter);	

		if (!(temp ==null))

			appendixFile_Uri = temp.toString();

		temp=appendixStudyVers.get(counter);

		if (!(temp ==null))	

			appendixStudyVer = temp.toString();

		temp=appendixPubFlags.get(counter);

		if (!(temp ==null))	

			appendixPubFlag = temp.toString(); 			 

	

		if ((counter%2)==0) {

		%>
      <tr class="browserEvenRow"> 
        <%

			}

		else{

		%>
      <tr class="browserOddRow"> 
        <%

			}

		%>
        <td width="40%"> 
        <span style="overflow: hidden; width: 150px; word-wrap: break-word; display: block;">
		<A href="<%=appendixFile_Uri%>" target="Information" onClick="openWin()"><%=appendixFile_Uri%></A>
		</span>		 
        </td>
		<td width="50%"><span style="overflow: hidden; width: 230px; word-wrap: break-word; display: block;"><%=appendixDescription%></span></td>
        <td width="5%">
 	    <% if (!(verStatus.equals("F")) && !isPopupWin.equals("1")) {%> 
			<A href="appendix_url_edit.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&appId=<%=appId%>&studyVerId=<%=studyVerId%>&statusPk=<%=statusPk %>&statusDesc=<%=statusDesc %>" onClick="return f_check_perm(<%=pageRight%>,'E')"><img src="./images/edit.gif" title="<%=LC.L_Edit%>" border="0" /></A>
		<%}%> 
        </td>
	
		 <td width="5%" align="center">
	    <% if (!(verStatus.equals("F")) && !isPopupWin.equals("1")) {%>
	     <!-- Changed By PK Bug #4017 --> 
		 <A href="appendix_file_delete.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&appId=<%=appId%>&studyVerId=<%=studyVerId%>&statusPk=<%=statusPk %>&statusDesc=<%=statusDesc %>&studyId=<%=studyIdForTabs %>" onClick="return confirmBox('<%= StringUtil.encodeString(appendixFile_Uri) %>',<%=pageRight%>);" ><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" /></A>
		<%}%> 
        </td>
      </tr>
      <%			

		

		}

	%>
    </table>
    </div>
<TD>
</TR>
</TABLE>

    <input type="hidden" name="studyId" value=<%=sessStudyId%>>
    
    
    <input type="hidden" name="tableName" value="ER_STUDYAPNDX">
    <input type="hidden" name="columnName" value="STUDYAPNDX_FILEOBJ">
    <input type="hidden" name="pkColumnName" value="PK_STUDYAPNDX">
    <input type="hidden" name="module" value="study">
    <input type="hidden" name="db" value="eres">
    <input type="hidden" name="pkValue" value="">
    <input type="hidden" name="file" value="">
    <input type="hidden" name="dnldurl" value="">
    <input type="hidden" name="moduleName" value="">
    
    
  </FORM>
  
<%
if(isPopupWin.equals("1")){%>
	<br><div style="text-align:center;">
	<button type="button" onclick="window.close()" style="width:05%; position:relative; margin:auto; text-align: center; background-color: #5c5c5c; font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:12px; color:#fff; border-radius:3px 3px 3px 3px;" value="<%=LC.L_Close %>"><%=LC.L_Close %></button>
	</div>
<%}
	} //end of if body for page right
	else
	{
%>
	  <jsp:include page="accessdenied.jsp" flush="true"/>
 <%	} //end of else body for page right

}//end of else
}//end of if for space.equals("1")
}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id="emenu"> 
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</BODY>



</HTML>

<%--dnld ="http://deepali:8080/appendixweb/servlet/Download/" + appendixFile1+"";JNDINames.ERESEARCH_CONF_PATH--%>