<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<HTML>
<HEAD>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>	
	

<SCRIPT>

	function confirmContinue(formobj, mode, name) {
		
			formobj.action="updateNewUser.jsp";		
			
			//KM-Modified for User Name and Email duplicate checking.
			if (mode =='M'){			
				if (name == 'UserName')
					formobj.countOfUserMod.value= "1";
				else 
	    			formobj.countOfEmailMod.value= "1";

			}
			else{
			
				if (name == 'UserName')
					formobj.countOfUserNew.value= "1";
				else
					formobj.countOfEmailNew.value= "1";
			}
    		
    		
    		formobj.submit();    		   		 
    		
	}

</SCRIPT>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id="usr" scope="request" class="com.velos.eres.web.accountWrapper.AccountWrapperJB"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="userBUpdate" scope="request" class="com.velos.eres.web.user.UserJB"/>

<jsp:useBean id="groupB" scope="request" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="usertemp" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>
<jsp:useBean id ="auditRowEresJB" scope="session" class="com.velos.eres.audit.web.AuditRowEresJB"/>
<jsp:useBean id ="audittrails" scope="session" class="com.aithent.audittrail.reports.AuditUtils"/>
<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*"%>
<%@ page language = "java" import = "com.velos.eres.web.moreDetails.*" %>

<%
	String pname=request.getParameter("pname");
	if (pname == null)
		pname = "null";
	int ret = 0;
	String usrFirstName,usrMidName, usrLastName, usrJobType, usrPrimarySpeciality,usrWrkExp,usrPhaseInv ;
	String  usrSiteName, usrGroup, usrLogin, usrPass, usrAddress, usrCity, usrState;
	String usrZip, usrEmail, usrCountry, usrPhone,userAddId, usrESign;
	String userId = null;
	String mode, userSecretQues, userSecretAns,src;
	String messageFrom = "";
	String messageText = "";
	String messageSubject = "";
	String messageHeader = "";
	String messageFooter = "";		
	String completeMessage = "";
	// String smtpHost = "";
	// String domain = "";
	String messageTo = "";
	int rows = 0;
	String uStat = "";
	String pwdXpDate="";
	String esignXpDate="";
	String usr_session = "";
	String timeZone="";
	String messageSubtext = "";
	String userResetPass = ""; 
	String usereResetSign = "";
	String prevStatus = ""; 
	String accsites="";
	String siteUrl = "";
	String resetMailStatus = "";
	String newUserMailStatus = "";
	String mailSentMessage = "";
	String userLoginModuleId="";
	String userLoginModuleMap="";
	String nLevel= "";
	String networkUsrId = "";
	
	String donot_sendNotificationFlag = "";
	
	int countUser = 0; 
	int countUserEmail = 0; 
	
	int metaTime = 1;
		   
	// Added by Rajeev K - 02/18/04
	// Begin
	// get site URL
	rows = 0;
	CtrlDao urlCtrl = new CtrlDao();	
	urlCtrl.getControlValues("site_url");
	rows = urlCtrl.getCRows();
	if (rows > 0) {
		siteUrl = (String) urlCtrl.getCValue().get(0);
	}
	// End

	//get velos user
	rows = 0;
	CtrlDao userCtrl = new CtrlDao();	
	userCtrl.getControlValues("eresuser");
	rows = userCtrl.getCRows();
	if (rows > 0){
		messageFrom = (String) userCtrl.getCDesc().get(0);
	}	   
	String eSign = request.getParameter("eSign");	
	String userType = request.getParameter("userType");	
	mode = request.getParameter("mode");	
	
	
	int countUserNames = 0; 
	int countUserMails = 0;
	
	String userAddIdHide = request.getParameter("userAddId"); 
	String userIdHide = request.getParameter("userId"); 
	
	
    String deactivateFromTeam = request.getParameter("deactivateFromTeam");
    //@Ankit 25-May-2011 #6251 
    UserDao userDao = userB.getUsersDetails(userIdHide);
    ArrayList uType = userDao.getUsrTypes();
    String dbUserType = "";
    if(uType!=null && uType.size()>0)
    {
    	dbUserType = (String)uType.get(0);
    }
    
    if (StringUtil.isEmpty(deactivateFromTeam))
    {
     deactivateFromTeam = "0";
    } 
	
	
	src = request.getParameter("src");
	String srcmenu = request.getParameter("srcmenu");
	nLevel = (request.getParameter("nLevel")==null)?"":request.getParameter("nLevel");
	networkUsrId = (request.getParameter("networkUsrId")==null)?"0":request.getParameter("networkUsrId");
	   

	
	usrFirstName = request.getParameter("userFirstName");
	usrFirstName = usrFirstName.trim();
	usrMidName = request.getParameter("userMidName");
	usrMidName  = usrMidName.trim();
	usrLastName = request.getParameter("userLastName");
	usrLastName = usrLastName.trim();
	
	usrFirstName = StringUtil.stripScript(StringUtil.htmlDecode(usrFirstName) ) ;
	usrMidName = StringUtil.stripScript(StringUtil.htmlDecode(usrMidName));
	usrLastName = StringUtil.stripScript(StringUtil.htmlDecode(usrLastName) ) ;
	
	
	usrJobType = request.getParameter("jobType");
	usrPrimarySpeciality = request.getParameter("primarySpeciality");
	usrWrkExp = request.getParameter("userWrkExp");
	usrWrkExp = (   usrWrkExp  == null      )?"":(  usrWrkExp ) ;	
	usrPhaseInv = request.getParameter("userPhaseInv");
	usrPhaseInv = (   usrPhaseInv  == null      )?"":(  usrPhaseInv ) ;	
	usrSiteName = 	request.getParameter("accsites");
	usrSiteName=(usrSiteName==null)?"":usrSiteName;
	
	//incase user is changed from Personalize account, site dropdown is not displayed
	if (usrSiteName.length()==0) usrSiteName = request.getParameter("userSiteId");
	usrGroup = request.getParameter("usrgrps");
	//prevGroup holds the previous value of the default group
	//will be different from usrGroup if default group was changed
	//if default group is changed,studyteam would be refreshed
	String prevGroup=request.getParameter("prevgroup");
	prevGroup=(prevGroup==null)?"":prevGroup;
	usrLogin = request.getParameter("userLogin");
	
	if (! StringUtil.isEmpty(usrLogin)){
		usrLogin = usrLogin.trim();
	}	
	
	usrPass = request.getParameter("userPassword");	
	userSecretAns = request.getParameter("userSecretAns");
	userSecretQues	= request.getParameter("userSecretQues");
	usrESign = request.getParameter("userESign");
	usrAddress = request.getParameter("userAddress");
	usrAddress = (   usrAddress  == null      )?"":(  usrAddress ) ;	
	usrCity = request.getParameter("userCity");
	usrCity = (   usrCity  == null      )?"":(  usrCity ) ;	
	usrState = request.getParameter("userState");
	usrState = (   usrState  == null      )?"":(  usrState ) ;	
	usrZip = request.getParameter("userZip");
	usrZip = (   usrZip  == null      )?"":(  usrZip ) ;	
	usrPhone = request.getParameter("userPhone");
	usrPhone = (   usrPhone  == null      )?"":(  usrPhone ) ;	
	usrEmail = request.getParameter("userEmail");
	usrEmail = (   usrEmail  == null      )?"":(  usrEmail ) ;	
	usrCountry = request.getParameter("userCountry");
	usrCountry = (   usrCountry  == null      )?"":(  usrCountry ) ;	
	uStat = request.getParameter("userStatus");
	timeZone = request.getParameter("timeZone");
	pwdXpDate= request.getParameter("pwdXpDate");
	esignXpDate = request.getParameter("esignXpDate");
	usr_session = request.getParameter("userSession");	
	accsites = request.getParameter("userSiteId");
	HttpSession tSession = request.getSession(true); 
	
	String ipAdd = (String) tSession.getValue("ipAdd");
    userLoginModuleId=request.getParameter("dnname");
    userLoginModuleMap=request.getParameter("lmmap");
    int rid=0;
	String usr1 = null;
	usr1 = (String) tSession.getValue("userId");
	String userName=(String) tSession.getAttribute("userName");
	//Added by Gopu for July-August'06 Enhancement (U2)
	String acc = (String) tSession.getValue("accountId");
	
	donot_sendNotificationFlag = request.getParameter("donotsend_notification");
	
	if (StringUtil.isEmpty(donot_sendNotificationFlag))
	{
		donot_sendNotificationFlag = "0";
	}
	
	//Added by Manimaran for Enh.#U11
	String userHidden = request.getParameter("userHidden");
	if (userHidden != null) {
		if(!userHidden.equals("0")) //KM-22Aug08
		userHidden = "1";
	}
	else
		userHidden = "0";
	
	if (sessionmaint.isValidSession(tSession)) {	
		%>
		<jsp:include page="sessionlogging.jsp" flush="true"/>	
		<%   
		String oldESign = (String) tSession.getValue("eSign");

		if(!oldESign.equals(eSign)) {
		%>
			<jsp:include page="incorrectesign.jsp" flush="true"/>
		<%
		} else {
		
		//KM-Moved up here.
		String JFlag=request.getParameter("JFlag");
		if (JFlag==null) JFlag="";

		String flagUserMod = request.getParameter("countOfUserMod");
		flagUserMod = (flagUserMod==null)?"":flagUserMod;
				
		String flagEmailMod = request.getParameter("countOfEmailMod");
		flagEmailMod = (flagEmailMod==null)?"":flagEmailMod;

		String flagUserNew = request.getParameter("countOfUserNew");
		flagUserNew = (flagUserNew==null)?"":flagUserNew;			
				
		String flagEmailNew = request.getParameter("countOfEmailNew");	
		flagEmailNew = (flagEmailNew==null)?"":flagEmailNew;


		%>
	<Form name = "userdetailssave" method="post" action="updateNewUser.jsp"  onsubmit = "" >		
			
	<input type="hidden" name="userAddId" size = 20  value = "<%=userAddIdHide%>" >
	<input type="hidden" name="mode" size = 20  value = "<%=mode%>" >
	<input type="hidden" name="userType" size = 20  value = "<%=userType%>" >
	<input type="hidden" name="eSign" size = 20  value = "<%=eSign%>" >
	<input type="hidden" name="pname" size = 20  value = "<%=pname%>" >	
	<input type="hidden" name="src" size = 20  value = "<%=src%>" >
	<input type="hidden" name="srcmenu" size = 20  value = "<%=srcmenu%>" >
	<input type="hidden" name="userFirstName" size = 20  value = "<%=usrFirstName%>" >
	<input type="hidden" name="userMidName" size = 20 value = "<%=usrMidName %>" >
	<input type="hidden" name="userLastName" size = 20  value = "<%=usrLastName%>" >
	<input type="hidden" name="jobType" size = 20  value = "<%=usrJobType%>" >
	
	<input type="hidden" name="primarySpeciality" size = 20  value = "<%=usrPrimarySpeciality%>" >
	<input type="hidden" name="userWrkExp" size = 20  value = "<%=usrWrkExp%>" >
	<input type="hidden" name="userPhaseInv" size = 20  value = "<%=usrPhaseInv%>" >
	<input type="hidden" name="accsites" size = 20  value = "<%=usrSiteName%>" >
	<input type="hidden" name="userSiteId" size = 20  value = "<%=usrSiteName%>" >
	<input type="hidden" name="usrgrps" size = 20  value = "<%=usrGroup%>" >
	<input type="hidden" name="prevgroup" size = 20  value = "<%=prevGroup%>" >
	<input type="hidden" name="userLogin" size = 20  value = "<%=usrLogin%>" >
	<input type="hidden" name="userPassword" size = 20  value = "<%=usrPass%>" >
	
	<input type="hidden" name="userSecretAns" size = 20  value = "<%=userSecretAns%>" >
	<input type="hidden" name="userSecretQues" size = 20  value = "<%=userSecretQues%>" >
	<input type="hidden" name="userESign" size = 20  value = "<%=usrESign%>" >
	<input type="hidden" name="userAddress" size = 20  value = "<%=usrAddress%>" >
	<input type="hidden" name="userCity" size = 20  value = "<%=usrCity%>" >
	<input type="hidden" name="userState" size = 20  value = "<%=usrState%>" >
	<input type="hidden" name="userZip" size = 20  value = "<%=usrZip%>" >
	<input type="hidden" name="userPhone" size = 20  value = "<%=usrPhone%>" >
	<input type="hidden" name="userEmail" size = 20  value = "<%=usrEmail%>" >
	
	<input type="hidden" name="userCountry" size = 20  value = "<%=usrCountry%>" >
	<input type="hidden" name="userStatus" size = 20  value = "<%=uStat%>" >
	<input type="hidden" name="timeZone" size = 20  value = "<%=timeZone%>" >
	<input type="hidden" name="pwdXpDate" size = 20  value = "<%=pwdXpDate%>" >
	<input type="hidden" name="esignXpDate" size = 20  value = "<%=esignXpDate%>" >
	<input type="hidden" name="userSession" size = 20  value = "<%=usr_session%>" >
	<input type="hidden" name="userSiteId" size = 20  value = "<%=accsites%>" >
	<input type="hidden" name="ipAdd" size = 20  value = "<%=ipAdd%>" >
	<input type="hidden" name="dnname" size = 20  value = "<%=userLoginModuleId%>" >
	
	<input type="hidden" name="lmmap" size = 20  value = "<%=userLoginModuleMap%>" >	
	<input type="hidden" name="userId" size = 20  value = "<%=userIdHide%>" >
	<input type="hidden" name="donotsend_notification" size = 20  value = "<%=donot_sendNotificationFlag%>" >	
		
	<!--KM-Modified for duplicate checking -->
	<input type="hidden" name="countOfUserMod" value="<%=flagUserMod%>" >
	<input type="hidden" name="countOfEmailMod" value="<%=flagEmailMod%>">
	<input type="hidden" name="countOfUserNew" value ="<%=flagUserNew%>">
	<input type="hidden" name="countOfEmailNew" value ="<%=flagEmailNew%>">
	<input type="hidden" name="deactivateFromTeam" value=<%=deactivateFromTeam%> >
	<input type="hidden" name="userHidden" value=<%=userHidden%> >
	<input type="hidden" name="JFlag" value="<%=JFlag%>" > <!--KM-11Aug08 -->
	
		
		<%
			String supportEmail = Configuration.SUPPORTEMAIL;
			String accId = (String) tSession.getValue("accountId");
	
			if (mode.equals("M")){
				userId = request.getParameter("userId"); 
				usr.setUserId(userId);
				usr.populateUser();
			}
			
			usr.setUserLoginModuleId(userLoginModuleId);
			usr.setUserLoginModuleMap(userLoginModuleMap);
			usr.setUserLastName(usrLastName);	
			usr.setUserMidName(usrMidName);
			usr.setUserFirstName(usrFirstName);
			usr.setUserCodelstJobtype(usrJobType);
			usr.setUserTimeZoneId(timeZone);
			usr.setUserCodelstSpl(usrPrimarySpeciality);
			usr.setUserWrkExp(usrWrkExp);
			usr.setUserPhaseInv(usrPhaseInv);
			usr.setUserSiteId(usrSiteName);
			usr.setUserType("S");
			
			
			if (pname.equals("null")) {
				usr.setUserGrpDefault(usrGroup);
				usr.setUserLoginName(usrLogin);
			}
			
		   //String JFlag=request.getParameter("JFlag");//JM
           //if (JFlag==null) JFlag="";

			//Added by Gopu for July-August'06 Enhancement (U2) - Admin Settings for Session timeout / Password, e-Sign expiry date.
			SettingsDao settingsDao=commonB.getSettingsInstance();
			int modname=1;
			ArrayList lstKeyword = new ArrayList();
			String keyword;
			String logsetvalue="";
			String pwdsetvalue="";
			String esignsetvalue="";
			lstKeyword.add("ACC_SESSION_TIMEOUT");		
			lstKeyword.add("ACC_DAYS_PWD_EXP");		
			lstKeyword.add("ACC_DAYS_ESIGN_EXP");
			settingsDao.retrieveSettings(EJBUtil.stringToNum(acc),modname,lstKeyword);
			ArrayList setkeywords=settingsDao.getSettingKeyword();
			ArrayList setvalues=settingsDao.getSettingValue();
			
			if (setkeywords!= null ){
				for(int i=0;i<setkeywords.size();i++){
					keyword=(setkeywords.get(i)==null)?"":(setkeywords.get(i)).toString();
					if(keyword.equals("ACC_SESSION_TIMEOUT"))
						logsetvalue=(setvalues.get(i)==null)?"":(setvalues.get(i)).toString();	
					if(keyword.equals("ACC_DAYS_PWD_EXP"))
						pwdsetvalue=(setvalues.get(i)==null)?"":(setvalues.get(i)).toString();
					if(keyword.equals("ACC_DAYS_ESIGN_EXP"))
						esignsetvalue=(setvalues.get(i)==null)?"":(setvalues.get(i)).toString();
						
						
				}
			}

						

			if (mode.equals("N") || (mode.equals("M") && userType.equals("NS")&& JFlag.equals("") )){
				usr.setUserESign(usrESign);
				usr.setUserPwd(Security.encryptSHA(usrPass));
				
				Date d = new Date();

				if(pwdsetvalue.equals("")){				
					d.setDate(d.getDate() + 365);
					usr.setUserPwdExpiryDate(DateUtil.dateToString(d));
					usr.setUserPwdExpiryDays("365");
				}else{ //Added by Gopu				
					d.setDate(d.getDate() + EJBUtil.stringToNum(pwdsetvalue));
					usr.setUserPwdExpiryDate(DateUtil.dateToString(d));
					usr.setUserPwdExpiryDays(pwdsetvalue);
				}
				
				if(esignsetvalue.equals("")){
					d = new Date();//JM:
					d.setDate(d.getDate() + 365);
					usr.setUserESignExpiryDate(DateUtil.dateToString(d));
					usr.setUserESignExpiryDays("365");
				}else{ //Added by Gopu
					d = new Date();//JM:
					d.setDate(d.getDate() + EJBUtil.stringToNum(esignsetvalue));
					usr.setUserESignExpiryDate(DateUtil.dateToString(d));
					usr.setUserESignExpiryDays(esignsetvalue);
				}
					
				//set the default session time
				if (logsetvalue.equals(""))
					usr.setUserSessionTime("15");		
				else //Added by Gopu
					usr.setUserSessionTime(logsetvalue);
				usr.setUserPwdReminderDate(DateUtil.getFormattedDateString("9999","01","01"));     
				usr.setUserESignReminderDate(DateUtil.getFormattedDateString("9999","01","01"));

			}
			
			usr.setUserAccountId(accId);
			if (pname.equals("null")   ) {
				usr.setUserStatus(uStat);
			}
			usr.setAddPriUser(usrAddress);
			usr.setAddCityUser(usrCity);
			usr.setAddStateUser(usrState);
			usr.setAddZipUser(usrZip);
			usr.setAddCountryUser(usrCountry);
			usr.setAddPhoneUser(usrPhone);
			usr.setAddEmailUser(usrEmail);

			if ((pname.equals("null") && (EJBUtil.stringToNum((String)tSession.getValue("userId")) == EJBUtil.stringToNum(userId ))) || (mode.equals("M") && userType.equals("NS"))) {
				usr.setUserAnswer(userSecretAns);
				usr.setUserSecQues(userSecretQues);
				
			}
			
			int loginCount=userB.getCount(usrLogin);
			int count =0;

			
			if (mode.equals("M") )
			{
			// added false to skip it always, an ajax validation is done on userDetails.jsp instead
			  if ((mode.equals("M") && userType.equals("NS") && JFlag.equals(""))&&false) {

				if(loginCount > 0) {
				%>
					<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>
					<p class = "successfulmsg" align = center> <%=MC.M_LoginIdExst_EtrNew%><%-- The Login ID you have entered already exists. Please enter a new Login Id.*****--%> </p>					
						<table align=center>
							<tr width="100%"> 
								<td width="100%" > 
									<button onClick="window.history.back()" name="return" type="button"><%=LC.L_Return%><%--Return*****--%></button>
								</td>
							</tr>
						</table>				
					<%
					return;
				  }
				}

			//if the user changes the primary organization, set the user site flag to S - User has access to only specified organizations
				if (!(accsites.equals(usrSiteName))) {
				   usr.setUserSiteFlag("S");
				}
				userAddId = request.getParameter("userAddId"); 
				usr.setUserId(userId);
				usr.setUserPerAddressId(userAddId );
				usr.setModifiedBy(usr1);
				usr.setIpAdd(ipAdd);
				usr.setUserType("S");
				//KM
				usr.setUserHidden(userHidden);
				
				usertemp.setUserId(EJBUtil.stringToNum(userId));
				usertemp.getUserDetails();
				prevStatus = usertemp.getUserStatus(); 
				
				if (!(mode.equals("M") && userType.equals("NS"))) //JM: 
					usr.setUserESign(usertemp.getUserESign()); 

				//KM-#3683
				if (!(mode.equals("M") && userType.equals("NS"))) {
				usr.setUserPwdExpiryDays(usertemp.getUserPwdExpiryDays());
				usr.setUserPwdReminderDate(usertemp.getUserPwdReminderDate());
				usr.setUserESignExpiryDays(usertemp.getUserESignExpiryDays());
				usr.setUserESignReminderDate(usertemp.getUserESignReminderDate());
				}

				//JM: 29Nov2007:				

				countUserNames = userB.getCount(usrLastName, usrFirstName, accId, mode, userIdHide );
				countUserMails = userB.getCount(usrEmail, accId, mode, userIdHide  );		
	
			
//				String flagUserMod = request.getParameter("countOfUserMod");
//				flagUserMod = (flagUserMod==null)?"":flagUserMod;
//				
//				String flagEmailMod = request.getParameter("countOfEmailMod");
//				flagEmailMod = (flagEmailMod==null)?"":flagEmailMod;
				
// added false to skip it always, an ajax validation is done on userDetails.jsp instead
				if((countUserNames>0 && flagUserMod.equals(""))&& false) {
				
				%>
				
				
					<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>
					<p class = "successfulmsg" align = center> <%=MC.M_UsrName_AldyExst%><%-- The User Name you have entered already exists. Do you want to continue?*****--%></p>										
						<table align=center>
							<tr width="100%"> 
								<td width="100%" > 
						<!--KM-Modified for duplicate checking -->	
									<button name="Continue" onclick="return confirmContinue(document.userdetailssave, 'M','UserName');"><%=LC.L_Ok_Upper%><%--OK--%></button>
									<button name="return" type="button" onClick="window.history.back()"><%=LC.L_Cancel%><%--Cancel--%></button>									
								</td>
							</tr>
						</table>				
				
					
					<%
					
					return;
					
					
					} 
				// added false to skip it always, an ajax validation is done on userDetails.jsp instead
				if((countUserMails>0 && flagEmailMod.equals(""))&&false) {


				%>
				
					<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>
					<p class = "successfulmsg" align = center> <%=MC.M_EmailIdExst_Cont%><%-- The User Email ID you have entered already exists. Do you want to continue?*****--%></p>					
						<table align=center>
							<tr width="100%"> 
								<td width="100%" >
									<button name="Continue" onclick="return confirmContinue(document.userdetailssave, 'M','Email');"><%=LC.L_Ok_Upper%><%--OK--%></button>
									<button name="return" onClick="window.history.back(); return false;" type="button"><%=LC.L_Cancel%><%--Cancel--%></button>
								</td>
							</tr>
						</table>					
					<%
					return;
				}


			   //JM: 12Dec2007: 			
			   //to convert a non-system user to the Active user	
			    if (mode.equals("M") && userType.equals("NS") && JFlag.equals("") && loginCount==0){ //KM
				   userB.changeUserType(EJBUtil.stringToNum(userId), EJBUtil.stringToNum(usrGroup), usrLogin,Security.encryptSHA(usrPass),Security.encrypt(usrESign),usrEmail,EJBUtil.stringToNum(usr1));
			    }
			

				//Added by Manimaran to fix the Bug2657
				if ( (mode.equals("M") && userType.equals("NS")) || ((mode.equals("M") &&  !(userType.equals("NS")) )))
				{
					rid = auditRowEresJB.getRidForDeleteOperation(StringUtil.stringToNum(userId), StringUtil.stringToNum(usr1), "ER_USER", "PK_USER");
					ret = usr.updateUser();
					if(ret >= 0) {
					if(rid > 0){
						audittrails.updateAuditROw("eres", EJBUtil.stringToNum(usr1)+", "+userName.substring(userName.indexOf(" ")+1,userName.length())+", "+userName.substring(0,userName.indexOf(" ")), StringUtil.integerToString(rid), "U");
					}
					}
					
					 
				}
				
				if (mode.equals("M") && ret >= 0 && deactivateFromTeam.equals("1"))
				{
					System.out.println("deactivateFromTeam" + deactivateFromTeam);
					  
					userBUpdate.deactivateUser(userId,usr1,ipAdd);
				}
				
				if 	(uStat == null)
					uStat = "-";

				//	JM: 04032006 modified, for including blocked user
				//	if (uStat.equals("A") && prevStatus.equals("D"))				
				//@Ankit 25-May-2011 #6251 
				if (uStat.equals("A") && (prevStatus.equals("D") || prevStatus.equals("B") ) && (!dbUserType.equals("N"))) {
				   double randomNumber=Math.random();
				   String randomNum=String.valueOf(randomNumber);
				   userResetPass=randomNum.substring(2,10);
				   usereResetSign=randomNum.substring(11,15);
				   //APR-04-2011,MSG-TEMPLAGE FOR USER INFO V-CTMS-1
				   userB.notifyUserResetInfo(userResetPass,siteUrl,usereResetSign,userId,usrLogin,"R",usr1); //Modified By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.
				   
				/*   messageSubtext=" \nNew Password    : " + userResetPass +  "\nNew e-Signature : " + usereResetSign + "\nApplication URL : " + siteUrl;
				   messageTo = usrEmail;
				   messageSubject = "Request for change of password/esign";	
				   messageHeader ="Dear Velos eResearch Member," ; 
				   messageText = "\n\nAs per your request your Password/e-Signature has been reset. Your new Password/e-Signature is:\n\nLogin ID           :    " + usrLogin + messageSubtext + "\n\nYou may of course change your Password/e-Signature at any time should you have any concerns regarding the privacy of your information. To change your Password/e-Signature, you will need to login with the above ID , go to the Personalize Account section and then change your Password/e-Signature.\n\nTo begin using the Velos eResearch system, login to your account.\n **** Important ****\nPlease remember that the above information needs to be kept in a secure place. Any one with access to the above information can access your account information.\n\nVelos eResearch" ;
				   messageFooter = "\n\nThis email transmission and any documents, files or previous email messages attached to it may contain information that is confidential or legally privileged. If you are not the intended recipient or a person responsible for delivering this transmission to the intended recipient you are hereby notified that you must not read this transmission and that any disclosure, copying, printing, distribution or use of this transmission is strictly prohibited. If you have received this transmission in error, please immediately notify the sender by telephone or return email and delete the original transmission and its attachments without reading or saving in any manner." ;
				   completeMessage = messageHeader  + messageText + messageFooter ;
				   try{
							VMailer vm = new VMailer();
							VMailMessage msgObj = new VMailMessage();
							msgObj.setMessageFrom(messageFrom);
							msgObj.setMessageFromDescription("Velos eResearch Customer Services");
							msgObj.setMessageTo(messageTo);
							msgObj.setMessageSubject(messageSubject);
							msgObj.setMessageSentDate(new Date());
							msgObj.setMessageText(completeMessage);
							vm.setVMailMessage(msgObj);
							resetMailStatus = vm.sendMail(); 							
							mailSentMessage = "and Notification sent.";									
					}
					catch(Exception ex){
							  resetMailStatus = ex.toString();
					}
			  
				*/	//end of if for user status	
				}

				// set the attempt count
				//JM: 04032006: 
				//if( prevStatus.equals("D") && uStat.equals("A") )
				//@Ankit 25-May-2011 #6251 
				if( (prevStatus.equals("D") || prevStatus.equals("B")) && uStat.equals("A") && (!dbUserType.equals("N"))){
					userB.setUserId(EJBUtil.stringToNum(userId));
					userB.getUserDetails();
					userB.setUserAttemptCount("0");
					userB.setUserPwd(Security.encryptSHA(userResetPass));
					userB.setUserESign(usereResetSign);
					rid = auditRowEresJB.getRidForDeleteOperation(StringUtil.stringToNum(userId), StringUtil.stringToNum(usr1), "ER_USER", "PK_USER");
					userB.updateUser();		  
					
					if(rid > 0){
						audittrails.updateAuditROw("eres", EJBUtil.stringToNum(usr1)+", "+userName.substring(userName.indexOf(" ")+1,userName.length())+", "+userName.substring(0,userName.indexOf(" ")), StringUtil.integerToString(rid), "U");
					}
					
				}
			 
				 
			}

			
		
			//Added by Manimaran to fix the Bug-2558(eMail Notification to be send while converting Non-System to Active User)

			if ((mode.equals("N")) || ((mode.equals("M") && userType.equals("NS") && JFlag.equals(""))))	{
				
							
				count = userB.getCount(usrLogin);
	
				//JM: 29Nov2007:		
				countUserNames = userB.getCount(usrLastName, usrFirstName, accId, mode, "");		
				countUserMails = userB.getCount(usrEmail, accId, mode ,"" );
				
				//KM-07Aug08-Modified
				
				if (mode.equals("M") && userType.equals("NS") ) //KM
					count--;

		
//				String flagUserNew = request.getParameter("countOfUserNew");
//				flagUserNew = (flagUserNew==null)?"":flagUserNew;			
//				
//				String flagEmailNew = request.getParameter("countOfEmailNew");	
//				flagEmailNew = (flagEmailNew==null)?"":flagEmailNew;
				
				// added false to skip it always, an ajax validation is done on userDetails.jsp instead
				if(count > 0 && false) {
				%>
					<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>
					<p class = "successfulmsg" align = center> <%=MC.M_LoginIdExst_EtrNew%><%-- The Login ID you have entered already exists. Please enter a new Login Id.*****--%> </p>					
						<table align=center>
							<tr width="100%"> 
								<td width="100%" > 
									<button name="return" onClick="window.history.back()" type="button"><%=LC.L_Return%><%--Return*****--%></button>
								</td>
							</tr>
						</table>				
					<%
					return;
				} else if((countUserNames>0 && flagUserNew.equals("") && mode.equals("N"))&& false ) {

				%>
				
					<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>
					
					<!--KM-12Aug08-Modified -->
					<p class = "successfulmsg" align = center> <%=MC.M_UsrName_AldyExst%><%-- The User Name you have entered already exists. Do you want to continue?*****--%></p>
					    <table align=center>
							<tr width="100%"> 
								<td width="100%" > 
									<button name="Continue" onclick="return confirmContinue(document.userdetailssave, 'N','UserName');"><%=LC.L_Ok_Upper%><%--OK--%></button>
									<button name="return" onClick="window.history.back()" type="button"><%=LC.L_Cancel%><%--Cancel--%></button>
								</td>
							</tr>
						</table>					
					<%
					return;
				
				
				} else if((countUserMails>0 && flagEmailNew.equals("")  && mode.equals("N"))&&false) {

				%>
				
					<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>	<BR>
					<p class = "successfulmsg" align = center> <%=MC.M_EmailIdExst_Cont%><%-- The User Email ID you have entered already exists. Do you want to continue?*****--%></p>
						<table align=center>
							<tr width="100%"> 
								<td width="100%" >  <!--KM-12Aug08 -->
									<button name="Continue" onclick="return confirmContinue(document.userdetailssave, 'N','Email');"><%=LC.L_Ok_Upper%><%--OK--%></button>
									<button name="return" onClick="window.history.back()"><%=LC.L_Cancel%><%--Cancel--%></button>
								</td>
							</tr>
						</table>					
					<%
					return;
				} else {
					
					if (mode.equals("N")){
						usr.setCreator(usr1);
						usr.setIpAdd(ipAdd);
						usr.setUserHidden("0");//KM
						ret = usr.createUser();
						ret = 0;
					}
					//APR-04-2011,MSG-TEMPLAGE FOR USER INFO
					if(donot_sendNotificationFlag.equals("0")){ //AK:Fixed BUG#6250 (18th May,2011)
					userB.notifyUserResetInfo(usrPass,siteUrl,usrESign,usr.getUserId(),usrLogin,"ID",usr1); //Modified By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.
					userB.notifyUserResetInfo(usrPass,siteUrl,usrESign,usr.getUserId(),usrLogin,"P",usr1);  //Modified By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.
					}
					/*
					messageTo = usrEmail;
					messageSubject = "Your New Velos eResearch Account";	
					messageHeader ="\nDear New Velos eResearch Member," ; 
					messageText = "\n\nWelcome to Velos eResearch. As per your Organization's request an account has been created for you. Your account information is displayed below:\n\nLogin ID        : " + usrLogin + "\nPassword        : " + usrPass  + "\nE-Sign          : " + usrESign + "\nApplication URL :  " + siteUrl + "\n\nYou may of course change your password at any time should you have any concerns regarding the privacy of your information. To change your password, you will need to login with the above ID and password, go to the Personalize Account section and then change your password.\n\nTo begin using the Velos eResearch system, login to your account.\n**** Important ****\nPlease remember that the above information needs to be kept in a secure place. Any one with access to the above information can access your account information.\n\nThank you for your interest in Velos eResearch.\n\nVelos eResearch" ;
					messageFooter = "\n\nThis email transmission and any documents, files or previous email messages attached to it may contain information that is confidential or legally privileged. If you are not the intended recipient or a person responsible for delivering this transmission to the intended recipient you are hereby notified that you must not read this transmission and that any disclosure, copying, printing, distribution or use of this transmission is strictly prohibited. If you have received this transmission in error, please immediately notify the sender by telephone or return email and delete the original transmission and its attachments without reading or saving in any manner." ;
					completeMessage = messageHeader  + messageText + messageFooter ;


					 if (donot_sendNotificationFlag.equals("0")){
						try{
							VMailer vm = new VMailer();
							VMailMessage msgObj = new VMailMessage();
							
							msgObj.setMessageFrom(messageFrom);
							msgObj.setMessageFromDescription("Velos eResearch Customer Services");
							msgObj.setMessageTo(messageTo);
							msgObj.setMessageSubject(messageSubject);
							msgObj.setMessageSentDate(new Date());
							msgObj.setMessageText(completeMessage);
	
							vm.setVMailMessage(msgObj);
							
							newUserMailStatus = vm.sendMail(); 							
							mailSentMessage = "and Notification sent.";									
						}
						catch(Exception ex){
							newUserMailStatus = ex.toString();
						}
						
					 }	
					 
					 */
					
				}
			}

			MoreDetailsJB mdJB = new MoreDetailsJB();
			mdJB.saveMoredetails("user",request,usr.getUserId(),usr1);
			
			if(!"".equals(nLevel) && mode.equals("M")){
				   mdJB.saveMoredetails("user_"+nLevel,request,networkUsrId,usr1);
			   }

			if (count ==0) {
			%>
				<br>
				<br>
				<br>
				<br>
				<br>
				<%
				if (EJBUtil.isEmpty(newUserMailStatus) && EJBUtil.isEmpty(resetMailStatus) ){
					metaTime = 1;
					Object[] arguments1 = {mailSentMessage};%>
					<p class = "successfulmsg" align = center><%=VelosResourceBundle.getMessageString("M_DataSvd_Succ",arguments1)%><%--Data was saved successfully <%=mailSentMessage%>***** --%></p>
				<%} else if (! EJBUtil.isEmpty(newUserMailStatus)) {
					metaTime = 10;
					Object[] arguments2 = {newUserMailStatus};%>
					<p class = "redMessage" align = center> <%=VelosResourceBundle.getMessageString("M_DataSvdNewUsr_Msg",arguments2)%><%--Data was saved successfully but there was an error in sending notification to the new user.<br>
					Please contact Customer support with the following message:<BR><%=newUserMailStatus%>***** --%></p>
				<%}	else if (! EJBUtil.isEmpty(resetMailStatus)) {
					metaTime = 10;
					Object[] arguments3 = {resetMailStatus};%>
					<p class = "redMessage" align = center>  <%=VelosResourceBundle.getMessageString("M_DataSuccErr_CustSup",arguments3)%><%--Data was saved successfully but there was an error in sending notification to the user.<br>
					Please contact Customer Support with the following message:<BR><%=resetMailStatus%>*****--%></p>
				<%}

				if(!"networkTabs".equals(src)){
				if(pname.equals("ulinkBrowser")){
				%>
					<META HTTP-EQUIV=Refresh CONTENT="<%=metaTime%>; URL=userdetails.jsp?mode=<%=mode%>&srcmenu=<%=srcmenu%>&selectedTab=1&userId=<%=userId%>&pname=ulinkBrowser">
				<% } else if (mode.equals("M") && userType.equals("NS")){
			
					
					%>
					<META HTTP-EQUIV=Refresh CONTENT="<%=metaTime%>; URL=userdetails.jsp?mode=M&srcmenu=<%=srcmenu%>&selectedTab=1&userId=<%=userId%>&userType=NS&JFlag=1">
				<%}else{%>
					<META HTTP-EQUIV=Refresh CONTENT="<%=metaTime%>;	URL=accountbrowser.jsp?srcmenu=<%=srcmenu%>&cmbViewList=<%=userType%>">
				<%} }else{%>
				<script type="text/javascript">
    			 window.opener.location.reload();
	 			setTimeout("self.close()",500);
	 			</script>
					<%} // end of if for pname check
				}//end of if for count

			
			if(pname.equals("ulinkBrowser")){
			%>
				<META HTTP-EQUIV=Refresh CONTENT="<%=metaTime%>; URL=userdetails.jsp?mode=<%=mode%>&srcmenu=<%=srcmenu%>&selectedTab=1&userId=<%=userId%>&pname=ulinkBrowser" >
			<% } else if (mode.equals("M") && userType.equals("NS")){
				
	
				%>
				<META HTTP-EQUIV=Refresh CONTENT="<%=metaTime%>; URL=userdetails.jsp?mode=M&srcmenu=<%=srcmenu%>&selectedTab=1&userId=<%=userId%>&userType=NS&JFlag=1">
			<% } else { //else of if for pname
				%>
				<META HTTP-EQUIV=Refresh CONTENT="<%=metaTime%>; URL=accountbrowser.jsp?srcmenu=<%=srcmenu%>&cmbViewList=<%=userType%>">
			<%} //end of if for pname



		
		}// end of if body for e-sign 
	
	%>
	</Form>
	<%
	}//end of if body for session
	else {
	%>	
		<jsp:include page="timeout.html" flush="true"/>
	<%
	} 
	%>
</BODY>
</HTML>