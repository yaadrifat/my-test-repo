<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Alerts_Notifications%><%--Alerts and Notifications*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT language="JavaScript1.1">



function  validate(formobj){    
  
     if (!(validate_col('e-Signature',formobj.eSign))) return false

// 	if(isNaN(formobj.eSign.value) == true) {
<%-- 		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/ --%>
// 	formobj.eSign.focus();
// 	return false;
//      }

}




function openwin1(formobj,counter) {
	  var names = formobj.enteredByName[counter].value;
	  var ids = formobj.enteredByIds[counter].value;
	  var windowToOpen = "multipleusersearchdetails.jsp?fname=&lname=&from=alertnotify&mode=initial&ids=" + ids + "&names=" + names + "&rownum=" + counter;
//alert(windowToOpen);
      window.open(windowToOpen,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400")
;}




</SCRIPT>

</head>



<% String src="";
src= request.getParameter("srcmenu");
%>	

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.DateUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>

<body>
<br>

<DIV class="browserDefault" id="div1"> 


  <%

   HttpSession tSession = request.getSession(true); 

   if (sessionmaint.isValidSession(tSession))
   {
   String mode="N";//request.getParameter("mode");
   String uName = (String) tSession.getValue("userName");

	int personPK = 0;
	String patientId = "";
	String globalFlag = "";
	String dob = "";
	String gender = "";
	String genderId = "";
	String yob = "";
	String protocolId="";
	int age = 0;
	
	Calendar cal1 = new GregorianCalendar();

	String page1=request.getParameter("page");
	protocolId=request.getParameter("protocolId");
	//String studyId = (String) tSession.getValue("studyId");	
	String studyId = request.getParameter("studyId");
	String enrollId =(String) tSession.getValue("enrollId");
	globalFlag=request.getParameter("globalFlag");

	String lockSetting= request.getParameter("lockSetting");

	String studyTitle = "";
	String studyNumber = "";
	studyB.setId(EJBUtil.stringToNum(studyId));

    studyB.getStudyDetails();
    studyTitle = studyB.getStudyTitle();
	studyNumber = studyB.getStudyNumber();	
	
	
	patEnrollB.setPatProtId(EJBUtil.stringToNum(enrollId));
	patEnrollB.getPatProtDetails();
	//protocolId =patEnrollB.getPatProtProtocolId();
	

	
	//String protName = "";
	//if(protocolId != null) {
	//	eventAssocB.setEvent_id(EJBUtil.stringToNum(protocolId));
	//	eventAssocB.getEventAssocDetails();
	//	protName = 	eventAssocB.getName();
	//} else {
	//	protocolId = "";
	//}
	
	

%>
<br>
<P class="sectionHeadings"> <%=MC.M_MngPatAlrt_EvtStatNotify%><%--Manage <%=LC.Pat_Patients%> >> Alerts and Notifications >> Event Status Notification*****--%></P>
<%
//<jsp:include page="patienttabs.jsp" flush="true"> 
//</jsp:include>
%>

	<%
//if(protocolId != null) {

%>

<P class = "userName"> <%= uName %> </P>

<Form name="alertnotify" method=post >
	
</FORM>





<table class=tableDefault  width="100%" border=0>
<tr><td width= 40%>
	<p class = "sectionHeadings" ><%=LC.L_Notification%><%--Notification*****--%> </p>
</td>

<td width=60% align=right>
<%
if(lockSetting.equals("0"))
{
%>
	 <a href="notificationglobal.jsp?srcmenu=tdMenuBarItem5&selectedTab=5&mode=N&studyId=<%=studyId%>&globalFlag=<%=globalFlag%>&fromPage=patientEnroll&protocolId=<%=protocolId%>&lockSetting=<%=lockSetting%>"><%=LC.L_Add_Notification%><%--Add Another Notification*****--%></a>
<%
}
%>

	 
</td>
</tr>
</table>
<table  class=tableDefault  width="100%" border=0>
<tr>
	<th width=35% ><%=LC.L_For_EvtStatus%><%--For Event Status*****--%> </th>
	<th width=35% ><%=LC.L_Notify_User%><%--Notify User*****--%> </th>
	<th width=30% ><%=LC.L_Event%><%--Event*****--%></th>
</tr>

<%
%>
	<% EventInfoDao alertNotify = new EventInfoDao();	
		
	alertNotify.getAlertNotifyValues(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(protocolId),"N"); 
	ArrayList anCodelstDescs=alertNotify.getAnCodelstDescs();
	ArrayList anIds=alertNotify.getAnIds();	
	ArrayList anCodelstSubTypes=alertNotify.getAnCodelstSubTypes();
	ArrayList anTypes=alertNotify.getAnTypes();
	ArrayList anuserLists=alertNotify.getAnuserLists();
	ArrayList anuserIds=alertNotify.getAnuserIds();
	ArrayList anMobIds=alertNotify.getAnMobIds();
	ArrayList anFlags=alertNotify.getAnFlags();

	String anCodelstDesc="";
	String anId="";
	String anCodelstSubType="";
	String anType="";
	String anuserList="";
	String anuserId="";
	String anMobId="";
	String anFlag="";
	String status="";
	int checkcount=0;

	int length=anIds.size();
	//out.println("length is " +length);	
	%>
	<% for (int i=0;i<length; i++) {
	
	anCodelstDesc=((anCodelstDescs.get(i)) == null)?"-":(anCodelstDescs.get(i)).toString();
	anId=((anIds.get(i)) == null)?"-":(anIds.get(i)).toString();
	anCodelstSubType=((anCodelstSubTypes.get(i)) == null)?"-":(anCodelstSubTypes.get(i)).toString();
	anType=((anTypes.get(i)) == null)?"-":(anTypes.get(i)).toString();
	anuserList=((anuserLists.get(i)) == null)?"":(anuserLists.get(i)).toString();
	anMobId=((anMobIds.get(i)) == null)?"":(anMobIds.get(i)).toString();
	%>
	<% if ((checkcount%2)==0) { %>
      <tr class="browserEvenRow"> 
        <%}
	else{
	 %>
      <tr class="browserOddRow"> 
      <%	
	}
	checkcount++;
	%>	
	<td width=20% align="center">
	
	<%
	if(lockSetting.equals("0"))
	{
	%>
	 <a href="notificationglobal.jsp?srcmenu=tdMenuBarItem5&selectedTab=5&mode=M&studyId=<%=studyId%>&alertNotifyId=<%=anId%>&globalFlag=<%=globalFlag%>&fromPage=patientEnroll&protocolId=<%=protocolId%>&lockSetting=<%=lockSetting%>"> <%=anCodelstDesc%></a>
 	<%
	}
	else
	{
	%>
	 <%=anCodelstDesc%>
 	<%
	}
	%>

	 
	</td>

	<td width=25% align="center"> <%=anuserList%> </td>
	<td width=25% align="center"> <%=anMobId%> </td>

	<% } %>	
	</tr>

<tr><td>&nbsp</td><tr>
</table>

  <table width="100%" cellspacing="0" cellpadding="0">
  	<tr>
      <td align=center> 
		<A type="submit" href="editglobalsetting.jsp?studyId=<%=studyId%>&mode=N&srcmenu=<%=src%>&protocolId=<%=protocolId%>"><%=LC.L_Back%></A>	
      </td>
	<tr>
  </table>
<%
} //end of if session times out
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

} //end of else body for page right

%>
   <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>   
</div>
</body>
</html>
