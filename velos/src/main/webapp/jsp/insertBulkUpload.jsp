<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<jsp:include page="localization.jsp" flush="true"/>
<head>
 
 <SCRIPT LANGUAGE="JavaScript" SRC="js/velos/ajaxengine.js"></SCRIPT>
 
<title>Insert bulk upload data</title>
<script>

	function callAjaxUploadData(){
 
	var savemapval = document.getElementById("savemapfld").value;
	var autospecid = document.getElementById("autospecid").value;
   	var savemapchk= document.getElementById("savemapchk").value;

   
   new VELOS.ajaxObject("startUpload.jsp", {
   		urlData:"savemapfld="+savemapval + "&autospecid="+autospecid+"&savemapchk="+savemapchk ,
		   reqType:"POST",
		   outputElement: "logoutput" }
   ).startRequest();
   
  
}


 
</script>
</head>
    <%@ page autoFlush="true" buffer="1094kb"%>   
<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page  language = "java" import="com.velos.esch.business.common.*,com.velos.eres.bulkupload.web.FileUploadAction,com.velos.eres.bulkupload.web.BulkUploadJB,java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*"%>



<body onload="clearcon();">
  <%
  int j=0;
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
{
	String autospecid="";
	String savemapfld="";
	savemapfld=request.getParameter("savemapfld");
	savemapfld=savemapfld.replace("*****"," ").trim();
	String mapName="";
	
	ArrayList<Integer> rowBulk=(ArrayList)tSession.getAttribute("wholefilerows");
	String accId = (String) tSession.getValue("accountId");
	autospecid=request.getParameter("autospecid");
	ArrayList<Integer> insertFlag=(ArrayList)tSession.getAttribute("insertFlag");
	
	if (StringUtil.isEmpty(autospecid))
		{
			autospecid = "";
		}
		if (StringUtil.isEmpty(savemapfld))
		{
			savemapfld = "";
		}
	mapName=request.getParameter("savemapchk");
		if (StringUtil.isEmpty(mapName))
		{
			mapName = "";
		}
		BulkUploadJB istData=new BulkUploadJB();
		int size=rowBulk.size();
		
		%>
	<BR/>
	<table class="basetbl outline midAlign lhsFont" width="100%" cellspacing="0" cellpadding="0">
	 <tr ><td><font class="sectionHeadingsFrm"><b> Upload in progress</b></font></td></tr>
	 <tr height="20"><td width="30%">Total rows in the File : <%=size%><br/></td> </tr>
	 	  
    </table>
	<div id="logoutput"></div>
		 
<input type="hidden" id="savemapfld"  name="savemapfld" Value="<%=savemapfld%>">
<input type="hidden" id="autospecid"  name="autospecid" Value="<%=autospecid%>">
<input type="hidden" id="savemapchk"  name="savemapchk" Value="<%=mapName%>">
 
 
 	   
	 

	<script>
	callAjaxUploadData();
	</script>
	
  <%

}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  
  <% 
}
%>
 

</body>

</html>