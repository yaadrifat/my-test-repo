<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<%@page import="com.aithent.audittrail.reports.EJBUtil,com.aithent.audittrail.reports.AuditUtils"%>
<html>



<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%-- Nicholas : Start --%>
<%--<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>--%>
<%-- Nicholas : End --%>

<script>

	function setOrder(formObj,orderBy,pgRight) //orderBy column number
{
	var lorderType;
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";		
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";	
	}

	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;
	lselectedtab= formObj.selectedTab.value;
	formObj.action="allPatient.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType+"&selectedTab="+lselectedtab;
	formObj.submit(); 
}
function openFilter(repId){
 windowname=window.open("dynfilterbrowse.jsp?repId="+repId,'','resizable=1,status=0, width=850,height=550 top=100,left=100,scrollbars=1');
 windowname.focus();

}
function setFilterText(form){
 	  form.ptxt.value=form.patCode.value;
 	  form.atxt.value=form.patage[form.patage.selectedIndex].text ;
 	  if (form.patgender[form.patgender.selectedIndex].value != "") 
 	       form.gtxt.value=form.patgender[form.patgender.selectedIndex].text ;
 	  if (form.patstatus[form.patstatus.selectedIndex].value != "") 
 	   form.rtxt.value=form.patstatus[form.patstatus.selectedIndex].text ;
 	  form.rbytxt.value=form.regBy.value ;
 	  
 }
 function openDisplay(repId){
 windowname=window.open("dynpreview.jsp?repId="+repId+"&from=browse",'','resizable=1,status=0, width=850,height=550 top=100,left=100,scrollbars=1');
 
 windowname.focus();
 }
function delFltr(formobj,fltrId,fltrName){
formobj.mode.value="D";
srcmenu=formobj.srcmenu.value;
repId=formobj.repId.value;
repName=formobj.repName.value;
formId=formobj.formId.value;
formobj.fltrId.value=fltrId;
fltrName=decodeString(fltrName);
formobj.fltrName.value=fltrName;
var paramArray = [fltrName];
if (confirm(getLocalizedMessageString("M_SureWantDel_Tplt",paramArray))){/*if (confirm("Are you sure that you want to delete filter template ' "+fltrName+ "'")){*****/
formobj.action="dynfilterdelete.jsp?fltrId="+fltrId+"&fltrName="+fltrName+"&mode=D&srcmenu="+srcmenu+"&repId="+repId+"&repName="+repName+"&formId="+formId;
formobj.submit();
}
else{
return false;
}
}
</script>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="resolution.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<title><%=LC.L_Saved_FilterTemplates%><%--Saved Filter Templates*****--%></title>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.DateUtil,com.velos.eres.business.common.*" %>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.person.*,com.velos.eres.service.util.*,com.velos.eres.web.user.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<jsp:useBean id="dynrepfltr" scope="request" class="com.velos.eres.web.dynrepfltr.DynRepFltrJB"/>
<jsp:useBean id="dynrep" scope="request" class="com.velos.eres.web.dynrep.DynRepJB"/>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
	<%-- Nicholas : Start --%>
	<jsp:include page="skinChoser.jsp" flush="true"/>
	<%-- Nicholas : End --%>
	<style>
	html, body { overflow:auto; }
	</style>
<% String src;
src= request.getParameter("srcmenu");
%>

   
<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1) 
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {	
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>
<br>
<DIV class="popDefault" id="div1"> 
<%
  HttpSession tSession = request.getSession(true);
  if (sessionmaint.isValidSession(tSession)) {
  String creator="" ,createOn="",templateName="",fltrPk="",repType="";
  String  repId = request.getParameter("repId");
  repId=(repId==null)?"0":repId;
  String repName=StringUtil.decodeString(request.getParameter("repName"));
  String formId=request.getParameter("formId");
  if (formId==null) formId="0";
//  if (tSession.getAttribute("attributes")!=null) tSession.removeAttribute("attributes");
  String groupId="",groupNamePullDown="",abnresult="",abnrFlag="";
  String filEnrDt="",labText="",labStr="",groupStr="",abnText="",dateStr="",whereStr="",abnStr="";
  String[] deleteIds;
  
  int pageRight = 0;
   		
   String mode=request.getParameter("mode");
  if (mode==null) mode="";
  if (mode.equals("D")){
  String fltrId=request.getParameter("fltrId");
  fltrId=(fltrId==null)?"0":fltrId;
  String fltrName=request.getParameter("fltrName");
  fltrName=(fltrName==null)?"":fltrName;
  dynrepfltr.setId(EJBUtil.stringToNum(fltrId));
  //Modified for INF-18183 ::: Raviesh
  dynrepfltr.removeDynRepFltr(AuditUtils.createArgs(session,"",LC.L_Adhoc_Queries));
	ArrayList temp=new ArrayList();
	temp.add(fltrId);
  dynrep.removeFilters(temp);
  
  }
    
 //if (pageRight>0) {
	Calendar cal = new GregorianCalendar();
	int pstudy = 0;
     String searchFilter ="" ;
     String selectedTab = "" ; 
     selectedTab=request.getParameter("selectedTab");
    String uName = (String) tSession.getValue("userName");	
    UserJB user = (UserJB) tSession.getValue("currentUser");
    String userId = (String) tSession.getValue("userId");
    String accId=(String) tSession.getValue("accountId");

	//SV, 8/4/04, fix for bug #1535: show names as <first name> <last name>
	// changed the following from usr_lastname || ', ' || usr_firstname to usr_firstname ||' '|| usr_lastname
	//creator:

String sql =  "select pk_dynrepfilter,dynrepfilter_name,created_on,(select usr_firstname ||' '|| usr_lastname from er_user where pk_user=a.creator ) creator,lower(dynrepfilter_name) dynrepfilter_name_lower from er_dynrepfilter a where fk_dynrep=" +repId + " and fk_form is null "; 
String countsql=  "select count(*) from er_dynrepfilter a where fk_dynrep=" +repId + " and fk_form is null ";

			System.out.println(sql);
			System.out.println(countsql);
			
			String pagenum = "";
			int curPage = 0;
			long startPage = 1;
			long cntr = 0;
			String pagenumView = "";
			int curPageView = 0;
			long startPageView = 1;
			long cntrView = 0;
				
			pagenum = request.getParameter("pagenum");
			if (pagenum == null)
			{
			pagenum = "1";
			}
			curPage = EJBUtil.stringToNum(pagenum);
			
			String orderBy = "";
			orderBy = request.getParameter("orderBy");
			if (orderBy==null) orderBy="dynrepfilter_name_lower";
			String orderType = "";
			orderType = request.getParameter("orderType");

			if (orderType == null)
			{
			orderType = "asc";
			}


			pagenumView = request.getParameter("pageView");
			if (pagenumView == null)
			{
			pagenumView = "1";
			}
			curPageView = EJBUtil.stringToNum(pagenumView);

			String orderByView = "";
			orderByView = request.getParameter("orderByView");

			String orderTypeView = "";
			orderTypeView = request.getParameter("orderTypeView");

			if (orderTypeView == null)
			{
			orderTypeView = "asc";
			}

			long rowsPerPage=0;
			long totalPages=0;	
			long rowsReturned = 0;
			long showPages = 0;
			long totalRows = 0;	   
			long firstRec = 0;
			long lastRec = 0;	   
			boolean hasMore = false;
			boolean hasPrevious = false;

			Configuration conf = new Configuration();
			rowsPerPage = conf.getRowsPerBrowserPage(conf.ERES_HOME +"eresearch.xml");
			totalPages = conf.getTotalPagesPerBrowser(conf.ERES_HOME +"eresearch.xml");

			BrowserRows br = new BrowserRows();
			br.getPageRows(curPage,rowsPerPage,sql,totalPages,countsql,orderBy,orderType);
			rowsReturned = br.getRowReturned();
			showPages = br.getShowPages();
        
		     startPage = br.getStartPage();
		     
			hasMore = br.getHasMore();
			 
			hasPrevious = br.getHasPrevious();
			 
			
			totalRows = br.getTotalRows();	   	   
			
            
			firstRec = br.getFirstRec();
			
			lastRec = br.getLastRec();	  	   
			
	   
%>
<table width="100%"><tr><td width="90%"><P class="sectionHeadings"><%=MC.M_DynaRep_FtrTplt%><%--Dynamic Report>> Filter Templates*****--%> </P></td>
<td>&nbsp;&nbsp;<button onClick = "self.close()"><%=LC.L_Close%></button></td></tr></table> 
 
  <Form name="repbrowse" method="post">
  	       	  
    <br>	
 <P class="defComments"> <%=LC.L_Saved_FiltersAre%><%--Saved Filters are*****--%>: </P> 
<!--<Form  name="search" method="post" action="allPatient.jsp">-->
<input type="hidden" name="srcmenu" Value="<%=src%>">
<input type="hidden" name="totcount" Value="<%=rowsReturned%>">
<input type="hidden" name="mode" Value="">
<input type="hidden" name="repId" Value="<%=repId%>">
<input type="hidden" name="repName" Value="<%=StringUtil.encodeString(repName)%>">
<input type="hidden" name="formId" Value="<%=formId%>">
<input type="hidden" name="fltrId" Value="">
<input type="hidden" name="fltrName" Value="">





<!--</Form>-->

<input type="hidden" name="selectedTab" Value=<%=selectedTab%>> 	  
<Input type="hidden" name="orderBy" value="<%=orderBy%>">
<Input type="hidden" name="pagenum" value="<%=curPage%>">	
<Input type="hidden" name="orderType" value="<%=orderType%>">	
		
<%
//Retrieve study count for this user 


	ArrayList studyList=new ArrayList();



	 

	   int i = 0;


	   if(rowsReturned == 0) {

%>
	<%-- <table class="tableDefault" width="100%" border="0">
	<tr><td width="90%"></td>
	<td><A href="dynfiltermod.jsp?formId=<%=formId%>&fltrId=<%=fltrPk%>&mode=N&repId=<%=repId%>&repName=<%=repName%>">
	<p class="sectionHeadings">Add New</p></A></td></tr>
	</table> --%>
	
	<table class="tableDefault" width="100%" border="0">
<tr> 
	<th class=tdDefault width="50%"><%=LC.L_Filter_Name%><%--Filter Name*****--%></th>
	<th class=tdDefault width="25%"><%=LC.L_Created_By%><%--Created By*****--%></th>
	<th class=tdDefault width="10%"></th>		
		
   </tr>
	<tr><td>
	<P class="defComments"><%=MC.M_No_FilterTemplDfn%><%--No Filter Templates Defined.*****--%></P></td></tr>
	</table>


<%

	} else {


	
	String patientId = "";
	String studyNumber="" ;
	String totalEnroll="" ; 
	%>
	
	<%--<table class="tableDefault" width="100%" border="0">
  <tr><td width="90%"></td><td><A href="dynfiltermod.jsp?formId=<%=formId%>&fltrId=<%=fltrPk%>&mode=N&repId=<%=repId%>&repName=<%=repName%>"><p class="sectionHeadings">Add New</p></A></td></tr>
		</table> --%>
		
	<table class="tableDefault" width="100%" border="0">
<tr> 
	<th class=tdDefault width="50%"><%=LC.L_Filter_Name%><%--Filter Name*****--%></th>
	<th class=tdDefault width="25%"><%=LC.L_Created_By%><%--Created By*****--%></th>
	<th class=tdDefault width="10%"><%=LC.L_Delete%><%--Delete*****--%></th>		
		
   </tr>

<%
        
        
            
		for(i = 1 ; i <=rowsReturned ; i++)
	  	{   
	  	   templateName = br.getBValues(i,"dynrepfilter_name");
		   templateName=(templateName==null)?"-":templateName.trim();
		   if (templateName.length()==0) templateName="-";
	  	   creator = br.getBValues(i,"creator");
		   createOn=  br.getBValues(i,"created_on");
		   if (createOn==null) createOn="-";
		   fltrPk= br.getBValues(i,"pk_dynrepfilter") ;
		   
	   if ((i%2)==0) {

%>

      <tr class="browserEvenRow"> 

<%

	 		   }else{

%>

      <tr class="browserOddRow"> 

<% 

	 		   } %>
	<td width="50%">
		<%--<A href="dynfiltermod.jsp?formId=<%=formId%>&fltrId=<%=fltrPk%>&mode=M&fltrName=<%=StringUtil.encodeString(templateName)%>&repId=<%=repId%>&repName=<%=repName%>">--%>
		<%=StringUtil.decodeString(templateName)%></td>
        <td width ="25%"><%=creator%></td>
	<td width ="10%" align="center"><A href="#" onClick="delFltr(document.repbrowse,<%=fltrPk%>,'<%=StringUtil.encodeString(templateName)%>')"><img border="0" title="<%=LC.L_Delete%>" src="./images/delete.gif"/></A></td>
	
             </tr>

	  <%

//	  		} //end for temppatient

	  }// end for for loop
	  %>
	  </table>
<%
	  } // end of if for length == 0

  

	  %>

    
    	<table>
	<tr>
	<td>
	<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
		<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
	<%}%>	
	</td>
	</tr>
	</table>
	
	<table align="center">
	<tr>
<% 

	if (curPage==1) startPage=1;

    for (int count = 1; count <= showPages;count++)
	{
   cntr = (startPage - 1) + count;
	 
	if ((count == 1) && (hasPrevious))
	{   
    %>
	<td colspan = 2>
  	<A href="dynfilterbrowse.jsp?pagenum=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&srcmenu=<%=src%>&repId=<%=repId%>&repName=<%=repName%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
	</td>	
	<%
  	}	
	%>
	<td>
	<%
 
	 if (curPage  == cntr)
	 {
     %>	   
		<FONT class = "pageNumber"><%= cntr %></Font>
       <%
       }
      else
        {
       %>		

	   <A href="dynfilterbrowse.jsp?pagenum=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&srcmenu=<%=src%>&repId=<%=repId%>&repName=<%=repName%>"><%= cntr%></A>
       <%
    	}	
	 %>
	</td>
	<%
	  }

	if (hasMore)
	{   
   %>
   <td colspan = 3 align = center>
  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="dynfilterbrowse.jsp?pagenum=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&srcmenu=<%=src%>&repId=<%=repId%>&repName=<%=repName%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
	</td>	
	<%
  	}	
	%>
   </tr>
  </table>
	
    	



  </Form>

 		

 <%// }	//} //end of else body for page right



}//end of if body for session

else{

%>

  <jsp:include page="timeout.html" flush="true"/> 

  <%

}



%>

  <div> 

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>
</div>

</body>

</html>


