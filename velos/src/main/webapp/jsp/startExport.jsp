<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title>Start Export</title>
  
<%@ page import="com.velos.eres.business.common.*,java.sql.*,com.velos.eres.service.util.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.impex.*,java.text.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "java.util.*,com.velos.eres.business.common.*"%>

<body>
<br>
<DIV id="div1"> 

<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{

%>	
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   
		String eSign= request.getParameter("eSign");
		String src = "";
		
    	String oldESign = (String) tSession.getValue("eSign");
		String param = "";
		String paramVal = "";
		String paramName = "";
		
		
		String study ;
		String userId;

		int  selectedModulePkParam ;
		String  totalModulePkParam ;
		
		int selectedModulePk = 0;
		int totalModulePk = 0;

		String[] partSites = null;
		int siteCount = 0;
		
		
		StringBuffer sbExportXML = new StringBuffer();
		
		sbExportXML.append("<exp>");

	   if(oldESign.equals(eSign)) 
	   {
		
		String accId = (String) tSession.getValue("accountId");
		String originatingSiteCode  = "";
		
		//accId = request.getParameter("accountId");
		//get SITE_CODE OF THE ACCOUNT FROM WHERE DATA IS EXPORTED
		
		if (! StringUtil.isEmpty(accId))
		{
			PreparedStatement pstmt = null;
		    String sqlAcc = ""; 
		  	
		  	Connection conn = null;
		  	
		  	sqlAcc = " Select site_code from er_account where pk_account = ?";
		  	
		  	CommonDAO cd = new CommonDAO();
		  	
		  	
		  	try{
				conn = cd.getConnection();
					 
				pstmt = conn.prepareStatement(sqlAcc);
				pstmt.setInt(1,EJBUtil.stringToNum(accId));			
				 
				ResultSet rs = pstmt.executeQuery();
					
				while (rs.next())
				{	
					originatingSiteCode = rs.getString("site_code");
				}
			 }
			  catch (Exception ex)
			  {
			  	out.println("Exception in getting the account site code: " + ex );
			   }
			  finally
				{
					try
					{
						if (pstmt != null ) pstmt.close();
					}
					catch (Exception e) 
					{
					}
					try 
					{
						if (conn!=null) conn.close();
					}
					catch (Exception e) {}
					
				}	
			
		}
		
		study = request.getParameter("studyId");
		userId = request.getParameter("userId");
					
		sbExportXML.append("<study>" + study + "</study>");
		sbExportXML.append("<account>" + accId + "</account>");
		sbExportXML.append("<originatingSiteCode>"+ originatingSiteCode + "</originatingSiteCode>");
							
		String[] module = null;

			
		module = request.getParameterValues("module");
		
		int moduleCount = 0;
		int reqId = 0;

		//count modules		
		moduleCount = module.length;
		
		   //loop through the selected modules

		sbExportXML.append("<modules>");
			
			for (int i = 0; i < moduleCount; i++)
			{
					String[] arrModulePK = null;
					StringBuffer sbModulePK = new StringBuffer();
					String moduleSubType;
					
					moduleSubType = module[i];
					
					totalModulePkParam = request.getParameter(moduleSubType + "_totalcount");
					
					totalModulePk = EJBUtil.stringToNum(totalModulePkParam);
					
					if (totalModulePk > 0) //get the selected module Primary keys  
					{
						arrModulePK = request.getParameterValues(moduleSubType + "_pk");
						
						if (arrModulePK == null)
						{
							selectedModulePkParam = 0; 
						}else //the module has some primary keys selected
						{
							selectedModulePkParam = arrModulePK.length;
							
							for (int m = 0; m < selectedModulePkParam; m++)
							{
								sbModulePK.append("<modulePK>" + arrModulePK[m] +"</modulePK>");
							}
														
						}
						
					}
					else
					{
						selectedModulePkParam = 0;
					}
					
					
					//get pkCount for this module
					
					sbExportXML.append("<module name = \"" + module[i] + "\"");
					
					if (selectedModulePkParam == totalModulePk || totalModulePk == 0) 
					{
						sbExportXML.append(" expAll=\"Y\" > ");
					}
					else if (selectedModulePkParam < totalModulePk )
					{
						sbExportXML.append(" expAll=\"N\" >");
						sbExportXML.append(sbModulePK.toString());
					}
	
						sbExportXML.append("</module>");
	
					//out.println(module[i]);
	
			} 

	 	 sbExportXML.append("</modules>");
		 sbExportXML.append("</exp>");
		 System.out.println(sbExportXML.toString());
		%>
		<%
		
		// get participating sites
		
		partSites = request.getParameterValues("partSite");
		siteCount = partSites.length;
	
		//Prepare export Request

		ImpexRequest expReq = new ImpexRequest();
		
		Impex impexObj = new Impex();
		
   		for (int k=0; k < siteCount ; k++)
   		{
   			expReq.setExpSites(partSites[k]);
   		}
		
		 expReq.setReqDetails(sbExportXML.toString());
		 expReq.setReqStatus("0"); //export not started

		 expReq.setReqBy(userId);

		 //set datetime
		 
		Calendar now = Calendar.getInstance();
		String dt = "";
		dt = "" + now.get(now.DAY_OF_MONTH) + now.get(now.MONTH) + (now.get(now.YEAR) - 1900) ;
		Calendar calToday=  Calendar.getInstance();
		String format="yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat todayFormat=new SimpleDateFormat(format);
		dt = todayFormat.format(calToday.getTime());
		
		 expReq.setReqDateTime(dt);
	 
		// Log this export request

		impexObj.setImpexRequest(expReq);
		reqId = impexObj.recordExportRequest();
		impexObj.setRequestType("E");		
		impexObj.start();
		
		%>
			<BR><BR><BR><BR>
			<P align = "center" class="defComments">
			
			We have recorded your request. 
			<BR> Your request Id is : <font color="red"><b><%=reqId%></b></font> . Please use this Id to track your export request.
			<BR> Please wait while we take you to the 'Export Logs' page.
		
			</P>
		  <META HTTP-EQUIV=Refresh CONTENT="2; URL=viewexpstatus.jsp">
		<%
    
    	}//end of if old esign
	else{  
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%	  
		}//end of else of incorrect of esign
     }//end of if body for session 	
	
	else
	{
%>
	  <jsp:include page="timeout.html" flush="true"/>
<%
	
} //end of else of the Session 
 

%>
</div>
</body>

</html>

