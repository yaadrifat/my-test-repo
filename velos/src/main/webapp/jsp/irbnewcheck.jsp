<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.submission.SubmissionStatusJB,com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<%
String isNewAmendment="false";
session.setAttribute("isNewAmendment", isNewAmendment);
boolean isIrb = "irb_check_tab".equals(request.getParameter("selectedTab")) ? true : false; 
if (isIrb) {
%>
<title><%=MC.M_ResCompApp_ChkSub%><%--Research Compliance >> New Application >> Check and Submit*****--%></title>
<% } else { %>
<title><%=MC.M_Std_CkhSubmit%><%--<%=LC.Std_Study%> >> Check and Submit*****--%></title>
<% } %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*" %>
<%@ page import="com.velos.esch.business.common.SchMsgTxtDao" %>
<%@ page import="com.velos.eres.compliance.web.ComplianceJB" %>
<%@ page import="com.velos.eres.widget.business.common.UIFlxPageDao,com.velos.eres.widget.service.util.FlxPageArchive" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="complianceJB" scope="request" class="com.velos.eres.compliance.web.ComplianceJB"/>
<jsp:useBean id="submStatusB" scope="request" class="com.velos.eres.web.submission.SubmissionStatusJB"/>
<jsp:useBean id="submB" scope="request" class="com.velos.eres.web.submission.SubmissionJB"/>
	
<% 
String src= request.getParameter("srcmenu");
HttpSession tSession = request.getSession(true); 
String accountId = "";
String studyId = "";
String grpId = "";
String usrId = "";
ArrayList boardNameList = null;
ArrayList boardIdList = null;
ArrayList statusList = null;
ArrayList statusDateList = null;
EIRBDao eIrbDao = new EIRBDao();
FlxPageArchive archive = new FlxPageArchive();
boolean isBlockedForSubmission = false;
boolean isIrbApproved = false;
boolean isIrbDisapproved = false;
int cntVersionSubmitted = -1;
int cntProtocolFrozen = -1;
boolean isProtocolLocked= complianceJB.getIsLockedStudy();
if (sessionmaint.isValidSession(tSession))
{
    accountId = (String) tSession.getValue("accountId");
    studyId = (String) tSession.getValue("studyId");
    grpId = (String) tSession.getValue("defUserGroup");
    usrId = (String) tSession.getValue("userId");
    
	if (StringUtil.isEmpty(usrId))
	{
		usrId="";
	}
	System.out.println("accountId:::"+accountId);

	if("M".equals(request.getParameter("mode")) 
	        && request.getParameter("studyId") != null) {
        studyId = StringUtil.htmlEncodeXss(request.getParameter("studyId"));
	}
    if(accountId == null || accountId == "") {
    %>
		<jsp:include page="timeout.html" flush="true"/>
	<%
        return;
    }
    eIrbDao.getReviewBoards(EJBUtil.stringToNum(accountId),EJBUtil.stringToNum(grpId));
    boardNameList = eIrbDao.getBoardNameList();
    boardIdList = eIrbDao.getBoardIdList();
    
    eIrbDao.getCurrentStatuses(EJBUtil.stringToNum(accountId),EJBUtil.stringToNum(studyId),
            EJBUtil.stringToNum(grpId), EJBUtil.stringToNum(usrId), 0);
    statusList = eIrbDao.getCurrentStatuses();
    statusDateList = eIrbDao.getCurrentStatusDates(); 
    
    //Check if version already submitted
    HashMap paramMap = new HashMap();
  	paramMap.put("studyId",  EJBUtil.stringToNum(studyId));
    paramMap.put("userId", Integer.parseInt(usrId));
    paramMap.put("accountId", Integer.parseInt(accountId));
  	
  	// Check based on study status history
  	UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
	uiFlxPageDao.getHighestFlexPageFullVersion("er_study", EJBUtil.stringToNum(studyId));
	int flxPageVerMajor = uiFlxPageDao.getMajorVer(), flxPageVerMinor = uiFlxPageDao.getMinorVer();
	CodeDao codeSubmitted = new CodeDao();
	
	int existsFlag = 0;
	if (submStatusB.checkCurrentStatusByStudyId(codeSubmitted.getCodeId("subm_status","submitted"),submB.getNewAmendSubmissionByFkStudy(studyId))==1 ||
			submStatusB.checkCurrentStatusByStudyId(codeSubmitted.getCodeId("subm_status","resubmitted"),submB.getNewAmendSubmissionByFkStudy(studyId))==1){
		
		if(submStatusB.checkCurrentStatusByStudyId(codeSubmitted.getCodeId("subm_status","pi_resp_req"),submB.getNewAmendSubmissionByFkStudy(studyId))!=1
				&& submStatusB.checkCurrentStatusByStudyId(codeSubmitted.getCodeId("subm_status","approved"),submB.getNewAmendSubmissionByFkStudy(studyId))!=1){
			existsFlag=1;
		}
		if(submStatusB.checkCurrentStatusByStudyId(codeSubmitted.getCodeId("subm_status","approved"),submB.getNewAmendSubmissionByFkStudy(studyId))==1
			&& submStatusB.checkCurrentStatusByStudyId(codeSubmitted.getCodeId("subm_status","resubmitted"),submB.getNewAmendSubmissionByFkStudy(studyId))==1){
				existsFlag=1;
			}
	}else{
		if(submStatusB.checkCurrentStatusByStudyId(codeSubmitted.getCodeId("subm_status","pi_resp_req"),submB.getNewAmendSubmissionByFkStudy(studyId))!=1
				&& submStatusB.checkCurrentStatusByStudyId(codeSubmitted.getCodeId("subm_status","approved"),submB.getNewAmendSubmissionByFkStudy(studyId))!=1){
			existsFlag=1;
		}
	}
	System.out.println("existsFlag:::"+existsFlag);
	
	if(existsFlag==1){
		cntVersionSubmitted = archive.chkPageSubmitStatus( EJBUtil.stringToNum(studyId));
	}else{
		cntVersionSubmitted = archive.chkPageVersionSubmited(paramMap);
	}
	System.out.println("cntVersionSubmitted 1st:::"+cntVersionSubmitted);
  	isBlockedForSubmission = complianceJB.getIsBlockedForSubmission();
  	System.out.println("isBlockedForSubmission:::"+isBlockedForSubmission);
    if (cntVersionSubmitted == 0){
    	if (!isBlockedForSubmission){
	    	//Check if protocol is frozen
	    	cntProtocolFrozen = archive.chkPageProtocolFrozen(EJBUtil.stringToNum(studyId));
	    	System.out.println("cntProtocolFrozen---->"+cntProtocolFrozen);
    	}
    } else {
	    // Check based on study status history
	    if (cntVersionSubmitted == 2) {
	    	isBlockedForSubmission = true;
	    }
    }
    String currStudyStat = complianceJB.getCurrentStudyStatus(paramMap);
    if ("sys_err_click".equals(currStudyStat) || "sys_err_soa".equals(currStudyStat)) {
    	cntVersionSubmitted = -1;
    }
    
    isIrbApproved = complianceJB.getIsIrbApproved();
    /* if (!isIrbApproved) {
	    //Check based on submission status history
	    UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
 	    int submStatusCodelstPk = uiFlxPageDao.getFlexLatestSubmissionStatus(StringUtil.stringToNum(studyId));
 	    if (submStatusCodelstPk > 0) {
 		    CodeDao submCodeDao = new CodeDao();
 		    String submStatusSubtype = submCodeDao.getCodeSubtype(submStatusCodelstPk);
  		    if ("submitted".equals(submStatusSubtype) || "resubmitted".equals(submStatusSubtype)) {
  				SubmissionStatusJB submissionStatusJB = new SubmissionStatusJB();
  				submissionStatusJB.getSubmissionStatusDetails(uiFlxPageDao.getSubmissionStatusPk());
  				Date submStatDate = submissionStatusJB.getSubmissionStatusDate();
  				long timeDiff = System.currentTimeMillis() - submStatDate.getTime();
  				System.out.println("timeDiff="+timeDiff);
  				isBlockedForSubmission = (timeDiff < 600000L)? true : false;
  			}
 		}
 	} */
}
%>
<jsp:include page="include.jsp" flush="true"/>
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
<%if (!"LIND".equals(CFG.EIRB_MODE)){ %>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<%} %>
<SCRIPT language="javascript">
var completenessChecked = false;
function enableSubmitButton(boardIndexStr) {
	var allOff = true;
	for (var iX=0; iX < <%=boardNameList.size()%>; iX++) {
		var elem = document.getElementById("submitTo"+iX);
		if (elem != null && elem.checked) {
			allOff = false;
		}
	}
	if (allOff) {
		document.mainForm.action="";
	}
}

function submitForm(formobj) {
	
	if (!(validate_col('e-Signature',formobj.eSign))) return false;
		
	var allOff = true;
	for (var iX=0; iX < <%=boardNameList.size()%>; iX++) {
		var elem = document.getElementById("submitTo"+iX);
		if (elem != null && elem.checked) {
			allOff = false;
		}
	}
	if (allOff) {
		if (completenessChecked){
			alert("<%=MC.M_AtLeast_OneCheckbox%>");/*alert("Please check at least one checkbox.");*****/
		} else {
			alert("<%=MC.M_ChkComplete_b4Submit%>");/*alert("Check Completeness prior to protocol submission.");*****/
		}
		return false;
	}
	
	if(<%=cntVersionSubmitted%> == 0){
		formobj.action="freezeProtocol.jsp?&srcmenu=<%=request.getParameter("srcmenu")%>&selectedTab=irb_check_tab"; 
	} else {
		formobj.action="updateNewSubmission.jsp?&srcmenu=<%=request.getParameter("srcmenu")%>&selectedTab=irb_check_tab&from=init_subm"; 
	}
		return true;
}

function callAjaxForEachBoard(study_id, board_id, board_name,
		board_index, board_total, status, status_date) {
	new VELOS.ajaxObject("irbnewcheckajax.jsp", {
		urlData:"studyId="+study_id+"&boardId="+board_id+"&boardName="+board_name+
		    "&boardIndex="+board_index+"&totalBoardCount="+board_total+
		    "&status="+status+"&statusDate="+status_date,
		reqType:"POST",
		outputElement: "span_board"+board_index } 
	).startRequest();
}

function callAjax() {
	completenessChecked = true;
	<% for (int iY=0; iY<boardIdList.size(); iY++) { %>
		callAjaxForEachBoard("<%=studyId%>", "<%=boardIdList.get(iY)%>",
				"<%=boardNameList.get(iY)%>", "<%=iY%>", "<%=boardNameList.size()%>",
				"<%=statusList.get(iY)%>", "<%=statusDateList.get(iY)%>");
	<% } %>
}

function fixAndCloseDialog(apndxId) {
	$j.ajax({
		type: "POST",
		url: 'fixAttachment.jsp?apndxId='+apndxId,
		success: function(o) {
			if (o) {
				var json = JSON.parse(o);
				alert(json.message);
			}
		}
	});
	$j("#fixAttachmentDialog").dialog("close");
}

function openFixAttachmentDialog(apndxId, apndxName) {
	if(document.getElementById("fixAttachmentDialog") != null) {
		(elem=document.getElementById("fixAttachmentDialog")).parentNode.removeChild(elem);
	}
	var attachmentDiv = document.createElement('div');
	attachmentDiv.id = 'fixAttachmentDialog';
	attachmentDiv.name = 'fixAttachmentDialog';
	attachmentDiv.style.display = 'none';
	attachmentDiv.style.overflowY = 'auto';
	attachmentDiv.style.height = 200;
	if (!apndxName) { apndxName = ''; }
	var dispApndxName = ''+apndxName;
	dispApndxName = dispApndxName.replace(/"/g, '\'');
	attachmentDiv.innerHTML="<table class='' width='100%' cellspacing='4' cellpadding='4' >"+
		"<tr><td align='center'>Would you like to fix "+dispApndxName+
		" by copying from the lastest available version of this file?</td></tr><tr><td align='center'>"+
		" <button onclick='javascript:fixAndCloseDialog("+
		apndxId+");'>Fix</button></td></tr></table>";
	(document.getElementsByTagName("form")[0]).appendChild(attachmentDiv);
	$j("#fixAttachmentDialog").dialog({
		minHeight: 200,
		minWidth: 500,
		maxHeight: 200,
		maxWidth: 500,
		modal: true,
		draggable: true,
		resizable: false,
		closeOnEscape: true,
		title: 'Fix Attachment',
		show: 'blind',
		hide: 'blind',
		width: 600,
		height: 200,
		buttons: {
			"Close": function() {
				$j(this).dialog("close");
			}
		}
	});
}

$j(document).ready(function() { 
	$j('html, body').css("overflow", "hidden"); 
	var screenWidth = screen.width;
	var screenHeight = screen.height;
	var divHeight = (screenHeight > 768)? 400 : 280;
	$j("#chkSubmitDiv").css("max-height",divHeight);
	if("LIND"=="<%=CFG.EIRB_MODE%>" && "N"=="<%=CFG. Workflows_Enabled%>"){				
		$j("#chkSubmitDiv").css({marginLeft:"-14%"});
	}
	});
</SCRIPT> 

<body>

<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<%
	String from = "version";
	String tab = request.getParameter("selectedTab");
	  
	String studyIdForTabs = ""; 
 	studyIdForTabs = request.getParameter("studyId");
 	
    String includeTabsJsp = isIrb ? ("LIND".equals(CFG.EIRB_MODE) ? "ecompNewTabs.jsp" : "irbnewtabs.jsp") : "studytabs.jsp";
%>
<%if (!"LIND".equals(CFG.EIRB_MODE)){ %>
<DIV class="BrowserTopn" id = "divtab">
<jsp:include page="<%=includeTabsJsp%>" flush="true">
	<jsp:param name="from" value="<%=from%>"/> 
	<jsp:param name="selectedTab" value="<%=tab%>"/>
	<jsp:param name="studyId" value="<%=studyIdForTabs%>"/>
</jsp:include>
</DIV>
<%} %>
<%
String browserBotDivClass = "M".equals(request.getParameter("mode")) ? 
		"BrowserBotN BrowserBotN_S_1" : "BrowserBotN BrowserBotN_RC_1";
%>
<%if (!"LIND".equals(CFG.EIRB_MODE)){ %>
<SCRIPT LANGUAGE="JavaScript">
var screenWidth = screen.width;
var screenHeight = screen.height;
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="<%=browserBotDivClass %>" id = "div1" style="height:77%">')
	else
		document.write('<DIV class="<%=browserBotDivClass %>" id = "div1">')
</SCRIPT>

<%} %>
	<jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>
	<jsp:useBean id="appendixB" scope="request" class="com.velos.eres.web.appendix.AppendixJB"/>
	<%@ page language = "java" import = "com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
	<%
	
	if (sessionmaint.isValidSession(tSession))
	{
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	    int pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
        if(studyId == "" || studyId == null || "0".equals(studyId)) {
	    %>
	    <jsp:include page="studyDoesNotExist.jsp" flush="true"/>
	    <%
	    } else {
        %>
        <% if ("LIND".equals(CFG.EIRB_MODE)) { %>
        <form name="studyScreenForm">
           <input type="hidden" name="formStudy" value="<%=studyId %>" />
        </form>
        <% } %>
        <form name="mainForm" id="irbcheckid" method="post" onsubmit="if (submitForm(this) == false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
        <%System.out.println("cntVersionSubmitted:::::::::::::::>>>>>>>>>"+cntVersionSubmitted);
        if (cntVersionSubmitted == 1){
        	String resultMsg = MC.M_Version_Freeze;/*"Version already Freeze, submit the "*****/;
            %><p class="successfulmsg" align=center><%=resultMsg%></p>
            <META HTTP-EQUIV=Refresh CONTENT="1; URL=flexProtocolChangeLog?mode=M&selectedTab=irb_check_tab&studyId=<%=studyId%>">
         <%     
        } else {
        	cntVersionSubmitted = archive.chkPageSubmitStatus( EJBUtil.stringToNum(studyId));%>
        	<%if (isProtocolLocked && cntProtocolFrozen == 1 && cntVersionSubmitted != 2){%>
		        <%String resultMsg = MC.M_Version_Freeze;/*"Version already Freeze, submit the "*****/;%>
	            <p class="successfulmsg" align=center><%=resultMsg%></p>
	            <META HTTP-EQUIV=Refresh CONTENT="1; URL=flexResubmissionDraft?mode=M&selectedTab=irb_check_tab&studyId=<%=studyId%>">
	        <%} else {%>
		        <input type="hidden" name="totalBoardCount" value="<%=boardIdList.size()%>">
				<table width="100%" cellspacing="0" cellpadding="0" border="0">
		        <tr><td align="center"><br /></td></tr>
		        <tr><td align="center"><button name="checkSubmission" onclick="callAjax()" type="button"><%=LC.L_Check_Submission%></button></td></tr>
		        <tr><td align="center"><br /></td></tr>
		        </table>
		        <div id="chkSubmitDiv" style = "overflow:auto;">
			    <table class="tableDefault" width="100%" cellspacing="0" cellpadding="0" border="0">
		        <%  for(int iBoard=0; iBoard<boardNameList.size(); iBoard++) { %>
		        <tr><td width="100%">
		        <span id="span_board<%=iBoard%>">
		        <table class="tableDefault" width="100%" cellspacing="0" cellpadding="0" border="0">
		        <tr class="browserEvenRow"><td colspan="5">&nbsp;</td></tr>
		        <tr class="browserOddRow">
		            <td width="20%"><b><%=boardNameList.get(iBoard)%></b></td>
		            <td width="32%">&nbsp;</td>
		            <td width="15%">&nbsp;</td>
		            <td width="29%" align="center">&nbsp;</td>
		            <td width="4%" align="center"><input type="checkbox" disabled></td>
		        </tr>
		        </table>
		        </span>
		        <%  }  %>
			    </table>
			    </div>
		        <br />
		        <% if (!"LIND".equals(CFG.EIRB_MODE)) { %>
			       <jsp:include page="submitBar.jsp" flush="true"> 
						<jsp:param name="displayESign" value="Y"/>
						<jsp:param name="formID" value="irbcheckid"/>
						<jsp:param name="showDiscard" value="N"/>
			       </jsp:include>
		        <%
		           } else {
		        	   if (isIrbApproved) {
		        %>
		          <table class="tableDefault" width="100%" >
		            <tr><td align="center"><b><%=LC.L_EC_ProtocolIRBApproved%></b></td></tr>
		          </table>
		        <%
		        	   } else if (isIrbDisapproved) {
		        %>
		          <table class="tableDefault" width="100%" >
		            <tr><td align="center"><b><%=LC.L_EC_ProtocolIRBDisapproved%></b></td></tr>
		          </table>
		        <%
		        	   } else if (isBlockedForSubmission) {
		        %>
		          <table class="tableDefault" width="100%" >
		            <tr><td align="center"><b><%=LC.L_EC_CannotSubmitProtocol%></b></td></tr>
		          </table>
		          <%
		        	   }  else if (cntVersionSubmitted > -1){
		        		   if(pageRight==6 || pageRight==7 || pageRight==2){
		        %>
			       <jsp:include page="freezeProtocolVersionBar.jsp" flush="true"> 
							<jsp:param name="displayESign" value="Y"/>
							<jsp:param name="formID" value="irbcheckid"/>
							<jsp:param name="showDiscard" value="N"/>
			       </jsp:include>
		        <%     
		        		   }
		               }  else {
		            	   if(pageRight==6 || pageRight==7 || pageRight==2){
		        %>
		        
		        <jsp:include page="submitBar.jsp" flush="true"> 
							<jsp:param name="displayESign" value="Y"/>
							<jsp:param name="formID" value="irbcheckid"/>
							<jsp:param name="showDiscard" value="N"/>
			       </jsp:include>
		        <%
		            	   }
		               }// End of not blocked for submission
		           } // End of Lind mode%>
	        <%}%>
         <%}
        %>
	        </form>
        <%
        if (request.getParameter("autoPopulate") != null) {
        %>
        <script>
        callAjax();
        </script>
        <%                
        }
        %>

<%
		}// end of else of study null check
	}//end of if body for session
	else
	{
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>
	<div> 
    	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
<%if (!"LIND".equals(CFG.EIRB_MODE)){ %>
</div>
<%} %>
<div class ="mainMenu" id = "emenu" > 
  <jsp:include page="getmenu.jsp" flush="true"/> 
</div>
</body>

</html>

