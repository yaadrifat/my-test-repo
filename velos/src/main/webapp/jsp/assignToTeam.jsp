
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=11" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.*" %>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:useBean id="addressUserB" scope="page" class="com.velos.eres.web.address.AddressJB" />
<%
String tab1 = request.getParameter("selectedTab");
boolean isIrb = false; 
if (tab1 != null && tab1.startsWith("irb")) { isIrb = true; } 
if (isIrb) {
%>
<title><%=MC.M_ResCompNew_AppStd%><%--Research Compliance >> New Application >>  <%=LC.Std_Study%> Personnel*****--%></title>
<% } else { %>
<title><%=MC.M_StdTeam_AssnUsr%><%--<%=LC.Std_Study%> >> Team >> Assign Users*****--%></title>
<% } %>
<style>
.img-right img {
    float: right;
}
</style>
<SCRIPT Language="javascript">
	
// if (document.getElementById('eSign')) { document.getElementById('eSign').disabled=false; } 
// if (document.getElementById('submit_btn')) { document.getElementById('submit_btn').disabled=false; }
// function  blockSubmit() {
// 	 setTimeout('document.getElementById(\'eSign\').disabled=true', 10);
// 	 setTimeout('document.getElementById(\'submit_btn\').disabled=true', 10);
// }
/* function changeCount(row)
{
	  	
	  selrow = row ;
	  checkedrows = document.users.checkedrows.value;
	  totusers = document.users.totalrows.value;
	  rows = parseInt(checkedrows);	
	  usernum = parseInt(totusers);	
    if (usernum > 1)
	{
       if (document.users.assign[selrow].checked)
       { 
			rows = parseInt(rows) + 1;

		}

	   else
		{
			rows = parseInt(rows) - 1;

		}

	}else{

	  if (document.users.assign.checked)

		{ 

			rows = parseInt(rows) + 1;

		}

	  else

		{

			rows = parseInt(rows) - 1;

		}



	}	

	document.users.checkedrows.value = rows;

	} */



function validate(formobj)

{
     formobj=document.users;
     var len=$j('#slctdStdTeamTable tr').length-1;
     $j('#totalrows').val(len);
   
     //whether the user is selected or not
     if(len==0 || selUserIds.toString()==""){
    	 alert("<%=MC.M_PlsSel_TheUsr%>");
    	 return false;
     }

     //whether role has been selected if the user is selected
	  if (len == 1)
     {
            //if (!(validate_col('Role',formobj.role))) return false;
		  if(formobj.role.value=='' || formobj.role.value==null){
			  alert("<%=MC.M_Select_UserRole%>");
			  formobj.role.focus();
			  return false;
		  }
		  
     } else
     {
         for (ll_cnt = 0;ll_cnt < len;ll_cnt ++)  {	
			   //if (!(validate_col('Role',formobj.role[ll_cnt]))) return false;
			   if(formobj.role[ll_cnt].value=='' || formobj.role[ll_cnt].value==null){
				   alert("<%=MC.M_Select_UserRole%>");
				   formobj.role[ll_cnt].focus();
				   return false;
			   }
         }
     }
     	 
// 	if (!(validate_col('e-Signature',formobj.eSign))) return false
// 	if(isNaN(formobj.eSign.value) == true) {
<%-- 		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/ --%>
// 		formobj.eSign.focus();
// 		return false;
// 	}	 
	 
	 
}

var selUserIds=[];

function searchTeamMember()
{
	var fname=document.getElementById("fname").value;
	var lname=document.getElementById("lname").value;
	var org=document.getElementById("accsites").value;
	var grp=document.getElementById("accgroup").value;
	var jobtyp=document.getElementById("jobType").value;
	var rights=document.getElementById("right").value;
		jQuery.ajax({
			url: 'assignToStudyTeam.jsp?right='+rights+'&fname='+fname+'&lname='+lname+'&accsites='+org+'&accgroup='+grp+'&jobType='+jobtyp+'&selUserIds='+selUserIds.toString(),
			cache: false,
			type: "POST",
			async:false,
			success : function (data) 
			{
				jQuery("#assignToStudyTeam").empty();
     			jQuery("#assignToStudyTeam").html(data);
			}
		});		
}

function selectTeamMember(event,usrId)
{
	 var len=$j('#slctdStdTeamTable tr').length;
		var userArray='';
	    var addRow=false;
	    var slctdIds="assignUser_";
	    userArray=$j(event).attr("value").split('||');
	    	    
	    if(event.checked){
			if(jQuery.inArray(userArray[0],selUserIds)<0){
				selUserIds.push(userArray[0]);
				addRow=true;
			}
			
			if (addRow){
			$j('#slctdStdTeamTable tr:last').after('<tr ><td width="5%" style="display:none"><input type="hidden" id="'+userArray[0]+'_hidden" name="selectedUser" value="'+userArray[0]+'"/></td>'+
					'<td width="30%">'+userArray[1]+'</td><td width="25%">'+userArray[2]+'</td><td width="45%">'+userArray[3]+'</td><td>'+"<img src=\"./images/delete.gif\" style=\"cursor:pointer\" onClick=\"deleteUser(this.id,'"+userArray[0]+"')\" id=\"DelUser_"+userArray[0]+"\"  title=\""+LC.Remove+"\" border=\"0\"/>"+'</td></tr>');
				addRow=false;
			}
			$j("#slctdStudyTeam, #slctdStudyTeamLabel,#selectedMemberHeader, #submit_button ").css("display", "block");
			$j("#slctdStdTeamTable tr").removeClass("browserOddRow browserEvenRow");
	    	$j( "#slctdStdTeamTable tr:odd" ).addClass( "browserEvenRow" );
	    	$j( "#slctdStdTeamTable tr:even" ).addClass( "browserOddRow" );
	    }
	    else{
	    	var index = selUserIds.indexOf(userArray[0]);
	    	if (index > -1) {
	    		selUserIds.splice(index, 1);
	      	}
	    	deleteUser(userArray[0]+'_hidden',userArray[0]);
	    	$j("#slctdStdTeamTable tr").removeClass("browserOddRow browserEvenRow");
	    	$j( "#slctdStdTeamTable tr:odd" ).addClass( "browserEvenRow" );
	    	$j( "#slctdStdTeamTable tr:even" ).addClass( "browserOddRow" );
	    }
	}
	
function deleteUser(e,userId){
	 $j("#"+e).closest('tr').remove();
		$j('input:checkbox[id=assignUser_'+userId+']').attr('checked', false);
		var index = selUserIds.indexOf(userId);
		if (index > -1) {
			selUserIds.splice(index, 1);
	  	}
		$j("#slctdStdTeamTable tr").removeClass("browserOddRow browserEvenRow");
    	$j( "#slctdStdTeamTable tr:odd" ).addClass( "browserOddRow" );
    	$j( "#slctdStdTeamTable tr:even" ).addClass( "browserEvenRow" );
	}

</SCRIPT>



<% String src;
  src= request.getParameter("srcmenu");
  String from = "team";
%>


<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>

</head>
<body>
<jsp:useBean id="groupB" scope="session" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
<%
HttpSession tSession = request.getSession(true);
String study = "";
if (sessionmaint.isValidSession(tSession))

{
	study = (String) tSession.getValue("studyId");
}
%>
<DIV class="BrowserTopn" id = "div1"> 
<% String includeTabsJsp = isIrb ? "irbnewtabs.jsp" : "studytabs.jsp"; %>
  <jsp:include page="<%=includeTabsJsp%>" flush="true">
  <jsp:param name="from" value="<%=from%>"/> 
  <jsp:param name="studyId" value="<%=study %>"/>  
  </jsp:include>
    </div>
    <% if("Y".equals(CFG.Workflows_Enabled) && (study!=null && !study.equals("") && !study.equals("0"))){ %>

<SCRIPT LANGUAGE="JavaScript">
	var isIE = jQuery.browser.msie;
	var screenWidth = screen.width;
	var screenHeight = screen.height;
	if(screenWidth>1280 || screenHeight>1024)
	{
		if(isIE==true)
		{
			document.write('<DIV class="BrowserBotN BrowserBotN_S_1 workflowDivBig" id="div1" style="height:75%">')
		}
		else
		{
			document.write('<DIV class="BrowserBotN BrowserBotN_S_1 workflowDivBig" id="div1" style="height:75%">')
		}
	}
	else
	{
		document.write('<DIV class="BrowserBotN BrowserBotN_S_1 workflowDivBig" id="div1">')
	}
</SCRIPT>
<%}else{ %>

<SCRIPT LANGUAGE="JavaScript">
	var isIE = jQuery.browser.msie;
	var screenWidth = screen.width;
	var screenHeight = screen.height;
	if(screenWidth>1280 || screenHeight>1024)
	{
		if(isIE==true)
		{
			document.write('<DIV class="BrowserBotN BrowserBotN_S_1" id="div1" style="height:78%">')
		}
		else
		{
			document.write('<DIV class="BrowserBotN BrowserBotN_S_1" id="div1" style="height:80%">')
		}
	}
	else
	{
		document.write('<DIV class="BrowserBotN BrowserBotN_S_1" id="div1">')
	}
</SCRIPT>

<%} %>
  <%

    UserDao userDao=new UserDao(); 
    

   if (sessionmaint.isValidSession(tSession))

   {
	int studyId= EJBUtil.stringToNum(study);
	String ufname= request.getParameter("fname") ;
	String ulname=request.getParameter("lname") ;
	String tab= request.getParameter("selectedTab");

//	JM : 21April05	
	String orgName=request.getParameter("accsites"); 
	String group=request.getParameter("accgroup");
	String jobType=request.getParameter("jobType");	



	//out.print(ufname +" " +ulname);	

	//if (request.getParameter("fname")==null){ ufname="%" ;}

	//if (request.getParameter("lname") ==null){ ulname="%";	}



	Integer codeId;

	int count;

	String role;

	CodeDao cdRole = new CodeDao();   

	if(! ("LIND".equals(CFG.EIRB_MODE) )) { 
	  	  cdRole.getCodeValues("role");
	  } else {		  
		  String[] result = CFG.Study_Role_Filter_Subtypes.split(",");
		  String[] codeSubTypes= new String[result.length];		  
	      for (int x=0; x<result.length; x++) {
	         codeSubTypes[x]=result[x];		         
	      }	
	   cdRole.getCodeValuesFilteredBySubtypes("role", codeSubTypes);		  
	} 

	ArrayList cDesc= cdRole.getCDesc();

	ArrayList cId= cdRole.getCId();

	/* StringBuffer mainStr = new StringBuffer();

	mainStr.append("<SELECT NAME=role >") ;

	for (count = 0; count <= cDesc.size() -1 ; count++){

		codeId = (Integer) cId.get(count);

		mainStr.append("<OPTION value = "+ codeId+">" + cDesc.get(count)+ "</OPTION>");			

	}

	mainStr.append("<OPTION value =\"\" SELECTED> </OPTION>");

	mainStr.append("</SELECT>");

        role = mainStr.toString(); */
    
    role = cdRole.toPullDown("role");        

	String uName = (String) tSession.getValue("userName");

	int pageRight = 0;

	String accId = (String) tSession.getValue("accountId");

	pageRight = EJBUtil.stringToNum(request.getParameter("right")); 



	//GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		

	//pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));

   if (pageRight > 0 )

	 {
//	 modified for using search criteria like orgName,group,jobType JM : 21April05
	 userDao.getAvailableTeamUsers(studyId,EJBUtil.stringToNum(accId),ulname.trim(),ufname.trim(),orgName,group,jobType);        ArrayList usrLastNames;
          ArrayList usrFirstNames;
          ArrayList usrMidNames;
          ArrayList usrIds;       
          ArrayList usrSiteNames;	
          ArrayList usrStats;
	  ArrayList usrTypes;
	  ArrayList usrEmailId;
	  ArrayList usrLoginNames ;
	  ArrayList usrPerAdds;

         String usrLastName = null;
 	 String usrFirstName = null;
	 String usrMidName = null;
	 String usrId=null;		
	 String usrSiteName=null;
	 String completeUsrStr="";
	 String usrOverlibParameter="";
	 String usrLoginName="";
	 String fk_perId="";
	 String eMail="";
	 
	 //code added for  #S12 enhamcement by KN
	
	 String usrStat = null;
	 String usrType = null;
	 String dAccSites ="";
	String dAccGroups = "";
	String dJobType= "";

	 int counter = 0;
	 CodeDao cd = new CodeDao();
		cd.getCodeValues("job_type");
		if (jobType==null) jobType="";
		if (jobType.equals(""))			
		   dJobType=cd.toPullDown("jobType");
		else
		dJobType=cd.toPullDown("jobType",EJBUtil.stringToNum(jobType),true);
	 
	  cd = new CodeDao();
	  cd.getAccountSites(EJBUtil.stringToNum(accId));
	  
	 if (orgName==null) orgName="";
	 if (orgName.equals("")){
	 dAccSites=cd.toPullDown("accsites");
	 }
	 else{
		 
		 dAccSites=cd.toPullDown("accsites",EJBUtil.stringToNum(orgName),true);
		}
	 
	 cd = new CodeDao();
	 cd.getAccountGroups(EJBUtil.stringToNum(accId));
	 if (group==null) group="";
	 if (group.equals("")){
	 dAccGroups = cd.toPullDown("accgroup");
	 }
	 else{
	 dAccGroups = cd.toPullDown("accgroup",EJBUtil.stringToNum(group),true);
	 }
%>
<div id="searchUsrStudyTeam"  Style="width:98%;"> 
 <table class="tableDefault" id="searchUsrStdyTeamTable" width="100%" border=0 cellspacing="0" cellpadding="0">
	<tr height="20">
	<td colspan="6" bgcolor="#dcdcdc"> <b> <%=MC.M_AddUser_TeamSrch%><%--To add a new User to the Team, Search By*****--%></b></td>
	</tr>
	<tr>
	<td width="10%" bgcolor="#dcdcdc"> <%=LC.L_First_Name%><%--First Name*****--%>:</td>
	 <td width="20%" bgcolor="#dcdcdc"> <Input type=text id="fname" name="fname" value=<%=ufname%> >  </td>
     <td width="10%" bgcolor="#dcdcdc" align="right"> <%=LC.L_Last_Name%><%--Last Name*****--%>:&nbsp; </td>
     <td width="20%" bgcolor="#dcdcdc"> <Input type=text id="lname" name="lname" value=<%=ulname %>> </td>
	<td width="10%" bgcolor="#dcdcdc" align="right"> <%=LC.L_Organization%><%--Organization*****--%>: &nbsp; </td>
      <td width="28%" bgcolor="#dcdcdc">  <%=dAccSites%>   </td>
     </tr>
	<tr>
	<td  bgcolor="#dcdcdc"> <%=LC.L_Group%><%--Group*****--%>:</td>
	 <td  bgcolor="#dcdcdc">  <%=dAccGroups%>    </td>
	<td bgcolor="#dcdcdc" width="20%" align="right"> <%=LC.L_Job_Type%><%--Job Type*****--%>:&nbsp; </td>
     <td  bgcolor="#dcdcdc">   <%=dJobType%>  </td>
  	<td colspan="2" align="center" bgcolor="#dcdcdc"> 
			<% if (pageRight == 5 || pageRight == 7){%>
			  <button type="button" onClick="searchTeamMember();"><%=LC.L_Search%></button>
		  <%}%>
        </td>
      </tr> 
       </table>
</div>
    <table id="addNewUser" width="98%" cellspacing="0" cellpadding="0" border=0>
    <tr > 
        <td width = "100%"> 
          <P class = "defcomments">
          <% String Hyper_Link = "<A href=\"userdetails.jsp?mode=N&srcmenu=tdMenuBarItem3&selectedTab=2\">"+LC.L_Add_NewUser+"</a>";
          Object[] arguments = {Hyper_Link}; %><%=VelosResourceBundle.getMessageString("M_UnableToFindUsr_AddUsr",arguments) %>
           <%--If you are unable to find a user in the existing user list, you may <A href="userdetails.jsp?mode=N&srcmenu=tdMenuBarItem3&selectedTab=2">Add New User </a> here*****--%></P> 
           <br><br>
            
        </td>
      </tr>
      <tr > 
        <td > 
          <P class = "sectionHeadings"> <%=MC.M_Asgn_SlctdUsersToTeam%><%--Assign Users to Team*****--%> </P>
        </td>
      </tr>
<!--       <tr> -->
<!-- 		<td align="right"> -->
<%-- 			<button onclick="window.history.back(); return false;"><%=LC.L_Back%></button> --%>
<!-- 		</td>	 -->
<!-- 	  </tr> -->
    </table>
    </br>

<div id="assignToStudyTeam"  Style="width:98%; max-height:40%; overflow:auto;">
    <table id="slctdUsrsToTeamRows" width="100%" border="1px">  
    <tr id="headerRow"> 
      <th width="8%"><%=LC.L_Select%><%--Select*****--%> </th>
      <th width="15%"> <%=LC.L_First_Name%><%--First Name*****--%> </th>
	  <th width="15%"> <%=LC.L_Last_Name%><%--Last Name*****--%> </th>
	  <th width="20%"> <%=LC.L_Organization%><%--Organization*****--%> </th>
	  <th width="15%"> <%=LC.L_User_Type%><%--User Type*****--%> </th>
      </tr>
      <%

		usrLastNames = userDao.getUsrLastNames();
		
		usrFirstNames = userDao.getUsrFirstNames();

		usrMidNames = userDao.getUsrMidNames();
		usrLoginNames = userDao.getUsrLogNames();
		usrPerAdds  =userDao.getUsrPerAdds();

            	usrIds = userDao.getUsrIds();   
		
	    	usrSiteNames = userDao.getUsrSiteNames();
		
		//code added for  #S12 enhamcement by KN	
		
		usrStats=userDao.getUsrStats();
		
		usrTypes=userDao.getUsrTypes();
		
		int i;
		int lenUsers = usrLastNames.size();


	%>
      <tr id ="browserBoldRow"><%Object[] arguments1 = {lenUsers}; %> 
        <td colspan="6"> <%=MC.M_Tot_NumOfUsers%><%--Total Number of Users*****--%> : <%=VelosResourceBundle.getLabelString("L_Usr_S",arguments1)%><%-- <%= lenUsers%> User(s)*****--%> </td>
      </tr>
      
      <%

	       for(i = 0 ; i < lenUsers ; i++)

               {

		 usrLastName=((usrLastNames.get(i)) == null)?"-":(usrLastNames.get(i)).toString();
		 usrFirstName=((usrFirstNames.get(i))==null)?"-":(usrFirstNames.get(i)).toString();
		 usrMidName=((usrMidNames.get(i))==null)?"-":(usrMidNames.get(i)).toString();
		 
		 usrId = ((Integer)usrIds.get(i)).toString();
		 fk_perId=((usrPerAdds.get(i))==null)?"":(usrPerAdds.get(i)).toString();
		 
		 if(!fk_perId.equals("")){
		 	addressUserB.setAddId(Integer.parseInt(fk_perId));
			addressUserB.getAddressDetails();
			eMail=addressUserB.getAddEmail();
		 }
		 
		if(usrLoginNames.size()>0){
			usrLoginName = ((usrLoginNames.get(i))==null)?"-":(usrLoginNames.get(i)).toString();
		}
		 
		if(usrMidName.equalsIgnoreCase("-")){
			usrMidName = "";
		}
		 completeUsrStr=usrFirstName+" "+usrMidName+" "+usrLastName+" "+eMail+" "+usrLoginName; 
		
			if(completeUsrStr.length()>500 && completeUsrStr.length()<=1000) 
				usrOverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,70,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
			else 
				usrOverlibParameter=",ABOVE,";
// 		JM : 21April05		
		 usrSiteName=((usrSiteNames.get(i)) == null)?"-":(usrSiteNames.get(i)).toString();
		 
		 
		 //code added for  #S12 enhamcement by KN
		 
		 usrStat=((usrStats.get(i)) == null)?"-":(usrStats.get(i)).toString();
		 usrType=((usrTypes.get(i)) == null)?"-":(usrTypes.get(i)).toString();
		 
		 if (usrStat.equals("A")){
		     usrStat=LC.L_Active_AccUser/*"Active Account User"*****/;
		 }
		 else if (usrStat.equals("B")){
		     usrStat=LC.L_Blocked_User/*"Blocked User"*****/;
		 }
		 else if (usrStat.equals("D")){
		     usrStat=LC.L_Deactivated_User/*"Deactivated User"*****/;
		 }
		 
		 if (usrType.equals("N")){  //KM-03Apr08
			 if (usrStat.equals(LC.L_Deactivated_User/*"Deactivated User"*****/))
		         usrStat=MC.M_DeactNon_SysUsr/*"Deactivated Non System User"*****/;
			 else
		         usrStat=MC.M_Non_SystemUser/*"Non System User"*****/;  
		 }
		 
		 
		if ((i%2)==0) {

  %>
      <tr class="browserEvenRow"> 
        <%

		}

		else{

  %>
      <tr class="browserOddRow"> 
        <%

		}

  %>
  		<td width =50> 
          <Input type="checkbox" id="assignUser_<%=usrId%>" name="assign" value = "<%=usrId%>||<%=usrFirstName%>||<%=usrLastName%>||<%=role%>" onclick="selectTeamMember(this,'<%=usrId %>')"  >
        </td>
        <td width =250>  <%=usrFirstName%>  
          <input type="hidden" name="userId" value="<%=usrId%>">
        </td>		
		<td width =250 class="img-right">  <%=usrLastName%> &nbsp;&nbsp;&nbsp; <img src="./images/View.gif" onmouseover="return overlib(htmlEncode('<%=completeUsrStr.replace("'", "\\'")%>')<%=usrOverlibParameter %>CAPTION,'');" onmouseout="return nd();" border="0" align="left" />
        </td>	
		<td width =250>  <%=usrSiteName%>            
        </td>
		<td width =250>  <%=usrStat%>            
        </td>
      </tr>
      <%

		}

%>
    
    </table>
 </div> 
 <Form name="users" id="userstoteam" method="post" action="updatestudyteam.jsp" onSubmit = "if (validate(document.users)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
 <Input type="hidden" name="checkedrows" value=0>
 <Input type="hidden" name= "src" value= <%=src%> >
 <Input type="hidden" name= "studyId" value= <%=studyId%> >
 <Input type="hidden" name="selectedTab" value="<%=tab%>">
 <Input type="hidden" id="right" name="right" value="<%=pageRight%>">
 <Input type="hidden" id="totalrows" name="totalrows" value=""  >
 </br>
 </br>
 <div id=slctdStudyTeamLabel Style="display:none; cellspacing:0; cellpadding:0; border:0">
 <table id="slctdStudyTeamTable"  width="98%" >
      <tr> 
        <td> 
          <P class = "sectionHeadings"> <%=MC.M_SlctdUsers_AddToTeam%><%--Assign Users to Team*****--%> </P>
        </td>
      </tr>
    </table>
    </br>
    </div>
 <div id="slctdStudyTeam"  Style="width:98%; max-height:40%; display:none;overflow:auto;" >
  
 <div id="selectedMemberHeader">
 <table width="100%" cellspacing="0" cellpadding="0" border="1px" id="slctdStdTeamTable" class="outline">
  <tr> 
  	<th width="5%"  style="display:none"></th>
    <th width="30%"> <%=LC.L_First_Name%><%--First Name*****--%></th>
    <th width="25%"> <%=LC.L_Last_Name%><%--First Name*****--%></th>
	<th width="45%"><%=LC.L_Role%><%--Role*****--%> </th>
	<th width="5%"><%=LC.L_Remove%><%--Remove*****--%>  </th>
 </tr>
  </table>
  </div>
  </br>

    </div>
  <div id="submit_button" style="margin-left: 40%;display:none;">
   <% 
		String displayESign="N";
	
		if (pageRight > 4){ 
		  String showSubmit = "";
// 		  if  (!(lenUsers > 0)) {
// 		     showSubmit = "N";
// 		     displayESign = "N";
// 	      }
	%>
	
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="<%=displayESign %>"/>
			<jsp:param name="formID" value="userstoteam"/>
			<jsp:param name="showDiscard" value="N"/>
			<jsp:param name="showSubmit" value="<%=showSubmit%>"/>
	</jsp:include>
	
	<%}%>
  </div>
  </Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>
