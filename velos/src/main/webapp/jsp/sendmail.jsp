<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page
  import=" javax.mail.*, javax.mail.internet.*, javax.activation.*,java.util.*,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"
  %>
<html>
<head>
<TITLE><%=MC.M_JspMeets_Javamail%><%--JSP meets JavaMail, what a sweet combo.*****--%></TITLE>
</HEAD>
<BODY>
<%

 try{
   Properties props = new Properties();
   Session sendMailSession;
   Store store;
   Transport transport;

  
  sendMailSession = Session.getInstance(props, null);

  props.put("mail.smtp.host", "intranet.velosmed.com");

  Message newMessage = new MimeMessage(sendMailSession);
  newMessage.setFrom(new InternetAddress("soniasahni@hotmail.com"));
  newMessage.setRecipient(Message.RecipientType.TO, new InternetAddress("ssahni@health-outcomes.com"));
  newMessage.setSubject(LC.L_Subject_Lower/*"subject"*****/);
  newMessage.setSentDate(new Date());
  newMessage.setText(MC.M_AmSending_Mail/*"i am sending a mail"*****/);

  transport = sendMailSession.getTransport("smtp");
  transport.send(newMessage);
   %>
<P><%=MC.M_YourMail_Sent%><%--Your mail has been sent.*****--%></P>
<%
  }
 catch(MessagingException m)
  {
  out.println(m.toString());
  }
%>
</BODY>
</HTML>

