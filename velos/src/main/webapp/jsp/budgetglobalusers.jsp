<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id="addressUserB" scope="page" class="com.velos.eres.web.address.AddressJB" />

<title>
<%String userFlag = request.getParameter("userFlag");
if (userFlag.equals("S")) {%>
    	<%=LC.L_Std_Team%><%--Study Team*****--%>
	<%} else if (userFlag.equals("O")) {%>
	   <%=LC.L_Users_InOrg%><%--Users in Organization*****--%>
	<%} else if (userFlag.equals("A")) {%>
	   <%=LC.L_Users_InAcc%><%--Users in Account*****--%>
	<%}%>
</title>
</head>
<%-- Nicholas : Start --%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%--<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>--%>
<%-- Nicholas : End --%>
<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1) 
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {	
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<jsp:useBean id="teamB" scope="session" class="com.velos.eres.web.team.TeamJB"/>
<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.team.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
<BR>
<DIV class="tableDefault" id="div1"> 
  <%
   HttpSession tSession = request.getSession(true); 

   if (sessionmaint.isValidSession(tSession))
	{ %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<% 	
	String acc = (String) tSession.getValue("accountId");
	String uId = (String) tSession.getValue("userId");
	String study = request.getParameter("studyId");
	String site = request.getParameter("siteId");
	//String userFlag = request.getParameter("userFlag");
	int studyId = EJBUtil.stringToNum(study);
	int accId = EJBUtil.stringToNum(acc);
	int siteId = EJBUtil.stringToNum(site);

   ArrayList userLastNames 	= null;
   ArrayList userFirstNames = null;   
   ArrayList userIds = null;
   ArrayList fk_PerAdd=null;
   ArrayList usrLogNames=null;
   ArrayList userMidNames=null;
   String userLastName = "";
   String userFirstName = "";
   String userMidName="";
   String fk_add="";
   String usrLogName="";
   String  userEmail = "";
   int counter = 0;
   int len = 0;
   int userId = 0;	
		
	if (userFlag.equals("S")) {
	   TeamDao teamDao = new TeamDao();
	   teamDao.getTeamRights(EJBUtil.stringToNum(study),EJBUtil.stringToNum(uId));
	   ArrayList tId = teamDao.getTeamIds();
	   teamDao = teamB.getTeamValues(studyId, accId);
	   ArrayList teamIds = teamDao.getTeamIds(); 
	   userLastNames = teamDao.getTeamUserLastNames();
	   userFirstNames = teamDao.getTeamUserFirstNames();
	   userIds = teamDao.getUserIds();
	   len = teamIds.size();
	}
		
	if (userFlag.equals("A")) {
	   UserDao userDao = new UserDao();
	   userDao.getAccountUsersDetails(accId);
	   userIds = userDao.getUsrIds();
	   userLastNames = userDao.getUsrLastNames();
	   userFirstNames = userDao.getUsrFirstNames();	
	   userMidNames=userDao.getUsrMidNames();
	   fk_PerAdd=userDao.getUsrPerAdds();
	   usrLogNames=userDao.getUsrLogNames();
	   len = userIds.size();
	}	

	if (userFlag.equals("O")) {
	   UserDao userDao = new UserDao();
	   userDao.getSiteUsers(siteId);
	   userIds = userDao.getUsrIds();	   
	   userLastNames = userDao.getUsrLastNames();
	   userFirstNames = userDao.getUsrFirstNames();	
	   userMidNames=userDao.getUsrMidNames();
	   fk_PerAdd=userDao.getUsrPerAdds();
	   usrLogNames=userDao.getUsrLogNames();
	   len = userIds.size();
	}		
%>
  <Form name="teambrowser" method="post" action="" onsubmit="">
	<%  if (userFlag.equals("S")) {%>
    	<P class = "defComments"> <%=MC.M_FlwUsrs_StdTeam%><%--The following are the users in Study Team.*****--%></P>
	<%} else if (userFlag.equals("O")) {%>
	   	<P class = "defComments"> <%=MC.M_Flw_UsrInOrg%><%--The following are the users in Organization.*****--%></P>
	<%} else if (userFlag.equals("A")) {%>
	   	<P class = "defComments"> <%=MC.M_Flw_UsrInAcc%><%--The following are the users in Account.***** --%></P>
	<%}%>
<%-- Nicholas : Start --%>	
	  <table class="comPopup" width=100% >			 
<%-- Nicholas : End --%>
		      <tr> 
		        <th > <%=LC.L_User_Name%><%--User Name*****--%> </th>
		      </tr>
    <%
    for(counter = 0;counter<len;counter++)
	{
		userLastName=((userLastNames.get(counter)) == null)?"-":(userLastNames.get(counter)).toString();
		userFirstName=((userFirstNames.get(counter)) == null)?"-":(userFirstNames.get(counter)).toString();
		userId = ((Integer)userIds.get(counter)).intValue();
		if (userFlag.equals("S")) {
		if(userId!=0){
			userB.setUserId(userId);
			userB.getUserDetails();
			
			userMidName=userB.getUserMidName();
			if(userMidName==null){userMidName="";};
			fk_add=userB.getUserPerAddressId();
			if(fk_add==null){fk_add="";}
			usrLogName=userB.getUserLoginName();
			if(usrLogName==null){usrLogName="";}
		}	
			
		}else{
		userMidName=((userMidNames.get(counter)) == null)?" ":(userMidNames.get(counter)).toString();
		fk_add=((fk_PerAdd.get(counter)) == null)?" ":(fk_PerAdd.get(counter)).toString();
		usrLogName=((usrLogNames.get(counter)) == null)?" ":(usrLogNames.get(counter)).toString();
		}
		
		
		if(!fk_add.equals("")){
			addressUserB.setAddId(EJBUtil.stringToNum(fk_add));
			addressUserB.getAddressDetails();
			userEmail = addressUserB.getAddEmail();
			userEmail = ( userEmail == null )?"":(userEmail);//JM
		}
%>
			  
      <%
			if ((counter%2)==0) {
			%>
			    <tr class="browserEvenRow"> 
	        <%
			}
			else{
			%>
	      		<tr class="browserOddRow"> 
	        <%
			}
			%>
	        <td width=100%> 
	       <%= userFirstName%>&nbsp;<%= userLastName%><a href="#"><img src="./images/View.gif" border="0" align="left" style="float: right;" onMouseOver="return overlib('<%=StringUtil.htmlEncodeXss(userFirstName)%>&nbsp;<%=StringUtil.htmlEncodeXss(userMidName)%>&nbsp;<%=StringUtil.htmlEncodeXss(userLastName)%>&nbsp;<%=StringUtil.htmlEncodeXss(userEmail)%>&nbsp;<%=StringUtil.htmlEncodeXss(usrLogName)%>',ABOVE);" onMouseOut="return nd();" > </a> </td>
	             
	      </tr>
	      

	    <%

} // end of for loop
%>
<tr>
<td align=center>
<button onClick = "self.close();"><%=LC.L_Close%></button> 
</td>
</tr>

</table>
  </Form>



<%
}//end of if body for session


else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}
%>
</body>

</html>
<script>
jQuery('html, body').attr('style','overflow:auto; height:auto;');
</script>
