<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.*"%>

<%@page import="com.velos.eres.business.common.SiteDao"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.velos.eres.business.common.CodeDao"%>
<%@page import="com.velos.eres.business.address.impl.AddressBean"%>
<%@page import="com.velos.eres.business.user.impl.UserBean"%>
<%@page import="com.velos.eres.service.addressAgent.AddressAgentRObj"%>
<%@page import="com.velos.eres.service.userAgent.UserAgentRObj"%>
<%@page import="com.velos.eres.business.common.UserDao"%><jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="accwrapB" scope="request" class="com.velos.eres.web.accountWrapper.AccountWrapperJB"/>

<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html");
	HttpSession tSession = request.getSession(false);
	//JSONObject jsObj = new JSONObject();
	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		//jsObj.put("result", -1);
		//jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
   	%>
		<input type="hidden" name="resultMsg" id="resultMsg" value="<%=MC.M_UsrNot_LoggedIn %>"/>    	
	<%
		//return;
	}
	
	String accountId = null;
	String SiteData = null;
	String userData = null;
	String phase = null;
	String studyType =null;
	String sponsorName = null;
	CodeDao cd1 = new CodeDao();
	int codePk = cd1.getCodeId("site_type","siteTypHospital");
    
	boolean flag=true;	
	int siteId = 0;
	int userId = 0;
	String codeSType = "";
	SiteData = request.getParameter("orgData");
	userData = request.getParameter("usrData");
	phase = request.getParameter("phase");
	studyType = request.getParameter("studyType");
	sponsorName = request.getParameter("sponsorName");
	JSONObject dataRecord = null;
	String reqType = request.getParameter("reqType");
	
	accountId = (String) tSession.getValue("accountId");
	String ipAdd = (String) tSession.getValue("ipAdd");
	 String usr = null;
	 usr = (String) tSession.getValue("userId");
    try {
    	if("orgusr".equals(reqType)){
    	dataRecord = new JSONObject(SiteData);
    	System.out.println(dataRecord.toString()+accountId);
    	SiteDao siteD = new SiteDao();
    	String siteName = dataRecord.getString("name");
    	siteName = (siteName.length()>50)?siteName.substring(0,50):siteName;
    	siteId = siteD.getSitePKBySiteName(siteName, StringUtil.stringToNum(accountId));
    	dataRecord = new JSONObject(dataRecord.getString("address"));
    	if(siteId<=0){
    		accwrapB.setUserAccountId(accountId);

    		  accwrapB.setSiteCodelstType(StringUtil.integerToString(codePk));
				
    		   accwrapB.setSiteName(siteName);
    		   accwrapB.setAddCitySite(dataRecord.getString("city"));

    		   accwrapB.setAddStateSite(dataRecord.getString("state"));

    		   accwrapB.setAddZipSite(dataRecord.getString("zip"));

    		   accwrapB.setAddCountrySite(dataRecord.getString("country"));
    		   
    		   accwrapB.setSiteHidden("0");//KM 
    		   accwrapB.setIpAdd(ipAdd);
    		   accwrapB.setCreator(usr);
    		   siteId = accwrapB.createSite();

    	}
    	
    	dataRecord = new JSONObject(userData);
    	System.out.println(dataRecord.toString()+accountId);
    	siteName = (siteName.length()>50)?siteName.substring(0,50):siteName;
    	siteId = siteD.getSitePKBySiteName(siteName, StringUtil.stringToNum(accountId));
    	String userName[] = dataRecord.getString("last_name").toString().split(" ");
		int len = userName.length;
    	UserDao userD = new UserDao();
    	userId = userD.getUserPK(userName[0],userName[len-1],dataRecord.getString("email"));
    	if(userId<=0){
    	AddressBean address = new AddressBean();
		address.setCreator(usr);
		address.setIpAdd(ipAdd);
		address.setAddEmail(dataRecord.getString("email"));
		address.setAddPhone(dataRecord.getString("phone"));
		AddressAgentRObj addressAgent = EJBUtil.getAddressAgentHome();
		int addressPK = addressAgent.setAddressDetails(address);
		UserAgentRObj userAgent =  EJBUtil.getUserAgentHome();
		UserBean newNonSystemUser = new UserBean();
		newNonSystemUser.setUserFirstName(userName[0]);
		newNonSystemUser.setUserLastName(userName[len-1]);
		newNonSystemUser.setUserType("N");
		newNonSystemUser.setUserSiteId(StringUtil.integerToString(siteId));
		newNonSystemUser.setUserAccountId(accountId);
		//added following line for 5728 DRM 1/14/11
		newNonSystemUser.setUserStatus("A");
		newNonSystemUser.setCreator(usr);
		newNonSystemUser.setIpAdd(ipAdd);
		newNonSystemUser.setUserHidden("0");
		newNonSystemUser.setUserPerAddressId(EJBUtil.integerToString(addressPK));
		int returnId = userAgent.setUserDetails(newNonSystemUser);
		
    	}

			
		%>
				<input type="hidden" name="resultMsg" id="resultMsg" value="<%=flag %>"/>    	
				<input type="hidden" name="siteId" id="siteId" value="<%=siteId%>"/>
				<input type="hidden" name="userId" id="userId" value="<%=userId%>"/>
		<%			
    	}else{
    		
    		CodeDao cdao = new CodeDao();
    		codeSType="";
    		if(phase!=null){
    			codeSType = cdao.getCodelstSubType(phase,"phase",true);
				if(codeSType.equals("")){
					phase = phase.replaceAll("1","I").replaceAll("2","II").replaceAll("3","III").replaceAll("4","IV");
					codeSType = cdao.getCodelstSubType(phase,"phase",true);
				}
    				%>
    				<input type="hidden" name="phaseSType" id="phaseSType" value="<%=codeSType%>"/>
    				<%
    			
    		}else{
    			%>
				<input type="hidden" name="phaseSType" id="phaseSType" value="<%=codeSType%>"/>
				<%
    		}
    		cdao = new CodeDao();
    		codeSType="";
    		if(studyType!=null){
    			codeSType = cdao.getCodelstSubType(studyType,"study_type",false);
    			%>
				<input type="hidden" name="studyTypeSType" id="studyTypeSType" value="<%=codeSType%>"/>
				<%
    			
    		}else{
    			%>
				<input type="hidden" name="studyTypeSType" id="studyTypeSType" value="<%=codeSType%>"/>
				<%
    		}
    		cdao = new CodeDao();
    		codeSType="";
    		if(sponsorName!=null){
    			codeSType = cdao.getCodelstSubType(sponsorName,"sponsor",false);
    			%>
				<input type="hidden" name="sponsorNameSType" id="sponsorNameSType" value="<%=codeSType%>"/>
				<%
    			
    		}else{
    			%>
				<input type="hidden" name="sponsorNameSType" id="sponsorNameSType" value="<%=codeSType%>"/>
				<%
    		}
    		
    		%>
			<input type="hidden" name="resultMsg" id="resultMsg" value="<%=flag %>"/>    	
			
	<%	
    		
    	}
    		
	} catch(Exception e) {
    	%>
    		<input type="hidden" name="resultMsg" id="resultMsg" value="<%=flag %>"/>    	
    	<%
		//return ;
	}
%>
	