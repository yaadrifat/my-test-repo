<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import = "com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*,com.velos.eres.business.common.CtrlDao,java.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.business.common.TeamDao"%>
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="studymod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="studyTeam" scope="request" class="com.velos.eres.web.team.TeamJB"/>

<%!
  private String getHrefBySubtype(String subtype, String mode, String studyId) {
      if (subtype == null) { return "#"; }
      if ("new_menu".equals(subtype)) {
          return "irbnewinit.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=N";
      }
      if ("irb_init_tab".equals(subtype)) {
          if ("M".equals(mode)) {
              return "irbnewinit.jsp?mode="+mode+"&srcmenu=tdmenubaritem3&selectedTab=irb_init_tab&studyId="+studyId;
          }
          return "irbnewinit.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=N";
      }
      if ("irb_personnel_tab".equals(subtype)) {
          if ("M".equals(mode)) {
              return "teamBrowser.jsp?mode="+mode+"&srcmenu=tdmenubaritem3&selectedTab=irb_personnel_tab&studyId="+studyId;
          }
          return "teamBrowser.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_personnel_tab&mode=N";
      }
      if ("irb_form_tab".equals(subtype)) {
          if ("M".equals(mode)) {
              return "formfilledstudybrowser.jsp?mode="+mode+"&srcmenu=tdmenubaritem3&selectedTab=irb_form_tab&studyId="+studyId;
          }
          return "formfilledstudybrowser.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_form_tab&mode=N";
      }
      if ("irb_upload_tab".equals(subtype)) {
          if ("M".equals(mode)) {
              return "studyVerBrowser.jsp?mode="+mode+"&srcmenu=tdmenubaritem3&selectedTab=irb_upload_tab&studyId="+studyId;
          }
          return "studyVerBrowser.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_upload_tab&mode=N";
      }
      if ("irb_check_tab".equals(subtype)) {
          if ("M".equals(mode)) {
              return "irbnewcheck.jsp?mode="+mode+"&srcmenu=tdmenubaritem3&selectedTab=irb_check_tab&studyId="+studyId;
          }
          return "irbnewcheck.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_check_tab&mode=N";
      }
      if ("irb_rpt_tab".equals(subtype)) {
          if ("M".equals(mode)) {
              return "reportsinstudy.jsp?mode="+mode+"&srcmenu=tdmenubaritem3&selectedTab=irb_rpt_tab&studyId="+studyId;
          }
          return "reportsinstudy.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_rpt_tab&mode=N";
      }
      return "#";
  }
%>

<%
String mode="N";
String selclass;
String studyId="";
String study="";
String studyNo="";
String verNumber = ""  ;
String userId = "";
String studyFromSession = "";
String userIdFromSession = "";
String accId = "";

String tab= request.getParameter("selectedTab");
String from= request.getParameter("from");
mode= request.getParameter("mode");
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
{
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	int grpRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));

	study = (String) tSession.getValue("studyId");

	studyFromSession = (String) tSession.getValue("studyId");
	userIdFromSession= (String) tSession.getValue("userId");
	accId = (String) tSession.getValue("accountId");

	studyId = request.getParameter("studyId");

	StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");

	studyNo = (String) tSession.getValue("studyNo");
	verNumber = request.getParameter("verNumber");
	userId = (String) tSession.getValue("userId");

	if(StringUtil.isEmpty(studyId) || (studyId.trim()).equals("null")){
		studyId = studyFromSession;
	}


	if (EJBUtil.stringToNum(studyId) > 0) mode="M";

if (mode == null){
 	mode = "N";
}else if (mode.equals("M")){

	//System.out.println("in tabs" + studyId +"*");

	if (StringUtil.isEmpty(studyFromSession))
	{
	 studyFromSession = "";

	}
	if (StringUtil.isEmpty(studyId))
	{
	 studyId = "";

	}

	if (!studyId.equals(studyFromSession) || stdRights==null)
	{
	    //use the one from parameter and set it in session


	    tSession.setAttribute("studyId",studyId);
		studyB.setId(EJBUtil.stringToNum(studyId));
		studyB.getStudyDetails();
		String studyNumber = studyB.getStudyNumber();
		tSession.setAttribute("studyNo",studyNumber);
		studyNo = studyNumber;

		TeamDao teamDao = new TeamDao();
		teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));

		ArrayList tId ;
		tId = new ArrayList();
		tId = teamDao.getTeamIds();

		stdRights = new StudyRightsJB();

		if (tId.size() <=0)
		{

			tSession.putValue("studyRights",stdRights);
		} else {

			stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));

				ArrayList teamRights ;
				teamRights  = new ArrayList();
				teamRights = teamDao.getTeamRights();

				stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
				stdRights.loadStudyRights();



			tSession.putValue("studyRights",stdRights);
		}


	}


}
if (EJBUtil.stringToNum(studyId) == 0)
{
	studyId = "";
}

out.print("<Input type=\"hidden\" name=\"mode\" value=\"" +mode+"\">");
String uName = (String) tSession.getValue("userName");
%>
<!-- <P class = "userName"><%= uName %></p> -->
<%
if(from.equals("sectionver")){
%>
<%
}else if (from.equals("appendixver")) {
%>
<%
}else if (from.equals("version")) {
%>

<% } else if (from.equals("admin")) { %>

<%
}else if (from.equals("status")) {
%>

<%
}else if (from.equals("calendar")) {
%>
<%
}

else if (from.equals("calenderstatus")) {
%>
<%
}
else if(from.equals("calendarhistory")) {
%>
<%
}
else if (from.equals("report")) {
%>
<%
}else if (from.equals("team")) {
%>
<%
}else if (from.equals("notify")) {
%>
<%
}else if (from.equals("form")) {
%>
<%
}else if (from.equals("studynot")) {
%>
<%
}else if (from.equals("verHistory")) {
%>
<%
}
else if (from.equals("studyTeamHistory")) { //Added by Manimaran for Enhancement S4.
%>
<%
}
else {
%>
<%
}

ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
ArrayList tabList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "irb_new_tab");

%>
<DIV>
<!-- <table  class= "selectedTab" cellspacing="0" cellpadding="0" border="0">  -->
<table   cellspacing="0" cellpadding="0" border="0">
	<tr>
    <%
    // check the rights
 	// To check for the account level rights
	String modRight = (String) tSession.getValue("modRight");
	 int patProfileSeq = 0, formLibSeq = 0;
	 acmod.getControlValues("module");
	 ArrayList acmodfeature =  acmod.getCValue();
	 ArrayList acmodftrSeq = acmod.getCSeq();
	 char formLibAppRight = '0';

	 patProfileSeq = acmodfeature.indexOf("MODPATPROF");
	 formLibSeq = acmodfeature.indexOf("MODFORMS");
	 formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
	 formLibAppRight = modRight.charAt(formLibSeq - 1);
	 int pageRight = 0;

	 for (int iX=0; iX<tabList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings)tabList.get(iX);
	    // If this tab is configured as invisible in DB, skip it
	    if ("0".equals(settings.getObjVisible())) {
	        continue;
	    }
	    // Figure out the access rights
        boolean showThisTab = false;
		if ("form_tab".equals(settings.getObjSubType())) {
		    if (String.valueOf(formLibAppRight).compareTo("1") == 0) {
		        showThisTab = true;
		    }
		} else if("irb_init_tab".equals(settings.getObjSubType())) {
			if (mode.equals("M")) {
				if ((stdRights.getFtrRights().size()) == 0){
					pageRight= 0;
				}else{
					pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSUM"));
				}
			}
			else
			{
	   			pageRight = grpRight;
			}

			if (((mode.equals("M") && pageRight >=4) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 ))) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 ))))
			{ showThisTab = true; }
		} else if ("irb_personnel_tab".equals(settings.getObjSubType())) {
			if (mode.equals("N"))
			{
					pageRight= 7;
			}
			else
			{

				if ((stdRights.getFtrRights().size()) == 0){
					  pageRight= 0;
				 }else{
					  pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYTEAM"));
				 }
			}
			if ((pageRight > 0) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 )))) {
				showThisTab = true;
			}
	    } else if ("irb_upload_tab".equals(settings.getObjSubType())) {
			if (mode.equals("N"))
			{
					pageRight= 7;
			}
			else
			{
				if ((stdRights.getFtrRights().size()) == 0){
			 		pageRight= 0;
				}else{
					pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYVER"));
		   		}
			}
			if ((pageRight > 0) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 )))) {
			   showThisTab = true;
			}
		} else if ("irb_rpt_tab".equals(settings.getObjSubType())) {
			if (mode.equals("N"))
			{
					pageRight= 7;
			}
			else
			{
				if ((stdRights.getFtrRights().size()) == 0){
			 		pageRight= 0;
				   }else{
					pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYREP"));
		   		}
		   	}
			if ((pageRight >= 4) && ((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 )))) {
				showThisTab = true;
			}
	    } else if ("irb_form_tab".equals(settings.getObjSubType())) {//KM-21Jul09
			int study_acc_form_right = 0;
			int study_team_form_access_right = 0;
			if ((String.valueOf(formLibAppRight).compareTo("1") == 0)) {
				if (mode.equals("N"))
				{
						showThisTab = true;
				}
				else
				{
					study_acc_form_right = Integer.parseInt(grpRights.getFtrRightsByValue("STUDYFRMSACC"));
					study_team_form_access_right=Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMACC"));
					if (((mode.equals("M") && grpRight >=4) || (mode.equals("N") && (grpRight == 5 || grpRight == 7 )))){
				    	if (study_acc_form_right >=4  || study_team_form_access_right>0) {
							showThisTab = true;
						}
					}
				}
			}
	    } else {
		    showThisTab = true;
		}
		if(!"LIND".equals(CFG.EIRB_MODE)) {
			if ("irb_approv_tab".equals(settings.getObjSubType())) {
				showThisTab = false;
			}
		}
	    if (!showThisTab) { continue; } // no access right; skip this tab
	    // Figure out the selected tab position
        if (tab == null) {
            selclass = "unselectedTab";
	    } else if (tab.equals(settings.getObjSubType())) {
            selclass = "selectedTab";
        } else {
            selclass = "unselectedTab";
        }
    %>
		<td  valign="TOP">
			<table class="<%=selclass%>"  cellspacing="0" cellpadding="0" border="0">
				<tr>
			<!--  	<td class="<%=selclass%>" rowspan="3" valign="top" >
						<img src="../images/leftgreytab.gif" height=20 border=0 alt="">
					</td> -->
					<td class="<%=selclass%>">
						<a href="<%=getHrefBySubtype(settings.getObjSubType(),mode, studyId)%>"><%=settings.getObjDispTxt()%></a>
					</td>
				<!--     <td class="<%=selclass%>" rowspan="3" valign="top">
						<img src="../images/rightgreytab.gif"  height=20 border=0 alt="">
			        </td> -->
			  	</tr>
		   	</table>
        </td>
    <%
     } // End of tabList loop
    %>

   	</tr>
 <!--   <tr>
     <td colspan=10 height=10></td>
  </tr>  -->
</table>
<table class="tabBorder" width="99%" height="5"><tr><td></td></tr></table>
	<%
	if (!(tab.equals("1")) && mode.equals("M")) {%>
	<table cellspacing="2" cellpadding="0" border="0">
 		<tr>
		<td><P class="defComments"><b><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>: </b><%=studyNo%></td>

		<% //JM: 11/18/2005: Display Study version number in version history page
		   //km-to fix Bug2442
		if ((tab.equals("2")) && mode.equals("M") && from.equals("verHistory") ) {%>
				<td><P class="defComments">&nbsp;&nbsp;<b><%=LC.L_Version_Number%><%--Version Number*****--%>: </b><%=verNumber%></td>
		<%}%>
<td>
<%
//Added by IA Bug # 2906 make visible the 'p' image when the study is active
//CodeDao cd1=new CodeDao();

String  aStudyActBeginDate = "";
String mPatRight = "0";
String aStudyTeamRight = "";

studymod.getControlValues("study_rights","STUDYMPAT");
ArrayList aRightSeq = studymod.getCSeq();
String rightSeq = aRightSeq.get(0).toString();
int iRightSeq = EJBUtil.stringToNum(rightSeq);


studyB.setId(EJBUtil.stringToNum(studyId));
studyB.getStudyDetails();

//activeCodeId = cd1.getCodeId("studystat", "active");
aStudyActBeginDate = studyB.getStudyActBeginDate();
aStudyActBeginDate = ((aStudyActBeginDate) == null || aStudyActBeginDate.equals(""))?"-":(aStudyActBeginDate);
TeamDao teamdao = studyTeam.getTeamRights(EJBUtil.stringToNum(studyId), EJBUtil.stringToNum(userId));
ArrayList aStudyTeamRights  = teamdao.getTeamRights();

if (aStudyTeamRights!= null && aStudyTeamRights.size() >0 )
{
	aStudyTeamRight = ((aStudyTeamRights.get(0)) == null)?"-":(aStudyTeamRights.get(0)).toString();

	if (aStudyTeamRight.length() >= 11 )
	{
		mPatRight = aStudyTeamRight.substring(iRightSeq - 1, iRightSeq);
	}
	else
	{
		mPatRight = "0";
	}
}

if (!aStudyActBeginDate.equals("-") && EJBUtil.stringToNum(mPatRight) > 0){

%>
&nbsp;&nbsp;&nbsp;&nbsp;<a href="studypatients.jsp?srcmenu=tdmenubaritem5&studyId=<%=studyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" title="<%=LC.L_PatMgmt_EnrlPat%><%--<%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patients%>*****--%>" border=0 ></a>
<%} %>
</td>
		</tr>
		</table>
	<%
	}

} //session time out.
%>

</DIV>
