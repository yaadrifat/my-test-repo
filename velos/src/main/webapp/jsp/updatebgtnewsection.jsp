<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/><%@page import="com.velos.eres.service.util.MC"%>
<HTML>  
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id="bgtSectionB" scope="request" class="com.velos.esch.web.bgtSection.BgtSectionJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.esch.business.common.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil"%>
<%

	String src =request.getParameter("src");   
	String mode =request.getParameter("mode");
	String selectedTab =request.getParameter("selectedTab");	
	String sequence= request.getParameter("sequence");
	String secName= request.getParameter("secName");
  	String calId=request.getParameter("calId");
  	String budgetId = request.getParameter("budgetId");
	String budgetTemplate = request.getParameter("budgetTemplate");
	String sectionMode =request.getParameter("sectionMode");
	String notes =request.getParameter("notes");
	String secType =request.getParameter("secType");
	String fromRt =request.getParameter("fromRt");
	if (fromRt==null || fromRt.equals("null")) fromRt="";
	
		// includedIn will be passed as P when this page is included in Protocol Calendar tabs to show the default budget

	String includedIn = request.getParameter("includedIn");
	if(StringUtil.isEmpty(includedIn))
	{
	 includedIn = "";
	}


	int bgtSectionId=0;

   String eSign = request.getParameter("eSign");

 HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))
   {	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
   	String oldESign = (String) tSession.getValue("eSign");
	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {

 String ipAdd = (String) tSession.getValue("ipAdd");
 String usr = null;
 usr = (String) tSession.getValue("userId");
 
 String patNo = "1";
 
 if (sectionMode.equals("M"))
 {
    bgtSectionId=EJBUtil.stringToNum(request.getParameter("bgtSectionId"));
    bgtSectionB.setBgtSectionId(bgtSectionId); 
    bgtSectionB.getBgtSectionDetails();
    patNo=bgtSectionB.getBgtSectionPatNo();
 }


   bgtSectionB.setBgtSectionName(secName);
   if (budgetTemplate.equals("P") || budgetTemplate.equals("C") ) {
      bgtSectionB.setBgtSectionType(secType);
   }
   bgtSectionB.setBgtSectionSequence(sequence);
   bgtSectionB.setBgtSectionNotes(notes);   
   bgtSectionB.setBgtCal(calId);
   if ("P".equals(secType)){
	  patNo= (patNo==null)?"":patNo;
	  patNo= (patNo.equals(""))?"1":patNo;
   }  else{
	  patNo=null;
   }
   bgtSectionB.setBgtSectionPatNo(patNo);
   
   if (sectionMode.equals("M")) {

	 bgtSectionB.setModifiedBy(usr);
	 bgtSectionB.setIpAdd(ipAdd);
	 bgtSectionB.updateBgtSection();

   }  else {     

	bgtSectionB.setCreator(usr);
	bgtSectionB.setIpAdd(ipAdd);   
	bgtSectionB.setBgtSectionDetails();

   }



%>
<br>
<br>
<br>
<br>
<br>

<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p>

<% if ("S".equals(budgetTemplate)) {  %>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=budgetsections.jsp?includedIn=<%=includedIn%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&budgetId=<%=budgetId%>&calId=<%=calId%>&budgetTemplate=<%=budgetTemplate%>&mode=<%=mode%>&fromRt=<%=fromRt%>">
<% } else { %>
<script>
	//window.opener.location.reload();
	window.location.href = "budgetsections.jsp?includedIn=<%=includedIn%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&budgetId=<%=budgetId%>&calId=<%=calId%>&budgetTemplate=<%=budgetTemplate%>&mode=<%=mode%>&fromRt=<%=fromRt%>";
	window.opener.location=window.opener.location;
	setTimeout("self.close()",1000);
</script>
<%
   }

} //end of if for eSign check
   }

else {   

%>
  <jsp:include page="timeout.html" flush="true"/>
<%
}
%> 


</BODY>
</HTML>
