<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Lcl_SampleSizeSubm %><%-- Local Sample Size Submit*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.MC" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>

<jsp:useBean id="studySiteB" scope="request" class="com.velos.eres.web.studySite.StudySiteJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>

<body>
<DIV class="popDefault" id="div1"> 

<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
		int userId = EJBUtil.stringToNum((String) tSession.getValue("userId"));
		String ipAdd = (String) tSession.getValue("ipAdd");	
%>	
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   
		String eSign= request.getParameter("eSign");
    	String oldESign = (String) tSession.getValue("eSign");
	   if(oldESign.equals(eSign)) 
	   {
			int errorCode=0;
			String[] siteIds=request.getParameterValues("siteId");
			String[] localSamples=request.getParameterValues("localSample");
			String[] studySiteIds=request.getParameterValues("studySiteId");
		
		
	   int ret = studySiteB.updateLSampleSizes(studySiteIds, localSamples, userId, ipAdd);
		
		if ( ret == -1 )
		{
%>
			
				<br><br><br><br><br><p class = "successfulmsg" align = center>		
	 			<%=MC.M_Not_Succ %><%-- Not Successful*****--%></p>
<%	
		} //end of if for Error Code

		else

		{
%>		<br><br><br><br><br><p class = "successfulmsg" align = center>	
		<%=MC.M_Data_SavedSucc %><%-- Data saved Successfully*****--%></p>
		
		<script>
					 
					setTimeout("self.close()",1000);
			</script>	

		
		
	<%
		}
	}//end of if old esign
	else{  
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%	  
	
	}//end of else of incorrect of esign
   }//end of if body for session 	
	
	else
	{

%>
		  <jsp:include page="timeout.html" flush="true"/>
<%
	
} //end of else of the Session 
 

%>

</body>

</html>
