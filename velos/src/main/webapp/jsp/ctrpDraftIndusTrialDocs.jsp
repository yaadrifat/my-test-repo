<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC" %>
<%@page import="com.velos.eres.service.util.MC" %>

<script>
var studyID = <s:property escape="false" value="ctrpDraftJB.fkStudy" />
function openStudyAppdx(docPkval,docDisVal){	    
	windowName=window.open("getlookup.jsp?viewId=&viewName=Study Appendix&protocolid="+studyID+"&form=ctrpDraftIndustrialForm&dfilter=&keyword="+docPkval+"|PK_STUDYAPNDX~"+docDisVal+"|STUDYAPNDX_URI","Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100");
	windowName.focus();
}
</script>
<table>
   <tr>
   		<td align="center" valign="middle" width="80%" ><div class="defComments"><%=MC.CTRP_DraftAbbTrialDocReqWhnNoNCT %></div></td>
   		<td width="20%">&nbsp;</td>
   </tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td width="50%" valign="top">
		<table cellpadding="0" cellspacing="0" border="0">
		    
		     <tr height="10px"><td colspan="6"></td></tr>
		      <tr>
				<td align="right" width="30%">
					<%=LC.CTRP_DraftAbbTrialTmplt %>&nbsp;
					<span id="ctrpDraftJB.abbrTrialTempDesc_error" class="errorMessage" style="display:none;"></span>
				</td>
				<td width="2%"><FONT style="visibility:visible" class="Mandatory">*</FONT>
				</td>
				<td align="left">
					<s:textfield name="ctrpDraftJB.abbrTrialTempDesc" id="ctrpDraftJB.abbrTrialTempDesc" size="40" readonly="true"/>
					<s:hidden name="ctrpDraftJB.abbrTrialTempPK" id="ctrpDraftJB.abbrTrialTempPK"  />
					<s:hidden name="ctrpDraftJB.trialAbbrTrialTempPK" id="ctrpDraftJB.trialAbbrTrialTempPK"  />
					<button type="button" id="select" name="select" onClick="openStudyAppdx('ctrpDraftJB.abbrTrialTempPK','ctrpDraftJB.abbrTrialTempDesc')"> <%=LC.L_Select%></button>
				</td>
			</tr>
			<tr>
				<td align="right">
					<%=LC.L_Other%>&nbsp;
					<span id="ctrpDraftJB.otherDesc_error" class="errorMessage" style="display:none;"></span>
				</td>
				<td>&nbsp;</td>
				<td align="left">
					<s:textfield name="ctrpDraftJB.otherDesc" id="ctrpDraftJB.otherDesc" size="40" readonly="true"/>
					<s:hidden name="ctrpDraftJB.otherPK" id="ctrpDraftJB.otherPK" />
					<s:hidden name="ctrpDraftJB.trialOtherPK" id="ctrpDraftJB.trialOtherPK" />
					<button type="button" id="select" name="select" onClick="openStudyAppdx('ctrpDraftJB.otherPK','ctrpDraftJB.otherDesc')"> <%=LC.L_Select%></button>
				</td>
			</tr>
		</table>
</td>
<td width="50%"></td>
</tr>
</table>