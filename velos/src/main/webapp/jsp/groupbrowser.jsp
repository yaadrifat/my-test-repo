<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Acc_Grps%><%--Account Groups*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<% String src;
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   
<body>
<br>
<jsp:useBean id="groupB" scope="request" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
<% int accountId=0;  
   int pageRight = 0;
   HttpSession tSession = request.getSession(true); 
%>
<DIV class="browserDefault" id="div1"> 
	<%
	if (sessionmaint.isValidSession(tSession))
	{
	   	String acc = (String) tSession.getValue("accountId");
	   	String uName = (String) tSession.getValue("userName");
	   	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	   	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CGRP"));
	   	accountId = EJBUtil.stringToNum(acc);
	   	GroupDao groupDao = groupB.getByAccountId(accountId);
	   	ArrayList grpIds = groupDao.getGrpIds(); 
	   	ArrayList grpNames = groupDao.getGrpNames();
	   	ArrayList grpDescs = groupDao.getGrpDescs();
	   	String grpName = null;
	   	String grpDesc = null;
	   	int len = grpIds.size();
	   	int counter = 0;
	   	if (pageRight > 0 )
		{
		%>
			<P class = "userName"> <%= uName %> </P>
			<P class="sectionHeadings"> <%=LC.L_MngAcc_Grp%><%--Manage Account >> Groups*****--%></P>
			<table width="100%" cellspacing="0" cellpadding="0" border=0 >
				<tr > 
				    <td width = "50%"> 
				        <P class = "defComments"><%=MC.M_ListGrp_AldyCreated%><%-- The list below displays the Groups that have already been created for your account*****--%>:</P>
				    </td>
				    <td width = "50%" align="right"> 
<p> 
					  <A href="grouplist.jsp?mode=N&srcmenu=<%=src%>&fromPage=groupbrowser"><%=LC.L_Add_MultiGrps%><%--Add multiple Groups*****--%></A> &nbsp;&nbsp		        <A href="group.jsp?mode=N&srcmenu=<%=src%>&fromPage=groupbrowser"><%=MC.M_AddNew_Grp%><%--Add a New Group*****--%></A> 
</p>
				    </td>
			    </tr>
			</table>
			<Form name="groupbrowser" method="post" action="" onsubmit="">
				<table width="100%" >
					<tr> 
						<th width="35%"> <%=LC.L_Group_Name%><%--Group Name*****--%> </th>
						<th width="30%"> <%=LC.L_Group_Desc%><%--Group Description*****--%> </th>
						<th width="35%"> &nbsp;</th>
					</tr>
				      <%
					for(counter = 0;counter<len;counter++)
					{	
						grpName=((grpNames.get(counter)) == null)?"-":(grpNames.get(counter)).toString();
						grpDesc=((grpDescs.get(counter)) == null)?"-":(grpDescs.get(counter)).toString();
						if ((counter%2)==0) {
						%>
						    <tr class="browserEvenRow"> 
				        <%
						}else{
						%>
						    <tr class="browserOddRow"> 
				        <%
						}
						%>
				        <td><A href = "group.jsp?mode=M&grpId=<%= grpIds.get(counter)%>&srcmenu=<%=src%>&fromPage=groupbrowser"><%= grpName%></A></td>
				        <td> <%= grpDesc%> </td>
				        <td> <A href="groupRights.jsp?mode=M&srcmenu=<%=src%>&groupId=<%=grpIds.get(counter)%>&groupName=<%=grpName%>"><%=LC.L_Assign_Rights%><%--Assign Rights*****--%></A> &nbsp;&nbsp; <A href="groupUsers.jsp?mode=M&srcmenu=<%=src%>&groupId=<%=grpIds.get(counter)%>&grpName=<%=grpName%>"><%=LC.L_Grp_Users%><%--Group Users*****--%></A> </td>
						</tr>
					    <%
					}
					%>
				</table>
			</Form>
			<%	
		} //end of if body for page right
		else
		{
		%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
		<%
		} //end of else body for page right
	}//end of if body for session
	else
	{
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>
	<div> 
    	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</DIV>
<div class ="mainMenu" id="emenu"> 
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>
