<%--
* File Name 	: calMilestoneSetup.jsp 
* Created on	: 2/24/11
* Created By	: YogeshKumar/AshuKhatkar
* Enhancement	: 8_10.0DFIV12 
* Purpose		: Shows Event-Visit Grid to Create MileStones through Study Calendar.
*
*
--%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.velos.esch.business.common.SchCodeDao"%><html>
<head>
<style type="text/css">
.yui-dt-bd[style="height: 480px; width: 1000px;"] { height: 60% !important; } 
.yui-dt-liner { white-space:normal; } 
.yui-skin-sam .yui-dt td.up { background-color: #efe; } 
.yui-skin-sam .yui-dt td.down { background-color: #fee; }
</style>
<%@page import="com.velos.eres.web.objectSettings.ObjectSettingsCache,com.velos.eres.web.objectSettings.ObjectSettings"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*" %>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="stdRightsB" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" /><!-- YK 10Mar2011 Bug#5905 -->

<%
ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
ArrayList tabList = null;
String titleStr = LC.L_Calendar/*"Calendar"*****/;
if (sessionmaint.isValidSession(request.getSession(true)))
{
    String accId = (String) request.getSession(true).getValue("accountId");
    
    ArrayList tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "protocol_tab");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("9".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
            	Object[] arguments = {titleStr,settings.getObjDispTxt()};
                titleStr = VelosResourceBundle.getLabelString("L_Gtgt",arguments);/*titleStr +=  " >> " + settings.getObjDispTxt();*****/
            }
            break;
        } 
    }
    tempList = null;
}
String src= StringUtil.htmlEncodeXss(request.getParameter("srcmenu"));
%>
<title><%=titleStr %></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<jsp:include page="ui-include.jsp" flush="true"/>
</head>

<% 
HttpSession tSession = request.getSession(true); 
String accountId = "";
String studyId = "";
String grpId = "";
String usrId = "";
if (sessionmaint.isValidSession(tSession)) {
	accountId = (String) tSession.getValue("accountId");
	studyId = (String) tSession.getValue("studyId");
	grpId = (String) tSession.getValue("defUserGroup");
	usrId = (String) tSession.getValue("userId");
	
	if ("N".equals(request.getParameter("mode"))) {
		studyId = "";
		tSession.removeAttribute("studyId");
	}
%>
<body class="yui-skin-sam yui-dt yui-dt-liner">
<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<%
	String from = LC.L_Version/*"version"*****/;
	String tab = request.getParameter("selectedTab");
    String calassoc = StringUtil.trueValue(request.getParameter("calassoc"));
	String studyIdForTabs = request.getParameter("studyId");
    String calledFrom = request.getParameter("calledFrom");
    /* YK 10Mar2011 Bug#5905 */
    int pageRight = 0;
    int mileGrpRight =0;

	if (calledFrom.equals("S")) {
		String study = request.getParameter("studyId");
	    GrpRightsJB grprightsJB = (GrpRightsJB) tSession.getValue("GRights");
		
		mileGrpRight = Integer.parseInt(grprightsJB.getFtrRightsByValue("MILEST"));

	    int mileRight= 0;
		String userIdFromSession = (String) tSession.getValue("userId");
	    int studyIdOne = EJBUtil.stringToNum(studyId);

		ArrayList teamId ;
		teamId = new ArrayList();
		TeamDao teamDao = new TeamDao();
		teamDao.getTeamRights(studyIdOne,EJBUtil.stringToNum(userIdFromSession));
		teamId = teamDao.getTeamIds();

		if (teamId.size() <=0)
		{
			mileRight  = 0;
		
		}
		ArrayList teamRights =null;
		if (teamId.size() == 0) {
			mileRight=0 ;
		}else {
			stdRightsB.setId(EJBUtil.stringToNum(teamId.get(0).toString()));

						teamRights  = new ArrayList();
						teamRights = teamDao.getTeamRights();

						stdRightsB.setSuperRightsStringForStudy((String)teamRights.get(0));
						stdRightsB.loadStudyRights();


			if ((stdRightsB.getFtrRights().size()) == 0){
				mileRight = 0;
			}else{
				mileRight = Integer.parseInt(stdRightsB.getFtrRightsByValue("MILESTONES"));
			}
		}
		tSession.setAttribute("mileRight",(new Integer(mileRight)).toString());

		pageRight = mileRight;
	   
	} 
	/* YK 10Mar2011 Bug#5905 */
		 
	if (pageRight > 0 && mileGrpRight >= 4)  {
%>

<DIV class="BrowserTopn" id = "div1">
	<jsp:include page="protocoltabs.jsp" flush="true">
	<jsp:param name="calassoc" value="<%=StringUtil.htmlEncodeXss(calassoc)%>" />
	<jsp:param name="selectedTab" value="<%=StringUtil.htmlEncodeXss(tab)%>" />
	</jsp:include>
</DIV>
<DIV class="BrowserBotN  BrowserBotN_CL_1" id = "div2">
<%
	} // end of pageRight > 0
	String protocolId = request.getParameter("protocolId");
	if(protocolId == null || protocolId == "" || protocolId.equals("null") || protocolId.equals("")) {
%>
	<jsp:include page="calDoesNotExist.jsp" flush="true"/>
<%
	} else {
	String calStatus = null;
    String calStatusPk = "";
    SchCodeDao scho = new SchCodeDao();
	if (pageRight > 0) {
	    String tableName = null;
	   /* YK 10Mar2011 un-neccessary code removed*/
 	  if("S".equals(calledFrom)) {
	   		tableName ="event_assoc";
			eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
			eventassocB.getEventAssocDetails();
			calStatusPk = eventassocB.getStatCode();
			calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();
	   	}
		String duration = StringUtil.trueValue(request.getParameter("duration"));
		StringBuffer urlParamSB = new StringBuffer();
		urlParamSB.append("protocolId=").append(StringUtil.htmlEncodeXss(protocolId))
		.append("&calledFrom=").append(StringUtil.htmlEncodeXss(calledFrom))
		.append("&duration=").append(StringUtil.htmlEncodeXss(duration))
		.append("&calassoc=").append(StringUtil.htmlEncodeXss(calassoc))
		.append("&calstatus=").append(StringUtil.htmlEncodeXss(request.getParameter("calStatus")))
		.append("&mode=").append(StringUtil.htmlEncodeXss(request.getParameter("mode")))
		.append("&calProtocolId=").append(StringUtil.htmlEncodeXss(protocolId))
		.append("&tableName=").append(tableName)
		.append("&studyId=").append(studyId)
		;
%>
<script>
function reloadDataGrid() {
	YAHOO.example.DataGrid = function() {
		var args = {
			urlParams: "<%=urlParamSB.toString()%>",
			dataTable: "calMilestoneGrid"
		};
		myDataGrid = new VELOS.calMileStoneGrid('calMileStoneJSON.jsp', args);
		myDataGrid.startRequest();
	   }();
}
YAHOO.util.Event.addListener(window, "load", function() {
	reloadDataGrid();
});
</script>

<%
%>
<input type="hidden" id ="pageRight" name="pageRight" value="<%=pageRight%>"/>
  <table width="98%" cellspacing="0" cellpadding="0" >
  <tr>
  <%Object[] arguments = {getCalStatusName(calStatus)};%>
  <td><%=VelosResourceBundle.getLabelString("L_CalStat",arguments)%><%--Calendar Status: <%=getCalStatusName(calStatus)%>*****--%>&nbsp;&nbsp;&nbsp;&nbsp;
  <!-- AK: Implemented enh 'D-FIN12 calendar status amendment (5th April 11--> 
    <%if ((calStatus.equals("D"))) 
		{%>
	       <FONT class="Mandatory"><%=MC.M_CntMstone_DeactCal%><%--Cannot create milestones for <b>'Deactivated'</b> calendars*****--%></Font>
		<%}%>
	<button id="save_changes" name="save_changes" onclick="if (f_check_perm(<%=pageRight%>,'N')) { VELOS.calMileStoneGrid.saveDialog(); }" ><%=LC.L_Create_MstoneRule%></button>
    &nbsp;&nbsp;&nbsp;<div id="accessPermission"></div></td>
    <% if ((pageRight == 7) || (pageRight == 1) || (pageRight == 3) || (pageRight == 5)) { %>
  <td>&nbsp;&nbsp;&nbsp;&nbsp;<A href = "studyMilestones.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=12&studyId=<%=studyId%>"><%=LC.L_AddOrEdit_Mstone%><%--Add/Edit Milestones*****--%></A></td> <!--AK:Fixed Bug#5961 13thApr11-->
  </tr> 
  <%} %>
  
  </table>
  <div id="calMilestoneGrid"></div>
  
  <!-- YK 10Mar2011 Bug#5905 -->
  <script type="text/javascript">
  var pageRight="<%=pageRight%>";
  if ((pageRight == "7") || (pageRight == "1") || (pageRight == "3") || (pageRight == "5")) {
	}else
	{
		document.getElementById("accessPermission").innerHTML=" <FONT class='Mandatory'><%=MC.M_NoRgt_CreateMstone%><%--You do not have Access Rights to create Milestone*****--%></font>";
		document.getElementById("save_changes").disabled=true;
		
		}
	
  </script>
  <!-- YK 10Mar2011 Bug#5905 -->
<%
  } // end of pageRight
  else {
%>
<DIV class="BrowserBotN  BrowserBotN_CL_1" id="div2">
	<jsp:include page="accessdenied.jsp" flush="true"/>
</DIV>
<%
  } // end of else of pageRight
} // end of else of invalid protocolId
} else { // else of valid session
%>
  <jsp:include page="timeout.html" flush="true"/>
<%
} // end of else of valid session
%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu" > 
  <jsp:include page="getmenu.jsp" flush="true"/> 
</div>
</body>
</html>
<%!
public static final String CAL_STATUS_A = LC.L_Active/*"Active"*****/;
public static final String CAL_STATUS_D = LC.L_Deactivated/*"Deactivated"*****/;
public static final String CAL_STATUS_F = LC.L_Freeze/*"Freeze"*****/;
public static final String CAL_STATUS_O = LC.L_Offline_ForEditing/*"Offline for Editing"*****/;
public static final String CAL_STATUS_R = LC.L_Reactivated/*"Reactivated"*****/;
public static final String CAL_STATUS_W = LC.L_Work_InProgress/*"Work in Progress"*****/;
public static final String LETTER_A = "A";
public static final String LETTER_D = "D";
public static final String LETTER_F = "F";
public static final String LETTER_O = "O";
public static final String LETTER_R = "R";
public static final String LETTER_W = "W";
public static final String EMPTY_STRING = "";

private String getCalStatusName(String calStatus) {
    if (LETTER_A.equals(calStatus)) { return CAL_STATUS_A; }
    if (LETTER_D.equals(calStatus)) { return CAL_STATUS_D; }
    if (LETTER_F.equals(calStatus)) { return CAL_STATUS_F; }
    if (LETTER_O.equals(calStatus)) { return CAL_STATUS_O; }
    if (LETTER_R.equals(calStatus)) { return CAL_STATUS_R; }
    if (LETTER_W.equals(calStatus)) { return CAL_STATUS_W; }
    return EMPTY_STRING;
}
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>