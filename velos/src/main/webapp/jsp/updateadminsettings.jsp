<!-- 
	File Name: updateadminsettings.jsp

	Author : Gopu
	
	Created Date	: 07/28/2006

	Last Modified Date : 

	Purpose	: Default account level settings for User / Patient Timezone, Seesion Timeout time, Number of days password will expire, Number of days e-Sign will expire
	 
-->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<HEAD>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><%=LC.L_Admin_UpdateSettings%><%-- Admin Update Settings*****--%></title>

	<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
	<%@ page language = "java" import="com.velos.eres.business.common.SettingsDao,com.velos.eres.service.util.*"%>
	<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<body>
<%

String utimeZone=request.getParameter("utimeZone");
String ptimeZone=request.getParameter("ptimeZone");
String uzone=request.getParameter("uzonetxt");
String pzone=request.getParameter("pzonetxt");
String logouttime=request.getParameter("logouttime");
String ltime=request.getParameter("ltimetxt");
String pwdexpires=request.getParameter("pwdexpires");
String pwdcheck=request.getParameter("pwdchecktxt");
String esignexpires=request.getParameter("esignexpires");
String echeck=request.getParameter("echecktxt");
String eSign = request.getParameter("eSign");


HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession)){
	%>
		<jsp:include page="sessionlogging.jsp" flush="true"/> 
	<%   
		String oldESign = (String) tSession.getValue("eSign");
	
	if(!oldESign.equals(eSign)){
	%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>	
	<%
	} else {
	
		String accId=(String)tSession.getValue("accountId");
		String usr=(String)tSession.getAttribute("userId"); //Added By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.
		SettingsDao settingsDao= new SettingsDao();

	for(int i=0;i<=9;i++){
	   settingsDao.setSettingsModNum(accId);
	   settingsDao.setSettingsModName("1");
	}
	
	settingsDao.setSettingKeyword("ACC_USER_TZ");
	settingsDao.setSettingKeyword("ACC_USER_TZ_OVERRIDE");
	settingsDao.setSettingKeyword("ACC_PAT_TZ");
	settingsDao.setSettingKeyword("ACC_PAT_TZ_OVERRIDE");
	settingsDao.setSettingKeyword("ACC_SESSION_TIMEOUT");
	settingsDao.setSettingKeyword("ACC_SESSION_TIMEOUT_OVERRIDE");
	settingsDao.setSettingKeyword("ACC_DAYS_PWD_EXP");
	settingsDao.setSettingKeyword("ACC_DAYS_PWD_EXP_OVERRIDE");
	settingsDao.setSettingKeyword("ACC_DAYS_ESIGN_EXP");
	settingsDao.setSettingKeyword("ACC_DAYS_ESIGN_EXP_OVERRIDE");
	
	settingsDao.setSettingValue(utimeZone);
	settingsDao.setSettingValue(uzone);
	settingsDao.setSettingValue(ptimeZone);
	settingsDao.setSettingValue(pzone);
	settingsDao.setSettingValue(logouttime);
	settingsDao.setSettingValue(ltime);
	settingsDao.setSettingValue(pwdexpires);
	settingsDao.setSettingValue(pwdcheck);
	settingsDao.setSettingValue(esignexpires);
	settingsDao.setSettingValue(echeck);
	settingsDao.setSettingModifiedBy(usr); //Added By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.
//	int ret=settingsDao.updateAdminSettings();
	int ret=commonB.updateSettingsWithoutPK(settingsDao);

	if(ret ==1){  
  %>
	 <p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%-- Data was saved successfully*****--%> </p>
	  <script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
	  </script>

<%}
  }//end of if for eSign check
}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</body>
</html>

