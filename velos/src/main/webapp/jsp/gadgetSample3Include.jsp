<!-- 
This JSP will be included in gadgetHome.jsp. All non-local javascript variables and functions 
will be global in the mother JSP. Therefore, make sure to suffix all non-local variables and functions
with the gadget ID.
 -->

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
if (!sessionmaint.isValidSession(request.getSession(false))) {
	return;
}
%>
<script>
var gadgetSample3_configure = function() {
	try {
		$j("#gadgetSample3_mainTabs").tabs({selected:2});
	} catch(e) {
		// console.log('In gadgetSample3_configure:'+e);
	}
}
var gadgetSample3_screenAction = function() {
	$j(function() {
		$j("#gadgetSample3_mainTabs").tabs({selected:1});
	});
	$j("#gadgetSample3_configButton").button();
	$j("#gadgetSample3_configButton").click(function() {
		gadgetSample3_configure();
	});
	$j("#gadgetSample3_saveConfigButton").button();
	$j("#gadgetSample3_saveConfigButton").click(function() {
		$j("#gadgetSample3_mainTabs").tabs({selected:1});
	});
};
screenActionRegistry['gadgetSample3'] = gadgetSample3_screenAction;
</script>
