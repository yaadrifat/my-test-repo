<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<jsp:include page="include.jsp" flush="true"/>
<title><%=LC.L_Review_Popup%><%--Review Pop-up*****--%></title> 
</head>
<body>
<DIV style="font-family:Verdana,Arial,Helvetica,sans-serif">
<form name="mainForm" id="mainForm" action="previewIRBDocs.jsp" method="post">
<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="submBoardB" scope="request" class="com.velos.eres.web.submission.SubmissionBoardJB"/>
<%@ page import="com.velos.eres.business.common.*,com.velos.eres.service.util.*" %>
<%@ page import="java.util.ArrayList,java.util.Calendar,java.util.Hashtable" %>
<%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {
    String accountId = (String)tSession.getAttribute("accountId");
    String studyId = StringUtil.htmlEncodeXss(request.getParameter("studyId"));
    String selType = StringUtil.htmlEncodeXss(request.getParameter("selType"));
    
    String study_acc_form_right = "";
	String study_team_form_access_right = "";
	String appSubmissionType = "";
		
	study_acc_form_right = request.getParameter("study_acc_form_right");
	study_team_form_access_right = request.getParameter("study_team_form_access_right");
	
	appSubmissionType= request.getParameter("appSubmissionType");

    if (studyId == null || studyId.length() == 0) { 
%>
<script>
  self.close();
</script></form></body></html>
<%      return;
    } 
%>
<input type="hidden" id="studyId" name="studyId" value="<%=EJBUtil.stringToNum(studyId)%>" />
<input type="hidden" id="selType" name="selType" value="<%=selType%>" />
<input type="hidden" name="study_acc_form_right" value="<%=study_acc_form_right%>" />
<input type="hidden" name="study_team_form_access_right" value="<%=study_team_form_access_right%>" />
<input type="hidden" name="appSubmissionType" value="<%=appSubmissionType%>" />

<iframe name="preview" src="previewIRBDocs.jsp" width="100%" height="100%" frameborder="1" scrolling="yes" allowautotransparency=true>
</iframe>

<%    
} 
else { // session invalid
%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%
}
%>
  <div class = "myHomebottomPanel">
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
</form>
<script>
document.mainForm.submit();
</script>
</body>
</html>
