<%@page import="com.velos.eres.service.util.Configuration"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC,com.velos.eres.service.util.CFG" %>
<%@ page import="com.velos.eres.service.util.StringUtil" %>
<%@page import="com.velos.eres.web.study.StudyJB"%><jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyJB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<SCRIPT LANGUAGE="JavaScript" src="validations.js"></SCRIPT>
<%!
static final String TAreaDisSiteFlag = "TAreaDisSite_ON";
%>
<%
HttpSession tSession = request.getSession(true);
if (!sessionmaint.isValidSession(tSession))
	return;

int accountId = StringUtil.stringToNum((String) tSession.getAttribute("accountId"));
int studyId = StringUtil.stringToNum((String) request.getParameter("studyId"));
String defUserGroup = (tSession.getAttribute("defUserGroup")).toString();
String mode = (String) request.getParameter("mode");
int pageRight = 0;
String nctNumber ="";
String l_valid_Sign="";
l_valid_Sign=LC.L_Valid_Esign;
if("userpxd".equals(Configuration.eSignConf))
	l_valid_Sign="Valid-Password";

if("M".equals(mode)){
nctNumber = (String) request.getParameter("nctNumber");
nctNumber = (nctNumber==null)?"":nctNumber;
if(nctNumber.length()>0){
StudyJB studyB = new StudyJB();
studyB.setId(studyId);
studyB.getStudyDetails();
studyB.setNctNumber(nctNumber);
studyB.updateStudy();
}
}

int studyRightForJS = 0;
if ("M".equals(mode)){
	String strRight = String.valueOf((Integer)tSession.getAttribute("studyRightForJS"));
	studyRightForJS = (StringUtil.isEmpty(strRight))? 0 : StringUtil.stringToNum(strRight);
}
tSession.removeAttribute("studyRightForJS");

String strAutoGenStudy = null;
int autoGenStudy = 0;
if (tSession.getAttribute("autoGenStudyForJS") instanceof String) {
	strAutoGenStudy = (String)tSession.getAttribute("autoGenStudyForJS");
	autoGenStudy = (StringUtil.isEmpty(strAutoGenStudy))? 0 : StringUtil.stringToNum(strAutoGenStudy);
} else if (tSession.getAttribute("autoGenStudyForJS") instanceof Integer) {
	autoGenStudy = (Integer)tSession.getAttribute("autoGenStudyForJS");
}
tSession.removeAttribute("autoGenStudyForJS");

//getting the value of the key for autogenstudy from the studyJB if this is not coming in the session variables
HashMap sysDataMap = studyJB.getStudySysData(request);
if (null == sysDataMap)
	return;	
pageRight = StringUtil.stringToNum(sysDataMap.get("pageRight").toString());
strAutoGenStudy = (String) sysDataMap.get("autoGenStudy");
autoGenStudy =(StringUtil.isEmpty(strAutoGenStudy))? 0 : StringUtil.stringToNum(strAutoGenStudy);
if (autoGenStudy >0){
	tSession.setAttribute("autoGenStudyForJS",autoGenStudy);
}
else{
	tSession.removeAttribute("autoGenStudyForJS");
 }



%>

<script>
	//Declare all functions here
	var studyFunctions = {
		sessAccId: <%=accountId%>,
		studyId: <%=studyId%>,
		defUserGroup: <%=defUserGroup%>,
		entryMode: '<%=mode%>',
		formObj: {},
		
		studyResType: "",
		
		studyRightForJS: <%=studyRightForJS%>,
		autoGenStudy: <%=autoGenStudy%>,
		openLSampleSize: {},
		openWinICD: {},
		openLookup: {},
		openLookupSpnsr: {},
		selectDisease: {},
		setValue: {},
		setValueCCSGReport: {},
		validate: {},
		validateMin: {},
		openwin: {},
		openwinstudyid: {},
		callAjaxGetTareaDD: {},
		jQueryAjaxStudyNum: {},
		fnAddNciNctRow: {},
		openwin1: {}
	};

	studyFunctions.openLSampleSize =  function (studyId, mode)	{
		if (mode == 'N') {
			alert("<%=MC.M_SbmtStdFirst_ForSampleSize%>");/*alert("Please Submit the Study first for entering Local Sample Size");*****/
			return false;
		}else{
			windowName = window.open("LSampleSizePopUp.jsp?studyId="+studyId,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=600,height=500 top=50,left=100 0, ");
			windowName.focus();
		}
	};
	//Added by Manimaran for September Enhancement2 to select the ICD codes.
	studyFunctions.openWinICD = function(codestr,lkpId,lkpColumn){
		windowName=window.open("getlookup.jsp?viewId="+lkpId+"&form="+studyFunctions.formObj.name+"&dfilter=&keyword="+codestr+"|"+lkpColumn+"","Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100");
	    windowName.focus();
	};

	studyFunctions.openLookup = function (acc){
		dfilter = "study";
		windowName = window.open("getlookup.jsp?viewId=&viewName=Study Summary&form="+studyFunctions.formObj.name+"&dfilter="+dfilter+"&keyword=studyAssocNum|VELSTUDY_NUM~studyAssoc|VELPK","Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100 0, ")
		windowName.focus();
	};

	studyFunctions.openLookupSpnsr = function(formobj) {
		formobj.target="Lookup";
		formobj.method="post";
		formobj.action="multilookup.jsp?viewId=6011&form="+formobj.name+"&seperator=;"+
	                  "&dfilter=codelst_type='sponsor'&keyword=sponsor1|CODEDESC~sponsorpk|CODEPK|[VELHIDE]&maxselect=1";

		formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');

		if (formWin && !formWin.closed) formWin.focus();
		formobj.submit();
		formobj.target="_self";
		formobj.action="#";
		void(0);
	};

	studyFunctions.selectDisease = function(formobj){
		var tArea = null;
		if (formobj.studyTArea) { tArea = formobj.studyTArea.value; }
		var name=formobj.disSitename.value;
		name=encodeString(name);
		var id= (formobj.disSiteid.value) ? formobj.disSiteid.value : "";
		windowName = window.open("selectdiseasesite.jsp?from="+studyFunctions.formObj.name+"&datafld=disSiteid&dispfld=disSitename&names="+name +"&ids="+id+"&tArea="+tArea,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=800,height=600 top=50,left=100 0, ")
		windowName.focus();
	};

	studyFunctions.setValue = function(formobj){
		value=formobj.author.value;
		if (value=="Y"){
			formobj.cbauth.checked=false;
			formobj.author.value="";
		}
		if ((value.length==0) || (value=="N")){
			formobj.cbauth.checked=true;
			formobj.author.value="Y";
		}
	
	};
	
	studyFunctions.setValueCCSGReport = function(formobj){
		value=formobj.studyDivision.value;
		if (value=="7801"){
			formobj.ccsgdt.checked=true;
		}else{
			formobj.ccsgdt.checked=false;
			}	
	};
	
	studyFunctions.validateMin = function (formobj,autogen,accId) {
		var mode = formobj.mode.value;
		if (studyFunctions.autoGenStudy != 1){
		  if (autogen != 1) {
			  if (formobj.studyNumber) {
				  if (!(validate_col('Study Number', formobj.studyNumber))) return false;
			  }
		  }
		}

		if (formobj.studyNumber) {
	 		   if (checkChar(formobj.studyNumber.value,'<') ) {
			      //alert("Special character '<' is not allowed in Study Number");
				  alert("<%=MC.M_SplChar_LtNtAlwdStd%>");/*alert("Special character '<' is not allowed in <%=LC.Std_Study%> Number");*****/
		          formobj.studyNumber.focus();
			   return false;
		    }
	 		  if ((formobj.studyNumber.value.indexOf("\"") != -1)) {	
	 			var paramArray = ["\""];
	 			alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));/*alert("Special Character(") not allowed for this Field");*****/
	 			formobj.studyNumber.focus();
	 			return false;
	 				return false;	
	 			}
	 		 if ((formobj.studyNumber.value.indexOf("'") != -1)) {	
	 			var paramArray = ["'"];
	 			alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));/*alert("Special Character(') not allowed for this Field");*****/
	 			formobj.studyNumber.focus();
	 			return false;
	 				return false;	
	 			}
		}

		if(!studyFunctions.jQueryAjaxStudyNum(formobj,accId)){
			alert(M_StdNumExst);
			formobj.studyNumber.focus();
			return false;
		}

		//Modified to not allowed the special character "," in Study Number
		if(formobj.studyNumber.value.indexOf(",") != -1 ){
			var paramArray = [","];
			alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));/*alert("Special Character(,) not allowed for this Field");*****/
			formobj.studyNumber.focus();
			return false;
		}
		if (!(validate_col('Title',formobj.studyTitle))) return false;
		<%-- if ((formobj.studyTitle.value.indexOf("'") != -1)) {			
			alert("<%=MC.M_SplChar_SqDqAlwdStd%>");			
			return false;	
		} --%>
		if ((formobj.studyTitle.value.indexOf("\"") != -1)) {			
			alert("<%=MC.M_SplChar_SqDqAlwdStd%>");				
			return false;	
		}		
		if (formobj.dataManagerName) {
			if (!(validate_col('Study Entered By',formobj.dataManagerName))) return false;
		}
		if (formobj.eSign){
			if (!(validate_col('e-Sign',formobj.eSign))) return false;
		}
		return true;
	};
	
	studyFunctions.validate = function (formobj,autogen,accId) {
		 if (document.getElementById('mandstudyent')) { //KM-3655
			if (!(validate_col('Study Entered By',formobj.dataManagerName))) return false;
		 }

		 var mode = formobj.mode.value;

		 //KM: Modified for Edit mode.
		 /* Changes By Sudhir on 05-Apr-2012 for Bug #9121*/
		 /* Bug#11005 24-Aug-2012 -Sudhir*/
		 if (autogen != 1 || mode == 'M'){  //KM-0702		 
		   if (document.getElementById('mandsnumber')) {
		  	 if (!(validate_col('Study Number',formobj.studyNumber))) return false;
		   }
         }
		//Modified to not allowed the special character "," in Study Number
		 if(formobj.studyNumber.value.indexOf(",") != -1 ){
			var paramArray = [","];
			alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));/*alert("Special Character(,) not allowed for this Field");*****/
		 	formobj.studyNumber.focus();
			return false;
		 }   
		 /* Changes By Sudhir on 05-Apr-2012 for Bug #9121*/
		 if (document.getElementById('mandtarea')) {
			if (!(validate_col('Therapeutic Area',formobj.studyTArea))) return false;
		 }

		 if (document.getElementById('mandphase')) {
			if (!(validate_col('Study Phase',formobj.studyPhase))) return false;
		 }

		 if (document.getElementById('mandtitle')) {
		     if (!(validate_col('Title',formobj.studyTitle))) return false;
		 }

		if (formobj.eSign){
			if (!(validate_col('e-Sign',formobj.eSign))) return false;
		}

		/*if (!(isInteger(formobj.studySize.value))){
	 		alert("Invalid Study Size");
			formobj.studySize.focus();
            return false;
		}*/

		/////KM


		 if (document.getElementById('pgcustomstudyentby')) {
				 if (!(validate_col('Study Entered By',formobj.dataManagerName))) return false;
		 }


		 //Prinicipal Investigator mandatory/non-mand implementation
		 if (document.getElementById('pgcustompi')) {
				 if (!(validate_col('pinvestigator',formobj.prinInvName))) return false;
		 }

		 //if other mandatory/non-mand implementation
		 if (document.getElementById('pgcustomifotheruser')) {
			 if (!(validate_col('prinvOther',formobj.prinvOther))) return false;
		 }


		 if (document.getElementById('pgcustomstudycont')) {
				 if (!(validate_col('Study Contact',formobj.studycoName))) return false;
		 }


		 /* Bug#11005 24-Aug-2012 -Sudhir*/
		if (autogen != 1){
	    	if (!(validate_col('Study Number',formobj.studyNumber))) return false;
	    }


		 if (autogen != 1 || mode == 'M' ){ //KM:31July08
  		   if (document.getElementById('pgcustomstudynum')) {
				  if (!(validate_col('Study Number',formobj.studyNumber))) return false;
		   }
	     }



		 if (document.getElementById('pgcustomstudytitle')) {
				  if (!(validate_col('Title',formobj.studyTitle))) return false;
		 }



		 if (document.getElementById('pgcustomobjective')) {
				  if (!(validate_col('Objective',formobj.studyObjective))) return false;
		 }


		 if (document.getElementById('pgcustomsummary')) {
				  if (!(validate_col('Summary',formobj.studySummary))) return false;
		 }

		 if (document.getElementById('pgcustomagent')) {
				  if (!(validate_col('Agent',formobj.studyProduct))) return false;
		 }

		 if (document.getElementById('pgcustomdivision')) {
				  if (!(validate_col('Division',formobj.studyDivision))) return false;
		 }

		 if (document.getElementById('pgcustompurpose')) {
				  if (!(validate_col('Purpose',formobj.studyPurpose))) return false;
		 }


		if (document.getElementById('pgcustomtarea')) {
				 if (!(validate_col('Therapeutic Area',formobj.studyTArea))) return false;
		}


		if (document.getElementById('pgcustomdissite')) {
				 if (!(validate_col('Disease Site',formobj.disSitename))) return false;
		 }


		if (document.getElementById('pgcustomspecsite')) {
				 if (!(validate_col('Specific Site1',formobj.ICDcode1))) return false;
		 }

		if (document.getElementById('pgcustomspecsite')) {
				 if (!(validate_col('Specific Site2',formobj.ICDcode2))) return false;
		 }

		if (document.getElementById('pgcustomnss')) {
				 if (!(validate_col('National Sample Size',formobj.nStudySize))) return false;
		 }

		if (document.getElementById('pgcustomnss')) {
				 if (!(validate_col('National Sample Size',formobj.nStudySize))) return false;
		 }

		if (document.getElementById('pgcustomestdt')) {
				 if (!(validate_col('Estimated Begin Date',formobj.studyEstBgnDate))) return false;
		 }


		if (document.getElementById('pgcustomphase')) {
				 if (!(validate_col('Study Phase',formobj.studyPhase))) return false;
		 }

		if (document.getElementById('pgcustomresearchtype')) {
				 if (!(validate_col('Research Type',formobj.studyResType))) return false;
		 }

		if (document.getElementById('pgcustomscope')) {
				 if (!(validate_col('Study Scope',formobj.studyScope))) return false;
		 }


		if (document.getElementById('pgcustomtype')) {
				 if (!(validate_col('Study Type',formobj.studyType))) return false;
		 }

		if (document.getElementById('pgcustomstdlinkedto')) {
				 if (!(validate_col('Study LinkedTo',formobj.studyAssocNum))) return false;
		 }

		if (document.getElementById('pgcustomblinding')) {
				 if (!(validate_col('Blinding',formobj.studyBlinding))) return false;
		 }


		if (document.getElementById('pgcustomrandom')) {
				 if (!(validate_col('Randomization',formobj.studyRandom))) return false;
		 }

		if (document.getElementById('pgcustomspname')) {
				 if (!(validate_col('Sponsor Name',formobj.sponsor))) return false;
		 }

		if (document.getElementById('pgcustomspname1')) {
				 if (!(validate_col('Sponsor Name',formobj.sponsor1))) return false;
		 }

		if (document.getElementById('pgcustomothsponsor')) {
				 if (!(validate_col('If other',formobj.studySponsor))) return false;
		 }

		if (document.getElementById('pgcustomsponsorid')) {
				 if (!(validate_col('Sponsor ID',formobj.studySponsorIdInfo))) return false;
		 }

		if (document.getElementById('pgcustomcontact')) {
				 if (!(validate_col('Contact',formobj.studyContact))) return false;
		 }

		if (document.getElementById('pgcustomothinfo')) {
				 if (!(validate_col('Other Information',formobj.studyOtherInfo))) return false;
		 }

		if (document.getElementById('pgcustomkeywords')) {
				 if (!(validate_col('Keywords',formobj.studyKeywrds))) return false;
		 }

		if (document.getElementById('pgcustomduration')) {
				 if (!(validate_col('Study duration',formobj.studyDuration))) return false;
		 }

		if (document.getElementById('pgcustomstudyncitrialidentifier')) {
			 if (!(validate_col('NCI Trial Identifier',formobj.NCITrialIdentifier))) return false;
	 	}

		if (document.getElementById('pgcustomstudynctnumber')) {
			 if (!(validate_col('NCT Number',formobj.nctNumber))) return false;
		 }
			 
		//Added by Yogendra to fix Bug#10071.
		if (document.getElementById('prinvIfOther')){
			if(formobj.prinvOther.disabled != true) {
				if(document.getElementById('prinvIfOther').value.length>100){
					alert("<%=MC.M_PIOther_Size%>");/*alert("Maximum limit of characters that can be entered in the principal investigator IF OTHER field is 100");*****/
					formobj.prinvOther.focus();
					return false;
			 	}
		 	}
		}
		////KM
	
		//Added by Manimaran to fix the Bug.2658
		if (formobj.studyNumber) {
	 		   if (checkChar(formobj.studyNumber.value,'<') ) {
			      //alert("Special character '<' is not allowed in Study Number");
				  alert("<%=MC.M_SplChar_LtNtAlwdStd%>");/*alert("Special character '<' is not allowed in <%=LC.Std_Study%> Number");*****/
		          formobj.studyNumber.focus();
			   return false;
		    }
		}
		
		if(!studyFunctions.jQueryAjaxStudyNum(formobj,accId)){
			alert(M_StdNumExst);
			formobj.studyNumber.focus();
			return false;
		}
	
		if (!(isInteger(formobj.nStudySize.value))){
			alert("<%=LC.L_Invalid_SampleSize%>");/*alert("Invalid Sample Size");*****/
			formobj.nStudySize.focus();
	           return false;
		}
		if (!(isInteger(formobj.studyDuration.value))){
	 		alert("<%=LC.L_Invalid_Duration%>");/*alert("Invalid Duration");*****/
			formobj.studyDuration.focus()
	           return false;
		}
	
		//Added by Manimaran to fix the issue 3721
		if (document.getElementById('stdDuration')) {
			if ((formobj.studyDuration.value != 0) && (formobj.durationUnit.value == '')) {
				if(formobj.durationUnit.disabled != true) {
				  alert("<%=MC.M_Selc_StdDurUnit%>");/*alert("Please select <%=LC.Std_Study%> Duration unit");*****/
				  formobj.durationUnit.focus();
				  return false;
				}
			}
		}
	
		//Added for CTRP-22471 : Raviesh
		var nctNumVal = document.getElementById('nctNumber').value;
		if(!(nctNumVal=='' || nctNumVal==null)){
			var ar=new Array();
			var nctNumberVal = document.getElementById('nctNumber').value
			ar=nctNumberVal.split(" ");			
			var isMatch = nctNumberVal.length == 11 && nctNumberVal.substr(0, 3) == "NCT" && ar.length ==1 && !(isNaN(nctNumberVal.substr(4, 10)));
			if(!(isMatch && allSpclcharcheck(nctNumberVal,formobj.nctNumber))){
				alert("<%=MC.M_NctNumber_Format%>");
				formobj.nctNumber.focus();
				return false;
			}
		}
	
		if (document.getElementById('durunit')) {
			if ((formobj.durationUnit.value != '') && (formobj.studyDuration.value == '')) {
				if(formobj.studyDuration.disabled != true ) {
				  alert("<%=MC.M_Etr_StdDurVal%>");/*alert("Please enter <%=LC.Std_Study%> Duration value");*****/
				  formobj.studyDuration.focus();
				  return false;
				}
			}
		}
	
		//if (!(validate_col('Study Entered By',formobj.dataManagerName))) return false---Commented by Manimaran temporarily
	
	
		<%-- if(isNaN(formobj.eSign.value) == true) {
			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
			formobj.eSign.focus();
			return false;
		} --%>
	
		// Enhancemnet CTRP-20527  : AGodara
		if(formobj.cbIndIdeGrid != null && formobj.cbIndIdeGrid.checked != null && formobj.cbIndIdeGrid.checked){
			if(!validateIndIdeGrid(formobj)){
				return false;
			}
		}
		//Ak:Added for enhancemnet CTRP-20527_1
			if(formobj.nihGrantChk.checked != null && formobj.nihGrantChk.checked){
				if(!validateNIHGrantGrid(formobj)){
					return false;
				}
			}
	
		// Bug#8098 Fixed : Raviesh
		if(document.getElementById("eSignMessage").innerHTML != "<%=l_valid_Sign%>") {
			alert("<%=MC.M_IncorrPassword_EtrAgain%>");/*alert("Incorrect e-Password. Please enter again");*****/
			formobj.eSign.focus();
			return false;
		}
		return true;
    };


	studyFunctions.openwin = function() {
		window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=600,height=450");
	};

	studyFunctions.openwinstudyid = function(studyid, mode, studyRights){
		if (mode == 'M'){
			windowname=window.open("newStudyIds.jsp?studyId=" + studyid+"&studyRights="+studyRights ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=650,height=550 top=120,left=200 0, ");
			windowname.focus();
		} else if (mode == 'N')	{
			alert("<%=MC.M_SbmtStdFirst_ForMoreStdDet%>");/*alert("Please Submit the <%=LC.Std_Study%> first for entering More <%=LC.Std_Study%> Details");*****/
			return false;
		}
	};

	studyFunctions.callAjaxGetTareaDD = function(formobj){
	   var selval = formobj.studyDivision.value;

 	   new VELOS.ajaxObject("getCodeDropDown.jsp", {
   		urlData:"selectedVal="+selval + "&ddName=studyTArea&codeType=tarea&ddType=child" ,
		   reqType:"POST",
		   outputElement: "span_tarea" }
		).startRequest();
	};
		
	// Bug #8090  Usign AJAX call to check for duplicate study Number
	studyFunctions.jQueryAjaxStudyNum = function (formobj,accId){
		var result;
		var studyNum = formobj.studyNumber.value;
		var studyNumberOld = formobj.studyNumberOld.value;

		if(studyNumberOld==null || studyNum!=studyNumberOld){
			studyNum = encodeURIComponent(studyNum);
			$j.ajax({		  
		   		url: "../validate?module=study:studyNumberId&dataset="+accId+':'+studyNum ,
		   		dataType: "xml",
		   		async: false,
				success: function(o) {
							var r = $j(o).find('valid').text();
							if(r.indexOf("false")>=0){
		   						result = false;
	   						}else {
		   						result = true;
		   					}
						}
		    });
			return result;
		}else{
			return true;
		}
	};

	studyFunctions.fnAddNciNctRow = function(){
		/* if($j('#NCITrialIdentifier').is(":visible")){ 
		   if ($j("#ctrp").is(':checked'))
		     $j("#NCIRow").show();
		   else
		     $j("#NCIRow").hide();
		} */
		 if ($j("#ctrp").is(':checked'))
		     $j("#NCIRow").show();
		   else
		     $j("#NCIRow").hide();
	};


	$j(window).load(function () {
		/* if($j('#NCITrialIdentifier').is(":visible")){ 
			if ($j("#ctrp").is(':checked'))
			     $j("#NCIRow").show();
		} */		
		if ($j("#ctrp").is(':checked'))
		     $j("#NCIRow").show();
	});

	studyFunctions.openwin1 = function(frm) {
		windowName = window.open("usersearchdetails.jsp?fname=&lname=&from="+ frm,"TEST","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
		windowName.focus();
	};

	//=====================CT Study
	
	studyFunctions.ctStudy = function(frm,nctNumber) {
		var mode = jQuery('#mode').val();
		windowName = window.open("ctStudy.jsp?nctNumber="+nctNumber+"&from="+frm+"&mode="+mode+"&keyRt=<%=pageRight%>","TEST","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=1000,height=1000,left=100,top=200");
		windowName.focus();
	};

	jQuery(document).ready( function() {		
		var studyinv_selectLink = document.getElementById('studyinv_selectLink');
		if(studyinv_selectLink){
			$j('#studyinv_selectLink').click(function(){
				studyFunctions.openwin1('studyinv');
			});
		}
	});
</script>

<script type="text/javascript">
jQuery(document).ready( function() {
	<%--if (StudyScreenFlag.equals(LC.Config_StudyScreen_Switch)){ --%>
		studyFunctions.formObj = document.studyScreenForm;
	<%--} else { --%>
		//studyFunctions.formObj = document.study;
	<%--}--%>
	if(jQuery('#mode').val()=='M' && '<%=CFG.CT_LINK%>' !== '' && '<%=CFG.CT_LINK%>' === 'Y'){
	jQuery("#nctNumber").after("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A id='viewCTStudy' HREF='#'>Import from clinicaltrials.gov</A>");
	var viewCTStudy = document.getElementById('viewCTStudy');
	if(viewCTStudy){
		jQuery('#viewCTStudy').click(function(){
			var nctNumber = jQuery("#nctNumber").val()
			studyFunctions.ctStudy('stStudySummary',nctNumber);
			});
		}
	}
	if('<%=nctNumber%>'!='')
		jQuery("#nctNumber").val('<%=nctNumber%>');
	 <%if (TAreaDisSiteFlag.equals(LC.Config_TAreaDisSite_Switch)){%>
		var tAreaFld = document.getElementsByName("studyTArea")[0];
		if (tAreaFld){
			tAreaFld.onchange = function(e){
				var disSitenameFld = document.getElementsByName("disSitename")[0];
				disSitenameFld.value = '';
				var disSiteIdFld = document.getElementsByName("disSiteid")[0];
				disSiteIdFld.value = '';
			};
		}
	<%}%>
});
</script>