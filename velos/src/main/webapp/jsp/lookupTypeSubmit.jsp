<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Field_Lookup%><%-- Field - Lookup*****--%> </title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="formSecJB" scope="request"  class="com.velos.eres.web.formSec.FormSecJB"/>
<jsp:useBean id="formFieldJB" scope="request"  class="com.velos.eres.web.formField.FormFieldJB"/>


<body><%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB, com.velos.eres.business.fieldLib.impl.FieldLibBean,com.velos.eres.service.util.*"%>
<DIV class="popDefault" id="div1"> 

<%
	HttpSession tSession = request.getSession(true);
	String codeStatus = request.getParameter("codeStatus");
	if (sessionmaint.isValidSession(tSession))
	{
		
%>
	<jsp:include page="sessionlogging.jsp" flush="true"/>
   
<%		String eSign= request.getParameter("eSign");
		String oldESign = (String) tSession.getValue("eSign");
	  	 if((codeStatus.equals("WIP")) || ((!codeStatus.equals("WIP") && !codeStatus.equals("")) && (oldESign.equals(eSign)))){
			int errorCode=0;
			String mode=request.getParameter("mode");
			String fieldLibId="";
			
			String fldInstructions="";
			String fldType="M"; //multiple choice
			String fldDataType="ML"; //multiple choice Lookup
		
			String accountId="";
			String fldLibFlag="F";
			String fldBrowserFlg="0" ;
			String fldName="";
			
			// THE STYLE OBJECT WHICH TAKES VALUES OF ATTRIBUTES OF THE FIELD  
			// LIKE BOLD, SAMELINE, ALIGN , ITALICS
			Style aStyle = new Style();
			String formId="";
			String formFldId="";
			String formSecId="";
			String fldMandatory="";
			String fldSeq="";
			String fldAlign="";
			String samLinePrevFld="";
			String fldBold="";
			String fldItalics="";
		
			String creator="";
			String ipAdd="";
			
			String[] keywordarr = null;
			String[] formfldarr = null;
			
			String keywordVal = null;
			String formfldVal = null;
			String lkpView = null;
			String lkpFilter = "";
			String lkpType="";
			
			creator=(String) tSession.getAttribute("userId");
			ipAdd=(String)tSession.getAttribute("ipAdd");
			accountId=(String)tSession.getAttribute("accountId");
			mode=request.getParameter("mode");
			String recordType=mode;  
			formId=request.getParameter("formId");
			fldInstructions=request.getParameter("instructions");
			fldName =  request.getParameter("name");
			formSecId=request.getParameter("section");
			fldSeq=request.getParameter("sequence");
			
			fldAlign=request.getParameter("align");
			samLinePrevFld=request.getParameter("sameLine");
			
			int totrows = 0;
			String lkpDispString = "";
			
			String strTotalRows = request.getParameter("totalrows");
			
			if (strTotalRows == null)
				strTotalRows = "0";
			
			totrows = Integer.parseInt(strTotalRows); //Total Rows
			lkpType=request.getParameter("lkptype");
			lkpType=(lkpType==null)?"":lkpType;
			System.out.println("LkpType in lookupTypeSubmit" + lkpType);
			if (lkpType.equals("A")) lkpView = request.getParameter("dynrepId");
			if (lkpType.equals("L")) lkpView = request.getParameter("lkpview");
						
			System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^LkpType"+lkpType+ " lkpview " + lkpView);
			System.out.println("totrows*" + totrows+ "*");
				
			if (totrows > 1 ) 
			{
					
				
				keywordarr  = request.getParameterValues("viewfld");
				formfldarr  = request.getParameterValues("selLkpFld");
				
				for (int cnt=0;  cnt < totrows; cnt ++)
	        	{
		           	if (!keywordarr[cnt].equals(""))
		            {

			            	lkpDispString = lkpDispString + "~" + formfldarr[cnt] + "|" + keywordarr[cnt];
		            }
		            
		        }
		       
			 //remove first ~	
				if (lkpDispString.length() > 0) 
			 	 lkpDispString = lkpDispString.substring(1);
			 	 			
			}	
		    else if(totrows == 1)
		    {

				keywordVal = request.getParameter("viewfld");
				formfldVal = request.getParameter("assignLkpFld");
				lkpDispString =  formfldVal + "|" + keywordVal;
			}
			
			System.out.println("lkpDispString &&& *" + lkpDispString + "*");
						
			//  We SET THE STYLE WE CHECK ITS VALUES TO BEFORE SENDING TO THE DB
			aStyle.setAlign(fldAlign);

			
						
			if (samLinePrevFld==null)
			aStyle.setSameLine("0");
			else if (samLinePrevFld.equals("1"))
			aStyle.setSameLine("1");
	
			FieldLibBean fieldLsk = new FieldLibBean();
			
			fieldLsk.setFormId(formId );
			
			
			fieldLsk.setFldInstructions(fldInstructions);
			fieldLsk.setFldType(fldType);
			fieldLsk.setIpAdd(ipAdd);
			fieldLsk.setRecordType(recordType);
			fieldLsk.setAccountId(accountId);
			fieldLsk.setLibFlag(fldLibFlag);
			fieldLsk.setFldName(fldName);
			
			fieldLsk.setFldDataType(fldDataType);
			
			fieldLsk.setFormFldBrowserFlg(fldBrowserFlg);
			
			fieldLsk.setFormSecId(formSecId );
			fieldLsk.setFormFldSeq( fldSeq );
			fieldLsk.setFormFldMandatory( fldMandatory );
			fieldLsk.setFldLkpType(lkpType);
			fieldLsk.setAStyle(aStyle);
			
			fieldLsk.setLkpDisplayVal(lkpDispString);
			fieldLsk.setLkpDataVal(lkpFilter);	
			fieldLsk.setLookup(lkpView);

			
			if (mode.equals("N"))
			{
				
				fieldLsk.setCreator(creator);
				errorCode=fieldLibJB.insertToFormField(fieldLsk);
				
			}

			else if (mode.equals("M"))
			{
			
		
				
				formFldId=request.getParameter("formFldId");
				formFieldJB.setFormFieldId(EJBUtil.stringToNum(formFldId));
				formFieldJB.getFormFieldDetails();
				
				fieldLibId=formFieldJB.getFieldId();
				
				
				fieldLibJB.setFieldLibId(EJBUtil.stringToNum(fieldLibId) );
				fieldLibJB.getFieldLibDetails();
				fieldLsk  = fieldLibJB.createFieldLibStateKeeper();
				 	

				
				aStyle.setSameLine(samLinePrevFld);		
				fieldLsk.setFormId(formId );
				fieldLsk.setFormFldId(formFldId );
				fieldLsk.setFldInstructions(fldInstructions);
				fieldLsk.setFldType(fldType);
				fieldLsk.setFldDataType(fldDataType);
				fieldLsk.setIpAdd(ipAdd);
				fieldLsk.setRecordType(recordType);
				fieldLsk.setAccountId(accountId);
				fieldLsk.setLibFlag(fldLibFlag);
				fieldLsk.setFldName(fldName);
				fieldLsk.setFldLkpType(lkpType);

				fieldLsk.setModifiedBy(creator);
				fieldLsk.setFormSecId(formSecId );
				fieldLsk.setFormFldSeq( fldSeq );
				
				fieldLsk.setLkpDisplayVal(lkpDispString);
				fieldLsk.setLkpDataVal(lkpFilter);	
				fieldLsk.setLookup(lkpView);


				fieldLsk.setAStyle(aStyle);
				
				
				errorCode=fieldLibJB.updateToFormField(fieldLsk);
				
						
		 }
		
		
		if ( errorCode == -2 || errorCode == -3 )
		{
%>
  	
<%             if (errorCode == -3 )
				{
%>
				
				<br><br><br><br><br>
				<p class = "successfulmsg" > <%=MC.M_FldIdExst_EtrDiff%><%-- The Field ID already exists. Please enter a different Field ID*****--%></p>
				<button tabindex=2 onClick="history.go(-1);"><%=LC.L_Back%></button>
				
				
<%
				} else 
%>				
				<br><br><br><br><br><p class = "successfulmsg" align = center>		
	 			<p class = "successfulmsg" ><%=MC.M_Data_NotSvdSucc%><%-- Data not saved successfullyl*****--%> </p>

<%
		
		} //end of if for Error Code

		else

		{
%>	
		
<%
		
%>		

		<br><br><br><br><br>
		<p class = "successfulmsg" align = center><%=MC.M_Data_SavedSucc%><%-- Data Saved Successfully*****--%> </p>
		<script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
		</script>		
			
<%
			}//end of else for Error Code Msg Display
		}//end of if old esign
	else{ 
		%>
	<jsp:include page="incorrectesign.jsp" flush="true"/>
		<%	  
		}//end of else of incorrect of esign */
	 }//end of if body for session 	
			
			else
	{

%>
		  <jsp:include page="timeout.html" flush="true"/>
<%
	
} //end of else of the Session 
 

%>

</body>

</html>
