<%@page import="com.velos.eres.service.util.StringUtil"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
  <jsp:useBean id="statusB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <jsp:useBean id="InvB" scope="request" class="com.velos.eres.web.invoice.InvoiceJB"/>
  <jsp:useBean id="StudyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
  <jsp:useBean id="StatusHistoryB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB" />
  <%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.EJBUtil, com.velos.eres.business.common.*,com.velos.eres.service.util.MC"%>
  <%@ page language="java" import="com.velos.eres.service.util.DateUtil,java.util.Calendar,java.util.Date,java.time.LocalTime,java.time.format.DateTimeFormatter" %>
  <%
  	String src = null;
  	src = request.getParameter("srcmenu");
  	String eSign = request.getParameter("eSign");
  	String mode = request.getParameter("mode");
  	String moduleTable = request.getParameter("moduleTable");
  	String modulePk = request.getParameter("modulePk");
  	String status = request.getParameter("status");
  	String parentNtwIdlePk = request.getParameter("parentNtwId");
  	
  	String fromPage=request.getParameter("fromPage")==null?"":request.getParameter("fromPage");
  	int studyId = 0;
  	if(request.getParameter("studyId")!=null){
  		studyId = EJBUtil.stringToNum(request.getParameter("studyId"));
  	}
  	String fullVerNo ="";
  	if(request.getParameter("fullVerNo")!=null){
  		fullVerNo = request.getParameter("fullVerNo");
  		System.out.println("Incoming fullVerNo==="+fullVerNo);
  	}

  	if(mode.equals("M"))
  	{
  		status = request.getParameter("statCode");
  	}

  	
  	ArrayList subTypStats =null;
  	CodeDao cdao = new CodeDao();
  	cdao.getCodeValuesById(EJBUtil.stringToNum(status));
  	subTypStats = (cdao.getCSubType());
  	String subTypStat = subTypStats.get(0).toString();
  	String statusDate = request.getParameter("statusDate");
  	String notes = request.getParameter("notes");
  	
  	// Added by Aakriti for Bug#30837 - Network Status History and Network User Role Status History should include timestamp.
  	String prevStatusDate = statusB.getStatusStartDate();
  	if (mode.equals("M") && prevStatusDate!=null) {
  		prevStatusDate = prevStatusDate.substring(0, prevStatusDate.indexOf(" "));
  	}

  	if (mode.equals("N") || (mode.equals("M") && !statusDate.equals(prevStatusDate))) {
  		statusDate = statusDate + " " + StatusHistoryDao.getCurrTime();
  	}
  	// Code of Bug#30837 ends here

  	if (EJBUtil.isEmpty(mode))
  		mode = "N";

  	HttpSession tSession = request.getSession(true);

  	if (sessionmaint.isValidSession(tSession))

  	{
  %>
     <jsp:include page="sessionlogging.jsp" flush="true"/>

     <%

   	String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign))
	{
%>
  	   <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	}
	else
  	{
		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");

%>

<%
	if(mode.equals("M"))
	{
	        String statusId = request.getParameter("statusId");


	 		statusB.setStatusId(EJBUtil.stringToNum(statusId));
			statusB.getStatusHistoryDetails();
			StudyApndxDao studyApndxDao = new StudyApndxDao();
			
	}

			statusB.setStatusModuleId(modulePk);
    		statusB.setStatusModuleTable(moduleTable);
    		statusB.setStatusCodelstId(status);

    		if (moduleTable.equals("er_invoice")){
    		InvB.setId(EJBUtil.stringToNum(modulePk));
    		InvB.getInvoiceDetails();
    		InvB.setInvStat(subTypStat);
    		InvB.setCreator(usr);
    		InvB.updateInvoice();
    		statusB.setIsCurrentStat("1");
    		statusB.setRecordType("N");
    		}

    		if(mode.equals("N") || (mode.equals("M") && !statusDate.equals(prevStatusDate))){
    			statusB.setStatusStartDate(statusDate);
    		}else{
    			statusB.setStatusStartDate(statusB.getStatusStartDate());
    		}
    		statusB.setStatusNotes(notes);
    		statusB.setIpAdd(ipAdd);
    if(moduleTable.equals("er_nwsites") || moduleTable.equals("er_studynetwork") || moduleTable.equals("er_nwusers") || moduleTable.equals("er_nwusers_addnlroles")){

    	if(mode.equals("N")){
    		statusB.setIsCurrentStat("1");
    		statusB.setRecordType("N");
    		if(moduleTable.equals("er_nwusers")||moduleTable.equals("er_nwusers_addnlroles")){
    			NetworkDao nwdao = new NetworkDao();
    			nwdao.updateUserNetworkUserStatus(StringUtil.stringToInteger(modulePk),StringUtil.stringToInteger(status),moduleTable,StringUtil.stringToInteger(usr));
    		}
    	}else{
    		statusB.setRecordType("M");
    	}
    }

	if(mode.equals("N"))
	{
    		statusB.setCreator(usr);
    		statusB.setModifiedBy(usr);
    		statusB.setStatusHistoryDetails();
    		if(moduleTable.equals("er_studyver")){
    			int statusId = statusB.getStatusId();
    			CodeDao approvedStat = new CodeDao();
    			System.out.println("statusId--->"+statusId);
    			if(EJBUtil.stringToNum(status)==approvedStat.getCodeId("versionStatus","A")){
    			
    			StudyB.setId(studyId);
    			StudyB.getStudyDetails();
    			String approvalDate = "";
    			if(request.getParameter("statusDate")!=null){
    				approvalDate = request.getParameter("statusDate");
    			}
    			
    			StudyApndxDao studyApndxDao = new StudyApndxDao();
    			studyApndxDao.addStampingforApproved(EJBUtil.stringToNum(modulePk),StudyB.getStudyNumber(),approvalDate,String.valueOf(statusId));
    			}
    		}

	}	//mode = N
	else if (mode.equals("M"))
	{
    		statusB.setModifiedBy(usr);
       		statusB.updateStatusHistory();

	}
	
	if("networkTab".equals(fromPage) && mode.equals("N") && moduleTable.equals("er_nwsites")){
		NetworkDao ntDao=new NetworkDao();
		/*For 30712 - Network Status should not propagate from parent to children*/
		ntDao.saveNetworkStatusUpdate(StringUtil.stringToInteger(modulePk),StringUtil.stringToInteger(status),StringUtil.stringToInteger(usr));
		//ntDao.saveNetworkStatus(StringUtil.stringToInteger(modulePk),StringUtil.stringToInteger(status),statusDate,notes);
    	}
	else if("StudyNetworkTab".equals(fromPage) && mode.equals("N") && moduleTable.equals("er_studynetwork")){
		NetworkDao ntDao=new NetworkDao();
		ntDao.updateStudyNetworkStatus(StringUtil.stringToInteger(modulePk), StringUtil.stringToInteger(status), StringUtil.stringToInteger(usr),studyId);
	}
	
	if(mode.equals("N") && moduleTable.equals("er_studyver")){
		String latestStat=StatusHistoryB.getLatestStatus(EJBUtil.stringToNum(modulePk),moduleTable);
		StudyVerDao svDao = new StudyVerDao();
		System.out.println("status====="+status);
		CodeDao tmpCode = new CodeDao();
		String statusSubType=tmpCode.getCodeSubtype(EJBUtil.stringToNum(status));
		System.out.println("latestStat======"+latestStat);
		System.out.println("statusSubType====="+statusSubType);
		if(latestStat.equals(statusSubType)){
			System.out.println("Inside If-----"+fullVerNo);
			System.out.println("Inside If-----"+studyId);
			System.out.println("Inside If-----"+EJBUtil.stringToNum(modulePk));
			System.out.println("Update return===="+svDao.updateVersionStatus(fullVerNo,studyId,EJBUtil.stringToNum(modulePk),statusSubType,Integer.parseInt(usr)));
		}
	}
	%>
	  <br>
      <br>
      <br>
      <br>
      <br>
     <p class = "successfulmsg custom-successfulmsg" align = center> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p>
     <%if("networkTab".equals(fromPage)&&(mode.equals("M"))) {%>
     <script>
     setTimeout("self.close()",1000);
     //window.onunload = refreshParent;
     /* function refreshParent() {
         window.opener.location.reload();
     } */
     <%-- window.opener.fetchNetworkRows('<%=parentNtwIdlePk%>'); --%>
	  </script>
     <%} 
      else if("networkTab".equals(fromPage)) {%>
      <script>
      setTimeout("self.close()",1000);
      window.opener.fetchNetworkRows('<%=parentNtwIdlePk%>');
 	  </script>
      <%}
      else if("StudyNetworkTab".equals(fromPage)) {%>
      <script>
      setTimeout("self.close()",1000);
      window.opener.fetchNetworkRows();
 	  </script>
      <%}
     else{%>
	  <script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
	  </script>
<%} %>
<%
}//end of if for eSign check











}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>
