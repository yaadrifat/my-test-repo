<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page language = "java" import = "com.velos.eres.web.user.UserJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*"%>
<%@ page import="com.velos.eres.widget.business.common.UIFlxPageDao" %>
	<%@ page import="com.velos.eres.compliance.web.ProtocolChangeLog"%>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<script>
var historyWin;
function openHistoryWin(tabsubtype,study,pkSubmission,submissionBoardPK,appSubmissionType) {
	var w = screen.availWidth-100;
	historyWin = window.open("irbhistory.jsp?appSubmissionType="+appSubmissionType+"&tabsubtype="+tabsubtype+"&studyId="+study+"&submissionPK="+pkSubmission+"&submissionBoardPK="+ submissionBoardPK,"historyWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=100");
	historyWin.focus();
}
function openActionWin(studyId, submissionPK, submissionBoardPK, IRBSelectedTab, pksubmissionStatus) {
    var actionWin = window.open("irbactionwin.jsp?&studyId="+studyId+"&submissionPK="+submissionPK+"&submissionBoardPK="+submissionBoardPK+"&selectedTab="+IRBSelectedTab+"&pksubmissionStatus="+pksubmissionStatus,"actionWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=900,height=700,left=10,top=100");
	if (!actionWin) { actionWin.focus(); }
}
function reloadIrbForms() {
	if (top != null && top.reloadOpener != undefined) {
		top.reloadOpener();
	}
}

</script>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>

<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
		 
		 <% 
		 
		 HttpSession tSession = request.getSession(true); 
 
		if (sessionmaint.isValidSession(tSession)) {
	
	  	 ArrayList arrFrmNames = null;
		 ArrayList arrNumEntries = null;
 		 ArrayList arrFrmIds = null;
		 ArrayList arrEntryChar = null;
		 ArrayList arrFrmInfo = new ArrayList();
		 String frmName = "";
		 String entryChar="";
		 String numEntries = "";
		 String frmInfo= "";
		 int formId = 0;
		 String firstFormInfo = "";
	
	 	 boolean isIrb = false;
		 
		 String strFormId = request.getParameter("formPullDown");	 
		 String formCategory = request.getParameter("formCategory");	 
		 String submissionType = request.getParameter("submissionType");	 
		 String calledFromForm = request.getParameter("calledFromForm");	 
		 String targateframe =  request.getParameter("target");	 
		 String studyId = StringUtil.htmlEncodeXss(request.getParameter("studyId"));
		 String tabsubtype = StringUtil.htmlEncodeXss(request.getParameter("tabsubtype"));
		 String appSubmissionType = StringUtil.htmlEncodeXss(request.getParameter("appSubmissionType"));
		 String submissionPK = StringUtil.htmlEncodeXss(request.getParameter("submissionPK"));
		 String submissionBoardPK = StringUtil.htmlEncodeXss(request.getParameter("submissionBoardPK"));
		 String pksubmissionStatus = StringUtil.htmlEncodeXss(request.getParameter("pksubmissionStatus"));
		 String formSubmissionType="";
		 int study_acc_form_right = EJBUtil.stringToNum(request.getParameter("study_acc_form_right"));	 
		 int study_team_form_access_right = EJBUtil.stringToNum(request.getParameter("study_team_form_access_right"));	 
		 
		 String accId=(String)tSession.getAttribute("accountId");
		 int iaccId=EJBUtil.stringToNum(accId);
		 
 	     String userId = (String) tSession.getValue("userId");
	     UserJB userB = (UserJB) tSession.getValue("currentUser");
   	     String siteId = userB.getUserSiteId();
	 	 
	 	 int istudyId = EJBUtil.stringToNum(studyId);
	 	String reviewtypdesc = StringUtil.htmlEncodeXss(request.getParameter("reviewtypdesc"));
	 	String tabSubTypeForApplicationForm=""; 
	 	 System.out.println("formSubmissionType"+formSubmissionType+"tabSubTypeForApplicationForm"+tabSubTypeForApplicationForm+"reviewtypdesc"+reviewtypdesc);
	 	if(reviewtypdesc==null){
	 		reviewtypdesc="";
	 	}
	 	if(tabsubtype.equalsIgnoreCase("irb_pend_rev")){
	 	   	if(reviewtypdesc.equals("rev_full"))
	 	   	{
	 	   		tabSubTypeForApplicationForm="irb_revfull_tab";	
	 	   	}
	 	   	else if(reviewtypdesc.equals("rev_exp")){
	 	   		tabSubTypeForApplicationForm="irb_revexp_tab";
	 	   	}
	 	   	else if(reviewtypdesc.equals("rev_exe")){
	 	   		tabSubTypeForApplicationForm="irb_revexem_tab";
	 	   	}
	 	   	else if(reviewtypdesc.equals("rev_anc")){
	 	   		tabSubTypeForApplicationForm="irb_revanc_tab";
	 	   	}	
	 	   }
			 
	 	if(tabsubtype.equalsIgnoreCase("irb_pend_rev")){
	 		isIrb=true;
	 		formSubmissionType = EIRBDao.getIRBFormSubmissionTypeSQL(tabSubTypeForApplicationForm,"irb_check4",appSubmissionType);
	 	}
	 	
		 if (! StringUtil.isEmpty(formCategory) && ! StringUtil.isEmpty(submissionType))
		 {
		 	 isIrb=true;
		 }
		 
		 if ("undefined".equals(tabsubtype)) {
		     isIrb=false;
		 }
		 
		 LinkedFormsDao lnkFrmDao = new LinkedFormsDao();
		 if (isIrb) {
			 if(tabsubtype.equalsIgnoreCase("irb_pend_rev")){
		 lnkFrmDao = lnkformB.getStudyForms(iaccId,istudyId, EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId), 
		         study_acc_form_right,  study_team_form_access_right, 
		         isIrb, formSubmissionType, "irb_check4");
			 }
			 else{
			 
				 lnkFrmDao = lnkformB.getStudyForms(iaccId,istudyId, EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId), 
				         study_acc_form_right,  study_team_form_access_right, 
				         isIrb, submissionType, formCategory);
			 } }
	 
		 arrFrmIds = lnkFrmDao.getFormId();		 
		 arrFrmNames = lnkFrmDao.getFormName();
		 arrEntryChar = lnkFrmDao.getFormEntryChar();
		 arrNumEntries = lnkFrmDao.getFormSavedEntriesNum();
		
		 
		
		 if (arrFrmIds.size() > 0) { 	
    		 if (strFormId==null) {
    		 	formId = EJBUtil.stringToNum(arrFrmIds.get(0).toString());
    			entryChar = arrEntryChar.get(0).toString();
    			numEntries = arrNumEntries.get(0).toString();		   
    			firstFormInfo = arrFrmIds.get(0).toString() + "*"+ entryChar + "*" + arrNumEntries.get(0).toString();
    		 }
    		 else {
    		 	 StringTokenizer strTokenizer = new StringTokenizer(strFormId,"*");
    		 	 if (calledFromForm.equals(""))
    	 		  
    			 	formId = EJBUtil.stringToNum(strTokenizer.nextToken());
    			 	entryChar = strTokenizer.nextToken();
    			 	
    			 	lnkformB.findByFormId(formId );
		   		 	numEntries = lnkformB.getLfDataCnt();
    			 	//get the number of times the form was answered
    			 	//prepare strFormId again
    			 	strFormId = formId + "*" + entryChar + "*" + numEntries;
    			 	firstFormInfo = strFormId;	   
    			 
    			 
    		 }
    		 
    		 
    		 for (int i=0;i<arrFrmIds.size();i++)
    		 {  //store the formId, entryChar and num Entries separated with a *
    		  	 frmInfo = arrFrmIds.get(i).toString() + "*"+ arrEntryChar.get(i).toString() + "*" + arrNumEntries.get(i).toString();
    		 	 arrFrmInfo.add(frmInfo);		 
    		 }
    		 		
    	}		 
    	 	 String dformPullDown = EJBUtil.createPullDownWithStrNoSelect("formPullDown",firstFormInfo, arrFrmInfo , arrFrmNames);
    	 	
    	 	StringBuffer formBuffer = new StringBuffer(dformPullDown);

			 //submit the form when the user selects the form name
			 
			 formBuffer.replace(0,7,"<SELECT onChange=\"document.studyform.submit();\"");
			 
			  
			 dformPullDown = formBuffer.toString();
			 
			    String studyNumber=null;
			    String studyTitle=null;
			    String versionNum = "0";
			    String callingjsp=request.getParameter("callingjsp")==null?"":request.getParameter("callingjsp");
			    
			    if(!studyId.equals("")){
					studyB.setId(EJBUtil.stringToNum(studyId));
					studyB.getStudyDetails();
					studyNumber = studyB.getStudyNumber();
					studyTitle = studyB.getStudyTitle();
					UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
					uiFlxPageDao.getHighestFlexPageFullVersion("er_study", Integer.parseInt(studyId));

					int versionMajor = uiFlxPageDao.getMajorVer();
					versionNum = String.valueOf(versionMajor);
					if (versionMajor > 0){
						versionNum += ".";
						int versionMinor = uiFlxPageDao.getMinorVer();
						versionNum += (versionMinor == 0)? "00" : ((versionMinor > 9)? versionMinor : "0" + versionMinor);
					}
			    }
			 
			    ProtocolChangeLog protocolChangeLog = new ProtocolChangeLog();
			    String changeVersion = protocolChangeLog.getAllProtocolVersions(StringUtil.stringToNum(studyId), (String) tSession.getValue("defUserGroup"));
    	 	%>
    	 	<script language="javascript">
    	 	function f_open_version(){
    	 		var origVer = $j('#versionCount').val();
    	 		if(document.getElementById("versionDiv")==null)
    	 			{
    	 			var versionDiv = document.createElement('div');
    	 			versionDiv.id = 'versionDiv';
    	 			versionDiv.name = 'versionDiv';
    	 			versionDiv.style.display = 'none';
    	 			versionDiv.style.overflowY = 'auto';
    	 			versionDiv.style.height = 500;
    	 			versionDiv.innerHTML="<%=changeVersion%>";
    	 			(document.getElementsByTagName("form")[0]).appendChild(versionDiv);
    	 			}
    	 			
    	 		var versionCount = "<%=versionNum%>";
    	 		if(versionCount>0){
    	 			$j("#versionDiv").dialog({
    	 				close: function()  { $j('#versionCount').val(origVer); },
    	 				minHeight: 600,
    	 				minWidth: 500,
    	 				maxHeight: 600,
    	 				maxWidth: 500,
    	 		        modal: true,
    	 		        draggable: true,
    	 		        resizable: false,
    	 		        closeOnEscape: true,
    	 		        //stack: true,
    	 		        title: 'Versions: ',
    	 		        position: ['center', 'center'],
    	 		        show: 'blind',
    	 		        hide: 'blind',
    	 		        width: 600,
    	 		        height: 500,
    	 		        dialogClass: 'ui-dialog-osx',
    	 		        buttons: {
    	 		            "Close": function() {
    	 		                $j(this).dialog("close");
    	 		            }
    	 		        }
    	 		    });
    	 		}
    	 	}
    	 	function f_open_versionDetails(node) {
    	 		if(document.getElementById("versionDetailsDiv")==null)
    	 		{
    	 		 var versionDetailsDiv = document.createElement('div');
    	 		  versionDetailsDiv.id = 'versionDetailsDiv';
    	 		  versionDetailsDiv.name = 'versionDetailsDiv';
    	 		  versionDetailsDiv.style.overflowY = 'scroll';
    	 		  (document.getElementsByTagName("form")[0]).appendChild(versionDetailsDiv);
    	 		  versionDetailsDiv.style.display = 'none';
    	 		}
    	 		$j("#versionDetailsDiv").html('<p><%=LC.L_Loading%>...</p>');
    	 			$j("#versionDetailsDiv").dialog({
    	 					minHeight: 600,
    	 					minWidth: 750,
    	 					maxHeight: 600,
    	 					maxWidth: 750,
    	 					modal: false,
    	 			        //stack: true,
    	 			        draggable: false,
    	 			        resizable: true,
    	 			        closeOnEscape: true,
    	 			        title: 'Version Details: ',
    	 			        position: ['center', 'center'],
    	 			        show: 'blind',
    	 			        hide: 'blind',
    	 			        width: 600,
    	 			        height: 500,
    	 			        dialogClass: 'ui-dialog-osx',
    	 			        open :  function () {
    	 			         	$j(this).load(node);
    	 			        },		       
    	 			        buttons: {
    	 			            "Close": function() {
    	 			                $j(this).dialog("close");
    	 			            }
    	 			        }
    	 			    });
    	 	}

    	 	function f_open_versionChangeLog(node) {
    	 		if(document.getElementById("versionChangeLogDiv")==null)
    	 		{
    	 		 var versionChangeLogDiv = document.createElement('div');
    	 		 versionChangeLogDiv.id = 'versionChangeLogDiv';
    	 		 versionChangeLogDiv.name = 'versionChangeLogDiv';
    	 		 versionChangeLogDiv.style.overflowY = 'scroll';
    	 		  (document.getElementsByTagName("form")[0]).appendChild(versionChangeLogDiv);
    	 		  versionChangeLogDiv.style.display = 'none';
    	 		}
    	 		$j("#versionChangeLogDiv").html('<p><%=LC.L_Loading%>...</p>');
    	 			$j("#versionChangeLogDiv").dialog({
    	 					minHeight: 600,
    	 					minWidth:  750,
    	 					maxHeight: 600,
    	 					maxWidth:  750,
    	 					modal: false,
    	 			        //stack: true,
    	 			        draggable: false,
    	 			        resizable: true,
    	 			        closeOnEscape: true,
    	 			        title: 'Change Log: ',
    	 			        position: ['center', 'center'],
    	 			        show: 'blind',
    	 			        hide: 'blind',
    	 			        width:  750,
    	 			        height: 500,
    	 			        dialogClass: 'ui-dialog-osx',
    	 			        open :  function () {
    	 			         	$j(this).load(node);
    	 			        },		       
    	 			        buttons: {
    	 			            "Close": function() {
    	 			                $j(this).dialog("close");
    	 			            }
    	 			        }
    	 			    });
    	 	}
    	 	function openAttachmentHistory(studyId){
    	 		var w = screen.availWidth-100;
    	 		var attWindow = window.open("studyVerBrowser.jsp?mode=N&isPopupWin=1&studyId="+studyId,"attWindow","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=1280,height=500,left=100,top=100");
    	 		attWindow.focus();
    	 	}
    	 	</script>
    	 		<form action="formfilledstudybrowser.jsp" method="POST" name="studyform" id="studyform" target="<%=targateframe%>">
                <div id="ddOpt" style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:11">
    	 		<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
				<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
				<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
    	 		
    	 		<script language=javascript>
					var study_Title = htmlEncode('<%=studyTitle%>');
				</script>
    	 		<table>
    	 		<% if(callingjsp.equalsIgnoreCase("irbformFrame")){ %>
    	 		<tr>
					<td><h5>
						<%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>:<%=studyNumber %>&nbsp;&nbsp;
						<%=LC.L_Version%><%--Version*****--%>:<%=versionNum %>&nbsp;&nbsp;
						<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>:<img src="./images/View.gif" onmouseover="return overlib(study_Title,CAPTION,'<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>' ,RIGHT , BELOW );" onmouseout="return nd();" > 
					</h5></td>
				</tr>
				<%} %>
    	 		<tr><td><p class="sectionHeadings"><%=LC.L_Select_AForm%><%--Select a Form*****--%>: <%=dformPullDown%></p></td>
    	 		<td>&nbsp;<button type="submit" name="goButton" value="<%=LC.L_Go%><%--Go--%>"><%=LC.L_Go%><%--Go--%></button></td><td>&nbsp;</td>
            <%  
			    Hashtable htIRBParams = (Hashtable)tSession.getAttribute("IRBParams");
                if (htIRBParams != null) {
                    if (submissionPK == null) {
                        submissionPK = (String) htIRBParams.get("submissionPK");
                    }
                    if (submissionBoardPK == null) {
    					submissionBoardPK = (String) htIRBParams.get("submissionBoardPK");
                    }
                    if (tabsubtype == null) {
                        tabsubtype = (String) htIRBParams.get("selectedTab");
                    }
                    if(pksubmissionStatus == null){
                    	pksubmissionStatus = (String) htIRBParams.get("pksubmissionStatus");
                    }
                }
                if (tabsubtype != null && EIRBDao.isTabForActionWindow(tabsubtype)) { %>
                <td align="center" style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:10">
                <a title="<%=LC.L_ShowSub_History%><%--Show Submission History*****--%>" href="javascript:void(0);"
                  onclick="openHistoryWin('<%=tabsubtype%>',<%=studyId%>,<%=submissionPK%>,
                           <%=submissionBoardPK%>,'<%=appSubmissionType%>')"><%=LC.L_Sub_Hist%><%--Submission<br/>History*****--%></a>
                </td>
                <td>&nbsp;</td>
                <td align="center" style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:10">
                <a href="javascript:void(0);" onclick="openActionWin(<%=studyId%>,<%=submissionPK%>,
                           <%=submissionBoardPK%>,'<%=tabsubtype%>','<%=pksubmissionStatus%>')"><%=LC.L_Act_Page%><%--Action<br/>Page*****--%></a>
                </td>
                <% if("LIND".equals(CFG.EIRB_MODE)){%>
                <td>&nbsp;</td>
                <td align="center" style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:10">
                <a href="javascript:void(0);" onclick="f_open_version()"><%=LC.L_Ver_History%><%--Action<br/>Page*****--%></a>
                </td>
                <td>&nbsp;</td>
                <td align="center" style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:10">
                <a href="javascript:void(0);" onclick="openAttachmentHistory(<%=studyId%>)"><%=LC.L_Att_History%><%--Action<br/>Page*****--%></a>
                </td>
                <% } %>
            <%  }  %>
    	 		</tr></table>
                </div>
    	 		
    	 		 
    	 			
		    	 	<input type="hidden" name="studyId" value=<%=studyId%>>				
		    		<input type="hidden" name="calledFromForm" value=<%=calledFromForm%>>
		    		
		    		<input type="hidden" name="showPanel" value=false>
		    			
		    		<input type="hidden" name="formCategory" value=<%=formCategory%>>
		    		<input type="hidden" name="submissionType" value=<%=submissionType%>>
		    		<input type="hidden" name="irbReviewForm" value="true">
		    	
    	
    	 		</form>
    	 		
    	 		<% 
    		} //end of session check 
    		
    		
    		 %>