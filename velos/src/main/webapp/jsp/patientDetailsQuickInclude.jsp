<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>
<%@ page import="org.json.*" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<script src="js/velos/patsearch.js" type="text/javascript"></script>
<%
HttpSession tSession = request.getSession(true);
if (!sessionmaint.isValidSession(tSession))
	return;
String defaultPatId = request.getParameter("defaultPatId");
String defaultFacility = request.getParameter("defaultFac");
String defaultStudyPK = request.getParameter("defaultStudyPK");
String defaultStudyName = request.getParameter("defaultStudyName");
%>

<script type="text/javascript">
var patientDetailsQuickFunctions = {
	formObj: {},

	setDIVContent: {},
	fetchData: {},
	toggleOrg: {},
	startDate1: {},
	openSpecialityWindow: {},
	openEthnicityWindow: {},
	openRaceWindow: {},
	openwin1: {},
	asyncValidate: {},
	updateDemoFromJSON: {},

	validate: {},
	autoGenPatient: {},
	defaultStudyPK: <%=defaultStudyPK%>,
	defaultStudyName: "<%=defaultStudyName%>",
	defaultFacility: "<%=defaultFacility%>",
	defaultPatId: "<%=defaultPatId%>"
};
</script>

<script type="text/javascript">
jQuery(document).ready( function() {
	patientDetailsQuickFunctions.formObj = document.patientEnrollForm;

	patientDetailsQuickFunctions.toggleOrg();
	
	<%if (defaultStudyPK != null){%>
		if (patientDetailsQuickFunctions.defaultStudyPK){
			document.getElementById("selectStudyLabel").style.visibility = "hidden";
			patientDetailsQuickFunctions.formObj.selstudyId.style.visibility = "hidden";
			$("studyNumberMessage").innerHTML="<%=MC.M_AddPat_Std%>/*Adding <%=LC.Pat_Patient_Lower%> for <%=LC.Std_Study%>*****/"+": '" + patientDetailsQuickFunctions.defaultStudyName + "'";
			$("studyDIV").style.visibility = "visible";
			$("studyDIV").style.height="15px";
			updateSourceField(patientDetailsQuickFunctions.formObj.selstudyId, patientDetailsQuickFunctions.defaultStudyPK);
		}
	<%} //end study fcondution%>
	if (patientDetailsQuickFunctions.defaultFacility){
		updateSourceField(patientDetailsQuickFunctions.formObj.patorganization, patientDetailsQuickFunctions.defaultFacility);
	}
	
	if (patientDetailsQuickFunctions.defaultPatId){
		updateSourceField(patientDetailsQuickFunctions.formObj.patid, patientDetailsQuickFunctions.defaultPatId);
	}

	<%//if a remotePatNum got sent in, we're going to do a bunch of work
	String remotePatStr = request.getParameter("remotePatSearchIndex");

	if (remotePatStr != null){
		//can't use EJBUtil.stringToNum to parse this because that
		//method will return 0 for a parse error, which is a valid index number.
		int remotePatInt = -1;
		try{
			remotePatInt = Integer.parseInt(remotePatStr);
		}
		catch(NumberFormatException e){
			e.printStackTrace();
		}
	%>
		<%if (remotePatInt > -1){%>
			<%//function added 1/22/2010 by DRM
			//Updates fields in this page to reflect patient selected on another page.
			//Remote serach in patientSearchWorkflowJSON.jsp adds a JSONArray of patients
			//to the session. If the request contains the remotePatSearchIndex parameter, then
			//the page that called this page (for example patientSearchWorkflow.jsp) wants this
			//page to default patient demographics fields from the patList array.
		
			JSONArray patList = (JSONArray)tSession.getAttribute("jsPatList");
			JSONObject jsPatient = null;
		
			if (patList != null){
				//tSession.removeAttribute("jsPatList");
				jsPatient = patList.getJSONObject(remotePatInt);
			}%>
	
			var jsPat = <%=jsPatient.toString()%>
			if (jsPat){
				//alert(jsPat);
				patientDetailsQuickFunctions.updateDemoFromJSON(jsPat);
			}
		<%} %>
	<%} %>
});
</script>

<SCRIPT LANGUAGE="JavaScript">
var dd1, delay;

patientDetailsQuickFunctions.setDIVContent = function(){
	var msg = "<%=MC.M_FetchingData_PlsWait%><%--Fetching Data,Please Wait...*****--%>";
	$("wsDIV").innerHTML="<DIV id='indicator' style='float:left;width:100%;top:5%'><img width='5%' src='../images/indicator.gif' align='absmiddle'>&nbsp;<font size='2'>"+msg+"</font></DIV>";
}

patientDetailsQuickFunctions.fetchData = function(){
	org=$("patientEnrollForm").orgDD;
	var orgDisp=org.options[org.selectedIndex].text;
	//orgId=org.options[org.selectedIndex].text;
	orgId=org.value;
	pat=document.getElementById("patid");
	if ((pat.value.length==0) || (orgId.length==0))
	{
		alert("<%=MC.M_PatId_OrgMndt%>");/*alert("Patient Id & Organization both are mandatory.Please fix and try again.");*****/
		pat.focus();
		return false;
	}
	patientDetailsQuickFunctions.setDIVContent();
	site=$("siteId");
	site.value=org.value;
	$("wsDIV").style.visibility="visible";
	$j.ajax({
		type: "POST",
		url: "/velos/jsp/wsresultDemo.jsp",
		data: { MRN: pat.value, hCFacility: orgId },
		success :  function(transport) {
			
				document.getElementById("trans").style.visibility="hidden";
			 	document.getElementById("wsDIV").innerHTML=transport;
			  	var fname=document.getElementById("wsPatFName").value;
			 	var lname=document.getElementById("wsPatLName").value
		
			  	if (fname.length>0) {
			  	fname=fname=fname.replace("'","`");
		
			 	}
			 	if (lname.length>0) {
			 		lname=lname.replace("'","`");
		
			 	}

			 	updateSourceField(document.getElementById("patfname"), fname);
			 	updateSourceField(document.getElementById("patlname"), lname);
			 	updateSourceField(document.getElementById("patdob"),document.getElementById("wsPatDOB").value);
			 	updateSourceField(document.getElementById("otherdthCause"), document.getElementById("wsPatCauseOfDeathOther").value);
		
			 	updateSourceField(patientDetailsQuickFunctions.formObj.patrace, dereferenceCode(document.getElementById("wsPatRace").value, "race"));
				updateSourceField(patientDetailsQuickFunctions.formObj.patethnicity, dereferenceCode(document.getElementById("wsPatEthnicity").value, "ethnicity"));
				updateSourceField(patientDetailsQuickFunctions.formObj.patsex, dereferenceCode(document.getElementById("wsPatSex").value, "gender"));
		
				updateSourceField(document.getElementById('aliasname'),pat.value);
				updateSourceField(document.getElementById('patadd1'),document.getElementById("wsPatAddress1").value);
				updateSourceField(document.getElementById('patadd2'),document.getElementById("wsPatAddress2").value);
				updateSourceField(document.getElementById('patcity'),document.getElementById("wsPatCity").value);
				updateSourceField(document.getElementById('patstate'),document.getElementById("wsPatState").value);
				updateSourceField(document.getElementById('patcounty'),document.getElementById("wsPatCounty").value);
				updateSourceField(document.getElementById('patcountry'),document.getElementById("wsPatCountry").value);
				updateSourceField(document.getElementById('patzip'),document.getElementById("wsPatZip").value);
				if(document.getElementById("wsPatId")){
			  	if (document.getElementById("wsPatId").value.length>0) {
					document.getElementById("trans").style.visibility="hidden";
					 //Select the organization for botton DD based on what was selected above
				  	for (var i=0;i<site.options.length;i++)
					{
						if (site.options[i].text==orgDisp)
				 		{
				  			site.selectedIndex=i;
				  			break;
				 		}
					}
					//end selecting organization
			 		}
				}
		}
	});
};

patientDetailsQuickFunctions.toggleOrg = function(){
	if (!document.getElementById("trans")){
		return;
	}
	if (!document.getElementById("orgDD")){
		document.getElementById("trans").style.visibility="hidden";
		return;
	}

	if ((document.getElementById("orgDD").value=='0')){
		document.getElementById("trans").style.visibility="hidden";
		document.getElementById("getPatientAnchor").style.visibility="hidden";
		$("wsDIV").style.visibility="hidden";
	}
	else{
		document.getElementById("trans").style.visibility="visible";
		document.getElementById("getPatientAnchor").style.visibility="visible";
	}
};

patientDetailsQuickFunctions.startDate1 = function(delay1) {
  var adate, date, amonth;
  delay = delay1;
  adate = new Date();
  date = adate.getDate();
  amonth = adate.getMonth()+1;

  if (amonth == 1) date += " January";
  else if (amonth == 2) date += " February";
  else if (amonth == 3) date += " March";
  else if (amonth == 4) date += " April";
  else if (amonth == 5) date += " May";
  else if (amonth == 6) date += " June";
  else if (amonth == 7) date += " July";
  else if (amonth == 8) date += " August";
  else if (amonth == 9) date += " September";
  else if (amonth == 10) date += " October";
  else if (amonth == 11) date += " November";
  else if (amonth == 12) date += " December";
  date += " " + adate.getFullYear();
  document.atime21.date.value = date;
  dd1 = setTimeout("patientDetailsQuickFunctions.startDate1(delay)",delay1);
};

patientDetailsQuickFunctions.openSpecialityWindow = function(formobj) {
		specialityIds =  patientDetailsQuickFunctions.formObj.selSpecialityIds.value;

	if(specialityIds=="null")
	{
		 specialityIds="";

	}
	windowName=window.open("multiselectcodepopup.jsp?selectedIds="+specialityIds+"&openerForm="+patientDetailsQuickFunctions.formObj.name+"&hidEleName=selSpecialityIds&eleName=txtSpeciality&codeType=prim_sp&ptitle=<%=LC.L_Speciality%>&heading=<%=LC.L_Mng_Pats%>","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=300");
	/*windowName=window.open("multiselectcodepopup.jsp?selectedIds="+specialityIds+"&openerForm=patient&hidEleName=selSpecialityIds&eleName=txtSpeciality&codeType=prim_sp&ptitle=Speciality&heading=Manage Patients","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=300");*****/

	windowName.focus();
};

patientDetailsQuickFunctions.openEthnicityWindow = function() {
	var addEthnicityIds = patientDetailsQuickFunctions.formObj.selAddEthnicityIds.value;
	windowName=window.open("multiselectcodepopup.jsp?selectedIds="+addEthnicityIds+"&openerForm="+patientDetailsQuickFunctions.formObj.name+"&hidEleName=selAddEthnicityIds&eleName=txtAddEthnicity&codeType=ethnicity&ptitle=<%=LC.L_Ethnicity%>&heading=<%=LC.L_Mng_Pats%>","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=300");
	/*windowName=window.open("multiselectcodepopup.jsp?selectedIds="+addEthnicityIds+"&openerForm=patient&hidEleName=selAddEthnicityIds&eleName=txtAddEthnicity&codeType=ethnicity&ptitle=Ethnicity&heading=Manage Patients","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=300");*****/
	windowName.focus();
};


patientDetailsQuickFunctions.openRaceWindow = function() {
	addRaceIds = patientDetailsQuickFunctions.formObj.selAddRaceIds.value;
	windowName=window.open("multiselectcodepopup.jsp?selectedIds="+addRaceIds+"&openerForm="+patientDetailsQuickFunctions.formObj.name+"&hidEleName=selAddRaceIds&eleName=txtAddRace&codeType=race&ptitle=<%=LC.L_Race%>&heading=<%=LC.L_Mng_Pats%>","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=320");
	/*windowName=window.open("multiselectcodepopup.jsp?selectedIds="+addRaceIds+"&openerForm=patient&hidEleName=selAddRaceIds&eleName=txtAddRace&codeType=race&ptitle=Race&heading=Manage Patients","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=320");*****/
	windowName.focus();
};


patientDetailsQuickFunctions.validate = function(){
	var autogen = patientDetailsQuickFunctions.autoGenPatient;
	var formobj = patientDetailsQuickFunctions.formObj;

     if (autogen != 1) {
     	if(!(validate_col('Pat ID',formobj.patID))) return false;
     }


   //Virendra: Fixed Bug No. 4729, Added '\' validation for PatientID, Firstname, Lastname
   var patIDval;
   var patfnameval;
   var patlnameval;

   if(formobj.patID){
	   patIDval = formobj.patID.value;
   }
   if(formobj.patfname){
	   patfnameval = formobj.patfname.value;
   }
   if(formobj.patlname){
	   patlnameval = formobj.patlname.value;
   }
	
	 

 	 if(formobj.patID &&(patIDval.indexOf("%")!=-1 || patIDval.indexOf("\\")!=-1|| patIDval.indexOf("#")!=-1  ))
	  {
 		var paramArray = ["%,\\,#"];
		alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));/*alert("Special Character(%,\\) not allowed for this Field");*****/
		  formobj.patID.focus();
		  return false;
	  }
 	//Modified to not allowed the special character "," in "Patient ID"
	 if(formobj.patID && patIDval.indexOf(",") != -1 ){
		var paramArray = [","];
		alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));/*alert("Special Character(,) not allowed for this Field");*****/
		formobj.patID.focus();
		return false;
	 } 
 	 if( formobj.patfname && patfnameval.indexOf("\\")!=-1  )
	  {
 		var paramArray = ["\\"];
		alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));/*alert("Special Character(\\) not allowed for this Field");*****/
		  formobj.patfname.focus();
		  return false;
	  }
 	if( formobj.patlname && patlnameval.indexOf("\\")!=-1  )
	  {
 		var paramArray = ["\\"];
		alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));/*alert("Special Character(\\) not allowed for this Field");*****/
		  formobj.patlname.focus();
		  return false;
	  }

// Modified by Gopu on 8th March 2005 for fixed the bug no. 2036 make the "First Name" field in Patient Demographic as non-Mandatory
   	 if (document.getElementById('pgcustompatfname')) {
	   if (!(validate_col('First Name',formobj.patfname))) return false;
	 }

	 if (document.getElementById('pgcustompatlname')) {
	   if (!(validate_col('Last Name',formobj.patlname))) return false;
	 }



	 if (document.getElementById('pgcustompatdob')) {
	     if (!(validate_col('dob',formobj.patdob))) return false;
	 }

	 //gender mandatory/non-mand implementation
	 if (document.getElementById('pgcustompatgender')) {
	     if (!(validate_col('patgender',formobj.patgender))) return false;
	 }




	//ethnicity mandatory/non-mand implementation
	 if (document.getElementById('pgcustompatethnicity')) {
	     if (!(validate_col('patethnicity',formobj.patethnicity))) return false;
	 }


	  //Additional mandatory/non-mand implementation
	 if (document.getElementById('pgcustompatadditional1')) {
	     if (!(validate_col('Additional',formobj.txtAddEthnicity))) return false;
	 }




	 //race mandatory/non-mand implementation
	 if (document.getElementById('pgcustompatrace')) {
	     if (!(validate_col('patrace',formobj.patrace))) return false;
	 }


	  //Additional mandatory/non-mand implementation
	 if (document.getElementById('pgcustompatadditional2')) {
	     if (!(validate_col('Additional',formobj.txtAddRace))) return false;
	 }

     if (formobj.patdob && !(validate_date(formobj.patdob))) return false

     if (formobj.patorganization && !(validate_col('organization',formobj.patorganization))) return false;

     if (formobj.patstatus && !(validate_col('pat status',formobj.patstatus))) return false;


	 if (document.getElementById('pgcustompatfacid')) {
	     if (!(validate_col('Pat Facility Id',formobj.patFacilityID))) return false;
	 }


	 if (document.getElementById('pgcustompatprov')) {
	     if (!(validate_col('Provider',formobj.patregbyname))) return false;
	 }

	 if (document.getElementById('pgcustompatifoth')) {
	     if (!(validate_col('if other',formobj.phyOther))) return false;
	 }

	 if (document.getElementById('pgcustompatdod')) {
	     if (!(validate_col('dod',formobj.patdeathdate))) return false;
	 }
	 //KM
	 //cod mandatory/non-mand implementation
	 if (document.getElementById('pgcustompatcod')) {
	     if (!(validate_col('dthCause',formobj.dthCause))) return false;
	 }

	 if (document.getElementById('pgcustompatspeccause')) {
	     if (!(validate_col('speccause',formobj.otherdthCause))) return false;
	 }

	 if (document.getElementById('pgcustompatspeccause')) {
	     if (!(validate_col('speccause',formobj.otherdthCause))) return false;
	 }


	 // Added by JM on 040805
     if (formobj.patdeathdate && !(validate_date(formobj.patdeathdate))) return false;


     // Added bY Gopu on 07th April 2005 DOB Validation DOB should not be greater than Today's date
	 // modified by GOpu for fixing the bug no. 2117
	    /*
	    da= new Date();
	    var str1,str2;
	    str1=parseInt(da.getMonth());
	    str2=str1+1;
		todate = str2+'/'+da.getDate()+'/'+da.getFullYear();
		*/

	//JM: #4025, #4026
	todate = formatDate(new Date(),calDateFormat);

	if (formobj.patdob && formobj.patdob.value != null && formobj.patdob.value !=''){
		if (CompareDates(formobj.patdob.value,todate,'>')){
		  	alert ("<%=MC.M_BirthDtNotGrt_Today%>");/*alert ("Date of Birth should not be greater than Today's Date");*****/
			formobj.patdob.focus();
			return false;
		}
	}


	// Added by Gopu on 08th April 2005 Validation for Death Date should not allow future date

	if (formobj.patdeathdate && formobj.patdeathdate.value != null && formobj.patdeathdate.value !=''){
		if (CompareDates(formobj.patdeathdate.value,todate,'>')){
			alert ("<%=MC.M_DthDtNotGtr_TodayDt%>");/*alert ("Death Date should not be greater than Today's Date");*****/
			formobj.patdeathdate.focus();
			return false;
		}

	// Added by Gopu on 08th April 2005 Validation for Death Date should not be less than Birth Date

		if (formobj.patdob && formobj.patdob.value != null && formobj.patdob.value !=''){
			if (CompareDates(formobj.patdob.value,formobj.patdeathdate.value,'>')){
				alert ("<%=MC.M_DthDtNotLess_BirthDt%>");/*alert ("Death Date should not be less than Birth Date");*****/
				formobj.patdeathdate.focus();
				return false;
			}
		}

	}



	//check for death stat

	var patselstat;
	 var deadstat;
	 var deathdate;

	if(formobj.patstatus){
		patselstat = formobj.patstatus.value;
	}
	
	
	if(formobj.deadStatPk){
		 deadstat =   formobj.deadStatPk.value;
	}
	
	if(formobj.patdeathdate){
		deathdate = formobj.patdeathdate.value;
	}

	 if(deadstat == patselstat)
	 {
			if (deathdate == null || deathdate == '' )
			{
				 alert("<%=MC.M_PlsEtr_PatDod%>");/*alert("Please enter <%=LC.Pat_Patient%>'s Date of Death");*****/
				 return false;
			}
	 }

	 if (deathdate != null && deathdate != '' )
	 {
	 	if ( deadstat != patselstat)
	 	{
	 		alert("<%=MC.M_YouEtrPat_DodPlsChg%>");/*alert("You have entered <%=LC.Pat_Patient%>'s Date of Death. Please change the <%=LC.Pat_Patient%>'s Status to 'Dead'");*****/
	 		return false;
	 	}
	}


	 if(formobj.dthCause && formobj.dthCause.value != null && formobj.dthCause.value != '')
	 {
	  if ( deadstat != patselstat)
	 	{
	 		alert("<%=MC.M_EtrPatDthCause_StatDead%>");/*alert("You have entered <%=LC.Pat_Patient%>'s Cause of Death. Please change the <%=LC.Pat_Patient%>'s Status to 'Dead'");*****/
	 		return false;
	 	}
	 }
	// Added by Gopu on 08th April 2005 for Death date should not allow future date added

	if (formobj.patdeathdate && deathdate != null && deathdate !=''){
		if (CompareDates(deathdate,todate,'>')){
		  	alert ("<%=MC.M_DthDtNotGtr_TodayDt%>");/*alert ("Death Date should not be greater than Today's Date");*****/
			formobj.patdeathdate.focus();
			return false;
		}
	}

	if (!(validate_col('e-Signature',formobj.eSign))) return false
	<%-- 	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	} --%>

	return true;
};

patientDetailsQuickFunctions.displayFldValMsg = function(name,minLen,msgTarget,passMsg,failMsg,addFldStr,msgMode) {
	var eleSpan = document.getElementById("showfldobMessage");

	eleSpan.getElementsByTagName('td')[0].innerHTML="";
	ajaxvalidate(name,minLen,msgTarget,passMsg,failMsg,addFldStr,msgMode);
    var valMsgEle= document.getElementById(msgTarget);
    eleSpan.style.display="none";
    var inHTML = (valMsgEle) ? ($j.trim((valMsgEle.innerHTML))) : "";

    if(inHTML.length>0){
    	eleSpan.style.display="block";
    }
    eleSpan.style.paddingTop="0px";
    eleSpan.style.paddingBottom="0px";
};
</SCRIPT>
<script src="js/velos/date.js" type="text/javascript"></script>
<script src="js/velos/dateformatSetting.js" type="text/javascript"></script>
<script src="js/velos/patSearchUtil.js" type="text/javascript"></script>
<SCRIPT language="JavaScript1.1">
patientDetailsQuickFunctions.openwin1 = function() {
    windowName = window.open("usersearchdetails.jsp?fname=&lname=&from=patient","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
	windowName.focus();
};

patientDetailsQuickFunctions.asyncValidate = function()
{

 valChangeReturn=0;
 if ((document.getElementById("patfname").value.length>0) && (document.getElementById("patlname").value.length>0) && (document.getElementById("patdob").value.length>0) ) {
 ajaxvalidate('patient~fldob:siteId',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid');
 }
 if (document.getElementById("patid").value.length>0) {
 ajaxvalidate('patient:patid',0,'ajaxPatIdMessage','','<%=MC.M_PatId_AldyExst%><%--This <%=LC.Pat_Patient%> ID already exists for the selected organization or it\'s  parent/child organizations.*****--%>','siteId') ;
 }
};

patientDetailsQuickFunctions.updateDemoFromJSON = function(jsPatient){
	var transparentDiv = document.getElementById("trans");
	if (transparentDiv){
		transparentDiv.style.visibility="hidden";
	}
	//update fields
	updateSourceField(patientDetailsQuickFunctions.formObj.patid, jsPatient.MRN);
	var patientDOBDate = new Date(jsPatient.DOB);
	//calDateFormat defined in dateformatSetting.js
	updateSourceField(patientDetailsQuickFunctions.formObj.patdob, formatDate(patientDOBDate, calDateFormat));
	updateSourceField(patientDetailsQuickFunctions.formObj.patfname, jsPatient.firstName);
	updateSourceField(patientDetailsQuickFunctions.formObj.patlname, jsPatient.lastName);

	updateSourceField(patientDetailsQuickFunctions.formObj.patgender, dereferenceCode(jsPatient.gender, "gender"));

	updateSourceField(patientDetailsQuickFunctions.formObj.patrace, dereferenceCode(jsPatient.race, "race"));
	updateSourceField(patientDetailsQuickFunctions.formObj.patethnicity, 	dereferenceCode(jsPatient.ethnicity, "ethnicity"));

};
</SCRIPT>