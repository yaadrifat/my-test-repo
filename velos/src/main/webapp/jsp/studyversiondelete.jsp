<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.*,com.velos.eres.service.util.CFG"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>

<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC,com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.StringUtil"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%
boolean isIrb = "irb_upload_tab".equals(request.getParameter("selectedTab")) ? true : false;
String includeTabsJsp = isIrb ? ("LIND".equals(CFG.EIRB_MODE) ? "ecompNewTabs.jsp" : "irbnewtabs.jsp") : "studytabs.jsp";
if (isIrb) {
%>
<title><%=MC.M_ResComp_NewUpldDocu%><%--Research Compliance >> New Application >> Upload Documents*****--%></title>
<% } else { %>
<title><%=LC.L_Std_VersionDel%><%--<%=LC.L_Study%> Version Delete*****--%></title>
<% } %>



<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

<%-- 	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   } --%>
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.EJBUtil"%>
<jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="studyVerBean" scope="session" class="com.velos.eres.business.studyVer.impl.StudyVerBean"/>
<jsp:useBean id ="userBean" scope="session" class="com.velos.eres.business.user.impl.UserBean"/>
<jsp:useBean id ="userJB" scope="session" class="com.velos.eres.web.user.UserJB"/>

<% String src;
src= request.getParameter("srcmenu");


			//JM:
			String from = request.getParameter("from");

%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<BODY>
<br>

<DIV class="formDefault" id="div1">
<%
	String studyVerId= "";
	String selectedTab="";
	String studyId="";

HttpSession tSession = request.getSession(true);
 if (sessionmaint.isValidSession(tSession))	{
	 String logUsr = (String) tSession.getValue("userId");
	 userJB.setUserId(StringUtil.stringToNum(logUsr));
	 userBean= userJB.getUserDetails();
		studyVerId= request.getParameter("studyVerId");
		selectedTab=request.getParameter("selectedTab");
		studyId=request.getParameter("studyId");
		System.out.println("studyId:::"+studyId);
		int ret=0;
		String isPopupWin = request.getParameter("isPopupWin")==null?"":request.getParameter("isPopupWin");
	    String mode	= request.getParameter("mode")==null?"":request.getParameter("mode");
		String delMode=request.getParameter("delMode");
		if (delMode.equals("null")) {
			delMode="final";

if(!isPopupWin.equalsIgnoreCase("1")){%>
  <jsp:include page="<%=includeTabsJsp%>" flush="true">
  <jsp:param name="from" value="<%=from%>"/>
  <jsp:param name="studyId" value="<%=studyId %>"/>
  </jsp:include>
  <%} %>
<% if("Y".equals(CFG.Workflows_Enabled)&& (studyId!=null && !studyId.equals("") && !studyId.equals("0"))){ %>
<DIV class="BrowserBotN BrowserBotN_S_3 workflowDivBig" id = "div1" style="top:90px;">
<% }else{ %>
<DIV class="BrowserBotN BrowserBotN_S_3" id = "div1" style="top:67px;">
<%} %>
	<FORM name="studyverdelete" id="stdverdelfrm" method="post" action="studyversiondelete.jsp" onSubmit="if (validate(document.studyverdelete)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

	<P class="defComments"><%=MC.M_Etr_Esign_ToProc%><%--Please enter e-Signature to proceed*****--%></P>



	<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="stdverdelfrm"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include>


	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
  	 <input type="hidden" name="studyVerId" value="<%=studyVerId%>">
   	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">
	 <input type="hidden" name="studyId" value="<%=studyId %>">
	 <input type="hidden" name="isPopupWin" value="<%=isPopupWin %>">
	 <input type="hidden" name="mode" value="<%=mode %>">
	</FORM>
	</DIV>
<%
	} else {
			String eSign = request.getParameter("eSign");
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
 		  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
			} else {
			// Modified for INF-18183 ::: AGodara 
			 String rid= AuditUtils.getRidValue("ER_STUDYVER","eres","PK_STUDYVER="+studyVerId); //Fetches the RID
			 String apndxRid= AuditUtils.getRidValue("ER_STUDYAPNDX","eres","FK_STUDYVER="+studyVerId); //Fetches the RID
			 studyVerB.setStudyVerId(StringUtil.stringToNum(studyVerId));
			 studyVerBean=studyVerB.getStudyVerDetails();
			ret = studyVerB.removeStudyVer(StringUtil.stringToNum(studyVerId),AuditUtils.createArgs(tSession,"",LC.L_Study));    //Akshi: Fixed for Bug #7604
			if (ret==-1) {%>
			<br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_NotRemSucc%><%--Data not removed successfully*****--%> </p>
			<%}else {
				/***Audit Maintain****/
					
					 AuditUtils.updateAuditROw("eres",userBean.getUserId()+", "+userBean.getUserFirstName()+", "+userBean.getUserLastName(),rid,"D");
					 AuditUtils.updateAuditROw("eres",userBean.getUserId()+", "+userBean.getUserFirstName()+", "+userBean.getUserLastName(),apndxRid,"D");
			   /***Audit Maintain****/
				%>
			<br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_RemSucc%><%--Data removed successfully*****--%> </p>
				<%if ("LIND".equals(CFG.EIRB_MODE)){
				if(mode.equalsIgnoreCase("N")){%>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=studyVerBrowser.jsp?mode=<%=mode%>&isPopupWin=<%=isPopupWin%>&studyId=<%=studyId%>"attWindow","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=1280,height=500,left=100,top=100">
				<%}else if(isPopupWin.equalsIgnoreCase("1")){%>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=studyVerBrowser.jsp?mode=<%=mode%>&isPopupWin=<%=isPopupWin%>&studyId=<%=studyId%>"attWindow","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=1280,height=500,left=100,top=100">
				<%}else{ %>
				<%if(isIrb==true) {%>
					<META HTTP-EQUIV=Refresh CONTENT="1; URL=studyAttachmentsLindFt?mode=M&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>">
				<%}else{ %>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=studyAttachments?mode=M&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">
				<%}}}else { %>
				<!-- @Gyanendra update for bug #20401 start -->
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=studyVerBrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">			
			    <!-- update for bug #20401 end -->
			    <%} %>	
			<%}
			} //end esign
	} //end of delMode
  }//end of if body for session
else { %>
 <jsp:include page="timeout.html" flush="true"/>
 <% } %>

 <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>
</HTML>


