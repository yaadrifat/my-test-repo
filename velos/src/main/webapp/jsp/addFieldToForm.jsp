<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Selc_FromFldLib%><%--Select from Field Library*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>

<script>


function openWin(Id,flag,formobject)
{
	 if ( (flag =="ET") || (flag=="EN") || (flag=="ED" ) || (flag=="EI") )
	{
		
		param1="editBox.jsp?fieldLibId="+Id+"&mode=RO";  

		windowName= window.open(param1,"FieldDetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=600");
		windowName.focus();
		return ;
	}
	
	else if ( (flag =="ML")  || (flag=="MD") || (flag=="MC") || (flag=="MR") )
	{
					
					
		param1="multipleChoiceBox.jsp?fieldLibId="+Id+"&mode=RO";
		windowName= window.open(param1,"FieldDetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=600");
		windowName.focus();
		return ;
	}
		
	
		
}

function setMode(formobject, flag)
{
	if(flag=="R")
	{
		formobject.refreshFlag.value="captureIdsSel" ;
		
		num = formobject.numSel.value ; 
		for (i=0;i<num;i++) 
		{
			if (num > 1)
			{
				if (formobject.selected[i].checked)
				{
					// by salil 
					formobject.submit();
					return true;
				}
			} 
			else 
			{
				if (formobject.selected.checked) 
				{
					// by salil 
					formobject.submit();
					return true;
				}	 
			}
	 }
	alert("<%=MC.M_Selc_Fld%>");/*alert("Please Select a Field.");*****/
	// Added by Gopu to fix the bug #2366
	formobject.refreshFlag.value="submit" ;
	return false;
		
		
		
	}
	if(flag=="L")
	{
		
		formobject.refreshFlag.value="captureIdsDSel" ;
		 num = formobject.numDsel.value ;
		for (i=0;i<num;i++) 
		{
			if (num > 1) 
			{
				if (formobject.deselected[i].checked) 
				{
					// by salil 
					formobject.submit();
					return true;
				}
			} 
			else 
			{
				if (formobject.deselected.checked) 
				{
					 //by salil 
					formobject.submit();
					return true;
				}	 
			}
	}
	alert("<%=MC.M_Deselect_Fld%>");/*alert("Please Deselect a Field.");*****/
	// Added by Gopu to fix the bug #2366
	formobject.refreshFlag.value="submit" ;
	return false;
		
		
		
		
	}
	if(flag=="S")
	{
		formobject.refreshFlag.value="submit" ;
	}
	if(flag=="SE") 
	{
		formobject.mode.value="final" ;
	}
		
}


function  validate(formobj)
 {
 	
	flag=formobj.refreshFlag.value;
	//len = formobj.cnt.length;

 	if  (   (flag=="captureIdsDSel")  || (flag=="captureIdsSel")  )  
	{  
		formobj.refreshFlag.value="submit" ;
		return false ;

	}  //we dont validate if the mode in capture ids
	else if (flag=="submit")

	{
		
		// by Anu
		//Commented by Gopu to fix the bug #2366
/*		if(formobj.numDsel.value==0)
		{
			alert("Please select Field");
			return false;
		}
*/
	  	if (  !(validate_col('section',formobj.section)     )   ) return false
	 	if(formobj.codeStatus.value != "WIP"){	
	 	 	if (  !(validate_col('e-Signature',formobj.eSign)     )   ) return false
	 		 
	 		<%-- if(isNaN(formobj.eSign.value) == true) 
	 			{
	 			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	 			formobj.eSign.focus();
	 			 return false;
	 	   		} --%>
	 	 	}
	
	}
  } 

  
 
  
  
  
  

</script>
<body>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="catLibJB" scope="request" class="com.velos.eres.web.catLib.CatLibJB"/>
<jsp:useBean id="formSecJB" scope="request"  class="com.velos.eres.web.formSec.FormSecJB"/>

<%@ page language = "java" import = "com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB"%>

<jsp:include page="include.jsp" flush="true"/>
<%
HttpSession tSession = request.getSession(true);
String codeStatus = request.getParameter("codeStatus");
if (sessionmaint.isValidSession(tSession))
{  
   String deselected[] = request.getParameterValues("deselected");
   String deselectedNames[] = request.getParameterValues("deselectedName");
   int pageRight = 0;	
   GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
   pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));
   
   if (pageRight >= 4) 
		{
		String formId="";
		String fldLibId="";
		String fkCategory="";
		String fldName="";
		String fldUniqueId="";
		String fldDescription="";	 
		String fldType="";
		String fldKeyword="";
		String catLibName="";	
		String catLibId="";
		String fldDataType="";		
		String tmpFldDataType= "";
		int counter = 0;
		int len=0;
		int forLastAll = 1 ;
		
		String uName = (String) tSession.getValue("userName");
		formId=request.getParameter("formId");		
		String accountId=(String)tSession.getAttribute("accountId");
		String mode= request.getParameter("mode");
		mode=(mode==null)?"":mode;
		fkCategory= request.getParameter("category");
		fldName= request.getParameter("name");
		fldKeyword= request.getParameter("keyword");
		int tempAccountId=EJBUtil.stringToNum(accountId);
		String refreshFlag=request.getParameter("refreshFlag");
		String section=request.getParameter("section");
		int tempfkCategory= 0;
			
		if (fldName == null ) 
		{
			fldName="";
		}
		
		if (fldKeyword == null ) 
		{
			fldKeyword="" ;
		}
	
	
	
		
		ArrayList fldLibIds= null; 
		ArrayList catLibIds= null; 
		ArrayList fldNames= null;
		ArrayList fldUniqueIds= null;
		ArrayList fldDescriptions= null;
		ArrayList fldTypes= null;
		ArrayList catLibNames= null;
		ArrayList fldDataTypes= null;

		//here we will get all the categories
		
		String catLibType="C";
		CatLibDao catLibDao= new CatLibDao();
		ArrayList categoryIds= new ArrayList();
		ArrayList categoryDesc= new ArrayList();
		ArrayList idSec= new ArrayList();
		ArrayList descSec= new ArrayList();
		
		String categoryPullDown="";
		String pullDownSection="";
		FormSecDao formSecDao= new FormSecDao();
		
		
		FieldLibDao fieldLibDao = new FieldLibDao();

		if ( mode.equals("initial"))
		{
			mode= "final";		
			
			catLibDao= catLibJB.getCategoriesWithAllOption(tempAccountId,catLibType,forLastAll);
		
			categoryIds = catLibDao.getCatLibIds();	
			categoryDesc= catLibDao.getCatLibNames();
			
			int  initialCat = 0 ;
			if ( categoryIds.size() > 0)
			{ 
				initialCat = (   (Integer)categoryIds.get(0)).intValue() ; 
				fkCategory = ( (Integer)categoryIds.get(0) ).toString()   ; 
			
				categoryPullDown=EJBUtil.createPullDownNoSelect("category", initialCat, categoryIds, categoryDesc);
				
			}
			else 
			{
				categoryPullDown = "" ;
			}	
			
			if (fkCategory != null)
			tempfkCategory=EJBUtil.stringToNum(fkCategory);
			
			//fieldLibDao=fieldLibJB.getFieldsFromSearch( tempfkCategory ,tempAccountId,0,"","");
				
			fieldLibDao=fieldLibJB.getFieldsForCopy(tempfkCategory ,tempAccountId,"","");		
			fldLibIds = fieldLibDao.getFieldLibId(); 
			catLibIds = fieldLibDao.getCatLibId();
			
			
			
			
			formSecDao = formSecJB.getAllSectionsOfAForm( EJBUtil.stringToNum(formId));	
			idSec= formSecDao.getFormSecId();	
			descSec=formSecDao.getFormSecName();		
			pullDownSection=EJBUtil.createPullDown("section", 0, idSec, descSec);	
			
			
			catLibNames =fieldLibDao.getFldCatName();
			fldNames = fieldLibDao.getFldName();
			fldUniqueIds = fieldLibDao.getFldUniqId();
			fldDescriptions =fieldLibDao.getFldDesc();
			fldTypes =fieldLibDao.getFldType();
			fldDataTypes =fieldLibDao.getFldDataType();
			catLibIds= fieldLibDao.getCatLibId();
			len=fieldLibDao.getRows();
	
		}
		else 
		{
			
			
		
			if (fkCategory != null)
			tempfkCategory=EJBUtil.stringToNum(fkCategory);
			
		
			if (section != null)
			{
				
				formSecDao = formSecJB.getAllSectionsOfAForm( EJBUtil.stringToNum(formId));	
				idSec= formSecDao.getFormSecId();	
				descSec=formSecDao.getFormSecName();		
				pullDownSection=EJBUtil.createPullDown("section", EJBUtil.stringToNum(section), idSec, descSec);	
				
				
			}
			else
			{
				
				formSecDao = formSecJB.getAllSectionsOfAForm( EJBUtil.stringToNum(formId));	
				idSec= formSecDao.getFormSecId();	
				descSec=formSecDao.getFormSecName();		
				pullDownSection=EJBUtil.createPullDown("section", 0, idSec, descSec);	
			
			}
			//to create the Category Pull Down
			
			catLibDao.getCategoriesWithAllOption(tempAccountId,catLibType,forLastAll);
			categoryIds = catLibDao.getCatLibIds();
			categoryDesc= catLibDao.getCatLibNames();
			categoryPullDown=EJBUtil.createPullDownNoSelect("category", tempfkCategory, categoryIds, categoryDesc);
			
						
			//fieldLibDao=fieldLibJB.getFieldsFromSearch(tempfkCategory,tempAccountId,0,fldName,fldKeyword);
			fieldLibDao=fieldLibJB.getFieldsForCopy(tempfkCategory,tempAccountId,fldName,fldKeyword);
			fldLibIds = fieldLibDao.getFieldLibId();
			
			catLibIds = fieldLibDao.getCatLibId();
			catLibNames =fieldLibDao.getFldCatName();
			fldNames = fieldLibDao.getFldName();
			fldUniqueIds = fieldLibDao.getFldUniqId();
			fldDescriptions =fieldLibDao.getFldDesc();
			
			fldTypes =fieldLibDao.getFldType();
			fldDataTypes =fieldLibDao.getFldDataType();
			
			
			len=fieldLibDao.getRows();
					
			
		}
		
		

		//if (pageRight > 0 )

   		//{

%> 
<DIV class="popDefault" id="div1">
		    <Form name="fieldLibrarySearch" action="addFieldToForm.jsp"  method="post">
			<Input type="hidden" name="mode" value=<%=mode%> >
			<Input type="hidden" name="section" value=<%=section%> >
			<Input type="hidden" name="codeStatus" value=<%=codeStatus%> >
			<Input type="hidden" name="formId" value=<%=formId%> >
	     	<P class="defComments"><%=MC.M_Search_FldBased%><%--Search fields based on*****--%>:</P>  
				<table width="100%" cellspacing="0" cellpadding="0" border="0">
   				 <tr> 
				      <td width="9%"><P ><%=LC.L_Category%><%--Category*****--%></P></td>
			          <td width="21%"><%=categoryPullDown%> </td>
					  <td width="10%"><P><%=LC.L_FieldName%><%--FieldName*****--%> </P> </td>
			          <td width="20%"><input type="text" name="name" size = 25 MAXLENGTH = 50 value="<%=fldName%>"> </td>
					  <td width="10%" align="center"><P><%=LC.L_Keyword%><%--Keyword*****--%> </P> </td>
			          <td width="20%"><input type="text" name="keyword" size = 25 MAXLENGTH = 50 value="<%=fldKeyword%>"> </td>
					  <td width ="10%"><button type="submit" onClick="setMode(document.fieldLibrarySearch,'SE')"><%=LC.L_Search%></button></td>
				</tr>					  
			</table>
				
				</FORM>
			
					  
				<!-- THE COLUMN FOR THE BROWSER -->		
			<Form name="fieldsOfLibrary" id="fieldsoflibfrm" action="addFieldToFormSubmit.jsp "  method="post" onsubmit="if (validate(document.fieldsOfLibrary)== false) {return false; } else {return true;}">
			<table width="100%" border="0">	
				<tr>
				<td  width="50%" >
			<input type="hidden" name="formId" value=<%=formId%>>
			<!--<input type="hidden" name="refreshFlag" value=<%=refreshFlag%>>-->
			<input type="hidden" name="refreshFlag" value="submit">
			<Input type="hidden" name="mode" value=<%=mode%> >
			<input type="hidden" name="codeStatus" value=<%=codeStatus%>>
		  	<input type="hidden" name="category" value=<%=fkCategory%>>
			<input type="hidden" name="keyword" value=<%=fldKeyword%>>
			<input type="hidden" name="name" value=<%=fldName%>>

			 <br>
			 <br>
			 <br>			 			
			  <div style="overflow:auto;overflow-x:hidden; height:400; border:groove;"> 

				<table width="100%" cellspacing="1" cellpadding="0" border="0" align="">
				    <tr> 
						<th width="10%"> <%=LC.L_Select%><%--Select*****--%> </th>
				        <th width="30%"> <%=LC.L_Fld_Name%><%--Field Name*****--%> </th>
				        <th width="20%"> <%=LC.L_Fld_Id%><%--Field ID*****--%></th>
						<th width="20%"><%=LC.L_Fld_Type%><%--Field Type*****--%> </th>
						
					</tr>

		<%
										
					for(counter = 0;counter<len;counter++)
					{
						
						
						mode="captureIds" ;
												
						fldName = ((fldNames.get(counter)) == null)?"-":(fldNames.get(counter)).toString();		
						fldUniqueId = ((fldUniqueIds.get(counter)) == null)?"-":(fldUniqueIds.get(counter)).toString();
																
						fldDescription = ((fldDescriptions.get(counter)) == null)?"-":(fldDescriptions.get(counter)).toString();
							
						tmpFldDataType=((fldDataTypes.get(counter)) == null)?"-":(fldDataTypes.get(counter)).toString();
								
						if (tmpFldDataType.equals("ET") ) 
						{ fldDataType=LC.L_Text;/*fldDataType="Text";*****/ }
						if (tmpFldDataType.equals("EN") ) 
						{ fldDataType=LC.L_Number;/*fldDataType="Number";*****/ }
						if (tmpFldDataType.equals("ED") ) 
						{ fldDataType=LC.L_Date;/*fldDataType="Date";*****/ }
						if (tmpFldDataType.equals("EI") ) 
						{ fldDataType=LC.L_Time;/*fldDataType="Time";*****/ }
						if (tmpFldDataType.equals("ML") ) 
						{ fldDataType=LC.L_Lookup;/*fldDataType="Lookup";*****/ }
						if (tmpFldDataType.equals("MD") ) 
						{ fldDataType=LC.L_Dropdown;/*fldDataType="Dropdown";*****/}
						if (tmpFldDataType.equals("MC") ) 
						{ fldDataType=LC.L_Checkbox;/*fldDataType="Checkbox";*****/ }
						if (tmpFldDataType.equals("MR") ) 
						{ fldDataType=LC.L_RadioButton;/*fldDataType="RadioButton";*****/ }  
						if (tmpFldDataType.equals("-") )
						{ fldDataType="-"; }  					
						fldLibId= fldLibIds.get(counter).toString();
						
						
						String fldNameHref="";
						
						
						if ( fldLibId.equals("0") ) 
						{							
							fldNameHref="-";
						}
						else 
						{  
							fldNameHref="<A href=\"# \" onClick=\"openWin( '"+fldLibId+"' , '"+tmpFldDataType+"' ,document.fieldsOfLibrary) \" >" + fldName+"</A>" ;
							
						}
						
						String selFields = fldLibId+"[VELSCOLON]"+fldName+"[VELSCOLON]"+fldUniqueId;
						
						selFields = StringUtil.encodeString(selFields);
						
 						if ((counter%2)==0) 
						{

						%>

					      	<tr class="browserEvenRow"> 

						<%}else
						 { %>

				      		<tr class="browserOddRow"> 

						<%}  %>	
					
						
						
						<td><input type="checkbox" name="selected" value="<%=selFields%>" > </td>
						<td> <%=fldName%> </A></td>
						<td><%=fldUniqueId%></td>
						<td><%=fldDataType%></td>	
			    	 	</tr>
						
					<%
			}//for loop 		
		
					
					%>
					
				
				</table>
				
				<input type="hidden" name="numSel" value="<%=len%>" >
			  </div>
				
			</td>	<!-- END OF THE TD OF THE FIELD BROWSER -->
		  
			
			<td width="5%"> <!-- TD FOR THE ARROWS BMP -->
				
				<!--<input  type="image" src="../images/jpg/formright.gif" onClick="return setMode(document.fieldsOfLibrary,'R')" align="absmiddle"   border="0">-->
				<img src="../images/jpg/formright.gif" align="absmiddle" border="0" onClick="return setMode(document.fieldsOfLibrary,'R')">
				
				<br>
				<br>
				<!--<input  type="image" src="../images/jpg/formleft.gif" onClick="return setMode(document.fieldsOfLibrary,'L')" align="absmiddle"   border="0">-->
				<img src="../images/jpg/formleft.gif" align="absmiddle" border="0" onClick="return setMode(document.fieldsOfLibrary,'L')">
			</td><!-- TD FOR THE ARROWS BMP-->
			
			<td width="45%">
			<P ><%=MC.M_SecTo_AddFld%><%--Section to Add Fields*****--%><FONT class="Mandatory">*</FONT>&nbsp;&nbsp;&nbsp;<%=pullDownSection%></P> 

			<div style="overflow:auto;overflow-x:hidden; height:400;border:groove;" >			 
			<table width="100%" cellspacing="1" cellpadding="0" border="0" align="left"> 
			    <tr> 
						<th width="33%"> <%=LC.L_DehypenSelect%><%--De-Select*****--%> </th>
				          <th width="33%"> <%=LC.L_Fld_Name%><%--Field Name*****--%> </th>
				        <th width="33%"> <%=LC.L_Fld_Id%><%--Field Id*****--%></th>
					</tr>
					<%
					int cnt=0;
					String deselectedName = "";
					String tmp = "";
					if (!(deselected==null)) 
					{
					   cnt =deselected.length;
					}
					String deselId = "";
					String deselName = "";
					String deselUniqId = "";
					int strlen  =0 ;
					int secondpos  = 0 ;
					int firstpos  = 0 ;
				
					for(counter = 0;counter<cnt;counter++)
					{
						
						 deselectedName = StringUtil.decodeString(deselected[counter]);
						 
						 strlen = deselectedName.length();		
						 //System.out.println("deselected[counter]" + deselected[counter]);
						 //System.out.println("strlen" + strlen);
						 
						 firstpos = deselectedName.indexOf("[VELSCOLON]",0);	
						 //System.out.println("firstpos" + firstpos);
						 
						 deselId = deselectedName.substring(0,firstpos);	
						 //System.out.println("deselId" + deselId);
						 
						 secondpos = deselectedName.indexOf("[VELSCOLON]",firstpos+11); 
						 
						 //System.out.println("secondpos" + secondpos);
						 
						 deselName = deselectedName.substring(firstpos+11,secondpos);	
						 
						 //System.out.println("deselName" + deselName);
						 
						 deselUniqId = deselectedName.substring(secondpos+11,strlen);
						 
						 //System.out.println("deselUniqId" + deselUniqId);
						
						String selFields = deselId+"[VELSCOLON]"+deselName+"[VELSCOLON]"+deselUniqId;  
						selFields = StringUtil.encodeString(selFields);
						
						%>
						<input type="hidden" name="deselectedAll" value="<%=selFields%>" >
						<%
						if ((counter%2)==0) 
						{
								
						%>

					      	<tr class="browserEvenRow"> 

						<%} else
						 { %>

				      		<tr class="browserOddRow"> 

						<%}  %>	
						<td><input type="checkbox" name="deselected" value="<%=selFields%>"> </td>
						<td> <%=deselName%> </A></td>
						<td><%=deselUniqId%></td>
					<%}%>	
					
			</table>		
			
			</div>
			<input type="hidden" name="numDsel" value="<%=cnt%>" >
			</td>	<!-- END OF THE TD FOR THE ADDED FEILDS -->			
		</tr>
		</table>
		<input type="hidden" name="formId" value=<%=formId%>>	
		<%	if(codeStatus.equals("WIP")){
						%>
						<table align="center"><tr>
	  <td align="center"><br />
				<button type="submit" id="submit_btn" ondblclick="return false"><%=LC.L_Submit%></button>
			</td> 
	  </tr></table>
     
<%}else{ 
%>

	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="editBoxFieldFrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include> 
 <%} %>
			</Form>


		<%
		} //end of if body for page right

		else
		{

		%>

			<jsp:include page="accessdenied.jsp" flush="true"/>

		<%

		} //end of else body for page right

	}//end of if body for session

	else

	{

	%>

		<jsp:include page="timeout.html" flush="true"/>

	<%

	}

	%>

	

</div>

 

 <div class = "myHomebottomPanel"> 
 <jsp:include page="bottompanel.jsp" flush="true"/>
 </div>


</body>

</html>

