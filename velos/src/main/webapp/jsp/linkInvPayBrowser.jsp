<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp"></jsp:include>
<jsp:include page="skinChoser.jsp" flush="true"/> 
<html>
<head>
<title><%=LC.L_Reconcile_PaymentDets%><%--Reconcile Payment Details*****--%></title>
</head>
<SCRIPT>
 
	function Add(windowName,pgRight) 
	{
	if (f_check_perm(pgRight,'N') == true) {     
      windowName= window.open(windowName,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=530,left=125,top=200")
      windowName.focus();      
	}else {
		return false;
	}			
	
	}


	
	function edit(windowName,pgRight) 
	{
      windowName= window.open(windowName,"Edit","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=530,left=125,top=200")
      windowName.focus();      
	
	}
	
</SCRIPT>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="calendar.js"></SCRIPT>

<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="PayDetB" scope="page" class="com.velos.eres.web.milepayment.PaymentDetailJB" />

<%@ page language = "java" import = "com.velos.eres.business.common.PaymentDetailsDao,com.velos.eres.business.common.TeamDao,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*,com.velos.esch.business.common.*"%>
<body>
<!-- Modified By Parminder Singh Bug#10127-->
<DIV class="popDefault" style="border: none; height:100%">
<% 
String studyId = request.getParameter("studyId");
int pageRight = EJBUtil.stringToNum(request.getParameter("pR"));
String paymentPk = request.getParameter("paymentPk");
String fromPage = (request.getParameter("fromPage")==null)?"":request.getParameter("fromPage");
if(fromPage.equals("report")){
	
	ArrayList teamId=new ArrayList();
	HttpSession tSession=request.getSession(true);
	String userId=(String)tSession.getValue("userId");
	TeamDao teamDao=new TeamDao();
	teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userId));
	teamId=teamDao.getTeamIds();
	
	if(teamId.size()<=0)
		pageRight=0;
	else{
		StudyRightsJB studyRightsJB=new StudyRightsJB();
		studyRightsJB.setId(EJBUtil.stringToNum(teamId.get(0).toString()));
		ArrayList teamRights=teamDao.getTeamRights();
		studyRightsJB.setSuperRightsStringForStudy(teamRights.get(0).toString());
		studyRightsJB.loadStudyRights();
		
		if(studyRightsJB.getFtrRights().size()==0)
			pageRight=0;
		else
			pageRight=Integer.parseInt(studyRightsJB.getFtrRightsByValue("MILESTONES"));
	}	
}
%>
 
	
  <jsp:include page="paymenttabs.jsp" flush="true">
	<jsp:param name="studyId" value="<%=studyId%>"/>
	<jsp:param name="selectedTab" value="1"/>
	<jsp:param name="paymentPk" value="<%=paymentPk%>"/>
	<jsp:param name="pR" value="<%=pageRight%>"/>
  </jsp:include>


 

<%


		PaymentDetailsDao pdao = new PaymentDetailsDao ();

		pdao = PayDetB.getLinkedPaymentBrowser(EJBUtil.stringToNum(paymentPk),"I");
		
		ArrayList arId = new ArrayList ();
		ArrayList  arDesc = new ArrayList ();

		ArrayList arAmount = new ArrayList ();
		ArrayList  arLinkId = new ArrayList ();
			
		int count  = 0;
		String id = "";
		String linkId = "";
		String amount = "";
		String desc= "";
		
		if (pdao != null)
		{
			arId = pdao.getId();
			arDesc = pdao.getPaymentLinkDescription();

			arAmount = pdao.getAmount();
			arLinkId = pdao.getLinkToId();
			
			if (arId == null) 
			{
				arId = new ArrayList();
			}
			System.out.println(arDesc.size());
			System.out.println(arAmount.size());
			System.out.println(arLinkId.size());
		}
	 	%>
		<P class = "defComments custom-defComments custom-not-defComments"><b> <A onClick="return f_check_perm(<%=pageRight%>,'N') " HREF="linkInvPayment.jsp?studyId=<%=studyId%>&paymentPk=<%=paymentPk%>&pR=<%=pageRight%>"><%=MC.M_ReconcilePment_ClkInv%><%--To Reconcile this Payment with an Invoice, Click Here to select an Invoice*****--%></A></b> </P>
		<P class = "defComments custom-defComments custom-not-defComments"><%=MC.M_ReconciledRec_Listed%><%--Previously reconciled records are listed below*****--%>:</P>
		<div onscroll="return  myscroll();" style="overflow:auto;position:relative;height:75%;">																				  
	 <table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline MidAlign" >
      <tr> 
        <th><%=LC.L_Inv_Number%><%--Invoice Number*****--%></th>
        <th><%=LC.L_Amount%><%--Amount*****--%></th>
      </tr>		
 	  	
<%
	count = arId.size();
	
	
	 for(int counter = 0;counter<count ;counter++)
	{	
		 linkId =  (String) arLinkId.get(counter); 
		 desc =  (String) arDesc.get(counter); 
		 amount =  (String) arAmount.get(counter); 
	    
		if (StringUtil.isEmpty(amount))
		{
			amount = "0.0";
		}
		%>
		
	<%	 
		if(counter%2==0){ 
	%>	
		<TR class="browserEvenRow">	
	<%
	}else{
	%>
		<TR class="browserOddRow">
	<% 
	} 
	%>
	
	<td align="center"><A  href="linkInvDetails.jsp?studyId=<%=studyId%>&paymentPk=<%=paymentPk%>&pR=<%=pageRight%>&invPk=<%=linkId%>" ><%=desc%></A>
	 
	</td>
	
	<td align="center"><span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=amount%></span></td>
	<!-- Mukul: BUG 4020 NumberUtil.roundOffNo() function used to roundoff number upto two decimal places -->
	</tr>
	
	
	<%
		
	}
	 
	 %>
</table>
</div>
</div>	  
</body>
<html>