<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</HEAD>

<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
  <jsp:useBean id="alertNotifyB" scope="request" class="com.velos.esch.web.alertNotify.AlertNotifyJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.MC"%>
  <%

	String src = null;
	src = request.getParameter("srcmenu");
	String addFlag = request.getParameter("addFlag");
	String eSign = request.getParameter("eSign");	
	String alNotId = request.getParameter("alNotId");
	String eventName = request.getParameter("eventName");
	String alertNotifyToNames = request.getParameter("alertNotifyToNames");
	String alertNotifyToId = request.getParameter("alertNotifyToId");

	String from = request.getParameter("from");
	String selectedTab = request.getParameter("selectedTab");
	

	if(alertNotifyToId.equalsIgnoreCase("null"))
	{
	alertNotifyToId="";
	}
	
	String mode = request.getParameter("mode");
	String protocolId = request.getParameter("protocolId");
	String codelstId= request.getParameter("eventstatus");	
	String studyId = request.getParameter("studyId");	

	String globalFlag = request.getParameter("globalFlag");


HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
   	String oldESign = (String) tSession.getValue("eSign");
	String eventId = (String) tSession.getValue("eventId");

	if(!oldESign.equals(eSign)) 
	{
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} 
	else 
  	{
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");

%>

<%

	if(mode.equals("N")) 
	{


		alertNotifyB.setAlertNotifyCodelstAnId(codelstId);
		alertNotifyB.setAlertNotifyAlNotType("N");
		alertNotifyB.setAlertNotifyAlNotUsers(alertNotifyToId);
		alertNotifyB.setAlertNotifyAlNotMobEve(eventName);
		alertNotifyB.setAlertNotifyAlNotFlag("1");
		alertNotifyB.setAlertNotifyStudy(studyId);
		alertNotifyB.setAlertNotifyProtocol(protocolId);
		
		alertNotifyB.setAlertNotifyGlobalFlag(globalFlag);
		
		alertNotifyB.setCreator(usr);
		alertNotifyB.setIpAdd(ipAdd);
		
		alertNotifyB.setAlertNotifyDetails();
		
	}
	
	if(mode.equals("M")) 
	{
		alertNotifyB.setAlNotId(EJBUtil.stringToNum(alNotId));
		alertNotifyB.getAlertNotifyDetails();		

		alertNotifyB.setAlertNotifyCodelstAnId(codelstId);
		alertNotifyB.setAlertNotifyAlNotType("N");
		alertNotifyB.setAlertNotifyAlNotUsers(alertNotifyToId);
		alertNotifyB.setAlertNotifyAlNotMobEve(eventName);
		alertNotifyB.setAlertNotifyAlNotFlag("1");
		alertNotifyB.setAlertNotifyStudy(studyId);
		alertNotifyB.setAlertNotifyProtocol(protocolId);
		alertNotifyB.setAlertNotifyGlobalFlag(globalFlag);
		alertNotifyB.setCreator(usr);
		alertNotifyB.setIpAdd(ipAdd);		
		
		alertNotifyB.updateAlertNotify();
	}	
%>


<%
if(addFlag.equals("Add"))
{
%>

<META HTTP-EQUIV=Refresh CONTENT="1; URL=studyeventnot.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&mode=N&studyId=<%=studyId %>&globalFlag=<%=globalFlag%>&protocolId=<%=protocolId%>&from=<%=from%>">

<%
}
else
{
mode="N";
%>

<META HTTP-EQUIV=Refresh CONTENT="1; URL=studyeventnotbrowse.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&mode=<%=mode%>&globalFlag=<%=globalFlag%>&studyId=<%=studyId%>&protocolId=<%=protocolId%>&from=<%=from%>">

<%
}
%>




<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%-- Data was Saved successfully.*****--%> </p>
<%
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





