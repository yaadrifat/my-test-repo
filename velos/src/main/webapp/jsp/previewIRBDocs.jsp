<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import="com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.web.appendix.AppendixJB" %>
<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@page import="com.velos.eres.widget.business.common.UIFlxPageDao" %>

<html>
	<body>

	
		<%
			
			HttpSession sessObj = request.getSession(true);
			
			if (sessionmaint.isValidSession(sessObj))
			{
					String getUrl = "";
					String url = "";
					String selType = "";
					String studyId = "";
					
					String filename = "";
					int filePk = 0;
					String dnldurl  = "";
					int formFlagIndex = -1;
					Hashtable htResponseInfo = new Hashtable();
					
					String message=MC.M_PlsSel_DocuCat/*"Please Select Document Category"*****/;
					
					String appSubmissionType = "";
					
					
					selType = request.getParameter("selType");
					
					String study_acc_form_right = "";
					String study_team_form_access_right = "";
					
					study_acc_form_right = request.getParameter("study_acc_form_right");
					study_team_form_access_right = request.getParameter("study_team_form_access_right");
					
					if (StringUtil.isEmpty(selType ))
					{
						selType ="";
						
					}
					
					String tabsubtype =request.getParameter("tabsubtype");
					
					appSubmissionType = request.getParameter("appSubmissionType");
					
					String submissionDate = request.getParameter("submissionDate");
					
					String statCreatedOn = request.getParameter("approvalDate");
					
					if (StringUtil.isEmpty(appSubmissionType))
					{
						appSubmissionType = "";
						
					}
					String submissionPK = StringUtil.trueValue(request.getParameter("submissionPK"));
					String submissionBoardPK = StringUtil.trueValue(request.getParameter("submissionBoardPK"));
					
					studyId = request.getParameter("studyId");
					
					formFlagIndex = selType.indexOf("*");
					boolean hasFormResponse = false;
					String formPK = "";
					String formlibver = "";
					String pkstudyforms = "";
					int majVersion = 0;
					int minVersion = 0;
					
					boolean checkedFormList = false;
					boolean showLastSubVersion = false;
					boolean showLastResubMemo = false;
					boolean showUploadedDocs = false;
					boolean submhist = false;
					
					if(request.getParameter("versionMajor")!=null){
						majVersion=Integer.parseInt(request.getParameter("versionMajor"));
					}
					if(request.getParameter("versionMinor")!=null){
						minVersion=Integer.parseInt(request.getParameter("versionMinor"));
					}
					
					if(selType.equals("LastSubVer")){
						showLastSubVersion = true;
					}
					if(selType.equals("LastResubMemo")){
						showLastResubMemo = true;
					}
					if(selType.equals("UploadDocs")){
						showUploadedDocs = true;
					}
					if(selType.equals("SubmHist")){%>
					 <form name="history" method="post" action="irbhistory.jsp" target="_self">
				          <input type="hidden" name="submissionPK" value="<%=submissionPK%>" />
		                  <input type="hidden" name="submissionBoardPK" value="<%=submissionBoardPK%>" />
		                  <input type="hidden" name="appSubmissionType" value="<%=appSubmissionType%>" />
		                  <input type="hidden" name="submissionPK" value="<%=tabsubtype%>" />
		                  <input type="hidden" name="submissionPK" value="<%=studyId%>" />
		                  
		                 
			            </form>
				   		<script>
				   			document.history.submit();
				   		</script>
						
						
			<%	submhist = true;	}
					if (formFlagIndex > -1 && selType.equals("AllCheck*F"))
					{
						checkedFormList = true;
					}
					
					if (formFlagIndex > -1 && checkedFormList == false)
					{
						formPK = selType.substring(0,formFlagIndex);
						//get form respnse information
						htResponseInfo  = lnkformB.getLatestStudyFormResponeData(formPK,studyId);	
		  					
		  				formlibver = (String) htResponseInfo.get("formlibver");	
		  				pkstudyforms = (String) htResponseInfo.get("pkstudyforms");	
		  				
		  				url = "formprint.jsp?formId="+formPK+"&filledFormId="+pkstudyforms+"&studyId="+studyId+"&formLibVerNumber="+formlibver+"&linkFrom=S";
		  				
		  				hasFormResponse = true;
					}
					
					if (formFlagIndex == -1 && checkedFormList == false && EJBUtil.stringToNum(selType) > 0)
					{
						AppendixJB aJB = new AppendixJB();
						
						AppendixDao apDao = new AppendixDao();
						if(appSubmissionType.equals("prob_rpt") || appSubmissionType.equals("cont_rev") || appSubmissionType.equals("closure")){
							apDao =	aJB.getLatestDocumentForSubmission(studyId,selType,submissionDate,statCreatedOn);
						}else{
							apDao =	aJB.getLatestDocumentForCategory(studyId,selType,submissionPK);
						}
						if (apDao != null && apDao.getCRows() > 0)
						{
							filename = (String)(apDao.getAppendixFile_Uris()).get(0);
							filePk = ((Integer)(apDao.getAppendixIds()).get(0)).intValue();
							message = LC.L_Loading_Document/*"Loading document*****/+".....";
						 } 
						else
						{
							
							message = MC.M_NoDocu_UploadCat/*"No documents uploaded for this Category"*****/;	
						}
					
					if (! StringUtil.isEmpty(filename) && filePk > 0)
					{
						
					com.aithent.file.uploadDownload.Configuration.readSettings("eres");
					com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "study");		  
					dnldurl = com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET ;
				
					url =  dnldurl +  "?file="+filename + "&pkValue="+filePk+
					"&tableName=ER_STUDYAPNDX&columnName=STUDYAPNDX_FILEOBJ&pkColumnName=PK_STUDYAPNDX&module=study&db=eres";
			
					}
						%>
							
							<%= message %>
								
							<%   
				   if (! StringUtil.isEmpty(url) && checkedFormList == false)
				   {
				   	%>
			            <form name="dummy" method="post" action="<%=dnldurl%>" target="_self">
				          <input type="hidden" name="file" value="<%=filename%>" />
		                  <input type="hidden" name="pkValue" value="<%=filePk%>" />
		                  <input type="hidden" name="tableName" value="ER_STUDYAPNDX" />
		                  <input type="hidden" name="columnName" value="STUDYAPNDX_FILEOBJ" />
		                  <input type="hidden" name="pkColumnName" value="PK_STUDYAPNDX" />
		                  <input type="hidden" name="module" value="study" />
		                  <input type="hidden" name="db" value="eres" />
			            </form>
				   		<script>
				   			document.dummy.submit();
				   		</script>
				   		<%
				   }
					
					
				}
				if (hasFormResponse && checkedFormList == false)
				{
					%>
			            <form name="dummy" method="post" action="formprint.jsp" target="_self">
				          <input type="hidden" name="formId" value="<%=formPK%>" />
		                  <input type="hidden" name="filledFormId" value="<%=pkstudyforms%>" />
		                  <input type="hidden" name="studyId" value="<%=StringUtil.htmlEncodeXss(studyId)%>" />
		                  <input type="hidden" name="formLibVerNumber" value="<%=formlibver %>" />
		                  <input type="hidden" name="linkFrom" value="S" />
			            </form>
				   		<script>
				   			document.dummy.submit();
				   		</script>
						<%
				}	
				
				if (checkedFormList == true)
				{
					%>
						<form name="viewcheck" method="post" action="viewSubmissionResponses.jsp" target="_self">
				          <input type="hidden" name="studyId" value="<%=StringUtil.htmlEncodeXss(studyId)%>" />
		                  <input type="hidden" name="appSubmissionType" value="<%=StringUtil.htmlEncodeXss(appSubmissionType)%>" />
		                  <input type="hidden" name="study_acc_form_right" value="<%=study_acc_form_right%>" />
		                  <input type="hidden" name="study_team_form_access_right" value="<%=study_team_form_access_right%>" />
		        	
		               </form>
				   		<script>
				   			document.viewcheck.submit();
				   		</script>
						
						
						<%
				}
				
				if (showLastSubVersion == true)
				{
					System.out.println("pageVer--->"+majVersion);
					System.out.println("pageMinorVer--->"+minVersion);
					%>
						<form name="viewLastSubVer" method="post" action="studyVersionDetails.jsp" target="_self">
				          <input type="hidden" name="studyId" value="<%=StringUtil.htmlEncodeXss(studyId)%>" />
		                  <input type="hidden" name="pageVer" value="<%=majVersion%>" />
		                  <input type="hidden" name="pageMinorVer" value="<%=minVersion%>" />
		                  <input type="hidden" name="isPopupWin" value="1"/>
		               </form>
				   		<script>
				   			document.viewLastSubVer.submit();
				   		</script>
						
						
						<%
				}
				if (showLastResubMemo == true)
				{
					if(majVersion==1 && minVersion==0){
					%>
						<%=LC.L_No_Resub_Memo %>
					<%
					}else{%>
						<form name="viewResubMemo" method="post" action="studyVersionChangeLog.jsp" target="_self">
				          <input type="hidden" name="studyId" value="<%=StringUtil.htmlEncodeXss(studyId)%>" />
		                  <input type="hidden" name="pageVer" value="<%=majVersion%>" />
		                  <input type="hidden" name="pageMinorVer" value="<%=minVersion%>" />
		                  <input type="hidden" name="isPopupWin" value="1"/>
		               </form>
				   		<script>
				   			document.viewResubMemo.submit();
				   		</script>
						
					<%}
				}
				if (showUploadedDocs == true)
				{
					UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
					String versionNum = String.valueOf(majVersion);
					if (majVersion > 0){
						versionNum += ".";
						int versionMinor = uiFlxPageDao.getHighestMinorFlexPageVersionSubmission(majVersion, "er_study",EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(submissionPK));
						versionNum += (versionMinor == 0)? "00" : ((versionMinor > 9)? versionMinor : "0" + versionMinor);
					}
				%>
					   <form name="viewUploadDocs" method="post" action="studyVerBrowser_print.jsp" target="_self">
				          <input type="hidden" name="studyId" value="<%=StringUtil.htmlEncodeXss(studyId)%>" />
		                  <input type="hidden" name="studyVerNumber" value="<%=versionNum%>" />
		                  <%if(appSubmissionType.equals("prob_rpt") || appSubmissionType.equals("cont_rev") || appSubmissionType.equals("closure")){
		                	  if(!CFG.ONGOING_SUBMISSION_CATEGORIES.equals("TBD") && !CFG.ONGOING_SUBMISSION_CATEGORIES.trim().equals("")){
			                	  String verCategories = CFG.ONGOING_SUBMISSION_CATEGORIES;
			                	  String dStudyvercat = "";
			                	  CodeDao cDao = new CodeDao();
									if(verCategories.contains(",")){
										cDao.getCodeValuesBySubTypes("studyvercat",verCategories.split(","));
										System.out.println("studyverCat Size--->"+cDao.getCId().size());
										if(cDao.getCId()!=null && cDao.getCId().size()>0){
											int indx = 0;
											for(Object codeId:cDao.getCId()){
												int cId = (Integer) codeId; 
												System.out.println("cId===="+cId);
												if (indx == 0){
													dStudyvercat=String.valueOf(cId);
												}else{
													dStudyvercat=dStudyvercat+","+String.valueOf(cId);
												}
												indx++;
											}
										}
									}else{
										dStudyvercat=String.valueOf(cDao.getCodeId("studyvercat",verCategories));
									}
		                  	
		                  %>
		                  	<input name="submissionDate" type="hidden" value="<%=submissionDate %>"/>
  				  			<input name="approvalDate" type="hidden" value="<%=statCreatedOn %>"/>
				  			<input type="hidden" name="dStudyvercat" value="<%=dStudyvercat%>"/>
		                  	
		                  <%}} %>
		                  <input type="hidden" name="appSubmissionType" value="<%=appSubmissionType %>"/>
		                  <input type="hidden" name="isPopupWin" value="1"/>
		                  <input type="hidden" name="mode" value="N"/>
		                  <input type="hidden" name="isPrint" value="1"/>
		                  <input type="hidden" name="orderBy" value="STUDYVER_CATEGORY" />
		                  <input type="hidden" name="orderType" value="asc" />
		               </form>
				   		<script>
				   			document.viewUploadDocs.submit();
				   		</script>
						
					
				<%}
			}			
			%>
	
	</body>
		</html>
		