<%@page import="com.velos.eres.web.network.NetworkJB"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/><!--km--> 
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>
<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.EJBUtil"%><%@page import="com.velos.eres.service.util.*"%>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>	

	<%
     boolean expandNtwFlag=false;
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html");
	HttpSession tSession = request.getSession(false);
	//JSONObject jsObj = new JSONObject();
	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		//jsObj.put("result", -1);
		//jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
   	%>
		<input type="hidden" name="resultMsg" id="resultMsg" value="<%=MC.M_UsrNot_LoggedIn %>"/>    	
	<%
	}
		int accountId=0;
		
		String calledFrom;
		calledFrom= request.getParameter("calledFrom")==null?"":request.getParameter("calledFrom");
		String network_flag;
		network_flag= request.getParameter("network_flag")==null?"":request.getParameter("network_flag");
		int pageRight = EJBUtil.stringToNum(request.getParameter("pageRight"));
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		//int	pageRightforuser = Integer.parseInt(grpRights.getFtrRightsByValue("MUSERS"));
		String src = request.getParameter("src");

		String acc = (String) tSession.getValue("accountId");
		accountId = EJBUtil.stringToNum(acc);
		String userId = (String) tSession.getValue("userId");
		int pkuser = EJBUtil.stringToNum(userId);
		String ipAdd = (String) tSession.getValue("ipAdd");
		String moreParam=request.getParameter("moreParam")==null?"":request.getParameter("moreParam");
		String flag = request.getParameter("flag");
		String searchByName=(request.getParameter("searchByName")==null)?"":request.getParameter("searchByName");
		String parentNtwId=(request.getParameter("parentNtwId")==null)?"":request.getParameter("parentNtwId");
		String expandNtw=request.getParameter("expandNtw");
		String studyId=(request.getParameter("studyId")==null)?"":request.getParameter("studyId");
		int usrId = EJBUtil.stringToNum(userId);
		userB.setUserId(usrId);
		userB.getUserDetails();
		String defGroup = userB.getUserGrpDefault();
		int grpId=EJBUtil.stringToNum(defGroup);
		groupB.setGroupId(EJBUtil.stringToNum(defGroup));
		groupB.getGroupDetails();
		String groupName = groupB.getGroupName();
       
		String from=(request.getParameter("from")==null)?"":request.getParameter("from");
		Integer netId=0;
		netId = EJBUtil.stringToNum(request.getParameter("networkId"));
		NetworkDao nwdao = new NetworkDao();
	        // nwdao = new NetworkDao();
		if("D".equals(flag)){
				if(nwdao.getStudyNetworkCount(netId)>0){%>
				<input type="hidden" name="resultMsg" id="resultMsg" value="true"/> 
				<%
				return;}
				else if(nwdao.getNetworkStatusSubtyp(netId).equalsIgnoreCase("notPending")){%>
					<input type="hidden" name="resultMsg" id="resultMsg" value="notPending"/> 
				<% return;}
				nwdao.deleteNetwork(userId, ipAdd, netId);
		}
		/*Fix for Bug 30834 - Manage Account >> Organizations in Network Top Network (Level 0) cannot be repeated. */
       if("duplicateNet".equals(flag)){
			int mainNetId=0;  //root network id and it will be used to validate existing network.
			String mainSiteId=(request.getParameter("mainSiteId")==null)?"":request.getParameter("mainSiteId");//getting the main site Id.
			mainNetId=nwdao.getMainNetwork(parentNtwId,mainSiteId,accountId);
			%>
			<input type="hidden" name="mainNetId" id="mainNetId" value="<%=mainNetId%>"/>
			<%
			return;
		}
		/*End */
		ArrayList HistoryIdsList = new ArrayList();
		ArrayList StdNtwrkStatsList = new ArrayList();
		ArrayList HistoryIdsChList = new ArrayList();
		ArrayList StdNtwrkStatsChList = new ArrayList();
		nwdao = new NetworkDao();
		ArrayList ctepIdList=new ArrayList();
		nwdao.getNetworkValues("parent",accountId,moreParam);
				
		ArrayList networkIdList = nwdao.getNetworkIdList();
		ArrayList siteNameList = nwdao.getSiteNameList();
		ctepIdList = nwdao.getCtepIdList();
		ArrayList siteIdList = nwdao.getSiteIdList();
		ArrayList networkLevelList = nwdao.getNetworkLevelList();
		ArrayList networkTypeIdList = nwdao.getNetworkTypeIdList();
		ArrayList networkTypeDescList = nwdao.getNetworkTypeDescList();
		ArrayList networkStatusIdList = nwdao.getNetworkStatusIdList();
		ArrayList networkStatusSubTypList = nwdao.getNetworkStatusSubTypList();
		ArrayList networkStatusDescList = nwdao.getNetworkStatusDescList();
		HistoryIdsList = nwdao.getHistoryIdsList();
		nwdao = new NetworkDao();
		ArrayList ctepIdChList = new ArrayList();
		nwdao.getNetworkValues("child",accountId,moreParam);
		ArrayList networkIdChList = nwdao.getNetworkIdList();
		ArrayList networkSiteIdChList = nwdao.getNetworkSiteIdList();
		ArrayList siteNameChList = nwdao.getSiteNameList();
		ctepIdChList = nwdao.getCtepIdList();
		ArrayList siteIdChList = nwdao.getSiteIdList();
		ArrayList networkLevelChList = nwdao.getNetworkLevelList();
		ArrayList networkTypeIdChList = nwdao.getNetworkTypeIdList();
		ArrayList networkTypeDescChList = nwdao.getNetworkTypeDescList();
		ArrayList networkMainIdChList = nwdao.getNetworkMainIdList();
		ArrayList networkStatusIdChList = nwdao.getNetworkStatusIdList();
		ArrayList networkStatusDescChList = nwdao.getNetworkStatusDescList();
		ArrayList networkStatusSubTypChList = nwdao.getNetworkStatusSubTypList();
		HistoryIdsChList = nwdao.getHistoryIdsList();  
		    String networkId = "";
		    String siteId = "";
		    String siteName = "";
		    String ctepId = "";
		    String networkLevel = "";
		    String networkTypeId = "";
		    String networkTypeDesc = "";
		    String networkSiteId = "";
		    String networkMainId = "";
		    String networkStatusDesc = "",networkStatusSubTyp="";
		    String networkStatusId="";
		    String historyId = "";
		    String sntwStat = "";
			int counter = 0;
			int counterCh = 0;
		    int len = networkIdList.size();
			int count = 0;
			
			if((!siteNameList.equals("") && calledFrom.equals("networkTabs"))||(calledFrom.equals("networkLookup"))){
			%><tr>
			<%if(calledFrom.equals("networkLookup")){ %>
			<td></td>
				<td><input  placeholder="Filter" name="searchNetWorkByName1" id="searchNetWorkByName1" class="search-table midAlign" onkeyup="networkSiteByName1(event);" autocomplete="off" type="input" style="border-radius:5px;border: 2px solid #aaa;padding:2px 2px 2px 0px;"></td>
				<td><input  placeholder="Filter" name="searchNetWorkByparticipationType" id="searchNetWorkByparticipationType" class="search-table midAlign" onkeyup="participationType(event);" autocomplete="off" type="input" style="border-radius:5px;border: 2px solid #aaa;padding:2px 2px 2px 0px;"></td>
				<td><input  name="searchNetWorkByStatus" placeholder="Filter" id="searchNetWorkByStatus" onkeyup="NetWorkByStatus(event);" autocomplete="off" type="input" style="border-radius:5px;border: 2px solid #aaa;padding:2px 2px 2px 0px;"></td>
			<%}else{%> 
			<td ><input style="width:99%"   placeholder="Filter" name="searchNetWorkByName1"  id="searchNetWorkByName1" class="search-table midAlign netfilter" onkeyup="networkSiteByName1(event);" autocomplete="off"  ></td>
			<td ><input style="width:99%" placeholder="Filter" name="searchNetWorkByparticipationType" id="searchNetWorkByparticipationType" class="search-table midAlign netfilter" onkeyup="participationType(event);" autocomplete="off"  ></td>
			<td ><input style="width:97%" placeholder="Filter" name="searchNetWorkByStatus" id="searchNetWorkByStatus" class="search-table midAlign netfilter" onkeyup="NetWorkByStatus(event);" autocomplete="off" ></td>
			<td><input style="width:97%" placeholder="Filter" name="searchNetWorkByCTEPID" id="searchNetWorkByCTEPID" class="search-table midAlign netfilter" onkeyup="searchByCtepId(event);" autocomplete="off" ></td><td></td><td></td><td></td><td></td>	
			<%}%>
			</tr>
			<%	}
    		if(len!=0)
    		{
			    for(counter = 0;counter<len;counter++)
				{ 
			    	networkId = networkIdList.get(counter).toString();
			    	siteId = siteIdList.get(counter).toString();
				    siteName = siteNameList.get(counter).toString();
				    ctepId = (ctepIdList.get(counter))==null?"-":(ctepIdList.get(counter).toString()); 
				    String parentSiteName=siteName;
				    networkLevel = networkLevelList.get(counter).toString();
				    networkTypeId = (networkTypeIdList.get(counter)==null)?"0":networkTypeIdList.get(counter).toString();
				    networkTypeDesc = (networkTypeDescList.get(counter)==null)?LC.L_Select_AnOption:networkTypeDescList.get(counter).toString();
				    networkStatusSubTyp=networkStatusSubTypList.get(counter).toString();
				    networkStatusDesc = networkStatusDescList.get(counter).toString();
				    networkStatusId=networkStatusIdList.get(counter).toString();
				    historyId = HistoryIdsList.get(counter)==null?"":HistoryIdsList.get(counter).toString();
				    CodeDao cd1 = new CodeDao();
					cd1.getCodeValues("relnshipTyp");
					 CodeDao cd2 = new CodeDao();
					 cd2.getCodeValues("networkstat");
					String dSiteType="";
					//String dSiteStatus="";
					dSiteType = cd1.toPullDown("siteTypeId_"+siteId+"_"+count,StringUtil.stringToInteger(networkTypeId),"networkTabs","style='display:none;' class='ddSelector' onfocusout='openDDList(this.id,1,7);' ");
					//dSiteStatus=cd2.toPullDown("siteStatusId_"+siteId+"_"+count,StringUtil.stringToInteger(networkStatusId),"networkTabs","style='display:none;' onblur='openDDstatusList(this.id,1);'");
					
					%>
					<%
					if(!expandNtw.equals("")){
						if(expandNtw.indexOf("row_"+networkId+"_")>=0)
							expandNtwFlag=true;
					}
					%>
				
					
					<tr id="row_<%=networkId%>_<%=networkLevel%>" class="sortable not" style="display: table-row;"> 
					
					<%if(calledFrom.equals("networkLookup")){
							%>
					 			<td><input type="checkbox"  <%=(pageRight<5 || pageRight==6) ?"disabled":"" %> id="<%=networkId%>_<%=networkLevel%>" name="ntwchecked" value="<%=networkId%>_<%=networkLevel%>||<%=siteName%>||<%=networkTypeDesc%>||<%=networkStatusDesc%>" onclick="validate_selectntw(this,'<%=networkId%>')"/></td>
					 		<%} %>						 

	        				<td style="padding-left:<%=EJBUtil.stringToNum(networkLevel)+EJBUtil.stringToNum(networkLevel)%>%;"> 
	        				<%if(networkMainIdChList.contains(networkId)){%>
	        				<img style="height:12px;width:12px;" src="./images/formright.gif" border="0" onclick="showHide(this)">
	        				
	        				<%}else{%>
	        				<img style="height:12px;width:12px;" src="./images/img-curv1.png" border="0" ">
	        				<%} %>
	        			
	        					<A href="#" id="orgName_<%=networkId%>" onclick="fOpenNetwork(<%=siteId%>,<%=networkId%>,'<%=networkLevel%>');">
	        				<!-- Commited For Bug 30835 by Deepti 
	        				Increased the sitename length to 40 characters-->
	        				<%if(siteName.length()>40){ %>
	          				<%=siteName.substring(0,40)%>&nbsp;<span onMouseOver="return overlib('<%=siteName%>',ABOVE,CAPTION,'<%=LC.L_Organization_Name%>');" onMouseOut="return nd();">...</span>
	          				<%}else{ %>
	          					 <%=siteName%>  
	          				<%} %>
	          				</A>
	          				<%if("networkTabs".equals(calledFrom)){ %>
	          					<!-- Commited For Bug 30824 by Deepti 
	          					Addded the title attribute to show on tooltip while adding the child network-->
	          					<img src="../images/add.png" align="right" title="Add Child to this Site"  style="cursor: pointer;" border="0" onclick="createNetworkSites('child','row_<%=networkId%>_<%=networkLevel%>');">
	          				<%}%>
	          				</td>
	          				<%
	          					if(calledFrom.equals("networkLookup")){%>
	        						<td><%=networkTypeDesc%></td>
	        						<td><span id="span_status_<%=siteId%>_<%=count%>"><%=networkStatusDesc%></span><%--=dSiteStatus--%></td>
	        					<%}else{%>
									<td> <span class="ddSpan" id="span_ntType_<%=siteId%>_<%=count%>" onclick="openDDList(this.id,0,'<%=pageRight%>');"><%=networkTypeDesc%></span><%=dSiteType%>
									
									<A href="#" id="span_ntType_<%=siteId%>_<%=count%>" onclick="openDDList(this.id,0,'<%=pageRight%>');"><img src="./images/edit.gif" title="<%=LC.L_Edit%>" border="0" align="right" /></A>&nbsp;&nbsp;
									<A href="#" id="span_ntType_<%=siteId%>_<%=count%>_cncl_img" name="span_ntType_<%=siteId%>_<%=count%>_cncl_img" onclick="cancelType('span_ntType_<%=siteId%>_<%=count%>');" style="display: none;"><img src="../images/jpg/delete_pg.png" title="<%=LC.L_Cancel%>" border="0" align="right"/></A>&nbsp;&nbsp;
	        						<A href="#" id="span_ntType_<%=siteId%>_<%=count%>_save_img" name="span_ntType_<%=siteId%>_<%=count%>_save_img" onclick="openDDList('siteTypeId_<%=siteId%>_<%=count%>',1,'<%=pageRight%>');" style="display: none;"><img src="../images/assets/correct.jpg" title="<%=LC.L_Save%>" border="0" align="right"/></A>&nbsp;&nbsp;
								
									</td>
									<td>
	        						<a href="#"><span id="span_status_<%=siteId%>_<%=count%>" onclick="openWinStatus('M',this.id,'<%=historyId%>','<%=pageRight%>')"><%=networkStatusDesc%></span></a><%--<%=dSiteStatus%>--%>&nbsp;&nbsp;
	        						<A href="#" id="span_status_<%=siteId%>_<%=count%>" onclick="openWinStatus('N',this.id,'0','<%=pageRight%>')"><img src="./images/edit.gif" title="<%=LC.L_Edit%>" border="0" align="right"/></A>&nbsp;&nbsp;
									<A href="#" onclick="openWinHistory('<%=networkId%>','<%=pageRight%>','<%=historyId%>','<%=parentNtwId%>')"><img border="0" title="<%=LC.L_History%>" alt="<%=LC.L_History%>" src="./images/History.gif" align="right"/></A>&nbsp;&nbsp;
									</td>
							<td id="ctepId_<%=networkId %>"><%=ctepId%></td>
	        					<%}%>
							<%if(!calledFrom.equals("networkLookup")){%>
	        					<td><A href="#" onclick="fAddMultiUserToSite('<%=networkId %>','<%=networkLevel%>','<%=pageRight%>');"><img title="Network Users" src="./images/User.gif" border="0"></A></td>
	        					<td align="center"><A href="#" onclick="networkSitesAppendix('<%=networkId %>','<%=siteId %>','<%=pageRight%>');"><img title="Network Appendix" src="./images/Appendix.gif" border="0" >
	        				<%}%>
	        				<input type="hidden" id="siteName<%=siteId %>" value="<%=siteName%>"></input>
	        				<input type="hidden" id="siteType" value=""></input>
	        				<input type="hidden" id="networkType<%=networkId %>" value="<%=networkTypeDesc%>"></input>
						<input type="hidden" name="pageRight" value="<%=pageRight%>">
						</A>
	        				</td>
	        				<%if("networkTabs".equals(calledFrom)){%>
	        				<td>
	        				<img title="Forms" src="./images/Form.gif" style="cursor: pointer;" onclick="openformwin(<%=networkId%>)">
	        				</td>
	        				<td>
	        				<% if(groupName.equalsIgnoreCase("Admin") && networkStatusSubTyp.equalsIgnoreCase("pending")) {%>
	        				<a href="#" onclick="deleteNetwork(<%=networkId%>);"><img src="./images/delete.gif" title="Delete" border="0"></a>
							<%} %>
	        				<%}%>
	        				</td>
	      				</tr>
		   				<%
		   				count++;
		   				if(networkMainIdChList.contains(networkId)){
		   				for(counterCh=0;counterCh<networkIdChList.size();counterCh++){
		   					if(networkMainIdChList.get(counterCh).toString().equals(networkIdList.get(counter).toString())){
		   					networkId = networkIdChList.get(counterCh).toString();
					    	siteId = siteIdChList.get(counterCh).toString();
						    siteName = siteNameChList.get(counterCh).toString();
					    	ctepId = (ctepIdChList.get(counterCh))==null?"-":(ctepIdChList.get(counterCh).toString()); //}
						    networkLevel = networkLevelChList.get(counterCh).toString();
						    networkTypeId = (networkTypeIdChList.get(counterCh)==null)?"0":networkTypeIdChList.get(counterCh).toString();
						    networkTypeDesc = (networkTypeDescChList.get(counterCh)==null)?LC.L_Select_AnOption:networkTypeDescChList.get(counterCh).toString();
						    networkSiteId = networkSiteIdChList.get(counterCh).toString();
						    networkMainId = networkMainIdChList.get(counterCh).toString();
						    networkStatusSubTyp = networkStatusSubTypChList.get(counterCh).toString();
						    networkStatusDesc = networkStatusDescChList.get(counterCh).toString();
						    networkStatusId=networkStatusIdChList.get(counterCh).toString();
						    dSiteType="";
						    historyId = HistoryIdsChList.get(counterCh)==null?"":HistoryIdsChList.get(counterCh).toString();						    
						    dSiteType = cd1.toPullDown("siteTypeId_"+siteId+"_"+count,StringUtil.stringToInteger(networkTypeId),"networkTabs","style='display:none;' class='ddSelector' onfocusout='openDDList(this.id,1,7);' ");
                            //dSiteStatus=cd2.toPullDown("siteStatusId_"+siteId+"_"+count,StringUtil.stringToInteger(networkStatusId),"networkTabs","style='display:none;' onblur='openDDstatusList(this.id,1);'");
						    %>
						    <%if(!network_flag.equalsIgnoreCase("")){ %>
						     <tr id="row_<%=networkMainId%>_<%=networkLevel%>_<%=networkId%>" class="sortable not" >
						    <%}else{ %>
						     <tr id="row_<%=networkMainId%>_<%=networkLevel%>_<%=networkId%>" class="sortable not" style="display:none;"> 
						    <%} %>
							<%if(calledFrom.equals("networkLookup")){
								%>
					 			<td><input type="checkbox" <%=(pageRight<5 || pageRight==6) ?"disabled":"" %> id="<%=networkMainId%>_<%=networkLevel%>_<%=networkId%>" name="ntwchecked" value="<%=networkMainId%>_<%=networkLevel%>_<%=networkId%>||<%=siteName%>||<%=networkTypeDesc%>||<%=networkStatusDesc%>" onclick="validate_selectntw(this,'<%=networkId%>')"/></td>
					 		<%} %>																				 																																										 
	        				<td style="padding-left:<%=EJBUtil.stringToNum(networkLevel)+EJBUtil.stringToNum(networkLevel)%>%;">
	        				<%if((counterCh<networkIdChList.size()-1) && networkSiteIdChList.get(counterCh+1).toString().equals(networkId)){ %>
	        				<%if(!network_flag.equalsIgnoreCase("")){ %>
	        				<img style="height:12px;width:12px;" src="./images/formdown.gif" border="0" onclick="showHide(this)">
	        				<%}else{ %>
	        				<img style="height:12px;width:12px;" src="./images/formright.gif" border="0" onclick="showHide(this)">
	        				<%} %>
	        				<%}else
	        				{%>
	        				<img style="height:12px;width:12px;" src="./images/img-curv1.png" border="0" ">
	        				<%} %>
	        				<A href="#" id="orgName_<%=networkId%>" onclick="fOpenNetwork(<%=siteId%>,<%=networkId%>,'<%=networkLevel%>');">
	          				  <!-- Commited For Bug 30835 by Deepti 
	          				  Increased the sitename length to 40 characters-->
	          				  <%if(siteName.length()>40){ %>
	          					<%=siteName.substring(0,40)%>&nbsp;<span onMouseOver="return overlib('<%=siteName%>',ABOVE,CAPTION,'<%=LC.L_Organization_Name%>');" onMouseOut="return nd();">...</span>
	          				  <%}else{ %>
	          					 <%=siteName%>  
	          				  <%} %>
	          				</A>
	          				  <%if("networkTabs".equals(calledFrom)){ %>
	          				    <!-- Commited For Bug 30824 by Deepti 
	          				    Addded the title attribute to show on tooltip while adding the child network-->
	          					<img src="../images/add.png" style="cursor: pointer;" title="Add Child to this Site"  align="right" border="0" onclick="createNetworkSites('child','row_<%=networkMainId%>_<%=networkLevel%>_<%=networkId%>');">
	          				  <%}%>
	          				</td>
	          				<%
							if(calledFrom.equals("networkLookup")){%>
								 <td><%=networkTypeDesc%></td> 
							<%}else{%>
	        				<td> <span class="ddSpan"  id="span_ntType_<%=siteId%>_<%=count%>" onclick="openDDList(this.id,0,'<%=pageRight%>');"><%=networkTypeDesc%></span><%=dSiteType%>
	        					<A href="#" id="span_ntType_<%=siteId%>_<%=count%>" onclick="openDDList(this.id,0,'<%=pageRight%>');"><img src="./images/edit.gif" title="<%=LC.L_Edit%>" border="0" align="right"/></A>&nbsp;&nbsp;
	        				<!-- New Code for Network Bug 30817-->
	        					<A href="#" id="span_ntType_<%=siteId%>_<%=count%>_cncl_img" name="span_ntType_<%=siteId%>_<%=count%>_cncl_img" onclick="cancelType('span_ntType_<%=siteId%>_<%=count%>');" style="display: none;"><img style="height:19px;width:19px;" src="../images/jpg/delete_pg.png" title="<%=LC.L_Cancel%>" border="0" align="right"/></A>&nbsp;&nbsp;
	        					<A href="#" id="span_ntType_<%=siteId%>_<%=count%>_save_img" name="span_ntType_<%=siteId%>_<%=count%>_save_img" onclick="openDDList('siteTypeId_<%=siteId%>_<%=count%>',1,'<%=pageRight%>');" style="display: none;"><img style="height:19px;width:19px;" src="../images/assets/correct.jpg" title="<%=LC.L_Save%>" border="0" align="right"/></A>&nbsp;&nbsp;
	        				</td>
						<%}%>
							<%if(calledFrom.equals("networkLookup")){%>
	        	        				<td> <span id="span_status_<%=siteId%>_<%=count%>"><%=networkStatusDesc%></span><%--=dSiteStatus--%> </td>
	        	        				<%}else{%>
	        	        				<td>
	        						<a href="#"><span id="span_status_<%=siteId%>_<%=count%>" onclick="openWinStatus('M',this.id,'<%=historyId%>','<%=pageRight%>')"><%=networkStatusDesc%></span></a><%--<%=dSiteStatus%>--%>&nbsp;&nbsp;
	        						<A href="#" id="span_status_<%=siteId%>_<%=count%>" onclick="openWinStatus('N',this.id,'0','<%=pageRight%>')"><img src="./images/edit.gif" title="<%=LC.L_Edit%>" border="0" align="right"/></A>&nbsp;&nbsp;
									<A href="#" onclick="openWinHistory('<%=networkId%>','<%=pageRight%>','<%=historyId%>','<%=parentNtwId%>')"><img border="0" title="<%=LC.L_History%>" alt="<%=LC.L_History%>" src="./images/History.gif" align="right"/></A>
										<td id="ctepId_<%=networkId%>"><%=ctepId%></td>
								<%}%>									   
							<%if(!calledFrom.equals("networkLookup")){%>
	        					<td><A href="#" onclick="fAddMultiUserToSite('<%=networkId %>','<%=networkLevel%>','<%=pageRight%>');"><img title="Network Users" src="./images/User.gif" border="0"></A></td>
	        					<td align="center"><A href="#" onclick="networkSitesAppendix('<%=networkId %>','<%=siteId %>','<%=pageRight%>');"><img title="Network Appendix" src="./images/Appendix.gif" border="0">
	        				<%}%>
	        				<input type="hidden" id="siteName<%=siteId %>" value="<%=siteName%>"></input>
	        				<input type="hidden" id="siteType" value=""></input>
	        				<input type="hidden" id="networkType<%=networkId %>" value="<%=networkTypeDesc%>"></input>
	        				</A> 
	        				</td>
	        				<%if("networkTabs".equals(calledFrom)){%>
	        				<td>
	        				<img title="Forms" src="./images/Form.gif" style="cursor: pointer;" onclick="openformwin(<%=networkId%>)">
	        				</td><td>
	        				<% if(groupName.equalsIgnoreCase("Admin") && networkStatusSubTyp.equalsIgnoreCase("pending")) {%>
							<a href="#" onclick="deleteNetwork(<%=networkId%>);"><img src="./images/delete.gif" title="Delete" border="0"></a>
							<%} %>
	        				<%}%>
	        				</td>
	      					</tr>
		   					<%	count++;}
		   					}
		   				}
		   				
				}%>
				<input type="hidden" name="expandNtw" id="expandNtw" value="<%=expandNtw%>">
		<%	}
			%>
<style>
.netfilter{border-radius:5px;border: 2px solid #aaa;padding:2px 2px 2px 0px;" }
</style>