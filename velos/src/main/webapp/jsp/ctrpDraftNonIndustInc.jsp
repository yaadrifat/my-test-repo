<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<%@ page import="java.util.*,com.velos.eres.service.util.*" %>
<%@page import="com.velos.eres.business.common.CodeDao"%>
<%@page import="com.velos.eres.service.util.VelosResourceBundle"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>
<%
String draftId = request.getParameter("draftId");
if (draftId == null) { draftId = request.getParameter("ctrpDraftJB.id"); }
int ctrpRights=0;
try {
	GrpRightsJB grpRights = (GrpRightsJB) request.getSession(false).getAttribute("GRights");
	if (grpRights == null) { return; }
	ctrpRights=Integer.parseInt(grpRights.getFtrRightsByValue("REGREPORT_CTRP"));
} catch(Exception e) { return; } 
%>
<%
CodeDao cdDraftStatus = new CodeDao();
String readyStat = cdDraftStatus.getCodeDescription(cdDraftStatus.getCodeId("ctrpDraftStatus", "readySubmission"));
Object[] params = { readyStat, readyStat };	

//22472
String draftDwlddStat = cdDraftStatus.getCodeDescription(cdDraftStatus.getCodeId("ctrpDraftStatus", "DWLD"));
String draftWipStat = cdDraftStatus.getCodeDescription(cdDraftStatus.getCodeId("ctrpDraftStatus", "wip"));

CodeDao cdDraftNCIStatus = new CodeDao();
String nciOnHoldStat = cdDraftNCIStatus.getCodeDescription(cdDraftStatus.getCodeId("nciTrialStatus", "onHold"));
String nciRejectedStat = cdDraftNCIStatus.getCodeDescription(cdDraftStatus.getCodeId("nciTrialStatus", "rejected"));

%>
<script type="text/JavaScript">
var lastTimeSubmitted = 0;
var isValidatedForm = false;
var constPrinv = 'prinv';
var constleadOrg = 'leadOrg';
var constSponsor = 'sponsor';
var constSponsResp = 'sponsResp';
var constPersonalContact = 'personalContact';
var constSum4Org = 'sum4Org';

var draftDwlddStat = '<%=draftDwlddStat %>';
var draftWipStat = '<%=draftWipStat %>';
var nciOnHoldStat = '<%=nciOnHoldStat %>';
var nciRejectedStat = '<%=nciRejectedStat %>';
var intialDraftStat;

$j(document).ready(function(){
	intialDraftStat = $j('#ctrpDraftJB\\.ctrpDraftStatus option:selected').text();
	// For a new draft, only WIP is permissble  
	var draftId = $j('#ctrpDraftJB\\.id').val();
	if(draftId = null || draftId == '' || draftId==0){
		$j('#ctrpDraftJB\\.ctrpDraftStatus').find("option:[text!='"+draftWipStat+"']").remove();
	}
	// Remove 'Downloaded' if Downloaded is not the current Status of the Draft
	if((intialDraftStat!=draftDwlddStat)){
		$j('#ctrpDraftJB\\.ctrpDraftStatus').find("option:[text='"+draftDwlddStat+"']").remove();
	}

	// For draft status other than WIP, Draft should be non editbale
	if(intialDraftStat!=draftWipStat){
		$j("#draftFldContainerDiv").find('.datefield').datepicker("destroy").removeClass("datefield");
		$j("#draftFldContainerDiv").find("select,input,textarea").bind("focus", function(){
			$j(this).unbind("click");
			$j(this).unbind("dblclick");
			$j(this).blur();
			return false;
		 });
		
		$j("#draftFldContainerDiv").find("button").attr("disabled", 'true');
		$j("#draftFldContainerDiv").find("a").click(function() { return false; });
		$j('input[type="radio"]').focus(function(e) {
			alert(M_CTRP_DRFT_EDIT);
		});
	}
});

function fnOnceEnterKeyPress(e) {
	try {
        if (e.keyCode == 13 || e.keyCode == 10) {
            var thisTimeSubmitted = new Date();
            if (!lastTimeSubmitted) { return true; }
            if (!thisTimeSubmitted) { return true; }
            if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
                return false;
            }
            lastTimeSubmitted = thisTimeSubmitted;
        }
	} catch(e) {}
	return true;
}
function fnClickOnce(e) {
	try {
        var thisTimeSubmitted = new Date();
        if (!lastTimeSubmitted) { return true; }
        if (!thisTimeSubmitted) { return true; }
        if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
            return false;
        }
        lastTimeSubmitted = thisTimeSubmitted;
	} catch(e) {}
    return true;
}
function fnReplicateESign() {
	try {
		$('eSign').value = $('eSign1').value;
	} catch(e) {}
}
function fnClearESign() {
	try {
		if ($('eSign')) {
			$('eSign').value = '';
			$j('#eSign').removeClass('validation-pass'); 
			$j('#eSign').removeClass('validation-failed');
		}
		if ($('eSignMessage')) { 
			$('eSignMessage').innerHTML = '';
			$j('#eSignMessage').removeClass('validation-pass');
			$j('#eSignMessage').removeClass('validation-failed');
		}
		if ($('eSign1')) {
			$('eSign1').value = '';
			$j('#eSign1').removeClass('validation-pass'); 
			$j('#eSign1').removeClass('validation-failed'); 
		}
		if ($('eSignMessage1')) {
			$('eSignMessage1').innerHTML = '';
			$j('#eSignMessage1').removeClass('validation-pass'); 
			$j('#eSignMessage1').removeClass('validation-failed');
		}
	} catch(e) {}
}
function runCtrpValidationReport(draftId){
	var win = window.open("ctrpDraftValidationReport.jsp?draftId="+draftId, "printerWin",
					'toolbar=no,scrollbars=yes,resizable=yes,menubar=yes,status=yes,width=950,height=700');
	if (navigator.userAgent.indexOf("MSIE") > -1) { win.location.reload(); }
	win.focus();
}
function openSelectUser(targetId) {
	$j("#selectUserDialog").dialog({
		modal:true,
		resizable:true,
		height:500,
		width:900,
		closeText: '',
		close: function() { $j("#selectUserDialog" ).dialog("destroy"); }
	})
	loadUserSearchResults(targetId);
};
function openSelectOrg(targetId) {
	$j("#selectLookupDialog").dialog({
		modal:true,
		resizable:true,
		height:500,
		width:900,
		closeText: '',
		close: function() { $j("#selectLookupDialog" ).dialog("destroy"); }
	})
	loadOrgLookupResults(targetId);
};
function validateThisDraft() {
	resetErrorMessages();
	$j.post('validateCtrpDraftNonIndustrialBusiness',
		$j('#ctrpDraftNonIndustrialForm').serialize(),
		function(data) {
			var errorMap = data.errorMap;
			var hasErrors = false;
			for (var key in errorMap) {
				hasErrors = true;
				isValidatedForm = false;
				if ($(key+'_error')) { $(key+'_error').style.display = 'block'; }
				if ($(key+'_error')) { $(key+'_error').innerHTML = '<div align=right>'+errorMap[key]+'</div>&nbsp;'; }
			}
			if (hasErrors) {
				$j('#validateDraftFailedDialog').dialog({
					modal:true,
					height:300,
					width:300,
					closeText: '',
					close: function() {
						$j("#validateDraftFailedDialog" ).dialog("destroy");
						try {
							for (var key in errorMap) {
								if ($(key)) {
									$(key).focus();
									$(key).select();
								} else {
									var elemByName = document.getElementsByName(key);
									elemByName[0].focus();
									elemByName[0].select();
								}
								break;
							}
						} catch(e) {}
					}
				});
			} else {
				$j('#validateDraftDialog').dialog({
					modal:true,
					height:300,
					width:500,
					closeText: '',
					close: function() {
						fnClearESign(); $j("#validateDraftDialog" ).dialog("destroy");
					}
				});
			}
		}
	);
}
function validateForm() {

	var newDraftStat = $j('#ctrpDraftJB\\.ctrpDraftStatus option:selected').text();
	var newNciStat = $j('#ctrpDraftJB\\.ctrpNciProcStatId option:selected').text();
	if((newNciStat==nciOnHoldStat || newNciStat==nciRejectedStat) && newDraftStat!=draftWipStat){
		alert(M_CTRP_WIP_NCI_STAT);
		$j('#ctrpDraftJB\\.ctrpDraftStatus').focus();
		return false;
	}	
	if(newNciStat != nciOnHoldStat && newNciStat != nciRejectedStat){
		if(newDraftStat == draftWipStat && intialDraftStat != draftWipStat && intialDraftStat != draftDwlddStat){
			alert(M_CTRP_WIP_DRFT_Stat);
			$j('#ctrpDraftJB\\.ctrpNciProcStatId').focus();
			return false;
		}
	}	
	
	if (isValidatedForm) { return true; }
	resetErrorMessages();
	$j.post('validateCtrpDraftNonIndustrialBasic',
		$j('#ctrpDraftNonIndustrialForm').serialize(),
		function(data) {
			var errorMap = data.errorMap;
			var hasErrors = false;
			for (var key in errorMap) {
				hasErrors = true;
				isValidatedForm = false;
				if ($(key+'_error')) { $(key+'_error').style.display = 'block'; }
				if ($(key+'_error')) {
					if (key.indexOf("eSign")>-1) {
						$(key+'_error').innerHTML = errorMap[key];
					}else if (key.indexOf("DuplicateDocDesc")>-1) {
						$(key+'_error').innerHTML = '<div align=center>'+errorMap[key]+'</div>&nbsp;';
					}else {
						$(key+'_error').innerHTML = '<div align=right>'+errorMap[key]+'</div>&nbsp;';
					}
				}
			}
			try {
				for (var key in errorMap) {
					if (key.indexOf("eSign")>-1) {
						$("eSign").focus();
						$("eSign").select();
					} else {
						if ($(key)) {
							$(key).focus();
							$(key).select();
						} else {
							var elemByName = document.getElementsByName(key);
							elemByName[0].focus();
							elemByName[0].select();
						}
					}
					break;
				}
			} catch(e) {}
			if (!hasErrors) {
				isValidatedForm = true;
				$("ctrpDraftNonIndustrialForm").onSubmit=null;
				$("ctrpDraftNonIndustrialForm").action ="updateCtrpDraftNonIndustrial";
				$("ctrpDraftNonIndustrialForm").submit();
				$("ctrpDraftNonIndustrialForm").action = '#';
				$("ctrpDraftNonIndustrialForm").onSubmit = " return validateForm();";
			}
		}
	);	
	return false;
}
function resetErrorMessages() {
	$j('#ctrpDraftNonIndustrialForm *').each(function() {
		var patternStr = /.*_error$/;
		if (this.id && this.id.match(patternStr)) {
			this.style.display = 'none';
		}
	});
}
function callFetchMiscJSONSite(targetId, siteId) {
	jQuery.ajax({
		url:'fetchMiscJSON.jsp?reqType=orgDetails&siteId='+siteId,
		success: function(data){
			fillSelectedOrgData(targetId, data, siteId);
		}
	});
}
function fetchLookupDetails(targetId, valueStr) {
	if (!targetId || !valueStr) { return; }
	if (valueStr == "remove") {
		removeSelectedOrg(targetId);
		return;
	}
	var startIndex = valueStr.indexOf('[pk_site=');
	var endIndex = valueStr.indexOf(']', startIndex);
	if (startIndex-9 < 0 || endIndex < 0 || endIndex < startIndex) {
		return;
	}
	var siteId = valueStr.substring(startIndex+9, endIndex);
	callFetchMiscJSONSite(targetId, siteId);
}
function fillSelectedOrgData(targetId, data, siteId) {
	if (targetId == constleadOrg) {
		fillSelectedOrgDataWithPrefix('ctrpDraftJB.leadOrg', data, siteId);
	} else if (targetId == constSponsor) {
		fillSelectedOrgDataWithPrefix('ctrpDraftSectSponsRespJB.sponsor', data, siteId);
	} else if (targetId == constSum4Org) {
		fillSelectedOrgDataWithPrefix('ctrpDraftJB.summ4', data, siteId);
	}
}
function fillSelectedOrgDataWithPrefix(prefix, data, siteId) {
	$(prefix+'FkSite').value = siteId == null ? '' : siteId;
	$(prefix+'FkAdd').value = data.addId == null ? '' : data.addId;
	$(prefix+'Name').value = data.name == null ? '' : data.name;
	$(prefix+'Street').value = data.street == null ? '' : data.street;
	$(prefix+'City').value = data.city == null ? '' : data.city;
	$(prefix+'State').value = data.state == null ? '' : data.state;
	$(prefix+'Zip').value = data.zip == null ? '' : data.zip;
	$(prefix+'Country').value = data.country == null ? '' : data.country;
	$(prefix+'Email').value = data.email == null ? '' : data.email;
	$(prefix+'Phone').value = data.phone == null ? '' : data.phone;
	$(prefix+'Tty').value = data.tty == null ? '' : data.tty;
	$(prefix+'Fax').value = data.fax == null ? '' : data.fax;
	$(prefix+'Url').value = data.url == null ? '' : data.url;
}
function removeSelectedOrg(targetId) {
	if (targetId == constleadOrg) {
		removeSelectedOrgWithPrefix('ctrpDraftJB.leadOrg');
	} else if (targetId == constSponsor) {
		removeSelectedOrgWithPrefix('ctrpDraftSectSponsRespJB.sponsor');
	} else if (targetId == constSum4Org) {
		removeSelectedOrgWithPrefix('ctrpDraftJB.summ4');
	}
}
function removeSelectedOrgWithPrefix(prefix) {
	$(prefix+'FkSite').value = '';
	$(prefix+'FkAdd').value = '';
	$(prefix+'Name').value = '';
	$(prefix+'Street').value = '';
	$(prefix+'City').value = '';
	$(prefix+'State').value = '';
	$(prefix+'Zip').value = '';
	$(prefix+'Country').value = '';
	$(prefix+'Email').value = '';
	$(prefix+'Phone').value = '';
	$(prefix+'Tty').value = '';
	$(prefix+'Fax').value = '';
	$(prefix+'Url').value = '';
	if ($(prefix+'Type')) { $(prefix+'Type').value = '0'; }
}
function fetchUserDetails(targetId, userId) {
	if (!targetId || !userId) { return; } 
	jQuery.ajax({
		url:'fetchMiscJSON.jsp?reqType=userDetails&userId='+userId,
		success: function(data){
			fillSelectedUserData(targetId, data);
		}
	});
}
function fillSelectedUserData(targetId, data) {
	if (targetId == constPrinv) {
		fillSelectedUserDataWithPrefix('ctrpDraftJB.pi', data);
	} else if (targetId == constPersonalContact) {
		fillSelectedUserDataWithPrefix('ctrpDraftSectSponsRespJB.personalContact', data);
	}
}
function fillSelectedUserDataWithPrefix(prefix, data) {
	$(prefix+'FkUser').value = data.userId == null ? '' : data.userId;
	$(prefix+'FkAdd').value = data.addId == null ? '' : data.addId;
	$(prefix+'FirstName').value = data.firstName == null ? '' : data.firstName;
	$(prefix+'MiddleName').value = data.middleName == null ? '' : data.middleName;
	$(prefix+'LastName').value = data.lastName == null ? '' : data.lastName;
	$(prefix+'StreetAddress').value = data.streetAddress == null ? '' : data.streetAddress;
	$(prefix+'City').value = data.city == null ? '' : data.city;
	$(prefix+'State').value = data.state == null ? '' : data.state;
	$(prefix+'Zip').value = data.zip == null ? '' : data.zip;
	$(prefix+'Country').value = data.country == null ? '' : data.country;
	$(prefix+'Email').value = data.email == null ? '' : data.email;
	$(prefix+'Phone').value = data.phone == null ? '' : data.phone;
	$(prefix+'Tty').value = data.tty == null ? '' : data.tty;
	$(prefix+'Fax').value = data.fax == null ? '' : data.fax;
	$(prefix+'Url').value = data.url == null ? '' : data.url;
}
function removeSelectedUser(targetId) {
	if (targetId == constPrinv) {
		removeSelectedUserWithPrefix('ctrpDraftJB.pi');
	} else if (targetId == constPersonalContact) {
		removeSelectedUserWithPrefix('ctrpDraftSectSponsRespJB.personalContact');
	}
}
function removeSelectedUserWithPrefix(prefix) {
	$(prefix+'FkUser').value = '';
	$(prefix+'FkAdd').value = '';
	$(prefix+'FirstName').value = '';
	$(prefix+'MiddleName').value = '';
	$(prefix+'LastName').value = '';
	$(prefix+'StreetAddress').value = '';
	$(prefix+'City').value = '';
	$(prefix+'State').value = '';
	$(prefix+'Zip').value = '';
	$(prefix+'Country').value = '';
	$(prefix+'Email').value = '';
	$(prefix+'Phone').value = '';
	$(prefix+'Tty').value = '';
	$(prefix+'Fax').value = '';
	$(prefix+'Url').value = '';
}
function markThisDraftReady(){
	var errCode = checkValidESign(this);
	var errMsg = '';
	var signMsg=$('eSignMessage1').innerHTML;
	if (signMsg == '<%=LC.L_Valid_Esign%>')
		{errMsg = '<%=LC.L_Valid_Esign%>';}
	if (signMsg == 'Valid-Password')
		{errMsg = 'Valid-Password';}
	switch (errCode){
		case -1:
			<%if("userpxd".equals(Configuration.eSignConf)){%>
			errMsg = 'Please enter e-Password';
			showUpdateDraftFailedDialog(errMsg);
			return false;
			break;
		<%}else{%>
			errMsg = '<%=MC.M_PlsEnterEsign%>';
			showUpdateDraftFailedDialog(errMsg);
			return false;
			break;
		<%}%>
		case -2:
			<%if("userpxd".equals(Configuration.eSignConf)){%>
			if (errMsg == ''){ errMsg = 'You entered a wrong e-Password. Please correct and try again.';
			showUpdateDraftFailedDialog(errMsg);
			return false;
			break;}
			<%}else{%>
			if (errMsg == ''){ errMsg = '<%=MC.M_EtrWrongEsign_PlsTryAgain%>';
			showUpdateDraftFailedDialog(errMsg);
			return false;
			break;}
			<%}%>
		case 0:	//No error
			$("ctrpDraftNonIndustrialForm").onSubmit=null;
			$("ctrpDraftNonIndustrialForm").action ="markCtrpDraftReadyForNonIndustrial";
			$("ctrpDraftNonIndustrialForm").submit();
			$("ctrpDraftNonIndustrialForm").action = null;
			$("ctrpDraftNonIndustrialForm").onSubmit = " return validateForm();";	
			break;
	}
	return true;
}
function checkValidESign(btn){
	var signValue=$('eSign1').value;
	if (signValue == null || signValue == "") return -1;
	
	var signMsg=$('eSignMessage1').innerHTML;
	if (signMsg == null || signMsg != '<%=LC.L_Valid_Esign%>') return -2;
	
	return 0; 
}
function showUpdateDraftFailedDialog(errMsg){
	$('updateDraftFailedDialogMsg').innerHTML =errMsg;
	$j('#updateDraftFailedDialog').dialog({
		modal:true,
		height:300,
		width:300,
		closeText: '',
		close: function() {
			$j("#updateDraftFailedDialog").dialog("destroy");
		}
	});
}

function showUpdateDraftDialog(err){
	isValidatedForm = false;
	fnClearESign();
	//var errorAction = data.errorAction;
	if (!err) {
		showUpdateDraftFailedDialog('<%=MC.M_Err_CnctAdmin%>');
	} else {
		$j('#updateDraftDialog').dialog({
			modal:true,
			height:300,
			width:300,
			closeText: '',
			close: function() {
				fnClearESign();
				$j("#updateDraftDialog" ).dialog("destroy");
			}
		});
	}
}

function switchToDownload(formobj)
{
	formobj.action = "/velos/servlet/Download";
	formobj.submit();
	return true;
}
</script>

<div id="selectUserDialog" title="<%=LC.L_Select_User%>" class="comments" style="display:none; overflow:auto">
	<jsp:include page="usersearchmodal.jsp"></jsp:include>
</div>
<div id="selectLookupDialog" title="<%=LC.L_Select_Org%>" class="comments" style="display:none; overflow:auto">
	<jsp:include page="getlookupmodal.jsp">
		<jsp:param name="viewId" value="6014" />
		<jsp:param name="keyword" value="selorgId|site_name~paramorgId|pk_site|[VELHIDE]" />
		<jsp:param name="seperator" value="," />
		<jsp:param name="form" value="" />
		<jsp:param name="dfilter" value="" />
	</jsp:include>
</div>
<div id="validateDraftDialog" title="<%=LC.CTRP_DraftValidateDraft%>" class="comments" style="display:none; overflow:auto">
	<table align="center" width="100%">
		<tr align="center">
			<td>
			<%if (((draftId == null || draftId == "0") && isAccessibleFor(ctrpRights, 'N'))
			   || (draftId != null && isAccessibleFor(ctrpRights, 'E'))){%>
				<%=VelosResourceBundle.getMessageString("CTRP_DraftValidateDialogMsg",params)%>
			<%} else {%>
				<%=VelosResourceBundle.getMessageString("CTRP_DraftPassNoAccessDialogMsg",params)%>
			<%}%>
			</td>
		</tr>
	</table>
	<%if (((draftId == null || draftId == "0") && isAccessibleFor(ctrpRights, 'N'))
	   || (draftId != null && isAccessibleFor(ctrpRights, 'E'))){%>
	<table align="center" width="100%">
		<tr align="center" >
			<td width="45%" align="right">
				<span id="eSignMessage1"></span>&nbsp;
				<% if ("userpxd".equals(Configuration.eSignConf)) {%>
					e-Password<FONT class="Mandatory">* </FONT>&nbsp;
				<% }else{%>
					<%=LC.L_Esignature %><FONT class="Mandatory">* </FONT>&nbsp;
				<%} %>
			</td>
			<td width="220em" align="left">
				<% if ("userpxd".equals(Configuration.eSignConf)) {%>
		   			<input type="password" name="eSign1" id="eSign1" style="background:url(./images/ic_lock_outline_gray_24dp.png) no-repeat; padding-left: 25px;background-color:white;" class="input21" autocomplete="off" onkeyup="fnReplicateESign(); ajaxvalidate('pass:'+this.id,-1,'eSignMessage1','Valid-Password','Invalid-Password','sessUserId')" 
		   			>
		   		<%}else{ %>
					<input type="password" name="eSign1" id="eSign1" maxlength="8" autocomplete="off"
					onkeyup="fnReplicateESign();ajaxvalidate('misc:eSign',4,'eSignMessage1','<%=LC.L_Valid_Esign %>','<%=LC.L_Invalid_Esign %>','sessUserId')"
					/>
				<%} %>
	   		</td>
		</tr>
	</table>
	<%}else {%>
	<br><br>
	<%} %>
	<table align="center" width="100%">
		<tr align="center">
			<td width="30%"></td>
			<% String labelText = LC.L_No; %>
			<%if (((draftId == null || draftId == "0") && isAccessibleFor(ctrpRights, 'N'))
			   || (draftId != null && isAccessibleFor(ctrpRights, 'E'))){%>
			<td width="15%">
				<button onclick='if(markThisDraftReady()) $j("#validateDraftDialog").dialog("close");'><%=LC.L_Yes%></button>
			</td>
			<%} else {
				labelText = LC.L_Close;
			} %>
			<td width="10%"></td>
			<td width="15%">
				<button onclick='$j("#validateDraftDialog").dialog("close");'><%=labelText%></button>
			</td>
			<td width="30%"></td>
		</tr>
	</table>
</div>
<div id="validateDraftFailedDialog" title="<%=LC.CTRP_DraftValidateDraft%>" class="comments" style="display:none; overflow:auto">
	<table align="center">
		<tr><td height="20px">&nbsp;</td></tr>
		<tr align="center">
			<td>
				<%=MC.CTRP_DraftValidateFailedDialogMsg%>
			</td>
		</tr>
		<tr><td height="20px">&nbsp;</td></tr>
		<tr align="center">
			<td>
				<button onclick='$j("#validateDraftFailedDialog").dialog("close");'><%=LC.L_Close%></button>
			</td>
		</tr>
	</table>
</div>
<div id="updateDraftDialog" title="<%=LC.CTRP_DraftValidateDraft%>" class="comments" style="display:none; overflow:auto">
	<table align="center" width="100%">
		<tr><td height="20px">&nbsp;</td></tr>
		<tr align="center" >
			<td>
			<%=MC.CTRP_DraftSaveAndValidateDialogMsg%>
			</td>
		</tr>
		<tr><td height="20px">&nbsp;</td></tr>
	</table>
	<table align="center" width="100%">
		<tr align="center" >
			<td width="20%">&nbsp;</td>
			<td width="20%">
				<button onclick='validateThisDraft();$j("#updateDraftDialog").dialog("close");'><%=LC.L_Yes %></button>
			</td>
			<td width="20%">&nbsp;</td>
			<td width="20%">
				<button onclick='location.href="ctrpDraftBrowser"'><%=LC.L_No %></button>
			</td>
			<td width="20%">&nbsp;</td>
		</tr>
	</table>
</div>
<div id="updateDraftFailedDialog" title="<%=LC.L_Error_Saving%>" class="comments" style="display:none; overflow:auto">
	<table align="center" width="100%">
		<tr><td height="20px">&nbsp;</td></tr>
		<tr align="center" >
			<td>
			<span id="updateDraftFailedDialogMsg"></span>
			</td>
		</tr>
		<tr><td height="20px">&nbsp;</td></tr>
		<tr align="center">
			<td>
				<button onclick='$j("#updateDraftFailedDialog").dialog("close");'><%=LC.L_Close%></button>
			</td>
		</tr>
	</table>
</div>