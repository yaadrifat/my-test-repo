<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.velos.eres.service.util.LC" %>
<%@ page import="com.velos.eres.service.util.StringUtil" %>
<%@ page import="com.velos.eres.service.util.SessionMaint" %>

<%--Include JSPs --%>
<jsp:include page="jqueryUtils.jsp"></jsp:include>
<jsp:include page="velos_includes.jsp"></jsp:include>
<jsp:include page="advEventNewInclude.jsp" flush="true"/>
<%----%>

<%!
static final String aeScreen_AdminGrps = "[Config_AEScreen_AdminGrps]";
%>

<%
String dashBoard=request.getParameter("dashBoard");
String sub_type=request.getParameter("sub_type");
String adveventId=request.getParameter("adveventId");
String pkey=request.getParameter("pkey");
String patProtId=request.getParameter("patProtId");
String statid=request.getParameter("statid");
String stdyid=request.getParameter("studyId");
%>
<input type="hidden" name="dashBoard" id="dashBoard" value="<%=dashBoard %>"></input>
<input type="hidden" name="sub_type" id="sub_type" value="<%=sub_type %>"></input>
<input type="hidden" name="adveventId" id="adveventId" value="<%=adveventId %>"></input>
<input type="hidden" name="pkey" id="pkey" value="<%=pkey %>"></input>
<input type="hidden" name="patProtId" id="patProtId" value="<%=patProtId %>"></input>
<input type="hidden" name="statid" id="statid" value="<%=statid %>"></input>
<input type="hidden" name="studyId" id="studyId" value="<%=stdyid %>"></input>
<%
SessionMaint sessionmaint = new SessionMaint();
HttpSession tSession = request.getSession(true);
if (!sessionmaint.isValidSession(tSession))
	return;

String defUserGroup = (tSession.getAttribute("defUserGroup")).toString();

%>
<div id="submitFailedDialog" title="<%=LC.L_Error%>" class="comments" style="display:none; overflow:auto">
	<table align="center">
		<tr><td height="20px">&nbsp;</td></tr>
		<tr align="center">
			<td id="errorMessageTD"></td>
		</tr>
		<tr><td height="20px">&nbsp;</td></tr>
		<tr align="center">
			<td>
				<button onclick='$j("#submitFailedDialog").dialog("close");'><%=LC.L_Close%></button>
			</td>
		</tr>
	</table>
</div>


<script>
var aeScreenFunctions = {
	aeScrnIsAdmin: {},
	validateAEScreen: {},
	openMessageDialog: {},
	defUserGroup:<%=defUserGroup %>,
	aeScrnAdminGrps:"<%=((aeScreen_AdminGrps.equals(LC.Config_AEScreen_AdminGrps)) ? 
		"" : LC.Config_AEScreen_AdminGrps)%>"
};

{
var grp = ''+aeScreenFunctions.defUserGroup;
var aeScrnAdminGrps = (aeScreenFunctions.aeScrnAdminGrps).split(",");

aeScreenFunctions.aeScrnIsAdmin = false;

if (aeScrnAdminGrps.length == 1){
	if (grp == aeScrnAdminGrps){
		aeScreenFunctions.aeScrnIsAdmin = true;
	}
} else {
	for (var indx=0; indx < aeScrnAdminGrps.length; indx++){
		if (grp == aeScrnAdminGrps[indx].trim()){
			aeScreenFunctions.aeScrnIsAdmin = true;
			break;
		}
	}
}

}

//Genric function to open a dialog with supplied Id
aeScreenFunctions.openMessageDialog = function openMessageDialog(divId)
{
	var messageHgth = navigator.userAgent.indexOf("MSIE") != -1 ? 160 : 90;
	jQuery("#"+divId).dialog({
		height: messageHgth,maxWidth: 250,position: 'center',resizable: false,modal: true,autoOpen: false
    }).siblings('.ui-dialog-titlebar').remove();
	jQuery("#"+divId).dialog("open");
	jQuery('.ui-widget-overlay').css('background', 'white');
	
	
}

	
 
</script>

<script type="text/javascript">
var isValidatedForm = false;
var dshbrd=document.getElementById("dashBoard").value;
var sub_type=document.getElementById("sub_type").value;
var adveventId=document.getElementById("adveventId").value;
var pkey=document.getElementById("pkey").value;
var patProtId=document.getElementById("patProtId").value;
var statid=document.getElementById("statid").value;
var stdyid=document.getElementById("studyId").value;

aeScreenFunctions.validateAEScreen = function() {
	
	

	if (aeScreen && aeScreen.validate){

		if(!aeScreen.overrideValidation){
			//Call all the validate functions one-by-one
			if(!advEventNewFunctions.validate(adverseEventScreenForm)){
				return false;
			}
			
			if (!moreDetailsFunctions.validate(adverseEventScreenForm)){ 
				return false;
			}
			
			if (!aeScreen.validate()){
				return false;
			}
		}else{
			alert('overirding validation');
		}
	}else{
		//Call all the validate functions one-by-one
		if(!advEventNewFunctions.validate(adverseEventScreenForm)){
			return false;
		}
		
		if (!moreDetailsFunctions.validate(adverseEventScreenForm)){ 
			return false;
		}
	}
	
	
	if (isValidatedForm) { return true; }
	isValidatedForm = true;
	
	aeScreenFunctions.openMessageDialog('progressDialog');

	if(dshbrd=="jupiter")
	{
		$j('.ui-dialog').css('top','100px');
		$j('.ui-dialog').css('left','50px');
	}
		
	$j.post('updateAdverseEventScreen',
			$j('#adverseEventScreenForm').serialize(),
			function(data) {

		if(dshbrd=="jupiter")
			{
				setTimeout(function () {
				$j('.ui-dialog').css('display','none');
		    	}, 1000);
			window.location="adverseEventScreen.jsp?adveventId="+adveventId+"&studyId="+stdyid+"&pkey="+pkey+"&patProtId="+patProtId+"&statid="+statid+"&sub_type="+sub_type+"&studyVer=&mode=M&src=tdMenuBarItem5&dashBoard=jupiter";
		    } 
 
				var errorMap = data.errorMap;
				var hasErrors = false;
				for (var key in errorMap) {
					hasErrors = true;
					isValidatedForm = false;
					$j('#errorMessageTD').html(errorMap[key]);
					break;
				}
				if (hasErrors) {
					
					$j('#progressDialog').dialog( "destroy" );
					
					$j('#submitFailedDialog').dialog({
						modal:true,
						closeText: '',
						close: function() {
							$j("#submitFailedDialog" ).dialog("destroy");
						}
					});
				} else {
					$("adverseEventScreenForm").onSubmit=null;
					$("adverseEventScreenForm").action ="genericMessage.jsp?messageKey=M_Data_SvdSucc&id=8";
					$("adverseEventScreenForm").submit();
					$("adverseEventScreenForm").action = '#';
					$("adverseEventScreenForm").onSubmit = "return aeScreenFunctions.validateAEScreen();";
				}
			}
		);
		return false;
};
</script>