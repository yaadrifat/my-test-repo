<%@page import="com.velos.eres.service.util.VelosResourceBundle"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@page import="com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:insertAttribute name="title" /></title>
	<div id="nav" style="width:99%;">
	    <tiles:insertAttribute name="localization" />
	    <tiles:insertAttribute name="panel" />
	</div>
	<div id="commoninc">
		<tiles:insertAttribute	name="velos_includes" />
	</div>
<style type="text/css">
ul.errorMessage { list-style-type:none; }
ul.errorMessage li { color:red; height:1.5em; text-align:right; }
span.errorMessage { color:red; height:1.5em; text-align:right; }
</style>
<script>
$j(document).ready(function(){
	if ("Y" == "<%=StringUtil.htmlEncodeXss(request.getParameter("noScroll"))%>") {
		$j("div.BrowserBotN").css({overflow:"hidden"});
	}
	if("LIND"!="<%=CFG.EIRB_MODE%>" && "Y"=="<%=CFG. FLEX_MODE%>"){
		$j("#workflowpan").css({width:"13%"});
		
	}
	<%-- if("LIND"!="<%=CFG.EIRB_MODE%>" && "Y"=="<%=CFG. FLEX_MODE%>"){
		$j("#workflowTab").css({style="height: 90% !important; left: 4px; padding: 0; position: absolute; visibility: visible; width: 12%; overflow:auto;"});
		
	} --%>
});
</script>
<s:head /> 
</head>
<body class="yui-skin-sam yui-dt yui-dt-liner" style="overflow:hidden"">
	<div id="bd">
		<table width="100%">
			<tr>
				<%if("LIND".equals(CFG.EIRB_MODE) && "N".equals(CFG. Workflows_Enabled)){ %>
				<td width="1%" valign="top" id="workflowpan">
				<%} 
				else{%>
				<td width="13%" valign="top" id="workflowpan">
				<%} %>
					<table width="100%">
						<tr>
							<td style="height:67px !important;">
								<div id="progressIconsTab">
									<tiles:insertAttribute	name="progressIcons" />
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div id="externalLinkTab">
									<tiles:insertAttribute name="externalLink"/>
								</div>
							</td>
						</tr>
						<tr>
							<td>
							<SCRIPT LANGUAGE="JavaScript">
	var isIE = $j.browser.msie;
	var screenWidth = screen.width;
	var screenHeight = screen.height;
	if(screenWidth>1280 || screenHeight>1024)
	{
		if(navigator.userAgent.toLowerCase().indexOf('msie') >= 0)
		{
			document.write('<DIV  id="workflowTab" style="overflow:auto;height:400px;">')
		}
		else
		{
			document.write('<DIV  id="workflowTab" style="height: 90% !important; left: 4px; padding: 0; position: absolute; visibility: visible; width: 13%; overflow:auto;">')
		}
	}
	else
	{
		if(navigator.userAgent.toLowerCase().indexOf('msie') >= 0)
		{
				document.write('<DIV id="workflowTab" style="overflow:auto;height:470px;">')
		}
		else
		{
			document.write('<div id="workflowTab" style="height: 90% !important; left: 4px; padding: 0; position: absolute; visibility: visible; width: 13%; overflow:auto;">')
		}	
		
	}
</SCRIPT>
								<!--  <div id="workflowTab" style="overflow:auto;height:470px;">-->
									<tiles:insertAttribute	name="workflowBar" />
								</div>
							</td>
						</tr>
						<tr></tr>
					</table>
				</td>
				<td  style="padding-bottom: 20%;" valign="top">
					<div id="divTab">
						<tiles:insertAttribute	name="tabs" />
					</div>
					<div class="flxPageContainer">
						<div id="body">
							<tiles:insertAttribute	name="body" />
						</div>
						<div id="footer" >
							<tiles:insertAttribute	name="footer" />
						</div>
					</div>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>
