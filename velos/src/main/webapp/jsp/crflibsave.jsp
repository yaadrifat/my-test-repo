<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</head>

<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="crflibB" scope="request" class="com.velos.esch.web.crflib.CrflibJB"/>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.esch.service.util.EJBUtil"%>

<%

String src = request.getParameter("srcmenu");
String duration = request.getParameter("duration");
String protocolId = request.getParameter("protocolId");
String calledFrom = request.getParameter("calledFrom");
String eventId = request.getParameter("eventId");
String costmode = request.getParameter("costmode");
String mode = request.getParameter("mode");
String fromPage = request.getParameter("fromPage");
String calStatus = request.getParameter("calStatus");
String eventmode = request.getParameter("eventmode");
String displayDur=request.getParameter("displayDur");
String displayType=request.getParameter("displayType");
String eSign = request.getParameter("eSign");	
String crflibmode=request.getParameter("crflibmode");
String crflibId=request.getParameter("crflibId");
String crfType="";

String propagateInVisitFlag = request.getParameter("propagateInVisitFlag"); //SV, 10/12/04, cal-enh-05
String propagateInEventFlag = request.getParameter("propagateInEventFlag");

String eventName = request.getParameter("eventName");

if(calledFrom.equals("S")){
crfType="S";
}else {
	if(fromPage.equals("fetchProt"))
	{
	crfType="C";
	}else{
	crfType="L";
	}
	}








HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession)) {
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<% 		
	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
	%>
	<jsp:include page="incorrectesign.jsp" flush="true"/>	
	<%
	
	} else {
String ipAdd = (String) tSession.getValue("ipAdd");
String usr = (String) tSession.getValue("userId");

String crflibName="";
String crflibNumber="";
crflibName= request.getParameter("crflibName");
crflibNumber= request.getParameter("crflibNumber");

String calAssoc=request.getParameter("calassoc");
calAssoc=(calAssoc==null)?"":calAssoc;


	if(crflibmode.equals("N"))
	{


	crflibB.setEventsId(eventId);
	crflibB.setCrflibNumber(crflibNumber); 
      crflibB.setCrflibName(crflibName);
	crflibB.setCrflibFlag(crfType);
	crflibB.setCrflibDetails();


	}
	else {
	crflibB.setCrflibId(EJBUtil.stringToNum(crflibId));   	
	crflibB.setEventsId(eventId);
	crflibB.setCrflibNumber(crflibNumber); 
    crflibB.setCrflibName(crflibName);
	//Added by Amarnadh to fix the BUg2996
	crflibB.setCrflibFlag(crfType);
    //Added by Manimaran to fix the Bug2995
	crflibB.setModifiedBy(usr);
    crflibB.setIpAdd(ipAdd);

	crflibB.updateCrflib();
	} 
System.out.println("*****************************eventsid=" + crflibB.getEventsId());

	// Cal-enh - 05, now check if the user wishes to propagate these changes to other events.
		if ((propagateInVisitFlag == null) || (propagateInVisitFlag.equals("")))
			propagateInVisitFlag = "N";

		if ((propagateInEventFlag == null) || (propagateInEventFlag.equals("")))
			propagateInEventFlag = "N";
		
		if ( (propagateInVisitFlag.equals("Y")) || (propagateInEventFlag.equals("Y"))) {
			//SV, 10/22/04, edit mode, the mode flag = "M" for propagation, "P" for protocol calendar; 10/28 changed "P" to calledFrom
	   		crflibB.propagateCrflib(EJBUtil.stringToNum(protocolId), propagateInVisitFlag, propagateInEventFlag, "M", calledFrom);  
		}
	
%>
<br><br><br><br><br>

<p class = "sectionHeadings" align = center><%=MC.M_Data_SavedSucc%><%--Data saved successfully.*****--%></p>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=crflibbrowser.jsp?selectedTab=6&srcmenu=<%=src%>&duration=<%=duration%>&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayDur=<%=displayDur%>&displayType=<%=displayType%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>">
<%
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</BODY>
</HTML>


