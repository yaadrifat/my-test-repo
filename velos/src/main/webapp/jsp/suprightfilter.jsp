<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Super_UserRightsFilter%><%--Super User Rights Filter*****--%> </title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,java.text.*" %>
<%@ page import="com.velos.eres.business.common.*,com.velos.esch.business.common.*,com.velos.eres.service.util.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.patProt.*,com.velos.eres.web.user.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<jsp:include page="popupJS.js" flush="true"/>

<script>
	function openLookup(type) 
{
 
 sql="codelst_type='"+type+"'";
 windowName = window.open("multilookup.jsp?viewId=6011&form=filter&dfilter="+sql+"&keyword="+type+"|CODEDESC~"+type+"pk|CODEPK|[VELHIDE]","Suprightfilter","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700")
 windowName.focus();
}
 function  validate(formobj){
 	
      if (!(validate_col('e-Signature',formobj.eSign))) return false
   <%--   if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

	formobj.eSign.focus();
	return false;

	 } --%>

   }
 		
</SCRIPT>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<body>
<br>
<jsp:useBean id="groupB" scope="session" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>
<jsp:useBean id="codelstB" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>

<%@ page language="java" import="com.velos.eres.business.common.SettingsDao,com.velos.eres.business.common.CodeDao,com.velos.eres.service.util.EJBUtil"%>
<jsp:include page="include.jsp" flush="true"/>

  <%
	HttpSession tSession = request.getSession(true);     

	if (sessionmaint.isValidSession(tSession))
	{
	 int pageRight=0,index=-1;
	 String tempValue="",tempPk="";
	 GrpRightsJB gRights = (GrpRightsJB) tSession.getValue("GRights");		
       	 pageRight = Integer.parseInt(gRights.getFtrRightsByValue("MGRPRIGHTS"));
	 
	 String groupIdStr=request.getParameter("groupid");
	 groupIdStr=(groupIdStr==null)?"":groupIdStr;
	 
	 String type=request.getParameter("type");
	 type=(type==null)?"":type;
	 
	 int grpId=EJBUtil.stringToNum(groupIdStr);
	 
	 ArrayList settingsValue=new ArrayList();
	 ArrayList settingsPk=new ArrayList();
	 ArrayList settingsKeyword=new ArrayList();
	 ArrayList keywordList=new ArrayList();
	 
	 if (type.equals("BUD"))
	 {
	 keywordList.add("BUD_TAREA");
	 keywordList.add("BUD_DISSITE");
	 keywordList.add("BUD_DIVISION");
	 } 
	 else if (type.equals("STUDY"))
	 {
	 keywordList.add("STUDY_TAREA");
	 keywordList.add("STUDY_DISSITE");
	 keywordList.add("STUDY_DIVISION");
	 }
	 
	 ArrayList groupFilterList=new ArrayList();
	 	 
	 SettingsDao settingsDao=commonB.retrieveSettings(grpId,4,keywordList);
	 
	 settingsPk=settingsDao.getSettingPK();
	 settingsValue=settingsDao.getSettingValue();
	 settingsKeyword=settingsDao.getSettingKeyword();
	 
	 	 	 
	 CodeDao codeDao=codelstB.getCodeDaoInstance();
	 
	if (pageRight > 0)

	{
%>
  <DIV class="popDefault" id="div1"> 
  <Form name="filter" id="filterfrm" method="post" action="updatesuprightfilter.jsp" onSubmit="if (validate(document.filter)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
  <input type="hidden" name="groupid" value=<%=groupIdStr%>>
  <input type="hidden" name="type" value=<%=type%>>
  <table width="70%" border="0">
  <%index=settingsKeyword.indexOf(type+"_TAREA");
   if (index>=0)
   {
   tempValue=(String)settingsValue.get(index);
   tempValue=(tempValue==null)?"":tempValue;
   tempValue=StringUtil.replace(tempValue,",",";");
   tempPk=(String)settingsPk.get(index);
   tempPk=(tempPk==null)?"":tempPk;
   }
   else 
   {
   tempValue="";
   tempPk="";
   }
     %>
  <tr>
	<td width=20%><%=LC.L_Therapeutic_Area%><%--Therapeutic Area*****--%></td>
	<td><input type="text" name="tarea" value="<%=(tempValue.length()>0)?codeDao.getCodeValuesByIds(tempValue,";",";"):""%>" readonly>
		<A href="#" onClick="openLookup('tarea')"><%=LC.L_Select%><%--Select*****--%></A>
	</td>
	<input type="hidden" name="tareapk" value='<%=tempValue%>'>
	<input type="hidden" name="tareasettingpk" value='<%=tempPk%>'>
  </tr>
  <%index=settingsKeyword.indexOf(type+"_DIVISION");
   if (index>=0)
   {
   tempValue=(String)settingsValue.get(index);
   tempValue=(tempValue==null)?"":tempValue;
   tempValue=StringUtil.replace(tempValue,",",";");
   tempPk=(String)settingsPk.get(index);
   tempPk=(tempPk==null)?"":tempPk;
   }
   else 
   {
   tempValue="";
   tempPk="";
   }
     %>
  <tr>
	<td width=20%><%=LC.L_Division%><%--Division*****--%></td>
	<td><input type="text" name="study_division" value='<%=(tempValue.length()>0)?codeDao.getCodeValuesByIds(tempValue,";",";"):""%>' readonly>
  		<A href="#" onClick="openLookup('study_division')"><%=LC.L_Select%><%--Select*****--%></A></td>
		<input type="hidden" name="study_divisionpk" value='<%=tempValue%>'>
		<input type="hidden" name="divsettingpk" value='<%=tempPk%>'>
  </tr>
  <%index=settingsKeyword.indexOf(type+"_DISSITE");
   if (index>=0)
   {
   tempValue=(String)settingsValue.get(index);
   tempValue=(tempValue==null)?"":tempValue;
   tempValue=StringUtil.replace(tempValue,",",";");
   tempPk=(String)settingsPk.get(index);
   tempPk=(tempPk==null)?"":tempPk;
   }
   else 
   {
   tempValue="";
   tempPk="";
   }
     %>
  <tr>
	<td width=20%><%=LC.L_Disease_Site%><%--Disease Site*****--%></td>
	<td><input type="text" name="disease_site"  value='<%=(tempValue.length()>0)?codeDao.getCodeValuesByIds(tempValue,";",";"):""%>' readonly>
  		<A href="#" onClick="openLookup('disease_site')"><%=LC.L_Select%><%--Select*****--%></A>
  	</td>
	 <input type="hidden" name="disease_sitepk" value='<%=tempValue%>'>
	 <input type="hidden" name="diseasesettingpk" value='<%=tempPk%>'>
  </tr>
  <tr valign=baseline>
  <% if (pageRight > 4) { %>
	<td align=left width=70% colspan=2>
		 <jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="filterfrm"/>
				<jsp:param name="showDiscard" value="N"/>
		 </jsp:include>
	</td>
  <% } %>
  </tr>
  </table>
  
   </Form>

  <%
	} //end of if body for page right

else

{

%>

  <jsp:include page="accessdenied.jsp" flush="true"/>

  <%
} //end of else body for page right
}//end of if body for session

else
{
%>

 <jsp:include page="timeout_childwindow.jsp" flush="true"/>

  <%
}

%>

  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>
</body>



</html>



