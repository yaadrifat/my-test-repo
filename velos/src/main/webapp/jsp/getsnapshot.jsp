<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,java.util.*,java.io.*" %>
<%@ page import="com.velos.eres.business.common.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB" %>
<%@ page import="com.velos.eres.business.user.impl.*,com.velos.eres.web.user.*,com.velos.eres.service.userAgent.*,com.velos.eres.service.util.*" %>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id="sv" scope="request" class="com.velos.eres.web.studyView.StudyViewJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="appendixB" scope="page" class="com.velos.eres.web.appendix.AppendixJB" />
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="sectionB" scope="page" class="com.velos.eres.web.section.SectionJB" />
<jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>


<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*"%>

<%
   String stdId = null;
   String msg = null;
   StudyViewDao svd = new StudyViewDao();
   StudyViewFile sfile = new StudyViewFile();
   StringBuffer output = new StringBuffer();
   /////////////////
		   int studyId = 0;
		   	boolean noDataFlag = false;
		 CodeDao cdTArea = new CodeDao();   
		 CodeDao cdPhase = new CodeDao();
		 CodeDao cdResType = new CodeDao();
         CodeDao cdBlinding = new CodeDao();
   		 CodeDao cdRandom = new CodeDao();

		 CodeDao cdType = new CodeDao(); 
		
		 String studyNumber = "" ;
		 String studyTitle = "" ;
		 String studyObjective = "" ;
		 String studySummary = "" ;
		 String studyProduct = "" ;
		 String studyTArea = "" ;
		 String studySize = "0" ;
		 String studyDuration = "0" ;
		 String studyDurationUnit = "" ;
		 String studyEstBgnDate = "" ;
		 String studyPhase = "" ;
		 String studyResType = "" ;
		 String studyType = "" ;
		 String studyBlinding = "" ;
		 String studyRandom = "" ;
		 String studySponsorId = "" ;
		 String studyContactId = "" ;
		 String studyOtherInfo = "" ;
		 String studyPartCenters = "" ;
		 String studyKeywrds = "" ;
		 String studyAuthor = "" ;
		 String studyPubCol="";
		 char[] stdSecRights =null ;
		
		 int counter = 0;
		 int userId = 0;
		 int siteId = 0;
		 int secCount = 0;	
		 UserDao userDao = new UserDao();
		 SiteDao siteDao = new SiteDao();
		 String str = "";
String refreshFlag = "";
   ////////////////
   
HttpSession tSession = request.getSession(true);  
if (sessionmaint.isValidSession(tSession))
   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/> 

<%   
   int svId = 0;
   int pkStudyView = 0;
   String type= "A";   
   stdId  = request.getParameter("studyId");
  refreshFlag = request.getParameter("refreshFlag");
  
  String ipAdd = (String) tSession.getValue("ipAdd");
 String usr = null;
 usr = (String) tSession.getValue("userId");
   
	pkStudyView =  svd.getStudyViewIdByStudy(EJBUtil.stringToNum(stdId));
	  
	  if (pkStudyView <=0 )
	  {
	    	sv.setStdStudy(stdId);
			sv.setCreator(usr);
			sv.setIpAdd(ipAdd);
			pkStudyView = sv.setStudyViewDetails();
			
		} else {
			if(refreshFlag.equals("T"))
			{
				sv.setId(pkStudyView);
				sv.getStudyViewDetails();
				sv.setModifiedBy(usr);
				sv.setIpAdd(ipAdd);
				sv.updateStudyView();
			}else{
				pkStudyView =0;
			}
		}
	
	if (pkStudyView > 0)
	{
	
	/////////////////////////////////////////////////////////////////////////////////////
	
	   		
		 studyId = EJBUtil.stringToNum(stdId);
		 studyB.setId(studyId);
		 studyB.getStudyDetails();
		 studyNumber = studyB.getStudyNumber();
			
	studyTitle = StringUtil.trueValue(studyB.getStudyTitle());
	studyObjective = StringUtil.trueValue(studyB.getStudyObjective());
	studySummary = StringUtil.trueValue(studyB.getStudySummary());
	studyProduct = StringUtil.trueValue(studyB.getStudyProduct());
	studySize = StringUtil.trueValue(studyB.getStudySize());
	studyDuration = StringUtil.trueValue(studyB.getStudyDuration());
	studyDurationUnit = StringUtil.trueValue(studyB.getStudyDurationUnit());
	studyEstBgnDate = StringUtil.trueValue(studyB.getStudyEstBeginDate());
   //studyOtherInfo = ((studyB.getStudyInfo()).equals(null))?"-":studyB.getStudyInfo();
   studyOtherInfo = StringUtil.trueValue(studyB.getStudyInfo());
   studyPartCenters = StringUtil.trueValue(studyB.getStudyPartCenters());
   studyKeywrds = StringUtil.trueValue(studyB.getStudyKeywords());
   studySponsorId = StringUtil.trueValue(studyB.getStudySponsor());
   studyContactId = StringUtil.trueValue(studyB.getStudyContact());
   studyAuthor = StringUtil.trueValue(studyB.getStudyAuthor());

	String reportName = LC.L_Study_Details/*"Study Details"*****/;
	   
	   UserBean userRObj = null;       //Msgcntr Entity Bean Remote Object
	  // UserPK userPK;
//	   userPK = new UserPK(EJBUtil.stringToNum(studyAuthor));
	//   UserHome userHome = EJBUtil.getUserHome();
	  // userRObj = userHome.findByPrimaryKey(userPK);
	  UserJB userB=new UserJB();
	  userB.setUserId(EJBUtil.stringToNum(studyAuthor));
	userRObj= userB.getUserDetails();
        String authorName = StringUtil.trueValue(userRObj.getUserLastName()) + ", " + StringUtil.trueValue(userRObj.getUserFirstName());
	   studyPubCol = studyB.getStudyPubCol();
	
	//so that all study details is saved	
	studyPubCol = 	"11111"; 
	
	if (type.equals("P")){ //View Public Studies	"P"
		if (studyPubCol == null ){
			studyPubCol = "11111"; // default rights for all section is Public
			reportName = LC.L_Pcol_Summary/*"Protocol Summary"*****/;
		}
	}else{ //Complete Protocol "A"
		studyPubCol = "11111";
		reportName =  studyTitle +"<BR> "+LC.L_Study_Number/*Study Number*****/+": " +studyNumber ;
	}
		stdSecRights= studyPubCol.toCharArray();
		int totSec = studyPubCol.length();
		cdTArea.getCodeValuesById(EJBUtil.stringToNum(studyB.getStudyTArea()));
		if(cdTArea.getCDesc().size() > 0){
      		studyTArea = (String) cdTArea.getCDesc().get(0);
	}

	 cdPhase.getCodeValuesById(EJBUtil.stringToNum(studyB.getStudyPhase()));
	if(cdPhase.getCDesc().size() > 0){
      		studyPhase = (String) cdPhase.getCDesc().get(0);
	}
	cdResType.getCodeValuesById(EJBUtil.stringToNum(studyB.getStudyResType()));
	if(cdResType.getCDesc().size() > 0){
      		studyResType = (String) cdResType.getCDesc().get(0);
	}


	cdBlinding.getCodeValuesById(EJBUtil.stringToNum(studyB.getStudyBlinding()));
	if(cdBlinding.getCDesc().size() > 0){
      		studyBlinding = (String) cdBlinding.getCDesc().get(0);
	}
	cdRandom.getCodeValuesById(EJBUtil.stringToNum(studyB.getStudyRandom()));
	if(cdRandom.getCDesc().size() > 0){
      		studyRandom = (String) cdRandom.getCDesc().get(0);
	}

	cdType.getCodeValuesById(EJBUtil.stringToNum(studyB.getStudyType()));
	if(cdType.getCDesc().size() > 0){
      		studyType = (String) cdType.getCDesc().get(0);
	}
      //studyAuthor = userIdFromSession;
	output.append(" <P><P><P>"); 
	output.append("  <table width='100%' cellspacing='0' cellpadding='0' border=0 >");
	output.append("    <tr > ");
	output.append("      <td width = '100%'> ");
	output.append("        <P class = '"+LC.L_Welcome_Lower/*welcome*****/+"' align='center'> "+ reportName+" </P>");
	output.append("      </td>");
	output.append("    </tr>");
	output.append("  </table>");
	output.append("<hr size=5 class='bluehr'>");
	output.append(" </P></P></P><P><P><P>");
	
	output.append("<Form name='report' method='POST'>");
	output.append("<input type='hidden' name='studyId' size = 20  value = " +studyId +">");
	counter = 0;

if (stdSecRights[secCount] == '1') {
	output.append("<BR>");
	output.append("<table width='100%'>");
	output.append("<tr>");
	output.append("<td class= 'labelheading'>");
	output.append(LC.L_Definition/*"Definition"*****/);
	output.append("</td>");
	output.append("</tr>");
	output.append("</table>");
	output.append("<hr size=2 class='bluehr'>");
	output.append("<BR>");
	output.append("<table width='100%' cellspacing='0' cellpadding='0' >");
	output.append("	  <tr>"); 
      output.append("<td width='30%' class= 'heading'>");
      output.append("   "+LC.L_Number/*Number*****/+" ");
      output.append("</td>");
      output.append("<td width='70%'>");
	output.append(studyNumber);
      output.append("</td>");
      output.append("</tr>");
      output.append("<tr>"); 
      output.append("  <td width='30%' class= 'heading'><br>");
      output.append("      "+LC.L_Title/*Title*****/+" ");
      output.append("	 </td>");
	output.append("   <td width='70%'><br>");
	output.append(studyTitle);
      output.append("<br></td>");
      output.append("</tr>");	
	output.append("<tr>"); 
      output.append("<td width='30%' class= 'heading'><br>");
     	output.append(LC.L_Primary_Objective/*"Primary Objective"*****/);
      output.append("</td>");
      output.append("<td width='70%'><br>");
	output.append(studyObjective);
      output.append("<br></td>"); 
     	output.append("</tr>");
	output.append("<tr>"); 
	output.append("<td width='30%' class= 'heading'><br>");
	output.append(LC.L_Summary/*"Summary"*****/);
	output.append("</td>");
      output.append("<td width='70%'><br>");
	output.append(studySummary);
      output.append("<br></td>");
      output.append("</tr>");		
      output.append("</tr>");
	output.append("</table>");
} else if (type.equals("A")){
	/*output.append("<BR>");
		output.append("<table width='100%'>");
		output.append("<tr>"); 
        output.append("<td width='30%' class= 'heading'><br>");
     	output.append("Objective");
        output.append("</td>");
        output.append("<td width='70%'><br>");
	    output.append(studyObjective);
        output.append("<br></td>"); 
     	output.append("</tr>");
		output.append("</table>");*/
}
	secCount++; 
	if (stdSecRights[secCount] == '1') {  //Display Section 1 only if it is Public section: 								//1=Public 0=Non-Public
		output.append("<BR>");
		output.append("<table width='100%'>");
		output.append("<tr id='headSection' >");
		output.append("<td class= 'labelheading'>");
		output.append(LC.L_Details/*"Details"*****/);
		output.append("</td>");
		output.append("</tr>");
		output.append("</table>");
		output.append("<hr size=2 class='bluehr'>");
		output.append("<BR>");
		output.append("<table width='100%' cellspacing='0' cellpadding='0'> ");
		output.append("<tr>"); 
	      output.append("<td width='30%' class= 'heading'>");
      	output.append(LC.L_Product_Name/*"Product Name"*****/);
	    	output.append("</td>");
	    	output.append("<td width='70%'>");
		output.append(studyProduct); 
	    	output.append("</td>");
		output.append("</tr>");
		output.append("<tr>"); 


	    output.append("<td width='30%' class= 'heading'>");

     	 output.append(LC.L_Therapeutic_Area/*"Therapeutic Area"*****/); 
	    output.append("</td>");
	  output.append("<td width='70%'>");
		output.append(studyTArea);
	  output.append("</td>");
	  output.append("</tr>");
	  output.append("<tr>"); 
	    output.append("<td width='30%' class= 'heading'>");
	       output.append(LC.L_Sample_Size/*"Sample Size"*****/);
	    output.append("</td>");
	    output.append("<td width='70%'>");
		output.append(studySize);
	    output.append("</td>");
	  output.append("</tr>");
	  output.append("<tr>"); 
	    output.append("<td width='30%' class= 'heading'>");
	       output.append(LC.L_Study_Duration/*"Study Duration"*****/);
	    output.append("</td>");
	    output.append("<td width='70%'>");
		output.append(studyDuration);	output.append(studyDurationUnit);
    	    output.append("</td>");
  	 output.append("</tr>");
	 output.append("<tr>"); 
    	output.append("<td width='30%' class= 'heading'>");
       output.append(LC.L_Estimated_BeginDate/*"Estimated Begin Date"*****/);
    	output.append("</td width='70%'>");
    	output.append("<td colspan=2>");
		output.append(studyEstBgnDate);
	 output.append("</td>");	
	output.append("</table>");

} else {

}
secCount++; 

if (stdSecRights[secCount] == '1' ) { 


	output.append("<BR>");


	output.append("<table width='100%'>");


	output.append("<tr id='headSection' >");


	output.append("<td class= 'labelheading'>");
	output.append(LC.L_Design/*"Design"*****/);

	output.append("</td>");

	output.append("</tr>");

	output.append("</table>");
	output.append("<hr size=2 class='bluehr'><br>");
	output.append("<table width='100%' cellspacing='0' cellpadding='0'>");


	    output.append("<td width='30%' class= 'heading'>");


      	 output.append(LC.L_Phase/*"Phase "*****/);


	    output.append("</td>");


	    output.append("<td width='70%'>");


		output.append(studyPhase);


	    output.append("</td>");


	  output.append("</tr>");		


	  output.append("<tr>"); 


	    output.append("<td width='30%' class= 'heading'>");


      	 output.append(LC.L_Research_Type/*"Research Type"*****/);


	    output.append("</td>");


	    output.append("<td width='70%'>");


		output.append(studyResType);


	  output.append("</td>");


	  output.append("</tr>");			


	  output.append("<tr>"); 


	    output.append("<td width='30%' class= 'heading'>");


	       output.append(LC.L_Study_Type/*"Study Type"*****/);


	    output.append("</td>");


	   output.append("<td width='70%'>");


		output.append(studyType);


	    output.append("</td>");


	  output.append("</tr>");			


	  output.append("<tr>"); 


	    output.append("<td width='30%' class= 'heading'>");


	       output.append(LC.L_Blinding/*"Blinding"*****/);


	    output.append("</td>");


	    output.append("<td width='70%'>");


		output.append(studyBlinding);


	    output.append("</td>");


	  output.append("</tr>");			


	  output.append("<tr>"); 


	    output.append("<td width='30%' class= 'heading'>");


	       output.append(LC.L_Randomization/*"Randomization"*****/);


	    output.append("</td>");


	    output.append("<td width='70%'>");


		output.append(studyRandom);


	    output.append("</td>");


	  output.append("</tr>");	


	output.append("</table>");
} 

secCount++; 
if (stdSecRights[secCount] == '1'){
	output.append("<BR>");
    output.append("<table width='100%'>");
     output.append("<tr id='headSection' >");
     output.append("<td class= 'labelheading'>");
     output.append(LC.L_Sponsor_Information/*"Sponsor Information"*****/);
     output.append("</td>");
     output.append("</tr>");
     output.append("</table>");
     output.append("<hr size=2 class='bluehr'>");
     output.append("<br>");
     output.append("<table width='100%' cellspacing='0' cellpadding='0'>");
     output.append("<td width='30%' class= 'heading'>");
     output.append(LC.L_Sponsored_By/*"Sponsored By"*****/);
     output.append("</td>");
     output.append("<td width='70%'>");
     output.append(studySponsorId);
     output.append("</td>");
     output.append("</tr>");		
     output.append("<tr>"); 
     output.append("<td width='30%' class= 'heading'>");
     output.append(LC.L_Contact/*"Contact"*****/);
     output.append("</td>");
     output.append("<td width='70%'>");
     output.append(studyContactId);
     output.append("</td>");
     output.append("</tr>");			
     output.append("<tr>"); 
     output.append("<td width='30%' class= 'heading'>");
     output.append(LC.L_Other_Information/*"Other Information"*****/);
     output.append("</td>");
     output.append("<td width='70%'>");
     output.append(studyOtherInfo);
     output.append("</td>");
     output.append("</tr>");		
     output.append("</table>");
     output.append("</table>");
}
secCount++; 
if ( stdSecRights[secCount] == '1'){
     output.append("<table width='100%' cellspacing='0' cellpadding='0' >");
     output.append("<tr>");
     output.append("<td width='30%' class= 'heading'>");
     output.append(LC.L_Participating_Centers/*"Participating Centers"*****/);
     output.append("</td>");
     output.append("<td width='70%'>");
     output.append(studyPartCenters);
     output.append("</td>");
     output.append("</tr>");
     output.append("</table>");
 }
 
//study versions
output.append("<BR>");
output.append("<table width='100%' >");
output.append("<tr>");
output.append("<td class= 'labelheading'>");
output.append(LC.L_Versions/*"Versions"*****/);
output.append("</td>");
output.append("</tr>");
output.append("</table>");
output.append("<hr size=2 class='bluehr'>");

StudyVerDao studyVerDao = studyVerB.getAllVers(studyId);
ArrayList studyVerIds = studyVerDao.getStudyVerIds();
ArrayList studyVerNumbers = studyVerDao.getStudyVerNumbers();
int verlen = studyVerIds.size();
SectionDao sectionDao;
ArrayList secNames;	 
ArrayList secContents;
AppendixDao appendixDao ;
ArrayList appendixIds;
ArrayList appendixFileUris;
ArrayList appendixDescs;
ArrayList appendixFiles;
ArrayList appendixTypes;

//get all study versions
for(int i=0;i<verlen;i++) {

	String stdVerId = ((studyVerIds.get(i))==null)?"-":(studyVerIds.get(i)).toString();
	int studyVerId = EJBUtil.stringToNum(stdVerId);
	String studyVerNum =((studyVerNumbers.get(i))==null)?"-":(studyVerNumbers.get(i)).toString();
	output.append("<table width='100%' cellspacing='0' cellpadding='0'>");
	output.append("<tr height='10'>");
	output.append("</tr>");	
	output.append("<tr>");
	output.append("<td class= 'labelheading'>"+LC.L_Version/*Version*****/+": ");
	output.append(studyVerNum);
	output.append("</td>");
	output.append("</tr>");
	output.append("</table>");
	output.append("<hr size=2 class='bluehr'>");
		
	//get all sections in each study version
	if (type.equals("P")){
		sectionDao= sectionB.getStudySection(studyVerId, "Y","");
	}else{
		sectionDao= sectionB.getStudySection(studyVerId, "A","");
	}

	secNames = sectionDao.getSecNames();
	secContents = sectionDao.getContents();
	String secName = "";
	String secContent = "";
	int len = secNames.size();
	boolean flag = true;
	boolean	uflag = true;

	for(counter=0;counter<len;counter++) {
		secName = (String) secNames.get(counter);
		secContent = (String) secContents.get(counter);
		if((flag == true) && type.equals("P")) {
			flag = false;			
			output.append("<BR>");
			output.append("<table width='100%' >");
			output.append("<tr>");
			output.append("<td class= 'labelheading'>");
			output.append(LC.L_Sections/*"Sections"*****/);
			output.append("</td>");
			output.append("</tr>");
			output.append("</table>");
			output.append("<hr size=2 class='bluehr'>");
			output.append("<BR>");
			output.append("<table width='100%' cellspacing='0' cellpadding='0'>");
		} else if (type.equals("A")){
				flag = false;
				output.append("<BR><table width='100%' cellspacing='0' cellpadding='0' border=0 >");
		}

		if(flag == false){
			output.append("<tr id='headSection' >");
			output.append("<td class= 'labelheading'>");
			output.append(secName);
			output.append("</td>");
			output.append("</tr><tr><td width=300><br>");
			output.append("<pre style='word-break:break-all;word-wrap:break-word;'>" + secContent + "</pre>");
			output.append("<br><br></td>");
			output.append("</tr>");
		} //end of if for checking value of flag

	}//end of for loop section
		
	output.append("</table>");

	//get all appendix in each study version
	if (type.equals("P")) {
		appendixDao	= appendixB.getAppendixValuesPublic(studyVerId);
	}else{
		appendixDao = appendixB.getByStudyVerId(studyVerId);
	}

	appendixIds = appendixDao.getAppendixIds();
	appendixFileUris = appendixDao.getAppendixFile_Uris();	
	appendixDescs = appendixDao.getAppendixDescriptions();
	appendixFiles = appendixDao.getAppendixFiles();
	appendixTypes = appendixDao.getAppendixTypes();
	String appendixFileUri = "";
	String appendixDesc = "";
	String appendixFile = "";
	String appendixType = "";
	len = appendixIds.size();
	Integer appId;
	flag = true;
	uflag = true;
	for(counter=0;counter<len;counter++) {
		appendixFileUri = (String) appendixFileUris.get(counter); 
		appendixDesc = (String) appendixDescs.get(counter);
		appendixFile = (String) appendixFiles.get(counter);
		appendixType = (String) appendixTypes.get(counter);
		appId = (Integer)appendixIds.get(counter);
		
		appendixType = appendixType.trim();
		if(appendixType.trim().equals("file")){
			if(flag == true){
				flag = false;
				output.append("<BR>");
				output.append("<table width='100%'>");
				output.append("<tr id='headSection' >");
				output.append("<td class= 'labelheading'>");
				output.append(LC.L_Appendix_Files/*"Appendix Files"*****/);
				output.append("</td>");
				output.append("</tr>");
				output.append("</table>");
				output.append("<hr size=2 class='bluehr'>");
				output.append("<BR>");
				output.append("<table width='100%' cellspacing='0' cellpadding='0' >");
			} 

			if ( flag == false) {
				output.append("<tr>");
				output.append("<td width='40%' align='left'>");
				String dnld="";
				Configuration.readAppendixParam(Configuration.ERES_HOME + "eresearch.xml");
				dnld=Configuration.DOWNLOADSERVLET+"/"+ appendixFile ;
				output.append("<A Href='" +dnld +"?appndxId=" + appId + "' target = '_new'>"); 
				output.append(appendixFileUri +"</A>");
				output.append("</td>");
				output.append("<td align='left'>[ ");
				output.append(appendixDesc);
				output.append(" ]</td>");
				output.append("</tr>");
			}
		}

		if(appendixType.equals("url")){ 
			 if (uflag == true) {
				uflag = false;
				output.append("</table>");
				output.append("<BR>");
				output.append("<table width='100%'>");
				output.append("<tr id='headSection' >");
				output.append("<td class= 'labelheading'>");
				output.append(LC.L_Appendix_Urls/*"Appendix URLs"*****/);
				output.append("</td>");
				output.append("</tr>");
				output.append("</table>");
				output.append("<hr size=2 class='bluehr'>");
				output.append("<BR>");
				output.append("<table width='100%' cellspacing='0' cellpadding='0' >");
			} 


			if (uflag == false) {
				output.append("<tr>");
				output.append("<td align='left' width='40%' >");
				output.append("<A Href='" +appendixFileUri +"' target = '_new'>"); 
				output.append(appendixFileUri +"</A>");
				output.append("</td>");
				output.append("<td align='left'>[ ");
				output.append(appendixDesc);
				output.append(" ] </td>");
				output.append("</tr>");
			}	
		}//end of if else if ladder
	}//end of for loop appendix
} //for loop study version

	output.append("</table>");
	output.append("<hr size=5 class='bluehr'>");
	output.append("</Form>");


	String Str, styleTag, completeFileName ;
	String contents = null;
	int sType = 0;
	Str = null;
	styleTag = null;
	Str = output.toString();
	Calendar now = Calendar.getInstance();
	Configuration.readSettings();
		
	Configuration.readAppendixParam(Configuration.ERES_HOME + "eresearch.xml");

	String filePath = Configuration.UPLOADFOLDER;

	 styleTag = "<style> body, layer { color: 000000; background-color: FFFFFF; } body, td, center, p, div { font-family: arial; font-size: 10pt } " +  
	" b { font-family: arial } .sp { line-height: 2pt; } .heading { font-weight: bold; font-family: arial; font-size: 10pt;} " + 
	".small { font-size: 8pt; font-family: arial } .welcome { font-size: 12pt; font-family: arial; color: 000099;font-weight:bold }" + 
	".large { font-size: 12pt; } .margin { margin-top: 2px; margin-bottom: 2px; }" + 
	".exc { padding-bottom: 3; padding-left: 3; padding-right: 3; padding-top: 5; }" + 
	".bluehr {color:#330099} .evenrow{background-color:#CCCCCC} .oddrow{background-color:#FFFFFF}" + 
	" .labelheading { font-size: 11pt; font-family: arial; color: 000000 ;font-weight:bold;background-color:#CCCCCC }" + 
	"</style>";
	
    String	header = "<html><head><title>"+LC.L_Velos_Eres/*Velos eResearch*****/+"</title><meta http-equiv='Content-Type' content='text/html'>"+ styleTag+"</head><body>";
	String	filExt = ".html";
	
	String footer = "</body></html>";
		
	contents = header + Str + footer;
	
			byte[] fileBytes = contents.getBytes();
		
		try{
			
			completeFileName  = "StudyView" + stdId + "["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "]" + filExt ;


			sType = EJBUtil.getSystemType();

			FileOutputStream file ;
			
			if (sType == 0)
			{
				filePath  = filePath + "\\" + completeFileName;
			//file = new FileOutputStream(filePath + "\\" + completeFileName);
			}
			else
			{
			filePath = filePath + "//" + completeFileName;
			
			//file = new FileOutputStream(filePath + "//" + completeFileName);
			}
		
			file = new FileOutputStream(filePath);
			
			file.write(fileBytes,0,fileBytes.length);

			file.close();
		
			}catch (Exception e)
			{
				System.out.println("Exception in saving report " + e);
				//return null;
			}
			

	
	
	if(!noDataFlag) {
	%>
		<br>
		<form method="POST">
		</form>

	<%} //end of if for noDataFlag
		//out.print(Str);

		
	////////////////////////////////////////////////////////////////////////////////////
	
	
	
	
    	if (sfile.saveFile(filePath, pkStudyView) == false)
			out.println(MC.M_File_NotUploaded/*"file not uploaded"*****/);
	}
%>
<%
}//end of if body for session     
%>
