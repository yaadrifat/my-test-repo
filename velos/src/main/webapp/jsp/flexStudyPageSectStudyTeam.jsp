<%--
This JSP inherited most of the logic from patientschedule.jsp. This JSP gets called 
by patientschedule.jsp via AJAX for a particular visit and returns the events in that visit.
 --%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="studyJB" scope="session" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="altId" scope="request" class="com.velos.eres.web.studyId.StudyIdJB"/>
<jsp:useBean id="teamB" scope="session" class="com.velos.eres.web.team.TeamJB"/>
<jsp:useBean id="studySiteB" scope="session" class="com.velos.eres.web.studySite.StudySiteJB"/>
<%@page import="com.velos.eres.business.common.*,com.velos.eres.service.util.*"%>
<%@page import="com.velos.eres.web.studyRights.StudyRightsJB"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.eres.web.study.StudyJB"%>
<%@page import="com.velos.esch.business.common.*"%>
<%@page import="com.velos.eres.widget.service.util.FlxPageCache,com.velos.eres.widget.service.util.FlxPageFields"%>
<%@page import="java.util.*,java.io.*"%>
<%@page import="org.json.*,org.w3c.dom.*"%>
<style type="text/css">
div.sTeamAccordContainer { width:96% !important; }
.yui-dt-liner { white-space:normal; } 
.yui-skin-sam .yui-dt td.up { background-color: #efe; } 
.yui-skin-sam .yui-dt td.down { background-color: #fee; }
</style>
<script language="JavaScript" src="formjs.js"><!-- FORM JS--></script>
  <%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
   {

    int sectionNo = StringUtil.stringToNum(request.getParameter("sectionNo"));

    String defUserGroup = (String) tSession.getAttribute("defUserGroup");

	//put the values in session
	String studyId = request.getParameter("studyId");
	if(studyId == null || studyId.equals("null")) {
		studyId = (String) tSession.getAttribute("studyId");
		studyJB = null;
	} else 	{
		if (StringUtil.stringToNum(studyId) < 1){
			studyId="0";
			studyJB = null;
		} else {
			studyJB = new StudyJB();
			studyJB.setId(StringUtil.stringToNum(studyId));
			studyJB.getStudyDetails();
		}
		tSession.setAttribute("studyId",studyId);
	}
	
	String usr = (String) tSession.getAttribute("userId");
	int accountId = StringUtil.stringToNum((String)tSession.getAttribute("accountId"));
    int pageRight = 0;

    if (StringUtil.stringToNum(studyId) == 0){
    	grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
   		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
    } else {
		//******************GET STUDY TEAM RIGHTS and put the same in session, this will be a separate obj as no study is selected //***************************************************************
	    TeamDao teamDao = new TeamDao();
	    teamDao.getTeamRights(StringUtil.stringToNum(studyId),StringUtil.stringToNum(usr));
	    ArrayList tId = teamDao.getTeamIds();
	    if (tId.size() == 0)
		{
	    	pageRight=0 ;
	    }
		else
		{
	    	stdRights.setId(StringUtil.stringToNum(tId.get(0).toString()));
	   	 	ArrayList teamRights ;
			teamRights  = new ArrayList();
			teamRights = teamDao.getTeamRights();
	
			stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
			stdRights.loadStudyRights();
	
	    	tSession.setAttribute("studyRights",stdRights);
	    	if ((stdRights.getFtrRights().size()) == 0)
			{
	    	 	pageRight= 0;
	    	}
			else
			{
	    		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSUM"));
	    	}
	    }
    }%>
    
    <%if (StringUtil.stringToNum(studyId) == 0){%>
    	<p><%=MC.M_EtrStd_BeforeDets%></p>
    	<%
    	return;
  	} %>
	
	<%if (StringUtil.isAccessibleFor(pageRight, 'V')) {%>
	<%
		StringBuffer urlParamSB = new StringBuffer();
		urlParamSB = new StringBuffer();
		urlParamSB.append("studyId=").append(StringUtil.htmlEncodeXss(studyId));
	%>

	<script type="text/javascript">
		var addOrgClick = function(){
			var siteFld = document.getElementsByName("accsites")[0];
			if (!siteFld) return;

			var siteTyleFld = document.getElementsByName("siteType")[0];
			var siteTyleFldValue = (!siteTyleFld)? "" : siteTyleFld.value;

			if (!(validate_col('Organization', siteFld))) return false;
			try{
				$j.post('createStudySite?accSites='+siteFld.value+"&siteType="+siteTyleFldValue,
					$j('#studyScreenForm').serialize(),
					function(data) {
						var errorMap = data.errorMap;
						var hasErrors = false;
						for (var key in errorMap) {
							hasErrors = true;
							isValidatedForm = false;
							$j('#errorMessageTD').html(errorMap[key]);
							break;
						}
						if (hasErrors) {
							$j('#dialog-message').dialog("destroy");
							$j('#submitFailedDialog').dialog({
								modal:true,
								closeText: '',
								close: function() {
									$j("#submitFailedDialog" ).dialog("destroy");
								}
							});
						} else {
							sectionHeaderClick(<%=sectionNo%>, true);
						}
					}
				);
			}catch(e){		
			}
		};
		function reloadStudyTeamGrid(counter, siteId) {
			$("studyTeamDataGrid"+siteId).innerHTML = "";
			YAHOO.example.studyTeamDataGrid = function() {
				var args = {
					urlParams: "<%=urlParamSB.toString()%>"+"&selSiteId="+siteId+"&gridIndex="+counter,
					dataTable: "studyTeamDataGrid"+siteId,
					gridIndex: ""+counter,
					siteId: siteId,
					lindMode: true
				};
				//console.log(JSON.stringify(args));
				myStudyTeamDataGrid = new VELOS.studyTeamDataGrid('fetchStudyTeamJSON.jsp', args);
				myStudyTeamDataGrid.startRequest();
		    }();
		}
	</script>
	<%
	CodeDao cDao = new CodeDao();
	cDao.getAccountSites(accountId);
	
	CodeDao cdType = new CodeDao();
	cdType.getCodeValues("studySiteType");
	String dSiteType = cdType.toPullDown("siteType");
	
	TeamDao teamDao = new TeamDao();
	StudySiteDao studySiteDao = new StudySiteDao();
	StudyStatusDao studyStatDao = new StudyStatusDao();
	String selName = null;
	StringBuffer sb = new StringBuffer();
	Integer org ;
	String ddSite = "";
	String selOrg = request.getParameter("accsites");
	if(selOrg == null || selOrg.equals(""))
		selOrg = "0";
	int selOrgId = StringUtil.stringToNum(selOrg);
	int accId = StringUtil.stringToNum((String) tSession.getAttribute("accId"));
	int enrPatCount = 0;//JM
	int iStudyId = StringUtil.stringToNum(request.getParameter("studyId"));

	StudySiteDao ssDao = new StudySiteDao();
	ssDao	= studySiteB.getStudyTeamSites(iStudyId, accId);
	ArrayList arrSiteIds = ssDao.getSiteIds();
	ArrayList arrSiteNames = ssDao.getSiteNames();

	ArrayList arrAccSiteIds = cDao.getCId();
	ArrayList arrAccSiteNames = cDao.getCDesc();
	
	for (int indx= 0; indx < arrSiteIds.size(); indx++){
		int i = ((Integer)arrSiteIds.get(indx)).intValue();
		if (i <= 0) continue;

		int foundAt = arrAccSiteIds.indexOf(i);
		if (foundAt < 0) continue;

		arrAccSiteIds.remove(foundAt);
		arrAccSiteNames.remove(foundAt);
	}
	String dAccSites = EJBUtil.createPullDown("accsites", 0, arrAccSiteIds, arrAccSiteNames);	
	%>

	<div class="sTeamAccordContainer" style="display:none;">
		<table style="border:1px solid #7f7f7f;">
			<tr><td span=4>To add an organization to this study, select one and click + icon:</td></tr>
			<tr>
				<td width="50%"><%=LC.L_Organization%><%--Organization*****--%> <FONT class="Mandatory">* </FONT>&nbsp;<%=dAccSites%></td>
				<td width="5%">&nbsp;&nbsp;</td>
				<td width="30%"><%=LC.L_Type%><%--Type*****--%>&nbsp;<%=dSiteType%></td>
				<td><img id='addOrg' name='addOrg' title='<%=LC.L_Add%>' src='../images/add.png'/></td>
			</tr>
			<tr><td colspan="4" id="errorMessageTD"></td></tr>
		</table>
	</div>
	<%if (arrSiteIds.size() > 0){%>
		<input type="hidden" id="sTeamPurged" name="sTeamPurged" value=""/>
		<%
		for (int counter = 0; counter <= arrSiteNames.size() -1 ; counter++){
			int siteId = ((Integer) arrSiteIds.get(counter));
			String siteName = ((String) arrSiteNames.get(counter));
		%>
		<div class="sTeamAccordContainer" ><%=LC.L_EC_StudyTeamLabel %></div>
		<div id="sTeamAccordContainer<%=counter%>" class="sTeamAccordContainer">
			<input type="hidden" id="sTeamGridData<%=siteId%>" name="sTeamGridData<%=siteId%>" value=""/>
			<div id="studyTeamDataGrid<%=siteId%>" onmouseout="return nd();"></div>
		</div>
		<script>
			reloadStudyTeamGrid(<%=counter%>, <%=siteId%>);
		</script>
		<%} %>
		<br/>
	<%}%>
	<script>
		$j('#addOrg').click(function(){
			addOrgClick();
		});
	</script>
	<% } else {%>
	  <jsp:include page="accessdenied.jsp" flush="true"/>
	<% }
} //end of if session times out
else
{
%>
	<jsp:include page="timeout.html" flush="true"/>
<%
} //end of else body for page right
%>