<html>
<%--

	Project Name:		Velos eResearch
	Author:				Jnanamay Majumdar
	Created on Date:	05Jul2009
	Purpose:			Gui, Labs module in Specimens
	File Name:			specimenlabbrowser.jsp

--%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Specimen_Labs %><%-- Specimen Labs*****--%></title>

<script>

function checkPerm(pageRight,orgRight)
{

	if (f_check_perm_org(pageRight,orgRight,'E') == true)
	{
		return true ;
 	}
	else
	{
		return false;
	}
}

</script>




 	<jsp:include page="popupJS.js" flush="true"/>
	<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
	<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
	<SCRIPT LANGUAGE="JavaScript" SRC="formjs.js"></SCRIPT>
	
	
	<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="tdmenubaritem6"/>
	</jsp:include>

<body>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*,com.velos.eres.business.common.CtrlDao,java.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.business.common.*,com.velos.eres.web.user.UserJB"%>

<%

HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession)) {
int pageRight = 0;
int orgRight = 0;
int pageRightPat = 0;

int labPageRight = 0;
GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
labPageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSPEC"));


String src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
int patId=EJBUtil.stringToNum(request.getParameter("pkey"));
String mode=request.getParameter("mode");
String FromPatPage = request.getParameter("FromPatPage");
if(FromPatPage==null){
	FromPatPage="";
}
String patientCode1=request.getParameter("patientCode");
if(patientCode1==null){
	patientCode1="";
}


	String stdId = request.getParameter("studyId");
  	stdId = (stdId==null)?"":stdId;

String userId = (String) tSession.getValue("userId");
//GET STUDY TEAM RIGHTS
    TeamDao teamDao = new TeamDao();
    teamDao.getTeamRights(EJBUtil.stringToNum(stdId),EJBUtil.stringToNum(userId));
    ArrayList tId = teamDao.getTeamIds();
    if (tId.size() == 0)
	{
    	pageRight=0 ;
    }
	else
	{
    	stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));

    		ArrayList teamRights ;
					teamRights  = new ArrayList();
					teamRights = teamDao.getTeamRights();

					stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
					stdRights.loadStudyRights();


    	if ((stdRights.getFtrRights().size()) == 0)
		{
    	 	pageRight= 0;
		    pageRightPat = 0;
    	}
		else
		{
    		//pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMACC"));
			pageRightPat = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));

    	}
    }

  orgRight = userSiteB.getUserPatientFacilityRight(EJBUtil.stringToNum(userId), patId );


  personB.setPersonPKId(patId);
  personB.getPersonDetails();
  String patientCode = personB.getPersonPId();
  patientCode = (patientCode==null)?"":patientCode;



	String specimenPk = request.getParameter("specimenPk");
	specimenPk = (specimenPk==null)?"":specimenPk;

	String calldFrom = request.getParameter("calledFromPg");
	calldFrom = (calldFrom==null)?"":calldFrom;


	if (labPageRight >=4) {
%>
 
 <SCRIPT LANGUAGE="JavaScript">
var isIe = jQuery.browser.msie;
	if(isIe)
		document.write('<DIV class="browserDefault" id="div1" style="height:80%;overflow:auto">');
	else
		document.write('<DIV class="browserDefault" id="div1" style="height:80%;overflow:auto">');
</SCRIPT>
  <!--  <DIV class="browserDefault" id="div1" style="overflow:auto;">-->

<% if(FromPatPage.equals("patient")  || FromPatPage.equals("patientEnroll")){%>

<jsp:include page="patienttabs.jsp" flush="true">
<jsp:param name="selectedTab" value="12"/>
<jsp:param name="page" value="<%=FromPatPage%>"/>
<jsp:param name="page" value="<%=patientCode1%>"/>
<jsp:param name="pkey" value="<%=patId%>"/>
<jsp:param name="specimenPk" value="<%=specimenPk%>" />
<jsp:param name="modespecimen" value="<%=mode%>" />
</jsp:include>

	<%} else{%>
	<table width="99%" cellspacing="0" cellpadding="0" border="0" >

<!-- removed some part of code from here  -->
		<tr><td colspan = "4">
		<jsp:include page="inventorytabs.jsp" flush="true">
			<jsp:param name="selectedTab" value="1"/>
			<jsp:param name="specimenPk" value="<%=specimenPk%>" />
			</jsp:include>
		</td>
		</tr>
	</table>
<%} %>



	<jsp:include page="labbrowser.jsp" flush="true">
   	 <jsp:param name="selectedTab" value="<%=selectedTab%>"/>
	 <jsp:param name="mode" value="<%=mode%>"/>
	 <jsp:param name="page" value="specimen"/>
	 <jsp:param name="pkey" value="<%=patId%>"/>
	 <jsp:param name="patientCode" value="<%=StringUtil.encodeString(patientCode)%>"/>
	 <jsp:param name="fromlab" value=""/>

	  <jsp:param name="pageRightPat" value="<%=labPageRight%>"/>

	 <jsp:param name="selectedStudy" value="<%=stdId%>"/>
	 <jsp:param name="studyId" value="<%=stdId%>"/>
	 <jsp:param name="calledFromPg" value="<%=calldFrom%>"/>
	 <jsp:param name="specimenPk" value="<%=specimenPk%>" />
	 <jsp:param name="page" value="<%=FromPatPage%>"/>
	 <jsp:param name="page" value="<%=patientCode1%>"/>
	</jsp:include>




<br>
<%
} //end of if body for page right
	else{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%
	} //end of else body for page right
}//end of if body for session
else {
%>
	<jsp:include page="timeout.html" flush="true"/>
<%}
%>

<div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</DIV>
<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>
</html>

