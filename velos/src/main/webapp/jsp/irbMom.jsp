<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html><head></head>
<body onunload="alertUnsaved(document.mom)">
<script>
	function editMom()
{
	var formatTextAreaStyle = "width:500;height:550;";
 
	
	url="popupeditor.jsp?displayDIV=displayMom&formname=mom&formatfieldname=editBMomF&toolbar=Velos&length=32000000&formatTextAreaStyle="+formatTextAreaStyle;
	url=url+"&formatTextAreaWidth=500&formatTextAreaHeight=550";
	
	     popupeditor=window.open(url,"popupeditor","toolbar=no,scrollbars=no,resizable=no,menubar=no,width=630,height=630,left=10,top=20");
	     popupeditor.focus();

	     document.mom.dirtyFlag.value = 'Y';

}

function saveMom(formobj)
{
	document.mom.dirtyFlag.value = 'N';
	document.mom.revokBR.value = 'Y';
	formobj.saveMode.value="S";
	formobj.submit();
}

function deleteOldMom(formobj)
{
	document.mom.revokBR.value = 'N';
	formobj.editBMomF.value="";
	document.mom.dirtyFlag.value = 'N';
	formobj.saveMode.value="S";
	formobj.submit();
}

function alertUnsaved(formobj) {
	if (formobj.dirtyFlag.value != 'Y') {
		return;
	}
	document.mom.dirtyFlag.value = 'N';
	var yesSave = confirm("<%=MC.M_YouUnsavedData_SaveNow%>");/*var yesSave = confirm('You may have unsaved data. Save now? (Click OK to save or Cancel to discard.)');*****/
	if (yesSave) {
		return saveMom(formobj);
	}
	return;
}
function myFunction(revBoard,meetingDatePK){
	
	var f=document.mom;
	document.mom.revokBR.value = "Y";
   	f.method="post";
    f.action='irbMom.jsp?revBoard='+revBoard+'&meetingDatePK='+meetingDatePK+'&disflag=Y';
    f.submit();
}
</script>
	
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*"%>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<jsp:useBean id="sessionmaint" scope="session"	class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="subBoardJB" scope="page" class="com.velos.eres.web.submission.SubmissionBoardJB" />
<jsp:useBean id ="reportIO" scope="session" class="com.velos.eres.service.util.ReportIO"/>	
<% 
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	
 { 	
	
String revokBR=request.getParameter("revokBR")==null?"":request.getParameter("revokBR");
if(revokBR==null){revokBR="";}
String revBoard =request.getParameter("revBoard");
String meetingDatePK =request.getParameter("meetingDatePK");
String saveMode =request.getParameter("saveMode");
String disflag=request.getParameter("disflag")==null?"":request.getParameter("disflag");

if (StringUtil.isEmpty(saveMode))
{
	saveMode = "V";
}
if (saveMode.equals("S") || disflag.equals("Y"))
{
	String momF=StringUtil.decodeString(request.getParameter("editBMomF"));
	//momF=(momF.replaceAll("\\n", "<br>")).replaceAll("&lt;br&gt;", "<br>");
	subBoardJB.updateReviewMeetingMOM(EJBUtil.stringToNum(meetingDatePK),momF);
}

			String userId = (String) tSession.getValue("userId");
			String mom = "";
			
			EIRBDao eirbdao=new EIRBDao();
			int meetingDatePK_disable = Integer.parseInt(meetingDatePK);
			if(disflag.equalsIgnoreCase("Y")){
			
			eirbdao.disableLinkUpdate(revBoard,meetingDatePK_disable);
			
			}
			String freezeflag=eirbdao.gedisableflagforfreeze(revBoard,meetingDatePK_disable);
			if(freezeflag==null){
				freezeflag="";
			}else{
				revokBR="Y";
			}
			mom = subBoardJB.getreviewMeetingMOM(EJBUtil.stringToNum(revBoard),EJBUtil.stringToNum(meetingDatePK));
			StringBuffer sbuffer = new StringBuffer(mom);
			if(!revokBR.equals("Y")){
			mom = sbuffer.toString().replaceAll("<BR>", "<BR></BR>").replaceAll("<br>", "<br></br>");
			}
			String printLink=reportIO.saveReportToDoc(mom,"htm","reporthtml");
			String wordLink =reportIO.saveReportToDoc(mom,"doc","reporthtml"); 
	        com.aithent.file.uploadDownload.Configuration.readSettings("eres");
	        com.aithent.file.uploadDownload.Configuration
	                .readUploadDownloadParam(
	                        com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD
	                                + "fileUploadDownload.xml", null);
	      	String  filePath = com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER;
			String pdflink="";
			String fileName=printLink.substring(printLink.lastIndexOf("=")+ 1);
			String htmlFile =StringUtil.encodeString( filePath +"\\"+fileName);
			pdflink =ReportIO.pdfGenerate(filePath,htmlFile,fileName,mom);
	
	%>
<form name="mom" action="irbMom.jsp" method="post">
<%if (!freezeflag.equalsIgnoreCase("Y")){ %>
	<A href="#" onclick="editMom()"><%=LC.L_Edit%><%--Edit*****--%></A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<A href="#" onclick="return saveMom(document.mom);"><%=LC.L_Save_MeetingMinutes%><%--Save Meeting Minutes*****--%></A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<A href="#" onclick="return deleteOldMom(document.mom);"><%=LC.L_Regenerate_MeetingMinutes%><%--Regenerate Meeting Minutes*****--%></A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<% }else{%>
	<%=LC.L_Edit%><%--Edit*****--%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<%=LC.L_Save_MeetingMinutes%><%--Save Meeting Minutes*****--%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<%=LC.L_Regenerate_MeetingMinutes%><%--Regenerate Meeting Minutes*****--%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<% }%>
	<A href="<%=wordLink%>"><%=LC.L_Word_Format%><%--Word Format*****--%> </A>&nbsp;&nbsp;&nbsp;&nbsp;<A href="<%=pdflink%>">PDF Format<%--Pdf Format*****--%></A>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href="<%=printLink%>"><%=LC.L_Printer_FriendlyFormat%><%--Printer Friendly Format*****--%></A>
    
		<button style="background-color:#5c5c5c;color:#FAFAFA;font-family:Arial, Helvetica, sans-serif;font-size;9pt;cursor:pointer"  type="button" onclick="myFunction('<%=revBoard %>','<%=meetingDatePK %>')">Freeze</button>	
		
		<DIV ID="displayMom"><%= mom %>
		 </DIV>
		
		<input type= "hidden" name="editBMomF" id="editBMomF" value="<%=StringUtil.encodeString(mom)%>">
		<input type= "hidden" name="saveMode" value="">
		
		<input type= "hidden" name="meetingDatePK" value="<%= meetingDatePK %>">
		<input type= "hidden" name="revBoard" value="<%=revBoard%>">		
        <input type= "hidden" name="dirtyFlag" value="N">
        <input type= "hidden" name="revokBR" value="Y">
</form>
		<%
 
			
}//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %>
 		
</body>	