<%@page import="com.velos.eres.web.network.NetworkJB"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/><!--km--> 
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>
<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil, com.aithent.audittrail.reports.AuditUtils"%><%@page import="com.velos.eres.service.util.*,org.json.*,org.apache.commons.lang.WordUtils"%>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="userBean" scope="session" class="com.velos.eres.business.user.impl.UserBean"/>		

	<%
     boolean expandNtwFlag=false;
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html");
	HttpSession tSession = request.getSession(false);
	//JSONObject jsObj = new JSONObject();
	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		//jsObj.put("result", -1);
		//jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
   	%>
		<input type="hidden" name="resultMsg" id="resultMsg" value="<%=MC.M_UsrNot_LoggedIn %>"/>    	
	<%
		//return;
	}
       // String studyNetworkIds="";
		int accountId=0;
		String userAgent = request.getHeader("user-agent");
		String calledFrom;
		calledFrom= request.getParameter("calledFrom")==null?"":request.getParameter("calledFrom");
		int pageRight = EJBUtil.stringToNum(request.getParameter("pageRight"));
		String src = request.getParameter("src");

		String acc = (String) tSession.getValue("accountId");
		accountId = EJBUtil.stringToNum(acc);
		String userId = (String) tSession.getValue("userId");
		String ipAdd = (String) tSession.getValue("ipAdd");
		String moreParam=request.getParameter("moreParam")==null?"":request.getParameter("moreParam");
		String flag = request.getParameter("flag");
		String searchByName=(request.getParameter("searchByName")==null)?"":request.getParameter("searchByName");
		String parentNtwId=(request.getParameter("parentNtwId")==null)?"":request.getParameter("parentNtwId");
		String studyId=(request.getParameter("studyId")==null)?"":request.getParameter("studyId");
		int usrId = EJBUtil.stringToNum(userId);
		userB.setUserId(usrId);
		userB.getUserDetails();
		String defGroup = userB.getUserGrpDefault();
		int grpId=EJBUtil.stringToNum(defGroup);
		groupB.setGroupId(EJBUtil.stringToNum(defGroup));
		groupB.getGroupDetails();
		String groupName = groupB.getGroupName();
       
		String from=(request.getParameter("from")==null)?"":request.getParameter("from");
		Integer studyNetId=0;
		studyNetId = EJBUtil.stringToNum(request.getParameter("studyNetworkId"));
		NetworkDao nwdao = new NetworkDao();
		userB.setUserId(StringUtil.stringToNum(userId));
		userBean= userB.getUserDetails();
		String ridStNwt= AuditUtils.getRidValue("er_studynetwork","eres","PK_STUDYNETWORK="+studyNetId); //Fetches the RID from er_studynetwork
		String ridStatusH= AuditUtils.getRidValue("er_status_history","eres","status_modpk="+studyNetId); //Fetches the RID from er_status_history
		int delFlagStudy=0;
		if("D".equals(flag)){
			delFlagStudy=nwdao.deleteStudyNetwork(EJBUtil.stringToNum(request.getParameter("studyId")),studyNetId,userId, ipAdd);
		}
		if(delFlagStudy>0){
		 AuditUtils.updateAuditROw("eres",userBean.getUserId()+", "+userBean.getUserFirstName()+", "+userBean.getUserLastName(),ridStNwt,"D");
		 AuditUtils.updateAuditROw("eres",userBean.getUserId()+", "+userBean.getUserFirstName()+", "+userBean.getUserLastName(),ridStatusH,"D");
		}
		
		nwdao = new NetworkDao();
		nwdao.getStudyNetworkValues(EJBUtil.stringToNum(studyId),accountId,moreParam);
				
		ArrayList studyNetworkIdList = nwdao.getStudyNetworkIdList();
		ArrayList networkIdList = nwdao.getNetworkIdList();
		ArrayList siteNameList = nwdao.getSiteNameList();
		ArrayList siteIdList = nwdao.getSiteIdList();
		ArrayList studyIdList = nwdao.getStudyIdList();
		ArrayList mainNetworkIdList = nwdao.getNetworkMainIdList();
		ArrayList studyNetworkStatus = nwdao.getStdNtwrkStatsList();
		ArrayList HistoryIdsList = nwdao.getHistoryIdsList();
		ArrayList networkLevelList = nwdao.getNetworkLevelList();
		ArrayList networkTypeDescList = nwdao.getNetworkTypeDescList();
		ArrayList networkStatusSubTypList=nwdao.getStdNetworkStatusSubTypList();
		
		    String studyNetworkId = "";
		    String siteName = "";
			String networkId = "";
		    String siteId = "";
		    String stdyId="";
		    String mainStudyNetwork="";
		    String StudyNetworkStatusDesc = "";
		    String historyId = "";
		    String networkLevel="";
		    String networkTypeDesc="";
		    String addedNetIds="";
		    String ntStatusSubtype="";
		    
			int counter = 0;
			int counterCh = 0;
		    int len = networkIdList.size();
			 String filter = "";
		    
		    JSONObject jsonObj = new JSONObject(moreParam);
		    
		    if(jsonObj.has("filter")){
		    	filter = jsonObj.getString("filter");
		    }
			
		    if(!siteNameList.isEmpty() || !(filter.equals(""))){
			%>
			<tr>
				<td><input size="36" placeholder="Filter" name="searchNetWorkByName" id="searchNetWorkByName" class="search-table midAlign netfilter" onkeyup="networkSiteByName(event);" autocomplete="off" type="input" style="border-radius:5px;border: 2px solid #aaa;padding:2px 2px 2px 0px;"></td>
				<td><input size="22" placeholder="Filter" name="searchStdNetWorkByAffiliated" id="searchStdNetWorkByAffiliated" class="search-table midAlign netfilter" onkeyup="stdNetworkSiteByAffiliated(event);" autocomplete="off" type="input" style="border-radius:5px;border: 2px solid #aaa;padding:2px 2px 2px 0px;"></td>
				<td><input size="22" placeholder="Filter" name="searchStdNetWorkByType" id="searchStdNetWorkByType" class="search-table midAlign netfilter" onkeyup="stdNetworkSiteByType(event);" autocomplete="off" type="input" style="border-radius:5px;border: 2px solid #aaa;padding:2px 2px 2px 0px;"></td>
				<td><input size="28" name="searchByStudyNetworkStatus" placeholder="Filter" id="searchByStudyNetworkStatus" class="netfilter" onkeyup="studyNetworkBystatus(event);" autocomplete="off" type="input" style="border-radius:5px;border: 2px solid #aaa;padding:2px 2px 2px 0px;"></td>
				<td></td>
				<td></td>
			</tr>
			<%
		    }
    		if(len!=0)
    		{
			    for(counter = 0;counter<len;counter++)
				{ 
			    	studyNetworkId = studyNetworkIdList.get(counter).toString();
			    	networkId = networkIdList.get(counter).toString();
			    	siteName = siteNameList.get(counter).toString();
			    	siteId = siteIdList.get(counter).toString();
				    mainStudyNetwork=mainNetworkIdList.get(counter)==null?"-":mainNetworkIdList.get(counter).toString();
				    StudyNetworkStatusDesc = studyNetworkStatus.get(counter)==null?"":studyNetworkStatus.get(counter).toString();
				    historyId = HistoryIdsList.get(counter)==null?"":HistoryIdsList.get(counter).toString();
				    networkLevel = networkLevelList.get(counter)==null?"":networkLevelList.get(counter).toString();
				    networkTypeDesc = networkTypeDescList.get(counter)==null?"-":networkTypeDescList.get(counter).toString();
				    ntStatusSubtype= ((networkStatusSubTypList.get(counter))==null)?"":networkStatusSubTypList.get(counter).toString();
				    addedNetIds=addedNetIds+networkId+",";
					%>
					
					<tr id="row_" style="display: table-row;">
					<td><A href="#" id="orgName_<%=networkId %>" onclick="fOpenNetwork(<%=siteId%>,<%=networkId%>,'<%=networkLevel%>','<%=studyNetworkId%>');">
	        			<%if(siteName.length()>25){ %>
	          			<%=siteName.substring(0,25)%>&nbsp;<span onMouseOver="return overlib('<%=siteName%>',ABOVE,CAPTION,'<%=LC.L_Organization_Name%>');" onMouseOut="return nd();">...</span>
	          			<%}else{ %>
	          			<%=siteName%>  
	          			<%} %>
	          			</A>
	   				</td>
	          			
	          		<td><%=WordUtils.wrap(mainStudyNetwork, 25, "<br/>\n", true) %></td>
	          				
	        		<td><%=networkTypeDesc%></td>   				       				
	        					
				    <td>
				    	<A href="#" onclick="openWinStatus(<%=pageRight%>,'M','<%=studyId%>','<%=StringUtil.encodeString(studyNetworkId)%>','<%=historyId%>')"><%= StudyNetworkStatusDesc%></A>&nbsp;&nbsp;
						<A href="#" onclick="openWinStatus(<%=pageRight%>,'N','<%=studyId%>', '<%=StringUtil.encodeString(studyNetworkId)%>','0')"><img src="./images/edit.gif" title="<%=LC.L_Edit%>" border="0"/></A>&nbsp;&nbsp;
						<A href="showStudyNetworkHistory.jsp?modulePk=<%=studyNetworkId%>&pageRight=<%=pageRight%>&srcmenu=<%=src%>&selectedTab=13&from=ntwhistory&fromjsp=showStudyNetworkHistory.jsp&currentStatId=<%=historyId%>&studyId=<%=studyId%>"><img border="0" title="<%=LC.L_History%>" alt="<%=LC.L_History%>" src="./images/History.gif" ><%//=LC.L_H%><%--H*****--%></A>
					</td>
	        			        		
	        		<td>
	        			<img title="<%=LC.L_Forms%>" src="./images/Form.gif" style="cursor: pointer;" onclick="openformwin(<%=studyNetworkId%>,'<%=siteName%>')">
	        		</td>
	        		
	        		<td>
	        		<% if(ntStatusSubtype.equalsIgnoreCase("PEND")){%>
	        			<a href="#" onclick="deleteNetwork(<%=studyNetworkId%>,<%=pageRight%>,'M');"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"></a>
	        		<%} %>
	        		</td>
	        				
	      		</tr>
	      		
	      		<input type="hidden" id="siteName<%=siteId %>" value="<%=siteName%>"></input>
	        	<input type="hidden" id="siteType" value=""></input>
	        	<input type="hidden" id="networkType<%=networkId %>" value="<%=networkTypeDesc%>"></input>
				<input type="hidden" name="pageRight" value="<%=pageRight%>">
	      		
		   	<%
		   						
				}
			    addedNetIds=addedNetIds.substring(0, addedNetIds.length()-1);
				%>
		<%	}
			%><input type="hidden" name="addedNetIds" id="addedNetIds" value="<%=addedNetIds%>">
<style>
.netfilter{border-radius:5px;border: 2px solid #aaa;padding:2px 2px 2px 0px;" }
</style>