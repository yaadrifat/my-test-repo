<%--
  Created by IntelliJ IDEA.
  User: PSingh
  Date: 8/29/2016
  Time: 9:34 AM
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@page import="com.velos.eres.service.util.*,java.util.*,com.velos.esch.business.common.SchCodeDao"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Study coordinators page</title>
    <jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
    <jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
	<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
    <!-- <script src="js/angularJS/xdomain.min.js" slave="https://172.16.1.195:55555/proxy"></script>  -->
    <link rel="stylesheet" href="js/angularJS/node_modules/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="js/angularJS/node_modules/angular-datepicker-master/src/css/angular-datepicker.css"  type="text/css" />
    <script src="js/angularJS/node_modules/jquery/dist/jquery.js"></script>

    <script src="js/angularJS/node_modules/angular/angular.js"></script>
    <script src="js/angularJS/bower_components/angular-bootstrap/ui-bootstrap-tpls.js"></script>
    <script src="js/angularJS/node_modules/bootstrap/dist/js/bootstrap.js"></script>
    <script src="js/angularJS/node_modules/angular-route/angular-route.js"></script>
    <script src="js/angularJS/node_modules/angular-datepicker-master/src/js/angular-datepicker.js"></script>
    <link rel="stylesheet" href="js/angularJS/node_modules/angular-ui-grid/ui-grid.css"/>
    <script src="js/angularJS/node_modules/angular-ui-grid/ui-grid.js"></script>
    <%--Adding ui-Select files--%>
    <script src="js/angularJS/node_modules/angular-sanitize/angular-sanitize.min.js"></script>
    <link rel="stylesheet" href="js/angularJS/node_modules/ui-select/dist/select.min.css"/>
    <script src="js/angularJS/node_modules/ui-select/dist/select.min.js"></script>

    <%--Adding Charting scripts to draw charts--%>
    <script src="js/angularJS/bower_components/angular-google-chart/ng-google-chart.js"></script>

    <script src="js/jquery/jquery-1.4.4.js"></script>
    <SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<script type="text/javascript">
refreshPageAlert =1;
function showWarningChange(){
	userTimeoutInMSec = userTimeoutInMSec;
	warningPeriodInMSec = 60 * 1000;
	sessTimeoutWarning = '<%=MC.M_Usr_SessTimeout%>';
	if (userTimeoutInMSec - warningPeriodInMSec < 10000) {
		warningPeriodInMSec = 30000;
		sessTimeoutWarning = '<%=MC.M_Usr_SessTimeoutShort%>';
	}

	resetSessionWarningOnTimer();
}
/*$(document).keydown(function(e) {
    if (e.keyCode == 116 && e.ctrlKey) {
        alert('Please save data before refreshing the page.');
        return false;
    }
    if(e.keyCode == 116){
        alert('Please save data before refreshing the page.');
        return false;
    }
});*/
$(function() {
    $('#firstname').on('keydown keyup keypress', function() {
		$("#patid").val("");
        });
    $('#lastname').on('keydown keyup keypress', function() {
		$("#patid").val("");
        });
    $("#patientId").on('keydown keyup keypress', function() {
		$("#patid").val("");
        });
	$(".datefield").on('keydown keyup keypress', function() {
		$("#patid").val("");
        });
});
$.noConflict();
var $j = jQuery;
$j(window).bind('beforeunload',function(){

    //save info somewhere
    console.log(refreshPageAlert);
   if(refreshPageAlert==1)
   	   return 'Hello?';
   if(refreshPageAlert==0)
	   refreshPageAlert=1;

});
</script>
    <style>
        #scrollable-dropdown-menu .dropdown-menu {
            max-height: 300px;
            overflow-y: auto;
        }

        .grid .ui-grid-row .green {
            background-color: lightgreen !important; 
        }

        .grid .ui-grid-row .grey {
            background-color: lightgrey !important;
        }

        .grid .ui-grid-row .lightblue {
            background-color: lightblue !important;
        }

        .grid .ui-grid-row .pink {
            background-color: pink !important;
        }
        .modal { overflow: auto !important;
        		}
		.text {
			color: #B80000;
				}

    </style>
</head>
<body>
<jsp:include page="sessionwarning.jsp" flush="true">
	<jsp:param name="includeJQueryUtils" value="N" />
</jsp:include>
<%
   HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession)){
	String modRight = (String) tSession.getValue("modRight");
	int modlen = modRight.length();

	ctrl.getControlValues("module");
	int ctrlrows = ctrl.getCRows();

	ArrayList feature =  ctrl.getCValue();
	ArrayList ftrDesc = ctrl.getCDesc();
	ArrayList ftrSeq = ctrl.getCSeq();
	ArrayList ftrRight = new ArrayList();

	String strR;

	for (int counter = 0; counter <= (modlen - 1);counter ++)
	{
		strR = String.valueOf(modRight.charAt(counter));
		ftrRight.add(strR);
	}

	grpRights.setGrSeq(ftrSeq);
    grpRights.setFtrRights(ftrRight);
	grpRights.setGrValue(feature);
	grpRights.setGrDesc(ftrDesc);
	int eptRight = 0;
	eptRight  = Integer.parseInt(grpRights.getFtrRightsByValue("MODEPT"));
	SchCodeDao schCDao = new SchCodeDao();
	schCDao.getCodeValues("eventstatus");
	ArrayList cId = schCDao.getCId();
    ArrayList cSubType= schCDao.getCSubType();
%>
<script type="text/javascript">
eventStatusJson = {};
eventCovTypeJson = {};
jwttoken = '<%=tSession.getAttribute("jwttoken")%>';
userId = '<%=tSession.getAttribute("userId")%>';
accountId = '<%=tSession.getAttribute("accountId")%>';
grpId  =  '<%=tSession.getAttribute("defUserGroup")%>';
eSign = '<%=tSession.getAttribute("eSign")%>';
ipAdd = '<%=(String) tSession.getValue("ipAdd")%>';
nodeURL= '<%=CFG.CT_URL%>';
eptRight = '<%=eptRight%>';
appDateFormat = '<%=DateUtil.getAppDateFormat()%>';
</script>
<% for(int i=0;i<cId.size();i++){ %>
<script type="text/javascript">
eventStatusJson['<%=cSubType.get(i)%>']='<%=cId.get(i)%>';
</script>
<%} %>
<%
schCDao.resetObject();
schCDao.getCodeValues("coverage_type");
cId = schCDao.getCId();
cSubType= schCDao.getCSubType();
for(int i=0;i<cId.size();i++){ %>
<script type="text/javascript">
eventCovTypeJson['<%=cSubType.get(i)%>']='<%=cId.get(i)%>';
</script>
<%} %>
<div ng-app="myApp">
    <div ng-controller="UCSD_StudyCoordinatorCtrl" ng-cloak>
        <div class="container-fluid">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-1">
                            <img alt="Brand" src="images/velos-squarelogo.png"
                                 class="img-thumbnail img-responsive" width="50">
                        </div>
                        <div class="col-md-8 col-xs-6">
                            <h4>Study Patient Roster</h4>
                        </div>
                        <div class="col-xs-2 pull-right hidden">
                            <img alt="Brand" class="img-thumbnail img-rounded img-responsive"
                                 src="images/UCSD_Health.png">
                        </div>
                        <div class="col-xs-2 pull-right">
                        <%= StringUtil.htmlEncodeXss((String) request.getSession(true).getValue("userName")) %>
						&nbsp;|&nbsp;
                        <a id="homePage" href="#" onClick="refreshPageAlert=0;window.location.href='myHome.jsp'">
									<img style="border:none;" 
									src="../images/home.png" title="My Homepage"/></a>
						&nbsp;|&nbsp;
						<a id="logOut" href="#" onClick="refreshPageAlert=0;window.location.href='logout.jsp'">
						<img src="../images/SignOff.png" title="Logout"/></a>
						</div>
                    </div>
                </div>
                                
                <%--Progress Bar Implement --%>
                
                <div id="spinner" class="spinner" align="center" style="display:none; z-index: 999;">
							<table>
    												<tr><td><img id="img-spinner" alt="Loading" style="width:200px; height:20px" src="./images/loader1.gif" /></td></tr>
    												<tr><td align="center">Processing...</td></tr></table>
															</div>
                  				<div id="sideMenuTogleDIV">

        		</div>
         <div id='pageContent'>
          
        </div>
         <%-- End Progress Bar Implement --%>
         
                <div class="panel-body">
                    <div class="row hidden">
                        <%--Add patients link and other links--%>
                        <div class="col-md-4 col-xs-12 pull-right">
                            <%--Icon sets for links to modules in eresearch--%>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-align-left" aria-hidden="true"></span>
                                </button>
                                <button type="button" class="btn btn-default" aria-label="Left Right">
                                    <span class="glyphicon glyphicon-align-right" aria-hidden="true"></span>
                                </button>
                                <button type="button" class="btn btn-default" aria-label="Bell">
                                    <span class="glyphicon glyphicon-bell" aria-hidden="true"></span>
                                </button>
                                <button type="button" class="btn btn-default" aria-label="Bed">
                                    <span class="glyphicon glyphicon-bed" aria-hidden="true"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <%--Top Panel: Look for Study and other options--%>
                    <div class="row">
                        <%--Search Study--%>
                        <div class="col-sm-5 col-xs-12">
                            <div class="form-group">
                                <h5>Search Patients on Study:</h5>
                                <%--Loading gif--%>
                                <div ng-if="isLoading" class="bg-info">
                                    Loading...
                                    <img src="images/loading_cubeRed.gif">
                                </div>
                                <%--Show if no results are obtained--%>
                                <div ng-if="noResults" class="bg-danger">
                                    {{studySearchError}}
                                </div>
                                <div class="col-xs-12" >
                                    <div id="scrollable-dropdown-menu">
                                        <input id="studySearchInput" type="text"
                                               class="form-control"
                                               placeholder="Search Studies..."
                                               ng-model="selectedStudy"
                                               uib-typeahead="study as study.studyNumber for study in studyLookup($viewValue)| limitTo:8"
                                               typeahead-loading="isLoading"
                                               typeahead-no-results="noResults"
                                               typeahead-min-length="2"
                                               typeahead-wait-ms="400"
                                               autofocus/>
                                    </div>
                                    <!-- <span class="input-group-btn">
                                        <button class="btn btn-default"
                                                type="button"
                                                ng-click="getStudyPatients(selectedStudy.studyNumber)">
                                            Go
                                        </button>
                                    </span> -->
                                </div>
                            </div>
                        </div>
                        <%--
                        Organization
                        ** Commented out: no reason to have this filter at this point **
                        ** Not sure how to populate the org list....yet.
                        --%>
                        <%--<div class="col-md-3">
                            <div class="form-group">
                                <h5>Organizations:</h5>
                                <select class="form-control">
                                    <optgroup label="UCSD-Health">
                                        <option>All</option>
                                        <option>Site1</option>
                                    </optgroup>
                                    <optgroup label="UCSD-CancerCenter">
                                        <option>All</option>
                                        <option>Cancer center 1</option>
                                        <option>Cancer center 2</option>
                                        <option>Cancer center 3</option>
                                    </optgroup>


                                </select>
                            </div>
                        </div>--%>

                        <%--Add Patient Button and Modal --%>
                        <div class="col-md-4 col-xs-6 pull-right">
                            <div ng-if="selectedStudy.studyNumber">
                                <div class="row">
                                    <h5>Add Patients to Study:</h5>
                                </div>
                                <div class="row">
                                    <button class="btn-sm btn-success"
                                            data-toggle="modal"
											data-dismiss="modal"
											ng-click="openPatDialog()">
                                        Add Patient
                                    </button>
                                </div>
                            </div>
                        </div>

                        <%--Add patient modal--%>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header bg-warning">
                                        <button type="button" class="close" ng-click="dialogClose()">&times;</button>
                                        <h4 class="modal-title">Enroll patient in {{selectedStudy.studyNumber}}</h4>
                                    </div>
                                    <div class="modal-body">
                                    <!-- <div class="row">
                                    		<div ng-if="isPatientFLNLoading" class="bg-info">
                                                    Searching for Patients...
                                                    <img src="images/loading_cubeRed.gif">
                                                </div>
                                                <%--Show if no results are obtained--%>
                                                <div ng-if="noPatientFLNResults" class="bg-danger">
                                                    {{patientSearchError}}
                                            </div>
                                            <div class="col-xs-12">
                                            	<button type="button" class="btn btn-default" 
													 ng-click="openSelectPatient(searchedPatient.patFirstName,searchedPatient.patFirstName)">
                                                Search
                                            	</button>
                                            </div>
                                    </div> -->
                                        <div class="row">
                                            <div class="col-xs-12">
                                                Search By Patient ID:
                                                <%--Loading gif--%>
                                                <div ng-if="isPatientLoading" class="bg-info">
                                                    Searching for Patients...
                                                    <img src="images/loading_cubeRed.gif">
                                                </div>
                                                <%--Show if no results are obtained--%>
                                                <div ng-if="noPatientResults" class="bg-danger">
                                                    {{patientSearchError}}
                                                </div>
                                                <!-- <input type="text"
                                                       class="form-control"
                                                       placeholder="Search by patient first name..."
                                                       ng-model="searchedPatient"
													   id="firstname"
                                                       uib-typeahead="patient as patient.patFirstName+' '+patient.patLastName for patient in patientLookup($viewValue)| limitTo:8"
                                                       typeahead-loading="isPatientLoading"
                                                       typeahead-no-results="noPatientResults"
                                                       typeahead-min-length="2"
                                                       typeahead-wait-ms="400"
                                                       autofocus/>-->
                                                <!--search by last name-->
                                                <!-- <input type="text"
                                                       class="form-control"
                                                       placeholder="Search by patient last name..."
                                                       ng-model="searchedPatient"
													   id="lastname"
                                                       uib-typeahead="patient as patient.patFirstName+' '+patient.patLastName for patient in patientLookupLastName($viewValue)| limitTo:8"
                                                       typeahead-loading="isPatientLoading"
                                                       typeahead-no-results="noPatientResults"
                                                       typeahead-min-length="2"
                                                       typeahead-wait-ms="400"
                                                       autofocus/> -->
                                                
                                                <!--search by patient id-->
                                                <input type="text"
                                                       class="form-control"
                                                       placeholder="Search by patient id..."
                                                       ng-model="searchedPatient"
													   id="patid"
                                                       uib-typeahead="patient as patient.patientIdentifier.patientId for patient in patientIDLookup($viewValue)| limitTo:8"
                                                       typeahead-loading="isPatientLoading"
                                                       typeahead-no-results="noPatientResults"
                                                       typeahead-min-length="2"
                                                       typeahead-wait-ms="400"
                                                       typeahead-editable="false"
                                                       autofocus/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-md-6 col-xs-12">
                                                        Patient ID:<span class="text">*</span> <input type="text"
                                                        				   ng-model="searchedPatient.patientIdentifier.patientId" name="patientId" id="patientId"
                                                                           class="form-control"/>
                                                    
                                                   <div ng-if="searchedPatient && searchedPatient.patientIdentifier.PK && searchedPatient.patientIdentifier.PK!=''">
                                                    First Name: <input type="text" readonly="readonly"  ng-model="searchedPatient.patFirstName" name="patFirstName" 
                                                                  class="form-control"></input>

                                                    Last Name: <input type="text" readonly="readonly"  ng-model="searchedPatient.patLastName" name="patLastName" 
                                                                  class="form-control"/>
                                                    </div>		
                                                   <div ng-if="!searchedPatient.patientIdentifier.PK || searchedPatient==null || searchedPatient==''">										
                                                    First Name: <input type="text"  id="firstname" ng-model="searchedPatient.patFirstName" name="patFirstName" 
                                                                 ng-blur="openSelectPatient(searchedPatient.patFirstName,searchedPatient.patLastName,searchedPatient.patDateofBirth)"  class="form-control"></input>

                                                    Last Name: <input type="text" id="lastname"  ng-model="searchedPatient.patLastName" name="patLastName" 
                                                                 ng-blur="openSelectPatient(searchedPatient.patFirstName,searchedPatient.patLastName,searchedPatient.patDateofBirth)"  class="form-control"/>
						 					       </div>	
						 					       <div ng-if="searchedPatient != '' && noPatientResults || buttonFlag==1 || buttonFLNFlag==2">
						   							 Ethnicity:<span class="text">*</span>
													<select class="form-control" ng-model="selectedEthnicity" id="selectedEthnicity" ng-change="showCodeSType('ethnicity',selectedEthnicity)">
													<option ng-repeat="cod in allEthnicityCodeList"  ng-selected="" value="{{cod.code}}">{{cod.description}}</option>
                                                        </select>
														Race:<span class="text">*</span>
														<select class="form-control" ng-model="selectedRace" ng-change="showCodeSType('race',selectedRace)">
                                                            <option ng-repeat="cod in allRaceList"  ng-selected="" value="{{cod.code}}">{{cod.description}}</option>
                                                        </select>
											</div>
													
						    							Patient Study ID: <input type="text" id="patStudyId"
                                                                      ng-model="patientStudyId"
                                                                      class="form-control"/>

                                                </div>
                                                <div class="col-md-6">
                                                        <div ng-if="searchedPatient && searchedPatient.patientIdentifier.PK && searchedPatient.patientIdentifier.PK!=''">
						    							DOB:<span class="text">*</span>  <input type="text" readonly="readonly"  ng-change="openSelectPatient(searchedPatient.patFirstName,searchedPatient.patLastName,searchedPatient.patDateofBirth)" placeholder="MM/dd/yyyy" alt-input-formats="altInputFormats" ng-model="searchedPatient.patDateofBirth" id="patDateofBirth" name="patDateofBirth" class="form-control datefield" uib-datepicker-popup="MM/dd/yyyy" is-open="popup1.opened" datepicker-options="dateOptions1" ng-required="true" close-text="Close"/>
																<!-- <span class="input-group-btn"><button type="button" class="btn btn-default" ng-click="open1()"><i class="glyphicon glyphicon-calendar"></i></button> 
         												 </span>-->
         												</div>
         												<div ng-if="!searchedPatient.patientIdentifier.PK || searchedPatient==null || searchedPatient==''">
						    							DOB:<span class="text">*</span>  <input type="text"  ng-change="openSelectPatient(searchedPatient.patFirstName,searchedPatient.patLastName,searchedPatient.patDateofBirth)" placeholder="MM/dd/yyyy" alt-input-formats="altInputFormats" ng-model="searchedPatient.patDateofBirth" id="patDateofBirth" name="patDateofBirth" class="form-control datefield" uib-datepicker-popup="MM/dd/yyyy" is-open="popup1.opened" datepicker-options="dateOptions1" ng-required="true" close-text="Close"/>
																<span class="input-group-btn"><button type="button" class="btn btn-default" ng-click="open1()"><i class="glyphicon glyphicon-calendar"></i></button> 
         												 </span>
         												</div>
                                                    Organization:<span class="text">*</span> <select class="form-control" ng-model="selectedOrganization" 
                                                    ng-options="organization.Name for organization in organizationList track by organization.Id">
																	</select><input type="hidden"
                                                                         ng-model="searchedPatient.patientIdentifier.organizationId.siteName"
                                                                         class="form-control"/>
                                                                         <input type="hidden"
                                                                         ng-model="searchedPatient.patientIdentifier.organizationId.PK"
                                                                         class="form-control"/>
                                                    <div ng-if="searchedPatient != '' && noPatientResults || buttonFlag==1 || buttonFLNFlag==2">
                                                        Survival Status:<span class="text">*</span> <select class="form-control" ng-model="selectedSurStatus" ng-change="showCodeSType('patient_status',selectedSurStatus)">
                                                            <option ng-repeat="cod in allSurvivalStatList"  ng-selected="" value="{{cod.code}}">{{cod.description}}</option>
                                                        </select>
                                                        Death Date: <input type="text"  placeholder="MM/dd/yyyy" alt-input-formats="altInputFormats" ng-model="searchedPatient.deathDate" class="form-control datefield" uib-datepicker-popup="MM/dd/yyyy" is-open="popup6.opened" datepicker-options="dateOptions" ng-required="true" close-text="Close"/>
														<span class="input-group-btn"> <button type="button" class="btn btn-default" ng-click="open6()"><i class="glyphicon glyphicon-calendar"></i></button>
         												</span>
         												Gender:<span class="text">*</span>
														<select class="form-control" ng-model="selectedGender" ng-change="showCodeSType('gender',selectedGender)">
                                                            <option ng-repeat="cod in allGenderList"  ng-selected="" value="{{cod.code}}">{{cod.description}}</option>
                                                        </select>
                                                        <Input type="hidden" name="patssn" ng-model="searchedPatient.patssn"></input>
														<Input type="hidden" name="patmname" ng-model="searchedPatient.patmname"></input>			
														<input type="hidden" name="patadd1" ng-model="searchedPatient.patadd1" size = 30  MAXLENGTH = 100></input>
														<input type="hidden" name="patcity" ng-model="searchedPatient.patcity" size = 20></input>
														<input type="hidden" name="patstate" ng-model="searchedPatient.patstate"></input>
														<input type="hidden" name="patcountry" ng-model="searchedPatient.patcountry" size = 20></input>
														<input type="hidden" name="patzip" ng-model="searchedPatient.patzip"></input>
														<input type="hidden" name="patcounty" ng-model="searchedPatient.patcounty" size = 20></input>
														<input type="hidden" name="pathphone" ng-model="searchedPatient.pathphone" size = 20></input>
                                                    </div>
                                                </div>
                                                <%--taking out this condition && noPatientResults--%>
                                               <!--  <div ng-if="searchedPatient != ''">  -->
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <%--Patient More Details--%>
                                                            <div class="panel panel-info in" id="patientDetails">
                                                                <div class="panel-heading" role="tab"
                                                                     id="patientMoreDetailsHeading">
                                                                    <h5 class="panel-title">
                                                                        <a role="button" data-toggle="collapse"
                                                                           data-parent="#patientDetails"
                                                                           href="#patientMoreDetails">
                                                                            Additional Patient Details:
                                                                        </a>
                                                                    </h5>
                                                                </div>
                                                                <div id="patientMoreDetails"
                                                                     class="panel-collapse in"
                                                                     role="tabpanel">
                                                                    <div class="panel-body">
                                                                        <%--Left column--%>
                                                                        <div class="row">
                                                                            <div class="col-sm-6">
                                                                            Status:<span class="text">*</span> <select class="form-control" ng-model="selectedPatStatus" ng-change="showCodeSType('patStatus',selectedPatStatus)">
                                                                                    <option ng-repeat="cod in allPatStatCodeList"  ng-selected="" value="{{cod.code}}">{{cod.description}}</option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                Status Date:<span class="text">*</span> <input type="text"  placeholder="MM/dd/yyyy" alt-input-formats="altInputFormats" ng-model="studyStatusDate" class="form-control datefield" uib-datepicker-popup="MM/dd/yyyy" is-open="popup2.opened" datepicker-options="dateOptions" ng-required="true" close-text="Close"/>
																			<span class="input-group-btn"> <button type="button" class="btn btn-default" ng-click="open2()"><i class="glyphicon glyphicon-calendar"></i></button>
         																				 </span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-6">
                                                                                Calendar:<select ng-model="selectedCalendar" id="newPatientCalendarSelect" ng-change="showSelectValue(selectedCalendar)"
                                                                                        class="form-control"><option ng-repeat="cal in allStudyCalendarList" title="{{operator.title}}" ng-selected="" value="{{cal.calendarIdentifier.PK}}">{{cal.calendarName}}</option>
																						</select>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                            Calendar Start Date: <input type="text"  placeholder="MM/dd/yyyy" alt-input-formats="altInputFormats" ng-model="calStartDate" class="form-control datefield" uib-datepicker-popup="MM/dd/yyyy" is-open="popup3.opened" datepicker-options="dateOptions" ng-required="true" close-text="Close"/>
																		<span class="input-group-btn">
            															<button type="button" class="btn btn-default" ng-click="open3()"><i class="glyphicon glyphicon-calendar"></i></button></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                               <!--  </div> -->
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <div class="col-xs-4 pull-right">
                                            <%--Enroll existing patient into the study
                                            Bootstrap modal dismissal 'data-dismiss="modal"' atribute removed
                                            since i added the ng-click.
                                            --%>
                                            <div ng-if="buttonFLNFlag==1 && (searchedPatient.patientIdentifier.PK && searchedPatient.patientIdentifier.PK!='' && buttonFlag!=1)">
                                                <button type="button" class="btn btn-success"
                                                        ng-click="enrollPatientToStudy(searchedPatient,selectedStudy)">
                                                    Enroll
                                                </button>
                                            </div>
                                            <%--If patient does not exist Create patient and then enroll--%>
                                            <div ng-if="noPatientResults || buttonFlag==1 || buttonFLNFlag==2">
                                                <button type="button" class="btn btn-warning" ng-click="createAndEnrollPatientToStudy(selectedStudy,searchedPatient)">
                                                    Create & Enroll
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-xs-2">
                                            <button type="button" class="btn btn-default" ng-click="dialogClose()">
                                                Close
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                                   <%-- Modal cratetion for bulk update through power bar --%>
                        
                    <div class="modal fade" id="eventPowerBarModal" role="dialog" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog">
                         					<!-- Modal content-->
                                <div class="modal-content">
                                 <div class="modal-header bg-warning">
                                        <button type="button" class="close" ng-click="dialogClose()">&times;</button>


                                        <h4 class="modal-title">Bulk Update Event Status For {{patientStudyID}}</h4>
                                    </div>
                                     <div class="modal-body">
                                      <div class="row">
                                       <div class="col-sm-6">
                                       Event Status:<span class="text">*</span> <select class="form-control" ng-model="selectedEventStatus" ng-change="showCodeSType('eventstatus',selectedEventStatus)">
                                                          <option ng-repeat="cod in allEventStatusList"  ng-selected="" value="{{cod.code}}">{{cod.description}}</option>
                                                      </select>
                                       </div>
                                       
                                        <div class="col-sm-6">
                                       Status Date:<span class="text">*</span>  <input type="text"  placeholder="MM/dd/yyyy" alt-input-formats="altInputFormats" ng-model="statusDate" class="form-control" uib-datepicker-popup="MM/dd/yyyy" is-open="popup4.opened" datepicker-options="dateOptions" ng-required="true" close-text="Close"/>
									<span class="input-group-btn">
            						<button type="button" class="btn btn-default" ng-click="open4()"><i class="glyphicon glyphicon-calendar"></i></button></span>
                                       </div>
                                       
                                       <div class="col-sm-6">
                                       Notes:<textarea class="form-control" ng-model="eventNotes" rows="1"></textarea>

                                       </div>
                                       <div class="col-sm-6">



                                       <input type="hidden" class="form-control" />
                                       </div>
                                       <div class="modal-footer">

                                       		<div class="col-xs-3 pull-right">
                                       			<div class="col-xs-2">
                                            		<button type="button" class="btn btn-default" ng-click="addScheduleEventStatus()">
                                                		Bulk Update
                                            		</button>
                                       			 </div>
                                       		 </div>
                                       		 <div class="col-xs-2">
                                            			<button type="button" class="btn btn-default" ng-click="dialogClose()">
                                               				 Close
                                            		</button>
                                        		</div>
                                        </div>
                                       </div>
                                     </div>
                                 </div>
                       		</div>
                       </div>
                        
                        <%-- Modal cratetion for bulk update through power bar --%>
                        
                    <%-- Modal cratetion for Patient Status Change --%>
                        
                    <div class="modal fade" id="patStatUpdate" role="dialog" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog">
                         					<!-- Modal content-->
                                <div class="modal-content">
                                 <div class="modal-header bg-warning">
                                        <button type="button" class="close" ng-click="dialogClose()">&times;</button>

                                        <h4 class="modal-title">Add New Status For {{patientStudyID}}</h4>
                                    </div>
                                     <div class="modal-body">
                                      <div class="row">
                                       <div class="col-sm-6">
                                       Status: <span class="text">*</span> <select class="form-control" ng-model="selectedPatStatus" ng-change="showCodeSType('patStatus',selectedPatStatus)">
                                                                                    <option ng-repeat="cod in allPatStatCodeList"  ng-selected="" value="{{cod.code}}">{{cod.description}}</option>
                                                                    </select>
                                       </div>
                                       
                                        <div class="col-sm-6">
                                       Status Date: <span class="text">*</span>  <input type="text"  placeholder="MM/dd/yyyy" alt-input-formats="altInputFormats" ng-model="patstatusDate" class="form-control" uib-datepicker-popup="MM/dd/yyyy" is-open="popup5.opened" datepicker-options="dateOptions" ng-required="true" close-text="Close"/>
									<span class="input-group-btn">
            						<button type="button" class="btn btn-default" ng-click="open5()"><i class="glyphicon glyphicon-calendar"></i></button></span>
                                       </div>
                                       
                                       <div class="col-sm-6">
                                       Notes: <textarea class="form-control" ng-model="patNotes" rows="1"></textarea>

                                       </div>
                                       <div class="col-sm-6">
                                        Organization: <span class="text">*</span> <select class="form-control" ng-model="selectedOrganization" 
                                                    ng-options="organization.Name for organization in organizationList track by organization.Id">
    																<option value="">Select an Organization</option>
																	</select><input type="hidden"
                                                                         ng-model="searchedPatient.patientIdentifier.organizationId.siteName"
                                                                         class="form-control"/>
                                                                         <input type="hidden"
                                                                         ng-model="searchedPatient.patientIdentifier.organizationId.PK"
                                                                         class="form-control"/>
                                       </div>
                                       <div class="modal-footer">
                                       		<div class="col-xs-4 pull-right">
                                       			<div class="col-xs-2">
                                            		<button type="button" class="btn btn-default" ng-click="addPatStatus()">
                                                		Add Status
                                            		</button>
                                       			 </div>
                                       			 
                                       		 </div>
                                        </div>
                                       </div>
                                     </div>
                                 </div>
                       		</div>
                       </div>
                        
                        <%-- Modal cratetion for Patient Status Change --%>
                        
                        <div class="modal" id="getPatientData" role="dialog" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog">
                         					<!-- Modal content-->
                                <div class="modal-content">
                                 <div class="modal-header bg-warning">
                                        <button type="button" class="close" data-toggle="modal" data-dismiss="modal" ng-click="#">&times;</button>

                                        <h4 class="modal-title">Select Patient From List &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a ng-if="selectEpicButton==1" role="button" data-toggle="modal" data-dismiss="modal"  href="" ng-click="selectPatDataEpic()">Continue to search in Epic</a></h4>
                                    </div>
                                     <div class="modal-body" style="padding:0px;">
                                      <div class="container">
											<table class="table table-striped table-bordered" style="width:auto;">
												<thead>
													<tr>
															<th >select</th>
															<th>Patient ID</th>
															<th>FirstName</th>
															<th>LastName</th>
															<th >DOB</th>
													</tr>
												</thead>
												<tbody>
													<tr ng-repeat="pat in patDataList">
															<td><a role="button"  href="" ng-click="selectPatData(pat)">Select
                                                			</a></td>
															<td style="width:120px; word-wrap: break-word !important; overflow-wrap: break-word !important;">{{pat.patientIdentifier.patientId}}</td>
															<td style="width:130px; word-wrap: break-word !important; overflow-wrap: break-word !important;">{{pat.patFirstName}}</td>
															<td style="width:130px; word-wrap: break-word !important; overflow-wrap: break-word !important;">{{pat.patLastName}}</td>
															<td>{{pat.patDOB}}</td>
													</tr>
												</tbody>
											</table>
										</div>
                                     </div>
                                 </div>
                       		</div>
                       </div>
                       
                       <div class="modal fade" id="addUnscheduleEvent" role="dialog" data-backdrop="static" data-keyboard="false">
    						<div class="modal-dialog">
        						<div class="modal-content">
            						<div class="modal-header bg-warning">
                                        <button type="button" class="close" data-toggle="modal" data-dismiss="modal" ng-click="#">&times;</button>
                                        <h4 class="modal-title">Add Unschedule Events</h4>
                                    </div>
            					<div class="modal-body" style="padding:0px;">
            						<div class="container">
                						<iframe name="preview" id="preview" src="about:blank" width="50%" height="500px" frameborder="0" scrolling="yes" allowautotransparency=true></iframe>
            						</div>
            					</div>
        						</div>
    						</div>
						</div>

                    <div class="row">
                        <%--Patient grid column options save and restore buttons currently hidden--%>
                        <div class="row hidden">
                            <div class="col-md-2 col-xs-4">
                                <button id="save" type="button"
                                        class="btn btn-sm btn-warning" ng-click="saveState()">
                                    <span class="glyphicon glyphicon-floppy-save"></span>
                                    Save Table Formatting
                                </button>
                            </div>
                            <div class="col-md-2 col-xs-4">
                                <button id="restore" type="button"
                                        class="btn btn-sm btn-success" ng-click="restoreState()">
                                    <span class="glyphicon glyphicon-floppy-open"></span>
                                    Restore Table Formatting
                                </button>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <%--If patient not selected show full 12 col grid. if selected show only col3 --%>
                                <div ng-class="{'col-md-4':selectedPatient, 'col-xs-12':!selectedPatient}">
                                    <%--Patient grid panel--%>
                                    <div class="panel panel-warning">
                                        <div class="panel-heading" id="patientGridHeading">
                                            <h5 class="panel-title">
                                                <%--removing collapse attribute --> data-toggle="collapse"--%>
                                                <a role="button" data-parent="#accordion"
                                                   href="#patientGrid">
                                                    Patient List :
                                                    <span ng-if="selectedPatient"> {{selectedPatient.studyPatFirstName}}  {{selectedPatient.studyPatLastName}} Selected</span>
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="patientGrid">
                                            <div class="panel-body">
                                                <div class="col-lg-12">
                                                    <div class="table-responsive">
                                                        <%--removing importer ui-grid-importer--%>
                                                        <div ui-grid="gridOptions" class="grid"
                                                             ui-grid-save-state
                                                             ui-grid-expandable
                                                             ui-grid-auto-resize
                                                             ui-grid-pinning
                                                             ui-grid-move-columns
                                                             ui-grid-grouping
                                                             ui-grid-cellNav
                                                             ui-grid-selection
                                                             ui-grid-resize-columns
                                                             ui-grid-edit
                                                             ui-grid-row-edit
                                                             ui-grid-pagination>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%--end patient grid panel--%>
                                <div class="col-lg-8 col-md-8" ng-if="selectedPatient">
                                    <%--Patient visit grid--%>
                                    <div class="panel panel-info">
                                        <div class="panel-heading" id="patientVisitGridHeading">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-3 col-xs-12">
                                                    <h4 class="panel-title">
                                                        <%-- data-toggle="collapse" --%>
                                                        <a class="collapsed" role="button"
                                                           data-parent="#accordion" href="#"
                                                           ng-click="openPatSchedule();"
                                                           aria-expanded="false"
                                                           aria-controls="collapseTwo">
                                                            <div ng-if="selectedPatient">
                                                                Visit List for {{selectedPatient.studyPatFirstName}}
                                                                {{selectedPatient.studyPatLastName}}

                                                            </div>
                                                        </a>
                                                    </h4>
                                                </div>
												<div class="col-sm-2 col-xs-12 pull-left">
                                                 	<button class="btn btn-sm btn-success"
                                            				ng-click="openWinProtocol(selectedPatient.studyPK,selectedPatient.patientIdentifier.PK)">
                                        					Edit Calendar/Date
                                    					</button>
                                    			</div>
    											 <div class="col-sm-2 col-xs-12 pull-right">
                                                 	<button class="btn btn-sm btn-success"
                                           			 		data-toggle="modal" 
                                           			 		data-dismiss="modal"
                                            				ng-click="openSchEventUpdate()">
                                        					Event Power Bar
                                    					</button>
                                    			</div>
                                    			<div class="col-sm-2 col-xs-12 pull-right">
                                                 	<button class="btn btn-sm btn-success"
                                           			 		ng-click="openWinMulEventEdit()">
                                        					Edit Multiple Events
                                    					</button>
                                    			</div>
                                            </div>
                                        </div>
                                        <div id="patientVisitGrid"
                                             aria-labelledby="headingTwo">
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <%--removing importer ui-grid-importer--%>
                                                    <div ui-grid="patientVisitGridOptions" class="grid" style="height: 377px;"
                                                         ui-grid-expandable
                                                         ui-grid-row-edit
                                                         ui-grid-selection
                                                         ui-grid-auto-resize
                                                         <%--
                                                         ui-grid-resize-columns
                                                         ui-grid-edit commenting oi
                                                         ui-grid-pinning
                                                         ui-grid-auto-resize
                                                         ui-grid-move-columns
                                                         ui-grid-resize-columns
                                                         ui-grid-grouping
                                                         ui-grid-cellNav
                                                         ui-grid-save-state
                                                         --%>
                                                         >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <p>{{soapStatus}}</p>
                </div>
            </div>
            <p>{{soapStatus1}}</p>
        </div>
    </div>
</div>
<%}//end of if body for session
	else{%> 
	 <jsp:include page="timeout.html" flush="true" /> 
<%}%>
	<%--Controller JS--%>
    <script src="js/consultingCustom/angularApp/myApp.js"></script>
    <script src="js/consultingCustom/angularApp/Controllers/studyCoordinatorCtrl.js"></script>
</body>
</html>