<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>


<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>



<% String src=null;
src= request.getParameter("srcmenu");
String mode = request.getParameter("mode");

%>

<BODY>
<jsp:include page="include.jsp" flush="true"/>
<%@ page language="java" import="java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB" %>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="protVisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>

<jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventdefDao"/>
<jsp:useBean id="assocdao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>
<jsp:useBean id="visitdao" scope="request" class="com.velos.esch.business.common.ProtVisitDao"/>


<%
	//*******Logic of this page
	//As per the saving logic, we need to send all the checked values to the SP. 
	//e.g: First the displayType is days and user selects 1,3 and 5 as the displacements. 
	//These are saved in the db also.
	//Now the user changes the displayType to week or month. 
	//The page shows only one checkbox for each week/month.
	//Thus if we read the checked values from this page, we get only 1 as the displacement.
	//If we pass 1 to the SP for saving, we lose 3 and 5. 
	//In order to pass 1, 3 and 5 three array lists - original disp, unchecked disp and checked disp are used in this page 

			//Make an array of original displacements
			//Get an array of unchecked displacements from the previous page
			//Get an array of checked displacements from the previous page
			//Add the checked displacements into original disp array based on their existence
			//Remove the unchecked displacements from original disp array based on their existence
			//Pass the new original disp array for final saving
			//A 3 dimensional array is created.
			//This final 3 dimensional aray has org_id,disp,cost for all the events						 
	//*******

ArrayList ids=new ArrayList();
ArrayList disps=new ArrayList();
ArrayList visits=new ArrayList();
ArrayList costs=new ArrayList();
ArrayList tmpVisits=new ArrayList();



String[] eventId = request.getParameterValues("eventIds");
String[] parentCosts = request.getParameterValues("parentCosts"); 
String protocolId = request.getParameter("protocolId");
String calledFrom = request.getParameter("calledFrom");
String calStatus = request.getParameter("calStatus");
String displayType = request.getParameter("displayType");
String duration = request.getParameter("duration");
String displayDur = request.getParameter("displayDur");
int pageNo = EJBUtil.stringToNum(request.getParameter("pageNo"));
int fromDisp = EJBUtil.stringToNum(request.getParameter("fromDisp"));
int toDisp = EJBUtil.stringToNum(request.getParameter("toDisp"));

String calAssoc=request.getParameter("calassoc");
calAssoc=(calAssoc==null)?"":calAssoc;

HttpSession tSession = request.getSession(true); 

String userId = (String) (tSession.getValue("userId"));
String ipAdd = (String) tSession.getValue("ipAdd");
%>

<DIV id="div1"> 

<INPUT NAME="mode" TYPE=hidden  value=<%=mode%>>

<%
 String eSign = request.getParameter("eSign");
 if (sessionmaint.isValidSession(tSession)) {
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
 	int flg=0;
	int exists = 0;
	int pageRight=0;	
	
	if (calledFrom.equals("S")) {
   	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");	 
  	   if ((stdRights.getFtrRights().size()) == 0){
		 	pageRight= 0;
	   }else{
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
   	   }	
	} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {
			GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
	}
	
	
	if ((!(calStatus.trim().equals("A")) && !(calStatus.trim().equals("F"))) && (pageRight > 4) )	{
	
	 	String oldESign = (String) tSession.getValue("eSign");
		if(!oldESign.equals(eSign)) {
%>
		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
		} else {	
	
	   if(eventId != null) {   
			for(int i=0;i<eventId.length;i++)
			{ 
			ArrayList orgDisp = new ArrayList();
			ArrayList orgVisit=new ArrayList();
			String parentCost = parentCosts[i];		
			 System.out.println("Loop"+i + " with " +" orgDisp " +orgDisp+" orgVisit " + orgVisit); 
			 
			String[] checkedDisp = request.getParameterValues("checkbox"+(i+1)); 

			String[] checkedVisit;
			if (checkedDisp!=null)
			{
			 checkedVisit=new String[checkedDisp.length];
			 }
			else
			{
			checkedVisit=new String[0];
			}
			
			//seperate the visits from displacements for checked checkboxes
			 if (checkedDisp!=null)	
			 {
			   for (int count=0;count<checkedDisp.length;count++)
			   {
			    String tempValue=checkedDisp[count];
			    String[] arrayOfStrings=new String[2];
			     if (tempValue.indexOf(";")>=0)
			     {
			      arrayOfStrings=tempValue.split(";");
			      checkedDisp[count]=arrayOfStrings[0];
			      checkedVisit[count]=arrayOfStrings[1];
			      
			     }
			   
			   }
			 
			 }
			 

			

			int protId = EJBUtil.stringToNum(protocolId);
			
			String[] uncheckedDisp = request.getParameterValues("uncheckedDisp"+(i+1));
			String[] uncheckedVisit;
			if (uncheckedDisp!=null)
			{
			 uncheckedVisit=new String[uncheckedDisp.length];
			 }
			else
			{
			uncheckedVisit=new String[0];
			}
			//seperate the visits from displacements from unchecked boxes
			if (uncheckedDisp!=null)	
			 {
			   for (int count=0;count<uncheckedDisp.length;count++)
			   {
			    String tempValue=uncheckedDisp[count];
			    String[] arrayOfStrings=new String[2];
			     if (tempValue.indexOf(";")>=0)
			     {
			      arrayOfStrings=tempValue.split(";");
			      uncheckedDisp[count]=arrayOfStrings[0];
			      uncheckedVisit[count]=arrayOfStrings[1];
			      
			     }
			   
			   }
			 
			 }
			
			
			
	 System.out.println("checkedDisp"+Arrays.toString(checkedDisp)+"\ncheckedVisit"+Arrays.toString(checkedVisit)+"\nuncheckedDisp"+Arrays.toString(uncheckedDisp));
		
			if (calledFrom.equals("P")  || calledFrom.equals("L")) { 
				//this is to get all the events within a displacement range for a particular row
				ctrldao= eventdefB.getProtRowEvents(protId,EJBUtil.stringToNum(parentCost),fromDisp,toDisp);  
				orgDisp= ctrldao.getDisplacements();
				orgVisit=ctrldao.getFkVisit();
			}
			else if (calledFrom.equals("S")) { //called From Study
				//this is to get all the events within a displacement range
				assocdao= eventassocB.getProtRowEvents(protId,EJBUtil.stringToNum(parentCost),fromDisp,toDisp);  
				orgDisp= assocdao.getDisplacements(); 
				orgVisit=assocdao.getFkVisit();
			}
			
			//Create a dummy arraylist to maintain mapping of visits with displacements in checkedDisp
			//This arraylist will copy the positiong of orgDisp and  later will be passed in eventDisp array 
			//to the bean. These visits wil be updated through bean to table
			//Set the dummy values
			if (orgDisp!=null)
			{
			for (int list=0;list<orgDisp.size();list++)
			{
			tmpVisits.add("-1");
			}
			}
			
			System.out.println("orgEvent-1"+orgVisit + "\n orgDisp "+orgDisp + orgDisp.size() );
			//change the unchecked and checked arrays disp acc to the page #
			if(pageNo!=1){	
				  if(checkedDisp != null){
					for ( int j=0;j<checkedDisp.length;j++) {
						String strDisp = checkedDisp[j];
						Integer newDisp = new Integer(strDisp);
						int calcDisp = newDisp.intValue() + fromDisp-1;
						newDisp = new Integer(calcDisp);
						checkedDisp[j] = newDisp.toString();
						
					}
				  }

				 if(uncheckedDisp != null){ 	
					for ( int j=0;j<uncheckedDisp.length;j++) {
						String strDisp = uncheckedDisp[j];
						if ((strDisp.length())>0) {
							Integer newDisp = new Integer(strDisp);
							int calcDisp = newDisp.intValue() + fromDisp-1;
							newDisp = new Integer(calcDisp);
							uncheckedDisp[j] = newDisp.toString();
						}				
					}				
				 }
			}//end (pageNo!=1)
			
		//new case
		System.out.println("orgEvent-2"+orgVisit);
		if ((orgDisp == null) || ((orgDisp.size()) == 0)) {
			if(checkedDisp != null){
	 			for ( int j=0;j<checkedDisp.length;j++) {
					orgDisp.add(checkedDisp[j]);
					orgVisit.add(checkedVisit[j]);
				}
			}	
		} //end new case
		System.out.println("orgEvent-4"+orgVisit + "\n orgDisp "+orgDisp);
		//modify case
		if ((orgVisit.size()) > 0) {
			//remove the unchecked boxes from orgDisp
			if(uncheckedVisit != null){
				for ( int j=0;j<uncheckedVisit.length;j++) {
					int pos = orgVisit.indexOf(uncheckedVisit[j]);
					
					if ((pos!=-1) && (orgDisp.get(pos).equals(uncheckedDisp[j])) ) { 
						orgDisp.remove(pos);
						orgVisit.remove(pos);
					}
				}
			}
			System.out.println("orgEvent-5"+orgVisit + "\n orgDisp "+orgDisp);
			//add the new checks into orgDisp
			if(checkedDisp != null){			
						System.out.println("checkedDisp-5.1"+Arrays.toString(checkedDisp)+"CheckVisit"+Arrays.toString(checkedVisit));
				//create a dummy arraylist to process all the values for orgDisp
				//There was an extra row inserted if orgDisp =[5,5],chekedDisp=[5,5],because 5
				//at 2 different positions,visitId can vary in orgVisit,and could result in duplicate row
				ArrayList dummy=new ArrayList(orgVisit);
				for ( int j=0;j<checkedDisp.length;j++) {
					System.out.println("DUMMY"+dummy);
					int pos = dummy.indexOf(checkedVisit[j]);
					
					if ((pos==-1)) { 
						orgDisp.add(checkedDisp[j]);
						orgVisit.add(checkedVisit[j]);
					}
					else if (pos>=0)
					{
					System.out.println("POS"+pos);
					dummy.set(pos,"Done");
					if	(orgDisp.get(pos)!=null){
					 if (!(((String)orgDisp.get(pos)).equals(checkedDisp[j])))
					{
						orgDisp.add(checkedDisp[j]);
						orgVisit.add(checkedVisit[j]);
					}
					}					
					}
				}
			}
		
		} //end modify case
			System.out.println("orgEvent-6"+orgVisit + "\n orgDisp "+orgDisp);
		 if(orgDisp != null) {
			for(int j=0;j<orgDisp.size();j++) {
				exists=0;
				flg=1;
				disps.add((String)orgDisp.get(j));
				if ((orgVisit.get(j))!=null)
				visits.add((orgVisit.get(j)).toString());			
				else
				visits.add("");
				ids.add(eventId[i]);
				//set the parent cost in all the event instances
				costs.add(parentCost);
			}
		}
		
	}//end loop eventId
	
		System.out.println("orgEvent-7"+visits + "\n orgDisp "+disps );
	

   
/*SV, 10/21/04, per Rehan, allow even if there is nothing checked.
			if (flg==1)
			{
*/
				String[][] event_disp = new String[disps.size()][4];
				for(int k=0;k<disps.size();k++)
					{
						//out.println("ids " + (String)ids.get(k) + " orgDisp " + (String)disps.get(k) + " cost " + (String)costs.get(k));
						
						event_disp[k][0]=(String)ids.get(k);
						event_disp[k][1]=(String)disps.get(k);
						Integer rowNo = new Integer(k);
						event_disp[k][2]=(String)costs.get(k);
						//if ((k<=orgVisit.size()) && (orgVisit.size()>0) )
						//{
						event_disp[k][3]=(String)visits.get(k);

						//}
					}

				//the final 3 dimensional aray has org_id,disp,cost for all the events
				
				if (calledFrom.equals("P") || calledFrom.equals("L")) {
					eventdefB.AddEventsToProtocol(protocolId,event_disp,fromDisp,toDisp,userId,ipAdd);			
					eventdefB.DeleteProtocolEvents(protocolId);
				}else if (calledFrom.equals("S")) {
			 		eventassocB.AddEventsToProtocol(protocolId,event_disp,fromDisp,toDisp,userId,ipAdd); 
					eventassocB.DeleteProtocolEvents(protocolId);
				}
				// SV, 10/28, This step is common to both lib/study calendars.
//				ProtVisitDao visitdao = new ProtVisitDao(); //SV, 10/28/04, remove the redundant generated visits in the calendar.
				visitdao.DeleteGenVisitsWithNoEvents(EJBUtil.stringToNum(protocolId), calledFrom);
				
//SV, REDTAG, 10/28/04, this doesn't seem to be used!!				visitdao = protVisitB.getProtocolVisits(EJBUtil.stringToNum(protocolId));
/*				totalVisits = visitdao.getTotalVisits();
				if (totalVisits > 0) 
				{
					// If there is at least 1 predefined visit, then add a predefined visit for each event.
				}
*/					

%>
  	<META HTTP-EQUIV=Refresh CONTENT="0;URL=displayDOW.jsp?selectedTab=4&mode=<%=mode%>&duration=<%=duration%>&calledFrom=<%=calledFrom%>&protocolId=<%=protocolId%>&srcmenu=<%=src%>&calStatus=<%=calStatus%>&pageNo=<%=pageNo%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&refresh_flag=Y&calassoc=<%=calAssoc%>"> - 
<%
/*SV, 10/21/04, save even when nothing checked - Rehan, contd.

	}else {

%>
		<br><br><br><br><br>
		<table width=100%>
		<tr>
		<td align="center"><P class="defComments" ><%=MC.M_SelcAtleast_OneDay%><%--Please select at least one day.*****--%></P></td>
		</tr>
		<tr>
		<td align="center">
		<button name="button1" onClick="window.history.back()"><%=LC.L_Back%></button>
		</td>
		</tr>

<%	}
SV, 10/21/04, save even ..........
*/
	} else { // else for if eventId == null
%>
		<br><br><br><br><br>
		<table width=100%>
		<tr>
		<td align="center">		
		<P class="defComments"><%=MC.M_SelcAtleast_OneEvt%><%--Please select atleast one event.*****--%></P></td>
		</tr>
		<tr>
		<td align="center">
		<button name="button1" onClick="window.history.back()"><%=LC.L_Back%></button>
		</td>
		</tr>		
<%
   	} // end for if eventId == null
	}//end of e-sign
	}else{
%>
  	<META HTTP-EQUIV=Refresh CONTENT="0;URL=displayDOW.jsp?selectedTab=4&mode=<%=mode%>&duration=<%=duration%>&calledFrom=<%=calledFrom%>&protocolId=<%=protocolId%>&srcmenu=<%=src%>&calStatus=<%=calStatus%>&pageNo=<%=pageNo%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&refresh_flag=Y&calassoc=<%=calAssoc%>">  
<%	
	} // end of calStatus

}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
<%
}
%>
 
</div>


</BODY>
</HTML>

