<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=MC.M_PatCrf_StatDet%><%--<%=LC.Pat_Patient%> CRF Status Details*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<%@ page language = "java" import = "com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.SchCodeDao,com.velos.eres.business.common.UserDao,com.velos.esch.business.common.EventdefDao,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>

<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="crfB" scope="request" class="com.velos.esch.web.crf.CrfJB"/>
<jsp:useBean id="crfStatB" scope="request" class="com.velos.esch.web.crfStat.CrfStatJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="eventDefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<% 
	String src;
	src= request.getParameter("srcmenu");
	String selectedTab = request.getParameter("selectedTab");
%>
<SCRIPT>

function openwin1() {

      window.open("usersearchdetails.jsp?fname=&lname=&from=crfstatus1","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")

;}

function openwin2(frm) {


	var crfStatVal = frm.crfStat.options[frm.crfStat.selectedIndex].value;
	var crfStatText = frm.crfStat.options[frm.crfStat.selectedIndex].text;
    var lowerText = crfStatText.toLowerCase();

	//if((lowerText == "approved")||(lowerText == "rejected")||(lowerText == "reviewed"))
	//if((crfStatVal == "26")||(crfStatVal == "33")||(crfStatVal == "34")) 
	//{
      window.open("usersearchdetails.jsp?fname=&lname=&from=crfstatus2","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
	/*  }
	  else
	  {
	  alert("To enter value in this field, please select Reviewed/Approved/Rejected as status from the CRF Status drop down");
	  }*/
	  
;}

function openwin3() {
      window.open("usersearchdetails.jsp?fname=&lname=&from=crfstatus3","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")	  
;}




	
	
function openCal(frm) {
  
var crfStatVal = frm.crfStat.options[frm.crfStat.selectedIndex].value;
var crfStatText = frm.crfStat.options[frm.crfStat.selectedIndex].text;
var lowerText = crfStatText.toLowerCase();

if((lowerText == "approved")||(lowerText == "rejected")||(lowerText == "reviewed"))

//if((crfStatVal == "26")||(crfStatVal == "33")||(crfStatVal == "34")) 
	{   
		return fnShowCalendar(frm.crfReviewOn)
	}
else
	{
	alert("<%=MC.M_ApprovedRejected_CrfStat%>");/*alert("To enter value in this field, please select Reviewed/Approved/Rejected as status from the CRF Status drop down");*****/
	}
	
	return true;
}
	
	

function openwin4(formobj)
	{
	  var names = formobj.crfSentToName.value;
	  var ids = formobj.crfSentToId.value;
	  var windowToOpen = "multipleusersearchdetails.jsp?fname=&lname=&from=crfstatus&mode=initial&ids=" + ids+ "&names=" + names ;
      window.open(windowToOpen,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
;}

function  validate(formobj){
 	if (!(validate_col('CRF Number',formobj.crfNumber))) return false
 	if (!(validate_col('CRF Name',formobj.crfName))) return false
	if (!(validate_col('CRF Status',formobj.crfStat))) return false
	if (!(validate_col('e-Signature',formobj.eSign))) return false
	if (!(validate_col('Entered By',formobj.crfEnterByName))) return false
	if (!(validate_col('Associated to Event',formobj.eventId))) return false
	var crfStatVal = formobj.crfStat.options[formobj.crfStat.selectedIndex].value;

	var crfStatText = formobj.crfStat.options[formobj.crfStat.selectedIndex].text;
    var lowerText = crfStatText.toLowerCase();

	/*if((lowerText == "approved")||(lowerText == "rejected")||(lowerText == "reviewed"))

	//if((crfStatVal == "26")||(crfStatVal == "33")||(crfStatVal == "34")) 
	{   
	value=formobj.crfReviewByName.value;
     if(value==''||value==null){
         alert("Please enter data in CRF Status By as you have selected Reviewed/Approved/Rejected as CRF Status")
         formobj.crfReviewByName.focus()
         return false;
     }

	 value=formobj.crfReviewOn.value;
     if(value==''||value==null){
         alert("Please enter data in CRF Status Date as you have selected Reviewed/Approved/Rejected as CRF Status")
         formobj.crfReviewOn.focus()
         return false;
     }

	}*/

	
	
		

<%-- 	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
   } --%>
   
}


</SCRIPT>
<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   

<body>
<br>
<DIV class="browserDefault"  id="div1">

<%

   HttpSession tSession = request.getSession(true); 

   if (sessionmaint.isValidSession(tSession))

	{
	String patProtId= request.getParameter("patProtId");
	String studyId = (String) tSession.getValue("studyId");	
	String statDesc=request.getParameter("statDesc");
	String statid=request.getParameter("statid");	
	String studyVer = request.getParameter("studyVer");
	String formType = request.getParameter("formType");
	int i=0;	
	SchCodeDao cd1 = new SchCodeDao();
	String dCrfStat = "";	
	Calendar cal1 = new GregorianCalendar();
	String mode = request.getParameter("mode");
	int personPK = 0;	
	personPK = EJBUtil.stringToNum(request.getParameter("pkey"));
    person.setPersonPKId(personPK);
 	person.getPersonDetails();
	String uName = (String) tSession.getValue("userName");
	String userIdFromSession = (String) tSession.getValue("userId");	
	String patientId = person.getPersonPId();
	String visit = request.getParameter("visit");
	String visitName=request.getParameter("visitName");
	visitName=(visitName==null)?"":visitName;
	
	String eventName = request.getParameter("eventName");
	if(eventName == null){
		eventName = "";
	} else {
		eventName = eventName.replace('~',' ');
	}
	String crfName = "";
	String crfNumber = "";
	String eventId = "";
	cd1.getCodeValues("crfstatus");
	

	
	// Arvind Start
	String str="";
	ArrayList crfStats = cd1.getCDesc();
	ArrayList crfStatIds = cd1.getCId();
	// Arvind End
	

	String crfId = "";
	String crfStatId = "";
	String crfEnterByName = uName;
	String crfEnterById = userIdFromSession;	
	String crfReviewByName = "";
	String crfReviewById = "";	
	String crfReviewOn = "";
	String crfSentToName = "";
	String crfSentToId = "";
	String crfSentByName = ""; 
	String crfSentById = "";
	String crfSentOn = "";
	String crfStatSentFlag = "";
	String crfStatus = "";

%>

<P class = "userName"> <%= uName %> </P>
<%
	if(formType.equals("crf")){
%>
<P class="sectionHeadings"> <%=MC.M_MngPatSch_CrfStat%><%--Manage <%=LC.Pat_Patients%> >> Schedule >> CRF New Status*****--%> </P>
<%
	} else {
%>
<P class="sectionHeadings"> <%=MC.M_MngPatSch_FrmStat%><%--Manage <%=LC.Pat_Patients%> >> Schedule >> Form Status*****--%> </P>
<%
	} 
%>

<jsp:include page="patienttabs.jsp" flush="true"> 
<jsp:param name="studyVer" value="<%=studyVer%>"/>
<jsp:param name="patientCode" value="<%=patientId%>"/>
</jsp:include>
<br>
<%
	if(formType.equals("crf")){
%>
<table width=100%>
 <tr>
	<td width=20%>
		<B><%=LC.L_Patient_Visit%><%--<%=LC.Pat_Patient%> Visit*****--%>: <%=visitName%> </B>
	</td>
</tr>
</table>
<%
	}
%>
<%--
Mode explained
M - Modify, This means that the crf as well as the staus exists and this page is being opened to modify the details 
N - New, this means that the crf as well as the status has to be entered anew
NS - New status, this means that the crf details already exists and a new status is to be entered against it.    
--%>

<%
	if(mode.equals("M")){
		crfStatId = request.getParameter("crfStatId");
		crfStatB.setCrfStatId(EJBUtil.stringToNum(crfStatId));
		crfStatB.getCrfStatDetails();
		crfId = crfStatB.getCrfStatCrfId();
		
		//Arvind Start
		  crfStatus = crfStatB.getCrfStatCodelstCrfStatId();
		// Arvind End
		
		crfB.setCrfId(EJBUtil.stringToNum(crfId));
		crfB.getCrfDetails();
		
		crfName = crfB.getCrfName();
		crfNumber = crfB.getCrfNumber();
		eventId = crfB.getEvents1Id();
//out.println("eventId" + eventId );
				
		//dCrfStat = cd1.toPullDown("crfStat",EJBUtil.stringToNum(crfStatB.getCrfStatCodelstCrfStatId()));

		crfEnterById = crfStatB.getCrfStatEnterBy();
		crfEnterById = (crfEnterById == null)?"":crfEnterById;		
		if(crfEnterById.length() > 0){
			userB.setUserId(EJBUtil.stringToNum(crfEnterById));
			userB.getUserDetails();		
			crfEnterByName = userB.getUserFirstName() + " "  + userB.getUserLastName() ;
		}

		crfReviewById = crfStatB.getCrfStatReviewBy();
		crfReviewById = (crfReviewById == null)?"":crfReviewById;		
		if(crfReviewById.length() > 0){
			
			userB.setUserId(EJBUtil.stringToNum(crfReviewById));
			userB.getUserDetails();		
			crfReviewByName = userB.getUserFirstName() + " "  + userB.getUserLastName() ;
		}
		
		crfReviewOn = crfStatB.getCrfStatReviewOn();
		crfSentToId = crfStatB.getCrfStatSentTo();
		crfSentToId = (crfSentToId == null)?"":crfSentToId;
		if(crfSentToId.length() > 0){				
			UserDao userDao = userB.getUsersDetails(crfSentToId);
	
			ArrayList fname = userDao.getUsrFirstNames();
			ArrayList lname = userDao.getUsrLastNames();
			for(i=0;i<fname.size();i++){
				crfSentToName = crfSentToName + fname.get(i) + " " + lname.get(i) + ";";
			}
			if(crfSentToName.length() > 0){
				crfSentToName = crfSentToName.substring(0,crfSentToName.length()-1);
			}
		}
		crfSentById = crfStatB.getCrfStatSentBy();
		crfSentById = (crfSentById == null)?"":crfSentById;				
		if(crfSentById.length() > 0){
			userB.setUserId(EJBUtil.stringToNum(crfSentById));
			userB.getUserDetails();
			crfSentByName = userB.getUserFirstName() + " "  + userB.getUserLastName() ;
		}
		crfSentOn = crfStatB.getCrfStatSentOn();
		crfStatSentFlag = crfStatB.getCrfStatSentFlag();
	} else {
		if(mode.equals("NS")){
       		crfId = request.getParameter("crfId");
       		crfB.setCrfId(EJBUtil.stringToNum(crfId));
       		crfB.getCrfDetails();
			crfName = crfB.getCrfName();
			crfNumber = crfB.getCrfNumber();
			eventId = crfB.getEvents1Id();				
		}
		dCrfStat = cd1.toPullDown("crfStat");
	}
	
	EventdefDao eventDefDao = eventDefB.getVisitEvents(EJBUtil.stringToNum(patProtId), EJBUtil.stringToNum(visit));
	ArrayList eventIds = eventDefDao.getEvent_ids();
	ArrayList eventNames = eventDefDao.getNames();
	String eventDD = "";
	
	if(mode.equals("N")) {
		eventDD = "<select name='eventId'><option value='' selected>"+LC.L_Select_AnOption/*Select an option*****/+"</option>";
		for(i=0;i<eventIds.size();i++){
			eventDD = eventDD + "<option value='" + eventIds.get(i) + "'>" + eventNames.get(i) + "</option>";
		}	
		eventDD = eventDD + "</select>";
	} else {
		eventDD = "<select disabled name='eventId'>";	

	String eventTrim=eventId.trim();
	Integer eveTrim=new Integer(eventTrim);

		for(i=0;i<eventIds.size();i++){
			if(((eventIds.get(i))).equals(eveTrim)){	
			
				eventDD = eventDD + "<option value='" + eventIds.get(i) + "' selected>" + eventNames.get(i) + "</option>";
			} else {
				eventDD = eventDD + "<option value='" + eventIds.get(i) + "'>" + eventNames.get(i) + "</option>";
			}
			
		}	
		eventDD = eventDD + "</select>";
	}
	
%>
<Form name="crf" method=post action="updatenewcrfstatus.jsp?" onsubmit="return validate(document.crf)">
<%
	if(formType.equals("crf")){
%>
	<p class = "sectionHeadings" ><%=LC.L_Crf_Status%><%--CRF Status*****--%></p>
<%
	} else {
%>	
	<p class = "sectionHeadings" ><%=LC.L_Form_Status%><%--Form Status*****--%></p>
<%
	}
%>	
	<table width=100%>
		<tr width=100%>
			<td width=35%>
<%
	if(formType.equals("crf")){
%>
		<%=LC.L_Crf_Number%><%--CRF Number*****--%> 
<%
	} else {
%>	
		<%=LC.L_Form_Number%><%--Form Number*****--%>
<%
	}
%>			
		<FONT class="Mandatory">* </FONT>
			</td>
			<td width=65%>
				<input type=text name=crfNumber maxlength=50 size=50
<%
	if(mode.equals("NS")){
%>				
		value="<%=crfNumber%>" readonly  
<%
	} else if(mode.equals("M")){
%>		
		value="<%=crfNumber%>" 
<%
	}
%>
				>
			</td> 
		</tr>
		<tr>
			<td>
<%
	if(formType.equals("crf")){
%>
		<%=LC.L_Crf_Name%><%--CRF Name*****--%> 
<%
	} else {
%>	
		<%=LC.L_Frm_Name%><%--Form Name*****--%>
<%
	}
%>			
				<FONT class="Mandatory">* </FONT>
			</td>
			<td>
				<input type=text name=crfName maxlength=100 size=50
<%
	if(mode.equals("NS")){
%>				
		value="<%=crfName%>" readonly  
<%
	} else if(mode.equals("M")){	
%>		
		value="<%=crfName%>" 
<%
	}
%>
				>
			</td> 
		</tr>
<%
	if(formType.equals("crf")) {
%>		
		<tr>
			<td>
				<%=LC.L_Assoc_Evt%><%--Associated to Event*****--%> <FONT class="Mandatory">* </FONT>
			</td>
			<td>
				<%=eventDD%>				
			</td> 
		</tr>
<%
	} else {
%>		
	<input type=hidden name="eventId" value="0">
<%
	}
%>
		
		<tr>
			<td>
<%
	if(formType.equals("crf")){
%>
		<%=LC.L_Crf_Status%><%--CRF Status*****--%>  
<%
	} else {
%>	
		<%=LC.L_Form_Status%><%--Form Status*****--%> 
<%
	}
%>						
				<FONT class="Mandatory">* </FONT>
			</td>
			
			<%--
			<td>
				CRF Status <FONT class="Mandatory">* </FONT>
			</td>
			<td>
				<%=dCrfStat%>
			</td> 
			
			
			--%>
			
			
			
			
			
			<td>

<%
//Arvind Start

int length=crfStats.size();

if(mode.equals("M"))
{
	str = "<select disabled name=crfStat >";
	}
else
{
  str = "<select name=crfStat >";
}

for(i=0 ;i<length; i++)
{		
Integer test=(Integer)crfStatIds.get(i);
String ar=test.toString();



	if(ar.equals(crfStatus) || (crfStatus.equals("") && i == 0))
	{	
		str = str + "<option selected value='" + crfStatIds.get(i) + "'>" +  crfStats.get(i) + "</option>";
	}
	else
	{
		str = str + "<option value='" + crfStatIds.get(i) + "'>" +  crfStats.get(i) + "</option>";
	}

}
	str = str + "</select>";	

// Arvind End 
%>


<%=str%>
</td>
</tr>

<%
if(mode.equals("M"))
{ %>

	<tr>
			<td colspan = 2>
				<FONT class="Mandatory"><%=MC.M_ChgStat_AddNewStatCrf%><%--To change the status, select 'Add New Status' for this CRF from the CRF Browser*****--%></FONT>
			</td>
		</tr>
<%
}
 %>	
		<tr>
			<td>
				<%=LC.L_Entered_By%><%--Entered By*****--%> <FONT class="Mandatory">* </FONT>
			</td>
			<td>
				<input type=text name=crfEnterByName maxlength=100 size=20 readonly	value='<%=crfEnterByName%>'	> 
				<A HREF="#" onClick="openwin1()"><%=LC.L_Select_User%><%--Select User*****--%></A>
			</td> 
		</tr>
		<input type=hidden name=crfEnterById value='<%=crfEnterById%>'>
		<tr>
			<td>
				<%=LC.L_Crf_StatusBy%><%--CRF Status By*****--%> 
			</td>
			<td>
				<input type=text name=crfReviewByName maxlength=100 size=20 readonly value = '<%=crfReviewByName%>'> 
				<A HREF="#" onClick="openwin2(document.crf)"><%=LC.L_Select_User%><%--Select User*****--%></A>
				
				
				
				
			</td>
		</tr>
		
		<input type=hidden name=crfReviewById value='<%=crfReviewById%>' >
		<tr>
			<td>
				<%=LC.L_Crf_StatusDate%><%--CRF Status Date*****--%> 
			</td>
<%-- INF-20084 Datepicker-- AGodara --%>
			<td>
				<input type=text name="crfReviewOn" class="datefield" size = 10 MAXLENGTH = 10 	READONLY value = '<%=crfReviewOn%>'>
			</td> 
		</tr>						
	</table>
	<p class = "sectionHeadings" ><%=LC.L_Tracking%><%--Tracking*****--%></p>
	<table width=100%>
		<tr width=100%>
			<td width=35%>
				<%=LC.L_Sent_To%><%--Sent To*****--%>
			</td>
			<td width=65%>
				<input type=text name=crfSentToName maxlength=100 size=20 readonly value = '<%=crfSentToName%>' > 
				<A HREF=# onClick='openwin4(document.crf)' ><%=LC.L_SelOrDeSel_Usrs%><%--Select/Deselect User(s)*****--%></A>
			</td>  
		</tr>
		<input type=hidden name=crfSentToId	value='<%=crfSentToId%>' >
		</tr>
		<tr width=100%>
			<td>
			</td>
			<td>
				<input type=checkbox name=crfStatSentFlag maxlength=100 size=20 
<%
	if(mode.equals("M")){
		if(crfStatSentFlag.equals("1")){
%>			
				checked
<%
		}
	} 
%>		
		>
		
<%
	if(formType.equals("crf")) {
%>		
		<%=MC.M_SendNotficInfo_CrfSent%><%--Send notification informing user that a CRF has been sent to them*****--%>
<%
	} else {
%>			
		<%=MC.M_SendNotficInfo_FrmSent%><%--Send notification informing user that a Form has been sent to them*****--%>	
<%
	}
%>				
				</input>
				
			</td>  
		</tr>
		<tr>
			<td>
				<%=LC.L_Sent_By%><%--Sent By*****--%>
			</td>
			<td>
				<input type=text name=crfSentByName maxlength=100 size=20 readonly value = '<%=crfSentByName%>'	> 
				<A HREF=# onClick=openwin3() ><%=LC.L_Select_User%><%--Select User*****--%></A>
			</td>  
		</tr>
		<input type=hidden name=crfSentById value='<%=crfSentById%>' >
		</tr>
		<tr>
			<td>
				<%=LC.L_Sent_On%><%--Sent On*****--%>
			</td>
<%-- INF-20084 Datepicker-- AGodara --%>			
			<td>
				<input type=text name="crfSentOn" class="datefield" size = 10 MAXLENGTH = 10 	READONLY value = '<%=crfSentOn%>'>
			</td>		
		</tr>						
	</table>
<input type=hidden name=crfId value='<%=crfId%>'>
<input type=hidden name=crfStatId value='<%=crfStatId%>'>
<input type=hidden name=mode value='<%=mode%>'>
<input type=hidden name=pkey value='<%=personPK%>'>	
<input type=hidden name=visit value='<%=visit%>'>
<input type=hidden name=visitName value='<%=visitName%>'>
<input type=hidden name=srcmenu value='<%=src%>'>

<input type=hidden name=patProtId value='<%=patProtId%>'>
<input type=hidden name=studyId value='<%=studyId%>'>
<input type=hidden name=statDesc value='<%=statDesc%>'>
<input type=hidden name=statid value='<%=statid%>'>
<input type=hidden name=studyVer value='<%=studyVer%>'>
<input type=hidden name=patientCode value='<%=StringUtil.encodeString(patientId)%>'>

<%
	eventName = eventName.replace(' ','~');
%>
<input type=hidden name=eventName value='<%=eventName%>'>
<%
	eventName = eventName.replace('~',' ');
%>	
<table width=100%>	
	<tr>
<%
	if(!mode.equals("M")){
%>	
	   <td width=35%>
		<%=LC.L_Esignature%><%--e-Signature*****--%> <FONT class="Mandatory">* </FONT>
	   </td>
	   <td width=30%>
		<input type="password" name="eSign" maxlength="8" autocomplete="off">
		
		</td>
		<td width="35%">
		
			<input type="image" src="../images/jpg/Submit.gif" align="right" border="0">
		</td>
<%
	} else { 
%>		
	 <td width="35%">
	<!--		<A href="#" onClick='window.history.back()'><img src='../images/jpg/Back.gif'   align='right' border='0'></img></A>-->
		</td>
<%
	}
%>		
	<tr>
</table>	
<input type=hidden name='formType' value=<%=formType%>>			
</Form>	
<button onClick="history.go(-1);"><%=LC.L_Back%></button>	
<%
}//end of if body for session
else
{
%>
<jsp:include page="timeout.html" flush="true"/> 
<%
}
%>
<div>
<jsp:include page="bottompanel.jsp" flush="true"> 
</jsp:include>   
</div>
</div>
<DIV class="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>   
</DIV>
</body>
</html>

