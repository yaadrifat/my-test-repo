<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<%@page import="com.velos.eres.bulkupload.web.FileUploadAction"%><html>
<jsp:include page="localization.jsp" flush="true"/>
<head>
<title>Bulk Upload Mapping</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
	<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
	<!--<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>--> <!-- YK 02Aug11: Commented for Bug #6728 already using Whichcss_skin.js in panel.jsp -->
	<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
	<%--
	<SCRIPT LANGUAGE="JavaScript" SRC="js/velos/prototype.js"></SCRIPT>
	--%>
<SCRIPT LANGUAGE="JavaScript" SRC="js/dojo/dojo.js"></SCRIPT>
<script type="text/javascript" src="js/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<SCRIPT LANGUAGE="JavaScript" SRC="js/velos/ajaxengine.js"></SCRIPT>
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<SCRIPT language="javascript">
//  AJAX function to fetch specimen ID List - Requirement No: INV 21675
function trimString(stringToTrim) 
{
	return stringToTrim.replace(/^\s+|\s+$/g, "");
}
function processAjaxCall(valCheck) 
{
          tepName=trimString(document.getElementById("savemapfld").value);
	var urlParameters = "tepName="+tepName;
	var resultArray;
	$j.ajax({
		url:'bulkUploadAjaxGetTemplateName.jsp',
		type: "GET",
		async:false,
		data:urlParameters,		
		success: function(data){
		resultArray = trimString(data).split("$");
		 },
		error:function(response) { alert(data); return false; }
	});
	if(resultArray[1]=='false'){	
		alert("<%=ES.ES_MappName_AllExst%>");
		document.getElementById("savemapfld").focus();
		return false;
	}
	else{
	if(valCheck==true)
	   alert("<%=ES.ES_Vald_Pass%>");
	return true;
	}
}


function checkAll(formobj){
    act="check";
 	totcount=formobj.totcount.value;
    if (formobj.chkAll.value=="checked")
		act="uncheck" ;
    if (totcount==1){
       if (act=="uncheck")
		   formobj.fldNm.checked =false ;
       else
		   formobj.fldNm.checked =true ;
	}
    else {
         for (i=0;i<totcount;i++){
             if (act=="uncheck")
				 formobj.fldNm[i].checked=false;
             else
				 formobj.fldNm[i].checked=true;
         }
    }
    if (act=="uncheck")
		formobj.chkAll.value="unchecked";
    else
		formobj.chkAll.value="checked";
}
function checkMandatory(formobj, valCheck){
 	totcount=formobj.totcount.value;
	var mycars = new Array();
	var count = 1;
	var i=0;
	var k=0;
	var orgmanflg= document.getElementById("orgmanflg").value;
	while (count<=totcount) {
	var mantry_text = document.getElementById("Mantry"+count).value;
	var mantry_name = document.getElementById("Name"+count).value;
	var fld_value = document.getElementById(count).value;
	var fld_colName = document.getElementById(count).options[document.getElementById(count).selectedIndex].text;
	if(document.getElementById("map"+count))
	var map_colName = document.getElementById("map"+count).value;
	else
	var map_colName = ""; 
	if(mantry_text==1 && formobj.fldNm[i].checked==false){
	if(mantry_name=='Specimen ID'){
	if(formobj.autospecid.checked==false)
	{
	var paramArray = [mantry_name];
	alert(getLocalizedMessageString("ES_Pls_Sel",paramArray));
	formobj.fldNm[i].focus();
	return false;
	}
	}
	else
	{
	var paramArray = [mantry_name];
	alert(getLocalizedMessageString("ES_Pls_Sel",paramArray));
	formobj.fldNm[i].focus();
	return false;
	}
	}
	if(mantry_name=='Specimen ID' && formobj.autospecid.checked==true && formobj.fldNm[i].checked==true)
	{
	alert("<%=ES.ES_EitherSelc_AutoSpec_OrMap%>");
	formobj.autospecid.focus();
	return false;
	}
	if(formobj.fldNm[i].checked==true){
	if(fld_value==''){
		var paramArray = [mantry_name];
	 alert(getLocalizedMessageString("ES_PlsSelc_FileHeader",paramArray));
	 formobj.fldNm[i].focus();
	 return false;
	}else{if(map_colName!="" && fld_colName!=map_colName){
		var paramArray = [fld_colName, map_colName];
		alert(getLocalizedMessageString("ES_FldName_Doesnt_Match",paramArray));
		return false;
		}
	  }
	}
	var j=0;
	var l=0;
	if(formobj.fldNm[i].checked==true && fld_value!=''){
	mycars[k]=fld_value;
	if(mycars.length>1){
	while (j<mycars.length){
	l=j+1;
	while(l<mycars.length){
	if(mycars[j]==mycars[l]){
		var paramArray = [mantry_name];
		 alert(getLocalizedMessageString("ES_FileHeaderUnq_ForSuppFld",paramArray));
	formobj.fldNm[i].focus();
	//formobj.count.focus();
	return false;
	}
	l++;
	}
	 j++;
	 }
	}
	k++;
	}
	if(fld_value!=''){
	if(formobj.fldNm[i].checked==false){
		var paramArray = [mantry_name];
		 alert(getLocalizedMessageString("ES_PlsSelc_SuppFld",paramArray));
	formobj.fldNm[i].focus();
	return false;
	}
	}	
	 if(orgmanflg==1 && mantry_name=="Organization" && formobj.fldNm[i].checked==false){
	 formobj.fldNm[i].focus();
	 var paramArray = [mantry_name];
	 alert(getLocalizedMessageString("ES_Pls_Sel",paramArray));
	 return false;
	 }
	count++;
	i++;
	}
	if(formobj.savemapchk.checked==true && trimString(formobj.savemapfld.value)=='')
	{
		alert("<%=ES.ES_PlsEntr_MappName%>");
		formobj.savemapfld.focus();
		 return false;
	}
	if(formobj.savemapchk.checked==false && formobj.savemapfld.value!='')
	{
		var paramArray = [formobj.savemapfld.value];
	if (confirm(getLocalizedMessageString("ES_WantToSave_Mapp",paramArray)))
	   {
	   formobj.savemapchk.checked=true;
	   return false;
	   } else
	   {
	   formobj.savemapfld.value='';
	    return false;
	   }
	}
   if(processAjaxCall(valCheck)==false)
   {
    return false;
	   }
}
</script>
<%@page import="com.velos.eres.bulkupload.business.BulkTemplateDao,com.velos.eres.service.util.*,com.velos.eres.bulkupload.business.BulkDao,java.util.*,com.velos.eres.bulkupload.business.bulkDetDao"%>
<% String src;
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<body>
<br>
<DIV class="tabDefTopN" id="div1">
		<jsp:include page="inventorytabs.jsp" flush="true">
		<jsp:param name="selectedTab" value="1"/>
		</jsp:include>
</DIV>
  <%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
{
	String mapping="";
	String userId="";
	String accountId="";
	long rowsReturned = 0;
	int manflg=0;
	int mapVal=-1;
	BulkDao bdao=new BulkDao();
	HashMap<Integer,String> fileHeader=null;
	ArrayList<Integer> pkBulkDet=null; 
	ArrayList<String> fldName=null; 
	ArrayList<Integer> fldFlag=null;
	ArrayList<Integer> fkEntityDetail=null;
	ArrayList fileFldName=null;
	ArrayList<Integer> fileFldColnum=null;
	userId = (String) tSession.getValue("userId");
	accountId = (String) tSession.getValue("accountId");
	pkBulkDet=(ArrayList)tSession.getAttribute("bulkpkfld");
	fldName=(ArrayList)tSession.getAttribute("bulkfldname");
	fileHeader=(HashMap)tSession.getAttribute("bulkfileheader");
	fldFlag=(ArrayList)tSession.getAttribute("bulkfldflag");
	manflg=(Integer)tSession.getAttribute("mandflag");
	mapping=request.getParameter("mapping");
	if(!mapping.equals("0")){
	BulkTemplateDao temDao=new BulkTemplateDao();
	temDao.getMappingDetails(mapping,accountId);
	fkEntityDetail=temDao.getFkEntityDetail();
	fileFldName=temDao.getFileFldName();
	fileFldColnum=temDao.getFileFldColnum();
	}	
   	%>
<DIV class="BrowserBotN BrowserBotN_MI_4"  id="div2"  >
<s:form action="bulkMapping" method="post" name="bulkmapping" class="custom-design-form" onSubmit = "if (checkMandatory(document.bulkmapping,false)== false) {setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">    
	<table class="basetbl outline midAlign lhsFont" width="100%" cellspacing="0" cellpadding="0">
      
      <tr >
		<td height="40"><font class = "sectionHeadingsFrm"><%=ES.ES_StatNew_Uplod%> > <%=ES.ES_FldMapp%></font></td>
        </tr>
 </table>
 <br/>
 <table class="basetbl outline midAlign lhsFont" width="100%" cellspacing="0" cellpadding="0">
	  <tr>
	 <td>
	<table width="40%" cellspacing="0" cellpadding="0" border="1" >
	<tr height="30">
    <td width="7%" >   <input type="checkbox" name="chkAll" value="" onClick="checkAll(document.bulkmapping)"></td>
	<th ><font > <%=ES.ES_VelosEsam_FldName %></font> </th>
    <th ><font ><%=ES.ES_File_FldNAme %></font></th>
    </tr>
	 <%for(int i=0;i<pkBulkDet.size();i++){
	 rowsReturned++;%>
	 <tr height="30">
	 <%if(mapping.equals("0")){%>
	 <td width="7%"><input type="checkbox" name="fldNm" value="<%=pkBulkDet.get(i)%>" /> <br/></td>
	 <%}else{
	 if(fkEntityDetail.contains(pkBulkDet.get(i))){%>
	 <td width="7%"><input type="checkbox" name="fldNm" value="<%=pkBulkDet.get(i)%>" checked /> <br/></td>
	 <%}else{%>
	 <td width="7%"><input type="checkbox" name="fldNm" value="<%=pkBulkDet.get(i)%> "  /> <br/></td>
	 <%}%>
	 <%}%>
	 <td> <%=fldName.get(i)%>
	  <input type="hidden" id="<%="Mantry"+pkBulkDet.get(i).toString()%>"  name="<%="Mantry"+pkBulkDet.get(i).toString()%>" Value="<%=fldFlag.get(i).toString()%>">
	  <input type="hidden" id="<%="Name"+pkBulkDet.get(i).toString()%>"  name="<%="Name"+pkBulkDet.get(i).toString()%>" Value="<%=fldName.get(i)%>">
	  <%if(fldName.get(i).equals("Specimen ID")){%>
	 <input type="hidden" id="specid"  name="specid" Value="<%=pkBulkDet.get(i)%>">
	 <%}%>
	 <% if(fldFlag.get(i)==1){%>
	 <FONT class="Mandatory">*</FONT> &nbsp;
	 <%}
	 else{
    if(fldName.get(i).equals("Organization")){
    if(manflg==1){%>
    <FONT class="Mandatory">*</FONT> &nbsp;
	 <%}%>
	 <%}%>
	  <%}%>
	 </td>
	 <td>
	 <SELECT NAME="<%=pkBulkDet.get(i).toString()%>" id="<%=pkBulkDet.get(i).toString()%>">
	  <OPTION value='' SELECTED><%=ES.ES_Select_AnOption%></OPTION>
	 <%if(mapping.equals("0")){%>
	 <%for (Map.Entry<Integer, String> entry : fileHeader.entrySet()) {%>
	 <OPTION value = "<%=entry.getKey()%>"><%=entry.getValue()%></OPTION>
	 <%}%>
	 <%}else {
	 int index=-1;
	 if(fkEntityDetail.contains(pkBulkDet.get(i)))
	 index=fkEntityDetail.indexOf(pkBulkDet.get(i));
	 for (Map.Entry<Integer, String> entry : fileHeader.entrySet()){
	 if(index>=0 && entry.getKey()==fileFldColnum.get(index)){
		 mapVal = index;
	 %>
	<OPTION value = "<%=entry.getKey()%>" SELECTED><%=entry.getValue()%></OPTION>
	 <%}else{%>
	 <OPTION value = "<%=entry.getKey()%>"><%=entry.getValue()%></OPTION>
	 <%}%>
	 <%}%>
	  <%}%>
	 </SELECT>
	 <%if(mapVal>=0){ %>
	 <input type="hidden" id="map<%=pkBulkDet.get(i).toString()%>" name="map<%=pkBulkDet.get(i).toString()%>" value="<%=fileFldName.get(mapVal)%>">
	 <%mapVal =-1;} %>
	 </td>
	 </tr>
	<%}%>
	<input type="hidden" id="orgmanflg"  name="orgmanflg" Value="<%=manflg%>">
	</table>
	<BR/>
	 </td> 
	 </tr>
	 <tr >
		<td><s:checkbox name="savemapchk"></s:checkbox>&nbsp;&nbsp;<%=ES.ES_SaveMApp_As%>&nbsp;&nbsp;<s:textfield name="savemapfld" id="savemapfld" size="25" maxlength="100"></s:textfield> <br/><br/></td>
        </tr>
	<tr >
		<td><s:checkbox name="autospecid" value="true"></s:checkbox>&nbsp;&nbsp;<%=ES.ES_AutoSpecId_NotProv_InFileOrMapp%><br/><br/></td>
        </tr>
	 <input type="hidden" name="totcount" Value="<%=rowsReturned%>">
	
		 <tr >
	 <td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" name="validate" onclick="checkMandatory(document.bulkmapping,true);"/><%=ES.ES_Validate_Map %></button>
	 &nbsp;&nbsp;<button type="submit"  name="upload"><%=ES.ES_PrevAndSave %></button> </td>
        </tr>
      </table>
</s:form>
</div>
  <%

}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <% 
}
%>
 <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

<DIV class="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>
</body>
</html>