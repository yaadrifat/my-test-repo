<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html class="remove-overflow">
<head>
<title><%=LC.L_Rpt_Central%><%--Report Central*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript">

var screenWidth = screen.width;
var screenHeight = screen.height;

function fOpenWindow(type,section,frmobj){
	width=300;
	height= 300;
	if (type == "Y"){
		width=280;
		height=250;
	}
	if (type == "D"){
		width=500;
		height=400;
	}
	if (type == "M"){
		width=355;
		height=200;
	}
	frmobj.year.value ="";
	frmobj.month.value ="";
	frmobj.dateFrom.value ="";
	frmobj.dateTo.value ="";

	frmobj.range.value = "";
	if (type == 'A'){
		frmobj.range.value = "[<%=LC.L_All%>]"/*frmobj.range.value = "[All]"*****/
	}
	if (type != 'A'){
		windowName=window.open("selectDateRange.jsp?parm="+type +"&section=" +section,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=" +width +",height="+height)
		windowName.resizeTo(width,height);
		windowName.focus();
	}
}

/*function filterFade(repId,flag)
{

var arrayOfFilters= new Array
if (flag=='O')
{
dojo.lfx.html.wipeOut('orgDIV', 300).play();
dojo.lfx.html.wipeOut('orgDIV1', 300).play();
dojo.lfx.html.wipeOut('protCalDIV', 300).play();
dojo.lfx.html.wipeOut('protCalDIV1', 300).play();
dojo.lfx.html.wipeOut('userDIV', 300).play();
dojo.lfx.html.wipeOut('userDIV1', 300).play();

}
else {


dojo.lfx.html.wipeIn('orgDIV', 300).play();
dojo.lfx.html.wipeIn('orgDIV1', 300).play();
dojo.lfx.html.wipeIn('protCalDIV', 300).play();
dojo.lfx.html.wipeIn('protCalDIV1', 300).play();
dojo.lfx.html.wipeIn('userDIV', 300).play();
dojo.lfx.html.wipeIn('userDIV1', 300).play();
}

}*/
function filterFade(repId,repFilter)
{
var currentFilter,dispDiv;
filterApplicable=repFilter;

arrayOfAvailFilters=(document.getElementById("keywordStr").value).split(",");

//if no filters are set for the report, we will show all the options
//this is to override the bahaviour where no filter would be shown
if (filterApplicable.length==0)
{
for (var i=0;i<arrayOfAvailFilters.length;i++)
{
if (filterApplicable.length==0)
 filterApplicable=arrayOfAvailFilters[i];
 else
 filterApplicable=filterApplicable+","+arrayOfAvailFilters[i];

}

}


arrayOfApbFilter=filterApplicable.split(":");

for (var i=0;i<arrayOfAvailFilters.length;i++)
{
currentFilter=arrayOfAvailFilters[i];

if ((typeof(document.getElementById("DIV"+currentFilter))!="undefined") && (document.getElementById("DIV"+currentFilter)!=null))
dispDiv=document.getElementById("DIV"+currentFilter).value.split(",");

if ((filterApplicable.indexOf(currentFilter)>=0))
{
	if (dispDiv!=null)
	{
  for (var j=0;j<dispDiv.length;j++)
  {
	if (dispDiv[j].length>0)
	{
	dojo.lfx.html.wipeIn(dispDiv[j], 300).play();
	}

}
}

} else
{
  if (dispDiv!=null) {
 for (var k=0;k<dispDiv.length;k++)
  {


	  if (dispDiv[k].length>0)
		dojo.lfx.html.wipeOut(dispDiv[k], 300).play();

}
}
}



}

}

function changeReportType(formobj)
{
 formobj.target="";	
 formobj.action="reportcentral.jsp";
 formobj.submit();
}
function checkDepend(keyword)
{
     formobj=document.reports;
	var dependStr=eval("formobj.depend" +keyword+".value");
   if (dependStr.length>0)
   {
		arrayOfStrings=dependStr.split(":");
		for (var i=0;i<arrayOfStrings.length;i++)
		{
		 if (eval("formobj.param" +arrayOfStrings[i]+".value.length==0"))
		 {
		 	tempName=eval("formobj.key"+arrayOfStrings[i]+".value");
		 	var paramArray = [tempName,eval("formobj.key"+keyword+".value")];
		 	alert(getLocalizedMessageString("M_PlsEtr_ValsForSel",paramArray));/*alert("Please enter value(s) for "+tempName+ " before selecting "+eval("formobj.key"+keyword+".value"));*****/
			return false;
		 }

		}

   } // end if for (mandStr.length>0)
   return true;


}
function openLookup(formobj,url,filter) {

	if(formobj.repId.value==281||formobj.repId.value==94||formobj.repId.value==93||formobj.repId.value==95||formobj.repId.value==289||formobj.repId.value==115||formobj.repId.value==81||formobj.repId.value ==266||formobj.repId.value==268||formobj.repId.value==267||formobj.repId.value==269||formobj.repId.value==271||formobj.repId.value==270||formobj.repId.value==272||formobj.repId.value==116||formobj.repId.value==43||formobj.repId.value==15||formobj.repId.value==8){
		url=replaceSubstring(url,"defaultvalue=[ALL]","defaultvalue=");
	}

 // process the depend String to see if current lookup depend on some other values
 //if yes, prompt a message


var tempValue;
var urlL="";
formobj.target="Lookup";
formobj.method="post";
urlL="multilookup.jsp?"+url;
if (filter.length>0){
	if (filter.indexOf(':sessUserId')>=0)
	{
	  tempValue="";
	  tempValue=formobj.sessUserId.value;
	  filter=replaceSubstring(filter,":sessUserId",tempValue);
	}
	if (filter.indexOf(':sessAccId')>=0)
	{
	  tempValue="";
	  tempValue=formobj.sessAccId.value;
	  filter=replaceSubstring(filter,":sessAccId",tempValue);
	}
	if (filter.indexOf(':studyId')>=0)
	{
	  tempValue="";
	  tempValue=formobj.paramstudyId.value;
	 /*if (tempValue.length==0){
	    alert("Please select a study to continue.");
	    return false;
	    }*/
	
	
	 		filter=replaceSubstring(filter,":studyId",tempValue);
	
	}
	
	urlL=urlL+"&"+filter;
}
// Tarun  -  Bug#16988 fix.
var formWin = window.open(urlL,'Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');
if(formWin && !formWin.closed){
	formobj.action= formWin;
	formWin.focus();
}else{
	formobj.action= formWin.location.reload();
	formWin.focus();
}


//formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');
//if (formWin && !formWin.closed) formWin.focus();
//formobj.submit();
formobj.target="";
}
function openwin12() {

    windowName=window.open("multipleusersearchdetails.jsp?mode=initial&fname=&lname=&from=reports&dispFld=seluserId&dataFld=paramuserId&names&ids=","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
	windowName.focus();
	}

function fSetId(frmobj,repStr,repFilter,calledfrom,patientCode)
{
	//alert("calledfrom ="+calledfrom);
	var ind = repStr.indexOf("%");
	frmobj.repId.value = repStr.substring(0,ind);
	frmobj.repName.value = repStr.substring(ind+1,repStr.length);

	if(calledfrom=='patient')
	{
		//alert("Patient");
		if(frmobj.repId.value==93||frmobj.repId.value==95||frmobj.repId.value==289||frmobj.repId.value==115||frmobj.repId.value==81||frmobj.repId.value==66)
		{
			//alert("Rep id ="+frmobj.repId.value);
			if(frmobj.parampatientId.value==0 || frmobj.parampatientId.value=='' ||frmobj.parampatientId.value=="[<%=LC.L_All.toUpperCase()%>]")
			{
				//alert("CODEING");
				//alert("PatientCode  ="+patientCode);
				//alert("ADdgytu");
				frmobj.selpatientId.value=patientCode;
				//alert("DAWQA");
				frmobj.parampatientId.value="[<%=LC.L_All.toUpperCase()%>]";
			}
			if(frmobj.paramstudyId.value==0 || frmobj.paramstudyId.value=='' ||frmobj.paramstudyId.value=="[<%=LC.L_All.toUpperCase()%>]")
			{
				if(frmobj.repId.value==66)
					{
						frmobj.selstudyId.value="[<%=LC.L_All.toUpperCase()%>]";	
					}
				//alert("Decoding");
				else
				{
					frmobj.selstudyId.value='';
					frmobj.paramstudyId.value='';
				}
			}
		}
		else
		{
			//alert("Hii");
			if(frmobj.paramstudyId.value==0 || frmobj.paramstudyId.value==''||frmobj.paramstudyId.value=="[<%=LC.L_All.toUpperCase()%>]")
			{
				frmobj.selpatientId.value="[<%=LC.L_All.toUpperCase()%>]";
				frmobj.parampatientId.value="[<%=LC.L_All.toUpperCase()%>]";
			}
			if(frmobj.paramstudyId.value==0 || frmobj.paramstudyId.value==''||frmobj.paramstudyId.value=="[<%=LC.L_All.toUpperCase()%>]")
			{
				frmobj.selstudyId.value="[<%=LC.L_All.toUpperCase()%>]";
				frmobj.paramstudyId.value="[<%=LC.L_All.toUpperCase()%>]";
			}
		}
	}

	if(calledfrom=='financial')
	{
		//alert("Financial");
		if(frmobj.repId.value==269||frmobj.repId.value==270)
		{
			//alert("Rep id ="+frmobj.repId.value);
			if(frmobj.parampatientId.value==0 || frmobj.parampatientId.value=='' ||frmobj.parampatientId.value=="[<%=LC.L_All.toUpperCase()%>]")
			{
				//alert("CODEING");
				//alert("PatientCode  ="+patientCode);
				//alert("ADdgytu");
				frmobj.selpatientId.value='';
				//alert("DAWQA");
				frmobj.parampatientId.value="[<%=LC.L_All.toUpperCase()%>]";
			}
		}
	}


	if(calledfrom=='')
	{
		var repDObject=document.getElementById("repcat");
		var repDObjValue = repDObject.options[repDObject.selectedIndex].value;
		//for data safety monitering
		if(repDObjValue=='data safety monitoring')
		{
			if(frmobj.repId.value==116)
			{
				if(frmobj.paramstudyId.value=="[<%=LC.L_All.toUpperCase()%>]")
				{
					frmobj.selstudyId.value='';
					frmobj.paramstudyId.value='';
				}
			}
		}
		//for study reports
		if(repDObjValue=='study')
		{
			if(frmobj.repId.value==281||frmobj.repId.value==94)
			{
				if(frmobj.paramstudyId.value==0 || frmobj.paramstudyId.value=='' ||frmobj.paramstudyId.value=="[<%=LC.L_All.toUpperCase()%>]")
				{
					frmobj.selstudyId.value='';	
					frmobj.paramstudyId.value='';
				}
			}
			else
			{
				if(frmobj.paramstudyId.value==0 || frmobj.paramstudyId.value==''||frmobj.paramstudyId.value=="[<%=LC.L_All.toUpperCase()%>]")
				{
					frmobj.selstudyId.value="[<%=LC.L_All.toUpperCase()%>]";
					frmobj.paramstudyId.value="[<%=LC.L_All.toUpperCase()%>]";
				}
			}
		}
		else if(repDObjValue=='financial')
		{
			if(frmobj.repId.value==258||frmobj.repId.value==257||frmobj.repId.value==263||frmobj.repId.value==265||frmobj.repId.value==264||frmobj.repId.value==261||frmobj.repId.value==256||frmobj.repId.value==262||frmobj.repId.value==259||frmobj.repId.value==260)
			{
				if(frmobj.paramstudyId.value==0 || frmobj.paramstudyId.value=='')
				{
					frmobj.selstudyId.value="[<%=LC.L_All.toUpperCase()%>]";
					frmobj.paramstudyId.value="[<%=LC.L_All.toUpperCase()%>]";
				}
			}
			else
			{
				if(frmobj.paramstudyId.value=="[<%=LC.L_All.toUpperCase()%>]" || frmobj.parampatientId.value=="[<%=LC.L_All.toUpperCase()%>]")
				{
					frmobj.selstudyId.value='';
					frmobj.paramstudyId.value='';
					frmobj.selpatientId.value='';
					frmobj.parampatientId.value='';
				}
			}
		}
		else if(repDObjValue=='system administration'){
			if(frmobj.repId.value==43){
				frmobj.seluserId.value='';
				frmobj.paramuserId.value='';
			}
		}
		if(repDObjValue=='patient')
		{
			if(frmobj.repId.value==93||frmobj.repId.value==95||frmobj.repId.value==289||frmobj.repId.value==115||frmobj.repId.value==81)
			{
				if(frmobj.parampatientId.value==0 || frmobj.parampatientId.value=='' ||frmobj.parampatientId.value=="[<%=LC.L_All.toUpperCase()%>]")
				{
					frmobj.selpatientId.value='';
					frmobj.parampatientId.value="[<%=LC.L_All.toUpperCase()%>]";
				}
				if(frmobj.paramstudyId.value==0 || frmobj.paramstudyId.value=='' ||frmobj.paramstudyId.value=="[<%=LC.L_All.toUpperCase()%>]")
				{
					frmobj.selstudyId.value='';
					frmobj.paramstudyId.value='';
				}
			}
			else
			{
				if(frmobj.paramstudyId.value==0 || frmobj.paramstudyId.value==''||frmobj.paramstudyId.value=="[<%=LC.L_All.toUpperCase()%>]")
				{
					frmobj.selpatientId.value="[<%=LC.L_All.toUpperCase()%>]";
					frmobj.parampatientId.value="[<%=LC.L_All.toUpperCase()%>]";
				}
				if(frmobj.paramstudyId.value==0 || frmobj.paramstudyId.value==''||frmobj.paramstudyId.value=="[<%=LC.L_All.toUpperCase()%>]")
				{
					frmobj.selstudyId.value="[<%=LC.L_All.toUpperCase()%>]";
					frmobj.paramstudyId.value="[<%=LC.L_All.toUpperCase()%>]";	
				}
			}
		}
	}
	filterFade(frmobj.repId.value,repFilter);
}

function validate(formobj)
{
  var tempName;
  var mandStr="";
  var repId;
  var arrayOfStrings;

  repId=formobj.repId.value;
 if (repId.length==0)
 {
  alert("<%=MC.M_Selc_Rpt%>");/*alert("Please select a Report");*****/
  return false;
 }

 if (formobj.range.value.length==0)
 {
 alert("<%=MC.M_Etr_DtFilter%>");/*alert("Please enter a Date Filter");*****/
 return false;
 }
  mandStr=$("keyword"+repId).value;

  if (mandStr.length>0)
   {
		arrayOfStrings=mandStr.split(":");
		for (var i=0;i<arrayOfStrings.length;i++)
		{
		if  (arrayOfStrings[i].indexOf("[DATEFILTER]")>=0)
		{
		 tempName=replaceSubstring(arrayOfStrings[i],"[DATEFILTER]","");

		 if ((tempName.indexOf("A")<0) && (formobj.filterType[0].checked))
		 {
		 alert("<%=MC.M_OptNotAvalFilter_SelOpt%>");/*alert("'All' option is not available for the Date Filter. Please select any of the other available options.");*****/
		 return false;
		 }
		 if ((tempName.indexOf("Y")<0) && (formobj.filterType[1].checked))
		 {
		 alert("<%=MC.M_YearOptNotAval_SelOther%>");/*alert("'Year' option is not available for the Date Filter. Please select any of the other available options.");*****/
		 return false;
		 }
		 if ((tempName.indexOf("M")<0) && (formobj.filterType[2].checked))
		 {
		 alert("<%=MC.M_MonthOptNotAval_DtFilter%>");/*alert("'Month' option is not available for the Date Filter. Please select any of the other available options.");*****/
		 return false;
		 }
		 if ((tempName.indexOf("D")<0) && (formobj.filterType[3].checked))
		 {
		 alert("<%=MC.M_DataRangeNotAval_SelOther%>");/*alert("'Date Range' option is not available for the Date Filter. Please select any of the other available options.");*****/
		 return false;
		 }
		 if ((tempName.indexOf("D")==0) && (formobj.filterType[3].checked) && (repId==181 || repId==182))
		 {
	           if(formobj.dateFrom.value == ''){
	        	   var paramArray = ["DATE FROM"];
	        	   alert(getLocalizedMessageString("M_PlsEtr_ValsFor",paramArray));/*alert("Please enter value(s) for "+tempName);*****/
	   		return false;
	           }
	           if(formobj.dateFrom.value == '' || formobj.dateTo.value == ''){
	        	   var paramArray = ["DATE TO"];
	        	   alert(getLocalizedMessageString("M_PlsEtr_ValsFor",paramArray));/*alert("Please enter value(s) for "+tempName);*****/
	   		return false;
	           }
	           var monthsdif=monthDiff(new Date(formobj.dateFrom.value),new Date(formobj.dateTo.value));		     
	           if(monthsdif>11){
	        	 var r=confirm("<%=MC.M_DataRangeNotMoreThan_Year%>");/*alert("The selected 'DATE RANGE' should be with in a period of one Year.");*****/
	        	 if (r==false)
	        	   {
		 formobj.range.value = '';
		 formobj.dateFrom.value = '';
		 formobj.dateTo.value = '';
	        	 return false;
	        	    }	 
	             }
		 }
		}
		else
		{
		if (eval(typeof(eval("formobj.param" +arrayOfStrings[i]))!="undefined")) {
		  if (eval("formobj.param" +arrayOfStrings[i]+".value.length==0"))
		 {
		 	tempName=eval("formobj.key"+arrayOfStrings[i]+".value");
		 	var paramArray = [tempName];
			alert(getLocalizedMessageString("M_PlsEtr_ValsFor",paramArray));/*alert("Please enter value(s) for "+tempName);*****/
			return false;
		 }
		 }
		}
		} //end for

   } // end if for (mandStr.length>0)

    else {

 //Run the validation check based on Mandatory filter columns - Category wide
mandStr =formobj.mandatory.value;
 arrayOfStrings=new Array(1);
 if (mandStr.length>0){

 arrayOfStrings=mandStr.split(":");
 for (var i=0;i<arrayOfStrings.length;i++)
 {
   if (eval("formobj.param" +arrayOfStrings[i]+".value.length==0"))
  {
  	tempName=eval("formobj.key"+arrayOfStrings[i]+".value");
  	var paramArray = [tempName];
	alert(getLocalizedMessageString("M_PlsEtr_ValsFor",paramArray));/*alert("Please enter value(s) for "+tempName);*****/
 	return false;
  }

  else if(formobj.repId.value==269 || formobj.repId.value==270){
   if(formobj.selpatientId.value==''||formobj.selpatientId.value==null)
	{
	var paramArray = ['Patient'];
	alert(getLocalizedMessageString("M_PlsEtr_ValsFor",paramArray));
	return false;
		}
	}
   
 }
 }
 }// end for else (mandStr.length>0)
 //end validation

 formobj.calledfrom.value = formobj.calledfrom.value;
 formobj.target="_blank";
 formobj.action="reportDisplay.jsp";
 formobj.method="post";
 formobj.submit();
 //formobj.target="";
//YPS:Bug# 8779 : Date 20-Mar-2012
 return false;
}

function monthDiff(d1, d2) {
    var months;
    months = d2.getMonth() -     d1.getMonth() + 
    (12 * (d2.getFullYear() - d1.getFullYear()));
    return months <= 0 ? 0 : months;
}

//JM: 12Mar2008: Enh #REP6:
function fnCheck(formobj){

	if (formobj.fltrChk.checked==true)
		formobj.fltrChk.value="1";
	else
		formobj.fltrChk.value="0";


	if (formobj.dnldChk.checked==true)
		formobj.dnldChk.value="1";
	else
		formobj.dnldChk.value="0";


}
</script>

</head>



<%
	String src = "";

	src = request.getParameter("srcmenu");
	String calledfrom = request.getParameter("calledfrom");

	String pkey = "";
	String patientCode = "";
	String studyPk = "";
	String studyNumber  = "";
	String reportsHeading = "";

		if (StringUtil.isEmpty(calledfrom))
		{
			calledfrom = "";
			reportsHeading = "";
		}
		else if (calledfrom.equals("patient"))
		{
			pkey  = request.getParameter("pkey");
			patientCode  = request.getParameter("patientCode");
			patientCode=(patientCode==null)?"":patientCode;
			patientCode=StringUtil.decodeString(patientCode);
			reportsHeading = "";
		}
		else if (calledfrom.equals("study"))
		{
			studyPk  = request.getParameter("studyPk");
			studyNumber  = request.getParameter("studyNumber");
			studyNumber=(studyNumber==null)?"":studyNumber;
			studyNumber=StringUtil.decodeString(studyNumber);
			reportsHeading = "";
		}

		else if (calledfrom.equals("financial"))
		{
			studyPk  = request.getParameter("studyPk");
			studyNumber  = request.getParameter("studyNumber");
			studyNumber=(studyNumber==null)?"":studyNumber;
			studyNumber=StringUtil.decodeString(studyNumber);
			reportsHeading = "";
		}


%>


<%if (StringUtil.isEmpty(calledfrom))
		{ %>
		<jsp:include page="panel.jsp" flush="true">

			<jsp:param name="src" value="<%=src%>" />

		</jsp:include>
<% } %>




<jsp:useBean id="sessionmaint" scope="session"
	class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="repdao" scope="request"
	class="com.velos.eres.business.common.ReportDaoNew" />
<jsp:useBean id="repdao1" scope="request"
	class="com.velos.eres.business.common.ReportDaoNew" />
<jsp:useBean id="grpRights" scope="request"
	class="com.velos.eres.web.grpRights.GrpRightsJB" />
<jsp:useBean id="ctrlD" scope="request"
	class="com.velos.eres.business.common.CtrlDao" />
<jsp:useBean id="siteB" scope="request"
	class="com.velos.eres.web.site.SiteJB" />
<jsp:useBean id="codeB" scope="request"
	class="com.velos.eres.web.codelst.CodelstJB" />
<jsp:useBean id="repB" scope="request"
	class="com.velos.eres.web.report.ReportJBNew" />
<jsp:useBean id="modCtlDao" scope="request"
	class="com.velos.eres.business.common.CtrlDao" />

<%@ page
	import="java.util.*,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.user.*,com.velos.eres.business.common.*,com.velos.eres.web.grpRights.*,com.velos.eres.service.util.VelosResourceBundle"%>



<body class="not-body">

<%if (StringUtil.isEmpty(calledfrom) )
		{ %>
	<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
	<script language="JavaScript" src="overlib.js" type="text/javascript"><!-- overLIB (c) Erik Bosrup --></script>
	<script language="JavaScript" src="overlib_shadow.js" type="text/javascript"><!-- overLIB (c) Erik Bosrup --></script>
	<script language="JavaScript" src="overlib_hideform.js" type="text/javascript"><!-- overLIB (c) Erik Bosrup --></script>
	<br>
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024){	
		if (navigator.userAgent.indexOf("MSIE") == -1){
			document.write('<DIV class="browserDefault custom-browserDefault" id="div1" style="height:530px;">');
		}else{
			document.write('<DIV class="browserDefault custom-browserDefault" id="div1" style="height:490px;">');
		}
	}else{
			document.write('<DIV class="browserDefault" id="div1">'); 
		}
</SCRIPT>
<% }
	else
	{
%>
	<DIV  >
<%
	} //end of else
	String report_id = "", report_name = "", report_type = "", repid_name = "";
	StringBuffer rep_first = new StringBuffer();

	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))

	{

		String sessionInventoryRight = "";

		sessionInventoryRight  = (String) tSession.getAttribute("sessionInventoryRight");

		if (StringUtil.isEmpty(sessionInventoryRight))
		{
			sessionInventoryRight = "0";
		}

		String userId = (String) tSession.getValue("userId");
		UserJB user = (UserJB) tSession.getValue("currentUser");
		String siteId = user.getUserSiteId();
		String siteName = "";
		siteB.setSiteId(EJBUtil.stringToNum(siteId));
		siteB.getSiteDetails();
		siteName = siteB.getSiteName();
		siteName=siteName.replaceAll("\"", "''");
		String acc = (String) tSession.getValue("accountId");
		int accId = EJBUtil.stringToNum(acc);

		//Process all the report categories available
		String reportCat = request.getParameter("repcat");

		String protocolManagementRight = (String) tSession.getValue("protocolManagementRight");

		int protocolManagementRightInt = 0;
		protocolManagementRightInt = EJBUtil.stringToNum(protocolManagementRight);

		String defUserGroup = (String) tSession.getAttribute("defUserGroup");

		reportCat = (reportCat == null) ? "" : reportCat;

		StringBuffer reportCatDD = new StringBuffer();
		CodeDao codeDao = codeB.getCodeDaoInstance();
		codeDao.getCodeCustomCol("report");

		codeDao.setCType("report");
	    codeDao.setForGroup(defUserGroup);
	    codeDao.getHiddenCodelstDataForUserCustom();
	    String hideStr = "";


		ArrayList reportCatList = codeDao.getCodeCustom();
		//Assumption is that reportCatList will never be null
		if (reportCat.length() == 0)
			reportCat = ((String) reportCatList.get(0)).toLowerCase();
		//Retrieve all the filter columns available for this category
		ArrayList repFilterColumns = new ArrayList();
		ArrayList repMapFilterColumns = new ArrayList();
		ArrayList repFilterDispNames = new ArrayList();
		ArrayList repFilterMandatoryList = new ArrayList();
		ArrayList repFilterDependOnList = new ArrayList();
		ArrayList repFilterKeywords = new ArrayList();
		ArrayList repFilterDispDIV = new ArrayList();

		ReportDaoNew repDao = repB.getFilterColumns(reportCat);
		repFilterColumns = repDao.getRepFilterColumns();
		repMapFilterColumns = repDao.getRepMapFilterColumns();
		repFilterDispNames = repDao.getRepFilterDisplayNames();
		repFilterMandatoryList = repDao.getRepFilterMandatoryList();
		repFilterDependOnList = repDao.getRepFilterDependOnList();
		repFilterKeywords = repDao.getRepFilterKeywords();
		repFilterDispDIV=repDao.getRepFilterDispDiv();
		String mandStr = "", dependStr = "", tempMandStr = "", tempDependStr = "", keywordStr = "";

		//process the Mandatory and Depend on list to create delimited Strings
		for (int count = 0; count < repFilterKeywords.size(); count++) {
			if (keywordStr.length() == 0)
		keywordStr = (String) repFilterKeywords.get(count);
			else
		keywordStr = keywordStr+","
				+ (String) repFilterKeywords.get(count);

			tempMandStr = (String) repFilterMandatoryList.get(count);
			tempMandStr = (tempMandStr == null) ? "" : tempMandStr;
			if (tempMandStr.equals("Y")) {
		if (mandStr.length() > 0)
			mandStr = mandStr + ":"
			+ (String) repFilterKeywords.get(count);
		else
			mandStr = (String) repFilterKeywords.get(count);
			}
			/*   tempDependStr = (String) repFilterDependOnList.get(count);
			 tempDependStr = (tempDependStr == null) ? ""
			 : tempDependStr;
			 if (((tempDependStr).length()) > 0) {
			 if (dependStr.length() > 0)
			 dependStr = dependStr + ":"
			 + (String) repFilterKeywords.get(count)
			 + "[VELDEPENDON]" + tempDependStr;
			 else
			 dependStr = (String) repFilterKeywords.get(count)
			 + "[VELDEPENDON]" + tempDependStr;
			 }*/

		}
		//end create Delimited String

		//end retrieving

		//Check rights
		grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
		String modRight = (String) tSession.getValue("modRight");


//JM: 20Nov2009: #3979

	int pageRight = 0;
    pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("REPORTS"));

	if (pageRight>=4){

		int modlen = modRight.length();

		modCtlDao.getControlValues("module"); //get extra modules

		ArrayList modCtlDaofeature = modCtlDao.getCValue();
		ArrayList modCtlDaoftrSeq = modCtlDao.getCSeq();

		int modseq = 0, budseq = 0, mileSeq = 0, milestoneSeq = 0, dsmSeq = 0;
		int patProfileSeq = 0, formLibSeq = 0, dsmModSeq = 0;
		int adhocqSeq = 0;
		int adhocqModSeq = 0;
		int dataSafMonGrpRight = 0, adHocGrpRight = 0;

		char budRight = '0';
		char mileRight = '0';
		char dataSafMonAppRight = '0';
		char patProfileAppRight = '0';
		char formLibAppRight = '0';
		char adhocqAppRight = '0';

		modseq = modCtlDaofeature.indexOf("MODBUD");
		mileSeq = modCtlDaofeature.indexOf("MODMIL");
		dsmModSeq = modCtlDaofeature.indexOf("MODDSM");
		patProfileSeq = modCtlDaofeature.indexOf("MODPATPROF");
		formLibSeq = modCtlDaofeature.indexOf("MODFORMS");
		adhocqModSeq = modCtlDaofeature.indexOf("ADHOCQ");

		budseq = ((Integer) modCtlDaoftrSeq.get(modseq)).intValue();
		milestoneSeq = ((Integer) modCtlDaoftrSeq.get(mileSeq))
		.intValue();
		patProfileSeq = ((Integer) modCtlDaoftrSeq.get(patProfileSeq))
		.intValue();
		formLibSeq = ((Integer) modCtlDaoftrSeq.get(formLibSeq))
		.intValue();
		dsmSeq = ((Integer) modCtlDaoftrSeq.get(dsmModSeq)).intValue();
		adhocqSeq = ((Integer) modCtlDaoftrSeq.get(adhocqModSeq))
		.intValue();

		budRight = modRight.charAt(budseq - 1);
		mileRight = modRight.charAt(milestoneSeq - 1);
		dataSafMonAppRight = modRight.charAt(dsmSeq - 1);
		patProfileAppRight = modRight.charAt(patProfileSeq - 1);
		formLibAppRight = modRight.charAt(formLibSeq - 1);
		adhocqAppRight = modRight.charAt(adhocqSeq - 1);

		//check for data safety montitoring
		dataSafMonGrpRight = Integer.parseInt(grpRights
		.getFtrRightsByValue("DSM"));
		adHocGrpRight = Integer.parseInt(grpRights
		.getFtrRightsByValue("ADHOC"));

		// end checking rights

		reportCatDD.append("<SELECT NAME='repcat' id='repcat' onchange='javascript:changeReportType(document.reports)'>");

		//reportCatDD.append("<OPTION value=''  selected>Select an Option</OPTION>");
		String tempReport = "";
		for (int counter = 0; counter < reportCatList.size(); counter++) {
			tempReport = (String) reportCatList.get(counter);

			hideStr = codeDao.getCodeHide(counter) ;

			if (StringUtil.isEmpty(hideStr))
			{
				hideStr = "N";
			}
			//System.out.println("hideStr " + hideStr );
			if (!hideStr.equals("N"))
        	{
        		continue;
        	}


			if ((tempReport.toLowerCase())
			.equals("data safety monitoring")) {
		if ((String.valueOf(dataSafMonAppRight).compareTo("1") != 0)
				|| (dataSafMonGrpRight <= 0))
			continue;
			}
			if ((tempReport.toLowerCase()).equals("ad-hoc queries")) {
		if ((String.valueOf(adhocqAppRight).compareTo("1") != 0)
				|| (adHocGrpRight <= 0))
			continue;
			}

			if ( (tempReport.toLowerCase()).equals("study") && protocolManagementRightInt <= 0 ) {
				continue;
			}

			if ( (tempReport.toLowerCase()).equals("inventory") && sessionInventoryRight.equals("0") ) {
				continue;
			}




			if (reportCat.equals(tempReport.toLowerCase()))

		reportCatDD.append("<OPTION value = '"
				+ tempReport.toLowerCase() + "' selected>"
				+ tempReport + "</OPTION>");
			else
		reportCatDD.append("<OPTION value = '"
				+ tempReport.toLowerCase() + "'>" + tempReport
				+ "</OPTION>");

		}

		reportCatDD.append("</SELECT>");
		//end processing
		codeDao.resetDao();
		codeDao.getCodeValues("report", reportCat, "codelst_seq");
		ArrayList codeSubTypeList = codeDao.getCSubType();
		ArrayList codeDescList = codeDao.getCDesc();
		ArrayList repIds = new ArrayList();
		ArrayList repNames = new ArrayList();
		ArrayList repColumns = new ArrayList();
		ArrayList repFilters = new ArrayList();
		ArrayList reportFilterKeywords = new ArrayList();
		ArrayList reportFilterApplicable = new ArrayList();
		String helpStr = "", repColumn = "", repFilter = "", reportFilterKeyword = "",repFilterApplicable="";
		int repId = 0;
%>

<Form Name="reports" method="post" action=""><input type="hidden"
	name="srcmenu" value='<%=src%>'/> <input type="hidden"
	name="repId"/> <input type="hidden" name="repName"/>

	 <input	type="hidden" name="year"/> <!-- <input type="hidden" name="paramaccId" value="<%=accId%>"/>-->
<input type="hidden" name="month"/> <input type="hidden"
	name="year1"/> <input type="hidden" name="dateFrom"/> <input
	type="hidden" name="dateTo"/> <input type="hidden"
	name="mandatory" value='<%=mandStr%>'/> <input type="hidden"
	name="depend" value='<%=dependStr%>'/>
	<input type="hidden" id="keywordStr" name="keywordStr" value='<%=keywordStr%>'/>
	
	<%if(calledfrom.isEmpty()){calledfrom="";}%>
	<input type="hidden" name="calledfrom" value="<%=calledfrom%>" />
	 <%
 		
	//print all the keyword and their display values
 		for (int i = 0; i < repFilterKeywords.size(); i++) {

 %> <input
	type="hidden" name="key<%=repFilterKeywords.get(i)%>"
	value="<%=repFilterDispNames.get(i)%>"/> <%
 }
 %>

<Table width="98%">
	<tr>
		<td colspan="3">
		<P class="sectionHeadings"><%=reportsHeading%></P>
		</td>
	</tr>
	<%if (StringUtil.isEmpty(calledfrom) )
		{ %>
	<tr bgcolor="#dcdcdc" class="custom-bg-color" height="40">
		<td><b><%=LC.L_Select_RptType%><%--Select Report Type*****--%></b> &nbsp;&nbsp;<%=reportCatDD.toString()%>
	<!--	&nbsp;&nbsp; <A href="javascript:changeReportType(document.reports)"><IMG
			src="./images/select.gif" align="absMiddle" border="0" alt=""></A>-->
			</td>
	</tr>
		<tr height="2"><td></td></tr>
	<% } //end of if
	%>
</Table>
<%
 if (reportCat.equals("ad-hoc queries")) {
	 /*Changes for: INF-22402 Date:10/04/2012 By:Yogendra Pratap Singh*/
	 tSession.setAttribute("reportCat",reportCat);
	 System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> : dynrepbrowse.jsp");
 %>
</Form>
<jsp:include page="dynrepbrowse.jsp" flush="true">
	<jsp:param name="panel" value="N" />
	<jsp:param name="calledfrom" value="<%=calledfrom%>" />
</jsp:include> <%
 } else {
	 /*Changes for: INF-22402 Date:10/04/2012 By:Yogendra Pratap Singh*/
	 if(null!=tSession.getAttribute("reportCat"))
		 tSession.removeAttribute("reportCat");
 %>
<Table width="98%">
	<tr>
		<td width="50%" valign="TOP">
		<table width="100%">
			<tr><%--YPS:comment for-UI Review Quick-fixes in Data Extraction >> Reporting  --%>
				<!-- <th width="40%"><Font class="reportText"><%=LC.L_Report%><%--Report*****--%></Font></th>
				<th width="10%"><Font class="reportText"></Font></th>-->

			</tr>
			<%
				String codeDescListStr ="";	
				int startIndx =0;
				int stopIndx =0;
				String labelKeyword ="";
				String labelString ="";
				String messageKeyword ="";
				String messageString ="";
				String repNamesStr ="";
				String messageKey ="";
				String tobeReplaced ="";
				String messageParaKeyword []=null;
				for (int i = 0; i < codeSubTypeList.size(); i++) {
					repdao.getReports((String) codeSubTypeList.get(i),
							accId);
					repIds = repdao.getPkReport();
					repNames = repdao.getRepName();
					repColumns = repdao.getRepColumns();
					repFilters = repdao.getRepFilters();
					reportFilterKeywords = repdao.getReportFilterKeywords();
					reportFilterApplicable = repdao.getRepFilterApplicable();
					//System.out.println("reportFilterApplicable"+reportFilterApplicable);

					codeDescListStr = (String)codeDescList.get(i);
					codeDescListStr = (codeDescListStr == null) ? "" :codeDescListStr;
					//KM-Study and Patient Label changes in codelst description for reports.
					while(codeDescListStr.contains("VELLABEL[")){
						startIndx = codeDescListStr.indexOf("VELLABEL[") + "VELLABEL[".length();
						stopIndx = codeDescListStr.indexOf("]",startIndx);
						labelKeyword = codeDescListStr.substring(startIndx, stopIndx);
						labelString = LC.getLabelByKey(labelKeyword);
						codeDescListStr=StringUtil.replaceAll(codeDescListStr, "VELLABEL["+labelKeyword+"]", labelString);
					}

					while(codeDescListStr.contains("VELMESSGE[")){
						startIndx = codeDescListStr.indexOf("VELMESSGE[") + "VELMESSGE[".length();
						stopIndx = codeDescListStr.indexOf("]",startIndx);
						messageKeyword = codeDescListStr.substring(startIndx, stopIndx);
						messageString = MC.getMessageByKey(messageKeyword);
						codeDescListStr=StringUtil.replaceAll(codeDescListStr, "VELMESSGE["+messageKeyword+"]", messageString);
					}
					while(codeDescListStr.contains("VELPARAMESSGE[")){
						startIndx = codeDescListStr.indexOf("VELPARAMESSGE[") + "VELPARAMESSGE[".length();
						stopIndx = codeDescListStr.indexOf("]",startIndx);
						messageKeyword = codeDescListStr.substring(startIndx, stopIndx);
						messageKey=messageKeyword.substring(0, messageKeyword.indexOf("||"));
						messageKey=messageKey.trim();
						messageParaKeyword=messageKeyword.substring(messageKeyword.indexOf("||")+2).split(java.util.regex.Pattern.quote("||"));
						messageString = VelosResourceBundle.getMessageString(messageKey,messageParaKeyword);
						codeDescListStr=StringUtil.replaceAll(codeDescListStr, "VELPARAMESSGE["+messageKeyword+"]", messageString);
					}
					if(!codeDescListStr.contains("word.GIF")){
						if(codeDescListStr.contains(LC.L_Word_Format)){
							tobeReplaced = "<img border=\"0\" title=\""+LC.L_Word_Format+"\" alt=\""+LC.L_Word_Format+"\" src=\"./images/word.GIF\"></img>";
							codeDescListStr=StringUtil.replace(codeDescListStr, LC.L_Word_Format, tobeReplaced);
						}
						if(codeDescListStr.contains(LC.L_Excel_Format)){
							tobeReplaced = "<img border=\"0\" title=\""+LC.L_Excel_Format+"\" alt=\""+LC.L_Excel_Format+"\" src=\"./images/excel.GIF\"></img>";
							codeDescListStr=StringUtil.replace(codeDescListStr,LC.L_Excel_Format, tobeReplaced);
						}
						if(codeDescListStr.contains(LC.L_Printer_FriendlyFormat)){
							tobeReplaced = "<img border=\"0\" src=\"./images/printer.gif\" title=\""+LC.L_Printer_FriendlyFormat+"\" alt=\""+LC.L_Printer_FriendlyFormat+"\"></img>";
							codeDescListStr=StringUtil.replace(codeDescListStr, LC.L_Printer_FriendlyFormat, tobeReplaced);
						}
					}


			%>
			<tr>
				<td colspan="3">
				<p class="reportHeadings"><BR/>
				<%=codeDescListStr%> </p>
				</td>
			</tr>
			<%
						
			
			
			for (int j = 0; j < repIds.size(); j++) {
				if ((j % 2) == 0) {
			%>
			<tr class="browserEvenRow">
				<%
				} else {
				%>
			<tr class="browserOddRow">
				<%
							}
							repId = ((Integer) repIds.get(j)).intValue();
							repColumn = (String) repColumns.get(j);
							repColumn = (repColumn == null) ? "" : repColumn;
							repFilter = (String) repFilters.get(j);
							repFilter = (repFilter == null) ? "" : repFilter;
							reportFilterKeyword = (String) reportFilterKeywords
							.get(j);
							reportFilterKeyword = (reportFilterKeyword == null) ? ""
							: reportFilterKeyword;
							repFilterApplicable=(String) reportFilterApplicable.get(j);
							repFilterApplicable=(repFilterApplicable==null)?"":repFilterApplicable;
							
							while (repColumn.contains("VELLABEL[")){
								startIndx = repColumn.indexOf("VELLABEL[") + "VELLABEL[".length();
								stopIndx = repColumn.indexOf("]",startIndx);
								labelKeyword = repColumn.substring(startIndx, stopIndx);
								labelString = LC.getLabelByKey(labelKeyword);
								repColumn=StringUtil.replaceAll(repColumn, "VELLABEL["+labelKeyword+"]", labelString);
							}
							while(repColumn.contains("VELMESSGE[")){
								startIndx = repColumn.indexOf("VELMESSGE[") + "VELMESSGE[".length();
								stopIndx = repColumn.indexOf("]",startIndx);
								messageKeyword = repColumn.substring(startIndx, stopIndx);
								messageString = MC.getMessageByKey(messageKeyword);
								repColumn=StringUtil.replaceAll(repColumn, "VELMESSGE["+messageKeyword+"]", messageString);
							}
							while(repColumn.contains("VELPARAMESSGE[")){
								startIndx = repColumn.indexOf("VELPARAMESSGE[") + "VELPARAMESSGE[".length();
								stopIndx = repColumn.indexOf("]",startIndx);
								messageKeyword = repColumn.substring(startIndx, stopIndx);
								messageKey=messageKeyword.substring(0, messageKeyword.indexOf("||"));
								messageKey=messageKey.trim();
								messageParaKeyword=messageKeyword.substring(messageKeyword.indexOf("||")+2).split(java.util.regex.Pattern.quote("||"));
								messageString = VelosResourceBundle.getMessageString(messageKey,messageParaKeyword);
								repColumn=StringUtil.replaceAll(repColumn, "VELPARAMESSGE["+messageKeyword+"]", messageString);
							}
							if(!repColumn.contains("word.GIF")){
								if(repColumn.contains(LC.L_Word_Format)){
									tobeReplaced = "<img border=\"0\" title=\""+LC.L_Word_Format+"\" alt=\""+LC.L_Word_Format+"\" src=\"./images/word.GIF\"></img>";
									repColumn=StringUtil.replace(repColumn, LC.L_Word_Format, tobeReplaced);
								}
								if(repColumn.contains(LC.L_Excel_Format)){
									tobeReplaced = "<img border=\"0\" title=\""+LC.L_Excel_Format+"\" alt=\""+LC.L_Excel_Format+"\" src=\"./images/excel.GIF\"></img>";
									repColumn=StringUtil.replace(repColumn,LC.L_Excel_Format, tobeReplaced);
								}
								if(repColumn.contains(LC.L_Printer_FriendlyFormat)){
									tobeReplaced = "<img border=\"0\" src=\"./images/printer.gif\" title=\""+LC.L_Printer_FriendlyFormat+"\" alt=\""+LC.L_Printer_FriendlyFormat+"\"></img>";
									repColumn=StringUtil.replace(repColumn, LC.L_Printer_FriendlyFormat, tobeReplaced);
								}
							}

							
							while (repFilter.contains("VELLABEL[")){
								startIndx = repFilter.indexOf("VELLABEL[") + "VELLABEL[".length();
								stopIndx = repFilter.indexOf("]",startIndx);
								labelKeyword = repFilter.substring(startIndx, stopIndx);
								labelString = LC.getLabelByKey(labelKeyword);
								repFilter=StringUtil.replaceAll(repFilter, "VELLABEL["+labelKeyword+"]", labelString);
							}
							while(repFilter.contains("VELMESSGE[")){
								startIndx = repFilter.indexOf("VELMESSGE[") + "VELMESSGE[".length();
								stopIndx = repFilter.indexOf("]",startIndx);
								messageKeyword = repFilter.substring(startIndx, stopIndx);
								messageString = MC.getMessageByKey(messageKeyword);
								repFilter=StringUtil.replaceAll(repFilter, "VELMESSGE["+messageKeyword+"]", messageString);
							}
							while(repFilter.contains("VELPARAMESSGE[")){
								startIndx = repFilter.indexOf("VELPARAMESSGE[") + "VELPARAMESSGE[".length();
								stopIndx = repFilter.indexOf("]",startIndx);
								messageKeyword = repFilter.substring(startIndx, stopIndx);
								messageKey=messageKeyword.substring(0, messageKeyword.indexOf("||"));
								messageKey=messageKey.trim();
								messageParaKeyword=messageKeyword.substring(messageKeyword.indexOf("||")+2).split(java.util.regex.Pattern.quote("||"));
								messageString = VelosResourceBundle.getMessageString(messageKey,messageParaKeyword);
								repFilter=StringUtil.replaceAll(repFilter, "VELPARAMESSGE["+messageKeyword+"]", messageString);
							}
							if(!repFilter.contains("word.GIF")){
								if(repFilter.contains(LC.L_Word_Format)){
									tobeReplaced = "<img border=\"0\" title=\""+LC.L_Word_Format+"\" alt=\""+LC.L_Word_Format+"\" src=\"./images/word.GIF\"></img>";
									repFilter=StringUtil.replace(repFilter, LC.L_Word_Format, tobeReplaced);
								}
								if(repFilter.contains(LC.L_Excel_Format)){
									tobeReplaced = "<img border=\"0\" title=\""+LC.L_Excel_Format+"\" alt=\""+LC.L_Excel_Format+"\" src=\"./images/excel.GIF\"></img>";
									repFilter=StringUtil.replace(repFilter,LC.L_Excel_Format, tobeReplaced);
								}
								if(repFilter.contains(LC.L_Printer_FriendlyFormat)){
									tobeReplaced = "<img border=\"0\" src=\"./images/printer.gif\" title=\""+LC.L_Printer_FriendlyFormat+"\" alt=\""+LC.L_Printer_FriendlyFormat+"\"></img>";
									repFilter=StringUtil.replace(repFilter, LC.L_Printer_FriendlyFormat, tobeReplaced);
								}
							}
							helpStr = "<li>"+LC.L_Rpt_Col/*Report Columns*****/+":" + repColumn
							+ "</li><br/><li>"+LC.L_Filter_By/*Filter By*****/+":" + repFilter
							+ "</li>";
							System.out.println("helpStr " +helpStr);

							//KM-Study and Patient Label changes for report names.
							repNamesStr = (String)repNames.get(j);
							repNamesStr = (repNamesStr == null) ? "" : repNamesStr;
							while (repNamesStr.contains("VELLABEL[")){
								startIndx = repNamesStr.indexOf("VELLABEL[") + "VELLABEL[".length();
								stopIndx = repNamesStr.indexOf("]",startIndx);
								labelKeyword = repNamesStr.substring(startIndx, stopIndx);
								labelString = LC.getLabelByKey(labelKeyword);
								repNamesStr=StringUtil.replaceAll(repNamesStr, "VELLABEL["+labelKeyword+"]", labelString);
							}
							
							while(repNamesStr.contains("VELMESSGE[")){
								startIndx = repNamesStr.indexOf("VELMESSGE[") + "VELMESSGE[".length();
								stopIndx = repNamesStr.indexOf("]",startIndx);
								messageKeyword = repNamesStr.substring(startIndx, stopIndx);
								messageString = MC.getMessageByKey(messageKeyword);
								repNamesStr=StringUtil.replaceAll(repNamesStr, "VELMESSGE["+messageKeyword+"]", messageString);
							}
							while(repNamesStr.contains("VELPARAMESSGE[")){
								startIndx = repNamesStr.indexOf("VELPARAMESSGE[") + "VELPARAMESSGE[".length();
								stopIndx = repNamesStr.indexOf("]",startIndx);
								messageKeyword = repNamesStr.substring(startIndx, stopIndx);
								messageKey=messageKeyword.substring(0, messageKeyword.indexOf("||"));
								messageKey=messageKey.trim();
								messageParaKeyword=messageKeyword.substring(messageKeyword.indexOf("||")+2).split(java.util.regex.Pattern.quote("||"));
								messageString = VelosResourceBundle.getMessageString(messageKey,messageParaKeyword);
								repNamesStr=StringUtil.replaceAll(repNamesStr, "VELPARAMESSGE["+messageKeyword+"]", messageString);
							}
							if(!repNamesStr.contains("word.GIF")){
								if(repNamesStr.contains(LC.L_Word_Format)){
									tobeReplaced = "<img border=\"0\" title=\""+LC.L_Word_Format+"\" alt=\""+LC.L_Word_Format+"\" src=\"./images/word.GIF\"></img>";
									repNamesStr=StringUtil.replace(repNamesStr, LC.L_Word_Format, tobeReplaced);
								}
								if(repNamesStr.contains(LC.L_Excel_Format)){
									tobeReplaced = "<img border=\"0\" title=\""+LC.L_Excel_Format+"\" alt=\""+LC.L_Excel_Format+"\" src=\"./images/excel.GIF\"></img>";
									repNamesStr=StringUtil.replace(repNamesStr,LC.L_Excel_Format, tobeReplaced);
								}
								if(repNamesStr.contains(LC.L_Printer_FriendlyFormat)){
									tobeReplaced = "<img border=\"0\" src=\"./images/printer.gif\" title=\""+LC.L_Printer_FriendlyFormat+"\" alt=\""+LC.L_Printer_FriendlyFormat+"\"></img>";
									repNamesStr=StringUtil.replace(repNamesStr, LC.L_Printer_FriendlyFormat, tobeReplaced);
								}
							}

				%>
				<td width="40%"><Input Type="radio" name="reportName"
					value="<%=repId%>"
					onClick="fSetId(document.reports,'<%=repId%>%<%=repNamesStr%>','<%=repFilterApplicable%>','<%=calledfrom%>','<%=patientCode %>')"/>
				<%=repNamesStr%> </td>
				<input type="hidden" name="keyword<%=repId%>" id="keyword<%=repId%>"
					value='<%=reportFilterKeyword%>'/>

				<!-----------JM: 18Mar08: enh:#REP6--------------------->
				<input type="hidden" name="keyFilters<%=repId%>"	value='<%=repFilterApplicable%>'/>

				<td width="10%"><img src="../images/jpg/help.jpg" border="0"
					onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS(helpStr)%>',CAPTION,'<%=repNamesStr%>' <% if (i==(codeSubTypeList.size() - 1)){%>,LEFT , ABOVE<%}%> );"
					onmouseout="return nd();" align="left" alt=""></img></td>
				<%
						repdao.resetDao();
						}//end inner for loop with j

							}//end for loop with i
				%>
		</table>
		</td>
		<td width="50%" valign="TOP">
		<DIV id="repFilter">
		<table width="100%" style="border: solid black 1px;">
			<tr>
				<td colspan=5><Font class="reportText"><%=MC.M_AvalRpt_Filters%><%--Available filters for this Report Type are*****--%>:</Font></td>
				</td>
			</tr>
			<tr>
				<td colspan="2"><br />
				<Font class="reportText"><%=LC.L_Date_Filter%><%--Date Filter*****--%>:</Font></td>
			</tr>
			<tr>
				<td colspan="2"><Input type="radio" name="filterType" checked
					value="1" onClick="fOpenWindow('A','1', document.reports)"/>
				<%=LC.L_All%><%--All*****--%> <Input type="radio" name="filterType" value="2"
					onClick="fOpenWindow('Y','1', document.reports)"/> <%=LC.L_Year%><%--Year*****--%> <Input
					type="radio" name="filterType" value="3"
					onClick="fOpenWindow('M','1', document.reports)"/> <%=LC.L_Month%><%--Month*****--%> <Input
					type="radio" name="filterType" value="4"
					onClick="fOpenWindow('D','1', document.reports)"/> <%=LC.L_Date_Range%><%--Date Range*****--%> <br>
				<input type="text" name="range" size="33" READONLY value="[<%=LC.L_All%>]"/><%--<input type="text" name="range" size="33" READONLY value="[<%=LC.L_All%>>]"/>*****--%></td>
			</tr>
			<tr>
				<td colspan="2"><br>
				<br>
				<Font class="reportText"><%=LC.L_Addl_Filters%><%--Additional Filters*****--%>:</Font><BR>
				</td>
			</tr>
			<%
						String tempRepFilterColumn = "";
						
					if (repFilterColumns != null) {
						

					if (repFilterColumns.size() > 0) {
						for (int i = 0; i < repFilterColumns.size(); i++) {
							tempRepFilterColumn = (String) repMapFilterColumns
							.get(i);
							tempRepFilterColumn = (tempRepFilterColumn == null) ? ""
							: tempRepFilterColumn;
							if (tempRepFilterColumn.length() == 0)
						tempRepFilterColumn = (String) repFilterColumns.get(i);

							if (tempRepFilterColumn.indexOf("[SESSSITENAME]") >= 0)
						tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SESSSITENAME]", siteName);

							if (tempRepFilterColumn.indexOf("[SESSSITEID]") >= 0)
						tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SESSSITEID]", siteId);

						if (calledfrom.equals("patient"))
						{
							if (tempRepFilterColumn.indexOf("[SELPATPK]") >= 0)
						tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SELPATPK]", pkey);


							if (tempRepFilterColumn.indexOf("[SELPATCODE]") >= 0)
						tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SELPATCODE]", patientCode);

						}else
						{
								if (tempRepFilterColumn.indexOf("[SELPATPK]") >= 0)
						tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SELPATPK]", "["+LC.L_All.toUpperCase()+"]");/*tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SELPATPK]", "[ALL]");*****/


							if (tempRepFilterColumn.indexOf("[SELPATCODE]") >= 0)
						tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SELPATCODE]", "["+LC.L_All.toUpperCase()+"]");/*	tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SELPATCODE]", "[ALL]");*****/


						}

						//for study
						if (calledfrom.equals("study") || calledfrom.equals("financial"))
						{
							if (tempRepFilterColumn.indexOf("[SELSTUDYPK]") >= 0)
						tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SELSTUDYPK]", studyPk);


							if (tempRepFilterColumn.indexOf("[SELSTUDYNUMBER]") >= 0)
						tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SELSTUDYNUMBER]", studyNumber);

						}else
						{
								if (tempRepFilterColumn.indexOf("[SELSTUDYPK]") >= 0)
						tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SELSTUDYPK]", "["+LC.L_All.toUpperCase()+"]");/*tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SELSTUDYPK]", "[ALL]");*****/


							if (tempRepFilterColumn.indexOf("[SELSTUDYNUMBER]") >= 0)
						tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SELSTUDYNUMBER]", "["+LC.L_All.toUpperCase()+"]");/*tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SELSTUDYNUMBER]", "[ALL]");*****/


						}


						//
							//account ID
							if (tempRepFilterColumn.indexOf("[SESSACC]") >= 0)
						tempRepFilterColumn = StringUtil.replace(
								tempRepFilterColumn, "[SESSACC]",
								acc);

							//logged in user id
							if (tempRepFilterColumn
							.indexOf("[SESSLOGUSER]") >= 0)
						tempRepFilterColumn = StringUtil.replace(
								tempRepFilterColumn,
								"[SESSLOGUSER]", userId);
						
						//KM- Label changes in Additional Filters.
						while(tempRepFilterColumn.contains("VELLABEL[")){
							startIndx = tempRepFilterColumn.indexOf("VELLABEL[") + "VELLABEL[".length();
							stopIndx = tempRepFilterColumn.indexOf("]",startIndx);
							labelKeyword = tempRepFilterColumn.substring(startIndx, stopIndx);
							labelString = LC.getLabelByKey(labelKeyword);
							tempRepFilterColumn=StringUtil.replaceAll(tempRepFilterColumn, "VELLABEL["+labelKeyword+"]", labelString);
						}
						
						while(tempRepFilterColumn.contains("VELMESSGE[")){
							startIndx = tempRepFilterColumn.indexOf("VELMESSGE[") + "VELMESSGE[".length();
							stopIndx = tempRepFilterColumn.indexOf("]",startIndx);
							messageKeyword = tempRepFilterColumn.substring(startIndx, stopIndx);
							messageString = MC.getMessageByKey(messageKeyword);
							tempRepFilterColumn=StringUtil.replaceAll(tempRepFilterColumn, "VELMESSGE["+messageKeyword+"]", messageString);
						}
						while(tempRepFilterColumn.contains("VELPARAMESSGE[")){
							startIndx = tempRepFilterColumn.indexOf("VELPARAMESSGE[") + "VELPARAMESSGE[".length();
							stopIndx = tempRepFilterColumn.indexOf("]",startIndx);
							messageKeyword = tempRepFilterColumn.substring(startIndx, stopIndx);
							messageKey=messageKeyword.substring(0, messageKeyword.indexOf("||"));
							messageKey=messageKey.trim();
							messageParaKeyword=messageKeyword.substring(messageKeyword.indexOf("||")+2).split(java.util.regex.Pattern.quote("||"));
							messageString = VelosResourceBundle.getMessageString(messageKey,messageParaKeyword);
							tempRepFilterColumn=StringUtil.replaceAll(tempRepFilterColumn, "VELPARAMESSGE["+messageKeyword+"]", messageString);
						}
						if(!tempRepFilterColumn.contains("word.GIF")){
							if(tempRepFilterColumn.contains(LC.L_Word_Format)){
								tobeReplaced = "<img border=\"0\" title=\""+LC.L_Word_Format+"\" alt=\""+LC.L_Word_Format+"\" src=\"./images/word.GIF\"></img>";
								tempRepFilterColumn=StringUtil.replace(tempRepFilterColumn, LC.L_Word_Format, tobeReplaced);
							}
							if(tempRepFilterColumn.contains(LC.L_Excel_Format)){
								tobeReplaced = "<img border=\"0\" title=\""+LC.L_Excel_Format+"\" alt=\""+LC.L_Excel_Format+"\" src=\"./images/excel.GIF\"></img>";
								tempRepFilterColumn=StringUtil.replace(tempRepFilterColumn,LC.L_Excel_Format, tobeReplaced);
							}
							if(tempRepFilterColumn.contains(LC.L_Printer_FriendlyFormat)){
								tobeReplaced = "<img border=\"0\" src=\"./images/printer.gif\" title=\""+LC.L_Printer_FriendlyFormat+"\" alt=\""+LC.L_Printer_FriendlyFormat+"\"></img>";
								tempRepFilterColumn=StringUtil.replace(tempRepFilterColumn, LC.L_Printer_FriendlyFormat, tobeReplaced);
							}
						}

							

			%>
			<tr>
				<%=tempRepFilterColumn%>
				<input type="hidden" name="depend<%=repFilterKeywords.get(i)%>"
					value='<%=((String)repFilterDependOnList.get(i))==null?"":repFilterDependOnList.get(i)%>'/>
				<input type="hidden" id="DIV<%=repFilterKeywords.get(i)%>"
					value='<%=((String)repFilterDispDIV.get(i))==null?"":repFilterDispDIV.get(i)%>'/>
			</tr>
			<%
					}
					} else {
			%>
			<td><%=MC.M_NoFiltersAvailable%><%--No Filters available*****--%></td>
			<%
						}
						}
			%>



			<tr>
				<td><input type="checkbox" name="fltrChk" value="0" onclick ="return fnCheck(document.reports);"/> <%=MC.M_DoNotDisp_SelFilters%><%--Do not display selected filters in Report Header*****--%> </td>

			</tr>
			<tr>
				<td><input type=hidden name="dnldChk"  value="0" onclick ="return fnCheck(document.reports);"/> <!--<%=MC.M_DoNotDisp_DldOpt%>--><%--Do not display download options in Report Header*****--%>  </td>

			</tr>

			<tr>
				<td colspan="2" align="center"><br>
				<br>
				<button onClick="return validate(document.reports);"><%=LC.L_Display%></button>
				</td>
			</tr>

		</table>
		</DIV>
		<DIV id="repFilterHidden">

		</DIV>
		</td>
	</tr>
</table>

<%
	} //end for else if reportcat.equals(adhoc)
	%>

	<%

	}//end of page right
	else{
%>
    <%@include file="accessdenied.jsp" %>
<%
	}//end of else for page right

} //end of if session times out

	else {
%> <jsp:include page="timeout.html" flush="true" /> <%
 }
 %>


<%if (StringUtil.isEmpty(calledfrom) )
		{ %>
<div><jsp:include page="bottompanel.jsp" flush="true" /></div>
<% } %>

</DIV>

<%if (StringUtil.isEmpty(calledfrom) )
		{ %>
<DIV class="mainMenu" id="emenu"><jsp:include page="getmenu.jsp"
	flush="true" /></DIV>
<% } %>
</body>

</html>





