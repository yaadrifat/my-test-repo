<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page   	import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.*" %>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
<%
String tab1 = request.getParameter("selectedTab");
boolean isIrb = false; 
if (tab1 != null && tab1.startsWith("irb")) { isIrb = true; } %>

<% String src;
  src= request.getParameter("srcmenu");
  String from = "team";
  String addeUsrIds="";
  String checked="";
%>

<%
HttpSession tSession = request.getSession(true);
String study = "";

    UserDao userDao=new UserDao(); 

   if (sessionmaint.isValidSession(tSession))

   {
	study = (String) tSession.getValue("studyId");
	int studyId= EJBUtil.stringToNum(study);
	String ufname= request.getParameter("fname")==null? "":request.getParameter("fname") ;
	String ulname=request.getParameter("lname")==null?"": request.getParameter("lname");
	String tab= request.getParameter("selectedTab")==null?"": request.getParameter("selectedTab");
	String orgName=request.getParameter("accsites")==null? "":request.getParameter("accsites") ;
	String group=request.getParameter("accgroup")==null? "":request.getParameter("accgroup");
	String jobType=request.getParameter("jobType")==null? "":request.getParameter("jobType");	
	String selUserIds=request.getParameter("selUserIds")==null?"":request.getParameter("selUserIds");

	Integer codeId;

	int count;

	String role;

	CodeDao cdRole = new CodeDao();   

	if(! ("LIND".equals(CFG.EIRB_MODE) )) { 
	  	  cdRole.getCodeValues("role");
	  } else {		  
		  String[] result = CFG.Study_Role_Filter_Subtypes.split(",");
		  String[] codeSubTypes= new String[result.length];		  
	      for (int x=0; x<result.length; x++) {
	         codeSubTypes[x]=result[x];		         
	      }	
	   cdRole.getCodeValuesFilteredBySubtypes("role", codeSubTypes);		  
	} 

	ArrayList cDesc= cdRole.getCDesc();

	ArrayList cId= cdRole.getCId();
	role = cdRole.toPullDown("role");        

	String uName = (String) tSession.getValue("userName");

	int pageRight = 0;

	String accId = (String) tSession.getValue("accountId");

	pageRight = EJBUtil.stringToNum(request.getParameter("right")); 
   if (pageRight > 0 )
	 {
	 userDao.getAvailableTeamUsers(studyId,EJBUtil.stringToNum(accId),ulname.trim(),ufname.trim(),orgName,group,jobType);        
	 	ArrayList usrLastNames;
          ArrayList usrFirstNames;
          ArrayList usrMidNames;
          ArrayList usrIds;       
          ArrayList usrSiteNames;	
          ArrayList usrStats;
	  ArrayList usrTypes;

     String usrLastName = null;
 	 String usrFirstName = null;
	 String usrMidName = null;
	 String usrId=null;		
	 String usrSiteName=null;
	 String usrStat = null;
	 String usrType = null;
	 
	 int counter = 0;%>

    <table width="100%" border=" 1px">
      <tr> 
      <th width="8%"><%=LC.L_Select%><%--Select*****--%> </th>
      <th width="15%"> <%=LC.L_First_Name%><%--First Name*****--%> </th>
	  <th width="15%"> <%=LC.L_Last_Name%><%--Last Name*****--%> </th>
	  <th width="20%"> <%=LC.L_Organization%><%--Organization*****--%> </th>
	  <th width="15%"> <%=LC.L_User_Type%><%--User Type*****--%> </th>
       
      </tr>
      <%

		usrLastNames = userDao.getUsrLastNames();
		
		usrFirstNames = userDao.getUsrFirstNames();

		usrMidNames = userDao.getUsrMidNames();

        usrIds = userDao.getUsrIds();   
		
	    usrSiteNames = userDao.getUsrSiteNames();
		
		usrStats=userDao.getUsrStats();
		
		usrTypes=userDao.getUsrTypes();
		
		int i;
		int lenUsers = usrLastNames.size();
	%>
      <tr id ="browserBoldRow"><%Object[] arguments1 = {lenUsers}; %> 
        <td colspan="6"> <%=MC.M_Tot_NumOfUsers%><%--Total Number of Users*****--%> : <%=VelosResourceBundle.getLabelString("L_Usr_S",arguments1)%><%-- <%= lenUsers%> User(s)*****--%> </td>
      </tr>
      <%
	     for(i = 0 ; i < lenUsers ; i++)
         {
		 usrLastName=((usrLastNames.get(i)) == null)?"-":(usrLastNames.get(i)).toString();
		 usrFirstName=((usrFirstNames.get(i))==null)?"-":(usrFirstNames.get(i)).toString();
		 usrMidName=((usrMidNames.get(i))==null)?"-":(usrMidNames.get(i)).toString();
		 usrId = ((Integer)usrIds.get(i)).toString();
		 usrSiteName=((usrSiteNames.get(i)) == null)?"-":(usrSiteNames.get(i)).toString();
		 addeUsrIds=addeUsrIds+usrId;
		 usrStat=((usrStats.get(i)) == null)?"-":(usrStats.get(i)).toString();
		 usrType=((usrTypes.get(i)) == null)?"-":(usrTypes.get(i)).toString();
		 
		 if (usrStat.equals("A")){
		     usrStat=LC.L_Active_AccUser/*"Active Account User"*****/;
		 }
		 else if (usrStat.equals("B")){
		     usrStat=LC.L_Blocked_User/*"Blocked User"*****/;
		 }
		 else if (usrStat.equals("D")){
		     usrStat=LC.L_Deactivated_User/*"Deactivated User"*****/;
		 }
		 
		 if (usrType.equals("N")){
			 if (usrStat.equals(LC.L_Deactivated_User/*"Deactivated User"*****/))
		         usrStat=MC.M_DeactNon_SysUsr/*"Deactivated Non System User"*****/;
			 else
		         usrStat=MC.M_Non_SystemUser/*"Non System User"*****/;  
		 }
		 if ((i%2)==0) { %>
      <tr class="browserEvenRow"> 
        <%		}
		else{
  %>
      <tr class="browserOddRow"> 
        <%		}
  %>
  		<td width =50> 
  		<%if(selUserIds.indexOf(usrId)>=0){
  		    checked="checked";	
  		}
  		else
  		{
  			 checked="";
  		}
  		%>
          <Input type="checkbox" id="assignUser_<%=usrId%>" name="assign" value = "<%=usrId%>||<%=usrFirstName%>||<%=usrLastName%>||<%=role%>" onclick="selectTeamMember(this,'<%=usrId %>')" <%=checked %> >
        </td>
        <td width =250>  <%=usrFirstName%>  
          <input type="hidden" name="userId" value="<%=usrId%>">
        </td>		
		<td width =250>  <%=usrLastName%>
        </td>	
		<td width =250>  <%=usrSiteName%>            
        </td>
		<td width =250>  <%=usrStat%>            
        </td>
      </tr>
      <%

		}

%>
    </table>
	
	<% 
	
	%>
	
	
	<%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

