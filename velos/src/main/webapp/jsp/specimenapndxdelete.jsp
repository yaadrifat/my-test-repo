<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.aithent.audittrail.reports.AuditUtils"%>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=MC.M_Del_SpmenAppdx%><%--Delete Specimen Appendix*****--%></title>
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>
<SCRIPT>
function  validate(formobj){

      if (!(validate_col('e-Signature',formobj.eSign))) return false ;

	<%-- if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   } --%>
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.SpecimenApndxDao"%><%@page import="com.velos.eres.service.util.MC,com.velos.eres.service.util.LC"%>
<jsp:useBean id="specimenApndxB" scope="page" class="com.velos.eres.web.specimenApndx.SpecimenApndxJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<% String src;
src= request.getParameter("srcmenu");

%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<BODY>
<br>

<DIV class="formDefault" id="div1">
<%

	String specApndxId= "";
	String selectedTab="";
	String mode = "";
	String specId= "";

HttpSession tSession = request.getSession(true);
 if (sessionmaint.isValidSession(tSession))	{

		selectedTab=request.getParameter("selectedTab");
		specId = request.getParameter("specId");
		specApndxId = request.getParameter("specApndxId");

		//JM: 21Jul2009: issue no-4174
		String perId = "", stdId = "";
		perId = request.getParameter("pkey");
	 	perId = (perId==null)?"":perId;

		stdId = request.getParameter("studyId");
		stdId = (stdId==null)?"":stdId;

		String userId = (String) tSession.getValue("userId");

		int ret=0;
		String delMode=request.getParameter("delMode");
		String FromPatPage=request.getParameter("FromPatPage");
		if(FromPatPage==null)
		{
			FromPatPage="";
		}
		String patientCode=request.getParameter("patientCode");
		if(patientCode==null){
			patientCode="";
		}

		if (delMode.equals("null")) {
			delMode="final";
%>
	<FORM name="specApndxDelete" id="specapndxdelfrm" method="post" action="specimenapndxdelete.jsp" onSubmit="if (validate(document.specApndxDelete)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>

	<P class="defComments"><%=MC.M_Etr_Esign_ToProc%><%--Please enter e-Signature to proceed*****--%></P>

	<jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="specapndxdelfrm"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
  	 <input type="hidden" name="specApndxId" value="<%=specApndxId%>">
  	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">
         <input type=hidden   name="specId" value=<%=specId%>>

     <input type="hidden" name="pkey" value="<%=perId%>">
     <input type="hidden" name="studyId" value="<%=stdId%>">
	 <input type="hidden" name="FromPatPage" value="<%=FromPatPage%>">
	 <input type="hidden" name="patientCode" value="<%=patientCode%>">
	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
 		  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
			} else {
				//Update LAST_MODIFIED_BY before delete Specimen Appendix 
				SpecimenApndxDao specimenApndDao =new SpecimenApndxDao();
				specimenApndDao.updateSpecimenapndx(Integer.parseInt(userId),Integer.parseInt(specApndxId));
		    // Modified for INF-18183 ::: Raviesh
			ret =specimenApndxB.removeSpecApndx(EJBUtil.stringToNum(specApndxId),AuditUtils.createArgs(session,"",LC.L_Manage_Invent));

			if (ret==-2) {%>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_LnkOrFileNt_DelSucc%><%--Link/File not deleted successfully*****--%></p>
			<%}else {
			         mode = "M";
			%>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_LnkOrFile_DelSucc%><%--Link/File deleted successfully*****--%></p>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=specimenapndx.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&pkId=<%=specId%>&mode=<%=mode%>&pkey=<%=perId%>&studyId=<%=stdId%>&FromPatPage=<%=FromPatPage%>&patientCode=<%=patientCode%>">
			<%}
			} //end esign
	} //end of delMode
  }//end of if body for session
else { %>
 <jsp:include page="timeout.html" flush="true"/>
 <% } %>
 <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</BODY>
</HTML>
