<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.velos.services.util.CodeCache"%>
<%@page import="com.velos.eres.service.util.LC"%>
<%@page import="com.velos.eres.service.util.MC"%>
<%@page import="com.velos.eres.service.util.ES"%>
<%@page import="com.velos.eres.service.util.SVC"%>
<%@page import="com.velos.eres.service.util.CFG"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB, com.velos.eres.service.util.EJBUtil" %>
<jsp:useBean id="addressUserB" scope="page" class="com.velos.eres.web.address.AddressJB" />
<jsp:useBean id="tokenJB" scope="request" class="com.velos.eres.web.userSession.UserSessionTokenJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>
<html>
<head>
<style>
#serverpagination_user table {
    width: 98%;
}
@font-face {
    font-family: 'open_sans_lightregular';
    src: url('font/opensans-light-webfont.woff2') format('woff2'),
         url('font/opensans-light-webfont.woff') format('woff');
    font-weight: normal;
    font-style: normal;

}
/*
USER Details 
*/
.user-details-section {
   margin-top: 44px;
    margin-left: 30px;
    margin-right: 5px;
}
.user-details-left-section {
    width:24%;
    float: left;
}
.user-details-section:after {
    content: "";
    display: block;
    clear: both;
}
.user-details-left-section li {
    display: inline-block;
    width: 100%;
}
.user-details-left-section li a {
    display: block;
    padding: 11px 17px;
    font-size: 16px;
    font-family: 'Open Sans', sans-serif;
    font-weight: 100;
    background: #f00;
    margin-bottom: 12px;
}
li.active-user a,li.deactivated-user a {
    background: #39b54a;
    color: #fff;
}
li.active-user a i,li.active-groups a i,li.deactivated-user a i {
    margin-right: 11px;
}
li.active-groups a {
    background: #106cb7;
    color: #fff;
}
li.deactivated-user a {
    background: #6b6b6b;
}
DIV.tabDefTopN {
    visibility: visible;
    position: absolute;
    overflow: auto;
    left: 4px;
}
.search-users {
    padding: 10px;
    border: 1px solid #ccc;
    box-shadow: 2px 0px 11px 2px #ccc;
}
.search-users h2 {
    margin-top: 0px;
    margin-bottom: 0px;
    font-weight: 100;
    color: #106cb7;
    font-size: 16px;
}
.inner-tab-menu li {
    padding: 5px 4px;
    width: 29%;
    margin-top: 10px;
    float: left;
    border-right: 1px solid #ccc;
    border-top: 1px solid #ccc;
    border-bottom: 1px solid #ccc;
}
.inner-tab-content {
    clear: both;
}
.search-users {
    clear: both;
}
.inner-tab-menu li:first-child {
    border-left: 1px solid #ccc;
}
DIV.tabDefTopN:not(.custom-not-tabDefTopN) {
    visibility: visible;
    overflow: inherit !important;
    top: 60px;
    width: inherit;
    left: inherit;
    position: fixed !important;
    width: 100%;
    clear: both;
}
.user-details-Menuleft {
    float: left;
}
.user-details-Menuright {
    float: right;
}
.custom-user-data {
    position: relative !important;
}
.custom-not-tabDefBotN {
    position: static !important;
    width: inherit !important;
}
.inner-tab-content input {
    width: 97%;
    border: 1px solid #ccc;
    margin-top: 6px;
    margin-bottom: 5px;
}
.inner-tab-content button {
    float: right;
}
.inner-tab-content::after {
    content: "";
    display: block;
    clear: both;
}
.custom-right-top li {
    display: inline-block;
}
.custom-right-top ul {
    float: right;
}
.custom-right-top li {
    margin-left: 10px;
}
.custom-not-tabDefBotN-clear {
clear: both;
}
.custom-not-tabDefTopN {
    overflow: hidden !important;
    position: static !important;
}
.user-details-Menuright li a {
    font-size: 12px;
}
.user-details-Menuright li i {
    float: left;
}
.user-details-Menuright li a:hover {
    color: #696969;
}
.custom-not-border {
    border: 0px !important;
}
.custom-not-tabDefTopN .tabBorder {
    position: absolute;
    left: 0px;
    right: -36px;
    width: 97%;
}
.user-details-right-section > form {
    margin-top: -28px;
}

.user-details-Menuright li i {
    float: left;
    margin-top: 5px;
    margin-right: 6px;
}
#serverpagination_user .yui-dt-data td {
    border-bottom: 1px solid #ccc !important;
    border-left: 0px !important;
    border-right: 0px !important;
}
.custom-table-width{width: 100% !important;}
.user-details-right-section {
    float: left;
    width: 73%;
    padding-left: 15px;
}
.user-details-Menuright li {
    display: inline-block;
    margin-top: 7px;
    margin-right: 17px;
}
.user-details-Menuright li a i {
    float: left;
    margin-top: 2px;
}
.custom-right-top li a:hover {
    background: transparent !important;
    color: #3B9EC6;
}
.custom-right-top li a {
    text-transform: uppercase;
}
.custom-right-top li a i {
    margin-right: 5px;
}
.user_tab_active {
    background: #3B9EC6;
    color: #fffefe;
    border: 1px solid #3B9EC6;
}
.user-details-left-section li a {
    font-size: 16px !important;
    font-weight: normal !important;
}
.user-details-Menuright li a:hover,.custom-right-top li a:hover {
    color: #737171  !important;
}
.custom-right-top li a {
    text-transform: uppercase !important;
}
li.active-user a, li.active-groups a,li.deactivated-user a {
    background: #D3D3D3;
    color: #555151;
}
.custom-span-space>span {
    text-align: initial !important;
    display: block;
    padding-top: 13px !important;
    clear: both;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%

 HttpSession tSession = request.getSession(true);
 int pageRight = 0;
 if (sessionmaint.isValidSession(tSession))

{
	 String viewList=request.getParameter("cmbViewList") ;
	 if(viewList==null){viewList= "AA"; }
	 String  searchUsr=request.getParameter("searchUsr")==null?"":request.getParameter("searchUsr");
	 String user_tab_nonsys="";
	 String user_tab_deactivate="";
	 String  user_tab_active="";
    
	 /***adding class based on the system users*****/
	if(viewList.equalsIgnoreCase("NS")){
	 		user_tab_nonsys="user_tab_active";
	}else if(viewList.equalsIgnoreCase("BD")){
	 		user_tab_deactivate="user_tab_active";
	}else if(viewList.equalsIgnoreCase("AA")){
	 	user_tab_active="user_tab_active";
	 }
	 /*****End of adding classed******/
String src="";
src= request.getParameter("srcmenu");

String uName = (String) tSession.getValue("userName");
String accId = (String) tSession.getValue("accountId");

//get the default group of the login user. If the default group is Admin then show reset password and reset user session for users
String logUsr = (String) tSession.getValue("userId");
GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MUSERS"));
String groupNme = request.getParameter("groupNme")==null?"":request.getParameter("groupNme");
UserDao userDao= new UserDao();
		 
 int loginUser = EJBUtil.stringToNum(logUsr);
	userB.setUserId(loginUser);
	userB.getUserDetails();
	String defGroup = userB.getUserGrpDefault();
	groupB.setGroupId(EJBUtil.stringToNum(defGroup));
	groupB.getGroupDetails();
	String groupName = ((groupB.getGroupName())==null)?"-":(groupB.getGroupName());
		
		 
	
if(viewList.equals("AA")){%>
	<%--<title>Manage Account >> Active Account Users</title>*****--%>
	<title><%=MC.M_MngAcc_ActAccUsr%></title>
<%}else if(viewList.equals("NS")){%>
	<%--<title>Manage Account >> Non System Users</title>*****--%>
	<title><%=MC.M_MngAcc_NonSysUsr%></title>
<%}else if(viewList.equals("BD")){%>
	
	<%--<title>Manage Account >> Blocked/Deactivated Users</title>*****--%>
	<title><%=MC.M_MngAcc_BlkOrDeactUsr%></title>
<%}%>


<SCRIPT language="Javascript1.2">
	function openReport(){
// INF-20084 Datepicker-- AGodara
		window.open("rep_activityreport.jsp",'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=550,height=350');
	}
</SCRIPT>

<SCRIPT language="javascript">

var screenWidth = screen.width;
var screenHeight = screen.height;


function confirmBox(name,pgRight,viewList) {
	if (f_check_perm(pgRight,'E') == true) {
		name = decodeString(name);
		if(viewList=="AA")
		{
		  var paramArray = [name];
		  if (confirm(getLocalizedMessageString("L_Del_FrmGrp",paramArray))) {/*if (confirm("Delete " + name + " from Group?")) {*****/
			
				return true;
			}
			else{
				return false;
				}
		}
		else if(viewList=="NS")
			{
			 var paramArray = [name];
			 if (confirm(getLocalizedMessageString("L_Del",paramArray))) {/*if (confirm("Delete " + name)) {*****/
			return true;
			}
			else	
				{	
				 return false;
				}
			}
	}else {
		return false;
	}
}
//JM: 20September2006
function fAddMultUserToStudies(pgRight){

	  windowName =window.open('addUsersToMultipleStudies.jsp?pageRight='+pgRight, 'Information', 'toolbar=no,scrollbar=yes,resizable=yes,menubar=no,status=yes,width=820,height=450, top=100, left=100');
	  windowName.focus();

}

function fopen(link,pgRight) {
	
	if (f_check_perm(pgRight,'E') == true) {
		windowName = window.open(link,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=600,height=280')
		windowName.focus();
	} else {
		return false;
	}
}

function fEditRight(pgRight) {
	if (f_check_perm(pgRight,'E') == true) {
		return true;
	} else {
		return false;
	}
}

//Added by Gopu for July-August'06 Enhancement (#U2) - Admin Settings(Default account login user is Admin then open the admin settings popup page)

function openadminsettings(pageRight) {	
	  
	  windowName=window.open("adminSettings.jsp?pageRight="+pageRight,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=780,height=429, top = 100 , left = 100");
	  windowName.focus();
}

userLink=function(elCell, oRecord, oColumn, oData)
{

var htmlStr="";
var userId=oRecord.getData("PK_USER");
var userNameMoreId=oRecord.getData("USER_NAME_MOREID");
var srcmenu='<%=StringUtil.htmlEncodeXss(src)%>';
var userType='<%=StringUtil.htmlEncodeXss(viewList)%>';

if(userType=="NS")
{
htmlStr="<A href=\"nonSystemUserDetails.jsp?mode=M&srcmenu="+srcmenu+"&userId="+userId+" \"onmouseover=\"return overlib('"+userNameMoreId.replace(/'/g, "\\'")+"',ABOVE);\" onmouseout=\"return nd();\")'>"+oData+"</A> ";
}else{
htmlStr="<A href=\"userdetails.jsp?mode=M&srcmenu="+srcmenu+"&userId="+userId+"&userType="+userType+" \"onmouseover=\"return overlib('"+userNameMoreId.replace(/'/g, "\\'")+"',ABOVE);\" onmouseout=\"return nd();\")'>"+oData+"</A> ";
}

 elCell.innerHTML=htmlStr;
}

resetPass=function(elCell, oRecord, oColumn, oData)
{

var htmlStr="";
var userId=oRecord.getData("PK_USER");
var pageRight='<%=pageRight%>';
var link="";
var groupName='<%=groupName%>';
var userType='<%=StringUtil.htmlEncodeXss(viewList)%>';
if ((groupName=='Admin') && (userType=='AA' || userType==(""))) {
link="resetpass.jsp?userId=" + userId + "&fromPage=User";

htmlStr="<A HREF=\"javascript:void(0);\" onclick=\"fopen('"+link+"','"+pageRight+"')\"><%=LC.L_Reset%></A> ";
}

 elCell.innerHTML=htmlStr;
}

resetUserSession=function(elCell, oRecord, oColumn, oData)
{

var htmlStr="";
var userId=oRecord.getData("PK_USER");
var usrLoggedInFlag=oRecord.getData("USR_LOGGEDINFLAG");
var logUser='<%=logUsr%>';
var pageRight='<%=pageRight%>';
var userType='<%=StringUtil.htmlEncodeXss(viewList)%>';
var groupName='<%=groupName%>';
var link="";
if ((groupName=='Admin') && (userType=='AA' || userType==(""))) {
if ((usrLoggedInFlag=='1') && (logUser!=(userId)) ) {
link="resetusersession.jsp?userId=" + userId;
htmlStr="<A HREF=\"javascript:void(0);\" onclick=\"fopen('"+link+"','"+pageRight+"')\"><%=LC.L_Reset%></A> ";
}
}

 elCell.innerHTML=htmlStr;
}

delLink=function(elCell, oRecord, oColumn, oData)
{

var htmlStr="";

var userName=oRecord.getData("USER_NAME");
var usrGrpId=oRecord.getData("PK_USRGRP");
var userId=oRecord.getData("PK_USER");
var srcmenu='<%=StringUtil.htmlEncodeXss(src)%>';
var userType='<%=StringUtil.htmlEncodeXss(viewList)%>';
var pageRight='<%=pageRight%>';
if(userType=='AA'|| userType==("")){
htmlStr="<A title=\"<%=LC.L_Delete%><%--Delete*****--%>\" href=\"delete_usr_from_grp.jsp?srcmenu="+srcmenu+ "&usrGrpId="+usrGrpId+ "\" onclick=\"return confirmBox('"+encodeString(userName)+"','"+pageRight+"','"+userType+"');\">"+
        "<img src=\"./images/delete.gif\" border=\"0\" align=\"left\"/></A>";
}
else if(userType=='NS'){
htmlStr="<A title=\"<%=LC.L_Delete%><%--Delete*****--%>\" href=\"deleteUser.jsp?srcmenu="+srcmenu+ "&userId="+userId+ "\" onclick=\"return confirmBox('"+encodeString(userName)+"','"+pageRight+"','"+userType+"');\">"+
	    "<img src=\"./images/delete.gif\" border=\"0\" align=\"left\"/></A>";
}	

 elCell.innerHTML=htmlStr;
}

usrLink=function(elCell, oRecord, oColumn, oData)
{

var htmlStr="";
var userId=oRecord.getData("PK_USER");
var srcmenu='<%=StringUtil.htmlEncodeXss(src)%>';
var userType='<%=StringUtil.htmlEncodeXss(viewList)%>';
var pageRight='<%=pageRight%>';

if(userType=='NS'){
htmlStr="<A href=\"userdetails.jsp?mode=M&srcmenu="+srcmenu+"&userId="+userId+"&userType="+userType+"\")'><%=MC.M_Convert_ToActUser%></A> ";
}else if(userType=='BD'){
htmlStr="<A href=\"activateuser.jsp?srcmenu="+srcmenu+"&userId="+userId+"&userType="+userType+"&delMode=null\" onclick=\"return fEditRight('"+pageRight+"')\")'><%=LC.L_Reactivate_User%></A> ";
}
 elCell.innerHTML=htmlStr;
}

</SCRIPT>
 
</head>





<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=StringUtil.htmlEncodeXss(src)%>"/>
</jsp:include>   
<jsp:include page="ui-include.jsp" flush="true"/>
<body class="yui-skin-sam" >

<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.VelosResourceBundle"%>
<%@ page import="com.velos.eres.service.util.FilterUtil"%>
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache"%>


  <%




    if (request.getParameter("refreshCache") != null 
	        && logUsr.equals(request.getParameter("usrId"))) {
        ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
	    objCache.populateObjectSettings();
	    FilterUtil.fillParamHashFromLkpFilterDao();
	    FilterUtil.fillViewHashFromLkpViewDao();
	    VelosResourceBundle.reloadProperties();
	    LC.reload();
	    MC.reload();
	    ES.reload();
	    SVC.reload();
	    CFG.reload();
	    CodeCache.getInstance().flush(); 
	}
	
	
	

%>

<DIV class="tabDefTopN custom-not-tabDefTopN" id = "div1"> 
<div class="user-details-Menuleft">
	<jsp:include page="accounttabs.jsp" flush="true"> 
	<jsp:param name="selectedTab" value="3"/>
	</jsp:include>
</div>
<div class="user-details-Menuright">
	<ul>
	<%if (groupName.equalsIgnoreCase("admin")) {%>
    <li> <A href=# onClick="openadminsettings(<%=pageRight%>);"><i class="fa fa-cog" aria-hidden="true"></i> <%=LC.L_Admin_Settings%></A> </li>
    <li><a  href="accountbrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=3&refreshCache=1&usrId=<%=StringUtil.htmlEncodeXss(logUsr)%>"><i class="fa fa-refresh" aria-hidden="true"></i> <%=LC.L_RefMenusOrTabs%></a></li>
	 <%}%>
    <li><A href =# onClick = "fAddMultUserToStudies(<%=pageRight%>);"><i class="fa fa-plus-circle" aria-hidden="true"></i> <%=MC.M_AddUser_SMultiStd%></A> </li>
	
	</ul>
</div>
</DIV>			

<Script>
var paginate_user;
$E.addListener(window, "load", function() {
	$('CSRFToken').value="<%=StringUtil.trueValue(tokenJB.createTokenForUser((String)tSession.getAttribute("userId")==null?0:StringUtil.stringToNum((String)tSession.getAttribute("userId"))))%>";
 paginate_user=new VELOS.Paginator('usrSearch',{
			sortDir:"asc",
			sortKey:"SITE_NAME",
			defaultParam:"userId,accountId,grpId",
			filterParam:"userId,siteId,userType,searchUsr,cmbViewList,CSRFToken,groupName",
			dataTable:'serverpagination_user',
			navigation: true,
			rowSelection:[25,50,100,250]
			});
	 paginate_user.render();
			
	});
				
			
</script>

<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="tabDefBotN custom-not-border custom-not-button-color" id="div1" style="overflow: visible; height: 82%; width:98%;">')
	else
		document.write('<DIV class="tabDefBotN custom-not-border custom-not-button-color" id="div1" style="overflow: visible; height: 87%; width:98%;">')
</SCRIPT>

<%

   if (pageRight > 0 )

	 {
	   int countActiveUsr=0;	
	   int activeGrp=0;
	   int deactivatedUsr=0;
	   countActiveUsr= userDao.getUsersCounts(StringUtil.stringToInteger(accId) ,"A");
	   activeGrp=userDao.getUsersCounts(StringUtil.stringToInteger(accId) ,"");
	   deactivatedUsr=userDao.getUsersCounts(StringUtil.stringToInteger(accId) ,"'D','B'");
%>
<div class="user-details-section">
<Form name="accountBrowser" method="post"  action="accountbrowser.jsp"  onsubmit = "">
	  <input type="hidden" name=srcmenu value=<%=StringUtil.htmlEncodeXss(src)%>>
	  <input type="hidden" id="CSRFToken" name="CSRFToken">
	  <input type="hidden" id="groupName" name="groupName" value=<%=groupName%>>
	  <input type="hidden" id="cmbViewList" name="cmbViewList" value="<%=viewList%>">
	  
	 
	  
<!-- LEFT SECTION -->
  <div class="user-details-left-section">
    <ul>
      <li class="active-user"><a href="#" style="color: #3B9EC6;"><i class="fa fa-user" aria-hidden="true"></i><span><%=LC.L_Active +" "+ LC.L_Users%>:<%=countActiveUsr %></span></a></li>
      <li class="active-groups"><a href="#" style="color: #3B9EC6;"><i class="fa fa-users" aria-hidden="true"></i><span><%=LC.L_Active +" "+ LC.L_Groups%>:<%=activeGrp %></span></a></li>
      <li class="deactivated-user"><a href="#" style="color: #3B9EC6;"><i class="fa fa-ban" aria-hidden="true"></i><span><%=LC.L_Deactivated +" "+ LC.L_Users%>:<%=deactivatedUsr %></span></a></li>
   
   </ul>
    <!-- Search Users Start -->
  <div class="search-users">
      <h2>Search Users</h2>
      <!-- Inner Tab Start -->
  <div class="inner-tab-section">
         <!-- Inner Tab Menu -->
  <div class="inner-tab-menu">
       <ul>
      <li class="user-tab-1  <%=user_tab_active%>" data-user-tab="active"><%=LC.L_Active%></li>
      <li class="user-tab-2  <%=user_tab_nonsys%>" data-user-tab="non-system"><%=LC.L_NonSystem %></li>
      <%if (groupName.equalsIgnoreCase("admin")){ %>
      <li class="user-tab-3  <%=user_tab_deactivate%>" data-user-tab="deactivated"><%=LC.L_Deactivated%></li>
      <%} %>
     </ul>
     </div>
      <!-- Inner Tab Menu End -->
      <!-- Inner Tab Content -->
  <div class="inner-tab-content custom-span-space">
      <span><%=LC.L_SearchByNameandGroup%></span>
      <input type="text" id="searchUsr" name="searchUsr" value="<%=searchUsr%>" size="25" >
      <button type="submit"><%=LC.L_Search%></button>
           
  </div>
      <!-- Inner Tab Content End -->
  </div>
      <!-- Inner Tab End  -->
  </div>
    <!-- Search Users End -->
  </div>
    <!-- LEFT SECTION END  -->
    <!-- RIGHT SECTION START  -->
  <div class="user-details-right-section">
  <div class="custom-right-top">

<ul>
<li>
<a href=# onclick="openReport()"><i class="fa fa-bar-chart" aria-hidden="true"></i> <%--ACTIVITY REPORT*****--%><%=LC.L_Activity_Rpt_Upper%></a>
</li>
<li>
<a href="emailuser.jsp?mode=N&srcmenu=<%=StringUtil.htmlEncodeXss(src)%>&fromPage=accountbrowser"><%--EMAIL USERS*****--%><i class="fa fa-paper-plane-o" aria-hidden="true"></i><%=LC.L_Email_Users_Upper%></a>
</li>
<%if(viewList.equals("") || viewList.equals("AA")){%>
<li>
<A href="userdetails.jsp?mode=N&srcmenu=<%=StringUtil.htmlEncodeXss(src)%>&userType=<%=StringUtil.htmlEncodeXss(viewList)%>"><i class="fa fa-plus-circle" aria-hidden="true"></i><%--ADD A NEW USER*****--%><%=MC.M_AddA_NewUser_Upper%></A>
</li>
<li>
<A href="group.jsp?mode=N&srcmenu=<%=StringUtil.htmlEncodeXss(src)%>&fromPage=accountbrowser"><i class="fa fa-plus-circle" aria-hidden="true"></i><%--ADD A NEW GROUP*****--%><%=MC.M_AddNew_Grp_Upper%></A>
</li>
<%}
if(viewList.equals("NS")){%>
<li>
<A href="nonSystemUserDetails.jsp?mode=N&srcmenu=<%=StringUtil.htmlEncodeXss(src)%>&fromPage=accountbrowser"><i class="fa fa-plus-circle" aria-hidden="true"></i><%--ADD A NEW NON-SYSTEM USER*****--%><%=MC.M_AddNew_NonSysUsr_Upper%></A>
<%}%>
</li>
</ul>
</div>
		
<!-- Bug#15425 Fix for 1360*768 resolution- Tarun Kumar -->	
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<div class="tabDefBotN custom-not-tabDefBotN custom-not-tabDefBotN-clear custom-not-border custom-table-width" id="tabbody" style="height: 72%;  margin-left: 1px; " >')
	else
		document.write('<div class="tabDefBotN custom-not-tabDefBotN custom-not-tabDefBotN-clear custom-not-border custom-table-width" id="tabbody" style="height: 82%;  margin-left: 1px; " >')

    var MSIEOffset = navigator.userAgent.indexOf("MSIE ");
if (MSIEOffset == -1) {
document.getElementById("tabbody").style.marginTop= 33;
document.getElementById("tabbody").style.width= 1229;
document.getElementById("tabhead").style.width= 1231;
}
else
{  
document.getElementById("tabbody").style.marginTop= 19;
document.getElementById("tabbody").style.width= 1230;
document.getElementById("tabhead").style.width= 1230;
}
</script>
	   
<div id="serverpagination_user" class="custom-table-width"> </div>

 <script>
$j(document).ready(function(){

  $j('.inner-tab-menu li').click(function(){
		
	     $j('li').removeClass('user_tab_active');
	     $j(this).addClass('user_tab_active'); 
	     
	     var getUserTabValue = $j(this).attr('data-user-tab');
	     if(getUserTabValue=='active')
	     {
	    	 $j("#cmbViewList").val('AA');
	       }
	     else if(getUserTabValue=='non-system'){
	    	 $j("#cmbViewList").val('NS');
	     }else if(getUserTabValue=='deactivated'){
	    	 $j("#cmbViewList").val('BD');
	     }
	  	var getTabClass =  $j(this).attr('class').split(" ")[0];
		
  });

 });
</script>
	
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
</div>

<BR>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  </form>
</div>

<div class ="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>
