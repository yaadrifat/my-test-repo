<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList,java.util.HashMap,org.json.*"%>
<%@page import="com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
<%@page import="com.velos.esch.business.common.EventdefDao"%>
<%@page import="com.velos.esch.business.common.EventAssocDao"%>
<%@page import="com.velos.esch.business.common.SchCodeDao"%>


<%@page import="com.velos.esch.web.protvisit.ProtVisitEventGridJB"%>
<jsp:useBean id="sessionmaint" scope="session"
	class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="eventdefB" scope="request"
	class="com.velos.esch.web.eventdef.EventdefJB" />
<jsp:useBean id="eventassocB" scope="request"
	class="com.velos.esch.web.eventassoc.EventAssocJB" />
<jsp:useBean id="protVisitB" scope="request"
	class="com.velos.esch.web.protvisit.ProtVisitJB" />
<jsp:useBean id="visitdao" scope="request"
	class="com.velos.esch.business.common.ProtVisitDao" />

<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	// response.setContentType("text/html");
	
	
	 String  eventName  = request.getParameter("eventName");  
	 eventName = eventName.trim();
		
	

	HttpSession tSession = request.getSession(true);
	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN,
				"SESSION_TIME_OUT");
		JSONObject jsObj = new JSONObject();
		jsObj.put("error", new Integer(-1));
		jsObj
				.put("errorMsg", MC.M_UsrNot_LoggedIn/*User is not logged in.*****/);
		out.println(jsObj.toString());
		return;
	}

	String protocolId = request.getParameter("protocolId");
	if (protocolId == "" || protocolId == null
			|| protocolId.equals("null") || protocolId.equals("")) {
		// A valid protocol ID is required; print an error and exit
		JSONObject jsObj = new JSONObject();
		jsObj.put("error", new Integer(-2));
		jsObj.put("errorMsg", MC.M_PcolId_Invalid/*Protocol ID is invalid.*****/);
		out.println(jsObj.toString());
		return;
	}

	// Define page-wide variables here
	ProtVisitEventGridJB protEventVisitJb = new ProtVisitEventGridJB();
	String calledFrom = request.getParameter("calledFrom");
	String calassoc = request.getParameter("calassoc");
	String pgRight = request.getParameter("doGet");
	String sortOrder = (String) request.getParameter("sortOrder");
	String viewAllFlag=request.getParameter("viewAllFlag");
	int initEventSize = StringUtil.stringToNum(request
			.getParameter("initEventSize"));
	int limitEventSize = StringUtil.stringToNum(request
			.getParameter("limitEventSize"));
	int initVisitSize = StringUtil.stringToNum(request
			.getParameter("initVisitSize"));
	int limitVisitSize = StringUtil.stringToNum(request
			.getParameter("limitVisitSize"));
	String eUrl = request.getParameter("eUrl");
	eUrl = StringUtil.replaceAll(eUrl,"@","&");
	String jsString = null;
	try {
		if("1".equals(viewAllFlag)){
			jsString = protEventVisitJb.fetchProtJSON(protocolId, calledFrom, calassoc, pgRight, sortOrder,eventName,eUrl);
		}else{
			jsString = protEventVisitJb.fetchProtJSON(protocolId, calledFrom, calassoc, pgRight, sortOrder,initEventSize,limitEventSize,initVisitSize,limitVisitSize,eventName);
		}
	} catch(Exception e) {
	    Rlog.fatal("fetchProt", "Error while calling fetchProtJSON "+e);
	}
	out.println(StringUtil.trueValue(jsString));
	// <html><head></head><body></body></html>
%>

<%!// Define Java functions here
	private String createCheckbox(String eventId, String visitId,
			boolean checked) {
		StringBuffer sb = new StringBuffer();
		sb.append("<input type='checkbox' ");
		sb.append(" name='e").append(eventId).append("v").append(visitId)
				.append("' ");
		sb.append(" id='e").append(eventId).append("v").append(visitId).append(
				"' ");
		if (checked) {
			sb.append(" checked ");
		}
		sb.append(" >");
		return sb.toString();
	}%>
