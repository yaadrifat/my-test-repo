<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page language="java" import="com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings"%>
<%@ page language="java" import="com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@ page language="java" import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*" %>

<html>
<head>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
String titleStr = "";
if (sessionmaint.isValidSession(request.getSession(true)))
{
    String accId = (String) request.getSession(true).getValue("accountId");
    
    ArrayList tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "top_menu");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("irb_menu".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
                titleStr += settings.getObjDispTxt();
            }
            break;
        } 
    }
    tempList = null;

    tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "irb_menu");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("meeting_menu".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
            	Object[] arguments = {titleStr,settings.getObjDispTxt()};
                titleStr = VelosResourceBundle.getLabelString("L_Gtgt",arguments);/*titleStr += " >> " + settings.getObjDispTxt();*****/
            }
            break;
        } 
    }
    tempList = null;
    
}
%>
<title><%=titleStr %></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<% 
String src= request.getParameter("srcmenu");
HttpSession tSession = request.getSession(true); 
String accountId = "";
String studyId = "";
int meetingGrpRight  = 0;

if (sessionmaint.isValidSession(tSession))
{
  accountId = (String) tSession.getValue("accountId");
  studyId = (String) tSession.getValue("studyId");
  if(accountId == null || accountId == "") {
  %>
    <jsp:include page="timeout.html" flush="true"/>
  <%
    return;
  }

	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	meetingGrpRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_MM"));

  
}
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<body>

<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<DIV class="BrowserBotN BrowserBotN_RC_2" id = "div1">
<%
  if (sessionmaint.isValidSession(tSession))
  {
    String usrId = (String) tSession.getValue("userId");
    String uName = (String) tSession.getValue("userName");
    String defUserGroup = (String) tSession.getAttribute("defUserGroup");
    
    if (meetingGrpRight >= 4)
    {	
  %>
	
    <jsp:include page="meetingBrowser.jsp" flush="true">
      <jsp:param name="tabsubtype" value="irb_meeting"/>
      <jsp:param name="tabtype" value="irbMeeting"/>
	  <jsp:param name="meetingGrpRight" value="<%=meetingGrpRight%>"/> 
    </jsp:include>
  <%
  	}
  	else
  	{
  		%>
  			<jsp:include page="accessdenied.jsp" flush="true"/>

  			<%
  	}
  }//end of if body for session
  else
  {
  %>
    <jsp:include page="timeout.html" flush="true"/>
  <%
  }
  %>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
</body>

</html>

