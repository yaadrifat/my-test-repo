<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<%
	String fromPage = request.getParameter("fromPage");

	if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary") || fromPage.equals("patientschedule") ) {		
	%>
		<title> <%=MC.M_EvtLib_CostEvtPg%><%--Event Library >> Event Cost >> Event Cost Page*****--%> </title>
	<%
	} else {%>
		<title><%=LC.L_Evt_CostPage%><%--Event Cost Page*****--%> </title>	
	<%
	}
%>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@ page import="com.velos.eres.service.util.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<%  
if ((fromPage.equals("selectEvent")) || (fromPage.equals("fetchProt")))
{ %>

 <SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<!-- YK 05Aug11: Removed for Bug #6728 already using Whichcss_skin.js in panel.jsp -->
	 
 <% } //end for if ((request.getParameter("fromPage")).equals("selectEvent"))

	%>

<SCRIPT Language="javascript">
 function  validate(formobj){
     if (!(validate_col('Cost Numerical',formobj.costNum))) return false
     //KV:BugNo:5120
    //if (!(validate_col('Cost Fractional',formobj.costFrac))) return false
	 formobj.cost.value = formobj.costNum.value ;

     if (!(validate_col('Description',formobj.costDesc))) return false
	 
	 if (!(isDecimal(formobj.cost.value))){
	 		alert("<%=LC.L_Invalid_Cost%>");/*alert("Invalid Cost");*****/
			formobj.costNum.focus();
		 	 return false;
		 }
	if (!(validate_col('e-Signature',formobj.eSign))) return false
			 
	<%-- if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   } --%>
		 

}


</SCRIPT>


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.*"%>
<jsp:useBean id="eventcostB" scope="request" class="com.velos.esch.web.eventcost.EventcostJB"/>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>


<% String src;

src= request.getParameter("srcmenu");

String eventName = request.getParameter("eventName");

%>

<%  if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){
    %>
    <jsp:include page="include.jsp" flush="true"/>
    <%    
    } else {%>
    <jsp:include page="panel.jsp" flush="true"> 
    <jsp:param name="src" value="<%=src%>"/>
    </jsp:include>   
<%  }  %>

<body>
<%  
    if ((fromPage.equals("selectEvent")) || (fromPage.equals("fetchProt"))) { %>
 <DIV class="popDefault" id="div1"> <br>
<%  } else {  %>
 <DIV class="formDefault" id="div1">  
<%  }
    String duration = request.getParameter("duration");   
	String protocolId = request.getParameter("protocolId");   
	String calledFrom = request.getParameter("calledFrom");   
	String eventId = request.getParameter("eventId");
	String costmode = request.getParameter("costmode");
	String eventmode = request.getParameter("eventmode");
	String calStatus = request.getParameter("calStatus");		
	String mode = request.getParameter("mode");
	String displayDur=request.getParameter("displayDur");
	String displayType=request.getParameter("displayType");
	String calassoc = request.getParameter("calassoc");	   
	
	String cost = "";
	String costNumeral = "0";
	String costFractional = "0";
	int decPos = 0;
	String costDesc = "";
	String costId = "";
	String costCur = "";
	String dCur = "";
	String dCostDesc= "";
	SchCodeDao cd = new SchCodeDao();
	SchCodeDao cdDesc = new SchCodeDao();
		
	HttpSession tSession = request.getSession(true); 

	if (sessionmaint.isValidSession(tSession))
	{
	  String uName = (String) tSession.getValue("userName");
	  String sAcc = (String) tSession.getValue("accountId");
	  String userId=(String)tSession.getAttribute("userId");

String calName = "";

if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary") || fromPage.equals("patientschedule") )
{
%>
	<!--P class="sectionHeadings"> Event Library >> Event Cost  </P-->
<%
}else{

	calName = (String) tSession.getValue("protocolname");
%>
	<P class="sectionHeadings"> <%Object[] arguments = {calName}; %><%=VelosResourceBundle.getMessageString("M_PcolCal_EvtCost",arguments)%><%--Protocol Calendar [ <%=calName%> ] >> Event Cost*****--%> </P>
<%
}%>

<jsp:include page="eventtabs.jsp" flush="true">
<jsp:param name="duration" value="<%=duration%>"/>
<jsp:param name="protocolId" value="<%=protocolId%>"/>
<jsp:param name="calledFrom" value="<%=calledFrom%>"/>
<jsp:param name="fromPage" value="<%=fromPage%>"/>
<jsp:param name="mode" value="<%=mode%>"/>
<jsp:param name="displayDur" value="<%=displayDur%>"/>
<jsp:param name="displayType" value="<%=displayType%>"/>
<jsp:param name="eventId" value="<%=eventId%>"/>
<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="eventName" value="<%=eventName%>"/>
</jsp:include>
<P class = "userName"><%=uName%></p>

<%
if(costmode.equals("M")) {
   costId = request.getParameter("costId");
   eventcostB.setEventcostId(EJBUtil.stringToNum(costId));
   eventcostB.getEventcostDetails();
   costDesc = eventcostB.getEventcostDescId();
   cost = eventcostB.getEventcostValue();
   costCur = eventcostB.getCurrencyId();
   decPos = cost.indexOf(".");
   if(decPos == -1) decPos = cost.length();
   costNumeral = cost.substring(0,decPos);
   if(decPos != cost.length()) decPos++;
   costFractional = cost.substring(decPos);
   
 

    cd.getCodeValues("currency", EJBUtil.stringToNum(sAcc)); 
	dCur =  cd.toPullDown("cur",EJBUtil.stringToNum(costCur));
	
	//@Ankit: 14-Jul-2011, #FIN-CostType-1
	//cdDesc.getCodeValues("cost_desc",EJBUtil.stringToNum(sAcc)); 
	//dCostDesc =cdDesc.toPullDown("costDesc",EJBUtil.stringToNum(costDesc)); 
   if (calledFrom.equals("P") || calledFrom.equals("L")) {					

	   cdDesc.getCodeValues("cost_desc", EJBUtil.stringToNum(sAcc)); 
   }

   if (calledFrom.equals("S")) { //Protocol called from Study
	    
	    String studyId = "";
	    String roleCodePk="";

	    eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
		eventassocB.getEventAssocDetails();
		String eventType = eventassocB.getEvent_type();
		studyId =(eventType!=null && eventType.equalsIgnoreCase("P")) ? eventassocB.getChain_id():"";

		if (! StringUtil.isEmpty(studyId) && StringUtil.stringToNum(studyId) > 0){
			roleCodePk = eventassocB.getStudyUserRole(studyId,userId);
		} 
		else{
		  roleCodePk ="";
		}
		cdDesc.getCodeValuesForStudyRole("cost_desc",roleCodePk);	
   }
   dCostDesc =cdDesc.toPullDown("costDesc",EJBUtil.stringToNum(costDesc));
	
   
%>
<form name="addcost" id="addevtcost" METHOD=POST action="costsave.jsp?costId=<%=costId%>&costmode=<%=costmode%>&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&eventmode=<%=eventmode%>&calStatus=<%=calStatus%>&fromPage=<%=fromPage%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>" onsubmit="ripLocaleFromAll(); if (validate(document.addcost)== false) { setValidateFlag('false'); applyLocaleToAll(); return false; } else { setValidateFlag('true'); return true;}">
<%
} else {

%>

<form name="addcost" id="addevtcost" METHOD=POST action="costsave.jsp?costmode=<%=costmode%>&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&eventmode=<%=eventmode%>&calStatus=<%=calStatus%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>" onsubmit="ripLocaleFromAll(); if (validate()== false) { setValidateFlag('false'); applyLocaleToAll(); return false; } else { setValidateFlag('true'); return true;}">

<%
}
%>
<table width="90%">
<tr>
<td>
<%=LC.L_Cost_Type%><%--Cost Type*****--%>: <FONT class="Mandatory">* </FONT>
</td>
<td>
<!--<INPUT NAME="costDesc" value="<%=costDesc%>" TYPE=TEXT SIZE=20> -->
<%=dCostDesc%> 
</td></tr>
<tr>
<td>
 <%=LC.L_Cost%><%--Cost*****--%>: <FONT class="Mandatory">* </FONT>
</td>
<td> <%=dCur%>
<INPUT NAME="cost" TYPE="hidden" value="<%=cost%>" SIZE=20>
<INPUT NAME="costNum" TYPE="text" value="<%=cost%>" SIZE=20 MAXLENGTH=10 align=right class="numberfield" data-unitsymbol="" data-formatas="currency">
</td></tr>
</table>
<jsp:include page="propagateEventUpdate.jsp" flush="true"> 
<jsp:param name="fromPage" value="<%=fromPage%>"/>
<jsp:param name="formName" value="addcost"/>
<jsp:param name="eventName" value="<%=eventName%>"/>	
</jsp:include>   

<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="addevtcost"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
<INPUT NAME="calassoc" TYPE="hidden" value="<%=calassoc%>" SIZE=20/>
</form>

<%

} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
 
}


if ((fromPage.equals("selectEvent")) || (fromPage.equals("fetchProt")))
{ } else {%>
</div>
<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>
<%}%>

  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</body>

</html>



