<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.*"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<head>
	<title>Delete Form Action</title>
</head>

<jsp:include page="popupJS.js" flush="true"/>


<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
	
<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

/* 	if(isNaN(formobj.eSign.value) == true) {
	alert("Incorrect e-Signature. Please enter again");
	formobj.eSign.focus();
	return false;
   } */
}
</SCRIPT>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY> 
<br>

<jsp:useBean id="formActionJB" scope="request"  class="com.velos.eres.web.formAction.FormActionJB"/>

<DIV class="popDefault" id="div1">
<% 

HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))	{
  
	String formActionId = request.getParameter("formActionId");//parameter
	    formActionId=(formActionId==null)?"":formActionId;
	String delMode=request.getParameter("delMode");
	if (delMode==null) {
		delMode="final";
%>
	<FORM name="deleteformAction" method="post" action="deleteformaction.jsp" onSubmit="return validate(document.deleteformAction)">
	<br>
	<P class="successfulmsg">Delete Form Action</P>
	<P class="defComments">Please enter e-Signature to proceed</P>	
	<TABLE width="100%" cellspacing="0" cellpadding="0" >
	<tr>
	   <td width="40%">
		e-Signature <FONT class="Mandatory">* </FONT> 
	   </td>
	   <td width="60%">
		<input type="password" name="eSign" maxlength="8" SIZE=15 autocomplete="off">
	   </td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
		 <button type="submit"><%=LC.L_Submit%></button>
		</td>
	</tr>
	</table>

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	
 	 <input type="hidden" name="formActionId" value="<%=formActionId%>">
	  </FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {

					int ret=0;
					formActionJB.setFormActionId(EJBUtil.stringToNum(formActionId));
					// Modified for INF-18183 ::: AGodara
					ret = formActionJB.removeFormAction(AuditUtils.createArgs(tSession,""));
					
%><br><BR><BR><BR><BR><%
	if(ret == 0) {
%>
		<p class = "successfulmsg" align = center> Form Action was deleted successfully.</p>
<%
		} 
		 else {
%>
		<p class = "successfulmsg" align = center> Form Action could not be deleted . </p>
<%
		}
	 %>
	<% if (ret >= 0)
		{%>
		  <script>
				window.opener.location.reload();
				setTimeout("self.close()",1000);
		  </script>	  				
		<%
		} // end of if status got deleted 
		  else
			 {
		 	%>
			 	<table width="550" border = "0">
		 			<tr>
						<td align="center">
							<button onClick="window.self.close();"><%=LC.L_Close%></button>	 
						</td>
		   			</tr>
	 			</table>	
			<%
		  }  
		
	} //end esign
 } //end of delMode
}//end of if body for session
else {
%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/> 
<%
}
%>
   
</DIV>

</BODY>
</HTML>
