<%@page import="java.util.Hashtable"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.aithent.audittrail.reports.AuditUtils"%>          
<%@page import="org.json.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.velos.esch.web.subCostItem.SubCostItemJB,com.velos.esch.web.subCostItemVisit.SubCostItemVisitJB"%>
<%@page import="com.velos.esch.service.util.EJBUtil,com.velos.eres.service.util.LC,com.velos.eres.service.util.Configuration"%>
<%@page import="com.velos.eres.service.util.StringUtil"%>
<%@page import="com.velos.esch.web.subCostItemVisit.SubCostItemVisitJB"%>
<%@page import="java.math.BigDecimal,com.velos.eres.service.util.MC"%>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="userBean" scope="session" class="com.velos.eres.business.user.impl.UserBean"/>
<jsp:useBean id ="userJB" scope="session" class="com.velos.eres.web.user.UserJB"/>

<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	HttpSession tSession = request.getSession(true);
	JSONObject jsObj = new JSONObject();
	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
		out.println(jsObj.toString());
		return;
	}
	
	String oldESign = (String) tSession.getAttribute("eSign");
	String eSign = request.getParameter("eSign");
	if (!oldESign.equals(eSign)) {
		jsObj.put("result", -4);
		if ("userpxd".equals(Configuration.eSignConf)) {
			jsObj.put("resultMsg", MC.M_EtrWrgPassword_Svg/*"You entered a wrong e-Password. Please try saving again. "*****/);
		}
		else
		{
		jsObj.put("resultMsg", MC.M_EtrWrgEsign_Svg/*"You entered a wrong e-signature. Please try saving again. "*****/);
		}
		out.println(jsObj.toString());
		return;
	}
	
	String ipAdd = (String) tSession.getAttribute("ipAdd");
    String userId = (String) tSession.getAttribute("userId");
	String accountId = (String) tSession.getAttribute("accountId");

	String mode=request.getParameter("mode");
	String duration=request.getParameter("duration");
	String calstatus=request.getParameter("calstatus");
	String calassoc=request.getParameter("calassoc");
	String[] ops= request.getParameterValues("ops");
 	String calledFrom = request.getParameter("calledfrom");
	String protId = request.getParameter("calProtocolId");
	String tableName = request.getParameter("tableName");
	String test1 = request.getParameter("test1");
	String[] scItems= request.getParameterValues("items");
	String[] purgeItems= request.getParameterValues("purgedItems");
	int changeCount = EJBUtil.stringToNum(request.getParameter("changeCount"));
	int updateAudit_row =0;
	String userFullName = "";
	userJB.setUserId(StringUtil.stringToNum(userId));
	userBean= userJB.getUserDetails();
	userFullName = userId+", "+userBean.getUserLastName()+", "+userBean.getUserFirstName();
	String pkVal ="",ridVal="";
	
	//For storing the existing and newly added item ids
	ArrayList ItemIds = new ArrayList();

	if (changeCount == 0) {
		jsObj.put("result", -2);
		jsObj.put("resultMsg", MC.M_No_OperPerformed);/*jsObj.put("resultMsg", "No operations were performed");*****/
		out.println(jsObj.toString());
		return;
	}
	
    try {
    	String item ="";  
    	SubCostItemJB subcostB = new SubCostItemJB();
    	if (scItems != null){
	    	for (int iX=0; iX<scItems.length; iX++) {
	    		item = scItems[iX];
	    		String[] itemData= StringUtil.strSplit(item, "|");
	    		
	    		String seq = itemData[0];
	    		int itemId = EJBUtil.stringToNum(itemData[1]);
	    		String itemName = itemData[2];
	    		int costCatId = EJBUtil.stringToNum(itemData[3]);
	    		String costCat = itemData[4];
	    		String cost = null;
	    		if (itemData.length > 5){
	    			cost = (StringUtil.isEmpty(itemData[5]))? null:itemData[5];	
	    		}
	    		BigDecimal costDecimal = (cost==null)?null:new BigDecimal(cost);
	
	    		String units = null;
	    		if (itemData.length > 6){
	    			units = (StringUtil.isEmpty(itemData[6]))? null:itemData[6];	
	    		}
	    		
	    		subcostB = new SubCostItemJB();
	    		if (itemId != 0){
	    			subcostB.setSubCostItemId(itemId);
	    			subcostB.getSubCostItemDetails();
	    			
	    			BigDecimal beanDecimal = new BigDecimal(subcostB.getSubCostItemCost());
	    			String beanUnits = subcostB.getSubCostItemUnit();
	    			
	    			//System.out.println(costDecimal.compareTo(beanDecimal));
	    			if (!itemName.equals(subcostB.getSubCostItemName())
	    				|| costCatId != (subcostB.getFkCodelstCategory()).intValue()
		    			|| ((costDecimal==null && beanDecimal!=null) || (costDecimal!=null && beanDecimal==null) 
		    			|| (costDecimal != null && 0 != costDecimal.compareTo(beanDecimal)))
		    			|| ((units==null && beanUnits!=null) || (units!=null && beanUnits==null) 
		    			|| (units != null && !units.equals(beanUnits))))
	    			{
	    				subcostB.setSubCostItemName(itemName);
			    		subcostB.setFkCodelstCategory(costCatId);
			    		subcostB.setFkCodelstCostType("0");
			    		subcostB.setSubCostItemCost(cost);
			    		subcostB.setSubCostItemUnit(units); 
	    				subcostB.setFkCalendar(EJBUtil.stringToNum(protId));
						subcostB.setIpAdd(ipAdd);
						
	    				subcostB.setModifiedBy(userId);
						itemId = subcostB.updateSubCostItem();
	    			}
	    		}else{
		    		subcostB.setSubCostItemName(itemName);
		    		subcostB.setFkCodelstCategory(costCatId);
		    		subcostB.setFkCodelstCostType("0");
		    		subcostB.setSubCostItemCost(cost);
		    		subcostB.setSubCostItemUnit(units);   		
					subcostB.setFkCalendar(EJBUtil.stringToNum(protId));
					subcostB.setIpAdd(ipAdd);
					
					subcostB.setCreator(userId);
					itemId = subcostB.setSubCostItemDetails();
				}
		        if (itemId >0) {
		        	for (;ItemIds.size() < EJBUtil.stringToNum(seq);){
		        		ItemIds.add("0");
		        	}
		        	ItemIds.add(EJBUtil.stringToNum(seq),""+itemId);
		        }
	    		System.out.println(seq+itemId+itemName+costCatId+costCat+cost+units);
	    	}
    	}
    	if (purgeItems != null){
	    	for (int iX=0; iX<purgeItems.length; iX++) {
	    		item = purgeItems[iX];
	    		String[] itemData= StringUtil.strSplit(item, "|");
	    		
	    		String seq = itemData[0];
	    		int itemId = EJBUtil.stringToNum(itemData[1]);
	    	
	    		if (itemId != 0){
	    			/*YK Bug 5821 & # 5818
	    			 * Code Changed to Remove physically from Table.
	    			 * */
	    			 
	    			 // Modified for INF-18183 and BUG#7331 : Raviesh 
	    			 String moduleName = (calledFrom.equalsIgnoreCase("S"))? LC.L_Study_Calendar : LC.L_Library_Calendar;
	    			 String getRidValue= AuditUtils.getRidValue("SCH_SUBCOST_ITEM","esch","PK_SUBCOST_ITEM="+itemId);/*Fetches the RID/PK_VALUE*/
	            	 Hashtable<String, ArrayList<String>> getRowValues = AuditUtils.getRowValues("SCH_SUBCOST_ITEM_VISIT","FK_SUBCOST_ITEM ="+itemId,"esch");/*Fetches the RID/PK_VALUE*/
	            	 ArrayList<String> rowPK  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_PK_KEY);
	 	    		 ArrayList<String> rowRID  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_RID_KEY);
	 	    		 int setId = subcostB.removeSubCostItem(itemId,AuditUtils.createArgs(session,"",moduleName));
	    			 if(!getRidValue.isEmpty()){
	    				 updateAudit_row = AuditUtils.updateAuditROw("esch", userFullName, getRidValue, "D");
	    			 }
	    			 for(int i=0; i<rowPK.size(); i++)
					 {
		    			pkVal = rowPK.get(i);
		    			ridVal = rowRID.get(i);
		    			updateAudit_row = AuditUtils.updateAuditROw("esch", userFullName, ridVal, "D");
					 }
	    			  
	    		}
	    	}
    	}
    	SubCostItemVisitJB subcostVisitB = new SubCostItemVisitJB();
    	if (ops != null){
    		ArrayList deleteVisits = new ArrayList();
    		ArrayList deleteItems = new ArrayList();
			for (int iX=0; iX<ops.length; iX++) {
				String[] part = ops[iX].split("\\|");
				if (part == null || part.length < 4) { continue; }
				String vId = part[1].substring(part[1].indexOf("v")+1);
				String iSeq = part[1].substring(1,part[1].indexOf("v"));
				
				String itemId = (String)ItemIds.get(EJBUtil.stringToNum(iSeq));
				String displacement = part[2];
				if ("A".equals(part[0])) { // Add operation
					subcostVisitB = new SubCostItemVisitJB();
					int pk = subcostVisitB.getExistingItemsByFkItemAndFkVisit(EJBUtil.stringToInteger(itemId), EJBUtil.stringToInteger(vId));
					
					if (pk < 1){
						subcostVisitB.setFkSubCostItem(itemId);
						subcostVisitB.setFkProtocolVisit(vId);
						subcostVisitB.setCreator(userId);
						subcostVisitB.setIpAdd(ipAdd);
						pk = subcostVisitB.setSubCostItemVisitDetails();
					}else{
						subcostVisitB.setModifiedBy(userId);
						subcostVisitB.setIpAdd(ipAdd);
						pk = subcostVisitB.updateSubCostItemVisit();
					}
					if (pk < 1) { throw new Exception("Could not save item-visit details"); }
				} else if ("D".equals(part[0])) { // Delete operation
					if (!itemId.equals("0")){
					    deleteVisits.add(vId);
					    deleteItems.add(itemId);
					}
				}
			}
			if (deleteVisits.size() > 0) {
			    //KM-#5949
			    
			    // Modified for INF-18183 ::: Raviesh
				int delResult = subcostB.deleteSubCostItems(deleteVisits, deleteItems, EJBUtil.stringToNum(userId),AuditUtils.createArgs(session,"",LC.L_Cal_Lib)); 
			    if (delResult < 0) { throw new Exception("Could not delete subject cost item"); }
			}
    	}
	} catch(Exception e) {
		jsObj.put("result", -3);
		jsObj.put("resultMsg", e);
		out.println(jsObj.toString());
		return;
	}
	
    jsObj.put("result", 0);
    jsObj.put("resultMsg", MC.M_Changes_SavedSucc);/*jsObj.put("resultMsg", "Changes saved successfully");*****/
	out.println(jsObj.toString());
%>
