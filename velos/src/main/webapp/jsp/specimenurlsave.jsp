<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>
<%@ page import="java.util.*,com.velos.esch.service.util.EJBUtil,com.velos.eres.web.specimenApndx.SpecimenApndxJB"%><%@page import="com.velos.eres.service.util.MC"%>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="specimenApndxB" scope="page" class="com.velos.eres.web.specimenApndx.SpecimenApndxJB"/>


<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	<%-- if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   } --%>
}
</SCRIPT>

<% String src;
	src= request.getParameter("srcmenu");

%>

<BODY>
<br>

<%
String mode = request.getParameter("mode");
System.out.println("mode .."+mode);
String specApndxId=request.getParameter("specimenApndxId");
System.out.println("specimenappendixId is <><><"+specApndxId);
String specId=request.getParameter("specId");
System.out.println("specimenId is <><><"+specId);
String specimenId=request.getParameter("fkSpecimenId");
String specApndxDesc=request.getParameter("specimenApndxDesc");
String specApndxUri=request.getParameter("specimenApndxUri");
String selectedTab=request.getParameter("selectedTab");

//JM: 21Jul2009: issue no-4174
		String perId = "", stdId = "";
		perId = request.getParameter("pkey");
	 	perId = (perId==null)?"":perId;

		stdId = request.getParameter("studyId");
		stdId = (stdId==null)?"":stdId;

String FromPatPage = request.getParameter("FromPatPage");
if(FromPatPage==null){
	FromPatPage="";
}
String patientCode=request.getParameter("patientCode");
if(patientCode==null){
	patientCode="";
}



String eSign = request.getParameter("eSign");


HttpSession tSession = request.getSession(true);
String ipAdd = (String) tSession.getValue("ipAdd");
String usr = null;
usr = (String) tSession.getValue("userId");

if (sessionmaint.isValidSession(tSession)) {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String oldESign = (String) tSession.getValue("eSign");

	if (!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	} else {
%>
	<form method="POST">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
  	 <input type="hidden" name="mode" value="<%=mode%>">
  	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">
	</FORM>
<%

		 if(mode.equals("M")) {
		     specimenApndxB.setSpecimenApndxId(EJBUtil.stringToNum(specId));
		     specimenApndxB.getSpecimenApndxDetails();

	   	}





        specimenApndxB.setSpecFkSpecimen(specimenId);
		specimenApndxB.setSpecApndxDesc(specApndxDesc);
   		specimenApndxB.setSpecApndxUri(specApndxUri);
         specimenApndxB.setIpAdd(ipAdd);


	   if(mode.equals("N")) {
	           specimenApndxB.setSpecApndxType("U");
	           specimenApndxB.setSpecFkSpecimen(specimenId);
	   	   specimenApndxB.setCreator(usr);
		   int i = specimenApndxB.setSpecimenApndxDetails();
		   mode = "M";
		 }
		else {
		 specimenApndxB.setModifiedBy(usr);
		 int i=specimenApndxB.updateSpecimenApndx();
		}

%>
<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully.*****--%> </p>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=specimenapndx.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&pkId=<%=specimenId%>&mode=<%=mode%>&pkey=<%=perId%>&studyId=<%=stdId%>&FromPatPage=<%=FromPatPage%>&patientCode=<%=patientCode%>" >

<%
	}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>
</HTML>


