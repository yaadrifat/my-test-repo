<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id="userLB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="userSessionB" scope="request" class="com.velos.eres.web.userSession.UserSessionJB"/>
<jsp:useBean id="accountB" scope="request" class="com.velos.eres.web.account.AccountJB"/>
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="alnot" scope="request" class="com.velos.esch.web.alertNotify.AlertNotifyJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="tokenJB" scope="request" class="com.velos.eres.web.userSession.UserSessionTokenJB"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB" />

<HTML>
<TITLE> <%=LC.L_Validate_User%><%--Validate User*****--%> </TITLE>
<%!
String htmlEncode(String input) {
    if (input == null) { return ""; }
    return StringUtil.htmlEncodeXss(input);
}
String token = null;
String action_url = null;
%>



<HEAD>
<%
   //load the setting here again . If the user does not come from ereslogin.jsp but instead through a post from another url. we need to be able
   //to load these settings.
	Configuration.readSettings();

%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>
<script>
var formWin = null;

function popupBlock()
{
	document.login.target="";
	logname=document.login.loguser.value;
	logurl=document.login.logurl.value;
	document.login.action="ereslogin.jsp?mode=popup&logname="+logname+"&logurl="+logurl;
	document.login.submit();
}
	</script>
<body>
<form name="login"  METHOD="POST">


<!-- Gopu : import the objects for customized filed -->
<%@ page import="com.velos.esch.business.common.SchCodeDao,java.util.*,java.text.*,com.velos.eres.service.util.*,java.util.*,java.text.SimpleDateFormat,com.velos.eres.web.user.ConfigFacade, com.velos.eres.business.common.*, com.velos.login.jwt.*"%>

<%//modifed by Gopu for MAHI Enahncement on 8th Mar 2005
	ConfigFacade.getConfigFacade();

String login= request.getParameter("username");

//JM: 24Nov2009: #4085
String fromPg = request.getParameter("fromPage");
fromPg = (fromPg==null)?"":fromPg;

int usrId = EJBUtil.stringToNum(request.getParameter("userId"));


	if (fromPg.equals("reset")){
		userB.setUserId(usrId);
		userB.getUserDetails();
		userB.setUserLoginFlag("0");
		userB.setUserCurrntSsID("");
		userB.updateUser();

%>
	<script>
	//window.self.close();
	</script>

<%
	}



HttpSession tSession = request.getSession(true);

boolean isValidToken = false;
String token = tokenJB.challengeTokenBeforeLogin(request.getParameter("CSRFToken"), request.getRemoteAddr());
if (token != null) { isValidToken = true; }

HttpSession tempSession = request.getSession(false);
if(!(tempSession == null)){
	tempSession.invalidate();
}

	String password =request.getParameter("password");
	int permittedFailedAccessCount = 0;
%>
<!-- commented by gopu to fix bugzilla issues # 2439 -->

<input type="hidden" name="logurl" value=<%=request.getServerName()%>>
<input type="hidden" name="userId" id="userId" value="0">
<input type="hidden" name="CSRFToken" id="CSRFToken" ></input>

<%

 userLB.validateUser(login,password);

 %>

<%! int output; %>

<%
int velosid = 0;
permittedFailedAccessCount = Configuration.FAILEDACCESSCOUNT;
Configuration conf = new Configuration();
String validLoginId = conf.getVelosAdminId(conf.ERES_HOME+"eresearch.xml");
velosid = EJBUtil.stringToNum(validLoginId);

output = 0;
int failCount = 0; int userFound = 0; int origfailCount = 0;
String loginFlagStr = "";
int loginFlag = 0;
String failedUserStat = "";

String realpathPath = request.getRealPath("/jsp/xml/releaseinfo.xml");
System.out.println(" realpathPath = = "+realpathPath);
boolean isSameVer=modCtlDao.chechkXmlVerWithDB(realpathPath);

%>
<%if (!isSameVer) {
	System.out.println("*****************Application version does not matches database version*******************");%>
	<!-- Application version does not match database version -->
	<SCRIPT>
    var w = screen.availWidth-10;
    var h = screen.availHeight-120;
    //signError = window.open("signerror.jsp","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w,"new")
	document.login.CSRFToken.value = "<%=StringUtil.trueValue(tokenJB.createTokenBeforeLogin(request.getRemoteAddr()))%>";
    document.login.action="signerror.jsp";
    document.login.submit();
    //window.history.back();
	//if (signError && !signError.closed)
    //	signError.focus();
    //else
    //	popupBlock();
   </SCRIPT>
<%} else { %>
	<!-- Application version matches database version -->
<%
System.out.println("*****************Application version matches database version*******************");
output=userLB.getUserId();
loginFlagStr = userLB.getUserLoginFlag();

if ( (!(loginFlagStr == null )) && (!(loginFlagStr.equals(""))))

 loginFlag = EJBUtil.stringToNum(loginFlagStr);
 else
 loginFlag = 0;
HttpSession thisSession = request.getSession(true);

if (!isValidToken) {
	login = null;
	output = 0;
}
 %>

<% if (output==0) {

	userFound = userLB.findUser(login);

	if (userFound == 0)
	{
		failCount = EJBUtil.stringToNum(userLB.getUserAttemptCount());
		origfailCount = EJBUtil.stringToNum(userLB.getUserAttemptCount());
		failedUserStat = userLB.getUserStatus();


		//user with pending accout status (not yet activated by velos admin)
		if (failedUserStat.equals("P") || failedUserStat.equals("D")) {
System.out.println("*****************"+userLB.getUserFirstName() + " " + userLB.getUserLastName()+" Status Not Active*******************");	
		%>
    	<SCRIPT>
    	var w = screen.availWidth-10;
	    var h = screen.availHeight-120;
        //signError = window.open("signerror.jsp","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w,"new")
    	document.login.CSRFToken.value = "<%=StringUtil.trueValue(tokenJB.createTokenBeforeLogin(request.getRemoteAddr()))%>";
        document.login.action="signerror.jsp";
        document.login.submit();
        //window.history.back();
        //if (signError && !signError.closed) signError.focus();
        //else
        	//popupBlock();
        </SCRIPT>
		<%
		}
	}
	else if (userFound == -1)
	{
System.out.println("*****************"+userLB.getUserFirstName() + " " + userLB.getUserLastName()+" User Not Found*******************");
	%>
    	<SCRIPT>
    	var w = screen.availWidth-10;
	    var h = screen.availHeight-120;
       // signError = window.open("signerror.jsp","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w,"new")
        //window.history.back();
    	document.login.CSRFToken.value = "<%=StringUtil.trueValue(tokenJB.createTokenBeforeLogin(request.getRemoteAddr()))%>";
        document.login.action="signerror.jsp";
        document.login.submit();
		//if (signError && !signError.closed)  signError.focus();
		//else
			//popupBlock();
        </SCRIPT>
    <%
	}

	failCount++;

	if ( (failCount > permittedFailedAccessCount) && (failedUserStat.equals("A")))
	{


		String faildatetime = DateUtil.getCurrentDateTime();


		String faildate =  faildatetime.substring(0,10);
		String failtime =  faildatetime.substring(11);

		userLB.setUserAttemptCount(String.valueOf(failCount));
		//set the user status to blocked
		userLB.setUserStatus("B");
		// Added By Amarnadh for Bugzilla issue #3197
		Integer uId = new Integer(userLB.getUserId());
		userLB.setModifiedBy(uId.toString());

		userLB.updateUser();

		alnot.notifyAdmin(userLB.getUserId(),request.getRemoteAddr(),velosid,faildate,failtime );
	System.out.println("*****************"+userLB.getUserFirstName() + " " + userLB.getUserLastName()+" Account Freeze due to max Attempt*******************");
		%>

		<SCRIPT>

		var w = screen.availWidth-400;
		var h = screen.availHeight-420;
		document.login.CSRFToken.value = "<%=StringUtil.trueValue(tokenJB.createTokenBeforeLogin(request.getRemoteAddr()))%>";
        document.login.action="accountfreeze.jsp";
        document.login.submit();
		//signerror = window.open("accountfreeze.jsp","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w,"new")
		//window.history.back();
		//if (signerror && !signerror.closed) signerror.focus();
		//else
			//popupBlock();
		</SCRIPT>
	<% return ;
	}
	else
	{
		if(failedUserStat.equals("A"))
		{
			userLB.setUserAttemptCount(String.valueOf(failCount));
			userLB.updateUser();

			//log in er_usersession table
 			String loginTime = DateUtil.getCurrentDateTime();


			String failedUser = String.valueOf(userLB.getUserId());

			userSessionB.setUserId(failedUser);
			userSessionB.setLoginTime(loginTime);
			userSessionB.setIpAdd(request.getRemoteAddr());
			userSessionB.setSuccessFlag("0");
			userSessionB.setUserSessionDetails();



		}
	}

if(failedUserStat.equals("A") || (failedUserStat.equals("B") && origfailCount <= permittedFailedAccessCount))
{
	System.out.println("*****************"+userLB.getUserFirstName() + " " + userLB.getUserLastName()+" User is Blocked or attemt is more than configuration*******************");
%>

<SCRIPT>

var w = screen.availWidth-10;
var h = screen.availHeight-120;
document.login.CSRFToken.value = "<%=StringUtil.trueValue(tokenJB.createTokenBeforeLogin(request.getRemoteAddr()))%>";
document.login.action="signerror.jsp";
document.login.submit();
//signerror = window.open("signerror.jsp","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w,"new")
//window.history.back();
//if (signerror && !signerror.closed) signerror.focus();
//else
	//popupBlock();
</SCRIPT>

<%
} //end of status = A
else if (failedUserStat.equals("B") && failCount > permittedFailedAccessCount)
{
System.out.println("*****************"+userLB.getUserFirstName() + " " + userLB.getUserLastName()+" Account Freeze due to account Blocked*******************");
%>
<SCRIPT>
var w = screen.availWidth-400;
var h = screen.availHeight-420;
document.login.CSRFToken.value = "<%=StringUtil.trueValue(tokenJB.createTokenBeforeLogin(request.getRemoteAddr()))%>";
document.login.action="accountfreeze.jsp";
document.login.submit();
//signerror = window.open("accountfreeze.jsp","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w,"new")
//window.history.back();
//if (signerror && !signerror.closed) signerror.focus();
//else
	//popupBlock();
</SCRIPT>

<%
}

 } else if (loginFlag==1) {
	thisSession.setAttribute("userType", userLB.getUserType());
	thisSession.setAttribute("UserID", output);
	thisSession.setAttribute("username",login);
  	thisSession.setAttribute("password",  htmlEncode(password));
 %>

<SCRIPT>
var w = screen.availWidth-400;
var h = screen.availHeight-400;
/* INF-22330 06-Aug-2012 -Sudhir*/
//var json="{'UserId':'<%//=output%>','UserType':'<%//=userLB.getUserType()%>','username':'<%//=login%>','password':'<%//=password%>'}";

<%
token = tokenJB.createTokenForUser(userLB.getUserId());
if (token == null) {
	token = "";
	action_url = "signerror.jsp";
	tokenJB.clearUserLoginFlag(userLB.getUserId());
	System.out.println("*****************"+userLB.getUserFirstName() + " " + userLB.getUserLastName()+" Token not Created*******************");
} else {
	action_url = "loggedon_err.jsp";
	System.out.println("*****************"+userLB.getUserFirstName() + " " + userLB.getUserLastName()+" You are currently logged in from a different session. Do you want to proceed?*******************");
}
%>
document.login.CSRFToken.value = "<%=token%>";
document.login.action="<%=action_url%>";
document.login.submit();
//window.history.back();
//if (signerror && !signerror.closed) signerror.focus();
//else
	//popupBlock();


</SCRIPT>


<%} else {
String stat = null;
stat = userLB.getUserStatus();
failCount = EJBUtil.stringToNum(userLB.getUserAttemptCount());


%>
<%


if (stat.equals("A"))

{
%>
<%

	thisSession.setAttribute("currentUser",userLB);
  	thisSession.setAttribute("loginname",  userLB.getUserLoginName());
	String uName;
	String sessionTime = null;
	String accSkin = null;
	int userSession = 0;

	accountB.setAccId(EJBUtil.stringToNum(userLB.getUserAccountId()));

	accountB.getAccountDetails();

	thisSession.setAttribute("totalUsersAllowed", accountB.getAccMaxUsr() );
	thisSession.setAttribute("totalPortalsAllowed", accountB.getAccMaxPortal() );//JM:
	thisSession.setAttribute("accMaxStorage", accountB.getAccMaxStorage() );

	String accName = accountB.getAccName();

	accSkin  = accountB.getAccSkin();
	//if (accSkin == null) accSkin  ="default";

	if (accName == null) accName="default";

	thisSession.setAttribute("accName", accName );

	uName = userLB.getUserFirstName() + " " + userLB.getUserLastName();

	String modRight = "";

	modRight = accountB.getAccModRight();
	thisSession.setAttribute("modRight", modRight );


	//set user's protocol management module access in session

	char protocolManagementRight = '0';
	int protocolMgmtSeq = 0,protocolMgmtModSeq = 0;

	modCtlDao.getControlValues("module"); //get extra modules

	ArrayList modCtlDaofeature =  modCtlDao.getCValue();
	ArrayList modCtlDaoftrSeq = modCtlDao.getCSeq();

	protocolMgmtModSeq = modCtlDaofeature.indexOf("MODPROTOCOL");
	protocolMgmtSeq = ((Integer) modCtlDaoftrSeq.get(protocolMgmtModSeq)).intValue();
	protocolManagementRight = modRight.charAt(protocolMgmtSeq - 1);

	thisSession.setAttribute("protocolManagementRight", String.valueOf(protocolManagementRight) );


	String eSign = userLB.getUserESign();

	sessionTime = userLB.getUserSessionTime() ;

	thisSession.setAttribute("userName", uName );
	thisSession.setAttribute("login", login );   // AVSP - Aug01'2016 - attribute set for project Fabric --> Only for Proof of concept
	thisSession.setAttribute("password", password );  // AVSP - Aug01'2016  -  attribute set for project Fabric -->  Only for Proof of concept
	System.out.println("jwttoken generated - " + AuthHelper.createJsonWebToken(login, password, Long.parseLong("2"))); 
	thisSession.setAttribute("jwttoken", AuthHelper.createJsonWebToken(login, password, Long.parseLong("2")) );
	thisSession.setAttribute("pwd", userLB.getUserPwd() );
	if("userpxd".equals(Configuration.eSignConf))
		thisSession.setAttribute("eSign", password );
	else
	thisSession.setAttribute("eSign", eSign );
	
	thisSession.setAttribute("ipAdd",request.getRemoteAddr());
	thisSession.setAttribute("userId", String.valueOf(userLB.getUserId()));
	thisSession.setAttribute("accountId", userLB.getUserAccountId());
	thisSession.setAttribute("userSession", sessionTime);
	thisSession.setAttribute("studyId","");
   	thisSession.setAttribute("studyNo","");
	thisSession.setAttribute("userAddressId",userLB.getUserPerAddressId());
	//set screen width n height in session
	// thisSession.setAttribute("appScHeight",scHeight);
   	// thisSession.setAttribute("appScWidth",scWidth);
	thisSession.setAttribute("sessionName","er");
	thisSession.setAttribute("accSkin",accSkin);

	//user's default group
	thisSession.setAttribute("defUserGroup",userLB.getUserGrpDefault());



	//Sonia ABrol, 10/08/07 get the timezone desc from database
	//accessing the DAO directly because this patch is needed for Korea selectively and I want to change minimum java files

	SchCodeDao schCode = new SchCodeDao();
	String tzDesc = "";
	tzDesc = schCode.getDefaultTimeZone();

	if (StringUtil.isEmpty(tzDesc))
	{
		tzDesc = "";
	}
	thisSession.setAttribute("defTZDesc",tzDesc);
	// end of TZ change



	//bind the session for tracking after setting all the attributes
	EresSessionBinding eresSession = new EresSessionBinding();
	thisSession.setAttribute("eresSessionBinder",eresSession);

	userSession = Integer.parseInt(sessionTime) * 60;
	thisSession.setMaxInactiveInterval(userSession);



	//by salil
	grpRights.setId(Integer.parseInt(userLB.getUserGrpDefault()));
	grpRights.getGrpRightsDetails();
	thisSession.setAttribute("GRights",grpRights);
	thisSession.setAttribute("defGroupPK",userLB.getUserGrpDefault());

	//end change by salil
	String eSignExp=userLB.getUserESignExpiryDate();
	if (StringUtil.isEmpty(eSignExp)) eSignExp= DateUtil.getFormattedDateString("9999","01","01");
	eSignExp= DateUtil.dateToString(DateUtil.stringToDate(eSignExp, null));

	String pwdExp=userLB.getUserPwdExpiryDate();
	if (StringUtil.isEmpty(pwdExp)) pwdExp=DateUtil.getFormattedDateString("9999","01","01");
	pwdExp= DateUtil.dateToString(DateUtil.stringToDate(pwdExp, null));



	Date eSignExpDate = DateUtil.stringToDate(eSignExp,null);
	Date pwdExpDate = DateUtil.stringToDate(pwdExp,null);
	Date d = new Date();


if(d.after(eSignExpDate) || d.after(pwdExpDate)) {
System.out.println("***************"+uName+" Password Expired*******************");
%>

<SCRIPT>

var w = screen.availWidth-10;
var h = screen.availHeight-120;
<%
token = StringUtil.trueValue(tokenJB.createTokenForUser(userLB.getUserId()));
System.out.println("token="+token);
%>
document.login.CSRFToken.value = "<%=token%>";
if (document.login.CSRFToken.value) {
	document.login.action="pwdexpired.jsp";
	document.login.submit();
}
//pwdexpired = window.open("pwdexpired.jsp","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w)
//window.history.back();
//if (pwdexpired && !pwdexpired.closed) pwdexpired.focus();
//else
	//popupBlock();
</SCRIPT>

<%

}

else

{



	grpRights.setId(Integer.parseInt(userLB.getUserGrpDefault()));
	grpRights.getGrpRightsDetails();
	thisSession.setAttribute("GRights",grpRights);

	userLB.setUserAttemptCount("0");
	userLB.updateUser();

System.out.println("***************"+uName+" Login Succesfully*******************");
%>

<!--Session is set<%=userSession%>-->


<%
//Added by IA 12.4.2006
String theme = "";
String homepage = "";
//String description = "";
//CodeDao cd = new CodeDao();

theme = userLB.getUserTheme();
homepage = codeLst.getCodeCustomCol(EJBUtil.stringToNum(theme) );

//description = codeLst.getCodeDescription(EJBUtil.stringToNum(theme) );


if (homepage != null ) {

}else{
	homepage  = "myHome.jsp";
}


//<jsp:forward page = "myHome.jsp?srcmenu=tdMenuBarItem1"> </jsp:forward>%>



<SCRIPT>
function openHome(form,act){
	document.login.CSRFToken.value = "<%=StringUtil.trueValue(tokenJB.createTokenForUser(userLB.getUserId())) %>";
	document.login.action=act;
	document.login.submit();
	//var w = screen.availWidth-10;
	//var h = screen.availHeight-120;
	//document.login.target="formWin";
	//document.login.action=act;
	//myHome = open('donotdelete.html','formWin','resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=' + h + ',width=' + w)

	//myHome = open(act,'formWin','resizable=yes,menubar=yes,toolbar=yes,location=yes,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=' + h + ',width=' + w)
	//if (myHome && !myHome.closed)
	//{
	//document.login.submit();
	//window.history.back();
	// myHome.focus();
	//}
	//else {		
		//document.location.href = act;
		//popupBlock();
	//}
	/*if (myHome && !myHome.closed) myHome.focus();
	else {
		alert("popup blocker,please check" + document.login);
		document.login.target="";
		document.login.action="sendtospace.jsp";
		alert(document.login.action);
		document.login.submit();
	}*/

	//form.submit();
	void(0);
}

	openHome('document.login',"<%=homepage%>");

</SCRIPT>


<%

}

}



else
{
  if (stat.equals("B") && failCount <= permittedFailedAccessCount)
	{
	  System.out.println("*****************"+userLB.getUserFirstName() + " " + userLB.getUserLastName()+" Account is blocked and attempt is less than configuration*******************");
   %>
    <SCRIPT>
    //alert("failCount <= permittedFailedAccessCount");
    var w = screen.availWidth-10;
    var h = screen.availHeight-120;
	document.login.CSRFToken.value = "<%=StringUtil.trueValue(tokenJB.createTokenBeforeLogin(request.getRemoteAddr()))%>";
    document.login.action="signerror.jsp";
    document.login.submit();
    //signError = window.open("signerror.jsp","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w,"new")
    //window.history.back();
   // if (signError && !signError.closed)    signError.focus();
    //else
    	//popupBlock();
   </SCRIPT>

    <%
	}
	else if (stat.equals("B") && failCount > permittedFailedAccessCount )
	{
System.out.println("*****************"+userLB.getUserFirstName() + " " + userLB.getUserLastName()+" Account Freeze for blocked Account*******************");
   %>
    <SCRIPT>
	 //alert("failCount > permittedFailedAccessCount");
     var w = screen.availWidth-400;
     var h = screen.availHeight-420;
	 document.login.CSRFToken.value = "<%=StringUtil.trueValue(tokenJB.createTokenBeforeLogin(request.getRemoteAddr()))%>";
     document.login.action="accountfreeze.jsp";
     document.login.submit();
     //signerror = window.open("accountfreeze.jsp","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w,"new")
     //window.history.back();
     //if (signerror && !signerror.closed)  signerror.focus();
     //else
     	//popupBlock();
   </SCRIPT>

    <%
	}
	else

	{
System.out.println("*****************"+userLB.getUserFirstName() + " " + userLB.getUserLastName()+" Account Status is any other than Active/Blocked/deactivated*******************");
	%>
    <SCRIPT>
    //alert("equals(A)");
    var w = screen.availWidth-10;
    var h = screen.availHeight-120;
	document.login.CSRFToken.value = "<%=StringUtil.trueValue(tokenJB.createTokenBeforeLogin(request.getRemoteAddr()))%>";
    document.login.action="signerror.jsp";
    document.login.submit();
   // signError = window.open("signerror.jsp","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w,"new")
    //window.history.back();
    //if (signError && !signError.closed)    signError.focus();
    //else
    	//popupBlock();
    </SCRIPT>
    <%
	}
}



}



%>
<!-- Added by gopu to fix bugzilla issues # 2439 -->
<script>
	document.getElementById("userId").value = "<%=output%>";
</script>
<%} %>
</form>

</body>

</HTML>