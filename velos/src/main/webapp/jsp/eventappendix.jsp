<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>


<title>
<%if((request.getParameter("fromPage")).equals("selectNetwork")){ %>
Network >> Network Appendix
<%} else if((request.getParameter("fromPage")).equals("siteDetails")){%>
Organization >> Organization Appendix
<%} else if((request.getParameter("fromPage")).equals("userDetails")){ %>
User >> User Appendix
<%} else { %>
<%=MC.M_EvtLib_EvtApdx%><%--Event Library >> Event Appendix*****--%>
<%} %> 
</title>
<!-- Akshi:Added for bug #6948 -->
<%-- Commented by Yogendra Pratap Singh : Bug# 6927 and 6947--%>
<%-- <link type="text/css" href="./styles/bethematch/common.css" rel="STYLESHEET">
<link type="text/css" href="./styles/ns_gt1024.css" rel="STYLESHEET">--%>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>






 

<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")||(request.getParameter("fromPage")).equals("selectNetwork") ||(request.getParameter("fromPage")).equals("siteDetails")||(request.getParameter("fromPage")).equals("userDetails"))

{ %>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>	
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>
	
 <% } //end for if ((request.getParameter("fromPage")).equals("selectEvent"))

	

	%>	



<SCRIPT language="javascript">



function confirmBox(fileName,pgRight) {

	if (f_check_perm(pgRight,'E') == true) {
		var paramArray = [fileName];
		if (confirm(getLocalizedMessageString("L_Del_FromEvt",paramArray))) {/*if (confirm("Delete " + fileName + " from Events?")) {*****/

		    return true;}

		else

		{

			return false;

		}

	} else {

		return false;

	}

}

function confirmDelete(pgRight){

	if (f_check_perm(pgRight,'E') == true) {
		if (confirm(M_DelDoc)) {             /*if(confirm("Would you want to delete this document"))*/

		    return true;
		}
		else
		{
			return false;
		}
	} else {
		return false;
	}
}

function fdownload(formobj,pk,filename,dnldurl)
{
	formobj.file.value = filename;
	formobj.pkValue.value = pk ;
	formobj.dnldurl.value=dnldurl;
	formobj.moduleName.value="Event";
	formobj.action="postFileDownload.jsp";
	//formobj.action=dnldurl;
	
	formobj.target = "_filedownload";
	formobj.method = "POST";
	formobj.submit();
}

</SCRIPT> 



</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.esch.business.common.EventInfoDao,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB,com.aithent.file.uploadDownload.*"%>

<jsp:useBean id="codelstB" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>


<% 
String networkFlag = request.getParameter("networkFlag")==null?"":request.getParameter("networkFlag");
String src;


src= request.getParameter("srcmenu");

//SV, commented on 10/28/04, changes: eventname, propagation flags
 String eventName = request.getParameter("eventName");
eventName = StringUtil.encodeString(eventName);

String calAssoc = request.getParameter("calassoc");
calAssoc = (calAssoc == null) ? "" : calAssoc;

%>


<%-- Modified by Yogendra Pratap Singh : Bug# 6927 and 6947 --%>
<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")||(request.getParameter("fromPage")).equals("selectNetwork") ||(request.getParameter("fromPage")).equals("siteDetails")||(request.getParameter("fromPage")).equals("userDetails")){ %>
<jsp:include page="include.jsp" flush="true"/>
<%}else{

%>

<jsp:include page="panel.jsp" flush="true"> 



<jsp:param name="src" value="<%=src%>"/>



</jsp:include>   

<%}%>



<body id="forms">

<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){%>

		<DIV class="popDefault" style="overflow-y:scroll;" id="div1"> 

	<%	} 

else {
	if(!((request.getParameter("fromPage")).equals("selectNetwork"))){	%>

<DIV class="browserDefault" style="overflow-y:scroll;" id="div1"> 

<%}
}%>	

<%

	int pageRight = 0;

	String duration = request.getParameter("duration");   

	String protocolId = request.getParameter("protocolId");   

	String calledFrom = request.getParameter("calledFrom");   

	String eventId = request.getParameter("eventId");

	if(!networkFlag.equalsIgnoreCase("")){
		eventId = "callingfromNetwork";
	}
	
	String networkId = request.getParameter("networkId");
	String mainNetworkId = request.getParameter("mainNetworkId");
	
	String siteId = (request.getParameter("siteId")==null)?"":request.getParameter("siteId");
	String userPk = (request.getParameter("userPk")==null)?"":request.getParameter("userPk");
	siteB.setSiteId(StringUtil.stringToNum(siteId));
	siteB.getSiteDetails();
	

	String mode = request.getParameter("mode");

	String selectedTab=request.getParameter("selectedTab");

	String fromPage = request.getParameter("fromPage");

	String calStatus = request.getParameter("calStatus");

	String eventmode = request.getParameter("eventmode");

	String displayDur=request.getParameter("displayDur");

	String displayType=request.getParameter("displayType");

    String m="";
	String incorrectFile = "";	

	HttpSession tSession = request.getSession(true); 


	if (sessionmaint.isValidSession(tSession))

	{
	String space = request.getParameter("outOfSpace");

	space = (space==null)?"":space.toString();
	incorrectFile = (incorrectFile==null)?"":incorrectFile.toString();	

	if(space.equals("1")) {

%>

<br><br><br><br>

<table width=100%>

<tr>

<td align=center>



<p class = "sectionHeadings">



<%=MC.M_UploadSpace_ContAdmin%><%--The space allotted to you for file uploading has exhausted. Please contact Velos Administrator.*****--%> 



</p>

</td>

</tr>



<tr height=20></tr>

<tr>

<td align=center>



		<button onclick="window.history.go(-2);"><%=LC.L_Back%></button>

</td>		

</tr>		

</table>		





<%		

	} else { //else of if for space.equals("1")

////////////////////////

	if(incorrectFile.equals("1")) {

%>

<br><br><br><br>

<table width=100%>

<tr>

<td align=center>



<p class = "sectionHeadings">



<%=MC.M_UploadingFilesNotExst_Chk%><%--Either the file specified for uploading does not exist or the file is empty. Please check the file and path and try again.*****--%> 



</p>

</td>

</tr>



<tr height=20></tr>

<tr>

<td align=center>



		<button onClick="window.history.go(-1);"><%=LC.L_Back%></button>

</td>		

</tr>		

</table>		





<%		

	} else { //else of if for incorrectFile.equals("1")





		if (calledFrom.equals("S")) {

   	   	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");	 

	  	   if ((stdRights.getFtrRights().size()) == 0){

			 	pageRight= 0;

		   }else{

				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));

    	   }	
		   
		   

		}else if (calledFrom.equals("P")) {

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
		}

		else if(calledFrom.equals("L")){

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("EVLIB"));
		
		}
		else if(networkFlag.equalsIgnoreCase("User_doc")){
			GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");	
			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MUSERS"));
		}
		else if(calledFrom.equals("NetWork")||calledFrom.equals("Site") || calledFrom.equals("Users")){
			GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");	
			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSITES"));
		}
	

	   int count = 0;
	   EventInfoDao eventinfoDao= new EventInfoDao() ;
	   if(networkFlag.equalsIgnoreCase("")){

	    eventinfoDao = eventdefB.getEventDefInfo(com.velos.eres.service.util.EJBUtil.stringToNum(eventId));
	   }
	   else{
		  	   
		   eventinfoDao = eventdefB.getEventDefInfo(com.velos.eres.service.util.EJBUtil.stringToNum(networkId),com.velos.eres.service.util.EJBUtil.stringToNum(siteId),com.velos.eres.service.util.EJBUtil.stringToNum(userPk),networkFlag);
	  
		  
	   }
	   ArrayList docIds= eventinfoDao.getDocIds();

	   ArrayList docNames= eventinfoDao.getDocNames();

	   ArrayList docTypes= eventinfoDao.getDocTypes();

	   ArrayList docDescs= eventinfoDao.getDocDescs();
	   
	   ArrayList docVersions= eventinfoDao.getDocVersions();

	   String docId="";

	   String docName="";

   	   String docType="";

   	   String docDesc="";	
   	   
   	   String docVersion="";


%>



<%
	String calName = "";

	if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary") || fromPage.equals("patientschedule") || fromPage.equals("selectNetwork") /* || fromPage.equals("selectStudyNetwork") */ || fromPage.equals("siteDetails") || fromPage.equals("userDetails"))
	{
%>
	<!--P class="sectionHeadings"> Protocol Calendar >> Event Appendix </P-->
<%
	}
	else
	{
		calName = (String) tSession.getValue("protocolname");
%>
	<P class="sectionHeadings"> <%Object[] arguments = {calName}; %>
	    <%=VelosResourceBundle.getMessageString("M_PcolCal_EvtApdx",arguments)%><%--Protocol Calendar [ {0} ] >> Event Appendix*****--%>  </P>
<%}
if(!calledFrom.equals("Site") && !calledFrom.equals("Users")){
%>

<jsp:include page="eventtabs.jsp" flush="true"> 

<jsp:param name="duration" value="<%=duration%>"/>

<jsp:param name="protocolId" value="<%=protocolId%>"/>

<jsp:param name="calledFrom" value="<%=calledFrom%>"/>

<jsp:param name="fromPage" value="<%=fromPage%>"/>

<jsp:param name="mode" value="<%=mode%>"/>

<jsp:param name="calStatus" value="<%=calStatus%>"/>

<jsp:param name="displayDur" value="<%=displayDur%>"/>

<jsp:param name="displayType" value="<%=displayType%>"/>

<jsp:param name="eventId" value="<%=eventId%>"/>

<jsp:param name="networkFlag" value="<%=networkFlag%>"/>

<jsp:param name="networkId" value="<%=networkId%>"/>

<jsp:param name="siteId" value="<%=siteId%>"/>

<jsp:param name="userPk" value="<%=userPk%>"/>

<jsp:param name="pageRight" value="<%=pageRight %>"/>

<jsp:param name="src" value="<%=src%>"/>

<jsp:param name="eventName" value="<%=eventName%>"/>

<jsp:param name="calassoc" value="<%=calAssoc%>"/>

<jsp:param name="selectedTab" value="<%=selectedTab%>"/>

<jsp:param name="mainNetworkId" value="<%=mainNetworkId%>" />


  </jsp:include>



<%}

 if(eventId == "" || eventId == null || eventId.equals("null")  || eventId.equals("")) {

	%>

	  <jsp:include page="eventDoesNotExist.jsp" flush="true"/>

  <%

	}else {

%>



<%



if(fromPage.equals("eventbrowser")) {

%>



<form name="eventbrowser" METHOD=POST action="eventbrowser.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calassoc=<%=calAssoc%>">



<%

} else {
	if(networkFlag.equalsIgnoreCase("")){

%>
<form form name="eventbrowser" METHOD=POST action="fetchProt.jsp?protocolId=<%=protocolId%>&srcmenu=<%=src%>&selectedTab=5&mode=<%=mode%>&calledFrom=<%=calledFrom%>&calStatus=<%=calStatus%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>">
<%}else{%>

<DIV class="popDefault" style="overflow-y:scroll;" id="div1">

<form form name="eventbrowser" METHOD=POST >

<div style="margin:10px 0 10px 0">
<table cellpadding="5" cellspacing="5">

<%

NetworkDao nwtDao=new NetworkDao();
nwtDao.getAppndxNtOrgDetails(StringUtil.stringToNum(networkId));
NetworkDao mainnwtDao=new NetworkDao();
mainnwtDao.getAppndxNtOrgDetails(StringUtil.stringToNum(mainNetworkId));
ArrayList siteName=nwtDao.getSiteNameList();	
String siteNm="";
String sitetp="";
String siteTyp="";
if(calledFrom.equals("Site")){
	siteNm = siteB.getSiteName();
	sitetp = siteB.getSiteCodelstType();
	CodeDao cd=new CodeDao();
	siteTyp=cd.getCodeDescription(StringUtil.stringToNum(sitetp));
}

if(networkFlag.equals("Org_doc")){
	String siteType=nwtDao.getSiteType();
%>
	<tr>
		<td><b><%=LC.L_Organization_Name%>:</b></td>
		<% if(calledFrom.equals("Site")){%>
			<td><%=siteNm==null?"-":siteNm %></td>
		<%} else {%>
			<td><%=siteName.get(0)==null?"-":siteName.get(0) %></td>
		<%} %>
	</tr>
	<tr>
		<td><b><%=LC.L_Type%>:</b></td>
		<% if(calledFrom.equals("Site")){%>
			<td><%=siteTyp%></td>
		<%} else {%>
			<td><%=siteType%></td>
		<%} %>	
	</tr>
<%} 
else if(networkFlag.equals("Org_level_doc")){
	
	ArrayList relationshipType=nwtDao.getNetworkTypeDescList();
	String mainNetwork=nwtDao.getMainNetwork();
%>
	<tr>
		<td><b><%=LC.L_InNetwork.substring(3)%>:</b></td>
		<td><%=mainNetwork%></td>
	</tr>
	<tr>
		<td><b><%=LC.L_Organization_Name%>:</b></td>
		<td><%=siteName.get(0)==null?"-":siteName.get(0)%></td>
	</tr>
	<tr>
		<td><b><%=LC.L_Relation_Type%>:</b></td>
		<td><%=relationshipType.get(0)==null?"-":relationshipType.get(0) %></td>
	</tr>
<%} 
else if(networkFlag.equals("Network_User_doc")){
	
	String mainNetwork=mainnwtDao.getMainNetwork();
	
%>
	<tr>
		<td><b><%=LC.L_Network_Name%>:</b></td>
		<td><%=mainNetwork==null?"-":mainNetwork%></td>
	</tr>
	<tr>
		<td><b><%=LC.L_User_Name%>:</b></td>
		<td><%=UserDao.getUserFullName(StringUtil.stringToNum(userPk))%></td>
	</tr>
	
<%} 
else if(networkFlag.equals("User_doc")){
	
%>
	<tr>
		<td><b><%=LC.L_User_Name%>:</b></td>
		<td><%=UserDao.getUserFullName(StringUtil.stringToNum(userPk))%></td>		
	</tr>
	
<%} %>


</table>
</div>

<%

}}

%>





<%         

//if ((calStatus.equals("A")) || (calStatus.equals("F"))){%>

         <!--  <P class = "defComments"><FONT class="Mandatory">Changes cannot be made to Event Interval and Event Details for an Active/Frozen Protocol</Font></P> -->

<%//}



if (pageRight == 4 && (!(calledFrom.equals("NetWork")) && !(calledFrom.equals("Site")) && !(calledFrom.equals("Users")))) {

%>

   <P class = "defComments"><FONT class="Mandatory"><%=MC.M_OnlyViewPerm_ForEvt%><%--You have only View permission for the Event.*****--%></Font></P>

<%}%>





<TABLE width="100%" class="custom-table-center">
<TR>
<TD WIDTH="50%" valign="top">
	<table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midalign">
      <tr> 
        <th width="100%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"> <%=LC.L_My_Files%><%-- My Files*****--%> </th>
      </tr>
     
      <tr> 
        <td>
		<%         

		if(networkFlag.equalsIgnoreCase("")){

			if (fromPage.equals("eventlibrary")){
	  			m="N"; 
				}
			else
				{
				if (mode.equals("N")){m="N";}else{m="E";}
				}
		%>
	<!--KM-#3207 -->

	<A href="addeventfile.jsp?docmode=N&eventId=<%=eventId%>&srcmenu=<%=src%>&selectedTab=4&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=M&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>" onClick="return f_check_perm(<%=pageRight%>,'<%=m%>')"><%=MC.M_ClickHere_UploadNewFile%><%--Upload Document*****--%></A>

	<%} else {

		if(((calledFrom.equals("NetWork")) && (pageRight==5 || pageRight==7))){
		%>
			
			<A href="addeventfile.jsp?docmode=N&networkId=<%=networkId%>&mainNetworkId=<%=mainNetworkId%>&siteId=<%=siteId%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&networkFlag=<%=networkFlag%>&calledFrom=<%=calledFrom%>&mode=M&fromPage=<%=fromPage%>&userPk=<%=userPk%>" onClick="return f_check_perm(<%=pageRight%>,'<%=m%>')"><%=MC.M_ClickHere_UploadNewFile%><%--Upload Document*****--%></A>	
		<%}else if((calledFrom.equals("Site") || calledFrom.equals("Users") || calledFrom.equals("NetWork") )  && (pageRight>=4)){%>
			<A href="addeventfile.jsp?docmode=N&networkId=<%=networkId%>&siteId=<%=siteId%>&mainNetworkId=<%=mainNetworkId%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&networkFlag=<%=networkFlag%>&calledFrom=<%=calledFrom%>&mode=M&fromPage=<%=fromPage%>&userPk=<%=userPk%>" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=MC.M_ClickHere_UploadNewFile%><%--Upload Document*****--%></A>
		<% }} %>
        </td>
      </tr>
	 
      
    </table>
     <div style="width:99%; max-height:350px; overflow:auto">
	 <% if((calledFrom.equals("NetWork") || calledFrom.equals("Site") || calledFrom.equals("Users")) && (pageRight==5 || pageRight==4)){%>
		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midalign">
	 <%}else{%>
	 	<table  cellspacing="0" cellpadding="0" border="0" class="basetbl outline midalign">
	 <%}%>
	<th width="40%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_File_Name%><%-- File name*****--%></th>
	<th width="50%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Description%><%-- Description*****--%></th>
	<%if(fromPage.equalsIgnoreCase("selectNetwork") /* || fromPage.equalsIgnoreCase("selectStudyNetwork") */ || fromPage.equalsIgnoreCase("siteDetails") || fromPage.equals("userDetails")){ %>
	<th width="10%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Version%><%-- Version*****--%></th>
	<%} %>
	<%if(((calledFrom.equals("NetWork")) && pageRight>=6) || calledFrom.equals("L") || calledFrom.equals("S")|| calledFrom.equals("P")){ %>
		<th width="5%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Edit%><%-- Edit*****--%></th>
		<th width="5%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Delete%><%-- Delete*****--%></th>
	<%}else if((calledFrom.equals("Site") || calledFrom.equals("Users")) && pageRight>=4){ %>
	<th width="5%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Edit%><%-- Edit*****--%></th>
	<th width="5%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Delete%><%-- Delete*****--%></th>
	<%}
	String dnld;
	com.aithent.file.uploadDownload.Configuration conf = new com.aithent.file.uploadDownload.Configuration();

	conf.readSettings("sch");
	conf.readUploadDownloadParam(conf.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "eventapndx");
	dnld=conf.DOWNLOADSERVLET ;
	String modDnld = "";
	
	
   for(int i=0;i<docIds.size(); i++) {
	docId=   (String)docIds.get(i);
	docDesc= (String)docDescs.get(i); 
	docName = (String)docNames.get(i); 
	docType = (String)docTypes.get(i); 
	if(fromPage.equalsIgnoreCase("selectNetwork") || fromPage.equalsIgnoreCase("siteDetails") || fromPage.equals("userDetails")){
	docVersion = (String)docVersions.get(i);
	}
	if(docType==null){
		docType="";
	}
	if(docType.equals("U"))
	continue;

		if ((i%2)==0) {
  %>
      <tr class="browserEvenRow"> 
        <%
		} else{
  %>
      <tr class="browserOddRow"> 
        <%
		}
modDnld = dnld + "?file=" + StringUtil.encodeString(docName) ;		
//out.println(StringUtil.encodeString(modDnld));
  %>

	<td>
	<span style="overflow: hidden; width: 150px; word-wrap: break-word; display: block;">
	<A href="#" style="color:#3B9EC6" onClick="fdownload(document.eventbrowser,<%=docId%>,'<%=StringUtil.encodeString(docName)%>','<%=dnld%>');return false;" >
	
 	 <%=docName%></A></span> </td>

	<td><span style="overflow: hidden; width: 230px; word-wrap: break-word; display: block;"> <%=docDesc%></span> </td>
	
	<%if(fromPage.equalsIgnoreCase("selectNetwork") || fromPage.equalsIgnoreCase("siteDetails") || fromPage.equals("userDetails")){ %>
	<td><span style="overflow: hidden; width: 100px; word-wrap: break-word; display: block;"> <%=docVersion==null?"-":docVersion%></span> </td>
	<%} %>
	
	<%if(((calledFrom.equals("NetWork")) && pageRight>=6) || calledFrom.equals("L") || calledFrom.equals("S")|| calledFrom.equals("P")){%>
		<td width=15%>
<%}else if((calledFrom.equals("Site") || calledFrom.equals("Users")) && pageRight>=4){%>
<td width=15%>
<%}
if(networkFlag.equalsIgnoreCase("")) {%>
	<A HREF="editeventfile.jsp?docType=F&docId=<%=docId%>&docmode=M&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>" onClick="return f_check_perm(<%=pageRight%>,'E')"><img src="../jsp/images/edit.gif" style="border-style: none;" title="<%=LC.L_Edit%>"/><A>
<%} else{
	if(((calledFrom.equals("NetWork")) && pageRight>=6)){%>
	<A HREF="editeventfile.jsp?docType=F&docId=<%=docId%>&docmode=M&networkId=<%=networkId%>&mainNetworkId=<%=mainNetworkId%>&siteId=<%=siteId%>&selectedTab=<%=selectedTab%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&networkFlag=<%=networkFlag%>&userPk=<%=userPk%>" onClick="return f_check_perm(<%=pageRight%>,'E')"><img src="../jsp/images/edit.gif" style="border-style: none;" title="<%=LC.L_Edit%>"/><A>
	<%}else if ((calledFrom.equals("Site") || calledFrom.equals("Users")) && pageRight>=4){%>
	<A HREF="editeventfile.jsp?docType=F&docId=<%=docId%>&docmode=M&networkId=<%=networkId%>&mainNetworkId=<%=mainNetworkId%>&siteId=<%=siteId%>&selectedTab=<%=selectedTab%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&networkFlag=<%=networkFlag%>&userPk=<%=userPk%>" onClick="return f_check_perm(<%=pageRight%>,'E')"><img src="../jsp/images/edit.gif" style="border-style: none;" title="<%=LC.L_Edit%>"/><A>
	<%}}%>
	</td>


	<%if(((calledFrom.equals("NetWork")) && pageRight>=6) ||calledFrom.equals("L") ||  calledFrom.equals("S")|| calledFrom.equals("P")){%>
		<td width=15%>
<%}else if((calledFrom.equals("Site") || calledFrom.equals("Users")) && pageRight>=4){%>
<td width=15%>
<%}
	if(networkFlag.equalsIgnoreCase("")) {%>

	<A HREF="urlsave.jsp?docType=F&docId=<%=docId%>&docmode=D&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>" onClick="return confirmBox('<%=docName%>',<%=pageRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A>
<%} else{
	if(((calledFrom.equals("NetWork")) && pageRight>=6)){%>
		<A HREF="urlsave.jsp?docType=F&docId=<%=docId%>&docmode=D&networkId=<%=networkId%>&mainNetworkId=<%=mainNetworkId%>&siteId=<%=siteId%>&selectedTab=<%=selectedTab%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&networkFlag=<%=networkFlag%>&userPk=<%=userPk%>" onClick="return confirmDelete(<%=pageRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A>
	<%}else if((calledFrom.equals("Site") || calledFrom.equals("Users")) && pageRight>=4){%>
		<A HREF="urlsave.jsp?docType=F&docId=<%=docId%>&docmode=D&networkId=<%=networkId%>&mainNetworkId=<%=mainNetworkId%>&siteId=<%=siteId%>&selectedTab=<%=selectedTab%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&networkFlag=<%=networkFlag%>&userPk=<%=userPk%>" onClick="return confirmDelete(<%=pageRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A>
	<%}}%>
	</td>

   </tr>

<%

   }

%>
    </table>
    </div>
</TD>

<%-- <%if (networkFlag.equalsIgnoreCase("")){ %> --%>
<TD WIDTH="50%" valign="top">
  	<table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midalign" >
      <tr> 
        <th width="100%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"> <%=LC.L_My_Links%><%-- My Links*****--%> </th>
      </tr>
      
      <tr> 
        <td> 
		
	<%         
if (networkFlag.equalsIgnoreCase("")){ %>
	 <A href="addeventurl.jsp?docmode=N&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&selectedTab=4&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>" onClick="return f_check_perm(<%=pageRight%>,'<%=m%>')"><%=MC.M_ClickHere_AddNewLink%><%--Link URL*****--%></A>
<%}else{

	if(((calledFrom.equals("NetWork")) && (pageRight==5 || pageRight==7))){%>
				<A href="addeventurl.jsp?docmode=N&networkId=<%=networkId%>&mainNetworkId=<%=mainNetworkId%>&siteId=<%=siteId%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&networkFlag=<%=networkFlag%>&calledFrom=<%=calledFrom%>&mode=M&fromPage=<%=fromPage%>&userPk=<%=userPk%>" onClick="return f_check_perm(<%=pageRight%>,'<%=m%>')"><%=MC.M_ClickHere_AddNewLink%><%--Link URL*****--%></A>	
		<%}
	else if((calledFrom.equals("Site") || calledFrom.equals("Users") || calledFrom.equals("NetWork")) && (pageRight>=4)){%>
	<A href="addeventurl.jsp?docmode=N&networkId=<%=networkId%>&mainNetworkId=<%=mainNetworkId%>&siteId=<%=siteId%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&networkFlag=<%=networkFlag%>&calledFrom=<%=calledFrom%>&mode=M&fromPage=<%=fromPage%>&userPk=<%=userPk%>" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=MC.M_ClickHere_AddNewLink%><%--Link URL*****--%></A>	
<%}	
}
%>
 
        </td>
      </tr>
	
      
     
    </table>
     <div style="width:99%; max-height:350px; overflow:auto">
	<% if(calledFrom.equals("NetWork") && (pageRight==5 || pageRight==4)){%>
		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midalign">
 	<%}else{%>
		<table  cellspacing="0" cellpadding="0" border="0" class="basetbl outline midalign">
	<%}%>
	<th width="40%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Url_Upper%><%-- URL*****--%></th>
	<th width="50%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Description%><%-- Description*****--%></th>
	<%if(((calledFrom.equals("NetWork")) && pageRight>=6) || calledFrom.equals("L")  || calledFrom.equals("S")|| calledFrom.equals("P")){%>
		<th width="5%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Edit%><%-- Edit*****--%></th>
		<th width="5%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Delete%><%-- Delete*****--%></th>
     <%}else if((calledFrom.equals("Site") || calledFrom.equals("Users")) && pageRight>=4){%>
		<th width="5%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Edit%><%-- Edit*****--%></th>
		<th width="5%" style="background-image: url(./images/skin_default/bg-tbheading.jpg);"><%=LC.L_Delete%><%-- Delete*****--%></th>
  	<%}
     for(int i=0;i<docIds.size(); i++) {



	docId= (String)docIds.get(i);

	docDesc= (String)docDescs.get(i); 

	docName = (String)docNames.get(i); 

	docType = (String)docTypes.get(i); 
 if(docType==null) {docType="";}
	if(docType.equals("F"))

	continue;





		if ((i%2)==0) {



  %>

      <tr class="browserEvenRow"> 

        <%



		}



		else{



  %>

      <tr class="browserOddRow"> 

        <%



		}



  %>

	<td><span style="overflow: hidden; width: 150px; word-wrap: break-word; display: block;"><A style="color:#3B9EC6" href=<%=docName%> target="_new"><%=docName%></A></span></td>

	<td><span style="overflow: hidden; width: 230px; word-wrap: break-word; display: block;"> <%=docDesc%></span> </td>

	<% if(((calledFrom.equals("NetWork")) && pageRight>=6) || calledFrom.equals("L") || calledFrom.equals("S")|| calledFrom.equals("P")){%>
       <td width=15%>
	<%}else if((calledFrom.equals("Site") || calledFrom.equals("Users")) && pageRight>=4){%>
    <td width=15%>
	<%}
if(networkFlag.equalsIgnoreCase("")) {%>
	<A HREF="addeventurl.jsp?docmode=M&docId=<%=docId%>&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&selectedTab=4&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>" onClick="return f_check_perm(<%=pageRight%>,'E')"><img src="../jsp/images/edit.gif" style="border-style: none;" title="<%=LC.L_Edit%>"/></A>
<%}

else{
	if(((calledFrom.equals("NetWork")) && pageRight>=6)){%>
		<A HREF="addeventurl.jsp?docmode=M&docId=<%=docId%>&networkId=<%=networkId%>&mainNetworkId=<%=mainNetworkId%>&siteId=<%=siteId%>&selectedTab=<%=selectedTab%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&networkFlag=<%=networkFlag%>&networkIdflag=<%=networkId%>&userPk=<%=userPk%>" onClick="return f_check_perm(<%=pageRight%>,'E')"><img src="../jsp/images/edit.gif" style="border-style: none;" title="<%=LC.L_Edit%>"/><A>
<%}else if((calledFrom.equals("Site") || calledFrom.equals("Users")) && pageRight>=4){%>
<A HREF="addeventurl.jsp?docmode=M&docId=<%=docId%>&networkId=<%=networkId%>&mainNetworkId=<%=mainNetworkId%>&siteId=<%=siteId%>&selectedTab=<%=selectedTab%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&networkFlag=<%=networkFlag%>&networkIdflag=<%=networkId%>&userPk=<%=userPk%>" onClick="return f_check_perm(<%=pageRight%>,'E')"><img src="../jsp/images/edit.gif" style="border-style: none;" title="<%=LC.L_Edit%>"/><A>
<%}} %>

 </td>
<%if(((calledFrom.equals("NetWork")) && pageRight>=6) || calledFrom.equals("L") || calledFrom.equals("S")|| calledFrom.equals("P")){ %>
	<td width=15%>
<%}else if((calledFrom.equals("Site") || calledFrom.equals("Users")) && pageRight>=4){ %>
<td width=15%>
<%}
if(networkFlag.equalsIgnoreCase("")) {%>
	<A HREF="urlsave.jsp?docType=U&docId=<%= docId%>&docmode=D&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>" onClick="return confirmBox('<%=docName%>',<%=pageRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A>
<%}

else{
	if(((calledFrom.equals("NetWork")) && pageRight>=6)){%>
		
		<A HREF="urlsave.jsp?docType=U&docId=<%=docId%>&docmode=D&networkId=<%=networkId%>&mainNetworkId=<%=mainNetworkId%>&siteId=<%=siteId%>&selectedTab=<%=selectedTab%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&networkFlag=<%=networkFlag%>&networkIdflag=<%=networkId%>&userPk=<%=userPk%>" onClick="return confirmDelete(<%=pageRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A>
<%}else if((calledFrom.equals("Site") || calledFrom.equals("Users")) && pageRight>=4){%>

<A HREF="urlsave.jsp?docType=U&docId=<%=docId%>&docmode=D&networkId=<%=networkId%>&mainNetworkId=<%=mainNetworkId%>&siteId=<%=siteId%>&selectedTab=<%=selectedTab%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&networkFlag=<%=networkFlag%>&networkIdflag=<%=networkId%>&userPk=<%=userPk%>" onClick="return confirmDelete(<%=pageRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A>
<%}}%>

	</td>

   </tr>

<%

   }

%>
    </table>
    </div>
</TD>
</TR>

</TABLE>
<br>

<table>

<tr>

  <td> 

    <%

  	if (fromPage.equals("selectEvent")) {

         %>   

		<A type="submit" href="selecteventus.jsp?fromPage=NewEvent"><%=LC.L_Back%></A>

 <%

 	}

 %> 

  <%

  	if (fromPage.equals("eventbrowser")) {

  %>

		<A type="submit" href="eventbrowser.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>"><%=LC.L_Back%></A>

 <%

 	}

  	if (fromPage.equals("eventlibrary")) {

 %>

		<A type="submit" href="eventlibrary.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>"><%=LC.L_Back%></A>

 <%

 	}

  %>

																				

 <%

 	//}

 %>



 </td>  

</tr>

</table>


	 
	
	<input type="hidden" name="tableName" value="SCH_DOCS">
    <input type="hidden" name="columnName" value="DOC">
    <input type="hidden" name="pkColumnName" value="PK_DOCS">
    <input type="hidden" name="module" value="eventapndx">
    <input type="hidden" name="db" value="sch">
    <input type="hidden" name="pkValue" value="">
    <input type="hidden" name="file" value="">
    <input type="hidden" name="dnldurl" value="">
    <input type="hidden" name="moduleName" value="">

</form>



<%



	} //event
} // end of if for incorrectFile
}// end of if for space

} else {  //else of if body for session



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%



}



%>



  <%if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){}
else {

%>



</div>

<div class ="mainMenu" id = "emenu">

<jsp:include page="getmenu.jsp" flush="true"/>

</div>

	<% }%>
<!-- Akshi:Added for bug #6948 -->
</div>
<div class = "myHomebottomPanel"> 

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>
</body>
</html>