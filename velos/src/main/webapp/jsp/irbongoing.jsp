<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page language="java" import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings"%>
<%@ page language="java" import="com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@ page language="java" import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB" %>

<html>
<head>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="tokenJB" scope="request" class="com.velos.eres.web.userSession.UserSessionTokenJB"/>
<%
ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
String titleStr = "";
if (sessionmaint.isValidSession(request.getSession(true)))
{
    String accId = (String) request.getSession(true).getValue("accountId");
    
    ArrayList tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "top_menu");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("irb_menu".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
                titleStr += settings.getObjDispTxt();
            }
            break;
        } 
    }
    tempList = null;

    tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "irb_menu");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("ongoing_menu".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
            	Object[] arguments = {titleStr,settings.getObjDispTxt()};
                titleStr = VelosResourceBundle.getLabelString("L_Gtgt",arguments);/*titleStr += " >> " + settings.getObjDispTxt();*****/
            }
            break;
        } 
    }
    tempList = null;
    
}
%>
<title><%=titleStr %></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<% 
String src= request.getParameter("srcmenu");
HttpSession tSession = request.getSession(true); 
String accountId = "";
String studyId = "";
if (sessionmaint.isValidSession(tSession))
{
  accountId = (String) tSession.getValue("accountId");
  studyId = (String) tSession.getValue("studyId");
  if(accountId == null || accountId == "") {
  %>
    <jsp:include page="timeout.html" flush="true"/>
  <%
    return;
  }
}
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<SCRIPT language="javascript">
function submitForm(formobj) {
	formobj.action="updateSubmission.jsp?&srcmenu=<%=request.getParameter("srcmenu")%>&selectedTab=irb_init_tab"; 
	return true;
}
</SCRIPT> 
<body>

<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<%
	
		int grpRight = 0;

	//copy a new study is controlled by Manage Protocols new group right
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	grpRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_ON"));

  String from = "version";
  String tab = request.getParameter("selectedTab");
  String studyIdForTabs = request.getParameter("studyId");
  String includeTabsJsp = "irbpendtabs.jsp";
%>
<DIV class="BrowserBotN BrowserBotN_RC_2" id = "div1">
<%
  if (sessionmaint.isValidSession(tSession))
  {
  	  if (grpRight >=4)
  	  {
    String usrId = (String) tSession.getValue("userId");
    String uName = (String) tSession.getValue("userName");
    String defUserGroup = (String) tSession.getAttribute("defUserGroup");
    String CSRFToken=StringUtil.trueValue(tokenJB.createTokenForUser((String) tSession.getValue("userId")==null?0:StringUtil.stringToNum((String) tSession.getValue("userId"))));
  %>

    <jsp:include page="ongoingBrowser.jsp" flush="true">
      <jsp:param name="tabsubtype" value="irb_ongoing_menu"/>
      <jsp:param name="tabtype" value="irbOngoing"/>
      <jsp:param name="CSRFToken" value="<%=CSRFToken%>"/>
    </jsp:include>

		<%
		} //access check
		else {
		%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
			<%
		}
 
  }//end of if body for session
  else
  {
  %>
    <jsp:include page="timeout.html" flush="true"/>
  <%
  }
  %>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
</body>

</html>

