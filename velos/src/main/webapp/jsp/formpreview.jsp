<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
 
<title><%=LC.L_Preview%><%--Preview*****--%></title>
<%--Commented For Bug# 8205:Yogendra--%>
<%--<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script> --%>
<jsp:include page="include.jsp" flush="true" />
<SCRIPT>
function openUserSearch(formobj,colName) {
//alert(formobj);
//alert(colName);
  //  windowName=window.open("formusersearch.jsp?formobj="+formobj+"&colName="+colName,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
//	windowName.focus();
}

//set calledFromPreview = true in form.js
	
    calledFromPreview = true;
</SCRIPT>

</head>

<%@page language = "java" import="com.velos.eres.business.common.SaveFormDao,com.velos.eres.business.common.FormSecDao,com.velos.eres.service.util.*" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="formLibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<% if (StringUtil.trueValue((String)request.getHeader("USER-AGENT")).indexOf("MSIE") != -1) { %>
<body style="overflow:scroll;">
<% } else {  %>
<body style="overflow:visible;">
<% }  %>
<%--For Bug# 8205:Yogendra--%>
<jsp:include page="popupPanel.jsp" flush="true"/>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script> 
<%

HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%

  String formHtml = MC.M_FrmCnt_Loaded;/*String formHtml = "Form could not be loaded";*****/
  String formName="";
  String  patientPortal= request.getParameter("patientPortal")==null?"":request.getParameter("patientPortal");
  String styleInline="";
  String DivClass="";
  
  try{
  	
  	String formId = request.getParameter("formId");
	formId=(formId==null)?"":formId;
	if (formId.length()>0)
	{
		formLibB.setFormLibId(EJBUtil.stringToNum(formId));
		formLibB.getFormLibDetails();
		formName=formLibB.getFormLibName();
	}
  
	FormSecDao fsdao = new FormSecDao();
	fsdao.getAllSectionsOfAForm(EJBUtil.stringToNum(formId));
    String formFmt=fsdao.getFormSecFmt().toString().substring(1, fsdao.getFormSecFmt().toString().length()-1);
    if(formFmt!=null && formFmt.contains("T"))
    {
    	styleInline="style=\"width:auto;min-width:96%;\"";
    	DivClass="popDefault custom_table_width";
    }
    else{
    	DivClass="popDefault";
    }
	
  	SaveFormDao fDao = new SaveFormDao();
  	formHtml = fDao.getFormHtmlforPreview(EJBUtil.stringToNum(formId));
  	formHtml = formHtml.replace("&amp;#39;","\'");
  	//remove the submit button as its preview mode
  //	String findStr = "<input type=\"image\" src=\"../images/jpg/Submit.gif\" align=\"absmiddle\" border=\"0\"/>";
	  String findStr="\"../images/jpg/Submit.gif\"";
	  String eSignStr="name=\"eSign\"";
  	
  	int len = findStr.length();
  	int pos = formHtml.lastIndexOf(findStr);
  	int posESig=formHtml.lastIndexOf(eSignStr);
  	int lenESig=eSignStr.length();

  	if (pos > 0)
  	{
  		StringBuffer sbuffer = new StringBuffer(formHtml);
  		sbuffer.replace(pos,pos+len,"src=\"/images/jpg/Submit.gif\" style=display:none");
  		if (posESig>0)
  		 sbuffer.replace(posESig,posESig+lenESig,"name=\"eSign\" disabled=\"true\"");
  		formHtml = sbuffer.toString();
  	}	

	//KM-#3954 (for preview mode)
	String submitStr = "onSubmit=\"return validationWithAjax()\"";
	int lngth = submitStr.length();
	int posn = formHtml.lastIndexOf(submitStr);
	if (posn > 0)
	{
		StringBuffer sb = new StringBuffer(formHtml);
		sb.replace(posn,posn+lngth,"onSubmit=\"return false\"");
		formHtml = sb.toString();
	}
  }
  catch (Exception e)
  {
  	formHtml = MC.M_FrmCnt_Loaded;/*formHtml = "Form could not be loaded";*****/
  	e.printStackTrace();
  }

%>
<%if(patientPortal.equalsIgnoreCase("Y")){ %>
<div  id="div1">
<%} else{%>
<div class="<%=DivClass%>" id="div1" <%=styleInline %>>
<%} %>
<table width="100%" > <!-- YK 26APR2011 Bug#6058 -->
		<tr>
			 <td width="40%">
			 	 <P><%=LC.L_Frm_Name%><%--Form Name*****--%>: <%=formName%></P>
			 </td>
			 </tr></table>
<%=formHtml%>
</div>

<%}else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
<%
}
%>
<div>

<jsp:include page="bottompanel.jsp" flush="true"/> 



</div>

</body>
</html>
<style>
html, body { overflow: scroll; }
.marker{
background-color:Yellow;}
.custom_table_width table{
width:inherit;
}
</style>


