<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList,java.util.HashMap,org.json.*"%>
<%@page import="com.velos.eres.service.util.EJBUtil"%>
<%@page import="com.velos.esch.business.common.ProtVisitDao"%>
<%@page import="com.velos.esch.business.common.EventAssocDao,com.velos.eres.service.util.*"%>
<%@page import="com.velos.eres.business.common.MilestoneDao"%>
<%@page import="com.velos.esch.business.common.SchCodeDao"%>
<%@page import="com.velos.eres.business.common.CodeDao"%>
<%@page import="com.velos.esch.web.eventassoc.EventAssocJB"%>
<%@page import="com.velos.eres.business.common.TeamDao"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="milestoneB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="protVisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>
<%
//DFIN-13 BK MAR-16-2011
    request.setCharacterEncoding("UTF-8");
    response.setContentType("application/json");
    // response.setContentType("text/html");

	HttpSession tSession = request.getSession(true);
    JSONObject jsObj = new JSONObject();
	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
        response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
        jsObj.put("error", new Integer(-1));
        jsObj.put("errorMsg", MC.M_UsrNot_LoggedIn/*User is not logged in.*****/);
	    out.println(jsObj.toString());
        return;
    }
    
    int accId = EJBUtil.stringToNum((String)tSession.getValue("accountId"));
    
	String studyId = request.getParameter("studyId");
	if(studyId == "" || studyId == null || studyId.equals("null") || studyId.equals("")) {
	    // A valid protocol ID is required; print an error and exit
        jsObj.put("error", new Integer(-2));
        jsObj.put("errorMsg", MC.M_StdID_Invalid/*Study ID is invalid.*****/);
	    out.println(jsObj.toString());
	    return;
	}
	//get all request ParametersPut and set
	//search parameter into the HashMap INF-22500
    HashMap<String,String> paramHMap = new HashMap<String,String>();
    String inactiveFlag 	= request.getParameter("inactiveFlag");
	String mileType 	= request.getParameter("msType");
	String msStatus 	= request.getParameter("msStatus");
	String msPayType 	= request.getParameter("msPayType");
	String calName 		= request.getParameter("calName");
	String visitName 	= request.getParameter("visitName");
	String evtName 		= request.getParameter("evtName");
	String isSearch 	= request.getParameter("isSearch");
	String datefrom 	= request.getParameter("datefrom");
	String dateto 		= request.getParameter("dateto");
	if (inactiveFlag==null) inactiveFlag="0";
	
	paramHMap.put("mileType",mileType);
	paramHMap.put("msStatus",msStatus);
	paramHMap.put("msPayType",msPayType);
	paramHMap.put("calName",calName);
	paramHMap.put("visitName",visitName);
	paramHMap.put("evtName",evtName);
	paramHMap.put("isSearch",isSearch);
	paramHMap.put("mileType",mileType);
	paramHMap.put("inactiveMiles",inactiveFlag);
	paramHMap.put("datefrom",datefrom);
	paramHMap.put("dateto",dateto);
	
	String srchStrProtocolId = request.getParameter("srchStrProtocolId");
	String strCalName = request.getParameter("strCalName");
	
    // Define page-wide variables here
    StringBuffer sb = new StringBuffer();
    String calledFrom = request.getParameter("calledFrom");

    int errorCode = 0;
    
    ArrayList milestoneIds = null, milestoneTypes = null, milestoneRuleDescs = null, 
    	milestoneCalendarIds = null, milestoneVisitIds = null, milestoneEventIds = null,
    	milestoneAmounts = null, milestoneCounts = null, milestoneUsers =null, milestoneLimits = null, milestoneHoldbacks = null;
    ArrayList milestonePaymentFors =null, milestonePaymentTypes = null, 
    	milestoneAchievedCounts = null, milestoneStatus = null, milestonePrimaryFKs=null,milestoneSecondaryFks = null,
    	milestoneRuleIds = null , milestoneDateFrom=null , milestoneDateTo=null;/* FIN-22373 09-Oct-2012 -Sudhir*/
    	ArrayList isInvoiceGenerated=null,isReconciledAmount=null;
    	
    int achMileId=0;
    String achMileCounts="";
    String isInvGenerated = "";
    String isReconAmount = "";
	// For INF-22500
    MilestoneDao milestoneDao = milestoneB.getMilestoneRowsForSearch (StringUtil.stringToNum(studyId),paramHMap);
    milestoneIds = milestoneDao.getMilestoneIds();

    int noOfMilestones = milestoneIds == null ? 0 : milestoneIds.size();   
       
    jsObj.put("error", errorCode);
    jsObj.put("errorMsg", "");
    jsObj.put("noOfMilestones", noOfMilestones);
    
    JSONObject jsMilestoneTypes = new JSONObject();
    jsMilestoneTypes.put("PM", LC.L_Patient_Status/*Patient Status*****/);
    jsMilestoneTypes.put("VM", LC.L_Visit/*Visit*****/);
    jsMilestoneTypes.put("EM", LC.L_Event/*Event*****/);
    jsMilestoneTypes.put("SM", LC.L_Study_Status/*Study Status*****/);
    jsMilestoneTypes.put("AM", LC.L_Additional/*Additional*****/);
    jsObj.put("mileTypeJSON",jsMilestoneTypes);

    { 
    	ArrayList milestoneTypeId = new ArrayList();
	    ArrayList milestoneType = new ArrayList();
	    
	    milestoneTypeId.add("PM"); milestoneType.add(LC.L_Patient_Status);
	    milestoneTypeId.add("VM"); milestoneType.add(LC.L_Visit);
	    milestoneTypeId.add("EM"); milestoneType.add(LC.L_Event);
	    milestoneTypeId.add("SM"); milestoneType.add(LC.L_Study_Status);
	    milestoneTypeId.add("AM"); milestoneType.add(LC.L_Additional);
	   
	    jsObj.put("milestoneTypeId",milestoneTypeId);
	    jsObj.put("milestoneType",milestoneType);
    }

    /*JSONArray jsMilestones = new JSONArray();
    for (int iX=0; iX<milestoneIds.size(); iX++) {
    	JSONObject jsObj1 = new JSONObject();
        jsObj1.put(String.valueOf(milestoneIds.get(iX)), milestoneRuleDescs.get(iX));
        jsMilestones.put(jsObj1);
    }
    jsObj.put("milestones", jsMilestones);*/
    
    CodeDao codeLstStatus = new CodeDao();
    ArrayList ids = new ArrayList();
    ArrayList descs = new ArrayList();
    ArrayList descsSubTypes = new ArrayList();
    
    //codeLstStatus.getCodeValues("patStatus",accId);   // changes made by shyam sunder on 07-08-2014
    codeLstStatus.getCodeValues("patStatus",accId,false);// to resolve bug#19854  (introduced new function. in CodeDao.java)
    ids = codeLstStatus.getCId();
    descs = codeLstStatus.getCDesc();   
    ids.add(0,"0");
    descs.add(0,LC.L_Select_AnOption/*Select an Option*****/);    
    jsObj.put("patStatusId",ids);
    jsObj.put("patStatus",descs);
    
    codeLstStatus.resetDao();
    // codeLstStatus.getCodeValues("studystat",accId); // changes made by shyam sunder on 07-08-2014
    codeLstStatus.getCodeValues("studystat",accId,false);// to resolve bug#19854  (introduced new function. in CodeDao.java)
    ids = codeLstStatus.getCId();
    descs = codeLstStatus.getCDesc();
    ids.add(0,"0");
    descs.add(0,LC.L_Select_AnOption/*Select an Option*****/);
    jsObj.put("studyStatusId",ids);
    jsObj.put("studyStatus",descs);
    
    codeLstStatus.resetDao();
    //codeLstStatus.getCodeValues("milepaytype");
    codeLstStatus.getCodeValuesMiles("milepaytype");
    ids = codeLstStatus.getCId();
    descs = codeLstStatus.getCDesc();
    //ids.add(0,"0");
    //descs.add(0,"Select an Option");
    jsObj.put("milePayTypeId",ids);
    jsObj.put("milePayType",descs);
    
    {
    	int id = codeLstStatus.getCodeId("milepaytype","rec");
	    jsObj.put("defaultPayTypeId",""+id);
	    jsObj.put("defaultPayType",codeLstStatus.getCodeDesc(id));	    
    }
    		
    codeLstStatus.resetDao();
   // codeLstStatus.getCodeValues("milePayfor");
   codeLstStatus.getCodeValuesMiles("milePayfor");
    ids = codeLstStatus.getCId();
    descs = codeLstStatus.getCDesc();
    ids.add(0,"0");
    descs.add(0,LC.L_Select_AnOption/*Select an Option*****/);
    jsObj.put("milePayForId",ids);
    jsObj.put("milePayFor",descs);

    ////*****SM: Study Team Role Based Milestone Statuses --START ******////
    /* Bug#10796 14-Aug-2012 -Sudhir*/
	String userIdFromSession = (String) tSession.getValue("userId");
	String roleCodePk="";

	if (EJBUtil.stringToNum(studyId) > 0){
		ArrayList tId = new ArrayList();

		TeamDao teamRoleDao = new TeamDao();
		teamRoleDao.getUserStudyTeamRole(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));

		ArrayList arRoles = new ArrayList();
		arRoles = teamRoleDao.getTeamRoleIds();

		if (arRoles != null && arRoles.size() >0 ){
			roleCodePk = (String) arRoles.get(0);

			if (StringUtil.isEmpty(roleCodePk)){
				roleCodePk="";
			}
		} else {
			roleCodePk ="";
		}
	} else {
		 roleCodePk ="";
	}

    /* Code removed Bug#10796 18-Sep-2012 -Yogesh Kumar*/

    /***-- To Store all the Codelist values---*/
	    ArrayList codelstIds = new ArrayList();
	    ArrayList codelstDesc = new ArrayList();
	    ArrayList codelstSubTypes = new ArrayList();
		codeLstStatus.resetDao();    
		codeLstStatus.getCodeValues("milestone_stat",accId,false);
		codelstIds = codeLstStatus.getCId();
		codelstDesc = codeLstStatus.getCDesc();
		codelstSubTypes = codeLstStatus.getCSubType();
    
	    jsObj.put("mileStatusId",codelstIds);
	    jsObj.put("mileStatus",codelstDesc);
	    jsObj.put("mileStatusSubType",codelstSubTypes);
    /***-- To Store all the Codelist values For active and Inactive---*/

	jsObj.put("mileDifStatusId",codelstIds);
	jsObj.put("mileDifStatus",codelstDesc);   
           

    codeLstStatus.resetDao();
    // codeLstStatus.getCodeValues("EM");
  	codeLstStatus.getCodeValuesMiles("EM");
    ids = codeLstStatus.getCId();
    descs = codeLstStatus.getCDesc();
    ids.add(0,"0");
    descs.add(0,LC.L_Select_AnOption/*Select an Option*****/);
    jsObj.put("EM_mileRuleId",ids);
    jsObj.put("EM_mileRule",descs);
    
    codeLstStatus.resetDao();
    //codeLstStatus.getCodeValues("VM");
   	codeLstStatus.getCodeValuesMiles("VM");
    ids = codeLstStatus.getCId();
    descs = codeLstStatus.getCDesc();
    ids.add(0,"0");
    descs.add(0,LC.L_Select_AnOption/*Select an Option*****/);
    jsObj.put("VM_mileRuleId",ids);
    jsObj.put("VM_mileRule",descs);
    
    SchCodeDao sCodeLstStatus = new SchCodeDao();
    //sCodeLstStatus.getCodeValues("eventstatus",accId);
    sCodeLstStatus.getCodeValuesMilesCust("eventstatus",accId);
    ids = sCodeLstStatus.getCId();
    descs = sCodeLstStatus.getCDesc();
    ids.add(0,"0");
    descs.add(0,LC.L_Select_AnOption/*Select an Option*****/);
    jsObj.put("eventStatusId",ids);
    jsObj.put("eventStatus",descs);
 
	ArrayList studyProtocols = new ArrayList();
	ArrayList studyProtIds = new ArrayList();
	ArrayList studyProtTypes = new ArrayList();
	ArrayList studyVisits = new ArrayList();
	ArrayList studyVisitIds = new ArrayList();
	ArrayList studyEvents = new ArrayList();
	ArrayList studyEventNames = new ArrayList();
	ArrayList studyEventIds = new ArrayList();
	
 	//EventAssocDao eventAssocDao = eventassocB.getStudyProtsActive(EJBUtil.stringToNum(studyId));

 	EventAssocDao eventAssocDao = eventassocB.getStudyProts(EJBUtil.stringToNum(studyId),"ND");
 	//ND:- all calendars except deactivated,apr-04-11,Bk
 	studyProtocols = eventAssocDao.getNames();
 	studyProtIds = eventAssocDao.getEvent_ids();
 	
 	for (int iZ =0; iZ < studyProtIds.size();iZ++){
 		studyProtTypes.add("P");
 	}
	studyProtocols.add(0,"***"+LC.L_Patient_Calendars/*Patient Calendars******/+"***");
	studyProtIds.add(0,"0");
	studyProtTypes.add(0,"");
	
	EventAssocDao eventAssocDaoAdmin = eventassocB.getStudyAdminProts(EJBUtil.stringToNum(studyId),"ND");
	//ND:- all calendars except deactivated,apr-04-11,Bk

 	studyProtocols.add("***"+LC.L_Admin_Cals/*Admin Calendars*****/+"***");
 	studyProtIds.add("0");
 	studyProtTypes.add("");
 	
 	//studyProtocols.addAll(eventAssocDaoAdmin.getNames());
 	for (int iZ =0; iZ < eventAssocDaoAdmin.getNames().size();iZ++){
 		studyProtocols.add(eventAssocDaoAdmin.getNames().get(iZ)+" ");
 		studyProtTypes.add("A");
 	}
 	studyProtIds.addAll(eventAssocDaoAdmin.getEvent_ids());
 	
 	ArrayList protIds = studyProtIds;
 	
 	ProtVisitDao visitdao = new ProtVisitDao();
 	 	
 	for (int iProt=0; iProt < protIds.size(); iProt++){
 		int protId = EJBUtil.stringToNum(protIds.get(iProt).toString());
 		if(protId > 0){
	 		visitdao = new ProtVisitDao();
	 		visitdao = protVisitB.getProtocolVisits(protId);
	 		 		
	 		ArrayList visitIds = visitdao.getVisit_ids();
	 		ArrayList visitNames = visitdao.getNames();
	 		JSONArray jsVisits = new JSONArray();
	 		JSONArray validVisits = new JSONArray();
	 		
	 		EventAssocDao eventDao = new EventAssocDao();
	 	    for (int iVisit=0; iVisit<visitIds.size(); iVisit++) {
	 	    	String visitId = visitIds.get(iVisit).toString();;
	 	    	
	 	    	eventDao = new EventAssocDao();
	 	    	eventDao.getAllProtSelectedEvents(protId,visitId,"");
	 	    	
	 	    	ArrayList eventIds = eventDao.getEvent_ids();
	 	 		ArrayList eventNames = eventDao.getNames();
	 	 		
	 	 		if (eventIds != null){
	 	 			eventIds.remove(0);
	 	 			eventNames.remove(0);	 	 		    
		 	 		JSONArray jsEvents = new JSONArray();
		 	 		for (int iEvent=0; iEvent<eventIds.size(); iEvent++) {
		 	 			String eventId = eventIds.get(iEvent).toString();
		 	 			
		 	 			if (EJBUtil.stringToNum(eventId) > 0 ){
			 	 			JSONObject jsObjEvent = new JSONObject();
			 	 			jsObjEvent.put(eventId, ((String)eventNames.get(iEvent)).length()>50 ?((String)eventNames.get(iEvent)).substring(0,50).concat("..."):(String)eventNames.get(iEvent));
			 	 			jsEvents.put(jsObjEvent);
		 	 			}
		 	 		}
		 	 		jsObj.put("v"+visitId, jsEvents);
		 	 		
		 	 		studyEventIds.addAll(eventIds);
		 	 		studyEvents.addAll(eventNames); 		
	 	 		}
	 	 		
	 	    	JSONObject jsObjVisit = new JSONObject();
	 	    	jsObjVisit.put(visitId, visitNames.get(iVisit));
	 	    	jsVisits.put(jsObjVisit);
	 	    }
	 	   jsObj.put("c"+protId, jsVisits);
	 	   studyVisits.addAll(visitNames);
	 	   studyVisitIds.addAll(visitIds);
 		}
 	}
	JSONArray jsDefColArray = new JSONArray();
 	{
    	JSONObject jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "recNum");
    	jsObjTemp1.put("label", LC.L_Serial/*Serial*****/+" #");
    	jsObjTemp1.put("type", "integer");
    	jsObjTemp1.put("sortable", "true");
    	jsObjTemp1.put("minWidth", 47);
    	//jsObjTemp1.put("hidden", "true");
    	jsDefColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileSeq");
    	jsObjTemp1.put("label", "mileSeq");
    	jsObjTemp1.put("hidden", "true");
    	jsDefColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileId");
    	jsObjTemp1.put("label", "mileId");
    	jsObjTemp1.put("hidden", "true");
    	jsDefColArray.put(jsObjTemp1);

    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileTypeId");
    	jsObjTemp1.put("label",LC.L_Mstone_TypeID/*Milestone Type Id*****/); 	
    	jsObjTemp1.put("hidden", "true");
    	jsDefColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileType");
    	jsObjTemp1.put("label",LC.L_Milestone_Type/*Milestone Type*****/);
    	jsDefColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "amount");
    	jsObjTemp1.put("label",LC.L_Amount/*Amount*****/);
    	jsObjTemp1.put("type", "number");
    	jsObjTemp1.put("formatter", "customNumber");
    	jsDefColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "holdBack");
    	jsObjTemp1.put("label",LC.L_Holdback);
    	jsObjTemp1.put("type", "integer");
    	jsObjTemp1.put("formatter", "customInteger");
    	jsDefColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "limit");
    	jsObjTemp1.put("label",LC.L_Limit/*Limit*****/);
    	jsObjTemp1.put("type", "integer");
    	jsObjTemp1.put("formatter", "customInteger");
    	jsDefColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payTypeId");
    	jsObjTemp1.put("label", LC.L_Pment_TypeID/*Payment Type Id*****/); 	
    	jsObjTemp1.put("hidden", "true");
    	jsDefColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payType");
    	jsObjTemp1.put("label",LC.L_Payment_Type/*Payment Type*****/);
    	jsDefColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payForId");
    	jsObjTemp1.put("label",LC.L_Pment_ForID/*Payment For Id*****/); 	
    	jsObjTemp1.put("hidden", "true");
    	jsDefColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payFor");
    	jsObjTemp1.put("label", LC.L_Payment_For/*Payment For*****/);
    	jsDefColArray.put(jsObjTemp1);    	
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileStatusId");
    	jsObjTemp1.put("label",LC.L_Mstone_StatID/*Milestone Status Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsDefColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileStatus");
    	jsObjTemp1.put("label", LC.L_Mstone_Status/*Milestone Status*****/+" <FONT class='mandatory'><sup>*</sup></FONT>");
    	jsDefColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileAchievedCount");
    	jsObjTemp1.put("label", LC.L_MstoneAch_Cnt/*Milestone Achieved count*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsDefColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileStaticStatusId");
    	jsObjTemp1.put("label",LC.L_MstoneStatic_StatID/*Milestone Static Status Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsDefColArray.put(jsObjTemp1);    	
    	
    
    	
    	
    }
    
    JSONArray jsColArray = new JSONArray();
    int itr =0;
    while (itr < jsDefColArray.length()){
    	jsColArray.put(jsDefColArray.get(itr));
    	itr++;
    }
    {   
    	JSONObject jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "patCount");
    	jsObjTemp1.put("label", LC.L_Pat_Count/*Patient Count*****/+" <FONT class='mandatory'><sup>*</sup></FONT>");
    	jsObjTemp1.put("type", "integer");
    	jsObjTemp1.put("formatter", "customInteger");
    	jsColArray = insertJSON(5,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "patStatusId");
    	jsObjTemp1.put("label", LC.L_PatStat_ID/*Patient Status Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray = insertJSON(6,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "patStatus");
    	jsObjTemp1.put("label", LC.L_Patient_Status/*Patient Status*****/+" <FONT class='mandatory'><sup>*</sup></FONT>");
    	jsColArray = insertJSON(7,jsObjTemp1,jsColArray);
    	
    	/* Fin-22373 28-Sep-2012 Sudhir */
    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "datefrom");
    	jsObjTemp1.put("label", LC.L_Date_From);
		jsObjTemp1.put("type", "date");
    	jsColArray = insertJSON(16,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "dateto");
    	jsObjTemp1.put("label", LC.L_Date_To);
		jsObjTemp1.put("type", "date");
    	jsColArray = insertJSON(17,jsObjTemp1,jsColArray);
    	
		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "isInvoiceGenerated");
		jsObjTemp1.put("label", "isInvoiceGenerated");
		jsObjTemp1.put("hidden", "true");
		jsColArray = insertJSON(19, jsObjTemp1, jsColArray);

		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "isReconciledAmount");
		jsObjTemp1.put("label", "isReconciledAmount");
		jsObjTemp1.put("hidden", "true");
		jsColArray = insertJSON(20, jsObjTemp1, jsColArray);
		
    	// Select icons : INF-22500
    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "selectAll_PM");
    	jsObjTemp1.put("label", LC.L_Select+" <input type='checkbox' name='selectAll_PM' id='selectAll_PM' onclick='VELOS.milestoneGrid.selectAll(\"PM\");'/>"); // Bug#6323 27-Mar-2012 Ankit
    	jsObjTemp1.put("action", "selectAll_PM");
    	jsColArray.put(jsObjTemp1);
    	
    	// action icons
    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "delete_PM");
    	jsObjTemp1.put("label", LC.L_Delete+" <input type='checkbox' name='deleteSelected_PM' id='deleteSelected_PM' onclick='VELOS.milestoneGrid.deleteAll(\"PM\");'/>"); // Bug#6323 27-Mar-2012 Ankit
    	jsObjTemp1.put("action", "delete_PM");
    	jsColArray.put(jsObjTemp1);
    	
    }
    jsObj.put("PM_colArray", jsColArray);

	jsColArray = new JSONArray();
    itr =0;
    while (itr < jsDefColArray.length()){
    	jsColArray.put(jsDefColArray.get(itr));
    	itr++;
    }
    {   	   	
    	JSONObject jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "calendarType");
    	jsObjTemp1.put("label", LC.L_Cal_Type/*Calendar Type*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray = insertJSON(5,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "calendarId");
    	jsObjTemp1.put("label", LC.L_Cal_ID/*Calendar Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray = insertJSON(6,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "calendar");
    	jsObjTemp1.put("label", LC.L_Calendar/*Calendar*****/+" <FONT class='mandatory'><sup>*</sup></FONT>");
    	jsColArray = insertJSON(7,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "visitId");
    	jsObjTemp1.put("label", LC.L_Visit_ID/*Visit Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray = insertJSON(8,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "visit");
    	jsObjTemp1.put("label", LC.L_Visit/*Visit*****/+" <FONT class='mandatory'><sup>*</sup></FONT>");
    	jsColArray = insertJSON(9,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileRuleId");
    	jsObjTemp1.put("label", LC.L_Mstone_RuleID/*Milestone Rule Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray = insertJSON(10,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileRule");
    	jsObjTemp1.put("label", LC.L_Milestone_Rule/*Milestone Rule*****/+" <FONT class='mandatory'><sup>*</sup></FONT>");
    	jsColArray = insertJSON(11,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "evtStatusId");
    	jsObjTemp1.put("label", LC.L_EvtStat_ID/*Event Status Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray = insertJSON(12,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "evtStatus");
    	jsObjTemp1.put("label", LC.L_Event_Status/*Event Status*****/+" <FONT class='mandatory'><sup>*</sup></FONT>");
    	jsColArray = insertJSON(13,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "patCount");
    	jsObjTemp1.put("label",LC.L_Pat_Count/*Patient Count*****/);
    	jsObjTemp1.put("type", "integer");
    	jsObjTemp1.put("formatter", "customInteger");
    	jsColArray = insertJSON(14,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "patStatusId");
    	jsObjTemp1.put("label",LC.L_PatStat_ID/*Patient Status Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray = insertJSON(15,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "patStatus");
    	jsObjTemp1.put("label", LC.L_Patient_Status/*Patient Status*****/);
    	jsColArray = insertJSON(16,jsObjTemp1,jsColArray);
    	
    	/* Fin-22373 28-Sep-2012 Sudhir */
    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "datefrom");
    	jsObjTemp1.put("label", LC.L_Date_From);
		jsObjTemp1.put("type", "date");
    	jsColArray = insertJSON(25,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "dateto");
    	jsObjTemp1.put("label", LC.L_Date_To);
		jsObjTemp1.put("type", "date");
    	jsColArray = insertJSON(26,jsObjTemp1,jsColArray);
    	
		jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "isReconciledAmount");
    	jsObjTemp1.put("label", "isReconciledAmount");
    	jsObjTemp1.put("hidden", "true");
    	jsColArray = insertJSON(28,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "isInvoiceGenerated");
    	jsObjTemp1.put("label", "isInvoiceGenerated");
    	jsObjTemp1.put("hidden", "true");
    	jsColArray = insertJSON(29,jsObjTemp1,jsColArray);
    	// Select icons : INF-22500
    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "selectAll_VM");
    	jsObjTemp1.put("label", LC.L_Select+" <input type='checkbox' name='selectAll_VM' id='selectAll_VM' onclick='VELOS.milestoneGrid.selectAll(\"VM\");'/>"); // Bug#6323 27-Mar-2012 Ankit
    	jsObjTemp1.put("action", "selectAll_VM");
    	jsColArray.put(jsObjTemp1);
    	
    	// action icons
    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "delete_VM");
    	jsObjTemp1.put("label", LC.L_Delete+" <input type='checkbox' name='deleteSelected_VM' id='deleteSelected_VM' onclick='VELOS.milestoneGrid.deleteAll(\"VM\");'/>"); // Bug#6323 27-Mar-2012 Ankit
    	jsObjTemp1.put("action", "delete_VM");
    	jsColArray.put(jsObjTemp1);
    	
    }
   jsObj.put("VM_colArray", jsColArray);
    
    jsColArray = new JSONArray();
    itr =0;
    while (itr < jsDefColArray.length()){
    	jsColArray.put(jsDefColArray.get(itr));
    	itr++;
    }
    {
    	JSONObject jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "calendarType");
    	jsObjTemp1.put("label", LC.L_Cal_Type/*Calendar Type*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray = insertJSON(5,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "calendarId");
    	jsObjTemp1.put("label", LC.L_Cal_ID/*Calendar Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray = insertJSON(6,jsObjTemp1,jsColArray);    	
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "calendar");
    	jsObjTemp1.put("label",LC.L_Calendar/*Calendar*****/+" <FONT class='mandatory'><sup>*</sup></FONT>");
    	jsColArray = insertJSON(7,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "visitId");
    	jsObjTemp1.put("label", LC.L_Visit_ID/*Visit Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray = insertJSON(8,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "visit");
    	jsObjTemp1.put("label", LC.L_Visit/*Visit*****/+" <FONT class='mandatory'><sup>*</sup></FONT>");
    	jsColArray = insertJSON(9,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "eventId");
    	jsObjTemp1.put("label",LC.L_EventId/*Event Id*****/);
    	jsObjTemp1.put("hidden", "true");    	
    	jsColArray = insertJSON(10,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "event");
    	jsObjTemp1.put("label", LC.L_Event/*Event*****/+" <FONT class='mandatory'><sup>*</sup></FONT>");
    	jsColArray = insertJSON(11,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileRuleId");
    	jsObjTemp1.put("label", LC.L_Mstone_RuleID/*Milestone Rule Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray = insertJSON(12,jsObjTemp1,jsColArray);

    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileRule");
    	jsObjTemp1.put("label", LC.L_Milestone_Rule/*Milestone Rule*****/+" <FONT class='mandatory'><sup>*</sup></FONT>");
    	jsColArray = insertJSON(13,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "evtStatusId");
    	jsObjTemp1.put("label", LC.L_EvtStat_ID/*Event Status Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray = insertJSON(14,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "evtStatus");
    	jsObjTemp1.put("label", LC.L_Event_Status/*Event Status*****/+" <FONT class='mandatory'><sup>*</sup></FONT>");
    	jsColArray = insertJSON(15,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "patCount");
    	jsObjTemp1.put("label",LC.L_Pat_Count/*Patient Count*****/);
    	jsObjTemp1.put("type", "integer");
    	jsObjTemp1.put("formatter", "customInteger");
    	jsColArray = insertJSON(16,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "patStatusId");
    	jsObjTemp1.put("label",LC.L_PatStat_ID/*Patient Status Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray = insertJSON(17,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "patStatus");
    	jsObjTemp1.put("label", LC.L_Patient_Status/*Patient Status*****/);
    	jsColArray = insertJSON(18,jsObjTemp1,jsColArray);
    	
    	/* Fin-22373 28-Sep-2012 Sudhir */
    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "datefrom");
    	jsObjTemp1.put("label", LC.L_Date_From);   
		jsObjTemp1.put("type", "date");
    	jsColArray = insertJSON(27,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "dateto");
    	jsObjTemp1.put("label", LC.L_Date_To); 
		jsObjTemp1.put("type", "date");
    	jsColArray = insertJSON(28,jsObjTemp1,jsColArray);
    	
		jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "isReconciledAmount");
    	jsObjTemp1.put("label", "isReconciledAmount");
    	jsObjTemp1.put("hidden", "true");
    	jsColArray = insertJSON(29,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "isInvoiceGenerated");
    	jsObjTemp1.put("label", "isInvoiceGenerated");
    	jsObjTemp1.put("hidden", "true");
    	jsColArray = insertJSON(30,jsObjTemp1,jsColArray);
    	// Select icons : INF-22500
    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "selectAll_EM");
    	jsObjTemp1.put("label", LC.L_Select+" <input type='checkbox' name='selectAll_EM' id='selectAll_EM' onclick='VELOS.milestoneGrid.selectAll(\"EM\");'/>"); // Bug#6323 27-Mar-2012 Ankit
    	jsObjTemp1.put("action", "selectAll_EM");
    	jsColArray.put(jsObjTemp1);
    	
    	// action icons
    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "delete_EM");
    	jsObjTemp1.put("label", LC.L_Delete+" <input type='checkbox' name='deleteSelected_EM' id='deleteSelected_EM' onclick='VELOS.milestoneGrid.deleteAll(\"EM\");'/>"); // Bug#6323 27-Mar-2012 Ankit
    	jsObjTemp1.put("action", "delete_EM");
    	jsColArray.put(jsObjTemp1);
    }
    jsObj.put("EM_colArray", jsColArray);
    
    jsColArray = new JSONArray();
    itr =0;
    while (itr < jsDefColArray.length()){
    	jsColArray.put(jsDefColArray.get(itr));
    	itr++;
    }
    {
    	JSONObject jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "stdStatusId");
    	jsObjTemp1.put("label",LC.L_StdStat_ID/*Study Status Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray = insertJSON(5,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "stdStatus");
    	jsObjTemp1.put("label", LC.L_Study_Status/*Study Status*****/+" <FONT class='mandatory'><sup>*</sup></FONT>");
    	jsColArray = insertJSON(6,jsObjTemp1,jsColArray);
    	
    	/* Fin-22373 28-Sep-2012 Sudhir */
    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "datefrom");
    	jsObjTemp1.put("label", LC.L_Date_From);
		jsObjTemp1.put("type", "date");
    	jsColArray = insertJSON(15,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "dateto");
    	jsObjTemp1.put("label", LC.L_Date_To);    	
		jsObjTemp1.put("type", "date");
    	jsColArray = insertJSON(16,jsObjTemp1,jsColArray);
    	
	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "isReconciledAmount");
    	jsObjTemp1.put("label", "isReconciledAmount");
    	jsObjTemp1.put("hidden", "true");
    	jsColArray = insertJSON(19,jsObjTemp1,jsColArray);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "isInvoiceGenerated");
    	jsObjTemp1.put("label", "isInvoiceGenerated");
    	jsObjTemp1.put("hidden", "true");
    	jsColArray = insertJSON(20,jsObjTemp1,jsColArray);
    	// Select icons : INF-22500
    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "selectAll_SM");
    	jsObjTemp1.put("label", LC.L_Select+" <input type='checkbox' name='selectAll_SM' id='selectAll_SM' onclick='VELOS.milestoneGrid.selectAll(\"SM\");'/>"); // Bug#6323 27-Mar-2012 Ankit
    	jsObjTemp1.put("action", "selectAll_SM");
    	jsColArray.put(jsObjTemp1);
    	
    	// action icons
    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "delete_SM");
    	jsObjTemp1.put("label", LC.L_Delete+" <input type='checkbox' name='deleteSelected_SM' id='deleteSelected_SM' onclick='VELOS.milestoneGrid.deleteAll(\"SM\");'/>"); // Bug#6323 27-Mar-2012 Ankit
    	jsObjTemp1.put("action", "delete_SM");
    	jsColArray.put(jsObjTemp1);
    	
    	//jsColArray.put(jsObjTemp1);
    }
    jsObj.put("SM_colArray", jsColArray);
    
    jsColArray = new JSONArray();
    itr =0;
    while (itr < jsDefColArray.length()){
    	if(itr != 7){
    	jsColArray.put(jsDefColArray.get(itr));
    	}
    	itr++;
    }
    {
    	JSONObject jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileDesc");
    	jsObjTemp1.put("label", LC.L_Milestone_Description/*Milestone Description*****/+" <FONT class='mandatory'><sup>*</sup></FONT>");
    	jsObjTemp1.put("type", "varchar");
    	jsObjTemp1.put("formatter", "customVarchar");
    	//jsColArray.put(jsObjTemp1);
    	jsColArray = insertJSON(5,jsObjTemp1,jsColArray);
    	
	jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "isReconciledAmount");
		jsObjTemp1.put("label", "isReconciledAmount");
		jsObjTemp1.put("hidden", "true");
		jsColArray = insertJSON(6, jsObjTemp1, jsColArray);

		jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "isInvoiceGenerated");
		jsObjTemp1.put("label", "isInvoiceGenerated");
		jsObjTemp1.put("hidden", "true");
		jsColArray = insertJSON(7, jsObjTemp1, jsColArray);
		
    	// Select icons : INF-22500
    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "selectAll_AM");
    	jsObjTemp1.put("label", LC.L_Select+" <input type='checkbox' name='selectAll_AM' id='selectAll_AM' onclick='VELOS.milestoneGrid.selectAll(\"AM\");'/>"); 
    	jsObjTemp1.put("action", "selectAll_AM");
    	jsColArray.put(jsObjTemp1);
    	
    	// action icons
    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "delete_AM");
    	jsObjTemp1.put("label", LC.L_Delete+" <input type='checkbox' name='deleteSelected_AM' id='deleteSelected_AM' onclick='VELOS.milestoneGrid.deleteAll(\"AM\");'/>"); 
    	jsObjTemp1.put("action", "delete_AM");
    	jsColArray.put(jsObjTemp1);
	
    }
    jsObj.put("AM_colArray", jsColArray);

	milestoneTypes= milestoneDao.getMilestoneTypes();
	milestoneRuleDescs= milestoneDao.getMilestoneRuleDescs();
	milestoneRuleIds = milestoneDao.getMilestoneRuleIds();
	
	milestoneCalendarIds= milestoneDao.getMilestoneCalIds();
	milestoneVisitIds= milestoneDao.getFkVisits();
	milestoneEventIds= milestoneDao.getMilestoneEvents();
	
	milestoneAmounts=milestoneDao.getMilestoneAmounts();
	milestoneCounts= milestoneDao.getMilestoneCounts();
	milestoneUsers= milestoneDao.getMilestoneUsers();
	milestoneAchievedCounts= milestoneDao.getMilestoneAchievedCounts();
	milestonePaymentTypes = milestoneDao.getMilePaymentTypeIds();
	milestonePaymentFors = milestoneDao.getMilePaymentForIds();
	milestoneLimits = milestoneDao.getMilestoneLimits();
	
	milestoneHoldbacks = milestoneDao.getHoldBack();
	
	//Milestone Status
	milestoneStatus = milestoneDao.getMilestoneStatFK();
	//Study/Patient/event Status
	milestonePrimaryFKs = milestoneDao.getMilePrimaryStatusPK();
	//secondary (patient status)
	milestoneSecondaryFks = milestoneDao.getMileSecondaryStatusPK();
	/* FIN-22373 09-Oct-2012 -Sudhir*/
	 milestoneDateFrom=milestoneDao.getMilestoneDateFrom();
	 milestoneDateTo=milestoneDao.getMilestoneDateTo();
	 
	isInvoiceGenerated=milestoneDao.getIsInvoiceGenerated();
	isReconciledAmount=milestoneDao.getIsReconciledAmount();
	
	SchCodeDao schCodeLst = new SchCodeDao();
    CodeDao codeLst = new CodeDao();
	ArrayList PM_dataList = new ArrayList();
	ArrayList VM_dataList = new ArrayList();
	ArrayList EM_dataList = new ArrayList();
	ArrayList SM_dataList = new ArrayList();
	ArrayList AM_dataList = new ArrayList();
	ArrayList ALL_MileStatDetails = new ArrayList();
	
	int mileSeq =0;int aRecNum=0,pRecNum =0,sRecNum= 0,eRecNum =0,vRecNum = 0;
	/* Fetching codelst based on role*/
	codeLstStatus.resetDao();
	codeLstStatus.getCodeValuesForStudyRole("milestone_stat",roleCodePk,false);
	ArrayList codelstIdsRl = new ArrayList();
    	ArrayList codelstDescRl = new ArrayList();
    	codelstIdsRl = codeLstStatus.getCId();
    	codelstDescRl = codeLstStatus.getCDesc();
	    jsObj.put("mileStatusIdRl",codelstIdsRl);
	    jsObj.put("mileStatusRl",codelstDescRl);
    
    for (int iX=0; iX<milestoneIds.size(); iX++) {   	
        if (((Integer)milestoneIds.get(iX)).intValue() < 1) { continue; }
    	String milestoneId = milestoneIds.get(iX).toString();
     
        HashMap map1 = new HashMap();
        HashMap milestoneStatIdDesc = new HashMap();
        HashMap milestausIdsSatus = new HashMap();
        mileSeq++;
		map1.put("mileSeq", ""+mileSeq);
		map1.put("mileId", milestoneIds.get(iX));
         
        String mileTypeId = (String)milestoneTypes.get(iX);
        String milestoneType = "";
               
       // map1.put("mileRule", milestoneRuleDescs.get(iX));
    	if (mileTypeId.equals("VM") || mileTypeId.equals("EM")){
	        Integer calendarId = EJBUtil.stringToInteger((String)milestoneCalendarIds.get(iX));
	        int calendarIndex = studyProtIds.indexOf(calendarId);
	        if (calendarIndex >= 0){
	         	map1.put("calendarId", calendarId);
	         	map1.put("calendar", studyProtocols.get(calendarIndex));
	         	map1.put("calendarType", studyProtTypes.get(calendarIndex));	         	
	        }else{
	       		map1.put("calendarId", calendarId);
	       		
	       		//find offline calendar's name
	       		EventAssocDao eventDao = new EventAssocDao();
	        	eventDao.getAllProtAndEvents(calendarId.intValue());
	         	map1.put("calendar", eventDao.getNames().get(0));

	         	String calendarType = (String)eventDao.getProtocolAssocTo().get(0);
	         	calendarType = ("P".equals(calendarType))? calendarType : "A";
	         	map1.put("calendarType", calendarType);
	         	eventDao.resetObject();
	        }
	         
	        Integer visitId = EJBUtil.stringToInteger((String)milestoneVisitIds.get(iX));
	        if (visitId.equals(EJBUtil.stringToInteger("-1"))){//FIX 6039
	        	map1.put("visitId", visitId);
	        	map1.put("visit", "All");
	        }else{
		        int visitIndex = studyVisitIds.indexOf(visitId);
		        if (visitIndex > 0){
		         	map1.put("visitId", visitId);
		        	map1.put("visit", studyVisits.get(visitIndex));
		        }else{
		        	map1.put("visitId", milestoneVisitIds.get(iX));
		       		
		       		//find offline calendar's visit name
		       		protVisitB.setVisit_id(visitId.intValue());
		       		protVisitB.getProtVisitDetails();
		         	map1.put("visit", protVisitB.getName());
		        }
	        }
	        Integer eventId = EJBUtil.stringToInteger((String)milestoneEventIds.get(iX));
	        int eventIndex = studyEventIds.indexOf(eventId);
	        if (eventIndex > 0){
	         	map1.put("eventId", eventId);
	         	if(studyEvents.get(eventIndex).toString().length()>50){
		         	map1.put("event", studyEvents.get(eventIndex).toString().substring(0,50)+"<span onmouseover='fnGetEventNameMOver("+eventId+");' onmouseout='return nd();'>...</span>");
	         		studyEventNames.add(studyEvents.get(eventIndex).toString().substring(0,50)+"...");
	         	}
	         	else{
	         		map1.put("event", studyEvents.get(eventIndex));
	         		studyEventNames.add(studyEvents.get(eventIndex));
	         		}
	        }else{
	       		map1.put("eventId", eventId);
	       		
	       		//find offline calendar's event name
	       		EventAssocJB assocJB = new EventAssocJB();
	       		assocJB.setEvent_id(eventId.intValue());
	       		assocJB.getEventAssocDetails();
	       		if(assocJB.getName()!=null)
	         	map1.put("event", assocJB.getName().length()>50?assocJB.getName().replaceAll("\n","").replaceAll("\r", "").substring(0,50)+"<span onmouseover='fnGetEventNameMOver("+eventId+");' onmouseout='return nd();'>...</span>":assocJB.getName().replaceAll("\n","").replaceAll("\r", ""));
	       		else
	       			map1.put("event", assocJB.getName());
	        }
    	}
    	
    	int id =0;
        map1.put("mileRuleId", milestoneRuleIds.get(iX));
        id = EJBUtil.stringToNum(milestoneRuleIds.get(iX).toString());
        if (id > 0)
         	map1.put("mileRule", codeLst.getCodeDescription(id));
    	
        map1.put("payTypeId", milestonePaymentTypes.get(iX));
        id = EJBUtil.stringToNum(milestonePaymentTypes.get(iX).toString());
        if (id > 0)
         	map1.put("payType", codeLst.getCodeDescription(id));
         
        map1.put("payForId", milestonePaymentFors.get(iX));
        id = EJBUtil.stringToNum(milestonePaymentFors.get(iX).toString());
        if (id > 0)
         	map1.put("payFor", codeLst.getCodeDescription(id));
         
        map1.put("amount", milestoneAmounts.get(iX));
        
        map1.put("holdBack", milestoneHoldbacks.get(iX));
        
        map1.put("limit", milestoneLimits.get(iX));      
        
         
        map1.put("patCount", milestoneCounts.get(iX));
         
        map1.put("mileStatusId", milestoneStatus.get(iX));
        id = EJBUtil.stringToNum(milestoneStatus.get(iX).toString());
        if (id > 0)
         	map1.put("mileStatus", codeLst.getCodeDescription(id));
        
        map1.put("mileStaticStatusId", milestoneStatus.get(iX));
		
        map1.put("mileAchievedCount", milestoneAchievedCounts.get(iX));  
        
        achMileId = EJBUtil.stringToNum(milestoneStatus.get(iX).toString());
        achMileCounts= milestoneAchievedCounts.get(iX).toString();
        isInvGenerated = isInvoiceGenerated.get(iX).toString();
        isReconAmount = isReconciledAmount.get(iX).toString();
        /* Bug#10796 18-Sep-2012 -Yogesh Kumar*/
        /*Milesonte status for existing Milestones*/
        codeLstStatus.resetDao();
        codeLstStatus.getCodeValuesForStudyRole("milestone_stat",roleCodePk,false);	
    	codeLstStatus.toPullDownMilestoneCodeLst(mileTypeId, achMileId, achMileCounts, isInvGenerated, isReconAmount,false);
    	milestoneStatIdDesc.put("codeId",codeLstStatus.getMileStatusIds());
    	milestoneStatIdDesc.put("codeDesc",codeLstStatus.getMileStatusIdescs());
    	milestausIdsSatus.put("mileStatusId",milestoneIds.get(iX).toString());
    	milestausIdsSatus.put("mileStatuses",milestoneStatIdDesc);
    	ALL_MileStatDetails.add(milestausIdsSatus);

    	/* FIN-22373 09-Oct-2012 -Sudhir*/
    	/*Milesonte status for existing Milestones*/
		map1.put("datefrom",  (milestoneDateFrom.get(iX)!=null)?milestoneDateFrom.get(iX):"");        
		map1.put("dateto", (milestoneDateTo.get(iX)!=null)?milestoneDateTo.get(iX):"");
		map1.put("isReconciledAmount",  (isReconciledAmount.get(iX)!=null)?isReconciledAmount.get(iX):"");        
		map1.put("isInvoiceGenerated", (isInvoiceGenerated.get(iX)!=null)?isInvoiceGenerated.get(iX):"");

        if (mileTypeId.equals("PM")){
        	pRecNum ++;
        	map1.put("patStatusId", milestonePrimaryFKs.get(iX));
            id = EJBUtil.stringToNum(milestonePrimaryFKs.get(iX).toString());
            if (id > 0)
             	map1.put("patStatus", codeLst.getCodeDescription(id));
        	milestoneType = LC.L_Patient_Status;
            map1.put("mileTypeId", mileTypeId);
            map1.put("mileType", milestoneType);
            map1.put("recNum", ""+pRecNum);
            PM_dataList.add(map1);
        } else if (mileTypeId.equals("VM")){
        	vRecNum++;
        	map1.put("patStatusId", milestoneSecondaryFks.get(iX));
            id = EJBUtil.stringToNum(milestoneSecondaryFks.get(iX).toString());
            if (id > 0)
             	map1.put("patStatus", codeLst.getCodeDescription(id));
            
            map1.put("evtStatusId", milestonePrimaryFKs.get(iX));
            id = EJBUtil.stringToNum(milestonePrimaryFKs.get(iX).toString());
            if (id > 0)
             	map1.put("evtStatus", schCodeLst.getCodeDescription(id));
         	milestoneType = LC.L_Visit;
         	map1.put("mileTypeId", mileTypeId);
            map1.put("mileType", milestoneType);
            map1.put("recNum",""+vRecNum);
            VM_dataList.add(map1);
        } else if (mileTypeId.equals("EM")){
        	eRecNum++;
        	map1.put("patStatusId", milestoneSecondaryFks.get(iX));
            id = EJBUtil.stringToNum(milestoneSecondaryFks.get(iX).toString());
            if (id > 0)
             	map1.put("patStatus", codeLst.getCodeDescription(id));
            
            map1.put("evtStatusId", milestonePrimaryFKs.get(iX));
            id = EJBUtil.stringToNum(milestonePrimaryFKs.get(iX).toString());
            if (id > 0)
             	map1.put("evtStatus", schCodeLst.getCodeDescription(id));         	
         	milestoneType = LC.L_Event;
         	map1.put("mileTypeId", mileTypeId);
            map1.put("mileType", milestoneType);
            map1.put("recNum", ""+eRecNum);
            EM_dataList.add(map1);
        } else if (mileTypeId.equals("SM")){
        	sRecNum ++;
        	map1.put("stdStatusId", milestonePrimaryFKs.get(iX));
            id = EJBUtil.stringToNum(milestonePrimaryFKs.get(iX).toString());
            if (id > 0)
             	map1.put("stdStatus", codeLst.getCodeDescription(id));
         	milestoneType = LC.L_Study_Status;
         	map1.put("mileTypeId", mileTypeId);
            map1.put("mileType", milestoneType);
            map1.put("recNum",""+sRecNum);
            SM_dataList.add(map1);
        } else {
        	aRecNum++;
        	 map1.put("mileDesc", milestoneRuleDescs.get(iX));
         	milestoneType = LC.L_Additional;
         	map1.put("mileTypeId", mileTypeId);
            map1.put("mileType", milestoneType);
            map1.put("recNum",""+aRecNum);
            AM_dataList.add(map1);
        }        
    }
    JSONArray jsAllMilestoneStats = new JSONArray();
    for (int iX=0; iX<ALL_MileStatDetails.size(); iX++) {
    	jsAllMilestoneStats.put(EJBUtil.hashMapToJSON((HashMap)ALL_MileStatDetails.get(iX)));
    }
    jsObj.put("jsAllMilestoneStats", jsAllMilestoneStats);

    jsObj.put("mileCount",mileSeq);
    jsObj.put("AMmileCount",aRecNum);
    jsObj.put("SMmileCount",sRecNum);
    jsObj.put("PMmileCount",pRecNum);
    jsObj.put("EMmileCount",eRecNum);
    jsObj.put("VMmileCount",vRecNum);

    studyProtocols.add(0,LC.L_Select_AnOption/*Select an Option*****/);
 	studyProtIds.add(0,new Integer(0));
 	
 	//For VM AND EM If Calander is present then only 
 	//current calander will be visiable. For INF-22500
 	if(!StringUtil.isEmpty( srchStrProtocolId) && !StringUtil.isEmpty( strCalName)){
 		studyProtIds.clear();
 		studyProtIds.add(StringUtil.stringToInteger(srchStrProtocolId));
 		studyProtocols.clear();
 		studyProtocols.add(strCalName);
 	}
 	jsObj.put("studyProtId",studyProtIds);
    jsObj.put("studyProtocols",studyProtocols);
    studyProtTypes.add(0,"");
    jsObj.put("studyProtTypes",studyProtTypes);
    
    studyVisits.add(0,LC.L_Select_AnOption/*Select an Option*****/);
	studyVisitIds.add("0");
 	jsObj.put("studyVisitId",studyVisitIds);
    jsObj.put("studyVisits",studyVisits);

    studyEventNames.add(0,LC.L_Select_AnOption/*Select an Option*****/);
    studyEventIds.add("0");	
    jsObj.put("studyEventId",studyEventIds);
    jsObj.put("studyEvents",studyEventNames);

    JSONArray jsMilestones = new JSONArray();
    for (int iX=0; iX<PM_dataList.size(); iX++) {
    	jsMilestones.put(EJBUtil.hashMapToJSON((HashMap)PM_dataList.get(iX)));
    }
    jsObj.put("PM_dataArray", jsMilestones);
    
    jsMilestones = new JSONArray();
    for (int iX=0; iX<VM_dataList.size(); iX++) {
    	jsMilestones.put(EJBUtil.hashMapToJSON((HashMap)VM_dataList.get(iX)));
    }
    jsObj.put("VM_dataArray", jsMilestones);
    
    jsMilestones = new JSONArray();
    for (int iX=0; iX<EM_dataList.size(); iX++) {
    	jsMilestones.put(EJBUtil.hashMapToJSON((HashMap)EM_dataList.get(iX)));
    }
    jsObj.put("EM_dataArray", jsMilestones);
    
    jsMilestones = new JSONArray();
    for (int iX=0; iX<SM_dataList.size(); iX++) {
    	jsMilestones.put(EJBUtil.hashMapToJSON((HashMap)SM_dataList.get(iX)));
    }
    jsObj.put("SM_dataArray", jsMilestones);
    
    jsMilestones = new JSONArray();
    for (int iX=0; iX<AM_dataList.size(); iX++) {
    	jsMilestones.put(EJBUtil.hashMapToJSON((HashMap)AM_dataList.get(iX)));
    }
    jsObj.put("AM_dataArray", jsMilestones);

    //Patient Status Milestone PowerBar Columns : INF-22500
    JSONArray jsParentPMColArray = new JSONArray();
    {    	
    	JSONObject jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "patCount");
    	jsObjTemp1.put("label", LC.L_Pat_Count/*Patient Count*****/);
    	jsObjTemp1.put("type", "integer");
    	jsObjTemp1.put("formatter", "customInteger");
    	jsParentPMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileId");
    	jsObjTemp1.put("label", "mileId");
    	jsObjTemp1.put("hidden", "true");
    	jsParentPMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileTypeId");
    	jsObjTemp1.put("label",LC.L_Mstone_TypeID/*Milestone Type Id*****/); 	
    	jsObjTemp1.put("hidden", "true");
    	jsParentPMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "patStatusId");
    	jsObjTemp1.put("label",LC.L_PatStat_ID/*Patient Status Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentPMColArray.put(jsObjTemp1);
    	    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "patStatus");
    	jsObjTemp1.put("label", LC.L_Patient_Status/*Patient Status*****/);
    	jsParentPMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "amount");
    	jsObjTemp1.put("label",LC.L_Amount/*Amount*****/);
    	jsObjTemp1.put("type", "number");
    	jsObjTemp1.put("formatter", "customNumber");
    	jsParentPMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "holdBack_parent");
    	jsObjTemp1.put("label",LC.L_Holdback);
    	jsObjTemp1.put("type", "integer");
    	jsObjTemp1.put("formatter", "customInteger");
    	jsParentPMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "limit");
    	jsObjTemp1.put("label",LC.L_Limit/*Limit*****/);
    	jsObjTemp1.put("type", "integer");
    	jsObjTemp1.put("formatter", "customInteger");
    	jsParentPMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payTypeId");
    	jsObjTemp1.put("label", LC.L_Pment_TypeID/*Payment Type Id*****/); 	
    	jsObjTemp1.put("hidden", "true");
    	jsParentPMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payType");
    	jsObjTemp1.put("label",LC.L_Payment_Type/*Payment Type*****/);
    	jsParentPMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payForId");
    	jsObjTemp1.put("label",LC.L_Pment_ForID/*Payment For Id*****/); 	
    	jsObjTemp1.put("hidden", "true");
    	jsParentPMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payFor");
    	jsObjTemp1.put("label", LC.L_Payment_For/*Payment For*****/);
    	jsParentPMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileStatus");
    	jsObjTemp1.put("label", LC.L_Mstone_Status/*Milestone Status*****/);
    	jsParentPMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileStatusId");
    	jsObjTemp1.put("label",LC.L_Mstone_StatID/*Milestone Status Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentPMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileStaticStatusId");
    	jsObjTemp1.put("label",LC.L_MstoneStatic_StatID/*Milestone Static Status Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentPMColArray.put(jsObjTemp1);
    	
    	
    }
    jsObj.put("parentPMColArray", jsParentPMColArray);
    
  	//Visit Milestone PowerBar Columns
    JSONArray jsParentVMColArray = new JSONArray();
    {    	
    	JSONObject jsObjTemp1 = new JSONObject();
    	
    	jsObjTemp1.put("key", "calendarType");
    	jsObjTemp1.put("label", LC.L_Cal_Type/*Calendar Type*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentVMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileId");
    	jsObjTemp1.put("label", "mileId");
    	jsObjTemp1.put("hidden", "true");
    	jsParentVMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileTypeId");
    	jsObjTemp1.put("label",LC.L_Mstone_TypeID/*Milestone Type Id*****/); 	
    	jsObjTemp1.put("hidden", "true");
    	jsParentVMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "calendarId");
    	jsObjTemp1.put("label", LC.L_Cal_ID/*Calendar Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentVMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "calendar");
    	jsObjTemp1.put("label", LC.L_Calendar/*Calendar*****/);
    	jsParentVMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "visitId");
    	jsObjTemp1.put("label", LC.L_Visit_ID/*Visit Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentVMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "visit");
    	jsObjTemp1.put("label", LC.L_Visit/*Visit*****/);
    	jsParentVMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileRuleId");
    	jsObjTemp1.put("label", LC.L_Mstone_RuleID/*Milestone Rule Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentVMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileRule");
    	jsObjTemp1.put("label", LC.L_Milestone_Rule/*Milestone Rule*****/);
    	jsParentVMColArray.put(jsObjTemp1);
    	
        	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "evtStatusId");
    	jsObjTemp1.put("label", LC.L_EvtStat_ID/*Event Status Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentVMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "evtStatus");
    	jsObjTemp1.put("label", LC.L_Event_Status/*Event Status*****/);
    	jsParentVMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "patCount");
    	jsObjTemp1.put("label",LC.L_Pat_Count/*Patient Count*****/);
    	jsObjTemp1.put("type", "integer");
    	jsObjTemp1.put("formatter", "customInteger");
    	jsParentVMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "patStatusId");
    	jsObjTemp1.put("label",LC.L_PatStat_ID/*Patient Status Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentVMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "patStatus");
    	jsObjTemp1.put("label", LC.L_Patient_Status/*Patient Status*****/);
    	jsParentVMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "amount");
    	jsObjTemp1.put("label",LC.L_Amount/*Amount*****/);
    	jsObjTemp1.put("type", "number");
    	jsObjTemp1.put("formatter", "customNumber");
    	jsParentVMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "holdBack_parent");
    	jsObjTemp1.put("label",LC.L_Holdback);
    	jsObjTemp1.put("type", "integer");
    	jsObjTemp1.put("formatter", "customInteger");
    	jsParentVMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "limit");
    	jsObjTemp1.put("label",LC.L_Limit/*Limit*****/);
    	jsObjTemp1.put("type", "integer");
    	jsObjTemp1.put("formatter", "customInteger");
    	jsParentVMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payTypeId");
    	jsObjTemp1.put("label", LC.L_Pment_TypeID/*Payment Type Id*****/); 	
    	jsObjTemp1.put("hidden", "true");
    	jsParentVMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payType");
    	jsObjTemp1.put("label",LC.L_Payment_Type/*Payment Type*****/);
    	jsParentVMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payForId");
    	jsObjTemp1.put("label",LC.L_Pment_ForID/*Payment For Id*****/); 	
    	jsObjTemp1.put("hidden", "true");
    	jsParentVMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payFor");
    	jsObjTemp1.put("label", LC.L_Payment_For/*Payment For*****/);
    	jsParentVMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileStatusId");
    	jsObjTemp1.put("label",LC.L_Mstone_StatID/*Milestone Status Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentVMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileStatus");
    	jsObjTemp1.put("label", LC.L_Mstone_Status/*Milestone Status*****/);
    	jsParentVMColArray.put(jsObjTemp1);
    	    	
    }
    jsObj.put("parentVMColArray", jsParentVMColArray);
    
  	//Event Milestone PowerBar Columns
    JSONArray jsParentEMColArray = new JSONArray();
    {    	
		JSONObject jsObjTemp1 = new JSONObject();
    	
    	jsObjTemp1.put("key", "calendarType");
    	jsObjTemp1.put("label", LC.L_Cal_Type/*Calendar Type*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileId");
    	jsObjTemp1.put("label", "mileId");
    	jsObjTemp1.put("hidden", "true");
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileTypeId");
    	jsObjTemp1.put("label",LC.L_Mstone_TypeID/*Milestone Type Id*****/); 	
    	jsObjTemp1.put("hidden", "true");
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "calendarId");
    	jsObjTemp1.put("label", LC.L_Cal_ID/*Calendar Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "calendar");
    	jsObjTemp1.put("label", LC.L_Calendar/*Calendar*****/);
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "visitId");
    	jsObjTemp1.put("label", LC.L_Visit_ID/*Visit Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "visit");
    	jsObjTemp1.put("label", LC.L_Visit/*Visit*****/);
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "eventId");
    	jsObjTemp1.put("label",LC.L_EventId/*Event Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "event");
    	jsObjTemp1.put("label", LC.L_Event/*Event*****/);
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileRuleId");
    	jsObjTemp1.put("label", LC.L_Mstone_RuleID/*Milestone Rule Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileRule");
    	jsObjTemp1.put("label", LC.L_Milestone_Rule/*Milestone Rule*****/);
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "evtStatusId");
    	jsObjTemp1.put("label", LC.L_EvtStat_ID/*Event Status Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "evtStatus");
    	jsObjTemp1.put("label", LC.L_Event_Status/*Event Status*****/);
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "patCount");
    	jsObjTemp1.put("label",LC.L_Pat_Count/*Patient Count*****/);
    	jsObjTemp1.put("type", "integer");
    	jsObjTemp1.put("formatter", "customInteger");
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "patStatusId");
    	jsObjTemp1.put("label",LC.L_PatStat_ID/*Patient Status Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "patStatus");
    	jsObjTemp1.put("label", LC.L_Patient_Status/*Patient Status*****/);
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "amount");
    	jsObjTemp1.put("label",LC.L_Amount/*Amount*****/);
    	jsObjTemp1.put("type", "number");
    	jsObjTemp1.put("formatter", "customNumber");
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "holdBack_parent");
    	jsObjTemp1.put("label",LC.L_Holdback);
    	jsObjTemp1.put("type", "integer");
    	jsObjTemp1.put("formatter", "customInteger");
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "limit");
    	jsObjTemp1.put("label",LC.L_Limit/*Limit*****/);
    	jsObjTemp1.put("type", "integer");
    	jsObjTemp1.put("formatter", "customInteger");
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payTypeId");
    	jsObjTemp1.put("label", LC.L_Pment_TypeID/*Payment Type Id*****/); 	
    	jsObjTemp1.put("hidden", "true");
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payType");
    	jsObjTemp1.put("label",LC.L_Payment_Type/*Payment Type*****/);
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payForId");
    	jsObjTemp1.put("label",LC.L_Pment_ForID/*Payment For Id*****/); 	
    	jsObjTemp1.put("hidden", "true");
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payFor");
    	jsObjTemp1.put("label", LC.L_Payment_For/*Payment For*****/);
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileStatusId");
    	jsObjTemp1.put("label",LC.L_Mstone_StatID/*Milestone Status Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentEMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileStatus");
    	jsObjTemp1.put("label", LC.L_Mstone_Status/*Milestone Status*****/);
    	jsParentEMColArray.put(jsObjTemp1);
    	    	
    }
    jsObj.put("parentEMColArray", jsParentEMColArray);
    
 	 //Study Status Milestone PowerBar Columns
    JSONArray jsParentSMColArray = new JSONArray();
    {    
    	JSONObject jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "stdStatusId");
    	jsObjTemp1.put("label",LC.L_StdStat_ID/*Study Status Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentSMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileId");
    	jsObjTemp1.put("label", "mileId");
    	jsObjTemp1.put("hidden", "true");
    	jsParentSMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileTypeId");
    	jsObjTemp1.put("label",LC.L_Mstone_TypeID/*Milestone Type Id*****/); 	
    	jsObjTemp1.put("hidden", "true");
    	jsParentSMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "stdStatus");
    	jsObjTemp1.put("label", LC.L_Study_Status/*Study Status*****/);
    	jsParentSMColArray.put(jsObjTemp1);
    	  	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "amount");
    	jsObjTemp1.put("label",LC.L_Amount/*Amount*****/);
    	jsObjTemp1.put("type", "number");
    	jsObjTemp1.put("formatter", "customNumber");
    	jsParentSMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "holdBack_parent");
    	jsObjTemp1.put("label",LC.L_Holdback);
    	jsObjTemp1.put("type", "integer");
    	jsObjTemp1.put("formatter", "customInteger");
    	jsParentSMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "limit");
    	jsObjTemp1.put("label",LC.L_Limit/*Limit*****/);
    	jsObjTemp1.put("type", "integer");
    	jsObjTemp1.put("formatter", "customInteger");
    	jsParentSMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payTypeId");
    	jsObjTemp1.put("label", LC.L_Pment_TypeID/*Payment Type Id*****/); 	
    	jsObjTemp1.put("hidden", "true");
    	jsParentSMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payType");
    	jsObjTemp1.put("label",LC.L_Payment_Type/*Payment Type*****/);
    	jsParentSMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payForId");
    	jsObjTemp1.put("label",LC.L_Pment_ForID/*Payment For Id*****/); 	
    	jsObjTemp1.put("hidden", "true");
    	jsParentSMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payFor");
    	jsObjTemp1.put("label", LC.L_Payment_For/*Payment For*****/);
    	jsParentSMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileStatusId");
    	jsObjTemp1.put("label",LC.L_Mstone_StatID/*Milestone Status Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentSMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileStatus");
    	jsObjTemp1.put("label", LC.L_Mstone_Status/*Milestone Status*****/);
    	jsParentSMColArray.put(jsObjTemp1);
    	    	
    }
    jsObj.put("parentSMColArray", jsParentSMColArray);
    
  	//Additional Milestone PowerBar Columns
    JSONArray jsParentAMColArray = new JSONArray();
    {    	
    	JSONObject jsObjTemp1 = new JSONObject();
    	
    	jsObjTemp1.put("key", "mileDesc");
    	jsObjTemp1.put("label", LC.L_Milestone_Description/*Milestone Description*****/);
    	jsObjTemp1.put("type", "varchar");
    	jsObjTemp1.put("formatter", "customVarchar");
    	jsParentAMColArray.put(jsObjTemp1);
  
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileId");
    	jsObjTemp1.put("label", "mileId");
    	jsObjTemp1.put("hidden", "true");
    	jsParentAMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileTypeId");
    	jsObjTemp1.put("label",LC.L_Mstone_TypeID/*Milestone Type Id*****/); 	
    	jsObjTemp1.put("hidden", "true");
    	jsParentAMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "amount");
    	jsObjTemp1.put("label",LC.L_Amount/*Amount*****/);
    	jsObjTemp1.put("type", "number");
    	jsObjTemp1.put("formatter", "customNumber");
    	jsParentAMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "holdBack_parent");
    	jsObjTemp1.put("label",LC.L_Holdback);
    	jsObjTemp1.put("type", "integer");
    	jsObjTemp1.put("formatter", "customInteger");
    	jsParentAMColArray.put(jsObjTemp1);
    	
    	//Bug#14891 : Remove Limit field from AM power bar : Yogendra
    	/*jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "limit");
    	jsObjTemp1.put("label",LC.L_Limit);
    	jsObjTemp1.put("type", "integer");
    	jsObjTemp1.put("formatter", "customInteger");
    	jsParentAMColArray.put(jsObjTemp1);*/
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payTypeId");
    	jsObjTemp1.put("label", LC.L_Pment_TypeID/*Payment Type Id*****/); 	
    	jsObjTemp1.put("hidden", "true");
    	jsParentAMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payType");
    	jsObjTemp1.put("label",LC.L_Payment_Type/*Payment Type*****/);
    	jsParentAMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payForId");
    	jsObjTemp1.put("label",LC.L_Pment_ForID/*Payment For Id*****/); 	
    	jsObjTemp1.put("hidden", "true");
    	jsParentAMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "payFor");
    	jsObjTemp1.put("label", LC.L_Payment_For/*Payment For*****/);
    	jsParentAMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileStatusId");
    	jsObjTemp1.put("label",LC.L_Mstone_StatID/*Milestone Status Id*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentAMColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "mileStatus");
    	jsObjTemp1.put("label", LC.L_Mstone_Status/*Milestone Status*****/);
    	jsParentAMColArray.put(jsObjTemp1);
    	    	
    }
    jsObj.put("parentAMColArray", jsParentAMColArray);
    
    //Patient Status Milestone PowerBar DataArray
    JSONArray parentPMDataArray = new JSONArray();
    JSONObject jObj = new JSONObject();
    jObj.put("patCount","");
    jObj.put("mileId",0);
    jObj.put("mileTypeId","PM_Parent");
    jObj.put("patStatus","");
    jObj.put("amount","");
    jObj.put("holdBack","");
    jObj.put("limit","");
    jObj.put("payType","");
    jObj.put("payFor","");
    jObj.put("mileStatus","");
    parentPMDataArray.put(jObj);    
    jsObj.put("parentPMDataArray", parentPMDataArray);
    
    JSONArray parentVMDataArray = new JSONArray();
  	//Visit Milestone PowerBar DataArray
    jObj = new JSONObject();
    jObj.put("calendar","");
    jObj.put("mileId",0);
    jObj.put("mileTypeId","VM_Parent");
    jObj.put("visit","");
    jObj.put("mileRule","");
    jObj.put("evtStatus","");
    jObj.put("patCount","");
    jObj.put("patStatus","");
    jObj.put("amount","");
    jObj.put("holdBack","");
    jObj.put("limit","");
    jObj.put("payType","");
    jObj.put("payFor","");
    jObj.put("mileStatus","");
    parentVMDataArray.put(jObj);    
    jsObj.put("parentVMDataArray", parentVMDataArray);
    
    JSONArray parentEMDataArray = new JSONArray();
  	//Event Milestone PowerBar DataArray
    jObj = new JSONObject();
    jObj.put("calendar","");
    jObj.put("mileId",0);
    jObj.put("mileTypeId","EM_Parent");
    jObj.put("visit","");
    jObj.put("event","");
    jObj.put("mileRule","");
    jObj.put("evtStatus","");
    jObj.put("patCount","");
    jObj.put("patStatus","");
    jObj.put("amount","");
    jObj.put("holdBack","");
    jObj.put("limit","");
    jObj.put("payType","");
    jObj.put("payFor","");
    jObj.put("mileStatus","");
    parentEMDataArray.put(jObj);    
    jsObj.put("parentEMDataArray", parentEMDataArray);
    
    JSONArray parentSMDataArray = new JSONArray();
  	//Study Status Milestone PowerBar DataArray
    jObj = new JSONObject();
    jObj.put("stdStatus","");
    jObj.put("mileId",0);
    jObj.put("mileTypeId","SM_Parent");
    jObj.put("amount","");
    jObj.put("holdBack","");
    jObj.put("limit","");
    jObj.put("payType","");
    jObj.put("payFor","");
    jObj.put("mileStatus","");
    parentSMDataArray.put(jObj);    
    jsObj.put("parentSMDataArray", parentSMDataArray);
    
    JSONArray parentAMDataArray = new JSONArray();
  	//Additional Milestone PowerBar DataArray
    jObj = new JSONObject();
    jObj.put("mileDesc","");
    jObj.put("mileId",0);
    jObj.put("mileTypeId","AM_Parent");
    jObj.put("amount","");
    jObj.put("holdBack","");
    jObj.put("limit","");
    jObj.put("payType","");
    jObj.put("payFor","");
    jObj.put("mileStatus","");
    parentAMDataArray.put(jObj);    
    jsObj.put("parentAMDataArray", parentAMDataArray);
    
   out.println(jsObj.toString());
   // System.out.println(jsObj.toString());
    // <html><head></head><body></body></html>
%>

<%! // Define Java functions here
private String createCheckbox(String itemId, String visitId, boolean checked) {
	StringBuffer sb = new StringBuffer();
	sb.append("<input type='checkbox' ");
	sb.append(" name='e").append(itemId).append("v").append(visitId).append("' ");
	sb.append(" id='e").append(itemId).append("v").append(visitId).append("' ");
	if (checked) {
	    sb.append(" checked ");
	}
	sb.append(" >");
	return sb.toString();
}
%>
<%! // for insert
private JSONArray insertJSON(int index, JSONObject pJsonObject,JSONArray pJSarray) throws JSONException {
	int length = pJSarray.length();
	int i = 0;
	for(i= length-1; i >= index ;i--){
		pJSarray.put(i+1,pJSarray.get(i));
	}
	pJSarray.put(i+1,pJsonObject);
	return pJSarray;
}
%>
