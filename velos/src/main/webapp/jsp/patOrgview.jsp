<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>
<title><%=LC.L_PatOrg_View %><%-- <%=LC.Pat_Patient%> Organization view*****--%></title>

	<%-- Nicholas : Start --%>
	<%--<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>--%>
	<%-- Nicholas : End --%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT Language="Javascript1.2">

</script>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>

<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*"%>


<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
	<%-- Nicholas : Start --%>
	<jsp:include page="skinChoser.jsp" flush="true"/>
	<%-- Nicholas : End --%>
	<Link Rel=STYLESHEET HREF='./styles/velos_popup.css' type=text/css>
<body>
<%
String patId = request.getParameter("patId");
String comRtStr = request.getParameter("c");
int comRtIntVal = 0;

if (!StringUtil.isEmpty(comRtStr))
{
  if (comRtStr.equals("nx")) // no right; weired code used to hide it from URL
  {
	  comRtIntVal = 0;
  }
  else
  {
	  comRtIntVal = 1;

  }
}
else
{
	comRtIntVal = 1;
}

%>

	<table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl">
		<tr><P><%=LC.L_Patent_ID_S %><%-- <%=LC.Pat_Patient%> ID(s)*****--%></P></tr>
		<tr >
			<th width=50% align="center"><%=LC.L_Site_Name %><%-- Site Name*****--%> </th>
			<th  width=50% align="center"><%=LC.L_Id_Upper %><%-- ID*****--%> </th>
		</tr>
	</table>

	<%
		ArrayList siteNames = null;
		ArrayList patFacilityIds = null;
		PatFacilityDao patFacDao = new PatFacilityDao();
		patFacDao.getPatientFacilitiesSiteIds( EJBUtil.stringToNum(patId));
		siteNames = patFacDao.getPatientSiteName();
		patFacilityIds = patFacDao.getPatientFacilityId();
		int patFacilityIdsize = patFacilityIds.size();
		String siteName = null ;
		String patFacilityId = null;

		for (int j=0 ; j<patFacilityIdsize; j++)
		{
		siteName =((siteNames.get(j))==null)?"":siteNames.get(j).toString();
		patFacilityId =((patFacilityIds.get(j))==null)?"":patFacilityIds.get(j).toString();

		%>
		<table width="100%" cellspacing="0" cellpadding="0" border="0" >
<%				if ((j%2)==0) {	%>

			<tr class="browserEvenRow">

			<%
			}
			else
			{ %>

		  <tr class="browserOddRow">
			<%
			}

	if (comRtIntVal ==0)
	{
		patFacilityId = "****";
	}
%>
		<td  width="50%" align="left">&nbsp;&nbsp;<%=siteName%></td>
		<td  width="50%" align="left">&nbsp;&nbsp;<%=patFacilityId%></td>

	  </tr>

	<%
		}

	%>

		</table>

<div class ="mainMenu" id = "emenu"> </div>
</body>
</html>
