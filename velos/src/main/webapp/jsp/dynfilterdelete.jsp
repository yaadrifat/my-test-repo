<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.LC"%>
<HTML>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



<title><%=LC.L_Del_FilterTemplate%><%--Delete Filter Template*****--%></title>



<SCRIPT>

function  validate(formobj){

	if (!(validate_col('e-Signature',formobj.eSign))) return false



	<%-- if(isNaN(formobj.eSign.value) == true) {

	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

	formobj.eSign.focus();

	return false;

   } --%>

}

</SCRIPT>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="resolution.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>

<!-- Virendra: Fixed Bug no. 4738, added import  com.velos.eres.service.util.StringUtil -->
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*"%>

<jsp:useBean id="dynrep" scope="request" class="com.velos.eres.web.dynrep.DynRepJB"/>
<jsp:useBean id="dynrepdt" scope="request" class="com.velos.eres.web.dynrepdt.DynRepDtJB"/>
<jsp:useBean id="dynrepfltr" scope="request" class="com.velos.eres.web.dynrepfltr.DynRepFltrJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 
<jsp:include page="include.jsp" flush="true"/>


<% String src;

	src= request.getParameter("srcmenu");

%>



  



<BODY> 

<br>



<DIV class="popDefault" id="div1">

<% 

	ArrayList repDeleteIds=new ArrayList();
	ArrayList repFltrIds=new ArrayList();
	String mode= "";
	String calStatus= "";
	String repId="";
	//Virendra: Fixed Bug no. 4738, added variable addAnotherWidth
	int addAnotherWidth = 0;
	DynRepDao dynDao=new DynRepDao();
	DynRepDao dynDaoFltr=new DynRepDao();
	
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{

		mode= request.getParameter("mode");
		int ret=0;
		String fltrId=request.getParameter("fltrId");
		fltrId=(fltrId==null)?"0":fltrId;
		String fltrName=request.getParameter("fltrName");
		fltrName=(fltrName==null)?"":fltrName;		
		String delMode=request.getParameter("delMode");
		repId=request.getParameter("repId");
		repId=(repId==null)?"":repId;
		String repName=request.getParameter("repName");
		repName=(repName==null)?"":repName;
		String formId=request.getParameter("formId");
		formId=(formId==null)?"":formId;
		
		
		
		if (delMode==null) {
			delMode="Del";
%>

	<FORM name="deletefilter" method="post" id="deldynfilter" action="dynfilterdelete.jsp" onSubmit="if (validate(document.deletefilter)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>
	<P class="defComments"><%=MC.M_EsignToProc_WithDel%><%--Please enter e-Signature to proceed with delete.*****--%></P>
	<table width=100% cellspacing="0" cellpadding="0" bgcolor="#cccccc">
  		<tr align="center">
  			<td bgcolor="#cccccc">
  				<!-- Ankit: Bug-10775 -->
				<button type="button" onClick="javascript:history.go(-1);"><%=LC.L_Back%></button>	
			</td>
			<!-- Virendra: Fixed Bug no. 4738, added td with addAnotherWidth  -->
			<td bgcolor="<%=StringUtil.eSignBgcolor%>" valign="baseline" align="left" width="85%" <%=addAnotherWidth > 0 ? "" : "colspan=5" %>>
				<jsp:include page="submitBar.jsp" flush="true"> 
					<jsp:param name="displayESign" value="Y"/>
					<jsp:param name="formID" value="deldynfilter"/>
					<jsp:param name="showDiscard" value="N"/>
					<jsp:param name="noBR" value="Y"/>
				</jsp:include>
			</td>
		</tr>
	</table>
	


	 <input type="hidden" name="fltrId" value="<%=fltrId%>">
	 <input type="hidden" name="fltrName" value="<%=fltrName%>">
	 <input type="hidden" name="repId" value="<%=repId%>">
	 <input type="hidden" name="formId" value="<%=formId%>">
	 <input type="hidden" name="repName" value="<%=repName%>">
	  <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
   	 <input type="hidden" name="mode" value="<%=mode%>">

  	 		 	

	 

	</FORM>

<%

	} else {

			String eSign = request.getParameter("eSign");	

			String oldESign = (String) tSession.getValue("eSign");

			if(!oldESign.equals(eSign)) {

%>

	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	

<%

			} else {
   
    dynrepfltr.setId(EJBUtil.stringToNum(fltrId));
  //Modified for INF-18183 ::: Akshi
  dynrepfltr.removeDynRepFltr(AuditUtils.createArgs(session,"",LC.L_Adhoc_Queries));
	ArrayList temp=new ArrayList();
	temp.add(fltrId);
	 //Modified for INF-18183 ::: Akshi
     dynrep.removeFilters(temp,AuditUtils.createArgs(session,"",LC.L_Adhoc_Queries));
    
   %>	
	<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_DelSucc%><%--Data deleted successfully.*****--%> </p>
	<META HTTP-EQUIV=Refresh CONTENT="1; URL=dynfilterbrowse.jsp?srcmenu=<%=src%>&repId=<%=repId%>&formId=<%=formId%>&repName=<%=repName%>">

	<%	}
		} //end esign
		
	 }//end of if body for session 

else { %>

 <jsp:include page="timeout.html" flush="true"/> 

 <% } %>


  

</DIV>



<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>



</body>

</HTML>






