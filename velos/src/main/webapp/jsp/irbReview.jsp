<%@page import="com.velos.eres.service.submissionAgent.SubmissionAgentBean"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html> 
<head> 
<title><%=LC.L_Review_AndForm%><%--Review and Form*****--%></title> 

<%@ page language = "java" import="com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.web.appendix.AppendixJB,com.velos.eres.web.user.UserJB" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="userjB2" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="addressUserB" scope="page" class="com.velos.eres.web.address.AddressJB" />
<%@ page import="com.velos.eres.widget.business.common.UIFlxPageDao" %>
<link href="styles/studyVersion.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function reloadOpener() {
    if (window.opener != null && window.opener.location != null && window.opener.location != undefined) {
	    window.opener.location.reload();
	    // setTimeout("self.close()",1000);
    }
}

function viewDocument(formobj){
		formobj.submit();
}

function openPopup(formobj) {
	if (formobj.popup.checked) {
	    windowName = window.open("irbReviewPopup.jsp?studyId="+formobj.studyId.value+
	    	    "&selType="+formobj.selType.value+ "&study_acc_form_right="+formobj.study_acc_form_right.value+ "&study_team_form_access_right="+formobj.study_team_form_access_right.value+"&appSubmissionType="+formobj.appSubmissionType.value,
	    	    "reviewWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=no,width=600,height=400,left=10,top=10");
	    if (!windowName) { windowName.focus(); }
	} else {
		formobj.submit();
	}
} 
function ClickHereToPrint(el)
{		
	var selType=$j( "#selType option:selected" ).text();
	var selValue=$j( "#selType option:selected" ).val();
	document.getElementById('printpage').style.display = 'none';
	document.getElementById('printspan').style.display = 'none';
	var iFrame =  document.getElementById('preview');
	var div = document.getElementById('previewtemp');
	$j('#study_titleprint').prepend("<br>");
	$j('#hovertitle').replaceWith(el);
	var option = '<select id="selType"><option value="1">'+selType+'</option></select>'
	$j('#selType').replaceWith(option);
	   var iFrameBody;
	   var content = document.getElementById('preview');

	   /* try {
	       content.contentWindow.document.execCommand('print', false, null);
	   } catch (e) {
	       content.contentWindow.print();
	   }     */  
	   
	   var isIE = /*@cc_on!@*/false || !!document.documentMode;
	    if ( iFrame.contentDocument ) 
	   { // FF
	    	
	     iFrameBody = iFrame.contentDocument.getElementsByTagName('body')[0];
	   }
	   else if ( iFrame.contentWindow ) 
	   { // IE
		   
	     iFrameBody = iFrame.contentWindow.document.getElementsByTagName('body')[0];
	   } 
	    if(selValue=="UploadDocs"){
		    $j(iFrameBody).find("#div1").css("height","");
		    $j(iFrameBody).find("#div1").css("overflow-y","hidden");
	    }
	    var widthSetting="800";
	    if(selValue=="LastResubMemo"){
	    	widthSetting="900";
	    }
	    /* if(isIE){
	    	
		    try {
	 	       var done=content.contentWindow.document.execCommand('print', false, null);
	 	       if(done){
	 	    	  window.location.href = window.location.href;
	 	    	 setTimeout(function () {
	 	    	  
	 	    	},500);
	 	       }
	 	   } catch (e) {
	 	       content.contentWindow.print();
	 	   }
		    
	    }else{
	    	alert("FF"); */
	    	setTimeout(function () {
	     WindowObject = window.open('', 'PrintWindow', 'width='+widthSetting+',height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes');
	    var strHtml = "<html>\n<head>\n <link rel=\"stylesheet\" media=\"print\" href=\"print.css\">\n</head><body style=\"height:auto\"><div style=\"testStyle\">\n" +div.innerHTML+iFrameBody.innerHTML+"\n</div>\n</body>\n</html>";
	    WindowObject.document.writeln(strHtml);
	    WindowObject.document.close();
	    WindowObject.focus();
	    WindowObject.print();
	    WindowObject.onafterprint = closePrintView(); //this is the thing that makes it work i
	    	},500);
	    /* } */
		   function closePrintView() { //this function simply runs something you want it to do

		      window.location.href = window.location.href;
	     }
   	  }

</script> 
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
</head> 
   	<jsp:include page="include.jsp" flush="true"/>
   	<jsp:include page="ui-include.jsp" flush="true"/>
<style>
	html, body { overflow:hidden; }
</style>
<body> 
<%
	HttpSession tSession = request.getSession(true);
	
	if (sessionmaint.isValidSession(tSession))
	{
	
					String defUserGroup = (String) tSession.getAttribute("defUserGroup");
				
								
					String studyId = StringUtil.htmlEncodeXss(request.getParameter("studyId"));
					
					// ddStudyvercat=cdCat.toPullDown("selType",EJBUtil.stringToNum(studyvercat)," onChange='' ");
					
					String tabsubtype =StringUtil.htmlEncodeXss(request.getParameter("tabsubtype"));
					String appSubmissionType =StringUtil.htmlEncodeXss(request.getParameter("appSubmissionType"));
					
					String reviewtypdesc = StringUtil.htmlEncodeXss(request.getParameter("reviewtypdesc"));
					if(reviewtypdesc==null){
						reviewtypdesc="";
					}
					String submissionPK = StringUtil.htmlEncodeXss(request.getParameter("submissionPK"));
					String submissionBoardPK = StringUtil.htmlEncodeXss(request.getParameter("submissionBoardPK"));
					String pksubmissionStatus = StringUtil.htmlEncodeXss(request.getParameter("pksubmissionStatus"));
					
					//bypass rights
					int study_acc_form_right = 7;
					int study_team_form_access_right = 7;
					
					String accId=(String)tSession.getAttribute("accountId");
					 
			 	    String userId = (String) tSession.getValue("userId");
			 	   
			 	    String newprovisoflag = request.getParameter("newprovisoflag")==null?"":request.getParameter("newprovisoflag");
			 	    String provisoflag = request.getParameter("provisoflag")==null?"":request.getParameter("provisoflag");
			 	   
			 	    String newflag = request.getParameter("newflag")==null?"":request.getParameter("newflag");
			 	    String submission_type = request.getParameter("submission_type");
			 	    System.out.println("submission_type ="+submission_type);
				     userB = (UserJB) tSession.getValue("currentUser");
			   	    String siteId = userB.getUserSiteId();
			   	    String hiddenflag = request.getParameter("hiddenflag")==null?"":request.getParameter("hiddenflag");
			   	    String hide = request.getParameter("hide")==null?"":request.getParameter("hide");
			   	    String link = request.getParameter("link")==null?"":request.getParameter("link");
			   	    String tabSubTypeForApplicationForm ="";
			   	    String formSubmissionType = "";
			   	    ArrayList arrFrmIds = new ArrayList();
			   	    ArrayList arrFrmNames = new ArrayList();
			   	    
			   	    StringBuffer sbForm = new StringBuffer();
			   		String formOptionsString = "";    
			   		String viewCompletedFormsOptionsString = "";
			   	    String viewLastSubVersionOptionsString = "";
			   	    String viewLastResubMemoOptionsString = "";
			   	    String viewUploadDocumentOptionsString = "";
			   	    String viewSubmissionHistory="";
			   	    String ICFForm = "";
			   	 	String ddStudyvercat="";
					SubmissionAgentBean submission = new SubmissionAgentBean();
					String tmpSubmissionDate = submission.getSubmissionCreatedDate(Integer.parseInt(submissionPK));
					System.out.println("tmpSubmissionDate==="+tmpSubmissionDate);
					String submissionDate = "";
					String submDisplayDate = "";
					if(!tmpSubmissionDate.equals("") && tmpSubmissionDate.contains(",")){
						String[] submitArr = tmpSubmissionDate.split(",");
						submissionDate = submitArr[0];
						submDisplayDate = submitArr[1];
					}
					String statCreatedOn = "";
					System.out.println("submissionDate==="+submissionDate);	
					CodeDao cdCat=new CodeDao();
					UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
					uiFlxPageDao.getHighestFlexPageFullVersion("er_study", Integer.parseInt(studyId));
					int versionMajor = 0;
					int versionMinor = 0;
					String versionNum = "0";
					if(appSubmissionType.equals("prob_rpt") || appSubmissionType.equals("cont_rev") || appSubmissionType.equals("closure")){
						String tmpVersion = uiFlxPageDao.getHighestFlexPageByDate("er_study",EJBUtil.stringToNum(studyId),submissionDate);
						if(!tmpVersion.equals("") && tmpVersion.contains(",")){
							String tmpVersionArr[] = tmpVersion.split(",");
							versionMajor = Integer.parseInt(tmpVersionArr[0]);
							versionNum = String.valueOf(versionMajor);
							if (versionMajor > 0){
								versionNum += ".";
								versionMinor = Integer.parseInt(tmpVersionArr[1]);
								versionNum += (versionMinor == 0)? "00" : ((versionMinor > 9)? versionMinor : "0" + versionMinor);
							}
						}
					}else{
						versionMajor = uiFlxPageDao.getHighestFlexPageVersionSubmission("er_study",EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(submissionPK));
						versionNum = String.valueOf(versionMajor);
						if (versionMajor > 0){
							versionNum += ".";
							versionMinor = uiFlxPageDao.getHighestMinorFlexPageVersionSubmission(versionMajor, "er_study",EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(submissionPK));
							versionNum += (versionMinor == 0)? "00" : ((versionMinor > 9)? versionMinor : "0" + versionMinor);
						}
					}
					//cdCat.getCodeValues("studyvercat");
					
					cdCat.setCType("studyvercat");
			        cdCat.setForGroup(defUserGroup);
			        if(appSubmissionType.equals("prob_rpt") || appSubmissionType.equals("cont_rev") || appSubmissionType.equals("closure")){
			        	CodeDao cDao = new CodeDao();
			        	int approvedCode = 0;
			        	statCreatedOn = submission.getSubmissionApprovedDate(Integer.parseInt(submissionPK),approvedCode);
			        	System.out.println("statCreatedOn--->"+statCreatedOn);
			        }
					if(appSubmissionType.equals("prob_rpt") || appSubmissionType.equals("cont_rev") || appSubmissionType.equals("closure")){
						if(!CFG.ONGOING_SUBMISSION_CATEGORIES.equals("TBD") && !CFG.ONGOING_SUBMISSION_CATEGORIES.equals("")){
							String verCategories = CFG.ONGOING_SUBMISSION_CATEGORIES;
							cdCat.getCodeValuesBySubTypes("studyvercat",verCategories.split(","));
						}else{
							cdCat.getCodeValues("studyvercat");
						}
					}else{
						cdCat.getCodeValues("studyvercat");
					}
					
			        HashMap sysDataMap = studyB.getStudySysData(request);
			        String roleCodePk = (String) sysDataMap.get("studyRoleCodePk");
			    	tSession.setAttribute("studyRoleCodePk",roleCodePk);  
			    	String fromPend = request.getParameter("fromPend")==null?"":request.getParameter("fromPend");
					String studyvercat=request.getParameter("selType");
					if (studyvercat==null) studyvercat="";	
					
					ddStudyvercat=cdCat.toPullDownprint("selType",EJBUtil.stringToNum(studyvercat)," onChange='viewDocument(document.rev);' ");
					
			   	    if (appSubmissionType.equals("new_app"))
			   	    {
			   	    	tabSubTypeForApplicationForm = "irb_form_tab";
			   	    }
			   	    else
			   	    {
			   	    	tabSubTypeForApplicationForm = "irb_ongoing_menu";
			   	    }
				 
				 	// Add link for all 'Checklist' forms
		 		 
			 		 int idxOption = -1;
				 	 LinkedFormsDao lf=new LinkedFormsDao();
				 	 ICFForm=lf.getICFForm();
				 	 boolean protOptionsFlag=false;
			 		 viewCompletedFormsOptionsString = "<OPTION value='AllCheck*F'>"+LC.L_ViewChk_Forms/*View Checklist Forms*****/+"</OPTION>";
			 		 viewLastSubVersionOptionsString = "<OPTION value='LastSubVer' selected>"+LC.L_LastSubmit_Version/*Latest Protocol Version*****/+"</OPTION>";
			 		 viewLastResubMemoOptionsString = "<OPTION value='LastResubMemo'>"+LC.L_LastResub_Memo/*Latest Submitted Resubmission Memo*****/+"</OPTION>";
			 		 viewUploadDocumentOptionsString = "<OPTION value='UploadDocs'>"+LC.L_Uploaded_Docs+"</OPTION>";
			 		 viewSubmissionHistory = "<OPTION value='SubmHist'>"+LC.L_Submhistory/*Latest Submitted Resubmission Memo*****/+"</OPTION>";
			 		 idxOption = ddStudyvercat.indexOf("<OPTION");
			 		 if(("LIND".equals(CFG.EIRB_MODE))){
		 		 		if (idxOption > -1)
		 		 		{
		 		 			if(ICFForm.equals("") || ICFForm==null){
		 		 				
		 		 			}else{
		 		 				ddStudyvercat = ddStudyvercat.substring(0,idxOption) + viewCompletedFormsOptionsString +  ICFForm + ddStudyvercat.substring(idxOption);
		 		 			}
		 		 			
		 		 		}
		 		 	}else{
		 		 		if (idxOption > -1)
		 		 		{
		 		 			ddStudyvercat = ddStudyvercat.substring(0,idxOption) + viewCompletedFormsOptionsString + ddStudyvercat.substring(idxOption);
		 		 		}
		 		 	}
		 		 		 
					ArrayList arTabs = new ArrayList();
					arTabs.add("irb_new_tab");
					arTabs.add("irb_assigned_tab");
					arTabs.add("irb_compl_tab");
					arTabs.add("irb_post_tab");
					arTabs.add("irb_pend_tab");
					arTabs.add("irb_ongoing_menu");
					arTabs.add("irb_revfull_tab");
					arTabs.add("irb_revanc_tab");
					arTabs.add("irb_revexem_tab");
					arTabs.add("irb_revexp_tab");
					arTabs.add("irb_meeting");
					 
					//arFormTypes are linked with tab types
					String[] arFormTypes = { "irb_check1","irb_check2","irb_check3","irb_check5","","irb_sub","irb_check4", "irb_check4","irb_check4","irb_check4","irb_meeting"};
					String formCategory = "";
					int idxForm = -1;
					if (! StringUtil.isEmpty(tabsubtype))
					{
						idxForm = arTabs.indexOf(tabsubtype);
						if (idxForm > -1)
						{
							formCategory = arFormTypes[idxForm];
						}
					}

					formSubmissionType = EIRBDao.getIRBFormSubmissionTypeSQL(tabSubTypeForApplicationForm,"irb_sub",appSubmissionType);	 						

					LinkedFormsDao lnkFrmDao = new LinkedFormsDao();
		 			
		 			lnkFrmDao = lnkformB.getStudyForms(EJBUtil.stringToNum(accId),EJBUtil.stringToNum(studyId), 
		 				EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId), 
		         study_acc_form_right,  study_team_form_access_right, 
		         true, formSubmissionType, "irb_sub");
	 
                 if (lnkFrmDao != null)
                 {
                 	arrFrmIds = lnkFrmDao.getFormId();		 
		 			arrFrmNames = lnkFrmDao.getFormName();
		 		 }
		 		 
		 		 if (arrFrmIds != null && arrFrmIds.size() > 0)
		 		 {
		 		 	 
		 		 	for (int k=0;k< arrFrmIds.size() ;k++)
		 		 	{
		 		 		sbForm.append("<OPTION ").append(k==0 ? "" : "")
		 		 		.append(" value=\""+arrFrmIds.get(k)+"*F\">"+ arrFrmNames.get(k)+"</OPTION>");
		 		 	}	
		 		 	formOptionsString  = sbForm.toString();
		 		 	
		 		 	
					
		 		 	if (! StringUtil.isEmpty(formOptionsString))
		 		 	{
		 		 		//append this string to the documents dropdown
		 		 		
		 		 		int idx = -1;
		 		 		
		 		 		idx = ddStudyvercat.indexOf("<OPTION");
		 		 		if (idx > -1)
		 		 		{
			 		 		ddStudyvercat = ddStudyvercat.substring(0,idx) + formOptionsString + ddStudyvercat.substring(idx);
			 		 	}
		 		 		
		 		 	}
		 		 }
		 		idxOption = ddStudyvercat.indexOf("<OPTION");
		 		if(("LIND".equals(CFG.EIRB_MODE))){
	 		 		if (idxOption > -1)
	 		 		{
	 		 			if(appSubmissionType.equals("prob_rpt") || appSubmissionType.equals("cont_rev") || appSubmissionType.equals("closure")){
		 		 			if(tabsubtype.equals("irb_appr_tab")){
		 		 				ddStudyvercat = ddStudyvercat.substring(0,idxOption) + viewUploadDocumentOptionsString + viewSubmissionHistory + ddStudyvercat.substring(idxOption);
		 		 			}else{
		 		 				ddStudyvercat = ddStudyvercat.substring(0,idxOption) + viewLastSubVersionOptionsString + viewLastResubMemoOptionsString + viewUploadDocumentOptionsString + viewSubmissionHistory + ddStudyvercat.substring(idxOption);
		 		 			}
	 		 			}else{
	 		 				ddStudyvercat = ddStudyvercat.substring(0,idxOption) + viewLastSubVersionOptionsString + viewLastResubMemoOptionsString + viewUploadDocumentOptionsString + viewSubmissionHistory + ddStudyvercat.substring(idxOption);
	 		 			}
	 		 		}
	 		 	}
		 		
		 		 
		 		 
		 		 boolean isReviewArea = false;
                 if (tabsubtype != null && tabsubtype.startsWith("irb_rev")) { isReviewArea = true; }
				
					%>
				<table id="printtb" border="1" width="100%" height="100%" cellpadding="3px" cellspacing="3px">
                <div id="ddOpt" style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12">
                <script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
				<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
				<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
                <tr><td id ="previewtemp">
				<form name="rev" style="margin:0px" action="previewIRBDocs.jsp" method="post" target="preview"> 
				 
  				  <input name="studyId" value="<%=studyId%>" type="hidden" >
  				  <input name="newflag" value="<%=newflag%>" type="hidden" >
  				  <input name="hide" value="<%=hide%>" type="hidden" >
  				  <input name="link" value="<%=link%>" type="hidden" >
  				  <input name="submissionDate" type="hidden" value="<%=submissionDate %>"/>
  				  <input name="approvalDate" type="hidden" value="<%=statCreatedOn %>"/>
				  <input name="appSubmissionType" value="<%=appSubmissionType%>" type="hidden" >
				  <input name="submissionType" value="<%=formSubmissionType%>" type="hidden" >
				   <input type="hidden" name="submissionPK" value="<%=submissionPK%>" />
		                  <input type="hidden" name="submissionBoardPK" value="<%=submissionBoardPK%>" />
		                  <input type="hidden" name="tabsubtype" value="<%=tabsubtype%>" />
		                  <input type="hidden" name="versionMajor" value="<%=versionMajor %>" />
		                  <input type="hidden" name="versionMinor" value="<%=versionMinor %>" />
		                  		  
				  <input name="study_acc_form_right" value="<%=study_acc_form_right%>" type="hidden" >
				  <input name="study_team_form_access_right" value="<%=study_team_form_access_right%>" type="hidden" >
				  
				  <%
				  String studyNumber=null;
				    String studyTitle=null;
				    String PI = "";
				    String PIDetails="";
				    int pricInvest=0;
				    if(!studyId.equals("")){
						studyB.setId(EJBUtil.stringToNum(studyId));
						studyB.getStudyDetails();
						studyNumber = studyB.getStudyNumber();
						studyTitle = studyB.getStudyTitle();
						PI = studyB.getStudyPrimInv();
						if(PI==null){
							PI="";
						}
						
						
						if(!PI.equalsIgnoreCase("")){
							pricInvest = EJBUtil.stringToNum(PI);
						
						if(pricInvest>0){						
						userjB2.setUserId(EJBUtil.stringToNum(PI));
						userjB2.getUserDetails();
						String firstname = userjB2.getUserFirstName();
						String lastname = userjB2.getUserLastName();
						String midname = userjB2.getUserMidName()==null?"":userjB2.getUserMidName();
						String logname = userjB2.getUserLoginName();
						String email;
						
						addressUserB.setAddId(EJBUtil.stringToNum(userjB2.getUserPerAddressId()));
						addressUserB.getAddressDetails();
						email = addressUserB.getAddEmail();
						email = ( email == null )?"":(email);//JM
					
						PI = firstname+" "+lastname ;
						PIDetails=firstname+" "+midname+" "+lastname+" "+email+" "+logname;
						System.out.println(firstname+""+midname+""+lastname+""+email+""+logname);
						}}
						
				    }
				  %>
				  
				  <script language=javascript>
						var study_Title = htmlEncode('<%=studyTitle%>');
				  </script>
				  
				  <h5><b>
					<%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>: <%=studyNumber %>&nbsp;&nbsp;&nbsp;&nbsp;
					<%=LC.L_Version%><%--Version*****--%>: <%=versionNum %>&nbsp;&nbsp;&nbsp;&nbsp;
					<span id="study_titleprint"><%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>: <img id ="hovertitle" src="./images/View.gif" onmouseover="return overlib(study_Title,CAPTION,'<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>' ,RIGHT , BELOW );" onmouseout="return nd();" > &nbsp;&nbsp;&nbsp;&nbsp;</span>
					&nbsp;&nbsp;<a  id ="printpage" onclick="ClickHereToPrint('<%=StringUtil.htmlEncode(studyTitle)%>');"><img src="./images/printer.gif" title="<%=LC.L_Printer_FriendlyFormat%><%--Printer Friendly Format*****--%>" alt="<%=LC.L_Printer_FriendlyFormat%><%--Printer Friendly Format*****--%>" align="absmiddle" border="0"></img></a>
					<br>
					<%=LC.L_SubmissionID%>: <%=submissionPK%>
					<br>
					<%=LC.L_Principal_Investigator%>: 
					<%if(pricInvest>0){ %>
					<%=PI%><img src="./images/View.gif" border="0" onmouseover="return overlib(htmlEncode('<%=PIDetails %>'),ABOVE,CAPTION,'');" onmouseout="nd();">
					<%} %>
					<br>
					<%=LC.L_Print_Submission_Date %>: <%=submDisplayDate %>
				  </b></h5><br>
 				   <%=ddStudyvercat%>
                  <span id ="printspan" ><input type="checkbox" name="popup" id="popupprint"><%=MC.M_OpenIn_NewWindow%><%--Open in a New Window*****--%></input>
                  <button type="button"  name="goButton" id="goButtonprint"  onClick="return openPopup(document.rev);" value="<%=LC.L_Go%><%--Go*****--%>"><%=LC.L_Go%></button></span>
				</form>
                </td>
                <%if(!fromPend.equalsIgnoreCase("fromPend")){ %>
                <td id="preview2">
                <%formSubmissionType = EIRBDao.getIRBFormSubmissionTypeSQL(tabsubtype,formCategory,appSubmissionType);%>
                <jsp:include page="studyFormDD.jsp" flush="true"> 
               	<jsp:param name="studyId" value="<%=studyId%>"/>
             	<jsp:param name="submissionType" value="<%=formSubmissionType%>"/>			
            	<jsp:param name="formCategory" value="<%=formCategory%>"/>
            	<jsp:param name="reviewtypdesc" value="<%=reviewtypdesc%>"/>
            	<jsp:param name="submissionPK" value="<%=submissionPK%>"/>
             	<jsp:param name="submissionBoardPK" value="<%=submissionBoardPK%>"/>
             	<jsp:param name="provisoflag" value="<%=provisoflag%>"/>
            	<jsp:param name="target" value="irbf"/>	
            	<jsp:param name="study_acc_form_right" value="7"/>	
            	<jsp:param name="hiddenflag" value="<%=hiddenflag%>"/>	
            	<jsp:param name="study_team_form_access_right" value="7"/>	
            	<jsp:param name="pksubmissionStatus" value="<%=pksubmissionStatus %>"/>
                <jsp:param name="submission_type" value="<%=submission_type %>"/>
                </jsp:include>   
                </td>
                <%} %>
                </tr>
                </div> 
				<% String formHeight = isReviewArea ? "70%" : "95%"; %>
				<tr height="<%=formHeight%>"> 
				 
				<td id="previewTD" valign="top" width="50%"> 
					
					<iframe name="preview" id="preview" src="previewIRBDocs.jsp?studyId=<%=studyId%>&tabsubtype=<%=tabsubtype%>&appSubmissionType=<%=appSubmissionType%>&submissionPK=<%=submissionPK%>&submissionBoardPK=<%=submissionBoardPK%>" width="100%" height="100%" frameborder="1" scrolling="no" allowautotransparency=true>
						
						</iframe>
						
				 </td> 
				 	<%if(!fromPend.equalsIgnoreCase("fromPend")){ %>
				<td id="reviewTD" valign="top" width="50%"> 
					<iframe name="review" src="irbReviewForm.jsp?studyId=<%=studyId%>&tabsubtype=<%=tabsubtype%>&appSubmissionType=<%=appSubmissionType%>&submissionPK=<%=submissionPK%>&submissionBoardPK=<%=submissionBoardPK%>&reviewtypdesc=<%=reviewtypdesc%>" 
						width="100%" height="100%" frameborder="1" scrolling="no" allowautotransparency=true>
						</iframe>
						
				 </td> 	
				 <%} %>			
				</tr>
                <% if (isReviewArea) { %>
					 <tr height="25%">
					 	<td colspan=2 valign="top" >
					 
							<iframe name="proviso" src="irbProvisos.jsp?studyId=<%=studyId%>&tabsubtype=<%=tabsubtype%>&appSubmissionType=<%=appSubmissionType%>&submissionPK=<%=submissionPK%>&submissionBoardPK=<%=submissionBoardPK%>&pksubmissionStatus=<%=pksubmissionStatus%>&hiddenflag=<%=hiddenflag%>&newprovisoflag=<%=newprovisoflag%>" 
						width="100%" height="100%" frameborder="1" scrolling="yes" allowautotransparency=true>
						</iframe>
											
						</td>
					 </tr> 
                <% }  %>
				</table> 

	<script>
		document.rev.selType.selectedIndex=0;
		
		viewDocument(document.rev);	
	</script>
	<% }
		else
		{
		%>
			<jsp:include page="timeout.html" flush="true"/>
		<%
		}
		%>
	  <div class = "myHomebottomPanel"> 
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</body> 
</html>