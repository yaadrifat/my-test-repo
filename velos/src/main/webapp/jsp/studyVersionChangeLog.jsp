<%@page import="com.velos.eres.web.study.StudyJB"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC,com.velos.eres.service.util.DateUtil"%> 
<%@page import="org.json.JSONObject,org.json.JSONArray" %>
<%@page import="com.velos.eres.widget.service.util.FlxPageCache, com.velos.eres.service.util.StringUtil"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.eres.web.studyRights.StudyRightsJB"%>
<%@page import="com.velos.eres.business.common.*,com.velos.eres.service.util.*"%>
<%@page import="com.velos.eres.compliance.web.ProtocolChangeLog"%>
<%@page import="com.velos.eres.compliance.web.ComplianceJB" %>
<%@page import="java.util.*,java.io.*"%>
<%@page import="com.velos.eres.web.submission.SubmissionStatusJB" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="grpRights" scope="session" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="stdRights" scope="session" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="studyJB" scope="session" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="complianceJB" scope="request" class="com.velos.eres.compliance.web.ComplianceJB"/>
<link href="styles/studyVersion.css" rel="stylesheet" type="text/css" />
<%
request.setCharacterEncoding("UTF-8");
response.setContentType("text/html");
String isNewAmendment="false";

HttpSession tSession = request.getSession(false); 
if (!sessionmaint.isValidSession(tSession)) {
	%>	
	<jsp:include page="index.html" flush="true" />
	<%
	return;
}
String userId = (String)tSession.getAttribute("userId");
String grpId = (String) tSession.getValue("defUserGroup");
String studyId = (String)request.getParameter("studyId");
int pageVer = StringUtil.stringToNum(request.getParameter("pageVer"));
int pageMinVer = StringUtil.stringToNum(request.getParameter("pageMinorVer"));
String isPopupWin = "0";
if(request.getParameter("isPopupWin")!=null){
	isPopupWin = request.getParameter("isPopupWin");
}

if(isPopupWin.equals("1")){%>
<jsp:include page="include.jsp">
<jsp:param value="1" name="isPopupWin"/>
</jsp:include>

 

<%}
if(studyId == null || studyId.equals("null")){
	studyId = "0";
} 

%>
<link rel="stylesheet" type="text/css" href="styles/flexPage.css">

<%
StudyDao studyDao = new StudyDao();
String studyNumber = studyDao.getStudy(StringUtil.stringToNum(studyId));
out.println(LC.L_Study_Number + " : " + studyNumber);
%>
<BR>
<%
boolean showHeader = false, fields = false, attachmts = false, formResp = false;
ProtocolChangeLog protocolChangeLog = new ProtocolChangeLog();
JSONObject protocolVerDiffJSON = protocolChangeLog.fetchStudyVersionChangeLog(pageVer, pageMinVer, StringUtil.stringToNum(studyId), grpId);
String fieldChanges = protocolVerDiffJSON.getString(ProtocolChangeLog.VERSIONDIFF_STR);
String attachChanges = protocolVerDiffJSON.getString(ProtocolChangeLog.ATTACHDIFF_STR);
String formRespChanges = protocolVerDiffJSON.getString(ProtocolChangeLog.FORMRESPDIFF_STR);

fields = (LC.Field_Version_No_Diff_Message.equals(fieldChanges))? false : true;
attachmts = (LC.Attach_Version_No_Diff_Message.equals(attachChanges))? false : true;
formResp = (LC.Field_Version_No_Diff_Message.equals(formRespChanges))? false : true;
showHeader = (fields || attachmts || formResp);

String comments = protocolVerDiffJSON.getString(ProtocolChangeLog.COMMENTS_STR);
%>
<BR>
<%if (!fields){%>
	<%=LC.Field_Version_No_Diff_Message%><BR>
<%}%>
<BR>
<%if (!fields && !attachmts){%>
	<%=LC.Attach_Version_No_Diff_Message%><BR>
<%}%>
<BR>
<%if (showHeader){%>
<table class='TFtable' id='resubMemoTbl' width='100%' cellspacing='4' cellpadding='4' border='1' >
	<tr>
		<th width='13%'><%=LC.Change_Version_Title%></th>
		<th width='17%'><%=LC.Change_Version_Field_Name%></th>
		<th style="word-wrap:break-word" width='20%'><%=LC.Change_Version_Old_Value%></th>
		<th style="word-wrap:break-word" width='20%'><%=LC.Change_Version_New_Value%></th>
		<th class="printClass" width='25%'><%=LC.Change_Version_Reason_for_Change%></th>
		<th width='15%'><%=LC.Change_Version_Review_Board%></th>
	</tr>
<%
	if (fields){
		out.println(fieldChanges);
	}
	if (attachmts){
		out.println(attachChanges);
	}
	if (formResp){
		out.println(formRespChanges);
	}
%>
</table>
<%} %>

<%if (fields && !attachmts){%>
	<%=LC.Attach_Version_No_Diff_Message%><BR>
<%}%>
<%if (!formResp){%>
	<%=LC.FormResp_Version_No_Diff_Message%><BR>
<%}%>
<BR>
<table class='TFtable' width='100%' cellspacing='4' cellpadding='4' border='1'>
	<tr><th><%=LC.L_Comments %></th></tr>
	<tr><td><%=comments%></td></tr>
</table>
<head>
<%if(isPopupWin.equals("1")){ %>
<style type="text/css">
.printClass{
min-width:200px;
word-wrap:normal;
}
.printClass1{
word-wrap:break-word;
}
.printClass2{
word-wrap:normal;
}
#resubMemoTbl{
table-layout: fixed;
}
</style>

<script>
var table = document.getElementById("resubMemoTbl");
for (var i = 0, row; row = table.rows[i]; i++) {
   for (var j = 0, col; col = row.cells[j]; j++) {
	 if(j==4){
     	col.className="printClass";
     }else if(j==2 || j==3){
    	 col.className="printClass1";
     }else{
    	 col.className="printClass2";
     }
     
   }  
}
</script>
<%} %>
</head>