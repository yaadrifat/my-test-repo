<!-- 
This JSP will be included in gadgetHome.jsp. All non-local javascript variables and functions 
will be global in the mother JSP. Therefore, make sure to suffix all non-local variables and functions
with the gadget ID.
 -->

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
if (!sessionmaint.isValidSession(request.getSession(false))) {
	return;
}
%>
<script>
var gadgetSample1_validationMessages = {};
var gadgetSample1_submitSettings = function() {
	if (!gadgetSample1_validate()) {
		return false;
	}
	var myGadgetId = 'gadgetSample1';
	setSettingsGlobal(myGadgetId);
	$j("#gadgetSample1_settings").slideUp(200).fadeOut(200);
	setTimeout(function() { 
		reloadGadgetScreenGlobal(myGadgetId);
		gadgetSample1_screenAction();
	}, 150);
	
	return true;
}
var gadgetSample1_clearSettings = function() {
	$('gadgetSample1_numRows').value = '';
	$('gadgetSample1_numCols').value = '';
	gadgetSample1_clearErrMsg();
};
var gadgetSample1_clearErrMsg = function() {
	if ($('gadgetSample1_numRows_error')) { $('gadgetSample1_numRows_error').innerHTML = ''; }
	if ($('gadgetSample1_numCols_error')) { $('gadgetSample1_numCols_error').innerHTML = ''; }
};
var formatErrMsg_gadgetSample1 = function() {
	try {
		var errMsgList = [];
		for (var key in gadgetSample1_validationMessages) {
			if (!gadgetSample1_validationMessages.hasOwnProperty(key)) { break; }
			errMsgList[key] = '<ul class="errMsg">';
			for (var iX=0; iX<gadgetSample1_validationMessages[key].length; iX++) {
				errMsgList[key] += '<li style="padding-left:0px;" >'+gadgetSample1_validationMessages[key][iX]+'</li>';
			}
			errMsgList[key] += '</ul>';
			$(key+'_error').innerHTML = errMsgList[key];
		}
		for (var key in gadgetSample1_validationMessages) {
			if (!gadgetSample1_validationMessages.hasOwnProperty(key)) { break; }
			$(key).focus();
			return false;
		}
	} catch(e) {
		// console.log(e.message);
		return false;
	}
	return true;
};
var gadgetSample1_validate = function() {
	var gadgetId = 'gadgetSample1';
	gadgetSample1_validationMessages = {};
	validationMessages[gadgetId] = {};
	gadgetSample1_clearErrMsg();
	var numRowsValidations = [];
	var numRowsKey = 'gadgetSample1_numRows';
	if (!$(numRowsKey).value) {
		numRowsValidations.push('This field is required.');
	} else {
		if (jQuery.trim($(numRowsKey).value).length == 0) {
			numRowsValidations.push('This field is required.');
		} else if (jQuery.trim($(numRowsKey).value).match(/[\D]+/g)) {
			numRowsValidations.push('Must be a valid number.');
		} else if (parseInt($(numRowsKey).value) > 20) {
			numRowsValidations.push('Must be 20 or less.');
		} else if (parseInt($(numRowsKey).value) < 1) {
			numRowsValidations.push('Must be 1 or more.');
		}
	}
	if (numRowsValidations && numRowsValidations.length > 0) {
		//gadgetSample1_validationMessages.gadgetSample1_numRows = numRowsValidations;
		validationMessages[gadgetId][numRowsKey] = numRowsValidations;
	}
	
	var numColsValidations = [];
	var numColsKey = 'gadgetSample1_numCols';
	if (!$(numColsKey).value) {
		numColsValidations.push('This field is required.');
	} else {
		if (jQuery.trim($(numColsKey).value).length == 0) {
			numColsValidations.push('This field is required.');
		} else if (jQuery.trim($(numColsKey).value).match(/[\D]+/g)) {
			numColsValidations.push('Must be a valid number.');
		} else if (parseInt($(numColsKey).value) > 20) {
			numColsValidations.push('Must be 20 or less.');
		} else if (parseInt($(numColsKey).value) < 1) {
			numColsValidations.push('Must be 1 or more.');
		}
	}
	if (numColsValidations && numColsValidations.length > 0) {
		//gadgetSample1_validationMessages.gadgetSample1_numCols = numColsValidations;
		validationMessages[gadgetId][numColsKey] = numColsValidations;
	}
	
	return formatErrMsgGlobal(gadgetId);
	//return formatErrMsg_gadgetSample1();
};

var updateSettings_gadgetSample1 = function() {
	var j = {};
	j.id = 'gadgetSample1';
	j.numRows = jQuery.trim($('gadgetSample1_numRows').value);
	j.numCols = jQuery.trim($('gadgetSample1_numCols').value);
	return j;
};
var getSettings_gadgetSample1 = function(o) {
	try {
		var j = jQuery.parseJSON(o.responseText);
		$j('#gadgetSample1_numRows').focus().val(j.gadgetSample1.numRows);
		$j('#gadgetSample1_numCols').val(j.gadgetSample1.numCols);
	} catch(e) {};
};
var openSettings_gadgetSample1 = function() {
	$j("#gadgetSample1_settings").slideDown(200).fadeIn(200);
	YUI({
	    gallery: 'gallery-2012.06.20-20-07'
	}).use('gallery-placeholder', 'node', 'io', function(Y) {
		Y.one('#gadgetSample1_saveButton').on('click', gadgetSample1_submitSettings);
    	Y.io('gadgetCookie.jsp', {
        	method: 'POST',
        	data: 'type=typeGetSettings',
        	sync: true,
        	on: {
            	success: function (tranId, o) {
            		getSettings_gadgetSample1(o);
                },
                failure: function (tranId, o) {
                }
            }
        });
	});
}
var cancelSettings_gadgetSample1 = function() {
	gadgetSample1_clearSettings();
	$j("#gadgetSample1_settings").slideUp(200).fadeOut(200);
}

var gadgetSample1_screenAction = function() {
	$j("#gadgetSample1_saveButton").button();
	$j("#gadgetSample1_cancelButton").button();
	$j("#gadgetSample1_cancelButton").click(function() {
		cancelSettings_gadgetSample1();
	});
};

clearerRegistry['gadgetSample1'] = gadgetSample1_clearSettings;
validatorRegistry['gadgetSample1'] = gadgetSample1_validate;
settingsUpdatorRegistry['gadgetSample1'] = updateSettings_gadgetSample1;
settingsGetterRegistry['gadgetSample1'] = getSettings_gadgetSample1;
settingsOpenerRegistry['gadgetSample1'] = openSettings_gadgetSample1;
screenActionRegistry['gadgetSample1'] = gadgetSample1_screenAction;

</script>

<div id="gadget-modal-gadgetSample1" style="display:none">
    <div class="yui3-widget-bd">
        <form id="form_gadgetSample1" onsubmit="return false;">
            <fieldset>
                <p>
                    <label for="gadgetSample1_numRows">Number of rows</label><br/>
                    <input type="text" name="gadgetSample1_numRows1" id="gadgetSample1_numRows1" placeholder="">
                </p>
                <p>
                    <label for="gadgetSample1_numCols">Number of cols</label><br/>
                    <input type="text" name="gadgetSample1_numCols1" id="gadgetSample1_numCols1" placeholder="">
                </p>
            </fieldset>
        </form>
    </div>
</div>
