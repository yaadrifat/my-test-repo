<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page import="com.velos.eres.service.util.LC"%>
<jsp:include page="ui-include.jsp" flush="true"/>
<jsp:useBean id="tokenJB" scope="request" class="com.velos.eres.web.userSession.UserSessionTokenJB"/>
<%
String selectedTab = StringUtil.htmlEncodeXss(request.getParameter("tabsubtype"));
String moduleName  = StringUtil.htmlEncodeXss(request.getParameter("tabtype"));
String originalTabType  = "";

if ("irbPend".equals(moduleName)) {
	if(selectedTab.equals("irb_appr_tab")){
    moduleName = "irbPend_final";
    }else{
    	moduleName = "irbPend";
    }
	
} else if ("irbReview".equals(moduleName))
{
	moduleName = "irbReview";
}
else{
    moduleName = "irbSub";
}

originalTabType = moduleName;

if (moduleName.equals("irbSub") && selectedTab.equals("irb_appr_tab"))
{
		moduleName = "irbPend_finalforsubmission"; //get the final outcome tab - same as final outcome in PIs area
}
int meetingGrpRight  = 0;

HttpSession tSession = request.getSession(true);
GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
meetingGrpRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_MM"));
int pageRight_meeting = meetingGrpRight;
System.out.println("pageRight_meeting"+pageRight_meeting);
%>

<script>
var paginate_study;
window.onload = initEvents;
function initEvents() {
	document.getElementById('searchCriteria').onkeypress = readCharacter;
	document.getElementById('reviewSearch').onkeypress = readCharacter;
}
function readCharacter(evt) {
	evt = (evt) || window.event;
    if (evt) {
        var code = evt.charCode || evt.keyCode;
        if (code == 13 || code == 10) {
         	paginate_study.runFilter();
        }
    }
}
function checkSearch(formobj)
{
 	paginate_study.runFilter();
}

 $E.addListener(window, "load", function() {
	 $('CSRFToken').value="<%=StringUtil.trueValue(tokenJB.createTokenForUser((String) tSession.getAttribute("userId")==null?0:StringUtil.stringToNum((String) tSession.getAttribute("userId"))))%>";
    paginate_study=new VELOS.Paginator('<%=moduleName%>',{
 				sortDir:"asc",
				sortKey:"STUDY_NUMBER",
				defaultParam:"userId,accountId,grpId,tabsubtype,revType",
				filterParam:"submissionType,revType,searchCriteria,studyPI,ecomp,reviewSearch,CSRFToken",
				dataTable:'serverpagination_study',
				rowSelection:[5,10,15,25,50,75,100]
				});
	paginate_study.render();
		elem = document.getElementById("yui-gen2")


			 }
				 )

		titleLink=function(elCell, oRecord, oColumn, oData)
		{
		var studyTitle=oRecord.getData("STUDY_TITLE");
		var reg_exp = /\'/g;
		studyTitle = studyTitle.replace(reg_exp, "\\'");
		reg_exp = /[\r\n|\n]/g;
		studyTitle = studyTitle.replace(reg_exp, " ");
 		elCell.innerHTML="<a href=\"#\" onmouseover=\"return overlib('"+studyTitle+"',CAPTION,'<%=LC.L_Study_Title%><%--Study Title*****--%>');\" onmouseout=\"return nd();\"><img border=\"0\" src=\"./images/View.gif\" title=\"<%=LC.L_View%><%--View*****--%>\"/></a>";
		}

		studyNumber=function(elCell, oRecord, oColumn, oData)
		{
		var htmlStr="";
		var study=oRecord.getData("PK_STUDY");
		var studyNumber=oRecord.getData("STUDY_NUMBER");
		var submTypeSubtype=oRecord.getData("SUBMISSION_TYPE_SUBTYPE");
		var submTypeDesc=oRecord.getData("SUBMISSION_TYPE_DESC");
		if (!study) study="";
		if (!studyNumber) studyNumber="";
		var fkFormlib = oRecord.getData("FK_FORMLIB");
		var lfEntrychar = oRecord.getData("LF_ENTRYCHAR");
		var savedcount = oRecord.getData("SAVEDCOUNT");
		if (!fkFormlib) fkFormlib="";
		if (!lfEntrychar) lfEntrychar="";
		if (!savedcount) savedcount="";
		var formValue = fkFormlib+'*'+lfEntrychar+'*'+savedcount;

		<% if ("irb_saved_tab".equals(selectedTab)) { %>
	           if ('new_app' == submTypeSubtype || 'study_amend' == submTypeSubtype ) {
//	    htmlStr="<A onClick='return checkEdit();' HREF=\"irbnewcheck.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_check_tab&mode=M&submissionType=new_app&studyId="+study+"\">"
//	        +studyNumber+"</A> ";
		<%if ("LIND".equals(CFG.EIRB_MODE)) {%>
 		htmlStr="<A href=\"flexStudyScreen?mode=M&srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&studyId="+study+"\" id="+study+" onClick='return checkEditAndLoad(\"flexStudyScreen?mode=M&srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&studyId="+study+"\")'>"
	        +studyNumber+"</A> ";
	    <%}else{%>
	    htmlStr="<A href=\"irbnewinit.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=M&studyId="+study+"&submissionType="+submTypeSubtype+"\" onClick='return checkEditAndLoad(\"irbnewinit.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=M&studyId="+study+"&submissionType="+submTypeSubtype+"\")'>"
        +studyNumber+"</A> ";
	    <%}%>
	           } else {
	    htmlStr="<A href=\"formfilledstudybrowser.jsp?selectedTab=8&tabsubtype=irb_ongoing_menu&mode=M&formValue="+formValue+"&studyId="+study+"\" onClick='return checkEditAndLoad(\"formfilledstudybrowser.jsp?selectedTab=8&tabsubtype=irb_ongoing_menu&mode=M&formValue="+formValue+"&studyId="+study+"\")'>"
	    	+studyNumber+"</A> ";
		       }
		<% } else { %>
               if ('new_app' == submTypeSubtype) {
                   <% if ("irb_act_tab".equals(selectedTab)){%>
                   htmlStr="<A href=\"study.jsp?srcmenu=tdMenuBarItem3&selectedTab=1&mode=M&studyId="+study+"\" class=\"studyMenuPop\" onClick='return checkEditAndLoad(\"study.jsp?srcmenu=tdMenuBarItem3&selectedTab=1&mode=M&studyId="+study+"\")'>"
                   +studyNumber+"</A> ";
                   <%}else {
                   if("LIND".equalsIgnoreCase(CFG.EIRB_MODE)){%>
 		htmlStr="<A href=\"study.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=M&studyId="+study+"&submissionType="+submTypeSubtype+"\" id="+study+" class=\"studyMenuPop\" onClick='return checkEditAndLoad(\"study.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=M&studyId="+study+"&submissionType="+submTypeSubtype+"\")'>"
		    +studyNumber+"</A> ";
		    <%} else {%>
		    htmlStr="<A href=\"irbnewinit.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=M&studyId="+study+"&submissionType="+submTypeSubtype+"\" id="+study+"  class=\"studyMenuPop\" onClick='return checkEditAndLoad(\"irbnewinit.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=M&studyId="+study+"&submissionType="+submTypeSubtype+"\")'>"
		    +studyNumber+"</A> ";
               
                   <%}}%>
               }else {
        htmlStr="<A href=\"study.jsp?srcmenu=tdMenuBarItem3&selectedTab=1&mode=M&studyId="+study+"\" id="+study+" class=\"studyMenuPop\" onClick='return checkEditAndLoad(\"study.jsp?srcmenu=tdMenuBarItem3&selectedTab=1&mode=M&studyId="+study+"\")'>"
            +studyNumber+"</A> ";
               }
		<% }  %>
 		elCell.innerHTML=htmlStr;
		}

		actionLink=function(elCell, oRecord, oColumn, oData)
		{
		var htmlStr="";
		var study=oRecord.getData("PK_STUDY");
		var pkSubmission = oRecord.getData("PK_SUBMISSION");
		var submissionBoardPK = oRecord.getData("PK_SUBMISSION_BOARD");
		var pksubmissionStatus = oRecord.getData("PK_SUBMISSION_STATUS");
        var submission_statussubtyp= oRecord.getData("STATUS_SUBTYP");
        var submission_type = oRecord.getData("SUBMISSION_TYPE");
		if (!study) study="";
		if (!pkSubmission) return;
		if (!submissionBoardPK) return;
         if('approved'!= submission_statussubtyp){
 		htmlStr="<A title=\"<%=LC.L_Action%><%--Action*****--%>\" HREF=\"javascript:void(0);\" onclick=\"openActionWin("+study+","+pkSubmission+","+submissionBoardPK+","+pksubmissionStatus+","+submission_type+",'<%=selectedTab%>')\">"+
		"<img src=\"../images/jpg/action.gif\" border=\"0\" align=\"left\"/></A> ";}
 		elCell.innerHTML=htmlStr;
		}


		form_submlink=function(elCell, oRecord, oColumn, oData)
		{
		var htmlStr="";
		var study=oRecord.getData("PK_STUDY");
		var pkSubmission = oRecord.getData("PK_SUBMISSION");
		var submissionBoardPK = oRecord.getData("PK_SUBMISSION_BOARD");
		var appSubmissionType	= oRecord.getData("SUBMISSION_TYPE_SUBTYPE");
		var pksubmissionStatus = oRecord.getData("PK_SUBMISSION_STATUS");
		var reviewtypdesc   = oRecord.getData("REVIEW_TYPE");
		var submission_type = oRecord.getData("SUBMISSION_TYPE");
		if (!study) study="";
		<%if(("irbPend".equals(moduleName) && selectedTab.equals("irb_items_tab")) || ("irbPend_final".equals(moduleName) && selectedTab.equals("irb_appr_tab")) || ("irbPend_finalforsubmission".equals(moduleName) && selectedTab.equals("irb_appr_tab"))) {%>
 		htmlStr=pkSubmission+"  <A title=\"<%=LC.L_Submpack%><%--Forms*****--%>\" HREF=\"javascript:void(0);\" onclick=\"openFormsWin('"+ $('tabsubtype').value+"', "+study+","+pkSubmission+","+submissionBoardPK+",'"+appSubmissionType+"','"+reviewtypdesc+"','"+pksubmissionStatus+"',"+submission_type+",'fromPend')\">"+
		"<img src=\"./images/Form.gif\" border=\"0\" align=\"left\"/></A> ";
		<%}else {%>
		htmlStr=pkSubmission;
		<%}%>
		elCell.innerHTML=htmlStr;
		}
        
		formsLink=function(elCell, oRecord, oColumn, oData)
		{
		var htmlStr="";
		var study=oRecord.getData("PK_STUDY");
		var pkSubmission = oRecord.getData("PK_SUBMISSION");
		var submissionBoardPK = oRecord.getData("PK_SUBMISSION_BOARD");
		var appSubmissionType	= oRecord.getData("SUBMISSION_TYPE_SUBTYPE");
		var pksubmissionStatus = oRecord.getData("PK_SUBMISSION_STATUS");
		var reviewtypdesc   = oRecord.getData("REVIEW_TYPE");
		var submission_type = oRecord.getData("SUBMISSION_TYPE");
		if (!study) study="";

 		htmlStr="<A title=\"<%=LC.L_Forms%><%--Forms*****--%>\" HREF=\"javascript:void(0);\" onclick=\"openFormsWin('"+ $('tabsubtype').value+"', "+study+","+pkSubmission+","+submissionBoardPK+",'"+appSubmissionType+"','"+reviewtypdesc+"','"+pksubmissionStatus+"',"+submission_type+")\">"+
		"<img src=\"./images/Form.gif\" border=\"0\" align=\"left\"/></A> ";
 		elCell.innerHTML=htmlStr;
		}

		reviewLink=function(elCell, oRecord, oColumn, oData)
		{
		var htmlStr="";
		var study=oRecord.getData("PK_STUDY");
		var pkSubmission = oRecord.getData("PK_SUBMISSION");
		var submissionBoardPK = oRecord.getData("PK_SUBMISSION_BOARD");
		var appSubmissionType	= oRecord.getData("SUBMISSION_TYPE_SUBTYPE");
		var pksubmissionStatus = oRecord.getData("PK_SUBMISSION_STATUS");
		var submission_type = oRecord.getData("SUBMISSION_TYPE");

		if (!study) study="";

 		htmlStr="<A title=\"<%=LC.L_Review%><%--Review***** --%>\" HREF=\"javascript:void(0);\" onclick=\"openReviewWin('"+ $('tabsubtype').value+"', "+study+","+pkSubmission+","+submissionBoardPK+",'"+appSubmissionType+"',"+pksubmissionStatus+","+submission_type+")\">"+
		"<img src=\"./images/Review.gif\" border=\"0\" align=\"left\"/></A> ";
 		elCell.innerHTML=htmlStr;
		}

		meetingLink=function(elCell, oRecord, oColumn, oData)
		{
		var htmlStr="";
		var study=oRecord.getData("PK_STUDY");
		var pkSubmission = oRecord.getData("PK_SUBMISSION");
		var submissionBoardPK = oRecord.getData("PK_SUBMISSION_BOARD");
		var revBoard = oRecord.getData("FK_REVIEW_BOARD");
		 var meetingDatePK = oRecord.getData("FK_REVIEW_MEETING");
		 var meeting_date = oRecord.getData("MEETING_DATE"); 
		if (!study) study="";
		if (!pkSubmission) return;
		if (!submissionBoardPK) return;
		
                
		if(meetingDatePK){
		    htmlStr="<A HREF=\"javascript:void(0);\" onclick=\"openMeetingMom("+revBoard+","+meetingDatePK+")\">"+
		    meeting_date+"</A> ";
				}
		elCell.innerHTML=htmlStr;
		}

		notesLink=function(elCell, oRecord, oColumn, oData)
		{
		var submNotes=oRecord.getData("SUBMISSION_NOTES");
		var pkSubmission = oRecord.getData("PK_SUBMISSION");
		var submissionStatusPK = oRecord.getData("PK_SUBMISSION_STATUS");
		if (!submNotes) submNotes = "";
		var reg_exp = /\'/g;
		var reg_dbqt = /\"/g;
		var reg_lt  = /&lt;/g;
		var reg_gt  = /&gt;/g;
		var reg_cr  = /\r/g;
		var reg_lf  = /\n/g;
		submNotes = submNotes.replace(reg_exp, "\\'").replace(reg_dbqt, "&quot;").replace(reg_lt, "<").replace(reg_gt, ">")
		                     .replace(reg_cr, "").replace(reg_lf, "<br/>");
 		elCell.innerHTML="<a href=\"#\" onclick=\"openNotesWindow("+pkSubmission+","+submissionStatusPK+")\" onmouseover=\"return overlib('"+submNotes+"',CAPTION,'<%=LC.L_Notes%><%--Notes*****--%>');\" onmouseout=\"return nd();\"><%=LC.L_View_Notes%><%--View Notes*****--%></a>";
		}

		sendLink=function(elCell, oRecord, oColumn, oData)
		{
		var htmlStr="";
		var study=oRecord.getData("PK_STUDY");
		var pkSubmission = oRecord.getData("PK_SUBMISSION");
		var submissionBoardPK = oRecord.getData("PK_SUBMISSION_BOARD");
		var submTypeSubtype=oRecord.getData("SUBMISSION_TYPE_SUBTYPE");
		var statusid=oRecord.getData("PK_SUBMISSION_STATUS");
		if (!study) study="";
		if (!pkSubmission) return;
		if (!submissionBoardPK) return;
		
		<%if("LIND".equals(CFG.EIRB_MODE)){ %>
		if ('new_app' == submTypeSubtype || 'study_amend' == submTypeSubtype){
			htmlStr="<A title=\"<%=LC.L_Action%><%--Action*****--%>\" HREF=\"javascript:void(0);\">"+
			"<img src=\"../images/jpg/action.gif\" border=\"0\" align=\"left\" onclick=\"window.location='studyCheckNSubmit?mode=M&srcmenu=tdmenubaritem3&studyId="+study+"'\"/></A> ";
		}else{
	 		htmlStr="<A title=\"<%=LC.L_Action%><%--Action*****--%>\" HREF=\"javascript:void(0);\" onclick=\"openSendBackWin("+study+","+pkSubmission+","+submissionBoardPK+","+statusid+",'<%=selectedTab%>')\">"+
			"<img src=\"../images/jpg/action.gif\" border=\"0\" align=\"left\"/></A> ";
		}
		<%}else{%>
			htmlStr="<A title=\"<%=LC.L_Action%><%--Action*****--%>\" HREF=\"javascript:void(0);\" onclick=\"openSendBackWin("+study+","+pkSubmission+","+submissionBoardPK+","+statusid+",'<%=selectedTab%>')\">"+
			"<img src=\"../images/jpg/action.gif\" border=\"0\" align=\"left\"/></A> ";
		<%}%>
 		elCell.innerHTML=htmlStr;
		}

		letterLink=function(elCell, oRecord, oColumn, oData)
		{
		var htmlStr="";
		var study=oRecord.getData("PK_STUDY");
		var pkSubmission = oRecord.getData("PK_SUBMISSION");
		var submissionBoardPK = oRecord.getData("PK_SUBMISSION_BOARD");
		var submissionStatus = oRecord.getData("SUBMISSION_STATUS");
		var pksubmissionStatus = oRecord.getData("PK_SUBMISSION_STATUS");
		var submission_type = oRecord.getData("SUBMISSION_TYPE");
		var templateflag = oRecord.getData("TEMPLATE_FLAG");
		if(!templateflag)templateflag="";
		if (!study) study="";
		if (!pkSubmission) return;
		if (!submissionBoardPK) return;
		if (!submissionStatus) return;
 		htmlStr="<A title=\"<%=LC.L_Outcome_Letter%><%--Outcome Letter*****--%>\" HREF=\"javascript:void(0);\" onclick=\"openLetterWin("+study+","+pkSubmission+","+submissionBoardPK+","+submissionStatus+","+pksubmissionStatus+","+submission_type+",'"+templateflag+"','<%=selectedTab%>')\">"+
		"<img src=\"../images/jpg/OutcomeLetter.gif\" border=\"0\" align=\"left\"/></A> ";
 		elCell.innerHTML=htmlStr;
		}

		deleteLink=function(elCell, oRecord, oColumn, oData)
		{
		var htmlStr="";
		var study=oRecord.getData("PK_STUDY");
		var studyNumber=oRecord.getData("STUDY_NUMBER");
		var pkSubmission = oRecord.getData("PK_SUBMISSION");
		var submissionBoardPK = oRecord.getData("PK_SUBMISSION_BOARD");
		var submissionStatus = oRecord.getData("SUBMISSION_STATUS");
		var pksubmissionStatus = oRecord.getData("PK_SUBMISSION_STATUS");
		if (!study) study="";
		if (!pkSubmission) return;
		if (!submissionBoardPK) return;
		if (!submissionStatus) return;

 		htmlStr="<A title=\"Delete\" HREF=\"javascript:void(0);\" onclick=\"openDeleteWin("+study+","+pkSubmission+","+submissionBoardPK+","+submissionStatus+","+pksubmissionStatus+",'"+studyNumber+"','<%=selectedTab%>')\">"+
 		"<img src=\"./images/delete.gif\" border=\"0\" align=\"left\"/></A> ";
 		elCell.innerHTML=htmlStr;
		}

		historyLink=function(elCell, oRecord, oColumn, oData)
		{
		var htmlStr="";
		var study=oRecord.getData("PK_STUDY");
		var pkSubmission = oRecord.getData("PK_SUBMISSION");
		var submissionBoardPK = oRecord.getData("PK_SUBMISSION_BOARD");
		var appSubmissionType = oRecord.getData("SUBMISSION_TYPE_SUBTYPE");
		var submissionStatus = oRecord.getData("SUBMISSION_STATUS_DESC");

		if (!study) study="";
		if (!submissionStatus) submissionStatus="";
		<% if ("irb_act_tab".equals(selectedTab) || "irb_items_tab".equals(selectedTab)|| "irb_saved_tab".equals(selectedTab)) {%>
 		htmlStr=submissionStatus;
 		<%} else{%>
 		htmlStr=submissionStatus+" <A title=\"<%=LC.L_History%><%--History*****--%>\" HREF=\"javascript:void(0);\" onclick=\"openHistoryWin('"+ $('tabsubtype').value+"', "+study+","+pkSubmission+","+submissionBoardPK+",'"+appSubmissionType+"')\">"+
		"<%=LC.L_H%><%--H*****--%></A> ";
 		<%}%>
		elCell.innerHTML=htmlStr;
 		
		}		

		function openReviewWin(selectedTab,study,pkSubmission,submissionBoardPK,appSubmissionType,pksubmissionStatus,submission_type)
		{
			if (checkSubmissionRight('E') == false)
			 {
			 	return false;
			 }
			   var w = screen.availWidth-100;
			   windowName = window.open("irbReview.jsp?appSubmissionType="+appSubmissionType+"&studyId="+study+"&submissionPK="+pkSubmission+"&submissionBoardPK="+ submissionBoardPK+"&tabsubtype="+selectedTab+"&pksubmissionStatus="+pksubmissionStatus+"&submission_type="+submission_type+"&hiddenflag=N&hide=N&link=N&provisoflag=N","revWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width="+w+",height=600,left=10,top=100");










			   windowName.focus();
  		}

		function openActionWin(study,pkSubmission,submissionBoardPK,pksubmissionStatus,submission_type,selectedTab)
		{
			if (checkSubmissionRight('E') == false)
			 {
			 	return false;
			 }

			   windowName = window.open("irbactionwin.jsp?&studyId="+study+"&submissionPK="+pkSubmission+"&submissionBoardPK="+ submissionBoardPK+"&pksubmissionStatus="+pksubmissionStatus+"&submission_type="+submission_type+"&selectedTab="+selectedTab,"actionWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=900,height=700,left=10,top=100");
			   windowName.focus();
  		}

		function openSendBackWin(study,pkSubmission,submissionBoardPK,statusid,selectedTab)
		{
			if (checkSubmissionRight('E') == false)
			 {
			 	return false;
			 }

			   windowName = window.open("irbsendback.jsp?&studyId="+study+"&submissionPK="+pkSubmission+"&submissionBoardPK="+ submissionBoardPK+"&selectedTab="+selectedTab+"&statusid="+statusid,"actionWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=10");
			   windowName.focus();
  		}

		function openLetterWin(study,pkSubmission,submissionBoardPK,submissionStatus,pksubmissionStatus,submission_type,templateflag,selectedTab)
		{
			if (checkSubmissionRight('E') == false)
			 {
			 	return false;
			 }
             if(templateflag==''){
			
			   windowName = window.open("irbletter.jsp?&studyId="+study+"&submissionPK="+pkSubmission+"&submissionBoardPK="+ submissionBoardPK+"&submissionStatus="+submissionStatus+"&pksubmissionStatus="+pksubmissionStatus+"&submission_type="+submission_type+"&selectedTab="+selectedTab,"actionWin","toolbar=no,scrollbars=no,resizable=yes,menubar=no,status=yes,width=700,height=700,left=10,top=10");
			   windowName.focus();
             }
             else{
            	 windowName = window.open("postFileDownload.jsp?&tableName=ER_SUBMISSION_STATUS&columnName=FILE_DATA&pkColumnName=pk_submission_status&module=study&db=eres&pkValue="+pksubmissionStatus+"&file="+pksubmissionStatus+".pdf&templateflag="+templateflag,"actionWin","toolbar=no,scrollbars=no,resizable=yes,menubar=no,status=yes,width=700,height=700,left=10,top=10");
            	 windowName.focus(); 
                 }
             var timer = setInterval(function () {
                 if (windowName.closed) {
                     clearInterval(timer);
                     window.location.reload(); // Refresh the parent page
                 }
             }, 100);
  		}
		function openDeleteWin(study,pkSubmission,submissionBoardPK,submissionStatus,pksubmissionStatus,studyNumber,selectedTab)
		{
			if (checkSubmissionRight('E') == false)
			 {
			 	return false;
			 }
			var result = confirm("Do you want to delete the Final outcome status for "+studyNumber+"?");
			if (result) {

				 windowName = window.open("updateSubmissionStatus.jsp?&studyId="+study+"&submissionPK="+pkSubmission+"&submissionBoardPK="+ submissionBoardPK+"&submissionStatus="+submissionStatus+"&pksubmissionStatus="+pksubmissionStatus+"&selectedTab="+selectedTab+"&hiddenflag=N","actionWin","toolbar=no,scrollbars=no,resizable=yes,menubar=no,status=yes,width=700,height=700,left=10,top=10");
				   windowName.focus();
			}

			  
			   
  		}

		function openFormsWin(tabsubtype,study,pkSubmission,submissionBoardPK,appSubmissionType,reviewtypdesc,pksubmissionStatus,submission_type,fromPend)
		{
			 if (checkSubmissionRight('E') == false)
			 {
			 	return false;
			 }
			   var w = screen.availWidth-100;
			   windowName = window.open("irbReview.jsp?appSubmissionType="+appSubmissionType+"&tabsubtype="+tabsubtype+"&studyId="+study+"&submissionPK="+pkSubmission+"&submissionBoardPK="+ submissionBoardPK+"&pksubmissionStatus="+pksubmissionStatus+"&reviewtypdesc="+reviewtypdesc+"&submission_type="+submission_type+"&fromPend="+fromPend+"&hiddenflag=N&hide=N&newflag=N","revWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width="+w+",height=800,left=10,top=100");
			   windowName.focus();
  		}

		function openHistoryWin(tabsubtype,study,pkSubmission,submissionBoardPK,appSubmissionType)
		{
			 if (checkSubmissionRight('V') == false)
			 {
			 	return false;
			 }
			   var w = screen.availWidth-100;
			   windowName = window.open("irbhistory.jsp?appSubmissionType="+appSubmissionType+"&tabsubtype="+tabsubtype+"&studyId="+study+"&submissionPK="+pkSubmission+"&submissionBoardPK="+ submissionBoardPK,"revWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=100");
			   windowName.focus();
  		}

		function openNotesWindow(pkSubmission,submissionStatusPK)
		{
			if (checkSubmissionRight('V') == false)
			{
				return false;
			}
			windowName = window.open("irbnotes.jsp?submissionPK="+pkSubmission+"&submissionStatusPK="+submissionStatusPK,"actionWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=600,height=400,left=100,top=10");
			windowName.focus();
	    }
		function openMeetingMom(revBoard,meetingDatePK)
		{
			
			if (checkSubmissionRight_meeting('E') == false)
			 {
			 	return false;
			 }
			 
			  
			  
			   windowName = window.open("irbMom.jsp?revBoard="+revBoard+"&meetingDatePK="+meetingDatePK,"momWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=750,height=650,left=20,top=20");
			   windowName.focus();
			   
		}
		function checkSubmissionRight_meeting(mode)
  		{
  			var elem = document.getElementById("pageRight_meeting");
			var submissionBrowserRight ;
			 
			 if (elem != null)
			 {
			 	submissionBrowserRight = elem.value;
			 }
			 
			 
		 	if (f_check_perm(submissionBrowserRight,mode) == false)	{
				return false;
			}
  		}
  		function checkSubmissionRight(mode)
  		{
  			var elem = document.getElementById("submissionBrowserRight");
			var submissionBrowserRight ;

			 if (elem != null)
			 {
			 	submissionBrowserRight = elem.value;
			 }


		 	if (f_check_perm(submissionBrowserRight,mode) == false)	{
				return false;
			}
  		}
  	function checkEdit()
  	{
  		if (checkSubmissionRight('E') == false)
			 {
			 	return false;
			 }

  	}

    function checkEditAndLoad(nextLocation)
    {
  		if (checkSubmissionRight('E') == false)
		{
			return false;
		}
		window.location.href = nextLocation;
    }

  //Added by dharam to implement user details mouse over link	
	enterbyLink = 
		function(elCell, oRecord, oColumn, oData)
		{
		var entered_by=oRecord.getData("ENTERED_BY");
		var entbyDetails=oRecord.getData("ENTR_BY_DET");
		//jQuery(".yui-dt-col-PI_NAME").css("width","15%");
			if(entered_by!='' && entbyDetails!=null){
				entbyDetails=escapeSingleQuoteEtc(entbyDetails);
 				elCell.innerHTML=entered_by+"<a href=\"#\"><img src=\"./images/View.gif\" border=\"0\" align=\"left\" style=\"float: right;\" onmouseover=\"return overlib(htmlEncode('"+entbyDetails+"'),ABOVE,CAPTION,'');\" onmouseout=\"nd();\" > </a>";
			}
		}
	prinvLink = 
		function(elCell, oRecord, oColumn, oData)
		{
		var PIs_By=oRecord.getData("STUDY_PRINV_NAME");
		var piDetails=oRecord.getData("PI_DETAILS");
		//jQuery(".yui-dt-col-PI_NAME").css("width","15%");
			if(PIs_By!='' && piDetails!=null){
				piDetails=escapeSingleQuoteEtc(piDetails);
 				elCell.innerHTML=PIs_By+"<a href=\"#\"><img src=\"./images/View.gif\" border=\"0\" align=\"left\" style=\"float: right;\" onmouseover=\"return overlib(htmlEncode('"+piDetails+"'),ABOVE,CAPTION,'');\" onmouseout=\"nd();\" > </a>";
			}
		}
	asgntoLink =
		function(elCell, oRecord, oColumn, oData)
		{
		var asgnBy=oRecord.getData("ASSIGNED_TO");
		var asgntoDetails=oRecord.getData("ASGN_TO_DET");
		//jQuery(".yui-dt-col-PI_NAME").css("width","15%");
			if(asgnBy!='' && asgntoDetails!=null){
				asgntoDetails=escapeSingleQuoteEtc(asgntoDetails);
 				elCell.innerHTML=asgnBy+"<a href=\"#\"><img src=\"./images/View.gif\" border=\"0\" align=\"left\" style=\"float: right;\" onmouseover=\"return overlib(htmlEncode('"+asgntoDetails+"'),ABOVE,CAPTION,'');\" onmouseout=\"nd();\" > </a>";
			}
		}
    
 </script>

<body class="yui-skin-sam">

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/><!--km-->
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/><!--km-->
<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.service.util.StringUtil,java.sql.*,com.velos.eres.business.common.*"%>




<%

 HashMap hmDisplayLabel = new HashMap();

 hmDisplayLabel.put("irb_new_tab",""+MC.M_NewSubRecv_Rev/*New Submissions Received for Review*****/+"");
 hmDisplayLabel.put("irb_assigned_tab",""+MC.M_SubAssg_AdminRev/*Submissions Assigned for Administrative Review*****/+"");
 hmDisplayLabel.put("irb_compl_tab",""+MC.M_SubComp_AdminRev/*Submissions Completed Administrative Review*****/+"");
 hmDisplayLabel.put("irb_pend_rev",""+LC.L_SubPend_Rev/*Submissions Pending Review*****/+"");
 hmDisplayLabel.put("irb_post_tab",""+MC.M_SubComp_CmmtRev/*Submissions Completed Committee/Member Review*****/+"");
 hmDisplayLabel.put("irb_pend_tab",""+MC.M_SubPend_PlResp/*Submissions Pending PI Response*****/+"");

 hmDisplayLabel.put("irb_act_tab",""+LC.L_Action_Required/*Action Required*****/+"");
 hmDisplayLabel.put("irb_items_tab",""+MC.M_ItemPend_RevApvl/*Items Pending Review/Approval*****/+"");
 hmDisplayLabel.put("irb_saved_tab",""+LC.L_Saved_Items/*Saved Items*****/+"");
 hmDisplayLabel.put("irb_appr_tab",""+LC.L_Final_Outcome/*Final outcome*****/+"");
 


if (sessionmaint.isValidSession(tSession)) {
    String userId ="";

    userId = (String) tSession.getValue("userId");
    String defUserGroup = (String) tSession.getAttribute("defUserGroup");
    
    String eComp=request.getParameter("ecomp");
    if(eComp==null || eComp=="" )
    {
    	eComp="1";
    	
    }


    String accountId = (String) tSession.getValue("accountId");
    String tabsubtype  = "";

    tabsubtype = request.getParameter("tabsubtype");

    String hideRevTypeFilter = request.getParameter("hideRevTypeFilter");;
    if (StringUtil.isEmpty(hideRevTypeFilter))
    {
    	hideRevTypeFilter = "N";
    }

    String submissionBrowserRight = request.getParameter("submissionBrowserRight");
      
    if (StringUtil.isEmpty(submissionBrowserRight))
    {
    	submissionBrowserRight = "0";
    }

    String revType = request.getParameter("revType");
    if (StringUtil.isEmpty(revType))
    {
    	revType= "0";
    }
    String submissionType = request.getParameter("submissionType");
    if (StringUtil.isEmpty(submissionType))
    {
    	submissionType= "0";
    }


    String displayLabel = (String) hmDisplayLabel.get(tabsubtype);
    if (displayLabel == null) { displayLabel = ""; }

    //Added by Manimaran to give access right for default admin group to the delete link
	int usrId = EJBUtil.stringToNum(userId);
	userB.setUserId(usrId);
	userB.getUserDetails();

	String ddSubType="";
	String ddRevType="";

	 int pageRight = 0;
     pageRight = 7;


	 CodeDao cdDD = new CodeDao();

   	cdDD.getCodeValues("submission",EJBUtil.stringToNum(accountId));
   	cdDD.setCType("submission");
	cdDD.setForGroup(defUserGroup);

	 ddSubType = cdDD.toPullDown("submissionType id='submissionType'",submissionType);

	 cdDD.resetDao();

    cdDD.getCodeValues("revType",EJBUtil.stringToNum(accountId));
   	cdDD.setCType("revType");
	cdDD.setForGroup(defUserGroup);

	 ddRevType = cdDD.toPullDown("revType id='revType'",revType);


    if (pageRight > 0 )    {



    %>

  <Form name="frmSubmission" autocomplete="off" method="POST" >
	<input type="hidden" name="ecomp" id="ecomp" value="<%=eComp %>" />
    <input type="hidden" id="accountId" value="<%=accountId%>">
    <input type="hidden" id="userId" value="<%=userId%>">
    <input type="hidden" id="grpId" value="<%=defUserGroup%>">

    <input type="hidden" id="tabsubtype" value="<%=tabsubtype%>">
    <input type="hidden" id="submissionBrowserRight" value="<%=submissionBrowserRight%>">
    <input type="hidden" id="pageRight_meeting" value="<%=pageRight_meeting%>">
    <input type="hidden" id="CSRFToken" name="CSRFToken">
    
	<%if(displayLabel != null && displayLabel.length() != 0){ %>
    <table cellspacing="0" cellpadding="0" border="0" width="99%">
    	<tr><td><p class="sectionHeadings lhsFont">&nbsp;&nbsp;<%=displayLabel %></p></td></tr>
    </table>
	<%} %>
    <table cellspacing="0" cellpadding="0" border="0" class="basetbl outline midAlign" width="99%">
    	<tr  >
            <td><%=LC.L_Filter_By%><%--Filter By*****--%></td>
    		<td><%=LC.L_Submission_Type%><%--Submission Type*****--%></td>
    		<td>
    			&nbsp;<%=ddSubType%>
    	    </td>

    	<% if (hideRevTypeFilter.equals("N")) { %>
    		<td><%=LC.L_Review_Type%><%--Review Type*****--%></td>

    		<td>
    				&nbsp;<%=ddRevType%>
      	    </td>
      	    <%   }  //hide filter = N
      	    	else
      	    	{
      	    		%>
      	    			<input type="hidden" name="revType" id='revType'  value="<%=revType%>">
      	    			<%
      	    	}
      	    	%>
    		<td><%=MC.M_Std_NumOrTitle%><%--<%=LC.Std_Study%> Number or Title*****--%>
    			  </td>
    		<td>
    		    <input type="text" id="searchCriteria" name="searchCriteria" size="15" value="">


      	    </td>
      	    
      	    <% if (!hideRevTypeFilter.equals("N")) { %>
      	    <td><%=LC.L_Reviewer %><%--Reviewer --%>	</td>
      	    
      	    <td><input type="text" id="reviewSearch" name="reviewSearch" size="15" value=""></td>
      	    <%} %>




	      	  <td>&nbsp;<button type="button" onClick="paginate_study.runFilter();"><%=LC.L_Search%></button>	</td>
    	</tr>
    			  <input type=hidden id = "studyPI" name="studyPI" value=''>

    				<% if (originalTabType.equals("irbSub") && selectedTab.equals("irb_appr_tab"))
					{ %>
			    		<tr>
			    				<td colspan=6 align="right"><%=LC.L_Search_ByPi%><%--Search by PI*****--%></td>
			    				<td colspan=2>
								<input type=text name="studyPIName" value='' readonly >
						  		<A HREF="#" onClick='openCommonUserSearchWindow("frmSubmission","studyPI","studyPIName")' ><%=LC.L_Select_User%><%--Select User*****--%></A>


			      	    </td>
			    				</tr>
			    	<% } %>
    	</table>



        <div >
    <div id="serverpagination_study" ></div>
    </div>





  </Form>

<script>
	 //	alert(paginate_study);
		//if (paginate_study) paginate_study.runFilter();

	</script>
    <%
    } //end of if body for page right
    else{
%>
    <%@include file="accessdenied.jsp" %>
<%
    } //end of else body for page right
}//end of if body for session
else {
%>

    <jsp:include page="timeout.html" flush="true"/>
<%}
%>
<div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>