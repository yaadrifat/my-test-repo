<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Horizontal_Rule%><%-- Horizontal Rule*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


<SCRIPT Language="javascript">

 function  validate(formobj)
 {
 
 	if (   !(validate_col ('Section', formobj.section)  )  )  return false
 	if (   !(validate_col ('Sequence', formobj.sequence)  )  )  return false	
	
 	if(formobj.codeStatus.value != "WIP"){	
 	 	if (  !(validate_col('e-Signature',formobj.eSign)     )   ) return false
 		 
 	<%-- 	if(isNaN(formobj.eSign.value) == true) 
 			{
 			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
 			formobj.eSign.focus();
 			 return false;
 	   		} --%>
 	 	}

	if(isNaN(formobj.sequence.value) == true) 
	{
		alert("<%=MC.M_SecSeqHasToNum_ReEtr%>");/*alert("Section Sequence has to be a number. Please enter again.");*****/
		formobj.sequence.focus();
		 return false;
   	}
	
	
  }
  
 

</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="formSecJB" scope="request"  class="com.velos.eres.web.formSec.FormSecJB"/>
<jsp:useBean id="formFieldJB" scope="request"  class="com.velos.eres.web.formField.FormFieldJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*"%>

<body>
<DIV class="popDefault" id="div1"> 
 <%

	HttpSession tSession = request.getSession(true);
 	String codeStatus = request.getParameter("codeStatus");
   if (sessionmaint.isValidSession(tSession))
   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
<%
	
	int pageRight = 0;	
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));
	if (pageRight >= 4)
	{
	
		String mode=request.getParameter("mode");
		
		String fieldLibId="";
		String formId="";
		String formFldId="";
		String formSecId="";
		String fldSeq="";
		
		String fldType="";
		formId=request.getParameter("formId");		
		String accountId=(String)tSession.getAttribute("accountId");
		int tempAccountId=EJBUtil.stringToNum(accountId);
		
		ArrayList idSec= new ArrayList();
		ArrayList descSec= new ArrayList();
				
		String pullDownSection;
		FormSecDao formSecDao= new FormSecDao();
		formSecDao = formSecJB.getAllSectionsOfAForm( EJBUtil.stringToNum(formId));
		idSec= formSecDao.getFormSecId();
		descSec=formSecDao.getFormSecName();
		
		pullDownSection=EJBUtil.createPullDown("section", 0, idSec, descSec);
						
		if (mode.equals("M"))
		{ 
				
				formId=request.getParameter("formId");		
				formFldId=request.getParameter("formFldId");
			    				
				formFieldJB.setFormFieldId(Integer.parseInt(formFldId) );
				formFieldJB.getFormFieldDetails();
				fieldLibId=formFieldJB.getFieldId();
				
				fieldLibJB.setFieldLibId(Integer.parseInt(fieldLibId));
				fieldLibJB.getFieldLibDetails();
			
				formSecId=formFieldJB.getFormSecId();  
				
				//TO GIVE A SINGLE SECTION IN EDIT MODE
				ArrayList idSecNew = new ArrayList();
				ArrayList descSecNew = new ArrayList();
				int secId =EJBUtil.stringToNum(formSecId) ;
				 for(int j = 0 ; j < idSec.size() ; j++)
				 {
				 	int  secIdNew =(   (Integer) idSec.get(j)  ).intValue();
					if ( secIdNew  ==   secId )
					{
				 		idSecNew.add(   (  Integer )idSec.get(j) );
						descSecNew.add( (String )descSec.get(j) ) ;
						break ;	
					}
				 }
				
				/// TO MAKE A READONLY BOX FOR NOT LETTING THE EDITING OF THE SECTION FOR FIELD IN EDIT MODE 
				String sectionN = (String)descSecNew.get(0) ;
				pullDownSection = "<label>"+sectionN+"</label>" 
				 							+ "<Input type=\"hidden\" name=\"section\" value="+(Integer)idSecNew.get(0)+" >  " ;
				
				  
			//	pullDownSection=EJBUtil.createPullDown("section", secId,idSecNew, descSecNew   );
				
				
				
				fldSeq=((formFieldJB.getFormFldSeq()) == null)?"":(formFieldJB.getFormFldSeq()) ;
				
				fldType=fieldLibJB.getFldType();
				
	
				
		}
// #5437,5438 02/02/2011 @Ankit		
if(((pageRight == 6 && mode.equals("M") ) || (pageRight == 5 && mode.equals("N")) || pageRight == 7) && (mode.equals("M") && (codeStatus.equals("WIP") || codeStatus.equals("Offline")  ))|| mode.equals("N")){%>

    <Form name="horizontalType" id="horizontalTypeId" method="post" action="horizontalRuleTypeSubmit.jsp" onsubmit="if (validate(document.horizontalType)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<%}else{%>
	<Form name="horizontalType" method="post" onsubmit = "return false">
	<%}%>
	<table width="80%" cellspacing="0" cellpadding="0" border="0">
  	<tr>
		<td ><P class="sectionHeadings"> <%=MC.M_FldType_HorzRule%><%--Field Type: Horizontal Rule*****--%> </P> </td>
	 </tr>   
	 </table>
	
	<Input type="hidden" name="mode" value=<%=mode%> >
	<Input type="hidden" name="codeStatus" value=<%=codeStatus%>>
	<Input type="hidden" name="formId" value=<%=formId%> >  
	<Input type="hidden" name="formFldId" value=<%=formFldId%> >  
	<br>
	<br>
	<br>
	<table width="80%" cellspacing="0" cellpadding="0" border="0"  >
		<tr> 
      <td width="20%"><%=LC.L_Section%><%-- Section*****--%><FONT class="Mandatory">* </FONT></td>
        <td width="60%"><%=pullDownSection%> </td>
	  </tr>
	  <tr colspan="2"><td></td></td></tr> 
	  <tr colspan="2"><td></td></tr>
	  <tr colspan="2"><td></td></tr>
	<tr> 
      <td width="20%"><%=LC.L_Sequence%><%-- Sequence*****--%><FONT class="Mandatory">* </FONT></td>
        <td width="60%"><input type="text" name="sequence" size = 10 MAXLENGTH = 10 value="<%=fldSeq%>"> </td>
	  </tr>
	</table>
	<br>
<%//out.println("codeStatus"+codeStatus);%>
	<br>
	<%// #5437,5438 02/02/2011 @Ankit
	if(((pageRight == 6 && mode.equals("M") ) || (pageRight == 5 && mode.equals("N")) || pageRight == 7) && (mode.equals("M") && (codeStatus.equals("WIP") || codeStatus.equals("Offline")  ))|| mode.equals("N")){
	if(codeStatus.equals("WIP")){
						%>
						<table align="center"><tr>
	  <td align="center"><br />
				<button type="submit" id="submit_btn" ondblclick="return false"><%=LC.L_Submit%></button>
			</td> 
	  </tr></table>
     
<%}else{ 
%>
<br />
	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="editBoxFieldFrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include> 


     <br>

 	 <table width="100%" cellspacing="0" cellpadding="0">
      <td align=right> 

      </td> 
      </tr>
  </table>
		<%}
	}%>
  </Form>
  
<%

	} //end of if body for page right
	else
	{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%

	} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

}

%>
<div class = "myHomebottomPanel"> 
 <jsp:include page="bottompanel.jsp" flush="true"/>
 </div> 
</body>

</html>


		
	
		
		
		
			
		
		
		
	




