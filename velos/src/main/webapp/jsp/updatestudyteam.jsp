<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id="team" scope="request" class="com.velos.eres.web.team.TeamJB"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyRights" scope="request" class="com.velos.eres.web.studyRights.StudyRightsJB"/>
<jsp:useBean id="ctrl" scope="session" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="stdSiteRightsB" scope="request" class="com.velos.eres.web.stdSiteRights.StdSiteRightsJB"/>
<jsp:useBean id="studySiteB" scope="request" class="com.velos.eres.web.studySite.StudySiteJB"/>
<jsp:useBean id="statusB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*"%>
	
	
<%
	int ret = 0;



	String stdId;

	int totrows =0,id; 



	String src = null;

	src=request.getParameter("src");

	String[] usrRole = null;

	String[] usrId = null;

	int cnt;

	int counter = 0;

	String[] rolearr = null;

	String[] userarr = null;

	String role= null;

	String user = null;

	String rights="";

	String tab = request.getParameter("selectedTab");
	HttpSession tSession = request.getSession(true);
	String accId = (String) tSession.getValue("accountId");
 	int iaccId = EJBUtil.stringToNum(accId);

	String right = request.getParameter("right");


String ipAdd = (String) tSession.getValue("ipAdd");
String usr = null;
usr = (String) tSession.getValue("userId");
int iusr = EJBUtil.stringToNum(usr);
//String eSign = request.getParameter("eSign");
if (sessionmaint.isValidSession(tSession))

{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	//String oldESign = (String) tSession.getValue("eSign");


	//if(!oldESign.equals(eSign)) {
%>
<%--   <jsp:include page="incorrectesign.jsp" flush="true"/>	 --%>
<%
	//} else {

	String study = (String) tSession.getValue("studyId");
	int studyId = EJBUtil.stringToNum(study);
	String userList = "";


	CodeDao cdao=new CodeDao();
	
	int codeId=cdao.getCodeId("teamstatus","Active");
	String statusCodeId=codeId+"";

	Date dt1 = new java.util.Date();
	String stDate = DateUtil.dateToString(dt1)+" "+StatusHistoryDao.getCurrTime();
		
	ctrl.getControlValues("study_rights");

	int rows = ctrl.getCRows();

	for(int count=0;count<rows;count++){ 

		rights = rights +"0";

	}

	//out.print("rigfhts" +rights);

 

	//id = Integer.parseInt(grpId);

	totrows = Integer.parseInt(request.getParameter("totalrows")); //Total Rows

//out.print("Total Rows" +totrows);

	if (totrows == 0 ){
		

		 usrRole = null;
		 

		 usrId = null;
		 
	} 
	
	if (totrows > 1 ) 

	{

		rolearr = request.getParameterValues("role");

		//userarr = request.getParameterValues("userId");
		userarr = request.getParameterValues("selectedUser");
	
		String[] assignarr = request.getParameterValues("selectedUser");
		
	  	for (cnt=0, counter=0 ; cnt < totrows;cnt ++)

        	{

		//	out.print("Check" +rolearr[cnt]);

            	if (!rolearr[cnt].equals(""))

	            {

				//out.print("Loop");

		      		
				team.setTeamUser(userarr[cnt]);

 				team.setStudyId(study);

				team.setTeamUserRole(rolearr[cnt]);
 				
				team.setCreator(usr);
				team.setIpAdd(ipAdd);
				team.setTeamUserType("D"); // D is for default team type
				team.setTeamStatus("Active");//km
				team.setTeamDetails();

				//Added by Manimaran for the July-August Enhancement S4.
				id = team.getId();
				String moduleId=id+"";
				statusB.setStatusModuleId(moduleId);
				statusB.setStatusModuleTable("er_studyteam");
				statusB.setStatusCodelstId(statusCodeId);
				statusB.setStatusStartDate(stDate);
				statusB.setCreator(usr);
				statusB.setIpAdd(ipAdd);
				//statusB.setModifiedBy(usr);
				statusB.setRecordType("N");
				statusB.setIsCurrentStat("1");
				statusB.setStatusHistoryDetailsWithEndDate();
				stdSiteRightsB.createStudySiteRightsData(EJBUtil.stringToNum(userarr[cnt]), iaccId, studyId, iusr, ipAdd );

				/*studyRights.setId(id);
				studyRights.setFtrRights(rights);
				studyRights.setModifiedBy(usr);
				studyRights.setIpAdd(ipAdd);

				studyRights.updateStudyRights(); */

		            //out.print("role[cnt]*" +rolearr[cnt] +"*userarr[cnt] " +userarr[cnt]);

                    	counter ++ ;

                }

        }
        
        for(int i =0 ; i< assignarr.length; i++)
        	{
        		if(i==0)
        			userList = assignarr[i].toString();
        		else
        			userList = userList + "," + assignarr[i].toString();
        	}
        	

    }

    else if(totrows == 1)

    {

	role = request.getParameter("role");

	user = request.getParameter("selectedUser");
	userList = user;


        if (role != ""){

		team.setTeamUser(user);

		team.setStudyId(study);

		team.setTeamUserRole(role);
		team.setCreator(usr);
		team.setModifiedBy(usr);
		team.setIpAdd(ipAdd);
		team.setTeamUserType("D"); // D is for default team type
		team.setTeamStatus("Active");//Added by Gopu Issue #2711
		team.setTeamDetails();

		id = team.getId();

		//Added by Gopu to fix the bugzilla Issue #2711
		id = team.getId();
		String moduleId=id+"";
		statusB.setStatusModuleId(moduleId);
		statusB.setStatusModuleTable("er_studyteam");
		statusB.setStatusCodelstId(statusCodeId);
		statusB.setStatusStartDate(stDate);
		statusB.setCreator(usr);
		statusB.setIpAdd(ipAdd);
		//statusB.setModifiedBy(usr);
		statusB.setRecordType("N");
		statusB.setIsCurrentStat("1");
		statusB.setStatusHistoryDetailsWithEndDate();		
		// 
		
		stdSiteRightsB.createStudySiteRightsData(EJBUtil.stringToNum(user), iaccId, studyId, iusr, ipAdd );
		
		/*studyRights.setId(id);
		studyRights.setFtrRights(rights);
		studyRights.setModifiedBy(usr);
		studyRights.setIpAdd(ipAdd);
		studyRights.updateStudyRights();  */
				

	  }
	}
		UserDao usrD = new UserDao();
		
     	usrD = userB.getUsersDetails(userList);
     	ArrayList arrSites = usrD.getUsrSiteIds();
        
      	
     	String siteId = "";
     	int count = 0;
     	StudySiteDao studySiteDao = new StudySiteDao();
     	for(int i=0; i<arrSites.size(); i++){
     		siteId = arrSites.get(i).toString();
	 		studySiteDao = studySiteB.getCntForSiteInStudy(studyId, EJBUtil.stringToNum(siteId));
	 		count = studySiteDao.getCRows();
	 	    
	 		if(count == 0){
	    		
	     		studySiteB.setSiteId(siteId);
	     		studySiteB.setStudyId(study);
	     		studySiteB.setCreator(usr);
	     		studySiteB.setRepInclude("1");
    	 		studySiteB.setIpAdd(ipAdd);
    	 		studySiteB.setStudySiteDetails();	    		
    	 		}
    	}
	    
	    
%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%-- Data was Saved successfully.*****--%> </p>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=teamBrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&right=<%=right%>&studyId=<%=studyId%>">

<%
//}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</BODY>

</HTML>





