<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<TITLE><%=LC.L_Dict_AndSettings%><%--Dictionaries and Settings*****--%></TITLE>
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>


<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>

<!--<Link Rel=STYLESHEET HREF="common.css" type=text/css>-->


<script>

function validate(formobj) {

  //JM: 15Nov2010: #5407  
    if ((formobj.pgRight.value) > 5){
    
    	if (!(validate_col('e-Signature',formobj.eSign))) return false;
    

	    <%-- if(isNaN(formobj.eSign.value) == true) {
	    	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	    	formobj.eSign.focus();
	    	return false;
	    } --%>	    

		return true;
	}else{
		return false;
	}
}

function resetFormat(formobj)
{
 formobj.format1.value="";
 formobj.format2.value="";
 formobj.format3.value="#####";
}

</script>

<BODY>
<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>
<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:include page="include.jsp" flush="true"/>
<%@ page language = "java" import = "com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.business.common.*,java.util.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.user.UserJB"%>
<%@ page import="com.velos.eres.service.util.*"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:include page="popupPanel.jsp" flush="true"/>
<Link Rel=STYLESHEET HREF='./styles/velos_popup.css' type=text/css>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<%
 HttpSession tSession = request.getSession(true);
 if (sessionmaint.isValidSession(tSession))
 {
%>
	<jsp:include page="sessionlogging.jsp" flush="true"/>

<%
       //Added by Manimaran to insert the Audit details in DB.
       String ipAdd = (String) tSession.getValue("ipAdd");
       String usr = null;
       usr = (String) tSession.getValue("userId");

	StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
	int pageRight;
    if ((stdRights.getFtrRights().size()) == 0){
	 	pageRight= 0;
    }else{
		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
 	 }

   	String accountId = (String) tSession.getValue("accountId");
	String pageMode = request.getParameter("pageMode");

	String studyLkpDict = "0";
	int stat = 0;

	if (EJBUtil.isEmpty(pageMode))
	{
		pageMode = "initial";
	}

	//String ipAdd = (String) tSession.getValue("ipAdd");

	//if(!(oldESign.equals(eSign)))
	//{
	//}
	//else {



	String studyid = request.getParameter("studyid");
	int study = EJBUtil.stringToNum(studyid);
	studyB.setId(study);
	studyB.getStudyDetails();


	if (pageMode.equals("final"))
	{
		String eSign = request.getParameter("eSign");
		String oldESign = (String) tSession.getValue("eSign");
		String selLookupDict = request.getParameter("advlkpDict");

		String studyIdGen=request.getParameter("studyidgen");
		studyIdGen=(studyIdGen==null)?"":studyIdGen;
		String genPk=request.getParameter("genpk");
		genPk=(genPk==null)?"":genPk;

		String studCEnroll=request.getParameter("studcenroll");
		studCEnroll=(studCEnroll==null)?"":studCEnroll;
		String studCEnrollpk=request.getParameter("studcenrollpk");
		studCEnrollpk=(studCEnrollpk==null)?"0":studCEnrollpk;

		String studAccrualFlag=request.getParameter("studaccrualflag");
		studAccrualFlag=(studAccrualFlag==null)?"":studAccrualFlag;
		String studAccrualFlagPk=request.getParameter("studaccrualflagpk");
		studAccrualFlagPk=(studAccrualFlagPk==null)?"0":studAccrualFlagPk;

		String format1=request.getParameter("format1");
		format1=(format1==null)?"":format1;

		String format2=request.getParameter("format2");
		format2=(format2==null)?"":format2;

		String format3=request.getParameter("format3");
		format3=(format3==null)?"":format3;

		String format=format1+"-"+format2+"-"+format3;

		String formatPk=request.getParameter("formatpk");
		formatPk=(formatPk==null)?"0":formatPk;

		String forwardValue=request.getParameter("forwardPullDown");
		forwardValue=(forwardValue==null)?"":forwardValue;

		String forwardPk=request.getParameter("forwardpk");
		forwardPk=(forwardPk==null)?"0":forwardPk;

		if(!oldESign.equals(eSign)) {

		%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>

		<%
			} else {
				SettingsDao settingsDao=commonB.getSettingsInstance();
				//set the keyword and the value and module information to the settingDao Object
				settingsDao.setSettingKeyword("STUDY_CENTRIC_ENROLL");
				settingsDao.setSettingKeyword("STUDY_ACCRUAL_FLAG");
				settingsDao.setSettingKeyword("PATSTUDY_ID_GEN");

				settingsDao.setSettingValue(studCEnroll);
				settingsDao.setSettingValue(studAccrualFlag);
				settingsDao.setSettingValue(studyIdGen);

				settingsDao.setSettingsModNum(studyid);
				settingsDao.setSettingsModNum(studyid);
				settingsDao.setSettingsModNum(studyid);

				settingsDao.setSettingsModName("3");
				settingsDao.setSettingsModName("3");
				settingsDao.setSettingsModName("3");

				//Added by Manimaran for Audit columns in the table
				settingsDao.setSettingCreator(usr);
				settingsDao.setSettingCreator(usr);
				settingsDao.setSettingCreator(usr);

				settingsDao.setSettingIpadd(ipAdd);
				settingsDao.setSettingIpadd(ipAdd);
				settingsDao.setSettingIpadd(ipAdd);

				settingsDao.setSettingModifiedBy(usr);
				settingsDao.setSettingModifiedBy(usr);
				settingsDao.setSettingModifiedBy(usr);


				if (studCEnrollpk.length()>0)
				 settingsDao.setSettingPK(studCEnrollpk);
				 else
				 settingsDao.setSettingPK("0");

				if (studAccrualFlagPk.length()>0)
					 settingsDao.setSettingPK(studAccrualFlagPk);
					 else
					 settingsDao.setSettingPK("0");

				if (genPk.length()>0)
				 settingsDao.setSettingPK(genPk);
				 else
				 settingsDao.setSettingPK("0");


				 //next
				 settingsDao.setSettingKeyword("PATSTUDY_ID_FORMAT");
				 settingsDao.setSettingValue(format);
				 settingsDao.setSettingsModNum(studyid);
				 settingsDao.setSettingsModName("3");

				//KM
                 settingsDao.setSettingCreator(usr);
				 settingsDao.setSettingIpadd(ipAdd);
				 settingsDao.setSettingModifiedBy(usr);

				 if (formatPk.length()>0)
				 settingsDao.setSettingPK(formatPk);
				 else
				 settingsDao.setSettingPK("0");

				 //next
				 settingsDao.setSettingKeyword("STUDY_ENROLL_FORWARD");
				 settingsDao.setSettingValue(forwardValue);
				 settingsDao.setSettingsModNum(studyid);
				 settingsDao.setSettingsModName("3");

				 //KM
                 settingsDao.setSettingCreator(usr);
				 settingsDao.setSettingIpadd(ipAdd);
				 settingsDao.setSettingModifiedBy(usr);

				 if (formatPk.length()>0)
				 settingsDao.setSettingPK(forwardPk);
				 else
				 settingsDao.setSettingPK("0");

				 settingsDao.saveSettings();


				 studyB.setStudyAdvlkpVer(selLookupDict);
				 studyB.setModifiedBy(usr);

				stat = studyB.updateStudy();

				if (stat >= 0)
				{
			%>
					<br><br><br><br><br>
					<p class = "sectionHeadings" align = center><%=MC.M_Data_SvdSucc%><%--Data was saved successfully.*****--%></p>
					<script>

					    if (typeof(window.opener!="undefined"))
						window.opener.location.reload();
					    /*Changes By Sudhir on 02-Apr-2012 for Bug 9097*/
						//self.close();						
						setTimeout("self.close()",1000);
						/*Changes By Sudhir on 02-Apr-2012 for Bug 9097*/
					</script>

			<%
				} else
				{
			%>
					<br><br><br><br><br>
					<p class = "sectionHeadings" align = center><%=MC.M_DataCnt_SvdSucc%><%--Data could not be saved successfully.*****--%></p>
					<script>
						self.close();
					</script>

			<%
				} // end of if data saved
		}	// end of if for esign
	}	// end of if pagemode is final

	if (pageMode.equals("initial"))
	{
		studyLkpDict = studyB.getStudyAdvlkpVer();
		if (EJBUtil.isEmpty(studyLkpDict))
			studyLkpDict = "0";
		LookupDao lookupDao = new LookupDao();
		lookupDao = studyB.getAllViewsForLkpType(EJBUtil.stringToNum(accountId) ,"NCI");
		ArrayList viewIds = lookupDao.getLViewIds();
		ArrayList viewNames = lookupDao.getLViewNames();
		Integer viewid ;
		String viewname = "",forwardPage="";
		String checkedString = "";

		//Retrieve patient study forms
		 LinkedFormsDao lnkFrmDao = new LinkedFormsDao();
		 String accId=(String)tSession.getAttribute("accountId");
		 int iaccId=EJBUtil.stringToNum(accId);
		 int personPK=0;
		 UserJB user = (UserJB) tSession.getValue("currentUser");
	       	String siteId = user.getUserSiteId();
		 String userId = (String) tSession.getValue("userId");
		 lnkFrmDao = lnkformB.getPatientStudyForms(iaccId,personPK,study, EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId));
		ArrayList arrFrmIds = lnkFrmDao.getFormId();
		ArrayList arrFrmNames = lnkFrmDao.getFormName();

		//Add the static page entry for


		arrFrmIds.add(0,"[VELCHAR]PATSTAT");
		arrFrmNames.add(0,LC.L_Patient_Status);/*arrFrmNames.add(0,LC.Pat_Patient+" Status");*****/
		
		if(("Y".equalsIgnoreCase(CFG.LAB_MODE))){
		arrFrmIds.add(0,"[VELCHAR]LAB");
		arrFrmNames.add(0,LC.L_Labs);/*arrFrmNames.add(0,"Labs");*****/
		}
		
		if(("Y".equalsIgnoreCase(CFG.ADV_EVENT))){
		arrFrmIds.add(0,"[VELCHAR]ADV");
		arrFrmNames.add(0,LC.L_Adverse_Events);/*arrFrmNames.add(0,"Adverse Events");*****/
		}

		arrFrmIds.add(0,"[VELCHAR]DEMO");
		arrFrmNames.add(0,LC.L_Demographics);/*arrFrmNames.add(0,"Demographics");*****/

		//end retrieving

		//Retrieve the settings from settings table
	SettingsDao settingsDao=commonB.retrieveSettings(study,3);
	ArrayList settingsValue=settingsDao.getSettingValue();
	ArrayList settingsKeyword=settingsDao.getSettingKeyword();
	ArrayList settingsPk=settingsDao.getSettingPK();
	int index;
	String studyCEnrollPk="",studyCEnrollValue="",genIdPk="",genIdValue="",forwardPk="",studyAccrualFlagPk="",studyAccrualFlagValue="";

	String genFormatPk="",genFormatValue="",forwardValue="";
	String[] format=new String[3];
	Arrays.fill(format,"");
	index=settingsKeyword.indexOf("STUDY_CENTRIC_ENROLL");
	if (index>=0)
	{
	 studyCEnrollPk=(String)settingsPk.get(index);
	 studyCEnrollValue=(String)settingsValue.get(index);
	}

	index=settingsKeyword.indexOf("STUDY_ACCRUAL_FLAG");
	if (index>=0)
	{
	 studyAccrualFlagPk=(String)settingsPk.get(index);
	 studyAccrualFlagValue=(String)settingsValue.get(index);
	 studyAccrualFlagValue=(studyAccrualFlagValue==null)?"":studyAccrualFlagValue;
	}

	index=settingsKeyword.indexOf("PATSTUDY_ID_GEN");
	if (index>=0)
	{
	 genIdPk=(String)settingsPk.get(index);
	 genIdValue=(String)settingsValue.get(index);
	}
	index=settingsKeyword.indexOf("PATSTUDY_ID_FORMAT");
	if (index>=0)
	{
	 genFormatPk=(String)settingsPk.get(index);
	 genFormatValue=(String)settingsValue.get(index);
	 if (genFormatValue.length()>2)
	 //format=StringUtil.strSplit(genFormatValue,"-",false);
	 format=StringUtil.chopChop(genFormatValue, '-');
	}
	index=settingsKeyword.indexOf("STUDY_ENROLL_FORWARD");
	if (index>=0)
	{
	 forwardPk=(String)settingsPk.get(index);
	 forwardValue=(String)settingsValue.get(index);
	 forwardValue=(forwardValue==null)?"":forwardValue;
	}

	String dformPullDown = EJBUtil.createPullDownWithStr("forwardPullDown",forwardValue, arrFrmIds , arrFrmNames);

		//end settinghs retrieval

	%>

<div class="popDefault">
	<form name="dictSetting" id="dictFrm" action="studyDictSettings.jsp" method="post" onSubmit="if (validate(document.dictSetting)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}")>
   	<INPUT type=hidden name= studyid value = <%=studyid%> >
   	<INPUT type=hidden name= pageMode value = "final">

	<%if(CFG.ADV_EVENT.equalsIgnoreCase("Y")){ %>
	<P class="sectionHeadings"><%=MC.M_Assoc_AdvEvtDict%><%--Associate Adverse Event Dictionary*****--%></P>
	<table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl" >
	<tr><th><%=LC.L_Adv_EvtDict%><%--Adverse Event Dictionary*****--%></th> <th><%=LC.L_Use%><%--Use*****--%></th></tr>

	<%
		if (studyLkpDict.equals("0"))
			checkedString = "CHECKED";
		else
			checkedString = "";
	%>

	<tr  class="browserEvenRow"><td ><%=LC.L_Free_TextEntry%><%--Free Text Entry*****--%></td>
		<td><input type="radio" name="advlkpDict" value = "0" <%=checkedString%> > </td>
	</tr>

	<%
		int counter = 0;
		int len = viewIds.size();

		for (counter = 0; counter <= len -1 ; counter++)
		{
			viewid = (Integer) viewIds.get(counter);
			viewname = (String) viewNames.get(counter);

			if (EJBUtil.isEmpty(viewname))
				viewname = "-";

			if ( viewid.intValue() == EJBUtil.stringToNum(studyLkpDict))
				checkedString = "CHECKED";
			else
				checkedString = "";

			if ((counter%2)!=0) {
			  %>
    		  <tr class="browserEvenRow">
        	<%
			}
			else{
			  %>
		      <tr class="browserOddRow">
    		  <%
				}
			%>
			<td class="tdDefault">
			 	<%=viewname%>
			</td>

			<td>
				<input type="radio" name="advlkpDict" <%= checkedString%> value ="<%=viewid%>" />
			</td>

	 	</tr>

	<%
		}

	%>
	</table>
	<%} %>
	</br>
		<P class="sectionHeadings"><%=MC.M_PatStdId_Genr%><%--<%=LC.Pat_Patient%> <%=LC.Std_Study%> ID Generation*****--%></P>
		<table  width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl"  >
		<tr><th><%=LC.L_Type%><%--Type*****--%></th> <th><%=LC.L_Use%><%--Use*****--%></th></tr>
		<tr class="browserEvenRow">
		 <td width="85%"><%=LC.L_Allow_ManualEntry%><%--Allow Manual Entry*****--%></td>
		 <td>
		 <%if ((genIdValue.equals("manual") || (genIdValue.length()==0))){%>
		 <input type="radio" name="studyidgen" value="manual" checked onClick="resetFormat(document.dictSetting);">
		 <%}else {%>
		 <input type="radio" name="studyidgen" value="manual" onClick="resetFormat(document.dictSetting);">
		 <%}%>
		  </td>
		</tr>
		<tr class="browserOddRow">
		 <td width="85%"><%=LC.L_Sysgen_Seq%><%--System-Generated sequential*****--%></td>
		  <td>
		  <%if (genIdValue.equals("auto")){%>
		  <input type="radio" name="studyidgen" value="auto" checked>
		  <%}else{%>
		  <input type="radio" name="studyidgen" value="auto">
		  <%}%>
		  </td>
		</tr>
		<input type="hidden" name="genpk" value="<%=genIdPk%>">
		</table><table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl" >
		<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  <select size="1" name="format1">
	  <option value="" selected><%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>
	  <%if (format[0].equals("STUDY")){%>
	  <option value="STUDY" selected><%=LC.L_Study%><%--<%=LC.Std_Study%>--%></option>
	  <%}else{%>
	  <option value="STUDY"><%=LC.L_Study%><%--<%=LC.Std_Study%>--%></option>
	  <%}%>
	  <%if (format[0].equals("SITEID")){%>
	  <option value="SITEID" selected><%=LC.L_Site_Id%><%--Site ID*****--%></option>
	  <%}else{%>
	  <option value="SITEID"><%=LC.L_Site_Id%><%--Site ID*****--%></option>
	  <%}%></select>-</td><td><select size="1" name="format2">
	  <option value="" selected><%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>
	  <%if (format[1].equals("STUDY")){%>
	  <option value="STUDY" selected><%=LC.L_Study%><%--<%=LC.Std_Study%>--%></option>
	  <%}else{%>
	  <option value="STUDY"><%=LC.L_Study%><%--<%=LC.Std_Study%>--%></option>
	  <%}%>
	  <%if (format[1].equals("SITEID")){%>
	  <option value="SITEID" selected><%=LC.L_Site_Id%><%--Site ID*****--%></option>
	  <%}else{%>
	  <option value="SITEID"><%=LC.L_Site_Id%><%--Site ID*****--%></option>
	  <%}%>
	  </select>-</td>
	  <td>
	  <%
	  if (genFormatPk.length()>0){%>
	  <input type="text" name="format3" value="<%=format[2]%>" size="20" maxlength="20">
	   <%}else{%>
	   <input type="text" name="format3" value="#####" size="20" maxlength="20">
	   <%}%>
	  </td>
	  <input type="hidden" name="formatpk" value="<%=genFormatPk%>">
		</tr>
		</table>
		<table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl" >
		<tr><td colspan=5>
		<Font color="red"><%=MC.M_NoteSysGen_PrefixNumeric%><%--Note:The complete format for system-generated <%=LC.Pat_Patient%> <%=LC.Std_Study%> ID should not exceed 20 characters. <BR>A predefined prefix format can be included in the numeric format e.g. xyz|###*****--%></Font>
</tr>
</table>
</br>
		<p class="sectionHeadings"><%=LC.L_Std_EnrollmentProcess%><%--<%=LC.Std_Study%> Enrollment Process*****--%></p>
		<table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl" >
		<tr class="browserOddRow">
		 <td width="50%"><%=MC.M_EnableStd_CentricSch%><%--Enable <%=LC.Std_Study%>-centric enrollment*****--%></td>
		 <td>
		 <%if (studyCEnrollValue.equals("Y")){%>
		 <input type="radio" name="studcenroll" value="Y" checked><%=LC.L_Yes%><%--Yes*****--%>
		 <%}else{%>
		 <input type="radio" name="studcenroll" value="Y"><%=LC.L_Yes%><%--Yes*****--%>
		 <%}if (studyCEnrollValue.equals("N") || (studyCEnrollValue.length()==0) ){%>
		  <input type="radio" name="studcenroll" value="N" checked><%=LC.L_No%><%--No*****--%>
		  <%}else{%>
		  <input type="radio" name="studcenroll" value="N"><%=LC.L_No%><%--No*****--%>
		  <%}%>
		  </td>
		  <input type="hidden" name="studcenrollpk" value="<%=studyCEnrollPk%>">
		  </tr>
		  <script>
		  var helpText="<html><body>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><u><%=MC.M_DfltEquvOrg_AllowStopTeam%><%--Default Settings</u></b><br/><br/><b>Active/ Enrolling equivalent study status:</b> Entered for any Organization ALLOWS patient accrual by ANY one of the Organizations in the Study Team.<br/><br/><b>Closed to Accrual equivalent study status:</b> Does not have any effect on patient accrual for any Organization in the Study Team<br/><br/><b>Permanent Closure equivalent study status:</b> Entered for any Organization STOPS patient accrual by ANY one of the Organizations in the Study Team.<br/><br/><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><u>Organization Specific </u></b> <br/><br/><b>Active/ Enrolling equivalent study status:</b> Entered for any Organization ALLOWS patient accrual by that SPECIFIC Organization in the Study Team.<br/><br/><b>Closed to Accrual equivalent study status:</b> Entered for an Organization STOPS patient accrual for that SPECIFIC Organization in the Study Team.<br/><br/><b>Permanent Closure equivalent study status:</b> Entered for any Organization STOPS patient accrual by ANY one of the Organizations in the Study Team.*****--%></p></body></html>";
		</script>

		  <tr class="browserEvenRow">
		 <td width="50%"><%=MC.M_FlagTo_AllowPatAccrual%><%--Flag to Allow <%=LC.Pat_Patient%> Accrual*****--%><img border="0" src="../images/jpg/help.jpg" onmouseover='javascript:return overlib(helpText,CAPTION,"<%=LC.L_Help%><%--Help*****--%>",RIGHT,ABOVE);' onmouseout="return nd();" ></img></td>

		 <td>
		 <%if ((studyAccrualFlagValue.equals("D")) || (studyAccrualFlagValue.length()==0) ){%>
		 <input type="radio" name="studaccrualflag" value="D" checked><%=LC.L_Default%><%--Default*****--%>
		 <%}else{%>
		 <input type="radio" name="studaccrualflag" value="D"><%=LC.L_Default%><%--Default*****--%>
		 <%}if (studyAccrualFlagValue.equals("O") ){%>
		  <input type="radio" name="studaccrualflag" value="O" checked><%=LC.L_Org_Specific%><%--Organization Specific*****--%>
		  <%}else{%>
		  <input type="radio" name="studaccrualflag" value="O"><%=LC.L_Org_Specific%><%--Organization Specific*****--%>
		  <%}%>
		  </td>
		  <input type="hidden" name="studaccrualflagpk" value="<%=studyAccrualFlagPk%>">
		  </tr>
		  <tr>
		  <td width="60%"><P class = "defComments"></P></td>
		  </tr>
		  <tr>
		 <td width="50%"><%=MC.M_OnStdCntrEnrl_UsrTakenTo%><%--On submission of <%=LC.Std_Study_Lower%>-centric enrollment page, user is taken to*****--%></td>
		 <td>
		 <%=dformPullDown%>
		  </td>
		  <input type="hidden" name="forwardpk" value="<%=forwardPk%>">
		  </tr>
			<input type="hidden" name="pgRight" value="<%=pageRight%>">
		  <%
		  //JM: 08Oct2010, #5290
		  if (pageRight > 5){

		  %>
		  <tr>
				<td colspan=2>
					<jsp:include page="submitBar.jsp" flush="true">
						<jsp:param name="displayESign" value="Y"/>
						<jsp:param name="formID" value="dictFrm"/>
						<jsp:param name="showDiscard" value="N"/>
						<jsp:param name="noBR" value="Y"/>
					</jsp:include>
				</td>
		  </tr>
		  <%}%>
		</table>

		<br>
		<% if (pageRight >= 6){%>





   <%}%>
</form>
</div>
<%
	} // end of if for pagemode
//}        //end of if for eSign
} //end of if for session
else{%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%
}%>

<div><jsp:include page="bottompanel.jsp" flush="true" /></div>
</DIV>
</BODY>
</HTML>
