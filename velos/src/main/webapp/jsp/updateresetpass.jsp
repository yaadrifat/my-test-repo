<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@page import="com.velos.eres.service.util.DateUtil"%>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="addressB" scope="page" class="com.velos.eres.web.address.AddressJB" />
<jsp:useBean id ="auditRowEresJB" scope="session" class="com.velos.eres.audit.web.AuditRowEresJB"/>
<jsp:useBean id ="audittrails" scope="session" class="com.aithent.audittrail.reports.AuditUtils"/>
 
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*"%>
<%
 HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))
   {   

 String fromPage = request.getParameter("fromPage");
 boolean incorrectesign=false;
 if (fromPage.equals("User")) {
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<% 
 String eSign = request.getParameter("eSign");
 String oldESign = (String) tSession.getValue("eSign");
 if(!oldESign.equals(eSign)) {
 	incorrectesign=true;
 }else {
	incorrectesign=false;
 }
 }

 if (incorrectesign==true) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
    
	String supportEmail = Configuration.SUPPORTEMAIL;
	   
 String ipAdd = (String) tSession.getValue("ipAdd");
// String user = null;
 String userPass="";
 String usereSign="";
 String messageSubtext="";
 String addressId="";
 

 String messageFrom = "";
 String messageText = "";
 String messageSubject = "";
 String messageHeader = "";
 String messageFooter = "";		
 String completeMessage = "";

 String messageTo = "";
 int rows = 0;
 String	usrEmail="";
 String userLoginName ="";
 String siteUrl = "";

//Added By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.
 String usr=(String) tSession.getAttribute("userId");
 String userName=(String) tSession.getAttribute("userName");

 int ret =0;
 double randomNumber=Math.random();
 String randomNum=String.valueOf(randomNumber);
 String userESignExpiryDate="";
 String userPwdExpiryDate="";
 String password=request.getParameter("password");
 String esign=request.getParameter("esignBox");
 String userId=request.getParameter("userId"); 


 int user = EJBUtil.stringToNum(userId);

 userB.setUserId(user);
 userB.getUserDetails();
 addressId=userB.getUserPerAddressId();
 addressB.setAddId(EJBUtil.stringToNum(addressId));
 addressB.getAddressDetails();
 usrEmail=addressB.getAddEmail();
 userLoginName =userB.getUserLoginName();

 String userMailStatus = "";
 int metaTime = 1;
 
 int rid = 0; //Added By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.

// Added by Rajeev K - 02/18/04
// Begin
// get site URL
	rows = 0;
	CtrlDao urlCtrl = new CtrlDao();	
	urlCtrl.getControlValues("site_url");
	rows = urlCtrl.getCRows();
   	if (rows > 0)
	   {
	   	siteUrl = (String) urlCtrl.getCValue().get(0);
	   }
// End


if((password!=null)&&(esign!=null)){
 //VCTMS-1 :April-15th-2011
	userPass=randomNum.substring(2,10);
	usereSign=randomNum.substring(11,15);
	//messageSubtext=" \nNew Password    : " + userPass +  "\nNew e-Signature : " + usereSign + "\nApplication URL : " + siteUrl;
	userB.setUserPwd(Security.encryptSHA(userPass));
	userB.setUserESign(usereSign);
	userB.setUserPwdExpiryDate(DateUtil.dateToString(DateUtil.addDays((new Date()),-1)));
	userB.setUserESignExpiryDate(DateUtil.dateToString(DateUtil.addDays((new Date()),-1)));
	userB.notifyUserResetInfo(userPass,siteUrl,usereSign,userId,userLoginName,"R",usr); //Modified By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.
	}
	else
	if(password!=null){
	userPass=randomNum.substring(2,10);
	userB.setUserPwd(Security.encryptSHA(userPass));
	userB.setUserPwdExpiryDate(DateUtil.dateToString(DateUtil.addDays((new Date()),-1)));
	//messageSubtext="\nNew Password    : " + userPass + "\nApplication URL : " + siteUrl; 
	userB.notifyUserResetInfo(userPass,siteUrl,"0000",userId,userLoginName,"RP",usr); //Modified By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.
	}
	else{
	usereSign=randomNum.substring(11,15);
	userB.setUserESign(usereSign);
	userB.setUserESignExpiryDate(DateUtil.dateToString(DateUtil.addDays((new Date()),-1)));
	//messageSubtext="\nNew e-Signature :   " + usereSign + "\nApplication URL : " + siteUrl; 
	userB.notifyUserResetInfo("NoPass",siteUrl,usereSign,userId,userLoginName,"RE",usr); 
	}
	userB.setIpAdd(ipAdd);
	//Added for Bug#15318 : Raviesh
	userB.setUsrLoggedIn(usr);
	//Added By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.
	userB.setModifiedBy(usr);
	
	rid = auditRowEresJB.getRidForDeleteOperation(user, EJBUtil.stringToNum(usr), "ER_USER", "PK_USER");
	ret = userB.updateUser();	
	//Added By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.
	if(ret>=0){
  	  if(rid > 0){
  			audittrails.updateAuditROw("eres", EJBUtil.stringToNum(usr)+", "+userName.substring(userName.indexOf(" ")+1,userName.length())+", "+userName.substring(0,userName.indexOf(" ")), StringUtil.integerToString(rid), "U");
  		}
    }


	messageTo = usrEmail;

/*

	rows = 0;
	CtrlDao userCtrl = new CtrlDao();	
	userCtrl.getControlValues("eresuser");
	rows = userCtrl.getCRows();
	if (rows > 0)
	   {
	   	messageFrom = (String) userCtrl.getCDesc().get(0);
	   }
	
	messageSubject = "Request for change of password/esign";	
	messageHeader ="\nDear Velos eResearch Member," ; 

	 messageText = "\n\nAs per your request your Password/e-Signature has been reset. Your new Password/e-Signature is:\n\nLogin ID        : " + userLoginName + messageSubtext + "\n\nYou may of course change your Password/e-Signature at any time should you have any concerns regarding the privacy of your information. To change your Password/e-Signature, you will need to login with the above ID , go to the Personalize Account section and then change your Password/e-Signature.\n\nTo begin using the Velos eResearch system, login to your account.\n **** Important ****\nPlease remember that the above information needs to be kept in a secure place. Any one with access to the above information can access your account information.\n\nVelos eResearch" ;

	messageFooter = "\n\nThis email transmission and any documents, files or previous email messages attached to it may contain information that is confidential or legally privileged. If you are not the intended recipient or a person responsible for delivering this transmission to the intended recipient you are hereby notified that you must not read this transmission and that any disclosure, copying, printing, distribution or use of this transmission is strictly prohibited. If you have received this transmission in error, please immediately notify the sender by telephone or return email and delete the original transmission and its attachments without reading or saving in any manner." ;
	completeMessage = messageHeader  + messageText + messageFooter ;


	try{
		VMailer vm = new VMailer();
    	VMailMessage msgObj = new VMailMessage();
    				
		msgObj.setMessageFrom(messageFrom);
		msgObj.setMessageFromDescription("Velos eResearch Customer Services");
		msgObj.setMessageTo(messageTo);
		msgObj.setMessageSubject(messageSubject);
		msgObj.setMessageSentDate(new Date());
		msgObj.setMessageText(completeMessage);

		vm.setVMailMessage(msgObj);
		userMailStatus = vm.sendMail(); 						
	  }
	  catch(Exception ex)
      {
     	 userMailStatus = ex.toString();
      }
	  */
%>
<br><br><br><br><br>

<%
  if ( EJBUtil.isEmpty(userMailStatus) )
  {
  	metaTime = 1;
	
  %>
	<p class = "successfulmsg" align = center> <%=MC.M_DataSvdSucc_NotficSent%><%--Data was saved successfully and Notification sent.*****--%> </p>

 <%
	 } else
	{
	 	metaTime = 10;
     %>
		<p class = "redMessage" align = center> <%=MC.M_DataSuccErr_CustSup%><%--Data was saved successfully but there was an error in sending notification to the user.<br>
		Please contact Customer Support with the following message:<BR><%=userMailStatus%>*****--%></p>

	 <%
  		}
	%>	 	

<br>
<table align="center" >
<tr>
<td>
<button onClick="window.self.close();"><%=LC.L_Close%></button>
</td>
</tr>
</table>
  
  <%
      }//e-sign 
	}//end of if body for session
	else
	{
	%>
	<jsp:include page="timeout_adminchild.jsp" flush="true"/>
	<%
	}
%>
	<div class = "myHomebottomPanel">
		<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>  
</BODY>

</HTML>
