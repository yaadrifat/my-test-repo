<!DOCTYPE html>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html style="height:auto">

<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<title><%=LC.L_Date_Filter%><%--Date Filter*****--%></title>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>

<SCRIPT Language="javascript">

function back(parm,sec,formobj) {
	var from = formobj.from.value;
	var container;
	if(from == "patientreports"){
		if (document.layers) {
			container = window.opener.document.div1.document.reports;
		}else{
			container = window.opener.document.reports;
		}
	}else{
		if(from == "db"){
			if (document.layers) {
				container = window.opener.document.div1.document.dashboardpg;
			}else{
				container = window.opener.document.dashboardpg;
			}
		}else{
			if (document.layers) {
				container = window.opener.document.div1.document.reports;
			}else{
				container = window.opener.document.reports;
			}
		}
	}
	container.range.value ="";
	container.year.value = "";
	container.year1.value = "";
	container.month.value = "";
	container.dateFrom.value = "";
	container.dateTo.value = "";
	if (parm == "Y") {
		container.range.value ="<%=LC.L_Year%><%--Year*****--%>"+": "  +formobj.year[formobj.year.selectedIndex].value;
		container.year.value = formobj.year[formobj.year.selectedIndex].value;
	}
	if (parm == "M") {
		container.year1.value =formobj.year1[formobj.year1.selectedIndex].value;
		container.month.value = formobj.month[formobj.month.selectedIndex].value;
		container.range.value ="<%=LC.L_Month%><%--Month*****--%>"+": "  +formobj.month[formobj.month.selectedIndex].value +"<%=LC.L_Year%><%--Year*****--%>"+": " +formobj.year1[formobj.year1.selectedIndex].value;
	}
	if (parm == "D"){
 		container.dateFrom.value = formobj.dateFrom.value;
		container.dateTo.value = formobj.dateTo.value;
		container.range.value ="<%=LC.L_From%><%--From*****--%>"+": "  +formobj.dateFrom.value +" "+"<%=LC.L_To%><%--To*****--%>"+": " +formobj.dateTo.value; 
	}
	self.close();
}

</SCRIPT>

<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>

<jsp:include page="skinChoser.jsp" flush="true"/>
<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>

</head>



<body >
<%-- INF-20084 Datepicker-- AGodara --%>
<jsp:include page="jqueryUtils.jsp" flush="false"/>
<Form  name="date" method="post">
<%HttpSession tSession = request.getSession(true);
  if (sessionmaint.isValidSession(tSession))
{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String parm=request.getParameter("parm") ;
	String section=request.getParameter("section");
	String from=request.getParameter("from");	
	Calendar cal = Calendar.getInstance();
	int currYear = cal.get(cal.YEAR);
	
	from = (from==null)?"-":from;
%>
<input type='hidden' name='from' value='<%=from%>'>
<P class="defComments custom-not-defComments custom-defComments custom-not-bg"> <%=MC.M_Specify_DtFilter%><%--Please specify the Date Filter*****--%></P>

  <% if (parm.equals("Y")) {%> 
  <table  width="100%" cellspacing="0" cellpadding="0" border=0 >
    <tr> 
      <td class=tdDefault width=80%> 
	  <P class="defComments custom-not-defComments custom-defComments custom-not-bg"><%=LC.L_Select_Year%><%--Select Year*****--%>: <select name="year"> 
    	<% int i = currYear - 50;
	    int count = 0;
    	for (count= i; count<= currYear + 10 ;count ++){
    		if (count == currYear) {%>
    			<option value=<%=count%> SELECTED><%=count%></option>
	    	<%}else{ %>
    			<option value=<%=count%>><%=count%></option>
    		<%}%>
	    <%}%>
		</select>
   	</P>
   </td>
   <%}%>
   <% if (parm.equals("M")) {%> 
  <table  width="100%" cellspacing="0" cellpadding="0" border=0 >
    <tr> 
      <td class=tdDefault width=80%> 
	    <P><%=LC.L_Sel_MthOrYear%><%--Select Month/Year*****--%>: 
		<select name="month"> 
        <% int cnt = 0;
        for (cnt= 1; cnt<= 12;cnt ++){%>
        	<option value=<%=cnt%>><%=cnt%></option>
        <%}%>
        </select>
        
        <select name="year1"> 
        <% int j = currYear - 50;
        for (cnt= j; cnt<= currYear + 10;cnt ++){
        	if (cnt == currYear) {%>
        		<option value=<%=cnt%> SELECTED><%=cnt%></option>
        	<%}else{ %>
        		<option value=<%=cnt%>><%=cnt%></option>
        
        	<%}%>
        <%}%>
        </select>
     </P>
	</select>
   </P>
   </td>
   <%}%>
   <% if (parm.equals("D")) {%>
 <table  width="100%" cellspacing="0" cellpadding="0" border=0 >
    <tr> 
      	<td class="tdDefault" width="15%"> 
      	   <%=LC.L_Date_From%><%--Date From*****--%>: 
		</td>
       	<td>
			<%-- INF-20084 Datepicker-- AGodara --%>
       	   <Input type=text name="dateFrom" class="datefield" READONLY> 
		</td>
       	<td class="tdDefault" width="15%">
       	   <%=LC.L_Date_To%><%--Date To*****--%>:
       	</td>
     	<td>
       	   <Input type=text name="dateTo" class="datefield" READONLY> 
       	</td>
    </tr>
    <tr>
       	<td></td>
       	<td></td>
     	<td></td>
	</tr>
	<tr>
   <%}%>
 
     <td class=tdDefault width=20%> 
     <!--   <Input type="submit" name="submit" value="Go"> 
	 <input type="image" src="./images/select.gif" align="bottom" border="0" onClick="back('<%//=parm%>','<%//=section%>')">-->
	
	 	<button type="submit" onclick="return back('<%=parm%>','<%=section%>',document.date)" ><%=LC.L_Select%></button> 


      </td>
    </tr>
  </table>
</Form>
<%}//end of if body for session
else{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}

%>
</div>
<div class ="mainMenu" id = "emenu"> </div>
</body>
</html>
