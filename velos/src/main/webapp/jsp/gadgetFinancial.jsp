<%@page import="com.velos.eres.service.util.VelosResourceBundle"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@page import="java.util.ArrayList,java.util.HashMap,org.json.*"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
HttpSession tSession = request.getSession(false); 
if (!sessionmaint.isValidSession(tSession)) {
	return;
}
String userId = (String)tSession.getAttribute("userId");
int numStudies = 0;

%>
<div>
	<table width="100%">
	<tr>
		<td>
		<div id="gadgetFinancial_settings" style="border:#aaaaaa 1px solid; padding: 2px; display:none">
		    <form id="gadgetFinancial_formSettings" name="gadgetFinancial_formSettings" onsubmit="return false;" >
		    <p><b><%=LC.L_Settings_FinGadget%></b></p>
		    <input id="gadgetFinancial_studyIds" name="gadgetFinancial_studyIds" type="hidden"></input>
		    <table style="width:100%; " >
		        <tr>
		            <td>
		            	<label for="gadgetFinancial_studies"><%=MC.M_YouCan_Add5Studies%></label><BR/>
		                <div id="gadgetFinancial_errMsg_studyIds"></div>
		            </td>
		            <td>
		                <div id="gadgetFinancial_studies"></div>
	                	<s:if test="%{gadgetFinancialJB.settingsStudyCount < 5}">
			                <input id="gadgetFinancial_study" name="gadgetFinancial_study" type="text" size="30" ></input>
			                <input id="gadgetFinancial_studyId" name="gadgetFinancial_studyId" type="hidden" size="30" ></input>
			                <button id="gadgetFinancial_addStudyButton" class="gadgetFinancial_addStudyButton" type="button"><%=LC.L_Add%></button>
		                </s:if>
		            </td>
		        </tr>
		        <tr>
                    <td colspan="2">
                    	<table style="width:100%">
                    	<tr>
                    	<td style="width:70%;">
                    	<table class="gdt-table" style="width:100%;">
                    		<tr>
                    			<td>
	                    		<div id="gadgetFinancial_myStudiesHTML"></div>
	                    		</td>
	                    	</tr>
	                    </table>
	                    </td>
	                    <td style="width:30%;">
	                    <div id="gadgetFinancial_reallyDeleteHTML"></div>
	                    </td>
	                    </tr>
	                    </table>
	                    <s:hidden id="gadgetFinancialJB.settingsStudyIds" name="gadgetFinancialJB.settingsStudyIds"/>
                    </td>
                </tr>
		        <tr>
		            <td colspan="2" align="right">
		            <!-- 
		            <button type="submit"
		                    id="gadgetFinancial_saveButton"
		                    class="gadgetFinancial_saveButton">Save</button>&nbsp;&nbsp;&nbsp;&nbsp;
		            -->
		            <button type="button" 
		                    id="gadgetFinancial_cancelButton"
		                    class="gadgetFinancial_cancelButton"><%=LC.L_Close%></button>
		            </td>
		        </tr>
		    </table>
		    </form>
		</div>
		</td>
	</tr>
	<tr>
		<td>
		<div id="gadgetFinancial_mainTabs">
		    <ul>
		        <li><a href="#gadgetFinancial_tab1"><%=LC.L_Billing%></a></li>
		        <li><a href="#gadgetFinancial_tab2"><%=LC.L_Collection%></a></li>
		        <li><a href="#gadgetFinancial_tab3"><%=LC.L_Payments%></a></li>
		    </ul>
		    <div id="gadgetFinancial_tab1">
		         <table style="width:100%">
			        <tr>
			            <td>
			            <div class="velos-yui-dt-container ui-gadget">
			           		<div id="gadgetFinancial_myBillingGrid"></div>
			           	</div>
			            </td>
			        </tr>
			    </table>
		    </div>
		    <div id="gadgetFinancial_tab2">
		        <table style="width:100%">
			        <tr>
			            <td>
			            <div class="velos-yui-dt-container ui-gadget">
			           		<div id="gadgetFinancial_myCollectionGrid"></div>
			           	</div>
			            </td>
			        </tr>
			    </table>
		    </div>
		    <div id="gadgetFinancial_tab3">
		       	<table style="width:100%">
			        <tr>
			            <td>
			            <div class="velos-yui-dt-container ui-gadget">
			           		<div id="gadgetFinancial_myPaymentsGrid"></div>
			           	</div>
			            </td>
			        </tr>
			    </table>
		    </div>
		</div>
		</td>
	</tr>
	</table>
</div>

