<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_FldEdt_BoxSubmit%><%--Field -Edit Box Submit*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %><%@page import="com.velos.eres.service.util.*"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="formSecJB" scope="request"  class="com.velos.eres.web.formSec.FormSecJB"/>
<jsp:useBean id="formFieldJB" scope="request"  class="com.velos.eres.web.formField.FormFieldJB"/>


<body><%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB, com.velos.eres.business.fieldLib.impl.FieldLibBean"%>
<DIV class="popDefault" id="div1"> 

<%
	HttpSession tSession = request.getSession(true);
	String codeStatus = request.getParameter("codeStatus");
	if (sessionmaint.isValidSession(tSession))
	{
		
%>
	<jsp:include page="sessionlogging.jsp" flush="true"/>
   
<%		String eSign= request.getParameter("eSign");
		String oldESign = (String) tSession.getValue("eSign");
	  	 if((codeStatus.equals("WIP")) || ((!codeStatus.equals("WIP") && !codeStatus.equals("")) && (oldESign.equals(eSign)))){
			int errorCode=0;
			String mode=request.getParameter("mode");
			String fieldLibId="";
			
			String fldInstructions="";
			String fldInstFormat="";
			String fldType="C";
		
			String accountId="";
			String fldLibFlag="F";
			String fldBrowserFlg="0" ;
			String fldName="";
			String fldIsVisible ="";
			
			// THE STYLE OBJECT WHICH TAKES VALUES OF ATTRIBUTES OF THE FIELD  
			// LIKE BOLD, SAMELINE, ALIGN , ITALICS
			Style aStyle = new Style();
			String formId="";
			String formFldId="";
			String formSecId="";
			String fldMandatory="";
			String fldSeq="";
			String fldAlign="";
			String samLinePrevFld="";
			String fldBold="";
			String fldItalics="";
		
			String creator="";
			String ipAdd="";
			String labelDisplayWidth= "";
		
			creator=(String) tSession.getAttribute("userId");
			ipAdd=(String)tSession.getAttribute("ipAdd");
	     	accountId=(String)tSession.getAttribute("accountId");
			
			mode=request.getParameter("mode");
			String recordType=mode;  
			
	
			formId=request.getParameter("formId");
			fldInstructions=request.getParameter("instructions");
			
			fldInstFormat=StringUtil.decodeString(request.getParameter("fldinst"));
			fldInstFormat=(fldInstFormat==null)?"":fldInstFormat;
			
			if (fldInstructions.length()  >= 89)
			{
				fldName=fldInstructions.substring(0,88) ;
				fldName=fldName.concat("...");
			}
			else
			{
				fldName=fldInstructions ;
			}		
			fldType="C"; //edit box type to be entered by us 
			formSecId=request.getParameter("section");
			fldSeq=request.getParameter("sequence");
			fldMandatory=request.getParameter("mandatory");
			fldAlign=request.getParameter("align");
			samLinePrevFld=request.getParameter("sameLine");
			fldBold=request.getParameter("bold");
			fldItalics=request.getParameter("italics");
			fldIsVisible = request.getParameter("isvisible");
			
			labelDisplayWidth = request.getParameter("labelDisplayWidth");
			
			//  We SET THE STYLE WE CHECK ITS VALUES TO BEFORE SENDING TO THE DB
			aStyle.setAlign(fldAlign);

			if (fldBold==null)
			aStyle.setBold("0");
			else if (fldBold.equals("1"))
			aStyle.setBold("1");

			if (fldItalics==null)  
			aStyle.setItalics("0");
			else if (fldItalics.equals("1"))
			aStyle.setItalics("1");
			
			
			if (samLinePrevFld==null)
			aStyle.setSameLine("0");
			else if (samLinePrevFld.equals("1"))
			aStyle.setSameLine("1");
	
			FieldLibBean fieldLsk = new FieldLibBean();
			
			fieldLsk.setFormId(formId );
			
			
			fieldLsk.setFldInstructions(fldInstructions);
			fieldLsk.setFldNameFormat(fldInstFormat);
			fieldLsk.setFldType(fldType);
			fieldLsk.setIpAdd(ipAdd);
			fieldLsk.setRecordType(recordType);
			fieldLsk.setAccountId(accountId);
			fieldLsk.setLibFlag(fldLibFlag);
			fieldLsk.setFldName(fldName);
		
			fieldLsk.setFormFldBrowserFlg(fldBrowserFlg);
			
			fieldLsk.setFormSecId(formSecId );
			fieldLsk.setFormFldSeq( fldSeq );
			fieldLsk.setFormFldMandatory( fldMandatory );
			fieldLsk.setFldIsVisible(fldIsVisible);
			
		
			fieldLsk.setAStyle(aStyle);
			fieldLsk.setFldDisplayWidth(labelDisplayWidth);
			

			
			if (mode.equals("N"))
			{
				
				fieldLsk.setCreator(creator);
				errorCode=fieldLibJB.insertToFormField(fieldLsk);
				
			}

			else if (mode.equals("M"))
			{
			
		
				
				formFldId=request.getParameter("formFldId");
				formFieldJB.setFormFieldId(EJBUtil.stringToNum(formFldId));
				formFieldJB.getFormFieldDetails();
				
				fieldLibId=formFieldJB.getFieldId();
				
				
				
				
			
				fieldLibJB.setFieldLibId(EJBUtil.stringToNum(fieldLibId) );
				fieldLibJB.getFieldLibDetails();
				fieldLsk  = fieldLibJB.createFieldLibStateKeeper();
				 	

				
				aStyle.setSameLine(samLinePrevFld);		
				fieldLsk.setFormId(formId );
				fieldLsk.setFormFldId(formFldId );
				fieldLsk.setFldInstructions(fldInstructions);
				fieldLsk.setFldNameFormat(fldInstFormat);
				fieldLsk.setFldType(fldType);
				fieldLsk.setIpAdd(ipAdd);
				fieldLsk.setRecordType(recordType);
				fieldLsk.setAccountId(accountId);
				fieldLsk.setLibFlag(fldLibFlag);
				fieldLsk.setFldName(fldName);
			
				fieldLsk.setModifiedBy(creator);
				fieldLsk.setFormSecId(formSecId );
				fieldLsk.setFormFldSeq( fldSeq );
				fieldLsk.setFormFldMandatory( fldMandatory );
				fieldLsk.setFldIsVisible(fldIsVisible);
				
				fieldLsk.setAStyle(aStyle);
				fieldLsk.setFldDisplayWidth(labelDisplayWidth);
				
				errorCode=fieldLibJB.updateToFormField(fieldLsk);
				
						
		 }
		
		
		if ( errorCode == -2 || errorCode == -3 )
		{
%>
  	
<%             if (errorCode == -3 )
				{
%>
				
				<br><br><br><br><br>
				<p class = "successfulmsg" > <%=MC.M_FldIdExst_EtrDiff%><%--The Field ID already exists. Please enter a different Field ID*****--%></p>
				<button tabindex = 2 onClick="history.go(-1);"><%=LC.L_Back%></button>
				
				
<%
				} else 
%>				
	 			<p class = "successfulmsg" ><%=MC.M_Data_NotSvdSucc%><%--Data not saved successfullyl*****--%> </p>

<%
		
		} //end of if for Error Code

		else

		{
%>	
		
<%
		
%>		

		<br><br><br><br><br>
		<p class = "successfulmsg" align = center><%=MC.M_Data_SavedSucc%><%--Data Saved Successfully*****--%> </p>
		<script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
		</script>		
			
<%
		}//end of else for Error Code Msg Display
	}//end of if old esign
	else{ 
%>
	<jsp:include page="incorrectesign.jsp" flush="true"/>
<%	  
		}//end of else of incorrect of esign */
     }//end of if body for session 	
	
	else
	{

%>
		  <jsp:include page="timeout.html" flush="true"/>
<%
	
} //end of else of the Session 
 

%>

</body>

</html>
