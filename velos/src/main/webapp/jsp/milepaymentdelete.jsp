<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Mstone_PaymentDel%><%-- Milestone Payment Delete*****--%></title>
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>
<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	/*if(isNaN(formobj.eSign.value) == true) {
		alert*/<%--("<%=MC.M_IncorrEsign_EtrAgain%>");--%>
		/*alert("Incorrect e-Signature. Please enter again");
	formobj.eSign.focus();
	return false;
   }*****/
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*,com.aithent.audittrail.reports.AuditUtils"%>
<jsp:useBean id="milepaymentB" scope="request" class="com.velos.eres.web.milepayment.MilepaymentJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 
<jsp:useBean id ="auditRowEresJB" scope="session" class="com.velos.eres.audit.web.AuditRowEresJB"/>
<jsp:useBean id ="audittrails" scope="session" class="com.aithent.audittrail.reports.AuditUtils"/>

<% String src;
	src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<BODY> 
<br>

<DIV class="formDefault" id="div1">
<% 
String milestoneId= "";
String selectedTab = request.getParameter("selectedTab");
		
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{

	String milepaymentId= request.getParameter("milepaymentId");
	String studyId = request.getParameter("studyId");
	String usrId = (String) tSession.getAttribute("userId");
	String userName = (String) tSession.getAttribute("userName");

	int ret=0;
	int rid=0;
	
	String delMode=request.getParameter("delMode");
	
	if (delMode == null) {
		delMode="final";
%>
	<FORM name="milepaymentdelete" id="milepaydelfrm" method="post" action="milepaymentdelete.jsp" onSubmit="if (validate(document.milepaymentdelete)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
<br><br>
	
	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%-- Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	
		</table>
			
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="milepaydelfrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

	<input type="hidden" name="delMode" value="<%=delMode%>">
	<input type="hidden" name="srcmenu" value="<%=src%>">
	<input type="hidden" name="milepaymentId" value="<%=milepaymentId%>">
	<input type="hidden" name="studyId" value="<%=studyId%>">  
	<input type="hidden" name="selectedTab" value="<%=selectedTab%>">
	

	</FORM>
<%
	} else {
  

			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>

 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {

//			ret = lineitemB.lineitemDelete(EJBUtil.stringToNum(lineitemId));
			milepaymentB.setId(EJBUtil.stringToNum(milepaymentId));
			milepaymentB.getMilepaymentDetails();
			milepaymentB.setMilepaymentDelFlag("Y");
			// Modified for INF-18183 ::: AGodara
			rid = auditRowEresJB.getRidForDeleteOperation(EJBUtil.stringToNum(milepaymentId), EJBUtil.stringToNum(usrId), "ER_MILEPAYMENT", "PK_MILEPAYMENT");
			ret = milepaymentB.updateMilepayment(AuditUtils.createArgs(tSession,"",LC.L_Milestones));		
		
			if(rid > 0){
    			audittrails.updateAuditROw("eres", EJBUtil.stringToNum(usrId)+", "+userName.substring(userName.indexOf(" ")+1,userName.length())+", "+userName.substring(0,userName.indexOf(" ")), StringUtil.integerToString(rid), "U");
    		}
  
			if (ret==-2) {%>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_MstonePayRec_NotDel%><%-- Milestone payment record not deleted.*****--%> </p>			
			<%}else { %>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Mstone_PayRecDel%><%-- Milestone payment record deleted.*****--%> </p>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=milepaymentbrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">				
		<%

			}
			
	
			} //end esign
			
			

	} //end of delMode
	
  }//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %>
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/> 
</div>

</body>
</HTML>


