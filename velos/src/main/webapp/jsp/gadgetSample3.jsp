<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList,java.util.HashMap,org.json.*"%>
<%@page import="com.velos.eres.widget.business.common.UIWidgetDao" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%!
static final String TYPE = "type";
%>
<%
HttpSession tSession = request.getSession(false); 
if (!sessionmaint.isValidSession(tSession)) {
	return;
}
%>
<div>
    <table style="width:100%">
        <tr>
            <td colspan="3">
            <form name="gadgetSample3_form" id="gadgetSample3_form" >
                <button type="button" name="gadgetSample3_configButton"
                        id="gadgetSample3_configButton" 
                		class="gadgetSample3_configButton">Switch Screen</button>
            </form>
            </td>
        </tr>
        <tr>
            <td colspan="3">
<div id="gadgetSample3_mainTabs">
    <ul style="display:none">
        <li><a href="#tabs-1"></a></li>
        <li><a href="#tabs-2"></a></li>
        <li><a href="#tabs-3"></a></li>
    </ul>
    <div id="tabs-1">
        <p>Screen 0. Some text here.</p>
    </div>
    <div id="tabs-2">
        <p>Screen 1. Some text here.</p>
    </div>
    <div id="tabs-3">
        <p>Screen 2. Some text here.</p>
        <p>More text here.</p>
        <p style="text-align:right">
                <button type="button" name="gadgetSample3_saveConfigButton" 
                        id="gadgetSample3_saveConfigButton" 
                		class="gadgetSample3_saveConfigButton">Save</button>
        </p>
    </div>
</div>
            </td>
        </tr>
    </table>
</div>
