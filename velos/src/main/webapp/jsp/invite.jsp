<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.Configuration"%>

<%@ page import="com.velos.eres.service.util.*"%>


<SCRIPT Language="Javascript">

 function  validate(formobj){
//  formobj=document.invite;
  if(formobj.messagefrom == "V"){

		formobj.message.value = formobj.defaultMsg.value;

	} else {

		formobj.message.value = formobj.defaultMsg.value;
	}


if (!(validate_col('e-Signature',formobj.eSign))) return false
if (!(validate_col('E-Mail',formobj.toEmail))) return false
<%-- 	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   } --%>
}
function setfocus()
{
document.invite.defaultMsg.focus()
}

function scrollToTop (element) {
  if (document.all)
    element.defaultMsg.scrollTop = 0;
}


   function msg(calledFrom,formobj){


   		if (calledFrom=="V"){

	 	formobj.defaultMsg.value=formobj.messageid.value;
	 	formobj.defaultMsg.focus();

	} else {


		formobj.defaultMsg.value="";
		formobj.defaultMsg.focus();

	}
   }



</SCRIPT>
	</head>


<% String src = null;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>



<br>
<body>
<%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
	{
	String supportEmail = Configuration.SUPPORTEMAIL;
	String uName = (String) tSession.getValue("userName");
	int pageRight = 1;

 if (pageRight > 0 )
	{


	   String userId = (String) tSession.getValue("userId");


%>

<%
	String mailText1 = MC.M_DearFrnd_RecvVelosJoin+" " +"\n\n" + uName + MC.M_VelosEresInst_StdInetHosted ;
	/*String mailText1 = "Dear Friend,\n\nYou are receiving this message from Velos eResearch's  'Invite a Friend to Join' service. " +
     "\n\n" + uName + " has invited you to join the Velos eResearch community. Simply go to" +
     " www.veloseresearch.com and click on the Sign Up link. Complete the registration form and Velos will set up an account for you." +
	 "\n\nVelos eResearch is ushering in a powerful new paradigm for managing clinical studies on the web. Velos eResearch is:"+
	"\n	1.	Productive quickly---no downloads or installation required" +
	"\n 2.	Useful for most any kind of study"+
	"\n 3.	Flexible, intuitive and friendly --- study coordinators can be proficient using Velos eResearch in minutes not hours"+
	"\n 4.	FREE!!! --- exclusively for investigators and your study team"+
	"\n\nMost clinical research software suppliers focus on the needs of study sponsors. Investigators and study administrators are largely unsupported. What sponsors and investigators need is a system that supports the investigator.  Enter Velos eResearch."+
	"\n\nVelos eResearch is a suite of Internet services (hosted software in internet parlance) that gives investigators and study coordinators the ability to:"+
	"\n-	Create and manage study protocols;" +
	"\n-	Enroll study subjects;" +
	"\n-	Automatically generate and update "+LC.Pat_Patient_Lower+" schedules;" +
	"\n-	Track study progress and tasks;" +
	"\n-	Send alerts and reminders to "+LC.Pat_Patients_Lower+" and study team members;" +
	"\n-	Capture billing information; and" +
	"\n-	Generate related reports relatively effortlessly." +
 	"\n\nBeing web-based, Velos eResearch allows the investigators and sponsors to connect with anyone anywhere, and share data for efficient, team-oriented study execution.  Velos eResearch employs state-of-the-art security, encryption, and firewall technology that enables you to keep "+LC.Pat_Patient_Lower+"-identifiable information confidential." +
	"\n\nVelos eResearch Console, which includes all the above capabilities, is FREE for you and your study team.  In the future, Velos intends to provide additional services that plug into your Console, and enhanced versions of services included in the Console, for a reasonable subscription fee.  We also reserve the right to charge trial sponsors for studies hosted on Velos eResearch." ;*****/

	String mailText2  = "\n\n"+MC.M_IfHaveQue_CnctUsAt+" " + supportEmail + MC.M_WebsiteInfo_VisionSuiteCa ; 
	/*String mailText2  = "\n\nIf you have any questions, please feel free to contact us " + supportEmail +" or visit our website at www.veloseresearch.com for more information." +
	 "\n\nEasy-to-use..powerful..productive in minutes..and FREE!!!  It doesn't get any better!" +
	 "\n\nThank you for your interest in Velos eResearch." +
	 "\n\nVelos is..Vision and Value with Velocity." +
     "\n\nVelos eResearch Customer Support" +
     "\n\nVelos, Inc." +
     "\n2201 Walnut Avenue, Suite 208" +
     "\nFremont, CA. 94538" ;*****/
	 String message = mailText1 + mailText2;
%>
<DIV class="formDefault" id="div1">
  <Form name="invite" method="post" action="sendinvitation.jsp?src=<%=src%>"  onSubmit="return validate(document.invite);">

  <P class = "userName"> <%= uName %> </P>

  	  <input type="hidden" name="messageid" value="<%=message%>">
  	 <table width="100%" cellspacing="0" cellpadding="0" border=0 >
      <tr >
        <td width = "70%">
          <P class = "defComments"> <%=MC.M_InviteFrnd_SendEmailBenft%><%-- You can invite your friend(s) to join Velos
            eResearch. We will send an e-mail describing the benefits they can
            get by joining us.*****--%> </P>
        </td>
      </tr>
    </table>
    <table width="90%" cellspacing="0" cellpadding="0">
      <td width="30%"> <%--To*****--%><%=LC.L_To%>: <FONT class="Mandatory">* </FONT> </td>
      <td width="60%">
        <input type="text" name="toEmail" size = 60 MAXLENGTH = 200>
      </td>
      </tr>
    </table>
    <table width="100%" cellspacing="0" cellpadding="0" border=0 >
      <tr >
        <td width = "70%">
          <P class = "defComments"> <br>
            <%=MC.M_SendInvit_MoreSeprComma%><%-- Enter the e-mail address of your friend and click on 'Send Invitation'
            button. If you want to send invitation to more than one person at
            a time, enter the e-mail addresses separated by a ',' (comma).*****--%> </P>
        </td>
      </tr>
	  <tr height=10><td height=10></td></tr>
	  <input type="hidden" name="message" >

	  <tr><td>
	  	   <input type="radio" name="messagefrom" value="V" Checked onClick='msg("V",document.invite)' ><%=LC.L_Send_StandardMsg%><%--Send Standard Message*****--%>
			<input type="radio" name="messagefrom" value="U" onClick='msg("U",document.invite)'><%=MC.M_Send_MyOwnMsg%><%--Send my own Message*****--%>

		</td>
		  </tr>
		</table>
		<Br>
	    <table width="100%" cellspacing="0" cellpadding="0" border=0 >
	  	  <tr>
		  <td valign=top><%=LC.L_Message%><%--Message*****--%>: </td></tr>
		       <tr><td><TextArea  name="defaultMsg"  ONFOCUS='scrollToTop(document.invite)' cols="50" rows="8"><%=message%></TextArea>
		</td>
	  </tr>
    </table>

    <BR>
    <TABLE width="90%" cellspacing="0" cellpadding="0" ><tr>
	   <td width="30%">
		<%=LC.L_Esignature%><%--e-Signature*****--%> <FONT class="Mandatory">* </FONT>
	   </td>
	   <td width="60%">
		<input type="password" name="eSign" maxlength="8" SIZE=15 autocomplete="off"> </td>
	</tr>
    </table>


    <table width="90%" >
      <tr>
        <td class=tdDefault align="right">
          <input type="image" src="../images/jpg/Invitation.gif" name="submit"   border="0">
        </td>
      </tr>
    </table>
	<input type=hidden name=fromName value="<%= uName %>">
  </Form>
  <%
} //end of if body for page right
else
{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%
} //end of else body for page right
}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div></body>
</html>

