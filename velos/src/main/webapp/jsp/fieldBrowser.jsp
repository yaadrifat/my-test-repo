<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<title><%=MC.M_Add_FldToBrowser%><%--Add Fields to Browser*****--%></title>
<head>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script language="javascript" src="validations.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
	function validate(formobj,fieldsInBrowser)
	{

	document.fieldbrowser.flag.value="2";
	
	selFieldNames = new Array();
	selFieldIds = new Array();
	notSelFieldIds = new Array();
	notSelFieldNames = new Array();
	var pos=0;
	var totalRows=0;
	var notselNumbers=0;
	totalRows=formobj.rows.value;
	checked = false;
	var k=0;
		 
   if (totalRows==1) {			
		if (formobj.chkBrowserFlg.checked) {
			selValue = formobj.chkBrowserFlg.value;
			pos = selValue.indexOf("*");
			selFieldIds[0] = selValue.substring(0,pos);
			selFieldNames[0] = selValue.substring(pos+1);
			notSelFieldNames[0]=null;
		}
		else
		{
		notSelValue=formobj.chkBrowserFlg.value;
		pos = notSelValue.indexOf("*");
		notSelFieldIds[0] = notSelValue.substring(0,pos);
		notSelFieldNames[0] = notSelValue.substring(pos+1);
		selFieldNames[0]=null;

		}
	
	} else {
		j=0;
		
		for (i=0;i<totalRows;i++) {
			
			if (formobj.chkBrowserFlg[i].checked) {
			
				pos = formobj.chkBrowserFlg[i].value.indexOf("*");
				selFieldIds[j] = formobj.chkBrowserFlg[i].value.substring(0,pos);
				selFieldNames[j] = formobj.chkBrowserFlg[i].value.substring(pos+1);
				j++;				
			} 							
		}
		
		//KM-#3977
		
		/*if (j > fieldsInBrowser )
	   {
		 alert ( " Please select only " + fieldsInBrowser + " fields ") ; 
		 return false ;
		
	  }	*/
		k=0;				
		for (i=0;i<totalRows;i++) {
			
			if (!(formobj.chkBrowserFlg[i].checked)) {
			
				pos = formobj.chkBrowserFlg[i].value.indexOf("*");
				notSelFieldIds[k] = formobj.chkBrowserFlg[i].value.substring(0,pos);
				notSelFieldNames[k] = formobj.chkBrowserFlg[i].value.substring(pos+1);
				k++;
				
			} 	
		}
	
	}
	
	

	notselNumbers= notSelFieldIds.length;
	 
	
	


 	if(formobj.codeStatus.value != "WIP"){	
 	 	if (  !(validate_col('e-Signature',formobj.eSign)     )   ) return false
 		 
 		<%-- if(isNaN(formobj.eSign.value) == true) 
 			{
 			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
 			formobj.eSign.focus();
 			 return false;
 	   		} --%>
 	 	}
	 
	formobj.selFieldIds.value=selFieldIds;
	formobj.notSelFieldIds.value=notSelFieldIds;
     return true;
	
	}
	
	
	function flag2()
	{
		document.fieldbrowser.flag.value="2";		
	}
	
	function flag0()
	{
		document.fieldbrowser.flag.value="0";
		self.close();
	}


	function forrefresh(){

		 if (document.fieldbrowser.flag.value=="0") {
		window.opener.location.reload();
  		}

    }
	</script>
	
	
	

<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"

%>

<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
String flag=request.getParameter("flag")==null?"":request.getParameter("flag");

if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) 
{
	if(flag.equalsIgnoreCase("0")){%>
		<body style="overflow:scroll" onUnload="forrefresh()" >
	<%}else{%> 
		<body style="overflow:scroll">
	<%}
}else {
	if(flag.equalsIgnoreCase("0")){%>
		<body onUnload="forrefresh()">
	<%}
	else{%>
		<body >
	<%}
}%>
<DIV class="popDefault" id="div1">

 <jsp:useBean id="fldlibB" scope="request" class="com.velos.eres.web.fieldLib.FieldLibJB"/>
 <jsp:useBean id="formFieldJB" scope="request"  class="com.velos.eres.web.formField.FormFieldJB"/>
 <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<% 
 HttpSession tSession = request.getSession(true); 
 
 
  if (sessionmaint.isValidSession(tSession))
	{%>
		<jsp:include page="sessionlogging.jsp" flush="true"/> 
		<jsp:include page="include.jsp" flush="true"/>

		<%
			int fieldsInBrowser = 0 ; 
		  Configuration conf = new Configuration();
		  fieldsInBrowser = conf.getFieldNumInFrmBrowser(conf.ERES_HOME +"eresearch.xml");
		
		
		String mode=request.getParameter("mode");
		String recordType=mode;  
		 String fldId = "";
		 String fldSecName = "";
		 String formFldId = "";
		 String fldName = "";
		 String fldUniId = "";
		 String fldType = "";
		 String fldBrowserFlag = "";

		 String fldUpId = "";
		 String formFldUpId = "";
		 String fldUpName = "";
		 String fldUpUniId = "";
		 String fldUpType = "";
		 String fldUpBrowserFlag = "";
		 String frmfldIds="";
		 String pkformsec="";
		 String codeStatus = request.getParameter("codeStatus");

		  String formfldIdT="";

		 int index=0;
		 int counter=0;
		 int k=0;
		 int pageRight = 0;
		 
		 String calledFrom=request.getParameter("calledFrom");
		 String lnkFrom=request.getParameter("lnkFrom");
		 String stdId=request.getParameter("studyId");
		 int studyId= EJBUtil.stringToNum(stdId);		 
		 String userIdFromSession = (String) tSession.getValue("userId");
	     GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	
		
		if( calledFrom.equals("A")){ //from account form management
	 	    if (lnkFrom.equals("S")) 
	 	      {   
		 		 ArrayList tId ;
				 tId = new ArrayList();
				 if(studyId >0)
				 {
				 	TeamDao teamDao = new TeamDao();
					teamDao.getTeamRights(studyId,EJBUtil.stringToNum(userIdFromSession));
					tId = teamDao.getTeamIds();

					if (tId.size() <=0)
					 {
						pageRight  = 0;
					  } else 
					  {
					  	StudyRightsJB stdRights = new StudyRightsJB();
						stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
						
							ArrayList teamRights ;
						teamRights  = new ArrayList();
						teamRights = teamDao.getTeamRights();
							 
						stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
						stdRights.loadStudyRights();
						
								
						if ((stdRights.getFtrRights().size()) == 0)
						{					
						  pageRight= 0;		
						} else 
						{								
							pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
						}
					  }
			    }
			 }	
    	 	else
    	    {		 
    	  		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));
    		 }
	  	} 
		

	 	if( calledFrom.equals("L")) //form library
	 	{
			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));			
	 	}
		
		if( calledFrom.equals("St")) //study
	 	{
			StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");	
	  		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
		}
		 
		 
		 int formId = EJBUtil.stringToNum(request.getParameter("formId"));

		 /////////for pagination 
		 String pagenum = "";
		int curPage = 0;
		long startPage = 1;
		String stPage;
		long cntr = 0;
		
		
		

		pagenum = request.getParameter("page");

		if (pagenum == null)
		{
			pagenum = "1";
		}
		curPage = EJBUtil.stringToNum(pagenum);

		String orderBy = "";
		orderBy = request.getParameter("orderBy");		
		String orderType = "";
		orderType = request.getParameter("orderType");
		String countSql = "";
		String formSql = "";
		if (orderBy == null)
		{
			orderBy = "";
		}
		if (orderType == null)
		{
			orderType = "";
		}

		String str1;
	/*	 str1 =" select pk_formfld, fld_name, pk_field," 
					+" fld_uniqueid, fld_type," 
					+" formfld_browserflg ,formsec_name, pk_formsec"
					+" from erv_formbrwflds "
					+" where fk_formlib="+formId; 		


		formSql= str1;
		
		
		countSql = "select count(*) from erv_formbrwflds where fk_formlib="+formId; */

		 str1 = "select pk_formfld,formsec_name,pk_formsec,fld_name, pk_field," 
					+" fld_uniqueid, fld_type," 
					+" formfld_browserflg "
					+" from er_formsec sec, er_formfld frmfld, er_fldlib fld" 
					+" where fk_formlib="+formId + " and nvl(sec.record_type,'Z') <> 'D' and "
					+" frmfld.fk_formsec =  sec.pk_formsec and "
					+" nvl(frmfld.record_type, 'Z') <> 'D' and "
					+" nvl(fld.record_type,'Z') <> 'D' and "
					+ "nvl(fld.fld_isvisible,0) <> 1 and "
					+" frmfld.fk_field =  fld.pk_field and "
					+" nvl(fld.fld_repeatflag,0) = 0 "
					+" and sec.formsec_repno=0 "
					+" and fld.fld_type IN ('E','M')" 
					+" and fld.fld_systemid <> 'er_def_date_01'" 
					+" and fld.fld_datatype <> 'ML' "
					+" order by formsec_seq , formfld_seq ,pk_formsec " ; 

		formSql= str1;
		
		
		countSql = "select count(*) "
				 	+" from er_formsec sec, er_formfld frmfld, er_fldlib fld" 
					+" where fk_formlib="+formId + " and nvl(sec.record_type,'Z') <> 'D' and "
					+" frmfld.fk_formsec =  sec.pk_formsec and "
					+" nvl(frmfld.record_type, 'Z') <> 'D' and "
					+" nvl(fld.record_type,'Z') <> 'D' and "
					+ "nvl(fld.fld_isvisible,0) <> 1 and "
					+" frmfld.fk_field =  fld.pk_field and "
					+" nvl(fld.fld_repeatflag,0) = 0 "
					+" and sec.formsec_repno=0 "
					+" and fld.fld_type IN ('E','M')" 
					+" and fld.fld_systemid <> 'er_def_date_01'" 
					+" and fld.fld_datatype <> 'ML' "
					+" order by  pk_formsec";
			 		
		
		long rowsPerPage=0;
		long totalPages=0;	
		long rowsReturned = 0;
		long showPages = 0;
			
	   boolean hasMore = false;
	   boolean hasPrevious = false;
	   long firstRec = 0;
	   long lastRec = 0;	   
	   long totalRows = 0;	   	   
	   
	  
	   rowsPerPage = Configuration.MOREBROWSERROWS;
   	   totalPages = Configuration.PAGEPERBROWSER;
	   
	   
	   BrowserRows br = new BrowserRows();
       br.getPageRows(curPage,rowsPerPage,formSql ,totalPages,countSql,orderBy,"null");

	   	   
   	   rowsReturned = br.getRowReturned();
	   showPages = br.getShowPages();
	   startPage = br.getStartPage();
	   hasMore = br.getHasMore();
	   hasPrevious = br.getHasPrevious();	 
	   totalRows = br.getTotalRows();	   
	   firstRec = br.getFirstRec();
	   lastRec = br.getLastRec();
	  
	   ///////////end for pagination
	   // flag=request.getParameter("flag");
	   //KM-#4593
	   //if (flag==null) flag="0";
		%>
		
<Form name="fieldbrowser" id="fldbrowserfrm" method="post" action="fieldBrowserSubmit.jsp" onsubmit="if (validate(document.fieldbrowser,<%=fieldsInBrowser%>)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<input type="hidden" name="flag" value="<%=flag%>">
	<input type="hidden" name="calledFrom" value="<%=calledFrom%>">
	<input type="hidden" name="lnkFrom" value="<%=lnkFrom%>">
	<input type="hidden" name="studyId" value="<%=studyId%>">	
	<input type="hidden" name="mode" value=<%=mode%>>			

		<%
		int count=0;
		int notME=0;
		int rows=0;
		String check="";
		

		%>
		
		<table width="100%" cellspacing="1" cellpadding="0"  border="0" class="basetbl midAlign">
					<tr> 
						<th width="15%"><%=LC.L_Sec_Name%><%--Section Name*****--%></th>
				        <th width="20%"><%=LC.L_Fld_Name%><%--Field Name*****--%></th>
				        <th width="20%"><%=MC.M_FldID_InForm%><%--Field ID(in form)*****--%></th>
						<th width="15%"><%=LC.L_Fld_Type%><%--Field Type*****--%></th>
						<th width="25%"><%=LC.L_Disp_InBrowser%><%--Display in Browser*****--%></th>
						
					</tr><%
		
		for(count=1;count<=rowsReturned;count++)
		{
				fldName = br.getBValues(count,"fld_name");
				if ( fldName == null ){
				fldName = "" ;
				}
				fldUniId = br.getBValues(count,"fld_uniqueid");
				if ( fldUniId == null ){
				fldUniId = "" ;
				}
				fldType= br.getBValues(count,"fld_type");
				if ( fldType == null ){
				fldType = "" ;
				}
				fldBrowserFlag= br.getBValues(count,"formfld_browserflg");
				if ( fldBrowserFlag == null ){
				fldBrowserFlag = "" ;
				}
				formFldId = br.getBValues(count,"pk_formfld");
				if ( formFldId == null ){
				formFldId = "" ;
				}
				fldSecName =br.getBValues(count,"formsec_name");
				pkformsec = br.getBValues(count,"pk_formsec");
				
					
			if ((count%2)==0) 
				{%>
			 <tr class="browserEvenRow"> 
				<%
				}else
				 { %>
				 <tr class="browserOddRow"> 
					<%
				}%>


			    
				<% 
								
				if(fldType.equals("M"))
				{
			
					fldType=LC.L_Multiple_Choice;/*fldType="Multiple Choice";*****/
					if( check.equals(pkformsec))
				    { %>
					<td> </td>
					<% }
					else 
					{%>
					<td><%=fldSecName%></td>
					<%}%>
					<td><%=fldName%></td>
					<td><%=fldUniId%></td>
					<td><%=fldType%></td>
					
					<%if(fldBrowserFlag.equals("1"))
					{%>
					<td> <input type="checkBox" name="chkBrowserFlg"  value="<%=formFldId%>*<%=fldUniId%>" checked></td>
				   <input type="hidden" name="fldId" value="<%=formFldId%>">
				   <input type="hidden" name="fldValue" value="<%=fldUniId%>">
					<%}else{%>
					<td><input type="checkBox" name="chkBrowserFlg"  value="<%=formFldId%>*<%=fldUniId%>"></td>
						
			
					<%}
					rows++;
				}
				else if(fldType.equals("E"))
				{
					
					
					 fldType=LC.L_Edit;/*fldType="Edit";*****/
					 if( check.equals(pkformsec))
				    { %>
					<td> </td>
					<% }
					else 
					{%>
					<td><%=fldSecName%></td>
					<%}%>
					<td><%=fldName%></td>
					<td><%=fldUniId%></td>
					<td><%=fldType%></td>
										
					 <%if(fldBrowserFlag.equals("1"))
					{%>
					<td> <input type="checkBox" name="chkBrowserFlg"  value="<%=formFldId%>*<%=fldUniId%>" checked></td>	
					<input type="hidden" name="fldId" value="<%=formFldId%>">
				    <input type="hidden" name="fldValue" value="<%=fldUniId%>">
					<%}else{%>
					<td><input type="checkBox" name="chkBrowserFlg"  value="<%=formFldId%>*<%=fldUniId%>"></td>
					
				
					<%}
					rows++;
				}%>
				
			
		</tr>
		
	
		<% check = pkformsec ; }
		%>

	
		<input type="hidden" name="formId"  value=<%=formId%>>	
		<input type="hidden" name="selFieldIds" >	
		<input type="hidden" name="notSelFieldIds" >
		<input type="hidden" name="rows" value=<%=rows%>>
	
	
	
	</table>
	
	
	
	
	
	
	<table>
		<tr><td>
		<% 
		/////////other table for page no -----------salil
		
		
		if (totalRows > 0) 
		{	Object[] arguments = {firstRec,lastRec,totalRows};
		%>
			<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
		<%} else {%>
			<font class="recNumber"><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></font>	
		<%}%>	
		</td></tr>
		</table>
	<table align=center>
	<tr>
	<%
		if (curPage==1) startPage=1;
	    for ( count = 1; count <= showPages;count++)
		{
		
		 
   			cntr = (startPage - 1) + count;
	 		if ((count == 1) && (hasPrevious))
			{  
			  %>
				<td colspan = 2>
				  <!--KM-3977-->
			  	<A href="fieldBrowser.jsp?formId=<%=formId%>&mode=<%=mode%>&page=<%=cntr-1%>&calledFrom=<%=calledFrom%>&lnkFrom=<%=lnkFrom%>&codeStatus=<%=codeStatus%>&studyId=<%=studyId%>&flag=<%=flag%>" onclick = "flag2()">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> </A>
				&nbsp;&nbsp;&nbsp;&nbsp;
				</td>	
				<%
  			}	%>
		<td>
		<%
 		 if (curPage  == cntr)
		 {	 
	    	 %>	   
				<FONT class = "pageNumber"><%= cntr %></Font>
       		<%
       	}else
        {
		//KM-#4593		 
		 %>		
		<A href="fieldBrowser.jsp?formId=<%=formId%>&mode=<%=mode%>&page=<%=cntr%>&calledFrom=<%=calledFrom%>&lnkFrom=<%=lnkFrom%>&codeStatus=<%=codeStatus%>&studyId=<%=studyId%>&flag=<%=flag%>"  onclick = "flag2()" ><%= cntr%></A>
       <%}%>
		</td>
		<%	}
		if (hasMore)
		{   %>
	   <td colspan = 3 align = center>&nbsp;&nbsp;&nbsp;&nbsp;
	   <A href="fieldBrowser.jsp?formId=<%=formId%>&mode=<%=mode%>&page=<%=cntr+1%>&calledFrom=<%=calledFrom%>&lnkFrom=<%=lnkFrom%>&codeStatus=<%=codeStatus%>&studyId=<%=studyId%>&flag=<%=flag%>" onclick = "flag2()" >< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
		</td>	
		<%	}	%>
	   </tr>
	  </table>
	
	  <%
	  
	if((pageRight == 5 && mode.equals("N")) || (pageRight == 6 && mode.equals("M")) || pageRight == 7){
		
	  if(!codeStatus.equals("Deactivated")){
		  if (rowsReturned > 1){%>
		  	<p class = "defcomments" ><FONT class="Mandatory"><%=MC.M_SbmtCurPg_DataLost%><%--Please Submit the current page before moving to any other page else data will be lost*****--%></FONT></p>
		  <%}%>

			<table width="100%" cellpadding=0 cellspacing=0>
				<tr bgcolor="#cccccc"> 
			<% if(!codeStatus.equals("Deactivated")){%>
					<td align="right" width="50%">
					<%if(codeStatus.equals("WIP")){ %>
					<jsp:include page="submitBar.jsp" flush="true"> 
							<jsp:param name="displayESign" value="N"/>
							<jsp:param name="formID" value="fldbrowserfrm"/>
							<jsp:param name="showDiscard" value="N"/>
							<jsp:param name="noBR" value="Y"/>
						</jsp:include> 
					<%}else{ %>
					<jsp:include page="submitBar.jsp" flush="true"> 
							<jsp:param name="displayESign" value="Y"/>
							<jsp:param name="formID" value="fldbrowserfrm"/>
							<jsp:param name="showDiscard" value="N"/>
							<jsp:param name="noBR" value="Y"/>
						</jsp:include>
						<%} %>
					</td>
					<td align = "left" width = "50%">
					<button type="button" onClick="window.close()"><%=LC.L_Close%></button>
					</td>
			<%}%>
				</tr>
			</table>
	  <%}%>
	<%}%>
		<input type="hidden" name="codeStatus" value=<%=codeStatus%>>
	</form>

	 <%
		
	}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</div>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>

</html>

