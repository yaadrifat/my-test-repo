<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/> 
<html>
<head>
<title><%=MC.M_Fld_SQuryHist%><%--Field(s)Query History*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<!--  <SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT> -->


<script>

function openAddWin(formQueryId, fieldName, queryType)
{
  param1="addNewQuery.jsp?queryType="+queryType+"&thread=old&fieldName="+fieldName+"&formQueryId="+formQueryId;
		windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,left=90,top=80,width=600,height=500");
		windowName.focus();

}

function openEdit(formQueryId, queryStatusId, fieldName)
{
  param1="addNewQuery.jsp?queryStatusId="+queryStatusId+"&thread=old&mode=M&fieldName="+fieldName+"&formQueryId="+formQueryId;
		windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,left=90,top=80,width=600,height=500");
		windowName.focus();

}


	
</script>


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="formQueryDao" scope="request" class="com.velos.eres.business.common.FormQueryDao"/>
<jsp:useBean id="formQueryB" scope="request" class="com.velos.eres.web.formQuery.FormQueryJB"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,com.velos.eres.service.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>

<BR>


<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body style="overflow:scroll">
<%}%>







<DIV id="div1"> 
<b><p class="defcomments"><%=LC.L_Frms_QryHist%><%--Forms >> Query History*****--%></p></b>

  <%
   HttpSession tSession = request.getSession(true); 
   if (sessionmaint.isValidSession(tSession))
   { 
	 %>
	 <jsp:include page="sessionlogging.jsp" flush="true"/>
	 <%
	   /*<!-- YK Bug#4822 19May2011  -->	*/
      int pageRight = 0;
	   String dashBoard=request.getParameter("dashBoard")==null?"":request.getParameter("dashBoard");
	   String formType=request.getParameter("formType");
	   String calledFrom=request.getParameter("calledFrom");
	   pageRight = EJBUtil.stringToNum((String)session.getAttribute("formQueryRight"));
	   if("jupiter".equals(dashBoard))
	   {
		   if(pageRight==0){
	    		  pageRight=7;
	    	  }  
	   }
      
      if("Advrse_Form".equals(formType)){/*pageRight given if pageRight==0 and calling form jupiter*/
    	  if("jupiter".equals(dashBoard)){
    		  pageRight=pageRight=EJBUtil.stringToNum(request.getParameter("pageRight"));
    	  }else{
    	  	if(pageRight==0){
    		  pageRight=7;
    	  	}
    	  }
      }else if("PatStudy_Form".equals(formType)){
    	  if("jupiter".equals(dashBoard)){
    		  pageRight=pageRight=EJBUtil.stringToNum(request.getParameter("pageRight"));
    	  }else{
    	  	if(pageRight==0){
    		  	pageRight=7;
    	  	}
    	  }
      }
	  String userId = (String) tSession.getValue("userId");
      String accountId = (String) tSession.getValue("accountId");
	 String loginUserName = 	(String) tSession.getValue("userName");	
	  int formId = EJBUtil.stringToNum(request.getParameter("formId"));	
	String filledFormId =  request.getParameter("filledFormId");
	String from = request.getParameter("from");	
	
	String showLink = request.getParameter("showLink");
	
	if (showLink == null || EJBUtil.isEmpty(showLink))
		showLink = "1";
	 
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		int account_form_rights = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));
		String studyId ="";
		
    	studyId = request.getParameter("studyId");
	 	
	 	if (StringUtil.isEmpty(studyId))
	 	{
	 		studyId="";
	 	}
     
     String formQueryId = "";
     String fieldName = "";
     int count = 0;


	
	   int iaccId=EJBUtil.stringToNum(accountId);

    /*<!-- YK Bug#4822 19May2011  -->	*/
     if (isAccessibleFor(pageRight, 'V'))
	 {
	 	 formQueryId = request.getParameter("formQueryId");
	 	 fieldName = request.getParameter("fieldName");	 
	 	
		 		 
	 	 fieldName=(fieldName==null)?"":fieldName;
		 fieldName =StringUtil.decodeString(fieldName);
		 if("Advrse_Form".equals(formType)){
			String fieldId=request.getParameter("fieldId");
			fieldName=getAdverseFieldName(EJBUtil.stringToNum(fieldId));
			/*if(EJBUtil.stringToNum(fieldId)==11012){
				fieldName="Adverse Event ID";
			}*/
		 }else if("PatStudy_Form".equals(formType)){
			 String fieldId=request.getParameter("fieldId");
			 fieldName=getPatStudyFieldName(EJBUtil.stringToNum(fieldId)); 
		 }
		 
		 %>
		<Form  name="queryHistory" method="post" action="">
	  <%
		
		int queryStatusId = 0;
		String entrdOn = "";
		int fieldId = 0; 
		String queryStat = "";
		String entrdBy = "";
		String queryType = "";
		String formQueryType = "";
		String note = "";
		String type = "";
		
		//formQueryDao = formQueryB.getQueriesForForm(EJBUtil.stringToNum(formId), EJBUtil.stringToNum(filledFormId), EJBUtil.stringToNum(from));
		
formQueryDao = formQueryB.getQueryHistoryForField(EJBUtil.stringToNum(formQueryId));
		
				
		ArrayList queryStatusIds = formQueryDao.getFormQueryStatusIds();
				
		ArrayList enteredOn = formQueryDao.getEnteredOn();
			
		ArrayList queryStatus = formQueryDao.getQueryStatus();
				
		ArrayList enteredBy = formQueryDao.getEnteredBy();
				
		ArrayList queryTypes = formQueryDao.getQueryType();
		ArrayList formQueryTypes = formQueryDao.getFormQueryType();
		ArrayList notes = formQueryDao.getQueryNotes();
		
		int rowsReturned = queryStatusIds.size();
	
	
	  %>
	    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="basetbl">
		<tr>
		 <%-- YK 14DEC-- BUG #5651--%>
	    <td width="60%" ><%=MC.M_Qry_HistForFld%><%--Query History for Field*****--%>: <B><%=fieldName%></B> (<%=LC.L_Query_Id%><%--Query ID*****--%> :<B> <%=formQueryId %>)</B></td>
	     
	     <%-- YK Changes on 23NOV2010 for EDC_AT2 Requirement --%>
	    <%if (showLink.equals("1")){ %>
		<td align="center" colspan="2"><!-- YK Bug#4822 19May2011  -->	
			<%if (isAccessibleFor(pageRight, 'E')){
			if("Advrse_Form".equals(formType)){%>
			<A href="addNewQuery.jsp?studyId=<%=studyId%>&queryType=3&thread=old&fieldName=<%=StringUtil.encodeString(fieldName)%>&formQueryId=<%=formQueryId%>&filledFormId=<%=filledFormId%>&from=<%=from%>&formId=<%=formId%>&page=H&formType=Advrse_Form&advFieldId=<%=request.getParameter("fieldId")%>&dashBoard=<%=dashBoard%>&pageRight=<%=pageRight%>"><%=LC.L_Add_Response %><%-- Add Response*****--%></A>   <%--show the LC.L_Add_Response for jupiter --%>
			<% } else if("PatStudy_Form".equals(formType)){%>
			<A href="addNewQuery.jsp?studyId=<%=studyId%>&queryType=3&thread=old&fieldName=<%=StringUtil.encodeString(fieldName)%>&formQueryId=<%=formQueryId%>&filledFormId=<%=filledFormId%>&from=<%=from%>&formId=<%=formId%>&page=H&formType=PatStudy_Form&patStudyFieldId=<%=request.getParameter("fieldId")%>&dashBoard=<%=dashBoard%>&pageRight=<%=pageRight%>"><%=LC.L_Add_Response %><%-- Add Response*****--%></A>   <%--show the LC.L_Add_Response for jupiter --%>
			<%}else{%>
			<A href="addNewQuery.jsp?studyId=<%=studyId%>&queryType=3&thread=old&fieldName=<%=StringUtil.encodeString(fieldName)%>&formQueryId=<%=formQueryId%>&filledFormId=<%=filledFormId%>&from=<%=from%>&formId=<%=formId%>&page=H"><%=LC.L_Add_Response %><%-- Add Response*****--%></A>
			<%} }%>
		</td>
		<%} %>
		<%-- <td>		
		<A href="addNewQuery.jsp?studyId=<%=studyId%>&queryType=2&thread=old&fieldName=<%=StringUtil.encodeString(fieldName)%>&formQueryId=<%=formQueryId%>&filledFormId=<%=filledFormId%>&from=<%=from%>&formId=<%=formId%>&page=H">Add Query</A>
		</td>	--%>	
		<%-- YK Changes on 23NOV2010 for EDC_AT2 Requirement --%>
		</tr>
		</table>

    <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="basetbl">
       
       <tr> 
       <%-- YK 14DEC-- BUG #5650--%>
       <th width=20%> <%=LC.L_Qry_StatID%><%--Query Status ID*****--%></th> <%-- YK 03DEC-- EDC_AT3/4 REQ--%>
       <th width=10%> <%=LC.L_Date%><%--Date*****--%></th>
       <th width=10%> <%=LC.L_Status%><%--Status*****--%></th> <%-- YK 03DEC-- EDC_AT3/4 REQ--%>
	   <th width=15%> <%=LC.L_QryOrResp%><%--Query/Response*****--%> </th>
	   <th width=5%>  <%=LC.L_Type%><%--Type*****--%></th>
	   <th width=20%> <%=LC.L_Comments%><%--Comments*****--%> </th>	   
	   <th width=20%> <%=LC.L_Entered_By%><%--Entered By*****--%></th>	  
	   </tr>
	   <% 
	    
	    for(count = 0;count < rowsReturned;count++)
					{
						
						 
						 queryStatusId = (   (Integer)queryStatusIds.get(count)  ).intValue();					 
					 	 
						 entrdOn = ((enteredOn.get(count)) == null)?"-":(enteredOn.get(count)).toString();
						 
						 queryStat = ((queryStatus.get(count)) == null)?"-":(queryStatus.get(count)).toString();
						 
						 entrdBy = ((enteredBy.get(count)) == null)?"-":(enteredBy.get(count)).toString();
						 queryType = ((queryTypes.get(count)) == null)?"-":(queryTypes.get(count)).toString();
						 formQueryType = ((formQueryTypes.get(count)) == null)?"-":(formQueryTypes.get(count)).toString();
						 if(formQueryType.equals("1") || formQueryType.equals("2"))
						  type = "Q";
						 else
						  type = "R";
						 
						 note = ((notes.get(count)) == null)?"-":(notes.get(count)).toString();						 
						 
						if ((count%2)==0) 
						{
						%>
					      	<tr class="browserEvenRow">
						<%}else
						 { %>
				      		<tr class="browserOddRow">
						<%} %>
						<td><%=queryStatusId%></td>  <%-- YK 03DEC-- EDC_AT3/4 REQ--%>
						<td>	
						<% if(formQueryType.equals("1")  && entrdBy.equals("system-generated")){%>
						<%=entrdOn%>
						<%}else{%>	
						<%-- YK 03DEC-- EDC_AT3/4 REQ--%>									
						<%=entrdOn%>
						<%--<A href="addNewQuery.jsp?studyId=<%=studyId%>&queryStatusId=<%=queryStatusId%>&thread=old&mode=M&fieldName=<%=StringUtil.encodeString(fieldName)%>&formQueryId=<%=formQueryId%>&filledFormId=<%=filledFormId%>&from=<%=from%>&formId=<%=formId%>&entrdBy=<%=entrdBy%>&page=H"><%=entrdOn%></A> --%>
						<%-- YK 03DEC-- EDC_AT3/4 REQ--%>
						<%}%>
						</td>
						<td><%=queryStat%></td> <%-- YK 03DEC-- EDC_AT3/4 REQ--%>
						<td><%=queryType%></td>
						<td><%=type%></td>
						<td><%=note%></td>
						<td>
						<%if(type.equals("R") && entrdBy.equals("system-generated")){%>
						<%="Unknown User"%>
						<%}else{
						if(entrdBy.equals("system-generated")){%>
						&lt;<%=entrdBy%>&gt;
						<%}else{%>
						<%=entrdBy%>						
						<%}
						}%>
						</td>						
			    	 	</tr>
						
				<%	}//counter
	%>
	<tr><td><br></td></tr>
	<%if (showLink.equals("1")){ %>
	<tr><td>
	<%if("Advrse_Form".equals(formType)){ %>
	<A href="addeditquery.jsp?studyId=<%=studyId%>&formId=&from=<%=from%>&filledFormId=<%=filledFormId%>&formType=Advrse_Form&dashBoard=<%=dashBoard%>&pageRight=<%=pageRight%>" type="submit"><%=LC.L_Back%></A></td><!-- vjha jupiter implement 19May2011  -->	
	<%}else if("PatStudy_Form".equals(formType)){%>
	<A href="addeditquery.jsp?studyId=<%=studyId%>&formId=&from=<%=from%>&filledFormId=<%=filledFormId%>&formType=PatStudy_Form&dashBoard=<%=dashBoard%>&pageRight=<%=pageRight%>" type="submit"><%=LC.L_Back%></A></td><!-- vjha jupiter implement 19May2011  -->	
	<%}else{ %>
	<A href="addeditquery.jsp?studyId=<%=studyId%>&formId=<%=formId%>&from=<%=from%>&filledFormId=<%=filledFormId%>" type="submit"><%=LC.L_Back%></A></td><!-- YK Bug#4822 19May2011  -->	
	<!--<button onclick="window.history.back();"><%=LC.L_Back%></button>-->
	<%} %>

	</tr>
	<%} %>
	  </table>
	  
	  
	  </FORM>
 <%
		
	} //end of if body for page right

else
{%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%
} //end of else body for page right
	}
else
{ %>
 <jsp:include page="timeout.html" flush="true"/>
<%}%>
  <div class = "myHomebottomPanel"> 
	    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>

</html>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
//Adverse Event  Field Identified 
private String getAdverseFieldName(int fieldId){
	String fieldName="";
	switch(fieldId){
	case 11012: fieldName="Adverse Event ID";
	break;
	case 11013: fieldName="Adverse Event Type";
    break;
	case 11014: fieldName="Category";
    break;
	case 11015: fieldName="Adverse Event Name";
    break;
	case 11016: fieldName="Toxicity";
    break;
	case 11017: fieldName="Toxicity Description";
    break;
	case 11018: fieldName="Severity/Grade";
    break;
	case 11019: fieldName="MedDRA code";
    break;
	case 11020: fieldName="Dictionary";
    break;
	case 11021: fieldName="Other Description";
    break;
	case 11022: fieldName="Treatment Course";
    break;
	case 11023: fieldName="Start Date";
    break;
	case 11024: fieldName="Stop Date";
    break;
	case 11025: fieldName="AE Discovery Date";
    break;
	case 11026: fieldName="AE Logged Date ";
    break;
	case 11027: fieldName="Entered By";
    break;
	case 11028: fieldName="Reported By";
    break;
	case 11029: fieldName="Attribution";
    break;
	case 11030: fieldName="Linked To ";
    break;
	case 11031: fieldName="Outcome Information";
    break;
	case 11032: fieldName="Action";
    break;
	case 11033: fieldName="Additional Information";
    break;
	case 11034: fieldName="The Following were Notified";
    break;
	case 11035: fieldName="Notes";
    break;
	case 11036: fieldName="Reason For Change (FDA Audit)";
    break;
	case 11037: fieldName="AE Status";
    break;
	case 11038: fieldName="Severity/Grade Description";
    break;
	case 11039: fieldName="Recovery Description";
    break;
	case 11040: fieldName="Outcome Notes";
    break;
	case 11041: fieldName="Adverse Event More Details";
    break;
	}
	return fieldName;
}

private String getPatStudyFieldName(int fieldId){
	String fieldName="";
	switch(fieldId){
	case 13012: fieldName="Attachment Status";
    break;
	case 13013: fieldName="Reason";
    break;
	case 13014: fieldName="Status Date";
    break;
	case 13015: fieldName="Notes";
    break;
	case 13016: fieldName="Next Follow-up Date";
    break;
	case 13017: fieldName="Patient Study ID";
    break;
	case 13018: fieldName="Enrolling Site";
    break;
	case 13019: fieldName="Assigned To";
    break;
	case 13020: fieldName="Physician";
    break;
	case 13021: fieldName="Treatment Location";
    break;
	case 13022: fieldName="Treating Organization";
    break;
	case 13023: fieldName="Disease Code";
    break;
	case 13024: fieldName="Anatomic Site";
    break;
	case 13025: fieldName="Evaluable Flag";
    break;
	case 13027: fieldName="Evaluable Status";
    break;
	case 13028: fieldName="Unevaluable Status";
    break;
	case 13029: fieldName="Status";
    break;
	case 13030: fieldName="Survival Status";
    break;
	case 13031: fieldName="Date of Death";
    break;
	case 13032: fieldName="Cause of Death";
    break;
	case 13033: fieldName="Specify Cause";
    break;
	case 13034: fieldName="Death Related to Study";
    break;
	case 13035: fieldName="Reason of Death Related to Study";
    break;
	case 13036: fieldName="Current Status";
    break;
	case 13037: fieldName="Screen Number";
    break;
	case 13038: fieldName="Screen By";
    break;
	case 13039: fieldName="Screen OutCome";
    break;
	case 13040: fieldName="Randomization Number";
    break;
	case 13041: fieldName="Enrolled By";
    break;
	case 13042: fieldName="Version No";
    break;
	
	}
	return fieldName;
}
%>