<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>

<title>New File</title>



<Link Rel=STYLESHEET HREF="common.css" type=text/css>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



</HEAD>



<% String src;

src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>



<SCRIPT Language="javascript">

 function  validate(){

     formobj=document.upload

     if (!(validate_col('File',formobj.name))) return false

     if (!(validate_col('Description',formobj.desc))) return false

 }

</SCRIPT>

<BODY>
<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>

<DIV class="formDefault" id="div1">
  <jsp:include page="studytabs.jsp" flush="true"/>
  <%

HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))

{

	String uName = (String) tSession.getValue("userName");

	String sessStudyId = (String) tSession.getValue("studyId");

	String studyNo = (String) tSession.getValue("studyNo");

	String tab = request.getParameter("selectedTab");

	int stId=EJBUtil.stringToNum(sessStudyId);



%>
  <br>
  <P class = "userName"> <%= uName %> </P>
  <% String upld;

Configuration.readAppendixParam(Configuration.ERES_HOME + "eresearch.xml");

upld=Configuration.UPLOADSERVLET;%>
  <form name=upload action= CreateWord  METHOD=POST ENCTYPE=multipart/form-data onSubmit="return validate();">
  <TextArea name = "reportStr"></textArea>
    <table width="100%" >
      <tr >
        <td width = "100%">
          <P class = "sectionHeadings"> <%=LC.Std_Study%>:<%=studyNo%> </P>
        </td>
      </tr>
      <tr>
        <td>
          <P class = "defComments"> Add documents/forms to your Protocol's Appendix.
          </P>
        </td>
      </tr>
    </table>
    <table width="100%" >
      <tr>
        <td width="35%"> File <FONT class="Mandatory">* </FONT> </td>
        <td width="65%">
          <input type=file name=name size=40>
        </td>
      </tr>
      <tr>
        <td width="35%"> </td>
        <td width="65%">
          <P class="defComments"> Specify full path of the file. </P>
        </td>
      </tr>
      <tr>
        <td width="35%"> Short Description <FONT class="Mandatory" >* </FONT>
        </td>
        <td width="65%">
          <input type=text name=desc MAXLENGTH=100 size=40>
        </td>
      </tr>
      <tr>
        <td width="35%"> </td>
        <td width="65%">
          <P class="defComments"> Give a short description of your file (100 char
            max.) </P>
        </td>
      </tr>
    </table>
    <table width="100%" >
      <tr>
        <td > <BR>
          <P class = "defComments"> <FONT class="Mandatory">* </FONT> Indicates
            mandatory fields </P>
        </td>
      </tr>
      <tr>
        <td  colspan=2> Do you want Information in this section to be available
          to the public? </td>
      </tr>
      <tr>
        <td colspan=2>
          <input type="Radio" name="pubflag" value=Y>
          Yes
          <input type="Radio" name="pubflag" value=N CHECKED>
          No &nbsp; <A href="">What is Public vs Not Public Information?</A> </td>
      </tr>
    </table>
    <input type="hidden" name="type" value='file'>
    <input type="hidden" name="study" value=<%=stId%>>
    <input type="hidden" name="studyId" value=<%=stId%>>
    <BR>
    <table width="200" cellspacing="0" cellpadding="0">
      <td align=right>
        <input type="Submit" name="submit" value="Submit">
      </td>
      </tr>
    </table>
  </form>
  <%

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<DIV class="mainmenu" id = "emenu">
  <jsp:include page="menus.htm" flush="true"/>
</DIV>
</body>

</html>
