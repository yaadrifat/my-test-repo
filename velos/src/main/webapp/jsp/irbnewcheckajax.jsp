<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="appendixB" scope="request" class="com.velos.eres.web.appendix.AppendixJB"/>
<%@ page language = "java" import = "com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@page import="com.velos.eres.web.intercept.InterceptorJB" %>
<%!
private static final String DEFAULT_TBD = "TBD";
private static final String RESULT_FLAG_KEY = "resultFlagKey";
private static final String RESULT_TEXT_KEY = "resultTextKey";
private static final String TEST_TEXT_KEY = "testTextKey";
%>
<%
    HttpSession tSession = request.getSession(true);
    if (!sessionmaint.isValidSession(tSession)) {
        return;
    }

    String accountId = (String) tSession.getValue("accountId");
    String studyId = (String) tSession.getValue("studyId");
    String usrId = (String) tSession.getValue("userId");
    String uName = (String) tSession.getValue("userName");
    String defUserGroup = (String) tSession
            .getAttribute("defUserGroup");

    int pageRight = 0;
    if (studyId == null || studyId.length() == 0) {
        return;
    }

    String boardId = (String) request.getParameter("boardId");
    if (boardId == null || boardId.length() == 0) {
        return;
    }

    String boardName = (String) request.getParameter("boardName");
    if (boardName == null) {
        boardName = "";
    }
    boardName = StringUtil.htmlEncode(boardName);

    String boardIndex = (String) request.getParameter("boardIndex");
    if (boardIndex == null) {
        boardIndex = "";
    }

    String totalBoardCount = (String) request.getParameter("totalBoardCount");
    if (totalBoardCount == null) {
        totalBoardCount = "";
    }

    String status = StringUtil.htmlEncode(request.getParameter("status"));
    if (status == null || "null".equals(status.toLowerCase())) { status = ""; }
    String statusDate = StringUtil.htmlEncode(request.getParameter("statusDate"));
    if (statusDate == null || "null".equals(statusDate.toLowerCase())) { statusDate = ""; }

    EIRBDao eIrbDao = new EIRBDao();
    eIrbDao.checkSubmission(EJBUtil.stringToNum(studyId), EJBUtil
            .stringToNum(boardId));
    ArrayList resultFlags = eIrbDao.getResultFlags();
    ArrayList resultTexts = eIrbDao.getResultTexts();
    ArrayList testTexts = eIrbDao.getTestTexts();
    
	// -- Add a hook for customization for submission validation
	ArrayList<Object> customValidationList = new ArrayList<Object>();
	try {
		if (!StringUtil.isEmpty(CFG.Config_Submission_Validation_Interceptor_Class) &&
				!DEFAULT_TBD.equals(CFG.Config_Submission_Validation_Interceptor_Class)) {
			System.out.println("\n\n Inside if in irb ajax");
			Class clazz = Class.forName(CFG.Config_Submission_Validation_Interceptor_Class);
			Object obj = clazz.newInstance();
			Object[] args = new Object[3];
			args[0] = (Object)request;
			args[1] = (Object)customValidationList;
			((InterceptorJB) obj).handle(args);
		}
	} catch(Exception e) {
		e.printStackTrace();
	}
	for (int iX = 0; iX < customValidationList.size(); iX++) {
		resultFlags.add(((HashMap<String, Object>)customValidationList.get(iX)).get(RESULT_FLAG_KEY));
		resultTexts.add(((HashMap<String, Object>)customValidationList.get(iX)).get(RESULT_TEXT_KEY));
		testTexts.add(((HashMap<String, Object>)customValidationList.get(iX)).get(TEST_TEXT_KEY));
	}
	// -- End of hook
    
    // -2=Not Required; -1=Application Incomplete; 0=Application Complete
    int isApplicationComplete = -2;
    boolean thereIsAPass = false;
    for(int iResult=0; iResult<resultFlags.size(); iResult++) {
        if ( ((Integer)resultFlags.get(iResult)).intValue() == -1 ) {
            isApplicationComplete = -1;
        } else if ( ((Integer)resultFlags.get(iResult)).intValue() == 0 ) {
            thereIsAPass = true;
        }
        System.out.println(resultTexts.get(iResult)+"::::"+isApplicationComplete+"::::"+thereIsAPass);
    }
    if (isApplicationComplete == -2 && thereIsAPass) {
        isApplicationComplete = 0;
    }
    String statusLabel = null;
    
   // isApplicationComplete=0;
    switch (isApplicationComplete) {
    case 0:  statusLabel = LC.L_App_Complete/*"Application Complete"*****/; break;
    case -1: statusLabel = LC.L_App_Incomplete/*"Application Incomplete"*****/;  break;
    case -2: statusLabel = LC.L_Not_Reqd/*"Not Required"*****/;  break;
    default: statusLabel = "";
    }
    String checkboxLabel = isApplicationComplete == 0 ?
            "<input type=checkbox id=\"submitTo"+boardIndex+"\" name=\"submitTo"+boardIndex+"\" value=\"on\""+
            " onClick=\"enableSubmitButton("+boardIndex+")\" "+("".equals(status) ? "checked" : "checked")+
            "></input>" : "<input type=checkbox disabled /> ";
    for(int iResult=0; iResult<resultFlags.size(); iResult++) {
        String requiredLabel = -2 == ((Integer)resultFlags.get(iResult)).intValue() ?
                LC.L_Not_Reqd/*"Not Required"*****/ : LC.L_Required/*"Required"*****/;
        if (testTexts.get(iResult) != null) {
            requiredLabel = "<b>"+requiredLabel+":</b> "+testTexts.get(iResult);
        }
        String resultImagePath = "../images/jpg/";
        String resultImageLabel = "";
        if (((Integer)resultFlags.get(iResult)).intValue() == -1) {
            resultImagePath += "Abnormal.jpg";
            resultImageLabel = "<br>"+(String)resultTexts.get(iResult);
        } else { resultImagePath += "Normal.jpg";  }
        if (iResult == 0) { %>

<table class="tableDefault" width="100%"  cellspacing="0" cellpadding="0" border="0">
    <%  }
        if ("LIND".equals(CFG.EIRB_MODE)) {
            if (iResult == 0) { %>
        <tr height="40" class="browserEvenRow">
            <td><b><%=boardName%></b></td>
            <td colspan=2><b><%=iResult==0 ? statusLabel:""%></b></td>
            <td align="center"><%=checkboxLabel%></td>
        </tr>
         <% }  %>    
        <tr class="browserOddRow">
            <td width="45%"><%=requiredLabel%></td>
            <td width="50%" align="center" colspan=2><img src="<%=resultImagePath%>" width="13" height="12"><%=resultImageLabel%></img></td>
            <td width="5%">&nbsp;</td>
        </tr>
    <%   } else {
            if (iResult == 0) { %>
        <tr class="browserEvenRow"><td colspan="6">&nbsp;</td></tr>
         <% }  %>
        <tr class="browserOddRow">
            <td width="20%"><b><%=iResult==0 ? boardName:""%></b></td>
            <td width="31%"><%=requiredLabel%></td>
            <td width="16%"><b><%=iResult==0 ? statusLabel:""%></b></td>
            <td width="9%"><%=iResult==0 ? "<b>"+status+"</b> "+statusDate:"&nbsp;"%> </td>
            <td width="30%" align="center"><img src="<%=resultImagePath%>" width="13" height="12"><%=resultImageLabel%></img></td>
            <td width="4%" align="center"><%=iResult==0 ? checkboxLabel:"&nbsp;"%></td>
        </tr>
    <%  }  %>
<%  } // End of for iResult %>
        <input type="hidden" name="boardIdFor<%=boardIndex%>" value="<%=boardId%>">
</table>

