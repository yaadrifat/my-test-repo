<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<title><%=LC.L_MngPat_UpldDoc%><%--Patient >> Uploaded Documents*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>

<% String src;
src= request.getParameter("srcmenu");

%>


<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>



<SCRIPT Language="javascript">

 function  validate(formobj){;
	 mode = formobj.mode.value;
	 if (mode == 'N')
	 {
	     if (!(validate_col('File',formobj.name))) return false
	 }
	 //KM-#4073	//KV:BugNo:4955
	 if (!(validate_date(formobj.perApndxDate))) return false
     if (!(validate_col('Description',formobj.desc))) return false
     if (!(validate_col('Date',formobj.perApndxDate))) return false
     if (!(validate_col('e-Sign',formobj.eSign))) return false
	 <%-- if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
		} --%>
		 if(formobj.desc.value.length>500){  
	    	 alert("<%=MC.M_ShortDesc_MaxCharsAllwd%>");/*alert(" 'Short Description' exceeded maximum number of characters allowed.");*****/
		     formobj.desc.focus();
		     return false;
	     } 
	     if (!(checkquote(formobj.desc.value))) return false 
 }


</SCRIPT>

<BODY>
<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="perApndx" scope="page" class="com.velos.eres.web.perApndx.PerApndxJB"/>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<%@ page  language = "java" import="com.velos.eres.business.common.*,java.util.*,com.aithent.file.uploadDownload.*"%>
<br>
<DIV class="formDefault" id="div1">
  <%

HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))

{
	String accId = (String) tSession.getAttribute("accountId");
	String userId = (String) tSession.getAttribute("userId");
	String uName = (String) tSession.getAttribute("userName");
    String patId = request.getParameter("patId");
	String accMaxStorage = (String) tSession.getAttribute("accMaxStorage");

	String ipAdd = (String) tSession.getAttribute("ipAdd");
	String mode = (String) request.getParameter("mode");
	String selectedTab = request.getParameter("selectedTab");
 	String studyId = null;
	String statDesc = null;
 	String statid = null;
	String studyVer = null;
	studyId = request.getParameter("studyId");
	statDesc = request.getParameter("statDesc");
	statid = request.getParameter("statid");
 	studyVer = request.getParameter("studyVer");
 	studyId = request.getParameter("studyId");
	String perApndxDate = "";
	String perApndxDesc = "";
	String perApndxId = "";
	CtrlDao ctrlDao = new CtrlDao();

	long freeSpace = ctrlDao.getFreeSpace(EJBUtil.stringToNum(accId));


  if (mode.equals("N"))
 {
	 com.aithent.file.uploadDownload.Configuration.readSettings("per");
	com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "perapndx");
	String upld=com.aithent.file.uploadDownload.Configuration.UPLOADSERVLET;
	//out.println(upld);
	%>

  <form name=upload id="perapndxfile" action=<%=upld%> METHOD=POST ENCTYPE=multipart/form-data onSubmit=" if (validate(document.upload)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
  <%
  }
  else
  {
	perApndxId = (String) request.getParameter("perApndxId");
	perApndx.setPerApndxId(com.velos.eres.service.util.EJBUtil.stringToNum(perApndxId));
	perApndx.getPerApndxDetails();
	perApndxDesc = perApndx.getPerApndxDesc();
	perApndxDate = perApndx.getPerApndxDate();

  %>
  <form name="upload" id="perapndxfile" action="perfileupdate.jsp" METHOD=POST onSubmit=" if (validate(document.upload)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
  <%
  }
   %>
    <BR><TABLE width="98%" class="basetbl">
      <tr>
        <td colspan="2" align="center">
		<%
		if (mode.equals("N"))
		  {
	   %>
          <P class = "sectionHeadingsFrm"><%=MC.M_UploadDocs_ToPatsAppx%><%--Upload documents to your Patient's Appendix.*****--%>
		  </P>
		 <%
			}
		 else
		  {
		%>
	      <P class = "defComments"> <b> <%=MC.M_Edit_DtAndDesc%><%--Edit Date and Short Description*****--%> </b>
		  </P>

		<%
		  }
		%>

        </td>
      </tr>
     <tr><td colspan="2">&nbsp;</td></tr>
      <tr>
        <td width="20%" align="right">   <%=LC.L_Date%><%--Date*****--%> <FONT class="Mandatory">* </FONT> </td>
<%-- INF-20084 Datepicker-- AGodara --%>
        <td width="65%"><input type=text name="perApndxDate" class="datefield" value='<%=perApndxDate%>' MAXLENGTH=20 size=10></td>
	  </tr>
	  <%
	  if (mode.equals("N"))
	  {
	   %>

      <tr>
        <td width="20%" align="right">   <%=LC.L_File%><%--File*****--%> 	 <FONT class="Mandatory">* </FONT> </td>
        <td width="65%">
          <input type=file name=name size=40 >
		  <input type=hidden name=db value='per'>
		  <input type=hidden name=module value='perapndx'>
		  <input type=hidden name=tableName value='PAT_PERAPNDX'>
		  <input type=hidden name=columnName value='PERAPNDX_FILEOBJ'>
		  <input type=hidden name=pkValue value='0'>
		  <input type=hidden name=pkColumnName value='PK_PERAPNDX'>

		  <input type=hidden name=nextPage value='../../velos/jsp/perapndx.jsp?srcmenu=tdMenuBarItem7&selectedTab=<%=selectedTab%>&page=patientEnroll&pkey=<%=patId%>&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>'>

        </td>
      </tr>
      <tr>
        <td width="35%"> </td>
        <td width="65%">
          <P class="defComments"> <%=MC.M_Specify_FullFilePath%><%--Specify full path of the file.*****--%> </P>
        </td>
      </tr>
	  <%
	  }
	  %>
      <tr>
        <td width="20%" align="right">   <%=LC.L_Short_Desc%><%--Short Description*****--%> <FONT class="Mandatory" >* </FONT>
        </td>
        <td width="65%">
		<%
		if (mode.equals("N"))
		  {
	   %>
		 <!--   <input type=text name=desc MAXLENGTH=250 size=40> -->
		     <TextArea type=text name=desc row=3 cols=50 ></TextArea>
		<%
			}
		 else
		  {
		%>
		   <%--  <input type=text name=desc MAXLENGTH=250 size=40 value='<%=perApndxDesc%>'> --%>
		    <TextArea type=text name=desc row=3 cols=50 ><%=perApndxDesc%></TextArea>
		<%
		  }
		%>

        </td>
      </tr>
      <tr>
        <td width="35%"> </td>
        <td width="65%">
          <P class="defComments"> <%=MC.M_ShortDescFile_500CharMax%><%--Give a short description of your file (500 char max.)*****--%> </P>
        </td>
      </tr>
    </table>

  	<input type=hidden name=accId value=<%=accId%>>
  	<input type=hidden name=userId value=<%=userId%>>
	<input type=hidden name=patId value=<%=patId%>>
	<input type=hidden name=ipAdd value=<%=ipAdd%>>
	<input type=hidden name=accountId value=<%=accId%>>
	<input type=hidden name=maxFileSize value=<%=freeSpace%>>
	<input type=hidden name=accMaxStorage value=<%=accMaxStorage%>>
    <input type="hidden" name="type" value='F'>
    <input type="hidden" name="mode" value=<%=mode%>>
	<input type="hidden" name="srcmenu" value=<%=src%>>
  	<input type="hidden" name="selectedTab" value="<%=selectedTab%>">
	<input type=hidden name="statDesc" value="<%=statDesc%>">
	<input type=hidden name="statid" value="<%=statid%>">
	<input type=hidden name="studyVer" value="<%=studyVer%>">
	<input type=hidden name="studyId" value="<%=studyId%>">

    <%
	  if (mode.equals("M"))
	  {
    %>
	   <input type="hidden" name="perApndxId" value=<%=perApndxId%>>
 	<%
	   }
	%>


	<BR>
   <jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="perapndxfile"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

  </form>
  <%

}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
 <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<DIV class="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>
</body>

</html>
