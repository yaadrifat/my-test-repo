<%@page import="com.velos.eres.web.network.NetworkJB"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%><%@page import="com.velos.eres.service.util.*"%>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="addressUserB" scope="page" class="com.velos.eres.web.address.AddressJB" />
<%
 
	 String from=(request.getParameter("from")==null)?"":request.getParameter("from");
	 String dNetTRole="";
	 String dNetADTRole="";
	 String dNetAdditonalTRole = "";     
	 String dNetUStatus="";
    if(from.equals("networkUserDropdown")){
    	String roleType=request.getParameter("roleType")==null?"":request.getParameter("roleType"); //getting roleType(primary/additional) for Network User role.
    	String nwUserPk=request.getParameter("nwUserPk")==null?"":request.getParameter("nwUserPk"); //getting Network User primary key value.
    	CodeDao cd1 = new CodeDao();
    	String rolePK = request.getParameter("rolePK")==null?"":request.getParameter("rolePK"); //getting codelst value for the membership roles of a user within the network.
    	String addnlRolePk=request.getParameter("addnlRolePk")==null?"":request.getParameter("addnlRolePk"); //getting primary key value for Network User Additional role.
    	cd1.getCodeValue("nwusersrole",nwUserPk,roleType,"0");
        //Pull dropdown for Network User's role if role-type is primary.
        if(roleType.equals("primary"))
        {
        	dNetTRole=cd1.toPullDown("ntusrroleId_"+nwUserPk+"_"+rolePK ,StringUtil.stringToInteger(rolePK),"style='display:none;' onblur='openDDList(this.id,1,7);'");
    		out.print(dNetTRole);
        }
        //Pull dropdown for Network User's role if role-type is additional.
        else
        {
        	cd1=new CodeDao();
        	cd1.getCodeValue("nwusersrole",nwUserPk,roleType,addnlRolePk);
        	dNetAdditonalTRole=cd1.toPullDown("ntusradroleId_"+addnlRolePk+"_"+rolePK,StringUtil.stringToInteger(rolePK),"style='display:none;' onblur='openDDListMul(this.id,1,7,"+nwUserPk+");'");
        	out.print(dNetAdditonalTRole);
        }
    }
    else{
    	request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		HttpSession tSession = request.getSession(false);
		if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
   	%>
		<input type="hidden" name="resultMsg" id="resultMsg" value="<%=MC.M_UsrNot_LoggedIn %>"/>    	
	<%
		//return;
	}
	String uName = (String) tSession.getValue("userName");
	int pageRight = 0;
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSITES"));
	
	int accountId=0;
	String calledFrom;
	calledFrom= request.getParameter("calledFrom");
	String src = request.getParameter("src");
	String acc = (String) tSession.getValue("accountId");
	accountId = EJBUtil.stringToNum(acc);
	String userId = (String) tSession.getValue("userId");
	int pkuser = EJBUtil.stringToNum(userId);
	userB.setUserId(pkuser);
	userB.getUserDetails();
	String defGroup = userB.getUserGrpDefault();
	groupB.setGroupId(EJBUtil.stringToNum(defGroup));
	groupB.getGroupDetails();
	String groupName = groupB.getGroupName();
	String ipAdd = (String) tSession.getValue("ipAdd");
	String flag = request.getParameter("flag");
	String searchNWUser=request.getParameter("searchNWUser");
	Integer netId=0;
	netId = EJBUtil.stringToNum(request.getParameter("netWorkId"));
	String nLevel = (request.getParameter("nLevel")==null)?"":request.getParameter("nLevel");

	String pagenum = "";
	int curPage = 1;
	long startPage = 1;
	String stPage;
	long cntr = 0;
	pagenum = request.getParameter("page");
	if (pagenum == null)
	{
		pagenum = "1";
	}
	curPage = EJBUtil.stringToNum(pagenum);
	
	String mainSql="";
	long rowsPerPage=0;
	long totalPages=0;	
	long rowsReturned = 0;
	long showPages = 0;
    boolean hasMore = false;
    boolean hasPrevious = false;
    long firstRec = 0;
    long lastRec = 0;	   
    long totalRows = 0;	   	   
    String formSql=""; 
    String countSql="";
    String sortOrderType="";
	
	rowsPerPage =  Configuration.MOREBROWSERROWS ;
	totalPages =Configuration.PAGEPERBROWSER ; 
	
    if("".equals(searchNWUser))
    	mainSql="SELECT pk_nwusers,cd1.codelst_subtyp,cd1.codelst_desc network_statdesc,a.fk_nwsites,fk_user,(usr_firstname||' '||usr_lastname) AS usr_name ,DECODE(USR_TYPE,'N','NS','AA') USR_TYPE,nwu_membertrole,nwu_memberaddtrole,(SELECT codelst_desc FROM er_codelst WHERE pk_codelst=nwu_membertrole) AS nw_trole,NWU_STATUS,sth.PK_STATUS PK_STATUS FROM er_nwusers a,er_user c,er_nwsites d,er_codelst cd1,(SELECT FK_CODELST_STAT,PK_STATUS,status_modpk,status_modtable,codelst_desc FROM ER_STATUS_HISTORY,er_codelst WHERE pk_codelst = FK_CODELST_STAT AND status_iscurrent = 1) sth  WHERE pk_user=fk_user and a.fk_nwsites=d.pk_nwsites and  usr_type not in ('X','P') and c.fk_account = "+acc+" and d.pk_nwsites="+netId+" and pk_nwusers= sth.status_modpk(+) and cd1.pk_codelst =NWU_STATUS and sth.status_modtable = 'er_nwusers' order by lower(usr_firstname),lower(usr_lastname)";
    else
    	mainSql="select * from (SELECT pk_nwusers,cd1.codelst_subtyp,cd1.codelst_desc network_statdesc,a.fk_nwsites,fk_user,(usr_firstname||' '||usr_lastname) AS usr_name ,DECODE(USR_TYPE,'N','NS','AA') USR_TYPE,nwu_membertrole,nwu_memberaddtrole,(SELECT codelst_desc FROM er_codelst WHERE pk_codelst=nwu_membertrole) AS nw_trole, usr_firstname, usr_lastname,NWU_STATUS,sth.PK_STATUS PK_STATUS FROM er_nwusers a,er_user c,er_nwsites d,er_codelst cd1,(SELECT FK_CODELST_STAT,PK_STATUS,status_modpk,status_modtable,codelst_desc FROM ER_STATUS_HISTORY,er_codelst WHERE pk_codelst = FK_CODELST_STAT AND status_iscurrent = 1) sth  WHERE pk_user=fk_user and a.fk_nwsites=d.pk_nwsites and  usr_type not in ('X','P') and c.fk_account = "+acc+" and d.pk_nwsites="+netId+" and pk_nwusers= sth.status_modpk(+) and cd1.pk_codelst =NWU_STATUS and sth.status_modtable = 'er_nwusers')a where lower(a.usr_name) like lower('%"+searchNWUser+"%') order by lower(usr_firstname),lower(usr_lastname) ";
	
    String count1 = "select count(*) from  ( " ;
	String count2 = ")"  ;
	countSql = count1 + mainSql + count2 ; 
	//sortOrderType=" lower(SITE_NAME) ASC";
 	BrowserRows br = new BrowserRows();
 	br.getPageRows(curPage,rowsPerPage,mainSql ,totalPages,countSql,"","");
	rowsReturned = br.getRowReturned();
    showPages = br.getShowPages();
    startPage = br.getStartPage();
    hasMore = br.getHasMore();
    hasPrevious = br.getHasPrevious();	 
    totalRows = br.getTotalRows();	   
    firstRec = br.getFirstRec();
    lastRec = br.getLastRec();
    
	/* NetworkDao nwdao = new NetworkDao();
	nwdao.getNetworkUsers(netId,accountId,searchNWUser);
	ArrayList networkUsrPkList = nwdao.getNetworkUsrPkList();
	ArrayList networkUsersList = nwdao.getNetworkUsersList();
	ArrayList networkMemeberTRoleList = nwdao.getNetworkMemeberTRoleList();
	ArrayList networkMemeberADTRoleList = nwdao.getNetworkMemeberADTRoleList();;
	ArrayList networkTRoleNameList = nwdao.getNetworkTRoleNameList();
	ArrayList networkADTRoleNameList = nwdao.getNetworkADTRoleNameList();
	ArrayList networkUsrNameList = nwdao.getNetworkUsrNameList();
	ArrayList networkUsrstatusList = nwdao.getNetworkUsrStatusList();
	ArrayList networkUsrTypeList = nwdao.getNetworkUsrTypeList();
	ArrayList networkUserStatusList = nwdao.getNetworkUserStatusList();
	ArrayList networkUserStatusSubTypList = nwdao.getNetworkUsrStatusSubTypList();
	ArrayList stathistoryId = nwdao.getHistoryIdsList();
	int len=0;
	len=nwdao.getcRows();*/
	String ntUsrAddlpk="";
	String ntUsrAddlroledesc="";
	String ntUsrAddlstatdesc="", ntUsrAddlstatSubTyp="";
	String ntUsrAddlstathistory="";
	String ntUsrAddlrole="";
	String ntUsrAddlstatus="";
	String ntUsrPk = "";
    String ntUsr = "";
    String ntUsrName = "";
    String ntUsrStatus = "";
    String ntTRole = "";
    String ntADTRole= "";
    String ntTRoleName = "";
    String ntTRoleNameMulti="";
    String ntADTRoleName="";
    String ntUsrType = "";
    String ntUserStatus = "", ntUserStatusSubtype="";
    String historyId = "";
    String usrDetails="",usrFirstName="",usrMidName="",usrLastName="",eMail="",usrLoginName="", fk_perId="";
    
	%>
	<table border="2"  width="100%">
<tr style="color: #D3D3D3">
<th width="25%"><%=LC.L_User_Name%></th>
<th width="45%"><%=LC.L_NetRole%></th>
<th width="20%"><%=LC.L_Documents%></th>
	<% if(groupName.equalsIgnoreCase("Admin")){%>
	<th width="10%"><%=LC.L_Delete%></th>
<%}%>
</tr>
<%
UserDao udao=new UserDao();
ArrayList usrFname= new ArrayList();
ArrayList usrMname= new ArrayList();
ArrayList usrLname= new ArrayList();
ArrayList usrPerId= new ArrayList();
ArrayList usrLog= new ArrayList();
int counter = 0;
for(counter = 1;counter<=rowsReturned;counter++)
{ 
	 ntUsrPk=((br.getBValues(counter,"pk_nwusers")) == null)?"-":(br.getBValues(counter,"pk_nwusers")).toString();
	 ntUsr=((br.getBValues(counter,"fk_user")) == null)?"-":(br.getBValues(counter,"fk_user")).toString();
	 ntUsrName=((br.getBValues(counter,"usr_name")) == null)?"-":(br.getBValues(counter,"usr_name")).toString();
	 ntUsrStatus=((br.getBValues(counter,"network_statdesc")) == null)?"-":(br.getBValues(counter,"network_statdesc")).toString();
	 ntTRole=((br.getBValues(counter,"nwu_membertrole")) == null)?"0":(br.getBValues(counter,"nwu_membertrole")).toString();
	 ntTRoleName=((br.getBValues(counter,"nw_trole")) == null)?"0":(br.getBValues(counter,"nw_trole")).toString();
	 ntUsrType = ((br.getBValues(counter,"usr_type")) == null)?"-":(br.getBValues(counter,"usr_type")).toString();
	 ntUserStatus = ((br.getBValues(counter,"NWU_STATUS")) == null)?"-":(br.getBValues(counter,"NWU_STATUS")).toString();
	 ntUserStatusSubtype = ((br.getBValues(counter,"codelst_subtyp")) == null)?"-":(br.getBValues(counter,"codelst_subtyp")).toString();
	 historyId = ((br.getBValues(counter,"PK_STATUS")) == null)?"-":(br.getBValues(counter,"PK_STATUS")).toString();
	 /* ntUsrPk=networkUsrPkList.get(counter).toString();
	 ntUsr=networkUsersList.get(counter).toString();
	 ntUsrName=((networkUsrNameList.get(counter))==null)?"":networkUsrNameList.get(counter).toString();
	 ntUsrStatus=((networkUsrstatusList.get(counter))==null)?"":networkUsrstatusList.get(counter).toString(); 
	 ntTRole=((networkMemeberTRoleList.get(counter))==null)?"":networkMemeberTRoleList.get(counter).toString();
	 ntTRoleName=((networkTRoleNameList.get(counter))==null)?"":networkTRoleNameList.get(counter).toString();
	 ntUsrType = ((networkUsrTypeList.get(counter))==null)?"":networkUsrTypeList.get(counter).toString();
	 ntUserStatus = ((networkUserStatusList.get(counter))==null)?"":networkUserStatusList.get(counter).toString();
	 ntUserStatusSubtype = ((networkUserStatusSubTypList.get(counter))==null)?"":networkUserStatusSubTypList.get(counter).toString();
	 historyId = ((stathistoryId.get(counter))==null)?"":stathistoryId.get(counter).toString(); */
     dNetAdditonalTRole= dNetAdditonalTRole.replaceAll("'", "@");
     if("0".equals(ntTRole)){
    	 ntTRoleName=LC.L_Select_AnOption;
    	 
     }
     
     udao.getUsersDetails(ntUsr);
     usrFname=udao.getUsrFirstNames();
     usrMname=udao.getUsrMidNames();
     usrLname=udao.getUsrLastNames();
     usrLog=udao.getUsrLogNames();
     usrPerId=udao.getUsrPerAdds();
     
     fk_perId=((usrPerId.get(counter-1))==null)?"":(usrPerId.get(counter-1)).toString();
     usrFirstName=((usrFname.get(counter-1))==null)?"":(usrFname.get(counter-1)).toString();
     usrMidName=((usrMname.get(counter-1))==null)?"":(usrMname.get(counter-1)).toString();
     usrLastName=((usrLname.get(counter-1))==null)?"":(usrLname.get(counter-1)).toString();
     usrLoginName=((usrLog.get(counter-1))==null)?"":(usrLog.get(counter-1)).toString();
     fk_perId=((usrPerId.get(counter-1))==null)?"":(usrPerId.get(counter-1)).toString();
     if(!fk_perId.equals("")){
		 	addressUserB.setAddId(Integer.parseInt(fk_perId));
			addressUserB.getAddressDetails();
			eMail=addressUserB.getAddEmail();
		 }
     usrDetails = usrFirstName +" "+ usrMidName+" "+usrLastName+"  "+eMail+" "+usrLoginName;
     
%>

<tr id="row_<%=ntUsrPk%>">
<td ><A href="#" onclick="fOpenNetworkUser(<%=ntUsr%>,<%=ntUsrPk%>,'<%=nLevel%>','<%=ntUsrType%>','<%=from%>');" onmouseover="return overlib(htmlEncode('<%=usrDetails.replace("'", "\\'")%>') ,ABOVE,  CAPTION,'');" onmouseout="return nd();" ><%=ntUsrName %> </A></td>
<td id ="additon_role_<%=ntUsrPk%>"><br>
<div style="float:left;width:60%"><span style="margin-left: 5px;" id="span_<%=ntUsrPk%>_<%=ntTRole%>" onclick="openDDList(this.id,0,'<%=pageRight%>');"><%=ntTRoleName %></span><%=dNetTRole %>
<span class="button-groups">
<!-- Added Edit icon for Bug 30714 - Network User Role drop-down should have edit icon. -->
<A href="#" id="span_<%=ntUsrPk%>_<%=ntTRole%>" onclick="openDDList(this.id,0,'<%=pageRight%>');"><img style="height:19px;width:19px; margin-right: 19%" src="./images/edit.gif" title="<%=LC.L_Edit%>" border="0" align="right" /></A>
	<!-- Added Save and cancel icon for Bug 30817 - Network Relationship Type and Network User Role after editing should have confirm or cancel  button. -->
	<A href="#" id="span_<%=ntUsrPk%>_<%=ntTRole%>_cncl_img" name="span_<%=ntUsrPk%>_<%=ntTRole%>_cncl_img" onclick="cancelType('span_<%=ntUsrPk%>_<%=ntTRole%>');" style="display: none;"><img src="../images/jpg/delete_pg.png" style="height:19px;width:19px; margin-right: 24%" title="<%=LC.L_Cancel%>" border="0" align="right"/></A>
	<A href="#" id="span_<%=ntUsrPk%>_<%=ntTRole%>_save_img" name="span_<%=ntUsrPk%>_<%=ntTRole%>_save_img" onclick="openDDList('ntusrroleId_<%=ntUsrPk%>_<%=ntTRole%>',1,'<%=pageRight%>');" style="display: none;"><img style="height:19px;width:19px;" src="../images/assets/correct.jpg" title="<%=LC.L_Save%>" border="0" align="right"/></A>
	<!-- End for Bug  30817 - Network Relationship Type and Network User Role after editing should have confirm or cancel  button. -->
	</span>
</div>
<div style="float:left;width:15%;margin-left:1px"><A href="#" id="span_status_<%=ntUsrPk%>_<%=ntUserStatus%>" onclick="openWinStatus(<%=pageRight%>,'M','er_nwusers','<%=ntUsrPk%>','<%=historyId%>')"><%=ntUsrStatus%></A></div>
<div style="float:left;width:18%"><A href="#" onclick="openWinStatus(<%=pageRight%>,'N','er_nwusers', '<%=ntUsrPk%>','0')"><img style="height:19px;width:19px;" src="./images/edit.gif" title="<%=LC.L_Edit%>" border="0"/></A>
<A href="showinvoicehistory.jsp?modulePk=<%=ntUsrPk%>&pageRight=<%=pageRight%>&srcmenu=<%=src%>&from=ntwusrhistory&fromjsp=usersnetworksites.jsp&currentStatId=<%=historyId%>&moduleTable=er_nwusers&netWorkId=<%=netId%>&nLevel=<%=nLevel%>"><img border="0" title="<%=LC.L_History%>" alt="<%=LC.L_History%>" style="height:19px;width:19px;" src="./images/History.gif" ><%//=LC.L_H%><%--H*****--%></A>
<img border="0" style="height:19px;width:19px;cursor:pointer;" src="./images/Add.gif" title="<%=LC.L_Add_NewRole %>" onclick="addrow(<%=ntUsrPk%>,<%=pageRight%>,'<%=historyId%>',<%=src%>,'<%=netId%>','<%=nLevel%>','<%=dNetAdditonalTRole%>')" >
</div>
<%
NetworkDao nwdao1 = new NetworkDao();
nwdao1.getNetworkAdditonalRoles(StringUtil.stringToInteger(ntUsrPk));
ArrayList networkUsersAddnlList = nwdao1.getNetworkAdlPkList();
ArrayList networkMemeberAddnlstatus = nwdao1.getNetworkAdtstatus();
ArrayList networkMemeberAddnlTRoleList = nwdao1.getNetworkAdtrole();
ArrayList networkMemeberAddnlTRoledesc = nwdao1.getNetworkADDTLRoleNameList();
ArrayList networkMemeberAddnlTRolestatdesc = nwdao1.getNetworkADDTLRoleStatus();
ArrayList networkMemeberAddnlTRolestathistory = nwdao1.getNetworkAdlPkHistory();
ArrayList setNetworkADDTLRoleStatusSubTypList = nwdao1.getNetworkADTLRoleStatusSubTyp();
int len1=0;
len1=nwdao1.getcRowsaddl();
for(int count = 0; count<len1;count++){

ntUsrAddlpk = ((networkUsersAddnlList.get(count))==null?"":networkUsersAddnlList.get(count).toString());
System.out.println(ntUsrAddlpk+ntUsrAddlrole+ntUsrAddlstatus);
ntUsrAddlrole= ((networkMemeberAddnlstatus.get(count))==null?"":networkMemeberAddnlstatus.get(count).toString());
ntUsrAddlstatus = ((networkMemeberAddnlTRoleList.get(count))==null?"":networkMemeberAddnlTRoleList.get(count).toString());
ntUsrAddlroledesc= ((networkMemeberAddnlTRoledesc.get(count))==null?"":networkMemeberAddnlTRoledesc.get(count).toString());
ntUsrAddlstatSubTyp=((setNetworkADDTLRoleStatusSubTypList.get(count))==null?"":setNetworkADDTLRoleStatusSubTypList.get(count).toString());
ntUsrAddlstatdesc = ((networkMemeberAddnlTRolestatdesc.get(count))==null?"":networkMemeberAddnlTRolestatdesc.get(count).toString());
ntUsrAddlstathistory = ((networkMemeberAddnlTRolestathistory.get(count))==null?"":networkMemeberAddnlTRolestathistory.get(count).toString());
if(ntUsrAddlpk.equals(""))
{
	ntUsrAddlpk="0";
}
if("0".equals(ntUsrAddlrole)){
	ntUsrAddlroledesc=LC.L_Select_AnOption;
	 
}
%>
<table style="width:100%">
<tr id="ntUsrAddlpk_<%=ntUsrAddlpk%>">
<td id ="additon_Role_<%=ntUsrAddlpk%>">
<div style="float:left;width:60%"><span style="margin-left:0px;" id="span_<%=ntUsrAddlpk%>_<%=ntUsrAddlrole%>" onclick="openDDListMul(this.id,0,'<%=pageRight%>','<%=ntUsrPk%>');"><%=ntUsrAddlroledesc %></span><%=dNetADTRole %>
<span class="button-groups">
<!-- Added Edit icon for Bug 30714 - Network User Role drop-down should have edit icon. -->
<A href='#' id="span_<%=ntUsrAddlpk%>_<%=ntUsrAddlrole%>" onclick="openDDListMul(this.id,0,'<%=pageRight%>','<%=ntUsrPk%>');"><img style='height:19px;width:19px; margin-right: 25%' src='./images/edit.gif' title='Edit' border='0' align='right'/></A>
<!-- Added Save and cancel icon for Bug 30817 - Network Relationship Type and Network User Role after editing should have confirm or cancel  button. -->
<A href='#' id="span_<%=ntUsrAddlpk%>_<%=ntUsrAddlrole%>_cncl_img" onclick="cancelChildType('span_<%=ntUsrAddlpk%>_<%=ntUsrAddlrole%>');" style="display: none;"><img style='height:19px;width:19px;margin-right:24%' src='../images/jpg/delete_pg.png' title='Cancel' border='0' align='right'/>
</A>	
<A href='#' id="span_<%=ntUsrAddlpk%>_<%=ntUsrAddlrole%>_save_img" onclick="openDDListMul('ntusradroleId_<%=ntUsrAddlpk%>_<%=ntUsrAddlrole%>',1,'<%=pageRight%>'); " style="display: none;"><img style='height:19px;width:19px;' src='../images/assets/correct.jpg' title='Save' border='0' align='right'/></A>
<!-- End for Bug  30817 - Network Relationship Type and Network User Role after editing should have confirm or cancel  button. -->
</span>
</div>
<div style="float:left;width:15%"><A href="#" id="span_addstatus_<%=ntUsrAddlpk%>_<%=ntUsrAddlstatus%>" onclick="openWinStatus(<%=pageRight%>,'M','er_nwusers_addnlroles','<%=ntUsrAddlpk%>','<%=ntUsrAddlstathistory%>')"><%=ntUsrAddlstatdesc%></A></div>
<div style="float:left;width:18%"><A href="#" onclick="openWinStatus(<%=pageRight%>,'N', 'er_nwusers_addnlroles','<%=ntUsrAddlpk%>','0')"><img style="height:19px;width:19px;" src="./images/edit.gif" title="<%=LC.L_Edit%>" border="0"/></A>
<!-- Committed for Bug 30822 by Deepti -->
<A href="showinvoicehistory.jsp?modulePk=<%=ntUsrAddlpk%>&nwusersPk=<%=ntUsrPk%>&pageRight=<%=pageRight%>&srcmenu=<%=src%>&from=ntwusrhistory&fromjsp=usersnetworksites.jsp&currentStatId=<%=ntUsrAddlstathistory%>&moduleTable=er_nwusers_addnlroles&netWorkId=<%=netId%>&nLevel=<%=nLevel%>"><img border="0" title="<%=LC.L_History%>" alt="<%=LC.L_History%>" style="height:19px;width:19px;" src="./images/History.gif" ><%//=LC.L_H%><%--H*****--%></A>
<%if (ntUsrAddlstatSubTyp.equalsIgnoreCase("Pending") ) {%>
<a href="#" ><img style="height:19px;width:19px;" src="./images/delete.gif" onclick="deleteUserNetworkAddRole(<%=ntUsrAddlpk%>);" title="Delete" border="0"></a>
<%} %>
</div>
</td></tr></table> 
<%} %>
</td>
<td align="center"><img title="Network Appendix" style="height:19px;width:19px;cursor:pointer;" onclick="networkUserSitesAppendix('<%=ntUsrPk %>','<%=netId %>','<%=from %>','<%=ntUsr%>','<%=pageRight %>');" src="./images/Appendix.gif" border="0"></td>
<td align="center">
<% if(groupName.equalsIgnoreCase("Admin")){%>
<%if(len1>0)  {%>		
<%if ( ntUsrAddlstatSubTyp.equalsIgnoreCase("Pending") && ntUserStatusSubtype.equalsIgnoreCase("Pending")) {%>	
	<a href="#" ><img style="height:19px;width:19px;" src="./images/delete.gif" onclick="deleteUserNetwork(<%=ntUsrPk%>);" title="Delete" border="0"></a>
<%}}
else if(ntUserStatusSubtype.equalsIgnoreCase("Pending")){%>
<a href="#" ><img style="height:19px;width:19px;" src="./images/delete.gif" onclick="deleteUserNetwork(<%=ntUsrPk%>);" title="Delete" border="0"></a>
<%} %>
<%} %>
</td></tr>
<% }%>
</table>

<br/>
<div id="paginationDiv" align="center">		
	<%
		if (curPage==1) startPage=1;
	    for ( counter = 1; counter <= showPages;counter++)
		{
		
		 
   			cntr = (startPage - 1) + counter;
	 		if ((counter == 1) && (hasPrevious))
			{
	 			long prev=0;
	 			prev=cntr-1;
			  %>
				
			  	<A href="#" onclick ="pagetraverse('<%= prev%>','<%=searchNWUser%>','<%=netId%>','<%=from%>','<%=nLevel%>');">
			  	<%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%>  </A>
				&nbsp;&nbsp;&nbsp;&nbsp;
					
				<%
  			}	%>
		
		<%
 		 if (curPage  == cntr)
		 {	 
	    	 %>	   
				<FONT class = "pageNumber"><%= cntr %></Font>
       		<%
       	}else
        {	%>		
		<A href="#" onclick ="pagetraverse('<%= cntr%>','<%=searchNWUser%>','<%=netId%>','<%=from%>','<%=nLevel%>');"><%= cntr%></A>
       <%}%>
	
		<%	}
		if (hasMore)
		{ 
			long next=cntr+1;
		%>
		
	   &nbsp;&nbsp;&nbsp;&nbsp;
	   <A href="#" onclick ="pagetraverse('<%= next%>','<%=searchNWUser%>','<%=netId%>','<%=from%>','<%=nLevel%>');"> <%=LC.L_Next%><%--Next*****--%> <%=totalPages%></A>
		<%	}	%>
	</div>


<%}%> 