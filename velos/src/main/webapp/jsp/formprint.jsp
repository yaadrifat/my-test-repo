<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Print_FormData%><%--Print Form Data*****--%></title>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<Link Rel=STYLESHEET HREF='./styles/velos_popup.css' type=text/css>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
 
<SCRIPT LANGUAGE="JavaScript" SRC="formjs.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
window.onbeforeprint = function()
{
	$j('<style id="onlyPdf">.page-header { position: fixed; } .page-header, .page-header-space { height: 135px;}</style>').appendTo('head');
	document.getElementById('ptag').style.display = "none";
}

window.onafterprint = function()
{
	document.getElementById('ptag').style.display = "block";
	$j('#onlyPdf').remove();
}

</SCRIPT>
<style>
/* New Css */
/*#forms table {
    table-layout: fixed;
}*/
.page-header, .page-header-space {
 /* height: 135px; */
}
#ptag {
    position: relative;
    z-index: 99;
}
/* New Css End */
body#forms {
overflow:inherit;
}
html {
    height: 95%;
}
#ptag {
    margin-top: 0px;
}
     .my-header {
  background: red;
  top: 0;
  left: 0;
  position: fixed;
  right: 0;
}
.header1 {
  position: fixed;
  
}
.page-footer, .page-footer-space {
  height: 50px;

}


.page-footer {
 /* position: fixed;*/
  bottom: 0;
  width: 100%;
  border-top: 1px solid black; /* for demo */
  background: yellow; /* for demo */
}

.page-header {
  /* position: fixed; */
  top: 0mm;
  width: 100%;
 /* border-bottom: 1px solid black;*/ /* for demo */
  /*background: yellow;  for demo */
}
.page-header1 {
  position: fixed;
  top: 20mm;
  width: 100%;
  border-bottom: 1px solid black; /* for demo */
  background: yellow; /* for demo */
}
.page {
  page-break-after: always;
}

@page {
  margin: 10mm
}

@media print {
   thead {display: table-header-group;} 
   tfoot {display: table-footer-group;}
   
   button {display: none;}
   
   body {margin: 0;}
      .reportName {
    position: fixed;
    top: 118px;
    width: 100%
}
}

.marker{
background-color:Yellow;}
}
</style>

<%@page language = "java" import="com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.SaveFormDao,com.velos.eres.service.util.*,com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*, java.util.regex.*" %>
<%@page import="com.velos.eres.web.studyRights.StudyRightsJB" %>
<%@page import="com.velos.eres.business.linkedForms.impl.LinkedFormsBean" %>
<%@page import="com.velos.eres.web.patLogin.PatLoginJB" %>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="lnkformsB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
<jsp:useBean id="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="portalJB" scope="request" class="com.velos.eres.web.portal.PortalJB"/>
<jsp:useBean id="userb" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="userSiteB" scope="request" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="portalDesignB" scope="request" class="com.velos.eres.web.portalDesign.PortalDesignJB"/>

<body>
<%
HttpSession tSession = request.getSession(true);
 if (sessionmaint.isValidSession(tSession))
 {%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	int pageRight = 0;
	String userId = (String) tSession.getValue("userId");
	String strStudyId = request.getParameter("studyId");
	String formId = request.getParameter("formId");
	String commonformflag=request.getParameter("commonformflag")==null?"":request.getParameter("commonformflag");
	%>
	<% if (!StringUtil.isEmpty(strStudyId)) {%>
	<input type="hidden" id="studyId" name="studyId" value="<%=strStudyId%>"/>
	<%} %>
	<input type="hidden" id="formId" name="formId" value="<%=formId%>"/>
	<%
	int personPK = StringUtil.stringToNum(request.getParameter("pkey"));

	LinkedFormsBean linkedFormsBean = 
			lnkformsB.findByFormId(StringUtil.stringToNum(request.getParameter("formId")));
	String linkedType = StringUtil.trueValue(linkedFormsBean.getLFDisplayType()).trim();
	String ignoreRights = (String) tSession.getValue("pp_ignoreAllRights");
	boolean isPatientPortal = false;
	if (!StringUtil.isEmpty(ignoreRights)) { // Patient Portal
		isPatientPortal = true; 
	} else {
		ignoreRights = "false";
	}
	
	if (isPatientPortal) {
		PatLoginJB patLoginJB = (PatLoginJB)tSession.getAttribute("pp_currentLogin");
		String patientPortalPatId = patLoginJB.getPlId();
		if (StringUtil.stringToNum(request.getParameter("filledFormId")) > 0) {
			// Make sure patientPortalPatId = fk_per
			String ppFkPer = lnkformsB.getFormResponseFkPer(
					StringUtil.stringToNum(request.getParameter("filledFormId")));
			// Make sure creator of form response matches with portal's audit user
			String creatorForPortal = lnkformsB.getCreatorForPortal();
			boolean auditUserMatchesWithCreator = portalJB.comparePortalAuditUserWith(
					StringUtil.stringToNum(creatorForPortal), 
					StringUtil.stringToNum(patLoginJB.getFkPortal()));
			System.out.println("auditUserMatchesWithCreator="+auditUserMatchesWithCreator
					+" patientPortalPatId="+patientPortalPatId+" ppFkPer="+ppFkPer);
			if (auditUserMatchesWithCreator &&
					StringUtil.stringToNum(ppFkPer) > 0 &&
					StringUtil.stringToNum(patientPortalPatId) ==
					StringUtil.stringToNum(ppFkPer)) {
				pageRight = 4;
			}
		}
		
		// Make sure patientPortalPatId = pkey
		if (StringUtil.stringToNum(patientPortalPatId) != personPK) {
			pageRight = 0;
		}
		
		// Make sure studyId is the fk_study of portal
		int fkPortal = StringUtil.stringToNum(patLoginJB.getFkPortal());
		portalJB.setPortalId(fkPortal);
		portalJB.getPortalDetails();
		int portalStudy = StringUtil.stringToNum(portalJB.getPortalStudy());
		if (StringUtil.stringToNum(strStudyId) > 0) {
			if (StringUtil.stringToNum(portalJB.getPortalStudy()) != StringUtil.stringToNum(strStudyId)) {
				pageRight = 0;
			}
		}
		
		// Make sure formId is in portal design
		if (StringUtil.stringToNum(formId) > 0) {
			PortalDesignDao pdDao = portalDesignB.getPortalModValues(fkPortal);
			ArrayList fkIds = pdDao.getFkIds();
			ArrayList portalModTypes = pdDao.getPortalModTypes();
			boolean isValidFormId = false;
			for (int iX = 0; iX < portalModTypes.size(); iX++) {
				if (StringUtil.isEmpty(formId)) { break; }
				if ("SF".equals(portalModTypes.get(iX)) || 
						"EF".equals(portalModTypes.get(iX)) || "LF".equals(portalModTypes.get(iX))) {
					if (fkIds.get(iX) == null) { break; }
					String[] splitFormIds = ((String)fkIds.get(iX)).split(",");
					for (int iY = 0; iY < splitFormIds.length; iY++) {
						if (StringUtil.stringToNum(splitFormIds[iY]) == StringUtil.stringToNum(formId)) {
							isValidFormId = true;
							break;
						}
					}
				}
				if (isValidFormId) { break; }
			}
			if (!isValidFormId) {
				pageRight = 0;
			}
		}
	} else {
		// Calculate access rights -- this part for eResearch user
		pageRight = lnkformsB.getFilledFormUserAccess(request);
	}
	
	if  ( pageRight > 0 )//&& study_acc_form_right > 0 && study_team_form_access_right >0)
	{

   		 String filledFormId = request.getParameter("filledFormId");
		 String linkFrom = request.getParameter("linkFrom")==null?"":request.getParameter("linkFrom");
		 String formLibVer = request.getParameter("formLibVerNumber");//tkg

		 String studyId = "";
		 String studyDesc = "";
		 String studyNo ="";
		 String studyNumber = "";
		 String protocolId = "";
		 String protName = "";
		 String studyTitle = "";
		 int  deadStatPk  = 0;
		 String patientCode = "";
		 String age= "";
		 String gender = "";
		 String siteName = "";
		 String deathDate = "";
		 String enrollId= "";
		 int patStatusId= 0;
		 String patStudyId = "";
		 String organization = "";
		 String genderId = "";
		 String patientId ="" ;
		 String dob= "";
		 String yob = "";


		 Calendar cal1 = new GregorianCalendar();

		if((!linkFrom.equals("A")) || (!linkFrom.equals("USR") ) || (!linkFrom.equals("ORG") ) || (!linkFrom.equals("NTW") )|| (!linkFrom.equals("SNW") ) ){
			studyId = request.getParameter("studyId");

			if(studyId==null)
				studyId = "";
			if(studyId.equals("null"))
				studyId="";


			if(!studyId.equals("")){
			studyB.setId(EJBUtil.stringToNum(studyId));
			studyB.getStudyDetails();
			studyNumber = studyB.getStudyNumber();
			studyTitle = studyB.getStudyTitle();

			}
		}//else{
		%>
			<!-- <P class="defComments"><b><%=LC.L_Version_Number%><%--Version Number*****--%>: </b><%=formLibVer%> -->
		<%//}
		 if(linkFrom.equals("S"))
		 {%>
		 

		 
		 
			<!-- <P class="defComments"><b><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>: </b><%=studyNumber%> -->
			<!-- <P class="defComments"><b><%=LC.L_Version_Number%><%--Version Number*****--%>: </b><%=formLibVer%> -->
			
			<table with="100%" class= "page-header">
				<tr><td width="100%">
					<table width="100%" cellspacing="0" cellpadding="0"  >
		      <tr>
		    	<td width=50%>
		    		<P class="defComments"><b><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>: </b><%=studyNumber%>
		    	</td>
				<td width=50%>
		    		<P class="defComments"><b><%=LC.L_Version_Number%><%--Version Number*****--%>: </b><%=formLibVer%>
		    	</td>
		
		      </tr>
		     
				
					</table>

					
              </td></tr></table>
			
			

		<%}else if(linkFrom.equals("A")){%>

			<table with="100%" class= "page-header">
				<tr><td width="100%">
					<table width="100%" cellspacing="0" cellpadding="0"  >
		      <tr>
		    	
				<td width=50%>
		    		<P class="defComments"><b><%=LC.L_Version_Number%><%--Version Number*****--%>: </b><%=formLibVer%>
		    	</td>
		
		      </tr>
		     
				
					</table>

					
              </td></tr></table>
			<%}else if(linkFrom.equals("P") || linkFrom.equals("SP")){
			if (ignoreRights.equals("false")) //if not called from patient portal
			{



					CodeDao cd = new CodeDao();
					deadStatPk	= cd.getCodeId("patient_status","dead");
					protocolId = request.getParameter("protocolId");
					patientCode = request.getParameter("patientCode");


					enrollId= request.getParameter("patProtId");

					if(protocolId==null)
						protocolId = "";
					if(protocolId.equals("null"))
						protocolId="";
					if(patientCode==null)
						patientCode = "";
					if(patientCode.equals("null"))
						patientCode="";

					patientCode = StringUtil.decodeString(patientCode);

					if(enrollId==null)
						enrollId = "";
					if(enrollId.equals("null"))
						enrollId="";


					if(!enrollId.equals("")){
						patEnrollB.setPatProtId(EJBUtil.stringToNum(enrollId));
						patEnrollB.getPatProtDetails();
						String patProtStudyId = patEnrollB.getPatProtStudyId();
						if (StringUtil.trueValue(patProtStudyId).equals(studyId)) {
							patStudyId = patEnrollB.getPatStudyId();
						}
					}

					 if(personPK !=0){
						 int orgRight = userSiteB.getUserPatientFacilityRight(
						 			StringUtil.stringToNum(userId), personPK);
						 if (orgRight > 0) {
							personB.setPersonPKId(personPK);
							personB.getPersonDetails();
							patientId = personB.getPersonPId();
		    				organization = personB.getPersonLocation();
							genderId = personB.getPersonGender();
							dob = personB.getPersonDob();
							deathDate = personB.getPersonDeathDate();
						 }
					 }
					 if(organization!=null){
					siteB.setSiteId( EJBUtil.stringToNum(organization));
		    		siteB.getSiteDetails();
		    		siteName = siteB.getSiteName();
					 }

		    		gender = codeLst.getCodeDescription(EJBUtil.stringToNum(genderId));
					patStatusId= EJBUtil.stringToNum(personB.getPersonStatus());

					if (gender==null){ gender=""; }
					 
					// IH - Apply the same PHI masking as done in patienttabs.jsp
					String userIdFromSession = (String) tSession.getValue("userId");
					userb.setUserId(EJBUtil.stringToNum(userIdFromSession));
					userb.getUserDetails();
					int usrGroup=EJBUtil.stringToNum(userb.getUserGrpDefault());
					int patDataDetail = personB.getPatientCompleteDetailsAccessRight(EJBUtil.stringToNum(userIdFromSession),
					        usrGroup,personPK);
					// IH - end

				// modified by Gopu on 18th March for fixing the bug No.2063

		       	int patientMob=0;
				int patientDob=0;
				int patientYob=0;
				int patientAge=0;
				int sysYear=0;
				int sysMonth=0;
				int sysDate=0;
				int noOfMonths=0;
				int noOfYears=0;
				int noOfDays=0;
				if(!(dob== null) && !(dob.equals("")))
				{
		              java.util.Date dtDob = DateUtil.stringToDate(dob);
					  patientYob = dtDob.getYear()+1900;
					  patientMob = dtDob.getMonth()+1;
					  patientDob = dtDob.getDate();

					  //by sonia for issue 2497
					  if (! StringUtil.isEmpty(deathDate)) // if patient is dead, calculate age till his dod
					  {
			              java.util.Date dtDeath = DateUtil.stringToDate(deathDate);
			              sysYear = dtDeath.getYear()+1900;
			              sysMonth = dtDeath.getMonth()+1;
			              sysDate = dtDeath.getDate();

					  }
					  else
					  {
						sysMonth=cal1.get(Calendar.MONTH)+1;
						sysDate=cal1.get(Calendar.DATE);
					    sysYear=cal1.get(Calendar.YEAR);

					  }
					if (sysYear==patientYob)
						patientAge=0;
				  if (sysYear > patientYob)
				  {
					 patientAge=sysYear-patientYob;
		             if(patientMob > sysMonth)
						patientAge--;
					 if(patientMob==sysMonth && patientDob>sysDate)
						patientAge--;
				  }
		    	  if(patientAge!=0){
		    	  Object[] arguments2 = {String.valueOf(patientAge)};
				  age=VelosResourceBundle.getLabelString("L_Spc_Years",arguments2)/*{0}&nbsp;years*****/;}
				  if(patientAge==0)
				  {
					  if(patientMob <= sysMonth && sysDate>=patientDob)
						  noOfMonths=sysMonth-patientMob;
					else if(patientMob<sysMonth && patientDob>sysDate)
						noOfMonths=(sysMonth-patientMob)-1;
					  else if (patientMob>=sysMonth&&patientDob<=sysDate)
					  {
						  noOfMonths=(patientMob-sysMonth);
						  noOfMonths=12-(noOfMonths);
					  }
					  else if(patientMob>=sysMonth&&patientDob>sysDate)
					  {
						  noOfMonths=patientMob-sysMonth;
						  noOfMonths=11-(noOfMonths);
					  }
					  Object[] arguments = {String.valueOf(noOfMonths)};
					  age=VelosResourceBundle.getLabelString("L_Spc_Months",arguments)/*{0}&nbsp;months*****/;
				  }
				  if(patientAge==0 && noOfMonths==0)
				  {
					  if(patientDob<=sysDate)
						  noOfDays=(sysDate-patientDob)+1;
					  else
					  {
						  noOfDays=sysDate-patientDob;
						  if (patientMob==1)
							  noOfDays=31-(-noOfDays);
						  if (patientMob==2)
						  {
							  noOfDays=28-(-noOfDays);
							  if(sysYear%4==0 && (sysYear % 100 != 0 || sysYear % 400 == 0))
								  noOfDays=29-(-noOfDays);
						   }
						   if (patientMob==3)
							   noOfDays=31-(-noOfDays);
						   if (patientMob==4)
							   noOfDays=30-(-noOfDays);
						   if (patientMob==5)
							   noOfDays=31-(-noOfDays);
						   if (patientMob==6)
							   noOfDays=30-(-noOfDays);
						   if (patientMob==7)
							   noOfDays=31-(-noOfDays);
						   if (patientMob==8)
							   noOfDays=31-(-noOfDays);
						   if (patientMob==9)
							   noOfDays=30-(-noOfDays);
						   if (patientMob==10)
							   noOfDays=31-(-noOfDays);
						   if (patientMob==11)
							   noOfDays=30-(-noOfDays);
						   if (patientMob==12)
							   noOfDays=31-(-noOfDays);
						    noOfDays=noOfDays+1;
					  }
					  Object[] arguments1 = {String.valueOf(noOfDays)};
					  age=VelosResourceBundle.getLabelString("L_Spc_Days",arguments1)/*{0}&nbsp;days*****/;
				}
				  //yob = dob.substring(6,10);
				  //age = (new Integer(cal1.get(Calendar.YEAR) - EJBUtil.stringToNum(yob))).toString();
			}
			else
			{
			  age = "-";
			}

					if(protocolId != null) {
						eventAssocB.setEvent_id(EJBUtil.stringToNum(protocolId));
						eventAssocB.getEventAssocDetails();
			    	    if (strStudyId == null) {
			    	    	protName = 	eventAssocB.getName();
			    	    } else if (strStudyId.equals(eventAssocB.getChain_id())) {
							protName = 	eventAssocB.getName();
						}
					}
					%>
				<table with="100%" class= "page-header">
				<tr><td width="100%">
					<table width="100%" cellspacing="0" cellpadding="0"  >
		      <tr>
		    	<td width=20%>
		    		<B> <%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> ID*****--%>: <%=patientCode%></B>
		    	</td>
				<%if(linkFrom.equals("P") && !enrollId.equals("")){%>
				<td width=25%>
		    		<B> <%=LC.L_Patient_StudyId%><%--<%=LC.Pat_Patient%> <%=LC.Std_Study%> ID*****--%>: <%=patStudyId%></B>
		    	</td>
						<%}%>
			<!--modified on 040405 for fixing the Bug # 2063 -->

				<%if(!(dob== null) && !(dob.equals(""))){%>
				    	<td width=15%>
				    		<B> <%=LC.L_Age%><%--Age*****--%>: <%=patDataDetail >= 4 ? age : "*"%> </B>
		    			</td>
					<%} else { %>
				    	<td width=15%>
				    		<B> <%=LC.L_Age%><%--Age*****--%>: <%=patDataDetail >= 4 ? age : "*"%></B>
		    			</td>
					<%}%>

		 <!--   	<td width=15%>
		    		<B> Age: <%=age%> years</B>
		    	</td>
		 -->
		    	<td width=15%>
		    		<B><%=LC.L_Gender%><%--Gender*****--%>: <%=gender%> </B>
		    	</td>
		    	<td width=25%>
		    		<B><%=LC.L_Organization%><%--Organization*****--%>: <%=siteName%> </B>
		    	</td>
		      </tr>
		     
				<%if(deadStatPk == patStatusId){
				%>
				<tr>
				<td><br><P class = "defComments"><B><FONT class="Mandatory"><%=LC.L_Pat_DiedOn%><%--<%=LC.Pat_Patient%> Died on*****--%> <%=deathDate%></font></B></P></td>
				</tr>
				
				<%}%>
					</table>

					<%if(linkFrom.equals("P") && !enrollId.equals("")){
						%>
					<table width=100%>
					<br>
					<tr>
						<td class=tdDefault width = 20% ><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>:</td><td class=tdDefault><%=studyNumber%></td>
					</tr>
					<%-- 
					<tr>
						<td class=tdDefault><%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%><%--:</td><td class=tdDefault><%=studyTitle%></td>--%>
					<%-- </tr>
					<tr>
						<td class=tdDefault ><%=LC.L_Protocol_Calendar%><%--Protocol Calendar*****--%><%--:</td><td class=tdDefault><%=protName%></td>--%>
					<%--</tr>--%>
					
					<!-- Added by Gopu to fix the bugzilla issue #2808 -->
					<tr>
						<td class=tdDefault ><%=LC.L_Version_Number%><%--Version Number*****--%>:<%=formLibVer%></td><td class=tdDefault></td>
					</tr>

					</table>

					<%}else{ %>
					<table align="Left" >
					<tr align="Left">
						<td class=tdDefault align="Left" ><%=LC.L_Version_Number%><%--Version Number*****--%>:<%=formLibVer%></td><td align="left" class=tdDefault></td>
					</tr>
					</table>
				
		<%		 	} %>
              </td></tr></table>
  <%
					
		  }//check for patient portal
		}

         String formHtml = "";

	if(!commonformflag.equalsIgnoreCase(""))
	{
		formHtml =  lnkformsB.getPrintFormHtml(EJBUtil.stringToNum(formId),EJBUtil.stringToNum(filledFormId),commonformflag);
	}else{
		formHtml =  lnkformsB.getPrintFormHtml(EJBUtil.stringToNum(formId),EJBUtil.stringToNum(filledFormId),linkFrom);
	}
		 
   	     formHtml = formHtml.replaceAll("var fld;","var fld='';");
   	     formHtml=  formHtml.replaceAll("<U></U>","<span>_______________ </span>");
   	  
   		 //Fix for print issue (newline)
	     /*commented for resolving ticket #35491 Printer Friendly Format Fails to Properly Display Date*/
	     formHtml = formHtml.replaceAll("<U>", "<u>").replaceAll("</U>", "</u>");
	     formHtml=formHtml.replaceAll("\\\\","&#92;").replaceAll("\\[", "&#91;").replaceAll("\\]", "&#93;").replaceAll("\\*", "&#42;").replaceAll("\\(", "&#40;").replaceAll("\\)", "&#41;").replaceAll("\\?", "&#63;").replaceAll("\\{", "&#123;").replaceAll("\\}", "&#125;").replaceAll("\\+", "&#43;").replaceAll("\\|", "&#124;").replaceAll("\\$", "&#36;").replaceAll("\\^", "&#94;");
	     Pattern pattern = Pattern.compile("<u>(.*?)</u>", Pattern.DOTALL);
	     Matcher matcher = pattern.matcher(formHtml);
	     while(matcher.find()){
			 formHtml = formHtml.replaceAll(matcher.group(1), (matcher.group(1).replaceAll("(\\r|\\n|\\r\\n)", "<br>").replaceAll("\\s", "&nbsp;")));
	     }
	     formHtml=formHtml.replaceAll("&#92;","\\\\").replaceAll("&#91;","\\[").replaceAll( "&#93;","\\]").replaceAll("&#42;","\\*").replaceAll("&#40;","\\(").replaceAll("&#41;","\\)").replaceAll("&#123;","\\{").replaceAll("&#125;","\\}").replaceAll("&#43;","\\+").replaceAll("&#124;","\\|").replaceAll("&#36;", "\\$").replaceAll("&#94;","\\^");
	    
	     %>
	     
	<% FormSecDao fsdao = new FormSecDao();
	fsdao.getAllSectionsOfAForm(EJBUtil.stringToNum(formId));
    String formFmt=fsdao.getFormSecFmt().toString().substring(1, fsdao.getFormSecFmt().toString().length()-1); 
	   if(formFmt!=null && formFmt.contains("T"))
       {%>
       <style>
       #forms table {
    table-layout: inherit !important;
    width: 100%;
}
       </style>
    	
		   <% }else { %> 
		        <style>
         #forms table {
        table-layout: fixed;
}
       </style>
		   <%} %>
  <table style="table-layout: fixed;">

    <thead>
      <tr>
        <td>
          <!--place holder for the fixed-position header-->
          <div class="page-header-space"></div>
        </td>
      </tr>
    </thead>

    <tbody>
      <tr>
        <td>
         <div class="page">
         <P id="ptag" style="display:block" align="right" class="defComments"><A HREF="javascript:window.print()"><img border="0" title="<%=LC.L_Print%><%--Print*****--%>" alt="<%=LC.L_Print%><%--Print*****--%>" src="./images/printer.gif"/></A></P>
         <%=formHtml%>
         </div>
         </td>
      </tr>
    </tbody>
</table>

<%
if ("L2_ON".equals(LC.L_Auth2_Switch)){
	if (!StringUtil.isEmpty(filledFormId) && !"null".equals(filledFormId)){
	 	LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
		linkedFormsDao = lnkformsB.getFormResponseAuditInfo(EJBUtil.stringToNum(formId),EJBUtil.stringToNum(filledFormId));
	
		ArrayList arrayCreator = linkedFormsDao.getCreator(); 
		if (null != arrayCreator && arrayCreator.size() > 0){
			String creator = StringUtil.htmlEncode((String)arrayCreator.get(0));
			ArrayList arrayCreatedOn = linkedFormsDao.getCreatedon();
			String createdOn = (null != arrayCreatedOn && arrayCreatedOn.size() > 0)?
				StringUtil.htmlEncode((String)arrayCreatedOn.get(0)) : "";
	
			ArrayList arrayModifiedby = linkedFormsDao.getModifiedby();
			String modifiedby = "-";
			String modifiedOn = "-";
			if (null != arrayModifiedby && arrayModifiedby.size() > 0){
				modifiedby = (StringUtil.isEmpty((String)arrayModifiedby.get(0)))?
						"-":StringUtil.htmlEncode((String)arrayModifiedby.get(0));
	
				ArrayList arrayModifiedOn = linkedFormsDao.getModifiedon();
				modifiedOn = (null != arrayModifiedOn && arrayModifiedOn.size() > 0)?
								(String)arrayModifiedOn.get(0) : "-";
				modifiedOn = (StringUtil.isEmpty(modifiedOn))?
								"-":StringUtil.htmlEncode(modifiedOn);
			}
%>
<br>
<table width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td align="left">
			<table>
				<tr>
					<td class="tdDefault" align="left"><b>Created By: </b></td>
					<td class="tdDefault" align="left"><%=creator%></td>
				</tr>
				<tr>
					<td class="tdDefault" align="left"><b>Last Modified By: </b></td>
					<td class="tdDefault" align="left"><%=modifiedby%></td>
				</tr>
			</table>
		</td>
		<td align="right">
			<table>
				<tr>
					<td width="50%" class="tdDefault" align="left"><b>Created On: </b></td>
					<td width="50%" class="tdDefault" align="left"><%=createdOn%></td>
				</tr>
				<tr>
					<td width="50%" class="tdDefault" align="left"><b>Last Modified On: </b></td>
					<td width="50%" class="tdDefault" align="left"><%=modifiedOn%></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%
		}
	}
}
	}// end of if for page right
	else
	{

	%>
	  <jsp:include page="accessdenied.jsp" flush="true"/>
	  <%

	} //end of else body for page right

  } //end of if for session

 else
 {  //else of if body for session
  %>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
 %>
<div class = "myHomebottomPanel">
	<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</html>

<jsp:include page="jqueryUtils.jsp" flush="true"/>

<!-- Committed for Bug#18859 -->
<script>
if (navigator.userAgent.indexOf("MSIE") == -1){
	document.write('<style> body { overflow: visible !important } </style> ');
	document.write('<style> P.dynSection { margin: 12px 0 14px 6px } </style> ');
}
</script>