<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.text.SimpleDateFormat,java.util.*" %>
<%
String studyId = request.getParameter("studyId");
	String src;
	src= request.getParameter("srcmenu");
	String selectedTab = request.getParameter("selectedTab");
String invNumber = "",invStr="";
String invNumberSubStr = "";
%>
<style>
.custom-table-td img {
    float: right;
    margin-right: 10PX;
}
 div#overDiv {
    position: absolute !important;
}
</style>
	<SCRIPT>
	function AddInvoice(windowName,pgRight)
	{
	if (f_check_perm(pgRight,'N') == true) {
      windowName= window.open(windowName,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=530,left=125,top=200")
      windowName.focus();
	}else {
		return false;
	}

	}

	function viewInvoice(strCovTypes,inv,invNumber,mode,pgright)
	{
	 if (mode == 'E')
	 {
       if(!f_check_perm(pgright,'E')) {
	          return false;
	  }
	 } 
      windowName= window.open("viewInvoice.jsp?invPk=" + inv +"&mode=" + mode + "&CoverType="+strCovTypes+"&currInvNumber="+invNumber,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=530,left=125,top=200")
      windowName.focus();

	}
	 function confirmBox(name,pageRight,value){
	 
	  if(!f_check_perm(pageRight,value)) {
	          return false;
	  }

	  	var paramArray = [name];
        if (confirm(getLocalizedMessageString("M_Want_DelInv",paramArray))) {/* if (confirm("Do you want to delete Invoice "+ name+"?" )) {*****/
              return true;
       }
        else
       { 
	  return false;
	  }

}
function setOrder(formObj,orderBy) {

	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";		
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
	}
	orderType=formObj.orderType.value;
	paymentPk=formObj.paymentPk.value;
	pageRight=formObj.pageRight.value;
	calledFrom=formObj.calledFrom.value;
	cntr=formObj.page.value;
	if(calledFrom=='p')
	formObj.action="linkInvPayment.jsp?srcmenu=tdmenubaritem7&selectedTab=7&studyId="+"<%=studyId%>"+"&paymentPk="+paymentPk+"&pR="+pageRight+"&orderBy="+orderBy+"&orderType="+orderType+"&page="+cntr;
	else
	formObj.action="invoicebrowser.jsp?srcmenu=tdmenubaritem7&selectedTab=7&studyId="+"<%=studyId%>"+"&orderBy="+ orderBy +"&orderType="+orderType;	
	formObj.submit();
}

function setPaymentDueDateOrder(formObj,orderBy) {

	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";		
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
	}

	orderType=formObj.orderType.value;
	paymentPk=formObj.paymentPk.value;
	pageRight=formObj.pageRight.value;
	calledFrom=formObj.calledFrom.value;
	cntr=formObj.page.value;
	if(calledFrom=='p')
	formObj.action="linkInvPayment.jsp?srcmenu=tdmenubaritem7&selectedTab=7&studyId="+"<%=studyId%>"+"&paymentPk="+paymentPk+"&pR="+pageRight+"&orderBy="+orderBy+"&orderType="+orderType+"&page="+cntr;
	else
	formObj.action="invoicebrowser.jsp?srcmenu=tdmenubaritem7&selectedTab=7&studyId="+"<%=studyId%>"+"&orderBy="+ orderBy +"&orderType="+orderType;	
	formObj.submit();
}


//KM-031708
function printWindow(invPk){
       windowName= window.open("invoicePrint.jsp?invPk=" + invPk,"Information", "toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=530,left=125,top=200")
       windowName.focus();
    
}



//JM: 13Mar2008: #FIN11
 function openWinStatus(pgRight,mode, studyNumber, invoiceId,invNumber,statusId){
 

	var checkType;

	 
		checkType = 'E';

	var otherParam;

		otherParam = "&moduleTable=er_invoice&statusCodeType=invStatus&sectionHeading=<%=MC.M_MstoneInv_StatDets%>&statusId=" + statusId;/*otherParam = "&moduleTable=er_invoice&statusCodeType=invStatus&sectionHeading=Milestones >> Invoicing >> Status Details&statusId=" + statusId;*****/

		if (f_check_perm(pgRight,checkType) == true) {

			windowName= window.open("editstatus.jsp?mode=" + mode + "&studyNumber="+ studyNumber+"&modulePk=" +  invoiceId + "&invNumber=" + invNumber + otherParam,"statusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=500,height=300");
			windowName.focus();
		}
}

</SCRIPT>
<SCRIPT LANGUAGE="javascript">
var isIE6 = isIE6Browser();

function fnOnceEnterKeyPress(e) {
	var evt = (e) || window.event;
    if (!evt) { return 0; }
	try {
        var code = evt.charCode || evt.keyCode;
        if (code == 13 || code == 10) {
            var thisTimeSubmitted = new Date();
            if (!lastTimeSubmitted) { lastTimeSubmitted = 0; }
            if (!thisTimeSubmitted) { return 0; }
            if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
                return -1;
            }
            lastTimeSubmitted = thisTimeSubmitted;
            return 1;
        }
	} catch(e) {}
	return 0;
} //fnOnceEnterKeyPress function closed

function fnClickOnce(e) {
	try {
        var thisTimeSubmitted = new Date();
        if (!lastTimeSubmitted) { return true; }
        if (!thisTimeSubmitted) { return true; }
        if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
            return false;
        }
        lastTimeSubmitted = thisTimeSubmitted;
	} catch(e) {}
    return true;
}//fnClickOnce function closed

var matches = new Array();
var failedData = '';
var paramArray= new Array();
var pArray =new Array();
var pFnlArray =new Array();
var strsplitArr =new Array();
var sInvc = "";
var flagCheckRecnld = false;
$j(window).load(function() {/*unchecked all checked checkboxes */
	 $j('input:checkbox').removeAttr('checked');
     $j(this).val('check all');    
});

$j(function(){

$j("#closeBttnAftr").click(function () {
	$j('input:checkbox').removeAttr('checked');
     $j(this).val('check all');
	window.location.reload();
});

$j("#closeBttn").click(function () {
	$j('input:checkbox').removeAttr('checked');	
});


	// add multiple select / deselect checkbox functionality
	$j(".allInv").click(function () {
		  $j('.InvOpt').attr('checked', this.checked);		  		
		  //$j(".InvOpt").removeAttr("checked");
	});
 
	// if any of checkboxes other than Delete Column header's checkbox is selected, uncheck Delete Column header's checkbox
	$j(".InvOpt").click(function(){ 
		if($j(".InvOpt").length == $j(".InvOpt:checked").length) {
			$j(".allInv").attr("checked", "checked");
		} else {
			$j(".allInv").removeAttr("checked");
		} 
	});
	/*This function is used to open dialog box with selected invoice(s)*/
	$j(".delSelected").click(function () {
		$j("#eSign").val('');		
		hideSplashMessage();
		$j('#InvSlctdData').empty();
		var removeItem = '';
		if ($j('input:checked').attr('checked') != true) {
			hasSelected = true;
			alert(M_AtLeast_OneCheckbox);/*Please check at least one checkbox.*/
		}//if closed
		else{
				
				if($j.browser.msie){
					$j( "#dialogDelete" ).dialog({
						modal:true,
						height: '100%',
						width: '25%'
					});
				}
				else{
					$j( "#dialogDelete" ).dialog({modal:true});
				}			

				matches = jQuery.grep(matches, function(value) {
					return value == removeItem;
				});

				paramArray = jQuery.grep(paramArray, function(value) {
					return value == removeItem;
				});

				pFnlArray = jQuery.grep(pFnlArray, function(value) {
					return value == removeItem;
				});

				strsplitArr = jQuery.grep(strsplitArr, function(value) {
					return value == removeItem;
				});
				
				$j("input:checked").each(function() {					
					matches.push(this.value);				
				});
				
				for (var prop=0;prop<matches.length;prop++){
					if(matches[prop]!=L_On){
					var strsplit=matches[prop];
					if(strsplit.indexOf("~~")>0)
						strsplitArr=strsplit.split('~~');
					var finalInvcStr="";
						if(strsplitArr[1].length>20)
							finalInvcStr=strsplitArr[1].substr(0,20)+"&nbsp;<IMG src ='./images/More.png' class='asIsImage' style='position:absolute;z-index: 1000;top-200px;' border=0 onMouseOver=\"return overlib('"+strsplitArr[1]+"',CAPTION,'<%=LC.L_Invoice%><%--Invoice*****--%>',FIXX, 700);\" onMouseOut=\"return nd();\"/><br>";
						else
							finalInvcStr=strsplitArr[1]+"<br>";

						$j('#InvSlctdData').append("#"+finalInvcStr);
					}//if closed
				}//for closed
		}//else closed
	});//delSelected function closed
	/*This function is used to delete selected invoice(s)*/
	$j("#submit_btn").click(function(){ 

		failedData = '';
		
		if($j("#eSignMessage").text()==L_Invalid_Esign){
			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
			$j("#eSign").focus();
			return false;
		}

		if($j("#eSign").val()==null || $j("#eSign").val()==''){
			
			if($j('#dialogDelete').closest('.ui-dialog').is(':visible')){
				alert("<%=MC.M_PlsEnterEsign%>");/*alert("Please enter e-Signature");*****/
				$j("#eSign").focus();
				return false;
			}
		}//if closed
	<%-- 	else if (isNaN($j("#eSign").val()) == true) {
			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
			$j("#eSign").focus();
			return false;
		} --%>
		else{			
			showProgressMessage();
			for (var prop=0;prop<matches.length;prop++){
				if(matches[prop]!=L_On){
					var strsplit=matches[prop];
					if(strsplit.indexOf("~~")>0)
						strsplitArr=strsplit.split('~~');

					var urlParam = "studyId=<%=studyId%>&invoiceId="+strsplitArr[0]+"&selectedTab=<%=selectedTab%>&srcmenu=<%=src%>&invcName="+strsplitArr[1];
					if(checkReconciledData(urlParam+"&delMode=true"))
						return false;
				}//if closed
			}//for closed


		if(failedData=="" || failedData==null){	
					for (var prop=0;prop<matches.length;prop++){
						if(matches[prop]!=L_On){
							var strsplit=matches[prop];

							if(strsplit.indexOf("~~")>0)
								strsplitArr=strsplit.split('~~');

							var urlParam = "studyId=<%=studyId%>&invoiceId="+strsplitArr[0]+"&selectedTab=<%=selectedTab%>&srcmenu=<%=src%>&invcName="+strsplitArr[1];
							if (isIE6) { 					
								submitData(urlParam); 
							}//if closed
							else { 
								//setTimeout('submitData('+urlParam+')', 100); 					
								submitData(urlParam); 					
							}//else closed
							
						}//if closed
					}//for closed

		}

				return true;
		}//else closed		
				
	});//#submit_btn function closed

});//main function closed

/*This function is used to check selected invoice(s) Id is Reconciled or Not*/
function checkReconciledData(urlParam) {
		$j.ajax({
		url:'checkDeleteInvoice.jsp',
		type: "POST",
		async:false,
		data:urlParam,		
		success: handleReconciledDataSuccess,
		error:function(response) { hideSplashMessage(); alert(data); return false; }
	});	
	return flagCheckRecnld;
}//submitData function closed

/*This is callback function to recieve response from server to check selected invoice(s) Id is Reconciled or Not*/
function handleReconciledDataSuccess(data) {
	try{
			var response=$j(data);
			var invcName = response.filter('#invcName').val();//response.filter('#resultMsg').val();
			var message = response.filter('#message').val();

			/*if(message == M_Failed){
				if(invcName != "undefined"){
					if(invcName!=null && invcName!=""){
						failedData +="#"+invcName+", ";
					}//if closed
				}//if closed
					openFailedDelDialogBox(message);			
			}//if closed
			else*/ if(message == M_NotDelete){

				if(invcName != "undefined"){
					if(invcName!=null && invcName!=""){
						if(failedData.indexOf(invcName)==-1)
							failedData +="#"+invcName+", ";
					}//if closed
				}//if closed
					openNotDelDialogBox();
					flagCheckRecnld = false;
			}//else if closed

			/*else{
					if (!data && !data.result) { hideSplashMessage(); return false; }
					if (invcName.length < 0) { hideSplashMessage(); alert(data); return false; }	
					showSplashMessage();		
					setTimeout('$j("#dialogDelete").dialog("close"); hideSplashMessage(); ', 2000);	
			}//else closed*/
			
	}catch(e1){}
	return flagCheckRecnld;		
}//handleReconciledDataSuccess function closed

/*This function is used to open dialog box to show message for non-deleted invoice(s).*/
function openNotDelDialogBox(){	
		$j('#InvNotDeletedData').empty();

		if($j.browser.msie){
			$j( "#dialogNotDelete" ).dialog({
				modal:true,
				height: '100%',
				width: '25%'
			});
		}
		else{
			$j( "#dialogNotDelete" ).dialog({
			modal:true});
		}	
		$j("#dialogDelete").dialog('close');
		pArray =failedData.split(",");// [failedData.substring(0, failedData.length - 1).replace(/,([^,]*)$/,' and $1')];
		for (var prop=0;prop<pArray.length;prop++){
		if(sInvc.indexOf(pArray[prop])==-1){
			if(pArray[prop].length>20)
				sInvc+=pArray[prop].substr(0,20)+"&nbsp;<IMG src ='./images/More.png' class='asIsImage' style='position:absolute;z-index: 1000;top-200px;' border=0 onMouseOver=\"return overlib('"+pArray[prop]+"',CAPTION,'<%=LC.L_Invoice%><%--Invoice*****--%>',FIXX, 800);\" onMouseOut=\"return nd();\"/>&nbsp;&nbsp;&nbsp;&nbsp;<br>,";
			else
				sInvc+=pArray[prop]+", ";
		}

		}
		
		sInvc=sInvc.replace(/<br>,([^,]*)/,' , $1');		
		var lastIndex = sInvc.lastIndexOf(" ");
		sInvc=sInvc.substr(0,lastIndex);
		pFnlArray =[sInvc.replace(/\w+[.!?]?$/, '')];		
		var TEXT=getLocalizedMessageString("M_Invc_PaymentsReconciledAgainstInvcsCannotDeleted",pFnlArray);		
		TEXT=TEXT.replace(", .",'.');
		TEXT=TEXT.replace(",.",'.');
		$j('#InvNotDeletedData').append(TEXT.replace(",.",'.'));//getLocalizedMessageString("M_Invc_PaymentsReconciledAgainstInvcCannotDeleted",pFnlArray));
	}//openNotDelDialogBox function closed

/*This function is used to open dialog box to show Unsuccessfull deletion message for invoice(s).*/	
function openFailedDelDialogBox(message){	
		$j('#InvNotDeletedData').empty();
		$j("#dialogDelete").dialog('close');
		if($j.browser.msie){
			$j( "#dialogNotDelete" ).dialog({
				modal:true,
				height: '100%',
				width: '25%'
			});
		}
		else{
			$j( "#dialogNotDelete" ).dialog({modal:true});
		}
		pArray =failedData.split(",");// [failedData.substring(0, failedData.length - 1).replace(/,([^,]*)$/,' and $1')];
		for (var prop=0;prop<pArray.length;prop++){
		if(sInvc.indexOf(pArray[prop])==-1){
			if(pArray[prop].length>20)
				sInvc+=pArray[prop].substr(0,20)+"&nbsp;<IMG src ='./images/More.png' class='asIsImage' style='position:absolute;z-index: 1000;top-200px;' border=0 onMouseOver=\"return overlib('"+pArray[prop]+"',CAPTION,'<%=LC.L_Invoice%><%--Invoice*****--%>',FIXX, 800);\" onMouseOut=\"return nd();\"/>&nbsp;&nbsp;&nbsp;&nbsp;<br>,";
			else
				sInvc+=pArray[prop]+",";
		}

		}
		
		sInvc=sInvc.replace(/<br>,([^,]*)/,' , $1');		
		var lastIndex = sInvc.lastIndexOf(" ");
		sInvc=sInvc.substr(0,lastIndex);		
		pFnlArray =[sInvc.replace(/\w+[.!?]?$/, '')];
		var TEXT=sInvc;
		TEXT=TEXT.replace(", .",'.');
		TEXT=TEXT.replace(",.",'.');
		$j('#InvNotDeletedData').append(TEXT.replace(",.",'.')+" "+M_InvCnt_DelSucc);//.append(pArray[0]+" "+M_InvCnt_DelSucc);//" Invoice could not be deleted successfully.");

}//openFailedDelDialogBox function closed	

/*This function is used to send selected invoice(s) Id for deletion*/
function submitData(urlParam) {
		$j.ajax({
		url:'checkDeleteInvoice.jsp',
		type: "POST",
		async:false,
		data:urlParam,		
		success: handleSuccess,
		error:function(response) { hideSplashMessage(); alert(data); return false; }
	});	
}//submitData function closed

/*This is callback function to recieve response from server for sent Invoice(s) Id for deletion*/
function handleSuccess(data) {
	try{
			var response=$j(data);
			var invcName = response.filter('#invcName').val();//response.filter('#resultMsg').val();
			var message = response.filter('#message').val();

			if(message == M_Failed){
				if(invcName != "undefined"){
					if(invcName!=null && invcName!=""){
						failedData +="#"+invcName+", ";
					}//if closed
				}//if closed
					openFailedDelDialogBox(message);			
			}//if closed
			/*else if(message == M_NotDelete){

				if(invcName != "undefined"){
					if(invcName!=null && invcName!=""){
						if(failedData.indexOf(invcName)==-1)
							failedData +="#"+invcName+", ";
					}//if closed
				}//if closed
					openNotDelDialogBox();
			}*///else if closed
			else{
					if (!data && !data.result) { hideSplashMessage(); return false; }
					if (invcName.length < 0) { hideSplashMessage(); alert(data); return false; }	
					showSplashMessage();		
					setTimeout('$j("#dialogDelete").dialog("close"); hideSplashMessage(); ', 2000);	
			}//else closed
			
	}catch(e1){}
	return false;		
}//handleSuccess function closed

/*This function is used to show loading message.*/
function showProgressMessage() {
	$j("#formdiv").hide();	
	$('progressMsg').style.display = 'block';
	$('successMsg').style.display = 'none';
}//showProgressMessage function closed

/*This function is used to show successfull message.*/
function showSplashMessage() {
	$j("#formdiv").hide();
	$('progressMsg').style.display = 'none';
	$('successMsg').style.display = 'block';
	var reloadTime = 1000; // FF
	if (navigator.userAgent.indexOf("MSIE") != -1) { 1990; } // IE
	setTimeout('window.location.reload();', reloadTime);

}//showSplashMessage function closed

/*This function is used to hide successfull and Loading message.*/
function hideSplashMessage() {
	$j('#eSignMessage').hide();
	$j("#formdiv").show();
	$('progressMsg').style.display = 'none';
	$('successMsg').style.display = 'none';
}//hideSplashMessage function closed
</SCRIPT>


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="invB" scope="page" class="com.velos.eres.web.invoice.InvoiceJB" />
<%@ page language = "java" import = "java.math.BigDecimal,com.velos.eres.web.invoice.InvoiceJB,com.velos.eres.business.common.InvoiceDao,com.velos.eres.business.common.TeamDao,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*,com.velos.esch.business.common.*"%>

<%


	studyB.setId(EJBUtil.stringToNum(studyId));
	studyB.getStudyDetails();
	String studyNumber = studyB.getStudyNumber();
	
	String pagenum = "";
	pagenum = request.getParameter("page");

	if (pagenum == null || StringUtil.isEmpty(pagenum)){
		pagenum = "1";
	}
	
	String calledFrom = request.getParameter("calledFrom");
	int pageRight = EJBUtil.stringToNum(request.getParameter("pageRight"));
	String paymentPk = request.getParameter("paymentPk");


if (StringUtil.isEmpty(calledFrom))
{
	 calledFrom = "";
}

		InvoiceDao invdao = new InvoiceDao();
		int invCount = 0;
		//invdao = invB.getSavedInvoices(studyId);
		
		String orderBy="", orderType = "";
		long cntr=0;

		orderBy=request.getParameter("orderBy");
		if (EJBUtil.isEmpty(orderBy) || (orderBy==null)) {
			orderBy="inv_number";
			orderType="lower(inv_number) asc" ;
		}
		
		orderType = request.getParameter("orderType");
		if (orderType == null || StringUtil.isEmpty(orderType)) orderType = "asc";
		
		// Pagination Implemententation for Bug#13372 : Raviesh	
		int curPage = 0;
        curPage = EJBUtil.stringToNum(pagenum);
		Map<String,Object> getRowsCount=new HashMap<String,Object>();
		invdao = invB.getSavedInvoicesWithPagination(studyId,orderBy,orderType, getRowsCount, curPage);
		getRowsCount = invdao.getRowsCount();
		
		long rowsPerPage=0;
	   	long totalPages=0;
	   	rowsPerPage = Configuration.ROWSPERBROWSERPAGE ;
		totalPages = Configuration.PAGEPERBROWSER ;
		long rowsReturned = 0;
		long showPages = 0;
		long startPage = 1; 
		boolean hasMore = false;
  	    boolean hasPrevious = false;
  	    long firstRec = 0;
	    long lastRec = 0;	   
	    long totalRows = 0;
        
		for(String key : getRowsCount.keySet()) {
			  Object value = getRowsCount.get(key);
			  if(key.equalsIgnoreCase("rowsReturned")){
				  rowsReturned=Long.parseLong(value.toString());
			  }else if(key.equalsIgnoreCase("showPages")){
				  showPages=Long.parseLong(value.toString());
			  }else if(key.equalsIgnoreCase("startPage")){
				  startPage=Long.parseLong(value.toString());
			  }else if(key.equalsIgnoreCase("hasMore")){
				  hasMore=Boolean.parseBoolean(value.toString());
			  }else if(key.equalsIgnoreCase("hasPrevious")){
				  hasPrevious=Boolean.parseBoolean(value.toString());
			  }else if(key.equalsIgnoreCase("firstRec")){
				  firstRec=Long.parseLong(value.toString());
			  }else if(key.equalsIgnoreCase("lastRec")){
				  lastRec=Long.parseLong(value.toString());
			  }else if(key.equalsIgnoreCase("totalRows")){
				  totalRows=Long.parseLong(value.toString());
			  }
		}
		
		// Pagination implemented for Bug#13372 : Raviesh
		
		ArrayList paymentDueDateArr=new ArrayList();
		
		ArrayList arId = new ArrayList ();
		ArrayList  arInvDate = new ArrayList ();

		ArrayList arInvIntervalFrom = new ArrayList ();
		ArrayList  arInvIntervalTo = new ArrayList ();

		ArrayList arInvNotes = new ArrayList();

		ArrayList arInvNumber = new ArrayList();

		ArrayList  arCreator = new ArrayList();
		
		ArrayList arUserData = new ArrayList();

//JM:
		ArrayList arInvStats = new ArrayList();

	 	ArrayList arHistoryIds = new ArrayList();

	 	ArrayList arSubTypes = new ArrayList();
	 	
	 	ArrayList arCovTypes = new ArrayList();
	 	
	 	ArrayList arAmtInv = new ArrayList();

		String invDate = "";		
		String invCreatedBy = "";
		String invUserData="";
		String invNotes = "";
		String invRange = "";
		String invId = "";
		String invoiceId = "";
		String dateFrom = "";
		String dateTo = "";

		String invStat = "";
		String historyId = "";
		String subType = "";
		String strCovTypes = "";
		
		String amtInvoiced = "";
		String paymentDueDate = "";
		
		if (invdao != null)
		{
			arId =  invdao.getId();
            arInvNumber = invdao.getInvNumber();
            arInvDate =   invdao.getInvDate();
            arCreator =   invdao.getCreator();
            arUserData = invdao.getUserData();
            arInvIntervalFrom =  invdao.getInvIntervalFrom();
            arInvIntervalTo =  invdao.getInvIntervalTo();
            arInvNotes =    invdao.getInvNotes();
            arInvStats =    invdao.getInvStats();
            arHistoryIds =    invdao.getHistoryIds();
			arSubTypes = invdao.getSubTypes();
			arCovTypes = invdao.getCovType();
			
			arAmtInv = invdao.getAmtInv();
			

			if (arId == null)
			{
				arId = new ArrayList();
			}
		}
		
		HashSet<String> invHashSet = new HashSet<String>();

		%>
		
		<Form name="invoicebrowser" method="post" onsubmit="if (validatedate(document.invoicebrowser,'invoicebrowser.jsp?srcmenu=tdmenubaritem7&selectedTab=7&studyId=<%=studyId%>')==false) {setValidateFlag('false');return false;} else {setValidateFlag('true'); return true;}">
		<input type="hidden" name="orderType" value="<%=orderType%>">
		<input type="hidden" name="orderBy" value="<%=orderBy%>">
		<Input type="hidden" name="page" value="<%=curPage%>">
		<input type="hidden" name="studyId" value="<%=studyId%>">
		<input type="hidden" name="paymentPk" value="<%=paymentPk%>">
		<input type="hidden" name="pageRight" value="<%=pageRight%>">
		<input type="hidden" name="calledFrom" value="<%=calledFrom%>">
		
		<%
		if (StringUtil.isEmpty(calledFrom))
	 	{
	 	%>
	 	<table width="100%"><tr height="18"><td align="left">
	 	<A href="invoice_step1.jsp?studyId=<%=studyId%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>"  onClick="return f_check_perm(<%=pageRight%>,'N')"><%=MC.M_CreateNew_Inv_Upper%><%--CREATE A NEW INVOICE*****--%></A>
		</td>
		<td align="right">
	 	 <A class="delSelected" href="#" ><%=(LC.L_Del_Selected).toUpperCase()%><%--Delete Selected*****--%></A>
		
		<!--<button onclick="showLayer() ;" name="save_changes" id="save_changes" type="submit" ><%//=LC.L_Del_Selected%></button>--></td></tr></table>
		<% } %>

	 <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midAlign" >
      <tr>
        <th width="10%" onclick="setOrder(document.invoicebrowser,'inv_number')"><%=LC.L_Invoice%><%--Invoice*****--%> # &loz;</th>
        <th width="5%"> <%=LC.L_Edit %><%--Edit*****--%></th>
        <th width="7%" onclick="setOrder(document.invoicebrowser,'invdate')"><%=LC.L_Invoice_Date%><%--Invoice Date*****--%> &loz;</th>
        <th width="10%" onclick="setOrder(document.invoicebrowser,'amount_invoiced')"><%=LC.L_Inv_Amt%><%--Invoice Amount*****--%> &loz;</th>
        <th width="10%" onclick="setOrder(document.invoicebrowser,'Payment_DueDt')"><%=LC.L_Payment_DueDt%><%--Payment Due Date*****--%> &loz;</th>
        <th width="10%" onclick="setOrder(document.invoicebrowser,'creator')"><%=LC.L_Created_By%><%--Created By*****--%> &loz;</th>
        <th width="10%" onclick="setOrder(document.invoicebrowser,'inv_intervalfrom')"><%=LC.L_Date_Range%><%--Date Range*****--%> &loz;</th>
        <th width="20%" onclick="setOrder(document.invoicebrowser,'invoice_Status')"><%=LC.L_Status%><%--Status*****--%> &loz;</th>
        <th width="23%" onclick="setOrder(document.invoicebrowser,'inv_notes')"><%=LC.L_Notes%><%--Notes*****--%> &loz;</th>
        <%if (StringUtil.isEmpty(calledFrom)){  %>
        <th width="5%"><%=LC.L_Delete%><%--Delete*****--%> <%if(arId!=null && arId.size()>0){%>&nbsp;&nbsp;<input title="<%=MC.M_CheckToSelectDisplayedInvc %>" alt="<%=MC.M_CheckToSelectDisplayedInvc %>" type="checkbox" name="allInvOpt" id="allInvOpt" class="allInv" /><%}%> </th>
        <%}else{ %>
        <th width="5%"><%=LC.L_Select%><%--Select*****--%></th>
        <%} %>
      </tr>

<%
	invCount = arId.size();

	InvoiceJB invJb = new InvoiceJB();

	for(int counter = 0;counter<invCount;counter++)
	{	
		invId = (String) arId.get(counter);	
		invJb.setId((Integer.parseInt(invId)));
		invJb.getInvoiceDetails();
		paymentDueDateArr.add(invJb.getCalculatedInvPaymentDate());
	}

	Date date = new Date(System.currentTimeMillis());
	SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

	/*String ordersBy="";
	ordersBy =request.getParameter("ordersBy");
	if(ordersBy!=null){
	Collections.sort(paymentDueDateArr);
	//Collections.sort(arInvNumber);
	if(orderType.equalsIgnoreCase("desc")){
		Collections.reverse(paymentDueDateArr);
		Collections.reverse(arInvNumber);
		Collections.reverse(arId);
        Collections.reverse(arInvDate);
        Collections.reverse(arCreator);
        Collections.reverse(arInvIntervalFrom);
        Collections.reverse(arInvIntervalTo);
        Collections.reverse(arInvNotes);
        Collections.reverse(arInvStats);
        Collections.reverse(arHistoryIds);
        Collections.reverse(arSubTypes);
        Collections.reverse(arCovTypes);
        Collections.reverse(arAmtInv);
		
		}
	}*/
	 int counterRow = 1;
	 for(int counter = 0;counter<invCount;counter++)
	{
	 	BigDecimal amountInvoiced = new BigDecimal("0.0");
	 	BigDecimal amount_Invoiced = new BigDecimal("0.0");
	 	BigDecimal totalAmountInvoiced = new BigDecimal("0.0");
		dateFrom =  (String) arInvIntervalFrom.get(counter);
        dateTo =    (String) arInvIntervalTo.get(counter);
		invDate = (String) arInvDate.get(counter);
		invNumber = (String) arInvNumber.get(counter);
		invCreatedBy = (String) arCreator.get(counter);
		invUserData = (String) arUserData.get(counter);
		invNotes = (String) arInvNotes.get(counter) ;
		invId = (String) arId.get(counter);
		invJb.setId((Integer.parseInt(invId)));
		invJb.getInvoiceDetails();	
		paymentDueDate = (String)paymentDueDateArr.get(counter);
		//paymentDueDate = invJb.getCalculatedInvPaymentDate();
		
		paymentDueDateArr.add(invJb.getCalculatedInvPaymentDate());
		//JM:
		invStat = (String) arInvStats.get(counter);
		invStat=(invStat==null)?"":invStat;

		historyId = (String) arHistoryIds.get(counter);
		historyId=(historyId==null)?"":historyId;

		subType = (String) arSubTypes.get(counter);
		subType = (subType==null)?"":subType;
		
		if(arCovTypes!=null  && arCovTypes.size()>0 ){
			strCovTypes = (String) arCovTypes.get(counter);
			strCovTypes = (strCovTypes==null)?"":strCovTypes;			
		}
		
		amount_Invoiced = new BigDecimal(Double.parseDouble((String) arAmtInv.get(counter)));
		
		if(StringUtil.isEmpty(paymentDueDate))
		{
			paymentDueDate = "";
		}
		
		if (StringUtil.isEmpty(dateFrom))
		{
			dateFrom = "";
		}

		if (StringUtil.isEmpty(dateTo))
		{
			dateTo = "";
		}

		if (StringUtil.isEmpty(invDate))
		{
			invDate = "";
		}

			if (StringUtil.isEmpty(invNumber))
		{
			invNumber = "";
		}
			if (StringUtil.isEmpty(invCreatedBy))
		{
			invCreatedBy = "";
		}
			if (StringUtil.isEmpty(invUserData))
			{
				invUserData = "";
			}
			
		if (StringUtil.isEmpty(invNotes))
		{
			invNotes = "";
		}

		if (! StringUtil.isEmpty(dateFrom ))
		{
			invRange = dateFrom + " - " + dateTo;
		}
		else
		{
			invRange = LC.L_All/*"All"*****/;
		}
		invStr = invNumber;
		
		if(invNumber.length() > 20){
		   invNumberSubStr = invNumber.substring(0,20);
		}
		else {
		  invNumberSubStr = invNumber;

		}
		
		for (int invCounter = 0;invCounter<invCount;invCounter++){
			invoiceId = (String) arId.get(invCounter);
			amountInvoiced = new BigDecimal(Double.parseDouble((String) arAmtInv.get(invCounter)));
			if(invId.equalsIgnoreCase(invoiceId) ){
				totalAmountInvoiced = totalAmountInvoiced.add(amountInvoiced);
			}
		}
		
		if(invHashSet.add(invId)){
		%>

	<%
		if(counterRow%2==0){
	%>
		<TR class="browserEvenRow">
	<%
	}else{
	%>
		<TR class="browserOddRow">
	<%
	}
	counterRow++;
	%>
	
	<!-- Modified for Bug#13367 and INF-22370 Enhancement : Raviesh -->
	<%if(invNumber.length()>50){%>
	<td class="tdDefault" align="center"><A href="#" onClick="return printWindow('<%=invId %>')" onClick="" HREF="#"><%=invNumber.substring(0,50)%>
	&nbsp;<IMG width="16" height="16" src ='./images/More.png' class='asIsImage' border=0 onMouseOver="return overlib('<%=invNumber%>',CAPTION,'<%=LC.L_Inv_Number%><%--Invoice Number*****--%>');" onMouseOut="return nd();"/>
	</A>
	</td><%}else{%><td class="tdDefault" align="center"><A href="#" onClick="return printWindow('<%=invId %>')" onClick="" HREF="#"><%=invNumber%></A>
	</td><%}%>
	<td align="center">
	<%if (subType.equals("F")){
	}else {%>
	&nbsp;&nbsp;&nbsp;<A href="#" onClick="return viewInvoice('<%=strCovTypes.replaceAll("'","") %>','<%=invId%>','<%=StringUtil.encodeString(invNumber)%>','E',<%=pageRight%>)" onClick="" HREF="#"><img style="margin-right: 5px; position: relative "src="./images/edit.gif" title="<%=LC.L_Edit%>" border="0"/></A>   <!--Akshi:Modified for Bug#8607-->
	<%}%>

	</td>
	<td class="tdDefault" align="center"><%=invDate%></td>
	<td class="tdDefault" align="center"><span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=totalAmountInvoiced%></span></td>
	<td class="tdDefault" align="center"><%=paymentDueDate %></td>
	<td class="tdDefault custom-table-td" align="center"><%=invCreatedBy %> <img src="./images/View.gif" onmouseover="return overlib(htmlEncode('<%=invUserData%>'),ABOVE,CAPTION,'');" onmouseout="return nd();" border="0" align="left" /></td>
	<td class="tdDefault" align="center"><%=invRange %></td>

	<!--JM: #FIN11, Feb2008 enhancements-->
	<td class="tdDefault" >
	<!--KM:Modified-->
	<A href="#" onclick="openWinStatus(<%=pageRight%>,'M','<%=StringUtil.encodeString(studyNumber)%>','<%=invId%>','<%=StringUtil.encodeString(invNumber)%>','<%=historyId%>')"><%= invStat%></A>&nbsp;&nbsp;
		
	<%
	if (StringUtil.isEmpty(calledFrom))
 	{  %>
	<A href="#" onclick="openWinStatus(<%=pageRight%>,'N','<%=StringUtil.encodeString(studyNumber)%>','<%=invId%>', '<%=StringUtil.encodeString(invNumber)%>','0')"><img src="./images/edit.gif" title="<%=LC.L_Edit%>" border="0"/></A>&nbsp;&nbsp;
	<A href="showinvoicehistory.jsp?modulePk=<%=invId%>&pageRight=<%=pageRight%>&srcmenu=<%=src%>&selectedTab=7&from=invhistory&fromjsp=showinvoicehistory.jsp&invNumber=<%=invNumber%>&currentStatId=<%=historyId%>&studyId=<%=studyId%>"><img border="0" title="<%=LC.L_History%>" alt="<%=LC.L_History%>" src="./images/History.gif" ><%//=LC.L_H%><%--H*****--%></A></td>
	<% } else { %>
	&nbsp;
	<% }  %>
	</td>

	</td>
	<td class="tdDefault" ><%=invNotes %></td>
	
	<!-- Added by Manimarn for #FIN12 Enhancement -->
	
	<td align="center">
	
	
	<%
	if (StringUtil.isEmpty(calledFrom))
 	{  %><input type="checkbox" name="InvOpt<%=counter%>" id="InvOpt<%=counter%>" class="InvOpt" value="<%=invId+"~~"+invStr%>"/>
 		 <!--<A href="deleteInvoice.jsp?studyId=<%//=studyId%>&invoiceId=<%//=invId%>&selectedTab=<%//=selectedTab%>&srcmenu=<%//=src%>" 
		onClick="return confirmBox('<%//=invNumberSubStr%>',<%//=pageRight%>,'E')"><img src="./images/delete.gif" title="<%//=LC.L_Delete%>" border="0"/>--> &nbsp;&nbsp; 
	<% } else { %>
		<a href="linkInvDetails.jsp?invPk=<%=invId %>&studyId=<%=studyId %>&paymentPk=<%=paymentPk%>&pR=<%=pageRight%>" ><%=LC.L_Select%><%--Select*****--%></a>
	<% }  %>
	</td>
	</tr>




	<%

	}	
	}

	 %>

<div id="dialogDelete" title="<%=LC.L_Del_Invoice+"(s)"%>" style="display:none; top: 0px;">
<div id="formdiv" style="display:block">

<p><div class = "successfulmsg"><%=MC.M_DelTheFollowingInvcs%>&nbsp;</div></p>
<div><p class="sectionHeadings" align="left" id="InvSlctdData" ></p></div>
<p><span id="eSignMessage" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"></span></p>

 <%if ("userpxd".equals(Configuration.eSignConf)) {%>
 <p style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;">e-Password<FONT class="Mandatory">*</FONT>&nbsp
	<input type="password" name="eSign" id="eSign"  autocomplete="off"
	 onkeyup="$j('#eSignMessage').show();ajaxvalidate('pass:'+this.id,-1,'eSignMessage','Valid-Password','Invalid-Password','sessUserId')" onkeypress="if (fnOnceEnterKeyPress(event)>0) { return false; } " style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"/>&nbsp;</p>
	 <script type="text/javascript">
	 L_Invalid_Esign='Invalid-Password';
	 </script>
 <%} else{%>
 <p style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"><%=LC.L_Esignature/*e-Signature*****/%><FONT class="Mandatory">*</FONT>&nbsp
	<input type="password" name="eSign" id="eSign" maxlength="8" autocomplete="off"
	 onkeyup="$j('#eSignMessage').show();ajaxvalidate('misc:'+this.id,4,'eSignMessage','<%=LC.L_Valid_Esign/*Valid e-Sign*****/%>','<%=LC.L_Invalid_Esign/*Invalid e-Sign*****/%>','sessUserId')" onkeypress="if (fnOnceEnterKeyPress(event)>0) { return false; } " style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"/>&nbsp;</p>
 <%} %>
<%if(pageRight >=6)
		{ %>
<p align="right"><button id="submit_btn" onclick="return fnClickOnce(event); " ondblclick="return false;"><%=LC.L_Save%></button>&nbsp;&nbsp;&nbsp;<button id="closeBttn" onclick="$j('#eSign').removeClass('validation-failed');$j('#dialogDelete').dialog('close');return false;" ><%=LC.L_Cancel%></button></p>
<%}
else {%>
<p align="right"><button id="closeBttn" onclick="$j('#eSign').removeClass('validation-failed');$j('#dialogDelete').dialog('close');return false;" ><%=LC.L_Cancel%></button></p>
<%} %>


</div>

<div id='progressMsg' style="display:none;"><p class="sectionHeadings" align="center"> <%=LC.L_PleaseWait_Dots %><%-- Please wait...*****--%> </p></div>
<div id='successMsg' style="display:none;"><p class="sectionHeadings" align="center"> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p></div>

</div>

<div id="dialogNotDelete" title="<%=LC.L_Del_Invoice%>" style="display:none; top: 0px;">
<div><p class="sectionHeadings" align="left" id="InvNotDeletedData" ></p></div>
<p align="center"><button id="closeBttnAftr" onclick="$j('#dialogNotDelete').dialog('close');return false;"><%=LC.L_Close%></button></p>
</div>
</table>

    <!-- Pagination implemented for Bug#13372 : Raviesh-->
	<table width="98%" cellspacing="0" cellpadding="0" class="midalign">
	<tr>
	<td>
	<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
		<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
	<%} else {%>
		<font class="recNumber"><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></font>	
	<%}%>	
	</td>
	</tr>
	</table>
	
	<table align=center>
	<tr>
	<td>
<%
	if (curPage==1) startPage=1;

for (int count = 1; count <= showPages;count++)
{
cntr = (startPage - 1) + count;
 
if ((count == 1) && (hasPrevious))
{%>
<%
    if (calledFrom.equals("p")){
 %>
<A href="linkInvPayment.jsp?srcmenu=tdmenubaritem7&selectedTab=7&studyId=<%=studyId%>&paymentPk=<%=paymentPk%>&pR=<%=pageRight%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&page=<%=cntr-1%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;

<%} else {%>

<A href="invoicebrowser.jsp?srcmenu=tdmenubaritem7&selectedTab=7&studyId=<%=studyId%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&page=<%=cntr-1%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;

<%
	}
}	    
%>

<%

 if (curPage  == cntr)
 {
 %>	   
 
 
	<FONT class = "pageNumber"><%= cntr %></Font>
   <%
   }
  else
    {if (calledFrom.equals("p")){
  	    	%>
  	<A href="linkInvPayment.jsp?srcmenu=tdmenubaritem7&selectedTab=7&studyId=<%=studyId%>&paymentPk=<%=paymentPk%>&pR=<%=pageRight%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&page=<%=cntr%>"><%= cntr%></A>
  	
  		<%} else {%>
  			
	<A href="invoicebrowser.jsp?srcmenu=tdmenubaritem7&selectedTab=7&studyId=<%=studyId%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&page=<%=cntr%>"><%= cntr%></A>
  
  		<%}
    	}	
	 %>
	<%
	  }
%>
	
<%if (hasMore)
	{  
	if (calledFrom.equals("p")){%>
&nbsp;&nbsp;&nbsp;&nbsp;<A href="linkInvPayment.jsp?srcmenu=tdmenubaritem7&selectedTab=7&studyId=<%=studyId%>&paymentPk=<%=paymentPk%>&pR=<%=pageRight%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&page=<%=cntr+1%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%> ></A>
	<%}else{ %>  
&nbsp;&nbsp;&nbsp;&nbsp;<A href="invoicebrowser.jsp?srcmenu=tdmenubaritem7&selectedTab=7&studyId=<%=studyId%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&page=<%=cntr+1%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%> ></A>

<%
		}
	}
	
%>
<!-- Pagination implemented for Bug#13372 : Raviesh-->
</td>
</tr>
</table>
</Form>