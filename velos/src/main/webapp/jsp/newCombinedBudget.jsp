<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>

</head>
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
<SCRIPT Language="javascript">

function setCountCal(formobj){
	var indx = formobj.calStatus.options.selectedIndex;
	var calStat = formobj.calStatus.options[indx].value;
	var studyId = formobj.studyId.value;

	new VELOS.ajaxObject("getStudySetupCalendars.jsp", {
   		urlData:"studyId="+studyId+"&calStat="+calStat ,
		   reqType:"POST",
		   outputElement: "countCalDiv" }
	).startRequest();

	document.getElementById('countCalDiv').style.display="block";
}

function  validate(formobj){
	if(formobj.bgtAllCal.checked==false){
		//RA Fixed Bug No :4889
		alert("<%=MC.M_SelCreateBgt_FromCal%>");/*alert("Please select 'Create Budget from All Calendars with Calendar Status'.");*****/
		return false;
	}

	//if (!(validate_col('Currency',formobj.currencyId))) return false

	if (!(validate_col('Budget Template',formobj.budgetTemplate))) return false

	if (document.getElementById('calCount').value ==0){
		/*var yesCreate = confirm("There are no '<%=LC.Std_Study%> Setup calendars' satisfying the criteria.\nA default Combined budget will be created.\n\nDo you want to continue?");*****/
		var yesCreate = confirm("<%=MC.M_NoStdCal_DefBgtCrtToCont%>");

		if (yesCreate)
			return true;
		else
			return false;
	}

	if (!(validate_col('e-Signature',formobj.eSign))) return false

	/* if(isNaN(formobj.eSign.value) == true) {
		alert*/<%--("<%=MC.M_IncorrEsign_EtrAgain%>");--%>
		/*alert("Incorrect e-Signature. Please enter again");
		formobj.eSign.focus();
		return false;
	}*/
}


</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="catLibJB" scope="request"  class="com.velos.eres.web.catLib.CatLibJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="budgetB" scope="page" class="com.velos.esch.web.budget.BudgetJB"/>
<jsp:useBean id="budget" scope="page" class="com.velos.esch.web.budget.BudgetJB"/>
<jsp:useBean id="bgtcalB" scope="page" class="com.velos.esch.web.budgetcal.BudgetcalJB"/>
<jsp:useBean id="bgtcaldao" scope="request" class="com.velos.esch.business.common.BudgetcalDao"/>
<jsp:useBean id="schcodedao" scope="request" class="com.velos.esch.business.common.SchCodeDao"/>

<%@ page import="com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.business.common.*"%>
<body>
<Form name="budgetForm" id="budgetForm" method="post" action="createCombinedBgt.jsp" onSubmit="if (validate(document.budgetForm)==false) {setValidateFlag('false');return false;} else {setValidateFlag('true'); return true;}">
<%
	String calledFrom="";
	String mode="";
	String durationUnit="";
	String calStatus = "";

	HttpSession tSession = request.getSession(true);
	String userIdFromSession = (String) tSession.getValue("userId");
	//String accId= (String) tSession.getValue("accountId");
	String studyId = (String) tSession.getValue("studyId");

	BudgetDao bgtdao = new BudgetDao();
	boolean val = bgtdao.getBgtTemplatesForCombinedBgt(EJBUtil.stringToNum(userIdFromSession),"budget_template");

	ArrayList arrFlgSeparatedIds = bgtdao.getBgtIdWithFlgs();

	ArrayList arrTemplateDesc = bgtdao.getBgtDescs();

	//SchCodeDao cd = new SchCodeDao();
    //cd.getCodeValues("currency", EJBUtil.stringToNum(accId));

    //int currId = 0;
    //currId= cd.getCodePKByDesc("currency","US Dollars","$" );

    //String currencyDD = "";
    //currencyDD = cd.toPullDown("currencyId",currId);

	String templatePullDown =  "";
	templatePullDown =  EJBUtil.createPullDownWithStr("budgetTemplate","", arrFlgSeparatedIds,arrTemplateDesc);

	int userId = EJBUtil.stringToNum(userIdFromSession);
	userB.setUserId(userId);
	userB.getUserDetails();

	String orgId = "";
	orgId = userB.getUserSiteId();
	orgId=(orgId==null)?"":orgId;
%>
<BR>
<input name="from" type="hidden" value="combBGT"/>
<input name="studyId" type="hidden" value="<%=studyId%>"/>
<input name="orgId" type="hidden" value="<%=orgId%>"/>
<input name="orgFlag" type="hidden" value="1"/>
<input name="mode" type="hidden" value="N"/>

<table align="center">
	<tr>
		<td ><b><%=MC.M_NoStdLvlComb_PatBgtChk%><%-- There is no <%=LC.Std_Study%> level Combined <%=LC.Pat_Patient%> Budget defined. Please Check the following checkbox and Submit to create a combined <%=LC.Pat_Patient%> budget.*****--%> </b></td>
	</tr>
	<tr><td><BR></td></tr>
	<tr>
		<td >
			<table>
				<tr>
					<td >
						<input name="bgtAllCal" id="bgtAllCal" type="checkbox"/>&nbsp;&nbsp; <%=MC.M_CreatBgtFrom_CalWthStat%><%-- Create Budget from All Calendars with Calendar Status*****--%> <FONT class="Mandatory">* </FONT>
				  		&nbsp;&nbsp; 
				  		
				  		<!-- 
				  		<select  name="calStatus" id="calstatus" onChange="document.getElementById('countCalDiv').style.display='none'; setCountCal(document.budgetForm);">
							<option value=W>Work in Progress</option>
							<option value=A selected>Active</option>
					  	</select>				  		
				  		 
				  		 -->	
					  	<%
					  	
					  	//JM: 27JAN2011: Enh-D-FIN9
					  	String dCalStat = "";
					  	SchCodeDao calStatDao = new SchCodeDao();
					  	
					  	calStatDao.getCodeValues("calStatStd");
					  	
					  	StringBuffer sbCalStat = new StringBuffer();
					  	int cnt = 0;					  	
					  	
					  	sbCalStat.append("<select  name=\"calStatus\" id=\"calstatus\" onChange=\"document.getElementById('countCalDiv').style.display='none'; setCountCal(document.budgetForm);\">");
					  	
						
						for (cnt = 0; cnt <= calStatDao.getCDesc().size() -1 ; cnt++)
						{							
							if (!(calStatDao.getCSubType().get(cnt)).equals("D") && !(calStatDao.getCSubType().get(cnt)).equals("O")){								
								sbCalStat.append("<OPTION value="+ calStatDao.getCSubType().get(cnt)+">" + calStatDao.getCDesc().get(cnt)+ "</OPTION>");
							}
						}
				
						sbCalStat.append("</SELECT>");
						dCalStat = sbCalStat.toString();

					  	
					  	
					  	%><%=dCalStat%>
					  	
					  
					  	
					  	
					</td>
					<td ><div id="countCalDiv" name="countCalDiv" border=1 style="display:block"></div></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td><BR></td></tr>
	<tr>
		<td >
			&nbsp;&nbsp; <%=LC.L_Bgt_Template%><%-- Budget Template*****--%> <FONT class="Mandatory">* </FONT>
	  		&nbsp;&nbsp; <%=templatePullDown%>
		</td>
	</tr>
</table>
<BR>
<jsp:include page="submitBar.jsp" flush="true">
	<jsp:param name="displayESign" value="Y"/>
	<jsp:param name="formID" value="budgetForm"/>
	<jsp:param name="showDiscard" value="N"/>
	<jsp:param name="noBR" value="Y"/>
</jsp:include>

<input type="hidden" name="mode" value="<%=mode %>">
<input type="hidden" name="calStat" value="">
</Form>
<script>
setCountCal(document.budgetForm);
</script>