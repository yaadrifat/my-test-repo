<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Panel%><%--Panel*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
 
<Link Rel=STYLESHEET HREF="commonNS.css" type=text/css>
<SCRIPT LANGUAGE="JavaScript" SRC="homeresolution.js"></SCRIPT>

</head>

<SCRIPT>
	function openwin() {
      window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
;}

</SCRIPT>  
<SCRIPT>
function opensearchwin() {
	var keyword = document.form1.keyword.value ;
      window.open("../jsp/studyKeywordSearch.jsp?keyword=" + keyword + "&src=xyz","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
;}
</SCRIPT> 


<body onload="load()" id="bd" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">

<form name="panel" METHOD="POST">
<% String srcMenu;
srcMenu= request.getParameter("src");
if (srcMenu == null){
 srcMenu = "";
}

out.print("<Input type=\"hidden\" name=\"src\" value=\"" +srcMenu +"\">");
%>

</form>

<table class=tableDefault width=546px cellspacing=0 cellpadding=0 border="0">
  <tr> 
    <td class=tdDefault width=52px><img name=topLeftCurve src="../images/jpg/top_left_curve.jpg" width=52px 	height=32px></td>

    <td class=tdDefault valign=TOP><img name="topline" src="../images/jpg/topline.jpg" width=494px height=5px align="top"></td>
  </tr>
</table>

<table class=leftPanelVLine width=22px cellpadding=0 cellspacing=0 border=0>
  <tr> 
    <td width=21px>&nbsp;</td>
    <td class=tdDefault width=1px ><img name=vertline src="../images/jpg/vline.jpg" width=2 height=350></td>
  </tr>
</table>


<table class=tableDefault width=22px cellpadding=0 cellspacing=0 border=0>
  <tr> 
    <td width=22px><img name=bottomcurve src="../images/jpg/test.jpg">
    </td>
  </tr>
</table>

<DIV CLASS=topRightPanel id=topRightDiv> 
 <form name="form1" onsubmit="opensearchwin()" METHOD="POST">
  <table width=260px cellpadding=0 cellspacing=0 border=0>
    <tr> 
      <td id=tdDefault width=49px ROWSPAN = 4><img src="../images/jpg/top_right_curve.jpg" width=49px height=89px></td>
	<td width=210px><A href="aboutus.jsp" target="Information" onClick="openwin()"><%=LC.L_About_Us%><%--About us*****--%></a>&nbsp;|&nbsp;<A href="underConstruction.jsp" target="Information" onClick="openwin()"><%=LC.L_Press%><%--Press*****--%></a>&nbsp;|&nbsp;<A href="underConstruction.jsp" target="Information" onClick="openwin()"><%=LC.L_Contact_Us%><%--Contact us*****--%></a>&nbsp;|&nbsp;<A href="underConstruction.jsp" target="Information" onClick="openwin()"><%=LC.L_Faqs%><%--FAQs*****--%></a></td>
    </tr>
    <tr> 
      <td id=tdDefault width=203px> <b><%=LC.L_Search%><%--Search*****--%></b> 
      </td>
    </tr>
    <tr>
	<td id=tdDefault width=203px height=34px>
		<input type="text" name="keyword" size = 10 MAXLENGTH = 20>
	         &nbsp;&nbsp;<input type="submit" value="<%=LC.L_Go%><%--Go*****--%>">
      </td>
    </tr>
    <tr >
      <td  width=203px height=6px> 
          <p><img src="../images/jpg/topline.jpg" width=245px height=7px vertical-align=bottom></p>
      </td>
    </tr>
  </table>
 </form>
</DIV>


<DIV  class="logo" ID="vlogo1" > 
<table width=200px border=0>
  <tr>
      <td><A href="http://www.velos.com"> <img src="../images/jpg/vlogo.jpg" width=189px height=78px border=0> </A></td>
  </tr>
</table>

</DIV>

</body>
</html>