package com.velos.webservices;

import java.util.ArrayList;
import java.util.HashMap;

import javax.jws.WebService;

import org.apache.log4j.Logger;

import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.PatientScheduleClient;
import com.velos.services.client.WorkflowClient;
import com.velos.services.model.EventStatusHistory;
import com.velos.services.model.StudyIdentifier;

/**
 * WebService class for Workflow Services
 * @author Elayaperumal
 *
 */

@WebService(
		serviceName="WorkflowService",
		endpointInterface="com.velos.webservices.WorkflowSEI",
		targetNamespace="http://velos.com/services/")

public class WorkflowWS implements WorkflowSEI {
	
	private static Logger logger = Logger.getLogger(WorkflowWS.class.getName());
	
	public WorkflowWS(){
	}

	public ResponseHolder calculateWorkFlow(
			StudyIdentifier studyIdentifier) throws OperationException {
		ResponseHolder response = new ResponseHolder();
		try {
			System.out.println("Inside calculateWorkFlow");
			response = WorkflowClient.calculateWorkFlow(studyIdentifier);  

		} catch (OperationException e) {
			logger.error("calculateWorkFlow", e);
			throw e;
		} catch (Throwable t) {
			logger.error("calculateWorkFlow", t);
			response.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return response;
	}

}
