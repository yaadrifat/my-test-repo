package com.velos.webservices;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.UserClient;
import com.velos.services.model.User;

@Path("/restServices/")
@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
public class UserRS {
	private static Logger logger = Logger.getLogger(CheckSubmissionWS.class.getName());
	@POST
	@Path("updateUser")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public ResponseHolder updateUserDetails(User user)throws OperationException{
		
		ResponseHolder response = new ResponseHolder();
		
		try {
			response = UserClient.updateUserDetails(user);

		} catch (OperationException e) {
			logger.error("getStudyCheckAndSubmitStatusResponse", e);
		} 
		return response;
	}
}
