package com.velos.webservices;



import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.Versions;

@Produces("application/json")
@Consumes
@WebService(targetNamespace="http://velos.com/services/")
public interface VersionSEI {
	
	@GET
	@Path("/getVersions/")
	@WebResult(name="Versions")
	public Versions getStudyVersions(
			@WebParam(name="StudyIdentifier")StudyIdentifier studyIdent,
			@WebParam(name="CurrentStatus")boolean currentStatus)
	throws OperationException;
	
	@POST
	@Path("/createVersions/")
	@WebResult(name="Response")
	public ResponseHolder createVersions(
			@WebParam(name="Versions")
			Versions versions
			)
	throws OperationException;

}
