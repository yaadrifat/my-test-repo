/**
 * @author Tarandeep Singh Bali
 */
package com.velos.webservices;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.velos.services.OperationException;
import com.velos.services.model.DateFieldValidations;
import com.velos.services.model.EventSearch;
import com.velos.services.model.Events;
import com.velos.services.model.FormDesign;
import com.velos.services.model.FormIdentifier;
import com.velos.services.model.LibraryEvents;
import com.velos.services.model.NumberFieldValidations;
import com.velos.services.model.TextFieldValidations;


@WebService(
		 targetNamespace="http://velos.com/services/")
@XmlSeeAlso({TextFieldValidations.class,
	DateFieldValidations.class,
	NumberFieldValidations.class
	})
public interface LibrarySEI {
	
	@POST
	@Path("/searchlibraryevent/")
	@WebResult(name="Library")
	public LibraryEvents searchLibraryEvents(
			@WebParam(name="EventSearch")
			EventSearch eventSearch,
			@WebParam(name="pageNumber")
	        Integer PageNumber,
	        @WebParam(name="pageSize")
	        Integer PageSize)
	throws OperationException; 

}
