/**
 * 
 */
package com.velos.webservices;

import java.util.Date;
import java.util.Set;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.LogicalHandler;
import javax.xml.ws.handler.LogicalMessageContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.user.impl.UserBean;
import com.velos.services.OperationException;
import com.velos.services.monitoring.MessageLog;
import com.velos.services.monitoring.MessageService;
import com.velos.services.monitoring.SessionLog;
import com.velos.services.monitoring.SessionService;
import com.velos.services.util.JNDINames;

/**
 * This jax-ws handler handles logging message activity to svc_session and svc_message tables. It
 * must be added to the jax-ws handler chain. In CXF, this can be done the beans.xml file.
 * 
 * @author dylan
 *
 */
public class LoggingMessageHandler implements LogicalHandler<LogicalMessageContext>{
	private static Logger logger = Logger.getLogger(LoggingMessageHandler.class.getName());

	private SessionService sessionService; 
	
	private MessageService messageService;
	
	private SessionLog sessionLog;
	
	private MessageLog messageLog;
	
	private String EmptyString = "";
	
	public LoggingMessageHandler(){
		
	}
	
	/* (non-Javadoc)
	 * @see javax.xml.ws.handler.soap.SOAPHandler#getHeaders()
	 */
	public Set<QName> getHeaders() {
		// TODO Auto-generated method stub
		return null;
	}



	/* (non-Javadoc)
	 * @see javax.xml.ws.handler.Handler#handleFault(javax.xml.ws.handler.MessageContext)
	 */
	public boolean handleFault(LogicalMessageContext context) {

		String errStr = (String)context.get(Constants.ERROR_STRING_KEY);
		messageLog.setErrorString(errStr);
		messageLog.setSuccessFlag(false);
		logExit(context);
		return true; 
	}

	/**
	 * Logs messageLog and sessionLog information. 
	 * Currently does not log user identification information.
	 * @see javax.xml.ws.handler.Handler#handleMessage(javax.xml.ws.handler.MessageContext)
	 */
	public boolean handleMessage(LogicalMessageContext context) {
		//This method gets called twice per message. Once before invoking the 
		//web service operation (and before authenticating) and the second
		//time on the way back out.
		//We set SessionLog and MessageLog parameters in both directions.
		try{
		
			Boolean messageOut = (Boolean)context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
			
			if (!messageOut){//receiving a message, log the initial info about it
			
				HttpServletRequest request =
					(HttpServletRequest)context.get(MessageContext.SERVLET_REQUEST);
				Date now = new Date();
				//log the user session
				sessionLog = 
					new SessionLog();

				sessionLog.setLoginTime(now);
				sessionLog.setRemoteAddress(request.getRemoteAddr());
				
				try {
					sessionService = getSessionService();
				} catch (OperationException e) {
					logger.error("error gettting sessionlog service", e);
					return false;
				}
				//persist a new sessionlog
				sessionLog = sessionService.setSessionDetails(sessionLog);
		
				QName operation = (QName)context.get(MessageContext.WSDL_OPERATION);
				QName service = (QName)context.get(MessageContext.WSDL_SERVICE);
				
				messageLog = new MessageLog();
		
				messageLog.setEndpoint(service.toString());

				messageLog.setModule(getClass().getName());
				messageLog.setOperation(operation == null ? EmptyString : operation.toString());
				messageLog.setRoute(MessageLog.ROUTE_INCOMING);
				messageLog.setTime(now); 
				messageLog.setSessionId(sessionLog.getId());
		
				try{
					messageService = getMessageService();
				} catch (OperationException e) {
					logger.error("error gettting message log service", e);
					return false;
				}
				
				//persist a new messagelog
				messageLog = messageService.setMessageDetails(messageLog);
				
			}
			else{
				messageLog.setSuccessFlag(true);
				logExit(context);
			}
		}
		catch(Throwable t){
			logger.error("error creating user session information " 
					 + " " + t); 
			return false;
		}
		return true;
	}

	private MessageService getMessageService() throws OperationException{
		if (messageService == null){
			InitialContext ic;
			try{
				ic = new InitialContext();
				messageService =
					(MessageService) ic.lookup(JNDINames.MessageServiceImpl);
				 
			}
			catch(NamingException e){
				throw new OperationException(e);
			}
		}
		return messageService;
	}

	private SessionService getSessionService() throws OperationException{
		if (sessionService == null){
			InitialContext ic;
			try{
				ic = new InitialContext();
				sessionService =
					(SessionService) ic.lookup(JNDINames.SessionServiceImpl);
				 
			}
			catch(NamingException e){
				throw new OperationException(e);
			}
		}
		return sessionService;
	}

	
	private void logExit(MessageContext context){
		
		long duration = (new Date()).getTime() - messageLog.getTime().getTime();
		messageLog.setDuration(duration);
		try{
			//The first pass into this handler happends BEFORE authentication,
			//so we can't set user identity until the second pass.
			UserBean userBean = (UserBean)context.get(Constants.USER_BEAN_KEY);
			if (userBean == null){
				logger.error("error updating message with user info" + " :" + messageLog.toString());
			}
			else{
				sessionLog.setUserIdent(userBean.getUserLoginName());
				messageLog.setIdentity(userBean.getUserId());
			}

			//persist duration into message
			MessageService messageService = getMessageService();
			messageService.updateMessage(messageLog);
			
			//persist close of session time into session...when we
			//support multiple messages per session, we will do this elsewhere
			sessionLog.setLogoutTime(new Date());
			sessionService.updateSessionDetails(sessionLog);
		}
		catch(Throwable t){
			//failure to persist the message log to the database...log to the logger
			logger.error("error updating message with duration" + " :" + messageLog.toString(), t); 	
		}
		


	}

	/* (non-Javadoc)
	 * @see javax.xml.ws.handler.Handler#close(javax.xml.ws.handler.MessageContext)
	 */
	public void close(MessageContext context) {
		//cxf does not appear to be ever calling this method
		
	}
	



}
