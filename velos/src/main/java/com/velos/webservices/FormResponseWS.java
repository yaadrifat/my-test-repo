/**
 * Created On Nov 9, 2011
 */
package com.velos.webservices;

import javax.jws.WebParam;
import javax.jws.WebService;

import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.FormResponseRemoteObj;
import com.velos.services.model.AccountFormRespns;
import com.velos.services.model.AccountFormResponse;
import com.velos.services.model.AccountFormResponseIdentifier;
import com.velos.services.model.AccountFormResponses;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.FieldNameValIdentifier;
import com.velos.services.model.FormDisplayTypeIdentifier;
import com.velos.services.model.FormIdentifier;
import com.velos.services.model.FormRespns;
import com.velos.services.model.FormResponse;
import com.velos.services.model.PatientFormRespns;
import com.velos.services.model.PatientFormResponse;
import com.velos.services.model.PatientFormResponseIdentifier;
import com.velos.services.model.PatientFormResponses;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.StudyFormRespns;
import com.velos.services.model.StudyFormResponse;
import com.velos.services.model.StudyFormResponseIdentifier;
import com.velos.services.model.StudyFormResponses;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatientFormResponse;
import com.velos.services.model.StudyPatientFormResponseIdentifier;
import com.velos.services.model.StudyPatientFormResponses;
import com.velos.services.model.StudyPatientScheduleFormRespns;
import com.velos.services.model.StudyPatientScheduleFormResponse;
import com.velos.services.model.VisitIdentifier;

/**
 * @author Kanwaldeep
 *
 */
@WebService(
		serviceName="FormResponseService",
		endpointInterface="com.velos.webservices.FormResponseSEI",
		targetNamespace="http://velos.com/services/")
public class FormResponseWS implements FormResponseSEI {

	/* (non-Javadoc)
	 * @see com.velos.webservices.FormResponseSEI#createStudyFormResponse(com.velos.services.model.StudyFormResponse)
	 */
	public ResponseHolder createStudyFormResponse(
			StudyFormResponse studyFormResponse)
			throws OperationRolledBackException, OperationException {		
		return FormResponseRemoteObj.createStudyFormResponse(studyFormResponse);
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.FormResponseSEI#removeStudyFormResponse(com.velos.services.model.StudyFormResponseIdentifier)
	 */
	public ResponseHolder removeStudyFormResponse(
			StudyFormResponseIdentifier formResponseIdentifier)
			throws OperationRolledBackException, OperationException {
		return FormResponseRemoteObj.removeStudyFormResponse(formResponseIdentifier);
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.FormResponseSEI#getStudyFormResponse(com.velos.services.model.StudyFormResponseIdentifier)
	 */
	public StudyFormResponse getStudyFormResponse(
			StudyFormResponseIdentifier formResponseIdentifier)
			throws OperationException {
		return FormResponseRemoteObj.getStudyFormResponse(formResponseIdentifier);
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.FormResponseSEI#createStudyPatientFormResponse(com.velos.services.model.StudyPatientFormResponse)
	 */
	public ResponseHolder createStudyPatientFormResponse(
			StudyPatientFormResponse studyPatientFormResponse)
			throws OperationException {
		// TODO Auto-generated method stub
		return FormResponseRemoteObj.createStudyPatientFormResponse(studyPatientFormResponse);
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.FormResponseSEI#getStudyPatientFormResponse(com.velos.services.model.PatientFormResponseIdentifier)
	 */
	public StudyPatientFormResponse getStudyPatientFormResponse(
			StudyPatientFormResponseIdentifier formResponseIdentifier)
			throws OperationException {
		return FormResponseRemoteObj.getStudyPatientFormResponse(formResponseIdentifier, false);
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.FormResponseSEI#removeStudyPatientFormResponse(com.velos.services.model.PatientFormResponseIdentifier)
	 */
	public ResponseHolder removeStudyPatientFormResponse(
			StudyPatientFormResponseIdentifier formResponseIdentifier)
			throws OperationException {
		return FormResponseRemoteObj.removeStudyPatientFormResponse(formResponseIdentifier);
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.FormResponseSEI#createAccountFormResponse(com.velos.services.model.AccountFormResponse)
	 */
	public ResponseHolder createAccountFormResponse(
			AccountFormResponse accountFormResponse,
			FormDisplayTypeIdentifier formDisplayTypeIdentifier) throws OperationException {
		// TODO Auto-generated method stub
		return FormResponseRemoteObj.createAccountFormResponse(accountFormResponse,formDisplayTypeIdentifier);
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.FormResponseSEI#removeAccountFormResponse(com.velos.services.model.AccountFormResponseIdentifier)
	 */
	public ResponseHolder removeAccountFormResponse(
			AccountFormResponseIdentifier formResponseIdentifier)
			throws OperationException {
		// TODO Auto-generated method stub
		return FormResponseRemoteObj.removeAccountFormResponse(formResponseIdentifier);
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.FormResponseSEI#getAccountFormResponse(com.velos.services.model.AccountFormResponseIdentifier)
	 */
	public AccountFormResponse getAccountFormResponse(
			AccountFormResponseIdentifier formResponseIdentifier)
			throws OperationException {
		// TODO Auto-generated method stub
		return FormResponseRemoteObj.getAccountFormResponse(formResponseIdentifier);
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.FormResponseSEI#createPatientFormResponse(com.velos.services.model.PatientFormResponse)
	 */
/*	public ResponseHolder createPatientFormResponse(
			PatientFormResponse patientFormResponse) throws OperationException {
		// TODO Auto-generated method stub
		return null;
	}
*/
	public ResponseHolder createPatientFormResponse(
			PatientFormResponse patientFormResponse) throws OperationException {
		// TODO Auto-generated method stub
		return FormResponseRemoteObj.createPatientFormResponse(patientFormResponse);
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.FormResponseSEI#removePatientFormResponse(com.velos.services.model.PatientFormResponseIdentifier)
	 */
	public ResponseHolder removePatientFormResponse(
			PatientFormResponseIdentifier formResponseIdentifier)
			throws OperationException {
		// TODO Auto-generated method stub
		return FormResponseRemoteObj.removePatientFormResponse(formResponseIdentifier);
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.FormResponseSEI#getPatientFormResponse(com.velos.services.model.PatientFormResponseIdentifier)
	 */
	public PatientFormResponse getPatientFormResponse(
			PatientFormResponseIdentifier formResponseIdentifier)
			throws OperationException {
		// TODO Auto-generated method stub
		return FormResponseRemoteObj.getPatientFormResponse(formResponseIdentifier);
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.FormResponseSEI#updatePatientFormResponse(com.velos.services.model.PatientFormResponseIdentifier, com.velos.services.model.PatientFormResponse)
	 */
	public ResponseHolder updatePatientFormResponse(
			PatientFormResponseIdentifier formResponseIdentifier,
			PatientFormRespns patientFormResponse) throws OperationException {
		// TODO Auto-generated method stub
		return FormResponseRemoteObj.updatePatientFormResponse(formResponseIdentifier, patientFormResponse);
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.FormResponseSEI#updateStudyPatientFormResponse(com.velos.services.model.PatientFormResponseIdentifier, com.velos.services.model.StudyPatientFormResponse)
	 */
	public ResponseHolder updateStudyPatientFormResponse(
			StudyPatientFormResponseIdentifier formResponseIdentifier,
			FormRespns studyPatientFormResponse)
			throws OperationException {
		return FormResponseRemoteObj.updateStudyPatientFormResponse(formResponseIdentifier, studyPatientFormResponse);
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.FormResponseSEI#updateStudyFormResponse(com.velos.services.model.StudyFormResponseIdentifier, com.velos.services.model.StudyFormResponse)
	 */
	
	//Kavitha : #30863
	public ResponseHolder updateStudyFormResponse(
			StudyFormResponseIdentifier formResponseIdentifier,
			StudyFormResponse studyFormResponse) throws OperationException {
		return FormResponseRemoteObj.updateStudyFormResponse(formResponseIdentifier, studyFormResponse);
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.FormResponseSEI#updateAccountFormResponse(com.velos.services.model.AccountFormResponseIdentifier, com.velos.services.model.AccountFormResponse)
	 */
	public ResponseHolder updateAccountFormResponse(
			AccountFormResponseIdentifier formResponseIdentifier,
			AccountFormRespns accountFormResponse,
			FormDisplayTypeIdentifier formDisplayTypeIdentifier) throws OperationException {
		// TODO Auto-generated method stub
		return FormResponseRemoteObj.updateAccountFormResponse(formResponseIdentifier, accountFormResponse,formDisplayTypeIdentifier);
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.FormResponseSEI#createStudyPatientScheduleFormResponse(com.velos.services.model.StudyPatientScheduleFormResponse, com.velos.services.model.CalendarIdentifier, java.lang.String, com.velos.services.model.VisitIdentifier, java.lang.String, com.velos.services.model.EventIdentifier, java.lang.String)
	 */
	public ResponseHolder createStudyPatientScheduleFormResponse(
			StudyPatientScheduleFormResponse studyPatientScheduleFormResponse,
			CalendarIdentifier calendarIdentifier, String calendarName,
			VisitIdentifier visitIdentifier, String visitName,
			EventIdentifier eventIdentifier, String eventName)
			throws OperationException {
		
		return FormResponseRemoteObj.createStudyPatientScheduleFormResponse(studyPatientScheduleFormResponse, calendarIdentifier, calendarName, visitIdentifier, visitName, eventIdentifier, eventName); 
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.FormResponseSEI#getStudyPatientScheduleFormResponse(com.velos.services.model.StudyPatientFormResponseIdentifier)
	 */
	public StudyPatientScheduleFormResponse getStudyPatientScheduleFormResponse(
			StudyPatientFormResponseIdentifier studyPatientFormResponseIdentifier)
			throws OperationException {
		return FormResponseRemoteObj.getStudyPatientScheduleFormResponse(studyPatientFormResponseIdentifier);
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.FormResponseSEI#updateStudyPatientScheduleFormResponse(com.velos.services.model.StudyPatientFormResponseIdentifier, com.velos.services.model.StudyPatientScheduleFormResponse)
	 */
	public ResponseHolder updateStudyPatientScheduleFormResponse(
			StudyPatientFormResponseIdentifier formResponseIdentifier,
			StudyPatientScheduleFormRespns studyPatientScheduleFormResponse)
			throws OperationException{
		return FormResponseRemoteObj.updateStudyPatientScheduleFormResponse(formResponseIdentifier, studyPatientScheduleFormResponse);
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.FormResponseSEI#removeStudyPatientScheduleFormResponse(com.velos.services.model.StudyPatientFormResponseIdentifier)
	 */
	public ResponseHolder removeStudyPatientScheduleFormResponse(
			StudyPatientFormResponseIdentifier studyPatientFormResponseIdentifier)
			throws OperationException {
		return FormResponseRemoteObj.removeStudyPatientScheduleFormResponse(studyPatientFormResponseIdentifier);
	}

	@Override
	public StudyFormResponses getListOfStudyFormResponses(
			FormIdentifier formIdentifier,
			FieldNameValIdentifier fieldNameValIdentifier,
			StudyIdentifier studyIdentifier, 
			int pageNumber, int pageSize) throws OperationException {
		return FormResponseRemoteObj.getListOfStudyFormResponses(formIdentifier, fieldNameValIdentifier, studyIdentifier, pageNumber, pageSize);
	}

	@Override
	public StudyPatientFormResponses getListOfStudyPatientFormResponses(
			FormIdentifier formIdentifier, FieldNameValIdentifier fieldNameValIdentifier, PatientIdentifier patientIdentifier,
			StudyIdentifier studyIdentifier, 
			int pageNumber, int pageSize) throws OperationException {
		return FormResponseRemoteObj.getListOfStudyPatientFormResponses(formIdentifier, fieldNameValIdentifier, patientIdentifier, studyIdentifier, pageNumber, pageSize);
	}

	@Override
	public PatientFormResponses getListOfPatientFormResponses(
			FormIdentifier formIdentifier, FieldNameValIdentifier fieldNameValIdentifier, PatientIdentifier patientIdentifier,
			int pageNumber, int pageSize) throws OperationException {
		return FormResponseRemoteObj.getListOfPatientFormResponses(formIdentifier, fieldNameValIdentifier, patientIdentifier, pageNumber, pageSize);
	}
	
	@Override
	public AccountFormResponses getListOfAccountFormResponses(FormIdentifier formIdentifier,
			FormDisplayTypeIdentifier formDisplayTypeIdentifier, FieldNameValIdentifier fieldNameValIdentifier,
			int pageNumber, int pageSize) throws OperationException {
		return FormResponseRemoteObj.getListOfAccountFormResponses(formIdentifier, formDisplayTypeIdentifier, fieldNameValIdentifier, pageNumber, pageSize);
	}}
