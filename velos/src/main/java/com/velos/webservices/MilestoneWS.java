package com.velos.webservices;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.MilestoneClient;
import com.velos.services.model.Milestone;
import com.velos.services.model.MilestoneList;
import com.velos.services.model.StudyIdentifier;

@WebService(
		serviceName="MilestoneService",
		endpointInterface="com.velos.webservices.MilestoneSEI",
		targetNamespace="http://velos.com/services/")
public class MilestoneWS implements MilestoneSEI{
ResponseHolder response = new ResponseHolder();
	
	@Resource
	private WebServiceContext context;  
	
	public MilestoneWS(){
		
	}
	
	@Override
	public Milestone getStudyMilestones(StudyIdentifier studyIdent) throws OperationException{
		return MilestoneClient.getStudyMilestones(studyIdent);
	}
	
	@Override
	public ResponseHolder createMMilestones(MilestoneList milestones) throws OperationException{
		return MilestoneClient.createMMilestones(milestones);
	}

}
