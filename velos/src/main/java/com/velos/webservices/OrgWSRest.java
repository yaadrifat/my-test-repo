package com.velos.webservices;

import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.StudyClient;
import com.velos.services.client.UserClient;
import com.velos.services.model.OrganizationDetail;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/restServices/")
@Produces("application/xml")
public class OrgWSRest{
	
	@POST
	@Path("/createorganization")
	@Consumes("application/xml")
	public ResponseHolder createOrganization(OrganizationDetail org)throws OperationException{
		
		ResponseHolder response = new ResponseHolder();
		
		try {
			return UserClient.createOrganization(org);

		} catch (OperationException e) {
			response.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE,"Filled Manadatory Field"));
			//throw e;
		} catch (Throwable t) {
			response.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE,"Filled Manadatory Field"));
		}
		return response;
	}
	
	@POST
	@Path("/updateOrganization")
	@Consumes("application/xml")
	public ResponseHolder updateOrganisation(OrganizationDetail organizationDetail) throws OperationException {
		// TODO Auto-generated method stub
		return UserClient.updateOrganisation(organizationDetail);
	}
}
