package com.velos.webservices;


import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.VersionClient;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.Versions;

@WebService(
		serviceName="VersionService",
		endpointInterface="com.velos.webservices.VersionSEI",
		targetNamespace="http://velos.com/services/")
public class VersionWS implements VersionSEI{
ResponseHolder response = new ResponseHolder();
	
	@Resource
	private WebServiceContext context;  
	
	public VersionWS(){
		
	}
	
	@Override
	public Versions getStudyVersions(StudyIdentifier studyIdent,boolean currentStatus) throws OperationException{
		return VersionClient.getStudyVesrion(studyIdent,currentStatus);
	}
	
	@Override
	public ResponseHolder createVersions(Versions versions) throws OperationException{
		return VersionClient.createVersions(versions);
	}


}
