package com.velos.webservices;

import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.StatusIdentifier;
import com.velos.services.model.StudyStatus;
import com.velos.services.model.StudyStatusIdentifier;
import com.velos.services.model.StudyStatusParent;

@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@WebService(targetNamespace="http://velos.com/services/")
public interface StudyRestSEI {

		@POST
		@Path("/deleteStudyStatus")
		@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
		@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
		public ResponseHolder deleteStudyStatus(StatusIdentifier studyStatusIdentifier) throws OperationException;
		
		@POST
		@Path("/updateStudyStatus")
		@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
		@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
		public ResponseHolder updateStudyStatus(StudyStatusParent studyStatus) throws OperationException;
		
}
