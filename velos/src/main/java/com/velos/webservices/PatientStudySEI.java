package com.velos.webservices;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.velos.services.OperationException;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientProtocolIdentifier;
import com.velos.services.model.PatientStudy;
/**
 * Service Endpoint Interface dealing with generation of Operations
 *  of PatientStudies webservices
 * @author Virendra
 *
 */
@Produces("application/json")
@WebService(
		 targetNamespace="http://velos.com/services/")

	public interface PatientStudySEI {
		/**
		 * Service Endpoint Interface public method which
		 * calls getPatientStudies of webservices and returns
		 * List of PatientStudy
		 * @param patientId
		 * @return
		 * @throws OperationException
		 */
		@POST
		@Path("/getpatientstudies/")
		@WebResult(name = "PatientStudy" )
		public List<PatientStudy> getPatientStudies(
		@WebParam(name = "PatientIdentifier")		
		PatientIdentifier patientId)
				throws OperationException;
		
		/**
		 * Service Endpoint Interface public method which
		 * calls getPatientStudyInfo of webservices and returns
		 * PatientProtocolIdentifier
		 * @param patProtId
		 * @return PatientProtocolIdentifier
		 * @throws OperationException
		 */
		@POST
		@Path("/getpatientstudyinfo/")
		@WebResult(name = "PatientStudy" )
		public PatientProtocolIdentifier getPatientStudyInfo(
		@WebParam(name = "patProtId")		
		Integer patProtId)
				throws OperationException;   

}