package com.velos.webservices;

import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import org.apache.log4j.Logger;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.PatientStudyClient;
import com.velos.services.client.StudyPatientClient;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientProtocolIdentifier;
import com.velos.services.model.PatientStudy;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatient;
/**
 * Webservices class for Patient Study
 * (Studies on which patient is enrolled on)
 * @author velos
 *
 */
@WebService(
		serviceName="PatientStudyService",
		endpointInterface="com.velos.webservices.PatientStudySEI",
		targetNamespace="http://velos.com/services/")
		
public class PatientStudyWS implements PatientStudySEI{
	
	private static Logger logger = Logger.getLogger(PatientStudyWS.class.getName());
	public PatientStudyWS(){
		
	}
	/**
	 * Calls PatientStudyClient with PatientIdentifier and returns
	 * list of Patient Studies.
	 */
	public List<PatientStudy> getPatientStudies(PatientIdentifier patientId)
			throws OperationException {
		List<PatientStudy> PatientStudies = PatientStudyClient.getPatientStudy(patientId);
		return PatientStudies;
	}

	/**
	 * Calls PatientStudyClient with patProtId and returns PatientProtocolIdentifier.
	 */
	public PatientProtocolIdentifier getPatientStudyInfo (Integer patProtId) throws OperationException {
		PatientProtocolIdentifier patProtIdentifier = PatientStudyClient.getPatientStudyInfo(patProtId);
		return patProtIdentifier;
	}
	
}