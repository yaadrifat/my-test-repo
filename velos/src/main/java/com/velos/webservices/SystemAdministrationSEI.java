/**
 * Created On Nov 5, 2012
 */
package com.velos.webservices;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.velos.services.OperationException;
import com.velos.services.model.CodeTypes;
import com.velos.services.model.Codes;
import com.velos.services.model.ObjectInfo;
import com.velos.services.model.ObjectMaps;
import com.velos.services.model.ObjectInfos;
import com.velos.services.model.SimpleIdentifier;

/**
 * @author Kanwaldeep
 *
 */
@Path("/systemadministrationservice/")
@Produces("application/json")
@WebService(
		 targetNamespace="http://velos.com/services/")
public interface SystemAdministrationSEI {
	
	@GET
	@Path("/codes/{codelsttype}")
	@WebResult(name="Codes")
	public Codes getCodeList(
			@WebParam(name="type")
			@PathParam("codelsttype") 
			String type) throws OperationException; 
	
	@GET
	@Path("/codetypes/")
	@WebResult(name="CodeTypes")
	public CodeTypes getCodeTypes() throws OperationException; 
	
	public ObjectMaps getorCreateMObjectMap(
			@WebParam(name="TableInfos")
			ObjectInfos tableinfos) throws OperationException; 

	@GET
	@Path("/objectInfo/")
	@WebResult(name="ObjectInfo")
	public ObjectInfo getObjectInfoFromOID(
			SimpleIdentifier simpleIdentifier) throws OperationException; 

}
