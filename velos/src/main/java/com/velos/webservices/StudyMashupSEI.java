package com.velos.webservices;

import java.util.ArrayList;
import java.util.HashMap;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.velos.services.OperationException;
import com.velos.services.model.FilterParams;
import com.velos.services.model.Modules;
import com.velos.services.model.StudyFilterFormResponse;
import com.velos.services.model.StudyFormResponse;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyMashup;
import com.velos.services.model.StudyMashupDetailsResponse;


/**
 * Service Endpoint Interface dealing with generation of Operations
 *  of Study Mashup Web Services
 * @author Tarandeep Singh Bali
 *
 */

@Produces("application/json")
@Consumes
@WebService(
		 targetNamespace="http://velos.com/services/")
	/**
	 * 
	 */
public interface StudyMashupSEI {
	
	/**
	 * public method of Study Mashup Service Endpoint Interface
	 * which calls getStudyMashup method of StudyMashup webservice
	 * and returns Study Mashup
	 * @param studyId
	 * @return studyMashup
	 * @throws OperationException
	 */
	
	@POST
	@Path("/getStudyMashup/")
	@WebResult(name="StudyMashup")
	    public StudyMashup getStudyMashup(
		@WebParam(name = "StudyIdentifier")		
		StudyIdentifier studyId
		)
		throws OperationException;
	
	@POST
	@Path("/getStudyFormResponse/")
	@WebResult(name="StudyMashup")
	    public StudyFormResponse getStudyFormResponse(
		@WebParam(name = "FormIdentifier")		
		Integer formPK
		)
		throws OperationException;
	
	@POST
	@Path("/getFilterStudyFormResponse/")
	@WebResult(name="StudyMashupFilterFormResponse")
	    public StudyFilterFormResponse getFilterStudyFormResponse(
		@WebParam(name = "StudyFormFilterParams")		
		FilterParams formFilterParams
	    )
		throws OperationException;
	
	@POST
	@Path("/getStudyMashupDetails/")
	@WebResult(name="StudyMashupDetailsResponse")
	    public StudyMashupDetailsResponse getStudyMashupDetails(
	    @WebParam(name = "StudyIdentifier")		
	    StudyIdentifier studyIdentifier,
	    @WebParam(name = "FormPK")		
	    Integer formPK,
	    @WebParam(name = "Modules")		
	    Modules modules
	    )
		throws OperationException;

}
