/**
 * 
 */
package com.velos.webservices;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.MoreDetailValue;
import com.velos.services.model.MoreStudyDetails;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.Study;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyOrganization;
import com.velos.services.model.StudySearch;
import com.velos.services.model.StudySearchResults;
import com.velos.services.model.StudyStatus;
import com.velos.services.model.StudyStatusIdentifier;
import com.velos.services.model.StudyStatuses;
import com.velos.services.model.StudySummary;
import com.velos.services.model.StudyTeamMember;
import com.velos.services.model.StudyTeamMembers;
import com.velos.services.model.UserIdentifier;

/**
 * @author dylan
 *
 */
@WebService(
		 targetNamespace="http://velos.com/services/")
public interface StudySEI {

	@POST
	@Path("/createstudy/")
	@WebResult(name = "Response")
	public abstract ResponseHolder createStudy(
			@WebParam(name = "Study") 
			Study study, 
			
			@WebParam(name = "createNonSystemUsers")
			boolean createNonSystemUsers)
			throws OperationException, OperationRolledBackException;

	@POST
	@Path("/getstudy/")
	@WebResult(name = "Study")
	public abstract Study getStudy(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id) 
		throws OperationException;
	
	//DRM - 5431 - fixed typo in studysummary result name
	
	@POST
	@Path("/getstudysummary/")
	@WebResult(name = "StudySummary")
	public abstract StudySummary getStudySummary(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id) 
		throws OperationException;
	
	
	@POST
	@Path("/updatestudysummary/")
	@WebResult(name = "Response")
	public abstract ResponseHolder updateStudySummary(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id,
			
			@WebParam(name = "StudySummary")
			StudySummary studySummary,

			@WebParam(name = "createNonSystemUsers")
			boolean createNonSystemUsers) 
		throws OperationException, OperationRolledBackException;
	
	
	@POST
	@Path("/addstudyteammember/")
	@WebResult(name = "Response")
	public abstract ResponseHolder addStudyTeamMember(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id,
			
			@WebParam(name = "StudyTeamMember")
			StudyTeamMember studyTeamMember,

			@WebParam(name = "createNonSystemUsers")
			boolean createNonSystemUsers) 
		throws OperationException, OperationRolledBackException; 
	
	
	@POST
	@Path("/updatestudyteammember/")
	@WebResult(name = "Response")
	public abstract ResponseHolder updateStudyTeamMember(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id,
			
			@WebParam(name = "StudyTeamMember")
			StudyTeamMember studyTeamMember) 
		throws OperationException, OperationRolledBackException;
	
	
	@POST
	@Path("/removestudyteammember/")
	@WebResult(name = "Response")
	public abstract ResponseHolder removeStudyTeamMember(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id,
	 
			@WebParam(name = "StudyTeamUserIdentifier")
			UserIdentifier userIdentifier)

		throws OperationException, OperationRolledBackException;
	
	
	@POST
	@Path("/addstudyorganization/")
	@WebResult(name = "Response")
	public abstract ResponseHolder addStudyOrganization(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id,
			
			@WebParam(name = "StudyOrganization")
			StudyOrganization studyOrganization) 
		throws OperationException, OperationRolledBackException;
	
	
	@POST
	@Path("/updatestudyorganization/")
	@WebResult(name = "Response") 
	public abstract ResponseHolder updateStudyOrganization(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id,
			
			@WebParam(name = "StudyOrganization")
			StudyOrganization studyOrganization) 
		throws OperationException, OperationRolledBackException;
	
	
	@POST
	@Path("/removestudyorganization/")
	@WebResult(name = "Response")
	public abstract ResponseHolder removeStudyOrganization(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id,
			
			@WebParam(name = "OrganizationIdentifier")
			OrganizationIdentifier organizationIdentifier) 
		throws OperationException, OperationRolledBackException;
	
	@POST
	@Path("/addstudystatus/")
	@WebResult(name = "Response")
	public abstract ResponseHolder addStudyStatus(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id,
			
			@WebParam(name = "StudyStatus")
			StudyStatus studyStatus)
		throws OperationException, OperationRolledBackException;
//	
//	@WebResult(name = "Response")
//	public abstract ResponseHolder updateStatus( 
//			@WebParam(name = "StudyIdentifier")
//			StudyIdentifier id,
//			
//			@WebParam(name = "StudyStatus")
//			StudyStatus studyStatus) 
//		throws OperationException, OperationRolledBackException;
	
	@POST
	@Path("/getstudystatuses/")
	@WebResult(name = "StudyStatuses")
	public StudyStatuses getStudyStatuses(
			@WebParam(name = "StudyIdentifier")  
			StudyIdentifier studyIdentifier) 
	
	throws OperationException;
	
//	
//	@WebResult(name = "Response")
//	public abstract ResponseHolder updateStudyOrganizations(
//			@WebParam(name = "StudyIdentifier")
//			StudyIdentifier id,
//			
//			@WebParam(name = "StudyOrganizations")
//			List<StudyOrganization> studyOrganizations) 
//		throws OperationException;
//	
//	@WebResult(name = "Response")
//	public abstract ResponseHolder updateStudyTeamMembers(
//			@WebParam(name = "StudyIdentifier")
//			StudyIdentifier id,
//			
//			@WebParam(name = "StudyTeamMembers")
//			List<StudyTeamMember> studyTeamMembers,
//
//			@WebParam(name = "createNonSystemUsers")
//			boolean createNonSystemUsers)  
//		throws OperationException;
	
	/**
	 * 
	 * @param studyStatusIdentifier
	 * @return StudyStatus
	 * @throws OperationException
	 */
	
	@POST
	@Path("/getstudystatus/")
	@WebResult(name = "StudyStatus" )
	public StudyStatus getStudyStatus(
	@WebParam(name = "StudyStatusIdentifier")		
	StudyStatusIdentifier studyStatusIdentifier)
			throws OperationException;
	
	@POST
	@Path("/searchstudy/")
	@WebResult(name = "StudySearchResults" )
	public StudySearchResults searchStudy(
	@WebParam(name = "StudySearch")		
	StudySearch studySearch)
			throws OperationException;
	
	@POST
	@Path("/getstudyteammembers/")
	@WebResult(name = "StudyTeamMembers")
	public abstract StudyTeamMembers getStudyTeamMembers(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id) 
		throws OperationException;
	
	@POST
	@Path("/getCurrentStudyStatus/")
	@WebResult(name = "studyStatus" )
	public StudyStatus getCurrentStudyStatus(
			@WebParam(name = "StudyIdentifier")  
			StudyIdentifier studyIdentifier) 
				throws OperationException;
	
	@POST
	@Path("/getStudyMoreDetailValue/")
	@WebResult(name = "MoreStudyDetails")
	public MoreDetailValue  getStudyMoreDetailValue(@WebParam(name = "StudyIdentifier")
	StudyIdentifier id,@WebParam(name = "MoreStudyDetails")MoreStudyDetails moredetails ) 
		throws OperationException;
	
}