package com.velos.webservices;

import java.io.IOException;
import java.util.Date;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.model.ChangesList;
/**
 * Service Endpoint Interface dealing with generation of Operations
 *  of JMSMessage services
 * @author Parminder Singh
 *
 */
@WebService(
		 targetNamespace="http://velos.com/services/")
public interface OutboundMessageSEI {
	
	/**
	 * method declaration for JMSMessage of eSP 
	 * @return JMSMessage Object with eResearchCompatibilityVersionNumber,
	 * @throws OperationException
	 * @throws OperationRolledBackException
	 * @throws IOException
	 */
	@WebResult(name = "ChangesList")
	@POST
	@Path("/getOutboundMessages/")
	public ChangesList getOutboundMessages(
			@WebParam(name = "fromDate") 
			Date fromDate,
			@WebParam(name = "toDate" ) 
			Date toDate,
			@WebParam(name = "moduleName")
			String moduleName,
			@WebParam(name = "calledFrom")
			String calledFrom)
		throws OperationException;
	

}
