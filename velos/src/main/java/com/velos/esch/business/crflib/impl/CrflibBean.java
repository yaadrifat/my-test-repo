/*
 * Classname : CrflibBean
 * 
 * Version information: 1.0
 *
 * Date: 1/31/2002
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh
 */

package com.velos.esch.business.crflib.impl;

/* Import Statements */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

// import java.sql.*;

/* End of Import Statements */
@Entity
@Table(name = "sch_crflib")
public class CrflibBean implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 4049636780653687600L;

    /**
     * crflib id
     */
    public int crflibId;

    /**
     * events1 id
     */
    public Integer eventsId;

    /**
     * crflib number
     */
    public String crflibNumber;

    /**
     * crflib name
     */

    public String crflibName;

    /*
     * the record crflibFlag
     */

    public String crflibFlag;

    /*
     * the record creator
     */

    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    public CrflibBean() {

    }

    public CrflibBean(int crflibId, String eventsId, String crflibNumber,
            String crflibName, String crflibFlag, String creator,
            String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setCrflibId(crflibId);
        setEventsId(eventsId);
        setCrflibNumber(crflibNumber);
        setCrflibName(crflibName);
        setCrflibFlag(crflibFlag);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_SCH_CRFLIB", allocationSize=1)
    @Column(name = "PK_CRFLIB")
    public int getCrflibId() {
        return this.crflibId;
    }

    public void setCrflibId(int crflibId) {
        this.crflibId = crflibId;
    }

    @Column(name = "FK_EVENTS")
    public String getEventsId() {
        return StringUtil.integerToString(this.eventsId);
    }

    public void setEventsId(String eventsId) {

        if (eventsId != null) {
            this.eventsId = Integer.valueOf(eventsId);
        }
    }

    @Column(name = "CRFLIB_NUMBER")
    public String getCrflibNumber() {
        return this.crflibNumber;
    }

    public void setCrflibNumber(String crflibNumber) {
        this.crflibNumber = crflibNumber;
    }

    @Column(name = "CRFLIB_NAME")
    public String getCrflibName() {
        return this.crflibName;
    }

    public void setCrflibName(String crflibName) {
        this.crflibName = crflibName;
    }

    @Column(name = "CRFLIB_FLAG")
    public String getCrflibFlag() {
        return this.crflibFlag;
    }

    public void setCrflibFlag(String crflibFlag) {
        this.crflibFlag = crflibFlag;
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return Modified By
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modified
     *            by
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // / END OF GETTER SETTER METHODS

    /**
     * returns the state holder object associated with this crflib
     */

    /*
     * public CrflibStateKeeper getCrflibStateKeeper() { Rlog.debug("crflib",
     * "CrflibBean.getCrflibStateKeeper"); return new
     * CrflibStateKeeper(getCrflibId(), getEventsId(), getCrflibNumber(),
     * getCrflibName(), getCrflibFlag(), getCreator(), getModifiedBy(),
     * getIpAdd()); }
     */

    /**
     * sets up a state keeper containing details of the crflib
     * 
     * @param edsk
     */

    /*
     * public void setCrflibStateKeeper(CrflibStateKeeper usk) { GenerateId cfId =
     * null;
     * 
     * try { Connection conn = null; conn = getConnection();
     * Rlog.debug("crflib", "CrflibBean.setCrflibStateKeeper() Connection :" +
     * conn); crflibId = cfId.getId("SEQ_SCH_CRFLIB", conn); conn.close();
     * setEventsId(usk.getEventsId()); setCrflibNumber(usk.getCrflibNumber());
     * setCrflibName(usk.getCrflibName()); setCrflibFlag(usk.getCrflibFlag());
     * setCreator(usk.getCreator()); setModifiedBy(usk.getModifiedBy());
     * setIpAdd(usk.getIpAdd());
     * 
     * Rlog.debug("user", "UserBean.setUserStateKeeper() userId :" + crflibId); }
     * catch (Exception e) { Rlog .fatal("user", "Error in setUserStateKeeper()
     * in UserBean " + e); } }
     */

    /**
     * updates the crflib details
     * 
     * @param edsk
     * @return 0 if successful; -2 for exception
     */

    public int updateCrflib(CrflibBean edsk) {
        try {

            setEventsId(edsk.getEventsId());
            setCrflibNumber(edsk.getCrflibNumber());
            setCrflibName(edsk.getCrflibName());
            setCrflibFlag(edsk.getCrflibFlag());
            setCreator(edsk.getCreator());
            setModifiedBy(edsk.getModifiedBy());
            setIpAdd(edsk.getIpAdd());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("crflib", " error in CrflibBean.updateCrflib : " + e);
            return -2;
        }
    }

}// end of class
