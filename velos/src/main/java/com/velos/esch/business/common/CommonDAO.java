/*

 * Classname : CommonDao

 * 

 * Version information: 1.0

 *

 * Copyright notice: Velos, Inc

 * date: 03/06/2001

 *

 * Author: eresearch team

 * Base class for all Data Access  classes 

 */

package com.velos.esch.business.common;

/* Import statments */

import java.sql.Connection;
import java.sql.DriverManager;

import com.velos.base.dbutil.DBEngine;
import com.velos.esch.service.util.Configuration;
import com.velos.esch.service.util.Rlog;
import com.velos.eres.service.util.EnvUtil;
import com.velos.eres.service.util.StringUtil;

/* End of Import statments */

/*
 * *************************History********************************* Modified by
 * :Sonia Modified On:06/11/2004 Comment: 1. Added new method resetObject
 * *************************END****************************************
 */

public class CommonDAO {

    private static boolean disableConnectionPool = false;
    
    static {
        String disableEresPool = System.getenv("DISABLE_ERES_POOL");
        if (StringUtil.isEmpty(disableEresPool)) {
            try {
                disableEresPool = EnvUtil.getEnvVariable("DISABLE_ERES_POOL");
            } catch(Exception e) {}
        }
        if ("%DISABLE_ERES_POOL%".equals(disableEresPool)) {
            disableEresPool = System.getProperty("DISABLE_ERES_POOL");
        }
        if (StringUtil.trueValue(disableEresPool).startsWith("Y")) {
            disableConnectionPool = true;
        }
    }

    public static void InitializePool(String alias) {
        String eHome="";
        DBEngine dbengine = DBEngine.getEngine();
        if (!(dbengine.IsInitialized())) {
            eHome = StringUtil.trueValue(System.getenv("ERES_HOME"));
            if (StringUtil.isEmpty(eHome)) {
                try {
                    eHome = EnvUtil.getEnvVariable("ERES_HOME");
                } catch(Exception e) {}
            }
            if (eHome.trim().equals("%ERES_HOME%")) {
                eHome = StringUtil.trueValue(System.getProperty("ERES_HOME"));
            }
            eHome = eHome.trim();
            dbengine.initializePool(eHome+"erespool.properties", alias);
        }
    }

    /**
     * 
     * returns a connection object
     * 
     * 
     * 
     * 
     * 
     */

    public Connection getConnection() {
        if (!disableConnectionPool) {
            try {
                InitializePool("esch");
                return (DBEngine.getEngine().getConnection("esch")) ;

            } catch (Exception e) {
                Rlog.fatal("common", "getConnection(): exception:" + e);
                return null;
            }
        }

        // ////////////

        try {

            Rlog.debug("common", Configuration.DBDriverName);

            if (Configuration.DBDriverName == null) {

                Configuration.readSettings();

            }

            Rlog.debug("common", "DB DRiver = " + Configuration.DBDriverName);

            Rlog.debug("common", "DB URL = " + Configuration.DBUrl);

            Rlog.debug("common", "DB USER= " + Configuration.DBUser);

            Rlog.debug("common", "DB PWD = " + Configuration.DBPwd);

        } catch (Exception e) {

            Rlog.fatal("common", "IN COMMON DAO" + e);

        }

        Connection conn = null;

        try {

            Class.forName(Configuration.DBDriverName);

            conn = DriverManager.getConnection(Configuration.DBUrl,
                    Configuration.DBUser, Configuration.DBPwd);

            Rlog.debug("common", "Got Connection(): " + conn);

            return conn;

        } catch (Exception e) {

            Rlog.fatal("common", this.toString()
                    + ":getConnection(): exception:" + e);

            return null;

        }

    }

    /**
     * Resets the DAO object's fields to default values. To be overridden in
     * child classes
     */
    // by sonia sahni 06/11/04
    public void resetObject() {

    }
    // //////

}