/*
 * Classname : EventResourceBean
 *
 * Version information: 1.0
 *
 * Date: 02/15/2008
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Jnanamay Majumdar
 */

package com.velos.esch.business.eventresource.impl;

/* Import Statements */
import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

/* End of Import Statements */

@Entity
@Table(name = "SCH_EVRES_TRACK")
@NamedQuery(name = "findEventResourceIds", query = "SELECT OBJECT(eventresource) FROM EventResourceBean eventresource where trim(FK_EVENTSTAT)=:evtStat")
public class EventResourceBean implements Serializable {

    private static final long serialVersionUID = 3618700816186357304L;

    /**
     * pk of the table SCH_EVRES_TRACK
     */

    public int evtResTrackId; //pk_evres_track



    /**
     * fk_eventstat
     */
    public Integer evtStat; //fk_eventstat


    /**
     * fk_codelst_role
     */

    public Integer evtRole; //fk_codelst_role


    /*
     * event resource duration
     */
    public String evtTrackDuration; //evres_track_duration;


    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;


    private ArrayList evtRoles;

    private ArrayList evtTrackDurations;



    //empty constructor...
    public EventResourceBean() {

    }

    public EventResourceBean( int evtResTrackId, String evtStat, String evtRole, String evtTrackDuration,
    		String creator, String modifiedBy, String ipAdd, ArrayList evtRoles, ArrayList evtTrackDurations) {
        super();
        // TODO Auto-generated constructor stub
        setEvtResTrackId(evtResTrackId);
        setEvtStat(evtStat);
        setEvtRole(evtRole);
        setEvtTrackDuration(evtTrackDuration);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setEvtRoles(evtRoles);
        setEvtTrackDurations(evtTrackDurations);
    }

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "seq_sch_evres_track", allocationSize=1)
    @Column(name = "PK_EVRES_TRACK")
    public int getEvtResTrackId() {
        return this.evtResTrackId;
    }

    public void setEvtResTrackId(int evtResTrackId) {
        this.evtResTrackId = evtResTrackId;
    }


    @Column(name = "FK_EVENTSTAT")
    public String getEvtStat() {
    	return StringUtil.integerToString(this.evtStat);
    }

    public void setEvtStat(String evtStat) {

        if (evtStat != null) {
        	this.evtStat = Integer.valueOf(evtStat);
        }
    }

    @Column(name = "FK_CODELST_ROLE")
    public String getEvtRole() {
        return StringUtil.integerToString(this.evtRole);
    }

    public void setEvtRole(String evtRole) {
        if (evtRole != null) {
            this.evtRole = Integer.valueOf(evtRole);
        }
    }

    @Transient
    public ArrayList getEvtRoles() {
        return this.evtRoles;
    }

    public void setEvtRoles(ArrayList evtRoles) {
        this.evtRoles = evtRoles;
    }


    @Column(name = "EVRES_TRACK_DURATION")
    public String getEvtTrackDuration() {
        return this.evtTrackDuration;
    }

    public void setEvtTrackDuration(String evtTrackDuration) {
        this.evtTrackDuration = evtTrackDuration;
    }

    @Transient
    public ArrayList getEvtTrackDurations() {
        return this.evtTrackDurations;
    }

    public void setEvtTrackDurations(ArrayList evtTrackDurations) {
        this.evtTrackDurations = evtTrackDurations;
    }

    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    // END OF GETTER SETTER METHODS



    /**
     * updates the eventresource details
     *
     * @param evresSk
     * @return 0 if successful; -2 for exception
     */
   /* public int updateEventResource(EventResourceBean evresSk) {
        try {
                //??????????????????????????????? I have query for set pk also this updateEventResource is not needed....
        		setEvtStat(evresSk.getEvtStat());
                setEvtRole(evresSk.getEvtRole());
                setEvtTrackDuration(evresSk.getEvtTrackDuration());
                setCreator(evresSk.getCreator());
                setModifiedBy(evresSk.getModifiedBy());
                setIpAdd(evresSk.getIpAdd());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("eventresource",
                    " error in EventResourceBean.updateEventResource : " + e);
            return -2;
        }
    }*/
    //Note: no need of updateEventResource(), because every time adding only and in modify mode resources are
    // fetched through the SQL query...

}// end of class
