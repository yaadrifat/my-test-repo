/*
 * Classname : ProtVisitDao
 *
 * Version information : 1.0
 *
 * Date: 09/22/2004
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Sam Varadarajan
 */

package com.velos.esch.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import com.velos.eres.service.util.StringUtil;
import com.velos.esch.service.util.Rlog;

/* End of Import Statements */

public class ProtVisitDao extends CommonDAO implements java.io.Serializable {

    ArrayList visit_ids;

    ArrayList protocol_ids;

    ArrayList names;

    ArrayList visit_nos;

    ArrayList descriptions;

    ArrayList displacements;

    ArrayList months;

    ArrayList weeks;

    ArrayList days;

    // ArrayList calName;
    ArrayList insertAfterInterval; // SV, 10/27/04, added to be able to display

    // interval for (insert after) relative
    // visits.

    ArrayList insertAfterIntervalUnit;

    ArrayList insertAfterVisitId;

    // ?? ArrayList visit;


    ArrayList noIntervals;
    
    Integer totalRecords;
    
    ArrayList winBeforeNumber;
    ArrayList WinBeforeUnit;
    ArrayList WinAfterNumber;
    ArrayList WinAfterUnit;

    // //////////////////////////

    public ProtVisitDao() {
        visit_ids = new ArrayList();
        protocol_ids = new ArrayList();
        names = new ArrayList();
        visit_nos = new ArrayList();
        descriptions = new ArrayList();
        displacements = new ArrayList();
        months = new ArrayList();
        weeks = new ArrayList();
        days = new ArrayList();
        // SV, 10/27/04, added to be able to display interval for (insert after)
        // relative visits.
        insertAfterInterval = new ArrayList();
        insertAfterIntervalUnit = new ArrayList();
        insertAfterVisitId = new ArrayList();
        noIntervals = new ArrayList();
        winBeforeNumber = new ArrayList() ;
        WinBeforeUnit = new ArrayList() ;
        WinAfterNumber = new ArrayList() ;
        WinAfterUnit =  new ArrayList();

        // ?? visit = new ArrayList();

    }

    // Getter and Setter methods
    
    public ArrayList getWinBeforeNumber() {
        return this.winBeforeNumber;
    }

    public void setWinBeforeNumber(ArrayList winBeforeNumber) {
        this.winBeforeNumber = winBeforeNumber;
    }

    public ArrayList getWinBeforeUnit() {
        return this.WinBeforeUnit;
    }

    public void setWinBeforeUnit(ArrayList winBeforeUnit) {
        this.WinBeforeUnit = winBeforeUnit;
    }

    public ArrayList getWinAfterNumber() {
        return this.WinAfterNumber;
    }

    public void setWinAfterNumber(ArrayList winAfterNumber) {
        this.WinAfterNumber = winAfterNumber;
    }

    public ArrayList getWinAfterUnit() {
        return this.WinAfterUnit;
    }

    public void setWinAfterUnit(ArrayList winAfterUnit) {
        this.WinAfterUnit = winAfterUnit;
    }

    public ArrayList getVisit_ids() {
        return this.visit_ids;
    }

    public void setVisit_ids(ArrayList visit_ids) {
        this.visit_ids = visit_ids;
    }

    public ArrayList getProtocol_ids() {
        return this.protocol_ids;
    }

    public void setProtocol_ids(ArrayList protocol_ids) {
        this.protocol_ids = protocol_ids;
    }

    public ArrayList getNames() {
        return this.names;
    }

    public void setNames(ArrayList names) {
        this.names = names;
    }

    public ArrayList getVisit_nos() {
        return this.visit_nos;
    }

    public void setVisit_nos(ArrayList visit_nos) {
        this.visit_nos = visit_nos;
    }

    public ArrayList getDescriptions() {
        return this.descriptions;
    }

    public void setDescriptions(ArrayList descriptions) {
        this.descriptions = descriptions;
    }

    public ArrayList getDisplacements() {
        return this.displacements;
    }

    public void setDisplacements(ArrayList displacements) {
        this.displacements = displacements;
    }

    public ArrayList getMonths() {
        return (this.months);
    }

    public void setMonths(ArrayList months) {
        this.months = months;
    }

    public ArrayList getWeeks() {
        return (this.weeks);
    }

    public void setWeeks(ArrayList weeks) {
        this.weeks = weeks;
    }

    public ArrayList getDays() {
        return (this.days);
    }

    public void setDays(ArrayList days) {
        this.days = days;
    }

    public ArrayList getInsertAfterInterval() {
        return (this.insertAfterInterval);
    }

    public void setInsertAfterInterval(ArrayList insertAfterInterval) {
        this.insertAfterInterval = insertAfterInterval;
    }

    public void setInsertAfterInterval(Integer insertAfterInterval) {
        this.insertAfterInterval.add(insertAfterInterval);
    }

    public ArrayList getInsertAfterIntervalUnit() {
        return (this.insertAfterIntervalUnit);
    }

    public void setInsertAfterIntervalUnit(ArrayList insertAfterIntervalUnit) {
        this.insertAfterIntervalUnit = insertAfterIntervalUnit;
    }

    public void setInsertAfterIntervalUnit(String insertAfterIntervalUnit) {
        this.insertAfterIntervalUnit.add(insertAfterIntervalUnit);
    }

    public ArrayList getinsertAfterVisitId() {
        return (this.insertAfterVisitId);
    }

    public void setinsertAfterVisitId(ArrayList insertAfterVisitId) {
        this.insertAfterVisitId = insertAfterVisitId;
    }

    public void setinsertAfterVisitId(Integer insertAfterVisitId) {
        this.insertAfterVisitId.add(insertAfterVisitId);
    }


    public ArrayList getNoIntervals() {
        return this.noIntervals;
    }

    public void setNoIntervals(ArrayList noIntervals) {
        this.noIntervals = noIntervals;
    }


    public void setVisit_ids(Integer visit_id) {
        visit_ids.add(visit_id);
    }

    public void setProtocol_ids(Integer protocol_id) {
        protocol_ids.add(protocol_id);
    }

    public void setNoIntervals(String noInterval){
    	noIntervals.add(noInterval);
    }

    public void setNames(String name) {
        names.add(name);
    }

    public void setVisit_nos(Integer visit_no) {
        visit_nos.add(visit_no);
    }

    public void setDescription(String description) {
        descriptions.add(description);
    }

    public void setDisplacements(Integer displacement) {
        displacements.add(displacement);
    }

    public void setMonths(Integer month) {
        months.add(month);
    }

    public void setWeeks(Integer week) {
        weeks.add(week);
    }

    public void setDays(Integer day) {
        days.add(day);
    }
    
    public Integer getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(Integer totalRecords) {
		this.totalRecords = totalRecords;
	}

    /*
     * public void setCalNames(String calname) { calName.add(calname); }
     */
    public int getTotalVisits(int protocol_id) {
        // return this.visit_ids.size();
        PreparedStatement pstmt = null;
        Connection conn = null;

        int total_visits = 0;
        try {
            conn = getConnection();

            String mysql = "SELECT COUNT(*) TOTAL_VISITS"
                    + " FROM SCH_PROTOCOL_VISIT" + " WHERE FK_PROTOCOL = ?";



            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, protocol_id);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                total_visits = rs.getInt("TOTAL_VISITS");
                break; // there is only one row!
            }
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "protvisit",
                            "ProtVisitDao.getTotalVisits EXCEPTION IN FETCHING FROM SCH_PROTOCOL_VISIT table"
                                    + ex);
            return (0);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();

            } catch (Exception e) {
            }

        }
        // total_visits here will either be = count or 0..
        return (total_visits);

    }
    public int getMaxVisitNo() {
        Integer visit_no;
        // SV, 10/01, return the visit_no for last visit.
        // if this doesn't work, we may have to get from d/b??
        if (this.visit_ids.size() > 0) {
            visit_no = (Integer) this.visit_nos.get(this.visit_ids.size() - 1);
            return (visit_no.intValue());
        } else
            return (0);
    }

    public int getMaxVisitNo(int protocol_id) {
        int visit_no=-1;
        PreparedStatement pstmt = null;
        Connection conn = null;
        int total_visits = 0;
        try {
            conn = getConnection();
            String sql="SELECT MAX(visit_no) max_visit_no FROM SCH_PROTOCOL_VISIT WHERE FK_PROTOCOL = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, protocol_id);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                visit_no=rs.getInt("max_visit_no");

            }
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "protvisit",
                            "ProtVisitDao.getMaxVisitNo EXCEPTION IN FETCHING FROM SCH_PROTOCOL_VISIT table"
                                    + ex);
            return (-1);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();

            } catch (Exception e) {
            }

        }

        return visit_no;
    }


    public String getVisitName(int visitId) {
        int index;

        index = this.visit_ids.indexOf(new Integer(visitId));
        return ((index == -1) ? "" : this.names.get(index).toString());
    }

    public String getNoInterval(int visitId) {
        int index;

        index = this.visit_ids.indexOf(new Integer(visitId));
        return ((index == -1) ? "" : this.noIntervals.get(index).toString());
    }


    public int getProtocolVisitChildCount(int protocolVisitId){

    	PreparedStatement pstmt = null;
        Connection conn = null;
        int visitAfter = 0;

        try {
               Integer iValue = new Integer(0);
               String sValue = "";

               conn = getConnection();

    	       String mysql = " SELECT COUNT(*) " +
                              " FROM SCH_PROTOCOL_VISIT" + " WHERE  INSERT_AFTER = ?" ;


    	       pstmt = conn.prepareStatement(mysql);
    	       pstmt.setInt(1, protocolVisitId);
    	       ResultSet rs = pstmt.executeQuery();


    	       while (rs.next()) {
    	    	   visitAfter = rs.getInt(1);
    	       }

        }
        catch (SQLException ex) {
            Rlog
                    .fatal(
                            "protvisit",
                            "ProtVisitDao.getProtocolVisitChildCount EXCEPTION IN FETCHING FROM SCH_PROTOCOL_VISIT table"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
             } catch (Exception e) {
            }

         }


    	 return visitAfter;

    }

    public void getProtocolVisits(int protocol_id) {

        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            Integer iValue = new Integer(0);
            String sValue = "";

            conn = getConnection();

            String mysql = "SELECT PK_PROTOCOL_VISIT," + "FK_PROTOCOL,"
                    + "VISIT_NO," + "VISIT_NAME," + "DESCRIPTION,"
                    + " DECODE(NUM_DAYS,0,0,DISPLACEMENT) AS CALC_ORDER, "
                    + " DISPLACEMENT,"
                    + " NUM_MONTHS," + " NUM_WEEKS,"
                    + " NUM_DAYS," + " CREATOR," + " LAST_MODIFIED_BY,"
                    + " INSERT_AFTER," + " INSERT_AFTER_INTERVAL,"
                    + " INSERT_AFTER_INTERVAL_UNIT,WIN_BEFORE_NUMBER,WIN_BEFORE_UNIT,WIN_AFTER_NUMBER,WIN_AFTER_UNIT,NO_INTERVAL_FLAG "
                    + " FROM SCH_PROTOCOL_VISIT" + " WHERE FK_PROTOCOL = ?"
                    + " ORDER BY CALC_ORDER, visit_no "; //KM



            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, protocol_id);




            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                iValue = new Integer(0);
                sValue = rs.getString("PK_PROTOCOL_VISIT");
                Rlog.debug("protvisit", sValue);
                iValue = new Integer(sValue);
                setVisit_ids(iValue);

                sValue = rs.getString("FK_PROTOCOL");
                Rlog.debug("protocol", sValue);
                sValue = sValue.trim();
                iValue = new Integer(sValue);
                setProtocol_ids(iValue);

                setNames(rs.getString("VISIT_NAME"));

                sValue = rs.getString("VISIT_NO");
                iValue = new Integer(sValue.trim());
                setVisit_nos(iValue);

                sValue = rs.getString("DISPLACEMENT");

                //KM
                if(sValue != null)
                   iValue = new Integer(sValue.trim());
                else
                	iValue = new Integer(0);
                setDisplacements(iValue);

                setDescription(rs.getString("DESCRIPTION"));

                sValue = rs.getString("NUM_MONTHS");
                iValue = new Integer(sValue.trim());
                setMonths(iValue);

                sValue = rs.getString("NUM_WEEKS");
                iValue = new Integer(sValue.trim());
                setWeeks(iValue);
////D-FIN-25-DAY0 BK
                sValue = rs.getString("NUM_DAYS");                
                if(sValue != null){
                	iValue = StringUtil.stringToNum(sValue);
                }
                else{
                	iValue = null;
                }
                 
                setDays(iValue);

                // SV, 10/28/04, added insert after fields, to be able to show
                // the interval string on visit list screen.
                sValue = rs.getString("INSERT_AFTER");
                iValue = (sValue==null)? null:new Integer(sValue.trim());
                setinsertAfterVisitId(iValue);

                sValue = rs.getString("INSERT_AFTER_INTERVAL");
                iValue = (sValue==null)? null:new Integer(sValue.trim());
                setInsertAfterInterval(iValue);

                sValue = rs.getString("INSERT_AFTER_INTERVAL_UNIT");
                setInsertAfterIntervalUnit(sValue);
                setNoIntervals(rs.getString("NO_INTERVAL_FLAG"));
                this.winBeforeNumber.add(rs.getString("WIN_BEFORE_NUMBER"));
                this.WinBeforeUnit.add(rs.getString("WIN_BEFORE_UNIT"));
                this.WinAfterNumber.add(rs.getString("WIN_AFTER_NUMBER"));
                this.WinAfterUnit.add(rs.getString("WIN_AFTER_UNIT"));
            }

        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "protvisit",
                            "ProtVisitDao.getProtocolVisits EXCEPTION IN FETCHING FROM SCH_PROTOCOL_VISIT table"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    
    public void getProtocolVisits(int protocol_id,int initVisitSize,int limitVisitSize) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs=null;
        try {
            Integer iValue = new Integer(0);
            String sValue = "";
            StringBuffer sql = new StringBuffer();
            StringBuffer countsql = new StringBuffer();
            conn = getConnection();
                        
            countsql.append("SELECT count(*)")
            .append(" FROM SCH_PROTOCOL_VISIT ")
            .append(" WHERE FK_PROTOCOL = ? ");
            pstmt = conn.prepareStatement(countsql.toString());
            pstmt.setInt(1, protocol_id);
            rs = pstmt.executeQuery();
            while (rs.next()){
            	totalRecords=rs.getInt("count(*)");
            }

            pstmt = null;
            rs = null;
            String mysql = "SELECT PK_PROTOCOL_VISIT," + "FK_PROTOCOL,"
            + "VISIT_NO," + "VISIT_NAME," + "DESCRIPTION,"
            + " DECODE(NUM_DAYS,0,0,DISPLACEMENT) AS CALC_ORDER, "
            + " DISPLACEMENT,"
            + " NUM_MONTHS," + " NUM_WEEKS,"
            + " NUM_DAYS," + " CREATOR," + " LAST_MODIFIED_BY,"
            + " INSERT_AFTER," + " INSERT_AFTER_INTERVAL,"
            + " INSERT_AFTER_INTERVAL_UNIT, NO_INTERVAL_FLAG "
            + " FROM SCH_PROTOCOL_VISIT" + " WHERE FK_PROTOCOL = ?"
            + " ORDER BY CALC_ORDER, visit_no "; //KM
            sql
            .append("SELECT * from ("+mysql+") where ROWNUM <= ?");


            pstmt = conn.prepareStatement(sql.toString(),ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            pstmt.setInt(1, protocol_id);
            pstmt.setInt(2,limitVisitSize );
            pstmt.setFetchSize(1000);
            rs = pstmt.executeQuery();
            rs.absolute(initVisitSize);
            do{

                iValue = new Integer(0);
                sValue = rs.getString("PK_PROTOCOL_VISIT");
                Rlog.debug("protvisit", sValue);
                iValue = new Integer(sValue);
                setVisit_ids(iValue);

                sValue = rs.getString("FK_PROTOCOL");
                Rlog.debug("protocol", sValue);
                sValue = sValue.trim();
                iValue = new Integer(sValue);
                setProtocol_ids(iValue);

                setNames(rs.getString("VISIT_NAME"));

                sValue = rs.getString("VISIT_NO");
                iValue = new Integer(sValue.trim());
                setVisit_nos(iValue);

                sValue = rs.getString("DISPLACEMENT");

                //KM
                if(sValue != null)
                   iValue = new Integer(sValue.trim());
                else
                	iValue = new Integer(0);
                setDisplacements(iValue);

                setDescription(rs.getString("DESCRIPTION"));

                sValue = rs.getString("NUM_MONTHS");
                iValue = new Integer(sValue.trim());
                setMonths(iValue);

                sValue = rs.getString("NUM_WEEKS");
                iValue = new Integer(sValue.trim());
                setWeeks(iValue);
////D-FIN-25-DAY0 BK
                sValue = rs.getString("NUM_DAYS");                
                if(sValue != null){
                	iValue = StringUtil.stringToNum(sValue);
                }
                else{
                	iValue = null;
                }
                 
                setDays(iValue);

                // SV, 10/28/04, added insert after fields, to be able to show
                // the interval string on visit list screen.
                sValue = rs.getString("INSERT_AFTER");
                iValue = (sValue==null)? null:new Integer(sValue.trim());
                setinsertAfterVisitId(iValue);

                sValue = rs.getString("INSERT_AFTER_INTERVAL");
                iValue = (sValue==null)? null:new Integer(sValue.trim());
                setInsertAfterInterval(iValue);

                sValue = rs.getString("INSERT_AFTER_INTERVAL_UNIT");
                setInsertAfterIntervalUnit(sValue);
                setNoIntervals(rs.getString("NO_INTERVAL_FLAG"));

            }while (rs.next());

        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "protvisit",
                            "ProtVisitDao.getProtocolVisits EXCEPTION IN FETCHING FROM SCH_PROTOCOL_VISIT table"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // SV, 10/26/04, added to be able to cascade delete events attached to
    // visits when a visit is deleted..
    public int DeleteVisitEvents(int protocol_id, int visit_id, String calType) {

        CallableStatement cstmt = null;
        int success = -1;

        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call pkg_protocol_visit.sp_cascade_delete_visit_events(?, ?,?)}");
            cstmt.setInt(1, protocol_id);
            cstmt.setInt(2, visit_id);
            cstmt.setString(3, calType); // SV, 10/26
            cstmt.execute();
            success = 1;

        }

        catch (SQLException ex) {
        	
            Rlog.fatal("protvisit",
                    "In DeleteVisitEvents in Protvisit line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    // SV, 10/26/04, added to propagate the displacement changes from visit to
    // the attached events.
    public int updateVisitEvents(int protocol_id, int visit_id, String calType) {

        CallableStatement cstmt = null;
        int success = -1;

        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call pkg_protocol_visit.sp_update_visit_event(?, ?,?)}");
            cstmt.setInt(1, protocol_id);
            cstmt.setInt(2, visit_id);
            cstmt.setString(3, calType); // SV, 10/26
            cstmt.execute();
            success = 1;

        }

        catch (SQLException ex) {
            Rlog.fatal("protvisit",
                    "In DeleteVisitEvents in Protvisit line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    // SV, 10/26/04, added to validate name, displacement for the visit against
    // the calendar. duplicate names are not allowed.
    // duplicate visits for the same displacement is not currently allowed. This
    // validation may be taken out, if we allow many-to-many relationship
    // between events and visits.
    //D-FIN-25-DAY0 BK JAN-23-2011
    //fix 5884
    public int ValidateVisit(int protocolId, int visitId, String visitName,
            int displacement,Integer days,int weeks,int months) {
        int result = 0;
        int daySevenCheck = 0;
        
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
        	if((months == 1 && weeks ==1)||  (months == 1 && weeks == 0) || (months == 0 && weeks ==1)){
        		 daySevenCheck = 1;
        	}
            String mysql = "";
            conn = getConnection();
            
            mysql = "select PKG_PROTOCOL_VISIT.validate_visit(?,?,?,?,?,?) as result from  dual";
            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, protocolId);
            pstmt.setInt(2, visitId);
            pstmt.setString(3, visitName);
            pstmt.setInt(4, displacement);
            if(days != null ){
            pstmt.setInt(5, days);
            }
            else{
            pstmt.setInt(5, 0);
            }
            pstmt.setInt(6, daySevenCheck);

            Rlog.debug("protvisit", mysql);

            ResultSet rs = pstmt.executeQuery();
            rs.next();
            result = rs.getInt("result");



            return result;

        } catch (SQLException ex) {
            Rlog.fatal("protvisit",
                    "ProtVisitDao.ValidateVisitName EXCEPTION IN ValidateVisitName "
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return result;

    }

    // SV, 10/28/04, delete the system generated visits created when scheduled
    // event was created and since removed.
    public int DeleteGenVisitsWithNoEvents(int protocol_id, String calType) {

        CallableStatement cstmt = null;
        int success = -1;

        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call pkg_protocol_visit.sp_delete_generated_visit(?,?)}");
            cstmt.setInt(1, protocol_id);
            cstmt.setString(2, calType); // SV, 10/26
            cstmt.execute();
            success = 1;

        }

        catch (SQLException ex) {
            Rlog
                    .fatal(
                            "protvisit",
                            "In DeleteGenVisitsWithNoEvents in Protvisit line EXCEPTION IN calling the procedure"
                                    + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }
    
    //This method is not used anymore. Bug #6631
    //pushes all the "No interval visits" out
    public int pushNoIntervalVisitsOut(int protocol_visit_id, String ip, int userId) {

        CallableStatement cstmt = null;
        CallableStatement cstmt2 = null;
        int success = -1;

        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call pkg_protocol_visit.sp_pushNoIntervalVisitsOut(?,?,?)}");
            cstmt.setInt(1, protocol_visit_id);
            cstmt.setString(2, ip);
            cstmt.setInt(3, userId);
            cstmt.execute();
            success = 1;

          }

        catch (SQLException ex) {
            Rlog.fatal("protvisit",
                    "In pushNoIntervalVisitsOut EXCEPTION IN calling the procedure"
                            + ex);
            ex.printStackTrace();
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    //returns the latest protocol duration
    public int generateRipple(int protocol_visit_id,String calledFrom) {

        CallableStatement cstmt = null;
        CallableStatement cstmt2 = null;
        int success = -1;

        Connection conn = null;
        try {
            conn = getConnection();
            /*cstmt = conn
                    .prepareCall("{call pkg_protocol_visit.sp_generate_ripple(?,?)}");
            cstmt.setInt(1, protocol_visit_id);
            cstmt.setString(2, calledFrom);
             cstmt.execute();*/
            success = 1;

            //update protocol duration in case it is less than the max protocol visit displacement

            cstmt2 = conn.prepareCall("{call pkg_protocol_visit.sp_update_protocol_duration(?,?,?)}");
		    cstmt2.setInt(1, protocol_visit_id);
		    cstmt2.setString(2, calledFrom);
		    cstmt2.registerOutParameter(3, java.sql.Types.INTEGER);

		    cstmt2.execute();

		    success = cstmt2.getInt(3); //get the new duration


          }

        catch (SQLException ex) {
            Rlog.fatal("protvisit",
                    "In GenerateRipple EXCEPTION IN calling the procedure"
                            + ex);
            ex.printStackTrace();
        } finally {
            try {
                //if (cstmt != null)
                //    cstmt.close();

                if (cstmt2 != null)
                    cstmt2.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }


    // Added by Manimaran for the Enhancement #C8.3
    public int copyVisit(int protocolId,int frmVisitId, int visitId,String calledFrom, String usr, String ipAdd) {
        int ret = 1;
        CallableStatement cstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            cstmt = conn
                    .prepareCall("{call PKG_PROTOCOL_VISIT.SP_VISIT_COPY(?,?,?,?,?,?,?)}");//KM-3447
            Rlog.debug("protstat",
                    "ProtVisitDao:copyVisit-after prepare call of sp_visit_copy");
            cstmt.setInt(1, protocolId);
            cstmt.setInt(2, frmVisitId);
            cstmt.setInt(3, visitId);
            cstmt.setString(4, calledFrom);//KM
            cstmt.setString(5, usr);
            cstmt.setString(6, ipAdd);
            cstmt.registerOutParameter(7, java.sql.Types.INTEGER);
            cstmt.execute();
            ret = cstmt.getInt(7);
            Rlog.debug("protvisit", "ProtVisitDao:copyVisit-after execute of RETURN VALUE"
                    + ret);

            return ret;

        } catch (Exception e) {
            Rlog.fatal("protvisit",
                    "EXCEPTION in ProtVisitDao:copyVisit, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }



    //

  /**JM: 16Apr2008
   *
   * @param protocol_id, search
   */

    public void getProtocolVisits(int protocol_id, String search) {

        PreparedStatement pstmt = null;
        Connection conn = null;


        StringBuffer sbMySql = new StringBuffer();

        try {
            Integer iValue = new Integer(0);
            String sValue = "";

            conn = getConnection();

            sbMySql.append("SELECT PK_PROTOCOL_VISIT," + "FK_PROTOCOL,"
                    + "VISIT_NO," + "VISIT_NAME," + "DESCRIPTION,"
                    + "DISPLACEMENT," + " NUM_MONTHS," + " NUM_WEEKS,"
                    + " NUM_DAYS," + " CREATOR," + " LAST_MODIFIED_BY,"
                    + " INSERT_AFTER," + " INSERT_AFTER_INTERVAL,"
                    + " INSERT_AFTER_INTERVAL_UNIT"
                    + " FROM SCH_PROTOCOL_VISIT" + " WHERE FK_PROTOCOL = ?");

            		if (!search.equals("")){
            			//KM-3456
            			sbMySql.append(" and lower(VISIT_NAME) like (lower('%"+search+"%')) escape '\\' ") ;
            		}

            		sbMySql.append("ORDER BY displacement,visit_no ");

            String mysql=sbMySql.toString();



            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, protocol_id);




            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                iValue = new Integer(0);
                sValue = rs.getString("PK_PROTOCOL_VISIT");
                Rlog.debug("protvisit", sValue);
                iValue = new Integer(sValue);
                setVisit_ids(iValue);

                sValue = rs.getString("FK_PROTOCOL");
                Rlog.debug("protocol", sValue);
                sValue = sValue.trim();
                iValue = new Integer(sValue);
                setProtocol_ids(iValue);

                setNames(rs.getString("VISIT_NAME"));

                sValue = rs.getString("VISIT_NO");
                iValue = new Integer(sValue.trim());
                setVisit_nos(iValue);

                sValue = rs.getString("DISPLACEMENT");
                //KM
                if(sValue != null)
                   iValue = new Integer(sValue.trim());
                else
                	iValue = new Integer(0);
                setDisplacements(iValue);

                setDescription(rs.getString("DESCRIPTION"));

                sValue = rs.getString("NUM_MONTHS");
                iValue = new Integer(sValue.trim());
                setMonths(iValue);

                sValue = rs.getString("NUM_WEEKS");
                iValue = new Integer(sValue.trim());
                setWeeks(iValue);

                sValue = rs.getString("NUM_DAYS");
                if(sValue != null)
                    iValue = new Integer(sValue.trim());
                 else
                 	iValue = null;
                setDays(iValue);

                // SV, 10/28/04, added insert after fields, to be able to show
                // the interval string on visit list screen.
                sValue = rs.getString("INSERT_AFTER");
                if(sValue != null)
                    iValue = new Integer(sValue.trim());
                 else
                 	iValue = null;
                setinsertAfterVisitId(iValue);

                sValue = rs.getString("INSERT_AFTER_INTERVAL");
                if(sValue != null)
                    iValue = new Integer(sValue.trim());
                 else
                 	iValue = null;
                setInsertAfterInterval(iValue);

                sValue = rs.getString("INSERT_AFTER_INTERVAL_UNIT");
                setInsertAfterIntervalUnit(sValue);

            }

        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "protvisit",
                            "ProtVisitDao.getProtocolVisits EXCEPTION IN FETCHING FROM SCH_PROTOCOL_VISIT table"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }


}
