/*
 * Classname : AdvInfoBean
 * 
 * Version information: 1.0
 *
 * Date: 12/10/2002
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.business.advInfo.impl;

/* Import Statements */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

// import java.sql.*;

/* End of Import Statements */

/**
 * CMP entity bean for Adverse Event Info
 * 
 * <p>
 * In eResearch Architecture, the class resides at the Business layer.
 * </p>
 * 
 * @author Arvind Kumar
 * @version %I%, %G%
 */
@Entity
@Table(name = "SCH_ADVINFO")
public class AdvInfoBean implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 3832899966220843056L;

    /**
     * adverse event info id
     */
    public int advInfoId;

    /**
     * adverse event id
     */
    public Integer advInfoAdverseId;

    /**
     * info code
     */
    public Integer advInfoCodelstInfoId;

    /**
     * info value
     */
    public Integer advInfoValue;

    /**
     * info type
     */
    public String advInfoType;

    /**
     * creator
     */
    public Integer creator;

    /**
     * modified By
     */
    public Integer modifiedBy;

    /**
     * IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS
    /**
     * Returns an id for the adverse event info
     * 
     * @return the adverse event info id
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_SCH_ADVINFO", allocationSize=1)
    @Column(name = "PK_ADVINFO")
    public int getAdvInfoId() {
        return this.advInfoId;
    }

    /**
     * Sets the id for the adverse event info
     * 
     * @param advInfoId
     *            the adverse event info id
     */
    public void setAdvInfoId(int advInfoId) {
        this.advInfoId = advInfoId;
    }

    /**
     * Gets id of the parent adverse event
     * 
     * @return id of the parent adverse event
     */
    @Column(name = "FK_ADVERSE")
    public String getAdvInfoAdverseId() {
        return StringUtil.integerToString(this.advInfoAdverseId);
    }

    /**
     * Sets the id for the parent adverse event
     * 
     * @param advInfoAdverseId
     *            the parent adverse event
     */
    public void setAdvInfoAdverseId(String advInfoAdverseId) {
        if (advInfoAdverseId != null) {
            this.advInfoAdverseId = Integer.valueOf(advInfoAdverseId);
            Rlog.debug("advInfo", "AdvInfoBeansetAdvInfoAdverseId line 1");
        } else {
            Rlog.debug("advInfo", "AdvInfoBeansetAdvInfoAdverseId line 2");
        }
    }

    /**
     * Gets adverse event info code
     * 
     * @return id of the adverse event info code
     */
    @Column(name = "FK_CODELST_INFO")
    public String getAdvInfoCodelstInfoId() {
        return StringUtil.integerToString(this.advInfoCodelstInfoId);
    }

    /**
     * Sets adverse event info code
     * 
     * @param advInfoCodelstInfoId
     *            the adverse event info code
     */
    public void setAdvInfoCodelstInfoId(String advInfoCodelstInfoId) {
        if (advInfoCodelstInfoId != null) {
            this.advInfoCodelstInfoId = Integer.valueOf(advInfoCodelstInfoId);
        }
    }

    /**
     * Gets adverse event info value
     * 
     * @return id of the adverse event info value
     */
    @Column(name = "ADVINFO_VALUE")
    public String getAdvInfoValue() {
        return StringUtil.integerToString(this.advInfoValue);
    }

    /**
     * Sets adverse event info value
     * 
     * @param advInfoValue
     *            the adverse event info value
     */
    public void setAdvInfoValue(String advInfoValue) {
        if (advInfoValue != null) {
            this.advInfoValue = Integer.valueOf(advInfoValue);
        }
    }

    /**
     * Gets adverse event info type
     * 
     * @return id of the adverse event info type
     */
    @Column(name = "ADVINFO_TYPE")
    public String getAdvInfoType() {
        return this.advInfoType;
    }

    /**
     * Sets adverse event info type
     * 
     * @param advInfoType
     *            the adverse event info type
     */
    public void setAdvInfoType(String advInfoType) {
        this.advInfoType = advInfoType;
    }

    /**
     * Gets the user who created the adverse event info
     * 
     * @return the user who created the adverse event info
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * Sets the user who created the adverse event info
     * 
     * @param creator
     *            the user who created the adverse event info
     */

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * Gets the user who modified the adverse event info
     * 
     * @return the user who modified the adverse event info
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * Sets the user who modified the adverse event info
     * 
     * @param modifiedBy
     *            the user who modified the adverse event info
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * Gets the IP address of the user who created/modified the adverse event
     * info
     * 
     * @return IP address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * Sets the IP address of the user who created/modified the adverse event
     * info
     * 
     * @param ipAdd
     *            IP address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // / END OF GETTER SETTER METHODS

    /**
     * updates adverse event info
     * 
     * @param edsk
     *            an AdvInfoStateKeeper object containing adverse event info
     *            details
     * @return success flag
     *         <ul>
     *         <li> 0 if successful</li>
     *         <li> -2 in case of a failure</li>
     *         </ul>
     */

    public int updateAdvInfo(AdvInfoBean edsk) {
        try {

            setAdvInfoAdverseId(edsk.getAdvInfoAdverseId());
            setAdvInfoCodelstInfoId(edsk.getAdvInfoCodelstInfoId());
            setAdvInfoValue(edsk.getAdvInfoValue());
            setAdvInfoType(edsk.getAdvInfoType());

            setCreator(edsk.getCreator());
            setModifiedBy(edsk.getModifiedBy());
            setIpAdd(edsk.getIpAdd());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("advInfo", " error in AdvInfoBean.updateInfoEve : " + e);
            return -2;
        }
    }

    public AdvInfoBean() {

    }

    public AdvInfoBean(int advInfoId, String advInfoAdverseId,
            String advInfoCodelstInfoId, String advInfoValue,
            String advInfoType, String creator, String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setAdvInfoId(advInfoId);
        setAdvInfoAdverseId(advInfoAdverseId);
        setAdvInfoCodelstInfoId(advInfoCodelstInfoId);
        setAdvInfoValue(advInfoValue);
        setAdvInfoType(advInfoType);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

}// end of class
