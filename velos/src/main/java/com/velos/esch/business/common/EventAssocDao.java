package com.velos.esch.business.common;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.StringUtil;
import com.velos.esch.service.util.Rlog;

// Referenced classes of package com.velos.esch.business.common:
//            CommonDAO

public class EventAssocDao extends CommonDAO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6143069265146969855L;

    ArrayList event_ids;
    
    ArrayList new_event_ids;     /*YK :: DFIN12 BUG #5954*/

    ArrayList org_ids;

	ArrayList chain_ids;

    ArrayList event_types;

    ArrayList names;

    ArrayList notes;

    ArrayList costs;

    ArrayList cost_descriptions;

    ArrayList durations;

    ArrayList user_ids;

    ArrayList linked_uris;

    ArrayList fuzzy_periods;

    ArrayList msg_tos;    

    ArrayList descriptions;

    ArrayList displacements;

    ArrayList statusChangedBys;

    ArrayList statusChangedOns;

    ArrayList protStatIds;

    String protName;

    String protDuration;

    // // Changes made for the Phase II by Sonia Kaura on 3rd November 2003
    ArrayList month; // SV 10/26

    ArrayList week;

    ArrayList day;

    // SV, 9/27/04, cal-enh-03-b
    ArrayList eventVisits;

    ArrayList flags;//JM

    private ArrayList fkVisit;

    private ArrayList cptCodes; //KM

    private ArrayList eventCategory;

    private ArrayList sequences;

  //JM: 25Apr2008, added for, Enh. #C9
    private ArrayList offlineFlags;
    private ArrayList hideFlags;

    private ArrayList protocolAssocTo;

    private ArrayList eventSOSIds;

    private ArrayList eventFacilityIds;

    private ArrayList eventCoverageTypes;

    private ArrayList eventCoverageNotes;

    ArrayList Facility; // KV:SW-FIN2c
    ArrayList eventCostDetails; //KV:SW-FIN2b

    private ArrayList budgetTemplates;

/* calendar status */

    private ArrayList statCodes;
    
    private ArrayList codeSubTypes;
    
    private ArrayList codeDescriptions;
    
    
    //BK-D-FIN25-DAY0
    ArrayList insertAfter;  
	ArrayList afterIntervalStrings;
	
	//Ankit PCAL-20282
	ArrayList eventLibraryType;

	Integer totalRecords;
	
	ArrayList servicecodes;
	ArrayList prscodes;

	public EventAssocDao() {
        Rlog.debug("eventassoc", "EventAssocDao.constructor ");
        event_ids = new ArrayList();
        new_event_ids = new ArrayList(); /*YK :: DFIN12 BUG #5954*/
        org_ids = new ArrayList();
        chain_ids = new ArrayList();
        event_types = new ArrayList();
        names = new ArrayList();
        notes = new ArrayList();
        costs = new ArrayList();
        cost_descriptions = new ArrayList();
        durations = new ArrayList();
        user_ids = new ArrayList();
        linked_uris = new ArrayList();
        fuzzy_periods = new ArrayList();
        msg_tos = new ArrayList();        
        descriptions = new ArrayList();
        displacements = new ArrayList();
        statusChangedBys = new ArrayList();
        statusChangedOns = new ArrayList();
        protStatIds = new ArrayList();
        protName = new String();
        protDuration = new String();

        // SONIA KAURA
        month = new ArrayList();
        week = new ArrayList();
        day = new ArrayList();
        // SV, 9/27/04, cal-enh-03-b
        eventVisits = new ArrayList();
        flags = new ArrayList();
        fkVisit = new ArrayList();
        cptCodes = new ArrayList(); //KM
        eventCategory = new ArrayList();

        //JM: 25Apr2008, added for, Enh. #C9
        offlineFlags = new ArrayList();
        hideFlags = new ArrayList();

        sequences = new ArrayList();
        protocolAssocTo= new ArrayList();

        eventSOSIds = new ArrayList();
        eventFacilityIds = new ArrayList();
        eventCoverageTypes = new ArrayList();
        eventCoverageNotes = new ArrayList();

        Facility = new ArrayList(); //KV:SW-FIN2c
        eventCostDetails = new ArrayList();

        budgetTemplates = new ArrayList();

        statCodes = new ArrayList();
        codeSubTypes = new ArrayList();
        
        codeDescriptions = new ArrayList();
        //BK-DFIN25-DAY0
        afterIntervalStrings = new ArrayList();
        insertAfter = new ArrayList();
        // Ankit PCAL-20282
        eventLibraryType = new ArrayList();
        servicecodes = new ArrayList();
    	prscodes = new ArrayList();
    }
	
	public ArrayList getServicecodes() {
		return servicecodes;
	}

	public void setServicecodes(ArrayList servicecodes) {
		this.servicecodes = servicecodes;
	}

	public ArrayList getPrscodes() {
		return prscodes;
	}

	public void setPrscodes(ArrayList prscodes) {
		this.prscodes = prscodes;
	}
	
    public void setNewPRSCodes(String String2) {
        this.prscodes.add(String2);
    }

    public void setNewServiceCodes(String String2) {
        this.servicecodes.add(String2);
    }

    public String getProtName() {
        return this.protName;
    }

    public void setProtName(String protName) {
        this.protName = protName;
    }

    public String getProtDuration() {
        return this.protDuration;
    }

    public void setProtDuration(String protDuration) {
        this.protDuration = protDuration;
    }

    public ArrayList getEvent_ids() {
        return event_ids;
    }

    public void setEvent_ids(ArrayList arraylist) {
        event_ids = arraylist;
    }

    /*YK :: DFIN12 BUG #5954*/
    public ArrayList getNew_event_ids() {
		return new_event_ids;
	}

	public void setNew_event_ids(ArrayList newEventIds) {
		new_event_ids = newEventIds;
	}



    public ArrayList getOrg_ids() {
        return org_ids;
    }

    public void setOrg_ids(ArrayList arraylist) {
        org_ids = arraylist;
    }

    public ArrayList getChain_ids() {
        return chain_ids;
    }

    public void setChain_ids(ArrayList arraylist) {
        chain_ids = arraylist;
    }

    public ArrayList getEvent_types() {
        return event_types;
    }

    public void setEvent_types(ArrayList arraylist) {
        event_types = arraylist;
    }

    public ArrayList getNames() {
        return names;
    }

    public void setNames(ArrayList arraylist) {
        names = arraylist;
    }

    public ArrayList getNotes() {
        return notes;
    }

    public void setNotes(ArrayList arraylist) {
        notes = arraylist;
    }

    public ArrayList getCosts() {
        return costs;
    }

    public void setCosts(ArrayList arraylist) {
        costs = arraylist;
    }

    public ArrayList getCost_descriptions() {
        return cost_descriptions;
    }

    public void setCost_descriptions(ArrayList arraylist) {
        cost_descriptions = arraylist;
    }

    public ArrayList getDurations() {
        return durations;
    }

    public void setDurations(ArrayList arraylist) {
        durations = arraylist;
    }

    public ArrayList getUser_ids() {
        return user_ids;
    }

    public void setUser_ids(ArrayList arraylist) {
        user_ids = arraylist;
    }

    public ArrayList getLinked_uris() {
        return linked_uris;
    }

    public void setLinked_uris(ArrayList arraylist) {
        linked_uris = arraylist;
    }

    public ArrayList getFuzzy_periods() {
        return fuzzy_periods;
    }

    public void setFuzzy_periods(ArrayList arraylist) {
        fuzzy_periods = arraylist;
    }

    public ArrayList getMsg_tos() {
        return msg_tos;
    }

    public void setMsg_tos(ArrayList arraylist) {
        msg_tos = arraylist;
    }    

    public ArrayList getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(ArrayList arraylist) {
        descriptions = arraylist;
    }

    public void setDescriptions(String description) {
        descriptions.add(description);
    }

    public ArrayList getDisplacements() {
        return displacements;
    }

    public void setDisplacements(ArrayList arraylist) {
        displacements = arraylist;
    }

    public ArrayList getStatusChangedBys() {
        return this.statusChangedBys;
    }

    public void setStatusChangedBys(ArrayList statusChangedBys) {
        this.statusChangedBys = statusChangedBys;
    }

    public ArrayList getStatusChangedOns() {
        return this.statusChangedOns;
    }

    public void setStatusChangedOns(ArrayList statusChangedOns) {
        this.statusChangedOns = statusChangedOns;
    }

    public ArrayList getProtStatIds() {
        return this.protStatIds;
    }

    public void setProtStatIds(ArrayList protStatIds) {
        this.protStatIds = protStatIds;
    }

    public void setEvent_ids(Integer integer) {
        event_ids.add(integer);
    }
    /*YK :: DFIN12 BUG #5954*/
    public void setNew_event_ids(Integer integer) {
    	new_event_ids.add(integer);
     }

    public void setOrg_ids(Integer integer) {
        org_ids.add(integer);
    }

    public void setChain_ids(String s) {
        chain_ids.add(s);
    }

    public void setEvent_types(String s) {
        event_types.add(s);
    }

    public void setNames(String s) {
        names.add(s);
    }

    public void setNotes(String s) {
        notes.add(s);
    }

    public void setCosts(String s) {
        costs.add(s);
    }

    public void setCost_descriptions(String s) {
        cost_descriptions.add(s);
    }

    public void setDurations(String s) {
        durations.add(s);
    }

    public void setUser_ids(String s) {
        user_ids.add(s);
    }

    public void setLinked_uris(String s) {
        linked_uris.add(s);
    }

    public void setFuzzy_periods(String s) {
        fuzzy_periods.add(s);
    }

    public void setMsg_tos(String s) {
        msg_tos.add(s);
    }

    public void setDescription(String s) {
        descriptions.add(s);
    }

    public void setDisplacement(String s) {
        displacements.add(s);
    }

    public void setStatusChangedBys(String statusChangedBy) {
        statusChangedBys.add(statusChangedBy);
    }

    public void setStatusChangedOns(String statusChangedOn) {
        statusChangedOns.add(statusChangedOn);
    }

    public void setProtStatIds(String protStatId) {
        protStatIds.add(protStatId);
    }

    // ////////Changes for Phase II By SONIA - KAURA On

    public void setMonth(ArrayList month) {
        this.month = month;
    }

    public void setWeek(ArrayList week) {
        this.week = week;
    }

    public void setDay(ArrayList day) {
        this.day = day;
    }

    public ArrayList getMonth() {
        return (this.month);
    }

    public ArrayList getWeek() {
        return (this.week);
    }

    public ArrayList getDay() {
        return (this.day);
    }

    // SV, 9/27/04, cal-enh-03-b
    public ArrayList getEventVisits() {
        return (this.eventVisits);
    }

    // //////////////

    public void setMonth(Integer month) {
        this.month.add(month);
    }

    public void setWeek(Integer week) {
        this.week.add(week);
    }

    public void setDay(Integer day) {
        this.day.add(day);
    }

    // SV, 9/27/04, cal-enh-03-b

    public void setEventVisits(ArrayList visits) {
        this.eventVisits = visits;
    }

    public void setEventVisits(Integer visit) {
        this.eventVisits.add(visit);
    }

    public ArrayList getFkVisit() {
        return fkVisit;
    }

    public void setFkVisit(ArrayList fkVisit) {
        this.fkVisit = fkVisit;
    }

    public void setFkVisit(String fkVisit) {
        this.fkVisit.add(fkVisit);
    }


  //KM
    public ArrayList getCptCodes() {
        return this.cptCodes;
    }

    public void setCptCodes(ArrayList cptCodes) {
        this.cptCodes = cptCodes;
    }

    public void setCptCodes(String cptCode) {
    	cptCodes.add(cptCode);
    }


    public ArrayList getEventCategory() {
        return this.eventCategory;
    }

    public void setEventCategory(ArrayList eventCategory) {
        this.eventCategory = eventCategory;
    }

    public void setEventCategory(String eveCategory) {
    	eventCategory.add(eveCategory);
    }

//JM:
    public ArrayList getEventSequences() {
        return this.sequences;
    }


    public void setEventSequences(String sequence) {
        sequences.add(sequence);
    }

    public void setEventSequences(ArrayList sequences) {
        this.sequences = sequences;
    }

  //JM: 11Apr2008

    public ArrayList getFlags() {
        return this.flags;
    }

    public void setFlag(String flag) {
    	flags.add(flag);
    }

    public void setFlag(ArrayList flags) {
        this.flags = flags;
    }

  //JM: 25Apr2008, added for, Enh. #C9
    public ArrayList getOfflineFlags() {
        return this.offlineFlags;
    }


    public void setOfflineFlag(String offlineFlag) {
    	offlineFlags.add(offlineFlag);
    }

    public void setOfflineFlag(ArrayList offlineFlags) {
        this.offlineFlags = offlineFlags;
    }


    public ArrayList getHideFlags() {
        return this.hideFlags;
    }


    public void setHideFlag(String hideFlag) {
    	hideFlags.add(hideFlag);
    }

    public void setHideFlag(ArrayList hideFlags) {
        this.hideFlags = hideFlags;
    }

    public ArrayList getEventSOSIds() {
        return this.eventSOSIds;
    }

    public void setEventSOSIds(String eventSOSId) {
    	eventSOSIds.add(eventSOSId);
    }

    public ArrayList getEventFacilityIds() {
        return this.eventFacilityIds;
    }

    public void setEventFacilityIds(String eventFacilityId) {
    	eventFacilityIds.add(eventFacilityId);
    }

    public ArrayList getEventCoverageTypes() {
        return this.eventCoverageTypes;
    }

    public void setEventCoverageTypes(String eventCoverageType) {
    	eventCoverageTypes.add(eventCoverageType);
    }

    public ArrayList getEventCoverageNotes() {
        return this.eventCoverageNotes;
    }

    public void setEventCoverageNotes(String coverageNotes) {
        eventCoverageNotes.add(coverageNotes);
    }

    //KV:SW-FIN2c
    public ArrayList getFacility() {
            return this.Facility;
    }

    public void setFacility(String Facilityid) {
   	Facility.add(Facilityid);

    }

    public void setFacility(ArrayList facility) {
		Facility = facility;
	}
  //KV:SW-FIN2b
    public ArrayList getEventCostDetails() {
		return eventCostDetails;
	}

	public void setEventCostDetails(ArrayList eventCostDetails) {
		this.eventCostDetails = eventCostDetails;
	}

	public void setEventCostDetails(String EventCostDetail) {
		eventCostDetails.add(EventCostDetail);
	}

	public ArrayList getBudgetTemplates() {
        return budgetTemplates;
    }

    public void setBudgetTemplates(String budgetTemplate) {
        budgetTemplates.add(budgetTemplate);
    }

    //JM: 27JAN2011: Enh- D-FIN9

    public ArrayList getStatCodes() {
    	return statCodes;
    }

    public void setStatCodes(ArrayList statCodes) {
    	statCodes = statCodes;
    }

    public void setStatCodes(String s) {
    	statCodes.add(s);
    }

    
    public ArrayList getCodeSubTypes() {
    	return codeSubTypes;
    }

    public void setCodeSubTypes(ArrayList subTypes) {
    	subTypes = subTypes;
    }

    public void setCodeSubTypes(String s) {
    	codeSubTypes.add(s);
    }
    
    public ArrayList getCodeDescriptions() {
    	return codeDescriptions;
    }

    public void setCodeDescriptions(ArrayList codeDescriptions) {
    	codeDescriptions = codeDescriptions;
    }

    public void setCodeDescriptions(String s) {
    	codeDescriptions.add(s);
    }
    
    //DFIN25-DAY0
    public ArrayList getAfterIntervalStrings() {
        return this.afterIntervalStrings;
    }
    public void setAfterIntervalStrings(ArrayList pList) {
        this.afterIntervalStrings.addAll(pList);
    }    
    
    public ArrayList getInsertAfter() {
		return insertAfter;
	}

	public void setInsertAfter(ArrayList insertAfter) {
		this.insertAfter = insertAfter;
	}
	public void setInsertAfter(int insertAfter) {
		this.insertAfter.add(insertAfter);
	}
	// Ankit PCAL-20282
	public ArrayList getEventLibraryType() {
        return this.eventLibraryType;
    }
    public void setEventLibraryType(String eventLibraryType) {
        this.eventLibraryType.add(eventLibraryType);
    }
    
    public Integer getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(Integer totalRecords) {
		this.totalRecords = totalRecords;
	}
    // ///////////////


	public int AddProtToStudy(String protocol_id, String study_id,
            String userId, String ipAdd,String type) {
        CallableStatement callablestatement = null;
        int newProtocol = -1;
        Connection connection = null;
        Rlog.debug("eventassoc",
                "In AddProtToStudy in EventAssocDao line number 1");
        try {
            connection = getConnection();
            callablestatement = connection
                    .prepareCall("{call sp_addprottostudy(?,?,?,?,?,?)}");
            callablestatement.setInt(1, StringUtil.stringToNum(protocol_id));
            callablestatement.setInt(2, StringUtil.stringToNum(study_id));
            callablestatement.setInt(3, StringUtil.stringToNum(userId));
            callablestatement.registerOutParameter(4, java.sql.Types.INTEGER);
            callablestatement.setString(5, ipAdd);
            callablestatement.setString(6, type);
            callablestatement.execute();
            newProtocol = callablestatement.getInt(4);
        } catch (SQLException sqlexception) {
            Rlog.fatal("eventassoc",
                    "In AddProtToStudy in Eventassoc line EXCEPTION IN calling the procedure"
                            + sqlexception);
        } finally {
            try {
                if (callablestatement != null)
                    callablestatement.close();
            } catch (Exception exception1) {
            }
            try {
                if (connection != null)
                    connection.close();
            } catch (Exception exception2) {
            }
        }
        return newProtocol;
    }



//JM: added new method getStudyProtsActive() for event/ visit milestones should take only 'Active' Calendar
    public void getStudyProtsActive(int i){

        PreparedStatement preparedstatement = null;
        Connection connection = null;
        try {
            connection = getConnection();

            //JM: #2454: Active calendars only ...
            String s = "select EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES, COST, COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI, FUZZY_PERIOD, MSG_TO, FK_CODELST_CALSTAT as codeStatus, DESCRIPTION, DISPLACEMENT FROM EVENT_ASSOC WHERE  CHAIN_ID= ? AND UPPER(EVENT_TYPE) IN('p','P') "
            	+ " and  FK_CODELST_CALSTAT=(select pk_codelst from sch_codelst where codelst_type='calStatStd' and CODELST_SUBTYP='A') and event_calassocto='P' ORDER BY NAME";


            preparedstatement = connection.prepareStatement(s);
            preparedstatement.setInt(1, i);
            ResultSet resultset = preparedstatement.executeQuery();

            for (; resultset.next(); setDisplacement(resultset
                    .getString("DISPLACEMENT"))) {

                String s1 = resultset.getString("EVENT_ID");
                s1 = s1.trim();
                Integer integer = new Integer(s1);
                setEvent_ids(integer);
                setChain_ids(resultset.getString("CHAIN_ID"));
                setEvent_types(resultset.getString("EVENT_TYPE"));
                setNames(resultset.getString("NAME"));
                setNotes(resultset.getString("NOTES"));
                setCosts(resultset.getString("COST"));
                setCost_descriptions(resultset.getString("COST_DESCRIPTION"));
                setDurations(resultset.getString("DURATION"));
                setUser_ids(resultset.getString("USER_ID"));
                setLinked_uris(resultset.getString("LINKED_URI"));
                setFuzzy_periods(resultset.getString("FUZZY_PERIOD"));
                setMsg_tos(resultset.getString("MSG_TO"));
                setDescription(resultset.getString("DESCRIPTION"));
                setStatCodes(resultset.getString("codeStatus"));//JM: 27JAN2011: enh - D-FIN9
            }

        } catch (SQLException sqlexception) {
            Rlog.fatal("eventassoc",
                    "EventAssocDao.getEventAssocValues EXCEPTION IN FETCHING FROM EventAssoc table"
                            + sqlexception);
        } finally {
            try {
                if (preparedstatement != null)
                    preparedstatement.close();
            } catch (Exception exception1) {
            }
            try {
                if (connection != null)
                    connection.close();
            } catch (Exception exception2) {
            }
        }

    }



    public void getStudyProts(int i)
    {
        PreparedStatement preparedstatement = null;
        Connection connection = null;
        try {
            connection = getConnection();            
            String s = "select EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES, COST, COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI, FUZZY_PERIOD, MSG_TO, FK_CODELST_CALSTAT as codeStatus, "
            	+ " (select trim(CODELST_SUBTYP) from sch_codelst where pk_codelst = FK_CODELST_CALSTAT ) CODELST_SUBTYP, "
            	+ " (select CODELST_DESC from sch_codelst where pk_codelst = FK_CODELST_CALSTAT ) CODELST_DESC,"
            	+ " DESCRIPTION, DISPLACEMENT FROM EVENT_ASSOC WHERE  CHAIN_ID= ? AND UPPER(EVENT_TYPE) IN('p','P') and event_calassocto='P' ORDER BY NAME";

            preparedstatement = connection.prepareStatement(s);
            preparedstatement.setInt(1, i);
            ResultSet resultset = preparedstatement.executeQuery();

            for (; resultset.next(); setDisplacement(resultset
                    .getString("DISPLACEMENT"))) {

                String s1 = resultset.getString("EVENT_ID");
                s1 = s1.trim();
                Integer integer = new Integer(s1);
                setEvent_ids(integer);
                setChain_ids(resultset.getString("CHAIN_ID"));
                setEvent_types(resultset.getString("EVENT_TYPE"));
                setNames(resultset.getString("NAME"));
                setNotes(resultset.getString("NOTES"));
                setCosts(resultset.getString("COST"));
                setCost_descriptions(resultset.getString("COST_DESCRIPTION"));
                setDurations(resultset.getString("DURATION"));
                setUser_ids(resultset.getString("USER_ID"));
                setLinked_uris(resultset.getString("LINKED_URI"));
                setFuzzy_periods(resultset.getString("FUZZY_PERIOD"));
                setMsg_tos(resultset.getString("MSG_TO"));
                setDescription(resultset.getString("DESCRIPTION")); //This is desc of the event
                setStatCodes(resultset.getString("codeStatus"));//JM: 27JAN2011: enh - D-FIN9
                
                setCodeSubTypes(resultset.getString("CODELST_SUBTYP"));
                
                setCodeDescriptions(resultset.getString("CODELST_DESC"));//This is codelst description
            }

        } catch (SQLException sqlexception) {
            Rlog.fatal("eventassoc",
                    "EventAssocDao.getEventAssocValues EXCEPTION IN FETCHING FROM EventAssoc table"
                            + sqlexception);
        } finally {
            try {
                if (preparedstatement != null)
                    preparedstatement.close();
            } catch (Exception exception1) {
            }
            try {
                if (connection != null)
                    connection.close();
            } catch (Exception exception2) {
            }
        }

    }

    public void getStudyAdminProts(int i,String status)
    {
        PreparedStatement preparedstatement = null;
        Connection connection = null;
        String s = "";
        try {
            connection = getConnection();

            if (status.equals("A"))
            {
               
            	s="select EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES, COST, COST_DESCRIPTION, DURATION, USER_ID,"
                	+ " LINKED_URI, FUZZY_PERIOD, MSG_TO, FK_CODELST_CALSTAT as codeStatus, "
                	+ " (select trim(CODELST_SUBTYP) from sch_codelst where pk_codelst = FK_CODELST_CALSTAT ) CODELST_SUBTYP, "
            		+ " (select CODELST_DESC from sch_codelst where pk_codelst = FK_CODELST_CALSTAT ) CODELST_DESC,"
                	+ " DESCRIPTION, DISPLACEMENT FROM EVENT_ASSOC WHERE "
                	+ " CHAIN_ID= ? AND UPPER(EVENT_TYPE) IN('p','P') and event_calassocto='S' "
                    + " and FK_CODELST_CALSTAT=(select pk_codelst from sch_codelst where codelst_type='calStatStd' "
                    + " and CODELST_SUBTYP='A') ORDER BY NAME";
            }
            else if (status.equals("ND")) //ND:- all calendars except deactivated,apr-04-11,Bk
            {
               
            	s="select EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES, COST, COST_DESCRIPTION, DURATION, USER_ID,"
                	+ " LINKED_URI, FUZZY_PERIOD, MSG_TO, FK_CODELST_CALSTAT as codeStatus, "
                	+ " (select trim(CODELST_SUBTYP) from sch_codelst where pk_codelst = FK_CODELST_CALSTAT ) CODELST_SUBTYP, "
            		+ " (select CODELST_DESC from sch_codelst where pk_codelst = FK_CODELST_CALSTAT ) CODELST_DESC,"
                	+ " DESCRIPTION, DISPLACEMENT FROM EVENT_ASSOC WHERE "
                	+ " CHAIN_ID= ? AND UPPER(EVENT_TYPE) IN('p','P') and event_calassocto='S' "
                    + " and FK_CODELST_CALSTAT in(select pk_codelst from sch_codelst where codelst_type='calStatStd' "
                    + " and CODELST_SUBTYP!='D') ORDER BY NAME";
            }
            else
            {
                s = "select EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES, COST, COST_DESCRIPTION, DURATION, USER_ID,"
                	+ " LINKED_URI, FUZZY_PERIOD, MSG_TO, FK_CODELST_CALSTAT as codeStatus, "
                	+ " (select trim(CODELST_SUBTYP) from sch_codelst where pk_codelst = FK_CODELST_CALSTAT ) CODELST_SUBTYP, "
            		+ " (select CODELST_DESC from sch_codelst where pk_codelst = FK_CODELST_CALSTAT ) CODELST_DESC,"
                	+ " DESCRIPTION, DISPLACEMENT FROM EVENT_ASSOC WHERE "
                	+ " CHAIN_ID= ? AND UPPER(EVENT_TYPE) IN('p','P') and event_calassocto='S'";

                if (! StringUtil.isEmpty(status))
                {

                	s = s + " and FK_CODELST_CALSTAT=(select pk_codelst from sch_codelst where codelst_type='calStatStd' and CODELST_SUBTYP ='" + status + "')";
                }

                s = s +	" ORDER BY NAME";

            }

            preparedstatement = connection.prepareStatement(s);
            preparedstatement.setInt(1, i);
            ResultSet resultset = preparedstatement.executeQuery();

            for (; resultset.next(); setDisplacement(resultset
                    .getString("DISPLACEMENT"))) {

                String s1 = resultset.getString("EVENT_ID");
                s1 = s1.trim();
                Integer integer = new Integer(s1);
                setEvent_ids(integer);
                setChain_ids(resultset.getString("CHAIN_ID"));
                setEvent_types(resultset.getString("EVENT_TYPE"));
                setNames(resultset.getString("NAME"));
                setNotes(resultset.getString("NOTES"));
                setCosts(resultset.getString("COST"));
                setCost_descriptions(resultset.getString("COST_DESCRIPTION"));
                setDurations(resultset.getString("DURATION"));
                setUser_ids(resultset.getString("USER_ID"));
                setLinked_uris(resultset.getString("LINKED_URI"));
                setFuzzy_periods(resultset.getString("FUZZY_PERIOD"));
                setMsg_tos(resultset.getString("MSG_TO"));                
                setDescription(resultset.getString("DESCRIPTION"));

                setStatCodes(resultset.getString("codeStatus"));
                setCodeSubTypes(resultset.getString("CODELST_SUBTYP"));
                
                setCodeDescriptions(resultset.getString("CODELST_DESC"));//This is codelst description

            }

        } catch (SQLException sqlexception) {
            Rlog.fatal("eventassoc",
                    "EventAssocDao.getEventAssocValues EXCEPTION IN FETCHING FROM EventAssoc table"
                            + sqlexception);
        } finally {
            try {
                if (preparedstatement != null)
                    preparedstatement.close();
            } catch (Exception exception1) {
            }
            try {
                if (connection != null)
                    connection.close();
            } catch (Exception exception2) {
            }
        }

    }


    public void getStudyProts(int studyId, int budgetId) {
        PreparedStatement preparedstatement = null;
        Connection connection = null;
        try {
            connection = getConnection();
            String s = "select EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES, COST, COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI, FUZZY_PERIOD, MSG_TO, FK_CODELST_CALSTAT as codeStatus, (select CODELST_DESC from sch_codelst where pk_codelst = FK_CODELST_CALSTAT ) CODELST_DESC, DESCRIPTION, DISPLACEMENT , event_calassocto FROM EVENT_ASSOC WHERE  CHAIN_ID= ? AND UPPER(EVENT_TYPE) IN('p','P')  "
                    + " and  event_id not in (select BGTCAL_PROTID from sch_bgtcal where FK_BUDGET= ? and BGTCAL_DELFLAG='N' and bgtcal_protid is not null ) "
                    + " ORDER BY NAME ";

            Rlog.debug("eventassoc", "getStudyProts: " + s);
            preparedstatement = connection.prepareStatement(s);
            preparedstatement.setInt(1, studyId);
            preparedstatement.setInt(2, budgetId);

            ResultSet resultset = preparedstatement.executeQuery();
            for (; resultset.next(); setDisplacement(resultset
                    .getString("DISPLACEMENT"))) {
                Rlog.debug("eventassoc", "getStudyProts line 1");
                String s1 = resultset.getString("EVENT_ID");
                s1 = s1.trim();
                Integer integer = new Integer(s1);
                setEvent_ids(integer);
                setChain_ids(resultset.getString("CHAIN_ID"));
                setEvent_types(resultset.getString("EVENT_TYPE"));
                setNames(resultset.getString("NAME"));
                setNotes(resultset.getString("NOTES"));
                setCosts(resultset.getString("COST"));
                setCost_descriptions(resultset.getString("COST_DESCRIPTION"));
                setDurations(resultset.getString("DURATION"));
                setUser_ids(resultset.getString("USER_ID"));
                setLinked_uris(resultset.getString("LINKED_URI"));
                setFuzzy_periods(resultset.getString("FUZZY_PERIOD"));
                setMsg_tos(resultset.getString("MSG_TO"));               
                setDescription(resultset.getString("DESCRIPTION"));
                setProtocolAssocTo(resultset.getString("event_calassocto"));
                setStatCodes(resultset.getString("codeStatus"));
                setCodeDescriptions(resultset.getString("CODELST_DESC"));
                

            }

        } catch (SQLException sqlexception) {
            Rlog.fatal("eventassoc",
                    "EventAssocDao.getEventAssocValues EXCEPTION IN FETCHING FROM EventAssoc table"
                            + sqlexception);
        } finally {
            try {
                if (preparedstatement != null)
                    preparedstatement.close();
            } catch (Exception exception1) {
            }
            try {
                if (connection != null)
                    connection.close();
            } catch (Exception exception2) {
            }
        }
    }

    // SV, 8/19/04, changed the name of deleteprotocolevents to what it does,
    // deleteduplicate.. to be consistent with EventAssocDao.
    public int DeleteDuplicateEvents(String chain_id) {
        CallableStatement cstmt = null;
        Integer chainid = new Integer(chain_id);
        int success = -1;

        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_deleteassocduplicate(?)}");
            cstmt.setInt(1, chainid.intValue());
            cstmt.execute();
            success = 1;

        }

        catch (SQLException ex) {
            Rlog
                    .fatal(
                            "eventassoc",
                            "In DeleteDuplicateEvents in EventAssoc line EXCEPTION IN calling the procedure"
                                    + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    // SV, 8/19/04, part of fix for bug #1602, original function now calls with
    // 0 for duration.
    public int DeleteProtocolEvents(String chain_id) {
        return (DeleteProtocolEvents(chain_id, 0));
    }

    // SV, 8/19/04, new method with duration parameter. If duration > 0, the
    // stored procedure will only delete those events w/ displacement >
    // duration.
    // Also, modified the stored procedure to receive duration with a default
    // value of 0, for duration, so it can still function with one (chain id)
    // argument.
    public int DeleteProtocolEvents(String chain_id, int duration) {

        CallableStatement cstmt = null;
        Integer chainid = new Integer(chain_id);
        int success = -1;

        // OracleCallableStatement cstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_deleteassocevents(?, ?)}");
            cstmt.setInt(1, chainid.intValue());
            cstmt.setInt(2, duration); // SV, 8/19
            cstmt.execute();
            success = 1;

        }

        catch (SQLException ex) {
            Rlog.fatal("eventAssoc",
                    "In DeleteProtocolEvents in EventAssoc line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    public int EditAssocEvent(String chain_id, int fromDisp, int toDisp) {

        CallableStatement cstmt = null;
        Integer chainid = new Integer(chain_id);
        int success = -1;

        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_editassocevent(?,?,?)}");
            cstmt.setInt(1, chainid.intValue());
            cstmt.setInt(2, fromDisp);
            cstmt.setInt(3, toDisp);
            cstmt.execute();
            success = 1;

        }

        catch (SQLException ex) {
            Rlog.fatal("eventassoc",
                    "In EditAssocEvent in EventAssoc line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    public void getAllProtAndEvents(int event_id) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String mysql = "select EVENT_ID, "
                    + "ORG_ID, "
                    + "CHAIN_ID, "
                    + "EVENT_TYPE, "
                    + "NAME, "
                    + "NOTES, "
                    + "COST, "
                    + "COST_DESCRIPTION, "
                    + "DURATION, "
                    + "USER_ID, "
                    + "LINKED_URI, "
                    + "FUZZY_PERIOD, "
                    + "MSG_TO, "
                    + "FK_CODELST_CALSTAT as codeStatus, "
                    + "DESCRIPTION, "
                    + "DISPLACEMENT, "
                    + "EVENT_CALASSOCTO "
                    + "FROM EVENT_ASSOC "
                    + "WHERE  CHAIN_ID= ? AND "
                    + "TRIM(UPPER(EVENT_TYPE)) IN('P','A') OR EVENT_ID = ?  ORDER BY CHAIN_ID,COST,EVENT_ID,NAME,DISPLACEMENT";

            Rlog.debug("eventassoc", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, event_id);
            pstmt.setInt(2, event_id);
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("eventassoc", "After execution" + rs.toString());

            while (rs.next()) {
                Rlog.debug("eventassoc", "getAllProtAndEvents line 1");
                String id = rs.getString("EVENT_ID");
                id = id.trim();
                Integer test1 = new Integer(id);
                setEvent_ids(test1);

                String id1 = rs.getString("ORG_ID");
                id1 = id1.trim();
                Integer test2 = new Integer(id1);
                setOrg_ids(test2);

                setChain_ids(rs.getString("CHAIN_ID"));
                setEvent_types(rs.getString("EVENT_TYPE"));
                setNames(rs.getString("NAME"));
                setNotes(rs.getString("NOTES"));
                setCosts(rs.getString("COST"));
                setCost_descriptions(rs.getString("COST_DESCRIPTION"));
                setDurations(rs.getString("DURATION"));

                setUser_ids(rs.getString("USER_ID"));

                setLinked_uris(rs.getString("LINKED_URI"));

                setFuzzy_periods(rs.getString("FUZZY_PERIOD"));

                setMsg_tos(rs.getString("MSG_TO"));
                setDescription(rs.getString("DESCRIPTION"));
                setDisplacement(rs.getString("DISPLACEMENT"));
                setStatCodes(rs.getString("codeStatus"));
                setProtocolAssocTo(rs.getString("EVENT_CALASSOCTO"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventassoc",
                    "EventAssocDao.getEventAssocValues EXCEPTION IN FETCHING FROM EventAssoc table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getFilteredEvents(int user_id, int protocol_id) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String mysql = "select EVENT_ID, " + "CHAIN_ID, " + "EVENT_TYPE, "
                    + "NAME, " + "NOTES, " + "COST, " + "COST_DESCRIPTION, "
                    + "DURATION, " + "USER_ID, " + "LINKED_URI, "
                    + "FUZZY_PERIOD, " + "MSG_TO, " 
                    + "DESCRIPTION, " + "DISPLACEMENT "

                    + "FROM EVENT_DEF " + "WHERE  TRIM(USER_ID)= ? AND "
                    + "TRIM(UPPER(EVENT_TYPE)) IN ('L','E') "
                    // + " AND EVENT_ID NOT IN (SELECT ORG_ID FROM EVENT_ASSOC
                    // WHERE EVENT_TYPE = 'A' AND CHAIN_ID = ?) "
                    + " ORDER BY CHAIN_ID,EVENT_ID";
            /*
             * + "MINUS select EVENT_ID, " + "CHAIN_ID, " + "EVENT_TYPE, " +
             * "NAME, " + "NOTES, " + "COST, " + "COST_DESCRIPTION, " +
             * "DURATION, " + "USER_ID, " + "LINKED_URI, " + "FUZZY_PERIOD, " +
             * "MSG_TO, " + "DESCRIPTION, " + "DISPLACEMENT " +
             * "FROM EVENT_DEF WHERE CHAIN_ID = ? AND EVENT_TYPE = 'A' " + "
             * ORDER BY CHAIN_ID,EVENT_ID" ;
             */

            Rlog.debug("eventassoc", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, user_id);
            // pstmt.setInt(2,protocol_id);

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("eventassoc", "After execution" + rs.toString());

            while (rs.next()) {
                Rlog.debug("eventassoc", "getFilteredEvents line 1");
                String id = rs.getString("EVENT_ID");
                id = id.trim();
                Integer test1 = new Integer(id);
                setEvent_ids(test1);
                setChain_ids(rs.getString("CHAIN_ID"));
                setEvent_types(rs.getString("EVENT_TYPE"));
                setNames(rs.getString("NAME"));
                setNotes(rs.getString("NOTES"));
                setCosts(rs.getString("COST"));
                setCost_descriptions(rs.getString("COST_DESCRIPTION"));
                setDurations(rs.getString("DURATION"));

                setUser_ids(rs.getString("USER_ID"));

                setLinked_uris(rs.getString("LINKED_URI"));

                setFuzzy_periods(rs.getString("FUZZY_PERIOD"));

                setMsg_tos(rs.getString("MSG_TO"));                
                setDescription(rs.getString("DESCRIPTION"));
                setDisplacement(rs.getString("DISPLACEMENT"));

            }

        } catch (SQLException ex) {
            Rlog.fatal("eventassoc",
                    "EventAssocDao.getEventAssocValues EXCEPTION IN FETCHING FROM EventAssoc table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getPrevStatus(int protocolId) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            Integer iValue = new Integer(0);
            String sValue = "";

            conn = getConnection();
            // SV, 8/27/04, fix for #1535, firstname lastname
            /*Bug# 10139 : Date 25 June 2012 : Yogendra Pratap */
            String mysql = "select a.pk_protstat as statusId,"
                    + "(select CODELST_DESC from sch_codelst where pk_codelst=a.FK_CODELST_CALSTAT) as codeStatus,"
                    + "to_char(a.protstat_dt,PKG_DATEUTIL.F_GET_DATEFORMAT) as statdate,"
                    + "a.protstat_note as notes ,"
                    + "b.USR_firstname||' '||b.usr_lastname as name "
                    + "from sch_protstat a,er_user b "
                    + "where a.protstat_by=b.pk_user "
                    + "and a.fk_event=? order by pk_protstat ";

            Rlog.debug("eventassoc", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, protocolId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setProtStatIds(rs.getString("statusId"));                
                setStatCodes(rs.getString("codeStatus"));
                setStatusChangedOns(rs.getString("statdate"));
                setNotes(rs.getString("NOTES"));
                setNames(rs.getString("name"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventassoc",
                    "EventAssocDao.getPrevStatus EXCEPTION IN FETCHING FROM Eventassoc table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    //KM-Query modified for Enh. #C7
    //JM: 27Nov2009: #4371: added "and event_type<>'U'": #unscheduled event added to the 'no interval visit' is not being added to the existing visits ...
    //Ankit: 26July2011: #PCAL-20282: added new column "EVENT_LIBRARY_TYPE".
    public void getProtSelectedEvents(int protocolId, String orderType, String orderBy) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {

            conn = getConnection();
            //Modify name, description, cptcode for bug 23909
            String mysql = "select EVENT_ID, " + "ORG_ID, " + "CHAIN_ID, "
                    + "EVENT_TYPE, " + "replace(replace(NAME,CHR(13),''),CHR(10),'') NAME, " + "NOTES, " + "COST, "
                    + "COST_DESCRIPTION, " + "DURATION, " + "USER_ID, "
                    + "LINKED_URI, " + "FUZZY_PERIOD, " + "MSG_TO, "
                    + "FK_CODELST_CALSTAT as codeStatus, " + "replace(replace(DESCRIPTION,CHR(13),''),CHR(10),'') DESCRIPTION, " + "DISPLACEMENT, "
                    + "replace(replace(EVENT_CPTCODE,CHR(13),''),CHR(10),'') EVENT_CPTCODE, EVENT_CATEGORY, offline_flag,(select site_name from er_site b  where b.pk_site = a.FACILITY_ID) as Facilityname, "
                    + "lst_cost(a.EVENT_ID) as eventcost, " 
                    + "(select CODELST_DESC from SCH_CODELST cl where cl.pk_codelst = EVENT_LIBRARY_TYPE ) as EVENT_LIBRARY_TYPE "
                    + " FROM EVENT_ASSOC a " + "WHERE  a.CHAIN_ID= ? AND "
                    + " a.FK_VISIT IS NULL and a.event_type<> 'U' "; //ORDER BY EVENT_ID"; //BK-DAY0 Feb09-2010; //Ankit: PCAL-20282



           if (! StringUtil.isEmpty(orderType) &&  !(StringUtil.isEmpty(orderBy)))  //KM-043008
                mysql = mysql +" ORDER BY " + orderBy +" "+orderType;
            else
            	mysql = mysql + " ORDER BY event_id ";


            Rlog.debug("eventassoc", mysql);

            pstmt = conn.prepareStatement(mysql);

            pstmt.setInt(1, protocolId);
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("eventassoc", "After execution" + rs.toString());

            while (rs.next()) {
                String id = rs.getString("EVENT_ID");
                Rlog.debug("eventassoc", id);
                id = id.trim();
                Integer test1 = new Integer(id);
                setEvent_ids(test1);

                String id1 = rs.getString("ORG_ID");
                id1 = id1.trim();
                Integer test2 = new Integer(id1);
                setOrg_ids(test2);
                setChain_ids(rs.getString("CHAIN_ID"));
                setEvent_types(rs.getString("EVENT_TYPE"));
                setNames(rs.getString("NAME"));
                setNotes(rs.getString("NOTES"));
                setCosts(rs.getString("COST"));
                setCost_descriptions(rs.getString("COST_DESCRIPTION"));
                setDurations(rs.getString("DURATION"));
                setUser_ids(rs.getString("USER_ID"));

                setLinked_uris(rs.getString("LINKED_URI"));

                setFuzzy_periods(rs.getString("FUZZY_PERIOD"));

                setMsg_tos(rs.getString("MSG_TO"));                
                setDescription(rs.getString("DESCRIPTION"));
                setDisplacement(rs.getString("DISPLACEMENT"));
                setCptCodes(rs.getString("EVENT_CPTCODE")); //KM
                setEventCategory(rs.getString("EVENT_CATEGORY"));//KM
                setOfflineFlag(rs.getString("offline_flag"));//JM: 19Jun2008
                setFacility(rs.getString("Facilityname"));//KV: SW-FIN2c
                setEventCostDetails(rs.getString("eventcost"));//KV: SW-FIN2b

                setStatCodes(rs.getString("codeStatus"));
                setEventLibraryType(rs.getString("EVENT_LIBRARY_TYPE")); //Ankit: PCAL-20282



            }

        } catch (SQLException ex) {
            Rlog.fatal("eventassoc",
                    "EventAssocDao.getEventAssocValues EXCEPTION IN FETCHING FROM EventAssoc table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * This methods copies the study calendar
     */

    public int copyStudyProtocol(String protocolId, String protocolName,
            String userId, String ipAdd,String accId, String calAssoc) {

        CallableStatement cstmt = null;
        int newProtocol = -1;
        int crfCopyStatus = 0;

        Connection conn = null;
        Connection erConn = null;

        Rlog.debug("eventassoc",
                "In copyStudyProtocol in EventassocDao line number 1");

        // copy the study calendar without copying the forms related to crfs
        // only free text forms are copied
        // due to dblinks, ditributed operations, forms would be copied
        // separately from er db
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_cprotinstudy(?,?,?,?,?,?,?)}");

            cstmt.setInt(1, StringUtil.stringToNum(protocolId));
            cstmt.setString(2, protocolName);
            cstmt.setInt(3, StringUtil.stringToNum(userId));
            cstmt.setString(4, ipAdd);

            cstmt.registerOutParameter(5, java.sql.Types.INTEGER);
            cstmt.setInt(6, StringUtil.stringToNum(accId));
            cstmt.setString(7, calAssoc);
            
            cstmt.execute();
            newProtocol = cstmt.getInt(5);

            // after making the copy of calendar and events, copy the linked
            // forms with the events
            // the new events are mapped with the old_events using org_id field
            // of event_assoc table
            if (newProtocol > 0) {
                try {
                    com.velos.eres.business.common.CommonDAO ercdao = new com.velos.eres.business.common.CommonDAO();
                    erConn = ercdao.getConnection();
                    cstmt = erConn
                            .prepareCall("{call SP_COPY_LNKCRFFORMS_FOREVENT(?,?,?,?)}");
                    cstmt.setInt(1, newProtocol);
                    cstmt.setInt(2, StringUtil.stringToNum(userId));
                    cstmt.setString(3, ipAdd);

                    cstmt.registerOutParameter(4, java.sql.Types.INTEGER);
                    cstmt.execute();
                    crfCopyStatus = cstmt.getInt(4);
                    Rlog
                            .debug(
                                    "eventassoc",
                                    "In CopyStudyProtocol in Eventassoc calling the procedure SP_COPY_LNKCRFFORMS crfCopyStatus "
                                            + crfCopyStatus);
                } catch (Exception e) {
                    Rlog
                            .fatal(
                                    "eventassoc",
                                    "In CopyStudyProtocol in Eventassoc EXCEPTION IN calling the procedure SP_COPY_LNKCRFFORMS"
                                            + e);
                }
            }

        }

        catch (Exception ex) {
            Rlog.fatal("eventassoc",
                    "In copyStudyProtocol in Eventassoc line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
                if (erConn != null)
                    erConn.close();

            } catch (Exception e) {
            }
        }
        return newProtocol;
    }

    public void getFilteredRangeEvents(int protocolId, int beginDisp,
            int endDisp) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            Integer iValue = new Integer(0);
            String sValue = "";

            conn = getConnection();
            String mysql = "select EVENT_ID,ORG_ID ," + "CHAIN_ID, "
                    + "EVENT_TYPE, " + "NAME, " + "NOTES, " + "COST, "
                    + "COST_DESCRIPTION, " + "DURATION, " + "USER_ID, "
                    + "LINKED_URI, " + "FUZZY_PERIOD, " + "MSG_TO, "
                    + "FK_CODELST_CALSTAT as codeStatus, " + "DESCRIPTION, " + "DISPLACEMENT, "
                    + " FK_VISIT " // SV, 9/27/04
                    + " FROM EVENT_ASSOC ";
            if (beginDisp == 1)
                mysql = mysql
                        + "WHERE  CHAIN_ID = ? AND ((DISPLACEMENT>= ? and DISPLACEMENT <= ?) or (DISPLACEMENT<0)) ";
            else
                mysql = mysql
                        + "WHERE  CHAIN_ID = ? AND (DISPLACEMENT>= ? and DISPLACEMENT <= ?) ";

            mysql = mysql + " ORDER BY EVENT_TYPE DESC ,COST,DISPLACEMENT";

            Rlog.debug("eventassoc", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, protocolId);
            pstmt.setInt(2, beginDisp);
            pstmt.setInt(3, endDisp);

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("eventassoc", "After execution" + rs.toString());

            while (rs.next()) {
                Rlog.debug("eventassoc", "getFilteredRangeEvents line 1");
                String id = rs.getString("EVENT_ID");
                id = id.trim();
                Integer test1 = new Integer(id);
                setEvent_ids(test1);

                String id1 = rs.getString("ORG_ID");
                id1 = id1.trim();
                Integer test2 = new Integer(id1);
                setOrg_ids(test2);

                setChain_ids(rs.getString("CHAIN_ID"));
                setEvent_types(rs.getString("EVENT_TYPE"));
                setNames(rs.getString("NAME"));
                setNotes(rs.getString("NOTES"));
                setCosts(rs.getString("COST"));
                setCost_descriptions(rs.getString("COST_DESCRIPTION"));
                setDurations(rs.getString("DURATION"));

                setUser_ids(rs.getString("USER_ID"));
                setLinked_uris(rs.getString("LINKED_URI"));

                setFuzzy_periods(rs.getString("FUZZY_PERIOD"));

                setMsg_tos(rs.getString("MSG_TO"));
                setDescription(rs.getString("DESCRIPTION"));



                setDisplacement(rs.getString("DISPLACEMENT"));

                // SV, 9/27/04, cal-enh-03-b
                setEventVisits(new Integer(rs.getInt("FK_VISIT")));


                setStatCodes(rs.getString("codeStatus"));

            }

        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "eventassoc",
                            "EventAssocDao.getFilteredRangeEvents EXCEPTION IN FETCHING FROM EventAssoc table"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public int RemoveProtEventInstance(int eventId, String whichTable) {

        CallableStatement cstmt = null;
        int newProtocol = -1;

        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_deleve(?,?,?)}");
            cstmt.setInt(1, eventId);
            cstmt.setString(2, whichTable);
            cstmt.registerOutParameter(3, java.sql.Types.INTEGER);
            cstmt.execute();
            newProtocol = cstmt.getInt(3);
        }

        catch (SQLException ex) {
            Rlog
                    .fatal(
                            "eventassoc",
                            "In RemoveProtEventInstance in EventAssoc line EXCEPTION IN calling the procedure"
                                    + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return newProtocol;
    }

    public void getProtRowEvents(int protocolId, int rownum, int beginDisp,
            int endDisp) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String mysql = "select EVENT_ID,ORG_ID ,"
                    + "CHAIN_ID, "
                    + "EVENT_TYPE, "
                    + "NAME, "
                    + "NOTES, "
                    + "COST, "
                    + "COST_DESCRIPTION, "
                    + "DURATION, "
                    + "USER_ID, "
                    + "LINKED_URI, "
                    + "FUZZY_PERIOD, "
                    + "MSG_TO, "
                    + "FK_CODELST_CALSTAT as codeStatus,"
                    + "DESCRIPTION, "
                    + "DISPLACEMENT, "
                    + "FK_VISIT "
                    + "FROM EVENT_ASSOC "
                    + "WHERE  CHAIN_ID = ? AND (DISPLACEMENT>= ? and DISPLACEMENT <= ? AND COST = ? ) "
                    + " ORDER BY EVENT_TYPE DESC ,COST,DISPLACEMENT";

            Rlog.debug("eventassoc", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, protocolId);
            pstmt.setInt(2, beginDisp);
            pstmt.setInt(3, endDisp);
            pstmt.setInt(4, rownum);

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("eventassoc", "After execution" + rs.toString());

            while (rs.next()) {
                Rlog.debug("eventassoc", "getProtRowEvents line 1");
                String id = rs.getString("EVENT_ID");
                id = id.trim();
                Integer test1 = new Integer(id);
                setEvent_ids(test1);

                String id1 = rs.getString("ORG_ID");
                id1 = id1.trim();
                Integer test2 = new Integer(id1);
                setOrg_ids(test2);

                setChain_ids(rs.getString("CHAIN_ID"));
                setEvent_types(rs.getString("EVENT_TYPE"));
                setNames(rs.getString("NAME"));
                setNotes(rs.getString("NOTES"));
                setCosts(rs.getString("COST"));
                setCost_descriptions(rs.getString("COST_DESCRIPTION"));
                setDurations(rs.getString("DURATION"));
                setUser_ids(rs.getString("USER_ID"));

                setLinked_uris(rs.getString("LINKED_URI"));

                setFuzzy_periods(rs.getString("FUZZY_PERIOD"));

                setMsg_tos(rs.getString("MSG_TO"));                
                setDescription(rs.getString("DESCRIPTION"));
                setDisplacement(rs.getString("DISPLACEMENT"));
                this.setFkVisit((rs.getString("FK_VISIT")));

                setStatCodes(rs.getString("codeStatus"));

            }

        } catch (SQLException ex) {
            Rlog.fatal("eventassoc",
                    "EventAssocDao.getProtRowEvents EXCEPTION IN FETCHING FROM EventAssoc table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // ///////////

    public void fetchCalTemplate(String id) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        String sql = null;
        int count=1;
        try {
            conn = getConnection();
            Rlog.debug("eschreports", "line 2");
            /*String mysql = "select event_id,name, " + "displacement "
                    + "from event_assoc "
                    + "where chain_id = ? and displacement != 0 "
                    + "order by displacement,org_id";*/
            String mysql=" SELECT  DISTINCT Pkg_Protocol_Visit.group_events(chain_id,displacement) as name " +
                    ",displacement  FROM EVENT_ASSOC  WHERE chain_id = ? AND displacement != 0 " +
                    "ORDER BY displacement" ;

            Rlog.debug("eschreports", "value is" + mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, StringUtil.stringToNum(id));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                /*String eventId = rs.getString("EVENT_ID");
                eventId = eventId.trim();
                Integer test1 = new Integer(eventId);
                setEvent_ids(test1);*/

                setEvent_ids(count);
                setNames(rs.getString("name"));
                setDisplacement(rs.getString("displacement"));
                count++;
            }

            Rlog.debug("eschreports", "line 5");

            // /till here

            mysql = "select name,duration from event_assoc where event_id=?";
            pstmt = conn.prepareStatement(mysql);

            pstmt.setInt(1, StringUtil.stringToNum(id));

            rs = pstmt.executeQuery();
            while (rs.next()) {
                setProtName(rs.getString("name"));
                setProtDuration(rs.getString("duration"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("eschreports",
                    "EschReportsDao.fetchEschReportsData EXCEPTION IN FETCHING "
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    public int getMaxCost(String chainId) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        int cost = 0;
        try {
            conn = getConnection();
            String mysql = "select nvl(max(COST),0) as cost "
                    + "FROM EVENT_ASSOC "
                    + "WHERE  CHAIN_ID= ? and event_id != ? ";

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, StringUtil.stringToNum(chainId));
            pstmt.setInt(2, StringUtil.stringToNum(chainId));
            rs = pstmt.executeQuery();
            rs.next();
            cost = rs.getInt("cost");
        } catch (SQLException ex) {
            Rlog.fatal("eventassoc", "EventAssocDao.GetMaxCost" + ex);
            return -2;
        }finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return cost;
    }

    /**
     * Delete calendar associated with the Study
     *
     * @param studyId
     * @param calendarId
     * @returns 0:successful, -1:error, -2 if patients enrolled in the calendar,
     *          -3 if budget linked to the calendar
     */

    public int studyCalendarDelete(int studyId, int calendarId) {
        int output = 0;
        CallableStatement callablestatement = null;
        Connection connection = null;
        try {
            connection = getConnection();
            callablestatement = connection
                    .prepareCall("{call PKG_GENSCH.SP_DELETE_STUDY_CALENDAR(?,?,?)}");
            callablestatement.setInt(1, studyId);
            callablestatement.setInt(2, calendarId);
            callablestatement.registerOutParameter(3, java.sql.Types.INTEGER);
            callablestatement.execute();
            output = callablestatement.getInt(3);
        } catch (SQLException sqlexception) {
            Rlog.fatal("eventassoc",
                    "ERROR in studyCalendarDelete in EventAsscoDao calling the procedure"
                            + sqlexception);
        } finally {
            try {
                if (callablestatement != null)
                    callablestatement.close();
            } catch (Exception exception1) {
            }
            try {
                if (connection != null)
                    connection.close();
            } catch (Exception exception2) {
            }
        }
        return output;
    }

    public void getStudyProtsWithAlnotStat(int studyId) {
        PreparedStatement preparedstatement = null;
        Connection connection = null;
        try {
            connection = getConnection();
            String s = "select EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME,  FK_CODELST_CALSTAT as codeStatus, (select trim(CODELST_SUBTYP) from sch_codelst where pk_codelst = FK_CODELST_CALSTAT ) CODELST_SUBTYP, NVL(ORIG_CAL,0) ORIG_CAL  FROM EVENT_ASSOC  WHERE  CHAIN_ID= ? AND UPPER(EVENT_TYPE) IN('p','P') and  FK_CODELST_CALSTAT=(select pk_codelst from sch_codelst where codelst_type='calStatStd' and CODELST_SUBTYP='A')  ORDER BY NAME";
            Rlog.debug("eventassoc", "getStudyProtsWithA1notStat:" + s);
            preparedstatement = connection.prepareStatement(s);

            preparedstatement.setInt(1, studyId);

            ResultSet resultset = preparedstatement.executeQuery();
            Rlog.debug("eventassoc", "After execution" + resultset.toString());
            while (resultset.next()) {

                String s1 = resultset.getString("EVENT_ID");
                s1 = s1.trim();
                Integer integer = new Integer(s1);
                setEvent_ids(integer);
                setChain_ids(resultset.getString("CHAIN_ID"));
                setEvent_types(resultset.getString("EVENT_TYPE"));
                setNames(resultset.getString("NAME"));
                setCosts(String.valueOf(resultset.getInt("ORIG_CAL"))); // sorry,
                // update
                // flag
                // stored
                // in
                // orig_cal
                // is is
                // set
                // in
                // cost
                
                setStatCodes(resultset.getString("CODELST_SUBTYP"));
                

            }

        } catch (SQLException sqlexception) {
            Rlog.fatal("eventassoc",
                    "EventAssocDao.getEventAssocValues EXCEPTION IN FETCHING FROM EventAssoc table"
                            + sqlexception);
        } finally {
            try {
                if (preparedstatement != null)
                    preparedstatement.close();
            } catch (Exception exception1) {
            }
            try {
                if (connection != null)
                    connection.close();
            } catch (Exception exception2) {
            }
        }
    }

    // //////////////////////

    public void setDefStudyAlnot(String studyid, String protocolid,
            String creator, String ipAdd) {
        CallableStatement callablestatement = null;

        Connection connection = null;

        Integer nullInt = null;
        Rlog.debug("eventassoc",
                "In setDefStudyAlnot  in EventAssocDao line number 1");
        try {
            connection = getConnection();
            callablestatement = connection
                    .prepareCall("{call pkg_alnot.set_alnot(?,?,?,?,?,?,?)}");

            callablestatement.setString(1, null);
            callablestatement.setInt(2, StringUtil.stringToNum(studyid));
            callablestatement.setInt(3, StringUtil.stringToNum(protocolid));
            callablestatement.setString(4, "G");
            callablestatement.setInt(5, StringUtil.stringToNum(creator));
            callablestatement.setString(6, null);
            callablestatement.setString(7, ipAdd);
            callablestatement.execute();

        } catch (SQLException sqlexception) {
            Rlog.fatal("eventassoc",
                    "In setDefStudyAlnot in Eventassoc line EXCEPTION IN calling the procedure"
                            + sqlexception);
        } finally {
            try {
                if (callablestatement != null)
                    callablestatement.close();
            } catch (Exception exception1) {
            }
            try {
                if (connection != null)
                    connection.close();
            } catch (Exception exception2) {
            }
        }

    }

    public void getProtSelectedAndGroupedEvents(int protocolId, boolean isExport, int finDetRight,String eventName) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
        	conn = getConnection();
            StringBuffer sql = new StringBuffer();
            StringBuffer sql1 = new StringBuffer();
            String costs = "";
            
            ArrayList eventCosts=new ArrayList();
            ResultSet rs = null;
			if(!"".equalsIgnoreCase(eventName)){
			sql1.append("SELECT COST ")
            .append(" FROM EVENT_ASSOC a ")
            .append(" WHERE a.CHAIN_ID = ? ")
            .append(" AND a.event_type = 'A' ")
            .append(" and (LOWER(a.NAME) LIKE LOWER('%"+eventName+"%')) ")
            .append(" AND a.fk_visit is null ");
            
            pstmt = conn.prepareStatement(sql1.toString());
            pstmt.setInt(1, protocolId);
            pstmt.setFetchSize(1000);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	eventCosts.add(Integer.parseInt(StringUtil.trueValue(rs.getString("COST")).trim()));
            }
            costs= eventCosts.toString().replace("[", "").replace("]", "")
            .replace(", ", ",");
			 }//end of if
			pstmt = null;
			rs = null;
			if(!"".equalsIgnoreCase(eventName) && !"".equalsIgnoreCase(costs)){
            sql.append(" SELECT a.EVENT_ID AS INTERSECTION_ID, b.event_id,b.name as event_name, a.ORG_ID, ")
            .append(" a.CHAIN_ID, a.EVENT_TYPE, replace(replace(a.NAME,CHR(13),''),CHR(10),'') NAME, a.COST, a.DESCRIPTION, 'E' AS flag, ")
            .append(" a. FK_VISIT, 0 AS VISIT_NO, 0 AS DISPLACEMENT, a.EVENT_CATEGORY, a.hide_flag, a.OFFLINE_FLAG, a.FK_CODELST_COVERTYPE, a.FK_CODELST_CALSTAT, a.COVERAGE_NOTES, replace(replace(b.EVENT_CPTCODE,CHR(13),''),CHR(10),'') EVENT_CPTCODE ");

            sql.append(" FROM EVENT_ASSOC a left join EVENT_ASSOC b on a.chain_id =b.chain_id and a.cost=b.cost AND b.FK_VISIT  IS NULL AND b.EVENT_TYPE = a.EVENT_TYPE ")
            .append(" WHERE a.CHAIN_ID = ?  ")
            .append(" AND a.event_type = 'A' ");
          
            	sql.append(" AND a.COST in ("+costs.toString()+")");
            sql.append(" ORDER BY Cost,FK_VISIT nulls first ");
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, protocolId);
            pstmt.setFetchSize(1000);
             rs = pstmt.executeQuery();
			}else if("".equalsIgnoreCase(eventName) && "".equalsIgnoreCase(costs)){
				 //Modify name, cptcode for bug 23909
				 sql.append(" SELECT a.EVENT_ID AS INTERSECTION_ID, b.event_id,b.name as event_name, a.ORG_ID, ")
		            .append(" a.CHAIN_ID, a.EVENT_TYPE, replace(replace(a.NAME,CHR(13),''),CHR(10),'') NAME, a.COST, a.DESCRIPTION, 'E' AS flag, ")
		            .append(" a. FK_VISIT, 0 AS VISIT_NO, 0 AS DISPLACEMENT, a.EVENT_CATEGORY, a.hide_flag, a.OFFLINE_FLAG, a.FK_CODELST_COVERTYPE, a.FK_CODELST_CALSTAT, a.COVERAGE_NOTES, replace(replace(b.EVENT_CPTCODE,CHR(13),''),CHR(10),'') EVENT_CPTCODE ");

		            sql.append(" FROM EVENT_ASSOC a left join EVENT_ASSOC b on a.chain_id =b.chain_id and a.cost=b.cost AND b.FK_VISIT  IS NULL AND b.EVENT_TYPE = a.EVENT_TYPE ")
		            .append(" WHERE a.CHAIN_ID = ?  ")
		            .append(" AND a.event_type = 'A' ");
		          
		            	
		            sql.append(" ORDER BY Cost,FK_VISIT nulls first ");
		          
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, protocolId);
            pstmt.setFetchSize(1000);
            rs = pstmt.executeQuery();
			}
			if(rs!=null){
            while (rs.next()) {
            	setNew_event_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("INTERSECTION_ID")).trim()));
                setEvent_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("EVENT_ID")).trim()));
                setOrg_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("ORG_ID")).trim()));
                setChain_ids(rs.getString("CHAIN_ID"));
                setEvent_types(rs.getString("EVENT_TYPE"));
                setNames(rs.getString("NAME"));
                setCosts(rs.getString("COST"));
                setDescription(rs.getString("DESCRIPTION"));
                setFlag(rs.getString("flag"));
                String sValue = rs.getString("FK_VISIT");
                if (sValue == null) { sValue = "0"; }
                setEventVisits(new Integer (sValue.trim()));
                setDisplacement(rs.getString("DISPLACEMENT"));
                setEventCategory(rs.getString("EVENT_CATEGORY"));
                setHideFlag(rs.getString("hide_flag"));
                setOfflineFlag(rs.getString("OFFLINE_FLAG"));
                setEventCoverageTypes(rs.getString("FK_CODELST_COVERTYPE"));
                setEventCoverageNotes(rs.getString("COVERAGE_NOTES"));               
                setCptCodes(rs.getString("EVENT_CPTCODE"));
                setStatCodes(rs.getString("FK_CODELST_CALSTAT"));

            }}
        } catch (Exception ex) {
            Rlog.fatal("eventassoc",
                       "EventAssocDao.getProtSelectedAndGroupedEvents: "+ ex);
        } finally {
            try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
            try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
    }
    
    
    public void getProtSelectedAndGroupedEvents(int protocolId, boolean isExport, int finDetRight) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            StringBuffer sql = new StringBuffer();
            sql
            .append(" SELECT a.EVENT_ID AS INTERSECTION_ID, b.event_id,b.name as event_name, a.ORG_ID, ")
            .append(" (select md_modelementdata from er_moredetails where md_modelementpk = (select pk_codelst from er_codelst where codelst_type = 'evtaddlcode' and codelst_subtyp = 'SVC_CD') and fk_modpk = a.event_id) as servicecode,")
            .append(" (select md_modelementdata from er_moredetails where md_modelementpk = (select pk_codelst from er_codelst where codelst_type = 'evtaddlcode' and codelst_subtyp = 'PRS_CODE') and fk_modpk = a.event_id) as prscode,")
            .append(" a.CHAIN_ID, a.EVENT_TYPE, replace(replace(a.NAME,CHR(13),''),CHR(10),'') NAME, a.COST, a.DESCRIPTION, 'E' AS flag, ")
            .append(" a. FK_VISIT, 0 AS VISIT_NO, 0 AS DISPLACEMENT, a.EVENT_CATEGORY, a.hide_flag, a.OFFLINE_FLAG, a.FK_CODELST_COVERTYPE, a.FK_CODELST_CALSTAT, a.COVERAGE_NOTES, replace(replace(b.EVENT_CPTCODE,CHR(13),''),CHR(10),'') EVENT_CPTCODE ");

            if (isExport){
            	String accessYN = "0";
            	if (StringUtil.isAccessibleFor(finDetRight, 'V')) {
            		accessYN ="-1";
	        	}

            	if (accessYN.equals("0")){
            		sql.append(", '' as EVENT_ADDCODE ");
            	}else{
            		sql.append(", esch.lst_additionalcodes_financial(a.event_id,'evtaddlcode',"+accessYN+") as EVENT_ADDCODE ");
            	}
        	}

            sql.append(" FROM EVENT_ASSOC a left join EVENT_ASSOC b on a.chain_id =b.chain_id and a.cost=b.cost AND b.FK_VISIT  IS NULL AND b.EVENT_TYPE = a.EVENT_TYPE ")
            .append(" WHERE a.CHAIN_ID = ? AND a.event_type = 'A' ");
            
            sql.append(" ORDER BY COST,FK_VISIT nulls first ");
 
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, protocolId);
            pstmt.setFetchSize(1000);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	setNew_event_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("INTERSECTION_ID")).trim()));
                setEvent_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("EVENT_ID")).trim()));
                setOrg_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("ORG_ID")).trim()));
                setChain_ids(rs.getString("CHAIN_ID"));
                setEvent_types(rs.getString("EVENT_TYPE"));
                setNames(rs.getString("NAME"));
                setCosts(rs.getString("COST"));
                setDescription(rs.getString("DESCRIPTION"));
                setFlag(rs.getString("flag"));
                String sValue = rs.getString("FK_VISIT");
                if (sValue == null) { sValue = "0"; }
                setEventVisits(new Integer (sValue.trim()));
                setDisplacement(rs.getString("DISPLACEMENT"));
                setEventCategory(rs.getString("EVENT_CATEGORY"));
                setHideFlag(rs.getString("hide_flag"));
                setOfflineFlag(rs.getString("OFFLINE_FLAG"));
                setEventCoverageTypes(rs.getString("FK_CODELST_COVERTYPE"));
                setEventCoverageNotes(rs.getString("COVERAGE_NOTES"));               
                setCptCodes(rs.getString("EVENT_CPTCODE"));
                setStatCodes(rs.getString("FK_CODELST_CALSTAT"));
                this.setNewPRSCodes(rs.getString("prscode"));
                this.setNewServiceCodes(rs.getString("servicecode"));
                if (isExport){
                	setCodeDescriptions(rs.getString("EVENT_ADDCODE"));
                }
            }
        } catch (Exception ex) {
            Rlog.fatal("eventassoc",
                       "EventAssocDao.getProtSelectedAndGroupedEvents: "+ ex);
        } finally {
            try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
            try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
    }
    
    public void getProtSelectedAndGroupedEvents(int protocolId, boolean isExport, int finDetRight,int initEventSize,int limitEventSize,String visitids,String eventName) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs=null;
        try {
            conn = getConnection();
            StringBuffer sql = new StringBuffer();
            StringBuffer sql1 = new StringBuffer();
            StringBuffer mainSql = new StringBuffer();
            StringBuffer sql2 = new StringBuffer();
            pstmt = null;
            rs = null;
            mainSql.append("SELECT count(*)")
            .append(" FROM EVENT_ASSOC a ")
            .append(" WHERE a.CHAIN_ID = ? ")
            .append(" AND a.event_type = 'A' ");
            if(!"".equalsIgnoreCase(eventName))
            mainSql.append(" and (LOWER(NAME) LIKE LOWER('%"+eventName+"%')) ");
            mainSql.append(" AND a.fk_visit is null");
            pstmt = conn.prepareStatement(mainSql.toString());
            pstmt.setInt(1, protocolId);
            rs = pstmt.executeQuery();
            while (rs.next()){
            	totalRecords=rs.getInt("count(*)");
            }
            pstmt = null;
            rs = null;
            ArrayList eventCosts=new ArrayList();
            sql1.append("SELECT * from (SELECT COST ");
            sql1.append(" FROM EVENT_ASSOC a ")
            .append(" WHERE a.CHAIN_ID = ? ")
            .append(" AND a.event_type = 'A' ");
            if(!"".equalsIgnoreCase(eventName))
            sql1.append(" and (LOWER(NAME) LIKE LOWER('%"+eventName+"%')) ");
            sql1.append(" AND a.fk_visit is null ORDER BY Cost) ")
            .append(" where ROWNUM <= ? ");
            pstmt = conn.prepareStatement(sql1.toString(),ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            pstmt.setInt(1, protocolId);
            pstmt.setInt(2,limitEventSize );
            pstmt.setFetchSize(1000);
            rs = pstmt.executeQuery();
            rs.absolute(initEventSize);
            do {
            	eventCosts.add(Integer.parseInt(StringUtil.trueValue(rs.getString("COST")).trim()));
            }while (rs.next());
            String costs = eventCosts.toString().replace("[", "").replace("]", "")
            .replace(", ", ",");
            
            pstmt = null;
            rs = null;
            sql2.append(" SELECT a.EVENT_ID AS INTERSECTION_ID, b.event_id,b.name as event_name, a.ORG_ID, ")
            .append(" a.CHAIN_ID, a.EVENT_TYPE, replace(replace(a.NAME,CHR(13),''),CHR(10),'') NAME, a.COST, a.DESCRIPTION, 'E' AS flag, ")
            .append(" a.FK_VISIT, 0 AS VISIT_NO, 0 AS DISPLACEMENT, a.EVENT_CATEGORY, a.hide_flag, a.OFFLINE_FLAG, ")
            .append(" a.FK_CODELST_COVERTYPE, a.FK_CODELST_CALSTAT, a.COVERAGE_NOTES, replace(replace(b.EVENT_CPTCODE,CHR(13),''),CHR(10),'') EVENT_CPTCODE  ");

            sql2.append(" FROM EVENT_ASSOC a left join EVENT_ASSOC b on a.chain_id =b.chain_id and a.cost=b.cost AND b.FK_VISIT  IS NULL AND b.EVENT_TYPE = a.EVENT_TYPE ")
            .append(" WHERE a.CHAIN_ID = ? AND a.event_type = 'A' ")
            .append(" and (a.fk_visit in("+visitids+") or a.fk_visit is null) ")
            .append(" AND a.COST in ("+costs+") ORDER BY Cost, FK_VISIT nulls first ");
            
            pstmt = conn.prepareStatement(sql2.toString());
            pstmt.setInt(1, protocolId);
            pstmt.setFetchSize(1000);
            rs = pstmt.executeQuery();
             while (rs.next()) {
            	setNew_event_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("INTERSECTION_ID")).trim()));
                setEvent_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("EVENT_ID")).trim()));
                setOrg_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("ORG_ID")).trim()));
                setChain_ids(rs.getString("CHAIN_ID"));
                setEvent_types(rs.getString("EVENT_TYPE"));
                setNames(rs.getString("NAME"));
                setCosts(rs.getString("COST"));
                setDescription(rs.getString("DESCRIPTION"));
                setFlag(rs.getString("flag"));
                String sValue = rs.getString("FK_VISIT");
                if (sValue == null) { sValue = "0"; }
                setEventVisits(new Integer (sValue.trim()));
                setDisplacement(rs.getString("DISPLACEMENT"));
                setEventCategory(rs.getString("EVENT_CATEGORY"));
                setHideFlag(rs.getString("hide_flag"));
                setOfflineFlag(rs.getString("OFFLINE_FLAG"));
                setEventCoverageTypes(rs.getString("FK_CODELST_COVERTYPE"));
                setEventCoverageNotes(rs.getString("COVERAGE_NOTES"));               
                setCptCodes(rs.getString("EVENT_CPTCODE"));
                setStatCodes(rs.getString("FK_CODELST_CALSTAT"));
            }
        } catch (Exception ex) {
            Rlog.fatal("eventassoc",
                       "EventAssocDao.getProtSelectedAndGroupedEvents: "+ ex);
        } finally {
            try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
            try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
    }
    
    /*YK :: DFIN12 FIX FOR BUG#5954-- RESOLVES  #5947 #5943 BUGS ALSO*/
    /**
     * This method retrieves all the scheduled events of a calendar with
     * the Visits of a Calendar
     *
     * @param protocolId
     */
    public void getEventVisitMileStoneGrid(int protocolId) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            StringBuffer sql = new StringBuffer();
            //Modify NAME for the bug id 23909
            sql
            .append(" SELECT 0 AS EVENT_ID,0 AS NEW_EVENT_ID, 0 AS ORG_ID, fk_protocol AS CHAIN_ID, ")
            .append(" '' AS EVENT_TYPE, visit_name AS NAME, 0 AS COST, DESCRIPTION AS DESCRIPTION, ")
            .append(" 'V' AS flag, pk_protocol_visit AS FK_VISIT, VISIT_NO, DISPLACEMENT, ")
            .append(" '' AS EVENT_CATEGORY, hide_flag, 0 as OFFLINE_FLAG, 0 AS FK_CODELST_COVERTYPE, ")
            .append(" '' AS COVERAGE_NOTES, null as FK_CODELST_CALSTAT ")
            .append(" FROM sch_protocol_visit WHERE fk_protocol = ? ")
            .append(" UNION ")
            .append(" SELECT ")
            .append(" (select EVENT_ID from EVENT_ASSOC where CHAIN_ID = ? ")
            .append("  and COST = a.COST and FK_VISIT IS NULL), EVENT_ID AS NEW_EVENT_ID,")       
            .append(" ORG_ID, CHAIN_ID, EVENT_TYPE, replace(replace(NAME,CHR(13),''),CHR(10),'') NAME, COST, DESCRIPTION, ")
            .append(" 'E' AS flag, FK_VISIT, 0 AS VISIT_NO, 0 AS DISPLACEMENT, ")
            .append(" EVENT_CATEGORY, hide_flag, OFFLINE_FLAG, FK_CODELST_COVERTYPE, ")
            .append(" COVERAGE_NOTES, FK_CODELST_CALSTAT ")
            .append(" FROM EVENT_ASSOC a ")
            .append(" WHERE a.CHAIN_ID = ?  ")
            .append(" AND a.event_type <> 'U' ")
            .append(" ORDER BY flag DESC, VISIT_NO, DISPLACEMENT, NAME ")
            ;
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, protocolId);
            pstmt.setInt(2, protocolId);
            pstmt.setInt(3, protocolId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setEvent_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("EVENT_ID")).trim()));
                setNew_event_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("NEW_EVENT_ID")).trim()));
                setOrg_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("ORG_ID")).trim()));
                setChain_ids(rs.getString("CHAIN_ID"));
                setEvent_types(rs.getString("EVENT_TYPE"));
                setNames(rs.getString("NAME"));
                setCosts(rs.getString("COST"));
                setDescription(rs.getString("DESCRIPTION"));
                setFlag(rs.getString("flag"));
                String sValue = rs.getString("FK_VISIT");
                if (sValue == null) { sValue = "0"; }
                setEventVisits(new Integer (sValue.trim()));
                setDisplacement(rs.getString("DISPLACEMENT"));
                setEventCategory(rs.getString("EVENT_CATEGORY"));
                setHideFlag(rs.getString("hide_flag"));
                setOfflineFlag(rs.getString("OFFLINE_FLAG"));
                setEventCoverageTypes(rs.getString("FK_CODELST_COVERTYPE"));
                setEventCoverageNotes(rs.getString("COVERAGE_NOTES"));               
                
                setStatCodes(rs.getString("FK_CODELST_CALSTAT"));
                
                
            }
        } catch (Exception ex) {
            Rlog.fatal("eventassoc",
                       "EventAssocDao.getEventVisitMileStoneGrid: "+ ex);
        } finally {
            try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
            try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
    }

    /**
     * This method retrieves all the scheduled events of a calendar with
     * displacement in weeks and days
     *
     * @param calendarId
     */

    public void getAllProtSelectedEvents(int protocolId, String visitId, String evtName) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            Integer iValue = new Integer(0);
            String sValue = "";

            conn = getConnection();

            StringBuffer sbSql = new StringBuffer();

          //JM: 11April2008: After discussing with RKS:
       //D-FIN-25-DAY0
       //BK-FIX-5979-MAR-28-2011
            		//KM:For No Interval denfined visit
		            sbSql.append(" SELECT decode(NUM_DAYS,0,NUM_DAYS,DISPLACEMENT) as DISPLACEMENT, " + "0 as EVENT_ID, " + "0 as ORG_ID, "
		            + " 0 as CHAIN_ID, " + " '' as EVENT_TYPE, " + " visit_name  as NAME, " + " '' as NOTES, "
		            + " 0 as COST, " + " '' as COST_DESCRIPTION, " + "0 as DURATION, "
		            + " 0 as USER_ID, " + " ''  as LINKED_URI, " + "''  as FUZZY_PERIOD, "
		            + " ''  as MSG_TO, " + " null as codeStatus," + " DESCRIPTION  as DESCRIPTION, "
		            + " NUM_MONTHS MONTH , "
	                + " NUM_WEEKS WEEK , "
	                + " NUM_DAYS DAY, " 
	                + " INSERT_AFTER,"
	                + " INSERT_AFTER_INTERVAL,"
	                + " INSERT_AFTER_INTERVAL_UNIT,"
		            + " 'V' as flag, pk_protocol_visit as FK_VISIT , '' as EVENT_CATEGORY, 0 as EVENT_SEQUENCE, offline_flag, hide_flag, "
		            + " '' as Facilityname, "
		            + " '' as eventcost "
		            + " FROM sch_protocol_visit "
		            + " WHERE  fk_protocol= ? ");
		            if (!visitId.equals("")){
		            		sbSql.append(" and pk_protocol_visit="+visitId);
		            }
		            sbSql.append(" union"
		            + " SELECT DECODE((SELECT NUM_DAYS FROM SCH_PROTOCOL_VISIT v WHERE v.pk_protocol_visit= a.FK_VISIT),0,0,DISPLACEMENT) AS DISPLACEMENT, " 
		    	    + "EVENT_ID, " + "ORG_ID, "
		            + " CHAIN_ID, " + "EVENT_TYPE, " + "replace(replace(a.NAME,CHR(13),''),CHR(10),'') NAME, " + "NOTES, "
		            + " COST, " + "COST_DESCRIPTION, " + "DURATION, "
		            + " USER_ID, " + "LINKED_URI, " + "FUZZY_PERIOD, "
		            + " MSG_TO, " + " FK_CODELST_CALSTAT as codeStatus," + "DESCRIPTION, "
		            + " 0 AS MONTH , "
		            + " 0 AS WEEK , "
		            + " 0 AS DAY, " 
		            + " 0 AS INSERT_AFTER,"
	                + " 0 AS INSERT_AFTER_INTERVAL,"
	                + " '' AS INSERT_AFTER_INTERVAL_UNIT,"
		            + " 'E' as flag, FK_VISIT,  EVENT_CATEGORY, EVENT_SEQUENCE, offline_flag, hide_flag, "
		            + " (select site_name from er_site b  where b.pk_site = a.FACILITY_ID) as Facilityname, "
		            + " lst_cost(a.EVENT_ID) as eventcost "
		            + " FROM EVENT_ASSOC a " + "WHERE  a.CHAIN_ID= ?  "
		            + " ANd a.FK_VISIT IS NOT NULL and a.event_type <> 'U' ");//JM: 06May2008, modified, added event_type <> 'U' for, Enh. #PS16
                   //BK-DAY0 Feb09-2010

	            	if (!visitId.equals("")){
	            		sbSql.append(" and fk_visit="+visitId);
	            	}

	            	if (!evtName.equals("")){
	            		sbSql.append(" and lower(name) like lower(lower('%"+evtName+"%'))");
	            	}

	            	sbSql.append("ORDER BY DISPLACEMENT,fk_visit,flag desc, EVENT_SEQUENCE ");
	            		//BK-FIX-5883-MAR-21-2011
	            	

	            	String mysql = sbSql.toString();




            Rlog.debug("eventassoc",
                    "In getAllProtSelectedEvents in EventAssocDao");

            pstmt = conn.prepareStatement(mysql);

            pstmt.setInt(1, protocolId);
            pstmt.setInt(2, protocolId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                String id = rs.getString("EVENT_ID");
                id = id.trim();
                Integer test1 = new Integer(id);
                setEvent_ids(test1);

                String id1 = rs.getString("ORG_ID");
                id1 = id1.trim();
                Integer test2 = new Integer(id1);
                setOrg_ids(test2);
                setChain_ids(rs.getString("CHAIN_ID"));
                setEvent_types(rs.getString("EVENT_TYPE"));
                setNames(rs.getString("NAME"));
                setNotes(rs.getString("NOTES"));
                setCosts(rs.getString("COST"));
                setCost_descriptions(rs.getString("COST_DESCRIPTION"));
                setDurations(rs.getString("DURATION"));
                setUser_ids(rs.getString("USER_ID"));

                setLinked_uris(rs.getString("LINKED_URI"));

                setFuzzy_periods(rs.getString("FUZZY_PERIOD"));

                setMsg_tos(rs.getString("MSG_TO"));                
                setStatCodes(rs.getString("codeStatus"));

                setDescription(rs.getString("DESCRIPTION"));
                setDisplacement(rs.getString("DISPLACEMENT"));
                // SV, 10/26/04, cal-enh-??, display month as well in the
                // interval.
                // setWeek(new Integer(rs.getInt("WEEK")));
                // setDay(new Integer(rs.getInt("DAY")));
                //D-FIN-25-DAY0 BK
                this.setIntervalArrays(rs.getInt("DISPLACEMENT"),rs.getInt("MONTH"),rs.getInt("WEEK"),rs.getInt("DAY"));
                this.setInsertAfter(rs.getInt("INSERT_AFTER"));
                this.createAfterIntervalStrings(rs.getInt("INSERT_AFTER"), rs.getInt("INSERT_AFTER_INTERVAL"), rs.getString("INSERT_AFTER_INTERVAL_UNIT"));
                // months/weeks/days.

                setFlag(rs.getString("flag"));
                // SV, 9/27/04, cal-enh-03-b
                sValue = rs.getString("FK_VISIT");
                if (sValue == null)
                    iValue = new Integer(0);
                else
                    iValue = new Integer(sValue.trim());
                setEventVisits(iValue);

                setEventCategory(rs.getString("EVENT_CATEGORY"));
              //JM: 25Apr2008, added for, Enh. #C9
                setOfflineFlag(rs.getString("offline_flag"));
                setHideFlag(rs.getString("hide_flag"));

                setEventSequences(rs.getString("EVENT_SEQUENCE"));
                setFacility(rs.getString("Facilityname"));//KV: SW-FIN2c
                setEventCostDetails(rs.getString("eventcost"));//KV: SW-FIN2b


            }

        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "eventassoc",
                            "EventAssocDao.getAllProtSelectedEvents EXCEPTION IN FETCHING FROM EventAssoc table"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getStudyProts(int i, String status) {
        PreparedStatement preparedstatement = null;
        Connection connection = null;
        try {
            connection = getConnection();
            String s = null;
            if("ND".equals(status)){//retrieve all calendars except de-activated ..DFIN13/04APRIL
            s = "select EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES, COST, COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI, FUZZY_PERIOD, MSG_TO, FK_CODELST_CALSTAT as codeStatus, DESCRIPTION, DISPLACEMENT FROM EVENT_ASSOC WHERE  CHAIN_ID= ? AND UPPER(EVENT_TYPE) IN('p','P') "
            	+ " and  FK_CODELST_CALSTAT in (select pk_codelst from sch_codelst where codelst_type='calStatStd' and CODELST_SUBTYP != 'D') and event_calassocto='P' ORDER BY NAME";
            }
            else
            {
            s = "select EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME  FROM EVENT_ASSOC WHERE CHAIN_ID= ? AND UPPER(EVENT_TYPE) IN('p','P') and event_calassocto='P' and FK_CODELST_CALSTAT=(select pk_codelst "
            	+ " from sch_codelst where codelst_type='calStatStd'and CODELST_SUBTYP= '"+status+"') ORDER BY NAME";
            }
            

            preparedstatement = connection.prepareStatement(s);

            preparedstatement.setInt(1, i);            

            ResultSet resultset = preparedstatement.executeQuery();

            while (resultset.next()) {

                String s1 = resultset.getString("EVENT_ID");
                s1 = s1.trim();
                Integer integer = new Integer(s1);

                setEvent_ids(integer);

                setChain_ids(resultset.getString("CHAIN_ID"));
                setEvent_types(resultset.getString("EVENT_TYPE"));
                setNames(resultset.getString("NAME"));

            }

        } catch (SQLException sqlexception) {
            Rlog
                    .fatal(
                            "eventassoc",
                            "EventAssocDao.getStudyProts(int i, String status) EXCEPTION IN FETCHING FROM eventassoc table"
                                    + sqlexception);
        } finally {
            try {
                if (preparedstatement != null)
                    preparedstatement.close();
            } catch (Exception exception1) {
            }
            try {
                if (connection != null)
                    connection.close();
            } catch (Exception exception2) {
            }
        }
    }

    public int UpdateEventVisitIds(String chain_id, String userId, String ipAdd) {
        Rlog.debug("eventassoc", "eventAssocdao - UpdateEventVisitIds start");
        CallableStatement cstmt = null;
        int success = -1;

        Connection conn = null;
        try {

            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call sp_update_assoc_with_visitids(?,?,?)}");

            cstmt.setString(1, chain_id);
            cstmt.setString(2, userId);
            cstmt.setString(3, ipAdd);

            cstmt.execute();
            success = 1;

        }

        catch (SQLException ex) {
            Rlog.fatal("eventassoc",
                    "In UpdateEventVisitIds in Eventassoc line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;

    }

    public int propagateEventUpdates(int protocolId, int eventId,
            String tableName, int primary_key, String propagateInVisitFlag,
            String propagateInEventFlag, String mode, String calType) {
        // SV, 9/16/04, a new method to propagate Event updates to other events
        // in the same calendar depending on the flags.
        // eventsInVisit - All the events with the same visit #, within the same
        // calendar
        // sameEvents - All events with the same name within the calendar.

        Rlog.debug("eventassoc", "eventassocdao - propagateEventUpdates Start");
        CallableStatement cstmt = null;
        int success = -1;
        Connection conn = null;
        try {
            Rlog.debug("eventassoc", "just before calling sp_propagate");
            Rlog.debug("eventassoc", "protocol_id=" + protocolId + "eventId="
                    + eventId + "tablename=" + tableName);
            Rlog
                    .debug("eventassoc", "propagateInVisit="
                            + propagateInVisitFlag);
            Rlog
                    .debug("eventassoc", "propagateInEvent="
                            + propagateInEventFlag);
            Rlog.debug("eventassoc", "mode=" + mode);
            Rlog.debug("eventassoc", "calType=" + calType);

            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_PROPGT_EVTUPDATES.sp_propagate_evt_updates(?,?,?,?,?,?,?,?)}");

            cstmt.setInt(1, protocolId);
            cstmt.setInt(2, eventId);
            cstmt.setString(3, tableName);
            cstmt.setInt(4, primary_key);
            cstmt.setString(5, propagateInVisitFlag);
            cstmt.setString(6, propagateInEventFlag);
            cstmt.setString(7, mode);
            cstmt.setString(8, calType);

            cstmt.execute();
            Rlog.debug("eventassoc", "just after calling sp_propagate");

            success = 1;

        }

        catch (SQLException ex) {
            Rlog
                    .fatal(
                            "eventassoc",
                            "In propagateEventUpdates in Eventassoc line EXCEPTION IN calling the procedure"
                                    + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;

    }
//D-FIN-25-DAY0 BK
    public void setIntervalArrays(int displacement,int pMonth,int pWeek,int pDay) {        
        int temp;
        int month, week, day;
        if (displacement < 0) {
            setWeek(new Integer("-1"));
            setMonth(new Integer("-1"));
            setDay(new Integer(displacement));

        } else {
        
            setDay(new Integer(pDay));
            setWeek(new Integer(pWeek));
            setMonth(new Integer(pMonth));
         }  

    }
    
  //D-FIN-25-DAY0 BK
    /**
     * creates the interval string for visit if set After field is set.
     * 
     */    
    public void createAfterIntervalStrings(int insertAfter,int insertAfterInterval,String intervalUnit ) {
    	String intervalString = "";
    	if(insertAfter!=0){
    		intervalString = ""+insertAfterInterval;
    		if("W".equals(intervalUnit)){
    			intervalString = intervalString + " Week(s)";
    		}else if("M".equals(intervalUnit)){
    			intervalString = intervalString + " Month(s)";
    		}
    		else{
    			intervalString = intervalString + " Day(s)";
    		}   
    		intervalString = intervalString + " After";        
    	}	
    	this.getAfterIntervalStrings().add(intervalString);
    }

    /** Refreshes the messages for all patients enrolled on this calendar (only current schedules) */
    /* Sonia Abrol, 10/30/06*/
    public int refreshCalendarMessages(String protocolId, String studyId, String modifiedBy,
            String ipAdd) {

        Rlog.debug("eventassoc", "eventassoc- refreshCalendarMessages start");
        CallableStatement cstmt = null;
        int success = -1;
        String output = "";
        Connection conn = null;
        try {

            conn = getConnection();
            cstmt = conn.prepareCall("{call PKG_GENSCH.sp_refresh_cal_messages(?,?,?,?,?)}");

            cstmt.setString(1, protocolId);
            cstmt.setString(2, studyId);
            cstmt.setString(3, modifiedBy);
            cstmt.setString(4, ipAdd);

            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);

            cstmt.execute();

            output = cstmt.getString(5);

            if (StringUtil.isEmpty(output))
            {
            	success = 1;
            }
            else
            {
            	Rlog.fatal("eventassoc",
                        "In refreshCalendarMessages in EventAssoc EXCEPTION IN calling the procedure"
                                + output);
            	success = -1;
            }

        }

        catch (SQLException ex) {
            Rlog.fatal("eventassoc",
                    "In refreshCalendarMessages in EventAssoc EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    //JM: 10Apr2008:
    public void getProtVisitAndEvents(int protocolId, String searchStr,String resetSort) { /*YK 04Jan- SUT_1_Requirement_18489*/

        PreparedStatement pstmt = null;
        Connection conn = null;

        StringBuffer sbMySql = new StringBuffer();

        try {

            conn = getConnection();
            sbMySql.append("select EVENT_ID, "
            		+ "ORG_ID, "
            		+ "CHAIN_ID, "
                    + "EVENT_TYPE, "
                    + "NAME, "
                    + "NOTES, "
                    + "COST, "
                    + "COST_DESCRIPTION, "
                    + "DURATION, "
                    + "USER_ID, "
                    + "LINKED_URI, "
                    + "FUZZY_PERIOD, "
                    + "MSG_TO, "
                    + "FK_CODELST_CALSTAT as codeStatus,"
                    + "DESCRIPTION, "
                    + "DISPLACEMENT, "
                    + " EVENT_CPTCODE, EVENT_CATEGORY  FROM EVENT_ASSOC "
                    + "WHERE  CHAIN_ID= ? ");
                    if(!searchStr.equals("")){
                    	//KM-3456
                    	sbMySql.append("and (trim(lower(name)) like trim(lower('%"+searchStr+"%')) escape '\\' OR trim(lower(event_category)) like trim(lower('%" +searchStr + "%')) escape '\\'  )");
                    }
                    /*YK 04Jan- SUT_1_Requirement_18489*/  //BK-DAY0 Feb09-2010
                    if(resetSort.equals("true")){
            			sbMySql.append("and FK_VISIT IS NULL ORDER BY EVENT_ID");
            		}else{
                    sbMySql.append(" and FK_VISIT IS NULL  ORDER BY trim(lower(NAME))");
            		}

            String mysql =  sbMySql.toString();



            Rlog.debug("eventassoc", mysql);

            pstmt = conn.prepareStatement(mysql);

            pstmt.setInt(1, protocolId);
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("eventassoc", "After execution" + rs.toString());

            while (rs.next()) {
                String id = rs.getString("EVENT_ID");
                Rlog.debug("eventassoc", id);
                id = id.trim();
                Integer test1 = new Integer(id);
                setEvent_ids(test1);

                String id1 = rs.getString("ORG_ID");
                id1 = id1.trim();
                Integer test2 = new Integer(id1);
                setOrg_ids(test2);
                setChain_ids(rs.getString("CHAIN_ID"));
                setEvent_types(rs.getString("EVENT_TYPE"));
                setNames(rs.getString("NAME"));
                setNotes(rs.getString("NOTES"));
                setCosts(rs.getString("COST"));
                setCost_descriptions(rs.getString("COST_DESCRIPTION"));
                setDurations(rs.getString("DURATION"));
                setUser_ids(rs.getString("USER_ID"));

                setLinked_uris(rs.getString("LINKED_URI"));

                setFuzzy_periods(rs.getString("FUZZY_PERIOD"));

                setMsg_tos(rs.getString("MSG_TO"));                
                setStatCodes(rs.getString("codeStatus"));
                setDescription(rs.getString("DESCRIPTION"));
                setDisplacement(rs.getString("DISPLACEMENT"));
                setCptCodes(rs.getString("EVENT_CPTCODE"));
                setEventCategory(rs.getString("EVENT_CATEGORY"));



            }

        } catch (SQLException ex) {
            Rlog.fatal("eventassoc",
                    "EventAssocDao.getEventAssocValues EXCEPTION IN FETCHING FROM EventAssoc table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }



    /**
	 *
	 * @param visitId, eventId, tableName
	 * @return maximum sequence number
	 */
	  public int getMaxSeqForVisitEvents(int visitId, String eventName, String tableName,int eventid){
		  PreparedStatement pstmt = null;
	      Connection conn = null;
	      String sql = "";
	      int ret = 0;
	      try {
	          conn = getConnection();

		          sql="select f_get_event_sequence_new(?,?,?,?) as max_seq from dual";
		          pstmt = conn.prepareStatement(sql);
		          pstmt.setInt(1, visitId);
		          pstmt.setString(2, eventName);
		          pstmt.setString(3, tableName);
		          pstmt.setString(4, "");

	          		ResultSet rs = pstmt.executeQuery();

	            while (rs.next()) {
	            	ret =   rs.getInt("max_seq");
	            }

	      	  if (pstmt != null)
	              pstmt.close();

	      	  if (conn != null)
	            conn.close();

	           return ret;

	      } catch (SQLException ex) {
	          Rlog.fatal("eventassoc","EventAssocDao EXCEPTION in getMaxSeqForVisitEvents()"+ ex);

	          return 0;

	      } finally{
	    	  try {
	     	  if (pstmt != null)
	              pstmt.close();

	    	  }
	    	  catch (SQLException ex) {
	              Rlog.fatal("eventassoc","EventAssocDao EXCEPTION in getMaxSeqForVisitEvents()"+ ex);

	              return 0;

	          }

	    	  try {
		      	  if (conn != null)
		            conn.close();
		    	  }
		    	  catch (SQLException ex) {
		              Rlog.fatal("eventassoc","EventAssocDao EXCEPTION in getMaxSeqForVisitEvents()"+ ex);

		              return 0;

		          }

	      }

	   }

	  public int getMaxSeqForVisitEvents(int visitId, String eventName, String tableName){
		  PreparedStatement pstmt = null;
	      Connection conn = null;
	      String sql = "";
	      int ret = 0;
	      try {
	          conn = getConnection();

		          sql="select pkg_calendar.f_get_event_sequence(?,?,?) as max_seq from dual";
		          pstmt = conn.prepareStatement(sql);
		          pstmt.setInt(1, visitId);
		          pstmt.setString(2, eventName);
		          pstmt.setString(3, tableName);

	          		ResultSet rs = pstmt.executeQuery();

	            while (rs.next()) {
	            	ret =   rs.getInt("max_seq");
	            }

	      	  if (pstmt != null)
	              pstmt.close();

	      	  if (conn != null)
	            conn.close();

	           return ret;

	      } catch (SQLException ex) {
	          Rlog.fatal("eventassoc","EventAssocDao EXCEPTION in getMaxSeqForVisitEvents()"+ ex);

	          return 0;

	      } finally{
	    	  try {
	     	  if (pstmt != null)
	              pstmt.close();

	    	  }
	    	  catch (SQLException ex) {
	              Rlog.fatal("eventassoc","EventAssocDao EXCEPTION in getMaxSeqForVisitEvents()"+ ex);

	              return 0;

	          }

	    	  try {
		      	  if (conn != null)
		            conn.close();
		    	  }
		    	  catch (SQLException ex) {
		              Rlog.fatal("eventassoc","EventAssocDao EXCEPTION in getMaxSeqForVisitEvents()"+ ex);

		              return 0;

		          }

	      }

	   }

	  /**
		 *
		 * @param visitId, eventId, tableName
		 * @return maximum sequence number
		 */
	  public void updateOfflnFlgForEventsVisits(int event_id, String offlineFlag) {

			  CallableStatement cstmt = null;
		      Connection conn = null;
		      String sql = "";
		      int ret = 0;
		      try {
		          conn = getConnection();

			          sql="{call pkg_calendar.sp_updteOflgEventsVisits(?,?)}";
			          cstmt = conn.prepareCall(sql);
			          cstmt.setInt(1, event_id);
			          cstmt.setString(2, offlineFlag);
			          cstmt.execute();


	          } catch (SQLException e) {
	              Rlog.fatal("eventassoc",
	                      "In updateOfflnFlgForEventsVisits in EventassocDao line EXCEPTION IN calling the procedure"
	                              + e);
	          } finally {
	              try {
	                  if (cstmt != null)
	                	  cstmt.close();
	              } catch (Exception exception1) {
	              }
	              try {
	                  if (conn != null)
	                	  conn.close();
	              } catch (Exception exception2) {
	              }
	          }

	  }


	//JM: 25Apr2008, added for, Enh. #C9
	    public int hideUnhideEventsVisits(String[] eventIds, String hide_flag, String[] flags ) {

	        CallableStatement cstmt = null;
	        Rlog.debug("eventassoc","In hideUnhideEventsVisits in EventdefDao line number 0");
	        int ret = -1;

	        Connection conn = null;
	        try {


	        	conn = getConnection();

	            ArrayDescriptor inIds = ArrayDescriptor.createDescriptor(
	                    "ARRAY_STRING", conn);
	            ARRAY delIdsArray = new ARRAY(inIds, conn, eventIds);



	            ArrayDescriptor flagIds = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
	            ARRAY flagsArray = new ARRAY(flagIds, conn, flags);


	            cstmt = conn.prepareCall("{call pkg_calendar.sp_hide_unhide_EventsVisits(?,?,?)}");

	            cstmt.setArray(1, delIdsArray);
	            cstmt.setString(2, hide_flag);
	            cstmt.setArray(3, flagsArray);

	            cstmt.execute();
	            ret = 0;
	        }

	        catch (SQLException ex) {
	            Rlog
	                    .fatal(
	                            "eventassoc","In hideUnhideEventsVisits() EXCEPTION IN calling the proc..."
	                                    + ex);
	        } finally {
	            try {
	                if (cstmt != null)
	                    cstmt.close();
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }
	        }
	        return ret;
	    }


	    //------

	    /**
		 *
		 * @param calId, checkedVal, statusId, selDate, userId, ipAdd
		 *
		 */


	  public void updatePatientSchedulesNow(int calId, int checkedVal, int statusId, java.sql.Date selDate,int userId, String ipAdd ){

			  CallableStatement cstmt = null;
		      Connection conn = null;
		      String sql = "";
		      int ret = 0;
		      try {
		          conn = getConnection();

		          sql="{call pkg_calendar.sp_update_Pat_Schedules_now(?,?,?,?,?,?)}";
			          cstmt = conn.prepareCall(sql);
			          cstmt.setInt(1, calId);
			          cstmt.setInt(2, checkedVal);
			          cstmt.setInt(3, statusId);
			          cstmt.setDate(4, selDate);
			          cstmt.setInt(5, userId);
			          cstmt.setString(6, ipAdd);
			          cstmt.execute();


	          } catch (SQLException e) {
	              Rlog.fatal("eventassoc",
	                      "In updatePatientSchedulesNow in EventassocDao line EXCEPTION IN calling the procedure sp_update_Pat_Schedules_now"
	                              + e);
	          } finally {
	              try {
	                  if (cstmt != null)
	                	  cstmt.close();
	              } catch (Exception exception1) {
	              }
	              try {
	                  if (conn != null)
	                	  conn.close();
	              } catch (Exception exception2) {
	              }
	          }

	  }

	//JM: 06May2008, added for, Enh. #PS16

	  public void updateUnscheduledEvents1( int patId,int patProtId,  int calId, int userId, String ipAdd, String[] eventsArr  ){


		  CallableStatement cstmt = null;
	      Connection conn = null;
	      String sql = "";
	      int ret = 0;
	      try {
	          conn = getConnection();



	          ArrayDescriptor evtIds = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
	          ARRAY evtArray = new ARRAY(evtIds, conn, eventsArr);

	          sql="{call pkg_calendar.sp_updt_unsch_evts1(?,?,?,?,?,?)}";
		          cstmt = conn.prepareCall(sql);
		          cstmt.setInt(1, patId);

		          cstmt.setInt(2, patProtId);
		          cstmt.setInt(3, calId);
		          cstmt.setInt(4, userId);
		          cstmt.setString(5, ipAdd);
		          cstmt.setArray(6, evtArray);

		          cstmt.execute();


          } catch (SQLException e) {
              Rlog.fatal("eventassoc",
                      "In updateUnscheduledEvents1 in EventassocDao line EXCEPTION IN calling the procedure sp_updt_unsch_evts1"
                              + e);
          } finally {
              try {
                  if (cstmt != null)
                	  cstmt.close();
              } catch (Exception exception1) {
              }
              try {
                  if (conn != null)
                	  conn.close();
              } catch (Exception exception2) {
              }
          }

  }

	public ArrayList getProtocolAssocTo() {
		return protocolAssocTo;
	}

	public void setProtocolAssocTo(ArrayList protocolAssocTo) {
		this.protocolAssocTo = protocolAssocTo;
	}

	public void setProtocolAssocTo(String pAssocTo) {
		this.protocolAssocTo.add(pAssocTo);
	}


	//JM: 16July2008, added for the issue #3586


    public int deleteEventStatusHistory(int eventStatusHistId){


        PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;

        try {
            conn = getConnection();
            String mysql = "delete from SCH_EVENTSTAT "
                    + " WHERE PK_EVENTSTAT = ?";

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, eventStatusHistId);
            rs = pstmt.executeQuery();
            rs.next();

        } catch (SQLException ex) {
            Rlog.fatal("EventAssocDao", "EventAssocDao.deleteEventStatusHistory()" + ex);
            return -2;
        }finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return 1;
    }


    public void updateCurrentStatus( int evtId ){


		  CallableStatement cstmt = null;
	      Connection conn = null;
	      String sql = "";

	      try {
	          conn = getConnection();

	          sql="{call pkg_calendar.sp_updateCurrentStatus(?)}";
		          cstmt = conn.prepareCall(sql);
		          cstmt.setInt(1, evtId);
		          cstmt.execute();


          } catch (SQLException e) {
        	  Rlog.fatal("EventAssocDao", "EventAssocDao.updateCurrentStatus()" + e);
          } finally {
              try {
                  if (cstmt != null)
                	  cstmt.close();
              } catch (Exception exception1) {
              }
              try {
                  if (conn != null)
                	  conn.close();
              } catch (Exception exception2) {
              }
          }

  }

  public int createProtocolBudget(int protocol_id, int bgtTemplate, String bgtName,
            int userId, String ipAdd, int studyId)  {
        CallableStatement callablestatement = null;
        int newProtocol = -1;
        Connection connection = null;
        Rlog.debug("eventassoc",
                "In sp_createProtocolBudget in EventAssocDao line number 1");
        try {
            connection = getConnection();
            callablestatement = connection
                    .prepareCall("{call Pkg_BGT.sp_createProtocolBudget(?,?,?,?,?,?,1,?)}");
            callablestatement.setInt(1, protocol_id);
            callablestatement.setInt(2, bgtTemplate);
            callablestatement.setString(3, bgtName);
            callablestatement.setInt(4, userId);
            callablestatement.setString(5, ipAdd);
            callablestatement.registerOutParameter(6, java.sql.Types.INTEGER);
            callablestatement.setInt(7, studyId);

            callablestatement.execute();
            newProtocol = callablestatement.getInt(6);
        } catch (SQLException sqlexception) {
            Rlog.fatal("eventassoc",
                    "In sp_createProtocolBudget in Eventassoc line EXCEPTION IN calling the procedure"
                            + sqlexception);
        } finally {
            try {
                if (callablestatement != null)
                    callablestatement.close();
            } catch (Exception exception1) {
            }
            try {
                if (connection != null)
                    connection.close();
            } catch (Exception exception2) {
            }
        }
        return newProtocol;
    }  
  
  
  public int getEventFormAccees(int crfLinkedForm, int assocId, int portalId){


      PreparedStatement pstmt = null;
      Connection conn = null;
      ResultSet rs = null;
      int eventcount = 0;

      try {
          conn = getConnection();
          String mysql = "SELECT COUNT(PK_PF) as EVENTCOUNT FROM SCH_PORTAL_FORMS WHERE FK_FORM=? and FK_EVENT=? and FK_PORTAL=?";

          pstmt = conn.prepareStatement(mysql);
          pstmt.setInt(1, crfLinkedForm);
          pstmt.setInt(2, assocId);
          pstmt.setInt(3, portalId);
          
          rs = pstmt.executeQuery();
          
          if (rs != null) {
              rs.next();
              eventcount = rs.getInt("EVENTCOUNT");
          }

         
       
          return eventcount;


      } catch (SQLException ex) {
          Rlog.fatal("EventAssocDao", "EventAssocDao.getEventFormAccees()" + ex);
          return -2;
      }finally {
          try {
              if (pstmt != null)
                  pstmt.close();
          } catch (Exception e) {
          }
          try {
              if (conn != null)
                  conn.close();
          } catch (Exception e) {
          }
      }
      
  }
  
  public void getPatSchEvent_SOS_CoverType(int eventId) {
      PreparedStatement pstmt = null;
      Connection conn = null;
      try {
          Integer iValue = new Integer(0);
          String sValue = "";

          conn = getConnection();
          String mysql = "select EVENT_ID, SERVICE_SITE_ID, FK_CODELST_COVERTYPE "
          	+ "FROM SCH_EVENTS1 "
          	+ "WHERE EVENT_ID = ?";

          Rlog.debug("eventAssoc", mysql);

          pstmt = conn.prepareStatement(mysql);
          pstmt.setInt(1, eventId);

          ResultSet rs = pstmt.executeQuery();

          while (rs.next()) {
              String id = rs.getString("EVENT_ID");
              Rlog.debug("eventAssoc", id);
              
              setEvent_ids(StringUtil.stringToInteger(id));
              setEventSOSIds(rs.getString("SERVICE_SITE_ID"));
              setEventCoverageTypes(rs.getString("FK_CODELST_COVERTYPE"));
          }

      } catch (SQLException ex) {
          Rlog.fatal("eventAssoc",
                  "EventAssocDao.getPatSchEvent_SOS_CoverType EXCEPTION IN FETCHING FROM SCH_EVENTS1 table"
                          + ex);
      } finally {
          try {
              if (pstmt != null)
                  pstmt.close();
          } catch (Exception e) {
          }
          try {
              if (conn != null)
                  conn.close();
          } catch (Exception e) {
          }
      }
  }
  
  public int createEventsAssoc(String[] eIdList, String[] vIdList, String[] seqList,
		  String[] dispList, String[] hideList,
		  int userId, String ipAdd) {
      int ret = -1;
      CallableStatement cstmt = null;
      Connection conn = null;
      try {
          conn = getConnection();
          ArrayDescriptor descEIdList = ArrayDescriptor.createDescriptor(
                  "ARRAY_STRING", conn);
          ARRAY eIdArray = new ARRAY(descEIdList, conn, eIdList);
          ArrayDescriptor descVIdList = ArrayDescriptor.createDescriptor(
                  "ARRAY_STRING", conn);
          ARRAY vIdArray = new ARRAY(descVIdList, conn, vIdList);
          ArrayDescriptor descSeqList = ArrayDescriptor.createDescriptor(
                  "ARRAY_STRING", conn);
          ARRAY seqArray = new ARRAY(descSeqList, conn, seqList);
          ArrayDescriptor descDispList = ArrayDescriptor.createDescriptor(
                  "ARRAY_STRING", conn);
          ARRAY dispArray = new ARRAY(descDispList, conn, dispList);
          ArrayDescriptor descHideList = ArrayDescriptor.createDescriptor(
                  "ARRAY_STRING", conn);
          ARRAY hideArray = new ARRAY(descHideList, conn, hideList);
          cstmt = conn.prepareCall("{call SP_CREATE_EVTS_ASSOC(?,?,?,?,?,?,?,?,?)}");
          cstmt.setArray(1, eIdArray);
          cstmt.setArray(2, vIdArray);
          cstmt.setArray(3, seqArray);
          cstmt.setArray(4, dispArray);
          cstmt.setArray(5, hideArray);
          cstmt.registerOutParameter(6, java.sql.Types.INTEGER);
          cstmt.setInt(7, userId);
          cstmt.setString(8, ipAdd);
          cstmt.setInt(9, 1);
          cstmt.execute();
          ret = cstmt.getInt(6);
      } catch (Exception e) {
    	  ret = -1;
    	  Rlog.fatal("eventassoc", "Exception in createEventsAssoc "+e);
      } finally {
    	  if (cstmt != null) { try { cstmt.close(); } catch(Exception e) {} }
    	  if (conn != null) { try { conn.close(); } catch(Exception e) {} }
      }
      return ret;
  }
  
  /**
   * This method retrieves all the scheduled events of a calendar with
   * EVENT DETAILS AND CPT CODE, EVENT LIBRARY TYPE
   *
   * @param calendarId
   */

  public void getAllProtSelectedVisitsEvents(int protocolId, String visitId, String evtName) {

      PreparedStatement pstmt = null;
      Connection conn = null;
      try {
          Integer iValue = new Integer(0);
          String sValue = "";

          conn = getConnection();

          StringBuffer sbSql = new StringBuffer();
          //Modify name for bug 23909
          sbSql.append("SELECT decode(NUM_DAYS,0,NUM_DAYS,DISPLACEMENT) as DISPLACEMENT, " + " 0 as EVENT_ID, " + "0 as ORG_ID, "
        		  + " 0 as CHAIN_ID, " + " '' as EVENT_TYPE, " + " visit_name as NAME, " 
        		  + " 0 AS USER_ID,"+" null as codeStatus," + " DESCRIPTION  as DESCRIPTION, "
        		  + " pk_protocol_visit as FK_VISIT , '' as EVENT_CATEGORY, 0 as EVENT_SEQUENCE, offline_flag, hide_flag, "
        		  + " 0 AS EVENT_LIBRARY_TYPE, "
        		  + " '' AS EVENT_CPTCODE "
        		  + " FROM sch_protocol_visit "
        		  + " WHERE  fk_protocol= ? ");
        		  if (!visitId.equals("")){
        		  sbSql.append(" and pk_protocol_visit="+visitId);
        		  }
        		  sbSql.append(" union"
        		  + " SELECT DECODE((SELECT NUM_DAYS FROM SCH_PROTOCOL_VISIT v WHERE v.pk_protocol_visit= a.FK_VISIT),0,0,DISPLACEMENT) AS DISPLACEMENT, "
        		  + " EVENT_ID, " + "ORG_ID, "
        		  + " CHAIN_ID, " + "EVENT_TYPE, " + "replace(replace(NAME,CHR(13),''),CHR(10),'') NAME, " 
        		  + " USER_ID, " + " FK_CODELST_CALSTAT as codeStatus," + "DESCRIPTION, "
        		  + " FK_VISIT,  EVENT_CATEGORY, EVENT_SEQUENCE, offline_flag, hide_flag, "
        		  + "  EVENT_LIBRARY_TYPE, "
        		  + " EVENT_CPTCODE "
        		  + " FROM EVENT_ASSOC a " + "WHERE  a.CHAIN_ID= ?  "
        		  + " ANd a.FK_VISIT IS NOT NULL and a.event_type <> 'U' ");//JM: 06May2008, modified, added event_type <> 'U' for, Enh. #PS16
        	
        		  if (!visitId.equals("")){
        		  sbSql.append(" and fk_visit="+visitId);
        		  }

        		  if (!evtName.equals("")){
        		  sbSql.append(" and lower(name) like lower(lower('%"+evtName+"%'))");
        		  }

        		  sbSql.append(" ORDER BY DISPLACEMENT, fk_visit, EVENT_SEQUENCE ");
	              String mysql = sbSql.toString();
          Rlog.debug("eventassoc",
                  "In getAllProtSelectedEvents in EventAssocDao");

          pstmt = conn.prepareStatement(mysql);

          pstmt.setInt(1, protocolId);
          pstmt.setInt(2, protocolId);
          ResultSet rs = pstmt.executeQuery();

          while (rs.next()) {
              String id = rs.getString("EVENT_ID");
              id = id.trim();
              Integer test1 = new Integer(id);
              setEvent_ids(test1);

              String id1 = rs.getString("ORG_ID");
              id1 = id1.trim();
              Integer test2 = new Integer(id1);
              setOrg_ids(test2);
              setChain_ids(rs.getString("CHAIN_ID"));
              setEvent_types(rs.getString("EVENT_TYPE"));
              setNames(rs.getString("NAME"));
              setUser_ids(rs.getString("USER_ID"));
              setStatCodes(rs.getString("codeStatus"));
              setDescription(rs.getString("DESCRIPTION"));
              setDisplacement(rs.getString("DISPLACEMENT"));
              sValue = rs.getString("FK_VISIT");
              if (sValue == null)
                  iValue = new Integer(0);
              else
                  iValue = new Integer(sValue.trim());
              setEventVisits(iValue);
              setEventCategory(rs.getString("EVENT_CATEGORY"));
              setOfflineFlag(rs.getString("offline_flag"));
              setHideFlag(rs.getString("hide_flag"));
              setEventSequences(rs.getString("EVENT_SEQUENCE"));
              setEventLibraryType(rs.getString("EVENT_LIBRARY_TYPE"));
              setCptCodes(rs.getString("EVENT_CPTCODE"));


          }

      } catch (SQLException ex) {
          Rlog
                  .fatal(
                          "eventassoc",
                          "EventAssocDao.getAllProtSelectedEvents EXCEPTION IN FETCHING FROM EventAssoc table"
                                  + ex);
      } finally {
          try {
              if (pstmt != null)
                  pstmt.close();
          } catch (Exception e) {
          }
          try {
              if (conn != null)
                  conn.close();
          } catch (Exception e) {
          }

      }

  }
  
 
 public void getProtSelectedAndVisitEvents(int protocolId,int initEventSize,int initVisitSize,int limitEventSize,int limitVisitSize,String visitIds,String eventName ){
  
	  
	  PreparedStatement pstmt = null;
      Connection conn = null;
      ResultSet rs=null;
      try {
          conn = getConnection();
          StringBuffer sql = new StringBuffer();
          StringBuffer sql1 = new StringBuffer();
          StringBuffer mainSql = new StringBuffer();
          StringBuffer sql2 = new StringBuffer();
          sql
          .append(" Select * from (SELECT 0 AS EVENT_ID,0 AS NEW_EVENT_ID, 0 AS ORG_ID, fk_protocol AS CHAIN_ID, ")
          .append(" '' AS EVENT_TYPE, visit_name AS NAME, 0 AS COST, DESCRIPTION AS DESCRIPTION, ")
          .append(" 'V' AS flag, pk_protocol_visit AS FK_VISIT, VISIT_NO,DECODE(NUM_DAYS,0,0,DISPLACEMENT) DISPLACEMENT, ")
          .append(" '' AS EVENT_CATEGORY, hide_flag, 0 as OFFLINE_FLAG, 0 AS FK_CODELST_COVERTYPE, ")
          .append(" '' AS COVERAGE_NOTES, null as FK_CODELST_CALSTAT,")
          .append(" 0 AS EVENT_LIBRARY_TYPE, '' AS EVENT_CPTCODE ");
        sql.append(" FROM sch_protocol_visit WHERE fk_protocol = ? ORDER BY DISPLACEMENT, VISIT_NO) where ROWNUM <= ?");
        
          pstmt = conn.prepareStatement(sql.toString(),ResultSet.TYPE_SCROLL_INSENSITIVE,
                  ResultSet.CONCUR_UPDATABLE);
          pstmt.setInt(1, protocolId);
          pstmt.setInt(2,limitVisitSize );
          pstmt.setFetchSize(1000);
          rs = pstmt.executeQuery();
          rs.absolute(initVisitSize);
          do {
        	  setEvent_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("EVENT_ID")).trim()));
              setNew_event_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("NEW_EVENT_ID")).trim()));
              setOrg_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("ORG_ID")).trim()));
              setChain_ids(rs.getString("CHAIN_ID"));
              setEvent_types(rs.getString("EVENT_TYPE"));
              setNames(rs.getString("NAME"));
              setCosts(rs.getString("COST"));
              setDescription(rs.getString("DESCRIPTION"));
              setFlag(rs.getString("flag"));
              String sValue = rs.getString("FK_VISIT");
              if (sValue == null) { sValue = "0"; }
              setEventVisits(new Integer (sValue.trim()));
              setDisplacement(rs.getString("DISPLACEMENT"));
              setEventCategory(rs.getString("EVENT_CATEGORY"));
              setHideFlag(rs.getString("hide_flag"));
              setOfflineFlag(rs.getString("OFFLINE_FLAG"));
              setEventCoverageTypes(rs.getString("FK_CODELST_COVERTYPE"));
              setEventCoverageNotes(rs.getString("COVERAGE_NOTES"));               
              setStatCodes(rs.getString("FK_CODELST_CALSTAT"));
              setEventLibraryType(rs.getString("EVENT_LIBRARY_TYPE"));
              setCptCodes(rs.getString("EVENT_CPTCODE"));
          }while (rs.next());
          pstmt = null;
          rs = null;
          mainSql.append("SELECT count(*)")
          .append(" FROM EVENT_ASSOC a ")
          .append(" WHERE a.CHAIN_ID = ? ")
          .append(" AND a.event_type = 'A' ")
          .append(" AND a.fk_visit is null");
          if(!"".equalsIgnoreCase(eventName))
            	mainSql.append(" and (LOWER(NAME) LIKE LOWER('%"+eventName.replace("'" , "''")+"%')) ");
          
          pstmt = conn.prepareStatement(mainSql.toString());
          pstmt.setInt(1, protocolId);
          rs = pstmt.executeQuery();
          while (rs.next()){
          	totalRecords=rs.getInt("count(*)");
          }
          pstmt = null;
          rs = null;
          ArrayList eventCosts=new ArrayList();
          sql1.append("SELECT * from (SELECT COST ");
          sql1.append(" FROM EVENT_ASSOC a ")
          .append(" WHERE a.CHAIN_ID = ? ")
          .append(" AND a.event_type = 'A' ");
          if(!"".equalsIgnoreCase(eventName))
        	  sql1.append(" and (LOWER(NAME) LIKE LOWER('%"+eventName+"%')) ");
          sql1.append(" AND a.fk_visit is null ORDER BY Cost) ");
		         
          sql1.append(" where ROWNUM <= ? ");
          
          pstmt = conn.prepareStatement(sql1.toString(),ResultSet.TYPE_SCROLL_INSENSITIVE,
                  ResultSet.CONCUR_UPDATABLE);
          pstmt.setInt(1, protocolId);
          pstmt.setInt(2,limitEventSize );
          pstmt.setFetchSize(1000);
          rs = pstmt.executeQuery();
          rs.absolute(initEventSize);
          do {
          	eventCosts.add(Integer.parseInt(StringUtil.trueValue(rs.getString("COST")).trim()));
          }while (rs.next());
          String costs = eventCosts.toString().replace("[", "").replace("]", "")
          .replace(", ", ",");
          
          pstmt = null;
          rs = null;
          sql2.append(" Select b.EVENT_ID,lower(b.name) as event_name, a.EVENT_ID AS NEW_EVENT_ID, ")    //BK-DAY0 fix for coverage analysis.Feb09-2010
          .append(" a.ORG_ID, a.CHAIN_ID, a.EVENT_TYPE, a.NAME, a.COST, a.DESCRIPTION, ")
          .append(" 'E' AS flag, a.FK_VISIT, 0 AS VISIT_NO, 0 AS DISPLACEMENT, ")
          .append(" a.EVENT_CATEGORY, a.hide_flag, a.OFFLINE_FLAG, a.FK_CODELST_COVERTYPE, ")
          .append(" a.COVERAGE_NOTES, a.FK_CODELST_CALSTAT, ")
          .append(" a.EVENT_LIBRARY_TYPE,a.EVENT_CPTCODE ");
          sql2.append(" FROM EVENT_ASSOC a LEFT JOIN EVENT_ASSOC b ")
          .append(" ON a.CHAIN_ID = b.CHAIN_ID AND a.COST = b.COST AND b.FK_VISIT IS NULL AND b.EVENT_TYPE = a.EVENT_TYPE ")
          .append(" WHERE a.CHAIN_ID = ? AND a.event_type = 'A' and (a.fk_visit in("+visitIds+") or a.fk_visit is null) ")
          .append(" AND a.COST in ("+costs+") ORDER BY Cost,fk_visit nulls first ");

          pstmt = conn.prepareStatement(sql2.toString());
          pstmt.setInt(1, protocolId);
          pstmt.setFetchSize(1000);
          rs = pstmt.executeQuery();
         
          while (rs.next()) {
              setEvent_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("EVENT_ID")).trim()));
              setNew_event_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("NEW_EVENT_ID")).trim()));
              setOrg_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("ORG_ID")).trim()));
              setChain_ids(rs.getString("CHAIN_ID"));
              setEvent_types(rs.getString("EVENT_TYPE"));
              setNames(rs.getString("NAME"));
              setCosts(rs.getString("COST"));
              setDescription(rs.getString("DESCRIPTION"));
              setFlag(rs.getString("flag"));
              String sValue = rs.getString("FK_VISIT");
              if (sValue == null) { sValue = "0"; }
              setEventVisits(new Integer (sValue.trim()));
              setDisplacement(rs.getString("DISPLACEMENT"));
              setEventCategory(rs.getString("EVENT_CATEGORY"));
              setHideFlag(rs.getString("hide_flag"));
              setOfflineFlag(rs.getString("OFFLINE_FLAG"));
              setEventCoverageTypes(rs.getString("FK_CODELST_COVERTYPE"));
              setEventCoverageNotes(rs.getString("COVERAGE_NOTES"));               
              setStatCodes(rs.getString("FK_CODELST_CALSTAT"));
              setEventLibraryType(rs.getString("EVENT_LIBRARY_TYPE"));
              setCptCodes(rs.getString("EVENT_CPTCODE"));   
          }
      } catch (Exception ex) {
          Rlog.fatal("eventassoc",
                     "EventAssocDao.getProtSelectedAndVisitEvents: "+ ex);
      } finally {
          try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
          try { if (conn != null) conn.close(); } catch (Exception e) {}
      }

  }
   
  
  //YK: Added for PCAL-19743
  public void getProtSelectedAndVisitEvents(int protocolId) {
      PreparedStatement pstmt = null;
      Connection conn = null;
      try {
          conn = getConnection();
          StringBuffer sql = new StringBuffer();
          sql
          .append(" SELECT 0 AS EVENT_ID,0 AS NEW_EVENT_ID, 0 AS ORG_ID, fk_protocol AS CHAIN_ID, ")
          .append(" '' AS EVENT_TYPE, visit_name AS NAME, 0 AS COST, DESCRIPTION AS DESCRIPTION, ")
          .append(" 'V' AS flag, pk_protocol_visit AS FK_VISIT, VISIT_NO, DISPLACEMENT, ")
          .append(" '' AS EVENT_CATEGORY, hide_flag, 0 as OFFLINE_FLAG, 0 AS FK_CODELST_COVERTYPE, ")
          .append(" '' AS COVERAGE_NOTES, null as FK_CODELST_CALSTAT, ")
          .append(" 0 AS EVENT_LIBRARY_TYPE, ")
          .append(" '' AS EVENT_CPTCODE ")
          .append(" FROM sch_protocol_visit WHERE fk_protocol = ? ")
          .append(" UNION ")
          .append(" SELECT ")
          .append(" (select EVENT_ID from EVENT_ASSOC where CHAIN_ID = ? ")
          .append("  and COST = a.COST and FK_VISIT IS NULL),  EVENT_ID AS NEW_EVENT_ID,")
          .append(" ORG_ID, CHAIN_ID, EVENT_TYPE, replace(replace(NAME,CHR(13),''),CHR(10),'') NAME, COST, DESCRIPTION, ")
          .append(" 'E' AS flag, FK_VISIT, 0 AS VISIT_NO, 0 AS DISPLACEMENT, ")
          .append(" EVENT_CATEGORY, hide_flag, OFFLINE_FLAG, FK_CODELST_COVERTYPE, ")
          .append(" COVERAGE_NOTES, FK_CODELST_CALSTAT, ")
          .append(" EVENT_LIBRARY_TYPE, ")
          .append(" EVENT_CPTCODE ")
          .append(" FROM EVENT_ASSOC a ")
          .append(" WHERE a.CHAIN_ID = ?  ")
          .append(" AND a.event_type <> 'U' ")
          .append(" ORDER BY flag DESC, VISIT_NO, DISPLACEMENT ") //YK : Modified for Bug #7513 
          ;
          pstmt = conn.prepareStatement(sql.toString());
          pstmt.setInt(1, protocolId);
          pstmt.setInt(2, protocolId);
          pstmt.setInt(3, protocolId);
          ResultSet rs = pstmt.executeQuery();
          while (rs.next()) {
              setEvent_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("EVENT_ID")).trim()));
              setNew_event_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("NEW_EVENT_ID")).trim()));
              setOrg_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("ORG_ID")).trim()));
              setChain_ids(rs.getString("CHAIN_ID"));
              setEvent_types(rs.getString("EVENT_TYPE"));
              setNames(rs.getString("NAME"));
              setCosts(rs.getString("COST"));
              setDescription(rs.getString("DESCRIPTION"));
              setFlag(rs.getString("flag"));
              String sValue = rs.getString("FK_VISIT");
              if (sValue == null) { sValue = "0"; }
              setEventVisits(new Integer (sValue.trim()));
              setDisplacement(rs.getString("DISPLACEMENT"));
              setEventCategory(rs.getString("EVENT_CATEGORY"));
              setHideFlag(rs.getString("hide_flag"));
              setOfflineFlag(rs.getString("OFFLINE_FLAG"));
              setEventCoverageTypes(rs.getString("FK_CODELST_COVERTYPE"));
              setEventCoverageNotes(rs.getString("COVERAGE_NOTES"));               
              setStatCodes(rs.getString("FK_CODELST_CALSTAT"));
              setEventLibraryType(rs.getString("EVENT_LIBRARY_TYPE"));
              setCptCodes(rs.getString("EVENT_CPTCODE"));
              
              
          }
      } catch (Exception ex) {
          Rlog.fatal("eventassoc",
                     "EventAssocDao.getProtSelectedAndVisitEvents: "+ ex);
      } finally {
          try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
          try { if (conn != null) conn.close(); } catch (Exception e) {}
      }
  }
  
  public void getProtSelectedAndVisitEvents(int protocolId , String eventName) {
      PreparedStatement pstmt = null;
      Connection conn = null;
      try {
    	  conn = getConnection();
          StringBuffer sql = new StringBuffer();
          StringBuffer sql1 = new StringBuffer();
          String costs = "";
          
          ArrayList eventCosts=new ArrayList();
          ResultSet rs = null;
			if(!"".equalsIgnoreCase(eventName)){
			sql1.append("SELECT COST ")
          .append(" FROM EVENT_ASSOC a ")
          .append(" WHERE a.CHAIN_ID = ? ")
          .append(" AND a.event_type = 'A' ")
          .append(" and (LOWER(NAME) LIKE LOWER('%"+eventName+"%')) ")
          .append(" AND a.fk_visit is null ");
          
          pstmt = conn.prepareStatement(sql1.toString());
          pstmt.setInt(1, protocolId);
          pstmt.setFetchSize(1000);
          rs = pstmt.executeQuery();
          while (rs.next()) {
          	eventCosts.add(Integer.parseInt(StringUtil.trueValue(rs.getString("COST")).trim()));
          }
          costs= eventCosts.toString().replace("[", "").replace("]", "")
          .replace(", ", ",");
			 } //end of if
			pstmt = null;
			rs = null;
			if(!"".equalsIgnoreCase(eventName) && !"".equalsIgnoreCase(costs)){
          sql.append(" SELECT 0 AS EVENT_ID, lower(visit_name) AS event_name, 0 AS NEW_EVENT_ID, 0 AS ORG_ID, fk_protocol AS CHAIN_ID, ")
          .append(" '' AS EVENT_TYPE, visit_name AS NAME, 0 AS COST, DESCRIPTION AS DESCRIPTION, ")
          .append(" 'V' AS flag, pk_protocol_visit AS FK_VISIT, VISIT_NO, DISPLACEMENT, ")
          .append(" '' AS EVENT_CATEGORY, hide_flag, 0 as OFFLINE_FLAG, 0 AS FK_CODELST_COVERTYPE, ")
          .append(" '' AS COVERAGE_NOTES, null as FK_CODELST_CALSTAT, ")
          .append(" 0 AS EVENT_LIBRARY_TYPE, ")
          .append(" '' AS EVENT_CPTCODE ")
          .append(" FROM sch_protocol_visit WHERE fk_protocol = ? ");
         
          sql.append(" UNION ")
          .append(" SELECT b.EVENT_ID, lower(b.name) as event_name, a.EVENT_ID AS NEW_EVENT_ID,")
          .append(" a.ORG_ID, a.CHAIN_ID, a.EVENT_TYPE, replace(replace(a.NAME,CHR(13),''),CHR(10),'') NAME, a.COST, a.DESCRIPTION, ")
          .append(" 'E' AS flag, a.FK_VISIT, 0 AS VISIT_NO, 0 AS DISPLACEMENT, ")
          .append(" a.EVENT_CATEGORY, a.hide_flag, a.OFFLINE_FLAG, a.FK_CODELST_COVERTYPE, ")
          .append(" a.COVERAGE_NOTES, a.FK_CODELST_CALSTAT, ")
          .append(" a.EVENT_LIBRARY_TYPE, ")
          .append(" a.EVENT_CPTCODE ")
          .append(" FROM EVENT_ASSOC a left join EVENT_ASSOC b on a.CHAIN_ID = b.CHAIN_ID and  a.COST = b.COST and b.FK_VISIT  IS NULL AND b.EVENT_TYPE = a.EVENT_TYPE")
          .append(" WHERE a.CHAIN_ID = ?  ")
          .append(" AND a.event_type <> 'U' ");
          sql.append(" AND a.COST in ("+costs.toString()+")");
          sql.append(" ORDER BY flag DESC,VISIT_NO, DISPLACEMENT,Cost,fk_visit nulls first ") //YK : Modified for Bug #7513 
          ;
          pstmt = conn.prepareStatement(sql.toString());
          pstmt.setInt(1, protocolId);
          pstmt.setInt(2, protocolId);
          pstmt.setFetchSize(1000);
           rs = pstmt.executeQuery();
           }else if("".equalsIgnoreCase(eventName) && "".equalsIgnoreCase(costs)){
        	   //Modify name for bug 23909
        	   sql.append(" SELECT 0 AS EVENT_ID, lower(visit_name) AS event_name, 0 AS NEW_EVENT_ID, 0 AS ORG_ID, fk_protocol AS CHAIN_ID, ")
               .append(" '' AS EVENT_TYPE, visit_name AS NAME, 0 AS COST, DESCRIPTION AS DESCRIPTION, ")
               .append(" 'V' AS flag, pk_protocol_visit AS FK_VISIT, VISIT_NO, DISPLACEMENT, ")
               .append(" '' AS EVENT_CATEGORY, hide_flag, 0 as OFFLINE_FLAG, 0 AS FK_CODELST_COVERTYPE, ")
               .append(" '' AS COVERAGE_NOTES, null as FK_CODELST_CALSTAT, ")
               .append(" 0 AS EVENT_LIBRARY_TYPE, ")
               .append(" '' AS EVENT_CPTCODE ")
               .append(" FROM sch_protocol_visit WHERE fk_protocol = ? ")
               .append(" UNION ")
               .append(" SELECT ")
               .append(" b.EVENT_ID, lower(b.name) as event_name, a.EVENT_ID AS NEW_EVENT_ID, ")
               .append(" a.ORG_ID, a.CHAIN_ID, a.EVENT_TYPE, replace(replace(a.NAME,CHR(13),''),CHR(10),'') NAME, a.COST, a.DESCRIPTION, ")
               .append(" 'E' AS flag, a.FK_VISIT, 0 AS VISIT_NO, 0 AS DISPLACEMENT, ")
               .append(" a.EVENT_CATEGORY, a.hide_flag, a.OFFLINE_FLAG, a.FK_CODELST_COVERTYPE, ")
               .append(" a.COVERAGE_NOTES, a.FK_CODELST_CALSTAT, ")
               .append(" a.EVENT_LIBRARY_TYPE, ")
               .append(" a.EVENT_CPTCODE ")
               .append(" FROM EVENT_ASSOC a LEFT JOIN EVENT_ASSOC b ")
               .append(" ON a.CHAIN_ID = b.CHAIN_ID AND a.COST = b.COST AND b.FK_VISIT IS NULL AND b.EVENT_TYPE = a.EVENT_TYPE ")
               .append(" WHERE a.CHAIN_ID = ? AND a.event_type = 'A' ")
               .append(" ORDER BY flag DESC,VISIT_NO, DISPLACEMENT,Cost,fk_visit nulls first ");
               pstmt = conn.prepareStatement(sql.toString());
               pstmt.setInt(1, protocolId);
               pstmt.setInt(2, protocolId);
               pstmt.setFetchSize(1000);
                rs = pstmt.executeQuery();
           }
			if(rs!=null){
          while (rs.next()) {
              setEvent_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("EVENT_ID")).trim()));
              setNew_event_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("NEW_EVENT_ID")).trim()));
              setOrg_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("ORG_ID")).trim()));
              setChain_ids(rs.getString("CHAIN_ID"));
              setEvent_types(rs.getString("EVENT_TYPE"));
              setNames(rs.getString("NAME"));
              setCosts(rs.getString("COST"));
              setDescription(rs.getString("DESCRIPTION"));
              setFlag(rs.getString("flag"));
              String sValue = rs.getString("FK_VISIT");
              if (sValue == null) { sValue = "0"; }
              setEventVisits(new Integer (sValue.trim()));
              setDisplacement(rs.getString("DISPLACEMENT"));
              setEventCategory(rs.getString("EVENT_CATEGORY"));
              setHideFlag(rs.getString("hide_flag"));
              setOfflineFlag(rs.getString("OFFLINE_FLAG"));
              setEventCoverageTypes(rs.getString("FK_CODELST_COVERTYPE"));
              setEventCoverageNotes(rs.getString("COVERAGE_NOTES"));               
              setStatCodes(rs.getString("FK_CODELST_CALSTAT"));
              setEventLibraryType(rs.getString("EVENT_LIBRARY_TYPE"));
              setCptCodes(rs.getString("EVENT_CPTCODE"));
              
              
          }}
      } catch (Exception ex) {
          Rlog.fatal("eventassoc",
                     "EventAssocDao.getProtSelectedAndVisitEvents: "+ ex);
      } finally {
          try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
          try { if (conn != null) conn.close(); } catch (Exception e) {}
      }
  }
  
  public String getEventMouseOver(int eventId, String eventType, int finDetRight) {
  	StringBuffer mouseOver = new StringBuffer();
  	String accessYN ="0";

  	PreparedStatement pstmt = null;
      Connection conn = null;
      try {
          conn = getConnection();
          StringBuffer eventAssocSql = new StringBuffer();
          eventType = (null == eventType)? "E": eventType;
          
          eventAssocSql.append("SELECT distinct EVENT_ID, ")
          	.append("chain_id category_id,EVENT_TYPE, NAME,")
          	.append("EVENT_LIBRARY_TYPE, (select codelst_desc from sch_codelst where codelst_type = 'lib_type' and pk_codelst = EVENT_LIBRARY_TYPE) as LIBRARY_TYPE_DESC, ")
          	.append("event_cptcode, case when length(notes) > 100 then substr(notes,1,100) || '...' else notes end as notes, ")
          	.append("lst_cost(event_id) as eventcost, ");
	        
          if ("E".equals(eventType)){
        	  accessYN ="1";
          }else{
        	  if (StringUtil.isAccessibleFor(finDetRight, 'V')) {
      			  accessYN ="1";
        	  }
          }
          eventAssocSql.append("esch.lst_additionalcodes_financial(event_id,'evtaddlcode',"+accessYN+") as additionalcode ");
          eventAssocSql
          	.append("FROM EVENT_ASSOC b ")
          	.append("WHERE EVENT_ID = ? ")
          	.append("AND TRIM(UPPER(EVENT_TYPE)) = ?");

          pstmt = conn.prepareStatement(eventAssocSql.toString());
          pstmt.setInt(1, eventId);
          pstmt.setString(2, eventType);
          
          ResultSet rs = pstmt.executeQuery();
          while (rs.next()) {
          	String eventName = rs.getString("NAME");
          	eventName = (null == eventName)? "" : eventName;
          	mouseOver.append("<b>"+LC.L_Event_Name+"</b>: "+eventName);
          	
          	String libraryName = rs.getString("LIBRARY_TYPE_DESC");
          	libraryName = (null == libraryName)? "" : libraryName;
          	mouseOver.append("<br><b>"+LC.L_Evt_Lib+"</b>: "+((libraryName.length()>25) ? libraryName.substring(0,25)+"...":libraryName));

          	if (!"0".equals(accessYN)){
	          	String cptCode = rs.getString("EVENT_CPTCODE");
	          	cptCode = (null == cptCode)? "" : cptCode;
	          	mouseOver.append("<br><b>"+LC.L_Cpt_Code+"</b>: "+cptCode);
          	}
          	String addCode = rs.getString("ADDITIONALCODE");
          	addCode = (null == addCode)? "" : addCode;
       		mouseOver.append("<br><b>"+LC.L_Addl_Codes+"</b>:<br>"+addCode);
          	break;
          }
      } catch (Exception ex) {
          Rlog.fatal("eventassoc",
                     "EventAssocDao.getEventMouseOver: "+ ex);
      } finally {
          try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
          try { if (conn != null) conn.close(); } catch (Exception e) {}
      }
      try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
      try { if (conn != null) conn.close(); } catch (Exception e) {}
      
      return mouseOver.toString();  	
  }  
  public int  saveCoverageTypeByVisitStudyCal(int visitId, int coverageTypeId, String eventName){
  	
  	int rowImpact=0;
  	int vId = visitId;
  	int covId =  coverageTypeId;
  	PreparedStatement pstmt = null;
      Connection conn = null;
      String sqlVisit="";
      try{
      	   conn = getConnection();
      	   sqlVisit="update event_assoc set FK_CODELST_COVERTYPE="+covId+ " where FK_VISIT="+vId+" AND (LOWER(NAME) LIKE LOWER('%"+eventName+"%'))";  
      	  pstmt = conn.prepareStatement(sqlVisit);
	          pstmt.executeUpdate();
	           conn.commit();
      }catch(SQLException e){
      	e.printStackTrace();
      	rowImpact=-1;
      }
      finally {
          try {
              if (pstmt != null)
              	pstmt.close();
          } catch (Exception e) {
          }
          try {
              if (conn != null)
                  conn.close();
          } catch (Exception e) {
          }
      }
  	return rowImpact;
  	
  }
  
  public int saveCoverageTypeByEventToAssoc(int chainId,int eventId,int coverageTypeId){
     int rowImpact=0;
     int cId = chainId;
	 int covId =  coverageTypeId;
	 int eID = eventId;
	PreparedStatement pstmt = null;
    Connection conn = null;
    String sqlEvent="";
    
    try{
    	   conn = getConnection();
    	   sqlEvent=" update event_assoc set FK_CODELST_COVERTYPE=?  where chain_id=? and cost =(select cost from event_assoc where event_id=?) and fk_visit is not null " ;
    	   pstmt = conn.prepareStatement(sqlEvent);
           pstmt.setInt(1, covId);
           pstmt.setInt(2, cId);
           pstmt.setInt(3, eID);
           rowImpact=  pstmt.executeUpdate();
           System.out.println("rowImpact-"+rowImpact);
           conn.commit();
    }catch(SQLException e){
    	e.printStackTrace();
    	
    }
    finally {
        try {
            if (pstmt != null)
            	pstmt.close();
        } catch (Exception e) {
        }
        try {
            if (conn != null)
                conn.close();
        } catch (Exception e) {
        }
    }

    
	return 0;
  }
  
  public void  getEventListForSaveInEventVisitGridFromStudy(int protocolId ,int visitId,String operation){
  	System.out.println("get EventList");
	PreparedStatement pstmt = null;
    Connection conn = null;
    ResultSet rs=null;
    String sqlEvent="";
    try{
    	   conn = getConnection();
    	   if("A".equals(operation)){
    	   sqlEvent=" select event_id, name ,hide_flag from event_assoc where fk_visit is  null and event_type='A' and chain_id=? and cost not in(select  cost from  event_assoc where fk_visit=? ) " ;
    	   }else{
    		   sqlEvent=" select event_id, name,hide_flag  from event_assoc  where fk_visit is  null and event_type='A' and chain_id=? and cost  in(select  cost from  event_assoc  where fk_visit=? ) " ;  
    	   }
    	   pstmt = conn.prepareStatement(sqlEvent);
           pstmt.setInt(1,protocolId);
           pstmt.setInt(2, visitId);
            
            rs = pstmt.executeQuery();
           System.out.println("rowImpact-");
           while (rs.next()) {
        	   setEvent_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("EVENT_ID")).trim()));
        	   setNames(rs.getString("NAME"));
        	   setFlag(rs.getString("HIDE_FLAG"));
           }
           if(event_ids.size()!=0){
        	   System.out.println("size of eventIds-"+ event_ids.size());
           }
           
          // System.out.println("rowImpact-"+rowImpact);
    }catch(SQLException e){
    	e.printStackTrace();
    	
    }
    finally {
        try {
            if (pstmt != null)
            	pstmt.close();
        } catch (Exception e) {
        }
        try {
            if (conn != null)
                conn.close();
        } catch (Exception e) {
        }
    }
	
}
  
  public int setEventCost(int orgId,String chainId,String newCost){  	
  	  PreparedStatement pstmt = null;
      Connection conn = null;
      int ret=0;
      try {
      conn = getConnection();
      StringBuffer eventDefSql = new StringBuffer();
      eventDefSql.append("update event_assoc set cost = ? where chain_id = ? and org_id = ?");
      pstmt = conn.prepareStatement(eventDefSql.toString());
      pstmt.setString(1, newCost);
      pstmt.setString(2, chainId);
      pstmt.setInt(3, orgId);
      ret=  pstmt.executeUpdate();
      }catch (Exception ex) {
          Rlog.fatal("eventAssoc",
                  "EventAssocDao.setEventCost: "+ ex);
   } finally {
       try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
       try { if (conn != null) conn.close(); } catch (Exception e) {}
   }
		return ret;
		
   }
  
  public int setEventsCost(String calId,String[] eventIds,String[] eventNewSeqs,String calledFrom,String userId,String ipAdd){  	
	  CallableStatement cstmt = null;
      Connection conn = null;
      int ret=0;
      try {
      conn = getConnection();
      ArrayDescriptor evtIds = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
  	
	  ARRAY eventPKArray = new ARRAY(evtIds, conn, eventIds);
	  ARRAY eventNewSeqArray = new ARRAY(evtIds, conn, eventNewSeqs);
    cstmt = conn.prepareCall("{call SP_UPDATE_EVESEQ(?,?,?,?,?,?,?)}");
	  cstmt.setInt(1, StringUtil.stringToNum(calId));
	  cstmt.setArray(2, eventPKArray);
	  cstmt.setArray(3, eventNewSeqArray);
	  cstmt.setString(4, calledFrom);
	  cstmt.setString(5, ipAdd);
	  cstmt.setInt(6, StringUtil.stringToNum(userId));
	  cstmt.registerOutParameter(7, Types.INTEGER);

	  cstmt.execute();
	  ret = cstmt.getInt(7);
	  if(ret==-1)
		  throw new SQLException();
      }catch (SQLException ex) {
          Rlog.fatal("eventAssoc",
                  "EventAssocDao.setEventsCost: "+ ex);
   } finally {
       try { if (cstmt != null) cstmt.close(); } catch (Exception e) {}
       try { if (conn != null) conn.close(); } catch (Exception e) {}
   }
		return ret;
		
   }
  
}//end of class
