/*
 * Classname : EventdefDao
 *
 * Version information : 1.0
 *
 * Date: 05/18/2001
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh
 */

package com.velos.esch.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.CtrlDao;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.util.BrowserRows;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.browserRows.BrowserRowsJB;
import com.velos.eres.web.user.UserJB;
import com.velos.esch.business.audit.impl.AuditBean;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

import edu.emory.mathcs.backport.java.util.Arrays;

/* End of Import Statements */

public class EventdefDao extends com.velos.esch.business.common.CommonDAO implements java.io.Serializable {

    ArrayList event_ids;
    
    ArrayList new_event_ids; // YK: Adde for PCAL-19743

    ArrayList org_ids;

    ArrayList chain_ids;

    ArrayList event_types;

    ArrayList names;

    ArrayList notes;

    ArrayList costs;

    ArrayList cost_descriptions;

    ArrayList durations;

    ArrayList user_ids;

    ArrayList linked_uris;

    ArrayList fuzzy_periods;

    ArrayList msg_tos;

    ArrayList status;

    ArrayList descriptions;

    ArrayList displacements;

    ArrayList eventcounts;

    ArrayList start_date_times;

    ArrayList event_exeons;

    ArrayList eventStats;

    ArrayList eventStatDates;

    ArrayList eventStatEndDates;

    ArrayList eventStatNotes;

    ArrayList eventStatAuthors;

    ArrayList crfCounts;

    ArrayList studyNum;

    ArrayList studyVersion;

    ArrayList patprotId;

    ArrayList calName;

    ArrayList protstartDate;

    ArrayList disReason;

    ArrayList disDate;

    ArrayList actualDate;

    ArrayList crfIds;

    ArrayList crfNumbers;

    ArrayList crfStats;

    ArrayList adverseCounts;

    ArrayList visit; // visit no, really.

    // Change Made on 3rd November by Sonia Kaura for enhancements Phase II

    ArrayList week;

    ArrayList day;

    ArrayList month; // SV, 10/25/04, cal-enh-??

    ArrayList flags;//JM: 11Apr08

    // SV, 9/27/04, cal-enh-03-b
    ArrayList fk_visit; // new foreign key field referring to sch_protocol_visit

    ArrayList visitName; // name of defined visit.

    ArrayList fkVisit;

    ArrayList actualDateString;

    Hashtable htScheduleCRF;

    ArrayList cptCodes; //KM

    ArrayList eventCategory;//KM

    ArrayList sequences;


    ArrayList subTypes;

    ArrayList eventStatIds;

    ArrayList isDefault;

    ArrayList eventSOSIds;

    ArrayList eventFacilityIds;

    ArrayList eventCoverageTypes;

    ArrayList eventCoverageNotes;

    ArrayList Facility; //KV:SW-FIN2c
    ArrayList eventCostDetails; //KV:SW-FIN2b

    ArrayList reasonForCoverageChange;

	// BK : Defect 4512 -- 13Aug2010
	ArrayList eventDurationAfterList;

	ArrayList eventDurationBeforeList;

	ArrayList eventFuzzyAfterList;
	
	Integer totalRecords;
	
	/* calendar status */

	private ArrayList statCodes;

    //KM-DFin9
    private ArrayList codeSubTypes;
    
    private ArrayList codeDescriptions;
    
    //BK-D-FIN25-DAY0
    ArrayList insertAfter;  
	ArrayList afterIntervalStrings;
	
	//Ankit: PCAL-20282
	ArrayList eventLibraryType;
	
	ArrayList servicecodes;
    ArrayList prscodes;

    // //////////////////////////
	// Added for INF-18183 ::: Raviesh
    @PersistenceContext(unitName = "esch")
    @Resource private SessionContext context;

    public EventdefDao() {
    	htScheduleCRF = new Hashtable();

        event_ids = new ArrayList();
        new_event_ids = new ArrayList();// YK: Added for PCAL-19743 
        org_ids = new ArrayList();
        chain_ids = new ArrayList();
        event_types = new ArrayList();
        names = new ArrayList();
        notes = new ArrayList();
        costs = new ArrayList();
        cost_descriptions = new ArrayList();
        durations = new ArrayList();
        user_ids = new ArrayList();
        linked_uris = new ArrayList();
        fuzzy_periods = new ArrayList();
        msg_tos = new ArrayList();
        status = new ArrayList();
        descriptions = new ArrayList();
        displacements = new ArrayList();
        eventcounts = new ArrayList();
        start_date_times = new ArrayList();
        event_exeons = new ArrayList();

        eventStats = new ArrayList();
        eventStatDates = new ArrayList();
        eventStatEndDates = new ArrayList();
        eventStatNotes = new ArrayList();
        eventStatAuthors = new ArrayList();

        crfCounts = new ArrayList();

        studyNum = new ArrayList();
        studyVersion = new ArrayList();
        patprotId = new ArrayList();
        calName = new ArrayList();
        protstartDate = new ArrayList();
        disReason = new ArrayList();
        disDate = new ArrayList();

        actualDate = new ArrayList();

        crfIds = new ArrayList();
        crfNumbers = new ArrayList();
        crfStats = new ArrayList();
        adverseCounts = new ArrayList();
        visit = new ArrayList();

        // SONIA KAURA
        month = new ArrayList();
        week = new ArrayList();
        day = new ArrayList();

        // SV, 9/27/04, cal-enh-03-b
        fk_visit = new ArrayList();
        flags = new ArrayList();
        visitName = new ArrayList();

        fkVisit = new ArrayList();
        actualDateString	= new ArrayList();
        cptCodes = new ArrayList(); //KM
        eventCategory = new ArrayList();

        sequences = new ArrayList();
        cptCodes = new ArrayList(); //KM
        eventCategory = new ArrayList();

        subTypes = new ArrayList();
        eventStatIds = new ArrayList();
        isDefault = new ArrayList();



        eventSOSIds = new ArrayList();
        eventFacilityIds = new ArrayList();
        eventCoverageTypes = new ArrayList();
        eventCoverageNotes = new ArrayList();
        Facility = new ArrayList(); //KV:SW-FIN2c
        eventCostDetails = new ArrayList(); //KV:SW-FIN2b
        reasonForCoverageChange = new ArrayList();

     // BK : Defect 4512 -- 16Aug2010
		eventDurationAfterList= new ArrayList<String>();
		eventDurationBeforeList = new ArrayList<String>();
		eventFuzzyAfterList = new ArrayList<String>();
		
		statCodes = new ArrayList();
		
		//KM-DFin9
		codeSubTypes = new ArrayList();
        
        codeDescriptions = new ArrayList();
        //BK-DFIN25-DAY0
        afterIntervalStrings = new ArrayList();
        insertAfter = new ArrayList();
        // Ankit PCAL-20282
        eventLibraryType = new ArrayList();
        servicecodes = new ArrayList();
    	prscodes = new ArrayList();
    }

    // Getter and Setter methods
    
    public ArrayList getServicecodes() {
		return servicecodes;
	}

	public void setServicecodes(ArrayList servicecodes) {
		this.servicecodes = servicecodes;
	}

	public ArrayList getPrscodes() {
		return prscodes;
	}

	public void setPrscodes(ArrayList prscodes) {
		this.prscodes = prscodes;
	}
	
    public void setNewPRSCodes(String String2) {
        this.prscodes.add(String2);
    }

    public void setNewServiceCodes(String String2) {
        this.servicecodes.add(String2);
    }


    public ArrayList getEvent_ids() {
        return this.event_ids;
    }

    public void setEvent_ids(ArrayList event_ids) {
        this.event_ids = event_ids;
    }

    // YK: Added for PCAL-19743
    public ArrayList getNew_event_ids() {
		return new_event_ids;
	}

	public void setNew_event_ids(ArrayList newEventIds) {
		new_event_ids = newEventIds;
	}
	
    public ArrayList getOrg_ids() {
        return this.org_ids;
    }

    public void setOrg_ids(ArrayList org_ids) {
        this.org_ids = org_ids;
    }

    public ArrayList getChain_ids() {
        return this.chain_ids;
    }

    public void setChain_ids(ArrayList chain_ids) {
        this.chain_ids = chain_ids;
    }

    public ArrayList getEvent_types() {
        return this.event_types;
    }

    public void setEvent_types(ArrayList event_types) {
        this.event_types = event_types;
    }

    public ArrayList getNames() {
        return this.names;
    }

    public void setNames(ArrayList names) {
        this.names = names;
    }

    public ArrayList getNotes() {
        return this.notes;
    }

    public void setNotes(ArrayList notes) {
        this.notes = notes;
    }

    public ArrayList getCosts() {
        return this.costs;
    }

    public void setCosts(ArrayList costs) {
        this.costs = costs;
    }

    public ArrayList getCost_descriptions() {
        return this.cost_descriptions;
    }

    public void setCost_descriptions(ArrayList cost_descriptions) {
        this.cost_descriptions = cost_descriptions;
    }

    public ArrayList getDurations() {
        return this.durations;
    }

    public void setDurations(ArrayList durations) {
        this.durations = durations;
    }

    public ArrayList getUser_ids() {
        return this.user_ids;
    }

    public void setUser_ids(ArrayList user_ids) {
        this.user_ids = user_ids;
    }

    public ArrayList getLinked_uris() {
        return this.linked_uris;
    }
    //DFIN25-DAY0
    public ArrayList getAfterIntervalStrings() {
        return this.afterIntervalStrings;
    }
    public void setAfterIntervalStrings(ArrayList pList) {
        this.afterIntervalStrings.addAll(pList);
    }    
    
    public ArrayList getInsertAfter() {
		return insertAfter;
	}

	public void setInsertAfter(ArrayList insertAfter) {
		this.insertAfter = insertAfter;
	}
	public void setInsertAfter(int insertAfter) {
		this.insertAfter.add(insertAfter);
	}
    
    
    
    public void setLinked_uris(ArrayList linked_uris) {
        this.linked_uris = linked_uris;
    }

    public ArrayList getFuzzy_periods() {
        return this.fuzzy_periods;
    }

    public void setFuzzy_periods(ArrayList fuzzy_periods) {
        this.fuzzy_periods = fuzzy_periods;
    }

    public ArrayList getMsg_tos() {
        return this.msg_tos;
    }

    public void setMsg_tos(ArrayList msg_tos) {
        this.msg_tos = msg_tos;
    }

    public ArrayList getStatus() {
        return this.status;
    }

    public void setStatus(ArrayList status) {
        this.status = status;
    }

    public ArrayList getDescriptions() {
        return this.descriptions;
    }

    public void setDescriptions(ArrayList descriptions) {
        this.descriptions = descriptions;
    }

    public void setDescriptions(String description) {
        this.descriptions.add(description);
    }
    
    public ArrayList getDisplacements() {
        return this.displacements;
    }

    public void setDisplacements(ArrayList displacements) {
        this.displacements = displacements;
    }

    public ArrayList getEventcounts() {
        return this.eventcounts;
    }

    public void setEventcounts(ArrayList eventcounts) {
        this.eventcounts = eventcounts;
    }

    public ArrayList getStart_date_times() {
        return this.start_date_times;
    }

    public ArrayList getEvent_exeons() {
        return this.event_exeons;
    }

    public ArrayList getEventStats() {
        return this.eventStats;
    }

    public void setEventStats(ArrayList eventStats) {
        this.eventStats = eventStats;
    }

    public ArrayList getEventStatDates() {
        return this.eventStatDates;
    }

    public void setEventStatDates(ArrayList eventStatDates) {
        this.eventStatDates = eventStatDates;
    }

    public ArrayList getEventStatEndDates() {
        return this.eventStatEndDates;
    }

    public void setEventStatEndDates(ArrayList eventStatEndDates) {
        this.eventStatEndDates = eventStatEndDates;
    }

    public ArrayList getEventStatNotes() {
        return this.eventStatNotes;
    }

    public void setEventStatNotes(ArrayList eventStatNotes) {
        this.eventStatNotes = eventStatNotes;
    }

    public ArrayList getEventStatAuthors() {
        return this.eventStatAuthors;
    }

    public void setEventStatAuthors(ArrayList eventStatAuthors) {
        this.eventStatAuthors = eventStatAuthors;
    }

    public ArrayList getCrfCounts() {
        return this.crfCounts;
    }

    public void setCrfCounts(ArrayList crfCounts) {
        this.crfCounts = crfCounts;
    }
 // BK : Defect 4512 -- 13Aug2010

	public ArrayList<String> getEventDurationAfter() {
		return eventDurationAfterList;
	}

	public ArrayList<String> getEventDurationBefore() {
		return eventDurationBeforeList;
	}

	public ArrayList<String> getEventFuzzyAfter() {
		return eventFuzzyAfterList;
	}

	public void setEventDurationAfter(ArrayList<String> eventDurationAfter) {
		this.eventDurationAfterList = eventDurationAfter;
	}

	public void setEventDurationBefore(ArrayList<String> eventDurationBefore) {
		this.eventDurationBeforeList = eventDurationBefore;
	}

	public void setEventFuzzyAfter(ArrayList<String> eventFuzzyAfter) {
		this.eventFuzzyAfterList = eventFuzzyAfter;
	}
	
	public Integer getTotalRecords() {
			return totalRecords;
	}

	public void setTotalRecords(Integer totalRecords) {
			this.totalRecords = totalRecords;
	}

    // ////////SONIA - KAURA

    public void setMonth(ArrayList month) {
        this.month = month;
    }

    public void setWeek(ArrayList week) {
        this.week = week;
    }

    public void setDay(ArrayList day) {
        this.day = day;
    }

    public ArrayList getMonth() {
        return (this.month);
    }

    public ArrayList getWeek() {
        return (this.week);
    }

    public ArrayList getDay() {
        return (this.day);
    }

    // SV, 9/27/04, cal-enh-03-b
    public ArrayList getFk_visit() {
        return (this.fk_visit);
    }

    public ArrayList getVisitName() {
        return (this.visitName);
    }

    // **

    // for previous schedule

    public ArrayList getStudyNum() {
        return this.studyNum;
    }

    public void setStudyNum(ArrayList studyNum) {
        this.studyNum = studyNum;

    }

    public ArrayList getStudyVersion() {
        return this.studyVersion;
    }

    public void setStudyVersion(ArrayList studyVersion) {
        this.studyVersion = studyVersion;
    }

    public ArrayList getPatprotId() {
        return this.patprotId;
    }

    public void setPatprotId(ArrayList patprotId) {
        this.patprotId = patprotId;
    }

    public ArrayList getCalName() {
        return this.calName;
    }

    public void setCalName(ArrayList calName) {
        this.calName = calName;
    }

    public ArrayList getProtstartDate() {
        return this.protstartDate;
    }

    public void setProtstartDate(ArrayList protstartDate) {
        this.protstartDate = protstartDate;
    }

    public ArrayList getDisReason() {
        return this.disReason;
    }

    public void setDisReason(ArrayList disReason) {
        this.disReason = disReason;
    }

    public ArrayList getDisDate() {
        return this.disDate;
    }

    public void setDisDate(ArrayList disDate) {
        this.disDate = disDate;
    }

    public ArrayList getActualDate() {
        return this.actualDate;
    }

    public void setActualDate(ArrayList actualDate) {
        this.actualDate = actualDate;
    }

    public ArrayList getCrfIds() {
        return this.crfIds;
    }

    public void setCrfIds(ArrayList crfIds) {
        this.crfIds = crfIds;
    }

    public ArrayList getCrfNumbers() {
        return this.crfNumbers;
    }

    public void setCrfNumbers(ArrayList crfNumbers) {
        this.crfNumbers = crfNumbers;
    }

    public ArrayList getCrfStats() {
        return this.crfStats;
    }

    public void setCrfStats(ArrayList crfStats) {
        this.crfStats = crfStats;
    }

    public ArrayList getAdverseCounts() {
        return this.adverseCounts;
    }

    public void setAdverseCounts(ArrayList adverseCounts) {
        this.adverseCounts = adverseCounts;
    }

    // previous schedule end

    // public void setStart_date_times(ArrayList start_date_times)
    // {
    // this.start_date_times = start_date_times;
    // }

    // overloaded funtions over here

    public void setEvent_ids(Integer event_id) {
        event_ids.add(event_id);
    }
    // YK: Added for PCAL-19743
    public void setNew_event_ids(Integer integer) {
    	new_event_ids.add(integer);
     }
    public void setOrg_ids(Integer org_id) {
        org_ids.add(org_id);
    }

    public void setChain_ids(String chain_id) {
        chain_ids.add(chain_id);
    }

    public void setEvent_types(String event_type) {
        event_types.add(event_type);
    }

    public void setNames(String name) {
        names.add(name);
    }

    public void setNotes(String note) {
        notes.add(note);
    }

    public void setCosts(String cost) {
        costs.add(cost);
    }

    public void setCost_descriptions(String cost_description) {
        cost_descriptions.add(cost_description);
    }

    public void setDurations(String duration) {
        durations.add(duration);
    }

    public void setUser_ids(String user_id) {
        user_ids.add(user_id);
    }

    public void setLinked_uris(String linked_uri) {
        linked_uris.add(linked_uri);
    }

    public void setFuzzy_periods(String fuzzy_period) {
        fuzzy_periods.add(fuzzy_period);
    }

    public void setMsg_tos(String msg_to) {
        msg_tos.add(msg_to);
    }

    public void setStatus(String stats) {
        status.add(stats);
    }

    public void setDescription(String description) {
        descriptions.add(description);
    }

    public void setDisplacement(String displacement) {
        displacements.add(displacement);
    }

    public void setEventcounts(String eventcount) {
        eventcounts.add(eventcount);
    }

    public void setStart_date_times(java.sql.Date start_date_time) {
        start_date_times.add(start_date_time);
    }

    public void setEvent_exeons(java.sql.Date event_exeon) {
        event_exeons.add(event_exeon);
    }

    public void setEventStats(String eventStat) {
        eventStats.add(eventStat);
    }

    public void setEventStatDates(String eventStatDate) {
        eventStatDates.add(eventStatDate);
    }

    public void setEventStatEndDates(String eventStatEndDate) {
        eventStatEndDates.add(eventStatEndDate);
    }

    public void setEventStatNotes(String eventStatNote) {
        eventStatNotes.add(eventStatNote);
    }

    public void setEventStatAuthors(String eventStatAuthor) {
        eventStatAuthors.add(eventStatAuthor);
    }

    public void setCrfCounts(Integer crfCount) {
        crfCounts.add(crfCount);
    }

    // Sonia Kaura

    public void setMonth(Integer month) {
        this.month.add(month);
    }

    public void setWeek(Integer week) {
        this.week.add(week);
    }

    public void setDay(Integer day) {
        this.day.add(day);
    }

    // //////////for view previous schedule

    public void setStudyNums(String studynum) {
        studyNum.add(studynum);
    }

    public void setStudyVersions(String studyversion) {
        studyVersion.add(studyversion);
    }

    public void setPatprotIds(String patprotid) {
        patprotId.add(patprotid);
    }

    public void setCalNames(String calname) {
        calName.add(calname);
    }

    public void setProtstartDates(java.sql.Date protstartdate) {
        protstartDate.add(protstartdate);
    }

    public void setDisReasons(String disreason) {
        disReason.add(disreason);
    }

    public void setDisDates(java.sql.Date disdate) {
        disDate.add(disdate);
    }

    // //

    public void setActualDate(java.sql.Date actDate) {
        actualDate.add(actDate);
    }

    public void setCrfIds(Integer crfId) {
        crfIds.add(crfId);
    }

    public void setAdverseCounts(Integer adverseCount) {
        adverseCounts.add(adverseCount);
    }

    public void setCrfNumbers(String crfNumber) {
        crfNumbers.add(crfNumber);
    }

    public void setCrfStats(String crfStat) {
        crfStats.add(crfStat);
    }

    public void setVisit(Integer v) {
        this.visit.add(v);
    }

    public ArrayList getVisit() {
        return this.visit;
    }

    public void setVisit(ArrayList visit) {
        this.visit = visit;
    }

    // SV, 9/27/04, cal-enh-03-b

    public void setFk_visit(ArrayList fk_visit) {
        this.fk_visit = fk_visit;
    }

    public void setFk_visit(Integer eventVisit) {
        this.fk_visit.add(eventVisit);
    }

    public void setVisitName(ArrayList visitName) {
        this.visitName = visitName;
    }

    public void setVisitName(String visitName) {
        this.visitName.add(visitName);
    }

    public ArrayList getFkVisit() {
        return fkVisit;
    }

    public void setFkVisit(ArrayList fkVisit) {
        this.fkVisit = fkVisit;
    }

    public void setFkVisit(String fkVisit) {
        this.fkVisit.add(fkVisit);
    }


   //KM
    public ArrayList getCptCodes() {
        return this.cptCodes;
    }

    public void setCptCodes(ArrayList cptCodes) {
        this.cptCodes = cptCodes;
    }

    public void setCptCodes(String cptCode) {
    	cptCodes.add(cptCode);
    }


    public ArrayList getEventCategory() {
        return this.eventCategory;
    }

    public void setEventCategory(ArrayList eventCategory) {
        this.eventCategory = eventCategory;
    }

    public void setEventCategory(String eveCategory) {
    	eventCategory.add(eveCategory);
    }


    public ArrayList getEventSequences() {
        return this.sequences;
    }

    public void setEventSequences(String sequence) {
        sequences.add(sequence);
    }

    public void setEventSequences(ArrayList sequences) {
        this.sequences = sequences;
    }




    public ArrayList getSubTypes() {
        return this.subTypes;
    }

    public void setSubTypes(String subType) {
    	subTypes.add(subType);
    }

    public void setSubTypes(ArrayList subTypes) {
        this.subTypes = subTypes;
    }

    public ArrayList getEventStatIds() {
        return this.eventStatIds;
    }

    public void setEventStatIds(String eventStatId) {
    	eventStatIds.add(eventStatId);
    }

    public void setEventStatIds(ArrayList eventStatIds) {
        this.eventStatIds = eventStatIds;
    }


    public ArrayList getIsDefault() {
        return this.isDefault;
    }

    public void setIsDefault(String is_default) {
    	isDefault.add(is_default);
    }

    public void setIsDefault(ArrayList isDefault) {
        this.isDefault = isDefault;
    }

    //JM: 11Apr2008

    public ArrayList getFlags() {
        return this.flags;
    }

    public void setFlag(String flag) {
    	flags.add(flag);
    }

    public void setFlag(ArrayList flags) {
        this.flags = flags;
    }


    public ArrayList getEventSOSIds() {
        return this.eventSOSIds;
    }

    public void setEventSOSIds(String eventSOSId) {
    	eventSOSIds.add(eventSOSId);
    }

    public ArrayList getEventFacilityIds() {
        return this.eventFacilityIds;
    }

    public void setEventFacilityIds(String eventFacilityId) {
    	eventFacilityIds.add(eventFacilityId);
    }

    public ArrayList getEventCoverageTypes() {
        return this.eventCoverageTypes;
    }

    public void setEventCoverageTypes(String eventCoverageType) {
    	eventCoverageTypes.add(eventCoverageType);
    }

    public ArrayList getEventCoverageNotes() {
        return this.eventCoverageNotes;
    }

    public void setEventCoverageNotes(String coverageNotes) {
        eventCoverageNotes.add(coverageNotes);
    }

    //KV:SW-FIN2c
    public ArrayList getFacility() {
            return this.Facility;
    }
    public void setFacility(ArrayList facility) {
		Facility = facility;
	}

    public void setFacility(String Facilityid) {
   	Facility.add(Facilityid);


    }
  //KV:SW-FIN2b
	public ArrayList getEventCostDetails() {
		return eventCostDetails;
	}

	public void setEventCostDetails(ArrayList eventCostDetails) {
		this.eventCostDetails = eventCostDetails;
	}
	public void setEventCostDetails(String EventCostDetail) {
		eventCostDetails.add(EventCostDetail);
	}


	public ArrayList getReasonForCoverageChange() {
        return this.reasonForCoverageChange;
    }

    public void setReasonForCoverageChange(String reasonCoverageChange) {
    	reasonForCoverageChange.add(reasonCoverageChange);
    }

    public void setReasonForCoverageChange(ArrayList reasonForCoverageChange) {
		this.reasonForCoverageChange = reasonForCoverageChange;
	}


    //JM: 27JAN2011: Enh- D-FIN9

    public ArrayList getStatCodes() {
    	return statCodes;
    }

    public void setStatCodes(ArrayList statCodes) {
    	statCodes = statCodes;
    }

    public void setStatCodes(String s) {
    	statCodes.add(s);
    }

    public ArrayList getCodeSubTypes() {
    	return codeSubTypes;
    }

    public void setCodeSubTypes(ArrayList subTypes) {
    	subTypes = subTypes;
    }

    public void setCodeSubTypes(String s) {
    	codeSubTypes.add(s);
    }
    
    public ArrayList getCodeDescriptions() {
    	return codeDescriptions;
    }

    public void setCodeDescriptions(ArrayList codeDescriptions) {
    	codeDescriptions = codeDescriptions;
    }

    public void setCodeDescriptions(String s) {
    	codeDescriptions.add(s);
    }
    // Ankit PCAL-20282
	public ArrayList getEventLibraryType() {
        return this.eventLibraryType;
    }
    public void setEventLibraryType(String eventLibraryType) {
        this.eventLibraryType.add(eventLibraryType);
    }







	// end of getter and setter methods

    public String getThisAndNextScheduleVisit(String eventId, String patprotId) {
    	
        Rlog.debug("eventdef", "eventdefdao - getThisAndNextScheduleVisit start");
        PreparedStatement pstmt = null;
        ArrayList eventIds = new ArrayList();
        
        Connection conn = null;
        String visitIds = "";
        try {
            String mysql = "";
            conn = getConnection();
            
            mysql = "SELECT FK_VISIT from SCH_EVENTS1 WHERE EVENT_ID = LPAD(TO_CHAR(" + eventId + "), 10, '0') " 
            	+ " UNION " ;
            
            mysql += "SELECT FK_VISIT from ("
            	+ " SELECT b.FK_VISIT from SCH_EVENTS1 a, SCH_EVENTS1 b " 
            	+ " WHERE a.FK_PATPROT = " + patprotId
            	+ " AND a.SESSION_ID = b.SESSION_ID AND a.FK_VISIT != b.FK_VISIT"
            	+ " AND a.EVENT_ID = LPAD (TO_CHAR (" + eventId + "), 10, '0')"
            	+ " AND b.ACTUAL_SCHDATE >= a.ACTUAL_SCHDATE"
            	+ " ORDER BY b.ACTUAL_SCHDATE,b.FK_VISIT) "
            	+ " WHERE rownum = 1";
            
	        Rlog.debug("eventdef", mysql);
	        
	        pstmt = conn.prepareStatement(mysql);
            ResultSet rs = pstmt.executeQuery();
            
            while (rs.next()) {
            	if (StringUtil.isEmpty(visitIds))
            		visitIds += rs.getInt("FK_VISIT");
            	else
            		visitIds += "," + rs.getInt("FK_VISIT");
            }
        }
        
        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In getThisAndNextScheduleVisit in Eventdef line EXCEPTION IN fetching visits "
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return visitIds;
    }

    public ArrayList findAllMovingEvents(String prevDate, String actualDate,
            String eventId, String patprotId, String statusFlag,String calAssoc,int synchSugDate) {
    	
        Rlog.debug("eventdef", "eventdefdao - findAllMovingEvents start");
        PreparedStatement pstmt = null;
        ArrayList eventIds = new ArrayList();
        
        Connection conn = null;
        try {
            String mysql = "";
            conn = getConnection();
            
            if ("1".equals(statusFlag)) 
        		//Move this event only.
            	return null;
            
            if ("0".equals(statusFlag)) 
            	//Move All subsequent events accordingly.
            	mysql = "SELECT EVENT_ID FROM SCH_EVENTS1 a "
            		+ " WHERE a.FK_PATPROT = " + patprotId
            		+ " AND (a.ACTUAL_SCHDATE >= TO_DATE ('"+ prevDate +"', PKG_DATEUTIL.F_GET_DATEFORMAT) OR a.ACTUAL_SCHDATE IS NULL)"
            		+ " AND a.SESSION_ID = (SELECT b.SESSION_ID FROM SCH_EVENTS1 b WHERE b.EVENT_ID = LPAD (TO_CHAR (" + eventId + "), 10, '0'))";
            
            if ("2".equals(statusFlag)) 
            	//Move the events scheduled for current and next visit
            	mysql = "SELECT EVENT_ID FROM SCH_EVENTS1 a"
            		+ " WHERE a.FK_PATPROT = " + patprotId
            		+ " AND a.FK_VISIT IN ("+getThisAndNextScheduleVisit(eventId, patprotId)+")"
            		+ " AND a.SESSION_ID = (SELECT SESSION_ID FROM SCH_EVENTS1 WHERE EVENT_ID = LPAD (TO_CHAR(" + eventId + "), 10, '0'))";
            
            if ("3".equals(statusFlag)) 
            	//Move all events of this visit.
            	mysql = "SELECT EVENT_ID FROM SCH_EVENTS1 a "
            		+ " WHERE a.FK_PATPROT = " + patprotId
            		+ " AND a.SESSION_ID = (SELECT b.SESSION_ID FROM SCH_EVENTS1 b WHERE b.EVENT_ID = LPAD (TO_CHAR(" + eventId + "), 10, '0'))"
            		+ " AND a.FK_VISIT = (SELECT c.FK_VISIT from SCH_EVENTS1 c WHERE c.EVENT_ID = LPAD(TO_CHAR(" + eventId + "), 10, '0'))";
            
            if ("4".equals(statusFlag)) 
            	//Move all dependent subsequent events accordingly.
            	mysql = "SELECT EVENT_ID FROM SCH_EVENTS1 a "
            		+ " WHERE a.FK_PATPROT = " + patprotId
            		+ " AND (a.ACTUAL_SCHDATE >= TO_DATE('"+ prevDate +"', PKG_DATEUTIL.F_GET_DATEFORMAT) OR a.ACTUAL_SCHDATE IS NULL)"
            		+ " AND a.SESSION_ID = (SELECT b.SESSION_ID FROM SCH_EVENTS1 b WHERE b.EVENT_ID = LPAD (TO_CHAR (" + eventId + "), 10, '0'))"
            		+ " AND fk_visit IN (SELECT PK_PROTOCOL_VISIT FROM SCH_PROTOCOL_VISIT  start with INSERT_AFTER="
            		+ "(SELECT fk_visit FROM SCH_EVENTS1 WHERE event_id = LPAD(TO_CHAR(" + eventId + "), 10, '0')"
				    + " )CONNECT BY PRIOR  PK_PROTOCOL_VISIT =INSERT_AFTER)";
            
            
	        Rlog.debug("eventdef", mysql);
	
	        pstmt = conn.prepareStatement(mysql);
	        ResultSet rs = pstmt.executeQuery();
	        
            while (rs.next()) {
            	eventIds.add(rs.getInt("EVENT_ID"));
            }
        }
        
        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In findAllMovingEvents in Eventdef line EXCEPTION IN fetching moving events "
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return eventIds;
    }

    public int ChangeActualDate(String prevDate, String actualDate,
            String eventId, String patprotId, String statusFlag, String ipAdd,
            String modifiedBy,String calAssoc,int synchSugDate) {

        Rlog.debug("eventdef", "eventdefdao - changeActDate start");
        CallableStatement cstmt = null;
        int success = -1;

        Connection conn = null;
        try {

            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_GENSCH.sp_update_actdt(?,?,?,?,?,?,?,?,?)}");

            if (StringUtil.isEmpty(prevDate) || "null".equals(prevDate.toLowerCase()))
            	cstmt.setNull(1, java.sql.Types.VARCHAR);
            else
            	cstmt.setString(1, prevDate);
            cstmt.setString(2, actualDate);
            cstmt.setString(3, eventId);
            cstmt.setString(4, patprotId);
            cstmt.setString(5, statusFlag);
            cstmt.setString(6, ipAdd);
            cstmt.setString(7, modifiedBy);
            cstmt.setString(8,calAssoc);
            cstmt.setInt(9,synchSugDate);

            cstmt.execute();
            success = 1;

        }

        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In ChangeActualDate in Eventdef line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    public int ChangeActDate(String prevDate, String actualDate,
            String eventId, String patprotId, String statusFlag, String ipAdd,
            String modifiedBy) {

        Rlog.debug("eventdef", "eventdefdao - changeActDate start");
        CallableStatement cstmt = null;
        int success = -1;

        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_GENSCH.sp_update_actdt(?,?,?,?,?,?,?)}");

            if (StringUtil.isEmpty(prevDate) || "null".equals(prevDate.toLowerCase()))
            	cstmt.setNull(1, java.sql.Types.VARCHAR);
            else
            	cstmt.setString(1, prevDate);
            cstmt.setString(2, actualDate);
            cstmt.setString(3, eventId);
            cstmt.setString(4, patprotId);
            cstmt.setString(5, statusFlag);
            cstmt.setString(6, ipAdd);
            cstmt.setString(7, modifiedBy);

            cstmt.execute();
            success = 1;

        }

        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In ChangeActualDate in Eventdef line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    public void getAllLibAndEvents(int user_id) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String mysql = "select EVENT_ID, "
                    + "CHAIN_ID, "
                    + "EVENT_TYPE, "
                    + "NAME, "
                    + "NOTES, "
                    + "COST, "
                    + "COST_DESCRIPTION, "
                    + "DURATION, "
                    + "USER_ID, "
                    + "LINKED_URI, "
                    + "FUZZY_PERIOD, "
                    + "MSG_TO, "
                    //+ "STATUS, "
                    + "FK_CODELST_CALSTAT, "
                    + "DESCRIPTION, "
                    + "DISPLACEMENT "
                    + "FROM EVENT_DEF "
                    + "WHERE  TRIM(USER_ID)= ? AND "
                    + "TRIM(UPPER(EVENT_TYPE)) IN('L','E') ORDER BY  CHAIN_ID,EVENT_TYPE DESC,upper(NAME)";

            Rlog.debug("eventdef", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, user_id);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                String id = rs.getString("EVENT_ID");
                Rlog.debug("eventdef", id);
                id = id.trim();
                Integer test1 = new Integer(id);

                setEvent_ids(test1);

                setChain_ids(rs.getString("CHAIN_ID"));

                setEvent_types(rs.getString("EVENT_TYPE"));

                setNames(rs.getString("NAME"));

                setNotes(rs.getString("NOTES"));

                setCosts(rs.getString("COST"));

                setCost_descriptions(rs.getString("COST_DESCRIPTION"));

                setDurations(rs.getString("DURATION"));

                setUser_ids(rs.getString("USER_ID"));

                setLinked_uris(rs.getString("LINKED_URI"));

                setFuzzy_periods(rs.getString("FUZZY_PERIOD"));

                setMsg_tos(rs.getString("MSG_TO"));
                //KM-DFIN9
                setStatCodes(rs.getString("FK_CODELST_CALSTAT"));
                setDescription(rs.getString("DESCRIPTION"));
                setDisplacement(rs.getString("DISPLACEMENT"));

            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.getAllLibAndEvents EXCEPTION IN FETCHING FROM Eventdef table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /*
     * public void EditProtocolEvents(int protocol_id,String[][] event_disps) {
     * PreparedStatement pstmt = null; Connection conn = null; try { conn =
     * getConnection(); String[][] event_disps1 = event_disps; ArrayDescriptor
     * descriptor = ArrayDescriptor.createDescriptor( "NUM_ARRAY", conn); ARRAY
     * array_to_pass = new ARRAY( descriptor, conn, event_disps1 );
     *
     * OraclePreparedStatement ps =
     * (OraclePreparedStatement)conn.prepareStatement ( "begin
     * give_me_an_array(:x,:x); end;" );
     *
     * ps.setInt( 1, protocol_id ); ps.setARRAY( 1, array_to_pass );
     *
     * ps.execute();
     *
     *  } catch (SQLException ex) {
     * Rlog.fatal("eventdef","EventdefDao.getEventdefValues EXCEPTION IN
     * FETCHING FROM Eventdef table" + ex); } finally { try { if (pstmt != null)
     * pstmt.close(); } catch (Exception e) { } try { if (conn!=null)
     * conn.close(); } catch (Exception e) {} } }
     */

    /**
     * Fetches Event Status History for an event
     *
     * @param eventId
     *
     */

    public void getEventStatusHistoryExcludeCurrent(String eventId) {
    	try {
    		this.getEventStatusHistory(eventId);
    		this.eventStatDates.remove(0);
            this.eventStatNotes.remove(0);
            this.subTypes.remove(0);
            this.eventStats.remove(0);
            this.eventStatEndDates.remove(0);
            this.eventStatIds.remove(0);
            this.isDefault.remove(0);
            this.eventStatAuthors.remove(0);
    		
    	} catch (Exception e) {
            Rlog.fatal("eventdef",
            	"EventdefDao.getEventStatusHistoryExcludeCurrent EXCEPTION IN FETCHING FROM sch_eventstat table"+ e);
    	}
    }
    
    
    
    public void getEventStatusHistoryExcludeCurrent(Integer[] eventId) {
    	try {
    		this.getEventStatusHistory(eventId);
    		this.eventStatDates.remove(0);
            this.eventStatNotes.remove(0);
            this.subTypes.remove(0);
            this.eventStats.remove(0);
            this.eventStatEndDates.remove(0);
            this.eventStatIds.remove(0);
            this.isDefault.remove(0);
            this.eventStatAuthors.remove(0);
    		
    	} catch (Exception e) {
            Rlog.fatal("eventdef",
            	"EventdefDao.getEventStatusHistoryExcludeCurrent EXCEPTION IN FETCHING FROM sch_eventstat table"+ e);
    	}
    }
    
    
    
    
    
    
    
    /*
     * *************************History*********************************
     * Modified by : Anu Modified On:06/16/2004 Comment: For bug
     * #1535(consistency is user name)
     *
     * *************************END****************************************
     */
    public void getEventStatusHistory(String eventId) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
           //Query  modified by KN for defect fix 2897
            //KM-Modified for the issue 3818
            //Rohit Modified for Bug # 5079
            String mysql = "select EVENTSTAT_DT, " + "EVENTSTAT_NOTES, "
            + "sch_codelst.CODELST_SUBTYP codeSubType, "
            + "sch_codelst.codelst_desc status, " + "EVENTSTAT_ENDDT, "
            + " (select (USR_FIRSTNAME || ' ' || USR_LASTNAME) from er_user where pk_user = sch_eventstat.CREATOR ) created_by, "
            + " (select (USR_FIRSTNAME || ' ' || USR_LASTNAME) from er_user where pk_user = sch_eventstat.LAST_MODIFIED_BY ) entered_by, "
            + "PK_EVENTSTAT, "
            + "((case sch_eventstat.rid "
            + "when (select min(a.rid) from sch_eventstat a where a.fk_event = sch_eventstat.fk_event)  then 1 "
            + "else 0 "
            + "end )) is_default "
            + "FROM sch_eventstat, "
            + "sch_codelst WHERE sch_eventstat.FK_EVENT = ? and "
            + "sch_codelst.PK_CODELST = sch_eventstat.EVENTSTAT "
            + "order by EVENTSTAT_ENDDT desc";
            //+ " order by EVENTSTAT_DT asc, EVENTSTAT_ENDDT asc";


            pstmt = conn.prepareStatement(mysql);
            pstmt.setString(1, eventId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setEventStatDates(rs.getString("EVENTSTAT_DT"));
                setEventStatNotes(rs.getString("EVENTSTAT_NOTES"));
                setSubTypes(rs.getString("codeSubType"));
                setEventStats(rs.getString("status"));
                setEventStatEndDates(rs.getString("EVENTSTAT_ENDDT"));
                setEventStatIds(rs.getString("PK_EVENTSTAT"));
                setIsDefault(rs.getString("is_default"));
                // code modified by KN for defect fix 2897
                //String modified_by = rs.getString("entered_by");
                //if(modified_by == null)
                   setEventStatAuthors(rs.getString("created_by"));
                //else
                  // setEventStatAuthors(modified_by);

            }

        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "eventdef",
                            "EventdefDao.getEventStatusHistory EXCEPTION IN FETCHING FROM sch_eventstat table"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    
    
    public void getEventStatusHistory(Integer[] eventId) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        
   	 String parameters = StringUtils.join(Arrays.asList(eventId).iterator(),",");  

        try {
            conn = getConnection();
           //Query  modified by KN for defect fix 2897
            //KM-Modified for the issue 3818
            //Rohit Modified for Bug # 5079
            String mysql = "select EVENTSTAT_DT, " + "EVENTSTAT_NOTES, "
            + "sch_codelst.CODELST_SUBTYP codeSubType, "
            + "sch_codelst.codelst_desc status, " + "EVENTSTAT_ENDDT, "
            + " (select (USR_FIRSTNAME || ' ' || USR_LASTNAME) from er_user where pk_user = sch_eventstat.CREATOR ) created_by, "
            + " (select (USR_FIRSTNAME || ' ' || USR_LASTNAME) from er_user where pk_user = sch_eventstat.LAST_MODIFIED_BY ) entered_by, "
            + "PK_EVENTSTAT, "
            + "((case sch_eventstat.rid "
            + "when (select min(a.rid) from sch_eventstat a where a.fk_event = sch_eventstat.fk_event)  then 1 "
            + "else 0 "
            + "end )) is_default "
            + "FROM sch_eventstat, "
            + "sch_codelst WHERE sch_eventstat.FK_EVENT in ("+parameters+")and "
            + "sch_codelst.PK_CODELST = sch_eventstat.EVENTSTAT "
            + "order by EVENTSTAT_ENDDT desc";
            //+ " order by EVENTSTAT_DT asc, EVENTSTAT_ENDDT asc";


            pstmt = conn.prepareStatement(mysql);
            pstmt.setFetchSize(500);

         //   pstmt.setString(1, eventId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setEventStatDates(rs.getString("EVENTSTAT_DT"));
                setEventStatNotes(rs.getString("EVENTSTAT_NOTES"));
                setSubTypes(rs.getString("codeSubType"));
                setEventStats(rs.getString("status"));
                setEventStatEndDates(rs.getString("EVENTSTAT_ENDDT"));
                setEventStatIds(rs.getString("PK_EVENTSTAT"));
                setIsDefault(rs.getString("is_default"));
                // code modified by KN for defect fix 2897
                String modified_by = rs.getString("entered_by");
                if(modified_by == null)
                   setEventStatAuthors(rs.getString("created_by"));
                else
                   setEventStatAuthors(modified_by);

            }

        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "eventdef",
                            "EventdefDao.getEventStatusHistory EXCEPTION IN FETCHING FROM sch_eventstat table"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    
    
    
    
    
    
    public void getFilteredEvents(int user_id, int protocol_id) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            Integer iValue = new Integer(0);
            String sValue = "";

            conn = getConnection();
            String mysql = "select EVENT_ID, " + "CHAIN_ID, " + "EVENT_TYPE, "
                    + "NAME, " + "NOTES, " + "COST, " + "COST_DESCRIPTION, "
                    + "DURATION, " + "USER_ID, " + "LINKED_URI, "
                    + "FUZZY_PERIOD, " + "MSG_TO, " 
                    + "DESCRIPTION, " + "DISPLACEMENT " + "FROM EVENT_DEF "
                    + "WHERE  TRIM(USER_ID)= ? AND "
                    + "TRIM(UPPER(EVENT_TYPE)) IN ('L','E') "
                    // + " AND EVENT_ID NOT IN (SELECT ORG_ID FROM EVENT_DEF
                    // WHERE EVENT_TYPE = 'A' AND CHAIN_ID = ?) "
                    + " ORDER BY CHAIN_ID,EVENT_ID";
            /*
             * + "MINUS select EVENT_ID, " + "CHAIN_ID, " + "EVENT_TYPE, " +
             * "NAME, " + "NOTES, " + "COST, " + "COST_DESCRIPTION, " +
             * "DURATION, " + "USER_ID, " + "LINKED_URI, " + "FUZZY_PERIOD, " +
             * "MSG_TO, " + "STATUS, " + "DESCRIPTION, " + "DISPLACEMENT " +
             * "FROM EVENT_DEF WHERE CHAIN_ID = ? AND EVENT_TYPE = 'A' " + "
             * ORDER BY CHAIN_ID,EVENT_ID" ;
             */

            Rlog.debug("eventdef", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, user_id);
            // pstmt.setInt(2,protocol_id);

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("eventdef", "After execution" + rs.toString());

            while (rs.next()) {
                Rlog.debug("eventdef", "eventdef line 1");
                String id = rs.getString("EVENT_ID");
                Rlog.debug("eventdef", id);
                id = id.trim();
                Integer test1 = new Integer(id);

                setEvent_ids(test1);

                setChain_ids(rs.getString("CHAIN_ID"));

                setEvent_types(rs.getString("EVENT_TYPE"));

                setNames(rs.getString("NAME"));

                setNotes(rs.getString("NOTES"));

                setCosts(rs.getString("COST"));

                setCost_descriptions(rs.getString("COST_DESCRIPTION"));

                setDurations(rs.getString("DURATION"));

                setUser_ids(rs.getString("USER_ID"));

                setLinked_uris(rs.getString("LINKED_URI"));

                setFuzzy_periods(rs.getString("FUZZY_PERIOD"));

                setMsg_tos(rs.getString("MSG_TO"));
                //KM-#DFin9
                setDescription(rs.getString("DESCRIPTION"));
                setDisplacement(rs.getString("DISPLACEMENT"));

            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.getFilteredEvents EXCEPTION IN FETCHING FROM Eventdef table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getFilteredRangeEvents(int protocolId, int beginDisp,
            int endDisp) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            Integer iValue = new Integer(0);
            String sValue = "";

            conn = getConnection();
            String mysql = "select EVENT_ID,ORG_ID ," + "CHAIN_ID, "
                    + "EVENT_TYPE, " + "NAME, " + "NOTES, " + "COST, "
                    + "COST_DESCRIPTION, " + "DURATION, " + "USER_ID, "
                    + "LINKED_URI, " + "FUZZY_PERIOD, " + "MSG_TO, "
                    + " DESCRIPTION, " + "DISPLACEMENT, "
                    + " FK_VISIT " // SV, 9/27/04
                    + "FROM EVENT_DEF ";

            if (beginDisp == 1)
                mysql = mysql
                        + "WHERE  CHAIN_ID = ? AND ((DISPLACEMENT>= ? and DISPLACEMENT <= ?) or (DISPLACEMENT<0)) ";
            else
                mysql = mysql
                        + "WHERE  CHAIN_ID = ? AND (DISPLACEMENT>= ? and DISPLACEMENT <= ?) ";

            mysql = mysql + " ORDER BY EVENT_TYPE DESC ,COST,DISPLACEMENT";

            Rlog.debug("eventdef", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, protocolId);
            pstmt.setInt(2, beginDisp);
            pstmt.setInt(3, endDisp);
            // pstmt.setInt(4,protocolId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                String id = rs.getString("EVENT_ID");
                Rlog.debug("eventdef", id);
                id = id.trim();
                Integer test1 = new Integer(id);
                setEvent_ids(test1);

                String id1 = rs.getString("ORG_ID");
                id1 = id1.trim();
                Integer test2 = new Integer(id1);
                setOrg_ids(test2);

                setChain_ids(rs.getString("CHAIN_ID"));

                setEvent_types(rs.getString("EVENT_TYPE"));

                setNames(rs.getString("NAME"));

                setNotes(rs.getString("NOTES"));

                setCosts(rs.getString("COST"));

                setCost_descriptions(rs.getString("COST_DESCRIPTION"));

                setDurations(rs.getString("DURATION"));

                setUser_ids(rs.getString("USER_ID"));

                setLinked_uris(rs.getString("LINKED_URI"));

                setFuzzy_periods(rs.getString("FUZZY_PERIOD"));

                setMsg_tos(rs.getString("MSG_TO"));
                //KM-DFin9
                setDescription(rs.getString("DESCRIPTION"));
                setDisplacement(rs.getString("DISPLACEMENT"));

                // SV, 9/27/04, cal-enh-03-b
                sValue = rs.getString("FK_VISIT");
                if (sValue == null)
                    iValue = new Integer(0);
                else
                    iValue = new Integer(sValue.trim());
                setFk_visit(iValue);

            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.getFilteredRangeEvents EXCEPTION IN FETCHING FROM Eventdef table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }


    //KM-Modified for Enh#C7
    public int RemoveProtEventInstance(String[] deleteIds, String whichTable) {

        CallableStatement cstmt = null;
        Rlog.debug("eventdef",
                "In RemoveProtEventInstance in EventdefDao line number 0");
        int newProtocol = -1;

        Connection conn = null;
        try {
            conn = getConnection();

            ArrayDescriptor inIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY deleteIdsArray = new ARRAY(inIds, conn, deleteIds);


            cstmt = conn.prepareCall("{call sp_deleve(?,?)}");

            cstmt.setArray(1, deleteIdsArray);
            //cstmt.setInt(1, eventId);

            cstmt.setString(2, whichTable);
            cstmt.execute();
        }

        catch (SQLException ex) {
        	newProtocol = 0; //Ankit 03-Oct-2011 Bug#7041
            Rlog
                    .fatal(
                            "eventdef",
                            "In RemoveProtEventInstance in Eventdef line EXCEPTION IN calling the procedure"
                                    + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return newProtocol;
    }


    public int CopyEventDetails(String old_event_id, String new_event_id,
            String crfInfo, String userId, String ipAdd) {

        CallableStatement cstmt = null;

        int newProtocol = 0;

        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_cpevedtls(?,?,?,?,?,?)}");

            cstmt.setInt(1, EJBUtil.stringToNum(old_event_id));

            cstmt.setInt(2, EJBUtil.stringToNum(new_event_id));

            cstmt.setString(3, crfInfo);

            cstmt.registerOutParameter(4, java.sql.Types.INTEGER);

            cstmt.setString(5, userId);
            cstmt.setString(6, ipAdd);

            cstmt.execute();

        }

        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In CopyEventDetails in Eventdef line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return newProtocol;
    }

    // SV, 8/19/04, part of fix for bug #1602, original function now calls with
    // 0 for duration.
    public int DeleteProtocolEvents(String chain_id) {
        return (DeleteProtocolEvents(chain_id, 0));
    }

    // SV, 8/19/04, new method with duration parameter. If duration > 0, the
    // stored procedure will only delete those events w/ displacement >
    // duration.
    // Also, modified the stored procedure to receive duration with a default
    // value of 0, for duration, so it can still function with one (chain id)
    // argument.
    public int DeleteProtocolEvents(String chain_id, int duration) {

        CallableStatement cstmt = null;
        Integer chainid = new Integer(chain_id);
        int success = -1;

        // OracleCallableStatement cstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_deleteproevents(?, ?)}");
            cstmt.setInt(1, chainid.intValue());
            cstmt.setInt(2, duration); // SV, 8/19
            cstmt.execute();
            success = 1;

        }

        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In DeleteProtocolEvents in Eventdef line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    public int EditProtoEvent(String chain_id, int fromDisp, int toDisp) {

        CallableStatement cstmt = null;
        Integer chainid = new Integer(chain_id);
        int success = -1;

        // OracleCallableStatement cstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_editproevent(?,?,?)}");
            cstmt.setInt(1, chainid.intValue());
            cstmt.setInt(2, fromDisp);
            cstmt.setInt(3, toDisp);
            cstmt.execute();
            success = 1;

        }

        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In EditProtoEvent in Eventdef line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

   //KM-02Sep09
    public int CopyEventToCat(int event_id, int whichTable, int category_id, int evtLibType,
            String name, String userId, String ipAdd) {

        CallableStatement cstmt = null;
        int success = -1;

        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_copyeventtocat(?,?,?,?,?,?,?)}");
            cstmt.setInt(1, event_id);
            cstmt.setInt(2, whichTable);
            cstmt.setInt(3, category_id);
            cstmt.setInt(4, evtLibType);
            cstmt.setString(5, name);
            cstmt.setString(6, userId);
            cstmt.setString(7, ipAdd);
            cstmt.execute();
            success = 1;

        }

        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In CopyEventToCat in Eventdef line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    public int DeleteDuplicateEvents(String chain_id) {

        CallableStatement cstmt = null;
        Integer chainid = new Integer(chain_id);
        int success = -1;

        // OracleCallableStatement cstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_deleteduplicate(?)}");
            cstmt.setInt(1, chainid.intValue());
            cstmt.execute();
            success = 1;

        }

        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In DeleteDuplicateEvents in Eventdef line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    public int MarkDone(int event_id, String notes, java.sql.Date exe_date,
            int exe_by, int status, int oldStatus, String ipAdd, String mode,
            int eventSOSId, int eventCoverType, String reasonForCoverageChange ) {

        CallableStatement cstmt = null;
        int success = -1;

        // OracleCallableStatement cstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_markdone(?,?,?,?,?,?,?,?,?,?)}");
            cstmt.setInt(1, event_id);
            cstmt.setString(2, notes);
            cstmt.setDate(3, exe_date);
            cstmt.setInt(4, exe_by);
            cstmt.setInt(5, status);
            cstmt.setString(6, ipAdd);
            cstmt.setString(7, mode);//JM: 29Aug2008 #3586
            cstmt.setInt(8, eventSOSId);
            cstmt.setInt(9, eventCoverType);
            cstmt.setString(10, reasonForCoverageChange);//JM: 10Jun2010 enh-#D-FIN23
            cstmt.execute();
            success = 1;

            cstmt = conn.prepareCall("{call SP_EVENTNOTIFY(?,?,?,?,?)}");
            cstmt.setInt(1, event_id);
            cstmt.setInt(2, status);
            cstmt.setInt(3, exe_by);
            cstmt.setString(4, ipAdd);
            cstmt.setInt(5, oldStatus);
            cstmt.execute();
            success = 1;

        }

        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In MarkDone in Eventdef line EXCEPTION IN calling the procedure"
                            + ex);
        }

        finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    public int CopyProtocol(String protocol_id, String name, String calType, String userId,
            String ipAdd) {

        CallableStatement cstmt = null;
        Rlog.debug("eventdef", "In CopyProtocol in EventdefDao line number 0");
        int newProtocol = -1;

        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_copyprotocol(?,?,?,?,?,?)}");

            cstmt.setInt(1, EJBUtil.stringToNum(protocol_id));
            cstmt.setString(2, name);

            cstmt.setString(3, calType);

            cstmt.registerOutParameter(4, java.sql.Types.INTEGER);
            cstmt.setString(5, userId);
            cstmt.setString(6, ipAdd);
            cstmt.execute();

            newProtocol = cstmt.getInt(4);

        }

        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In CopyProtocol in Eventdef line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return newProtocol;
    }

    public int GenerateSchedule(int protocol_id, int patient_id,
            java.sql.Date st_date, int patProtId, Integer days, String modifiedBy,
            String ipAdd) {

        CallableStatement cstmt = null;
        int success = -1;

        Connection conn = null;

        com.velos.eres.business.common.CommonDAO cd = new CommonDAO();

        try {
            conn = cd.getConnection(); //get eres connection
            cstmt = conn
                    .prepareCall("{call PKG_RESCHEDULE.sp_generate_schedule(?,?,?,?,?,?,?,?)}");

            cstmt.setInt(1, protocol_id);

            cstmt.setInt(2, patient_id);

            cstmt.setDate(3, st_date);
            cstmt.setInt(4, patProtId);
            if (null == days)
            	cstmt.setNull(5, java.sql.Types.NUMERIC);
            else
            	cstmt.setInt(5, days);
            cstmt.setString(6, modifiedBy);
            cstmt.setString(7, ipAdd);
            cstmt.registerOutParameter(8, java.sql.Types.INTEGER);
            cstmt.execute();

            success = 1;

        }

        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In GenerateSchedule in Eventdef line EXCEPTION IN calling the procedure PKG_RESCHEDULE.sp_generate_schedule(?,?,?,?,?,?,?,?)"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    public int DeactivateEvents(int protocol_id, int patient_id,
            java.sql.Date st_date,String type,int User) {

        CallableStatement cstmt = null;
        int success = -1;

        Connection conn = null;

        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_deactevents(?,?,?,?,?)}");

            cstmt.setInt(1, protocol_id);

            cstmt.setInt(2, patient_id);

            cstmt.setDate(3, st_date);
            cstmt.setString(4, type);
            cstmt.setInt(5, User);
            cstmt.execute();

            success = 1;

        }

        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In DeactivateEvents in Eventdef line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    /* Added by Deepali on 20 August */

    public void getTotalVisit(int protocol_id, int patient_id,
            String pro_start_date) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String mysql = "select Distinct visit" + " FROM SCH_EVENTS1 "
                    + " WHERE  PATIENT_ID= lpad(to_char(?),10,'0') AND "
                    + " SESSION_ID= lpad(to_char(?),10,'0') "
                    + " AND BOOKEDON=TO_DATE('" + pro_start_date
                    + "','yyyy-mm-dd')" + " and status = 0 "
                    + " ORDER BY VISIT";

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, patient_id);
            pstmt.setInt(2, protocol_id);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setVisit(new Integer(rs.getInt("visit")));
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.getTotalVisit EXCEPTION IN FETCHING FROM Eventdef table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    public void getSchEvents(int protocol_id, int patient_id,
            String pro_start_date) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String mysql = "select Distinct DESCRIPTION" + " FROM SCH_EVENTS1 "
                    + " WHERE  PATIENT_ID= lpad(to_char(?),10,'0') AND "
                    + " SESSION_ID= lpad(to_char(?),10,'0') "
                    + " AND BOOKEDON=TO_DATE('" + pro_start_date
                    + "','yyyy-mm-dd')" + " and status = 0 "
                    + " ORDER BY DESCRIPTION";

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, patient_id);
            pstmt.setInt(2, protocol_id);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setNames(rs.getString("DESCRIPTION"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.getSchEvents EXCEPTION IN FETCHING FROM Eventdef table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    public void FetchFilteredSchedule(int protocol_id, int patient_id,
            String pro_start_date, String visit, String month, String year,
            String event, String status) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String mysql = "select m.DESCRIPTION, m.START_DATE_TIME, m.VISIT_TYPE_ID,"
                    + " m.EVENT_ID,m.ISCONFIRMED, m.FK_ASSOC, m.NOTES,  m.EVENT_EXEON,"
                    + " m.ACTUAL_SCHDATE,  m.EVENT_EXEBY,m.ADVERSE_COUNT, m.crfCount, "
                    + " m.visit "
                    + " from ("
                    + "select DESCRIPTION, "
                    + "START_DATE_TIME, "
                    + "VISIT_TYPE_ID,"
                    + "EVENT_ID, "
                    + "ISCONFIRMED, "
                    + "	FK_ASSOC, "
                    + "	NOTES, "
                    + "	EVENT_EXEON, "
                    + "	ACTUAL_SCHDATE, "
                    + "	EVENT_EXEBY, "
                    + "	ADVERSE_COUNT, "
                    + "(select count(*) from sch_crf where fk_events1 in "
                    + "(select a.event_id from sch_events1 a "
                    + "where a.PATIENT_ID= lpad(to_char(?),10,'0') "
                    + "AND a.SESSION_ID= lpad(to_char(?),10,'0') "
                    + "and a.VISIT= sch_events1.visit "
                    + "and a.status = 0 )) crfCount, "
                    + "visit, "
                    + "status "
                    + " FROM SCH_EVENTS1 "
                    + " WHERE  PATIENT_ID= lpad(to_char(?),10,'0') AND "
                    + " SESSION_ID= lpad(to_char(?),10,'0') "
                    + " AND BOOKEDON=TO_DATE('"
                    + pro_start_date
                    + "','yyyy-mm-dd')"
                    + " and status = 0 "
                    + " ) m where status = 0 ";

            if (!(visit.equals(""))) {
                mysql = mysql + " and visit =" + visit + " ";
            }
            if (!(year.equals(""))) {
                mysql += " and to_Char(ACTUAL_SCHDATE,'yyyy') = " + year;
            }
            if (!(month.equals(""))) {
                mysql += " and to_Char(ACTUAL_SCHDATE,'mm') = " + month;
            }
            if (!(event.equals(""))) {
                mysql += " and UPPER(trim(DESCRIPTION)) = UPPER(trim('" + event
                        + "'))";
            }
            if (!(status.equals(""))) {
                mysql += " and ISCONFIRMED = '" + status + "'";
            }

            mysql += " ORDER BY ACTUAL_SCHDATE, START_DATE_TIME, EVENT_ID ";


            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, patient_id);
            pstmt.setInt(2, protocol_id);
            pstmt.setInt(3, patient_id);
            pstmt.setInt(4, protocol_id);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setEvent_ids(Integer.valueOf(rs.getString("EVENT_ID")));
                setNames(rs.getString("DESCRIPTION"));
                setFuzzy_periods(rs.getString("VISIT_TYPE_ID"));
                setStart_date_times(rs.getDate("START_DATE_TIME"));
                setStatus(rs.getString("ISCONFIRMED"));
                setOrg_ids(Integer.valueOf(rs.getString("FK_ASSOC")));
                setNotes(rs.getString("NOTES"));
                setActualDate(rs.getDate("ACTUAL_SCHDATE"));
                setEvent_exeons(rs.getDate("EVENT_EXEON"));
                setUser_ids(rs.getString("EVENT_EXEBY"));
                setAdverseCounts(new Integer(rs.getInt("ADVERSE_COUNT")));
                setCrfCounts(new Integer(rs.getInt("crfCount")));
                setVisit(new Integer(rs.getInt("visit")));
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.FetchFilteredSchedule EXCEPTION IN FETCHING FROM Eventdef table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /** *********************Ends*************************** */

    public void FetchSchedule(int protocol_id, int patient_id,
            String pro_start_date) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String mysql = "select DESCRIPTION, " + "START_DATE_TIME, "
                    + "VISIT_TYPE_ID," + "EVENT_ID, " + "ISCONFIRMED, "
                    + "	FK_ASSOC, " + "	NOTES, " + "	EVENT_EXEON, "
                    + "	ACTUAL_SCHDATE, " + "	EVENT_EXEBY, "
                    + "	  visit , visit_name,(select codelst_desc from sch_codelst where pk_codelst = isconfirmed) status_desc "
                    + " FROM SCH_EVENTS1 ,sch_protocol_visit"
                    + " WHERE  PATIENT_ID= lpad(to_char(?),10,'0') AND "
                    + " SESSION_ID= lpad(to_char(?),10,'0') "
                    + " AND BOOKEDON=TO_DATE('" + pro_start_date
                    + "','yyyy-mm-dd')" + " and status = 0 and fk_visit = pk_protocol_visit"
                    + " ORDER BY ACTUAL_SCHDATE, START_DATE_TIME, EVENT_ID ";

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, patient_id);
            pstmt.setInt(2, protocol_id);
            pstmt.setInt(3, patient_id);
            pstmt.setInt(4, protocol_id);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setEvent_ids(Integer.valueOf(rs.getString("EVENT_ID")));
                setNames(rs.getString("DESCRIPTION"));
                setFuzzy_periods(rs.getString("VISIT_TYPE_ID"));
                setStart_date_times(rs.getDate("START_DATE_TIME"));
                setStatus(rs.getString("ISCONFIRMED"));
                setOrg_ids(Integer.valueOf(rs.getString("FK_ASSOC")));
                setNotes(rs.getString("NOTES"));
                setEvent_exeons(rs.getDate("EVENT_EXEON"));
                setActualDate(rs.getDate("ACTUAL_SCHDATE"));
                setUser_ids(rs.getString("EVENT_EXEBY"));
                //setAdverseCounts(new Integer(rs.getInt("ADVERSE_COUNT")));
                //setCrfCounts(new Integer(rs.getInt("crfCount")));
                setVisit(new Integer(rs.getInt("visit")));
                setVisit(new Integer(rs.getInt("visit_name")));
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.FetchSchedule EXCEPTION IN FETCHING FROM Eventdef table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public int getCount(int protocol_id, String user_id, String name,
            String mode) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        String mysql = "";
        try {
            conn = getConnection();
            if (mode.equals("N")) {
                mysql = "select  COUNT(*) AS EVENTCOUNT  FROM EVENT_DEF WHERE TRIM(UPPER(EVENT_TYPE)) IN('P') AND NAME=? AND USER_ID = ?";
            } else if (mode.equals("U")) {
                mysql = "select  COUNT(*) AS EVENTCOUNT  FROM EVENT_DEF WHERE TRIM(UPPER(EVENT_TYPE)) IN('P') AND NAME=? AND USER_ID = ? AND CHAIN_ID != ?";

            }

            Rlog.debug("eventdef", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setString(1, name);
            pstmt.setString(2, user_id);
            if (mode.equals("U")) {
                pstmt.setInt(3, protocol_id);

            }

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("eventdef", "After execution" + rs.toString());

            while (rs.next()) {

                Integer count = new Integer(rs.getString("EVENTCOUNT"));
                return count.intValue();

            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.getCount EXCEPTION IN FETCHING FROM Eventdef table"
                            + ex);

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return 0;
    }

    public void getUserProtAndEvents(int user_id) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String mysql = "select CHAIN_ID, "
                    + "EVENT_TYPE, "
                    + "NAME,"
                    + "COUNT(*) AS EVENTCOUNT"
                    + " FROM EVENT_DEF "
                    + "WHERE  TRIM(USER_ID)= ? AND "
                    + "TRIM(UPPER(EVENT_TYPE)) IN('P','A')"
                    + " GROUP BY ORG_ID, NAME , CHAIN_ID , EVENT_TYPE ORDER BY CHAIN_ID,EVENT_TYPE DESC";

            Rlog.debug("eventdef", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, user_id);

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("eventdef", "After execution" + rs.toString());

            while (rs.next()) {

                setChain_ids(rs.getString("CHAIN_ID"));

                setEvent_types(rs.getString("EVENT_TYPE"));

                setNames(rs.getString("NAME"));

                setEventcounts(rs.getString("EVENTCOUNT"));

            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.getUserProtAndEvents EXCEPTION IN FETCHING FROM Eventdef table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Fetches all the header events corrosponding to a user
     *
     * @param userId
     * @param type
     *            p/P Protocol s/S Study l/L Library added on 23rd May
     */

    //KM
    public void getHeaderList(int user_id, String type, int evtLibType) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            String mysql = "select EVENT_ID, " + "CHAIN_ID, " + "EVENT_TYPE, "
                    + "NAME, " + "NOTES, " + "COST, " + "COST_DESCRIPTION, "
                    + "DURATION, " + "USER_ID, " + "LINKED_URI, "
                    //+ "FUZZY_PERIOD, " + "MSG_TO, " + "STATUS, "
                    //KM-Enh-D-FIN9
                    + "FUZZY_PERIOD, " + "MSG_TO, " + "FK_CODELST_CALSTAT, " 
                    + "DESCRIPTION, " + "DISPLACEMENT " + "FROM EVENT_DEF "
                    + "WHERE  USER_ID= ? AND "
                    + "TRIM(UPPER(EVENT_TYPE))= ? AND EVENT_LIBRARY_TYPE = ? ORDER BY NAME";

            Rlog.debug("eventdef", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, user_id);
            pstmt.setString(2, type);
            pstmt.setInt(3, evtLibType);

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("eventdef", "After execution" + rs.toString());

            while (rs.next()) {

                setEvent_ids(Integer.valueOf(rs.getString("EVENT_ID")));

                setChain_ids(rs.getString("CHAIN_ID"));

                setEvent_types(rs.getString("EVENT_TYPE"));

                setNames(rs.getString("NAME"));

                setNotes(rs.getString("NOTES"));

                setCosts(rs.getString("COST"));

                setCost_descriptions(rs.getString("COST_DESCRIPTION"));

                setDurations(rs.getString("DURATION"));

                setUser_ids(rs.getString("USER_ID"));

                setLinked_uris(rs.getString("LINKED_URI"));

                setFuzzy_periods(rs.getString("FUZZY_PERIOD"));

                setMsg_tos(rs.getString("MSG_TO"));
                
                //KM-Enh-DFIN9
                setStatCodes(rs.getString("FK_CODELST_CALSTAT"));
                
                setDescription(rs.getString("DESCRIPTION"));
                setDisplacement(rs.getString("DISPLACEMENT"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.getHeaderList EXCEPTION IN FETCHING FROM Eventdef table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Fetches all the header events corrosponding to a user
     *
     * @param userId
     * @param type
     *            p/P Protocol s/S Study l/L Library added on 23rd May
     */

    public void getHeaderList(int account_id, int user_id, String type) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            String mysql = "select EVENT_ID, "
                    + "CHAIN_ID, "
                    + "EVENT_TYPE, "
                    + "NAME, "
                    + "NOTES, "
                    + "COST, "
                    + "COST_DESCRIPTION, "
                    + "DURATION, "
                    + "USER_ID, "
                    + "LINKED_URI, "
                    + "FUZZY_PERIOD, "
                    + "MSG_TO, "
                    //KM-#DFin9
                    //+ "STATUS, "
                    + "DESCRIPTION, "
                    + "DISPLACEMENT "
                    + " FROM EVENT_DEF evt"
                    + " WHERE  evt.USER_ID= ? AND "
                    + " TRIM(UPPER(EVENT_TYPE))= ? "
                    + " and evt.event_id in "
                    + " (select osh.fk_object  from er_objectshare  osh"
					+ " where osh.objectshare_type ='U' "
                    + " and osh.object_number = 3 "
                    + " and osh.fk_objectshare_id = ?)" + " ORDER BY NAME";


            Rlog.debug("eventdef", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, account_id);
            pstmt.setString(2, type);
            pstmt.setInt(3, user_id);

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("eventdef", "After execution" + rs.toString());

            while (rs.next()) {

                setEvent_ids(Integer.valueOf(rs.getString("EVENT_ID")));

                setChain_ids(rs.getString("CHAIN_ID"));

                setEvent_types(rs.getString("EVENT_TYPE"));
                setNames(rs.getString("NAME"));

                setNotes(rs.getString("NOTES"));

                setCosts(rs.getString("COST"));

                setCost_descriptions(rs.getString("COST_DESCRIPTION"));

                setDurations(rs.getString("DURATION"));

                setUser_ids(rs.getString("USER_ID"));

                setLinked_uris(rs.getString("LINKED_URI"));

                setFuzzy_periods(rs.getString("FUZZY_PERIOD"));

                setMsg_tos(rs.getString("MSG_TO"));
                //setStatus(rs.getString("STATUS"));
                setDescription(rs.getString("DESCRIPTION"));
                setDisplacement(rs.getString("DISPLACEMENT"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.getHeaderList EXCEPTION IN FETCHING FROM Eventdef table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    //Getting the headers with multipleorganizations selected
    public void getHeaderList(int account_id, int user_id, String type, String organizations) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            String mysql = "select EVENT_ID, "
                    + "CHAIN_ID, "
                    + "EVENT_TYPE, "
                    + "NAME, "
                    + "NOTES, "
                    + "COST, "
                    + "COST_DESCRIPTION, "
                    + "DURATION, "
                    + "USER_ID, "
                    + "LINKED_URI, "
                    + "FUZZY_PERIOD, "
                    + "MSG_TO, "
                    //KM-#DFin9
                    //+ "STATUS, "
                    + "DESCRIPTION, "
                    + "DISPLACEMENT "
                    + " FROM EVENT_DEF evt"
                    + " WHERE  evt.USER_ID= ? AND "
                    + " TRIM(UPPER(EVENT_TYPE))= ? "
                    + " and evt.event_id in "
                    + " (select osh.fk_object  from er_objectshare  osh"
                    + " where osh.object_number = 3 "
                    + " and ( ( osh.objectshare_type ='U' and osh.fk_objectshare_id = ?) or  "
                    + " (osh.objectshare_type ='O' and osh.fk_objectshare_id in (" + organizations +") )))" + " ORDER BY NAME";

            Rlog.debug("eventdef", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, account_id);
            pstmt.setString(2, type);
            pstmt.setInt(3, user_id);
            //pstmt.setInt(3, user_id);
           // pstmt.setString(4, organizations);


            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("eventdef", "After execution" + rs.toString());

            while (rs.next()) {

                setEvent_ids(Integer.valueOf(rs.getString("EVENT_ID")));

                setChain_ids(rs.getString("CHAIN_ID"));

                setEvent_types(rs.getString("EVENT_TYPE"));
                setNames(rs.getString("NAME"));

                setNotes(rs.getString("NOTES"));

                setCosts(rs.getString("COST"));

                setCost_descriptions(rs.getString("COST_DESCRIPTION"));

                setDurations(rs.getString("DURATION"));

                setUser_ids(rs.getString("USER_ID"));

                setLinked_uris(rs.getString("LINKED_URI"));

                setFuzzy_periods(rs.getString("FUZZY_PERIOD"));

                setMsg_tos(rs.getString("MSG_TO"));
                //setStatus(rs.getString("STATUS"));
                setDescription(rs.getString("DESCRIPTION"));
                setDisplacement(rs.getString("DISPLACEMENT"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.getHeaderList EXCEPTION IN FETCHING FROM Eventdef table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }



    public void getHeaderList(int account_id, int user_id, String type,
            int budgetId) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            String mysql = "select EVENT_ID, "
                    + "CHAIN_ID, "
                    + "EVENT_TYPE, "
                    + "NAME, "
                    + "NOTES, "
                    + "COST, "
                    + "COST_DESCRIPTION, "
                    + "DURATION, "
                    + "USER_ID, "
                    + "LINKED_URI, "
                    + "FUZZY_PERIOD, "
                    + "MSG_TO, "
                    + "FK_CODELST_CALSTAT, " //KM-Enh-DFin9
                    + "(select codelst_desc from sch_codelst where pk_codelst = fk_codelst_calstat)  codelst_desc, "
                    + "DESCRIPTION, "
                    + "DISPLACEMENT "
                    + " FROM EVENT_DEF evt "
                    + " WHERE  USER_ID= ? AND "
                    + " TRIM(UPPER(EVENT_TYPE))= ? "
                    + " and (evt.calendar_sharedwith = 'A' OR  evt.event_id in "
                    + " (select osh.fk_object  from er_objectshare  osh "
                    + " where osh.objectshare_type in ('U','O' ) "
                    + " and osh.object_number = 3 "
                    + " and osh.fk_objectshare_id = ? or osh.fk_objectshare_id in (select x.fk_site from er_usersite x where x.fk_user = ? and usersite_right > 0)) ) "
                    + " and event_id not in "
                    + " (select BGTCAL_PROTID from sch_bgtcal where FK_BUDGET= ? and "
                    + " BGTCAL_DELFLAG='N' and bgtcal_protid is not null ) "
                    + " ORDER BY NAME";

            Rlog.fatal("eventdef", "line 0" + mysql);

            pstmt = conn.prepareStatement(mysql);
            // SV, 9/13/04 pstmt.setInt(1,user_id);
            pstmt.setInt(1, account_id);
            pstmt.setString(2, type);
            pstmt.setInt(3, user_id);
            pstmt.setInt(4, user_id);
            pstmt.setInt(5, budgetId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setEvent_ids(Integer.valueOf(rs.getString("EVENT_ID")));

                setChain_ids(rs.getString("CHAIN_ID"));

                setEvent_types(rs.getString("EVENT_TYPE"));

                setNames(rs.getString("NAME"));

                setNotes(rs.getString("NOTES"));

                setCosts(rs.getString("COST"));

                setCost_descriptions(rs.getString("COST_DESCRIPTION"));

                setDurations(rs.getString("DURATION"));

                setUser_ids(rs.getString("USER_ID"));

                setLinked_uris(rs.getString("LINKED_URI"));

                setFuzzy_periods(rs.getString("FUZZY_PERIOD"));

                setMsg_tos(rs.getString("MSG_TO"));
                //setStatus(rs.getString("STATUS"));
                //KM-Enh-DFIN9
                setCodeDescriptions(rs.getString("codelst_desc"));
                
                setDescription(rs.getString("DESCRIPTION"));
                setDisplacement(rs.getString("DISPLACEMENT"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.getHeaderList EXCEPTION IN FETCHING FROM Eventdef table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Fetches the protocol and the corrosponding events
     *
     * @param event_id
     *
     */
    public void getAllProtAndEvents(int event_id) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String mysql = "select EVENT_ID, " + "ORG_ID, " + "CHAIN_ID, "
                    + "EVENT_TYPE, " + "NAME, " + "NOTES, " + "COST, "
                    + "COST_DESCRIPTION, " + "DURATION, " + "USER_ID, "
                    + "LINKED_URI, " + "FUZZY_PERIOD, " + "MSG_TO, "
                    + "DESCRIPTION, " + "DISPLACEMENT "
                    + "FROM EVENT_DEF " + "WHERE  CHAIN_ID= ?  "
                    // + " AND TRIM(UPPER(EVENT_TYPE)) IN('P','A') ORDER BY
                    // CHAIN_ID,COST,EVENT_ID,NAME,DISPLACEMENT";
                    + " ORDER BY CHAIN_ID,COST,EVENT_ID,NAME,DISPLACEMENT";

            Rlog.debug("eventdef", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, event_id);

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("eventdef", "After execution" + rs.toString());

            while (rs.next()) {
                Rlog.debug("eventdef", "eventdef line 1");
                String id = rs.getString("EVENT_ID");

                id = id.trim();
                Integer test1 = new Integer(id);

                setEvent_ids(test1);

                String id1 = rs.getString("ORG_ID");
                id1 = id1.trim();
                Integer test2 = new Integer(id1);
                setOrg_ids(test2);

                setChain_ids(rs.getString("CHAIN_ID"));

                setEvent_types(rs.getString("EVENT_TYPE"));

                setNames(rs.getString("NAME"));

                setNotes(rs.getString("NOTES"));

                setCosts(rs.getString("COST"));

                setCost_descriptions(rs.getString("COST_DESCRIPTION"));

                setDurations(rs.getString("DURATION"));

                setUser_ids(rs.getString("USER_ID"));

                setLinked_uris(rs.getString("LINKED_URI"));

                setFuzzy_periods(rs.getString("FUZZY_PERIOD"));

                setMsg_tos(rs.getString("MSG_TO"));
                //KM-DFin9
                //setStatus(rs.getString("STATUS"));
                setDescription(rs.getString("DESCRIPTION"));
                setDisplacement(rs.getString("DISPLACEMENT"));

            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.getAllProtAndEvents EXCEPTION IN FETCHING FROM Eventdef table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getProtSelectedAndGroupedEvents(int protocolId, boolean isExport, int finDetRight,String eventName) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            StringBuffer sql = new StringBuffer();
            StringBuffer sql1 = new StringBuffer();
            String costs = "";
            
            ArrayList eventCosts=new ArrayList();
            ResultSet rs = null;
			if(!"".equalsIgnoreCase(eventName)){
			sql1.append("SELECT COST ")
            .append(" FROM EVENT_DEF a ")
            .append(" WHERE a.CHAIN_ID = ? ")
            .append(" AND a.event_type = 'A' ")
            .append(" and (LOWER(a.NAME) LIKE LOWER('%"+eventName+"%')) ")
            .append(" AND a.fk_visit is null ");
            
            pstmt = conn.prepareStatement(sql1.toString());
            pstmt.setInt(1, protocolId);
            pstmt.setFetchSize(1000);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	eventCosts.add(Integer.parseInt(StringUtil.trueValue(rs.getString("COST")).trim()));
            }
            costs= eventCosts.toString().replace("[", "").replace("]", "")
            .replace(", ", ",");
			 }//end of if
			pstmt = null;
			rs = null;
			if(!"".equalsIgnoreCase(eventName) && !"".equalsIgnoreCase(costs)){
            sql.append(" SELECT a.EVENT_ID AS INTERSECTION_ID, b.event_id,b.name as event_name, a.ORG_ID, ")
            .append(" a.CHAIN_ID, a.EVENT_TYPE, replace(replace(a.NAME,CHR(13),''),CHR(10),'') NAME, a.COST, a.DESCRIPTION, 'E' AS flag, ")
            .append(" a. FK_VISIT, 0 AS VISIT_NO, 0 AS DISPLACEMENT, a.EVENT_CATEGORY, a.FK_CODELST_COVERTYPE, a.COVERAGE_NOTES, replace(replace(b.EVENT_CPTCODE,CHR(13),''),CHR(10),'') EVENT_CPTCODE ");
            
            sql.append(" FROM EVENT_DEF a left join event_def b on a.chain_id =b.chain_id and a.cost=b.cost AND b.FK_VISIT  IS NULL AND b.EVENT_TYPE = a.EVENT_TYPE ")
            .append(" WHERE a.CHAIN_ID = ? ")
            .append(" AND a.event_type = 'A' ");
            sql.append(" AND a.COST in ("+costs.toString()+")");
            sql.append(" ORDER BY Cost, FK_VISIT nulls first ");
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, protocolId);
            pstmt.setFetchSize(1000);
             rs = pstmt.executeQuery();
			}else if("".equalsIgnoreCase(eventName) && "".equalsIgnoreCase(costs)){
				//Coverage Analysis Tab
				//Modified event name,cptcode for the bug id 23909
				sql.append(" SELECT a.EVENT_ID AS INTERSECTION_ID, b.event_id,b.name as event_name, a.ORG_ID, ")
	            .append(" a.CHAIN_ID, a.EVENT_TYPE, replace(replace(a.NAME,CHR(13),''),CHR(10),'') NAME, a.COST, a.DESCRIPTION, 'E' AS flag, ")
	            .append(" a. FK_VISIT, 0 AS VISIT_NO, 0 AS DISPLACEMENT, a.EVENT_CATEGORY, a.FK_CODELST_COVERTYPE, a.COVERAGE_NOTES, replace(replace(b.EVENT_CPTCODE,CHR(13),''),CHR(10),'') EVENT_CPTCODE ");
	            
	            sql.append(" FROM EVENT_DEF a left join event_def b on a.chain_id =b.chain_id and a.cost=b.cost AND b.FK_VISIT  IS NULL AND b.EVENT_TYPE = a.EVENT_TYPE ")
	            .append(" WHERE a.CHAIN_ID = ? ")
	            .append(" AND a.event_type = 'A' ");
	            sql.append(" ORDER BY Cost, FK_VISIT nulls first ");
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, protocolId);
            pstmt.setFetchSize(1000);
            rs = pstmt.executeQuery();
			}
			if(rs!=null){
            while (rs.next()) {
            	setNew_event_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("INTERSECTION_ID")).trim()));
                setEvent_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("EVENT_ID")).trim()));
                setOrg_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("ORG_ID")).trim()));
                setChain_ids(rs.getString("CHAIN_ID"));
                setEvent_types(rs.getString("EVENT_TYPE"));
                setNames(rs.getString("NAME"));
                setCosts(rs.getString("COST"));
                setDescription(rs.getString("DESCRIPTION"));
                setFlag(rs.getString("flag"));
                String sValue = rs.getString("FK_VISIT");
                if (sValue == null) { sValue = "0"; }
                setFk_visit(new Integer (sValue.trim()));
                setDisplacement(rs.getString("DISPLACEMENT"));
                setEventCategory(rs.getString("EVENT_CATEGORY"));
                setEventCoverageTypes(rs.getString("FK_CODELST_COVERTYPE"));
                setEventCoverageNotes(rs.getString("COVERAGE_NOTES"));
                setCptCodes(rs.getString("EVENT_CPTCODE"));

                if (isExport){
                	setCodeDescriptions(rs.getString("EVENT_ADDCODE"));
                }
            }}
        } catch (Exception ex) {
            Rlog.fatal("eventdef",
                       "EventDefDao.getProtSelectedAndGroupedEvents: "+ ex);
        } finally {
            try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
            try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
    }
    
    
    public void getProtSelectedAndGroupedEvents(int protocolId, boolean isExport, int finDetRight) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            StringBuffer sql = new StringBuffer();
            sql
            .append(" SELECT a.EVENT_ID AS INTERSECTION_ID, b.event_id,b.name as event_name, a.ORG_ID, ")
            .append(" (select md_modelementdata from er_moredetails where md_modelementpk = (select pk_codelst from er_codelst where codelst_type = 'evtaddlcode' and codelst_subtyp = 'SVC_CD') and fk_modpk = a.event_id) as servicecode,")
            .append(" (select md_modelementdata from er_moredetails where md_modelementpk = (select pk_codelst from er_codelst where codelst_type = 'evtaddlcode' and codelst_subtyp = 'PRS_CODE') and fk_modpk = a.event_id) as prscode,")
            .append(" a.CHAIN_ID, a.EVENT_TYPE, replace(replace(a.NAME,CHR(13),''),CHR(10),'') NAME, a.COST, a.DESCRIPTION, 'E' AS flag, ")
            .append(" a. FK_VISIT, 0 AS VISIT_NO, 0 AS DISPLACEMENT, a.EVENT_CATEGORY, a.FK_CODELST_COVERTYPE, a.COVERAGE_NOTES, replace(replace(b.EVENT_CPTCODE,CHR(13),''),CHR(10),'') EVENT_CPTCODE ");
            if (isExport){
            	sql.append(", esch.lst_additionalcodes_financial(a.event_id,'evtaddlcode',-1) as EVENT_ADDCODE");
            }

            sql.append(" FROM EVENT_DEF a left join event_def b on a.chain_id =b.chain_id and a.cost=b.cost AND b.FK_VISIT  IS NULL AND b.EVENT_TYPE = a.EVENT_TYPE ")
            .append(" WHERE a.CHAIN_ID = ? ")
            .append(" AND a.event_type = 'A' ");
            
            sql.append(" ORDER BY Cost, FK_VISIT nulls first ");
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, protocolId);
            pstmt.setFetchSize(1000);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	setNew_event_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("INTERSECTION_ID")).trim()));
                setEvent_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("EVENT_ID")).trim()));
                setOrg_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("ORG_ID")).trim()));
                setChain_ids(rs.getString("CHAIN_ID"));
                setEvent_types(rs.getString("EVENT_TYPE"));
                setNames(rs.getString("NAME"));
                setCosts(rs.getString("COST"));
                setDescription(rs.getString("DESCRIPTION"));
                setFlag(rs.getString("flag"));
                String sValue = rs.getString("FK_VISIT");
                if (sValue == null) { sValue = "0"; }
                setFk_visit(new Integer (sValue.trim()));
                setDisplacement(rs.getString("DISPLACEMENT"));
                setEventCategory(rs.getString("EVENT_CATEGORY"));
                setEventCoverageTypes(rs.getString("FK_CODELST_COVERTYPE"));
                setEventCoverageNotes(rs.getString("COVERAGE_NOTES"));
                setCptCodes(rs.getString("EVENT_CPTCODE"));
                this.setNewPRSCodes(rs.getString("prscode"));
                this.setNewServiceCodes(rs.getString("servicecode"));
                if (isExport){
                	setCodeDescriptions(rs.getString("EVENT_ADDCODE"));
                }
            }
        } catch (Exception ex) {
            Rlog.fatal("eventdef",
                       "EventDefDao.getProtSelectedAndGroupedEvents: "+ ex);
        } finally {
            try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
            try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
    }
    
    
    
    
    
    public void getProtSelectedAndGroupedEvents(int protocolId, boolean isExport, int finDetRight,int initEventSize,int limitEventSize,String visitids,String eventName) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs=null;
        try {
            conn = getConnection();
            StringBuffer sql = new StringBuffer();
            StringBuffer sql1 = new StringBuffer();
            StringBuffer mainSql = new StringBuffer();
            StringBuffer sql2 = new StringBuffer();
            pstmt = null;
            rs = null;
            mainSql.append("SELECT count(*)")
            .append(" FROM EVENT_DEF a ")
            .append(" WHERE a.CHAIN_ID = ? ")
            .append(" AND a.event_type = 'A' ");
            if(!"".equalsIgnoreCase(eventName))
            mainSql.append(" and (LOWER(NAME) LIKE LOWER('%"+eventName+"%')) ");
            mainSql.append(" AND a.fk_visit is null");
            pstmt = conn.prepareStatement(mainSql.toString());
            pstmt.setInt(1, protocolId);
            rs = pstmt.executeQuery();
            while (rs.next()){
            	totalRecords=rs.getInt("count(*)");
            }
            pstmt = null;
            rs = null;
            ArrayList eventCosts=new ArrayList();
            sql1.append("SELECT * from (SELECT COST ");
            sql1.append(" FROM EVENT_DEF a ")
            .append(" WHERE a.CHAIN_ID = ? ")
            .append(" AND a.event_type = 'A' ");
            if(!"".equalsIgnoreCase(eventName))
            	sql1.append(" and (LOWER(NAME) LIKE LOWER('%"+eventName+"%')) ");
            sql1.append(" AND a.fk_visit is null ORDER BY cost) ")
            .append(" where ROWNUM <= ? ");
            pstmt = conn.prepareStatement(sql1.toString(),ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            pstmt.setInt(1, protocolId);
            pstmt.setInt(2,limitEventSize );
            pstmt.setFetchSize(1000);
            rs = pstmt.executeQuery();
            rs.absolute(initEventSize);
            do {
            	eventCosts.add(Integer.parseInt(StringUtil.trueValue(rs.getString("COST")).trim()));
            }while (rs.next());
            String costs = eventCosts.toString().replace("[", "").replace("]", "")
            .replace(", ", ",");
            pstmt = null;
            rs = null;
            sql2.append("SELECT a.EVENT_ID AS INTERSECTION_ID, b.event_id,b.name as event_name, a.ORG_ID, ")
            .append(" a.CHAIN_ID, a.EVENT_TYPE, replace(replace(a.NAME,CHR(13),''),CHR(10),'') NAME, a.COST, a.DESCRIPTION, 'E' AS flag, ")
            .append(" a.FK_VISIT, 0 AS VISIT_NO, 0 AS DISPLACEMENT, a.EVENT_CATEGORY, a.FK_CODELST_COVERTYPE, a.COVERAGE_NOTES,replace(replace(b.EVENT_CPTCODE,CHR(13),''),CHR(10),'') EVENT_CPTCODE ");
            sql2.append(" FROM EVENT_DEF a left join event_def b on a.chain_id =b.chain_id and a.cost=b.cost AND b.FK_VISIT  IS NULL AND b.EVENT_TYPE = a.EVENT_TYPE ")
            .append(" WHERE a.CHAIN_ID = ? AND a.event_type = 'A' ")
            .append(" and (a.fk_visit in("+visitids+") or a.fk_visit is null) ")
            .append(" AND a.COST in ("+costs+") ORDER BY Cost, FK_VISIT nulls first");
            pstmt = conn.prepareStatement(sql2.toString());
            pstmt.setInt(1, protocolId);
            pstmt.setFetchSize(1000);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	setNew_event_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("INTERSECTION_ID")).trim()));
                setEvent_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("EVENT_ID")).trim()));
                setOrg_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("ORG_ID")).trim()));
                setChain_ids(rs.getString("CHAIN_ID"));
                setEvent_types(rs.getString("EVENT_TYPE"));
                setNames(rs.getString("NAME"));
                setCosts(rs.getString("COST"));
                setDescription(rs.getString("DESCRIPTION"));
                setFlag(rs.getString("flag"));
                String sValue = rs.getString("FK_VISIT");
                if (sValue == null) { sValue = "0"; }
                setFk_visit(new Integer (sValue.trim()));
                setDisplacement(rs.getString("DISPLACEMENT"));
                setEventCategory(rs.getString("EVENT_CATEGORY"));
                setEventCoverageTypes(rs.getString("FK_CODELST_COVERTYPE"));
                setEventCoverageNotes(rs.getString("COVERAGE_NOTES"));
                setCptCodes(rs.getString("EVENT_CPTCODE"));

            }
        } catch (Exception ex) {
            Rlog.fatal("eventdef",
                       "EventDefDao.getProtSelectedAndGroupedEvents: "+ ex);
        } finally {
            try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
            try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
    }

    public void getProtSelectedEvents(int protocolId, String orderType, String orderBy) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {

            conn = getConnection();
            //Enh#C7
            //Select Events Tab
            //Modified event name, cptcode and Description for the bug id 23909
            //Calendar Enhancements Cal-35776_No1:- Select Events Tab
            String mysql = "select Cost, (select CODELST_DESC from SCH_CODELST cl where cl.pk_codelst = EVENT_LIBRARY_TYPE ) as EVENT_LIBRARY_TYPE, "
            	    + "EVENT_CATEGORY, "
            	    + "replace(replace(NAME,CHR(13),''),CHR(10),'') NAME, "
            	    + "replace(replace(EVENT_CPTCODE,CHR(13),''),CHR(10),'') EVENT_CPTCODE, "
            	    + "replace(replace(DESCRIPTION,CHR(13),''),CHR(10),'') DESCRIPTION, "
            	    + "NOTES, "
            	    + "lst_cost(a.EVENT_ID) as eventcost, "
            	    + "(select site_name from er_site b  where b.pk_site = a.FACILITY_ID) as Facilityname, "
            	    + "EVENT_ID, "
            	    + "ORG_ID, "
                    + "CHAIN_ID, "
                    + "EVENT_TYPE, "
                    + "COST_DESCRIPTION, "
                    + "DURATION, "
                    + "USER_ID, "
                    + "LINKED_URI, "
                    + "FUZZY_PERIOD, "
                    + "MSG_TO, "
                    //+ "STATUS, "
                    + "DISPLACEMENT "
                    + "FROM EVENT_DEF a "
                    + "WHERE  a.CHAIN_ID= ? AND a.FK_VISIT IS NULL and a.event_id != ? "; //BK-DAY0, Ankit: PCAL-20282
                    // + " AND TRIM(UPPER(EVENT_TYPE)) IN('P','A') ORDER BY
                    // CHAIN_ID,COST,EVENT_ID,NAME,DISPLACEMENT";
                    //+ " ORDER BY event_id";
            //End Enhancements
            // /till here
            //Bug#4651
             if ((!orderType.equals("")) && (!orderBy.equals("")))
            	mysql = mysql +" ORDER BY " + orderBy +" "+orderType;
             else
            	mysql = mysql + " ORDER BY event_id ";

            Rlog.debug("eventdef", mysql);


            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, protocolId);
            pstmt.setInt(2, protocolId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                String id = rs.getString("EVENT_ID");

                id = id.trim();

                Integer test1 = new Integer(id);

                setEvent_ids(test1);

                String id1 = rs.getString("ORG_ID");
                id1 = id1.trim();
                Integer test2 = new Integer(id1);
                setOrg_ids(test2);

                setChain_ids(rs.getString("CHAIN_ID"));
                setEvent_types(rs.getString("EVENT_TYPE"));

                setNames(rs.getString("NAME"));

                setNotes(rs.getString("NOTES"));

                setCosts(rs.getString("COST"));

                setCost_descriptions(rs.getString("COST_DESCRIPTION"));

                setDurations(rs.getString("DURATION"));

                setUser_ids(rs.getString("USER_ID"));

                setLinked_uris(rs.getString("LINKED_URI"));

                setFuzzy_periods(rs.getString("FUZZY_PERIOD"));

                setMsg_tos(rs.getString("MSG_TO"));
                //KM-#DFin9
                //setStatus(rs.getString("STATUS"));
                setDescription(rs.getString("DESCRIPTION"));
                setDisplacement(rs.getString("DISPLACEMENT"));
                setCptCodes(rs.getString("EVENT_CPTCODE")); //KM
                setEventCategory(rs.getString("EVENT_CATEGORY"));//KM
                setFacility(rs.getString("Facilityname"));//KV: SW-FIN2c
                setEventCostDetails(rs.getString("eventcost"));//KV: SW-FIN2b
                setEventLibraryType(rs.getString("EVENT_LIBRARY_TYPE")); //Ankit: PCAL-20282
            }



        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.getProtSelectedEvents EXCEPTION IN FETCHING FROM Eventdef table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getProtRowEvents(int protocolId, int rownum, int beginDisp,
            int endDisp) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String mysql = "select EVENT_ID,ORG_ID ,"
                    + "CHAIN_ID, "
                    + "EVENT_TYPE, "
                    + "NAME, "
                    + "NOTES, "
                    + "COST, "
                    + "COST_DESCRIPTION, "
                    + "DURATION, "
                    + "USER_ID, "
                    + "LINKED_URI, "
                    + "FUZZY_PERIOD, "
                    + "MSG_TO, "
                    //+ "STATUS, "
                    + "DESCRIPTION, "
                    + "DISPLACEMENT, "
                    + " FK_VISIT "
                    + "FROM EVENT_DEF "
                    + "WHERE  CHAIN_ID = ? AND (DISPLACEMENT>= ? and DISPLACEMENT <= ? AND COST = ? ) "
                    + " ORDER BY EVENT_TYPE DESC ,COST,DISPLACEMENT";

            Rlog.debug("eventdef", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, protocolId);
            pstmt.setInt(2, beginDisp);
            pstmt.setInt(3, endDisp);
            pstmt.setInt(4, rownum);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                String id = rs.getString("EVENT_ID");

                id = id.trim();
                Integer test1 = new Integer(id);

                setEvent_ids(test1);

                String id1 = rs.getString("ORG_ID");
                id1 = id1.trim();
                Integer test2 = new Integer(id1);
                setOrg_ids(test2);

                setChain_ids(rs.getString("CHAIN_ID"));

                setEvent_types(rs.getString("EVENT_TYPE"));

                setNames(rs.getString("NAME"));

                setNotes(rs.getString("NOTES"));

                setCosts(rs.getString("COST"));

                setCost_descriptions(rs.getString("COST_DESCRIPTION"));

                setDurations(rs.getString("DURATION"));

                setUser_ids(rs.getString("USER_ID"));

                setLinked_uris(rs.getString("LINKED_URI"));

                setFuzzy_periods(rs.getString("FUZZY_PERIOD"));

                setMsg_tos(rs.getString("MSG_TO"));
                //KM-#DFin9
                //setStatus(rs.getString("STATUS"));
                setDescription(rs.getString("DESCRIPTION"));
                setDisplacement(rs.getString("DISPLACEMENT"));
                this.setFkVisit((rs.getString("FK_VISIT")));

            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.getFilteredRangeEvents EXCEPTION IN FETCHING FROM Eventdef table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     *
     * This function returns true if an event in the library has instance in
     * protocol or study.
     */
    public boolean hasInstance(int event_id, String type) {
        boolean output = false;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            String mysql = "";
            conn = getConnection();
            if (type.equals("E")) {
                mysql = "select a.cnt1+ b.cnt2 ROWCOUNT from   (select count(event_id) cnt1 from event_def a where a.org_id= ? ) a,(select count(event_id)  cnt2  from event_assoc b where b.org_id=? ) b ";
            } else {
                mysql = "select a.cnt1+ b.cnt2 ROWCOUNT from   (select count(event_id) cnt1 from event_def a where a.org_id in (select event_id from event_def where chain_id=?)) a  , (select count(event_id)  cnt2  from event_assoc b  where b.org_id in (select event_id from event_def where chain_id=?)) b ";
            }

            Rlog.debug("eventdef", mysql);
            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, event_id);
            pstmt.setInt(2, event_id);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                int count = rs.getInt("ROWCOUNT");
                if (count > 0)
                    output = true;
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.hasInstance EXCEPTION IN hasInstance" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return output;
    }

    /* Modified by Sonia Abrol, 04/01/05 to include visit name */

    public void FetchPreviousSchedule(int studyId, int patientId) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String mysql =

            	" select a.event_id as eventId, "
				+ " a.fk_patprot as patprotid, "
				+ " (select name from event_assoc e where e.event_id=b.fk_protocol) as calname, "
				+ " b.PATPROT_DISCDT as disdate, "
				+ " b.PATPROT_REASON as disreason, "
				+ " b.PATPROT_START as protStartDate, "
				+ " a.START_DATE_TIME as evestartdate, "
				+ " a.ACTUAL_SCHDATE as actualdate, "
				+ " c.STUDY_NUMBER as studynum, "
				+ " c.STUDY_VER_NO as studyversion, "
				+ " a.description as evename, "
				+ " (select CODELST_DESC from sch_codelst f where a.isconfirmed=f.pk_codelst) as evestat, "
				+ " a.VISIT_TYPE_ID as fuzzyperiod , a.fk_visit, "
				+ " d.EVENT_DURATIONBEFORE as eventdurationbefore,d.EVENT_FUZZYAFTER as eventfuzzyafter,d.EVENT_DURATIONAFTER as eventdurationafter,  "
				+ " a.ADVERSE_COUNT as advcount, "
				+ " visit_name,a.visit "
				+ " from sch_events1 a ,  "
				+ " er_patprot  b , "
				+ " er_study  c, "
				+ " event_assoc d , sch_protocol_visit "
				+ " where  "
				+ " a.fk_patprot =b.pk_patprot  "
				+ " and c.pk_study=b.fk_study "
				+ " and d.event_id=a.fk_assoc "
				+ " and b.fk_study=? "
				+ " and b.fk_per=? "
				+ " and (select count(event_id) from  "
				+ " sch_events1  where isconfirmed in "
				+ " (select pk_codelst from sch_codelst where (codelst_subtyp != 'ev_notdone' or codelst_subtyp is null) and  codelst_type='eventstatus')  "
				+ " and status=5 "
				+ " and fk_patprot=b.pk_patprot ) >=1 and a.status = 5 and sch_protocol_visit.pk_protocol_visit = a.fk_visit "
				+ " order by b.PATPROT_DISCDT desc ,a.fk_patprot desc ,a.ACTUAL_SCHDATE , a.fk_visit,  a.event_sequence ";

            Rlog.debug("eventdef", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, studyId);
            pstmt.setInt(2, patientId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                String id = rs.getString("eventId");

                id = id.trim();
                Integer test1 = new Integer(id);

                setEvent_ids(test1);
                setPatprotIds(rs.getString("patprotid"));
                setCalNames(rs.getString("calname"));
                setDisDates(rs.getDate("disdate"));

                setProtstartDates(rs.getDate("protStartDate"));

                setDisReasons(rs.getString("disreason"));
                setStart_date_times(rs.getDate("evestartdate"));
                setActualDate(rs.getDate("actualdate"));
                setStudyNums(rs.getString("studynum"));
                setStudyVersions(rs.getString("studyversion"));
                setNames(rs.getString("evename"));
                setStatus(rs.getString("evestat"));
                setFuzzy_periods(rs.getString("fuzzyperiod"));
                setVisit(new Integer(rs.getInt("visit")));
                setFk_visit(new Integer(rs.getInt("fk_visit")));
                setAdverseCounts(new Integer(rs.getInt("advcount")));
                //setCrfCounts(new Integer(rs.getInt("crfCount")));
                setVisitName(rs.getString("visit_name"));
             // BK : Defect 4512 -- 16Aug2010
				this.getEventDurationBefore().add(rs.getString("eventdurationbefore"));
				this.getEventDurationAfter().add(rs.getString("eventdurationafter"));
				this.getEventFuzzyAfter().add(rs.getString("eventfuzzyafter"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.getFilteredRangeEvents EXCEPTION IN FETCHING FROM Eventdef table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
    }

    // to get whether the schedule has any number of executed events

    public int getModEvents(int patprot, int scheduleStatus) {
        int num = 0;

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            String mysql = "";
            conn = getConnection();

            mysql = "select count(event_id) from  sch_events1"
                    + " where   status = " + scheduleStatus
                    + " and fk_patprot= " + patprot
                    + " and  isconfirmed in (select pk_codelst from "
                    + " sch_codelst where (codelst_subtyp != 'ev_notdone' or"
                    + " codelst_subtyp is null)  and "
                    + " codelst_type = 'eventstatus')";

            Rlog.debug("eventdef", mysql);
            pstmt = conn.prepareStatement(mysql);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                num = rs.getInt(1);
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.getModEvents EXCEPTION IN getModEvents " + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return num;
    }

    public void FetchSchedule(int eventId) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String mysql = "select DESCRIPTION, " + "START_DATE_TIME, "
                    + "VISIT_TYPE_ID, " + "ISCONFIRMED, " + "	FK_ASSOC, "
                    + "	NOTES, " + "	EVENT_EXEON, " + "	ACTUAL_SCHDATE, "
                    + "	EVENT_EXEBY, REASON_FOR_COVERAGECHANGE " + " FROM SCH_EVENTS1 "
                    + " WHERE  event_id= ? ";

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, eventId);

            Rlog.debug("eventdef", "SQL is" + mysql);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setNames(rs.getString("DESCRIPTION"));
                setStart_date_times(rs.getDate("START_DATE_TIME"));
                setStatus(rs.getString("ISCONFIRMED"));
                setOrg_ids(Integer.valueOf(rs.getString("FK_ASSOC")));
                setNotes(rs.getString("NOTES"));
                setEvent_exeons(rs.getDate("EVENT_EXEON"));
                setActualDate(rs.getDate("ACTUAL_SCHDATE"));
                setUser_ids(rs.getString("EVENT_EXEBY"));
                setReasonForCoverageChange(rs.getString("REASON_FOR_COVERAGECHANGE"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.FetchSchedule EXCEPTION IN FETCHING FROM sch_events1 table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public int ValidateEventName(int eventId, int userId, String eventName,
            String eventType, String chainId, String calAssocTo,int flag) {
        int result = 0;

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            String mysql = "";
            conn = getConnection();

            mysql = "select PKG_COMMON.ValidateName(?,?,?,?,?,?,?) as result from  dual";
            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, eventId);
            pstmt.setInt(2, userId);
            pstmt.setString(3, eventName);
            pstmt.setString(4, eventType);
            
            pstmt.setString(5, chainId);
            pstmt.setString(6, calAssocTo);            
            pstmt.setInt(7, flag);
            
            System.out.println("*****"+eventId+":::"+userId+":::"+eventName+":::"+eventType+":::"+chainId+":::"+calAssocTo); 
            

            Rlog.debug("eventdef", mysql);

            ResultSet rs = pstmt.executeQuery();
            rs.next();
            result = rs.getInt("result");

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.ValidateEventName EXCEPTION IN ValidateEventName "
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return result;
    }

    // /////////////

    public int copyNotifySetting(int p_old_fkpatprot, int p_new_fkpatprot,
            int p_copyflag, int p_creator, String p_ip) {

        CallableStatement cstmt = null;
        int success = -1;

        // OracleCallableStatement cstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_GENSCH.SP_COPYNOTIFYSETTING(?,?,?,?,?)}");
            cstmt.setInt(1, p_old_fkpatprot);
            cstmt.setInt(2, p_new_fkpatprot);
            cstmt.setInt(3, p_copyflag);
            cstmt.setInt(4, p_creator);
            cstmt.setString(5, p_ip);
            cstmt.execute();
            success = 1;

        }

        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In copyNotifySetting in EventdefDAO EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    public void getVisitEvents(int patProtId, int visit,String type,int protocolId) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        type=(type==null)?"":type;
        try {
            String mysql = "";
            conn = getConnection();

            if (type.equals("S"))
                mysql = "select event_id, description, SERVICE_SITE_ID, FK_CODELST_COVERTYPE from  sch_events1 where fk_study = ? and visit = ? and status = 0 and session_Id= LPAD (TO_CHAR(?), 10, '0')";
            else
                mysql = "select event_id, description, SERVICE_SITE_ID, FK_CODELST_COVERTYPE from  sch_events1 where fk_patprot = ? and visit = ?";

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, patProtId);
            pstmt.setInt(2, visit);

            if (type.equals("S")) pstmt.setInt(3, protocolId);

            Rlog.debug("eventdef", "EventDefDao.getVisitEvents SQL " + mysql);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setNames(rs.getString("description"));
                String id = rs.getString("event_id");
                id = id.trim();
                Integer test1 = new Integer(id);

                setEvent_ids(test1);
                setEventSOSIds(rs.getString("SERVICE_SITE_ID"));
                setEventCoverageTypes(rs.getString("FK_CODELST_COVERTYPE"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.ValidateEventName EXCEPTION IN ValidateEventName "
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }


    }
    /*
     *
     */

    public void getVisitEvents(int patProtId, int visit)
    {
        getVisitEvents(patProtId, visit,"",0);
    }

    public int updateStudyAlertNotify(int studyId, int protocolId,
            int modifiedBy, String ipAdd, String globFlag, int lockValue) {

        CallableStatement cstmt = null;
        int success = -1;

        // OracleCallableStatement cstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_ALNOT.update_study_alnot(?,?,?,?,?,?)}");
            cstmt.setInt(1, studyId);
            cstmt.setInt(2, protocolId);
            cstmt.setInt(3, modifiedBy);
            cstmt.setString(4, ipAdd);
            cstmt.setString(5, globFlag);
            cstmt.setInt(6, lockValue);

            cstmt.execute();
            success = 1;

        }

        catch (SQLException ex) {
            Rlog
                    .fatal(
                            "eventdef",
                            "In copyNotifySetting in EventdefDAO EXCEPTION IN calling the updateStudyAlertNotify"
                                    + ex);
            success = -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    public int getMaxCost(String chainId) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        int cost = 0;
        try {
            conn = getConnection();
            String mysql = "select nvl(max(COST),0) as cost "
                    + "FROM EVENT_DEF "
                    + "WHERE  CHAIN_ID= ? and event_id != ? ";

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, EJBUtil.stringToNum(chainId));
            pstmt.setInt(2, EJBUtil.stringToNum(chainId));
            rs = pstmt.executeQuery();
            rs.next();
            cost = rs.getInt("cost");
        } catch (SQLException ex) {
            Rlog.fatal("eventassoc", "EventdefDao.getMaxCost" + ex);
            return -2;
        }finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return cost;
    }

    /*
     * OLD SQL FOR CALCULATING VISIT USING DENSERANK ON ACT SCH DATE
     *
     * String mysql="select m.DESCRIPTION, m.START_DATE_TIME, m.VISIT_TYPE_ID," + "
     * m.EVENT_ID,m.ISCONFIRMED, m.FK_ASSOC, m.NOTES, m.EVENT_EXEON," + "
     * m.ACTUAL_SCHDATE, m.EVENT_EXEBY,m.ADVERSE_COUNT, m.visit," + "m.PK_CRF,
     * m.CRF_NUMBER,m.crfstatus,m.status" + " from (" + "select DESCRIPTION, " +
     * "START_DATE_TIME, " + "VISIT_TYPE_ID," + "EVENT_ID, " + "ISCONFIRMED, " + "
     * FK_ASSOC, " + " NOTES, " + " EVENT_EXEON, " + " ACTUAL_SCHDATE, " + "
     * EVENT_EXEBY, " + " ADVERSE_COUNT, " + " PK_CRF, " + " CRF_NUMBER, " + "
     * (select codelst_desc from sch_codelst where pk_codelst = " + " (select
     * fk_codelst_crfstat from sch_crfstat where fk_crf = " + " PK_CRF and
     * CRFSTAT_REVIEWON = " + " (select max(CRFSTAT_REVIEWON) from sch_crfstat
     * where fk_crf = PK_CRF))) as crfstatus " + " , dense_rank () over (
     * partition by a.st_dt order by (a.st_dt - ACTUAL_SCHDATE) desc nulls last )
     * visit, " + " status " + " FROM SCH_EVENTS1, " + " SCH_CRF " + ", (select
     * min(ACTUAL_SCHDATE) st_dt from sch_events1 " + " where PATIENT_ID=
     * lpad(to_char(" + patient_id + "),10,'0') AND SESSION_ID = lpad(to_char(" +
     * protocol_id + "),10,'0') and status = 0 ) a" + " WHERE PATIENT_ID=
     * lpad(to_char(?),10,'0') AND " + " SESSION_ID= lpad(to_char(?),10,'0') " + "
     * AND BOOKEDON=TO_DATE('" + pro_start_date + "','yyyy-mm-dd')" + " and
     * status = 0 " + " and SCH_CRF.FK_EVENTS1 (+)= SCH_EVENTS1.EVENT_ID" + " )
     * m where status = 0 ";
     *
     */

    public int notifyAdmin(int userId, String ipAdd, int velosUser,
            String failDate, String failTime) {

        CallableStatement cstmt = null;
        int success = -1;
        int toUser = 0;
        int rows = 0;
        String mailString;

        String smtpHost = "";
        String domain = "";
        String messageFrom = "";
        String tomail = "";

        CtrlDao ctrl = new CtrlDao();
        CtrlDao userCtrl = new CtrlDao();

        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_ALNOT.sp_notifyadmin(?,?,?,?,?,?,?,?)}");
            cstmt.setInt(1, userId);
            cstmt.setString(2, ipAdd);
            cstmt.setInt(3, velosUser);
            cstmt.setString(4, failDate);
            cstmt.setString(5, failTime);
            cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(7, java.sql.Types.INTEGER);
            cstmt.registerOutParameter(8, java.sql.Types.VARCHAR);

            cstmt.execute();

            mailString = cstmt.getString(6);
            toUser = cstmt.getInt(7);
            tomail = cstmt.getString(8);

            if (toUser > 0) {
                ctrl.getControlValues("mailsettings");
                rows = ctrl.getCRows();
                if (rows > 0) {
                    smtpHost = (String) ctrl.getCValue().get(0);
                    domain = (String) ctrl.getCDesc().get(0);
                }

                userCtrl.getControlValues("eresuser");
                rows = userCtrl.getCRows();
                if (rows > 0) {
                    messageFrom = (String) userCtrl.getCDesc().get(0);
                }

                Properties props = new Properties();
                Session sendMailSession;
                Store store;
                Transport transport;

                sendMailSession = Session.getInstance(props, null);
                props.put("mail.smtp.host", smtpHost);

                Message newMessage = new MimeMessage(sendMailSession);
                newMessage.setFrom(new InternetAddress(messageFrom,
                        "Velos eResearch Customer Services"));
                newMessage.setRecipient(Message.RecipientType.TO,
                        new InternetAddress(tomail));
                newMessage.setSubject("Failed Login Attempt");
                newMessage.setSentDate(new java.util.Date());
                newMessage.setText(mailString);

                transport = sendMailSession.getTransport("smtp");
                transport.send(newMessage);
                success = 0;
            } else {
                success = -1;
            }

        }

        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In notifyAdmin in EventdefDAO EXCEPTION IN calling the notifyAdmin"
                            + ex);
            success = -1;
        } catch (MessagingException m) {
            Rlog.fatal("eventdef",
                    "In notifyAdmin in EventdefDAO EXCEPTION IN calling the notifyAdmin"
                            + m);
            success = -1;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "In notifyAdmin in EventdefDAO EXCEPTION IN calling the notifyAdmin"
                            + e);
            success = -1;
        }

        finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    /**
     * This method retrieves all the scheduled events of a calendar with
     * displacement in weeks and days
     *
     * @param calendarId
     */

    public void getAllProtSelectedEvents(int protocolId, String visitId, String evtName) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            Integer iValue = new Integer(0);
            String sValue = "";

            conn = getConnection();

            StringBuffer sbSql = new StringBuffer();

            //JM: 11April2008: After discussing with RKS:
            //KV:SW-FIN2b & SW-FIN2c
            //D-FIN-25-DAY0 BK
            sbSql.append(" SELECT decode(NUM_DAYS,0,NUM_DAYS,DISPLACEMENT) as DISPLACEMENT, " + "0 as EVENT_ID, " + "0 as ORG_ID, "
                + " 0 as CHAIN_ID, " + " '' as EVENT_TYPE, " + " visit_name  as NAME, " + " '' as NOTES, "
                + " 0 as COST, " + " '' as COST_DESCRIPTION, " + "0 as DURATION, "
                + " 0 as USER_ID, " + " ''  as LINKED_URI, " + "''  as FUZZY_PERIOD, "
                + " ''  as MSG_TO, " + " ''  as STATUS, " + " DESCRIPTION  as DESCRIPTION, "
                + " NUM_MONTHS MONTH , "
                + " NUM_WEEKS WEEK , "
                + " NUM_DAYS DAY, "
                + " INSERT_AFTER,"
                + " INSERT_AFTER_INTERVAL,"
                + " INSERT_AFTER_INTERVAL_UNIT,"
                + " 'V' as flag, pk_protocol_visit as FK_VISIT , '' as EVENT_CATEGORY, 0 as EVENT_SEQUENCE, "
                + " '' as Facilityname, "
                + " '' as eventcost "
                + " FROM sch_protocol_visit "
                + " WHERE  fk_protocol= ? ");

            	if (!visitId.equals("")){
            		sbSql.append(" and pk_protocol_visit="+visitId);
            	}

            	//KM-For No Interval denfined visit
                //KV:SW-FIN2b & SW-FIN2c
            	//BK-FIX-5979-MAR-28-2011
            	sbSql.append(" union"
	            + " SELECT DECODE((SELECT NUM_DAYS FROM SCH_PROTOCOL_VISIT v WHERE v.pk_protocol_visit= a.FK_VISIT),0,0,DISPLACEMENT) AS DISPLACEMENT, " 
	            + "EVENT_ID, " + "ORG_ID, "
	            + " CHAIN_ID, " + "EVENT_TYPE, " + "NAME, " + "NOTES, "
	            + " COST, " + "COST_DESCRIPTION, " + "DURATION, "
	            + " USER_ID, " + "LINKED_URI, " + "FUZZY_PERIOD, "
	            + " MSG_TO, " + "STATUS, " + "DESCRIPTION, "
	            + " 0 AS MONTH , "
	            + " 0 AS WEEK , "
	            + " 0 AS DAY, " 
	            + " 0 AS INSERT_AFTER,"
                + " 0 AS INSERT_AFTER_INTERVAL,"
                + " '' AS INSERT_AFTER_INTERVAL_UNIT,"
	            + "  'E' as flag, FK_VISIT , EVENT_CATEGORY, EVENT_SEQUENCE, "
	            + " (select site_name from er_site b  where b.pk_site = a.FACILITY_ID) as Facilityname, "
	            + " lst_cost(a.EVENT_ID) as eventcost "
	            + " FROM EVENT_DEF a "
	            + " WHERE  a.CHAIN_ID= ? AND a.FK_VISIT IS NOT NULL ");//BK-DAY 0


            	if (!visitId.equals("")){
            		sbSql.append(" and fk_visit="+visitId);
            	}

            	if (!evtName.equals("")){
            		sbSql.append(" and lower(name) like lower(lower('%"+evtName+"%'))");
            	}


            	sbSql.append("ORDER BY DISPLACEMENT,fk_visit,flag desc, EVENT_SEQUENCE");
            	//BK-FIX-5883-MAR-21-2011


            String mysql =sbSql.toString();

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, protocolId);
            pstmt.setInt(2, protocolId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setEvent_ids(new Integer(rs.getString("EVENT_ID")));
                setOrg_ids(new Integer(rs.getString("ORG_ID")));
                setChain_ids(rs.getString("CHAIN_ID"));
                setEvent_types(rs.getString("EVENT_TYPE"));
                setNames(rs.getString("NAME"));
                setNotes(rs.getString("NOTES"));
                setCosts(rs.getString("COST"));
                setCost_descriptions(rs.getString("COST_DESCRIPTION"));
                setDurations(rs.getString("DURATION"));
                setUser_ids(rs.getString("USER_ID"));
                setLinked_uris(rs.getString("LINKED_URI"));
                setFuzzy_periods(rs.getString("FUZZY_PERIOD"));
                setMsg_tos(rs.getString("MSG_TO"));
                setStatus(rs.getString("STATUS"));
                setDescription(rs.getString("DESCRIPTION"));
                setDisplacement(rs.getString("DISPLACEMENT"));                
                this.setIntervalArrays(rs.getInt("DISPLACEMENT"),rs.getInt("MONTH"),rs.getInt("WEEK"),rs.getInt("DAY"));
                this.setInsertAfter(rs.getInt("INSERT_AFTER")); //DFIN25-DAY0,BK, MAR-24-10
                this.createAfterIntervalStrings(rs.getInt("INSERT_AFTER"), rs.getInt("INSERT_AFTER_INTERVAL"), rs.getString("INSERT_AFTER_INTERVAL_UNIT"));
                setFlag(rs.getString("flag"));
                // SV, 9/27/04, cal-enh-03-b
                sValue = rs.getString("FK_VISIT");
                setEventCategory(rs.getString("EVENT_CATEGORY"));
                setEventSequences(rs.getString("EVENT_SEQUENCE"));
                setFacility(rs.getString("Facilityname"));//KV: SW-FIN2c	
                setEventCostDetails(rs.getString("eventcost"));//KV: SW-FIN2b
                if (sValue == null)
                    iValue = new Integer(0);
                else
                    iValue = new Integer(sValue.trim());
                setFk_visit(iValue);

            }

        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "eventdef",
                            "EventdefDao.getAllProtSelectedEvents EXCEPTION IN FETCHING FROM Eventdef table"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // by sonia, 24 dec 2003
    // REDTAG, shouldn't this be in assocdao????
    //modified by sonia abrol, 11/15/06 - removed all checks for status = 0 (enh# PS6)
    public void getScheduleEventsVisits(int patprot) {

        PreparedStatement pstmt = null;
        PreparedStatement pstmt2 = null;

        Connection conn = null;
        try {
            String visitsql = "";
            int totalDefinedVisits = 0;

            totalDefinedVisits = getTotalDefinedVisitsInSchedule(patprot);

            conn = getConnection();

            if (totalDefinedVisits > 0) {
                /*
                 * SV, 10/28/04, with the new understanding that visit # is not
                 * really serving it's purpose, decided to scratch it from
                 * patient schedule and use the original dense rank to get it
                 * (which is closely tied to the displacement, the new sorting
                 * order for everything) and use fk_Visit field in events1 to
                 * get visit related details like name. visitsql = " select
                 * distinct visit.visit_no visit, visit.visit_name visit_name " + "
                 * FROM SCH_EVENTS1 ev, event_assoc assoc, sch_protocol_visit
                 * visit " + " where ev.FK_ASSOC = assoc.event_id " + " and
                 * assoc.FK_VISIT = visit.PK_PROTOCOL_VISIT " + " and FK_PATPROT = ?
                 * and ev.status = 0 " ;
                 */
                visitsql = " select  Distinct visit, visit.visit_name visit_name "
                        + " FROM SCH_EVENTS1 ev, sch_protocol_visit visit "
                        + " where ev.FK_VISIT = visit.PK_PROTOCOL_VISIT "
                        + " and  FK_PATPROT = ? "
                        + " order by visit";

            } else {

                // get visits
                visitsql = "select Distinct visit "
                        + " FROM SCH_EVENTS1 "
                        + " WHERE  FK_PATPROT = ? ORDER BY VISIT";
            }

            pstmt = conn.prepareStatement(visitsql);
            pstmt.setInt(1, patprot);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setVisit(new Integer(rs.getInt("visit")));
                if (totalDefinedVisits > 0) {
                    setVisitName(rs.getString("visit_name"));
                    Rlog.debug("eventdef", "visit name="
                            + rs.getString("visit_name"));

                }
            }

            // get events

            String evsql = "select Distinct DESCRIPTION, SERVICE_SITE_ID FROM SCH_EVENTS1 "
                    + " WHERE  FK_PATPROT = ?   ORDER BY DESCRIPTION";

            pstmt2 = conn.prepareStatement(evsql);
            pstmt2.setInt(1, patprot);

            ResultSet rs2 = pstmt2.executeQuery();

            while (rs2.next()) {

                setNames(rs2.getString("DESCRIPTION"));
                setEventSOSIds(rs2.getString("SERVICE_SITE_ID"));
            }

            // end if events
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "eventdef",
                            "EventdefDao.getScheduleEventsVisits EXCEPTION IN FETCHING FROM sch_events1 table"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
                if (pstmt2 != null)
                    pstmt2.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    public void getStudyScheduleEventsVisits(int studyId,int protocolId) {

        PreparedStatement pstmt = null;
        PreparedStatement pstmt2 = null;

        Connection conn = null;
        try {
            String visitsql = "";
            int totalDefinedVisits = 0;

            totalDefinedVisits = getTotalDefinedVisitsInStudySchedule(studyId,protocolId);

            conn = getConnection();

            if (totalDefinedVisits > 0) {
                visitsql = " select  Distinct visit, visit.visit_name visit_name "
                        + " FROM SCH_EVENTS1 ev, sch_protocol_visit visit "
                        + " where ev.FK_VISIT = visit.PK_PROTOCOL_VISIT "
                        + " and  FK_STUDY = ? and ev.status = 0 and session_Id= LPAD (TO_CHAR(?), 10, '0') "
                        + " order by visit";

            } else {

                // get visits
                visitsql = "select Distinct visit "
                        + " FROM SCH_EVENTS1 "
                        + " WHERE  FK_STUDY = ? and status = 0 and session_Id= LPAD (TO_CHAR(?), 10, '0') "
                         + " ORDER BY VISIT";
            }

            pstmt = conn.prepareStatement(visitsql);
            pstmt.setInt(1, studyId);
            pstmt.setInt(2, protocolId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setVisit(new Integer(rs.getInt("visit")));
                if (totalDefinedVisits > 0) {
                    setVisitName(rs.getString("visit_name"));
                    Rlog.debug("eventdef", "visit name="
                            + rs.getString("visit_name"));

                }
            }

            // get events

            String evsql = "select Distinct DESCRIPTION FROM SCH_EVENTS1 "
                    + " WHERE  FK_STUDY = ? and session_Id= LPAD (TO_CHAR(?), 10, '0')   and status = 0  ORDER BY DESCRIPTION";

            pstmt2 = conn.prepareStatement(evsql);
            pstmt2.setInt(1, studyId);
            pstmt2.setInt(2, protocolId);

            ResultSet rs2 = pstmt2.executeQuery();

            while (rs2.next()) {

                setNames(rs2.getString("DESCRIPTION"));
            }
            // end if events
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "eventdef",
                            "EventdefDao.getStudyScheduleEventsVisits EXCEPTION IN FETCHING FROM sch_events1 table"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
                if (pstmt2 != null)
                    pstmt2.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /* modified by Sonia Abrol, removed status=0 check*/

    public void getVisitEvents(int patProtId, int status, int visit,
            String exeon,String type,int protocolId) {
    	PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            String mysql = "";
            String mysql1 = "";
            String mysql2 = "";
            String mysql3 = "";
            String mysql4 = "";

            conn = getConnection();
            if (type.equals("S"))
            	mysql1 = "select event_id, description, visit, isconfirmed, notes, event_exeon, SERVICE_SITE_ID, FK_CODELST_COVERTYPE, REASON_FOR_COVERAGECHANGE, "
            		+ " '' event_type "
                    + "from  sch_events1 where fk_study = ? and status= 0 and session_Id= LPAD (TO_CHAR(?), 10, '0')   and fk_visit is not null";
            else
                mysql1 = "select event_id, description, visit, isconfirmed, notes, event_exeon, SERVICE_SITE_ID, FK_CODELST_COVERTYPE , REASON_FOR_COVERAGECHANGE, "
                	+ " (select EVENT_TYPE from event_assoc ea where ea.event_id = fk_assoc) event_type "
                    + "from  sch_events1 where fk_patprot = ?  and fk_visit is not null";

            if (status != 0) {
                mysql2 = "  and isconfirmed = " + status + " ";
            }
            if (visit != 0) {
                mysql3 = " and visit = " + visit + " ";
            }
            if (exeon != null) {
                mysql4 = " and event_exeon = TO_DATE('" + exeon
                        + "','yyyy-mm-dd')";
            }

            //mysql = mysql1 + mysql2 + mysql3 + mysql4+ "order by fk_visit";
            //mysql = mysql1 + mysql2 + mysql3 + mysql4+ "ORDER BY ACTUAL_SCHDATE, START_DATE_TIME, EVENT_ID";
             mysql=mysql1 + mysql2 + mysql3 + mysql4+ " order by ACTUAL_SCHDATE, fk_visit, event_sequence ";
            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, patProtId);
            if (type.equals("S"))           pstmt.setInt(2, protocolId);



            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setDescription(rs.getString("description"));
                String id = rs.getString("event_id");
                id = id.trim();
                Integer test1 = new Integer(id);

                setEvent_ids(test1);
                setVisit(new Integer(rs.getInt("visit")));
                setStatus(rs.getString("ISCONFIRMED"));
                setNotes(rs.getString("NOTES"));
                setEvent_exeons(rs.getDate("EVENT_EXEON"));
                setEventSOSIds(rs.getString("SERVICE_SITE_ID"));
                setEventCoverageTypes(rs.getString("FK_CODELST_COVERTYPE"));
                setReasonForCoverageChange(rs.getString("REASON_FOR_COVERAGECHANGE"));
                setEvent_types(rs.getString("event_type"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.ValidateEventName EXCEPTION IN getVisitEvents(int patProtId) "
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }


    }


    /* Method to retrieve events for visit and status */

    public void getVisitEvents(int patProtId, int status, int visit,
            String exeon) {

    	        getVisitEvents( patProtId,  status,  visit,
                exeon,"",0);

            }

    /*
     * Method to update records for eventIds and then to send notification to
     * the user for the status change
     */

    public int saveMultipleEventsVisits(int enrollId, int userId,
            String[] arrEventIds, String[] arrStatusId,
            String[] arrOldStatusId, String[] arrExeon, String[] arrNote,
            String[] arrSos, String[] arrCoverage, String[] reasonForCoverageChng,
            String ipAdd)
    {
        return saveMultipleEventsVisits( enrollId,  userId,
                 arrEventIds, arrStatusId,
                arrOldStatusId, arrExeon, arrNote, arrSos, arrCoverage, reasonForCoverageChng,
                ipAdd,"");
    }
    
    public int saveMultipleEventsVisits(int enrollId, int userId,
            String[] arrEventIds, String[] arrStatusId,
            String[] arrOldStatusId, String[] arrExeon,
            String ipAdd)
    {
        return saveMultipleEventsVisits( enrollId,  userId,
                 arrEventIds, arrStatusId,
                arrOldStatusId, arrExeon,
                ipAdd,"");
    }

    public int saveMultipleEventsVisits(int enrollId, int userId,
            String[] arrEventIds, String[] arrStatusId,
            String[] arrOldStatusId, String[] arrExeon,
            String ipAdd,String type)

    {

        Rlog.debug("eventdef", "eventdefdao - enrollId" + enrollId);

        CallableStatement cstmt = null;
        int ret = -3;

        Connection conn = null;

        try {
            conn = getConnection();
            ArrayDescriptor adView = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY eventId = new ARRAY(adView, conn, arrEventIds);

            ArrayDescriptor adStatusId = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY statusId = new ARRAY(adStatusId, conn, arrStatusId);

            ArrayDescriptor adOldStatusId = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY oldStatusId = new ARRAY(adOldStatusId, conn, arrOldStatusId);

            ArrayDescriptor adExeon = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY exeon = new ARRAY(adExeon, conn, arrExeon);

            cstmt = conn.prepareCall("{call sp_mulEvtVstView(?,?,?,?,?,?,?,?,?)}");

            cstmt.setInt(1, enrollId);

            cstmt.setInt(2, userId);

            cstmt.setArray(3, eventId);

            cstmt.setArray(4, statusId);

            cstmt.setArray(5, oldStatusId);

            cstmt.setArray(6, exeon);            

            cstmt.setString(7, ipAdd);

            cstmt.registerOutParameter(8, java.sql.Types.INTEGER);

            cstmt.setString(9, type);

            cstmt.execute();

            Rlog.debug("eventdef", "after execute");

            ret = cstmt.getInt(8);

            return ret;

        }

        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In saveMultipleEventsVisits EXCEPTION IN calling the procedure"
                            + ex);
        }

        finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return ret;

    }

    public int saveMultipleEventsVisits(int enrollId, int userId,
            String[] arrEventIds, String[] arrStatusId,
            String[] arrOldStatusId, String[] arrExeon, String[] arrNote,
            String[] arrSos, String[] arrCoverage, String[] reasonForCoverageChng,
            String ipAdd,String type)

    {

        Rlog.debug("eventdef", "eventdefdao - enrollId" + enrollId);

        CallableStatement cstmt = null;
        int ret = -3;

        Connection conn = null;

        try {
            conn = getConnection();
            ArrayDescriptor adView = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY eventId = new ARRAY(adView, conn, arrEventIds);

            ArrayDescriptor adStatusId = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY statusId = new ARRAY(adStatusId, conn, arrStatusId);

            ArrayDescriptor adOldStatusId = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY oldStatusId = new ARRAY(adOldStatusId, conn, arrOldStatusId);

            ArrayDescriptor adExeon = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY exeon = new ARRAY(adExeon, conn, arrExeon);

            ArrayDescriptor adNote = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY note = new ARRAY(adNote, conn, arrNote);

            ArrayDescriptor adSos = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY sos = new ARRAY(adSos, conn, arrSos);

            ArrayDescriptor adCoverage = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY coverage = new ARRAY(adCoverage, conn, arrCoverage);

            ArrayDescriptor adReasonForCoverageChange = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
            ARRAY rsnForCovCng = new ARRAY(adReasonForCoverageChange, conn, reasonForCoverageChng);

            cstmt = conn.prepareCall("{call sp_mulEvtVst(?,?,?,?,?,?,?,?,?,?,?,?,?)}");

            cstmt.setInt(1, enrollId);

            cstmt.setInt(2, userId);

            cstmt.setArray(3, eventId);

            cstmt.setArray(4, statusId);

            cstmt.setArray(5, oldStatusId);

            cstmt.setArray(6, exeon);

            cstmt.setArray(7, note);

            cstmt.setArray(8, sos);

            cstmt.setArray(9, coverage);

            cstmt.setArray(10, rsnForCovCng);

            cstmt.setString(11, ipAdd);

            cstmt.registerOutParameter(12, java.sql.Types.INTEGER);

            cstmt.setString(13, type);



            cstmt.execute();

            Rlog.debug("eventdef", "after execute");

            ret = cstmt.getInt(12);

            return ret;

        }

        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In saveMultipleEventsVisits EXCEPTION IN calling the procedure"
                            + ex);
        }

        finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return ret;

    }
    

    public int propagateEventUpdates(int protocolId, int eventId,
            String tableName, int primary_key, String propagateInVisitFlag,
            String propagateInEventFlag, String mode, String calType) {
        // SV, 9/16/04, a new method to propagate Event updates to other events
        // in the same calendar depending on the flags.
        // eventsInVisit - All the events with the same visit #, within the same
        // calendar
        // sameEvents - All events with the same name within the calendar.
        // SV, 10/20/04, added the 2 extra parameters: mode= 'A' - Add, 'U' -
        // Update., calType = 'L' - Library, 'S' - Study Calendar.
        Rlog.debug("eventdef", "eventdefdao - propagateEventUpdates Start");
        CallableStatement cstmt = null;
        int success = -1;
        Connection conn = null;
        try {
            Rlog.debug("eventdef",
                    "eventdefDao: just before calling sp_propagate");
            Rlog.debug("eventdef", "protocolId=" + protocolId);
            Rlog.debug("eventdef", "eventId=" + eventId);
            Rlog.debug("eventdef", tableName);
            Rlog.debug("eventdef", "propagateInVisit=" + propagateInVisitFlag);
            Rlog.debug("eventdef", "propagateInEvent=" + propagateInEventFlag);
            Rlog.debug("eventdef", "mode=" + mode);
            Rlog.debug("eventdef", "calType=" + calType);

            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_PROPGT_EVTUPDATES.sp_propagate_evt_updates(?,?,?,?,?,?,?,?)}");

            cstmt.setInt(1, protocolId);
            cstmt.setInt(2, eventId);
            cstmt.setString(3, tableName);
            cstmt.setInt(4, primary_key);
            cstmt.setString(5, propagateInVisitFlag);
            cstmt.setString(6, propagateInEventFlag);
            cstmt.setString(7, mode);
            cstmt.setString(8, calType);

            cstmt.execute();
            Rlog.debug("eventdef",
                    "eventdefDao: just after calling sp_propagate");

            success = 1;

        }

        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In propagateEventUpdates in Eventdef line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;

    }
    
    public void saveNetworkDoc(int docid, int networkId,String networkFlag,
            String creator,String ipAdd) {
        // SV, 9/16/04, a new method to propagate Event updates to other events
        // in the same calendar depending on the flags.
        // eventsInVisit - All the events with the same visit #, within the same
        // calendar
        // sameEvents - All events with the same name within the calendar.
        // SV, 10/20/04, added the 2 extra parameters: mode= 'A' - Add, 'U' -
        // Update., calType = 'L' - Library, 'S' - Study Calendar.
        Rlog.debug("eventdef", "eventdefdao - saveNetworkDoc Start");
        CallableStatement cstmt = null;
        
        Connection conn = null;
        try {
            Rlog.debug("eventdef",
                    "eventdefDao: just before calling sp_propagate");
            Rlog.debug("eventdef", "docid=" + docid);
            Rlog.debug("eventdef", "networkId=" + networkId);
            

            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_PROPGT_EVTUPDATES.sp_add_network_doc(?,?,?,?,?)}");

            cstmt.setInt(1, docid);
            cstmt.setInt(2, networkId);
            cstmt.setString(3, networkFlag);
            cstmt.setString(4, creator);
            
            cstmt.setString(5, ipAdd);
                        cstmt.execute();
            Rlog.debug("eventdef",
                    "eventdefDao: just after calling sp_propagate");

            

        }

        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In propagateEventUpdates in Eventdef line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        
    }


    public int removeEventCost(int eventId, int eventcostId, String mode) {
        // SM, 9/07/10, a new method to remove Event cost and corr lineitem
    	
        Rlog.debug("eventdef", "eventdefdao - removeEventCost Start");
        CallableStatement cstmt = null;
        int success = -1;
        Connection conn = null;
        try {
        	           
            Rlog.debug("eventdef", "eventdefDao: just before calling removeEventCost");
            Rlog.debug("eventdef", "eventId=" + eventId);
            Rlog.debug("eventdef", "eventcostId=" + eventcostId);
            Rlog.debug("eventdef", "mode=" + mode);
            
            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_PROPGT_EVTUPDATES.sp_delete_event_cost(?,?,?)}");
            
            cstmt.setInt(1, eventId);
            cstmt.setInt(2, eventcostId);
            cstmt.setString(3, mode);

            cstmt.execute();
            Rlog.debug("eventdef",
                    "eventdefDao: just after calling removeEventCost");

            success = 1;

        }

        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In propagateEventUpdates in Eventdef line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;

    }

    // removeEventcost() Overloaded for INF-18183 ::: Raviesh
    public int removeEventCost(int eventId, int eventcostId, String mode,Hashtable<String, String> args) {
        // SM, 9/07/10, a new method to remove Event cost and corr lineitem
    	
        Rlog.debug("eventdef", "eventdefdao - removeEventCost Start");
        CallableStatement cstmt = null;
        UserBean userBean=null;
        UserJB userJB=new UserJB();
        int success = -1;
        Connection conn = null;
        Connection connAudit = null;
        try {
        	 String currdate =DateUtil.getCurrentDate(); /*gets the current date from DateUtil*/
             String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
             String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
             String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
             String appModule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the App Module Name from the Hashtable*/
             userJB.setUserId(StringUtil.stringToNum(userID));
             userBean= userJB.getUserDetails();
             String getRidValue= AuditUtils.getRidValue("SCH_EVENTCOST","esch","PK_EVENTCOST="+eventcostId);/*Fetches the RID/PK_VALUE*/
             Hashtable<String, ArrayList<String>> resultHT = new Hashtable<String, ArrayList<String>>();
             ArrayList<String> pkValue = new ArrayList<String>();
             pkValue.add(String.valueOf(eventcostId));
             ArrayList<String> ridValue = new ArrayList<String>();
             ridValue.add(getRidValue);
             resultHT.put(AuditUtils.ROW_PK_KEY, pkValue);
             resultHT.put(AuditUtils.ROW_RID_KEY, ridValue);
             connAudit = AuditUtils.insertAuditRow("SCH_EVENTCOST",resultHT,appModule,args,"esch");
         	        
            Rlog.debug("eventdef", "eventdefDao: just before calling removeEventCost");
            Rlog.debug("eventdef", "eventId=" + eventId);
            Rlog.debug("eventdef", "eventcostId=" + eventcostId);
            Rlog.debug("eventdef", "mode=" + mode);
            
            conn = getConnection();
            cstmt = conn.prepareCall("{call PKG_PROPGT_EVTUPDATES.sp_delete_event_cost(?,?,?)}");
            
            cstmt.setInt(1, eventId);
            cstmt.setInt(2, eventcostId);
            cstmt.setString(3, mode);

            cstmt.execute();
            /* Maintain audit row after delete*/
            AuditUtils.updateAuditROw( "esch",userJB.getUserId()+", "+userBean.getUserLastName()+", "+userBean.getUserFirstName(),getRidValue,mode );
            AuditUtils.commitOrRollback(connAudit, true);
            Rlog.debug("eventdef",
                    "eventdefDao: just after calling removeEventCost");

            success = 1;
            
        }

        catch (SQLException ex) {
        	context.setRollbackOnly();
            Rlog.fatal("eventdef",
                    "In propagateEventUpdates in Eventdef line EXCEPTION IN calling the procedure"
                            + ex);
            try{
                AuditUtils.commitOrRollback(connAudit, false);
            }catch(Exception e){
                Rlog.fatal("eventdef","Exception occurs at AuditUtils.commitOrRollback()" + e);
            }
        }   catch (Exception ex) {
        	context.setRollbackOnly();
            Rlog.fatal("eventdef",
                    "In propagateEventUpdates in Eventdef line EXCEPTION IN calling the procedure"
                            + ex);
            try{
                AuditUtils.commitOrRollback(connAudit, false);
            }catch(Exception e){
                Rlog.fatal("eventdef","Exception occurs at AuditUtils.commitOrRollback()" + e);
            }
        }
        finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;

    }
    
    public int UpdateEventVisitIds(String chain_id, String userId, String ipAdd) {
        Rlog.debug("eventdef", "eventdefdao - UpdateEventVisitIds start");
        CallableStatement cstmt = null;
        int success = -1;

        Connection conn = null;
        try {

            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call sp_update_events_with_visitids(?,?,?)}");

            cstmt.setString(1, chain_id);
            cstmt.setString(2, userId);
            cstmt.setString(3, ipAdd);

            cstmt.execute();
            success = 1;

        }

        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In UpdateEventVisitIds in Eventdef line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;

    }

    // SV, 10/14/04, added to be able to see if there is any user defined visits
    // in calendar.
    public int getTotalDefinedVisitsInSchedule(int patprot) {
        PreparedStatement pstmt = null;
        Connection conn = null;

        int total_visits = 0;
        try {
            conn = getConnection();

            String mysql = "select count(distinct visit.visit_no) TOTAL_VISITS "
                    + " FROM SCH_EVENTS1 ev, SCH_PROTOCOL_VISIT visit "
                    + " where ev.FK_VISIT = visit.PK_PROTOCOL_VISIT "
                    + " and  FK_PATPROT = ?  ";

            Rlog.debug("eventdef", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, patprot);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                total_visits = rs.getInt("TOTAL_VISITS");
                break; // there is only one row!
            }
            Rlog.debug("eventdef", "total visits = " + total_visits);

        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "eventdef",
                            "EventdefDao.getTotalDefinedVisitsInSchedule EXCEPTION IN FETCHING FROM SCH_PROTOCOL_VISIT table"
                                    + ex);
            return (0);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();

            } catch (Exception e) {
            }

        }
        // total_visits here will either be = count or 0..
        return (total_visits);

    }

    public int getTotalDefinedVisitsInStudySchedule(int studyId,int protocolId) {
        PreparedStatement pstmt = null;
        Connection conn = null;

        int total_visits = 0;
        try {
            conn = getConnection();

            String mysql = "select count(distinct visit.visit_no) TOTAL_VISITS "
                    + " FROM SCH_EVENTS1 ev, SCH_PROTOCOL_VISIT visit "
                    + " where ev.FK_VISIT = visit.PK_PROTOCOL_VISIT "
                    + " and  FK_Study = ? and ev.status = 0 and session_Id= LPAD (TO_CHAR(?), 10, '0') ";

            Rlog.debug("eventdef", mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, studyId);
            pstmt.setInt(2,protocolId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                total_visits = rs.getInt("TOTAL_VISITS");
                break; // there is only one row!
            }
            Rlog.debug("eventdef", "total visits = " + total_visits);

        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "eventdef",
                            "EventdefDao.getTotalDefinedVisitsInStudySchedule EXCEPTION IN FETCHING FROM SCH_PROTOCOL_VISIT table"
                                    + ex);
            ex.printStackTrace();
            return (0);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();

            } catch (Exception e) {
            }

        }
        // total_visits here will either be = count or 0..
        return (total_visits);

    }


    // SV, 10/14/04, gets the defined visit name for the defined visit #. If the
    // visit no doesn't exist, then "visit + visit_no" is returned.
    public String getDefinedVisitNameFromVisitNo(int visitNo) {
        int index;
        try {
            index = this.visit.indexOf(new Integer(visitNo));
            Rlog.debug("eventdef", "visit No=" + visitNo + " index = " + index);
            Rlog.debug("eventdef", "name ="
                    + this.visitName.get(index).toString());

            return ((index == -1) ? "" : this.visitName.get(index).toString());

        } catch (IndexOutOfBoundsException ie) {
            Rlog.debug("eventdef", "index out of bound");

            return ("Visit " + visitNo);
        } catch (Exception e) {
            return ("");
        }
    }
//D-FIN-25-DAY0 BK
    public void setIntervalArrays(int displacement,int pMonth,int pWeek,int pDay) {        
        int temp;
        int month, week, day;
        if (displacement < 0) {
            setWeek(new Integer("-1"));
            setMonth(new Integer("-1"));
            setDay(new Integer(displacement));

        } else {
        
            setDay(new Integer(pDay));
            setWeek(new Integer(pWeek));
            setMonth(new Integer(pMonth));
         }  

    }
    
  //D-FIN-25-DAY0 BK
    /**
     * creates the interval string for visit if set After field is set.
     * 
     */    
    public void createAfterIntervalStrings(int insertAfter,int insertAfterInterval,String intervalUnit ) {
    	String intervalString = "";
    	if(insertAfter!=0){
    		intervalString = ""+insertAfterInterval;
    		if("W".equals(intervalUnit)){
    			intervalString = intervalString + " Week(s)";
    		}else if("M".equals(intervalUnit)){
    			intervalString = intervalString + " Month(s)";
    		}
    		else{
    			intervalString = intervalString + " Day(s)";
    		}   
    		intervalString = intervalString + " After";        
    	}	
    	this.getAfterIntervalStrings().add(intervalString);
    }
    
    public int copyProtToLib(String protocol_id, String study_id,String name,String desc,String type,String shareWith,
            String userId, String ipAdd) {
        CallableStatement callablestatement = null;
        int newProtocol = -1;
        Connection connection = null;
        Rlog.debug("eventassoc",
                "In CopyProtToLib in EventDefDao line number 1");
        try {
            connection = getConnection();
            callablestatement = connection
                    .prepareCall("{call sp_copyprottolib(?,?,?,?,?,?,?,?,?)}");
            callablestatement.setInt(1, EJBUtil.stringToNum(protocol_id));
            callablestatement.setInt(2, EJBUtil.stringToNum(study_id));
            callablestatement.setString(3, name);
            callablestatement.setString(4, desc);
            callablestatement.setInt(5, EJBUtil.stringToNum(type));
            callablestatement.setString(6,shareWith);
            callablestatement.setInt(7, EJBUtil.stringToNum(userId));
            callablestatement.registerOutParameter(8, java.sql.Types.INTEGER);
            callablestatement.setString(9, ipAdd);
            callablestatement.execute();
            newProtocol = callablestatement.getInt(8);
        } catch (SQLException sqlexception) {
            Rlog.fatal("eventdef",
                    "In CopyProtToLib in Eventassoc line EXCEPTION IN calling the procedure"
                            + sqlexception);
            sqlexception.printStackTrace();
        } finally {
            try {
                if (callablestatement != null)
                    callablestatement.close();
            } catch (Exception exception1) {
            }
            try {
                if (connection != null)
                    connection.close();
            } catch (Exception exception2) {
            }
        }
        return newProtocol;
    }


    //Modified by Manimaran on july06,2006 to stop sending emails after the schedule is discontinued
    public int msgStatusUpdateForSchdiscontinue(int studyId,int patProtPK) {
        CallableStatement cstmt = null;
        int success = -1;
        Connection conn = null;
        try {

            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call pkg_gensch.sp_update_msgstatus_discdt(?,?)}");

            cstmt.setInt(1, studyId);
            cstmt.setInt(2, patProtPK);
            cstmt.execute();
            success = 1;
        }

        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "In msgStatusUpdateForSchdiscontinue in Eventdef line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;

    }

    ////////used in patient portal
    public void FetchPatientSchedule(int protocol_id, int patient_id, Hashtable moreParams) {

        //PreparedStatement pstmt = null;
        //Connection conn = null;
        String scheduleFilter = "";
        String CRFSQL = "";
        String OrderBy = "";
        String countSql = "";
        BrowserRows br = new BrowserRows();

        Hashtable htCRF = new Hashtable();
        long rowsReturned = 0;
        BrowserRowsJB brJb = new BrowserRowsJB();

        String rangeFrom = "";
        String rangeFromUnit = "";
        String rangeTo = "";
        String rangeToUnit = "";

        int lessThanCurrentDays = 0;
        int greaterThanCurrentDays = 0;
        String dateFilter = "";

        try {
           // conn = getConnection();

        	if (moreParams != null)
        	{
        		if (moreParams.containsKey("scheduleFrom"))
        		{
        			rangeFrom = (String) moreParams.get("scheduleFrom");
        			rangeFromUnit = (String) moreParams.get("scheduleFromUnit");

        			if (! StringUtil.isEmpty(rangeFrom))
        			{
        				lessThanCurrentDays = DateUtil.getNumberOfDaysFromUnit(Integer.parseInt(rangeFrom),rangeFromUnit);
        			}
        		}

        		if (moreParams.containsKey("scheduleTo"))
        		{
        			rangeTo = (String) moreParams.get("scheduleTo");
        			rangeToUnit = (String) moreParams.get("scheduleToUnit");


        			if (! StringUtil.isEmpty(rangeTo))
        			{
        				greaterThanCurrentDays = DateUtil.getNumberOfDaysFromUnit(Integer.parseInt(rangeTo),rangeToUnit);
        			}

        		}

        		if (lessThanCurrentDays > 0)
        		{
        			dateFilter = " and ACTUAL_SCHDATE >= (sysdate - " + lessThanCurrentDays + ") ";

        		}

        		if (greaterThanCurrentDays > 0)
        		{
        			dateFilter = dateFilter + " and ACTUAL_SCHDATE <= (sysdate + " + greaterThanCurrentDays + ") ";

        		}

        	}

            scheduleFilter = " PATIENT_ID= lpad(to_char("+patient_id+"),10,'0') AND  SESSION_ID = lpad(to_char("+protocol_id+"),10,'0')  " + dateFilter ;

            //apply check for latest schedule for a calendar even if its discontinued
            scheduleFilter = scheduleFilter + " and fk_patprot = pkg_patient.fget_latest_sch_patprot("+ patient_id+" , "+ protocol_id+") ";

            String mysql = "select ev.DESCRIPTION, " + "START_DATE_TIME, "
                    + " VISIT_TYPE_ID, EVENT_ID, ISCONFIRMED, "
                    + "	FK_ASSOC, " + "	EVENT_NOTES, " + "	EVENT_EXEON, "
                    + "	to_char(ACTUAL_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) ACTUAL_SCHDATE, " + "	EVENT_EXEBY, "
                    + "	  visit , visit_name,(select codelst_desc from sch_codelst where pk_codelst = isconfirmed) status_desc,fk_patprot,fk_visit "
                    + " FROM SCH_EVENTS1 ev ,sch_protocol_visit"
                    + " WHERE  " + scheduleFilter + " and  fk_visit = pk_protocol_visit";
             // BK #4012 modified 20th july 2010
            OrderBy = " ORDER BY ev.ACTUAL_SCHDATE , ev.fk_visit , ev.event_sequence";

            countSql = " select count(9) from (" +  mysql  +")";

            Hashtable htData = new Hashtable();

            mysql = mysql + OrderBy;

            htData = brJb.getSchedulePageRows(1, 500,mysql, 1, countSql, null,
                    null, " where " +  scheduleFilter) ;



            br = (BrowserRows) htData.get("schedule");
			htCRF = (Hashtable) htData.get("eventcrf");


			rowsReturned = br.getRowReturned();

			for(int i=1; i<=rowsReturned ; i++)
			{
			    setEvent_ids(Integer.valueOf(br.getBValues(i,"EVENT_ID")));
                setNames(br.getBValues(i,"DESCRIPTION"));
                setFuzzy_periods(br.getBValues(i,"VISIT_TYPE_ID"));
                //setStart_date_times(br.getBValues(i,"START_DATE_TIME") );
                setStatus(br.getBValues(i,"status_desc"));
                setOrg_ids(Integer.valueOf(br.getBValues(i,"FK_ASSOC")));
                setNotes(br.getBValues(i,"EVENT_NOTES"));
                //setEvent_exeons(br.getBValues(i,"EVENT_EXEON"));
                setActualDateString(br.getBValues(i,"ACTUAL_SCHDATE"));
                setUser_ids(br.getBValues(i,"EVENT_EXEBY"));

                setFkVisit(br.getBValues(i,"fk_visit"));

                setVisitName(br.getBValues(i,"visit_name"));
                setPatprotIds(br.getBValues(i,"fk_patprot")); //to get the patprot of the schedule
            }

			setHtScheduleCRF(htCRF);

        } catch (Exception ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.FetchPatientSchedule  EXCEPTION : ");
             ex.printStackTrace();
        } finally
        {
        }

    }

    public int fetchEventIdByVisitAndDisplacement(String anEventId, String whichTable,
            String fkVisit, String displacement) {
        int myEventId = 0;
        Connection conn = null;
        PreparedStatement pstmt = null;
        String myTable = null;
        if ("event_assoc".equals(StringUtil.trueValue(whichTable).toLowerCase())) {
            myTable = "event_assoc";
        } else {
            myTable = "event_def";
        }
        int aChainId = 0, aCost = 0;
        try {
            StringBuffer sql = new StringBuffer();
            sql.append(" select chain_id, cost from ").append(myTable).append(" where event_id = ? ");
            conn = getConnection();
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, Integer.parseInt(anEventId));
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()) {
                aChainId = rs.getInt("chain_id");
                aCost = rs.getInt("cost");
            }
        } catch(Exception e) {
        } finally {
            if (pstmt != null) { try { pstmt.close(); pstmt = null; } catch(Exception e) {} }
            if (conn != null) { try { conn.close(); conn = null; } catch(Exception e) {} }
        }
        try {
            StringBuffer sql = new StringBuffer();
            sql.append(" select event_id from ").append(myTable).append(" where chain_id = ? ");
            sql.append(" and cost = ? and fk_visit = ? and ");
            if (displacement == null || displacement.length() == 0 || "0".equals(displacement)) {
                sql.append(" displacement is null ");
            } else {
                sql.append(" displacement = ? ");
            }
            conn = getConnection();
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, aChainId);
            pstmt.setInt(2, aCost);
            pstmt.setInt(3, Integer.parseInt(fkVisit));
            if (displacement != null && displacement.length() > 0 && !"0".equals(displacement)) {
                pstmt.setInt(4, Integer.parseInt(displacement));
            }
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()) {
                myEventId = rs.getInt("event_id");
            }
        } catch(Exception e) {
        } finally {
            if (pstmt != null) { try { pstmt.close(); pstmt = null; } catch(Exception e) {} }
            if (conn != null) { try { conn.close(); conn = null; } catch(Exception e) {} }
        }
        return myEventId;
    }

    public int fetchEventIdByVisit(String anEventId, String whichTable, String fkVisit) {
        int myEventId = 0;
        Connection conn = null;
        PreparedStatement pstmt = null;
        String myTable = null;
        if ("event_assoc".equals(StringUtil.trueValue(whichTable).toLowerCase())) {
            myTable = "event_assoc";
        } else {
            myTable = "event_def";
        }
        int aChainId = 0, aCost = 0;
        try {
            StringBuffer sql = new StringBuffer();
            sql.append(" select chain_id, cost from ").append(myTable).append(" where event_id = ? ");
            conn = getConnection();
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, Integer.parseInt(anEventId));
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()) {
                aChainId = rs.getInt("chain_id");
                aCost = rs.getInt("cost");
            }
        } catch(Exception e) {
            Rlog.fatal("eventdef", "EventdefDao.fetchEventIdByVisit-1: "+e);
        } finally {
            if (pstmt != null) { try { pstmt.close(); pstmt = null; } catch(Exception e) {} }
            if (conn != null) { try { conn.close(); conn = null; } catch(Exception e) {} }
        }
        try {
            StringBuffer sql = new StringBuffer();
            sql.append(" select event_id from ").append(myTable).append(" where chain_id = ? ");
            sql.append(" and cost = ? and fk_visit = ? ");
            conn = getConnection();
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, aChainId);
            pstmt.setInt(2, aCost);
            pstmt.setInt(3, Integer.parseInt(fkVisit));
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()) {
                myEventId = rs.getInt("event_id");
            }
        } catch(Exception e) {
            Rlog.fatal("eventdef", "EventdefDao.fetchEventIdByVisit-2: "+e);
        } finally {
            if (pstmt != null) { try { pstmt.close(); pstmt = null; } catch(Exception e) {} }
            if (conn != null) { try { conn.close(); conn = null; } catch(Exception e) {} }
        }
        return myEventId;
    }

    //-------JM: 31Mar2008
    //KM-#5949
    public int deleteEvtOrVisits(String[] deleteIds, String whichTable, String[] flags, int user) {

        CallableStatement cstmt = null;
        Rlog.debug("eventdef",
                "In deleteEvtOrVisits in EventdefDao line number 0");
        int newProtocol = -1;

        Connection conn = null;
        try {


        	conn = getConnection();

            ArrayDescriptor inIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY delIdsArray = new ARRAY(inIds, conn, deleteIds);



            ArrayDescriptor flagIds = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
            ARRAY flagsArray = new ARRAY(flagIds, conn, flags);

            //KM-#5949
            cstmt = conn.prepareCall("{call pkg_calendar.sp_delete_event_visit(?,?,?,?)}");

            cstmt.setArray(1, delIdsArray);
            cstmt.setString(2, whichTable);
            cstmt.setArray(3, flagsArray);
            cstmt.setInt(4, user);

            cstmt.execute();
            newProtocol = 0;
        }

        catch (SQLException ex) {
            Rlog
                    .fatal(
                            "eventdef","In deleteEvtOrVisits() EXCEPTION IN calling the proc..."
                                    + ex);
            newProtocol = -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return newProtocol;
    }

    //Modified for INF-18183 and BUG#7224 : Raviesh   
    public int deleteEvtOrVisits(String[] deleteIds, String whichTable, String[] flags, int user,Hashtable<String, String> args) {

        CallableStatement cstmt = null;
        Rlog.debug("eventdef",
                "In deleteEvtOrVisits in EventdefDao line number 0");
        int newProtocol = -1;
        String condition="";
        Connection conn = null;
        Connection connAudit = null;
        try {


        	conn = getConnection();

            ArrayDescriptor inIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY delIdsArray = new ARRAY(inIds, conn, deleteIds);



            ArrayDescriptor flagIds = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
            ARRAY flagsArray = new ARRAY(flagIds, conn, flags);
            
            AuditBean audit=null;
        	String pkVal = "";
        	String ridVal = "";
           
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appModule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the App Module Name from the Hashtable*/
            
            for(int i=0; i<deleteIds.length; i++)
        		condition = condition + "," + deleteIds[i];  
        	
        	//condition = "PK_PATPROT IN ("+condition.substring(1)+")";
            condition="PK_PROTOCOL_VISIT IN ("+condition.substring(1)+")";

        	Hashtable<String, ArrayList<String>> getRowValues = AuditUtils.getRowValues("SCH_PROTOCOL_VISIT",condition,"esch");/*Fetches the RID/PK_VALUE*/
        	Hashtable<String, ArrayList<String>> resultHT = new Hashtable<String, ArrayList<String>>();
        	       	
        	ArrayList<String> rowPK  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_PK_KEY);
    		ArrayList<String> rowRID  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_RID_KEY);                
    		
    			resultHT.put(AuditUtils.ROW_PK_KEY, rowPK);    			
    			resultHT.put(AuditUtils.ROW_RID_KEY, rowRID);
    			
                try {
					connAudit = AuditUtils.insertAuditRow("SCH_PROTOCOL_VISIT",resultHT,appModule,args,"esch");
				} catch (Exception e) {					
					e.printStackTrace();
				}          
            
            
            //KM-#5949
            cstmt = conn.prepareCall("{call pkg_calendar.sp_delete_event_visit(?,?,?,?)}");

            cstmt.setArray(1, delIdsArray);
            cstmt.setString(2, whichTable);
            cstmt.setArray(3, flagsArray);
            cstmt.setInt(4, user);

            cstmt.execute();
            newProtocol = 0;
            
            try {
				AuditUtils.commitOrRollback(connAudit, true);
			} catch (Exception e) {				
				e.printStackTrace();
			}
            
        }

        catch (SQLException ex) {
            Rlog
                    .fatal(
                            "eventdef","In deleteEvtOrVisits() EXCEPTION IN calling the proc..."
                                    + ex);
            try{
        		AuditUtils.commitOrRollback(connAudit, false);
        	}catch(Exception e){
        		Rlog.fatal("formlib","Exception occurs at AuditUtils.commitOrRollback()" + e);
        	}
            newProtocol = -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
            try {
                if (connAudit != null)
                	connAudit.close();
            } catch (Exception e) {
            }
        }
        return newProtocol;
    }
    //------

	public ArrayList getActualDateString() {
		return actualDateString;
	}

	public void setActualDateString(ArrayList actualDateString) {
		this.actualDateString = actualDateString;
	}

	public void setActualDateString(String actualDateStr) {
		this.actualDateString.add(actualDateStr);
	}

	public Hashtable getHtScheduleCRF() {
		return htScheduleCRF;
	}

	public void setHtScheduleCRF(Hashtable htScheduleCRF) {
		this.htScheduleCRF = htScheduleCRF;
	}
	//JM: 14Apr2008
	public void getProtVisitAndEvents(int protocolId, String searchStr,String resetSort) { /*YK 04Jan- SUT_1_Requirement_18489*/
        PreparedStatement pstmt = null;
        Connection conn = null;

        StringBuffer sbMySql = new StringBuffer();
        try {

            conn = getConnection();
            sbMySql.append("select EVENT_ID, "
                    + "ORG_ID, "
                    + "CHAIN_ID, "
                    + "EVENT_TYPE, "
                    + "NAME, "
                    + "NOTES, "
                    + "COST, "
                    + "COST_DESCRIPTION, "
                    + "DURATION, "
                    + "USER_ID, "
                    + "LINKED_URI, "
                    + "FUZZY_PERIOD, "
                    + "MSG_TO, "
                    //+ "STATUS, "
                    + "DESCRIPTION, "
                    + "DISPLACEMENT, "
                    + "EVENT_CPTCODE, "
                    + "EVENT_CATEGORY "
                    + "FROM EVENT_DEF "
                    + "WHERE  CHAIN_ID= ? AND FK_VISIT IS NULL and event_id != ? ");   //BK-DAY0

            		if(!searchStr.equals("")){
            			//KM-3456
            			sbMySql.append("and ( trim(lower(name)) like trim(lower('%"+searchStr+"%')) escape '\\' OR trim(lower(event_category)) like trim(lower('%" +searchStr + "%')) escape '\\' ) ");
            		}
            		/*YK 04Jan- SUT_1_Requirement_18489*/
            		if(resetSort.equals("true")){
            			sbMySql.append("ORDER BY EVENT_ID");
            		}else{

                		sbMySql.append("ORDER BY trim(lower(NAME))");
            		}
            		

            String mysql = 	sbMySql.toString();


            Rlog.debug("eventdef", mysql);


            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, protocolId);
            pstmt.setInt(2, protocolId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                String id = rs.getString("EVENT_ID");

                id = id.trim();

                Integer test1 = new Integer(id);

                setEvent_ids(test1);

                String id1 = rs.getString("ORG_ID");
                id1 = id1.trim();
                Integer test2 = new Integer(id1);
                setOrg_ids(test2);

                setChain_ids(rs.getString("CHAIN_ID"));
                setEvent_types(rs.getString("EVENT_TYPE"));

                setNames(rs.getString("NAME"));

                setNotes(rs.getString("NOTES"));

                setCosts(rs.getString("COST"));

                setCost_descriptions(rs.getString("COST_DESCRIPTION"));

                setDurations(rs.getString("DURATION"));

                setUser_ids(rs.getString("USER_ID"));

                setLinked_uris(rs.getString("LINKED_URI"));

                setFuzzy_periods(rs.getString("FUZZY_PERIOD"));

                setMsg_tos(rs.getString("MSG_TO"));
                //KM-#DFin9
                //setStatus(rs.getString("STATUS"));
                setDescription(rs.getString("DESCRIPTION"));
                setDisplacement(rs.getString("DISPLACEMENT"));
                setCptCodes(rs.getString("EVENT_CPTCODE"));
                setEventCategory(rs.getString("EVENT_CATEGORY"));

            }



        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.getProtVisitAndEvents EXCEPTION IN FETCHING FROM Eventdef table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }


	//JM: 14Apr2008

	/**
	 *
	 * @param visitId, eventId, tableName
	 * @return maximum sequence number
	 */
	  public int getMaxSeqForVisitEvents(int visitId, String eventNam, String tableName,String catName){
		  PreparedStatement pstmt = null;
	      Connection conn = null;
	      String sql = "";
	      int ret = 0;
	      try {
	          conn = getConnection();

		          sql="select f_get_event_sequence_new(?,?,?,?) as max_seq from dual";
		          pstmt = conn.prepareStatement(sql);
		          pstmt.setInt(1, visitId);
		          pstmt.setString(2, eventNam);
		          pstmt.setString(3, tableName);
		          pstmt.setString(4, catName);


	          ResultSet rs = pstmt.executeQuery();

	            while (rs.next()) {
	            	ret =   rs.getInt("max_seq");
	            }

	      	  if (pstmt != null)
	              pstmt.close();

	      	  if (conn != null)
	            conn.close();


	           return ret;

	      } catch (SQLException ex) {
	          Rlog.fatal("EventdefDao","EventdefDao EXCEPTION in getMaxSeqForVisitEvents()"+ ex);

	          return 0;

	      } finally{
	    	  try {
	     	  if (pstmt != null)
	              pstmt.close();
	     	  }
	    	  catch (SQLException ex) {
	              Rlog.fatal("EventdefDao","EventdefDao EXCEPTION in getMaxSeqForVisitEvents()"+ ex);

	              return 0;

	          }

	    	  try {
		      	  if (conn != null)
		            conn.close();
		    	  }
		    	  catch (SQLException ex) {
		              Rlog.fatal("EventdefDao","EventdefDao EXCEPTION in getMaxSeqForVisitEvents()"+ ex);

		              return 0;

		          }


	      }

	   }
	  
	  
	  public int getMaxSeqForVisitEvents(int visitId, String eventNam, String tableName){
		  PreparedStatement pstmt = null;
	      Connection conn = null;
	      String sql = "";
	      int ret = 0;
	      try {
	          conn = getConnection();

		          sql="select pkg_calendar.f_get_event_sequence(?,?,?) as max_seq from dual";
		          pstmt = conn.prepareStatement(sql);
		          pstmt.setInt(1, visitId);
		          pstmt.setString(2, eventNam);
		          pstmt.setString(3, tableName);
		     


	          ResultSet rs = pstmt.executeQuery();

	            while (rs.next()) {
	            	ret =   rs.getInt("max_seq");
	            }

	      	  if (pstmt != null)
	              pstmt.close();

	      	  if (conn != null)
	            conn.close();


	           return ret;

	      } catch (SQLException ex) {
	          Rlog.fatal("EventdefDao","EventdefDao EXCEPTION in getMaxSeqForVisitEvents()"+ ex);

	          return 0;

	      } finally{
	    	  try {
	     	  if (pstmt != null)
	              pstmt.close();
	     	  }
	    	  catch (SQLException ex) {
	              Rlog.fatal("EventdefDao","EventdefDao EXCEPTION in getMaxSeqForVisitEvents()"+ ex);

	              return 0;

	          }

	    	  try {
		      	  if (conn != null)
		            conn.close();
		    	  }
		    	  catch (SQLException ex) {
		              Rlog.fatal("EventdefDao","EventdefDao EXCEPTION in getMaxSeqForVisitEvents()"+ ex);

		              return 0;

		          }


	      }

	   }


	// ///////////////////////
	    /*
	     * Update Budget lineitem category of all events under a category, called when budget lineitem category is modified for an event category
	     */

	    public int updateBudgetLineitemCategoryForAllEvents(int category,String newBudgetLineItemCategory,String modifiedBy,String ipAdd) {

	        PreparedStatement pstmt = null;
	        Connection conn = null;
	        String sqlStr = "";
	        try {
	            conn = getConnection();

	            sqlStr = " Update event_def set event_line_category = ?,  last_modified_by = ?, ip_add = ? where chain_id = ? and event_type = 'E' ";


	            pstmt = conn.prepareStatement(sqlStr);

	            pstmt.setString(1, newBudgetLineItemCategory);
	            pstmt.setString(2, modifiedBy);
	            pstmt.setString(3, ipAdd);
	            pstmt.setInt(4, category);

	            pstmt.executeUpdate();
	            conn.commit();

	        } catch (Exception e) {
	            Rlog.fatal("Lineitem",
	                    "EXCEPTION IN EventdefDao:updateBudgetLineitemCategoryForAllEvents()" + e);
	            try {
	                conn.rollback();
	            } catch (Exception ex) {

	            }
	            return -1;

	        } finally {
	            try {
	                if (pstmt != null)
	                    pstmt.close();
	            } catch (Exception e) {
	            }

	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }

	        }
	        return 1;
	    }




	    /*
	     * Update Event Library Type of all events under a category, called when ELT is modified for an event category
	     */
	    //KM- #4550
	    public int updateEventLibTypeForAllEvents(int category,String newEvtLibType,String modifiedBy,String ipAdd) {

	        PreparedStatement pstmt = null;
	        Connection conn = null;
	        String sqlStr = "";
	        try {
	            conn = getConnection();

	            sqlStr = " Update event_def set event_library_type = ?,  last_modified_by = ?, ip_add = ? where chain_id = ? and event_type = 'E' ";


	            pstmt = conn.prepareStatement(sqlStr);

	            pstmt.setString(1, newEvtLibType);
	            pstmt.setString(2, modifiedBy);
	            pstmt.setString(3, ipAdd);
	            pstmt.setInt(4, category);

	            pstmt.executeUpdate();
	            conn.commit();

	        } catch (Exception e) {
	            Rlog.fatal("Lineitem",
	                    "EXCEPTION IN EventdefDao:updateEventLibTypeForAllEvents()" + e);
	            try {
	                conn.rollback();
	            } catch (Exception ex) {

	            }
	            return -1;

	        } finally {
	            try {
	                if (pstmt != null)
	                    pstmt.close();
	            } catch (Exception e) {
	            }

	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }

	        }
	        return 1;
	    }


	    /**
	     * This method shall update the fk_patprot column of ER_PATTXARM table with the current protocol id/the current patient schedule
	     * @param StudyId
	     */


	    public int updatePatTxtArmsForTheNewPatProt(int patProtIdOld, int patProtId) {
	    	PreparedStatement pstmt = null;
	        Connection conn = null;
	        String sqlStr = "";
	        try {

	            conn = getConnection();

	            sqlStr = "Update ER_PATTXARM set FK_PATPROT=? where FK_PATPROT = ?";


	            pstmt = conn.prepareStatement(sqlStr);
	            pstmt.setInt(1, patProtId);
	            pstmt.setInt(2, patProtIdOld);

	            pstmt.executeUpdate();
	            conn.commit();

	        }

	        catch (SQLException ex) {
	            Rlog.fatal("eventdef",
	                    "In updatePatTxtArmsForTheNewPatProt in Eventdef line EXCEPTION IN dao"
	                            + ex);

	            return -1;
	        } finally {
	            try {
	                if (pstmt != null)
	                	pstmt.close();
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }
	        }

	        return 1;


	    }
	    
	    public int createEventsDef(String[] eIdList, String[] vIdList, String[] seqList,
	  		  String[] dispList, String[] hideList,
	  		  int userId, String ipAdd) {
	        int ret = -1;
	        CallableStatement cstmt = null;
	        Connection conn = null;
	        try {
	            conn = getConnection();
	            ArrayDescriptor descEIdList = ArrayDescriptor.createDescriptor(
	                    "ARRAY_STRING", conn);
	            ARRAY eIdArray = new ARRAY(descEIdList, conn, eIdList);
	            ArrayDescriptor descVIdList = ArrayDescriptor.createDescriptor(
	                    "ARRAY_STRING", conn);
	            ARRAY vIdArray = new ARRAY(descVIdList, conn, vIdList);
	            ArrayDescriptor descSeqList = ArrayDescriptor.createDescriptor(
	                    "ARRAY_STRING", conn);
	            ARRAY seqArray = new ARRAY(descSeqList, conn, seqList);
	            ArrayDescriptor descDispList = ArrayDescriptor.createDescriptor(
	                    "ARRAY_STRING", conn);
	            ARRAY dispArray = new ARRAY(descDispList, conn, dispList);
	            ArrayDescriptor descHideList = ArrayDescriptor.createDescriptor(
	                    "ARRAY_STRING", conn);
	            ARRAY hideArray = new ARRAY(descHideList, conn, hideList);
	            cstmt = conn.prepareCall("{call SP_CREATE_EVTS_DEF(?,?,?,?,?,?,?,?,?)}");
	            cstmt.setArray(1, eIdArray);
	            cstmt.setArray(2, vIdArray);
	            cstmt.setArray(3, seqArray);
	            cstmt.setArray(4, dispArray);
	            cstmt.setArray(5, hideArray);
	            cstmt.registerOutParameter(6, java.sql.Types.INTEGER);
	            cstmt.setInt(7, userId);
	            cstmt.setString(8, ipAdd);
	            cstmt.setInt(9, 1);
	            cstmt.execute();
	            ret = cstmt.getInt(6);
	        } catch (Exception e) {
	      	  ret = -1;
	      	  Rlog.fatal("eventdef", "Exception in createEventsDef "+e);
	        } finally {
	      	  if (cstmt != null) { try { cstmt.close(); } catch(Exception e) {} }
	      	  if (conn != null) { try { conn.close(); } catch(Exception e) {} }
	        }
	        return ret;
	    }
	    

	    /**
	     * This method retrieves all the scheduled events of a calendar with
	     * EVENT DETAILS AND CPT CODE, EVENT LIBRARY TYPE
	     *
	     * @param calendarId
	     */

	    public void getAllProtSelectedVisitsEvents(int protocolId, String visitId, String evtName) {

	        PreparedStatement pstmt = null;
	        Connection conn = null;
	        try {
	            Integer iValue = new Integer(0);
	            String sValue = "";

	            conn = getConnection();

	            StringBuffer sbSql = new StringBuffer();

	            sbSql.append(" SELECT decode(NUM_DAYS,0,NUM_DAYS,DISPLACEMENT) as DISPLACEMENT, "+" 0 as EVENT_ID, " + "0 as ORG_ID, "
	                + " 0 as CHAIN_ID, " + " '' as EVENT_TYPE, " + " visit_name  as NAME, " 
	                + " 0 as USER_ID, " +" ''  as STATUS, " 
	                + " DESCRIPTION  as DESCRIPTION, "
	                + " pk_protocol_visit as FK_VISIT , '' as EVENT_CATEGORY, 0 as EVENT_SEQUENCE, "
	                + " 0 as EVENT_LIBRARY_TYPE, "
	                + " '' as EVENT_CPTCODE "
	                + " FROM sch_protocol_visit "
	                + " WHERE  fk_protocol= ? ");

	            	if (!visitId.equals("")){
	            		sbSql.append(" and pk_protocol_visit="+visitId);
	            	}
	            	//Modified event name for the bug id 23909
	            	sbSql.append(" union"
		            + " SELECT DECODE((SELECT NUM_DAYS FROM SCH_PROTOCOL_VISIT v WHERE v.pk_protocol_visit= a.FK_VISIT),0,0,DISPLACEMENT) AS DISPLACEMENT, "
		            + " EVENT_ID, " + "ORG_ID, "
		            + " CHAIN_ID, " + "EVENT_TYPE, " + " replace(replace(NAME,CHR(13),''),CHR(10),'') NAME, " 
		            + " USER_ID, " +"STATUS, " + "DESCRIPTION, "
		            + " FK_VISIT , EVENT_CATEGORY, EVENT_SEQUENCE, "
		            + " EVENT_LIBRARY_TYPE, "
		            + " EVENT_CPTCODE "
		            + " FROM EVENT_DEF a "
		            + " WHERE  a.CHAIN_ID= ? AND a.FK_VISIT IS NOT NULL ");//BK-DAY 0


	            	if (!visitId.equals("")){
	            		sbSql.append(" and fk_visit="+visitId);
	            	}

	            	if (!evtName.equals("")){
	            		sbSql.append(" and lower(name) like lower(lower('%"+evtName+"%'))");
	            	}


	            	sbSql.append(" ORDER BY DISPLACEMENT,fk_visit, EVENT_SEQUENCE");
	  
	            	String mysql =sbSql.toString();

	            pstmt = conn.prepareStatement(mysql);
	            pstmt.setInt(1, protocolId);
	            pstmt.setInt(2, protocolId);

	            ResultSet rs = pstmt.executeQuery();

	            while (rs.next()) {
	                setEvent_ids(new Integer(rs.getString("EVENT_ID")));
	                setOrg_ids(new Integer(rs.getString("ORG_ID")));
	                setChain_ids(rs.getString("CHAIN_ID"));
	                setEvent_types(rs.getString("EVENT_TYPE"));
	                setNames(rs.getString("NAME"));
	                setUser_ids(rs.getString("USER_ID"));
	                setStatus(rs.getString("STATUS"));
	                setDescription(rs.getString("DESCRIPTION"));
	                setDisplacement(rs.getString("DISPLACEMENT"));                
	                sValue = rs.getString("FK_VISIT");
	                setEventCategory(rs.getString("EVENT_CATEGORY"));
	                setEventSequences(rs.getString("EVENT_SEQUENCE"));
	                setEventLibraryType(rs.getString("EVENT_LIBRARY_TYPE"));
	                setCptCodes(rs.getString("EVENT_CPTCODE"));
	                if (sValue == null)
	                    iValue = new Integer(0);
	                else
	                    iValue = new Integer(sValue.trim());
	                setFk_visit(iValue);

	            }

	        } catch (SQLException ex) {
	            Rlog
	                    .fatal(
	                            "eventdef",
	                            "EventdefDao.getAllProtSelectedVisitsEvents EXCEPTION IN FETCHING FROM Eventdef table"
	                                    + ex);
	        } finally {
	            try {
	                if (pstmt != null)
	                    pstmt.close();
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }

	        }

	    }
	    //YK: Added for PCAL-19743
	    public void getProtSelectedAndVisitEvents(int protocolId) {
	        PreparedStatement pstmt = null;
	        Connection conn = null;
	        try {
	            conn = getConnection();
	            StringBuffer sql = new StringBuffer();
	            sql
	            .append(" SELECT   0 AS EVENT_ID,0 AS NEW_EVENT_ID, 0 AS ORG_ID, fk_protocol AS CHAIN_ID, ")
	            .append(" '' AS EVENT_TYPE, visit_name AS NAME, 0 AS COST, DESCRIPTION AS DESCRIPTION, ")
	            .append(" 'V' AS flag, pk_protocol_visit AS FK_VISIT, VISIT_NO, DISPLACEMENT ")
	            .append(" FROM sch_protocol_visit WHERE fk_protocol = ? ")
	            .append(" UNION ")
	            .append(" SELECT ")
	            .append(" (SELECT EVENT_ID FROM EVENT_DEF WHERE CHAIN_ID = ? ")
	            .append("  AND COST = a.COST AND FK_VISIT is null AND EVENT_TYPE = 'A'), EVENT_ID AS NEW_EVENT_ID,")
	            .append(" ORG_ID, CHAIN_ID, EVENT_TYPE, replace(replace(NAME,CHR(13),''),CHR(10),'') NAME, COST, DESCRIPTION, ")
	            .append(" 'E' AS flag, FK_VISIT, 0 AS VISIT_NO, 0 AS DISPLACEMENT ")
	            .append(" FROM EVENT_DEF a ")
	            .append(" WHERE a.CHAIN_ID = ? ")
	            .append(" AND a.event_type = 'A' ")
	            .append(" ORDER BY flag DESC, DISPLACEMENT")//YK : Modified for Bug #7513 
	            ;
	            pstmt = conn.prepareStatement(sql.toString());
	            pstmt.setInt(1, protocolId);
	            pstmt.setInt(2, protocolId);
	            pstmt.setInt(3, protocolId);
	            ResultSet rs = pstmt.executeQuery();
	            while (rs.next()) {
	                setEvent_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("EVENT_ID")).trim()));
	                setNew_event_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("NEW_EVENT_ID")).trim()));
	                setOrg_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("ORG_ID")).trim()));
	                setChain_ids(rs.getString("CHAIN_ID"));
	                setEvent_types(rs.getString("EVENT_TYPE"));
	                setNames(rs.getString("NAME"));
	                setCosts(rs.getString("COST"));
	                setDescription(rs.getString("DESCRIPTION"));
	                setFlag(rs.getString("flag"));
	                String sValue = rs.getString("FK_VISIT");
	                if (sValue == null) { sValue = "0"; }
	                setFk_visit(new Integer (sValue.trim()));
	                setDisplacement(rs.getString("DISPLACEMENT"));
	            }
	        } catch (Exception ex) {
	            Rlog.fatal("eventassoc",
	                       "EventdefDao.getProtSelectedAndVisitEvents: "+ ex);
	        } finally {
	            try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
	            try { if (conn != null) conn.close(); } catch (Exception e) {}
	        }
	    }
	    
	    
	   public void getProtSelectedAndVisitEvents(int protocolId,int initEventSize,int initVisitSize,int limitEventSize,int limitVisitSize,String visitids,String eventName ) { 
	   
	   PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs=null;
        try {
            conn = getConnection();
            StringBuffer sql = new StringBuffer();
            StringBuffer sql1 = new StringBuffer();
            StringBuffer mainSql = new StringBuffer();
            StringBuffer sql2 = new StringBuffer();
            pstmt = null;
            rs = null;
            mainSql.append("SELECT count(*)")
            .append(" FROM EVENT_DEF a ")
            .append(" WHERE a.CHAIN_ID = ? ")
            .append(" AND a.event_type = 'A' ")
            .append(" AND a.fk_visit is null");
            if(!"".equalsIgnoreCase(eventName))
            	mainSql.append(" and (LOWER(NAME) LIKE LOWER('%"+eventName.replace("'" , "''")+"%')) ");
            pstmt = conn.prepareStatement(mainSql.toString());
            pstmt.setInt(1, protocolId);
            rs = pstmt.executeQuery();
            while (rs.next()){
            	totalRecords=rs.getInt("count(*)");
            }
            pstmt = null;
            rs = null;
            ArrayList eventCosts=new ArrayList();
            sql1.append("SELECT * from (SELECT COST ");
            sql1.append(" FROM EVENT_DEF a ")
            .append(" WHERE a.CHAIN_ID = ? ")
            .append(" AND a.event_type = 'A' ");
            if(!(eventName).equalsIgnoreCase(""))
            	sql1.append(" and (LOWER(NAME) LIKE LOWER('%"+eventName.replace("'" , "''")+"%')) ");
            sql1.append(" AND a.fk_visit is null ORDER BY Cost) ");
            
            
            
            sql1.append(" where ROWNUM <= ? ");
            pstmt = conn.prepareStatement(sql1.toString(),ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            pstmt.setInt(1, protocolId);
            pstmt.setInt(2,limitEventSize );
            pstmt.setFetchSize(1000);
            rs = pstmt.executeQuery();
            rs.absolute(initEventSize);
            do {
            	eventCosts.add(Integer.parseInt(StringUtil.trueValue(rs.getString("COST")).trim()));
            }while (rs.next());
            String costs = eventCosts.toString().replace("[", "").replace("]", "")
            .replace(", ", ",");
            pstmt = null;
            rs = null;
            sql2.append(" SELECT b.EVENT_ID,lower(b.name) as event_name, a.EVENT_ID AS NEW_EVENT_ID, ")
            .append(" a.ORG_ID, a.CHAIN_ID, a.EVENT_TYPE, replace(replace(a.NAME,CHR(13),''),CHR(10),'') NAME, a.COST, a.DESCRIPTION, ")
            .append(" 'E' AS flag, a.FK_VISIT, 0 AS VISIT_NO, 0 AS DISPLACEMENT ");
            sql2.append(" FROM EVENT_DEF a left join EVENT_DEF b on a.CHAIN_ID =b.chain_id ")
            .append(" AND b.COST = a.COST AND b.FK_VISIT is null AND b.EVENT_TYPE  = a.EVENT_TYPE ")
            .append(" WHERE a.CHAIN_ID = ? AND a.event_type = 'A' and (a.fk_visit in("+visitids+") or a.fk_visit is null) ");
           sql2.append(" AND a.COST in ("+costs+") ORDER BY Cost,fk_visit nulls first");
           
            pstmt = conn.prepareStatement(sql2.toString());
            pstmt.setInt(1, protocolId);
            pstmt.setFetchSize(1000);
            rs = pstmt.executeQuery();
            
            while (rs.next()) {
                setEvent_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("EVENT_ID")).trim()));
                setNew_event_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("NEW_EVENT_ID")).trim()));
                setOrg_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("ORG_ID")).trim()));
                setChain_ids(rs.getString("CHAIN_ID"));
                setEvent_types(rs.getString("EVENT_TYPE"));
                setNames(rs.getString("NAME"));
                setCosts(rs.getString("COST"));
                setDescription(rs.getString("DESCRIPTION"));
                setFlag(rs.getString("flag"));
                String sValue = rs.getString("FK_VISIT");
                if (sValue == null) { sValue = "0"; }
                setFk_visit(new Integer (sValue.trim()));
                setDisplacement(rs.getString("DISPLACEMENT"));
            }
        } catch (Exception ex) {
            Rlog.fatal("eventdef",
                       "EventDefDao.getProtSelectedAndVisitEvents: "+ ex);
        } finally {
            try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
            try { if (conn != null) conn.close(); } catch (Exception e) {}
        }}
	 
	    
	    
	    
	    public String getEventMouseOver(int eventId, String eventType) {
	    	StringBuffer mouseOver = new StringBuffer();
	    	
	    	PreparedStatement pstmt = null;
	        Connection conn = null;
	        try {
	            conn = getConnection();
	            StringBuffer eventDefSql = new StringBuffer();
	            eventType = (null == eventType)? "E": eventType;

	            eventDefSql.append("SELECT distinct EVENT_ID, ")
	            	.append("chain_id category_id,EVENT_TYPE, NAME, ")
	            	.append("EVENT_LIBRARY_TYPE, (select codelst_desc from sch_codelst where codelst_type = 'lib_type' and pk_codelst = EVENT_LIBRARY_TYPE) as LIBRARY_TYPE_DESC, ")
	            	.append("event_cptcode, case when length(notes) > 100 then substr(notes,1,100) || '...' else notes end as notes, ")
	            	.append("lst_cost(event_id) as eventcost, ");
	            
	            eventDefSql.append("esch.lst_additionalcodes_financial(event_id,'evtaddlcode',1) as additionalcode ");
	            eventDefSql
	            	.append("FROM EVENT_DEF b ")
	            	.append("WHERE EVENT_ID = ? ")
	            	.append("AND TRIM(UPPER(EVENT_TYPE)) = ?");

	            pstmt = conn.prepareStatement(eventDefSql.toString());
	            pstmt.setInt(1, eventId);
	            pstmt.setString(2, eventType);
	            
	            ResultSet rs = pstmt.executeQuery();
	            while (rs.next()) {
	            	String eventName = rs.getString("NAME");
	            	eventName = (null == eventName)? "" : eventName;
	            	mouseOver.append("<b>"+LC.L_Event_Name+"</b>: "+((eventName.length() >25) ? eventName.substring(0,25)+"...":eventName));
	            	
	            	String libraryName = rs.getString("LIBRARY_TYPE_DESC");
	            	libraryName = (null == libraryName)? "" : libraryName;
	            	mouseOver.append("<br><b>"+LC.L_Evt_Lib+"</b>: "+((libraryName.length()>25) ? libraryName.substring(0,25)+"...":libraryName));

	            	String cptCode = rs.getString("EVENT_CPTCODE");
	              	cptCode = (null == cptCode)? "" : cptCode;
	              	mouseOver.append("<br><b>"+LC.L_Cpt_Code+"</b>: "+cptCode);
	              	
	              	String addCode = rs.getString("ADDITIONALCODE");
	              	addCode = (null == addCode)? "" : addCode;
	              	mouseOver.append("<br><b>"+LC.L_Addl_Codes+"</b>:<br>"+addCode);
	            	break;
	            }
	        } catch (Exception ex) {
	            Rlog.fatal("eventdef",
	                       "EventDefDao.getEventMouseOver: "+ ex);
	        } finally {
	            try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
	            try { if (conn != null) conn.close(); } catch (Exception e) {}
	        }
	        try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
	        try { if (conn != null) conn.close(); } catch (Exception e) {}
	          
			return mouseOver.toString();
	    }
	    
   public void getProtSelectedAndVisitEvents(int protocolId ,String eventName) {
	        
	        PreparedStatement pstmt = null;
	        Connection conn = null;
	        try {
	        	conn = getConnection();
	            StringBuffer sql = new StringBuffer();
	            StringBuffer sql1 = new StringBuffer();
	            String costs = "";
	            
	            ArrayList eventCosts=new ArrayList();
	            ResultSet rs = null;
				if(!"".equalsIgnoreCase(eventName)){
				sql1.append("SELECT COST ")
	            .append(" FROM EVENT_DEF a ")
	            .append(" WHERE a.CHAIN_ID = ? ")
	            .append(" AND a.event_type = 'A' ")
	            .append(" and (LOWER(NAME) LIKE LOWER('%"+eventName+"%')) ")
	            .append(" AND a.fk_visit is null ");
	            
	            pstmt = conn.prepareStatement(sql1.toString());
	            pstmt.setInt(1, protocolId);
	            pstmt.setFetchSize(1000);
	            rs = pstmt.executeQuery();
	            while (rs.next()) {
	            	eventCosts.add(Integer.parseInt(StringUtil.trueValue(rs.getString("COST")).trim()));
	            }
	            costs= eventCosts.toString().replace("[", "").replace("]", "")
	            .replace(", ", ",");
				 }//end of if
				pstmt = null;
				rs = null;
				if(!"".equalsIgnoreCase(eventName) && !"".equalsIgnoreCase(costs)){
	            sql.append(" SELECT   0 AS EVENT_ID, lower(visit_name) AS event_name,0 AS NEW_EVENT_ID, 0 AS ORG_ID, fk_protocol AS CHAIN_ID, ")
	            .append(" '' AS EVENT_TYPE, visit_name AS NAME, 0 AS COST, DESCRIPTION AS DESCRIPTION,'' AS EVENT_CATEGORY, ")
	            .append(" 'V' AS flag, pk_protocol_visit AS FK_VISIT, VISIT_NO, DISPLACEMENT ")
	            .append(" FROM sch_protocol_visit WHERE fk_protocol = ? ");
	          
	            
	            sql.append(" UNION ")
	            .append(" SELECT b.EVENT_ID, lower(b.name) AS event_name, a.EVENT_ID AS NEW_EVENT_ID,")
	            .append(" a.ORG_ID, a.CHAIN_ID, a.EVENT_TYPE, replace(replace(a.NAME,CHR(13),''),CHR(10),'') NAME, a.COST, a.DESCRIPTION,a.EVENT_CATEGORY, ")
	            .append(" 'E' AS flag, a.FK_VISIT, 0 AS VISIT_NO, 0 AS DISPLACEMENT ")
	            .append(" FROM EVENT_DEF a left join EVENT_DEF b on a.CHAIN_ID = b.CHAIN_ID and  a.COST = b.COST and b.FK_VISIT  IS NULL AND b.EVENT_TYPE = a.EVENT_TYPE ")
	            .append(" WHERE a.CHAIN_ID = ? ")
	            .append(" AND a.event_type = 'A' ");
	            
	            
	            	sql.append(" AND a.COST in ("+costs.toString()+")");
	            
	            sql.append(" ORDER BY flag DESC, DISPLACEMENT,Cost,fk_visit nulls first ")//YK : Modified for Bug #7513 
	            ;
	            pstmt = conn.prepareStatement(sql.toString());
	            pstmt.setInt(1, protocolId);
	            pstmt.setInt(2, protocolId);	
	            pstmt.setFetchSize(1000);
	            rs = pstmt.executeQuery();
	            //Event-Visit Grid Tab
	            //Modified event name for the bug id 23909
				}else if("".equalsIgnoreCase(eventName) && "".equalsIgnoreCase(costs)){
					sql.append(" SELECT 0 AS EVENT_ID, lower(visit_name) AS event_name, 0 AS NEW_EVENT_ID, 0 AS ORG_ID, fk_protocol AS CHAIN_ID, ")
		            .append(" '' AS EVENT_TYPE, visit_name AS NAME, 0 AS COST, DESCRIPTION AS DESCRIPTION, '' AS EVENT_CATEGORY, ")
		            .append(" 'V' AS flag, pk_protocol_visit AS FK_VISIT, VISIT_NO, DISPLACEMENT ")
		            .append(" FROM sch_protocol_visit WHERE fk_protocol = ? ")
		            .append(" UNION ")
		            .append(" SELECT ")
		            .append(" b.EVENT_ID,lower(b.name) as event_name, a.EVENT_ID AS NEW_EVENT_ID, ")
		            .append(" a.ORG_ID, a.CHAIN_ID, a.EVENT_TYPE, replace(replace(a.NAME,CHR(13),''),CHR(10),'') NAME, a.COST, a.DESCRIPTION,a.EVENT_CATEGORY, ")
		            .append(" 'E' AS flag, a.FK_VISIT, 0 AS VISIT_NO, 0 AS DISPLACEMENT ")
		            .append(" FROM EVENT_DEF a left join EVENT_DEF b on a.CHAIN_ID =b.chain_id ")
		            .append(" AND b.COST = a.COST AND b.FK_VISIT is null AND b.EVENT_TYPE  = a.EVENT_TYPE ")
		            .append(" WHERE a.CHAIN_ID = ? AND a.event_type = 'A' ")
		            .append(" ORDER BY flag DESC,DISPLACEMENT,Cost,fk_visit nulls first ");
			            pstmt = conn.prepareStatement(sql.toString());
			            pstmt.setInt(1, protocolId);
			            pstmt.setInt(2, protocolId);	
			            pstmt.setFetchSize(1000);
			            rs = pstmt.executeQuery();
				}
				if(rs!=null){
	            while (rs.next()) {
	                setEvent_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("EVENT_ID")).trim()));
	                setNew_event_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("NEW_EVENT_ID")).trim()));
	                setOrg_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("ORG_ID")).trim()));
	                setChain_ids(rs.getString("CHAIN_ID"));
	                setEvent_types(rs.getString("EVENT_TYPE"));
	                setNames(rs.getString("NAME"));
	                setCosts(rs.getString("COST"));
	                setDescription(rs.getString("DESCRIPTION"));
	                setFlag(rs.getString("flag"));
	                String sValue = rs.getString("FK_VISIT");
	                if (sValue == null) { sValue = "0"; }
	                setFk_visit(new Integer (sValue.trim()));
	                setDisplacement(rs.getString("DISPLACEMENT"));
	                setEventCategory(rs.getString("EVENT_CATEGORY"));
	            }}
	        } catch (Exception ex) {
	            Rlog.fatal("eventassoc",
	                       "EventdefDao.getProtSelectedAndVisitEvents: "+ ex);
	        } finally {
	            try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
	            try { if (conn != null) conn.close(); } catch (Exception e) {}
	        }
	    }
	    public String getEventMouseOver(int eventId, String eventType, int finDetRight) {
	    	StringBuffer mouseOver = new StringBuffer();
	    	String accessYN ="0";

	    	PreparedStatement pstmt = null;
	        Connection conn = null;
	        try {
	            conn = getConnection();
	            StringBuffer eventDefSql = new StringBuffer();
	            eventType = (null == eventType)? "E": eventType;

	            eventDefSql.append("SELECT distinct EVENT_ID, ")
	            	.append("chain_id category_id,EVENT_TYPE, NAME, ")
	            	.append("EVENT_LIBRARY_TYPE, (select codelst_desc from sch_codelst where codelst_type = 'lib_type' and pk_codelst = EVENT_LIBRARY_TYPE) as LIBRARY_TYPE_DESC, ")
	            	.append("event_cptcode, case when length(notes) > 100 then substr(notes,1,100) || '...' else notes end as notes, ")
	            	.append("lst_cost(event_id) as eventcost, ");
	            
	            if ("E".equals(eventType)){
	          	  accessYN ="1";
	            }else{
	            	if (StringUtil.isAccessibleFor(finDetRight, 'V')) {
	            		accessYN ="1";
	            	}
	            }
	            eventDefSql.append("esch.lst_additionalcodes_financial(event_id,'evtaddlcode',"+accessYN+") as additionalcode ");
	            eventDefSql
	            	.append("FROM EVENT_DEF b ")
	            	.append("WHERE EVENT_ID = ? ")
	            	.append("AND TRIM(UPPER(EVENT_TYPE)) = ?");

	            pstmt = conn.prepareStatement(eventDefSql.toString());
	            pstmt.setInt(1, eventId);
	            pstmt.setString(2, eventType);
	            
	            ResultSet rs = pstmt.executeQuery();
	            while (rs.next()) {
	            	String eventName = rs.getString("NAME");
	            	eventName = (null == eventName)? "" : eventName;
	            	mouseOver.append("<b>"+LC.L_Event_Name+"</b>: "+eventName);
	            	String libraryName = rs.getString("LIBRARY_TYPE_DESC");
	            	libraryName = (null == libraryName)? "" : libraryName;
	            	mouseOver.append("<br><b>"+LC.L_Evt_Lib+"</b>: "+((libraryName.length()>25) ? libraryName.substring(0,25)+"...":libraryName));

	            	if (!"0".equals(accessYN)){
		            	String cptCode = rs.getString("EVENT_CPTCODE");
		              	cptCode = (null == cptCode)? "" : cptCode;
		              	mouseOver.append("<br><b>"+LC.L_Cpt_Code+"</b>: "+cptCode);
	            	}
	              	String addCode = rs.getString("ADDITIONALCODE");
	              	addCode = (null == addCode)? "" : addCode;
	              	mouseOver.append("<br><b>"+LC.L_Addl_Codes+"</b>:<br>"+addCode);
	            	break;
	            }
	        } catch (Exception ex) {
	            Rlog.fatal("eventdef",
	                       "EventDefDao.getEventMouseOver: "+ ex);
	        } finally {
	            try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
	            try { if (conn != null) conn.close(); } catch (Exception e) {}
	        }
	        try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
	        try { if (conn != null) conn.close(); } catch (Exception e) {}
	          
			return mouseOver.toString();
	    }
	    public int  saveCoverageTypeByVisit(int visitId, int coverageTypeId, String eventName){
	    	
	    	int rowImpact=0;
	    	int vId = visitId;
	    	int covId =  coverageTypeId;
	    	PreparedStatement pstmt = null;
	        Connection conn = null;
	        String sqlVisit="";
	        try{
	        	   conn = getConnection();
	        	   sqlVisit="update EVENT_DEF set FK_CODELST_COVERTYPE="+covId+ " where FK_VISIT="+vId+" AND (LOWER(NAME) LIKE LOWER('%"+eventName+"%'))";  
	        	   pstmt = conn.prepareStatement(sqlVisit);
		           pstmt.executeUpdate();
		           conn.commit();
	        }catch(SQLException e){
	        	e.printStackTrace();
	        	rowImpact=-1;
	        }
	        finally {
	            try {
	                if (pstmt != null)
	                	pstmt.close();
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }
	        }
	    	return rowImpact;
	    	
	    }
	    

	    
	    public int saveCoverageTypeByEventToLib(int chainId,int eventId,int coverageTypeId){
	    	int rowImpact=0;
	    	int cId = chainId;
	    	int covId =  coverageTypeId;
	    	int eID = eventId;
	    	PreparedStatement pstmt = null;
	        Connection conn = null;
	        String sqlEvent="";
	        
	        try{
	        	   conn = getConnection();
	        	   sqlEvent=" update event_def set FK_CODELST_COVERTYPE=?  where chain_id=? and cost =(select cost from event_def where event_id=?) and fk_visit is not null " ;
	        	   pstmt = conn.prepareStatement(sqlEvent);
		           pstmt.setInt(1, covId);
	               pstmt.setInt(2, cId);
	               pstmt.setInt(3, eID);
	               rowImpact=  pstmt.executeUpdate();
	               System.out.println("rowImpact-"+rowImpact);
		           conn.commit();
	        }catch(SQLException e){
	        	e.printStackTrace();
	        	
	        }
	        finally {
	            try {
	                if (pstmt != null)
	                	pstmt.close();
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }
	        }
	
	        
	    	return 0;
	    }
	    public int saveVisitToEvent(String chainId,String visitId,String dispacement){
	    	System.out.println("in");
	    	int rowImpact=0;
	    	int protocolId=StringUtil.stringToNum(chainId);
	    	int vId=StringUtil.stringToNum(visitId);
	    	int disPlacement=StringUtil.stringToNum(dispacement);
	    	PreparedStatement pstmt = null;
	        Connection conn = null;
	        String sqlEvent="";
	        try{
	        	   conn = getConnection();
	        	   //sqlEvent=" update event_def set FK_CODELST_COVERTYPE=?  where chain_id=? and name =(select name from event_def where event_id=?) and fk_visit is not null " ;
	        	   pstmt = conn.prepareStatement(sqlEvent);
		           pstmt.setInt(1,1);
	               pstmt.setInt(2, 2);
	               pstmt.setInt(3,3);
	               rowImpact=  pstmt.executeUpdate();
	               System.out.println("rowImpact-"+rowImpact);
		           conn.commit();
		          // System.out.println("rowImpact-"+rowImpact);
	        }catch(SQLException e){
	        	e.printStackTrace();
	        	
	        }
	        finally {
	            try {
	                if (pstmt != null)
	                	pstmt.close();
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }
	        }
	    	return 0;
	    }
	    
	    public void  getEventListForSaveInEventVisitGrid(int protocolId ,int visitId,String operation){
	    	System.out.println("get EventList");
	    	PreparedStatement pstmt = null;
	        Connection conn = null;
	        ResultSet rs=null;
	        String sqlEvent="";
	        try{
	        	   conn = getConnection();
	        	   if("A".equals(operation)){
	        	   sqlEvent=" select event_id, name,event_category  from event_def where fk_visit is  null and event_type='A' and chain_id=? and cost not in(select  cost from  event_def where fk_visit=? ) " ;
	        	   }else{
	        		   sqlEvent=" select event_id, name,event_category  from event_def where fk_visit is  null and event_type='A' and chain_id=? and cost  in(select  cost from  event_def where fk_visit=? ) " ;  
	        	   }
	        	   pstmt = conn.prepareStatement(sqlEvent);
		           pstmt.setInt(1,protocolId);
	               pstmt.setInt(2, visitId);
	                
	                rs = pstmt.executeQuery();
	               System.out.println("rowImpact-");
	               while (rs.next()) {
	            	   setEvent_ids(Integer.parseInt(StringUtil.trueValue(rs.getString("EVENT_ID")).trim()));
	            	   setNames(rs.getString("NAME"));
	            	   setEventCategory(rs.getString("EVENT_CATEGORY"));
	               }
	               if(event_ids.size()!=0){
	            	   System.out.println("size of eventIds-"+ event_ids.size());
	               }
	               
		          // System.out.println("rowImpact-"+rowImpact);
	        }catch(SQLException e){
	        	e.printStackTrace();
	        	
	        }
	        finally {
	            try {
	                if (pstmt != null)
	                	pstmt.close();
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }
	        }
	    	
	    }
	    
	    
	public void getVisitListForSaveInEventVisitGrid(int protocolId ,int eventId,String operation){
		PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs=null;
        String sqlEvent="";
        try{
     	   conn = getConnection();
     	   if("A".equals(operation)){
     	   sqlEvent=" select pk_protocol_visit,displacement,hide_flag from sch_protocol_visit where fk_protocol=? and pk_protocol_visit not in (select fk_visit from event_def where chain_id=? and cost=(select cost from event_def where event_id=? and fk_visit is not null)) " ;
     	   }else{
     		   sqlEvent=" select event_id, name  from event_def where fk_visit is  null and event_type='A' and chain_id=? and cost  in(select  cost from  event_def where fk_visit=? ) " ;  
     	   }
     	   pstmt = conn.prepareStatement(sqlEvent);
	           pstmt.setInt(1,protocolId);
            pstmt.setInt(2, protocolId);
            pstmt.setInt(3, eventId);
             
             rs = pstmt.executeQuery();
            System.out.println("rowImpact-");
            while (rs.next()) {
            	setFk_visit(Integer.parseInt(StringUtil.trueValue(rs.getString("pk_protocol_visit")).trim()));
         	  
            }
            if(visit.size()!=0){
         	   System.out.println("size of eventIds-"+ visit.size());
            }
            
	          // System.out.println("rowImpact-"+rowImpact);
     }catch(SQLException e){
     	e.printStackTrace();
     	
     }
     finally {
         try {
             if (pstmt != null)
             	pstmt.close();
         } catch (Exception e) {
         }
         try {
             if (conn != null)
                 conn.close();
         } catch (Exception e) {
         }
     }
	}
	
	  public ArrayList findletst(String[] pkey) {
	    	 PreparedStatement pstmt = null;
	         Connection conn = null;
       ArrayList rowValues = new ArrayList();
	         String sql="";
	         ResultSet rs=null;
	         try{
	        
	        	 if (pkey.length < 0){
	        		   return rowValues;
	        	 }else{
	        		
		        	 conn = getConnection();
		        	 String parameters = StringUtils.join(Arrays.asList(pkey).iterator(),","); 
		        	 pstmt =conn.prepareStatement("SELECT e.FK_EVENT,"+
			            " MAX(e.PK_EVENTSTAT) KEEP(DENSE_RANK FIRST ORDER BY e.PK_EVENTSTAT DESC) PK_EVENTSTAT "+
			            " FROM sch_eventstat e where  e.FK_EVENT in ("+parameters+") GROUP BY e.FK_EVENT ORDER BY 1 DESC"
	                );  
		        rs = pstmt.executeQuery();
			            
		        while (rs.next()){
	                 rowValues.add(new Integer(rs.getInt("PK_EVENTSTAT")));

		        }
	             System.out.println(rowValues);
 
	             return rowValues;
		             
	        	 }

	         }catch (SQLException ex) {
	             Rlog.fatal("","eventDefDao.findletst EXCEPTION IN READING table "+ ex);
	             return rowValues;
	         } finally {
	             try {
	                 if (pstmt != null)
	                     pstmt.close();
	             } catch (Exception e) {
	             }
	             try {
	                 if (conn != null)
	                     conn.close();
	             } catch (Exception e) {
	             }
	         }
	    }
	  
	  public ArrayList findSecondLetst(String[] pkey) {
	    	 PreparedStatement pstmt = null;
	         Connection conn = null;
    ArrayList rowValues = new ArrayList();
	         String sql="";
	         ResultSet rs=null;
	         try{
	        
	        	 if (pkey.length < 0){
	        		   return rowValues;
	        	 }else{
	        		
		        	 conn = getConnection();
		        	 String parameters = StringUtils.join(Arrays.asList(pkey).iterator(),","); 
		        	 pstmt =conn.prepareStatement(" SELECT FK_EVENT,MAX(PK_EVENTSTAT) PK_EVENTSTAT FROM sch_eventstat e where "+
			            " e.FK_EVENT in ("+parameters+") and PK_EVENTSTAT <> (select MAX(PK_EVENTSTAT) FROM sch_eventstat where FK_EVENT = e.fk_event) "+
	                " GROUP BY FK_EVENT ORDER BY 1 DESC ");  
		        rs = pstmt.executeQuery();
			            
		        while (rs.next()){
	                 rowValues.add(new Integer(rs.getInt("PK_EVENTSTAT")));

		        }
	             System.out.println(rowValues);

	             return rowValues;
		             
	        	 }

	         }catch (SQLException ex) {
	             Rlog.fatal("","eventDefDao.findletst EXCEPTION IN READING table "+ ex);
	             return rowValues;
	         } finally {
	             try {
	                 if (pstmt != null)
	                     pstmt.close();
	             } catch (Exception e) {
	             }
	             try {
	                 if (conn != null)
	                     conn.close();
	             } catch (Exception e) {
	             }
	         }
	    }
	public String getEventAdditionalCodeAndCostMouseOver(int eventId){
		StringBuffer mouseOver = new StringBuffer();    	
    	PreparedStatement pstmt = null;
        Connection conn = null;
        try {
        conn = getConnection();
        StringBuffer eventDefSql = new StringBuffer();
        eventDefSql.append("select  name, lst_cost(event_id) AS eventcost,lst_additionalcode(event_id,'evtaddlcode') AS additionalcode from event_def where event_id =?");
        pstmt = conn.prepareStatement(eventDefSql.toString());
        pstmt.setInt(1, eventId);
        ResultSet rs = pstmt.executeQuery();      
        while (rs.next()) {
        	String eventName=rs.getString("NAME");eventName = (null == eventName)? "" : eventName;
        	mouseOver.append("<b>"+LC.L_Event_Name+"</b>: "+((eventName.length() >25) ? eventName.substring(0,25)+"...":eventName));
        	String eventCost=rs.getString("EVENTCOST");
        	eventCost = (null == eventCost)? "" : eventCost;
        	mouseOver.append("<br><b>"+LC.L_Cost+"</b>: "+eventCost);
        	String additionalCost=rs.getString("ADDITIONALCODE");
        	additionalCost = (null == additionalCost)? "" : additionalCost;
        	mouseOver.append("<br><b>"+LC.L_Addl_Codes+"</b>: "+additionalCost);
        }
        }catch (Exception ex) {
            Rlog.fatal("eventdef",
                    "EventDefDao.getEventMouseOver: "+ ex);
     } finally {
         try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
         try { if (conn != null) conn.close(); } catch (Exception e) {}
     }
		return mouseOver.toString();
		
     }
	
	public int setEventCost(int orgId,String chainId,String newCost){    	
    	PreparedStatement pstmt = null;
        Connection conn = null;
        int ret=0;
        try {
        conn = getConnection();
        StringBuffer eventDefSql = new StringBuffer();
        eventDefSql.append("update event_def set cost = ? where chain_id = ? and org_id = ?");
        pstmt = conn.prepareStatement(eventDefSql.toString());
        pstmt.setString(1, newCost);
        pstmt.setString(2, chainId);
        pstmt.setInt(3, orgId);
        ret=  pstmt.executeUpdate();
        }catch (Exception ex) {
            Rlog.fatal("eventdef",
                    "EventDefDao.setEventCost: "+ ex);
     } finally {
         try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
         try { if (conn != null) conn.close(); } catch (Exception e) {}
     }
		return ret;
		
     }
	
	public int setEventsCost(String calId,String[] eventIds,String[] eventNewSeqs,String calledFrom,String userId,String ipAdd){    	
		CallableStatement cstmt = null;
        Connection conn = null;
        int ret=0;
        try {
        conn = getConnection();
        ArrayDescriptor evtIds = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
    	
		  ARRAY eventPKArray = new ARRAY(evtIds, conn, eventIds);
		  ARRAY eventNewSeqArray = new ARRAY(evtIds, conn, eventNewSeqs);
        cstmt = conn.prepareCall("{call SP_UPDATE_EVESEQ(?,?,?,?,?,?,?)}");
		  cstmt.setInt(1, StringUtil.stringToNum(calId));
		  cstmt.setArray(2, eventPKArray);
		  cstmt.setArray(3, eventNewSeqArray);
		  cstmt.setString(4, calledFrom);
		  cstmt.setString(5, ipAdd);
		  cstmt.setInt(6, StringUtil.stringToNum(userId));
		  cstmt.registerOutParameter(7, Types.INTEGER);

		  cstmt.execute();
		  ret = cstmt.getInt(7);
		  if(ret==-1)
			  throw new SQLException();
        }catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventDefDao.setEventsCost: "+ ex);
     } finally {
         try { if (cstmt != null) cstmt.close(); } catch (Exception e) {}
         try { if (conn != null) conn.close(); } catch (Exception e) {}
     }
		return ret;
		
     }
//end of class    
}
