/*
 * Classname : EventdocBean
 * 
 * Version information: 1.0
 *
 * Date: 07/03/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana 
 */

package com.velos.esch.business.eventdoc.impl;

/* Import Statements */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

/* End of Import Statements */
@Entity
@Table(name = "sch_docs")
@NamedQuery(name = "findEventdocId1", query = "SELECT OBJECT(eventdoc) FROM EventdocBean eventdoc where trim(pk_docs)=:pkDocs")
public class EventdocBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3977016236746028596L;

    /**
     * docId
     */
    public int docId;

    /**
     * docName
     */
    public String docName;

    /**
     * docDesc
     */
    public String docDesc;

    /**
     * docValue
     */
    // public byte[] docValue;
    /**
     * docSize
     */
    public Integer docSize;

    /**
     * doc type
     */
    public String docType;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    /**
     * eventId
     */
    public String eventId;
    
    public String networkId;
    
    public String networkFlag;
    
    public String docVersion;
    

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "sch_docs_seq", allocationSize=1)
    @Column(name = "PK_DOCS")
    public int getDocId() {
        return this.docId;
    }

    public void setDocId(int docId) {
        this.docId = docId;
    }

    @Column(name = "DOC_NAME")
    public String getDocName() {
        return this.docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    @Column(name = "DOC_DESC")
    public String getDocDesc() {
        return this.docDesc;
    }

    public void setDocDesc(String docDesc) {
        this.docDesc = docDesc;
    }

    @Column(name = "DOC_SIZE")
    public String getDocSize() {
        return StringUtil.integerToString(this.docSize);
    }

    public void setDocSize(String docSize) {
        Rlog.debug("eventdoc", "EventdocBean.setDocSize " + docSize);
        if (docSize != null) {
            Rlog.debug("eventdoc", "EventdocBean.setDocSize inside if");
            this.docSize = Integer.valueOf(docSize);
        }
        Rlog.debug("eventdoc", "EventdocBean.setDocSize this.docSize"
                + this.docSize);
    }

    /*
     * public byte[] getDocValue() { return this.docValue; }
     * 
     * public void setDocValue(byte[] docValue) { this.docValue = docValue; }
     */
    @Column(name = "DOC_TYPE")
    public String getDocType() {
        return this.docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Transient
    public String getEventId() {
        return this.eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
    public String getNetworkId() {
        return this.networkId;
    }

    public void setNetworkId(String networkId) {
        this.networkId = networkId;
    }
    public String getNetworkFlag() {
        return this.networkFlag;
    }

    public void setNetworkFlag(String networkFlag) {
        this.networkFlag = networkFlag;
    }
    
    @Column(name = "DOC_VERSION")
    public String getDocVersion(){
    	return this.docVersion;
    }
    
    public void setDocVersion(String docVersion){
    	this.docVersion = docVersion;
    }
    // / END OF GETTER SETTER METHODS

    /*
     * * returns the state holder object associated with this eventdoc
     */

    /*
     * public EventdocStateKeeper getEventdocStateKeeper() {
     * Rlog.debug("eventdoc", "EventdocBean.getEventdocStateKeeper");
     * Rlog.debug("eventdoc", "the value is " + getDocName()); //
     * Rlog.debug("eventdoc","the size is " + docValue.length); return new
     * EventdocStateKeeper(getDocId(), getDocName(), getDocDesc(), getDocSize(), //
     * getDocValue(),getDocType(),"aaa",getCreator(),getModifiedBy(),getIpAdd());
     * getDocType(), "aaa", getCreator(), getModifiedBy(), getIpAdd()); }
     */

    /**
     * sets up a state keeper containing details of the eventdoc
     * 
     * @param edsk
     */

    /*
     * public void setEventdocStateKeeper(EventdocStateKeeper edsk) { GenerateId
     * Id = null;
     * 
     * try {
     * 
     * Connection conn = null; conn = getConnection(); Rlog .debug("eventdoc",
     * "EventdocBean.setEventdocStateKeeper() Connection :" + conn); docId =
     * Id.getId("sch_docs_seq", conn); conn.close();
     * 
     * setDocName(edsk.getDocName()); Rlog.debug("eventdoc", "EventdocBean.line
     * 1"); setDocDesc(edsk.getDocDesc()); Rlog.debug("eventdoc",
     * "EventdocBean.line 2"); // setDocValue(edsk.getDocValue()); // byte[]
     * abc=edsk.getDocValue(); // System.out.println("value of the byte array is " +
     * abc.length); // this.docSize=new Integer(abc.length);
     * setDocSize(edsk.getDocSize());
     * 
     * Rlog.debug("eventdoc", "EventdocBean.line 3");
     * setDocType(edsk.getDocType()); Rlog.debug("eventdoc", "EventdocBean.line
     * 4"); setCreator(edsk.getCreator()); Rlog.debug("eventdoc",
     * "EventdocBean.line 5"); setModifiedBy(edsk.getModifiedBy());
     * Rlog.debug("eventdoc", "EventdocBean.line 6"); setIpAdd(edsk.getIpAdd());
     * 
     * Rlog.debug("eventdoc", "EventdocBean.setEventdocStateKeeper() eventdocId :" +
     * docId); } catch (Exception e) { System.out .println("Error in
     * setEventdocStateKeeper() in EventdocBean " + e); } }
     */

    /**
     * updates the eventdoc details
     * 
     * @param edsk
     * @return 0 if successful; -2 for exception
     */
    public int updateEventdoc(EventdocBean edsk) {
        try {

            setDocName(edsk.getDocName());
            setDocDesc(edsk.getDocDesc());
            setDocSize(edsk.getDocSize());
            // setDocValue(edsk.getDocValue());
            setDocType(edsk.getDocType());
            setCreator(edsk.getCreator());
            setModifiedBy(edsk.getModifiedBy());
            setIpAdd(edsk.getIpAdd());
            setEventId(edsk.getEventId());
            setNetworkId(edsk.getNetworkId());
            setNetworkFlag(edsk.getNetworkFlag());
            setDocVersion(edsk.getDocVersion());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("eventdoc", " error in EventdocBean.updateEventdoc : "
                    + e);
            return -2;
        }
    }

    public EventdocBean() {

    }

    public EventdocBean(int docId, String docName, String docDesc,
            String docSize, String docType, String creator, String modifiedBy,
            String ipAdd, String eventId,String networkId,String networkFlag, String docVersion) {
        super();
        // TODO Auto-generated constructor stub
        setDocId(docId);
        setDocName(docName);
        setDocDesc(docDesc);
        setDocSize(docSize);
        setDocType(docType);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setEventId(eventId);
        setNetworkId(networkId);
        setNetworkFlag(networkFlag);
        setDocVersion(docVersion);
    }

}// end of class
