/* 
 * Classname			BudgetUsersBean.class
 * 
 * Version information
 *
 * Date					03/21/2002
 *
 * Author 				Sonika Talwar
 * Copyright notice
 */

package com.velos.esch.business.budgetUsers.impl;

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

/**
 * The BudgetUsers CMP entity bean.<br>
 * <br>
 * 
 * @author Sonika Talwar
 */
@Entity
@Table(name = "sch_bgtusers")
public class BudgetUsersBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3618414913048359986L;

    /**
     * the budget user Ids
     */
    public ArrayList userIds;

    /**
     * the budget user Id
     */
    public int bgtUsersId;

    /**
     * the Budget
     */
    public Integer bgtUsersBudget;

    /**
     * the User
     */
    public Integer bgtUsers;

    /**
     * the budget users rights
     */
    public String bgtUsersRights;

    /**
     * the budget users type
     */

    public String bgtUsersType;

    /*
     * creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_SCH_BGTUSERS", allocationSize=1)
    @Column(name = "PK_BGTUSERS")
    public int getBgtUsersId() {
        return this.bgtUsersId;
    }

    public void setBgtUsersId(int bgtUsersId) {
        this.bgtUsersId = bgtUsersId;
    }

    @Transient
    public ArrayList getUserIds() {
        return this.userIds;
    }

    @Column(name = "FK_BUDGET")
    public String getBgtUsersBudget() {
        return StringUtil.integerToString(this.bgtUsersBudget);
    }

    public void setBgtUsersBudget(String bgtUsersBudget) {
        if (bgtUsersBudget != null) {
            this.bgtUsersBudget = Integer.valueOf(bgtUsersBudget);
        }
    }

    @Column(name = "FK_USER")
    public String getBgtUsers() {
        return StringUtil.integerToString(this.bgtUsers);
    }

    public void setBgtUsers(String bgtUsers) {
        if (bgtUsers != null) {
            this.bgtUsers = Integer.valueOf(bgtUsers);
        }
    }

    public void setBgtUsers(ArrayList userIds) {
        this.userIds = userIds;
    }

    /**
     * @return Budget User Rights
     */
    @Column(name = "BGTUSERS_RIGHTS")
    public String getBgtUsersRights() {
        return this.bgtUsersRights;
    }

    /**
     * @param userRights
     *            Budget User Rights
     */
    public void setBgtUsersRights(String bgtUsersRights) {
        this.bgtUsersRights = bgtUsersRights;
    }

    /**
     * @return Budget User Type
     */
    @Column(name = "BGTUSERS_TYPE")
    public String getBgtUsersType() {
        return this.bgtUsersType;
    }

    /**
     * @param userType
     *            Budget User Type
     */
    public void setBgtUsersType(String bgtUsersType) {
        this.bgtUsersType = bgtUsersType;
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /*
     * * returns the state holder object associated with this study
     */
    /*
     * public BudgetUsersStateKeeper getBudgetUsersStateKeeper() {
     * Rlog.debug("BudgetUser", "GET BudgetUser KEEPER"); return new
     * BudgetUsersStateKeeper(getBgtUsersId(), getBgtUsersBudget(),
     * getBgtUsers(), getBgtUsersRights(), getBgtUsersType(), getCreator(),
     * getModifiedBy(), getIpAdd(), getUserIds()); }
     */

    /**
     * sets up a state holder containing details of the budget user
     */
    /*
     * public void setBudgetUsersStateKeeper(BudgetUsersStateKeeper busk) {
     * Rlog.debug("BudgetUser", "IN BudgetUser STATE HOLDER"); GenerateId genId =
     * null; try { Connection conn = null; conn = getConnection();
     * 
     * Rlog.debug("BudgetUser", "Connection :" + conn); bgtUsersId =
     * genId.getId("SEQ_SCH_BGTUSERS", conn); conn.close();
     * 
     * setBgtUsersBudget(busk.getBgtUsersBudget()); Rlog.debug("BudgetUser",
     * "etBudget '" + busk.getBgtUsersBudget() + "'");
     * setBgtUsers(busk.getBgtUsers()); Rlog.debug("BudgetUser", "etBudgetUser '" +
     * busk.getBgtUsers() + "'"); setBgtUsersRights(busk.getBgtUsersRights());
     * Rlog.debug("BudgetUser", "etBgtUsersRights '" + busk.getBgtUsersRights() +
     * "'"); setBgtUsersType(busk.getBgtUsersType()); Rlog.debug("BudgetUser",
     * "etBudgetUserType'" + busk.getBgtUsersType() + "'");
     * setCreator(busk.getCreator()); Rlog.debug("BudgetUser", "etCreator '" +
     * busk.getCreator() + "'"); setModifiedBy(busk.getModifiedBy());
     * Rlog.debug("BudgetUser", "etModifiedBy '" + busk.getModifiedBy() + "'");
     * setIpAdd(busk.getIpAdd()); Rlog.debug("BudgetUser", "etIpAdd '" +
     * busk.getIpAdd() + "'"); } catch (Exception e) { Rlog.fatal("BudgetUser",
     * "EXCEPTION IN CREATING NEW BUDGET USER" + e); } }
     */

    public int updateBudgetUsers(BudgetUsersBean busk) {

        try {
            setBgtUsersBudget(busk.getBgtUsersBudget());
            setBgtUsers(busk.getBgtUsers());
            setBgtUsersRights(busk.getBgtUsersRights());
            setBgtUsersType(busk.getBgtUsersType());
            setCreator(busk.getCreator());
            setModifiedBy(busk.getModifiedBy());
            setIpAdd(busk.getIpAdd());
            return 0;
        } catch (Exception e) {
            Rlog.fatal("BudgetUser", " error BudgetUser.update");
            return -2;
        }
    }

    public BudgetUsersBean() {

    }

    public BudgetUsersBean(ArrayList userIds, int bgtUsersId,
            String bgtUsersBudget, String bgtUsers, String bgtUsersRights,
            String bgtUsersType, String creator, String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setBgtUsers(userIds);
        setBgtUsersId(bgtUsersId);
        setBgtUsersBudget(bgtUsersBudget);
        setBgtUsers(bgtUsers);
        setBgtUsersRights(bgtUsersRights);
        setBgtUsersType(bgtUsersType);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

}// end of class

