/*
 Classname	PortalFormsDao
 
 Version information 	1.0
 
 Date	06DEC2010
 
 Copyright notice		Velos, Inc.
 
 Author 		Jnanamay Majumdar
 */

package com.velos.esch.business.common;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import com.velos.esch.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

public class PortalFormsDao extends CommonDAO implements java.io.Serializable {

	private ArrayList portalFormIds;

	/*
	 * Array list for Event Ids
	 */
	private ArrayList eventIds;

	/*
	 * Array list for Event Names
	 */
	private ArrayList eventNames;

	/*
	 * Array list for Visit Ids
	 */

	private ArrayList visitIds;

	/*
	 * Array list for Visit Names
	 */

	private ArrayList visitNames;

	/*
	 * Array list for form Ids
	 */

	private ArrayList formIds;

	/*
	 * Array list for Form Names
	 */

	private ArrayList formNames;

	/*
	 * Array list for the checked Form Names
	 */

	private ArrayList checkedForms;
	
	private ArrayList<Integer> portalIds;
	private ArrayList<Integer> calIds;
	
	private ArrayList<Integer> creator;
	private ArrayList<Integer> lastModifiedBy;
	private ArrayList<Date> lastModifiedDate;
	private ArrayList<Date> createdOn;
	private ArrayList<String> ipAdd;

	public PortalFormsDao() {
		portalFormIds = new ArrayList();
		eventIds = new ArrayList();
		eventNames = new ArrayList();
		visitIds = new ArrayList();
		visitNames = new ArrayList();
		formIds = new ArrayList();
		formNames = new ArrayList();
		checkedForms = new ArrayList();
		
		portalIds = new ArrayList();
		calIds = new ArrayList();
		creator = new ArrayList();
		lastModifiedBy = new ArrayList();
		lastModifiedDate = new ArrayList();
		createdOn = new ArrayList();
		ipAdd = new ArrayList();

	}
	// Getter and Setter methods
	public ArrayList getCreator() {
		return creator;
	}
	public void setCreator(Integer creator) {
		this.creator.add(creator);
	}
	
	public ArrayList getLastModifiedBy() {
		return lastModifiedBy;
	}
	public void setLastModifiedBy(Integer lastModifiedBy) {
		this.lastModifiedBy.add(lastModifiedBy);
	}
	
	public ArrayList getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate.add(lastModifiedDate);
	}
	
	public ArrayList getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn.add(createdOn);
	}
	
	public ArrayList getIPAdd() {
		return createdOn;
	}
	public void setIPAdd(String ipAdd) {
		this.ipAdd.add(ipAdd);
	}


	public ArrayList getPortalIds() {
		return portalIds;
	}

	public void setPortalIds(Integer portalIds) {
		this.portalIds.add(portalIds);
	}

	public ArrayList getCalIds() {
		return calIds;
	}

	public void setCalIds(Integer calIds) {
		this.calIds.add(calIds);
	}

	public ArrayList getPortalFormIds() {
		return portalFormIds;
	}

	public void setPortalFormIds(ArrayList eventIds) {
		this.portalFormIds = portalFormIds;
	}

	public void setPortalFormIds(String portalFormId) {
		this.portalFormIds.add(portalFormId);
	}

	public ArrayList getEventIds() {
		return eventIds;
	}

	public void setEventIds(ArrayList eventIds) {
		this.eventIds = eventIds;
	}

	public void setEventIds(String eventId) {
		this.eventIds.add(eventId);
	}

	public ArrayList getEventNames() {
		return eventNames;
	}

	public void setEventNames(ArrayList eventNames) {
		this.eventNames = eventNames;
	}

	public void setEventNames(String eventName) {
		this.eventNames.add(eventName);
	}

	public ArrayList getVisitIds() {
		return visitIds;
	}

	public void setVisitIds(ArrayList visitIds) {
		this.visitIds = visitIds;
	}

	public void setVisitIds(String visitId) {
		this.visitIds.add(visitId);
	}

	public ArrayList getVisitNames() {
		return visitNames;
	}

	public void setVisitNames(ArrayList visitNames) {
		this.visitNames = visitNames;
	}

	public void setVisitNames(String visitName) {
		this.visitNames.add(visitName);
	}

	public ArrayList getFormIds() {
		return formIds;
	}

	public void setFormIds(ArrayList formIds) {
		this.formIds = formIds;
	}

	public void setFormIds(String formId) {
		this.formIds.add(formId);
	}

	public ArrayList getFormNames() {
		return formNames;
	}

	public void setFormNames(ArrayList formNames) {
		this.formNames = formNames;
	}

	public void setFormNames(String formName) {
		this.formNames.add(formName);
	}

	public ArrayList getCheckedForms() {
		return checkedForms;
	}

	public void setCheckedForms(ArrayList checkedForms) {
		this.checkedForms = checkedForms;
	}

	public void setCheckedForms(String checkedForm) {
		this.checkedForms.add(checkedForm);
	}

	// end of getter and setter methods

	/**
	 * Gets all the forms of Calendar events of a portal
	 * 
	 * @param calId
	 */

	public void getCheckedFormsForPortalCalendar(int calId, int portalId) {

		PreparedStatement pstmt = null;
		Connection conn = null;
		String sqlStr = "";
		try {
			conn = getConnection();
			sqlStr = "select a.event_id fk_event, a.name as event_name, "
					+ "a.fk_visit as visit_id, "
					+ "(select VISIT_NAME from SCH_PROTOCOL_VISIT where PK_PROTOCOL_VISIT = a.fk_visit) visit_name,  "
					+ "b.fk_form fk_form, (select form_name from er_formlib where pk_formlib=b.fk_form) form_Name , "
					+ "(select pk_pf from SCH_PORTAL_FORMS where b.fk_event=fk_event and b.fk_form=fk_form and fk_portal=?) as pkPf, "
					+ "(select nvl(FK_FORM,0) from SCH_PORTAL_FORMS where b.fk_event=fk_event and b.fk_form=fk_form and fk_portal=?) as formAccess "
					+ "from event_assoc a, sch_Event_crf b "
					+ "where a.chain_id=lpad(to_char(?),10,'0') and a.FK_VISIT<>0 "
					+ "and b.fk_event=a.event_id and b.fk_form <> 0 "
					+ "ORDER BY  a.DISPLACEMENT, a.fk_visit, a.EVENT_SEQUENCE, lower(form_name) ";
			pstmt = conn.prepareStatement(sqlStr);
			pstmt.setInt(1, portalId);
			pstmt.setInt(2, portalId);
			pstmt.setInt(3, calId);

			// System.out.println("sqlStr=="+sqlStr);

			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {

				setPortalFormIds(rs.getString("pkPf"));
				setEventIds(rs.getString("fk_event"));
				setEventNames(rs.getString("event_name"));
				setVisitIds(rs.getString("visit_id"));
				setVisitNames(rs.getString("visit_name"));
				setFormIds(rs.getString("fk_form"));
				setFormNames(rs.getString("form_Name"));
				setCheckedForms(rs.getString("formAccess"));

			}

		} catch (SQLException ex) {
			Rlog.fatal("portalForms",
					"PortalFormsDao.getCheckedFormsForPortalCalendar EXCEPTION IN FETCHING FROM db"
							+ ex);
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}

	}

	public int removePortalFormId(int pfId) {

		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;

		try {
			conn = getConnection();
			String mysql = "delete from SCH_PORTAL_FORMS " + " WHERE PK_PF = ?";

			pstmt = conn.prepareStatement(mysql);
			pstmt.setInt(1, pfId);
			rs = pstmt.executeQuery();
			rs.next();

		} catch (SQLException ex) {
			Rlog.fatal("portalForms", "PortalFormsDao.removePortalFormId()"
					+ ex);
			return -2;
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
		return 1;

	}

	public int removeCalFromPortal(int potalId, int calId) {

		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;

		try {
			conn = getConnection();
			String mysql = "delete from SCH_PORTAL_FORMS "
					+ " WHERE FK_PORTAL = ? and FK_CALENDAR<>?";

			pstmt = conn.prepareStatement(mysql);
			pstmt.setInt(1, potalId);
			pstmt.setInt(2, calId);
			// PP-18306 AGodara
			pstmt.executeUpdate();

		} catch (SQLException ex) {
			Rlog.fatal("portalForms", "PortalFormsDao.removeCalFromPortal()"
					+ ex);
			return -2;
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
		return 1;

	}

	// PP-18306 ::: AGodara
	public int removeMulCalForms(int potalId, String calIds) {

		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;

		try {
			String[] calIdArray = calIds.split(",");
			int len = calIdArray.length;

			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < len;) {
				builder.append("?");
				if (++i < len) {
					builder.append(",");
				}
			}
			conn = getConnection();
			String mysql = "delete from SCH_PORTAL_FORMS "
					+ " WHERE FK_PORTAL = ? and FK_CALENDAR NOT IN ("
					+ builder.toString() + ")";

			pstmt = conn.prepareStatement(mysql);
			pstmt.setInt(1, potalId);
			for (int i = 0; i < calIdArray.length; i++) {
				pstmt.setInt(2 + i, StringUtil.stringToNum(calIdArray[i]));
			}
			pstmt.executeUpdate();

		} catch (SQLException ex) {
			Rlog.fatal("portalForms", "PortalFormsDao.removeCalFromPortal()"
					+ ex);
			return -2;
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
		return 1;

	}

	public void getPortalFormsDAO(int eventId1,
			ArrayList<String> selId) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		try {
			conn = getConnection();
			StringBuilder sqlStr = new StringBuilder();
			sqlStr
					.append("SELECT FK_PORTAL,FK_CALENDAR,FK_EVENT,FK_FORM");
			sqlStr
			.append(",CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD from SCH_PORTAL_FORMS ");
			sqlStr.append("Where FK_EVENT=? AND FK_FORM IN (");
			while(selId.remove("~"));
			int len =selId.size();
			for (int i = 0; i < len; i++) {
					sqlStr.append("?,");
			}
			sqlStr.deleteCharAt(sqlStr.length()-1);
			sqlStr.append(")");
			pstmt = conn.prepareStatement(sqlStr.toString());
			pstmt.setInt(1, eventId1);
			for (int j = 0; j < len; j++) {
					pstmt.setInt(j + 2, StringUtil.stringToInteger(selId.get(j)));
			}
			
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				
				setPortalIds(rs.getInt("FK_PORTAL"));
				setCalIds(rs.getInt("FK_CALENDAR"));
				setEventIds(rs.getString("FK_EVENT"));
				setFormIds(rs.getString("FK_FORM"));
				setCreator(rs.getInt("CREATOR"));
				setLastModifiedBy(rs.getInt("LAST_MODIFIED_BY"));
				setLastModifiedDate(rs.getDate("LAST_MODIFIED_DATE"));
				setCreatedOn(rs.getDate("CREATED_ON"));
				setIPAdd(rs.getString("IP_ADD"));
			}					
		} catch (SQLException ex) {
			Rlog.fatal("PortalFormsDao",
					"getPortalFormsDAO EXCEPTION IN FETCHING FROM db"
							+ ex);
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
	}

	public int setPortalFormsDAO(PortalFormsDao pfDao) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		try {
			conn = getConnection();
			StringBuilder sqlStr = new StringBuilder();
			sqlStr
					.append("INSERT INTO SCH_PORTAL_FORMS (PK_PF,FK_PORTAL,FK_CALENDAR,FK_EVENT,FK_FORM,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD)");
			sqlStr.append(" values (SEQ_SCH_PORTAL_FORMS.nextval,?,?,?,?,?,?,?,?,?)");
			int len = this.formIds.size();
			pstmt = conn.prepareStatement(sqlStr.toString());
			for(int i=0;i<len;i++){
				pstmt.setInt(1,this.portalIds.get(i));
				pstmt.setInt(2,this.calIds.get(i));
				pstmt.setInt(3,StringUtil.stringToInteger((String) this.eventIds.get(i)));
				pstmt.setInt(4,StringUtil.stringToInteger((String)  this.formIds.get(i)));
				pstmt.setInt(5,this.creator.get(i));
				pstmt.setInt(6,this.lastModifiedBy.get(i));
				pstmt.setDate(7, this.lastModifiedDate.get(i));
				pstmt.setDate(8, this.createdOn.get(i));
				pstmt.setString(9,this.ipAdd.get(i));
				pstmt.execute();
			}
		} catch (SQLException ex) {
			Rlog.fatal("PortalFormsDao",
					"setPortalFormsDAO EXCEPTION IN FETCHING FROM db"
							+ ex);
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
		return 0;
	}
}