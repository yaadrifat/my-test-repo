/*
 Classname	StorageKitDao.class
 
 Version information 	1.0
 
 Date	05/09/2008
 
 Copyright notice		Velos, Inc.
 
 Author 		Sajal
 */

package com.velos.esch.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.esch.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

public class EventKitDao extends CommonDAO implements java.io.Serializable {
	
    private ArrayList ids;

    private ArrayList eventids;

    private ArrayList storageKits;

    private ArrayList storageKitNames;
    private ArrayList storageKitIds;

    
    private int cRows;
    
     
   
   
   public EventKitDao() {
        ids = new ArrayList();
        
        eventids = new ArrayList();
        storageKitNames = new ArrayList();
        storageKits = new ArrayList();
        
        storageKitIds = new ArrayList();
        
    }

    public void resetDao() {
    	ids.clear();
    	eventids.clear();
    	storageKitNames.clear();
    	storageKits.clear();
        
        
    }

    // Getter and Setter methods

       
   

    // end of getter and setter methods

    /**
     * Gets all Storage Kits linked with an event
     * 
     * @param eventid
     */

    public void getEventStorageKits(int eventId)
    {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
            conn = getConnection();
            sqlStr = "select  storage_name, PK_EVENTKIT  ,  FK_EVENT ,  k.FK_STORAGE, STORAGE_ID  from sch_event_kit k,er_storage where k.fk_event = ? and pk_storage=k.fk_storage "
                    + "order by lower(storage_name) ";
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setInt(1, eventId);
           

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setIds(rs.getString("PK_EVENTKIT"));
                
                setEventids(rs.getString("FK_EVENT"));
                setStorageKitNames(rs.getString("storage_name"));
                setStorageKits(rs.getString("FK_STORAGE"));
                setStorageKitIds(rs.getString("STORAGE_ID"));
                
                rows++;
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("evebtkit",
                    "eventkit.getEventStorageKits  EXCEPTION :"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }
  
     
    /**
     * Delete Event Storage Kits 
     * 
     * @param eventId
     */
    
  
  public void deleteEventKit(int eventId){
  	PreparedStatement pstmt = null;
      Connection conn = null;
      String sql = "";

      try {
          conn = getConnection();

          sql = "delete from sch_event_kit where fk_event=?"; 

          pstmt = conn.prepareStatement(sql);
          pstmt.setInt(1, eventId);
          pstmt.executeQuery();
      } catch (SQLException ex) {
          Rlog.fatal("eventkit",
                  "EventKitDao.deleteEventKit EXCEPTION"
                          + ex);
      } finally {
          try {
              if (pstmt != null)
            	  pstmt.close();
          } catch (Exception e) {
          }
          try {
              if (conn != null)
                  conn.close();
          } catch (Exception e) {
          }

      }
 	
  }
 


		public int getCRows() {
			return cRows;
		}

		public void setCRows(int rows) {
			cRows = rows;
		}

		public ArrayList getEventids() {
			return eventids;
		}

		public void setEventids(ArrayList eventids) {
			this.eventids = eventids;
		}
		
		public void setEventids(String eventid) {
			this.eventids.add(eventid);
		}

		public ArrayList getIds() {
			return ids;
		}

		public void setIds(ArrayList ids) {
			this.ids = ids;
		}
		
		public void setIds(String id) {
			this.ids.add(id);
		}


		public ArrayList getStorageKitNames() {
			return storageKitNames;
		}

		public void setStorageKitNames(ArrayList storageKitNames) {
			this.storageKitNames = storageKitNames;
		}
		
		public void setStorageKitNames(String storageKitName) {
			this.storageKitNames.add(storageKitName);
		}

		public ArrayList getStorageKits() {
			return storageKits;
		}

		public void setStorageKits(ArrayList storageKits) {
			this.storageKits = storageKits;
		}
		public void setStorageKits(String storageKit) {
			this.storageKits.add(storageKit);
		}
		
		public ArrayList getStorageKitsIds(){
			return storageKitIds;
		}
		 
		public void setStorageKitIds(ArrayList storageKitsIds){
			this.storageKitIds = storageKitsIds;
		}
		public void setStorageKitIds(String storageKitsId) {
			this.storageKitIds.add(storageKitsId);
		}
		
}
