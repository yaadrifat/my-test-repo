/*
 * Classname	BgtApndxDao.class
 * 
 * Version information 	1.0
 *
 * Date	04/09/2002
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.esch.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.esch.service.util.Rlog;

/**
 * BgtApndxDao for getting urls and files added to the budget
 * 
 * @author Sonika
 * @version : 1.0 04/09/2002
 */

public class BgtApndxDao extends CommonDAO implements java.io.Serializable {
    private ArrayList bgtApndxIds;

    private ArrayList bgtApndxBudgets;

    private ArrayList bgtApndxDescs;

    private ArrayList bgtApndxUris;

    private ArrayList bgtApndxTypes;

    private int aRows;

    public BgtApndxDao() {
        bgtApndxIds = new ArrayList();
        bgtApndxBudgets = new ArrayList();
        bgtApndxDescs = new ArrayList();
        bgtApndxUris = new ArrayList();
        bgtApndxTypes = new ArrayList();
    }

    // Getter and Setter methods

    public ArrayList getBgtApndxIds() {
        return this.bgtApndxIds;
    }

    public void setBgtApndxIds(ArrayList bgtApndxIds) {
        this.bgtApndxIds = bgtApndxIds;
    }

    public void setBgtApndxIds(Integer bgtApndxId) {
        this.bgtApndxIds.add(bgtApndxId);
    }

    public ArrayList getBgtApndxBudgets() {
        return this.bgtApndxBudgets;
    }

    public void setBgtApndxBudgets(ArrayList bgtApndxBudgets) {
        this.bgtApndxBudgets = bgtApndxBudgets;
    }

    public void setBgtApndxBudgets(String bgtApndxBudget) {
        this.bgtApndxBudgets.add(bgtApndxBudget);
    }

    public ArrayList getBgtApndxDescs() {
        return this.bgtApndxDescs;
    }

    public void setBgtApndxDescs(ArrayList bgtApndxDescs) {
        this.bgtApndxDescs = bgtApndxDescs;
    }

    public void setBgtApndxDescs(String bgtApndxDesc) {
        this.bgtApndxDescs.add(bgtApndxDesc);
    }

    public ArrayList getBgtApndxUris() {
        return this.bgtApndxUris;
    }

    public void setBgtApndxUris(ArrayList bgtApndxUris) {
        this.bgtApndxUris = bgtApndxUris;
    }

    public void setBgtApndxUris(String bgtApndxUri) {
        this.bgtApndxUris.add(bgtApndxUri);
    }

    public ArrayList getBgtApndxTypes() {
        return this.bgtApndxTypes;
    }

    public void setBgtApndxTypes(ArrayList bgtApndxTypes) {
        this.bgtApndxTypes = bgtApndxTypes;
    }

    public void setBgtApndxTypes(String bgtApndxType) {
        this.bgtApndxTypes.add(bgtApndxType);
    }

    public void setARows(int bRows) {
        this.aRows = aRows;
    }

    public int getARows() {
        return this.aRows;
    }

    // end of getter and setter methods

    /**
     * Gets all URLs linked with the budget
     * 
     * @param budgetId
     */

    public void getBgtApndxUrls(int budgetId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
            conn = getConnection();
            sqlStr = "select PK_BGTAPNDX, " + "BGTAPNDX_DESC, "
                    + "BGTAPNDX_URI " + "from sch_bgtapndx "
                    + "where fk_budget= ? " + "and bgtapndx_type = 'U' ";

            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setInt(1, budgetId);

            Rlog.debug("BgtApndx", "getBgtApndxUrls SQL**** " + sqlStr);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setBgtApndxIds(new Integer(rs.getInt("PK_BGTAPNDX")));
                setBgtApndxDescs(rs.getString("BGTAPNDX_DESC"));
                setBgtApndxUris(rs.getString("BGTAPNDX_URI"));
                rows++;
                Rlog.debug("BgtApndx", "getBgtApndxUrls rows " + rows);
            }

            setARows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("BgtApndx",
                    "getBgtApndxUrls EXCEPTION IN FETCHING FROM Budget Appendix table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Gets all Files linked with the budget
     * 
     * @param budgetId
     */

    public void getBgtApndxFiles(int budgetId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
            conn = getConnection();
            sqlStr = "select PK_BGTAPNDX, " + "BGTAPNDX_DESC, "
                    + "BGTAPNDX_URI " + "from sch_bgtapndx "
                    + "where fk_budget= ? " + "and bgtapndx_type = 'F' ";

            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setInt(1, budgetId);

            Rlog.debug("BgtApndx", "getBgtApndxFiles SQL**** " + sqlStr);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setBgtApndxIds(new Integer(rs.getInt("PK_BGTAPNDX")));
                setBgtApndxDescs(rs.getString("BGTAPNDX_DESC"));
                setBgtApndxUris(rs.getString("BGTAPNDX_URI"));
                rows++;
                Rlog.debug("BgtApndx", ".getBgtApndxFiles rows " + rows);
            }

            setARows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("BgtApndx",
                    "getBgtApndxFiles EXCEPTION IN FETCHING FROM Budget Appendix table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

} // end of class
