/*
 * Classname : PortalFormsBean
 *
 * Version information: 1.0
 *
 * Date: 03DEC2010
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Jnanamay Majumdar
 */

package com.velos.esch.business.portalForms.impl;

/* Import Statements */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

/* End of Import Statements */

@Entity
@Table (name="SCH_PORTAL_FORMS")
public class PortalFormsBean implements Serializable{

	private static final long serialVersionUID = 4049636780653687600L;


	public int portalFormId;
	public Integer portalId;
	public Integer caledarId;
	public Integer eventId;
	public Integer formId;

	/*
     * the record creator
     */

    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;



	public PortalFormsBean() {

    }


	public PortalFormsBean(int portalFormId,
		String portalId, String caledarId, String eventId,
		String formId, String creator, String modifiedBy, String ipAdd){

		setPortalFormId(portalFormId);
		setPortalId(portalId);
		setCaledarId(caledarId);
		setEventId(eventId);
		setFormId(formId);
		setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
	}

    // GETTER SETTER METHODS

	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_SCH_PORTAL_FORMS", allocationSize=1)
    @Column(name = "PK_PF")
    public int getPortalFormId() {
        return this.portalFormId;
    }

    public void setPortalFormId(int portalFormId) {
        this.portalFormId = portalFormId;
    }


	@Column(name = "FK_PORTAL")
    public String getPortalId() {
        return StringUtil.integerToString(this.portalId);
    }

    public void setPortalId(String portalId) {

        if (portalId != null) {
            this.portalId = Integer.valueOf(portalId);
        }
    }

	@Column(name = "FK_CALENDAR")
    public String getCaledarId() {
        return StringUtil.integerToString(this.caledarId);
    }

    public void setCaledarId(String caledarId) {

        if (caledarId != null) {
            this.caledarId = Integer.valueOf(caledarId);
        }
    }

	@Column(name = "FK_EVENT")
    public String getEventId() {
        return StringUtil.integerToString(this.eventId);
    }

    public void setEventId(String eventId) {

        if (eventId != null) {
            this.eventId = Integer.valueOf(eventId);
        }
    }


	@Column(name = "FK_FORM")
    public String getFormId() {
        return StringUtil.integerToString(this.formId);
    }

    public void setFormId(String formId) {

        if (formId != null) {
            this.formId = Integer.valueOf(formId);
        }
    }


	/**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return Modified By
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modified
     *            by
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }


	// GETTER SETTER METHODS


	/**
     * updates the portal forms details
     *
     * @param edsk
     * @return 0 if successful; -2 for exception
     */

    public int updatePortalForms(PortalFormsBean edsk) {
        try {


			setPortalFormId(edsk.getPortalFormId());
			setPortalId(edsk.getPortalId());
			setCaledarId(edsk.getCaledarId());
			setEventId(edsk.getEventId());
			setFormId(edsk.getFormId());
			setCreator(edsk.getCreator());
			setModifiedBy(edsk.getModifiedBy());
			setIpAdd(edsk.getIpAdd());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("portalForms", " error in PortalFormsBean.updatePortalForms : " + e);
            return -2;
        }
    }





};