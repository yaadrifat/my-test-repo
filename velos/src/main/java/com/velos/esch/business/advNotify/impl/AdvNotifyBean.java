/*
 * Classname : AdvNotifyBean
 *
 * Version information: 1.0
 *
 * Date: 12/11/2002
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.business.advNotify.impl;

/* Import Statements */
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

//import com.velos.esch.service.util.DateUtil;//KM
import com.velos.eres.service.util.DateUtil;
import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

/* End of Import Statements */
@Entity
@Table(name = "SCH_ADVNOTIFY")
public class AdvNotifyBean implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 3256446910599543092L;

    /**
     * adverse event notification id
     */
    public int advNotifyId;

    /**
     * adverse event id
     */
    public Integer advNotifyAdverseId;

    /**
     * notification type
     */
    public Integer advNotifyCodelstNotTypeId;

    /**
     * notification date
     */
    public Date advNotifyDate;

    /**
     * notes
     */
    public String advNotifyNotes;

    /**
     * adverse event notification flag. The flag determines if the notification
     * is sent.
     */
    public Integer advNotifyValue;

    /** creator */
    public Integer creator;

    /**
     * modified By
     */
    public Integer modifiedBy;

    /**
     * IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS
    /**
     * Returns an id for the adverse event notification
     *
     * @return the adverse event info id
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_SCH_ADVNOTIFY", allocationSize=1)
    @Column(name = "PK_ADVNOTIFY")
    public int getAdvNotifyId() {
        return this.advNotifyId;
    }

    /**
     * Sets the id for the adverse event notification
     *
     * @param advNotifyId
     *            the adverse event notification id
     */
    public void setAdvNotifyId(int advNotifyId) {
        this.advNotifyId = advNotifyId;
    }

    /**
     * Gets id of the parent adverse event
     *
     * @return id of the parent adverse event
     */
    @Column(name = "FK_ADVERSEVE")
    public String getAdvNotifyAdverseId() {
        return StringUtil.integerToString(this.advNotifyAdverseId);
    }

    /**
     * Sets the id for the parent adverse event
     *
     * @param advInfoAdverseId
     *            the parent adverse event
     */
    public void setAdvNotifyAdverseId(String advNotifyAdverseId) {
        if (advNotifyAdverseId != null) {
            this.advNotifyAdverseId = Integer.valueOf(advNotifyAdverseId);
        }
    }

    /**
     * Gets notification type
     *
     * @return notification type
     */
    @Column(name = "FK_CODELST_NOT_TYPE")
    public String getAdvNotifyCodelstNotTypeId() {
        return StringUtil.integerToString(this.advNotifyCodelstNotTypeId);
    }

    /**
     * Sets notification type
     *
     * @param advNotifyCodelstNotTypeId
     *            notification type
     */
    public void setAdvNotifyCodelstNotTypeId(String advNotifyCodelstNotTypeId) {
        if (advNotifyCodelstNotTypeId != null) {
            this.advNotifyCodelstNotTypeId = Integer
                    .valueOf(advNotifyCodelstNotTypeId);
        }
    }

    /**
     * Gets notification date
     *
     * @return notification date
     */
    @Column(name = "ADVNOTIFY_DATE")
    public Date getAdvNotifyDt() {
        return this.advNotifyDate;
    }

    /**
     * Sets notification date
     *
     * @param advNotifyDate
     *            notification date
     */

    public void setAdvNotifyDt(Date advNotifyDate) {
        this.advNotifyDate = advNotifyDate;

    }

    @Transient
    public String getAdvNotifyDate() {
        return DateUtil.dateToString(getAdvNotifyDt());
    }

    /**
     * Sets notification date
     *
     * @param advNotifyDate
     *            notification date
     */
    public void setAdvNotifyDate(String advNotifyDate) {
    	setAdvNotifyDt(DateUtil.stringToDate(advNotifyDate, null));

    }

    /**
     * Gets notification notes
     *
     * @return notification notes
     */
    @Column(name = "ADVNOTIFY_NOTES")
    public String getAdvNotifyNotes() {
        return this.advNotifyNotes;
    }

    /**
     * Sets notification notes
     *
     * @param advNotifyNotes
     *            notification notes
     */
    public void setAdvNotifyNotes(String advNotifyNotes) {
        this.advNotifyNotes = advNotifyNotes;
    }

    /**
     * Gets notification flag. The flag determines if the notification is sent.
     *
     * @return notification flag
     */
    @Column(name = "ADVNOTIFY_VALUE")
    public String getAdvNotifyValue() {
        return StringUtil.integerToString(this.advNotifyValue);
    }

    /**
     * Sets notification flag. The flag determines if the notification is sent.
     *
     * @param advNotifyValue
     *            notification flag
     */
    public void setAdvNotifyValue(String advNotifyValue) {
        if (advNotifyValue != null) {
            this.advNotifyValue = Integer.valueOf(advNotifyValue);
        }
    }

    /**
     * Gets the user who created the adverse event notification
     *
     * @return the user who created the adverse event notification
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * Sets the user who created the adverse event notification
     *
     * @param creator
     *            the user who created the adverse event notification
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * Gets the user who modified the adverse event notification
     *
     * @return the user who modified the adverse event notification
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * Sets the user who modified the adverse event notification
     *
     * @param modifiedBy
     *            the user who modified the adverse event notification
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * Gets the IP address of the user who created/modified the adverse event
     * info
     *
     * @return IP address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * Sets the IP address of the user who created/modified the adverse event
     * info
     *
     * @param ipAdd
     *            IP address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // / END OF GETTER SETTER METHODS

    /**
     * returns an AdvNotifyStateKeeper object containing details of the bean
     * instance
     *
     * @return AdvNotifyStateKeeper object containing adverse event notification
     *         details
     */

    /*
     * public AdvNotifyStateKeeper getAdvNotifyStateKeeper() {
     * Rlog.debug("advNotify", "AdvNotifyBean.getAdvNotifyStateKeeper"); return
     * new AdvNotifyStateKeeper(getAdvNotifyId(), getAdvNotifyAdverseId(),
     * getAdvNotifyCodelstNotTypeId(), getAdvNotifyDate(), getAdvNotifyNotes(),
     * getAdvNotifyValue(), getCreator(), getModifiedBy(), getIpAdd()); }
     */

    /**
     * sets bean's attributes
     *
     * @param usk
     *            AdvNotifyStateKeeper object containing adverse event
     *            notification details
     */

    /*
     * public void setAdvNotifyStateKeeper(AdvNotifyStateKeeper usk) {
     * GenerateId advId = null;
     *
     * try { Connection conn = null; conn = getConnection();
     * Rlog.debug("advNotify", "AdvNotify.setAdvNotifyStateKeeper() Connection :" +
     * conn); advNotifyId = advId.getId("SEQ_SCH_ADVNOTIFY", conn);
     * conn.close();
     *
     * Rlog.debug("advNotify", "AdvNotifyBean.line 1 " + advNotifyId);
     *
     * Rlog.debug("advNotify", "AdvNotifyBean.line 2 usk.getAdvNotifyId() " +
     * usk.getAdvNotifyId());
     *
     * setAdvNotifyAdverseId(usk.getAdvNotifyAdverseId());
     * Rlog.debug("advNotify", "AdvNotifyBean.line 2 usk.getAdvNotifyAdverseId() " +
     * usk.getAdvNotifyAdverseId());
     *
     * setAdvNotifyCodelstNotTypeId(usk.getAdvNotifyCodelstNotTypeId());
     * Rlog.debug("advNotify", "AdvNotifyBean.line 2
     * usk.getAdvNotifyCodelstNotTypeId() " +
     * usk.getAdvNotifyCodelstNotTypeId());
     *
     * setAdvNotifyDate(usk.getAdvNotifyDate()); Rlog.debug("advNotify",
     * "AdvNotifyBean.line 2 usk.getAdvNotifyDate() " + usk.getAdvNotifyDate());
     *
     * setAdvNotifyNotes(usk.getAdvNotifyNotes()); Rlog.debug("advNotify",
     * "AdvNotifyBean.line 2 usk.getAdvNotifyNotes() " +
     * usk.getAdvNotifyNotes());
     *
     * setAdvNotifyValue(usk.getAdvNotifyValue()); Rlog.debug("advNotify",
     * "AdvNotifyBean.line 2 usk.getAdvNotifyValue() " +
     * usk.getAdvNotifyValue());
     *
     * setCreator(usk.getCreator()); Rlog.debug("advNotify", "AdvNotifyBean.line
     * 2 usk.getCreator()" + usk.getCreator());
     *
     * setModifiedBy(usk.getModifiedBy()); Rlog.debug("advNotify",
     * "AdvNotifyBean.line 2 usk.getModifiedBy()" + usk.getModifiedBy());
     *
     * setIpAdd(usk.getIpAdd()); Rlog.debug("advNotify", "AdvNotifyBean.line 2
     * usk.getIpAdd()" + usk.getIpAdd());
     *
     * Rlog.debug("advNotify", "AdvNotifyBean.setAdvNotifyStateKeeper()
     * advNotifyId :" + advNotifyId); } catch (Exception e) {
     * Rlog.fatal("advNotify", "Error in setAdvNotifyStateKeeper() in
     * AdvNotifyBean " + e); } }
     */

    /**
     * updates adverse event notification
     *
     * @param edsk
     *            an AdvNotifyStateKeeper object containing adverse event
     *            notification details
     * @return success flag
     *         <ul>
     *         <li> 0 if successful</li>
     *         <li> -2 in case of a failure</li>
     *         </ul>
     */
    public int updateAdvNotify(AdvNotifyBean edsk) {
        try {

            setAdvNotifyAdverseId(edsk.getAdvNotifyAdverseId());
            setAdvNotifyCodelstNotTypeId(edsk.getAdvNotifyCodelstNotTypeId());
            setAdvNotifyDate(edsk.getAdvNotifyDate());
            setAdvNotifyNotes(edsk.getAdvNotifyNotes());
            setAdvNotifyValue(edsk.getAdvNotifyValue());

            setCreator(edsk.getCreator());
            setModifiedBy(edsk.getModifiedBy());
            setIpAdd(edsk.getIpAdd());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("advNotify",
                    " error in AdvNotifyBean.updateAdvNotify : " + e);
            return -2;
        }
    }

    public AdvNotifyBean() {

    }

    public AdvNotifyBean(int advNotifyId, String advNotifyAdverseId,
            String advNotifyCodelstNotTypeId, String advNotifyDate,
            String advNotifyNotes, String advNotifyValue, String creator,
            String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setAdvNotifyId(advNotifyId);
        setAdvNotifyAdverseId(advNotifyAdverseId);
        setAdvNotifyCodelstNotTypeId(advNotifyCodelstNotTypeId);
        setAdvNotifyDate(advNotifyDate);
        setAdvNotifyNotes(advNotifyNotes);
        setAdvNotifyValue(advNotifyValue);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

}// end of class
