package com.velos.esch.web.protvisit;

import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.*;
import com.velos.esch.business.common.EventAssocDao;
import com.velos.esch.web.eventassoc.EventAssocJB;
import com.velos.esch.business.common.EventdefDao;
import com.velos.esch.web.eventdef.EventdefJB;
import com.velos.esch.business.common.ProtVisitDao;
import com.velos.esch.web.protvisit.ProtVisitJB;
import com.velos.esch.business.common.SchCodeDao;


public class ProtVisitEventGridJB {

	public ProtVisitEventGridJB() {
		
	}
    public String fetchProtJSON(String protocolId, String calledFrom,String calassoc,String pgRight,String sortOrder,String eventName,String eUrl) 
    throws JSONException {	
    JSONObject jsObj = new JSONObject();
    boolean isLibCal = false;
	boolean isStudyCal = false;
	String protocolName = null;
	String calStatus = null;
	int errorCode = 0;
	ArrayList costs=null;
	ArrayList eventIds = null, eventNames = null;
	ArrayList eventVisitIds = null;
	ArrayList eventOfflines = null;
	ArrayList visithideFlags = null;//YK: Fix for Bug#7264
	ArrayList eventCatNames=null;
	//Yk: Added for Enhancement :PCAL-19743
	ArrayList newEventIds = null;
	   int totalEventRecords=0;
       int totalVisitRecords=0;
       
	if ("P".equals(calledFrom) || "L".equals(calledFrom)) {
		isLibCal = true;
	} else if ("S".equals(calledFrom)) {
		isStudyCal = true;
	} else {
		jsObj.put("error", new Integer(-3));
		jsObj.put("errorMsg", MC.M_CalType_Miss/*Calendar Type is missing.*****/);
		return jsObj.toString();
	}
	ProtVisitDao visitdao = new ProtVisitDao();
	ProtVisitJB protVisitB = new ProtVisitJB();
	ArrayList visitIds = null;
	visitdao = protVisitB.getProtocolVisits(EJBUtil.stringToNum(protocolId));
		
	
	visitIds = visitdao.getVisit_ids();
	

  String visitIds1= visitIds.toString().replace("[", "").replace("]", "");
	String calStatusPk = "";
	SchCodeDao scho = new SchCodeDao();
	EventdefJB eventdefB = new EventdefJB();
	EventAssocJB eventassocB = new EventAssocJB();
	if (isLibCal) {
		EventdefDao defdao=null;
		defdao=eventdefB.getProtSelectedAndVisitEvents(EJBUtil.stringToNum(protocolId),eventName);
		eventIds = defdao.getEvent_ids();
		eventVisitIds = defdao.getFk_visit();
		newEventIds = defdao.getNew_event_ids();
		eventCatNames=defdao.getEventCategory();
		costs=defdao.getCosts();
		eventNames = defdao.getNames();
		eventdefB.setEvent_id(EJBUtil.stringToNum(protocolId));
		eventdefB.getEventdefDetails();
		
		protocolName = eventdefB.getName();


		//KM-#DFin9
		calStatusPk = eventdefB.getStatCode();
		calStatus = scho.getCodeSubtype(
				EJBUtil.stringToNum(calStatusPk)).trim();
		
	} else if (isStudyCal) {
		EventAssocDao assocdao=null;
		assocdao=eventassocB.getProtSelectedAndVisitEvents(EJBUtil.stringToNum(protocolId),eventName);
		eventIds = assocdao.getEvent_ids();
		newEventIds = assocdao.getNew_event_ids();
		eventCatNames=assocdao.getEventCategory();
		eventVisitIds = assocdao.getEventVisits();
		eventNames = assocdao.getNames();
		costs=assocdao.getCosts();
		eventOfflines = assocdao.getOfflineFlags(); // Unique to event_assoc, not in event_def
		eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
		eventassocB.getEventAssocDetails();
		protocolName = eventassocB.getName();
		visithideFlags = assocdao.getHideFlags();//YK: Fix for Bug#7264
		//KM-#DFin9
		calStatusPk = eventassocB.getStatCode();
		calStatus = scho.getCodeSubtype(
				EJBUtil.stringToNum(calStatusPk)).trim();
       // totalEventRecords = assocdao.getTotalRecords();

	}

	ArrayList visitNames = null;
	ArrayList visitDisplacements = null;
	String visitId = null;
	String visitName = null;
	String offLineFlag = ""; //Yk: Added for Enhancement :PCAL-19743
	
	visitNames = visitdao.getNames();
	visitDisplacements = visitdao.getDisplacements();

	int noOfEvents = eventIds == null ? 0 : eventIds.size();
	if (protocolName == null) {
		protocolName = "";
	}
	int noOfVisits = visitIds == null ? 0 : visitIds.size();

	jsObj.put("error", errorCode);
	jsObj.put("errorMsg", "");
	jsObj.put("noOfEvents", noOfEvents);

	JSONArray jsEvents = new JSONArray();
	for (int iX = 0; iX < eventIds.size(); iX++) {
		JSONObject jsObj1 = new JSONObject();
		jsObj1
				.put(String.valueOf(eventIds.get(iX)), eventNames
						.get(iX));
		jsEvents.put(jsObj1);
	}
	//YK: Fix for Bug#7264
	JSONArray jsVisits = new JSONArray();
	if (isStudyCal) {
		for (int iX = 0; iX < eventIds.size(); iX++) {
			JSONObject visitsObj = new JSONObject();
			if (((Integer) eventIds.get(iX)).intValue() != 0) {
				continue;
			}
			String hideFlag = visithideFlags.get(iX) == null ? "0"
					: (String) visithideFlags.get(iX);
			visitsObj.put("key", "v"
					+ String.valueOf(eventVisitIds.get(iX)));
			visitsObj.put("value", hideFlag);
			jsVisits.put(visitsObj);

		}
		jsObj.put("visitsHideFlag", jsVisits);
	}
	//Yk: Added for Enhancement :PCAL-19743 -Starts
	//ArrayList eventVisitIdArray = new ArrayList();
	//ArrayList eventVisitCombinations = new ArrayList();

	/*for (int iX = 0; iX < newEventIds.size(); iX++) {

		if (calledFrom.equals("S")) {

			offLineFlag = ((eventOfflines.get(iX)) == null) ? ""
					: (eventOfflines.get(iX)).toString();

		}
		//YK : Modified for Bug #7513 
		if (((Integer) eventIds.get(iX)).intValue() == 0) {
			continue;
		}

		HashMap eventVisitIdMap = new HashMap();
		eventVisitIdMap.put("event", (String) eventNames.get(iX));
		eventVisitIdMap.put("eventId", String.valueOf(eventIds.get(iX))
				+ "V" + (Integer) eventVisitIds.get(iX));
		eventVisitIdMap.put("key", "e"
				+ String.valueOf(newEventIds.get(iX)));
		eventVisitIdMap.put("value", "e"
				+ String.valueOf(newEventIds.get(iX)) + "v"
				+ eventVisitIds.get(iX) + "f" + offLineFlag);
		eventVisitIdArray.add(eventVisitIdMap);

		eventVisitCombinations.add(String.valueOf(eventIds.get(iX))
				+ "V" + (Integer) eventVisitIds.get(iX));
	}*/

	ArrayList eventNameArray = new ArrayList();
	ArrayList eventCategoryArray = new ArrayList();
	for (int iX = 0; iX < newEventIds.size(); iX++) {
		//YK : Modified for Bug #7513 
		HashMap eventNameMap = new HashMap();
		HashMap eventCatMap = new HashMap();
		if (((Integer) eventIds.get(iX)).intValue() == 0) {
			continue;
		}
		eventNameMap.put("event", (String) eventNames.get(iX));
		eventNameMap.put("eventId", String.valueOf(eventIds.get(iX)));
		eventNameMap.put("key", "e"
				+ String.valueOf(newEventIds.get(iX)));
		eventCatMap.put("category",eventCatNames.get(iX));
		eventNameMap.put("category", eventCatNames.get(iX));
		eventNameArray.add(eventNameMap);
		eventCategoryArray.add(eventCatMap);
	}

	//Yk: Modified for Enhancement :PCAL-19743 - Ends
	jsObj.put("events", jsEvents);
	jsObj.put("protocolName", protocolName);
	jsObj.put("calStatus", calStatus);
	jsObj.put("noOfVisits", noOfVisits);
	jsObj.put("totalEventRecords",totalEventRecords);
	jsObj.put("totalVisitRecords",totalVisitRecords);


	JSONArray jsColArray = new JSONArray();
	{
		JSONObject jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "event");
		jsObjTemp1.put("label", LC.L_Event/*Event*****/);
		jsColArray.put(jsObjTemp1);
		JSONObject jsObjTemp2 = new JSONObject();
		jsObjTemp2.put("key", "eventId");
		jsObjTemp2.put("label", LC.L_EventId/*Event ID*****/);
		jsColArray.put(jsObjTemp2);
	}
	JSONArray jsDisplacements = new JSONArray();
	for (int iX = 0; iX < noOfVisits; iX++) {
		JSONObject jsObj1 = new JSONObject();
		jsObj1.put("key", "v" + String.valueOf(visitIds.get(iX)));
		jsObj1.put("value", visitDisplacements.get(iX));
		jsDisplacements.put(jsObj1);
		JSONObject jsObjTemp = new JSONObject();
		jsObjTemp.put("key", "v" + (visitIds.get(iX)));
		StringBuffer sb1 = new StringBuffer();
		String vstName=(String) visitNames.get(iX);
		if(vstName.length()>5){
	    	String vsitName=vstName.substring(0,5);
		sb1.append("<span onmouseover=\"return overlib('" + vstName + "',CAPTION,'" + LC.L_Visit_Name	+ "');\" ");
		sb1.append(" onmouseout=\"return nd();\">"+vsitName+"...</span>");
		}
		else{
		sb1.append("<span>"+vstName+"</span>");
		}
		sb1.append(" <input type='checkbox' title=\"" + LC.L_Edit
				+ "\" ");
		sb1.append("name='").append("all_v").append(visitIds.get(iX))
				.append("'");
		sb1.append("id='").append("all_v").append(visitIds.get(iX))
				.append("'");
		sb1.append(" onclick='VELOS.dataGrid.checkAll(\"v").append(
				visitIds.get(iX)).append("\");'");
		sb1.append("> ");
		sb1
				.append("&nbsp;<img src=\"./images/edit1.gif\" alt=\"\" style=\"cursor:pointer\" class=\"headerImage\" "); ////YK: Added for PCAL-20461 - Adds edit image with each visit in the YUI datagrid 
		sb1.append(" onclick='loadModifySequenceDialog(\"").append(
				calStatus).append("\", \"").append(pgRight).append(
				"\",\"").append(calledFrom).append("\", \"").append(
				protocolId).append("\",\"").append(calassoc).append(
				"\",\"").append(visitIds.get(iX)).append("\");'");
		/********************** Remove Mouse functionality on pencil to Label****************************************/
//		sb1.append(" onmouseover=\"return overlib('"
//				+ MC.M_ClkChgSeq_HideEvt/*Click to change sequence and hide/unhide Events*****/
//				+ "',CAPTION,'" + LC.L_EdtVst_Evt/*Edit Visit Events*****/
//				+ "');\" ");
//		sb1.append(" onmouseout=\"return nd();\" ");
		/********************** Remove Mouse functionality on pencil to Label****************************************/
		sb1.append("/> ");
		jsObjTemp.put("label", sb1.toString());
		jsObjTemp.put("seq", "v" + (iX + 1));
		jsColArray.put(jsObjTemp);
	}
	jsObj.put("displacements", jsDisplacements);
	jsObj.put("colArray", jsColArray);

	if (eventOfflines != null) {
		JSONArray offlineKeys = new JSONArray();
		JSONArray offlineValues = new JSONArray();
		for (int iX = 0; iX < eventOfflines.size(); iX++) {
			if (((Integer) eventIds.get(iX)).intValue() == 0
					|| ((Integer) eventVisitIds.get(iX)).intValue() == 0
					|| eventOfflines.get(iX) == null) {
				continue;
			}
			JSONObject jsObjTemp1 = new JSONObject();
			jsObjTemp1.put("key", "e" + eventIds.get(iX) + "v"
					+ eventVisitIds.get(iX));
			offlineKeys.put(jsObjTemp1);
			JSONObject jsObjTemp2 = new JSONObject();
			jsObjTemp2.put("value", eventOfflines.get(iX));
			offlineValues.put(jsObjTemp2);
		}
		jsObj.put("offlineKeys", offlineKeys);
		jsObj.put("offlineValues", offlineValues);
	}

	ArrayList dataList = new ArrayList();
	for (int iX = 0; iX < eventIds.size(); iX++) {
		if (calledFrom.equals("S")) {

			offLineFlag = ((eventOfflines.get(iX)) == null) ? ""
					: (eventOfflines.get(iX)).toString();

		}
		// Implement mouseover for #23812 bug   'onmouseout='return nd()' onmouseover='return VELOS.dataGrid.getEventMouseOver("+newEventIds.get(iX)+")' '
		String img= "<img  onclick=\"openEvtDetailsPopUp(\'eventdetails.jsp?eventmode=M&amp;protocolId="+protocolId+"&amp;calledFrom="+calledFrom+"&amp;calStatus="+calStatus+"&amp;fromPage=fetchProt&amp;selectedTab=1&amp;calassoc="+calassoc+"&amp;eventId="+newEventIds.get(iX)+"&amp;"+eUrl+"&amp;offLineFlag="+offLineFlag+"')\" src='./images/edit1.gif' name='e"+newEventIds.get(iX)+"' onmouseout='return nd()' onmouseover='return VELOS.dataGrid.getEventMouseOver("+newEventIds.get(iX)+")'>";
		if (((Integer) eventIds.get(iX)).intValue() < 1) {
			continue;
		}
		if (dataList.size() == 0) {
			HashMap map1 = new HashMap();
			map1.put("event", (String) eventNames.get(iX));
			map1.put("category", eventCatNames.get(iX));
			map1.put("eventId", eventIds.get(iX));
			map1.put("cost", Integer.valueOf((String)costs.get(iX)));
			if(((Integer)newEventIds.get(iX))>0)
				map1.put("v" + eventVisitIds.get(iX), img);
			else
				map1.put("v" + eventVisitIds.get(iX), "Y");
			dataList.add(map1);
		} else {
			HashMap map1 = (HashMap) dataList.get(dataList.size() - 1);
			if (((Integer) map1.get("eventId")).intValue() == ((Integer) eventIds
					.get(iX)).intValue()) {
				map1.put("v" + eventVisitIds.get(iX), img);
				dataList.set(dataList.size() - 1, map1);
			} else {
				HashMap map2 = new HashMap();
				map2.put("event", (String) eventNames.get(iX));
				map2.put("category", eventCatNames.get(iX));
				map2.put("eventId", eventIds.get(iX));
				map2.put("v" + eventVisitIds.get(iX), img);
				map2.put("cost", Integer.valueOf((String)costs.get(iX)));
				dataList.add(map2);
			}
		}
	}
	//Yk: Modified for Enhancement :PCAL-19743
	// YK : Added for Bug#7470
	if (!sortOrder.equalsIgnoreCase("undefined")
			&& !sortOrder.equals("")) {
		if("insert_order".equals(sortOrder))
		{
			dataList = eventassocB.sortListByPk(dataList,
			"eventId");	
		} else{
		sortOrder = (sortOrder.equals("asc") ? sortOrder : "desc");
		if (sortOrder.equalsIgnoreCase("desc")) {
			dataList = eventassocB.sortListByPkInt(dataList, "cost");
		}
		
		else {
			dataList = eventassocB.sortListByPkInt(dataList, "cost");
		}
	}
	}
	JSONArray jsEventCat = new JSONArray();
	for (int iX = 0; iX < dataList.size(); iX++) {
		jsEventCat.put(EJBUtil.hashMapToJSON((HashMap) eventCategoryArray
				.get(iX)));
	}
	JSONArray jsEventVisits = new JSONArray();
	for (int iX = 0; iX < dataList.size(); iX++) {
		jsEventVisits.put(EJBUtil.hashMapToJSON((HashMap) dataList
				.get(iX)));
	}
	//YK : Modified for Bug #7513 
	//JSONArray jsEventVisitIds = new JSONArray();
	/*for (int iX = 0; iX < eventVisitIdArray.size(); iX++) {
		jsEventVisitIds.put(EJBUtil
				.hashMapToJSON((HashMap) eventVisitIdArray.get(iX)));
	}*/
	JSONArray jsEventName = new JSONArray();
	for (int iX = 0; iX < eventNameArray.size(); iX++) {
		jsEventName.put(EJBUtil.hashMapToJSON((HashMap) eventNameArray
				.get(iX)));
	}

	jsObj.put("dataArray", jsEventVisits);
	jsObj.put("eventNames", jsEventName);
	jsObj.put("eventVisitIds", "");
	jsObj.put("eventCat", jsEventCat);
	jsObj.put("eventVisitCombinations", "");
    return jsObj.toString();
    }
    
    public String fetchProtJSON(String protocolId, String calledFrom,String calassoc,String pgRight,String sortOrder,int initEventSize,int limitEventSize,int initVisitSize,int limitVisitSize,String eventName) 
    throws JSONException {	
    JSONObject jsObj = new JSONObject();
    boolean isLibCal = false;
	boolean isStudyCal = false;
	String protocolName = null;
	String calStatus = null;
	int errorCode = 0;
	ArrayList eventIds = null, eventNames = null;
	ArrayList eventVisitIds = null;
	ArrayList eventOfflines = null;
	ArrayList cost=null;
	ArrayList visithideFlags = null;//YK: Fix for Bug#7264
	//Yk: Added for Enhancement :PCAL-19743
	ArrayList newEventIds = null;
	   int totalEventRecords=0;
       int totalVisitRecords=0;
       
	if ("P".equals(calledFrom) || "L".equals(calledFrom)) {
		isLibCal = true;
	} else if ("S".equals(calledFrom)) {
		isStudyCal = true;
	} else {
		jsObj.put("error", new Integer(-3));
		jsObj.put("errorMsg", MC.M_CalType_Miss/*Calendar Type is missing.*****/);
		return jsObj.toString();
	}

	ArrayList visitIds = null;
	ProtVisitDao visitdao = new ProtVisitDao();
	ProtVisitJB protVisitB = new ProtVisitJB();
	visitdao = protVisitB.getProtocolVisits(EJBUtil.stringToNum(protocolId),initVisitSize,limitVisitSize);
	totalVisitRecords = visitdao.getTotalRecords();

	visitIds = visitdao.getVisit_ids();
	

  String visitIds1= visitIds.toString().replace("[", "").replace("]", "");
	String calStatusPk = "";
	SchCodeDao scho = new SchCodeDao();
	EventdefJB eventdefB = new EventdefJB();
	EventAssocJB eventassocB = new EventAssocJB();
	if (isLibCal) {
		EventdefDao defdao=null;
		
		 defdao = eventdefB.getProtSelectedAndVisitEvents(
				EJBUtil.stringToNum(protocolId), initEventSize,
				initVisitSize, limitEventSize, limitVisitSize,visitIds1,eventName);
		 totalEventRecords = defdao.getTotalRecords();
	
		 eventIds = defdao.getEvent_ids();
			eventVisitIds = defdao.getFk_visit();
			newEventIds = defdao.getNew_event_ids();
			cost=defdao.getCosts();
			eventNames = defdao.getNames();
			eventdefB.setEvent_id(EJBUtil.stringToNum(protocolId));
			eventdefB.getEventdefDetails();
			protocolName = eventdefB.getName();
       
		//KM-#DFin9
		calStatusPk = eventdefB.getStatCode();
		calStatus = scho.getCodeSubtype(
				EJBUtil.stringToNum(calStatusPk)).trim();
		
	} else if (isStudyCal) {
		EventAssocDao assocdao=null;
		 assocdao = eventassocB
				.getProtSelectedAndVisitEvents(
						EJBUtil.stringToNum(protocolId), initEventSize,
						initVisitSize, limitEventSize, limitVisitSize,visitIds1,eventName);
		 totalEventRecords = assocdao.getTotalRecords();
		
		eventIds = assocdao.getEvent_ids();
		newEventIds = assocdao.getNew_event_ids();
		eventVisitIds = assocdao.getEventVisits();
		cost=assocdao.getCosts();
		eventNames = assocdao.getNames();
		eventOfflines = assocdao.getOfflineFlags(); // Unique to event_assoc, not in event_def
		eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
		eventassocB.getEventAssocDetails();
		protocolName = eventassocB.getName();
		visithideFlags = assocdao.getHideFlags();//YK: Fix for Bug#7264
		//KM-#DFin9
		calStatusPk = eventassocB.getStatCode();
		calStatus = scho.getCodeSubtype(
				EJBUtil.stringToNum(calStatusPk)).trim();
       // totalEventRecords = assocdao.getTotalRecords();

	}

	ArrayList visitNames = null;
	ArrayList visitDisplacements = null;
	String visitId = null;
	String visitName = null;
	String offLineFlag = ""; //Yk: Added for Enhancement :PCAL-19743
	
	
	visitNames = visitdao.getNames();
	visitDisplacements = visitdao.getDisplacements();

	int noOfEvents = eventIds == null ? 0 : eventIds.size();
	if (protocolName == null) {
		protocolName = "";
	}
	int noOfVisits = visitIds == null ? 0 : visitIds.size();

	jsObj.put("error", errorCode);
	jsObj.put("errorMsg", "");
	jsObj.put("noOfEvents", noOfEvents);

	JSONArray jsEvents = new JSONArray();
	for (int iX = 0; iX < eventIds.size(); iX++) {
		JSONObject jsObj1 = new JSONObject();
		jsObj1
				.put(String.valueOf(eventIds.get(iX)), eventNames
						.get(iX));
		jsEvents.put(jsObj1);
	}
	//YK: Fix for Bug#7264
	JSONArray jsVisits = new JSONArray();
	if (isStudyCal) {
		for (int iX = 0; iX < eventIds.size(); iX++) {
			JSONObject visitsObj = new JSONObject();
			if (((Integer) eventIds.get(iX)).intValue() != 0) {
				continue;
			}
			String hideFlag = visithideFlags.get(iX) == null ? "0"
					: (String) visithideFlags.get(iX);
			visitsObj.put("key", "v"
					+ String.valueOf(eventVisitIds.get(iX)));
			visitsObj.put("value", hideFlag);
			jsVisits.put(visitsObj);

		}
		jsObj.put("visitsHideFlag", jsVisits);
	}
	//Yk: Added for Enhancement :PCAL-19743 -Starts
	ArrayList eventVisitIdArray = new ArrayList();
	ArrayList eventVisitCombinations = new ArrayList();

	for (int iX = 0; iX < newEventIds.size(); iX++) {

		if (calledFrom.equals("S")) {

			offLineFlag = ((eventOfflines.get(iX)) == null) ? ""
					: (eventOfflines.get(iX)).toString();

		}
		//YK : Modified for Bug #7513 
		if (((Integer) eventIds.get(iX)).intValue() == 0) {
			continue;
		}

		HashMap eventVisitIdMap = new HashMap();
		eventVisitIdMap.put("event", (String) eventNames.get(iX));
		eventVisitIdMap.put("eventId", String.valueOf(eventIds.get(iX))
				+ "V" + (Integer) eventVisitIds.get(iX));
		eventVisitIdMap.put("key", "e"
				+ String.valueOf(newEventIds.get(iX)));
		eventVisitIdMap.put("value", "e"
				+ String.valueOf(newEventIds.get(iX)) + "v"
				+ eventVisitIds.get(iX) + "f" + offLineFlag);
		eventVisitIdArray.add(eventVisitIdMap);

		eventVisitCombinations.add(String.valueOf(eventIds.get(iX))
				+ "V" + (Integer) eventVisitIds.get(iX));
	}

	ArrayList eventNameArray = new ArrayList();
	for (int iX = 0; iX < newEventIds.size(); iX++) {
		//YK : Modified for Bug #7513 
		HashMap eventNameMap = new HashMap();
		if (((Integer) eventIds.get(iX)).intValue() == 0) {
			continue;
		}
		eventNameMap.put("event", (String) eventNames.get(iX));
		eventNameMap.put("eventId", String.valueOf(eventIds.get(iX)));
		eventNameMap.put("key", "e"
				+ String.valueOf(newEventIds.get(iX)));
		eventNameArray.add(eventNameMap);
	}

	//Yk: Modified for Enhancement :PCAL-19743 - Ends
	jsObj.put("events", jsEvents);
	jsObj.put("protocolName", protocolName);
	jsObj.put("calStatus", calStatus);
	jsObj.put("noOfVisits", noOfVisits);
	jsObj.put("totalEventRecords",totalEventRecords);
	jsObj.put("totalVisitRecords",totalVisitRecords);


	JSONArray jsColArray = new JSONArray();
	{
		JSONObject jsObjTemp1 = new JSONObject();
		jsObjTemp1.put("key", "event");
		jsObjTemp1.put("label", LC.L_Event/*Event*****/);
		jsColArray.put(jsObjTemp1);
		JSONObject jsObjTemp2 = new JSONObject();
		jsObjTemp2.put("key", "eventId");
		jsObjTemp2.put("label", LC.L_EventId/*Event ID*****/);
		jsColArray.put(jsObjTemp2);
	}
	JSONArray jsDisplacements = new JSONArray();
	for (int iX = 0; iX < noOfVisits; iX++) {
		JSONObject jsObj1 = new JSONObject();
		jsObj1.put("key", "v" + String.valueOf(visitIds.get(iX)));
		jsObj1.put("value", visitDisplacements.get(iX));
		jsDisplacements.put(jsObj1);
		JSONObject jsObjTemp = new JSONObject();
		jsObjTemp.put("key", "v" + (visitIds.get(iX)));
		StringBuffer sb1 = new StringBuffer((String) visitNames.get(iX));
		sb1.append(" <input type='checkbox' title=\"" + LC.L_Edit
				+ "\" ");
		sb1.append("name='").append("all_v").append(visitIds.get(iX))
				.append("'");
		sb1.append("id='").append("all_v").append(visitIds.get(iX))
				.append("'");
		sb1.append(" onclick='VELOS.dataGrid.checkAll(\"v").append(
				visitIds.get(iX)).append("\");'");
		sb1.append("> ");
		sb1
				.append("&nbsp;<img src=\"./images/edit1.gif\" alt=\"\" style=\"cursor:pointer\" class=\"headerImage\" "); ////YK: Added for PCAL-20461 - Adds edit image with each visit in the YUI datagrid 
		sb1.append(" onclick='loadModifySequenceDialog(\"").append(
				calStatus).append("\", \"").append(pgRight).append(
				"\",\"").append(calledFrom).append("\", \"").append(
				protocolId).append("\",\"").append(calassoc).append(
				"\",\"").append(visitIds.get(iX)).append("\");'");
		/********************** Remove Mouse functionality on pencil to Label****************************************/
//		sb1.append(" onmouseover=\"return overlib('"
//				+ MC.M_ClkChgSeq_HideEvt/*Click to change sequence and hide/unhide Events*****/
//				+ "',CAPTION,'" + LC.L_EdtVst_Evt/*Edit Visit Events*****/
//				+ "');\" ");
//		sb1.append(" onmouseout=\"return nd();\" ");
		/********************** Remove Mouse functionality on pencil to Label****************************************/
		sb1.append("/> ");
		jsObjTemp.put("label", sb1.toString());
		jsObjTemp.put("seq", "v" + (iX + 1));
		jsColArray.put(jsObjTemp);
	}
	jsObj.put("displacements", jsDisplacements);
	jsObj.put("colArray", jsColArray);

	if (eventOfflines != null) {
		JSONArray offlineKeys = new JSONArray();
		JSONArray offlineValues = new JSONArray();
		for (int iX = 0; iX < eventOfflines.size(); iX++) {
			if (((Integer) eventIds.get(iX)).intValue() == 0
					|| ((Integer) eventVisitIds.get(iX)).intValue() == 0
					|| eventOfflines.get(iX) == null) {
				continue;
			}
			JSONObject jsObjTemp1 = new JSONObject();
			jsObjTemp1.put("key", "e" + eventIds.get(iX) + "v"
					+ eventVisitIds.get(iX));
			offlineKeys.put(jsObjTemp1);
			JSONObject jsObjTemp2 = new JSONObject();
			jsObjTemp2.put("value", eventOfflines.get(iX));
			offlineValues.put(jsObjTemp2);
		}
		jsObj.put("offlineKeys", offlineKeys);
		jsObj.put("offlineValues", offlineValues);
	}

	ArrayList dataList = new ArrayList();
	for (int iX = 0; iX < eventIds.size(); iX++) {
		if (((Integer) eventIds.get(iX)).intValue() < 1) {
			continue;
		}
		if (dataList.size() == 0) {
			HashMap map1 = new HashMap();
			map1.put("event", (String) eventNames.get(iX));
			map1.put("eventId", eventIds.get(iX));
			map1.put("cost", Integer.valueOf((String)cost.get(iX)));
			map1.put("v" + eventVisitIds.get(iX), "Y");
			dataList.add(map1);
		} else {
			HashMap map1 = (HashMap) dataList.get(dataList.size() - 1);
			if (((Integer) map1.get("eventId")).intValue() == ((Integer) eventIds
					.get(iX)).intValue()) {
				map1.put("v" + eventVisitIds.get(iX), "Y");
				dataList.set(dataList.size() - 1, map1);
			} else {
				HashMap map2 = new HashMap();
				map2.put("event", (String) eventNames.get(iX));
				map2.put("eventId", eventIds.get(iX));
				map2.put("v" + eventVisitIds.get(iX), "Y");
				map2.put("cost", Integer.valueOf((String)cost.get(iX)));
				dataList.add(map2);
			}
		}
	}

	//Yk: Modified for Enhancement :PCAL-19743
	// YK : Added for Bug#7470
	if (!sortOrder.equalsIgnoreCase("undefined")
			&& !sortOrder.equals("")) {
		if("insert_order".equals(sortOrder))
		{
			dataList = eventassocB.sortListByPk(dataList,
			"eventId");	
		}else{
		sortOrder = (sortOrder.equals("asc") ? sortOrder : "desc");
		if (sortOrder.equalsIgnoreCase("desc")) {
			dataList = eventassocB.sortListByPkInt(dataList,
					"cost");
		}
		
		else {
			dataList = eventassocB.sortListByPkInt(dataList, "cost");
		}
	}
	}

	JSONArray jsEventVisits = new JSONArray();
	for (int iX = 0; iX < dataList.size(); iX++) {
		jsEventVisits.put(EJBUtil.hashMapToJSON((HashMap) dataList
				.get(iX)));
	}
	//YK : Modified for Bug #7513 
	JSONArray jsEventVisitIds = new JSONArray();
	for (int iX = 0; iX < eventVisitIdArray.size(); iX++) {
		jsEventVisitIds.put(EJBUtil
				.hashMapToJSON((HashMap) eventVisitIdArray.get(iX)));
	}
	JSONArray jsEventName = new JSONArray();
	for (int iX = 0; iX < eventNameArray.size(); iX++) {
		jsEventName.put(EJBUtil.hashMapToJSON((HashMap) eventNameArray
				.get(iX)));
	}

	jsObj.put("dataArray", jsEventVisits);
	jsObj.put("eventNames", jsEventName);
	jsObj.put("eventVisitIds", jsEventVisitIds);
	jsObj.put("eventVisitCombinations", eventVisitCombinations);
    
        return jsObj.toString();
    }

}
