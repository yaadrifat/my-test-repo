/*
 * Classname 			:	EventKitJB
 * 
 * Version information 	: 	1.0
 *
 * Date 				:	05/09/2008
 * 
 * Copyright notice		: 	Velos Inc
 * 
 * Author				:	Sonia Abrol
 */

package com.velos.esch.web.eventKit;

/* IMPORT STATEMENTS */

//import com.velos.esch.business.common.CrfDao;
import java.util.Hashtable;
import com.velos.esch.business.common.EventKitDao;
import com.velos.esch.business.eventKit.impl.EventKitBean;
import com.velos.esch.service.eventKitAgent.EventKitAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for Event Kit module.  Acts as a facade over Service layer
 * 
 */

public class EventKitJB {

    /**
     * Event KIT Id
     */
    private int eventKitId;

    /**
     * FK to ER_STORAGE to identify teh storage KIT record
     */
    private String eventStorageKit;

    /**
     * FK_Event
     */
    private int fkEvent;

     /**
     * Event CRF creator
     */
    private String creator;

    /**
     * Event CRF last modified by
     */
    private String lastModifiedBy;

    /**
     * IP address
     */
    private String ipAdd;

    // GETTER METHODS

    public int getEventKitId() {
        return this.eventKitId;
    }
    
    public String getEventStorageKit() {
        return this.eventStorageKit;
    }
    
    public int getFkEvent() {
        return this.fkEvent;
    }
    
        
    
    
    // SETTER METHODS

    public void setEventKitId(int eventKitId) {
        this.eventKitId = eventKitId;
    }

    public void setEventStorageKi(String fkkit) {
        this.eventStorageKit = fkkit;
    }

    public void setFkEvent(int fkEvent) {
        this.fkEvent = fkEvent;
    }

  
    
    /**
     * @return The Id of the creator of the record
     */
    
    
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creatorId) {
       
            this.creator = creatorId;
       
    }

    /**
     * @return The Id of the user modified this record
     */
    
    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
   
    public void getLastModifiedBy(String eventKitLastModBy) {
        
            this.lastModifiedBy = eventKitLastModBy;
      
    }

    /**
     * @return IP Address
     */
     
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String eventKitIPAdd) {
        this.ipAdd = eventKitIPAdd;
    }
 
    
    // END OF GETTER SETTER METHODS

    
    
    /**
     * Defines an eventKitJB object with the specified eventKit Id
     */
    public EventKitJB(int eventKitId) {
        setEventKitId(eventKitId);
    }

    /**
     * Defines an eventKitJB object with default values for fields
     */
    public EventKitJB() {
        Rlog.debug("eventKit", "eventKitJB.eventKitJB() ");
    };

    /**
     * Defines an CatLibJB object with the specified values for the fields
     * 
     */
    public EventKitJB(int eventKitId, String eventStorageKit, int fkEvent,
             String eventKitCreator,
            String eventKitLastModBy, String eventKitIPAdd) {
    		setEventKitId(eventKitId);
    		setEventStorageKit(eventStorageKit);
    		setFkEvent(fkEvent);
    	 
    		setCreator(eventKitCreator);
    		setLastModifiedBy(eventKitLastModBy);
    		setIpAdd(eventKitIPAdd);
    		
    }

    /**
     * Retrieves the details of an Event Kit in eventKitStateKeeper Object
     * 
     * @return EventKitBean consisting of the eventKit details
     * @see EventKitBean
     */
    public EventKitBean getEventKitDetails() {
        EventKitBean ecsk = null;
        try {
            EventKitAgentRObj eventKitAgentRObj = EJBUtil.getEventKitAgentHome();
            ecsk = eventKitAgentRObj.getEventKitDetails(this.eventKitId);
            
        } catch (Exception e) {
            Rlog
                    .fatal("eventKit", "Error in getEventKitDetails() in eventKitJB "
                            + e);
        }
        if (ecsk != null) {
            this.eventKitId = ecsk.getEventKitId();
            this.eventStorageKit = ecsk.getEventStorageKit();
            this.fkEvent = ecsk.getFkEvent();
          
            this.creator = ecsk.getCreator();
            this.lastModifiedBy = ecsk.getLastModifiedBy();
            this.ipAdd = ecsk.getIpAdd();
           
        }
        return ecsk;
    }

    public int setEventKitDetails() {
        int output = 0;
        try {
            EventKitAgentRObj eventKitAgentRObj = EJBUtil.getEventKitAgentHome();
            output = eventKitAgentRObj.setEventKitDetails(this
                    .createEventKitStateKeeper());
            this.setEventKitId(output);
             

        } catch (Exception e) {
            Rlog.fatal("eventKit", "Exception in setEventKitDetails() in eventKitJB "
                            + e);
            return -2;

        }
        return output;
    }
    
    public int setEventKitDetails(String[] storagePks) {
        int output = 0;
        try {
            EventKitAgentRObj eventKitAgentRObj = EJBUtil.getEventKitAgentHome();
            output = eventKitAgentRObj.setEventKitDetails(this
                    .createEventKitStateKeeper(),storagePks);
           
             return output;

        } catch (Exception e) {
            Rlog.fatal("eventKit", "Exception in setEventKitDetails(String[] storagePks) in eventKitJB "
                            + e);
            return -2;

        }
        
    }
    
    /**
     * Saves the modified details of the event kit  to the database
     * 
     * @return 0 - If update succeeds <BR>
     *         -2 - If update fails
     */
    
    public int updateEventKit() {
        int output;
        try {
        	EventKitAgentRObj eventKitAgentRObj = EJBUtil.getEventKitAgentHome();
        	
            output = eventKitAgentRObj.updateEventKit(this
                    .createEventKitStateKeeper());
        } catch (Exception e) {
            Rlog.debug("eventKit",
                    "EXCEPTION IN SETTING eventKit DETAILS TO  SESSION BEAN"
                            + e.getMessage());

            return -2;
        }
        return output;
    }
    

    /**
     * Uses the values of the current eventKitJB object to create an
     * EventKitBean Object
     * 
     * @return EventKitBean object with all the details of the eventKit
     * @see EventKitBean
     */
    public EventKitBean createEventKitStateKeeper() {
        
    	        
        return new EventKitBean(this.eventKitId, this.eventStorageKit, this.fkEvent,
                 this.creator,
                this.lastModifiedBy, this.ipAdd);
    }
     
    
    public void deleteEventKit(int eventId){
         try {
         	EventKitAgentRObj eventKitAgentRObj = EJBUtil.getEventKitAgentHome();
         	eventKitAgentRObj.deleteEventKit(eventId);
         } catch (Exception e) {
             Rlog.debug("eventKitJb",
                     "Exception in deleteEventKit "
                             + e.getMessage());
         }
    }

    public int deleteEventKitRecord(int eventKitId)
    {
    try {
     	EventKitAgentRObj eventKitAgentRObj = EJBUtil.getEventKitAgentHome();
     	return eventKitAgentRObj.deleteEventKitRecord(eventKitId);
     } catch (Exception e) {
         Rlog.debug("eventKitJb",
                 "Exception in deleteEventKitRecord "
                         + e.getMessage());
         return -2;
     }
}

    //Overloaded for INF-18183 ::: Ankit
    public int deleteEventKitRecord(int eventKitId, Hashtable<String, String> argsvalues)
    {
    try {
     	EventKitAgentRObj eventKitAgentRObj = EJBUtil.getEventKitAgentHome();
     	return eventKitAgentRObj.deleteEventKitRecord(eventKitId, argsvalues);
     } catch (Exception e) {
         Rlog.debug("eventKitJb",
                 "Exception in deleteEventKitRecord "
                         + e.getMessage());
         return -2;
     }
}

    
	public void setEventStorageKit(String eventStorageKit) {
		this.eventStorageKit = eventStorageKit;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
    
	
	  public EventKitDao getEventStorageKits(int eventId ){
		  EventKitDao evd = new EventKitDao();
         try {
         	EventKitAgentRObj eventKitAgentRObj = EJBUtil.getEventKitAgentHome();
         	evd = eventKitAgentRObj.getEventStorageKits(eventId);
         	
         } catch (Exception e) {
             Rlog.debug("eventKitJb",
                     "Exception in getEventStorageKits "
                             + e.getMessage());
         }
         
         return evd;
    }
	 
 
}
