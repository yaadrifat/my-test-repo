/*
 * Classname : EventdocJB
 * 
 * Version information: 1.0 
 *
 * Date: 07/04/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana 
 */

package com.velos.esch.web.eventdoc;

/* IMPORT STATEMENTS */

import java.util.Hashtable;
import com.velos.esch.business.common.EventdefDao;
import com.velos.esch.business.eventdoc.impl.EventdocBean;
import com.velos.esch.service.eventdocAgent.EventdocAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

public class EventdocJB {

    /**
     * docId
     */
    private int docId;

    /**
     * userId
     */
    private String docName;

    /**
     * docDesc
     */
    private String docDesc;

    /**
     * docValue
     */
    // private byte[] docValue;
    /**
     * docSize
     */
    public String docSize;

    /**
     * doc type
     */
    private String docType;

    /**
     * doc type
     */
    private String eventId;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;
    
    private String networkId;
    
    private String networkFlag;
    
    private String docVersion;

    // Start of Getter setter methods

    public int getDocId() {
        return this.docId;
    }

    public void setDocId(int docId) {
        this.docId = docId;
    }

    public String getDocName() {
        return this.docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public String getDocDesc() {
        return this.docDesc;
    }

    public void setDocDesc(String docDesc) {
        this.docDesc = docDesc;
    }

    public String getDocSize() {
        return this.docSize;
    }

    public void setDocSize(String docSize) {
        this.docSize = docSize;
    }
    
    public String getDocVersion(){
    	return this.docVersion;
    }
    
    public void setDocVersion(String docVersion){
    	this.docVersion = docVersion;
    }

    /*
     * public byte[] getDocValue() { return this.docValue; }
     * 
     * public void setDocValue(byte[] docValue) { this.docValue = docValue; }
     */

    public String getDocType() {
        return this.docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getEventId() {
        return this.eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
    
    public String getNetworkId() {
        return this.networkId;
    }

    public void setNetworkId(String networkId) {
        this.networkId = networkId;
    }
    public String getNetworkFlag() {
        return this.networkFlag;
    }

    public void setNetworkFlag(String networkFlag) {
        this.networkFlag = networkFlag;
    }


    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Constructor; Accepts Eventdoc Id
     * 
     * @param eventdocId
     *            Id of the Eventdoc
     */

    public EventdocJB(int docId) {
        setDocId(docId);
    }

    /**
     * Default Constructor
     * 
     */
    public EventdocJB() {
        Rlog.debug("eventdoc", "EventdocJB.EventdocJB() ");
    }

    public EventdocJB(int docId, String docName, String docDesc,
            byte[] docValue, String docType, String eventId, String creator,
            String modifiedBy, String ipAdd ,String networkId ,String networkFlag, String docVersion) {
        setDocId(docId);
        setDocName(docName);
        setDocDesc(docDesc);
        setDocSize(docSize);
        // setDocValue(docValue);
        setDocType(docType);
        setEventId(eventId);
        setNetworkId(networkId);
        setNetworkFlag(networkFlag);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setDocVersion(docVersion);

        Rlog.debug("eventdoc", "EventdocJB.EventdocJB(all parameters)");
    }

    /**
     * Calls getEventdocDetails() of Eventdoc Session Bean: EventdocAgentBean
     * 
     * 
     * @return The Details of the eventdoc in the Eventdoc StateKeeper Object
     * @See EventdocAgentBean
     */

    public EventdocBean getEventdocDetails() {
        EventdocBean edsk = null;

        try {

            EventdocAgentRObj eventdocAgent = EJBUtil.getEventdocAgentHome();
            edsk = eventdocAgent.getEventdocDetails(this.docId);
            Rlog.debug("eventdoc",
                    "EventdocJB.getEventdocDetails() EventdocStateKeeper "
                            + edsk);
        } catch (Exception e) {
            Rlog.debug("eventdoc",
                    "Error in getEventdocDetails() in EventdocJB " + e);
        }
        if (edsk != null) {
            this.docId = edsk.getDocId();
            this.docName = edsk.getDocName();
            this.docDesc = edsk.getDocDesc();
            // this.docValue=edsk.getDocValue();
            this.docSize = edsk.getDocSize();
            this.docType = edsk.getDocType();
            this.eventId = edsk.getEventId();
            this.creator = edsk.getCreator();
            this.modifiedBy = edsk.getModifiedBy();
            this.ipAdd = edsk.getIpAdd();
            this.networkId = edsk.getNetworkId();
            this.networkFlag = edsk.getNetworkFlag();
            this.docVersion = edsk.getDocVersion();
        }
        return edsk;
    }

    /**
     * Calls setEventdocDetails() of Eventdoc Session Bean: EventdocAgentBean
     * 
     */

    public void setEventdocDetails() {

        try {

            EventdocAgentRObj eventdocAgent = EJBUtil.getEventdocAgentHome();
            this.docId = eventdocAgent.setEventdocDetails(this
                    .createEventdocStateKeeper());
            Rlog.debug("eventdoc", "EventdocJB.setEventdocDetails()");
        } catch (Exception e) {
            Rlog.debug("eventdoc",
                    "Error in setEventdocDetails() in EventdocJB " + e);
        }
    }

    /**
     * Calls updateEventdoc() of Eventdoc Session Bean: EventdocAgentBean
     * 
     * @return
     */
    public int updateEventdoc() {
        int output;
        try {

            EventdocAgentRObj eventdocAgentRObj = EJBUtil
                    .getEventdocAgentHome();
            output = eventdocAgentRObj.updateEventdoc(this
                    .createEventdocStateKeeper());
        } catch (Exception e) {
            Rlog.debug("eventdoc",
                    "EXCEPTION IN SETTING USER DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * Calls updateEventdoc() of eventdefdao.
     * 
     * @return
     */
    public int propagateEventdoc(int protocol_id, String propagateInVisitFlag,
            String propagateInEventFlag, String mode, String calType) {
        int output;
        EventdocBean edsk;
        EventdefDao eventdefdao = new EventdefDao();
        try {
            EventdocAgentRObj EventdocAgentRObj = EJBUtil
                    .getEventdocAgentHome();
            edsk = this.createEventdocStateKeeper();
            output = eventdefdao.propagateEventUpdates(protocol_id, EJBUtil
                    .stringToNum(edsk.getEventId()), "EVENT_DOC", edsk
                    .getDocId(), propagateInVisitFlag, propagateInEventFlag,
                    mode, calType);

        } catch (Exception e) {
            Rlog.debug("Eventdoc",
                    "EXCEPTION IN SETTING USER DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * 
     * 
     * @return the Eventdoc StateKeeper Object with the current values of the
     *         Bean
     */

    public EventdocBean createEventdocStateKeeper() {
        Rlog.debug("eventdoc", "EventdocJB.createEventdocStateKeeper ");
        return new EventdocBean(docId, docName, docDesc, docSize, docType,
                creator, modifiedBy, ipAdd, eventId,networkId,networkFlag,docVersion);
    }

    public int removeEventdoc(int docId, String docType) {
        int output;
        try {

            EventdocAgentRObj eventdocAgentRObj = EJBUtil
                    .getEventdocAgentHome();
            output = eventdocAgentRObj.removeEventdoc(docId, docType);
        } catch (Exception e) {
            Rlog.debug("eventdoc", "EXCEPTION IN DELETING THE CHILD NODE");
            e.printStackTrace();
            return -2;
        }
        return output;
    }
    
    //Overloaded for INF-18183 ::: Ankit
    public int removeEventdoc(int docId, String docType, Hashtable<String, String> argsvalues) {
        int output;
        try {

            EventdocAgentRObj eventdocAgentRObj = EJBUtil
                    .getEventdocAgentHome();
            output = eventdocAgentRObj.removeEventdoc(docId, docType, argsvalues);
        } catch (Exception e) {
            Rlog.debug("eventdoc", "EXCEPTION IN DELETING THE CHILD NODE");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /*
     * generates a String of XML for the eventdoc details entry form.<br><br>
     * NOT IN USE
     */

    public String toXML() {
        Rlog.debug("eventdoc", "EventdocJB.toXML()");
        return "test";
        // to be implemented later
        /*
         * return new String("<?xml version=\"1.0\"?>" + "<?cocoon-process
         * type=\"xslt\"?>" + "<?xml-stylesheet
         * href=\"/stylesheet/form-html.xsl\" type=\"text/xsl\"?>" + "<?xml-stylesheet
         * href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>" + "<head>" + "</head>" + "<input
         * type=\"hidden\" name=\"eventdocId\" value=\"" + this.getEventdocId()+
         * "\" size=\"10\"/>" + "<input type=\"text\"
         * name=\"eventdocCodelstJobtype\" value=\"" +
         * this.getEventdocCodelstJobtype() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"eventdocAccountId\" value=\"" +
         * this.getEventdocAccountId() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"eventdocSiteId\" value=\"" +
         * this.getEventdocSiteId() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"eventdocPerAddressId\" value=\"" +
         * this.getEventdocPerAddressId() + "\" size=\"38\"/>" + "<input
         * type=\"text\" name=\"eventdocLastName\" value=\"" +
         * this.getEventdocLastName() + "\" size=\"30\"/>" + "<input
         * type=\"text\" name=\"eventdocFirstName\" value=\"" +
         * this.getEventdocFirstName() + "\" size=\"30\"/>" + "<input
         * type=\"text\" name=\"eventdocMidName \" value=\"" +
         * this.getEventdocMidName() + "\" size=\"30\"/>" + "<input
         * type=\"text\" name=\"eventdocWrkExp\" value=\"" +
         * this.getEventdocWrkExp() + "\" size=\"100\"/>" + "<input
         * type=\"text\" name=\"eventdocPhaseInv\" value=\"" +
         * this.getEventdocPhaseInv() + "\" size=\"100\"/>" + "<input
         * type=\"text\" name=\"eventdocSessionTime\" value=\"" +
         * this.getEventdocSessionTime() + "\" size=\"3\"/>" + "<input
         * type=\"text\" name=\"eventdocLoginName\" value=\"" +
         * this.getEventdocLoginName() + "\" size=\"20\"/>" + "<input
         * type=\"text\" name=\"eventdocPwd\" value=\"" + this.getEventdocPwd() +
         * "\" size=\"20\"/>" + "<input type=\"text\" name=\"eventdocSecQues\"
         * value=\"" + this.getEventdocSecQues() + "\" size=\"150\"/>" + "<input
         * type=\"text\" name=\"eventdocAnswer\" value=\"" +
         * this.getEventdocAnswer() + "\" size=\"150\"/>" + "<input
         * type=\"text\" name=\"eventdocStatus\" value=\"" +
         * this.getEventdocStatus() + "\" size=\"1\"/>" + "<input type=\"text\"
         * name=\"eventdocCodelstSpl\" value=\"" + this.getEventdocCodelstSpl() +
         * "\" size=\"10\"/>" + "<input type=\"text\"
         * name=\"eventdocGrpDefault\" value=\"" + this.getEventdocGrpDefault() +
         * "\" size=\"10\"/>" + "</form>" );
         */

    }// end of method

}// end of class
