/*
 * Classname : BudgetcalJB
 * 
 * Version information: 1.0 
 *
 * Date: 03/19/2002
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana
 */

package com.velos.esch.web.budgetcal;

/* IMPORT STATEMENTS */

import java.util.Hashtable;
import javax.persistence.Column;

import com.velos.esch.business.budgetcal.impl.BudgetcalBean;
import com.velos.esch.business.common.BudgetcalDao;
import com.velos.esch.service.budgetcalAgent.BudgetcalAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

public class BudgetcalJB {

    /**
     * budgetcalId
     */
    private int budgetcalId;

    /**
     * budgetId
     */
    private String budgetId;

    /**
     * budgetProtId
     */
    private String budgetProtId;

    /**
     * budgetProtType
     */
    private String budgetProtType;

    /**
     * budgetDelFlag
     */
    private String budgetDelFlag;

    /**
     * spOverHead
     */
    private String spOverHead;

    /**
     * clOverHead
     */
    private String clOverHead;

    /**
     * spFlag
     */
    private String spFlag;

    /**
     * clFlag
     */
    private String clFlag;

    /**
     * indirectCost
     */
    private String indirectCost;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    /*
     * Fringe benefit
     */
    private String budgetFrgBenefit;

    /*
     * Fringe Flag
     */
    private String budgetFrgFlag;

    /*
     * Budget discount
     */
    private String budgetDiscount;

    /*
     * Budget discount flag
     */
    private String budgetDiscountFlag;

    /*
     * Exclude SOC Flag
     */
    private String budgetExcldSOCFlag;

    //Added By IA 9/18/2006
    /*
     * Grand Research Total (cost/patient)
     */
    private String budgetResearchCost;

    /*
     * Grand Research Total (cost/patient)
     */
    private String budgetResearchTotalCost;

    /*
     * Grand Research Total (cost/patient)
     */
    private String budgetSOCCost;
    
    /*
     * Grand Research Total (cost/patient)
     */
    private String budgetSOCTotalCost;
    
    /*
     * Grand Research Total (cost/patient)
     */
    private String budgetTotalCost;
    
    /*
     * Grand Research Total (cost/patient)
     */
    private String budgetTotalTotalCost;
    
    /*
     * Grand Research Total (cost/patient)
     */
    private String budgetSalaryCost;
    
    /*
     * Grand Research Total (cost/patient)
     */
    private String budgetSalaryTotalCost;
    
    /*
     * Grand Research Total (cost/patient)
     */
    private String budgetFringeCost;
  
    /*
     * Grand Research Total (cost/patient)
     */
    private String budgetFringeTotalCost;
    
    /*
     * Grand Research Total (cost/patient)
     */
    private String budgetDiscontinueCost;
 
    /*
     * Grand Research Total (cost/patient)
     */
    private String budgetDiscontinueTotalCost;
 
    /*
     * Grand Research Total (cost/patient)
     */
    private String budgetIndirectCost;
    
    /*
     * Grand Research Total (cost/patient)
     */
    private String budgetIndirectTotalCost;
    
    //End Added By IA 9/18/2006
    
    
    //Added by IA 11.03.2006 Comparative Budget
    
    private String budgetResearchSponsorAmount;
    
    /*
     * Grand Research Total SponsorAmount
     */
    
    private String budgetResearchVariance;
    
    
    /*
     * Grand Research Total Variance
     */
    
    private String budgetSOCSponsorAmount;
    
    
    /*
     * Grand SOC Total Sponsor Amount
     */
    
    private String budgetSOCVariance;
    
    
    /*
     * Grand SOC Variance
     */
    
    private String budgetTotalSponsorAmount;
    
    
    /*
     * Grand Total Sponsor Amount
     */
    
    private String budgetTotalVariance;
    
    
    /*
     * Grand Total Variance
     * 
     *      */
    
    
    //end Added 11.03.2006 
    
    
    
    // GETTER SETTER METHODS

    // Start of Getter setter methods

    public int getBudgetcalId() {
        return this.budgetcalId;
    }

    public void setBudgetcalId(int budgetcalId) {
        this.budgetcalId = budgetcalId;
    }

    public String getBudgetId() {
        return this.budgetId;
    }

    public void setBudgetId(String budgetId) {
        this.budgetId = budgetId;
    }

    public String getBudgetProtId() {
        return this.budgetProtId;
    }

    public String getBudgetProtType() {
        return this.budgetProtType;
    }

    public void setBudgetProtType(String budgetProtType) {
        this.budgetProtType = budgetProtType;
    }

    public void setBudgetProtId(String budgetProtId) {
        this.budgetProtId = budgetProtId;
    }

    public String getBudgetDelFlag() {
        return this.budgetDelFlag;
    }

    public void setBudgetDelFlag(String budgetDelFlag) {
        this.budgetDelFlag = budgetDelFlag;
    }

    public String getSpOverHead() {
        return this.spOverHead;
    }

    public void setSpOverHead(String spOverHead) {
        this.spOverHead = spOverHead;
    }

    public String getClOverHead() {
        return this.clOverHead;
    }

    public void setClOverHead(String clOverHead) {
        this.clOverHead = clOverHead;
    }

    public String getSpFlag() {
        return this.spFlag;
    }

    public void setSpFlag(String spFlag) {
        this.spFlag = spFlag;
    }

    public String getClFlag() {
        return this.clFlag;
    }

    public void setClFlag(String clFlag) {
        this.clFlag = clFlag;
    }

    public String getIndirectCost() {
        return this.indirectCost;
    }

    public void setIndirectCost(String indirectCost) {
        this.indirectCost = indirectCost;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public String getBudgetFrgBenefit() {
        return this.budgetFrgBenefit;
    }

    public void setBudgetFrgBenefit(String budgetFrgBenefit) {
        this.budgetFrgBenefit = budgetFrgBenefit;
    }

    public String getBudgetFrgFlag() {
        return this.budgetFrgFlag;
    }

    public void setBudgetFrgFlag(String budgetFrgFlag) {
        this.budgetFrgFlag = budgetFrgFlag;
    }

    public String getBudgetDiscount() {
        return this.budgetDiscount;
    }

    public void setBudgetDiscount(String budgetDiscount) {
        this.budgetDiscount = budgetDiscount;
    }

    public String getBudgetDiscountFlag() {
        return this.budgetDiscountFlag;
    }

    public void setBudgetDiscountFlag(String budgetDiscountFlag) {
        this.budgetDiscountFlag = budgetDiscountFlag;
    }

    public String getBudgetExcldSOCFlag() {
        return this.budgetExcldSOCFlag;
    }

    public void setBudgetExcldSOCFlag(String budgetExcldSOCFlag) {
        this.budgetExcldSOCFlag = budgetExcldSOCFlag;
    }

    
    // ADDED BY IA 9/18/2006
    
    public String getBudgetResearchCost() {
        return this.budgetResearchCost;
    }

    public void setBudgetResearchCost(String budgetResearchCost) {
         this.budgetResearchCost = budgetResearchCost;
    }
    
    
    
    public String getBudgetResearchTotalCost() {
        return this.budgetResearchTotalCost;
     }

    public void setBudgetResearchTotalCost(String budgetResearchTotalCost) {
            this.budgetResearchTotalCost = budgetResearchTotalCost;
      }
    
    public String getBudgetSOCCost() {
        return this.budgetSOCCost;
    }

    public void setBudgetSOCCost(String budgetSOCCost) {
          this.budgetSOCCost = budgetSOCCost;

    }
    
    public String getBudgetSOCTotalCost() {
        return this.budgetSOCTotalCost;
    }

    public void setBudgetSOCTotalCost(String budgetSOCTotalCost) {
            this.budgetSOCTotalCost = budgetSOCTotalCost;
    }
    
    public String getBudgetTotalCost() {
        return this.budgetTotalCost;
    }

    public void setBudgetTotalCost(String budgetTotalCost) {
            this.budgetTotalCost = budgetTotalCost;
    }

    public String getBudgetTotalTotalCost() {
        return this.budgetTotalTotalCost;
    }

    public void setBudgetTotalTotalCost(String budgetTotalTotalCost) {
            this.budgetTotalTotalCost = budgetTotalTotalCost;
    }
    
    public String getBudgetSalaryCost() {
        return this.budgetSalaryCost;
    }

    public void setBudgetSalaryCost(String budgetSalaryCost) {
            this.budgetSalaryCost = budgetSalaryCost;
    }
    
    public String getBudgetSalaryTotalCost() {

        return this.budgetSalaryTotalCost;

    }

    public void setBudgetSalaryTotalCost(String budgetSalaryTotalCost) {
            this.budgetSalaryTotalCost = budgetSalaryTotalCost;
    }
    
    public String getBudgetFringeCost() {
        return this.budgetFringeCost;
    }

    public void setBudgetFringeCost(String budgetFringeCost) {
            this.budgetFringeCost = budgetFringeCost;
    }
    
    
    public String getBudgetFringeTotalCost() {
        return this.budgetFringeTotalCost;
    }

    public void setBudgetFringeTotalCost(String budgetFringeTotalCost) {
            this.budgetFringeTotalCost = budgetFringeTotalCost;
    }
    
    public String getBudgetDiscontinueCost() {
        return this.budgetDiscontinueCost;
    }

    public void setBudgetDiscontinueCost(String budgetDiscontinueCost) {
            this.budgetDiscontinueCost = budgetDiscontinueCost;
    }
    public String getBudgetDiscontinueTotalCost() {
        return this.budgetDiscontinueTotalCost;
    }

    public void setBudgetDiscontinueTotalCost(String budgetDiscontinueTotalCost) {
            this.budgetDiscontinueTotalCost = budgetDiscontinueTotalCost;
    }

    public String getBudgetIndirectCost() {
        return this.budgetIndirectCost;
    }
    public void setBudgetIndirectCost(String budgetIndirectCost) {
            this.budgetIndirectCost = budgetIndirectCost;
    }
    
    public String getBudgetIndirectTotalCost() {
        return this.budgetIndirectTotalCost;
    }

    public void setBudgetIndirectTotalCost(String budgetIndirectTotalCost) {
            this.budgetIndirectTotalCost = budgetIndirectTotalCost;
    }
    
    // END ADDED BY IA 9/18/2006
    
    //Added by IA 11.03.2006 Comparative Budget
    
    public String getBudgetResearchSponsorAmount() {
        return this.budgetResearchSponsorAmount;
    }

    public void setBudgetResearchSponsorAmount(String budgetResearchSponsorAmount) {
            this.budgetResearchSponsorAmount = budgetResearchSponsorAmount;
    }
    
    
    public String getBudgetResearchVariance() {
        return this.budgetResearchVariance;
    }

    public void setBudgetResearchVariance(String budgetResearchVariance) {
            this.budgetResearchVariance = budgetResearchVariance;
    }

    
    public String getBudgetSOCSponsorAmount() {
        return this.budgetSOCSponsorAmount;
    }

    public void setBudgetSOCSponsorAmount(String budgetSOCSponsorAmount) {
            this.budgetSOCSponsorAmount = budgetSOCSponsorAmount;
    }

    public String getBudgetSOCVariance() {
        return this.budgetSOCVariance;
    }

    public void setBudgetSOCVariance (String budgetSOCVariance) {
            this.budgetSOCVariance = budgetSOCVariance;
    }
    
    
    public String getBudgetTotalSponsorAmount() {
        return this.budgetTotalSponsorAmount;
    }

    public void setBudgetTotalSponsorAmount(String budgetTotalSponsorAmount) {
            this.budgetTotalSponsorAmount = budgetTotalSponsorAmount;
    }

    
    public String getBudgetTotalVariance() {
        return this.budgetTotalVariance;
    }

    public void setBudgetTotalVariance(String budgetTotalVariance) {
            this.budgetTotalVariance = budgetTotalVariance;
    }
    
    //End Added 
    
    // END OF GETTER SETTER METHODS

    /**
     * Constructor; Accepts Budgetcal Id
     * 
     * @param budgetcalId
     *            Id of the Budgetcal
     */

    public BudgetcalJB(int budgetcalId) {
        setBudgetcalId(budgetcalId);
    }

    /**
     * Default Constructor
     * 
     */
    public BudgetcalJB() {
        Rlog.debug("budgetcal", "BudgetcalJB.BudgetcalJB() ");
    }

    // ///////////////////////////till here dk

    public BudgetcalJB(int budgetcalId, String budgetId, String budgetProtId,
            String budgetProtType, String budgetDelFlag, String spOverHead,
            String clOverHead, String spFlag, String clFlag,
            String indirectCost, String creator, String modifiedBy,
            String ipAdd, String budgetFrgBenefit, String budgetFrgFlag,
            String budgetDiscount, String budgetDiscountFlag, String budgetResearchCost, 
            String budgetResearchTotalCost, String budgetSOCCost, String budgetSOCTotalCost, 
            String budgetTotalCost, String budgetTotalTotalCost, String budgetSalaryCost, 
            String budgetSalaryTotalCost, String budgetFringeCost,String budgetFringeTotalCost, 
            String budgetDiscontinueCost, String budgetDiscontinueTotalCost, String budgetIndirectCost, 
            String budgetIndirectTotalCost, String budgetResearchSponsorAmount, String budgetResearchVariance, String budgetSOCSponsorAmount,
            String budgetSOCVariance, String budgetTotalSponsorAmount, String budgetTotalVariance) {

        setBudgetcalId(budgetcalId);
        setBudgetId(budgetId);
        setBudgetProtId(budgetProtId);
        setBudgetProtType(budgetProtType);
        setBudgetDelFlag(budgetDelFlag);
        setSpOverHead(spOverHead);
        setClOverHead(clOverHead);
        setSpFlag(spFlag);
        setClFlag(clFlag);
        setIndirectCost(indirectCost);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setBudgetFrgBenefit(budgetFrgBenefit);
        setBudgetFrgFlag(budgetFrgFlag);
        setBudgetDiscount(budgetDiscount);
        setBudgetDiscountFlag(budgetDiscountFlag);
        setBudgetExcldSOCFlag(budgetExcldSOCFlag);        
        setBudgetResearchCost(budgetResearchCost);
       	setBudgetResearchTotalCost(budgetResearchTotalCost);
       	setBudgetSOCCost(budgetSOCCost);
       	setBudgetSOCTotalCost(budgetSOCTotalCost);
       	setBudgetTotalCost(budgetTotalCost);
       	setBudgetTotalTotalCost(budgetTotalTotalCost);
       	setBudgetSalaryCost(budgetSalaryCost);
       	setBudgetSalaryTotalCost(budgetSalaryTotalCost);
       	setBudgetFringeCost(budgetFringeCost);
       	setBudgetFringeTotalCost(budgetFringeTotalCost);
       	setBudgetDiscontinueCost(budgetDiscontinueCost);
       	setBudgetDiscontinueTotalCost(budgetDiscontinueTotalCost);
       	setBudgetIndirectCost(budgetIndirectCost);
       	setBudgetIndirectTotalCost(budgetIndirectTotalCost);

       	//	Added By IA 11.03.2006 Comparative Budget
       	
       	setBudgetResearchSponsorAmount(budgetResearchSponsorAmount);
       	
       	setBudgetResearchVariance(budgetResearchVariance);
       	
       	setBudgetSOCSponsorAmount(budgetSOCSponsorAmount);
       	
       	setBudgetSOCVariance(budgetSOCVariance);
       	
       	setBudgetTotalSponsorAmount(budgetTotalSponsorAmount);
       	
       	setBudgetTotalVariance(budgetTotalVariance);
       	
       	// End Added
       	
       	
        Rlog.debug("budgetcal", "BudgetcalJB.BudgetcalJB(all parameters)");
    }

    /**
     * Calls getBudgetcalDetails() of Budgetcal Session Bean: BudgetcalAgentBean
     * 
     * 
     * @return The Details of the budgetcal in the Budgetcal StateKeeper Object
     * @See BudgetcalAgentBean
     */

    public BudgetcalBean getBudgetcalDetails() {
        BudgetcalBean bcsk = null;

        try {
            BudgetcalAgentRObj budgetcalAgent = EJBUtil.getBudgetcalAgentHome();
            bcsk = budgetcalAgent.getBudgetcalDetails(this.budgetcalId);
            Rlog.debug("budgetcal",
                    "BudgetcalJB.getBudgetcalDetails() BudgetcalStateKeeper "
                            + bcsk);
        } catch (Exception e) {
            Rlog.debug("budgetcal",
                    "Error in getBudgetcalDetails() in BudgetcalJB " + e);
        }
        if (bcsk != null) {

            budgetcalId = bcsk.getBudgetcalId();
            budgetId = bcsk.getBudgetId();
            budgetProtId = bcsk.getBudgetProtId();
            budgetProtType = bcsk.getBudgetProtType();
            budgetDelFlag = bcsk.getBudgetDelFlag();
            spOverHead = bcsk.getSpOverHead();
            clOverHead = bcsk.getClOverHead();
            spFlag = bcsk.getSpFlag();
            clFlag = bcsk.getClFlag();
            indirectCost = bcsk.getIndirectCost();
            creator = bcsk.getCreator();
            modifiedBy = bcsk.getModifiedBy();
            ipAdd = bcsk.getIpAdd();
            budgetFrgBenefit = bcsk.getBudgetFrgBenefit();
            budgetFrgFlag = bcsk.getBudgetFrgFlag();
            budgetDiscount = bcsk.getBudgetDiscount();
            budgetDiscountFlag = bcsk.getBudgetDiscountFlag();
            budgetExcldSOCFlag = bcsk.getBudgetExcldSOCFlag();
            budgetResearchCost = bcsk.getBudgetResearchCost();
            budgetResearchTotalCost = bcsk.getBudgetResearchTotalCost();
            budgetSOCCost = bcsk.getBudgetSOCCost();
            budgetSOCTotalCost = bcsk.getBudgetSOCTotalCost();
            budgetTotalCost = bcsk.getBudgetTotalCost();
            budgetTotalTotalCost = bcsk.getBudgetTotalTotalCost();
            budgetSalaryCost = bcsk.getBudgetSalaryCost();
            budgetSalaryTotalCost = bcsk.getBudgetSalaryTotalCost();
            budgetFringeCost = bcsk.getBudgetFringeCost();
            budgetFringeTotalCost = bcsk.getBudgetFringeTotalCost();
            budgetDiscontinueCost = bcsk.getBudgetDiscontinueCost();
            budgetDiscontinueTotalCost = bcsk.getBudgetDiscontinueTotalCost();
            budgetIndirectCost = bcsk.getBudgetIndirectCost();
            budgetIndirectTotalCost = bcsk.getBudgetIndirectTotalCost();
            
            //Added by IA 11.03.2006 Comparative Budget

            budgetResearchSponsorAmount = bcsk.getBudgetResearchSponsorAmount();
            budgetResearchVariance = bcsk.getBudgetResearchVariance();
            budgetSOCSponsorAmount = bcsk.getBudgetSOCSponsorAmount();
            budgetSOCVariance = bcsk.getBudgetSOCVariance();
            budgetTotalSponsorAmount= bcsk.getBudgetTotalSponsorAmount();
            budgetTotalVariance= bcsk.getBudgetTotalVariance();
            
        }
        return bcsk;
    }

    /**
     * Calls setBudgetcalDetails() of Budgetcal Session Bean: BudgetcalAgentBean
     * 
     */

    public void setBudgetcalDetails() {

        try {

            BudgetcalAgentRObj budgetcalAgent = EJBUtil.getBudgetcalAgentHome();
            this.budgetcalId = budgetcalAgent.setBudgetcalDetails(this
                    .createBudgetcalStateKeeper());
            Rlog.debug("budgetcal", "BudgetcalJB.setBudgetcalDetails()");
        } catch (Exception e) {
            Rlog.debug("budgetcal",
                    "Error in setBudgetcalDetails() in BudgetcalJB " + e);
        }
    }

    /**
     * Calls updateBudgetcal() of Budgetcal Session Bean: BudgetcalAgentBean
     * 
     * @return
     */
    public int updateBudgetcal() {
        int output;
        try {
            BudgetcalAgentRObj budgetcalAgentRObj = EJBUtil
                    .getBudgetcalAgentHome();
            Rlog
                    .debug("budgetcal",
                            "BudgetcalJB.updateBudgetcal() **********3");
            output = budgetcalAgentRObj.updateBudgetcal(this
                    .createBudgetcalStateKeeper());
        } catch (Exception e) {
            Rlog
                    .fatal("budgetcal",
                            "EXCEPTION IN UPDATING BUDGET CAL DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }
    // Overloaded for INF-18183 ::: Akshi
    public int updateBudgetcal(Hashtable<String, String> args) {
        int output;
        try {
            BudgetcalAgentRObj budgetcalAgentRObj = EJBUtil
                    .getBudgetcalAgentHome();
            Rlog
                    .debug("budgetcal",
                            "BudgetcalJB.updateBudgetcal() **********3");
            output = budgetcalAgentRObj.updateBudgetcal(this
                    .createBudgetcalStateKeeper(),args);
        } catch (Exception e) {
            Rlog
                    .fatal("budgetcal",
                            "EXCEPTION IN UPDATING BUDGET CAL DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    public int removeBudgetcal(int budgetcalId) {
        int output;
        try {
            BudgetcalAgentRObj budgetcalAgentRObj = EJBUtil
                    .getBudgetcalAgentHome();
            output = budgetcalAgentRObj.removeBudgetcal(budgetcalId);
        } catch (Exception e) {
            Rlog.fatal("budgetcal", "EXCEPTION IN DELETING THE CHILD NODE");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * 
     * 
     * @return the Budgetcal StateKeeper Object with the current values of the
     *         Bean
     */

    public BudgetcalBean createBudgetcalStateKeeper() {

        return new BudgetcalBean(budgetcalId, budgetId, budgetProtId,
                budgetProtType, budgetDelFlag, spOverHead, clOverHead, spFlag,
                clFlag, indirectCost, creator, modifiedBy, ipAdd,
                budgetFrgBenefit, budgetFrgFlag, budgetDiscount,
                budgetDiscountFlag, budgetExcldSOCFlag,  budgetResearchCost,  budgetResearchTotalCost,  budgetSOCCost, 
                budgetSOCTotalCost,  budgetTotalCost,  budgetTotalTotalCost, 
                budgetSalaryCost,  budgetSalaryTotalCost,  budgetFringeCost, 
                budgetFringeTotalCost,  budgetDiscontinueCost,  budgetDiscontinueTotalCost, 
                budgetIndirectCost,  budgetIndirectTotalCost, budgetResearchSponsorAmount, budgetResearchVariance, budgetSOCSponsorAmount,
                budgetSOCVariance, budgetTotalSponsorAmount, budgetTotalVariance );
    }

    /**
     * Get the details of all the protocols associated to the budget
     * 
     * @param budgetId
     */

    public BudgetcalDao getAllBgtCals(int budgetId) {
        BudgetcalDao budgetcalDao = new BudgetcalDao();
        try {
            BudgetcalAgentRObj budgetcalAgentRObj = EJBUtil
                    .getBudgetcalAgentHome();
            Rlog.debug("budgetcal", "BudgetcalJB.getAllBgtCals after remote");
            budgetcalDao = budgetcalAgentRObj.getAllBgtCals(budgetId);
            Rlog.debug("budgetcal", "BudgetcalJB.getAllBgtCals after call");
        } catch (Exception e) {
            Rlog.fatal("budgetcal", "Error in BudgetcalJB.getAllBgtCals " + e);
        }
        return budgetcalDao;
    }

    /**
     * Get the details of the protocol calendar associated to the budget
     * 
     * @param budgetCalId
     */

    public BudgetcalDao getBgtcalDetail(int bgtcalId) {
        BudgetcalDao budgetcalDao = new BudgetcalDao();
        try {
            BudgetcalAgentRObj budgetcalAgentRObj = EJBUtil
                    .getBudgetcalAgentHome();
            budgetcalDao = budgetcalAgentRObj.getBgtcalDetail(bgtcalId);
            Rlog.debug("budgetcal", "BudgetcalJB.getBgtcalDetail after call");
        } catch (Exception e) {
            Rlog
                    .fatal("budgetcal", "Error in BudgetcalJB.getBgtcalDetail "
                            + e);
        }
        return budgetcalDao;
    }

    // ///////////////////////
    /*
     * Update lineitem_othercost when 'exclude SOC' is updated
     */

    public int updateOtherCostToExcludeSOC(String bgtCalId, int excludeFlag,String modifiedBy,String ipAdd) 
    {
        int output;
        try {
            BudgetcalAgentRObj budgetcalAgentRObj = EJBUtil.getBudgetcalAgentHome();
            
            output = budgetcalAgentRObj.updateOtherCostToExcludeSOC(bgtCalId, excludeFlag,modifiedBy,ipAdd);
            
        } catch (Exception e) {
            Rlog.fatal("budgetcal", "EXCEPTION IN updateOtherCostToExcludeSOC");
            e.printStackTrace();
            return -1;
        }
        return output;
    }


    /*
     * generates a String of XML for the budgetcal details entry form.<br><br>
     * NOT IN USE
     */

    public String toXML() {
        Rlog.debug("budgetcal", "BudgetcalJB.toXML()");
        return "test";
        // to be implemented later
        /*
         * return new String("<?xml version=\"1.0\"?>" + "<?cocoon-process
         * type=\"xslt\"?>" + "<?xml-stylesheet
         * href=\"/stylesheet/form-html.xsl\" type=\"text/xsl\"?>" + "<?xml-stylesheet
         * href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>" + "<head>" + "</head>" + "<input
         * type=\"hidden\" name=\"budgetcalId\" value=\"" +
         * this.getBudgetcalId()+ "\" size=\"10\"/>" + "<input type=\"text\"
         * name=\"budgetcalCodelstJobtype\" value=\"" +
         * this.getBudgetcalCodelstJobtype() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"budgetcalAccountId\" value=\"" +
         * this.getBudgetcalAccountId() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"budgetcalSiteId\" value=\"" +
         * this.getBudgetcalSiteId() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"budgetcalPerAddressId\" value=\"" +
         * this.getBudgetcalPerAddressId() + "\" size=\"38\"/>" + "<input
         * type=\"text\" name=\"budgetcalLastName\" value=\"" +
         * this.getBudgetcalLastName() + "\" size=\"30\"/>" + "<input
         * type=\"text\" name=\"budgetcalFirstName\" value=\"" +
         * this.getBudgetcalFirstName() + "\" size=\"30\"/>" + "<input
         * type=\"text\" name=\"budgetcalMidName \" value=\"" +
         * this.getBudgetcalMidName() + "\" size=\"30\"/>" + "<input
         * type=\"text\" name=\"budgetcalWrkExp\" value=\"" +
         * this.getBudgetcalWrkExp() + "\" size=\"100\"/>" + "<input
         * type=\"text\" name=\"budgetcalPhaseInv\" value=\"" +
         * this.getBudgetcalPhaseInv() + "\" size=\"100\"/>" + "<input
         * type=\"text\" name=\"budgetcalSessionTime\" value=\"" +
         * this.getBudgetcalSessionTime() + "\" size=\"3\"/>" + "<input
         * type=\"text\" name=\"budgetcalLoginName\" value=\"" +
         * this.getBudgetcalLoginName() + "\" size=\"20\"/>" + "<input
         * type=\"text\" name=\"budgetcalPwd\" value=\"" +
         * this.getBudgetcalPwd() + "\" size=\"20\"/>" + "<input type=\"text\"
         * name=\"budgetcalSecQues\" value=\"" + this.getBudgetcalSecQues() +
         * "\" size=\"150\"/>" + "<input type=\"text\" name=\"budgetcalAnswer\"
         * value=\"" + this.getBudgetcalAnswer() + "\" size=\"150\"/>" + "<input
         * type=\"text\" name=\"budgetcalStatus\" value=\"" +
         * this.getBudgetcalStatus() + "\" size=\"1\"/>" + "<input
         * type=\"text\" name=\"budgetcalCodelstSpl\" value=\"" +
         * this.getBudgetcalCodelstSpl() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"budgetcalGrpDefault\" value=\"" +
         * this.getBudgetcalGrpDefault() + "\" size=\"10\"/>" + "</form>" );
         */

    }// end of method

}// end of class
