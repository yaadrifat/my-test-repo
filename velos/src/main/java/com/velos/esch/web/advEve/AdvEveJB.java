package com.velos.esch.web.advEve;

/* IMPORT STATEMENTS */

import java.util.Hashtable;

import com.velos.eres.business.common.NCILibDao;
import com.velos.esch.business.advEve.impl.AdvEveBean;
import com.velos.esch.business.common.EventInfoDao;
import com.velos.esch.service.advEveAgent.AdvEveAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * AdvEveJB is a client side Java Bean for Adverse Events' business components.
 * The class implements the Business Delegate pattern to reduce coupling between
 * presentation-tier clients and business services.
 * 
 * <p>
 * In eResearch Architecture, the class resides at the Application layer.
 * </p>
 * 
 * @author Arvind Kumar
 * @version %I%, %G%
 */
/*
 * *************************History********************************* Modified by
 * :Sonia Modified On:06/11/2004 Comment: 1. Added new attribute formStatus and
 * respective getter/setters 2. Modified methods for the new attribute.
 * *************************END****************************************
 */

public class AdvEveJB {

    /** adverse event id */
    private int advEveId;

    /** patient event id */
    private String advEveEvents1Id;

    /** type */
    private String advEveCodelstAeTypeId;
    
    
    
    /** MedDRA code */
    private String advEveMedDRA;
    
    /** dictionary */
    private String advEveDictionary;
    
    

    /** description */
    private String advEveDesc;

    /** start date */
    private String advEveStDate;

    /** end date */
    private String advEveEndDate;

    /** reported by */
    private String advEveEnterBy;
    
    /** adEveLinkedTo */
    private String advEveLinkedTo;
    
    /** reported by */
    private String advEveReportedBy;
    
    /** discovery date */
    private String advEveDiscoveryDate;
    
    /** logged date */
    private String advEveLoggedDate;
    
    
    

    /** outcome type */
    private String advEveOutType;

    /** outcome date */
    private String advEveOutDate;

    /** outcome notes */
    private String advEveOutNotes;

    /** additional info */
    private String advEveAddInfo;

    /** notes */
    private String advEveNotes;

    /** study */
    private String fkStudy;

    /** patient */
    private String fkPer;

    /** severity */
    private String advEveSeverity;

    /** body system affected */
    private String advEveBdsystemAff;

    /** relationship */
    private String advEveRelationship;

    /** recovery description */
    private String advEveRecoveryDesc;

    /** creator */
    private String creator;

    /** modified By */
    private String modifiedBy;

    /** IP Address */
    private String ipAdd;

    /** toxicity grade */
    private String advEveGrade;

    /** adverse Event Name */
    private String advEveName;

    /** associated lookup (CDE) */
    private String advEveLKPId;

    /** adverse event's treatment course */
    private String advEveTreatment;

    /** adverse event form status */
    private String formStatus;

    /** adverse Event Outcome Action added by Gopu dated on 01/20/05 */
    private String fkOutcomeAction;

    /** adverse event Category */
    private String aeCategory;
    
    /** adverse event Toxicity */
    private String aeToxicity;
    
    /** adverse event Toxicity Description */
    private String aeToxicityDesc;
    

	/** adverse event Grade Description */
    private String aeGradeDesc;

    // GETTER SETTER METHODS

    /**
     * Returns id for the adverse event
     * 
     * @return the adverse event id
     */
    public int getAdvEveId() {
        return this.advEveId;
    }

    /**
     * Sets the id for the adverse event
     * 
     * @param advEveId
     *            the adverse event id
     */

    public void setAdvEveId(int advEveId) {
        this.advEveId = advEveId;
    }

    /**
     * Gets the id for the associated patient event
     * 
     * @return event id
     * @deprecated patient events are no longer associated with an adverse event
     */

    public String getAdvEveEvents1Id() {
        return this.advEveEvents1Id;
    }

    /**
     * Sets the id for the associated patient event
     * 
     * @param advEveEvents1Id
     *            id for the associated patient event
     * @deprecated patient events are no longer associated with an adverse event
     */

    public void setAdvEveEvents1Id(String advEveEvents1Id) {
        this.advEveEvents1Id = advEveEvents1Id;
    }

    /**
     * Gets the id for the adverse event's type
     * 
     * @return adverse event type id
     */

    public String getAdvEveCodelstAeTypeId() {
        return this.advEveCodelstAeTypeId;
    }

    /**
     * Sets the id for the adverse event's type
     * 
     * @param advEveCodelstAeTypeId
     *            adverse event type id
     */

    public void setAdvEveCodelstAeTypeId(String advEveCodelstAeTypeId) {
        this.advEveCodelstAeTypeId = advEveCodelstAeTypeId;
    }

       
    //KM: added 05/10/06 May - June requirement PS1 
    /**
     * Gets the Adverse Event MedDRA code 
     * @return Adverse Event MedDRA code
     */
    public String getAdvEveMedDRA() {
        return this.advEveMedDRA;
    }
    
    /**
     * Sets the Adverse Event MedDRA code
     * @param advEveMedDRA
     */
    public void setAdvEveMedDRA(String advEveMedDRA){
    	this.advEveMedDRA=advEveMedDRA;
    }
    
    /**
     * Gets the Adverese Event Dictionary item
     * @return Adverese Event Dictionary item
     */
    public String getAdvEveDictionary() {
        return this.advEveDictionary;
    }
    
    /**
     * Sets the Adverese Event Dictionary item
     * @param advEveDictionary
     */
    public void setAdvEveDictionary(String advEveDictionary){
    	this.advEveDictionary=advEveDictionary;
    }
    
    
    
        
    /**
     * Gets the adverse event's description text
     * 
     * @return adverse event's description text
     */

    public String getAdvEveDesc() {
        return this.advEveDesc;
    }

    /**
     * Sets the adverse event's description text
     * 
     * @param advEveDesc
     *            adverse event's description text
     */

    public void setAdvEveDesc(String advEveDesc) {
        this.advEveDesc = advEveDesc;
    }

    /**
     * Gets the adverse event's start date
     * 
     * @return adverse event's start date
     */
    public String getAdvEveStDate() {
        return this.advEveStDate;
    }

    /**
     * Sets the adverse event's start date
     * 
     * @param advEveStDate
     *            adverse event's start date
     */
    public void setAdvEveStDate(String advEveStDate) {
        this.advEveStDate = advEveStDate;
    }

    /**
     * Gets the adverse event's end date
     * 
     * @return adverse event's end date
     */
    public String getAdvEveEndDate() {
        return this.advEveEndDate;
    }

    /**
     * Sets the adverse event's end date
     * 
     * @param advEveEndDate
     *            adverse event's end date
     */
    public void setAdvEveEndDate(String advEveEndDate) {
        this.advEveEndDate = advEveEndDate;
    }

    /**
     * Gets the user who entered the adverse event
     * 
     * @return user who entered the adverse event
     */
    public String getAdvEveEnterBy() {
        return this.advEveEnterBy;
    }

    /**
     * Sets the user who entered the adverse event
     * 
     * @param advEveEnterBy
     *            user who entered the adverse event
     */
    public void setAdvEveEnterBy(String advEveEnterBy) {
        this.advEveEnterBy = advEveEnterBy;
    }
   
    
    /**
     * Gets the user who entered the adverse event
     * 
     * @return user who entered the adverse event
     */
    public String getAdvEveReportedBy() {
        return this.advEveReportedBy;
    }

    /**
     * Sets the user who entered the adverse event
     * 
     * @param advEveEnterBy
     *            user who entered the adverse event
     */
    public void setAdvEveReportedBy(String advEveReportedBy) {
        this.advEveReportedBy = advEveReportedBy;
    }
    /**
     * Gets the adverse event outcome type
     * 
     * @return outcome type
     */
    
    public String getAdvEveLinkedTo() {
        return this.advEveLinkedTo;
    }

    /**
     * Sets the user who entered the adverse event
     * 
     * @param advEveLinkecTo
     *            user who entered the adverse event
     */
    public void setAdvEveLinkedTo(String advEveLinkedTo) {
        this.advEveLinkedTo = advEveLinkedTo;
    }
    
    public String getAdvEveDiscvryDate() {
        return this.advEveDiscoveryDate;
    }

    /**
     * Sets the user who entered the adverse event
     * 
     * @param advEveEnterBy
     *            user who entered the adverse event
     */
    public void setAdvEveDiscvryDate(String advEveDiscoveryDate) {
        this.advEveDiscoveryDate = advEveDiscoveryDate;
    }
     
    
    public String getAdvEveLoggedDate() {
        return this.advEveLoggedDate;
    }

    /**
     * Sets the user who entered the adverse event
     * 
     * @param advEveEnterBy
     *            user who entered the adverse event
     */
    public void setAdvEveLoggedDate(String advEveLoggedDate) {
        this.advEveLoggedDate = advEveLoggedDate;
    }

    
    
    public String getAdvEveOutType() {
        return this.advEveOutType;
    }

    /**
     * Sets the adverse event outcome type
     * 
     * @param advEveOutType
     *            outcome type
     */
    public void setAdvEveOutType(String advEveOutType) {
        this.advEveOutType = advEveOutType;
    }

    /**
     * Gets the adverse event outcome date
     * 
     * @return outcome date
     */
    public String getAdvEveOutDate() {
        return this.advEveOutDate;
    }

    /**
     * Sets the adverse event outcome date
     * 
     * @param advEveOutDate
     *            outcome date
     */
    public void setAdvEveOutDate(String advEveOutDate) {
        this.advEveOutDate = advEveOutDate;
    }

    /**
     * Gets the adverse event outcome notes
     * 
     * @return outcome notes
     */
    public String getAdvEveOutNotes() {
        return this.advEveOutNotes;
    }

    /**
     * Sets the adverse event outcome notes
     * 
     * @param advEveOutNotes
     *            outcome notes
     */
    public void setAdvEveOutNotes(String advEveOutNotes) {
        this.advEveOutNotes = advEveOutNotes;
    }

    /**
     * Gets the adverse event additional information code
     * 
     * @return additional information code
     */

    public String getAdvEveAddInfo() {
        return this.advEveAddInfo;
    }

    /**
     * Sets the adverse event additional information code
     * 
     * @param advEveAddInfo
     *            additional information code
     */

    public void setAdvEveAddInfo(String advEveAddInfo) {
        this.advEveAddInfo = advEveAddInfo;
    }

    /**
     * Gets the adverse event notes
     * 
     * @return notes
     */

    public String getAdvEveNotes() {
        return this.advEveNotes;
    }

    /**
     * Sets the adverse event notes
     * 
     * @param advEveNotes
     *            notes
     */

    public void setAdvEveNotes(String advEveNotes) {
        this.advEveNotes = advEveNotes;
    }

    /**
     * Gets patient's study on which the adverse event occurred
     * 
     * @return study
     */

    public String getFkStudy() {
        return this.fkStudy;
    }

    /**
     * Sets patient's study on which the adverse event occurred
     * 
     * @param fkStudy
     *            study
     */

    public void setFkStudy(String fkStudy) {
        this.fkStudy = fkStudy;
    }

    /**
     * Gets patient
     * 
     * @return patient
     */
    public String getFkPer() {
        return this.fkPer;
    }

    /**
     * Sets patient
     * 
     * @param fkPer
     *            patient
     */
    public void setFkPer(String fkPer) {
        this.fkPer = fkPer;
    }

    /**
     * Gets severity
     * 
     * @return severity
     */
    public String getAdvEveSeverity() {
        return this.advEveSeverity;
    }

    /**
     * Sets severity
     * 
     * @param advEveSeverity
     *            severity
     */
    public void setAdvEveSeverity(String advEveSeverity) {
        this.advEveSeverity = advEveSeverity;
    }

    /**
     * Gets body system affected
     * 
     * @return body system affected
     * @deprecated body system affected is no longer associated with an adverse
     *             event
     */
    public String getAdvEveBdsystemAff() {
        return this.advEveBdsystemAff;
    }

    /**
     * Sets body system affected
     * 
     * @param advEveBdsystemAff
     *            body system affected
     * @deprecated body system affected is no longer associated with an adverse
     *             event
     */

    public void setAdvEveBdsystemAff(String advEveBdsystemAff) {
        this.advEveBdsystemAff = advEveBdsystemAff;
    }

    /**
     * Gets relationship
     * 
     * @return relationship
     */

    public String getAdvEveRelationship() {
        return this.advEveRelationship;
    }

    /**
     * Sets relationship
     * 
     * @param advEveRelationship
     *            relationship
     */

    public void setAdvEveRelationship(String advEveRelationship) {
        this.advEveRelationship = advEveRelationship;
    }

    /**
     * Gets recovery description
     * 
     * @return recovery description
     */

    public String getAdvEveRecoveryDesc() {
        return this.advEveRecoveryDesc;
    }

    /**
     * Sets recovery description
     * 
     * @param advEveRecoveryDesc
     *            recovery description
     */
    public void setAdvEveRecoveryDesc(String advEveRecoveryDesc) {
        this.advEveRecoveryDesc = advEveRecoveryDesc;
    }

    /**
     * Gets the user who created the adverse event
     * 
     * @return the user who created the adverse event
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * Sets the user who created the adverse event
     * 
     * @param creator
     *            the user who created the adverse event
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * Gets the user who modified the adverse event
     * 
     * @return the user who modified the adverse event
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * Sets the user who modified the adverse event
     * 
     * @param modifiedBy
     *            the user who modified the adverse event
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * Gets the IP address of the user who created/modified the adverse event
     * 
     * @return IP address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * Sets the IP address of the user who created/modified the adverse event
     * 
     * @param ipAdd
     *            IP address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * Sets the grade
     * 
     * @param advEveGrade
     *            adverse event grade
     */

    public void setAdvEveGrade(String advEveGrade) {
        this.advEveGrade = advEveGrade;
    }

    /**
     * Gets the grade
     * 
     * @return adverse event grade
     */
    public String getAdvEveGrade() {
        return this.advEveGrade;
    }

    /**
     * Sets name for the adverse event
     * 
     * @param advEveName
     *            adverse event Name
     */
    public void setAdvEveName(String advEveName) {
        this.advEveName = advEveName;
    }

    /**
     * Gets name for the adverse event
     * 
     * @return returns the adverse event Name
     */
    public String getAdvEveName() {
        return this.advEveName;
    }

    /**
     * Associates a CDE Lookup Id with the adverse event
     * 
     * @param advEveLKPId
     *            CDE Lookup Id
     */
    public void setAdvEveLKPId(String advEveLKPId) {
        this.advEveLKPId = advEveLKPId;
    }

    /**
     * gets CDE Lookup Id
     * 
     * @return CDE Lookup Id
     */
    public String getAdvEveLKPId() {
        return this.advEveLKPId;
    }

    /**
     * Gets the adverse event's treatment course
     * 
     * @return adverse event's treatment course
     */

    public String getAdvEveTreatment() {
        return this.advEveTreatment;
    }

    /**
     * Sets the adverse event's treatment course
     * 
     * @param advEveDesc
     *            adverse event's treatment course
     */

    public void setAdvEveTreatment(String advEveTreatment) {
        this.advEveTreatment = advEveTreatment;
    }

    /**
     * Gets adverse event form status
     * 
     * @return adverse event form status
     */
    public String getFormStatus() {
        return formStatus;
    }

    /**
     * Sets adverse event form status
     * 
     * @param formStatus
     *            adverse event form status
     */
    public void setFormStatus(String formStatus) {
        this.formStatus = formStatus;
    }

    // Method added by Gopu dated on 01/20/05

    public String getFkOutcomeAction() {
        return fkOutcomeAction;
    }

    public void setFkOutcomeAction(String fkOutcomeAction) {
        this.fkOutcomeAction = fkOutcomeAction;
    }
    
    
    /**
     * Gets the adverse event's Category
     *
     * @return adverse event's Category
     */
    public String getAeCategory() {
		return aeCategory;
	}

    
    /**
     * Sets the adverse event's Category
     *
     * @param adverse event's Category
     */
	public void setAeCategory(String aeCategory) {
		this.aeCategory = aeCategory;
	}
	
	/**
     * Gets the adverse event's Toxicity
     *
     * @return adverse event's Toxicity
     */
	public String getAeToxicity() {
		return aeToxicity;
	}

	/**
     * Sets the adverse event's Toxicity
     *
     * @param adverse event's Toxicity
     */
	public void setAeToxicity(String aeToxicity) {
		this.aeToxicity = aeToxicity;
	}
	
	/**
     * Gets the adverse event's Toxicity Description
     *
     * @return adverse event's Toxicity Description
     */
	public String getAeToxicityDesc() {
		return aeToxicityDesc;
	}

	/**
     * Sets the adverse event's Toxicity Description
     *
     * @param adverse event's Toxicity Description
     */
	public void setAeToxicityDesc(String aeToxicityDesc) {
		this.aeToxicityDesc = aeToxicityDesc;
	}
	
	/**
     * Gets the adverse event's Grade Description
     *
     * @return adverse event's Grade Description
     */
	public String getAeGradeDesc() {
		return aeGradeDesc;
	}
	
	/**
     * Sets the adverse event's Grade Description
     *
     * @param adverse event's Grade Description
     */
	public void setAeGradeDesc(String aeGradeDesc) {
		this.aeGradeDesc = aeGradeDesc;
	}

    
    
    

    // END OF GETTER SETTER METHODS

    /**
     * Class constructor: creates an adverse event object for an adverse event
     * id
     * 
     * @param advEveId
     *            adverse event id
     */
    public AdvEveJB(int advEveId) {
        setAdvEveId(advEveId);
    }

    /**
     * Default Constructor
     * 
     */
    public AdvEveJB() {
        Rlog.debug("advEve", "AdvEveJB.AdvEveJB() ");
    }

    /**
     * Class constructor: full argument constructor
     * 
     * @param advEveId
     *            adverse event id
     * @param advEveEvents1Id
     *            patient event id
     * @param advEveCodelstAeTypeId
     *            type
     * @param advEveDesc
     *            description
     * @param advEveStDate
     *            start date
     * @param advEveEndDate
     *            end date
     * @param advEveEnterBy
     *            reported by
     * @param advEveOutType
     *            outcome type
     * @param advEveOutDate
     *            outcome date
     * @param advEveOutNotes
     *            outcome notes
     * @param advEveAddInfo
     *            additional info
     * @param advEveNotes
     *            notes
     * @param fkStudy
     *            study
     * @param fkPer
     *            patient
     * @param advEveSeverity
     *            severity
     * @param advEveBdsystemAff
     *            body system affected
     * @param advEveRelationship
     *            relationship
     * @param advEveRecoveryDesc
     *            recovery description
     * @param creator
     *            creator
     * @param modifiedBy
     *            modified By
     * @param ipAdd
     *            IP Address
     * @param advEveGrade
     *            toxicity grade
     * @param advEveName
     *            adverse Event Name
     * @param advEveLKPId
     *            associated lookup (CDE)
     * @param advEveTreatment
     *            adverse event treatment course
     * @param formStatus
     *            adverse event form status
     */
    /*
     * *************************History*********************************
     * Modified by :Sonia Modified On:06/11/2004 Comment: 1. to set the new
     * attribute formStatus.
     * *************************END****************************************
     */

    public AdvEveJB(int advEveId, String advEveEvents1Id,
            String advEveCodelstAeTypeId, String advEveMedDRA,String advEveDictionary,String advEveDesc,
            String advEveStDate, String advEveEndDate, String advEveEnterBy,String advEveReportedBy,
            String advEveLinkedTo,
            String advEveDiscoveryDate,String advEveLoggedDate,
            String advEveOutType, String advEveOutDate, String advEveOutNotes,
            String advEveAddInfo, String advEveNotes, String fkPer,
            String fkStudy, String advEveSeverity, String advEveBdsystemAff,
            String advEveRelationship, String advEveRecoveryDesc,
            String creator, String modifiedBy, String ipAdd,
            String advEveGrade, String advEveName, String advEveLKPId,
            String advEveTreatment, String formStatus, String fkOutcomeAction,String aeCategory,String aeToxicity,String aeToxicityDesc,String aeGradeDesc) {

        setAdvEveId(advEveId);
        setAdvEveEvents1Id(advEveEvents1Id);
        setAdvEveCodelstAeTypeId(advEveCodelstAeTypeId);
        setAdvEveMedDRA(advEveMedDRA);
        setAdvEveDictionary(advEveDictionary);
        setAdvEveDesc(advEveDesc);
        setAdvEveStDate(advEveStDate);
        setAdvEveEndDate(advEveEndDate);
        setAdvEveEnterBy(advEveEnterBy);
        setAdvEveReportedBy(advEveReportedBy);
        setAdvEveLinkedTo(advEveLinkedTo);
        setAdvEveDiscvryDate(advEveDiscoveryDate);
        setAdvEveLoggedDate(advEveLoggedDate);
        
        
                       
        
        setAdvEveOutType(advEveOutType);
        setAdvEveOutDate(advEveOutDate);
        setAdvEveOutNotes(advEveOutNotes);
        setAdvEveAddInfo(advEveAddInfo);
        setAdvEveNotes(advEveNotes);
        setFkStudy(fkStudy);
        setFkPer(fkPer);
        setAdvEveSeverity(advEveSeverity);
        setAdvEveBdsystemAff(advEveBdsystemAff);
        setAdvEveRelationship(advEveRelationship);
        setAdvEveRecoveryDesc(advEveRecoveryDesc);
        setCreator(creator);
        setCreator(creator);
        
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setAdvEveGrade(advEveGrade);
        setAdvEveName(advEveName);
        setAdvEveLKPId(advEveLKPId);
        setAdvEveTreatment(advEveTreatment);
        setFormStatus(formStatus);
        // Method added by Gopu dated on 01/20/05
        setFkOutcomeAction(fkOutcomeAction);
        setAeCategory(aeCategory);
        setAeToxicity(aeToxicity);
        setAeToxicityDesc(aeToxicityDesc);
        setAeGradeDesc(aeGradeDesc);
        Rlog.debug("advEve", "AdvEveJB.AdvEveJB(all parameters)");
    }

    /**
     * Populates itself with the adverse event's details using its attribute :
     * advEveId Calls getAdvEveDetails() on the Adverse Events Session Bean:
     * AdvEveAgentBean. The session bean method returns an AdvEveStateKeeper
     * object that is used by this method to populate the object's attributes.
     * 
     * @see AdvEveStateKeeper
     */
    /*
     * *************************History*********************************
     * Modified by :Sonia Modified On:06/11/2004 Comment: 1. to set the new
     * attribute formStatus.
     * *************************END****************************************
     */

    public AdvEveBean getAdvEveDetails() {
        AdvEveBean edsk = null;

        try {
            AdvEveAgentRObj advEveAgent = EJBUtil.getAdvEveAgentHome();
            edsk = advEveAgent.getAdvEveDetails(this.advEveId);
            Rlog.debug("advEve",
                    "AdvEveJB.getAdvEveDetails() AdvEveStateKeeper " + edsk);
        } catch (Exception e) {
            Rlog
                    .debug("advEve", "Error in getAdvEveDetails() in AdvEveJB "
                            + e);
        }
        if (edsk != null) {

            this.advEveId = edsk.getAdvEveId();
            this.advEveEvents1Id = edsk.getAdvEveEvents1Id();
            this.advEveCodelstAeTypeId = edsk.getAdvEveCodelstAeTypeId();
            this.advEveMedDRA=edsk.getAdvEveMedDRA();
            this.advEveDictionary=edsk.getAdvEveDictionary();
            this.advEveDesc = edsk.getAdvEveDesc();
            this.advEveStDate = edsk.getAdvEveStDate();
            this.advEveEndDate = edsk.getAdvEveEndDate();
            this.advEveEnterBy = edsk.getAdvEveEnterBy();
            this.advEveReportedBy = edsk.getAdvEveReportedBy();
            this.advEveLinkedTo =edsk.getAdvEveLinkedTo();
            this.advEveDiscoveryDate = edsk.getAdvEveDiscoveryDate();
            this.advEveLoggedDate = edsk.getAdvEveLoggedDate();
            this.advEveOutType = edsk.getAdvEveOutType();
            this.advEveOutDate = edsk.getAdvEveOutDate();
            this.advEveOutNotes = edsk.getAdvEveOutNotes();
            this.advEveAddInfo = edsk.getAdvEveAddInfo();
            this.advEveNotes = edsk.getAdvEveNotes();
            this.fkStudy = edsk.getFkStudy();
            this.fkPer = edsk.getFkPer();
            this.advEveSeverity = edsk.getAdvEveSeverity();
            this.advEveBdsystemAff = edsk.getAdvEveBdsystemAff();
            this.advEveRelationship = edsk.getAdvEveRelationship();
            this.advEveRecoveryDesc = edsk.getAdvEveRecoveryDesc();
            this.creator = edsk.getCreator();
            this.modifiedBy = edsk.getModifiedBy();
            this.ipAdd = edsk.getIpAdd();
            this.advEveTreatment = edsk.getAdvEveTreatment();
            // Method added by Gopu dated on 01/20/05
            this.fkOutcomeAction = edsk.getFkOutcomeAction();

            setAdvEveGrade(edsk.getAdvEveGrade());
            setAdvEveName(edsk.getAdvEveName());
            setAdvEveLKPId(edsk.getAdvEveLKPId());
            setFormStatus(edsk.getFormStatus());
            setAeCategory(edsk.getAeCategory());
            setAeToxicity(edsk.getAeToxicity());
            setAeToxicityDesc(edsk.getAeToxicityDesc());
            setAeGradeDesc(edsk.getAeGradeDesc());

        }
        return edsk;
    }

    /**
     * Creates a new adverse event using the object's attributes. Calls
     * setAdvEveDetails() on the Adverse Events Session Bean: AdvEveAgentBean.
     * 
     * @return success flag
     *         <ul>
     *         <li> id of new adverse event</li>
     *         <li> -2 in case of a failure</li>
     *         </ul>
     * 
     */
    public int setAdvEveDetails() {
        int ret = 0;
        try {
            AdvEveAgentRObj advEveAgent = EJBUtil.getAdvEveAgentHome();
            this.setAdvEveId(advEveAgent.setAdvEveDetails(this
                    .createAdvEveStateKeeper()));
            Rlog.debug("advEve", "AdvEveJB.setAdvEveDetails()");
            return this.getAdvEveId();
        } catch (Exception e) {
        	e.printStackTrace();
            Rlog
                    .fatal("advEve", "Error in setAdvEveDetails() in AdvEveJB "
                            + e);
            return -2;
        }
    }

    /**
     * Updates an adverse event using the object's attributes. Calls
     * updateAdvEve() on the Adverse Events Session Bean: AdvEveAgentBean.
     * 
     * @return success flag
     *         <ul>
     *         <li> 0 if successful</li>
     *         <li> -2 in case of a failure</li>
     *         </ul>
     * 
     */

    public int updateAdvEve() {
        int output;
        try {
            AdvEveAgentRObj advEveAgentRObj = EJBUtil.getAdvEveAgentHome();
            output = advEveAgentRObj.updateAdvEve(this
                    .createAdvEveStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("advEve",
                    "EXCEPTION IN SETTING AdvEve DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * Returns an AdvEveStateKeeper object with the current values of the Bean
     * 
     * @return AdvEveStateKeeper
     * @see AdvEveStateKeeper
     */
    /*
     * *************************History*********************************
     * Modified by :Sonia Modified On:06/11/2004 Comment: 1. to set the new
     * attribute formStatus.
     * *************************END****************************************
     */

    public AdvEveBean createAdvEveStateKeeper() {
    	
    	          
    	            

       return new AdvEveBean(advEveId, advEveEvents1Id, advEveCodelstAeTypeId,
        		advEveMedDRA, advEveDictionary, advEveDesc, advEveStDate, advEveEndDate,advEveEnterBy,
        		advEveReportedBy,advEveLinkedTo,advEveDiscoveryDate,advEveLoggedDate,
                advEveOutType, advEveOutDate, advEveOutNotes, advEveAddInfo,
                advEveNotes, fkStudy, fkPer, advEveSeverity, advEveBdsystemAff,
                advEveRelationship, advEveRecoveryDesc, creator, modifiedBy,
                ipAdd, advEveGrade, advEveName, advEveLKPId, advEveTreatment,
                formStatus, fkOutcomeAction,aeCategory,aeToxicity,aeToxicityDesc,aeGradeDesc);
    }

    /**
     * Returns an EventInfoDao object with patient adverse events. Calls
     * getAdverseEventNames() on the Adverse Events Session Bean:
     * AdvEveAgentBean.
     * 
     * @param patProtID
     *            patient enrollment id
     * @return returns EventInfoDao
     * 
     */
    public EventInfoDao getAdverseEventNames(int patProtID) {
        EventInfoDao eventInfoDao = new EventInfoDao();
        try {
            Rlog.debug("person", "PersonJB.getPatients starting");
            AdvEveAgentRObj advEveAgent = EJBUtil.getAdvEveAgentHome();
            eventInfoDao = advEveAgent.getAdverseEventNames(patProtID);
            Rlog.debug("person", "PersonJB.getPatients after Dao");
        } catch (Exception e) {
            Rlog.fatal("person", "Error in getPatients in PersonJB " + e);
        }
        return eventInfoDao;
    }

    /**
     * Returns an NCILibDao object with patient adverse events toxicity
     * information. Calls getToxicityGrade() on the Adverse Events Session Bean:
     * AdvEveAgentBean.
     * 
     * @param toxicityGroup
     *            toxicity group
     * @param labDate
     *            lab test date
     * @param labUnit
     *            lab unit
     * @param labSite
     *            lab site
     * @param labResult
     *            lab result
     * @param labLLN
     *            lab LLN
     * @param labULN
     *            lab ULN
     * @param userRangeFlag
     *            flag whether to user user specified range or lab facility's
     *            range
     * @param nciVersion
     *            NCI CDE version
     * @param patient
     *            patient
     * @return returns NCILibDao
     * 
     */

    public NCILibDao getToxicityGrade(int toxicityGroup, String labDate,
            String labUnit, int labSite, String labResult, int labLLN,
            int labULN, int userRangeFlag, String nciVersion, int patient) {
        NCILibDao nciLDao = new NCILibDao();
        try {

            AdvEveAgentRObj advEveAgent = EJBUtil.getAdvEveAgentHome();
            nciLDao = advEveAgent.getToxicityGrade(toxicityGroup, labDate,
                    labUnit, labSite, labResult, labLLN, labULN, userRangeFlag,
                    nciVersion, patient);

        } catch (Exception e) {
            Rlog.fatal("common",
                    "Exception in getToxicityGrade in getToxicityGrade in AdveEveJB "
                            + e);
        }
        return nciLDao;
    }

    /**
     * Returns an NCILibDao object with adverse events toxicity names. Calls
     * getNCILib() on the Adverse Events Session Bean: AdvEveAgentBean.
     * 
     * @param nciVersion
     * @return returns NCILibDao
     * 
     */

    public NCILibDao getNCILib(String nciVersion) {
        NCILibDao nciLDao = new NCILibDao();
        try {

            AdvEveAgentRObj advEveAgent = EJBUtil.getAdvEveAgentHome();
            nciLDao = advEveAgent.getNCILib(nciVersion);

        } catch (Exception e) {
            Rlog.fatal("common",
                    "Exception in getNCILib in getNCILib in AdveEveJB " + e);
        }
        return nciLDao;
    }

    /**
     * method added by Majumdar dt. 19Jan2005 deletes adverse event using the
     * object's attributes. Calls reomoveAdvEve() on the Adverse Events Session
     * Bean: AdvEveAgentBean.
     * 
     * @return success flag
     */

    public int reomoveAdvEve(int advId) {
        int ret = 1;
        try {
            AdvEveAgentRObj advEveAgentRObj = EJBUtil.getAdvEveAgentHome();
            ret = advEveAgentRObj.reomoveAdvEve(advId);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("advEve",
                    "EXCEPTION IN SETTING AdvEve DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }

    }
    // Overloaded for INF-18183 ::: AGodara
    public int reomoveAdvEve(int advId,Hashtable<String, String> auditInfo) {
        int ret = 1;
        try {
            AdvEveAgentRObj advEveAgentRObj = EJBUtil.getAdvEveAgentHome();
            ret = advEveAgentRObj.reomoveAdvEve(advId,auditInfo);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("advEve",
                    "EXCEPTION IN SETTING AdvEve DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }

    }
}// end of class
