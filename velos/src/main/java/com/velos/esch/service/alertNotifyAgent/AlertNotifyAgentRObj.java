/*
 * Classname : AlertNotifyAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 12/11/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.service.alertNotifyAgent;

/* Import Statements */

import javax.ejb.Remote;

import com.velos.esch.business.alertNotify.impl.AlertNotifyBean;
import com.velos.esch.business.common.EventInfoDao;

// import com.velos.esch.business.common.EventAssocDao;
@Remote
public interface AlertNotifyAgentRObj {

    AlertNotifyBean getAlertNotifyDetails(int alertNotifyId);

    public int setAlertNotifyDetails(AlertNotifyBean account);

    public int updateAlertNotify(AlertNotifyBean usk);

    public int updateAlertNotifyList(String[] anIds, String[] flags,
            String[] userIds, String[] mobIds, String lastModifiedBy,
            String ipAdd, String studyId, String protocolId);

    public EventInfoDao getStudyAlertNotify(int StudyId, int protocolId);

    public int updateStudyAlertNotify(int studyId, int protocolId,
            int modifiedBy, String ipAdd, String globFlag, int lockValue);

    public String getANGlobalFlag(int studyId, int protocolId);

    public int notifyAdmin(int userId, String ipAdd, int velosUser,
            String failDate, String failTime);

    public int setDefStudyAlnot(String studyid, String protocolid,
            String creator, String ipAdd);

}
