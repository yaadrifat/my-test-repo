/*
 * Classname : BgtSectionAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 03/22/2002
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Sahni
 */

package com.velos.esch.service.bgtSectionAgent;

/* Import Statements */

import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.esch.business.bgtSection.impl.BgtSectionBean;
import com.velos.esch.business.common.BgtSectionDao;

@Remote
public interface BgtSectionAgentRObj {

    BgtSectionBean getBgtSectionDetails(int bgtSectionId);

    public int setBgtSectionDetails(BgtSectionBean account);

    public int updateBgtSection(BgtSectionBean usk);
    
    //Overloaded for INF-18183 ::: Ankit
    public int updateBgtSection(BgtSectionBean usk, Hashtable<String, String> auditInfo);

    public BgtSectionDao getBgtSections(int calId);

    public int bgtSectionDelete(int bgtSectionId);

    public int insertDefaultSecLineitems(int bgtSecId, int bgtCalId,
            int creator, String ipAdd);
    
    /**
     * Get all the sections (with calendar names) for a budget
     * 
     * @param budgetId
     */

    public BgtSectionDao  getAllBgtSections(int budgetId);
    
    /** 
     * Update patient number for all per patient sections in a budget
     *  */
    public int applyNumberOfPatientsToAllSections(String sourceSectionPK,String bgtCalPK,String patNo,String last_modified_by,String ipAdd);
    

}