/*
 * Classname : EventdocAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 07/04/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana 
 */

package com.velos.esch.service.eventdocAgent.impl;

/* IMPORT STATEMENTS */

import java.util.Enumeration;

import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import java.util.Hashtable;
import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.DateUtil;
import com.velos.esch.business.audit.impl.AuditBean;
import com.velos.esch.business.common.EventdefDao;
import com.velos.esch.business.docref.impl.DocrefBean;
import com.velos.esch.business.eventdoc.impl.EventdocBean;
import com.velos.esch.service.docrefAgent.DocrefAgentRObj;
import com.velos.esch.service.eventdocAgent.EventdocAgentRObj;
import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.EJBUtil;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP EventdocBean.<br>
 * <br>
 * 
 */
@Stateless
public class EventdocAgentBean implements EventdocAgentRObj {
    @PersistenceContext(unitName = "esch")
    protected EntityManager em;
    @Resource private SessionContext context;
    /**
     * Calls getEventdocDetails() on Eventdoc Entity Bean EventdocBean. Looks up
     * on the Eventdoc id parameter passed in and constructs and returns a
     * Eventdoc state holder. containing all the summary details of the Eventdoc<br>
     * 
     * @param eventdocId
     *            the Eventdoc id
     * @ee EventdocBean
     */
    public EventdocBean getEventdocDetails(int eventdocId) {

        try {
            return (EventdocBean) em.find(EventdocBean.class, new Integer(
                    eventdocId));

        } catch (Exception e) {
            System.out
                    .print("Error in getEventdocDetails() in EventdocAgentBean"
                            + e);
            return null;
        }

    }
    
    /**
     * Calls setEventdocDetails() on Eventdoc Entity Bean EventdocBean.Creates a
     * new Eventdoc with the values in the StateKeeper object.
     * 
     * @param eventdoc
     *            a eventdoc state keeper containing eventdoc attributes for the
     *            new Eventdoc.
     * @return int - 0 if successful; -2 for Exception;
     */

    public int setEventdocDetails(EventdocBean edsk) {
        int Id;
        Integer docId;
        String eventId;
        String networkId;
        String networkFlag;

        try {

            EventdocBean eventdoc = new EventdocBean();

            DocrefAgentRObj docrefHome = EJBUtil.getDocrefAgentHome();
            Rlog.debug("eventdoc", "EventrefAgentBean.reference is  "
                    + docrefHome);

            eventdoc.updateEventdoc(edsk);
            em.persist(eventdoc);
            int pkDoc = 0;
            Id = eventdoc.getDocId();
            docId = new Integer(Id);
            networkId = eventdoc.getNetworkId();
            networkFlag = eventdoc.getNetworkFlag();
            eventId = eventdoc.getEventId();
            int networkID = 0;
            Rlog.debug("eventdoc", "EventdocAgentBean.doc id " + docId);
            Rlog.debug("eventdoc", "EventdocAgentBean.eventid " + eventId);
            String temp = docId.toString();
            
            if(networkId==null || networkId.equalsIgnoreCase("")){
            	
            DocrefBean docrefBean = new DocrefBean(pkDoc, temp, eventId, edsk
                    .getCreator(), edsk.getModifiedBy(), edsk.getIpAdd());

            docrefHome.setDocRefDetails(docrefBean);
            Rlog.debug("eventdoc",
                    "EventdocAgentBean.setEventdocDetails EventdocStateKeeper "
                            + eventdoc);
        }else{
            	 networkID = new Integer(networkId);
            	EventdefDao eventdef = new EventdefDao();
            	eventdef.saveNetworkDoc(docId,networkID,networkFlag,edsk.getCreator(), edsk.getIpAdd());
            }
           return (eventdoc.getDocId());
            
        } catch (Exception e) {
            System.out
                    .print("Error in setEventdocDetails() in EventdocAgentBean "
                            + e);
            e.printStackTrace();
        }
        return 0;
    }

    public int updateEventdoc(EventdocBean edsk) {

        EventdocBean retrieved = null; // Eventdoc Entity Bean Remote Object
        int output;

        try {

            retrieved = (EventdocBean) em.find(EventdocBean.class, edsk
                    .getDocId());

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateEventdoc(edsk);

        } catch (Exception e) {
            Rlog.debug("eventdoc",
                    "Error in updateEventdoc in EventdocAgentBean" + e);
            return -2;
        }
        return output;
    }

    public int removeEventdoc(int docId, String docType) {

        int output = 0;
        Enumeration eventdocIds;

        try {

            Integer doc = new Integer(docId);

            // Query querySiteIdentifier =
            // em.createNamedQuery("findEventdocId1");
            // querySiteIdentifier.setParameter("pkDocs", doc);
            // ArrayList list = (ArrayList) querySiteIdentifier.getResultList();
            // if (list == null) list = new ArrayList();
            // System.out.println("SiteAgentBean.setSiteDetails
            // SiteStateKeeper:enum_velos "
            // + list);
            DocrefAgentRObj docref = EJBUtil.getDocrefAgentHome();

            docref.remove(docId);
//Mukul:Bug 4107: since record should be delete from SCH_DOCs table in case of Upload document also, SO THis condition should be comment : 
          //  if (docType.equals("U")) {
                EventdocBean eventDocBean = (EventdocBean) em.find(
                        EventdocBean.class, new Integer(docId));
                em.remove(eventDocBean);
           // }

        } catch (Exception e) {
            Rlog.debug("eventeventdocId",
                    "Error in remove eventdocChild  in eventdocAgentBean" + e);
            e.printStackTrace();
            return -2;
        }
        return output;
    }
    
    //Overloaded for INF-18183 ::: Ankit
    public int removeEventdoc(int docId, String docType, Hashtable<String, String> argsvalues) {

        int output = 0;
        Enumeration eventdocIds;

        try {

            Integer doc = new Integer(docId);

            DocrefAgentRObj docref = EJBUtil.getDocrefAgentHome();

            docref.remove(docId);
    	  //Mukul:Bug 4107: since record should be delete from SCH_DOCs table in case of Upload document also, SO THis condition should be comment : 
          //  if (docType.equals("U")) {
            
	            AuditBean audit=null;
	            String currdate =DateUtil.getCurrentDate();
	            String userID=(String)argsvalues.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
	            String ipAdd=(String)argsvalues.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
	            String reason=(String)argsvalues.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
	            String moduleName = (String)argsvalues.get(AuditUtils.APP_MODULE);
	            String getRidValue= AuditUtils.getRidValue("SCH_DOCS","esch","PK_DOCS="+docId);/*Fetches the RID/PK_VALUE*/ 
	            audit = new AuditBean("SCH_DOCS",String.valueOf(docId),getRidValue,
	        			userID,currdate,moduleName,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
	        	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
            
            
                EventdocBean eventDocBean = (EventdocBean) em.find(
                        EventdocBean.class, new Integer(docId));
                em.remove(eventDocBean);
           // }

        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.debug("eventeventdocId",
                    "Error in remove eventdocChild  in eventdocAgentBean" + e);
            e.printStackTrace();
            return -2;
        }
        return output;
    }

}// end of class