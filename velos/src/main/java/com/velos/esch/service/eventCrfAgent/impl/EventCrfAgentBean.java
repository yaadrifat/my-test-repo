/*
 * Classname			EventCrfAgentBean
 * 
 * Version information : 
 *
 * Date					06/25/2003
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.esch.service.eventCrfAgent.impl;

/* IMPORT STATEMENTS */

import java.util.ArrayList;
import java.util.Hashtable;

import java.sql.Connection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ejb.SessionContext;
import javax.annotation.Resource;
import javax.persistence.Query;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.DateUtil;
import com.velos.esch.business.audit.impl.AuditBean;
import com.velos.esch.business.common.CrfDao;
import com.velos.esch.business.eventCrf.impl.EventCrfBean;
//import com.velos.eres.business.common.CatLibDao;
import com.velos.esch.service.eventCrfAgent.EventCrfAgentRObj;
import com.velos.esch.service.util.Rlog;


/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP.
 * 
 * @author Manimaran
 * @version 1.0, 06/25/2003
 * @ejbRemote EventCrfAgentRObj
 */

@Stateless
public class EventCrfAgentBean implements EventCrfAgentRObj {

    @PersistenceContext(unitName = "esch")
    protected EntityManager em;
    @Resource private SessionContext context;

   
    public EventCrfBean getEventCrfDetails(int eventCrfId) {
        EventCrfBean eb = null;
        try {

            eb = (EventCrfBean) em.find(EventCrfBean.class, new Integer(eventCrfId));

        } catch (Exception e) {
            Rlog.fatal("eventcrf",
                    "Error in getEventCrfDetails() in EventCrfAgentBean" + e);
        }

        return eb;
    }
    
    
    /**
     * Sets a Eventcrf.
     * 
     * @param eventcrf
     *            The new eventCrfDetails value
     * @return void
     */

    public int setEventCrfDetails(EventCrfBean eventcrf) {
        try {
        	EventCrfBean cd = new EventCrfBean();
            cd.updateEventCrf(eventcrf);
            em.persist(cd);
            return (cd.getEventCrfId());
        } catch (Exception e) {
            System.out
                    .print("Error in setEventCrfDetails() in EventCrfAgentBean "
                            + e);
        }
        return 0;
    }

    
    /**
     * Description of the Method
     * 
     * @param usk
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public int updateEventCrf(EventCrfBean eventcrf) {

    	EventCrfBean retrieved = null;

        int output;

        try {

            retrieved = (EventCrfBean) em.find(EventCrfBean.class, new Integer(
            		eventcrf.getEventCrfId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateEventCrf(eventcrf);
            em.merge(retrieved);
        } catch (Exception e) {
            Rlog.debug("eventcrf", "Error in updateEventCrf in EventCRfAgentBean"
                    + e);
            return -2;
        }
        return output;
    }
    //Added by Gopu for May-June Enhancement #F1 Calendar to fomr linking 
    /**
     * Get Event CRF Form 
     * 
     * @param eventId
     *            
     * @return 
     */

    public CrfDao getEventCrfForms(int eventId,String dispType){
    	try {
            CrfDao crfDao = new CrfDao();
            crfDao.getEventCrfForms(eventId,dispType);
            return crfDao;
        } catch (Exception e) {
            Rlog.fatal("crf", "Exception In getEventCrfForms in EventCrfAgentBean " + e);
        }
        return null;
    }
    
    /**
     * Delete Event CRF Forms
     * 
     * @param eventId
     * 
     */
	// Overloaded for INF-18183 ::: Akshi
    public void deleteEventCrf(int eventId,Hashtable<String, String> args){
    		
    	 String condition="";
    	   	try {
    	   		AuditBean audit=null;
            	String pkVal = "";
            	String ridVal = "";
               
                String currdate =DateUtil.getCurrentDate();
                String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
                String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
                String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
                String appmodule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/    
                                           	
            	condition = "FK_EVENT="+eventId;        //Fixed for Bug#7330: Akshi

            	Hashtable<String, ArrayList<String>> getRowValues = AuditUtils.getRowValues("SCH_EVENT_CRF",condition,"esch");/*Fetches the RID/PK_VALUE*/
            	ArrayList<String> rowPK  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_PK_KEY);
        		ArrayList<String> rowRID  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_RID_KEY);
        		//String getRidValue= AuditUtils.getRidValues("ER_PATPROT","PK_PATPROT="+condition);/*Fetches the RID/PK_VALUE*/ 
                for(int i=0; i<rowPK.size(); i++)
    			{
        			pkVal = rowPK.get(i);
        			ridVal = rowRID.get(i);
        			audit = new AuditBean("SCH_EVENT_CRF",pkVal,ridVal,
            			userID,currdate,appmodule,ipAdd,reason);
        			em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
        			CrfDao crfDao = new CrfDao();
                    int ret =crfDao.deleteEventCrf(eventId);
                    if(ret!=0){/*if Dao returns result without exception*/                    	
                    	context.setRollbackOnly();
                    }
    			}
    	        	       
            
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("crf", "Exception In deleteEventCrf in EventCrfAgentBean " + e);
        }
    }

    public void deleteEventCrf(int eventId){
    	try {
    		
    		    		
            CrfDao crfDao = new CrfDao();
            crfDao.deleteEventCrf(eventId);
        } catch (Exception e) {
        	Rlog.fatal("crf", "Exception In deleteEventCrf in EventCrfAgentBean " + e);
        }
    }
    
    /** Returns 'Visit Mark Done' prompt Flag = 1 if all the CRfs are filled for the scheduled event record 
     *  and if the event has no marked
     * status. It will return 0 if the event already has a status. 
     * @param schEventId PK to identify scheduled event (sch_events1 pk)
     * @return 1 if application should prompt for marking 'events done', 0 - for no prompts
     */
    public int getEventMarkDoneFlagWhenAllCRFsFilled(int schEventId)
    {
    	try {
            CrfDao crfDao = new CrfDao();
            return crfDao.getEventMarkDoneFlagWhenAllCRFsFilled(schEventId);
        } catch (Exception e) {
            Rlog.fatal("crf", "Exception In getEventMarkDoneFlagWhenAllCRFsFilled in EventCrfAgentBean " + e);
            return 0;
        }
    }
    
}// end of class

