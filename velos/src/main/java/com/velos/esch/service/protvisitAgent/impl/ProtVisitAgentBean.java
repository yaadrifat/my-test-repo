/*
 * Classname : ProtVisitAgentBean
 *
 * Version information: 1.0
 *
 * Date: 09/22/2004
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Sam Varadarajan
 */

package com.velos.esch.service.protvisitAgent.impl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.NumberUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.esch.business.audit.impl.AuditBean;
import com.velos.esch.business.common.EventdefDao;
import com.velos.esch.business.common.ProtVisitDao;
import com.velos.esch.business.common.SchCodeDao;
import com.velos.esch.business.common.ScheduleDao;
import com.velos.esch.business.protvisit.impl.ProtVisitBean;
import com.velos.esch.service.protvisitAgent.ProtVisitAgentRObj;
import com.velos.esch.service.util.Rlog;
import com.velos.esch.web.protvisit.ProtVisitResponse;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP
 * ProtVisitBean.<br>
 * <br>
 *
 */
@Stateless
public class ProtVisitAgentBean implements ProtVisitAgentRObj {
	@Resource private SessionContext context;
    @PersistenceContext(unitName = "esch")
    protected EntityManager em;

    /**
     *
     */
    private static final long serialVersionUID = 3257852060673454384L;

    /**
     * Calls getProtocolVisits() on ProtVisitDao;
     *
     * @param protocol
     *            Id
     * @return ProtVisitDao object with details of all Visits for a protcol
     *         calendar.
     * @ee EventdefDao
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public ProtVisitDao getProtocolVisits(int protocol_id) {
        try {
            Rlog
                    .debug("protvisit",
                            "In getProtocolVisits in ProtocolVisitAgentBean line number 0");
            ProtVisitDao protVisitDao = new ProtVisitDao();

            protVisitDao.getProtocolVisits(protocol_id);

            return protVisitDao;
        } catch (Exception e) {
            Rlog
                    .fatal("protvisit",
                            "Exception In getProtocolVisits in ProtVisitAgentBean "
                                    + e);
        }
        return null;

    }
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public ProtVisitDao getProtocolVisits(int protocol_id,int initVisitSize,int limitVisitSize) {
        try {
            Rlog
                    .debug("protvisit",
                            "In getProtocolVisits in ProtocolVisitAgentBean line number 0");
            ProtVisitDao protVisitDao = new ProtVisitDao();

            protVisitDao.getProtocolVisits(protocol_id,initVisitSize,limitVisitSize);

            return protVisitDao;
        } catch (Exception e) {
            Rlog
                    .fatal("protvisit",
                            "Exception In getProtocolVisits in ProtVisitAgentBean "
                                    + e);
        }
        return null;

    }

    
    /**JM: 16Apr2008
    *
    * @param protocol_id, search
    */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public ProtVisitDao getProtocolVisits(int protocol_id, String search) {
        try {
            Rlog
                    .debug("protvisit",
                            "In getProtocolVisits in ProtocolVisitAgentBean line number 0");
            ProtVisitDao protVisitDao = new ProtVisitDao();

            protVisitDao.getProtocolVisits(protocol_id, search);

            return protVisitDao;
        } catch (Exception e) {
            Rlog
                    .fatal("protvisit",
                            "Exception In getProtocolVisits in ProtVisitAgentBean "
                                    + e);
        }
        return null;

    }




    /**
     * Calls getProtVisitDetails() on ProtVisit Entity Bean ProtVisitBean. Looks
     * up on the ProtVisit id parameter passed in and constructs and returns a
     * ProtVisit state holder. containing all the summary details of the
     * ProtVisit<br>
     *
     * @param ProtVisitId
     *            the ProtVisit id
     * @ee ProtVisitBean
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public ProtVisitBean getProtVisitDetails(int visit_id) {

        try {
            return (ProtVisitBean) em.find(ProtVisitBean.class, new Integer(
                    visit_id));
        }

        catch (Exception e) {
            System.out
                    .print("Error in getProtVisitDetails() in ProtVisitAgentBean"
                            + e);
            return null;
        }

    }

    /**
     * Calls setProtVisitDetails() on ProtVisit Entity Bean
     * ProtVisitBean.Creates a new ProtVisit with the values in the StateKeeper
     * object.
     *
     * @param ProtVisit
     *            a ProtVisit state keeper containing ProtVisit attributes for
     *            the new ProtVisit.
     * @return int - 0 if successful; -2 for Exception;
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int setProtVisitDetails(ProtVisitBean vdsk) {
        int status = 0;
        int dispVal =0;
        ProtVisitBean pv = new ProtVisitBean();

        try {
            Rlog.debug("protvisit", "entering ProtVisitAgentBean.");
            ProtVisitDao ProtVisitdao = new ProtVisitDao();

            System.out.println("visitid=" + vdsk.getVisit_id() + "visit name"
                    + vdsk.getName() + "protocol_id=" + vdsk.getProtocol_id()
                    + "displacement=" + vdsk.getDisplacement());
            
            //KM
            if(vdsk.getDisplacement() == null)
            	dispVal = 0;
            else
            	dispVal = StringUtil.stringToNum(vdsk.getDisplacement());
            //D-FIN-25-DAY0 BK JAN-23-2011
            int result = ProtVisitdao.ValidateVisit(vdsk.getProtocol_id(), -1,
                    vdsk.getName(), dispVal,vdsk.getDays(),vdsk.getWeeks(),vdsk.getMonths()); // SV, 10/24/04,
            // turned
            // duplicate
            // name check.
            System.out.println("visitid=" + vdsk.getVisit_id() + "visit name"
                    + vdsk.getName() + "protocol_id=" + vdsk.getProtocol_id()
                    + "displacement=" + vdsk.getDisplacement()
                    + "protvisitAgentBean:count=" + result);
            if (result < 0) {
                return result;
            }

            //em.merge(vdsk);
            em.persist(vdsk);
            
            
            return (vdsk.getVisit_id());
        } catch (Exception e) {
            Rlog.fatal("protvisit", "In the remote exception :" + e);
            e.printStackTrace();
            return -1;

        }
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int updateProtVisit(ProtVisitBean vdsk) {

        ProtVisitBean retrieved = null; // ProtVisit Entity Bean Remote Object
        int output;
        int dispVal =0;

        try {
        //BK,May/13/2011,Fix #6227
        	Query query = em.createNamedQuery("findInsertAfterVisits");        	
        	query.setParameter(1, vdsk.visit_id);
        	Number setAfterDependendent = (Number) query.getSingleResult(); 
        	if( setAfterDependendent.intValue() > 0 && "1".equals(vdsk.intervalFlag)){
        		return -5;
        	}
            ProtVisitDao ProtVisitdao = new ProtVisitDao();
            //KM
            if(vdsk.getDisplacement() == null)
            	dispVal = 0;
            else
            	dispVal = StringUtil.stringToNum(vdsk.getDisplacement());
            //D-FIN-25-DAY0 BK JAN-23-2011
            int pId = vdsk.getProtocol_id();
            int vId = vdsk.getVisit_id();
            String Name = vdsk.getName();
            Integer day = vdsk.getDays();
            int weeks = vdsk.getWeeks();
            int months =vdsk.getMonths();
            
            int result = ProtVisitdao.ValidateVisit(pId, vId,Name,dispVal,day,weeks,months); // SV,
            // 10/24/04,
            // turned
            // duplicate
            // name
            // check.

            System.out.println("visitid=" + vdsk.getVisit_id() + "visit name"
                    + vdsk.getName() + "protocol_id" + vdsk.getProtocol_id()
                    + "protvisitAgentBean:count=" + result);
            if (result < 0) {
                return result;
            }

            retrieved = (ProtVisitBean) em.find(ProtVisitBean.class,
                    new Integer(vdsk.getVisit_id()));

            if (retrieved == null) {
                return -4;
            }
            output = retrieved.updateProtVisit(vdsk);
        } catch (Exception e) {
            Rlog.fatal("protvisit",
                    "Error in updateProtVisit in ProtVisitAgentBean" + e);
            return -4;
        }
        return output;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int removeProtVisit(int visit_id) {

        ProtVisitBean retrieved = null; // ProtVisit Entity Bean Remote Object
        int output = 0;

        try {
        	
        	retrieved = (ProtVisitBean) em.find(ProtVisitBean.class,
                    new Integer(visit_id));
            em.remove(retrieved);
        } catch (Exception e) {
    
            Rlog.fatal("protvisit",
                    "Error in removeProtVisit  in ProtVisitAgentBean" + e);
            return -2;
        }
        return output;
    }

    // removeProtVisit() Overloaded for INF-18183 ::: Raviesh
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int removeProtVisit(int visit_id,Hashtable<String, String> args) {

        ProtVisitBean retrieved = null; // ProtVisit Entity Bean Remote Object
        int output = 0;

        try {
        	
        	AuditBean audit=null; //Audit Bean for AppDelete
            String currdate =DateUtil.getCurrentDate(); /*gets the current date from DateUtil*/
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appModule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the App Module Name from the Hashtable*/
            
            String getRidValue= AuditUtils.getRidValue("SCH_PROTOCOL_VISIT","esch","pk_protocol_visit="+visit_id);/*Fetches the RID/PK_VALUE*/ 
        	audit = new AuditBean("SCH_PROTOCOL_VISIT",String.valueOf(visit_id),getRidValue,
        			userID,currdate,appModule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
        	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
           
        	
        	retrieved = (ProtVisitBean) em.find(ProtVisitBean.class,
                    new Integer(visit_id));
            em.remove(retrieved);
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("protvisit",
                    "Error in removeProtVisit  in ProtVisitAgentBean" + e);
            return -2;
        }
        return output;
    }

    /** This method is not used anymore. Bug #6631
     * This method updates the displacement for all no interval visits out
     * @param protocol_visit_id
     * @return  -1 if unsuccessful, new protocol duration if successful
     */
    public int pushNoIntervalVisitsOut(int protocol_visit_id, String ip, int userId)
    {
         int ret=-1;
        try {
            ProtVisitDao protVisitDao = new ProtVisitDao();
            ret=protVisitDao.pushNoIntervalVisitsOut(protocol_visit_id, ip, userId);

        } catch (Exception e) {
            Rlog
                    .fatal("protvisit",
                            "Exception In pushNoIntervalVisitsOut in ProtVisitAgentBean "
                                    + e);
            e.printStackTrace();
        }
        return ret;
    }
    
    /** This method updates the displacement for all child records based on parent
     * @param protocol_visit_id
     * @return  -1 if unsuccessful, new protocol duration if successful
     */
    public int generateRipple(int protocol_visit_id,String calledFrom)
    {
         int ret=-1;
        try {
            ProtVisitDao protVisitDao = new ProtVisitDao();
            ret=protVisitDao.generateRipple(protocol_visit_id,calledFrom);

        } catch (Exception e) {
            Rlog
                    .fatal("protvisit",
                            "Exception In generateRipple in ProtVisitAgentBean "
                                    + e);
            e.printStackTrace();
        }
        return ret;
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int getMaxVisitNo(int protocol_id)
    {
         int ret=-1;
        try {
            ProtVisitDao protVisitDao = new ProtVisitDao();
            ret=protVisitDao.getMaxVisitNo(protocol_id);

        } catch (Exception e) {
            Rlog
                    .fatal("protvisit",
                            "Exception In getmaxvisitNo "
                                    + e);
            e.printStackTrace();
        }
        return ret;
    }
    

    // Added by Manimaran for the Enhancement #C8.3
    public int copyVisit(int protocolId,int frmVisitId, int visitId,String calledFrom, String usr, String ipAdd) {
        ProtVisitDao protDao = new ProtVisitDao();
        int ret = 0;
        try {
            Rlog.debug("protvisit", "ProtVisitAgentBean:copyVisit- 0");
            ret = protDao.copyVisit(protocolId,frmVisitId,visitId,calledFrom,usr,ipAdd);//KM-3447

        } catch (Exception e) {
            Rlog.fatal("protvisit", "ProtVisitAgentBean:Exception In copyVisit-"
                    + e);
            return -1;
        }
 
        return ret;

    }

    public ScheduleDao getVisitsByMonth(int patProdId, HashMap paramMap) {
        ScheduleDao schDao = new ScheduleDao();
        schDao.getVisitsByMonth(patProdId, paramMap);
        return schDao;
    }
    
  //BK,May/20/2011,Fixed #6014
    /**
     * This method validates the duration against day zero visits in the calendar.
     * @param duration The Calendar duration
     * @param protocolId Calendar Identifier
     * @param checkDayZero flag for day zero check.
     *        1-Check for day zero visit.
     *        2-Check for last day visit.
     * @param visitId visit Identifier
     * @return integer 
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int validateDayZeroVisit(int duration,int protocolId,int checkDayZero,int visitId) {
    	ProtVisitBean aProtVisitBean = new ProtVisitBean();
    	//aProtVisitBean.setVisit_id(protocolId);
        int output = 0;
        int dispVal = 0;

        try {
        	if(checkDayZero == 1 ){
        		if (0 == visitId){
        			Query query = em.createNamedQuery("findDayZeroVisit");        	
		        	query.setParameter(1, protocolId);
		        	Number DayZeroVisit = (Number) query.getSingleResult(); 
		        	return DayZeroVisit.intValue();
        		} else {
		        	//ProtVisitBean  	bProtVisitBean = em.find(ProtVisitBean.class,new Integer(visitId));        
		        	Query query = em.createNamedQuery("findDayZeroVisitCount");        	
		        	query.setParameter(1, protocolId);
		        	query.setParameter(2, 0);
		        	query.setParameter(3, visitId);
		        	Number DayZeroCount = (Number) query.getSingleResult(); 
		        	return DayZeroCount.intValue();
        		}
        	}
        	else{
        		Query query = em.createNamedQuery("findLastDayVisitCount");        	
            	query.setParameter(1, protocolId);
            	query.setParameter(2, duration);
            	query.setParameter(3, visitId);
            	Number DayZeroCount = (Number) query.getSingleResult();             	
            	return DayZeroCount.intValue();
        	}
    }
        catch(Exception e){
        	Rlog.fatal("protvisit", "ProtVisitAgentBean:Exception In validateDayZeroVisit-"
                    + e);
            return -6;
        }
    }   

    private int findGridContents(JSONArray dataGridArray,String key, String lookFor, int startLikeEnd){
    	JSONObject gridRecord = null;
    	String value = null;
    	try{
	    	for (int i = 0; i < dataGridArray.length(); ++i) { 			
				gridRecord = dataGridArray.getJSONObject(i);
				value = gridRecord.getString(key);
				value=(value == null)? null:value.trim();
				if (null == value || value.equals(""))
					return 0;
				switch(startLikeEnd){
				case 1:
					if (lookFor.equals(value)){
						return 1;
					}
					break;
				case 0:
					if (lookFor.startsWith(value)){
						return 1;
					}
					break;
				case -1:
					if (lookFor.contains(value)){
						return 1;
					}
					break;
				case 2:
					if (lookFor.endsWith(value)){
						return 1;
					}
					break;
				}
	    	}
    	} catch (Exception e){
    		
    	}
	    return 0;
    }

    private JSONObject findCircularRelations(JSONArray dataGridArray, JSONObject oRecordChild) throws JSONException{
    	JSONObject resultObj = new JSONObject();
    	resultObj.put("parentVisitSeq", -1);
		resultObj.put("isFake", false);

		boolean whileParent = true;
		SchCodeDao schcodedao = new SchCodeDao();
		int ntpd = schcodedao.getCodeId("timepointtype", "NTPD");
    	int ftp = schcodedao.getCodeId("timepointtype", "FTP");
    	int dtp = schcodedao.getCodeId("timepointtype", "DTP");
		HashMap myParentArray = new HashMap();

    	//int childVisitId = StringUtil.stringToNum(gridRecord.getString("visitId"));
		int childVisitSeq = StringUtil.stringToNum(oRecordChild.getString("visitSeq"));
		int childNoIntervalId=oRecordChild.getInt("noIntervalId");
		String childInsertAfterId=oRecordChild.getString("insertAfterId");
		String[] childInsertAfterIds = childInsertAfterId.split("/");
		childInsertAfterId = childInsertAfterIds[0];
		
		if (dtp == childNoIntervalId){
			if (whileParent){
				if ("-1".equals(childInsertAfterId)){
					//Fake Visit
					String fakeVisit = oRecordChild.getString("isFakeVisit");
					if (!StringUtil.isEmpty(fakeVisit)){
						String[] arrayFakeVisit=fakeVisit.split("/");
						fakeVisit = arrayFakeVisit[0];
						childInsertAfterId = arrayFakeVisit[2];
						if ("true".equals(fakeVisit)){
							if ("-1".equals(childInsertAfterId)){
								resultObj.put("result", false);
								resultObj.put("isFake", true);
								return resultObj;
							}
						}
					}else{
						resultObj.put("result", false);
						return resultObj;
					}
				}
			}

			while (whileParent){
				childVisitSeq = StringUtil.stringToNum(oRecordChild.getString("visitSeq"));
				childNoIntervalId=oRecordChild.getInt("noIntervalId");
				childInsertAfterId=oRecordChild.getString("insertAfterId");
				childInsertAfterIds = childInsertAfterId.split("/");
				JSONObject oRecordParent = null;

				boolean parentFound = false;
				for (int iPV = 0; iPV < dataGridArray.length(); ++iPV) {
					oRecordParent = dataGridArray.getJSONObject(iPV);
					
					if(childInsertAfterIds[0].equals(oRecordParent.get("visitId"))){
						parentFound = true;
						break;
					}
				}
				if (!parentFound){
					//Last parent found but not in updated list
					resultObj.put("result", false);
					return resultObj;
				}
				
				//String parentVisitId = oRecordParent.getString("visitId");
				String parentVisitSeq = oRecordParent.getString("visitSeq");
				
				if (childVisitSeq != StringUtil.stringToNum(parentVisitSeq)){
					//Match found
					String visitNameParent = oRecordParent.getString("visitName");
					if (null != myParentArray && myParentArray.containsKey(parentVisitSeq)){
						//Circular relation found
						resultObj.put("result", true);
						resultObj.put("parentVisitSeq", parentVisitSeq);
						return resultObj;
					} else {
						myParentArray.put(parentVisitSeq, 1);

						int parentNoIntervalId=oRecordParent.getInt("noIntervalId");
						if (parentNoIntervalId == ntpd){
							//Last parent found
							resultObj.put("result", false);
							return resultObj;
						}else if (parentNoIntervalId == ftp){
							//Last parent found
							resultObj.put("result", false);
							return resultObj;
						}else if (parentNoIntervalId == dtp){
							//There is parent left
							oRecordChild = oRecordParent;
						}
					}
				}
			}
	     }
		resultObj.put("result", false);
		return resultObj;
    }

    /**
     * The method to validate visits in a calendar.
     **/    

    public ProtVisitResponse validateVisits(String dataGridString,String updateString,ProtVisitBean vsk,int duration, String calStatus
    	    ,String calType,String deleteString,Hashtable<String, String> args){
    	int displacement;
    	int insertAfterInterval;
    	ProtVisitResponse visitResponse = new ProtVisitResponse();
    	JSONArray dataGridArray;
    	JSONArray updateArray;
    	JSONArray deleteArray;
    	JSONObject gridRecord = null;
    	JSONObject updateRecord = null;
    	ProtVisitBean visitBean=null,aVisitBean = null,bVisitBean = null,cVisitBean = null;
    	List<ProtVisitBean> visitBeanList = null;
    	int hideFlag = 0,offLineFlag = 0,beanHideFlag;
    	ProtVisitDao protVisitDao = new ProtVisitDao();
    	SchCodeDao schcodedao = new SchCodeDao();
    	int ntpd = schcodedao.getCodeId("timepointtype", "NTPD");
    	int ftp = schcodedao.getCodeId("timepointtype", "FTP");
    	int dtp = schcodedao.getCodeId("timepointtype", "DTP");
    	int fromCopyVisitId;
    	HashMap<Integer, Integer> copyVisitMap= new HashMap<Integer, Integer>();//Ak:Added for enhancement PCAL-20801
     try{
    	 int fakeVisitCount = 0;
    	 //fix #7109,BK,09/21/11
    	 int visitNum = this.getMaxVisitNo(vsk.getProtocol_id()); 
    	     dataGridArray = new JSONArray(dataGridString);   
    	     visitBeanList =  this.calculateDisplacement(dataGridArray);
    	    ArrayList<Integer> updatedVisitSuccessList = new ArrayList<Integer>();

			for (int i = 0; i < dataGridArray.length(); ++i) {
				gridRecord = dataGridArray.getJSONObject(i);
				bVisitBean = visitBeanList.get(i);
				visitResponse.setRowNumber(StringUtil.stringToNum(gridRecord.getString("visitSeq")));
                
				boolean isUpdate =false;
				boolean incompleteInfo = false;
				boolean circularRelation = false;

				if((!"[]".equals(updateString) || !StringUtil.isEmpty(updateString))) {
					updateArray = new JSONArray(updateString);
					for (int j = 0; j < updateArray.length(); ++j) {
						updateRecord = updateArray.getJSONObject(j);
						if(updateRecord.getInt("i") == gridRecord.getInt("visitSeq")){
							isUpdate = true;
							break;
						}
					}
				}

				String visitName = gridRecord.getString("visitName");
				if (StringUtil.isEmpty(visitName)){ //Visit Name is missing
					visitResponse.setVisitId(-16);
					visitResponse.setColumnKey("visitName");
       				break;
				}
				//==========================================================//
				
				int visitId = StringUtil.stringToNum(gridRecord.getString("visitId"));
				int visitSeq = StringUtil.stringToNum(gridRecord.getString("visitSeq"));
				int noIntervalId=gridRecord.getInt("noIntervalId");

				String insertAfter=gridRecord.getString("insertAfter");
				
				JSONObject dummyRecord= dataGridArray.getJSONObject(0);
				int count=0;
				int noIntId=dummyRecord.getInt("noIntervalId");
				if(noIntId==ntpd){
					 count++;
				 }
				if(count>0 || (noIntId==dtp && dummyRecord.getString("insertAfter").equals("Visit Not Listed"))){
				if(insertAfter.equals(LC.L_First_Visit)){
					visitResponse.setVisitId(-27);
					visitResponse.setColumnKey("insertAfter");
					break;
				}}
				
					if(insertAfter.equals(LC.L_Previous_Visit)){
						count=0;
						  for(int j=i;j>0;j--){
							 dummyRecord= dataGridArray.getJSONObject(j-1);
							 noIntId=dummyRecord.getInt("noIntervalId");
							 if(noIntId!=ntpd){
								 count++;
								 break;
							 }
						  }
					if(count==0){
						visitResponse.setVisitId(-28);
						visitResponse.setColumnKey("insertAfter");
						break;
					}else if(noIntId==dtp && dummyRecord.getString("insertAfter").equals("Visit Not Listed")){
						visitResponse.setVisitId(-28);
						visitResponse.setColumnKey("insertAfter");
						break;
					}
					}				
				String insertAfterId=gridRecord.getString("insertAfterId");
				String[] parentVisitId=insertAfterId.split("/");
				insertAfterInterval = StringUtil.stringToNum(gridRecord.getString("insertAfterInterval"));

				//==========================================================//

				if (dtp == noIntervalId){
					String p = gridRecord.getString("insertAfterInterval");
					String intervalUnit = gridRecord.getString("intervalUnit");

					if ((StringUtil.isEmpty(p) || insertAfterInterval == 0) || StringUtil.isEmpty(intervalUnit) || StringUtil.isEmpty(insertAfter)){
						visitResponse.setVisitId(-14);
						String colName = (StringUtil.isEmpty(p) || insertAfterInterval == 0)? "insertAfterInterval"
								: StringUtil.isEmpty(intervalUnit) ? "intervalUnit" : "insertAfter";
						visitResponse.setColumnKey(colName);
           				break;
					}					

					if (StringUtil.isEmpty(insertAfter) || LC.L_Select_AnOption.equals(insertAfter)) incompleteInfo = true;
					if (StringUtil.isEmpty(insertAfterId)) incompleteInfo = true;
					insertAfterId=StringUtil.isEmpty(insertAfterId)?"":insertAfterId;

					if (null == parentVisitId || parentVisitId.length < 1) 
						incompleteInfo = true;
					else if (StringUtil.isEmpty(parentVisitId[0])) 
						incompleteInfo = true;

					if(incompleteInfo){
           				break;
					} else {
						JSONObject resultObj = findCircularRelations(dataGridArray, gridRecord);
						
						if (null != resultObj){
							circularRelation = resultObj.getBoolean("result");
							
							if(circularRelation){
								visitResponse.setVisitId(-15);
			    				visitResponse.setParentVisit(
			    						StringUtil.stringToNum(resultObj.getString("parentVisitSeq")));
			    				visitResponse.setColumnKey("insertAfter");
		           				break;
							} else {
								//If visitResponse.VisitId > 0 i.e. no error then RowNumber will contain fakeCount
								if (resultObj.getBoolean("isFake")) fakeVisitCount++;
								visitResponse.setFakeCount(fakeVisitCount);
							}
						}
					}
				}
				//==========================================================//

				int duplicateFound = 0;
				
				for (int j = 0; j < dataGridArray.length(); ++j) {
					//bVisitBean = visitBeanList.get(j);
					if (i != j){
    					JSONObject dupGridRecord = null;
        				dupGridRecord = dataGridArray.getJSONObject(j);
        				
        				String dupVisitName = dupGridRecord.getString("visitName");
        				if (StringUtil.isEmpty(dupVisitName)){ //Visit Name is missing
        					visitResponse.setRowNumber(StringUtil.stringToNum(dupGridRecord.getString("visitSeq")));
        					visitResponse.setVisitId(-16);
        					visitResponse.setColumnKey("visitName");
               				break;
        				}

        				if(visitSeq != dupGridRecord.getInt("visitSeq")){
        					if((dupVisitName.toUpperCase()).equals(visitName.toUpperCase())){//Visit Name duplicate
        						duplicateFound = 1;
                   				break;
        					}
        				}
        				
        				//==========================================================//
        				if (ntpd == noIntervalId){
	        				String dupInsertAfter=dupGridRecord.getString("insertAfter");
	        				String dupInsertAfterIds=dupGridRecord.getString("insertAfterId");
	        				
	        				if (!StringUtil.isEmpty(dupInsertAfterIds)){
		        				String dupInsertAfterVisitId = dupInsertAfterIds.substring(0,dupInsertAfterIds.indexOf("/"));
		        				if(visitId == StringUtil.stringToNum(dupInsertAfterVisitId) || visitName == dupInsertAfter){
		        					visitResponse.setVisitId(-18);
		        					visitResponse.setRowNumber(StringUtil.stringToNum(dupGridRecord.getString("visitSeq")));
		        					visitResponse.setColumnKey("insertAfter");
		               				break;
		        				}
	        				}
        				}
        				
        				//==========================================================//
					}
				}

				if (duplicateFound > 0){
					visitResponse.setVisitId(-17);
					visitResponse.setColumnKey("visitName");
					break;
				}
				//==========================================================//

				if (!StringUtil.isEmpty(parentVisitId[0])){
					if((!"[]".equals(deleteString) && !StringUtil.isEmpty(deleteString))){
						deleteArray = new JSONArray(deleteString);
						for (int j = 0; j < deleteArray.length(); ++j) {
							JSONObject delGridRecord = null;
							delGridRecord = deleteArray.getJSONObject(j);
	
							String delVisitId = delGridRecord.getString("i");
							if(parentVisitId[0].equals(delVisitId)){// Parent Visit is a deleted visit
								visitResponse.setVisitId(-19);
								visitResponse.setColumnKey("insertAfter");
	               				break;
							}
						}
					}
				}
				//==========================================================//

				if (ftp == noIntervalId){
					String months=gridRecord.getString("months");
					String weeks=gridRecord.getString("weeks");
					String days=gridRecord.getString("days");
	
					String offlineFlg=gridRecord.getString("offlineFlag");
					
					boolean m, w, d;
					m = w = d = true;
	
					if (StringUtil.isEmpty(months))
			    		m = false;
	
					if (StringUtil.isEmpty(weeks))
						w = false;
	
					if (StringUtil.isEmpty(days))
						d = false;

					if (!m && !w && !d) {
						visitResponse.setVisitId(-20);
						String colName = (!m)? "months" : (!w)? "weeks" : "days";
						visitResponse.setColumnKey(colName);
           				break;
					}
					if (!NumberUtil.isInteger(months) && !NumberUtil.isInteger(weeks) && !NumberUtil.isInteger(days)){
						visitResponse.setVisitId(-21);
						visitResponse.setColumnKey("months");
           				break;
					}
					if ( m || w || d) {
						int iM = StringUtil.stringToNum(months);
						int iW = StringUtil.stringToNum(weeks);
						int iD = StringUtil.stringToNum(days);

						if(iD==0 && (iW > 1 || iM > 1)){
							visitResponse.setVisitId(-22);
							visitResponse.setColumnKey("days");
	           				break;
					    }
	    				displacement = StringUtil.stringToNum(bVisitBean.getDisplacement()); 
	    				
	    				if ("O".equals(calStatus) && "0".equals(offlineFlg) && (iD == 0 || displacement == 0)){
							if(visitId == 0 || isUpdate){
								visitResponse.setVisitId(-23);
								visitResponse.setColumnKey("days");
		           				break;
							}
	    				}

						if ((iM > 0) && (iW == 0) && (iD > 30)) {
							visitResponse.setVisitId(-24);
							visitResponse.setColumnKey("days");
	           				break;
						}

						if (iM > 0) {
							if (iW > 4) {
								visitResponse.setVisitId(-25);
								visitResponse.setColumnKey("weeeks");
		           				break;
							}

						}
						if (iW > 0) 	{
							if (iD > 7) {	
								visitResponse.setVisitId(-26);
								visitResponse.setColumnKey("days");
		           				break;
							}
						}
					}
				}
				//==========================================================//
				int intervalType =gridRecord.getInt("noIntervalId");
				bVisitBean = visitBeanList.get(i);
				displacement = StringUtil.stringToNum(bVisitBean.getDisplacement());    		            

				visitResponse.setVisitName(gridRecord.getString("visitName"));
				int offlineFlag=StringUtil.stringToNum(gridRecord.getString("offlineFlag")); //AK:Fixed Bug#7633
				fromCopyVisitId= StringUtil.stringToNum(gridRecord.getString("fromCopyVisitId"));//Ak:Added for enhancement PCAL-20801
				//*************Validate visit interval against duration of the calendar.   				  
				if(displacement >= duration){
					cVisitBean = this.validateDayZeroVisit(vsk, duration, i, 1, visitBeanList);
				} 
				    				
				//**************    				
				if(gridRecord.getInt("visitId")==0 ){
					int iInsertAfter = (bVisitBean.getInsertAfter()).intValue();
					if(displacement == 0 && calStatus.equals("O") && iInsertAfter != -1 && intervalType!=ntpd){ //YK:Modified for Bug#10009
    					visitResponse.setVisitId(-12);				
    					break;
    				}
					//fix #7056,BK,09/21/11 ,Fixed 7107 ,10/12/2011
    				if ((displacement >= duration && cVisitBean != null) && insertAfterInterval !=0 ){
    					visitResponse.setVisitId(-7);
    					break;
    				}    				
    				else if ((displacement >= duration && cVisitBean != null) && insertAfterInterval ==0 ){
    					visitResponse.setVisitId(-9);				
    					break;
    				}    			
    				//fix #7105,7107,BK,09/21/11
    				if((displacement == 1 &&  bVisitBean.getDays() == 0 && intervalType!=ntpd) 
    						|| displacement == 0){
    					//checkVisit = this.validateDayZeroVisit(duration,vsk.getProtocol_id(),0,gridRecord.getInt("visitId"));
    					cVisitBean = this.validateDayZeroVisit(vsk, duration, i, 0, visitBeanList);
    					}
    				if (cVisitBean != null){
    					visitResponse.setVisitId(-8);
    					break;
    				}
					visitBean = new ProtVisitBean();									//visit creation i.e if visitId is 0 and delete flag					 
					visitBean.setip_add(vsk.getip_add());								//is false then record has to be added in database.
					visitBean.setCreator(vsk.getCreator());
					visitBean.setProtocol_id(vsk.getProtocol_id());    					
					visitBean.setVisit_no(++visitNum);      //setting visitId 
					//SETTING THE HIDE FLAG
              	    hideFlag = gridRecord.getInt("hideFlag"); //AK:fixed BUG#7486
              	    if(hideFlag>0){
              	    	visitBean.setHideFlag(hideFlag);
              	    }
					visitBean.setProtVisitBeanJSON(gridRecord, 'C',bVisitBean); 
					//validating visit in request data
					cVisitBean = this.validateVisit(i, visitBeanList);
					if(cVisitBean != null){
						visitResponse.setVisitId(-2);
						break;
					}
					else{
						visitResponse.setVisitId(visitBean.getVisit_id());
						if(fromCopyVisitId>0 && visitBean.getVisit_id()>0){ //Ak:Added for enhancement PCAL-20801
							copyVisitMap.put(visitBean.getVisit_id(),fromCopyVisitId);
						}
					}    							

				}
				
				// visit updates
				else if((!"[]".equals(updateString) || !StringUtil.isEmpty(updateString))) {
                    if(isUpdate){
                		Query query = em.createNamedQuery("findInsertAfterVisits");        	
                       	query.setParameter(1, gridRecord.getInt("visitId"));
                       	Number setAfterDependendent = (Number) query.getSingleResult(); 
                       	if( setAfterDependendent.intValue() > 0 && intervalType==ntpd){ //Ak:Fixed BUG#7085
                       		int parentInGrid = this.findGridContents(dataGridArray, "insertAfterId", gridRecord.getString("visitId")+"/",0);
                       		if (parentInGrid > 0){
                       			visitResponse.setVisitId(-5);
                       			break;
                       		}
                       	}
                       	int iInsertAfter = (bVisitBean.getInsertAfter()).intValue();
                	   if(displacement == 0 && calStatus.equals("O") && offlineFlag==0 && iInsertAfter != -1 && intervalType!=ntpd){ //AK:Fixed Bug#7633,//YK:Modified for Bug#10009
                		   visitResponse.setVisitId(-12);				
                		   break;
                	   }
                    	 if(displacement == 1 &&  bVisitBean.getDays() == 0 && calStatus.equals("O") && offlineFlag=='0'){  //AK:Fixed Bug#7633
           					visitResponse.setVisitId(-13);				
           					break;
           					}
                    	 //fix #7056,BK,09/21/11
           				if ((displacement >= duration && cVisitBean != null) && insertAfterInterval !=0 ){
           					visitResponse.setVisitId(-7);				
           					break;
           				}    				
           				else if ((displacement >= duration && cVisitBean != null) && insertAfterInterval ==0 ){
           					visitResponse.setVisitId(-9);				
           					break;
           				}    			
           				//fix #7105,7107,BK,09/21/11
           				if((displacement == 1 &&  bVisitBean.getDays() == 0 && intervalType!=ntpd) 
           						|| displacement == 0){
           					//checkVisit = this.validateDayZeroVisit(duration,vsk.getProtocol_id(),0,gridRecord.getInt("visitId"));
           					cVisitBean = this.validateDayZeroVisit(vsk,duration, i, 0, visitBeanList);
           				}
           				if (cVisitBean != null){
           					visitResponse.setVisitId(-8);
           					break;
           				} 
                    
           				//D-FIN-25-DAY0 BK JAN-23-2011                           				 
                   		//validating visit in request data                   
                   		cVisitBean = this.validateVisit(i, visitBeanList);
                   		if(cVisitBean != null){
                   			visitResponse.setVisitId(-2);
                   			break;
                   		}
                       	aVisitBean = new ProtVisitBean();
                        aVisitBean = (ProtVisitBean) em.find(ProtVisitBean.class,new Integer(gridRecord.getInt("visitId")));

                        if (aVisitBean == null) {
                        	visitResponse.setVisitId(-4);
                     		break;
                        }

                  	   aVisitBean.setModifiedBy(vsk.getCreator());
                  	   //SETTING THE HIDE FLAG
                  	    hideFlag = gridRecord.getInt("hideFlag");
                  	    offLineFlag = aVisitBean.getOfflineFlag() == null? 0: aVisitBean.getOfflineFlag();
                  	    beanHideFlag = aVisitBean.getHideFlag() == null? 0: aVisitBean.getHideFlag();
                  	    if(beanHideFlag != hideFlag){
                  	    	aVisitBean.setHideFlag(hideFlag);
                  	    }
                       aVisitBean.setProtVisitBeanJSON(gridRecord, 'U', bVisitBean);
                       visitResponse.setVisitId(aVisitBean.getVisit_id());
                       em.clear();
                       // Use this list to copy displacement from visit to event later in copyDisplacementsFromVisitsToEvents
                       updatedVisitSuccessList.add(gridRecord.getInt("visitId"));
                        
                	} // end of isUpdate
				}
			}
 		} catch(Exception e){
			Rlog.fatal("protvisit","Exception In validateVisits in ProtVisitAgentBean "
			         + e);
			visitResponse.setVisitId(-11);
			context.setRollbackOnly();
			return visitResponse ;
	     }     	
	        
		if(visitResponse.getVisitId() < 0){
			context.setRollbackOnly();
		}
	   	return visitResponse ;	
	}

    /**
     * The method to add,modify and delete visits in a calendar.
     **/    
    
    //Modified for INF-18183 and BUG#7224 : Raviesh
    public ProtVisitResponse manageVisits(String dataGridString,String updateString,ProtVisitBean vsk,int duration, String calStatus
    	    ,String calType,String deleteString,Hashtable<String, String> args){
    	    	
    	int displacement,checkVisit;
    	int insertAfterInterval;
    	ProtVisitResponse visitResponse = new ProtVisitResponse();
    	JSONArray dataGridArray;
    	JSONArray updateArray;
    	JSONArray deleteArray;
    	JSONObject gridRecord = null;
    	JSONObject updateRecord = null;
    	JSONObject deleteRecord = null;
    	ProtVisitBean visitBean=null,aVisitBean = null,bVisitBean = null,cVisitBean = null;
    	List<ProtVisitBean> visitBeanList = null;
    	int hideFlag = 0,offLineFlag = 0,beanHideFlag;
    	ProtVisitDao protVisitDao = new ProtVisitDao();
    	SchCodeDao schcodedao = new SchCodeDao();
    	int ntpd = schcodedao.getCodeId("timepointtype", "NTPD");
    	int ftp = schcodedao.getCodeId("timepointtype", "FTP");
    	int dtp = schcodedao.getCodeId("timepointtype", "DTP");
    	String tableName = null;
    	int fromCopyVisitId;
    	HashMap<Integer, Integer> copyVisitMap= new HashMap<Integer, Integer>();//Ak:Added for enhancement PCAL-20801
     try{
    	 int fakeVisitCount = 0;
    	 //fix #7109,BK,09/21/11
    	 int visitNum = this.getMaxVisitNo(vsk.getProtocol_id()); 
    	     dataGridArray = new JSONArray(dataGridString);   
    	     visitBeanList =  this.calculateDisplacement(dataGridArray);    	     
    	     
    	    if((!"[]".equals(deleteString) && !StringUtil.isEmpty(deleteString))){
    	    	deleteArray = new JSONArray(deleteString);
    	    	int visitId = 0;
    			boolean isUpdate = false;
    			//deletion
    			if(calType.equals("L") || calType.equals("P"))
    				tableName = "event_def";
    			else
    				tableName = "event_assoc";

    			ArrayList<String> deleteIdArr = new ArrayList();
    			ArrayList<String> flagArr = new ArrayList();
    			for (int j = 0; j < deleteArray.length(); ++j) {
    				deleteRecord = deleteArray.getJSONObject(j);
    				visitId = deleteRecord.getInt("i");
    				deleteIdArr.add(deleteRecord.getString("i"));
    				flagArr.add("1");
    			}
    			String[] deleteIds = new String[deleteIdArr.size()];
    			deleteIds = deleteIdArr.toArray(deleteIds); 
    			String[] flags = new String[flagArr.size()];
    			flags = flagArr.toArray(flags); 
    			
    			EventdefDao aEventDefDao = new EventdefDao();
				//Modified for INF-18183 and BUG#7224 : Raviesh
				aEventDefDao.deleteEvtOrVisits(deleteIds, tableName, flags, Integer.parseInt(vsk.getCreator()),args);
    	    }
    	    ArrayList<Integer> updatedVisitSuccessList = new ArrayList<Integer>();

			for (int i = 0; i < dataGridArray.length(); ++i) {
				gridRecord = dataGridArray.getJSONObject(i);
				bVisitBean = visitBeanList.get(i);
				visitResponse.setRowNumber(StringUtil.stringToNum(gridRecord.getString("visitSeq")));

				boolean isUpdate =false;
				boolean incompleteInfo = false;
				boolean circularRelation = false;

				if((!"[]".equals(updateString) || !StringUtil.isEmpty(updateString))) {
					updateArray = new JSONArray(updateString);
					for (int j = 0; j < updateArray.length(); ++j) {
						updateRecord = updateArray.getJSONObject(j);
						if(updateRecord.getInt("i") == gridRecord.getInt("visitSeq")){
							isUpdate = true;
							break;
						}
					}
				}

				String visitName = gridRecord.getString("visitName");
				if (StringUtil.isEmpty(visitName)){ //Visit Name is missing
					visitResponse.setVisitId(-16);
					visitResponse.setColumnKey("visitName");
       				break;
				}
				//==========================================================//
				
				int visitId = StringUtil.stringToNum(gridRecord.getString("visitId"));
				int visitSeq = StringUtil.stringToNum(gridRecord.getString("visitSeq"));
				int noIntervalId=gridRecord.getInt("noIntervalId");

				String insertAfter=gridRecord.getString("insertAfter");
				String insertAfterId=gridRecord.getString("insertAfterId");
				String[] parentVisitId=insertAfterId.split("/");
				insertAfterInterval = StringUtil.stringToNum(gridRecord.getString("insertAfterInterval"));

				//==========================================================//

				if (dtp == noIntervalId){
					String p = gridRecord.getString("insertAfterInterval");
					String intervalUnit = gridRecord.getString("intervalUnit");

					if ((StringUtil.isEmpty(p) || insertAfterInterval == 0) || StringUtil.isEmpty(intervalUnit) || StringUtil.isEmpty(insertAfter)){
						visitResponse.setVisitId(-14);
						String colName = (StringUtil.isEmpty(p) || insertAfterInterval == 0)? "insertAfterInterval"
								: StringUtil.isEmpty(intervalUnit) ? "intervalUnit" : "insertAfter";
						visitResponse.setColumnKey(colName);
           				break;
					}					

					if (StringUtil.isEmpty(insertAfter) || LC.L_Select_AnOption.equals(insertAfter)) incompleteInfo = true;
					if (StringUtil.isEmpty(insertAfterId)) incompleteInfo = true;
					insertAfterId=StringUtil.isEmpty(insertAfterId)?"":insertAfterId;

					if (null == parentVisitId || parentVisitId.length < 1) 
						incompleteInfo = true;
					else if (StringUtil.isEmpty(parentVisitId[0])) 
						incompleteInfo = true;

					if(incompleteInfo){
           				break;
					} else {
						JSONObject resultObj = findCircularRelations(dataGridArray, gridRecord);
						
						if (null != resultObj){
							circularRelation = resultObj.getBoolean("result");
							
							if(circularRelation){
								visitResponse.setVisitId(-15);
			    				visitResponse.setParentVisit(
			    						StringUtil.stringToNum(resultObj.getString("parentVisitSeq")));
			    				visitResponse.setColumnKey("insertAfter");
		           				break;
							} else {
								//If visitResponse.VisitId > 0 i.e. no error then RowNumber will contain fakeCount
								if (resultObj.getBoolean("isFake")) fakeVisitCount++;
								visitResponse.setFakeCount(fakeVisitCount);
							}
						}
					}
				}
				//==========================================================//

				int duplicateFound = 0;
				
				for (int j = 0; j < dataGridArray.length(); ++j) {
					//bVisitBean = visitBeanList.get(j);
					if (i != j){
    					JSONObject dupGridRecord = null;
        				dupGridRecord = dataGridArray.getJSONObject(j);
        				
        				String dupVisitName = dupGridRecord.getString("visitName");
        				if (StringUtil.isEmpty(dupVisitName)){ //Visit Name is missing
        					visitResponse.setRowNumber(StringUtil.stringToNum(dupGridRecord.getString("visitSeq")));
        					visitResponse.setVisitId(-16);
        					visitResponse.setColumnKey("visitName");
               				break;
        				}

        				if(visitSeq != dupGridRecord.getInt("visitSeq")){
        					if((dupVisitName.toUpperCase()).equals(visitName.toUpperCase())){//Visit Name duplicate
        						duplicateFound = 1;
                   				break;
        					}
        				}
        				
        				//==========================================================//
        				if (ntpd == noIntervalId){
	        				String dupInsertAfter=dupGridRecord.getString("insertAfter");
	        				String dupInsertAfterIds=dupGridRecord.getString("insertAfterId");
	        				
	        				if (!StringUtil.isEmpty(dupInsertAfterIds)){
		        				String dupInsertAfterVisitId = dupInsertAfterIds.substring(0,dupInsertAfterIds.indexOf("/"));
		        				if(visitId == StringUtil.stringToNum(dupInsertAfterVisitId) || visitName == dupInsertAfter){
		        					visitResponse.setVisitId(-18);
		        					visitResponse.setRowNumber(StringUtil.stringToNum(dupGridRecord.getString("visitSeq")));
		        					visitResponse.setColumnKey("insertAfter");
		               				break;
		        				}
	        				}
        				}
        				
        				//==========================================================//
					}
				}

				if (duplicateFound > 0){
					visitResponse.setVisitId(-17);
					visitResponse.setColumnKey("visitName");
					break;
				}
				//==========================================================//

				if (!StringUtil.isEmpty(parentVisitId[0])){
					if((!"[]".equals(deleteString) && !StringUtil.isEmpty(deleteString))){
						deleteArray = new JSONArray(deleteString);
						for (int j = 0; j < deleteArray.length(); ++j) {
							JSONObject delGridRecord = null;
							delGridRecord = deleteArray.getJSONObject(j);
	
							String delVisitId = delGridRecord.getString("i");
							if(parentVisitId[0].equals(delVisitId)){// Parent Visit is a deleted visit
								visitResponse.setVisitId(-19);
								visitResponse.setColumnKey("insertAfter");
	               				break;
							}
						}
					}
				}
				//==========================================================//

				if (ftp == noIntervalId){
					String months=gridRecord.getString("months");
					String weeks=gridRecord.getString("weeks");
					String days=gridRecord.getString("days");
	
					String offlineFlg=gridRecord.getString("offlineFlag");
					
					boolean m, w, d;
					m = w = d = true;
	
					if (StringUtil.isEmpty(months))
			    		m = false;
	
					if (StringUtil.isEmpty(weeks))
						w = false;
	
					if (StringUtil.isEmpty(days))
						d = false;

					if (!m && !w && !d) {
						visitResponse.setVisitId(-20);
						String colName = (!m)? "months" : (!w)? "weeks" : "days";
						visitResponse.setColumnKey(colName);
           				break;
					}
					if (!NumberUtil.isInteger(months) && !NumberUtil.isInteger(weeks) && !NumberUtil.isInteger(days)){
						visitResponse.setVisitId(-21);
						visitResponse.setColumnKey("months");
           				break;
					}
					if ( m || w || d) {
						int iM = StringUtil.stringToNum(months);
						int iW = StringUtil.stringToNum(weeks);
						int iD = StringUtil.stringToNum(days);

						if(iD==0 && (iW > 1 || iM > 1)){
							visitResponse.setVisitId(-22);
							visitResponse.setColumnKey("days");
	           				break;
					    }
	    				displacement = StringUtil.stringToNum(bVisitBean.getDisplacement()); 
	    				
	    				if ("O".equals(calStatus) && "0".equals(offlineFlg) && (iD == 0 || displacement == 0)){
							if(visitId == 0 || isUpdate){
								visitResponse.setVisitId(-23);
								visitResponse.setColumnKey("days");
		           				break;
							}
	    				}

						if ((iM > 0) && (iW == 0) && (iD > 30)) {
							visitResponse.setVisitId(-24);
							visitResponse.setColumnKey("days");
	           				break;
						}

						if (iM > 0) {
							if (iW > 4) {
								visitResponse.setVisitId(-25);
								visitResponse.setColumnKey("weeeks");
		           				break;
							}

						}
						if (iW > 0) 	{
							if (iD > 7) {	
								visitResponse.setVisitId(-26);
								visitResponse.setColumnKey("days");
		           				break;
							}
						}
					}
				}
				//==========================================================//
    				int intervalType =gridRecord.getInt("noIntervalId");
    				bVisitBean = visitBeanList.get(i);
    				displacement = StringUtil.stringToNum(bVisitBean.getDisplacement());    		            

    				visitResponse.setVisitName(gridRecord.getString("visitName"));
    				int offlineFlag=StringUtil.stringToNum(gridRecord.getString("offlineFlag")); //AK:Fixed Bug#7633
    				fromCopyVisitId= StringUtil.stringToNum(gridRecord.getString("fromCopyVisitId"));//Ak:Added for enhancement PCAL-20801
    				//*************Validate visit interval against duration of the calendar.   				  
    				if(displacement >= duration){
    					cVisitBean = this.validateDayZeroVisit(vsk, duration, i, 1, visitBeanList);
    				} 
    				    				
    				//**************    				
    				if(gridRecord.getInt("visitId")==0 ){
    					int iInsertAfter = (bVisitBean.getInsertAfter()).intValue();
    					if(displacement == 0 && calStatus.equals("O") && iInsertAfter != -1 && intervalType!=ntpd){ //YK:Modified for Bug#10009
        					visitResponse.setVisitId(-12);
        					break;
        				}
    					//fix #7056,BK,09/21/11 ,Fixed 7107 ,10/12/2011
        				if ((displacement >= duration && cVisitBean != null) && insertAfterInterval !=0 ){
        					visitResponse.setVisitId(-7);
        					break;
            			}
    					else if ((displacement >= duration && cVisitBean != null) && insertAfterInterval ==0 ){
    						visitResponse.setVisitId(-9);
    						break;
            			}
        				//fix #7105,7107,BK,09/21/11
    					if((displacement == 1 &&  bVisitBean.getDays() == 0 && intervalType!=ntpd) 
    						|| displacement == 0){
    						//checkVisit = this.validateDayZeroVisit(duration,vsk.getProtocol_id(),0,gridRecord.getInt("visitId"));
    						cVisitBean = this.validateDayZeroVisit(vsk, duration, i, 0, visitBeanList);
    					}
    					if (cVisitBean != null){
    						visitResponse.setVisitId(-8);
    						break;
    					}
    					visitBean = new ProtVisitBean();									//visit creation i.e if visitId is 0 and delete flag					 
    					visitBean.setip_add(vsk.getip_add());								//is false then record has to be added in database.
    					visitBean.setCreator(vsk.getCreator());
    					visitBean.setProtocol_id(vsk.getProtocol_id());    					
    					visitBean.setVisit_no(++visitNum);      //setting visitId 
    					//SETTING THE HIDE FLAG
    					hideFlag = gridRecord.getInt("hideFlag"); //AK:fixed BUG#7486
    					if(hideFlag>0){
    						visitBean.setHideFlag(hideFlag);
    					}
    					visitBean.setProtVisitBeanJSON(gridRecord, 'C',bVisitBean); 
    					//validating visit in request data
    					cVisitBean = this.validateVisit(i, visitBeanList);
    					if(cVisitBean != null){
    						visitResponse.setVisitId(-2);
    						break;
    					}
    					else{
    						if(gridRecord.getString("insertAfter").equals(LC.L_First_Visit)||gridRecord.getString("insertAfter").equals(LC.L_Previous_Visit))
    						{visitBean.setInsertAfter(0);}
    						em.persist(visitBean);
    						
    						visitResponse.setVisitId(visitBean.getVisit_id());
    						if(fromCopyVisitId>0 && visitBean.getVisit_id()>0){ //Ak:Added for enhancement PCAL-20801
    							copyVisitMap.put(visitBean.getVisit_id(),fromCopyVisitId);
    						}
    					}

    				}
    				
    				// visit updates
    				else if((!"[]".equals(updateString) || !StringUtil.isEmpty(updateString))) {
                        if(isUpdate){
                    		Query query = em.createNamedQuery("findInsertAfterVisits");        	
                           	query.setParameter(1, gridRecord.getInt("visitId"));
                           	Number setAfterDependendent = (Number) query.getSingleResult(); 
                           	if( setAfterDependendent.intValue() > 0 && intervalType==ntpd){ //Ak:Fixed BUG#7085
                           		int parentInGrid = this.findGridContents(dataGridArray, "insertAfterId", gridRecord.getString("visitId")+"/",0);
                           		if (parentInGrid > 0){
                           			visitResponse.setVisitId(-5);
                           			break;
                           		}
                           	}
                           	int iInsertAfter = (bVisitBean.getInsertAfter()).intValue();
                    	   if(displacement == 0 && calStatus.equals("O") && offlineFlag==0 && iInsertAfter != -1 && intervalType!=ntpd){ //AK:Fixed Bug#7633,//YK:Modified for Bug#10009
                    		   visitResponse.setVisitId(-12);				
                    		   break;
                    	   }
                    	   if(displacement == 1 &&  bVisitBean.getDays() == 0 && calStatus.equals("O") && offlineFlag=='0'){  //AK:Fixed Bug#7633
                    		   visitResponse.setVisitId(-13);
                    		   break;
                    	   }
                    	   //fix #7056,BK,09/21/11
                    	   if ((displacement >= duration && cVisitBean != null) && insertAfterInterval !=0 ){
                    		   visitResponse.setVisitId(-7);
                    		   break;
                    	   }
                    	   else if ((displacement >= duration && cVisitBean != null) && insertAfterInterval ==0 ){
               					visitResponse.setVisitId(-9);				
               					break;
               				}
               				//fix #7105,7107,BK,09/21/11
               				if((displacement == 1 &&  bVisitBean.getDays() == 0 && intervalType!=ntpd) 
               						|| displacement == 0){
               					//checkVisit = this.validateDayZeroVisit(duration,vsk.getProtocol_id(),0,gridRecord.getInt("visitId"));
               					cVisitBean = this.validateDayZeroVisit(vsk,duration, i, 0, visitBeanList);
               				}
               				if (cVisitBean != null){
               					visitResponse.setVisitId(-8);
               					break;
               				} 
                        
               				//D-FIN-25-DAY0 BK JAN-23-2011                           				 
                       		//validating visit in request data                   
                       		cVisitBean = this.validateVisit(i, visitBeanList);
                       		if(cVisitBean != null){
                       			visitResponse.setVisitId(-2);
                       			break;
                       		}
                           	aVisitBean = new ProtVisitBean();
                            aVisitBean = (ProtVisitBean) em.find(ProtVisitBean.class,new Integer(gridRecord.getInt("visitId")));

                            if (aVisitBean == null) {
                            	visitResponse.setVisitId(-4);
                         		break;
                            }

                      	   aVisitBean.setModifiedBy(vsk.getCreator());
                      	   //SETTING THE HIDE FLAG
                      	    hideFlag = gridRecord.getInt("hideFlag");
                      	    offLineFlag = aVisitBean.getOfflineFlag() == null? 0: aVisitBean.getOfflineFlag();
                      	    beanHideFlag = aVisitBean.getHideFlag() == null? 0: aVisitBean.getHideFlag();
                      	    if(beanHideFlag != hideFlag){
                      	    	aVisitBean.setHideFlag(hideFlag);
                      	    }
                           aVisitBean.setProtVisitBeanJSON(gridRecord, 'U', bVisitBean);
                           visitResponse.setVisitId(aVisitBean.getVisit_id());
                           em.merge(aVisitBean);
                           
                           // Use this list to copy displacement from visit to event later in copyDisplacementsFromVisitsToEvents
                           updatedVisitSuccessList.add(gridRecord.getInt("visitId"));
                            
                    	} // end of isUpdate
    				}
    			}
    			visitResponse.setUpdatedVisitList(updatedVisitSuccessList);
     		}
    	     catch(Exception e){
    	    	 Rlog.fatal("protvisit","Exception In manageVisit in ProtVisitAgentBean "
    	                 + e);
    	    	 visitResponse.setVisitId(-11);
    	    	 context.setRollbackOnly();
    				 return visitResponse ;
    	     }     	
    	        
    			if(visitResponse.getVisitId() < 0){
    				context.setRollbackOnly();
    			}
    			visitResponse.setCopyVisitMap(copyVisitMap); //Ak:Added for enhancement PCAL-20801
    	    	return visitResponse ;	
    	    }
    	    
    	    /**
    	     * Copy the displacements from visits to the associated events in EVENT_DEF or EVENT_ASSOC.
    	     * If the updatedVisitList is empty, it will do nothing.
    	     * This method is intended to be called by the same caller right after the above manageVisit() method completes.
    	     * 
    	     * @param updatedVisitList
    	     * @param protocolId
    	     * @param dataGridString
    	     * @param updateString
    	     * @param calType
    	     */
    	    public void copyDisplacementsFromVisitsToEvents(ArrayList<Integer> updatedVisitList, int protocolId, String calType) {
    	    	if (updatedVisitList == null) { return; }
    	    	ProtVisitDao protVisitDao = new ProtVisitDao();
    	    	for(int iX=0; iX<updatedVisitList.size(); iX++) {
    	        	protVisitDao.updateVisitEvents(protocolId, updatedVisitList.get(iX), calType);
    	    	}
    	    }
    	    
    /**
     * Calculating Displacement based on parent visit or interval.
     * @param gridRecord
     * @return
     * @throws Exception
     */
    private List<ProtVisitBean> calculateDisplacement(JSONArray dataGridArray) throws Exception{
    	JSONObject gridRecord = null;
    	List<ProtVisitBean> aVisitBeanList = new ArrayList<ProtVisitBean>();
    	int displacement = 0;
    	SchCodeDao schcodedao = new SchCodeDao();
		int ntpd = schcodedao.getCodeId("timepointtype", "NTPD");
    	boolean flag=true;;
    	ProtVisitBean aTempBean = null;     	
    	for (int i = 0; i < dataGridArray.length(); ++i) { 			
			gridRecord = dataGridArray.getJSONObject(i); 
			aTempBean = new ProtVisitBean();
			int intervalType =gridRecord.getInt("noIntervalId");
	    	int insertAfterInterval = StringUtil.stringToNum(gridRecord.getString("insertAfterInterval"));
			String insertAfterDispStr = gridRecord.getString("insertAfterId");
			String insertAf=gridRecord.getString("insertAfter");
			if(LC.L_First_Visit.equals(insertAf)||LC.L_Previous_Visit.equals(insertAf)) flag=true;
			String insertAfterIntervalUnit = gridRecord.getString("intervalUnit");
			String monthStr = gridRecord.getString("months");
			String weekStr = gridRecord.getString("weeks");
			String dayStr = gridRecord.getString("days");
			String dispStr = "";
			int iInsertAfter = 0,prevdisp,interval,months,weeks,days;
			prevdisp = 0;
			int noOfDays = 0;
			String strIntervalType = "";
			if (intervalType == gridRecord.getInt("noTimePointId")){
				strIntervalType = "1";
			} else {
				if ((insertAfterInterval != 0)) {
					int pos = insertAfterDispStr.indexOf('/');
					if (pos > 0 ) {
						if(flag){
						String insertAfterStr =  insertAfterDispStr.substring(0, pos);
						dispStr = insertAfterDispStr.substring(pos+1);
						iInsertAfter = StringUtil.stringToNum(insertAfterStr);
						prevdisp = StringUtil.stringToNum(dispStr);
						if(LC.L_First_Visit.equals(insertAf)&& "-7".equalsIgnoreCase(dispStr)){
							if(!aVisitBeanList.get(0).getInsertAfter().equals(-1))
								prevdisp=aVisitBeanList.get(0).getDays()==0?0:Integer.parseInt(aVisitBeanList.get(0).getDisplacement());
							else{
								prevdisp = -1;
								iInsertAfter = -1;
							}
						}
						if(LC.L_Previous_Visit.equals(insertAf)&& "-9".equalsIgnoreCase(dispStr)){
							int count=0;
		    				  for(int j=i;j>0;j--){
		    					 JSONObject dummyRecord= dataGridArray.getJSONObject(j-1);
		    					 int noIntervalId=dummyRecord.getInt("noIntervalId");
		    					 if(noIntervalId!=ntpd){
		    						 count=j-1;
		    						 break;
		    					 }
		    						 
		    				  }
							
							if(!aVisitBeanList.get(count).getInsertAfter().equals(-1))
								prevdisp=aVisitBeanList.get(count).getDays()==0?0:Integer.parseInt(aVisitBeanList.get(count).getDisplacement());	
							else{
								prevdisp = -1;
								iInsertAfter = -1;
							}
							}
						
						}
					}
					 months = 0;weeks = 0; days = 0;
					 //FIX#7010		 
					if (insertAfterIntervalUnit.equals("Months"))
						interval = insertAfterInterval *30;
					else if (insertAfterIntervalUnit.equals("Weeks"))
						interval = insertAfterInterval *7;
					else //days
						interval = insertAfterInterval;
					displacement = prevdisp + interval;
				}
				else {
					months = StringUtil.stringToNum(monthStr);
					weeks = StringUtil.stringToNum(weekStr);
					days = StringUtil.stringToNum(dayStr);
		
					if (months == 0) months++;
					if (weeks == 0) weeks++;
					if (days == 0) days++;
					displacement = (months - 1)*30 + (weeks - 1)*7 + days;
		
		
					displacement = 0;
					if (months > 0) displacement+= (months -1)*30;
					if (weeks > 0) displacement+= (weeks -1)*7;
					displacement += days;  		// Imp: in case of calculated day zero visits, displacement = 0
					iInsertAfter = 0;
				}
				//DFIN-25
		        if(!StringUtil.isEmpty(gridRecord.getString("days")) && iInsertAfter == 0){               
		    		noOfDays = gridRecord.getInt("days");
		        	 }
		        else if(iInsertAfter != 0){
		        	noOfDays = displacement;
		    		}
		        else {
		    	    noOfDays = 1; 
		        }
		     	strIntervalType = "0";
			}
			months = StringUtil.stringToNum(monthStr);
			weeks = StringUtil.stringToNum(weekStr);
			aTempBean.setName(gridRecord.getString("visitName"));
			aTempBean.setVisit_id(gridRecord.getInt("visitId"));
			aTempBean.setMonths(months);
			aTempBean.setWeeks(weeks);
			aTempBean.setDays(noOfDays);
			aTempBean.setInsertAfter(iInsertAfter);
		    aTempBean.setIntervalFlag(strIntervalType);
			aTempBean.setDisplacement(""+displacement);
	    	aVisitBeanList.add(aTempBean);
    	
    	}
    	return aVisitBeanList;
    	
    }
 /**
  *   Fixed #6994
  * @param index
  * @param pVisitBeanList
  * @return
  */
    private ProtVisitBean validateVisit(int index,List<ProtVisitBean> pVisitBeanList){
    	ProtVisitBean aVisitBean = pVisitBeanList.get(index);
    	int disp,days,weeks,months;
    	ProtVisitBean bVisitBean = null;
    	for(int i=0;i<pVisitBeanList.size();i++){
    		bVisitBean = pVisitBeanList.get(i);
    		disp = Integer.parseInt(bVisitBean.getDisplacement());
    		months = bVisitBean.getMonths();
    		days = bVisitBean.getDays();
    		weeks = bVisitBean.getWeeks();    
    		
    		if(i!=index){
    			int bWeekMonth = bVisitBean.getWeeks()|bVisitBean.getMonths();
    			int aWeekMonth = aVisitBean.getWeeks()|aVisitBean.getMonths();
    			if( (Integer.parseInt(aVisitBean.getDisplacement()) == 1 ||Integer.parseInt(aVisitBean.getDisplacement()) == 0) 
    					&& aVisitBean.getDays() == 0 &&(Integer.parseInt(bVisitBean.getDisplacement()) == 7 && bWeekMonth == 1)) {
    				// day 7 condition
    				return aVisitBean;

    			}
    			else if((Integer.parseInt(aVisitBean.getDisplacement()) == 7 && aWeekMonth == 1) &&( (Integer.parseInt(bVisitBean.getDisplacement()) == 1 
    					||Integer.parseInt(bVisitBean.getDisplacement()) == 0) 
    					&& bVisitBean.getDays() == 0 )){
    				//day 0 condition
    				return aVisitBean;
    			}

    		}
    	}

    	
    	
    	return  null;
    }
    /**
     * 
     * @param index
     * @param pVisitBeanList
     * @return
     */
    private ProtVisitBean validateDayZeroVisit(ProtVisitBean vsk, int duration,int index,int checkDayZero,List<ProtVisitBean> pVisitBeanList){
    	ProtVisitBean aVisitBean = pVisitBeanList.get(index);
    	String aIntervalFlag = aVisitBean.getIntervalFlag();
    	int aInsertAfter = (aVisitBean.getInsertAfter()).intValue();
    	
    	if ("0".equals(aIntervalFlag) && (-1 != aInsertAfter)){
	    	int days,weeks,months;
			int visitDisp = Integer.parseInt(aVisitBean.getDisplacement());

	    	for(int i=0;i<pVisitBeanList.size();i++){
		    	ProtVisitBean bVisitBean = null;
	    		bVisitBean = pVisitBeanList.get(i);
    			int disp = Integer.parseInt(bVisitBean.getDisplacement());
	    		String bIntervalFlag = bVisitBean.getIntervalFlag();
	    		int bInsertAfter = (bVisitBean.getInsertAfter()).intValue();

	    		if(i!=index){
		    		if( checkDayZero == 0 && disp == duration ) {
	    				if((disp == 1 ||disp == 0) 
	        				&& bVisitBean.getDays() == 0 ){
	        				//day 0 condition
	        				if (duration != 1)
	        					return aVisitBean;
	    				}else{
		    				// last day condition
		    				return aVisitBean;
	    				}
	    			}
	    			else if(checkDayZero == 1){
			    		if ("0".equals(bIntervalFlag)){
				    		if (disp != 0){
					    		months = bVisitBean.getMonths();
					    		days = bVisitBean.getDays();
					    		weeks = bVisitBean.getWeeks();    
	
				    			int bWeekMonth = bVisitBean.getWeeks()|bVisitBean.getMonths();
				    			int aWeekMonth = aVisitBean.getWeeks()|aVisitBean.getMonths();

			    				if (visitDisp == 1 || visitDisp == 0){
			    					if (aVisitBean.getDays() == 0 ){
			    						//day 0 condition
			    						if (disp == 1 && duration != 1)
			    							return aVisitBean;
			    					}else{
				    					int dayZeroExists = 0;
				    					dayZeroExists = validateDayZeroVisit(duration, vsk.getProtocol_id(),1,aVisitBean.getVisit_id());
				    					// last day condition
				    					if (dayZeroExists > 0 && visitDisp == duration){
				    						return aVisitBean;
				    					}
			    					}
			    				}else{
			    					int dayZeroExists = 0;
			    					dayZeroExists = validateDayZeroVisit(duration, vsk.getProtocol_id(),1,aVisitBean.getVisit_id());
			    					
			    					if (visitDisp > duration || (dayZeroExists > 0 && visitDisp == duration)){
			    						return aVisitBean;
			    					}    					
			    				}
				    		}else{
				    			if (-1 != bInsertAfter){ //fix #9924
					    			// bVisitBean is a calculated day zero visit
									int dayZeroExists = 1;
									if (visitDisp == 1 || visitDisp == 0){
										//day 0 condition
										if (aVisitBean.getDays() == 0 ){
					    					if (dayZeroExists > 0 && visitDisp == duration){
					    						return aVisitBean;
					    					}
										}
									}else{				
										if (visitDisp > duration || (dayZeroExists > 0 && visitDisp == duration)){
											return aVisitBean;
										}
									}
				    			}
				    		}
			    		}
	    			}
	    		} else{
	    			// Processing self fix #9940
	    			if (visitDisp > duration){
						return aVisitBean;
					}
	    		}
	    	}
    	}    	
    	
    	return  null;
    }
    // CAL Enhancement on the behalf of First Visit And Previous Visit
    public void updateFirstAndPreviousVisit(String dataGridString,String UpdateString,ProtVisitBean psk,int duration,String calStatus,
    	    String calType,String deleteString,Hashtable<String, String> args){
    	JSONArray dataGridArray;
    	JSONObject gridRecord = null;
    	JSONArray deleteStringArray;
    	JSONObject deleteRecord=null;
    	String childVisitName="";
    	ProtVisitBean parentVisitBean=null;
    	ProtVisitBean childVisitBean=null;
    	  try{
    		  dataGridArray = new JSONArray(dataGridString);
    		  deleteStringArray=new JSONArray(deleteString);
    		  SchCodeDao schcodedao = new SchCodeDao();
    		  int ntpd = schcodedao.getCodeId("timepointtype", "NTPD");
    		  for (int i = 0; i < dataGridArray.length(); ++i) {
    			  gridRecord=dataGridArray.getJSONObject(i);
    			//  int seq=gridRecord.getInt("visitSeq")-1;
    			  childVisitName=gridRecord.getString("visitName");
    			  Query query = em.createNamedQuery("findDetailByVisitName");        	
                  query.setParameter(1, childVisitName);
                  query.setParameter(2, psk.getProtocol_id());
                  childVisitBean=(ProtVisitBean)query.getSingleResult();
    			  if(LC.L_Previous_Visit.equals(gridRecord.get("insertAfter"))){
    				  for(int j=i;j>0;j--){
    					 JSONObject dummyRecord= dataGridArray.getJSONObject(j-1);
    					 int noIntervalId=dummyRecord.getInt("noIntervalId");
    					 if(noIntervalId==ntpd){
    						 continue;
    					 }
    						 String visitName=dummyRecord.getString("visitName");
    						  query = em.createNamedQuery("findDetailByVisitName");        	
                             query.setParameter(1, visitName);
                             query.setParameter(2, psk.getProtocol_id());
                             parentVisitBean=(ProtVisitBean)query.getSingleResult();
                             childVisitBean.setInsertAfter(parentVisitBean.getVisit_id());
                             em.merge(childVisitBean);
                             break;
    				  }
    			  }
    			  else if(LC.L_First_Visit.equals(gridRecord.get("insertAfter"))){
    				 JSONObject dummyRecord=dataGridArray.getJSONObject(0);
    				 String visitName=dummyRecord.getString("visitName");
    				 query = em.createNamedQuery("findDetailByVisitName");        	
                     query.setParameter(1, visitName);
                     query.setParameter(2, psk.getProtocol_id());
                     parentVisitBean=(ProtVisitBean)query.getSingleResult();
                     childVisitBean.setInsertAfter(parentVisitBean.getVisit_id());
                     em.merge(childVisitBean);
    				 
    			  }
    		  }
    	  }catch(Exception e){
 	    	 Rlog.fatal("protvisit","Exception In manageVisit in ProtVisitAgentBean "
	                 + e);
    	  }
    	
    }

}// end of class
