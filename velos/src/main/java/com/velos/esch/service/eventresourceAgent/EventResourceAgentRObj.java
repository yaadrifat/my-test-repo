/*
 * Classname : EventResourceAgentRObj
 *
 * Version information: 1.0
 *
 * Date: 02/15/2008
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Jnanamay Majumdar
 */

package com.velos.esch.service.eventresourceAgent;

/* Import Statements */
import java.util.Hashtable;
import javax.ejb.Remote;


import com.velos.esch.business.eventresource.impl.EventResourceBean;
import com.velos.esch.business.common.EventResourceDao;

/* End of Import Statements */
@Remote
public interface EventResourceAgentRObj {

	EventResourceBean getEventResourceDetails(int evtResTrackId);

    public int setEventResourceDetails(EventResourceBean ersk);

    // Overloaded for INF-18183 ::: Akshi
    public int removeEventResources(int eventId,Hashtable<String, String> args);

    public int removeEventResources(int eventId);

    public int getStatusIdOfTheEvent(int eventId);

    public EventResourceDao getAllRolesForEvent(int eventId);


}