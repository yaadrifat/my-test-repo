/*
 * Classname			SubCostItemAgentBean
 * 
 * Version information : 1.0
 *
 * Date				: 01/19/2011
 * 
 * Copyright notice : Velos Inc
 */
package com.velos.esch.service.subCostItemAgent.Impl;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.util.ArrayList;
import java.util.Hashtable;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.esch.business.audit.impl.AuditBean;
import com.velos.eres.business.submission.impl.SubmissionBean;
import com.velos.eres.service.util.DateUtil;
import com.velos.esch.business.common.EventdefDao;
import com.velos.esch.business.common.SubCostItemDao;
import com.velos.esch.business.subCostItem.impl.SubCostItemBean;
import com.velos.esch.service.subCostItemAgent.SubCostItemAgentRObj;
import com.velos.esch.service.util.Rlog;

/**
 * The stateless session EJB acting as a facade for the entity CMP.
 * 
 * @author Manimaran
 * @version 1.0, 01/19/2011
 * @ejbRemote SubCostItemAgentRObj
 */

@Stateless
public class SubCostItemAgentBean implements SubCostItemAgentRObj {
	@PersistenceContext(unitName = "esch")
    protected EntityManager em;
	@Resource private SessionContext context;
	
	private static final long serialVersionUID = 3257007661496480563L;
	
	public SubCostItemBean getSubCostItemDetails(int subCostItemId) {
	        try {

	            return (SubCostItemBean) em.find(SubCostItemBean.class, new Integer(
	            		subCostItemId));

	        } catch (Exception e) {
	            System.out.print("Error in getSubCostItemDetails() in SubCostItemAgentBean"+ e);
	            return null;
	        }
	}
	
	 public int setSubCostItemDetails(SubCostItemBean scsk) {
	        try {

	            SubCostItemBean subcost = new SubCostItemBean();

	            subcost.updateSubCostItem(scsk);

	            em.persist(subcost);
	            return (subcost.getSubCostItemId());

	        }

	        catch (Exception e) {
	            System.out
	                    .print("Error in setSubCostItemDetails() in SubCostItemAgentBean "
	                            + e);
	        }
	        return 0;
	}
	 
	 
	 public int updateSubCostItem(SubCostItemBean scsk) {

		 SubCostItemBean retrieved = null; 
	     int output;

	        try {

	            retrieved = (SubCostItemBean) em.find(SubCostItemBean.class,
	                    new Integer(scsk.getSubCostItemId()));

	            if (retrieved == null) {
	                return -2;
	            }
	            output = retrieved.updateSubCostItem(scsk);
	            em.merge(retrieved);

	        } catch (Exception e) {
	            Rlog.debug("subjectcost",
	                    "Error in updateSubCostItem in SubCostItemAgentBean" + e);
	            return -2;
	        }
	        return output;
	 }
	 
	 public SubCostItemDao getProtSelectedAndGroupedItems(int protocolId) {
	        try {
	        	SubCostItemDao subCostItemDao = new SubCostItemDao();
	        	subCostItemDao.getProtSelectedAndGroupedItems(protocolId);
	            return subCostItemDao;
	        } catch (Exception e) {
	            Rlog.fatal("subCostItem",
	                    "In SubCostItemAgentBean.getProtSelectedAndGroupedItems: "+e);
	        }
	        return null;
	 }
	 
	 //KM-#5949   

	 
	 public int deleteSubCostItems(ArrayList visitIds, ArrayList itemIds, int user) {

	        int output;	        

	        try {     	
	            SubCostItemDao sciDao = new SubCostItemDao();
	            output = sciDao.deleteSubCostItems(visitIds, itemIds, user);
	   	        }

	        catch (Exception e) {
	   	            Rlog.fatal("subCostItem",
	                    "Error in deleteSubCostItems  in SubCOstItemAgentBean" + e);
	            return -1;
	        }
	        return output;
	    }
	 
	 // deleteSubCostItems() Overloaded for INF-18183 ::: Raviesh
	 public int deleteSubCostItems(ArrayList visitIds, ArrayList itemIds, int user,Hashtable<String, String> args) {

	        int output;
	        String condition="";

	        try {
    	
	        	
	        	AuditBean audit=null;
	        	String pkVal = "";
	        	String ridVal = "";
	           
	            String currdate =DateUtil.getCurrentDate();
	            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
	            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
	            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
	            String appModule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the App Module Name from the Hashtable*/
	            
	            for(int i=0; i<visitIds.size(); i++)
	        		condition = condition + ", " + visitIds.get(i);  
	        	
	        	condition = "PK_SUBCOST_ITEM_VISIT IN ("+condition.substring(1)+")";

	        	Hashtable<String, ArrayList<String>> getRowValues = AuditUtils.getRowValues("SCH_SUBCOST_ITEM_VISIT",condition,"esch");/*Fetches the RID/PK_VALUE*/
	        	ArrayList<String> rowPK  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_PK_KEY);
	    		ArrayList<String> rowRID  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_RID_KEY);
	    		
	            //String getRidValue= AuditUtils.getRidValues("ER_PATPROT","PK_PATPROT="+condition);/*Fetches the RID/PK_VALUE*/ 
	            
	    		for(int i=0; i<rowPK.size(); i++)
				{
	    			pkVal = rowPK.get(i);
	    			ridVal = rowRID.get(i);
	    			audit = new AuditBean("SCH_SUBCOST_ITEM_VISIT",pkVal,ridVal,
	        			userID,currdate,appModule,ipAdd,reason);
	    			em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
				}
	        	
	            SubCostItemDao sciDao = new SubCostItemDao();
	            output = sciDao.deleteSubCostItems(visitIds, itemIds, user);
	            if(output!=0){context.setRollbackOnly();}; /*Checks whether the actual delete has occurred with return 0 else, rollback */
	        }

	        catch (Exception e) {
	        	context.setRollbackOnly();
	            Rlog.fatal("subCostItem",
	                    "Error in deleteSubCostItems  in SubCOstItemAgentBean" + e);
	            return -1;
	        }
	        return output;
	    }
	 
	 /*YK Bug 5821 & # 5818
		 * Code Changed to Remove physically from Table.
		 * */
	 
	 
	 public int removeSubCostItem(int itemId)
	 {
		 
		 SubCostItemBean retrieved = null; 
		 
		 try {     			   	
         				 
			 retrieved = (SubCostItemBean) em.find(SubCostItemBean.class,
					 new Integer(itemId));
			 
			 if (retrieved == null) {
				 return -2;
			 }
			 
			 em.remove(retrieved);
			  Rlog.debug("subjectcost", "removed subjectcost");
			 
		 } catch (Exception e) {
			 
			 Rlog.debug("subjectcost",
					 "Error in removeSubCostItem in SubCostItemAgentBean" + e);
			 return -2;
		 }
		 return 1;
	 }
	 
	 
	// removeSubCostItem() Overloaded for INF-18183 ::: Raviesh
	 public int removeSubCostItem(int itemId,Hashtable<String, String> args)
	 {
		 
		 SubCostItemBean retrieved = null; 
		 
		 try {          
			 
        	 AuditBean audit=null; //Audit Bean for AppDelete
             String currdate =DateUtil.getCurrentDate(); /*gets the current date from DateUtil*/
             String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
             String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
             String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
             String appModule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the App Module Name from the Hashtable*/
             
             String getRidValue= AuditUtils.getRidValue("SCH_SUBCOST_ITEM","esch","PK_SUBCOST_ITEM="+itemId);/*Fetches the RID/PK_VALUE*/ 
         	audit = new AuditBean("SCH_SUBCOST_ITEM",String.valueOf(itemId),getRidValue,
         			userID,currdate,appModule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
         	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
         	        	
			 
			 retrieved = (SubCostItemBean) em.find(SubCostItemBean.class,
					 new Integer(itemId));
			 
			 if (retrieved == null) {
				 context.setRollbackOnly();
				 return -2;
			 }
			 
			 em.remove(retrieved);
			  Rlog.debug("subjectcost", "removed subjectcost");
			 
		 } catch (Exception e) {
			 context.setRollbackOnly();
			 Rlog.debug("subjectcost",
					 "Error in removeSubCostItem in SubCostItemAgentBean" + e);
			 return -2;
		 }
		 return 1;
	 }
	 
	 
	
}
