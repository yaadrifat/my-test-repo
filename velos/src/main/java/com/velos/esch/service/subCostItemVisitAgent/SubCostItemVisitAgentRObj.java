/*
 * Classname			SubCostItemVisitAgentRObj
 * 
 * Version information : 1.0  
 *
 * Date					01/21/2011
 * 
 * Copyright notice : Velos Inc
 */
package com.velos.esch.service.subCostItemVisitAgent;

import javax.ejb.Remote;

import com.velos.esch.business.subCostItemVisit.impl.SubCostItemVisitBean;

/**
 * Remote interface for SubCostItemVisitAgentBean session EJB
 * 
 * @author Manimaran
 * @version 1.0, 01/21/2011
 */

@Remote
public interface SubCostItemVisitAgentRObj {

	public SubCostItemVisitBean getSubCostItemVisitDetails(int subCostItemVisitId);

    public int setSubCostItemVisitDetails(SubCostItemVisitBean subCostItemVisit);
    
    public int updateSubCostItemVisit(SubCostItemVisitBean subCostItemVisit);
    
    public Integer getExistingItemsByFkItemAndFkVisit(Integer fkItem, Integer fkVisit);	
}