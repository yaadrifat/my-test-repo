package com.velos.esch.audit.service;
import javax.ejb.Remote;

import com.velos.esch.audit.business.AuditRowEschBean;
@Remote
public interface AuditRowEschAgent {
	public AuditRowEschBean getAuditRowEschBean(Integer id);
	public int getRID(int pkey, String tableName, String pkColumnName);
	public int findLatestRaid(int rid, int userId);
	public int setReasonForChange(int raid, String remarks);
	public int setReasonForChange(Integer[] raid, String remarks);
}






