package com.velos.validator;

import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.web.pref.PrefJB;
import com.velos.eres.web.study.StudyJB;
import com.velos.eres.web.studyStatus.StudyStatusJB;
import com.velos.eres.web.user.UserJB;



public class StudyValidator extends Validator
{
  

public StudyValidator()
 {
  
 }
 public StudyValidator(String validateField,String data,String targetVal)
 {
	 super(validateField,data,targetVal);
     
     
 }
 public boolean validate()
 {
	 boolean codeExists;
	 int count=0;
	 
	 if (validateFld.equals("studyorgcheck")) {
	 	 StudyStatusJB studyStatB =new StudyStatusJB();
	 	 count=studyStatB.getCountByOrgStudyStat(EJBUtil.stringToNum(dataArray[0]),EJBUtil.stringToNum(dataArray[1]),dataArray[2],dataArray[3]);
	 	 if (count>0) 
	 		 codeExists=false;
	 	 else
	 		 codeExists=true;
     
	 	 return codeExists;
	 }
	 // For checking duplicate studyNum
	 if(validateFld.equalsIgnoreCase("studyNumberId")){
		 
		 StudyJB studyB = new StudyJB();
		 //accId,newStudyNum
		 count = studyB.validateStudyNumber(dataArray[0],dataArray[1]);
		 if (count > 0 )
			 codeExists=false;
		 else
			 codeExists=true;
		 
		 return codeExists;
	 
	 }
	 return false;
     
 }
 
 
 
 
 public String getDataSet() {
     return dataSet;
 }
 public void setDataSet(String dataSet) {
     this.dataSet = dataSet;
 }
 public String getValidateFld() {
     return validateFld;
 }
 public void setValidateFld(String validateFld) {
     this.validateFld = validateFld;
 }
 
}