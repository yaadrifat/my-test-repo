package com.velos.eres.dashboard.business;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import com.velos.eres.service.util.GenerateId;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.VelosResourceBundle;

public class DashboardDAO {

	public ArrayList<HashMap<String, Object>> dbFetchCodeLstStaues(
			String codelstType) {

		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> dataMap = null;

		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;

		String erCodeLst = "select PK_CODELST, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ,codelst_hide,codelst_custom_col1,codelst_custom_col from er_codelst where rtrim(CODELST_TYPE) = ? AND CODELST_HIDE<>'Y' order by  CODELST_SEQ";
		try {
			boolean redFlgChk=false;
			boolean orngFlgChk=false;
			boolean yeloFlgChk=false;
			boolean prplFlgChk=false;
			boolean whitFlgChk=false;
			boolean greenFlgChk=false;
					
			
			String redFlagDisp="";
			String orngFlgDisp="";
			String yeloFlgDisp="";
			String prplFlgDisp="";
			String whitFlgDisp="";
			String greenFlgDisp="";
			
			
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(erCodeLst);
			pstmt.setString(1, codelstType);
			rs = pstmt.executeQuery();

			while (rs.next()) {
                
				dataMap = new HashMap<String, Object>();
				dataMap.put("PK_CODELST", rs.getInt("PK_CODELST"));
				dataMap.put("CODELST_SUBTYP", rs.getString("CODELST_SUBTYP"));
				//For Red icon
				if(rs.getString("CODELST_SUBTYP").equalsIgnoreCase("priority_1") && (redFlgChk==false))
				  {//we need to validate Query Type==High Priority and age>threshold days
					redFlagDisp = "initlized";					
					redFlgChk = true;
					dataMap.put("REDICONDISP","SHOWRED");
				  }
//				else if(rs.getString("CODELST_SUBTYP").equalsIgnoreCase("re-opened") && (redFlgChk==false))
//				  { //we need to validate Query Type==High Priority and age>threshold days
//					redFlagDisp = "initlized";	
//					redFlgChk = true;
//					dataMap.put("REDICONDISP","SHOWRED");
//				  }
//					else
//					 {
//						dataMap.put("REDICONDISP","HIDE");
//				     }
				
				//For Orange icon
				if(rs.getString("CODELST_SUBTYP").equalsIgnoreCase("priority_2"))
				  {//we need to validate Query Type==High
					orngFlgDisp = "initlized";	
					dataMap.put("ORANGICONDISP","SHOWORANGE");
				  }
//				else
//					 {
//						dataMap.put("ORANGICONDISP","HIDE");
//				     }
				
				//For Yellow icon
				if(rs.getString("CODELST_SUBTYP").equalsIgnoreCase("priority_3"))
				  {//we need to validate Query Type==Normal
					yeloFlgDisp = "initlized";	
					dataMap.put("YELLOWICONDISP","SHOWYELLOW");
				  }
//				else
//					 {
//						dataMap.put("YELLOWICONDISP","HIDE");
//				     } 
				
				//For Purple icon Query status=open or re-open and Query Type=Normal
				if(rs.getString("CODELST_SUBTYP").equalsIgnoreCase("priority_4") && (prplFlgChk==false))
				  {//we need to validate Query Type==Normal
					prplFlgDisp = "initlized";	
					prplFlgChk = true;
					dataMap.put("PURPLICONDISP","SHOWPURPLE");
				  }
//				else if(rs.getString("CODELST_SUBTYP").equalsIgnoreCase("re-opened") && (prplFlgChk==false))
//				  { //we need to validate Query Type==Normal
//					prplFlgDisp = "initlized";	
//					prplFlgChk = true;
//					dataMap.put("PURPLICONDISP","SHOWPURPLE");
//				  }
//					else
//					 {
//						dataMap.put("PURPLICONDISP","HIDE");
//				     }
				
				//For White Icon Form responses are not monitored Yet
//				if(whitFlgDisp.equalsIgnoreCase(""))
//				  {//we need to validate Query Type==Normal
//					whitFlgDisp = "initlized";	
//					dataMap.put("WHITICONDISP","SHOWHITE");
//				  }
//				else
//					 {
//						dataMap.put("WHITICONDISP","HIDE");
//				     }
				
				//For Green icon All queries are resolved
				if(rs.getString("CODELST_SUBTYP").equalsIgnoreCase("priority_5") && (whitFlgChk==false))
				  {//we need to validate Query Type==Normal
					prplFlgDisp = "initlized";	
					prplFlgChk = true;
					dataMap.put("WHITICONDISP","SHOWHITE");
				  }
				if(rs.getString("CODELST_SUBTYP").equalsIgnoreCase("priority_6"))
				  {//we need to validate Query Type==Normal
					greenFlgDisp = "initlized";	
					dataMap.put("GREENICONDISP","SHOWGREEN");
				  }
//				else
//					 {
//						dataMap.put("GREENICONDISP","HIDE");
//				     }
				/********************************************/
				
				dataMap.put("CODELST_DESC", rs.getString("CODELST_DESC"));
				dataMap.put("CODELST_SEQ", rs.getString("CODELST_SEQ"));
				dataMap.put("CODELST_HIDE", rs.getString("CODELST_HIDE"));
				dataMap.put("CODELST_CUSTOM_COL1",
						rs.getString("CODELST_CUSTOM_COL1"));
				dataMap.put("CODELST_CUSTOM_COL",
						rs.getString("CODELST_CUSTOM_COL"));
				dataMap.put("colorcode",
						VelosResourceBundle.getString(
								VelosResourceBundle.CONFIG_BUNDLE, codelstType
										+ "." + rs.getString("CODELST_SUBTYP")
										+ ".colorcode"));
				maplist.add(dataMap);
				//System.out.println("@51 DashboardDAO.java===>>>"+dataMap);
			}
                  
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if ("subm_status".equals(codelstType)) {
			ArrayList<HashMap<String, Object>> maplistSubmStatus = new ArrayList<HashMap<String, Object>>();

			dataMap = new HashMap<String, Object>();
			dataMap.put("PK_CODELST", 0);
			dataMap.put("CODELST_SUBTYP", "");
			dataMap.put("CODELST_DESC", VelosResourceBundle.getString(
					VelosResourceBundle.CONFIG_BUNDLE, "study.savedItems"));
			dataMap.put("CODELST_SEQ", "");
			dataMap.put("CODELST_HIDE", "");
			dataMap.put("CODELST_CUSTOM_COL1", "");
			dataMap.put("CODELST_CUSTOM_COL", "");
			dataMap.put("colorcode", VelosResourceBundle.getString(
					VelosResourceBundle.CONFIG_BUNDLE,
					"subm_status.savedItems.colorcode"));
			maplistSubmStatus.add(dataMap);

			for (HashMap<String, Object> dataMapSubmStatus : maplist) {
				String status = VelosResourceBundle
						.getString(
								VelosResourceBundle.CONFIG_BUNDLE,
								(codelstType
										+ "."
										+ dataMapSubmStatus
												.get("CODELST_SUBTYP") + ".display"));
				if (!"N".equals(status))
					maplistSubmStatus.add(dataMapSubmStatus);
			}
			return maplistSubmStatus;
		} else if ("studystat".equals(codelstType)) {
			ArrayList<HashMap<String, Object>> maplistSubmStatus = new ArrayList<HashMap<String, Object>>();

			for (HashMap<String, Object> dataMapSubmStatus : maplist) {
				String status = VelosResourceBundle
						.getString(
								VelosResourceBundle.CONFIG_BUNDLE,
								(codelstType
										+ "."
										+ dataMapSubmStatus
												.get("CODELST_SUBTYP") + ".colorcode"));
				if (!"".equals(status) && status != null)
					maplistSubmStatus.add(dataMapSubmStatus);
			}
			return maplistSubmStatus;
		}

		else {
			return maplist;
		}
	}
	
   /*
    * For Delete functionality in DashBoard
    * ****       */

	public int dbDeleteSavedSearches(String saveSearch,String modulename) {
		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> dataMap = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		int i=0;

		String savedSearchSql = "delete from ER_BROWSERSEARCH where BROWSERSEARCH_NAME= '"+ saveSearch +"' and BROWSERSEARCH_MODULE='"+modulename+"'";
		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(savedSearchSql);
			i=  pstmt.executeUpdate();

			 

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return i;

	}
	
	public ArrayList<HashMap<String, Object>> dbFetchFormAdvStaues(
			Integer userId,Integer accountId,String codelstType, Integer tabType, String studyNum) {

		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> dataMap = null;

		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		
		String erCodeLst = "select PK_CODELST, CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ,codelst_hide,codelst_custom_col1,codelst_custom_col,codelst_study_role from er_codelst where rtrim(CODELST_TYPE) = ? AND CODELST_HIDE<>'Y' order by  CODELST_SEQ";
		if(tabType==0){
			//erCodeLst = "select PK_CODELST, CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ,codelst_hide,codelst_custom_col1,codelst_custom_col,codelst_study_role from er_codelst where rtrim(CODELST_TYPE) = ? AND CODELST_HIDE<>'Y' order by  CODELST_SEQ";
			//*** Add column as "aa.CODELST_SUBTYP AS frm_codelst_subtyp" during implementation to hide unlock form status *******/
			erCodeLst=" select fk_codelst_tmrole,(select codelst_subtyp from er_codelst where pk_codelst=a.fk_codelst_tmrole ) AS CODELST_SUBTYP,aa.pk_codelst,aa.CODELST_SUBTYP AS frm_codelst_subtyp,aa.CODELST_DESC,aa.CODELST_SEQ,aa.codelst_hide,aa.codelst_custom_col1,aa.codelst_custom_col,aa.codelst_study_role from er_studyteam a " +
					" inner join (select PK_CODELST, CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ  ,codelst_hide,codelst_custom_col1,codelst_custom_col,codelst_study_role from er_codelst " +
					" where rtrim(CODELST_TYPE) = ?)  aa on  aa.codelst_hide<>'Y' where a.fk_user="+userId+" and a.fk_study=";
					if(null==studyNum ||"".equals(studyNum)){
						erCodeLst=erCodeLst+0;
					}
		             else{
						String []dStdNum=studyNum.split(",");
						erCodeLst=erCodeLst+dStdNum[0];
		                }
					erCodeLst=	erCodeLst+	" and (codelst_study_role like (SELECT codelst_subtyp FROM er_codelst WHERE pk_codelst=a.fk_codelst_tmrole) or codelst_study_role like '%,'||(SELECT codelst_subtyp FROM er_codelst WHERE pk_codelst=a.fk_codelst_tmrole)||',%' or  codelst_study_role like '%,'||(SELECT codelst_subtyp FROM er_codelst WHERE pk_codelst=a.fk_codelst_tmrole) OR codelst_study_role LIKE (SELECT codelst_subtyp FROM er_codelst WHERE pk_codelst=a.fk_codelst_tmrole)||',%')"+
					"  ORDER BY CODELST_SEQ";
		}else{
			//erCodeLst = "select PK_CODELST, CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ,codelst_hide,codelst_custom_col1,codelst_study_role from ESCH.SCH_CODELST where rtrim(CODELST_TYPE) = ? AND CODELST_HIDE<>'Y' order by  CODELST_SEQ";
			erCodeLst=" select fk_codelst_tmrole,(select codelst_subtyp from er_codelst where pk_codelst=a.fk_codelst_tmrole ) AS CODELST_SUBTYP,aa.pk_codelst,aa.CODELST_SUBTYP AS frm_codelst_subtyp,aa.CODELST_DESC,aa.CODELST_SEQ,aa.codelst_hide,aa.codelst_custom_col1,aa.codelst_study_role from er_studyteam a " +
			          " inner join (select PK_CODELST, CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ  ,codelst_hide,codelst_custom_col1,codelst_study_role from ESCH.SCH_CODELST " +
			          " where rtrim(CODELST_TYPE) = ?)  aa on  aa.codelst_hide<>'Y' where a.fk_user="+userId+" and a.fk_study=";
			          if(null==studyNum ||"".equals(studyNum)){
							erCodeLst=erCodeLst+0;
						}
			             else{
							String []dStdNum=studyNum.split(",");
							erCodeLst=erCodeLst+dStdNum[0];
			                }
			erCodeLst=erCodeLst+ " and (codelst_study_role like (SELECT codelst_subtyp FROM er_codelst WHERE pk_codelst=a.fk_codelst_tmrole) or codelst_study_role like '%,'||(SELECT codelst_subtyp FROM er_codelst WHERE pk_codelst=a.fk_codelst_tmrole)||',%' or  codelst_study_role like '%,'||(SELECT codelst_subtyp FROM er_codelst WHERE pk_codelst=a.fk_codelst_tmrole) OR codelst_study_role LIKE (SELECT codelst_subtyp FROM er_codelst WHERE pk_codelst=a.fk_codelst_tmrole)||',%')"+
			          "  ORDER BY CODELST_SEQ";
		}
		try
		{
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(erCodeLst);
			pstmt.setString(1, codelstType);
			rs = pstmt.executeQuery();
            if(rs.next())
            {
            	do
            	{
					dataMap = new HashMap<String, Object>();
					dataMap.put("PK_CODELST", rs.getInt("PK_CODELST"));
					dataMap.put("CODELST_SUBTYP", rs.getString("CODELST_SUBTYP"));
					dataMap.put("CODELST_DESC", rs.getString("CODELST_DESC"));
					dataMap.put("CODELST_SEQ", rs.getString("CODELST_SEQ"));
					dataMap.put("CODELST_HIDE", rs.getString("CODELST_HIDE"));
					dataMap.put("CODELST_CUSTOM_COL1",
							rs.getString("codelst_custom_col1"));
					if(tabType==0)
					dataMap.put("CODELST_CUSTOM_COL",
							rs.getString("CODELST_CUSTOM_COL"));
					dataMap.put("codelst_study_role",
							rs.getString("codelst_study_role"));
					maplist.add(dataMap);
					if("unlock".equals(rs.getString("frm_codelst_subtyp").trim())) /* hide the unlock status from list of  the bulk upload status to form Response only */
						maplist.remove(maplist.size()-1);
            	}
            	while(rs.next());
            }
            else
            {
				int pk_role = 0;
				String role;
				String getRolePk="SELECT fk_codelst_st_role FROM er_grps g, er_user u WHERE u.fk_grp_default = g.pk_grp AND u.pk_user ="+userId;
				pstmt=conn.prepareStatement(getRolePk);
				rs=pstmt.executeQuery();
				rs.next();
				pk_role=rs.getInt("fk_codelst_st_role");
				if(tabType==0)
				{
					if(pk_role!=0)
					{
						String erGrpsRole="SELECT fk_codelst_st_role,codelst_subtyp FROM er_grps g,er_user u,er_codelst d WHERE u.fk_grp_default = g.pk_grp AND u.pk_user="+userId+" and d.pk_codelst=(SELECT fk_codelst_st_role FROM er_grps g, er_user u WHERE u.fk_grp_default = g.pk_grp AND u.pk_user="+userId+")";
						pstmt=conn.prepareStatement(erGrpsRole);
						rs=pstmt.executeQuery();
						rs.next();
						role=rs.getString("codelst_subtyp");
						erCodeLst="select pk_codelst,CODELST_SUBTYP,CODELST_DESC,CODELST_SEQ,codelst_hide,codelst_custom_col1,codelst_custom_col,codelst_study_role  from er_codelst WHERE rtrim(CODELST_TYPE) = ? " +
					" and codelst_hide<>'Y'  " +
					" AND (codelst_study_role LIKE '"+role+"' OR codelst_study_role LIKE '%,"+role+",%' " +
					" OR codelst_study_role LIKE '%,"+role+"' OR codelst_study_role LIKE '"+role+",%') ORDER BY CODELST_SEQ ";
					}
					else
					{
						erCodeLst="select pk_codelst,CODELST_SUBTYP,CODELST_DESC,CODELST_SEQ,codelst_hide,codelst_custom_col1,codelst_custom_col,codelst_study_role  from er_codelst WHERE rtrim(CODELST_TYPE) = ? " +
						" and codelst_hide<>'Y'  " +
						" AND (codelst_study_role LIKE 'default_data' OR codelst_study_role LIKE '%,default_data,%' " +
						" OR codelst_study_role LIKE '%,default_data' OR codelst_study_role LIKE 'default_data,%') ORDER BY CODELST_SEQ ";
					}
				}
				else
				{
					if(pk_role!=0)
					{
						String erGrpsRole="SELECT fk_codelst_st_role,codelst_subtyp FROM er_grps g,er_user u,er_codelst d WHERE u.fk_grp_default = g.pk_grp AND u.pk_user="+userId+" and d.pk_codelst=(SELECT fk_codelst_st_role FROM er_grps g, er_user u WHERE u.fk_grp_default = g.pk_grp AND u.pk_user="+userId+")";
						pstmt=conn.prepareStatement(erGrpsRole);
						rs=pstmt.executeQuery();
						rs.next();
						role=rs.getString("codelst_subtyp");
						erCodeLst="select pk_codelst,CODELST_SUBTYP,CODELST_DESC,CODELST_SEQ,codelst_hide,codelst_custom_col1,codelst_study_role  from ESCH.SCH_codelst WHERE rtrim(CODELST_TYPE) = ? " +
					" and codelst_hide<>'Y'  " +
					" AND (codelst_study_role LIKE '"+role+"' OR codelst_study_role LIKE '%,"+role+",%' " +
					" OR codelst_study_role LIKE '%,"+role+"' OR codelst_study_role LIKE '"+role+",%') ORDER BY CODELST_SEQ ";
					}
					else
					{
						erCodeLst="select pk_codelst,CODELST_SUBTYP,CODELST_DESC,CODELST_SEQ,codelst_hide,codelst_custom_col1,codelst_study_role  from ESCH.SCH_codelst WHERE rtrim(CODELST_TYPE) = ? " +
					          " and codelst_hide<>'Y'  " +
					          " AND (codelst_study_role LIKE 'default_data' OR codelst_study_role LIKE '%,default_data,%' " +
					          " OR codelst_study_role LIKE '%,default_data' OR codelst_study_role LIKE 'default_data,%') ORDER BY CODELST_SEQ ";
					}
				}
				pstmt = conn.prepareStatement(erCodeLst);
				pstmt.setString(1, codelstType);
				rs = pstmt.executeQuery();
			
				while(rs.next())
				{
					dataMap = new HashMap<String, Object>();
					dataMap.put("PK_CODELST", rs.getInt("PK_CODELST"));
					dataMap.put("CODELST_SUBTYP", rs.getString("CODELST_SUBTYP"));
					dataMap.put("CODELST_DESC", rs.getString("CODELST_DESC"));
					dataMap.put("CODELST_SEQ", rs.getString("CODELST_SEQ"));
					dataMap.put("CODELST_HIDE", rs.getString("CODELST_HIDE"));
					dataMap.put("CODELST_CUSTOM_COL1",
							rs.getString("codelst_custom_col1"));
					if(tabType==0)
					dataMap.put("CODELST_CUSTOM_COL",
							rs.getString("CODELST_CUSTOM_COL"));
					dataMap.put("codelst_study_role",
							rs.getString("codelst_study_role"));
					maplist.add(dataMap);
					if("unlock".equals(rs.getString("CODELST_SUBTYP").trim())) /* hide the unlock status from list of  the bulk upload status to form Response only  */
						maplist.remove(maplist.size()-1);
				}
			}	
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				rs.close();
				pstmt.close();
				conn.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return maplist;
	}
	
	public ArrayList<HashMap<String, Object>> dbFetchSavedSearches(String searchModule, Integer userId) {
		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> dataMap = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;

		String savedSearchSql = "select * from er_browsersearch where "+
		"lower(browsersearch_module)='"+searchModule+
		"' and fk_user="+userId+
		" order by LOWER(browsersearch_name)";

		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(savedSearchSql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				dataMap = new HashMap<String, Object>();
				dataMap.put("browsersearch_name", rs.getString(("browsersearch_name")));
				dataMap.put("browsersearch_criteria", rs.getString("browsersearch_criteria"));
				dataMap.put("browsersearch_isdefault", rs.getString("browsersearch_isdefault"));
				maplist.add(dataMap);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return maplist;

	}

	public ArrayList<HashMap<String, Object>> dbFetchStudies(Integer userId,
			Integer accountId, String studyNum) {
		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> dataMap = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;

		String studySql = "select PK_STUDY, STUDY_NUMBER"
				+ " from er_study"
				+ " where fk_account = "
				+ accountId
				+ " and ( exists ( select * from er_studyteam where fk_study = pk_study and "
				+ " fk_user = "
				+ userId
				+ " and  nvl(study_team_usr_type,'D')='D') or pkg_superuser.F_Is_Superuser("
				+ userId + ", pk_study) = 1 )  "
				+ " and (LOWER(STUDY_NUMBER) LIKE LOWER('%"+studyNum+"%')) ";

		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(studySql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				dataMap = new HashMap<String, Object>();
				dataMap.put("id", rs.getInt(("PK_STUDY")));
				dataMap.put("text", rs.getString("STUDY_NUMBER"));
				maplist.add(dataMap);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return maplist;

	}
	
	public ArrayList<HashMap<String, Object>> dbFetchOrganizations(Integer userId,
			Integer accountId, String studyNum, String orgName, Integer tabType, String selectedOrgId) {
		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> dataMap = null;
		HashMap<String,Object> dataMap1 = null;   //Bug #21074 by Shoaib
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		String orgSql="";
		selectedOrgId=(selectedOrgId==null)?"":selectedOrgId;
		if(selectedOrgId.equals("0")){
			orgName="@Do not display org list@";
		}
		// This code has been commented due to the fix of bug no #23201.
/*
		String orgSql = " SELECT pk_site, site_name "
				+ " FROM er_site "
				+ " WHERE fk_account = "
				+ accountId
				+ " and ( exists ( SELECT *  FROM er_patprot , er_usersite usersite WHERE fk_site_enrolling  = pk_site ";
				if(!"".equals(studyNum))
			   orgSql	=	orgSql+ " and fk_study in ("+studyNum+") ";
			   orgSql	=	orgSql+ " and  nvl(PATPROT_STAT,0)=1 and pkg_user.f_chk_right_for_patprotsite(pk_patprot, "
			    + userId
			   	+ " ) = 1 and usersite.fk_user="+userId+" and usersite.usersite_right >0 and pk_site=usersite.fk_site)   ) "
				+ " and (LOWER(site_name) LIKE LOWER('%"+orgName+"%')) ";*/
		orgSql=" SELECT DISTINCT ss.fk_site AS pk_site, site_name, site_altid, site_restrict, LOWER(site_name) AS sn FROM er_studysites ss,er_site WHERE ss.fk_study  in ("+studyNum+")"+
				" AND pk_site= ss.fk_site AND ( pkg_user.f_chk_right_for_studysite(ss.fk_study,"+userId+",ss.fk_site) > 0)"+" and (LOWER(site_name) LIKE LOWER('%"+orgName+"%')) "	;

		try {
		/*Start Bug #21074 by Shoaib*/
			if(tabType==0 && selectedOrgId.equals(""))
			{
				dataMap1 = new HashMap<String, Object>();
				dataMap1.put("id", 0);
				dataMap1.put("text", "None");
                maplist.add(dataMap1);
		/*End Bug #21074 by Shoaib*/
			}
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(orgSql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				dataMap = new HashMap<String, Object>();
				dataMap.put("id", rs.getInt(("pk_site")));
				dataMap.put("text", rs.getString("site_name"));
				maplist.add(dataMap);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return maplist;

	}
	
	public ArrayList<HashMap<String, Object>> dbFetchForms(Integer userId,
			Integer accountId, String studyNum, String orgId, String formName, Integer tabType) {
		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> dataMap = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		String formSql="";
        if(tabType==0){
		formSql = " select distinct pk_formlib,form_name from er_formlib, ";
			if("".equals(studyNum) && "".equals(orgId)){
				formSql	=	formSql+" er_acctforms ";}
			else if(!"".equals(studyNum) &&  "0".equals(orgId)){  //remove  {"".equals(orgId) ||} for Bug No:#23254
				formSql	=	formSql+" er_studyforms ";}
			else{ 
				formSql	=	formSql+" er_patforms pf, er_per p, er_linkedforms lf";}
			formSql	=	formSql+" where er_formlib.fk_account = "+accountId+" and ";
			/*  Case of Study Forms. */
			if(!"".equals(studyNum) &&  "0".equals(orgId))
					formSql	=	formSql+ " pk_formlib  = fk_formlib and pkg_superuser.F_Is_Superuser("+userId+",fk_study)>0 and fk_study in ("+studyNum+") and er_studyforms.record_type <> 'D' ";
			/*  Patient Enrolling Site having at least one Org. */
				if(!"".equals(studyNum) && !"0".equals(orgId) && !"".equals(orgId))
					formSql	=	formSql+ " pk_formlib  = lf.fk_formlib and p.pk_per = pf.fk_per and pf.fk_formlib=lf.fk_formlib and lf.lf_displaytype<>'PA' and pk_formlib in (select fk_formlib from er_patprot,er_patforms,er_per where fk_study in ("+studyNum+") and pk_patprot=fk_patprot and er_patprot.fk_per=er_per.pk_per and er_per.fk_site in ("+orgId+") and record_type <>'D') and pkg_user.f_right_forpatsites(pk_per , "+userId+")>0 ";    //remove 'and fk_site in ("+orgId+")' for Bug No:#23254
				/*  Patient Enrolling Site having ALL(Default). */
				if(!"".equals(studyNum)&& "".equals(orgId))
					formSql	=	formSql+ " pk_formlib  = lf.fk_formlib and p.pk_per = pf.fk_per and pf.fk_formlib=lf.fk_formlib and lf.lf_displaytype<>'PA' and pk_formlib in (select fk_formlib from er_patprot,er_patforms,er_per where fk_study in ("+studyNum+") and pk_patprot=fk_patprot and er_patprot.fk_per=er_per.pk_per and er_per.fk_site in ("+" SELECT DISTINCT ss.fk_site AS pk_site FROM er_studysites ss, er_site WHERE ss.fk_study in ("+studyNum+") and record_type <>'D' AND pk_site = ss.fk_site AND ( pkg_user.f_chk_right_for_studysite(ss.fk_study,"+userId+",ss.fk_site) > 0)"+")) and pkg_user.f_right_forpatsites(pk_per , "+userId+")>0 "; 	
				if("".equals(studyNum) && !"".equals(orgId) && !"0".equals(orgId) )
				formSql	=	formSql+ " and fk_patprot is null ";
				formSql	=	formSql+ " and (LOWER(form_name) LIKE LOWER('%"+formName+"%')) ";
        }
        else if(tabType==1){
        	
             	formSql = " select pk_codelst as pk_formlib, codelst_desc as form_name from sch_codelst where trim(codelst_type) = 'adve_type' and codelst_hide <> 'Y' ";
             	formSql	=	formSql+ " and (LOWER(codelst_desc) LIKE LOWER('%"+formName+"%')) ";
        	
       
        }
		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(formSql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				dataMap = new HashMap<String, Object>();
				dataMap.put("id", rs.getInt(("pk_formlib")));
				dataMap.put("text", rs.getString("form_name"));
				maplist.add(dataMap);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return maplist;

	}
	
	public ArrayList<HashMap<String, Object>> dbFetchFormStatues(Integer userId,
			Integer accountId,String studyNum, String statusDesc,Integer tabType) {
		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> dataMap = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		String formStatuesSql="";
		if(tabType==2){
			 formStatuesSql = " SELECT PK_CODELST, CODELST_DESC,CODELST_SUBTYP FROM ER_CODELST WHERE rtrim(CODELST_TYPE) = 'patStatus' "+
			" and codelst_hide<>'Y' and (LOWER(CODELST_DESC) LIKE LOWER('%"+statusDesc+"%')) ORDER BY CODELST_SEQ ";
			
		} 
		else{
		 formStatuesSql = " SELECT PK_CODELST, CODELST_DESC, CODELST_SUBTYP FROM ER_CODELST WHERE rtrim(CODELST_TYPE) = 'fillformstat' "+
						" and codelst_hide<>'Y' and (LOWER(CODELST_DESC) LIKE LOWER('%"+statusDesc+"%')) ORDER BY CODELST_SEQ ";
		}
		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(formStatuesSql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				dataMap = new HashMap<String, Object>();
				dataMap.put("id", rs.getInt(("PK_CODELST")));
				dataMap.put("text", rs.getString("CODELST_DESC"));
				maplist.add(dataMap);
				if("unlock".equals(rs.getString("CODELST_SUBTYP").trim()))
					maplist.remove(maplist.size()-1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return maplist;

	}
	
	public ArrayList<HashMap<String, Object>> dbFetchAEStatues(Integer userId,
			Integer accountId,String studyNum, String aeStatusDesc) {
		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> dataMap = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		String []dStdNum=studyNum.split(",");
		/*String advStatusRoleBaseSql=" select fk_codelst_tmrole,(select codelst_subtyp from er_codelst where pk_codelst=a.fk_codelst_tmrole ) AS sub_typ,aa.pk_codelst,aa.CODELST_DESC from er_studyteam a "
			+" inner join (select PK_CODELST, CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ  ,codelst_hide,codelst_study_role from sch_codelst "
			+" where rtrim(CODELST_TYPE) = 'fillformstat')  aa on  aa.codelst_hide<>'Y' where a.fk_user="+userId+" and a.fk_study="+dStdNum[0]
			+" and (codelst_study_role like (SELECT codelst_subtyp FROM er_codelst WHERE pk_codelst=a.fk_codelst_tmrole) or codelst_study_role like '%,'||(SELECT codelst_subtyp FROM er_codelst WHERE pk_codelst=a.fk_codelst_tmrole)||',%' or  codelst_study_role like '%,'||(SELECT codelst_subtyp FROM er_codelst WHERE pk_codelst=a.fk_codelst_tmrole) OR codelst_study_role LIKE (SELECT codelst_subtyp FROM er_codelst WHERE pk_codelst=a.fk_codelst_tmrole)||',%')"
			+"   ORDER BY CODELST_SEQ";
//		String advEveSql = " SELECT PK_CODELST,CODELST_DESC FROM sch_codelst WHERE rtrim(CODELST_TYPE) = 'fillformstat' "+
//		" and codelst_hide <> 'Y' and (LOWER(CODELST_DESC) LIKE LOWER('%"+aeStatusDesc+"%'))  ORDER BY CODELST_SEQ ";
*/
		try {
			conn = CommonDAO.getConnection();
			/*pstmt = conn.prepareStatement(advStatusRoleBaseSql);
			rs = pstmt.executeQuery();
			if(rs.next()){

				 int findDesc=-1;
				do{
					findDesc=rs.getString("CODELST_DESC").toLowerCase().indexOf(aeStatusDesc.toLowerCase());
					if(findDesc>=0){
					dataMap = new HashMap<String, Object>();
					dataMap.put("id", rs.getInt(("PK_CODELST")));
					dataMap.put("text", rs.getString("CODELST_DESC"));
					maplist.add(dataMap);
					}
				}while(rs.next());
				}else{
			 advStatusRoleBaseSql="select pk_codelst,CODELST_DESC from sch_codelst WHERE rtrim(CODELST_TYPE) = 'fillformstat' " 
						+" and codelst_hide<>'Y' and (LOWER(CODELST_DESC) LIKE LOWER('%"+aeStatusDesc+"%')) " +
								" AND (codelst_study_role LIKE 'default_data' OR codelst_study_role LIKE '%,default_data,%' " +
								" OR codelst_study_role LIKE '%,default_data' OR codelst_study_role LIKE 'default_data,%') ORDER BY CODELST_SEQ ";*/
			
			String advStatusRoleBaseSql="select pk_codelst,CODELST_DESC,CODELST_SUBTYP from sch_codelst WHERE rtrim(CODELST_TYPE) = 'fillformstat' " 
								+" and codelst_hide<>'Y' and (LOWER(CODELST_DESC) LIKE LOWER('%"+aeStatusDesc+"%')) ";
			
					pstmt = conn.prepareStatement(advStatusRoleBaseSql);
					rs = pstmt.executeQuery();
					while(rs.next()){
						dataMap = new HashMap<String, Object>();
						dataMap.put("id", rs.getInt(("PK_CODELST")));
						dataMap.put("text", rs.getString("CODELST_DESC"));
						maplist.add(dataMap);
						if("unlock".equals(rs.getString("CODELST_SUBTYP").trim()))
							maplist.remove(maplist.size()-1);
					}
				//}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return maplist;

	}
	
	public ArrayList<HashMap<String, Object>> dbFetchPatientIDs(Integer userId,
			Integer accountId, String studyNum, String orgId, String patientId) {
		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> dataMap = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		String patientSql = " SELECT distinct pk_per, PER_CODE FROM er_per,er_patprot WHERE fk_account = "+ accountId+" and pk_per=fk_per and  fk_study in ("+studyNum+") and ( pkg_user.f_chk_right_for_studysite(fk_study,"+userId+",fk_site) > 0) and ";
			if(!"".equals(orgId)){
				patientSql	=	patientSql+" fk_site in ("+orgId+") and ";}
				patientSql	=	patientSql+ " (LOWER(PER_CODE) LIKE LOWER('%"+patientId+"%')) ";

		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(patientSql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				dataMap = new HashMap<String, Object>();
				dataMap.put("id", rs.getInt("pk_per"));
				dataMap.put("text", rs.getString("PER_CODE"));
				maplist.add(dataMap);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return maplist;

	}
	
	public ArrayList<HashMap<String, Object>> dbFetchPatientStudyIDs(Integer userId,
			Integer accountId, String studyNum, String orgId, String studyPatientId) {
		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> dataMap = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		
		String patStdSql = " SELECT distinct pk_per, PATPROT_PATSTDID FROM er_patprot,er_per WHERE PK_PER=fk_per and  fk_account = "+accountId+" and ( pkg_user.f_chk_right_for_studysite(fk_study,"+userId+",fk_site) > 0) and patprot_stat=1 ";
		if(!"".equals(studyNum)){
			patStdSql	=	patStdSql+ " and fk_study in ("+studyNum+") ";}
		if(!"".equals(orgId)){
			patStdSql	=	patStdSql+" and fk_site_enrolling in ("+orgId+") ";}
			patStdSql	=	patStdSql+ " and (LOWER(PATPROT_PATSTDID) LIKE LOWER('%"+studyPatientId+"%')) ";

		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(patStdSql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				dataMap = new HashMap<String, Object>();
				dataMap.put("id", rs.getInt("pk_per"));
				dataMap.put("text", rs.getString("PATPROT_PATSTDID"));
				maplist.add(dataMap);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return maplist;

	}
	
	public ArrayList<HashMap<String, Object>> dbFetchCalendars(Integer userId,
			Integer accountId, String studyNum, String orgId, String calendarName) {
		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> dataMap = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;

		String calSql = " select distinct ac.event_id, name from event_assoc ac, sch_events1 sc, er_patprot pt,er_per WHERE PK_PER=fk_per and ac.event_id= SESSION_ID and pk_patprot = fk_patprot ";
		if(!"".equals(studyNum)){
			calSql	=	calSql+ " and pt.fk_study in ("+studyNum+") ";}
		if(!"".equals(orgId)){
			calSql	=	calSql+" and pt.fk_site_enrolling in ("+orgId+") ";}
		    calSql	=	calSql+ " and (LOWER(name) LIKE LOWER('%"+calendarName+"%')) ";

		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(calSql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				dataMap = new HashMap<String, Object>();
				dataMap.put("id", rs.getInt(("event_id")));
				dataMap.put("text", rs.getString("name"));
				maplist.add(dataMap);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return maplist;

	}
	
	public ArrayList<HashMap<String, Object>> dbFetchVisits(Integer userId,
			Integer accountId, String calendarIds, String visitName) {
		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> dataMap = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;

		String visitSql = " select PK_PROTOCOL_VISIT, VISIT_NAME from sch_protocol_visit where ";
		if(!"".equals(calendarIds)){
			visitSql	=	visitSql+ "  FK_PROTOCOL in ("+calendarIds+") and ";}
			visitSql	=	visitSql+ " (LOWER(VISIT_NAME) LIKE LOWER('%"+visitName+"%')) ";

		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(visitSql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				dataMap = new HashMap<String, Object>();
				dataMap.put("id", rs.getInt(("PK_PROTOCOL_VISIT")));
				dataMap.put("text", rs.getString("VISIT_NAME"));
				maplist.add(dataMap);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return maplist;

	}
	
	public ArrayList<HashMap<String, Object>> dbFetchEvents(Integer userId,
			Integer accountId, String visitIds, String eventName) {
		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> dataMap = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;

		String eventSql = " select event_id, name from event_assoc where ";
		if(!"".equals(visitIds)){
			eventSql	=	eventSql+ "  fk_visit in ("+visitIds+") and ";}
			eventSql	=	eventSql+ " (LOWER(name) LIKE LOWER('%"+eventName+"%')) ";

		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(eventSql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				dataMap = new HashMap<String, Object>();
				dataMap.put("id", rs.getInt(("event_id")));
				dataMap.put("text", rs.getString("name"));
				maplist.add(dataMap);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return maplist;

	}
	
	public ArrayList<HashMap<String, Object>> dbFetchQueryCreators(Integer userId,
			Integer accountId, String queryCreator) {
		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> dataMap = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;

		String queryCrtSql = " select * from (select pk_user, usr_firstname||' '||usr_lastname as user_name from er_user where fk_account="+accountId+
						"and pkg_superuser.F_get_Superuser_right("+userId+")>0) ";
			   queryCrtSql	=	queryCrtSql+ " where (LOWER(user_name) LIKE LOWER('%"+queryCreator+"%')) ";

		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(queryCrtSql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				dataMap = new HashMap<String, Object>();
				dataMap.put("id", rs.getInt(("pk_user")));
				dataMap.put("text", rs.getString("user_name"));
				maplist.add(dataMap);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return maplist;

	}
	
	public ArrayList<HashMap<String, Object>> dbFetchQueryStatuses(Integer userId,
			Integer accountId, String queryStatus) {
		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> dataMap = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;

		String queryStatSql = " select pk_codelst, codelst_desc from er_codelst where codelst_type='query_status' and codelst_hide<>'Y' ";
			queryStatSql	=	queryStatSql+ " and (LOWER(codelst_desc) LIKE LOWER('%"+queryStatus+"%')) ";

		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(queryStatSql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				dataMap = new HashMap<String, Object>();
				dataMap.put("id", rs.getInt(("pk_codelst")));
				dataMap.put("text", rs.getString("codelst_desc"));
				maplist.add(dataMap);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return maplist;

	}
	public int browserSaveFilter(String browserName,String searchUrl,String browsersearch_isdefault,String moduleName,int userId){
		int rowImpact=0;
		int id=0;
		Connection conn = null;
        PreparedStatement pstmt = null;
		String sql="";
		
		sql = "INSERT INTO er_browsersearch(pk_browsersearch,browsersearch_name, "
            + " browsersearch_criteria,browsersearch_isdefault,browsersearch_module,fk_user,creator) VALUES(?,?,?,?,?,?,?)";
		try{
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(sql);
			id = GenerateId.getId("seq_er_browsersearch", conn);
            pstmt.setInt(1, id);
            pstmt.setString(2, browserName);
            pstmt.setString(3, searchUrl);
            pstmt.setString(4,browsersearch_isdefault );
            pstmt.setString(5, moduleName);
            pstmt.setInt(6,userId);
            pstmt.setInt(7,userId);
            rowImpact=pstmt.executeUpdate();
           
		}catch(SQLException e){
			System.out.println("sqlException-");
			e.printStackTrace();
		}
		finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rowImpact;
	}
	public int browserSaveView(String searchUrl,String browsersearch_isdefault,String moduleName,int userId,String viewCriteria){
		int rowImpact=0;
		
		int id=0;
		Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
		String sql="";
		int count=0;
		
		try{
			conn = CommonDAO.getConnection();
			
			sql="select count(*) from er_savedview where FK_USER="+userId;
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				count=rs.getInt("count(*)");
			}
			if(count>0){
				sql ="update er_savedview set SAVEDVIEW_FILTERS=?,SAVEDVIEW_CRITERIA=?,SAVEDVIEW_MODULE=?,LAST_MODIFIED_BY=? where FK_USER=?";
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, searchUrl);
	            pstmt.setString(2,viewCriteria );
	            pstmt.setString(3,moduleName);
	            pstmt.setInt(4,userId);
	            pstmt.setInt(5,userId);}
			else{
				sql = "INSERT INTO er_savedview(PK_SAVEDVIEW,SAVEDVIEW_FILTERS,SAVEDVIEW_CRITERIA,SAVEDVIEW_ISDEFAULT,SAVEDVIEW_MODULE,FK_USER,CREATOR) VALUES(?,?,?,?,?,?,?)";
			pstmt = conn.prepareStatement(sql);
			id = GenerateId.getId("SEQ_ER_SAVEDVIEW", conn);
            pstmt.setInt(1, id);
            pstmt.setString(2, searchUrl);
            pstmt.setString(3,viewCriteria );
            pstmt.setString(4, browsersearch_isdefault);
            pstmt.setString(5,moduleName);
            pstmt.setInt(6,userId);
            pstmt.setInt(7,userId);}
            rowImpact=pstmt.executeUpdate();
           
		}catch(SQLException e){
			System.out.println("sqlException-");
			e.printStackTrace();
		}
		finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rowImpact;
	}
public ArrayList<HashMap<String, Object>> fetchSavedView(int userId){
	System.out.println("fetchSavedView method");
	ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
	HashMap<String, Object> dataMap = new HashMap<String, Object>();
	Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
	String sql="";
	int i=0;
	sql="select savedview_filters,savedview_criteria from er_savedview where fk_user="+userId;
	try{
		conn = CommonDAO.getConnection();
		pstmt = conn.prepareStatement(sql);
		rs = pstmt.executeQuery();
		while (rs.next()) {
			String savedViewFilter=rs.getString("savedview_filters");
			String savedViewCriteria=rs.getString("savedview_criteria");
			String urlFilterCtra=savedViewFilter.concat("&").concat(savedViewCriteria);
			dataMap.put("urlFilterCtra", urlFilterCtra);
			maplist.add(dataMap);
		}
	}catch(SQLException ex){
		ex.printStackTrace();
	}
	finally {
		try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return maplist;
}
public ArrayList<HashMap<String, Object>> getPatStudyFormRes(String studyNumber,String patStudyId){
	System.out.println("getPatStudyFormRes method in DashboardDao start");
	ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
	HashMap<String, Object> dataMap = null;
	Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
     int count=0;
    StringBuffer sqlBuffer = new StringBuffer();
    sqlBuffer.append("SELECT DISTINCT STY.PK_STUDY,STY.STUDY_NUMBER,per.pk_per,per.per_code,PAT.PK_PATPROT,LIN.FK_FILLEDFORM,");
    sqlBuffer.append(" LIN.FK_FORM,LIN.FILLDATE, LIBVER.PK_FORMLIBVER,LIBVER.FORMLIBVER_NUMBER,LINFRM.LF_ENTRYCHAR,ROW_NUMBER() OVER(order by PAT.PK_PATPROT )ROWNUMBER");
    sqlBuffer.append(" FROM ER_PATPROT PAT,ER_FORMSLINEAR LIN,ER_FORMLIBVER LIBVER,er_linkedforms LINFRM,ER_STUDY STY,er_per per");
    sqlBuffer.append(" WHERE STY.STUDY_NUMBER='"+studyNumber+"'");
    sqlBuffer.append(" AND pat.patprot_patstdid='"+patStudyId+"'");
    sqlBuffer.append(" AND per.pk_per=pat.fk_per");
    sqlBuffer.append(" AND PAT.PK_PATPROT    =LIN.FK_PATPROT AND LIN.FK_FORM       =LIBVER.FK_FORMLIB AND PAT.FK_STUDY      =STY.PK_STUDY");
    sqlBuffer.append(" AND LINFRM.FK_FORMLIB = LIN.FK_FORM  AND PAT.FK_PER        =PER.pk_per order by ROWNUMBER asc");
    System.out.println("sql-"+sqlBuffer.toString());
    try{
    	conn = CommonDAO.getConnection();
		pstmt = conn.prepareStatement(sqlBuffer.toString());
		rs=pstmt.executeQuery();
		while (rs.next()) {
			dataMap=new HashMap<String, Object>();
			dataMap.put("pk_study", rs.getInt(("PK_STUDY")));
			dataMap.put("study_number", rs.getString(("STUDY_NUMBER")));
			dataMap.put("pk_per", rs.getInt(("pk_per")));
     		dataMap.put("per_code", rs.getString(("per_code")));
			dataMap.put("pk_patprot", rs.getInt(("PK_PATPROT")));
			dataMap.put("fk_filledform", rs.getInt(("FK_FILLEDFORM")));
			dataMap.put("fk_form", rs.getInt(("FK_FORM")));
			dataMap.put("pk_formLibver", rs.getInt(("PK_FORMLIBVER")));
			dataMap.put("formLibver_number", rs.getInt(("FORMLIBVER_NUMBER")));
			dataMap.put("lf_entrychar", rs.getString(("LF_ENTRYCHAR")));
            dataMap.put("rowNumber", rs.getInt(("ROWNUMBER")));
			maplist.add(dataMap);
		}
    }catch(SQLException ex){
    	ex.printStackTrace();
    	
    }
    finally {
		try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
    return maplist;
}
public ArrayList<HashMap<String, Object>> getPatStudyFormQueryFetch(int filledForm){
	System.out.println("getPatStudyFormQueryFetch in DashboardDao.java and filledForm-"+filledForm);
	ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
	HashMap<String, Object> dataMap = null;
	Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    StringBuffer sqlBuffer = new StringBuffer();
    sqlBuffer.append("SELECT  patfrm.pk_patforms,patfrm.fk_formLib ,patfrm.fk_per,per.per_code,patprot.fk_study,std.study_number,patfrm.fk_patprot ,formqry.pk_formquery,");
    sqlBuffer.append(" formqry.fk_querymodule ,formqry.fk_field,fldlib.fld_name,frmqrystatus.FORMQUERY_TYPE,frmqrystatus.FK_CODELST_QUERYTYPE,frmqrystatus.QUERY_NOTES,frmqrystatus.ENTERED_ON ,");
    sqlBuffer.append(" frmqrystatus.FK_CODELST_QUERYSTATUS,codelst.codelst_desc");
    sqlBuffer.append(" FROM er_formquery formqry,er_patforms patfrm, er_patprot patprot,er_per per, er_study std,er_fldlib fldlib,er_formquerystatus frmqrystatus,er_codelst codelst");
    sqlBuffer.append(" WHERE fk_querymodule         ="+filledForm);
    sqlBuffer.append(" AND patfrm.pk_patforms       ="+filledForm);
    sqlBuffer.append(" AND patfrm.fk_patprot=patprot.pk_patprot AND per.pk_per=patfrm.fk_per AND std.PK_STUDY=patprot.fk_study AND formqry.fk_field=fldlib.pk_field AND frmqrystatus.FK_FORMQUERY=formqry.PK_FORMQUERY");
    sqlBuffer.append(" and frmqrystatus.FK_CODELST_QUERYSTATUS=codelst.pk_codelst");
    try{
    	conn = CommonDAO.getConnection();
		pstmt = conn.prepareStatement(sqlBuffer.toString());
		rs=pstmt.executeQuery();
		while (rs.next()) {
			dataMap=new HashMap<String, Object>();	
			dataMap.put("pk_study", rs.getInt(("FK_STUDY")));
			dataMap.put("pk_per", rs.getInt(("FK_PER")));
			dataMap.put("formLib", rs.getInt(("FK_formLib")));
			dataMap.put("fld_name", rs.getString(("fld_name")));
			dataMap.put("pk_formquery", rs.getInt(("pk_formquery")));
			dataMap.put("filledFormedId",rs.getInt("PK_PATFORMS"));
			maplist.add(dataMap);
		}
    }catch(SQLException sqlEx){
    	sqlEx.printStackTrace();
    }finally{
    	try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
	return maplist;
}
public ArrayList<HashMap<String, Object>> studyTeamRoleQueryFetch(int userId, int accountId,String studyIds,String tabSelected){
	ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
	HashMap<String, Object> dataMap = null;
	Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    int TRoleFlag=0;
    TRoleFlag=isTeamRoleNotRequired(tabSelected);
    if(TRoleFlag==0){
    	dataMap=new HashMap<String, Object>();
    	dataMap.put("codelst_subtyp", "tRoleNotRequired");
    	maplist.add(dataMap);
    }else{
    StringBuffer sqlBuffer = new StringBuffer();
    sqlBuffer.append(" select fk_user,fk_study,fk_codelst_tmrole, (select codelst_subtyp from er_codelst where pk_codelst=fk_codelst_tmrole) AS codelst_subtyp,");
    sqlBuffer.append(" (select codelst_desc from er_codelst where pk_codelst=fk_codelst_tmrole) AS Role_Type from er_studyteam");
    sqlBuffer.append("  where fk_study in ("+studyIds+")  and fk_user="+userId);
    try{
    	conn = CommonDAO.getConnection();
		pstmt = conn.prepareStatement(sqlBuffer.toString());
		rs=pstmt.executeQuery();
		while (rs.next()) {
			dataMap=new HashMap<String, Object>();
			dataMap.put("codelst_subtyp", rs.getString(("codelst_subtyp")));
			dataMap.put("pk_study", rs.getInt(("fk_study")));
			maplist.add(dataMap);
		}
    }catch(SQLException sqlEx){
    	sqlEx.printStackTrace();
    }finally{
    	try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    }
	return maplist;
}
public ArrayList<HashMap<String, Object>> fetchDefaultStRoleForSuper(int userId){
	ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
	HashMap<String, Object> dataMap = null;
	Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    StringBuffer sqlBuffer = new StringBuffer();
    sqlBuffer.append(" select fk_codelst_st_role,(SELECT codelst_subtyp FROM er_codelst WHERE pk_codelst=fk_codelst_st_role ) AS sup_codelst_subtyp");
    sqlBuffer.append(" from er_user,er_grps where pk_user="+userId+" and fk_grp_default=pk_grp");
    try{
    	conn = CommonDAO.getConnection();
		pstmt = conn.prepareStatement(sqlBuffer.toString());
		rs=pstmt.executeQuery();
		while (rs.next()) {
			dataMap=new HashMap<String, Object>();
			dataMap.put("codelst_subtyp", rs.getString(("sup_codelst_subtyp")));
			dataMap.put("pk_codelst", rs.getInt(("fk_codelst_st_role")));
			maplist.add(dataMap);
		}
    }catch(SQLException sqlEx){
    	sqlEx.printStackTrace();
    }finally{
    	try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
	return maplist;
}
/* Team Role Required or not  */
public int isTeamRoleNotRequired(String tabSelected){
	Connection conn = null;
	String  fromTab="";
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    String sql="";
    if("1".equals(tabSelected))
    	fromTab="SCH_CODELST";
    else
    	fromTab="ER_CODELST";
    int TRoleFlag=0;
    try{
    	conn = CommonDAO.getConnection();
    	sql=" select count(*) AS TROLE_FLAG from " +fromTab+ " where codelst_type='fillformstat' and codelst_hide <>'Y' and codelst_study_role <>'default_data' and codelst_study_role  <> 'default_data,' ";
		pstmt = conn.prepareStatement(sql);
		rs=pstmt.executeQuery();
		while (rs.next()) {
			TRoleFlag=rs.getInt("TROLE_FLAG");
		}
    }catch(SQLException sqlEx){
    	sqlEx.printStackTrace();
    }finally{
    	try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
	return TRoleFlag;
}

}
