/*
 * Classname : DashboardAgent
 * 
 * Version information: 1.0
 *
 * Date: 07/31/2014
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Ashwani Godara
 */

package com.velos.eres.dashboard.service;

import javax.ejb.Remote;

@Remote
public abstract interface DashboardAgent
{
	
}