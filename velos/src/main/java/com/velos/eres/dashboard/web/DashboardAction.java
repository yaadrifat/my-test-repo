package com.velos.eres.dashboard.web;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.crypto.Data;
import org.json.*;
import org.apache.struts2.ServletActionContext;

import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.velos.eres.querymanagement.web.QueryManagementJB;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.formQueryStatus.FormQueryStatusJB;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.web.formQuery.FormQueryJB;
import com.velos.eres.business.common.*;

public class DashboardAction extends ActionSupport {

	public static final String dashboardHome = "dashboardHome";
	public static final String protocolDashboardHome = "protocolDashboardHome";
	
	public static final String browsersearch_isdefault="N";
	private HttpServletRequest request = null;
	private HttpSession tSession = null;
	private HttpSession forwardSession = null;
	private ArrayList<HashMap<String, Object>> jsonData = null;
	
	private Integer userId = null;
	private Integer accountId = null;
    private String ipAdd=null;
	public void setJsonData(ArrayList<HashMap<String, Object>> jsonData) {
		this.jsonData = jsonData;
	}

	public ArrayList<HashMap<String, Object>> getJsonData() {
		return jsonData;
	}
	public DashboardAction() {
		ActionContext ac = ActionContext.getContext();
		request = (HttpServletRequest) ac
				.get(ServletActionContext.HTTP_REQUEST);
		tSession = request.getSession(false);

		userId = StringUtil.stringToInteger((String) tSession
				.getAttribute("userId"));
		accountId = StringUtil.stringToInteger((String) tSession
				.getAttribute("accountId"));
		ipAdd= (String) tSession
				.getAttribute("ipAdd");

	}

	public String fetchDashboardHome() {
		return dashboardHome;
	}

	public String dbFetchCodeLstStaues() {
		setJsonData((new DashboardJB().dbFetchCodeLstStaues(request
				.getParameter("codelstType"))));
		
		return SUCCESS;
	}
	
	public String dbFetchFormAdvStaues() {
		Integer tabType = Integer.parseInt(request.getParameter("tabType"));
		String codelstType = request.getParameter("codelstType");
		String studyNum=request.getParameter("studyNum");
		setJsonData((new DashboardJB().dbFetchFormAdvStaues(userId,accountId,codelstType,tabType,studyNum)));
		return SUCCESS;
	}
	
	public String dbFetchSavedSearches() {
		
		setJsonData((new DashboardJB().dbFetchSavedSearches(request
				.getParameter("searchModule"),userId)));
		return SUCCESS;
	}
	
	
	/** Functionality is being Added for Delete     */
	
	
	public String dbDeleteSavedSearches() {
		
		String  saveSearch = request.getParameter("savedSearchId");
		String modulename = request.getParameter("modulename");
		new DashboardJB().dbDeleteSavedSearches(saveSearch,modulename);
				

		setJsonData((new DashboardJB().dbFetchSavedSearches(request
				.getParameter("searchModule"),userId)));
		return SUCCESS;
	}
	


	public String dbFetchStudies() {
		String studyNum = request.getParameter("studyNum");
		setJsonData((new DashboardJB().dbFetchStudies(userId, accountId, studyNum)));
		return SUCCESS;
	}
		
	public String dbFetchOrganizations() {
		String studyNum = request.getParameter("studyNum");
		String orgName = request.getParameter("orgName");
		String selectedOrgId = request.getParameter("selectedOrg");
		Integer tabType = Integer.parseInt(request.getParameter("tabType"));
		setJsonData((new DashboardJB().dbFetchOrganizations(userId, accountId, studyNum, orgName, tabType, selectedOrgId)));
		return SUCCESS;
	}
	
	public String dbFetchForms() {
		String studyNum = request.getParameter("studyNum");
		String orgId = request.getParameter("orgId");
		String formName= request.getParameter("formName");
		Integer tabType = Integer.parseInt(request.getParameter("tabType"));
		setJsonData((new DashboardJB().dbFetchForms(userId, accountId, studyNum, orgId, formName, tabType)));
		return SUCCESS;
	}
	
	public String dbFetchFormStatues() {
		String statusDesc = request.getParameter("statusDesc");
		String studyNum = request.getParameter("studyNum");
		Integer tabType = Integer.parseInt(request.getParameter("tabType"));
		setJsonData((new DashboardJB().dbFetchFormStatues(userId, accountId,studyNum, statusDesc,tabType)));
		return SUCCESS;
	}
	
	public String dbFetchAEStatues() {
		String aeStatusDesc = request.getParameter("aeStatusDesc");
		String studyNum = request.getParameter("studyNum");
		setJsonData((new DashboardJB().dbFetchAEStatues(userId, accountId,studyNum, aeStatusDesc)));
		return SUCCESS;
	}
	
	public String dbFetchPatientIDs() {
		String studyNum = request.getParameter("studyNum");
		String orgId = request.getParameter("orgId");
		String patientId= request.getParameter("patientId");
		setJsonData((new DashboardJB().dbFetchPatientIDs(userId, accountId, studyNum, orgId, patientId)));
		return SUCCESS;
	}
	
	public String dbFetchPatientStudyIDs() {
		String studyNum = request.getParameter("studyNum");
		String orgId = request.getParameter("orgId");
		String studyPatientId= request.getParameter("studyPatientId");
		setJsonData((new DashboardJB().dbFetchPatientStudyIDs(userId, accountId, studyNum, orgId, studyPatientId)));
		return SUCCESS;
	}
	
	public String dbFetchCalendars() {
		String studyNum = request.getParameter("studyNum");
		String orgId = request.getParameter("orgId");
		String calendarName= request.getParameter("calendarName");
		setJsonData((new DashboardJB().dbFetchCalendars(userId, accountId, studyNum, orgId, calendarName)));
		return SUCCESS;
	}
	
	public String dbFetchVisits() {
		String calendarIds = request.getParameter("calendarIds");
		String visitName= request.getParameter("visitName");
		setJsonData((new DashboardJB().dbFetchVisits(userId, accountId, calendarIds, visitName)));
		return SUCCESS;
	}
	
	public String dbFetchEvents() {
		String visitIds = request.getParameter("visitIds");
		String eventName= request.getParameter("eventName");
		setJsonData((new DashboardJB().dbFetchEvents(userId, accountId, visitIds, eventName)));
		return SUCCESS;
	}
	
	public String dbFetchQueryCreators() {
		String queryCreator= request.getParameter("queryCreator");
		setJsonData((new DashboardJB().dbFetchQueryCreators(userId, accountId, queryCreator)));
		return SUCCESS;
	}
	
	public String dbFetchQueryStatuses() {
		String queryStatus= request.getParameter("queryStatus");
		setJsonData((new DashboardJB().dbFetchQueryStatuses(userId, accountId, queryStatus)));
		return SUCCESS;
	}

	public String fetchProtocolDashboardHome(){
		return protocolDashboardHome;
	}
	public String dbSaveFlterData(){
		int rowImpact=0;
		String browserName="";
		String dStudy="dStudy=";
		String dOrgId="dOrgId=";
		String dFormScreen="dFormScreen=";
		String dFormStatus="dFormStatus=";
		String dAeStatus="dAeStatus=";
		String dPatId="dPatId=";
		String dPatStdId="dPatStdId=";
		String dCalName="dCalName=";
		String dVisitName="dVisitName=";
		String dEventName="eventName=";
		String dQueryCtr="dQueryCtr=";
		String dQuerySts="dQuerySts=";
		String dtotalRes="dtotalRes=";
		String dQueryStsGT="dQueryStsGT=";
		String dDataEntFrom="dDataEntFrom=";
		String dDataEntTo="dDataEntTo=";
		String dEventDate="dEventDate=";
		String dShowHide="dShowHide=";
		String searchUrl1="";
		String dMOduleName="dMOduleName=";
		String moduleUrl="";
		String moduleName="";
		String dtabSelected="dtabSelected=";
		String dIDisplayLength="dIDisplayLength=";
		String dIDisplayStart="dIDisplayStart=";
		String dSortCol="sortCol=";
		String dSortDir="sortDir=";
		String sortCol="";
		String sortDir="";
		String sortCriteria [];
		String saveType=request.getParameter("saveType");
		String tabSelected=request.getParameter("tabSelected");
		tabSelected=dtabSelected+tabSelected;
		String iDisplayLength=request.getParameter("iDisplayLength");
		iDisplayLength=dIDisplayLength+iDisplayLength;
		String iDisplayStart=request.getParameter("iDisplayStart");
		iDisplayStart=dIDisplayStart+iDisplayStart;
		String sortAttr=request.getParameter("sortAttr");
		sortCriteria=sortAttr.split(",");
		sortCol=dSortCol+sortCriteria[0];
		sortDir=dSortDir+sortCriteria[1];
		StringBuffer viewCriteria=new StringBuffer();
		viewCriteria.append(tabSelected).append("&");
		viewCriteria.append(iDisplayLength).append("&");
		viewCriteria.append(iDisplayStart).append("&");
		viewCriteria.append(sortCol).append("&");
		viewCriteria.append(sortDir);
		StringBuffer searchUrl=new StringBuffer();
		browserName=request.getParameter("saveAs");
		moduleName=request.getParameter("moduleName");
		moduleUrl=dMOduleName+moduleName;
		String studyNum =(request.getParameter("studyIds")==null) ? "" : (request.getParameter("studyIds"));
		studyNum=studyNum.replaceAll(",", "%2C");
		studyNum=dStudy+studyNum;
		String orgId=(request.getParameter("orgIds")==null) ? "" : (request.getParameter("orgIds"));
		orgId=orgId.replaceAll(",", "%2C");
		orgId=dOrgId+orgId;
		String formDataScreen=(request.getParameter("selectFormsIds")==null) ? "" : (request.getParameter("selectFormsIds"));
		formDataScreen=formDataScreen.replaceAll(",", "%2C");
		formDataScreen=dFormScreen+formDataScreen;
		String formStatus=(request.getParameter("formStatus")==null) ? "" : (request.getParameter("formStatus"));
		formStatus=formStatus.replaceAll(",", "%2C");
		formStatus=dFormStatus+formStatus;
		String aeStatus=(request.getParameter("aeStatus")==null) ? "" : (request.getParameter("aeStatus"));
		aeStatus=aeStatus.replaceAll(",", "%2C");
		aeStatus=dAeStatus+aeStatus;
		String dataEntryFrom=(request.getParameter("selectedDataEntryDateFrom")==null) ? "" : (request.getParameter("selectedDataEntryDateFrom"));
		dataEntryFrom=dDataEntFrom+dataEntryFrom;
		String dataEntryTo=(request.getParameter("selectedDataEntryDateTo")==null) ? "" : (request.getParameter("selectedDataEntryDateTo"));
		dataEntryTo=dDataEntTo+dataEntryTo;
		String patientId=(request.getParameter("patientId")==null) ? "" : (request.getParameter("patientId"));
		patientId=patientId.replaceAll(",", "%2C");
		patientId=dPatId+patientId;
		String patinetStudyId=(request.getParameter("patStudyId")==null) ? "" : (request.getParameter("patStudyId"));
		patinetStudyId=patinetStudyId.replaceAll(",", "%2C");
		patinetStudyId=dPatStdId+patinetStudyId;
		String calName=(request.getParameter("calName")==null) ? "" : (request.getParameter("calName"));
		calName=calName.replaceAll(",", "%2C");
		calName=dCalName+calName;
		String visitName=(request.getParameter("visitName")==null) ? "" : (request.getParameter("visitName"));
		visitName=visitName.replaceAll(",", "%2C");
		visitName=dVisitName+visitName;
		String eventName=(request.getParameter("eventName")==null) ? "" : (request.getParameter("eventName"));
		eventName=eventName.replaceAll(",", "%2C");
		eventName=dEventName+eventName;
		String eventDate=(request.getParameter("selectedEventDate")==null) ? "" : (request.getParameter("selectedEventDate"));
		eventDate=dEventDate+eventDate;
		String queryCreator=(request.getParameter("queryCreator")==null) ? "" : (request.getParameter("queryCreator"));
		queryCreator=queryCreator.replaceAll(",", "%2C");
		queryCreator=dQueryCtr+queryCreator;
		String queryStatus=(request.getParameter("queryStatus")==null) ? "" : (request.getParameter("queryStatus"));
		queryStatus=queryStatus.replaceAll(",", "%2C");
		queryStatus=dQuerySts+queryStatus;
		String queryStatusGt=(request.getParameter("selectedqueryStatuess")==null) ? "" : (request.getParameter("selectedqueryStatuess"));
		queryStatusGt=dQueryStsGT+queryStatusGt;
		String totalResQuery=(request.getParameter("selectedTotalRespQuery")==null) ? "" : (request.getParameter("selectedTotalRespQuery"));
		String showHide=(request.getParameter("showHide")==null) ? "" : (request.getParameter("showHide"));
		showHide=dShowHide+showHide;
		totalResQuery=dtotalRes+totalResQuery;
		searchUrl1=studyNum+"&"+orgId+"&"+formDataScreen+"&"+formStatus+"&"+aeStatus+"&"+patientId+"&"+patinetStudyId+"&"+calName+"&"+visitName+"&"+eventName+"&"+queryCreator+"&"+queryStatus+"&"+dQueryStsGT+"&"+dtotalRes;
		searchUrl.append(studyNum).append("&");
		searchUrl.append(orgId).append("&");
		searchUrl.append(formDataScreen).append("&");
		searchUrl.append(formStatus).append("&");
		searchUrl.append(aeStatus).append("&");
		searchUrl.append(patientId).append("&");
		searchUrl.append(patinetStudyId).append("&");
		searchUrl.append(calName).append("&");
		searchUrl.append(visitName).append("&");
		searchUrl.append(eventName).append("&");
		searchUrl.append(queryCreator).append("&");
		searchUrl.append(queryStatus).append("&");
		searchUrl.append(queryStatusGt).append("&");
		searchUrl.append(totalResQuery).append("&");
		searchUrl.append(dataEntryTo).append("&");
		searchUrl.append(dataEntryFrom).append("&");
		searchUrl.append(eventDate).append("&");
		searchUrl.append(showHide).append("&");
		searchUrl.append(moduleUrl);
		if(saveType.equals("saveSearch")){
		rowImpact=new DashboardJB().saveSearchFilter(browserName,searchUrl.toString(),browsersearch_isdefault,moduleName,userId);
	    }else{
	    	rowImpact=new DashboardJB().saveViewFilter(searchUrl.toString(),browsersearch_isdefault,moduleName,userId,viewCriteria.toString());
	    }
		return SUCCESS;
	}
	public String dbfetchSavedSelected(){
		HashMap<String,Object> hm=new HashMap<String,Object>();
		 ArrayList<HashMap<String, Object>> tempjsonSrchDta=new  ArrayList<HashMap<String, Object>>();
		String dStudy = request.getParameter("dStudy");
		hm.put("dStudy", dStudy);
		String dOrgId  = request.getParameter("dOrgId");
		hm.put("dOrgId", dOrgId);
		String dFormScreen=request.getParameter("dFormScreen");
		hm.put("dFormScreen", dFormScreen);
		String dFormStatus=request.getParameter("dFormStatus");
		hm.put("dFormStatus", dFormStatus);
		String dAeStatus=request.getParameter("dAeStatus");
		hm.put("dAeStatus", dAeStatus);
		String dPatId = request.getParameter("dPatId");
		hm.put("dPatId", dPatId);
		System.out.println("patientId-"+dPatId);
		String dPatStdId=request.getParameter("dPatStdId");
		hm.put("dPatStdId", dPatStdId);
		String dCalName=request.getParameter("dCalName");
		hm.put("dCalName", dCalName);
		String dVisitName=request.getParameter("dVisitName");
		hm.put("dVisitName", dVisitName);
		String deventName=request.getParameter("eventName");
		hm.put("deventName", deventName);
		String dQuerySts=request.getParameter("dQuerySts");
		hm.put("dQuerySts", dQuerySts);
		String dQueryStatusess=request.getParameter("dQueryStsGT");
		hm.put("dQueryStatusess", dQueryStatusess);
		String dDataEntFrom=request.getParameter("dDataEntFrom");
		hm.put("dDataEntFrom", dDataEntFrom);
		String dDataEntTo=request.getParameter("dDataEntTo");
		hm.put("dDataEntTo", dDataEntTo);
		String dEventDate=request.getParameter("dEventDate");
		hm.put("dEventDate", dEventDate);
		String dMOduleName=request.getParameter("dMOduleName");
		hm.put("dMOduleName", dMOduleName);
		String dQueryCtr=request.getParameter("dQueryCtr");
		hm.put("dQueryCtr", dQueryCtr);
		String dtotalRes=request.getParameter("dtotalRes");
		hm.put("dtotalRes", dtotalRes);
        HashMap<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("userId", StringUtil.integerToString(userId));
		paramMap.put("accountId", StringUtil.integerToString(accountId));
		paramMap.put("studyIds", dStudy);
		paramMap.put("orgIds", dOrgId);
		paramMap.put("patientId", dPatId);
		tempjsonSrchDta.add(hm);
		setJsonData(tempjsonSrchDta);
		return SUCCESS;
	}
	public String dbfetchSavedView(){
	    setJsonData(new DashboardJB().fetchSavedView(userId));
	    return SUCCESS;
	}
	public String patStudyFormQuery(){
		HashMap<String,Object> hm=new HashMap<String,Object>();
		ArrayList<HashMap<String, Object>> jsonData1=new ArrayList<HashMap<String, Object>>();	
		String studyNumber=request.getParameter("studyNumber");
		String patStudyId=request.getParameter("patStudyId");
	    String indexNum=request.getParameter("indexNum");
	    System.out.println("index-"+indexNum);
	    if(indexNum != null){
	    	forwardSession=request.getSession(false);
	    	jsonData=(ArrayList<HashMap<String, Object>>)forwardSession.getAttribute("jsonData");
	    	if(jsonData.size()!=0){
	    		if(StringUtil.stringToInteger(indexNum)<=jsonData.size()){
	    			
	    			hm=jsonData.get(StringUtil.stringToInteger(indexNum));
	    			jsonData1.add(hm);
	    			setJsonData(jsonData1);
	    		}else{
	    			hm=jsonData.get(0);
	    			jsonData1.add(hm);
	    			setJsonData(jsonData1);
	    		}
	    	}
	    }
	    else{
			setJsonData(new DashboardJB().getPatStudyFormRes(studyNumber,patStudyId));
			forwardSession=request.getSession();
			forwardSession.setAttribute("jsonData", jsonData);
	    	
	    }
		return SUCCESS;
	}
	public String patStudyFormQueryFetch(){
		int filledForm=Integer.parseInt(request.getParameter("filledForm"));
		setJsonData(new DashboardJB().getPatStudyFormQueryFetch(filledForm));
		return SUCCESS;
	}
	/**
	 * Save the query response to new enhancement(Jupiter).
	 * 
	* @author Vivek
	 */
	public String saveQueryResponse(){
		HashMap<String,Object>insertData=new HashMap<String,Object>();
		ArrayList<HashMap<String, Object>> tempjsonDta=new  ArrayList<HashMap<String, Object>>();
		int rowImpact=0;
		FormQueryStatusJB formQueryStatusB=new FormQueryStatusJB();
		String enteredOn =  DateUtil.getCurrentDateTime();
		String formQueryId=request.getParameter("formQueryId");
		String strstatusId=request.getParameter("queryStatus");
		String formQueryType=request.getParameter("formQueryType");
		String instructions=request.getParameter("instructions");
		formQueryStatusB.setFormQueryId(formQueryId);
		formQueryStatusB.setFormQueryType(formQueryType);
		formQueryStatusB.setQueryTypeId(null);
		formQueryStatusB.setQueryNotes(instructions);
		formQueryStatusB.setEnteredBy(new Integer(userId).toString());
		formQueryStatusB.setEnteredOn(enteredOn);
		formQueryStatusB.setQueryStatusId(strstatusId);
		formQueryStatusB.setCreator(new Integer(userId).toString());
		formQueryStatusB.setIpAdd(ipAdd);
		formQueryStatusB.saveQueryResponse();
		rowImpact = formQueryStatusB.getFormQueryStatusId();
		insertData.put("rowImpact", rowImpact);
		tempjsonDta.add(insertData);
		setJsonData(tempjsonDta);
		System.out.println("success");
	  return SUCCESS;
	}
	public String patStudyQuerySave(){
		FormQueryJB formQueryB=new FormQueryJB();
		String fieldId=request.getParameter("fieldId");
		String formQueryType=request.getParameter("formQueryType");
		String calledFrom=request.getParameter("calledFrom");
		String filledFormId=request.getParameter("filledFormId");
		String queryStatus=request.getParameter("queryStatus");
		String queryTypeId=request.getParameter("queryTypeId");
		String instructions=request.getParameter("instructions");
		String enteredOn =  DateUtil.getCurrentDateTime();
		int ret = formQueryB.insertQueryResponse(Integer.parseInt(filledFormId) , Integer.parseInt(calledFrom) , Integer.parseInt(fieldId),
				Integer.parseInt(formQueryType) , Integer.parseInt(queryTypeId) ,
				Integer.parseInt(queryStatus) , instructions,
				   enteredOn ,userId.intValue(),
				   userId.intValue() , ipAdd );
		return SUCCESS;
	}
	public String findFieldDetails(){
		HashMap<String,Object>fieldMap=new HashMap<String,Object>();
		ArrayList<HashMap<String, Object>> tempjsonDta=new  ArrayList<HashMap<String, Object>>();
		FormFieldDao formFieldDao=new FormFieldDao();
		String filledFormId=request.getParameter("filledFormId");
		String fieldId=request.getParameter("fieldId");
		String formType=request.getParameter("formType");
		int tempField=formFieldDao.findFieldDetails(Integer.parseInt(filledFormId), fieldId,formType);
		fieldMap.put(fieldId, tempField);
		tempjsonDta.add(fieldMap);
		setJsonData(tempjsonDta);
		return SUCCESS;
	}
	public String adversetColorFlagData(){
		
		ArrayList<HashMap<String, Object>> tempjsonDta=new  ArrayList<HashMap<String, Object>>();
		String responseId=request.getParameter("responseId");
		FormQueryDao formQueryDao=new FormQueryDao();
		tempjsonDta=formQueryDao.getAdverseEventFieldQuery(Integer.parseInt(responseId));
		setJsonData(tempjsonDta);
		return SUCCESS;
	}
	public String studyTeamRoleQueryFetch(){
		String studyIds=request.getParameter("studyIds");
		String tabSelected=request.getParameter("tabSelected");
		setJsonData(new DashboardJB().studyTeamRoleQueryFetch(userId, accountId,studyIds,tabSelected));
		return SUCCESS;
	}
	public String fetchDefaultAdverseFormData(){
		return SUCCESS;
	}
	/*public String viewRightOrgBaseToUse(){
		System.out.println("find the viewRightOrgBaseToUse in DashboardAction  Class");
		String siteId=request.getParameter("siteId");
		setJsonData(new DashboardJB().viewRightOrgBaseToUse(userId, siteId));
		return SUCCESS;
	}*/
	public String fetchDefaultStRoleForSuper(){
		setJsonData(new DashboardJB().fetchDefaultStRoleForSuper(userId));
		return SUCCESS;
	}
}
