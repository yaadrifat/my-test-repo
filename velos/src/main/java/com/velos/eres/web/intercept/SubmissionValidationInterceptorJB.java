package com.velos.eres.web.intercept;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.velos.eres.business.common.StudyINDIDEDAO;
import com.velos.eres.business.common.StudyIdDao;
import com.velos.eres.business.common.StudyVerDao;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.appendix.AppendixJB;
import com.velos.eres.web.intercept.InterceptorJB;
import com.velos.eres.web.study.StudyJB;
import com.velos.eres.web.studyId.StudyIdJB;
import com.velos.services.model.Code;
import com.velos.services.util.CodeCache;

public class SubmissionValidationInterceptorJB extends InterceptorJB {
  private static final String RESULT_TEXT_KEY = "resultTextKey";
  private static final String RESULT_FLAG_KEY = "resultFlagKey";
  private static final String TEST_TEXT_KEY = "testTextKey";
  private static final String STUDY_ID = "studyId";
  private static final String STUDY_INCLUDES_DRUGS = "option1" ;
  private static final String STUDY_INCLUDES_DEVICES = "option2" ;
  private static final String STUDY_OBJECTIVES_SECONDARY = "option1";
  private static final String STUDY_OBJECTIVES_EXPLORATORY = "option2";
  private static final String STUDY_OBJECTIVES_BIOMARKER = "option3";
  private static final String OTHER = "OTHER" ;
  private static final String CRITERIA_REMOVAL_PARTICIPANTS_OTHER = "option16";
  private static final String NA_DSMB = "notAppl";
  private static final String OPTION_YES = "YES";
  private static final String OPTION_NO = "NO" ;
  private static final Integer SUCCESS_FLAG = 0;
  private static final Integer FAILURE_FLAG = -1;
  private static final Integer NOT_REQ_FLAG = -2;
  private static final String PROTOCOL_TEMPLATE_MDACC = "MDACCInvIni";
  private static final String PROTOCOL_TEMPLATE_INDUSTRY = "indstrStdy";
  private static final String RANDOMIZATION_TYPE = "randomization";
  private static final String NONE_STR = "none";
  private static final String SCOPE_TYPE = "studyscope";
  private static final String SCOPE_ONLY_MDACC = "std_cen_single";
  private static final String SCOPE_MULTI_CENTER = "std_cen_multi";
  private static final String SCOPE_MULTI_MDACC_LEAD = "studyscope_3";
  private static final String SCOPE_ONLY_OTHER = "onlyOther";

  private String otherObjective;
  boolean isBioMarkerObjSelected = false;
  private String protocol_Template;
  private CodeCache codeCache = CodeCache.getInstance();

  public void handle(Object... args) {
    HttpServletRequest request = (HttpServletRequest) args[0];
    String studyId = request.getParameter(STUDY_ID);
    if (StringUtil.isEmpty(studyId)) {
      if (request.getSession(false) != null) {
        studyId = (String) request.getSession(false).getAttribute(studyId);
      }
    }
    // System.out.println("studyId="+studyId);
    if (studyId == null) {
      return;
    }
    ArrayList<Object> customValidationList = (ArrayList<Object>) args[1];
    StudyJB studyJB = new StudyJB();
    studyJB.setId(StringUtil.stringToNum(studyId));
    studyJB.getStudyDetails();
    validateStandardFields(studyJB, customValidationList, studyId);
    validateAttachmentLengths(customValidationList, studyId);
  }
  
  private static final String FIX_ATTACHMENT = "Fix attachment";
  private void validateAttachmentLengths(ArrayList<Object> customValidationList, String studyId) {
	  AppendixJB appendixJB = new AppendixJB();
	  StudyVerDao studyVerDao = appendixJB.getStudyVerAndApndxDetails(StringUtil.stringToNum(studyId), null);
	  ArrayList apndxLens = studyVerDao.getApndxLens();
	  ArrayList apndxUris = studyVerDao.getApndxUris();
	  ArrayList apndxPks = studyVerDao.getApndxPks();
	  if (apndxLens != null) {
		  for (int iX = 0; iX < apndxLens.size(); iX++) {
			  if (((Integer) apndxLens.get(iX)) == 0) {
				  this.populateCustomValidationListForErrors(
						  customValidationList, 
						  "Attachement is blank for "+apndxUris.get(iX)+
						  ". Would you like to fix it? <a href='javascript:openFixAttachmentDialog("+
								  apndxPks.get(iX)+",\""+apndxUris.get(iX)+"\");'>Fix</a>"
						  , FIX_ATTACHMENT, true);
			  }
		  }
	  }
  }

  private void validateStandardFields(StudyJB studyJB, ArrayList<Object> customValidationList,
      String studyId) {
    StudyIdDao studyIdDao = new StudyIdDao();
    Map<String, String> codeLst_studyIdPkMap = studyIdDao.getCodelst_Subtyp_StudyIdPkMap(studyId);
    Map<String, String> studySecFieldsMap = studyIdDao.getStudySecFields(studyId);
    boolean mdaccTemplate = false;
    boolean industryTemplate = false;
    
    
 //first check the protocol template and then decide which way to go ! 
    protocol_Template = checkStudyProtocolTemplateField(studyId, studyIdDao, codeLst_studyIdPkMap, customValidationList);// study protocol template
    if (protocol_Template != null){
      
      if (protocol_Template.equals(PROTOCOL_TEMPLATE_MDACC))
        mdaccTemplate = true;
      
      else if (protocol_Template.equals(PROTOCOL_TEMPLATE_INDUSTRY))
        industryTemplate = true;
   
    }    
    
    checkStudyContactField(studyJB, customValidationList);// study contact
    checkStudyDivisionField(studyJB, customValidationList); // study division
    checkStudyDepartmentField(studyJB, customValidationList);// study department
    checkPrincipalInvestigtorField(studyJB, customValidationList);// Check Principal Investigator
    checkRoleMdaccPrincipalInvestigtorField(studyJB, customValidationList, studyId, studyIdDao, codeLst_studyIdPkMap);
    //checkProtocolRequiredConsentField(studyJB, customValidationList);
    checkResearchTypeField(studyJB, customValidationList);
    checkClinicalResearchCategoryField(studyJB, customValidationList);
    checkPrimaryPurposeField(studyJB, customValidationList);
    //checkExpBioStatReviewField(studyId, studyIdDao, codeLst_studyIdPkMap, customValidationList);
    checkStudyIncludesField(studyId, studyIdDao, codeLst_studyIdPkMap, customValidationList,mdaccTemplate,industryTemplate);// study includes,drugs devices section
    
    
    
    if (industryTemplate) {
      // check for the additional fields for the industry specific template
      checkSponsorProtocolNumberField(studyJB, customValidationList);// Check Principal Investigator
      checkSecondaryDepartmentField(studyId, studyIdDao, codeLst_studyIdPkMap, customValidationList);
      
    }
    
    
   //*** study objectives section***********************************************************************************
    if (industryTemplate) {
      checkPrimaryObjectiveField(studyJB, customValidationList);
      checkForStudyObjectivesFields(studyId, studyIdDao, codeLst_studyIdPkMap,
          customValidationList, false);
    } else if (mdaccTemplate) {
      checkPrimaryObjectiveField(studyJB, customValidationList);
      checkForStudyObjectivesFields(studyId, studyIdDao, codeLst_studyIdPkMap,
          customValidationList, false);
    }
    
    //****** Study Background Section ********************************************************************************
    
    if (mdaccTemplate) {
      checkStudyBackgroundFields(studyJB, customValidationList, studyId, studyIdDao,
          studySecFieldsMap); //
    }
    
    
    //*** Study Design Section**********************************************************************************************
    if (industryTemplate) {
      checkStudyDesignSectionField(studyJB, customValidationList, studyId, studyIdDao,
          codeLst_studyIdPkMap, true);
      checkSpecificSitesField(studyJB, customValidationList, studyId, studyIdDao, codeLst_studyIdPkMap);
      

    } else if (mdaccTemplate) {
      checkSpecificSite(studyJB, customValidationList);
      checkDiseaseSite(studyJB, customValidationList);
      checkTreatmentArms(customValidationList, studyId, studyIdDao, codeLst_studyIdPkMap);
      checkSampleSizeJustification(customValidationList, studyId, studyIdDao, codeLst_studyIdPkMap);
      
      checkStudyDesignSectionField(studyJB, customValidationList, studyId, studyIdDao,
          codeLst_studyIdPkMap, false);
      
    }
    
    checkStudyScopeFields(studyJB, customValidationList, studyId, studyIdDao, codeLst_studyIdPkMap); // study scope
    
    //************Subject selection****************************************************************************************
    
    if (industryTemplate) {
      checkSubjectSelectionFields(studyJB, customValidationList, studyId, studyIdDao, codeLst_studyIdPkMap);
    }
    
    
    //*******IND/IDE Section************************************************************************************************
   // checkForStudyInd_Ide_Fields(studyId, customValidationList);// check for IND/IDE fields
    
    //******IND Exempt Section***********************************************************************************************
    
    if (industryTemplate) {
      checkStudyRequiresInd(studyId, studyIdDao, codeLst_studyIdPkMap, customValidationList);
    }
    
    // *****Study Drugs Section
    // ***********************************************************************************************
    // this is checked in the Study includes section **********

    // Study Devices Section
    // *************************************************************************************************
    // this is checked in the Study Includes Section ***********
    
    
    // ********Withdrawal and Replacement of Participants section
    // ***********************************************************
    
    checkForWithdrawlOfParticipantFields(studyId, studyIdDao, codeLst_studyIdPkMap,customValidationList);// Withdrawl and replacement of participants
    if (industryTemplate) {  
      //checkCriteriaReplacement(studyId, studyIdDao, codeLst_studyIdPkMap, customValidationList);
    }
    
    checkForStudyEvaluationProceduresFields(studyId, studyIdDao, studySecFieldsMap,customValidationList);// Study Evaluation and Procedures
    //if (industryTemplate) {  
      checkFreshBiopsyNeedField(studyId, studyIdDao, codeLst_studyIdPkMap, customValidationList);
   // }
      //checkBlindingField(studyJB, customValidationList);
      
    
    // **********Biomarker and Correlative Studies section
    // ************************************************************
    if (industryTemplate) {
    	checkForBiomarkerCorrelativeFields(studyId, studyIdDao, codeLst_studyIdPkMap,customValidationList);
    }
    //*****Statistical Considerations section *************************************************************************
    
    checkForStatisticalConsiderationsFields(studyId, studyIdDao, codeLst_studyIdPkMap,customValidationList, mdaccTemplate, industryTemplate );// check for statistical considerations fields
    
    //******* Safety and Adverse Events Section
    
    checkForIfCommerciallyAvailableField(studyId, studyIdDao, codeLst_studyIdPkMap, customValidationList);
    
    if (mdaccTemplate){
      checkForReportingToSponsorField(studyId, studyIdDao, codeLst_studyIdPkMap, customValidationList);
      checkForReportSaeUnanticipatedProblems(studyId, studyIdDao, codeLst_studyIdPkMap, customValidationList);
    }
    
    if (industryTemplate) {
      checkSafetyAdverseEventFields(studyId, studyIdDao, codeLst_studyIdPkMap, customValidationList);
    }

    // *** Data Safety and monitoring plan Section
    // **************************************************************************
    if (mdaccTemplate){
      checkForDataSafetyMonitoringPlanFields(studyId, studyIdDao, studySecFieldsMap,customValidationList);
    }
    
    // ****** financial support section
    // **********************************************************************************
    checkForFinancialSupportFields(studyId, studyIdDao, codeLst_studyIdPkMap, customValidationList);// financial support
    
    
    //********* References and Reviewer Section *********************************************
    
     if (mdaccTemplate){
       checkForReferencesFields(studyId, studyIdDao, codeLst_studyIdPkMap, customValidationList);// references
       checkForNominatedReviewerFields(studyId, studyIdDao, codeLst_studyIdPkMap, customValidationList);// check for nominated reviewer fields   
     }
    
  }

  private void checkSampleSizeJustification(ArrayList<Object> customValidationList, String studyId,
      StudyIdDao studyIdDao, Map<String, String> codeLst_studyIdPkMap) {
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.SAMPLE_SIZE_JUSTIFICATION.getCodelst_subtyp(), customValidationList,
        SubmissionErrorMsg.SAMPLE_SIZE_JUSTIFICATION_MISSING.getResultText(),
        SubmissionErrorMsg.SAMPLE_SIZE_JUSTIFICATION_MISSING.getTestText(), codeLst_studyIdPkMap, true);
    
  }

  private void checkForReportSaeUnanticipatedProblems(String studyId, StudyIdDao studyIdDao,
      Map<String, String> finalMap, ArrayList<Object> customValidationList) {
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.REPORT_OF_SAE_UNANTICIPATED_PROBLEMS.getCodelst_subtyp(), customValidationList,
        SubmissionErrorMsg.REPORT_OF_SAE_UNANTICIPATED_PROBLEMS_MISSING.getResultText(),
        SubmissionErrorMsg.REPORT_OF_SAE_UNANTICIPATED_PROBLEMS_MISSING.getTestText(), finalMap, true);
    
  }

  private void checkForIfCommerciallyAvailableField(String studyId, StudyIdDao studyIdDao,
      Map<String, String> finalMap, ArrayList<Object> customValidationList) {
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.COMMERCIAL_DRUG_INSERT.getCodelst_subtyp(), customValidationList,
        SubmissionErrorMsg.COMMERCIAL_DRUG_INSERT_MISSING.getResultText(),
        SubmissionErrorMsg.COMMERCIAL_DRUG_INSERT_MISSING.getTestText(), finalMap, true);

    
  }

  private void checkPrimaryPurposeField(StudyJB studyJB, ArrayList<Object> customValidationList) {
    checkStandardFields(studyJB, customValidationList, studyJB.getStudyPurpose(),
        SubmissionErrorMsg.PRIMARY_PURPOSE_MISSING.getResultText(),
        SubmissionErrorMsg.PRIMARY_PURPOSE_MISSING.getTestText());
    
  }

  private void checkClinicalResearchCategoryField(StudyJB studyJB,
      ArrayList<Object> customValidationList) {
    checkStandardFields(studyJB, customValidationList, studyJB.getStudyType(),
        SubmissionErrorMsg.CLINICAL_RESEARCH_CATEGORY_MISSING.getResultText(),
        SubmissionErrorMsg.CLINICAL_RESEARCH_CATEGORY_MISSING.getTestText());
    
  }

  private void checkResearchTypeField(StudyJB studyJB, ArrayList<Object> customValidationList) {
    checkStandardFields(studyJB, customValidationList, studyJB.getStudyResType(),
        SubmissionErrorMsg.RESEARCH_TYPE_MISSING.getResultText(),
        SubmissionErrorMsg.RESEARCH_TYPE_MISSING.getTestText());
    
  }

  private void checkProtocolRequiredConsentField(StudyJB studyJB,
      ArrayList<Object> customValidationList) {
    checkStandardFields(studyJB, customValidationList, studyJB.getCcsgReportableStudy(),
        SubmissionErrorMsg.PROTOCOL_REQUIRES_SIGNED_INFORMED_CONSENT_MISSING.getResultText(),
        SubmissionErrorMsg.PROTOCOL_REQUIRES_SIGNED_INFORMED_CONSENT_MISSING.getTestText());
   
    
  }

  private void checkFreshBiopsyNeedField(String studyId, StudyIdDao studyIdDao,
      Map<String, String> codeLst_studyIdPkMap, ArrayList<Object> customValidationList) {
    String freshBiopsyNeeded = checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.FRESH_BIOPSY_NEEDED.getCodelst_subtyp(),
        customValidationList,
        SubmissionErrorMsg.FRESH_BIOPSY_NEEDED_MISSING.getResultText(),
        SubmissionErrorMsg.FRESH_BIOPSY_NEEDED_MISSING.getTestText(),
        codeLst_studyIdPkMap, true);
    
    if (freshBiopsyNeeded != null && freshBiopsyNeeded.toUpperCase().equals(OPTION_YES)){
      checkMoreStudyDetailsFields(studyId, studyIdDao,
          MoreStudyDetailsFormFields.FRESH_BIOPSY_NEEDED_YES.getCodelst_subtyp(),
          customValidationList,
          SubmissionErrorMsg.FRESH_BIOPSY_NEEDED_YES_MISSING.getResultText(),
          SubmissionErrorMsg.FRESH_BIOPSY_NEEDED_YES_MISSING.getTestText(),
          codeLst_studyIdPkMap, true);
    }
    
  }

  private void checkSubjectSelectionFields(StudyJB studyJB, ArrayList<Object> customValidationList,
      String studyId, StudyIdDao studyIdDao, Map<String, String> codeLst_studyIdPkMap) {
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.SUBJECT_INCLUSION_CREITERIA.getCodelst_subtyp(),
        customValidationList,
        SubmissionErrorMsg.SUBJECT_INCLUSION_CREITERIA_MISSING.getResultText(),
        SubmissionErrorMsg.SUBJECT_INCLUSION_CREITERIA_MISSING.getTestText(),
        codeLst_studyIdPkMap, true);
    
    /*checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.SUBJECT_INCLUSION_CREITERIA_NA.getCodelst_subtyp(),
        customValidationList,
        SubmissionErrorMsg.SUBJECT_INCLUSION_CREITERIA_NA_MISSING.getResultText(),
        SubmissionErrorMsg.SUBJECT_INCLUSION_CREITERIA_NA_MISSING.getTestText(),
        codeLst_studyIdPkMap, true);*/
    
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields. SUBJECT_EXCLUSION_CREITERIA.getCodelst_subtyp(),
        customValidationList,
        SubmissionErrorMsg. SUBJECT_EXCLUSION_CREITERIA_MISSING.getResultText(),
        SubmissionErrorMsg. SUBJECT_EXCLUSION_CREITERIA_MISSING.getTestText(),
        codeLst_studyIdPkMap, true);
    
    /*checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.SUBJECT_EXCLUSION_CREITERIA_NA.getCodelst_subtyp(),
        customValidationList,
        SubmissionErrorMsg.SUBJECT_EXCLUSION_CREITERIA_NA_MISSING.getResultText(),
        SubmissionErrorMsg.SUBJECT_EXCLUSION_CREITERIA_NA_MISSING.getTestText(),
        codeLst_studyIdPkMap, true);*/
    
  }

  private void checkPrimaryObjectiveField(StudyJB studyJB, ArrayList<Object> customValidationList) {
    checkStandardFields(studyJB, customValidationList, studyJB.getStudyObjective(),
        SubmissionErrorMsg.STUDY_PRIMARY_OBJECTIVE_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_PRIMARY_OBJECTIVE_MISSING.getTestText());
    
  }

  private void checkExpBioStatReviewField(String studyId, StudyIdDao studyIdDao,
      Map<String, String> codeLst_studyIdPkMap, ArrayList<Object> customValidationList) {
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.REQUEST_EXPEDITED_BIOSTAT_REVIEW.getCodelst_subtyp(),
        customValidationList,
        SubmissionErrorMsg.REQUEST_EXPEDITED_BIOSTAT_REVIEW_MISSING.getResultText(),
        SubmissionErrorMsg.REQUEST_EXPEDITED_BIOSTAT_REVIEW_MISSING.getTestText(),
        codeLst_studyIdPkMap, true);
    
  }

  private void checkSafetyAdverseEventFields(String studyId, StudyIdDao studyIdDao,
      Map<String, String> codeLst_studyIdPkMap, ArrayList<Object> customValidationList) {
   String advEvent =  checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.WHAT_CRITERIA_ADVERSE_EVENTS.getCodelst_subtyp(),
        customValidationList,
        SubmissionErrorMsg.WHAT_CRITERIA_ADVERSE_EVENTS_MISSING.getResultText(),
        SubmissionErrorMsg.WHAT_CRITERIA_ADVERSE_EVENTS_MISSING.getTestText(),
        codeLst_studyIdPkMap, true);
    
   if (advEvent != null && advEvent.toUpperCase().equals(OTHER)){
     checkMoreStudyDetailsFields(studyId, studyIdDao,
         MoreStudyDetailsFormFields.WHAT_CRITERIA_ADVERSE_EVENTS_OTHER.getCodelst_subtyp(),
         customValidationList,
         SubmissionErrorMsg.WHAT_CRITERIA_ADVERSE_EVENTS_OTHER_MISSING.getResultText(),
         SubmissionErrorMsg.WHAT_CRITERIA_ADVERSE_EVENTS_OTHER_MISSING.getTestText(),
         codeLst_studyIdPkMap, true);
     }
   
   checkMoreStudyDetailsFields(studyId, studyIdDao,
       MoreStudyDetailsFormFields.PROVIDE_LIST_ANTICIPATED_AE.getCodelst_subtyp(),
       customValidationList,
       SubmissionErrorMsg.PROVIDE_LIST_ANTICIPATED_AE_MISSING.getResultText(),
       SubmissionErrorMsg.PROVIDE_LIST_ANTICIPATED_AE_MISSING.getTestText(),
       codeLst_studyIdPkMap, true);
  }

  private void checkRoleMdaccPrincipalInvestigtorField(StudyJB studyJB,
      ArrayList<Object> customValidationList, String studyId, StudyIdDao studyIdDao,
      Map<String, String> codeLst_studyIdPkMap) {
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.STUDY_ROLE_MDACC_PI.getCodelst_subtyp(),
        customValidationList,
        SubmissionErrorMsg.STUDY_ROLE_MDACC_PI_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_ROLE_MDACC_PI_MISSING.getTestText(),
        codeLst_studyIdPkMap, true);
    
  }

  /*private void checkCriteriaReplacement(String studyId, StudyIdDao studyIdDao,
      Map<String, String> codeLst_studyIdPkMap, ArrayList<Object> customValidationList) {
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.CRITERIA_FOR_REPLACEMENT.getCodelst_subtyp(),
        customValidationList,
        SubmissionErrorMsg.CRITERIA_FOR_REPLACEMENT_MISSING.getResultText(),
        SubmissionErrorMsg.CRITERIA_FOR_REPLACEMENT_MISSING.getTestText(),
        codeLst_studyIdPkMap, true);
  }*/

  private void checkInvolvesOrgInfHumans(String studyId, StudyIdDao studyIdDao,
      Map<String, String> codeLst_studyIdPkMap, ArrayList<Object> customValidationList) {
    
   String involves_Inf_Org =  checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.STUDY_INVOLVE_ORGANISMS_INFECTIOUS_TO_HUMANS.getCodelst_subtyp(),
        customValidationList,
        SubmissionErrorMsg.STUDY_INVOLVE_ORGANISMS_INFECTIOUS_TO_HUMANS_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_INVOLVE_ORGANISMS_INFECTIOUS_TO_HUMANS_MISSING.getTestText(),
        codeLst_studyIdPkMap, true);
   
    if (involves_Inf_Org != null && involves_Inf_Org.toUpperCase().equals(OPTION_YES)) {
      checkMoreStudyDetailsFields(studyId, studyIdDao,
          MoreStudyDetailsFormFields.STUDY_INVOLVE_ORGANISMS_INFECTIOUS_TO_HUMANS_COMMENT
              .getCodelst_subtyp(), customValidationList,
          SubmissionErrorMsg.STUDY_INVOLVE_ORGANISMS_INFECTIOUS_TO_HUMANS_COMMENT_MISSING
              .getResultText(),
          SubmissionErrorMsg.STUDY_INVOLVE_ORGANISMS_INFECTIOUS_TO_HUMANS_COMMENT_MISSING
              .getTestText(), codeLst_studyIdPkMap, true);

    }
  }

  private void checkStudyRequiresInd(String studyId, StudyIdDao studyIdDao,
      Map<String, String> codeLst_studyIdPkMap, ArrayList<Object> customValidationList) {
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.DOES_PROTOCOL_REQUIRE_IND.getCodelst_subtyp(),
        customValidationList,
        SubmissionErrorMsg.DOES_PROTOCOL_REQUIRE_IND_MISSING.getResultText(),
        SubmissionErrorMsg.DOES_PROTOCOL_REQUIRE_IND_MISSING.getTestText(),
        codeLst_studyIdPkMap, true);
    
  }
    
  private void checkSpecificSitesField(StudyJB studyJB, ArrayList<Object> customValidationList,
      String studyId, StudyIdDao studyIdDao, Map<String, String> codeLst_studyIdPkMap) {
    checkSpecificSite(studyJB, customValidationList);

    checkSpecificSite2(studyJB, customValidationList);

    checkDiseaseSite(studyJB, customValidationList);

    checkTreatmentArms(customValidationList, studyId, studyIdDao, codeLst_studyIdPkMap);
  }

  private void checkTreatmentArms(ArrayList<Object> customValidationList, String studyId,
      StudyIdDao studyIdDao, Map<String, String> codeLst_studyIdPkMap) {
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.TREATMENT_ARMS.getCodelst_subtyp(), customValidationList,
        SubmissionErrorMsg.TREATMENT_ARMS_MISSING.getResultText(),
        SubmissionErrorMsg.TREATMENT_ARMS_MISSING.getTestText(), codeLst_studyIdPkMap, true);
  }

  private void checkDiseaseSite(StudyJB studyJB, ArrayList<Object> customValidationList) {
    checkStandardFields(studyJB, customValidationList, studyJB.getDisSite(),
        SubmissionErrorMsg.STUDY_DISEASE_SITE_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_DISEASE_SITE_MISSING.getTestText());
  }

  private void checkSpecificSite(StudyJB studyJB, ArrayList<Object> customValidationList) {
    checkStandardFields(studyJB, customValidationList, studyJB.getStudyICDCode1(),
        SubmissionErrorMsg.STUDY_SPECIFIC_SITE_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_SPECIFIC_SITE_MISSING.getTestText());
  }

  private void checkSpecificSite2(StudyJB studyJB, ArrayList<Object> customValidationList) {
    checkStandardFields(studyJB, customValidationList, studyJB.getStudyICDCode2(),
        SubmissionErrorMsg.STUDY_SPECIFIC_SITE2_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_SPECIFIC_SITE2_MISSING.getTestText());
  }

  private void checkSecondaryDepartmentField(String studyId, StudyIdDao studyIdDao,
      Map<String, String> codeLst_studyIdPkMap, ArrayList<Object> customValidationList) {
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.SECONDARY_DEPARTMENT.getCodelst_subtyp(),
        customValidationList,
        SubmissionErrorMsg.SECONDARY_DEPARTMENT_MISSING.getResultText(),
        SubmissionErrorMsg.SECONDARY_DEPARTMENT_MISSING.getTestText(),
        codeLst_studyIdPkMap, true);
    
  }

  private void checkSponsorProtocolNumberField(StudyJB studyJB,
      ArrayList<Object> customValidationList) {
    checkStandardFields(studyJB, customValidationList, studyJB.getStudySponsorIdInfo(),
        SubmissionErrorMsg.SPONSOR_PROTOCOL_NUMBER_MISSING.getResultText(),
        SubmissionErrorMsg.SPONSOR_PROTOCOL_NUMBER_MISSING.getTestText());
    
  }

  private void checkStudyBackgroundFields(StudyJB studyJB, ArrayList<Object> customValidationList,
      String studyId, StudyIdDao studyIdDao, Map<String, String> studySecFieldsMap) {
    checkMoreStudyDetailsTextAreaFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.STUDY_RATIONALE.getfield_label(),
        customValidationList,
        SubmissionErrorMsg.STUDY_RATIONALE_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_RATIONALE_MISSING.getTestText(),
        studySecFieldsMap, true);

    checkMoreStudyDetailsTextAreaFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.CLINICAL_DATA_TO_DATE.getfield_label(),
        customValidationList,
        SubmissionErrorMsg.CLINICAL_DATA_TO_DATE_MISSING.getResultText(),
        SubmissionErrorMsg.CLINICAL_DATA_TO_DATE_MISSING.getTestText(),
        studySecFieldsMap, true);

  }

  private void checkStudyScopeFields(StudyJB studyJB, ArrayList<Object> customValidationList,
      String studyId, StudyIdDao studyIdDao, Map<String, String> codeLst_studyIdPkMap) {
    if (checkStandardFields(studyJB, customValidationList, studyJB.getStudyScope(),
        SubmissionErrorMsg.STUDY_SCOPE_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_SCOPE_MISSING.getTestText())) {

      Code scopeCode = codeCache.getCodeSubTypeByPK(SCOPE_TYPE, studyJB.getStudyScope(), 0);
      String scopeSubtype = scopeCode.getCode();
      if (SCOPE_ONLY_MDACC.equals(scopeSubtype)) {
        checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.ESTIMATED_ENROLLED_MDACC.getCodelst_subtyp(),
            customValidationList,
            SubmissionErrorMsg.STUDY_ESTIMATED_ENROLLED_MDACC_MISSING.getResultText(),
            SubmissionErrorMsg.STUDY_ESTIMATED_ENROLLED_MDACC_MISSING.getTestText(),
            codeLst_studyIdPkMap, true);
        checkStandardFields(studyJB, customValidationList, studyJB.getStudyNSamplSize(),
            SubmissionErrorMsg.STUDY_TOTAL_ENROLLED_MDACC_MISSING.getResultText(),
            SubmissionErrorMsg.STUDY_TOTAL_ENROLLED_MDACC_MISSING.getTestText());
      }

      else if (SCOPE_MULTI_MDACC_LEAD.equals(scopeSubtype) || 
    		  SCOPE_MULTI_CENTER.equals(scopeSubtype)) {

        checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.ESTIMATED_ENROLLED_MDACC.getCodelst_subtyp(),
            customValidationList,
            SubmissionErrorMsg.STUDY_ESTIMATED_ENROLLED_MDACC_MISSING.getResultText(),
            SubmissionErrorMsg.STUDY_ESTIMATED_ENROLLED_MDACC_MISSING.getTestText(),
            codeLst_studyIdPkMap, true);
        checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.TOTAL_ENROLLED_ALL_SITES.getCodelst_subtyp(),
            customValidationList,
            SubmissionErrorMsg.STUDY_TOTAL_ENROLLED_ALL_SITES_MISSING.getResultText(),
            SubmissionErrorMsg.STUDY_TOTAL_ENROLLED_ALL_SITES_MISSING.getTestText(),
            codeLst_studyIdPkMap, true);

        checkStandardFields(studyJB, customValidationList, studyJB.getStudyNSamplSize(),
            SubmissionErrorMsg.STUDY_TOTAL_ENROLLED_MDACC_MISSING.getResultText(),
            SubmissionErrorMsg.STUDY_TOTAL_ENROLLED_MDACC_MISSING.getTestText());

      }

      else if (SCOPE_ONLY_OTHER.equals(scopeSubtype)) {

        checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.TOTAL_ENROLLED_ALL_SITES.getCodelst_subtyp(),
            customValidationList,
            SubmissionErrorMsg.STUDY_TOTAL_ENROLLED_ALL_SITES_MISSING.getResultText(),
            SubmissionErrorMsg.STUDY_TOTAL_ENROLLED_ALL_SITES_MISSING.getTestText(),
            codeLst_studyIdPkMap, true);
      }
    }

  }

  private void checkStudyIncludesField(String studyId, StudyIdDao studyIdDao,
      Map<String, String> finalMap, ArrayList<Object> customValidationList, boolean mdaccTemplate, boolean industryTemplate) {
 
    String studyIncludes =
        checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.STUDY_INCLUDES.getCodelst_subtyp(), customValidationList,
            SubmissionErrorMsg.STUDY_INCLUDES_MISSING.getResultText(),
            SubmissionErrorMsg.STUDY_INCLUDES_MISSING.getTestText(), finalMap, true);
    if (studyIncludes != null) {

      if (studyIncludes.contains(STUDY_INCLUDES_DRUGS)) {
        checkForStudyDrugFields(studyId, studyIdDao, finalMap, customValidationList, mdaccTemplate, industryTemplate);
      }
      if (studyIncludes.contains(STUDY_INCLUDES_DEVICES)) {
        checkForStudyDeviceFields(studyId, studyIdDao, finalMap, customValidationList);
      }
    }
  }


  private void checkStudyDepartmentField(StudyJB studyJB, ArrayList<Object> customValidationList) {
    checkStandardFields(studyJB, customValidationList, studyJB.getStudyTArea(),
        SubmissionErrorMsg.STUDY_DEPARTMENT_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_DEPARTMENT_MISSING.getTestText());

  }

  private String checkStudyProtocolTemplateField(String studyId, StudyIdDao studyIdDao,
      Map<String, String> finalMap, ArrayList<Object> customValidationList) {
    return checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.PROTOCOL_TEMPLATE.getCodelst_subtyp(), customValidationList,
        SubmissionErrorMsg.STUDY_TEMPLATE_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_TEMPLATE_MISSING.getTestText(), finalMap, true);

  }

  private void checkStudyDesignSectionField(StudyJB studyJB, ArrayList<Object> customValidationList,
      String studyId, StudyIdDao studyIdDao, Map<String, String> finalMap, boolean checkBlindingNotes) {
    checkStandardFields(studyJB, customValidationList, studyJB.getStudyPhase(),
        SubmissionErrorMsg.STUDY_PHASE_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_PHASE_MISSING.getTestText());
    // study randomization
    if (checkStandardFields(studyJB, customValidationList, studyJB.getStudyRandom(),
        SubmissionErrorMsg.STUDY_RANDOMIZATION_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_RANDOMIZATION_MISSING.getTestText())) {

      Code randomizeCode = codeCache.getCodeSubTypeByPK(RANDOMIZATION_TYPE, studyJB.getStudyRandom(), 0);
      if (randomizeCode != null && !NONE_STR.equals(randomizeCode.getCode())) {
        // rndmnTrtGrp
        checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.RANDOMIZATION_NOTES.getCodelst_subtyp(),
            customValidationList,
            SubmissionErrorMsg.STUDY_RANDOMIZATION_NOTES_MISSING.getResultText(),
            SubmissionErrorMsg.STUDY_RANDOMIZATION_NOTES_MISSING.getTestText(), finalMap, true);
        // checking for study blinding
        checkBlindingField(studyJB, customValidationList,studyId, studyIdDao, finalMap, checkBlindingNotes);
      }
    }

  }

  private void checkBlindingField(StudyJB studyJB, ArrayList<Object> customValidationList,
      String studyId, StudyIdDao studyIdDao, Map<String, String> finalMap, boolean checkBlindingNotes) {
    checkStandardFields(studyJB, customValidationList, studyJB.getStudyBlinding(),
        SubmissionErrorMsg.STUDY_BLINDING_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_BLINDING_MISSING.getTestText());

    // for industry specific protocol check for blinding note
    if (checkBlindingNotes) {
      checkMoreStudyDetailsFields(studyId, studyIdDao,
          MoreStudyDetailsFormFields.STUDY_BLINDING_NOTES.getCodelst_subtyp(),
          customValidationList, SubmissionErrorMsg.STUDY_BLINDING_NOTES_MISSING.getResultText(),
          SubmissionErrorMsg.STUDY_BLINDING_NOTES_MISSING.getTestText(), finalMap, true);
    }
  }
  
  

  private void checkStudyDivisionField(StudyJB studyJB, ArrayList<Object> customValidationList) {
    checkStandardFields(studyJB, customValidationList, studyJB.getStudyDivision(),
        SubmissionErrorMsg.STUDY_DIVISION_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_DIVISION_MISSING.getTestText());
  }

  private void checkStudyContactField(StudyJB studyJB, ArrayList<Object> customValidationList) {
    checkStandardFields(studyJB, customValidationList, studyJB.getStudyCoordinator(),
        SubmissionErrorMsg.STUDY_CONTACT_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_CONTACT_MISSING.getTestText());

  }

  private void checkPrincipalInvestigtorField(StudyJB studyJB,
      ArrayList<Object> customValidationList) {
    checkStandardFields(studyJB, customValidationList, studyJB.getStudyPrimInv(),
        SubmissionErrorMsg.PRINCIPAL_INV_MISSING.getResultText(),
        SubmissionErrorMsg.PRINCIPAL_INV_MISSING.getTestText());
  }


  private void checkForStudyInd_Ide_Fields(String studyId, ArrayList<Object> customValidationList) {
    StudyINDIDEDAO studyIndIdeDAO = new StudyINDIDEDAO();
    studyIndIdeDAO.getStudyIndIdeInfo(Integer.parseInt(studyId));


    if (studyIndIdeDAO.getNumberIndIdeList() != null
        && !studyIndIdeDAO.getNumberIndIdeList().isEmpty()) {

      if ((studyIndIdeDAO.getTypeIndIdeList() == null)
          || studyIndIdeDAO.getTypeIndIdeList().isEmpty()) {


      }

      if ((studyIndIdeDAO.getGrantorList() == null) || studyIndIdeDAO.getGrantorList().isEmpty()) {


      }

      if ((studyIndIdeDAO.getHolderList() == null) || studyIndIdeDAO.getHolderList().isEmpty()) {


      }


      if ((studyIndIdeDAO.getProgramCodeList() == null)
          || studyIndIdeDAO.getProgramCodeList().isEmpty()) {


      }

      if ((studyIndIdeDAO.getExpandedAccessList() == null)
          || studyIndIdeDAO.getExpandedAccessList().isEmpty()) {


      } else {

        // TODO : expander access type
        if ((studyIndIdeDAO.getAccessTypeList() == null)
            || studyIndIdeDAO.getAccessTypeList().isEmpty()) {


        }

      }

    }

  }


  private void checkForStudyObjectivesFields(String studyId, StudyIdDao studyIdDao,
      Map<String, String> finalMap, ArrayList<Object> customValidationList, boolean displayError) {

    otherObjective = checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.SELECT_OTHER_OBJECTIVES.getCodelst_subtyp(),
            customValidationList,
            SubmissionErrorMsg.SELECT_OTHER_OBJECTIVES_MISSING.getResultText(),
            SubmissionErrorMsg.SELECT_OTHER_OBJECTIVES_MISSING.getTestText(), finalMap, displayError);

    if (otherObjective != null && !otherObjective.isEmpty()) {
      if (otherObjective.contains(STUDY_OBJECTIVES_SECONDARY)) {

        checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.STUDY_SEC_OBJ_TEXT.getCodelst_subtyp(),
            customValidationList, SubmissionErrorMsg.STUDY_SEC_OBJ_TEXT_MISSING.getResultText(),
            SubmissionErrorMsg.STUDY_SEC_OBJ_TEXT_MISSING.getTestText(), finalMap, true);
      }

      if (otherObjective.contains(STUDY_OBJECTIVES_EXPLORATORY)) {
        checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.STUDY_EXP_OBJ_TEXT.getCodelst_subtyp(),
            customValidationList, SubmissionErrorMsg.STUDY_EXP_OBJ_TEXT_MISSING.getResultText(),
            SubmissionErrorMsg.STUDY_EXP_OBJ_TEXT_MISSING.getTestText(), finalMap, true);
      }

      if (otherObjective.contains(STUDY_OBJECTIVES_BIOMARKER)) {
        isBioMarkerObjSelected = true;
        checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.STUDY_BIO_OBJ_TEXT.getCodelst_subtyp(),
            customValidationList, SubmissionErrorMsg.STUDY_BIO_OBJ_TEXT_MISSING.getResultText(),
            SubmissionErrorMsg.STUDY_BIO_OBJ_TEXT_MISSING.getTestText(), finalMap, true);
      }
    }
  }

  private void checkForBiomarkerCorrelativeFields(String studyId, StudyIdDao studyIdDao,
      Map<String, String> finalMap, ArrayList<Object> customValidationList) {

    String knownBioMarkers = null;
    if (isBioMarkerObjSelected) {
      String areYouMeasuringBioMarkers = checkAreYouMeasuringBioMarkers(studyId, studyIdDao, finalMap, customValidationList, true);
      if (areYouMeasuringBioMarkers != null && areYouMeasuringBioMarkers.toUpperCase().equals(OPTION_YES)) {

        knownBioMarkers = checkMoreStudyDetailsFields(studyId, studyIdDao,
                MoreStudyDetailsFormFields.ARE_THEY_KNOWN_BIOMARKERS.getCodelst_subtyp(),
                customValidationList,
                SubmissionErrorMsg.ARE_THEY_KNOWN_BIOMARKERS_MISSING.getResultText(),
                SubmissionErrorMsg.ARE_THEY_KNOWN_BIOMARKERS_MISSING.getTestText(), finalMap, true);

        checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.WHAT_ARE_YOU_MEASURING.getCodelst_subtyp(),
            customValidationList,
            SubmissionErrorMsg.WHAT_ARE_YOU_MEASURING_MISSING.getResultText(),
            SubmissionErrorMsg.WHAT_ARE_YOU_MEASURING_MISSING.getTestText(), finalMap, true);

        checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.TIMEPOINTS.getCodelst_subtyp(), customValidationList,
            SubmissionErrorMsg.TIMEPOINTS_MISSING.getResultText(),
            SubmissionErrorMsg.TIMEPOINTS_MISSING.getTestText(), finalMap, true);

        checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.METHODOLOGY.getCodelst_subtyp(), customValidationList,
            SubmissionErrorMsg.METHODOLOGY_MISSING.getResultText(),
            SubmissionErrorMsg.METHODOLOGY_MISSING.getTestText(), finalMap, true);
      }

      if (knownBioMarkers != null && !knownBioMarkers.isEmpty()) {
        if (knownBioMarkers.toUpperCase().equals(OPTION_YES)) {
          checkMoreStudyDetailsFields(studyId, studyIdDao,
              MoreStudyDetailsFormFields.BIOMARKER_TYPE.getCodelst_subtyp(), customValidationList,
              SubmissionErrorMsg.BIOMARKER_TYPE_MISSING.getResultText(),
              SubmissionErrorMsg.BIOMARKER_TYPE_MISSING.getTestText(), finalMap, true);

          checkMoreStudyDetailsFields(studyId, studyIdDao,
              MoreStudyDetailsFormFields.TEST_NAME.getCodelst_subtyp(), customValidationList,
              SubmissionErrorMsg.TEST_NAME_MISSING.getResultText(),
              SubmissionErrorMsg.TEST_NAME_MISSING.getTestText(), finalMap, true);
        }
        if (knownBioMarkers.toUpperCase().equals(OPTION_NO)) {
          checkMoreStudyDetailsFields(studyId, studyIdDao,
              MoreStudyDetailsFormFields.BIOMARKER_NAME.getCodelst_subtyp(), customValidationList,
              SubmissionErrorMsg.BIOMARKER_NAME_MISSING.getResultText(),
              SubmissionErrorMsg.BIOMARKER_NAME_MISSING.getTestText(), finalMap, true);

          checkMoreStudyDetailsFields(studyId, studyIdDao,
              MoreStudyDetailsFormFields.BIOMARKER_LEVEL.getCodelst_subtyp(), customValidationList,
              SubmissionErrorMsg.BIOMARKER_LEVEL_MISSING.getResultText(),
              SubmissionErrorMsg.BIOMARKER_LEVEL_MISSING.getTestText(), finalMap, true);
        }

        if (knownBioMarkers.toUpperCase().equals(OTHER)) {
          checkMoreStudyDetailsFields(studyId, studyIdDao,
              MoreStudyDetailsFormFields.SPECIFY_OTHER_BIOMARKER.getCodelst_subtyp(),
              customValidationList,
              SubmissionErrorMsg.SPECIFY_OTHER_BIOMARKER_MISSING.getResultText(),
              SubmissionErrorMsg.SPECIFY_OTHER_BIOMARKER_MISSING.getTestText(), finalMap, true);

        }
      }
    }
  }

  private String checkAreYouMeasuringBioMarkers(String studyId, StudyIdDao studyIdDao,
      Map<String, String> finalMap, ArrayList<Object> customValidationList, boolean displayError) {
    return checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.ARE_YOU_MEASURING_BIOMARKERS.getCodelst_subtyp(),
            customValidationList,
            SubmissionErrorMsg.ARE_YOU_MEASURING_BIOMARKERS_MISSING.getResultText(),
            SubmissionErrorMsg.ARE_YOU_MEASURING_BIOMARKERS_MISSING.getTestText(), finalMap,
            displayError);
  }

  private void checkForStatisticalConsiderationsFields(String studyId, StudyIdDao studyIdDao,
      Map<String, String> finalMap, ArrayList<Object> customValidationList, boolean mdaccTemplate, boolean industryTemplate) {


    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.DID_MCDACC_BIOSTAT_PARTICIPATE.getCodelst_subtyp(),
        customValidationList,
        SubmissionErrorMsg.DID_MCDACC_BIOSTAT_PARTICIPATE_MISSING.getResultText(),
        SubmissionErrorMsg.DID_MCDACC_BIOSTAT_PARTICIPATE_MISSING.getTestText(), finalMap, true);
   
   
   
    boolean displayError  =  mdaccTemplate ;
    String dataSafetyMonitoring = checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.DATA_SAFETY_MONITORING_BOARD.getCodelst_subtyp(),
            customValidationList,
            SubmissionErrorMsg.DATA_SAFETY_MONITORING_BOARD_MISSING.getResultText(),
            SubmissionErrorMsg.DATA_SAFETY_MONITORING_BOARD_MISSING.getTestText(), finalMap, displayError);

    if (dataSafetyMonitoring != null && !dataSafetyMonitoring.isEmpty()) {
      if (!dataSafetyMonitoring.equals(NA_DSMB)) {
        checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.DSMB_COMPOSITION.getCodelst_subtyp(), customValidationList,
            SubmissionErrorMsg.DSMB_COMPOSITION_MISSING.getResultText(),
            SubmissionErrorMsg.DSMB_COMPOSITION_MISSING.getTestText(), finalMap, true);
      }
    }
    
    if (mdaccTemplate){
      checkMoreStudyDetailsFields(studyId, studyIdDao,
          MoreStudyDetailsFormFields.PRIMARY_END_POINT.getCodelst_subtyp(), customValidationList,
          SubmissionErrorMsg.PRIMARY_END_POINT_MISSING.getResultText(),
          SubmissionErrorMsg.PRIMARY_END_POINT_MISSING.getTestText(), finalMap, true);

    String interimAnalysis = checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.IS_PLANNED_INTERIM_ANALYSIS.getCodelst_subtyp(),
            customValidationList,
            SubmissionErrorMsg.IS_PLANNED_INTERIM_ANALYSIS_MISSING.getResultText(),
            SubmissionErrorMsg.IS_PLANNED_INTERIM_ANALYSIS_MISSING.getTestText(), finalMap, true);

    if (interimAnalysis != null && !interimAnalysis.isEmpty()) {

      if (interimAnalysis.toUpperCase().equals(OPTION_YES)) {
        checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.SUMMARY_OF_PLANNED_INTERIM_ANALYSIS.getCodelst_subtyp(),
            customValidationList,
            SubmissionErrorMsg.SUMMARY_OF_PLANNED_INTERIM_ANALYSIS_MISSING.getResultText(),
            SubmissionErrorMsg.SUMMARY_OF_PLANNED_INTERIM_ANALYSIS_MISSING.getTestText(), finalMap,
            true);
      }

      if (interimAnalysis.toUpperCase().equals(OPTION_NO)) {
        checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.RATIONALE_FOR_NO_INTERIM_ANALYSIS.getCodelst_subtyp(),
            customValidationList,
            SubmissionErrorMsg.RATIONALE_FOR_NO_INTERIM_ANALYSIS_MISSING.getResultText(),
            SubmissionErrorMsg.RATIONALE_FOR_NO_INTERIM_ANALYSIS_MISSING.getTestText(), finalMap,
            true);
      }
    }

    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.STATISTICAL_ANALYSIS_PLAN.getCodelst_subtyp(),
        customValidationList, SubmissionErrorMsg.STATISTICAL_ANALYSIS_PLAN_MISSING.getResultText(),
        SubmissionErrorMsg.STATISTICAL_ANALYSIS_PLAN_MISSING.getTestText(), finalMap, true);
    
    }
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.BAYSEAN_COMPONENT_INCLUDED.getCodelst_subtyp(),
        customValidationList,
        SubmissionErrorMsg.BAYSEAN_COMPONENT_INCLUDED_MISSING.getResultText(),
        SubmissionErrorMsg.BAYSEAN_COMPONENT_INCLUDED_MISSING.getTestText(), finalMap, true);
  }

  private void checkForNominatedReviewerFields(String studyId, StudyIdDao studyIdDao,
      Map<String, String> finalMap, ArrayList<Object> customValidationList) {
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.NOMINATED_REVIEWER.getCodelst_subtyp(), customValidationList,
        SubmissionErrorMsg.NOMINATED_REVIEWER_MISSING.getResultText(),
        SubmissionErrorMsg.NOMINATED_REVIEWER_MISSING.getTestText(), finalMap, true);

  }

  private void checkForReferencesFields(String studyId, StudyIdDao studyIdDao,
      Map<String, String> finalMap, ArrayList<Object> customValidationList) {
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.REFERENCES.getCodelst_subtyp(), customValidationList,
        SubmissionErrorMsg.REFERENCES_MISSING.getResultText(),
        SubmissionErrorMsg.REFERENCES_MISSING.getTestText(), finalMap, true);

  }

  private void checkForFinancialSupportFields(String studyId, StudyIdDao studyIdDao,
      Map<String, String> finalMap, ArrayList<Object> customValidationList) {
    
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.FINANCIAL_SUPPORT_SPONSOR_NAME.getCodelst_subtyp(),
        customValidationList,
        SubmissionErrorMsg.FINANCIAL_SUPPORT_SPONSOR_NAME_MISSING.getResultText(),
        SubmissionErrorMsg.FINANCIAL_SUPPORT_SPONSOR_NAME_MISSING.getTestText(), finalMap, true);
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.FINANCIAL_SUPPORT_TYPE_OF_FUNDING.getCodelst_subtyp(),
        customValidationList,
        SubmissionErrorMsg.FINANCIAL_SUPPORT_TYPE_OF_FUNDING_MISSING.getResultText(),
        SubmissionErrorMsg.FINANCIAL_SUPPORT_TYPE_OF_FUNDING_MISSING.getTestText(), finalMap, true);
      
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.FINANCIAL_SUPPORT_CANCER_SUPPORT_GRANT.getCodelst_subtyp(),
        customValidationList,
        SubmissionErrorMsg.FINANCIAL_SUPPORT_CANCER_SUPPORT_GRANT_MISSING.getResultText(),
        SubmissionErrorMsg.FINANCIAL_SUPPORT_CANCER_SUPPORT_GRANT_MISSING.getTestText(), finalMap,
        true);
    
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.DEPARTMENT_CHAIR.getCodelst_subtyp(),
        customValidationList,
        SubmissionErrorMsg.DEPARTMENT_CHAIR_MISSING.getResultText(),
        SubmissionErrorMsg.DEPARTMENT_CHAIR_MISSING.getTestText(), finalMap,
        true);
  }

  private void checkForDataSafetyMonitoringPlanFields(String studyId, StudyIdDao studyIdDao,
      Map<String, String> finalMap, ArrayList<Object> customValidationList) {
    checkMoreStudyDetailsTextAreaFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.DATA_SAFETY_MONITORING_PLAN.getfield_label(),
        customValidationList,
        SubmissionErrorMsg.DATA_SAFETY_MONITORING_PLAN_MISSING.getResultText(),
        SubmissionErrorMsg.DATA_SAFETY_MONITORING_PLAN_MISSING.getTestText(), finalMap, true);

  }

  private void checkForReportingToSponsorField(String studyId, StudyIdDao studyIdDao,
      Map<String, String> finalMap, ArrayList<Object> customValidationList) {
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.REPORTING_TO_SPONSOR.getCodelst_subtyp(), customValidationList,
        SubmissionErrorMsg.REPORTING_TO_SPONSOR_MISSING.getResultText(),
        SubmissionErrorMsg.REPORTING_TO_SPONSOR_MISSING.getTestText(), finalMap, true);

  }

  private void checkForStudyEvaluationProceduresFields(String studyId, StudyIdDao studyIdDao,
      Map<String, String> studySecFieldsMap,ArrayList<Object> customValidationList) {
    checkMoreStudyDetailsTextAreaFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.SCREENING_BASELINE.getfield_label(), customValidationList,
        SubmissionErrorMsg.SCREENING_BASELINE_MISSING.getResultText(),
        SubmissionErrorMsg.SCREENING_BASELINE_MISSING.getTestText(), studySecFieldsMap, true);
    checkMoreStudyDetailsTextAreaFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.ON_STUDY_TREATMENT_EVALUATIONS.getfield_label(),
        customValidationList,
        SubmissionErrorMsg.ON_STUDY_TREATMENT_EVALUATIONS_MISSING.getResultText(),
        SubmissionErrorMsg.ON_STUDY_TREATMENT_EVALUATIONS_MISSING.getTestText(), studySecFieldsMap, true);
    checkMoreStudyDetailsTextAreaFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.END_OF_THERAPY.getfield_label(), customValidationList,
        SubmissionErrorMsg.END_OF_THERAPY_MISSING.getResultText(),
        SubmissionErrorMsg.END_OF_THERAPY_MISSING.getTestText(), studySecFieldsMap, true);
    checkMoreStudyDetailsTextAreaFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.FOLLOW_UP.getfield_label(), customValidationList,
        SubmissionErrorMsg.FOLLOW_UP_MISSING.getResultText(),
        SubmissionErrorMsg.FOLLOW_UP_MISSING.getTestText(), studySecFieldsMap, true);
    /* This field is now converted into a hyperlink navigating to Attachments tab */
    /*checkMoreStudyDetailsTextAreaFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.STUDY_CALENDER.getfield_label(), customValidationList,
        SubmissionErrorMsg.STUDY_CALENDER_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_CALENDER_MISSING.getTestText(), studySecFieldsMap, true);*/
  }

  private void checkForWithdrawlOfParticipantFields(String studyId, StudyIdDao studyIdDao,
      Map<String, String> finalMap, ArrayList<Object> customValidationList) {
    String criteriaForRemoval = checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.OFF_STUDY_CRITERIA.getCodelst_subtyp(), customValidationList,
        SubmissionErrorMsg.OFF_STUDY_CRITERIA_MISSING.getResultText(),
        SubmissionErrorMsg.OFF_STUDY_CRITERIA_MISSING.getTestText(), finalMap, true);

   /* String criteriaForRemoval = checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.CRITERIA_FOR_REMOVAL.getCodelst_subtyp(),
            customValidationList, SubmissionErrorMsg.CRITERIA_FOR_REMOVAL_MISSING.getResultText(),
            SubmissionErrorMsg.CRITERIA_FOR_REMOVAL_MISSING.getTestText(), finalMap, false);*/

    if (criteriaForRemoval != null && criteriaForRemoval.equals(CRITERIA_REMOVAL_PARTICIPANTS_OTHER)) {
      checkMoreStudyDetailsFields(studyId, studyIdDao,
          MoreStudyDetailsFormFields.SPECIFY_OTHER_CRITERIA.getCodelst_subtyp(),
          customValidationList, SubmissionErrorMsg.SPECIFY_OTHER_CRITERIA_MISSING.getResultText(),
          SubmissionErrorMsg.SPECIFY_OTHER_CRITERIA_MISSING.getTestText(), finalMap, true);
    }
  }

  private void checkForStudyDeviceFields(String studyId, StudyIdDao studyIdDao,
      Map<String, String> finalMap, ArrayList<Object> customValidationList) {
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.STUDY_DEVICE1.getCodelst_subtyp(), customValidationList,
        SubmissionErrorMsg.STUDY_DEVICE1_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_DEVICE1_MISSING.getTestText(), finalMap, true);

    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.STUDY_DEVICE_MANUFACTURER1.getCodelst_subtyp(),
        customValidationList,
        SubmissionErrorMsg.STUDY_DEVICE_MANUFACTURER1_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_DEVICE_MANUFACTURER1_MISSING.getTestText(), finalMap, true);

  }

  private void checkForStudyDrugFields(String studyId, StudyIdDao studyIdDao,
      Map<String, String> finalMap, ArrayList<Object> customValidationList, boolean mdaccTemplate, boolean industryTemplate) {
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.DRUG_AGENT1.getCodelst_subtyp(), customValidationList,
        SubmissionErrorMsg.STUDY_DRUG_AGENT_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_DRUG_AGENT_MISSING.getTestText(), finalMap, true);

    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.DRUG_AGENT_DOSE.getCodelst_subtyp(), customValidationList,
        SubmissionErrorMsg.STUDY_DRUG_AGENT_DOSE_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_DRUG_AGENT_DOSE_MISSING.getTestText(), finalMap, true);

    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.DRUG_DOSE_RATIONALE.getCodelst_subtyp(), customValidationList,
        SubmissionErrorMsg.STUDY_DRUG_AGENT_DOSE_RATNL_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_DRUG_AGENT_DOSE_RATNL_MISSING.getTestText(), finalMap, true);

    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.DRUG_ROUTE.getCodelst_subtyp(), customValidationList,
        SubmissionErrorMsg.STUDY_DRUG_AGENT_ROUTE_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_DRUG_AGENT_ROUTE_MISSING.getTestText(), finalMap, true);
    
    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.DRUG_MANUFACTURER.getCodelst_subtyp(), customValidationList,
        SubmissionErrorMsg.STUDY_DRUG_MANUFACTURER_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_DRUG_MANUFACTURER_MISSING.getTestText(), finalMap, true);
    
    if (industryTemplate) {
    	checkInvolvesOrgInfHumans(studyId, studyIdDao, finalMap, customValidationList);
    }

    checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.TREATMENT_DURATION.getCodelst_subtyp(), customValidationList,
        SubmissionErrorMsg.STUDY_TREATMENT_DURATION_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_TREATMENT_DURATION_MISSING.getTestText(), finalMap, true);

    String study_Involves_Human_Animal_Tissue = checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.STUDY__INVOLVE_HUMAN_ANIMAL_TISSUE.getCodelst_subtyp(),
            customValidationList,
            SubmissionErrorMsg.STUDY_INOLVE_HUMAN_ANIMAL_TISSUE_MISSING.getResultText(),
            SubmissionErrorMsg.STUDY_INOLVE_HUMAN_ANIMAL_TISSUE_MISSING.getTestText(), finalMap,
            true);

      if (study_Involves_Human_Animal_Tissue != null
          && study_Involves_Human_Animal_Tissue.toUpperCase().equals(OPTION_YES)) {
        checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.USING_TISSEL_OR_EVICEL.getCodelst_subtyp(),
            customValidationList,
            SubmissionErrorMsg.USING_TISSEL_OR_EVICEL_MISSING.getResultText(),
            SubmissionErrorMsg.USING_TISSEL_OR_EVICEL_MISSING.getTestText(), finalMap, true);
      }

    String study_Involves_Recomb_Dna = checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.STUDY_INVOLVE_RECOMB_DNA.getCodelst_subtyp(),
            customValidationList,
            SubmissionErrorMsg.STUDY_INVOLVE_RECOMB_DNA_MISSING.getResultText(),
            SubmissionErrorMsg.STUDY_INVOLVE_RECOMB_DNA_MISSING.getTestText(), finalMap, true);

    if (study_Involves_Recomb_Dna != null) {
      if (study_Involves_Recomb_Dna.toUpperCase().equals(OPTION_YES)) {

        checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.IBC_PROTOCOL_NUMBER.getCodelst_subtyp(),
            customValidationList, SubmissionErrorMsg.IBC_PROTOCOL_NUMBER_MISSING.getResultText(),
            SubmissionErrorMsg.IBC_PROTOCOL_NUMBER_MISSING.getTestText(), finalMap, true);

        String requires_Nih_Rac_Review = checkDoesRequireNihRacReview(studyId, studyIdDao, finalMap, customValidationList, true);
        if (industryTemplate) {
          if (requires_Nih_Rac_Review != null && requires_Nih_Rac_Review.toUpperCase().equals(OPTION_YES)) {
            checkMoreStudyDetailsFields(studyId, studyIdDao,
                MoreStudyDetailsFormFields.STUDY_REQUIRE_NIH_RAC_REVIEW_COMMENT.getCodelst_subtyp(),
                customValidationList,SubmissionErrorMsg.STUDY_REQUIRE_NIH_RAC_REVIEW_COMMENT_MISSING.getResultText(),
                SubmissionErrorMsg.STUDY_REQUIRE_NIH_RAC_REVIEW_COMMENT_MISSING.getTestText(),
                finalMap, true);
          }
        }
      }
    }

      String study_Involves_Radio_Isotopes = checkMoreStudyDetailsFields(studyId, studyIdDao,
          MoreStudyDetailsFormFields.STUDY_INVOLVE_RADIO_ISOTOPE.getCodelst_subtyp(),
          customValidationList,
          SubmissionErrorMsg.STUDY_INVOLVE_RADIO_ISOTOPE_MISSING.getResultText(),
          SubmissionErrorMsg.STUDY_INVOLVE_RADIO_ISOTOPE_MISSING.getTestText(), finalMap, true);
      
      if (study_Involves_Radio_Isotopes != null && study_Involves_Radio_Isotopes.toUpperCase().equals(OPTION_YES)) {
           checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.AUTHORIZED_USER.getCodelst_subtyp(), customValidationList,
            SubmissionErrorMsg.AUTHORIZED_USER_MISSING.getResultText(),
            SubmissionErrorMsg.AUTHORIZED_USER_MISSING.getTestText(), finalMap, true);

           checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.AUTHORIZATION_NUMBER.getCodelst_subtyp(),
            customValidationList,
            SubmissionErrorMsg.AUTHORIZED_NUMBER_MISSING.getResultText(),
            SubmissionErrorMsg.AUTHORIZED_NUMBER_MISSING.getTestText(), finalMap, true);

           checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.STUDY_ADMINISTER_RADIO_ACTIVE.getCodelst_subtyp(),
            customValidationList,
            SubmissionErrorMsg.STUDY_ADMINISTER_RADIO_ACTIVE_MISSING.getResultText(),
            SubmissionErrorMsg.STUDY_ADMINISTER_RADIO_ACTIVE_MISSING.getTestText(), finalMap, true);

           checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.RADIO_ACTIVE_FDA_APPROVED.getCodelst_subtyp(),
            customValidationList,
            SubmissionErrorMsg.RADIO_ACTIVE_FDA_APPROVED_MISSING.getResultText(),
            SubmissionErrorMsg.RADIO_ACTIVE_FDA_APPROVED_MISSING.getTestText(), finalMap, true);

           checkMoreStudyDetailsFields(studyId, studyIdDao,
            MoreStudyDetailsFormFields.FACILITY_INVESTIGATIONAL_AGENT_BOUND.getCodelst_subtyp(),
            customValidationList,
            SubmissionErrorMsg.FACILITY_INVESTIGATIONAL_AGENT_BOUND_MISSING.getResultText(),
            SubmissionErrorMsg.FACILITY_INVESTIGATIONAL_AGENT_BOUND_MISSING.getTestText(),
            finalMap, true);
      }
  }

  private String checkDoesRequireNihRacReview(String studyId, StudyIdDao studyIdDao,
      Map<String, String> finalMap, ArrayList<Object> customValidationList, boolean displayError) {
   return  checkMoreStudyDetailsFields(studyId, studyIdDao,
        MoreStudyDetailsFormFields.STUDY_REQUIRE_NIH_RAC_REVIEW.getCodelst_subtyp(),
        customValidationList,
        SubmissionErrorMsg.STUDY_REQUIRE_NIH_RAC_REVIEW_MISSING.getResultText(),
        SubmissionErrorMsg.STUDY_REQUIRE_NIH_RAC_REVIEW_MISSING.getTestText(), finalMap, displayError);
  }

  private StudyIdJB studyIdJbFactory(String studyId, StudyIdDao studyIdDao, String codelst_subType,
      Map<String, String> finalMap) {
    StudyIdJB studyIdJb = null;
    if (finalMap.get(codelst_subType) != null && !finalMap.get(codelst_subType).isEmpty()) {
      try {
        studyIdJb = new StudyIdJB(Integer.parseInt(finalMap.get(codelst_subType)));
        studyIdJb.getStudyIdDetails();
      } catch (NumberFormatException e) {
        // TODO :do sthg here
    	e.printStackTrace();
      }
    }
    return studyIdJb;
  }


  private void populateCustomValidationListForErrors(ArrayList<Object> customValidationList,
      String result_text_Key, String test_text_key, boolean displayError) {
    if (displayError) {
      HashMap<String, Object> resultMap = new HashMap<String, Object>();
      resultMap.put(RESULT_TEXT_KEY, result_text_Key);
      resultMap.put(RESULT_FLAG_KEY, FAILURE_FLAG);
      resultMap.put(TEST_TEXT_KEY, test_text_key);
      customValidationList.add(resultMap);
    }
  }

  private String checkMoreStudyDetailsFields(String studyId, StudyIdDao studyIdDao,
      String codelst_subType, ArrayList<Object> customValidationList, String resultText,
      String testText, Map<String, String> finalMap, boolean displayError) {

    StudyIdJB studyIdJb = studyIdJbFactory(studyId, studyIdDao, codelst_subType, finalMap);
    if (studyIdJb != null) {
      if ((null == studyIdJb.getAlternateStudyId())
          || (StringUtil.isEmpty(studyIdJb.getAlternateStudyId()))) {
        populateCustomValidationListForErrors(customValidationList, resultText, testText,
            displayError);
        return null;
      }
    } else {
      populateCustomValidationListForErrors(customValidationList, resultText, testText,
          displayError);
      return null;
    }
    return studyIdJb.getAlternateStudyId();
  }
  
  private String checkMoreStudyDetailsTextAreaFields(String studyId, StudyIdDao studyIdDao,
      String codelst_subType, ArrayList<Object> customValidationList, String resultText,
      String testText, Map<String, String> finalMap, boolean displayError) {

    if (finalMap.get(codelst_subType) != null && !finalMap.get(codelst_subType).isEmpty()) {
      return finalMap.get(codelst_subType);
    } else {
      populateCustomValidationListForErrors(customValidationList, resultText, testText,
          displayError);
      return null;
    }
  }


  private boolean checkStandardFields(StudyJB studyJB, ArrayList<Object> customValidationList,
      String studyJbfieldValue, String resultText, String testText) {
    if (StringUtil.isEmpty(studyJbfieldValue) || studyJbfieldValue.equals("0")) {
      populateCustomValidationListForErrors(customValidationList, resultText, testText, true);
      return false;
    }

    return true;
  }


}
