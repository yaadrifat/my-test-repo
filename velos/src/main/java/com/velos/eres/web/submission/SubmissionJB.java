package com.velos.eres.web.submission;

import java.util.ArrayList;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.submission.impl.SubmissionBean;
import com.velos.eres.service.submissionAgent.SubmissionAgent;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

public class SubmissionJB {

    private int id = 0;
    private Integer fkStudy;
    private Integer submissionFlag;
    private Integer submissionType;
    private Integer submissionStatus;
    private Integer amendFlag;
    private Integer creator;
    private Integer lastModifiedBy;
    private String ipAdd;

    public boolean getSubmissionAccess(String usrId, String submissionPK) {
        boolean hasAccess = false;
        try {
            SubmissionAgent submissionAgent = EJBUtil.getSubmissionAgentHome();
            hasAccess = submissionAgent.getSubmissionAccess(usrId, submissionPK);
        } catch(Exception e) {
            return hasAccess;
        }
        return hasAccess;
    }
    
    public int getExistingSubmissionByFkStudy(String studyId) {
        int output = 0;
        try {
            SubmissionAgent submissionAgent = EJBUtil.getSubmissionAgentHome();
            Integer eOut = 
                submissionAgent.getExistingSubmissionByFkStudy(EJBUtil.stringToInteger(studyId));
            if (eOut.intValue() > 0) { return eOut.intValue(); }
        } catch(Exception e) {
            Rlog.fatal("submission", "Error in SubmissionJB.getExistingSubmission "+e);
            output = -1;
        }
        return output;
    }
    
    public int getNewAmendSubmissionByFkStudy(String studyId) {
        int output = 0;
        try {
            SubmissionAgent submissionAgent = EJBUtil.getSubmissionAgentHome();
            Integer eOut = 
                submissionAgent.getNewAmendSubmissionByFkStudy(EJBUtil.stringToInteger(studyId));
            if (eOut.intValue() > 0) { return eOut.intValue(); }
        } catch(Exception e) {
            Rlog.fatal("submission", "Error in SubmissionJB.getExistingSubmission "+e);
            output = -1;
        }
        return output;
    }
    
    public int updateSubmissionByFkStudy() {
        int output = 0;
        try {
            SubmissionAgent submissionAgent = EJBUtil.getSubmissionAgentHome();
            output = submissionAgent.updateSubmissionByFkStudy(createEntityBean());
        } catch(Exception e) {
            Rlog.fatal("submission", "Error in SubmissionJB.updateSubmissionByFkStudy "+e);
            output = -1;
        }
        return output;
    }

    public int createSubmission() {
        int output = 0;
        try {
            SubmissionAgent submissionAgent = EJBUtil.getSubmissionAgentHome();
            Integer eOut;
            if("LIND".equals(CFG.EIRB_MODE)){
            	eOut = submissionAgent.getNewAmendSubmissionByFkStudy(this.fkStudy);
            }else{
            	eOut = submissionAgent.getExistingSubmissionByFkStudy(this.fkStudy);
            }
            if (eOut.intValue() > 0) { return eOut.intValue(); }
            
            CodeDao codeDao = new CodeDao();
            codeDao.getCodeValues("submission");
            ArrayList codeSubtypes = codeDao.getCSubType();
            ArrayList codePks = codeDao.getCId();
            for (int iX=0; iX<codeSubtypes.size(); iX++) {
                if ("new_app".equals(codeSubtypes.get(iX))) {
                    submissionType = (Integer)codePks.get(iX);
                    break;
                }
            }
            codeDao = null;
            codeSubtypes = null;
            codePks = null;
            codeDao = new CodeDao();
            codeDao.getCodeValues("subm_status");
            codeSubtypes = codeDao.getCSubType();
            codePks = codeDao.getCId();
            for (int iX=0; iX<codeSubtypes.size(); iX++) {
                if ("submitted".equals(codeSubtypes.get(iX))) {
                    submissionStatus = (Integer)codePks.get(iX);
                    break;
                }
            }
            submissionFlag = new Integer(1); // 1=New/Initial; 2=Ongoing
            output = submissionAgent.createSubmission(createEntityBean());
        } catch(Exception e) {
            Rlog.fatal("submission", "Error in SubmissionJB.createSubmission "+e);
            output = -1;
        }
        return output;
    }

    private SubmissionBean createEntityBean() {
        return new SubmissionBean(this.id, this.fkStudy, this.submissionFlag, 
                this.submissionType, this.submissionStatus, this.creator,
                this.lastModifiedBy, this.ipAdd, this.amendFlag);
    }
    
    public void loadSubmissionDetails() {
        SubmissionBean submissionBean = null;
        try {
            SubmissionAgent submissionAgent = EJBUtil.getSubmissionAgentHome();
            submissionBean = submissionAgent.getSubmissionDetails(this.id);
        } catch (Exception e) {
            Rlog.fatal("submission", "Error in SubmissionJB.loadSubmissionDetails " + e);
        }
        
        if (submissionBean == null) { return; }
        
        // Pull the details from Entity bean
        this.fkStudy = submissionBean.getFkStudy();
    }
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getLastModifiedBy() {
        return String.valueOf(lastModifiedBy);
    }
    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = EJBUtil.stringToInteger(lastModifiedBy);
    }
    public String getCreator() {
        return String.valueOf(creator);
    }
    public void setCreator(String creator) {
        this.creator = EJBUtil.stringToInteger(creator);
    }
    public Integer getSubmissionStatus() {
        return submissionStatus;
    }
    public void setSubmissionStatus(String submissionStatus) {
        this.submissionStatus = EJBUtil.stringToInteger(submissionStatus);
    }
    public Integer getSubmissionType() {
        return submissionType;
    }
    public void setSubmissionType(Integer submissionType) {
        this.submissionType = submissionType;
    }
    public String getSubmissionFlag() {
        return String.valueOf(submissionFlag);
    }
    public void setSubmissionFlag(String submissionFlag) {
        if (submissionFlag == null || submissionFlag.length() == 0) {
            this.submissionFlag = null;
        } else {
            this.submissionFlag = EJBUtil.stringToInteger(submissionFlag);
        }
    }
    public String getStudy() {
        return String.valueOf(fkStudy);
    }
    public void setStudy(String study) {
        this.fkStudy = EJBUtil.stringToInteger(study);
    }
    public String getIpAdd() {
        return ipAdd;
    }
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

	public Integer getAmendFlag() {
		return amendFlag;
	}

	public void setAmendFlag(Integer amendFlag) {
		this.amendFlag = amendFlag;
	}
    public int updateAmendFlagSubmission(Integer pkSubmission, Integer amendType){
    	int output = 0;
    try{
    	SubmissionAgent submissionAgent = EJBUtil.getSubmissionAgentHome();
    	output = submissionAgent.updateAmendFlagSubmission(pkSubmission, amendType);
    } catch(Exception e) {
        Rlog.fatal("submission", "Error in SubmissionJB.updateAmendFlagSubmission "+e);
        output = -1;
    }
    	return output;
    }
}
