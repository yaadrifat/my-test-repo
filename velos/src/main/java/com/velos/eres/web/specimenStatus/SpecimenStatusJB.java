/*
 * Classname : 				SpecimenStatusJB
 *
 * Version information : 	1.0
 *
 * Date 					10/15/2007
 *
 * Copyright notice: 		Velos Inc
 *
 * Author 					Jnanamay
 */

package com.velos.eres.web.specimenStatus;

/* IMPORT STATEMENTS */
import java.util.Hashtable;
import com.velos.eres.business.common.SpecimenStatusDao;
import com.velos.eres.business.specimenStatus.impl.SpecimenStatusBean;
import com.velos.eres.service.specimenStatusAgent.SpecimenStatusAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.audit.web.AuditRowEresJB;
import com.velos.eres.business.common.CommonDAO;

/* END OF IMPORT STATEMENTS */

public class SpecimenStatusJB {


    private int pkSpecimenStat;
    private String fkSpecimen;
    private String ssDate;
    private String fkCodelstSpecimenStat;
    private String ssQuantity;
    private String ssQuantityUnits;
    private String ssAction;
    private String fkStudy;
    private String fkUserRecpt;
    private String ssTrackingNum;
    private String ssNote;
    private String ssStatusBy;
    private String creator;
    private String modifiedBy;
    private String ipAdd;
    private java.util.Date ssDt;
    
    private String ssProcType;
    private String ssHandOffDate;
    private String ssProcDate;
    int rid=0;





    /**
	 * @return the pkSpecimenStat
	 */
	public int getPkSpecimenStat() {
		return pkSpecimenStat;
	}

	/**
	 * @param pkSpecimenStat the pkSpecimenStat to set
	 */
	public void setPkSpecimenStat(int pkSpecimenStat) {
		this.pkSpecimenStat = pkSpecimenStat;
	}


	/**
	 * @return fkSpecimen
	 */
	public String getFkSpecimen() {
		return fkSpecimen;
	}

	/**
	 * @param fkSpecimen the fkSpecimen to set
	 */
	public void setFkSpecimen(String fkSpecimen) {
		this.fkSpecimen = fkSpecimen;
	}


	 /**
	 * @return ssDate
	 */
	public String getSsDate() {
		return ssDate;
	}

	public java.util.Date getSsDt() {
		return ssDt;
	}
	
	public void setSsDt(java.util.Date ssDt) {
		this.ssDt = ssDt;
	}
	
	public void setSsDat(String Date) {
	   	setSsDt(DateUtil.stringToTimeStamp(Date, null));
	}
	
	public String getSsDat(){
		return DateUtil.dateToString(getSsDt(),null);
	}
	
	/**
	 * @param ssDate the ssDate to set
	 */
	public void setSsDate(String ssDate) {
		this.ssDate = ssDate;
	}

	/**
	 * @return fkCodelstSpecimenStat
	 */
	public String getFkCodelstSpecimenStat() {
		return fkCodelstSpecimenStat;
	}

	/**
	 * @param fkCodelstSpecimenStat the fkCodelstSpecimenStat to set
	 */
	public void setFkCodelstSpecimenStat(String fkCodelstSpecimenStat) {
		this.fkCodelstSpecimenStat = fkCodelstSpecimenStat;
	}
	/**
	 * @return ssQuantity
	 */
	public String getSsQuantity() {
		return ssQuantity;
	}

	/**
	 * @param ssQuantity the ssQuantity to set
	 */
	public void setSsQuantity(String ssQuantity) {
		this.ssQuantity = ssQuantity;
	}

	/**
	 * @return ssQuantityUnits
	 */
	public String getSsQuantityUnits() {
		return ssQuantityUnits;
	}

	/**
	 * @param ssQuantityUnits the ssQuantityUnits to set
	 */
	public void setSsQuantityUnits(String ssQuantityUnits) {
		this.ssQuantityUnits = ssQuantityUnits;
	}

	 /**
	 * @return ssAction
	 */
	public String getSsAction() {
		return ssAction;
	}

	/**
	 * @param ssDate the ssDate to set
	 */
	public void setSsAction(String ssAction) {
		this.ssAction = ssAction;
	}

	 /**
	 * @return fkStudy
	 */
	public String getFkStudy() {
		return fkStudy;
	}

	/**
	 * @param fkStudy the fkStudy to set
	 */
	public void setFkStudy(String fkStudy) {
		this.fkStudy = fkStudy;
	}

	/**
	 * @return fkUserRecpt
	 */
	public String getFkUserRecpt() {
		return fkUserRecpt;
	}

	/**
	 * @param fkUserRecpt the fkUserRecpt to set
	 */
	public void setFkUserRecpt(String fkUserRecpt) {
		this.fkUserRecpt = fkUserRecpt;
	}

	/**
	 * @return ssTrackingNum
	 */
	public String getSsTrackingNum() {
		return ssTrackingNum;
	}

	/**
	 * @param ssTrackingNum the ssTrackingNum to set
	 */
	public void setSsTrackingNum(String ssTrackingNum) {
		this.ssTrackingNum = ssTrackingNum;
	}


////////////////////////////////////////////////////


	public String getSsNote(){
		return this.ssNote;
	}


	public void setSsNote(String ssNote){
    	this.ssNote = ssNote;

    }



	////////////////////////////////////////////////




	/**
	 * @return ssStatusBy
	 */
	public String getSsStatusBy() {
		return ssStatusBy;
	}

	/**
	 * @param ssTrackingNum the ssTrackingNum to set
	 */
	public void setSsStatusBy(String ssStatusBy) {
		this.ssStatusBy = ssStatusBy;
	}


	/**
	 * @return the creator
	 */
	public String getCreator() {
		return creator;
	}

	/**
	 * @param creator the creator to set
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}


	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}


	/**
	 * @return the ipAdd
	 */
	public String getIpAdd() {
		return ipAdd;
	}

	/**
	 * @param ipAdd the ipAdd to set
	 */
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}



	 //JM: #INVP2.5.1(c)

    public String getSsProcType() {
    	return ssProcType;
    }


    public void setSsProcType(String ssProcType) {
        this.ssProcType = ssProcType;
    }

    /**
	 * @return ssHandOffDate
	 */
	public String getSsHandOffDate() {
		return ssHandOffDate;
	}

	/**
	 * @param ssHandOffDate the ssHandOffDate to set
	 */
	public void setSsHandOffDate(String ssHandOffDate) {
		this.ssHandOffDate = ssHandOffDate;
	}

	/**
	 * @return ssProcDate
	 */
	public String getSsProcDate() {
		return ssProcDate;
	}

	/**
	 * @param ssProcDate the ssProcDate to set
	 */
	public void setSsProcDate(String ssProcDate) {
		this.ssProcDate = ssProcDate;
	}


//	 END OF THE GETTER AND SETTER METHODS

	/**
     * Constructor
     *
     * @param pkStorageStat:
     *            Unique Id constructor
     *
     */

    public SpecimenStatusJB(int pkSpecimenStat) {
    	setPkSpecimenStat(pkSpecimenStat);
    }

    /**
     * Default Constructor
     */

    public SpecimenStatusJB() {
        Rlog.debug("specimenStatus", "SpecimenStatusJB.SpecimenStatusJB() ");
    }





	public SpecimenStatusJB(int pkSpecimenStat,
		    String fkSpecimen,
		    String ssDate,
		    String fkCodelstSpecimenStat,
		    String ssQuantity,
		    String ssQuantityUnits,
		    String ssAction,
		    String fkStudy,
		    String fkUserRecpt,
		    String ssTrackingNum,
		    String ssNote,
		    String ssStatusBy,
		    String creator,
		    String modifiedBy,
		    String ipAdd,
		    String ssProcType,
		    String ssHandOffDate,
		    String ssProcDate) {

		setPkSpecimenStat(pkSpecimenStat);
		setFkSpecimen(fkSpecimen);
		setSsDate(ssDate);
		setFkCodelstSpecimenStat(fkCodelstSpecimenStat);
		setSsQuantity(ssQuantity);
		setSsQuantityUnits(ssQuantityUnits);
		setSsAction(ssAction);
		setFkStudy(fkStudy);
		setFkUserRecpt(fkUserRecpt);
		setSsTrackingNum(ssTrackingNum);
		setSsNote(ssNote);
		setSsStatusBy(ssStatusBy);
		setCreator(creator);
		setModifiedBy(modifiedBy);
		setIpAdd(ipAdd);
		setSsProcType(ssProcType);
		setSsHandOffDate(ssHandOffDate);
        setSsProcDate(ssProcDate);
	}


	  /**
     * Retrieves the details of a specimen in SpecimenStateKeeper Object
     *
     * @return SpecimenStateKeeper consisting of the storage details
     * @see SpecimenStateKeeper
     */
    public SpecimenStatusBean getSpecimenStatusDetails() {
    	SpecimenStatusBean spsk = null;
        try {
        	SpecimenStatusAgentRObj specimenStatusAgentRObj = EJBUtil.getSpecimenStatusAgentHome();
        	spsk = specimenStatusAgentRObj.getSpecimenStatusDetails(this.pkSpecimenStat);
            Rlog.debug("specimenStatus", "In SpecimenStatusJB.getSpecimenStatusDetails() " + spsk);
        } catch (Exception e) {
            Rlog.fatal("specimenStatus", "Error in getSpecimenStatusDetails() in SpecimenStatusJB " + e);
        }
        if (spsk != null) {
            this.fkSpecimen = spsk.getFkSpecimen();
            this.ssDate = spsk.getSsDate();
            this.ssDt = spsk.getSsDt();
            this.fkCodelstSpecimenStat = spsk.getFkCodelstSpecimenStat();
            this.ssQuantity = spsk.getSsQuantity();
            this.ssQuantityUnits = spsk.getSsQuantityUnits();
            this.ssAction = spsk.getSsAction();
            this.fkStudy = spsk.getFkStudy();
            this.fkUserRecpt = spsk.getFkUserRecpt();
            this.ssTrackingNum = spsk.getSsTrackingNum();
            this.ssNote = spsk.getSsNotes();
            this.ssStatusBy = spsk.getSsStatusBy();
            this.creator = spsk.getCreator();
            this.modifiedBy = spsk.getModifiedBy();
            this.ipAdd = spsk.getIpAdd();
            this.ssProcType = spsk.getSsProcType();
            this.ssHandOffDate = spsk.getSsHandOffDate();
            this.ssProcDate = spsk.getSsProcDate();
        }
        return spsk;
    }


    public int setSpecimenStatusDetails() {
        int output = 0;
        try {
        	SpecimenStatusAgentRObj specimenStatusAgentRObj = EJBUtil.getSpecimenStatusAgentHome();
            output = specimenStatusAgentRObj.setSpecimenStatusDetails(this.createSpecimenStatusStateKeeper());
            this.setPkSpecimenStat(output);

            CommonDAO cDao = new CommonDAO();
            AuditRowEresJB auditRowEresJB = new  AuditRowEresJB();
            AuditUtils audittrails =new AuditUtils();
            String notesspec=this.getSsNote();
            int pkspecimenStat=this.getPkSpecimenStat();
            String usrname=this.getCreator();
            if(notesspec==null){
            	notesspec="";
            }
            Rlog.debug("specimen", "specimen notes clob goes here ..." + this.getSsNote());
            if(!notesspec.equals("")){
            cDao.updateClob(this.getSsNote(),"ER_SPECIMEN_STATUS","SS_NOTES"," where PK_SPECIMEN_STATUS = " + output);
           
            rid = auditRowEresJB.getRidForDeleteOperation(pkspecimenStat, EJBUtil.stringToNum(usrname), "ER_SPECIMEN_STATUS", "PK_SPECIMEN_STATUS");
			audittrails.deleteRaidList("eres", rid, usrname, "U");
            
            }
        }catch (Exception e) {
            Rlog
                    .fatal("specimenStatus", "Error in setSpecimenStatusDetails() in SpecimenStatusJB " + e);
          return -2;
        }
        return output;
    }


    public int updateSpecimenStatus() {
        int output;
        try {
        	SpecimenStatusAgentRObj specimenStatusAgentRObj = EJBUtil.getSpecimenStatusAgentHome();
            output = specimenStatusAgentRObj.updateSpecimenStatus(this.createSpecimenStatusStateKeeper());

            CommonDAO cDao = new CommonDAO();
            AuditRowEresJB auditRowEresJB = new  AuditRowEresJB();
            AuditUtils audittrails =new AuditUtils();
            String notesspec=this.getSsNote();
            int pkspecimenStat=this.getPkSpecimenStat();
            String usrname=this.getModifiedBy();
           // if(!notesspec.equals("")){
            cDao.updateClob(this.getSsNote(),"ER_SPECIMEN_STATUS","SS_NOTES"," where PK_SPECIMEN_STATUS = " + this.getPkSpecimenStat());
           
            rid = auditRowEresJB.getRidForDeleteOperation(pkspecimenStat, EJBUtil.stringToNum(usrname), "ER_SPECIMEN_STATUS", "PK_SPECIMEN_STATUS");
			audittrails.deleteRaidList("eres", rid, usrname, "U");
            
          //  }
        } catch (Exception e) {
        	Rlog
            .fatal("specimenStatus", "Error in updateSpecimenStatus() in SpecimenStatusJB " + e);

            return -2;
        }
        return output;
    }

    public SpecimenStatusBean createSpecimenStatusStateKeeper() {
        Rlog.debug("specimenStatus", "SpecimenStatusJB.createSpecimenStatusStateKeeper ");
        return new SpecimenStatusBean(
        		this.pkSpecimenStat,
        		this.fkSpecimen,
        		this.ssDate,
        		this.ssDt,
        		this.fkCodelstSpecimenStat,
        		this.ssQuantity,
        		this.ssQuantityUnits,
        		this.ssAction,
        		this.fkStudy,
        		this.fkUserRecpt,
        		this.ssTrackingNum,
        		this.ssNote,
        		this.ssStatusBy,
        		this.creator,
        		this.modifiedBy,
        		this.ipAdd,
        		this.ssProcType,
        		this.ssHandOffDate,
                this.ssProcDate
        		);
    }



    public SpecimenStatusDao getSpecimenStausValues(int specimenId) {

    	SpecimenStatusDao spStatusDao = new SpecimenStatusDao();
    	try {
    		SpecimenStatusAgentRObj specimenStatusAgentRObj = EJBUtil.getSpecimenStatusAgentHome();

    		spStatusDao = specimenStatusAgentRObj.getSpecimenStausValues(specimenId);
            return spStatusDao;

        } catch (Exception e) {
            Rlog.fatal("specimenStatus", "Exception In getSpecimenStausValues in SpecimenStatusJB " + e);
        }
        return spStatusDao;


    }
    //JM: #INV 2.29 added method
    public SpecimenStatusDao getSpecimenStausProcessValues(int specimenId) {

    	SpecimenStatusDao spStatusDao = new SpecimenStatusDao();
    	try {
    		SpecimenStatusAgentRObj specimenStatusAgentRObj = EJBUtil.getSpecimenStatusAgentHome();

    		spStatusDao = specimenStatusAgentRObj.getSpecimenStausProcessValues(specimenId);
            return spStatusDao;

        } catch (Exception e) {
            Rlog.fatal("specimenStatus", "Exception In getSpecimenStausProcessValues in SpecimenStatusJB " + e);
        }
        return spStatusDao;


    }

    //Added by Manimaran for deletion of specimen status.
    public int delSpecimenStatus(int pkSpecimenStat) {
        int ret = 0;
    	try {
    		SpecimenStatusAgentRObj specimenStatusAgentRObj = EJBUtil.getSpecimenStatusAgentHome();
            Rlog.debug("specimenstatus", "SpecimenStatusJB.delSpecimenStatus after remote");
            ret = specimenStatusAgentRObj.delSpecimenStatus(pkSpecimenStat);
        } catch (Exception e) {
            Rlog.fatal("study", "Error in delSpecimenStatus in SpecimenStatusJB " + e);
            return -1;
        }
        return ret;
    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int delSpecimenStatus(int pkSpecimenStat,Hashtable<String, String> auditInfo) {
        int ret = 0;
    	try {
    		SpecimenStatusAgentRObj specimenStatusAgentRObj = EJBUtil.getSpecimenStatusAgentHome();
            ret = specimenStatusAgentRObj.delSpecimenStatus(pkSpecimenStat,auditInfo);
        } catch (Exception e) {
            Rlog.fatal("study", "Error in delSpecimenStatus in SpecimenStatusJB " + e);
            return -1;
        }
        return ret;
    }


    public SpecimenStatusDao getLatestSpecimenStaus(int specimenId) {

    	SpecimenStatusDao spStatusDao = new SpecimenStatusDao();
    	try {
    		SpecimenStatusAgentRObj specimenStatusAgentRObj = EJBUtil.getSpecimenStatusAgentHome();

    		spStatusDao = specimenStatusAgentRObj.getLatestSpecimenStaus(specimenId);
            return spStatusDao;

        } catch (Exception e) {
            Rlog.fatal("specimenStatus", "Exception In getLatestSpecimenStaus in SpecimenStatusJB " + e);
        }
        return spStatusDao;


    }
    
    //Ankit: Bug#9971 Date: 27-June-2012
    public int updateSpecimenProcessType() {
        int output;
        try {
        	SpecimenStatusAgentRObj specimenStatusAgentRObj = EJBUtil.getSpecimenStatusAgentHome();
            output = specimenStatusAgentRObj.updateSpecimenProcessType(this.createSpecimenStatusStateKeeper());

            CommonDAO cDao = new CommonDAO();
            if(this.getSsNote()!=null)
            cDao.updateClob(this.getSsNote(),"ER_SPECIMEN_STATUS","SS_NOTES"," where PK_SPECIMEN_STATUS = " + this.getPkSpecimenStat());

        } catch (Exception e) {
        	Rlog.fatal("specimenStatus", "Error in updateSpecimenProcessType() in SpecimenStatusJB " + e);

            return -2;
        }
        return output;
    }



}// end of class

