package com.velos.eres.web.network;

import com.velos.eres.business.network.impl.NetworkBean;
import com.velos.eres.service.networkAgent.NetworkAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

public class NetworkJB {

	private Integer networkId;
	private String networkSiteId;
	private String siteId;
	private String networkLevel;
	private String creator;
	private String modifiedBy;
	private String ipAdd;
	private String networkTypeId;
	private String networkMainId;
	private String networkStatusId;
	
	public Integer getNetworkId() {
		return networkId;
	}
	public void setNetworkId(Integer networkId) {
		this.networkId = networkId;
	}
	public String getNetworkSiteId() {
		return networkSiteId;
	}
	public void setNetworkSiteId(String networkSiteId) {
		this.networkSiteId = networkSiteId;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public String getNetworkLevel() {
		return networkLevel;
	}
	public void setNetworkLevel(String networkLevel) {
		this.networkLevel = networkLevel;
	}
	public String getNetworkTypeId() {
		return networkTypeId;
	}
	public void setNetworkTypeId(String networkTypeId) {
		this.networkTypeId = networkTypeId;
	}
	public String getNetworkMainId() {
		return networkMainId;
	}
	public void setNetworkMainId(String networkMainId) {
		this.networkMainId = networkMainId;
	}
	public String getNetworkStatusId() {
		return networkStatusId;
	}
	public void setNetworkStatusId(String networkStatusId) {
		this.networkStatusId = networkStatusId;
	}	
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getIpAdd() {
		return ipAdd;
	}
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
	public NetworkJB() {
		super();
		// TODO Auto-generated constructor stub
	}
	public NetworkJB(Integer networkId) {
        setNetworkId(networkId);
    }
	public NetworkJB(Integer networkId, String networkSiteId, String siteId, String networkLevel,String creator,String modifiedBy, 
			String ipAdd,String networkTypeId, String networkMainId, String networkStatusId) {
		super();
		this.networkId = networkId;
		this.networkSiteId = networkSiteId;
		this.siteId = siteId;
		this.networkLevel = networkLevel;
		this.creator = creator;
		this.ipAdd = ipAdd;
		this.modifiedBy = modifiedBy;
		this.networkTypeId = networkTypeId;
		this.networkMainId = networkMainId;
		this.networkStatusId = networkStatusId;
	}
	public NetworkBean createNetworkStateKeeper() {

        return new NetworkBean(networkId, networkSiteId,siteId,networkLevel,creator,modifiedBy,ipAdd,
        		networkTypeId,networkMainId,networkStatusId);

    }
	public void setNetworkDetails() {
        try {
            NetworkAgentRObj networkAgent = EJBUtil.getNetworkAgentHome();
            this.setNetworkId(networkAgent.setNetworkDetails(this
                    .createNetworkStateKeeper()));
            Rlog.debug("network", "NetworkJB.setNetworkDetails()");
        } catch (Exception e) {
            Rlog.debug("network", "Error in setNetworkDetails() in NetworkJB " + e);
        }
    }
	
	public int updateNetwork() {
        int output;
        try {
        	NetworkAgentRObj networkAgent = EJBUtil.getNetworkAgentHome();
            output = networkAgent.updateNetwork(this.createNetworkStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("network", "EXCEPTION IN SETTING NETWORK DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }
	public NetworkBean getNetworkDetails() {
        NetworkBean nsk = null;

        try {

            NetworkAgentRObj networkAgent = EJBUtil.getNetworkAgentHome();

            nsk = networkAgent.getNetworkDetails(this.networkId);
            Rlog
                    .debug("network", "NetworkJB.getNetworkDetails() NetworkStateKeeper "
                            + nsk);
        } catch (Exception e) {
            Rlog.debug("network", "Error in getNetworkDetails() in NetworkJB " + e);
        }
        if (nsk != null) {
        	this.networkId = nsk.getNetworkId();
        	this.networkSiteId = nsk.getNetworkSiteId();
        	this.networkLevel = nsk.getNetworkLevel();
        	this.siteId = nsk.getSiteId();
        	this.networkMainId = nsk.getNetworkMainId();
        	this.networkStatusId = nsk.getNetworkStatusId();
        	this.networkTypeId = nsk.getNetworkTypeId();
            Rlog.debug("network", "NetworkJB.nsk.getNetworkId" + nsk.getNetworkId());
        }
        return nsk;
    }

}
