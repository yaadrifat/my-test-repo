package com.velos.eres.web.specimen;

import java.util.ArrayList;
/**
 * Data transfer object class for preparation cart.
 * 
 * @author Bikash
 */
public class PreparationCartDto {
	/**
	 * 
	 * 
	 * the Person ID List
	 * 
	 * 
	 */
	private ArrayList<String> personPIdList;	
	/**
	 * the Study Number List
	 */
	private ArrayList<String> studyNumberList;
	/**
	 * the Calendar List
	 */
	private ArrayList<String> CalendarList;
	/**
	 * the visit List
	 */
	private ArrayList<String> visitList;
	/**
	 * the scheduled date List
	 */
	private ArrayList<String> scheduledDateList;
	/**
	 * the event List
	 */
	private ArrayList<String> eventList;
	/**
	 * the storageKitList List
	 */
	private ArrayList<String> storageKitList;
	/**
	 * the storage primary key List
	 */
	private ArrayList<String> pkStorageList;
	/**
	 * the events foreign keys List
	 */
	private ArrayList<String> fkSchEventList;
	/**
	 * the visit foreign key List
	 */
	private ArrayList<String> fkVisitList;
	/**
	 * the study foreign key List
	 */
	private ArrayList<String> fkStudyList;
	/**
	 * the person foreign key List
	 */
	private ArrayList<String> fkPerList;
	/**
	 * the person foreign key List
	 */
	private ArrayList<String> fkSiteList;
	/**
	 *  * the person study ID List
	 */
	private ArrayList<String> patientstudyIdList;
	/**
	 *  * the event status List
	 */
	private ArrayList<String> eventstatusList;
	/**
	 *  * the name of enrolling site List
	 */
	private ArrayList<String> enrollingSiteList;
	/**
	 *  * the the date of event status List
	 */
	private ArrayList<String> eventStatusDateList;
	/**
	 
	  
	 
	 * Default Constructor.
	 */
	public PreparationCartDto()
	{
		personPIdList = new ArrayList<String>();	
		studyNumberList = new ArrayList<String>();	
		CalendarList = new ArrayList<String>();
		visitList = new ArrayList<String>();
		scheduledDateList = new ArrayList<String>();
		eventList = new ArrayList<String>();
		storageKitList= new ArrayList<String>();
		pkStorageList= new ArrayList<String>();
		fkSchEventList= new ArrayList<String>();
		fkVisitList= new ArrayList<String>();
		fkStudyList= new ArrayList<String>();
		fkPerList= new ArrayList<String>();
		fkSiteList= new ArrayList<String>();
		patientstudyIdList= new ArrayList<String>();
		eventstatusList= new ArrayList<String>();
		enrollingSiteList= new ArrayList<String>();
		eventStatusDateList= new ArrayList<String>();
	}



	//getters
	/**
	 * get patient id list.
	 * 
	 * @return ArrayList.
	 */
	public ArrayList<String> getPersonPIdList() {
		return personPIdList;
	}
	/**
	 * get Study id list.
	 * 
	 * @return ArrayList.
	 */

	public ArrayList<String> getStudyNumberList() {
		return studyNumberList;
	}
	/**
	 * get calendar list.
	 * @return
	 */
	public ArrayList<String> getCalendarList() {
		return CalendarList;
	}
	/**
	 * get schedule date list.
	 * 
	 * @return
	 */
	public ArrayList<String> getScheduledDateList() {
		return scheduledDateList;
	}	
	/**
	 * get visit list.
	 * 
	 * @return
	 */
	public ArrayList<String> getVisitList() {
		return visitList;
	}
	/**
	 * get list of events.
	 * @return
	 */
	public ArrayList<String> getEventList() {
		return eventList;
	}
	/**
	 * get list of storage kits.
	 * @return
	 */
	public ArrayList<String> getStorageKitList() {
		return storageKitList;
	}
	/**
	 * get list of primary key storage kit.
	 * 
	 * @return
	 */
	public ArrayList<String> getPkStorageList() {
		return pkStorageList;
	}
	/**
	 * get list of foreign keys of scheduled events.
	 * 
	 * @return
	 */
	public ArrayList<String> getFkSchEventList() {
		return fkSchEventList;
	}
	/**
	 * get list of foreign keys of visits.
	 * 
	 * @return
	 */

	public ArrayList<String> getFkVisitList() {
		return fkVisitList;
	}
	/**
	 * get list of foreign keys of study.
	 * @return
	 */
	public ArrayList<String> getFkStudyList() {
		return fkStudyList;
	}
	/**
	 * get of foreign keys of patient.
	 * 
	 * @return
	 */
	public ArrayList<String> getFkPerList() {
		return fkPerList;
	}
	/**
	 * get list of foreign keys of organizations.
	 * @return
	 */
	public ArrayList<String> getFkSiteList() {
		return fkSiteList;
	}
	/**
	 * get list of patient Id.
	 * @return
	 */
	public ArrayList<String> getPatientstudyIdList() {
		return patientstudyIdList;
	}
	/**
	 * get list of event status.
	 * @return
	 */
	public ArrayList<String> getEventstatusList() {
		return eventstatusList;
	}
	/**
	 * get list of name of organization.
	 * @return
	 */
	public ArrayList<String> getEnrollingSiteList() {
		return enrollingSiteList;
	}
	
	/**
	 * get list of event status Date.
	 * @return
	 */
	public ArrayList<String> getEventStatusDateList() {
		return eventStatusDateList;
	}
	

	//setters
	/**
	 * set the value of <code>personPIdList</code>.
	 * 
	 * @param personPIdList
	 */
	public void setPersonPIdList(ArrayList<String> personPIdList) {
		this.personPIdList.addAll(personPIdList);
	}
	/**
	 * set the value of <code>studyNumberList</code>.
	 * 
	 * @param studyNumberList
	 */
	public void setStudyNumberList(ArrayList<String> studyNumberList) {	
		this.studyNumberList.addAll(studyNumberList);
	}
	/**
	 * set the value of <code>calendarList</code>.
	 * 
	 * @param calendarList
	 */
	public void setCalendarList(ArrayList<String> calendarList) {
		this.CalendarList.addAll(calendarList);
	}	
	/**
	 * set the value of <code>visitList</code>.
	 * 
	 * @param visitList
	 */
	public void setVisitList(ArrayList<String> visitList) {
		this.visitList.addAll(visitList);
	}	
	/**
	 * set the value of <code>visitList</code>.
	 * 
	 * @param scheduledDateList
	 */
	public void setScheduledDateList(ArrayList<String> scheduledDateList) {
		this.scheduledDateList.addAll(scheduledDateList);
	}	
	/**
	 * set the value of <code>eventList</code>.
	 * @param eventList
	 */
	public void setEventList(ArrayList<String> eventList) {
		this.eventList.addAll(eventList);
	}	
	/**
	 *  set the value of <code>storageKitList</code>.
	 * @param storageKitList
	 */
	public void setStorageKitList(ArrayList<String> storageKitList) {
		this.storageKitList.addAll(storageKitList);
	}
	/**
	 * set the value of <code>pkStorageList</code>.
	 * @param pkStorageList
	 */
	public void setPkStorageList(ArrayList<String> pkStorageList) {
		this.pkStorageList.addAll(pkStorageList);
	}
	/**
	 * set the value of <code>fkSchEventList</code>.
	 * @param fkSchEventList
	 */
	public void setFkSchEventList(ArrayList<String> fkSchEventList) {
		this.fkSchEventList.addAll(fkSchEventList);
	}
	/**
	 * set the value of <code>fkVisitList</code>.
	 * 
	 * @param fkVisitList
	 */
	public void setFkVisitList(ArrayList<String> fkVisitList) {
		this.fkVisitList.addAll(fkVisitList);
	}
	/**
	 * set the value of <code>fkStudyList</code>.
	 * 
	 * @param fkStudyList
	 */
	public void setFkStudyList(ArrayList<String> fkStudyList) {
		this.fkStudyList.addAll(fkStudyList);
	}
	/**
	 * set the value of <code>fkPerList</code>.
	 * 
	 * @param fkPerList
	 */
	public void setFkPerList(ArrayList<String> fkPerList) {
		this.fkPerList.addAll(fkPerList);
	}
	/**
	 * set the value of <code>fkSiteList</code>.
	 * 
	 * @param fkSiteList
	 */
	public void setFkSiteList(ArrayList<String> fkSiteList) {
		this.fkSiteList.addAll(fkSiteList);
	}
	
	/**
	 * set the value of <code>patientstudyIdList</code>.
	 * 
	 * @param patientstudyIdList
	 */
	public void setPatientstudyIdList(ArrayList<String> patientstudyIdList) {
		this.patientstudyIdList.addAll(patientstudyIdList);
	}
	/**
	 * set the value of <code>eventstatusList</code>.
	 * 
	 * @param eventstatusList
	 */
	public void setEventstatusList(ArrayList<String> eventstatusList) {
		this.eventstatusList.addAll(eventstatusList);
	}
	/**
	 * set the value of <code>enrollingSiteList</code>.
	 * 
	 * @param enrollingSiteList
	 */
	public void setEnrollingSiteList(ArrayList<String> enrollingSiteList) {
		this.enrollingSiteList.addAll(enrollingSiteList);
	}
	/**
	 * set the value of <code>eventStatusDateList</code>.
	 * 
	 * @param eventStatusDateList
	 */
	public void setEventStatusDateList(ArrayList<String> eventStatusDateList) {
		this.eventStatusDateList.addAll(eventStatusDateList);
	}
	
	
	//remover function


	/**
	 * Removes the given element from data transfer object. 
	 * 
	 * @param index 
	 */
	public void remove(int index){
		this.personPIdList.remove(index);
		this.studyNumberList.remove(index);
		this.CalendarList.remove(index);
		this.visitList.remove(index);
		this.scheduledDateList.remove(index);
		this.eventList.remove(index);
		this.storageKitList.remove(index);
		this.pkStorageList.remove(index);
		this.fkSchEventList.remove(index);
		this.fkVisitList.remove(index);
		this.fkStudyList.remove(index);
		this.fkPerList.remove(index);
		this.fkSiteList.remove(index);
		this.patientstudyIdList.remove(index);
		this.eventstatusList.remove(index);
		this.enrollingSiteList.remove(index);
		this.eventStatusDateList.remove(index);
		
		
	}
	/**
	 * Removes all element from data transfer object. 
	 * 
	 * @param index 
	 */
	public void removeAllElements(){
		this.personPIdList.clear();
		this.studyNumberList.clear();
		this.CalendarList.clear();
		this.visitList.clear();
		this.scheduledDateList.clear();
		this.eventList.clear();
		this.storageKitList.clear();
		this.pkStorageList.clear();
		this.fkSchEventList.clear();
		this.fkVisitList.clear();
		this.fkStudyList.clear();
		this.fkPerList.clear();
		this.fkSiteList.clear();
		this.patientstudyIdList.clear();
		this.eventstatusList.clear();
		this.enrollingSiteList.clear();
		this.eventStatusDateList.clear();
		
		
	}

}
