package com.velos.eres.web.dynrep.holder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;

import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;

/**
 * Holds the data of an AdHoc query and exposes methods to generate XML output
 * 
 * @author Sonia Abrol
 * @created October 2, 2006
 */

public class ReportDataHolder implements Serializable {

    /**
     * Hashtable of data rows for each form
     */
    Hashtable htFormData;
     
    
    /**
     * Hashtable of Section Ids for each form
     */
    Hashtable htFormMapSecIds;
   
    /**
     * Hashtable of Section Names for each form
     */
    Hashtable htFormMapSecNames;
    
    /**
     * Hashtable of Repeat Field Names Sequence (in the form query) for each form
     */
    Hashtable htFormRepFldNamesSeq;
    
    /**
     * Hashtable Repeat Field Display Data value flag for each form
     */
    Hashtable htFormRepFldDisplayDataValue;
    
    /**
     * Hashtable of Repeat Field Columns for each form
     */
    Hashtable htFormRepFldCols;
    
    /** ArrayList of form Ids*/
    
    ArrayList arFormIds;
    
    /** ArrayList of form Names*/
    ArrayList arFormNames;
    
    /** Hashtable of actual form field Names*/
    Hashtable htActualFormFieldNames;
    
    /** Hashtable of report form field Names*/
    Hashtable htReportFormFieldNames;
    
    
    /** Total field count in the report (each repeat field section is counted as one field)*/
    int fieldCount ;
    
    /** field count for the last form processed (with or without data rows) (while generating xml)*/
    int lastFormFieldCount;
    
    
    
    
    /** field types*/
    Hashtable htFormFieldTypes;
    
    /**
     * Hashtable Field Display Data value flag for each form
     */
    Hashtable htFormFldDisplayDataValue;
    
    Hashtable htFormfldWidth;
    
    /**
     * Hashtable of Repeat Field Names for each form
     */
    Hashtable htFormRepFldNames;
    
    /**
     * Hashtable of max field couner for each form
     */
    Hashtable htFormFieldMaxCounter;
    
    
    /** Hashtable to hold SummaryHolder objects for each form*/			
	Hashtable htSummaryHolder;
	
	Hashtable 	htFileNameForIDROWMappingHash; 
	
	Hashtable 	htFormfieldCount;
	
	Hashtable htLinearData;
	
	ArrayList reportDataKeys;
	
	ArrayList reportUseDispdataValue;
	
	 
	Hashtable 	htFormfieldCountWithoutSplitRepeat;
	
	Hashtable 	htFormfieldRepeatCounts;
	
	public Hashtable getHtFileNameForIDROWMappingHash() {
		return htFileNameForIDROWMappingHash;
	}

	public String getHtFileNameForIDROWMappingHash(String formId) {
		if ( htFileNameForIDROWMappingHash.containsKey(formId))
		{
			return (String) htFileNameForIDROWMappingHash.get(formId);
		}
		else
		{
			
			return "";
		}
		
		
	}

	
	public void setHtFileNameForIDROWMappingHash(
			Hashtable htFileNameForIDROWMappingHash) {
		this.htFileNameForIDROWMappingHash = htFileNameForIDROWMappingHash;
	}

 	
	public void setHtFileNameForIDROWMappingHash(String formId, String fileNameForIDROWMappingHash) {
		this.htFileNameForIDROWMappingHash.put( formId, fileNameForIDROWMappingHash);
	}
	
 
	public Hashtable getHtSummaryHolder() {
		return htSummaryHolder;
	}

	public void setHtSummaryHolder(Hashtable htSummaryHolder) {
		this.htSummaryHolder = htSummaryHolder;
	}
	

	public void setHtSummaryHolder(String formId, SummaryHolder summaryHolder) {
		this.htSummaryHolder.put( formId, summaryHolder);
	}

	
	public SummaryHolder getHtSummaryHolder(String formId) 
	{
		SummaryHolder sd = new SummaryHolder();
	
		if (htSummaryHolder.containsKey(formId))	
		{
			sd  = (SummaryHolder) htSummaryHolder.get(formId);
		}
	 
		return sd;
		
		 
	}

	public Hashtable getHtFormData() {
		return htFormData;
	}
	
	public ArrayList getHtFormData(String formId) {
		ArrayList arData = new ArrayList();
	
		if (htFormData.containsKey(formId))	
		{
			arData  = (ArrayList) htFormData.get(formId);
		}
		
		
		
		return arData;
	}

	public void setHtFormData(Hashtable htFormData) {
		this.htFormData = htFormData;
	}
	
	public void setHtFormData(String formId, ArrayList datalist) {
		this.htFormData.put(formId , datalist);
		if (datalist != null)
		{
			System.out.println("****adding htData for for form*" + formId + "* data count:" + datalist.size());
		}	
	}

	public Hashtable getHtFormMapSecIds() {
		return htFormMapSecIds;
	}
	
	public ArrayList getHtFormMapSecIds(String formId) {
		ArrayList arMapId = new ArrayList();
		
		if (htFormMapSecIds.containsKey(formId))
		{
			arMapId = (ArrayList) htFormMapSecIds.get(formId);   
			
		}
		return arMapId ;
	}

	public void setHtFormMapSecIds(Hashtable htFormMapSecIds) {
		this.htFormMapSecIds = htFormMapSecIds;
	}
	
	public void setHtFormMapSecIds(String formId, ArrayList arId) {
		this.htFormMapSecIds.put(formId , arId);
	}
	

	public Hashtable getHtFormMapSecNames() {
		return htFormMapSecNames;
	}
	
	public ArrayList getHtFormMapSecNames(String formId) {
		ArrayList arMap = new ArrayList();
		
		if (htFormMapSecNames.containsKey(formId))
		{
			
			arMap = (ArrayList) htFormMapSecNames.get(formId);
		}
		return arMap;
	}

	public void setHtFormMapSecNames(Hashtable htFormMapSecNames) {
		this.htFormMapSecNames = htFormMapSecNames;
	}

	public void setHtFormMapSecNames(String formId, ArrayList arNames) {
		this.htFormMapSecNames.put(formId, arNames);
	}

	public Hashtable getHtFormRepFldCols() {
		return htFormRepFldCols;
	}

	public void setHtFormRepFldCols(Hashtable htFormRepFldCols) {
		this.htFormRepFldCols = htFormRepFldCols;
	}
	
	public void setHtFormRepFldCols(String formId, ArrayList cols) {
		this.htFormRepFldCols.put(formId,cols);
	}

	public Hashtable getHtFormRepFldDisplayDataValue() {
		return htFormRepFldDisplayDataValue;
	}
	
	public ArrayList getHtFormRepFldDisplayDataValue(String formId) 
	{
		ArrayList arRep = new ArrayList();
		
		if (htFormRepFldDisplayDataValue.containsKey(formId))
		{
			arRep = (ArrayList) htFormRepFldDisplayDataValue.get(formId);
			
		}
		return arRep;
	}

	public void setHtFormRepFldDisplayDataValue(
			Hashtable htFormRepFldDisplayDataValue) {
		this.htFormRepFldDisplayDataValue = htFormRepFldDisplayDataValue;
	}
	
	public void setHtFormRepFldDisplayDataValue(
			String formId, ArrayList ardd) {
		this.htFormRepFldDisplayDataValue .put(formId,ardd);
	}

	public Hashtable getHtFormRepFldNamesSeq() {
		return htFormRepFldNamesSeq;
	}
	
	public ArrayList getHtFormRepFldNamesSeq(String formId) {
		
		ArrayList arSeq = new ArrayList();
		
		if (htFormRepFldNamesSeq.containsKey(formId))
		{
			arSeq = (ArrayList) htFormRepFldNamesSeq.get(formId);
			
		}
		return arSeq ;
	}
	
	public void setHtFormRepFldNamesSeq(Hashtable htFormRepFldNamesSeq) {
		this.htFormRepFldNamesSeq = htFormRepFldNamesSeq;
	}

	public void setHtFormRepFldNamesSeq(String formId, ArrayList arSeq) {
		this.htFormRepFldNamesSeq.put(formId,arSeq);
	}
    
	private void compileFormsIntoLinear()
	{
		ArrayList forms = new ArrayList();
		int count = 0;
		String formId = "";
		String dataFileName = "";
		
		Hashtable htLinearHash = new Hashtable();
		String rowKey = "";
		String prevFormId = "";
		forms = getArFormIds();
		count = forms.size();
		int totalCountForColumns = 0;
		int preFormFieldCount = 0;
		ArrayList formFldNames   = new ArrayList();
		String fldName = "";
		
//		Maintain an ArrayList of Keys, because enumeration of keys from Hashtable does not return a sorted result
		ArrayList arMainKeys = new ArrayList();
		ArrayList arForDispDataValues = new ArrayList();
		ArrayList arEmptyNodes = new ArrayList();
		
		
		int repCount = 0;
		int totalPrevFields = 2; //for ID and PATPROT
		int prefFieldDiff = 0;
		int emptyNodePosition = 0;
		
		
		//loop through all the forms
		for (int k=0; k < count; k++)
		{    
			formId = ((String) forms.get(k));
			formFldNames = getHtActualFormFieldNames(formId);
			ArrayList arReportFldNames = new ArrayList();
			 ArrayList arRepeatCounts = new ArrayList();
			 repCount = 0;
			arReportFldNames.addAll(formFldNames);
			
			
			setHtReportFormFieldNames(formId, arReportFldNames);
			arForDispDataValues.addAll(getHtFormFldDisplayDataValue(formId));
			 
			dataFileName = this.getHtFileNameForIDROWMappingHash(formId);
			
			Hashtable htTempFormData = new Hashtable();
			ArrayList arKeys = new ArrayList();	
			Hashtable htSerial = new Hashtable();
			
			//System.out.println("After this read file" + dataFileName);
			//read the data from the serialized file
			htSerial = (Hashtable) EJBUtil.readSerializedObject(dataFileName);
			
 
			
			htTempFormData  = (Hashtable) htSerial.get("data");
			arKeys = (ArrayList) htSerial.get("keyObj");
				
			if (k==0)
			{
				prevFormId = formId ;
			} else
			{
				prevFormId =  ((String) forms.get(k-1));  
				
				arRepeatCounts = getHtFormfieldRepeatCounts(prevFormId);
			}
			
			if (k > 0)
			{
			preFormFieldCount =   getHtFormfieldCountWithoutSplitRepeat(prevFormId);
			
			totalPrevFields = totalPrevFields + preFormFieldCount;
			
		//	System.out.println("preFormFieldCount" + prevFormId + "*" + preFormFieldCount);
			//System.out.println("preFormFieldCount" + prevFormId + "*totalPrevFields" + totalPrevFields);
			
			
			/////////////////
				if (k==1)
				{
					arEmptyNodes.add("-");
					arEmptyNodes.add("-");
					
									
				}	
			 
			
				 for (int arctr = 0; arctr < preFormFieldCount; arctr++)
				 {
					 if (arctr < arRepeatCounts.size())
					 {
						 repCount = Integer.parseInt((String) arRepeatCounts.get(arctr));
						 
					 }
					 else
					 {
						 repCount = 0;
						 
					 }
					 
					 arEmptyNodes.add(getEmptyRepeatStringForCount(repCount));
					 
					  
				 }
				 
				 
				 
				 
			}
			///////////////
			
			totalCountForColumns = totalCountForColumns + getHtFormfieldCount(formId);
			
			//System.out.println("htTempFormData" + htTempFormData.keys());
			
			if (htTempFormData != null)
			{
				//if its the first form, add it as it is to the linear hash
				if (k==0)
				{
					htLinearHash.putAll(htTempFormData);
					arMainKeys.addAll(arKeys);
					
					//System.out.println("htLinearHash" + htLinearHash);
				}
				else //for 2nd,3rd form ...and so on 
				{
					
					 
					//go through all the elements in this hash
					 for (int ctr = 0 ;ctr < arKeys.size() ; ctr++) {
						 rowKey = (String) arKeys.get(ctr);
						 
						 ArrayList arData = new ArrayList();
						 
						 if (htLinearHash.containsKey(rowKey))
						 {
							 arData = (ArrayList) htLinearHash.get(rowKey);
							 //System.out.println("*prev arData" + arData.size() + "rowKey" + rowKey);
							 
							 prefFieldDiff = totalPrevFields - arData.size();
							 
							 if (prefFieldDiff > 0) //add empty nodes for the difference 
							 {
								 emptyNodePosition =  arEmptyNodes.size() - prefFieldDiff;
								 //System.out.println("*emptyNodePosition" + emptyNodePosition + "arEmptyNodes.size()" + arEmptyNodes.size());
								 for (int diffctr=(emptyNodePosition); diffctr <arEmptyNodes.size(); diffctr++ )
								 {
									 //System.out.println("*diffctr" + diffctr + "arEmptyNodes.size()" + arEmptyNodes.size());
									 arData.add(arEmptyNodes.get(diffctr));
									 
								 }
								 
							 } //if there is a difference 
							 
						 }
						 else
						 { 
							 arData.addAll(arEmptyNodes);
							//add empty nodes to arData
							 
							 arMainKeys.add(rowKey);
						 }
						 
						 
						 
						 ArrayList arFormData = new ArrayList();
						 
						 arFormData = (ArrayList) htTempFormData.get(rowKey);
						 //remove the first 2 fields as they are ID AND FK_PATPROT
						 
						 arFormData.remove(0);
						 arFormData.remove(0);
						 
						 arData.addAll(arFormData);
						 htLinearHash.put(rowKey,arData);
						 
				     }

				}
				
				
			}
			
			
		
		}//end of forms
		
		setHtLinearData(htLinearHash);
		 //set the total count field
		 setFieldCount(totalCountForColumns);
		 
		  
		 
		//set report keys
		 
		 setReportDataKeys(arMainKeys);		 
		 setReportUseDispdataValue(arForDispDataValues);
		// System.out.println("arForDispDataValues )" + arForDispDataValues.size() );
	}
	
	
	public String getLinearXML()
	{
			StringBuffer sbXML = new StringBuffer();
			String rowKey = "";
			String value = "";
			ArrayList arDispDataValueFlag = new ArrayList();
			
			
			compileFormsIntoLinear();
			sbXML.append("<ROWSET>");
			
			int rowColCount = 0;
			int actualRowColCount = 0;
			int totalColCount = 0;
			int missingCol = 0 ,fldDataSeperatorPos=-1 ,repeatColCount = 0;

			totalColCount = getFieldCount();
	//		System.out.println("totalColCount in linear" + totalColCount);
			
			ArrayList forms = new ArrayList();
			String formId = "";
			int repColumnCount = 0;
			
			int count = 0;
			forms = getArFormIds();
			count = forms.size();
			
			ArrayList arKeys = new ArrayList();
			String fldUseDataValue = "";

			
			arKeys = getReportDataKeys();
			arDispDataValueFlag = getReportUseDispdataValue();
			int totalColumns = 0;
			String repColCounterString = "";
			
			StringBuffer formSumXML = new StringBuffer();

			//System.out.println("KEYS" + (getHtLinearData()).keys());
			//System.out.println("totalcol" + totalColCount);
			
			 for (int ctr = 0 ;ctr < arKeys.size() ; ctr++) {
				 rowKey = (String) arKeys.get(ctr);
				 
				 //System.out.println("rowKey" + rowKey);
				 
				 ArrayList arData = new ArrayList();
				 arData = getHtLinearData(rowKey);
				 String dispVal = "";
				 
				 sbXML.append("<ROW><COLUMNS>");
				 
				 //	remove first 2 columns, ID and PATPROT
					arData.remove(0);
					arData.remove(0);
				 
					rowColCount = arData.size();
					actualRowColCount = 0;
					
					//missingCol = totalColCount - rowColCount;
				
					//System.out.println("missingCol in linear" + missingCol);
					//System.out.println("totalColCount in linear" + totalColCount);
		 			//System.out.println("rowColCount in linear" + rowColCount);
					
				  for (int j = 0; j < rowColCount ; j++)  
				  {
					  
					StringTokenizer valSt = null;
					  
				  	value = (String )arData.get(j);
				  	//System.out.println("value" + value);
				  	
				  	if (! StringUtil.isEmpty(value))//if its not empty
				  	{
				  		
				  		if (value.indexOf("[VELSEP]")>=0)
				  		{
				  			value=StringUtil.replace(value,"[VELSEP]","~");
				  			valSt=new StringTokenizer(value,"~");
				  				
				  		}

				  		else
				  		{
				  			valSt=new StringTokenizer(value,"~");
				  		}
				  		
				  		 

				  		 if (valSt != null ) 
			  		        {
				  			while(valSt.hasMoreTokens())
				  			{
				  			 
			  		         dispVal=(String)valSt.nextToken();
			  				 dispVal=(dispVal==null)?"":dispVal;
			  				//System.out.println("arDispDataValueFlag" + arDispDataValueFlag.size() + "j" + j);
				  		
						  		fldUseDataValue = (String) arDispDataValueFlag.get(j)	;
								if (StringUtil.isEmpty(fldUseDataValue))
								{
									fldUseDataValue = "0";
								}
								
								fldDataSeperatorPos = dispVal.indexOf("[VELSEP1]");
							
							if (fldDataSeperatorPos >0){
								if (fldUseDataValue.equals("0"))	
								{
									dispVal=dispVal.substring(0,fldDataSeperatorPos);
								}
								else
								{
									dispVal=dispVal.substring(fldDataSeperatorPos + 9);
								}	
							}	
							
							dispVal=StringUtil.replace(dispVal,"[VELCOMMA]",","); //!!! finally we got the value for regular section field !!!	
							sbXML.append("<COLUMN>" + StringUtil.htmlEncode(dispVal) + "</COLUMN>");
							actualRowColCount = actualRowColCount +1;
				  				}
			  		       }
					  	
				  	} //if its not empty
					else
					{
						dispVal="-";	
						sbXML.append("<COLUMN>" + StringUtil.htmlEncode(dispVal) + "</COLUMN>");
						actualRowColCount = actualRowColCount +1;
				  	}
				  	
				  	
				  }
				  
				  
				  //if the arrayList size is less than the total field count, add the missing '-' at the end. This is the last form for which there 
					//is no data for the form
				  
				  missingCol = totalColCount - actualRowColCount;
					
			 
				
				   
				  
					if ( missingCol > 0)
					{
					 for (int i = 0; i < missingCol ; i++)  
					  {
					  	sbXML.append("<COLUMN>-</COLUMN>"); 
					  }
					}	
				  
				 sbXML.append("</COLUMNS></ROW>");				 
			} //end of enumeration
			
			
			//			add forms mapping
			sbXML.append("<FORM_MAP><COLUMN_NAMES>");
			for (int k=0; k < count; k++)
			{
				ArrayList arReportColumns = new ArrayList();
				ArrayList arRepeatColNumbers = new ArrayList();
				
				formId = ((String) forms.get(k));
				
				//sbReportXML.append("<NAME  formpk =\""+ formId  +"\" totalcol= \""+ getHtFormFieldMaxCounter(formId )+"\">"+ StringUtil.htmlEncode((String)formNames.get(k))+"</NAME>");
				
				formSumXML.append(getSummaryFieldsXML(formId));   
				
				arReportColumns = getHtReportFormFieldNames(formId); 
				repColumnCount = arReportColumns.size();
				
				//iterate through columns and create form report column XML
				arRepeatColNumbers = getHtFormfieldRepeatCounts(  formId);
				for (int j = 0; j <repColumnCount ;j++)
				{
					
					repeatColCount = Integer.parseInt((String) arRepeatColNumbers.get(j));
					
					//System.out.println("repeatColCount" + repeatColCount);
					
					if (repeatColCount >= 1)
						{
							repColCounterString = "_1";
						}
					else
					{
						repColCounterString = "";
					}
					
					sbXML.append("<NAME   formpk = \""+formId+"\">"+ StringUtil.htmlEncode((String) arReportColumns.get(j))+ repColCounterString +"</NAME>");
					
					totalColumns = totalColumns + 1;
					
					
					if (repeatColCount >= 1 )
					{
						for (int rct = 0 ;rct <  repeatColCount;rct++ )
							{
							 	totalColumns = totalColumns + 1;
							 	
							 	repColCounterString = "_" + (rct+2);
							 	
								sbXML.append("<NAME   formpk = \""+formId+"\">"+ StringUtil.htmlEncode((String) arReportColumns.get(j)) + repColCounterString +"</NAME>");
							}
						
					}
					
				}
				
				// create form report column mapping
					 
			}
			
			
			sbXML.append("</COLUMN_NAMES><TOTALCOLS>"+ totalColumns +"</TOTALCOLS></FORM_MAP>");
			sbXML.append("<SUMMARY");
			if (StringUtil.isEmpty(formSumXML.toString()))
			{
				sbXML.append(" summaryAvailable = \"0\"");
			}
			else
			{
				sbXML.append(" summaryAvailable = \"1\"");
			}
			sbXML.append(">" + formSumXML.toString());
			
			sbXML.append("</SUMMARY>");
			sbXML.append("</ROWSET>");
			return  sbXML.toString();
			
	}
	
	public String getReportXML()
	{
		StringBuffer sbReportXML = new StringBuffer(); 
		ArrayList forms = new ArrayList();
		ArrayList formNames = new ArrayList();
		int count = 0;
		String formId = "";
		StringBuffer sbFormReportColumn = new StringBuffer();
		StringBuffer formSumXML = new StringBuffer();
		int repColumnCount = 0;
		
		sbReportXML.append("<ROWSET>");
		
		//iterate through form list
				 	
		forms = getArFormIds();
		formNames = getArFormNames();
		
		count = forms.size();
		
		int finalcolcounter = 0;
		
		//add forms columns
		for (int k=0; k < count; k++)
		{
			formId = ((String) forms.get(k));
			
			//System.out.println("get form xml for: " + formId);
			
			sbReportXML.append(getFieldXML(formId ) );
		
		}
		//add forms mapping
		sbReportXML.append("<FORM_MAP>");
		for (int k=0; k < count; k++)
		{
			ArrayList arReportColumns = new ArrayList();
			formId = ((String) forms.get(k));
			
			sbReportXML.append("<NAME  formpk =\""+ formId  +"\" totalcol= \""+ getHtFormFieldMaxCounter(formId )+"\">"+ StringUtil.htmlEncode((String)formNames.get(k))+"</NAME>");
			
			formSumXML.append(getSummaryFieldsXML(formId));
			
			arReportColumns = getHtReportFormFieldNames(formId); 
			repColumnCount = arReportColumns.size();
			
			//iterate through columns and create form report column XML
			
			for (int j = 0; j <repColumnCount ;j++)
			{
				finalcolcounter = finalcolcounter  + 1;
				sbFormReportColumn.append("<NAME colcounter= \""+finalcolcounter+"\" formpk = \""+formId+"\">"+ StringUtil.htmlEncode((String) arReportColumns.get(j))+"</NAME>");
				
			}
			
			// create form report column mapping
				 
		}
		sbReportXML.append("</FORM_MAP>");
		//append report columns
		
		sbReportXML.append("<COLUMN_NAMES>");
		
		sbReportXML.append(sbFormReportColumn);
		
		sbReportXML.append("</COLUMN_NAMES>");
		
		//get report field count, each repeat section will be counted as one field 
		sbReportXML.append("<TOTALCOL>"+getFieldCount()+"</TOTALCOL>");
		
		sbReportXML.append("<SUMMARY");

		if (StringUtil.isEmpty(formSumXML.toString()))
		{
			sbReportXML.append(" summaryAvailable = \"0\"");
		}
		else
		{
			sbReportXML.append(" summaryAvailable = \"1\"");
		}
	sbReportXML.append(">" + formSumXML.toString());
			
		sbReportXML.append("</SUMMARY>");
		
		sbReportXML.append("</ROWSET>");
		
		return sbReportXML.toString();
		
	}
	
	public String getSummaryFieldsXML(String formId)
	{
		StringBuffer sbXml = new StringBuffer();
		SummaryHolder sh = new SummaryHolder();
		int countSummaryField = 0;
		ArrayList arActualFieldNames = new ArrayList();
		
		arActualFieldNames = getHtActualFormFieldNames(formId);
		
		 
		
		sh = this.getHtSummaryHolder(formId);	
		
		for (Enumeration e = (sh.getHtHolders()).keys() ; e.hasMoreElements() ;) 
	    {
			 SummaryHolder sd = new SummaryHolder();
	         String keyName = (String)e.nextElement();
	         countSummaryField++;
	         
	         sd = (SummaryHolder) sh.getHtHolders().get( keyName);
	         
	         if (sd != null)
	         {
	        	    
	        	 sbXml.append("<SUMCOL><NAME>" + StringUtil.htmlEncode((String)arActualFieldNames.get(Integer.parseInt(keyName))) + "</NAME>" );

	        	 if (sd.getInfoType().equals("sum") || sd.getInfoType().equals("both"))
	        	 {
	        	 	sbXml.append("<CALC min=\"" + sd.getMinValue() + "\" max=\"" + sd.getMaxValue() + "\" median=\"" + sd.getMedianValue() + " \" average=\"" + sd.getAverageValue() + "\" " );

		        	 	if(!sd.getInfoType().equals("both"))
		      			{
		      				sbXml.append("countPer=\"0\"");
		      			}	
		      			else
		      			{
		      				sbXml.append("countPer=\"1\"");
		      			}	
		        	 	sbXml.append("/>"); //close CALC tag 
		        	 	
	        	    }	


	        	  	if (sd.getInfoType().equals("per") || sd.getInfoType().equals("both"))
	        	      	{
	        	   	     /*if(!sd.getInfoType().equals("both"))
	        	  			{		
	        	  			
	        	  			sbXml.append("<CALC>N/A</CALC>");
	        	  		
	        	    			}*/
	        	      		sbXml.append("<RESPCOUNTS>"); 
	        	      		for (Enumeration p = (sd.getHtValueCounts()).keys() ; p.hasMoreElements() ;) 
	        	      			{
	        	      				String dispValResp = "";
	        	      				dispValResp = (String)p.nextElement();
	        	      				
	        	      				sbXml.append("<RESPCOUNT dispValResp=\""+ StringUtil.htmlEncode(dispValResp) +"\" dispCount=\""+ sd.getHtValueCounts(dispValResp)+"\" dispValPer=\""+ String.valueOf(sd.getHtValueCountPercentage(dispValResp))+"\"  />" ); 
	        	      				
	        	      			}
	        	      		sbXml.append("</RESPCOUNTS>"); 
	        	      		
	        	      	}	
	        	  	sbXml.append("</SUMCOL>");		
	        	 
	         } // end if for sd!=null
			
	    }//end of for
		
		 
		return sbXml.toString();
	}
	

	
	public String getFieldXML(String formId)
	{
		StringBuffer sbXML = new StringBuffer();
		int rows = 0;
		
		//get the datalist arraylist for this form
		
		ArrayList arDataList = new ArrayList();
		ArrayList fieldTypes = new ArrayList();
		ArrayList fieldDisplayDataValue = new ArrayList();
		ArrayList fldWidth = new ArrayList();
		ArrayList repFldNames = new ArrayList();
		ArrayList mapSecNames = new ArrayList();
		ArrayList mapSecIds = new ArrayList();
		ArrayList repFieldDisplayDataValue = new ArrayList();
		ArrayList formFldNames   = new ArrayList();
		ArrayList repFormFldNamesSeq   = new ArrayList();
		
		arDataList = getHtFormData(formId);
		fieldTypes  = getHtFormFieldTypes(formId);
		fieldDisplayDataValue  = getHtFormFldDisplayDataValue(formId);
		fldWidth = getHtFormfldWidth(formId);
		repFldNames		= getHtFormRepFldNames(formId);
		mapSecNames = getHtFormMapSecNames(formId);
		mapSecIds = getHtFormMapSecIds(formId);
		repFieldDisplayDataValue = getHtFormRepFldDisplayDataValue(formId);
		formFldNames = getHtActualFormFieldNames(formId);
		repFormFldNamesSeq = getHtFormRepFldNamesSeq(formId);
		                 
		String fieldTypeStr = "";
		String fldUseDataValue = "";
		
		rows = arDataList.size();
		int formtotalcolcount = 0;
		int formcolcounter = 0;
		int formNetColumns = 0;
		
		String fldWidthStr = "";
		int maxTokens=0,tempCountToken=0,fldDataSeperatorPos=-1;
		StringBuffer repcolname = new StringBuffer();
		
		int currentRow = 0;
		
		String fldName = "";
		ArrayList arReportFldNames = new ArrayList();
		//System.out.println("*************Got datalist :" + arDataList.size());
		
		
		
		IdDataCount idCount = new IdDataCount(); // the object will hold data mapping count for each ID field
		int curCountForID = 0; 
		   
		
		if (arDataList != null && rows > 0)
		{
		
			 
			for (int i=0; i< rows; i++) // iterate for each row
			{  
				ArrayList tempList = (ArrayList) arDataList.get(i); //get each row
				ArrayList arRepeatFieldToken = new ArrayList();
				
				StringBuffer sbRow = new StringBuffer();
				StringBuffer colXML = new StringBuffer (); 
				StringBuffer repColXML = new StringBuffer ();
				
				currentRow = currentRow +1; 
				 
				String id = ""; // stores patient/study/account id in database or related primary key in core
				
				if (tempList.size()==0)
					{
						continue;
					}
					
				  id = (String)tempList.get(0); //patient/study/account id in database or related primary key in core
				  
				  curCountForID = idCount.getArCount(id);//if there was no row added for the ID, it will return 0;
				  curCountForID++; //increment the current count for ID 
				  
		
				  formtotalcolcount  = tempList.size();
				  
				  //get the formcolcounter from the master list.
				  
				  formcolcounter =  getLastFormFieldCount();
				  
				 
				  maxTokens=0 ;
				    
				   for (int j=2; j<formtotalcolcount; j++) // first 2 columns are ID and PATPROT
				  {
					   String value = "";
					   value = (String)tempList.get(j);
					    
					   tempCountToken=0;
					   fldDataSeperatorPos = -1;
					   
					   fieldTypeStr = (String) fieldTypes.get(j-2);	
					  	
					  	fldUseDataValue = (String) fieldDisplayDataValue.get(j-2)	;
					  	if (StringUtil.isEmpty(fldUseDataValue))
					  	{
					  		fldUseDataValue = "0";
					  	}
					  	
					  	fldWidthStr = (String)fldWidth.get(j-2);
						if (StringUtil.isEmpty(fldWidthStr))
							{
							 fldWidthStr = "15";
							}
					
						if (value!=null){
							if (value.indexOf("[VELSEP]")>=0){
								value=StringUtil.replace(value,"[VELSEP]","~");
							
							StringTokenizer valSt=new StringTokenizer(value,"~");
								
							tempCountToken=valSt.countTokens();
							 
							arRepeatFieldToken.add(valSt); //add here for later processing
							
							//Rlog.fatal("dynrep", "ReportDataHolder:getFieldXML  field tempCountToken" + tempCountToken + "maxTokens" +maxTokens );
							   
							
							if (tempCountToken>maxTokens)
								{
									maxTokens=tempCountToken;
								}
									continue;
								
							 
							}
							
							
							}
						else
						{
							value="-";
						}
						
						fldDataSeperatorPos = value.indexOf("[VELSEP1]");
						
						if (fldDataSeperatorPos >=0){
							if (fldUseDataValue.equals("0"))	
							{
								value=value.substring(0,fldDataSeperatorPos);
							}
							else
							{
								value=value.substring(fldDataSeperatorPos + 9);
							}	
						}	
						value=StringUtil.replace(value,"[VELCOMMA]",","); //!!! finally we got the value for regular section field !!!
							
						value = StringUtil.htmlEncode(	value); //escape xml restricted char
						
						formcolcounter = formcolcounter + 1;
						
						if (i==0) //only for first row of the form
						{
							incrementFieldCount(); //increase the report field count
							
							//incrementLastFormWithDataFieldCount();
							
							formNetColumns = formNetColumns  + 1; 
							
							fldName = (String) formFldNames.get(j-2);
							arReportFldNames.add(fldName);
							
							
						}	
						
						
						colXML.append("<COLUMN rep=\"no\"  colnum = \"" + formcolcounter + "\">" + value + "</COLUMN>");
											
			  } //for each column
				   
				  // now we have XML for all non-repeat columns
				   
				   
				   
			   ////////////////////now lets worry about repeat fields
				   //Rlog.fatal("dynrep", "SQL in ReportDataHolder:getFieldXML repFldNames" + repFldNames.size());
				   //Rlog.fatal("dynrep", "SQL in ReportDataHolder:getFieldXML  mapSecNames" + mapSecNames.size());
				   
				  if (repFldNames.size()>0)
				   {
					  int colCounter=0,counter=0,toCount=0,currCount=0;
				     String currSec="",prevSec="";
				     String dispVal="";
				     StringTokenizer tempSt;
				     

				   while (colCounter<repFldNames.size())
				   {
				    if (mapSecNames.size()>0)
				    currSec=(String)mapSecIds.get(colCounter); 
				    //do the processing here to display data for the repeated fields
				    toCount=(colCounter);

				    //Rlog.fatal("dynrep", "SQL in ReportDataHolder:getFieldXML  currSec" + currSec + "prevSec" + prevSec);
				    //Rlog.fatal("dynrep", "SQL in ReportDataHolder:getFieldXML  colCounter" + colCounter);
				    
				   if ((colCounter>0) && !currSec.equals(prevSec)) //when the section is changed
				   {
					   repColXML = new StringBuffer();
					   formcolcounter = formcolcounter + 1;
					   
					   if (i==0) //only for first row of the form
						{
						   incrementFieldCount();
						   //incrementLastFormWithDataFieldCount();
						   
						   formNetColumns = formNetColumns  + 1;
						   
						   fldName = (String)mapSecNames.get(colCounter-1); //section name will be the field name in this case
						   arReportFldNames.add(fldName);
						   
							
						}
					   
					 

					   
					   repColXML.append("<COLUMN rep=\"yes\"   colnum = \""+ formcolcounter +"\"><REPROW>" + repcolname + "</REPROW>");
					   
					 if (maxTokens>0)
				   {
				   for (int tCount=1;tCount<=maxTokens;tCount++){
				   
				   counter=0;

				   repColXML.append("<REPROW>");
				   
					   while ((counter + currCount )<toCount){
					      dispVal="";
					      tempSt=((StringTokenizer)arRepeatFieldToken.get(counter+currCount));
					         if (tempSt.hasMoreTokens()) dispVal=(String)tempSt.nextToken();
					         dispVal=(dispVal==null)?"":dispVal;
					         
					         fldUseDataValue = (String) repFieldDisplayDataValue.get(counter+currCount);
					         fldDataSeperatorPos = dispVal.indexOf("[VELSEP1]");
					   	
					   		if (fldDataSeperatorPos >=0){
					   			if (fldUseDataValue.equals("0"))	
					   			{
					   				dispVal = dispVal.substring(0,fldDataSeperatorPos);
					   			}
					   			else
					   			{
					   				dispVal = dispVal.substring(fldDataSeperatorPos + 9);
					   			}	
					   		}	
					         dispVal = StringUtil.replace(dispVal,"[VELCOMMA]",",");
					         			         
					         dispVal = StringUtil.htmlEncode(dispVal);
					         repColXML.append("<REPCOL>" + dispVal + "</REPCOL>");
					         counter++;
					    }
				   repColXML.append("</REPROW>");
				    }
				    currCount=toCount;
				    //reset repcolname  
				    
				    repColXML.append("</COLUMN>");
				    
				    colXML.append(repColXML); // add repeat column xml to to column xml
				    
				    repcolname = new StringBuffer();
				    }
//				   end processing here 
				   
				   
				   repcolname = repcolname.append("<REPCOLNAME fldwidth=\""+fldWidthStr+"\">" + StringUtil.htmlEncode((String)repFldNames.get(colCounter)) + "</REPCOLNAME>");
				   
			} //prevsection!= new section
				   else{
					   repcolname = repcolname.append("<REPCOLNAME fldwidth=\""+fldWidthStr+"\">" + StringUtil.htmlEncode((String)repFldNames.get(colCounter)) + "</REPCOLNAME>");
				  }
				    colCounter++;
				    prevSec=currSec;
			   }

				    //do this processing to display the elements of last section to the end of list
				   
				   //Rlog.fatal("dynrep", "ReportDataHolder:getFieldXML  for last section toCount+1" + (toCount+1));
				   //Rlog.fatal("dynrep", "ReportDataHolder:getFieldXML  for last section maxTokens" + maxTokens);
				   
				   if ((toCount+1) == repFldNames.size())
				   {
					 
					  
				   	if (maxTokens>0)
				   	{
				   		
				   	  repColXML = new StringBuffer();
					  formcolcounter = formcolcounter + 1;
					   
				   		
				   	 if (i==0) //only for first row of the form
						{
						   incrementFieldCount();
						  // incrementLastFormWithDataFieldCount();
						   formNetColumns = formNetColumns  + 1;
						
						   //System.out.println("colCounter" + colCounter);
						   //System.out.println("mapSecNames" + mapSecNames.size());
						   
						   fldName = (String)mapSecNames.get(toCount); //section name will be the field name in this case
						   arReportFldNames.add(fldName);
						  
							   
						} 
				   	 	 
					   
					   repColXML.append("<COLUMN rep=\"yes\"    colnum = \""+ formcolcounter +"\"><REPROW>" + repcolname + "</REPROW>");
					   
					   
				   	for (int tCount=1;tCount<=maxTokens;tCount++)
				   	{
 
				   		counter=0;
				   	
				   		repColXML.append("<REPROW>");
				   		
				   		while ((counter + currCount )<toCount+1)
				   		{
				   	   dispVal="";
				   	   tempSt=((StringTokenizer)arRepeatFieldToken.get(counter+currCount));
				   	   
				   	   	fldUseDataValue = (String) repFieldDisplayDataValue.get(counter+currCount);
				   	   	

				   	      if (tempSt.hasMoreTokens()) dispVal=(String)tempSt.nextToken();
				   	      dispVal=(dispVal==null)?"":dispVal;
				        
				   	         fldDataSeperatorPos = dispVal.indexOf("[VELSEP1]");
				   		
				   			if (fldDataSeperatorPos >=0){
				   				if (fldUseDataValue.equals("0"))	
				   				{
				   					dispVal = dispVal.substring(0,fldDataSeperatorPos);
				   				}
				   				else
				   				{
				   					dispVal = dispVal.substring(fldDataSeperatorPos + 9);
				   				}	
				   			}	
				   	      dispVal = StringUtil.replace(dispVal,"[VELCOMMA]",",");
				   	     dispVal = StringUtil.htmlEncode(dispVal);
				   	   repColXML.append("<REPCOL>" + dispVal + "</REPCOL>");
				    counter++;
				   	 }
				   		repColXML.append("</REPROW>");
				   		
				    }
				   	 //currCount=toCount;
				    repColXML.append("</COLUMN>");
				    colXML.append(repColXML); // add repeat column xml to to column xml
				   }
				   	repcolname = new StringBuffer();
				   	// 	End for do this processing to display the elements of last section to the end of list
				   	
				  
				   } // last section 
				}  //repeat sections
			   
			 /////////////////////////////////////////end of repeat fields
				//add row XML:
				  
				  sbRow.append("<ROW formpk =\""+ formId +"\" ID=\""+ id +"\" rownum= \""+curCountForID+"\" ><ID>"+id+"</ID>");
				  sbRow.append("<ROWNUM>"+curCountForID+"</ROWNUM><FORMPK>"+ formId +"</FORMPK>");
				  
				  sbRow.append("<COLUMNS maxcol=\"" + formcolcounter + "\" >");	
				  
				  sbRow.append(colXML + "</COLUMNS></ROW>");
				  
				  //set the value for new count for the ID
				  
				  idCount.setArCount(id, curCountForID); 
				  
				  sbXML.append(sbRow);
				   
				  
			} //for each row
			
			//increment the field count of the report
			
			addToLastFormFieldCount(formNetColumns);
			
			//System.out.println("data foundcurrent field count:" + getFieldCount());
			
			
			setHtFormFieldMaxCounter(formId, String.valueOf(formcolcounter)); // set the max column counter for the form
			
			// add the report field arraylist to hashtable
			
			setHtReportFormFieldNames(formId, arReportFldNames);
			
		} //arDataList != null
		else if (rows <= 0)
		{
			//create columns data even if there are no rows
			int fldCount = 0;
			
			fldCount = formFldNames.size();
			String name = "";
			int index = 0;
			String prevSecId = "";
			String mapSecId = "";
			formNetColumns = 0;
			
			formcolcounter =  getLastFormFieldCount(); 
			
			//System.out.println("no data formID" + formId + "getLastFormFieldCount " + formcolcounter );
			//System.out.println("no data formID" + formId + "form" + fldCount );
			
			for (int ct = 0; ct < fldCount; ct++) //iterate through field list
			{
				name = (String) formFldNames.get(ct);
				
				//check if exists in repFormFlds
				
				index= (repFormFldNamesSeq.indexOf(new Integer(ct))); //if the field's current sequence exists in the repFormFldNamesSeq, then its a repeat field 
				   if (index>=0) 
				   {
				   	continue;
				   }
				   else
				   {
					  incrementFieldCount();
					  formcolcounter = formcolcounter +  1;
					  formNetColumns = formNetColumns  + 1;
				   	  arReportFldNames.add(name); 
				   }	
			}
			//for repeat fields
			if (mapSecIds!=null && mapSecIds.size()>0){
				for (int count=0;count<mapSecIds.size();count++){
					mapSecId=(String)mapSecIds.get(count);
					if (StringUtil.isEmpty(mapSecId))
					{
						mapSecId = "";
					}
										
					if (mapSecId.length()>0){
				    if ((mapSecId).equals(prevSecId))
				    {
				    	continue;
				    }else{ 
				    	name = (String) mapSecNames.get(count); 
				    	incrementFieldCount();
						formNetColumns = formNetColumns  + 1;
					   	arReportFldNames.add(name);
					   	formcolcounter = formcolcounter +  1;
				    	
				    	
				    	
					}
					prevSecId=(String)mapSecIds.get(count);
				} else {
					name="";
				}
				}
				
			}
			
			//System.out.println("no data found formNetColumns:" + formNetColumns);
						
			addToLastFormFieldCount(formNetColumns);
			
			//System.out.println("no data foundcurrent field count:" + getLastFormFieldCount());
			
			setHtFormFieldMaxCounter(formId, String.valueOf(formcolcounter)); // set the max column counter for the form
			
			// add the report field arraylist to hashtable
			
			setHtReportFormFieldNames(formId, arReportFldNames);
			
			
		} // else for if dta list is null
		
		return sbXML.toString();
		
	}

	

	public ArrayList getArFormIds() {
		return arFormIds;
	}

	public void setArFormIds(ArrayList arFormIds) {
		this.arFormIds = arFormIds;
	}
	
	public void setArFormIds(String arFormId) {
		this.arFormIds.add(arFormId);
	}


	public ArrayList getArFormNames() {
		return arFormNames;
	}

	public void setArFormNames(ArrayList arFormNames) {
		this.arFormNames = arFormNames;
	}
	
	public void setArFormNames(String arFormName) {
		this.arFormNames.add(arFormName);
	}


	public int getFieldCount() {
		return fieldCount;
	}

	public void setFieldCount(int fieldCount) {
		this.fieldCount = fieldCount;
	}
	
	
	public void incrementFieldCount() {
		this.fieldCount = this.fieldCount + 1;
	}

	public Hashtable getHtFormFieldTypes() {
		return htFormFieldTypes;
	}
	
	public ArrayList getHtFormFieldTypes(String formId) {
		ArrayList arfldtypes = new ArrayList(); 
		
		if (htFormFieldTypes.containsKey(formId))
		{
			arfldtypes  = (ArrayList) htFormFieldTypes.get(formId);
		}
				
		return arfldtypes  ;
	}

	public void setHtFormFieldTypes(Hashtable htFormFieldTypes) {
		this.htFormFieldTypes = htFormFieldTypes;
	}
	
	public void setHtFormFieldTypes(String formId, ArrayList arFormFieldTypes) {
		this.htFormFieldTypes.put(formId, arFormFieldTypes);
	}


	public Hashtable getHtFormFldDisplayDataValue() {
		return htFormFldDisplayDataValue;
	}
	
	public ArrayList getHtFormFldDisplayDataValue(String formId) 
	{	
		ArrayList arDD = new ArrayList();
		
		if (htFormFldDisplayDataValue.containsKey(formId))
		{
			arDD = (ArrayList)htFormFldDisplayDataValue.get(formId);
		}
		
		return arDD;
	}


	public void setHtFormFldDisplayDataValue(Hashtable htFormFldDisplayDataValue) {
		this.htFormFldDisplayDataValue = htFormFldDisplayDataValue;
	}
	
	public void setHtFormFldDisplayDataValue(String formId, ArrayList arFormFldDisplayDataValue) {
		this.htFormFldDisplayDataValue.put(formId, arFormFldDisplayDataValue) ;
	}

	public Hashtable getHtFormfldWidth() {
		return htFormfldWidth;
	}

	public ArrayList getHtFormfldWidth(String formId) {
		
		ArrayList arWidth = new ArrayList();
		if (htFormfldWidth.containsKey(formId))
		{
			
			arWidth  = (ArrayList )htFormfldWidth.get(formId);
		}
		
		return arWidth ;
	}

	public void setHtFormfldWidth(Hashtable htFormfldWidth) {
		this.htFormfldWidth = htFormfldWidth;
	}

	public void setHtFormfldWidth(String formId, ArrayList arFormfldWidth) {
		this.htFormfldWidth.put(formId,arFormfldWidth);
	}

	
	public int getLastFormFieldCount() {
		return lastFormFieldCount;
	}

	public void setLastFormFieldCount(int lastFormFieldCount) {
		this.lastFormFieldCount = lastFormFieldCount;
	}
	
	public void addToLastFormFieldCount(int num) {
		this.lastFormFieldCount = this.lastFormFieldCount + num;
	}

	public Hashtable getHtFormRepFldNames() {
		return htFormRepFldNames;
	}
	
	public ArrayList getHtFormRepFldNames(String formId) {
		ArrayList arRep = new ArrayList();
		if (htFormRepFldNames.containsKey(formId))
		{
			arRep = (ArrayList) htFormRepFldNames.get(formId);
			
		}
		return arRep;
	}

	public void setHtFormRepFldNames(Hashtable htFormRepFldNames) {
		this.htFormRepFldNames = htFormRepFldNames;
	}
	
	public void setHtFormRepFldNames(String formId, ArrayList arFormRepFldNames) {
		this.htFormRepFldNames.put(formId, arFormRepFldNames);
	}

	public Hashtable getHtFormFieldMaxCounter() {
		return htFormFieldMaxCounter;
	}

	public String  getHtFormFieldMaxCounter(String formId) {
		String counter = "";
		
		if (htFormFieldMaxCounter.containsKey(formId))
		{
			counter = (String) htFormFieldMaxCounter.get(formId); 
		}
		
		return counter;
	}

	
	public void setHtFormFieldMaxCounter(Hashtable htFormFieldMaxCounter) {
		this.htFormFieldMaxCounter = htFormFieldMaxCounter;
	}
	
	public void setHtFormFieldMaxCounter(String formId, String counter) {
		this.htFormFieldMaxCounter.put(formId,counter);
	}

	public Hashtable getHtActualFormFieldNames() {
		return htActualFormFieldNames;
	}

	public void setHtActualFormFieldNames(Hashtable  htActualFormFieldNames) {
		this.htActualFormFieldNames = htActualFormFieldNames;
	}
	
	public void setHtActualFormFieldNames(String formId, ArrayList arActualFormFieldName) {
		this.htActualFormFieldNames.put(formId, arActualFormFieldName);
	}

	public ArrayList getHtActualFormFieldNames(String formId) {
		
		ArrayList arName = new ArrayList();
		
		if (htActualFormFieldNames.containsKey(formId))
		{
			arName = (ArrayList)htActualFormFieldNames.get(formId);
		}
		
		return arName ;
	}

	
	public Hashtable  getHtReportFormFieldNames() {
		return htReportFormFieldNames;
	}
		

	
	public ArrayList getHtReportFormFieldNames(String formId) {
		
		ArrayList arName = new ArrayList();
		
		if (htReportFormFieldNames.containsKey(formId))
		{
			arName = (ArrayList)htReportFormFieldNames.get(formId);
		}
		return arName ;
	}
	
	public void setHtReportFormFieldNames(Hashtable htReportFormFieldNames) {
		this.htReportFormFieldNames = htReportFormFieldNames;
	}
	
	public void setHtReportFormFieldNames(String formId, ArrayList arReportFormFieldName) {
		this.htReportFormFieldNames.put(formId, arReportFormFieldName);
	}

	public ReportDataHolder() {
		super();
		// TODO Auto-generated constructor stub
		htFormData = new Hashtable();
		htFormMapSecIds = new Hashtable();
		htFormMapSecNames = new Hashtable();
		htFormRepFldNamesSeq = new Hashtable();
		htFormRepFldDisplayDataValue = new Hashtable();
		htFormRepFldCols = new Hashtable();

		arFormIds = new ArrayList();
		arFormNames = new ArrayList();
		htActualFormFieldNames = new Hashtable();
		htReportFormFieldNames = new Hashtable();
		htFormFieldTypes = new Hashtable();
		htFormFldDisplayDataValue = new Hashtable();
		htFormfldWidth = new Hashtable();
		htFormRepFldNames = new Hashtable();
		htFormFieldMaxCounter = new Hashtable();
		htSummaryHolder= new Hashtable();
		htFileNameForIDROWMappingHash = new Hashtable();
		htFormfieldCount = new Hashtable();
		reportDataKeys = new ArrayList();
		reportUseDispdataValue = new ArrayList();
	 
		htFormfieldCountWithoutSplitRepeat = new Hashtable();
		htFormfieldRepeatCounts = new Hashtable(); 
		}

	public Hashtable getHtFormfieldCount() {
		return htFormfieldCount;
	}
	
	public int getHtFormfieldCount(String formId) {
		int count = 0;
		if (htFormfieldCount.containsKey(formId))
		{
			count = ((Integer)htFormfieldCount.get(formId)).intValue();
		}
		
		return count;
	}
	
	public int getHtFormfieldCountWithoutSplitRepeat(String formId) 
	{
		int count = 0;
		if (htFormfieldCountWithoutSplitRepeat.containsKey(formId))
		{
			count = ((Integer)htFormfieldCountWithoutSplitRepeat.get(formId)).intValue();
		}
		
		return count;
		
		
	}
	public void setHtFormfieldCount(Hashtable htFormfieldCount) {
		this.htFormfieldCount = htFormfieldCount;
	}

	public void setHtFormfieldCount(String formId, int count) {
		this.htFormfieldCount.put(formId, new Integer(count));
	}
	
	public void setHtFormfieldCount(String formId, ArrayList fields,ArrayList sectionRepeat) 
	{
		
		int count = 0;
		int repCount = 0;
		 
		for (int i =0;i < fields.size();i++)
			{
					repCount = 	Integer.parseInt((String)sectionRepeat.get(i));
					
					count = count + 1 + repCount;	//add extra fields for the repeated fields
					
					 
			}
		 
		this.htFormfieldCount.put(formId, new Integer(count));
		//System.out.println("formId..........." + count);	 
		setHtFormfieldCountWithoutSplitRepeat(formId,fields.size());
		setHtFormfieldRepeatCounts(formId,sectionRepeat);  
	}



	

	public Hashtable getHtLinearData() {
		return htLinearData;
	}

	public void setHtLinearData(Hashtable htLinearData) {
		this.htLinearData = htLinearData;
	}

	
	public ArrayList getHtLinearData(String key) {
		ArrayList row = new ArrayList();
		
		if (htLinearData.containsKey(key))
		{
			row = 	(ArrayList) htLinearData.get(key);
		}
		return row;	
	}

	public ArrayList getReportDataKeys() {
		return reportDataKeys;
	}

	public void setReportDataKeys(ArrayList reportDataKeys) {
		this.reportDataKeys = reportDataKeys;
	}

	public ArrayList getReportUseDispdataValue() {
		return reportUseDispdataValue;
	}

	public void setReportUseDispdataValue(ArrayList reportUseDispdataValue) {
		this.reportUseDispdataValue = reportUseDispdataValue;
	}

 

	public Hashtable getHtFormfieldCountWithoutSplitRepeat() {
		return htFormfieldCountWithoutSplitRepeat;
	}

	public void setHtFormfieldCountWithoutSplitRepeat(
			Hashtable htFormfieldCountWithoutSplitRepeat) {
		this.htFormfieldCountWithoutSplitRepeat = htFormfieldCountWithoutSplitRepeat;
	}
	

	public void setHtFormfieldCountWithoutSplitRepeat(
			String formId, int count) {
		this.htFormfieldCountWithoutSplitRepeat.put(formId, new Integer(count));
	}

	public Hashtable getHtFormfieldRepeatCounts() {
		return htFormfieldRepeatCounts;
	}

	public void setHtFormfieldRepeatCounts(Hashtable htFormfieldRepeatCounts) {
		this.htFormfieldRepeatCounts = htFormfieldRepeatCounts;
	}
		
	public void setHtFormfieldRepeatCounts(String formId, ArrayList mapFormfieldRepeatCounts) {
		this.htFormfieldRepeatCounts.put(formId,mapFormfieldRepeatCounts );
	}
	
	public ArrayList getHtFormfieldRepeatCounts(String formId) {
		ArrayList arRepeatCounts = new ArrayList();
		if (htFormfieldRepeatCounts.containsKey(formId))
		{
		  arRepeatCounts = (ArrayList) htFormfieldRepeatCounts.get(formId);
		}

		return arRepeatCounts;
	}
		

	private String getEmptyRepeatStringForCount(int count)
	{
		String str = "-";
		
		if (count > 0)
		{
		  for (int k = 0; k < count;k++)	
		{
			if (StringUtil.isEmpty(str))
			{
			 str = "-";
			}
			else
			{
				str = str +  "[VELSEP]-";
			}

		}
		}
		return str;
	}
}