 package com.velos.eres.web.dynrep.holder;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Description of the Class
 * 
 * @author vabrol
 * @created October 25, 2004
 */
/*
 * Modified by Sonia Abrol 01/25/05 Added new attributes pkTableName,
 * mainFilterColName, pkColName
 */
public class FieldContainer implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 3691044274557236023L;

    ArrayList fieldColId;

    ArrayList fieldDbId;

    ArrayList fieldName;

    ArrayList fieldDisplayName;

    ArrayList fieldSequence;

    ArrayList fieldFormat;

    ArrayList fieldType;

    ArrayList fieldWidth;

    ArrayList fieldStr;

    ArrayList fieldSectionId;

    ArrayList fieldSectionName;

    /*
     * contains all the fields available in forms
     */
    ArrayList fieldList;

    ArrayList fieldNameList;

    ArrayList fieldTypeList;

    ArrayList fieldPkList;


	 ArrayList fieldRepeatNumber;

    /* will store the field types without the repeat fields */
    private ArrayList fieldTypesNoRepeatFields;
    private ArrayList mapColSecRepNo;
    /*
     * This arrayList to preserve the fields selected which were available when
     * the report was modified in modify mode.
     */
    ArrayList fieldPrevColId;

    /** Table name of the Primary Key for the form data - forms/core lookup */
    private String pkTableName;

    /** Main Filter Column Name for the form data - forms/core lookup */

    private String mainFilterColName;

    /** Primary Key Column Name for the form data - forms/core lookup */

    private String pkColName;

    /**
     * The column name for pk_study. A value in this field indicates that the
     * data should be checked for study access rights
     */
    private String mapStudyColumn;

    /**
     * Flag to Use 'Unique Field ID' as Display Name. possible values:
     * 0:true,1:false
     */
    private ArrayList useUniqueIds;

    /**
     * Flag to Display Data value of multiple choice fields. possible values:
     * 0:true,1:false (default, display value will be shown)
     */

    private ArrayList useDataValues;
    
    /**
     *Flag to calculate summary information for this field - min/max/median/avg. Possible values-0:do not calculate, 1:calculate
     */
    private ArrayList calcSums;


    /**
     *Flag to calculate count/percentage information. Possible values-0:do not calculate,1:calculate
     */
    private ArrayList calcPers;

    /**
     *Stores field keyword
     */
    private ArrayList fieldKeyword;
    
    /**
     * Constructor for the FieldContainer object
     */
    public FieldContainer() {
        fieldColId = new ArrayList();
        fieldDbId = new ArrayList();
        fieldName = new ArrayList();
        fieldDisplayName = new ArrayList();
        fieldSequence = new ArrayList();
        fieldFormat = new ArrayList();
        fieldType = new ArrayList();
        fieldWidth = new ArrayList();
        fieldStr = new ArrayList();
        fieldSectionId = new ArrayList();
        fieldSectionName = new ArrayList();
        fieldList = new ArrayList();
        fieldNameList = new ArrayList();
        fieldTypeList = new ArrayList();
        fieldPrevColId = new ArrayList();
        pkTableName = "";
        mainFilterColName = "";
        pkColName = "";

        mapStudyColumn = "";
        useUniqueIds = new ArrayList();
        useDataValues = new ArrayList();
        mapStudyColumn = "";
        fieldTypesNoRepeatFields = new ArrayList();
        calcSums = new ArrayList();
        calcPers  = new ArrayList();
        mapColSecRepNo = new ArrayList();
        fieldRepeatNumber = new ArrayList();
        fieldKeyword  = new ArrayList();
    }

    
    
    public ArrayList getCalcPers() {
		return calcPers;
	}



	public void setCalcPers(ArrayList calcPers) {
		this.calcPers = calcPers;
	}



	public ArrayList getCalcSums() {
		return calcSums;
	}



	public void setCalcSums(ArrayList calcSums) {
		this.calcSums = calcSums;
	}

	public void setCalcSums(String calcSum) {
		this.calcSums.add(calcSum);
	}
	
	
public void setCalcPers(String calcPer) {
		this.calcPers.add(calcPer);
	}

	/**
     * Returns the value of fieldTypesNoRepeatFields.
     */
    public ArrayList getFieldTypesNoRepeatFields() {
        return fieldTypesNoRepeatFields;
    }

    /**
     * Sets the value of fieldTypesNoRepeatFields.
     * 
     * @param fieldTypesNoRepeatFields
     *            The value to assign fieldTypesNoRepeatFields.
     */
    public void setFieldTypesNoRepeatFields(ArrayList fieldTypesNoRepeatFields) {
        this.fieldTypesNoRepeatFields = fieldTypesNoRepeatFields;
    }

    /**
     * Sets the value of fieldTypesNoRepeatFields.
     * 
     * @param fieldTypesNoRepeatFields
     *            The value to assign fieldTypesNoRepeatFields.
     */
    public void setFieldTypesNoRepeatFields(String fieldTypesNoRepeatField) {
        this.fieldTypesNoRepeatFields.add(fieldTypesNoRepeatField);
    }

    /**
     * Returns the value of fieldColId.
     * 
     * @return The fieldColId value
     */
    public ArrayList getFieldColId() {
        return fieldColId;
    }

    /**
     * Sets the value of fieldColId.
     * 
     * @param fieldColId
     *            The value to assign fieldColId.
     */
    public void setFieldColId(ArrayList fieldColId) {
        this.fieldColId = fieldColId;
    }

    /**
     * Sets the value of fieldColId.
     * 
     * @param fieldColId
     *            The value to add tp fieldColId.
     */
    public void setFieldColId(String fieldColId) {
        this.fieldColId.add(fieldColId);
    }

    /**
     * Returns the value of fieldDbId.
     * 
     * @return The fieldDbId value
     */
    public ArrayList getFieldDbId() {
        return fieldDbId;
    }

    /**
     * Sets the value of fieldDbId.
     * 
     * @param fieldDbId
     *            The value to assign fieldDbId.
     */
    public void setFieldDbId(ArrayList fieldDbId) {
        this.fieldDbId = fieldDbId;
    }

    /**
     * Sets the value of fieldDbId.
     * 
     * @param fieldDbId
     *            The value to assign fieldDbId.
     */
    public void setFieldDbId(String fieldDbId) {
        this.fieldDbId.add(fieldDbId);
    }

    /**
     * Returns the value of fieldName.
     * 
     * @return The fieldName value
     */
    public ArrayList getFieldName() {
        return fieldName;
    }

    /**
     * Sets the value of fieldName.
     * 
     * @param fieldName
     *            The value to assign fieldName.
     */
    public void setFieldName(ArrayList fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * Sets the value of fieldName.
     * 
     * @param fieldName
     *            The value to add to fieldName.
     */
    public void setFieldName(String fieldName) {
        this.fieldName.add(fieldName);
    }

    /**
     * Returns the value of fieldDisplayName.
     * 
     * @return The fieldDisplayName value
     */
    public ArrayList getFieldDisplayName() {
        return fieldDisplayName;
    }

    /**
     * Sets the value of fieldDisplayName.
     * 
     * @param fieldDisplayName
     *            The value to assign fieldDisplayName.
     */
    public void setFieldDisplayName(ArrayList fieldDisplayName) {
        this.fieldDisplayName = fieldDisplayName;
    }

    /**
     * Sets the value of fieldDisplayName.
     * 
     * @param fieldDisplayName
     *            The value to add to fieldDisplayName.
     */
    public void setFieldDisplayName(String fieldDisplayName) {
        this.fieldDisplayName.add(fieldDisplayName);
    }

    /**
     * Returns the value of fieldSequence.
     * 
     * @return The fieldSequence value
     */
    public ArrayList getFieldSequence() {
        return fieldSequence;
    }

    /**
     * Sets the value of fieldSequence.
     * 
     * @param fieldSequence
     *            The value to assign fieldSequence.
     */
    public void setFieldSequence(ArrayList fieldSequence) {
        this.fieldSequence = fieldSequence;
    }

    /**
     * Sets the value of fieldSequence.
     * 
     * @param fieldSequence
     *            The value to assign fieldSequence.
     */
    public void setFieldSequence(String fieldSequence) {
        this.fieldSequence.add(fieldSequence);
    }

    /**
     * Returns the value of fieldFormat.
     * 
     * @return The fieldFormat value
     */
    public ArrayList getFieldFormat() {
        return fieldFormat;
    }

    /**
     * Sets the value of fieldFormat.
     * 
     * @param fieldFormat
     *            The value to assign fieldFormat.
     */
    public void setFieldFormat(ArrayList fieldFormat) {
        this.fieldFormat = fieldFormat;
    }

    /**
     * Sets the value of fieldFormat.
     * 
     * @param fieldFormat
     *            The value to add to fieldFormat.
     */
    public void setFieldFormat(String fieldFormat) {
        this.fieldFormat.add(fieldFormat);
    }

    /**
     * Returns the value of fieldType.
     * 
     * @return The fieldType value
     */
    public ArrayList getFieldType() {
        return fieldType;
    }

    /**
     * Sets the value of fieldType.
     * 
     * @param fieldType
     *            The value to assign fieldType.
     */
    public void setFieldType(ArrayList fieldType) {
        this.fieldType = fieldType;
    }

    /**
     * Sets the value of fieldType.
     * 
     * @param fieldType
     *            The value to assign fieldType.
     */
    public void setFieldType(String fieldType) {
        this.fieldType.add(fieldType);
    }

    /**
     * Returns the value of fieldWidth.
     * 
     * @return The fieldWidth value
     */
    public ArrayList getFieldWidth() {
        return fieldWidth;
    }

    /**
     * Sets the value of fieldWidth.
     * 
     * @param fieldWidth
     *            The value to assign fieldWidth.
     */
    public void setFieldWidth(ArrayList fieldWidth) {
        this.fieldWidth = fieldWidth;
    }

    /**
     * Sets the value of fieldWidth.
     * 
     * @param fieldWidth
     *            The value to assign fieldWidth.
     */
    public void setFieldWidth(String fieldWidth) {
        this.fieldWidth.add(fieldWidth);
    }

    /**
     * Returns the value of fieldStr.
     * 
     * @return The fieldStr value
     */
    public ArrayList getFieldStr() {
        return fieldStr;
    }

    /**
     * Sets the value of fieldStr.
     * 
     * @param fieldStr
     *            The value to assign fieldStr.
     */
    public void setFieldStr(ArrayList fieldStr) {
        this.fieldStr = fieldStr;
    }

    /**
     * Sets the value of fieldStr.
     * 
     * @param fieldStr
     *            The value to assign fieldStr.
     */
    public void setFieldStr(String fieldStr) {
        this.fieldStr.add(fieldStr);
    }

    /**
     * Returns the value of fieldSectionId.
     * 
     * @return The fieldSectionId value
     */
    public ArrayList getFieldSectionId() {
        return fieldSectionId;
    }

    /**
     * Sets the value of fieldSectionId.
     * 
     * @param fieldSectionId
     *            The value to assign fieldSectionId.
     */
    public void setFieldSectionId(ArrayList fieldSectionId) {
        this.fieldSectionId = fieldSectionId;
    }

    /**
     * Returns the value of fieldSectionName.
     * 
     * @return The fieldSectionName value
     */
    public ArrayList getFieldSectionName() {
        return fieldSectionName;
    }

    /**
     * Sets the value of fieldSectionName.
     * 
     * @param fieldSectionName
     *            The value to assign fieldSectionName.
     */
    public void setFieldSectionName(ArrayList fieldSectionName) {
        this.fieldSectionName = fieldSectionName;
    }

    /**
     * Returns the value of fieldList.
     * 
     * @return The fieldList value
     */
    public ArrayList getFieldList() {
        return fieldList;
    }

    /**
     * Sets the value of fieldList.
     * 
     * @param fieldList
     *            The value to assign fieldList.
     */
    public void setFieldList(ArrayList fieldList) {
        this.fieldList = fieldList;
    }

    /**
     * Sets the value of fieldList.
     * 
     * @param fieldId
     *            The new fieldList value
     */
    public void setFieldList(String fieldId) {
        this.fieldList.add(fieldId);
    }

    /**
     * Returns the value of fieldNameList.
     * 
     * @return The fieldNameList value
     */
    public ArrayList getFieldNameList() {
        return fieldNameList;
    }

    /**
     * Sets the value of fieldNameList.
     * 
     * @param fieldNameList
     *            The value to assign fieldNameList.
     */
    public void setFieldNameList(ArrayList fieldNameList) {
        this.fieldNameList = fieldNameList;
    }

    /**
     * Sets the value of fieldNameList.
     * 
     * @param fieldName
     *            The new fieldNameList value
     */
    public void setFieldNameList(String fieldName) {
        this.fieldNameList.add(fieldNameList);
    }

    /**
     * Returns the value of fieldType.
     * 
     * @return The fieldTypeList value
     */
    public ArrayList getFieldTypeList() {
        return fieldTypeList;
    }

    /**
     * Sets the value of fieldType.
     * 
     * @param fieldType
     *            The value to assign fieldType.
     */
    public void setFieldTypeList(ArrayList fieldType) {
        this.fieldTypeList = fieldType;
    }

    /**
     * Returns the value of fieldPkList.
     */
    public ArrayList getFieldPkList() {
        return fieldPkList;
    }

    /**
     * Sets the value of fieldPkList.
     * 
     * @param fieldPkList
     *            The value to assign fieldPkList.
     */
    public void setFieldPkList(ArrayList fieldPkList) {
        this.fieldPkList = fieldPkList;
    }

    /**
     * Sets the value of fieldType.
     * 
     * @param fieldType
     *            The value to assign fieldType.
     */
    public void setFieldTypeList(String fieldType) {
        this.fieldTypeList.add(fieldType);
    }

    /**
     * Returns the value of fieldPrevColId.
     */
    public ArrayList getFieldPrevColId() {
        return fieldPrevColId;
    }

    /**
     * Sets the value of fieldPrevColId.
     * 
     * @param fieldPrevColId
     *            The value to assign fieldPrevColId.
     */
    public void setFieldPrevColId(ArrayList fieldPrevColId) {
        this.fieldPrevColId = fieldPrevColId;
    }

    /**
     * Sets the value of fieldPrevColId.
     * 
     * @param fieldPrevColId
     *            The value to assign fieldPrevColId.
     */
    public void setFieldPrevColId(String ColId) {
        this.fieldPrevColId.add(ColId);
    }

    /**
     * Returns the value of pkTableName.
     */
    public String getPkTableName() {
        return pkTableName;
    }

    /**
     * Sets the value of pkTableName.
     * 
     * @param pkTableName
     *            The value to assign pkTableName.
     */
    public void setPkTableName(String pkTableName) {
        this.pkTableName = pkTableName;
    }

    /**
     * Returns the value of mainFilterColName.
     */
    public String getMainFilterColName() {
        return mainFilterColName;
    }

    /**
     * Sets the value of mainFilterColName.
     * 
     * @param mainFilterColName
     *            The value to assign mainFilterColName.
     */
    public void setMainFilterColName(String mainFilterColName) {
        this.mainFilterColName = mainFilterColName;
    }

    /**
     * Returns the value of pkColName.
     */
    public String getPkColName() {
        return pkColName;
    }

    /**
     * Sets the value of pkColName.
     * 
     * @param pkColName
     *            The value to assign pkColName.
     */
    public void setPkColName(String pkColName) {
        this.pkColName = pkColName;
    }

    /**
     * Returns the value of mapStudyColumn.
     */
    public String getMapStudyColumn() {
        return mapStudyColumn;
    }

    /**
     * Sets the value of mapStudyColumn.
     * 
     * @param mapStudyColumn
     *            The value to assign mapStudyColumn.
     */
    public void setMapStudyColumn(String mapStudyColumn) {
        this.mapStudyColumn = mapStudyColumn;
    }

    /**
     * Returns the value of useUniqueIds.
     */
    public ArrayList getUseUniqueIds() {
        return useUniqueIds;
    }

    /**
     * Sets the value of useUniqueIds.
     * 
     * @param useUniqueIds
     *            The value to assign useUniqueIds.
     */
    public void setUseUniqueIds(ArrayList useUniqueIds) {
        this.useUniqueIds = useUniqueIds;

    }

    /**
     * Sets the value of useUniqueIds.
     * 
     * @param useUniqueIds
     *            The value to assign useUniqueIds.
     */
    public void setUseUniqueIds(String useUniqueId) {
        this.useUniqueIds.add(useUniqueId);

    }

    /**
     * Returns the value of useDataValues.
     */
    public ArrayList getUseDataValues() {
        return useDataValues;
    }

    /**
     * Sets the value of useDataValues.
     * 
     * @param useDataValues
     *            The value to assign useDataValues.
     */
    public void setUseDataValues(ArrayList useDataValues) {
        this.useDataValues = useDataValues;
    }

    /**
     * Sets the value of useDataValues.
     * 
     * @param useDataValue
     *            The value to add to useDataValues.
     */
    public void setUseDataValues(String useDataValue) {
        this.useDataValues.add(useDataValue);
    }

    /**
     * Method to reset all the lists
     */
    public void reset() {
        fieldColId.clear();
        fieldDbId.clear();
        fieldName.clear();
        fieldDisplayName.clear();
        fieldSequence.clear();
        fieldFormat.clear();
        fieldType.clear();
        fieldWidth.clear();
        fieldStr.clear();
        fieldSectionId.clear();
        fieldSectionName.clear();
        fieldList.clear();
        fieldNameList.clear();
        fieldTypeList.clear();
        useDataValues.clear();
        useUniqueIds.clear();
        fieldTypesNoRepeatFields.clear();
    	calcSums.clear();
    	calcPers.clear();
    	fieldRepeatNumber.clear();
        fieldKeyword.clear();
    	//mapColSecRepNo.clear();
        
        // Dont reset the previous column selected.
        // fieldPrevColId.clear();
    }

    /**
     * Description of the Method
     */
    public void Display() {
        System.out.println("*************Field Objects lists****************");
        System.out.println(fieldColId + "\n" + fieldDbId + "\n" + fieldName
                + "\n" + fieldDisplayName + "\n" + fieldSequence + "\n"
                + fieldFormat + "\n" + fieldType + "\n" + fieldWidth + "\n"
                + fieldStr + "\n" + fieldSectionId + "\n" + fieldSectionName
                + "\n" + fieldList + "\n" + fieldNameList + "\n"
                + fieldTypeList + "\n fieldPkList" + fieldPkList
                + "\n fieldPrevColId" + fieldPrevColId + "\n pkTableName"
                + pkTableName + "\n mainFilterColName" + mainFilterColName
                + "\npkColName" + pkColName + "\n mapStudyColumn"
                + getMapStudyColumn());
        System.out.println(useDataValues + "\n" + useUniqueIds
                + "\n fieldTypesNoRepeatFields" + fieldTypesNoRepeatFields + "\n calcSums" + calcSums + "\n calcPers" + calcPers);

        System.out.println("*************Field Objects END****************");  
    }

	public void setMapColSecRepNo(ArrayList mapColSecRepNo) {
		this.mapColSecRepNo = mapColSecRepNo;
	}
	
	public void setMapColSecRepNo( String repNo) {
		this.mapColSecRepNo.add(repNo);
	}
	
	public ArrayList getMapColSecRepNo() {
		return mapColSecRepNo;
	}



	public ArrayList getFieldRepeatNumber() {
		return fieldRepeatNumber;
	}



	public void setFieldRepeatNumber(ArrayList fieldRepeatNumber) {
		this.fieldRepeatNumber = fieldRepeatNumber;
	}

	public void setFieldRepeatNumber(String fieldRepeatNum) {
		this.fieldRepeatNumber.add(fieldRepeatNum);
	}



	public ArrayList getFieldKeyword() {
		return fieldKeyword;
	}



	public void setFieldKeyword(ArrayList fieldKeyword) {
		this.fieldKeyword = fieldKeyword;
	}
	
	public void setFieldKeyword(String fldKeyword) {
		this.fieldKeyword.add(fldKeyword);
	}
}
