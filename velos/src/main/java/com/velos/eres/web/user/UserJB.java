/*
 * Classname : UserJB
 * 
 * Version information: 1.0
 *
 * Date: 02/25/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana
 */

package com.velos.eres.web.user;

/* IMPORT STATEMENTS */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import com.velos.login.ConnectObj;

import com.velos.login.callback.handler.VelosCallBackHandler;

import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

import com.velos.eres.business.common.AccountDao;
import com.velos.eres.business.common.UserDao;
import com.velos.eres.business.study.UserStudiesStateKeeper;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.EnvUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.Security;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.account.AccountJB;

import com.velos.login.*;

/* END OF IMPORT STATEMENTS */

/**
 * Client side Java Bean for User
 * 
 * @author Dinesh
 * @version 1.0 02/25/2001
 * 
 */

public class UserJB {

    /*
     * Class Methods : public int getUserId() public void setUserId(int userId)
     * public String getUserCodelstJobtype() public void
     * setUserCodelstJobtype(String userCodelstJobtype) public String
     * getUserAccountId() public void setUserAccountId(String userAccountId)
     * public String getUserSiteId() public void setUserSiteId(String
     * userSiteId) public String getUserPerAddressId() public void
     * setUserPerAddressId(String userPerAddressId) public String
     * getUserLastName() public void setUserLastName(String userLastName) public
     * String getUserFirstName() public void setUserFirstName(String
     * userFirstName) public String getUserMidName() public void
     * setUserMidName(String userMidName) public String getUserWrkExp() public
     * void setUserWrkExp(String userWrkExp) public String getUserPhaseInv()
     * public void setUserPhaseInv(String userPhaseInv) public String
     * getUserSessionTime() public void setUserSessionTime(String
     * userSessionTime) public String getUserLoginName() public void
     * setUserLoginName(String userLoginName) public String getUserPwd() public
     * void setUserPwd(String userPwd) public String getUserSecQues() public
     * void setUserSecQues(String userSecQues) public String getUserAnswer()
     * public void setUserAnswer(String userAnswer) public String
     * getUserStatus() public void setUserStatus(String userStatus) public
     * String getUserCodelstSpl() public void setUserCodelstSpl(String
     * userCodelstSpl) public String getUserGrpDefault() public void
     * setUserGrpDefault(String userGrpDefault) public String getUserSiteFlag()
     * public void setUserSiteFlag(String userSiteFlag) public UserJB(int
     * userId) public UserJB() public UserJB(int userId, String
     * userCodelstJobtype, String userAccountId, String userSiteId, String
     * userPerAddressId, String userLastName, String userFirstName, String
     * userMidName, String userWrkExp, String userPhaseInv, String
     * userSessionTime, String userAccessLevel, String userLoginName, String
     * userPwd, String userSecQues, String userAnswer, String userStatus, String
     * userCodelstSpl, String userGrpDefault)
     * 
     * public UserStateKeeper getUserDetails() public UserStateKeeper
     * validateUser(String username,String password) public void
     * setUserDetails() public int updateUser() public UserStateKeeper
     * createUserStateKeeper() public UserStudiesStateKeeper getUserStudies()
     * public UserDao getByGroupId(int groupId) public UserDao
     * getUserValuesByLoginName(String logName) public String toXML()
     * 
     * 
     */

    /**
     * the user Id
     */
    private int userId;

    /**
     * user jobtype
     */
    private String userCodelstJobtype;

    /**
     * user accountId
     */

    private String userAccountId;

    /**
     * user siteId
     */

    private String userSiteId;

    /**
     * user permanent AddressId
     */
    private String userPerAddressId;

    /**
     * user lastName
     */
    private String userLastName;

    /**
     * user firstName
     */

    private String userFirstName;

    /**
     * user mid Name
     */

    private String userMidName;

    /**
     * user work experience
     */

    private String userWrkExp;

    /**
     * the user phase ivaluation
     */

    private String userPhaseInv;

    /**
     * user session Time out
     */

    private String userSessionTime;

    /**
     * user login Name
     */

    private String userLoginName;

    /**
     * user password
     */

    private String userPwd;

    /**
     * user Secret Question
     */

    private String userSecQues;

    /**
     * user Answer to the secret question
     */

    private String userAnswer;

    /**
     * user status
     */

    private String userStatus;

    /**
     * user codelist primary Speciality
     */

    private String userCodelstSpl;

    /**
     * user Default group
     */
    private String userGrpDefault;

    /**
     * Time Zone
     */
    private String timeZoneId;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;
    
    private String usrLoggedIn;

    /*
     * IP Address
     */
    private String ipAdd;

    /*
     * Pwd Expiry Date
     */
    private String userPwdExpiryDate;

    /*
     * Pwd Expiry Days
     */
    private String userPwdExpiryDays;

    /*
     * Pwd Reminder to be sent on
     */
    private String userPwdReminderDate;

    /*
     * E-signature
     */
    private String userESign;

    /*
     * E-signature Expiry Date
     */
    private String userESignExpiryDate;

    /*
     * Pwd Expiry Days
     */
    private String userESignExpiryDays;

    /*
     * Pwd Reminder to be sent on
     */
    private String userESignReminderDate;

    /*
     * User Login attempt count
     */
    private String userAttemptCount;

    /*
     * User Login attempt count
     */
    private String userLoginFlag;

    /*
     * User Site Access flag
     */
    private String userSiteFlag;

    /*
     * User Site TypeN - Non system user,S - System User
     */
    private String userType;
    
    
    private String userLoginModule;
    
    private String userLoginModuleMap;

    private String userSkin;
    
    private String userThemes;
    
    private String userHidden;//KM
    /* INF-22330 06-Aug-2012 -Sudhir*/
    /**
     * user session ID
     */

    private String userCurrntSsID;

    
    // GETTER SETTER METHODS

    

	/**
     * 
     * 
     * @return Id of the User
     */
    public int getUserId() {
        Rlog.debug("user", "UserJB.getUserDetails() getUserId this.userId "
                + this.userId);
        return this.userId;

    }

    /**
     * 
     * 
     * @param userId
     *            Id of the User
     */
    public void setUserId(int userId) {

        Rlog
                .debug("user", "UserJB.getUserDetails() setUserId userId "
                        + userId);
        this.userId = userId;
    }

    /**
     * 
     * 
     * @return the code List Id of User Job Type
     */
    public String getUserCodelstJobtype() {
        Rlog.debug("user", "UserJB.getUserDetails() getuserCodelstJobtype "
                + this.userCodelstJobtype);
        return this.userCodelstJobtype;
    }

    /**
     * 
     * 
     * @param userCodelstJobtype
     *            code List Id of User Job Type
     */
    public void setUserCodelstJobtype(String userCodelstJobtype) {
        Rlog.debug("user", "UserJB.getUserDetails() setuserCodelstJobtype "
                + userCodelstJobtype);
        this.userCodelstJobtype = userCodelstJobtype;
    }

    /**
     * 
     * 
     * @return the Account Id of the User
     */
    public String getUserAccountId() {
        return this.userAccountId;
    }

    /**
     * 
     * 
     * 
     * @param userAccountId
     *            the Account ID of the User
     */
    public void setUserAccountId(String userAccountId) {
        this.userAccountId = userAccountId;
    }

    /**
     * 
     * 
     * @return Site ID of the User
     */
    public String getUserSiteId() {
        Rlog.debug("user", "UserJB.getUserDetails() getUserSiteId "
                + this.userSiteId);
        return this.userSiteId;
    }

    /**
     * 
     * 
     * @param userSiteId
     *            Site ID of the User
     */
    public void setUserSiteId(String userSiteId) {
        Rlog.debug("user", "UserJB.getUserDetails() setUserSiteId "
                + userSiteId);
        this.userSiteId = userSiteId;
    }

    /**
     * 
     * 
     * @return Address ID of the User
     */
    public String getUserPerAddressId() {
        Rlog.debug("user", "UserJB.getUserDetails() getUserPerAddressId "
                + this.userPerAddressId);
        return this.userPerAddressId;
    }

    /**
     * 
     * 
     * @param userPerAddressId
     *            Address ID of the User
     */
    public void setUserPerAddressId(String userPerAddressId) {
        Rlog.debug("user", "UserJB.getUserDetails() setUserPerAddressId "
                + userPerAddressId);
        this.userPerAddressId = userPerAddressId;
    }

    /**
     * 
     * 
     * @return the last Name of the User
     */
    public String getUserLastName() {
        return this.userLastName;
    }

    /**
     * 
     * 
     * @param userLastName
     *            the last Name of the User
     */
    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    /**
     * 
     * 
     * @return the First Name of the User
     */
    public String getUserFirstName() {
        return this.userFirstName;
    }

    /**
     * 
     * 
     * @param userFirstName
     *            the First Name of the User
     */
    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    /**
     * 
     * 
     * @return the Middle Name of the User
     */
    public String getUserMidName() {
        return this.userMidName;
    }

    /**
     * 
     * 
     * @param userMidName
     *            the Middle Name of the User
     */
    public void setUserMidName(String userMidName) {
        this.userMidName = userMidName;
    }

    /**
     * 
     * 
     * @return the Work Experience of the User
     */
    public String getUserWrkExp() {
        return this.userWrkExp;
    }

    /**
     * 
     * 
     * @param userWrkExp
     *            the Work Experience of the User
     */
    public void setUserWrkExp(String userWrkExp) {
        this.userWrkExp = userWrkExp;
    }

    /**
     * 
     * 
     * @return the phases the user has been involved in
     */
    public String getUserPhaseInv() {
        return this.userPhaseInv;
    }

    /**
     * 
     * 
     * @param userPhaseInv
     *            the phases the user has been involved in
     */
    public void setUserPhaseInv(String userPhaseInv) {
        this.userPhaseInv = userPhaseInv;
    }

    /**
     * 
     * 
     * @return User Session Time Period
     */
    public String getUserSessionTime() {
        return this.userSessionTime;
    }

    /**
     * 
     * 
     * @param userSessionTime
     *            User Session Time Period
     */
    public void setUserSessionTime(String userSessionTime) {
        this.userSessionTime = userSessionTime;
    }

    /**
     * 
     * 
     * @return User Login Name
     */
    public String getUserLoginName() {
        return this.userLoginName;
    }

    /**
     * 
     * 
     * @param userLoginName
     *            User Login Name
     */
    public void setUserLoginName(String userLoginName) {
        this.userLoginName = userLoginName;
    }

    /**
     * 
     * 
     * @return User Password User Password
     */
    public String getUserPwd() {
        return this.userPwd;
    }

    /**
     * 
     * 
     * @param userPwd
     *            User Password
     */
    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    /**
     * 
     * 
     * @return Secret Question identified by the User
     */
    public String getUserSecQues() {
        return this.userSecQues;
    }

    /**
     * 
     * 
     * @param userSecQues
     *            Secret Question identified by the User
     */
    public void setUserSecQues(String userSecQues) {
        this.userSecQues = userSecQues;
    }

    /**
     * 
     * 
     * @return Answer to the Secret Question identified by the User
     */
    public String getUserAnswer() {
        return this.userAnswer;
    }

    /**
     * 
     * 
     * @param userAnswer
     *            Answer to the Secret Question identified by the User
     */
    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    /**
     * 
     * 
     * @return User Status
     */
    public String getUserStatus() {
        return this.userStatus;
    }

    /**
     * 
     * 
     * @param userStatus
     *            User Status
     */
    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    /**
     * 
     * 
     * @return Code List Id of User Speciality
     */
    public String getUserCodelstSpl() {
        return this.userCodelstSpl;
    }

    /**
     * 
     * 
     * @param userCodelstSpl
     *            Code List Id of User Speciality
     */
    public void setUserCodelstSpl(String userCodelstSpl) {
        this.userCodelstSpl = userCodelstSpl;
    }

    /**
     * 
     * 
     * @return Id of the Default Group of the User
     */
    public String getUserGrpDefault() {
        return this.userGrpDefault;
    }

    /**
     * 
     * 
     * @param userGrpDefault
     *            Id of the Default Group of the User
     */
    public void setUserGrpDefault(String userGrpDefault) {
        this.userGrpDefault = userGrpDefault;
    }

    /**
     * 
     * 
     * @return Time Zone Id of the Default Group of the User
     */
    public String getTimeZoneId() {
        return this.timeZoneId;
    }

    /**
     * 
     * 
     * @param Time
     *            Zone Id of the Default Group of the User
     */
    public void setTimeZoneId(String timeZoneId) {
        this.timeZoneId = timeZoneId;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * @return Pwd Expiry Date
     */
    public String getUserPwdExpiryDate() {
        return this.userPwdExpiryDate;
    }

    /**
     * @param userPwdExpiryDate
     *            The value that is required to be registered as the Pwd Expiry
     *            Date
     */
    public void setUserPwdExpiryDate(String userPwdExpiryDate) {
        this.userPwdExpiryDate = userPwdExpiryDate;
    }

    /**
     * @return Pwd Expiry Days
     */
    public String getUserPwdExpiryDays() {
        return this.userPwdExpiryDays;
    }

    /**
     * @param userPwdExpiryDays
     *            The value that is required to be registered as the Pwd Expiry
     *            Days
     */
    public void setUserPwdExpiryDays(String userPwdExpiryDays) {
        this.userPwdExpiryDays = userPwdExpiryDays;
    }

    /**
     * @return Pwd Reminder to be sent
     */
    public String getUserPwdReminderDate() {
        return this.userPwdReminderDate;
    }

    /**
     * @param userPwdReminderDate
     *            The value that is required to be registered as the Pwd
     *            Reminder to be sent on
     */
    public void setUserPwdReminderDate(String userPwdReminderDate) {
        this.userPwdReminderDate = userPwdReminderDate;
    }

    /**
     * @return E-signature
     */
    public String getUserESign() {
        return this.userESign;
    }

    /**
     * @param userESign
     *            The value that is required to be registered as the E-signature
     */
    public void setUserESign(String userESign) {
        this.userESign = userESign;
    }

    /**
     * @return E-signature Expiry Date
     */
    public String getUserESignExpiryDate() {
        return this.userESignExpiryDate;
    }

    /**
     * @param userESignExpiryDate
     *            The value that is required to be registered as the E-signature
     *            Expiry Date
     */
    public void setUserESignExpiryDate(String userESignExpiryDate) {
        this.userESignExpiryDate = userESignExpiryDate;
    }

    /**
     * @return Pwd Expiry Days
     */
    public String getUserESignExpiryDays() {
        return this.userESignExpiryDays;
    }

    /**
     * @param userESignExpiryDays
     *            The value that is required to be registered as the Pwd Expiry
     *            Days
     */
    public void setUserESignExpiryDays(String userESignExpiryDays) {
        this.userESignExpiryDays = userESignExpiryDays;
    }

    /**
     * @return Pwd Reminder to be sent on
     */
    public String getUserESignReminderDate() {
        return this.userESignReminderDate;
    }

    /**
     * @param userESignReminderDate
     *            The value that is required to be registered as the Pwd
     *            Reminder to be sent on
     */
    public void setUserESignReminderDate(String userESignReminderDate) {
        this.userESignReminderDate = userESignReminderDate;
    }

    public String getUserLoginFlag() {
        return this.userLoginFlag;
    }

    public void setUserLoginFlag(String userLoginFlag) {
        this.userLoginFlag = userLoginFlag;
    }

    public String getUserAttemptCount() {
        return this.userAttemptCount;
    }

    public void setUserAttemptCount(String userAttemptCount) {
        this.userAttemptCount = userAttemptCount;
    }

    public String getUserSiteFlag() {
        return this.userSiteFlag;
    }

    public void setUserSiteFlag(String userSiteFlag) {
        this.userSiteFlag = userSiteFlag;
    }

    public String getUserType() {
        return this.userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
    /**
	 * @return the userLoginModule
	 */
	public String getUserLoginModule() {
		return userLoginModule;
	}

	/**
	 * @param userLoginModule the userLoginModule to set
	 */
	public void setUserLoginModule(String userLoginModule) {
		this.userLoginModule = userLoginModule;
	}

	/**
	 * @return the userLoginModuleMap
	 */
	public String getUserLoginModuleMap() {
		return userLoginModuleMap;
	}

	/**
	 * @param userLoginModuleMap the userLoginModuleMap to set
	 */
	public void setUserLoginModuleMap(String userLoginModuleMap) {
		this.userLoginModuleMap = userLoginModuleMap;
	}
    

	public String getUserSkin() {
		return userSkin;
	}

	/**
	 * @param userLoginModuleMap the userLoginModuleMap to set
	 */
	public void setUserSkin(String userSkin) {
		this.userSkin = userSkin;
	}

	
	public String getUserTheme() {
		return userThemes;
	}

	/**
	 * @param userLoginModuleMap the userLoginModuleMap to set
	 */
	public void setUserTheme(String userThemes) {
		this.userThemes = userThemes;
	}
	
	public String getUsrLoggedIn() {
		return usrLoggedIn;
	}

	public void setUsrLoggedIn(String usrLoggedIn) {
		this.usrLoggedIn = usrLoggedIn;
	}
	
	
	//Added by Manimaran for Enh.#U11
	/**
	 * @param userHidden the userHidden to set
	 */
	public void setUserHidden(String userHidden) {
		
		String UH = "";
		
		if (StringUtil.isEmpty(userHidden))
    	{
			UH = "0";
    	}
    	else
    	{
    		UH = userHidden;
    	}
		
		this.userHidden = UH;
	}	
	
	public String getUserHidden() {
		return userHidden;
	}
    
 	// END OF GETTER SETTER METHODS

    /**
	 * @param userCurrntSsID the userCurrntSsID to set
	 */
	public void setUserCurrntSsID(String userCurrntSsID) {
		this.userCurrntSsID = userCurrntSsID;
	}

	/**
	 * @return the userCurrntSsID
	 */
	public String getUserCurrntSsID() {
		return userCurrntSsID;
	}

	/**
     * Constructor; Accepts User Id
     * 
     * @param userId
     *            Id of the User
     */
    public UserJB(int userId) {
        setUserId(userId);
    }

    /**
     * Default Constructor
     * 
     */
    public UserJB() {

    }

    /**
     * Full Arguments Constructor
     * 
     * @param userId
     * @param userCodelstJobtype
     * @param userAccountId
     * @param userSiteId
     * @param userPerAddressId
     * @param userLastName
     * @param userFirstName
     * @param userMidName
     * @param userWrkExp
     * @param userPhaseInv
     * @param userSessionTime
     * @param userAccessLevel
     * @param userLoginName
     * @param userPwd
     * @param userSecQues
     * @param userAnswer
     * @param userStatus
     * @param userCodelstSpl
     * @param userGrpDefault
     * @param userSiteFlag
     */
    public UserJB(Integer userId, String userCodelstJobtype,
            String userAccountId, String userSiteId, String userPerAddressId,
            String userLastName, String userFirstName, String userMidName,
            String userWrkExp, String userPhaseInv, String userSessionTime,
            String userAccessLevel, String userLoginName, String userPwd,
            String userSecQues, String userAnswer, String userStatus,
            String userCodelstSpl, String userGrpDefault, String creator,
            String modifiedBy, String ipAdd, String userPwdExpiryDate,
            String userPwdExpiryDays, String userPwdReminderDate,
            String userESign, String userESignExpiryDate,
            String userESignExpiryDays, String userESignReminderDate,
            String userLoginFlag, String userAttemptCount, String userSiteFlag,
            String userType, String skin, String theme, String userHidden,String userCurrntSsID,String usrLoggedIn) {

        setUserId(userId);
        setUserCodelstJobtype(userCodelstJobtype);
        setUserAccountId(userAccountId);
        setUserSiteId(userSiteId);
        setUserPerAddressId(userPerAddressId);
        setUserLastName(userLastName);
        setUserFirstName(userFirstName);
        setUserMidName(userMidName);
        setUserWrkExp(userWrkExp);
        setUserPhaseInv(userPhaseInv);
        setUserSessionTime(userSessionTime);
        setUserLoginName(userLoginName);
        setUserPwd(userPwd);
        setUserSecQues(userSecQues);
        setUserAnswer(userAnswer);
        setUserStatus(userStatus);
        setUserCodelstSpl(userCodelstSpl);
        setUserGrpDefault(userGrpDefault);
        setTimeZoneId(timeZoneId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setUserPwdExpiryDate(userPwdExpiryDate);
        setUserPwdExpiryDays(userPwdExpiryDays);
        setUserPwdReminderDate(userPwdReminderDate);
        setUserESign(userESign);
        setUserESignExpiryDate(userESignExpiryDate);
        setUserESignExpiryDays(userESignExpiryDays);
        setUserESignReminderDate(userESignReminderDate);
        setUserLoginFlag(userLoginFlag);
        setUserAttemptCount(userAttemptCount);
        setUserSiteFlag(userSiteFlag);
        setUserType(userType);
        setUserSkin(skin);
        setUserTheme(theme);
        setUserHidden(userHidden);//KM
        setUserCurrntSsID(userCurrntSsID);
        setUsrLoggedIn(usrLoggedIn);
        
        Rlog.debug("user", "UserJB.userType" + userType);
        Rlog.debug("user", "UserJB.UserJB(all parameters)");
    }

    /**
     * Calls getUserDetails() of User Session Bean: UserAgentBean
     * 
     * 
     * @return The Details of the user in the User StateKeeper Object
     * @See UserAgentBean
     */
    public UserBean getUserDetails() {
        UserBean usk = null;

        try {

            UserAgentRObj userAgent = EJBUtil.getUserAgentHome();

            usk = userAgent.getUserDetails(this.userId);
            Rlog
                    .debug("user", "UserJB.getUserDetails() UserStateKeeper "
                            + usk);
        } catch (Exception e) {
            Rlog.debug("user", "Error in getUserDetails() in UserJB " + e);
        }
        if (usk != null) {

            this.userId = usk.getUserId();
            this.userCodelstJobtype = usk.getUserCodelstJobtype();
            this.userAccountId = usk.getUserAccountId();
            this.userSiteId = usk.getUserSiteId();
            this.userPerAddressId = usk.getUserPerAddressId();
            this.userLastName = usk.getUserLastName();
            this.userFirstName = usk.getUserFirstName();
            this.userMidName = usk.getUserMidName();
            this.userWrkExp = usk.getUserWrkExp();
            this.userPhaseInv = usk.getUserPhaseInv();
            this.userSessionTime = usk.getUserSessionTime();
            this.userLoginName = usk.getUserLoginName();
            this.userPwd = usk.getUserPwd();
            this.userSecQues = usk.getUserSecQues();
            this.userAnswer = usk.getUserAnswer();
            this.userStatus = usk.getUserStatus();
            this.userCodelstSpl = usk.getUserCodelstSpl();
            this.userGrpDefault = usk.getUserGrpDefault();
            this.timeZoneId = usk.getTimeZoneId();
            this.creator = usk.getCreator();
            this.modifiedBy = usk.getModifiedBy();
            this.ipAdd = usk.getIpAdd();
            this.userPwdExpiryDate = usk.getUserPwdExpiryDate();
            this.userPwdExpiryDays = usk.getUserPwdExpiryDays();
            this.userPwdReminderDate = usk.getUserPwdReminderDate();
            this.userESign = (usk.getUserESign());
            this.userESignExpiryDate = usk.getUserESignExpiryDate();
            this.userESignExpiryDays = usk.getUserESignExpiryDays();
            this.userESignReminderDate = usk.getUserESignReminderDate();
            setUserLoginFlag(usk.getUserLoginFlag());
            setUserAttemptCount(usk.getUserAttemptCount());
            setUserSiteFlag(usk.getUserSiteFlag());
            this.userType = usk.getUserType();
            this.userLoginModule=usk.getUserLoginModule();
            this.userLoginModuleMap=usk.getUserLoginModuleMap();
            this.userSkin=usk.getUserSkin();
            this.userThemes=usk.getUserTheme();
            this.userHidden = usk.getUserHidden();//KM
            this.userCurrntSsID = usk.getUserCurrntSsID();
            Rlog.debug("user", "UserJB.usk.getUserType" + usk.getUserType());
        }
        return usk;
    }

    /**
     * Calls validateUser() of User Session Bean: UserAgentBean
     * 
     * @param username
     * @param password
     * @return The details of the valid user in a User StateKeeper Object
     */
    public UserBean validateUser(String username, String password) {
    	return validateUser(username, password, false);
    }
    
    public UserBean validateUser(String username, String password, boolean enableSessionMode) {
        UserBean usk = null;
        String ldapEnabled="";
        ConnectObj connObj=new ConnectObj();
        int loginModuleId=0;
        String loginModuleStr="";
        String loginModuleMap="",accName="";;
        AccountDao accountDao=new AccountDao();
        LoginContext lc=null;
        username=(username==null)?"":username;
        password=(password==null)?"":password;
        if ((username.length()==0) || (password.length()==0))
        	return null;
        
        
        	AccountJB accB=new AccountJB();
            UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
            usk=userAgent.findUser(username);
            if (usk!=null)
            {
            	
            	accB.setAccId((new Integer(usk.getUserAccountId())).intValue());
            	accB.getAccountDetails();
            	ldapEnabled=accB.getLdapEnabled();
            	ldapEnabled=(ldapEnabled==null)?"":ldapEnabled.toUpperCase();
            	accName=accB.getAccName();
           
            	loginModuleStr=usk.getUserLoginModule();
            	loginModuleStr=(loginModuleStr==null)?"0":loginModuleStr;
            	loginModuleId=new Integer(loginModuleStr).intValue();
            	loginModuleMap=usk.getUserLoginModuleMap();
            	if (loginModuleId>0 ) {
            	accountDao=accB.getLoginModule(loginModuleId);
            	connObj.setProtocol((String)accountDao.getAccLoginModuleProtocolList().get(0));
            	connObj.setHost((String)accountDao.getAccLoginModuleServerList().get(0));
            	connObj.setPort(((Integer)accountDao.getAccLoginModulePortList().get(0)).intValue());
            	connObj.setUserName("cn="+loginModuleMap+","+(String)accountDao.getAccLoginModuleDNList().get(0));
            	}
            	connObj.setPassword(password);
            	connObj.setVelosUser(username);
            	if (enableSessionMode) {  connObj.setAuthMode("session"); }
            }
            	
            	try {
        		if (ldapEnabled.equals("Y")){        			
        			lc = new LoginContext(accName, new VelosCallBackHandler(connObj));
        		}
        		//	lc = new LoginContext("Sample", new MyCallbackHandler());
        		} catch (LoginException le) {
        			Rlog.fatal("user","Cannot create LoginContext. " + le.getMessage());
        			
        			lc=null;
        			le.printStackTrace();
        			
        			
        		} catch (SecurityException se) {
        			Rlog.fatal("user","Cannot create LoginContext. " + se.getMessage());
        			se.printStackTrace();
        			lc=null;
        			
        		}
        		try {
        			if (lc==null)
        			lc = new LoginContext("velos-default", new VelosCallBackHandler(connObj));
        			
        		//	lc = new LoginContext("Sample", new MyCallbackHandler());
        		} catch (LoginException le) {
        			Rlog.fatal("user","Cannot create LoginContext. " + le.getMessage());
        			usk=null;
        			lc=null;
        			le.printStackTrace();
        			
        			
        		} catch (SecurityException se) {
        			Rlog.fatal("user","Cannot create LoginContext. " + se.getMessage());
        			se.printStackTrace();
        			lc=null;
        			usk=null;
        		}
        		try{
        		
        			lc.login();
        			Rlog.debug("user", "User Authentication Successful");
        			
        		}catch (LoginException le) {

    				Rlog.fatal("user", "User Authentication Failed.");
    				usk=null;
    				le.printStackTrace();
    				

    			}
              
        
        if (usk != null) {

            this.userId = usk.getUserId();
            this.userCodelstJobtype = usk.getUserCodelstJobtype();
            this.userAccountId = usk.getUserAccountId();
            this.userSiteId = usk.getUserSiteId();
            this.userPerAddressId = usk.getUserPerAddressId();
            this.userLastName = usk.getUserLastName();
            this.userFirstName = usk.getUserFirstName();
            this.userMidName = usk.getUserMidName();
            this.userWrkExp = usk.getUserWrkExp();
            this.userPhaseInv = usk.getUserPhaseInv();
            this.userSessionTime = usk.getUserSessionTime();
            this.userLoginName = usk.getUserLoginName();
            this.userPwd = usk.getUserPwd();
            this.userSecQues = usk.getUserSecQues();
            this.userAnswer = usk.getUserAnswer();
            this.userStatus = usk.getUserStatus();
            this.userCodelstSpl = usk.getUserCodelstSpl();
            this.userGrpDefault = usk.getUserGrpDefault();
            this.timeZoneId = usk.getTimeZoneId();
            this.userPwdExpiryDate = usk.getUserPwdExpiryDate();
            this.userPwdExpiryDays = usk.getUserPwdExpiryDays();
            this.userPwdReminderDate = usk.getUserPwdReminderDate();
            this.userESign = usk.getUserESign();
            this.userESignExpiryDate = usk.getUserESignExpiryDate();
            this.userESignExpiryDays = usk.getUserESignExpiryDays();
            this.userESignReminderDate = usk.getUserESignReminderDate();
            setUserLoginFlag(usk.getUserLoginFlag());
            setUserAttemptCount(usk.getUserAttemptCount());
            setUserSiteFlag(usk.getUserSiteFlag());
            Rlog.debug("user", "UserJB.usk.getUserType() in validate user"
                    + usk.getUserType());
            this.userType = usk.getUserType();
            this.userHidden = usk.getUserHidden();
            this.userCurrntSsID = usk.getUserCurrntSsID();
        }
        return usk;
    }

    /**
     * Calls setUserDetails() of User Session Bean: UserAgentBean
     * 
     * 
     */
    public void setUserDetails() {
        try {
            UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
            this.setUserId(userAgent.setUserDetails(this
                    .createUserStateKeeper()));
            Rlog.debug("user", "UserJB.setUserDetails()");
        } catch (Exception e) {
            Rlog.debug("user", "Error in setUserDetails() in UserJB " + e);
        }
    }

    /**
     * Calls updateUser() of User Session Bean: UserAgentBean
     * 
     * @return
     */
    public int updateUser() {
        int output;
        try {
            UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
            output = userAgent.updateUser(this.createUserStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("user", "EXCEPTION IN SETTING USER DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int updateUser(Hashtable<String, String> auditInfo) {
        int output;
        try {
            UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
            output = userAgent.updateUser(this.createUserStateKeeper(),auditInfo);
        } catch (Exception e) {
            Rlog.fatal("user", "EXCEPTION IN SETTING USER DETAILS TO  SESSION BEAN updateUser(Hashtable<String, String> auditInfo)");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * 
     * 
     * @return the User StateKeeper Object with the current values of the Bean
     */
    public UserBean createUserStateKeeper() {

        return new UserBean(new Integer(userId), userCodelstJobtype,
                userAccountId, userSiteId, userPerAddressId, userLastName,
                userFirstName, userMidName, userWrkExp, userPhaseInv,
                userSessionTime, userLoginName, (new Integer(userId)>0)?userPwd:Security.encryptSHA(userPwd), userSecQues,
                userAnswer, userStatus, userCodelstSpl, userGrpDefault,
                timeZoneId, creator, modifiedBy, ipAdd, userPwdExpiryDate,
                userPwdExpiryDays, userPwdReminderDate, userESign,
                userESignExpiryDate, userESignExpiryDays,
                userESignReminderDate, userAttemptCount, userLoginFlag,
                userSiteFlag, userType,userLoginModule,userLoginModuleMap, userSkin, userThemes, userHidden , userCurrntSsID, usrLoggedIn);

    }

    /*
     * Sonia Sahni Gets all studies for the user (the user belongs to the study
     * team)
     * 
     */

    /**
     * Returns all Studies to which the User has access to. Calls
     * getUserStudies() on StudyAgentBean
     * 
     * @return The StateKeeper Object for Studies
     * @See StudyAgentBean
     */
    public UserStudiesStateKeeper getUserStudies() {
        // get access to study session bean
        UserStudiesStateKeeper usk = null;
        try {
            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            usk = studyAgent.getUserStudies(this.userId);
        } catch (Exception e) {
            Rlog.debug("study", "EXCEPTION IN GETTING STUDY AGENT" + e);
        }
        return usk;

        // end of method
    }

    /**
     * Calls getByGroupId() of User Session Bean: UserAgentBean
     * 
     * @param groupId
     * @return returns UserDao for list of all users
     * @See UserDao
     */
    public UserDao getByGroupId(int groupId) {
        UserDao userDao = new UserDao();
        try {
            UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
            userDao = userAgent.getByGroupId(groupId);
            Rlog.debug("user", "UserJB.getByGroupId after Dao");
            return userDao;
        } catch (Exception e) {
            Rlog.fatal("user", "Error in getByGroupId(int groupId) in UserJB "
                    + e);
        }
        return userDao;
    }

    /**
     * Calls getAccountUsers() of User Session Bean: UserAgentBean
     * 
     * @param accId
     * @return returns UserDao for list of all users
     * @See UserDao
     */
    public UserDao getAccountUsers(int accId) {
        UserDao userDao = new UserDao();
        try {
            UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
            userDao = userAgent.getAccountUsers(accId);
            Rlog.debug("user", "UserJB.getAccountUsers after Dao");
            return userDao;
        } catch (Exception e) {
            Rlog.fatal("user", "Error in getAccountUsers(int accId) in UserJB "
                    + e);
        }
        return userDao;  
    }

    /**
     * Calls getAccountUsers() of User Session Bean: UserAgentBean
     * 
     * @param accId
     * @param userStatus
     * @return returns UserDao for list of all users
     * @See UserDao
     */
    //Modified by Manimaran to include firstname and  lastname in search criteria.
    public UserDao getAccountUsers(int accId, String userStatus,String searchUsr,String from) {
        UserDao userDao = new UserDao();
        try {
        	UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
            userDao = userAgent.getAccountUsers(accId, userStatus,searchUsr, from);
            return userDao;
        } catch (Exception e) {
            Rlog.fatal("user", "Error in getAccountUsers(int accId) in UserJB "
                    + e);
        }
        return userDao;
    }

    /*
     * Modified by Sonia - 08/06/04 Changed method Name
     * getActivatedDeactivatedUsers() to getUsersSelectively() added javadoc
     */

    /**
     * Calls getUsersSelectively() of User Session Bean: UserAgentBean
     * 
     * @param accId
     *            Account Id
     * @param status
     *            a comma separated list of status sun types. Possible Values:
     *            <BR>
     *            A - Active <br>
     *            B - Blocked <br>
     *            D - Deactivated <br>
     *            P - Pending <br>
     *            X - Deleted <br>
     *            Example a string of "'D','B'" will return Blocked and
     *            Deactivated users
     * @return UserDao object with details of all Users of a specified Account
     *         and for a list of status types
     * @see UserDao
     */
    
    //Modified by Manimaran to include firstname and  lastname in search criteria.
    public UserDao getUsersSelectively(int accId, String status, String searchUsr,String from) {
        UserDao userDao = new UserDao();
        try {

            UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
            userDao = userAgent.getUsersSelectively(accId, status, searchUsr,from);

            return userDao;
        } catch (Exception e) {
            Rlog.fatal("user",
                    "Error in getUsersSelectively(int accId) in UserJB " + e);
        }
        return userDao;
    }

    /**
     * Calls getNonSystemUsers() of User Session Bean: UserAgentBean
     * 
     * @param accId
     * @param user
     *            type
     * @return returns UserDao for list of all users
     * @See UserDao
     */
    
    //Modified by Manimaran to include firstname and  lastname in search criteria.
    public UserDao getNonSystemUsers(int accId, String userType, String searchUsr,String from) {
        UserDao userDao = new UserDao();
        try {

            UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
            userDao = userAgent.getNonSystemUsers(accId, userType, searchUsr,from);

            return userDao;
        } catch (Exception e) {
            Rlog
                    .fatal("user",
                            "Error in getNonSystemUsers(accId,userType) in UserJB "
                                    + e);
        }
        return userDao;
    }

    /**
     * Calls getAccountUsersDetails() of User Session Bean: UserAgentBean
     * 
     * @param accId
     * @return returns UserDao for list of all users
     * @See UserDao
     */
    public UserDao getAccountUsersDetails(int accId) {
        UserDao userDao = new UserDao();
        try {
            UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
            userDao = userAgent.getAccountUsersDetails(accId);
            Rlog.debug("user", "UserJB.getAccountUsersDetails after Dao");
            return userDao;
        } catch (Exception e) {
            Rlog.fatal("user",
                    "Error in getAccountUsersDetails(int groupId) in UserJB "
                            + e);
        }
        return userDao;
    }

    /**
     * Calls getAccountUsersDetails() of User Session Bean: UserAgentBean
     * 
     * @param accId
     * @return returns UserDao for list of all users
     * @See UserDao
     */
    public UserDao getAccountUsersDetailsForLookup(int accId) {
        UserDao userDao = null;
        try {
            UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
            userDao = userAgent.getAccountUsersDetailsForLookup(accId);
        } catch (Exception e) {
            Rlog.fatal("user",
                    "Error in getAccountUsersDetails(int groupId) in UserJB "+e);
        }
        if (userDao == null) { userDao = new UserDao(); } 
        return userDao;
    }
    
    /**
     * Calls getUserValuesByLoginName() of User Session Bean: UserAgentBean
     * 
     * @param logName
     * @return returns UserDao for list of all users
     * @See UserDao
     */
    public UserDao getUserValuesByLoginName(String logName) {
        UserDao userDao = new UserDao();
        try {
            UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
            userDao = userAgent.getUserValuesByLoginName(logName);
            Rlog.debug("user", "UserJB.getUserValuesByLoginName after Dao");
            return userDao;
        } catch (Exception e) {
            Rlog.fatal("user",
                    "Error in getUserValuesByLoginName(String logName) in UserJB "
                            + e);
        }
        return userDao;
    }

    /**
     * Calls getCount() of User Session Bean: UserAgentBean
     * 
     * @param userLogin
     * @return returns int for
     * @See UserDao
     */
    public int getCount(String userLogin) {
        int count = 0;
        try {
            UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
            count = userAgent.getCount(userLogin);
            Rlog.debug("user", "UserJB.getCount after Dao");
        } catch (Exception e) {
            Rlog.fatal("user", "Error in getCount(String userLogin) in UserJB "
                    + e);
        }
        return count;
    }

    /**
     * Calls getUserCode() of User Session Bean: UserAgentBean
     * 
     * @param userId
     * @return returns String for
     * @See UserDao
     */
    public String getUserCode(String userId) {
        String userCode = "";
        try {
            UserAgentRObj userAgentRObj = EJBUtil.getUserAgentHome();
            userCode = userAgentRObj.getUserCode(userId);
            Rlog.debug("user", "UserJB.getUserCode after Dao");
        } catch (Exception e) {
            Rlog.fatal("user", "Error in getUserCode(String userId) in UserJB "
                    + e);
        }
        return userCode;
    }

    /**
     * Calls getUsersWithStudyCopy(String studyId, String accountId) of User
     * Session Bean: UserAgentBean
     * 
     * @param studyId
     * @param accountId
     * @See UserDao
     */
    public UserDao getUsersWithStudyCopy(String studyId, String accountId) {
        UserDao userDao = new UserDao();
        try {
            UserAgentRObj userAgentRObj = EJBUtil.getUserAgentHome();
            userDao = userAgentRObj.getUsersWithStudyCopy(studyId, accountId);
            Rlog.debug("user", "UserJB.getUsersWithStudyCopy after Dao");
        } catch (Exception e) {
            Rlog.fatal("user", "Error in getUsersWithStudyCopy in UserJB " + e);
        }
        return userDao;
    }

    /**
     * Calls getUsersForEvent(int eventId, String userType) of User Session
     * Bean: UserAgentBean
     * 
     * @param eventId
     * @param userType
     * @See UserDao
     */
    public UserDao getUsersForEvent(int eventId, String userType) {
        UserDao userDao = new UserDao();
        try {
            UserAgentRObj userAgentRObj = EJBUtil.getUserAgentHome();
            userDao = userAgentRObj.getUsersForEvent(eventId, userType);
            Rlog.debug("user", "UserJB.getUsersForEvent after Dao");
        } catch (Exception e) {
            Rlog.fatal("user", "Error in getUsersForEvent in UserJB " + e);
        }
        return userDao;
    }

    /**
     * Calls getAvailableEventUsers(int accId, String lName, String fName,
     * String userType, int eventId) of User Session Bean: UserAgentBean
     * 
     * @param accId
     *            Account Id
     * @param lname
     *            Last Name
     * @param fname
     *            First Name
     * @param userType
     *            User Type
     * @param eventId
     *            Event Id
     * @See UserDao
     */
    public UserDao getAvailableEventUsers(int accId, String lName,
            String fName, String role, String organization, String userType,
            int eventId) {
        UserDao userDao = new UserDao();
        try {
            UserAgentRObj userAgentRObj = EJBUtil.getUserAgentHome();
            userDao = userAgentRObj.getAvailableEventUsers(accId, lName, fName,
                    role, organization, userType, eventId);
            Rlog.debug("user", "UserJB.getAvailableEventUsers after Dao");
        } catch (Exception e) {
            Rlog
                    .fatal("user", "Error in getAvailableEventUsers in UserJB "
                            + e);
        }
        return userDao;
    }

    /**
     * Calls getUsersDetails(String userList) of User Session Bean:
     * UserAgentBean
     * 
     * @param userList
     *            a comma separated list of userIds
     * @See UserDao
     */
    public UserDao getUsersDetails(String userList) {
        UserDao userDao = new UserDao();
        try {
            UserAgentRObj userAgentRObj = EJBUtil.getUserAgentHome();
            userDao = userAgentRObj.getUsersDetails(userList);
            Rlog.debug("user", "UserJB.getUsersDetails after Dao");
        } catch (Exception e) {
            Rlog.fatal("user", "Error in getUsersDetails in UserJB " + e);
        }
        return userDao;
    }

    /*
     * Returns the number of users in the account 'accId'
     */

    public int getUsersInAccount(int accId) {
        int rows = 0;
        try {
            UserAgentRObj userAgentRObj = EJBUtil.getUserAgentHome();
            rows = userAgentRObj.getUsersInAccount(accId);
            Rlog.debug("user", "UserJB.getUsersInAccount after Dao");
        } catch (Exception e) {
            Rlog.fatal("user", "Error in getUsersInAccount in UserJB " + e);
        }
        return rows;
    }

    /**
     * 
     * added by JM dt 27Jan05 Returns the number of Active users in the account
     * 'accId'
     */

    public int getActiveUsersInAccount(int accId) {
        int rows = 0;
        try {
            UserAgentRObj userAgentRObj = EJBUtil.getUserAgentHome();
            rows = userAgentRObj.getActiveUsersInAccount(accId);
            Rlog.debug("user", "UserJB.getActiveUsersInAccount after Dao");
        } catch (Exception e) {
            Rlog.fatal("user", "Error in getActiveUsersInAccount in UserJB "
                    + e);
        }
        return rows;
    }

    /*
     * generates a String of XML for the user details entry form.<br><br> NOT
     * IN USE
     */

    /**
     * NOT IN USE
     * 
     * @return
     */
    public String toXML() {
        Rlog.debug("user", "UserJB.toXML()");
        return new String(
                "<?xml version=\"1.0\"?>"
                        + "<?cocoon-process type=\"xslt\"?>"
                        + "<?xml-stylesheet href=\"/stylesheet/form-html.xsl\" type=\"text/xsl\"?>"
                        + "<?xml-stylesheet href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>"
                        +
                        // to be done "<form action=\"usersummary.jsp\">" +
                        "<head>"
                        +
                        // to be done "<title>User Summary </title>" +
                        "</head>"
                        +

                        "<input type=\"hidden\" name=\"userId\" value=\""
                        + this.getUserId()
                        + "\" size=\"10\"/>"
                        + "<input type=\"text\" name=\"userCodelstJobtype\" value=\""
                        + this.getUserCodelstJobtype()
                        + "\" size=\"10\"/>"
                        + "<input type=\"text\" name=\"userAccountId\" value=\""
                        + this.getUserAccountId()
                        + "\" size=\"10\"/>"
                        + "<input type=\"text\" name=\"userSiteId\" value=\""
                        + this.getUserSiteId()
                        + "\" size=\"10\"/>"
                        + "<input type=\"text\" name=\"userPerAddressId\" value=\""
                        + this.getUserPerAddressId()
                        + "\" size=\"38\"/>"
                        + "<input type=\"text\" name=\"userLastName\" value=\""
                        + this.getUserLastName()
                        + "\" size=\"30\"/>"
                        + "<input type=\"text\" name=\"userFirstName\" value=\""
                        + this.getUserFirstName()
                        + "\" size=\"30\"/>"
                        + "<input type=\"text\" name=\"userMidName \" value=\""
                        + this.getUserMidName()
                        + "\" size=\"30\"/>"
                        + "<input type=\"text\" name=\"userWrkExp\" value=\""
                        + this.getUserWrkExp()
                        + "\" size=\"100\"/>"
                        + "<input type=\"text\" name=\"userPhaseInv\" value=\""
                        + this.getUserPhaseInv()
                        + "\" size=\"100\"/>"
                        + "<input type=\"text\" name=\"userSessionTime\" value=\""
                        + this.getUserSessionTime()
                        + "\" size=\"3\"/>"
                        + "<input type=\"text\" name=\"userLoginName\" value=\""
                        + this.getUserLoginName()
                        + "\" size=\"20\"/>"
                        + "<input type=\"text\" name=\"userPwd\" value=\""
                        + this.getUserPwd()
                        + "\" size=\"20\"/>"
                        + "<input type=\"text\" name=\"userSecQues\" value=\""
                        + this.getUserSecQues()
                        + "\" size=\"150\"/>"
                        + "<input type=\"text\" name=\"userAnswer\" value=\""
                        + this.getUserAnswer()
                        + "\" size=\"150\"/>"
                        + "<input type=\"text\" name=\"userStatus\" value=\""
                        + this.getUserStatus()
                        + "\" size=\"1\"/>"
                        + "<input type=\"text\" name=\"userCodelstSpl\" value=\""
                        + this.getUserCodelstSpl()
                        + "\" size=\"10\"/>"
                        + "<input type=\"text\" name=\"userGrpDefault\" value=\""
                        + this.getUserGrpDefault() + "\" size=\"10\"/>"
                        + "</form>");

    }// end of method

    /**
     * Gets all Users in a Site(Organization)
     * 
     * @param siteId
     *            Site Id
     */
    public UserDao getSiteUsers(int siteId) {
        UserDao userDao = new UserDao();
        try {
            UserAgentRObj userAgentRObj = EJBUtil.getUserAgentHome();
            userDao = userAgentRObj.getSiteUsers(siteId);
            Rlog.debug("user", "UserJB.getSiteUsers after Dao");
        } catch (Exception e) {
            Rlog.fatal("user", "Error in getSiteUsers in UserJB " + e);
        }
        return userDao;
    }
    
    public String  getUserNamesFromIds(String ids)
    {
    	String usernames = "";
    	
    	try {
            UserAgentRObj userAgentRObj = EJBUtil.getUserAgentHome();
            usernames = userAgentRObj.getUserNamesFromIds(ids);
            Rlog.debug("user", "UserJB.getUserNamesFromIds after Dao");
        } catch (Exception e) {
            Rlog.fatal("user", "Error in getUserNamesFromIds in UserJB " + e);
        }
    	return usernames;
    	
    }

    public int findUser(String username) {
        UserBean usk = null;
        int success = 0;

        try {

            UserAgentRObj userAgentRObj = EJBUtil.getUserAgentHome();

            usk = userAgentRObj.findUser(username);

            Rlog.debug("user", "UserJB.findUser()");

        } catch (Exception e) {
            Rlog.fatal("user", "Error in findUser() in UserJB " + e);
            e.printStackTrace();
        }
        if (usk != null) {

            this.userId = usk.getUserId();
            this.userCodelstJobtype = usk.getUserCodelstJobtype();
            this.userAccountId = usk.getUserAccountId();
            this.userSiteId = usk.getUserSiteId();
            this.userPerAddressId = usk.getUserPerAddressId();
            this.userLastName = usk.getUserLastName();
            this.userFirstName = usk.getUserFirstName();
            this.userMidName = usk.getUserMidName();
            this.userWrkExp = usk.getUserWrkExp();
            this.userPhaseInv = usk.getUserPhaseInv();
            this.userSessionTime = usk.getUserSessionTime();
            this.userLoginName = usk.getUserLoginName();
            this.userPwd = usk.getUserPwd();
            this.userSecQues = usk.getUserSecQues();
            this.userAnswer = usk.getUserAnswer();
            this.userStatus = usk.getUserStatus();
            this.userCodelstSpl = usk.getUserCodelstSpl();
            this.userGrpDefault = usk.getUserGrpDefault();
            this.timeZoneId = usk.getTimeZoneId();
            this.userPwdExpiryDate = usk.getUserPwdExpiryDate();
            this.userPwdExpiryDays = usk.getUserPwdExpiryDays();
            this.userPwdReminderDate = usk.getUserPwdReminderDate();
            this.userESign = usk.getUserESign();
            this.userESignExpiryDate = usk.getUserESignExpiryDate();
            this.userESignExpiryDays = usk.getUserESignExpiryDays();
            this.userESignReminderDate = usk.getUserESignReminderDate();
            setUserLoginFlag(usk.getUserLoginFlag());
            setUserAttemptCount(usk.getUserAttemptCount());
            setUserSiteFlag(usk.getUserSiteFlag());
            this.setUserLoginModule(usk.getUserLoginModule());
            this.setUserLoginModuleMap(usk.getUserLoginModuleMap());
            this.setUserTheme(usk.getUserTheme());
            this.setUserSkin(usk.getUserSkin());
            Rlog.debug("user", "UserJB.usk.getUserType() in findUser"
                    + usk.getUserType());
            this.userType = usk.getUserType();
            this.userHidden = usk.getUserHidden();

        } else {
            success = -1;
        }
        return success;
    }

    public void getGroupUsers(String groupIds) {

        UserBean usk = null;
        try {

            UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
            usk = userAgent.findUser(groupIds);

            Rlog.debug("user", "UserJB.getGroupUsers()");

            
        } catch (Exception e) {
            Rlog.fatal("user", "Error in getGroupUsers() in UserJB " + e);
        }
    }

    public void getStudyUsers(String studyIds) {

        UserBean usk = null;
        try {

            UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
            usk = userAgent.findUser(studyIds);

            Rlog.debug("user", "UserJB.getStudyUsers()");

        } catch (Exception e) {
            Rlog.fatal("user", "Error in getStudyUsers() in UserJB " + e);
        }
    }

    public void getOrgUsers(String orgIds) {

        UserBean usk = null;
        try {

            UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
            usk = userAgent.findUser(orgIds);

            Rlog.debug("user", "UserJB.getOrgUsers()");

        } catch (Exception e) {
            Rlog.fatal("user", "Error in getOrgUsers() in UserJB " + e);
        }
    }

    /* To delete User */

    public int deleteUser() {
        int ret = 0;
        try {
            UserAgentRObj userAgentRObj = EJBUtil.getUserAgentHome();
            Rlog.debug("user", "UserJB. user Robj" + userAgentRObj);
            ret = userAgentRObj.deleteUser(this.createUserStateKeeper());
            Rlog.debug("user", "UserJB. ret" + ret);
            return ret;

        }

        catch (Exception e)

        {

            Rlog.fatal("team", "UserJB.deleteUser exception - " + e);

            return -2;

        }

    }
    
    public static void main(String[] args) {

		// Obtain a LoginContext, needed for authentication. Tell it
		// to use the LoginModule implementation specified by the
		// entry named "Sample" in the JAAS login configuration
		// file and to also use the specified CallbackHandler.
		LoginContext lc = null;
		ConnectObj connObj=new ConnectObj();;
		try {
			connObj.setAuthMode("NORMAL");
			connObj.setHost("66.237.42.87");
			connObj.setPort(389);
			connObj.setVelosUser("vabrol");
			connObj.setUserName("updateuser");
			connObj.setPassword("velosadmin");
			
			lc = new LoginContext("velos", new VelosCallBackHandler(connObj));
		//	lc = new LoginContext("Sample", new MyCallbackHandler());
		} catch (LoginException le) {
			System.err
					.println("Cannot create LoginContext. " + le.getMessage());
			System.exit(-1);
		} catch (SecurityException se) {
			System.err
					.println("Cannot create LoginContext. " + se.getMessage());
			System.exit(-1);
		}

		// the user has 3 attempts to authenticate successfully
		int i;
		
			try {

				// attempt authentication
				lc.login();

				

			} catch (Exception le) {
				le.printStackTrace();

				System.err.println("Authentication failed:");
				System.err.println("  " + le.getMessage());
				System.out.println("Sorry");
				System.exit(-1);
			
				try {
					Thread.currentThread().sleep(3000);
				} catch (Exception e) {
					// ignore
				}

			}
		

		// did they fail three times?
		
		

		System.out.println("Authentication succeeded!");

	}

    

//JM: 12/10/05    
    public void changeUserType(int userId, int group, String usrLoginName,String usrPass,String usrESign, String usrMail,int loggedInUser) {
    	try {
            UserAgentRObj userAgent = EJBUtil.getUserAgentHome();            
            userAgent.changeUserType(userId, group, usrLoginName, usrPass,usrESign,usrMail,loggedInUser);
            UserDao userDao=new UserDao();
            userDao.updateAuditRow(loggedInUser);
            Rlog.debug("user", "UserJB.changeUserType() in UserJB");
            
        } catch (Exception e) {
            Rlog.fatal("user", "UserJB.changeUserType  exception - " + e);
        }
    }


    /**
     * Calls getCount() of User Session Bean: UserAgentBean
     * 
     * @param userFname, userLname,  accIdand and userId
     * @return returns int for
     * @See UserDao
     */  
   
    public int getCount(String userLname, String userFname, String accId, String mode, String userId) {
        int count = 0;
        try {
            UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
            count = userAgent.getCount(userLname, userFname, accId, mode, userId);
            Rlog.debug("user", "UserJB.getCount after Dao");
        } catch (Exception e) {
            Rlog.fatal("user", "Error in getCount(String userFname, String userLname, String accId ) in UserJB "
                    + e);
        }
        return count;
    }
    
    
    /**
     * Calls getCount() of User Session Bean: UserAgentBean
     * 
     * @param email, accId, mode and userId
     * @return returns int for
     * @See UserDao
     */
    public int getCount(String email, String accId, String mode, String userId) {    	 
        int count = 0;
        try {
            UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
            count = userAgent.getCount(email, accId, mode,userId );
            Rlog.debug("user", "UserJB.getCount after Dao");
        } catch (Exception e) {
            Rlog.fatal("user", "Error in getCount(String email, String accId ) in UserJB "
                    + e);
        }
        return count;
    }    
   

    /**
     * Calls deactivateUser() on UserDao;
     * 
     * @param  userPk: user to be deactivated
     * @param creator: logged in user for audit
     * @param ipAdd: ip address of logged in user for audit
     *            
     * @return int returns success flag: 0:successful;-1:unsuccessful
     *         
     * @see UserDao
     */

    public int deactivateUser(String userPk, String creator, String ipAdd)
    {
    	 int ret = 0;
         try {
             UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
             ret = userAgent.deactivateUser( userPk,  creator,  ipAdd);
             
         } catch (Exception e) {
             Rlog.fatal("user", "Exception in deactivateUser(String userPk, String creator, String ipAdd)in UserJB "
                     + e);
         }
         return ret;
    	
    	
    }
    
    public int migratePass()
    {
   	 int count = 0;
   	 UserJB uJB=new UserJB();
        String tempPass="";     
        PreparedStatement pstmt = null;
        Connection conn = null;
        int id=0;
        try {
            conn = EnvUtil.getConnection();
            //KM-3257	
            pstmt = conn.prepareStatement("select pk_user from er_user order by pk_user asc");
                    ResultSet rs = pstmt.executeQuery();
   
           while (rs.next())
           {
           	 id=rs.getInt("PK_USER");
            	 uJB.setUserId(id);
            	 
            	 //System.out.println("Processing Id=" +id );
            	 
            	 uJB.getUserDetails();
            	 tempPass=Security.encryptSHA(Security.decrypt(uJB.getUserPwd()));
            	 uJB.setUserPwd(tempPass);
            	 uJB.updateUser();
            	 count++;
           }
            
   
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.migratePass EXCEPTION IN FETCHING FROM User table"
                            + ex);
            ex.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();               
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();                
            } catch (Exception e) {
            }
    
        }
        return count;
    }
        
    

    //APR-04-2011,MSG-TEMPLAGE FOR USER INFO V-CTMS-1
    

    /**
     * Calls notifyUserResetInfo on UserDao;
     * 
     * @param pWord: password of the user to be notified.
     * @param url: url information.
     * @param eSign: e signature of the user.
     * @param userId: user id information.
     * @param userLogin: login id of the user.               
              
     * @see UserDao
     */
    //Modified By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.
    public int notifyUserResetInfo(String pWord, String url,String eSign,String userId,String userLogin,String infoFlag, String loggedInUser){    
    	 int ret = 0;
         try {
             UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
             Integer userIdValue = new Integer(userId);
             ret = userAgent.notifyUserResetInfo(pWord,url,eSign,userIdValue,userLogin,infoFlag,loggedInUser);
             
         } catch (Exception e) {
             Rlog.fatal("user", "Exception in notifyUserResetInfoin UserJB "
                     + e);
         }
         return ret;
    	
    	
    }
    
    /**
     * To Check Password Rotation Error
     * Calls checkPasswordRotation on UserAgentBean;
     * 
     * @author Raviesh
     * @Enhancement INF-18669 
     * 
     * @param encryptedPass: New Password.
     * @param userId: user id information.
     * @param userAccId: Account id of the user.
              
     * @see UserAgentBean
     */
    public boolean checkPasswordRotation(String encryptedPass,Integer userId,Integer userAccId){
    	UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
    	return userAgent.checkPasswordRotation(encryptedPass,userId,userAccId);
    }
    
    
}// end of class
