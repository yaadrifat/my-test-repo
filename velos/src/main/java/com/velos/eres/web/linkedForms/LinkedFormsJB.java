/*
 * Classname : 				LinkedFormJB
 * 
 * Version information : 	1.0
 *
 
 * Date 					08/08/2003
 * 
 * Copyright notice: 		Velos Inc
 * 
 * Author 					Anu
 */

package com.velos.eres.web.linkedForms;

/* IMPORT STATEMENTS */

import java.util.ArrayList;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.velos.browser.Study;
import com.velos.eres.business.common.LinkedFormsDao;
import com.velos.eres.business.common.SaveFormDao;
import com.velos.eres.business.common.TeamDao;
import com.velos.eres.business.linkedForms.impl.LinkedFormsBean;
import com.velos.eres.business.studyRights.impl.StudyRightsBean;
import com.velos.eres.service.linkedFormsAgent.LinkedFormsAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.grpRights.GrpRightsJB;
import com.velos.eres.web.studyRights.StudyRightsJB;
import com.velos.eres.web.userSite.UserSiteJB;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for linked forms module. This class is used for any kind of
 * manipulation for the linked forms.
 * 
 * @author Anu Khanna
 * @version 1.0, 08/08/2003
 */

public class LinkedFormsJB {

    /**
     * the linkedform id
     */
    private int linkedFormId;

    /**
     * the form id
     */
    private String formLibId;

    /**
     * the form's display setting
     */
    private String lfDisplayType;

    /**
     * number of times the form can be filled
     */
    private String lfEntry;

    /**
     * the account id
     */
    private String accountId;

    /**
     * the study id
     */
    private String studyId;

    /**
     * to indicate location of association of form
     */
    private String lnkFrom;

    /**
     * the calender id
     */
    private String calenderId;

    /**
     * the event id
     */
    private String eventId;

    /**
     * the crf
     */
    private String crfId;

    /**
     * last modified by
     */
    private String lastModifiedBy;

    /**
     * the Creator
     */
    private String creator;
    
    /**
     * Creator of form response from Patient Portal
     */
    private String creatorForPortal;

    /**
     * the Ip address of the creator
     */
    private String ipAdd;

    /**
     * Record Type
     */
    private String recordType;

    /**
     * the data count
     */
    private String lfDataCnt;

    /**
     * Form Hide
     */

    private String lfHide;

    /**
     * Form Sequence
     */

    private String lfSeq;

    /**
     * Form to be Display in Patient Profile
     */

    private String lfDispInPat;

    /***************************************************************************
     * LinkedFormDao
     **************************************************************************/

    private LinkedFormsDao linkedFormsDao;
    
    /**
     * Form to be Display in Specimen module
     */

    private String lfDispInSpec;

    // GETTER SETTER METHODS

    public int getLinkedFormId() {
        return this.linkedFormId;
    }

    public void setLinkedFormId(int linkedFormId) {
        Rlog.debug("lnkform", "LinkedFormJB.setLinkedFormId() " + linkedFormId);
        this.linkedFormId = linkedFormId;
    }

    public String getAccountId() {
        return this.accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getCalenderId() {
        return this.calenderId;
    }

    public void setCalenderId(String calenderId) {
        this.calenderId = calenderId;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getCreatorForPortal() {
        return this.creatorForPortal;
    }

    public void setCreatorForPortal(String creatorForPortal) {
        this.creatorForPortal = creatorForPortal;
    }

    public String getCrfId() {
        return this.crfId;
    }

    public void setCrfId(String crfId) {
        this.crfId = crfId;
    }

    public String getEventId() {
        return this.eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getFormLibId() {
        return this.formLibId;
    }

    public void setFormLibId(String formLibId) {
        this.formLibId = formLibId;
    }

    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getLFDisplayType() {
        return this.lfDisplayType;
    }

    public void setLFDisplayType(String lfDisplayType) {
        this.lfDisplayType = lfDisplayType;
    }

    public String getLFEntry() {
        return this.lfEntry;
    }

    public void setLFEntry(String lfEntry) {
        this.lfEntry = lfEntry;
    }

    public String getLnkFrom() {
        return this.lnkFrom;
    }

    public void setLnkFrom(String lnkFrom) {
        this.lnkFrom = lnkFrom;
    }

    public String getRecordType() {
        return this.recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getStudyId() {
        return this.studyId;
    }

    public void setStudyId(String studyId) {
        Rlog.debug("lnkform", "****LinkedFormJB studyId" + studyId);
        this.studyId = studyId;
    }

    public String getLfDataCnt() {
        return this.lfDataCnt;
    }

    public void setLfDataCnt(String lfDataCnt) {
        this.lfDataCnt = lfDataCnt;
    }

    public String getLfHide() {
        return this.lfHide;
    }

    public void setLfHide(String lfHide) {
        this.lfHide = lfHide;
    }

    public String getLfSeq() {
        return this.lfSeq;
    }

    public void setLfSeq(String lfSeq) {
        this.lfSeq = lfSeq;
    }

    public String getLfDispInPat() {
        return this.lfDispInPat;
    }

    public void setLfDispInPat(String lfDispInPat) {
        this.lfDispInPat = lfDispInPat;
    }

    public LinkedFormsDao getLinkedFormsDao() {
        return this.linkedFormsDao;
    }

    public void setLinkedFormsDao(LinkedFormsDao linkedFormsDao) {
        this.linkedFormsDao = linkedFormsDao;
    }

	public String getLfDispInSpec() {
		return lfDispInSpec;
	}

	 
	public void setLfDispInSpec(String lfDispInSpec) {
		this.lfDispInSpec = lfDispInSpec;
	}
	
    // END OF GETTER SETTER METHODS

    /**
     * Defines an LinkedFormJB object with the specified LinkedForm Id
     */
    public LinkedFormsJB(int linkedFormId) {
        setLinkedFormId(linkedFormId);
    }

    /**
     * Defines an LinkedFormJB object with default values for LinkedForms
     */
    public LinkedFormsJB() {
        Rlog.debug("lnkform", "LinkedFormJB.LinkedFormJB() ");

    };

    /**
     * Defines an LinkedFormJB object with the specified values for the
     * LinkedForms
     * 
     */
    public LinkedFormsJB(int linkedFormId, String formLibId,
            String lfDisplayType, String lfEntry, String accountId,
            String studyId, String lnkFrom, String calenderId, String eventId,
            String crfId, String recordType, String creator,
            String lastModifiedBy, String ipAdd, String lfDataCnt,
            String lfHide, String lfSeq, String lfDispInPat,String lfdispinSpec) {
        setLinkedFormId(linkedFormId);
        setFormLibId(formLibId);
        setLFDisplayType(lfDisplayType);
        setLFEntry(lfEntry);
        setAccountId(accountId);
        setStudyId(studyId);
        setLnkFrom(lnkFrom);
        setCalenderId(calenderId);
        setEventId(eventId);
        setCrfId(crfId);
        setRecordType(recordType);
        setCreator(creator);
        setLastModifiedBy(lastModifiedBy);
        setIpAdd(ipAdd);
        setLfDataCnt(lfDataCnt);
        setLfHide(lfHide);
        setLfSeq(lfSeq);
        setLfDispInPat(lfDispInPat);
        setLfDispInSpec(lfdispinSpec);
        Rlog.debug("lnkform", "LinkedFormJB.LinkedFormJB(all parameters)");
    }

    /**
     * Retrieves the details of LinkedForm in LinkedFormStateKeeper Object
     * 
     * @return LinkedFormStateKeeper consisting of the LinkedForm details
     * @see LinkedFormStateKeeper
     */
    public LinkedFormsBean getLinkedFormDetails() {
        LinkedFormsBean lfsk = null;
        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            lfsk = linkedFormsAgentRObj.getLinkedFormDetails(this.linkedFormId);
            Rlog.debug("lnkform",
                    "LinkedFormJB.getLinkedFormDetails() LinkedFormStateKeeper "
                            + linkedFormId);
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Error in getLinkedFormDetails() in LinkedFormJB " + e);
        }
        if (lfsk != null) {
            this.linkedFormId = lfsk.getLinkedFormId();
            this.formLibId = lfsk.getFormLibId();
            this.lfDisplayType = lfsk.getLFDisplayType();
            this.lfEntry = lfsk.getLFEntry();
            this.accountId = lfsk.getAccountId();
            this.studyId = lfsk.getStudyId();
            Rlog.debug("lnkform",
                    "LinkedFormJB.getLinkedFormDetails() this.studyId "
                            + this.studyId);
            this.lnkFrom = lfsk.getLnkFrom();
            this.calenderId = lfsk.getCalenderId();
            this.eventId = lfsk.getEventId();
            this.crfId = lfsk.getCrfId();
            this.recordType = lfsk.getRecordType();
            this.creator = lfsk.getCreator();
            this.lastModifiedBy = lfsk.getLastModifiedBy();
            this.ipAdd = lfsk.getIpAdd();
            this.lfDataCnt = lfsk.getLfDataCnt();
            this.lfHide = lfsk.getLfHide();
            this.lfSeq = lfsk.getLfSeq();
            this.lfDispInPat = lfsk.getLfDispInPat();
            this.linkedFormsDao = lfsk.getLinkedFormsDao();
            setLfDispInSpec(lfsk.getLfDispInSpec());

        }
        return lfsk;
    }

    /**
     * Stores the details of field in the database
     */
    public void setLinkedFormDetails() {
        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            this.setLinkedFormId(linkedFormsAgentRObj.setLinkedFormDetails(this
                    .createLinkedFormStateKeeper()));
            Rlog.debug("lnkform", "LinkedFormJB.setLinkedFormDetails()");
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Error in setLinkedFormDetails() in LinkedFormJB " + e);
        }
    }

    /**
     * Saves the modified details of the LinkedForms to the database
     * 
     * @return 0 - If update succeeds <BR>
     *         -2 - If update fails
     */
    public int updateLinkedForm() {
        int output;

        LinkedFormsBean linkedFormsStateKeeper = new LinkedFormsBean();
        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsStateKeeper = this.createLinkedFormStateKeeper();
            linkedFormsStateKeeper.setLinkedFormsDao(getLinkedFormsDao());

            output = linkedFormsAgentRObj
                    .updateLinkedForm(linkedFormsStateKeeper);
        } catch (Exception e) {
            Rlog.fatal("lnkform", "Exception in updateLinkedForm " + e);
            return -2;
        }
        return output;
    }

    /**
     * Uses the values of the current LinkedFormJB object to create an
     * LinkedFormStateKeeper Object
     * 
     * @return LinkedFormStateKeeper object with all the details of the
     *         LinkedForm
     * @see LinkedFormStateKeeper
     */
    public LinkedFormsBean createLinkedFormStateKeeper() {

        return new LinkedFormsBean(linkedFormId, formLibId, lfDisplayType,
                lfEntry, accountId, studyId, lnkFrom, calenderId, eventId,
                crfId, lastModifiedBy, creator, ipAdd, recordType, lfDataCnt,
                lfHide, lfSeq, lfDispInPat,getLfDispInSpec());
    }

    /**
     * Method to get all the Forms in the Form Library and the Forms already
     * linked with a particular study
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getFormListForLinkToStudy(int accountId, int userId,
            String formName, int formType, int studyId, String siteIdAcc) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsDao = linkedFormsAgentRObj.getFormListForLinkToStudy(
                    accountId, userId, formName, formType, studyId, siteIdAcc);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getFormListForLinkToStudy in LinkedFormsJB "
                            + e);
            return null;
        }
    }

    /**
     * Method to get all the Forms linked with a particular study
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getStudyLinkedForms(int studyId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsDao = linkedFormsAgentRObj.getStudyLinkedForms(studyId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getFormListForLinkToStudy in LinkedFormsJB "
                            + e);
            return null;
        }
    }

    /**
     * Method to get only the Active Forms linked with a particular study
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getStudyLinkedActiveForms(int studyId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsDao = linkedFormsAgentRObj
                    .getStudyLinkedActiveForms(studyId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getStudyLinkedActiveForms in LinkedFormsJB "
                            + e);
            return null;
        }
    }

    /**
     * Method to get only the Active Forms linked with a particular study
     * 
     * @return LinkedFormsDao
     */
    public LinkedFormsDao getStudyLinkedActiveForms(int studyId, int accountId,
            int userId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsDao = linkedFormsAgentRObj.getStudyLinkedActiveForms(
                    studyId, accountId, userId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog
                    .fatal(
                            "lnkform",
                            "Exception in getStudyLinkedActiveForms(studyId,accountId,userId) in LinkedFormsJB "
                                    + e);
            return null;
        }
    }

    /**
     * Method to Link forms to a Study/Account multiple forms are linked to
     * study/account through a stored procedure All the form ids and respective
     * data is set in LinkedFormsDao attributes
     * 
     * @param linkedFormsDao
     * @returns -1 in case of error, 0 for success
     */
    public int linkFormsToStudyAccount(LinkedFormsDao linkedFormsDao) {
        int ret = 0;
        Rlog.debug("lnkform", "In linkFormsToStudyAccount in LinkedFormsJB");
        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            ret = linkedFormsAgentRObj.linkFormsToStudyAccount(linkedFormsDao);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in linkFormsToStudyAccount in LinkedFormsJB "
                            + e);
            return -1;
        }
    }

    // ////////////////////////////////////////
    /**
     * Method to get the list of patient specific forms and respective number of
     * times they have been filled
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getPatientForms(int accountId, int patient,
            int userId, int siteId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsDao = linkedFormsAgentRObj.getPatientForms(accountId,
                    patient, userId, siteId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getPatientForms in LinkedFormsJB " + e);
            return null;
        }
    }

    /**
     * Method to get the list of study specific forms for patient and respective
     * number of times they have been filled
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getPatientStudyForms(int accountId, int patId,
            int studyId, int userId, int siteId) {
    	return this.getPatientStudyForms(accountId, patId, studyId, userId, siteId, false);
    }
    
    public LinkedFormsDao getPatientStudyForms(int accountId, int patId,
            int studyId, int userId, int siteId, boolean includeHidden)

    {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsDao = linkedFormsAgentRObj.getPatientStudyForms(
                    accountId, patId, studyId, userId, siteId, includeHidden);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getPatientStudyForms in LinkedFormsJB " + e);
            return null;
        }
    }

    /**
     * 
     * Method to get the list of account specific forms and respective number of
     * times they have been filled
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getAccountForms(int accountId, int userId, int siteId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsDao = linkedFormsAgentRObj.getAccountForms(accountId,
                    userId, siteId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getAccountForms in LinkedFormsJB " + e);
            return null;
        }
    }
    public LinkedFormsDao getOrganisationForms(int accountId, int userId, int siteId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsDao = linkedFormsAgentRObj.getOrganisationForms(accountId,
                    userId, siteId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getOrganisationForms in LinkedFormsJB " + e);
            return null;
        }
    }
    
    public LinkedFormsDao getNetworkForms(int accountId, int userId, int siteId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsDao = linkedFormsAgentRObj.getNetworkForms(accountId,
                    userId, siteId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getNetoworkForms in LinkedFormsJB " + e);
            return null;
        }
    }
    public LinkedFormsDao getSNetworkForms(int accountId, int userId, int siteId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsDao = linkedFormsAgentRObj.getSNetworkForms(accountId,
                    userId, siteId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getNetoworkForms in LinkedFormsJB " + e);
            return null;
        }
    }
    
    public LinkedFormsDao getUserForms(int accountId, int userId, int siteId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsDao = linkedFormsAgentRObj.getUserForms(accountId,
                    userId, siteId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getAccountForms in LinkedFormsJB " + e);
            return null;
        }
    }
    public LinkedFormsDao getLinkedFormByFormId(int accountId, int formId, String type) {
    	try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();
            return linkedFormsAgentRObj.getLinkedFormByFormId(accountId, formId, type);
    	} catch (Exception e) {}
    	return null;
    }
    
    public String getFormResponseFkPer(int filledFormId) {
    	String fkPer = null;
    	try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();
            LinkedFormsDao linkedFormsDao = linkedFormsAgentRObj.getFormResponseFkPer(filledFormId);
            if (linkedFormsDao == null) { return null; }
            ArrayList creatorArray = linkedFormsDao.getCreator();
            if (creatorArray != null && creatorArray.size() > 0) {
            	setCreatorForPortal(new String((String)creatorArray.get(0)));
            	creatorArray.clear();
            }
        	ArrayList fkPerArray = linkedFormsDao.getFkPerArray();
        	if (fkPerArray != null && fkPerArray.size() > 0) {
        		fkPer = new String((String)fkPerArray.get(0));
            	fkPerArray.clear();
        	}
        	linkedFormsDao = null;
    	} catch (Exception e) {}
    	return fkPer;
    }
    
    public LinkedFormsDao getFormResponseCreator(int filledFormId, int formId) {
    	try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();
            return linkedFormsAgentRObj.getFormResponseCreator(filledFormId, formId);
    	} catch (Exception e) {}
    	return null;
    }
    
    public int getUserAccess(int formId, int userId, int creator) {
    	try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();
            return linkedFormsAgentRObj.getUserAccess(formId, userId, creator);
    	} catch (Exception e) {}
    	return 0;
    }
    
    public int getFilledFormUserAccess(int filledFormId, int formId, int userId) {
    	if (filledFormId < 1) { return 1; } // New form
    	if(formId<=0) { return 1; } // No form Response
    	LinkedFormsDao lfDaoForCreator = this.getFormResponseCreator(filledFormId, formId);
    	ArrayList creatorList = lfDaoForCreator.getCreator();
    	int creatorInt = 0;
    	if (creatorList != null) {
    		if (creatorList.size() == 1) {
    			creatorInt = Integer.parseInt((String)creatorList.get(0));
    		} else if (creatorList.size() > 1) {
    			for (int iX=0; iX<creatorList.size(); iX++) {
    				int aCreator = Integer.parseInt((String)creatorList.get(iX));
    				if (aCreator == userId) {
    					creatorInt = userId;
    					break;
    				}
    				creatorInt = aCreator;
    			}
    		}
    	}
    	if(creatorInt>0){
    		return this.getUserAccess(formId, userId, creatorInt);
    	}else{
    		return 0;
    	}
    }

    public int getFilledFormUserAccess(HttpServletRequest request) {
		int pageRight = 0;
    	try {
    		HttpSession tSession = request.getSession(true);
    		int iAccountId = StringUtil.stringToNum((String)tSession.getAttribute("accountId"));
    		if (iAccountId < 1) return pageRight;
    		
    		int formId = StringUtil.stringToNum((String)request.getParameter("formId"));
    		if (formId < 1) return pageRight;

    		String strStudyId = request.getParameter("studyId");
    		
    		//Bug #17189
    		if(strStudyId==null || strStudyId.isEmpty()){
    			strStudyId = request.getParameter("formStudy");
    		}
    		
    	    if (StringUtil.isEmpty(strStudyId)) {
    	    	strStudyId = request.getParameter("patStudyId");
    	    }
    	    int iStudyId = StringUtil.stringToNum(strStudyId);
    	    String fkPer = "0";
    	    UserSiteJB userSiteB = new UserSiteJB();
    	    int userId = StringUtil.stringToNum((String)tSession.getValue("userId"));
    	    if (userId < 1) return pageRight;

    	    int formFilledFormId = StringUtil.stringToNum(request.getParameter("formFilledFormId"));

    		LinkedFormsAgentRObj linkedFormAgent = EJBUtil.getLinkedFormAgentHome();
    		LinkedFormsBean linkedFormsBean = 
    			linkedFormAgent.findByFormId(formId);
			String linkedType = StringUtil.trueValue(linkedFormsBean.getLFDisplayType()).trim();

			if (iAccountId == StringUtil.stringToNum(linkedFormsBean.getAccountId())// compare with User type like SuperUser.
		    		&& iAccountId != 0) {
				TeamDao teamDao = new TeamDao();
				int teamRight = 0;
				boolean isAssociatedWithStudy=false;
				
	    	    if (iStudyId > 0) {
		    	    teamDao.getTeamRights(iStudyId,  userId);
		    	    ArrayList tId = teamDao.getTeamIds();
		    		StudyRightsJB stdTeamRights = null;		    		
		    		if (tId != null && tId.size() > 0) {
		    			stdTeamRights = new StudyRightsJB();
		    			stdTeamRights.setId(StringUtil.stringToNum(tId.get(0).toString()));
		    			ArrayList teamRights = teamDao.getTeamRights();
		    			stdTeamRights.setSuperRightsStringForStudy((String)teamRights.get(0));
		    			stdTeamRights.loadStudyRights();
		    			
		    			if(StringUtil.stringToNum(tId.get(0).toString())>0) isAssociatedWithStudy=true;
		    			
		    			/*
		    			StudyRightsBean srb=stdTeamRights.loadStudyRights();
		    			for(int i=1; i<teamRights.size();i++)
		    			{
		    				 srb=stdTeamRights.loadStudyRights((String)teamRights.get(i),srb);
		    			}
		    			*/
		    			
		    			
		    		}
		    		StudyRightsJB stdRits =(StudyRightsJB) tSession.getValue("studyRights");
		    		if(null != stdRits) {
		    			teamRight = Integer.parseInt(stdRits.getFtrRightsByValue("STUDYFRMACC"));
		    		}
		    		if(null != stdTeamRights) {
						if ((stdTeamRights.getFtrRights().size()) == 0) {
							teamRight = 0;
						} else {
							teamRight = Integer.parseInt(stdTeamRights.getFtrRightsByValue("STUDYFRMACC"));
						}
		    		}

	    	    }
	    		System.out.println("\n\n\t\tFinal Team Right.....::"+teamRight);
	    		System.out.println("isAssociatedWithStudy :: "+ isAssociatedWithStudy);
	    		System.out.println("..linkedType..........:: "+linkedType);
	    		System.out.println("\n\n.............................\n");
	    		
		    	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
				if ("A".equals(linkedType)) {
		    		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("ACTFRMSACC"));
		    		
		    	} else if ( "ORG".equals(linkedType) || "NTW".equals(linkedType)){
		    		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSITES"));
		    	}
		    	else if ( "USR".equals(linkedType))
		    	{
		    		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MUSERS"));
		    	}
				
				else if ("S".equals(linkedType) || "SNW".equals(linkedType)) 
		    	{// this is the area you have to work.for bug #17970....................
		    		int groupStudyRights = Integer.parseInt(grpRights.getFtrRightsByValue("STUDYFRMSACC"));
		        	StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
		        	
		        	try {
		        		if(isAssociatedWithStudy)
		        		{
		        			pageRight=teamRight;
		        		}else{
		        			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMACC"));
		        			pageRight &= groupStudyRights;
		        			pageRight &= teamRight;
		        			}
		    		} catch(Exception e) {
		    			pageRight = 0;
		    		}
		    		
		        }// this is the area you have to work.for bug #17970....................
				
		    	else if ("SA".equals(linkedType)) {
					pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("STUDYFRMSACC"));
					if (iStudyId > 0) {
						pageRight &= teamRight;
					}
		    	} else if ("P".equals(linkedType) || "PA".equals(linkedType) || "PS".equals(linkedType) || "PR".equals(linkedType)) {
		    	    if (iStudyId <= 0) {
		    	    	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("PATFRMSACC"));	    	    	
		    	    } else {
		    	    	pageRight = teamRight;
		    	    }
	
		    		if (formFilledFormId > 0) {	        		
		    			fkPer = getFormResponseFkPer(formFilledFormId);
					 	int orgRight = userSiteB.getUserPatientFacilityRight(
					 			userId, StringUtil.stringToNum(fkPer));
					 	pageRight &= orgRight;
		    		}
		    	} else if ("SP".equals(linkedType)) {
		    	   pageRight = teamRight;
		    		if (formFilledFormId > 0) {	        		
		    			fkPer = getFormResponseFkPer(formFilledFormId);

					 	int orgRight = userSiteB.getUserPatientFacilityRight(
					 			userId, StringUtil.stringToNum(fkPer));
					 	pageRight &= orgRight;
		    		}
		    	}
				
				if("SP".equals(linkedType) || "PS".equals(linkedType) || "PR".equals(linkedType)){
					if(pageRight>=0 && StringUtil.stringToNum(fkPer)!=0 ){
						pageRight = userSiteB.getMaxRightForStudyPatient(userId,StringUtil.stringToNum(fkPer),iStudyId);
						if(pageRight>0){
							pageRight = 7;
							}	
						}
				}
				
				// Check "Keep response private"
		   		int filledFormUserAccessInt = getFilledFormUserAccess(formFilledFormId, formId, userId);
				if (filledFormUserAccessInt < 1) {
					pageRight = 0;
		    	}
			}
    	} catch (Exception e) {
    		Rlog.fatal("lnkform", 
            		"Exception in getFilledFormUserAccess in LinkedFormsJB "+e);
    	}
    	return pageRight;
    }

    /**
     * Method to get the list of Filled study specific forms and respective
     * number of times they have been filled
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getStudyForms(int accountId, int studyId, int userId,
            int siteId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsDao = linkedFormsAgentRObj.getStudyForms(accountId,
                    studyId, userId, siteId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getStudyForms in LinkedFormsJB " + e);
            return null;
        }
    }

    /**
     * Overloaded getStudyForms Method to get the list of Filled study-specific forms
     * and respective number of times they have been filled according to the
     * right
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getStudyForms(int accountId, int studyId, int userId,
            int siteId, int grpRight, int stdRight, 
            boolean isIrb, String submissionType, String formSubtype) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsDao = linkedFormsAgentRObj.getStudyForms(accountId,
                    studyId, userId, siteId, grpRight, stdRight, 
                    isIrb, submissionType, formSubtype);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getStudyForms in LinkedFormsJB " + e);
            return null;
        }
    }

    /**
     * Overloaded getStudyForms Method to get the list of Filled study-specific non-IRB forms
     * and respective number of times they have been filled according to the
     * right
     * 
     * @return LinkedFormsDao
     */
    public LinkedFormsDao getStudyForms(int accountId, int studyId, int userId,
            int siteId, int grpRight, int stdRight) {
        return this.getStudyForms(accountId, studyId, userId,
                siteId, grpRight, stdRight, false, null, null);
    }

    /**
     * Method to get the list of forms related to a account also helps in
     * searching the forms in an account
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getFormsLinkedToAccount(int accountId,
            String formName, int studyId, String linkedTo, int siteId,
            int groupId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsDao = linkedFormsAgentRObj.getFormsLinkedToAccount(
                    accountId, formName, studyId, linkedTo, siteId, groupId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getFormsLinkedToAccount in LinkedFormsJB "
                            + e);
            return null;
        }
    }

    /**
     * Method to get the list of Active Forms related to a account
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getActiveFormsLinkedToAccount(int accountId,
            String linkedTo) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsDao = linkedFormsAgentRObj
                    .getActiveFormsLinkedToAccount(accountId, linkedTo);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getActiveFormsLinkedToAccount in LinkedFormsJB "
                            + e);
            return null;
        }
    }

    /**
     * Method to copy multiple/single forms of a study also helps in searching
     * the forms in an account
     * 
     * @return pk of the new form
     */

    public int copyFormsForStudy(String[] idArray, String formType,
            String studyId, String formName, String creator, String ipAdd) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
        int pkNewForm = 0;

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            pkNewForm = linkedFormsDao.copyFormsForStudy(idArray, formType,
                    studyId, formName, creator, ipAdd);
            return pkNewForm;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in copyFormsForStudy in LinkedFormsJB " + e);
            return -1;
        }
    }

    /**
     * Method to copy multiple/single forms of a account also helps in searching
     * the forms in an account
     * 
     * @return pk of the new form
     */

    public int copyFormsForAccount(String[] idArray, String formType,
            String formName, String creator, String ipAdd) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
        int pkNewForm = 0;

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            pkNewForm = linkedFormsDao.copyFormsForAccount(idArray, formType,
                    formName, creator, ipAdd);
            return pkNewForm;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in copyFormsForAccount in LinkedFormsJB " + e);
            return -1;
        }
    }

    /*
     * This method gets the Form HTML for preview @param formId Id of the form
     * for which html is required @returns html string
     */
    
    
    public String getFormHtmlforPreview(int formId) {
        int count = 0;
        String htmlstr = null;
        ;
        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            htmlstr = linkedFormsAgentRObj.getFormHtmlforPreview(formId);
            Rlog.debug("lnkform",
                    "LinkedFormJB.getFormHtmlforPreview after Dao");
            return htmlstr;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Error in getFormHtmlforPreview in LinkedFormJB " + e);
        }
        return htmlstr;
    }

    public String getFormHtmlforPreviewNoRole(int formId) {
        int count = 0;
        String htmlstr = null;
        ;
        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            htmlstr = linkedFormsAgentRObj.getFormHtmlforPreviewNoRole(formId);
            Rlog.debug("lnkform",
                    "LinkedFormJB.getFormHtmlforPreview after Dao");
            return htmlstr;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Error in getFormHtmlforPreview in LinkedFormJB " + e);
        }
        return htmlstr;
    }
    /*
     * This method gets the Form HTML for preview 
     * 
     * @param formId -Id of the form
     * @param htParam - Hastable of more parameters for filters
     * <br> Supported Filters
     * <br> teamRolePK String - Only for study/studypatient  based forms where we can identify logged in users study team role
     * for which html is required @returns html string
     */
    
    
    public String getFormHtmlforPreview(int formId,Hashtable moreParams) {
        int count = 0;
        String htmlstr = null;
        
        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            htmlstr = linkedFormsAgentRObj.getFormHtmlforPreview(formId, moreParams);
            Rlog.debug("lnkform",
                    "LinkedFormJB.getFormHtmlforPreview(int formId,Hashtable moreParams) after Dao");
            return htmlstr;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Error in getFormHtmlforPreview(int formId,Hashtable moreParams) in LinkedFormJB " + e);
        }
        return htmlstr;
    }

    /*
     * This method gets the Form HTML for filled form @param formId Id of the
     * form for which html is required @returns html string
     */

    public String getFilledFormHtml(int pkfilledform, String formdisplaytype) {
        String htmlstr = null;
        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            htmlstr = linkedFormsAgentRObj.getFilledFormHtml(pkfilledform,
                    formdisplaytype);
            Rlog.debug("lnkform",
                    "LinkedFormJB.getFormHtmlforPreview after Dao");
            return htmlstr;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Error in getFormHtmlforPreview in LinkedFormJB " + e);
        }
        return htmlstr;
    }
    
    public SaveFormDao getFilledFormHtml(int pkfilledform, String formdisplaytype,String flag) {
        String htmlstr = null;
        SaveFormDao saveDao=new SaveFormDao();
        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

             saveDao=linkedFormsAgentRObj.getFilledFormHtml(pkfilledform,
                    formdisplaytype,flag);
            Rlog.debug("lnkform",
                    "LinkedFormJB.getFormHtmlforPreview after Dao");
            return saveDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Error in getFormHtmlforPreview in LinkedFormJB " + e);
            
        }
        return saveDao;
    }
    public SaveFormDao getFilledFormHtmlNoRole(int pkfilledform, String formdisplaytype,String flag) {
        String htmlstr = null;
        SaveFormDao saveDao=new SaveFormDao();
        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

             saveDao=linkedFormsAgentRObj.getFilledFormHtmlNoRole(pkfilledform,
                    formdisplaytype,flag);
            Rlog.debug("lnkform",
                    "LinkedFormJB.getFormHtmlforPreview after Dao");
            return saveDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Error in getFormHtmlforPreview in LinkedFormJB " + e);
            
        }
        return saveDao;
    }
    /*
     * This method gets the Form HTML for filled form 
     * @param formId Id of the
     * form for which html is required @returns html string
     * @param formdisplaytype Display type flag for the form
     * @param flag - apparently not in use
     * @param htParam - Hastable of more parameters for filters
     * <br> Supported Filters
     * <br> teamRolePK String - Only for study/studypatient  based forms where we can identify logged in users study team role
     * for which html is required @returns html string
     */
      public SaveFormDao getFilledFormHtml(int pkfilledform, String formdisplaytype,String flag,Hashtable moreParams) {
        String htmlstr = null;
        SaveFormDao saveDao=new SaveFormDao();
        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

             saveDao=linkedFormsAgentRObj.getFilledFormHtml(pkfilledform,
                    formdisplaytype,flag,moreParams);
            Rlog.debug("lnkform",
                    "LinkedFormJB.getFilledFormHtml(int pkfilledform, String formdisplaytype,String flag,Hashtable moreParams) after Dao");
            return saveDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Error in getFilledFormHtml(int pkfilledform, String formdisplaytype,String flag,Hashtable moreParams) in LinkedFormJB " + e);
            
        }
        return saveDao;
    }

    
    /*
     * This method gets the Printer Friendly Form HTML for filled forms @param
     * formId Id of the form for which html is required @param filledFormId Id
     * of the form for which html is required @param linkFrom @returns html
     * string
     */

    public String getPrintFormHtml(int formId, int filledFormId, String linkFrom) {
        String htmlstr = null;
        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            htmlstr = linkedFormsAgentRObj.getPrintFormHtml(formId,
                    filledFormId, linkFrom);
            Rlog.debug("lnkform", "LinkedFormJB.getPrintFormHtml ");
            return htmlstr;
        } catch (Exception e) {
            Rlog.fatal("lnkform", "Error in getPrintFormHtml in LinkedFormJB "
                    + e);
        }
        return htmlstr;
    }

    /**
     * Method to insert values for forms linked to CRF in a calendar
     * 
     * @return 0 for success, -1 for error
     */

    public int insertCrfForms(String[] formIds, String accId,
            String[] crfNames, String[] crfNumbers, String calId,
            String eventId, String user, String ipAdd) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
        int pkLnkForm = 0;

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            pkLnkForm = linkedFormsDao.insertCrfForms(formIds, accId, crfNames,
                    crfNumbers, calId, eventId, user, ipAdd);
            return pkLnkForm;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in insertCrfForms in LinkedFormsJB " + e);
            return -1;
        }
    }

    public LinkedFormsBean findByCrfEventId(int crfId, int eventId) {
        LinkedFormsBean lnkFormsSk = null;
        try {

            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            lnkFormsSk = linkedFormsAgentRObj.findByCrfEventId(crfId, eventId);

        } catch (Exception e) {
            Rlog.fatal("lnkform", "Error in lnkform findByCrfEventId" + e);
        }

        if (lnkFormsSk != null) {
            this.linkedFormId = lnkFormsSk.getLinkedFormId();
            // Rlog.debug("lnkform","LinkedFormJB lnkFormsSk.getLinkedFormId()
            // "+lnkFormsSk.getLinkedFormId());
            this.lfDataCnt = lnkFormsSk.getLfDataCnt();
            this.lfHide = lnkFormsSk.getLfHide();
            this.lfSeq = lnkFormsSk.getLfSeq();
            this.lfDispInPat = lnkFormsSk.getLfDispInPat();
            setLfDispInSpec(lnkFormsSk.getLfDispInSpec());

        }

        return lnkFormsSk;

    }

    public LinkedFormsBean findByFormId(int formId) {
        LinkedFormsBean lnkFormsSk = null;
        try {

            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            lnkFormsSk = linkedFormsAgentRObj.findByFormId(formId);

        } catch (Exception e) {
            Rlog.fatal("lnkform", "Error in lnkform findByFormId" + e);
        }

        if (lnkFormsSk != null) {
            this.linkedFormId = lnkFormsSk.getLinkedFormId();
            this.lfDataCnt = lnkFormsSk.getLfDataCnt();
            this.lfHide = lnkFormsSk.getLfHide();
            this.lfSeq = lnkFormsSk.getLfSeq();
            this.lfDispInPat = lnkFormsSk.getLfDispInPat();
            this.linkedFormId = lnkFormsSk.getLinkedFormId();
            this.calenderId = lnkFormsSk.getCalenderId();
            this.crfId = lnkFormsSk.getCrfId();
            this.eventId = lnkFormsSk.getEventId();
            this.lfDisplayType = lnkFormsSk.getLFDisplayType();
            this.lfEntry = lnkFormsSk.getLFEntry();
            this.studyId = lnkFormsSk.getStudyId();
            this.lnkFrom = lnkFormsSk.getLnkFrom();
            setRecordType(lnkFormsSk.getRecordType());
            
            //System.out.println("lnkFormsSk.getLfDispInSpec()" + lnkFormsSk.getLfDispInSpec());
            
            
            setLfDispInSpec(lnkFormsSk.getLfDispInSpec());

        }

        return lnkFormsSk;

    }

    /*
     * Method to update the hide and sequence fields of Linked Forms
     */
    public int updateHideSeqOfLF(String[] pkLF, String[] hide, String[] seq,
            String[] patProfile, String ipAdd, int user , String[] dispInSpec) {
        int ret = 0;

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            ret = linkedFormsAgentRObj.updateHideSeqOfLF(pkLF, hide, seq,
                    patProfile, ipAdd, user, dispInSpec);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in updateHideSeqOfLF in LinkedFormsJB " + e);
            return -1;
        }
    }

    /*
     * Method to delete filled Form response
     */
    public int deleteFilledFormResp(String filledFormId,
            String formDispLocation, String lastModifiedBy, String ipAdd) {
        int ret = 0;

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            ret = linkedFormsAgentRObj.deleteFilledFormResp(filledFormId,
                    formDispLocation, lastModifiedBy, ipAdd);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in deleteFilledFormResp in LinkedFormsJB " + e);
            return -1;
        }
    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int deleteFilledFormResp(String filledFormId,String formDispLocation,Hashtable<String, String> auditInfo) {
        int ret = 0;

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            ret = linkedFormsAgentRObj.deleteFilledFormResp(filledFormId,
                    formDispLocation,auditInfo);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in deleteFilledFormResp(String filledFormId,String formDispLocation,Hashtable<String, String> auditInfo) in LinkedFormsJB " + e);
            return -1;
        }
    }
    

    //added by sonia Abrol, to check if user has right a form
    
    /** checks if user has access to the form (evenif the form is hidden)
     * 
     * @param accountId
     *            The form Id
     * @param userId
     *            The user for which access rights are checked
     * 
     */

    public boolean hasFormAccess(int formId, int userId )
    {
    	  try {
              LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                      .getLinkedFormAgentHome();

              return linkedFormsAgentRObj.hasFormAccess(formId,userId);
              
          } catch (Exception e) {
              Rlog.fatal("lnkform",
                      "Exception in hasFormAccess in LinkedFormsJB " + e);
              return false;
          }
    	
    }
    /** Get the Study Active / Lockdonw forms for the study and patient (All Studies)
     * 
     * @param StudyId
     *            
     * @param accountId
     *            
     * 
     */
    public LinkedFormsDao getStudyActiveLockdownForms(int studyId, int accountId){
    	try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            return linkedFormsAgentRObj.getStudyActiveLockdownForms(studyId,accountId);
            
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getStudyActiveLockdownForms in LinkedFormsJB " + e);
            return null;
        }
    }
    
    
    /**
     * 
     * Method to get the list of account specific forms and respective number of
     * times they have been filled
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getAccountFormsForSpecimen(int accountId, int userId, int siteId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsDao = linkedFormsAgentRObj.getAccountFormsForSpecimen(accountId,
                    userId, siteId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getAccountFormsForSpecimen in LinkedFormsJB " + e);
            return null;
        }
    }

    /**
     * Overloaded method Method to get the list of Filled study specific forms
     * and respective number of times they have been filled according to the
     * right. looks for forms marked as 'display in specimen management'
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getStudyFormsForSpecimen(int accountId, int studyId, int userId,
            int siteId, int grpRight, int stdRight) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsDao = linkedFormsAgentRObj.getStudyFormsForSpecimen(accountId,
                    studyId, userId, siteId, grpRight, stdRight);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getStudyFormsForSpecimen in LinkedFormsJB " + e);
            return null;
        }
    }

    
    /**
     * Method to get the list of patient specific forms and respective number of
     * times they have been filled
     * looks for forms marked as 'display in specimen management'
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getPatientFormsForSpecimen(int accountId, int patient,
            int userId, int siteId) 
    {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsDao = linkedFormsAgentRObj.getPatientFormsForSpecimen(accountId,
                    patient, userId, siteId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getPatientFormsForSpecimen in LinkedFormsJB " + e);
            return null;
        }
    }

    /**
     * Method to get the list of study specific forms for patient and respective
     * number of times they have been filled
     * looks for forms marked as 'display in specimen management'
     * 
     * @return LinkedFormsDao
     */
    

    public LinkedFormsDao getPatientStudyFormsForSpecimen(int accountId, int patId,
            int studyId, int userId, int siteId)

    {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsDao = linkedFormsAgentRObj.getPatientStudyFormsForSpecimen(
                    accountId, patId, studyId, userId, siteId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getPatientStudyFormsForSpecimen in LinkedFormsJB " + e);
            return null;
        }
    }


    /** get latest response for a study form 
     *  @author Sonia Abrol
     *  @return Hashtable of data with response attributes
     *  	possible keys:
     *  
     *  	formlibver - form version for the response
     *     pkstudyforms - response PK
     *     
     *  07/02/09 */
       
    
   	public Hashtable getLatestStudyFormResponeData(String formId, String studyId) 
   	{
   		Hashtable htReturn = new Hashtable();
   		
   		try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            htReturn = linkedFormsAgentRObj.getLatestStudyFormResponeData(formId, studyId);
            return htReturn;
            
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getLatestStudyFormResponeData in LinkedFormsJB " + e);
            
            return null;
        }

   		
   	}
   	
    public LinkedFormsDao getStudyFormsWithResponseData(int accountId, int studyId, int userId,
            int siteId, int grpRight, int stdRight, 
            boolean isIrb, String submissionType, String formSubtype)

    {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsDao = linkedFormsAgentRObj.getStudyFormsWithResponseData(accountId, studyId, userId,
                    siteId, grpRight, stdRight, 
                    isIrb, submissionType, formSubtype);
            
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getStudyFormsWithResponseData in LinkedFormsJB " + e);
            return null;
        }
    }

    /**
     * Method to get the list of study specific forms for patient and respective
     * number of times they have been filled
     * 
     * @return LinkedFormsDao
     */
    /*Added For Bug# 9255 :Date 14 May 2012 :By YPS*/
    public LinkedFormsDao getPatStdFormsForPatRestrAndSingleEntry(int accountId, int patId,
            int studyId, int userId, int siteId,Hashtable<String, String> htParams)

    {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil
                    .getLinkedFormAgentHome();

            linkedFormsDao = linkedFormsAgentRObj.getPatStdFormsForPatRestrAndSingleEntry(
                    accountId, patId, studyId, userId, siteId,htParams);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getPatStdFormsForPatRestrAndSingleEntry in LinkedFormsJB " + e);
            return null;
        }
    }

    public LinkedFormsDao getFormResponseAuditInfo(int formId, int filledFormId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
        try {
            LinkedFormsAgentRObj linkedFormsAgentRObj = EJBUtil.getLinkedFormAgentHome();
            linkedFormsDao = linkedFormsAgentRObj.getFormResponseAuditInfo(formId, filledFormId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getFormResponseAuditInfo in LinkedFormsJB " + e);
            return null;
        }
    }
    //end of class
}

