/*
 * Classname : ConfigFacade
 * 
 * Version information: 1.0
 *
 * Date: 02/17/2005
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: 
 */

package com.velos.eres.web.user;

/* IMPORT STATEMENTS */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.velos.eres.business.common.CustomValidationDao;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */ 

/**
 * Singleton class for ConfigFacade
 */

public class ConfigFacade {

    public HashMap hashmap;

    private static ConfigFacade configFacade;

    CustomValidationDao customDAO;

    private ConfigFacade() {
        hashmap = new HashMap();
        customDAO = new CustomValidationDao(); 
        ConfigObject conobj = new ConfigObject();
        customDAO.getPageCustomFields(); 
        List accList = customDAO.getPkPageCustom();
        if (accList != null && accList.size() > 0) {
            int oldAccId = 0;
//          JM: 03July2006
            ArrayList alAccId = new ArrayList();       
            
            ArrayList alPkId = new ArrayList();
            ArrayList alPageName = new ArrayList();
            
            
            for (int i = 0; i < accList.size(); i++) {
                try {
                    if (oldAccId == 0) {
                        oldAccId = Integer.parseInt(((String) customDAO
                                .getFkAccount().get(i)));
                       
                    }
                    if (oldAccId != Integer.parseInt(((String) customDAO
                            .getFkAccount().get(i)))) {
                    	
                    	
                    	conobj.setAccountId(alAccId);//JM: 03July2006
                    	
                    	conobj.setPrimaryKey(alPkId);
                        conobj.setPageName(alPageName);
                        hashmap.put(String.valueOf(oldAccId), conobj);
                        oldAccId = Integer.parseInt(((String) customDAO
                                .getFkAccount().get(i)));
                        conobj = new ConfigObject();
                        alPkId = new ArrayList();
                        alPageName = new ArrayList();

                    }
                    
                   
                    alAccId.add(customDAO.getFkAccount().get(i));//JM: 03July2006
                    
                    alPkId.add(customDAO.getPkPageCustom().get(i));
                    alPageName.add(customDAO.getPageCustomPage().get(i));
                } catch (Exception ex) {
                    Rlog.fatal("ConfigFacade",
                            "ConfigFacade constructor error : " + ex);
                }
            }
            conobj.setAccountId(alAccId);//JM: 03July2006
           
            conobj.setPrimaryKey(alPkId);
            conobj.setPageName(alPageName);
            hashmap.put(String.valueOf(oldAccId), conobj);
        }
    }

    public static synchronized ConfigFacade getConfigFacade() {

        if (configFacade == null) {
            configFacade = new ConfigFacade();
        }
        return configFacade;
    }

    /**
     * ConfigFacade for getting Custom Information @ PrimaryKey which the Custom
     * Details fields fetched @ Return the customDetailsList 
     */

    public ConfigDetailsObject populateObject(int accId, String pageName) {
        int pkId = 0;
        // JM: 17Jul2006
        customDAO = new CustomValidationDao(); 
        
        ConfigDetailsObject configDetails = new ConfigDetailsObject();        
        ArrayList customDetailsList = new ArrayList(1);
        try {
            
        	
        	if (hashmap.containsKey(String.valueOf(accId)) == true) {
            	
                ConfigObject conObj = (ConfigObject) hashmap.get(String
                        .valueOf(accId));

                for (int i = 0; i < conObj.primaryKey.size(); i++) {
                    if (pageName.equals(conObj.getPageName().get(i))) {
                        pkId = Integer.parseInt((String) conObj.getPrimaryKey()
                                .get(i));
                        customDAO.getPageCustomDetails(pkId);
                        if (customDAO.getPagecustomfldsfield() != null
                                && customDAO.getPagecustomfldsfield().size() > 0) {
                            configDetails.setPcfId(customDAO.getPkpagecustomflds());                                                    
                            configDetails.setPcfField(customDAO.getPagecustomfldsfield());                       
                            configDetails.setPcfAttribute(customDAO.getPagecustomfldsattribute());                       
                            configDetails.setPcfMandatory(customDAO.getPagecustomfldsmandatory());
                            configDetails.setPcfLabel(customDAO.getPagecustomfldslabel()); //KM-042308
                        }

                        break;
                    }
                }
            }
        } catch (Exception ex) {
            Rlog.fatal("customaccount", "ConfigFacade PopulateObject method"
                    + ex);
        }
        return configDetails;
    }

    /**
     * ConfigFacade for getting Custom Information
     * 
     * @return customDetailsList
     * 
     * Currently this method is not in use
     * 
     * public ArrayList getCustomDetails(int primaryKey){ ArrayList
     * customDetailsList = new ArrayList(1); try{ CustomValidationDao customDAO =
     * new CustomValidationDao(); customDetailsList =
     * customDAO.getPageCustomDetails(primaryKey); }catch(Exception ex){
     * Rlog.fatal("customaccount","CustomValidationDao.getCustomDetails
     * EXCEPTION IN FETCHING FROM er_pagecustomflds" + ex); } return
     * customDetailsList; }
     */

}