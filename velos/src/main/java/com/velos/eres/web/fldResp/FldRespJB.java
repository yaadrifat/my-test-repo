/*
 * Classname : 				FldRespJB
 *
 * Version information : 	1.0
 *

 * Date 					07/02/2003
 *
 * Copyright notice: 		Velos Inc
 *
 * Author 					Anu
 */

package com.velos.eres.web.fldResp;

/* IMPORT STATEMENTS */

import java.util.Hashtable;
import com.velos.eres.business.common.FldRespDao;
import com.velos.eres.business.fldResp.impl.FldRespBean;
import com.velos.eres.service.fldRespAgent.FldRespAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for field response module. This class is used for any kind
 * of manipulation for the field.
 *
 * @author Anu Khanna
 * @version 1.0, 07/02/2003
 */

public class FldRespJB {

    /*
     * Field Id
     */
    private int fldRespId;

    /*
     * field
     */
    private String field;

    /*
     * field sequence
     */
    private String seq;

    /*
     * display value for the field
     */
    private String dispVal;

    /*
     * data value for the field
     */
    private String dataVal;

    /*
     * score
     */
    private String score;

    /*
     * Default response for the field
     */
    private String fldRespDefault;

    /*
     * field creator
     */
    private String creator;

    /*
     * field last modified by
     */
    private String lastModifiedBy;

    /*
     * IP address
     */
    private String ipAdd;

    /*
     * Record Type
     */
    private String recordType;

    // GETTER SETTER METHODS

    public int getFldRespId() {
        return this.fldRespId;
    }

    public void setFldRespId(int fldRespId) {
        this.fldRespId = fldRespId;
    }

    public String getField() {
        return this.field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getSequence() {
        return this.seq;
    }

    public void setSequence(String seq) {
        this.seq = seq;
    }

    public String getDispVal() {
        return this.dispVal;
    }

    public void setDispVal(String dispVal) {
        this.dispVal = dispVal;

    }

    public String getDataVal() {
        return this.dataVal;
    }

    public void setDataVal(String dataVal) {
        this.dataVal = dataVal;
    }

    public String getScore() {
        return this.score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getFldRespDefault() {
        return this.fldRespDefault;
    }

    public void setFldRespDefault(String fldRespDefault) {
        this.fldRespDefault = fldRespDefault;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getIPAdd() {
        return this.ipAdd;
    }

    public void setIPAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public String getRecordType() {
        return this.recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Defines an FldRespJB object with the specified field Id
     */
    public FldRespJB(int fldRespId) {
        setFldRespId(fldRespId);
    }

    /**
     * Defines an FldRespJB object with default values for fields
     */
    public FldRespJB() {
        Rlog.debug("fldresp", "FldRespJB.FldRespJB() ");
    };

    /**
     * Defines an FldRespJB object with the specified values for the fields
     *
     */
    public FldRespJB(int fldRespId, String field, String seq, String dispVal,
            String dataVal, String score, String fldRespDefault,
            String creator, String lastModifiedBy, String ipAdd,
            String recordType) {
        setFldRespId(fldRespId);
        setField(field);
        setSequence(seq);
        setDispVal(dispVal);
        setDataVal(dataVal);
        setScore(score);
        setFldRespDefault(fldRespDefault);
        setCreator(creator);
        setLastModifiedBy(lastModifiedBy);
        setIPAdd(ipAdd);
        setRecordType(recordType);
        Rlog.debug("fldresp", "FldRespJB.FldRespJB(all parameters)");
    }

    /**
     * Retrieves the details of field in FldRespStateKeeper Object
     *
     * @return FldRespStateKeeper consisting of the field details
     * @see FldRespStateKeeper
     */
    public FldRespBean getFldRespDetails() {
        FldRespBean frsk = null;
        try {
            FldRespAgentRObj fldRespAgentRObj = EJBUtil.getFldRespAgentHome();
            frsk = fldRespAgentRObj.getFldRespDetails(this.fldRespId);
            Rlog.debug("fldresp",
                    "FldRespJB.getFldRespDetails() FldRespStateKeeper " + frsk);
        } catch (Exception e) {
            Rlog.fatal("fldresp", "Error in getFldRespDetails() in FldRespJB "
                    + e);
        }
        if (frsk != null) {
            this.fldRespId = frsk.getFldRespId();
            this.field = frsk.getField();
            this.seq = frsk.getSequence();
            this.dispVal = frsk.getDispVal();
            this.dataVal = frsk.getDataVal();
            this.score = frsk.getScore();
            this.creator = frsk.getCreator();
            this.fldRespDefault = frsk.getFldRespDefault();
            this.lastModifiedBy = frsk.getLastModifiedBy();
            this.ipAdd = frsk.getIPAdd();
            this.recordType = frsk.getRecordType();
        }
        return frsk;
    }

    /**
     * Stores the details of field in the database
     */
    public void setFldRespDetails() {
        try {
            FldRespAgentRObj fldRespAgentRObj = EJBUtil.getFldRespAgentHome();

            this.setFldRespId(fldRespAgentRObj.setFldRespDetails(this
                    .createFldRespStateKeeper()));
            Rlog.debug("fldresp", "FldRespJB.setFldRespDetails()");
        } catch (Exception e) {
            Rlog.fatal("fldresp", "Error in setFldRespDetails() in FldRespJB "
                    + e);
        }
    }

    /**
     * Saves the modified details of the field to the database
     *
     * @return 0 - If update succeeds <BR>
     *         -2 - If update fails
     */
    public int updateFldResp() {
        int output;
        try {
            FldRespAgentRObj fldRespAgentRObj = EJBUtil.getFldRespAgentHome();
            output = fldRespAgentRObj.updateFldResp(this
                    .createFldRespStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("fldresp", "Exception in updateFldResp " + e);
            return -2;
        }
        return output;
    }

    /**
     * Uses the values of the current FldRespJB object to create an
     * FldRespStateKeeper Object
     *
     * @return FldRespStateKeeper object with all the details of the field
     * @see FldRespStateKeeper
     */
    public FldRespBean createFldRespStateKeeper() {
        Rlog.debug("fldresp", "FldRespJB.createFldRespStateKeeper ");

        return new FldRespBean(fldRespId, field, seq, dispVal, dataVal, score,
                fldRespDefault, creator, lastModifiedBy, ipAdd, recordType);
    }

    /**
     * Calls getResponsesForField of fldResp Session Bean: FldRespAgentBean
     *
     * @param field
     * @return FldRespDao
     * @see FldRespDao
     */
    public FldRespDao getResponsesForField(int field) {
        FldRespDao fldRespDao = new FldRespDao();
        try {
            Rlog.debug("fldresp", "FldRespJB.getResponsesForField starting");
            FldRespAgentRObj fldRespAgentRObj = EJBUtil.getFldRespAgentHome();

            fldRespDao = fldRespAgentRObj.getResponsesForField(field);
            return fldRespDao;
        } catch (Exception e) {
            Rlog.fatal("fldresp", "Error in getResponsesForField in FldRespJB "
                    + e);
        }
        return fldRespDao;
    }

    public int insertMultipleResponses(FldRespDao fldRespDao) {

        try {
            Rlog.debug("fldresp", "FldRespJB.insertMultipleResponses starting");
            FldRespAgentRObj fldRespAgentRObj = EJBUtil.getFldRespAgentHome();
            fldRespAgentRObj.insertMultipleResponses(fldRespDao);
        } catch (Exception e) {
            Rlog.fatal("fldresp",
                    "Error in insertMultipleResponses in FldRespJB " + e);
            return -2;
        }
        return 1;
    }

    public int updateMultipleResponses(FldRespDao fldRespDao) {
        try {
            Rlog.debug("fldresp", "FldRespJB.updateMultipleResponses starting");
            FldRespAgentRObj fldRespAgentRObj = EJBUtil.getFldRespAgentHome();

            fldRespAgentRObj.updateMultipleResponses(fldRespDao);
        } catch (Exception e) {
            Rlog.fatal("fldresp",
                    "Error in updateMultipleResponses in FldRespJB " + e);
            return -2;
        }
        return 1;
    }

    public FldRespDao getMaxSeqNumForMulChoiceFld(int formfldId) {
        FldRespDao fldRespDao = new FldRespDao();
        try {
            Rlog.debug("fldresp",
                    "FldRespJB.getMaxSeqNumForMulChoiceFld starting");
            FldRespAgentRObj fldRespAgentRObj = EJBUtil.getFldRespAgentHome();

            fldRespDao = fldRespAgentRObj
                    .getMaxSeqNumForMulChoiceFld(formfldId);
            return fldRespDao;
        } catch (Exception e) {
            Rlog.fatal("fldresp", "Error in formfld in FldRespJB " + e);
        }
        return fldRespDao;

    }

    public FldRespDao getMaxRespSeqForFldInLib(int fldId) {
        FldRespDao fldRespDao = new FldRespDao();
        try {
            Rlog
                    .debug("fldresp",
                            "FldRespJB.getMaxRespSeqForFldInLib starting");
            FldRespAgentRObj fldRespAgentRObj = EJBUtil.getFldRespAgentHome();

            fldRespDao = fldRespAgentRObj.getMaxRespSeqForFldInLib(fldId);
            return fldRespDao;
        } catch (Exception e) {
            Rlog.fatal("fldresp", "Error in fld in FldRespJB " + e);
        }
        return fldRespDao;

    }



//JM: 24Jun2008, added for the issue #3309 Need to delete record physically from ER_FLDRESP on deletion.

	public int deleteFldResp(int fldRespId){

		int retme = 0;

		try {
            Rlog.debug("fldresp", "FldRespJB.deleteFldResp() starting");
            FldRespAgentRObj fldRespAgentRObj = EJBUtil.getFldRespAgentHome();

            retme = fldRespAgentRObj.deleteFldResp(fldRespId);
        } catch (Exception e) {
            Rlog.fatal("fldresp",
                    "Error in deleteFldResp() in FldRespJB " + e);
            return -2;
        }
        return retme;

	}
	
	// deleteFldResp() Overloaded for INF-18183 ::: Raviesh
	public int deleteFldResp(int fldRespId,Hashtable<String, String> args){

		int retme = 0;

		try {
            Rlog.debug("fldresp", "FldRespJB.deleteFldResp() starting");
            FldRespAgentRObj fldRespAgentRObj = EJBUtil.getFldRespAgentHome();

            retme = fldRespAgentRObj.deleteFldResp(fldRespId,args);
        } catch (Exception e) {
            Rlog.fatal("fldresp",
                    "Error in deleteFldResp() in FldRespJB " + e);
            return -2;
        }
        return retme;

	}

}//end of class
