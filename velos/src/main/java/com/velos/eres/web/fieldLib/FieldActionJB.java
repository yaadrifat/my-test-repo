package com.velos.eres.web.fieldLib;

import com.velos.eres.business.common.FieldActionDao;
import com.velos.eres.business.common.FieldLibDao;

public class FieldActionJB {
	public FieldLibDao getFieldsForFieldAction(int formId) {
		FieldActionDao fieldActionDao = new FieldActionDao();
		fieldActionDao.getFieldsForFieldAction(formId);
		
		FieldLibDao fieldLibDao = new FieldLibDao();
		int numRows = fieldActionDao.getCRows();
		if (numRows < 1) { return fieldLibDao; }
		fieldLibDao.setCRows(numRows);
		fieldLibDao.setFieldLibId(fieldActionDao.getFldIds());
		fieldLibDao.setFldUniqId(fieldActionDao.getFldUniqIds());
		fieldLibDao.setFldName(fieldActionDao.getFldNames());
		fieldLibDao.setFldType(fieldActionDao.getFldTypes());
		fieldLibDao.setFldDataType(fieldActionDao.getFldDataTypes());
		fieldLibDao.setFldSysID(fieldActionDao.getFldSysIds());
		return fieldLibDao;
	}
}
