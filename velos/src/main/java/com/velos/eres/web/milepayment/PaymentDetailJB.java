/*
 * Classname : 				PaymentDetailJB
 * 
 * Version information : 	1.0
 *
 * Date 					10/18/2005
 * 
 * Copyright notice: 		Velos Inc
 * 
 * Author 					Sonia Abrol
 */

package com.velos.eres.web.milepayment;

/* IMPORT STATEMENTS */





import javax.persistence.Column;

import com.velos.eres.business.common.PaymentDetailsDao;
import com.velos.eres.business.milepayment.impl.PaymentDetailBean;
import com.velos.eres.service.milepaymentAgent.PaymentDetailAgentRObj;

import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for PaymentDetail module.
 * 
 * @author Sonia Abrol
 * @version 1.0, 10/18/2005
 */

public class PaymentDetailJB {
	
	/**
	 * the PaymentDetail Primary Key
	 */
	public int id;

	/**
	 * the payment
	 */
	public String fkPayment;

	/**
	 * payment detail linked to
	 */
	public String linkedTo;

	/**
	 * the payment link to id
	 */
	public String linkToId;

	/**
	 * the payment detail amount
	 */
	public String amount;

	/**
	 * the id of the user who created the record
	 */
	public String creator;

	/**
	 * Id of the user who last modified the record
	 */
	public String lastModifiedBy;

	/**
	 * the IP address of the user who created or modified the record
	 */
	public String iPAdd;
	
	/**
	 * the payment link to level 1 id
	 */
	public String linkToLevel1;
	
	/**
	 * the payment link to level 2 id
	 */
	public String linkToLevel2;

	/**
	 * FK to er_mileachieved, will be used for milestone related records. <br>
	 * (this will help identify the exact milestone achievement record for multiple achievements for the same patient and milestone combination)
	 */
	public String linkMileAchieved;

	 // END OF GETTER SETTER METHODS
	
	//constants for the HOLDBACK Implementation .
	
	public static String ACTUAL_MILESTONE_AMOUNT="ACTUAL_MILESTON_AMOUNT";

	public static String HOLDBACK_AMOUNT="HOLDBACK_AMOUNT";
	
	public static String HOLDBACK_NOTATION="H";
	
	
	
	// END OF GETTER SETTER METHODS
	
	 
	public String getLinkMileAchieved() {
		return linkMileAchieved;
	}

	public void setLinkMileAchieved(String linkMileAchieved) {
		this.linkMileAchieved = linkMileAchieved;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getFkPayment() {
		return fkPayment;
	}

	public void setFkPayment(String fkPayment) {
		this.fkPayment = fkPayment;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIPAdd() {
		return iPAdd;
	}

	public void setIPAdd(String add) {
		iPAdd = add;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getLinkedTo() {
		return linkedTo;
	}

	public void setLinkedTo(String linkedTo) {
		this.linkedTo = linkedTo;
	}

	public String getLinkToId() {
		return linkToId;
	}

	public void setLinkToId(String linkToId) {
		this.linkToId = linkToId;
	}
	
	public String getLinkToLevel1() {
		return linkToLevel1;
	}

	public void setLinkToLevel1(String linkToLevel1) {
		this.linkToLevel1 = linkToLevel1;
	}

	
	public String getLinkToLevel2() {
		return linkToLevel2;
	}

	public void setLinkToLevel2(String linkToLevel2) {
		this.linkToLevel2 = linkToLevel2;
	}

	/**
     * Defines an PaymentDetailJB object with the specified category Id
     */
    public PaymentDetailJB(int id) {
        setId(id);
    }

    /**
     * Defines an PaymentDetailJB object with default values for fields
     */
    public PaymentDetailJB() {
        Rlog.debug("PaymentDetail", "PaymentDetailJB.PaymentDetailJB() ");
    };
    
    

    /**
     * Defines an PaymentDetailJB object with the specified values for the fields
     * 
     */
     /** Full Argument constructor*/
    
    /** Full Argument constructor */
	public PaymentDetailJB(String amount, String creator, String payment, int id, String iPAdd, String lastModifiedBy,
			String linkedTo, String linkToId, String linkToLevel1, String linkToLevel2,String mileach) {
		this.amount = amount;
		this.creator = creator;
		this.fkPayment = payment;
		this.id = id;
		this.iPAdd =  iPAdd;
		this.lastModifiedBy = lastModifiedBy;
		
		this.linkedTo = linkedTo;
		this.linkToId = linkToId ;
		setLinkToLevel1(linkToLevel1);
		setLinkToLevel2(linkToLevel2);
		setLinkMileAchieved(mileach);
	}
    

    /**
     * Retrieves the details of an category in PaymentDetailBean Object
     * 
     * @return PaymentDetailBean 
     * @see PaymentDetailBean
     */
    public PaymentDetailBean getPaymentDetail() {
        PaymentDetailBean iBean = null;
        try {
            PaymentDetailAgentRObj paymetDetailAgentRObj = EJBUtil.getPaymentDetailAgentHome();
            iBean = paymetDetailAgentRObj.getPaymentDetail(this.id);
            Rlog.debug("PaymentDetail", "In PaymentDetailJB.getPaymentDetail() " + iBean);
        } catch (Exception e) {
            Rlog.fatal("PaymentDetail", "Error in getPaymentDetailDetails() in PaymentDetailJB "
                            + e);
        }
        if (iBean != null) {
        	setAmount(iBean.getAmount()); 
			 setFkPayment(iBean.getFkPayment()) ;
			 setLinkedTo(iBean.getLinkedTo()) ;
			 setLinkToId(iBean.getLinkToId() ) ;
			 setIPAdd(iBean.getIPAdd()) ;
			 setLastModifiedBy(iBean.getLastModifiedBy()) ;
			 setCreator(iBean.getCreator());
			 setLinkToLevel1(iBean.getLinkToLevel1());
 			 setLinkToLevel2(iBean.getLinkToLevel2());
 			setLinkMileAchieved(iBean.getLinkMileAchieved());
        }
        return iBean;
    }

    public void setPaymentDetailDetails() {
        int output = 0;
        try {
            PaymentDetailAgentRObj paymetDetailAgentRObj = EJBUtil.getPaymentDetailAgentHome();
            output = paymetDetailAgentRObj.setPaymentDetail(this.createPaymentDetailBean());
            this.setId(output);
            Rlog.debug("PaymentDetail", "PaymentDetailJB.setPaymentDetails()");
            
          
            
        } catch (Exception e) {
            Rlog
                    .fatal("PaymentDetail", "Error in setPaymentDetailDetails() in PaymentDetailJB "
                            + e);
            e.printStackTrace();
        }
    }
    
    /**
     * sets the milepayment details using values from  PaymentDetailsDao
     */
    public int setPaymentDetail(PaymentDetailsDao  pd)
    {
    	 int output = 0;
         try {
             PaymentDetailAgentRObj paymetDetailAgentRObj = EJBUtil.getPaymentDetailAgentHome();
             output = paymetDetailAgentRObj.setPaymentDetail(pd);
              
             Rlog.debug("PaymentDetail", "PaymentDetailJB.setPaymentDetail(PaymentDetailsDao  pd)");
             
           
             
         } catch (Exception e) {
             Rlog
                     .fatal("PaymentDetail", "Error in setPaymentDetail(PaymentDetailsDao  pd) in PaymentDetailJB "
                             + e);
             e.printStackTrace();
         }
         return output ;
    	
    }

    /**
     * Saves the modified details of the category to the database
     * 
     * @return 0 - If update succeeds <BR>
     *         -2 - If update fails
     */
    public int updatePaymentDetail() {
        int output;
        try {
            PaymentDetailAgentRObj paymetDetailAgentRObj = EJBUtil.getPaymentDetailAgentHome();
            output = paymetDetailAgentRObj.updatePaymentDetail(this.createPaymentDetailBean());
                      
          
        } catch (Exception e) {
            Rlog.fatal("PaymentDetail",    "EXCEPTION IN updatePaymentDetail in PaymentDetailJB BEAN"
                            + e.getMessage());

            return -2;
        }
        return output;
    }

    /**
     * Uses the values of the current PaymentDetailJB object to create an
     * PaymentDetailBean Object
     * 
     * @return PaymentDetailBean object 
      */
    public PaymentDetailBean createPaymentDetailBean() {
        Rlog.debug("PaymentDetail", "PaymentDetailJB.createPaymentDetailBean ");
        
                
        return new PaymentDetailBean(amount, creator, fkPayment, id, iPAdd, lastModifiedBy, linkedTo, linkToId,
        		linkToLevel1,linkToLevel2,linkMileAchieved);
    }
    
    /**
     *   if payment details are added, gets payment detail records with previous totals, else 
     *  gets previous payment totals
     * @param payment the paymentPk for which detail records are retrieved. If payment == 0, then just get the totals of previous payments for the same linkToId 
     * @param linkToId the linkToId, main record type for which payment detail records are retrieved
     * @param linkType the type of payment link. possible values: M-Milestones, I-Invoice, B-Budgets              
     */

    public PaymentDetailsDao getPaymentDetailsWithTotals(int payment, int linkToId ,String linkType)
    {
    	PaymentDetailsDao pd = new PaymentDetailsDao();
    	try {
            PaymentDetailAgentRObj paymetDetailAgentRObj = EJBUtil.getPaymentDetailAgentHome();
            pd = paymetDetailAgentRObj.getPaymentDetailsWithTotals( payment,  linkToId,linkType );
                      
          
        } catch (Exception e) {
            Rlog.fatal("PaymentDetail",    "EXCEPTION IN getPaymentDetailsWithTotals(int payment, int linkToId ) in PaymentDetailJB BEAN"
                            + e.getMessage());

            }
        return pd;
    }

     
    /**
     *   get linked payment browser info
     * @param payment 
     * @param linkType        
     */

    public PaymentDetailsDao getLinkedPaymentBrowser(int payment, String linkType)
    {
       	PaymentDetailsDao pd = new PaymentDetailsDao();
    	try {
            PaymentDetailAgentRObj paymetDetailAgentRObj = EJBUtil.getPaymentDetailAgentHome();
            pd = paymetDetailAgentRObj.getLinkedPaymentBrowser( payment,  linkType );
                      
          
        } catch (Exception e) {
            Rlog.fatal("PaymentDetail",    "EXCEPTION IN getPaymentDetailsWithTotals(int payment, int linkToId ) in PaymentDetailJB BEAN"
                            + e.getMessage());

            }
        return pd;
    	
    	
    }
  
    /**
     * returns the holdback percentage. 
     * @param holdBack
     * @return
     */
    public String getHoldBackPercentage(Float holdBack){
    	return holdBack>0?holdBack+"%":"0.00%";
    }
    
    
    /**
     * gets Milestone Amount according to their type.    
     * @param holdBackPercent
     * @param milestoneAmountType
     * @return
     */
    public double getMilestoneAmountByType(Float holdBackPercent,double milestoneAmount,String milestoneAmountType){
    	double milAmt=0;
    	if(holdBackPercent<=0)
    		return milestoneAmount;

    	try{

    		if(milestoneAmountType.equals(ACTUAL_MILESTONE_AMOUNT)){
    			milAmt=(milestoneAmount*(1-holdBackPercent/100));

    		}else if(milestoneAmountType.equals(HOLDBACK_AMOUNT)){
    			milAmt=(milestoneAmount*(holdBackPercent/100));
    		}
    	}catch (Exception ex) {
    		Rlog.error("PaymentDetailJB","method Name :getMilestoneAmountByType "+ex.getMessage());
    	}
    	return milAmt;
    }
    
    public boolean isHoldBack(float holdBack){
    	return holdBack>0;
    }
}
