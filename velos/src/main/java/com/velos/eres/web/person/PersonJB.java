/*
 * Classname : PersonJB
 * 
 * Version information: 1.0
 *
 * Date: 06/03/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Sahni
 */

package com.velos.eres.web.person;

/* IMPORT STATEMENTS */
import java.io.Serializable;
import java.util.Hashtable;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.audit.web.AuditRowEresJB;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.PatientDao;
import com.velos.eres.business.person.impl.PersonBean;
import com.velos.eres.service.personAgent.PersonAgentRObj;
import com.velos.eres.service.personAgent.impl.PersonAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.web.pref.PrefJB;
import com.velos.eres.web.study.StudyJB;
//import com.velos.eres.web.patFacility.PatFacilityJB;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for Person (Pat Ref)
 * 
 * @author Sonia Sahni
 * @version 1.0 06/02/2001
 */

public class PersonJB implements Serializable {

    /**
     * 
     * 
     * the Person PK Id
     * 
     * 
     */

    private int personPKId;

    /**
     * 
     * 
     * the Person ID
     * 
     * 
     */

    private String personPId;

    /**
     * 
     * 
     * the Person Account
     * 
     * 
     */

    private String personAccount;

    /**
     * 
     * 
     * the Person Account
     * 
     * 
     */

    private String personLocation;

    private String personAltId;

    private String personLname;

    private String personFname;

    private String personMname;

    private String personSuffix;

    private String personPrefix;

    private String personDegree;

    private String personMotherName;

    private String personDob;

    private String personGender;

    private String personAlias;

    private String personRace;

    private String personAddRace;

    private String personEthnicity;

    private String personAddEthnicity;

    private String personAddressId;

    private String personPrimaryLang;

    private String personMarital;

    private String personRelegion;

    private String personSSN;

    private String personDrivLic;

    private String personMotherId;

    private String personEthGroup;

    private String personBirthPlace;

    private String personMultiBirth;

    private String personBirthOrder;

    private String personCitizen;

    private String personVetMilStatus;

    private String personNationality;

    private String personDeathDate;

    private String personDeathIndicator;

    private String personEmployment;

    private String personEducation;

    private String personNotes;

    private String personStatus;

    private String personBloodGr;

    private String personAddress1;

    private String personAddress2;

    private String personCity;

    private String personState;

    private String personZip;

    private String personCountry;

    private String personHphone;

    private String personBphone;

    private String personEmail;

    private String personCounty;

    private String personRegDate;

    private String personRegBy;

    private String timeZoneId;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    private String phyOther;

    private String orgOther;

    private String personSplAccess;

    private String patDthCause;

    private String dthCauseOther;
    
        /**
     * the Patient Facility Id
     */
    private String patientFacilityId;


    // GETTER SETTER METHODS

    public String getPersonAccount() {
        return this.personAccount;
    }

    public void setPersonAccount(String personAccount) {
        this.personAccount = personAccount;
    }

    public String getPersonLocation() {
        return this.personLocation;
    }

    public void setPersonLocation(String personLocation) {
        this.personLocation = personLocation;
    }

    public String getPersonPId() {
        return this.personPId;
    }

    public void setPersonPId(String personPId) {
        this.personPId = personPId;
    }

    public int getPersonPKId() {
        return this.personPKId;
    }

    public void setPersonPKId(int personPKId) {
        this.personPKId = personPKId;
    }

    public String getPersonAddressId() {
        return this.personAddressId;
    }

    public void setPersonAddressId(String personAddressId) {
        this.personAddressId = personAddressId;
    }

    public String getPersonAlias() {
        return this.personAlias;
    }

    public void setPersonAlias(String personAlias) {
        this.personAlias = personAlias;
    }

    public String getPersonAltId() {
        return this.personAltId;
    }

    public void setPersonAltId(String personAltId) {
        this.personAltId = personAltId;
    }

    public String getPersonBirthOrder() {
        return this.personBirthOrder;
    }

    public void setPersonBirthOrder(String personBirthOrder) {
        this.personBirthOrder = personBirthOrder;
    }

    public String getPersonBirthPlace() {
        return this.personBirthPlace;
    }

    public void setPersonBirthPlace(String personBirthPlace) {
        this.personBirthPlace = personBirthPlace;
    }

    public String getPersonBloodGr() {
        return this.personBloodGr;
    }

    public void setPersonBloodGr(String personBloodGr) {
        this.personBloodGr = personBloodGr;
    }

    public String getPersonCitizen() {
        return this.personCitizen;
    }

    public void setPersonCitizen(String personCitizen) {
        this.personCitizen = personCitizen;
    }

    public String getPersonDeathDate() {
        return this.personDeathDate;
    }

    public void setPersonDeathDate(String personDeathDate) {
        this.personDeathDate = personDeathDate;
    }

    public String getPersonDeathIndicator() {
        return this.personDeathIndicator;
    }

    public void setPersonDeathIndicator(String personDeathIndicator) {
        this.personDeathIndicator = personDeathIndicator;
    }

    public String getPersonDegree() {
        return this.personDegree;
    }

    public void setPersonDegree(String personDegree) {
        this.personDegree = personDegree;
    }

    public String getPersonDob() {
        return this.personDob;
    }

    public void setPersonDob(String personDob) {
        this.personDob = personDob;
    }

    public String getPersonDrivLic() {
        return this.personDrivLic;
    }

    public void setPersonDrivLic(String personDrivLic) {
        this.personDrivLic = personDrivLic;
    }

    public String getPersonEducation() {
        return this.personEducation;
    }

    public void setPersonEducation(String personEducation) {
        this.personEducation = personEducation;
    }

    public String getPersonEmployment() {
        return this.personEmployment;
    }

    public void setPersonEmployment(String personEmployment) {
        this.personEmployment = personEmployment;
    }

    public String getPersonEthGroup() {
        return this.personEthGroup;
    }

    public void setPersonEthGroup(String personEthGroup) {
        this.personEthGroup = personEthGroup;
    }

    public String getPersonFname() {
        return this.personFname;
    }

    public void setPersonFname(String personFname) {
        this.personFname = personFname;
    }

    public String getPersonGender() {
        return this.personGender;
    }

    public void setPersonGender(String personGender) {
        this.personGender = personGender;
    }

    public String getPersonLname() {
        return this.personLname;
    }

    public void setPersonLname(String personLname) {
        this.personLname = personLname;
    }

    public String getPersonMarital() {
        return this.personMarital;
    }

    public void setPersonMarital(String personMarital) {
        this.personMarital = personMarital;
    }

    public String getPersonMname() {
        return this.personMname;
    }

    public void setPersonMname(String personMname) {
        this.personMname = personMname;
    }

    public String getPersonMotherId() {
        return this.personMotherId;
    }

    public void setPersonMotherId(String personMotherId) {
        this.personMotherId = personMotherId;
    }

    public String getPersonMotherName() {
        return this.personMotherName;
    }

    public void setPersonMotherName(String personMotherName) {
        this.personMotherName = personMotherName;
    }

    public String getPersonMultiBirth() {
        return this.personMultiBirth;
    }

    public void setPersonMultiBirth(String personMultiBirth) {
        this.personMultiBirth = personMultiBirth;
    }

    public String getPersonNationality() {
        return this.personNationality;
    }

    public void setPersonNationality(String personNationality) {
        this.personNationality = personNationality;
    }

    public String getPersonNotes() {
        return this.personNotes;
    }

    public void setPersonNotes(String personNotes) {
        this.personNotes = personNotes;
    }

    public String getPersonPrefix() {
        return this.personPrefix;
    }

    public void setPersonPrefix(String personPrefix) {
        this.personPrefix = personPrefix;
    }

    public String getPersonPrimaryLang() {
        return this.personPrimaryLang;
    }

    public void setPersonPrimaryLang(String personPrimaryLang) {
        this.personPrimaryLang = personPrimaryLang;
    }

    public String getPersonRace() {
        return this.personRace;
    }

    public void setPersonRace(String personRace) {
        this.personRace = personRace;
    }

    public String getPersonAddEthnicity() {
        return this.personAddEthnicity;
    }

    public void setPersonAddEthnicity(String personAddEthnicity) {
        this.personAddEthnicity = personAddEthnicity;
    }

    public String getPersonAddRace() {
        return this.personAddRace;
    }

    public void setPersonAddRace(String personAddRace) {
        this.personAddRace = personAddRace;
    }

    public String getPersonEthnicity() {
        return this.personEthnicity;
    }

    public void setPersonEthnicity(String personEthnicity) {
        this.personEthnicity = personEthnicity;
    }

    public String getPersonRelegion() {
        return this.personRelegion;
    }

    public void setPersonRelegion(String personRelegion) {
        this.personRelegion = personRelegion;
    }

    public String getPersonSSN() {
        return this.personSSN;
    }

    public void setPersonSSN(String personSSN) {
        this.personSSN = personSSN;
    }

    public String getPersonStatus() {
        return this.personStatus;
    }

    public void setPersonStatus(String personStatus) {
        this.personStatus = personStatus;
    }

    public String getPersonSuffix() {
        return this.personSuffix;
    }

    public void setPersonSuffix(String personSuffix) {
        this.personSuffix = personSuffix;
    }

    public String getPersonVetMilStatus() {
        return this.personVetMilStatus;
    }

    public void setPersonVetMilStatus(String personVetMilStatus) {
        this.personVetMilStatus = personVetMilStatus;
    }

    public String getPersonAddress1() {
        return this.personAddress1;
    }

    public void setPersonAddress1(String personAddress1) {
        this.personAddress1 = personAddress1;
    }

    public String getPersonAddress2() {
        return this.personAddress2;
    }

    public void setPersonAddress2(String personAddress2) {
        this.personAddress2 = personAddress2;
    }

    public String getPersonCity() {
        return this.personCity;
    }

    public void setPersonCity(String personCity) {
        this.personCity = personCity;
    }

    public String getPersonCountry() {
        return this.personCountry;
    }

    public void setPersonCountry(String personCountry) {
        this.personCountry = personCountry;
    }

    public String getPersonCounty() {
        return this.personCounty;
    }

    public void setPersonCounty(String personCounty) {
        this.personCounty = personCounty;
    }

    public String getPersonEmail() {
        return this.personEmail;
    }

    public void setPersonEmail(String personEmail) {
        this.personEmail = personEmail;
    }

    public String getPersonHphone() {
        return this.personHphone;
    }

    public void setPersonHphone(String personHphone) {
        this.personHphone = personHphone;
    }

    public String getPersonState() {
        return this.personState;
    }

    public void setPersonState(String personState) {
        this.personState = personState;
    }

    public String getPersonZip() {
        return this.personZip;
    }

    public void setPersonZip(String personZip) {
        this.personZip = personZip;
    }

    public String getPersonBphone() {
        return this.personBphone;
    }

    public void setPersonBphone(String personBphone) {
        this.personBphone = personBphone;
    }

    public String getPersonRegBy() {
        return this.personRegBy;
    }

    public void setPersonRegBy(String personRegBy) {
        this.personRegBy = personRegBy;
    }

    public String getTimeZoneId() {
        return this.timeZoneId;
    }

    public void setTimeZoneId(String timeZoneId) {
        this.timeZoneId = timeZoneId;
    }

    public String getPersonRegDate() {
        return this.personRegDate;
    }

    public void setPersonRegDate(String personRegDate) {
        this.personRegDate = personRegDate;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public void setPhyOther(String phyOther) {
        this.phyOther = phyOther;
    }

    public String getPhyOther() {
        return this.phyOther;
    }

    public void setOrgOther(String orgOther) {
        this.orgOther = orgOther;
    }

    public String getOrgOther() {
        return this.orgOther;
    }

    public void setPersonSplAccess(String personSplAccess) {
        this.personSplAccess = personSplAccess;
    }

    public String getPersonSplAccess() {
        return this.personSplAccess;
    }

    public void setPatDthCause(String patDthCause) {
        this.patDthCause = patDthCause;
    }

    public String getPatDthCause() {
        return this.patDthCause;
    }

    public void setDthCauseOther(String dthCauseOther) {
        this.dthCauseOther = dthCauseOther;
    }

    public String getDthCauseOther() {
        return this.dthCauseOther;
    }

    /**
	 * Returns the value of patientFacilityId.
	 */
	public String getPatientFacilityId()
	{
		return patientFacilityId;
	}

	/**
	 * Sets the value of patientFacilityId.
	 * @param patientFacilityId The value to assign patientFacilityId.
	 */
	public void setPatientFacilityId(String patientFacilityId)
	{
		this.patientFacilityId = patientFacilityId;
	}

    // end of getter setter methods here

    /**
     * Constructor
     * 
     * @param personId
     *            PK
     */
    public PersonJB(int personPKId) {
        setPersonPKId(personPKId);
    }

    /**
     * Default Constructor
     * 
     */
    public PersonJB() {
        Rlog.debug("person", "PersonJB.PersonJB() ");
    };

    /**
     * Minimum arguments constructor.
     * 
     * @param personPkId
     * @param personPId
     * @param personAccount
     * @param personLocation
     */
    public PersonJB(int personPkId, String personPId, String personAccount,
            String personLocation) {

        setPersonPKId(personPKId);
        setPersonPId(personPId);
        setPersonAccount(personAccount);
        setPersonLocation(personLocation);

    }

    /**
     * Full arguments constructor.
     * 
     */

    public PersonJB(int personPkId, String personPId, String personAccount,
            String personLocation, String personAltId, String personLname,
            String personFname, String personMname, String personSuffix,
            String personPrefix, String personDegree, String personMotherName,
            String personDob, String personGender, String personAlias,
            String personRace, String personAddRace, String personEthnicity,
            String personAddEthnicity, String personPrimaryLang,
            String personMarital, String personRelegion, String personSSN,
            String personDrivLic, String personMotherId, String personEthGroup,
            String personBirthPlace, String personMultiBirth,
            String personBirthOrder, String personCitizen,
            String personVetMilStatus, String personNationality,
            String personDeathDate, String personDeathIndicator,
            String personEmployment, String personEducation,
            String personNotes, String personStatus, String personBloodGr,
            String personAddress1, String personAddress2, String personCity,
            String personCountry, String personCounty, String personEmail,
            String personHphone, String personBphone, String personState,
            String personZip, String personRegDate, String personRegBy,
            String timeZoneId, String creator, String modifiedBy, String ipAdd,
            String phyOther, String orgOther, String personSplAccess,
            String patDthCause, String dthCauseOther, String patientFacilityId) {

        setPersonPKId(personPkId);
        setPersonPId(personPId);
        setPersonAccount(personAccount);
        setPersonLocation(personLocation);
        setPersonAlias(personAlias);
        setPersonAltId(personAltId);
        setPersonBirthOrder(personBirthOrder);
        setPersonBirthPlace(personBirthPlace);
        setPersonBloodGr(personBloodGr);
        setPersonCitizen(personCitizen);
        setPersonDeathDate(personDeathDate);
        setPersonDeathIndicator(personDeathIndicator);
        setPersonDegree(personDegree);
        setPersonDob(personDob);
        setPersonDrivLic(personDrivLic);
        setPersonEducation(personEducation);
        setPersonEmployment(personEmployment);
        setPersonEthGroup(personEthGroup);
        setPersonFname(personFname);
        setPersonGender(personGender);
        setPersonLname(personLname);
        setPersonMarital(personMarital);
        setPersonMname(personMname);
        setPersonMotherId(personMotherId);
        setPersonMotherName(personMotherName);
        setPersonMultiBirth(personMultiBirth);
        setPersonNationality(personNationality);
        setPersonNotes(personNotes);
        setPersonPrefix(personPrefix);
        setPersonPrimaryLang(personPrimaryLang);
        setPersonRace(personRace);
        setPersonAddRace(personAddRace);
        setPersonEthnicity(personEthnicity);
        setPersonAddEthnicity(personAddEthnicity);
        setPersonRelegion(personRelegion);
        setPersonSSN(personSSN);
        setPersonStatus(personStatus);
        setPersonSuffix(personSuffix);
        setPersonVetMilStatus(personVetMilStatus);

        setPersonAddress1(personAddress1);
        setPersonAddress2(personAddress2);
        setPersonCity(personCity);
        setPersonCountry(personCountry);
        setPersonCounty(personCounty);
        setPersonEmail(personEmail);
        setPersonHphone(personHphone);
        setPersonBphone(personBphone);
        setPersonState(personState);
        setPersonZip(personZip);
        setPersonRegDate(personRegDate);
        setPersonRegBy(personRegBy);
        setTimeZoneId(timeZoneId);

        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setPhyOther(phyOther);
        setOrgOther(orgOther);

        setPersonSplAccess(personSplAccess);

        setPatDthCause(patDthCause);
        setDthCauseOther(dthCauseOther);
	setPatientFacilityId(patientFacilityId);
    }

    /**
     * Calls getPersonDetails on Person Session Bean: PersonAgentBean
     * 
     * @return PersonStateKeeper
     * @see PersonAgentBean
     */
    public PersonBean getPersonDetails() {
        PersonBean psk = null;
        try {
            PersonAgentRObj personAgentRObj = EJBUtil.getPersonAgentHome();
            psk = personAgentRObj.getPersonDetails(this.personPKId);
            Rlog.debug("person",
                    "PersonJB.getPersonDetails() PersonStateKeeper " + psk);
        } catch (Exception e) {
            Rlog
                    .fatal("person", "Error in getPersonDetails() in PersonJB "
                            + e);
        }
        if (psk != null) {
            personPKId = psk.getPersonPKId();
            personPId = psk.getPersonPId();
            personAccount = psk.getPersonAccount();
            personLocation = psk.getPersonLocation();

            setPersonAlias(psk.getPersonAlias());
            setPersonAltId(psk.getPersonAltId());
            setPersonBirthOrder(psk.getPersonBirthOrder());
            setPersonBirthPlace(psk.getPersonBirthPlace());
            setPersonBloodGr(psk.getPersonBloodGr());
            setPersonCitizen(psk.getPersonCitizen());
            setPersonDeathDate(psk.getPersonDeathDate());
            setPersonDeathIndicator(psk.getPersonDeathIndicator());
            setPersonDegree(psk.getPersonDegree());
            setPersonDob(psk.getPersonDob());
            setPersonDrivLic(psk.getPersonDrivLic());
            setPersonEducation(psk.getPersonEducation());
            setPersonEmployment(psk.getPersonEmployment());
            setPersonEthGroup(psk.getPersonEthGroup());
            setPersonFname(psk.getPersonFname());
            setPersonGender(psk.getPersonGender());
            setPersonLname(psk.getPersonLname());
            setPersonMarital(psk.getPersonMarital());
            setPersonMname(psk.getPersonMname());
            setPersonMotherId(psk.getPersonMotherId());
            setPersonMotherName(psk.getPersonMotherName());
            setPersonMultiBirth(psk.getPersonMultiBirth());
            setPersonNationality(psk.getPersonNationality());
            setPersonNotes(psk.getPersonNotes());
            setPersonPrefix(psk.getPersonPrefix());
            setPersonPrimaryLang(psk.getPersonPrimaryLang());
            setPersonRace(psk.getPersonRace());
            setPersonAddRace(psk.getPersonAddRace());
            setPersonEthnicity(psk.getPersonEthnicity());
            setPersonAddEthnicity(psk.getPersonAddEthnicity());

            setPersonRelegion(psk.getPersonRelegion());
            setPersonSSN(psk.getPersonSSN());
            setPersonStatus(psk.getPersonStatus());
            setPersonSuffix(psk.getPersonSuffix());
            setPersonVetMilStatus(psk.getPersonVetMilStatus());

            setPersonAddress1(psk.getPersonAddress1());
            setPersonAddress2(psk.getPersonAddress2());
            setPersonCity(psk.getPersonCity());
            setPersonCountry(psk.getPersonCountry());
            setPersonCounty(psk.getPersonCounty());
            setPersonEmail(psk.getPersonEmail());
            setPersonHphone(psk.getPersonHphone());
            setPersonBphone(psk.getPersonBphone());
            setPersonState(psk.getPersonState());
            setPersonZip(psk.getPersonZip());
            setPersonRegDate(psk.getPersonRegDate());
            setPersonRegBy(psk.getPersonRegBy());

            setTimeZoneId(psk.getTimeZoneId());

            setCreator(psk.getCreator());
            setModifiedBy(psk.getModifiedBy());
            setIpAdd(psk.getIpAdd());
            setPhyOther(psk.getPhyOther());
            setOrgOther(psk.getOrgOther());
            setPersonSplAccess(psk.getPersonSplAccess());

            setPatDthCause(psk.getPatDthCause());
            setDthCauseOther(psk.getDthCauseOther());
	    setPatientFacilityId(psk.getPatientFacilityId());
        }
        return psk;
    }

    /**
     * Calls setPersonDetails() on Person Session Bean: PersonAgentBean
     * 
     */
    public PersonBean setPersonDetails() {
        PersonBean psk = null;
        String notes="";
        // PrefJB pref = new PrefJB();

        try {

            // save pref record first
            // pref.setPrefPId(this.getPersonPId());
            // pref.setPrefAccount(this.getPersonAccount());
            // pref.setPrefLocation(this.getPersonLocation());
            // pref.setCreator(this.getCreator());
            // pref.setIpAdd(this.getIpAdd());

            // pskPref = pref.setPrefDetails();
            // this.setPersonPKId(pskPref.getPrefPKId());

            PersonAgentRObj personAgentRObj = EJBUtil.getPersonAgentHome();

            psk = personAgentRObj.setPersonDetails(this
                    .createPersonStateKeeper());
            // psk.setPersonPKId(pskPref.getPrefPKId());

            Rlog.debug("person", "PersonJB.setPersonDetails() PK"
                    + psk.getPersonPKId());
            

            Rlog.debug("person", "person details saved above except notes(in clob data type)" );
            
            //JM: 04Oct2006
            CommonDAO cDao = new CommonDAO();            
            Rlog.debug("person", "person this.getPersonNotes():" + this.getPersonNotes());
            notes=psk.getPersonNotes()==null?"":psk.getPersonNotes();
              
            if(!(notes.equals(""))){
            	cDao.updateClob(psk.getPersonNotes(),"person","person_notes_clob"," where pk_person  = " + psk.getPersonPKId());
            }
           
        } catch (Exception e) {
            Rlog
                    .fatal("person", "Error in setPersonDetails() in PersonJB "
                            + e);
            e.printStackTrace();
        }

        return psk;
    }

    /**
     * Calls updatePerson() on Person Session Bean: PersonAgentBean
     * 
     * @return
     */
    public int updatePerson() {
        int output;
        String notes="";
        int rid=0;
        PrefJB pref = new PrefJB();
        try {
            pref.setPrefPKId(this.getPersonPKId());
            pref.getPrefDetails();
            pref.setPrefPId(this.getPersonPId());
            pref.setPrefAccount(this.getPersonAccount());
            pref.setPrefLocation(this.getPersonLocation());
            pref.setModifiedBy(this.getModifiedBy());
            pref.setIpAdd(this.getIpAdd());
	    pref.setPatientFacilityId(this.getPatientFacilityId());
            pref.updatePref();

            PersonAgentRObj personAgentRObj = EJBUtil.getPersonAgentHome();
            output = personAgentRObj.updatePerson(this
                    .createPersonStateKeeper());
            
            
            //JM: 04Oct2006
            CommonDAO cDao = new CommonDAO();
            
            notes=this.getPersonNotes();
            
            if(!(notes.equals(""))){
            	cDao.updateClob(this.getPersonNotes(),"person","person_notes_clob"," where pk_person = " + this.getPersonPKId());
            	
            	AuditRowEresJB auditRowEresJB = new AuditRowEresJB();
            	AuditUtils audittrails = new AuditUtils();
            	
            	rid = auditRowEresJB.getRidForDeleteOperation(this.getPersonPKId(), EJBUtil.stringToNum(this.getModifiedBy()), "PERSON", "PK_PERSON");
				audittrails.deleteRaidList("epat", rid, this.getModifiedBy(), "U");          	
            }
    
        } catch (Exception e) {
            Rlog
                    .fatal("person",
                            "EXCEPTION IN updating Person DETAILS TO  SESSION BEAN"
                                    + e);
            return -2;
        }
        return output;
    }

    public int removePerson() {

        int output;

        try {

            PersonAgentRObj personAgentRObj = EJBUtil.getPersonAgentHome();
            output = personAgentRObj.removePerson(this.personPKId);
            return output;

        } catch (Exception e) {
            Rlog.debug("person", "in JB - remove PREF");
            return -1;
        }

    }

    /**
     * 
     * 
     * @return a statekeeper object for the User Group Record with the current
     *         values of the bean
     */
    public PersonBean createPersonStateKeeper() {
        Rlog.debug("person", "PersonJB.createPersonStateKeeper ");

        return new PersonBean(personPKId, personPId, personAccount,
                personLocation, personAltId, personLname, personFname,
                personMname, personSuffix, personPrefix, personDegree,
                personMotherName, personDob, personGender, personAlias,
                personRace, personEthnicity, personPrimaryLang, personMarital,
                personRelegion, personSSN, personDrivLic, personMotherId,
                personEthGroup, personBirthPlace, personMultiBirth,
                personBirthOrder, personCitizen, personVetMilStatus,
                personNationality, personDeathDate, personDeathIndicator,
                personEmployment, personEducation, personNotes, personStatus,
                personBloodGr, personAddress1, personAddress2, personCity,
                personState, personZip, personCountry, personHphone,
                personBphone, personEmail, personCounty, personRegDate,
                personRegBy, timeZoneId, creator, modifiedBy, ipAdd, phyOther,
                orgOther, personSplAccess, personAddRace, personAddEthnicity,
                patDthCause, dthCauseOther,patientFacilityId);

    }

    public PatientDao getPatients(int usrOrganization, String patientID,
            int patStatus) {
        PatientDao patientDao = new PatientDao();
        try {
            PersonAgentRObj personAgent = EJBUtil.getPersonAgentHome();
            patientDao = personAgent.getPatients(usrOrganization, patientID,
                    patStatus);
        } catch (Exception e) {
            Rlog.fatal("person", "Error in getPatients in PersonJB " + e);
        }
        return patientDao;
    }

    public PatientDao searchPatients(String siteId, String studyId,
            String patientCode) {
        PatientDao patientDao = new PatientDao();
        try {
            PersonAgentRObj personAgentRObj = EJBUtil.getPersonAgentHome();
            patientDao = personAgentRObj.searchPatients(siteId, studyId,
                    patientCode);
        } catch (Exception e) {
            Rlog.fatal("person", "Error in searchPatients in PersonJB " + e);
        }
        return patientDao;
    }

    public PatientDao searchPatientsForEnr(String siteId, String studyId,
            String patientCode, String age, String genderVal, String regBy,
            String pstat, String pName, String pStudyExists) {
        PatientDao patientDao = new PatientDao();
        try {

            PersonAgentRObj personAgentRObj = EJBUtil.getPersonAgentHome();

            patientDao = personAgentRObj.searchPatientsForEnr(siteId, studyId,
                    patientCode, age, genderVal, regBy, pstat, pName,
                    pStudyExists);

        } catch (Exception e) {
            Rlog.fatal("person", "Error in searchPatientsForEnr in PersonJB "
                    + e);
        }
        return patientDao;
    }

    public int getPatientCompleteDetailsAccessRight(int userId, int groupId,
            int patientId) {
        int returnRight = 0;
        try {

            PersonAgentRObj personAgentRObj = EJBUtil.getPersonAgentHome();

            returnRight = personAgentRObj.getPatientCompleteDetailsAccessRight(
                    userId, groupId, patientId);

        } catch (Exception e) {

            Rlog.fatal("person",
                    "Error in getPatientCompleteDetailsAccessRight in PersonJB "
                            + e);
        }
        return returnRight;
    }

    /**
     * NOT IN USE
     * 
     * @return
     */
    public String toXML() {
        Rlog.debug("person", "PersonJB.toXML()");
        String str = "";
        return str;

    }// end of method

    // to delete a patient
    // km
 // Overloaded for INF-18183 ::: Akshi
    public void deletePatient(int patientId, Hashtable<String, String> args) {
        try {

            PersonAgentRObj personAgent = EJBUtil.getPersonAgentHome();
            ;
            Rlog.debug("person", "PersonJB.deletePatient after remote");
            personAgent.deletePatient(patientId,args);
            Rlog.debug("person", "PersonJB.deletePatient after Dao");
        } catch (Exception e) {
            Rlog.fatal("person", "Error in deletePatient in PatientJB " + e);
            e.printStackTrace();
        }
    }

    public void deletePatient(int patientId) {
        try {

            PersonAgentRObj personAgent = EJBUtil.getPersonAgentHome();
            ;
            Rlog.debug("person", "PersonJB.deletePatient after remote");
            personAgent.deletePatient(patientId);
            Rlog.debug("person", "PersonJB.deletePatient after Dao");
        } catch (Exception e) {
            Rlog.fatal("person", "Error in deletePatient in PatientJB " + e);
            e.printStackTrace();
        }
    }
    // km

    
    // to delete a patient and track the user
    // Sonia Abrol, 09/21/06
    public void deletePatient(int patientId, String deletedBy) {
        try {
        	
        	//update person bean with the lastmodified by 
        	
        	PersonJB pb = new PersonJB();
        	
        	pb.setPersonPKId(patientId);
        	pb.getPersonDetails();
        	pb.setModifiedBy(deletedBy);
        	pb.updatePerson();

            PersonAgentRObj personAgent = EJBUtil.getPersonAgentHome();
            ;
            Rlog.debug("person", "PersonJB.deletePatient after remote");
            personAgent.deletePatient(patientId);
            Rlog.debug("person", "PersonJB.deletePatient after Dao");
        } catch (Exception e) {
            Rlog.fatal("person", "Error in deletePatient in PatientJB " + e);
            e.printStackTrace();
        }
    }
 // Overloaded for INF-18183 ::: AGodara
    public void deletePatient(int patientId, String deletedBy,Hashtable<String, String> auditInfo) {
        try {
        	
        	//update person bean with the lastmodified by 
        	
        	PersonJB pb = new PersonJB();
        	
        	pb.setPersonPKId(patientId);
        	pb.getPersonDetails();
        	pb.setModifiedBy(deletedBy);
        	pb.updatePerson();

            PersonAgentRObj personAgent = EJBUtil.getPersonAgentHome();
            ;
            Rlog.debug("person", "PersonJB.deletePatient after remote");
            personAgent.deletePatient(patientId,auditInfo);
            Rlog.debug("person", "PersonJB.deletePatient after Dao");
        } catch (Exception e) {
            Rlog.fatal("person", "Error in deletePatient in PatientJB " + e);
            e.printStackTrace();
        }
    }
}// end of class

