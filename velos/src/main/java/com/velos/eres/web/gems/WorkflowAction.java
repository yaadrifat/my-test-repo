package com.velos.eres.web.gems;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.velos.eres.gems.business.WorkflowInstanceJB;
import com.velos.eres.gems.business.Workflowkeys;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.StringUtil;

public class WorkflowAction extends ActionSupport {

	
	private static final String CALL_ACTION_PATSTD_STAT = "PATSTD_STAT";
	private static final String CALL_ACTION_PAT_SCH = "PAT_SCH";
	private static final String CALL_ACTION_PATSTD_FORM = "PATSTD_FORM";
	
	
	private static final long serialVersionUID = 5442290981626630039L;

	private static final String NEXT_TASK_IN_QUEUE = "nextTaskInQueue";
	private static final String PREVIOUS_TASK_IN_QUEUE = "firstPendingTaskInQueue";
	private static final String GOTO_WORKFLOW_TASK = "goToWFTask";

	private HttpServletRequest request = null;
	private HttpServletResponse response = null;
	private HttpSession tSession = null;

	private String urlString;

	public String getUrlString() {
		return urlString;
	}

	private String workflowType;
	private String workflowPage;
	private Integer wfEntityId;
	
	private String moreParams;
	private String workflowModule;
	private String callAction;
	
	public WorkflowAction() {
		ActionContext ac = ActionContext.getContext();
		request = (HttpServletRequest) ac
				.get(ServletActionContext.HTTP_REQUEST);
		response = (HttpServletResponse) ac
				.get(ServletActionContext.HTTP_RESPONSE);
		tSession = request.getSession(true);

		urlString = "";

		workflowType = request.getParameter("workflowType");
		workflowModule = request.getParameter("worFlowModule");
		workflowPage = request.getParameter("worFlowPage");
		wfEntityId = StringUtil.stringToInteger(request
				.getParameter("wfEntityId"));
		moreParams = request.getParameter("moreParams");
	}

	public String goToNextTaskInQueue() {

		WorkflowInstanceJB wfJB = new WorkflowInstanceJB();
		Integer taskId = wfJB.fetchNextTaskInQueue(wfEntityId, workflowType);

		generateURLString(taskId);
		return NEXT_TASK_IN_QUEUE;
	}

	public String goToFirstPendingTaskInQueue() {

		WorkflowInstanceJB wfJB = new WorkflowInstanceJB();
		Integer taskId = wfJB.fetchFirstPendingTaskInQueue(wfEntityId,
				workflowType);

		generateURLString(taskId);
		return PREVIOUS_TASK_IN_QUEUE;
	}

	public String goToWFTask() {
		Integer taskId = StringUtil.stringToInteger(request
				.getParameter("taskId"));
		generateURLString(taskId);
		return GOTO_WORKFLOW_TASK;
	}

	private void generateURLString(Integer taskId) {

		Map<String, String> taskDetails = new HashMap<String, String>();
		WorkflowInstanceJB wfJB = new WorkflowInstanceJB();
		taskDetails = wfJB.fetchTaskDetails(taskId);
		
		callAction = taskDetails.get("WA_CALLACTION");
		
		taskDetails.get("WA_KEYCOLUMN");
		taskDetails.get("WA_NAME");
		taskDetails.get("WA_SEQUENCE");
		taskDetails.get("TASK_NAME");
		taskDetails.get("ACTIVITY_NAME");
		taskDetails.get("ACTIVITY_DESC");
		taskDetails.get("WA_SUCESSFORWARDACTION");

		String src = request.getParameter("srcmenu");
		String selectedTab = request.getParameter("selectedTab");
		String mode = request.getParameter("mode");

		// JSP name taken form WA_CALLACTION column
		urlString = urlString + Workflowkeys.wfJSPNames.get(callAction) + "?srcmenu=" + src
				+ "&selectedTab=" + selectedTab + "&mode=" + mode;

		Map<String, String> wfParams = new HashMap<String, String>();

		populateWFTypeSpecificParams(wfParams);
		populatePageSpecificParams(wfParams);

		for (String key : wfParams.keySet()) {
			urlString = urlString + "&" + key + "=" + wfParams.get(key);
		}

	}
	
	// To load paramters which would be required throughout a specific Workflow
	private void populateWFTypeSpecificParams(Map<String, String> paramMap) {

		if (workflowType.equals(CFG.Workflow_PatEnroll_WfType)) {
			String pkey = request.getParameter("pkey");
			String studyId = request.getParameter("studyId");
			paramMap.put("pkey",pkey);
			paramMap.put("studyId",studyId);
		}
		populatePageSpecificParams(paramMap);
	}

	// To load paramters which would be required on the page where we are forwarding the user
	private void populatePageSpecificParams(Map<String, String> paramMap) {	
		
		if (Workflowkeys.WF_Pat_Enrolled.equals(callAction)) {

		} else if (Workflowkeys.WF_Pat_Created.equals(callAction)) {

		}else if (Workflowkeys.WF_Pat_Demographics.equals(callAction)){
			
		}else if (Workflowkeys.WF_Pat_More_Details.equals(callAction)){
			
		}

	}
}
