/*
 * Classname : StdSiteAddInfoJB
 * 
 * Version information: 1.0
 *
 * Date: 11/16/2004
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.web.stdSiteAddInfo;

/* IMPORT STATEMENTS */

import com.velos.eres.business.common.StdSiteAddInfoDao;
import com.velos.eres.business.stdSiteAddInfo.impl.StdSiteAddInfoBean;
import com.velos.eres.service.stdSiteAddInfoAgent.StdSiteAddInfoAgentRObj;
import com.velos.eres.service.stdSiteAddInfoAgent.impl.StdSiteAddInfoAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for StudyId
 * 
 * @author Anu Khanna
 * @version 1.0 11/16/2004
 */

public class StdSiteAddInfoJB {

    /**
     * The primary key:pk_stdSiteADdinfoid
     */

    private int id;

    /**
     * The foreign key reference to er_studySite
     */
    private String studySiteId;

    /**
     * The Codelst Info
     */

    private String addCodelstInfo;

    /**
     * The value for codelst type
     */
    private String data;

    private String creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    private String modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    private String ipAdd;

    // /////////////////////////////////////////////////////////////////////////////////

    // GETTER AND SETTER METHODS

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setStudySiteId(String studySiteId) {
        this.studySiteId = studySiteId;
    }

    public String getStudySiteId() {
        return studySiteId;
    }

    public void setAddCodelstInfo(String addCodelstInfo) {
        this.addCodelstInfo = addCodelstInfo;
    }

    public String getAddCodelstInfo() {
        return addCodelstInfo;
    }

    public void setAddInfoData(String data) {
        this.data = data;
    }

    public String getAddInfoData() {
        return data;
    }

    /**
     * @return The Creator of the Form Section
     */

    public String getCreator() {
        return this.creator;
    }

    /**
     * 
     * @param to
     *            set the Creator of the Form Section
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the IP Address of the Form Section request
     */

    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Section request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // //////////////////////////////////////////////////////////////////

    // END OF THE GETTER AND SETTER METHODS

    /**
     * Constructor
     * 
     * @param studyIdId:
     *            Unique Id constructor
     * 
     */

    public StdSiteAddInfoJB(int id) {
        setId(id);
    }

    /**
     * Default Constructor
     */

    public StdSiteAddInfoJB() {
        Rlog.debug("stdSiteAddInfo", "StdSiteAddInfoJB.StdSiteAddInfoJB() ");
    }

    public StdSiteAddInfoJB(int id, String studySiteId, String addCodelstInfo,
            String data, String creator, String modifiedBy, String ipAdd) {
        setId(id);
        setStudySiteId(studySiteId);
        setAddCodelstInfo(addCodelstInfo);
        setAddInfoData(data);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

    /**
     * Calls getStdSiteAddInfoDetails of StdSiteAddInfo Session Bean:
     * StdSiteAddInfoAgentBean
     * 
     * @return StdSiteAddInfoStatKeeper
     * @see StdSiteAddInfoAgentBean
     */

    public StdSiteAddInfoBean getStdSiteAddInfoDetails() {
        StdSiteAddInfoBean stdSiteAddInfosk = null;
        try {

            StdSiteAddInfoAgentRObj stdSiteAddInfoAgentRObj = EJBUtil
                    .getStdSiteAddInfoAgentHome();
            stdSiteAddInfosk = stdSiteAddInfoAgentRObj
                    .getStdSiteAddInfoDetails(this.id);

        } catch (Exception e) {
            Rlog.fatal("stdSiteAddInfo",
                    "Error in StdSiteADdInfo getStdSiteAddInfoDetails" + e);
        }

        if (stdSiteAddInfosk != null) {
            setId(stdSiteAddInfosk.getId());
            setStudySiteId(stdSiteAddInfosk.getStudySiteId());
            setAddCodelstInfo(stdSiteAddInfosk.getAddCodelstInfo());
            setAddInfoData(stdSiteAddInfosk.getAddInfoData());
            setCreator(stdSiteAddInfosk.getCreator());
            setModifiedBy(stdSiteAddInfosk.getModifiedBy());
            setIpAdd(stdSiteAddInfosk.getIpAdd());

        }

        return stdSiteAddInfosk;

    }

    /**
     * Calls setStdSiteAddInfoDetails() of StdSiteAdInfo Id Session Bean:
     * StdSiteADdinfoAgentBean
     * 
     */

    public int setStdSiteAddInfoDetails() {
        int toReturn;
        try {

            StdSiteAddInfoAgentRObj stdSiteAddInfoAgentRObj = EJBUtil
                    .getStdSiteAddInfoAgentHome();

            StdSiteAddInfoBean tempStateKeeper = new StdSiteAddInfoBean();

            tempStateKeeper = this.createStdSiteAddInfoStateKeeper();

            toReturn = stdSiteAddInfoAgentRObj
                    .setStdSiteAddInfoDetails(tempStateKeeper);
            this.setId(toReturn);
            return toReturn;

        } catch (Exception e) {
            Rlog.fatal("stdSiteAddInfo",
                    "Error in setStdSiteAddInfoDetails() in StdSiteAddInfoJB "
                            + e);
            return -2;
        }
    }

    /**
     * 
     * @return a statekeeper object for the StdSiteAddInfo Record with the
     *         current values of the bean
     */
    public StdSiteAddInfoBean createStdSiteAddInfoStateKeeper() {

        return new StdSiteAddInfoBean(addCodelstInfo, creator, data, id, ipAdd,
                modifiedBy, studySiteId);

    }

    /**
     * Calls updateStdSiteAddInfo() of StdSiteAddInfo Session Bean:
     * StdSiteAddInfoAgentBean
     * 
     * @return The status as an integer
     */
    public int updateStdSiteAddInfo() {
        int output;
        try {

            StdSiteAddInfoAgentRObj stdSiteAddInfoAgentRObj = EJBUtil
                    .getStdSiteAddInfoAgentHome();
            output = stdSiteAddInfoAgentRObj.updateStdSiteAddInfo(this
                    .createStdSiteAddInfoStateKeeper());
        } catch (Exception e) {
            Rlog.debug("stdSiteAddInfo",
                    "EXCEPTION IN SETTING studySiteaddinfo DETAILS TO  SESSION BEAN"
                            + e);
            return -2;
        }
        return output;
    }

    public int removeStdSiteAddInfo() {

        int output;

        try {

            StdSiteAddInfoAgentRObj stdSiteAddInfoAgentRObj = EJBUtil
                    .getStdSiteAddInfoAgentHome();
            output = stdSiteAddInfoAgentRObj.removeStdSiteAddInfo(this.id);
            return output;

        } catch (Exception e) {
            Rlog.fatal("stdSiteAddInfo",
                    "in StudyIdJB -removeStdSiteAddInfo() method" + e);
            return -1;
        }

    }

    /**
     * Method to add the multiple study Ids
     * 
     * @param StdSiteAddInfoStateKeeper
     * 
     */

    public int createMultipleStdSiteAddInfo(StdSiteAddInfoBean ssk) {

        int ret = 0;

        try {

            StdSiteAddInfoAgentRObj stdSiteAddInfoAgentRObj = EJBUtil
                    .getStdSiteAddInfoAgentHome();
            ret = stdSiteAddInfoAgentRObj.createMultipleStdSiteAddInfo(ssk);

            return ret;
        } catch (Exception e) {
            Rlog.fatal("stSiteAddInfo",
                    "createMultipleStdSiteAddInfo in StdJB " + e);
            return -2;
        }

    }

    /**
     * Method to update multiple study Ids
     * 
     * @param StudyIdStateKeeper
     * 
     */

    /*
     * public int updateMultipleStudyIds(StudyIdStateKeeper ssk) {
     * 
     * int ret=0 ;
     * 
     * try { Rlog.debug("studyId","StudyIdJB.updateMultipleStudyIds");
     * StudyIdAgentHome studyIdAgentHome = EJBUtil.getStudyIdAgentHome();
     * StudyIdAgentRObj studyIdAgentRObj = studyIdAgentHome.create();
     * 
     * ret = studyIdAgentRObj.updateMultipleStudyIds(ssk);
     * 
     * return ret; } catch(Exception e) {
     * Rlog.fatal("studyId","updateMultipleStudyIds() in StudyIdJB " + e);
     * return -2 ; } }
     */
    public StdSiteAddInfoDao getStudySiteAddInfo(int studySiteId) {

        StdSiteAddInfoDao sd = new StdSiteAddInfoDao();

        try {

            StdSiteAddInfoAgentRObj stdSiteAddInfoAgentRObj = EJBUtil
                    .getStdSiteAddInfoAgentHome();

            sd = stdSiteAddInfoAgentRObj.getStudySiteAddInfo(studySiteId);

            return sd;
        } catch (Exception e) {
            Rlog
                    .fatal("stdSiteAddInfo", "getStudySiteAddInfo() in StdyJB "
                            + e);
            return null;
        }

    }

}
