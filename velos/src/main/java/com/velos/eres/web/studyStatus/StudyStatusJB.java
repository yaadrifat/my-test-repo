/*
 * Classname : StudyStatusJB.java 
 * 
 * Version information
 *
 * Date 02/26/2001
 * 
 * Copyright notice: Aithent
 */

package com.velos.eres.web.studyStatus;

import java.util.Date;
import java.util.Hashtable;
import javax.persistence.Column;

import com.velos.eres.business.common.StudySiteDao;
import com.velos.eres.business.common.StudyStatusDao;
import com.velos.eres.business.studyStatus.impl.StudyStatusBean;
import com.velos.eres.service.studyStatusAgent.StudyStatusAgentRObj;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/**
 * Client side bean for the Study Status
 * 
 * @author Sonia Sahni
 */

public class StudyStatusJB {

    /**
     * the Study Status Id
     */
    private int id;

    /**
     * the User who documented the status
     */
    private String statDocUser;

    /**
     * study status
     */
    private String studyStatus;

    /**
     * the Study id
     */

    private String statStudy;

    /**
     * the status start date
     */

    private String statStartDate;

    /**
     * the HSPN number
     */
    private String statHSPN;

    /**
     * the associated Notes
     */
    private String statNotes;
    
    private String externalLink;

    /**
     * the status end date
     */

    private String statEndDate;

    /**
     * the status valid unit date
     */

    private String statValidDate;
    
    /**
     * the Approval Status
     */

    private String statApproval;

    /**
     * the Approval Renewal Number
     */

    private String statAppRenNo;

    /**
     * the Site id
     */

    private String siteId;

    /**
     * creator
     */
    private String creator;

    /**
     * last modified by
     */
    private String modifiedBy;

    /**
     * IP Address
     */
    private String ipAdd;
    
    /**
     * Current Status
     */
    
    private String currentStat;
    
    /**
     * This field represent meeting date
     */
    private String statMeetingDate;
    
   /**
     * This field represent study status outcome
     */
    private String studyStatusOutcome;
    
   /**
     * This field represent study status type
     */
    private String studyStatusType;
    
    /**
     * the review board
     */
    private String studyStatusReviewBoard; 
    
    /**
     * the user tstaus assigned to
     */
    private String studyStatusAssignedTo; 
    
    

    // GETTER SETTER METHODS

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatApproval() {
        return this.statApproval;
    }

    public void setStatApproval(String statApproval) {
        this.statApproval = statApproval;
    }

    public String getStatDocUser() {
        return this.statDocUser;
    }

    public void setStatDocUser(String statDocUser) {
        this.statDocUser = statDocUser;
    }

    public String getStatEndDate() {
        return this.statEndDate;
    }

    public void setStatEndDate(String statEndDate) {
        this.statEndDate = statEndDate;
    }

    public String getStatValidDate() {
        Rlog.debug("studystatus", "getStatVliadDate in JB" + this.statValidDate);
        return this.statValidDate;
    }

    public void setStatValidDate(String statValidDate) {
        this.statValidDate = statValidDate;
        Rlog.debug("studystatus", "stStatVliadDate in JB" + statValidDate);
    }
    
   public String getStatHSPN() {
        return this.statHSPN;
    }

    public void setStatHSPN(String statHSPN) {
        this.statHSPN = statHSPN;
    }

    public String getStatNotes() {
        return this.statNotes;
    }

    public void setStatNotes(String statNotes) {
        this.statNotes = statNotes;
    }

    public String getStatStartDate() {
        return this.statStartDate;
    }

    public void setStatStartDate(String statStartDate) {
        this.statStartDate = statStartDate;
    }

    public String getStatStudy() {
        return this.statStudy;
    }

    public void setStatStudy(String statStudy) {
        this.statStudy = statStudy;
    }

    public String getStudyStatus() {
        return this.studyStatus;
    }

    public void setStudyStatus(String studyStatus) {
        this.studyStatus = studyStatus;
    }

    public String getStatAppRenNo() {
        return this.statAppRenNo;
    }

    public void setStatAppRenNo(String statAppRenNo) {
        this.statAppRenNo = statAppRenNo;
    }

    public String getSiteId() {
        return this.siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    //Added by Manimaran for the September Enhancement (S8).
    
    /**
     * @return Current Status
     */
    public String getCurrentStat() {
    	return this.currentStat;
    }
    
    /**
     * @param currentStat
     *            The Status of the study
     */
    public void setCurrentStat(String currentStat) {
    	this.currentStat = currentStat;
    }
    
//  Added by Amarnadh 
    
    public String getStatMeetingDate() {
           return this.statMeetingDate;
    }

    public void setStatMeetingDate(String statMeetingDate) {
        this.statMeetingDate = statMeetingDate;
    }
    
    public String getStudyStatusOutcome() {
        return this.studyStatusOutcome;
    }

    public void setStudyStatusOutcome(String studyStatusOutcome) {
     this.studyStatusOutcome = studyStatusOutcome;
 	}
    
    public String getStudyStatusType() {
        return this.studyStatusType;
    }

    public void setStudyStatusType(String studyStatusType) {
     this.studyStatusType = studyStatusType;
 	}
    
    //End added
    
    // END OF GETTER SETTER METHODS

    // CONSTRUCTOR TO CREATE SECTION OBJ WITH SEC ID
    public StudyStatusJB(String id) {

        int studyStatId = 0;
        studyStatId = EJBUtil.stringToNum(id);
        setId(studyStatId);
    }

    // DEFAULT CONSTRUCTOR
    public StudyStatusJB() {

    }

    /**
     * 
     */
    public StudyStatusBean getStudyStatusDetails() {

        StudyStatusBean ssk = null;

        try {

            StudyStatusAgentRObj statusAgent = EJBUtil
                    .getStudyStatusAgentHome();
            ssk = statusAgent.getStudyStatusDetails(this.id);
        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "EXception in calling session bean in study status" + e);

        }
        if (ssk != null) {

            this.id = ssk.getId();
            this.statApproval = ssk.getStatApproval();
            this.statDocUser = ssk.getStatDocUser();
            this.statEndDate = ssk.returnStatEndDate();
            this.statHSPN = ssk.getStatHSPN();
            this.statNotes = ssk.getStatNotes();
            this.externalLink = ssk.getExternalLink();
            this.statStartDate = ssk.returnStatStartDate();
            this.statValidDate = ssk.getStatValidDate();
            this.statStudy = ssk.getStatStudy();
            this.studyStatus = ssk.getStudyStatus();
            this.statAppRenNo = ssk.getStatAppRenNo();
            this.siteId = ssk.getSiteId();
            this.creator = ssk.getCreator();
            this.modifiedBy = ssk.getModifiedBy();
            this.ipAdd = ssk.getIpAdd();
            this.currentStat = ssk.getCurrentStat();//KM
          //Added by Amarnadh 
            this.statMeetingDate = ssk.getStatMeetingDate();
            this.studyStatusOutcome = ssk.getStudyStatusOutcome();
            this.studyStatusType = ssk.getStudyStatusType();
            
            setStudyStatusAssignedTo(ssk.getStudyStatusAssignedTo());
            setStudyStatusReviewBoard(ssk.getStudyStatusReviewBoard());
            	

        }
        return ssk;
    }

    /**
     * calls setCustomer() on the ReservationAgent facade.
     */
    public void setStudyStatusDetails() {

        try {
            StudyStatusAgentRObj statusAgent = EJBUtil
                    .getStudyStatusAgentHome();

            statusAgent.setStudyStatusDetails(this
                    .createStudyStatusStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "EXception in session bean - set study status details" + e);
            e.printStackTrace();

        }
    }

    /**
     * calls updateStudyStatus() on the StudyStatusAgent facade.
     */
    public int updateStudyStatus() {
        int ret = 0;
        try {
            StudyStatusAgentRObj statusAgent = EJBUtil
                    .getStudyStatusAgentHome();

            Rlog.debug("studystatus", "updateStudyStatus in JB");
            ret = statusAgent.updateStudyStatus(this
                    .createStudyStatusStateKeeper());
            return ret;
        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "in session bean - set study status details" + e);
            return -2;
        }
    }

    /*
     * this method returns all the details from table er_studystat corrosponding
     * to a perticular study id dinesh 04/10/01
     */
    public StudyStatusDao getStudy(int study) {
        try {
            StudyStatusAgentRObj statusAgent = EJBUtil
                    .getStudyStatusAgentHome();

            Rlog.debug("studystatus", "returning the study");

            return statusAgent.getStudy(study);

        } catch (Exception e) {
            Rlog.fatal("studystatus", "Error in getStudy() in SectionStatusJB "
                    + e);
            return null;
        }
    }

    public StudyStatusDao getStudyStatusDesc(int study, int siteId, int usr,
            int accId) {
        try {
            StudyStatusAgentRObj statusAgent = EJBUtil
                    .getStudyStatusAgentHome();
            return statusAgent.getStudyStatusDesc(study, siteId, usr, accId);
        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Error in getStudyStatusDesc() in SectionStatusJB " + e);
            return null;
        }
    }

    public int checkStatus(String studyId) {
        try {
            StudyStatusAgentRObj statusAgent = EJBUtil
                    .getStudyStatusAgentHome();

            return statusAgent.checkStatus(studyId);
        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Error in checkStatus() in SectionStatusJB " + e);
            return 0;
        }
    }

    /*
     * places bean attributes into a CustomerStateHolder.
     */
    public StudyStatusBean createStudyStatusStateKeeper() {
        Rlog.debug("studystatus", "createStudyStatusStateKeeper in JB this.statValidDate" + this.statValidDate);
        Rlog.debug("studystatus","createStudyStatusStateKeeper in JB this.statStartDate" + this.statStartDate);
      
        return new StudyStatusBean(id, statDocUser, studyStatus, statStudy,
                statStartDate, statHSPN, statNotes, statEndDate, statValidDate,statMeetingDate,studyStatusOutcome,studyStatusType,
                statApproval, statAppRenNo, siteId, creator, modifiedBy, ipAdd, currentStat,this.studyStatusAssignedTo,this.studyStatusReviewBoard,
                externalLink);
    }

    /*
     * delete study status
     */
    public int studyStatusDelete(int studyStatusId) {
        int output = 0;

        try {
            StudyStatusAgentRObj studyStatusAgentRobj = EJBUtil.getStudyStatusAgentHome();

            output = studyStatusAgentRobj.studyStatusDelete(studyStatusId);

        } catch (Exception e) {
            Rlog.fatal("studystatus", "Exception in removing study status");
            return -1;
        }
        return output;
    }
 // Overloaded for INF-18183 ::: Akshi
    public int studyStatusDelete(int studyStatusId,Hashtable<String, String> args) {
        int output = 0;

        try {
            StudyStatusAgentRObj studyStatusAgentRobj = EJBUtil.getStudyStatusAgentHome();

            output = studyStatusAgentRobj.studyStatusDelete(studyStatusId,args);

        } catch (Exception e) {
            Rlog.fatal("studystatus", "Exception in removing study status");
            return -1;
        }
        return output;
    }
 
    public int getFirstActiveEnrollPK(int studyId) {

        try {
            StudyStatusAgentRObj statusAgent = EJBUtil.getStudyStatusAgentHome();

            return statusAgent.getFirstActiveEnrollPK(studyId);
        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Error in getStudyStatusDesc() in SectionStatusJB " + e);
            return 0;
        }
    }

    public String getMinActiveDate(int studyId) {

        try {
            StudyStatusAgentRObj statusAgent = EJBUtil
                    .getStudyStatusAgentHome();

            return statusAgent.getMinActiveDate(studyId);
        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Error in getMinActiveDate() in StudyStatusJB " + e);
            return null;
        }
    }

    public String getMinPermanentClosureDate(int studyId) {

        try {
            StudyStatusAgentRObj statusAgent = EJBUtil
                    .getStudyStatusAgentHome();

            return statusAgent.getMinPermanentClosureDate(studyId);
        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Error in getMinPermanentClosureDate() in StudyStatusJB "
                            + e);
            return null;
        }
    }

    public int getCountStudyStat(int studyId) {
        try {
            StudyStatusAgentRObj statusAgent = EJBUtil
                    .getStudyStatusAgentHome();

            return statusAgent.getCountStudyStat(studyId);
        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Error in getCountStudyStat() in StudyStatusJB " + e);
            return -1;
        }
    }

    /*
     * generates a String of XML for the customer details entry form.<br><br>
     * Note that it is planned to encapsulate this detail in another object.
     */
    public String toXML() {
        return null;
    }// end of method
    
    
    public int getCountByOrgStudyStat(int studyId,int orgId,String studyTypeList,String flag) {
        try {
            StudyStatusAgentRObj statusAgent = EJBUtil
                    .getStudyStatusAgentHome();

            return statusAgent.getCountByOrgStudyStat(studyId,orgId,studyTypeList,flag);
        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Error in getCountByOrgStudyStat() in StudyStatusJB " + e);
            return -1;
        }
    }
    public StudyStatusDao getDAOByOrgStudyStat(int studyId,int orgId,String studyTypeList,String flag) {
        try {
            StudyStatusAgentRObj statusAgent = EJBUtil
                    .getStudyStatusAgentHome();

            return statusAgent.getDAOByOrgStudyStat(studyId,orgId,studyTypeList,flag);
        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Error in getCountByOrgStudyStat() in StudyStatusJB " + e);
            return null;
        }
    }

     
	public String getStudyStatusReviewBoard() {
		return studyStatusReviewBoard;
	}

	public void setStudyStatusReviewBoard(String studyStatusReviewBoard) {
		this.studyStatusReviewBoard = studyStatusReviewBoard;
	}

	 
	public String getStudyStatusAssignedTo() {
		return studyStatusAssignedTo;
	}

	public void setStudyStatusAssignedTo(String studyStatusAssignedTo) {
		this.studyStatusAssignedTo = studyStatusAssignedTo;
	}

	public String getExternalLink() {
		return externalLink;
	}

	public void setExternalLink(String externalLink) {
		this.externalLink = externalLink;
	}
	
	public void addStudyStatus(String studyId, String usr, int submittingCodeId) {
		//Get the pk of er_site for default org
  		StudySiteDao ssDao = new StudySiteDao();
  		int pkSite=0;
  		String pkSiteStr="";
  		//pkSite = ssDao.getPkSiteBySiteName(CFG.EIRB_DEFAULT_STUDY_STAT_ORG);
  		pkSite = ssDao.getPrimarySiteByUserId(Integer.parseInt((usr)));

  		if(pkSite != 0 ) {
  			pkSiteStr = (Integer.valueOf(pkSite)).toString();
  		}
  		
	    StudyStatusBean statBean = new StudyStatusBean();

		statBean.setStudyStatus(String.valueOf(submittingCodeId));
		statBean.setCreator(usr);
		statBean.setModifiedBy(usr);
		statBean.setStatStudy(studyId);
		statBean.setSiteId(pkSiteStr);
		statBean.setCurrentStat("1");
		statBean.setStatStartDate(new Date(System.currentTimeMillis()));
		
		StudyStatusAgentRObj studyStatusAgent = (StudyStatusAgentRObj)EJBUtil.getStudyStatusAgentHome();	   
		int newStatusPK = studyStatusAgent.setStudyStatusDetails(statBean);
		
		if (newStatusPK < 1) {
			Rlog.info("StudyStatusJB:addStudyStatus", "Study Status is not created.");            			    	                               
		} else {
			Rlog.info("StudyStatusJB:addStudyStatus", "Study Status is created successfully." + newStatusPK);
		}
	}
	
}// end of class

