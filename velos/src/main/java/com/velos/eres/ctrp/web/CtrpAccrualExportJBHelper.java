package com.velos.eres.ctrp.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletResponse;

import com.aithent.file.uploadDownload.Configuration;
import com.velos.eres.business.common.CtrpPatientAccrualDao;
import com.velos.eres.business.study.impl.StudyBean;
import com.velos.eres.business.studySite.impl.StudySiteBean;
import com.velos.eres.ctrp.service.CtrpAccrualAgent;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.service.util.VelosResourceBundle;
/**
 * @author Yogendra Pratap Singh
 */
public class CtrpAccrualExportJBHelper {

	private static final String SCH 				= "sch";
	public static final String LINE_SEPRATOR		= "\r\n";
	private static final String ZIP 				= ".zip";
	private static final String CONTENT_DISPOSITION = "Content-Disposition";
	private static final String COLLECTIONS = "COLLECTIONS";
	private static final String PATIENTS="PATIENTS";
	private static final String PATIENT_RACES="PATIENT_RACES";
	private static final String ACCRUAL_COUNT="ACCRUAL_COUNT";
	
	public ArrayList<String>  getStudySubjectData(
			Map<String, ArrayList<String>> stdNResTypeMap,CtrpAccrualJB ctrpAccrualJB) {
		ArrayList<String> generatedDoclist = new ArrayList<String>();	
		StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
		StudyBean studyBean = new StudyBean();
		ArrayList<CtrpAccrualExportJB> ctrpAccrualExportJBList = new ArrayList<CtrpAccrualExportJB>();
		Map<String , String> dataMapForPatient = new HashMap<String, String>();
		ArrayList<String> studyIds = stdNResTypeMap.get(CtrpAccrualExportJB.STUDY_IDS);
		ArrayList<String> researchTypes =stdNResTypeMap.get(CtrpAccrualExportJB.RESEARCH_TYPE);
		Boolean indusFlag = false;
		if(researchTypes.indexOf("Industrial")!=-1)
			indusFlag=true;
		ArrayList<String> userDetails =stdNResTypeMap.get(CtrpAccrualExportJB.USER_DETAILS);
		ArrayList<String> cutOffDateList =stdNResTypeMap.get(CtrpAccrualExportJB.CUT_OFFDATE);
		dataMapForPatient.put("userId", userDetails.get(0));
		dataMapForPatient.put("userGroupId", userDetails.get(1));
		dataMapForPatient.put("siteId", userDetails.get(2));
		dataMapForPatient.put("accountId", userDetails.get(3));
		dataMapForPatient.put("cutOffDate", cutOffDateList.get(0));
		for(String studyId :	studyIds){
			int index = 0;
			ArrayList<CtrpAccrualExportJB> ctrpAccrualExportJBListLocal = new ArrayList<CtrpAccrualExportJB>();
			CtrpAccrualExportJB ctrpAccrualExportJB = new CtrpAccrualExportJB();
			//String researchType = researchTypes.get( index );
			studyBean = studyAgent.getStudyDetails(StringUtil.stringToInteger(studyId));
			//Data for Study Collections 
			ArrayList<SubjectCollectionsAccrual> patientCollectionsList = this.getSubjectCollectionsAccrualList(studyBean);
			ctrpAccrualExportJB.setPatientCollectionsList(patientCollectionsList);
			
			//Set the Patient Data for the particular Study
			if(dataMapForPatient.containsKey("studyId"))
				dataMapForPatient.remove("studyId");
			dataMapForPatient.put("studyId", studyId);
		
			ArrayList<PatientRacesAccrual> patientRacesAccrualList = null;
			ArrayList<SiteAccrualCount> siteAccrualCountDataList = null;
			ArrayList<PatientAccrual> patientAccrualList = null;
			
			if(indusFlag){
				ArrayList<StudySiteBean> siteAccrualCountList = this.getSiteAccrualCount(dataMapForPatient);
				siteAccrualCountDataList = this.getPatientRacesAccraulForIndustrial(siteAccrualCountList,studyBean);
				
				// Get patient data for Indus Studies
				ArrayList<CtrpPatientAccrualDao> patAccPojoList = this.getPatientAccrualList(dataMapForPatient);
				patientAccrualList = fetchPatientAccraul(patAccPojoList );				
			}else{
				ArrayList<CtrpPatientAccrualDao> patAccPojoList = this.getPatientAccrualList(dataMapForPatient);
				patientRacesAccrualList = getPatientRacesAccraul(patAccPojoList);
				patientAccrualList = fetchPatientAccraul(patAccPojoList );
			}
			
			ctrpAccrualExportJB.setPatientList(patientAccrualList);			
			ctrpAccrualExportJB.setPatientRacesList(patientRacesAccrualList);
			ctrpAccrualExportJB.setSiteAccrualCountList(siteAccrualCountDataList);
			
			ctrpAccrualExportJBList.add(ctrpAccrualExportJB);
			ctrpAccrualExportJBListLocal.add(ctrpAccrualExportJB);
			String formatedCutOffDt ="";
			try {
				SimpleDateFormat fromUser = new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat myFormat = new SimpleDateFormat(com.velos.eres.service.util.Configuration.getAppDateFormat());

				formatedCutOffDt= fromUser.format(myFormat.parse(cutOffDateList.get(0)));
			} catch (ParseException e) {
					e.printStackTrace();
			}
			
			
			String docName=this.generateCtrpAccrualDocument( ctrpAccrualExportJBListLocal , indusFlag, formatedCutOffDt );
			
			generatedDoclist.add(docName);
			index++;
		}
		ctrpAccrualJB.setCtrpAccrualExportJB(ctrpAccrualExportJBList);
		return generatedDoclist;
	}
	
	public void populateStudySubjectData(
			Map<String, ArrayList<String>> stdNResTypeMap,CtrpAccrualJB ctrpAccrualJB) {
		
		StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
		StudyBean studyBean = new StudyBean();
		ArrayList<CtrpAccrualExportJB> ctrpAccrualExportJBList = new ArrayList<CtrpAccrualExportJB>();
		Map<String , String> dataMapForPatient = new HashMap<String, String>();
		ArrayList<String> studyIds = stdNResTypeMap.get(CtrpAccrualExportJB.STUDY_IDS);
		ArrayList<String> researchTypes =stdNResTypeMap.get(CtrpAccrualExportJB.RESEARCH_TYPE);
		Boolean indusFlag = false;
		if(researchTypes.indexOf("Industrial")!=-1)
			indusFlag=true;
		ArrayList<String> userDetails =stdNResTypeMap.get(CtrpAccrualExportJB.USER_DETAILS);
		ArrayList<String> cutOffDateList =stdNResTypeMap.get(CtrpAccrualExportJB.CUT_OFFDATE);
		dataMapForPatient.put("userId", userDetails.get(0));
		dataMapForPatient.put("userGroupId", userDetails.get(1));
		dataMapForPatient.put("siteId", userDetails.get(2));
		dataMapForPatient.put("cutOffDate", cutOffDateList.get(0));
		
		for(String studyId :	studyIds){
			int index = 0;
			CtrpAccrualExportJB ctrpAccrualExportJB = new CtrpAccrualExportJB();
			studyBean = studyAgent.getStudyDetails(StringUtil.stringToInteger(studyId));
			//Data for Study Collections 
			ArrayList<SubjectCollectionsAccrual> patientCollectionsList = this.getSubjectCollectionsAccrualList(studyBean);
			ctrpAccrualExportJB.setPatientCollectionsList(patientCollectionsList);
			
			//Set the Patient Data for the particular Study
			if(dataMapForPatient.containsKey("studyId"))
				dataMapForPatient.remove("studyId");
			dataMapForPatient.put("studyId", studyId);
		
			ArrayList<PatientRacesAccrual> patientRacesAccrualList = null;
			ArrayList<SiteAccrualCount> siteAccrualCountDataList = null;
			ArrayList<PatientAccrual> patientAccrualList = null;
			
			if(indusFlag){
				ArrayList<StudySiteBean> siteAccrualCountList = this.getSiteAccrualCount(dataMapForPatient);
				siteAccrualCountDataList = this.getPatientRacesAccraulForIndustrial(siteAccrualCountList,studyBean);
				ArrayList<CtrpPatientAccrualDao> patAccPojoList = this.getPatientAccrualList(dataMapForPatient);
				patientAccrualList = fetchPatientAccraul(patAccPojoList);
			}else{
				ArrayList<CtrpPatientAccrualDao> patAccPojoList = this.getPatientAccrualList(dataMapForPatient);
				patientRacesAccrualList = getPatientRacesAccraul(patAccPojoList);
				patientAccrualList = fetchPatientAccraul(patAccPojoList);
			}
			
			ctrpAccrualExportJB.setPatientList(patientAccrualList);			
			ctrpAccrualExportJB.setPatientRacesList(patientRacesAccrualList);
			ctrpAccrualExportJB.setSiteAccrualCountList(siteAccrualCountDataList);
			ctrpAccrualExportJBList.add(ctrpAccrualExportJB);
			index++;
		}
		ctrpAccrualJB.setCtrpAccrualExportJB(ctrpAccrualExportJBList);
	}

	private ArrayList<StudySiteBean> getSiteAccrualCount(Map<String, String> dataMapForPatient) {
		CtrpAccrualAgent ctrpAccrualAgent = null;
		ArrayList<StudySiteBean> patAcclist = null;
		//get the enrolled patient DataList on the basis of Study Id
		try {
			ctrpAccrualAgent = EJBUtil.getCtrpAccrualAgentBean();
			patAcclist = ctrpAccrualAgent.getSiteAccrualCount( dataMapForPatient );
		} catch (NamingException e) {
			e.printStackTrace();
		}
		return patAcclist;
		
	}

	public ArrayList<SubjectCollectionsAccrual> getSubjectCollectionsAccrualList(StudyBean studyBean ){
		ArrayList<SubjectCollectionsAccrual> patientCollectionsList = new ArrayList<SubjectCollectionsAccrual>();
		SubjectCollectionsAccrual subjectCollectionsAccrual= new SubjectCollectionsAccrual();
		subjectCollectionsAccrual.setSubjectCollectionsAccrual(COLLECTIONS);
		subjectCollectionsAccrual.setStudyIdentifier(studyBean.getNciTrialIdentifier());
		subjectCollectionsAccrual.setStudyId(studyBean.getId().toString());
		subjectCollectionsAccrual.setBlankField1(CtrpAccrualExportJB.BLANK_VAL);//1
		subjectCollectionsAccrual.setBlankField2(CtrpAccrualExportJB.BLANK_VAL);//2
		subjectCollectionsAccrual.setBlankField3(CtrpAccrualExportJB.BLANK_VAL);//3
		subjectCollectionsAccrual.setBlankField4(CtrpAccrualExportJB.BLANK_VAL);//4
		subjectCollectionsAccrual.setBlankField5(CtrpAccrualExportJB.BLANK_VAL);//5
		subjectCollectionsAccrual.setBlankField6(CtrpAccrualExportJB.BLANK_VAL);//6
		subjectCollectionsAccrual.setBlankField7(CtrpAccrualExportJB.BLANK_VAL);//7
		subjectCollectionsAccrual.setBlankField8(CtrpAccrualExportJB.BLANK_VAL);//8
		subjectCollectionsAccrual.setBlankField9(CtrpAccrualExportJB.BLANK_VAL);//9
		
		patientCollectionsList.add(subjectCollectionsAccrual);
		return patientCollectionsList;
	}
	
	public ArrayList<CtrpPatientAccrualDao> getPatientAccrualList(Map<String , String> dataMapForPatient){
		CtrpAccrualAgent ctrpAccrualAgent = null;
		ArrayList<CtrpPatientAccrualDao> patAcclist = null;
		//get the enrolled patient DataList on the basis of Study Id
		try {
			ctrpAccrualAgent = EJBUtil.getCtrpAccrualAgentBean();
			patAcclist = ctrpAccrualAgent.getPatientAccrualData( dataMapForPatient );
		} catch (NamingException e) {
			e.printStackTrace();
		}
		return patAcclist;
	}
	
	public ArrayList<PatientAccrual> fetchPatientAccraul( ArrayList<CtrpPatientAccrualDao> patAccPojoList) {
		ArrayList<PatientAccrual> patientAccrualList = new ArrayList<PatientAccrual>();
		Set<String> nciCheckForPatStdId = new HashSet<String>();
		for(CtrpPatientAccrualDao ctrpPatientAccrualPojo : patAccPojoList){
			PatientAccrual patientAccrual = new PatientAccrual();
			patientAccrual.setPatientAccrual(PATIENTS);
			patientAccrual.setStudyIdentifier( ctrpPatientAccrualPojo.getNciTrailIden());
			patientAccrual.setStudySubjectIdentifier(ctrpPatientAccrualPojo.getPkPatProt());
			//To check for duplicate (PATIENT STUDY ID) 
			/*if(!nciCheckForPatStdId.add(ctrpPatientAccrualPojo.getPkPatProt())){
				patientAccrual.setIsDuplicatePatStdId(Boolean.TRUE);
			}*/
			patientAccrual.setZipCode(ctrpPatientAccrualPojo.getPersonZip());
			patientAccrual.setCountryCode(ctrpPatientAccrualPojo.getPersonCountry());
			patientAccrual.setDmgrphReportable(ctrpPatientAccrualPojo.getDmgrphReportable());
			String dob =formatDateString(ctrpPatientAccrualPojo.getPersonDOB(),"yyyyMM","yyyy-MM-dd");
			if(!(ctrpPatientAccrualPojo.getDmgrphReportable() == 1 || "".equals(dob))){
				dob=dob.substring(0, 4);
				dob= dob+"00";
			}
			patientAccrual.setBirthDate(dob);
			patientAccrual.setGender(ctrpPatientAccrualPojo.getPersonGender());
			patientAccrual.setEthnicity(ctrpPatientAccrualPojo.getPatEthnicity());
			patientAccrual.setPaymentMethod(CtrpAccrualExportJB.BLANK_VAL);//Need to change With Real value
			patientAccrual.setSubjectRegistrationDate(formatDateString(ctrpPatientAccrualPojo.getPersonRegDate(),"yyyyMMdd","yyyy-MM-dd"));
			patientAccrual.setStudySiteIdentifier(ctrpPatientAccrualPojo.getSiteId());
			patientAccrual.setSubjectDiseaseCode(ctrpPatientAccrualPojo.getSubjectDiseaseCode());
			patientAccrual.setBlankField1(CtrpAccrualExportJB.BLANK_VAL);
			patientAccrual.setBlankField2(CtrpAccrualExportJB.BLANK_VAL);
			patientAccrual.setBlankField3(CtrpAccrualExportJB.BLANK_VAL);
			patientAccrual.setBlankField4(CtrpAccrualExportJB.BLANK_VAL);
			patientAccrual.setBlankField5(CtrpAccrualExportJB.BLANK_VAL);
			patientAccrual.setBlankField6(CtrpAccrualExportJB.BLANK_VAL);
			patientAccrual.setBlankField7(CtrpAccrualExportJB.BLANK_VAL);
			patientAccrual.setBlankField8(CtrpAccrualExportJB.BLANK_VAL);
			patientAccrual.setBlankField9(CtrpAccrualExportJB.BLANK_VAL);
			patientAccrual.setBlankField10(CtrpAccrualExportJB.BLANK_VAL);
			patientAccrual.setBlankField11(CtrpAccrualExportJB.BLANK_VAL);
			patientAccrual.setPatEnrSiteOnStdTeam(ctrpPatientAccrualPojo.getPatEnrSiteOnStdTeam());
			
			patientAccrualList.add(patientAccrual);
		}
		return patientAccrualList;
	}
		
	public ArrayList<PatientRacesAccrual> getPatientRacesAccraul(ArrayList<CtrpPatientAccrualDao> patAccPojoList) {
		ArrayList<PatientRacesAccrual> patientAccrualList = new ArrayList<PatientRacesAccrual>();
		for(CtrpPatientAccrualDao ctrpPatientAccrualPojo : patAccPojoList){
			PatientRacesAccrual patientRacesAccrual = new PatientRacesAccrual();
			patientRacesAccrual.setPatientRaces(PATIENT_RACES);
			patientRacesAccrual.setStudyIdentifier(ctrpPatientAccrualPojo.getNciTrailIden());
			patientRacesAccrual.setStudySubjectIdentifier(ctrpPatientAccrualPojo.getPkPatProt());
			patientRacesAccrual.setRace(ctrpPatientAccrualPojo.getPatRace());
			patientAccrualList.add(patientRacesAccrual);
		}
		return patientAccrualList;
	}
	
	public ArrayList<SiteAccrualCount> getPatientRacesAccraulForIndustrial(ArrayList<StudySiteBean> siteAccrualCountList ,StudyBean studyBean) {
		ArrayList<SiteAccrualCount> patientAccrualCountList = new ArrayList<SiteAccrualCount>();
		for(StudySiteBean studySiteBean : siteAccrualCountList){
			SiteAccrualCount siteAccrualCount = new SiteAccrualCount();
			siteAccrualCount.setAccrualCount(ACCRUAL_COUNT);
			siteAccrualCount.setStudyIdentifier(studyBean.getNciTrialIdentifier());
			siteAccrualCount.setStudySiteIdentifier(studySiteBean.getSiteSiteId());
			siteAccrualCount.setStudySiteAccrualCount(studySiteBean.getCurrentSitePatientCount());
			patientAccrualCountList.add(siteAccrualCount);
		}
		return patientAccrualCountList;
	}

	
	public String generateCtrpAccrualDocument(ArrayList<CtrpAccrualExportJB> ctrpAccrualExportJBList ,Boolean industrialFlag, String CutOffDate) {
		String docName = null;
		if(industrialFlag)
			docName = this.generateIndustryDcoument( ctrpAccrualExportJBList , CutOffDate);
		else
			docName = this.generateNonIndustryDcoument( ctrpAccrualExportJBList, CutOffDate );
		return docName;		
	}
	
	public HashMap<String , Object> getFileWriterForAccrualExport(String fileName) {
		HashMap<String , Object> hValue = new HashMap<String, Object>();
		Calendar now = Calendar.getInstance();
		long st = now.getTimeInMillis();
		FileWriter fw = null;
		//String fileName = null;
		File file = null;
		try{
		if (com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD==null) com.aithent.file.uploadDownload.Configuration.readSettings(SCH);
		com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(
        		com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD
                        + "fileUploadDownload.xml", "study");
		file = new File(com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER + "/"+fileName+".txt");
		if (file.exists()){
			//file.delete();
			file = new File(com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER + "/"+fileName+"_"+st+".txt");
			hValue.put("file", fileName+"_"+st );
		}else{
			hValue.put("file", fileName );
		}
		file.createNewFile();
		fw = new FileWriter(file);
		hValue.put("fw", fw);
		
		}catch(Exception e){
			Rlog.fatal("ctrp", "Exception in CtrpDraftDocJBHelper.getFileWriterForCSVExport"+e);
			e.printStackTrace();
		}
		return hValue;
	}
	/**
	 * @param ctrpAccrualExportJBList
	 * @return
	 */
	public String generateIndustryDcoument(ArrayList<CtrpAccrualExportJB> ctrpAccrualExportJBList, String CutOffDate) {
		
		FileWriter fw =null;
		String docName =null;
		HashMap<String , Object> hValue=null;
		for(CtrpAccrualExportJB ctrpAccrualExportJB : ctrpAccrualExportJBList){
			try{
			ArrayList<SubjectCollectionsAccrual>  subjectCollectionsAccrualList = ctrpAccrualExportJB.getPatientCollectionsList();
			for(SubjectCollectionsAccrual subjectCollectionsAccrual: subjectCollectionsAccrualList){
				
				String NCINumber =subjectCollectionsAccrual.getStudyIdentifier()+"_"+CutOffDate;
				hValue =this.getFileWriterForAccrualExport(NCINumber);
				fw =	( FileWriter ) hValue.get("fw");
				appendDataAndComman (subjectCollectionsAccrual.getSubjectCollectionsAccrual(),fw);
				appendDataAndComman (subjectCollectionsAccrual.getStudyIdentifier(),fw);
				appendDataAndComman (subjectCollectionsAccrual.getBlankField1(),fw);
				appendDataAndComman (subjectCollectionsAccrual.getBlankField2(),fw);
				appendDataAndComman (subjectCollectionsAccrual.getBlankField3(),fw);
				appendDataAndComman (subjectCollectionsAccrual.getBlankField4(),fw);
				appendDataAndComman (subjectCollectionsAccrual.getBlankField5(),fw);
				appendDataAndComman (subjectCollectionsAccrual.getBlankField6(),fw);
				appendDataAndComman (subjectCollectionsAccrual.getBlankField7(),fw);
				appendDataAndComman (subjectCollectionsAccrual.getBlankField8(),fw);
				appendDataAndComman (subjectCollectionsAccrual.getBlankField9(),fw,true);
				appendDataAndComman(LINE_SEPRATOR,fw,true);	
				
				ArrayList<SiteAccrualCount> siteAccrualCountList = ctrpAccrualExportJB.getSiteAccrualCountList();
				for(SiteAccrualCount siteAccrualCount:siteAccrualCountList){
					appendDataAndComman(siteAccrualCount.getAccrualCount() , fw);
					appendDataAndComman(siteAccrualCount.getStudyIdentifier().toString() , fw);
					appendDataAndComman(siteAccrualCount.getStudySiteIdentifier().toString() , fw);
					appendDataAndComman(siteAccrualCount.getStudySiteAccrualCount() , fw , true);
					appendDataAndComman(LINE_SEPRATOR, fw , true);						
				}
				docName =	( String ) hValue.get("file");
				//docName = subjectCollectionsAccrual.getStudyIdentifier();
				fw.close();
			}
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
		return docName;
	}
	/**
	 * @param ctrpAccrualExportJBList
	 * @return
	 */
	public String generateNonIndustryDcoument(ArrayList<CtrpAccrualExportJB> ctrpAccrualExportJBList , String CutOffDate) {
		FileWriter fw =null;
		String docName = null;
		HashMap<String , Object> hValue=null;
		for(CtrpAccrualExportJB ctrpAccrualExportJB : ctrpAccrualExportJBList){
			try{
			ArrayList<SubjectCollectionsAccrual>  subjectCollectionsAccrualList = ctrpAccrualExportJB.getPatientCollectionsList();
			for(SubjectCollectionsAccrual subjectCollectionsAccrual: subjectCollectionsAccrualList){
				String NCINumber =subjectCollectionsAccrual.getStudyIdentifier()+"_"+CutOffDate;
				hValue =this.getFileWriterForAccrualExport(NCINumber);
				fw =	( FileWriter ) hValue.get("fw");
				appendDataAndComman (subjectCollectionsAccrual.getSubjectCollectionsAccrual(),fw);
				appendDataAndComman (subjectCollectionsAccrual.getStudyIdentifier(),fw);
				appendDataAndComman (subjectCollectionsAccrual.getBlankField1(),fw);
				appendDataAndComman (subjectCollectionsAccrual.getBlankField2(),fw);
				appendDataAndComman (subjectCollectionsAccrual.getBlankField3(),fw);
				appendDataAndComman (subjectCollectionsAccrual.getBlankField4(),fw);
				appendDataAndComman (subjectCollectionsAccrual.getBlankField5(),fw);
				appendDataAndComman (subjectCollectionsAccrual.getBlankField6(),fw);
				appendDataAndComman (subjectCollectionsAccrual.getBlankField7(),fw);
				appendDataAndComman (subjectCollectionsAccrual.getBlankField8(),fw);
				appendDataAndComman (subjectCollectionsAccrual.getBlankField9(),fw,true);
				appendDataAndComman(LINE_SEPRATOR,fw,true);	
				
				ArrayList<PatientAccrual>  patientAccrualList = ctrpAccrualExportJB.getPatientList();
				for(PatientAccrual PatientAccrual:patientAccrualList){
					appendDataAndComman(PatientAccrual.getPatientAccrual(),fw);
					appendDataAndComman(PatientAccrual.getStudyIdentifier(),fw);
					appendDataAndComman(PatientAccrual.getStudySubjectIdentifier(),fw);
					appendDataAndComman(PatientAccrual.getZipCode(),fw);
					appendDataAndComman(PatientAccrual.getCountryCode(),fw);
					appendDataAndComman(PatientAccrual.getBirthDate(),fw);
					appendDataAndComman(PatientAccrual.getGender(),fw);
					appendDataAndComman(PatientAccrual.getEthnicity(),fw);
					appendDataAndComman(PatientAccrual.getPaymentMethod(),fw);
					appendDataAndComman(PatientAccrual.getSubjectRegistrationDate(),fw);
					appendDataAndComman(PatientAccrual.getBlankField1(),fw);
					appendDataAndComman(PatientAccrual.getStudySiteIdentifier(),fw);
					appendDataAndComman(PatientAccrual.getBlankField2(),fw);
					appendDataAndComman(PatientAccrual.getBlankField3(),fw);
					appendDataAndComman(PatientAccrual.getBlankField4(),fw);
					appendDataAndComman(PatientAccrual.getBlankField5(),fw);
					appendDataAndComman(PatientAccrual.getBlankField6(),fw);
					appendDataAndComman(PatientAccrual.getBlankField7(),fw);
					appendDataAndComman(PatientAccrual.getBlankField8(),fw);
					appendDataAndComman(PatientAccrual.getBlankField9(),fw);
					appendDataAndComman(PatientAccrual.getBlankField10(),fw);
					appendDataAndComman(PatientAccrual.getSubjectDiseaseCode(),fw);
					appendDataAndComman(PatientAccrual.getBlankField11(),fw);
					appendDataAndComman(PatientAccrual.getBlankField12(),fw,true);
					appendDataAndComman(LINE_SEPRATOR,fw,true);	
				}
				
				ArrayList<PatientRacesAccrual> patientRacesAccrualList = ctrpAccrualExportJB.getPatientRacesList();
				for(PatientRacesAccrual patientRacesAccrual:patientRacesAccrualList){
					appendDataAndComman(patientRacesAccrual.getPatientRaces(),fw);
					appendDataAndComman(patientRacesAccrual.getStudyIdentifier(),fw);
					appendDataAndComman(patientRacesAccrual.getStudySubjectIdentifier(),fw);
					appendDataAndComman(patientRacesAccrual.getRace(),fw,true);
					appendDataAndComman(LINE_SEPRATOR,fw,true);						
				}
				docName =	( String ) hValue.get("file");
				//docName= subjectCollectionsAccrual.getStudyIdentifier() ;
				fw.close();
			}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return docName;
	}

	/**
	 * @param data
	 * @param fileWriter
	 * @return
	 */
	public FileWriter appendDataAndComman(String data,FileWriter fileWriter){
		return appendDataAndComman( data, fileWriter,false);
	}

	/**
	 * @param data
	 * @param fileWriter
	 * @param isLast
	 * @return
	 */
	public FileWriter appendDataAndComman(String data,FileWriter fileWriter,boolean isLast){
		ArrayList<String> alist = getConstantsList();
		try{
			if(StringUtil.isEmpty(data) && !isLast){
				data="";
			}
			boolean isNum = isNumeric(data);
			if(!isLast){
				if(!isNum && !data.equalsIgnoreCase("") ){
					if(!alist.contains(data)){
						fileWriter.append("\""+data+"\"").append(",");
					}else{
						fileWriter.append(data).append(",");
					}
				}else{
					fileWriter.append(data).append(",");
				}
			}else{
				if(!isNum && !data.equalsIgnoreCase("") && !data.equalsIgnoreCase(LINE_SEPRATOR)){
					if(!alist.contains(data)){
						fileWriter.append("\""+data+"\"");
					}else{
						fileWriter.append(data);
					}
				}else{
					fileWriter.append(data);
				}
			}
		}catch (Exception e) {
			Rlog.fatal("CTRP ACCRUAL", "Porblem while appending the comma" +
					" in CtrpAccrualExportJBHelper.appendDataAndComman "+e);
		}
		return fileWriter;
	}
	
	/**
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str) {
		try {
			NumberFormat.getInstance().parse(str);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}

	/**
	 * @param accrualNameList
	 * @return
	 */
	public String prepareZipFileFromDoc(List<String> accrualNameList,Boolean indusFlag) {
		String zipFileLocation = null;
		String targetFileName = null;
		String FILE_NAME = null;
		try {
			
			Calendar now = Calendar.getInstance();
			long st = now.getTimeInMillis();
			String strDate = DateUtil.getCurrentDate("yyyyMMDD").replaceAll("/", "");
			
			if(indusFlag){
				FILE_NAME = LC.L_Abbreviated_Accrual_ZIP;
			}else{
				FILE_NAME = LC.L_Complete_Accrual_ZIP;
			}
			if (Configuration.FILE_UPLOAD_DOWNLOAD==null) Configuration.readSettings(SCH);
            Configuration.readUploadDownloadParam(
                    Configuration.FILE_UPLOAD_DOWNLOAD
                            + "fileUploadDownload.xml", "study");
			zipFileLocation = Configuration.DOWNLOADFOLDER+"/"+FILE_NAME+"_"+strDate+"_"+st+ZIP;
			//to delete the file if path length is greater then 260 and return
			if(zipFileLocation.length()>260){
				for (String fileName:accrualNameList) {
					String filePath = EJBUtil.getActualPath(Configuration.DOWNLOADFOLDER, fileName+".txt");
					deleteCTRPAccrualTempDoc(filePath);
				}
				return "fileNameTooLong";
			}
			byte[] buffer = new byte[1024 * 1024 * 8];
			FileOutputStream fout = new FileOutputStream(zipFileLocation);
			ZipOutputStream zout = new ZipOutputStream(fout);

			for (String fileName:accrualNameList) {
				String filePath = EJBUtil.getActualPath(Configuration.DOWNLOADFOLDER, fileName+".txt");
				FileInputStream fin = new FileInputStream(filePath);
				zout.putNextEntry(new ZipEntry(FILE_NAME+"_"+strDate+"_"+st+"/"+ fileName+".txt"));
				int length = 0;
				while ((length = fin.read(buffer)) > 0) {
					zout.write(buffer, 0, length);
				}
				zout.closeEntry();
				fin.close();
				deleteCTRPAccrualTempDoc(filePath);
			}
			// close the ZipOutputStream
			zout.close();
			fout.close();
			targetFileName = zipFileLocation.substring(zipFileLocation.lastIndexOf("/")+1,zipFileLocation.length());
		} catch (IOException ioe) {
			Rlog.debug("CTRP ACCRUAL","Exception in prepareZipFileFromDoc "+ioe.getMessage());
		}
		return targetFileName;
	}

	/**
	 * @param filePath
	 */
	public void deleteCTRPAccrualTempDoc(String filePath) {
		try{
			File file = new File(filePath);
			if(file.exists() && file.delete())
				Rlog.info("CTRP ACCRUAL", "Temporary file "+file.getName()+" deleted successfully");
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void downloadPatStdAccrual(HttpServletResponse response, String zipFileName) {
		InputStream in=null;
		OutputStream output=null;
		String fileName =zipFileName;
		String tempFilePath = null;
		File file = null;
		try{
			file = new File(zipFileName);
			if (com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD==null) com.aithent.file.uploadDownload.Configuration.readSettings("sch");
				com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(
				com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD
		                + "fileUploadDownload.xml", "study");
		
			tempFilePath = EJBUtil.getActualPath(com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER, zipFileName);
			file = new File(tempFilePath );
			response.reset();
			response.resetBuffer();
			response.setContentType("application/zip");
			response.setHeader(CONTENT_DISPOSITION,"attachment;filename="+fileName);
			in = new FileInputStream( file );
			output = response.getOutputStream();
			int bufread = 0;
			while ((bufread = in.read()) != -1) {
				output.write(bufread);
			}
			Rlog.info("CTRP", "Zip file generated with name:"+fileName);
		} catch (Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpAccrualExportJBHelper.downloadPatStdAccrual " +
					"when open the ZIP document :"+fileName+" Exception is :"+e);
			e.printStackTrace();
		} finally {
			try {
				output.close();
				in.close();
				response.flushBuffer();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public void downloadErrorList(HttpServletResponse response, String zipFileName) {
		
		InputStream in=null;
		OutputStream output=null;
		String fileName =zipFileName;
		String tempFilePath = null;
		File file = null;
		try{
			file = new File(zipFileName);
			if (com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD==null) com.aithent.file.uploadDownload.Configuration.readSettings("sch");
				com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(
				com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD
		                + "fileUploadDownload.xml", "study");
		
			tempFilePath = EJBUtil.getActualPath(com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER, zipFileName);
			file = new File(tempFilePath );
			response.reset();
			response.resetBuffer();
			response.setContentType("text/plain");
			response.setHeader(CONTENT_DISPOSITION,"attachment;filename="+fileName);
			in = new FileInputStream( file );
			output = response.getOutputStream();
			int bufread = 0;
			while ((bufread = in.read()) != -1) {
				output.write(bufread);
			}
			Rlog.info("CTRP", "Zip file generated with name:"+fileName);
		} catch (Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpAccrualExportJBHelper.downloadErrorList " +
					"when open the ZIP document :"+fileName+" Exception is :"+e);
			e.printStackTrace();
		} finally {
			try {
				output.close();
				in.close();
				response.flushBuffer();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public String formatDateString(String dateStr, String format,String currentFormat) {
		String reformattedStr = "";
		if (null != dateStr) {
			try {
				SimpleDateFormat fromUser = new SimpleDateFormat(currentFormat);
				SimpleDateFormat myFormat = new SimpleDateFormat(format);

				reformattedStr = myFormat.format(fromUser.parse(dateStr));
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return reformattedStr;
	}

	public void setStudyLastAccrualGenerationDate(List<String> studyIds) {
		StudyBean studyBean = new StudyBean();
		Date dt1 = new java.util.Date();
		String stDate = DateUtil.dateToString(dt1);
		StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
		for(String    studyId : studyIds){
			studyBean = studyAgent.getStudyDetails(StringUtil.stringToInteger(studyId));
			studyBean.setCtrpAccrualLastGenerationDate(stDate);
			studyAgent.updateStudy(studyBean);
		}
	}
	public void setCutOffDtAndLastDwnLoadBy(List<String> studyIds, String cutOffDate, String userId) {
		StudyBean studyBean = new StudyBean();
		StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
		for(String    studyId : studyIds){
			studyBean = studyAgent.getStudyDetails(StringUtil.stringToInteger(studyId));
			studyBean.setCutOffDate(cutOffDate);
			studyBean.setLastDownloadBy(userId);
			studyAgent.updateStudy(studyBean);
		}
	}

	public void deleteCTRPAccrualDoc(String zipFileName) {
		try {
			if (com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD == null)
				com.aithent.file.uploadDownload.Configuration.readSettings("sch");

			com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(
					com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD
						+ "fileUploadDownload.xml", "study");

			String filePath = EJBUtil.getActualPath(Configuration.DOWNLOADFOLDER, zipFileName);
			this.deleteCTRPAccrualTempDoc(filePath);
		} catch (Exception e) {
			Rlog.debug("CTRP ACCRUAL","Exception in deleteCTRPAccrualDoc "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	private ArrayList<String> getConstantsList(){
		ArrayList<String> alist = new ArrayList<String>();
		alist.add(COLLECTIONS);
		alist.add(PATIENTS);
		alist.add(PATIENT_RACES);
		alist.add(ACCRUAL_COUNT);
		return alist;
	}
	/**
	 * @param 
	 * @return
	 */
	public String saveErrorList(String stdIdentifier,String validationMsg,String patEnrSite,int msgType) {
		
		FileWriter fw =null;
		File file = null;
		
		try{
			if (com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD==null){
			com.aithent.file.uploadDownload.Configuration.readSettings(SCH);
			com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(
			com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD
		                        + "fileUploadDownload.xml", "study");
			}
		
			file = new File(com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER + "/"+stdIdentifier+".txt");
			file.createNewFile();
			fw = new FileWriter(file);
			
			if(msgType==0){// Indus
				fw.append(LC.CTRP_ACCR_Val_Fail_Rea+LINE_SEPRATOR);
				fw.append(validationMsg);
			}else if(msgType==1){// Non Indus Study level 
				fw.append(LC.CTRP_ACCR_Val_Fail_Rea+LINE_SEPRATOR);
				fw.append(validationMsg);
			}else if(msgType==2){// Non Indis Pat level
				fw.append(MC.M_CTRP_ACC_ERR_LST+LINE_SEPRATOR);
				String [] patMsgArray = validationMsg.split("~");
				for(String patmsg:patMsgArray){
					//StringTokenizer st = new StringTokenizer(patmsg,":");	
					fw.append(LINE_SEPRATOR+patmsg);
				}
			}else if(msgType==3){// Indus with pat enrolling site error
				
				fw.append(LC.CTRP_ACCR_Val_Fail_Rea+LINE_SEPRATOR);
				fw.append(validationMsg+LINE_SEPRATOR);
			
				String [] patEnrSiteErr = patEnrSite.split(",");
				for(String patSiteData:patEnrSiteErr){
					Object [] patAndSiteName = patSiteData.split(":");
					fw.append(VelosResourceBundle.getMessageString("M_PatEnrSite_NotOnStdTeam",
							patAndSiteName)+LINE_SEPRATOR);
				}
			}else if(msgType==4){
				fw.append(MC.M_CTRP_ACC_ERR_LST+LINE_SEPRATOR);
				String [] patMsgArray = validationMsg.split("~");
				for(String patmsg:patMsgArray){
					fw.append(patmsg+LINE_SEPRATOR);
				}
			
				String [] patEnrSiteErr = patEnrSite.split(",");
				for(String patSiteData:patEnrSiteErr){
					Object [] patAndSiteName = patSiteData.split(":");
					fw.append(VelosResourceBundle.getMessageString("M_PatEnrSite_NotOnStdTeam",
							patAndSiteName)+LINE_SEPRATOR);
				}
				
			}else if(msgType==5){
				fw.append(LC.CTRP_ACCR_Val_Fail_Rea+LINE_SEPRATOR);
				fw.append(validationMsg+LINE_SEPRATOR);
				String [] patEnrSiteErr = patEnrSite.split(",");
				for(String patSiteData:patEnrSiteErr){
					Object [] patAndSiteName = patSiteData.split(":");
					fw.append(VelosResourceBundle.getMessageString(MC.M_PatEnrSite_NotOnStdTeam,
							patAndSiteName)+LINE_SEPRATOR);
				}
				
			}
			fw.close();
		}catch(Exception e){
			Rlog.fatal("ctrp", "Exception in CtrpDraftDocJBHelper.saveErrorList"+e);
			e.printStackTrace();
		}		
		return stdIdentifier+".txt";
	}
	
	public void validateStudyData(CtrpAccrualJB ctrpAccrualJB,Map<String, String> dataMap, Boolean indusFlag) {

		StringBuilder validationStrStd = new StringBuilder();
		StringBuilder validationStrPat = new StringBuilder();
		StringBuilder tempPatEnrSiteMsg = new StringBuilder();

		if (indusFlag) {// For Indus Studies

			for (CtrpAccrualExportJB ctrpAccrualExportJB : ctrpAccrualJB.ctrpAccrualExportJB) {
				ArrayList<SiteAccrualCount> siteAccrualCountList = ctrpAccrualExportJB
						.getSiteAccrualCountList();
				ArrayList<PatientAccrual> patientAccrualList = ctrpAccrualExportJB
				.getPatientList();
				
				String stdIdentifier =""; 
				for (SiteAccrualCount siteAccrualCount : siteAccrualCountList) {

					// Not expected to enter this NCI Identifier is prerequisite
					if (StringUtil.isEmpty(siteAccrualCount
							.getStudyIdentifier())) {
						validationStrStd
								.append(LC.CTRP_ACCR_NCI_IDEN);
						break;
					}else{
						stdIdentifier = siteAccrualCount.getStudyIdentifier();
					}
					// Not expecting to enter this
					if (siteAccrualCount.getStudySiteIdentifier() == null) {
						validationStrStd
								.append(LC.CTRP_ACCR_SITE_IDEN);
						break;
					}
					if (siteAccrualCount.getStudySiteAccrualCount() == null
							|| StringUtil.stringToInteger(siteAccrualCount
									.getStudySiteAccrualCount()) < 1) {
						validationStrStd
								.append(LC.CTRP_ACCR_SITE_NO_PAT);
						break;
					}
				}
				
				
				if (patientAccrualList.size() < 1) {
					/*validationStrStd
							.append(LC.CTRP_ACCR_NO_PAT);*/
				} else {

					for (int i = 0; i < patientAccrualList.size(); i++) {
						PatientAccrual patientAccrual = patientAccrualList.get(i);
						
						if(patientAccrual.getPatEnrSiteOnStdTeam()==0){
							tempPatEnrSiteMsg.append(",").append(patientAccrual
									.getStudySubjectIdentifier()).append(":").append(patientAccrual.getStudySiteIdentifier());
						}
						
						
					}
				}
				
				dataMap.put("result", "0");
				
				if (validationStrStd.length() > 0 && tempPatEnrSiteMsg.length()==0) {// validation Fails
					dataMap.put("validationStrIndus", validationStrStd.toString());
					dataMap.put("fileName",this.saveErrorList(stdIdentifier,
							validationStrStd.toString(),"",1));
					dataMap.put("result", "1");
				}
				
				if(tempPatEnrSiteMsg.length()>0){
					dataMap.put("indusPatEnrSite", tempPatEnrSiteMsg.toString().substring(1));
					dataMap.put("fileName",this.saveErrorList(stdIdentifier,validationStrStd.toString(),
							tempPatEnrSiteMsg.toString().substring(1),3));
					dataMap.put("result", "1");
				}
			}

		} else {// For Non-Industrial Study

			for (CtrpAccrualExportJB ctrpAccrualExportJB : ctrpAccrualJB.ctrpAccrualExportJB) {

				ArrayList<SubjectCollectionsAccrual> subjectCollectionsAccrualList = ctrpAccrualExportJB
						.getPatientCollectionsList();
				ArrayList<PatientAccrual> patientAccrualList = ctrpAccrualExportJB
						.getPatientList();
				ArrayList<PatientRacesAccrual> patientRacesAccrualList = ctrpAccrualExportJB
						.getPatientRacesList();
				String stdIdentifier =""; 
				
				for (int i = 0; i < subjectCollectionsAccrualList.size(); i++) {
					SubjectCollectionsAccrual subjectCollectionsAccrual = subjectCollectionsAccrualList
							.get(i);
					
					// Not expected to enter this
					if (StringUtil.isEmpty(subjectCollectionsAccrual
							.getStudyIdentifier())) {
						validationStrStd
								.append(LC.CTRP_ACCR_SITE_IDEN);
					}
					
					stdIdentifier = subjectCollectionsAccrual.getStudyIdentifier();
					
				}

				if (patientAccrualList.size() < 1) {
					validationStrStd
							.append(LC.CTRP_ACCR_NO_PAT);
				} else {

					HashSet<String> patStdIdDupes = new HashSet<String>(); 
					for (int i = 0; i < patientAccrualList.size(); i++) {

						StringBuilder tempPatMsg = new StringBuilder();

						PatientAccrual patientAccrual = patientAccrualList.get(i);
						PatientRacesAccrual patientRacesAccrual = patientRacesAccrualList.get(i);
						
						// To check for duplicate Patient Study Id
						if(!patStdIdDupes.add(patientAccrual.getStudySubjectIdentifier())){
							validationStrStd.append(MC.M_CTRP_DUPE_PAT_STD_ID);
							break;
						}
						
						// Patient ID, not expected to enter this
						if (StringUtil.isEmpty(patientAccrual
								.getStudySubjectIdentifier())) {
							tempPatMsg.append(LC.CTRP_ACCR_Patient_Id).append(",");
						}
						
						// Enrolling site name
						if (StringUtil.isEmpty(patientAccrual
								.getStudySiteIdentifier())) {
							tempPatMsg.append(LC.L_Enrolling_Site+" "+LC.L_NCI_POID).append(",");
						}
						
						// Paitent enrollign site
						if(patientAccrual.getPatEnrSiteOnStdTeam()==0){
							tempPatEnrSiteMsg.append(",").append(patientAccrual
									.getStudySubjectIdentifier()).append(":").append(patientAccrual.getStudySiteIdentifier());
						}
						
						// Patient Zip Code, query pending
						/*
						 * if(StringUtil.isEmpty(patientAccrual.getCountryCode())
						 * ){ validationStr = validationStr +
						 * "Patient ID(required field),"; continue; }else{
						 * CodeDao cd =new CodeDao();
						 * if(!cd.getCodeSubtype(StringUtil
						 * .stringToInteger(patientAccrual
						 * .getCountryCode())).equalsIgnoreCase("USA")){
						 * if(StringUtil.isEmpty(patientAccrual.getZipCode())){
						 * if(studiesStr.length()==0){ studiesStr =
						 * subjectCollectionsAccrual.getStudyIdentifier();
						 * }else{ studiesStr = studiesStr + ", "
						 * +subjectCollectionsAccrual.getStudyIdentifier(); }
						 * continue; } } }
						 */

						// Patient birth date
						if (StringUtil.isEmpty(patientAccrual.getBirthDate())) {
							tempPatMsg.append(LC.CTRP_ACCR_Date_OfBirth).append(",");
						}

						// Patient gender
						if (StringUtil.isEmpty(patientAccrual.getGender())) {
							tempPatMsg.append(LC.CTRP_ACCR_Gender).append(",");
						}

						// Patient Ethnicity
						if (StringUtil.isEmpty(patientAccrual.getEthnicity())) {
							tempPatMsg.append(LC.CTRP_ACCR_Ethnicity).append(",");
						}

						// Date of Patient Entry
						if (StringUtil.isEmpty(patientAccrual
								.getSubjectRegistrationDate())) {
							tempPatMsg.append(LC.CTRP_ACCR_Enrolled_Date).append(",");
						}

						// Patient Disease Code
						if (StringUtil.isEmpty(patientAccrual.getSubjectDiseaseCode())) {
							tempPatMsg.append(LC.CTRP_ACCR_Pat_Disease_Code).append(",");
						}
						
						// Patient Race
						if (patientRacesAccrual == null
								|| StringUtil.isEmpty(patientRacesAccrual.getRace())) {
							tempPatMsg.append(LC.CTRP_ACCR_Race).append(",");
						}

						// Removing the last "," delimiter
						if (tempPatMsg.length() > 0) {
							tempPatMsg.deleteCharAt(tempPatMsg
									.lastIndexOf(","));
							validationStrPat.append("~").append(
									patientAccrual.getStudySubjectIdentifier())
									.append(":").append(tempPatMsg);
						}
					}
				}
				
				dataMap.put("result", "0");
				
				if (validationStrPat.length() > 1 && validationStrStd.length() == 0){
					
					dataMap.put("validationStrPat", validationStrPat
							.substring(1));
					dataMap.put("fileName", this.saveErrorList(stdIdentifier,validationStrPat.substring(1),"",2));
					dataMap.put("result", "1");
					
				}
				
				if( validationStrStd.length() > 0){
						dataMap
						.put("validationStrStd", validationStrStd
								.toString());
					dataMap.put("fileName", this.saveErrorList(stdIdentifier,validationStrStd.toString(),"",1));
					dataMap.put("result", "1");
				}
				
				if(tempPatEnrSiteMsg.length()>0){
					dataMap.put("indusPatEnrSite", tempPatEnrSiteMsg.toString().substring(1));
					if(validationStrPat.length()>1){
						dataMap.put("fileName", this.saveErrorList(stdIdentifier,validationStrPat.substring(1),
								tempPatEnrSiteMsg.toString().substring(1),4));
					}
					if(validationStrStd.length()>0){
						dataMap.put("fileName", this.saveErrorList(stdIdentifier,validationStrStd.toString(),
								tempPatEnrSiteMsg.toString().substring(1),5));
					}
					dataMap.put("result", "1");
				}
				
			}
		}
	}
	// To validate data of multiple studies
	public void validateStudiesData(CtrpAccrualJB ctrpAccrualJB,Map<String, String> dataMap,
			Boolean indusFlag) {

		StringBuilder validationStr = new StringBuilder();

		if (indusFlag) {// Validation for Industrial Studies
			for (CtrpAccrualExportJB ctrpAccrualExportJB : ctrpAccrualJB.ctrpAccrualExportJB) {

				
				ArrayList<SiteAccrualCount> siteAccrualCountList = ctrpAccrualExportJB
						.getSiteAccrualCountList();
				ArrayList<PatientAccrual> patientAccrualList = ctrpAccrualExportJB
				.getPatientList();
				
				for (SiteAccrualCount siteAccrualCount : siteAccrualCountList) {
					if (StringUtil.isEmpty(siteAccrualCount
							.getStudyIdentifier())) {
						validationStr.append(siteAccrualCount
								.getStudyIdentifier());
						validationStr.append(",");
						break;
					}
					if (siteAccrualCount.getStudySiteIdentifier() == null) {
						validationStr.append(siteAccrualCount
								.getStudyIdentifier());
						validationStr.append(",");
						break;
					}
					if (siteAccrualCount.getStudySiteAccrualCount() == null
							|| StringUtil.stringToInteger(siteAccrualCount
									.getStudySiteAccrualCount()) < 1) {
						validationStr.append(siteAccrualCount
								.getStudyIdentifier());
						validationStr.append(",");
						break;
					}
				}
				
				for (PatientAccrual patientAccrual : patientAccrualList) {

					if(patientAccrual.getPatEnrSiteOnStdTeam()==0){
						validationStr.append(patientAccrual.getStudyIdentifier());
						validationStr.append(",");
						break;
					}
					
					if(patientAccrual.getStudySiteIdentifier()==null){
						validationStr.append(patientAccrual.getStudyIdentifier());
						validationStr.append(",");
						break;
					}
				}
			}
			dataMap.put("result", "0");
			if (validationStr.length() > 1) {// Validation Fails
				dataMap.put("validationStr", validationStr.substring(0,validationStr.length()-1));
				dataMap.put("result", "1");
			}

		} else {// Validation for Non-Indus Studies

			
			
			for (CtrpAccrualExportJB ctrpAccrualExportJB : ctrpAccrualJB.ctrpAccrualExportJB) {
				
				//validationStr.append(",");
				
				ArrayList<SubjectCollectionsAccrual> subjectCollectionsAccrualList = ctrpAccrualExportJB
						.getPatientCollectionsList();
				ArrayList<PatientAccrual> patientAccrualList = ctrpAccrualExportJB
						.getPatientList();
				ArrayList<PatientRacesAccrual> patientRacesAccrualList = ctrpAccrualExportJB
						.getPatientRacesList();

				StringBuilder tempValidationStr = new StringBuilder();
				HashSet<String> dupePatStdIds = new HashSet<String>();
				
				SubjectCollectionsAccrual subjectCollectionsAccrual = subjectCollectionsAccrualList.get(0);
				if(patientAccrualList.size()<1){
					validationStr.append(subjectCollectionsAccrual
							.getStudyIdentifier());
					
				}else{
				
				for (int i = 0; i < patientAccrualList.size(); i++) {

					PatientAccrual patientAccrual = patientAccrualList.get(i);
					if(patientRacesAccrualList.size()<1){
						tempValidationStr.append(subjectCollectionsAccrual
								.getStudyIdentifier());
						break;
					}
					
					PatientRacesAccrual patientRacesAccrual = patientRacesAccrualList.get(i);
					
					if (patientAccrual == null) {
						tempValidationStr.append(subjectCollectionsAccrual
								.getStudyIdentifier());
						break;
					}

					if (patientRacesAccrual == null) {
						tempValidationStr.append(subjectCollectionsAccrual
								.getStudyIdentifier());
						break;
					}

					// Study ID
					if (StringUtil.isEmpty(subjectCollectionsAccrual
							.getStudyIdentifier())) {
						tempValidationStr.append(subjectCollectionsAccrual
								.getStudyIdentifier());
						break;
					}
					// Patient ID
					if (StringUtil.isEmpty(patientAccrual
							.getStudySubjectIdentifier())) {
						tempValidationStr.append(subjectCollectionsAccrual
								.getStudyIdentifier());
						break;
					}
					
					// Paitent enrolling site
					if(patientAccrual.getPatEnrSiteOnStdTeam()==0){
						tempValidationStr.append(subjectCollectionsAccrual
								.getStudyIdentifier());
						break;
					}
					
					// for checking dupes in Pat id
					if(!dupePatStdIds.add(patientAccrual
							.getStudySubjectIdentifier())){
						tempValidationStr.append(subjectCollectionsAccrual
								.getStudyIdentifier());
						break;						
					}
					// Patient Zip Code , query pending
					/*
					 * if(StringUtil.isEmpty(patientAccrual.getCountryCode())){
					 * if(studiesStr.length()==0){ studiesStr =
					 * subjectCollectionsAccrual.getStudyIdentifier(); }else{
					 * studiesStr = studiesStr + ", "
					 * +subjectCollectionsAccrual.getStudyIdentifier(); } break;
					 * }else{ CodeDao cd =new CodeDao();
					 * if(!cd.getCodeSubtype(StringUtil
					 * .stringToInteger(patientAccrual
					 * .getCountryCode())).equalsIgnoreCase("USA")){
					 * if(StringUtil.isEmpty(patientAccrual.getZipCode())){
					 * if(studiesStr.length()==0){ studiesStr =
					 * subjectCollectionsAccrual.getStudyIdentifier(); }else{
					 * studiesStr = studiesStr + ", "
					 * +subjectCollectionsAccrual.getStudyIdentifier(); } break;
					 * } } }
					 */

					// Patient Birth date
					if (StringUtil.isEmpty(patientAccrual.getBirthDate())) {
						tempValidationStr.append(subjectCollectionsAccrual
								.getStudyIdentifier());
						break;
					}

					// Patient gender
					if (StringUtil.isEmpty(patientAccrual.getGender())) {
						tempValidationStr.append(subjectCollectionsAccrual
								.getStudyIdentifier());
						break;
					}

					// Patient Ethnicity
					if (StringUtil.isEmpty(patientAccrual.getEthnicity())) {
						tempValidationStr.append(subjectCollectionsAccrual
								.getStudyIdentifier());
						break;
					}

					// Date of Patient Entry
					if (StringUtil.isEmpty(patientAccrual
							.getSubjectRegistrationDate())) {
						tempValidationStr.append(subjectCollectionsAccrual
								.getStudyIdentifier());
						break;
					}

					// Patient Disease Code
					if (StringUtil.isEmpty(patientAccrual
							.getSubjectDiseaseCode())) {
						tempValidationStr.append(subjectCollectionsAccrual
								.getStudyIdentifier());
						break;
					}

					// Study Site Identifier
					if (StringUtil.isEmpty(patientAccrual
							.getStudySiteIdentifier())) {
						tempValidationStr.append(subjectCollectionsAccrual
								.getStudyIdentifier());
						break;
					}
					// Patient Race
					if (StringUtil.isEmpty(patientRacesAccrual.getRace())) {
						tempValidationStr.append(subjectCollectionsAccrual
								.getStudyIdentifier());
						break;
					}
				}
				if(tempValidationStr.length()>0){
					if(validationStr.length()>0){
						validationStr.append(",").append(tempValidationStr);
					}else{
						validationStr.append(tempValidationStr);
					}
				}
			}}

			if (validationStr.length() > 0) {// Validation Fails
				dataMap.put("validationStr", validationStr.toString());
				dataMap.put("result", "1");
			} else {// Validation Succeeds
				dataMap.put("result", "0");
			}
		}
	}
	
}