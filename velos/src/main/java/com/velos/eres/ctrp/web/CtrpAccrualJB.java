package com.velos.eres.ctrp.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.ctrp.service.CtrpAccrualAgent;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

public class CtrpAccrualJB {

	public static final String STUDY_STATUS = "studystat";
	public static final String PROTOCOL_STATUS = "protocolStatus";
	private CodeDao currntStudyStatusDao = null;
	private String protocolStatus = null;
	private String accountId = null;
	private String studyIds = null;
	private String userIdS = null;
	private String grpId = null;
	private String groupName = null;
	private String rightSeq = null;
	private String superuserRights = null;
	private String studyId = null;
	private String studyType = null;

	public ArrayList<CtrpAccrualExportJB> ctrpAccrualExportJB;

	public String getProtocolStatus() {
		return protocolStatus;
	}

	public void setProtocolStatus(String protocolStatus) {
		this.protocolStatus = protocolStatus;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getStudyIds() {
		return studyIds;
	}

	public void setStudyIds(String studyIds) {
		this.studyIds = studyIds;
	}

	public String getUserIdS() {
		return userIdS;
	}

	public void setUserIdS(String userIdS) {
		this.userIdS = userIdS;
	}

	public String getGrpId() {
		return grpId;
	}

	public void setGrpId(String grpId) {
		this.grpId = grpId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getRightSeq() {
		return rightSeq;
	}

	public void setRightSeq(String rightSeq) {
		this.rightSeq = rightSeq;
	}

	public String getSuperuserRights() {
		return superuserRights;
	}

	public void setSuperuserRights(String superuserRights) {
		this.superuserRights = superuserRights;
	}

	public void setStudyId(String studyId) {
		this.studyId = studyId;
	}

	public String getStudyId() {
		return studyId;
	}

	public void setStudyType(String studyType) {
		this.studyType = studyType;
	}

	public String getStudyType() {
		return studyType;
	}

	public CtrpAccrualJB() {
		currntStudyStatusDao = new CodeDao();
		currntStudyStatusDao.getCodeValues(STUDY_STATUS);
		setProtocolStatus(currntStudyStatusDao.toPullDown(PROTOCOL_STATUS));
	}

	public Map<String, ArrayList<String>> findStudyAndResearchTypeData(
			String stdNResType) {
		Map<String, ArrayList<String>> stdNResTypeMap = findStudyAndResearchType(stdNResType);
		return stdNResTypeMap;
	}

	/**
	 * @return the ctrpAccrualExportJB
	 */
	public ArrayList<CtrpAccrualExportJB> getCtrpAccrualExportJB() {
		return ctrpAccrualExportJB;
	}

	/**
	 * @param ctrpAccrualExportJB
	 *            the ctrpAccrualExportJB to set
	 */
	public void setCtrpAccrualExportJB(
			ArrayList<CtrpAccrualExportJB> ctrpAccrualExportJB) {
		this.ctrpAccrualExportJB = ctrpAccrualExportJB;
	}

	private Map<String, ArrayList<String>> findStudyAndResearchType(
			String stdNResType) {
		Map<String, ArrayList<String>> stdNResTypeMap = new HashMap<String, ArrayList<String>>();
		ArrayList<String> studylist = new ArrayList<String>();
		ArrayList<String> resTypelist = new ArrayList<String>();
		if (stdNResType.indexOf(",") != -1) {
			String dataArr[] = stdNResType.split(",");
			for (String data : dataArr) {
				String singleData[] = data.split("~");
				studylist.add(singleData[0]);
				resTypelist.add(singleData[1]);
			}
		}
		stdNResTypeMap.put(CtrpAccrualExportJB.STUDY_IDS, studylist);
		stdNResTypeMap.put(CtrpAccrualExportJB.RESEARCH_TYPE, resTypelist);
		return stdNResTypeMap;
	}

	public Map validateStudyAccru() {
		Map<String, String> validation = new HashMap<String, String>();
		try {
			CtrpAccrualAgent ctrpAccrualAgent = EJBUtil
					.getCtrpAccrualAgentBean();
			validation = ctrpAccrualAgent.validateStudyAccru(StringUtil
					.stringToInteger(this.getStudyId()), StringUtil
					.stringToInteger(this.getStudyType()));
		} catch (Exception e) {
			Rlog.fatal("ctrp",
					"Exception in CtrpAccrualJB.validateStudyAccru: " + e);
			validation = null;
		}
		return validation;
	}

	public ArrayList<String> generateStudySubjectAccrual(
			Map<String, ArrayList<String>> stdNResTypeMap) {
		ArrayList<String> generatedDoclist = null;
		CtrpAccrualExportJBHelper ctrpAccrualExportJBHelper = null;
		try {
			ctrpAccrualExportJBHelper = new CtrpAccrualExportJBHelper();
			generatedDoclist = ctrpAccrualExportJBHelper.getStudySubjectData(
					stdNResTypeMap, this);
		} catch (Exception e) {
			Rlog.fatal("CTRP",
					"Exception in CtrpAccrualJB.generateStudySubjectAccrual: "
							+ e);
		}
		return generatedDoclist;
	}

	public void populateStudySubjectAccrual(
			Map<String, ArrayList<String>> stdNResTypeMap) {
		CtrpAccrualExportJBHelper ctrpAccrualExportJBHelper = null;
		try {
			ctrpAccrualExportJBHelper = new CtrpAccrualExportJBHelper();
			ctrpAccrualExportJBHelper.populateStudySubjectData(stdNResTypeMap,
					this);
		} catch (Exception e) {
			Rlog.fatal("CTRP",
					"Exception in CtrpAccrualJB.populateStudySubjectAccrual: "
							+ e);
		}

	}

	public String prepareZipFileFromDoc(ArrayList<String> generatedDoclist,
			Boolean indusFlag) {
		CtrpAccrualExportJBHelper ctrpAccrualExportJBHelper = new CtrpAccrualExportJBHelper();
		return ctrpAccrualExportJBHelper.prepareZipFileFromDoc(
				generatedDoclist, indusFlag);
	}

	public void downloadPatStdAccrual(HttpServletResponse response,
			String zipFileName) {
		CtrpAccrualExportJBHelper ctrpAccrualExportJBHelper = null;
		try {
			ctrpAccrualExportJBHelper = new CtrpAccrualExportJBHelper();
			ctrpAccrualExportJBHelper.downloadPatStdAccrual(response,
					zipFileName);
		} catch (Exception e) {
			Rlog.fatal("CTRP",
					"Exception in CtrpAccrualJB.downloadPatStdAccrual: " + e);
		}

	}
	
	
	public void downloadErrorList(HttpServletResponse response,
			String zipFileName) {
		CtrpAccrualExportJBHelper ctrpAccrualExportJBHelper = null;
		try {
			ctrpAccrualExportJBHelper = new CtrpAccrualExportJBHelper();
			ctrpAccrualExportJBHelper.downloadErrorList(response,
					zipFileName);
		} catch (Exception e) {
			Rlog.fatal("CTRP",
					"Exception in CtrpAccrualJB.downloadErrorList: " + e);
		}

	}
	
	public void deleteErrorList(String zipFileName) {
		CtrpAccrualExportJBHelper ctrpAccrualExportJBHelper = new CtrpAccrualExportJBHelper();
		ctrpAccrualExportJBHelper.deleteCTRPAccrualDoc(zipFileName);

	}

	public void setStudyLastAccrualGenerationDate(String downloadedStudyIds) {
		CtrpAccrualExportJBHelper ctrpAccrualExportJBHelper = new CtrpAccrualExportJBHelper();
		List<String> studyIds = Arrays.asList(downloadedStudyIds.split(","));
		ctrpAccrualExportJBHelper.setStudyLastAccrualGenerationDate(studyIds);
	}
	public void setCutOffDtAndLastDwnLoadBy(String downloadedStudyIds, String cutOffDate,String userId) {
		CtrpAccrualExportJBHelper ctrpAccrualExportJBHelper = new CtrpAccrualExportJBHelper();
		List<String> studyIds = Arrays.asList(downloadedStudyIds.split(","));
		ctrpAccrualExportJBHelper.setCutOffDtAndLastDwnLoadBy(studyIds, cutOffDate, userId);
	}

	public void deleteCTRPAccrualDoc(String zipFileName) {
		CtrpAccrualExportJBHelper ctrpAccrualExportJBHelper = new CtrpAccrualExportJBHelper();
		ctrpAccrualExportJBHelper.deleteCTRPAccrualDoc(zipFileName);

	}

}