package com.velos.eres.ctrp.service;

import java.util.ArrayList;
import java.util.Map;

import javax.ejb.Remote;

import com.velos.eres.business.common.CtrpPatientAccrualDao;
import com.velos.eres.business.studySite.impl.StudySiteBean;

@Remote
public interface CtrpAccrualAgent {
	public Map<String, String> validateStudyAccru(Integer pkStudy,Integer studyType);

	/**
	 * @param dataMapForPatient
	 * @return
	 */
	public ArrayList<CtrpPatientAccrualDao> getPatientAccrualData(Map<String , String> dataMapForPatient);

	/**
	 * @param dataMapForPatient
	 * @return
	 */
	public ArrayList<StudySiteBean> getSiteAccrualCount( Map<String, String> dataMapForPatient);
}
