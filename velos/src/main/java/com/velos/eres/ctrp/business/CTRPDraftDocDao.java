/*
 * Classname : CTRPDraftDocDao
 * 
 * Version information: 1.0
 *
 * Date: 12/20/2011
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Ashu Khatkar
 */

package com.velos.eres.ctrp.business;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.Rlog;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CTRPDraftDocDao extends CommonDAO  implements Serializable
{
  private ArrayList ctrpDocIds;
  private ArrayList ctrpDrafts;
  private ArrayList studyAppendixs;
  private ArrayList ctrpDocTypes;

  public CTRPDraftDocDao()
  {
    this.ctrpDocIds = new ArrayList();
    this.ctrpDrafts = new ArrayList();
    this.studyAppendixs = new ArrayList();
    this.ctrpDocTypes = new ArrayList();
  }

  public ArrayList getCtrpDocids()
  {
    return this.ctrpDocIds;
  }

  public void setCtrpDocIds(ArrayList ctrpDocIds) {
    this.ctrpDocIds = ctrpDocIds;
  }

  public ArrayList getCtrpDrafts() {
    return this.ctrpDrafts;
  }

  public void setCtrpDrafts(ArrayList ctrpDrafts) {
    this.ctrpDrafts = ctrpDrafts;
  }

  public ArrayList getStudyAppendixs() {
    return this.studyAppendixs;
  }

  public void setStudyAppendixs(ArrayList studyAppendixs) {
    this.studyAppendixs = studyAppendixs;
  }
  /**
   * 
   * @return ctrpDocTypes
   */

  public ArrayList getCtrpDocTypes() {
    return this.ctrpDocTypes;
  }
  /**
   * 
   * @param ctrpDocTypes
   */

  public void setCtrpDocTypes(ArrayList ctrpDocTypes) {
    this.ctrpDocTypes = ctrpDocTypes;
  }
  /**
   * 
   * @param ctrpDocId
   */

  public void setCtrpDocId(Integer ctrpDocId) {
    this.ctrpDocIds.add(ctrpDocId);
  }
  /**
   * 
   * @param ctrpDraft
   */
  public void setCtrpDraft(Integer ctrpDraft) {
    this.ctrpDrafts.add(ctrpDraft);
  }
  /**
   * 
   * @param studyAppendix
   */
  public void setStudyAppendix(Integer studyAppendix) {
    this.studyAppendixs.add(studyAppendix);
  }
  /**
   * 
   * @param ctrpDocType
   */
  public void setCtrpDocType(Integer ctrpDocType) {
    this.ctrpDocTypes.add(ctrpDocType);
  }
 
  /**
   * 
   * @param ctrpDraftId
   */
  public void getCtrpDocsValues(int ctrpDraftId)
  {
    PreparedStatement pstmt = null;
    Connection conn = null;
    try {
      conn = getConnection();
      pstmt = conn
        .prepareStatement("Select PK_CTRP_DOC, FK_CTRP_DRAFT, FK_STUDYAPNDX, FK_CODELST_CTRP_DOCTYPE from ER_CTRP_DOCUMENTS where FK_CTRP_DRAFT = ? order by FK_CODELST_CTRP_DOCTYPE");

      pstmt.setInt(1, ctrpDraftId);
      ResultSet rs = pstmt.executeQuery();

      while (rs.next()) {
        Rlog.debug("CTRPDoc", "CTRPDraftDocDao.id " + 
          rs.getInt("PK_CTRP_DOC"));
        setCtrpDocId(new Integer(rs.getInt("PK_CTRP_DOC")));
        setCtrpDraft(new Integer(rs.getInt("FK_CTRP_DRAFT")));
        setStudyAppendix(new Integer(rs.getInt("FK_STUDYAPNDX")));
        setCtrpDocType(new Integer(rs.getInt("FK_CODELST_CTRP_DOCTYPE")));
      }
    }
    catch (SQLException ex) {
      Rlog.fatal("CTRPDocs", 
        "CTRPDraftDocDao.getCTRpDocsValues EXCEPTION IN FETCHING FROM CTRp DOcuments table" + 
        ex);
      try
      {
        if (pstmt != null)
          pstmt.close();
      } catch (Exception localException) {
      }
      try {
        if (conn != null)
          conn.close();
      }
      catch (Exception localException1)
      {
      }
    }
    finally
    {
      try
      {
        if (pstmt != null)
          pstmt.close();
      } catch (Exception localException2) {
      }
      try {
        if (conn != null)
          conn.close();
      }
      catch (Exception localException3)
      {
      }
    }
  }
}