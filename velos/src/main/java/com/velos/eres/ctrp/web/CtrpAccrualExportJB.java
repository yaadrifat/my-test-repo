package com.velos.eres.ctrp.web;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * @author Yogendra Pratap Singh
 */
public class CtrpAccrualExportJB implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static final String STUDY_IDS="studyIds";
	public static final String RESEARCH_TYPE="researchType";
	public static final String BLANK_VAL="";
	public static final String USER_DETAILS="userDeatils";
	public static final String CUT_OFFDATE="cutOffDate";
	
	private ArrayList<SubjectCollectionsAccrual> patientCollectionsList;
	private ArrayList<PatientAccrual> patientList;
	private ArrayList<PatientRacesAccrual> patientRacesList;
	private ArrayList<SiteAccrualCount> siteAccrualCountList;
	
	/**
	 * @return the patientCollectionsList
	 */
	public ArrayList<SubjectCollectionsAccrual> getPatientCollectionsList() {
		return patientCollectionsList;
	}
	/**
	 * @param patientCollectionsList the patientCollectionsList to set
	 */
	public void setPatientCollectionsList(
			ArrayList<SubjectCollectionsAccrual> patientCollectionsList) {
		this.patientCollectionsList = patientCollectionsList;
	}
	/**
	 * @return the patientList
	 */
	public ArrayList<PatientAccrual> getPatientList() {
		return patientList;
	}
	/**
	 * @param patientList the patientList to set
	 */
	public void setPatientList(ArrayList<PatientAccrual> patientList) {
		this.patientList = patientList;
	}
	/**
	 * @return the patientRacesList
	 */
	public ArrayList<PatientRacesAccrual> getPatientRacesList() {
		return patientRacesList;
	}
	/**
	 * @param patientRacesList the patientRacesList to set
	 */
	public void setPatientRacesList(ArrayList<PatientRacesAccrual> patientRacesList) {
		this.patientRacesList = patientRacesList;
	}
	/**
	 * @return the siteAccrualCountList
	 */
	public ArrayList<SiteAccrualCount> getSiteAccrualCountList() {
		return siteAccrualCountList;
	}
	/**
	 * @param siteAccrualCountList the siteAccrualCountList to set
	 */
	public void setSiteAccrualCountList(
			ArrayList<SiteAccrualCount> siteAccrualCountList) {
		this.siteAccrualCountList = siteAccrualCountList;
	}
}
