/*
 * Classname : CTRPDraftDocJB
 * 
 * Version information: 1.0
 *
 * Date: 12/14/2011
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Ashu Khatkar
 */

package com.velos.eres.ctrp.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.aithent.file.uploadDownload.Configuration;
import com.aithent.file.uploadDownload.EJBUtil;
import com.aithent.file.uploadDownload.SaveFile;
import com.velos.eres.business.appendix.impl.AppendixBean;
import com.velos.eres.ctrp.business.CTRPDraftDocBean;
import com.velos.eres.ctrp.business.CTRPDraftDocDao;
import com.velos.eres.ctrp.business.CtrpDraftBean;
import com.velos.eres.ctrp.service.CTRPDraftDocAgent;
import com.velos.eres.ctrp.service.CtrpDraftAgent;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.JNDINames;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

public class CTRPDraftDocJB {

	private Integer id=0;
	private Integer  fkCtrpDraft=0;
	private Integer  fkStudyApndx=0;
	private Integer  fkCtrpDocType=0;
	
    public CTRPDraftDocJB(){
      	
    }
	private static final String USER_ID = "userId";
	private static final String IP_ADD = "ipAdd";
	private static final String ERES="eres";
	private static final String ER_STUDYAPNDX = "ER_STUDYAPNDX";
	private static final String STUDYAPNDX_FILEOBJ = "STUDYAPNDX_FILEOBJ";
	private static final String PK_STUDYAPNDX = "PK_STUDYAPNDX";
	private static final String ZIP = ".zip";
	private static final String SCH = "sch";

   /**
    * 
    * @param id
    * @param fkCtrpDraft
    * @param fkStudyApndx
    * @param fkCtrpDocType
    */
    public CTRPDraftDocJB(Integer id,Integer fkCtrpDraft,Integer fkStudyApndx, Integer fkCtrpDocType){
    	super();
    	setId(id);
    	setFkCtrpDraft(fkCtrpDraft);
    	setFkStudyApndx(fkStudyApndx);
    	setFkCtrpDocType(fkCtrpDocType);
    }
    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	 
	public Integer getFkCtrpDraft() {
		return fkCtrpDraft;
	}
	public void setFkCtrpDraft(Integer fkCtrpDraft) {
		this.fkCtrpDraft = fkCtrpDraft;
	}
	public Integer getFkStudyApndx() {
		return fkStudyApndx;
	}
	public void setFkStudyApndx(Integer fkStudyApndx) {
		this.fkStudyApndx = fkStudyApndx;
	}
	public Integer getFkCtrpDocType() {
		return fkCtrpDocType;
	}
	public void setFkCtrpDocType(Integer fkCtrpDocType) {
		this.fkCtrpDocType = fkCtrpDocType;
	}
	 
		
	private CTRPDraftDocAgent getCtrpDraftDocAgent() throws NamingException {
		InitialContext ic = new InitialContext();
		return (CTRPDraftDocAgent)ic.lookup(JNDINames.CtrpDraftDocAgentHome);
	}
	/**
	 * 
	 * @param argMap
	 * @return output
	 */
	
	public Integer setCtrpDraftDocInfo(HashMap<String, String> argMap) {
		Integer output = 0;
		CTRPDraftDocBean ctrpDraftDocBean = new CTRPDraftDocBean();
		fillCtrpDraftDocBean(ctrpDraftDocBean);
		if (argMap.get(IP_ADD) != null) {
			ctrpDraftDocBean.setIpAdd(argMap.get(IP_ADD));
		}
		try {
			CTRPDraftDocAgent ctrpDraftDocAgent = this.getCtrpDraftDocAgent();
			if (id > 0) {
				fillCtrpDraftBeanDocLastModifiedBy(ctrpDraftDocBean, argMap);
				output = ctrpDraftDocAgent.updateCtrpDraftDocDetails(ctrpDraftDocBean);
			} else {
				fillCtrpDraftDocBeanCreator(ctrpDraftDocBean, argMap);
				
				output = ctrpDraftDocAgent.setCtrpDraftDocDetails(ctrpDraftDocBean);
				id = output; // newly created ID
			}
		} catch(Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftJB.updateCtrpDraftNonIndustrial: "+e);
			output = -1;
		}
		return output;
	}
	/**
	 * 
	 * @param ctrpDraftDocBean
	 */
	private void fillCtrpDraftDocBean(CTRPDraftDocBean ctrpDraftDocBean) {
		if (id > 0) { ctrpDraftDocBean.setPkCtrpDoc(this.id); }
		ctrpDraftDocBean.setFkCtrpDocType(this.fkCtrpDocType);
		ctrpDraftDocBean.setFkCtrpDraft(this.fkCtrpDraft);
		ctrpDraftDocBean.setFkStudyApndx(this.fkStudyApndx);
		
	}
	/**
	 * 
	 * @param ctrpDraftDocBean
	 * @param argMap
	 */
	
	private void fillCtrpDraftBeanDocLastModifiedBy(CTRPDraftDocBean ctrpDraftDocBean, HashMap<String, String> argMap) {
		if (argMap.get(USER_ID) == null) { return; }
		ctrpDraftDocBean.setLastModifiedBy(StringUtil.stringToInteger(argMap.get(USER_ID)));
	}
	/**
	 * 
	 * @param ctrpDraftDocBean
	 * @param argMap
	 */
	
	private void fillCtrpDraftDocBeanCreator(CTRPDraftDocBean ctrpDraftDocBean, HashMap<String, String> argMap) {
		if (argMap.get(USER_ID) == null) { return; }
		ctrpDraftDocBean.setCreator(StringUtil.stringToInteger(argMap.get(USER_ID)));
	}
	
	/**
	 * 
	 * @return int
	 */
	 public int setCtrpDraftDocDetails() {
	        int apndxId = 0;
	        try {
	        	CTRPDraftDocAgent CTRPDraftDocAgentRObj =this.getCtrpDraftDocAgent();
	            apndxId = CTRPDraftDocAgentRObj.setCtrpDraftDocDetails(this.createCtrpDraftDocStateKeeper());
	        }
	        catch (Exception e) {
	            Rlog.debug("CTRPDraftDoc", "in session bean - set CTRPDraftDoc details");
	            return -1;
	        }
	        return apndxId;
	    }
	 
	/**
	 * 
	 * @param fkCTRPDraft
	 * @return CTRPDraftDocDao
	 */
	
	public CTRPDraftDocDao getCTRPDraftDocDetails(int fkCTRPDraft) {
		CTRPDraftDocDao ctrpDraftDocDao = null;
	     try {
	    	 
	    	 CTRPDraftDocAgent CTRPDraftDocAgentRObj =this.getCtrpDraftDocAgent();
	    	 ctrpDraftDocDao=CTRPDraftDocAgentRObj.getCTRPDraftDocDetails(fkCTRPDraft);
	     } catch (Exception e) {
	         Rlog.fatal("CTRPDraftDoc", "Error in Study  getCTRPDraftDocDetails"
	                 + e);
	     }
	      return ctrpDraftDocDao;

	 }
	 
	
	
	/**
	 * 
	 * @return CTRPDraftDocBean
	 */
	public CTRPDraftDocBean createCtrpDraftDocStateKeeper() {
	     Rlog.debug("CTRPDraftDoc", "CTRPDraftDocJB.createCtrpDraftDocStateKeeper ");
	     return new CTRPDraftDocBean(
	     		this.id,
	     		this.fkCtrpDocType,
	     		this.fkCtrpDraft,
	     		this.fkStudyApndx);
	 }
	
	 public int deleteCTRPDraftDocRecord(int CTRPDraftDocAgentRObj)
	 {
	 try {
		 
		 CTRPDraftDocAgent CtrpDraftDocAgentRObj =this.getCtrpDraftDocAgent();
	  	 return CtrpDraftDocAgentRObj.deleteCTRPDraftDocRecord(CTRPDraftDocAgentRObj);
	  	 
	  } catch (Exception e) {
	      Rlog.debug("CTRPDraftDoc",
	              "Exception in deleteCTRPDraftDocRecord "
	                      + e.getMessage());
	      return -2;
	  }
	}
	/**
	 * 
	 * @param appenxBeanList
	 * @return List<String>
	 */
	public List<String> generateDraftTempDoc(List<AppendixBean> appenxBeanList,int draftId) {
		List<String> draftNameList = new ArrayList<String>();
		int studyId =  0 ; 
		try{
			CtrpDraftAgent ctrpDraftAgent = this.getCtrpDraftAgent();
			CtrpDraftBean ctrpDraftBean = ctrpDraftAgent.getCtrpDraftBean(draftId);
			draftId = ctrpDraftBean.getId();
			studyId = ctrpDraftBean.getFkStudy();
			int count=1;
			for(AppendixBean appenxBean:appenxBeanList){
				String db=ERES;
				String tableName = ER_STUDYAPNDX;
				String columnName = STUDYAPNDX_FILEOBJ;
				String pkColumnName = PK_STUDYAPNDX;
				int pkValueInt = appenxBean.getAppendixId();
				String filName = appenxBean.getAppendixUrl_File();
				if (Configuration.FILE_UPLOAD_DOWNLOAD==null) Configuration.readSettings(SCH);
	            Configuration.readUploadDownloadParam(
	                    Configuration.FILE_UPLOAD_DOWNLOAD
	                            + "fileUploadDownload.xml", "study");
				String filePath = EJBUtil.getActualPath(Configuration.DOWNLOADFOLDER, studyId+"_"+draftId+"_"+filName);
				
				File toCheck = new File(filePath);
				boolean status = false;
				if(toCheck.exists()){
					filePath = EJBUtil.getActualPath(Configuration.DOWNLOADFOLDER, studyId+"_"+draftId+"_"+count+"_"+filName);
					status = true;
				}
				if(status){
					filName=studyId+"_"+draftId+"_"+count+"_"+filName;
					count=count+1;
				}else{
					filName=studyId+"_"+draftId+"_"+filName;
				}
				if (SaveFile.readFile(db, tableName, columnName, pkColumnName, filePath, pkValueInt)){
					draftNameList.add(filName);
				}
			}
		}catch(Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftJB.generateDraftTempDoc: "+e);
		}
		return draftNameList;
	}
	/**
	 * @param draftNameList
	 * @return zipFileLocation
	 */
	public String prepareZipFileFromTempDoc(List<String> draftNameList) {
		String zipFileLocation = null;
		String targetFileName = null;
		try {
			Calendar now = Calendar.getInstance();
			long st = now.getTimeInMillis();
			String strDate = DateUtil.getCurrentDate().replaceAll("/", "");
			
			if (Configuration.FILE_UPLOAD_DOWNLOAD==null) Configuration.readSettings(SCH);
            Configuration.readUploadDownloadParam(
                    Configuration.FILE_UPLOAD_DOWNLOAD
                            + "fileUploadDownload.xml", "study");
			zipFileLocation = Configuration.DOWNLOADFOLDER+"/"+LC.L_Trail_Related_ZIP_Name+"_"+strDate+"_"+st+ZIP;
			// create byte buffer
			byte[] buffer = new byte[1024 * 1024 * 8];
			// FileOutputStream Object
			FileOutputStream fout = new FileOutputStream(zipFileLocation);
			// ZipOutputStream Object from FileOutputStream Object
			ZipOutputStream zout = new ZipOutputStream(fout);

			for (String fileName:draftNameList) {
				String filePath = EJBUtil.getActualPath(Configuration.DOWNLOADFOLDER, fileName);
				FileInputStream fin = new FileInputStream(filePath);
				zout.putNextEntry(new ZipEntry(LC.L_Trail_Related_ZIP_Name+"_"+strDate+"_"+st+"/"+ fileName));
				int length = 0;
				while ((length = fin.read(buffer)) > 0) {
					zout.write(buffer, 0, length);
				}
				//close the stream
				zout.closeEntry();
				fin.close();
				deleteCTRPDraftTempDoc(filePath);
			}
			// close the ZipOutputStream
			zout.close();
			fout.close();
			targetFileName = zipFileLocation.substring(zipFileLocation.lastIndexOf("/")+1,zipFileLocation.length());
		} catch (IOException ioe) {
			Rlog.debug("CTRPDraftDoc",
		              "Exception in prepareZipFileFromTempDoc "
		                      + ioe.getMessage());
		}
		return targetFileName;
	}
	/**
	 * @param filePath
	 * delete intermediate temporary documents.
	 */
	public void deleteCTRPDraftTempDoc(String filePath) {
		try{
				File file = new File(filePath);
	    		if(file.exists() && file.delete())
	    			Rlog.info("CTRPDraftDoc", "Temporary file "+file.getName()+" deleted successfully");
    	}catch(Exception e){
    		e.printStackTrace();
    	}
	}
	
	private CtrpDraftAgent getCtrpDraftAgent() throws NamingException {
		InitialContext ic = new InitialContext();
		return (CtrpDraftAgent)ic.lookup(JNDINames.CtrpDraftAgentHome);
	}
}
