package com.velos.eres.barcode;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.commons.lang.StringEscapeUtils;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.impl.datamatrix.DataMatrix;
import org.krysalis.barcode4j.impl.datamatrix.DataMatrixBean;
import org.krysalis.barcode4j.impl.pdf417.PDF417;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;

import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

public class VBarcode {


    private String tempImagePath = null;
    private String tempImageDownloadPath = null;


    public VBarcode() {
        Configuration.readSettings();
        Configuration.readReportParam(Configuration.ERES_HOME + "eresearch.xml");
        tempImagePath = StringUtil.trueValue(Configuration.getTempImagePath());
        tempImageDownloadPath = StringUtil.trueValue(Configuration.getTempImageDownloadPath());
    }


    public String generateBarcode(String codeString)
    {
        return generateBarcode(codeString, "");
    }
    
    public String generateBarcode(String codeString, String date)
    {
        String codePathOnDisk = "";

        try {
            //Create the barcode bean
            Code128Bean bean = new Code128Bean();

            final int dpi = 100;

            //Configure the barcode generator
            bean.setModuleWidth(UnitConv.in2mm(1.0f / dpi)); //makes the narrow bar
            //width exactly one pixel

            bean.doQuietZone(true);
            bean.setBarHeight(7); //7 mm
            bean.setFontSize(3);
             
            String separator = (date == null || date.length() == 0) ? "" : "_";
            String fileName = StringUtil.encodeFileName(codeString)+separator+date+".jpg";
            String fileNameAbsolute = tempImagePath+fileName;

            File outputFile = new File(fileNameAbsolute);
            OutputStream outf = new FileOutputStream(outputFile);

            try {
                //Set up the canvas provider for monochrome JPEG output 
                BitmapCanvasProvider canvas = new BitmapCanvasProvider(
                        outf, "image/jpeg", dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0);

                //Generate the barcode
                bean.generateBarcode(canvas, codeString);

                //Signal end of generation
                canvas.finish();

                codePathOnDisk  = tempImageDownloadPath+fileName;

            } finally {
                outf.close();
            }
        } catch (Exception e) { 
            Rlog.fatal("barcode","VBarcode.generateBarcode EXCEPTION :"+ e);
            e.printStackTrace();
        }

        return codePathOnDisk  ;
    }
    /**
     * Created by : Ankit Kumar
     * Created On : 06/14/2011 
     * Enhancement: INVP3.1
     * Purpose    : Generate 2D-Barcode using DataMatrix.
     * @return    : codePathOnDisk;
     */
    public String generateDataMatrixBarcode(String codeString, String date)
    {
        String codePathOnDisk = "";

        try {
            //Create the instance of DataMatrix.
        	DataMatrix xs = new DataMatrix();
        	
            final int dpi = 200;

            String separator = (date == null || date.length() == 0) ? "" : "_";
            String fileName = codeString+separator+date+".jpg";
            String fileNameAbsolute = tempImagePath+fileName;

            File outputFile = new File(fileNameAbsolute);
            OutputStream outf = new FileOutputStream(outputFile);

            try {
                //Set up the canvas provider for monochrome JPEG output 
                BitmapCanvasProvider canvas = new BitmapCanvasProvider(
                        outf, "image/jpeg", dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0);
                //Generate the barcode
                xs.generateBarcode(canvas, codeString);
                //Signal end of generation
                canvas.finish();

                codePathOnDisk  = tempImageDownloadPath+fileName;

            } finally {
                outf.close();
            }
        } catch (Exception e) {
            Rlog.fatal("barcode","VBarcode.generateDataMatrixBarcode EXCEPTION :"+ e);
            e.printStackTrace();
        }
        return codePathOnDisk  ;
    }
    
    /**
     * Created by : Ankit Kumar
     * Created On : 06/14/2011 
     * Enhancement: INVP3.1
     * Purpose    : Generate 2D-Barcode using PDF417.
     * @return    : codePathOnDisk;
     */
    public String generatePDF417Barcode(String codeString, String date)
    {
        String codePathOnDisk = "";

        try {
            //Create the barcode bean
        	
        	PDF417 xs = new PDF417();
            final int dpi = 200;

            //Configure the barcode generator
            String separator = (date == null || date.length() == 0) ? "" : "_";
            String fileName = codeString+separator+date+".jpg";
            String fileNameAbsolute = tempImagePath+fileName;

            File outputFile = new File(fileNameAbsolute);
            OutputStream outf = new FileOutputStream(outputFile);
            
            try {
                //Set up the canvas provider for monochrome JPEG output 
                BitmapCanvasProvider canvas = new BitmapCanvasProvider(
                        outf, "image/jpeg", dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0);

                //Generate the barcode
                xs.generateBarcode(canvas, codeString);

                //Signal end of generation
                canvas.finish();

                codePathOnDisk  = tempImageDownloadPath+fileName;

            } finally {
                outf.close();
            }
        } catch (Exception e) {
            Rlog.fatal("barcode","VBarcode.generatePDF417Barcode EXCEPTION :"+ e);
            e.printStackTrace();
        }

        return codePathOnDisk  ;
    }
   
} // end of class
