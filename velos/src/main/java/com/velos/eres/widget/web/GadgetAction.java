package com.velos.eres.widget.web;

import com.opensymphony.xwork2.ActionSupport;

public class GadgetAction extends ActionSupport  {
	private static final long serialVersionUID = 4675375635366808793L;
	private static final String GADGET_HOME_TITLE = "gadgetHomeTile";
	public String getGadgetHome() {
		return GADGET_HOME_TITLE;
	}
}
