package com.velos.eres.widget.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

/**
 * @author Isaac Huang
 * Class to hold all gadget DAO methods and to cache all the UI Gadget data into memory.
 *
 */
public class UIGadgetDao extends CommonDAO implements java.io.Serializable {
	private static final long serialVersionUID = 7264047530840182647L;
	public UIGadgetDao() {}
    private static final String getAllGadgetDataSql = " select * from UI_GADGET order by GADGET_SEQUENCE ";
	public JSONArray retrieveGadgetRegistry() {
        PreparedStatement pstmt = null;
        Connection conn = null;
        JSONArray newJArray = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(getAllGadgetDataSql);
            newJArray = formGadgetRegistryJson(pstmt.executeQuery());
        } catch(Exception ex){
            Rlog.fatal("UIGadgetDao",
                    "UIGadgetDao.reloadGadgetRegistry EXCEPTION: " + ex);
        } finally {
            try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
            try { if (conn != null) { conn.close(); } } catch (Exception e) {}
        }
        if (newJArray == null) { newJArray = new JSONArray(); }
        return newJArray;
	}
	private static final String GADGET_ID = "GADGET_ID";
	private static final String GADGET_TITLE = "GADGET_TITLE";
	private static final String GADGET_URL = "GADGET_URL";
	private static final String GADGET_VISIBLE = "GADGET_VISIBLE";
	public static final String ID_STR = "id";
	public static final String TITLE_STR = "title";
	public static final String URL_STR = "url";
	public static final String VISIBLE_STR = "visible";
	private JSONArray formGadgetRegistryJson(ResultSet rs) throws Exception {
		JSONArray jArray = new JSONArray();
		if (rs == null) { return jArray; }
		while(rs.next()) {
			JSONObject oneRec = new JSONObject();
			String myKey = rs.getString(GADGET_ID);
			String myTitle = rs.getString(GADGET_TITLE);
			String myURL = rs.getString(GADGET_URL);
			String myVisible = String.valueOf(rs.getInt(GADGET_VISIBLE));
			oneRec.put(ID_STR, myKey);
			oneRec.put(TITLE_STR, myTitle);
			oneRec.put(URL_STR, myURL);
			oneRec.put(VISIBLE_STR, myVisible);
			jArray.put(oneRec);
		}
		return jArray;
	}
    
    private static final String updateGadgetInstanceCookieByUserIdSql = "update UI_GADGET_INSTANCE set COOKIE = ? where " +
    		" FK_USER = ? ";
    private static final String insertGadgetInstanceCookieByUserIdSql = "insert into UI_GADGET_INSTANCE (PK_GADGET_INSTANCE, " +
    		" FK_USER, COOKIE) values (SEQ_UI_GADGET_INSTANCE.nextval, ?, ?) ";
    private static final String getGadgetInstanceCookieByUserIdSql = "select COOKIE from UI_GADGET_INSTANCE where " +
    		" FK_USER = ? ";
    public static final String GADGET_COOKIE = "GADGET_COOKIE";
    public String getGadgetInstanceCookie(String userId) {
    	String cookie = null;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(getGadgetInstanceCookieByUserIdSql);
            pstmt.setInt(1, StringUtil.stringToNum(userId));
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()) {
            	cookie = rs.getString(1);
            }
        } catch(Exception ex){
            Rlog.fatal("UIGadgetDao",
                    "UIGadgetDao.getGadgetInstanceCookie EXCEPTION: " + ex);
        } finally {
            try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
            try { if (conn != null) { conn.close(); } } catch (Exception e) {}
        }
    	return cookie;
    }
    public void setGadgetInstanceCookie(String userId, HashMap<String, String> hash) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int rowsUpdated = 0;
        
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(updateGadgetInstanceCookieByUserIdSql);
            pstmt.setString(1, hash.get(GADGET_COOKIE));
            pstmt.setInt(2, StringUtil.stringToNum(userId));
            rowsUpdated = pstmt.executeUpdate();
        } catch(Exception ex){
            Rlog.fatal("UIGadgetDao",
                    "UIGadgetDao.setGadgetInstanceCookie excp1: " + ex);
        } finally {
            try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
            try { if (conn != null) { conn.close(); } } catch (Exception e) {}
        }
        
        if (rowsUpdated > 0) {  return; }
        
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(insertGadgetInstanceCookieByUserIdSql);
            pstmt.setInt(1, StringUtil.stringToNum(userId));
            pstmt.setString(2, (String)hash.get(GADGET_COOKIE));
            rowsUpdated = pstmt.executeUpdate();
        } catch(Exception ex){
            Rlog.fatal("UIGadgetDao",
                    "UIGadgetDao.setGadgetInstanceCookie excp2: " + ex);
        } finally {
            try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
            try { if (conn != null) { conn.close(); } } catch (Exception e) {}
        }
    }
    public static final String GADGET_SETTINGS = "GADGET_SETTINGS";
    public void setGadgetInstanceSettings(String userId, HashMap<String, String> hash) throws Exception {
    	String newSettings = hash.get(GADGET_SETTINGS);
    	if (StringUtil.isEmpty(newSettings)) { return; } // Nothing to save
    	// Get existing JSON
    	String settings = getGadgetInstanceSettings(userId);
    	if (settings == null) { throw new Exception("Gadget record does not exist; cannot save settings"); }
    	if (settings.length() < 1) {
    		// Existing JSON record is blank; add new settings and be done
    		addNewGadgetSettings(userId, newSettings);
    		return;
    	}
    	
		JSONObject jObj = null;
		try {
    		jObj = (JSONObject) new JSONTokener(settings).nextValue();
		} catch(Exception e) {
            Rlog.fatal("UIGadgetDao",
                    "UIGadgetDao.setGadgetInstanceSettings could not parse existing JSON: " + e);
            throw e;
		}
    	boolean hasThisGadget = false;
    	String[] keys = JSONObject.getNames(jObj);
    	if (keys == null || keys.length < 1) {
    		// Existing JSON record is not properly formed; add new settings and be done
    		addNewGadgetSettings(userId, newSettings);
    		return;
    	}
    	JSONObject newJObj = null;
    	try{
    		newJObj = (JSONObject) new JSONTokener(newSettings).nextValue();
    	} catch(Exception e) {
            Rlog.fatal("UIGadgetDao",
                    "UIGadgetDao.setGadgetInstanceSettings could not parse new JSON: " + e);
            throw e;
    	}
    	String gadgetId = (String)newJObj.get("id");
    	newJObj.remove("id"); // this is redundant
    	for(int iX=0; iX<keys.length; iX++) {
        	if (!keys[iX].equals(gadgetId)) { continue; }
        	// Merge new JSON with existing JSON
        	JSONObject mergedJObj = mergeTwoJsons((JSONObject)jObj.get(gadgetId), newJObj);
        	jObj.put(gadgetId, mergedJObj);
        	hasThisGadget = true;
        	break;
    	}
    	if (!hasThisGadget) {
    		// Existing JSON record does not have this gadget; just add this gadget w/o merging
    		jObj.put(gadgetId, newJObj);
    	}
    	// Persist combined JSON in DB
    	updateNewGadgetSettings(userId, jObj.toString());
    }
    
    private JSONObject mergeTwoJsons(JSONObject oldJObj, JSONObject newJObj) throws Exception{
    	if (newJObj == null) { return oldJObj; } // Nothing to merge
    	try {
        	String[] keys = JSONObject.getNames(newJObj);
        	if (keys == null) { return oldJObj; }  // Nothing to merge
        	for(int iX=0; iX<keys.length; iX++) {
        		oldJObj.put(keys[iX], newJObj.get(keys[iX]));
        	}
    	} catch(Exception e) {
            Rlog.fatal("UIGadgetDao",
                    "UIGadgetDao.mergeTwoJsons could not merge: " + e);
            throw e;
    	}
    	return oldJObj;
    }
    
    private static final String updateGadgetInstanceSettingsByUserIdSql = "update UI_GADGET_INSTANCE set SETTINGS = ? where " +
    		" FK_USER = ? ";
    
    // Adds new gadget settings by user ID. Use this when the existing settings record is completely blank.
    private int addNewGadgetSettings(String userId, String newSettings) throws Exception {
		// First make sure the newSettings is parse-able.
		JSONObject newJObj = null;
		try {
			newJObj = (JSONObject) new JSONTokener(newSettings).nextValue();
		} catch(Exception e) {
            Rlog.fatal("UIGadgetDao",
                    "UIGadgetDao.addNewGadgetSettings could not parse new JSON: " + e);
            throw e;
		}
		// Create the parent JSON. Eventually the newParentJObj will be saved in DB.
		JSONObject newParentJObj = new JSONObject();
		String jobjId = (String)newJObj.get("id");
		newJObj.remove("id"); // this is redundant; so remove it
		newParentJObj.put(jobjId, newJObj);
		
		// Now save it in DB
        PreparedStatement pstmt = null;
        Connection conn = null;
        int rowsUpdated = 0;
        
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(updateGadgetInstanceSettingsByUserIdSql);
            pstmt.setString(1, newParentJObj.toString());
            pstmt.setInt(2, StringUtil.stringToNum(userId));
            rowsUpdated = pstmt.executeUpdate();
        } catch(Exception ex){
            Rlog.fatal("UIGadgetDao",
                    "UIGadgetDao.addNewGadgetSettings: " + ex);
        } finally {
            try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
            try { if (conn != null) { conn.close(); } } catch (Exception e) {}
        }
        return rowsUpdated; 
    }
    
    // Update gadget settings by user ID. Use this to persist the new settings the way they are
    private int updateNewGadgetSettings(String userId, String newSettings) throws Exception {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int rowsUpdated = 0;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(updateGadgetInstanceSettingsByUserIdSql);
            pstmt.setString(1, newSettings);
            pstmt.setInt(2, StringUtil.stringToNum(userId));
            rowsUpdated = pstmt.executeUpdate();
        } catch(Exception ex){
            Rlog.fatal("UIGadgetDao",
                    "UIGadgetDao.addNewGadgetSettings: " + ex);
        } finally {
            try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
            try { if (conn != null) { conn.close(); } } catch (Exception e) {}
        }
        return rowsUpdated; 
    }
    
    private static String EMPTY_STRING = "";
    private static String selectGadgetInstanceSettingsByUserIdSql = " select SETTINGS from UI_GADGET_INSTANCE where "+
    		" FK_USER = ? ";
    /**
     * Gets the gadget settings by the user ID 
     * @param userId
     * @return Settings in JSON; returns "" if record has blank settings; returns null if record does not exist.
     */
    public String getGadgetInstanceSettings(String userId) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        String settings = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(selectGadgetInstanceSettingsByUserIdSql);
            pstmt.setInt(1, StringUtil.stringToNum(userId));
            ResultSet rs = pstmt.executeQuery();
            int num = 0;
            while(rs.next()) {
            	settings = rs.getString(1);
            	num++;
            }
            if (num == 0) { settings = null; }
            else { if (StringUtil.isEmpty(settings)) { settings = EMPTY_STRING; } }
        } catch(Exception ex){
            Rlog.fatal("UIGadgetDao",
                    "UIGadgetDao.getGadgetInstanceSettings EXCEPTION: " + ex);
        } finally {
            try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
            try { if (conn != null) { conn.close(); } } catch (Exception e) {}
        }
    	return settings;
    }

    public LinkedHashMap<String, Object> getGadgetInstanceSettingsHash(String userId, String gadgetId){
    	LinkedHashMap<String, Object> myGdtInstSettingsData = new LinkedHashMap<String, Object>();
    	
    	try{
	    	UIGadgetDao uiGadgetDao = new UIGadgetDao();
	    	String settings = uiGadgetDao.getGadgetInstanceSettings(userId);
	    	
	    	if (settings != null && settings.length() > 0) {
				JSONObject parentJObj = null;
				parentJObj = (JSONObject) new JSONTokener(settings).nextValue();
				boolean hasSettingsForThisGadgetId = false;
				try {
					if (parentJObj.get(gadgetId) != null) {
						hasSettingsForThisGadgetId = true;
					}
				} catch(Exception e) {}
				if (hasSettingsForThisGadgetId) {
					JSONObject jobj = (JSONObject)parentJObj.get(gadgetId);
					Iterator jsonKeys = jobj.keys();

					while (jsonKeys.hasNext()){
						String jsonKey = (String)jsonKeys.next();
						String jsonData = jobj.getString(jsonKey);

						myGdtInstSettingsData.put(jsonKey, jsonData);
					}
				}
	    	}
	    } catch(Exception e) {
    		//e.printStackTrace();
    	    System.out.println("UIGadgetDao.getGadgetInstanceSettingsHash exception: " + e);
    	    myGdtInstSettingsData = null;
    	}
    	return myGdtInstSettingsData;
    }
}

