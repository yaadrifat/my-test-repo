package com.velos.eres.widget.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.widget.business.domain.Widget;

/**
 * @author Isaac Huang
 * Class to cache all the UI Widget data into memory.
 *
 */
public class UIWidgetDao extends CommonDAO implements java.io.Serializable {
	private static final long serialVersionUID = 7264047530840182647L;
	private Map<Long, List<Widget>> widgetMap = null;
	public UIWidgetDao() {
		widgetMap = new HashMap<Long, List<Widget>>();
	}
	public Map<Long, List<Widget>> getWidgetMap() { return widgetMap; }
	public void clearWidgetData() {
		widgetMap.clear();
	}
    public void getWidgetData() {
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr =
        	"select p.page_code, wg.* from ui_page p left join ui_widget_container cn " + 
            "    on p.pk_page = cn.fk_page_id " +
            "  left join UI_CONTAINER_WIDGET_MAP wd " + 
            "    on cn.pk_container = wd.fk_container_id " + 
            "  left join ui_widget wg " + 
            "    on wd.fk_widget_id = wg.pk_widget " +
            "  order by page_code, pk_widget ";
        
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(sqlStr);
            ResultSet rs = pstmt.executeQuery();
            Long pageCode = -999999L;
            ArrayList<Widget> widgetListPerPageCode = null;
            while (rs.next()) {
            	if (rs.getLong("PAGE_CODE") != pageCode) {
            		if (widgetListPerPageCode != null) {
            			widgetMap.put(pageCode, widgetListPerPageCode);
            		}
        			widgetListPerPageCode = new ArrayList<Widget>();
        			widgetListPerPageCode.add(createWidgetBean(rs));
        			pageCode = rs.getLong("PAGE_CODE");
            	} else {
        			widgetListPerPageCode.add(createWidgetBean(rs));
            	}
            }
            if (widgetListPerPageCode != null) {
    			widgetMap.put(pageCode, widgetListPerPageCode);
    		}
        } catch(SQLException ex){
            Rlog.fatal("UIWidgetDao",
                    "UIWidgetDao.getWidgetData EXCEPTION: " + ex);
        } finally {
            try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
            try { if (conn != null) { conn.close(); } } catch (Exception e) {}
        }
    }
    
    private Widget createWidgetBean(ResultSet rs) throws SQLException {
		Widget widget = new Widget();
		widget.setId(rs.getLong("PK_WIDGET"));
		widget.setState(rs.getString("STATE"));
		widget.setWidgetName(rs.getString("NAME"));
		widget.setWidgetDivId(rs.getString("WIDGET_DIV_ID"));
		widget.setHelpUrl(rs.getString("HELP_URL"));
		widget.setDescription(rs.getString("DESCRIPTION"));
		widget.setIsAuthenticated("1".equals(rs.getString("IS_AUTHENTICATED")));
		widget.setIsMinimizable("1".equals(rs.getString("IS_MINIZABLE")));
		widget.setIsClosable("1".equals(rs.getString("IS_CLOSABLE")));
		widget.setIsResizable("1".equals(rs.getString("IS_RESIZEABLE")));
		widget.setIsDragable("1".equals(rs.getString("IS_DRAGABLE")));
		widget.setIsMouseHoverEnabled("1".equals(rs.getString("IS_MOUSE_HOVER_ENABLED")));
		widget.setCeatedDate(rs.getDate("CREATED_ON"));
		widget.setLastUpdate(rs.getDate("LAST_MODIFIED_DATE"));
		widget.setVersionNo(rs.getLong("VERSION_NO"));
		widget.setResizeMinHeight(rs.getLong("RESIZE_MIN_HEIGHT"));
		widget.setResizeMinWidth(rs.getLong("RESIZE_MIN_WIDTH"));
		widget.setResizeMaxHeight(rs.getLong("RESIZE_MAX_HEIGHT"));
		widget.setResizeMaxWidth(rs.getLong("RESIZE_MAX_WIDTH"));
    	return widget;
    }
}

