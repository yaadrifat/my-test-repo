package com.velos.eres.widget.web;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.grpRights.GrpRightsJB;

public class GadgetSample2Action extends ActionSupport {
	private static final long serialVersionUID = -3017353659165410425L;
	private HttpServletRequest request = null;
	private HttpSession tSession = null;
	private GadgetSample2JB gadgetSample2JB = null;
	private Map<String, List<String>> inputErrors = null;
	private Collection<String> operationErrors = null;
	private LinkedHashMap<String, Object> jsonData = null;
	
	private static final String ESIGN_STR = "eSign";
	private static final String USER_ID = "userId";
	private static final String ACCOUNT_ID = "accountId";
	private static final String IP_ADD = "ipAdd";
	private static final String EMPTY_STR = "";
	private static final String GRIGHTS_STR = "GRights";
	private static final String MACCLINKS = "MACCLINKS";
	
	public GadgetSample2Action() {
		ActionContext ac = ActionContext.getContext();
		request = (HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		tSession = request.getSession(false);
		gadgetSample2JB = new GadgetSample2JB();
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute(GRIGHTS_STR);
		gadgetSample2JB.setAcctLinksAccessRights(
				Integer.parseInt(grpRights.getFtrRightsByValue(MACCLINKS)));
	}
	
	public Map<String, List<String>> getInputErrors() { return this.inputErrors; }
	
	public Collection<String> getOperationErrors() { return this.operationErrors; }
	
	public LinkedHashMap<String, Object> getJsonData() { return jsonData; }

	public String getGadgetSample2() {
		gadgetSample2JB.loadStudyDD(this.getUserIdInSession());
		return SUCCESS;
	}
	
	public String getUserIdInSession() {
		String userId = null;
		try {
			userId = (String) request.getSession(false).getAttribute(USER_ID);
		} finally {
			userId = StringUtil.trueValue(userId);
		}
		return userId;
	}
	
	private void validateESign(String eSign, String target) {
		if (StringUtil.isEmpty(eSign) ||
				!eSign.equals(request.getSession(false).getAttribute(ESIGN_STR))) {
			addFieldError(target, MC.M_IncorrEsign_EtrAgain);
		}
	}
	
	public String getMyLinksData() {
		jsonData = gadgetSample2JB.getMyLinksData(this.getUserIdInSession());
		return SUCCESS;
	}
	
	public String getAcctLinksData() {
		jsonData = gadgetSample2JB.getAcctLinksData(
				(String)tSession.getAttribute(ACCOUNT_ID), GadgetSample2JB.LNK_GEN);
		return SUCCESS;
	}
	
	public GadgetSample2JB getGadgetSample2JB() { return gadgetSample2JB; }
	
	public void setGadgetSample2JB(GadgetSample2JB gadgetSample2JB) {
		this.gadgetSample2JB = gadgetSample2JB;
	}
	
	public String deleteMyLink() {
		String userId = this.getUserIdInSession();
		if (StringUtil.isEmpty(userId)) { return null; }
		
		// If eSign is wrong, don't process the rest of the request
		validateESign(gadgetSample2JB.getMyLinksDeleteESign(), GadgetSample2JB.FLD_MY_LINKS_DELETE_ESIGN);
		if (hasFieldErrors()) {
			this.inputErrors = getFieldErrors();
			return INPUT;
		}
		
		// Do field-by-field validation in the JB
		gadgetSample2JB.validateMyLinksForDelete(this);
		if (hasFieldErrors()) {
			this.inputErrors = getFieldErrors();
			return INPUT;
		}
		
		// Do the actual delete
		int resultFromEJB = gadgetSample2JB.deleteUlinkForMyLinks(
				AuditUtils.createArgs(request.getSession(false), EMPTY_STR, LC.L_Manage_Acc));
		if (resultFromEJB < 0 || hasActionErrors()) {
			addActionError(MC.M_Err_SavingData);
			this.operationErrors = getActionErrors();
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	public String saveMyLink() {
		String userId = this.getUserIdInSession();
		if (StringUtil.isEmpty(userId)) { return null; }
		
		// If eSign is wrong, don't process the rest of the request
		validateESign(gadgetSample2JB.getMyLinksESign(), GadgetSample2JB.FLD_MY_LINKS_ESIGN);
		if (hasFieldErrors()) {
			this.inputErrors = getFieldErrors();
			return INPUT;
		}
		
		// Do field-by-field validation in the JB
		gadgetSample2JB.validateMyLinksForSave(this);
		if (hasFieldErrors()) {
			this.inputErrors = getFieldErrors();
			return INPUT;
		}
		
		// Do the actual save
		gadgetSample2JB.setAccountId((String)tSession.getAttribute(ACCOUNT_ID));
		gadgetSample2JB.setIpAdd((String)tSession.getAttribute(IP_ADD));
		gadgetSample2JB.setUserId(getUserIdInSession());
		int resultFromEJB = gadgetSample2JB.saveUlinkForMyLinks();
		if (resultFromEJB < 0 || hasActionErrors()) {
			addActionError(MC.M_Err_SavingData);
			this.operationErrors = getActionErrors();
			return ERROR;
		}

		return SUCCESS;
	}
	
	public String deleteAcctLink() {
		String userId = this.getUserIdInSession();
		if (StringUtil.isEmpty(userId)) { return null; }
		
		// If eSign is wrong, don't process the rest of the request
		validateESign(gadgetSample2JB.getAcctLinksDeleteESign(), GadgetSample2JB.FLD_ACCT_LINKS_DELETE_ESIGN);
		if (hasFieldErrors()) {
			this.inputErrors = getFieldErrors();
			return INPUT;
		}
		
		// Do field-by-field validation in the JB
		gadgetSample2JB.validateAcctLinksForDelete(this);
		if (hasFieldErrors()) {
			this.inputErrors = getFieldErrors();
			return INPUT;
		}
		
		// Do the actual delete
		int resultFromEJB = gadgetSample2JB.deleteUlinkForAcctLinks(this,
				AuditUtils.createArgs(request.getSession(false), EMPTY_STR, LC.L_Manage_Acc));
		if (resultFromEJB < 0 || hasActionErrors()) {
			addActionError(MC.M_Err_SavingData);
			this.operationErrors = getActionErrors();
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	public String saveAcctLink() {
		String userId = this.getUserIdInSession();
		if (StringUtil.isEmpty(userId)) { return null; }
		
		// If eSign is wrong, don't process the rest of the request
		validateESign(gadgetSample2JB.getAcctLinksESign(), GadgetSample2JB.FLD_ACCT_LINKS_ESIGN);
		if (hasFieldErrors()) {
			this.inputErrors = getFieldErrors();
			return INPUT;
		}
		
		// Do field-by-field validation in the JB
		gadgetSample2JB.validateAcctLinksForSave(this);
		if (hasFieldErrors()) {
			this.inputErrors = getFieldErrors();
			return INPUT;
		}
		
		// Do the actual save
		gadgetSample2JB.setAccountId((String)tSession.getAttribute(ACCOUNT_ID));
		gadgetSample2JB.setIpAdd((String)tSession.getAttribute(IP_ADD));
		gadgetSample2JB.setUserId(getUserIdInSession());
		int resultFromEJB = gadgetSample2JB.saveUlinkForAcctLinks(this);
		if (resultFromEJB < 0 || hasActionErrors()) {
			addActionError(MC.M_Err_SavingData);
			this.operationErrors = getActionErrors();
			return ERROR;
		}

		return SUCCESS;
	}

}
