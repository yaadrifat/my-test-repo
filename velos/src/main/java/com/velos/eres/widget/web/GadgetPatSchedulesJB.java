package com.velos.eres.widget.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import org.json.JSONArray;
import org.json.JSONObject;

import com.opensymphony.xwork2.ActionContext;
import com.velos.eres.ctrp.web.RadioOption;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.widget.business.common.UIGadgetDao;
import com.velos.services.patientschedule.PatientScheduleDAO;

public class GadgetPatSchedulesJB {	
	private static final String GADGET_PATSCHEDULES = "gadgetPatSchedules";
	public static final String GADGET_PATSCHEDULESJB_DOT = "gadgetPatSchedulesJB.";

	private static final String STUDYIDS = "studyIds";
	public static final String VISITFILTER_KEY = "visitFilter";
	
    private static final String SETTINGSVISITFILTERMENU_KEY = "settingsVisitFilterMenu";
    public static final String VISITFILTERMENU_KEY = "visitFilterMenu";

    public static final String PATIENTID_KEY = "patientId";
    public static final String PATSTUDYID_KEY = "patientStudyId";
    public static final String STUDYNUMBER_KEY = "studyNumber";
    
    public static final String SORTBY_KEY = "sortBy";    
    public static final String SORTDIR_KEY = "sortDir";
    private static final String SETTINGSSORTDIRMENU_KEY = "settingsSortDirMenu";

	private LinkedHashMap<String, Object> settingsHash = null;
	private String settingsStudyIds = null;
	private int settingsStudyCount = 0;
	private String settingsVisitFilter = null;
	private String settingsVisitFilterMenu = null;
	private String settingsSortBy = null;
	private List<RadioOption> settingsSortByList = null;
	private String settingsSortDir = null;
	private String settingsSortDirMenu = null;

    private String visitFilter = null;
    private String visitFilterMenu = null;
    private String showMoreLink = null;
    
    JSONArray mySchedulesColumns  = null;
    JSONArray mySchedulesData  = null;
	
    private void loadGadgetSettings(String userId){
    	UIGadgetDao uiGadgetDao = new UIGadgetDao();
    	settingsHash = uiGadgetDao.getGadgetInstanceSettingsHash(userId, GADGET_PATSCHEDULES);
    	
    	//settingsStudyIds = (String)settingsHash.get(STUDYIDS);
    	//settingsStudyIds=settingsStudyIds.substring(settingsStudyIds.indexOf("[")+1, settingsStudyIds.indexOf("]"));
    	
    	//settingsStudyCount = (StringUtil.chopChop(settingsStudyIds,',')).length;
    	
    	settingsVisitFilter = (String)settingsHash.get(VISITFILTER_KEY);
    	settingsVisitFilter = (StringUtil.isEmpty(settingsVisitFilter))? "T" : settingsVisitFilter;
    	
    	settingsVisitFilterMenu = EJBUtil.getRelativeTimeDDSchedule(GADGET_PATSCHEDULESJB_DOT+SETTINGSVISITFILTERMENU_KEY, settingsVisitFilter); 
		visitFilterMenu = EJBUtil.getRelativeTimeDDSchedule(GADGET_PATSCHEDULESJB_DOT+VISITFILTERMENU_KEY, settingsVisitFilter);
		
		settingsSortBy = (String)settingsHash.get(SORTBY_KEY);
		if (StringUtil.isEmpty(settingsSortBy)) 
			settingsSortBy = PATSTUDYID_KEY;
		if (PATSTUDYID_KEY.equals(settingsSortBy)){
			settingsSortBy = "1";
		} else if (STUDYNUMBER_KEY.equals(settingsSortBy)){
			settingsSortBy = "2";
		}

		settingsSortByList = new ArrayList<RadioOption>();
		settingsSortByList.add(new RadioOption(LC.L_Patient_StudyId + "<BR>", 1));
		settingsSortByList.add(new RadioOption(LC.L_Study_Number + "<BR>", 2));
		
		
		settingsSortDir = (String)settingsHash.get(SORTDIR_KEY);
		if (StringUtil.isEmpty(settingsSortDir)) { settingsSortDir = "asc"; }
		settingsSortDirMenu = createSortDirDD(settingsSortDir);
    }

	public GadgetPatSchedulesJB(String userId) {
		this.loadGadgetSettings(userId);
	}
	
	public void setSettingsHash(LinkedHashMap<String, Object>  settingsHash) {
		this.settingsHash = settingsHash; 
	}

	public LinkedHashMap<String, Object> getSettingsHash() {
		return settingsHash;
	}

	public void setSettingsStudyIds(String settingsStudyIds) {
		this.settingsStudyIds = settingsStudyIds;
	}

	public int getSettingsStudyCount() {
		return settingsStudyCount;
	}
	
	public void setSettingsStudyCount(int settingsStudyCount) {
		this.settingsStudyCount = settingsStudyCount;
	}

	public String getSettingsStudyIds() {
		return settingsStudyIds;
	}
	
	public void setSettingsVisitFilter(String settingsVisitFilter) {
		this.settingsVisitFilter = settingsVisitFilter;
		this.settingsVisitFilterMenu = EJBUtil.getRelativeTimeDDSchedule(
				GADGET_PATSCHEDULESJB_DOT+SETTINGSVISITFILTERMENU_KEY, settingsVisitFilter); 
	}

	public String getSettingsVisitFilter() {
		return settingsVisitFilter;
	}
	
	public void setSettingsVisitFilterMenu(String settingsVisitFilterMenu) {
		this.settingsVisitFilterMenu = settingsVisitFilterMenu;
	}

	public String getSettingsVisitFilterMenu() {
		return settingsVisitFilterMenu;
	}

	public void setSettingsSortBy(String settingsSortBy) {
		if (settingsSortBy == null) { settingsSortBy = "1"; } // 1 = Patient Study Id
		this.settingsSortBy = settingsSortBy;
	}

	public String getSettingsSortBy() {
		return settingsSortBy;
	}

	public void setSettingsSortByList(List<RadioOption> settingsSortByList) {
		this.settingsSortByList = settingsSortByList;
	}
	
	public List<RadioOption> getSettingsSortByList() {
		return settingsSortByList;
	}

	private String createSortDirDD(String settingsSortDir){
		ArrayList desc = new ArrayList();
		ArrayList ids = new ArrayList();
		ArrayList abbr = new ArrayList();
		desc.add(LC.L_Ascending); ids.add("1"); abbr.add("asc");
		desc.add(LC.L_Descending); ids.add("2"); abbr.add("desc");
		
		int dir = StringUtil.stringToNum((String)ids.get(abbr.indexOf(settingsSortDir)));
		return EJBUtil.createPullDownNoSelect(GADGET_PATSCHEDULESJB_DOT+SETTINGSSORTDIRMENU_KEY, 
				dir, ids, desc);
	}
	
	public void setSettingsSortDir(String settingsSortDir) {
		if (settingsSortDir == null) { settingsSortDir = "1"; } // asc = Ascending
		this.settingsSortDir = settingsSortDir;

		if(settingsSortDir != null){
			settingsSortDirMenu = createSortDirDD(settingsSortDir);
		}
	}

	public String getSettingsSortDir() {
		return settingsSortDir;
	}
	
	public void setSettingsSortDirMenu(String settingsSortDirMenu) {
		this.settingsSortDirMenu = settingsSortDirMenu;
	}

	public String getSettingsSortDirMenu() {
		return settingsSortDirMenu;
	}
	
	public void setVisitFilter(String visitFilter) {
		this.visitFilter = visitFilter;
		this.visitFilterMenu = EJBUtil.getRelativeTimeDDSchedule(
				GADGET_PATSCHEDULESJB_DOT+VISITFILTERMENU_KEY, visitFilter); 
	}

	public String getVisitFilter() {
		return visitFilter;
	}
	public void setVisitFilterMenu(String visitFilterMenu) {
		this.visitFilterMenu = visitFilterMenu;
	}

	public String getVisitFilterMenu() {
		return visitFilterMenu;
	}
	
	public void setShowMoreLink(String showMoreLink) {
		this.showMoreLink = showMoreLink;
	}

	public String getShowMoreLink() {
		return showMoreLink;
	}
	
	public String getPatSchedulesColumns(){
		mySchedulesColumns = new JSONArray();

		try{
			JSONObject aColumn = new JSONObject();	
			aColumn.put("key", PATIENTID_KEY);
			aColumn.put("label",LC.L_Patient_Id);
			//aColumn.put("type","image");
			aColumn.put("hidden","true");
			this.mySchedulesColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", "patprotId");
			aColumn.put("label","pk_patprot");
			aColumn.put("hidden","true");
			this.mySchedulesColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", PATSTUDYID_KEY);
			aColumn.put("label",LC.L_Pat_StudyId_Short);
			aColumn.put("type","image");
			this.mySchedulesColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", "patStudyStatus");
			aColumn.put("label",LC.L_Pat_StdStatus_Short);
			aColumn.put("type","image");
			this.mySchedulesColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", "fk_study");
			aColumn.put("label","Study ID");
			aColumn.put("hidden","true");
			this.mySchedulesColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", STUDYNUMBER_KEY);
			aColumn.put("label",LC.L_Study_Number);
			aColumn.put("type","image"); /** Special case of truncation **/
			this.mySchedulesColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", "studyAccess");
			aColumn.put("label","Study Access");
			aColumn.put("hidden","true");
			this.mySchedulesColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", "visitName");
			aColumn.put("label",LC.L_Visit);
			aColumn.put("type","image"); /** Special case of truncation **/
			this.mySchedulesColumns.put(aColumn);

			aColumn = new JSONObject();
			aColumn.put("key", "visitDate");
			aColumn.put("label","Visit Date");
			this.mySchedulesColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", "visitStatus");
			aColumn.put("label",LC.L_Visit_Status);
			aColumn.put("type","image");
			this.mySchedulesColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", "patStudyStatusDate");
			aColumn.put("label","Patient Study Status Date");
			aColumn.put("hidden","true");
			this.mySchedulesColumns.put(aColumn);

			aColumn = new JSONObject();
			aColumn.put("key", "patStudyStatusSubType");
			aColumn.put("label","Patient Study Status SubType");
			aColumn.put("hidden","true");
			this.mySchedulesColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", "patStudyStatusReason");
			aColumn.put("label","Patient Study Status Reason");
			aColumn.put("hidden","true");
			this.mySchedulesColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", "patientAccess");
			aColumn.put("label","Patient Access");
			aColumn.put("hidden","true");
			this.mySchedulesColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", "fk_per");
			aColumn.put("label","fk_per");
			aColumn.put("hidden","true");
			this.mySchedulesColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", "pk_patStudyStatus");
			aColumn.put("label","Patient Study Status PK");
			aColumn.put("hidden","true");
			this.mySchedulesColumns.put(aColumn);
			
			aColumn = new JSONObject();
			aColumn.put("key", "patStudyStatusNote");
			aColumn.put("label","Patient Study Status Note");
			aColumn.put("hidden","true");
			this.mySchedulesColumns.put(aColumn);
		} catch (Exception e){
			
		}
		
		return mySchedulesColumns.toString();
	}

    public String getPatSchedules(String userId, String studyIds, String sortBy, String sortDir){
    	mySchedulesData = new JSONArray();
    	//if (com.velos.esch.service.util.StringUtil.isEmpty(studyIds)) { return null; }
    	
    	PatientScheduleDAO patschDao = new PatientScheduleDAO();
    	ActionContext ac = ActionContext.getContext();
    	HttpServletRequest request = (HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
    	
    	String visitFilter = this.getVisitFilter();
    	
    	HashMap<String, String> jParams = new HashMap<String, String>();
    	jParams.put("userId", userId);
    	jParams.put("studyIds", studyIds);
    	jParams.put("visitFilter", visitFilter);
    	jParams.put("sortBy", sortBy);
    	jParams.put("sortDir", sortDir);

    	mySchedulesData = patschDao.getPatientSchedulesByStudy(request, jParams);
    	
    	int recordCount = StringUtil.stringToNum((String)request.getAttribute("gdtPatSchCount"));
		request.removeAttribute("gdtPatSchCount");
		if (recordCount > 10)
			this.setShowMoreLink("true");
		else
			this.setShowMoreLink("false");

    	return mySchedulesData.toString();
	}

}
