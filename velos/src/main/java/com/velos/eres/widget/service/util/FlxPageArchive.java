package com.velos.eres.widget.service.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.FormLibDao;
import com.velos.eres.business.common.FormResponseDao;
import com.velos.eres.business.common.LinkedFormsDao;
import com.velos.eres.business.common.StatusHistoryDao;
import com.velos.eres.business.common.StudyDao;
import com.velos.eres.business.common.StudyINDIDEDAO;
import com.velos.eres.business.common.StudyIdDao;
import com.velos.eres.business.common.StudyNIHGrantDao;
import com.velos.eres.business.common.StudySecDao;
import com.velos.eres.business.common.StudyTeamDao;
import com.velos.eres.business.common.StudyVerDao;
import com.velos.eres.business.studyVer.impl.StudyVerBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.compliance.service.ComplianceAgent;
import com.velos.eres.compliance.web.ComplianceJB;
import com.velos.eres.gems.business.WorkflowDAO;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.SessionMaint;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.statusHistory.StatusHistoryJB;
import com.velos.eres.web.study.StudyJB;
import com.velos.eres.web.studyId.StudyIdJB;
import com.velos.eres.web.studyNIHGrant.StudyNIHGrantJB;
import com.velos.eres.web.studyVer.StudyVerJB;
import com.velos.eres.web.submission.SubmissionJB;
import com.velos.eres.web.user.UserJB;
import com.velos.eres.widget.business.common.ResubmDraftDao;
import com.velos.eres.widget.business.common.UIFlxPageDao;
import com.velos.services.util.CodeCache;

public class FlxPageArchive {
	public static final String EMPTY_STRING = "", TITLE_STR = "title", FIELDS_STR = "fields", SECTIONS_STR = "sections";
	public static final String FLX_FIELD_ID_STR = "flxFieldId", STD_STR = "STD", MSD_STR = "MSD", TYPE_STR = "type", VERSION_STR = "version", STUDYSECTION_STR = "studySection",INDIDE_STR = "indide",STEAM_STR="steam";
	public static final String JSON_STR = "json", HTML_STR = "html", LABEL_STR = "label", CODE_PK_STR = "codePk", DATA_STR = "data", CODE_SUBTYP_STR = "codeSubtype";
	public static final String SOURCE_STR = "source", DRAFT_STR = "draft";
	public static final String NEWATTACHMENT_STR = "NEWATTACHMENT", DELETEDATTACHMENT_STR = "DELETEDATTACHMENT";

	private static final String Str_dropdown = "dropdown", Str_Hidden_input = "hidden-input", Str_input = "input";
	private static final String ER_STUDY_STR = "er_study", PROTOCOL_STR = "protocol";
	private static final String Str_chkbox = "chkbox", Str_checkbox = "checkbox";
	private static final String STUDY_DIVISION = "study_division", STUDY_ADM_DIV = "studyAdmDiv", STUDY_ADM_DEPT = "studyCond2";
	private static final String TAREA = "tarea";
	private static CodeCache codeCache = null;
	private static HashMap<Integer, String> studyDivisionMap = null;
	private static HashMap<Integer, String> tareaMap = null;
	private static final String VERSION_STATUS = "versionStatus";
	private static final String LETTER_F = "F";
	private static final String LETTER_W = "W";
	private static final String ER_STUDYVER_TABLE = "er_studyver";
	
	public static void main(String[] args) {
		System.setProperty("ERES_HOME", "C:/Velos/eResearch_jboss510/conf/");
		StudyJB studyJB = new StudyJB();
		studyJB.setStudyNumber("IH-A1b");
		studyJB.setStudyTitle("IH A1b title");
		StudyIdDao sidDao = new StudyIdDao();
		FlxPageArchive archive = new FlxPageArchive();
		JSONObject archiveJson = archive.createArchiveJson("protocol", studyJB, sidDao, 1, 0);
		String archiveHtml = archive.createArchiveHtml("protocol", archiveJson, studyJB.getId());
		System.out.println(archiveHtml);
	}
	
	public FlxPageArchive() {
		codeCache = CodeCache.getInstance();
		CodeDao studyDivisionCodeDao = codeCache.getCodes(STUDY_DIVISION, 0);
		ArrayList<Integer> studyDivisionPks = studyDivisionCodeDao.getCId();
		ArrayList<String> studyDivisionDescs = studyDivisionCodeDao.getCDesc();
		studyDivisionMap = new HashMap<Integer, String>();
		for (int iX = 0; iX < studyDivisionPks.size(); iX++) {
			studyDivisionMap.put(studyDivisionPks.get(iX), studyDivisionDescs.get(iX));
		}
		CodeDao tareaCodeDao = codeCache.getCodes(TAREA, 0);
		ArrayList<Integer> tareaPks = tareaCodeDao.getCId();
		ArrayList<String> tareaDescs = tareaCodeDao.getCDesc();
		tareaMap = new HashMap<Integer, String>();
		for (int iX = 0; iX < tareaPks.size(); iX++) {
			tareaMap.put(tareaPks.get(iX), tareaDescs.get(iX));
		}
	}
	
	private JSONArray fetchPageSections(String pageId) {
		UIFlxPageDao flxDao = new UIFlxPageDao();
		try {
			JSONObject pageDefObj = flxDao.flexPageData(pageId);
			JSONObject sectionsObj = flxDao.getFlxPageCookie(pageDefObj);
			return sectionsObj.getJSONArray(SECTIONS_STR);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new JSONArray();
	}

	private JSONObject getThisVersionNumber(JSONObject versionDetailsJSON){
		String pageVer = "", pageMinorVer = "";
		try {
			pageVer = versionDetailsJSON.getString("majorVersion");
			pageMinorVer = versionDetailsJSON.getString("minorVersion");
		} catch (JSONException e) {
			e.printStackTrace();
			//return null;
		}

		String fullVer = pageVer + ".";
		fullVer += (StringUtil.stringToNum(pageMinorVer) > 9) ? pageMinorVer : "0" + pageMinorVer;
		
		try {
			versionDetailsJSON.put("fullVersion", fullVer);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return versionDetailsJSON;
	}

	public JSONObject getNextVersionNumber(JSONObject versionDetailsJSON){
		JSONObject nextVersionDetailsJSON = new JSONObject();
		
		int versionNum = 0, minorVerNum = 0, rowExists = 0;
		String curStatus = EMPTY_STRING;
		try {
			versionNum = versionDetailsJSON.getInt("majorVersion");
			minorVerNum = versionDetailsJSON.getInt("minorVersion");
			rowExists = versionDetailsJSON.getInt("rowExists");
			curStatus = versionDetailsJSON.getString("curStatus");
		} catch (JSONException e) {
			e.printStackTrace();
			//return null;
		}

		if("crc_amend".equals(curStatus)) {
	    	versionNum++;
	    	minorVerNum=0;
	    } else if(minorVerNum >= 0) {
	    	if(rowExists == 1) {
		    	minorVerNum++;
	    	} else {
		    	if (versionNum == 0){
		    		versionNum++;
		    	} else {
		    		minorVerNum++;
		    	}
	    	}
	    } else {
	    	versionNum++;
	    	//minorVerNum++;
	    }

		String fullVersionNum = versionNum + ".";
		fullVersionNum += (minorVerNum > 9) ? minorVerNum : "0" + minorVerNum;
		
		try {
			nextVersionDetailsJSON.put("nextMajorVersion", versionNum);
			nextVersionDetailsJSON.put("nextMinorVersion", minorVerNum);
			nextVersionDetailsJSON.put("nextFullVersion", fullVersionNum);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return nextVersionDetailsJSON;
	}

	public String createOrUpdateResubmissionDraft(StudyJB studyJB, StudyIdDao studyIdDao, HashMap paramMap) {
	    int retNum = 0;
		if (studyJB == null || studyIdDao == null) { return String.valueOf(retNum); }
	    ResubmDraftDao resubmDraftDao = new ResubmDraftDao();
	    JSONObject flexPageData = null;
	    String pageDef = null;
	    String pageJS = null;
	    try {
	    	flexPageData = resubmDraftDao.flexPageData(PROTOCOL_STR);
	    	pageDef = (String)flexPageData.get(resubmDraftDao.PAGE_DEF);
	    	pageJS = (String)flexPageData.get(resubmDraftDao.PAGE_JS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    String attachFieldDetails = (String)paramMap.get("attachFieldDetails");
	    UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
	    int minorVerNum=0;
	    ComplianceJB compJB = new ComplianceJB();
	    String curStatus = compJB.getCurrentStudyStatus(paramMap);
 		int versionNum = uiFlxPageDao.getHighestFlexPageVersion("er_study",  studyJB.getId());	    
	    minorVerNum=uiFlxPageDao.getHighestMinorFlexPageVersion(versionNum, "er_study",  studyJB.getId());
	    int rowExists = uiFlxPageDao.checkLockedVersionExists(studyJB.getId(), versionNum);
	    if (rowExists < 1) {
	    	rowExists = resubmDraftDao.checkIrbApprovedExists(studyJB.getId(), versionNum);
	    }

	    JSONObject versionDetailsJSON = new JSONObject();
	    try {
			versionDetailsJSON.put("majorVersion", versionNum);
			versionDetailsJSON.put("minorVersion", minorVerNum);
			versionDetailsJSON.put("rowExists", rowExists);
			versionDetailsJSON.put("curStatus", curStatus);
		} catch (JSONException e) {
			e.printStackTrace();
		}

	    int nextVersionNum = 0, nextMinorVerNum = 0;
	    String nextFlxPageFullVersionNum = EMPTY_STRING;
	    JSONObject nextVersionDetailsJSON = getNextVersionNumber(versionDetailsJSON);
	    try {
			nextVersionNum = nextVersionDetailsJSON.getInt("nextMajorVersion");
			nextMinorVerNum = nextVersionDetailsJSON.getInt("nextMinorVersion");
			nextFlxPageFullVersionNum = nextVersionDetailsJSON.getString("nextFullVersion");
		} catch (JSONException e) {
			e.printStackTrace();
		}

	    SubmissionJB submissionJB = new SubmissionJB();
	    int submissionId = submissionJB.getExistingSubmissionByFkStudy(String.valueOf(studyJB.getId()));
		JSONObject archiveJson = this.createArchiveJson("protocol", studyJB, studyIdDao, nextVersionNum, nextMinorVerNum);
		String archiveHtml = this.createArchiveHtml("protocol", archiveJson, studyJB.getId());
		JSONObject formResponseArvJSON = this.createFormResponseArchiveJson(studyJB, paramMap);

		JSONArray attachmentArvJson = new JSONArray();
		String comments = EMPTY_STRING;
		if(!("1.00".equals(nextFlxPageFullVersionNum))) {
			attachmentArvJson = this.createAttachArchiveJson(attachFieldDetails );
		}

		HashMap<String, Object> argMap = new HashMap<String, Object>();
		argMap.put(ResubmDraftDao.MOD_TABLE_STR, ER_STUDY_STR);
		argMap.put(ResubmDraftDao.MOD_PK_STR, studyJB.getId());
		argMap.put(ResubmDraftDao.PAGE_VER_STR, nextVersionNum);
		argMap.put(ResubmDraftDao.PAGE_ID_STR, PROTOCOL_STR);
		argMap.put(ResubmDraftDao.PAGE_DEF_STR, pageDef);
		argMap.put(ResubmDraftDao.PAGE_JS_STR, pageJS);
		argMap.put(ResubmDraftDao.PAGE_JSON_STR, archiveJson.toString());
		argMap.put(ResubmDraftDao.PAGE_HTML_STR, archiveHtml);
		argMap.put(ResubmDraftDao.PAGE_MINOR_VER, nextMinorVerNum);
		argMap.put(ResubmDraftDao.FK_SUBMISSION, submissionId);
		argMap.put(ResubmDraftDao.PAGE_ATTACHDIFF, attachmentArvJson.toString());
		argMap.put(ResubmDraftDao.PAGE_FORMRESPONSES, formResponseArvJSON.toString());
		argMap.put(ResubmDraftDao.PAGE_FORMRESPDIFF, "[]");
		argMap.put(ResubmDraftDao.PAGE_COMMENTS, comments);
		
		resubmDraftDao.getHighestResubmDraftFullVersion("er_study", studyJB.getId());
		JSONObject versionDetailsJSON2 = new JSONObject();
		try {
			versionDetailsJSON2.put("majorVersion", resubmDraftDao.getMajorVer());
			versionDetailsJSON2.put("minorVersion", resubmDraftDao.getMinorVer());
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JSONObject nextVersionDetailsJSON2 = getThisVersionNumber(versionDetailsJSON2);

		String nextResubmDraftFullVersion = EMPTY_STRING;
		try {
			nextResubmDraftFullVersion = nextVersionDetailsJSON2.getString("fullVersion");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		if (!nextResubmDraftFullVersion.equals(nextFlxPageFullVersionNum)){
			retNum = resubmDraftDao.insertResubmissionDraft(argMap);
			if (retNum < 1) { return String.valueOf(retNum); }
		} else {
			// Update version draft ver_diff here;
			retNum = resubmDraftDao.updateResubmissionDraftRetainLastDiff(argMap);
		}

	    return nextFlxPageFullVersionNum;
	}

	public String createArchive(StudyJB studyJB, StudyIdDao studyIdDao, HashMap paramMap) {
	    int retNum = 0;
		if (studyJB == null || studyIdDao == null) { return String.valueOf(retNum); }
	    UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
	    JSONObject flexPageData = null;
	    String pageDef = null;
	    String pageJS = null;
	    try {
	    	flexPageData = uiFlxPageDao.flexPageData(PROTOCOL_STR);
	    	pageDef = (String)flexPageData.get(UIFlxPageDao.PAGE_DEF);
	    	pageJS = (String)flexPageData.get(UIFlxPageDao.PAGE_JS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    int minorVerNum=0;
	    ComplianceJB compJB = new ComplianceJB();
	    String curStatus = compJB.getCurrentStudyStatus(paramMap);
 		int versionNum = uiFlxPageDao.getHighestFlexPageVersion("er_study",  studyJB.getId());	    
	    minorVerNum=uiFlxPageDao.getHighestMinorFlexPageVersion(versionNum, "er_study",  studyJB.getId());
	    int rowExists = uiFlxPageDao.checkLockedVersionExists(studyJB.getId(), versionNum);
	    if (rowExists < 1) {
	    	rowExists = uiFlxPageDao.checkIrbApprovedExists(studyJB.getId(), versionNum);
	    }

	    JSONObject versionDetailsJSON = new JSONObject();
	    try {
			versionDetailsJSON.put("majorVersion", versionNum);
			versionDetailsJSON.put("minorVersion", minorVerNum);
			versionDetailsJSON.put("rowExists", rowExists);
			versionDetailsJSON.put("curStatus", curStatus);
		} catch (JSONException e) {
			e.printStackTrace();
		}

	    int nextVersionNum = 0, nextMinorVerNum = 0;
	    String nextFlxPageFullVersionNum = EMPTY_STRING;
	    JSONObject nextVersionDetailsJSON = getNextVersionNumber(versionDetailsJSON);
	    try {
			nextVersionNum = nextVersionDetailsJSON.getInt("nextMajorVersion");
			nextMinorVerNum = nextVersionDetailsJSON.getInt("nextMinorVersion");
			nextFlxPageFullVersionNum = nextVersionDetailsJSON.getString("nextFullVersion");
		} catch (JSONException e) {
			e.printStackTrace();
		}
	      
	    SubmissionJB submissionJB = new SubmissionJB();
	    int submissionId = submissionJB.getExistingSubmissionByFkStudy(String.valueOf(studyJB.getId()));

	    HashMap<String, Object> argMap1 = new HashMap<String, Object>();
	    argMap1.put(ResubmDraftDao.PAGE_ID_STR, PROTOCOL_STR);
	    argMap1.put(ResubmDraftDao.MOD_TABLE_STR, ER_STUDY_STR);
	    argMap1.put(ResubmDraftDao.MOD_PK_STR, studyJB.getId());
	    argMap1.put(ResubmDraftDao.PAGE_VER_STR, nextVersionNum);
	    argMap1.put(ResubmDraftDao.PAGE_MINOR_VER, nextMinorVerNum);

	    JSONObject archiveJson = null;
	    String archiveHtml = null;
	    JSONObject formResponseArvJSON = null;
		JSONArray attachmentArvJson = new JSONArray();
		JSONArray jsFormRespDiffArray = new JSONArray();
		String comments = null;

	    if (nextVersionNum == 1 && nextMinorVerNum == 0) {
	    	// New Submission
		    String attachFieldDetails = (String)paramMap.get("attachFieldDetails");
		    archiveJson = this.createArchiveJson("protocol", studyJB, studyIdDao, nextVersionNum, nextMinorVerNum);
			formResponseArvJSON = this.createFormResponseArchiveJson(studyJB, paramMap);
			archiveHtml = this.createArchiveHtml("protocol", archiveJson, studyJB.getId());
			attachmentArvJson = new JSONArray();
			if(!("1.0".equals(nextFlxPageFullVersionNum))) {
				attachmentArvJson = this.createAttachArchiveJson(attachFieldDetails );
			}
			comments = EMPTY_STRING;
	    } else {
	    	/* Instead of recalculating all the columns copy them from re-submission draft */
		    // Read values from re-submission draft
			ResubmDraftDao resubmDraftDao = new ResubmDraftDao();
			JSONObject jsResubmDraftObj = resubmDraftDao.getResubmissionDraft(argMap1);
		    if (null == jsResubmDraftObj){ return null;}
		    
		    try {
				archiveJson = new JSONObject(jsResubmDraftObj.getString("PAGE_JSON"));
				archiveHtml = this.createArchiveHtml("protocol", archiveJson, studyJB.getId());
				if (StringUtil.isEmpty(archiveHtml)) { return null; }
				formResponseArvJSON =  new JSONObject(jsResubmDraftObj.getString(ResubmDraftDao.PAGE_FORMRESPONSES));
				if(!("1.0".equals(nextFlxPageFullVersionNum))) {
					attachmentArvJson = new JSONArray(jsResubmDraftObj.getString(ResubmDraftDao.PAGE_ATTACHDIFF));
				}
				jsFormRespDiffArray = new JSONArray(jsResubmDraftObj.getString(ResubmDraftDao.PAGE_FORMRESPDIFF));
				comments = (String) paramMap.get("comments");
				comments = StringUtil.htmlEncodeXss((StringUtil.isEmpty(comments))? EMPTY_STRING : comments.toString());
			} catch (JSONException e) {
				e.printStackTrace();
				return null;
			}
	    }

		HashMap<String, Object> argMap = new HashMap<String, Object>();
		argMap.put(UIFlxPageDao.MOD_TABLE_STR, ER_STUDY_STR);
		argMap.put(UIFlxPageDao.MOD_PK_STR, studyJB.getId());
		argMap.put(UIFlxPageDao.PAGE_VER_STR, nextVersionNum);
		argMap.put(UIFlxPageDao.PAGE_ID_STR, PROTOCOL_STR);
		argMap.put(UIFlxPageDao.PAGE_DEF_STR, pageDef);
		argMap.put(UIFlxPageDao.PAGE_JS_STR, pageJS);
		argMap.put(UIFlxPageDao.PAGE_JSON_STR, archiveJson.toString());
		argMap.put(UIFlxPageDao.PAGE_HTML_STR, archiveHtml);
		argMap.put(UIFlxPageDao.PAGE_MINOR_VER, nextMinorVerNum);
		argMap.put(UIFlxPageDao.FK_SUBMISSION, submissionId);
		argMap.put(UIFlxPageDao.PAGE_ATTACHDIFF, attachmentArvJson.toString());
		argMap.put(UIFlxPageDao.PAGE_FORMRESPONSES, formResponseArvJSON.toString());
		argMap.put(UIFlxPageDao.PAGE_FORMRESPDIFF, jsFormRespDiffArray.toString());
		argMap.put(UIFlxPageDao.PAGE_COMMENTS, comments);

		retNum = uiFlxPageDao.insertFlexPageVer(argMap);
		if (retNum < 1) { return String.valueOf(retNum); }

	    return nextFlxPageFullVersionNum;
	}
	StudyJB studyJB =new StudyJB();
	StudyNIHGrantJB studyNIHGrantB=new StudyNIHGrantJB();
	StudyNIHGrantDao studyNIHGrantDao = studyNIHGrantB.getStudyNIHGrantRecords(studyJB.getId());
	ArrayList pk_NIhGrants=studyNIHGrantDao.getPkStudyNIhGrants();
	ArrayList fund_mechsms = studyNIHGrantDao.getFundMechsms();
	ArrayList inst_codes = studyNIHGrantDao.getInstitudeCodes();
	ArrayList program_codes = studyNIHGrantDao.getProgramCodes();
	ArrayList serial_numbers = studyNIHGrantDao.getNihGrantSerials();
	public String createArchiveHtml(String pageId, JSONObject dataJson, int studyId) {
		System.out.println("dataJson===="+dataJson.toString());
		StringBuffer sb1 = new StringBuffer("<html>");
		sb1.append("<head>");
		sb1.append("<style>");
		sb1.append(".flex-section-field { border:1px solid #aaaaaa; color:#111111; padding:3px; margin:3px; } ");
		sb1.append(" tr { border:1px; color:#212121; padding:3px; margin:3px; } ");
		sb1.append(" td { border:1px; color:#212121; } ");
		sb1.append(" table.flex-child-table { border:1px solid #aaaaaa; color:#111111; padding:3px; margin:3px; } ");
		sb1.append("</style>");
		sb1.append("<script>$j(document).ready(function(){flexStudyScreenJS.isProtocolLocked = true;});</script>");
		sb1.append("</head><body>");
		sb1.append("<form id='studyScreenForm' name='studyScreenForm'>");
		sb1.append("<input type='hidden' id='formStudy' name ='formStudy' value='");
		sb1.append(studyId + "'/>");
		sb1.append("<table class='flex-top-table' width='100%'>");
		try {
			sb1.append("<tr class='flex-section-header'><td align='right' colspan='2'>Version: ");
			sb1.append(dataJson.get(VERSION_STR));
			sb1.append("</td></tr>");
			sb1.append("<tr><td>");
			JSONArray sectionDataArray = dataJson.getJSONArray(SECTIONS_STR);
			for (int iS = 0; iS < sectionDataArray.length(); iS++) {
				JSONObject section = sectionDataArray.getJSONObject(iS);
				sb1.append("<table id='sectionHead"+ (iS+1) +"' class='flex-top-table' width='100%'>");
				sb1.append("<tr class='flex-section-header ui-accordion-header ui-helper-reset ui-state-active ui-corner-top'><td colspan='2'>");
				String sectionTitle = null;
				try {
					sectionTitle = (String)section.get(TITLE_STR);
				} catch(Exception e) {}
				sb1.append(sectionTitle).append("</td></tr>");
				JSONArray fieldArray = null;
				try {
					fieldArray = section.getJSONArray(FIELDS_STR);
				} catch(Exception e) {
					fieldArray = null;
				}
				if (fieldArray == null) {
					continue; // to next section
				}
				for (int iF = 0; iF < fieldArray.length(); iF++) {
					JSONObject fieldObj = (JSONObject)fieldArray.get(iF);
					try {
						if (STD_STR.equals(fieldObj.get(TYPE_STR))) {
							String flexKey = null;
							Iterator<?> flexKeys = fieldObj.keys();
							while(flexKeys.hasNext()) {
								String key = (String)flexKeys.next();
								if (!TYPE_STR.equals(key)) {
									flexKey = String.valueOf(key);
									break;
								}
							}
							FlxPageFields field = FlxPageFields.getFlxPageFieldByKey(flexKey);
							sb1.append("<tr class='flex-section-field'><td width='40%'>").append(field.getFieldLabel());

							String fieldValue = (String) fieldObj.get(flexKey);
							JSONObject fieldHTMLSpecJSON = field.getFieldHTMLSpecJSON();
							String fieldType = fieldHTMLSpecJSON.getString("fieldType");
							String htmlFieldId = fieldHTMLSpecJSON.getString("id");

							if (Str_checkbox.equals(fieldType)&& !("nihGrantChk".equals(String.valueOf(fieldHTMLSpecJSON.get("id"))))){
							 if(!fieldValue.equals("Y")&& !fieldValue.equals("N")){
								fieldValue = (StringUtil.stringToNum(fieldValue) > 0)? "Y" : "N";
								}
							}
							
							System.out.println("nihGrantChk===---------======"+String.valueOf(fieldHTMLSpecJSON.get("id")));
							 if ("nihGrantChk".equals(String.valueOf(fieldHTMLSpecJSON.get("id")))) {
								 System.out.println("nihGrantChk==="+String.valueOf(fieldHTMLSpecJSON.get("id")));
								 JSONObject archiveJson=new JSONObject();
								 archiveJson=createJsonForNihGrantInformation(pk_NIhGrants,fund_mechsms,inst_codes,serial_numbers,program_codes);
								 //flexValue = (String)archiveJson.get(archiveJson);
								// System.out.println("archiveJson.has(flexValue))---=-=--=-=-="+archiveJson.has(flexValue));
								// System.out.println("archiveJson.getString(flexValue)1------------------>"+archiveJson.getString(flexValue));
								 if (archiveJson.has("STUDY_GRANT_NIH_INFORMATION")){
										System.out.println("NihGrant_Detailsrgftdrtr------====="+archiveJson.getString("STUDY_GRANT_NIH_INFORMATION"));
										//System.out.println("archiveJson.getString(flexValue)2------------------>"+archiveJson.getString(flexValue));
										sb1.append(createHtmlTableNihGrant(archiveJson.getString("STUDY_GRANT_NIH_INFORMATION")));
									}
							 }else{
							sb1.append("</td><td id='"+ htmlFieldId +"' name='"+ htmlFieldId +"' width='60%'>").append(fieldValue).append("</td>");
							 }
							sb1.append("</tr>");
						} else if (MSD_STR.equals(fieldObj.get(TYPE_STR))) {
							sb1.append("<tr class='flex-section-field'><td width='40%'>").append(fieldObj.get(LABEL_STR));
							String fieldKey = fieldObj.getString(CODE_SUBTYP_STR);
							CodeDao cdMSD = new CodeDao();
							cdMSD.getCodeValuesBySubTypes("studyidtype", new String[] { fieldKey });
							if (null == cdMSD.getCId() || cdMSD.getCId().size() < 1){
								continue;
							}

							String htmlFieldId = "alternateId"+StringUtil.stringToNum((cdMSD.getCId()).get(0).toString());
							if (STUDY_ADM_DIV.equals(fieldKey)) {
								String studyDivisionDescription = studyDivisionMap.get(StringUtil.stringToInteger((String)fieldObj.get(DATA_STR)));
								studyDivisionDescription = (StringUtil.isEmpty(studyDivisionDescription))? EMPTY_STRING : studyDivisionDescription;
								sb1.append("</td><td id ='"+ htmlFieldId +"' name='"+ htmlFieldId +"' width='60%'>").append(studyDivisionDescription).append("</td>");
							} else if (STUDY_ADM_DEPT.equals(fieldKey)) {
								String tareaDescription = tareaMap.get(StringUtil.stringToInteger((String)fieldObj.get(DATA_STR)));
								tareaDescription = (StringUtil.isEmpty(tareaDescription))? EMPTY_STRING : tareaDescription;
								sb1.append("</td><td id ='"+ htmlFieldId +"' name='"+ htmlFieldId +"' width='60%'>").append(tareaDescription).append("</td>");
							} else {
								String fieldData = fieldObj.getString(DATA_STR);

								if (!StringUtil.isEmpty(fieldData)){
									ArrayList customColList = cdMSD.getCodeCustom();
									if (null != customColList){
										String fldType = (null == customColList)? EMPTY_STRING : (String)(customColList.get(0));
										fldType = (StringUtil.isEmpty(fldType))? Str_input : fldType;

										if (Str_dropdown.equals(fldType)){
											ArrayList customCol1List = cdMSD.getCodeCustom1();
											if (null != customCol1List){
												try{
													JSONObject jsCustomCol1Config = new JSONObject((String)customCol1List.get(0));
													String codeType = jsCustomCol1Config.getString("codelst_type");

													CodeDao cdMSDResp = new CodeDao();
													cdMSDResp.getCodeValuesBySubTypes(codeType, new String[] { fieldData });

													@SuppressWarnings("unchecked")
													ArrayList<String> cdDescriptions = cdMSDResp.getCDesc();
													if (null != cdDescriptions){
														fieldData = (String) cdDescriptions.get(0);	
													}
												} catch(JSONException jsonE){}
											}
										} else if ("textarea".equals(fldType)){
											fieldData = StringUtil.replace(fieldData, "\n", "<BR>");
										}
									}
								}
								sb1.append("</td><td id ='"+ htmlFieldId +"' name='"+ htmlFieldId +"' width='60%'>").append(fieldData).append("</td>");
							}
							sb1.append("</tr>");
						} else if (STUDYSECTION_STR.equals(fieldObj.get(TYPE_STR))) {
							Iterator<?> flexKeys = fieldObj.keys();
							String description="";
							String flexValue="";
							while(flexKeys.hasNext()) {
								String key = (String)flexKeys.next();
								if (!TYPE_STR.equals(key)) {
									CodeDao cDao= new CodeDao();
									int codeId = cDao.getCodeId("section", key);
									description = cDao.getCodeDesc(codeId);
									flexValue = (String)fieldObj.get(key);
									break;
								}
							}
							sb1.append("<tr class='flex-section-field'><td width='40%'>").append(description);
							sb1.append("</td><td width='60%'>").append(flexValue).append("</td>");
							sb1.append("</tr>");
						} else if (INDIDE_STR.equals(fieldObj.get(TYPE_STR))) {
							Iterator<?> flexKeys = fieldObj.keys();
							String flexValue="";
							while(flexKeys.hasNext()) {
								String key = (String)flexKeys.next();
								flexValue = (String)fieldObj.get(key);
								if ("IND/IDE Details ".equals(key)){
									sb1.append(createHtmlTableINDIDE(flexValue));
								}
							}
						}  else if (STEAM_STR.equals(fieldObj.get(TYPE_STR))) {
							Iterator<?> flexKeys = fieldObj.keys();
							String flexValue="";
							while(flexKeys.hasNext()) {
								String key = (String)flexKeys.next();
								flexValue = (String)fieldObj.get(key);
								if ("Collaborators Details ".equals(key)){
									sb1.append(createHtmlTableSTeam(flexValue));
								}
							}
						}
						
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
				sb1.append("</table>");
			}
			sb1.append("</td></tr></table>");
		} catch(Exception e) {
			e.printStackTrace();
		}
		sb1.append("</td></tr></table>");
		sb1.append("</form>");
		sb1.append("</body></html>");
		return sb1.toString();
	}

	private StringBuffer createHtmlTableINDIDE (String flexValue){
		StringBuffer tableBuffer = new StringBuffer();
		tableBuffer.append("<tr class='flex-section-field'><td colspan='2'>")
		.append("<table class='flex-child-table'><tr style='font-weight:bold; background:none; background-color:#aaaaaa'>")
		.append("<td>").append(LC.L_IndIde_Type).append("</td>")
		.append("<td>").append(LC.L_IndIde_Number).append("</td>")
		.append("<td>").append(LC.L_IndIde_Grantor).append("</td>")
		.append("<td>").append(LC.L_IndIde_HldrType).append("</td>")
		.append("<td>").append(MC.M_NihInstNic_DivCode).append("</td>")
		.append("<td>").append(LC.L_Expanded_Access).append("</td>")
		.append("<td>").append(LC.L_Expanded_AccType).append("</td>")
		.append("<td>").append(LC.L_Exempt).append("</td>")
		.append("</tr>");

		JSONArray jsonArrayINDIDE;
		try {
			jsonArrayINDIDE = new JSONArray(flexValue);
			for (int indx = 0; indx < jsonArrayINDIDE.length(); indx++){
				JSONObject jsObjINDIDE = jsonArrayINDIDE.getJSONObject(indx);

				String INDIDEType = EMPTY_STRING;
				try{
					INDIDEType = jsObjINDIDE.getString("INDIDEType");
					if (StringUtil.isEmpty(INDIDEType)){
						continue;
					}

					INDIDEType = ("1".equals(INDIDEType))? "IND" : 
						("2".equals(INDIDEType))? "IDE" : 
						("3".equals(INDIDEType))? "IND Exempt" : 
						("4".equals(INDIDEType))? "IDE Exempt" :
						("5".equals(INDIDEType))? "IND/IDE" : INDIDEType;
					tableBuffer.append("<tr><td>").append(INDIDEType).append("</td>");
				} catch (Exception e1){}

				String INDIDENumber = EMPTY_STRING;
				try{
					INDIDENumber = jsObjINDIDE.getString("INDIDENumber");
					INDIDENumber = (StringUtil.isEmpty(INDIDENumber))? EMPTY_STRING : INDIDENumber;
				}catch (Exception e1){}
				tableBuffer.append("<td>").append(INDIDENumber).append("</td>");

				String INDIDEGrantor  = EMPTY_STRING;
				try{
					INDIDEGrantor = jsObjINDIDE.getString("INDIDEGrantor");
					INDIDEGrantor = (StringUtil.isEmpty(INDIDEGrantor))? EMPTY_STRING : INDIDEGrantor;
				}catch (Exception e1){}
				tableBuffer.append("<td>").append(INDIDEGrantor).append("</td>");

				String INDIDEHolder = EMPTY_STRING;
				try{
					INDIDEHolder = jsObjINDIDE.getString("INDIDEHolder");
					INDIDEHolder = (StringUtil.isEmpty(INDIDEHolder))? EMPTY_STRING : INDIDEHolder;
				}catch (Exception e1){}
				tableBuffer.append("<td>").append(INDIDEHolder).append("</td>");

				String ProgramCode = EMPTY_STRING;
				try{
					ProgramCode = jsObjINDIDE.getString("ProgramCode");
					ProgramCode = (StringUtil.isEmpty(ProgramCode))? EMPTY_STRING : ProgramCode;
				}catch (Exception e1){}
				tableBuffer.append("<td>").append(ProgramCode).append("</td>");

				String INDIDEExpandAccess = EMPTY_STRING;
				try{
					INDIDEExpandAccess = jsObjINDIDE.getString("INDIDEExpandAccess");
					INDIDEExpandAccess = (StringUtil.stringToNum(INDIDEExpandAccess) > 0)? "Y" : "N";
				}catch (Exception e1){}
				tableBuffer.append("<td>").append(INDIDEExpandAccess).append("</td>");

				String INDIDEAccess = EMPTY_STRING;
				try{
					INDIDEAccess = jsObjINDIDE.getString("INDIDEAccess");
					INDIDEAccess = (StringUtil.isEmpty(INDIDEAccess))? EMPTY_STRING : INDIDEAccess;
				}catch (Exception e1){}
				tableBuffer.append("<td>").append(INDIDEAccess).append("</td>");

				String INDIDEExempt = EMPTY_STRING;
				try{
					INDIDEExempt = jsObjINDIDE.getString("INDIDEExempt");
					INDIDEExempt = (StringUtil.stringToNum(INDIDEExempt) > 0)? "Y" : "N";
				}catch (Exception e1){}
				tableBuffer.append("<td>").append(INDIDEExempt).append("</td>");

				tableBuffer.append("</tr>");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		tableBuffer.append("</table>");
		tableBuffer.append("</td>");
		tableBuffer.append("</tr>");
		return tableBuffer;
	}

	private StringBuffer createHtmlTableSTeam (String flexValue){
		StringBuffer tableBuffer = new StringBuffer();
		tableBuffer.append("<tr class='flex-section-field'><td colspan='2'>")
		.append("<table class='flex-child-table'><tr style='font-weight:bold; background:none; background-color:#aaaaaa'>")
		.append("<td>").append(LC.L_Name).append("</td>")
		.append("<td>").append(LC.L_Role).append("</td>")
		.append("<td>").append(LC.L_Email).append("</td>")
		.append("</tr>");

		JSONArray jsonArrayStudyTeam;
		try {
			jsonArrayStudyTeam = new JSONArray(flexValue);
			for (int indx = 0; indx < jsonArrayStudyTeam.length(); indx++){
				JSONObject jsObjTeamMember = jsonArrayStudyTeam.getJSONObject(indx);

				String memberName = EMPTY_STRING;
				try{
					memberName = jsObjTeamMember.getString("name");
					if (StringUtil.isEmpty(memberName)){
						continue;
					}
					tableBuffer.append("<tr><td>").append(memberName).append("</td>");
				} catch (Exception e1){}
				
				String memberRole = EMPTY_STRING;
				try{
					memberRole = jsObjTeamMember.getString("role");
					memberRole = (StringUtil.isEmpty(memberRole))? EMPTY_STRING : memberRole;
				}catch (Exception e1){}
				tableBuffer.append("<td>").append(memberRole).append("</td>");

				String memberEmail  = EMPTY_STRING;
				try{
					memberEmail = jsObjTeamMember.getString("email");
					memberEmail = (StringUtil.isEmpty(memberEmail))? EMPTY_STRING : memberEmail;
				}catch (Exception e1){}
				tableBuffer.append("<td>").append(memberEmail).append("</td>");

				tableBuffer.append("</tr>");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		tableBuffer.append("</table>");
		tableBuffer.append("</td>");
		tableBuffer.append("</tr>");
		return tableBuffer;
	}
	private StringBuffer createHtmlTableNihGrant(String flexValue){
		System.out.println("createHtmlTableNihGrant---"+flexValue);
		StringBuffer tableBuffer = new StringBuffer();
		tableBuffer.append("<tr class='flex-section-field'><td colspan='2'>")
		.append("<table class='flex-child-table'><tr style='font-weight:bold; background:none; background-color:#aaaaaa'>")
		.append("<td>").append(LC.L_Funding_Mechsm).append("</td>")
		.append("<td>").append(LC.L_Institute_Code).append("</td>")
		.append("<td>").append(LC.L_Serial_Number).append("</td>")
		.append("<td>").append(LC.L_NCI_Div_Prom_Code).append("</td>")
		.append("</tr>");

		JSONArray jsonArrayStudyTeam;
		
		try {
			jsonArrayStudyTeam = new JSONArray(flexValue);
			for (int indx = 0; indx < jsonArrayStudyTeam.length(); indx++){
				JSONObject jsObjTeamMember = jsonArrayStudyTeam.getJSONObject(indx);
				String memberFunding_Mechsm = EMPTY_STRING;
				try{
					
					memberFunding_Mechsm = jsObjTeamMember.getString("fund_mechsms");
					if (StringUtil.isEmpty(memberFunding_Mechsm)){
						continue;
					}
					System.out.println("memberFunding_Mechsm after1 ==="+memberFunding_Mechsm);
					tableBuffer.append("<tr><td>").append(memberFunding_Mechsm).append("</td>");
				} catch (Exception e1){
					System.out.println("memberFunding_Mechsm inside catch===");

				}
				
				
				
				String memberInstitute_Code = EMPTY_STRING;
				try{
					memberInstitute_Code = jsObjTeamMember.getString("inst_codes");
					if (StringUtil.isEmpty(memberInstitute_Code)){
						continue;
					}
					tableBuffer.append("<td>").append(memberInstitute_Code).append("</td>");
				} catch (Exception e1){}
				
				String memberSerial_Number  = EMPTY_STRING;
				
				try{
					memberSerial_Number = jsObjTeamMember.getString("serial_numbers");
					memberSerial_Number = (StringUtil.isEmpty(memberSerial_Number))? EMPTY_STRING : memberSerial_Number;
					System.out.println("memberSerial_Number===="+memberSerial_Number);
					tableBuffer.append("<td>").append(memberSerial_Number).append("</td>");
				}catch (Exception e1){}
				String memberProgram_codes = EMPTY_STRING;
				try{
					memberProgram_codes = jsObjTeamMember.getString("program_codes");
					if (StringUtil.isEmpty(memberProgram_codes)){
						continue;
					}
					System.out.println("memberProgram_codes===="+memberProgram_codes);
					tableBuffer.append("<td>").append(memberProgram_codes).append("</td>");
				} catch (Exception e1){}
				
				

				tableBuffer.append("</tr>");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		tableBuffer.append("</table>");
		tableBuffer.append("</td>");
		tableBuffer.append("</tr>");
		return tableBuffer;
	}

	public JSONObject createArchiveJson(String pageId, StudyJB studyJB, StudyIdDao studyIdDao, 
			int version, int minorVer) {
		JSONObject outputObj = new JSONObject();
		JSONArray sectionInputArray = null;
		JSONArray sectionOutputArray = new JSONArray();
		StudySecDao ssDao = new StudySecDao();
		HashMap<String, String> smap = ssDao.getStudySectionData(studyJB.getId());
		StudyINDIDEDAO siDao = new StudyINDIDEDAO();
		ArrayList<String> indideList = siDao.getIndIdeData(studyJB.getId());
		StudyTeamDao stDao = new StudyTeamDao();
		ArrayList<String> arList = stDao.getStudyTeamAndCollaboratorsData(studyJB.getId());
		StudyNIHGrantJB studyNIHGrantB=new StudyNIHGrantJB();
		StudyNIHGrantDao studyNIHGrantDao = studyNIHGrantB.getStudyNIHGrantRecords(studyJB.getId());		
		//int studyId = StringUtil.stringToNum("studyId");
		//StudyNIHGrantDao studyNIHGrantDao = studyNIHGrantB.getStudyNIHGrantRecords(studyId);
		//ArrayList <String> pk_NIhGrants = studyNIHGrantDao.getPkStudyNIhGrants();
		 pk_NIhGrants = studyNIHGrantDao.getPkStudyNIhGrants();
		 fund_mechsms = studyNIHGrantDao.getFundMechsms();
		 inst_codes = studyNIHGrantDao.getInstitudeCodes();
		 program_codes = studyNIHGrantDao.getProgramCodes();
		 serial_numbers = studyNIHGrantDao.getNihGrantSerials();
		try { 
			sectionInputArray = fetchPageSections(pageId);
			for (int iS = 0; iS < sectionInputArray.length(); iS++) {
				JSONObject section = sectionInputArray.getJSONObject(iS);
				String sectionTitle = null;
				try {
					sectionTitle = (String)section.get(TITLE_STR);
				} catch(Exception e) {}
				if (sectionTitle == null) { sectionTitle = EMPTY_STRING; }
				JSONArray fieldArray = null;
				try {
					fieldArray = section.getJSONArray(FIELDS_STR);
				} catch(Exception e) {
					fieldArray = null;
				}
				JSONArray fieldsOutputArray = new JSONArray();
				String secType=(String)section.get("secType");
				if (fieldArray == null && !secType.equals("predef")) {
					System.out.println("secType========================================="+secType.equals("predef"));
					continue; // to next section
					
				}
				
				
				else{
					if(fieldArray!=null){
				for (int iF = 0; iF < fieldArray.length(); iF++) {
					JSONObject fieldObj = (JSONObject)fieldArray.get(iF);
					if (STD_STR.equals(fieldObj.get(TYPE_STR))) {
						
						System.out.println("fieldObj"+fieldObj);
						FlxPageFields field = FlxPageFields.getFlxPageFieldByKey((String)fieldObj.get(FLX_FIELD_ID_STR));
						JSONObject fieldJson = createPageFieldJsonStd(field, studyJB);
						JSONObject flxHTMLJSON = field.getFieldHTMLSpecJSON();
						if(!"nihGrantChk".equals(String.valueOf(flxHTMLJSON.get("id")))){
						if (fieldJson != null) {
							fieldJson.put(TYPE_STR, STD_STR);
							fieldsOutputArray.put(fieldJson);
						}
						}
						
						System.out.println("id=====>"+String.valueOf(flxHTMLJSON.get("id")));
						if("nihGrantChk".equals(String.valueOf(flxHTMLJSON.get("id")))){
							System.out.println("Str_checkbox.equals(fieldObj)==========>>>>>>>>>>>"+String.valueOf(flxHTMLJSON.get("id")));
							//ArrayList  pk_NIhGrants = studyNIHGrantDao.getPkStudyNIhGrants();
							System.out.println("pk_NIhGrants.size()===+++====+++"+pk_NIhGrants.size());
							
							if(pk_NIhGrants.size()>0){
								System.out.println("pk_NIhGrants.size()+++++===="+pk_NIhGrants.size());
								//FlxPageFields field = FlxPageFields.getFlxPageFieldByKey((String)sectionTitle.get(FLX_FIELD_ID_STR));
								JSONObject fieldJson1 = createJsonForNihGrantInformation(pk_NIhGrants,fund_mechsms,inst_codes,serial_numbers,program_codes);
								System.out.println("after createJsonForNihGrantInformation call");

								if (fieldJson1 != null) {
									System.out.println("inside null check");
									fieldJson1.put(TYPE_STR, STD_STR);
									fieldsOutputArray.put(fieldJson1);
								}
							}
						}
					} else if (MSD_STR.equals(fieldObj.get(TYPE_STR))) {
						JSONObject fieldJson = createPageFieldJsonMsd((String)fieldObj.get(FLX_FIELD_ID_STR), studyIdDao);
						if (fieldJson != null) {
							fieldJson.put(TYPE_STR, MSD_STR);
							fieldsOutputArray.put(fieldJson);
						}
					} else if (STUDYSECTION_STR.equals(fieldObj.get(TYPE_STR))) {
						JSONObject fieldJson = createPageFieldJsonStudySection((String)fieldObj.get(FLX_FIELD_ID_STR), smap);
						if (fieldJson != null) {
							fieldJson.put(TYPE_STR, STUDYSECTION_STR);
							fieldsOutputArray.put(fieldJson);
						}
					} 
					
					
					
				}
					}
				}
				System.out.println("Before if LC.L_IndIde_Info------------>");
				System.out.println("LC.L_IndIde_Info.equals(sectionTitle)------------>"+LC.L_IndIde_Info.equals(sectionTitle));
				if(LC.L_IndIde_Info.equals(sectionTitle)) {
					System.out.println("Before if LC.L_IndIde_Info111111------------>");
					System.out.println("LC.L_IndIde_Info.equals(sectionTitle)2222222------------>"+LC.L_IndIde_Info.equals(sectionTitle));
					if(arList.size() > 0) {
						JSONObject fieldJson = createPageFieldJsonForIndIde(indideList);
						if (fieldJson != null) {
							fieldJson.put(TYPE_STR, "indide");
							fieldsOutputArray.put(fieldJson);
						}
					}
				}
				System.out.println("Before if------------>");
				System.out.println("after if L_Loc_Steam_Collaborators------------>"+LC.L_Loc_Steam_Collaborators.trim());
				System.out.println("after if sectionTitle.trim()------------>"+sectionTitle.trim());
				if(LC.L_Loc_Steam_Collaborators.trim().equals(sectionTitle.trim())) {
					System.out.println("after if L_Loc_Steam_Collaborators33333------------>"+LC.L_Loc_Steam_Collaborators.trim());
					System.out.println("after if sectionTitle.trim()44444------------>"+sectionTitle.trim());
					if(arList.size() > 0) {
						JSONObject fieldJson = createPageFieldJsonForStudyTeam(arList);
						if (fieldJson != null) {
							fieldJson.put(TYPE_STR, "steam");
							fieldsOutputArray.put(fieldJson);
						}
					}
				}
				
				JSONObject sectionOutput = new JSONObject();
				sectionOutput.put(TITLE_STR, sectionTitle);
				sectionOutput.put(FIELDS_STR, fieldsOutputArray);
				sectionOutputArray.put(sectionOutput);
			}
			outputObj.put(SECTIONS_STR, sectionOutputArray);

			JSONObject versionDetailsJSON = new JSONObject();
			try {
				versionDetailsJSON.put("majorVersion", version);
				versionDetailsJSON.put("minorVersion", minorVer);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			JSONObject nextVersionDetailsJSON = getThisVersionNumber(versionDetailsJSON);

			String fullVersion = EMPTY_STRING;
			try {
				fullVersion = nextVersionDetailsJSON.getString("fullVersion");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			outputObj.put(VERSION_STR, fullVersion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		//System.out.println(outputObj.toString());
		return outputObj;
	}
	
	public void setFormResponseStatus(int formId, int studyId, int userId, String formResponseStatusSubtype){
		CodeDao cdFormRespStatusDao = new CodeDao();
		int formResponseStatusPK = cdFormRespStatusDao.getCodeId("fillformstat", formResponseStatusSubtype);
		if (formResponseStatusPK < 1) return;
		setFormResponseStatus(formId, studyId, userId, formResponseStatusPK);
	}

	public void setFormResponseStatus(int formId, int studyId, int userId, int formResponseStatusPK){
		FormResponseDao formResponseDao = new FormResponseDao();
		LinkedFormsDao lfDao = new LinkedFormsDao();
		int isIRB = lfDao.checkIsIRBform(formId);
		if(isIRB==1){
			formResponseDao.setStudyFormResponeStatus(formId, studyId, userId, formResponseStatusPK);
		}
	}

	
	public void freezeAttachments(int studyId, HashMap paramMap, double versionNum){
		int userId = StringUtil.stringToNum((paramMap.get("userId")).toString());
		if (studyId < 1 || userId < 1 || versionNum < 1){ return; }

		// Freeze all categories of this version of this study
	    StudyVerJB studyVerJB = new StudyVerJB();
	    StudyVerDao studyVerDao = studyVerJB.getAllVers(studyId);
	    ArrayList studyVerIds = studyVerDao.getStudyVerIds();
	    ArrayList studyVerNumbers = studyVerDao.getStudyVerNumbers();
	    // Get a codelst item for Freeze status
	    CodeDao statDao = new CodeDao();
	    int freezeStat = statDao.getCodeId(VERSION_STATUS, LETTER_F);
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(Configuration.getAppDateFormat()+" HH:mm:ss");
		
    	if( versionNum > 0 ) {
	    	double vNum = Double.valueOf(versionNum).doubleValue();
		    for (int iX = 0; iX < studyVerNumbers.size(); iX++) {
		    	String svNumStr = (String)studyVerNumbers.get(iX);
		    	double svNum = Double.valueOf(svNumStr).doubleValue();
    			if( svNum == vNum) {
		    		// add a Freeze row to ER_STATUS_HISTORY
		    		StatusHistoryJB statusHistoryJB = new StatusHistoryJB();
		    		statusHistoryJB.setStatusModuleTable(ER_STUDYVER_TABLE);
		    		int modulePk = (Integer)studyVerIds.get(iX);
		    		statusHistoryJB.setStatusModuleId(String.valueOf(modulePk));
		    		statusHistoryJB.setStatusCodelstId(String.valueOf(freezeStat));
		    		statusHistoryJB.setStatusStartDate(sdf.format(cal.getTime()));
		    		statusHistoryJB.setStatusEnteredBy(""+userId);
		    		statusHistoryJB.setCreator(""+userId);
		    		statusHistoryJB.setStatusHistoryDetails();
		    		// update version status to "F"( freeze) in er_studyver table
		    		studyVerDao.updateVersionStatus(svNumStr, studyId, modulePk, "F",userId);		    
    			}
	    	}
	    }
	}

	public float unfreezeProtocol (HashMap<String, Object> paramMap){
		int retInt = -1;
		HttpServletRequest request = (HttpServletRequest) paramMap.get("request"); if (null == request) return retInt;
		SessionMaint sessionMaint = new SessionMaint();
		HttpSession tSession =  request.getSession(true); if (null == tSession || !sessionMaint.isValidSession(tSession)) return retInt;

		String studyIdStr = (String) tSession.getAttribute("studyId");
		int studyId = StringUtil.stringToNum(studyIdStr);
		if (studyId <= 0) return retInt;

		String defUserGroup = (String) tSession.getAttribute("defUserGroup");
		StudyIdDao sidDao = new StudyIdDao();
		sidDao.getStudyIds(studyId, defUserGroup);

    	ResubmDraftDao resubmDraftDao = new ResubmDraftDao();
    	resubmDraftDao.getHighestResubmDraftFullVersion("er_study", studyId);
    	int majorVerNum = resubmDraftDao.getMajorVer(); //resubmDraftDao.getHighestResubmDraftVersion("er_study",  studyJB.getId());	    
	    int minorVerNum = resubmDraftDao.getMinorVer(); //resubmDraftDao.getHighestMinorResubmDraftVersion(majorVerNum, "er_study",  studyJB.getId());
	    
	    JSONObject versionDetailsJSON = new JSONObject();
		try {
			versionDetailsJSON.put("majorVersion", resubmDraftDao.getMajorVer());
			versionDetailsJSON.put("minorVersion", resubmDraftDao.getMinorVer());
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JSONObject nextVersionDetailsJSON = getThisVersionNumber(versionDetailsJSON);

		String fullVersion = null;
		try {
			fullVersion = nextVersionDetailsJSON.getString("fullVersion");
		} catch (JSONException e) {
			e.printStackTrace();
		}
	    double versionNum = Double.valueOf(fullVersion).doubleValue();
	    
	    /* Unfreeze attachments */
	    this.unfreezeAttachments(studyId, paramMap, versionNum);

    	/* Form unlocking. */
    	paramMap.put(SOURCE_STR, DRAFT_STR);
    	this.unfreezeFormResponses(studyId, paramMap, versionNum);

	    //Locking the Study as Version is frozen
	    StudyDao studyDao = new StudyDao();
	    studyDao.lockUnlockStudy(studyId, 0);
		return StringUtil.stringToFloat(fullVersion);
	}

	private void unfreezeAttachments(int studyId, HashMap paramMap, double versionNum){
		int userId = StringUtil.stringToNum((paramMap.get("userId")).toString());
		if (studyId < 1 || userId < 1 || versionNum < 1){ return; }

		// Unfreeze all categories of this version of this study
	    StudyVerJB studyVerJB = new StudyVerJB();
	    StudyVerDao studyVerDao = studyVerJB.getAllVers(studyId);
	    ArrayList studyVerIds = studyVerDao.getStudyVerIds();
	    ArrayList studyVerNumbers = studyVerDao.getStudyVerNumbers();
	    // Get a codelst item for Freeze status
	    CodeDao statDao = new CodeDao();
	    int freezeStat = statDao.getCodeId(VERSION_STATUS, LETTER_W);
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(Configuration.getAppDateFormat());
		
    	if( versionNum > 0 ) {
	    	double vNum = Double.valueOf(versionNum).doubleValue();
		    for (int iX = 0; iX < studyVerNumbers.size(); iX++) {
		    	String svNumStr = (String)studyVerNumbers.get(iX);
		    	double svNum = Double.valueOf(svNumStr).doubleValue();
    			if( svNum == vNum) {
		    		// add a Freeze row to ER_STATUS_HISTORY
		    		int modulePk = (Integer)studyVerIds.get(iX);
		    		String moduleTable = "er_studyver";
		    		studyVerDao.deleteFreezeStatus(modulePk,moduleTable,userId);
		    		// update version status to "W"(work in progress) in er_studyver table
		    		studyVerDao.updateVersionStatus(svNumStr, studyId, modulePk, "W",userId);
    			}
	    	}
	    }
	}
	
	public void freezeFormResponses(int studyId, HashMap paramMap, double versionNum){
		if (studyId < 1 || paramMap.isEmpty()) return;

		ArrayList wfFormIds = new ArrayList();
		String [] wfKeys = CFG.Workflow_FlexStudy_WfTypes.split(",");
		for (int wfIndx = 0; wfIndx < wfKeys.length; wfIndx++){
			WorkflowDAO wfDao = new WorkflowDAO();
			ArrayList wflowFormIds = wfDao.getWorkflowInstanceForms(""+studyId, wfKeys[wfIndx]);
			if (null != wflowFormIds && wflowFormIds.size() > 0){
				wfFormIds.addAll(wflowFormIds);
			}
		}
		
		CodeDao cdFormRespStatusDao = new CodeDao();
		int lockedFormStatusPK = cdFormRespStatusDao.getCodeId("fillformstat", "lockdown");

		int accountId = StringUtil.stringToNum((paramMap.get("accountId")).toString());
		int userId = StringUtil.stringToNum((paramMap.get("userId")).toString());
		
		if (accountId > 0 && userId > 0){
			//Lock the response here
			this.lockUnlockFormResponses(wfFormIds, studyId, userId, lockedFormStatusPK);
		}
	}
	
	public void unfreezeFormResponses(int studyId, HashMap paramMap){
		// -1 indicates that highest version is unknown to calling page hence find it first
		this.unfreezeFormResponses(studyId, paramMap, -1);
	}

	public void unfreezeFormResponses(int studyId, HashMap paramMap, double versionNum){
		if (studyId < 1 || paramMap.isEmpty()) return;
		FormResponseDao fdao = new FormResponseDao();
		fdao.unlockFormResponse(studyId);
		/*String source = paramMap.get(SOURCE_STR).toString();
		if (StringUtil.isEmpty(source)) return;

		String pageFormResponses = "";
		// Here retrieve the latest flx page version form responses
		if (VERSION_STR.equals(source)){
			UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
			String flxFullVersion = null;
			if (-1 == versionNum){
				// -1 indicates that highest version is unknown to calling page hence find it first
				uiFlxPageDao.getHighestFlexPageFullVersion(ER_STUDY_STR, studyId);
				flxFullVersion = uiFlxPageDao.getMajorVer() + "." + uiFlxPageDao.getMinorVer();
			} else {
				flxFullVersion = String.valueOf(versionNum);
			}
			pageFormResponses = uiFlxPageDao.getPageVersionFormResponses(ER_STUDY_STR, studyId, flxFullVersion);
		}

		// Here retrieve the latest re-submission draft form responses
		if (DRAFT_STR.equals(source)){
			ResubmDraftDao resubmDraftDao = new ResubmDraftDao();
			String resubmFullVersion = null;
			if (-1 == versionNum){
				// -1 indicates that highest version is unknown to calling page hence find it first
				resubmDraftDao.getHighestResubmDraftFullVersion(ER_STUDY_STR, studyId);
				resubmFullVersion = resubmDraftDao.getMajorVer() + "." + resubmDraftDao.getMinorVer();
			} else {
				resubmFullVersion = String.valueOf(versionNum);
			}
			pageFormResponses = resubmDraftDao.getResubmDraftFormResponses(ER_STUDY_STR, studyId, resubmFullVersion);
		}

		int accountId = StringUtil.stringToNum((paramMap.get("accountId")).toString());
		int userId = StringUtil.stringToNum((paramMap.get("userId")).toString());
		
		CodeDao cdFormRespStatusDao = new CodeDao();
		int wipFormStatusPK = cdFormRespStatusDao.getCodeId("fillformstat", "working");
		
		if (accountId > 0 && userId > 0 && !StringUtil.isEmpty(pageFormResponses)){
			JSONObject jsFormRespJSON = null;
			try {
				jsFormRespJSON = new JSONObject(pageFormResponses);
				Iterator formRespIterator = jsFormRespJSON.keys();
				while (formRespIterator.hasNext()) {
					String formNameKey = (formRespIterator.next()).toString();
					JSONObject jsFormRespObj =  jsFormRespJSON.getJSONObject(formNameKey);
					String formIdStr = jsFormRespObj.getString("formId");
					if (StringUtil.isEmpty(formIdStr)){ continue; }

					String formResponseIdStr = jsFormRespObj.getString("formRespId");
					if (StringUtil.isEmpty(formResponseIdStr)) { continue; }

					String formRespStatus = jsFormRespObj.getString("formRespStatus");
					formRespStatus = (StringUtil.isEmpty(formRespStatus)) ? "working" : formRespStatus;

					int unlockedFormStatusPK = 0;
					if ("working".equals(formRespStatus) || "lockdown".equals(formRespStatus)){
						unlockedFormStatusPK = wipFormStatusPK;
					} else {
						cdFormRespStatusDao = new CodeDao();
						unlockedFormStatusPK = cdFormRespStatusDao.getCodeId("fillformstat", formRespStatus);
						unlockedFormStatusPK = (unlockedFormStatusPK <= 0) ? wipFormStatusPK : unlockedFormStatusPK;
					}
					//Unlock the response here
					this.setFormResponseStatus(StringUtil.stringToNum(formIdStr), studyId, userId, unlockedFormStatusPK);
				}
			} catch (JSONException e) {
				e.printStackTrace();
				return;
			}
		}*/
	}

	private void lockUnlockFormResponses (ArrayList wfFormIds, int studyId, int userId, int formResponseStatusPK){
		for (int formIndx = 0; formIndx < wfFormIds.size(); formIndx++){
			int formId = StringUtil.stringToNum((wfFormIds.get(formIndx)).toString());
			
			if (formId <= 0){ continue; }
			Hashtable htForResp = LinkedFormsDao.getLatestStudyFormResponeData(formId, studyId);
			if (null != htForResp){
				if (!htForResp.containsKey("pkstudyforms")){ continue; }
				int formResponseId = StringUtil.stringToNum(htForResp.get("pkstudyforms").toString());
				if (formResponseId <= 0){ continue;	}
				
				int formStatusPK = StringUtil.stringToNum(htForResp.get("form_completed").toString());
				if (formStatusPK != formResponseStatusPK){
					//Lock the response here if not locked already
					this.setFormResponseStatus(formId, studyId, userId, formResponseStatusPK);
				}
			}
		}
	}

	public JSONObject createFormResponseArchiveJson(StudyJB studyJB, HashMap paramMap) {
		ArrayList wfFormIds = new ArrayList();
		String [] wfKeys = CFG.Workflow_FlexStudy_WfTypes.split(",");
		int studyId = studyJB.getId();
		for (int wfIndx = 0; wfIndx < wfKeys.length; wfIndx++){
			WorkflowDAO wfDao = new WorkflowDAO();
			ArrayList wflowFormIds = wfDao.getWorkflowInstanceForms(""+studyId, wfKeys[wfIndx]);
			if (null != wflowFormIds && wflowFormIds.size() > 0){
				wfFormIds.addAll(wflowFormIds);
			}
		}
		
		CodeDao cdFormRespStatusDao = new CodeDao();
		int lockedFormStatusPK = cdFormRespStatusDao.getCodeId("fillformstat", "lockdown");

		JSONObject jsPageFormResponseJSON = new JSONObject();
		int accountId = StringUtil.stringToNum((paramMap.get("accountId")).toString());
		int userId = StringUtil.stringToNum((paramMap.get("userId")).toString());
		
		if (accountId > 0 && userId > 0){
			for (int formIndx = 0; formIndx < wfFormIds.size(); formIndx++){
				int formId = StringUtil.stringToNum((wfFormIds.get(formIndx)).toString());
				if (formId <= 0){ continue; }
				
				
				JSONObject formRespJSON = new JSONObject();
				FormLibDao formDao = new FormLibDao();

				formDao.getPBFormNames(accountId, formId);
				String formName = (formDao.getFormNames().get(0).toString());
				
				Hashtable htForResp = LinkedFormsDao.getLatestStudyFormResponeData(formId, studyId);
				if (null != htForResp && htForResp.size()>0){
					try {
						formRespJSON.put("formId", formId);
						formRespJSON.put("formName", formName);
						if (!htForResp.containsKey("pkstudyforms")){ continue; }
						int formResponseId = StringUtil.stringToNum(htForResp.get("pkstudyforms").toString());
						if (formResponseId <= 0){ continue;	}
						formRespJSON.put("formRespId", formResponseId);
						
						int formStatusPK = StringUtil.stringToNum(htForResp.get("form_completed").toString());

						cdFormRespStatusDao = new CodeDao();
						formRespJSON.put("formRespStatus", cdFormRespStatusDao.getCodeSubtype(formStatusPK));
						
						//System.out.println(htForResp.get("studyforms_xml").toString());
						JSONObject jsXMLObj = EJBUtil.xmlToJSON(htForResp.get("studyforms_xml").toString());
						jsXMLObj = jsXMLObj.getJSONObject("rowset");
						//System.out.println(jsXMLObj);
						Iterator<?> fields = jsXMLObj.keys();
						
						while (fields.hasNext()){
							String fieldId = (String)fields.next();
						    if ( jsXMLObj.get(fieldId) instanceof JSONObject ) {
						    	JSONObject jsFieldObj = jsXMLObj.getJSONObject(fieldId);
						    	String fldKeyword = EMPTY_STRING;
						    	try{
						    		fldKeyword = jsFieldObj.getString("keyword");
						    	} catch (JSONException e){
						    		fldKeyword = null;
						    	}

						    	try{
						    		fldKeyword = (StringUtil.isEmpty(fldKeyword))? jsFieldObj.getString("uid") : fldKeyword;
						    	} catch (JSONException e){
						    		fldKeyword = null;
						    	}

						    	if (StringUtil.isEmpty(fldKeyword)) { continue; }
						    	jsFieldObj.put("keyword", fldKeyword);

						    	jsFieldObj.put("label", jsFieldObj.getString("name"));

						    	String fldSection = EMPTY_STRING;
						    	try{
						    		fldSection = jsFieldObj.getString("secname");
						    	} catch (JSONException e){
						    		fldSection = null;
						    	}
						    	jsFieldObj.put("title", fldSection);
						    	
						    	jsFieldObj.remove("name");
						    	jsFieldObj.remove("uid");
						    	jsFieldObj.remove("origsysid");
						    	jsFieldObj.remove("checkboxesdata");
						    	jsFieldObj.remove("keyword");
						    	jsFieldObj.remove("pkfld");
						    	jsFieldObj.remove("browser");
						    	jsFieldObj.remove("colcount");
						    	jsFieldObj.remove("fldcharsno");
						    	jsFieldObj.remove("pksec");
						    	jsFieldObj.remove("linesno");
						    	jsFieldObj.remove("systemid");
						    	jsFieldObj.remove("secname");
						    	
						    	String fieldValue = EMPTY_STRING;
						    	String fldType = jsFieldObj.getString("type");
						    	jsFieldObj.remove("type");

						    	if ("MD".equals(fldType)){
							    	if ( jsFieldObj.get("resp") instanceof JSONArray ) {
							    		JSONArray jsFldRespArray = jsFieldObj.getJSONArray("resp");
							    		for (int respIndx = 0; respIndx < jsFldRespArray.length(); respIndx++){
							    			try {
							    				JSONObject jsFldRespObj = jsFldRespArray.getJSONObject(respIndx);
							    				String selected = jsFldRespObj.getString("selected");
							    				/*jsFldRespObj.remove("selected");
							    				jsFldRespObj.remove("no");
							    				jsFldRespObj.remove("score");
							    				jsFldRespObj.remove("respcount");
							    				jsFldRespObj.remove("no");*/
							    				if (StringUtil.stringToNum(selected) > 0){
							    					fieldValue = jsFldRespObj.getString("dispval");
							    					break;
							    				}
							    			} catch (JSONException jsE1){
							    				fieldValue = EMPTY_STRING;
							    			}
							    		}
							    		jsFieldObj.remove("resp");
							    	}
						    	} else if ("MC".equals(fldType)){
							    	if ( jsFieldObj.get("resp") instanceof JSONArray ) {
							    		JSONArray jsFldRespArray = jsFieldObj.getJSONArray("resp");
							    		for (int respIndx = 0; respIndx < jsFldRespArray.length(); respIndx++){
							    			try {
							    				JSONObject jsFldRespObj = jsFldRespArray.getJSONObject(respIndx);
							    				String checked = jsFldRespObj.getString("checked");
							    				/*jsFldRespObj.remove("selected");
							    				jsFldRespObj.remove("no");
							    				jsFldRespObj.remove("score");
							    				jsFldRespObj.remove("respcount");
							    				jsFldRespObj.remove("no");*/
							    				if (StringUtil.stringToNum(checked) > 0){
							    					fieldValue += ((respIndx == 0)? "" : ",") + jsFldRespObj.getString("dispval");
							    				}
							    			} catch (JSONException jsE1){
							    				fieldValue = EMPTY_STRING;
							    			}
							    		}
							    		jsFieldObj.remove("resp");
							    	}
						    	} else if ("MR".equals(fldType)){
							    	if ( jsFieldObj.get("resp") instanceof JSONArray ) {
							    		JSONArray jsFldRespArray = jsFieldObj.getJSONArray("resp");
							    		for (int respIndx = 0; respIndx < jsFldRespArray.length(); respIndx++){
							    			try {
							    				JSONObject jsFldRespObj = jsFldRespArray.getJSONObject(respIndx);
							    				String checked = jsFldRespObj.getString("checked");
							    				/*jsFldRespObj.remove("selected");
							    				jsFldRespObj.remove("no");
							    				jsFldRespObj.remove("score");
							    				jsFldRespObj.remove("respcount");
							    				jsFldRespObj.remove("no");*/
							    				if (StringUtil.stringToNum(checked) > 0){
							    					fieldValue = jsFldRespObj.getString("dispval");
							    				}
							    			} catch (JSONException jsE1){
							    				fieldValue = EMPTY_STRING;
							    			}
							    		}
							    		jsFieldObj.remove("resp");
							    	}
						    	} else {
						    		try{
						    			fieldValue = jsFieldObj.getString("content");
						    			if ("ET".equals(fldType) && "er_textarea_tag".equals(fieldValue)){
						    				fieldValue = EMPTY_STRING;
						    			}
						    			jsFieldObj.remove("content");
						    		} catch (JSONException jsE){
						    			fieldValue = EMPTY_STRING;
						    		}
						    	}
						    	jsFieldObj.put("data", fieldValue);
						    	jsXMLObj.put(fieldId, jsFieldObj);
						    }
						}
						formRespJSON.put("formRespData", jsXMLObj);
						
						if (formStatusPK != lockedFormStatusPK){
							//Lock the response here if not locked already
							this.setFormResponseStatus(formId, studyId, userId, lockedFormStatusPK);
						}
						jsPageFormResponseJSON.put(StringUtil.replace(formName, " ", "_"), formRespJSON);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			System.out.print("Error! accountId and/or user Id not set in paramMap");
			jsPageFormResponseJSON = null;
		}
		return jsPageFormResponseJSON;
	}

	public int chkPageProtocolFrozen(int studyId) {
		ComplianceAgent compAgent = (ComplianceAgent)EJBUtil.getComplianceAgentHome();		
		List<StudyVerBean> studyVerList = compAgent.getHighestStudyVersions(studyId);
		int majorVer = 1, minorVer = 0;
		if (studyVerList != null && studyVerList.size() > 0) {
			StudyVerBean highVerBean = studyVerList.get(0);
			String highVerStr = highVerBean.getStudyVerNumber();
			if (highVerStr != null) {
				String[] parts = highVerStr.split("\\.");
				if (parts != null && parts.length > 1) {
					try {
						majorVer = Integer.parseInt(parts[0]);
						minorVer = Integer.parseInt(parts[1]);
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		int isProtocolFrozen = -1;
		ResubmDraftDao resubmDraftDao = new ResubmDraftDao();
		isProtocolFrozen = resubmDraftDao.chkPageProtocolFrozen("er_study", studyId, majorVer, minorVer);
		System.out.println("majorVer="+majorVer+" minorVer="+minorVer+" isProtocolFrozen="+isProtocolFrozen);
		return isProtocolFrozen;
	}
	
	public int chkPageVersionSubmited(HashMap paramMap) {
		int studyId = (Integer)paramMap.get("studyId");
		/*ComplianceAgent compAgent = (ComplianceAgent)EJBUtil.getComplianceAgentHome();		
		List<StudyVerBean> studyVerList = compAgent.getHighestStudyVersions(studyId);
		int attachMajorVer = 1, attachMinorVer = 0;
		if (studyVerList != null && studyVerList.size() > 0) {
			StudyVerBean highVerBean = studyVerList.get(0);
			String highVerStr = highVerBean.getStudyVerNumber();
			if (highVerStr != null) {
				String[] parts = highVerStr.split("\\.");
				if (parts != null && parts.length > 1) {
					try {
						attachMajorVer = Integer.parseInt(parts[0]);
						attachMinorVer = Integer.parseInt(parts[1]);
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		}*/

		ComplianceJB compJB = new ComplianceJB();
		String curStatus = compJB.getCurrentStudyStatus(paramMap);
		int versionSubmitted = -1;
		UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
		uiFlxPageDao.getHighestFlexPageFullVersion("er_study", studyId);
		int flxPageVerMajor = uiFlxPageDao.getMajorVer(), flxPageVerMinor = uiFlxPageDao.getMinorVer();
		
		int rowExists = uiFlxPageDao.checkLockedVersionExists(studyId, flxPageVerMajor);
	    if (rowExists < 1) {
	    	ResubmDraftDao resubmDraftDao = new ResubmDraftDao();
	    	rowExists = resubmDraftDao.checkIrbApprovedExists(studyId, flxPageVerMajor);
	    }

		JSONObject versionDetailsJSON = new JSONObject();
	    try {
			versionDetailsJSON.put("majorVersion", flxPageVerMajor);
			versionDetailsJSON.put("minorVersion", flxPageVerMinor);
			versionDetailsJSON.put("rowExists", rowExists);
			versionDetailsJSON.put("curStatus", curStatus);
		} catch (JSONException e) {
			e.printStackTrace();
		}

	    int nextVersionNum = 0, nextMinorVerNum = 0;
	    String nextFlxPageFullVersionNum = EMPTY_STRING;
	    JSONObject nextVersionDetailsJSON = getNextVersionNumber(versionDetailsJSON);
	    try {
			nextVersionNum = nextVersionDetailsJSON.getInt("nextMajorVersion");
			nextMinorVerNum = nextVersionDetailsJSON.getInt("nextMinorVersion");
			nextFlxPageFullVersionNum = nextVersionDetailsJSON.getString("nextFullVersion");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		versionSubmitted = uiFlxPageDao.chkPageVersionSubmited("er_study", studyId, nextVersionNum, nextMinorVerNum);
		System.out.println("majorVer="+nextVersionNum+" minorVer="+nextMinorVerNum+" versionSubmitted="+versionSubmitted);
		return versionSubmitted;
	}
	public int chkPageSubmitStatus(int studyId) {
		System.out.println("chkPageSubmitStatus:::");
		UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
		ComplianceAgent compAgent = (ComplianceAgent)EJBUtil.getComplianceAgentHome();		
		List<StudyVerBean> studyVerList = compAgent.getHighestStudyVersions(studyId);
		int majorVer = 1, minorVer = 0, versionSubmitted =-1;
		if (studyVerList != null && studyVerList.size() > 0) {
			StudyVerBean highVerBean = studyVerList.get(0);
			String highVerStr = highVerBean.getStudyVerNumber();
			if (highVerStr != null) {
				String[] parts = highVerStr.split("\\.");
				if (parts != null && parts.length > 1) {
					try {
						majorVer = Integer.parseInt(parts[0]);
						minorVer = Integer.parseInt(parts[1]);
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		}

		versionSubmitted = uiFlxPageDao.chkPageVersionSubmited("er_study", studyId, majorVer, minorVer);
		System.out.println("majorVer="+majorVer+" minorVer="+minorVer+" versionSubmitted="+versionSubmitted);
		return versionSubmitted;
	}
	private JSONObject createPageFieldJsonMsd(String fldCodeSubtype, StudyIdDao sidDao) {
		if (null == sidDao) return null;

		ArrayList alternateIdKey = sidDao.getAlternateIdKey();
		if (null == alternateIdKey) return null;
		int counter = alternateIdKey.indexOf(fldCodeSubtype);
		if (counter < 0) return null;

		ArrayList studyIdType = sidDao.getStudyIdType();
		ArrayList id = sidDao.getId();

		ArrayList studyIdTypesDesc = sidDao.getStudyIdTypesDesc();
		ArrayList alternateId = sidDao.getAlternateId();
		ArrayList recordType = sidDao.getRecordType ();
		ArrayList dispTypes = sidDao.getDispType();
		ArrayList dispData = sidDao.getDispData();
		String strStudyIdTypesDesc = (String) studyIdTypesDesc.get(counter);
		String strAlternateId = (String) alternateId.get(counter);
		int intStudyIdType = (Integer) studyIdType.get(counter) ; 
		String disptype=(String) dispTypes.get(counter);
		String dispdata=(String) dispData.get(counter);
		if (disptype==null) disptype = EMPTY_STRING;
		disptype=disptype.toLowerCase();
		if (dispdata==null) dispdata = EMPTY_STRING;
		if (disptype.equals(Str_dropdown)) {
			if (null != dispdata) {
				dispdata = StringUtil.replace(dispdata, "alternateId", "alternateId"+intStudyIdType);
			}
		}
		if (strAlternateId == null) strAlternateId = EMPTY_STRING;
		String strRecordType = (String) recordType.get(counter);
		// System.out.println("fldCodePKey="+fldCodePKey+" strStudyIdTypesDesc="+strStudyIdTypesDesc);
		int cbcount=0;
		int intId = (Integer) id.get(counter);
		JSONObject json = new JSONObject();
		try {
			json.put(LABEL_STR, strStudyIdTypesDesc);
			if (Str_Hidden_input.equals(disptype)) {
				System.out.println("intId="+intId+" studyIdType="+intStudyIdType);
			}
			else {
				json.put(CODE_SUBTYP_STR, fldCodeSubtype);

				if (Str_chkbox.equals(disptype) || Str_checkbox.equals(disptype)) {
					if(!EJBUtil.isEmpty(dispdata))
					{
						JSONObject chkConfigJSON = new JSONObject(dispdata);
				    	int chkColCnt = 1;
				    	try {
				    		chkColCnt = StringUtil.stringToNum(""+chkConfigJSON.get("chkColCnt"));
				    	}catch(Exception e1){}
				    	StringBuilder description = new StringBuilder();
				    	String delim = "";
				    	try {
				    		JSONArray checkboxArray = chkConfigJSON.getJSONArray("chkArray");
				    
							for (int indx=0; indx < checkboxArray.length(); indx++) {
								JSONObject checkboxJSON = (JSONObject)checkboxArray.get(indx);
						        try{
									String data = checkboxJSON.getString("data");
									String display = checkboxJSON.getString("display");
									String checked = EMPTY_STRING;
									if((","+strAlternateId+",").indexOf(","+data+",") > -1) {
										description.append(delim);
										description.append(display);
										delim = ",";
									}
							 	}catch(Exception e) {}
							}
				    	}catch(Exception e1){}
				    
				    	json.put(DATA_STR, description.toString().trim());
					}
					else
						json.put(DATA_STR, strAlternateId.trim());
				}
				else
				{
					json.put(DATA_STR, strAlternateId.trim());
				}
				return json;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private JSONObject createPageFieldJsonStudySection(String fldCodeSubtype, HashMap<String, String> smap) {
		JSONObject json = new JSONObject();
		CodeDao cDao= new CodeDao();
		int codeId = cDao.getCodeId("section", fldCodeSubtype);
		String description = cDao.getCodeDesc(codeId);

		try {
			json.put(fldCodeSubtype, ((smap.get(description)) == null ? "" : smap.get(description)) );
			return json;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	private JSONObject createPageFieldJsonForIndIde(ArrayList<String> indideList) {
		JSONObject json = new JSONObject();
		String indideStr="[";
		try {
			for(int i=0; i< indideList.size(); i++) {
				//indideStr += (String)indideList.get(i) + " ; ";
				indideStr += (String)indideList.get(i);
				if( i == (indideList.size() - 1)) { } else {
					indideStr += ",";
				}
			}
			indideStr += "]";
			json.put("IND/IDE Details " , indideStr);
			return json;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	private JSONObject createPageFieldJsonForStudyTeam(ArrayList<String> arList) {
		JSONObject json = new JSONObject();
		String steam="[";
		try {
			for(int i=0; i< arList.size(); i++) {
				steam += (String)arList.get(i);
				System.out.println("Team Member--->"+(String)arList.get(i));
				if( i == (arList.size() - 1)) { } else {
					steam += ",";
				}
				
			}
			steam += "]";
			System.out.println("Study Team--->"+steam);
			json.put("Collaborators Details " , steam);
			return json;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	private JSONObject createJsonForNihGrantInformation(ArrayList pk_NIhGrants,ArrayList fund_mechsms,ArrayList inst_codes,ArrayList serial_numbers,
			ArrayList program_codes) {
		
		JSONObject json = new JSONObject();
		String pk_NIhGrantStr="[";
		try {
			
			if(pk_NIhGrants.size()>0){
				for(int k=0;k<pk_NIhGrants.size();k++){
			/*for(int i=0; i< pk_NIhGrants.size(); i++) {*/
				/*pk_NIhGrantStr += String.valueOf(pk_NIhGrants.get(i));
				pk_NIhGrantStr += String.valueOf(fund_mechsms.get(i));
				pk_NIhGrantStr += String.valueOf(inst_codes.get(i));
				pk_NIhGrantStr += String.valueOf(program_codes.get(i));
				pk_NIhGrantStr += String.valueOf(serial_numbers.get(i));*/
					JSONObject nihJSON = new JSONObject();
					CodeDao cDao = new CodeDao();
					String fund_mechsmsStr=cDao.getCodeDescription(Integer.parseInt(String.valueOf(fund_mechsms.get(k))));
					String inst_codesStr=cDao.getCodeDescription(Integer.parseInt(String.valueOf(inst_codes.get(k))));
					String serial_numbersStr=String.valueOf(serial_numbers.get(k));
					String program_codesStr=cDao.getCodeDescription(Integer.parseInt(String.valueOf(program_codes.get(k))));
					System.out.println("fund_mechsms==="+fund_mechsmsStr);
					System.out.println("inst_codes==="+inst_codesStr);
					System.out.println("program_codes==="+program_codesStr);
					System.out.println("serial_numbers==="+serial_numbersStr);
					nihJSON.put("fund_mechsms", fund_mechsmsStr);
					nihJSON.put("inst_codes", inst_codesStr);
					nihJSON.put("serial_numbers", serial_numbersStr);
					nihJSON.put("program_codes", program_codesStr);
					System.out.println("nihJSON==="+nihJSON.toString());
				if( k == (pk_NIhGrants.size() - 1)) {
					pk_NIhGrantStr += nihJSON.toString();
					System.out.println("json=============inside if========================");
				} else {
					pk_NIhGrantStr += nihJSON.toString()+",";
					System.out.println("json=================inside else====================");
				}
				
				}
			
			}
			pk_NIhGrantStr += "]";
			json.put("STUDY_GRANT_NIH_INFORMATION" , pk_NIhGrantStr);
			System.out.println("json===============ytugigjgo======================"+json.toString());
			return json;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	private JSONObject createPageFieldJsonStd(FlxPageFields field, StudyJB studyJB) {
		if (studyJB == null) { return null; }
		try {
			switch(field) {
			case STUDY_STUDYNUMBER:
				return doStudyNumber(field, studyJB);
			case STUDY_STUDYTITLE:
				return doStudyTitle(field, studyJB);
			case STUDY_DISEASESITES:
				return doStudyDiseaseSites(field, studyJB);
			case STUDY_DATAMANAGER:
				return doStudyDataManager(field, studyJB);
			case STUDY_OBJECTIVE:
				return doStudyObjective(field, studyJB);
			case STUDY_STUDYSUMM:
				return doStudySummary(field, studyJB);
			case STUDY_PIAUTHOR:
				return doStudyPiAuthor(field, studyJB);
			case STUDY_PIOTHER:
				return doStudyPiOther(field, studyJB);
			case STUDY_STUDYCONTACT:
				return doStudyStudyContact(field, studyJB);
			case STUDY_PINVCHECK:
				return doStudyCheck(field, studyJB);
			case STUDY_CRTP_REPORTABLE:
				return doStudyCtrpReportable (field, studyJB);
			case STUDY_FDA_REGULATED:
				return doStudyFdaRegulated(field, studyJB);
			case STUDY_NCTNUMBER:
				return doStudyNctNumber(field, studyJB);
			case STUDY_PPURPOSE:
				return doStudyPurpose(field, studyJB);
			case STUDY_TAREA:
				return doStudyTArea(field, studyJB);
			case STUDY_AGENTDEVICE:
				return doStudyAgentDevice(field, studyJB);
			case STUDY_SPECIFICSITES1:
				return doStudySpecificSites1(field, studyJB);
			case STUDY_SPECIFICSITES2:
				return doStudySpecificSites2(field, studyJB);
			case STUDY_SAMPLESIZE:
				return doStudySampleSize(field, studyJB);
			case STUDY_STUDYDURN:
				return doStudyDuration(field, studyJB);
			case STUDY_STUDYDURN_DROP:
				return doStudyDurationDropDown(field, studyJB);
			case STUDY_DIVISION:
				return doStudyDivision(field, studyJB);
			case STUDY_CCSG:
				return doStudyCcsgReportable(field, studyJB);
			case STUDY_ESTIMATEDBEGINDATE:
				return doStudyEstimatedBeginDate(field, studyJB);
			case STUDY_PHASE:
				return doStudyPhase(field, studyJB);
			case STUDY_RESEARCHTYPE:
				return doStudyResearchType(field, studyJB);
			case STUDY_STUDYSCOPE:
				return doStudyScope(field, studyJB);
			case STUDY_STUDYTYPE:
				return doStudyType(field, studyJB);
			case STUDY_LINKEDTO:
				return doStudyLinkedTo(field, studyJB);
			case STUDY_BLINDING:
				return doStudyBlinding(field, studyJB);
			case STUDY_RANDOMIZATION:
				return doStudyRandomization(field, studyJB);
			case STUDY_SPONSOR:
				return doStudySponsor(field, studyJB);
			case STUDY_SPONSOR_IFOTHER:
				return doStudySponsorIfOther(field, studyJB);
			case STUDY_SPONSORID:
				return doStudySponsorId(field, studyJB);
			case STUDY_CONTACT:
				return doStudyContact(field, studyJB);
			case STUDY_KEYWORDS:
				return doStudyKeywords(field, studyJB);
			case STUDY_NCITrialIdentifier:
	            return doStudyNciIdentifier(field, studyJB);//Kavitha : #31069
			 case STUDY_GRANT_NIH_INFORMATION:
		            return doStudyGrantNihInformation(field, studyJB);
			default:
			}
		} catch(JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private JSONObject doStudyNumber(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		json.put(field.getFlxPageFieldKey(), studyJB.getStudyNumber());
		return json;
	}
	
	private JSONObject doStudyTitle(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		json.put(field.getFlxPageFieldKey(), studyJB.getStudyTitle());
		return json;
	}
	
	private JSONObject doStudyDiseaseSites(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = EMPTY_STRING;
		if(studyJB != null && studyJB.getDisSite() != null){
			StringBuffer sb1 = new StringBuffer();
			String[] pkDisease = studyJB.getDisSite().split(",");
			CodeDao cdDao = new CodeDao();
			for (int iX = 0; iX < pkDisease.length; iX++) {
				int pkDis = 0;
				try {
					pkDis = Integer.parseInt(pkDisease[iX]);
					if (pkDis < 1) { continue; }
					sb1.append(cdDao.getCodeDescription(pkDis)).append(";");
				} catch(Exception e) {}
			}
			if (sb1.length() > 0) {
				sb1.deleteCharAt(sb1.length()-1);
			}
			fldValue = sb1.toString();
		}
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyDataManager(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		int fldPK = 0;
 		try {
 			String authorStr = (studyJB == null || studyJB.getStudyAuthor() == null) ? EMPTY_STRING : studyJB.getStudyAuthor();
 			fldPK = StringUtil.stringToNum(authorStr);
 		} catch (Exception e) {
 			e.printStackTrace();
 			fldPK = 0;
 		}
		String fldValue = EMPTY_STRING;
		if(fldPK > 0) {
			UserJB userB = new UserJB();
			userB.setUserId(fldPK);
			userB.getUserDetails();
			fldValue = userB.getUserFirstName() + " " + userB.getUserLastName();
			fldValue = (null == fldValue)? "":fldValue;
		}
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}

	private JSONObject doStudyObjective(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyObjective() == null) ? EMPTY_STRING : studyJB.getStudyObjective();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}

	private JSONObject doStudySummary(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudySummary() == null) ? EMPTY_STRING : studyJB.getStudySummary();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	//Kavitha : #31069
	private JSONObject doStudyNciIdentifier(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getNciTrialIdentifier() == null) ? EMPTY_STRING : studyJB.getNciTrialIdentifier();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	private JSONObject doStudyGrantNihInformation(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getGrantNihInformation() == null) ? EMPTY_STRING : studyJB.getGrantNihInformation();
		System.out.println("fldValue=++++++++++++"+fldValue);
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}

	private JSONObject doStudyPiAuthor(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		int fldPK = 0;
		try {
			String primaryInv = (studyJB == null || studyJB.getStudyPrimInv() == null) ? EMPTY_STRING : studyJB.getStudyPrimInv();
			fldPK = StringUtil.stringToNum(primaryInv);
		} catch (Exception e) {
			e.printStackTrace();
			fldPK = 0;
		}
		String fldValue = EMPTY_STRING;
		if (fldPK > 0){
			UserJB userB = new UserJB();
			userB.setUserId(fldPK);
			userB.getUserDetails();
			fldValue =  userB.getUserFirstName() + " " +userB.getUserLastName(); 
			fldValue = (null==fldValue)? "" : fldValue;
		}
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyPiOther(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = studyJB.getStudyOtherPrinv() == null ? EMPTY_STRING : studyJB.getStudyOtherPrinv();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyStudyContact(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		int fldPK= 0;
		try {
			String studyContact = (studyJB.getStudyCoordinator() == null) ? EMPTY_STRING : studyJB.getStudyCoordinator();
			fldPK = StringUtil.stringToNum(studyContact);
		} catch (Exception e) {
			e.printStackTrace();
			fldPK = 0;
		}
		String fldValue = EMPTY_STRING;
		if(fldPK > 0){
			UserJB userB = new UserJB();
			userB.setUserId(fldPK);
			userB.getUserDetails();
			fldValue = userB.getUserFirstName() + " " + userB.getUserLastName();
			fldValue = (null==fldValue)?"":fldValue;
		}
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyCheck(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getMajAuthor() == null) ? EMPTY_STRING : studyJB.getMajAuthor();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}

	private JSONObject doStudyCtrpReportable (FlxPageFields field, 
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyCtrpReportable() == null) ? 
				EMPTY_STRING : studyJB.getStudyCtrpReportable();
		json.put(field.getFlxPageFieldKey(), fldValue);
		//Kavitha : #31069
				if(fldValue.equals("1")){
					fldValue="Y";
				}else if(fldValue.equals("0")){
					fldValue="N";
				}
		return json;
	}
	
	private JSONObject doStudyFdaRegulated(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getFdaRegulatedStudy() == null) ? 
				EMPTY_STRING : studyJB.getFdaRegulatedStudy();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyNctNumber(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getNctNumber() == null) ? EMPTY_STRING : studyJB.getNctNumber();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	private JSONObject doStudyPurpose(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyPurpose() == null) ? EMPTY_STRING : studyJB.getStudyPurpose();
		CodeDao cdDao = new CodeDao();
		int codeId = 0;
		try { codeId = Integer.parseInt(fldValue); } catch(Exception e) {}
		String codeDesc = cdDao.getCodeDescription(codeId);
		if (codeDesc == null) { codeDesc = EMPTY_STRING; } 
		json.put(field.getFlxPageFieldKey(), codeDesc);
		return json;
	}

	private JSONObject doStudyTArea(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyTArea() == null) ? EMPTY_STRING : studyJB.getStudyTArea();
		CodeDao cdDao = new CodeDao();
		int codeId = 0;
		try { codeId = Integer.parseInt(fldValue); } catch(Exception e) {}
		String codeDesc = cdDao.getCodeDescription(codeId);
		if (codeDesc == null) { codeDesc = EMPTY_STRING; } 
		json.put(field.getFlxPageFieldKey(), codeDesc);
		return json;
	}
	
	private JSONObject doStudyAgentDevice(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyProduct() == null) ? EMPTY_STRING : studyJB.getStudyProduct();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}

	private JSONObject doStudySpecificSites1(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyICDCode1() == null) ? EMPTY_STRING : studyJB.getStudyICDCode1();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}

	private JSONObject doStudySpecificSites2(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyICDCode2() == null) ? EMPTY_STRING : studyJB.getStudyICDCode2();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudySampleSize(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyNSamplSize() == null) ? EMPTY_STRING : studyJB.getStudyNSamplSize();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}

	private JSONObject doStudyDuration(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyDuration() == null) ? EMPTY_STRING : studyJB.getStudyDuration();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyDurationDropDown(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyDurationUnit() == null) ? EMPTY_STRING : studyJB.getStudyDurationUnit();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyDivision(FlxPageFields field, StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyDivision() == null) ? EMPTY_STRING : studyJB.getStudyDivision();
		CodeDao cdDao = new CodeDao();
		int codeId = 0;
		try { codeId = Integer.parseInt(fldValue); } catch(Exception e) {}
		String codeDesc = cdDao.getCodeDescription(codeId);
		if (codeDesc == null) { codeDesc = EMPTY_STRING; } 
		json.put(field.getFlxPageFieldKey(), codeDesc);
		return json;
	}
	
	private JSONObject doStudyCcsgReportable(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getCcsgReportableStudy() == null) ? EMPTY_STRING : studyJB.getCcsgReportableStudy();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyEstimatedBeginDate(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyEstBeginDate() == null) ? EMPTY_STRING : studyJB.getStudyEstBeginDate();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyPhase(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyPhase() == null) ? EMPTY_STRING : studyJB.getStudyPhase();
		CodeDao cdDao = new CodeDao();
		int codeId = 0;
		try { codeId = Integer.parseInt(fldValue); } catch(Exception e) {}
		String codeDesc = cdDao.getCodeDescription(codeId);
		if (codeDesc == null) { codeDesc = EMPTY_STRING; } 
		json.put(field.getFlxPageFieldKey(), codeDesc);
		return json;
	}
	
	private JSONObject doStudyResearchType(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyResType() == null) ? EMPTY_STRING : studyJB.getStudyResType();
		CodeDao cdDao = new CodeDao();
		int codeId = 0;
		try { codeId = Integer.parseInt(fldValue); } catch(Exception e) {}
		String codeDesc = cdDao.getCodeDescription(codeId);
		if (codeDesc == null) { codeDesc = EMPTY_STRING; } 
		json.put(field.getFlxPageFieldKey(), codeDesc);
		return json;
	}
	
	private JSONObject doStudyScope(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyScope() == null) ? EMPTY_STRING : studyJB.getStudyScope();
		CodeDao cdDao = new CodeDao();
		int codeId = 0;
		try { codeId = Integer.parseInt(fldValue); } catch(Exception e) {}
		String codeDesc = cdDao.getCodeDescription(codeId);
		if (codeDesc == null) { codeDesc = EMPTY_STRING; } 
		json.put(field.getFlxPageFieldKey(), codeDesc);
		return json;
	}
	
	private JSONObject doStudyType(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyType() == null) ? EMPTY_STRING : studyJB.getStudyType();
		CodeDao cdDao = new CodeDao();
		int codeId = 0;
		try { codeId = Integer.parseInt(fldValue); } catch(Exception e) {}
		String codeDesc = cdDao.getCodeDescription(codeId);
		if (codeDesc == null) { codeDesc = EMPTY_STRING; } 
		json.put(field.getFlxPageFieldKey(), codeDesc);
		return json;
	}
	
	private JSONObject doStudyLinkedTo(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyAssoc() == null) ? EMPTY_STRING : studyJB.getStudyAssoc();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyBlinding(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyBlinding() == null) ? EMPTY_STRING : studyJB.getStudyBlinding();
		CodeDao cdDao = new CodeDao();
		int codeId = 0;
		try { codeId = Integer.parseInt(fldValue); } catch(Exception e) {}
		String codeDesc = cdDao.getCodeDescription(codeId);
		if (codeDesc == null) { codeDesc = EMPTY_STRING; } 
		json.put(field.getFlxPageFieldKey(), codeDesc);
		return json;
	}
	
	private JSONObject doStudyRandomization(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyRandom() == null) ? EMPTY_STRING : studyJB.getStudyRandom();
		CodeDao cdDao = new CodeDao();
		int codeId = 0;
		try { codeId = Integer.parseInt(fldValue); } catch(Exception e) {}
		String codeDesc = cdDao.getCodeDescription(codeId);
		if (codeDesc == null) { codeDesc = EMPTY_STRING; } 
		json.put(field.getFlxPageFieldKey(), codeDesc);
		return json;
	}
	
	//kavitha VEL-490(11.1 Sprint#2) or Bug-30881
		private JSONObject doStudySponsor(FlxPageFields field,
				StudyJB studyJB) throws JSONException {
			JSONObject json = new JSONObject();
			String fldValue = (studyJB.getStudySponsorName() == null) ? EMPTY_STRING : studyJB.getStudySponsorName();
			CodeDao cdDao = new CodeDao();
			int codeId = 0;
			try { codeId = Integer.parseInt(fldValue); } catch(Exception e) {}
			String codeDesc = cdDao.getCodeDescription(codeId);
			if (codeDesc == null) { codeDesc = EMPTY_STRING; } 
			json.put(field.getFlxPageFieldKey(), codeDesc);
			return json;	
		}
		
	private JSONObject doStudySponsorIfOther(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudySponsor() == null) ? EMPTY_STRING : studyJB.getStudySponsor();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudySponsorId(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudySponsorIdInfo() == null) ? EMPTY_STRING : studyJB.getStudySponsorIdInfo();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyContact(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyContact() == null) ? EMPTY_STRING : studyJB.getStudyContact();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	private JSONObject doStudyKeywords(FlxPageFields field,
			StudyJB studyJB) throws JSONException {
		JSONObject json = new JSONObject();
		String fldValue = (studyJB.getStudyKeywords() == null) ? EMPTY_STRING : studyJB.getStudyKeywords();
		json.put(field.getFlxPageFieldKey(), fldValue);
		return json;
	}
	
	public JSONArray createAttachArchiveJson(String attachFieldDetails) {
		JSONArray attachJsonArray =  null;
		try {
			if( attachFieldDetails == null ) {
				attachFieldDetails="[]";
			}
			 attachJsonArray = new JSONArray(attachFieldDetails);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return attachJsonArray;
	}
	
	public double lockStudy(int entityId, UserBean userBean){
		System.out.println("Inside LockStudy");
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
				    final String VERSION_STATUS = "versionStatus";
					final String LETTER_F = "F";
					final String ER_STUDYVER_TABLE = "er_studyver";
				    paramMap.put("userId", (userBean.getUserId().equals(null) ? null : Integer.valueOf(userBean.getUserId())));
					paramMap.put("accountId", (userBean.getUserAccountId().equals(null) ? null : Integer.valueOf(userBean.getUserAccountId())));
					paramMap.put("studyId", entityId);
					System.out.println("entityId--->"+entityId);
					// Implement Flex Study part here
					StudyJB studyJB = new StudyJB();
					studyJB.setId(entityId);
					studyJB.getStudyDetails();
					String defUserGroup = (String) userBean.getUserGrpDefault();
					System.out.println("defUserGroup--->"+defUserGroup);
					StudyIdJB studyIdJB = new StudyIdJB();
					StudyIdDao sidDao = new StudyIdDao();
					sidDao = studyIdJB.getStudyIds(
							entityId, defUserGroup);

					
					String fullVersion = createArchive(studyJB, sidDao, paramMap);
					System.out.println("fullVersion--->"+fullVersion);
				    double versionNum = Double.valueOf(fullVersion).doubleValue();
				    // Freeze all categories of this version of this study
				    StudyVerJB studyVerJB = new StudyVerJB();
				    StudyVerDao studyVerDao = studyVerJB.getAllVers(entityId);
				    ArrayList studyVerIds = studyVerDao.getStudyVerIds();
				    ArrayList studyVerNumbers = studyVerDao.getStudyVerNumbers();
				    // Get a codelst item for Freeze status
				    CodeDao statDao = new CodeDao();
				    int freezeStat = statDao.getCodeId(VERSION_STATUS, LETTER_F);
					Calendar cal = Calendar.getInstance();
					SimpleDateFormat sdf = new SimpleDateFormat(Configuration.getAppDateFormat());
				    for (int iX = 0; iX < studyVerNumbers.size(); iX++) {
				    	String svNumStr = (String)studyVerNumbers.get(iX);
				    	double svNum = Double.valueOf(svNumStr).doubleValue();
				    	double vNum = Double.valueOf(versionNum).doubleValue();
				    	if( versionNum > 0 ) {
			    			if( svNum == vNum) {
					    		// add a Freeze row to ER_STATUS_HISTORY
					    		StatusHistoryJB statusHistoryJB = new StatusHistoryJB();
					    		statusHistoryJB.setStatusModuleTable(ER_STUDYVER_TABLE);
					    		int modulePk = (Integer)studyVerIds.get(iX);
					    		statusHistoryJB.setStatusModuleId(String.valueOf(modulePk));
					    		statusHistoryJB.setStatusCodelstId(String.valueOf(freezeStat));
					    		statusHistoryJB.setStatusStartDate(sdf.format(cal.getTime()));
						    	statusHistoryJB.setStatusEnteredBy(String.valueOf(userBean.getUserId()));
						    	statusHistoryJB.setCreator(String.valueOf(userBean.getUserId()));
						   		statusHistoryJB.setStatusHistoryDetails();
						   		// add version status "F" in er_studyver table
						   		studyVerDao.updateVersionStatus(svNumStr, entityId, modulePk, "F",userBean.getUserId());			   		
				    		}
					   	}
					}

			    	/* Add form locking code here. */
			    	
			    	System.out.println("Before Locking");
				    //Locking the Study as Version is frozen
				    StudyDao studyDao = new StudyDao();
				    studyDao.lockUnlockStudy(entityId, 1);
				    if ("0".equals(fullVersion)) {
				    	versionNum=0;
				    }
		return versionNum;
	}
	
}
