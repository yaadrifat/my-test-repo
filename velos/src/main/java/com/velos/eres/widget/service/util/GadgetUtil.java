package com.velos.eres.widget.service.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.velos.eres.business.common.StudyDao;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.study.StudyJB;

public class GadgetUtil {
    private static final String ALLSTUDYID_KEY = "allStudyId";
    private static final String ALLSTUDYNUMBER_KEY = "allStudyNumber";
    private static final String STUDYID_KEY = "studyId";
    private static final String STUDYNUMBER_KEY = "studyNumber";
    private static final String ARRAY_KEY = "array";
    
	/* Method: getMyStudies
     * returns LinkedHashMap<String, Object>
     * Parameters:
     * String userId, - pk_user 
     * String accId, - fk_account
     * String studyIds, - comma delimited string of pk_studies
     * int criterion - 
     * 		Values: -1 = no action
     * 				1 = only these studies 
     * 				0 = except these studies
     * */
    public static LinkedHashMap<String, Object> getStudies(String userId, String accId, String studyIds, int criterion){
    	LinkedHashMap<String, Object> myStudiesData = new LinkedHashMap<String, Object>();
    	if (StringUtil.isEmpty(userId)) { return myStudiesData; }
		
		StudyJB studyJB1 = new StudyJB();
		StudyDao studyDao = studyJB1.getStudyAutoRows(userId, accId, studyIds, criterion);
		if (studyDao == null) { return myStudiesData; }
		
		String studyNumberKey = STUDYNUMBER_KEY;
		String studyIdKey = STUDYID_KEY;
		
		switch (criterion){
		case 1:
			//Only these studies
			studyNumberKey = STUDYNUMBER_KEY;
			studyIdKey = STUDYID_KEY;
			break;
		case 0:
			//Except these studies
			studyNumberKey = ALLSTUDYNUMBER_KEY;
			studyIdKey = ALLSTUDYID_KEY;
			break;
		}

		ArrayList<HashMap<String, String>> jArray = new ArrayList<HashMap<String, String>>();
		
		for (int iX=0; iX<studyDao.getStudyAutoNames().size(); iX++) {
			if (criterion == 1 && iX > 4) { break; }
			HashMap<String, String> aStudy = new HashMap<String, String>();
			aStudy.put(studyIdKey, String.valueOf(studyDao.getStudyAutoIds().get(iX)));
			aStudy.put(studyNumberKey, (String)studyDao.getStudyAutoNames().get(iX));
			
			try {
				jArray.add(aStudy);
			} catch(Exception e) {
				Rlog.fatal("finStudy", "jArray.put error: e");
			}
		}
		myStudiesData.put(ARRAY_KEY, jArray);
    	return myStudiesData;
    }

    public static LinkedHashMap<String, Object> getExportFile (String colArray1, String dataArray1, 
    		String headerStr, String format){
    	JSONArray colArray=null;
    	JSONArray dataArray=null;
		try {
			colArray = new JSONArray(colArray1);
			dataArray = new JSONArray(dataArray1);
		} catch (JSONException e1) {
			System.out.println("Exception in getExportFile of GadgetUtil class:");
			e1.printStackTrace();
		}
    	
    	LinkedHashMap<String, Object> formattedFile = new LinkedHashMap<String, Object>();
    	String fileName = null;
		String contentApp = null;
		
		StringBuffer fileData =  new StringBuffer();
    	StringBuffer completeFile =  new StringBuffer();
		if ("csv".equals(format)) {
		    fileName = "reportcsv["+System.currentTimeMillis()+"].csv" ;
		    contentApp = "text/csv; ";
		    
			if (dataArray.length() > 0) {
			    fileData.append(""+headerStr+"\n");

			    if (colArray.length() > 0) {
			        for (int iX=0; iX<colArray.length(); iX++) {
			        	try{
				            String key = (String)((JSONObject)colArray.get(iX)).get("key");
				            String hideExport = 
				            ((JSONObject)colArray.get(iX)).has("hideExport")?
				            		(String)((JSONObject)colArray.get(iX)).get("hideExport") : "false";
		
				            if ("true".equals(hideExport)) { continue; }
				            
				            String label = (String)((JSONObject)colArray.get(iX)).get("label");
				            label.replace("\"", "\"\"");
				            fileData.append("\""+label+"\"");
				            if (iX != colArray.length()-1)
				        		fileData.append(",");
				            else
				            	fileData.append("\n");
			        	} catch (Exception e){
			        		
			        	}
			        }
			    }
			    for (int iX=0; iX<dataArray.length(); iX++) {
			    	try{
				        JSONObject rowData = (JSONObject)dataArray.get(iX);
					    for (int iY=0; iY<colArray.length(); iY++) {
				            String key = (String)((JSONObject)colArray.get(iY)).get("key");
				            
				            String columnData = rowData.has(key)?
				            		(String)rowData.get(key) : "";

				            columnData.replace("\"", "\"\"");
				            fileData.append("\""+columnData+"\"");

				        	if (iY != colArray.length()-1)
				        		fileData.append(",");
				            else
				            	fileData.append("\n");
					    }
			    	} catch (Exception e){
		        		
		        	}
			    }
			}
			completeFile.append(fileData);
		    
		} else {
			if ("excel".equals(format)) {
			    fileName = "reportexcel["+System.currentTimeMillis()+"].xls" ;
			    contentApp = "application/vnd.ms-excel; ";
			} else if ("word".equals(format)) {
				fileName = "reportword["+System.currentTimeMillis()+"].doc" ;
			    contentApp = "application/vnd.ms-word; ";
			} else if ("html".equals(format)) {
				fileName = "reporthtml["+System.currentTimeMillis()+"].html" ;
			    contentApp = "text/html; ";
			}

			completeFile.append("<HTML><HEAD>");
			completeFile.append("<META http-equiv=\"Content-Type\" content=\"").append(contentApp).append("charset=UTF-8\"/>");
			completeFile.append("</HEAD>");
			completeFile.append("<BODY>");
			if (dataArray.length() > 0) {
			    ArrayList visitIdList = new ArrayList();
			    fileData.append("<TABLE class='basetbl midAlign' ALIGN='CENTER' style='border-style:none;' WIDTH='99%'>");
			    fileData.append("<TR><TD align='center'>");
			    fileData.append("<P>"+headerStr+"</P>");
			    fileData.append("</TD></TR>");
			    fileData.append("</TABLE>");
			    fileData.append("<TABLE class='basetbl' style='padding:0; border-collapse:collapse;' ALIGN='CENTER' WIDTH='99%'>");
			    if (colArray.length() > 0) {
			        fileData.append("<tr>");
			        for (int iX=0; iX<colArray.length(); iX++) {
			        	try{
			        		JSONObject column = (JSONObject)colArray.get(iX);
				            String key = (String)column.get("key");
				            String hideExport = column.has("hideExport")?
				            		(String)column.get("hideExport") : "false";

				            if ("true".equals(hideExport)) { continue; }
				            if(key.equalsIgnoreCase("patientCode")){
			            	//fileData.append("<th><a href=\"javascript:void(0)\" style=\"text-decoration: none; color:black;\" onmouseover=\" return overlib('"+LC.L_pat_Or_Stat_Date+"',CAPTION,'');\" onmouseout=\"return nd();\">").append(column.get("label")).append("</a></th>");	
				            fileData.append("<th>").append(column.get("label")).append("</th>");
				            }else{
				            fileData.append("<th>").append(column.get("label")).append("</th>");
				            }
			        	} catch (Exception e){
			        		
			        	}
			        }
			        fileData.append("</tr>");
			    }
			    for (int iX=0; iX<dataArray.length(); iX++) {
			    	try{
				        JSONObject rowData = (JSONObject)dataArray.get(iX);
				    	String rowClass = ((iX%2) == 0)? "browserEvenRow" : "browserOddRow";
					    fileData.append("<tr class='"+rowClass+"'>");

					    String isTotal = rowData.has("isTotal")?
			            		(String)rowData.get("isTotal") : "N";
			            		
					    for (int iY=0; iY<colArray.length(); iY++) {
					    	JSONObject column = (JSONObject)colArray.get(iY);
					    	
					    	String hideExport = column.has("hideExport")?
				            		(String)column.get("hideExport") : "false";

				            if ("true".equals(hideExport)) { continue; }
				            
				            String key = (String)column.get("key");
				            String isNumber = column.has("isNumber")?
				            		(String)column.get("isNumber") : "N";

				            String alignStr = "";
				            if ("Y".equals(isNumber)){
				            	alignStr = " align='right' NOWRAP ";
				            }
				            String columnData = rowData.has(key)?
				            		(String)rowData.get(key) : "";
				            if ("Y".equals(isTotal)){
				            	if ("Y".equals(isNumber)){
					            	fileData.append("<th"+alignStr+">").append(columnData).append("</th>");
				            	} else {
					            	fileData.append("<th>").append(columnData).append("</th>");
				            	}
				            } else {
				            	fileData.append("<td"+alignStr+">").append(columnData).append("</td>");
				            }
					    }
					    fileData.append("</tr>");
			    	} catch (Exception e){
		        		
		        	}
			    }
			    fileData.append("</TABLE>");
			}
			completeFile.append(fileData);
			completeFile.append("</BODY></HTML>");
		}
		
		formattedFile.put("formattedFile", completeFile);
		formattedFile.put("fileData", fileData);
		formattedFile.put("fileName", fileName);
		formattedFile.put("contentApp", contentApp);
    	return formattedFile;
    }
}
