package com.velos.eres.widget.web;

import java.io.Serializable;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.grpRights.GrpRightsJB;
import com.velos.eres.widget.service.util.GadgetUtil;


public class GadgetMyStudiesAction extends ActionSupport implements Serializable {

	private static final long serialVersionUID = -1426225417025575644L;
	private HttpServletRequest request = null;
	private HttpSession tSession = null;
	private GadgetMyStudiesJB gadgetMyStudiesJB = null;
	private LinkedHashMap<String, Object> jsonData = null;
	private static final String STUDYDISPLAYOPTION = "displayOpt";
	private static final String USER_ID = "userId";
	private static final String ACCOUNT_ID = "accountId";
	
	public GadgetMyStudiesAction() {
		ActionContext ac = ActionContext.getContext();
		request = (HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		tSession = request.getSession(false);
		gadgetMyStudiesJB = new GadgetMyStudiesJB(getUserIdInSession());
	}
	
	public GadgetMyStudiesJB getGadgetMyStudiesJB(){
		return this.gadgetMyStudiesJB;
	}

	public void setGadgetMyStudiesJB(GadgetMyStudiesJB gadgetMyStudiesJB){
		this.gadgetMyStudiesJB = gadgetMyStudiesJB;
	}
	
	public String getGadgetMyStudies() { return SUCCESS; }
	
	public LinkedHashMap<String, Object> getJsonData() { return jsonData; }

	private String getUserIdInSession() {
		String userId = null;
		try {
			userId = (String) request.getSession(false).getAttribute(USER_ID);
		} finally {
			userId = StringUtil.trueValue(userId);
		}
		return userId;
	}
	
	public String getStudies() {
		String accId = (String)tSession.getAttribute(ACCOUNT_ID);
		if (StringUtil.isEmpty(accId)) { return null; }
		
		String userId = (String)tSession.getAttribute(USER_ID);
		if (StringUtil.isEmpty(userId)) { return null; }
		
		String exceptStudies = gadgetMyStudiesJB.getSettingsStudyIds();
		
		jsonData = GadgetUtil.getStudies(userId, accId, exceptStudies, 0);
		return SUCCESS;
	}

	public String getStudyModuleAccess() {
		String accId = (String)tSession.getAttribute(ACCOUNT_ID);
		if (StringUtil.isEmpty(accId)) { return null; }
		
		String userId = (String)tSession.getAttribute(USER_ID);
		if (StringUtil.isEmpty(userId)) { return null; }
		
        int userProtocol  = 0;
		GrpRightsJB groupRights = (GrpRightsJB) 	tSession.getAttribute("GRights");
		userProtocol = Integer.parseInt(groupRights.getFtrRightsByValue("NPROTOCOL"));
		
		if (!StringUtil.isAccessibleFor(userProtocol, 'V')){
			 return null;
		}
		return SUCCESS;
	}

	
	public String getMyStudies() {	
		if (StringUtil.isEmpty(getStudyModuleAccess())) { return null; }
		
		String accId = (String)tSession.getAttribute(ACCOUNT_ID);
		if (StringUtil.isEmpty(accId)) { return null; }

		String userId = (String)tSession.getAttribute(USER_ID);
		if (StringUtil.isEmpty(userId)) { return null; }
		
		jsonData = new LinkedHashMap<String, Object>();
		jsonData.put(STUDYDISPLAYOPTION, -1);
		
		int displayOption = gadgetMyStudiesJB.getsettingsDisplayOption();
		if (displayOption == 0) { 
			jsonData.put("array", null);
			jsonData.put(STUDYDISPLAYOPTION, 0);
		} 
		if (displayOption == 1) { 
			String onlyStudies = gadgetMyStudiesJB.getSettingsStudyIds();
			if (!StringUtil.isEmpty(onlyStudies)){
				jsonData = GadgetUtil.getStudies(userId, accId, onlyStudies, 1);
			} else{
				jsonData.put("array", null);
			}
			jsonData.put(STUDYDISPLAYOPTION, 1);
		}
		return SUCCESS;
	}
	
	public String getMyStudiesData() {
		if (StringUtil.isEmpty(getStudyModuleAccess())) { return null; }
		
		String accId = (String)tSession.getAttribute(ACCOUNT_ID);
		if (StringUtil.isEmpty(accId)) { return null; }
		
		String userId = (String)tSession.getAttribute(USER_ID);
		if (StringUtil.isEmpty(userId)) { return null; }
		
		int displayOption = gadgetMyStudiesJB.getsettingsDisplayOption();
		if (displayOption < 0) { return null; }		
	
		jsonData = new LinkedHashMap<String, Object>();
		jsonData.put("colArray", gadgetMyStudiesJB.getMyStudiesColumns());
		jsonData.put("dataArray", gadgetMyStudiesJB.getMyStudiesData(accId, userId));
		return SUCCESS;
	}
}
