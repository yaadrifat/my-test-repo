/*
 * Classname:			StorageKitAgentBean
 *
 * Version information:	1.0
 *
 * Date:				05Aug2009
 *
 * Copyright notice: 	Velos Inc
 *
 * Author: 				Jnanamay
 */

package com.velos.eres.service.storageKitAgent.impl;

/* IMPORT STATEMENTS */

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.storageKit.impl.StorageKitBean;
import com.velos.eres.service.storageKitAgent.StorageKitAgentRObj;
import com.velos.eres.service.util.Rlog;

import com.velos.eres.business.common.StorageKitDao;


/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP.
 *
 * @author Jnanamay
 * @version 1.0, 08/05/2009
 * @ejbHome StorageKitAgentHome
 * @ejbRemote StorageKitAgentRObj
 */

@Stateless
public class StorageKitAgentBean implements StorageKitAgentRObj {

    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    public StorageKitBean getStorageKitDetails(int pkStorageKit) {
    	StorageKitBean ap = null;
        try {

            ap = (StorageKitBean) em.find(StorageKitBean.class, new Integer(pkStorageKit));

        } catch (Exception e) {
            Rlog.fatal("storagekit",
                    "Error in getStorageKitDetails() in StorageKitAgentBean" + e);
        }

        return ap;
    }

    public int setStorageKitDetails(StorageKitBean admin) {

    	StorageKitBean ad = new StorageKitBean();
        int rowfound = 0;

    	try {
            Rlog.debug("storagekit", "in try bloc of storagekit agent");
            ad.updateStorageKit(admin);
    		em.persist(ad);
    		return ad.getPkStorageKit();

            } catch (Exception ex) {
                Rlog.fatal("storagekit",
                        "In setStorageKitDetails "
                                + ex);
                rowfound = 0;
            }

        return 0;
    }

    public int updateStorageKit(StorageKitBean ap) {
    	StorageKitBean adm = null;
    	int rowfound = 0;
        int output;
        try {
            adm = (StorageKitBean) em.find(StorageKitBean.class, new Integer(
                    ap.getPkStorageKit()));
            if (adm == null) {
                return -2;
            }

            output = adm.updateStorageKit(ap);
            em.merge(adm);
        } catch (Exception e) {
            Rlog.debug("storagekit", "Error in updateStorageKit in StorageKitAgentBean"
                    + e);
            return -2;
        }
        return output;
    }

  //JM: 21Aug2009
    public StorageKitDao getStorageKitAttributes(int pkStorageKit) {

        try {

        	StorageKitDao skDao = new StorageKitDao();
        	skDao.getStorageKitAttributes(pkStorageKit);
            return skDao;

        } catch (Exception e) {
            Rlog.fatal("storagekit", "Exception In getStorageKitAttributes in StorageKitAgentBean " + e);
        }
        return null;
    }


}
