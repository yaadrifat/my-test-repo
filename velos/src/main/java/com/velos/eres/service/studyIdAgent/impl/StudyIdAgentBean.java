/*
 * Classname : StudyIdAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 09/22/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Sahni
 */

package com.velos.eres.service.studyIdAgent.impl;

/* Import Statements */

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.StudyIdDao;
import com.velos.eres.business.studyId.impl.StudyIdBean;
import com.velos.eres.service.studyIdAgent.StudyIdAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The stateless session EJB acting as a facade for the entity CMP -
 * StudyIdAgentBean.<br>
 * <br>
 * 
 * @author Sonia Sahni
 * @see studyIdAgentBean
 * @version 1.0 09/22/2003
 * @ejbHome studyIdAgentHome
 * @ejbRemote studyIdAgentRObj
 */
@Stateless
public class StudyIdAgentBean implements StudyIdAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * 
     * Looks up on the id parameter passed in and constructs and returns a
     * studyId state keeper. containing all the details of the studyId <br>
     * 
     * @param id
     *            the studyId id
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public StudyIdBean getStudyIdDetails(int id) {
        StudyIdBean sb = null;
        try {

            sb = (StudyIdBean) em.find(StudyIdBean.class, new Integer(id));
            return sb;

        } catch (Exception e) {
            Rlog.fatal("studyId",
                    "Exception in getStudIdDetails() in StudyIdAgentBean" + e);
            return null;
        }

    }

    /**
     * Creates studyId record.
     * 
     * @param a
     *            State Keeper containing the studyId attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int setStudyIdDetails(StudyIdBean ssk) {
        try {
            em.merge(ssk);
            return ssk.getId();
        } catch (Exception e) {
            Rlog.fatal("studyId",
                    "Error in setStudyIdDetails() in StudyIdAgentBean " + e);
            return -2;
        }

    }

    /**
     * Updates a studyId record.
     * 
     * @param a
     *            studyId state keeper containing studyId attributes to be set.
     * @return int for successful:0 ; e\Exception : -2
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int updateStudyId(StudyIdBean ssk) {

        StudyIdBean sb = null; // Entity Bean Remote Object
        int output;

        try {
            sb = (StudyIdBean) em.find(StudyIdBean.class, new Integer(ssk
                    .getId()));

            if (sb == null) {
                return -2;
            }
            output = sb.updateStudyId(ssk);

        } catch (Exception e) {
            Rlog.fatal("studyId", "Error in updateStudyId in StudyIdAgentBean"
                    + e);
            return -2;
        }
        return output;
    }

    /**
     * Removes a studyId record.
     * 
     * @param a
     *            StudyId Primary Key
     * @return int for successful:0; e\Exception : -2
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int removeStudyId(int id) {
        int output;
        StudyIdBean sb = null;

        try {
            sb = (StudyIdBean) em.find(StudyIdBean.class, new Integer(id));
            em.remove(sb);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("studyId", "Exception in removeStudyId" + e);
            return -1;

        }

    }

    /**
     * Creates multiple studyids
     * 
     * @param StudyIdStateKeeper
     * @return int
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int createMultipleStudyIds(StudyIdBean ssk,String defuserGroup) {
        try {
            Rlog.debug("studyId", "StudyIdAgentBean.createMultipleStudyIds ");

            StudyIdDao sidDao = new StudyIdDao();

            ArrayList sskArray = ssk.getStudyIdBeans();

            ArrayList studyIdType = new ArrayList();
            ArrayList recordTypeList = new ArrayList();
            ArrayList studyIdPk = new ArrayList();
            int rows = sskArray.size();
            int studyId = 0, index = -1, idPk = 0;

            if (rows > 0) {
                studyId = StringUtil.stringToNum(((StudyIdBean) sskArray.get(0))
                        .getStudyId());
            }
            sidDao = getStudyIds(studyId,defuserGroup);
            studyIdType = sidDao.getStudyIdType();
            recordTypeList = sidDao.getRecordType();
            studyIdPk = sidDao.getId();
            Rlog.debug("studyId", "studyIdType" + studyIdType
                    + "recordTypeList" + recordTypeList);
            int ret = 0;
            String recordType = "";
            String recType = "";
            String studyTypeId = "";

            for (int i = 0; i < rows; i++) {
                StudyIdBean StudyIdBeanTemp = new StudyIdBean();

                StudyIdBeanTemp = (StudyIdBean) sskArray.get(i);

                recordType = StudyIdBeanTemp.getRecordType();
                studyTypeId = StudyIdBeanTemp.getStudyIdType();
                Rlog.debug("studyId",
                        "before find index of studyTypeId@createMultipleStudyIds"
                                + studyTypeId);
                index = studyIdType.indexOf(new Integer(studyTypeId));
                Rlog.debug("studyId",
                        "index of studyTypeId@createMultipleStudyIds" + index);
                if (index >= 0) {
                    recordType = (String) recordTypeList.get(index);
                    idPk = ((Integer) studyIdPk.get(index)).intValue();
                }
                Rlog.debug("studyId", "createMultipleStudyIds:recroType"
                        + recordType);
                if (recordType == null)
                    recordType = "";
                if (recordType.length() == 0)
                    recordType = "N";
                Rlog.debug("studyId",
                        "createMultipleStudyIds:recroType after setting for Null"
                                + recordType);

                if (recordType.equals("N"))
                    ret = setStudyIdDetails(StudyIdBeanTemp);
                else if (recordType.equals("M")) {
                    StudyIdBeanTemp.setId(idPk);
                    StudyIdBeanTemp.setRecordType("M");
                    ret = updateStudyId(StudyIdBeanTemp);
                }

                Rlog.debug("studyId",
                        "StudyIdAgentBean.INSIDE ROWS AFTER SETTING###" + ret);
            }

            return ret;
        } catch (Exception e) {
            Rlog.fatal("studyId",
                    "Exception In createMultipleStudyIds in StudyIdAgentBean "
                            + e);
            return -2;
        }

    }

    /**
     * Updates multiple Study Ids
     * 
     * @param StudyIdStateKeeper
     * @return int
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int updateMultipleStudyIds(StudyIdBean ssk) {
        try {
            Rlog.debug("studyId", "StudyIdAgentBean.updateMultipleStudyIds");

            ArrayList sskArray = ssk.getStudyIdBeans();

            int rows = sskArray.size();
            int ret = 0;

            for (int i = 0; i < rows; i++) {

                StudyIdBean studyIdBeanTemp = new StudyIdBean();
                studyIdBeanTemp = (StudyIdBean) sskArray.get(i);

                // getStudyIdDetails(StudyIdStateKeeperTemp.getId());
                ret = updateStudyId(studyIdBeanTemp);
                Rlog.debug("studyId",
                        "StudyIdAgentBean.INSIDE ROWS AFTER SETTING ###" + ret);
            }

            return ret;
        } catch (Exception e) {
            Rlog.fatal("studyId",
                    "Exception StudyIdAgentBean.updateMultipleStudyIds " + e);
            return -2;
        }

    }
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public StudyIdDao getStudyIds(int studyId,String defUserGroup) {

        try {
            StudyIdDao sId = new StudyIdDao();
            sId.getStudyIds(studyId, defUserGroup);
            return sId;

        } catch (Exception e) {
            Rlog.fatal("studyId", "Exception in getStudyIds" + e);
            return null;

        }
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int findMoreStudyId(int studyId, String studyIdType) {
		int ret = 0;
    	try {
    		StudyIdBean stdIdRobj = null; // Study Entity Bean Remote Object
    		
    		CodeDao cdDao = new CodeDao();
			int cdProtId = cdDao.getCodeId("studyidtype", studyIdType);
			if (cdProtId <= 0){
				return ret;
			}
			
			Query queryStudyIdentifier = em.createNamedQuery("findMoreStudyId");
			queryStudyIdentifier.setParameter("studyId", studyId);
			queryStudyIdentifier.setParameter("studyIdFK", cdProtId);
			ArrayList list = (ArrayList) queryStudyIdentifier.getResultList();
			
			if (list == null)
				list = new ArrayList();
			if (list.size() > 0) {
				stdIdRobj = (StudyIdBean) list.get(0);
				ret = stdIdRobj.getId();
			}
			return ret;

        } catch (Exception e) {
            Rlog.fatal("studyId", "Exception in findMoreStudyId" + e);
            return ret;

        }
    }
    
}
