/*
 * Classname			InvoiceDetailAgentBean
 *
 * Version information :
 *
 * Date					10/19/2005
 *
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.invoiceAgent.impl;

/* IMPORT STATEMENTS */

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.eres.business.common.InvoiceDetailDao;
import com.velos.eres.business.invoice.InvoiceDetailBean;
import com.velos.eres.service.invoiceAgent.InvoiceDetailAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the PatientFacility POJO.
 *
 * @author Sonia Abrol
 * @version 1.0, 09/22/2005
  */

@Stateless
public class InvoiceDetailAgentBean implements InvoiceDetailAgentRObj {

    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * Looks up on the id parameter passed in and constructs and
     * returns a InvoiceDetailBean containing all the Invoice Details
     * @param id   The Invoice Id
     */

    public InvoiceDetailBean getInvoiceDetail(int id) {
        InvoiceDetailBean pb = null; // Category Entity Bean Remote Object
        try {

           pb = (InvoiceDetailBean) em.find(InvoiceDetailBean.class, new Integer(id));

        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "Error in getInvoiceDetails() in InvoiceDetailAgentBean" + e);
        }

        return pb;
    }

    /**
     * Creates a new Invoice record
     *
     * @param invoice
     *            A InvoiceDetailBean containing patient invoice details.
     * @return The Invoice Id for the new record <br>
     *         0 - if the method fails
     */

    public int setInvoiceDetail(InvoiceDetailBean invoice) {

        try {


            InvoiceDetailBean pf = new InvoiceDetailBean();

            pf.updateInvoiceDetail(invoice);
            em.persist(pf);
            return (pf.getId());

        } catch (Exception e) {
            Rlog.fatal("Invoice",  "Error in setInvoiceDetails() in InvoiceDetailAgentBean " + e);
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Creates a new Invoice record from data from InvoiceDetailDdao
     *
     * @param inv
     *            A InvoiceDetailDao containing patient invoice details.
     * @return The Invoice Id for the last record <br>
     *         0 - if the method fails
     */
    public int setInvoiceDetails(InvoiceDetailDao inv)
    {
    	ArrayList detailType = new ArrayList<String>();
    	ArrayList amountDue = new ArrayList<String>();
    	ArrayList amountInvoiced = new ArrayList<String>();
    	ArrayList<String> holdbackFlag = new ArrayList<String>();
    	ArrayList creator = new ArrayList<String>();
    	ArrayList invPk = new ArrayList<String>();
    	ArrayList iPAdd = new ArrayList<String>();

    	ArrayList milestone = new ArrayList<String>();
    	ArrayList patient = new ArrayList<String>();
    	ArrayList patProt = new ArrayList<String>();
    	ArrayList paymentDueDate = new ArrayList<String>() ;
    	ArrayList study = new ArrayList<String>();
    	ArrayList displayDetail = new ArrayList<String>();

    	ArrayList milestonesAchievedCount = new ArrayList<String>();
    	ArrayList achDate = new ArrayList<String>();
    	ArrayList arMileAchievedPk = new ArrayList();

    	int id = 0;


    	detailType = inv.getDetailType();

    	int rows = 0;

    	rows = detailType.size();

    	if (rows > 0)
    	{

    		amountDue = inv.getAmountDue();
    		amountInvoiced = inv.getAmountInvoiced();
    		holdbackFlag = inv.getHoldbackFlag();
    		invPk = inv.getInvPk() ;
    		milestone = inv.getMilestone();
    		patient = inv.getPatient() ;
    		patProt = inv.getPatProt() ;
    		paymentDueDate = inv.getPaymentDueDate();
    		study = inv.getStudy() ;
			creator = inv.getCreator();
			iPAdd = inv.getIPAdd() ;
			displayDetail =  inv.getDisplayDetail();
			milestonesAchievedCount =  inv.getMilestonesAchievedCount();
			achDate = inv.getAchDate();
			arMileAchievedPk = inv.getLinkMileAchieved();

		    	for (int i=0; i<rows; i++)
		    	{

		    		Date dtPaymentDue = new Date();
		            dtPaymentDue  = DateUtil.stringToDate((String)paymentDueDate.get(i), null);

		            Date dtAchDate = new Date();
		            dtAchDate= DateUtil.stringToDate((String) achDate.get(i), null);

		            InvoiceDetailBean idb ;
		           if(holdbackFlag.get(i).equals("false"))
		           {idb = new InvoiceDetailBean((String) amountDue.get(i), (String) amountInvoiced.get(i), "0" , (String) creator.get(i), 0, (String)invPk.get(i), (String)iPAdd.get(i), "", (String)milestone.get(i),
		            		(String)patient.get(i), (String)patProt.get(i), dtPaymentDue ,(String)study.get(i),(String)displayDetail.get(i),(String)detailType.get(i),(String) milestonesAchievedCount.get(i), dtAchDate ,
		            		(String) arMileAchievedPk.get(i));
		        	   id = setInvoiceDetail(idb );
		           }
		           else
		           {
		        	   Query query = em.createNamedQuery("findInvDetailByMileIdAndMileAchId");
		        	   query.setParameter("FK_MILESTONE", milestone.get(i));
		        	   query.setParameter("FK_MILEACHIEVED", arMileAchievedPk.get(i));
		        	   query.setParameter("FK_INV", invPk.get(i));
		        	   ArrayList<InvoiceDetailBean> list = (ArrayList<InvoiceDetailBean>) query.getResultList();
		        	   double dueAmount=0.0;
		   				if(list!=null && !list.isEmpty())
		   					{
		   					for(int j=0;j<list.size();j++)
		   						{
		   						idb=(InvoiceDetailBean)list.get(j);
		   						dueAmount = Double.parseDouble(idb.getAmountDue());
		   						dueAmount += Double.parseDouble((String) amountDue.get(i));
		   						idb.setAmountDue(""+dueAmount);
		   						idb.setLastModifiedBy((String) creator.get(i));
		   						idb.setPaymentDueDate(dtPaymentDue);
		   						idb.setAchievedOn(dtAchDate);
		   						idb.setAmountHoldback((String) amountInvoiced.get(i));
		   						id = updateInvoiceDetail(idb);
		   						}
		   					} 
		   				else
		   					{
		   					idb = new InvoiceDetailBean((String) amountDue.get(i), "0", (String) amountInvoiced.get(i) , (String) creator.get(i), 0, (String)invPk.get(i), (String)iPAdd.get(i), "", (String)milestone.get(i),
				            		(String)patient.get(i), (String)patProt.get(i), dtPaymentDue ,(String)study.get(i),(String)displayDetail.get(i),(String)detailType.get(i),(String) milestonesAchievedCount.get(i), dtAchDate ,
				            		(String) arMileAchievedPk.get(i));
				        	id = setInvoiceDetail(idb );
		   					
		   					}
		   				
		           	 }
		           
		    	 }
		}
    		return id;
    }


    /**
     * Updates the Invoice record with data passed in InvoiceDetailBean object
     *
     * @param clsk
     *            An InvoiceDetailBean containing category attributes to be
     *            updated.
     * @return 0 - if the update is successful<br>
     *         -2 - if the update fails
     */

    public int updateInvoiceDetail(InvoiceDetailBean pb) {

        InvoiceDetailBean retrieved = null;
        int output;

        try {

            retrieved = (InvoiceDetailBean) em.find(InvoiceDetailBean.class, new Integer(pb.getId()));

            if (retrieved == null) {
                return -2;
            }

            output = retrieved.updateInvoiceDetail(pb);
            em.merge(retrieved);
        } catch (Exception e) {
            Rlog.debug("Invoice", "Error in updateInvoice in InvoiceDetailAgentBean"
                            + e);
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * Deletes an Invoice record
     * @param id  The Invoice Id
     */

    public int  deleteInvoiceDetail(int id) {
        InvoiceDetailBean pb = null;
        try {

           pb = (InvoiceDetailBean) em.find(InvoiceDetailBean.class, new Integer(id));
	   em.remove(pb);
	   return 0;
        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "Error in getInvoiceDetails() in InvoiceDetailAgentBean" + e);
	 return -2;
        }

    }

    /** returns InvoiceDetailDao with details of a generated invoice (for a milestone type)
     * @param invId : Invoice Id
     * @param milestoneType milestone type : Pm,EM,VM,SM
     * @param htAdditionalParam hashtable of additional parameters.
     * <br>possible values: key: "calledFrom" ; values: INV, PAY
     * */
    public InvoiceDetailDao getInvoiceDetailsForMilestoneType(int invId, String mileStoneType,  Hashtable htAdditionalParam)
    {
    	InvoiceDetailDao inv = new InvoiceDetailDao();
    	try {
    		inv.getInvoiceDetailsForMilestoneType(invId,mileStoneType,htAdditionalParam);
    	} catch (Exception e) {
            Rlog.fatal("Invoice",
                    "Error in getInvoiceDetailsForMilestoneType() in InvoiceDetailAgentBean" + e);
	         }

    	return inv;
    }
    
    /** returns InvoiceDetailDao with details of a generated invoice (for Event and Additional milestone type)*/
    public InvoiceDetailDao getInvoiceDetailsForMilestoneTypeEMAM(int invId, String mileStoneType,  Hashtable htAdditionalParam)
    {
    	InvoiceDetailDao inv = new InvoiceDetailDao();
    	try {
    		inv.getInvoiceDetailsForMilestoneTypeEMAM(invId,mileStoneType,htAdditionalParam);
    	} catch (Exception e) {
            Rlog.fatal("Invoice",
                    "Error in getInvoiceDetailsForMilestoneTypeEMAM() in InvoiceDetailAgentBean" + e);
	         }

    	return inv;
    }

    /**
     * Updates Invoice Details records from data from InvoiceDetailDdao
     *
     * @param inv
     *            A InvoiceDetailDao containing patient invoice details.
     * @return
     *         -1 if the method fails
     */
    public int updateInvoiceDetails(InvoiceDetailDao inv)
    {
    	ArrayList arAmountInvoiced = new ArrayList<String>();
    	ArrayList arAmountHoldback = new ArrayList<String>();
    	ArrayList arLastModifiedBy = new ArrayList<String>();
    	ArrayList arIPAdd = new ArrayList<String>();
    	ArrayList arInvPK = new ArrayList<String>();

    	ArrayList arMilestoneDetailPK = new ArrayList<String>();

    	arMilestoneDetailPK = inv.getId();

    	int rows = 0;

    	String invPkVal = "";
    	String last_modified_by = "";
    	String ipAdd = "";


    	rows = arMilestoneDetailPK.size();
    	try
    	{
	    	if (rows > 0)
	    	{

	    		arAmountInvoiced = inv.getAmountInvoiced();
	    		arAmountHoldback = inv.getAmountHoldback();
	    		arLastModifiedBy = inv.getLastModifiedBy();

	    		arIPAdd = inv.getIPAdd() ;
	    		arInvPK = inv.getInvPk();

			    	for (int i=0; i<rows; i++)
			    	{

			    		if (i == 0)
			    		{
			    			invPkVal =(String) arInvPK.get(i);
			    	    	last_modified_by = (String)arLastModifiedBy.get(i);
			    	    	ipAdd = (String)arIPAdd.get(i);

			    		}

			    		InvoiceDetailBean retrieved = null;

			    		retrieved = (InvoiceDetailBean) em.find(InvoiceDetailBean.class, new Integer((String)arMilestoneDetailPK.get(i)));

			            if (retrieved != null) {
			            	retrieved.setAmountInvoiced((String)arAmountInvoiced.get(i));
			            	retrieved.setAmountHoldback((String)arAmountHoldback.get(i));
			            	retrieved.setLastModifiedBy(last_modified_by);
			            	retrieved.setIPAdd(ipAdd);

				            em.merge(retrieved);

			            }

			    	}

			}
    	} catch (Exception e)
    	{
    		Rlog.fatal("Invoice",
                    "Exception in updateInvoiceDetails(InvoiceDetailDao inv) in InvoiceDetailAgentBean" + e);
    			return -1		;
    	}
    		return 0;
    }


}// end of class

