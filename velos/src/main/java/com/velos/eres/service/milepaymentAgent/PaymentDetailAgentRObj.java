/*
 * Classname			PaymentDetailAgentRObj
 * 
 * Version information 	1.0
 *
 * Date					10/19/2005
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.service.milepaymentAgent;

import javax.ejb.Remote;

import com.velos.eres.business.common.PaymentDetailsDao;
import com.velos.eres.business.milepayment.impl.PaymentDetailBean;

/**
 * Remote interface for MilepaymentAgent session EJB
 * 
 * @author Sonia Abrol
 */
@Remote
public interface PaymentDetailAgentRObj {
    /**
     * gets the milepayment details
     */
    public PaymentDetailBean  getPaymentDetail(int milepaymentId);

    /**
     * sets the milepayment details
     */
    public int setPaymentDetail(PaymentDetailBean  msk);

    public int updatePaymentDetail(PaymentDetailBean  ssk);
    
    /**
     * sets the milepayment details
     */
    public int setPaymentDetail(PaymentDetailsDao  pd);

    /**
     *   if payment details are added, gets payment detail records with previous totals, else 
     *  gets previous payment totals
     * @param payment the paymentPk for which detail records are retrieved. If payment == 0, then just get the totals of previous payments for the same linkToId 
     * @param linkToId the linkToId, main record type for which payment detail records are retrieved
     *      * @param linkType the type of payment link. possible values: M-Milestones, I-Invoice, B-Budgets        
     */

    public PaymentDetailsDao getPaymentDetailsWithTotals(int payment, int linkToId ,String linkType);
    
    /**
     *   get linked payment browser info
     * @param payment 
     * @param linkType        
     */

    public PaymentDetailsDao getLinkedPaymentBrowser(int payment, String linkType);
    
}
