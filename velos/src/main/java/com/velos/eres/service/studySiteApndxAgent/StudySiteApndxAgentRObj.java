/*
 * Classname			StudySiteApndxAgentRObj.class
 * 
 * Version information   
 *
 * Date					10/29/2004
 * 
 * Copyright notice
 */

package com.velos.eres.service.studySiteApndxAgent;

import java.util.Hashtable;
import javax.ejb.Remote;
import com.velos.eres.business.common.StudySiteApndxDao;
import com.velos.eres.business.studySiteApndx.impl.StudySiteApndxBean;

/**
 * Remote interface for StudySiteApndxAgent session EJB
 * 
 * @author
 */

@Remote
public interface StudySiteApndxAgentRObj {
    /**
     * gets the StudySiteApndx details
     */

    StudySiteApndxBean getStudySiteApndxDetails(int studySiteApndxId);

    /**
     * sets the StudySite Apndx details
     */

    public int setStudySiteApndxDetails(StudySiteApndxBean ask);

    public int updateStudySiteApndx(StudySiteApndxBean appsk);

    public StudySiteApndxDao getStudySitesApndxValues(int studySiteId,
            String type);

    public int removeStudySiteApndx(int studySiteApndxId);
    
    // Overloaded for INF-18183 ::: AGodara
    public int removeStudySiteApndx(int studySiteApndxId,Hashtable<String, String> auditInfo);

}
