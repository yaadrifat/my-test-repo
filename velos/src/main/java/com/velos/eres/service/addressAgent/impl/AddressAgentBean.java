/*
 * Classname			AddressAgentBean.class 
 * 
 * Version information
 *
 * Date					02/26/2001
 * 
 * Copyright notice
 */

package com.velos.eres.service.addressAgent.impl;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.address.impl.AddressBean;
import com.velos.eres.service.addressAgent.AddressAgentRObj;
import com.velos.eres.service.util.Rlog;

/**
 * The stateless session EJB acting as a facade for the entity CMP.<br>
 * <br>
 * 
 * @author Sajal
 * @version 1.0 02/26/2001
 */

@Stateless
public class AddressAgentBean implements AddressAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * Looks up on the Address id parameter passed in and constructs and returns
     * a AddressStateKeeper object, containing all the details of the address<br>
     * 
     * @param addId
     *            Address Id
     * @return AddressStateKeeper object, which holds the address details
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public AddressBean getAddressDetails(int addId) {
        AddressBean address = null;

        try {
            address = (AddressBean) em.find(AddressBean.class, new Integer(
                    addId));
        } catch (Exception e) {
            Rlog.fatal("address",
                    "Error in getAddressDetails() in AddressAgentBean" + e);
        }
        return address;
    }

    /**
     * Sets the address details
     * 
     * @param address
     *            AddressStateKeeper, which holds the address attributes to be
     *            set
     * @return the generated address id in case the address attributes are set
     *         successfully, 0 if an error occurs
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int setAddressDetails(AddressBean address) {
        try {
            AddressBean ad = new AddressBean();
            ad.updateAddress(address);
            em.persist(ad);
            return ad.getAddId();
        } catch (Exception e) {
            Rlog.fatal("address",
                    "Error in setAddressDetails() in AddressAgentBean " + e);
        }
        return 0;
    }

    /**
     * Updates the address attributes stored in AddressStateKeeper
     * 
     * @param addsk
     *            AddressStateKeeper, an object which hold the address
     *            attributes
     * @return 0 incase of successful update, -2 incase of error
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int updateAddress(AddressBean addsk) {
        AddressBean address = null;
        int output;
        try {
            address = (AddressBean) em.find(AddressBean.class, new Integer(
                    addsk.getAddId()));
            if (address == null) {
                return -2;
            }
            output = address.updateAddress(addsk);
            em.merge(address);
        } catch (Exception e) {
            Rlog.debug("address", "Error in updateAddress in AddressAgentBean"
                    + e);
            return -2;
        }
        return output;
    }

    /** deletes an address */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int removeAddress(int id) {
        int ret = 0;
        AddressBean address = null;

        try {
            address = (AddressBean) em.find(AddressBean.class, new Integer(id));
            em.remove(address);
            Rlog.debug("address", "removed address");
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("address", "EXCEPTION IN DELETING address" + e);
            return -2;
        }
    }

}// end of class

