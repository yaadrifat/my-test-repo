/*
 * Classname			PatStudyStatAgentBean.class
 * 
 * Version information
 *
 * Date					06/16/2001
 * 
 * Copyright notice
 */

package com.velos.eres.service.patStudyStatAgent.impl;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.PatStudyStatDao;
import com.velos.eres.business.patProt.impl.PatProtBean;
import com.velos.eres.business.patStudyStat.impl.PatStudyStatBean;
import com.velos.eres.service.patProtAgent.PatProtAgentRObj;
import com.velos.eres.service.patStudyStatAgent.PatStudyStatAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.web.patProt.PatProtJB;
import com.velos.eres.web.person.PersonJB;
import com.velos.eres.web.pref.PrefJB;
import com.velos.eres.web.studySite.StudySiteJB;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.StringUtil;


/**
 * The stateless session EJB acting as a facade for the entity CMP
 * PatStudyStatBean
 * 
 * @author Deepali Sachdeva
 */

@Stateless
public class PatStudyStatAgentBean implements PatStudyStatAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * returns a PatStudyStatStateKeeper object with Patient Study Status
     * Details
     * <p>
     * Calls getPatStudyStatDetails() on Entity Bean PatStudyStatBean and
     * creates and returns a PatStudyStatStateKeeper object.
     * 
     * @param id
     *            Patient Study Status Id
     * @see com.velos.eres.business.patProt.PatProtStateKeeper
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public PatStudyStatBean getPatStudyStatDetails(int id) {
        PatStudyStatBean psObj = null;
        PatProtBean patProtBean = null;
        PatProtJB ppJB = new PatProtJB();

        try {
            Rlog.debug("patstudystat", "IN TRY OF SESSION BEAN");
            psObj = (PatStudyStatBean) em.find(PatStudyStatBean.class, id);

            if (psObj == null) {
                return null;
            }

            // find out the latest enrollment.
            patProtBean = ppJB.findCurrentPatProtDetails(StringUtil
                    .stringToNum(psObj.getStudyId()), StringUtil.stringToNum(psObj
                    .getPatientId()));

            Rlog.debug("patstudystat", "GOT findByEnroll" + patProtBean);

            psObj.setPatProtObj(patProtBean);

        } catch (Exception e) {
            Rlog.fatal("patstudystat",
                    "EXCEPTION IN GETTING PATIENT STUDY STATUS" + e);
            e.printStackTrace();
        }

        return psObj;
    }

    /**
     * Sets a Patient Study status.
     * 
     * @param patient
     *            study status a state holder containing study status attributes
     *            to be set.
     * @return Returns:
     *         <ul>
     *         <li>if successful: Primary Key of the new patient study status</li>
     *         <li>For Exceptions: -2</li>
     *         <li>If 'Enrolled' status already exists and status could not be
     *         inserted: -3</li>
     *         <li>If there is no 'Enrolled' status previously entered and a
     *         'Off Study'/'Off Treatment'/'In Follwup'/<br>
     *         'Active-Enrolloing status is entered': -4 </li>
     *         </ul>
     */
    /*
     * Modified by Sonia, 09/01/04 added logic to find the latest status, and
     * set its end date to null if its not null, this way we should have atleast
     * one cuurrent status
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int setPatStudyStatDetails(PatStudyStatBean psk) {

        PatStudyStatBean patStudyStatRObj = new PatStudyStatBean();

        int output = 0;
        Enumeration enumEnrolledStatus;
        PatStudyStatBean stausFoundRObj = null;
        PatProtJB ppJB = new PatProtJB();
        ArrayList list = new ArrayList();
        Query query = null;
        int newPk = 0;
        int studyId = 0;
        String patStudyStat = "";
        Enumeration enum_velos;
        PatStudyStatBean prevCurStat = null;
        PatStudyStatBean nextStat = null;

        int patientId = 0;
        int pkEnrolledCode = 0;
        CodeDao cdStat = new CodeDao();

        String statSubtype = "";
        ArrayList arSubType = new ArrayList();
        String currentStat="";

        String latestEndDate = "";
        PatStudyStatBean latestStat = null;
        PatProtBean patProtRet = new PatProtBean();
        PatProtBean patProtSK = new PatProtBean();
        String dateFormat = Configuration.getAppDateFormat();
        if (dateFormat.indexOf("MMM") != -1)
        	dateFormat = StringUtil.replace(dateFormat,"MMM","Mon");

        try {

            // get codelist data for the patient study status

            patStudyStat = psk.getPatStudyStat();
            cdStat.getCodeValuesById(StringUtil.stringToNum(patStudyStat));
            Rlog.debug("patstudystat", "New patient study status patStudyStat"
                    + patStudyStat);
            arSubType = cdStat.getCSubType();
            Rlog.debug("patstudystat",
                    "New patient study status arSubType.size() "
                            + arSubType.size());
            if (arSubType.size() > 0) {
                statSubtype = (String) arSubType.get(0);
            }
            Rlog.debug("patstudystat",
                    "New patient study status arSubType.size() " + statSubtype);

            pkEnrolledCode = cdStat.getCodeId("patStatus", "enrolled");

            if (statSubtype.equals("enrolled") || statSubtype.equals("active")
                    || statSubtype.equals("followup")
                    || statSubtype.equals("offtreat")
                    || statSubtype.equals("offstudy") || statSubtype.equals("lockdown")) {
                // check if there is an existing 'enrolled' status

                query = em
                        .createNamedQuery("findByPatientStudyStatusCodeSubType");
                query.setParameter("fk_study", psk.getStudyId());
                query.setParameter("fk_per", psk.getPatientId());
                query.setParameter("fk_codelst_status", pkEnrolledCode);
                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    if (statSubtype.equals("enrolled")) {
                        stausFoundRObj = (PatStudyStatBean) list.get(0);
                        Rlog.debug("patstudystat",
                                "New patient study status enumEnrolledStatus got statatsu"
                                        + stausFoundRObj);
                        // a previous patient status found
                        if (stausFoundRObj != null)
                            return -3;
                    }
                } else {
                    if (!statSubtype.equals("enrolled") && (!statSubtype.equals("lockdown"))) {
                        // there is no enrolled and lockdown status
                        return -4;
                    }

                }
            }
            currentStat = psk.getCurrentStat();
            if("0".equalsIgnoreCase(currentStat))
            	psk.setEndDate(psk.getStartDate());
            patStudyStatRObj.updatePatStudyStat(psk);
            em.persist(patStudyStatRObj);

            // /////// manage status end dates

            // find the last current status and update its end date

            newPk = patStudyStatRObj.getId();
            studyId = StringUtil.stringToNum(psk.getStudyId());
            patientId = StringUtil.stringToNum(psk.getPatientId());
            Rlog.debug("patstudystat", "Start of finders ");
            if("1".equalsIgnoreCase(currentStat)){
            query = em.createNamedQuery("findByPreviousCurrentStatusPat");
            query.setParameter("fk_study", psk.getStudyId());
            query.setParameter("fk_per", psk.getPatientId());
            query.setParameter("begindate", psk.returnStartDate());
            query.setParameter("pk_patstudystat", newPk);
            query.setParameter("date_format",dateFormat);
            list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            if (list.size() > 0) {
            	for(int k=0;k<list.size();k++){
                prevCurStat = (PatStudyStatBean) list.get(k);

                // a previous current status found, update its end date with the
                // new status's start date

                prevCurStat.updateEndDate(psk.returnStartDate());
                prevCurStat.setCurrentStat("0");
                prevCurStat.setModifiedBy(psk.getModifiedBy());
            	}

            } else // the start date of this status exists between 2 existing
            // status records or is the first record
            {

                // get the previous record to change its end date

                // find the a previous status from this new start date and
                // update the previous status's end date

                Rlog.debug("patstudystat",
                        "Start of finders findByPreviousStatusPat");

                query = em.createNamedQuery("findByPreviousStatusPat");
                query.setParameter("fk_study", psk.getStudyId());
                query.setParameter("fk_per", psk.getPatientId());
                query.setParameter("begindate", psk.returnStartDate());
                query.setParameter("pk_patstudystat", newPk);
                query.setParameter("date_format",dateFormat);
                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    prevCurStat = (PatStudyStatBean) list.get(0);

                    // a previous current status found, update its end date with
                    // the new status's start date

                    prevCurStat.updateEndDate(psk.returnStartDate());

                }

                //
                Rlog.debug("patstudystat", "Start of finders findByNextStatusPat");

                query = em.createNamedQuery("findByNextStatusPat");
                query.setParameter("fk_study", psk.getStudyId());
                query.setParameter("fk_per", psk.getPatientId());
                query.setParameter("begindate", psk.returnStartDate());
                query.setParameter("pk_patstudystat", newPk);
                query.setParameter("date_format",dateFormat);
                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    nextStat = (PatStudyStatBean) list.get(0);
                    // a next status found,
                    // update the new status's end date with the next status's
                    // start date
                    patStudyStatRObj.updateEndDate(nextStat.returnStartDate());

                }

              }
            }
            // /////////////////////////////////////
            // find out the latest enrollment.
            Rlog.debug("patstudystat", "End of finders findByNextStatusPat");

            Rlog
                    .debug("patstudystat",
                            "New patient study status got home before find enrollment ");
            try {
                patProtRet = ppJB.findCurrentPatProtDetails(StringUtil
                        .stringToNum(psk.getStudyId()), StringUtil.stringToNum(psk
                        .getPatientId()));

            } catch (Exception ex) {
                // the finder failed, no remote obj found
                patProtRet = null;

            }

            Rlog.debug("patstudystat",
                    "New patient study status find enrollment ");

            // if you get an enrollment record, update it with the attributes
            // passed
            // else create a new enrollment record

            patProtSK = psk.getPatProtObj();

            if (patProtSK != null) {
                Rlog.debug("patstudystat", "got enrollment patProtRet"
                        + patProtRet);
                if (patProtRet != null && (patProtSK.getPatProtId() > 0)) {
                    Rlog.debug("patstudystat", "update enrollment");
                    PatProtAgentRObj patProtAgentRObj = EJBUtil
                            .getPatProtAgentHome();
                    output = patProtAgentRObj.updatePatProt(patProtSK);
                    // em.merge(patProtRet);

                } else {
                    // patProtRet = new PatProtBean();
                    Rlog.debug("patstudystat", "create enrollment");
                    /*
                     * patProtRet.updatePatProt(patProtSK);
                     * em.persist(patProtRet);
                     */
                    PatProtAgentRObj patProtAgentRObj = EJBUtil
                            .getPatProtAgentHome();
                    output = patProtAgentRObj.setPatProtDetails(patProtSK);

                    // its a new enrollment, update the enrollment count in
                    // studysite
                    StudySiteJB ssJB = new StudySiteJB();
                    PrefJB pJB = new PrefJB();
                    String site = "";
                    int curEnrCount = 0;

                    // get patient site info

                    pJB.setPrefPKId(patientId);
                    pJB.getPrefDetails();
                    site = pJB.getPrefLocation();

                    ssJB.setStudyId(psk.getStudyId());
                    ssJB.setSiteId(site);
                    ssJB.findBeanByStudySite();
                    curEnrCount = StringUtil.stringToNum(ssJB
                            .getCurrentSitePatientCount());
                    curEnrCount = curEnrCount + 1;
                    ssJB
                            .setCurrentSitePatientCount(String
                                    .valueOf(curEnrCount));
                    ssJB.setModifiedBy(psk.getModifiedBy());
                    ssJB.updateStudySite();

                }
            }

            // find the latest status, and set its end date to null if its not
            // null

            try {
                query = em.createNamedQuery("findByLatestStatusPat");
                query.setParameter("fk_study", psk.getStudyId());
                query.setParameter("fk_per", psk.getPatientId());
                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    latestStat = (PatStudyStatBean) list.get(0);
                    // a latest status found, get its end date
                    latestEndDate = latestStat.returnEndDate();
                    // if end date is not empty, set it to empty
                    if (!StringUtil.isEmpty(latestEndDate) && "1".equalsIgnoreCase(latestStat.getCurrentStat())) {
                        latestStat.updateEndDate("");
                    }
                }

            } catch (EJBException ex) {
                latestStat = null;
            }
            if (output < 0)
                return output;
            else
                return patStudyStatRObj.getId();

        } catch (Exception e) {
            Rlog.fatal("patstudystat",
                    "Exception in PatStudyStatAgentBean.setPatStudyStatDetails()"
                            + e);
            e.printStackTrace();
            return -2;
        }
    }

    /**
     * Updates a Patient Study status .
     * 
     * @param psk
     *            A PatStudyStatStateKeeper object
     * @return 0 if successful and -2 in case of any Exception
     */
    /*
     * Modified by Sonia, 09/01/04 added logic to find the latest status, and
     * set its end date to null if its not null, this way we should have atleast
     * one cuurrent status
     */
    /*
     * Modified by Sonia, 09/14/04 when finding the next status do not set its
     * stataus end date to null (if any status is found)
     */

    // updates study status
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int updatePatStudyStat(PatStudyStatBean psk) {
        PatStudyStatBean psObj = null; // Study Status Entity Bean Remote
        // Object
        ArrayList list = new ArrayList();
        Query query = null;
        int ret = 0;
        PatProtBean patProtSK = new PatProtBean();
        int output = 0;
        String oldStartDate = "";
        String newStartDate = "";
        String oldEndDate = "";
        Enumeration enum_velos;
        PatStudyStatBean prevCurStat = null;
        PatStudyStatBean nextStat = null;
        int studyId = 0;
        int patientId = 0;
        String latestEndDate = "";
        PatStudyStatBean latestStat = null;
        PatProtJB ppJb = new PatProtJB();
        String dateFormat = Configuration.getAppDateFormat();
        if (dateFormat.indexOf("MMM") != -1)
        	dateFormat = StringUtil.replace(dateFormat,"MMM","Mon");

        try {
            Rlog.debug("patstudystat", "IN TRY OF SESSION BEAN:update");
            psObj = (PatStudyStatBean) em.find(PatStudyStatBean.class, psk
                    .getId());

            // ////////////////manage status end dates
            // get old start date and end date
            oldStartDate = psObj.returnStartDate();
            oldEndDate = psObj.returnEndDate();
            // get new start date

            newStartDate = psk.returnStartDate();
            ret = psObj.updatePatStudyStat(psk);
            em.merge(psObj);
            // if there is a change in the start date

            if (!newStartDate.equals(oldStartDate)) {

                studyId = StringUtil.stringToNum(psk.getStudyId());
                patientId = StringUtil.stringToNum(psk.getPatientId());

                // find the a previous status from this new start date and
                // update the previous status's end date
                query = em.createNamedQuery("findByPreviousStatusPat");
                query.setParameter("fk_study", studyId);
                query.setParameter("fk_per", patientId);
                query.setParameter("begindate", psk.returnStartDate());
                query.setParameter("pk_patstudystat", psk.getId());
                query.setParameter("date_format",dateFormat);
                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    prevCurStat = (PatStudyStatBean) list.get(0);

                    // a previous current status found, update its end date with
                    // the new status's start date
                    prevCurStat.setModifiedBy(psk.getModifiedBy()); 
                    prevCurStat.updateEndDate(newStartDate);

                }

                // see if the the start date of this status exists between 2
                // existing status records

                query = em.createNamedQuery("findByNextStatusPat");
                query.setParameter("fk_study", psk.getStudyId());
                query.setParameter("fk_per", psk.getPatientId());
                query.setParameter("begindate", psk.returnStartDate());
                query.setParameter("pk_patstudystat", psk.getId());
                query.setParameter("date_format",dateFormat);
                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    nextStat = (PatStudyStatBean) list.get(0);
                    // a next status found,
                    // update the new status's end date with the next status's
                    // start date
                    psObj.updateEndDate(nextStat.returnStartDate());

                    /*
                     * not required handled later // if oldEndDate is null, that
                     * means it was a latest status, //update the end date of
                     * next status to null to make the next stat current if
                     * (StringUtil.isEmpty(oldEndDate)) {
                     * nextStat.setEndDate(null); }
                     */

                } else // now this status has become the current status because
                // there is no next record
                {
                    psObj.setEndDate(null);
                }

                // find the latest status, and set its end date to null if its
                // not null

                try {

                    query = em.createNamedQuery("findByLatestStatusPat");
                    query.setParameter("fk_study", psk.getStudyId());
                    query.setParameter("fk_per", psk.getPatientId());
                    list = (ArrayList) query.getResultList();
                    if (list == null)
                        list = new ArrayList();
                    if (list.size() > 0) {
                        latestStat = (PatStudyStatBean) list.get(0);
                        // a latest status found, get its end date
                        latestEndDate = latestStat.returnEndDate();
                        // if end date is not empty, set it to empty
                        if (!StringUtil.isEmpty(latestEndDate)) {
                            latestStat.updateEndDate("");
                        }
                    }
                } catch (EJBException ex) {
                    latestStat = null;
                }

            } // end of if for checking if status is changed

            em.flush();
            // ///////////////////////////////////////////////////////////////
            // get the enrollment

            patProtSK = psk.getPatProtObj();

            if (patProtSK != null) {
            	// if you get an enrollment record, update it with the
                // attributes passed
                // else create a new enrollment record
            	
            	if (patProtSK.getPatProtId() > 0)
            	{
	            	ppJb.setPatProtId(patProtSK.getPatProtId());
	            	ppJb.getPatProtDetails();
	            	ppJb.setJBFromStateKeeper(patProtSK);
	            	ppJb.updatePatProt();
            	}
                /*patProtRet = (PatProtBean) em.find(PatProtBean.class, patProtSK
                        .getPatProtId());
           
                if (patProtRet != null && patProtRet.getPatProtId() > 0) {
                    output = patProtRet.updatePatProt(patProtSK);
                    em.merge(patProtRet);
                }*/
                
            }

            Rlog.debug("patstudystat", "updates patient study state keeper"
                    + psk);
            return ret;
        } catch (Exception e) {
            // e.printStackTrace();
            Rlog.fatal("patstudystat",
                    "EXCEPTION IN UPDATING PATIENT STUDY STATUS" + e);
            return -2;
        }
    }

    public PatStudyStatDao getStudyPatients(String studyId) {
        try {
            Rlog.debug("patstudystat",
                    "PatStudyStatAgentBean.getStudyPatients line number 0");
            PatStudyStatDao pssDao = new PatStudyStatDao();
            Rlog.debug("patstudystat",
                    "PatStudyStatAgentBean.getStudyPatients line number 1");
            pssDao.getStudyPatients(studyId);
            Rlog.debug("patstudystat",
                    "PatStudyStatAgentBean.getStudyPatients line number 2");
            return pssDao;
        } catch (Exception e) {
            Rlog.fatal("patstudystat",
                    "Exception In getStudyPatients in PatStudyStatAgentBean "
                            + e);
        }
        return null;

    }

    /**
     * Looks for patient's current study status and returns a
     * PatStudyStatStateKeeper object
     * 
     * @param studyId
     *            Study Id
     * @param patientId
     *            Patient Id
     * @return A PatStudyStatStateKeeper object if a current status is found,
     *         null if not found or if<br>
     *         there was any exception
     */
    public PatStudyStatBean getPatStudyCurrentStatus(String studyId,
            String patientId) {

        PatStudyStatBean psk = null;
        ArrayList list = new ArrayList();
        Query query = null;

        try {
            query = em.createNamedQuery("findByCurrentPatientStudyStatus");
            query.setParameter("fk_study", String.valueOf(studyId));
            query.setParameter("fk_per", String.valueOf(patientId));
            list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            if (list.size() > 0) {
                psk = (PatStudyStatBean) list.get(0);
                // a current status found
            }

            Rlog
                    .debug(
                            "patstudystat",
                            "PatStudyStatAgentBean.getPatStudyCurrentStatus: GOT PATIENT STUDY STATUS KEEPER"
                                    + psk);
        } catch (Exception e) {
            Rlog
                    .fatal(
                            "patstudystat",
                            "PatStudyStatAgentBean.getPatStudyCurrentStatus:EXCEPTION IN GETTING PATIENT STUDY STATUS"
                                    + e);
        }

        return psk;
    }

    public int getPkForStatusCodelstSubType(int studyId, int patId,
            String codeSubType) {
        PatStudyStatBean statusRObj = null;
        int pkPatStudyStat = 0;
        ArrayList list = new ArrayList();
        Query query = null;
        int pkEnrolledCode = 0;
        CodeDao cdStat = new CodeDao();

        try {
            pkEnrolledCode = cdStat.getCodeId("patStatus", codeSubType);

            query = em.createNamedQuery("findByPatientStudyStatusCodeSubType");
            query.setParameter("fk_study", String.valueOf(studyId));
            query.setParameter("fk_per", String.valueOf(patId));
            query.setParameter("fk_codelst_status", pkEnrolledCode);

            list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            if (list.size() > 0) {
                statusRObj = (PatStudyStatBean) list.get(0);
                Rlog.debug("patstudystat",
                        "getPkForStatusCodelstSubType in patstudystaAgentBean statusRObj"
                                + statusRObj);
                // a previous patient status found
                if (statusRObj != null)
                    Rlog.debug("patstudystat", "inside statusRObj != null");
                pkPatStudyStat = statusRObj.getId();

            }
        } catch (Exception e) {
            Rlog
                    .fatal(
                            "patstudystat",
                            "PatStudyStatAgentBean.getPatStudyCurrentStatus:EXCEPTION IN GETTING PATIENT STUDY STATUS"
                                    + e);
        }
        return pkPatStudyStat;
    }

    // /////////

    /**
     * Removes Patient Study Status
     * 
     * @param id
     *            Patient Study Status Id to be deleted
     * @return int :0 if successful <br>
     *         -1 if unsuccessful <br>
     *         -3 if the status can not be deleted as this is the only patient
     *         study status left
     */
    /*
     * Modified by Sonia, 09/01/04 added logic to find the latest status, and
     * set its end date to null if its not null, this way we should have atleast
     * one cuurrent status
     */
    /*
     * Modified by Sonia, 09/13/04 patient study id will not be reset to null
     * when an enrolled status is deleted
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int reomovePatStudyStat(int id) {
        PatStudyStatBean ssObj = null;
        PatStudyStatBean otherPatStudyStatRObj = null;

        ArrayList list = new ArrayList();
        Query query = null;

        // PatStudyStatBean psk = null;
        int studyId = 0;
        int patientId = 0;

        String nextStartDate = "";

        PatStudyStatBean prevCurStat = null;
        PatStudyStatBean nextStat = null;
        String patStudyStat = null;
        CodeDao cdStat = new CodeDao();
        ArrayList arSubType = new ArrayList();
        PatProtBean patProtSK = null;
        PatProtBean patProtRet = null;
        String statSubtype = "";
        int output = 0;
        String latestEndDate = "";
        PatStudyStatBean latestStat = null;
        PatProtJB ppJB = new PatProtJB();
        String dateFormat = Configuration.getAppDateFormat();
        if (dateFormat.indexOf("MMM") != -1)
        	dateFormat = StringUtil.replace(dateFormat,"MMM","Mon");
        try {
            ssObj = (PatStudyStatBean) em.find(PatStudyStatBean.class, id);

            if (ssObj != null) {
                // check if there is atleast one more patient study status
                // besides the one that is passed

                try {

                    query = em.createNamedQuery("findByAtLeastOneStatusPat");
                    query.setParameter("fk_study", ssObj.getStudyId());
                    query.setParameter("fk_per", ssObj.getPatientId());
                    query.setParameter("pk_patstudystat", id);

                    list = (ArrayList) query.getResultList();
                    if (list == null)
                        list = new ArrayList();
                    if (list.size() > 0) {
                        otherPatStudyStatRObj = (PatStudyStatBean) list.get(0);
                    }
                } catch (Exception ex) {
                    // no other status found
                    return -3;
                }

                if (otherPatStudyStatRObj == null) {
                    return -3;
                }

                patStudyStat = ssObj.getPatStudyStat();
                cdStat.getCodeValuesById(StringUtil.stringToNum(patStudyStat));

                arSubType = cdStat.getCSubType();

                if (arSubType.size() > 0) {
                    statSubtype = (String) arSubType.get(0);
                }

                if (statSubtype.equals("enrolled")) {
                    // find current enrollment

                    try {
                        patProtRet = ppJB.findCurrentPatProtDetails(StringUtil
                                .stringToNum(ssObj.getStudyId()), StringUtil
                                .stringToNum(ssObj.getPatientId()));

                    } catch (Exception ex) {
                        // the finder failed, no remote obj found
                        patProtRet = null;

                    }

                    // if you get an enrollment
                    // else create a new enrollment record

                    if (patProtRet != null && patProtRet.getPatProtId() > 0) {

                        Rlog.debug("patstudystat", "update enrollment");
                        // patProtSK.setPatStudyId("");
                        patProtRet.setPatProtRandomNumber("");
                        patProtRet.updatePatProtEnrolDt(null);
                        patProtRet.setPatProtUserId(null);
                        
                        //changed by sonia abrol, 12/16/05
                        //set enrolling site with patient's default site
                        
//                      commented by SA,08/14/08 - bug#2374
                        //PersonJB personB = new  PersonJB();
                        //personB.setPersonPKId(StringUtil.stringToNum(ssObj.getPatientId()));
                     	//personB.getPersonDetails();
                     	
                     	
                        //patProtRet.setEnrollingSite(personB.getPersonLocation());
                     	
                        
                        PatProtAgentRObj patProtAgentRObj = EJBUtil
                                .getPatProtAgentHome();
                        patProtAgentRObj.updatePatProt(patProtRet);

                        // em.merge(patProtRet);

                    }

                } // if status is enrolled

                // /////////////////
                // update end dates
                studyId = StringUtil.stringToNum(ssObj.getStudyId());
                patientId = StringUtil.stringToNum(ssObj.getPatientId());

                // see if the the start date of the deleted status exists
                // between 2 existing status records

                query = em.createNamedQuery("findByNextStatusPat");
                query.setParameter("fk_study", String.valueOf(studyId));
                query.setParameter("fk_per", String.valueOf(patientId));
                query.setParameter("begindate", ssObj.returnStartDate());
                query.setParameter("pk_patstudystat", id);
                query.setParameter("date_format",dateFormat);
                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    nextStat = (PatStudyStatBean) list.get(0);
                    // a next status found,
                    // get next start date
                    nextStartDate = nextStat.returnStartDate();

                }

                // find the a previous status from the deleted start date and
                // update the previous status's end date

                query = em.createNamedQuery("findByPreviousStatusPat");
                query.setParameter("fk_study", String.valueOf(studyId));
                query.setParameter("fk_per", String.valueOf(patientId));
                query.setParameter("begindate", ssObj.returnStartDate());
                query.setParameter("pk_patstudystat", id);
                query.setParameter("date_format",dateFormat);
                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {

                    prevCurStat = (PatStudyStatBean) list.get(0);

                    // a previous current status found, update its end date with
                    // the
                    // start date of the nextstatus found from the deleted
                    // status record

                    prevCurStat.updateEndDate(nextStartDate);

                }

                em.remove(ssObj);

                // find the latest status, and set its end date to null if its
                // not null

                try {
                    query = em.createNamedQuery("findByLatestStatusPat");
                    query.setParameter("fk_study", String.valueOf(studyId));
                    query.setParameter("fk_per", String.valueOf(patientId));
                    list = (ArrayList) query.getResultList();
                    if (list == null)
                        list = new ArrayList();
                    if (list.size() > 0) {
                        latestStat = (PatStudyStatBean) list.get(0);
                        // a latest status found, get its end date
                        latestEndDate = latestStat.returnEndDate();
                        // if end date is not empty, set it to empty
                        if (!StringUtil.isEmpty(latestEndDate)) {
                            latestStat.updateEndDate("");
                        }
                    }

                } catch (EJBException ex) {
                    latestStat = null;
                }

            }
            return 0;
        } catch (Exception e) {
            Rlog.fatal("patstudystat",
                    "Exception in PatStudyStatAgentBean.reomovePatStudyStat()"
                            + e);
            return -1;
        }

    }
    
    // Overloaded for INF-18183 : Raviesh
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int reomovePatStudyStat(int id,Hashtable<String, String> args) {
        PatStudyStatBean ssObj = null;
        PatStudyStatBean otherPatStudyStatRObj = null;

        ArrayList list = new ArrayList();
        Query query = null;

        // PatStudyStatBean psk = null;
        int studyId = 0;
        int patientId = 0;

        String nextStartDate = "";

        PatStudyStatBean prevCurStat = null;
        PatStudyStatBean nextStat = null;
        String patStudyStat = null;
        CodeDao cdStat = new CodeDao();
        ArrayList arSubType = new ArrayList();
        PatProtBean patProtSK = null;
        PatProtBean patProtRet = null;
        String statSubtype = "";
        int output = 0;
        String latestEndDate = "";
        PatStudyStatBean latestStat = null;
        PatProtJB ppJB = new PatProtJB();
        String dateFormat = Configuration.getAppDateFormat();
        if (dateFormat.indexOf("MMM") != -1)
        	dateFormat = StringUtil.replace(dateFormat,"MMM","Mon");
        try {
        	AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appmodule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/
            String getRidValue= AuditUtils.getRidValue("ER_PATSTUDYSTAT","eres","PK_PATSTUDYSTAT="+id);/*Fetches the RID/PK_VALUE*/ 
            audit = new AuditBean("ER_PATSTUDYSTAT",String.valueOf(id),getRidValue,
        			userID,currdate,appmodule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
        	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
        	
            ssObj = (PatStudyStatBean) em.find(PatStudyStatBean.class, id);
            if(ssObj.getModifiedBy()!=null){
            	 ssObj.setModifiedBy(userID);
            }
           

            if (ssObj != null) {
                // check if there is atleast one more patient study status
                // besides the one that is passed

                try {

                    query = em.createNamedQuery("findByAtLeastOneStatusPat");
                    query.setParameter("fk_study", ssObj.getStudyId());
                    query.setParameter("fk_per", ssObj.getPatientId());
                    query.setParameter("pk_patstudystat", id);

                    list = (ArrayList) query.getResultList();
                    if (list == null)
                        list = new ArrayList();
                    if (list.size() > 0) {
                        otherPatStudyStatRObj = (PatStudyStatBean) list.get(0);
                    }
                } catch (Exception ex) {
                    // no other status found
                    return -3;
                }

                if (otherPatStudyStatRObj == null) {
                    return -3;
                }

                patStudyStat = ssObj.getPatStudyStat();
                cdStat.getCodeValuesById(StringUtil.stringToNum(patStudyStat));

                arSubType = cdStat.getCSubType();

                if (arSubType.size() > 0) {
                    statSubtype = (String) arSubType.get(0);
                }

                if (statSubtype.equals("enrolled")) {
                    // find current enrollment

                    try {
                        patProtRet = ppJB.findCurrentPatProtDetails(StringUtil
                                .stringToNum(ssObj.getStudyId()), StringUtil
                                .stringToNum(ssObj.getPatientId()));

                    } catch (Exception ex) {
                        // the finder failed, no remote obj found
                        patProtRet = null;

                    }

                    // if you get an enrollment
                    // else create a new enrollment record

                    if (patProtRet != null && patProtRet.getPatProtId() > 0) {

                        Rlog.debug("patstudystat", "update enrollment");
                        // patProtSK.setPatStudyId("");
                        patProtRet.setPatProtRandomNumber("");
                        patProtRet.updatePatProtEnrolDt(null);
                        patProtRet.setPatProtUserId(null);
                        patProtRet.setModifiedBy(userID);
                        
                        //changed by sonia abrol, 12/16/05
                        //set enrolling site with patient's default site
                        
//                      commented by SA,08/14/08 - bug#2374
                        //PersonJB personB = new  PersonJB();
                        //personB.setPersonPKId(StringUtil.stringToNum(ssObj.getPatientId()));
                     	//personB.getPersonDetails();
                     	
                     	
                        //patProtRet.setEnrollingSite(personB.getPersonLocation());
                     	
                        
                        PatProtAgentRObj patProtAgentRObj = EJBUtil
                                .getPatProtAgentHome();
                        patProtAgentRObj.updatePatProt(patProtRet);

                        // em.merge(patProtRet);

                    }

                } // if status is enrolled

                // /////////////////
                // update end dates
                studyId = StringUtil.stringToNum(ssObj.getStudyId());
                patientId = StringUtil.stringToNum(ssObj.getPatientId());

                // see if the the start date of the deleted status exists
                // between 2 existing status records

                query = em.createNamedQuery("findByNextStatusPat");
                query.setParameter("fk_study", String.valueOf(studyId));
                query.setParameter("fk_per", String.valueOf(patientId));
                query.setParameter("begindate", ssObj.returnStartDate());
                query.setParameter("pk_patstudystat", id);
                query.setParameter("date_format",dateFormat);
                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    nextStat = (PatStudyStatBean) list.get(0);
                    // a next status found,
                    // get next start date
                    nextStartDate = nextStat.returnStartDate();

                }

                // find the a previous status from the deleted start date and
                // update the previous status's end date

                query = em.createNamedQuery("findByPreviousStatusPat");
                query.setParameter("fk_study", String.valueOf(studyId));
                query.setParameter("fk_per", String.valueOf(patientId));
                query.setParameter("begindate", ssObj.returnStartDate());
                query.setParameter("pk_patstudystat", id);
                query.setParameter("date_format",dateFormat);
                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {

                    prevCurStat = (PatStudyStatBean) list.get(0);

                    // a previous current status found, update its end date with
                    // the
                    // start date of the nextstatus found from the deleted
                    // status record
                    prevCurStat.setModifiedBy(userID);
                    prevCurStat.updateEndDate(nextStartDate);

                }

                em.remove(ssObj);

                // find the latest status, and set its end date to null if its
                // not null

                try {
                    query = em.createNamedQuery("findByLatestStatusPat");
                    query.setParameter("fk_study", String.valueOf(studyId));
                    query.setParameter("fk_per", String.valueOf(patientId));
                    list = (ArrayList) query.getResultList();
                    if (list == null)
                        list = new ArrayList();
                    if (list.size() > 0) {
                        latestStat = (PatStudyStatBean) list.get(0);
                        // a latest status found, get its end date
                        latestEndDate = latestStat.returnEndDate();
                        // if end date is not empty, set it to empty
                        if (!StringUtil.isEmpty(latestEndDate)) {
                            latestStat.updateEndDate("");
                        }
                    }

                } catch (EJBException ex) {
                    latestStat = null;
                }

            }
            return 0;
        } catch (Exception e) {
            Rlog.fatal("patstudystat",
                    "Exception in PatStudyStatAgentBean.reomovePatStudyStat()"
                            + e);
            return -1;
        }

    }
    
    //Added by Manimaran to prompt if PatientStudyId being entered is duplicate
    public int getCountPatStudyId(int studyId,String patStudyId) {
        int count = 0;
        try {
            PatStudyStatDao statDao = new PatStudyStatDao();
            count = statDao.getCountPatStudyId(studyId,patStudyId);
            
        } catch (Exception e) {
            Rlog.fatal("patstudystat", "Exception In getCountPatStudyId in PatStudyStatAgentBean " + e);
        }
        return count;
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public PatStudyStatBean getLatestPatStudyStat(int studyId, int patientId)
    {
    	Query query = null;
    	ArrayList list = new ArrayList();
    	PatStudyStatBean pss = null; 
        try {
            query = em.createNamedQuery("findByLatestStatusPat");
            query.setParameter("fk_study", studyId);
            query.setParameter("fk_per", patientId);
            list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            if (list.size() > 0) {
                pss = (PatStudyStatBean) list.get(0);
                // a latest status found, get its end date               
            }

        } catch (EJBException ex) {
            pss = null;
        }
        return pss; 
    }

    // /////////

}// end of class
