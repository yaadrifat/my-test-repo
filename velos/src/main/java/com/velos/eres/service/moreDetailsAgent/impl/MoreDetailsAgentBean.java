package com.velos.eres.service.moreDetailsAgent.impl;

/* Import Statements */

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.MoreDetailsDao;
import com.velos.eres.business.moreDetails.impl.MoreDetailsBean;
import com.velos.eres.service.moreDetailsAgent.MoreDetailsAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */


@Stateless
public class MoreDetailsAgentBean implements MoreDetailsAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public MoreDetailsBean getMoreDetailsBean(int id) {
        MoreDetailsBean sb = null;
        try {

            sb = (MoreDetailsBean) em.find(MoreDetailsBean.class, new Integer(id));
            return sb;

        } catch (Exception e) {
            Rlog.fatal("moredetails",
                    "Exception in getMoreDetails() in MoreDetailsAgentBean" + e);
            return null;
        }

    }

    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int setMoreDetails(MoreDetailsBean mdb) {
        try {
            em.merge(mdb);
            return mdb.getId();
        } catch (Exception e) {
            Rlog.fatal("moredetails",
                    "Error in setMoreDetails() in MoreDeatilsAgentBean " + e);
            return -2;
        }

    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int setMoreDetails(MoreDetailsBean mdb,boolean serviceFlag) {
        try {
            em.merge(mdb);
            if(serviceFlag){
            	em.flush();
            }
            return mdb.getId();
        } catch (Exception e) {
            Rlog.fatal("moredetails",
                    "Error in setMoreDetails() in MoreDeatilsAgentBean " + e);
            return -2;
        }

    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int updateMoreDetails(MoreDetailsBean mdb) {

        MoreDetailsBean mb = null; // Entity Bean Remote Object
        int output;

        try {
            mb = (MoreDetailsBean) em.find(MoreDetailsBean.class, new Integer(mdb
                    .getId()));

            if (mb == null) {
                return -2;
            }
            output = mb.updateMoreDetails(mdb);

        } catch (Exception e) {
            Rlog.fatal("moredetails", "Error in updateMoreDetails in MoreDetailsAgentBean"
                    + e);
            return -2;
        }
        return output;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int removeMoreDetails(int id) {
        int output;
        MoreDetailsBean mb = null;

        try {
            mb = (MoreDetailsBean) em.find(MoreDetailsBean.class, new Integer(id));
            em.remove(mb);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("moredetails", "Exception in removemoreDetails" + e);
            return -1;

        }

    }

    /**
     * Creates multiple studyids
     * 
     * @param StudyIdStateKeeper
     * @return int
     */
    public int createMultipleMoreDetails(MoreDetailsBean mdb, String defUserGroup ) {
        try {
        	System.out.println("Inside createMultipleMoreDetails");
              MoreDetailsDao mdDao = new MoreDetailsDao();

            ArrayList mdbList = mdb.getMoreDetailBeans();

            ArrayList mdElementIds = new ArrayList();
            ArrayList recordTypeList = new ArrayList();
            ArrayList moreDetailsPks = new ArrayList();
            int rows = mdbList.size();
            System.out.println("rows==="+rows);
            int mdModId = 0, index = -1, idPk = 0;
            String mdModName="";
            if (rows > 0) {
                mdModId = StringUtil.stringToNum(((MoreDetailsBean) mdbList.get(0))
                        .getModId());
                mdModName = (((MoreDetailsBean) mdbList.get(0))
                        .getModName());

                
            }
            mdDao = getMoreDetails(mdModId,mdModName, defUserGroup );
            mdElementIds = mdDao.getMdElementIds();
            recordTypeList = mdDao.getRecordTypes();
            moreDetailsPks = mdDao.getId();
            
            int ret = 0;
            String recordType = "";
            String recType = "";
            String mdElementId = "";

            for (int i = 0; i < rows; i++) {
                MoreDetailsBean mdbTemp = new MoreDetailsBean();

                mdbTemp = (MoreDetailsBean) mdbList.get(i);

                recordType = mdbTemp.getRecordType();
                mdElementId = mdbTemp.getModElementId();
                
                index = mdElementIds.indexOf(new Integer(mdElementId));
               
                if (index >= 0) {
                    recordType = (String) recordTypeList.get(index);
                    idPk = ((Integer) moreDetailsPks.get(index)).intValue();
                }
               
                if (recordType == null)
                    recordType = "";
                if (recordType.length() == 0)
                    recordType = "N";
              
                System.out.println("recordType==="+recordType);
                if (recordType.equals("N"))
                	if(defUserGroup.equals("-1")){
                		System.out.println("Inside Boolean Flag");
                		ret = setMoreDetails(mdbTemp,true);
                	}else{
                		ret = setMoreDetails(mdbTemp);
                	}
                else if (recordType.equals("M")) {
                    mdbTemp.setId(idPk);
                    mdbTemp.setRecordType("M");
                    ret = updateMoreDetails(mdbTemp);
                }

              
            }

            return ret;
        } catch (Exception e) {
            Rlog.fatal("moredetails",
                    "Exception In createMultipleMoreDetails in MoreDetailsAgentBean "
                            + e);
            e.printStackTrace();
            return -2;
        }

    }

    
    public int updateMultipleMoreDetails(MoreDetailsBean mdb) {
        try {
           

            ArrayList mdbList = mdb.getMoreDetailBeans();

            int rows = mdbList.size();
            int ret = 0;

            for (int i = 0; i < rows; i++) {

                MoreDetailsBean mdbTemp = new MoreDetailsBean();
                mdbTemp = (MoreDetailsBean) mdbList.get(i);

                
                ret = updateMoreDetails(mdbTemp);
               
            }

            return ret;
        } catch (Exception e) {
            Rlog.fatal("moredetails",
                    "Exception MoreDetailsAgentBean.updateMultipleMoreDetails " + e);
            e.printStackTrace();
            return -2;
        }

    }

    public MoreDetailsDao getMoreDetails(int modId,String modName, String defUserGroup ) {

        try {
            MoreDetailsDao mdDao = new MoreDetailsDao();
            mdDao.getMoreDetails(modId, modName, defUserGroup );
            return mdDao;

        } catch (Exception e) {
            Rlog.fatal("moredetails", "Exception in getMoreDetails" + e);
            e.printStackTrace();
            return null;

        }
    }

    public MoreDetailsDao getEventMoreDetails(int modId,String modName,String defUserGroup,int finDetRight) {

        try {
            MoreDetailsDao mdDao = new MoreDetailsDao();
            mdDao.getEventMoreDetails(modId, modName, defUserGroup,finDetRight);
            return mdDao;

        } catch (Exception e) {
            Rlog.fatal("moredetails", "Exception in getEventMoreDetails" + e);
            e.printStackTrace();
            return null;

        }
    }

}
