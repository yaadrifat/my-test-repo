package com.velos.eres.service.util;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.velos.eres.business.common.CtrlDao;

/**
 * Class: com.velos.eres.service.util.VMailer Author: Sonia Sahni Date :
 * 02/27/04
 * 
 * @author vabrol
 * @created January 26, 2005
 */

public class VMailer {
    // Message Object

    private VMailMessage vMailMessage;

    private Properties propObj;

    private VelosAuthenticator velAuthenticator;

    /**
     * Default Constructor; when used will use standard settings for mails
     */

    public VMailer() {
        Rlog.debug("vmailer", "VMailer.VMailer() in default constructor");
        setDefaults();

    }

    /**
     * Constructor for the VMailer object
     * 
     * @param props
     *            Description of the Parameter
     * @param velAuth
     *            Description of the Parameter
     * @param vMessage
     *            Description of the Parameter
     */
    public VMailer(Properties props, VelosAuthenticator velAuth,
            VMailMessage vMessage) {
        // set class instance variables
        setVelAuthenticator(velAuth);
        setPropObj(props);
        setVMailMessage(vMessage);
    }

    /**
     * Sends the mail to specificed recipients
     * 
     * @return Returns the email server message returned.
     */
    public String sendMail() {
        try {
            // prepare mail session

            Session sendMailSession;
            Transport transport;

            Rlog.debug("vmailer", "VMailer.sendMail() ..getPropObj()"
                    + getPropObj());
            Rlog.debug("vmailer", "VMailer.sendMail() ..getVelAuthenticator()"
                    + getVelAuthenticator());

            sendMailSession = Session.getInstance(getPropObj(),
                    getVelAuthenticator());

            Rlog.debug("vmailer", "VMailer.setMail() ..sendMailSession"
                    + sendMailSession);

            Message newMessage = new MimeMessage(sendMailSession);

            if (vMailMessage == null) {
                return "Can not send mail, VMailer.vMailMessage is null";
            }

            newMessage
                    .setFrom(new InternetAddress(vMailMessage.getMessageFrom(),
                            vMailMessage.getMessageFromDescription()));

            if (!EJBUtil.isEmpty(vMailMessage.getMessageTo())) {
                // VA- To suppot send email to multiple addresses. Before it was
                // restricted to only one user.
                newMessage.setRecipients(Message.RecipientType.TO,
                        (new InternetAddress()).parse(vMailMessage
                                .getMessageTo()));
            }
            // newMessage.setRecipient(Message.RecipientType.TO, new
            // InternetAddress(vMailMessage.getMessageTo()));

            if (!EJBUtil.isEmpty(vMailMessage.getMessageCC())) {
                // VA- To suppot send email to multiple addresses. Before it was
                // restricted to only one user.
                // newMessage.setRecipient(Message.RecipientType.CC, new
                // InternetAddress(vMailMessage.getMessageCC()));
                newMessage.setRecipients(Message.RecipientType.CC,
                        (new InternetAddress()).parse(vMailMessage
                                .getMessageCC()));
            }

            if (!EJBUtil.isEmpty(vMailMessage.getMessageBCC())) {
                // VA- To suppot send email to multiple addresses. Before it was
                // restricted to only one user.
                // newMessage.setRecipient(Message.RecipientType.BCC, new
                // InternetAddress(vMailMessage.getMessageBCC()));
                newMessage.setRecipients(Message.RecipientType.BCC,
                        (new InternetAddress()).parse(vMailMessage
                                .getMessageBCC()));
            }

            newMessage.setSubject(vMailMessage.getMessageSubject());
            newMessage.setSentDate(vMailMessage.getMessageSentDate());
            newMessage.setText(vMailMessage.getMessageText());

            if (StringUtil.isEmpty((String)this.propObj.get("mail.smtp.port")) 
                    || "25".equals((String)this.propObj.get("mail.smtp.port"))) {
                transport = sendMailSession.getTransport("smtp");
            } else {
                transport = sendMailSession.getTransport("smtps");
            }
            transport.send(newMessage);

            return "";

        } catch (Exception ex) {
            Rlog.fatal("vmailer",
                    "VMailer.sendMail() : Exception in sending mail " + ex);
            ex.printStackTrace();
            return ex.toString();
        }

    }

    /**
     * Sets the defaults attribute of the VMailer object
     */
    public void setDefaults() {

        Rlog.debug("vmailer", "VMailer.setDefaults() ..started");

        String smtpHost = "";
        String defMailUser = "";
        String defMailPass = "";

        CtrlDao ctrl = new CtrlDao();
        int rows = 0;

        try {
            // get SMTPHost

            ctrl.getControlValues("mailsettings");
            rows = ctrl.getCRows();

            if (rows > 0) {
                smtpHost = (String) ctrl.getCValue().get(0);
            }

            // get Default Mail Server User

            ctrl = new CtrlDao();
            ctrl.getControlValues("vmailuser");
            rows = ctrl.getCRows();

            if (rows > 0) {
                defMailUser = (String) ctrl.getCValue().get(0);
            }

            // get Default Mail Server Password

            ctrl = new CtrlDao();
            ctrl.getControlValues("vmailpass");
            rows = ctrl.getCRows();

            if (rows > 0) {
                defMailPass = (String) ctrl.getCValue().get(0);
            }
            defMailUser = (defMailUser == null) ? "" : defMailUser;
            defMailPass = (defMailPass == null) ? "" : defMailPass;

            // create default properties object

            Properties props = new Properties();
            if (defMailUser.length() > 0) {
                props.put("mail.smtp.auth", "true");
            } else {
                props.put("mail.smtp.auth", "false");
            }
            props.put("mail.smtp.host", smtpHost);
            props.put("mail.debug", "false");
            ctrl = new CtrlDao();
            ctrl.getControlValues("mailport");
            rows = ctrl.getCRows();
            if (rows > 0) {
                String mailPort = StringUtil.trueValue((String) ctrl.getCValue().get(0)).trim();
                if (!StringUtil.isEmpty(mailPort)) {
                    props.put("mail.smtp.port", mailPort);
                    if (!"25".equals(mailPort)) {
                        props.put("mail.smtp.class", "com.sun.mail.smtp.SMTPSSLTransport");
                    }
                }
            }

            props.put("mail.smtp.connectiontimeout","30000");
            props.put("mail.smtp.timeout","30000");
            
            Rlog.debug("vmailer", "VMailer.setDefaults() value of smtpHost "
                    + smtpHost);

            // create default VelosAuthenticator

            VelosAuthenticator auth = new VelosAuthenticator();

            auth.setUser(defMailUser);
            auth.setPassword(defMailPass);

            // set class instance variables

            setVelAuthenticator(auth);
            setPropObj(props);

            Rlog.debug("vmailer", "VMailer.setDefaults() value of defMailUser "
                    + defMailUser);

            Rlog.debug("vmailer", "VMailer.setDefaults() ..defaults set");
 
        } catch (Exception ex) {
            Rlog.fatal("vmailer",
                    "EXCEPTION IN DEFAULT CONSTRUCTOR of VMailer " + ex);
            ex.printStackTrace();
        }

    }

    /**
     * Gets the propObj attribute of the VMailer object
     * 
     * @return The propObj value
     */
    public Properties getPropObj() {
        return this.propObj;
    }

    /**
     * Sets the propObj attribute of the VMailer object
     * 
     * @param propObj
     *            The new propObj value
     */
    public void setPropObj(Properties propObj) {
        this.propObj = propObj;
    }

    /**
     * Gets the velAuthenticator attribute of the VMailer object
     * 
     * @return The velAuthenticator value
     */
    public VelosAuthenticator getVelAuthenticator() {
        return this.velAuthenticator;
    }

    /**
     * Sets the velAuthenticator attribute of the VMailer object
     * 
     * @param velAuthenticator
     *            The new velAuthenticator value
     */
    public void setVelAuthenticator(VelosAuthenticator velAuthenticator) {
        this.velAuthenticator = velAuthenticator;
    }

    /**
     * Gets the vMailMessage attribute of the VMailer object
     * 
     * @return The vMailMessage value
     */
    public VMailMessage getVMailMessage() {
        return this.vMailMessage;
    }

    /**
     * Sets the vMailMessage attribute of the VMailer object
     * 
     * @param vMailMessage
     *            The new vMailMessage value
     */
    public void setVMailMessage(VMailMessage vMailMessage) {
        this.vMailMessage = vMailMessage;
    }

}
