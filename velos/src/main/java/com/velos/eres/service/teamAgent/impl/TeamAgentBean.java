/*


 * Classname			TeamAgentBean.class


 * 


 * Version information


 *


 * Date					02/26/2001


 * 


 * Copyright notice


 */
/*Modified by Sonia, 08/04/05 for ejb 3*/
package com.velos.eres.service.teamAgent.impl;

import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.StatusHistoryDao;
import com.velos.eres.business.common.TeamDao;
import com.velos.eres.business.team.impl.TeamBean;
import com.velos.eres.service.teamAgent.TeamAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;

/**
 * 
 * 
 * The stateless session EJB acting as a facade for the entity CMP.<br>
 * <br>
 * 
 * 
 * @author
 * 
 * 
 */

@Stateless
@Remote( { TeamAgentRObj.class })
public class TeamAgentBean implements TeamAgentRObj

{
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    
	@Resource
	private SessionContext context;

    /**
     * 
     * 
     * Looks up on the Team id parameter passed in and constructs and returns a
     * state holder.
     * 
     * 
     * containing the details of the study team<br>
     * 
     * 
     * @param id
     *            the Study team id
     * 
     * 
     */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public TeamBean getTeamDetails(int id) {

        TeamBean teamBean = null;

        try {
            teamBean = (TeamBean) em.find(TeamBean.class, new Integer(id));
            Rlog.debug("team", "Got Team" + teamBean.getId());
            return teamBean;
        } catch (Exception e) {

            Rlog.fatal("team", "EXCEPTION in getting team" + e);

            return teamBean;

        }
    }

    /**
     * 
     * 
     * Sets a Study Team.
     * 
     * 
     * @param TeamBean
     *            a state holder containing study team attributes to be set.
     * 
     * 
     * @return int
     * 
     * 
     */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int setTeamDetails(TeamBean team)

    {
        int teamId = 0;
        TeamBean teamBean = new TeamBean();

        try

        {
            Rlog.debug("team", "in try bloc of team agent");
            teamBean.updateTeam(team);
            em.persist(teamBean);
            teamId = teamBean.getId();
            Rlog.debug("team", "Created Team" + teamId);
            return teamId;
        } catch (Exception e) {

            e.printStackTrace();
            Rlog.fatal("team", "EXCEPTION IN SET TEAM DETAILS" + e);
            return 0;
        }
    }

    // calls update method
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int updateTeam(TeamBean tsk) {

        int ret = 0;

        try {

            TeamBean teamBean = (TeamBean) em.find(TeamBean.class, tsk.getId());

            if (teamBean == null) {
                return -2;
            }
            Rlog.debug("team", "SESSION : IN TRY OF UPDATETEAM() ");
            ret = teamBean.updateTeam(tsk);
            em.merge(teamBean);
            Rlog.debug("team", "TEAM UPDATED" + ret);
            return ret;
        } catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("team", "EXCEPTION IN UPDATING TEAM DETAILS" + e);
            return -2;

        }

    }

    /**
     * 
     * 
     * Calls getTeamValues() on TeamDao;
     * 
     * 
     * 
     * 
     * 
     * @param teamId
     *            Id of a Team for which the details are to be retrieved
     * 
     * 
     * @return TeamDao object with details of a specified Team
     * 
     * 
     * @see TeamDao
     * 
     * 
     */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public TeamDao getTeamValues(int studyId, int accId) {

        try

        {

            Rlog.debug("team",
                    "In getTeamValues in TeamAgentBean line number 0");

            TeamDao teamDao = new TeamDao();

            Rlog.debug("team",
                    "In getTeamValues in TeamAgentBean line number 1");

            teamDao.getTeamValues(studyId, accId);

            Rlog.debug("team",
                    "In getTeamValues in TeamAgentBean line number 2");

            return teamDao;

        } catch (Exception e) {

            Rlog.fatal("team", "Exception In getTeamValues in TeamAgentBean "
                    + e);

        }

        return null;

    }

    /**
     * 
     * 
     * Calls getTeamRights() on TeamDao;
     * 
     * 
     * 
     * 
     * 
     * @param studyId
     *            Id of a Team for which the details are to be retrieved
     * 
     * 
     * @param userId
     *            Id of a Team for which the details are to be retrieved
     * 
     * 
     * @return TeamDao object with details of a specified Team
     * 
     * 
     * @see TeamDao
     * 
     * 
     */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public TeamDao getTeamRights(int studyId, int userId) {

        try

        {

            Rlog.debug("team",
                    "In getTeamRights in TeamAgentBean line number 0");

            TeamDao teamDao = new TeamDao();

            Rlog.debug("team",
                    "In getTeamRights in TeamAgentBean line number 1");

            teamDao.getTeamRights(studyId, userId);

            Rlog.debug("team",
                    "In getTeamRights in TeamAgentBean line number 2");

            return teamDao;

        } catch (Exception e) {

            Rlog.fatal("team", "Exception In getTeamRights in TeamAgentBean "
                    + e);

        }

        return null;

    }

    // remove from team
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int deleteFromTeam(TeamBean tsk) {

        int ret = 0;

        try {

            Rlog.debug("team", "in remove from team");

            TeamBean teamBean = (TeamBean) em.find(TeamBean.class, tsk.getId());

            if (teamBean == null) {
                return -2;
            }
            em.remove(teamBean);
            Rlog.debug("team", "removed from team");
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("team", "EXCEPTION IN DELETING FROM TEAM" + e);
            return -2;
        }

    }
    
    // Overloaded for INF-18183 ::: AGodara
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int deleteFromTeam(TeamBean tsk,java.util.Hashtable<String, String> auditInfo){
        
    	try {

        	AuditBean audit=null;
        	String currdate =DateUtil.getCurrentDate()+" "+StatusHistoryDao.getCurrTime();
        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE);// Fetches the APP_Module from the Hashtable
            String rid= AuditUtils.getRidValue("ER_STUDYTEAM","eres","PK_STUDYTEAM="+tsk.getId()); //Fetches the RID
            
            //POPULATE THE AUDIT BEAN 
            audit = new AuditBean("ER_STUDYTEAM",String.valueOf(tsk.getId()),rid,userID,currdate,app_module,ipAdd,reason);
			TeamBean teamBean = (TeamBean) em.find(TeamBean.class, tsk.getId());

			if (teamBean == null) {
                return -2;
            }
            em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
            em.remove(teamBean);
            return 0;
        } catch (Exception e) {
        	context.setRollbackOnly();
            e.printStackTrace();
            Rlog.fatal("team", "EXCEPTION IN deleteFromTeam(TeamBean tsk,java.util.Hashtable<String, String> auditInfo)" + e);
            return -2;
        }
    }
    

    // /

    public TeamDao getSuperUserTeam(int studyId) {

        try

        {

            TeamDao teamDao = new TeamDao();
            teamDao.getSuperUserTeam(studyId);
            return teamDao;

        } catch (Exception e) {
            Rlog.fatal("team",
                    "Exception In getSuperUserTeam in TeamAgentBean " + e);

        }

        return null;

    }
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public TeamDao getBgtSuperUserTeam() {
        try

        {
            TeamDao teamDao = new TeamDao();
            teamDao.getBgtSuperUserTeam();
            return teamDao;

        } catch (Exception e) {
            Rlog.fatal("team",
                    "Exception In getBgtSuperUserTeam in TeamAgentBean " + e);

        }

        return null;

    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public TeamDao getTeamValuesBySite(int studyId, int accId, int orgId) {
        try {
            TeamDao teamDao = new TeamDao();
            teamDao.getTeamValuesBySite(studyId, accId, orgId);
            Rlog.debug("team", "In getTeamValuesBySite in TeamAgentBean");
            return teamDao;
        } catch (Exception e) {
            Rlog.fatal("team",
                    "Exception In getTeamValuesBySite in TeamAgentBean " + e);
        }

        return null;

    }
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public TeamDao findUserInTeam(int studyId, int userId) {
        try {
            TeamDao teamDao = new TeamDao();
            teamDao.findUserInTeam(studyId, userId);
            Rlog.debug("team", "In findUserInTeam in TeamAgentBean");
            return teamDao;
        } catch (Exception e) {
            Rlog.fatal("team", "Exception In findUserInTeam in TeamAgentBean "
                    + e);
        }

        return null;
    }
    
    //JM: 09-13-2006
    /** Add users to multiple studies    
     * 
     * @param String[]
     * 					userIds - String array of user ids
     * @param String[]
     * 					studyIds - String array of study ids
     * @param String[]
     * 					roles - String array of roles 
     * @param int
     * 				user - logged in user id
     * @param ipAdd
     * 				ipAdd - ipAdd of the logged in user 
     *      
     * @return int
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int addUsersToMultipleStudies(String[] userIds, String[] studyIds, String[] roles,int user, String ipAdd){
    
    	int ret = 0;
    	try{
    	
    	TeamDao teamDao = new TeamDao();
    	ret = teamDao.addUsersToMultipleStudies(userIds, studyIds, roles,user, ipAdd);    	
    	
    	return ret;
    	
    	} catch (Exception  e){    		
    		Rlog.fatal("team", "Exception in addUsersToMultipleStudies in TeamAgentBean" + e);
    	}
    	
    	return -2;
    }
    
    

}// end of class

