/*
 * Classname : LabAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 09/28/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Vishal Abrol		
 */

package com.velos.eres.service.labAgent;

/* Import Statements */

import java.util.Hashtable;
import javax.ejb.Remote;

import com.velos.eres.business.common.LabDao;
import com.velos.eres.business.lab.impl.LabBean;

/* End of Import Statements */

/**
 * Remote interface for LabAgent session EJB
 * 
 * @author Vishal Abrol
 * @version : 1.0 09/28/2003
 */
@Remote
public interface LabAgentRObj {
    /**
     * gets the StudyId record
     */
    LabBean getLabDetails(int id);

    /**
     * creates a new lab Record
     */
    public int setLabDetails(LabBean ssk);

    /**
     * updates the lab Record
     */
    public int updateLab(LabBean ssk);

    /**
     * remove Lab Record
     */
    public int removeLab(int id);

    public int updateClobData(int labId, String result, String notes);

    public LabDao getData(String groupId,String searchLabName);

    public LabDao getData();

    public LabDao groupPullDown(String groupId);

    public int insertLab(LabDao labdao);

    public LabDao retLabUnit(String[] testIds);

    public LabDao retLabUnit(String testId);

    public LabDao getClobData(String testId);

    public int deleteLab(String[] deleteIds, String tableName, String colName,
            String type);
    // Overloaded for INF-18183 ::: AGodara
    public int deleteLab(String[] deleteIds, String tableName, String colName,
            String type,Hashtable<String, String> auditInfo);
}
