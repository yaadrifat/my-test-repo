/*
 * Classname			StudyStatusAgentRObj.class 
 * 
 * Version information   
 *
 * Date					02/26/2001
 * 
 * Copyright notice
 */

package com.velos.eres.service.studyStatusAgent;

import java.util.HashMap;
import java.util.Hashtable;
import javax.ejb.Remote;

import com.velos.eres.business.common.StudyStatusDao;
import com.velos.eres.business.studyStatus.impl.StudyStatusBean;

/**
 * Remote interface for StudyStatus session EJB
 * 
 * @author
 */
@Remote
public interface StudyStatusAgentRObj {
    /**
     * gets the section details
     */
    StudyStatusBean getStudyStatusDetails(int id);

    /**
     * sets the section details
     */
    public int setStudyStatusDetails(StudyStatusBean ssk);

    public int updateStudyStatus(StudyStatusBean ssk);

    public StudyStatusDao getStudy(int study);

    public StudyStatusDao getStudyStatusDesc(int study, int siteId, int usr,
            int accId);

    public int checkStatus(String studyId);

    public int studyStatusDelete(int studyStatusId);
    
 // Overloaded for INF-18183 ::: Akshi
    public int studyStatusDelete(int studyStatusId,Hashtable<String, String> args);

    public int getFirstActiveEnrollPK(int studyId);

    public String getMinActiveDate(int studyId);

    public String getMinPermanentClosureDate(int studyId);

    public int getCountStudyStat(int studyId);

    public StudyStatusBean findByAtLeastOneStatus(int studyId, String codelstId);
    
    public int getCountByOrgStudyStat(int studyId,int orgId,String studyTypeList,String flag) ;
    
    public StudyStatusDao getDAOByOrgStudyStat(int studyId,int orgId,String studyTypeList,String flag) ;
    
    public void copyPreviousVerDocstoNextVersion(int studyId, String userId, String studyStatus);
    
    public void updateBlobfileObject(HashMap<String, Integer> hMap, int studyId);
    
    public void updateVersioninStudyVersionTable(int studyId, String verNum);

    public int updateStudyStatService(StudyStatusBean ssk);
}
