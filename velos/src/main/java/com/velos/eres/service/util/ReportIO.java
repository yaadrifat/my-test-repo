/*


 * Classname			ReportIO



 * Version information  1.0


 *


 * Date					05/12/2001	


 * 


 * Copyright notice		Velos Inc.


 * 


 * Author 				Sonia Sahni


 */

package com.velos.eres.service.util;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.Writer;
import java.sql.Blob;
import java.util.Calendar;


import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfDocument;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import java.net.MalformedURLException;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oracle.sql.BLOB;

import com.velos.controller.PdfController;
import com.velos.eres.business.common.StudyApndxDao;

/**
 * 
 * 
 * This class is used to save and access report output as temporary documents.
 * It was originally designed for report outputs but can be used for saving any
 * type of HTML output as a temporary file
 * 
 * @author Sonia Sahni
 * @version 1.0 05/12/2001
 * 
 * 
 */

/*
 * Modified on 4/Aug/04. This method now use fileUploadDownload.xml and files in
 * com.aithent.file.uploadDownload
 */

public final class ReportIO

{

    public static String STYLE_TAG = "";
    private BufferedWriter bWriter;
    private String fileName="";

    /**
     * Saves an HTML string as a temporary document. <br>
     * Uses settings in FileUploadDownload.xml to store and download the
     * temporary file.
     * 
     * @param reportStr
     *            HTML string
     * @param docType
     *            document type for the temporary document, possible values:
     *            <br>
     *            doc - MS Document <br>
     *            xls - MS Excel htm - HTML document
     * @param fileName
     *            First part of the temporary file name. The file name gets
     *            appended <br>
     *            with date/time information
     * @return returns download path for the temporary file created
     * @throws DocumentException 
     * @throws IOException 
     * @throws MalformedURLException 
     * 
     */
    
    public static void pdfWrite(Blob fileObj, String studyno, String status,String filename,String apndx_filename,int pkStudyApndx,String statusId) throws DocumentException, MalformedURLException, IOException
    {
    		String tmpSign=CFG.DOCUMENT_STAMP_SIGNATURE;
    		if(tmpSign.contains("<studyNumber>")){
    			tmpSign= tmpSign.replaceAll("<studyNumber>", studyno);
    		}
    		if(tmpSign.contains("<approve_status_date>")){
    			tmpSign= tmpSign.replaceAll("<approve_status_date>", status);
    		}
    		if(tmpSign.contains("<br/>")){
    			tmpSign=tmpSign.replaceAll("<br/>", "\n");
    		}
 			try
    		{  
 				InputStream is =fileObj.getBinaryStream();
 			PdfReader pdfReader =new PdfReader(is);	
 			String tempfile = apndx_filename.replace(".pdf", "(123).pdf");
			
			    //Create PdfStamper instance.
 			 PdfStamper pdfStamper = new PdfStamper(pdfReader,
 					new FileOutputStream(tempfile));
	 
			    //Create BaseFont instance.
			    BaseFont baseFont = BaseFont.createFont(
		                BaseFont.TIMES_ROMAN, 
		                BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
	 
			    //Get the number of pages in pdf.
			    int pages = pdfReader.getNumberOfPages(); 
	 
			    //Iterate the pdf through pages.
			    for(int i=1; i<=pages; i++) { 
				//Contain the pdf data.
				PdfContentByte pageContentByte = 
						pdfStamper.getOverContent(i);
				 PdfPTable footer = new PdfPTable(1);
				 footer.setTotalWidth(300);
				 
				 footer.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
				 footer.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
				 Paragraph here = new Paragraph();
				 Font Font1 = new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL);
				 
				 
				 footer.addCell(new Paragraph(tmpSign,Font1));
				
				 Document document = pageContentByte.getPdfDocument();
				pageContentByte.setFontAndSize(baseFont, 6);			
				footer.writeSelectedRows(0, -1,
				        (document.right() - document.left() - 20) / 2
				                + document.leftMargin(), document.bottom() + 10, pageContentByte);
			    }
	 
			    //Close the pdfStamper.
			    pdfStamper.close();	
			    pdfReader.close(); 				
			    InputStream inputStream = new FileInputStream(tempfile);
		        byte[] byteArray=null;		        
		        byte[] buffer = new byte[8192];
		        ByteArrayOutputStream baos = new ByteArrayOutputStream();
		        int bytesRead;
		        while ((bytesRead = inputStream.read(buffer)) != -1)
		        {
		            baos.write(buffer, 0, bytesRead);
		        }
		        byteArray= baos.toByteArray();

		        inputStream.close();
		        File file = new File(tempfile);
		        file.delete();
		        String filename_uri = filename;
            	StudyApndxDao pdf = new StudyApndxDao();
            	int	ret = pdf.updateFileObj(pkStudyApndx,byteArray,apndx_filename,filename_uri,statusId,1);
    		}
    	 catch (Exception e)
 		{
 			System.out.println("Error in Creation of PDF file" + e + e.getMessage());
 		}
		  
    	 
    }
    
    
    
    public static String pdfGenerate(String filePath,String htmlFile,String fileName, String MomHtml) throws DocumentException, MalformedURLException, IOException
    {
    	String downloadStr = null;
    	int sType=0;
    	sType = EJBUtil.getSystemType();

    		String pdfFileName="reportpdf"+"["+ System.currentTimeMillis() +  "].pdf" ;
 			try
    		{  			
 					PdfWriter writer;
 					Document document = new Document(PageSize.A3, 36, 36, 36, 36);
 					 if (sType == 0) {
 					 writer = PdfWriter.getInstance(document, new FileOutputStream(filePath+ "\\" +pdfFileName));
 					 }
 					 else{
 						 writer = PdfWriter.getInstance(document, new FileOutputStream(filePath+ "//" +pdfFileName));
 					 }
 					HTMLWorker fff=new HTMLWorker(document);
 					document.open();
 					if(sType == 0){
 						
 						fff.parse(new StringReader(MomHtml));
 						new FileInputStream(filePath+ "\\" +fileName);
 					}
 					else{
 						fff.parse(new StringReader(MomHtml));
 			 			new FileInputStream(filePath+ "//" +fileName);
 						
 						
 					}
 					document.close();
    		}
    	 catch (Exception e)
 		{
 			System.out.println("Error in Creation of PDF file" + e + e.getMessage());
 		}
    	 downloadStr = com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET + "?file=" + pdfFileName;

		return downloadStr;
    }
    
    public static String saveReportToDoc(String reportStr, String docType,
            String fileName)

    {

        String header = null;
        String footer = null;
        String contents = null;
        String filExt = "txt";
        String filePath = "null";
        String completeFileName = null;
        String downloadStr = null;
        String fileUrl="";
        int sType;

        Calendar now = Calendar.getInstance();

        com.aithent.file.uploadDownload.Configuration.readSettings("eres");

        com.aithent.file.uploadDownload.Configuration
                .readUploadDownloadParam(
                        com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD
                                + "fileUploadDownload.xml", null);

        filePath = com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER;

        if (docType.compareToIgnoreCase("doc") == 0) {
            header = "<html><head><title>Velos eResearch</title><meta http-equiv='Content-Type' content='application/ms-word; charset=UTF-8'>"
                    + STYLE_TAG + "</head><body>";
            filExt = ".doc";
        }

        if (docType.compareToIgnoreCase("xls") == 0) {
            header = "<html><head><title>Velos eResearch</title><meta http-equiv='Content-Type' content='application/vnd.ms-excel; charset=UTF-8'>"
                    + STYLE_TAG + "</head><body>";
            filExt = ".xls";
        }

        if (docType.compareToIgnoreCase("htm") == 0) {
            header = "<html><head><title>Velos eResearch</title><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'></meta>"
                    + STYLE_TAG + "</head><body>";
            filExt = ".html";
        }

        footer = "</body></html>";
        
        if (docType.compareToIgnoreCase("xml") == 0) {
            header = "<html><head><title>Velos eResearch</title><meta http-equiv='Content-Type' content='text/html'>"
                    + STYLE_TAG + "</head><body>";
            filExt = ".xml";
            footer = "";
            header = "";
        }
        

        Rlog.debug("common", "###header" + header);
        contents = header + reportStr + footer;

        Rlog.debug("common", "###contents" + contents);

        //byte[] fileBytes = contents.getBytes();

        try {

            completeFileName = fileName + "[" +System.currentTimeMillis()+ "]" + filExt;

            sType = EJBUtil.getSystemType();

           // FileOutputStream file;
            Writer       writer  = null;
            if (sType == 0) {

            	OutputStream outputStream = new FileOutputStream(filePath + "\\" + completeFileName);
            	writer       = new OutputStreamWriter(outputStream,"UTF8");
            	
                
            } else {
            	OutputStream outputStream = new FileOutputStream(filePath + "//" + completeFileName);
            	writer       = new OutputStreamWriter(outputStream,"UTF8");
            	
            }
            
            writer.write(contents, 0, contents.length());

            writer.close();

        } catch (Exception e) {
            Rlog.fatal("common", "Exception in saving report " + e);
            return null;
        }

        //downloadStr = com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET
         //       + "/" + completeFileName;
        downloadStr = com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET
               + "?file=" + completeFileName;

        return downloadStr;

    }
  
    //KLUDGE : This method can be generalized a bit and lot of information can be at class level.This needs to be done if time permits, ever
   public BufferedWriter initializeWriter(String docType, String fileName)
    {
	    String header="",filExt="";
	   Calendar now = Calendar.getInstance();

       com.aithent.file.uploadDownload.Configuration.readSettings("eres");

       com.aithent.file.uploadDownload.Configuration
               .readUploadDownloadParam(
                       com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD
                               + "fileUploadDownload.xml", null);

       String filePath = com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER;

       if (docType.compareToIgnoreCase("doc") == 0) {
           header = "<html><head><title>Velos eResearch</title><meta http-equiv='Content-Type' content='application/ms-word; charset=UTF-8'>"
                   + STYLE_TAG + "</head><body>";
           filExt = ".doc";
       }

       if (docType.compareToIgnoreCase("xls") == 0) {
           header = "<html><head><title>Velos eResearch</title><meta http-equiv='Content-Type' content='application/vnd.ms-excel; charset=UTF-8'>"
                   + STYLE_TAG + "</head><body>";
           filExt = ".xls";
       }

       if (docType.compareToIgnoreCase("htm") == 0) {
           header = "<html><head><title>Velos eResearch</title><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>"
                   + STYLE_TAG + "</head><body>";
           filExt = ".html";
       }
       String completeFileName = fileName + "[" + System.currentTimeMillis()+"]" + filExt;
       this.fileName=completeFileName;
       try {
      int sType = EJBUtil.getSystemType();
      if (sType == 0) {

           bWriter = new BufferedWriter(new FileWriter(filePath + "\\" + completeFileName, true));
       } else {
    	   bWriter = new BufferedWriter(new FileWriter(filePath + "//" + completeFileName, true));
       }
      	bWriter.write(header);
      	}catch(Exception e )
       {
    	   e.printStackTrace();
       }
	 return bWriter;  
    }
   
   public void write(String data)
   {
	  try {
	   bWriter.write(data);
	    } catch(Exception ie)
	    {
	    	ie.printStackTrace();
	    }
   }
   
   public String getFileUrl()
   {
	   return com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET
       + "?file=" + this.fileName;
   }
   
   
   
   //KLUDGE : Hard Coding for footer. Footer can be member of Class and allowed to be customized
   public void deactivateWriter()
   {
	   try{
	   String footer = "</body></html>";
	   bWriter.write(footer);
	   bWriter.close();
	   } catch (IOException ie) 
	   {
		   ie.printStackTrace();
	   }
   }
 }