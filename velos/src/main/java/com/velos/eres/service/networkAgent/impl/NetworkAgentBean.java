package com.velos.eres.service.networkAgent.impl;

import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.network.impl.NetworkBean;
import com.velos.eres.service.networkAgent.NetworkAgentRObj;
import com.velos.eres.service.util.Rlog;

@Stateless
@Remote( { NetworkAgentRObj.class })
public class NetworkAgentBean implements NetworkAgentRObj {
	@PersistenceContext(unitName = "eres")
    protected EntityManager em;
	@Resource
	private SessionContext context;
	
	public int setNetworkDetails(NetworkBean nwb) {
        try {
            NetworkBean nwkBean = new NetworkBean();
            nwkBean.updateNetwork(nwb);
            em.persist(nwkBean);
            return (nwkBean.getNetworkId());
        } catch (Exception e) {
            Rlog.fatal("network", "Error in setNetworkDetails() in NetworkAgentBean "
                    + e);
        }
        return 0;
    }
	
	public int updateNetwork(NetworkBean nwb) {

        int output;
        NetworkBean networkBean = null;

        try {
            /*
             * UserPK pkuser; pkuser = new UserPK(usk.getUserId()); UserHome
             * userHome = EJBUtil.getUserHome(); retrieved =
             * userHome.findByPrimaryKey(pkuser);
             */
        	networkBean = (NetworkBean) em.find(NetworkBean.class, nwb.getNetworkId());
            if (networkBean == null) {
                return -2;
            }
             networkBean.updateNetwork(nwb);
             //networkBean.setNetworkTypeId(nwb.getNetworkTypeId());
             //networkBean.setModifiedBy(nwb.getModifiedBy());
             em.merge(networkBean);
             output = networkBean.getNetworkId();

        } catch (Exception e) {
            Rlog.debug("user", "Error in updateUser in UserAgentBean" + e);
            return -2;
        }
        return output;
    }
	
	public NetworkBean getNetworkDetails(int networkId) {

        NetworkBean networkBean = null;

        try {
            Rlog
                    .debug("network", "in getNetworkDetails agentbean networkId## "
                            + networkId);

            networkBean = (NetworkBean) em.find(NetworkBean.class, new Integer(networkId));
            return networkBean;
        } catch (Exception e) {
            Rlog.fatal("network", "Error in getNetworkDetails() in NetworkAgentBean"
                    + e);

        }
        return networkBean;

    }

}
