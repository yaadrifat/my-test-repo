package com.velos.eres.service.userSessionAgent;

import javax.ejb.Remote;

@Remote
public interface UserSessionTokenAgentRObj {
	public String createTokenBeforeLogin(String ipAdd);
	public String createTokenForUser(Integer fkUser);
	public String challengeTokenBeforeLogin(String token, String ipAdd);
	public String challengeTokenForUser(String token, Integer fkUser);
	public Integer invalidateAllTokensForUser(Integer fkUser);
}
