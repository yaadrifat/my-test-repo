/*
 *  Classname			CodelstserAgentBean.class
 *
 *  Version information
 *
 *  Date					03/19/2001
 *
 *  Copyright notice
 */

package com.velos.eres.service.codelstAgent.impl;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.eres.business.codelst.impl.CodelstBean;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.service.codelstAgent.CodelstAgentRObj;
import com.velos.eres.service.util.Rlog;

/**
 * The stateless session EJB acting as a facade for the entity CMP.<br>
 * <br>
 * 
 * 
 * @author dinesh
 * @created May 10, 2005
 */

@Stateless
public class CodelstAgentBean implements CodelstAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * Looks up on the Codelst id parameter passed in and constructs and returns
     * a Codelst state holder. containing all the summary details of the u<br>
     * 
     * 
     * @param codelstId
     *            the Codelst id
     * @return The codelstDetails value
     */
    public CodelstBean getCodelstDetails(int codelstId) {

        CodelstBean retrieved = null;

        try {

            retrieved = (CodelstBean) em.find(CodelstBean.class, new Integer(
                    codelstId));
        } catch (Exception e) {
            System.out.print("Error in getCodelstDetails() in CodelstAgentBean"
                    + e);
        }

        return retrieved;
    }

    /**
     * Sets a Codelst.
     * 
     * @param codelst
     *            The new codelstDetails value
     * @return void
     */

    //Modified by Manimaran on 20,May06 for Role based Study Rights.
    public int setCodelstDetails(CodelstBean codelst) {
    	
    	try {
    		int rowfound = 0;
            CodelstBean cd = new CodelstBean();
            CodeDao codeDao = new CodeDao();
            int ret = codeDao.validateAccSection(codelst.getClstDesc(), codelst
                    .getClstAccId());

            if (ret > 0) {
                return -1;
            }
            
            try {
                Query query = em.createNamedQuery("findByUniqueCodelstSubtype");
                query.setParameter("codelst_subtyp", codelst.getClstSubType());
                ArrayList list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    // subtype is not unique
                    Rlog
                            .debug("codelst",
                                    "setCodelstAgentBean.setCodelstDetails : the subtype is not unique");
                    rowfound = 1;
                    return -3;
                }

            } catch (Exception ex) {
                Rlog.fatal("codelst",
                        "In setCodelstAgentBean Details Session FIND UNIQUE SUBTYPE NAME EXCEPTION"
                                + ex);
                rowfound = 0; // unique name does not exist
            }
            
            

            cd.updateCodelst(codelst);
            em.persist(cd);
            return (cd.getClstId());
        } catch (Exception e) {
            System.out
                    .print("Error in setCodelstDetails() in CodelstAgentBean "
                            + e);
            return -2;
        }
        
    }

    /**
     * Description of the Method
     * 
     * @param usk
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    
    //  Modified by Manimaran on 20,May06 for Role based Study Rights.
    public int updateCodelst(CodelstBean usk) {

        CodelstBean retrieved = null;
        int output;

        try {

            retrieved = (CodelstBean) em.find(CodelstBean.class, new Integer(
                    usk.getClstId()));
                                    
            if (!((retrieved.getClstSubType()).toUpperCase()).equals((usk.getClstSubType()).toUpperCase())) {
            
            	 try {
                     Query query = em.createNamedQuery("findByUniqueCodelstSubtype");
                     query.setParameter("codelst_subtyp", usk.getClstSubType());
                     ArrayList list = (ArrayList) query.getResultList();
                     if (list == null)
                         list = new ArrayList();
                     if (list.size() > 0) {
                         // subtype is not unique
                         Rlog
                                 .debug("codelst",
                                         "setCodelstAgentBean.setCodelstDetails : the subtype is not unique");
                       
                         return -3;
                     }

                 } catch (Exception ex) {
                     Rlog.fatal("codelst",
                             "In setCodelstAgentBean Details Session FIND UNIQUE SUBTYPE NAME EXCEPTION"
                                     + ex);
                      // unique name does not exist
                 }

           

        } // end check if rolesubtype  is changed
            
           
            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateCodelst(usk);
            em.merge(retrieved);
        } catch (Exception e) {
            Rlog.debug("codelst", "Error in updateCodelst in CodelstAgentBean"
                    + e);
            return -2;
        }
        return output;
    }

   

    /**
     * Calls getCodelstData(int accId, String codelstType) on CodeDao;
     * 
     * @param accId
     *            AccountId
     * @param codelstType
     *            Codelist Type
     * @return The codelstData value
     * @see CodeDao
     */

    public CodeDao getCodelstData(int accId, String codelstType) {
        CodeDao codeDao = new CodeDao();
        try {
            Rlog.debug("codelst", "In getCodelstData line number 1");
            codeDao.getCodelstData(accId, codelstType);
            Rlog.debug("codelst", "In getCodelstData line number 2");
        } catch (Exception e) {
            Rlog.fatal("codelst",
                    "Exception In getCodelstData in CodelstAgentBean " + e);
        }
        return codeDao;
    }

    /**
     * Calls getCodeDescription(int codeId) on CodeDao;
     * 
     * @param codeId
     * @return The codeDescription value
     * @see CodeDao
     */

    public String getCodeDescription(int codeId) {
        CodeDao codeDao = new CodeDao();
        String codeDesc = "";
        try {
            Rlog.debug("codelst", "In getCodeDescription line number 1");
            codeDesc = codeDao.getCodeDescription(codeId);
            Rlog.debug("codelst", "In getCodeDescription line number 2");
        } catch (Exception e) {
            Rlog.fatal("codelst",
                    "Exception In getCodeDescription in CodelstAgentBean " + e);
        }
        return codeDesc;
    }

    /**
     * Calls getCodeDescription(int codeId) on CodeDao;
     * 
     * @param codeId
     * @return The codeDescription value
     * @see CodeDao
     */

    public String getCodeCustomCol(int codeId) {
        CodeDao codeDao = new CodeDao();
        String codeCustomCol = "";
        try {
            Rlog.debug("codelst", "In getCodeDescription line number 1");
            codeCustomCol = codeDao.getCodeCustomCol(codeId);
            Rlog.debug("codelst", "In getCodeDescription line number 2");
        } catch (Exception e) {
            Rlog.fatal("codelst",
                    "Exception In getCodeDescription in CodelstAgentBean " + e);
        }
        return codeCustomCol;
    }
    
    
    /**
     * Calls getRolesForEvent(int eventId) on CodeDao;
     * 
     * @param eventId
     *            AccountId
     * @return The rolesForEvent value
     * @see CodeDao
     */

    public CodeDao getRolesForEvent(int eventId) {
        CodeDao codeDao = new CodeDao();
        try {
            Rlog.debug("codelst", "In getRolesForEvent line number 1");
            codeDao.getRolesForEvent(eventId);
            Rlog.debug("codelst", "In getRolesForEvent line number 2");
        } catch (Exception e) {
            Rlog.fatal("codelst",
                    "Exception In getRolesForEvent in CodelstAgentBean " + e);
        }
        return codeDao;
    }

    /**
     * Calls getDescForEventUsers(ArrayList eventUserIds, ArrayList userTypes)
     * on CodeDao;
     * 
     * @param eventUserIds
     *            User Ids or codelst Ids
     * @param userTypes
     *            User Types U for User, R for role and S for mail users
     * @return The descForEventUsers value
     * @see CodeDao
     */

    public CodeDao getDescForEventUsers(ArrayList eventUserIds,
            ArrayList userTypes) {
        CodeDao codeDao = new CodeDao();
        try {
            Rlog.debug("codelst", "In getDescForEventUsers line number 1");
            codeDao.getDescForEventUsers(eventUserIds, userTypes);
            Rlog.debug("codelst", "In getDescForEventUsers line number 2");
        } catch (Exception e) {
            Rlog.fatal("codelst",
                    "Exception In getDescForEventUsers in CodelstAgentBean "
                            + e);
        }
        return codeDao;
    }

    /**
     * Gets the CodeDao Object
     * 
     * @return empty CodeDao Object
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public CodeDao getCodeDaoInstance() {
        return new CodeDao();
    }
    
    /**
     * Gets the codeId attribute of the CodeDao object
     * 
     * @param type
     *            codelist type
     * @param subType
     *            codelst subtype
     * @return The codeId value
     */
    public int getCodeId(String type, String subType)
    {
    	CodeDao codeDao = new CodeDao();
    	int id = 0;
        try {
            Rlog.debug("codelst", "In getCodeId(String type, String subType) line number 1");
            id = codeDao.getCodeId(type, subType);
            
        } catch (Exception e) {
            Rlog.fatal("codelst",
                    "Exception In getCodeId(String type, String subType) in CodelstAgentBean "
                            + e);
        }
        return id;

    	
    }

}
