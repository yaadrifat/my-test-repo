/*
 * Classname			LinkedFormAgentBean
 * 
 * Version information : 
 *
 * Date					08/08/2003
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.linkedFormsAgent.impl;

/* IMPORT STATEMENTS */

import java.util.Hashtable;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.LinkedFormsDao;
import com.velos.eres.business.common.SaveFormDao;
import com.velos.eres.business.linkedForms.impl.LinkedFormsBean;
import com.velos.eres.service.linkedFormsAgent.LinkedFormsAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP.
 * 
 * @author Anu Khanna
 * @version 1.0, 08/08/2003
 * @ejbHome LinkedFormAgentHome
 * @ejbRemote LinkedFormAgentRObj
 */

@Stateless
public class LinkedFormsAgentBean implements LinkedFormsAgentRObj {

    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    
    @Resource
    private SessionContext context;

    /**
     * Looks up on the linkedform id parameter passed in and constructs and
     * returns a LinkedFormStateKeeper containing all the summary details of the
     * form
     * 
     * @param linkedFormId
     * @see LinkedFormStateKeeper
     */
    public LinkedFormsBean getLinkedFormDetails(int linkedFormId) {
        LinkedFormsBean retrieved = null;
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {

            retrieved = (LinkedFormsBean) em.find(LinkedFormsBean.class,
                    new Integer(linkedFormId));

            linkedFormsDao.getOrgNamesIds(linkedFormId, Integer
                    .parseInt(retrieved.getFormLibId()));

            linkedFormsDao.getGrpNamesIds(linkedFormId, Integer
                    .parseInt(retrieved.getFormLibId()));

            Rlog.debug("lnkform", "LinkedFormAgent. linkedFormsDao getGrpNames"
                    + linkedFormsDao.getGrpNames());
            Rlog.debug("lnkform", "LinkedFormAgent.linkedFormsDao getGrpIds"
                    + linkedFormsDao.getGrpIds());

            retrieved.setLinkedFormsDao(linkedFormsDao);
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Error in getLinkedFormDetails() in LinkedFormAgentBean"
                            + e);
        }

        return retrieved;
    }

    /**
     * Creates LinkedForm record.
     * 
     * @param a
     *            State Keeper containing the LinkedForm attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */

    public int setLinkedFormDetails(LinkedFormsBean linkedFormsk) {
        try {
            em.merge(linkedFormsk);
            return linkedFormsk.getLinkedFormId();
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Error in setLinkedFormDetails() in LinkedFormAgentBean "
                            + e);
            return -2;
        }

    }

    /**
     * Updates a LinkedForm record.
     * 
     * @param a
     *            LinkedForm state keeper containing LinkedForm attributes to be
     *            set.
     * @return int for successful:0; e\Exception : -2
     */
    public int updateLinkedForm(LinkedFormsBean linkedFormsk) {

        LinkedFormsBean retrieved = null; // Entity Bean Remote Object
        int output1, output;
        String recordType = null;
        String formLibId = null;

        try {
            recordType = linkedFormsk.getRecordType();
            formLibId = linkedFormsk.getFormLibId();

            retrieved = (LinkedFormsBean) em.find(LinkedFormsBean.class,
                    new Integer(linkedFormsk.getLinkedFormId()));

            if (retrieved == null) {
                Rlog.debug("lnkform",
                        "LinkedFormAgentBean.updateLinkedForm retrieved"
                                + retrieved);
                return -2;
            }

            if (!(recordType.equals("D"))) {
                // do not update organizations and groups if form is deleted
                output1 = updateOrgIds(linkedFormsk.getLinkedFormId(),
                        linkedFormsk.getFormLibId(), linkedFormsk
                                .getLinkedFormsDao().getOrgIds(), 
                                linkedFormsk.getLinkedFormsDao().getOldOrgIds(), 
                                linkedFormsk.getLinkedFormsDao().getUserId(),
                                linkedFormsk.getLinkedFormsDao().getIpAdd());
                output1 = updateGrpIds(linkedFormsk.getLinkedFormId(),
                        linkedFormsk.getFormLibId(), linkedFormsk
                                .getLinkedFormsDao().getGrpIds(), 
                                linkedFormsk.getLinkedFormsDao().getOldGrpIds(), 
                                linkedFormsk.getLinkedFormsDao().getUserId(),
                                linkedFormsk.getLinkedFormsDao().getIpAdd());
            }

            output = retrieved.updateLinkedForm(linkedFormsk);

            // delete the versions for this form
            if (recordType.equals("D")) {
                LinkedFormsDao lnkFormsDao = new LinkedFormsDao();
                lnkFormsDao.deleteFormVersions(formLibId);
            }

        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Error in updateLinkedForm in LinkedFormAgentBean" + e);
            return -2;
        }
        return output;
    } 

    /**
     * Method to get all the Forms in the Form Library and the Forms already
     * linked with a particular study
     * 
     * @return LinkedFormsDao
     */
//  "siteIdAcc" Parameter Added by Gopu to fix the bugzilla Issue #2406
    public LinkedFormsDao getFormListForLinkToStudy(int accountId, int userId,
            String formName, int formType, int studyId, String siteIdAcc ) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getFormListForLinkToStudy(accountId, userId,
                    formName, formType, studyId, siteIdAcc );
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getFormListForLinkToStudy in LinkedFormsAgentBean "
                            + e);
            return null;
        }
    }

    /**
     * Method to get all the Forms in a particular study
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getStudyLinkedForms(int studyId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getStudyLinkedForms(studyId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getStudyLinkedForms in LinkedFormsAgentBean "
                            + e);
            return null;
        }
    }

    /**
     * Method to get only the Active Forms linked to a particular study
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getStudyLinkedActiveForms(int studyId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getStudyLinkedActiveForms(studyId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getStudyLinkedActiveForms in LinkedFormsAgentBean "
                            + e);
            return null;
        }
    }

    /**
     * Method to get only the Active Forms for all Patient studies and all
     * studies
     * 
     * @return LinkedFormsDao
     */
    public LinkedFormsDao getStudyLinkedActiveForms(int studyId, int accountId,
            int userId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao
                    .getStudyLinkedActiveForms(studyId, accountId, userId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog
                    .fatal(
                            "lnkform",
                            "Exception in getStudyLinkedActiveForms(studyId,accountId,userId) in LinkedFormsAgentBean "
                                    + e);
            return null;
        }
    }

    /**
     * Method to Link forms to a Study/Account multiple forms are linked to
     * study/account through a stored procedure All the form ids and respective
     * data is set in LinkedFormsDao attributes
     * 
     * @param linkedFormsDao
     * @returns -1 in case of error, 0 for success
     */
    public int linkFormsToStudyAccount(LinkedFormsDao linkedFormsDao) {
        int ret = 0;
        LinkedFormsDao lnkFormsDao = new LinkedFormsDao();
        Rlog.debug("lnkform",
                "In linkFormsToStudyAccount in LinkedFormsAgentBean");
        try {
            ret = lnkFormsDao.linkFormsToStudyAccount(linkedFormsDao);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in linkFormsToStudyAccount in LinkedFormsAgentBean "
                            + e);
            return -1;
        }
    }

    // ///////////////////////////////////////////////////////////
    /**
     * Method to get the list of patient specific forms and respective number of
     * times they have been filled
     * 
     * @return LinkedFormsDao
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public LinkedFormsDao getPatientForms(int accountId, int patient,
            int userId, int siteId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getPatientForms(accountId, patient, userId, siteId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog
                    .fatal("lnkform",
                            "Exception in getPatientForms in LinkedFormsAgentBean "
                                    + e);
            return null;
        }
    }

    /**
     * Method to get the list of study specific forms for patient and respective
     * number of times they have been filled
     * 
     * @return LinkedFormsDao
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public LinkedFormsDao getPatientStudyForms(int accountId, int patId,
            int studyId, int userId, int siteId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getPatientStudyForms(accountId, patId, studyId,
                    userId, siteId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getPatientStudyForms in LinkedFormsAgentBean "
                            + e);
            return null;
        }
    }

    public LinkedFormsDao getPatientStudyForms(int accountId, int patId,
            int studyId, int userId, int siteId, boolean includeHidden) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getPatientStudyForms(accountId, patId, studyId,
                    userId, siteId, includeHidden);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getPatientStudyForms in LinkedFormsAgentBean "
                            + e);
            return null;
        }
    }
    
    /**
     * 
     * Method to get the list of account specific forms and respective number of
     * times they have been filled
     * 
     * @return LinkedFormsDao
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public LinkedFormsDao getAccountForms(int accountId, int userId, int siteId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getAccountForms(accountId, userId, siteId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog
                    .fatal("lnkform",
                            "Exception in getAccountForms in LinkedFormsAgentBean "
                                    + e);
            return null;
        }
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public LinkedFormsDao getAccountGenericForms(int accountId, int userId, int siteId, String displayType) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getAccountGenericForms(accountId, userId, siteId, displayType);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog
                    .fatal("lnkform",
                            "Exception in getAccountForms in LinkedFormsAgentBean "
                                    + e);
            return null;
        }
    }
    public LinkedFormsDao getOrganisationForms(int accountId, int userId, int siteId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getOrganisationForms(accountId, userId, siteId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog
                    .fatal("lnkform",
                            "Exception in getOrganisationForms in LinkedFormsAgentBean "
                                    + e);
            return null;
        }
    }
 
    public LinkedFormsDao getNetworkForms(int accountId, int userId, int siteId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getNetworkForms(accountId, userId, siteId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog
                    .fatal("lnkform",
                            "Exception in getNetworkForms in LinkedFormsAgentBean "
                                    + e);
            return null;
        }
    }
    public LinkedFormsDao getSNetworkForms(int accountId, int userId, int siteId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getSNetworkForms(accountId, userId, siteId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog
                    .fatal("lnkform",
                            "Exception in getNetworkForms in LinkedFormsAgentBean "
                                    + e);
            return null;
        }
    }
    public LinkedFormsDao getUserForms(int accountId, int userId, int siteId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getUserForms(accountId, userId, siteId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog
                    .fatal("lnkform",
                            "Exception in getUserForms in LinkedFormsAgentBean "
                                    + e);
            return null;
        }
    }
    /**
     * Method to get the list of Filled study specific forms and respective
     * number of times they have been filled
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getStudyForms(int accountId, int studyId, int userId,
            int siteId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getStudyForms(accountId, studyId, userId, siteId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getStudyForms in LinkedFormsAgentBean " + e);
            return null;
        }
    }

    /**
     * Overloaded method Method to get the list of Filled study specific forms
     * and respective number of times they have been filled according to the
     * rights
     * 
     * @return LinkedFormsDao
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public LinkedFormsDao getStudyForms(int accountId, int studyId, int userId,
            int siteId, int grpRight, int stdRight, 
            boolean isIrb, String submissionType, String formSubtype) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getStudyForms(accountId, studyId, userId, siteId,
                    grpRight, stdRight, isIrb, submissionType, formSubtype);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getStudyForms in LinkedFormsAgentBean " + e);
            return null;
        }
    }

    /**
     * Method to get the list of forms linked with a particular account also
     * handles the searching of the forms in an account
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getFormsLinkedToAccount(int accountId,
            String formName, int studyId, String linkedTo, int siteId,
            int groupId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getFormsLinkedToAccount(accountId, formName,
                    studyId, linkedTo, siteId, groupId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getFormsLinkedToAccount in LinkedFormsAgentBean "
                            + e);
            return null;
        }
    }

    /**
     * Method to get the list of Active forms linked with a particular account
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getActiveFormsLinkedToAccount(int accountId,
            String linkedTo) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getActiveFormsLinkedToAccount(accountId, linkedTo);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getActiveFormsLinkedToAccount in LinkedFormsAgentBean "
                            + e);
            return null;
        }
    }

    // ////

    /**
     * 
     * Method to copy single/multiple forms linked to a study
     * 
     * @return pk of the new form
     */

    public int copyFormsForStudy(String[] idArray, String formType,
            String studyId, String formName, String creator, String ipAdd) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
        int pkNewForm = 0;
        try {
            pkNewForm = linkedFormsDao.copyFormsForStudy(idArray, formType,
                    studyId, formName, creator, ipAdd);
            return pkNewForm;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in copyFormsForStudy in LinkedFormsAgentBean "
                            + e);
            return -1;
        }
    }

    /**
     * Method to copy single/multiple forms linked to a account
     * 
     * @return pk of the new form
     */

    public int copyFormsForAccount(String[] idArray, String formType,
            String formName, String creator, String ipAdd) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
        int pkNewForm = 0;
        try {
            pkNewForm = linkedFormsDao.copyFormsForAccount(idArray, formType,
                    formName, creator, ipAdd);
            return pkNewForm;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in copyFormsForAccount in LinkedFormsAgentBean "
                            + e);
            return -1;
        }
    }

    /**
     * Method to get the organization corresponding to the forms
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getOrgNamesIds(int pkLf, int fkFormlib) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
        Rlog.debug("lnkform", "In getOrgNamesIds in LinkedFormsAgentBean ");
        try {
            linkedFormsDao.getOrgNamesIds(pkLf, fkFormlib);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getOrgNamesIds in LinkedFormsAgentBean " + e);
            return null;
        }
    }

    /**
     * Method to update the organization corresponding to the forms
     * 
     * @return -1 error
     */

    public int updateOrgIds(int pkLf, String fkForm, String orgIds, String oldOrgIds, String userId, String ipAdd) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
        int ret;

        try {
        	ret = linkedFormsDao.updateOrgIds(pkLf, fkForm, orgIds, oldOrgIds, userId, ipAdd);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in updateOrgIds in LinkedFormsAgentBean " + e);
            return -1;
        }
    }

    /**
     * Method to get the groups corresponding to the forms
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getGrpNamesIds(int pkLf, int fkFormlib) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
        Rlog.debug("lnkform", "In getGrpNamesIds in LinkedFormsAgentBean ");
        try {
            linkedFormsDao.getGrpNamesIds(pkLf, fkFormlib);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getGrpNamesIds in LinkedFormsAgentBean " + e);
            return null;
        }
    }

    /**
     * Method to update the groups corresponding to the forms
     * 
     * @return -1 for error
     */

    public int updateGrpIds(int pkLf, String fkForm, String grpIds, String oldGrpIds, String userId, String ipAdd) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
        int ret;

        try {
            ret = linkedFormsDao.updateGrpIds(pkLf, fkForm, grpIds, oldGrpIds, userId, ipAdd);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in updateGrpIds in LinkedFormsAgentBean " + e);
            return -1;
        }
    }

    /*
     * This method gets the Form HTML for preview @param formId Id of the form
     * for which html is required @returns html string
     */

    public String getFormHtmlforPreview(int formId) {
        SaveFormDao saveFormDao = new SaveFormDao();
        String htmlstr = null;

        try {
            htmlstr = saveFormDao.getFormHtmlforPreview(formId);
            return htmlstr;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getFormHtmlforPreview in LinkedFormsAgentBean "
                            + e);
            return null;
        }
    }

    public String getFormHtmlforPreviewNoRole(int formId) {
        SaveFormDao saveFormDao = new SaveFormDao();
        String htmlstr = null;

        try {
            htmlstr = saveFormDao.getFormHtmlforPreviewNoRole(formId);
            return htmlstr;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getFormHtmlforPreview in LinkedFormsAgentBean "
                            + e);
            return null;
        }
    }
    
    /*
     * This method gets the Form HTML for preview 
     * 
     * @param formId -Id of the form
     * @param htParam - Hastable of more parameters for filters
     * <br> Supported Filters
     * <br> teamRolePK String - Only for study/studypatient  based forms where we can identify logged in users study team role
     * for which html is required @returns html string
     */
    

    public String getFormHtmlforPreview(int formId,Hashtable moreParams) {
        SaveFormDao saveFormDao = new SaveFormDao();
        String htmlstr = null;

        try {
            
        	saveFormDao.setHtMoreParams(moreParams);
        	htmlstr = saveFormDao.getFormHtmlforPreview(formId);
            return htmlstr;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getFormHtmlforPreview(int formId,Hashtable moreParams) in LinkedFormsAgentBean "
                            + e);
            return null;
      
        }
    } 

    /*
     * This method gets the Form HTML for filled form @param formId Id of the
     * form for which html is required @returns html string
     */
    public String getFilledFormHtml(int pkfilledform, String formdisplaytype)

    {
        SaveFormDao saveFormDao = new SaveFormDao();
        String htmlstr = null;

        try {
            htmlstr = saveFormDao.getFilledFormHtml(pkfilledform,
                    formdisplaytype);
            return htmlstr;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getFilledFormHtml in LinkedFormsAgentBean "
                            + e);
            return null;
        }
    }
    /*
     * This method gets the Form HTML for filled form 
     * @param formId Id of the
     * form for which html is required @returns html string
     * @param formdisplaytype Display type flag for the form
     * @param flag - apparently not in use
     * @param htParam - Hastable of more parameters for filters
     * <br> Supported Filters
     * <br> teamRolePK String - Only for study/studypatient  based forms where we can identify logged in users study team role
     * for which html is required @returns html string
     */
    public SaveFormDao getFilledFormHtml(int pkfilledform, String formdisplaytype,String flag,Hashtable moreParams)

    {
        SaveFormDao saveFormDao = new SaveFormDao();
        

        try {
        	
        	saveFormDao.setHtMoreParams(moreParams);
            saveFormDao.getFilledFormHtml(pkfilledform,
                    formdisplaytype);
            return saveFormDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getFilledFormHtml in LinkedFormsAgentBean ()"
                            + e);
            e.printStackTrace();
            return null;
        }
    }

    /*
     * This method gets the Form HTML for filled form @param formId Id of the
     * form for which html is required @returns html string
     */
    public SaveFormDao getFilledFormHtml(int pkfilledform, String formdisplaytype,String flag)

    {
        SaveFormDao saveFormDao = new SaveFormDao();
        

        try {
            saveFormDao.getFilledFormHtml(pkfilledform,
                    formdisplaytype);
            return saveFormDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getFilledFormHtml in LinkedFormsAgentBean ()"
                            + e);
            e.printStackTrace();
            return null;
        }
    }

    public SaveFormDao getFilledFormHtmlNoRole(int pkfilledform, String formdisplaytype,String flag)

    {
        SaveFormDao saveFormDao = new SaveFormDao();
        

        try {
            saveFormDao.getFilledFormHtmlNoRole(pkfilledform,
                    formdisplaytype);
            return saveFormDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getFilledFormHtml in LinkedFormsAgentBean ()"
                            + e);
            e.printStackTrace();
            return null;
        }
    }
    /*
     * This method gets the Printer Friendly Form HTML for filled forms @param
     * formId Id of the form for which html is required @param filledFormId Id
     * of the form for which html is required @param linkFrom @returns html
     * string
     */
    public String getPrintFormHtml(int formId, int filledFormId, String linkFrom) {
        SaveFormDao saveFormDao = new SaveFormDao();
        String htmlstr = null;

        try {
            htmlstr = saveFormDao.getPrintFormHtml(formId, filledFormId,
                    linkFrom);
            return htmlstr;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getPrintFormHtml in LinkedFormsAgentBean "
                            + e);
            return null;
        }
    }

    /**
     * Method to insert values for forms linked to CRF
     * 
     * @return 0 for success, -1 for error
     */
    public int insertCrfForms(String[] formIds, String accId,
            String[] crfNames, String[] crfNumbers, String calId,
            String eventId, String user, String ipAdd) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
        int pkLnkForm = 0;
        try {
            pkLnkForm = linkedFormsDao.insertCrfForms(formIds, accId, crfNames,
                    crfNumbers, calId, eventId, user, ipAdd);
            return pkLnkForm;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in insertCrfForms in LinkedFormsAgentBean " + e);
            return -1;
        }
    }

    /**
     * @return LinkedFormsStateKeeper object with LinkedForms details.
     */

    public LinkedFormsBean findByCrfEventId(int crfId, int eventId) {
        LinkedFormsBean toreturn = null;
        Rlog.debug("lnkform", "inside LInkedFormsAgentBean findByCrfEventId()");

        try {
            Query query = em.createNamedQuery("findByCrfEventId");
            query.setParameter("fk_crf", crfId);
            query.setParameter("fk_event", eventId);
            toreturn = (LinkedFormsBean) query.getSingleResult();

        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in findByCrfEventId in LinkedFormsAgentBean "
                            + e);
        }

        return toreturn;
    }

    /**
     * Find Linked Form details with Form Id
     * 
     * @param formId
     * @return LinkedFormsStateKeeper object with LinkedForms details.
     */

    public LinkedFormsBean findByFormId(int formId) {

        Rlog.debug("lnkform", "inside LinkedFormsAgentBean findByFormId()");

        LinkedFormsBean toreturn = null;

        try {

            Query query = em.createNamedQuery("findByFormId");
            query.setParameter("fk_formlib", formId);
            toreturn = (LinkedFormsBean) query.getSingleResult();

        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in findByFormId in LinkedFormsAgentBean " + e);
        }

        return toreturn;
    }

    /*
     * Method to update the hide and sequence fields of Linked Forms
     */
    public int updateHideSeqOfLF(String[] pkLF, String[] hide, String[] seq,
            String[] patProfile, String ipAdd, int user, String[] dispInSpec) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
        int ret;

        try {
            ret = linkedFormsDao.updateHideSeqOfLF(pkLF, hide, seq, patProfile,
                    ipAdd, user, dispInSpec);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in updateHideSeqOfLF in LinkedFormsAgentBean "
                            + e);
            return -1;
        }
    }

    /*
     * Method to delete filled Form response
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int deleteFilledFormResp(String filledFormId,
            String formDispLocation, String lastModifiedBy, String ipAdd) {
        SaveFormDao saveFormDao = new SaveFormDao();
        int ret;

        try {
            ret = saveFormDao.deleteFilledFormResp(filledFormId,
                    formDispLocation, lastModifiedBy, ipAdd);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in deleteFilledFormResp in LinkedFormsAgentBean "
                            + e);
            return -1;
        }
    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int deleteFilledFormResp(String filledFormId,
            String formDispLocation,Hashtable<String, String> auditInfo ) {
        SaveFormDao saveFormDao = new SaveFormDao();
        int ret;
        String tablename = null;
        String condition = null;
        String module = null;
        AuditBean audit=null;
        try {
        	
            if (formDispLocation.equals("S") || formDispLocation.equals("SA")) {

				condition = "pk_studyforms =";
				tablename = "ER_STUDYFORMS";
				module = LC.Std_Study;
			} else if (formDispLocation.equals("SP")
					|| formDispLocation.equals("PA")) {

				condition = "pk_patforms =";
				tablename = "ER_PATFORMS";
				module = LC.L_Manage_Pats;
			} else if (formDispLocation.equals("A")) {

				condition = "pk_acctforms =";
				tablename = "ER_ACCTFORMS";
				module = LC.L_Manage_Acc;
			} else if (formDispLocation.equals("C")) {

				condition = "pk_crfforms =";
				tablename = "ER_CRFFORMS";
				module = LC.L_Manage_Pats;
			}else if (formDispLocation.equals("ORG")) {

				condition = "pk_acctforms =";
				tablename = "ER_ACCTFORMS";
				module = LC.L_Organizations;
			}else if (formDispLocation.equals("USR")) {

				condition = "pk_acctforms =";
				tablename = "ER_ACCTFORMS";
				module = LC.L_User;
			}
			else if (formDispLocation.equals("NTW")) {

				condition = "pk_acctforms =";
				tablename = "ER_ACCTFORMS";
				module = LC.L_Netwrk_Level;
			}
			else if (formDispLocation.equals("SNW")) {

				condition = "pk_acctforms =";
				tablename = "ER_ACCTFORMS";
				module = LC.L_SNetwrk_Level;
			}
            
            String lastModifiedBy = auditInfo.get(AuditUtils.USER_ID_KEY);
			String ipAdd = auditInfo.get(AuditUtils.IP_ADD_KEY);			
			 
        	String currdate =DateUtil.getCurrentDate();
        	String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String rid= AuditUtils.getRidValue(tablename,"eres",condition+filledFormId); //Fetches the RID
            audit = new AuditBean(tablename,String.valueOf(filledFormId),rid,lastModifiedBy,currdate,module,ipAdd,reason); //POPULATE THE AUDIT BEAN 
        	em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
            ret = saveFormDao.deleteFilledFormResp(filledFormId,
                    formDispLocation, lastModifiedBy, ipAdd);
			if(ret!=0){
				context.setRollbackOnly();
	          
			}
			
            return ret;
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("lnkform",
                    "Exception in deleteFilledFormResp in LinkedFormsAgentBean "
                            + e);
            return -1;
        }
    }


    //added by sonia Abrol, to check if user has right a form
    
    /** checks if user has access to the form (evenif the form is hidden)
     * 
     * @param accountId
     *            The form Id
     * @param userId
     *            The user for which access rights are checked
     * 
     */

    public boolean hasFormAccess(int formId, int userId )
    {
         
        try {
             return LinkedFormsDao.hasFormAccess(formId, userId);
            
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in hasFormAccess in LinkedFormsAgentBean " + e);
            return false;
        }

	}
    
   // Added by Gopu for May - June Requirement (#F1) 
    /** get Study Active lockdonw forms linked to patients of this study
     * 
     * @param accountId
     *             
     * @param studyId
     * 
     */

    
    public LinkedFormsDao getStudyActiveLockdownForms(int studyId, int accountId){
    	LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
        try {
            linkedFormsDao.getStudyActiveLockdownForms(studyId,accountId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getStudyActiveLockdownForms in LinkedFormsAgentBean "
                            + e);
            return null;
        }
    }
    
    /**
     * 
     * Method to get the list of account specific forms and respective number of
     * times they have been filled. get sonly those forms that have display in specimen checked
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getAccountFormsForSpecimen(int accountId, int userId, int siteId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getAccountFormsForSpecimen(accountId, userId, siteId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog
                    .fatal("lnkform",
                            "Exception in getAccountFormsForSpecimen in LinkedFormsAgentBean "
                                    + e);
            e.printStackTrace();
            return null;
        } 
    }

    /**
     * Overloaded method Method to get the list of Filled study specific forms
     * and respective number of times they have been filled according to the
     * rights. looks for forms marked as 'display in specimen management'
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getStudyFormsForSpecimen(int accountId, int studyId, int userId,
            int siteId, int grpRight, int stdRight) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getStudyFormsForSpecimen(accountId, studyId, userId, siteId,
                    grpRight, stdRight);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getStudyFormsForSpecimen in LinkedFormsAgentBean " + e);
            return null;
        }
    }

    /**
     * Method to get the list of patient specific forms and respective number of
     * times they have been filled
     * looks for forms marked as 'display in specimen management'
     * 
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getPatientFormsForSpecimen(int accountId, int patient,
            int userId, int siteId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getPatientFormsForSpecimen(accountId, patient, userId, siteId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog
                    .fatal("lnkform",
                            "Exception in getPatientFormsForSpecimen in LinkedFormsAgentBean "
                                    + e);
            return null;
        }
    }

    /**
     * Method to get the list of study specific forms for patient and respective
     * number of times they have been filled
     *  looks for forms marked as 'display in specimen management'
     *  
     * @return LinkedFormsDao
     */

    public LinkedFormsDao getPatientStudyFormsForSpecimen(int accountId, int patId,
            int studyId, int userId, int siteId) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getPatientStudyFormsForSpecimen(accountId, patId, studyId,
                    userId, siteId);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getPatientStudyFormsForSpecimen in LinkedFormsAgentBean "
                            + e);
            return null;
        }
    }
    
    public LinkedFormsDao getLinkedFormByFormId(int accountId, int formId, String type) {
    	try {
    		LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
    		linkedFormsDao.getLinkedFormByFormId(accountId, formId, type);
    		return linkedFormsDao;
    	} catch(Exception e) {
            Rlog.fatal("lnkform", 
            		"Exception in getLinkedFormByFormId in LinkedFormsAgentBean "+e);
    	}
    	return null;
    }
    
    public LinkedFormsDao getFormResponseFkPer(int filledFormId) {
    	try {
    		LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
    		linkedFormsDao.getFormResponseFkPer(filledFormId);
    		return linkedFormsDao;
    	} catch(Exception e) {
            Rlog.fatal("lnkform", 
            		"Exception in getFormResponseFkPer in LinkedFormsAgentBean "+e);
    	}
    	return null;
    }
    
    public LinkedFormsDao getFormResponseCreator(int filledFormId, int formId) {
    	try {
    		LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
    		linkedFormsDao.getFormResponseCreator(filledFormId, formId);
    		return linkedFormsDao;
    	} catch(Exception e) {
            Rlog.fatal("lnkform", 
            		"Exception in getFormResponseCreator in LinkedFormsAgentBean "+e);
    	}
    	return null;
    }
    
    public int getUserAccess(int formId, int userId, int creator) {
    	try {
    		LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
    		return linkedFormsDao.getUserAccess(formId, userId, creator);
    	} catch(Exception e) {
            Rlog.fatal("lnkform", 
            		"Exception in getUserAccess in LinkedFormsAgentBean "+e);
    	}
    	return 0;
    }

    /** get latest response for a study form 
     *  @author Sonia Abrol
     *  @return Hashtable of data with response attributes
     *  	possible keys:
     *  
     *  	formlibver - form version for the response
     *     pkstudyforms - response PK
     *     
     *  07/02/09 */
       
    
   	public Hashtable getLatestStudyFormResponeData(String formId, String studyId) 
   	{
   		Hashtable htReturn = new Hashtable();
       try {
    	   htReturn = LinkedFormsDao.getLatestStudyFormResponeData(formId, studyId);
           return htReturn ;
           
       } catch (Exception e) {
           Rlog.fatal("lnkform",
                   "Exception in getLatestStudyFormResponeData in LinkedFormsAgentBean "
                           + e);
           return null;
       }

   		
   	}
   	
   	
   	/**
     * Overloaded method Gets the list of Filled study specific forms and
     * respective number of times they have been filled - Study Level/Specific
     * Study Level For displaying the Study forms for data entry, the primary
     * organization of the logged in user is matched with the organizations
     * selected in the filter and all the groups that the user belongs to are
     * matched with the selected filter groups. The hidden forms are not
     * displayed and forms are displayed as per the sequence entered by user The
     * study forms are displayed before All Study Forms Retrieves Active and
     * Lockdown forms. The method also gets the response data and sets it within the LinkedFormsDao
     *
     * @param accountId
     *            The accountId for which the Forms are to be fetched
     * @param studyId
     *            The studyId for which the Forms are to be fetched
     * @param userId
     *            The user for which the Forms are to be fetched
     * @param siteId
     *            The primary organization of loggedin user
     * @param grpRight
     *            The group right for All Study Forms access
     * @param stdRight
     *            The study team right for Study Forms access
     * @param isIrb
     *            true=only get IRB forms; false=get all forms
     * @param submissionType
     *            Submission type for IRB application (study) 
     * @param formSubtype
     *            form subtype for IRB application (study) 
     */

    public LinkedFormsDao getStudyFormsWithResponseData(int accountId, int studyId, int userId,
            int siteId, int grpRight, int stdRight, 
            boolean isIrb, String submissionType, String formSubtype)
    {
    	
    	LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getStudyFormsWithResponseData(accountId, studyId, userId,
                    siteId, grpRight, stdRight, 
                    isIrb,  submissionType, formSubtype);
            
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getStudyFormsWithResponseData in LinkedFormsAgentBean "
                            + e);
            return null;
        }

    }
    
    
    /**
     * Method to get the list of study specific forms for patient and respective
     * number of times they have been filled
     * 
     * @return LinkedFormsDao
     */
    /*//Added For Bug# 9255 :Date 14 May 2012 :By YPS*/
    public LinkedFormsDao getPatStdFormsForPatRestrAndSingleEntry(int accountId, int patId,
            int studyId, int userId, int siteId,Hashtable<String, String> htParams) {
        LinkedFormsDao linkedFormsDao = new LinkedFormsDao();

        try {
            linkedFormsDao.getPatStdFormsForPatRestrAndSingleEntry(accountId, patId, studyId,
                    userId, siteId,htParams);
            return linkedFormsDao;
        } catch (Exception e) {
            Rlog.fatal("lnkform",
                    "Exception in getPatStdFormsForPatRestrAndSingleEntry in LinkedFormsAgentBean "
                            + e);
            return null;
        }
    }

    public LinkedFormsDao getFormResponseAuditInfo(int formId, int filledFormId) {
    	try {
    		LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
    		linkedFormsDao.getFormResponseAuditInfo(formId, filledFormId);
    		return linkedFormsDao;
    	} catch(Exception e) {
            Rlog.fatal("lnkform", 
            		"Exception in getFormResponseAuditInfo in LinkedFormsAgentBean "+e);
    	}
    	return null;
    }
}// end of class

