/*	
 * Classname			GrpRightsRObj
 * 
 * Version information   1.0
 *
 * Date					03/02/2001
 * 
 * Copyright notice 	Velos Inc.
 */

package com.velos.eres.service.grpRightsAgent;

import javax.ejb.Remote;

import com.velos.eres.business.grpRights.impl.GrpRightsBean;
import com.velos.eres.service.grpRightsAgent.impl.GrpRightsAgentBean;

/**
 * Remote interface for grpRightsAgentRObj session EJB
 * 
 * @author Deepali
 * @verison 1.0 03/02/2001
 * @see GrpRightsAgentBean
 */
@Remote
public interface GrpRightsAgentRObj {

    /**
     * gets the Rights of a Group
     */
    public GrpRightsBean getGrpRightsDetails(int id);

    /**
     * sets the Rights of a Group
     */
    public int setGrpRightsDetails(GrpRightsBean grp);

    /**
     * calls updateGrpRights() on entity bean
     */

    public int updateGrpRights(GrpRightsBean gsk);
}
