package com.velos.eres.service.submissionAgent;

import javax.ejb.Remote;

import com.velos.eres.business.submission.impl.SubmissionBoardBean;

@Remote
public interface SubmissionBoardAgent {
    public Integer createSubmissionBoard(SubmissionBoardBean submissionBoardBean);
    public SubmissionBoardBean findByPkSubmissionBoard(Integer pkSubmissionBoard);
    public SubmissionBoardBean findByFkSubmissionAndBoard(Integer pkSubmission, Integer fkReviewBoard);
    public Integer getExistingSubmissionBoard(SubmissionBoardBean submissionBoardBean);
    public Integer updateSubmissionBoard(SubmissionBoardBean submissionBoardBean, boolean applyNull);
    
    /** Returns Minutes of Meeting for a review board and meeting date*/
    public  String getreviewMeetingMOM(int fkBoard, int fkMeetingDatePK);
    
    /** Updates Minutes of Meeting for a review board and meeting date*/
    public int updateReviewMeetingMOM( int fkMeetingDatePK,String mom) ;
    
   
    /** returns the PK of the default review board for an account ONLY if the logged in uder's default group (passed as a parameter)
     * has access to the review board
     * 
     * returns 0 if no default group is configured or the group does not have access to it
     * 
     * */
    
    public int getDefaultReviewBoardWithCheckGroupAccess(int accountId,int grpId) ;
    
    public int getReviewBoardIfExistsForStudy(int submissionId , int reviewBoard); 
     
    public int getDefaultReviewBoardWithCheckGroupAccess(int accountId,int grpId,int submissionBoardPK) ;

}
