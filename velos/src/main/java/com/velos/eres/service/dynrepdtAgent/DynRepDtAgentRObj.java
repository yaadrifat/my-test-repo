/*
 * Classname : DynRepAgentDtRObj
 * 
 * Version information: 1.0
 *
 * Date: 09/28/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Vishal Abrol		
 */

package com.velos.eres.service.dynrepdtAgent;

/* Import Statements */

import java.util.ArrayList;

import javax.ejb.Remote;

import com.velos.eres.business.dynrepdt.impl.DynRepDtBean;

/* End of Import Statements */

/**
 * Remote interface for dynrepdtAgent session EJB
 * 
 * @author Vishal Abrol
 * @version : 1.0 09/28/2003
 */
/*
 * Modified by Sonia Abrol. 01/26/05, added a new attribute formType to method
 * signatures - updateAllCols,setAllCols. This attribute is added to distinguish
 * between application forms and core table lookup forms
 */
@Remote
public interface DynRepDtAgentRObj {
    public DynRepDtBean getDynRepDtDetails(int id);

    public int setDynRepDtDetails(DynRepDtBean dsk);

    public int updateDynRepDt(DynRepDtBean dsk);

    public int removeDynRepDt(int id);

    public int setAllCols(String repId, ArrayList repCols, ArrayList colSeqs,
            ArrayList colWidths, ArrayList colFormats, ArrayList colDisps,
            String usr, String ipadd, ArrayList colNames,
            ArrayList colDataTypes, String formId, String formType,
            ArrayList useUniqueIds, ArrayList useDataValues ,ArrayList calcSums, ArrayList calcPers,ArrayList repeatNumbers);

    public int updateAllCols(ArrayList dyncolIds, String repId,
            ArrayList repCols, ArrayList colSeqs, ArrayList colWidths,
            ArrayList colFormats, ArrayList colDisps, String usr, String ipadd,
            ArrayList colNames, ArrayList colDataTypes, String formId,
            String formType, ArrayList useUniqueIds, ArrayList useDataValues ,ArrayList calcSums, ArrayList calcPers,ArrayList repeatNumbers);

    public int removeAllCols(ArrayList Ids);
}
