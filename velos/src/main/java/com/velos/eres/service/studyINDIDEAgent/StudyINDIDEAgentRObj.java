/**
 * Remote interface for StudyINDIDEAgentBean session EJB
 * Class name : StudyIndIdeAgentRObj<br>
 * @version 1.0 11/21/2011
 * Copyright notice: Velos, Inc
 * Author: Ashwani Godara
 */

package com.velos.eres.service.studyINDIDEAgent;


/* Import Statements */

import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.business.common.StudyINDIDEDAO;
import com.velos.eres.business.studyINDIDE.impl.StudyINDIDEBean;


/* End of Import Statements */

@Remote
public interface StudyINDIDEAgentRObj {
	
	
	
	public  int setStudyINDIDEInfo(int studyId);
	public StudyINDIDEDAO getStudyINDIDEInfo(int studyId);
	
	public int deleteIndIdeInfo(int indIdeId,Hashtable<String, String> auditInfo);
	public int updateIndIdeInfo(StudyINDIDEBean indIdeBean);
	public int setIndIdeInfo(StudyINDIDEBean indIdeBean);
	


}
