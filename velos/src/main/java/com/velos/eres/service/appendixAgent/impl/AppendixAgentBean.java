/*
 * Classname			AppendixAgentBean.class
 * 
 * Version information
 *
 * Date					03/03/2001
 * 
 * Copyright notice Aithent
 */
package com.velos.eres.service.appendixAgent.impl;
import java.util.Hashtable;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.appendix.impl.AppendixBean;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.AppendixDao;
import com.velos.eres.service.appendixAgent.AppendixAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
/**
 * The stateless session EJB acting as a facade for the entity CMP.<br>
 * <br>
 * 
 * @author
 */
@Stateless
public class AppendixAgentBean implements AppendixAgentRObj
{
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    
    @Resource
    private SessionContext context;
    /**
     * 
     */
    private static final long serialVersionUID = 4050482322359071027L;
    public AppendixBean getAppendixDetails(int id) {
        try {
            return (AppendixBean) em.find(AppendixBean.class, new Integer(id));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    /**
     * Sets a Study.
     * 
     * @param study
     *            a state holder containing study attributes to be set.
     * @return String
     */
    public int setAppendixDetails(AppendixBean ask) {
        int AppendixID = 0;
        try {
            AppendixBean ap = new AppendixBean();
            ap.updateAppendix(ask);
            em.persist(ap);
            // added by sonia; need the pk for updating file in db
            AppendixID = ap.getAppendixId();
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
        return AppendixID;
    }
    public int removeAppendix(int id) {
        int output;
        AppendixBean app_ro = null; // Study Appendix Entity Bean Remote Object
        try {
            app_ro = (AppendixBean) em
                    .find(AppendixBean.class, new Integer(id));
            em.remove(app_ro);
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int removeAppendix(int id,Hashtable<String, String> auditInfo) {
     
        AppendixBean app_ro = null; // Study Appendix Entity Bean Remote Object
        AuditBean audit=null;
        try {
        	String currdate =DateUtil.getCurrentDate();
        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE);// Fetches the APP_Module from the Hashtable
            String rid= AuditUtils.getRidValue("ER_STUDYAPNDX","eres","PK_STUDYAPNDX="+id); //Fetches the RID
            
            audit = new AuditBean("ER_STUDYAPNDX",String.valueOf(id),rid,userID,currdate,app_module,ipAdd,reason);
        	app_ro = (AppendixBean) em.find(AppendixBean.class, new Integer(id));
			
        	em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
			em.remove(app_ro);
            return 0;
            
        } catch (Exception e) {
        	context.setRollbackOnly();
            e.printStackTrace();
            return -1;
        }
    }
    
    public int updateAppendix(AppendixBean appsk) {
        AppendixBean retrieved = null; // User Entity Bean Remote Object
        int output;
        try {
            retrieved = (AppendixBean) em.find(AppendixBean.class, new Integer(
                    appsk.getAppendixId()));
            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateAppendix(appsk);
            em.merge(retrieved);
        } catch (Exception e) {
            Rlog.debug("appendix",
                    "Error in updateAppendix in AppendixAgentBean" + e);
            return -2;
        }
        return output;
    }
    public AppendixDao getByStudyVerId(int studyVerId) {
        try {
            Rlog.debug("appendix",
                    "In getByStudyVerid in AppendixAgentBean line number 0");
            AppendixDao appendixDao = new AppendixDao();
            Rlog.debug("appendix",
                    "In getByStudyVerId in AppendixAgentBean line number 1");
            appendixDao.getAppendixValues(studyVerId);
            Rlog.debug("appendix",
                    "In getByStudyVerId in AppendixAgentBean line number 2");
            return appendixDao;
        } catch (Exception e) {
            Rlog.fatal("appendix",
                    "Exception In getByStudyVerId in AppendixAgentBean " + e);
            return null;
        }
    } // end of getByStudyVerId
    public AppendixDao getAppendixValuesPublic(int studyVerId) {
        try {
            Rlog
                    .debug("appendix",
                            "In getAppendixValuesPublic in AppendixAgentBean line number 0");
            AppendixDao appendixDao = new AppendixDao();
            Rlog.debug("appendix",
            "In getAppendixValuesPublic in AppendixAgentBean line number 1");
            appendixDao.getAppendixValuesPublic(studyVerId);
            Rlog
                    .debug("appendix",
                            "In getAppendixValuesPublic in AppendixAgentBean line number 2");
            return appendixDao;
        } catch (Exception e) {
            Rlog.fatal("appendix",
                    "Exception In getAppendixValuesPublic in AppendixAgentBean "
                            + e);
            return null;
        }
    } // end of getAppendixValuesPublic
    public AppendixDao getByStudyIdAndType(int studyVerId, String type) {
        try {
            Rlog
                    .debug("appendix",
                            "In getByStudyidAndType in AppendixAgentBean line number 0");
            AppendixDao appendixDao = new AppendixDao();
            Rlog
                    .debug("appendix",
                            "In getByStudyidAndType in AppendixAgentBean line number 1");
            appendixDao.getAppendixValues(studyVerId, type);
            Rlog
                    .debug("appendix",
                            "In getByStudyIdAndType in AppendixAgentBean line number 2");
            return appendixDao;
        } catch (Exception e) {
            Rlog.fatal("appendix",
                    "Exception In getByStudyIdAndType in AppendixAgentBean "
                            + e);
            return null;
        }
    } // end of getByStudyIdAndType
    /** gets latest document for version category */
    public AppendixDao getLatestDocumentForCategory(String studyId,String versionCategory)
    {
        try {
             AppendixDao appendixDao = new AppendixDao();
             appendixDao.getLatestDocumentForCategory(studyId, versionCategory);
             return appendixDao;
        } catch (Exception e) {
            Rlog.fatal("appendix",
                    "Exception In getLatestDocumentForCategory in AppendixAgentBean "
                            + e);
            return null;
        }
    } 
    public AppendixDao getLatestDocumentForCategory(String studyId,String versionCategory, String submissionPK)
    {
        try {
             AppendixDao appendixDao = new AppendixDao();
             appendixDao.getLatestDocumentForCategory(studyId, versionCategory,submissionPK);
             return appendixDao;
        } catch (Exception e) {
            Rlog.fatal("appendix",
                    "Exception In getLatestDocumentForCategory in AppendixAgentBean "
                            + e);
            return null;
        }
    } 
    
    public AppendixDao getLatestDocumentForSubmission(String studyId,String versionCategory, String submissionPK,String approvalDate)
    {
        try {
             AppendixDao appendixDao = new AppendixDao();
             appendixDao.getLatestDocumentForSubmission(studyId, versionCategory,submissionPK,approvalDate);
             return appendixDao;
        } catch (Exception e) {
            Rlog.fatal("appendix",
                    "Exception In getLatestDocumentForSubmission in AppendixAgentBean "
                            + e);
            return null;
        }
    } 
    
}// end of class
