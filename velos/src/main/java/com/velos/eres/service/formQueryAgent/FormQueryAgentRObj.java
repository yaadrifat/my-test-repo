/**
 * Classname : FormQueryAgentRObj
 *
 * Version information: 1.0
 *
 * Date: 01/03/2005
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.service.formQueryAgent;

/* Import Statements */

import java.util.ArrayList;

import javax.ejb.Remote;

import com.velos.eres.business.common.FormQueryDao;
import com.velos.eres.business.formQuery.impl.FormQueryBean;

/* End of Import Statements */

/**
 * Remote interface for FormQueryAgent session EJB
 *
 * @author Anu Khanna
 */

@Remote
public interface FormQueryAgentRObj {
    /**
     * gets the FormQuery record
     */
    FormQueryBean getFormQueryDetails(int formQueryId);

    /**
     * creates a new FormQuery Record
     */
    public int setFormQueryDetails(FormQueryBean fqk);

    /**
     * updates the FormQuery Record
     */
    public int updateFormQuery(FormQueryBean fqk);

    /**
     * remove FormQuery Record
     */
    public int removeFormQuery(int formQueryId);

    //KM-#4016
    public FormQueryDao insertIntoFormQuery(int filledFormId, int formId, int calledFrom,
            int queryType, String[] queryTypeIds, String[] fldSysIds,
            String[] notes, String[] status, String[] comments,
            String[] modes, String[] pkQueryStats, int creator,
            String ipAdd);

    public FormQueryDao getQueriesForForm(int formId, int queryModuleId, int moduleLinkedTo);

    public FormQueryDao getQueryHistoryForField(int formQueryId);
    public FormQueryDao getCurrentQueryHistoryForField(String fieldId,int responseId);

    public int insertQueryResponse(int filledformId, int calledFrom, int fldId,
            int queryType, int queryTypeId, int queryStatusId, String comments,
            String enteredOn, int enteredBy, int creator, String ipAdd);

    public FormQueryDao getSystemFormQueryStat(int filledFormId,
            String fldSysIds);
    public FormQueryDao getSystemFormQueryStat(int filledFormId,
            ArrayList fldSysIds, ArrayList fldReasons);

    public FormQueryDao getSystemQueryNotes(int formQueryId);
    public FormQueryDao getOtherQueryHistoryForField(int  fieldId,int responseId);
}
