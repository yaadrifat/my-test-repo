/** Class provides the various functions on Date like converting string to date


 and java.util.Date to java.sql.Date etc.,


 Autor : Dharani Babu on 07/27/2000.


 */

package com.velos.eres.service.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

/*
 * Modified by Sonia Abrol, 05/02/05 added methods to get Month and Year drop
 * downs
 */
public class DateUtil {

    /**
     *
     *
     * Converts the string in the given format to the java.util.Date.
     *
     *
     * (e.g)stringToDate("05/23/1978","MM/dd/yyyy") gives the Date
    *  @param format This is the default format.
     * It is only used when there is no format setting in the eresearch.xml
     *
     *
     *
     *
     */
	private  static String monthString[] = new String[] {"","January","February","March","April","May","June","July","August","September","October","November","December"};

	private  static String monthStringShort[] = new String[] {"","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec","13"};


    public static java.util.Date stringToDate(String dtString, String format) {

        java.util.Date dt = null;
        String formatForDate = "";

        if (format == null || format.equals(""))
            format = "MM/dd/yyyy";

        formatForDate  = Configuration.getAppDateFormat();

        if (StringUtil.isEmpty(formatForDate))
        {

        	formatForDate = format;
        }

        //Rlog.fatal("date", "{ORIG DATE STR:}" + dtString);
        //Rlog.fatal("date", "format to be used" + formatForDate + " , format received: " + format);

        if (dtString == null || dtString.equals(""))
            return null; // here exception should be thrown

        SimpleDateFormat sdf = new SimpleDateFormat(formatForDate);

        try {
            dt = sdf.parse(dtString);
            //Rlog.fatal("date", "{NEW DATE STR:}" + dt);
        } catch (Exception e) {
            Rlog.fatal("date", "{EXCEPTION IN CONVERTING TO DATE:}" + e);
        }
        return dt;
    }


    /**
     * @param dtString Date in string format
     *  @param format This is the default format.
     * It is only used when there is no format setting in the eresearch.xml
     *
     * */
    public static Timestamp stringToTimeStamp(String dtString, String format) {

    	String formatForDate  = "";
    	 java.util.Date dt = null;
         java.sql.Timestamp timestamp = null;


    	formatForDate    = Configuration.getAppDateFormat();



        format = (format == null) ? "" : format;

        if (StringUtil.isEmpty(format))
        {
            format = "MM/dd/yyyy HH:mm:ss";
        }

        if (!StringUtil.isEmpty(formatForDate  ))
        {

        	format = format.replace("MM/dd/yyyy", formatForDate);

        }




        if (dtString == null || dtString.equals(""))
            return null; // here exception should be thrown

        SimpleDateFormat sdf = new SimpleDateFormat(format);

        try {
            dt = sdf.parse(dtString);
            timestamp = new java.sql.Timestamp(dt.getTime());

        } catch (Exception e) {
            Rlog.fatal("date", "{EXCEPTION IN CONVERTING TO DATE:}" + e);
        }
        return timestamp;
    }

    /*
     *modified by sonia, for supporting multiple date formats
     *
     */

    public static String dateToString(java.util.Date dt) {
        try {
            // Debug.println("{Date VALUE:}" + dt);

            String sReturn = "";
            String appDF = "";

            if (dt == null) {
                return sReturn;

            } else

            {

                // changed by Sajal to fix the unparseable date error

            	appDF = Configuration.getAppDateFormat();

            	if (StringUtil.isEmpty(appDF))
            		appDF = "MM/dd/yyyy";

            	SimpleDateFormat df = new SimpleDateFormat(appDF);

                sReturn = df.format(dt);
                // Debug.println("{String VALUE:}" + sReturn);
                Rlog.debug("date", "{String VALUE:}" + sReturn);
                return sReturn;
            }
        } catch (Exception e) {
            // Debug.println("{EXCEPTION IN CONVERTING DATE TO STRING:}" + e);
            Rlog.fatal("date", "{EXCEPTION IN CONVERTING DATE TO STRING:}" + e);
            return null;
        }
    }

    /**  @param format This is the default format.
    * It is only used when there is no format setting in the eresearch.xml */

    public static String dateToString(java.util.Date dt, String format) {
        try {
            String sReturn = "";

            String formatForDate  = "";

        	formatForDate    = Configuration.getAppDateFormat();

        	 format = (format == null) ? "" : format;

             if (StringUtil.isEmpty(format))
             {
                 format = "MM/dd/yyyy HH:mm:ss";
             }

             if (!StringUtil.isEmpty(formatForDate  ))
             {

            	format = format.replace("MM/dd/yyyy", formatForDate);

             }


            if (dt == null) {
                return sReturn;
            } else

            {
                SimpleDateFormat df = new SimpleDateFormat(format);
                sReturn = df.format(dt);
                Rlog.debug("date", "{String VALUE:}" + sReturn);
                return sReturn;
            }
        } catch (Exception e) {
            Rlog.fatal("date", "{EXCEPTION IN CONVERTING DATE TO STRING:}" + e);
            return null;
        }
    }

    public static java.sql.Date dateToSqlDate(java.util.Date dt) {
        java.sql.Date sqlDt = null;
        try {
            sqlDt = new java.sql.Date(dt.getTime());
        } catch (Exception e) {
            // Debug.println("dateToSQlDate:ex:"+e.getMessage());
            Rlog.debug("date", "dateToSQlDate:ex:" + e.getMessage());
        }
        return sqlDt;
    }

    public static java.sql.Timestamp dateToSqlTimestamp(java.util.Date dt) {
        java.sql.Timestamp sqlDt = null;
        try {
            sqlDt = new java.sql.Timestamp(dt.getTime());
        } catch (Exception e) {
            // Debug.println("dateToSQlTimestamp:ex:"+e.getMessage());
            Rlog.debug("date", "dateToSQlTimestamp:ex:" + e.getMessage());
        }
        return sqlDt;
    }

    public static boolean compareCalendar(Calendar cal1, Calendar cal2,
            String operator) {

        int month1 = 0;
        int month2 = 0;
        int year1 = 0;
        int year2 = 0;
        int day1 = 0;
        int day2 = 0;

        month1 = cal1.get(Calendar.MONTH);
        year1 = cal1.get(Calendar.YEAR);
        day1 = cal1.get(Calendar.DATE);

        month2 = cal2.get(Calendar.MONTH);
        year2 = cal2.get(Calendar.YEAR);
        day2 = cal2.get(Calendar.DATE);

        // case to check if date 1 is greater than date 2
        if (operator.equals(">")) {
            // CHECK FOR YR
            if (year1 < year2) {
                return false;
            } else if (year1 > year2) {
                return true;
            }

            if (month1 < month2) {
                return false;
            } else if (month1 > month2) {
                return true;
            }

            if (day1 <= day2) {
                return false;
            }

        }

        return true; // this means date 1 is greater than date 2
    }

    // end of class

    /**
     * takes the oracle's default date format and converts to MM/DD/YYYY with time part
     *
     * @param dbDate
     *            date in oracle's default format
     * @return String of date converted to mm/dd/yyyy
     */
    public static String format2DateFormat(String dbDate) {
    	return format2DateFormat(dbDate,"Y");
    }

    public static String format2DateFormat(String dbDate,String wTime) {
        String strDate = dbDate;

        String dtYear = "";
        String dtMonth = "";
        String dtDay = "";
        String strDateFormatted = "";

        int tokenCount = 0;


        if (StringUtil.isEmpty(strDate)) {
            return "";
        }
        if (strDate.length() < 10) {
            return "";
        }


        StringTokenizer std = new StringTokenizer(strDate.substring(0, 10), "-");
        tokenCount = std.countTokens();



        if (tokenCount == 3) {
            dtYear = std.nextToken();
            dtMonth = std.nextToken();
            dtDay = std.nextToken();

            strDateFormatted = DateUtil.getFormattedDateString(dtYear,dtDay,dtMonth);

            if (wTime.equals("Y")) {
            if (dbDate.length() > 19) {

            	strDateFormatted = strDateFormatted +  dbDate.substring(10, 19);
            }
            }
            strDate = strDateFormatted;

        }


        return strDate  ;
    }

    /**
     * Returns the current system date
     *
     * @param format
     *            of the date e.g mm/dd/yyyy
     * @return daate as string in specified format.
     */
    public static String getCurrentDate(String format) {
     //   String date = "";

        if (StringUtil.isEmpty(format))
        {
        	format = Configuration.getAppDateFormat();
        }

        if (!format.equals(Configuration.getAppDateFormat()))
        {
        	format = Configuration.getAppDateFormat();
        }


         Calendar cal = Calendar.getInstance();


    	  SimpleDateFormat dateFormat = new SimpleDateFormat(format);

    	  return dateFormat.format(cal.getTime());



    }

    /**
     * Returns the current system date in mm/dd/yyyy format
     *
     * @return daate as string in mm/dd/yyyy format.
     */
    public static String getCurrentDate() {
        return getCurrentDate( Configuration.getAppDateFormat() );
    }

    /**
     * returns HTML String to create an Year dropdown
     *
     * @param ddName
     *            HTML name for the dropdown
     * @param selectedValue
     *            any selected value for the dropdown
     * @param isDisabled
     *            if the dropdown should be disabled
     */

    public static String getYearDropDown(String ddName, String selectedValue,
            boolean isDisabled) {
        Calendar cal = Calendar.getInstance();
        int currYear = cal.get(cal.YEAR);
        int i = 1850;
        int beginCount = 0;
        int count = 0;
        int intSelValue;
        String disabledStr = "";

        if (isDisabled) {
            disabledStr = " DISABLED ";
        }

        if (StringUtil.isEmpty(ddName)) {
            ddName = "YearDD";
        }

        if (StringUtil.isEmpty(selectedValue)) {
            intSelValue = currYear;
        } else {
            intSelValue = StringUtil.stringToNum(selectedValue);

        }

        StringBuffer sbYear = new StringBuffer();

        sbYear.append("<select " + disabledStr + " name = " + ddName + ">");
        sbYear.append("<option SELECTED value = 0>----</option>");

        beginCount = currYear + 100;

        for (count = beginCount; count >= i; count--) {
            if (count == intSelValue) {
                sbYear.append("<option SELECTED value = " + count + ">" + count
                        + "</option>");
            } else {
                sbYear.append("<option  value = " + count + ">" + count
                        + "</option>");
            }

        }
        sbYear.append("</select>");
        return sbYear.toString();

    }

    /**
     * returns HTML String to create a Month dropdown
     *
     * @param ddName
     *            HTML name for the dropdown
     * @param selectedValue
     *            any selected value for the dropdown
     * @param isDisabled
     *            if the dropdown should be disabled
     */

    public static String getMonthDropDown(String ddName, String selectedValue,
            boolean isDisabled) {

        int cnt = 0;
        String disabledStr = "";
        StringBuffer sbMonth = new StringBuffer();
        int intSelValue = 0;

        Calendar cal = Calendar.getInstance();
        int currMonth = cal.get(cal.MONTH);

        currMonth = currMonth + 1;

        if (isDisabled) {
            disabledStr = " DISABLED ";
        }

        if (StringUtil.isEmpty(ddName)) {
            ddName = "MonthDD";
        }

        if (StringUtil.isEmpty(selectedValue)) {
            intSelValue = currMonth;
        } else {
            intSelValue = StringUtil.stringToNum(selectedValue);

        }

        sbMonth.append("<select " + disabledStr + " name = " + ddName + ">");
        sbMonth.append("<option SELECTED value = 0>--</option>");

        for (cnt = 1; cnt <= 12; cnt++) {
            if (cnt == intSelValue) {
                sbMonth.append("<option SELECTED value = " + cnt + ">" + cnt
                        + "</option>");
            } else {
                sbMonth.append("<option  value = " + cnt + ">" + cnt
                        + "</option>");
            }

        }

        sbMonth.append("</select>");
        return sbMonth.toString();

    }

    /** Adds or subtracts number of days to a date.
     * Returns a String of new date
     * @param dateOrig String to add/subtract days from
     * @param numberOfDays Number of days to add or subtract. Pass a negative value to subtract. */
    public static String calculateNewDate( String dateOrig, int  numberOfDays)
    {

    	 Date dtOr = new Date();
    	 String newDate = "";
    	 Calendar cal = Calendar.getInstance();
    	 String dayStr ="";
    	 String monthStr ="";
        try{
	    	 if (! StringUtil.isEmpty(dateOrig))
	    	 {
		    	 dtOr = DateUtil.stringToDate(dateOrig, Configuration.getAppDateFormat());

		    	 if (dtOr != null)
		    	 {
		    		 cal.setTime(dtOr);
		    		 cal.add(Calendar.DAY_OF_MONTH, numberOfDays);
		    		 //KM:Fix for the issue 4813
		    		 dayStr = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
		    		 monthStr = String.valueOf(cal.get(Calendar.MONTH)+1);
		    		 if (Integer.parseInt(monthStr) <=9 )
		    			 monthStr = "0" + monthStr;
		    		 if (Integer.parseInt(dayStr) <=9 )
		    			 dayStr = "0" + dayStr;

		    		 //newDate = (cal.get(Calendar.MONTH) + 1)+ "/" + cal.get(Calendar.DAY_OF_MONTH) + "/" + cal.get(Calendar.YEAR);
		    		 newDate = DateUtil.getFormattedDateString(String.valueOf(cal.get(Calendar.YEAR)),
		    				 dayStr,monthStr);
		    	 }
	    	 }
        } catch (Exception ex)
        {
        	 Rlog.fatal("date", "Exception in:calculateNewDate" + ex.getMessage());

        }
    	 return newDate;
    }

    /** returns the current date time in String format*/
    public static String  getCurrentDateTime()
    {
    	Calendar calToday=  Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat (Configuration.getAppDateFormat() +" H:mm:ss" );
		sdf.setTimeZone(calToday.getTimeZone());
		String curdate = sdf.format(calToday.getTime()) ;
		return curdate;

    }

    public static int getNumberOfDaysFromUnit(int num, String unit)
    {
    	int number_of_days = 0;

    	if (StringUtil.isEmpty(unit))
    	{

    		unit = "D";
    	}

    	if ( unit.equals("D") )
    	{
    		number_of_days = num;
    	}
    	else if ( unit.equals("W") )
    	{
    		number_of_days = num * 7;
    	} if ( unit.equals("M") )
    	{
    		number_of_days = num * 30;
    	}
    	if ( unit.equals("Y") )
    	{
    		number_of_days = num * 365;
    	}

    	return number_of_days ;

    }

    /** Returns TO date as String for NULL date parameter*/
    public static String  getToDateStringForNullDate()
    {
    	String yr = "3000";
    	String dt = "01";
    	String month = "01";
    	String dateString = "";

    	dateString = getFormattedDateString(yr,dt,month);

    	return dateString;

    }

    /** Returns FROM date as String for NULL date parameter*/
    public static String  getFromDateStringForNullDate()
    {
    	String yr = "1900";
    	String dt = "01";
    	String month = "01";
    	String dateString = "";

    	dateString = getFormattedDateString(yr,dt,month);

    	return dateString;

    }


    /** Returns FROM date as String for NULL date parameter*/
    public static int getMonthMaxNumberOfDays(String month,String year)
    {
    	int maxDay =0;

    	Calendar calendar = Calendar.getInstance();

        int yr = StringUtil.stringToNum(year);

        int mo = StringUtil.stringToNum(month)-1;

        int date = 1;

        calendar.set(yr, mo, date);

        maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        return maxDay;

    }

    public static String  getFormattedDateString(String year,String dt,String month)
    {
    	//get the default setting

        String formatForDate = "";


        formatForDate  = Configuration.getAppDateFormat();

        if (StringUtil.isEmpty(formatForDate ))
        {

        	formatForDate = "MM/dd/yyyy";
        }



        if (monthStringShort.length>StringUtil.stringToNum(month))
        {
        	formatForDate = formatForDate.replace("MMM",DateUtil.monthStringShort[StringUtil.stringToNum(month)]);
        }

        /*
        // KV: Fixed Bug No.4813. Appended "0" in case of single digit number for Day & month.
        if(Integer.parseInt(month)<=9)
        formatForDate = formatForDate.replace("MM","0"+month);
        else
        formatForDate = formatForDate.replace("MM",month);
        formatForDate = formatForDate.replace("yyyy",year);
        if(Integer.parseInt(dt)<=9)
        formatForDate = formatForDate.replace("dd","0"+dt);
        else
        formatForDate = formatForDate.replace("dd",dt);
		*/

        //KM: Above code commented to fix date format related issues 5038 and 5064.
        formatForDate = formatForDate.replace("MM",month);
        formatForDate = formatForDate.replace("yyyy",year);
        formatForDate = formatForDate.replace("dd",dt);

    	return formatForDate;
    }

    public static java.util.Date stringToDate(String dtString) {
        String format  = Configuration.getAppDateFormat();
        if (StringUtil.isEmpty(format)) {
        	format = "MM/dd/yyyy";
        }
        java.util.Date dt = null;
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            dt = sdf.parse(dtString);
    	} catch(Exception e) {
            Rlog.fatal("date", "Could not convert " + dtString
            		+ "to Date due to exception: " + e);
    	}
    	return dt;
    }

    public static int getMonth(String dtString) {
        String format  = Configuration.getAppDateFormat();
        if (StringUtil.isEmpty(format)) {
        	format = "MM/dd/yyyy";
        }
        java.util.Date dt = null;
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            dt = sdf.parse(dtString);
    	} catch(Exception e) {
            Rlog.fatal("date", "Could not convert " + dtString
            		+ "to Date due to exception: " + e);
    	}


    	return dt.getMonth()+1;
    }

    public static int getYear(String dtString) {
        String format  = Configuration.getAppDateFormat();
        if (StringUtil.isEmpty(format)) {
        	format = "MM/dd/yyyy";
        }
        java.util.Date dt = null;
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            dt = sdf.parse(dtString);
    	} catch(Exception e) {
            Rlog.fatal("date", "Could not convert " + dtString
            		+ "to Date due to exception: " + e);
    	}


    	return dt.getYear()+1900;
    }

    public static int getDate(String dtString) {
        String format  = Configuration.getAppDateFormat();
        if (StringUtil.isEmpty(format)) {
        	format = "MM/dd/yyyy";
        }
        java.util.Date dt = null;
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            dt = sdf.parse(dtString);
    	} catch(Exception e) {
            Rlog.fatal("date", "Could not convert " + dtString
            		+ "to Date due to exception: " + e);
    	}


    	return dt.getDate();
    }
    /**
     * This method pre-pads the year with 0's if it's less than 1000.
     * It assumes that the input String is in the only format used by
     * java.sql.Date.toString(), which is "yyyy-MM-dd". Do not use this
     * method for any other formats.
     * @return string of date in "yyyy-MM-dd"
     */
    public static String prepadYear(String dtStr) {
        if (dtStr == null) { return null; }
        String[] parts = dtStr.split("-");
        StringBuffer sb = new StringBuffer();
        try {
            int length = parts[0].length();
            switch(length) {
            case 4:
                sb.append(parts[0]);
                break;
            case 3:
                sb.append("0");
                sb.append(parts[0]);
                break;
            case 2:
                sb.append("00");
                sb.append(parts[0]);
                break;
            case 1:
                sb.append("000");
                sb.append(parts[0]);
                break;
            case 0:
            default:
                sb.append("0001"); // There is no year 0 => make it year 1
            }
            sb.append("-").append(parts[1]).append("-").append(parts[2]);
        } catch (Exception e) {
            Rlog.fatal("DateUtil", "Exception in prepadYear occurred: "+e);
            return null;
        }
        return sb.toString();
    }
    
    /**
     * Check if the date string passed in is valid according to the application date format 
     * @param dtStr
     * @return true if valid; otherwise false
     */
    public static boolean isValidDateFormat(String dtStr) {
    	if (dtStr == null) { return false; }
    	boolean isValid = false;
    	String format  = Configuration.getAppDateFormat();
    	SimpleDateFormat sdf = new SimpleDateFormat(format);
    	//YK:Fixed Bug#8355
    	if (dtStr.trim().length() != sdf.toPattern().length())
    	      return false;
    	sdf.setLenient(false);
    	Date myDate = null;
    	try {
    		myDate = sdf.parse(dtStr);
    		if (myDate.getYear()+1900 > 9999) {
    			isValid = false;
    		} else {
    			isValid = true;
    		}
    	} catch(Exception e) {
    		isValid = false;
    	}
    	return isValid;
    }
    
    /**
     * Same as Configuration.getAppDateFormat(). Added for convenience of using DateUtil.
     * @return String - application date format
     */
    public static String getAppDateFormat() {
    	return Configuration.getAppDateFormat();
    }
    
    
    public static Date addDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }

    // end of class
}
