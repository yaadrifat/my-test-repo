/*
 * Classname : UserAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 02/23/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana
 */

package com.velos.eres.service.userAgent.impl;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.UserDao;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.business.userPwdHistory.impl.UserPwdHistoryBean;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.Security;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP UserBean.<br>
 * <br>
 * 
 * @author Dinesh Khurana
 * @version 1.0 02/23/2001
 * @ejbHome UserAgentHome
 * @ejbRemote UserAgentRObj
 * @see UserBean
 */

@Stateless
@Remote( { UserAgentRObj.class })
public class UserAgentBean implements UserAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
	@Resource
	private SessionContext context;

    /**
     * Calls getUserDetails() on User Entity Bean UserBean. Looks up on the User
     * id parameter passed in and constructs and returns a User state holder.
     * containing all the summary details of the User<br>
     * 
     * @param userId
     *            the User id
     * @ee UserBean
     */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public UserBean getUserDetails(int userId) {

        UserBean userBean = null;

        try {
            Rlog
                    .debug("user", "in getuserdetails agentbean userId## "
                            + userId);

            // UserPK pkuser;
            // pkuser = new UserPK(userId);
            // Rlog.debug("user", "in getuserdetails agentbean pkuser## "+
            // pkuser);
            // UserHome userHome = EJBUtil.getUserHome();

            userBean = (UserBean) em.find(UserBean.class, new Integer(userId));
            // toreturn = userBean.getUserStateKeeper();
            // Rlog.debug("user", "in getuserdetails agentbean toreturn "+
            // toreturn);
            return userBean;
        } catch (Exception e) {
            Rlog.fatal("useer", "Error in getUserDetails() in UserAgentBean"
                    + e);

        }
        return userBean;

    }

    /**
     * Calls setUserDetails() on User Entity Bean UserBean.Creates a new User
     * with the values in the StateKeeper object.
     * 
     * @param user
     *            a user state keeper containing user attributes for the new
     *            User.
     * @return int - 0 if successful; -2 for Exception;
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int setUserDetails(UserBean user) {
        try {
            UserBean usrBean = new UserBean();
            usrBean.updateUser(user);
            em.persist(usrBean);
            return (usrBean.getUserId());
        } catch (Exception e) {
            Rlog.fatal("user", "Error in setUserDetails() in UserAgentBean "
                    + e);
        }
        return 0;
    }

    /**
     * Calls updateUser() on User Entity Bean UserBean.Updates the User with the
     * values in the StateKeeper object.
     * 
     * @param user
     *            a user state keeper containing user attributes for the new
     *            User.
     * @return int - 0 if successful; -2 for Exception;
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int updateUser(UserBean usk) {

        int output;
        UserBean userBean = null;

        try {
            /*
             * UserPK pkuser; pkuser = new UserPK(usk.getUserId()); UserHome
             * userHome = EJBUtil.getUserHome(); retrieved =
             * userHome.findByPrimaryKey(pkuser);
             */
            userBean = (UserBean) em.find(UserBean.class, usk.getUserId());
            if (userBean == null) {
                return -2;
            }
            output = userBean.updateUser(usk);

        } catch (Exception e) {
            Rlog.debug("user", "Error in updateUser in UserAgentBean" + e);
            return -2;
        }
        return output;
    }
   
    // Overloaded for INF-18183 ::: AGodara
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int updateUser(UserBean usk,Hashtable<String, String> auditInfo) {

        int output;
        UserBean userBean = null;
        AuditBean audit=null;
        
        try {
        	String currdate =DateUtil.getCurrentDate();
        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE);// Fetches the APP_Module from the Hashtable
            String rid= AuditUtils.getRidValue("ER_USER","eres","PK_USER="+usk.userId); //Fetches the RID
            
            //POPULATE THE AUDIT BEAN 
            audit = new AuditBean("ER_USER",String.valueOf(usk.userId),rid,userID,currdate,app_module,ipAdd,reason);
        	userBean = (UserBean) em.find(UserBean.class, usk.getUserId());
            
        	if (userBean == null) {
                return -2;
            }
        	
            em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
            output = userBean.updateUser(usk);
            if(output!=0){
            	context.setRollbackOnly();
            }	
            
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.debug("user", "Error in updateUser in UserAgentBean" + e);
            return -2;
        }
        return output;
    }
    /**
     * Checks if there is a User for the given Login name and Password
     * 
     * @param user
     *            a user state keeper containing user attributes for the new
     *            User.
     * @return UserStateKeeper object with user details in case its a valid
     *         user. Returns null object in case of invalid users.
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public UserBean validateUser(String username, String password) {
    	return validateUser(username, password, false);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public UserBean validateUser(String username, String password, boolean enableSessionMode) {

        UserBean userBean = null;
        try {

            /*
             * UserHome userHome = EJBUtil.getUserHome(); password =
             * Security.encrypt(password); Rlog.debug("user", "****" +
             * password); retrieved = userHome.findValidUser(username,
             * password); toreturn = retrieved.getUserStateKeeper();
             */

            Query queryUserIdentifier = em.createNamedQuery("findValidUser");
            queryUserIdentifier.setParameter("user_logname", username);
            String encPwd=Security.encryptSHA(password);
            queryUserIdentifier.setParameter("user_pwd", encPwd);
            userBean = (UserBean) queryUserIdentifier.getSingleResult();
            // toreturn=userBean.getUserStateKeeper();
            return userBean;

        } catch (Exception e) {
        	if (!enableSessionMode) {
                Rlog.debug("user", "Error in validateUser() in UserAgentBean" + e);
        		e.printStackTrace();
        		return userBean;
        	}
        	try {
                Query queryBySession = em.createNamedQuery("findLoggedInUser");
                queryBySession.setParameter("user_logname", username);
                queryBySession.setParameter("user_pwd", password);
                userBean = (UserBean) queryBySession.getSingleResult();
        	} catch(Exception e1) {
                Rlog.debug("user", "Error in validateUser() in UserAgentBean" + e1);
                e1.printStackTrace();
        	}
        }

        return userBean;
    }

    /**
     * Calls getUserValues() on UserDao;
     * 
     * @param Id
     *            of a Group
     * @return UserDao object with details of all Users of a specified Group
     * @ee UserDao
     */

    public UserDao getByGroupId(int groupId) {
        try {
            UserDao userDao = new UserDao();
            userDao.getUserValues(groupId);
            Rlog
                    .debug("user",
                            "In getByGroupId in UserAgentBean line number 2");
            return userDao;
        } catch (Exception e) {
            Rlog.fatal("user", "Exception In findByAccountId in UserAgentBean "
                    + e);
        }
        return null;

    }

    /**
     * Calls getAccountUsers() on UserDao;
     * 
     * @param Id
     *            of an Account
     * @return UserDao object with details of all Users of a specified Account
     * @ee UserDao
     */

    public UserDao getAccountUsers(int accId) {
        try {
            UserDao userDao = new UserDao();
            userDao.getAccountUsers(accId);
            Rlog.debug("user",
                    "In getAccountUsers in UserAgentBean line number 2");
            return userDao;
        } catch (Exception e) {
            Rlog.fatal("user", "Exception In getAccountUsers in UserAgentBean "
                    + e);
        }
        return null;

    }

    /**
     * Calls getAccountUsers() on UserDao;
     * 
     * @param Id
     *            of an Account
     * @param status
     *            User status
     * @return UserDao object with details of all Users of a specified Account
     * @ee UserDao
     */

    //Modified by Manimaran to include firstname and  lastname in search criteria.
    public UserDao getAccountUsers(int accId, String userStatus, String searchUsr,String from) {
        try {
 
            UserDao userDao = new UserDao();
            userDao.getAccountUsers(accId, userStatus,searchUsr,from);
            return userDao;
        } catch (Exception e) {
            Rlog.fatal("user", "Exception In getAccountUsers in UserAgentBean "
                    + e);
        }
        return null;

    }

    /*
     * Modified by Sonia - 08/06/04 Changed method Name
     * getActivatedDeactivatedUsers() to getUsersSelectively() added javadoc
     */

    /**
     * Calls getUsersSelectively() on UserDao;
     * 
     * @param accId
     *            Account Id
     * @param status
     *            a comma separated list of status sun types. Possible Values:
     *            <BR>
     *            A - Active <br>
     *            B - Blocked <br>
     *            D - Deactivated <br>
     *            P - Pending <br>
     *            X - Deleted <br>
     *            Example a string of "'D','B'" will return Blocked and
     *            Deactivated users
     * @return UserDao object with details of all Users of a specified Account
     *         and for a list of status types
     * @see UserDao
     */
    //Modified by Manimaran to include firstname and  lastname in search criteria.
    public UserDao getUsersSelectively(int accId, String status, String searchUsr,String from) {
        try {

            UserDao userDao = new UserDao();
            userDao.getUsersSelectively(accId, status, searchUsr,from);
            return userDao;
        } catch (Exception e) {
            Rlog.fatal("user",
                    "Exception In getUsersSelectively in UserAgentBean " + e);
        }
        return null;

    }

    /**
     * Calls getNonSystemUsers() on UserDao;
     * 
     * @param Id
     *            of an Account
     * @param user
     *            Type
     * @return UserDao object with details of all Users of a specified Account
     * @ee UserDao
     */

    //Modified by Manimaran to include firstname and  lastname in search criteria.
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public UserDao getNonSystemUsers(int accId, String userType, String searchUsr,String from) {
        try {
            UserDao userDao = new UserDao();
            userDao.getNonSystemUsers(accId, userType, searchUsr,from);
            return userDao;
        } catch (Exception e) {
            Rlog.fatal("user",
                    "Exception In getNonSystemUsers in UserAgentBean " + e);
        }
        return null;

    }

    /**
     * Calls getAccountUsersDetails() on UserDao;
     * 
     * @param Id
     *            of an Account
     * @return UserDao object with details of all Users of a specified Account
     * @ee UserDao
     */

    public UserDao getAccountUsersDetails(int accId) {
        try {
            UserDao userDao = new UserDao();
            userDao.getAccountUsersDetails(accId);
            Rlog.debug("user",
                    "In getAccountUsersDetails in UserAgentBean line number 2");
            return userDao;
        } catch (Exception e) {
            Rlog
                    .fatal("user",
                            "Exception In getAccountUsersDetails in UserAgentBean "
                                    + e);
        }
        return null;

    }
    
    /**
     * Calls getAccountUsersDetailsForLookup() on UserDao;
     * 
     * @param Account Id
     * @return UserDao object with details of all Users of a specified Account
     *         including non-system users
     * @see UserDao.getAccountUsersDetailsForLookup()
     */
    public UserDao getAccountUsersDetailsForLookup(int accId) {
        try {
            UserDao userDao = new UserDao();
            userDao.getAccountUsersDetailsForLookup(accId);
            return userDao;
        } catch (Exception e) {
            Rlog.fatal("user",
                    "Exception In getAccountUsersDetailsForLookup in UserAgentBean "+ e);
        }
        return null;
    }
    

    /**
     * Calls getUserValuesByLoginName() on UserDao;
     * 
     * @param logName
     *            Login Name
     * @return UserDao object with details of the User for the specified Login
     *         Name
     * @ee UserDao
     */

    public UserDao getUserValuesByLoginName(String logName) {
        try {
            UserDao userDao = new UserDao();
            userDao.getUserValuesByLoginName(logName);
            Rlog
                    .debug("user",
                            "In getByGroupId in getUserValuesByLoginName line number 2");
            return userDao;
        } catch (Exception e) {
            Rlog.fatal("user",
                    "Exception In getUserValuesByLoginName in UserAgentBean "
                            + e);
        }
        return null;
    }

    /**
     * Calls getCount() on UserDao;
     * 
     * @param userLogin
     *            Login ID
     * @return int with count of number of users with the same login ID as
     *         userLogin
     * @see UserDao
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int getCount(String userLogin) {
        int count = 0;
        try {
            UserDao userDao = new UserDao();
            count = userDao.getCount(userLogin);
            Rlog.debug("user", "In getCount line number 2");
        } catch (Exception e) {
            Rlog.fatal("user", "Exception In getCount in UserAgentBean " + e);
        }
        return count;
    }

    /**
     * Calls getUserCode() on UserDao;
     * 
     * @param userId
     *            UserId
     * @return String the user code corresponding to userId
     * @see UserDao
     */

    public String getUserCode(String userId) {
        String userCode = "";
        try {
            UserDao userDao = new UserDao();
            userCode = userDao.getUserCode(userId);
            Rlog.debug("user", "In getUserCode line number 2");
        } catch (Exception e) {
            Rlog
                    .fatal("user", "Exception In getUserCode in UserAgentBean "
                            + e);
        }
        return userCode;
    }

    /**
     * Calls getUsersWithStudyCopy(String studyId, String accountId) on UserDao;
     * 
     * @param studyId
     *            StudyId
     * @param accountId
     *            AccountId
     * @see UserDao
     */

    public UserDao getUsersWithStudyCopy(String studyId, String accountId) {
        UserDao userDao = new UserDao();
        try {
            userDao.getUsersWithStudyCopy(studyId, accountId);
            Rlog.debug("user", "In getUsersWithStudyCopy line number 2");
        } catch (Exception e) {
            Rlog.fatal("user",
                    "Exception In getUsersWithStudyCopy in UserAgentBean " + e);
        }
        return userDao;
    }

    /**
     * Calls getUsersForEvent(int eventId, String userType) on UserDao;
     * 
     * @param eventId
     *            EventId
     * @param userType
     *            userType
     * @see UserDao
     */

    public UserDao getUsersForEvent(int eventId, String userType) {
        UserDao userDao = new UserDao();
        try {
            userDao.getUsersForEvent(eventId, userType);
            Rlog.debug("user", "In getUsersForEvent line number 2");
        } catch (Exception e) {
            Rlog.fatal("user",
                    "Exception In getUsersForEvent in UserAgentBean " + e);
        }
        return userDao;
    }

    /**
     * Calls getAvailableEventUsers(int accId, String lName, String fName,
     * String userType, int eventId) on UserDao;
     * 
     * @param accId
     *            AccountId
     * @param lName
     *            Last Name
     * @param fName
     *            First Name
     * @param userType
     *            User Type
     * @param eventId
     *            Event Id
     * @see UserDao
     */

    public UserDao getAvailableEventUsers(int accId, String lName,
            String fName, String role, String organization, String userType,
            int eventId) {
        UserDao userDao = new UserDao();
        try {
            userDao.getAvailableEventUsers(accId, lName, fName, role,
                    organization, userType, eventId);
            Rlog.debug("user", "In getAvailableEventUsers line number 2");
        } catch (Exception e) {
            Rlog
                    .fatal("user",
                            "Exception In getAvailableEventUsers in UserAgentBean "
                                    + e);
        }
        return userDao;
    }

    /**
     * Calls getUsersDetails(String userList) on UserDao;
     * 
     * @param userList
     *            a comma separated list of userIds
     * @see UserDao
     */

    public UserDao getUsersDetails(String userList) {
        UserDao userDao = new UserDao();
        try {
            userDao.getUsersDetails(userList);
            Rlog.debug("user", "In getUsersDetails line number 2");
        } catch (Exception e) {
            Rlog.fatal("user", "Exception In getUsersDetails in UserAgentBean "
                    + e);
        }
        return userDao;
    }

    /*
     * Returns the number of users in the account 'accId'
     */

    public int getUsersInAccount(int accId) {
        UserDao userDao = new UserDao();
        int rows = 0;
        try {
            rows = userDao.getUsersInAccount(accId);
            Rlog.debug("user", "In getUsersInAccount line number 2");
        } catch (Exception e) {
            Rlog.fatal("user",
                    "Exception In getUsersInAccount in UserAgentBean " + e);
        }
        return rows;
    }

    /**
     * added by JM dt. 27Jan05 Returns the number active users in the account
     * 'accId'
     */

    public int getActiveUsersInAccount(int accId) {
        UserDao userDao = new UserDao();
        int rows = 0;
        try {
            rows = userDao.getActiveUsersInAccount(accId);
            Rlog.debug("user", "In getActiveUsersInAccount line number 2");
        } catch (Exception e) {
            Rlog.fatal("user",
                    "Exception In getActiveUsersInAccount in UserAgentBean "
                            + e);
        }
        return rows;
    }

    /*
     * Returns the number of users in the site(organization) @param site Id
     */

    public UserDao getSiteUsers(int siteId) {
        UserDao userDao = new UserDao();
        try {
            userDao.getSiteUsers(siteId);
            Rlog.debug("user", "In getSiteUsers line number 2");
        } catch (Exception e) {
            Rlog.fatal("user", "Exception In getSiteUsers in UserAgentBean "
                    + e);
        }
        return userDao;
    }

    /**
     * Checks if there is a User for the given Login name
     * 
     * @param userlogin
     * @return UserStateKeeper object with user details in case its a valid
     *         user. Returns null object in case of invalid users.
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public UserBean findUser(String username) {

        UserBean userBean = null;

        try {

            Query queryUserIdentifier = em.createNamedQuery("findUser");
            queryUserIdentifier.setParameter("user_logname", username);
            userBean = (UserBean) queryUserIdentifier.getSingleResult();
            Rlog.debug("user", "eres" + userBean);
            // toreturn=userBean.getUserStateKeeper();
        } catch (Exception e) {
            Rlog.fatal("user", "Error in findUser() in UserAgentBean" + e);
        }

        return userBean;
    }

    /*
     * public Collection findUser(String username) { UserBean userBean=null;
     * Collection userCollect=em .createQuery( "from UserBean r where
     * trim(r.userLoginName)=:usrName") .setParameter("usrName",
     * username).getResultList(); for (Iterator iter = userCollect.iterator();
     * iter.hasNext();) { userBean=(UserBean)iter.next(); }
     * 
     * Rlog.debug("user", "Return User " + userBean); Rlog.debug("user", "Return
     * User Name " + userBean.getUserFirstName()+ " Last Name"
     * +userBean.getUserLastName()); return userCollect; }
     */

    /*
     * public void getGroupUsers(String groupIds) {
     * 
     * try { UserHome userHome = EJBUtil.getUserHome(); UserRObj usr =
     * userHome.findUser(groupIds); Rlog.debug("user",
     * "UserAgentBean.getGroupUsers UserStateKeeper " + usr); } catch (Exception
     * e) { Rlog .fatal("user", "Error in getGroupUsers() in UserAgentBean " +
     * e); } }
     */

    /*
     * public void getStudyUsers(String studyIds) {
     * 
     * try { UserHome userHome = EJBUtil.getUserHome(); UserRObj usr =
     * userHome.findUser(studyIds); Rlog.debug("user",
     * "UserAgentBean.getStudyUsers UserStateKeeper " + usr); } catch (Exception
     * e) { Rlog .fatal("user", "Error in getStudyUsers() in UserAgentBean " +
     * e); } }
     */

    /*
     * public void getOrganizationUsers(String orgIds) {
     * 
     * try { UserHome userHome = EJBUtil.getUserHome(); UserRObj usr =
     * userHome.findUser(orgIds); Rlog.debug("user", "UserAgentBean.getOrgUsers
     * UserStateKeeper " + usr); } catch (Exception e) { Rlog.fatal("user",
     * "Error in getOrgUsers() in UserAgentBean " + e); } }
     */

    /* remove user */

    public int deleteUser(UserBean usk) {

        int ret = 0;
        try {

            /*
             * UserPK pkUser; pkUser = new UserPK(usk.getUserId());
             * Rlog.debug("user", "in user agent bean pkUser" + pkUser);
             * UserHome userHome = EJBUtil.getUserHome(); Rlog.debug("user", "in
             * user agent bean userHome" + userHome); user =
             * userHome.findByPrimaryKey(pkUser); Rlog.debug("user", "in user
             * agent bean user" + user);
             */

            em.remove(usk);

            Rlog.debug("user", "remove user");
            return 0;
        }

        catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("user", "EXCEPTION IN DELETING USER" + e);
            return -2;

        }

    }
    //JM: 12/10/05 
    public void changeUserType(int userId, int group, String usrLoginName,String usrPass,String usrESign,String usrMail,int loggedInUser){
    	try{
	    	UserDao udao= new UserDao();
	    	udao.changeUserType(userId,group,usrLoginName,usrPass,usrESign,usrMail,loggedInUser);    
	    	Rlog.debug("user", "changeUserType() in UserAgentBean");
    	}
    	catch(Exception e){
    		Rlog.fatal("User","EXCEPTION in changeUserType" + e);
    	}
    }
    
    /** Added by Sonia Abrol, 03/07/06*/
    /** Gets list of user names
     * @param ids comma separated list of user ids 
     * @return list of user names*/
    
    public String getUserNamesFromIds(String ids){
    	String names = "";
    	try{
	    	UserDao udao= new UserDao();
	    	names = udao.getUserNamesFromIds(ids);    
	    	Rlog.debug("user", "getUserNamesFromIds() in UserAgentBean");
    	}
    	catch(Exception e){
    		Rlog.fatal("User","EXCEPTION in getUserNamesFromIds" + e);
    	}
    	return names;
    }
    
    /**
     * Calls getCount() on UserDao;
     * 
     * @param @param userFname, userLname, accId, mode and userId
     *            user's First name,  Last name, account id, mode of gui page, user id
     * @return int with count of number of users with the same First name,  Last name matching per account id 
     *         
     * @see UserDao
     */

    public int getCount(String userLname, String userFname, String accId, String mode, String userId ) {
        int count = 0;
        try {
            UserDao userDao = new UserDao();
            count = userDao.getCount(userLname, userFname, accId, mode, userId);
            Rlog.debug("user", "In getCount line number 2");
        } catch (Exception e) {
            Rlog.fatal("user", "Exception In getCount(String userLname, String userFname, String accId, mode ) in UserAgentBean " + e);
        }
        return count;
    }

    /**
     * Calls getCount() on UserDao;
     * 
     * @param @param email, accId, mode, userId
     *            user's Email, account id, mode of gui page and user id
     * @return int with count of number of users with the same Email per account id
     *         
     * @see UserDao
     */

    public int getCount(String email, String accId , String mode, String userId ) {
        int count = 0;
        try {
            UserDao userDao = new UserDao();
            count = userDao.getCount( email, accId, mode, userId);
            Rlog.debug("user", "In getCount line number 2");
        } catch (Exception e) {
            Rlog.fatal("user", "Exception In getCount(email, accId ) in UserAgentBean " + e);
        }
        return count;
    }

    
    /**
     * Calls deactivateUser() on UserDao;
     * 
     * @param  userPk: user to be deactivated
     * @param creator: logged in user for audit
     * @param ipAdd: ip address of logged in user for audit
     *            
     * @return int returns success flag: 0:successful;-1:unsuccessful
     *         
     * @see UserDao
     */

    public int deactivateUser(String userPk, String creator, String ipAdd) {
        int count = 0;
        try {
            UserDao userDao = new UserDao();
            count = userDao.deactivateUser( userPk,  creator,  ipAdd);
            
            
        } catch (Exception e) {
            Rlog.fatal("user", "Exception In deactivateUser in UserAgentBean " + e);
        }
        return count;
    }
  //APR-04-2011,MSG-TEMPLAGE FOR USER INFO  
    /**
     * Calls notifyUserResetInfo() on UserDao;
     * 
     * @param pWord: password of the user to be notified.
     * @param url: url information.
     * @param eSign: e signature of the user.
     * @param userId: user id information.
     * @param userLogin: login id of the user.               
              
     * @see UserDao
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int notifyUserResetInfo(String pWord, String url,String eSign,Integer userId,String userLogin,String infoFlag, String loggedInUser){
    	try {
            UserDao userDao = new UserDao();
            int  output  =userDao.notifyUserResetInfo( pWord,url,eSign,userId,userLogin,infoFlag,loggedInUser); ////Modified By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.
            return output;
            
        } catch (Exception e) {
            Rlog.fatal("user", "Exception InnotifyUserResetInfo in UserAgentBean " + e);
            return -2;
        }
    }
    
    
    public void flush(){
    	em.flush();
    }
    
    /**
     * To Check User Password Rotation Error
     * 
     * @author Raviesh
     * @Enhancement INF-18669
     * 
     * @param encryptedPass: New Password.
     * @param userId: user id information.
     * @param userAccId: Account id of the user.
     * 
     */
    public Boolean checkPasswordRotation(String encryptedPass, Integer userId,Integer userAccId){
    	try{
    		Query query=em.createNamedQuery("findUserPwdRotation");
    		query.setMaxResults(10);
    		query.setParameter(1, userId);
    		query.setParameter(2, userAccId);
    		ArrayList result=(ArrayList)query.getResultList();
    		if (result == null || result.size() == 0) { return false; }		
    		for(Object o:result){
    			UserPwdHistoryBean userPwdHistoryBean = (UserPwdHistoryBean)o;
    			if(encryptedPass.equals(userPwdHistoryBean.getUserPwd())){
    				return true;
    			}
    		}
   		}catch (Exception e) {
    		Rlog.fatal("user", "Exception in method checkPasswordRotation (UserAgentBean) " + e);
		}
		return false;
    }
    
}// end of class
