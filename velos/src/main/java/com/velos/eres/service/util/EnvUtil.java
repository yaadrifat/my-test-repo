package com.velos.eres.service.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Hashtable;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.velos.base.dbutil.DBEngine;

/**
 * Class to hold utility methods used in the 'velos-base' classes.
 * These methods were copied from EJBUtil in order to remove dependencies
 * of velos-base on the EJB packages.
 * 
 * @author Isaac Huang
 *
 */
public class EnvUtil {
	
	private static boolean disableConnectionPool = false;
    
    static {
        String disableEresPool = System.getenv("DISABLE_ERES_POOL");
        if (StringUtil.isEmpty(disableEresPool)) {
            try {
                disableEresPool = EnvUtil.getEnvVariable("DISABLE_ERES_POOL");
            } catch(Exception e) {}
        }
        if ("%DISABLE_ERES_POOL%".equals(disableEresPool)) {
            disableEresPool = System.getProperty("DISABLE_ERES_POOL");
        }
        if (StringUtil.trueValue(disableEresPool).startsWith("Y")) {
            disableConnectionPool = true;
        }
    }
	
    public static String getEnvVariable(String Name) throws Exception {
        String envValue = "";
        int sType = 0;
        Process theProcess = null;
        BufferedReader stdInput = null;
        try {
            // Figure out what kind of system we are running on
            // 0 = windows
            // 1 = unix
            sType = getSystemType();
            if (sType == 0) {
                String envPath = "cmd /C echo %" + Name + "%";
                theProcess = Runtime.getRuntime().exec(envPath);
            } else {
                theProcess = Runtime.getRuntime().exec("printenv " + Name);
            }

            stdInput = new BufferedReader(
                new InputStreamReader(theProcess.getInputStream())
            );

            envValue = stdInput.readLine();
            stdInput.close();
            theProcess.destroy();
            return envValue;
        } catch (IOException ioe) {
            throw new Exception(ioe.getMessage());
        }
    }

    public static int getSystemType() {
        Properties p = System.getProperties();
        String os = p.getProperty("os.name");
        int osType = 0;
        if (os.indexOf("Win") != -1) {
            osType = 0;
        } else {
            osType = 1;
        }
        return osType;
    }

    /**
    *
    *
    * if the appServerParam are set in configuration class then return it,
    * otherwise read
    *
    *
    * it from xml file and store in appServerParam then return.
    *
    *
    */
   public static Hashtable getContextProp() {
      try {
          if (Configuration.getServerParam() == null)
              Configuration.readSettings();
      } catch (Exception e) {
          Rlog.fatal("common", "EnvUtil:getContextProp:general ex" + e);
      }
      return Configuration.getServerParam();
   }
   
   public static void InitializePool(String alias) {
       String eHome="";
       DBEngine dbengine = DBEngine.getEngine();
       if (!(dbengine.IsInitialized())) {
           eHome = StringUtil.trueValue(System.getenv("ERES_HOME"));
           if (StringUtil.isEmpty(eHome)) {
               try {
                   eHome = EnvUtil.getEnvVariable("ERES_HOME");
               } catch(Exception e) {}
           }
           if (eHome.trim().equals("%ERES_HOME%")) {
               eHome = StringUtil.trueValue(System.getProperty("ERES_HOME"));
           }
           eHome = eHome.trim();
           dbengine.initializePool(eHome+"erespool.properties", alias);
       }
   }
   
   /** *********GET CONNECTION FROM RESOURCE********* */
   public static Connection getConnection() {
       if (!disableConnectionPool) {
           try {
               InitializePool("eres");
               return (DBEngine.getEngine().getConnection("eres")) ;

           } catch (Exception e) {
               Rlog.fatal("common", "getConnection(): exception:" + e);
               return null;
           }
       }
       
       // ////////////
       try {
           Rlog.debug("common",
                   Configuration.DBDriverName);

           if (Configuration.DBDriverName == null) {
               Configuration.readSettings();
           }
           Rlog.debug("common", "DB DRiver = "
                   + Configuration.DBDriverName);
           Rlog.debug("common", "DB URL = "
                   + Configuration.DBUrl);
           Rlog.debug("common", "DB USER= "
                   + Configuration.DBUser);
           Rlog.debug("common", "DB PWD = "
                   + Configuration.DBPwd);
       } catch (Exception e) {
           Rlog.fatal("common", "IN COMMON DAO" + e);
       }

       // ////////////
       Connection conn = null;
       try {
           Class
                   .forName(Configuration.DBDriverName);
           conn = DriverManager.getConnection(
                   Configuration.DBUrl,
                   Configuration.DBUser,
                   Configuration.DBPwd);
           Rlog.debug("common", "Got Connection(): " + conn);
           return conn;

       } catch (Exception e) {
           Rlog.fatal("common", "getConnection(): exception:" + e);
           return null;
       }
   }

}
