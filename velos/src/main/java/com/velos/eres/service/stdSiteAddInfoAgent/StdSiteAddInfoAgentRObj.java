/*
 * Classname : StdSiteAddInfoAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 11/16/2004
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.service.stdSiteAddInfoAgent;

/* Import Statements */
import javax.ejb.Remote;

import com.velos.eres.business.common.StdSiteAddInfoDao;
import com.velos.eres.business.stdSiteAddInfo.impl.StdSiteAddInfoBean;

/* End of Import Statements */

/**
 * Remote interface for StdSiteAddInfoAgent session EJB
 * 
 * @author Anu Khanna
 * @version : 1.0 11/16/2004
 */
@Remote
public interface StdSiteAddInfoAgentRObj {
    /**
     * gets the StdSiteAddInfo record
     */
    StdSiteAddInfoBean getStdSiteAddInfoDetails(int id);

    /**
     * creates a new StdSiteAddInfo Record
     */
    public int setStdSiteAddInfoDetails(StdSiteAddInfoBean ssk);

    /**
     * updates the StdSiteAddInfo Record
     */
    public int updateStdSiteAddInfo(StdSiteAddInfoBean ssk);

    /**
     * remove StdSiteAddInfo Record
     */
    public int removeStdSiteAddInfo(int id);

    /**
     * create multiple study ids
     * 
     */

    public int createMultipleStdSiteAddInfo(StdSiteAddInfoBean ssk);

    /**
     * updates multiple study ids
     * 
     */

    // public int updateMultipleStudyIds(StudyIdStateKeeper ssk) throws
    // RemoteException;
    /**
     * gets study siteadd info ids
     * 
     */

    public StdSiteAddInfoDao getStudySiteAddInfo(int studySiteId);

}
