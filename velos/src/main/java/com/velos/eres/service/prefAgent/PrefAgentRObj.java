/*
 * Classname : PrefAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 06/02/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Sahni
 */

package com.velos.eres.service.prefAgent;

/* Import Statements */
import javax.ejb.Remote;

import com.velos.eres.business.pref.impl.PrefBean;

/* End of Import Statements */

/**
 * Remote interface for PrefAgent session EJB
 * 
 * @author Sonia Sahni
 * @version : 1.0 06/02/2001
 */
@Remote
public interface PrefAgentRObj {
    /**
     * gets the Pref record
     */
    PrefBean getPrefDetails(int prefPKId);

    /**
     * creates a new PREF
     */
    public PrefBean setPrefDetails(PrefBean psk);

    /**
     * updates PREF
     */
    public int updatePref(PrefBean psk);

    /**
     * remove Pref
     */
    public int removePref(int prefPKId);

    public boolean patientCodeExists(String patientCode, String siteId);
    public boolean patientCodeExistsOneOrg(String patientCode, String siteId);
    public boolean patientFacilityCodeExists(String patfacilityid,String patientStudyID);
   public boolean patientCheck(String fname,String lname,String date, String siteId,String patId) ;
}
