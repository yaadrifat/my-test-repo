/*
 * Classname : StudySiteAgentRObj
 *
 * Version information: 1.0
 *
 * Date: 10/27/2004
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.service.studySiteAgent;

/* Import Statements */

import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.business.common.StudySiteDao;
import com.velos.eres.business.studySite.impl.StudySiteBean;

/* End of Import Statements */

/**
 * Remote interface for StudySiteAgent session EJB
 *
 * @author Anu Khanna
 */
/* Modified by Sonia Abrol, 03/23/04 , added method findBeanByStudySite */
@Remote
public interface StudySiteAgentRObj {
    /**
     * gets the StudySite record
     */
    StudySiteBean getStudySiteDetails(int studySiteId);

    /**
     * creates a new StudySite Record
     */
    public int setStudySiteDetails(StudySiteBean ssk);

    /**
     * updates the StudySite Record
     */
    public int updateStudySite(StudySiteBean ssk);

    /**
     * remove StudySite Record
     */
    public int removeStudySite(int studySiteId);

    public StudySiteDao getCntForSiteInStudy(int studyId, int siteId);

    public StudySiteDao getStudySiteTeamValues(int studyId, int orgId,
            int userId, int accId);

    public StudySiteDao getAllStudyTeamSites(int studyId, int accId);
    
    public StudySiteDao getStudyTeamSites(int studyId, int accId);

    public int getCountSitesForUserStudy(int studyId, int userId, int accountId);

    public StudySiteDao getSitesForUserStudy(int studyId, int userId,
            int accountId);

    public StudySiteDao getStdSitesLSampleSize(int studyId);

    public int updateLSampleSizes(String[] studySiteIds, String[] localSamples,
            int modifiedBy, String ipAdd);

    public StudySiteDao getAddedAccessSitesForStudy(int studyId, int userId,
            int accountId);

    public int deleteOrgFromStudyTeam(int studyId, int accId, int siteId, int userId);
    
    //Overloaded for INF-18183 ::: Ankit
    public int deleteOrgFromStudyTeam(int studyId, int accId, int siteId, int userId, Hashtable<String, String> auditInfo);

    public int checkSuperUser(int studyId, int userId, int accountId);

    public StudySiteDao getSitesForStatAndEnrolledPat(int studyId, int userId,
            int accId);

    /**
     * finds the StudySite record
     */
    StudySiteBean findBeanByStudySite(int study, int site);

}
