/*
 * Classname			FormLibAgentRObj
 * 
 * Version information : 1.0  
 *
 * Date					07/11/2003
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.formLibAgent;

import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.business.common.FormLibDao;
import com.velos.eres.business.formLib.impl.FormLibBean;
import com.velos.eres.business.saveForm.SaveFormStateKeeper;

/**
 * Remote interface for FormLibAgentBean session EJB
 * 
 * @author Anu Khanna
 * @version 1.0, 07/11/2003
 */

@Remote
public interface FormLibAgentRObj {
    public FormLibBean getFormLibDetails(int formLibId);

    public int setFormLibDetails(FormLibBean formlib);

    public int updateFormLib(FormLibBean flsk);
    
    public int updateFormLib(FormLibBean flsk, boolean skipNameCheck);
    
 // Overloaded for INF-18183 ::: AGodara
    public int updateFormLib(FormLibBean flsk,Hashtable<String, String> auditInfo);

    public FormLibDao getFormDetails(int accountId, int userId,
            String formName, int formType);
    //Added by IA bug 2614 multiple organization access
    public FormLibDao getFormDetails(int accountId, int userId,
            String formName, int formType, String orgId);
    //end added
    public int[] insertCopyFieldToForm(int pkForm, int pkSection, int pkField,
            String user, String ipAdd);

    public FormLibDao getSectionFieldInfo(int formId);

    public int copyFormsFromLib(String[] idArray, String formType,
            String formName, String creator, String ipAdd, String formDesc);

    public int createFormShareWithData(int userId, int accountId, int siteId,
            int creator, String ipAdd);

    public int updateInsertForStatusChange(int formLibId, int newStatus,
            int creator, String ipAdd);
    public int updateFilledFormBySP(SaveFormStateKeeper sk);
    public int insertFilledFormBySP(SaveFormStateKeeper sk);
  //Added by Vivek Jha for Monitor Team Role in jupiter
    public int updateFilledFormStatus(SaveFormStateKeeper sk);
    //Added by Manimaran for the Enhancement F6
    public String getFormLibVersionNumber(int formlibverPK);
    
    //added by Sonia, 06/15/07, to get the first PK of a filled form answered for a patient by a creator
    public int getPatFilledFormPKForCreator(int form,int patientPK,int creator);

	public int validateFormField(int formID, int responseID, String systemID, int studyID, int patientID, int accountID, String datavalue, String operator);
	
	public String validateFormField(int formID, String systemID, int studyID, int patientID, int accountID, String datavalue);
    
	public int getPatFilledFormPKForCreatorForParam(int form,int patientPK,int creator,Hashtable htParam);
	  
}
