/*
 * Classname			StudyVerAgentRObj.class
 * 
 * Version information   
 *
 * Date			11/25/2002
 * 
 * Copyright notice
 */

package com.velos.eres.service.studyVerAgent;

import java.util.Hashtable;
import javax.ejb.Remote;

import com.velos.eres.business.common.StudyVerDao;
import com.velos.eres.business.studyVer.impl.StudyVerBean;

/**
 * Remote interface for StudyVerAgent session EJB
 * 
 * @author Arvind
 */

@Remote
public interface StudyVerAgentRObj {
    /**
     * gets the studyVer details
     */
    StudyVerBean getStudyVerDetails(int studyVerId);

    /**
     * sets the studyVer details
     */
    public int setStudyVerDetails(StudyVerBean srsk);

    public int updateStudyVer(StudyVerBean srsk);

    public StudyVerDao getAllVers(int studyId);

    public StudyVerDao getAllVers(int studyId, String status);

    int removeStudyVer(int id) throws java.rmi.RemoteException;
    
    // Overloaded for INF-18183 ::: AGodara
    int removeStudyVer(int id,Hashtable<String, String> auditInfo) throws java.rmi.RemoteException;

    //int copyStudyVersion(int studyVerId, String newVerNum, int usr);
    int copyStudyVersion(int studyVerId, String newVerNum, String verDate,String verCat, String verType ,int usr);

    int findStudyVersion(int studyId, int versionNum, int versionCategory);
    
    int findStudyVersion(int studyId, String versionNum, int versionCategory);

    //int newStudyVersion(int studyId, String verNum, String verNotes, int usr);
    int newStudyVersion(int studyId, String verNum, String verNotes, String versionDate,String studyVercat, String studyVertype, int usr);

	int newStudyApndx(int oldVerPk, int fkstudy, int fkstudyver, int creator,
			String fileSize, int modifiedBy, String appendixDescription,
			String appendixPubFlag, String appendixSavedFileName,
			String appendixType, String appendixUrl_File, Integer stamp_flag,
			Integer stamp_ref,String bkpAppendix_URI);
   
    //public  StudyVerDao getAllStudyVers(int studyId,String verNum,String verCat, String verType, String verStat, String orderBy, String orderType);
}
