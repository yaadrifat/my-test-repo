/*
 * Classname			StudyAgent.class
 * 
 * Version information   
 *
 * Date					02/16/2001
 * 
 * Copyright notice
 */

package com.velos.eres.service.studyAgent;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.ejb.Remote;

import org.json.JSONArray;

import com.velos.eres.business.common.LookupDao;
import com.velos.eres.business.common.PatStudyStatDao;
import com.velos.eres.business.common.StudyDao;
import com.velos.eres.business.study.UserStudiesStateKeeper;
import com.velos.eres.business.study.impl.StudyBean;

/**
 * Remote interface for StudyAgent session EJB
 * 
 * @author
 */
@Remote
public interface StudyAgent extends Serializable {
    /**
     * gets the study details
     */
    public StudyBean getStudyDetails(int id);

    /**
     * sets the study details
     */
    public int setStudyDetails(StudyBean study);

    /**
     * sets the studies on which user has rights
     */
    public UserStudiesStateKeeper getUserStudies(int usrId);

    /**
     * updates the study
     */
    public int updateStudy(StudyBean study);

    public StudyDao getStudyValuesKeyword(String search);

    public StudyDao getStudyValuesKeywordAll(ArrayList search);

    public StudyDao getStudyValuesAuthor(String search);

    public StudyDao getStudyValuesTitle(String search);

    public StudyDao getStudyValuesTarea(String search);

    public StudyDao getStudyValuesForUsers(String userId);

    public StudyDao getStudyValuesForUsers(String userId, String studycount);

    public String getStudyValuesForGadget(String userId, String studyIds);

    public StudyDao verifyStudyRequest(String studyId, String userId);

    public int getStudyCount(String accountId);

    public void copyStudy(int studyId, int xUser, int author,
            String permission, int msgId);

    public int createVersion(int studyId, int xUser);

    public StudyDao getUserStudies(String userId, String dName, int selVal,
            String flag);

    public StudyDao getUserStudiesWithRights(String userId, String dName,
            int selVal, String siteId);

    public PatStudyStatDao getStudyPatientResults(int studyId,
            String patientID, int patStatus, int user, int userSite);

    /**
     * Gets Patients associated with a study. Filters data according to the
     * parameters passed. If no study is <br>
     * passed, gets data for all the studies
     * 
     * @param studyId
     *            Study Id
     * @param patientID
     *            Patient Code
     * @param patStatus
     *            Patient current status in Study
     * @param user
     *            Logged in user id
     * @param userSite
     *            Organization id
     * @param enrolDt
     *            Enrollment date
     * @param lastDoneDt
     *            Last visit done on
     * @param nextVisitDate
     *            Next visit date
     * @param lastVisit
     *            last Visit Number
     * @param htMoreFilterParams
     *            A Hashtable object with more filter parameters.
     * @return A PatStudyStatDao object with patient enrollment data
     */
    // public PatStudyStatDao getStudyPatientResultsWithVisits(int studyId,
    // String patientID, int patStatus, int user, String userSite, String
    // enrolDt, String lastDoneDt, String nextVisitDate, String
    // lastVisit,Hashtable htMoreFilterParams) ;
    // patStudyId is added by Manimaran on 21Apr2005 for Enrolled PatientStudyId
    // search
    public PatStudyStatDao getStudyPatientResultsWithVisits(int studyId,
            String patientID, String patStudyId, int patStatus, int user,
            String userSite, String enrolDt, String lastDoneDt,
            String nextVisitDate, String lastVisit, Hashtable htMoreFilterParams);

    public int validateStudyNumber(String accId, String studyNum);

    //public int copyStudySelective(int origstudy, int statflag, int teamflag,
      //      String[] verarr, String[] calarr, String studynum, int studydm,
        //    String title, int userId, String ipAdd);
    
    //JM: 31July2006: Modified
    public int copyStudySelective(int origstudy, int statflag, int teamflag, int dictflag, 
            String[] verarr, String[] calarr, String[] txarr, String[] formsarr, String studynum, int studydm,
            String title, int userId, String ipAdd);

    public PatStudyStatDao getPatientStudiesWithVisits(int studyId, int pkey,
            int patStatus, int user, int userSite, String enrolDt,
            String lastDoneDt, String nextVisitDate, String lastVisit);

    public PatStudyStatDao getPatientStudies(int pkey, int user);

    public String getLkpTypeVersionForView(int viewpk);

    public LookupDao getAllViewsForLkpType(int account, String lkptype);

    public int getOrgAccessForStudy(int userId, int studyId, int orgId);

    public int getStudyCompleteDetailsAccessRight(int userId, int groupId,
            int studyId);

    // km
    public void deleteStudy(int studyId);

    // deleteStudy() Overloaded for INF-18183 ::: Raviesh
    public void deleteStudy(int studyId,Hashtable<String, String> args);
    
    // km
    public void deletePatStudy(int studyId, int patId);
    
    // Overloaded for INF-18183 ::: AGodara
    public void deletePatStudy(int studyId, int patId,Hashtable<String, String> auditInfo);
    
    public StudyDao getStudyAutoRows(String userId ,String actId, String studyIds, int criterion);
    /**
     * Public flush method that invokes the EJB's EntityManager's flush() method.
     * 
     * This helps callers address an issue with transactions that use EJBs
     * packaged separately. Sub methods in the same transaction do not see a flushed 
     * state in the database. Use this method with caution, it may be deprecated 
     * very soon.
     */
    public void flush();
}
