/*
 * Classname			ReportsAgentRObj
 * 
 * Version information : 1.0  
 *
 * Date					05/12/2001
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.reportsAgent;

import javax.ejb.Remote;

import com.velos.eres.web.reports.ReportsDao;

/**
 * Remote interface for REportsAgentBean session EJB
 * 
 * @author sajal
 * @version 1.0, 05/12/2001
 */

@Remote
public interface ReportsAgentRObj {
    public ReportsDao fetchReportsData(int reportNumber, String filterType,
            String year, String month, String dateFrom, String dateTo,
            String id, String accId);
}
