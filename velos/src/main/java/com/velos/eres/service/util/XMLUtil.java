package com.velos.eres.service.util;

import java.io.ByteArrayInputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.xml.sax.InputSource;

import oracle.xml.parser.v2.DOMParser;
import oracle.xml.parser.v2.XMLDocument;

public class XMLUtil {
    
    public static String transform (String xml, String xsl)
    {
        String strHTML = "";
        
        try{
            
        	TransformerFactory tFactory =new  net.sf.saxon.TransformerFactoryImpl();
            DOMParser parser = new DOMParser();
            parser.setPreserveWhitespace(false);
            //parser.setPreserveWhitespace(true);
            parser.parse(new StringReader(xml));
            XMLDocument doc = parser.getDocument();
            doc.normalize();
            //doc.print(System.out);
            
            //INF-19817 Number formatting
            xsl = StringUtil.replace(xsl,"[VELNUMFORMATDEF]", NumberUtil.getXSLNumFormatDef());
            xsl = StringUtil.replace(xsl,"[VELFLOATFORMATDEF]", NumberUtil.getXSLFloatFormatDef());
            xsl = StringUtil.replace(xsl, "class=\"numberfield\"", "");
            xsl = StringUtil.replace(xsl, "format-number(", "");
            xsl = StringUtil.replace(xsl, ",$floatFormat,'floatFormatDef')", "");
            //end here
            //Reader mR=new StringReader(xml); 
            Reader sR=new StringReader(xsl);
            
            //Source xmlSource=new StreamSource(mR);
            Source xslSource = new StreamSource(sR);
            

            // Generate the transformer.
            Transformer transformer = tFactory.newTransformer(xslSource);
            //transformer.transform(xmlSource, new StreamResult(System.out));
            
            //INF-19817 Number formatting
            transformer.setParameter("numFormat",NumberUtil.getXSLNumFormat());
    		transformer.setParameter("floatFormat",NumberUtil.getXSLFloatFormat());
           
            transformer.setOutputProperty(OutputKeys.METHOD, "html");

            StringWriter str = new java.io.StringWriter();
            StreamResult sResult = new StreamResult (str);
            transformer.transform(new StreamSource(new ByteArrayInputStream(xml.getBytes())),sResult);
            strHTML = str.toString(); 
        }catch (Exception ex)
        {
            Rlog.fatal("common","Exception in XMLUtil.transform" + ex.toString());
             ex.printStackTrace();
        }

         return strHTML ;
        
        
    }
    
    
    public static String unEscapeSpecialChars(String html)
    {
        String newStr = new String();
        
        newStr = html;
        
        newStr = StringUtil.replace(newStr , "&amp;" , "&");
        newStr = StringUtil.replace(newStr , "&quot;","\"");
        newStr = StringUtil.replace(newStr , "&apos;","'");
        newStr = StringUtil.replace(newStr , "&lt;" , "<");
        newStr = StringUtil.replace(newStr , "&gt;" , ">");
        newStr = StringUtil.replace(newStr , "\\\\" , "\\");
        newStr = StringUtil.replace(newStr , "&#xa0;" , " ");
        
        //unescape the special characters 
        //VELDQUOTE was replaced in main clob to preserve special html encoding &#34; for double quotes
        // while using FCKeditor, we replace  Double quotes with   &#34; to support those fields on browser and througut the application
        
        newStr = StringUtil.replace(newStr , "VELDQUOTE" , "\"");  
          
        
        return newStr;
        
    }
    
    public static String escapeSpecialChars(String html)
    {
        String newStr = new String();
        
        newStr = html;
        
       newStr = StringUtil.replace(newStr , "&nbsp;" , "&#xa0;");
                        
        
        return newStr;
        
    }
    
}
