/*
 * Classname			MsgcntrAgentRObj.class
 * 
 * Version information   1.0
 *
 * Date					02/27/2001
 * 
 * Copyright notice		Velos Inc.
 */

package com.velos.eres.service.msgcntrAgent;

import java.rmi.RemoteException;

import javax.ejb.Remote;

import com.velos.eres.business.common.MsgCntrDao;
import com.velos.eres.business.msgcntr.MsgsStateKeeper;
import com.velos.eres.business.msgcntr.impl.MsgcntrBean;
import com.velos.eres.service.msgcntrAgent.impl.MsgcntrAgentBean;

/**
 * Remote interface for MsgcntrAgent session EJB
 * 
 * @author sajal
 * @version 1.0 02/27/2001
 * @see MsgcntrAgentBean
 */
@Remote
public interface MsgcntrAgentRObj {
    /**
     * gets the msgcntr details
     */
    MsgcntrBean getMsgcntrDetails(int msgcntrId) throws RemoteException;

    /**
     * sets the msgcntr details
     */
    public int setMsgcntrDetails(MsgcntrBean msgsk);

    public int updateMsgcntr(MsgcntrBean msgsk);

    public MsgCntrDao updateMsgs(MsgsStateKeeper msgsk);

    // public MsgsPendingValidationStateKeeper findByMsgsPendingValidation()
    // throws RemoteException;

    public MsgCntrDao getMsgCntrUserTo(int userTo);

    public MsgCntrDao getMsgCntrUserToRead(int userTo);

    public MsgCntrDao getMsgCntrUserToUnRead(int userTo);

    public MsgCntrDao getMsgCntrStudy(int study);

    //
    public MsgCntrDao getMsgCntrVelosUser(int userTo);

    public MsgCntrDao getMsgCntrVelosUser(int userTo, String status);

    public MsgCntrDao getMsgCntrVelosUserAcc(int userTo);

    public MsgCntrDao getUserMsgs(int userTo);

    public MsgCntrDao getUserMsgsWithSearch(int userTo, String lname,
            String fname, String userStatus);

    public MsgCntrDao getMsgCount(int userTo);

}
