package com.velos.eres.service.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.velos.eres.business.common.LkpFilterDao;
import com.velos.eres.business.common.LkpViewDao;

/**
 * Utility class to register all known SQL filters so that we can reject any unknown filter request
 * in order to prevent SQL injection from getlookup.jsp and getmultilookup.jsp.
 */
public class FilterUtil {
    public static final String fakeFilter = "1=2";
    public static final String emptyString = "";
    public static final String NUMBER = "NUMBER";
    public static final String NUMBER_LIST = "NUMBER_LIST";
    public static final String CHAR2 = "CHAR2";
    public static final String VARCHAR = "VARCHAR";
    public static final String VARCHAR_NO_SPACES = "VARCHAR_NO_SPACES";

    public static final String study_keyword = "study";
    public static final String formQuery_keyword = "formQuery";
    public static final String formQueryPats_keyword = "formQueryPats";
    public static final String linkFormType_keyword = "linkFormType";
    public static final String lookupType_keyword = "lookupType";
    public static final String mdynreptype_keyword = "mdynreptype";
    public static final String portalDesign_keyword = "portalDesign";
    public static final String portalDesign1_keyword = "portalDesign1";
    public static final String protocolCalendar_keyword = "protocolCalendar";
    public static final String updatemultschedules = "updatemultschedules";
    public static final String study_appendix_keyword = "study_appendix";
    public static final String patient_keyword = "patient";
    public static final String portaldetails_keyword = "portaldetails";
    public static final String demographics_keyword = "demographics";
    public static final String patOrgs_keyword = "patOrgs";
    public static final String AE_keyword = "AE";
    public static final String patTxArm_keyword = "patTxArm";
    public static final String patProt_keyword = "patProt";
    public static final String patSchedule_keyword = "patSchedule";
    public static final String patLabs_keyword = "patLabs";
    public static final String allPatLabs_keyword = "allPatLabs";
    public static final String patStudyFormQry_keyword = "patStudyFormQry";
    public static final String studyLookup_keyword = "studyLookup";
    public static final String userProfile_keyword = "userProfile";
    public static final String emailUser_keyword = "emailuser";
    public static final String poPerson_keyword = "PoPerson";
    public static final String poOrganization_keyword = "PoOrganization";
    public static final String patStudyId_keyword = "patStudyId";
    public static final String codeLstData_keyword = "codeLstData";
    public static final String countryCode_keyword = "countryCode";
    public static final String NIHInstitutionCode_keyword = "NIHInstitutionCode";
    public static final String NCIProgramCode_keyword = "NCIProgramCode";
    public static final String adhocQueries_keyword = "adhocQueries";
    public static final String storages_keyword = "keyword_storages";
    public static final String addeventkit_keyword = "addeventkit";
    public static final String orgLookup_keyword = "orgLookup";
    public static final String selProtCal_keyword = "selProtCal";
    public static final String CustLkp_keyword = "CustLkp";

    /**
     * The literalHash contains static strings which can be safely plugged into a SQL.
     * It is loaded at the startup time. Only accept safe strings in this hash.
     */
    private static HashMap literalHash = null;
    
    /**
     * The paramHash contains key, SQL, and an arbitrary number of parameter values.
     * This hash is used for the dfilter values that take on the format "key|value1|value2|...".
     * For example, if the key is "addeventkit", value1 corresponds to parameter ?1 in
     * the string paramHash.get("addeventkit").
     * 
     * It is possible to write a custom method to do the replacement. The custom method 
     * could be in validateDFilter() or another method called by validateDFilter(). 
     * Make sure to sanitize every parameter value according to its data type.
     * 
     * To add a new filter to paramHash, insert a record into ER_LKPFILTER and refresh cache.
     */
    private static HashMap paramHash = null;
    
    /**
     * The viewHash contains viewId, view keyword, filter SQL, ignore filter flag values.
     * This hash is used for the validating dfilter values as well as adding extra clauses 
     * based on user's access right.
     * For example, 
     * 1. If the viewId is 6000 and dfilter is "demographics", system will first validate whether
     * both values are in sync. 
     * 2. If viewId and dfilter are in sync, system will check the view key and logged-in user's access right. 
     * Based on access rights system will chose to add and extra clause.
     * 
     * This view key setting is one time setting done by Velos.
     */
    private static HashMap viewHash = null;
    /**
     * Make the constructor private to disallow instantiation. All methods in this class
     * should be static and called statically.
     */
    private FilterUtil() {}
    
    /**
     * Make a DB call to populate paramHash from ER_LKPFILTER.
     * This method is called at startup and also when a cache refresh is requested.
     */
    public static void fillParamHashFromLkpFilterDao() {
        if (paramHash == null) { paramHash = new HashMap(); } 
        else { paramHash.clear(); }
        LkpFilterDao lkpFilterDao = new LkpFilterDao();
        lkpFilterDao.getLkpFilterData();
        ArrayList<String> lkpFilterKey = lkpFilterDao.getLkpFilterKey();
        ArrayList<String> lkpFilterSQL = lkpFilterDao.getLkpFilterSQL();
        ArrayList<String> lkpFilterParamType = lkpFilterDao.getLkpFilterParamType();
        for (int iX=0; iX<lkpFilterKey.size(); iX++) {
            LkpFilterObject lfObj = new LkpFilterObject(lkpFilterKey.get(iX),
                    lkpFilterSQL.get(iX), lkpFilterParamType.get(iX));
            paramHash.put(lkpFilterKey.get(iX), lfObj);
        }
    }
    
    public static void fillViewHashFromLkpViewDao() {
        if (viewHash == null) { viewHash = new HashMap(); } 
        else { viewHash.clear(); }
        LkpViewDao lkpViewDao = new LkpViewDao();
        lkpViewDao.getLkpViewData();
        ArrayList<Integer> lkpViewPK = lkpViewDao.getPkLkpView();
        ArrayList<String> lkpViewKey = lkpViewDao.getLkpViewKey();
        ArrayList<String> lkpViewName = lkpViewDao.getLkpViewName();
        ArrayList<String> lkpType = lkpViewDao.getLkpType();
        ArrayList<String> lkpViewFilterSQL = lkpViewDao.getLkpViewFilterSQL();
        ArrayList<String> lkpViewIgnoreFilter = lkpViewDao.getLkpViewIgnoreFilter();

        for (int iX=0; iX<lkpViewKey.size(); iX++) {
            LkpViewObject lVObj = new LkpViewObject(lkpViewPK.get(iX), lkpViewKey.get(iX), lkpViewName.get(iX),
            		lkpType.get(iX), lkpViewFilterSQL.get(iX), lkpViewIgnoreFilter.get(iX));
            viewHash.put(lkpViewPK.get(iX), lVObj);
        }
    }

    static {
        // Load literalHash at startup
        literalHash = new HashMap();
        literalHash.put("codelst_type='study_division'", "Y");
        literalHash.put("codelst_type='tarea'", "Y");
        literalHash.put("codelst_type='disease_site'", "Y");
        literalHash.put("codelst_type='patStatus' and codelst_hide='N'", "Y");
        literalHash.put("codelst_type='research_type'", "Y");
        literalHash.put("codelst_type='study_type'", "Y");
        literalHash.put("codelst_type='studystat'", "Y");
        literalHash.put("codelst_type='sponsor'", "Y");
        literalHash.put("codelst_type='country'", "Y");
        literalHash.put("storage_subtyp!='Kit' and STORAGE_ISTEMPLATE!='Yes'", "Y");
        
        // Load paramHash at startup
        fillParamHashFromLkpFilterDao();

        // Load viewHash at startup
        fillViewHashFromLkpViewDao();
    }
    
    /**
     * The main method to be called by getlookup.jsp and getmultilookup.jsp.
     * The incoming HTTP request should only contain minimum key info and parameter values, not SQL.
     * The output of this method should not be passed back and forth between the client and server.
     * It should remain on the server side. If any parameter value is suspicious, return a 1=2 which
     * will result in no match found for the client.
     * 
     * @param request - incoming HTTP request
     * @return - Authorized SQL filter to be passed directly to BrowserRows. 
     */
    public static String validateDFilter(HttpServletRequest request) {
        HttpSession tSession = request.getSession(false);
        if (tSession == null) { return fakeFilter; }

        String viewIdStr = request.getParameter("viewId");
        int viewId =  StringUtil.stringToNum(viewIdStr);
        LkpViewObject lVObj = new LkpViewObject(0,"","","","","");

        if (viewId <= 0){
        	String viewNameStr = request.getParameter("viewName");
        	if (!StringUtil.isEmpty(viewNameStr)){
	        	lVObj.setLkpViewObject(getLkpView("viewName", viewNameStr));
	        	viewId = lVObj.getLkpViewPk();
	        	if (viewId <= 0)
	        		return emptyString;
        	} else {
        		String viewKeywordStr = request.getParameter("viewNameKeyword");
            	lVObj.setLkpViewObject(getLkpView("viewKey", viewKeywordStr));
            	viewId = lVObj.getLkpViewPk();
	        	if (viewId <= 0)
	        		return emptyString;
        	}
        } else {
        	lVObj.setLkpViewObject(getLkpView("viewId", ""+viewId));
        }

        String addlSqlClause = null;
        int isPortalUser = checkPortalUser(request).intValue();
        if (isPortalUser == 1) {
        	addlSqlClause = getAddlClauseForPortalUser(request);
            if (addlSqlClause == null) { return fakeFilter; }
        }

        String dfilter = request.getParameter("dfilter");
        if (dfilter == null || dfilter.length() == 0) {
       		return addlSqlClause;
        }

        if ("Y".equals(literalHash.get(dfilter))) { return dfilter; }
        String[] parts = dfilter.split("\\|");
        String search_data = request.getParameter("search_data");
        if(search_data==null || search_data.length()==0){
        	
        }else{
        	dfilter = parts[0];
        }
        int valid = (validateLkpViewFilterCombination(viewId, parts[0])).intValue();
        if (valid == 0){ return fakeFilter; } // IMP

        String sql = null;
        if ((CustLkp_keyword + lVObj.getLkpViewPk()).equals(parts[0])) {
        	/* IMP: Newly imported custom lookup will have LKPVIEW_KEYWORD values with 
        	 * CustLkp as a prefix followed by PK_LKPVIEW.
        	 * In such case it becomes customer's responsibility to change the LKPVIEW_KEYWORD value 
        	 * to a stricter value so portal users cannot access the sensitive lookup data. 
        	 */
        	return emptyString;
        }
        if (study_keyword.equals(parts[0])) {
        	String accId =  (isPortalUser == 1) ? 
        		StringUtil.trueValue((String)tSession.getAttribute("pp_accountId"))
        		: StringUtil.trueValue((String)tSession.getAttribute("accountId"));
            if (accId == null) { return fakeFilter; }
            sql = " fk_account= " + accId;
        }
        if (formQuery_keyword.equals(parts[0])) {
            String userId = StringUtil.trueValue((String)tSession.getAttribute("userId"));
            if (userId == null) { return fakeFilter; }
            sql = "(pkg_util.f_getStudyRight(pkg_studystat.F_GET_USERRIGHTS_FOR_STUDY("+userId+
            ", pk_study),(select CTRL_SEQ from er_ctrltab where CTRL_KEY = 'study_rights' and upper(ctrl_value) = 'STUDYMPAT')) > 0)";            
        }
        if (linkFormType_keyword.equals(parts[0])) {
            sql = getLinkFormTypeFilter(request, parts);
            sql = sql != null ? sql : fakeFilter;
        }
        LkpFilterObject lfObj = null;
        if (lookupType_keyword.equals(parts[0])) {
            if ((lfObj = (LkpFilterObject)paramHash.get(parts[0])) == null) { return fakeFilter; }
            if ((sql = lfObj.getLkpFilterSQL()) == null) { return fakeFilter; }
            String accId = StringUtil.trueValue((String)tSession.getAttribute("accountId"));
            String userId = StringUtil.trueValue((String)tSession.getAttribute("userId"));
            sql = sql.replaceFirst("[?][1]\\b", accId).replaceFirst("[?][2]\\b", userId);
        }
        if (mdynreptype_keyword.equals(parts[0]) || portalDesign_keyword.equals(parts[0])) {
            sql = getWhereClauseForFormsLookup(request, parts);
            sql = sql != null ? sql : fakeFilter;
        }
        if (poPerson_keyword.equals(parts[0])) {
        	sql = emptyString;
        }
        if (poOrganization_keyword.equals(parts[0])) {
        	sql = emptyString;
        }
        if (orgLookup_keyword.equals(parts[0])) {
        	sql = emptyString;
        }

        if (sql != null) {
        	if (addlSqlClause != null) {
        		sql += " AND " + addlSqlClause;
        	}
        	return sql;
        }
        
        // All the rest of filters configured in ER_LKPFILTER come here 
        if ((lfObj = (LkpFilterObject)paramHash.get(parts[0])) == null) { return fakeFilter; }
        if ((sql = lfObj.getLkpFilterSQL()) == null) { return fakeFilter; }
        if (parts.length == 1) { return sql; }
        String lfParamType = lfObj.getLkpFilterParamType();
        if (lfParamType == null) {
        	//YK 10Mar2011 Bug#5892
         	sql=sql.replaceFirst("[?][1]\\b", parts[1]);  
        	return sql!= null ? sql : fakeFilter; 
        }
        String[] rules = lfParamType.split(",");
        if (rules.length != parts.length-1) {
            Rlog.fatal("getmultilookup", "Numbers of parameters do not match.");
            return fakeFilter;
        }
        for (int iX=0; iX<parts.length-1; iX++) {
            sql = sql.replaceAll("[?]["+(iX+1)+"]\\b", sanitizeParam(parts[iX+1], rules[iX]));
        }
        if (sql != null){
	        if (addlSqlClause != null) {
	    		sql += " AND " + addlSqlClause;
	    	}
        } else{
        	sql = emptyString;
        }
        return sql;
    }
    
    public static Integer validateLkpViewFilterCombination(int viewId, String dfilter) {
    	Integer returnInt = 0; 
        if (viewId <= 0){ return returnInt; }
        if (dfilter == null) { return 1; }
        if (dfilter.length() == 0) { return 1; }

        LkpViewObject lVObj = (LkpViewObject)viewHash.get(viewId);
        String lkpViewKey = lVObj.getLkpViewKey();
        
        if (null == lkpViewKey) {return returnInt;}

        if (mdynreptype_keyword.equals(lkpViewKey) && portalDesign_keyword.equals(dfilter))
        	return 1;
        if (mdynreptype_keyword.equals(lkpViewKey) && linkFormType_keyword.equals(dfilter))
        	return 1;
        if (protocolCalendar_keyword.equals(lkpViewKey) && portalDesign1_keyword.equals(dfilter))
        	return 1;
        if (protocolCalendar_keyword.equals(lkpViewKey) && selProtCal_keyword.equals(dfilter))
        	return 1;
        if (protocolCalendar_keyword.equals(lkpViewKey) && updatemultschedules.equals(dfilter))
        	return 1;
        if (userProfile_keyword.equals(lkpViewKey) && emailUser_keyword.equals(dfilter))
        	return 1;
        if (studyLookup_keyword.equals(lkpViewKey) && formQuery_keyword.equals(dfilter))
        	return 1;
        if (patStudyId_keyword.equals(lkpViewKey) && formQuery_keyword.equals(dfilter))
        	return 1;
        if (patStudyId_keyword.equals(lkpViewKey) && formQueryPats_keyword.equals(dfilter))
        	return 1;
        if (patient_keyword.equals(lkpViewKey) && portaldetails_keyword.equals(dfilter))
        	return 1;
        if (codeLstData_keyword.equals(lkpViewKey) && countryCode_keyword.equals(dfilter))
        	return 1;
        if (codeLstData_keyword.equals(lkpViewKey) && NIHInstitutionCode_keyword.equals(dfilter))
        	return 1;
        if (codeLstData_keyword.equals(lkpViewKey) && NCIProgramCode_keyword.equals(dfilter))
        	return 1;
        if (adhocQueries_keyword.equals(lkpViewKey) && lookupType_keyword.equals(dfilter))
        	return 1;
        if (storages_keyword.equals(lkpViewKey) && addeventkit_keyword.equals(dfilter))
        	return 1;
        if (lkpViewKey.equals(dfilter)) return 1;
        
        return returnInt;
    }

    public static LkpViewObject getLkpView(String key, String value) {
    	LkpViewObject emptyObj = new LkpViewObject(0,"","","","","");
    	LkpViewObject lVObj = emptyObj;
    	if (StringUtil.isEmpty(key) || StringUtil.isEmpty(value)){ return lVObj; }
    	
    	if ("viewId".equals(key)) {
        	try {
        		lVObj = (LkpViewObject)viewHash.get(StringUtil.stringToInteger(value));
        	} catch(Exception e) {}
    		return lVObj;
    	}
        
        Iterator it = viewHash.entrySet().iterator();
        while (it.hasNext()) {
        	Map.Entry pairs = (Map.Entry)it.next();
            Integer mapViewId = (Integer)pairs.getKey();
            lVObj = (LkpViewObject)pairs.getValue();

            if ("viewName".equals(key)){
            	 if (lVObj.getLkpViewName().equals(value)){
                 	return lVObj;
                 }
            }
            if ("viewKey".equals(key)){
            	String lkpKey = lVObj.getLkpViewKey();
           	 	if (lkpKey != null && lkpKey.equals(value)){
                	return lVObj;
                }
           }
        }
        return emptyObj;
    }

    public static Integer checkPortalUser(HttpServletRequest request) {
        HttpSession tSession = request.getSession(false);
        if (tSession == null) { return null; }

        String userId = (String)tSession.getValue("userId");
        if (StringUtil.isEmpty(userId) || "null".equals(userId)){
        	return null; 	
        }
        String accountId = (String)tSession.getValue("accountId");
        if (StringUtil.isEmpty(accountId) || "null".equals(accountId)){
        	//logged-in user is a portal user
        	String ignoreRights = (String) tSession.getValue("pp_ignoreAllRights");
        	if (StringUtil.isEmpty(ignoreRights) || (!"true".equals(ignoreRights))){
        		return null;
        	}
        	accountId = (String) tSession.getValue("pp_accountId");
        	if (StringUtil.isEmpty(accountId) || "null".equals(accountId)){
        		return null;
        	}
        } else {
        	//logged-in user is a eResearch user
        	return 0;
        }
        
        return 1;
    }

    /**
     * The main method to be called by getlookup.jsp and getmultilookup.jsp.
     * The incoming HTTP request should only contain minimum key info and parameter values, not SQL.
     * The output of this method should not be passed back and forth between the client and server.
     * It should remain on the server side. If any parameter value is suspicious, return a Empty String which
     * will result in no match found for the client.
     * 
     * @param request - incoming HTTP request
     * @return - Authorized SQL filter to be passed directly to BrowserRows. 
     * 		Return value will be null if request is erroneous. 
     */
    public static String getAddlClauseForPortalUser(HttpServletRequest request) {
        HttpSession tSession = request.getSession(false);
        if (tSession == null) { return null; }
        String userId = (String)tSession.getValue("userId");
        if (StringUtil.isEmpty(userId) || "null".equals(userId)){
        	return null; 	
        }
        String accountId = (String)tSession.getValue("accountId");
        if (StringUtil.isEmpty(accountId) || "null".equals(accountId)){
        	//logged-in user is a portal user
        	String ignoreRights = (String) tSession.getValue("pp_ignoreAllRights");
        	if (StringUtil.isEmpty(ignoreRights) || (!"true".equals(ignoreRights))){
        		return null;
        	}
        	accountId = (String) tSession.getValue("pp_accountId");
        	if (StringUtil.isEmpty(accountId) || "null".equals(accountId)){
        		return null;
        	}
        } else {
        	//logged-in user is a eResearch user
        	return emptyString;
        }
        
        //Control comes here only if logged-in user is a portal user
        String patientId = (String)tSession.getValue("portalPat");
        if (StringUtil.isEmpty(patientId) || "null".equals(patientId)){
        	return null; 	
        }

        String studyId = (String)tSession.getValue("portalStudy");
        if (StringUtil.isEmpty(studyId) || "null".equals(studyId)){
        	return null; 	
        }
        
        String viewIdStr = request.getParameter("viewId");
        int viewId =  StringUtil.stringToNum(viewIdStr);
        if (viewId <= 0){ return null; }
    	LkpViewObject lVObj = (LkpViewObject)viewHash.get(viewId);

        String dfilter = request.getParameter("dfilter");
        if (dfilter == null || dfilter.length() == 0) { 
            String lkpType = lVObj.getLkpViewType();
       	   	dfilter = lVObj.getLkpViewKey();
       	   	if (null == dfilter) { return emptyString; }
        } else {
        	if ("Y".equals(literalHash.get(dfilter))) { return emptyString; }
        }

        String[] parts = dfilter.split("\\|");
        String addlSqlClause = null;
        
        if (parts[0].equals(CustLkp_keyword + lVObj.getLkpViewPk())) {
        	/* IMP: Newly imported custom lookup will have LKPVIEW_KEYWORD values with 
        	 * CustLkp as a prefix followed by PK_LKPVIEW.
        	 * In such case it becomes customer's responsibility to change the LKPVIEW_KEYWORD value 
        	 * to a stricter value so portal users cannot access the sensitive lookup data. 
        	 */
        	return emptyString;
        }

        if (study_keyword.equals(parts[0])) {
        	addlSqlClause = "pk_study = " + studyId;
        }

        if (study_appendix_keyword.equals(parts[0])) {
        	addlSqlClause = "fk_study = " + studyId;
        }

        if (patient_keyword.equals(parts[0])) {
        	addlSqlClause = "pk_person = " + patientId;
        }

        if (demographics_keyword.equals(parts[0])) {
        	addlSqlClause = "pk_person = " + patientId;
        }

        if (patOrgs_keyword.equals(parts[0])) {
        	addlSqlClause = "FK_PER = " + patientId;
        }
        
        if (AE_keyword.equals(parts[0])) {
        	addlSqlClause = "(FK_PER = " + patientId + " AND PK_STUDY = " + studyId +")";
        }
        
        if (patTxArm_keyword.equals(parts[0])) {
        	addlSqlClause = "(FK_PER = " + patientId + " AND FK_STUDY = " + studyId +")";
        }
        
        if (patProt_keyword.equals(parts[0])) {
        	addlSqlClause = "(FK_PER = " + patientId + " AND FK_STUDY = " + studyId +")";
        }
        
        if (patSchedule_keyword.equals(parts[0])) {
        	addlSqlClause = "(FK_PER = " + patientId + " AND FK_STUDY = " + studyId +")";
        }
        
        if (patLabs_keyword.equals(parts[0])) {
        	addlSqlClause = "(FK_PER = " + patientId + " AND (NVL(FK_STUDY, 0) = 0 OR FK_STUDY = " + studyId +"))";
        }
        
        if (allPatLabs_keyword.equals(parts[0])) {
        	addlSqlClause = "(FK_PER = " + patientId + " AND (NVL(FK_STUDY, 0) = 0 OR FK_STUDY = " + studyId +"))";
        }
        
        if (patStudyFormQry_keyword.equals(parts[0])) {
        	//addlSqlClause = "(FK_PER = " + patientId + " AND FK_STUDY = " + studyId +")";
        }
        
        return addlSqlClause;
    }
    
    public static String sanitizeText(String input) {
        if (input == null) { return null; }
        if (input.length() == 0) { return emptyString; }
        return sanitizeParam(input, VARCHAR);
    }

    /**
     * 
     * @param input - String to sanitize
     * @return - sanitize string or (if suspicious) emptyString
     */
    public static String sanitizeNumberForSQL(String input) {
    	if (input == null) { return null; }
        if (input.length() == 0) { return emptyString; }
    	return sanitizeParam(input, NUMBER);
    }
    
    /**
     * @param input - String to sanitize
     * @return - Returns the String stripped of all special SQL keywords.
     */
    public static String sanitizeTextForSQL(String input) {
    	if (input == null) { return null; }

    	//Select
		String outStr = Pattern.compile("\\bselect\\b", Pattern.CASE_INSENSITIVE).matcher(input).replaceAll(" ");
		//From
		outStr = Pattern.compile("\\bfrom\\b", Pattern.CASE_INSENSITIVE).matcher(outStr).replaceAll(" ");
		//Where
		outStr = Pattern.compile("\\bwhere\\b", Pattern.CASE_INSENSITIVE).matcher(outStr).replaceAll(" ");
		//Union
		outStr = Pattern.compile("\\bunion\\b", Pattern.CASE_INSENSITIVE).matcher(outStr).replaceAll(" ");
		return outStr;
    }
    
    /**
     * The custom method to handle the "linkFormType" request.
     * 
     * @param request - incoming HTTP request
     * @param parts - incoming dfilter value split by pipe (|) into String[]. part[0] is always the request identifier.
     * @return - Authorized SQL filter to be passed directly to BrowserRows. 
     */
    private static String getLinkFormTypeFilter(HttpServletRequest request, String[] parts) {
        String type = sanitizeParam(parts[1], CHAR2);
        String linkedStudy = sanitizeParam(parts[2], NUMBER);
        
        HttpSession tSession = request.getSession(false);
        if (tSession == null) { return null; }
        String accId = StringUtil.trueValue((String)tSession.getAttribute("accountId"));
        String userId = StringUtil.trueValue((String)tSession.getAttribute("userId"));
        
        String typeFilter = "";
        String studyFilter = "";
        if ("SA".equals(type)) {
            typeFilter = "'SA'";
        } else if ("S".equals(type)) {
            typeFilter = "'S','SA'";
            studyFilter = "  and (b.fk_study is null or b.fk_study = "+ linkedStudy +")  ";
        } else if ("A".equals(type)) {
            typeFilter = "'A'";
        } else if ("PA".equals(type)) {
            typeFilter = "'PA'";
        } else if ("SP".equals(type)) {
            typeFilter = "'SP','PS','PR','PA'";
            studyFilter = "  and (b.fk_study is null or b.fk_study = "+ linkedStudy +")  ";
        } else if ("PS".equals(type)) {
            typeFilter = "'PA','PS','PR'";
        } else if ("PR".equals(type)) {
            typeFilter = "'PA','PS','PR'";
        }else if ("USR".equals(type)) {
            typeFilter = "'USR'";
        }else if ("ORG".equals(type)) {
            typeFilter = "'ORG'";
        }else if ("NTW".equals(type)) {
            typeFilter = "'NTW'";
        }
        else if ("SNW".equals(type)) {
            typeFilter = "'SNW'";
        }
        String s1 = " b.fk_account ="+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D'and "+ 
        "(c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp = 'A' and codelst_type='frmstat')) "+
        " and (c.formstat_enddate is null) and  "+
        " b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+ 
        "b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) "+
        " AND (LF_DISPLAYTYPE in  ("+ typeFilter+") " + studyFilter + " ) [VELFILTERSTR] union " +
        "select PK_FORMLIB,FORM_NAME,FORM_DESC,DECODE (trim(LF_DISPLAYTYPE) ,'PA','All Patients','PS','Patient (All Studies)','PR','Patient (All Studies - Restricted)','A','Account','S','Study','SA','All Studies','SP','Patient (Specific Study)','-') " +
        ",(SELECT codelst_desc  FROM ER_CODELST WHERE pk_codelst=c.fk_codelst_stat) AS form_status ,lf_displaytype "+ 
        " from er_formlib a,er_linkedforms b,er_formstat c " +
        "where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D'and "+ 
        " (c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp = 'A' and codelst_type='frmstat')) "+
        " and (c.formstat_enddate is null) and  "+
        " (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
        " or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) "+
        " AND ( LF_DISPLAYTYPE in  ("+ typeFilter+")  " + studyFilter + " ) " ;
        return s1;
    }
    
    /**
     * The custom method to handle the "mdynreptype" request. The parts array must be 6 in length. 
     * 
     * @param request - incoming HTTP request
     * @param parts - incoming dfilter value split by pipe (|) into String[]. part[0] is always the request identifier.
     * @return - Authorized SQL filter to be passed directly to BrowserRows. 
     */
    private static String getWhereClauseForFormsLookup(HttpServletRequest request, String[] parts) {
        if (parts == null || parts.length < 6) { return fakeFilter; }
        HttpSession tSession = request.getSession(false);
        if (tSession == null) { return fakeFilter; }
        String accId = StringUtil.trueValue((String)tSession.getAttribute("accountId"));
        String userId = StringUtil.trueValue((String)tSession.getAttribute("userId"));
        String uniqueFilterString = getUniqueFilterForFormsLookup(parts);
        String formtype = sanitizeParam(parts[1], CHAR2);
        String study = sanitizeParam(parts[4], NUMBER);
        String pat = sanitizeParam(parts[5], NUMBER);
        String whereClause = null;
        if ("A".equals(formtype)) {
           whereClause = " b.fk_account = "+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D' and "+ 
               "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
           "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
           " and (c.formstat_enddate is null) and  "+
           "b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+ 
           "b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) "+
           " AND ((LF_DISPLAYTYPE = 'A' )) " + uniqueFilterString +  "[VELFILTERSTR]  union " +
           "select PK_FORMLIB,FORM_NAME,FORM_DESC,DECODE (trim(LF_DISPLAYTYPE) ,'PA','All Patients','PS','Patient (All Studies)','PR','Patient (All Studies - Restricted)','A','Account','S','Study','SA','All Studies','SP','Patient (Specific Study)','-') " +
           ",(SELECT codelst_desc  FROM ER_CODELST WHERE pk_codelst=c.fk_codelst_stat) AS form_status,lf_displaytype "+
           " from er_formlib a,er_linkedforms b,er_formstat c " +
           " where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D'and "+ 
           " ((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
           "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
           " and (c.formstat_enddate is null) and  "+
           " (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
           " or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) "+
           " AND ((LF_DISPLAYTYPE = 'A' )) "  + uniqueFilterString   ;
        } else if ("S".equals(formtype)) {
            if ( Integer.parseInt(study) == 0) {
                whereClause = " b.fk_account ="+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D' and "+ 
                "(fk_study is null)  and "+
                "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
                "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
                " and (c.formstat_enddate is null) and  "+
                "b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+ 
                "b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) "+
                " AND ((LF_DISPLAYTYPE = 'SA' )) " + uniqueFilterString +  " [VELFILTERSTR]  union  " +
                "select PK_FORMLIB,FORM_NAME,FORM_DESC,DECODE (trim(LF_DISPLAYTYPE) ,'PA','All Patients','PS','Patient (All Studies)','PR','Patient (All Studies - Restricted)','A','Account','S','Study','SA','All Studies','SP','Patient (Specific Study)','-') " +
                ",(SELECT codelst_desc  FROM ER_CODELST WHERE pk_codelst=c.fk_codelst_stat) AS form_status,lf_displaytype"+
                " from er_formlib a,er_linkedforms b,er_formstat c " +
                "where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D'and "+ 
                "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
                "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
                " and (c.formstat_enddate is null) and  "+
                " (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
                " or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) "+
                " AND ((LF_DISPLAYTYPE = 'SA' )) "   + uniqueFilterString ;
            } else {
                whereClause = " b.fk_account ="+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D'and "+ 
                "((fk_study="+ study+ " and LF_DISPLAYTYPE = 'S') or (LF_DISPLAYTYPE = 'SA' ) ) and "+
                "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
                "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
                " and (c.formstat_enddate is null) and  "+
                "b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+ 
                "b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) "+
                "  " + uniqueFilterString +  " [VELFILTERSTR]  union  "+
                "select PK_FORMLIB,FORM_NAME,FORM_DESC,DECODE (trim(LF_DISPLAYTYPE) ,'PA','All Patients','PS','Patient (All Studies)','PR','Patient (All Studies - Restricted)','A','Account','S','Study','SA','All Studies','SP','Patient (Specific Study)','-') " +
                ",(SELECT codelst_desc  FROM ER_CODELST WHERE pk_codelst=c.fk_codelst_stat) AS form_status,lf_displaytype "+
                " from er_formlib a,er_linkedforms b,er_formstat c " +
                "where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D'and "+
                "((fk_study="+ study + " and LF_DISPLAYTYPE = 'S') or (LF_DISPLAYTYPE = 'SA' ) ) and "+ 
                "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
                "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
                " and (c.formstat_enddate is null) and  "+
                " (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
                " or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) "+
                " AND ((LF_DISPLAYTYPE = 'S' ) or (LF_DISPLAYTYPE = 'SA' )) " + uniqueFilterString   ;
            }
        } else if ("P".equals(formtype)) {
            if (Integer.parseInt(pat) == 0) {
                if (Integer.parseInt(study) == 0) {
                    whereClause =  " b.fk_account ="+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D'and "+ 
                    " fk_study is null  and "+
                    "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
                    "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
                    " and (c.formstat_enddate is null) and  "+
                    " b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+ 
                    " b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) "+
                    " AND ((LF_DISPLAYTYPE = 'PA' ) or (LF_DISPLAYTYPE = 'PS' ) or (LF_DISPLAYTYPE = 'PR')) " + uniqueFilterString +  " [VELFILTERSTR]  union  " +
                    "select PK_FORMLIB,FORM_NAME,FORM_DESC,DECODE (trim(LF_DISPLAYTYPE) ,'PA','All Patients','PS','Patient (All Studies)','PR','Patient (All Studies - Restricted)','A','Account','S','Study','SA','All Studies','SP','Patient (Specific Study)','-') " +
                    ",(SELECT codelst_desc  FROM ER_CODELST WHERE pk_codelst=c.fk_codelst_stat) AS form_status,lf_displaytype "+
                    " from er_formlib a,er_linkedforms b,er_formstat c " +
                    "where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D'and "+ 
                    "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
                    "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
                    " and (c.formstat_enddate is null) and  "+
                    " (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
                    " or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) "+
                    " AND ((LF_DISPLAYTYPE = 'PA' ) or (LF_DISPLAYTYPE = 'PS' ) or (LF_DISPLAYTYPE = 'PR' )) " + uniqueFilterString ; 
                    //******************************endhere**************************************************//  
                } else {
                    whereClause =  " b.fk_account ="+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D'and "+ 
                    "((fk_study="+ study + " and LF_DISPLAYTYPE = 'SP' ) or (LF_DISPLAYTYPE = 'PS' ) or (LF_DISPLAYTYPE = 'PR' ) or (LF_DISPLAYTYPE = 'PA' ) ) and "+
                    "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
                    "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
                    " and (c.formstat_enddate is null) and  "+
                    " b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+ 
                    " b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) " +
                    "  " + uniqueFilterString +  " [VELFILTERSTR]  union  "+
                    "select PK_FORMLIB,FORM_NAME,FORM_DESC,DECODE (trim(LF_DISPLAYTYPE) ,'PA','All Patients','PS','Patient (All Studies)','PR','Patient (All Studies - Restricted)','A','Account','S','Study','SA','All Studies','SP','Patient (Specific Study)','-') " +
                    ",(SELECT codelst_desc  FROM ER_CODELST WHERE pk_codelst=c.fk_codelst_stat) AS form_status,lf_displaytype "+
                    " from er_formlib a,er_linkedforms b,er_formstat c " +
                    "where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D'and "+
                    "((fk_study="+ study + " and LF_DISPLAYTYPE = 'SP' ) or (LF_DISPLAYTYPE = 'PS' ) or (LF_DISPLAYTYPE = 'PR') or (LF_DISPLAYTYPE = 'PA' ) ) and "+ 
                    "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
                    "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
                    " and (c.formstat_enddate is null) and  "+
                    " (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
                    " or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) " + uniqueFilterString  ; 
                    //******************************endhere**************************************************//  
                }
            } else {
                whereClause =  " b.fk_account ="+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D'and "+ 
                "((fk_study is null) or (fk_study in (select fk_study from er_patprot where fk_per="+pat+" and patprot_stat=1))) and "+
                "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
                "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
                " and (c.formstat_enddate is null) and  "+
                "b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+ 
                "b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) "+
                " AND ((LF_DISPLAYTYPE = 'PA' ) or (LF_DISPLAYTYPE = 'PS') or (LF_DISPLAYTYPE = 'PR' ) or (LF_DISPLAYTYPE = 'SP') ) " +
                " " + uniqueFilterString +  " [VELFILTERSTR]  union  " +
                "select PK_FORMLIB,FORM_NAME,FORM_DESC,DECODE (trim(LF_DISPLAYTYPE) ,'PA','All Patients','PS','Patient (All Studies)','PR','Patient (All Studies - Restricted)','A','Account','S','Study','SA','All Studies','SP','Patient (Specific Study)','-') " +
                ",(SELECT codelst_desc  FROM ER_CODELST WHERE pk_codelst=c.fk_codelst_stat) AS form_status ,lf_displaytype"+
                " from er_formlib a,er_linkedforms b,er_formstat c " +
                "where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D' and "+
                "((fk_study is null) or (fk_study in (select fk_study from er_patprot where fk_per="+pat+" and patprot_stat=1))) and "+ 
                "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
                "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
                " and (c.formstat_enddate is null) and  "+
                " (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
                " or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) "+
                " AND ((LF_DISPLAYTYPE = 'PA' ) or (LF_DISPLAYTYPE = 'PS') or (LF_DISPLAYTYPE = 'PR') or (LF_DISPLAYTYPE = 'SP')) " + uniqueFilterString  ; 
                //******************************endhere**************************************************//
            }
        }
        return whereClause;
    }
    
    /**
     * Private method used only within getWhereClauseForFormsLookup().
     * 
     * @param parts - incoming dfilter value split by pipe (|) into String[]. part[0] is always the request identifier.
     * @return - string that handles form field filter
     */
    private static String getUniqueFilterForFormsLookup(String[] parts) {
        String uniqueSearchType = sanitizeParam(parts[2], CHAR2);
        String searchUniqueId = sanitizeParam(parts[3], VARCHAR);
        String uniqueFilterString = null;
        if (searchUniqueId != null && searchUniqueId.length() > 0) {
            uniqueFilterString = " and exists (select * from erv_formflds v where v.pk_formlib = b.fk_formlib and lower(trim(v.fld_uniqueid))  " ;
            if ("E".equals(uniqueSearchType)) {
                uniqueFilterString += " = lower(trim('" + searchUniqueId + "') ))" ;
            }
            else {
                uniqueFilterString += " like lower(trim('%" + searchUniqueId + "%') ))" ;
            }
        } else {
            uniqueFilterString = "";
        }
        return uniqueFilterString;
    }
    
    /**
     * 
     * @param str - String to sanitize
     * @param type - one of predefined data type: NUMBER, NUMBER_LIST, etc.
     * @return - sanitize string or (if suspicious) emptyString
     */
    private static String sanitizeParam(String str, String type) {
        if (str == null || type == null) { return emptyString; }
        type = type.trim().toUpperCase();
        // If NUMBER, reject the string if there is any char which is not a number, period or space.
        if (NUMBER.equals(type)) {
            String tmp = str.replaceAll("[^0-9. ]", "@");
            if (tmp.indexOf("@") > -1) { return emptyString; }
            return str;
        }
        // If NUMBER_LIST, reject the string if there is any char which is not a number, comma, period or space.
        if (NUMBER_LIST.equals(type)) {
            String tmp = str.replaceAll("[^0-9,. ]", "@");
            if (tmp.indexOf("@") > -1) { return emptyString; }
            return str;
        }
        // If CHAR2, reject the string if it contains a whitespace or if it does not contain
        // upto three alphanumeric characters.
        if (CHAR2.equals(type)) {
            if (str.matches(".*\\s+.*") || str.length() > 3) { return emptyString; }
            if (!str.matches("[A-Za-z0-9]{1,3}")) { return emptyString; }
            return str;
        }
        // If VARCHAR_NO_SPACES, reject the string if it contains a whitespace. Also, make a single quote two
        // consecutive single quotes and wipe out some special characters.
        if (VARCHAR_NO_SPACES.equals(type)) {
            if (str.matches(".*\\s+.*")) { return emptyString; }
            return str.replaceAll("'", "''").replaceAll("[*()%]", "");
        }
        // If VARCHAR_NO_SPACES, reject the string if it contains a whitespace. Also, make a single quote two
        // consecutive single quotes and wipe out some special characters.
        if (VARCHAR.equals(type)) {
            return str.replaceAll("'", "''").replaceAll("[*()%]", "");
        }
        return emptyString;
    }

}
