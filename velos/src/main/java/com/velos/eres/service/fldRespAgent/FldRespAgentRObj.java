/*
 * Classname			FldRespAgentRObj
 *
 * Version information : 1.0
 *
 * Date					07/02/2003
 *
 * Copyright notice :   Velos Inc
 */

package com.velos.eres.service.fldRespAgent;

import java.util.Hashtable;
import javax.ejb.Remote;

import com.velos.eres.business.common.FldRespDao;
import com.velos.eres.business.fldResp.impl.FldRespBean;

/**
 * Remote interface for FldRespAgentBean session EJB
 *
 * @author Anu Khanna
 * @version 1.0, 02/20/2001
 */
@Remote
public interface FldRespAgentRObj {
    public FldRespBean getFldRespDetails(int fldRespId);

    public int setFldRespDetails(FldRespBean fldresp);

    public int updateFldResp(FldRespBean frsk);

    public FldRespDao getResponsesForField(int field);

    public int insertMultipleResponses(FldRespDao fldRespDao);

    public int updateMultipleResponses(FldRespDao fldRespDao);

    public FldRespDao getMaxSeqNumForMulChoiceFld(int formfldId);

    public FldRespDao getMaxRespSeqForFldInLib(int fldId);

  //JM: 24Jun2008, added for #3309
    public int deleteFldResp(int fldRespId);
    
    // deleteFldResp() Overloaded for INF-18183 ::: Raviesh
    public int deleteFldResp(int fldRespId,Hashtable<String, String> args);


}
