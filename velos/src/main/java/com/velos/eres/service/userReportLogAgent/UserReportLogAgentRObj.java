package com.velos.eres.service.userReportLogAgent;

import javax.ejb.Remote;

import com.velos.eres.business.userReportLog.impl.UserReportLogBean;

/**
 * 
 * @author Yogendra Pratap Singh
 *
 */

@Remote
public interface UserReportLogAgentRObj {

	public int setUserReportLogDetails(UserReportLogBean userReportLogBean);

}
