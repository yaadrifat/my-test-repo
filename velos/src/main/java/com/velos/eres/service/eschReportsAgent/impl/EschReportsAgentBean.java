/*
 * Classname			EschReportsAgentBean
 * 
 * Version information : 1.0
 *
 * Date					07/16/2001
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.eschReportsAgent.impl;

/* IMPORT STATEMENTS */
import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import com.velos.eres.business.common.EschCostReportDao;
import com.velos.eres.business.common.EschReportsDao;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the EschReportsDao.
 * 
 * @author dinesh
 * @version 1.0, 07/16/2001
 * @ejbHome EschReportsAgentHome
 * @ejbRemote EschReportsAgentRObj
 */

public class EschReportsAgentBean implements SessionBean {

    private SessionContext ctx;

    public void setSessionContext(SessionContext context)
            throws RemoteException, EJBException {
        Rlog.debug("eschreports", "EschReportsAgentBean.setSessionContext");
        ctx = context;
    }

    public void ejbActivate() throws RemoteException, EJBException {
    }

    public void ejbPassivate() throws RemoteException, EJBException {
    }

    public void ejbRemove() throws RemoteException, EJBException {
    }

    public void ejbCreate() throws CreateException, EJBException,
            RemoteException {
    }

    public EschReportsDao fetchEschReportsData(int reportNumber,
            String filterType, String year, String month, String dateFrom,
            String dateTo, String id, String accId) {
        try {
            Rlog.debug("eschreports",
                    "EschReportsAgentBean.fetchEschReportsData starting");
            EschReportsDao eschreportsDao = new EschReportsDao();
            Rlog.debug("eschreports",
                    "EschReportsAgentBean.fetchEschReportsData middle");
            eschreportsDao.fetchEschReportsData(reportNumber, filterType, year,
                    month, dateFrom, dateTo, id, accId);
            Rlog.debug("eschreports",
                    "EschReportsAgentBean.fetchEschReportsData end");
            return eschreportsDao;
        } catch (Exception e) {
            Rlog.fatal("eschreports",
                    "Error in fetchEschReportsData in EschReportsJB " + e);
        }
        return null;
    }

    public EschCostReportDao getCostReport(int reportNumber, int protocol_id,
            int orderBy) {
        try {
            Rlog.debug("eschreports",
                    "EschReportsAgentBean.fetchEschReportsData starting");
            EschCostReportDao eschcostreportDao = new EschCostReportDao();
            Rlog.debug("eschreports",
                    "EschReportsAgentBean.fetchEschReportsData middle");
            eschcostreportDao.getCostReport(reportNumber, protocol_id, orderBy);
            Rlog.debug("eschreports",
                    "EschReportsAgentBean.fetchEschReportsData end");
            return eschcostreportDao;
        } catch (Exception e) {
            Rlog.fatal("eschreports",
                    "Error in fetchEschReportsData in EschReportsJB " + e);
        }
        return null;
    }

}// end of class

