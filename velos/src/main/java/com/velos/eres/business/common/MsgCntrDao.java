//Dinesh dt 03/28/2001
package com.velos.eres.business.common;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import oracle.jdbc.OracleTypes;
import com.velos.eres.service.util.Rlog;
public class MsgCntrDao extends CommonDAO implements java.io.Serializable
{
    private ArrayList msgcntrIds;
    private ArrayList msgcntrStudyIds;
    private ArrayList msgcntrFromUserIds;
    private ArrayList msgcntrToUserIds;
    private ArrayList msgcntrTexts;
    private ArrayList msgcntrReqTypes;
    private ArrayList msgcntrStatuses;
    private ArrayList userTos;
    private ArrayList userFroms;
    private ArrayList createOns;
    private ArrayList studyNames;
    private ArrayList studyVerNos;
    private ArrayList permissions;
    private ArrayList userStatus;
    private ArrayList userAccIds;
    private String msgRead;
    private String msgUnRead;
    private String msgAcc;
    private ArrayList msgStudyView;
    private ArrayList accType;
    private ArrayList userCode;
    private ArrayList userSite;
    public MsgCntrDao() {
        msgcntrIds = new ArrayList();
        msgcntrStudyIds = new ArrayList();
        msgcntrFromUserIds = new ArrayList();
        msgcntrToUserIds = new ArrayList();
        msgcntrTexts = new ArrayList();
        msgcntrReqTypes = new ArrayList();
        msgcntrStatuses = new ArrayList();
        userTos = new ArrayList();
        userFroms = new ArrayList();
        userStatus = new ArrayList();
        userAccIds = new ArrayList();
        createOns = new ArrayList();
        studyNames = new ArrayList();
        studyVerNos = new ArrayList();
        permissions = new ArrayList();
        msgRead = new String();
        msgUnRead = new String();
        msgAcc = new String();
        msgStudyView = new ArrayList();
        accType = new ArrayList();
        userCode = new ArrayList();
        userSite = new ArrayList();
    }
    // Getter and Setter methods
    public ArrayList getMsgcntrIds() {
        return this.msgcntrIds;
    }
    public void setMsgcntrIds(ArrayList msgcntrIds) {
        this.msgcntrIds = msgcntrIds;
    }
    public ArrayList getMsgcntrStudyIds() {
        return this.msgcntrStudyIds;
    }
    public void setMsgcntrStudyIds(ArrayList msgcntrStudyIds) {
        this.msgcntrStudyIds = msgcntrStudyIds;
    }
    public ArrayList getMsgcntrFromUserIds() {
        return this.msgcntrFromUserIds;
    }
    public void setMsgcntrFromUserIds(ArrayList msgcntrFromUserIds) {
        this.msgcntrFromUserIds = msgcntrFromUserIds;
    }
    public ArrayList getMsgcntrToUserIds() {
        return this.msgcntrToUserIds;
    }
    public void setMsgcntrToUserIds(ArrayList msgcntrToUserIds) {
        this.msgcntrToUserIds = msgcntrToUserIds;
    }
    public ArrayList getMsgcntrTexts() {
        return this.msgcntrTexts;
    }
    public void setMsgcntrTexts(ArrayList msgcntrTexts) {
        this.msgcntrTexts = msgcntrTexts;
    }
    public ArrayList getMsgcntrReqTypes() {
        return this.msgcntrReqTypes;
    }
    public void setMsgcntrReqTypes(ArrayList msgcntrReqTypes) {
        this.msgcntrReqTypes = msgcntrReqTypes;
    }
    public ArrayList getMsgcntrStatuses() {
        return this.msgcntrStatuses;
    }
    public void setMsgcntrStatuses(ArrayList msgcntrStatuses) {
        this.msgcntrStatuses = msgcntrStatuses;
    }
    public ArrayList getUserFroms() {
        return this.userFroms;
    }
    public void setUserFroms(ArrayList userFroms) {
        this.userFroms = userFroms;
    }
    public ArrayList getUserStatus() {
        return this.userStatus;
    }
    public void setUserStatus(ArrayList userStatus) {
        this.userStatus = userStatus;
    }
    public ArrayList getUserAccIds() {
        return this.userAccIds;
    }
    public void setUserAccIds(ArrayList userAccIds) {
        this.userAccIds = userAccIds;
    }
    public ArrayList getUserTos() {
        return this.userTos;
    }
    public void setUserTos(ArrayList userTos) {
        this.userTos = userTos;
    }
    public ArrayList getCreateOns() {
        return this.createOns;
    }
    public void setCreateOns(ArrayList createOns) {
        this.createOns = createOns;
    }
    public ArrayList getStudyNames() {
        return this.studyNames;
    }
    public void setStudyNames(ArrayList studyNames) {
        this.studyNames = studyNames;
    }
    public ArrayList getStudyVerNos() {
        return this.studyVerNos;
    }
    public void setStudyVerNos(ArrayList studyVerNos) {
        this.studyVerNos = studyVerNos;
    }
    public ArrayList getPermissions() {
        return this.permissions;
    }
    public void setPermissions(ArrayList permissions) {
        this.permissions = permissions;
    }
    // /overloaded setter methods
    public void setMsgcntrIds(Integer msgcntrId)
    {
        msgcntrIds.add(msgcntrId);
    }
    public void setMsgcntrStudyIds(String msgcntrStudyId)
    {
        msgcntrStudyIds.add(msgcntrStudyId);
    }
    public void setMsgcntrFromUserIds(String msgcntrFromUserId)
    {
        msgcntrFromUserIds.add(msgcntrFromUserId);
    }
    public void setMsgcntrToUserIds(String msgcntrToUserId)
    {
        msgcntrToUserIds.add(msgcntrToUserId);
    }
    public void setMsgcntrTexts(String msgcntrText)
    {
        msgcntrTexts.add(msgcntrText);
    }
    public void setMsgcntrReqTypes(String msgcntrReqType)
    {
        msgcntrReqTypes.add(msgcntrReqType);
    }
    public void setMsgcntrStatuses(String msgcntrStatus)
    {
        msgcntrStatuses.add(msgcntrStatus);
    }
    public void setCreateOns(String createOn) {
        createOns.add(createOn);
    }
    public void setUserFroms(String userFrom) {
        userFroms.add(userFrom);
    }
    public void setUserTos(String userTo) {
        userTos.add(userTo);
    }
    public void setUserStatus(String userStat) {
        userStatus.add(userStat);
    }
    public void setUserAccIds(String userAccId) {
        userAccIds.add(userAccId);
    }
    public void setStudyNames(String studyName) {
        studyNames.add(studyName);
    }
    public void setStudyVerNos(String studyVerNo) {
        studyVerNos.add(studyVerNo);
    }
    public void setPermissions(String permission) {
        permissions.add(permission);
    }
    // methods implemented to get read,unread and ackno of the user
    public String getMsgRead() {
        return this.msgRead;
    }
    public void setMsgRead(String msgRead) {
        this.msgRead = msgRead;
    }
    public String getMsgUnRead() {
        return this.msgUnRead;
    }
    public void setMsgUnRead(String msgUnRead) {
        this.msgUnRead = msgUnRead;
    }
    public String getMsgAcc() {
        return this.msgAcc;
    }
    public void setMsgAcc(String msgAcc) {
        this.msgAcc = msgAcc;
    }
    public ArrayList getMsgStudyView() {
        return this.msgStudyView;
    }
    public void setMsgStudyView(ArrayList msgStudyView) {
        this.msgStudyView = msgStudyView;
    }
    public void setMsgStudyView(String msgStudyView)
    {
        this.msgStudyView.add(msgStudyView);
    }
    public ArrayList getAccType() {
        return this.accType;
    }
    public void setAccType(ArrayList accType) {
        this.accType = accType;
    }
    public void setAccType(String accType)
    {
        this.accType.add(accType);
    }
    public ArrayList getUserCode() {
        return this.userCode;
    }
    public void setUserCode(ArrayList userCode) {
        this.userCode = userCode;
    }
    public void setUserCode(String userCode)
    {
        this.userCode.add(userCode);
    }
    public ArrayList getUserSite() {
        return this.userSite;
    }
    public void setUserSite(ArrayList userSite) {
        this.userSite = userSite;
    }
    public void setUserSite(String userSite)
    {
        this.userSite.add(userSite);
    }
    // end of getter and setter methods
    public void getMsgCntrUserTo(int userTo) {
        Rlog.debug("msgcntr", "in DAO:getMsgCntrUserTo");
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            Rlog.debug("msgcntr", "got connection" + conn);
            Rlog.debug("msgcntr", "USER TO" + "*" + userTo + "*");
            String mysql = "select PK_MSGCNTR,FK_STUDY, FK_USERX, FK_USERTO,MSGCNTR_TEXT, MSGCNTR_REQTYPE, MSGCNTR_STAT from ER_MSGCNTR where FK_USERTO = ?";
            Rlog.debug("msgcntr", mysql);
            pstmt = conn.prepareStatement(mysql);
            Rlog.debug("msgcntr", "pstmt " + pstmt);
            pstmt.setInt(1, userTo);
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("msgcntr", "got resultset" + rs);
            while (rs.next()) {
                setMsgcntrIds(new Integer(rs.getInt("PK_MSGCNTR")));
                Rlog.debug("msgcntr", "got pkmsg center");
                Integer studyId = new Integer(rs.getInt("FK_STUDY"));
                Rlog.debug("msgcntr", "got study");
                setMsgcntrStudyIds(studyId.toString());
                Integer userId = new Integer(rs.getInt("FK_USERX"));
                setMsgcntrFromUserIds(userId.toString());
                Rlog.debug("msgcntr", "got userX");
                setMsgcntrToUserIds(rs.getString("FK_USERTO"));
                Rlog.debug("msgcntr", "got user to");
                setMsgcntrTexts(rs.getString("MSGCNTR_TEXT"));
                Rlog.debug("msgcntr", "got msgcntr txt");
                setMsgcntrReqTypes(rs.getString("MSGCNTR_REQTYPE"));
                Rlog.debug("msgcntr", "got req type");
                setMsgcntrStatuses(rs.getString("MSGCNTR_STAT"));
                Rlog.debug("msgcntr", "got stat");
                Rlog.debug("msgcntr",
                        "msgcntrDao.getmsgcntrValuesByAccountId rows ");
            }
        } catch (SQLException ex) {
            Rlog.fatal("msgcntr",
                    "msgcntrDao.getMsgcntr EXCEPTION IN FETCHING FROM msgcntr table"
                            + ex);
        }
        finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
    public void getMsgCntrUserToRead(int userTo) {
        Rlog.debug("msgcntr", "in DAO:getMsgCntrUserTo");
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            Rlog.debug("msgcntr", "got connection" + conn);
            Rlog.debug("msgcntr", "USER TO" + "*" + userTo + "*");
            String mysql = "select PK_MSGCNTR,FK_STUDY, FK_USERX, FK_USERTO,MSGCNTR_TEXT, MSGCNTR_REQTYPE, MSGCNTR_STAT from ER_MSGCNTR where FK_USERTO = ? and MSGCNTR_STAT in('y','Y')";
            Rlog.debug("msgcntr", mysql);
            pstmt = conn.prepareStatement(mysql);
            Rlog.debug("msgcntr", "pstmt " + pstmt);
            pstmt.setInt(1, userTo);
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("msgcntr", "got resultset" + rs);
            while (rs.next()) {
                setMsgcntrIds(new Integer(rs.getInt("PK_MSGCNTR")));
                Rlog.debug("msgcntr", "got pkmsg center");
                Integer studyId = new Integer(rs.getInt("FK_STUDY"));
                Rlog.debug("msgcntr", "got study");
                setMsgcntrStudyIds(studyId.toString());
                Integer userId = new Integer(rs.getInt("FK_USERX"));
                setMsgcntrFromUserIds(userId.toString());
                Rlog.debug("msgcntr", "got userX");
                setMsgcntrToUserIds(rs.getString("FK_USERTO"));
                Rlog.debug("msgcntr", "got user to");
                setMsgcntrTexts(rs.getString("MSGCNTR_TEXT"));
                Rlog.debug("msgcntr", "got msgcntr txt");
                setMsgcntrReqTypes(rs.getString("MSGCNTR_REQTYPE"));
                Rlog.debug("msgcntr", "got req type");
                setMsgcntrStatuses(rs.getString("MSGCNTR_STAT"));
                Rlog.debug("msgcntr", "got stat");
                Rlog.debug("msgcntr",
                        "msgcntrDao.getmsgcntrValuesByAccountId rows ");
            }
        } catch (SQLException ex) {
            Rlog.fatal("msgcntr",
                    "msgcntrDao.getMsgcntr EXCEPTION IN FETCHING FROM msgcntr table"
                            + ex);
        }
        finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
    public void getMsgCntrUserToUnRead(int userTo) {
        Rlog.debug("msgcntr", "in DAO:getMsgCntrUserTo");
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            Rlog.debug("msgcntr", "got connection" + conn);
            Rlog.debug("msgcntr", "USER TO" + "*" + userTo + "*");
            String mysql = "select PK_MSGCNTR,FK_STUDY, FK_USERX, FK_USERTO,MSGCNTR_TEXT, MSGCNTR_REQTYPE, MSGCNTR_STAT from ER_MSGCNTR where FK_USERTO = ? and MSGCNTR_STAT in('n','N')";
            Rlog.debug("msgcntr", mysql);
            pstmt = conn.prepareStatement(mysql);
            Rlog.debug("msgcntr", "pstmt " + pstmt);
            pstmt.setInt(1, userTo);
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("msgcntr", "got resultset" + rs);
            while (rs.next()) {
                setMsgcntrIds(new Integer(rs.getInt("PK_MSGCNTR")));
                Rlog.debug("msgcntr", "got pkmsg center");
                Integer studyId = new Integer(rs.getInt("FK_STUDY"));
                Rlog.debug("msgcntr", "got study");
                setMsgcntrStudyIds(studyId.toString());
                Integer userId = new Integer(rs.getInt("FK_USERX"));
                setMsgcntrFromUserIds(userId.toString());
                Rlog.debug("msgcntr", "got userX");
                setMsgcntrToUserIds(rs.getString("FK_USERTO"));
                Rlog.debug("msgcntr", "got user to");
                setMsgcntrTexts(rs.getString("MSGCNTR_TEXT"));
                Rlog.debug("msgcntr", "got msgcntr txt");
                setMsgcntrReqTypes(rs.getString("MSGCNTR_REQTYPE"));
                Rlog.debug("msgcntr", "got req type");
                setMsgcntrStatuses(rs.getString("MSGCNTR_STAT"));
                Rlog.debug("msgcntr", "got stat");
                Rlog.debug("msgcntr",
                        "msgcntrDao.getmsgcntrValuesByAccountId rows ");
            }
        } catch (SQLException ex) {
            Rlog.fatal("msgcntr",
                    "msgcntrDao.getMsgcntr EXCEPTION IN FETCHING FROM msgcntr table"
                            + ex);
        }
        finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
    public void getMsgCntrVelosUser(int userTo) {
        Rlog.debug("msgcntr", "in DAO:getMsgVelosUser");
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            Rlog.debug("msgcntr", "USER TO" + "*" + userTo + "*");
            String mysql = "SELECT "
            + "PK_MSGCNTR,"
            + "FK_STUDY,"
            + "FK_USERX,"
            + "FK_USERTO,"
            + "MSGCNTR_TEXT,"
            + "MSGCNTR_REQTYPE,"
            + "MSGCNTR_STAT,"
            + "STUDY_NUMBER,"
            + "STUDY_VER_NO, "
            + "A.USR_LASTNAME|| ',' ||  A.USR_FIRSTNAME USERFROM, "
            + "A.USR_STAT user_status "
            + "FROM  ER_USER A,ER_MSGCNTR B,ER_STUDY C "
            + "WHERE  B.FK_USERTO = ? "
            + "AND  B.FK_STUDY=C.PK_STUDY AND  A.PK_USER=B.FK_USERX "
                    + "and A.USR_STAT != 'X' ";
            Rlog.debug("msgcntr", mysql);
            pstmt = conn.prepareStatement(mysql);
            Rlog.debug("msgcntr", "pstmt " + pstmt);
            pstmt.setInt(1, userTo);
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("msgcntr", "got resultset" + rs);
            while (rs.next()) {
                setMsgcntrIds(new Integer(rs.getInt("PK_MSGCNTR")));
                Rlog.debug("msgcntr", "got pkmsg center");
                setMsgcntrStudyIds(rs.getString("FK_STUDY"));
                setMsgcntrFromUserIds(rs.getString("FK_USERX"));
                Rlog.debug("msgcntr", "got userX");
                setMsgcntrToUserIds(rs.getString("FK_USERTO"));
                Rlog.debug("msgcntr", "got user to");
                setMsgcntrTexts(rs.getString("MSGCNTR_TEXT"));
                Rlog.debug("msgcntr", "got msgcntr txt");
                setMsgcntrReqTypes(rs.getString("MSGCNTR_REQTYPE"));
                Rlog.debug("msgcntr", "got req type");
                setMsgcntrStatuses(rs.getString("MSGCNTR_STAT"));
                Rlog.debug("msgcntr", "got stat");
                setStudyNames(rs.getString("STUDY_NUMBER"));
                Rlog.debug("msgcntr", "got study NUMBER");
                setStudyVerNos(rs.getString("STUDY_VER_NO"));
                setUserFroms(rs.getString("USERFROM"));
                setUserStatus(rs.getString("user_status"));
                Rlog.debug("msgcntr", " got user to");
                Rlog.debug("msgcntr", "msgcntrDao.getMsgCntrVelosUser rows ");
            }
        } catch (SQLException ex) {
            Rlog.fatal("msgcntr",
                    "msgcntrDao.getMsgcntr EXCEPTION IN FETCHING FROM msgcntr table"
                            + ex);
        }
        finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
    // FUNCTION OVERLOADED TO GET THE SPECIFIC USERS
    public void getMsgCntrVelosUser(int userTo, String status)
    {
        Rlog.debug("msgcntr", "in DAO:getMsgVelosUser");
        PreparedStatement pstmt = null;
        Connection conn = null;
        String pkSview = null;
        try {
            conn = getConnection();
            Rlog.debug("msgcntr", "USER TO" + "*" + userTo + "*");
            String mysql = "SELECT "
                    + "PK_MSGCNTR,"
                    + "FK_STUDY,"
                    + "FK_USERX,"
                    + "FK_USERTO,"
                    + "MSGCNTR_TEXT,"
                    + "MSGCNTR_REQTYPE,"
                    + "MSGCNTR_STAT,"
                    + "STUDY_NUMBER,"
                    + "STUDY_VER_NO, "
                    + "MSGCNTR_PERM,"
                    + "A.USR_LASTNAME|| ',' ||  A.USR_FIRSTNAME USERFROM, FK_STUDYVIEW  "
                    + "FROM  ER_USER A,ER_MSGCNTR B,ER_STUDY C "
                    + "WHERE B.FK_USERTO = ? AND MSGCNTR_STAT = ? "
                    + "AND B.FK_STUDY=C.PK_STUDY AND  A.PK_USER=B.FK_USERX";
            Rlog.debug("msgcntr", mysql);
            pstmt = conn.prepareStatement(mysql);
            Rlog.debug("msgcntr", "pstmt " + pstmt);
            pstmt.setInt(1, userTo);
            // int msgStatus=Integer.parseInt(status);
            pstmt.setString(2, status);
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("msgcntr", "got resultset" + rs);
            while (rs.next()) {
                setMsgcntrIds(new Integer(rs.getInt("PK_MSGCNTR")));
                Rlog.debug("msgcntr", "got pkmsg center");
                setMsgcntrStudyIds(rs.getString("FK_STUDY"));
                setMsgcntrFromUserIds(rs.getString("FK_USERX"));
                Rlog.debug("msgcntr", "got userX");
                setMsgcntrToUserIds(rs.getString("FK_USERTO"));
                Rlog.debug("msgcntr", "got user to");
                setMsgcntrTexts(rs.getString("MSGCNTR_TEXT"));
                Rlog.debug("msgcntr", "got msgcntr txt");
                setMsgcntrReqTypes(rs.getString("MSGCNTR_REQTYPE"));
                Rlog.debug("msgcntr", "got req type");
                setMsgcntrStatuses(rs.getString("MSGCNTR_STAT"));
                Rlog.debug("msgcntr", "got stat");
                setStudyNames(rs.getString("STUDY_NUMBER"));
                Rlog.debug("msgcntr", "got study NUMBER");
                setStudyVerNos(rs.getString("STUDY_VER_NO"));
                setPermissions(rs.getString("MSGCNTR_PERM"));
                setUserFroms(rs.getString("USERFROM"));
                pkSview = rs.getString("FK_STUDYVIEW");
                if ((pkSview != null) && (pkSview.trim().length() > 0)) {
                    setMsgStudyView(pkSview);
                } else {
                    setMsgStudyView("");
                }
                Rlog.debug("msgcntr", " got user to");
                Rlog.debug("msgcntr", "msgcntrDao.getMsgCntrVelosUser rows ");
            }
        } catch (SQLException ex) {
            Rlog.fatal("msgcntr",
                    "msgcntrDao.getMsgcntr EXCEPTION IN FETCHING FROM msgcntr table"
                            + ex);
        }
        finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
    // function to get all the acknowledeged messages
    public void getMsgCntrVelosUserAcc(int userTo) {
        Rlog.debug("msgcntr", "in DAO:getMsgVelosUser");
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            Rlog.debug("msgcntr", "USER TO" + "*" + userTo + "*");
            String mysql = "SELECT "
            + "PK_MSGCNTR,"
            + "FK_STUDY,"
            + "FK_USERX,"
            + "FK_USERTO,"
            + "MSGCNTR_TEXT,"
            + "MSGCNTR_REQTYPE,"
            + "MSGCNTR_STAT,"
            + "STUDY_NUMBER,"
            + "STUDY_VER_NO, "
            + "MSGCNTR_PERM,"
            + "A.USR_LASTNAME|| ',' ||  A.USR_FIRSTNAME USERFROM "
            + "FROM  ER_USER A,ER_MSGCNTR B,ER_STUDY C "
            + "WHERE  B.FK_USERTO = ? AND MSGCNTR_REQTYPE in ('a','A') "
            + "AND B.FK_STUDY=C.PK_STUDY AND  A.PK_USER=B.FK_USERX";
            Rlog.debug("msgcntr", mysql);
            pstmt = conn.prepareStatement(mysql);
            Rlog.debug("msgcntr", "pstmt " + pstmt);
            pstmt.setInt(1, userTo);
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("msgcntr", "got resultset" + rs);
            while (rs.next()) {
                setMsgcntrIds(new Integer(rs.getInt("PK_MSGCNTR")));
                Rlog.debug("msgcntr", "got pkmsg center");
                setMsgcntrStudyIds(rs.getString("FK_STUDY"));
                setMsgcntrFromUserIds(rs.getString("FK_USERX"));
                Rlog.debug("msgcntr", "got userX");
                setMsgcntrToUserIds(rs.getString("FK_USERTO"));
                Rlog.debug("msgcntr", "got user to");
                setMsgcntrTexts(rs.getString("MSGCNTR_TEXT"));
                Rlog.debug("msgcntr", "got msgcntr txt");
                setMsgcntrReqTypes(rs.getString("MSGCNTR_REQTYPE"));
                Rlog.debug("msgcntr", "got req type");
                setMsgcntrStatuses(rs.getString("MSGCNTR_STAT"));
                Rlog.debug("msgcntr", "got stat");
                setStudyNames(rs.getString("STUDY_NUMBER"));
                Rlog.debug("msgcntr", "got study NUMBER");
                setStudyVerNos(rs.getString("STUDY_VER_NO"));
                setPermissions(rs.getString("MSGCNTR_PERM"));
                setUserFroms(rs.getString("USERFROM"));
                Rlog.debug("msgcntr", " got user to");
                Rlog.debug("msgcntr", "msgcntrDao.getMsgCntrVelosUser rows ");
            }
        } catch (SQLException ex) {
            Rlog.fatal("msgcntr",
                    "msgcntrDao.getMsgcntr EXCEPTION IN FETCHING FROM msgcntr table"
                            + ex);
        }
        finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
    public void getUserMsgs(int userTo) {
        Rlog.debug("msgcntr", "in DAO:getMsgVelosUser");
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            Rlog.debug("msgcntr", "USER TO" + "*" + userTo + "*");
            String mysql = "SELECT "
            + "PK_MSGCNTR,"
            + "FK_STUDY,"
            + "FK_USERX,"
            + "FK_USERTO,"
            + "MSGCNTR_TEXT,"
            + "MSGCNTR_REQTYPE,"
            + "MSGCNTR_STAT,"
            + "A.USR_LASTNAME|| ',' ||  A.USR_FIRSTNAME USERFROM, "
            + "B.CREATED_ON CREATED, "
            + "A.USR_STAT user_status, " + "er_account.ac_type ac_type, "
                    + "a.usr_code usr_code, " + "er_site.site_name site_name, "
                    + "a.fk_account accId "
                    + "FROM  ER_USER A, " + "ER_MSGCNTR B, " + "er_account, "
                    + "er_site "
                    + "WHERE  B.FK_USERTO = ? "
                    + "AND A.PK_USER = B.FK_USERX "
                    + "AND a.fk_account = pk_account "
                    + "and a.fk_siteid = pk_site " + "and A.USR_STAT != 'X' ";
            Rlog.debug("msgcntr", mysql);
            pstmt = conn.prepareStatement(mysql);
            Rlog.debug("msgcntr", "pstmt " + pstmt);
            pstmt.setInt(1, userTo);
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("msgcntr", "got resultset" + rs);
            while (rs.next()) {
                setMsgcntrIds(new Integer(rs.getInt("PK_MSGCNTR")));
                Rlog.debug("msgcntr", "got pkmsg center");
                setMsgcntrStudyIds(rs.getString("FK_STUDY"));
                setMsgcntrFromUserIds(rs.getString("FK_USERX"));
                Rlog.debug("msgcntr", "got userX");
                setMsgcntrToUserIds(rs.getString("FK_USERTO"));
                Rlog.debug("msgcntr", "got user to");
                setMsgcntrTexts(rs.getString("MSGCNTR_TEXT"));
                Rlog.debug("msgcntr", "got msgcntr txt");
                setMsgcntrReqTypes(rs.getString("MSGCNTR_REQTYPE"));
                Rlog.debug("msgcntr", "got req type");
                setMsgcntrStatuses(rs.getString("MSGCNTR_STAT"));
                Rlog.debug("msgcntr", "got stat");
                setUserFroms(rs.getString("USERFROM"));
                setCreateOns(rs.getString("CREATED"));
                setUserStatus(rs.getString("user_status"));
                setAccType(rs.getString("ac_type"));
                setUserCode(rs.getString("usr_code"));
                setUserSite(rs.getString("site_name"));
                setUserAccIds(rs.getString("accId"));
                Rlog.debug("msgcntr", " got user to");
                Rlog.debug("msgcntr", "msgcntrDao.getMsgCntrVelosUser rows ");
            }
        } catch (SQLException ex) {
            Rlog.fatal("msgcntr",
                    "msgcntrDao.getMsgcntr EXCEPTION IN FETCHING FROM msgcntr table"
                            + ex);
        }
        finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
    // this function find all the users (with details) with last name and
    // first name as lname and fname who have sent msgs to
    // the user userTo and with usr status as userStatus
    public void getUserMsgsWithSearch(int userTo, String lname, String fname,
            String userStatus) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String mysql = "SELECT " + "PK_MSGCNTR," + "FK_STUDY,"
                    + "FK_USERX," + "FK_USERTO," + "MSGCNTR_TEXT,"
                    + "MSGCNTR_REQTYPE," + "MSGCNTR_STAT,"
                    + "A.USR_LASTNAME|| ', ' ||  A.USR_FIRSTNAME USERFROM, "
                    + "B.CREATED_ON CREATED, " + "A.USR_STAT user_status, "
                    + "er_account.ac_type ac_type, " + "a.usr_code usr_code, "
                    + "er_site.site_name site_name, " + "a.fk_account accId "
                    + "FROM  ER_USER A, " + "ER_MSGCNTR B, " + "er_account, "
                    + "er_site " + "WHERE  B.FK_USERTO = ? "
                    + "AND A.PK_USER = B.FK_USERX "
                    + "AND a.fk_account = pk_account "
                    + "and a.fk_siteid = pk_site " + "and A.USR_STAT != 'X' ";
            if (!lname.equals(""))
                mysql = mysql + " and upper(a.usr_lastname) like upper('"
                        + lname + "%') ";
            if (!fname.equals(""))
                mysql = mysql + " and upper(a.usr_firstname) like upper('"
                        + fname + "%') ";
            if (!userStatus.equals("All"))
                mysql = mysql + " and a.usr_stat = '" + userStatus + "'";
            Rlog.debug("msgcntr", mysql);
            pstmt = conn.prepareStatement(mysql);
            Rlog.debug("msgcntr", "pstmt " + pstmt);
            pstmt.setInt(1, userTo);
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("msgcntr", "got resultset" + rs);
            while (rs.next()) {
                setMsgcntrIds(new Integer(rs.getInt("PK_MSGCNTR")));
                Rlog.debug("msgcntr", "got pkmsg center");
                setMsgcntrStudyIds(rs.getString("FK_STUDY"));
                setMsgcntrFromUserIds(rs.getString("FK_USERX"));
                Rlog.debug("msgcntr", "got userX");
                setMsgcntrToUserIds(rs.getString("FK_USERTO"));
                Rlog.debug("msgcntr", "got user to");
                setMsgcntrTexts(rs.getString("MSGCNTR_TEXT"));
                Rlog.debug("msgcntr", "got msgcntr txt");
                setMsgcntrReqTypes(rs.getString("MSGCNTR_REQTYPE"));
                Rlog.debug("msgcntr", "got req type");
                setMsgcntrStatuses(rs.getString("MSGCNTR_STAT"));
                Rlog.debug("msgcntr", "got stat");
                setUserFroms(rs.getString("USERFROM"));
                setCreateOns(rs.getString("CREATED"));
                setUserStatus(rs.getString("user_status"));
                setAccType(rs.getString("ac_type"));
                setUserCode(rs.getString("usr_code"));
                setUserSite(rs.getString("site_name"));
                setUserAccIds(rs.getString("accId"));
                Rlog.debug("msgcntr", " got user to");
                Rlog.debug("msgcntr", "msgcntrDao.getMsgCntrVelosUser rows ");
            }
        } catch (SQLException ex) {
            Rlog.fatal("msgcntr",
                    "msgcntrDao.getMsgcntr EXCEPTION IN FETCHING FROM msgcntr table"
                            + ex);
        }
        finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
    public void getMsgCntrStudy(int study) {
        Rlog.debug("msgcntr", "in DAO:getMsgCntrStudy");
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            Rlog.debug("msgcntr", "got connection" + conn);
            Rlog.debug("msgcntr", "STUDY" + "*" + study + "*");
            String mysql = "SELECT "
                    +
                    "PK_MSGCNTR,"
                    +
                    "FK_STUDY,"
                    +
                    "FK_USERX,"
                    +
                    "FK_USERTO,"
                    +
                    "MSGCNTR_TEXT,"
                    +
                    "MSGCNTR_REQTYPE,"
                    +
                    "MSGCNTR_STAT,"
                    +
                    "ER_MSGCNTR.CREATED_ON CREATED, "
                    +
                    "A.USR_LASTNAME|| ',' ||  A.USR_FIRSTNAME USERTO, "
                    +
                    "B.USR_LASTNAME|| ',' ||  B.USR_FIRSTNAME USERFROM   from "
                    +
                    "ER_MSGCNTR,ER_USER A ,ER_USER B WHERE ER_MSGCNTR.FK_STUDY= ? AND ER_MSGCNTR.FK_USERTO =A.PK_USER "
                    +
                    "AND ER_MSGCNTR.FK_USERX= B.PK_USER ";
            Rlog.debug("msgcntr", mysql);
            pstmt = conn.prepareStatement(mysql);
            Rlog.debug("msgcntr", "pstmt " + pstmt);
            pstmt.setInt(1, study);
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("msgcntr", "got resultset" + rs);
            while (rs.next()) {
                setMsgcntrIds(new Integer(rs.getInt("PK_MSGCNTR")));
                Rlog.debug("msgcntr", "got pkmsg center");
                Integer studyId = new Integer(rs.getInt("FK_STUDY"));
                Rlog.debug("msgcntr", "got study");
                setMsgcntrStudyIds(studyId.toString());
                Integer userId = new Integer(rs.getInt("FK_USERX"));
                setMsgcntrFromUserIds(userId.toString());
                Rlog.debug("msgcntr", "got userX");
                setMsgcntrToUserIds(rs.getString("FK_USERTO"));
                Rlog.debug("msgcntr", "got user to");
                setMsgcntrTexts(rs.getString("MSGCNTR_TEXT"));
                Rlog.debug("msgcntr", "got msgcntr txt");
                setMsgcntrReqTypes(rs.getString("MSGCNTR_REQTYPE"));
                Rlog.debug("msgcntr", "got req type");
                setMsgcntrStatuses(rs.getString("MSGCNTR_STAT"));
                setCreateOns(rs.getString("CREATED"));
                setUserTos(rs.getString("USERTO"));
                setUserFroms(rs.getString("USERFROM"));
                Rlog.debug("msgcntr", "got stat");
                Rlog.debug("msgcntr",
                        "msgcntrDao.getmsgcntrValuesByAccountId rows ");
            }
        } catch (SQLException ex) {
            Rlog.fatal("msgcntr",
                    "msgcntrDao.getMsgcntr EXCEPTION IN FETCHING FROM msgcntr table"
                            + ex);
        }
        finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
    public void updateMsgs(int studyId, int xUser, int author,
            String permission, int msgId) {
        CallableStatement cstmt = null;
        Rlog.debug("msgcntr", "In updateMsgs in MsgCntrDao line number 0");
        // OracleCallableStatement cstmt = null;
        Connection conn = null;
        Rlog.debug("msgcntr", "In updateMsgs in MsgCntrDao line number 1");
        try
        {
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_copystudy(?,?,?,?,?,?,?)}");
            Rlog.debug("msgcntr", "In updateMsgs in StudyDao line number 2");
            cstmt.setInt(1, studyId);
            Rlog.debug("msgcntr", "In updateMsgs in StudyDao line number 3");
            cstmt.setInt(2, xUser);
            Rlog.debug("msgcntr", "In updateMsgs in StudyDao line number 4");
            cstmt.setInt(3, author);
            Rlog.debug("msgcntr", "In updateMsgs in StudyDao line number 5");
            cstmt.setString(4, permission);
            Rlog.debug("msgcntr", "In updateMsgs in StudyDao line number 6");
            cstmt.setInt(5, msgId);
            Rlog.debug("msgcntr", "In updateMsgs in StudyDao line number 7");
            cstmt.registerOutParameter(6, java.sql.Types.INTEGER);
            Rlog.debug("msgcntr", "In updateMsgs in StudyDao line number 8");
            // cstmt.registerOutParameter(7,java.sql.Types.CHAR);
            cstmt.registerOutParameter(7, OracleTypes.CURSOR);
            Rlog.debug("msgcntr", "BEFORE EXECUTE");
            cstmt.execute();
            // ResultSet rset = (ResultSet)cstmt.getObject(1);
            Rlog.debug("msgcntr", "In updateMsgs in StudyDao line number 9");
            this.setMsgcntrStudyIds(Integer.toString(cstmt.getInt(6)));
            Rlog.debug("msgcntr", "In updateMsgs in StudyDao line number 9");
            this.setCreateOns(cstmt.getString(7));
            Rlog.debug("msgcntr", "In updateMsgs in StudyDao line number 10");
        }
        catch (SQLException ex)
        {
            Rlog.fatal("msgcntr",
                    "In updateMsgs in StudyDao line EXCEPTION IN calling the procedure"
                            + ex);
        }
        finally
        {
            try
            {
                if (cstmt != null)
                    cstmt.close();
            }
            catch (Exception e)
            {
            }
            try
            {
                if (conn != null)
                    conn.close();
            }
            catch (Exception e) {
            }
        }
    }
    // function added to get the count of read,unread and acclno of the logged
    // in user
    public void getMsgCount(int userTo) {
        Rlog.debug("msgcntr", "in DAO:getMsgCount");
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            Rlog.debug("msgcntr", "got connection" + conn);
            String mysql = "select sum(decode(upper(msgcntr_stat),'R',1,0) ) Read,"
                    +
                    "sum(decode(upper(msgcntr_stat),'U',1,0) ) Unread," +
                    "sum(decode(upper(msgcntr_stat),'A',1,0) ) Acc " +
                    "from er_msgcntr where  fk_userto = ? ";
            Rlog.debug("msgcntr", mysql);
            pstmt = conn.prepareStatement(mysql);
            Rlog.debug("msgcntr", "pstmt " + pstmt);
            pstmt.setInt(1, userTo);
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("msgcntr", "got resultset" + rs);
            while (rs.next()) {
                setMsgRead(rs.getString("Read"));
                setMsgUnRead(rs.getString("UnRead"));
                setMsgAcc(rs.getString("Acc"));
                Rlog.debug("msgcntr", "msgcntrDao.getMSgCount rows ");
            }
        } catch (SQLException ex) {
            Rlog.fatal("msgcntr",
                    "msgcntrDao.getMsgcntr EXCEPTION IN FETCHING FROM msgcntr table"
                            + ex);
        }
        finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
}