package com.velos.eres.business.appendix.impl;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="ER_STUDYAPNDX")
public class StudyApndxPojo implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer PK_STUDYAPNDX;

	private Integer FK_STUDY;

	private String STUDYAPNDX_TYPE;

	private String STUDYAPNDX_URI;

	private String STUDYAPNDX_DESC;

	private String STUDYAPNDX_PUBFLAG;

	private String STUDYAPNDX_FILE;

	private byte[] STUDYAPNDX_FILEOBJ;

	private Integer CREATOR;

	private Date CREATED_ON;

	private String IP_ADD;

	private Integer STUDYAPNDX_FILESIZE;

	private Integer FK_STUDYVER;

	private byte[] fileStream;
	
	@Id
	@Column(name="PK_STUDYAPNDX")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN")
	@SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_STUDYAPNDX", allocationSize=1)
	public Integer getPK_STUDYAPNDX() {
		return PK_STUDYAPNDX;
	}
	public void setPK_STUDYAPNDX(Integer pKSTUDYAPNDX) {
		PK_STUDYAPNDX = pKSTUDYAPNDX;
	}
	@Column(name="FK_STUDY")
	public Integer getFK_STUDY() {
		return FK_STUDY;
	}
	public void setFK_STUDY(Integer fKSTUDY) {
		FK_STUDY = fKSTUDY;
	}
	@Column(name="STUDYAPNDX_TYPE")
	public String getSTUDYAPNDX_TYPE() {
		return STUDYAPNDX_TYPE;
	}
	public void setSTUDYAPNDX_TYPE(String sTUDYAPNDXTYPE) {
		STUDYAPNDX_TYPE = sTUDYAPNDXTYPE;
	}
	@Column(name="STUDYAPNDX_URI")
	public String getSTUDYAPNDX_URI() {
		return STUDYAPNDX_URI;
	}
	public void setSTUDYAPNDX_URI(String sTUDYAPNDXURI) {
		STUDYAPNDX_URI = sTUDYAPNDXURI;
	}
	@Column(name="STUDYAPNDX_DESC")
	public String getSTUDYAPNDX_DESC() {
		return STUDYAPNDX_DESC;
	}
	public void setSTUDYAPNDX_DESC(String sTUDYAPNDXDESC) {
		STUDYAPNDX_DESC = sTUDYAPNDXDESC;
	}
	@Column(name="STUDYAPNDX_PUBFLAG")
	public String getSTUDYAPNDX_PUBFLAG() {
		return STUDYAPNDX_PUBFLAG;
	}
	public void setSTUDYAPNDX_PUBFLAG(String sTUDYAPNDXPUBFLAG) {
		STUDYAPNDX_PUBFLAG = sTUDYAPNDXPUBFLAG;
	}
	
	@Column(name="STUDYAPNDX_FILE")
	public String getSTUDYAPNDX_FILE() {
		return STUDYAPNDX_FILE;
	}
	public void setSTUDYAPNDX_FILE(String sTUDYAPNDXFILE) {
		STUDYAPNDX_FILE = sTUDYAPNDXFILE;
	}

	@Column(name = "CREATOR")
	public Integer getCREATOR() {
		return CREATOR;
	}
	public void setCREATOR(Integer cREATOR) {
		CREATOR = cREATOR;
	}
	@Column(name = "CREATED_ON")
	public Date getCREATED_ON() {
		return CREATED_ON;
	}
	public void setCREATED_ON(Date cREATEDON) {
		CREATED_ON = cREATEDON;
	}
	@Column(name="IP_ADD")
	public String getIP_ADD() {
		return IP_ADD;
	}
	public void setIP_ADD(String iPADD) {
		IP_ADD = iPADD;
	}
	@Column(name="STUDYAPNDX_FILESIZE")
	public Integer getSTUDYAPNDX_FILESIZE() {
		return STUDYAPNDX_FILESIZE;
	}
	public void setSTUDYAPNDX_FILESIZE(Integer sTUDYAPNDXFILESIZE) {
		STUDYAPNDX_FILESIZE = sTUDYAPNDXFILESIZE;
	}
	@Column(name="FK_STUDYVER")
	public Integer getFK_STUDYVER() {
		return FK_STUDYVER;
	}
	public void setFK_STUDYVER(Integer fKSTUDYVER) {
		FK_STUDYVER = fKSTUDYVER;
	}
	public StudyApndxPojo() {
		super();
	}
	@Transient
	public byte[] getFileStream() {
		return fileStream;
	}
	public void setFileStream(byte[] fileStream) {
		this.fileStream = fileStream;
	}
	@Lob
	@Column(name="STUDYAPNDX_FILEOBJ")
	public byte[] getSTUDYAPNDX_FILEOBJ() {
		return STUDYAPNDX_FILEOBJ;
	}
	public void setSTUDYAPNDX_FILEOBJ(byte[] sTUDYAPNDXFILEOBJ) {
		STUDYAPNDX_FILEOBJ = sTUDYAPNDXFILEOBJ;
	}

}
