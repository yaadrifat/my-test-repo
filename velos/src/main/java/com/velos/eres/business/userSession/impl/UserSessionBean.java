/*
 * Classname		UserSessionBean.class
 * 
 * Version information	1.0
 *
 * Date					03/03/2003
 * 
 * Copyright notice    Velos Inc.
 */

package com.velos.eres.business.userSession.impl;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The UserSession CMP entity bean.<br>
 * <br>
 * 
 * @author Sonia Sahni
 * @version 1.0
 * @ejbHome UserSessionHome
 * @ejbRemote UserSessionRObj
 */
@Entity
@Table(name = "er_USERSESSION")
public class UserSessionBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3906930092692090929L;

    /**
     * the UserSession Id
     */
    public Integer id;

    /**
     * the user id
     */
    public Integer userId;

    /**
     * the login date time of user session
     */
    public Timestamp loginTime;

    /**
     * the logout date time of user session
     */
    public Timestamp logoutTime;

    /*
     * Login sucess flag
     */
    public Integer successFlag;

    /*
     * IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_USERSESSION", allocationSize=1)
    @Column(name = "PK_USERSESSION")
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "FK_USER")
    public String getUserId() {
        return StringUtil.integerToString(this.userId);
    }

    public void setUserId(String usrId) {
        if (usrId != null) {
            this.userId = Integer.valueOf(usrId);
        }
    }

    @Column(name = "LOGIN_TIME")
    public Timestamp getLoginTimeDate() {
        return this.loginTime;
    }

    public void setLoginTimeDate(Timestamp date) {
        this.loginTime = date;
    }

    @Transient
    public String getLoginTime() {
        String pDate;
        pDate = "";
 
        pDate = DateUtil.dateToString(getLoginTimeDate(), null);
        return pDate;
    }

    public void setLoginTime(String loginTime) {
        DateUtil pDate = null;
        setLoginTimeDate(DateUtil.stringToTimeStamp(loginTime,
                null));

        // this.loginTime=Timestamp.valueOf(loginTime);
  
    }

    @Column(name = "LOGOUT_TIME")
    public Timestamp getLogoutTimeDate() {
        return this.logoutTime;
    }

    public void setLogoutTimeDate(Timestamp date) {
        this.logoutTime = date;
    }

    @Transient
    public String getLogoutTime() {
        String pDate;
        pDate = "";
        pDate = DateUtil.dateToString(getLogoutTimeDate() ,null);
        // pDate = DateUtil.dateToString(this.logoutTime);
        return pDate;
    }

    public void setLogoutTime(String logoutTime) {
        DateUtil pDate = null;
        setLogoutTimeDate(DateUtil.stringToTimeStamp(logoutTime, null));
        // this.logoutTime = Timestamp.valueOf(logoutTime);
        // this.logoutTime=DateUtil.stringToDate(logoutTime);
    }

    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Column(name = "SUCCESS_FLAG")
    public String getSuccessFlag() {
        return StringUtil.integerToString(this.successFlag);
    }

    public void setSuccessFlag(String successFlag) {
        if (successFlag != null) {
            this.successFlag = Integer.valueOf(successFlag);
        }

    }

    // END OF GETTER SETTER METHODS

    /**
     * @return state holder object associated with this milepayment
     */

    /*
     * public UserSessionStateKeeper getUserSessionStateKeeper() {
     * Rlog.debug("usersession", "Get UserSessionState Keeper"); return (new
     * UserSessionStateKeeper(getId(), getUserId(), getLoginTime(),
     * getLogoutTime(), getSuccessFlag(), getIpAdd())); }
     */

    /**
     * Inserts the usersession in the Database
     * 
     * @param usk
     *            StateKeeper of usersession
     * @return 0 when updation is successful and -2 when some exception occurs
     */
    /*
     * public int setUserSessionStateKeeper(UserSessionStateKeeper usk) {
     * 
     * GenerateId genId = null;
     * 
     * try {
     * 
     * setId(id); setIpAdd(usk.getIpAdd()); setLoginTime(usk.getLoginTime());
     * setLogoutTime(usk.getLogoutTime()); setSuccessFlag(usk.getSuccessFlag());
     * setUserId(usk.getUserId()); return 0; } catch (Exception e) {
     * Rlog.fatal("usersession", "EXCEPTION IN STORING usersession" + e); return
     * -2; } }
     */

    /**
     * Updates the usersession in the Database
     * 
     * @param usk
     *            StateKeeper of usersession
     * @return 0 when updation is successful and -2 when some exception occurs
     */
    public int updateUserSession(UserSessionBean usk) {

        try {
            setIpAdd(usk.getIpAdd());
            setLoginTime(usk.getLoginTime());
            setLogoutTime(usk.getLogoutTime());
            setSuccessFlag(usk.getSuccessFlag());
            setUserId(usk.getUserId());
           
            Rlog.debug("usersession", "UPDATED");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("usersession",
                    "EXCEPTION IN UPDATING usersession IN DATABASE " + e);
            return -2;
        }
    }

    public UserSessionBean() {

    }

    public UserSessionBean(Integer id, String userId, String loginTime,
            String logoutTime, String successFlag, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setId(id);
        setUserId(userId);
        setLoginTime(loginTime);
        setLogoutTime(logoutTime);
        setSuccessFlag(successFlag);
        setIpAdd(ipAdd);
    }
}
