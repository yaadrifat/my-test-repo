/*
 * Classname			StudyVer.class
 * 
 * Version information 	1.0
 *
 * Date					12/03/2002				
 * 
 * Copyright notice		Velos, Inc.
 * by Arvind Kumar		
 */

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import com.velos.eres.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;
import com.velos.services.OperationException;

/**
 * StudyVerDao for Study Versions
 * 
 * @author Arvind
 * @version : 1.0 12/03/2002
 */

public class StudyVerDao extends CommonDAO implements java.io.Serializable {
    private ArrayList studyVerIds;

    private ArrayList studyVerStudyIds;

    private ArrayList studyVerNumbers;

    private ArrayList studyVerStatus;

    private ArrayList studyVerNotes;
    
    
    //JM: 11/17/2005  
    private ArrayList studyVerDates;
    
    private ArrayList studyVerCategories;
    
    private ArrayList studyVerCategoryIds;

    private ArrayList studyVerTypes;

    /**
     * Stores a collection of Version Status Description (Description from
     * er_codelst)
     */
    private ArrayList studyVerStatusDescs;

    /** Stores a collection of Version Status Pks ( from er_status_history) */
    private ArrayList studyVerStatusPKs;

    private ArrayList creators;

    private ArrayList ipAdds;

    private ArrayList secCounts;

    private ArrayList apndxCounts;
    
    private ArrayList apndxPks;
    
    private ArrayList apndxUris;
    
    private ArrayList apndxLens;
    
    private ArrayList prevStampFlags;
    
    private ArrayList currStampFlags;
    
    private ArrayList prevApndxUris;
    
    private ArrayList currApndxUris;
    
    private ArrayList prevStudyVerIds;

    private int cRows;

    /**
     * Default Constructor
     */

    /*
     * Modified by Sonia Sahni 08/11/04, added initialization for
     * studyVerStatusDesc,studyVerStatusPKs
     */

    public StudyVerDao() {
        studyVerIds = new ArrayList();
        studyVerStudyIds = new ArrayList();
        studyVerNumbers = new ArrayList();
        studyVerStatus = new ArrayList();
        studyVerNotes = new ArrayList();
        
        studyVerDates = new  ArrayList();
        studyVerCategories = new ArrayList();
        studyVerCategoryIds = new ArrayList();
        studyVerTypes = new ArrayList();
        
        secCounts = new ArrayList();
        apndxCounts = new ArrayList();
        apndxPks = new ArrayList();
        apndxUris = new ArrayList();
        apndxLens = new ArrayList();
        studyVerStatusDescs = new ArrayList();
        studyVerStatusPKs = new ArrayList();
        prevStampFlags = new ArrayList();
        currStampFlags = new ArrayList();
        prevApndxUris = new ArrayList();
        currApndxUris = new ArrayList();
        prevStudyVerIds = new ArrayList();
        creators = new ArrayList();
        ipAdds = new ArrayList();

    }

    // Getter and Setter methods

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public ArrayList getSecCounts() {
        return this.secCounts;
    }

    public void setSecCounts(ArrayList secCounts) {
        this.secCounts = secCounts;
    }

    public ArrayList getApndxCounts() {
        return this.apndxCounts;
    }

    public void setApndxCounts(ArrayList apndxCounts) {
        this.apndxCounts = apndxCounts;
    }
    
    public ArrayList getPrevStampFlags() {
        return this.prevStampFlags;
    }

    public void setPrevStampFlags(ArrayList prevStampFlags) {
        this.prevStampFlags = prevStampFlags;
    }
    
    public ArrayList getCurrStampFlags() {
        return this.currStampFlags;
    }

    public void setCurrStampFlags(ArrayList currStampFlags) {
        this.currStampFlags = currStampFlags;
    }
    
    public ArrayList getPrevStudyVerIds() {
        return this.prevStudyVerIds;
    }

    public void setPrevStudyVerIds(ArrayList prevStudyVerIds) {
        this.prevStudyVerIds = prevStudyVerIds;
    }

    public ArrayList getApndxPks() {
        return this.apndxPks;
    }

    public void setApndxPks(ArrayList apndxPks) {
        this.apndxPks = apndxPks;
    }

    public ArrayList getApndxUris() {
        return this.apndxUris;
    }

    public void setApndxUris(ArrayList apndxUris) {
        this.apndxUris = apndxUris;
    }
    
    public ArrayList getPrevApndxUris() {
        return this.prevApndxUris;
    }

    public void setPrevApndxUris(ArrayList prevApndxUris) {
        this.prevApndxUris = prevApndxUris;
    }
    
    public ArrayList getCurrApndxUris() {
        return this.currApndxUris;
    }

    public void setCurrApndxUris(ArrayList currApndxUris) {
        this.currApndxUris = currApndxUris;
    }

    public ArrayList getApndxLens() {
        return this.apndxLens;
    }

    public void setApndxLens(ArrayList apndxLens) {
        this.apndxLens = apndxLens;
    }

    public ArrayList getStudyVerIds() {
        return this.studyVerIds;
    }

    public void setStudyVerIds(ArrayList studyVerIds) {
        this.studyVerIds = studyVerIds;
    }

    public ArrayList getStudyVerStudyIds() {
        return this.studyVerStudyIds;
    }

    public void setStudyVerStudyIds(ArrayList studyVerStudyIds) {
        this.studyVerStudyIds = studyVerStudyIds;
    }

    public ArrayList getStudyVerNumbers() {
        return this.studyVerNumbers;
    }

    public void setStudyVerNumbers(ArrayList studyVerNumbers) {
        this.studyVerNumbers = studyVerNumbers;
    }

    public ArrayList getStudyVerStatus() {
        return this.studyVerStatus;
    }

    public void setStudyVerStatus(ArrayList studyVerStatus) {
        this.studyVerStatus = studyVerStatus;
    }

    public ArrayList getStudyVerNotes() {
        return this.studyVerNotes;
    }

    public void setStudyVerNotes(ArrayList studyVerNotes) {
        this.studyVerNotes = studyVerNotes;
    }

    public ArrayList getCreators() {
        return this.creators;
    }

    public void setCreators(ArrayList creators) {
        this.creators = creators;
    }

    public ArrayList getIpAdds() {
        return this.ipAdds;
    }

    public void setIpAdds(ArrayList ipAdds) {
        this.ipAdds = ipAdds;
    }

    //

    public void setSecCounts(Integer secCount) {
        secCounts.add(secCount);
    }

    public void setApndxCounts(Integer apndxCount) {
        apndxCounts.add(apndxCount);
    }    

    public void setStudyVerIds(Integer verId) {
        studyVerIds.add(verId);
    }

    public void setStudyVerStudyIds(Integer studyId) {
        studyVerStudyIds.add(studyId);
    }

    public void setStudyVerNumbers(String verNo) {
        studyVerNumbers.add(verNo);
    }

    public void setStudyVerStatus(String verStatus) {
        studyVerStatus.add(verStatus);
    }

    public void setStudyVerNotes(String verNote) {
        studyVerNotes.add(verNote);
    }

    public void setCreators(Integer creator) {
        creators.add(creator);
    }

    public void setPrevStudyVerIds(Integer prevStudyVerId) {
    	prevStudyVerIds.add(prevStudyVerId);
    }
    
    public void setIpAdds(String ipAdd) {
        ipAdds.add(ipAdd);
    }
    
//JM:
    public void setStudyVerDates(String verDate) {
    	studyVerDates.add(verDate);
    }

    public void setStudyVerCategories(String verCategories) {
    	studyVerCategories.add(verCategories);
    }

    public void setStudyVerTypes(String verTypes) {
    	studyVerTypes.add(verTypes);
    }
//
    

    

    /**
     * appends a study version status description to class member ArrayList
     * studyVerStatusDescs
     * 
     * @param studyVerStatusDesc
     *            Study Version Description
     */
    public void setStudyVerStatusDescs(String studyVerStatusDesc) {
        studyVerStatusDescs.add(studyVerStatusDesc);
    }

    /**
     * Returns an ArrayList of Study Version Descriptions
     * 
     * @return ArrayList of Study Version Descriptions
     */

    public ArrayList getStudyVerStatusDescs() {
        return this.studyVerStatusDescs;
    }

    /**
     * Sets an ArrayList of Study Version Descriptions to class member ArrayList
     * studyVerStatusDescs
     * 
     * @param studyVerStatusDesc
     *            ArrayList of Study Version Descriptions
     */
    public void setStudyVerStatusDescs(ArrayList studyVerStatusDescs) {
        this.studyVerStatusDescs = studyVerStatusDescs;
    }

    /**
     * appends a study version Status PK to class member ArrayList
     * studyVerStatusPKs
     * 
     * @param studyVerStatusPK
     *            Study Version Status PK
     */
    public void setStudyVerStatusPKs(String studyVerStatusPK) {
        studyVerStatusPKs.add(studyVerStatusPK);
    }

    /**
     * Returns an ArrayList of Study Version Status PKs
     * 
     * @return ArrayList of Study Version Status Pks
     */

    public ArrayList getStudyVerStatusPKs() {
        return this.studyVerStatusPKs;
    }

    /**
     * Sets an ArrayList of Study Version Status Pks to class member ArrayList
     * studyVerStatusPKs
     * 
     * @param studyVerStatusPKs
     *            ArrayList of Study Version Status Pks
     */
    public void setStudyVerStatusPKs(ArrayList studyVerStatusPKs) {
        this.studyVerStatusPKs = studyVerStatusPKs;
    }
    
  //JM 
    /**
     * Returns an ArrayList of Study Version Dates
     * 
     * @return ArrayList of Study Version Dates
     */

    public ArrayList getStudyVerDates() {
        return this.studyVerDates;
    }

    /**
     * Sets an ArrayList of Study Version Dates to class member ArrayList
     * 
     * 
     * @param studyVerDates
     *            ArrayList of Study Version Dates
     */
    public void setStudyVerDates(ArrayList studyVerDates) {
        this.studyVerDates = studyVerDates;
    }
    
    
    
    
    /**
     * Returns an ArrayList of Study Version Categories
     * 
     * @return ArrayList of Study Version Categories
     */

    public ArrayList getStudyVerCategories() {
        return this.studyVerCategories;
    }

    /**
     * Sets an ArrayList of Study Version Categories to class member ArrayList
     * 
     * 
     * @param Categories
     *            ArrayList of Study Version Categories
     */
    public void setStudyVerCategories(ArrayList studyVerCategories) {
        this.studyVerCategories = studyVerCategories;
    }
      
    public ArrayList getStudyVerCategoryIds() {
        return this.studyVerCategoryIds;
    }

    private void setStudyVerCategoryIds(String categoryId) {
        studyVerCategoryIds.add(categoryId);
    }


     public ArrayList getStudyVerTypes(){
    	  
    	  return this.studyVerTypes;
    	  
      }
      
      public void setStudyVerTypes(ArrayList studyVerTypes) {
          this.studyVerTypes = studyVerTypes;
      }
      
      //JM
 
    // end of getter and setter methods

    /**
     * Populates the Data Access object with all study versions
     * 
     * @param StudyId -
     *            Id of the Study
     */

    /*
     * Changed by Sonia Sahni 08/11/04, Version's latest Status will be fetched
     * from er_status_history Now we are maintaining version's status history
     * also.STUDYVER_STATUS will not be used anymore.
     */

    //public void getAllVers(int studyId) { 
      public void getAllVers(int studyId){
          int rows = 0;
          PreparedStatement pstmt = null;
          Connection conn = null;
          
      /*    
          String sWhere = "";
          String vcWhere = "";
          String vtWhere = "";
          String vstWhere = "";
          String obWhere = "";
          String otWhere = "";
          
          */
        //  StringBuffer sqlBuffer = new StringBuffer();
          try {
              conn = getConnection();
              // Modified by Ganapathy on 05/25/05 for may enhancement-2
              String mysql = "SELECT PK_STUDYVER,"
                  + " (select count(*) from er_studysec where fk_studyver = pk_studyver ) secCount, "
                  + " (select count(*) from er_studyapndx where fk_studyver = pk_studyver) apndxCount, "
                  + " FK_STUDY,"
                  + " STUDYVER_NUMBER,STUDYVER_DATE,"
                  + " (select codelst_desc from er_codelst where pk_codelst=a.STUDYVER_CATEGORY) as STUDYVER_CATEGORY,"
                  + " (select codelst_desc from er_codelst where pk_codelst=a.STUDYVER_TYPE) as STUDYVER_TYPE,"
                  + " STUDYVER_NOTES,"
                  + " CREATOR,"
                  + " IP_ADD, CODELST_DESC STATUS_CODEDESC, CODELST_SUBTYP STATUS_CODESUBTYP,PK_STATUS, a.STUDYVER_CATEGORY as STUDYVER_CATID "
                  + " FROM ER_STUDYVER a , ( Select CODELST_DESC, CODELST_SUBTYP, PK_STATUS,STATUS_MODPK, FK_CODELST_STAT  from "
                  + " er_studyver i, ER_STATUS_HISTORY, er_codelst c Where i.fk_study = ? and i.PK_STUDYVER =  STATUS_MODPK and STATUS_MODTABLE = ? and "
                  + " STATUS_END_DATE is null and c.pk_codelst = FK_CODELST_STAT ) stat"
                  + " WHERE FK_STUDY = ? and STATUS_MODPK (+) = pk_studyver " ;
              
              Rlog.debug("StudyVer", "StudyVerDao.getAllVers SQL " + mysql);
              pstmt = conn.prepareStatement(mysql);//JM
              pstmt.setInt(1, studyId);
              pstmt.setString(2, "er_studyver");
              pstmt.setInt(3, studyId);

              ResultSet rs = pstmt.executeQuery();

              while (rs.next()) {

                  setStudyVerIds(new Integer(rs.getInt("PK_STUDYVER")));
                  setSecCounts(new Integer(rs.getInt("secCount")));
                  setApndxCounts(new Integer(rs.getInt("apndxCount")));
                  setStudyVerStudyIds(new Integer(rs.getInt("FK_STUDY")));
                  setStudyVerNumbers(rs.getString("STUDYVER_NUMBER"));
                  setStudyVerStatus(rs.getString("STATUS_CODESUBTYP"));
                  setStudyVerNotes(rs.getString("STUDYVER_NOTES"));
                  setCreators(new Integer(rs.getInt("CREATOR")));
                  setIpAdds(rs.getString("IP_ADD"));
                  setStudyVerStatusDescs(rs.getString("STATUS_CODEDESC"));
                  setStudyVerStatusPKs(rs.getString("PK_STATUS"));
                  
                  //JM: 
                  setStudyVerDates(rs.getString("STUDYVER_DATE"));
                  setStudyVerCategories(rs.getString("STUDYVER_CATEGORY"));
                  setStudyVerTypes(rs.getString("STUDYVER_TYPE"));
                  setStudyVerCategoryIds(rs.getString("STUDYVER_CATID"));

                  rows++;
                  Rlog.debug("StudyVer", "StudyVerDao.getAllVers " + rows);
              }

              setCRows(rows);
          } catch (SQLException ex) {
              Rlog.fatal("StudyVer",
                      "StudyVerDao.getAllVers  EXCEPTION IN FETCHING FROM er_studyver table"
                              + ex);
          } finally {
              try {
                  if (pstmt != null)
                      pstmt.close();
              } catch (Exception e) {
              }
              try {
                  if (conn != null)
                      conn.close();
              } catch (Exception e) {
              }

          }

      }


/*
      public void getAllStudyVers(int studyId,String studyVerNumber,String verCat, String verType, String verStat, String orderBy, String orderType){
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        
        
        String sWhere = "";
        String vcWhere = "";
        String vtWhere = "";
        String vstWhere = "";
        String obWhere = "";
        String otWhere = "";
        
        StringBuffer sqlBuffer = new StringBuffer();
        try {
            conn = getConnection();

            String mysqlSelect = "SELECT PK_STUDYVER,"
                    + " (select count(*) from er_studysec where fk_studyver = pk_studyver ) secCount, "
                    + " (select count(*) from er_studyapndx where fk_studyver = pk_studyver) apndxCount, "
                    + " FK_STUDY,"
                    + " STUDYVER_NUMBER,STUDYVER_DATE,"
                    + " (select codelst_desc from er_codelst where pk_codelst=a.STUDYVER_CATEGORY) as STUDYVER_CATEGORY,"
                    + " (select codelst_desc from er_codelst where pk_codelst=a.STUDYVER_TYPE) as STUDYVER_TYPE,"
                    + " STUDYVER_NOTES,"
                    + " CREATOR,"
                    + " IP_ADD, CODELST_DESC STATUS_CODEDESC, CODELST_SUBTYP STATUS_CODESUBTYP,PK_STATUS "
                    + " FROM ER_STUDYVER a , ( Select CODELST_DESC, CODELST_SUBTYP, PK_STATUS,STATUS_MODPK, FK_CODELST_STAT  from "
                    + " er_studyver i, ER_STATUS_HISTORY, er_codelst c Where i.fk_study = ? and i.PK_STUDYVER =  STATUS_MODPK and STATUS_MODTABLE = ? and "
                    + " STATUS_END_DATE is null and c.pk_codelst = FK_CODELST_STAT ) stat"
                    + " WHERE FK_STUDY = ? and STATUS_MODPK (+) = pk_studyver " ;
            
       	 
				if ((studyVerNumber!= null) && (! studyVerNumber.trim().equals("") && ! studyVerNumber.trim().equals("null")))
				{
					sWhere = " and upper(a.STUDYVER_NUMBER) like upper('";							
					sWhere+=studyVerNumber;
					sWhere+="%')";
				}
				
				if ((verCat!= null) && (! verCat.trim().equals("") && ! verCat.trim().equals("null")))
				{
					vcWhere = " and upper(a.STUDYVER_CATEGORY) = upper(";
					vcWhere+=verCat;
					vcWhere+=")";
				}
				
				if ((verType!= null) && (! verType.trim().equals("") && ! verType.trim().equals("null")))
				{
					vtWhere = " and a.STUDYVER_TYPE = UPPER(" ;
					vtWhere+=verType;
					vtWhere+=")";
				}
				
				if ((verStat!= null) && (! verStat.trim().equals("") && ! verStat.trim().equals("null")))
				{
					vstWhere = " and FK_CODELST_STAT = UPPER(" ;
					vstWhere+=verStat;
					vstWhere+=")";
				}
				if ((orderBy!= null) && (! orderBy.trim().equals("") && ! orderBy.trim().equals("null")))
				{
					obWhere = " order by " +orderBy+ " " + orderType;
					
				}
				else{
						  
					obWhere = " order by PK_STUDYVER DESC ";
				}
            
				sqlBuffer.append(mysqlSelect);
				
	            if(sWhere!=null){
	            	sqlBuffer.append(sWhere);
	            }
	            if(vcWhere !=null){
	            	sqlBuffer.append(vcWhere );
	            }
	            if(vtWhere!=null){
	            	sqlBuffer.append(vtWhere);
	            }
	            if(vstWhere !=null){
	            	sqlBuffer.append(vstWhere );
	            }
	            if(orderBy!=null){
	            	sqlBuffer.append(obWhere);
	            }
	            
            
	            String mysqlFinal=sqlBuffer.toString();

            Rlog.debug("StudyVer", "StudyVerDao.getAllVers SQL " + mysqlFinal);
            pstmt = conn.prepareStatement(mysqlFinal);
            pstmt.setInt(1, studyId);
            pstmt.setString(2, "er_studyver");
            pstmt.setInt(3, studyId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setStudyVerIds(new Integer(rs.getInt("PK_STUDYVER")));
                setSecCounts(new Integer(rs.getInt("secCount")));
                setApndxCounts(new Integer(rs.getInt("apndxCount")));
                setStudyVerStudyIds(new Integer(rs.getInt("FK_STUDY")));
                setStudyVerNumbers(rs.getString("STUDYVER_NUMBER"));
                setStudyVerStatus(rs.getString("STATUS_CODESUBTYP"));
                setStudyVerNotes(rs.getString("STUDYVER_NOTES"));
                setCreators(new Integer(rs.getInt("CREATOR")));
                setIpAdds(rs.getString("IP_ADD"));
                setStudyVerStatusDescs(rs.getString("STATUS_CODEDESC"));
                setStudyVerStatusPKs(rs.getString("PK_STATUS"));
                
                //JM: 
                setStudyVerDates(rs.getString("STUDYVER_DATE"));
                setStudyVerCategories(rs.getString("STUDYVER_CATEGORY"));
                setStudyVerTypes(rs.getString("STUDYVER_TYPE"));

                rows++;
                Rlog.debug("StudyVer", "StudyVerDao.getAllVers " + rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("StudyVer",
                    "StudyVerDao.getAllStudyVers  EXCEPTION IN FETCHING FROM er_studyver table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
*/
    /**
     * Populates the Data Access Object with study versions with latest status
     * as the specified status. <br>
     * Status is passed as codelst_subtyp for codelst_type = 'versionStatus'
     * 
     * @param studyId
     *            Study Id
     * @param status
     *            Version Status : codelst_subtyp for codelst_type =
     *            'versionStatus'
     */
    /* Modified by sonia sahni, changed to get the status from status history */
    public void getAllVers(int studyId, String status) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            String sql = "SELECT PK_STUDYVER,STUDYVER_NUMBER "
                    + " FROM ER_STUDYVER , ER_STATUS_HISTORY, er_codelst c  "
                    + " WHERE FK_STUDY = ?  and PK_STUDYVER =  STATUS_MODPK and "
                    + " STATUS_MODTABLE = ? and   STATUS_END_DATE is null and "
                    + " c.pk_codelst = FK_CODELST_STAT and CODELST_SUBTYP = ? order by PK_STUDYVER ASC";

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);
            pstmt.setString(2, "er_studyver");
            pstmt.setString(3, status);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setStudyVerIds(new Integer(rs.getInt("PK_STUDYVER")));
                setStudyVerNumbers(rs.getString("STUDYVER_NUMBER"));

                rows++;
                Rlog.debug("StudyVer",
                        "StudyVerDao.getAllVers(int studyId, String status) "
                                + rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "StudyVer",
                            "StudyVerDao.getAllVers(int studyId, String status)  EXCEPTION IN FETCHING FROM er_studyver table"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    public int updateVersionInStudyVersionTable(int studyId, String verNumber) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int result=0;
        String str1="";
     	String str2="";
     	String newVersion="";
        if(verNumber.contains(".")) {
			StringTokenizer str = new StringTokenizer(verNumber, ".");
         	
         	while(str.hasMoreTokens()) {
         		str1 = str.nextToken();
         		str2 = str.nextToken();		                     		
         	}
         	newVersion = str1 + "." + "0";
		} else {
			newVersion = verNumber + "." + "0";
		}      
     	
        try {
            conn = getConnection();
            String sql = "update er_studyver set studyver_number = '" + newVersion + "' where fk_study=? and studyver_number=? ";            

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId); 
            pstmt.setString(2, verNumber);
            
            result = pstmt.executeUpdate();
            
        } catch (SQLException ex) {
            Rlog.fatal("StudyVer", "StudyVerDao.updateVersioninStudyVersionTable(int studyId)  EXCEPTION IN updating er_studyver table" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return result;

    }
    
    public ArrayList<String> getStudyVersions(int studyId) {		
		
    	PreparedStatement pstmt = null;		
        Connection conn = null;
        ResultSet rs = null;    
        String studyverNumber="";
        ArrayList<String> verList= new  ArrayList<String>();
               
        try {
            conn = getConnection();
            String sql = "select distinct studyver_number, STUDYVER_MAJORVER, STUDYVER_MINORVER from er_studyver where "
            		+ " fk_study=? order by STUDYVER_MAJORVER desc, STUDYVER_MINORVER desc, f_to_number(studyver_number) desc ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);
            
            rs = pstmt.executeQuery();           
            while (rs.next()) {
            	studyverNumber = rs.getString("studyver_number");
            	verList.add(studyverNumber);    
            }           
        } catch(Exception e) {
        	Rlog.fatal("objectMap", "Exception in StudyApndxDao.getLatestStudyVer:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}        	
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}        
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }       
		return verList;		
	}
    
    public ArrayList<String> getStudyLindVersions(int studyId) {		
		
    	PreparedStatement pstmt = null;		
        Connection conn = null;
        ResultSet rs = null;    
        String studyverNumber="";
        ArrayList<String> verList= new  ArrayList<String>();
               
        try {
            conn = getConnection();
            String sql = "select distinct studyver_number, STUDYVER_MAJORVER, STUDYVER_MINORVER from er_studyver where "
            		+ " fk_study=? and studyver_number!='1' order by STUDYVER_MAJORVER desc, STUDYVER_MINORVER desc, f_to_number(studyver_number) desc ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);
            
            rs = pstmt.executeQuery();           
            while (rs.next()) {
            	studyverNumber = rs.getString("studyver_number");
            	verList.add(studyverNumber);    
            }           
        } catch(Exception e) {
        	Rlog.fatal("objectMap", "Exception in StudyApndxDao.getLatestStudyVer:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}        	
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}        
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }       
		return verList;		
	}
    
    public  ArrayList<String> getStudyVerAndApndxDetails(int studyId, String version) {	
    	
    	PreparedStatement pstmt = null;		
        Connection conn = null;
        ResultSet rs = null;       
        String studyapndxFile="";
        String studyapndxFile1="";
        ArrayList<String> verFileList = new ArrayList<String>();        
        
        try {
            conn = getConnection();            
            String sql = "select studyapndx_uri , studyapndx_file, PK_STUDYAPNDX, nvl(length(STUDYAPNDX_FILEOBJ), 0) LEN from er_studyver es , er_studyapndx esa where "
            		+"es.pk_studyver= esa.fk_studyver and es.fk_study=? and studyver_number=? and STUDYAPNDX_TYPE = 'file'";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);
            pstmt.setString(2, version);
            rs = pstmt.executeQuery();           
            while (rs.next()) {            	
            	studyapndxFile = rs.getString("studyapndx_uri");
            	studyapndxFile1 = rs.getString("studyapndx_file");            	
            	verFileList.add(studyapndxFile + " " + studyapndxFile1);
            	this.apndxUris.add(rs.getString("studyapndx_uri"));
            	this.apndxPks.add(rs.getInt("PK_STUDYAPNDX"));
            	this.apndxLens.add(rs.getInt("LEN"));
            }
        } catch(Exception e) {
        	Rlog.fatal("objectMap", "Exception in StudyApndxDao.getStudyVerAndApndxDetails:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}        	
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}        
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }       
		return verFileList;		
	}
    
public  ArrayList<String> getStudyVerAndApndxDetails(int studyId, String version, boolean newVerFlag) {	
    	
    	PreparedStatement pstmt = null;		
        Connection conn = null;
        ResultSet rs = null;       
        String studyapndxFile="";
        String studyapndxFile1="";
        ArrayList<String> verFileList = new ArrayList<String>();        
        
        try {
            conn = getConnection();            
            String sql = "select studyapndx_uri , studyapndx_file, nvl(stamp_flag,0) as stamp_flag, PK_STUDYAPNDX, nvl(length(STUDYAPNDX_FILEOBJ), 0) LEN, FK_STUDYVER from er_studyver es , er_studyapndx esa where "
            		+"es.pk_studyver= esa.fk_studyver and es.fk_study=? and studyver_number=? and STUDYAPNDX_TYPE = 'file' order by PK_STUDYAPNDX";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);
            pstmt.setString(2, version);
            rs = pstmt.executeQuery();  
            System.out.println("SQL--->"+sql);
            System.out.println("studyId--->"+studyId);
            System.out.println("version--->"+version);
            while (rs.next()) {            	
            	studyapndxFile = rs.getString("studyapndx_uri");
            	studyapndxFile1 = rs.getString("studyapndx_file");            	
            	verFileList.add(studyapndxFile + " " + studyapndxFile1);
            	this.apndxUris.add(rs.getString("studyapndx_uri"));
            	this.apndxPks.add(rs.getInt("PK_STUDYAPNDX"));
            	this.apndxLens.add(rs.getInt("LEN"));
            	
            	System.out.println("studyapndxFile===="+studyapndxFile);
            	if(!newVerFlag){
            		this.prevApndxUris.add(studyapndxFile);
            		this.prevStampFlags.add(rs.getInt("stamp_flag"));   
            		this.prevStudyVerIds.add(rs.getInt("FK_STUDYVER"));
            	}else{
            		this.currApndxUris.add(studyapndxFile);
            		this.currStampFlags.add(rs.getInt("stamp_flag"));
            	}
            	
            }
            System.out.println("studyVerIds size-->"+prevStudyVerIds.size());
        } catch(Exception e) {
        	
        	e.printStackTrace();
        	Rlog.fatal("objectMap", "Exception in StudyApndxDao.getStudyVerAndApndxDetails:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}        	
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}        
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }       
		return verFileList;		
	}
    
    public int checkVersionExists(int studyId, String strVer) {		
    	
    	PreparedStatement pstmt = null;		
        Connection conn = null;
        ResultSet rs = null;    
        int pkStudyVer=0;               
        
        try {
            conn = getConnection();
            String sql = "select pk_studyver from er_studyver where fk_study=? and studyver_number=?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);
            pstmt.setString(2, strVer);            
            rs = pstmt.executeQuery();           
            while (rs.next()) {
            	pkStudyVer = rs.getInt("pk_studyver");            	
            }           
        } catch(Exception e) {
        	Rlog.fatal("objectMap", "Exception in StudyApndxDao.getLatestStudyVer:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}        	
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}        
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }       
		return pkStudyVer;		
	}
    
    public int checkVersionExists(int studyId, String strVer, int categoryId) {		
    	
    	PreparedStatement pstmt = null;		
        Connection conn = null;
        ResultSet rs = null;    
        int pkStudyVer=0;               
        
        try {
            conn = getConnection();
            String sql = "select pk_studyver from er_studyver where fk_study=? and studyver_number=? and STUDYVER_CATEGORY=?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);
            pstmt.setString(2, strVer);
            pstmt.setInt(3, categoryId);
            rs = pstmt.executeQuery();           
            while (rs.next()) {
            	pkStudyVer = rs.getInt("pk_studyver");            	
            }           
        } catch(Exception e) {
        	Rlog.fatal("objectMap", "Exception in StudyApndxDao.getLatestStudyVer:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}        	
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}        
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }       
		return pkStudyVer;		
	}
    
    public int getActualRecordsForAttachments(int studyId) {		
    	
    	PreparedStatement pstmt = null;		
        Connection conn = null;
        ResultSet rs = null;    
        int apndxCount=0;          
        
        try {
            conn = getConnection();
            String sql = "select count(*) as apndxCount from er_studyapndx esa, er_studyver es where es.pk_studyver =esa.fk_studyver and es.fk_study=?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);
            
            rs = pstmt.executeQuery();           
            while (rs.next()) {
            	apndxCount = rs.getInt("apndxCount");            	
            }           
        } catch(Exception e) {
        	Rlog.fatal("objectMap", "Exception in StudyApndxDao.getLatestStudyVer:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}        	
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}        
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }       
		return apndxCount;		
	}
    
    private static final String getStudyVerNumberByPkSql = 
    		" select STUDYVER_NUMBER from ER_STUDYVER where PK_STUDYVER = ? ";
    public String getStudyVerNumberByPk(int pkStudyVer) {
    	PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        String studyVerNumber = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(getStudyVerNumberByPkSql);
            pstmt.setInt(1, pkStudyVer);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	studyVerNumber = rs.getString(1);	
            }
        } catch(Exception e) {
        	Rlog.fatal("studyVer", "Exception in StudyVerDao.getStudyVerNumberByPk:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
        return studyVerNumber;
    }
    
    private static final String updateMajorMinorForPkStudySql = 
    		" update ER_STUDYVER set STUDYVER_MAJORVER = ?, STUDYVER_MINORVER = ? where PK_STUDYVER  = ? ";
    public int updateMajorMinorByPk(int pkStudyVer) {
    	int result = 1;
    	/*String versionNumber = getStudyVerNumberByPk(pkStudyVer);
    	if (StringUtil.isEmpty(versionNumber)) {
    		return result;
    	}
    	try {
    		Double.parseDouble(versionNumber);
    	} catch(Exception e) {
    		Rlog.fatal("studyVer", "versionNumber is not a number:"+e);
    		return result;
    	}
    	int majorVer = 1;
    	int minorVer = 0;
    	String[] versionParts = versionNumber.split("\\.");
    	if (versionParts != null && versionParts.length > 0) {
    		try { majorVer = Integer.parseInt(versionParts[0]); } catch(Exception e) {}
    		if (versionParts.length > 1) {
        		try { minorVer = Integer.parseInt(versionParts[1]); } catch(Exception e) {}
    		}
    	}
    	
    	PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(updateMajorMinorForPkStudySql);
            pstmt.setInt(1, majorVer);
            pstmt.setInt(2, minorVer);
            pstmt.setInt(3, pkStudyVer);
            result = pstmt.executeUpdate();
        } catch(Exception e) {
        	Rlog.fatal("studyVer", "Exception in StudyVerDao.updateMajorMinorForPkStudy:"+e);
        	result = 0;
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }*/
        return result;
    }
    // Update studyver_status in er_studyver table
    public int updateVersionStatus(String studyVerNumber, int studyId, int pkStudyVer , String versionStatus ,int usr) {
    	int result = 0;    	   	
    	PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
        	conn = getConnection();           
        	pstmt = conn.prepareStatement("update er_studyver set studyver_status= ?,LAST_MODIFIED_BY=? where pk_studyver = ? and fk_study=? and STUDYVER_NUMBER= ? ");
            pstmt.setString(1, versionStatus);
            pstmt.setInt(2,usr);
            pstmt.setInt(3, pkStudyVer);
            pstmt.setInt(4, studyId);
            pstmt.setString(5, studyVerNumber);           
            result = pstmt.executeUpdate();
        } catch(Exception e) {
        	Rlog.fatal("studyVer", "Exception in StudyVerDao.updateVersionStatus:"+e);
        	result = 0;
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
    	return result;
    }
    public int countLockedVerByCategory(int studyId, String verNum, int studyVercat, int freezeCode) {

    	PreparedStatement pstmt = null; 
    	Connection conn = null;
        int output =0;

            try {

            	conn = getConnection();
    			String Sql = "SELECT MAX(sv.PK_STUDYVER) as LOCKED_VER_PK FROM ER_STUDYVER sv LEFT OUTER JOIN ER_STATUS_HISTORY sh on (sv.PK_STUDYVER = sh.STATUS_MODPK and sh.STATUS_MODTABLE='er_studyver') "+ 
    						 "WHERE FK_STUDY = ? AND STUDYVER_NUMBER = ? AND STUDYVER_CATEGORY = ? AND sh.FK_CODELST_STAT = ? ";
    			pstmt = conn.prepareStatement(Sql);
    			pstmt.setInt(1, studyId);
    			pstmt.setString(2, verNum);
    			pstmt.setInt(3, studyVercat);
    			pstmt.setInt(4, freezeCode);
    			
    			ResultSet rs = pstmt.executeQuery();
    			while (rs.next()) {
    				output = rs.getInt("LOCKED_VER_PK");
    			}

                    } catch (Exception e) {
                    	Rlog.fatal("studyVer", "Exception in countLockedVerByCategory:"+e);
                    }
                return output;
        }
    public void setOldVerDocsVoid(int studyVerPk,int userid){
    	CallableStatement cstmt = null; 
    	Connection conn = null;
    	try {
        	conn = getConnection();
			cstmt = conn.prepareCall("{call PKG_STUDYSTAT.SP_ADD_VOID_STATUS_DOCS(?,?)}");
			cstmt.setInt(1, studyVerPk);
			cstmt.setInt(2, userid);
			cstmt.execute();
    	} catch (Exception e) {
        	Rlog.fatal("studyVer", "Exception in setOldVerDocsVoid:"+e);
        }finally {
        	try { if (cstmt != null) cstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
    }
    public void copyStudySections(int studyId,String oldVerNum,String newVerNum, int userid){
    	CallableStatement cstmt = null; 
    	Connection conn = null;
    	try {
        	conn = getConnection();
			cstmt = conn.prepareCall("{call PKG_STUDYSTAT.SP_COPY_STUDYSECTION(?,?,?,?)}");
			cstmt.setInt(1, studyId);
			cstmt.setString(2, oldVerNum);
			cstmt.setString(3, newVerNum);
			cstmt.setInt(4, userid);
			cstmt.execute();
    	} catch (Exception e) {
    		e.printStackTrace();
        	Rlog.fatal("studyVer", "Exception in copyStudySections:"+e);
        }finally {
        	try { if (cstmt != null) cstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
    }
    public void deleteFreezeStatus(int modPk, String modTable,int userId){
    	CallableStatement cstmt = null; 
    	Connection conn = null;
    	int result =0;
    	try {
        	conn = getConnection();
			cstmt = conn.prepareCall("{call PKG_STUDYSTAT.SP_UNFREEZE_ATTACHMENTS(?,?,?,?)}");
			cstmt.setInt(1, modPk);
			cstmt.setString(2, modTable);
			cstmt.setInt(3, userId);
			cstmt.registerOutParameter(4, java.sql.Types.INTEGER);
			cstmt.execute();
			result=cstmt.getInt(4);
			if(result==0){
				throw new OperationException();
			}
    	} catch (OperationException e){
    		Rlog.fatal("studyVer", "Exception in deleteFreezeStatus:Freeze Status not deleted");
    	}catch (Exception e) {
        	Rlog.fatal("studyVer", "Exception in deleteFreezeStatus:"+e);
        }finally {
        	try { if (cstmt != null) cstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
    }
}
