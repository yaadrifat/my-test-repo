/*
 * Classname			SpecimenStatusBean.class
 *
 * Version information
 *
 * Date					10/15/2007
 *
 * Author:  Jnanamay Majumdar
 *
 * Copyright notice
 */

package com.velos.eres.business.specimenStatus.impl;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import javax.persistence.Transient;

/**
 * The Specimen BMP entity bean.<br>
 * <br>
 *
 * @author Jnanamay Majumdar
 * @version 1.0 10/15/2007
 */
@Entity
@Table(name = "ER_SPECIMEN_STATUS")

public class SpecimenStatusBean implements Serializable {

    private static final long serialVersionUID = 3761410811205333553L;

    /**
     *
     */
    public int pkSpecimenStat;

    /**
     *
     */
    public Integer fkSpecimen;

    /**
     * SS_DATE
     */
    public Date ssDate;

    /**
     * FK_CODELST_STATUS
     */
    public Integer fkCodelstSpecimenStat;


    /**
     * SS_QUANTITY
     */
    public Float ssQuantity;


    /**
     * SS_QUANTITY_UNITS
     */
    public Integer ssQuantityUnits;


    /**
     * SS_ACTION
     */
    public Integer ssAction;



    /**
     * FK_STUDY
     */
    public Integer fkStudy;


    /**
     * FK_USER_RECEPIENT
     */
    public Integer fkUserRecpt;


    /**
     * SS_TRACKING_NUMBER
     */
    public String ssTrackingNum;


    /**
     * SS_NOTES
     */

    public String sSnotes;




    /**
     * SS_STATUS_BY
     */
    public Integer ssStatusBy;


    /**
     * the record creator
     */
    public Integer creator;

    /**
     * last modified by
     */
    public Integer modifiedBy;

    /**
     * the IP Address
     */
    public String ipAdd;

    /**
     * SS_PROC_TYPE
     */
    public Integer ssProcType;


    /**
     * SS_HAND_OFF_DATE
     */
    public Date ssHandOffDate;

    /**
     * SS_PROC_DATE
     */
    public Date ssProcDate;


    // GETTER SETTER METHODS

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_SPECIMEN_STATUS", allocationSize=1)

    @Column(name = "PK_SPECIMEN_STATUS")
    public int getPkSpecimenStat() {
        return this.pkSpecimenStat;
    }

    public void setPkSpecimenStat(int pkSpecimenStat) {
        this.pkSpecimenStat = pkSpecimenStat;
    }

    @Column(name = "FK_SPECIMEN")
    public String getFkSpecimen() {
        return StringUtil.integerToString(this.fkSpecimen);
    }

    public void setFkSpecimen(String fkSpecimen) {
        this.fkSpecimen = StringUtil.stringToInteger(fkSpecimen);
    }


    @Column(name = "SS_DATE")
    public java.util.Date getSsDt() {
    	return ssDate;
    }
    public void setSsDt(java.util.Date ssDate) {
    	this.ssDate = ssDate;
    }

    @Transient
	public String getSsDate(){
		return DateUtil.dateToString(getSsDt(),null);
	}

    public void setSsDate(String Date) {
    	setSsDt(DateUtil.stringToTimeStamp(Date, null));
    }



    @Column(name = "FK_CODELST_STATUS")
    public String getFkCodelstSpecimenStat() {
    	return StringUtil.integerToString(this.fkCodelstSpecimenStat);
    }

    public void setFkCodelstSpecimenStat(String fkCodelstSpecimenStat) {
        this.fkCodelstSpecimenStat = StringUtil.stringToInteger(fkCodelstSpecimenStat);
    }


    @Column(name = "SS_QUANTITY")
    public String getSsQuantity() {
    	return StringUtil.floatToString(this.ssQuantity);
    }

    public void setSsQuantity(String ssQuantity) {
        if (ssQuantity == null || ssQuantity.trim().length() == 0) {
            this.ssQuantity = null;
        } else {
            this.ssQuantity = StringUtil.stringToFloat(ssQuantity);
        }
    }

    @Column(name = "SS_QUANTITY_UNITS")
    public String getSsQuantityUnits() {
    	return StringUtil.integerToString(this.ssQuantityUnits);
    }

    public void setSsQuantityUnits(String ssQuantityUnits) {
        this.ssQuantityUnits = StringUtil.stringToInteger(ssQuantityUnits);
    }

    @Column(name = "SS_ACTION")
    public String getSsAction() {
    	return StringUtil.integerToString(this.ssAction);
    }

    public void setSsAction(String ssAction) {
        this.ssAction = StringUtil.stringToInteger(ssAction);
    }

    @Column(name = "FK_STUDY")
    public String getFkStudy() {
    	return StringUtil.integerToString(this.fkStudy);
    }


    public void setFkStudy(String fkStudy) {
        this.fkStudy = StringUtil.stringToInteger(fkStudy);
    }


    @Column(name = "FK_USER_RECEPIENT")
    public String getFkUserRecpt() {
    	return StringUtil.integerToString(this.fkUserRecpt);
    }


    public void setFkUserRecpt(String fkUserRecpt) {
        this.fkUserRecpt = StringUtil.stringToInteger(fkUserRecpt);
    }


    @Column(name = "SS_TRACKING_NUMBER")
    public String getSsTrackingNum() {
        return this.ssTrackingNum;
    }

    public void setSsTrackingNum(String ssTrackingNum) {
        this.ssTrackingNum = ssTrackingNum;
    }



    @Transient
    public String getSsNotes() {
        return this.sSnotes;
    }

    public void setSsNotes(String sSnotes) {
        this.sSnotes = sSnotes;
    }

    @Column(name = "SS_STATUS_BY")
    public String getSsStatusBy() {
    	return StringUtil.integerToString(this.ssStatusBy);
    }


    public void setSsStatusBy(String ssStatusBy) {
        this.ssStatusBy = StringUtil.stringToInteger(ssStatusBy);
    }


    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }


    //JM: 02Jun2009, #INVP2.5.1(c)
    @Column(name = "SS_PROC_TYPE")
    public String getSsProcType() {
    	return StringUtil.integerToString(this.ssProcType);
    }


    public void setSsProcType(String ssProcType) {
        this.ssProcType = StringUtil.stringToInteger(ssProcType);
    }

    @Column(name = "SS_HAND_OFF_DATE")
    public java.util.Date getSsHandOffDt() {
    	return ssHandOffDate;
    }
    public void setSsHandOffDt(java.util.Date ssHandOffDate) {
    	this.ssHandOffDate = ssHandOffDate;
    }

    @Transient
	public String getSsHandOffDate(){
		return DateUtil.dateToString(getSsHandOffDt(),null);
	}

    public void setSsHandOffDate(String Date) {
    	setSsHandOffDt(DateUtil.stringToTimeStamp(Date, null));
    }


    @Column(name = "SS_PROC_DATE")
    public java.util.Date getSsProcDt() {
    	return ssProcDate;
    }
    public void setSsProcDt(java.util.Date ssProcDate) {
    	this.ssProcDate = ssProcDate;
    }

    @Transient
	public String getSsProcDate(){
		return DateUtil.dateToString(getSsProcDt(),null);
	}

    public void setSsProcDate(String Date) {
    	setSsProcDt(DateUtil.stringToTimeStamp(Date, null));
    }

    ////////////////

    // END OF GETTER SETTER METHODS

    public int updateSpecimenStatus(SpecimenStatusBean spsk) {
        try {



        	setPkSpecimenStat(spsk.getPkSpecimenStat());
        	setFkSpecimen(spsk.getFkSpecimen());
        	setSsDt(spsk.getSsDt());
        	setFkCodelstSpecimenStat(spsk.getFkCodelstSpecimenStat());
        	setSsQuantity(spsk.getSsQuantity());
        	setSsQuantityUnits(spsk.getSsQuantityUnits());
        	setSsAction(spsk.getSsAction());
        	setFkStudy(spsk.getFkStudy());
        	setFkUserRecpt(spsk.getFkUserRecpt());
        	setSsTrackingNum(spsk.getSsTrackingNum());
        	setSsNotes(spsk.getSsNotes());
        	setSsStatusBy(spsk.getSsStatusBy());
        	setCreator(spsk.getCreator());
            setModifiedBy(spsk.getModifiedBy());
            setIpAdd(spsk.getIpAdd());
            setSsProcType(spsk.getSsProcType());
            setSsHandOffDate(spsk.getSsHandOffDate());
            setSsProcDate(spsk.getSsProcDate());
            Rlog.debug("specimenStatus", "SpecimenStatusBean.updateSpecimenStatus");
            return 0;
        } catch (Exception e) {

            Rlog.fatal("specimenStatus", " error in SpecimenStatusBean.updateSpecimenStatus");
            return -2;
        }
    }

    public SpecimenStatusBean() {

    }

    public SpecimenStatusBean(int pkSpecimenStat, String fkSpecimen, String ssDate,Date ssDt, String fkCodelstSpecimenStat,  String ssQuantity,
    		String ssQuantityUnits,  String ssAction,  String fkStudy, String fkUserRecpt,  String ssTrackingNum,  String ssNotes,String ssStatusBy,
    		String creator, String modifiedBy, String ipAdd, String ssProcType,
    		String ssHandOffDate, String ssProcDate) {
        super();
        // TODO Auto-generated constructor stub


        setPkSpecimenStat(pkSpecimenStat);
    	setFkSpecimen(fkSpecimen);
    	if(ssDt!=null){
    		setSsDt(ssDt);
    	}
    	else if(ssDate !=null){
    		if(ssDate.contains(":")){
    			setSsDate(ssDate);
    		}else{
    			setSsDt(DateUtil.stringToTimeStamp(ssDate,DateUtil.getAppDateFormat()));
    		//setSsDate(ssDate);
    		}
    	}else{
    		setSsDt(DateUtil.stringToDate(DateUtil.getCurrentDate(),DateUtil.getAppDateFormat()));
    	}
    	setFkCodelstSpecimenStat(fkCodelstSpecimenStat);
    	setSsQuantity(ssQuantity);
    	setSsQuantityUnits(ssQuantityUnits);
    	setSsAction(ssAction);
    	setFkStudy(fkStudy);
    	setFkUserRecpt(fkUserRecpt);
    	setSsTrackingNum(ssTrackingNum);
    	setSsNotes(ssNotes);
    	setSsStatusBy(ssStatusBy);
    	setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setSsProcType(ssProcType);
        setSsHandOffDate(ssHandOffDate);
        setSsProcDate(ssProcDate);
    }

}


