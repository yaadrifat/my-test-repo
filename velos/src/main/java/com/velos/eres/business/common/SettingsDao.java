package com.velos.eres.business.common;

import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.GenerateId;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * This class is an Interface to eResearch settings. Setting are stored,based on
 * module name,module number and keyword.
 * 
 * @author vishal
 * @created March 18, 2005
 */

public class SettingsDao extends CommonDAO implements java.io.Serializable {

    private ArrayList settingKeyword;

    private ArrayList settingValue;

    private ArrayList settingPK;

    private ArrayList settingsModName;

    private ArrayList settingsModNum;
    
    private ArrayList settingCreator;//KM
    
    private ArrayList settingIpadd;
    
    private ArrayList settingModifiedBy;

    /**
     * Constructor for the SettingsDao object
     */
    public SettingsDao() {
        settingKeyword = new ArrayList();
        settingValue = new ArrayList();
        settingPK = new ArrayList();
        settingsModName = new ArrayList();
        settingsModNum = new ArrayList();
        settingCreator = new ArrayList();
        settingIpadd = new ArrayList();
        settingModifiedBy = new ArrayList();
    }

    // Start getter/setter methods
    /**
     * Returns the value of settingKeyword.
     * 
     * @return The settingKeyword value
     */
    public ArrayList getSettingKeyword() {
        return settingKeyword;
    }

    /**
     * Sets the value of settingKeyword.
     * 
     * @param settingKeyword
     *            The value to assign settingKeyword.
     */
    public void setSettingKeyword(ArrayList settingKeyword) {
        this.settingKeyword = settingKeyword;
    }

    /**
     * Sets the value of settingKeyword.
     * 
     * @param settingKeyword
     *            The value to assign settingKeyword.
     */
    public void setSettingKeyword(String settingKeyword) {
        this.settingKeyword.add(settingKeyword);
    }

    /**
     * Returns the value of settingValue.
     * 
     * @return The settingValue value
     */
    public ArrayList getSettingValue() {
        return settingValue;
    }

    /**
     * Sets the value of settingValue.
     * 
     * @param settingValue
     *            The value to assign settingValue.
     */
    public void setSettingValue(ArrayList settingValue) {
        this.settingValue = settingValue;
    }

    /**
     * Sets the value of settingValue.
     * 
     * @param settingValue
     *            The value to assign settingValue.
     */
    public void setSettingValue(String settingValue) {
        this.settingValue.add(settingValue);
    }

    /**
     * Returns the value of settingPK.
     * 
     * @return The settingPK value
     */
    public ArrayList getSettingPK() {
        return settingPK;
    }

    /**
     * Sets the value of settingPK.
     * 
     * @param settingPK
     *            The value to assign settingPK.
     */
    public void setSettingPK(ArrayList settingPK) {
        this.settingPK = settingPK;
    }

    /**
     * Sets the value of settingPK.
     * 
     * @param settingPK
     *            The value to assign settingPK.
     */
    public void setSettingPK(String settingPK) {
        this.settingPK.add(settingPK);
    }

    /**
     * Returns the value of settingsModName.
     * 
     * @return The settingsModName value
     */
    public ArrayList getSettingsModName() {
        return settingsModName;
    }

    /**
     * Sets the value of settingsModName.
     * 
     * @param settingsModName
     *            The value to assign settingsModName.
     */
    public void setSettingsModName(ArrayList settingsModName) {
        this.settingsModName = settingsModName;
    }

    /**
     * Sets the value of settingsModName.
     * 
     * @param settingsModName
     *            The value to assign settingsModName.
     */
    public void setSettingsModName(String settingsModName) {
        this.settingsModName.add(settingsModName);
    }

    /**
     * Returns the value of settingsModNum.
     * 
     * @return The settingsModNum value
     */
    public ArrayList getSettingsModNum() {
        return settingsModNum;
    }

    /**
     * Sets the value of settingsModNum.
     * 
     * @param settingsModNum
     *            The value to assign settingsModNum.
     */
    public void setSettingsModNum(ArrayList settingsModNum) {
        this.settingsModNum = settingsModNum;
    }

    /**
     * Sets the value of settingsModNum.
     * 
     * @param settingsModNum
     *            The value to assign settingsModNum.
     */
    public void setSettingsModNum(String settingsModNum) {
        this.settingsModNum.add(settingsModNum);
    }

    
    
    //KM
    
    /**
     * Returns the value of settingCreator.
     * 
     * @return The settingCreator value
     */
    public ArrayList getSettingCreator() {
        return settingCreator;
    }

    /**
     * Sets the value of settingsCreator.
     * 
     * @param settingsCreator
     *            The value to assign settingsCreator.
     */
    public void setSettingCreator(ArrayList settingCreator) {
        this.settingCreator = settingCreator;
    }

    /**
     * Sets the value of settingsCreator.
     * 
     * @param settingsCreator
     *            The value to assign settingsCreator.
     */
    public void setSettingCreator(String settingCreator) {
        this.settingCreator.add(settingCreator);
    }
    
    
    /**
     * Returns the value of settingIpadd.
     * 
     * @return The settingIpadd value
     */
    public ArrayList getSettingIpadd() {
        return settingIpadd;
    }

    /**
     * Sets the value of settingIpadd.
     * 
     * @param settingIpadd
     *            The value to assign settingIpadd.
     */
    public void setSettingIpadd(ArrayList settingIpadd) {
        this.settingIpadd = settingIpadd;
    }

    /**
     * Sets the value of settingIpadd.
     * 
     * @param settingIpadd
     *            The value to assign settingIpadd.
     */
    public void setSettingIpadd(String settingIpadd) {
        this.settingIpadd.add(settingIpadd);
    }
    
    /**
     * Returns the value of settingModifiedBy.
     * 
     * @return The settingModifiedBy value
     */
    public ArrayList getSettingModifiedBy() {
        return settingModifiedBy;
    }

    /**
     * Sets the value of settingModifiedBy.
     * 
     * @param settingModifiedBy
     *            The value to assign settingModifiedBy.
     */
    public void setSettingModifiedBy(ArrayList settingModifiedBy) {
        this.settingModifiedBy = settingModifiedBy;
    }

    /**
     * Sets the value of settingModifiedBy.
     * 
     * @param settingModifiedBy
     *            The value to assign settingModifiedBy.
     */
    public void setSettingModifiedBy(String settingModifiedBy) {
        this.settingModifiedBy.add(settingModifiedBy);
    }
    
    // end getter/setter methods

    /**
     * Sends the data to the database in a batch and inserts new records.
     * 
     * @return Return the update status. 1 if update was successful and -1 if an
     *         error.
     */

    public int insertSettings() {
        Connection connection = null;
        PreparedStatement pstmt = null;
        int id = -1;
        try {
            // Disable auto-commit
            connection = getConnection();
            connection.setAutoCommit(false);

            // Create a prepared statement
            String sql = "INSERT INTO er_settings(settings_pk,settings_modnum, "
                    + " settings_modname,settings_keyword,settings_value) VALUES(?,?,?,?,?)";
            pstmt = connection.prepareStatement(sql);

            for (int i = 0; i < this.getSettingKeyword().size(); i++) {
                id = GenerateId.getId("seq_er_settings", connection);
                pstmt.setInt(1, id);
                pstmt.setString(2, (String) this.getSettingsModNum().get(i));
                pstmt.setString(3, (String) this.getSettingsModName().get(i));
                pstmt.setString(4, (String) this.getSettingKeyword().get(i));
                pstmt.setString(5, (String) this.getSettingValue().get(i));
                pstmt.addBatch();
            }

            // Execute the batch
            int[] updateCounts = pstmt.executeBatch();

            // processUpdateCounts(updateCounts);
            connection.commit();
            return 1;
        } catch (BatchUpdateException e) {
            // Not all of the statements were successfully executed
            int[] updateCounts = e.getUpdateCounts();
            Rlog.fatal("common",
                    "SettingsDao:InsertSettings:Error in Inserting settings.");
            return -1;
            // processUpdateCounts(updateCounts);
            // connection.rollback();
        } catch (SQLException e) {
            return -1;
        }finally{
            try { if (pstmt != null) { pstmt.close(); } } catch(Exception e) {}
            try{
                if (connection!=null) connection.close();
            }catch(Exception e){
                
            }
        }
    }

    /**
     * Sends the data to the database in a batch and inserts new records.
     * 
     * @return Return the update status. 1 if update was successful and -1 if an
     *         error.
     */

    public int updateSettings() {
        Connection connection = null;
        PreparedStatement pstmt = null;
        int id = -1;
        try {
            // Disable auto-commit
            connection = getConnection();
            connection.setAutoCommit(false);

            // Create a prepared statement
            String sql = "update er_settings set settings_modnum=?, "
                    + " settings_modname=?,settings_keyword=?,settings_value=? where settings_pk=?";
            pstmt = connection.prepareStatement(sql);
            for (int i = 0; i < this.getSettingKeyword().size(); i++) {
                pstmt.setString(1, (String) this.getSettingsModNum().get(i));
                pstmt.setString(2, (String) this.getSettingsModName().get(i));
                pstmt.setString(3, (String) this.getSettingKeyword().get(i));
                pstmt.setString(4, (String) this.getSettingValue().get(i));
                pstmt.setInt(5, StringUtil.stringToNum((String) getSettingPK()
                        .get(i)));
                pstmt.addBatch();
            }

            // Execute the batch
            int[] updateCounts = pstmt.executeBatch();

            // processUpdateCounts(updateCounts);
            connection.commit();
            return 1;
        } catch (BatchUpdateException e) {
            // Not all of the statements were successfully executed
            int[] updateCounts = e.getUpdateCounts();
            Rlog.fatal("common",
                    "SettingsDao:InsertSettings:Error in Inserting settings.");
            return -1;
            // processUpdateCounts(updateCounts);
            // connection.rollback();
        } catch (SQLException e) {
            return -1;
        }finally{
            try{ if (pstmt != null) { pstmt.close(); } } catch(Exception e) {}
            try{
                if (connection!=null) connection.close();
            }catch(Exception e){
                
            }
        }
    }

    /**
     * Sends the data to the database non-batch mode by keyword.
     * 
     * @return Return the update status. 1 if update was successful and -1 if an
     *         error.
     */

    public int saveSettings() {
        Connection connection = null;
        int id = -1;
        String pkStr = "";
        String sql = "";
        int settingsPk = -1;
        PreparedStatement pstmt = null;
        try {
            // Disable auto-commit
            connection = getConnection();
            connection.setAutoCommit(false);
            Rlog.debug("common", "SAveSettings:size of arrayList"
                    + this.getSettingPK() + "\nModnum"
                    + this.getSettingsModNum() + "\nodname"
                    + this.getSettingsModName() + "\nKeyword"
                    + this.getSettingKeyword() + "\nvalue"
                    + this.getSettingValue());
            for (int i = 0; i < this.getSettingPK().size(); i++) {
                pkStr = (String) this.getSettingPK().get(i);
                settingsPk = (pkStr == null) ? 0 : StringUtil.stringToNum(pkStr);
                Rlog.debug("common", "SettingsDao:saveSettings:settingsPk"
                        + settingsPk + " @Index " + i);
                if (settingsPk == 0) {
                	sql = "INSERT INTO er_settings(settings_pk,settings_modnum, "
                            + " settings_modname,settings_keyword,settings_value,creator,ip_add) VALUES(?,?,?,?,?,?,?)";
                    pstmt = connection.prepareStatement(sql);
                    id = GenerateId.getId("seq_er_settings", connection);
                    pstmt.setInt(1, id);
                    pstmt
                            .setString(2, (String) this.getSettingsModNum()
                                    .get(i));
                    pstmt.setString(3, (String) this.getSettingsModName()
                            .get(i));
                    pstmt
                            .setString(4, (String) this.getSettingKeyword()
                                    .get(i));
                    pstmt.setString(5, (String) this.getSettingValue().get(i));
                    
                    //KM
                    pstmt.setString(6,(String) this.getSettingCreator().get(i));
                    pstmt.setString(7,(String) this.getSettingIpadd().get(i));
                                       
                    pstmt.executeUpdate();
                    
                } else {

                    sql = "update er_settings set settings_modnum=?, "
                            + " settings_modname=?,settings_keyword=?,settings_value=?,last_modified_by=?,ip_add=? where settings_pk=?";
                    pstmt = connection.prepareStatement(sql);
                    pstmt
                            .setString(1, (String) this.getSettingsModNum()
                                    .get(i));
                    pstmt.setString(2, (String) this.getSettingsModName()
                            .get(i));
                    pstmt
                            .setString(3, (String) this.getSettingKeyword()
                                    .get(i));
                    pstmt.setString(4, (String) this.getSettingValue().get(i));
                    
                    //KM
                    pstmt.setString(5,(String) this.getSettingModifiedBy().get(i));
                    pstmt.setString(6,(String) this.getSettingIpadd().get(i));
                    pstmt.setInt(7, settingsPk);
                    pstmt.executeUpdate();

                }
            }

            connection.commit();
            return 1;
        } catch (SQLException e) {
            // Not all of the statements were successfully executed

            Rlog.fatal("common",
                    "SettingsDao:InsertSettings:Error in savesettings.");
            return -1;
        }finally{
            try{ if (pstmt != null) { pstmt.close(); } } catch(Exception e) {}
            try{
                if (connection!=null) connection.close();
            }catch(Exception e){
                
            }
        }
    }

    /**
     * Retrieve the setting based on module number,module name and keyword
     * 
     * @param modnum
     *            Module Number
     * @param modname
     *            Module Name
     * @param keyword
     *            Keyword of the setting
     */
    public void retrieveSettings(int modnum, int modname, String keyword) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "select settings_pk,"
                + "settings_value from er_settings where "
                + " settings_modname=? and settings_modnum=? "
                + " and settings_keyword=?";

        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, modname);
            pstmt.setInt(2, modnum);
            pstmt.setString(3, keyword);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setSettingPK(rs.getString("settings_pk"));
                setSettingValue(rs.getString("settings_value"));

            }

        } catch (SQLException ex) {
            Rlog.fatal("common",
                    "SettingsDoa.RetrieveSettings EXCEPTION IN Fetch" + ex);

        } finally {
        	try { 
        	    if (pstmt != null) { pstmt.close(); }
        	} catch(Exception e) {}
        	try {
        	    if (conn != null) { conn.close(); }
        	} catch(Exception e) {}

        }

    }

    /**
     * Retrieve the setting based on module number,module name and keyword
     * 
     * @param modnum
     *            Module Number
     * @param modname
     *            Module Name
     * @param keywordList
     *            Description of the Parameter
     */
    public void retrieveSettings(int modnum, int modname, ArrayList keywordList) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        String keywordStr = "";
        int size = keywordList.size();
        if (size <= 0) {
            return;
        }
        for (int i = 0; i < size; i++) {
            if (keywordStr.length() == 0) {
                keywordStr = "'" + (String) keywordList.get(i) + "'";
            } else if (keywordStr.length() > 0) {
                keywordStr = keywordStr + ",'" + (String) keywordList.get(i)
                        + "'";
            }
        }
        String sql = "select settings_pk,"
                + "settings_value,settings_keyword from er_settings where "
                + " settings_modname=? and settings_modnum=? "
                + " and settings_keyword in (" + keywordStr + ")";

        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, modname);
            pstmt.setInt(2, modnum);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setSettingPK(rs.getString("settings_pk"));
                setSettingValue(rs.getString("settings_value"));
                setSettingKeyword(rs.getString("settings_keyword"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("common",
                    "SettingsDoa.RetrieveSettings EXCEPTION IN Fetch" + ex);

        } finally {
        	try { 
        	    if (pstmt != null) { pstmt.close(); }
        	} catch(Exception e) {}
        	try {
        	    if (conn != null) { conn.close(); }
        	} catch(Exception e) {}

        }

    }

    /**
     * Retrieve the setting based on module number and module name
     * 
     * @param modname
     *            Module Number
     * @param modnum
     *            Module Name
     */
    public void retrieveSettings(int modnum, int modname) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "select settings_pk,settings_keyword,"
                + "settings_value from er_settings where "
                + " settings_modname=? and settings_modnum=? ";
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, modname);
            pstmt.setInt(2, modnum);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {

                setSettingPK(rs.getString("settings_pk"));
                setSettingValue(rs.getString("settings_value"));
                setSettingKeyword(rs.getString("settings_keyword"));

            }

        } catch (SQLException ex) {
            Rlog.fatal("common",
                    "SettingsDoa.RetrieveSettings EXCEPTION IN Fetch" + ex);

        } finally {
        	try { 
        	    if (pstmt != null) { pstmt.close(); }
        	} catch(Exception e) {}
        	try {
        	    if (conn != null) { conn.close(); }
        	} catch(Exception e) {}

        }

    }

    /**
     * Purge the setting based on Velos Internal Id
     * 
     * @param purgeId
     *            Internal Id of the setting to delete
     * @return Description of the Return Value
     */
    public int purgeSettings(int purgeId) {
        int ret = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "delete from er_settings where settings_pk=?";
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, purgeId);
            ret = pstmt.executeUpdate();

        } catch (SQLException ex) {
            Rlog.fatal("common",
                    "SettingsDoa.RetrieveSettings EXCEPTION IN Fetch" + ex);
            return -1;
        } finally {
        	try { 
        	    if (pstmt != null) { pstmt.close(); }
        	} catch(Exception e) {}
        	try {
        	    if (conn != null) { conn.close(); }
        	} catch(Exception e) {}

        }

        return ret;
    }

    /**
     * Purge the setting based on Velos Internal Id's
     * 
     * @param purgeList
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public int purgeSettings(ArrayList purgeList) {
        int ret = 0;
        int len = purgeList.size();
        String delStr = "";
        for (int i = 0; i < len; i++) {
            if (delStr.length() == 0) {
                delStr = "(" + (String) purgeList.get(i);
            } else {
                delStr = delStr + "," + (String) purgeList.get(i);
            }
        }
        // length 1 is checked because one character '(' is added
        if (delStr.length() > 1) {
            delStr = delStr + ")";
        }
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "delete from er_settings where settings_pk in " + delStr;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            ret = pstmt.executeUpdate();
        } catch (SQLException ex) {
            Rlog.fatal("common", "SettingsDoa.purgeSettings EXCEPTION IN Fetch"
                    + ex.getMessage());
            return -1;
        } finally {
        	try { 
        	    if (pstmt != null) { pstmt.close(); }
        	} catch(Exception e) {}
        	try {
        	    if (conn != null) { conn.close(); }
        	} catch(Exception e) {}

        }
        return ret;
    }

    /**
     * Remove the records from settings based on the combination of module
     * number,module name,keyword and value
     * 
     * @param modnum
     *            ID for that record in specified module
     * @param modname
     *            Module name for which rows are deleted
     * @param keyword
     *            Keyword
     * @param value
     *            Value of keyword
     * @return return the status, -1 if an error
     */
    public int purgeSettings(String modnum, String modname, String keyword,
            String value) {
        int ret = 0;
        String delStr = "";
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "delete from er_settings where settings_modnum=? and settings_modname=? "
                + " and settings_keyword=? and settings_value=? ";
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, modnum);
            pstmt.setString(2, modname);
            pstmt.setString(3, keyword);
            pstmt.setString(4, value);
            ret = pstmt.executeUpdate();
        } catch (SQLException ex) {
            Rlog.fatal("common", "SettingsDoa.purgeSettings EXCEPTION IN Fetch"
                    + ex.getMessage());
            return -1;
        } finally {
        	try { 
        	    if (pstmt != null) { pstmt.close(); }
        	} catch(Exception e) {}
        	try {
        	    if (conn != null) { conn.close(); }
        	} catch(Exception e) {}

        }
        return ret;
    }

    /**
     * Remove the records from settings based on the combination of module
     * number,module name and keyword
     * 
     * @param modnum
     *            ID for that record in specified module
     * @param modname
     *            Module name for which rows are deleted
     * @param keyword
     *            Keyword
     * @return return the status, -1 if an error
     */
    public int purgeSettings(String modnum, String modname, String keyword) {
        int ret = 0;
        String delStr = "";
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "delete from er_settings where settings_modnum=? and settings_modname=? "
                + " and settings_keyword=?";
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, modnum);
            pstmt.setString(2, modname);
            pstmt.setString(3, keyword);
            ret = pstmt.executeUpdate();
        } catch (SQLException ex) {
            Rlog.fatal("common", "SettingsDoa.purgeSettings EXCEPTION IN Fetch"
                    + ex.getMessage());
            return -1;
        } finally {
        	try { 
        	    if (pstmt != null) { pstmt.close(); }
        	} catch(Exception e) {}
        	try {
        	    if (conn != null) { conn.close(); }
        	} catch(Exception e) {}

        }
        return ret;
    }

    /**
     * Remove the records from settings based on the combination of module
     * number,module name and keyword
     * 
     * @param modnum
     *            ID for that record in specified module
     * @param modname
     *            Module name for which rows are deleted
     * @param keywordList
     *            ArrayList of keywords
     * @return return the status, -1 if an error
     */
    public int purgeSettings(String modnum, String modname,
            ArrayList keywordList) {
        int ret = 0, len = 0;
        String keywordStr = "";
        len = keywordList.size();
        for (int i = 0; i < len; i++) {
            if (keywordStr.length() == 0) {
                keywordStr = "'" + (String) keywordList.get(i) + "'";
            } else {
                keywordStr = keywordStr + ",'" + (String) keywordList.get(i)
                        + "'";
            }
        }
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "delete from er_settings where settings_modnum=? and settings_modname=? "
                + " and settings_keyword in(" + keywordStr + ")";
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, modnum);
            pstmt.setString(2, modname);
            ret = pstmt.executeUpdate();
        } catch (SQLException ex) {
            Rlog.fatal("common", "SettingsDoa.purgeSettings EXCEPTION IN Fetch"
                    + ex.getMessage());
            return -1;
        } finally {
        	try { 
        	    if (pstmt != null) { pstmt.close(); }
        	} catch(Exception e) {}
        	try {
        	    if (conn != null) { conn.close(); }
        	} catch(Exception e) {}

        }
        return ret;
    }

    /**
     * Purge the setting based on Velos Internal Id's
     * 
     * @return Description of the Return Value
     */
    public int purgeSettings() {
        int ret = 0;
        String delStr = "";
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "delete from er_settings where settings_modnum=? and settings_modname=? "
                + " and settings_keyword=? and settings_value=? ";
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            for (int i = 0; i < this.getSettingKeyword().size(); i++) {
                pstmt.setString(1, (String) this.getSettingsModNum().get(i));
                pstmt.setString(2, (String) this.getSettingsModName().get(i));
                pstmt.setString(3, (String) this.getSettingKeyword().get(i));
                pstmt.setString(4, (String) this.getSettingValue().get(i));
                // ret= pstmt.executeUpdate();
                pstmt.addBatch();
            }
            // Execute the batch
            int[] updateCounts = pstmt.executeBatch();

            conn.commit();
            return 1;
        } catch (BatchUpdateException e) {
            Rlog.fatal("common", "SettingsDoa.purgeSettings EXCEPTION IN Fetch"
                    + e.getMessage());
            return -1;
        } catch (SQLException e) {
            Rlog.fatal("common", "SettingsDoa.purgeSettings EXCEPTION IN Fetch"
                    + e.getMessage());
            return -1;
        } finally {

        	try { 
        	    if (pstmt != null) { pstmt.close(); }
        	} catch(Exception e) {}
        	try {
        	    if (conn != null) { conn.close(); }
        	} catch(Exception e) {}

        }

    }

    /**
     * Reinitialize all the ArrayLists in this object
     */
    public void reset() {
        settingKeyword = new ArrayList();
        settingValue = new ArrayList();
        settingPK = new ArrayList();
        settingsModName = new ArrayList();
        settingsModNum = new ArrayList();
    }

    /**
     * Description of the Method
     */
    public void display() {
        System.out.println("settingKeyword" + settingKeyword + "\nsettingValue"
                + settingValue + "\nsettingPK" + settingPK
                + "\nsettingsModName" + settingsModName + "\nsettingsModNum"
                + settingsModNum);
    }

    //Added by Gopu for July-August'06 Enhancement (U2) - Default account level Admin settings for User / Patient Timezone, Seesion Timeout time, Number of days password will expire, Number of days e-Sign will expire 
    /**
     * Sends the data to the database in a batch and inserts new records.
     * 
     * @return Return the update status. 1 if update was successful and -1 if an
     *         error.
     */


	public int updateSettingsWithoutPK() {
        Connection connection = null;
        PreparedStatement pstmt = null;
        int id = -1;
        try {
            // Disable auto-commit
            connection = getConnection();
            connection.setAutoCommit(false);

            // Create a prepared statement
            String sql = "update er_settings set settings_value=?, LAST_MODIFIED_BY=? where settings_modnum=? and "
                    + " settings_modname=? and settings_keyword=?";
            pstmt = connection.prepareStatement(sql);
            for (int i = 0; i < this.getSettingKeyword().size(); i++) {
            	pstmt.setString(1, (String) this.getSettingValue().get(i));
            	pstmt.setInt(2, StringUtil.stringToNum((String) this.getSettingModifiedBy().get(0))); //Modified By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.
            	pstmt.setString(3, (String) this.getSettingsModNum().get(i));
                pstmt.setString(4, (String) this.getSettingsModName().get(i));
                pstmt.setString(5, (String) this.getSettingKeyword().get(i));
                pstmt.addBatch();
            }

            // Execute the batch
            int[] updateCounts = pstmt.executeBatch();

            // processUpdateCounts(updateCounts);
            connection.commit();
            return 1;
        } catch (BatchUpdateException e) {
            // Not all of the statements were successfully executed
            int[] updateCounts = e.getUpdateCounts();
            Rlog.fatal("common",
                    "SettingsDao:updateSettingsWithoutPK:Error in update Admin Settings.");
            return -1;
            // processUpdateCounts(updateCounts);
            // connection.rollback();
        } catch (SQLException e) {
				Rlog.fatal("common", "SettingsDoa.updateSettingsWithoutPK EXCEPTION IN Fetch"
                    + e.getMessage());
            return -1;
        }finally{
            try{ if (pstmt != null) { pstmt.close(); } } catch(Exception e) {}
            try{
                if (connection!=null) connection.close();
            }catch(Exception e){
                
            }
        }
    }
	
	public void getSettingsValue(String settingKeyword,int accountId, int groupId, int userId) {
		PreparedStatement pstmt = null;
	    Connection conn = null;
	    String setValue="";
	    String [] keywords=null;
	    ArrayList keywordList=null;
		try{  
			 conn = getConnection();
			 int split=settingKeyword.indexOf(",");
			 
			 if (split>-1)
				 
			 {
				 pstmt = conn.prepareStatement(" select settings_modname,settings_modnum,settings_value from er_settings where " 
							+ " ((settings_modnum = ? and settings_modname=1)"
							+ " OR (settings_modnum = ? and settings_modname=2)"
							+ " OR (settings_modnum = ? and settings_modname=4))"
							+ " and settings_keyword in ( "+settingKeyword+")");
			
				 
			 }
			 else { 
			pstmt = conn.prepareStatement(" select settings_modname,settings_modnum,settings_value from er_settings where " 
							+ " ((settings_modnum = ? and settings_modname=1)"
							+ " OR (settings_modnum = ? and settings_modname=2)"
							+ " OR (settings_modnum = ? and settings_modname=4))"
							+ " and settings_keyword= ? ");
			 }
			
			pstmt.setInt(1,accountId);
			pstmt.setInt(2,userId);
			pstmt.setInt(3,groupId);
			 if (split<0) 	pstmt.setString(4,settingKeyword);
			
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next()){		
				setValue=rs.getString("settings_value");
				if(!StringUtil.isEmpty(setValue)) {
					setSettingValue(setValue);
					setSettingsModName(rs.getString("settings_modname"));
					setSettingsModNum(rs.getString("settings_modnum"));
				}
				
			}
			
		}
		catch(Exception e){
			Rlog.fatal("In SettingsDao.getSettingsValue","exception "+e);
        	e.printStackTrace();		
		}
		finally {
            try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
            try { if (conn != null) { conn.close(); } } catch (Exception e) {}
        }
	}
	
	public String getOrderedSetting(String order)
	{
		//Return if Nothing is retrieved
		 if (this.getSettingsModName().size()==0) return "";
		 int index=-1;
		 String settingValue="",nameCode="";
		 
		 ArrayList orderList=null;
		 ArrayList orderNumList=null;
		 String orderStr=StringUtil.trueValue(order);
		 
		 if (orderStr.length()==0) orderStr="U,G,A";
		 
		 orderList=StringUtil.strArrToArrayList(orderStr.split(","));
		 
		 for (int i=0;i<orderList.size();i++)
		 {
			//orderNumList.add(getSettingModNameCode((String)orderList.get(i)));
			 nameCode=new Integer(getSettingModNameCode((String)orderList.get(i))).toString();
			index=this.getSettingsModName().indexOf(nameCode);
			if (index>=0) { 
				settingValue=(String)this.getSettingValue().get(index);
				break;
			}
			
			
		 }
		 
		  
		 
		 	 
		 
		return settingValue;
	}
	
	
	public int getSettingModNameCode(String settingStr){
		int modName=0;
		String 
		lSetting=(settingStr==null)?"":settingStr;
		
		try{
			
				if (settingStr.equals("A")){ modName=1;}
				if (settingStr.equals("U")){ modName=2;}
				if (settingStr.equals("S")){ modName=3;}
				if (settingStr.equals("G")){ modName=4;}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return modName;
	}
	
    
    
}
