/*
 * Classname : StudySiteBean
 * 
 * Version information: 1.0
 *
 * Date: 10/27/2004
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.business.studySite.impl;

/* Import Statements */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The StudySite CMP entity bean.<br>
 * <br>
 * 
 * @author Anu
 * @vesion 1.0 08/09/2001
 * @ejbHome StudySiteHome
 * @ejbRemote StudySiteRObj
 */
/*
 * Modified by Sonia Abrol, 03/22/05, for a new attribute
 * currentSitePatientCount - The number of patients enrolled (with any status)
 * for the study -site combination
 */
/*Modified by Sonia Abrol, 03/06/06, for study site site enrollment settings attributes */

@Entity
@Table(name = "er_studysites")
@NamedQuery(name = "findByStudySite", query = "SELECT OBJECT(site) FROM StudySiteBean site where fk_study = :fk_study AND fk_site = :fk_site")
public class StudySiteBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3834031338016880691L;

    /**
     * the Study Site Id
     */
    public int studySiteId;

    /**
     * the study Id
     */
    public Integer studyId;

    /**
     * the site id
     */

    public Integer siteId;

    /**
     * the study Site type
     */

    public Integer studySiteType;

    /**
     * the Study Site sample size
     */

    public String sampleSize;

    /**
     * the study Site Public
     */

    public Integer studySitePublic;

    /**
     * the study Site report include flag
     */

    public Integer repInclude;

    /**
     * creator
     */
    public Integer creator;

    /**
     * last modified by
     */
    public Integer modifiedBy;

    /**
     * IP Address
     */
    public String ipAdd;

    /**
     * The number of patients enrolled (with any status) for the study -site
     * combination
     */

    public String currentSitePatientCount;
    
    
    /** If Enrollment is review based for this site*/
    public Integer isReviewBasedEnrollment;
    
    
    /**
     * List of user PKs; these users will receive the notification if a new 'enrolled' status is added
     */
    public String sendEnrNotificationTo;

    /**
     * List of user PKs; these users will receive the notification if a new enrollment status related status is added.
     * example - 'enrollment approved','enrollment denied'
     */
    
    public String sendEnrStatNotificationTo;
	
	public String siteSiteId;


    // GETTER SETTER METHODS



	public StudySiteBean() {

    }

    public StudySiteBean(int studySiteId, String studyId, String siteId,
            String studySiteType, String sampleSize, String studySitePublic,
            String repInclude, String creator, String modifiedBy, String ipAdd,
            String currentSitePatientCount, String enrType, String notifyEnrTo, String notifyEnrStatTo) {
        super();
        // TODO Auto-generated constructor stub
        setStudySiteId(studySiteId);
        setStudyId(studyId);
        setSiteId(siteId);
        setStudySiteType(studySiteType);
        setSampleSize(sampleSize);
        setStudySitePublic(studySitePublic);
        setRepInclude(repInclude);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setCurrentSitePatientCount(currentSitePatientCount);
        setIsReviewBasedEnrollment(enrType);
    	setSendEnrNotificationTo(notifyEnrTo);
    	setSendEnrStatNotificationTo(notifyEnrStatTo);
    }

    /**
     * 
     * Get study site Id
     * 
     * @return the studySiteId
     */

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_STUDYSITES", allocationSize=1)
    @Column(name = "PK_STUDYSITES")
    public int getStudySiteId() {
        return this.studySiteId;
    }

    public void setStudySiteId(int id) {
        this.studySiteId = id;
    }

    /**
     * Get study Id
     * 
     * @return studyId
     */
    @Column(name = "FK_STUDY")
    public String getStudyId() {
        return StringUtil.integerToString(this.studyId);
    }

    /**
     * sets Study Id
     * 
     * @param studyId
     */
    public void setStudyId(String studyId) {
        this.studyId = StringUtil.stringToInteger(studyId);
    }

    /**
     * Get site Id
     * 
     * @return siteId
     */

    @Column(name = "FK_SITE")
    public String getSiteId() {
        return StringUtil.integerToString(this.siteId);
    }

    /**
     * sets Site Id
     * 
     * @param siteId
     */
    public void setSiteId(String siteId) {
        this.siteId = StringUtil.stringToInteger(siteId);
    }

    /**
     * Get Study site type
     * 
     * @return study Site Type
     */

    @Column(name = "FK_CODELST_STUDYSITETYPE")
    public String getStudySiteType() {
        return StringUtil.integerToString(this.studySiteType);
    }

    /**
     * sets Site Id
     * 
     * @param site
     *            type
     */
    public void setStudySiteType(String studySiteType) {
        this.studySiteType = StringUtil.stringToInteger(studySiteType);
    }

    /**
     * Get Study site Sample Size
     * 
     * @return String sampleSize
     */
    @Column(name = "STUDYSITE_LSAMPLESIZE")
    public String getSampleSize() {
        return this.sampleSize;
    }

    /**
     * sets sampleSize
     * 
     * @param sampleSize
     */
    public void setSampleSize(String sampleSize) {
        this.sampleSize = sampleSize;
    }

    /**
     * Get Study site Public
     * 
     * @return study Site Public
     */
    @Column(name = "STUDYSITE_PUBLIC")
    public String getStudySitePublic() {
        return StringUtil.integerToString(this.studySitePublic);
    }

    /**
     * sets study site public
     * 
     * @param study
     *            site public
     */
    public void setStudySitePublic(String studySitePublic) {
        this.studySitePublic = StringUtil.stringToInteger(studySitePublic);
    }

    /**
     * Get Study site report include flag
     * 
     * @return study Site repInclude
     */
    @Column(name = "STUDYSITE_REPINCLUDE")
    public String getRepInclude() {
        return StringUtil.integerToString(this.repInclude);
    }

    /**
     * sets study site repInclude
     * 
     * @param study
     *            site repInclude
     */
    public void setRepInclude(String repInclude) {
        this.repInclude = StringUtil.stringToInteger(repInclude);
    }

    /**
     * Get Status creator
     * 
     * @return The Id of the creator of the record
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {

        this.creator = StringUtil.stringToInteger(creator);
    }

    /**
     * Get Status modified by
     * 
     * @return The Id of the user modifying this record
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = StringUtil.stringToInteger(modifiedBy);
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * Returns the value of currentSitePatientCount.
     */
    @Column(name = "STUDYSITE_ENRCOUNT")
    public String getCurrentSitePatientCount() {
        return currentSitePatientCount;
    }
    
    @Column(name = "IS_REVIEWBASED_ENR")
    public String getIsReviewBasedEnrollment() {
		return StringUtil.integerToString(isReviewBasedEnrollment);
	}

	public void setIsReviewBasedEnrollment(String isReviewBasedEnrollment) {
		this.isReviewBasedEnrollment = StringUtil.stringToInteger(isReviewBasedEnrollment);
	}

		
	@Column(name = "ENR_NOTIFYTO")
	public String getSendEnrNotificationTo() {
		return sendEnrNotificationTo;
	}

	public void setSendEnrNotificationTo(String sendEnrNotificationTo) {
		this.sendEnrNotificationTo = sendEnrNotificationTo;
	}
	
	@Column(name = "ENR_STAT_NOTIFYTO")
	public String getSendEnrStatNotificationTo() {
		return sendEnrStatNotificationTo;
	}

	public void setSendEnrStatNotificationTo(String sendEnrStatNotificationTo) {
		this.sendEnrStatNotificationTo = sendEnrStatNotificationTo;
	}

    /**
     * Sets the value of currentSitePatientCount.
     * 
     * @param currentSitePatientCount
     *            The value to assign currentSitePatientCount.
     */
    public void setCurrentSitePatientCount(String currentSitePatientCount) {
        this.currentSitePatientCount = currentSitePatientCount;
    }
	
	@Transient
    public String getSiteSiteId() {
        return this.siteSiteId;
    }

    public void setSiteSiteId(String siteSiteId) {
        this.siteSiteId = siteSiteId;
    }

    // END OF GETTER SETTER METHODS

 
    /**
     * updates the Status History details
     * 
     * @param usk
     *            StatusHistoryStateKeeper
     * @return 0 if successful; -2 for exception
     */
    public int updateStudySite(StudySiteBean ssk) {
        try {

            setStudyId(ssk.getStudyId());
            setSiteId(ssk.getSiteId());
            setStudySiteType(ssk.getStudySiteType());
            setSampleSize(ssk.getSampleSize());
            setStudySitePublic(ssk.getStudySitePublic());
            setRepInclude(ssk.getRepInclude());
            setCreator(ssk.getCreator());
            setModifiedBy(ssk.getModifiedBy());
            setIpAdd(ssk.getIpAdd());
            setCurrentSitePatientCount(ssk.getCurrentSitePatientCount());
            setIsReviewBasedEnrollment(ssk.getIsReviewBasedEnrollment());
        	setSendEnrNotificationTo(ssk.getSendEnrNotificationTo());
        	setSendEnrStatNotificationTo(ssk.getSendEnrStatNotificationTo());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Exception in  StudySiteBean.updateStudySite" + e);
            return -2;
        }
    }

}// end of class
