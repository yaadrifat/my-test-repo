package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.velos.eres.service.util.Rlog;

public class StudyTeamDao extends CommonDAO implements java.io.Serializable {

	private static final long serialVersionUID = 4154720142506874517L;
	
	public ArrayList<String> getStudyTeamAndCollaboratorsData(int studyPk) {
		
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sbSQL = new StringBuffer();        
        ArrayList<String> arList = new ArrayList<String>();
        JSONObject teamJSON = null; 
       
        try {
            conn = getConnection();
            sbSQL.append("select  usr_firstname, usr_lastname, codelst_desc , add_email  from er_studyteam est, er_user eu , er_codelst ec , er_add ea " +
            				" where est.fk_study=? and est.fk_codelst_tmrole=ec.pk_codelst and est.fk_user=eu.pk_user and eu.fk_peradd=ea.pk_add ");                   
            pstmt = conn.prepareStatement(sbSQL.toString());
            Rlog.debug("studyId", "SQL- " + sbSQL);
            pstmt.setInt(1, studyPk);           
           
            ResultSet rs = pstmt.executeQuery();          
            while (rs.next()) {
            	teamJSON = new JSONObject(); 
            	String firstName = rs.getString("usr_firstname");
	        	String lastName = rs.getString("usr_lastname");
	        	String name = firstName + " " + lastName;	   
	        	try {
					teamJSON.put("name" , name);
		        	teamJSON.put("role" , rs.getString("codelst_desc"));
		        	teamJSON.put("email" , rs.getString("add_email"));
				} catch (JSONException e) {
					teamJSON = null;
					e.printStackTrace();
				}
	        	arList.add(teamJSON.toString());
            }
        } catch (SQLException ex) {
            Rlog.fatal("StudyTeamDao", "EXCEPTION IN FETCHING getStudyTeamAndCollaboratorsData" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return arList;
    }
}
