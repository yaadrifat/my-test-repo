/*
 * Classname : SaveFormDao
 * 
 * Version information: 1.0 
 *
 * Date: 07/23/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Sahni
 */

package com.velos.eres.business.common;

/* Import Statements */
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.concurrent.locks.*;

import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;
import oracle.sql.CLOB;
import oracle.xml.parser.v2.DOMParser;
import oracle.xml.parser.v2.XMLDocument;
import oracle.xml.parser.v2.XMLElement;
import oracle.xml.parser.v2.XMLNode;
import oracle.xml.parser.v2.XMLText;

import org.w3c.dom.NodeList;

import com.velos.eres.business.saveForm.SaveFormStateKeeper;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.service.util.XMLUtil;
import com.velos.eres.business.common.FormSecDao;
import com.velos.eres.web.formSec.FormSecJB; 
/* End of Import Statements */

/**
 * The Save Form Dao.<br>
 * <br>
 * This class has methods to manipulate forms
 * 
 * @author Sonia Sahni
 * @vesion 1.0
 */

/**
 * @author vishal
 * 
 */
public class SaveFormDao extends CommonDAO implements java.io.Serializable {
    int formId;
    private final Lock lock = new ReentrantLock();
    ArrayList colSysIds;

    ArrayList colTypes;

    ArrayList colNames;

    ArrayList colOrigSysIds;
    
    ArrayList colUIds;

    SaveFormStateKeeper houseKeeper;

    // These three variables are maintained to
    // keep track of possible responses for a multiple choice field.
    ArrayList colOrigSysIdWValueSet;

    ArrayList colResponseDispValSet;
    
    ArrayList lockdownStatuses ;
    ArrayList lockdownReviewStatuses; // new field introduced for jupiter enhancement ( form status 22620)
    
    int filledFormStat;
    int lockDownStat;
    int lockDownReviewStat;
    
    String htmlStr="";
    String fsFmt="";
    String formSecFmt="";
    Hashtable htOtherAttributes ;
	
    //additional optional parameters supplied to DAO methods
    Hashtable htMoreParams;

    ArrayList colDisplayNames;

	public SaveFormDao() {
        colSysIds = new ArrayList();
        colTypes = new ArrayList();
        colNames = new ArrayList();
        colOrigSysIds = new ArrayList();
        colOrigSysIdWValueSet = new ArrayList();
        colResponseDispValSet = new ArrayList();
        htOtherAttributes = new Hashtable();
        lockdownStatuses =  new ArrayList();
        lockdownReviewStatuses = new ArrayList(); // new field introduced for jupiter enhancement ( form status 22620)
        htMoreParams= new Hashtable();
        colDisplayNames =  new ArrayList();
        colUIds=new  ArrayList();
	}

    // Getter-Setters
	
	public ArrayList getColUIds() {
		return colUIds;
	}

	public void setColUIds(ArrayList colUIds) {
		this.colUIds = colUIds;
	}
	
	public void setColUIds(String colUId) {
		this.colUIds.add(colUId);
	}

    public ArrayList getColNames() {
        return colNames;
    }

    public void setColNames(ArrayList colNames) {
        this.colNames = colNames;
    }

    public void setColNames(String colName) {
        this.colNames.add(colName);
    }

    public ArrayList getColSysIds() {
        return colSysIds;
    }

    public void setColSysIds(ArrayList colSysIds) {
        this.colSysIds = colSysIds;
    }

    public void setColSysIds(String colSysId) {
        this.colSysIds.add(colSysId);
    }

    public ArrayList getColTypes() {
        return colTypes;
    }

    public void setColTypes(ArrayList colTypes) {
        this.colTypes = colTypes;
    }

    public void setColTypes(String colType) {
        this.colTypes.add(colType);
    }

    public int getFormId() {
        return formId;
    }

    public void setFormId(int formId) {
        this.formId = formId;
    }

    public SaveFormStateKeeper getHouseKeeper() {
        return houseKeeper;
    }

    public void setHouseKeeper(SaveFormStateKeeper houseKeeper) {
        this.houseKeeper = houseKeeper;
    }

    public ArrayList getColOrigSysIds() {
        return colOrigSysIds;
    }

    public void setColOrigSysIds(ArrayList colOrigSysIds) {
        this.colOrigSysIds = colOrigSysIds;
    }

    public void setColOrigSysIds(String colOrigSysId) {
        this.colOrigSysIds.add(colOrigSysId);
    }

    public ArrayList getColOrigSysIdWValueSet() {
        return colOrigSysIdWValueSet;
    }

    public void setColOrigSysIdWValueSet(ArrayList colOrigSysIdWValueSet) {
        this.colOrigSysIdWValueSet = colOrigSysIdWValueSet;
    }

    public void setColOrigSysIdWValueSet(String colOrigSysIdWValue) {
        this.colOrigSysIdWValueSet.add(colOrigSysIdWValue);
    }

    public ArrayList getColResponseDispValSet() {
        return colResponseDispValSet;
    }

    public void setColResponseDispValSet(ArrayList colResponseDispValSet) {
        this.colResponseDispValSet = colResponseDispValSet;
    }

    public void setColResponseDispValSet(String colResponseDispVal) {
        this.colResponseDispValSet.add(colResponseDispVal);
    }
    /**
	 * @return the filledFormStat
	 */
	public int getFilledFormStat() {
		return filledFormStat;
	}

	/**
	 * @param filledFormStat the filledFormStat to set
	 */
	public void setFilledFormStat(int filledFormStat) {
		this.filledFormStat = filledFormStat;
	}
	/**
	 * @return the htmlStr
	 */
	public String getHtmlStr() {
		return htmlStr;
	}

	/**
	 * @param htmlStr the htmlStr to set
	 */
	public void setHtmlStr(String htmlStr) {
		this.htmlStr = htmlStr;
	}
	/**
	 * @return the lockDownStat
	 */
	public int getLockDownStat() {
		return lockDownStat;
	}

	/**
	 * @param lockDownStat the lockDownStat to set
	 */
	public void setLockDownStat(int lockDownStat) {
		this.lockDownStat = lockDownStat;
	}

    // End Getter-Setters

    /**
	 * @return the lockDownReviewStat
	 */
	public int getLockDownReviewStat() {
		return lockDownReviewStat;
	}

	/**
	 * @param lockDownReviewStat the lockDownReviewStat to set
	 */
	public void setLockDownReviewStat(int lockDownReviewStat) {
		this.lockDownReviewStat = lockDownReviewStat;
	}

	public void getFormDetails() {
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("SELECT mp_mapcolname,mp_systemid,mp_flddatatype,mp_origsysid,mp_uid FROM ER_MAPFORM WHERE fk_form=? order by pk_mp");
            pstmt.setInt(1, formId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                this.setColNames(rs.getString("mp_mapcolname"));
                this.setColTypes(rs.getString("mp_flddatatype"));
                this.setColSysIds(rs.getString("mp_systemid"));
                this.setColOrigSysIds(rs.getString("mp_origsysid"));
                this.setColUIds(rs.getString("mp_uid"));
                
            }
        } catch (SQLException ex) {
            Rlog.fatal("saveforms", "EXCEPTION IN  SaveFormDao.getFormDetails"
                    + ex);
            ex.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

	public void getFieldDetails(int formpk, int fieldpk) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("SELECT mp_mapcolname,mp_systemid,mp_flddatatype,mp_origsysid,mp_uid FROM ER_MAPFORM WHERE fk_form=? and mp_pkfld=?");
            pstmt.setInt(1, formpk);
            pstmt.setInt(2, fieldpk);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                this.setColNames(rs.getString("mp_mapcolname"));
                this.setColTypes(rs.getString("mp_flddatatype"));
                this.setColSysIds(rs.getString("mp_systemid"));
                this.setColOrigSysIds(rs.getString("mp_origsysid"));
                this.setColUIds(rs.getString("mp_uid"));
            }
        } catch (SQLException ex) {
            Rlog.fatal("saveforms", "EXCEPTION IN  SaveFormDao.getFormDetails"
                    + ex);
            ex.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    /*
     * This method gets the Form XML @param formId Id of the form for blank xml
     * is required
     */
    public String getFormXML(int formId) {

        Connection conn = null;
        Statement s = null;

        String xmlStr = "";

        try {
            conn = getConnection();

            String query = "select c.form_xml.getClobVal() from er_formlib c where pk_formlib = "
                    + formId;

            s = conn.createStatement();
            OraclePreparedStatement stmt;

            ResultSet rset = s.executeQuery(query);
            OracleResultSet orset = (OracleResultSet) rset;

            while (orset.next()) {

                // retrieve form xml document from database

                Clob xmlCLob = null;

                xmlCLob = orset.getClob(1);

                if (!(xmlCLob == null)) {
                    xmlStr = xmlCLob.getSubString(1, (int) xmlCLob.length());
                }

            }

            /*
             * //Parse the XML to normalize the doc DOMParser parser = new
             * DOMParser(); parser.setPreserveWhitespace(false);
             * //parser.setPreserveWhitespace(true); parser.parse(new
             * StringReader(xmlStr)); XMLDocument doc = parser.getDocument();
             * doc.normalize();
             * 
             * //End here
             */
            return xmlStr;
        }

        catch (Exception ex) {
            Rlog.fatal("saveform",
                    "SaveFormDao.getFormXML EXCEPTION IN getting XML" + ex);
            return "";
        } finally {
            try {
                if (s != null)
                    s.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

        // end of method

    }

    /*
     * This method inserts a filled form @param SaveFormStateKeeper sk
     */

    public int insertFilledFormBySP(SaveFormStateKeeper sk) {
        Rlog.debug("saveform", "SaveFormDao.inside insertFilledFormBySP");
        String formXML = "";
        String updatedXML = "";
        int ret = 0, linearRet = -1;
        Integer study = null;
        String specimenPK = "";

        setHouseKeeper(sk);
        String commonformflag=sk.getCommonFormFlag();
        if (commonformflag == null){
        	commonformflag = "";
        	
        }
        int modPk=0;
        
        if(!commonformflag.equalsIgnoreCase("")){
        	 modPk=StringUtil.stringToNum(sk.getModPK());
        }    
        
        int account = StringUtil.stringToNum(sk.getAccountId());

        int creator = StringUtil.stringToNum(sk.getCreator());

        int formCompleted = StringUtil.stringToNum(sk.getFormCompleted());

        String formFillDate = sk.getFormFillDate();
        int formId = StringUtil.stringToNum(sk.getFormId());

        int id = sk.getId();
        String ipAdd = sk.getIpAdd();
        int isValid = StringUtil.stringToNum(sk.getIsValid());

        String lastModifiedDate = sk.getLastModifiedDate();
        ArrayList params = sk.getParams();
        ArrayList paramValues = sk.getParamValues();

        String patientId = sk.getPatientId();
        String lastModifiedBy = sk.getLastModifiedBy();
        String patprotId = sk.getPatprotId();

        String recordType = sk.getRecordType();
        String studyId = sk.getStudyId();
        String displayType = sk.getDisplayType();

        String schevent = sk.getSchEvent();
        specimenPK = sk.getSpecimenPk();
        
        String fkcrf = sk.getFkCrf();

        CLOB xmlClob = null;

        if (StringUtil.isEmpty(formFillDate))
            formFillDate = null;

        if (StringUtil.isEmpty(patientId))
            patientId = null;

        if (StringUtil.isEmpty(patprotId))
            patprotId = null;

        if (StringUtil.isEmpty(lastModifiedBy))
            lastModifiedBy = null;
        

        if (StringUtil.isEmpty(specimenPK))
        	specimenPK = "0";

        Connection conn = null;

        CallableStatement stmt = null;

        StringBuffer sqlSB = new StringBuffer();

        try {

            // get form XML
            formXML = getFormXML(formId);
            updatedXML = updateXML(formXML, params, paramValues);
            updatedXML = uncheckRepeatedCheckboxes(updatedXML, params);
            
          //Added by IA 1.03.2006 bug 2523 Replace [VELSYSTEMDATE] for the systemdate
            
            updatedXML = StringUtil.replace(updatedXML, "[VELSYSTEMDATE]",
                    DateUtil.getCurrentDate());
            
            //end date
            
            conn = getConnection();

            if (displayType.equals("S") || displayType.equals("SA")) {
                Rlog.debug("saveform", "inside S or SA");
                stmt = conn
                        .prepareCall("{call PKG_FORM.SP_INSERT_STUDY_FORM(?,?,?,?,?,?,?,?,?)}");

                stmt.setInt(1, formId);

                stmt.setInt(2, StringUtil.stringToNum(studyId));

                stmt.setClob(3, this.getCLOB(updatedXML, conn));

                stmt.setInt(4, formCompleted);

                stmt.setInt(5, creator);

                stmt.setInt(6, isValid);

                stmt.setString(7, ipAdd);

                stmt.registerOutParameter(8, java.sql.Types.INTEGER);
                
                stmt.setString(9, specimenPK);
                
                
                stmt.execute();

                ret = stmt.getInt(8);

            } else if (displayType.equals("SP") || displayType.equals("PA")) {
                Rlog.debug("saveform", "inside SP or PA");
                stmt = conn
                        .prepareCall("{call PKG_FORM.SP_INSERT_PAT_FORM(?,?,?,?,?,?,?,?,?,?,?,?)}");
                stmt.setInt(1, formId);
                stmt.setInt(2, StringUtil.stringToNum(patientId));
                stmt.setInt(3, StringUtil.stringToNum(patprotId));
                stmt.setClob(4, this.getCLOB(updatedXML, conn));
                stmt.setInt(5, formCompleted);
                stmt.setInt(6, creator);
                stmt.setInt(7, isValid);
                stmt.setString(8, ipAdd);
                stmt.setString(9, displayType);

                stmt.registerOutParameter(10, java.sql.Types.INTEGER);
                stmt.setInt(11, StringUtil.stringToNum(schevent));
                
                stmt.setString(12, specimenPK);
                
                stmt.execute();

                ret = stmt.getInt(10);

            } else if (displayType.equals("A") || displayType.equals("ORG") || displayType.equals("USR") || displayType.equals("NTW") || displayType.equals("SNW")) {
                Rlog.debug("saveform", "inside A");
                stmt = conn
                        .prepareCall("{call PKG_FORM.SP_INSERT_ACCT_FORM(?,?,?,?,?,?,?,?,?,?,?)}");
                stmt.setInt(1, formId);
                stmt.setInt(2, account);
                stmt.setClob(3, getCLOB(updatedXML, conn));
                stmt.setInt(4, formCompleted);
                stmt.setInt(5, creator);
                stmt.setInt(6, isValid);
                stmt.setString(7, ipAdd);
                stmt.registerOutParameter(8, java.sql.Types.INTEGER);
                stmt.setString(9, specimenPK);
                stmt.setString(10, commonformflag);
                stmt.setInt(11, modPk);
                
                stmt.execute();

                ret = stmt.getInt(8);

            }

            else if (displayType.equals("C")) {
                Rlog.debug("saveform", "inside C");
                stmt = conn
                        .prepareCall("{call PKG_FORM.SP_INSERT_CRF_FORM(?,?,?,?,?,?,?,?,?)}");
                stmt.setInt(1, formId);
                stmt.setClob(2, getCLOB(updatedXML, conn));
                stmt.setInt(3, StringUtil.stringToNum(schevent));
                stmt.setInt(4, StringUtil.stringToNum(fkcrf));
                stmt.setInt(5, formCompleted);
                stmt.setInt(6, creator);
                stmt.setInt(7, isValid);
                stmt.setString(8, ipAdd);

                stmt.registerOutParameter(9, java.sql.Types.INTEGER);
                stmt.execute();

                ret = stmt.getInt(9);

            }
            if (ret > 0) {
                long startTime = System.currentTimeMillis();
                // Get all the details for the form to pre-process the record
                // to send to er_formslinear
                this.setFormId(formId);
                this.getFormDetails();
                populateFormAttributes(formId);
                 
                getHouseKeeper().setId(ret);
                String sql = this.createSQL("insert");
                linearRet = this.saveFormsLinear(sql, "insert");
                
                if (linearRet < 0)
                {
                	
                	linearRet = -5; //mapping issue
                	return linearRet ;
                }
                
                
            }
            return ret;

        } catch (Exception ex) {
            Rlog.fatal("saveform",
                    "SaveFormDao.insertFilledFormBySP EXCEPTION IN saving form data"
                            + ex);
            return -1;
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
            }

            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

        // end of method

    }

    /*
     * This method gets the Form XML according to the version and updates the
     * data in XML
     */
    public int updateFilledFormBySP(SaveFormStateKeeper sk) {
        String formXML = "";
        String updatedXML = "";
        int formLibVer = 0, linearRet = -1;

        setHouseKeeper(sk);
        int account = StringUtil.stringToNum(sk.getAccountId());
        int creator = StringUtil.stringToNum(sk.getCreator());
        int formCompleted = StringUtil.stringToNum(sk.getFormCompleted());

        String formFillDate = sk.getFormFillDate();
        int formId = StringUtil.stringToNum(sk.getFormId());
        formLibVer = StringUtil.stringToNum(sk.getFormLibVer());

        int id = sk.getId();
        String ipAdd = sk.getIpAdd();
        int isValid = StringUtil.stringToNum(sk.getIsValid());
        int lastModifiedBy = StringUtil.stringToNum(sk.getLastModifiedBy());
        String lastModifiedDate = sk.getLastModifiedDate();
        ArrayList params = sk.getParams();
        ArrayList paramValues = sk.getParamValues();
        int patientId = StringUtil.stringToNum(sk.getPatientId());
        int patprotId = StringUtil.stringToNum(sk.getPatprotId());
        String recordType = sk.getRecordType();
        int studyId = StringUtil.stringToNum(sk.getStudyId());
        String displayType = sk.getDisplayType();

        Connection conn = null;
        CallableStatement stmt = null;
        StringBuffer sqlSB = new StringBuffer();
        int ret = 0;
        lock.lock();
        try {

            // get form XML
            // formXML = getFormXML(formId);
            // changed as per form version requirements
            // in modify mode data is picked from er_formlibver table instead of
            // er_formlib
            // so retrieve XML for that verison PK

            formXML = getFormVerXML(formLibVer);
System.out.println("formLibVer===="+formLibVer);
System.out.println("formXML===="+formXML);
            updatedXML = updateXML(formXML, params, paramValues);
            System.out.println("params size==="+params.size());
            System.out.println("paramValues size==="+paramValues.size());
            updatedXML = uncheckRepeatedCheckboxes(updatedXML, params);
            
            //Added by IA 1.03.2006 bug 2523 Replace [VELSYSTEMDATE] for the systemdate
            
            updatedXML = StringUtil.replace(updatedXML, "[VELSYSTEMDATE]",
                    DateUtil.getCurrentDate());
        
            //end date
            System.out.println("updatedXML===="+updatedXML);
            
            conn = getConnection();
            stmt = conn
                    .prepareCall("{call PKG_FORM.SP_UPDATE_FILLEDFORM(?,?,?,?,?,?,?,?)}");
            stmt.setInt(1, id);
            stmt.setClob(2, getCLOB(updatedXML, conn));
            stmt.setInt(3, formCompleted);
            stmt.setInt(4, lastModifiedBy);
            stmt.setInt(5, isValid);
            stmt.setString(6, ipAdd);
            stmt.setString(7, displayType);

            stmt.registerOutParameter(8, java.sql.Types.INTEGER);
            stmt.execute();

            ret = stmt.getInt(8);
            if (ret >= 0) {
                // getHouseKeeper().setId(ret);
                long startTime = System.currentTimeMillis();
                // Get all the details for the form to pre-process the record
                // to send to er_formslinear
                this.setFormId(formId);
                this.getFormDetails();
                populateFormAttributes(formId);

                
                String sql = this.createSQL("update");
                linearRet = this.saveFormsLinear(sql, "update");
                
                if (linearRet < 0)
                {
                	
                	linearRet = -5; //mapping issue
                	return linearRet ;
                }
                
                
                
            }
            return ret;
        } catch (Exception ex) {
            Rlog.fatal("saveform",
                    "SaveFormDao.updateFilledFormBySP EXCEPTION IN updating form data"
                            + ex + "SQL is" + sqlSB.toString());
            ex.printStackTrace();
            return -1;
        } finally {
        	lock.unlock();
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
            }

            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

        // end of method

    }
  //Added by Vivek Jha for Monitor Team Role in jupiter
    public int updateFilledFormStatus(SaveFormStateKeeper sk) {
    	int ret = 0;
    	setHouseKeeper(sk);
    	Connection conn = null;
    	PreparedStatement stmt = null;
    	String ipAdd = sk.getIpAdd();
    	int lastModifiedBy = StringUtil.stringToNum(sk.getLastModifiedBy());
    	int id = sk.getId();
    	int formCompleted = StringUtil.stringToNum(sk.getFormCompleted());
    	String sql="";
    	String displayType = sk.getDisplayType(); 
    	  if(displayType.equals("S")|| displayType.equals("SA")){
    		  sql=" UPDATE ER_STUDYFORMS SET FORM_COMPLETED=" +formCompleted+"," +
    		  		" LAST_MODIFIED_DATE=SYSDATE,IP_ADD='"+ipAdd+"' ,  RECORD_TYPE = 'M' ,last_modified_by="+lastModifiedBy+" "+
    		  		" WHERE PK_STUDYFORMS="+id;
    	  }else if(displayType.equals("SP") ||displayType.equals("PA")){
    		  sql=" UPDATE ER_PATFORMS SET FORM_COMPLETED=" +formCompleted+"," +
		  		" LAST_MODIFIED_DATE=SYSDATE,IP_ADD='"+ipAdd+"' ,  RECORD_TYPE = 'M' ,last_modified_by="+lastModifiedBy+" "+
		  		" WHERE PK_PATFORMS="+id;  
    	  }else if(displayType.equals("A")){
    		  sql=" UPDATE ER_ACCTFORMS SET FORM_COMPLETED=" +formCompleted+"," +
		  		" LAST_MODIFIED_DATE=SYSDATE,IP_ADD='"+ipAdd+"' ,  RECORD_TYPE = 'M' ,last_modified_by="+lastModifiedBy+" "+
		  		" WHERE PK_ACCTFORMS="+id;  
    	  }else if(displayType.equals("C")){
    		  sql=" UPDATE ER_CRFFORMS SET FORM_COMPLETED=" +formCompleted+"," +
		  		" LAST_MODIFIED_DATE=SYSDATE,IP_ADD='"+ipAdd+"' ,  RECORD_TYPE = 'M' ,last_modified_by="+lastModifiedBy+" "+
		  		" WHERE PK_CRFFORMS="+id;  
    		  
    	  }else
    	  {
    		  return -5;
    	  }
    	  try{
    		  conn=getConnection();
    		  stmt=conn.prepareStatement(sql);
    		  ret=stmt.executeUpdate();
    		  Rlog.info("Return", new Integer(ret).toString());
    		  if(ret==1){
    			  int linRet=this.saveLinearFormSatatus();
    			 Rlog.info("update", "formsLinear update");
    			 ret=0;
    		  }
    		  return ret;
    		  
    	  }catch (Exception ex) {
              Rlog.fatal("saveform",
                      "SaveFormDao.updateFilledFormBySP EXCEPTION IN updating form data"
                              + ex + "SQL is" + sql.toString());
              ex.printStackTrace();
              return -1;
    	  }finally {
              try {
                  if (stmt != null)
                      stmt.close();
              } catch (Exception e) {
              }

              try {
                  if (conn != null)
                      conn.close();
              } catch (Exception e) {
              }

          }
    	
    	
    }
    
  //Added by Vivek Jha for Monitor Team Role in jupiter
   public  int saveLinearFormSatatus(){
	   Rlog.info("saveLinearFormSatatus", "Start Method ");
	    Connection conn = null;
     	PreparedStatement stmt = null;
	   int ret=0;
	   String sql="";
	  
	   int formCompleted= StringUtil.stringToNum(getHouseKeeper().getFormCompleted());
	   int lastModifiedBy=StringUtil.stringToNum(getHouseKeeper().getLastModifiedBy());
	   int id=getHouseKeeper().getId();
	   int formId=StringUtil.stringToNum(getHouseKeeper().getFormId());
	   sql=" update er_formslinear set form_completed="+formCompleted+", " +
		" last_modified_date=sysdate, last_modified_by="+lastModifiedBy+" " +
		"  where fk_filledform="+id+ "and fk_form="+formId ;
	   try{
		   conn=getConnection();
		   stmt=conn.prepareStatement(sql);
		   ret=stmt.executeUpdate();
		   return ret;
	   }catch (Exception ex) {
           Rlog.fatal("saveLinearFormSatatus",
                   "SaveFormDao saveLinearFormSatatus EXCEPTION IN updating form data"
                           + ex + "SQL is" + sql.toString());
           ex.printStackTrace();
           return -1;
 	  }finally {
           try {
               if (stmt != null)
                   stmt.close();
           } catch (Exception e) {
           }

           try {
               if (conn != null)
                   conn.close();
           } catch (Exception e) {
           }

       }
	   
   }

    /*
     * Method to update the XML with data if no data is entered by the user in
     * textarea then the method adds 'er_textarea_tag' as data which is later
     * replaced in Stored Procedure. This has been as <textarea/> tag after
     * transformation is not supported in IE 6, otherwise it was not required
     * For user selected values of all the checkboxes are stored in the field
     * element attribute checkboxesdata
     */

    static String updateXML(String xmlTypeStr, ArrayList params,
            ArrayList values)

    {

        Rlog.debug("saveform", "SaveFormDao in updateXML params " + params
                + " values " + values);

        int paramCt = 0;
        String paramStr = null;
        String paramValueStr = null;

        String attrColCountVal = "";
        String attrLinesNo = "";
        String attrDataVal = "";
        String attrDisplayVal = "";
        String attrTypeVal = "";
        String multiAtrrType = "";
        String checkboxData = "";

        paramCt = params.size();

        String outXML = null;
        try {
            DOMParser parser = new DOMParser();

            parser.setPreserveWhitespace(false);

            parser.parse(new StringReader(xmlTypeStr));

            XMLDocument doc = parser.getDocument();

            for (int i = 0; i < paramCt; i++) // iterate for every parameter
            {
                paramStr = (String) params.get(i);
                paramValueStr = (String) values.get(i);

                Rlog.debug("saveform",
                        "SaveFormDao in updateXML paramValueStr "
                                + paramValueStr);

                NodeList nl = doc.getElementsByTagName(paramStr);

                for (int n = 0; n < nl.getLength(); n++) {
                    XMLElement elem = (XMLElement) nl.item(n);

                    XMLNode textNode = (XMLNode) elem.getFirstChild();

                    // Create a new node under the node
                    XMLText text = (XMLText) doc.createTextNode("text");
                    // End

                    attrColCountVal = elem.getAttribute("colcount");
                    attrLinesNo = elem.getAttribute("linesno");
                    String temp = elem.getNodeValue();
                    // special handling for textarea, add 'er_textarea_tag' as
                    // data if user hasn't entered any data

                    /*
                     * if (StringUtil.stringToNum(attrLinesNo) > 1) {
                     * 
                     * if (paramValueStr.equals("")) { paramValueStr =
                     * "er_textarea_tag"; } }
                     */

                    attrTypeVal = elem.getAttribute("type");

                    if (attrTypeVal.equals("MR") || attrTypeVal.equals("MC")) {
                        multiAtrrType = "checked";
                    } else if (attrTypeVal.equals("MD")) {
                        multiAtrrType = "selected";
                    }

                    if (attrTypeVal.equals("MC")) {
                        // the values of checkboxes are separated with
                        // [VELCOMMA]
                        paramValueStr = addQuote(paramValueStr, "[VELCOMMA]");
                        Rlog.debug("saveform",
                                "SaveFormDao in updateXML paramValueStr after addQuote"
                                        + paramValueStr);
                    }

                    if (StringUtil.isEmpty(attrColCountVal)) {
                        // if (textNode != null)
                        // textNode.setNodeValue(paramValueStr);
                        text.setNodeValue(paramValueStr);
                        if (elem != null) {
                            if (textNode != null) {
                                elem.replaceChild(text, textNode);
                            } else {
                                elem.appendChild(text);
                            }
                        }

                    } else // multichoice field
                    {
                        // get decendent elements

                        NodeList nlresp = elem.getElementsByTagName("resp");

                        checkboxData = "";

                        for (int p = 0; p < nlresp.getLength(); p++) {
                            XMLElement elemresp = (XMLElement) nlresp.item(p);
                            XMLNode textNoderesp = (XMLNode) elemresp
                                    .getFirstChild();

                            attrDataVal = elemresp.getAttribute("dataval");
                            attrDisplayVal = elemresp.getAttribute("dispval");

                            if (StringUtil.isEmpty(attrDataVal)) {
                                attrDataVal = attrDisplayVal;
                            }

                            if (attrTypeVal.equals("MC")) {
                            	System.out.println("Inside MD or MR");
                            	System.out.println("paramValueStr==="+paramValueStr);
                            	System.out.println("attrDataVal==="+attrDataVal);
                                if (paramValueStr.indexOf("'" + attrDataVal
                                        + "'") >= 0) // check the option
                                {

                                    elemresp.setAttribute(multiAtrrType, "1");
                                    // incase of checkboxes, make the comma
                                    // separated string of selected values
                                    if (checkboxData.equals("")) {
                                        checkboxData = "[" + attrDisplayVal
                                                + "]";
                                    } else {
                                        checkboxData = checkboxData + ", ["
                                                + attrDisplayVal + "]";
                                    }

                                } else // uncheck the option
                                {
                                    elemresp.setAttribute(multiAtrrType, "0");
                                }
                            } // for checkbox
                            else if (attrTypeVal.equals("MD")
                                    || attrTypeVal.equals("MR")) {
                            	System.out.println("Inside MD or MR");
                            	System.out.println("paramValueStr==="+paramValueStr);
                            	System.out.println("attrDataVal==="+attrDataVal);
                                if (paramValueStr.equals(attrDataVal)) // check
                                // the
                                // option
                                {
                                	
                                    elemresp.setAttribute(multiAtrrType, "1");
                                    // store the selected value in checkboxData
                                    checkboxData = attrDisplayVal;
                                } else // uncheck the option
                                {
                                    elemresp.setAttribute(multiAtrrType, "0");
                                }

                            } // end for radio and dropdown

                        }
                        // }//end of multi choice field

                        // store the selected checkboxes in a field atribute
                        elem.setAttribute("checkboxesdata", checkboxData);

                    }

                }

            }

            StringWriter sw = new StringWriter();
            doc.print(new PrintWriter(sw));

            outXML = sw.toString();

        } // end of try
        catch (Exception e) {
            Rlog.fatal("saveform", "Error in updateXML in saveFormDao" + e);
            e.printStackTrace();
        }
        // System.out.println("outXML" + outXML);
        return outXML;
    }

    private String uncheckRepeatedCheckboxes(String xmlTypeStr, ArrayList params) {
        String outXML = null;
        try {
            DOMParser parser = new DOMParser();
            parser.setPreserveWhitespace(false);
            parser.parse(new StringReader(xmlTypeStr));
            XMLDocument doc = parser.getDocument();
            String attrTypeVal = null;
            String systemIdVal = null;
            String origSysIdVal = null;
            NodeList nl = doc.getDocumentElement().getChildNodes();
            for (int n = 0; n < nl.getLength(); n++) {
                XMLElement elem = (XMLElement) nl.item(n);
                attrTypeVal = elem.getAttribute("type");
                if (!"MC".equals(attrTypeVal) && !"MR".equals(attrTypeVal)) { continue; }
                systemIdVal = elem.getAttribute("systemid");
                if (systemIdVal == null) { continue; }
                origSysIdVal = elem.getAttribute("origsysid");
                if (systemIdVal.equals(origSysIdVal)) { continue; }
                if (params.contains(systemIdVal)) { continue; }
                NodeList nlresp = elem.getElementsByTagName("resp");
                for (int p = 0; p < nlresp.getLength(); p++) {
                    XMLElement elemresp = (XMLElement) nlresp.item(p);
                    elemresp.setAttribute("checked", "0");
                }
            }
            StringWriter sw = new StringWriter();
            doc.print(new PrintWriter(sw));
            outXML = sw.toString();
        } catch (Exception e) {
            Rlog.fatal("saveform", "Error in uncheckRepeatedCheckboxes in SaveFormDao " + e);
            e.printStackTrace();
            return xmlTypeStr;
        }
        return outXML;
    }
    
    static String addQuote(String mainStr) {

        String str = "";

        StringTokenizer st = new StringTokenizer(mainStr, ",");
        while (st.hasMoreTokens()) {
            str = str + ",'" + st.nextToken() + "'";
        }

        str = str.substring(1);
        return str;
    }

    /*
     * Method to add quotes @param Main String @param separator, separating the
     * data
     */

    static String addQuote(String mainStr, String separator) {

        int mainlen = mainStr.length();
        int pos = mainStr.indexOf(separator);
        int startpos = 0;
        int len = separator.length();
        String str = "";
        while (pos > 0) {
            str = str + ",'" + mainStr.substring(startpos, pos) + "'";
            startpos = pos + len;
            pos = mainStr.indexOf(separator, pos + len);
        }
        str = str + ",'" + mainStr.substring(startpos, mainlen) + "'";

        str = str.substring(1);
        return str;
    }

    /*
     * Modified by Sonia, 10/28/05, to transform the XML in java instead of
     * database
     */
    /*
     * This method gets the Form HTML for preview @param formId Id of the form
     * for which html is required @returns html string
     */
    /* Modified by Sonia, 12/01/06 to generate activation_js/custom_js for inter field actions when its blank*/

    public String getFormHtmlforPreview(int formId) {

        int rows = 0;
        Clob htmlClob = null;
        String htmlStr = "";
        CallableStatement cstmt = null;
        Connection conn = null;
        String xml = "";
        String xsl = "";
        String findStr = "";
        int len = 0;
        String htmlstr1 ="";
        String htmlstr2 ="";
        
        FormLibDao fdao = new FormLibDao();
        FormSecDao fsdao=new FormSecDao();
        String arForms[] = new String[1];
        LinkedFormsDao ldao = new LinkedFormsDao(); 

        try {
            conn = getConnection();
            
            if (fdao.shouldGenerateActiovationScript(formId)) // will return true only if there is an inter field action defined and action script is blank
            {
            	  arForms[0] = String.valueOf(formId);
                  ldao.updateFormFieldActionsScript(arForms);

            }

            cstmt = conn.prepareCall("{call PKG_FORM.SP_CLUBFORMXSL(?,?)}");

            cstmt.setInt(1, formId);
            cstmt.registerOutParameter(2, java.sql.Types.CLOB);

            cstmt.execute();

            xml = fdao.getFormXml(formId);
            while(xml.indexOf("colcount = ")!=-1 || xml.indexOf("colcount=")!=-1){
        		int index1= (xml.indexOf("colcount = ")==-1) ? xml.indexOf("colcount=") : xml.indexOf("colcount = ");
        		String fldBeforeStr= xml.substring(0,index1);
        		int nameIndex=fldBeforeStr.lastIndexOf("name = ")==-1 ? fldBeforeStr.lastIndexOf("name=") : fldBeforeStr.lastIndexOf("name = ");
        		String fld_=fldBeforeStr.substring(fldBeforeStr.lastIndexOf("<fld")+1,nameIndex-1);
        		String afterStr=xml.substring(index1+8,xml.length());
        		xml=fldBeforeStr+"colcnt_"+fld_+afterStr;
        		
         		
        	 }
            xsl = fdao.getFormXsl(formId);
            xsl = XMLUtil.escapeSpecialChars(xsl);
            findStr = "<BODY id=\"forms\" onFocus=";
            len = findStr.length();
            int pos1 = xsl.indexOf(findStr);
            if(pos1>0){
            int pos2 = xsl.indexOf("();\">");
            len=len-8;
		    htmlstr1 = xsl.substring(0,pos1+len);
		    htmlstr2 = xsl.substring(pos2+4,xsl.length());
		    xsl = htmlstr1+htmlstr2;
            }
           
            while(xsl.indexOf("colcount")!=-1){
            	int index1=xsl.indexOf("/@colcount");
            	String fldBeforeStr=xsl.substring(0,xsl.indexOf("/@colcount"));
            	String fld_=fldBeforeStr.substring(fldBeforeStr.lastIndexOf("fld"),fldBeforeStr.length());
            	index1=xsl.indexOf(fld_+"/@colcount");
            	String beforeStr=xsl.substring(0,index1);
            	beforeStr=beforeStr.substring(0, beforeStr.lastIndexOf("colcount"))+"colcnt_"+fld_+beforeStr.substring(beforeStr.lastIndexOf("colcount")+8, beforeStr.length());
            	String afterStr=xsl.substring(index1,xsl.length());
            	afterStr=afterStr.replace(fld_+"/@colcount",fld_+"/@colcnt_"+fld_);
            	afterStr=afterStr.substring(0, afterStr.indexOf("$colcount"))+"$colcnt_"+fld_+afterStr.substring(afterStr.indexOf("$colcount")+9,afterStr.length());
            	
            	xsl=beforeStr+afterStr;
            }
            
           
             htmlStr = XMLUtil.transform(xml, xsl);
           
            /*FormFieldDao formfieldDao =new FormFieldDao();
            formfieldDao.getFormfieldDetails(formId);
            ArrayList fldSystemIds=new ArrayList();
            fldSystemIds=formfieldDao.getFldSystemId();
            for(int i=0;i<fldSystemIds.size();i++){
            	int index1=0;
            	int lastIndex=0;
            	index1=xsl.indexOf(fldSystemIds.get(i)+"/@colcount");
            	if(index1==-1)
            		continue;
            	String beforeStr=xsl.substring(0,index1);
            	beforeStr=beforeStr.substring(0, beforeStr.lastIndexOf("colcount"))+"colcount_"+fldSystemIds.get(i)+beforeStr.substring(beforeStr.lastIndexOf("colcount")+8, beforeStr.length());
            	String afterStr=xsl.substring(index1,xsl.length());
            	afterStr=afterStr.replace(fldSystemIds.get(i)+"/@colcount",fldSystemIds.get(i)+"/@colcount_"+fldSystemIds.get(i));
            	afterStr=afterStr.substring(0, afterStr.indexOf("$colcount"))+"$colcount_"+fldSystemIds.get(i)+afterStr.substring(afterStr.indexOf("$colcount")+9,afterStr.length());
            	xsl=beforeStr+afterStr;
            }*/
            
            /*Remove colcount  is multiply defined in the same scope.(END)*/
            
           
            htmlStr = XMLUtil.unEscapeSpecialChars(htmlStr);

            htmlStr = StringUtil.replace(htmlStr, "er_textarea_tag<", "<");

            Rlog.fatal("saveform", "SaveFormDao.getFormHtml i m here");

            // htmlClob = cstmt.getClob(2);
            // if (!(htmlClob == null)) {

            // htmlStr = htmlClob.getSubString(1, (int) htmlClob.length());
            // add default form status dropdown to the html, this is an
            // application hardcoded field
            // not user defined
            htmlStr = addFormStatusToHtml(htmlStr, 0);

            htmlStr = htmlStr.replaceAll("e-Signature", LC.L_Esignature);
            htmlStr = htmlStr.replaceAll("Data Entry Date", LC.L_Data_EntryDate);          
            htmlStr = htmlStr.replaceAll("Valid e-Sign", LC.L_Valid_Esign);            
            htmlStr = htmlStr.replaceAll("Invalid e-Sign", LC.L_Invalid_Esign);
            if( !"".equals(Configuration.eSignConf)){
            	String eSignTag="<input type=\"password\" autocomplete=\"off\" name=\"eSign\" maxlength=\"8\" id=\"eSign\" onkeyup=\"ajaxvalidate('misc:'+this.id,4,'eSignMessage','Valid e-Sign','Invalid e-Sign','sessUserId')\">";
   	            htmlStr=htmlStr.replace(eSignTag,"<input type=\"password\" autocomplete=\"off\" name=\"eSign\"  id=\"eSign\" style=\"background:url(./images/ic_lock_outline_gray_24dp.png) no-repeat; padding-left: 25px;\" class=\"input21\" onkeyup=\"ajaxvalidate('pass:'+this.id,-1,'eSignMessage','Valid-Password','Invalid-Password','sessUserId')\">");
   	            htmlStr = htmlStr.replaceAll(LC.L_Esignature, "e-Password");
               }
            // }
            return htmlStr;
        } catch (SQLException ex) {
            Rlog.fatal("saveform",
                    "SaveFormDao.getFormHtml EXCEPTION IN FETCHING form html from SP_CLUBFORMXSL"
                            + ex);
            return null;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }


    public String getFormHtmlforPreviewNoRole(int formId) {

        int rows = 0;
        Clob htmlClob = null;
        String htmlStr = "";
        CallableStatement cstmt = null;
        Connection conn = null;
        String xml = "";
        String xsl = "";
        FormLibDao fdao = new FormLibDao();
        String arForms[] = new String[1];
        LinkedFormsDao ldao = new LinkedFormsDao(); 

        try {
            conn = getConnection();
            
            if (fdao.shouldGenerateActiovationScript(formId)) // will return true only if there is an inter field action defined and action script is blank
            {
            	  arForms[0] = String.valueOf(formId);
                  ldao.updateFormFieldActionsScript(arForms);

            }

            cstmt = conn.prepareCall("{call PKG_FORM.SP_CLUBFORMXSL(?,?)}");

            cstmt.setInt(1, formId);
            cstmt.registerOutParameter(2, java.sql.Types.CLOB);

            cstmt.execute();

            xml = fdao.getFormXml(formId);
            while(xml.indexOf("colcount = ")!=-1 || xml.indexOf("colcount=")!=-1){
        		int index1= xml.indexOf("colcount = ")==-1 ? xml.indexOf("colcount=") : xml.indexOf("colcount = ");
        		String fldBeforeStr= xml.substring(0,index1);
        		int nameIndex=fldBeforeStr.lastIndexOf("name = ")==-1 ? fldBeforeStr.lastIndexOf("name=") : fldBeforeStr.lastIndexOf("name = ");
        		String fld_=fldBeforeStr.substring(fldBeforeStr.lastIndexOf("<fld")+1,nameIndex-1);
        		String afterStr=xml.substring(index1+8,xml.length());
        		xml=fldBeforeStr+"colcnt_"+fld_+afterStr;
        		
         		
        	 }
            xsl = fdao.getFormXsl(formId);
            xsl = XMLUtil.escapeSpecialChars(xsl);
            while(xsl.indexOf("colcount")!=-1){
            	int index1=xsl.indexOf("/@colcount");
            	String fldBeforeStr=xsl.substring(0,xsl.indexOf("/@colcount"));
            	String fld_=fldBeforeStr.substring(fldBeforeStr.lastIndexOf("fld"),fldBeforeStr.length());
            	index1=xsl.indexOf(fld_+"/@colcount");
            	String beforeStr=xsl.substring(0,index1);
            	beforeStr=beforeStr.substring(0, beforeStr.lastIndexOf("colcount"))+"colcnt_"+fld_+beforeStr.substring(beforeStr.lastIndexOf("colcount")+8, beforeStr.length());
            	String afterStr=xsl.substring(index1,xsl.length());
            	afterStr=afterStr.replace(fld_+"/@colcount",fld_+"/@colcnt_"+fld_);
            	afterStr=afterStr.substring(0, afterStr.indexOf("$colcount"))+"$colcnt_"+fld_+afterStr.substring(afterStr.indexOf("$colcount")+9,afterStr.length());
            	
            	xsl=beforeStr+afterStr;
            	
            }
            
            
            
            /*FormFieldDao formfieldDao =new FormFieldDao();
            formfieldDao.getFormfieldDetails(formId);
            ArrayList fldSystemIds=new ArrayList();
            fldSystemIds=formfieldDao.getFldSystemId();
            for(int i=0;i<fldSystemIds.size();i++){
            	int index1=0;
            	int lastIndex=0;
            	index1=xsl.indexOf(fldSystemIds.get(i)+"/@colcount");
            	if(index1==-1)
            		continue;
            	String beforeStr=xsl.substring(0,index1);
            	beforeStr=beforeStr.substring(0, beforeStr.lastIndexOf("colcount"))+"colcount_"+fldSystemIds.get(i)+beforeStr.substring(beforeStr.lastIndexOf("colcount")+8, beforeStr.length());
            	String afterStr=xsl.substring(index1,xsl.length());
            	afterStr=afterStr.replace(fldSystemIds.get(i)+"/@colcount",fldSystemIds.get(i)+"/@colcount_"+fldSystemIds.get(i));
            	afterStr=afterStr.substring(0, afterStr.indexOf("$colcount"))+"$colcount_"+fldSystemIds.get(i)+afterStr.substring(afterStr.indexOf("$colcount")+9,afterStr.length());
            	xsl=beforeStr+afterStr;
            }*/
            
            /*Remove colcount  is multiply defined in the same scope.(END)*/
            
            

            
             htmlStr = XMLUtil.transform(xml, xsl);
           
            htmlStr = XMLUtil.unEscapeSpecialChars(htmlStr);

            htmlStr = StringUtil.replace(htmlStr, "er_textarea_tag<", "<");

            Rlog.fatal("saveform", "SaveFormDao.getFormHtml i m here");

            // htmlClob = cstmt.getClob(2);
            // if (!(htmlClob == null)) {

            // htmlStr = htmlClob.getSubString(1, (int) htmlClob.length());
            // add default form status dropdown to the html, this is an
            // application hardcoded field
            // not user defined
            htmlStr = addFormStatusToHtmlNoRole(htmlStr, 0);

            htmlStr = htmlStr.replaceAll("e-Signature", LC.L_Esignature);
            htmlStr = htmlStr.replaceAll("Data Entry Date", LC.L_Data_EntryDate);          
            htmlStr = htmlStr.replaceAll("Valid e-Sign", LC.L_Valid_Esign);            
            htmlStr = htmlStr.replaceAll("Invalid e-Sign", LC.L_Invalid_Esign);
            // }
            if( !"".equals(Configuration.eSignConf)){
            	String eSignTag="<input type=\"password\" autocomplete=\"off\" name=\"eSign\" maxlength=\"8\" id=\"eSign\" onkeyup=\"ajaxvalidate('misc:'+this.id,4,'eSignMessage','Valid e-Sign','Invalid e-Sign','sessUserId')\">";
   	            htmlStr=htmlStr.replace(eSignTag,"<input type=\"password\" autocomplete=\"off\" name=\"eSign\"  id=\"eSign\" style=\"background:url(./images/ic_lock_outline_gray_24dp.png) no-repeat; padding-left: 25px;\" class=\"input21\" onkeyup=\"ajaxvalidate('pass:'+this.id,-1,'eSignMessage','Valid-Password','Invalid-Password','sessUserId')\">");
   	            htmlStr = htmlStr.replaceAll(LC.L_Esignature, "e-Password");
               }

            return htmlStr;
        } catch (SQLException ex) {
            Rlog.fatal("saveform",
                    "SaveFormDao.getFormHtml EXCEPTION IN FETCHING form html from SP_CLUBFORMXSL"
                            + ex);
            return null;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /*
     * This method gets the Form HTML for filled form @param formId Id of the
     * form for which html is required @returns html string
     */
    /* Modified by Sonia Abrol, 11/02.05, transform xml and xsl in java */
    public String getFilledFormHtml(int pkfilledform, String formdisplaytype) {

        int rows = 0;
        Clob xmlClob = null;
        Clob xslClob = null;

        String htmlStr = "";
        String xmlStr = "";
        String xslStr = "";
        String findStr = "";
        int len = 0;
        String htmlstr1 ="";
        String htmlstr2 ="";

        CallableStatement cstmt = null;
        Connection conn = null;
        int formStatus = 0;
        String fkSpecimen = "";
        try {
        	
            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_FORM.SP_GETFILLEDFORM_HTML(?,?,?,?,?,?)}");

            cstmt.setInt(1, pkfilledform);
            cstmt.setString(2, formdisplaytype);
            cstmt.registerOutParameter(3, java.sql.Types.CLOB);
            cstmt.registerOutParameter(4, java.sql.Types.INTEGER);
            cstmt.registerOutParameter(5, java.sql.Types.CLOB);
            cstmt.registerOutParameter(6, java.sql.Types.INTEGER);
            cstmt.execute();

            xmlClob = cstmt.getClob(3);
            formStatus = cstmt.getInt(4);
            xslClob = cstmt.getClob(5);
            this.setFilledFormStat(formStatus);
            fkSpecimen = cstmt.getString(6);
            
            if (!(xmlClob == null)) {
                xmlStr = xmlClob.getSubString(1, (int) xmlClob.length());
                while(xmlStr.indexOf("colcount = ")!=-1 || xmlStr.indexOf("colcount=")!=-1){
            		int index1= (xmlStr.indexOf("colcount = ")==-1) ? xmlStr.indexOf("colcount=") : xmlStr.indexOf("colcount = ");
            		String fldBeforeStr= xmlStr.substring(0,index1);
            		int nameIndex=(fldBeforeStr.lastIndexOf("name = ")==-1) ? fldBeforeStr.lastIndexOf("name=") : fldBeforeStr.lastIndexOf("name = ");
            		String fld_=fldBeforeStr.substring(fldBeforeStr.lastIndexOf("<fld")+1,nameIndex-1);
            		String afterStr=xmlStr.substring(index1+8,xmlStr.length());
            		xmlStr=fldBeforeStr+"colcnt_"+fld_+afterStr;
            		
             		
            	 }
                xslStr = xslClob.getSubString(1, (int) xslClob.length());

                xslStr = XMLUtil.escapeSpecialChars(xslStr);
                findStr = "<BODY id=\"forms\" onFocus=";
                len = findStr.length();
                int pos1 = xslStr.indexOf(findStr);
                if(pos1>0){
                int pos2 = xslStr.indexOf("();\">");
                len=len-8;
    		    htmlstr1 = xslStr.substring(0,pos1+len);
    		    htmlstr2 = xslStr.substring(pos2+4,xslStr.length());
    		    xslStr = htmlstr1+htmlstr2;
                }
                // Rlog.debug("saveform","getfilledformhtml. xmlStr" + xmlStr);
                // Rlog.debug("saveform","getfilledformhtml. xslStr" + xslStr);
                
                
                while(xslStr.indexOf("colcount")!=-1){
                	int index1=xslStr.indexOf("/@colcount");
                	String fldBeforeStr=xslStr.substring(0,xslStr.indexOf("/@colcount"));
                	String fld_=fldBeforeStr.substring(fldBeforeStr.lastIndexOf("fld"),fldBeforeStr.length());
                	index1=xslStr.indexOf(fld_+"/@colcount");
                	String beforeStr=xslStr.substring(0,index1);
                	beforeStr=beforeStr.substring(0, beforeStr.lastIndexOf("colcount"))+"colcnt_"+fld_+beforeStr.substring(beforeStr.lastIndexOf("colcount")+8, beforeStr.length());
                	String afterStr=xslStr.substring(index1,xslStr.length());
                	afterStr=afterStr.replace(fld_+"/@colcount",fld_+"/@colcnt_"+fld_);
                	afterStr=afterStr.substring(0, afterStr.indexOf("$colcount"))+"$colcnt_"+fld_+afterStr.substring(afterStr.indexOf("$colcount")+9,afterStr.length());
                	
                	xslStr=beforeStr+afterStr;
                	
                }
                
                /*FormFieldDao formfieldDao =new FormFieldDao();
                formfieldDao.getFormfieldDetails(formId);
                ArrayList fldSystemIds=new ArrayList();
                fldSystemIds=formfieldDao.getFldSystemId();
                for(int i=0;i<fldSystemIds.size();i++){
                	int index1=0;
                	int lastIndex=0;
                	index1=xslStr.indexOf(fldSystemIds.get(i)+"/@colcount");
                	if(index1==-1)
                		continue;
                	String beforeStr=xslStr.substring(0,index1);
                	beforeStr=beforeStr.substring(0, beforeStr.lastIndexOf("colcount"))+"colcount_"+fldSystemIds.get(i)+beforeStr.substring(beforeStr.lastIndexOf("colcount")+8, beforeStr.length());
                	String afterStr=xslStr.substring(index1,xslStr.length());
                	afterStr=afterStr.replace(fldSystemIds.get(i)+"/@colcount",fldSystemIds.get(i)+"/@colcount_"+fldSystemIds.get(i));
                	afterStr=afterStr.substring(0, afterStr.indexOf("$colcount"))+"$colcount_"+fldSystemIds.get(i)+afterStr.substring(afterStr.indexOf("$colcount")+9,afterStr.length());
                	xslStr=beforeStr+afterStr;
                }*/
                
                /*Remove colcount  is multiply defined in the same scope.(END)*/
                

                
                  htmlStr = XMLUtil.transform(xmlStr, xslStr);
                
                // Rlog.debug("saveform","getfilledformhtml. htmlStr" +
                // htmlStr);

                htmlStr = XMLUtil.unEscapeSpecialChars(htmlStr);

                htmlStr = StringUtil.replace(htmlStr, "er_textarea_tag<", "<");

                // add default form status dropdown to the html, this is an
                // application hardcoded field
                // not user defined
                htmlStr = addFormStatusToHtml(htmlStr, formStatus);

	            htmlStr = htmlStr.replaceAll("e-Signature", LC.L_Esignature);
	            htmlStr = htmlStr.replaceAll("Data Entry Date", LC.L_Data_EntryDate);          
	            htmlStr = htmlStr.replaceAll("Valid e-Sign", LC.L_Valid_Esign);            
	            htmlStr = htmlStr.replaceAll("Invalid e-Sign", LC.L_Invalid_Esign);
	            
	            if( !"".equals(Configuration.eSignConf)){
	            	String eSignTag="<input type=\"password\" autocomplete=\"off\" name=\"eSign\" maxlength=\"8\" id=\"eSign\" onkeyup=\"ajaxvalidate('misc:'+this.id,4,'eSignMessage','Valid e-Sign','Invalid e-Sign','sessUserId')\">";
	   	            htmlStr=htmlStr.replace(eSignTag,"<input type=\"password\" autocomplete=\"off\" name=\"eSign\"  id=\"eSign\" style=\"background:url(./images/ic_lock_outline_gray_24dp.png) no-repeat; padding-left: 25px;\" class=\"input21\" onkeyup=\"ajaxvalidate('pass:'+this.id,-1,'eSignMessage','Valid-Password','Invalid-Password','sessUserId')\">");
	   	            htmlStr = htmlStr.replaceAll(LC.L_Esignature, "e-Password");
	               }
		            
		            
            }
            this.setHtmlStr(htmlStr);
            
            if ( !StringUtil.isEmpty(fkSpecimen))
            {
            	setHtOtherAttributes("fkSpecimen",fkSpecimen);
            }	
            
            return htmlStr;
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "saveform",
                            "SaveFormDao.getFormHtml EXCEPTION IN FETCHING form html from SP_GETFILLEDFORM_HTML"
                                    + ex);
            ex.printStackTrace();
            this.setHtmlStr("");
            return null;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public String getFilledFormHtmlNoRole(int pkfilledform, String formdisplaytype) {

        int rows = 0;
        Clob xmlClob = null;
        Clob xslClob = null;

        String htmlStr = "";
        String xmlStr = "";
        String xslStr = "";

        CallableStatement cstmt = null;
        Connection conn = null;
        int formStatus = 0;
        String fkSpecimen = "";
        try {
        	
            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_FORM.SP_GETFILLEDFORM_HTML(?,?,?,?,?,?)}");

            cstmt.setInt(1, pkfilledform);
            cstmt.setString(2, formdisplaytype);
            cstmt.registerOutParameter(3, java.sql.Types.CLOB);
            cstmt.registerOutParameter(4, java.sql.Types.INTEGER);
            cstmt.registerOutParameter(5, java.sql.Types.CLOB);
            cstmt.registerOutParameter(6, java.sql.Types.INTEGER);
            cstmt.execute();

            xmlClob = cstmt.getClob(3);
            formStatus = cstmt.getInt(4);
            xslClob = cstmt.getClob(5);
            this.setFilledFormStat(formStatus);
            fkSpecimen = cstmt.getString(6);
            
            if (!(xmlClob == null)) {
                xmlStr = xmlClob.getSubString(1, (int) xmlClob.length());
                while(xmlStr.indexOf("colcount = ")!=-1 || xmlStr.indexOf("colcount=")!=-1){
            		int index1= (xmlStr.indexOf("colcount = ")==-1) ? xmlStr.indexOf("colcount=") : xmlStr.indexOf("colcount = ");
            		String fldBeforeStr= xmlStr.substring(0,index1);
            		int nameIndex=(fldBeforeStr.lastIndexOf("name = ")==-1) ? fldBeforeStr.lastIndexOf("name=") : fldBeforeStr.lastIndexOf("name = ");
            		String fld_=fldBeforeStr.substring(fldBeforeStr.lastIndexOf("<fld")+1,nameIndex-1);
            		String afterStr=xmlStr.substring(index1+8,xmlStr.length());
            		xmlStr=fldBeforeStr+"colcnt_"+fld_+afterStr;
            		
             		
            	 }
                xslStr = xslClob.getSubString(1, (int) xslClob.length());

                xslStr = XMLUtil.escapeSpecialChars(xslStr);
                // Rlog.debug("saveform","getfilledformhtml. xmlStr" + xmlStr);
                // Rlog.debug("saveform","getfilledformhtml. xslStr" + xslStr);
                
                
                
                /*Remove colcount  is multiply defined in the same scope.*/
                
                while(xslStr.indexOf("colcount")!=-1){
                	int index1=xslStr.indexOf("/@colcount");
                	String fldBeforeStr=xslStr.substring(0,xslStr.indexOf("/@colcount"));
                	String fld_=fldBeforeStr.substring(fldBeforeStr.lastIndexOf("fld"),fldBeforeStr.length());
                	index1=xslStr.indexOf(fld_+"/@colcount");
                	String beforeStr=xslStr.substring(0,index1);
                	beforeStr=beforeStr.substring(0, beforeStr.lastIndexOf("colcount"))+"colcnt_"+fld_+beforeStr.substring(beforeStr.lastIndexOf("colcount")+8, beforeStr.length());
                	String afterStr=xslStr.substring(index1,xslStr.length());
                	afterStr=afterStr.replace(fld_+"/@colcount",fld_+"/@colcnt_"+fld_);
                	afterStr=afterStr.substring(0, afterStr.indexOf("$colcount"))+"$colcnt_"+fld_+afterStr.substring(afterStr.indexOf("$colcount")+9,afterStr.length());
                	
                	xslStr=beforeStr+afterStr;
                	
                }
                
                
                
                /*FormFieldDao formfieldDao =new FormFieldDao();
                formfieldDao.getFormfieldDetails(formId);
                ArrayList fldSystemIds=new ArrayList();
                fldSystemIds=formfieldDao.getFldSystemId();
                for(int i=0;i<fldSystemIds.size();i++){
                	int index1=0;
                	int lastIndex=0;
                	index1=xsl.indexOf(fldSystemIds.get(i)+"/@colcount");
                	if(index1==-1)
                		continue;
                	String beforeStr=xsl.substring(0,index1);
                	beforeStr=beforeStr.substring(0, beforeStr.lastIndexOf("colcount"))+"colcount_"+fldSystemIds.get(i)+beforeStr.substring(beforeStr.lastIndexOf("colcount")+8, beforeStr.length());
                	String afterStr=xsl.substring(index1,xsl.length());
                	afterStr=afterStr.replace(fldSystemIds.get(i)+"/@colcount",fldSystemIds.get(i)+"/@colcount_"+fldSystemIds.get(i));
                	afterStr=afterStr.substring(0, afterStr.indexOf("$colcount"))+"$colcount_"+fldSystemIds.get(i)+afterStr.substring(afterStr.indexOf("$colcount")+9,afterStr.length());
                	xsl=beforeStr+afterStr;
                }*/
                
                /*Remove colcount  is multiply defined in the same scope.(END)*/

               
             		 htmlStr = XMLUtil.transform(xmlStr, xslStr);  
             	 
                // Rlog.debug("saveform","getfilledformhtml. htmlStr" +
                // htmlStr);

                htmlStr = XMLUtil.unEscapeSpecialChars(htmlStr);

                htmlStr = StringUtil.replace(htmlStr, "er_textarea_tag<", "<");

                // add default form status dropdown to the html, this is an
                // application hardcoded field
                // not user defined
                htmlStr = addFormStatusToHtmlNoRole(htmlStr, formStatus);

	            htmlStr = htmlStr.replaceAll("e-Signature", LC.L_Esignature);
	            htmlStr = htmlStr.replaceAll("Data Entry Date", LC.L_Data_EntryDate);          
	            htmlStr = htmlStr.replaceAll("Valid e-Sign", LC.L_Valid_Esign);            
	            htmlStr = htmlStr.replaceAll("Invalid e-Sign", LC.L_Invalid_Esign); 
	            if( !"".equals(Configuration.eSignConf)){
	            	String eSignTag="<input type=\"password\" autocomplete=\"off\" name=\"eSign\" maxlength=\"8\" id=\"eSign\" onkeyup=\"ajaxvalidate('misc:'+this.id,4,'eSignMessage','Valid e-Sign','Invalid e-Sign','sessUserId')\">";
	   	            htmlStr=htmlStr.replace(eSignTag,"<input type=\"password\" autocomplete=\"off\" name=\"eSign\"  id=\"eSign\" style=\"background:url(./images/ic_lock_outline_gray_24dp.png) no-repeat; padding-left: 25px;\" class=\"input21\" onkeyup=\"ajaxvalidate('pass:'+this.id,-1,'eSignMessage','Valid-Password','Invalid-Password','sessUserId')\">");
	   	            htmlStr = htmlStr.replaceAll(LC.L_Esignature, "e-Password");
	               }
            }
            this.setHtmlStr(htmlStr);
            
            if ( !StringUtil.isEmpty(fkSpecimen))
            {
            	setHtOtherAttributes("fkSpecimen",fkSpecimen);
            }	
            
            return htmlStr;
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "saveform",
                            "SaveFormDao.getFormHtml EXCEPTION IN FETCHING form html from SP_GETFILLEDFORM_HTML"
                                    + ex);
            ex.printStackTrace();
            this.setHtmlStr("");
            return null;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /*
     * This method uses temporary clob to create the CLOB object.
     */
    private CLOB getCLOB(String clobData) throws Exception {
        CLOB tempClob = null;
        Connection conn = null;

        try {

            conn = getConnection();
            // create a new temporary CLOB
            tempClob = CLOB.createTemporary(conn, true, CLOB.DURATION_SESSION);

            // Open the temporary CLOB in readwrite mode to enable writing
            tempClob.open(CLOB.MODE_READWRITE);

            // Get the output stream to write
            Writer tempClobWriter = tempClob.getCharacterOutputStream();

            // Write the data into the temporary CLOB
            tempClobWriter.write(clobData);

            // Flush and close the stream
            tempClobWriter.flush();
            tempClobWriter.close();

            // Close the temporary CLOB
            tempClob.close();

        } catch (Exception exp) {
            // Free CLOB object
            tempClob.freeTemporary();
            //System.out.println("getClob exception " + exp);
            // do something
        }

        try {
            if (conn != null)
                conn.close();
        } catch (Exception e) {
        }
        return tempClob;
    }

    /*
     * This method generates the Printer Friendly Form XSL @param formId Id of
     * the form for which XSL is required @param linkFrom
     */

    public void generatePrintFormXsl(int formId) {
        int rows = 0;
        Clob htmlClob = null;
        String htmlStr = "";
        CallableStatement cstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_FILLEDFORM.SP_GENERATE_PRINTXSL(?)}");

            cstmt.setInt(1, formId);

            cstmt.execute();
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "saveform",
                            "SaveFormDao.generatePrintFormXsl EXCEPTION IN FETCHING form html from SP_GENERATE_PRINTXSL"
                                    + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /*
     * This method gets the Printer Friendly Form HTML for filled forms @param
     * formId Id of the form for which html is required @param filledFormId Id
     * of the form for which html is required @param linkFrom @returns html
     * string
     */

    public String getPrintFormHtml(int formId, int filledFormId, String linkFrom) {
        int rows = 0;
        Clob xmlClob = null;
        Clob xslClob = null;

        String xmlStr = "";
        String htmlStr = "";
        String xslStr = "";
        String findStr = "";
        int len = 0;
        String htmlstr1 ="";
        String htmlstr2 ="";

        CallableStatement cstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_FILLEDFORM.SP_GETPRINTFORM_HTML(?,?,?,?,?)}");

            cstmt.setInt(1, formId);
            cstmt.setInt(2, filledFormId);
            cstmt.setString(3, linkFrom);
            cstmt.registerOutParameter(4, java.sql.Types.CLOB);
            cstmt.registerOutParameter(5, java.sql.Types.CLOB);
            cstmt.execute();

            xmlClob = cstmt.getClob(4);
            xslClob = cstmt.getClob(5);

            if (!(xmlClob == null)) {

                xmlStr = xmlClob.getSubString(1, (int) xmlClob.length());
                while(xmlStr.indexOf("colcount = ")!=-1 || xmlStr.indexOf("colcount=")!=-1){
            		int index1= (xmlStr.indexOf("colcount = ")==-1) ? xmlStr.indexOf("colcount=") : xmlStr.indexOf("colcount = ");
            		String fldBeforeStr= xmlStr.substring(0,index1);
            		int nameIndex=fldBeforeStr.lastIndexOf("name = ")==-1 ? fldBeforeStr.lastIndexOf("name=") : fldBeforeStr.lastIndexOf("name = ");
            		String fld_=fldBeforeStr.substring(fldBeforeStr.lastIndexOf("<fld")+1,nameIndex-1);
            		String afterStr=xmlStr.substring(index1+8,xmlStr.length());
            		xmlStr=fldBeforeStr+"colcnt_"+fld_+afterStr;
            		
             		
            	 }
                xslStr = xslClob.getSubString(1, (int) xslClob.length());
            }
            xslStr = XMLUtil.escapeSpecialChars(xslStr);
            findStr = "<BODY id=\"forms\" onFocus=";
            len = findStr.length();
            int pos1 = xslStr.indexOf(findStr);
            if(pos1>0){
            int pos2 = xslStr.indexOf("();\">");
            len=len-8;
		    htmlstr1 = xslStr.substring(0,pos1+len);
		    htmlstr2 = xslStr.substring(pos2+4,xslStr.length());
		    xslStr = htmlstr1+htmlstr2;
            }
            
            /*Remove colcount  is multiply defined in the same scope.*/
            
            while(xslStr.indexOf("colcount")!=-1){
            	int index1=xslStr.indexOf("/@colcount");
            	String fldBeforeStr=xslStr.substring(0,xslStr.indexOf("/@colcount"));
            	String fld_=fldBeforeStr.substring(fldBeforeStr.lastIndexOf("fld"),fldBeforeStr.length());
            	index1=xslStr.indexOf(fld_+"/@colcount");
            	String beforeStr=xslStr.substring(0,index1);
            	beforeStr=beforeStr.substring(0, beforeStr.lastIndexOf("colcount"))+"colcnt_"+fld_+beforeStr.substring(beforeStr.lastIndexOf("colcount")+8, beforeStr.length());
            	String afterStr=xslStr.substring(index1,xslStr.length());
            	afterStr=afterStr.replace(fld_+"/@colcount",fld_+"/@colcnt_"+fld_);
            	afterStr=afterStr.substring(0, afterStr.indexOf("$colcount"))+"$colcnt_"+fld_+afterStr.substring(afterStr.indexOf("$colcount")+9,afterStr.length());
            	
            	xslStr=beforeStr+afterStr;
            	
            }
            
            
            
            /*FormFieldDao formfieldDao =new FormFieldDao();
            formfieldDao.getFormfieldDetails(formId);
            ArrayList fldSystemIds=new ArrayList();
            fldSystemIds=formfieldDao.getFldSystemId();
            for(int i=0;i<fldSystemIds.size();i++){
            	int index1=0;
            	int lastIndex=0;
            	index1=xsl.indexOf(fldSystemIds.get(i)+"/@colcount");
            	if(index1==-1)
            		continue;
            	String beforeStr=xsl.substring(0,index1);
            	beforeStr=beforeStr.substring(0, beforeStr.lastIndexOf("colcount"))+"colcount_"+fldSystemIds.get(i)+beforeStr.substring(beforeStr.lastIndexOf("colcount")+8, beforeStr.length());
            	String afterStr=xsl.substring(index1,xsl.length());
            	afterStr=afterStr.replace(fldSystemIds.get(i)+"/@colcount",fldSystemIds.get(i)+"/@colcount_"+fldSystemIds.get(i));
            	afterStr=afterStr.substring(0, afterStr.indexOf("$colcount"))+"$colcount_"+fldSystemIds.get(i)+afterStr.substring(afterStr.indexOf("$colcount")+9,afterStr.length());
            	xsl=beforeStr+afterStr;
            }*/
            
            /*Remove colcount  is multiply defined in the same scope.(END)*/
            
            
         		 htmlStr = XMLUtil.transform(xmlStr, xslStr);  
         	 
            htmlStr = XMLUtil.unEscapeSpecialChars(htmlStr);

            /*
             * added by sonia abrol, 02/03/06, apparently, the xsl is escaped
             * twice somewhere, so 1 unescape leaves the escaped chars in
             * formatting in preview is not displayed properly. for time being
             * unescaping the xsl again, will have to look into the actual
             * problem
             */
            htmlStr = XMLUtil.unEscapeSpecialChars(htmlStr);

            htmlStr = StringUtil.replace(htmlStr, "er_textarea_tag<", "<");

            if (htmlStr.indexOf("[VELSYSTEMDATE]") >= 0)
                htmlStr = StringUtil.replace(htmlStr, "[VELSYSTEMDATE]",
                        DateUtil.getCurrentDate());
            return htmlStr;

        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "saveform",
                            "SaveFormDao.getPrintFormHtml EXCEPTION IN FETCHING form html from SP_GETPRINTFORM_HTML"
                                    + ex);
            return null;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /*
     * This method adds the Form status field to the form. Form status is stored
     * in er_codeslst @param html string @param Status Id to be selected in the
     * dropdown @returns html string
     */

    public String addFormStatusToHtml(String htmlStr, int statValue) {
        // make status dropdown
        String statDropdown = "";
        String insertStr = "";
        String strBeforeEsign = "";
        String strTable = "";
        int index = -1;
        int lockDownValue=0;
        int incompleteValue = 0;
        int lockDownReviewValue = 0;
        
        CodeDao cdao = new CodeDao();
        lockDownValue= cdao.getCodeId("fillformstat","lockdown");
        incompleteValue= cdao.getCodeId("fillformstat","incomplete");
        lockDownReviewValue= cdao.getCodeId("fillformstat","soft_lock");

        int tdpos = 0;
        int pos = 0;
        int tablepos = 0;
        Hashtable htMore = new Hashtable();
        String studyRolePk="";
        
        boolean valueFound = false;
        
        String currentSubType="";
        
        
        StringBuffer sbuffer = new StringBuffer(htmlStr);
        
        //get the more params Hashtable
        htMore = getHtMoreParams();
        
        if (htMore != null)
        {
        	
        	if (htMore.containsKey("teamRolePK"))
        	{
        		studyRolePk = (String) htMore.get("teamRolePK");
        	}
        }
        
        
        CodeDao cdStatus = new CodeDao();
        
        if (statValue > 0)
        {
        	currentSubType = cdStatus.getCodeSubtype(statValue);
        	
        	if (StringUtil.isEmpty(currentSubType))
        	{
        		currentSubType = "";
        	}
        }	
         
       	cdStatus.getCodeValuesForStudyRole("fillformstat",studyRolePk);
       	//when role is not passed, gets the value with default codelst_custom_col1
        
        ArrayList cDesc=cdStatus.getCDesc();
        ArrayList cId = cdStatus.getCId();
        ArrayList cSubType = cdStatus.getCSubType();
        
        String subTypeStr = "";
        
        if (statValue == 0) {
        	
        	if(!"[FORMRESPSTAT_NOT_CONFIGURED]".equalsIgnoreCase(CFG.DEFAULT_FORMRESPSTAT_CONFIG)){
        		
        		index = cSubType.indexOf(CFG.DEFAULT_FORMRESPSTAT_CONFIG);
        		if(index < 0)
        			statValue = 0;
        		else
        			statValue = ((Integer) cId.get(index)).intValue();        		
        	}else{
                        
        		if (cSubType != null) {
        			index = cSubType.indexOf("working");
        			if (index < 0)
        				statValue = 0;
        			else
        				statValue = ((Integer) cId.get(index)).intValue();
        		}
        	}
        }
        this.setFilledFormStat(statValue);
   
        this.setLockDownStat(lockDownValue);  
        this.setLockDownReviewStat(lockDownReviewValue);
        
        //statDropdown = cdStatus.toPullDown("er_def_formstat", statValue);
        Integer codeId = null;
        StringBuffer statDropdownSB = new StringBuffer();
        String hideStr  ="";
        
            int counter = 0;
            statDropdownSB.append("<SELECT NAME='er_def_formstat' id='er_def_formstat' onChange=ShowLockDownMessage()>");
            Integer val = new java.lang.Integer(statValue);

            

            for (counter = 0; counter <= cDesc.size() - 1; counter++) {
                codeId = (Integer) cId.get(counter);
                
                hideStr = cdStatus.getCodeHide(counter) ;
    			
    			if (StringUtil.isEmpty(hideStr))
    			{
    				hideStr = "N";
    			}
    			
    			subTypeStr = (String)cSubType.get(counter);
    			
    			if ( subTypeStr.equals("lockdown") ||  subTypeStr.equals("notconsent") || subTypeStr.equals("consent") )
    			{
    			   setLockdownStatuses(codeId);
    			}  
    			if(subTypeStr.equals("soft_lock")){
    				setLockdownReviewStatuses(codeId);
    			}
    			 if(subTypeStr.equals("unlock")){ /*Hide the unlock from the list of Form Status under bug no #20922*/
    				 continue;
    			 }
    			
                if (statValue == codeId.intValue()) {
                	
                	if(hideStr.equals("N")){
                	valueFound = true;
                	
                	statDropdownSB.append("<OPTION value = " + codeId + " SELECTED>"
                            + cDesc.get(counter) + "</OPTION>");
                	}
                } else {
                	
                	if (hideStr.equals("N"))
                	{
                	  		statDropdownSB.append("<OPTION value = " + codeId + ">"
                            + cDesc.get(counter) + "</OPTION>");
                	}
                }
            }
            
            if (val.toString().equals("") || val.toString().equals(null)
                    || val.toString().equals("0") || valueFound== false) {
            	statDropdownSB
                        .append("<OPTION value ='' SELECTED>"+LC.L_Select_AnOption+"</OPTION>");
            }
            
            if (currentSubType.equals("lockdown") && valueFound==false)
            {
            	setLockdownStatuses(Integer.valueOf(statValue));
            }
            if(currentSubType.equals("soft_lock") && valueFound==false){
            	setLockdownReviewStatuses(Integer.valueOf(statValue));
            }
            
            statDropdownSB.append("</SELECT>");
        
            statDropdown=statDropdownSB.toString();
    

        // add the status dropdown before e-signature in the form html
        // find position of e-signature
        pos = htmlStr.lastIndexOf(">e-Signature");
        strBeforeEsign = htmlStr.substring(1, pos);

        // find the start <td of e-signature for inserting status dropdown
        tdpos = strBeforeEsign.lastIndexOf("<td");

        insertStr = "<td width=\"15%\" id=\"def_form_status_label\">"+ LC.L_Form_ResponseStatus +"<FONT class=\"Mandatory\">* </FONT></td><td width=\"25%\">"
                + statDropdown + " </td>" +
                "<input type=\"hidden\" name=\"lockDownStatus\" id=\"lockDownStatus\"  value=\""+lockDownValue+ "\">" +
                "<input type=\"hidden\" name=\"lockDownReviewStatus\" id=\"lockDownReviewStatus\"  value=\""+lockDownReviewValue+ "\">" +
                " <input type=\"hidden\" name=\"incompleteStatusValue\" id=\"incompleteStatusValue\"  value=\""+incompleteValue+ "\">" +
                "<input type=\"hidden\" name=\"currentStatus\" id=\"currentStatus\"  value=\""+statValue+ "\">"
                ;
        
        
        
        tdpos=tdpos+1;
        sbuffer.insert(tdpos, insertStr);
        htmlStr = sbuffer.toString();
        if (htmlStr.indexOf("[VELSYSTEMDATE]") >= 0)
            htmlStr = StringUtil.replace(htmlStr, "[VELSYSTEMDATE]", DateUtil
                    .getCurrentDate());
        return htmlStr;

    }


    public String addFormStatusToHtmlNoRole(String htmlStr, int statValue) {
        // make status dropdown
        String statDropdown = "";
        String insertStr = "";
        String strBeforeEsign = "";
        String strTable = "";
        int index = -1;
        int lockDownValue=0;
        int incompleteValue = 0;
        int lockDownReviewValue = 0;
        
        CodeDao cdao = new CodeDao();
        lockDownValue= cdao.getCodeId("fillformstat","lockdown");
        incompleteValue= cdao.getCodeId("fillformstat","incomplete");
        lockDownReviewValue= cdao.getCodeId("fillformstat","soft_lock");

        int tdpos = 0;
        int pos = 0;
        int tablepos = 0;        
        boolean valueFound = false;        
        String currentSubType="";        
        
        StringBuffer sbuffer = new StringBuffer(htmlStr);
        
        CodeDao cdStatus = new CodeDao();
        
        if (statValue > 0)
        {
        	currentSubType = cdStatus.getCodeSubtype(statValue);
        	
        	if (StringUtil.isEmpty(currentSubType))
        	{
        		currentSubType = "";
        	}
        }	
         
       	cdStatus.getCodeValues("fillformstat");
        
        ArrayList cDesc=cdStatus.getCDesc();
        ArrayList cId = cdStatus.getCId();
        ArrayList cSubType = cdStatus.getCSubType();
        
        String subTypeStr = "";
        
        if (statValue == 0) {
        	
        	if(!"[FORMRESPSTAT_NOT_CONFIGURED]".equalsIgnoreCase(CFG.DEFAULT_FORMRESPSTAT_CONFIG)){
        		
        		index = cSubType.indexOf(CFG.DEFAULT_FORMRESPSTAT_CONFIG);
        		if(index < 0)
        			statValue = 0;
        		else
        			statValue = ((Integer) cId.get(index)).intValue();        		
        	}else{
                
		if (cSubType != null) {
			index = cSubType.indexOf("working");
			if (index < 0)
				statValue = 0;
			else
				statValue = ((Integer) cId.get(index)).intValue();
				}
        	}

        }
        this.setFilledFormStat(statValue);
   
        this.setLockDownStat(lockDownValue);  
        this.setLockDownReviewStat(lockDownReviewValue);
        
        //statDropdown = cdStatus.toPullDown("er_def_formstat", statValue);
        Integer codeId = null;
        StringBuffer statDropdownSB = new StringBuffer();
        String hideStr  ="";
        
            int counter = 0;
            statDropdownSB.append("<SELECT NAME='er_def_formstat' id='er_def_formstat' onChange=ShowLockDownMessage()>");
            Integer val = new java.lang.Integer(statValue);

            

            for (counter = 0; counter <= cDesc.size() - 1; counter++) {
                codeId = (Integer) cId.get(counter);
                
                hideStr = cdStatus.getCodeHide(counter) ;
    			
    			if (StringUtil.isEmpty(hideStr))
    			{
    				hideStr = "N";
    			}
    			
    			subTypeStr = (String)cSubType.get(counter);
    			
    			if ( subTypeStr.equals("lockdown") ||  subTypeStr.equals("notconsent") || subTypeStr.equals("consent") )
    			{
    			   setLockdownStatuses(codeId);
    			}  
    			if(subTypeStr.equals("soft_lock")){
    				setLockdownReviewStatuses(codeId);
    			}
    			 
    			
                if (statValue == codeId.intValue()) {
                	
                	if(!"[FORMRESPSTAT_NOT_CONFIGURED]".equalsIgnoreCase(CFG.DEFAULT_FORMRESPSTAT_CONFIG)){
                    	
                		if(hideStr.equalsIgnoreCase("N")){
                	
                			valueFound = true;
                	
                			statDropdownSB.append("<OPTION value = " + codeId + " SELECTED>"
                					+ cDesc.get(counter) + "</OPTION>");
                		}
                	
                	}
                	else{
                		
                		valueFound = true;
                    	
            			statDropdownSB.append("<OPTION value = " + codeId + " SELECTED>"
            					+ cDesc.get(counter) + "</OPTION>");
                	}
                } else {
                	
                	if (hideStr.equals("N"))
                	{
                	  		statDropdownSB.append("<OPTION value = " + codeId + ">"
                            + cDesc.get(counter) + "</OPTION>");
                	}
                }
            }
            
            if (val.toString().equals("") || val.toString().equals(null)
                    || val.toString().equals("0") || valueFound== false) {
            	statDropdownSB
                        .append("<OPTION value ='' SELECTED>"+LC.L_Select_AnOption+"</OPTION>");
            }
            
            if (currentSubType.equals("lockdown") && valueFound==false)
            {
            	setLockdownStatuses(Integer.valueOf(statValue));
            }
            if(currentSubType.equals("soft_lock") && valueFound==false){
            	setLockdownReviewStatuses(Integer.valueOf(statValue));
            }
            
            statDropdownSB.append("</SELECT>");
        
            statDropdown=statDropdownSB.toString();
    

        // add the status dropdown before e-signature in the form html
        // find position of e-signature
        pos = htmlStr.lastIndexOf(">e-Signature");
        strBeforeEsign = htmlStr.substring(1, pos);

        // find the start <td of e-signature for inserting status dropdown
        tdpos = strBeforeEsign.lastIndexOf("<td");

        insertStr = "<td width=\"15%\" id=\"def_form_status_label\">"+ LC.L_Form_ResponseStatus +"<FONT class=\"Mandatory\">* </FONT></td><td width=\"25%\">"
                + statDropdown + " </td><input type=\"hidden\" name=\"lockDownStatus\" id=\"lockDownStatus\"  value=\""+lockDownValue+ "\">" +
                "<input type=\"hidden\" name=\"lockDownReviewStatus\" id=\"lockDownReviewStatus\"  value=\""+lockDownReviewValue+ "\">" +
                " <input type=\"hidden\" name=\"incompleteStatusValue\" id=\"incompleteStatusValue\"  value=\""+incompleteValue+ "\">" +
                "<input type=\"hidden\" name=\"currentStatus\" id=\"currentStatus\"  value=\""+statValue+ "\">"
                ;
        
        
        
         tdpos=tdpos+1;
        sbuffer.insert(tdpos, insertStr);
        htmlStr = sbuffer.toString();
        if (htmlStr.indexOf("[VELSYSTEMDATE]") >= 0)
            htmlStr = StringUtil.replace(htmlStr, "[VELSYSTEMDATE]", DateUtil
                    .getCurrentDate());
        return htmlStr;

    }

    public void generateFormLayout(int formId) {
        int success = 0;
        CallableStatement cstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call PKG_FORMREP.SP_GENFORMLAYOUT(?)}");

            cstmt.setInt(1, formId);

            cstmt.execute();
        } catch (SQLException ex) {
            Rlog.fatal("saveform",
                    "Execption in SaveFormDao.generateFormLayout: " + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /*
     * This method delete the answered response of Form, logical deletion @param
     * formFilledId pk of response @param formDispLocation indicates the Form
     * LinkedTo @param modifiedBy userId of the user who deletes the response
     * @param ipAdd ipAdd of the client machine from which the record is deleted
     */

    public int deleteFilledFormResp(String filledFormId,
            String formDispLocation, String lastModifiedBy, String ipAdd) {
        Rlog.debug("saveform", "SaveFormDao.inside deleteFilledFormResp");

        int fillFormId = StringUtil.stringToNum(filledFormId);

        Connection conn = null;

        PreparedStatement stmt = null;
        String sqlStr = "";

        StringBuffer sqlSB = new StringBuffer();

        try {
            conn = getConnection();

            if (formDispLocation.equals("S") || formDispLocation.equals("SA")) {
                sqlStr = "update er_studyforms set record_type='D', last_modified_by=?, ip_add=? where pk_studyforms=?";

            } else if (formDispLocation.equals("SP")
                    || formDispLocation.equals("PA")) {
                sqlStr = "update er_patforms set record_type='D', last_modified_by=?, ip_add=? where pk_patforms=?";

            } else if (formDispLocation.equals("A")) {
                sqlStr = "update er_acctforms set record_type='D', last_modified_by=?, ip_add=? where pk_acctforms=? and acct_formtype is null";
            } else if (formDispLocation.equals("C")) {
                sqlStr = "update er_crfforms set record_type='D', last_modified_by=?, ip_add=? where pk_crfforms=? ";
            }
            else if (formDispLocation.equals("ORG") || formDispLocation.equals("USR") || formDispLocation.equals("NTW")|| formDispLocation.equals("SNW")) {
                sqlStr = "update er_acctforms set record_type='D', last_modified_by=?, ip_add=? where pk_acctforms=? and acct_formtype is not null";
            } 

            stmt = conn.prepareStatement(sqlStr);
            stmt.setString(1, lastModifiedBy);
            stmt.setString(2, ipAdd);
            stmt.setInt(3, fillFormId);
            stmt.execute();

            return 0;
        } catch (Exception ex) {
            Rlog.fatal("saveform",
                    "SaveFormDao.deleteFilledFormResp EXCEPTION IN deleting form data"
                            + ex);
            ex.printStackTrace();
            return -1;
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
            }

            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /*
     * This method performs the various tasks when a form status is changed to
     * active it creates a new verion generates the Printer Friendly Form XSL
     * generates form report data @param formId Id of the form for which XSL is
     * required @param linkFrom
     */

    public void doFormActiveStatusTasks(int formId,boolean migrateVersion) {
        int rows = 0;
        CallableStatement cstmt = null;
        Connection conn = null;
        int migrateFlag = 0;
        try {
        	
        	if (migrateVersion)
        	{
        		migrateFlag = 1;
        	}
        	
            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_LNKFORM.SP_FORM_ACTIVESTATUS_TASKS(?,?)}");

            cstmt.setInt(1, formId);
            cstmt.setInt(2, migrateFlag);

            cstmt.execute();
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "saveform",
                            "SaveFormDao.doFormActiveStatusTasks EXCEPTION IN FETCHING form html from SP_GENERATE_PRINTXSL"
                                    + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /*
     * This method gets the Form Version XML @param formVersion Id of the form
     * version for updating xml data is required
     */
    public String getFormVerXML(int formLibVer) {

        Connection conn = null;
        PreparedStatement pstmt = null;
        OraclePreparedStatement stmt;

        String xmlStr = "";

        try {
            conn = getConnection();

            String query = "select c.formlibver_xml.getClobVal() from er_formlibver c where pk_formlibver = ?";

            pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, formLibVer);

            ResultSet rset = pstmt.executeQuery();
            OracleResultSet orset = (OracleResultSet) rset;

            while (orset.next()) {

                // retrieve form xml document from database

                Clob xmlCLob = null;

                xmlCLob = orset.getClob(1);

                if (!(xmlCLob == null)) {
                    xmlStr = xmlCLob.getSubString(1, (int) xmlCLob.length());
                }

            }
            return xmlStr;
        }

        catch (Exception ex) {
            Rlog.fatal("saveform",
                    "SaveFormDao.getFormVerXML EXCEPTION IN getting XML" + ex);
            return "";
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

        // end of method
    }

    public String createValue(String colOrigSysId, String value, String colType) {
        int index = -1;
        String dispVal = "";
        String finalValue = "";
        String[] valueArr = null;
        String tempVal = "";
        try {
            if (value.indexOf("[VELCOMMA]") >= 0) {
                valueArr = StringUtil.strSplit(value, "[VELCOMMA]", false);
            }
            if (valueArr != null) {
                for (int i = 0; i < valueArr.length; i++) {
                    tempVal = valueArr[i];
                    index = this.getColOrigSysIdWValueSet().indexOf(
                            (String) colOrigSysId + ":" + tempVal);
                    if ((index >= 0)) {
                        dispVal = (String) this.getColResponseDispValSet().get(
                                index);
                        if (finalValue.length() > 0)
                            finalValue = finalValue + ",[" + dispVal + "]";
                        else
                            finalValue = "[" + dispVal + "]";
                    }
                }
                finalValue = finalValue + "[VELSEP1]" + value;
            }// if valueArr!=null
            else {
                index = this.getColOrigSysIdWValueSet().indexOf(
                        colOrigSysId + ":" + value);
                
                //System.out.println("get value index" + index) ;
                
                if (index >= 0) {
                    dispVal = (String) this.getColResponseDispValSet().get(
                            index);
                    
                  //  System.out.println("get value dispVal" + dispVal) ;
                    
                    if (colType.equals("MC"))
                        finalValue = "[" + dispVal + "][VELSEP1]" + value;
                    else
                        finalValue = dispVal + "[VELSEP1]" + value;

                }
            }
            return finalValue;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void populateFormAttributes(int formId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer paramString = new StringBuffer();
        // Prepare the SQL
        /*
         * for (int i=0;i<getHouseKeeper().getParams().size();i++) { if
         * (paramString.length()>0)
         * paramString.append(","+getHouseKeeper().getParams().get(i)); else
         * paramString.append(getHouseKeeper().getParams().get(i)); }
         */
        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("SELECT distinct mp_origsysid||':'||fldresp_dataval as mp_origsysid,fldresp_dispval"
                            + " FROM ER_MAPFORM a,ER_FLDRESP b WHERE fk_form=? "
                            + " AND a.MP_PKFLD=b.FK_FIELD ");
            pstmt.setInt(1, formId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                this.setColOrigSysIdWValueSet(rs.getString("mp_origsysid"));
                this.setColResponseDispValSet(rs.getString("fldresp_dispval"));
                
                //System.out.println("get value fldresp_dispval" + rs.getString("fldresp_dispval")) ;
            }
        } catch (SQLException ex) {
            Rlog.fatal("saveform",
                    "SaveFormDao.populateFormAttributes EXCEPTION IN FETCHING"
                            + ex);
            ex.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public int saveFormsLinear(String sql, String mode) {
        int ret = -1, index = -1, hardCodedCount = 6;
        String colType = "";
        String colValue = "", tempValue = "";
        String colSysId = "";
        Connection conn = null;
        // PreparedStatement stmt=null;
        PreparedStatement stmt = null;
        ArrayList paramIds = getHouseKeeper().getParams();
        ArrayList paramValues = getHouseKeeper().getParamValues();
        String idForLinear = "";
        InputStream in=null;
        int strLen=0;
        int fk_sch_events1 = 0; //for new CRF implementation
        String formTypeTable = "";
        String specimen = "";
        specimen = getHouseKeeper().getSpecimenPk();
        String modPk = "";
        modPk = getHouseKeeper().getModPK();
        System.out.println("modPk"+modPk);
        try {
        	
        	if (getColNames().size() <= 0 )
        	{
        		return -1; //missing form mapping
        		
        	}
            conn = getConnection();

            stmt = conn.prepareStatement(sql);
            
            //System.out.println("SQL Statement - " + sql);
            
            // pk_formslinear,fk_filledform,fk_form,form_type,ID,fk_patprot,filldate"
            // +
            // pk_formslinear,fk_filledform,fk_form,form_type,ID,fk_patprot,filldate
            // ",form_completed,export_flag
            stmt.setInt(1, getHouseKeeper().getId());
            stmt.setString(2, getHouseKeeper().getFormId());
            
            fk_sch_events1 = StringUtil.stringToNum(getHouseKeeper().getSchEvent());

            if (getHouseKeeper().getDisplayType().equals("S")
                    || getHouseKeeper().getDisplayType().equals("SA")) {
                stmt.setString(3, "S");
                idForLinear = getHouseKeeper().getStudyId();
                
                formTypeTable = "S";

            }
            if (getHouseKeeper().getDisplayType().equals("SP")
                    || getHouseKeeper().getDisplayType().equals("PA")
                    || getHouseKeeper().getDisplayType().equals("PR")
                    || getHouseKeeper().getDisplayType().equals("PS")) {
                stmt.setString(3, "P");
                idForLinear = getHouseKeeper().getPatientId();
                formTypeTable = "P";
            }
            if (getHouseKeeper().getDisplayType().equals("A")) {
                stmt.setString(3, "A");
                idForLinear = getHouseKeeper().getAccountId();
                
                formTypeTable = "A";
            }
            if (getHouseKeeper().getDisplayType().equals("ORG")) {
                stmt.setString(3, "ORG");
                idForLinear = getHouseKeeper().getModPK();
                
                formTypeTable = "ORG";
            }
            if (getHouseKeeper().getDisplayType().equals("USR")) {
                stmt.setString(3, "USR");
                idForLinear = getHouseKeeper().getModPK();
                
                formTypeTable = "USR";
            }
            
            if (getHouseKeeper().getDisplayType().equals("NTW")) {
                stmt.setString(3, "NTW");
                idForLinear = getHouseKeeper().getModPK();
                
                formTypeTable = "NTW";
            }
			if (getHouseKeeper().getDisplayType().equals("SNW")) {
                stmt.setString(3, "SNW");
                idForLinear = getHouseKeeper().getModPK();
                
                formTypeTable = "SNW";
            }

            // Set the formFillDate to value entered for er_def_date_01
            stmt.setDate(4, DateUtil.dateToSqlDate(DateUtil.stringToDate(
                    (String) paramValues
                            .get(paramIds.indexOf("er_def_date_01")),
                    null)));

            stmt.setString(5, getHouseKeeper().getFormCompleted());
            if (mode.equals("insert"))
                stmt.setString(6, getHouseKeeper().getCreator());
            else if (mode.equals("update"))
                stmt.setString(6, getHouseKeeper().getLastModifiedBy());

            for (int i = 0; i < getColNames().size(); i++) {
                // if (paramIds.size())
                colSysId = (String) this.getColSysIds().get(i);
                index = paramIds.indexOf(colSysId);
                if (index >= 0) {
                    colType = (String) this.getColTypes().get(i);
                    if (colType.equals("MD") || colType.equals("MC")
                            || colType.equals("MR")) {
                        colValue = this.createValue((String) this
                                .getColOrigSysIds().get(i),
                                (String) paramValues.get(index), colType);
                        if (colValue != null) {
                            if (colValue.length() >= 4000)
                                colValue = colValue.substring(0, 4000);
                        }

                        stmt.setString(hardCodedCount + 1 + i, colValue);
                    } else {
                        tempValue = ((String) paramValues.get(index));
                        
                        if (tempValue != null) {
                            if (tempValue.length() >= 2000) {
                                //System.out.println("before" + tempValue.length());
                                strLen=tempValue.length();
                                 if (strLen>=4000){ 
                                     tempValue = tempValue.substring(0, 4000);
                                     strLen=4000;
                                 } 
                                  //System.out.println("after" + tempValue.length());
                                in = new java.io.ByteArrayInputStream(tempValue.getBytes("UTF-8"));
                                stmt.setAsciiStream(hardCodedCount+1+i,in,strLen);
                            }
                            else
                            {
                                //System.out.println("Insert"+tempValue);
                                stmt.setString(hardCodedCount + 1 + i, tempValue);
                            }
                        }
                        
                        // StringReader stRead=new StringReader(tempValue);

                         //InputStream in = new java.io.ByteArrayInputStream(tempValue.getBytes("UTF-8"));
                        
                        // stmt.setFormOfUse(hardCodedCount+1+i,oracle.jdbc.OraclePreparedStatement.FORM_NCHAR);
                         //stmt.setAsciiStream(hardCodedCount+1+i,in,4000);
                        
                        
                         
                        // stmt.setCharacterStream(hardCodedCount+1+i,stRead,1100);
                       // stmt.setString(hardCodedCount + 1 + i, tempValue);

                    }
                } // if index>0
                else
                    stmt.setString(hardCodedCount + 1 + i, null);

            }

            if (mode.equals("insert")) {
                stmt.setString(hardCodedCount + getColNames().size() + 1,
                        idForLinear);
                stmt.setString(hardCodedCount + getColNames().size() + 2,
                        getHouseKeeper().getPatprotId());
                
                //for new CRF implementation 
                stmt.setInt(hardCodedCount + getColNames().size() + 3, fk_sch_events1);
                stmt.setInt(hardCodedCount + getColNames().size() + 4, fk_sch_events1);
                stmt.setInt(hardCodedCount + getColNames().size() + 5, fk_sch_events1);
                stmt.setInt(hardCodedCount + getColNames().size() + 6, fk_sch_events1);
                
                //for getting the formlibver version number
                
                stmt.setInt(hardCodedCount + getColNames().size() + 7, getHouseKeeper().getId());
                
                
                stmt.setString(hardCodedCount + getColNames().size() + 8, formTypeTable);
                stmt.setString(hardCodedCount + getColNames().size() + 9, specimen);
                
            }

            if (mode.equals("update")) {

                stmt.setInt(hardCodedCount + getColNames().size() + 1,
                        getHouseKeeper().getId());
                stmt.setString(hardCodedCount + getColNames().size() + 2,
                        getHouseKeeper().getFormId());

            }

            ret = stmt.executeUpdate();

        } catch (Exception ex) {
            Rlog.fatal("saveform", "SaveFormDao.saveLinearData" + ex);
            ex.printStackTrace();
            return -1;
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
            }

            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return ret;
    }

   
    /**
     * 
     * @param mode -
     *            Mode to check if SQL creation is for new Insertion or an
     *            update
     * @return SQL as an String
     */
    private String createSQL(String mode) {
        String sql = "";
        String sqlValue = "";
        int filledForm = getHouseKeeper().getId();
        int fk_sch_events1 = 0;
        
                
        if (mode.equals("insert")) {
        	
        	fk_sch_events1 = StringUtil.stringToNum(getHouseKeeper().getSchEvent());
        	
            sql = "insert into er_formslinear(pk_formslinear,fk_filledform,fk_form,form_type,filldate"
                    + ",form_completed,export_flag,created_on,creator";
            // sqlValue="values(seq_er_formslinear.nextval, "+
            // filledForm+","+houseKeeper.getFormId()+","+houseKeeper.getDisplayType()
            // +","+houseKeeper.getId()+","+houseKeeper.getPatprotId();
            sqlValue = "values(seq_er_formslinear.nextval,?,?,?,?,?,0,sysdate,?";
            for (int i = 0; i < this.getColNames().size(); i++) {
                sql = sql + "," + this.getColNames().get(i);
                sqlValue = sqlValue + ",?";
            }
            sql = sql + " ,ID,fk_patprot,fk_sch_events1,event_name,visit_name,protocol_name,form_version,fk_specimen)";
            
            sqlValue = sqlValue + ", ?,?,?,f_get_schevent(?),f_get_schvisit(?),f_get_schprotocol(?),pkg_formver.f_get_versionumber(?,?), ?)";
            sql = sql + " " + sqlValue;
            
        } else if (mode.equals("update")) {
            sql = "update er_formslinear set fk_filledform=?,fk_form=?,form_type=? , filldate=?,form_completed=?,"
                    + "export_flag=0,last_modified_date=sysdate,last_modified_by=? ";
            for (int i = 0; i < this.getColNames().size(); i++) {
                sql = sql + "," + this.getColNames().get(i) + "=?";

            }

            /*
             * if (!(getHouseKeeper().getDisplayType()).equals("PA")) { sql =
             * sql + " ,ID=?,fk_patprot=? "; }
             */

            sql = sql + " where fk_filledform=? and fk_form=?";
        }

        return sql;
    }
    
    
    public String getForeignKeyField(int formId,int parentId)
    {
    	String returnStr="";
    	Connection conn=null;
    	PreparedStatement pstmt=null;
    	String sql="select  fld_systemid from er_formfld,er_fldlib where fk_field=pk_field and fk_formsec in (select pk_formsec from er_formsec where fk_formlib=?) " + 
    	     " and FORMFLD_foreignkeyflag='Y' and formfld_foreignkey=? " ;
 try {
    conn = getConnection();
    pstmt = conn.prepareStatement(sql);
    pstmt.setInt(1, formId);
    pstmt.setInt(2, parentId);

    ResultSet rset = pstmt.executeQuery();
    

    while (rset.next()) {

        
    	returnStr=rset.getString("fld_systemid");
        
}
    rset.close();
}
catch (Exception ex) {
    Rlog.fatal("saveform",
            "SaveFormDao.getForeignKeyField EXCEPTION " + ex);
    	ex.printStackTrace();
    return "";
} finally {
    try {
        if (pstmt != null)
            pstmt.close();
    } catch (Exception e) {
    }
    try {
        if (conn != null)
            conn.close();
    } catch (Exception e) {
    }

}
return returnStr;

    }
    
    public String getChildDetails(int formId,String linkFrom,int responseId)
    {
    	String whereClauseStr="";
    	Connection conn=null;
    	PreparedStatement pstmt=null;
    	String returnStr="";
    	
        if (linkFrom.equals("A")) {
            whereClauseStr = "ER_ACCTFORMS WHERE pk_acctforms=? and acct_formtype is null ";
            
        } else if ((linkFrom.equals("SA")) || (linkFrom.equals("S"))) {
        	whereClauseStr = "ER_STUDYFORMS WHERE pk_studyforms=?";
        } else if ((linkFrom.equals("SP")) || (linkFrom.equals("PA"))) {
        	whereClauseStr = "ER_PATFORMS WHERE pk_patforms=?";
        } else if (linkFrom.equals("C")) {
        	whereClauseStr = "ER_CRFFORMS WHERE pk_crfforms=?";
        }
        else if (linkFrom.equals("ORG") || linkFrom.equals("USR") || linkFrom.equals("NTW")|| linkFrom.equals("SNW")) {
        	whereClauseStr = "ER_ACCTFORMS WHERE pk_acctforms=? and acct_formtype is not null " ;
        }
        
        String sql="	SELECT mp_mapcolname FROM ER_MAPFORM WHERE mp_pkfld IN " + 
    			" (SELECT fk_field FROM ER_FORMFLD WHERE formfld_foreignkeyFLAG='Y' AND formfld_foreignkey=(SELECT fk_formlib FROM " + whereClauseStr +")) and fk_form=?";
        
        try {
            conn = getConnection();

            

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, responseId);
            pstmt.setInt(2, formId);

            ResultSet rset = pstmt.executeQuery();
            

            while (rset.next()) {

                
            	returnStr=rset.getString("mp_mapcolname");
                
        }
            rset.close();
        }
        catch (Exception ex) {
            Rlog.fatal("saveform",
                    "SaveFormDao.getChildDetails EXCEPTION " + ex);
            	ex.printStackTrace();
            return "";
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return returnStr;
    }

	public boolean isLocked()
	{
		if ( (this.getFilledFormStat()>0) || (this.getLockDownStat()>0)){
			
			if (  (this.getFilledFormStat()==this.getLockDownStat()) || isAnyOfLockDownStatuses(this.getFilledFormStat()) )
			{
				return true;
			}	
			else
			{
				return false;
			}	
		} else
		{
			return false;
		}	
	}
	public boolean isSoftLock()
	{ 
		System.out.println(".........................SaveFormDao.isSoftLock()..called");
		if((this.getFilledFormStat()>0) || (this.getLockDownReviewStat() >0))
		{ 
			if( (this.getFilledFormStat()==this.getLockDownReviewStat())|| isAnyOfLockDownReviewStatuses(this.filledFormStat)){
				System.out.println("......\nSaveFormDao.isSoftLock().this.filledFormStat::"+this.filledFormStat
						+"..\n.......returning True.");
				return true;
			}
				
		
		}
		System.out.println("......\nSaveFormDao.isSoftLock().this.filledFormStat::"+this.filledFormStat
				+"..\n.......returning False");
	return false;	
	}
	
	public boolean isAllowedUnlock()
	{
		String role=(String)this.getHtMoreParams().get("teamRolePK");
		System.out.println("*********************\nSaveFormDao.isAllowedUnlock.role ::"+role+"\n---------------");
		return new CodeDao().matchCodeListType(role);
	}

	public Hashtable getHtOtherAttributes() {
		return htOtherAttributes;
	}

	public void setHtOtherAttributes(Hashtable htOtherAttributes) {
		this.htOtherAttributes = htOtherAttributes;
	}

	public void setHtOtherAttributes(String key,String value) {
		this.htOtherAttributes.put(key,value);
	}

	public ArrayList getLockdownStatuses() {
		return lockdownStatuses;
	}

	public void setLockdownStatuses(ArrayList lockdownStatuses) {
		this.lockdownStatuses = lockdownStatuses;
	}
	
	public void setLockdownStatuses(Integer lockdownStatus) {
		this.lockdownStatuses.add(lockdownStatus);
	}
	public ArrayList getLockdownReviewStatuses() {
		return lockdownReviewStatuses;// new field introduced for jupiter enhancement ( form status 22620)
	}
	public void setLockdownReviewStatuses(ArrayList lockdownReviewStatuses) {
		this.lockdownReviewStatuses = lockdownReviewStatuses;// new field introduced for jupiter enhancement ( form status 22620)
	}
	public void setLockdownReviewStatuses(Integer lockdownReviewStatus) {
		this.lockdownReviewStatuses.add(lockdownReviewStatus);// new field introduced for jupiter enhancement ( form status 22620)
	}
	public boolean isAnyOfLockDownStatuses(int stat)
	{
		int indx = -1;
			if (lockdownStatuses != null && lockdownStatuses.size() > 0)
			{
				indx = lockdownStatuses.indexOf(new Integer(stat));
			}
			
		if (indx >= 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public boolean isAnyOfLockDownReviewStatuses(int stat)// new field introduced for jupiter enhancement ( form status 22620)
	{
		int indx = -1;
			if (lockdownReviewStatuses != null && lockdownReviewStatuses.size() > 0)
			{
				indx = lockdownReviewStatuses.indexOf(new Integer(stat));
			}
			
		if (indx >= 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public Hashtable getHtMoreParams() {
		return htMoreParams;
	}

	public void setHtMoreParams(Hashtable htMoreParams) {
		this.htMoreParams = htMoreParams;
	}
	
	
	/**
	 * Populates the object with information on browser columns names 
	 * @param formID PK of the form record
	 * 
	 * */
	
	public void getFormResponseBrowserColumns(String formID) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        StringBuffer sbSQL = new StringBuffer();
     try {
        	
        	sbSQL.append("SELECT mp_mapcolname,mp_systemid,mp_flddatatype,mp_dispname " +
        			"FROM ER_MAPFORM WHERE fk_form = ? and nvl(mp_browser,0) = 1 order by decode(mp_systemid,'er_def_date_01',0,mp_sequence) asc  ");
        	
            conn = getConnection();
            pstmt = conn.prepareStatement(sbSQL.toString());
            pstmt.setString(1, formID);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                
            	this.setColNames(rs.getString("mp_mapcolname"));
            	this.setColSysIds(rs.getString("mp_systemid"));
            	this.setColTypes(rs.getString("mp_flddatatype"));
                this.setColDisplayNames(rs.getString("mp_dispname"));
            }
        } catch (SQLException ex) {
            Rlog.fatal("saveforms", "EXCEPTION IN  SaveFormDao.getFormResponseBrowserColumns"
                    + ex);
            ex.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

	public ArrayList getColDisplayNames() {
		return colDisplayNames;
	}

	public void setColDisplayNames(ArrayList colDisplayNames) {
		this.colDisplayNames = colDisplayNames;
	}

	public void setColDisplayNames(String colDisplayName) {
		this.colDisplayNames.add(colDisplayName);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SaveFormDao [colDisplayNames=" + colDisplayNames
				+ ", colNames=" + colNames + ", colOrigSysIdWValueSet="
				+ colOrigSysIdWValueSet + ", colOrigSysIds=" + colOrigSysIds
				+ ", colResponseDispValSet=" + colResponseDispValSet
				+ ", colSysIds=" + colSysIds + ", colTypes=" + colTypes
				+ ", filledFormStat=" + filledFormStat + ", formId=" + formId
				+ ", houseKeeper=" + houseKeeper + ", htMoreParams="
				+ htMoreParams + ", htOtherAttributes=" + htOtherAttributes
				+ ", htmlStr=" + htmlStr + ", lockDownReviewStat="
				+ lockDownReviewStat + ", lockDownStat=" + lockDownStat
				+ ", lockdownReviewStatuses=" + lockdownReviewStatuses
				+ ", lockdownStatuses=" + lockdownStatuses + "]";
	}

	public boolean isMonitor(){
		String role=(String)this.getHtMoreParams().get("teamRolePK");
		return new CodeDao().matchCodeListTypeAsMonitor(role);
	}
	//Added by Vivek Jha for Monitor Team Role in jupiter
	public boolean isMonitor(String role){
		//String role=(String)this.getHtMoreParams().get("teamRolePK");
		System.out.println("*********************\nSaveFormDao.isMonitor.role ::"+role+"\n---------------");
		return new CodeDao().matchCodeListTypeAsMonitor(role);
	}

}
