/*
 * Classname : FldValidateBean
 * 
 * Version information: 1.0
 *
 * Date: 11/05/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: SoniaKaura
 */

package com.velos.eres.business.fldValidate.impl;

/* Import Statements */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The FldValidate CMP entity bean.<br>
 * <br>
 * 
 * @author Sonia Kaura
 * @vesion 1.0 11/07/2003
 * @ejbHome FldValidateHome
 * @ejbRemote FldValidateRObj
 */

@Entity
@Table(name = "ER_FLDVALIDATE")
@NamedQuery(name = "findByFieldId", query = "SELECT OBJECT(val) FROM FldValidateBean val  where fk_fldlib = :fk_fldlib")
public class FldValidateBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3762818177398355249L;

    /**
     * The primary key:pk_fldValidate
     */

    public int fldValidateId;

    /**
     * The foreign key reference from the Field : FK_FLDLIB
     */
    public Integer fldLibId;

    /**
     * The First Operator in the Validation : FLD_VALIDATE_OPT1
     */
    public String fldValidateOp1;

    /**
     * The First Value in the Validation : FLD_VALIDATE_Val1
     */
    public String fldValidateVal1;

    /**
     * The First Boolean Operator in the Validation : FLDVALIDATE_LOGOP1
     */

    public String fldValidateLogOp1;

    /**
     * The Second Operator in the Validation : FLD_VALIDATE_OPT2
     * 
     */
    public String fldValidateOp2;

    /**
     * The Second Value in the Validation : FLD_VALIDATE_VAL2
     */
    public String fldValidateVal2;

    /**
     * The Boolean Connected Operator in Validation : FLDVALIDATE_LOGOP2
     * 
     */
    public String fldValidateLogOp2;

    /**
     * 
     * The Java Script of the Validation : FLD_VALIDATE_JAVASCR
     * 
     */

    public String fldValidateJavaScr;

    /**
     * The Record Type : Record_Type
     */

    public String recordType;

    public Integer creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    public Integer modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    public String ipAdd;

    // //////////////////////////////////////////////////////////////////////////////////////
    public FldValidateBean() {
    }

    public FldValidateBean(int fldValidateId, String fldLibId,
            String fldValidateOp1, String fldValidateVal1,
            String fldValidateLogOp1, String fldValidateOp2,
            String fldValidateVal2, String fldValidateLogOp2,
            String fldValidateJavaScr, String recordType, String creator,
            String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setFldValidateId(fldValidateId);
        setFldLibId(fldLibId);
        setFldValidateOp1(fldValidateOp1);
        setFldValidateVal1(fldValidateVal1);
        setFldValidateLogOp1(fldValidateLogOp1);
        setFldValidateOp2(fldValidateOp2);
        setFldValidateVal2(fldValidateVal2);
        setFldValidateLogOp2(fldValidateLogOp2);
        setFldValidateJavaScr(fldValidateJavaScr);
        setRecordType(recordType);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

    /**
     * 
     * 
     * @return fldValidateId
     */

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_FLDVALIDATE", allocationSize=1)
    @Column(name = "PK_FLDVALIDATE")
    public int getFldValidateId() {
        return fldValidateId;
    }

    /**
     * 
     * 
     * @param fldValidateId
     */
    public void setFldValidateId(int fldValidateId) {
        this.fldValidateId = fldValidateId;
    }

    /**
     * 
     * 
     * @return fldLibId
     */
    @Column(name = "FK_FLDLIB")
    public String getFldLibId() {

        return StringUtil.integerToString(fldLibId);

    }

    /**
     * 
     * 
     * @param fldLibId
     */
    public void setFldLibId(String fldLibId) {

        // Rlog.debug("fldvalidate","stringtointeger fldLibId" + fldLibId);
        this.fldLibId = StringUtil.stringToInteger(fldLibId);

    }

    /**
     * 
     * 
     * @return fldValidateOp1
     */

    @Column(name = "FLDVALIDATE_OP1")
    public String getFldValidateOp1() {
        return fldValidateOp1;

    }

    /**
     * 
     * 
     * @param fldValidateOp1
     */
    public void setFldValidateOp1(String fldValidateOp1) {

        this.fldValidateOp1 = fldValidateOp1;
    }

    /**
     * 
     * 
     * @return fldValidateVal1
     */
    @Column(name = "FLDVALIDATE_VAL1")
    public String getFldValidateVal1() {

        return fldValidateVal1;
    }

    /**
     * 
     * 
     * @param fldValidateVal1
     */
    public void setFldValidateVal1(String fldValidateVal1) {

        this.fldValidateVal1 = fldValidateVal1;
    }

    /**
     * 
     * 
     * @return fldValidateLogOp1
     */
    @Column(name = "FLDVALIDATE_LOGOP1")
    public String getFldValidateLogOp1() {

        return fldValidateLogOp1;
    }

    /**
     * 
     * 
     * @param fldValidateLogOp1
     */
    public void setFldValidateLogOp1(String fldValidateLogOp1) {
        this.fldValidateLogOp1 = fldValidateLogOp1;
    }

    /**
     * 
     * 
     * @return fldValidateOp2
     */
    @Column(name = "FLDVALIDATE_OP2")
    public String getFldValidateOp2() {
        return fldValidateOp2;
    }

    /**
     * 
     * 
     * @param fldValidateOp2
     */

    public void setFldValidateOp2(String fldValidateOp2) {

        this.fldValidateOp2 = fldValidateOp2;
    }

    /**
     * 
     * 
     * @return fldValidateVal2
     */
    @Column(name = "FLDVALIDATE_VAL2")
    public String getFldValidateVal2() {

        return fldValidateVal2;
    }

    /**
     * 
     * 
     * @param fldValidateVal2
     */
    public void setFldValidateVal2(String fldValidateVal2) {

        this.fldValidateVal2 = fldValidateVal2;
    }

    /**
     * 
     * 
     * @return fldValidateLogOp2
     */
    @Column(name = "FLDVALIDATE_LOGOP2")
    public String getFldValidateLogOp2() {

        return fldValidateLogOp2;
    }

    /**
     * 
     * 
     * @param fldValidateLogOp2
     */
    public void setFldValidateLogOp2(String fldValidateLogOp2) {
        this.fldValidateLogOp2 = fldValidateLogOp2;
    }

    /**
     * 
     * 
     * @return fldValidateJavaScr
     */
    @Column(name = "FLDVALIDATE_JAVASCR")
    public String getFldValidateJavaScr() {

        return fldValidateJavaScr;
    }

    /**
     * 
     * 
     * @param fldValidateJavaScr
     */
    public void setFldValidateJavaScr(String fldValidateJavaScr) {

        this.fldValidateJavaScr = fldValidateJavaScr;
    }

    /**
     * @return the Record Type : m: modified n:new d: delet
     */
    @Column(name = "RECORD_TYPE")
    public String getRecordType() {
        return this.recordType;
    }

    /**
     * @param set
     *            the Record Type to be modified,new or delete
     */
    public void setRecordType(String recordType) {
        this.recordType = recordType;

    }

    /**
     * @return the Creator
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);

    }

    /**
     * @param takes
     *            a string
     */

    public void setCreator(String creator) {

        // Rlog.debug("fldvalidate","stringtointeger creator" + creator);
        this.creator = StringUtil.stringToInteger(creator);

    }

    /**
     * @return the unique ID of the User
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {

        return StringUtil.integerToString(this.modifiedBy);

    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {

        // Rlog.debug("fldvalidate","stringtointeger modifiedBy" + modifiedBy);
        this.modifiedBy = StringUtil.stringToInteger(modifiedBy);

    }

    /**
     * @return the IP Address of the Form Notification request
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Notification request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF THE GETTER AND SETTER METHODS

    /*
     * public FldValidateStateKeeper getFldValidateStateKeeper() {
     * Rlog.debug("fldvalidate", "FldValidateBean.getFldValidateStateKeeper");
     * return new FldValidateStateKeeper(getFldValidateId(), getFldLibId(),
     * getFldValidateOp1(), getFldValidateVal1(), getFldValidateLogOp1(),
     * getFldValidateOp2(), getFldValidateVal2(), getFldValidateLogOp2(),
     * getFldValidateJavaScr(), getRecordType(), getCreator(), getModifiedBy(),
     * getIpAdd()); }
     */

    /**
     * sets up a state keeper containing details of the form field
     */

    /*
     * public void setFldValidateStateKeeper(FldValidateStateKeeper
     * fldValidatesk) { GenerateId formSId = null; try { Connection conn = null;
     * conn = getConnection(); //
     * Rlog.debug("fldvalidate","FldValidateBean.setFldValidateStateKeeper() //
     * Connection :"+ conn); this.fldValidateId =
     * formSId.getId("SEQ_ER_FLDVALIDATE", conn); //
     * Rlog.debug("fldvalidate","FldValidateBean.setFldValidateStateKeeper() //
     * fldValidateId :"+ this.fldValidateId); conn.close(); //
     * Rlog.debug("fldvalidate","get getFldLibId //
     * :"+fldValidatesk.getFldLibId());
     * setFldLibId(fldValidatesk.getFldLibId()); //
     * Rlog.debug("fldvalidate","get getFldValidateOp1 //
     * :"+fldValidatesk.getFldValidateOp1());
     * setFldValidateOp1(fldValidatesk.getFldValidateOp1()); //
     * Rlog.debug("fldvalidate","get getFldValidateVal1 //
     * :"+fldValidatesk.getFldValidateVal1());
     * setFldValidateVal1(fldValidatesk.getFldValidateVal1()); //
     * Rlog.debug("fldvalidate","get getFldValidateLogOp1 //
     * :"+fldValidatesk.getFldValidateLogOp1());
     * setFldValidateLogOp1(fldValidatesk.getFldValidateLogOp1()); //
     * Rlog.debug("fldvalidate","get getFldValidateOp2 //
     * :"+fldValidatesk.getFldValidateOp2());
     * setFldValidateOp2(fldValidatesk.getFldValidateOp2()); //
     * Rlog.debug("fldvalidate","get getFldValidateVal2 //
     * :"+fldValidatesk.getFldValidateVal2());
     * setFldValidateVal2(fldValidatesk.getFldValidateVal2()); //
     * Rlog.debug("fldvalidate","get getFldValidateLogOp2 //
     * :"+fldValidatesk.getFldValidateLogOp2());
     * setFldValidateLogOp2(fldValidatesk.getFldValidateLogOp2()); //
     * Rlog.debug("fldvalidate","get getFldValidateJavaScr //
     * :"+fldValidatesk.getFldValidateJavaScr());
     * setFldValidateJavaScr(fldValidatesk.getFldValidateJavaScr());
     * 
     * setRecordType(fldValidatesk.getRecordType());
     * 
     * setCreator(fldValidatesk.getCreator());
     * 
     * setIpAdd(fldValidatesk.getIpAdd()); } catch (Exception e) {
     * Rlog.fatal("fldvalidate", "Error in setFldValidateStateKeeper() in
     * FldValidateBean " + e); } }
     */

    /**
     * updates the Form Sec Record
     * 
     * @param fldValidatesk
     * @return int 0: in case of successful updation; -2 in caseof exception
     */
    public int updateFldValidate(FldValidateBean fldValidatesk) {
        try {

            setFldValidateId(fldValidatesk.getFldValidateId());

            setFldLibId(fldValidatesk.getFldLibId());
            setFldValidateOp1(fldValidatesk.getFldValidateOp1());
            setFldValidateVal1(fldValidatesk.getFldValidateVal1());
            setFldValidateLogOp1(fldValidatesk.getFldValidateLogOp1());
            setFldValidateOp2(fldValidatesk.getFldValidateOp2());
            setFldValidateVal2(fldValidatesk.getFldValidateVal2());
            setFldValidateLogOp2(fldValidatesk.getFldValidateLogOp2());
            setFldValidateJavaScr(fldValidatesk.getFldValidateJavaScr());
            setRecordType(fldValidatesk.getRecordType());
            setCreator(fldValidatesk.getCreator());
            setModifiedBy(fldValidatesk.getModifiedBy());
            setIpAdd(fldValidatesk.getIpAdd());

            return 0;
        } catch (Exception e) {
            Rlog
                    .fatal("fldvalidate",
                            " error in FormFieldBean.updateFormField");
            return -2;
        }
    }

    public void generateJavaScript(String sysId, String rangeOverRide,
            String fldName) {

        StringBuffer strBufJavaScr = new StringBuffer();
        String strJavaScr = "";
        String validationAlert = "";
        String validationHdn = "";

        fldName = StringUtil.htmlEncode(fldName);

        float fVal1 = 0;
        float fVal2 = 0;
        String op1 = getFldValidateOp1();

        String val1 = getFldValidateVal1();

        String logOp1 = getFldValidateLogOp1();

        String op2 = getFldValidateOp2();

        String val2 = getFldValidateVal2();

        String operator1 = "";
        String operator2 = "";
        String overRideStr = "";
        if (rangeOverRide == null)
            rangeOverRide = "0";

        if (!(StringUtil.isEmpty(val1)) || !(StringUtil.isEmpty(val2))) {

            op1 = op1.trim();
            val1 = val1.trim();
            logOp1 = logOp1.trim();
            op2 = op2.trim();
            val2 = val2.trim();

            if (op1.equals(">")) {
                op1 = "&gt;";
                operator1 = "Greater than";
            }
            if (op1.equals("<")) {
                op1 = "&lt;";
                operator1 = "Less than";
            }
            if (op1.equals(">=")) {
                op1 = "&gt;=";
                operator1 = "Greater than equal to";
            }
            if (op1.equals("<=")) {
                op1 = "&lt;=";
                operator1 = "Less than equal to";
            }
            if (op2.equals(">")) {
                op2 = "&gt;";
                operator2 = "Greater than";
            }
            if (op2.equals("<")) {
                op2 = "&lt;";
                operator2 = "Less than";
            }
            if (op2.equals(">=")) {
                op2 = "&gt;=";
                operator2 = "Greater than equal to";
            }
            if (op2.equals("<=")) {
                op2 = "&lt;=";
                operator2 = "Less than equal to";
            }

            // String validation = operator1+" " +fVal1+ " "+logOp1+ "
            // "+operator2+ " "+fVal2;
            String validation = "";

            if (!operator1.equals(null) && !operator1.equals("")
                    && !operator1.equals("null")) {
                validation = operator1 + " ";
            }
            ;
            if (!val1.equals(null) && !val1.equals("") && !val1.equals("null")) {
                validation = validation + StringUtil.stringToFloat(val1) + " ";
            }
            if (!logOp1.equals(null) && !logOp1.equals("")
                    && !logOp1.equals("null")) {
                validation = validation + logOp1 + " ";
            }
            if (!operator2.equals(null) && !operator2.equals("")
                    && !operator2.equals("null")) {
                validation = validation + operator2 + " ";
            }

            if (!val2.equals(null) && !val2.equals("") && !val2.equals("null")) {
                validation = validation + StringUtil.stringToFloat(val2) + " ";
            }

            validationAlert = validation + "for " + fldName;

            validationHdn = validation + "[VELSEPLABEL]" + fldName;

            // String validation = operator1+" " +fVal1+ " "+logOp1+ "
            // "+operator2+ " "+fVal2+"[VELSEPLABEL]"+fldName;

            if (!val1.equals(""))
                fVal1 = StringUtil.stringToFloat(val1);
            if (!val2.equals(""))
                fVal2 = StringUtil.stringToFloat(val2);

            // modified by sonia abrol 03/10/05 added check for disabled
            strBufJavaScr.append(" if (! formobj." + sysId
                    + ".disabled) { if(validateDataRange(formobj." + sysId
                    + ".value,\"" + op1 + "\"," + fVal1 + ",\"" + logOp1
                    + "\",\"" + op2 + "\"," + fVal2 + ") == false) { ");
            // strBufJavaScr.append(" alert(\"Please enter a Number within the
            // Range\"); ");
            if (rangeOverRide.equals("1")) {
                overRideStr = "formobj.hdn_rng_" + sysId
                        + ".value = \"Please enter a Number which is "
                        + validationHdn + "\";}\n else " + "formobj.hdn_rng_"
                        + sysId + ".value ='';";
                strBufJavaScr.append(overRideStr);
                strBufJavaScr.append(" } else {  formobj.hdn_rng_" + sysId
                        + ".value =''; }");
            } else {

                strBufJavaScr.append(" alert(\"Please enter a Number which is "
                        + validationAlert + "\" ); ");

                strBufJavaScr.append(" formobj." + sysId + ".focus(); ");
                strBufJavaScr.append(" return false; } ");
                strBufJavaScr.append(" } ");
            }

            strJavaScr = strBufJavaScr.toString();
            // Rlog.debug("fldvalidate","strJavaScr"+strJavaScr);

        }

        setFldValidateJavaScr(strJavaScr);

    }

}
