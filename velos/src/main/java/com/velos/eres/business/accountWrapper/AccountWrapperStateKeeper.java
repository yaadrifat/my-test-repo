/*
 * Classname : 				AccountWrapperStateKeeper.java
 * 
 * Version information : 	1.0
 *
 * Date 					03/14/2001
 * 
 * Copyright notice: 		Velos Inc
 *
 * Author					Sajal
 */

package com.velos.eres.business.accountWrapper;

import com.velos.eres.service.util.Rlog;

/**
 * AccountWrapperStateKeeper class resulting from implementation of the
 * StateKeeper pattern This class is used as a wrapper class for account
 * creation module.
 * 
 * @author Sajal
 */

public class AccountWrapperStateKeeper implements java.io.Serializable {
    // This class is used as a wrapper class for account creation module.

   /* Modified by Amarnadh ,Added an attribute siteNotes for June Enhancement '07  #U7 */
	
    /**
     * the account type
     */
    private String accType;

    /**
     * the account role type
     */
    private String accRoleType;

    /**
     * the account name
     */
    private String accName;

    /**
     * the account mail flag
     */
    private String accMailFlag;

    /**
     * the account public flag
     */
    private String accPubFlag;

    /**
     * the account logo
     */
    private byte[] accLogo;

    /**
     * the account module right
     */
    private String accModRight;

    /**
     * the group name
     */
    private String groupName;

    /**
     * the group description
     */
    private String groupDesc;

    /**
     * the address type for User
     */
    private String addTypeUser;

    /**
     * the address primary for User
     */
    private String addPriUser;

    /**
     * the address city for User
     */
    private String addCityUser;

    /**
     * the address state for User
     */
    private String addStateUser;

    /**
     * the address zip code for User
     */
    private String addZipUser;

    /**
     * the address country for User
     */
    private String addCountryUser;

    /**
     * the address county for User
     */
    private String addCountyUser;

    /**
     * the address effective date for User
     */
    private String addEffDateUser;

    /**
     * the address phone for User
     */
    private String addPhoneUser;

    /**
     * the address email for User
     */
    private String addEmailUser;

    /**
     * the address pager for User
     */
    private String addPagerUser;

    /**
     * the address pager for User
     */
    private String addMobileUser;

    /**
     * the address fax for User
     */
    private String addFaxUser;

    /**
     * the address type for Site
     */
    private String addTypeSite;

    /**
     * the address primary for Site
     */
    private String addPriSite;

    /**
     * the address city for Site
     */
    private String addCitySite;

    /**
     * the address state for Site
     */
    private String addStateSite;

    /**
     * the address zip code for Site
     */
    private String addZipSite;

    /**
     * the address country for Site
     */
    private String addCountrySite;

    /**
     * the address county for Site
     */
    private String addCountySite;

    /**
     * the address effective date for Site
     */
    private String addEffDateSite;

    /**
     * the address phone for Site
     */
    private String addPhoneSite;

    /**
     * the address email for Site
     */
    private String addEmailSite;

    /**
     * the address pager for Site
     */
    private String addPagerSite;

    /**
     * the address pager for Site
     */
    private String addMobileSite;

    /**
     * the address fax for Site
     */
    private String addFaxSite;

    /**
     * the site Type
     */
    private String siteCodelstType;

    /**
     * the site name
     */
    private String siteName;
   
    private String ctepid;
    /* Added an attribute siteNotes for June Enhancement '07  #U7 */
    
    /**
     * the site notes
     */
    
    private String siteNotes;

    /**
     * the site Info
     */
    private String siteInfo;

    /**
     * the site parent
     */
    private String siteParent;

    /**
     * the user codelst jobtype
     */
    private String userCodelstJobtype;

    /**
     * the user lastName
     */
    private String userLastName;

    /**
     * the user firstName
     */

    private String userFirstName;

    /**
     * the user mid Name
     */

    private String userMidName;

    /**
     * the user work experience
     */

    private String userWrkExp;

    /**
     * the user phase ivaluation
     */

    private String userPhaseInv;

    /**
     * the user session Time out
     */

    private String userSessionTime;

    /**
     * the user login Name
     */

    private String userLoginName;

    /**
     * the user password
     */

    private String userPwd;

    /**
     * the user Secret Question
     */

    private String userSecQues;

    /**
     * the user Answer
     */

    private String userAnswer;

    /**
     * the user status
     */

    private String userStatus;

    /**
     * the user default group
     */
    private String userGrpDefault;

    /**
     * the user Account
     */
    private String userAccountId;

    /**
     * the user Site ID
     */
    private String userSiteId;

    /**
     * the user Address ID
     */
    private String userPerAddressId;

    /**
     * the user codelist primary Speciality
     */

    private String userCodelstSpl;

    private String userTimeZoneId;

    /**
     * the msgcntr to userId
     */
    private String msgcntrToUserId;

    /**
     * the user Id
     */

    private String userId;

    /**
     * the account Status
     */

    private String accStatus;

    /**
     * the account start date
     */

    private String accStartDate;

    /**
     * the msgcntr status
     */

    private String msgcntrStatus;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    /*
     * Pwd Expiry Date
     */
    private String userPwdExpiryDate;

    /*
     * Pwd Expiry Days
     */
    private String userPwdExpiryDays;

    /*
     * Pwd Reminder to be sent on
     */
    private String userPwdReminderDate;

    /*
     * E-signature
     */
    private String userESign;

    /*
     * E-signature Expiry Date
     */
    private String userESignExpiryDate;

    /*
     * Pwd Expiry Days
     */
    private String userESignExpiryDays;

    /*
     * Pwd Reminder to be sent on
     */
    private String userESignReminderDate;

    /*
     * user Site Flag
     */
    private String userSiteFlag;

    /*
     * user Type
     */
    private String userType;
    
    private String userLoginModuleId;
    
    private String userLoginModuleMap;
    
    //Added by Manimaran for Enh.#U11
    private String siteHidden;
    
    private String poIdentifier;
    
    private String userHidden;

    // GETTER SETTER METHODS

    /**
	 * @return the userLoginModuleMap
	 */
	public String getUserLoginModuleMap() {
		return userLoginModuleMap;
	}

	/**
	 * @param userLoginModuleMap the userLoginModuleMap to set
	 */
	public void setUserLoginModuleMap(String userLoginModuleMap) {
		this.userLoginModuleMap = userLoginModuleMap;
	}

	/**
	 * @return the usrLoginModuleId
	 */
	public String getUserLoginModuleId() {
		return userLoginModuleId;
	}

	/**
	 * @param usrLoginModuleId the usrLoginModuleId to set
	 */
	public void setUserLoginModuleId(String userLoginModuleId) {
		this.userLoginModuleId = userLoginModuleId;
	}

	public String getAccType() {
        return this.accType;
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }

    public String getAccRoleType() {
        return this.accRoleType;
    }

    public void setAccRoleType(String accRoleType) {
        this.accRoleType = accRoleType;
    }

    public String getAccName() {
        return this.accName;
    }

    public void setAccName(String accName) {
        this.accName = accName;
    }

    public String getAccMailFlag() {
        return this.accMailFlag;
    }

    public void setAccMailFlag(String accMailFlag) {
        this.accMailFlag = accMailFlag;
    }

    public String getAccPubFlag() {
        return this.accPubFlag;
    }

    public void setAccPubFlag(String accPubFlag) {
        this.accPubFlag = accPubFlag;
    }

    public byte[] getAccLogo() {
        return this.accLogo;
    }

    public void setAccLogo(byte[] accLogo) {
        this.accLogo = accLogo;
    }

    public String getAccModRight() {
        return this.accModRight;
    }

    public void setAccModRight(String accModRight) {
        this.accModRight = accModRight;
    }

    public String getGroupName() {
        return this.groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupDesc() {
        return this.groupDesc;
    }

    public void setGroupDesc(String groupDesc) {
        this.groupDesc = groupDesc;
    }

    public String getAddTypeUser() {
        return this.addTypeUser;
    }

    public void setAddTypeUser(String addTypeUser) {
        this.addTypeUser = addTypeUser;
    }

    public String getAddPriUser() {
        return this.addPriUser;
    }

    public void setAddPriUser(String addPriUser) {
        this.addPriUser = addPriUser;
    }

    public String getAddCityUser() {
        return this.addCityUser;
    }

    public void setAddCityUser(String addCityUser) {
        this.addCityUser = addCityUser;
    }

    public String getAddStateUser() {
        return this.addStateUser;
    }

    public void setAddStateUser(String addStateUser) {
        this.addStateUser = addStateUser;
    }

    public String getAddZipUser() {
        return this.addZipUser;
    }

    public void setAddZipUser(String addZipUser) {
        this.addZipUser = addZipUser;
    }

    public String getAddCountryUser() {
        return this.addCountryUser;
    }

    public void setAddCountryUser(String addCountryUser) {
        this.addCountryUser = addCountryUser;
    }

    public String getAddCountyUser() {
        return this.addCountyUser;
    }

    public void setAddCountyUser(String addCountyUser) {
        this.addCountyUser = addCountyUser;
    }

    public String getAddEffDateUser() {
        return this.addEffDateUser;
    }

    public void setAddEffDateUser(String addEffDateUser) {
        this.addEffDateUser = addEffDateUser;
    }

    public String getAddPhoneUser() {
        return this.addPhoneUser;
    }

    public void setAddPhoneUser(String addPhoneUser) {
        this.addPhoneUser = addPhoneUser;
    }

    public String getAddEmailUser() {
        return this.addEmailUser;
    }

    public void setAddEmailUser(String addEmailUser) {
        this.addEmailUser = addEmailUser;
    }

    public String getAddPagerUser() {
        return this.addPagerUser;
    }

    public void setAddPagerUser(String addPagerUser) {
        this.addPagerUser = addPagerUser;
    }

    public String getAddMobileUser() {
        return this.addMobileUser;
    }

    public void setAddMobileUser(String addMobileUser) {
        this.addMobileUser = addMobileUser;
    }

    public String getAddFaxUser() {
        return this.addFaxUser;
    }

    public void setAddFaxUser(String addFaxUser) {
        this.addFaxUser = addFaxUser;
    }

    public String getAddTypeSite() {
        return this.addTypeSite;
    }

    public void setAddTypeSite(String addTypeSite) {
        this.addTypeSite = addTypeSite;
    }

    public String getAddPriSite() {
        return this.addPriSite;
    }

    public void setAddPriSite(String addPriSite) {
        this.addPriSite = addPriSite;
    }

    public String getAddCitySite() {
        return this.addCitySite;
    }

    public void setAddCitySite(String addCitySite) {
        this.addCitySite = addCitySite;
    }

    public String getAddStateSite() {
        return this.addStateSite;
    }

    public void setAddStateSite(String addStateSite) {
        this.addStateSite = addStateSite;
    }

    public String getAddZipSite() {
        return this.addZipSite;
    }

    public void setAddZipSite(String addZipSite) {
        this.addZipSite = addZipSite;
    }

    public String getAddCountrySite() {
        return this.addCountrySite;
    }

    public void setAddCountrySite(String addCountrySite) {
        this.addCountrySite = addCountrySite;
    }

    public String getAddCountySite() {
        return this.addCountySite;
    }

    public void setAddCountySite(String addCountySite) {
        this.addCountySite = addCountySite;
    }

    public String getAddEffDateSite() {
        return this.addEffDateSite;
    }

    public void setAddEffDateSite(String addEffDateSite) {
        this.addEffDateSite = addEffDateSite;
    }

    public String getAddPhoneSite() {
        return this.addPhoneSite;
    }

    public void setAddPhoneSite(String addPhoneSite) {
        this.addPhoneSite = addPhoneSite;
    }

    public String getAddEmailSite() {
        return this.addEmailSite;
    }

    public void setAddEmailSite(String addEmailSite) {
        this.addEmailSite = addEmailSite;
    }

    public String getAddPagerSite() {
        return this.addPagerSite;
    }

    public void setAddPagerSite(String addPagerSite) {
        this.addPagerSite = addPagerSite;
    }

    public String getAddMobileSite() {
        return this.addMobileSite;
    }

    public void setAddMobileSite(String addMobileSite) {
        this.addMobileSite = addMobileSite;
    }

    public String getAddFaxSite() {
        return this.addFaxSite;
    }

    public void setAddFaxSite(String addFaxSite) {
        this.addFaxSite = addFaxSite;
    }

    public String getSiteCodelstType() {
        return this.siteCodelstType;
    }

    public void setSiteCodelstType(String siteCodelstType) {
        this.siteCodelstType = siteCodelstType;
    }

    public String getSiteName() {
        return this.siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }
    
    public String getCtepId() {
    	return this.ctepid;
    }

    public void setCtepId(String ctepid) {
    	this.ctepid = ctepid;
    }

    /* Added an attribute siteNotes for June Enhancement '07  #U7 */
    
    public String getSiteNotes(){
    	return this.siteNotes;
    }
    
    public void setSiteNotes(String siteNotes){
    	this.siteNotes = siteNotes;
    }

    public String getSiteInfo() {
        return this.siteInfo;
    }

    public void setSiteInfo(String siteInfo) {
        this.siteInfo = siteInfo;
    }

    public String getSiteParent() {
        return this.siteParent;
    }

    public void setSiteParent(String siteParent) {
        this.siteParent = siteParent;
    }

    public String getUserCodelstJobtype() {
        return this.userCodelstJobtype;
    }

    public void setUserCodelstJobtype(String userCodelstJobtype) {
        this.userCodelstJobtype = userCodelstJobtype;
    }

    public String getUserLastName() {
        return this.userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserFirstName() {
        return this.userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserMidName() {
        return this.userMidName;
    }

    public void setUserMidName(String userMidName) {
        this.userMidName = userMidName;
    }

    public String getUserWrkExp() {
        return this.userWrkExp;
    }

    public void setUserWrkExp(String userWrkExp) {
        this.userWrkExp = userWrkExp;
    }

    public String getUserPhaseInv() {
        return this.userPhaseInv;
    }

    public void setUserPhaseInv(String userPhaseInv) {
        this.userPhaseInv = userPhaseInv;
    }

    public String getUserSessionTime() {
        return this.userSessionTime;
    }

    public void setUserSessionTime(String userSessionTime) {
        this.userSessionTime = userSessionTime;
    }

    public String getUserLoginName() {
        return this.userLoginName;
    }

    public void setUserLoginName(String userLoginName) {
        this.userLoginName = userLoginName;
    }

    public String getUserPwd() {
        return this.userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public String getUserSecQues() {
        return this.userSecQues;
    }

    public void setUserSecQues(String userSecQues) {
        this.userSecQues = userSecQues;
    }

    public String getUserAnswer() {
        return this.userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    public String getUserCodelstSpl() {
        return this.userCodelstSpl;
    }

    public void setUserCodelstSpl(String userCodelstSpl) {
        this.userCodelstSpl = userCodelstSpl;
    }

    public String getUserTimeZoneId() {
        return this.userTimeZoneId;
    }

    public void setUserTimeZoneId(String userTimeZoneId) {
        this.userTimeZoneId = userTimeZoneId;
    }

    public String getMsgcntrToUserId() {
        return this.msgcntrToUserId;
    }

    public void setMsgcntrToUserId(String msgcntrToUserId) {
        this.msgcntrToUserId = msgcntrToUserId;
    }

    public String getUserStatus() {
        return this.userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
        Rlog.debug("accountWrapper", "AccountWrapperStateKeeper setUserStatus "
                + this.userStatus);
    }

    public String getUserGrpDefault() {
        return this.userGrpDefault;
    }

    public void setUserGrpDefault(String userGrpDefault) {
        this.userGrpDefault = userGrpDefault;
    }

    public String getUserAccountId() {
        return this.userAccountId;
    }

    public void setUserAccountId(String userAccountId) {
        this.userAccountId = userAccountId;
    }

    public String getUserPerAddressId() {
        return this.userPerAddressId;
    }

    public void setUserPerAddressId(String userPerAddressId) {
        this.userPerAddressId = userPerAddressId;
    }

    public String getUserSiteId() {
        return this.userSiteId;
    }

    public void setUserSiteId(String userSiteId) {
        this.userSiteId = userSiteId;
    }

    /**
     * Returns the Site Flag of the user
     * 
     * @return the Site Flag associated to the user
     */

    public String getUserSiteFlag() {
        return this.userSiteFlag;
    }

    /**
     * Set Site Flag of the user
     * 
     * @param userSiteFlag
     *            A/S
     */

    public void setUserSiteFlag(String userSiteFlag) {
        this.userSiteFlag = userSiteFlag;
    }

    /**
     * Returns user type
     * 
     * @return user type N/S
     */

    public String getUserType() {
        return this.userType;
    }

    /**
     * Set user type
     * 
     * @param user
     *            type N/S
     */

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccStatus() {
        return this.accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    public String getAccStartDate() {
        return this.accStartDate;
    }

    public void setAccStartDate(String accStartDate) {
        this.accStartDate = accStartDate;
    }

    /**
     * Returns msgcntrStatus
     * 
     * @return the msgcntrStatus
     */
    public String getMsgcntrStatus() {
        return this.msgcntrStatus;
    }

    /**
     * Registers the msgcntrStatus
     * 
     * @param msgcntrStatus
     *            The String that is required to be registered as the
     *            msgcntrStatus
     */
    public void setMsgcntrStatus(String msgcntrStatus) {
        this.msgcntrStatus = msgcntrStatus;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * @return Pwd Expiry Date
     */
    public String getUserPwdExpiryDate() {
        return this.userPwdExpiryDate;
    }

    /**
     * @param userPwdExpiryDate
     *            The value that is required to be registered as the Pwd Expiry
     *            Date
     */
    public void setUserPwdExpiryDate(String userPwdExpiryDate) {
        this.userPwdExpiryDate = userPwdExpiryDate;
    }

    /**
     * @return Pwd Expiry Days
     */
    public String getUserPwdExpiryDays() {
        return this.userPwdExpiryDays;
    }

    /**
     * @param userPwdExpiryDays
     *            The value that is required to be registered as the Pwd Expiry
     *            Days
     */
    public void setUserPwdExpiryDays(String userPwdExpiryDays) {
        this.userPwdExpiryDays = userPwdExpiryDays;
    }

    /**
     * @return Pwd Reminder to be sent
     */
    public String getUserPwdReminderDate() {
        return this.userPwdReminderDate;
    }

    /**
     * @param userPwdReminderDate
     *            The value that is required to be registered as the Pwd
     *            Reminder to be sent on
     */
    public void setUserPwdReminderDate(String userPwdReminderDate) {
        this.userPwdReminderDate = userPwdReminderDate;
    }

    /**
     * @return E-signature
     */
    public String getUserESign() {
        return this.userESign;
    }

    /**
     * @param userESign
     *            The value that is required to be registered as the E-signature
     */
    public void setUserESign(String userESign) {
        this.userESign = userESign;
    }

    /**
     * @return E-signature Expiry Date
     */
    public String getUserESignExpiryDate() {
        return this.userESignExpiryDate;
    }

    /**
     * @param userESignExpiryDate
     *            The value that is required to be registered as the E-signature
     *            Expiry Date
     */
    public void setUserESignExpiryDate(String userESignExpiryDate) {
        this.userESignExpiryDate = userESignExpiryDate;
    }

    /**
     * @return Pwd Expiry Days
     */
    public String getUserESignExpiryDays() {
        return this.userESignExpiryDays;
    }

    /**
     * @param userESignExpiryDays
     *            The value that is required to be registered as the Pwd Expiry
     *            Days
     */
    public void setUserESignExpiryDays(String userESignExpiryDays) {
        this.userESignExpiryDays = userESignExpiryDays;
    }

    /**
     * @return Pwd Reminder to be sent on
     */
    public String getUserESignReminderDate() {
        return this.userESignReminderDate;
    }

    /**
     * @param userESignReminderDate
     *            The value that is required to be registered as the Pwd
     *            Reminder to be sent on
     */
    public void setUserESignReminderDate(String userESignReminderDate) {
        this.userESignReminderDate = userESignReminderDate;
    }
    
    //KM
    /**
     * @return siteHidden
     */
    public String getSiteHidden() {
        return this.siteHidden;
    }

    /**
     * @param siteHidden
     *            
     */
    public void setSiteHidden(String siteHidden) {
        this.siteHidden = siteHidden;
    }
    
   //PS
    /**
     * @return poIdentifier
     */
    public String getPoIdentifier() {
		return poIdentifier;
	}
    
   //PS
    /**
     * @return poIdentifier
     */
	public void setPoIdentifier(String poIdentifier) {
		this.poIdentifier = poIdentifier;
	}
    
    /**
     * @return userHidden
     */
    public String getUserHidden() {
        return this.userHidden;
    }

    /**
     * @param userHidden
     *            
     */
    public void setUserHidden(String userHidden) {
        this.userHidden = userHidden;
    }
    

    // END OF GETTER SETTER METHODS

    public AccountWrapperStateKeeper(String accType, String accRoleType,
            String accName, String accMailFlag, String accPubFlag,
            byte[] accLogo, String accModRight, String groupName,
            String groupDesc, String addTypeUser, String addPriUser,
            String addCityUser, String addStateUser, String addZipUser,
            String addCountryUser, String addCountyUser, String addEffDateUser,
            String addPhoneUser, String addEmailUser, String addPagerUser,
            String addMobileUser, String addFaxUser, String addTypeSite,
            String addPriSite, String addCitySite, String addStateSite,
            String addZipSite, String addCountrySite, String addCountySite,
            String addEffDateSite, String addPhoneSite, String addEmailSite,
            String addPagerSite, String addMobileSite, String addFaxSite,
            String siteCodelstType, String siteName,String ctepid, String siteNotes, String siteInfo,
            String siteParent, String userCodelstJobtype, String userLastName,
            String userFirstName, String userMidName, String userWrkExp,
            String userPhaseInv, String userSessionTime, String userLoginName,
            String userPwd, String userSecQues, String userAnswer,
            String userCodelstSpl, String userTimeZoneId,
            String msgcntrToUserId, String userId, String accStatus,
            String accStartDate, String msgcntrStatus, String creator,
            String modifiedBy, String ipAdd, String userPwdExpiryDate,
            String userPwdExpiryDays, String userPwdReminderDate,
            String userESign, String userESignExpiryDate,
            String userESignExpiryDays, String userESignReminderDate,
            String userType,String userLoginModuleId,String userLoginModuleMap, String siteHidden, String userHidden, String poIdentifier) {
        Rlog.debug("accountWrapper", "AccountWrapperStateKeeper constructor ");
        setAccType(accType);
        setAccRoleType(accRoleType);
        setAccName(accName);
        setAccMailFlag(accMailFlag);
        setAccPubFlag(accPubFlag);
        setAccLogo(accLogo);
        setAccModRight(accModRight);
        setGroupName(groupName);
        setGroupDesc(groupDesc);
        setAddTypeUser(addTypeUser);
        setAddPriUser(addPriUser);
        setAddCityUser(addCityUser);
        setAddStateUser(addStateUser);
        setAddZipUser(addZipUser);
        setAddCountryUser(addCountryUser);
        setAddCountyUser(addCountyUser);
        setAddEffDateUser(addEffDateUser);
        setAddPhoneUser(addPhoneUser);
        setAddEmailUser(addEmailUser);
        setAddPagerUser(addPagerUser);
        setAddMobileUser(addMobileUser);
        setAddFaxUser(addFaxUser);
        setAddTypeSite(addTypeSite);
        setAddPriSite(addPriSite);
        setAddCitySite(addCitySite);
        setAddStateSite(addStateSite);
        setAddZipSite(addZipSite);
        setAddCountrySite(addCountrySite);
        setAddCountySite(addCountySite);
        setAddEffDateSite(addEffDateSite);
        setAddPhoneSite(addPhoneSite);
        setAddEmailSite(addEmailSite);
        setAddPagerSite(addPagerSite);
        setAddMobileSite(addMobileSite);
        setAddFaxSite(addFaxSite);
        setSiteCodelstType(siteCodelstType);
        setSiteName(siteName);
        setCtepId(ctepid);
        setSiteNotes(siteNotes);
        setSiteInfo(siteInfo);
        setSiteParent(siteParent);
        setUserCodelstJobtype(userCodelstJobtype);
        setUserLastName(userLastName);
        setUserFirstName(userFirstName);
        setUserMidName(userMidName);
        setUserWrkExp(userWrkExp);
        setUserPhaseInv(userPhaseInv);
        setUserSessionTime(userSessionTime);
        setUserLoginName(userLoginName);
        setUserPwd(userPwd);
        setUserSecQues(userSecQues);
        setUserAnswer(userAnswer);
        setUserCodelstSpl(userCodelstSpl);
        setUserTimeZoneId(userTimeZoneId);
        setUserType(userType);
        setMsgcntrToUserId(msgcntrToUserId);
        setUserId(userId);
        setAccStatus(accStatus);
        setAccStartDate(accStartDate);
        setMsgcntrStatus(msgcntrStatus);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setUserPwdExpiryDate(userPwdExpiryDate);
        setUserPwdExpiryDays(userPwdExpiryDays);
        setUserPwdReminderDate(userPwdReminderDate);
        setUserESign(userESign);
        setUserESignExpiryDate(userESignExpiryDate);
        setUserESignExpiryDays(userESignExpiryDays);
        setUserESignReminderDate(userESignReminderDate);
        this.setUserLoginModuleMap(userLoginModuleMap);
        this.setUserLoginModuleId(userLoginModuleId);
        setSiteHidden(siteHidden);//KM
        setUserHidden(userHidden);
        setPoIdentifier(poIdentifier);

    }

    public AccountWrapperStateKeeper getAccountWrapperStateKeeper() {
        Rlog.debug("accountWrapper",
                "AccountWrapperStateKeeper.getAccountWrapperStateKeeper() ");
        return new AccountWrapperStateKeeper(accType, accRoleType, accName,
                accMailFlag, accPubFlag, accLogo, accModRight, groupName,
                groupDesc, addTypeUser, addPriUser, addCityUser, addStateUser,
                addZipUser, addCountryUser, addCountyUser, addEffDateUser,
                addPhoneUser, addEmailUser, addPagerUser, addMobileUser,
                addFaxUser, addTypeSite, addPriSite, addCitySite, addStateSite,
                addZipSite, addCountrySite, addCountySite, addEffDateSite,
                addPhoneSite, addEmailSite, addPagerSite, addMobileSite,
                addFaxSite, siteCodelstType, siteName, ctepid, siteNotes,siteInfo, siteParent,
                userCodelstJobtype, userLastName, userFirstName, userMidName,
                userWrkExp, userPhaseInv, userSessionTime, userLoginName,
                userPwd, userSecQues, userAnswer, userCodelstSpl,
                userTimeZoneId, msgcntrToUserId, userId, accStatus,
                accStartDate, msgcntrStatus, creator, modifiedBy, ipAdd,
                userPwdExpiryDate, userPwdExpiryDays, userPwdReminderDate,
                userESign, userESignExpiryDate, userESignExpiryDays,
                userESignReminderDate, userType,userLoginModuleId,userLoginModuleMap,siteHidden, userHidden, poIdentifier);
    }

    /**
     * Setter for all attributes.<br>
     * Receives a AccountWrapperStateKeeper and assigns attributes to local
     * variables
     * 
     * @param awsh
     *            The AccountWrapperStateKeeper to set to.
     */
    public void setAccountWrapperStateKeeper(AccountWrapperStateKeeper awsk) {

        setAccType(awsk.getAccType());
        setAccRoleType(awsk.getAccRoleType());
        setAccName(awsk.getAccName());
        setAccMailFlag(awsk.getAccMailFlag());
        setAccPubFlag(awsk.getAccPubFlag());
        setAccLogo(awsk.getAccLogo());
        setAccModRight(awsk.getAccModRight());
        setGroupName(awsk.getGroupName());
        setGroupDesc(awsk.getGroupDesc());
        setAddTypeUser(awsk.getAddTypeUser());
        setAddPriUser(awsk.getAddPriUser());
        setAddCityUser(awsk.getAddCityUser());
        setAddStateUser(awsk.getAddStateUser());
        setAddZipUser(awsk.getAddZipUser());
        setAddCountryUser(awsk.getAddCountryUser());
        setAddCountyUser(awsk.getAddCountyUser());
        setAddEffDateUser(awsk.getAddEffDateUser());
        setAddPhoneUser(awsk.getAddPhoneUser());
        setAddEmailUser(awsk.getAddEmailUser());
        setAddPagerUser(awsk.getAddPagerUser());
        setAddMobileUser(awsk.getAddMobileUser());
        setAddFaxUser(awsk.getAddFaxUser());
        setAddTypeSite(awsk.getAddTypeSite());
        setAddPriSite(awsk.getAddPriSite());
        setAddCitySite(awsk.getAddCitySite());
        setAddStateSite(awsk.getAddStateSite());
        setAddZipSite(awsk.getAddZipSite());
        setAddCountrySite(awsk.getAddCountrySite());
        setAddCountySite(awsk.getAddCountySite());
        setAddEffDateSite(awsk.getAddEffDateSite());
        setAddPhoneSite(awsk.getAddPhoneSite());
        setAddEmailSite(awsk.getAddEmailSite());
        setAddPagerSite(awsk.getAddPagerSite());
        setAddMobileSite(awsk.getAddMobileSite());
        setAddFaxSite(awsk.getAddFaxSite());
        setSiteCodelstType(awsk.getSiteCodelstType());
        setSiteName(awsk.getSiteName());
        setCtepId(awsk.getCtepId());
        setSiteNotes(awsk.getSiteNotes());
        setSiteInfo(awsk.getSiteInfo());
        setSiteParent(awsk.getSiteParent());
        setUserCodelstJobtype(awsk.getUserCodelstJobtype());
        setUserLastName(awsk.getUserLastName());
        setUserFirstName(awsk.getUserFirstName());
        setUserMidName(awsk.getUserMidName());
        setUserWrkExp(awsk.getUserWrkExp());
        setUserPhaseInv(awsk.getUserPhaseInv());
        setUserSessionTime(awsk.getUserSessionTime());
        setUserLoginName(awsk.getUserLoginName());
        setUserPwd(awsk.getUserPwd());
        setUserSecQues(awsk.getUserSecQues());
        setUserAnswer(awsk.getUserAnswer());
        setUserCodelstSpl(awsk.getUserCodelstSpl());
        setUserTimeZoneId(awsk.getUserTimeZoneId());
        setUserType(awsk.getUserType());
        setMsgcntrToUserId(awsk.getMsgcntrToUserId());
        setUserId(awsk.getUserId());
        setAccStatus(awsk.getAccStatus());
        setAccStartDate(awsk.getAccStartDate());
        setMsgcntrStatus(awsk.getMsgcntrStatus());
        setCreator(awsk.getCreator());
        setModifiedBy(awsk.getModifiedBy());
        setIpAdd(awsk.getIpAdd());
        setUserPwdExpiryDate(awsk.getUserPwdExpiryDate());
        setUserPwdExpiryDays(awsk.getUserPwdExpiryDays());
        setUserPwdReminderDate(awsk.getUserPwdReminderDate());
        setUserESign(awsk.getUserESign());
        setUserESignExpiryDate(awsk.getUserESignExpiryDate());
        setUserESignExpiryDays(awsk.getUserESignExpiryDays());
        setUserESignReminderDate(awsk.getUserESignReminderDate());
        this.setUserLoginModuleId(awsk.getUserLoginModuleId());
        this.setUserLoginModuleMap(awsk.getUserLoginModuleMap());
        setSiteHidden(awsk.getSiteHidden());//KM
        setUserHidden(awsk.getUserHidden());
        setPoIdentifier(awsk.getPoIdentifier());
        Rlog.debug("accountWrapper",
                "AccountWrapperStateKeeper.setAccountWrapperStateKeeper() ");
    }

}
