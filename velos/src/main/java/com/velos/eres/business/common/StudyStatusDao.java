 
//Dinesh dt 04/09/2001
	
package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;

public class StudyStatusDao extends CommonDAO implements java.io.Serializable {

    private ArrayList ids;

    private ArrayList statDocUsers;

    private ArrayList studyStatus;

    private ArrayList statStudys;

    private ArrayList statStartDates;

    private ArrayList statHSPNs;

    private ArrayList statNotes;
    
    private ArrayList externalLinks;

    private ArrayList statEndDates;

    private ArrayList statValidDates;
    
    private ArrayList statApprovals;

    private ArrayList descStudyStats;

    private ArrayList descAprStats;

    private ArrayList studyStartDates;

    private ArrayList studyEndDates;

    private ArrayList subTypeStats;

    private ArrayList siteIds;

    private ArrayList siteNames;

    private int firstNonActiveStatus;
    
    private int enrolledPatCount;
    
    //JM: 25/10/2005
    
    //private ArrayList irbStatuses;

    private ArrayList maxStudyStatDates;
    
    private ArrayList irbExpirationDates;
    
    private ArrayList expDateDiffs;
    
    //  Added by Manimaran for the September Enhancement S8.
    private ArrayList currentStats;
    
    private ArrayList studyStatList;
    
    private Hashtable studyStatCountMap;
    
    //Added By Amarnadh 
    private ArrayList statMeetingDates; 
    
    private ArrayList studyStatusOutcome; 
    
    private ArrayList studyStatusType;
    
    private ArrayList statusCreatedOn; //Added for CTRP-20579, Ankit
    
	public StudyStatusDao() {
        Rlog.debug("studystatus", "in the StudyStatusDao default constructor");
        ids = new ArrayList();
        statDocUsers = new ArrayList();
        studyStatus = new ArrayList();
        statStudys = new ArrayList();
        statStartDates = new ArrayList();
        statHSPNs = new ArrayList();
        statNotes = new ArrayList();
        externalLinks = new ArrayList();
        statEndDates = new ArrayList();
        statValidDates = new ArrayList();
        statApprovals = new ArrayList();
        descStudyStats = new ArrayList();
        descAprStats = new ArrayList();
        studyStartDates = new ArrayList();
        studyEndDates = new ArrayList();
        subTypeStats = new ArrayList();
        siteIds = new ArrayList();
        siteNames = new ArrayList();
        firstNonActiveStatus = 0;
        enrolledPatCount = 0;
        //JM: 25/10/2005
        //irbStatuses = new ArrayList();        
        maxStudyStatDates = new ArrayList();
        irbExpirationDates = new ArrayList();
        expDateDiffs = new ArrayList(); 
        currentStats = new ArrayList();
        studyStatList = new ArrayList();
        studyStatCountMap=new Hashtable();
        // Amarnadh :
        statMeetingDates = new ArrayList();
        studyStatusOutcome = new ArrayList();
        studyStatusType = new ArrayList();
        statusCreatedOn = new ArrayList();        

    }

    // Getter and Setter methods

    
    public ArrayList getIds() {
        return this.ids;
    }
    
    public Integer getIds(int idx) {
    	if (ids != null && idx < ids.size())
    	{
    		return (Integer) ids.get(idx);
    	}
    	else
    	{
    		return null;
    	}
    	
    }


   
	public void setIds(ArrayList ids) {
        this.ids = ids;
    }

    public ArrayList getStatDocUsers() {
        return this.statDocUsers;
    }
    
    public String getStatDocUsers(int idx) {
    	if (statDocUsers != null && idx < statDocUsers.size())
    	{
    		return (String) statDocUsers.get(idx);
    	}
    	else
    	{
    		return null;
    	}
    	
    }

    

    public void setStatDocUsers(ArrayList statDocUsers) {
        this.statDocUsers = statDocUsers;
    }

    public ArrayList getStudyStatus() {
        return this.studyStatus;
    }

    public void setStudyStatus(ArrayList studyStatus) {
        this.studyStatus = studyStatus;
    }
    
    public ArrayList getStatStudys() {
        return this.statStudys;
    }

    public void setStatStudys(ArrayList statStudys) {
        this.statStudys = statStudys;
    }

    public ArrayList getStatStartDates() {
        return this.statStartDates;
    }

    public void setStatStartDates(ArrayList statStartDates) {
        this.statStartDates = statStartDates;
    }

    public ArrayList getStatHSPNs() {
        return this.statHSPNs;
    }

    public void setStatHSPNs(ArrayList statHSPNs) {
        this.statHSPNs = statHSPNs;
    }

    public ArrayList getStatNotes() {
        return this.statNotes;
    }

    public void setStatNotes(ArrayList statNotes) {
        this.statNotes = statNotes;
    }

    public ArrayList getStatEndDates() {
        return this.statEndDates;
    }

    public void setStatEndDates(ArrayList statEndDates) {
        this.statEndDates = statEndDates;
    }

    public ArrayList getStatValidDates() {
        return this.statValidDates;
    }

    public void setStatValidDates(ArrayList statValidDates) {
        this.statValidDates = statValidDates;
    }

    public ArrayList getStatApprovals() {
        return this.statApprovals;
    }

    public void setStatApprovals(ArrayList statApprovals) {
        this.statApprovals = statApprovals;
    }

    public ArrayList getDescAprStats() {
        return this.descAprStats;
    }

    public void setDescAprStats(ArrayList descAprStats) {
        this.descAprStats = descAprStats;
    }

    public ArrayList getDescStudyStats() {
        return this.descStudyStats;
    }

    public void setDescStudyStats(ArrayList descStudyStats) {
        this.descStudyStats = descStudyStats;
    }

    public ArrayList getSubTypeStudyStats() {
        return this.subTypeStats;
    }

    public void setSubTypeStudyStats(ArrayList subTypeStats) {
        this.subTypeStats = subTypeStats;
    }

    public ArrayList getStudyStartDates() {
        return this.studyStartDates;
    }

    public void setStudyStartDates(ArrayList studyStartDates) {
        this.studyStartDates = studyStartDates;
    }

    public ArrayList getStudyEndDates() {
        return this.studyEndDates;
    }

    public void setStudyEndDates(ArrayList studyEndDates) {
        this.studyEndDates = studyEndDates;
    }

    public ArrayList getMaxStudyStatDates(){
    	return this.maxStudyStatDates;
    }    
    
    public void setMaxStudyStatDates(ArrayList maxStudyStatDates){
    	this.maxStudyStatDates = maxStudyStatDates;
    }
    public ArrayList getIrbExpirationDates(){
    	return this.irbExpirationDates;
    }    
    
    public void setIrbExpirationDates(ArrayList irbExpirationDates){
    	this.irbExpirationDates = irbExpirationDates;
    }    
        
    public ArrayList getExpDateDiffs(){
    	return this.expDateDiffs;
    }    
    
    public void setExpDateDiffs(ArrayList expDateDiffs){
    	this.expDateDiffs = expDateDiffs;
    }
    
    //Added by Manimaran for September Enhancement S8.
    public ArrayList getCurrentStats() {
    	return this.currentStats;
    }
    
    public void setCurrentStats(ArrayList currentStats) {
    	this.currentStats = currentStats;
    }
    
    //Added by Amarnadh  
    
    public ArrayList getStatMeetingDates() {
        return this.statMeetingDates;
    }

    public void setStatMeetingDates(ArrayList statMeetingDates) {
        this.statMeetingDates = statMeetingDates;
    }    
  
    public ArrayList getStudyStatusOutcome() {
        return this.studyStatusOutcome;
    }

    public void setStudyStatusOutcome(ArrayList studyStatusOutcome) {
        this.studyStatusOutcome = studyStatusOutcome;
    }
    
    public ArrayList getStudyStatusType() {
        return this.studyStatusType;
    }

    public void setStudyStatusType(ArrayList studyStatusType) {
        this.studyStatusType = studyStatusType;
    }

	public ArrayList getStatusCreatedOn() {
		return statusCreatedOn;
	}

	public void setStatusCreatedOn(ArrayList statusCreatedOn) {
		this.statusCreatedOn = statusCreatedOn;
	}
    
    // /overloaded setter methods

    public void setIds(Integer id) {
        ids.add(id);
    }

    public void setStatDocUsers(String statDocUser) {
        statDocUsers.add(statDocUser);
    }

    public void setStudyStatus(String studyStat) {
        studyStatus.add(studyStat);
    }
    
    public String getStudyStatus(int idx) {
    	if (studyStatus != null && idx < studyStatus.size())
    	{
    		return (String) studyStatus.get(idx);
    	}
    	else
    	{
    		return null;
    	}
    	
    }

    
    public void setStatStudys(String statStudy) {
        statStudys.add(statStudy);
    }
    
    public String getStatStudys(int idx) {
    	if (statStudys != null && idx < statStudys.size())
    	{
    		return (String) statStudys.get(idx);
    	}
    	else
    	{
    		return null;
    	}
    	
    }


    public void setStatStartDates(String statStartDate) {
        statStartDates.add(statStartDate);
    }
    
    public String getStatStartDates(int idx) {
    	if (statStartDates != null && idx < statStartDates.size())
    	{
    		return (String) statStartDates.get(idx);
    	}
    	else
    	{
    		return null;
    	}
    	
    }


    public void setStatHSPNs(String statHSPN) {
        statHSPNs.add(statHSPN);
    }

    public void setStatNotes(String statNote) {
        statNotes.add(statNote);
    }
    
    public String getStatNotes(int idx) {
    	if (statNotes != null && idx < statNotes.size())
    	{
    		return (String) statNotes.get(idx);
    	}
    	else
    	{
    		return null;
    	}
    	
    }


    public void setStatEndDates(String statEndDate) {
        statEndDates.add(statEndDate);
    }
    
    public String getStatEndDates(int idx) {
    	if (statEndDates != null && idx < statEndDates.size())
    	{
    		return (String) statEndDates.get(idx);
    	}
    	else
    	{
    		return null;
    	}
    	
    }


    public void setStatValidDates(String statValidDate) {
        statValidDates.add(statValidDate);
    }
    
    public String getStatValidDates(int idx) {
    	if (statValidDates!= null && idx < statValidDates.size())
    	{
    		return (String) statValidDates.get(idx);
    	}
    	else
    	{
    		return null;
    	}
    	
    }

    // Added by Amarnadh 
    
    public void setStatMeetingDates(String statMeetingDate) {
        statMeetingDates.add(statMeetingDate);
    }
    
    public String getStatMeetingDates(int idx) {
    	if (statMeetingDates!= null && idx < statMeetingDates.size())
    	{
    		return (String) statMeetingDates.get(idx);
    	}
    	else
    	{
    		return null;
    	}
    	
    }
    
    public void setStudyStatusOutcome(String studyStatOutcome) {
        this.studyStatusOutcome.add(studyStatOutcome);
    }
    
    public String getSStudyStatusOutcome(int idx) {
    	if (studyStatusOutcome != null && idx < studyStatusOutcome.size())
    	{
    		return (String) studyStatusOutcome.get(idx);
    	}
    	else
    	{
    		return null;
    	}
    	
    }
    
    public void setStudyStatusType(String studyStatType) {
        this.studyStatusType.add(studyStatType);
    }
    
    public String getSStudyStatusType(int idx) {
    	if (studyStatusType != null && idx < studyStatusType.size())
    	{
    		return (String) studyStatusType.get(idx);
    	}
    	else
    	{
    		return null;
    	}
    	
    }

    //End added
    
    //Overloaded method CTRP-20579, Ankit
    public void setStatusCreatedOn(String statusCreatedOn) {
        this.statusCreatedOn.add(statusCreatedOn);
    }
    
    public String getStatusCreatedOn(int idx) {
    	if (statusCreatedOn != null && idx < statusCreatedOn.size())
    	{
    		return (String) statusCreatedOn.get(idx);
    	}
    	else
    	{
    		return null;
    	}
    	
    }

    public void setStatApprovals(String statApproval) {
        statApprovals.add(statApproval);
    }
    
    public String getStatApprovals(int idx) {
    	if (statApprovals!= null && idx < statApprovals.size())
    	{
    		return (String) statApprovals.get(idx);
    	}
    	else
    	{
    		return null;
    	}
    	
    }

    public void setDescAprStats(String descAprStat) {
        descAprStats.add(descAprStat);
    }
    
    public String getDescAprStats(int idx) {
    	if (descAprStats!= null && idx < descAprStats.size())
    	{
    		return (String) descAprStats.get(idx);
    	}
    	else
    	{
    		return null;
    	}
    	
    }

    

    public void setDescStudyStats(String descStudyStat) {
        descStudyStats.add(descStudyStat);
    }
    
    public String getDescStudyStats(int idx) {
    	if (descStudyStats!= null && idx < descStudyStats.size())
    	{
    		return (String) descStudyStats.get(idx);
    	}
    	else
    	{
    		return null;
    	}
    	
    }

    

    public void setSubTypeStudyStats(String subType) {
        subTypeStats.add(subType);
    }
    
    public String getSubTypeStudyStats(int idx) {
    	if (subTypeStats!= null && idx < subTypeStats.size())
    	{
    		return (String) subTypeStats.get(idx);
    	}
    	else
    	{
    		return null;
    	}
    	
    }
    

    public void setStudyStartDates(String studyStartDate) {
        studyStartDates.add(studyStartDate);
    }

    public void setStudyEndDates(String studyEndDate) {
        studyEndDates.add(studyEndDate);
    }

    public int getFirstNonActiveStatusId() {
        return this.firstNonActiveStatus;
    }

    public int getEnrPatCnt() {
        return this.enrolledPatCount;
    }

    public ArrayList getSiteIds() {
        return this.siteIds;
    }
    
    public String getSiteIds(int idx) {
    	if (siteIds!= null && idx < siteIds.size())
    	{
    		return (String) siteIds.get(idx);
    	}
    	else
    	{
    		return null;
    	}
    	
    }
   

    public void setSiteIds(ArrayList siteIds) {
        this.siteIds = siteIds;
    }

    public void setSiteIds(String siteId) {
        siteIds.add(siteId);
    }

    public ArrayList getSiteNames() {
        return this.siteNames;
    }
    
    public String getSiteNames(int idx) {
    	if (siteNames!= null && idx < siteNames.size())
    	{
    		return (String) siteNames.get(idx);
    	}
    	else
    	{
    		return null;
    	}
    	
    }
   

    public void setSiteNames(ArrayList siteNames) {
        this.siteNames = siteNames;
    }

    public void setSiteNames(String siteName) {
        siteNames.add(siteName);
    }

    
    public void setMaxStudyStatDates(String maxStudyStatDate){
    	maxStudyStatDates.add(maxStudyStatDate);
    }
    public void setIrbExpirationDates(String irbExpDate){
    	irbExpirationDates.add(irbExpDate);
    }
    
    public void setExpDateDiffs(String expDateDif){
    	expDateDiffs.add(expDateDif);
    }
    
    public void setCurrentStats(Integer currentStat) {
        currentStats.add(currentStat);
    }
    /**
	 * @return the studyStatList
	 */
	public ArrayList getStudyStatList() {
		return studyStatList;
	}

	/**
	 * @param studyStatList the studyStatList to set
	 */
	public void setStudyStatList(ArrayList studyStatList) {
		this.studyStatList = studyStatList;
	}
	
	/**
	 * @param studyStatList the studyStatList to set
	 */
	public void setStudyStatList(String studyStat) {
		this.studyStatList.add(studyStat);
	}
	
	/**
	 * @return the studyStatCountMap
	 */
	public Hashtable getStudyStatCountMap() {
		return studyStatCountMap;
	}

	/**
	 * @param studyStatCountMap the studyStatCountMap to set
	 */
	public void setStudyStatCountMap(Hashtable studyStatCountMap) {
		this.studyStatCountMap = studyStatCountMap;
	}
	
	public void setStudyStatCountMap(String key,String value) {
		this.studyStatCountMap.put(key, value);
	}

    // end of getter and setter methods

    /*
     * Modified by sonia 08/04/04 for javadoc and changed order by to
     * studystat_date DESC, PK_STUDYSTAT DESC
     */

    /**
     * Populates the Data Access Object with study status records
     * 
     * @param study
     *            Study Id
     */

    /*
     * * Also calls getFirstNonActiveStatus() to get the status id of the I
     * non-active status Also calls getEnrolledPatientCount() to get the
     * enrolled patient count
     */
    /*
     * Modified by Sonia Abrol, 06/28/05, changed the logic, in study status
     * page, we need status of all organizations that are available in study
     * team page. also when siteId is passed we do not have to check the site
     * rights because they are already checked in the organization dropdown
     */
    /** JM: 25/10/2005 Query modified to get the IRB status indicator, IRB expiry date for a particular site,
     *  date differnce among IRB expiry date and current date.    
     * 
     */
	
	//Amarnadh :Query modified to get the meeting date ,outcome and status type.  
    
    public void getStudyStatusDesc(int study, int siteId, int usr, int accId) {

        Rlog.debug("studystatus", "in DAO: studystatus");
        PreparedStatement pstmt = null;
        String sql1 = "";
        String sql2 = "";
        String sql3 = "";
        String mysql = "";
        Connection conn = null;
        StringBuffer sbOrgCheck = new StringBuffer();
        try {
            conn = getConnection();
            Rlog.debug("studystatus", "got connection" + conn);
            sql1 = " SELECT a.PK_STUDYSTAT, a.FK_USER_DOCBY, a.FK_CODELST_STUDYSTAT,a.OUTCOME, a.STATUS_TYPE,a.FK_STUDY, a.STUDYSTAT_DATE,  "
                    + " a.STUDYSTAT_HSPN, a.STUDYSTAT_NOTE, a.EXTERNAL_LINK, a.STUDYSTAT_ENDT, a.STUDYSTAT_VALIDT,a.STUDYSTAT_MEETDT, a.FK_CODELST_APRSTAT, a.FK_SITE, a.CURRENT_STAT, "
                    + " (select site_name from er_site where pk_site = fk_site) as site_name, c.CODELST_DESC STUDYSTAT, "
                    + " c.CODELST_SUBTYP SUBTYPE, (Select CODELST_DESC from ER_CODELST where a.FK_CODELST_APRSTAT = PK_CODELST) APRSTAT "
                    + " , STUDY_ACTUALDT,   STUDY_END_DATE, "                    
                    + " (select max(studystat_validt) from er_studystat, er_site where fk_study = ?"
                    
                    + " and fk_site = a.FK_SITE and er_studystat.fk_codelst_studystat in (select pk_codelst from "
                    + " er_codelst where codelst_subtyp=(select ctrl_value from er_ctrltab " 
                    + " where ctrl_key ='irb_app_stat'))) IRB_EXP_DATE,"
                    + " (select max(STUDYSTAT_DATE) from er_studystat,er_site where fk_study=? and fk_site=a.fk_site and "
                    + " er_studystat.fk_codelst_studystat in (select pk_codelst from er_codelst where codelst_subtyp=(select ctrl_value "
                    + " from er_ctrltab where ctrl_key ='irb_app_stat'))) MAX_STUDYSTAT_DATE ,"
                    
                    + " replace((trunc((select max(studystat_validt) from er_studystat, er_site where fk_study = ? "
                   
                    + " and fk_site=a.fk_site and er_studystat.fk_codelst_studystat in (select pk_codelst from  " 
                    + " er_codelst where codelst_subtyp=(select ctrl_value from er_ctrltab  "
                    + " where ctrl_key ='irb_app_stat')))- sysdate)),'-','') DATEDIFF,a.CREATED_ON "
                    + " FROM ER_STUDYSTAT a, ER_STUDY b, ER_CODELST c WHERE a.FK_STUDY= ? "
                    + " AND a.FK_STUDY = b.PK_STUDY AND c.PK_CODELST= a.FK_CODELST_STUDYSTAT  ";            
            
            if (siteId != 0) {
                sql2 = " AND FK_SITE = ? ";
            } else {
                sbOrgCheck
                        .append(" AND FK_SITE IN (SELECT FK_SITE AS site_id ");
                sbOrgCheck.append(" FROM ER_STUDYSITES WHERE fk_study = ? ");
                sbOrgCheck.append(" UNION ");
                sbOrgCheck
                        .append(" SELECT FK_SITEID FROM ER_STUDYTEAM, ER_USER ");
                sbOrgCheck
                        .append(" WHERE FK_STUDY = ?  AND NVL(STUDY_TEAM_USR_TYPE,'D') = 'D' AND ");
                sbOrgCheck.append(" pk_user = fk_user ) ");
                sql2 = sbOrgCheck.toString();
            }

            sql3 = " order by FK_SITE, STUDYSTAT_DATE DESC,  PK_STUDYSTAT DESC";

            mysql = sql1 + sql2 + sql3;
            
            Rlog.debug("studystatus", mysql);
            pstmt = conn.prepareStatement(mysql);
            Rlog.debug("studystatus", "pstmt " + pstmt);

            pstmt.setInt(1, study);
            pstmt.setInt(2, study);
            pstmt.setInt(3, study);
            pstmt.setInt(4, study);
            
            if (siteId != 0) {
                pstmt.setInt(5, siteId);
            } else {
                pstmt.setInt(5, study);
                pstmt.setInt(6, study);
            }

             
            
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("studystatus", "got resultset" + rs);

            while (rs.next()) {

                setIds(new Integer(rs.getInt("PK_STUDYSTAT")));
                setStatDocUsers(rs.getString("FK_USER_DOCBY"));
                setStudyStatus(rs.getString("FK_CODELST_STUDYSTAT"));
                setStatStudys(rs.getString("FK_STUDY"));
                setSiteIds(rs.getString("FK_SITE"));
                setSiteNames(rs.getString("site_name"));
                setStatStartDates(rs.getString("STUDYSTAT_DATE"));
                setStatHSPNs(rs.getString("STUDYSTAT_HSPN"));
                setStatNotes(rs.getString("STUDYSTAT_NOTE"));
                setExternalLinks(rs.getString("EXTERNAL_LINK"));
                setStatEndDates(rs.getString("STUDYSTAT_ENDT"));
                setStatValidDates(rs.getString("STUDYSTAT_VALIDT"));
                setStatApprovals(rs.getString("FK_CODELST_APRSTAT"));

                setDescStudyStats(rs.getString("STUDYSTAT")); // table codelst
                setSubTypeStudyStats(rs.getString("SUBTYPE")); // table codelst
                setDescAprStats(rs.getString("APRSTAT")); // table codelst

                setStudyStartDates(rs.getString("STUDY_ACTUALDT")); // table
                // study
                setStudyEndDates(rs.getString("STUDY_END_DATE")); // table
                // study                
              
                setIrbExpirationDates(rs.getString("IRB_EXP_DATE"));
                setMaxStudyStatDates(rs.getString("MAX_STUDYSTAT_DATE"));
                setExpDateDiffs(rs.getString("DATEDIFF"));
                setCurrentStats(new Integer(rs.getInt("CURRENT_STAT")));//KM
                
                setStatMeetingDates(rs.getString("STUDYSTAT_MEETDT"));
                setStudyStatusOutcome(rs.getString("OUTCOME"));
                setStudyStatusType(rs.getString("STATUS_TYPE"));
                setStatusCreatedOn(rs.getString("CREATED_ON")); //Added for CTRP-20579, Ankit

            }

            // find the first non active status
            getFirstNonActiveStatus(study);

            // get the enrolled patient count
            getEnrolledPatientCount(study);

            Rlog.debug("studystatus", "StudyStatusDao.rows ");
        } catch (SQLException ex) {
            Rlog.fatal("studystatus",
                    "studystatusDao.getMsgcntr EXCEPTION IN FETCHING FROM er_studystat ........."
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     *  This method is the same as getStudyStatusDesc except that any future dates will be treated
     *  as todays date with the help of F_NON_FUTURE_DATE function.
     */
    public void getStudyStatusDescNonFuture(int study, int siteId, int usr, int accId) {

        Rlog.debug("studystatus", "in DAO: studystatus");
        PreparedStatement pstmt = null;
        String sql1 = "";
        String sql2 = "";
        String sql3 = "";
        String mysql = "";
        Connection conn = null;
        StringBuffer sbOrgCheck = new StringBuffer();
        try {
            conn = getConnection();
            Rlog.debug("studystatus", "got connection" + conn);
            sql1 = " SELECT a.PK_STUDYSTAT, a.FK_USER_DOCBY, a.FK_CODELST_STUDYSTAT,a.OUTCOME, a.STATUS_TYPE,a.FK_STUDY, F_NON_FUTURE_DATE(a.STUDYSTAT_DATE) STUDYSTAT_DATE,  "
                    + " a.STUDYSTAT_HSPN, a.STUDYSTAT_NOTE, a.EXTERNAL_LINK, a.STUDYSTAT_ENDT, a.STUDYSTAT_VALIDT,a.STUDYSTAT_MEETDT, a.FK_CODELST_APRSTAT, a.FK_SITE, a.CURRENT_STAT, "
                    + " (select site_name from er_site where pk_site = fk_site) as site_name, c.CODELST_DESC STUDYSTAT, "
                    + " c.CODELST_SUBTYP SUBTYPE, (Select CODELST_DESC from ER_CODELST where a.FK_CODELST_APRSTAT = PK_CODELST) APRSTAT "
                    + " , STUDY_ACTUALDT,   STUDY_END_DATE, "                    
                    + " (select max(studystat_validt) from er_studystat, er_site where fk_study = ?"
                    
                    + " and fk_site = a.FK_SITE and er_studystat.fk_codelst_studystat in (select pk_codelst from "
                    + " er_codelst where codelst_subtyp=(select ctrl_value from er_ctrltab " 
                    + " where ctrl_key ='irb_app_stat'))) IRB_EXP_DATE,"
                    + " (select max(F_NON_FUTURE_DATE(STUDYSTAT_DATE)) from er_studystat,er_site where fk_study=? and fk_site=a.fk_site and "
                    + " er_studystat.fk_codelst_studystat in (select pk_codelst from er_codelst where codelst_subtyp=(select ctrl_value "
                    + " from er_ctrltab where ctrl_key ='irb_app_stat'))) MAX_STUDYSTAT_DATE ,"
                    
                    + " replace((trunc((select max(studystat_validt) from er_studystat, er_site where fk_study = ? "
                   
                    + " and fk_site=a.fk_site and er_studystat.fk_codelst_studystat in (select pk_codelst from  " 
                    + " er_codelst where codelst_subtyp=(select ctrl_value from er_ctrltab  "
                    + " where ctrl_key ='irb_app_stat')))- sysdate)),'-','') DATEDIFF,a.CREATED_ON "
                    + " FROM ER_STUDYSTAT a, ER_STUDY b, ER_CODELST c WHERE a.FK_STUDY= ? "
                    + " AND a.FK_STUDY = b.PK_STUDY AND c.PK_CODELST= a.FK_CODELST_STUDYSTAT  ";            
            
            if (siteId != 0) {
                sql2 = " AND FK_SITE = ? ";
            } else {
                sbOrgCheck
                        .append(" AND FK_SITE IN (SELECT FK_SITE AS site_id ");
                sbOrgCheck.append(" FROM ER_STUDYSITES WHERE fk_study = ? ");
                sbOrgCheck.append(" UNION ");
                sbOrgCheck
                        .append(" SELECT FK_SITEID FROM ER_STUDYTEAM, ER_USER ");
                sbOrgCheck
                        .append(" WHERE FK_STUDY = ?  AND NVL(STUDY_TEAM_USR_TYPE,'D') = 'D' AND ");
                sbOrgCheck.append(" pk_user = fk_user ) ");
                sql2 = sbOrgCheck.toString();
            }

            sql3 = " order by FK_SITE, STUDYSTAT_DATE DESC,  PK_STUDYSTAT DESC";

            mysql = sql1 + sql2 + sql3;
            
            Rlog.debug("studystatus", mysql);
            pstmt = conn.prepareStatement(mysql);
            Rlog.debug("studystatus", "pstmt " + pstmt);

            pstmt.setInt(1, study);
            pstmt.setInt(2, study);
            pstmt.setInt(3, study);
            pstmt.setInt(4, study);
            
            if (siteId != 0) {
                pstmt.setInt(5, siteId);
            } else {
                pstmt.setInt(5, study);
                pstmt.setInt(6, study);
            }
            
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("studystatus", "got resultset" + rs);

            while (rs.next()) {
                setIds(new Integer(rs.getInt("PK_STUDYSTAT")));
                setStatDocUsers(rs.getString("FK_USER_DOCBY"));
                setStudyStatus(rs.getString("FK_CODELST_STUDYSTAT"));
                setStatStudys(rs.getString("FK_STUDY"));
                setSiteIds(rs.getString("FK_SITE"));
                setSiteNames(rs.getString("site_name"));
                setStatStartDates(rs.getString("STUDYSTAT_DATE"));
                setStatHSPNs(rs.getString("STUDYSTAT_HSPN"));
                setStatNotes(rs.getString("STUDYSTAT_NOTE"));
                setExternalLinks(rs.getString("EXTERNAL_LINK"));
                setStatEndDates(rs.getString("STUDYSTAT_ENDT"));
                setStatValidDates(rs.getString("STUDYSTAT_VALIDT"));
                setStatApprovals(rs.getString("FK_CODELST_APRSTAT"));
                setDescStudyStats(rs.getString("STUDYSTAT")); // table codelst
                setSubTypeStudyStats(rs.getString("SUBTYPE")); // table codelst
                setDescAprStats(rs.getString("APRSTAT")); // table codelst
                setStudyStartDates(rs.getString("STUDY_ACTUALDT")); // table
                setStudyEndDates(rs.getString("STUDY_END_DATE")); // table
                setIrbExpirationDates(rs.getString("IRB_EXP_DATE"));
                setMaxStudyStatDates(rs.getString("MAX_STUDYSTAT_DATE"));
                setExpDateDiffs(rs.getString("DATEDIFF"));
                setCurrentStats(new Integer(rs.getInt("CURRENT_STAT")));//KM
                setStatMeetingDates(rs.getString("STUDYSTAT_MEETDT"));
                setStudyStatusOutcome(rs.getString("OUTCOME"));
                setStudyStatusType(rs.getString("STATUS_TYPE"));
                setStatusCreatedOn(rs.getString("CREATED_ON")); //Added for CTRP-20579, Ankit
            }

            // find the first non active status
            getFirstNonActiveStatus(study);

            // get the enrolled patient count
            getEnrolledPatientCount(study);

            Rlog.debug("studystatus", "StudyStatusDao.rows ");
        } catch (SQLException ex) {
            Rlog.fatal("studystatus",
                    "studystatusDao.getMsgcntr EXCEPTION IN FETCHING FROM er_studystat ........."
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
    
    public void getStudy(int study) {

        Rlog.debug("studystatus", "in DAO: studystatus");
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            Rlog.debug("studystatus", "got connection" + conn);
            String mysql = "SELECT PK_STUDYSTAT," + "FK_USER_DOCBY,"
                    + "FK_CODELST_STUDYSTAT," + "FK_STUDY," + "STUDYSTAT_DATE,"
                    + "STUDYSTAT_HSPN," + "STUDYSTAT_NOTE," + "STUDYSTAT_ENDT,"
                    + "FK_CODELST_APRSTAT"
                    + " FROM ER_STUDYSTAT WHERE FK_STUDY =?";

            Rlog.debug("studystatus", mysql);
            pstmt = conn.prepareStatement(mysql);

            Rlog.debug("studystatus", "pstmt " + pstmt);
            pstmt.setInt(1, study);
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("studystatus", "got resultset" + rs);

            while (rs.next()) {

                setIds(new Integer(rs.getInt("PK_STUDYSTAT")));
                setStatDocUsers(rs.getString("FK_USER_DOCBY"));
                setStudyStatus(rs.getString("FK_CODELST_STUDYSTAT"));
                setStatStudys(rs.getString("FK_STUDY"));
                setStatStartDates(rs.getString("STUDYSTAT_DATE"));
                setStatHSPNs(rs.getString("STUDYSTAT_HSPN"));
                setStatNotes(rs.getString("STUDYSTAT_NOTE"));
                setStatEndDates(rs.getString("STUDYSTAT_ENDT"));
                setStatApprovals(rs.getString("FK_CODELST_APRSTAT"));
            }
            Rlog.debug("studystatus", "StudyStatusDao.rows ");
        } catch (SQLException ex) {
            Rlog.fatal("studystatus",
                    "studystatusDao.getMsgcntr EXCEPTION IN FETCHING FROM er_studystat"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    // Checks the current status of a study and
    // returns 1 if it is Active ie Open
    // 0 if it is not Active

    public int checkStatus(String studyId) {

        Rlog.debug("studystatus", "StudyStatusDAO.checkStatus");
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            Rlog.debug("studystatus",
                    "StudyStatusDAO.checkStatus got connection" + conn);
            String mysql = "select 	count(*) count " + "from 	er_studystat "
                    + "where 	fk_study = ? "
                    + "and 	fk_codelst_studystat = 	(select ctrl_value "
                    + "from	er_ctrltab " + "where 	ctrl_key = 'open') "
                    + "and 	STUDYSTAT_DATE =	(select max(STUDYSTAT_DATE) "
                    + "from	er_studystat " + "where 	fk_study = ?)";

            Rlog.debug("studystatus", mysql);
            pstmt = conn.prepareStatement(mysql);

            Rlog.debug("studystatus", "StudyStatusDAO.checkStatus pstmt "
                    + pstmt);
            pstmt.setString(1, studyId);
            pstmt.setString(2, studyId);
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("studystatus",
                    "StudyStatusDAO.checkStatus got resultset" + rs);

            rs.next();
            return rs.getInt("count");
        } catch (SQLException ex) {
            Rlog.fatal("studystatus",
                    "StudyStatusDAO.checkStatus EXCEPTION IN FETCHING FROM er_studystat"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return 0;
    }

    /*
     * Finds the first study_status_id with non active status. First non active
     * status can't be deleted
     */
    public void getFirstNonActiveStatus(int study) {
        Rlog
                .debug("studystatus",
                        "in getFirstNonActiveStatus DAO: studystatus");
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String mysql = "select min(PK_STUDYSTAT)  PK_STUDYSTAT from "
                    + "er_studystat , er_codelst "
                    + "where FK_CODELST_STUDYSTAT   = pk_codelst "
                    + "and codelst_subtyp='not_active' and "
                    + "codelst_type='studystat' " + "and fk_study = ? "
                    + "and studystat_date = (select min(studystat_date) "
                    + "from er_studystat  , er_codelst "
                    + "where fk_study=? and "
                    + "FK_CODELST_STUDYSTAT   = pk_codelst "
                    + "and  codelst_subtyp='not_active' and "
                    + "codelst_type='studystat' ) ";

            Rlog.debug("studystatus", mysql);
            pstmt = conn.prepareStatement(mysql);
            Rlog.debug("studystatus", "pstmt " + pstmt);

            pstmt.setInt(1, study);
            pstmt.setInt(2, study);
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("studystatus", "got resultset" + rs);

            while (rs.next()) {
                firstNonActiveStatus = rs.getInt("PK_STUDYSTAT");
            }
            Rlog.debug("studystatus",
                    "StudyStatusDao.getFirstNonActiveStatus rows ");
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "studystatus",
                            "studystatusDao.getFirstNonActiveStatus EXCEPTION IN FETCHING FROM er_studystat"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    /*
     * Counts the number of patients enrolled to a study Active/Enrolling status
     * can be deleted as long as patients have not been enrolled
     */
    public void getEnrolledPatientCount(int study) {
        Rlog
                .debug("studystatus",
                        "in getEnrolledPatientCount DAO: studystatus");
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String mysql = "select count(*) cnt from erv_patstudystat where fk_study= ?";
            pstmt = conn.prepareStatement(mysql);
            Rlog.debug("studystatus", "pstmt " + pstmt);
            pstmt.setInt(1, study);
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("studystatus", "got resultset" + rs);

            while (rs.next()) {
                enrolledPatCount = rs.getInt("cnt");
            }
            Rlog.debug("studystatus",
                    "StudyStatusDao.getEnrolledPatientCount rows ");
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "studystatus",
                            "studystatusDao.getEnrolledPatientCount EXCEPTION IN FETCHING FROM er_studystat"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    public int getMaxStudyStatId(int studyStatusId, int studyId) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String mysql = "";
            if (studyStatusId == 0) {
                mysql = "select max(PK_STUDYSTAT) maxId from ER_STUDYSTAT where fk_study= ?";
                pstmt = conn.prepareStatement(mysql);
                pstmt.setInt(1, studyId);
            } else {
                mysql = "select max(PK_STUDYSTAT) maxId from ER_STUDYSTAT where fk_study= ? and pk_studystat < ?";
                pstmt = conn.prepareStatement(mysql);
                pstmt.setInt(1, studyId);
                pstmt.setInt(2, studyStatusId);
            }
            Rlog.debug("studystatus", "studystatusDao.getMaxStudyStatId mysql"
                    + mysql);
            ResultSet rs = pstmt.executeQuery();

            rs.next();
            Rlog.debug("studystatus", "studystatusDao.getMaxStudyStatId mysql"
                    + mysql);
            return rs.getInt("maxId");
        } catch (SQLException ex) {
            Rlog.fatal("studystatus",
                    "studystatusDao.getMaxStudyStatId EXCEPTION IN FETCHING FROM er_studystat"
                            + ex);
            return -1;
        }finally
        {
            try
            {
                if (pstmt!=null) pstmt.close();
             }catch(Exception e)
             {
                 e.printStackTrace();
             }
             
             try
             {
                 if (conn!=null) conn.close();
              }catch(Exception e)
              {
                  e.printStackTrace();
              }
         
          }
        
    }

    /* Method to get the codeLst type of a status */

    public String getCodelstStatusType(int studyStatId, int studyId) {

        Rlog.debug("studystatus",
                "studystatusDao.getCodelstStatusType studyId##" + studyId);
        Rlog.debug("studystatus",
                "studystatusDao.getCodelstStatusType studyStatId##"
                        + studyStatId);
        PreparedStatement pstmt = null;
        Connection conn = null;
        String subType = null;
        try {
            conn = getConnection();
            String sql = "";

            sql = "select codelst_subtyp " + "	from er_studystat,er_codelst "
                    + " where fk_study=? " + " and codelst_type='studystat' "
                    + " and fk_codelst_studystat = pk_codelst "
                    + " and pk_studystat = ? ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);
            pstmt.setInt(2, studyStatId);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                subType = rs.getString("codelst_subtyp");
            }
            Rlog.debug("studystatus",
                    "studystatusDao.getCodelstStatusType sql##" + sql);
            Rlog.debug("studystatus",
                    "studystatusDao.getCodelstStatusType subType##" + subType);

        } catch (SQLException ex) {
            Rlog.fatal("studystatus",
                    "studystatusDao.getCodelstStatusType EXCEPTION IN FETCHING FROM er_studystat"
                            + ex);

        }finally
        {
            try
            {
                if (pstmt!=null) pstmt.close();
             }catch(Exception e)
             {
                 e.printStackTrace();
             }
             
             try
             {
                 if (conn!=null) conn.close();
              }catch(Exception e)
              {
                  e.printStackTrace();
              }
             
          }
        return subType;
    }

    /*
     * Method which calls SP to set Actual date to null if the status to be
     * deleted is the only active status in the study
     */

    public void setActualDateToNull(int studyId, String status) {

        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            Rlog.debug("formsection", "In setActualDateToNull### ");
            cstmt = conn
                    .prepareCall("{call PKG_STUDYSTAT.SP_SET_ACTUAL_DATE (?,?)}");
            cstmt.setInt(1, studyId);
            cstmt.setString(2, status);

            cstmt.execute();

        } catch (Exception e) {
            Rlog.fatal("formsection",
                    "EXCEPTION in setActualDateToNull, excecuting Stored Procedure "
                            + e);

        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /* Method to get the PK of the first Active/enroll status in a study */
    public int getFirstActiveEnrollPK(int studyId) {

        Rlog.debug("studystatus",
                "studystatusDao.getFirstActiveEnrollPK studyId##" + studyId);

        PreparedStatement pstmt = null;
        Connection conn = null;
        int firstActiveId = 0;
        try {
            conn = getConnection();
            String sql = "";

            sql = "select min(PK_STUDYSTAT) activeId"
                    + "	from er_studystat,er_codelst " + " where fk_study=? "
                    + " and codelst_type='studystat' "
                    + " and fk_codelst_studystat = pk_codelst "
                    + " and codelst_subtyp='active'";

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                firstActiveId = rs.getInt("activeId");
            }

        } catch (SQLException ex) {
            Rlog.fatal("studystatus",
                    "studystatusDao.getCodelstStatusType EXCEPTION IN FETCHING FROM er_studystat"
                            + ex);

        }finally
        {
            try
            {
                if (pstmt!=null) pstmt.close();
             }catch(Exception e)
             {
                 e.printStackTrace();
             }
             
             try
             {
                 if (conn!=null) conn.close();
              }catch(Exception e)
              {
                  e.printStackTrace();
              }
          }
        return firstActiveId;
    }

    /**
     * Method to find the minimum Active/ Enrolling date among all the Active/
     * Enrolling dates entered for a study
     * 
     * @param pk
     *            of study
     * @return String : minimum Active status date
     * 
     */
    public String getMinActiveDate(int studyId) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        String minActiveDate = "";
        java.sql.Date minDt = null;
        try {
            conn = getConnection();
            String sql = "";

            sql = "select  min(STUDYSTAT_DATE) as activeDate "
                    + " from er_studystat "
                    + " where  fk_study = ? "
                    + " and FK_CODELST_STUDYSTAT = "
                    + " (select pk_codelst "
                    + " from er_codelst where codelst_type ='studystat' "
                    + " and codelst_subtyp='active' )";

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                minDt = rs.getDate("activeDate");
            }

        } catch (SQLException ex) {
            Rlog.fatal("studystatus",
                    "studystatusDao.getMinActiveDate EXCEPTION IN FETCHING FROM er_studystat"
                            + ex);

        }finally
        {
            try
            {
                if (pstmt!=null) pstmt.close();
             }catch(Exception e)
             {
                 e.printStackTrace();
             }
             
             try
             {
                 if (conn!=null) conn.close();
              }catch(Exception e)
              {
                  e.printStackTrace();
              }
             
             
             
          }
        // IH: Bug 2609 fix - The date format inconsistency was causing a parse
        // exception in StudyBean. Then it was causing StudyAgentBean.updateStudy() to fail.
        // Finally it caused errors in the Audit Report. Handle format conversion here.
        if (minDt != null) {
            minActiveDate = DateUtil.dateToString(minDt);
        }
        return minActiveDate;
    }

    /**
     * Method to find the minimum Study completed/Retired date among all the
     * Study completed/Retired dates entered for a study
     * 
     * @param pk
     *            of study
     * @return minimum study closure status date
     * 
     */
    public String getMinPermanentClosureDate(int studyId) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        String minClosureDate = "";
        java.sql.Date minDt = null;
        try {
            conn = getConnection();
            String sql = "";

            sql = "select  min(STUDYSTAT_DATE) as closureDate"
                    + " from er_studystat "
                    + " where  fk_study=? "
                    + " and FK_CODELST_STUDYSTAT = "
                    + " (select pk_codelst from er_codelst "
                    + " where codelst_type ='studystat' "
                    + " and codelst_subtyp='prmnt_cls' )";

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                minDt = rs.getDate("closureDate");
            }

        } catch (SQLException ex) {
            Rlog.fatal("studystatus",
                    "studystatusDao.getMinActiveDate EXCEPTION IN FETCHING FROM er_studystat"
                            + ex);

        }finally
        {
            try
            {
                if (pstmt!=null) pstmt.close();
             }catch(Exception e)
             {
                 e.printStackTrace();
             }
             
             
             try
             {
                 if (conn!=null) conn.close();
              }catch(Exception e)
              {
                  e.printStackTrace();
              }
             
          }
        // IH: Bug 2609 fix - The date format inconsistency was causing a parse
        // exception in StudyBean. Then it was causing StudyAgentBean.updateStudy() to fail.
        // Finally it caused errors in the Audit Report. Handle format conversion here.
        if (minDt != null) {
            minClosureDate = DateUtil.dateToString(minDt);
        }
        return minClosureDate;
    }

    /**
     * Method to find the total number of study status entered for a study dates
     * entered for a study
     * 
     * @param pk
     *            of study
     * @return count for study status for a study
     * 
     */
    public int getCountStudyStat(int studyId) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int totalStat = 0;
        try {
            conn = getConnection();
            String sql = "";

            sql = " select count(*) as cnt from er_studystat where fk_study = ?";

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                totalStat = rs.getInt("cnt");
            }

        } catch (SQLException ex) {
            Rlog.fatal("studystatus",
                    "studystatusDao.getCountStudyStat EXCEPTION IN FETCHING FROM er_studystat"
                            + ex);

        }finally
        {
            try
            {
                if (pstmt!=null) pstmt.close();
             }catch(Exception e)
             {
                 e.printStackTrace();
             }
             
             try
             {
                 if (conn!=null) conn.close();
              }catch(Exception e)
              {
                  e.printStackTrace();
              }
             
          }
        return totalStat;
    }

    public void updateStartEndDate(int studyId,int userId) {

        CallableStatement cstmt = null;
        Connection conn = null;


        try {
            conn = getConnection();

            cstmt = conn
                    .prepareCall("{call PKG_USER.SP_UPDATE_START_END_DATES (?,?)}");
            cstmt.setInt(1, studyId);
            cstmt.setInt(2, userId);

            cstmt.execute();

        } catch (Exception e) {
            Rlog.fatal("studyStatus",
                    "EXCEPTION in updateStartEndDate, excecuting Stored Procedure "
                            + e);

        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
        
    public int getClosure(int studyId) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int totalStat = 0;
        try {
            conn = getConnection();
            String sql = "";

            sql = " select count(*) as cnt from er_studystat where fk_study = ?";

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                totalStat = rs.getInt("cnt");
            }

        } catch (SQLException ex) {
            Rlog.fatal("studystatus",
                    "studystatusDao.getCountStudyStat EXCEPTION IN FETCHING FROM er_studystat"
                            + ex);

        }finally
        {
            try
            {
                if (pstmt!=null) pstmt.close();
             }catch(Exception e)
             {
                 e.printStackTrace();
             }
             
             try
             {
                 if (conn!=null) conn.close();
              }catch(Exception e)
              {
                  e.printStackTrace();
              }
             
          }
        return totalStat;
    }
    
    /**
     * flag = "studyprot4pat"  // in this case we check statuses in patient organizations ORGID will contain patient PK
     * */
    public int getCountByOrgStudyStat(int studyId,int orgId,String studyTypeList,String flag) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        flag=(flag==null)?"":flag;
        if (flag.length()==0) flag="L"; 
        int totalStat = 0;
        
        try {
            conn = getConnection();
            String sql = "";
            if (flag.equals("L"))
                sql = " SELECT  codelst_subtyp FROM ER_STUDYSTAT,ER_CODELST WHERE fk_study = ? AND fk_site=? AND fk_codelst_studystat=pk_codelst " + 
                      "  AND codelst_type='studystat' AND codelst_subtyp IN ( " + studyTypeList + ") and studystat_endt is null";
            else if (flag.equals("studyprot"))
            {
            	sql= " SELECT SUM(active) active ,SUM(active_cls) active_cls,COUNT(DISTINCT fk_site) sitecount,'' as codelst_subtyp FROM ( " +
            		 "	SELECT DECODE(codelst_subtyp,'active',1,0) AS active ,DECODE(codelst_subtyp,'active_cls',1,0) AS active_cls,fk_site " +
            		"	FROM (SELECT  DISTINCT codelst_subtyp,fk_site FROM ER_STUDYSTAT,ER_CODELST WHERE fk_study = ?  AND fk_codelst_studystat=pk_codelst" +
            		"  AND codelst_type='studystat' AND codelst_subtyp IN ( " +studyTypeList+")))";
            }
              else
               {   
                	sql = " SELECT  codelst_subtyp FROM ER_STUDYSTAT,ER_CODELST WHERE fk_study = ? AND fk_site=? AND fk_codelst_studystat=pk_codelst " + 
                    "  AND codelst_type='studystat' AND codelst_subtyp IN ( " + studyTypeList + ")";
               } 	

            pstmt = conn.prepareStatement(sql);
            
             	pstmt.setInt(1, studyId);
            	
            	if (!flag.equals("studyprot"))
                    pstmt.setInt(2, orgId);
            	
            
            
            
            
            
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                 totalStat++;
                 
                 this.setStudyStatList(rs.getString("codelst_subtyp"));
                 if ((flag.equals("studyprot") || flag.equals("studyprot4pat")) && studyStatCountMap.size()==0)
                 {
                	 this.setStudyStatCountMap("active",rs.getString("active"));
                	 this.setStudyStatCountMap("active_cls",rs.getString("active_cls"));
                	 this.setStudyStatCountMap("sitecount",rs.getString("sitecount"));
                	 
                 }
            }
            
        } catch (SQLException ex) {
            Rlog.fatal("studystatus",
                    "studystatusDao.getCountByOrgStat EXCEPTION IN FETCHING FROM er_studystat"
                            + ex);

        }finally
        {
            try
            {
                if (pstmt!=null) pstmt.close();
             }catch(Exception e)
             {
                 e.printStackTrace();
             }
             
             try
             {
                 if (conn!=null) conn.close();
              }catch(Exception e)
              {
                  e.printStackTrace();
              }
          }
        return totalStat;
    }

    
    public void getStudyStatusDetails( int pkstudystatus) {
        PreparedStatement pstmt = null;
        StringBuffer sql = new StringBuffer();
        Connection conn = null;
         
        try {
            conn = getConnection();
             // modified by Amarnadh
            sql.append(" SELECT a.FK_USER_DOCBY, a.FK_CODELST_STUDYSTAT, a.FK_STUDY,OUTCOME, STATUS_TYPE,to_char(STUDYSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDYSTAT_DATE ,  ");
            sql.append(" a.STUDYSTAT_NOTE,  STUDYSTAT_ENDT, a.STUDYSTAT_VALIDT,a.STUDYSTAT_MEETDT, a.FK_CODELST_APRSTAT, a.FK_SITE, a.CURRENT_STAT, ");
            sql.append(" (select site_name from er_site where pk_site = fk_site) as site_name, c.CODELST_DESC STUDYSTAT, ");
            sql.append(" c.CODELST_SUBTYP SUBTYPE, (Select CODELST_DESC from ER_CODELST where a.FK_CODELST_APRSTAT = PK_CODELST) APRSTAT ");
            sql.append(" FROM ER_STUDYSTAT a,  ER_CODELST c WHERE pk_studystat = ? AND c.PK_CODELST= a.FK_CODELST_STUDYSTAT  ");            
            
            Rlog.debug("studystatus", sql.toString());
            pstmt = conn.prepareStatement(sql.toString());
            
            pstmt.setInt(1, pkstudystatus);
 
            ResultSet rs = pstmt.executeQuery();
  
            while (rs.next()) {

                setIds(new Integer(pkstudystatus));
                setStatDocUsers(rs.getString("FK_USER_DOCBY"));
                setStudyStatus(rs.getString("FK_CODELST_STUDYSTAT"));
                setStatStudys(rs.getString("FK_STUDY"));
                setSiteIds(rs.getString("FK_SITE"));
                setSiteNames(rs.getString("site_name"));
                setStatStartDates(rs.getString("STUDYSTAT_DATE"));
                 
                setStatNotes(rs.getString("STUDYSTAT_NOTE"));
                setStatEndDates(rs.getString("STUDYSTAT_ENDT"));
                setStatValidDates(rs.getString("STUDYSTAT_VALIDT"));
                setStatApprovals(rs.getString("FK_CODELST_APRSTAT"));

                setDescStudyStats(rs.getString("STUDYSTAT")); // table codelst
                setSubTypeStudyStats(rs.getString("SUBTYPE")); // table codelst
                setDescAprStats(rs.getString("APRSTAT")); // table codelst

                setCurrentStats(new Integer(rs.getInt("CURRENT_STAT")));
                
                setStatMeetingDates(rs.getString("STUDYSTAT_MEETDT")); 
                setStudyStatusOutcome(rs.getString("OUTCOME"));
                setStudyStatusType(rs.getString("STATUS_TYPE"));

            }

              
        } catch (SQLException ex) {
            Rlog.fatal("studystatus","studystatusDao.getStudyStatusDetails( int pkstudystatus) exception........." + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

	public ArrayList getExternalLinks() {
		return externalLinks;
	}

	public void setExternalLinks(ArrayList externalLinks) {
		this.externalLinks = externalLinks;
	}
	
    private void setExternalLinks(String externalLink) {
    	this.externalLinks.add(externalLink);
	}
    
	public String getLatestExternalLinkForStudy(int studyId) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		String externalLink = "";

		try {
			conn = getConnection();
			pstmt = conn
					.prepareStatement("select external_link from er_studystat where fk_study=? and external_link IS NOT NULL ORDER BY pk_studystat asc");
			pstmt.setInt(1, studyId);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				externalLink = rs.getString("external_link");
			}
		} catch (SQLException se) {
			Rlog.error("StudyStatusDao.getLatestExternalLinkForStudy method",
					se.getMessage());
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
				Rlog.error(
						"StudyStatusDao.getLatestExternalLinkForStudy method",
						e.getMessage());
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				Rlog.error(
						"StudyStatusDao.getLatestExternalLinkForStudy method",
						e.getMessage());
			}
		}

		return externalLink;

	}
	
	public String getCurrentStudyStatus( int studyId) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String fk_codelst_studystat = "";
		String curStat="";

		try {
			conn = getConnection();
			pstmt = conn
					.prepareStatement("select fk_codelst_studystat from er_studystat where fk_study=? and current_stat=1");
			pstmt.setInt(1, studyId);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				fk_codelst_studystat = rs.getString("fk_codelst_studystat");
			}
			
			if( fk_codelst_studystat != null && !(fk_codelst_studystat.equals("")) ) {
				pstmt = conn.prepareStatement("select codelst_subtyp from er_codelst where pk_codelst=?");
				pstmt.setInt(1, Integer.valueOf(fk_codelst_studystat));
				
				ResultSet result = pstmt.executeQuery();
				while (result.next()) {
					curStat = result.getString("codelst_subtyp");
				}
			}
			
		} catch (SQLException se) {
			Rlog.error("StudyStatusDao.getCurrentStudyStatus method",
					se.getMessage());
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
				Rlog.error(
						"StudyStatusDao.getCurrentStudyStatus method",
						e.getMessage());
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				Rlog.error(
						"StudyStatusDao.getCurrentStudyStatus method",
						e.getMessage());
			}
		}
		return curStat;	
	} 
	
	 	 public int getPkstudystat(int studyId,int fksite) {

	        PreparedStatement pstmt = null;
	        Connection conn = null;
	        int pkstudystat = 0;
	        try {
	            conn = getConnection();
	            String sql = "";

	            sql = "select PK_STUDYSTAT "
	                    + "	from er_studystat" + " where fk_study=? "
	                    + " and fk_site=? "
	                    + " and STUDYSTAT_HSPN = 1 ";
	                    
	            pstmt = conn.prepareStatement(sql);
	            pstmt.setInt(1, studyId);
	            pstmt.setInt(2, fksite);

	            ResultSet rs = pstmt.executeQuery();
	            if(rs.next()) {
	            	pkstudystat = rs.getInt("pk_studystat");
	            }

	        } catch (SQLException ex) {
	            Rlog.fatal("studystatus",
	                    "studystatusDao.getPkstudystat EXCEPTION IN FETCHING FROM er_studystat"+ ex);

	        }finally
	        {
	            try
	            {
	                if (pstmt!=null) pstmt.close();
	             }catch(Exception e)
	             {
	                 e.printStackTrace();
	             }
	             
	             try
	             {
	                 if (conn!=null) conn.close();
	              }catch(Exception e)
	              {
	                  e.printStackTrace();
	              }
	          }
	        return pkstudystat;
	    }
	 	 
	 	 public int getStudyActiveCount(int studyId) {

		        Rlog.debug("studystatus",
		                "studystatusDao.getStudyActiveCount studyId##" + studyId);

		        PreparedStatement pstmt = null;
		        Connection conn = null;
		        int count = 0;
		        try {
		            conn = getConnection();
		            String sql = "";

		            sql = "select count(*)"
		                    + "	from er_studystat, er_codelst" + " where fk_study=? And pk_codelst=fk_codelst_studystat "
		                    + " and codelst_type='studystat' and codelst_subtyp='active' ";
		                    
		                    

		            pstmt = conn.prepareStatement(sql);
		            pstmt.setInt(1, studyId);
		            

		            ResultSet rs = pstmt.executeQuery();
		            rs.next() ;
		            	count = rs.getInt(1);
		            

		        } catch (SQLException ex) {
		            Rlog.fatal("studystatus",
		                    "studystatusDao.getStudyActiveCount EXCEPTION IN FETCHING FROM er_studystat"+ ex);

		        }finally
		        {
		            try
		            {
		                if (pstmt!=null) pstmt.close();
		             }catch(Exception e)
		             {
		                 e.printStackTrace();
		             }
		             
		             try
		             {
		                 if (conn!=null) conn.close();
		              }catch(Exception e)
		              {
		                  e.printStackTrace();
		              }
		          }
		        return count;
		    }
	 	
	 	 public void updatestudyhspn(int pkstudystat,int user) {

	         Rlog.debug("studystatus", "in DAO: studystatus");
	         PreparedStatement pstmt = null;
	         Connection conn = null;
	         try {
	             conn = getConnection();
	             Rlog.debug("studystatus", "got connection" + conn);
	             StringBuffer sql = new StringBuffer(); 
	             sql.append("UPDATE ER_STUDYSTAT ");
	             sql.append("SET STUDYSTAT_HSPN = '0' , LAST_MODIFIED_BY = ?  WHERE PK_STUDYSTAT = ? ");
	               
	             pstmt = conn.prepareStatement(sql.toString());
	             pstmt.setInt(1, user);
	             pstmt.setInt(2, pkstudystat);
	             pstmt.executeUpdate();
	            } catch (SQLException ex) {
	             Rlog.fatal("studystatus",
	                     "studystatusDao.updatestudyhspn EXCEPTION IN updating  er_studystat"+ ex);
	         } finally {
	             try {
	                 if (pstmt != null)
	                     pstmt.close();
	             } catch (Exception e) {
	             }
	             try {
	                 if (conn != null)
	                     conn.close();
	             } catch (Exception e) {
	             }
	         }
	     }

    //end of class
}