/*
 * Classname : StatusHistoryBean
 * 
 * Version information: 1.0
 *
 * Date: 08/09/2004
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.business.statusHistory.impl;

/* Import Statements */
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The StatusHistory CMP entity bean.<br>
 * <br>
 * 
 * @author Anu
 * @vesion 1.0 08/09/2001
 * @ejbHome StatusHistoryHome
 * @ejbRemote StatusHistoryRObj
 */

@Entity
@Table(name = "er_status_history")
@NamedQueries( {
	    @NamedQuery(name = "findByPreviousIsCurrentFlag", query = " SELECT OBJECT(stat) FROM StatusHistoryBean stat where stat.statusModuleId = :STATUS_MODPK and  lower(stat.statusModuleTable) = lower(:STATUS_MODTABLE) and stat.isCurrentStat = 1  "),
	    @NamedQuery(name = "findByPreviousIsCurrentFlagByModParent", query = " SELECT OBJECT(stat) FROM StatusHistoryBean stat where stat.statusModuleId = :STATUS_MODPK and  lower(stat.statusModuleTable) = lower(:STATUS_MODTABLE) and stat.isCurrentStat = 1  "),
	    @NamedQuery(name = "findByPreviousCurrentStatusInHistory", query = " SELECT OBJECT(stat) FROM StatusHistoryBean stat where stat.statusModuleId = :STATUS_MODPK and  lower(stat.statusModuleTable) = lower(:STATUS_MODTABLE) and  stat.statusEndDate  is null and trunc(stat.statusStartDate) <= to_date(:begindate,:date_format) and stat.statusId <> :pk_status "),
	    @NamedQuery(name = "findByPreviousCurrentStatusByModParentInHistory", query = " SELECT OBJECT(stat) FROM StatusHistoryBean stat where stat.statusModuleId = :STATUS_MODPK and  lower(stat.statusModuleTable) = lower(:STATUS_MODTABLE) and  stat.statusEndDate  is null and trunc(stat.statusStartDate) <= to_date(:begindate,:date_format) and stat.statusId <> :pk_status "),
	    @NamedQuery(name = "findByNextStatusInHistory", query = "SELECT OBJECT(stat) FROM StatusHistoryBean stat  Where stat.statusId = ( select max(i.statusId) from StatusHistoryBean i where               i.statusModuleId = :STATUS_MODPK and lower(i.statusModuleTable) = lower(:STATUS_MODTABLE) and        trunc(i.statusStartDate) = ( select min(trunc(ii.statusStartDate)) from StatusHistoryBean ii           where ii.statusModuleId = :STATUS_MODPK and lower(ii.statusModuleTable) = lower(:STATUS_MODTABLE) and           trunc(ii.statusStartDate) >= to_date(:begindate,:date_format) and ii.statusId <> :pk_status ) and         i.statusId <> :pk_status ) "),
	    @NamedQuery(name = "findByNextStatusByModParentInHistory", query = "SELECT OBJECT(stat) FROM StatusHistoryBean stat  Where stat.statusId = ( select max(i.statusId) from StatusHistoryBean i where               i.statusModuleId = :STATUS_MODPK and lower(i.statusModuleTable) = lower(:STATUS_MODTABLE) and        trunc(i.statusStartDate) = ( select min(trunc(ii.statusStartDate)) from StatusHistoryBean ii           where ii.statusModuleId = :STATUS_MODPK and lower(ii.statusModuleTable) = lower(:STATUS_MODTABLE) and trunc(ii.statusStartDate) >= to_date(:begindate,:date_format) and ii.statusId <> :pk_status ) and         i.statusId <> :pk_status ) "),
	    @NamedQuery(name = "findByPreviousStatusInHistory", query = "SELECT OBJECT(stat) FROM StatusHistoryBean stat Where stat.statusId = ( select max(i.statusId) from StatusHistoryBean i where        i.statusModuleId = :STATUS_MODPK and lower(i.statusModuleTable) = lower(:STATUS_MODTABLE) and       trunc(i.statusStartDate) = ( select max(trunc(ii.statusStartDate)) from StatusHistoryBean ii        where ii.statusModuleId = :STATUS_MODPK and lower(ii.statusModuleTable) = lower(:STATUS_MODTABLE) and       trunc(ii.statusStartDate) <= to_date(:begindate,:date_format) and ii.statusId <> :pk_status ) and       i.statusId <> :pk_status ) "),
        @NamedQuery(name = "findByPreviousStatusByModParentInHistory", query = "SELECT OBJECT(stat) FROM StatusHistoryBean stat Where stat.statusId = ( select max(i.statusId) from StatusHistoryBean i where        i.statusModuleId = :STATUS_MODPK and lower(i.statusModuleTable) = lower(:STATUS_MODTABLE) and  trunc(i.statusStartDate) = ( select max(trunc(ii.statusStartDate)) from StatusHistoryBean ii        where ii.statusModuleId = :STATUS_MODPK and lower(ii.statusModuleTable) = lower(:STATUS_MODTABLE) and trunc(ii.statusStartDate) <= to_date(:begindate,:date_format) and ii.statusId <> :pk_status ) and  i.statusId <> :pk_status ) "),
        @NamedQuery(name = "findByLatestStatusInHistory", query = "SELECT OBJECT(stat) FROM StatusHistoryBean stat  Where stat.statusId = ( select max(i.statusId) from StatusHistoryBean i where   i.statusModuleId = :STATUS_MODPK and lower(i.statusModuleTable) = lower(:STATUS_MODTABLE) and   trunc(i.statusStartDate) = ( select max(trunc(ii.statusStartDate)) from StatusHistoryBean ii    where ii.statusModuleId = :STATUS_MODPK and lower(ii.statusModuleTable) = lower( :STATUS_MODTABLE) ) )"),
        @NamedQuery(name = "findByLatestStatusByModParentInHistory", query = "SELECT OBJECT(stat) FROM StatusHistoryBean stat  Where stat.statusId = ( select max(i.statusId) from StatusHistoryBean i where   i.statusModuleId = :STATUS_MODPK and lower(i.statusModuleTable) = lower(:STATUS_MODTABLE) and   trunc(i.statusStartDate) = ( select max(trunc(ii.statusStartDate)) from StatusHistoryBean ii    where ii.statusModuleId = :STATUS_MODPK and lower(ii.statusModuleTable) = lower( :STATUS_MODTABLE) ) )"),
        @NamedQuery(name = "findByAtLeastOneStatusInHistory", query = "SELECT OBJECT(stat) FROM StatusHistoryBean stat  where stat.statusModuleId = :STATUS_MODPK and lower(stat.statusModuleTable) = lower(:STATUS_MODTABLE) and   stat.statusId <> :pk_status and rownum < 2 order by stat.statusId desc") ,
        @NamedQuery(name = "findByAtLeastOneStatusByModParentInHistory", query = "SELECT OBJECT(stat) FROM StatusHistoryBean stat  where stat.statusModuleId = :STATUS_MODPK and lower(stat.statusModuleTable) = lower(:STATUS_MODTABLE) and   stat.statusId <> :pk_status and rownum < 2 order by stat.statusId desc") ,
        @NamedQuery(name = "findByLatestStatusInHistoryForUpdate", query = "SELECT OBJECT(stat) FROM StatusHistoryBean stat Where stat.statusId = ( select max(i.statusId) from StatusHistoryBean i where   i.statusModuleId = :STATUS_MODPK and lower(i.statusModuleTable) = lower(:STATUS_MODTABLE))")})
public class StatusHistoryBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3973145368082059958L;

    /**
     * the Status Id
     */
    public int statusId;

    /**
     * the status Module Id
     */
    public Integer statusModuleId;

    /**
     * the status Module Table
     */

    public String statusModuleTable;

    /**
     * the status Codelst Id
     */

    public Integer statusCodelstId;

    /**
     * the status Start Date
     */
    public java.util.Date statusStartDate;

    /**
     * the status End Date
     */
    public java.util.Date statusEndDate;

    /**
     * the status history Notes
     */

    public String statusNotes;

    /*
     * For future use
     */
    public String statusCustom1;

    /**
     * the status Entered by
     */
    public Integer statusEnteredBy;

    /*
     * creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * IP Address
     */
    public String ipAdd;
    
    /*
     * Record Type
     */
    public String recordType;
    
    /*
     * Current Status
     */
    public String isCurrentStat;
    /*
     * Status Module Pk
     */

    // GETTER SETTER METHODS

    /**
     * 
     * Get status Id
     * 
     * @return the Id of the status
     */

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_STATUS_HISTORY", allocationSize=1)
    @Column(name = "PK_STATUS")
    public int getStatusId() {
        return this.statusId;
    }

    public void setStatusId(int id) {
        this.statusId = id;
    }

    /**
     * Get Status Module ID
     * 
     * @return String module Id
     */
    @Column(name = "STATUS_MODPK")
    public String getStatusModuleId() {
        return StringUtil.integerToString(this.statusModuleId);
    }

    /**
     * sets Status Module Id
     * 
     * @param statusModuleId
     */
    public void setStatusModuleId(String statusModuleId) {
        this.statusModuleId = StringUtil.stringToInteger(statusModuleId);
    }

    /**
     * Get Status Module Table
     * 
     * @return String Module Table
     */
    @Column(name = "STATUS_MODTABLE")
    public String getStatusModuleTable() {
        return this.statusModuleTable;
    }

    /**
     * sets Status Module Table
     * 
     * @param statusModuleTable
     */
    public void setStatusModuleTable(String statusModuleTable) {
        this.statusModuleTable = statusModuleTable;
    }

    /**
     * Get Status Codelst ID
     * 
     * @return String codelst Id
     */
    @Column(name = "FK_CODELST_STAT")
    public String getStatusCodelstId() {
        return StringUtil.integerToString(this.statusCodelstId);
    }

    /**
     * sets Status Codelst Id
     * 
     * @param statusCodelstId
     */

    public void setStatusCodelstId(String statusCodelstId) {
        this.statusCodelstId = StringUtil.stringToInteger(statusCodelstId);

    }

    /**
     * Get Status Start Date
     * 
     * @return String Status Start date
     */

    public String returnStatusStartDate() {
        return DateUtil.dateToString(getStatusStartDate(),null); //Modified by Aakriti for Bug#30837 - Network Status History and Network User Role Status History should include timestamp..

    }

    /**
     * sets Status Start Date
     * 
     * @param statusStartDate
     */

    public void updateStatusStartDate(String statusStartDate) {
        setStatusStartDate(DateUtil.stringToTimeStamp(statusStartDate, null));  //Modified by Aakriti for Bug#30837 - Network Status History and Network User Role Status History should include timestamp..

    }

    @Column(name = "STATUS_DATE")
    public Date getStatusStartDate() {
        return this.statusStartDate;

    }

    public void setStatusStartDate(Date statusStartDate) {
        this.statusStartDate = statusStartDate;

    }

    /**
     * Get Status End Date
     * 
     * @return String Status end date
     */

    public String returnStatusEndDate() {
        return DateUtil.dateToString(getStatusEndDate(),null);    //Modified by Aakriti for Bug#30837 - Network Status History and Network User Role Status History should include timestamp..

    }

    /**
     * sets Status End Date
     * 
     * @param statusEndDate
     */

    public void updateStatusEndDate(String statusEndDate) {
        setStatusEndDate(DateUtil.stringToTimeStamp(statusEndDate,
        		null));                                             //Modified by Aakriti for Bug#30837 - Network Status History and Network User Role Status History should include timestamp..
    }

    @Column(name = "STATUS_END_DATE")
    public Date getStatusEndDate() {
        return this.statusEndDate;

    }

    public void setStatusEndDate(Date statusEndDate) {
        this.statusEndDate = statusEndDate;
    }

    /**
     * Get Status Notes
     * 
     * @return String status notes
     */
    @Column(name = "STATUS_NOTES")
    public String getStatusNotes() {
        return this.statusNotes;

    }

    /**
     * sets Status Notes
     * 
     * @param statusNotes
     */
    public void setStatusNotes(String statusNotes) {
        this.statusNotes = statusNotes;
    }

    /*
     * For Future use
     */
    @Column(name = "STATUS_CUSTOM1")
    public String getStatusCustom1() {
        return this.statusCustom1;
    }

    /*
     * For Future use
     */

    public void setStatusCustom1(String statusCustom1) {
        this.statusCustom1 = statusCustom1;
    }

    /**
     * Get Status Entered by
     * 
     * @return String status entered by
     */
    @Column(name = "STATUS_ENTERED_BY")
    public String getStatusEnteredBy() {
        return StringUtil.integerToString(this.statusEnteredBy);
    }

    /**
     * sets Status Entered by
     * 
     * @param statusEnteredBy
     */
    public void setStatusEnteredBy(String statusEnteredBy) {
        this.statusEnteredBy = StringUtil.stringToInteger(statusEnteredBy);
    }

    /**
     * Get Status creator
     * 
     * @return The Id of the creator of the record
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {

        this.creator = StringUtil.stringToInteger(creator);
    }

    /**
     * Get Status modified by
     * 
     * @return The Id of the user modifying this record
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = StringUtil.stringToInteger(modifiedBy);
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

   /**
    * @return record Type
    */
    
    @Column(name = "RECORD_TYPE")
    public String getRecordType() {
        return this.recordType;
    }

    /**
     * @param recordType
     *           The record type 
     */
    
    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }
    
    //Added by Manimaran for the July-August Enhancement S4.
    
    /**
     * @return current Status
     */
     
     @Column(name = "STATUS_ISCURRENT")
     public String getIsCurrentStat() {
         return this.isCurrentStat;
     }

     /**
      * @param isCurrentStat
      *           The isCurrentStat 
      */
     
     public void setIsCurrentStat(String isCurrentStat) {
         this.isCurrentStat = isCurrentStat;
     }
     
    // END OF GETTER SETTER METHODS

    /*
     * 
     * public StatusHistoryStateKeeper getStatusHistoryStateKeeper() {
     * 
     * return new StatusHistoryStateKeeper(getStatusId(), getStatusModuleId(),
     * getStatusModuleTable(), getStatusCodelstId(), getStatusStartDate(),
     * getStatusEndDate(), getStatusNotes(), getStatusCustom1(),
     * getStatusEnteredBy(), getCreator(), getModifiedBy(), getIpAdd()); }
     * 
     * 
     * public void setStatusHistoryStateKeeper(StatusHistoryStateKeeper hsk) {
     * GenerateId statId = null;
     * 
     * try { Connection conn = null; conn = getConnection();
     * 
     * statusId = statId.getId("SEQ_ER_STATUS_HISTORY", conn); conn.close();
     * setStatusModuleId(hsk.getStatusModuleId());
     * setStatusModuleTable(hsk.getStatusModuleTable());
     * setStatusCodelstId(hsk.getStatusCodelstId());
     * setStatusStartDate(hsk.getStatusStartDate());
     * setStatusEndDate(hsk.getStatusEndDate());
     * setStatusNotes(hsk.getStatusNotes());
     * setStatusCustom1(hsk.getStatusCustom1());
     * setStatusEnteredBy(hsk.getStatusEnteredBy());
     * setCreator(hsk.getCreator()); setModifiedBy(hsk.getModifiedBy());
     * setIpAdd(hsk.getIpAdd()); } catch (Exception e) {
     * Rlog.fatal("statusHistory", "Exception in
     * StatusHistoryBean.setStatusHistoryStateKeeper" + e); } }
     */

    /**
     * updates the Status History details
     * 
     * @param usk
     *            StatusHistoryStateKeeper
     * @return 0 if successful; -2 for exception
     */
    public int updateStatusHistory(StatusHistoryBean hsk) {
        try {

            setStatusModuleId(hsk.getStatusModuleId());
            setStatusModuleTable(hsk.getStatusModuleTable());
            setStatusCodelstId(hsk.getStatusCodelstId());
            updateStatusStartDate(hsk.returnStatusStartDate());
            updateStatusEndDate(hsk.returnStatusEndDate());
            setStatusNotes(hsk.getStatusNotes());
            setStatusCustom1(hsk.getStatusCustom1());
            setStatusEnteredBy(hsk.getStatusEnteredBy());
            setCreator(hsk.getCreator());
            setModifiedBy(hsk.getModifiedBy());
            setIpAdd(hsk.getIpAdd());
            setRecordType(hsk.getRecordType());
            setIsCurrentStat(hsk.getIsCurrentStat());
            return 0;
        } catch (Exception e) {
            Rlog.fatal("statusHistory",
                    "Exception in  StatusHistoryBean.updateStatusHistory" + e);
            return -2;
        }
    }


    //Added by Manimaran for the July-August Enhancement S4.
    public int updateStatusHistoryWithEndDate(StatusHistoryBean hsk) {
        try {
        	setStatusModuleId(hsk.getStatusModuleId());
            setStatusModuleTable(hsk.getStatusModuleTable());
            setStatusCodelstId(hsk.getStatusCodelstId());
            updateStatusStartDate(hsk.returnStatusStartDate());  //Modified by Aakriti for Bug#30837 - Network Status History and Network User Role Status History should include timestamp..
            updateStatusEndDate(hsk.returnStatusEndDate());      //Modified by Aakriti for Bug#30837 - Network Status History and Network User Role Status History should include timestamp..
            setStatusNotes(hsk.getStatusNotes());
            setStatusCustom1(hsk.getStatusCustom1());
            setStatusEnteredBy(hsk.getStatusEnteredBy());
            setCreator(hsk.getCreator());
            setModifiedBy(hsk.getModifiedBy());
            setIpAdd(hsk.getIpAdd());
            setRecordType(hsk.getRecordType());
            setIsCurrentStat(hsk.getIsCurrentStat());
            return 0;
        } catch (Exception e) {
            Rlog.fatal("statusHistory",
                    "Exception in  StatusHistoryBean.updateStatusHistory" + e);
            return -2;
        }
    }
   
    
    /////////////////
    public StatusHistoryBean() {

    }

    public StatusHistoryBean(int statusId, String statusModuleId,
            String statusModuleTable, String statusCodelstId,
            String statusStartDate, String statusEndDate, String statusNotes,
            String statusCustom1, String statusEnteredBy, String creator,
            String modifiedBy, String ipAdd,String recordType, String isCurrentStat) {
        // TODO Auto-generated constructor stub
        setStatusId(statusId);
        setStatusModuleId(statusModuleId);
        setStatusModuleTable(statusModuleTable);
        setStatusCodelstId(statusCodelstId);
        updateStatusStartDate(statusStartDate);
        updateStatusEndDate(statusEndDate);  // 2
        setStatusNotes(statusNotes);
        setStatusCustom1(statusCustom1);
        setStatusEnteredBy(statusEnteredBy);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setRecordType(recordType);
        setIsCurrentStat(isCurrentStat); 
    }

}// end of class
