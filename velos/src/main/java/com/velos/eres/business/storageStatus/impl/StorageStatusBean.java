/*
 * Classname			StorageStatusBean.class
 * 
 * Version information
 *
 * Date					08/14/2007 
 * 
 * Copyright notice
 */

package com.velos.eres.business.storageStatus.impl;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import javax.persistence.Transient;

/**
 * The Storage BMP entity bean.<br>
 * <br>
 * 
 * @author Manimaran
 * @version 1.0 08/14/2007
 */
@Entity
@Table(name = "er_storage_status")

public class StorageStatusBean implements Serializable {
   
    private static final long serialVersionUID = 3761410811205333553L;

    /**
     * PkStorageStat
     */
    public int pkStorageStat;

    /**
     * fkStorage
     */
    public Integer fkStorage;
    
    /**
     * ssStartDate
     */
    public Date ssStartDate;
    
    /**
     * ssEndDate
     */
    //public Date ssEndDate;//JM: 29Sep2007: SS_END_DATE removed 

    /**
     * fkCodelstStorageStat
     */
    public Integer fkCodelstStorageStat;

    /**
     * ssNotes
     */
    public String ssNotes;

    /**
     * fkUser
     */
    public Integer fkUser;
    
    
    /**
     * ssTrackingNum
     */
    public String ssTrackingNum;
    
    /**
     * fkStudy
     */
    public Integer fkStudy;
    
    /**
     * the record creator
     */
    public Integer creator;

    /**
     * last modified by
     */
    public Integer modifiedBy;

    /**
     * the IP Address
     */
    public String ipAdd;

        
    // GETTER SETTER METHODS

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "seq_er_storage_stat", allocationSize=1)
    
    @Column(name = "PK_STORAGE_STATUS")
    public int getPkStorageStat() {
        return this.pkStorageStat;
    }

    public void setPkStorageStat(int pkStorageStat) {
        this.pkStorageStat = pkStorageStat;
    }

    @Column(name = "FK_STORAGE")
    public String getFkStorage() {
        return StringUtil.integerToString(this.fkStorage);
    }

    public void setFkStorage(String fkStorage) {
        this.fkStorage = StringUtil.stringToInteger(fkStorage);
    }
  
    @Column(name = "SS_START_DATE")
    public Date getSsStartDt() {
        return this.ssStartDate;
    }

    @Transient
	public String getSsStartDate(){
		return DateUtil.dateToString(getSsStartDt());
	}
    
    public void setSsStartDt(Date ssStartDate) {
        this.ssStartDate = ssStartDate;
    }
    
    public void setSsStartDate(String ssStartDate) {
    	setSsStartDt(DateUtil.stringToDate(ssStartDate, null));
    }
    
  
    
    
    @Column(name = "FK_CODELST_STORAGE_STATUS")
    public String getFkCodelstStorageStat() {
    	return StringUtil.integerToString(this.fkCodelstStorageStat);
    }

    
    public void setFkCodelstStorageStat(String fkCodelstStorageStat) {
        this.fkCodelstStorageStat = StringUtil.stringToInteger(fkCodelstStorageStat);
    }
    
    
    
    @Transient
    public String getSsNotes() {
        return this.ssNotes;
    }

    public void setSsNotes(String ssNotes) {
        this.ssNotes = ssNotes;
    }
    
    @Column(name = "FK_USER")
    public String getFkUser() {
    	return StringUtil.integerToString(this.fkUser);
    }

    
    public void setFkUser(String fkUser) {
        this.fkUser = StringUtil.stringToInteger(fkUser);
    }
    
    @Column(name = "SS_TRACKING_NUMBER")
    public String getSsTrackingNum() {
        return this.ssTrackingNum;
    }

    public void setSsTrackingNum(String ssTrackingNum) {
        this.ssTrackingNum = ssTrackingNum;
    }
    
    @Column(name = "FK_STUDY")
    public String getFkStudy() {
    	return StringUtil.integerToString(this.fkStudy);
    }

    
    public void setFkStudy(String fkStudy) {
        this.fkStudy = StringUtil.stringToInteger(fkStudy);
    }
    
           
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    
    // END OF GETTER SETTER METHODS

    public int updateStorageStatus(StorageStatusBean ssk) {
        try {
        	
            
        	setPkStorageStat(ssk.getPkStorageStat());
        	setFkStorage(ssk.getFkStorage());
        	setSsStartDate(ssk.getSsStartDate());
        	//setSsEndDate(ssk.getSsEndDate());
        	setFkCodelstStorageStat(ssk.getFkCodelstStorageStat());
        	setSsNotes(ssk.getSsNotes());
        	setFkUser(ssk.getFkUser());
        	setSsTrackingNum(ssk.getSsTrackingNum());
        	setFkStudy(ssk.getFkStudy());
    		setCreator(ssk.getCreator());
            setModifiedBy(ssk.getModifiedBy());
            setIpAdd(ssk.getIpAdd());
            Rlog.debug("storageStatus", "StorageStatusBean.updateStorageStatus");
            return 0;
        } catch (Exception e) {
        	
            Rlog.fatal("storageStatus", " error in StorageStatusBean.updateStorageStatus");
            return -2;
        }
    }

    public StorageStatusBean() {

    }

    public StorageStatusBean(int pkStorageStat, String fkStorage, String ssStartDate, String fkCodelstStorageStat, String ssNotes,
    		String fkUser, String ssTrackingNum, String fkStudy, String creator, String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setPkStorageStat(pkStorageStat);
		setFkStorage(fkStorage);
		setSsStartDate(ssStartDate);
		//setSsEndDate(ssEndDate);
		setFkCodelstStorageStat(fkCodelstStorageStat);
		setSsNotes(ssNotes);
		setFkUser(fkUser);
		setSsTrackingNum(ssTrackingNum);
		setFkStudy(fkStudy);
		setCreator(creator);
		setModifiedBy(modifiedBy);
		setIpAdd(ipAdd);
    }

}


