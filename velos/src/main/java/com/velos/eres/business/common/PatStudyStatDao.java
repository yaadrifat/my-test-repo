/*
 * Classname : PatStudyStatDao
 * 
 * Version information: 1.0
 *
 * Date: 06/18/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sajal
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The PatStudyStat Dao.<br>
 * <br>
 * This class has various methods to get resultsets of Patient Study data for
 * different parameters.
 * 
 * @author Sajal
 * @vesion 1.0 06/18/2001
 */

/*
 * Modified by Sonia, 09/13/04, to include patient study id with enrolled
 * patient results, added new attribute to hold the same
 */

public class PatStudyStatDao extends CommonDAO implements java.io.Serializable {

    private ArrayList patientIds;

    private ArrayList patientEnrollDates;

    private ArrayList patientCodes;

    private ArrayList patientLnames;

    private ArrayList patientFNames;

    private ArrayList patientStats;

    private ArrayList patientProtIds;

    private ArrayList patientStudyIds;

    private ArrayList patientAddress;

    private ArrayList patientPhone;

    private ArrayList patientRace;

    private ArrayList patientEthnicity;

    private ArrayList patientNotes;

    // by sonia

    private ArrayList studyIds;

    private ArrayList studyNums;

    private ArrayList studyVersions;

    private ArrayList patientStatusDescs;

    private ArrayList patientStatusSubTypes;

    private ArrayList patientStatusPk;

    private ArrayList studyTitles;

    // /////////////////////
    private ArrayList patientStatusStartDate;

    private ArrayList patientStatusEndDate;

    private ArrayList patientStatusNotes;

    private ArrayList patientStudyStatusReasons;

    // / for visits

    private ArrayList curVisits;

    private ArrayList curVisitsName;

    private ArrayList curVisitDates;

    private ArrayList nextVisits;

    private ArrayList siteIds;

    private ArrayList siteNames;

    private ArrayList <String> patFacilities;
    
    //  Added by Manimaran for the September Enhancement S8.
    private ArrayList currentStats;
    
    private int cRows;

    public PatStudyStatDao() {
    	patFacilities = new ArrayList<String> ();
        patientIds = new ArrayList();
        patientStudyIds = new ArrayList();
        patientEnrollDates = new ArrayList();
        patientCodes = new ArrayList();
        patientLnames = new ArrayList();
        patientFNames = new ArrayList();
        patientStats = new ArrayList();
        patientProtIds = new ArrayList();
        patientAddress = new ArrayList();
        patientRace = new ArrayList();
        patientEthnicity = new ArrayList();
        patientNotes = new ArrayList();
        patientPhone = new ArrayList();

        studyIds = new ArrayList();
        studyNums = new ArrayList();
        studyVersions = new ArrayList();
        patientStatusDescs = new ArrayList();
        patientStatusSubTypes = new ArrayList();
        patientStatusPk = new ArrayList();

        patientStatusStartDate = new ArrayList();
        patientStatusEndDate = new ArrayList();
        patientStatusNotes = new ArrayList();

        curVisits = new ArrayList();
        curVisitsName = new ArrayList();
        curVisitDates = new ArrayList();
        nextVisits = new ArrayList();

        studyTitles = new ArrayList();
        patientStudyStatusReasons = new ArrayList();

        siteIds = new ArrayList();
        siteNames = new ArrayList();
        currentStats = new ArrayList();//KM
    }

    // Getter and Setter methods

    public ArrayList getCurVisits() {
        return this.curVisits;
    }

    public void setCurVisits(ArrayList curVisits) {
        this.curVisits = curVisits;
    }

    public ArrayList getCurVisitDates() {
        return this.curVisitDates;
    }

    public void setCurVisitDates(ArrayList curVisitDates) {
        this.curVisitDates = curVisitDates;
    }

    public ArrayList getNextVisits() {
        return this.nextVisits;
    }

    public void setNextVisits(ArrayList nextVisits) {
        this.nextVisits = nextVisits;
    }

    public ArrayList getPatientIds() {
        return this.patientIds;
    }

    public void setPatientIds(ArrayList patientIds) {
        this.patientIds = patientIds;
    }

    public ArrayList getPatientEnrollDates() {
        return this.patientEnrollDates;
    }

    public void setPatientEnrollDates(ArrayList patientEnrollDates) {
        this.patientEnrollDates = patientEnrollDates;
    }

    public ArrayList getPatientCodes() {
        return this.patientCodes;
    }

    public void setPatientCodes(ArrayList patientCodes) {
        this.patientCodes = patientCodes;
    }

    public ArrayList getPatientLnames() {
        return this.patientLnames;
    }

    public void setPatientLnames(ArrayList patientLnames) {
        this.patientLnames = patientLnames;
    }

    public ArrayList getPatientFNames() {
        return this.patientFNames;
    }

    public void setPatientFNames(ArrayList patientFNames) {
        this.patientFNames = patientFNames;
    }

    public ArrayList getPatientStats() {
        return this.patientStats;
    }

    public void setPatientStats(ArrayList patientStats) {
        this.patientStats = patientStats;
    }

    public ArrayList getPatientProtIds() {
        return this.patientProtIds;
    }

    public void setPatientProtIds(ArrayList patientProtIds) {
        this.patientProtIds = patientProtIds;
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public ArrayList getSiteIds() {
        return this.siteIds;
    }

    public void setSiteIds(ArrayList siteIds) {
        this.siteIds = siteIds;
    }

    public void setSiteIds(String siteId) {
        siteIds.add(siteId);
    }

    public ArrayList getSiteNames() {
        return this.siteNames;
    }

    public void setSiteNames(ArrayList siteNames) {
        this.siteNames = siteNames;
    }

    public void setSiteNames(String siteName) {
        siteNames.add(siteName);
    }

    public void setPatientIds(Integer patientId) {
        patientIds.add(patientId);
    }

    public void setPatientEnrollDates(String patientEnrollDate) {
        patientEnrollDates.add(patientEnrollDate);
    }

    public void setPatientCodes(String patientCode) {
        patientCodes.add(patientCode);
    }

    public void setPatientLnames(String patientLname) {
        patientLnames.add(patientLname);
    }

    public void setPatientFNames(String patientFName) {
        patientFNames.add(patientFName);
    }

    public void setPatientStats(String patientStat) {
        patientStats.add(patientStat);
    }

    public void setPatientProtIds(String patientProtId) {
        patientProtIds.add(patientProtId);
    }

    /**
     * Returns the value of patientAddress.
     */
    public ArrayList getPatientAddress() {
        return patientAddress;
    }

    /**
     * Sets the value of patientAddress.
     * 
     * @param patientAddress
     *            The value to assign patientAddress.
     */
    public void setPatientAddress(ArrayList patientAddress) {
        this.patientAddress = patientAddress;
    }

    /**
     * Sets the value of patientAddress.
     * 
     * @param patientAddress
     *            The value to assign patientAddress.
     */
    public void setPatientAddress(String patAddress) {
        this.patientAddress.add(patAddress);
    }

    /**
     * Returns the value of patientRace.
     */
    public ArrayList getPatientRace() {
        return patientRace;
    }

    /**
     * Sets the value of patientRace.
     * 
     * @param patientRace
     *            The value to assign patientRace.
     */
    public void setPatientRace(ArrayList patientRace) {
        this.patientRace = patientRace;
    }

    /**
     * Sets the value of patientRace.
     * 
     * @param patientRace
     *            The value to assign patientRace.
     */
    public void setPatientRace(String patRace) {
        this.patientRace.add(patRace);
    }

    /**
     * Returns the value of patientEthnicity.
     */
    public ArrayList getPatientEthnicity() {
        return patientEthnicity;
    }

    /**
     * Sets the value of patientEthnicity.
     * 
     * @param patientEthnicity
     *            The value to assign patientEthnicity.
     */
    public void setPatientEthnicity(ArrayList patientEthnicity) {
        this.patientEthnicity = patientEthnicity;
    }

    /**
     * Sets the value of patientEthnicity.
     * 
     * @param patientEthnicity
     *            The value to assign patientEthnicity.
     */
    public void setPatientEthnicity(String patEthnicity) {
        this.patientEthnicity.add(patEthnicity);
    }

    /**
     * Returns the value of patientNotes.
     */
    public ArrayList getPatientNotes() {
        return patientNotes;
    }

    /**
     * Sets the value of patientNotes.
     * 
     * @param patientNotes
     *            The value to assign patientNotes.
     */
    public void setPatientNotes(ArrayList patientNotes) {
        this.patientNotes = patientNotes;
    }

    /**
     * Sets the value of patientNotes.
     * 
     * @param patientNotes
     *            The value to assign patientNotes.
     */
    public void setPatientNotes(String patientNote) {
        this.patientNotes.add(patientNote);
    }

    /**
     * Returns the value of patientPhone.
     */
    public ArrayList getPatientPhone() {
        return patientPhone;
    }

    /**
     * Sets the value of patientPhone.
     * 
     * @param patientPhone
     *            The value to assign patientPhone.
     */
    public void setPatientPhone(ArrayList patientPhone) {
        this.patientPhone = patientPhone;
    }

    /**
     * Sets the value of patientPhone.
     * 
     * @param patientPhone
     *            The value to assign patientPhone.
     */
    public void setPatientPhone(String patPhone) {
        this.patientPhone.add(patPhone);
    }

    public ArrayList getStudyIds() {
        return this.studyIds;
    }

    public void setStudyIds(ArrayList studyIds) {
        this.studyIds = studyIds;
    }

    public void setStudyIds(Integer studyId) {
        studyIds.add(studyId);
    }

    public ArrayList getStudyNums() {
        return this.studyNums;
    }

    public void setStudyNums(ArrayList studyNums) {
        this.studyNums = studyNums;
    }

    public void setStudyNums(String studyNum) {
        this.studyNums.add(studyNum);
    }

    public ArrayList getStudyVersions() {
        return this.studyVersions;
    }

    public void setStudyVersions(ArrayList studyVersions) {
        this.studyVersions = studyVersions;
    }

    public void setStudyVersions(String studyVersion) {
        this.studyVersions.add(studyVersion);
    }

    public ArrayList getPatientStatusDescs() {
        return this.patientStatusDescs;
    }

    public void setPatientStatusDescs(ArrayList patientStatusDescs) {
        this.patientStatusDescs = patientStatusDescs;
    }

    public void setPatientStatusDescs(String patientStatusDesc) {
        this.patientStatusDescs.add(patientStatusDesc);
    }

    public ArrayList getPatientStatusSubTypes() {
        return this.patientStatusSubTypes;
    }

    public void setPatientStatusSubTypes(ArrayList patientStatusSubTypes) {
        this.patientStatusSubTypes = patientStatusSubTypes;
    }

    public void setPatientStatusSubTypes(String patientStatusSubType) {
        this.patientStatusSubTypes.add(patientStatusSubType);
    }

    public ArrayList getPatientStatusPk() {
        return this.patientStatusPk;
    }

    public void setPatientStatusPk(ArrayList patientStatusPk) {
        this.patientStatusPk = patientStatusPk;
    }

    public void setPatientStatusPk(Integer patientStatusPk) {
        this.patientStatusPk.add(patientStatusPk);
    }

    public ArrayList getPatientStatusNotes() {
        return this.patientStatusNotes;
    }

    public void setPatientStatusNotes(ArrayList patientStatusNotes) {
        this.patientStatusNotes = patientStatusNotes;
    }

    public void setPatientStatusNotes(String patientStatusNotes) {
        this.patientStatusNotes.add(patientStatusNotes);
    }

    public ArrayList getPatientStatusEndDate() {
        return this.patientStatusEndDate;
    }

    public void setPatientStatusEndDate(ArrayList patientStatusEndDate) {
        this.patientStatusEndDate = patientStatusEndDate;
    }

    public void setPatientStatusEndDate(String patientStatusEndDate) {
        this.patientStatusEndDate.add(patientStatusEndDate);
    }

    public ArrayList getPatientStatusStartDate() {
        return this.patientStatusStartDate;
    }

    public void setPatientStatusStartDate(ArrayList patientStatusStartDate) {
        this.patientStatusStartDate = patientStatusStartDate;
    }

    public void setPatientStatusStartDate(String patientStatusStartDate) {
        this.patientStatusStartDate.add(patientStatusStartDate);
    }

    public void setCurVisits(Integer curVisit) {
        curVisits.add(curVisit);
    }

    public void setCurVisitDates(String curVisitDate) {
        curVisitDates.add(curVisitDate);
    }

    public void setNextVisits(String nextVisit) {
        nextVisits.add(nextVisit);
    }

    /**
     * Returns the value of curVisitsName.
     */
    public ArrayList getCurVisitsName() {
        return curVisitsName;
    }

    /**
     * Sets the value of curVisitsName.
     * 
     * @param curVisitsName
     *            The value to assign curVisitsName.
     */
    public void setCurVisitsName(ArrayList curVisitsName) {
        this.curVisitsName = curVisitsName;
    }

    /**
     * Sets the value of curVisitsName.
     * 
     * @param curVisitsName
     *            The value to assign curVisitsName.
     */
    public void setCurVisitsName(String curVisitName) {
        this.curVisitsName.add(curVisitName);
    }

    public ArrayList getStudyTitles() {
        return studyTitles;
    }

    public void setStudyTitles(ArrayList studyTitles) {
        this.studyTitles = studyTitles;
    }

    public void setStudyTitles(String studyTitle) {
        this.studyTitles.add(studyTitle);
    }

    /**
     * Gets an ArrayList of patient study status change reason descriptions
     * 
     * @return ArrayList of patient study status change reason descriptions
     */
    public ArrayList getPatientStudyStatusReasons() {
        return patientStudyStatusReasons;
    }

    /**
     * Sets the value of patientStudyStatusReasons
     * 
     * @param patientStudyStatusReasons
     *            ArrayList of patient study status change reason descriptions
     */
    public void setPatientStudyStatusReasons(ArrayList patientStudyStatusReasons) {
        this.patientStudyStatusReasons = patientStudyStatusReasons;
    }

    /**
     * Appends the patientStudyStatusReason to the ArrayList
     * patientStudyStatusReasons
     * 
     * @param patientStudyStatusReasons
     *            patient study status change reason description
     */
    public void setPatientStudyStatusReasons(String patientStudyStatusReason) {
        this.patientStudyStatusReasons.add(patientStudyStatusReason);

    }

    public ArrayList getPatientStudyIds() 
    {
        return this.patientStudyIds;
    }

    public void setPatientStudyIds(ArrayList patientStudyIds) {
        this.patientStudyIds = patientStudyIds;
    }

    public void setPatientStudyIds(String patientStudyId) {
        patientStudyIds.add(patientStudyId);
    }
    
    //  Added by Manimaran for September Enhancement S8.
    public ArrayList getCurrentStats() {
    	return this.currentStats;
    }
    
    public void setCurrentStats(ArrayList currentStats) {
    	this.currentStats = currentStats;
    }
    
    public void setCurrentStats(Integer currentStat) {
        currentStats.add(currentStat);
    }

    // end of getter and setter methods

    /**
     * Gets the details of all the patients enrolled in a study
     * 
     * @param studyId
     *            Study Id
     */
    public void getStudyPatients(String studyId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select person.pk_person, "
                    + "er_patprot.patprot_enroldt, " + "person.person_code, "
                    + "person.person_lname, " + "person.person_fname, "
                    + "(select codelst_desc " + "from	er_codelst "
                    + "where pk_codelst = (select max(fk_codelst_stat) "
                    + "from	er_patstudystat " + "where fk_study = ? "
                    + "and fk_per = er_patprot.fk_per "
                    + "and PATSTUDYSTAT_DATE = (select max(PATSTUDYSTAT_DATE) "
                    + "from	er_patstudystat " + "where fk_study = ? "
                    + "and fk_per = er_patprot.fk_per ))) status, "
                    + "(select pk_patprot " + "from er_patprot "
                    + "where fk_study = ? " + "and fk_per = person.pk_person "
                    + "and patprot_stat = '1') pk_patprot "
                    + "from er_patprot, " + " person "
                    + "where er_patprot.fk_study = ? "
                    + "and	er_patprot.fk_per = person.pk_person "
                    + "and patprot_stat = '1' "
                    + "Order by patprot_enroldt desc");
            pstmt.setString(1, studyId);
            pstmt.setString(2, studyId);
            pstmt.setString(3, studyId);
            pstmt.setString(4, studyId);
            ResultSet rs = pstmt.executeQuery();

                       
            while (rs.next()) {
                setPatientIds(new Integer(rs.getInt("pk_person")));
                setPatientEnrollDates(rs.getString("patprot_enroldt"));
                setPatientCodes(rs.getString("person_code"));
                setPatientLnames(rs.getString("person_lname"));
                setPatientFNames(rs.getString("person_fname"));
                setPatientStats(rs.getString("status"));
                setPatientProtIds(rs.getString("pk_patprot"));
                rows++;
                Rlog.debug("patstudystat",
                        "PatStudyStatDao.getStudyPatients rows " + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "patstudystat",
                            "PatStudyStatDao.getStudyPatients EXCEPTION IN FETCHING FROM PatStudyStat table "
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // /////////////////////
    /** @deprecated*/
    public void getStudyPatientResults(int studyId, String patientID,
            int patStatus, int user, int userSite) {
        int rows = 0;
        int rows2 = 0;

        PreparedStatement pstmt = null;
        PreparedStatement pstmt2 = null;
        Rlog.debug("patstudystat", "PatStudyStatDao.getStudyPatients studyId "
                + studyId + " patientID " + patientID + " patStatus "
                + " patStatus " + " user " + user + " userSite " + userSite);
        Connection conn = null;

        String selectClause, whereClause1, orderBy;
        String whereClause2 = "";
        String whereClause3 = "";

        String selectClause2 = "";        
        String sqlst = "";
        Rlog.debug("patstudystat", "PatStudyStatDao.getStudyPatients Line 1");
        int sId1 = 0;
        int sId2 = 0;

        PreparedStatement pseq = null;
        int rightSeq = 0;
        String right = "";
        Rlog.debug("patstudystat", "PatStudyStatDao.getStudyPatients Line 2");        
        try {
            conn = getConnection();
            if (studyId == 0) {
                pseq = conn
                        .prepareStatement("select ctrl_seq from er_ctrltab where ctrl_key = 'study_rights' and upper(ctrl_value) = 'STUDYMPAT'");
                ResultSet rsSeq = pseq.executeQuery();
                Rlog.debug("patstudystat",
                        "PatStudyStatDao.getStudyPatients Line 3");
                while (rsSeq.next()) {
                    rightSeq = rsSeq.getInt("ctrl_seq");
                }
                Rlog.debug("patstudystat",
                        "PatStudyStatDao.getStudyPatients Line 4");
                pseq.close();
            }
            selectClause = "select erv.FK_STUDY, STUDY_NUMBER, STUDY_VER_NO, PER_CODE, PATPROT_ENROLDT,"
                    + "STATUS_ID, STATUS_DESC, FK_PER, PK_PATPROT,  PK_PATSTUDYSTAT ";                
            Rlog.debug("patstudystat",
                    "PatStudyStatDao.getStudyPatients Line 5");
            if (studyId == 0) {
                Rlog.debug("patstudystat",
                        "PatStudyStatDao.getStudyPatients Line 6");
                whereClause1 = ", t.STUDY_TEAM_RIGHTS from erv_patstudystat erv , er_studyteam t where per_site = "
                        + userSite
                        + " and "
                        + " t.fk_user = "
                        + user
                        + " and "
                        + " t.fk_study = erv.fk_study ";                     
            } else {
                Rlog.debug("patstudystat",
                        "PatStudyStatDao.getStudyPatients Line 7");
                whereClause1 = "from erv_patstudystat erv where per_site = "
                        + userSite + " and   erv.FK_STUDY = " + studyId;                
            }
            if (patStatus > 0) {
                Rlog.debug("patstudystat",
                        "PatStudyStatDao.getStudyPatients Line 8");
                whereClause2 = " and STATUS_ID = " + patStatus;
            } else if (patStatus < 0) {
                Rlog.debug("patstudystat",
                        "PatStudyStatDao.getStudyPatients Line 9");
                pseq = conn
                        .prepareStatement("select pk_codelst from er_codelst where CODELST_TYPE = 'patStatus' and (ltrim(rtrim(lower(CODELST_SUBTYP))) = 'active' or ltrim(rtrim(lower(CODELST_SUBTYP))) = 'reactivate')");
                ResultSet rsSeq3 = pseq.executeQuery();
                while (rsSeq3.next()) {
                    Rlog.debug("patstudystat",
                            "PatStudyStatDao.getStudyPatients Line 10");
                    if (sId1 == 0) {
                        sId1 = rsSeq3.getInt("pk_codelst");
                    } else {
                        sId2 = rsSeq3.getInt("pk_codelst");
                    }                
                }
                Rlog.debug("patstudystat",
                        "PatStudyStatDao.getStudyPatients Line 11");
                whereClause2 = " and STATUS_ID in(" + sId1 + "," + sId2 + " ) ";
                pseq.close();
                Rlog.debug("patstudystat",
                        "PatStudyStatDao.getStudyPatients Line 12");
            }
            Rlog.debug("patstudystat",
                    "PatStudyStatDao.getStudyPatients Line 13");
            if ((patientID != null) && (!(patientID.trim().equals("")))) {
                Rlog.debug("patstudystat",
                        "PatStudyStatDao.getStudyPatients Line 14");
                whereClause3 = " and UPPER(PER_CODE) = UPPER('" + patientID
                        + "')";
            }            
            Rlog.debug("patstudystat",
                    "PatStudyStatDao.getStudyPatients Line 15");

            orderBy = " Order By FK_STUDY, PER_CODE DESC ";
            selectClause = selectClause + whereClause1 + whereClause2
                    + whereClause3 + orderBy;
            Rlog.debug("patstudystat",
                    "PatStudyStatDao.getStudyPatients select Clause "
                            + selectClause);
            //System.out.println("PatStudyStatDao.getStudyPatients sql1 " + selectClause);
            
            pstmt = conn.prepareStatement(selectClause);
            // pstmt.setString(1,studyId);
            // pstmt.setString(2,studyId);
            // pstmt.setString(3,studyId);
            // pstmt.setString(4,studyId);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                Rlog.debug("patstudystat",
                        "PatStudyStatDao.getStudyPatients Line 16");
                if (studyId == 0) {
                    right = rs.getString("STUDY_TEAM_RIGHTS");
                    if (right.length() >= 11) {
                        Rlog.debug("patstudystat",
                                "PatStudyStatDao.getStudyPatients Line 16");
                        right = right.substring(rightSeq - 1, rightSeq);
                    } else {
                        Rlog.debug("patstudystat",
                                "PatStudyStatDao.getStudyPatients Line 17");
                        right = "0";
                    }
                } else {
                    Rlog.debug("patstudystat",
                            "PatStudyStatDao.getStudyPatients Line 18");
                    right = "4";
                }
                if (Integer.parseInt(right) > 0) {
                    Rlog.debug("patstudystat",
                            "PatStudyStatDao.getStudyPatients Line 19");
                    setPatientIds(new Integer(rs.getInt("FK_PER")));
                    setPatientEnrollDates(rs.getString("PATPROT_ENROLDT"));
                    setPatientCodes(rs.getString("PER_CODE"));
                    setPatientStats(String.valueOf(rs.getInt("STATUS_ID")));
                    setStudyVersions(rs.getString("STUDY_VER_NO"));
                    setStudyNums(rs.getString("STUDY_NUMBER"));
                    setStudyIds(new Integer(rs.getInt("FK_STUDY")));
                    setPatientStatusDescs(rs.getString("STATUS_DESC"));
                    setPatientProtIds(rs.getString("pk_patprot"));
                    setPatientStatusPk(new Integer(rs.getInt("PK_PATSTUDYSTAT")));
                    rows++;
                    Rlog.debug("patstudystat",
                            "PatStudyStatDao.getStudyPatients Line 20");
                }
            }
            setCRows(rows);
            if (pstmt != null)
                pstmt.close();
            Rlog.debug("patstudystat",
                    "PatStudyStatDao.getStudyPatients Line 21");
            if (conn != null)
                conn.close();
            if (studyId == 0) {
                selectClause2 = "select pk_study , study_number, STUDY_VER_NO, t.STUDY_TEAM_RIGHTS   "
                        + "from er_study, er_studyteam t  "
                        + "where not exists ( select 1 from er_patstudystat, er_per "
                        + " where pk_per = fk_per "
                        + " and fk_site = "
                        + userSite
                        + " and fk_study = er_Study.pk_study) "
                        + "and fk_account = (select fk_account from er_site where pk_site = "
                        + userSite
                        + ") "
                        + " and t.fk_user = "
                        + user
                        + " and "
                        + " t.fk_study = pk_study and study_actualdt is not null ";

                sqlst = selectClause2 + " Order By pK_STUDY";
                Rlog.debug("patstudystat",
                        "PatStudyStatDao.getStudyPatients sqlst " + sqlst);
                
                //System.out.println("PatStudyStatDao.getStudyPatients sql2 " + sqlst);
                
                conn = getConnection();
                pstmt2 = conn.prepareStatement(sqlst);
                String right2 = "";
                Rlog.debug("patstudystat",
                        "PatStudyStatDao.getStudyPatients Line 22");
                // pstmt.setString(1,studyId);
                // pstmt.setString(2,studyId);
                // pstmt.setString(3,studyId);
                // pstmt.setString(4,studyId);
                ResultSet rs2 = pstmt2.executeQuery();
                while (rs2.next()) {
                    Rlog.debug("patstudystat",
                            "PatStudyStatDao.getStudyPatients Line 23");
                    right2 = rs2.getString("STUDY_TEAM_RIGHTS");
                    if (right2.length() >= 11) {
                        Rlog.debug("patstudystat",
                                "PatStudyStatDao.getStudyPatients Line 24");
                        right2 = right2.substring(rightSeq - 1, rightSeq);
                    } else {
                        Rlog.debug("patstudystat",
                                "PatStudyStatDao.getStudyPatients Line 25");
                        right2 = "0";
                    }                    
                    if (Integer.parseInt(right2) > 0) {
                        setPatientIds(new Integer("0"));
                        setPatientEnrollDates("");
                        setPatientCodes("");
                        setPatientStats("");
                        setStudyVersions(rs2.getString("STUDY_VER_NO"));
                        setStudyNums(rs2.getString("STUDY_NUMBER"));
                        setStudyIds(new Integer(rs2.getInt("pK_STUDY")));
                        setPatientStatusDescs("");
                        setPatientProtIds("");
                        setPatientStatusPk(new Integer("0"));
                        rows2++;
                        Rlog.debug("patstudystat",
                                "PatStudyStatDao.getStudyPatients Line 26");
                    }
                }
                Rlog.debug("patstudystat",
                        "PatStudyStatDao.getStudyPatients Line 27");
                setCRows(getCRows() + rows2);
                Rlog.debug("patstudystat",
                        "PatStudyStatDao.getStudyPatients Line 28");
                if (pstmt2 != null)
                    pstmt2.close();
                // end of if to get all studies in which there are no patients
            }
        } catch (Exception ex) {
            Rlog
                    .fatal(
                            "patstudystat",
                            "PatStudyStatDao.getStudyPatients EXCEPTION IN FETCHING FROM PatStudyStat table "
                                    + ex);
        } finally {
            try { if (conn != null) { conn.close(); } } catch(Exception e) {}
        }

    }

    // //////////////////////

    /**
     * Populates the DAO object with patient study history data.
     * 
     * @param patId
     *            Patient Id
     * @param studyId
     *            Study Id
     */

    public void getPatStatHistory(int patId, int studyId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        String dt = "";
        StringBuffer sbSQL = new StringBuffer();

        try {
            conn = getConnection();

            sbSQL
                    .append("select PK_PATSTUDYSTAT, FK_CODELST_STAT, PATSTUDYSTAT_DATE, ");
            sbSQL
                    .append(" PATSTUDYSTAT_ENDT, PATSTUDYSTAT_NOTE, er_codelst.codelst_desc, ");
            sbSQL
                    .append("(Select codelst_desc from er_codelst where pk_codelst = patstudystat_reason ) reason_desc, CURRENT_STAT, ");
            sbSQL.append("er_codelst.codelst_subtyp codelst_subtyp ");
            sbSQL.append(" from er_patstudystat,  er_codelst  ");
            sbSQL
                    .append(" where fk_per = ? and fk_study = ? and FK_CODELST_STAT = pk_codelst order by  PATSTUDYSTAT_DATE DESC,PK_PATSTUDYSTAT DESC  ");

            pstmt = conn.prepareStatement(sbSQL.toString());
            pstmt.setInt(1, patId);
            pstmt.setInt(2, studyId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setPatientStatusPk(new Integer(rs.getInt(1)));
                dt = rs.getString(3);
                if ((dt == null) || dt.trim().equals(""))
                    dt = "";

                Rlog.debug("dummy", "dt1" + dt);

                setPatientStatusStartDate(dt);

                Rlog.debug("dummy", "dt1" + dt);

                dt = "";

                dt = rs.getString(4);
                if ((dt == null) || dt.trim().equals(""))
                    dt = "";

                Rlog.debug("dummy", "dt2" + dt);

                setPatientStatusEndDate(dt);

                setPatientStatusNotes(rs.getString(5));
                setPatientStatusSubTypes(rs.getString("codelst_subtyp"));
                setPatientStatusDescs(rs.getString(6));
                setPatientStudyStatusReasons(rs.getString("reason_desc"));
                setCurrentStats(new Integer(rs.getInt("CURRENT_STAT")));//KM
                rows++;
                Rlog.debug("dummy", "PatStudyStatDao.getStudyPatients rows "
                        + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "dummy",
                            "PatStudyStatDao.getStudyPatients EXCEPTION IN FETCHING FROM PatStudyStat table "
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Populates the DAO object with patients associated with a study. Filters
     * data according to the parameters passed. If no study is <br>
     * passed, gets data for all the studies
     * 
     * @param studyId
     *            Study Id
     * @param patientID
     *            Patient Code
     * @param patStatus
     *            Patient current status in Study
     * @param user
     *            Logged in user id
     * @param userSite
     *            Organization id
     * @param enrolDt
     *            Enrollment date
     * @param lastDoneDt
     *            Last visit done on
     * @param nextVisitDate
     *            Next visit date
     * @param lastVisit
     *            last Visit Number
     * @param htMoreFilterParams
     *            A Hashtable object with more filter parameters.
     * @return A PatStudyStatDao object with patient enrollment data
     * @deprecated
     */
    /*
     * Modified by Sonia, 09/04/04, added another filter parameter:
     * htMoreFilterParams
     */
    /*
     * Modified by Sonia, 09/09/04, implemented selective sorting (point n click
     * at browser). Uses getSortByColumnName(sortByColumnCode) to determine the
     * sort by column name. Modify this method also if any change is made to
     * getStudyPatientResultsWithVisits(..) for column names/ sorting columns
     * used in its SQL
     */
    /*
     * Modified by Sonia, 09/13/04, to include patient study id with enrolled
     * patient results
     */
    /* Modified by Sonia, 09/15/04, to fix issue 1748 */

    // patientStudyId is added by Manimaran on 21Apr2005 for Enrolled
    // PatientStudyId search
    /*Modified by Sonia Abrol, 10/03/05 to check patient facility access and remove done on filter*/
    
    public void getStudyPatientResultsWithVisits(int studyId, String patientID,
            String patientStudyId, int patStatus, int user, String userSite,
            String enrolDt, String lastDoneDt, String nextVisitDate,
            String lastVisit, Hashtable htMoreFilterParams) {
        int rows = 0;
        int rows2 = 0;

        PreparedStatement pstmt = null;
        PreparedStatement pstmt2 = null;

        Connection conn = null;

        String selectClause, whereClause1, orderBy;
        String whereClause2 = "";
        String whereClause3 = "";
        String selectClause2 = "";
        String whereAllStudy = "";

        String whereClause4 = "";

        StringBuffer whereDate = new StringBuffer();

        Calendar calEnrolDt = Calendar.getInstance();
        
        Calendar calNextVisitDate = Calendar.getInstance();

        String filEnrolDt = "";
        
        String filNextVisitDate = "";
        String filLastVisit = "";

        String excludeNotEnrolled = "1";
        String sortByColumnCode = "";
        String sortByOrder = "";
        String sortByColumnName = "";
        String anySiteSelected = "0"; 

        StringBuffer sbAllStudies = new StringBuffer();
        lastVisit = (lastVisit == null) ? "" : lastVisit;
        lastVisit = (lastVisit.toLowerCase()).trim();

        if (htMoreFilterParams.containsKey("excludeNotEnrolled")) {
            excludeNotEnrolled = (String) htMoreFilterParams
                    .get("excludeNotEnrolled");
        }

        if (htMoreFilterParams.containsKey("sortByColumn")) {
            sortByColumnCode = (String) htMoreFilterParams.get("sortByColumn");
        }

        if (htMoreFilterParams.containsKey("sortByOrder")) {
            sortByOrder = (String) htMoreFilterParams.get("sortByOrder");
        }

        if (htMoreFilterParams.containsKey("anySiteSelected")) {
        	anySiteSelected = (String) htMoreFilterParams.get("anySiteSelected");
        }

        
        
        if (!StringUtil.isEmpty(sortByColumnCode)) {
            sortByColumnName = getSortByColumnName(sortByColumnCode);
        }

        Rlog.debug("patstudystat", "PatStudyStatDao.getStudyPatientsWithVisits: enrolDt:"
                        + enrolDt + "excludeNotEnrolled"  + excludeNotEnrolled);

        
        
        if (!enrolDt.equals("ALL")) {
            filEnrolDt = " and PATPROT_ENROLDT "
                    + StringUtil.getRelativeTime(calEnrolDt, enrolDt) + " ";
        } else if (excludeNotEnrolled.equals("1")) {
            filEnrolDt = " and PATPROT_ENROLDT IS NOT NULL ";
        }

        

        if (!nextVisitDate.equals("ALL")) {
            filNextVisitDate = " and NEXT_VISIT "
                    + StringUtil.getRelativeTime(calNextVisitDate, nextVisitDate)
                    + " ";
        }

        if (lastVisit.length() > 0) {
            // filLastVisit = " and CUR_VISIT = " + Integer.parseInt(lastVisit);
            filLastVisit = " and lower(LAST_VISIT_NAME) like '%"
                    + lastVisit.toLowerCase() + "%'";
        }

        whereDate.append(filLastVisit);
        whereDate.append(filEnrolDt);
        
        whereDate.append(filNextVisitDate);

        String sqlst = "";

        int sId1 = 0;
        int sId2 = 0;

        PreparedStatement pseq = null;
        int rightSeq = 0;
        String right = "";
        int selectedSite = 0;

        try {
        	
        	if (anySiteSelected.equals("0"))
        	{
        		selectedSite = 0;
           	}
        	else
        	{
        		try{
        			selectedSite = Integer.parseInt(userSite); 
        			
        		}catch (Exception ex)
        		{
        			selectedSite  = 0;
        			
        		}
        		
        	}
            conn = getConnection();
            if (studyId == 0) {
                pseq = conn
                        .prepareStatement("select ctrl_seq from er_ctrltab where ctrl_key = 'study_rights' and upper(ctrl_value) = 'STUDYMPAT'");
                ResultSet rsSeq = pseq.executeQuery();

                while (rsSeq.next()) {
                    rightSeq = rsSeq.getInt("ctrl_seq");
                }
                pseq.close();
            }
            //Query is Modified by Manimaran to get the patient study status details.
             //Query is modified by Manimaran to fix the Bug2425	    
            selectClause = " Select erv.fk_per, p.person_lname, p.person_fname, per_site siteId, "
                    + " (select site_name from er_site where pk_site = per_site) site_name, "
                    + " erv.study_number, erv.pk_patprot, erv.fk_study,lower(per_code) lowerpercode, erv.per_code, " 
                    + "(case when (erv.patstudystat_desc  in(select CODELST_DESC from er_codelst where codelst_type='patStatus' and codelst_custom_col='browser'))  then erv.patstudystat_desc else '-'  end) patstudystat_desc,"
                    //+ "erv.patstudystat_desc, " 
                    + "erv.PATSTUDYSTAT_SUBTYPE, erv.PK_PATSTUDYSTAT, erv.patstudystat_date,erv.patstudystat_note,"
		    
		    +"(select codelst_desc from er_codelst where pk_codelst=erv.patstudystat_reason) patstudystat_reason,"
		    // query modified by Gopu to fix the bugzilla issues #2604 while selecting "All" from study dropdown 
		    +"erv.patprot_enroldt, lower(erv.last_visit_name) lowerlastvisit,erv.last_visit_name,erv.cur_visit, erv.cur_visit_date, erv.next_visit , lower(p.person_lname || p.person_fname) patnamelower,lower(erv.PATPROT_PATSTDID) lowerpatstdid,erv.PATPROT_PATSTDID, "
                    + "('[VELSEP]'||p.PERSON_HPHONE||'[VELSEP]'||p.PERSON_BPHONE||'[VELSEP]') PERSON_PHONE ,('[VELSEP]'||p.PERSON_ADDRESS1||'[VELSEP]'||p.PERSON_ADDRESS2||'[VELSEP]'||p.PERSON_CITY||'[VELSEP]'|| p.PERSON_STATE||'[VELSEP]'||p.PERSON_ZIP||'[VELSEP]'||p.PERSON_COUNTRY||'[VELSEP]') as pataddress "
                    + " ,(select codelst_desc from er_codelst where pk_codelst=p.FK_CODELST_RACE) patrace "
                   //Query modified by Manimaran to fix the Bug2739.
                    + " ,(select codelst_desc from er_codelst where pk_codelst=p.FK_CODELST_ETHNICITY) patethnicity ,dbms_lob.substr(p.PERSON_NOTES_CLOB,1000,1) personnotes,lower(p.person_lname) lowerplname,lower(p.person_fname) lowerpfname,  pkg_patient.f_get_patsites(erv.fk_per,"+selectedSite+") pat_facilities ";
		 		    
            if (studyId == 0) {

                
                whereClause1 = ", t.STUDY_TEAM_RIGHTS from erv_studypat_by_visit erv , er_studyteam t, erv_person p"
                        
                        + " where  "
                        + " exists "
                        + " (select * from er_study_site_rights r, er_user u, er_studyteam t , er_patfacility fac "
                        + " where pk_user=r.fk_user and t.fk_user = pk_user  "
                        + " and t.fk_study = r.fk_study and t.fk_user = r.fk_user  "
                        + " and r.fk_study = erv.fk_study "
                        + " and r.fk_user = "  + user
                        + " and USER_STUDY_SITE_RIGHTS = 1 "
                        + " and r.fk_site in(select fk_site from er_studysites where fk_study =  erv.fk_study) "
                        + " and  nvl(study_team_usr_type,'Y') <> 'S'  AND fac.fk_site = r.fk_site and " +
                        " fac.patfacility_accessright > 0 AND fac.fk_per = erv.fk_per and fac.fk_site in( "
                        + userSite
                        + ") ) "
                        + " and p.pk_person = erv.fk_per and "
                        + " t.fk_user = "
                        + user
                        + " and t.fk_study = erv.fk_study and nvl(t.study_team_usr_type,'D')='D' ";

                       // + " and erv.PATSTUDYSTAT_ID in (select pk_codelst from er_codelst where CODELST_TYPE='patStatus' "
                        //+  " and CODELST_CUSTOM_COL='browser')"	;
                
                		//JM: 02/11/2005 above 2 lines added


                whereAllStudy = ", t.STUDY_TEAM_RIGHTS from erv_studypat_by_visit erv , er_studyteam t , erv_person p where p.pk_person = erv.fk_per "
                	    + " and exists (select * from er_patfacility fac where fac.fk_per = erv.fk_per and  fac.fk_site in( " + userSite + ") and fac.patfacility_accessright > 0 )" 
                	    + " and  t.fk_user = " + user
                        + " and t.fk_study = erv.fk_study "; 
                        
            } else {
                whereClause1 = " from erv_studypat_by_visit erv, erv_person p where p.pk_person = erv.fk_per and erv.FK_STUDY = " + studyId 
                	+  " and exists (select * from  er_patfacility fac where fac.fk_per = erv.fk_per and  fac.fk_site in( " + userSite + ") and fac.patfacility_accessright > 0 )";
                	
                	//+  " and erv.PATSTUDYSTAT_ID in (select pk_codelst from er_codelst where CODELST_TYPE='patStatus' "
                	//+  " and CODELST_CUSTOM_COL='browser')"	;
                
                	
                }

            
            if (patStatus > 0) {
                whereClause2 = " and PATSTUDYSTAT_ID = " + patStatus;

            } else if (patStatus < 0) {

                pseq = conn
                        .prepareStatement("select pk_codelst from er_codelst where CODELST_TYPE = 'patStatus' and (ltrim(rtrim(lower(CODELST_SUBTYP))) = 'active' or ltrim(rtrim(lower(CODELST_SUBTYP))) = 'reactivate')");
                ResultSet rsSeq3 = pseq.executeQuery();
                while (rsSeq3.next()) {
                    if (sId1 == 0) {
                        sId1 = rsSeq3.getInt("pk_codelst");
                    } else {
                        sId2 = rsSeq3.getInt("pk_codelst");
                    }
                }

                whereClause2 = " and PATSTUDYSTAT_ID in(" + sId1 + "," + sId2
                        + " ) ";
                pseq.close();
            }

            if ((patientID != null) && (!(patientID.trim().equals("")))) {
                whereClause3 = " and (UPPER(PER_CODE) LIKE UPPER('%" + patientID
                 + "%') or exists (select * from er_patfacility fac where " +
              " lower(pat_facilityid) like lower('%"+patientID+"%') and  fac.fk_site in( " + userSite + ")  and fac.patfacility_accessright > 0 " + 
            " AND fac.fk_per = erv.fk_per)  )";
            }

            // for search criteria on patstudyid by Manimaran: 21Apr05
            if ((patientStudyId != null)
                    && (!(patientStudyId.trim().equals("")))) {
                whereClause4 = " and UPPER(PATPROT_PATSTDID) LIKE UPPER('%"
                        + patientStudyId + "%')";
            }

	        if (StringUtil.isEmpty(sortByColumnName)) {	        	
	        	orderBy = " Order By FK_STUDY,siteId, patnamelower  ";
            } else {            	
				// modified by gopu to fix the bugzilla issue #2044 on 24th Apr 2006 Patient Enrolled browser 'Patient Study Id' 
	    		// Sorting is case sensitive
                 //orderBy = " Order By FK_STUDY, siteId, lower(" + sortByColumnName+")" + sortByOrder ;
            	//commented by gopu 
            	 orderBy = " Order By FK_STUDY, siteId, " + sortByColumnName + " " + sortByOrder ;                 
                   
              /* 
            	 // if (sortByColumnName.equals("patprot_enroldt")){                	  
                //	   orderBy = orderBy + " , " + sortByColumnName + " "+ sortByOrder;
            	//	} else{
            	//		   orderBy = " Order By (" + sortByColumnName + ") " + sortByOrder;                   
            	//	}
              */                       	   
            }
	        	
            if (studyId == 0) {
                sbAllStudies.append(" UNION " + selectClause + whereAllStudy
                        + whereClause2 + whereClause3 + whereClause4
                        + whereDate.toString());
                sbAllStudies
                        .append(" and nvl(t.study_team_usr_type,'D')='S' and  0 = ");
                sbAllStudies
                        .append(" ( Select count(*) from er_studyteam i where i.fk_study = t.fk_study and i.fk_user = "
                                + user + " and i.study_team_usr_type ='D')");                
            } else {
                sbAllStudies.append(" ");
            }
            selectClause = selectClause + whereClause1 + whereClause2
                    + whereClause3 + whereClause4 + whereDate.toString()
                    + sbAllStudies.toString() + orderBy;            
            Rlog.fatal("patstudystat",
                    "PatStudyStatDao.getStudyPatients select Clause by visit"
                            + selectClause);
			
            
            	
            pstmt = conn.prepareStatement(selectClause);
            Rlog.debug("patstudystat", "PatStudyStatDao. 11111");
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("patstudystat", "PatStudyStatDao.222221");
            while (rs.next()) {

                if (studyId == 0) {
                    right = rs.getString("STUDY_TEAM_RIGHTS");
                    if (right.length() >= 11) {
                        right = right.substring(rightSeq - 1, rightSeq);
                    } else {
                        right = "0";
                    }
                } else {
                    right = "4";
                }
                Rlog.debug("patstudystat", "PatStudyStatDao. 333333");
                if (Integer.parseInt(right) > 0) {

                    setPatientIds(new Integer(rs.getInt("FK_PER")));
                    setPatientLnames(rs.getString("person_lname"));
                    setPatientFNames(rs.getString("person_fname"));
                    setSiteIds(rs.getString("siteId"));
                    setSiteNames(rs.getString("site_name"));
                    setPatientEnrollDates(rs.getString("PATPROT_ENROLDT"));
                    setPatientCodes(rs.getString("PER_CODE"));
                    setStudyNums(rs.getString("STUDY_NUMBER"));
                    setStudyIds(new Integer(rs.getInt("FK_STUDY")));
                    setPatientStatusDescs(rs.getString("PATSTUDYSTAT_DESC"));
                    setPatientProtIds(rs.getString("pk_patprot"));
                    setPatientStatusPk(new Integer(rs.getInt("PK_PATSTUDYSTAT")));
		    
		    //Added by Manimaran to display the patient study status details.
		    setPatientStatusStartDate(rs.getString("PATSTUDYSTAT_DATE"));
		    setPatientStatusNotes(rs.getString("PATSTUDYSTAT_NOTE"));
		    setPatientStudyStatusReasons(rs.getString("PATSTUDYSTAT_REASON"));
		    
		    setPatientStatusSubTypes(rs.getString("PATSTUDYSTAT_SUBTYPE"));
                    setCurVisits(new Integer(rs.getInt("CUR_VISIT")));
                    setCurVisitsName(rs.getString("last_VISIT_NAME"));
                    setCurVisitDates(rs.getString("CUR_VISIT_DATE"));
                    setNextVisits(rs.getString("NEXT_VISIT"));
                    setPatientStudyIds(rs.getString("PATPROT_PATSTDID"));
                    setPatientAddress(rs.getString("pataddress"));
                    setPatientPhone(rs.getString("person_phone"));
                    setPatientRace(rs.getString("patrace"));
                    setPatientEthnicity(rs.getString("patethnicity"));
                    setPatientNotes(rs.getString("personnotes"));//JM: 04October2006
                    setPatFacilities(rs.getString("pat_facilities"));
                    rows++;

                }
            }
            Rlog.debug("patstudystat", "PatStudyStatDao. 44444");
            setCRows(rows);
            if (pstmt != null)
                pstmt.close();

            if (conn != null)
                conn.close();
            Rlog.debug("patstudystat", "PatStudyStatDao. 55555");
            
            
                        
            if (studyId == 0) // when 'all' is selected, get data for studies
            // in which there is no patint enrollment
            {
                Rlog.debug("patstudystat", "PatStudyStatDao. 66666");

                selectClause2 = "select pk_study , study_number, STUDY_VER_NO, t.STUDY_TEAM_RIGHTS   "
                        +

                          "from er_study, er_studyteam t,  "
                        + " er_study_site_rights r  where "
                        + "	r.fk_study = t.fk_study "
                        + " and r.fk_study = pk_study "
                        + " and r.fk_user  = t.fk_user  "
                        + " and r.fk_site in(  "
                        + userSite
                        + ") and USER_STUDY_SITE_RIGHTS = 1  "
                        + " and not exists ( select 1 from er_patstudystat, er_per "
                        + " where pk_per = fk_per "
                        +

                        " and fk_site in( "
                        + userSite
                        + ")"
                        + " and fk_study = er_Study.pk_study) "
                        + "and fk_account = (select distinct(fk_account) from er_site where pk_site in( "
                        + userSite
                        + ")) "
                        + " and t.fk_user = "
                        + user
                        + " and "
                        + " t.fk_study = pk_study and nvl(t.study_team_usr_type,'D')='D' and study_actualdt is not null UNION "
                        + "select pk_study , study_number, STUDY_VER_NO, t.STUDY_TEAM_RIGHTS   "
                        + "from er_study, er_studyteam t  "
                        + "where not exists ( select 1 from er_patstudystat, er_per "
                        +

                        " where pk_per = fk_per "
                        + " and fk_site in( "
                        + userSite
                        + ")"
                        + " and fk_study = er_Study.pk_study) "
                        + "and fk_account = (select distinct(fk_account) from er_site where pk_site in( "
                        + userSite
                        + ")) "
                        + " and t.fk_user = "
                        + user
                        + " and "
                        + " t.fk_study = pk_study and nvl(t.study_team_usr_type,'D')='S' and study_actualdt is not null  "
                        + " and  0 = ( Select count(*) from er_studyteam i where i.fk_study = t.fk_study and i.fk_user = "
                        + user + " and i.study_team_usr_type ='D')";

                sqlst = selectClause2 + " Order By pk_study";
                
        
                Rlog.debug("patstudystat", "PatStudyStatDao.77777");
                Rlog.debug("patstudystat",
                        "PatStudyStatDao.getStudyPatientsbyvisit sqlst2 "
                                + sqlst);
                
                
                
                conn = getConnection();
                pstmt2 = conn.prepareStatement(sqlst);
                String right2 = "";
                Rlog.debug("patstudystat", "PatStudyStatDao.88888");
                ResultSet rs2 = pstmt2.executeQuery();
                while (rs2.next()) {

                    right2 = rs2.getString("STUDY_TEAM_RIGHTS");
                    if (right2.length() >= 11) {
                        right2 = right2.substring(rightSeq - 1, rightSeq);
                    } else {
                        right2 = "0";
                    }
                    if (Integer.parseInt(right2) > 0) {
                        setPatientIds(new Integer("0"));
                        setPatientLnames("");
                        setPatientFNames("");

                        setSiteIds("");
                        setSiteNames("");
                        setPatientAddress("");
                        setPatientPhone("");
                        setPatientRace("");
                        setPatientEthnicity("");
                        setPatientNotes("");

                        setPatientEnrollDates("");
                        setPatientCodes("");
                        setStudyNums(rs2.getString("STUDY_NUMBER"));
                        setStudyIds(new Integer(rs2.getInt("pK_STUDY")));
                        setPatientStatusDescs("");
                        setPatientStatusSubTypes("");
                        setPatientProtIds("");
                        setPatientStatusPk(new Integer("0"));
			//km
			
			setPatientStatusStartDate("");
                        setPatientStatusNotes("");
			setPatientStudyStatusReasons("");
			
			setCurVisits(new Integer("0"));
                        setCurVisitDates("");
                        setNextVisits("");

                        // Added by Manimaran on 21Apr2005 for patient studyid
                        // search.
                        setPatientStudyIds("");
                        setPatFacilities("");

                        rows2++;
                    }
                }
                Rlog.debug("patstudystat", "PatStudyStatDao. 999994");
                setCRows(getCRows() + rows2);
                if (pstmt2 != null)
                    pstmt2.close();
                // end of if to get all studies in which there are no patients
            }
        } catch (Exception ex) {
            Rlog.fatal("patstudystat", "getStudyPatientResults" + ex);
            ex.printStackTrace();
        } finally {
            try { if (conn != null) { conn.close(); } } catch(Exception e) {}
        }
        Rlog.debug("patstudystat", "PatStudyStatDao. 10101010");
    } // end of method

    // ///////////////////

    /* Modified by sonia Abrol, 10/12/05, check rights according to patient facilties*/
    public void getPatientStudiesWithVisits(int studyId, int pkey,
            int patStatus, int user, int userSite, String enrolDt,
            String lastDoneDt, String nextVisitDate, String lastVisit) {
        int rows = 0;

        PreparedStatement pstmt = null;

        Connection conn = null;

        String selectClause = "";
        String  whereClause1          ="";
        String  whereClause0          ="";
        String orderBy = "";
        String whereClause2 = "";
        String whereClause3 = "";

        String selectClause2 = "";

        StringBuffer whereDate = new StringBuffer();

        Calendar calEnrolDt = Calendar.getInstance();
        Calendar calLastDoneDt = Calendar.getInstance();
        Calendar calNextVisitDate = Calendar.getInstance();

        String filEnrolDt = "";
        String filLastDoneDt = "";
        String filNextVisitDate = "";
        String filLastVisit = "";
        int rightSeq = 0;
        
        if (!enrolDt.equals("ALL")) {
            filEnrolDt = " and PATPROT_ENROLDT "
                    + StringUtil.getRelativeTime(calEnrolDt, enrolDt) + " ";
        }

        if (!lastDoneDt.equals("ALL")) {
            filLastDoneDt = " and CUR_VISIT_DATE "
                    + StringUtil.getRelativeTime(calLastDoneDt, lastDoneDt) + " ";
        }

        if (!nextVisitDate.equals("ALL")) {
            filNextVisitDate = " and NEXT_VISIT "
                    + StringUtil.getRelativeTime(calNextVisitDate, nextVisitDate)
                    + " ";
        }

        if (lastVisit != null) {
            filLastVisit = " and CUR_VISIT = " + Integer.parseInt(lastVisit);
        }

        whereDate.append(filLastVisit);
        whereDate.append(filEnrolDt);
        whereDate.append(filLastDoneDt);
        whereDate.append(filNextVisitDate);

       
        PreparedStatement pseq = null;
  
        try {
            conn = getConnection();
            if (studyId == 0) {
                pseq = conn
                        .prepareStatement("select ctrl_seq from er_ctrltab where ctrl_key = 'study_rights' and upper(ctrl_value) = 'STUDYMPAT'");
                ResultSet rsSeq = pseq.executeQuery();

                while (rsSeq.next()) {
                    rightSeq = rsSeq.getInt("ctrl_seq");
                }
                pseq.close();
            }
            selectClause = " Select erv.fk_per, erv.study_number, erv.STUDY_TITLE,  erv.pk_patprot, erv.fk_study,  erv.per_code, "
			+ "(case when (erv.patstudystat_desc  in(select CODELST_DESC from er_codelst where codelst_type='patStatus' and codelst_custom_col='browser'))  then erv.patstudystat_desc else '-'  end) patstudystat_desc,"
			
			//erv.patstudystat_desc,
			// modified by gopu to fix the Issues #2517
			+"erv.PK_PATSTUDYSTAT, erv.patprot_enroldt, erv.cur_visit, erv.last_visit_name,erv.cur_visit_date, erv.next_visit ";

            whereClause1 = "  from erv_studypat_by_visit erv where EXISTS " +
            		" (SELECT * FROM ER_USERSITE  usr, ER_PATFACILITY fac WHERE fk_user ="+user +
            	" AND 	usersite_right>=4 AND usr.fk_site = fac.fk_site AND fac.patfacility_accessright > 0 AND " +
            	" fk_per ="+ pkey +  ")";
				// commented by gopu to fix the Issues #2517
				//;+" and erv.PATSTUDYSTAT_ID in (select pk_codelst from er_codelst where CODELST_TYPE='patStatus' " +
            	//" and CODELST_CUSTOM_COL='browser')";
            
            
            if (studyId > 0) {
                whereClause0 = " and  erv.FK_STUDY = " + studyId;                
            }

            if (patStatus > 0) {
                whereClause2 = " and PATSTUDYSTAT_ID = " + patStatus;

            } /*
                 * else if (patStatus <= 0) {
                 * 
                 * pseq = conn.prepareStatement("select pk_codelst from
                 * er_codelst where CODELST_TYPE = 'patStatus' and
                 * (ltrim(rtrim(lower(CODELST_SUBTYP))) = 'active' or
                 * ltrim(rtrim(lower(CODELST_SUBTYP))) = 'reactivate')");
                 * ResultSet rsSeq3 = pseq.executeQuery(); while (rsSeq3.next()) {
                 * if (sId1 == 0) { sId1 = rsSeq3.getInt("pk_codelst"); } else {
                 * sId2 = rsSeq3.getInt("pk_codelst"); } }
                 * 
                 * whereClause2 = " and PATSTUDYSTAT_ID in(" + sId1 + "," + sId2 + " ) ";
                 * pseq.close(); }
                 */

            if (pkey != 0) {
                whereClause3 = " and FK_PER = " + pkey;
            }

            orderBy = " Order By erv.FK_STUDY  ";

            selectClause = selectClause + whereClause1 +whereClause0 + whereClause2
                    + whereClause3 + whereDate.toString() + orderBy;
            
           
            Rlog.debug("patstudystat",
                    "PatStudyStatDao.getPatientStudiesWithVisits select Clause by visit"
                            + selectClause);

            pstmt = conn.prepareStatement(selectClause);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {

                setPatientIds(new Integer(rs.getInt("FK_PER")));
                setPatientEnrollDates(rs.getString("PATPROT_ENROLDT"));
                setPatientCodes(rs.getString("PER_CODE"));
                setStudyNums(rs.getString("STUDY_NUMBER"));
                setStudyTitles(rs.getString("STUDY_TITLE"));
                setStudyIds(new Integer(rs.getInt("FK_STUDY")));
                setPatientStatusDescs(rs.getString("PATSTUDYSTAT_DESC"));
                setPatientProtIds(rs.getString("pk_patprot"));
                setPatientStatusPk(new Integer(rs.getInt("PK_PATSTUDYSTAT")));
                setCurVisits(new Integer(rs.getInt("CUR_VISIT")));
                setCurVisitsName(rs.getString("LAST_VISIT_Name"));
                setCurVisitDates(rs.getString("CUR_VISIT_DATE"));
                setNextVisits(rs.getString("NEXT_VISIT"));
                rows++;

            }
            setCRows(rows);
            
        } catch (Exception ex) {
        	Rlog.fatal("patstudystat", "getPatientStudiesWithVisits " + ex);
            Rlog.fatal("patstudystat", "getPatientStudiesWithVisits SQL" + selectClause);
        } finally {
            try { if (pstmt != null) { pstmt.close(); } } catch(Exception e) {}
            try { if (conn != null)  { conn.close(); } } catch(Exception e) {}
        }

    } // end of method

    // /
    /**
     * Retrieve the Studies for the the specified patient.
     * 
     * @param pKey
     *            int - patient for which studies are retrieved.
     * @param user
     *            int - current logged in user which is used to check user
     *            rights for the studies.
     */
  /* 11/09/07 modified by sonia for revised super user implementation*/
    
    public void getPatientStudies(int pkey, int user) {
        int rows = 0;

        PreparedStatement pstmt = null;
        Connection conn = null;

        StringBuffer sqlBuf = new StringBuffer();

        PreparedStatement pseq = null;

         
        String right = "";
        String superUserRights = "";
         
        
        try {
             
        	superUserRights = GroupDao.getDefaultStudySuperUserRights(String.valueOf(user));
        	
        	if (StringUtil.isEmpty(superUserRights))
        	{
        		superUserRights = "";
        	}
        	
        	 
	   		
        	
            sqlBuf.append(" SELECT   pk_study,  STUDY_NUMBER ");
            
            sqlBuf.append(" , (nvl((select study_team_rights from er_studyteam x where x.fk_user = ? and  x.fk_study = c.fk_study");
            sqlBuf.append("  	  and study_team_usr_type = 'D'), ? ) ) STUDY_TEAM_RIGHTS ");
              sqlBuf.append("FROM  ER_PATPROT C, ER_STUDY E  ");
            sqlBuf.append("WHERE C.PATPROT_STAT = 1 AND c.fk_per=" + pkey
                    + " AND ");
            sqlBuf
                    .append("c.fk_study=e.pk_study AND e.study_actualdt IS NOT NULL");
            sqlBuf.append(" and ( exists (select * from er_studyteam a where a.fk_study = c.fk_study and  fk_user = ? and study_team_usr_type = 'D'  ) "); 
            sqlBuf.append(" or pkg_superuser.F_Is_Superuser(?, c.fk_study) = 1 )   ");
             
            conn = getConnection();

             
            Rlog.debug("patstudystat",
                    "PatStudyStatDao.getPatientStudies - SQL - "
                            + sqlBuf.toString());

            pstmt = conn.prepareStatement(sqlBuf.toString());
            
            pstmt.setInt(1, user);
            pstmt.setString(2, superUserRights);
            pstmt.setInt(3, user);
            pstmt.setInt(4, user);
             
            
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setStudyNums(rs.getString("STUDY_NUMBER"));
                setStudyIds(new Integer(rs.getInt("PK_STUDY")));
                rows++;
             

            }

            setCRows(rows);
        } catch (Exception ex) {
            Rlog.fatal("patstudystat", "getPatientStudies " + ex);
        } finally {
            try { if (pstmt != null) { pstmt.close(); } } catch(Exception e) {}
            try { if (conn != null) { conn.close(); } } catch(Exception e) {}
        }

    } // end of method

    /**
     * Returns the column name mapped to the sort by column Code. This is a
     * private method used by method <br>
     * public void getStudyPatientResultsWithVisits(int studyId, String
     * patientID, int patStatus, int user, int userSite, String enrolDt, String
     * lastDoneDt, String nextVisitDate, String lastVisit,Hashtable
     * htMoreFilterParams) <br>
     * to determine the sort by column name. Modify this method also if any
     * change is made to getStudyPatientResultsWithVisits(..) for column names/
     * sorting columns used in its SQL
     */
    /*
     * Modified by Sonia, 09/14/04, to include sorting on patient study id with
     * enrolled patient results
     */
    private String getSortByColumnName(String sortByColumnCode) {
        if (StringUtil.isEmpty(sortByColumnCode))
            return "";

        if (sortByColumnCode.equals("patId"))
            return "lowerpercode";
        else if (sortByColumnCode.equals("patStudyId"))
          //return "PATPROT_PATSTDID";
            return "lowerpatstdid";
        else if (sortByColumnCode.equals("patName"))
            return "patnamelower";
        else if (sortByColumnCode.equals("patfname"))
            return "lowerpfname";
        else if (sortByColumnCode.equals("patlname"))
            return "lowerplname";
        else if (sortByColumnCode.equals("enrolledOn"))
            return "patprot_enroldt";
        else if (sortByColumnCode.equals("lastVisit"))
            //return "cur_visit";
        	return "lowerlastvisit";
        else if (sortByColumnCode.equals("nextVisit"))
            return "next_visit";
        else if (sortByColumnCode.equals("doneOn"))
            return "cur_visit_date";
        else if (sortByColumnCode.equals("patientStudyStatus"))
            return "patstudystat_desc";
        else
            return "";
    }
    
    public void updatePatCurrStat(int patStatId, int userId){
    	
    	Connection conn = null;
    	PreparedStatement pstmt = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement(" update er_patstudystat set CURRENT_STAT=0, LAST_MODIFIED_BY=?, LAST_MODIFIED_DATE=sysdate where PK_PATSTUDYSTAT=? ");
              
            pstmt.setInt(1, userId);
            pstmt.setInt(2, patStatId);
            pstmt.executeUpdate();

        } catch (SQLException ex) {
            Rlog.fatal("patstudystat",
                    "PatStudyStatDao.getCountPatStudyId EXCEPTION IN FETCHING FROM patprot table"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }

    
    //Added by Manimaran to prompt if PatientStudyId being entered is duplicate
    public int getCountPatStudyId(int studyId,String patStudyId) {
        int count = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select count(*) as count "
                    + "from er_patprot where upper(patprot_patstdid) = upper(?) and fk_study = ? and patprot_stat = 1 ");
                    
            pstmt.setString(1, patStudyId);
            pstmt.setInt(2, studyId);
            
            ResultSet rs = pstmt.executeQuery();

            rs.next();
            count = rs.getInt("count");

        } catch (SQLException ex) {
            Rlog.fatal("patstudystat",
                    "PatStudyStatDao.getCountPatStudyId EXCEPTION IN FETCHING FROM patprot table"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }
        return count;
    }
        
    
	public ArrayList<String> getPatFacilities() {
		return patFacilities;
	}

	public void setPatFacilities(ArrayList<String> patFacilities) {
		this.patFacilities = patFacilities;
	}

	public void setPatFacilities(String patFacility) {
		this.patFacilities.add(patFacility);
	}

	public int getPatientFdaStudies(int pkey, int user) {
        int fdaStudy = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        String selectClause = "";
        String whereClause1 ="";
        String orderBy = "";
        try {
            conn = getConnection();
            selectClause = " Select erv.fk_per, erv.study_number, erv.STUDY_TITLE,  erv.pk_patprot, erv.fk_study,  erv.per_code, "
			+ "(case when (erv.patstudystat_desc  in(select CODELST_DESC from er_codelst where codelst_type='patStatus' and codelst_custom_col='browser'))  then erv.patstudystat_desc else '-'  end) patstudystat_desc,"
			+"erv.PK_PATSTUDYSTAT, erv.patprot_enroldt, erv.cur_visit, erv.last_visit_name,erv.cur_visit_date, erv.next_visit ";

            whereClause1 = "  from erv_studypat_by_visit erv where EXISTS " +
            	" (SELECT * FROM ER_USERSITE  usr, ER_PATFACILITY fac WHERE fk_user ="+user +
            	" AND 	usersite_right>=4 AND usr.fk_site = fac.fk_site AND fac.patfacility_accessright > 0 AND " +
            	" fk_per ="+ pkey +  ")" +
            	" AND 1 = (SELECT NVL(FDA_REGULATED_STUDY,0) FROM ER_STUDY WHERE PK_STUDY = erv.FK_STUDY) ";    

            if (pkey != 0) {
            	whereClause1 += " and FK_PER = " + pkey;
            }

            orderBy = " Order By erv.FK_STUDY  ";

            selectClause = selectClause + whereClause1 + orderBy;
            
            Rlog.debug("patstudystat","PatStudyStatDao.getPatientFdaStudies select Clause by visit"
                            + selectClause);

            pstmt = conn.prepareStatement(selectClause);

            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
            	fdaStudy = 1;
            }            
        } catch (Exception ex) {
        	Rlog.fatal("patstudystat", "getPatientFdaStudies " + ex);
            Rlog.fatal("patstudystat", "getPatientFdaStudies SQL" + selectClause);
        } finally {
            try { if (pstmt != null) { pstmt.close(); } } catch(Exception e) {}
            try { if (conn != null)  { conn.close(); } } catch(Exception e) {}
        }
    	return fdaStudy;
    } // end of method

    // //////////////////////end of class

}
