//Vishal dt 03/29/2001

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.CLOB;

import com.velos.eres.audit.web.AuditRowEresJB;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

public class LabDao extends CommonDAO implements java.io.Serializable {
    private ArrayList labIds;

    private ArrayList labNames;

    private ArrayList groupIds;

    private ArrayList groupNames;

    private ArrayList labUnits;

    private String groupPullDown;

    private ArrayList nciIds;

    private ArrayList gradeCalc;

    private String[] saLabDates;

    private String[] saLabIds;

    private String[] saAccnNums;

    private String[] saLabResults;

    private String[] saTestTypes;

    private String[] saLabUnits;

    private String[] saLabLlns;

    private String[] saLabUlns;

    private String[] saLabStatus;

    private String[] saLabAbnrs;

    private String[] saLabCatIds;

    private String[] saLabAdvNames;

    private String[] saLabGrades;

    private String[] saLabLResults;

    private String[] saLabNotes;

    private String[] saStdPhase;

    private String pkey;

    private String siteId;

    private String patprotId;

    private String studyId;

    private String user;

    private String ipAdd;

    private String notes;

    private String longResult;

    private String specimenId;
    
    private Boolean hasFDAItem = false;

    public LabDao() {
        labIds = new ArrayList();
        labNames = new ArrayList();
        groupIds = new ArrayList();
        groupNames = new ArrayList();
        labUnits = new ArrayList();
        nciIds = new ArrayList();
        gradeCalc = new ArrayList();
        pkey = "";
        siteId = "";
        patprotId = "";
        user = "";
        ipAdd = "";
        longResult = "";
        notes = "";

    }

    // Getter and Setter methods
    public ArrayList getLabIds() {
        return labIds;
    }

    public void setLabIds(ArrayList labIds) {
        this.labIds = labIds;
    }

    public void setLabId(int labId) {
        labIds.add(new Integer(labId));
    }

    public ArrayList getLabNames() {
        return labNames;
    }

    public void setLabName(String labName) {
        this.labNames.add(labName);
    }

    public void setLabNames(ArrayList labNames) {
        this.labNames = labNames;
    }

    public ArrayList getGroupIds() {
        return groupIds;
    }

    public void setGroupId(int groupId) {
        this.groupIds.add(new Integer(groupId));
    }

    public void setGroupIds(ArrayList groupIds) {
        this.groupIds = groupIds;
    }

    public ArrayList getGroupNames() {
        return groupNames;
    }

    public void setGroupName(String groupName) {
        this.groupNames.add(groupName);
    }

    public void setGroupNames(ArrayList groupName) {
        this.groupNames = groupNames;
    }

    public ArrayList getNciIds() {
        return nciIds;
    }

    public void setNciIds(ArrayList nciIds) {
        this.nciIds = nciIds;
    }

    public void setNciIds(String nciId) {
        nciIds.add(nciId);
    }

    public ArrayList getGradeCalc() {
        return gradeCalc;
    }

    public void setGradeCalc(ArrayList gradeCalc) {
        this.gradeCalc = gradeCalc;
    }

    public void setGradeCalc(String gradeFlag) {
        gradeCalc.add(gradeFlag);
    }

    public void setLongResult(String longResult) {
        this.longResult = longResult;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getLongResult() {
        return longResult;
    }

    public String getNotes() {
        return notes;
    }

    public String getPullDown() {
        return groupPullDown;
    }

    public void setPullDown(String groupPullDown) {
        this.groupPullDown = groupPullDown;
    }

    public void setSaLabDates(String[] saLabDates) {
        this.saLabDates = saLabDates;
    }

    public void setSaLabIds(String[] saLabIds) {
        this.saLabIds = saLabIds;
    }

    public void setSaAccnNums(String[] saAccnNums) {
        this.saAccnNums = saAccnNums;
    }

    public void setSaLabResults(String[] saLabResults) {
        this.saLabResults = saLabResults;
    }

    public void setSaTestTypes(String[] saTestTypes) {
        this.saTestTypes = saTestTypes;
    }

    public void setSaLabUnits(String[] saLabUnits) {
        this.saLabUnits = saLabUnits;
    }

    public void setSaLabLlns(String[] saLabLlns) {
        this.saLabLlns = saLabLlns;
    }

    public void setSaLabUlns(String[] saLabUlns) {
        this.saLabUlns = saLabUlns;
    }

    public void setSaLabStatus(String[] saLabStatus) {
        this.saLabStatus = saLabStatus;
    }

    public void setSaLabAbnrs(String[] saLabAbnrs) {
        this.saLabAbnrs = saLabAbnrs;
    }

    public void setSaLabCatIds(String[] saLabCatIds) {
        this.saLabCatIds = saLabCatIds;
    }

    public void setSaLabAdvNames(String[] saLabAdvNames) {
        this.saLabAdvNames = saLabAdvNames;
    }

    public void setSaLabGrades(String[] saLabGrades) {
        this.saLabGrades = saLabGrades;
    }

    public void setSaLabLResults(String[] saLabLResults) {
        this.saLabLResults = saLabLResults;
    }

    public void setSaLabNotes(String[] saLabNotes) {
        this.saLabNotes = saLabNotes;
    }

    public void setSaStdPhase(String[] saStdPhase) {
        this.saStdPhase = saStdPhase;
    }

    public String[] getSaLabDates() {
        return saLabDates;
    }

    public String[] getSaLabIds() {
        return saLabIds;
    }

    public String[] getSaAccnNums() {
        return saAccnNums;
    }

    public String[] getSaLabResults() {
        return saLabResults;
    }

    public String[] getSaTestTypes() {
        return saTestTypes;
    }

    public String[] getSaLabUnits() {
        return saLabUnits;
    }

    public String[] getSaLabLlns() {
        return saLabLlns;
    }

    public String[] getSaLabUlns() {
        return saLabUlns;
    }

    public String[] getSaLabStatus() {
        return saLabStatus;
    }

    public String[] getSaLabAbnrs() {
        return saLabAbnrs;
    }

    public String[] getSaLabCatIds() {
        return saLabCatIds;
    }

    public String[] getSaLabAdvNames() {
        return saLabAdvNames;
    }

    public String[] getSaLabGrades() {
        return saLabGrades;
    }

    public String[] getSaLabLResults() {
        return saLabLResults;
    }

    public String[] getSaLabNotes() {
        return saLabNotes;
    }

    public String[] getSaStdPhase() {
        return saStdPhase;
    }

    public void setPkey(String pkey) {
        this.pkey = pkey;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public void setPatprotId(String patprotId) {
        this.patprotId = patprotId;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public String getPkey() {
        return pkey;
    }

    public String getSiteId() {
        return siteId;
    }

    public String getPatprotId() {
        return patprotId;
    }

    public String getUser() {
        return user;
    }

    public String getIpAdd() {
        return ipAdd;
    }

    public void setLabUnits(ArrayList labUnits) {
        this.labUnits = labUnits;
    }

    public void setLabUnits(String labUnit) {
        this.labUnits.add(labUnit);
    }

    public ArrayList getLabUnits() {
        return labUnits;
    }

    public void setStudyId(String studyId) {
        this.studyId = studyId;
    }

    public String getStudyId() {
        return studyId;
    }

    public void setSpecimenId(String specimenId) {
        this.specimenId = specimenId;
    }

    public String getSpecimenId() {
        return specimenId;
    }
    
    public Boolean isFDARegulated() {
    	return hasFDAItem;
    }

    // end of getter and setter methods

    /**
     * Populates lab test information for group/category provided.
     *
     * @param String
     *            groupId - retrieves lab test information for the category
     *            provided
     */
    public void getData(String groupId,String searchLabName) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int id = StringUtil.stringToNum(groupId);
        Rlog.debug("lab",
                "groupId received in LabDAO(after StringUtil Conversion):}" + id);
        try {
            conn = getConnection();
            String sql = "SELECT c.labtest_name,c.PK_LABTEST,b.GROUP_NAME, c.labtest_cpt, b.pk_labgroup,"
                    + " c.labtest_shortname,c.FK_LKP_ADV_NCI,c.GRADE_CALC FROM er_labtestgrp a , er_labgroup b, er_labtest c"
                    + " WHERE (    (a.fk_testid = c.pk_labtest) AND (a.fk_labgroup = b.pk_labgroup)"
                    + " AND (a.fk_labgroup=?) AND (lower(c.labtest_name) like ('%"+searchLabName.toLowerCase()+"%'))) order by lower(c.labtest_name)";
            Rlog.debug("lab", "sql" + sql);
            pstmt = conn.prepareStatement(sql);
            Rlog.debug("lab", "pstmt" + pstmt);
            pstmt.setInt(1, id);
            Rlog.debug("lab", "id" + id);
            rs = pstmt.executeQuery();
            Rlog.debug("lab", "rs" + rs);

            while (rs.next()) {
                setLabName(rs.getString("labtest_name"));
                setGroupName(rs.getString("group_name"));
                setLabId(StringUtil.stringToNum(rs.getString("pk_labtest")));
                setGroupId(StringUtil.stringToNum(rs.getString("pk_labgroup")));
                setNciIds(rs.getString("fk_lkp_adv_nci"));
                setGradeCalc(rs.getString("grade_calc"));
            }

        } catch (SQLException e) {
            Rlog.fatal("Lab", "{SQLEXCEPTION IN LabDao:getData(groupId)}" + e);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getData() {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            conn = getConnection();
            String sql = "SELECT c.labtest_name,c.PK_LABTEST,b.GROUP_NAME, c.labtest_cpt, b.pk_labgroup,"
                    + " c.labtest_shortname ,c.FK_LKP_ADV_NCI,c.GRADE_CALC FROM er_labtestgrp a , er_labgroup b, er_labtest c"
                    + " WHERE (    (a.fk_testid = c.pk_labtest) AND (a.fk_labgroup = b.pk_labgroup))";
            pstmt = conn.prepareStatement(sql);
            // pstmt.setInt(1,id);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                setLabName(rs.getString("labtest_name"));
                setGroupName(rs.getString("group_name"));
                setLabId(StringUtil.stringToNum(rs.getString("pk_labtest")));
                setGroupId(StringUtil.stringToNum(rs.getString("pk_labgroup")));
                setNciIds(rs.getString("fk_lkp_adv_nci"));
                setGradeCalc(rs.getString("grade_calc"));
            }
        } catch (SQLException e) {
            Rlog.fatal("Lab", "{SQLEXCEPTION IN LabDao:getData()}" + e);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void groupPullDown(String groupId) {
        Rlog
                .debug("lab",
                        "LabDao:getPullDown:getting groupPulldown for select lab screen");
        ArrayList tempNames = new ArrayList(), tempIds = new ArrayList();
        String pullDown = "";
        if (groupId == "")
            groupId = "0";
        int id = StringUtil.stringToNum(groupId);

        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            String sql = "SELECT GROUP_NAME,pk_labgroup from er_labgroup";
            pstmt = conn.prepareStatement(sql);
            // pstmt.setInt(1,id);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                tempIds.add(new Integer(rs.getInt("pk_labgroup")));
                tempNames.add(rs.getString("group_name"));
            }
            Rlog
                    .debug("lab",
                            "LabDao:getPullDown:populated arraylist,Lets make dropdown");
            pullDown = StringUtil.createPullDown("groupName", id, tempIds,
                    tempNames);
            this.setPullDown(pullDown);
        } catch (SQLException e) {
            Rlog.fatal("lab", "SQLException in LABDAO:groupPullDown");
            this.setPullDown("error");
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
    }

    public int updateClobData(int labId, String result, String notes) {

        CallableStatement cs_notes = null;
        CallableStatement cs_result = null;
        PreparedStatement pstmt = null;
        Connection conn=null;
        String filterStr = "";
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            if((!(notes.equals(""))) || (!(result.equals("")))){
            String sql = "update er_patlabs set notes=EMPTY_CLOB(),test_result_tx=EMPTY_CLOB() where pk_patlabs=? ";
            pstmt = conn.prepareStatement(sql);
            pstmt.clearParameters();
            pstmt.setInt(1, labId);
            pstmt.executeUpdate();
            pstmt.close();
            conn.commit();
            CLOB clob_notes = getCLOB(notes, conn);
            CLOB clob_result = getCLOB(result, conn);
            if(!(notes.equals(""))){
            cs_notes = (CallableStatement) conn
                    .prepareCall("begin sp_updateClob(?,?,?,?); end;");
            }
            if(!(result.equals(""))){
            cs_result = (CallableStatement) conn
                    .prepareCall("begin sp_updateClob(?,?,?,?); end;");
            }
            filterStr = "where pk_patlabs=" + labId;
            if(!(notes.equals(""))){
            // Set Notes Parameter
            cs_notes.setObject(1, clob_notes);
            cs_notes.setObject(2, "er_patlabs");
            cs_notes.setObject(3, "notes");
            cs_notes.setObject(4, filterStr);
            // Execute callable statement.
            cs_notes.execute();
            // Close the Statement object
            cs_notes.close();
            }
            if(!(result.equals(""))){
            // set Result Parameter
            // set clob data
            cs_result.setObject(1, clob_result);
            cs_result.setObject(2, "er_patlabs");
            cs_result.setObject(3, "test_result_tx");
            cs_result.setObject(4, filterStr);
            // Execute callable statement.
            cs_result.execute();
            // Close the Statement object
            cs_result.close();
            }
            
            }
            return 1; // successful

        } catch (Exception e) {
            Rlog.fatal("lab", "LabDao:Exception thrown in updateCLOB method "
                    + e.getMessage());
            return -1;
        }finally
        {
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    public int insertLab(LabDao labdao) {
        Rlog.fatal("lab", "In LabDao:insertLab start");
        String ipAdd = labdao.getIpAdd();
        String user = labdao.getUser();
        String patProtId = labdao.getPatprotId();
        String studyId = labdao.getStudyId();
        // String stdPhase=labdao.getStdPhase();
        String siteId = labdao.getSiteId();
        String pkey = labdao.getPkey();
        String[] saLabDates = labdao.getSaLabDates();
        String[] saLabIds = labdao.getSaLabIds();
        String[] saAccnNum = labdao.getSaAccnNums();
        String[] saLabResults = labdao.getSaLabResults();
        String[] saTestTypes = labdao.getSaTestTypes();
        String[] saLabUnits = labdao.getSaLabUnits();
        String[] saLabLlns = labdao.getSaLabLlns();
        String[] saLabUlns = labdao.getSaLabUlns();
        String[] saLabStatus = labdao.getSaLabStatus();
        String[] saLabAbnrs = labdao.getSaLabAbnrs();
        String[] saLabCatIds = labdao.getSaLabCatIds();
        String[] saLabAdvNames = labdao.getSaLabAdvNames();
        String[] saLabGrades = labdao.getSaLabGrades();
        String[] saLabLResults = labdao.getSaLabLResults();
        String[] saLabNotes = labdao.getSaLabNotes();
        String[] saStdPhase = labdao.getSaStdPhase();
        ArrayList saLabNClobs = new ArrayList();
        ArrayList saLabRClobs = new ArrayList();

      //JM: 07Jul2009: #INVP2.11
        String specimenId = labdao.getSpecimenId();

        int ret = 1;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            Clob tempClob;
            // Convert notes and labresults to Clob
            if (saLabNotes != null) {
                for (int i = 0; i < saLabNotes.length; i++) {
                    tempClob = getCLOB(saLabNotes[i], conn);
                    saLabNClobs.add((oracle.sql.CLOB) tempClob);
                }
            }
            if (saLabLResults != null) {
                for (int i = 0; i < saLabLResults.length; i++) {
                    tempClob = getCLOB(saLabLResults[i], conn);
                    saLabRClobs.add((oracle.sql.CLOB) tempClob);
                }
            }
            ArrayDescriptor inLabLResult = ArrayDescriptor.createDescriptor(
                    "ARRAY_CLOB", conn);
            ARRAY labLResultsArray = new ARRAY(inLabLResult, conn, saLabRClobs
                    .toArray());
            ArrayDescriptor inLabNotes = ArrayDescriptor.createDescriptor(
                    "ARRAY_CLOB", conn);
            ARRAY labNotesArray = new ARRAY(inLabNotes, conn, saLabNClobs
                    .toArray());
            ArrayDescriptor inLabDate = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY labDatesArray = new ARRAY(inLabDate, conn, saLabDates);
            ArrayDescriptor inLabIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY labIdsArray = new ARRAY(inLabIds, conn, saLabIds);
            ArrayDescriptor inAccnNums = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY accnNumsArray = new ARRAY(inAccnNums, conn, saAccnNum);
            ArrayDescriptor inLabResults = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY labResultsArray = new ARRAY(inLabResults, conn, saLabResults);
            ArrayDescriptor inTestTypes = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY labTestTypes = new ARRAY(inTestTypes, conn, saTestTypes);
            ArrayDescriptor inLabUnits = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY labUnitsArray = new ARRAY(inLabUnits, conn, saLabUnits);
            ArrayDescriptor inLabLlns = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY labLlnsArray = new ARRAY(inLabLlns, conn, saLabLlns);
            ArrayDescriptor inLabUlns = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY labUlnsArray = new ARRAY(inLabLlns, conn, saLabUlns);
            ArrayDescriptor inLabStatus = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY labStatusArray = new ARRAY(inLabStatus, conn, saLabStatus);
            ArrayDescriptor inLabAbnrs = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY labAbnrsArray = new ARRAY(inLabAbnrs, conn, saLabAbnrs);
            ArrayDescriptor inLabCatIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY labCatIdsArray = new ARRAY(inLabCatIds, conn, saLabCatIds);
            ArrayDescriptor inLabAdvNames = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY labAdvNamesArray = new ARRAY(inLabAdvNames, conn,
                    saLabAdvNames);
            ArrayDescriptor inLabGrades = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY labGradesArray = new ARRAY(inLabGrades, conn, saLabGrades);
            ArrayDescriptor inStdPhase = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY stdPhaseArray = new ARRAY(inStdPhase, conn, saStdPhase);
            cstmt = conn
                    .prepareCall("{call PKG_LAB.SP_INSERT_PATLABS(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            Rlog.debug("lab",
                    "Labdao:insertlab-after prepare call of sp_insert_patlabs");
            cstmt.setString(1, pkey);
            cstmt.setString(2, siteId);
            cstmt.setString(3, patProtId);
            cstmt.setArray(4, labDatesArray);
            cstmt.setArray(5, labIdsArray);
            cstmt.setArray(6, accnNumsArray);
            cstmt.setArray(7, labResultsArray);
            cstmt.setArray(8, labTestTypes);
            cstmt.setArray(9, labUnitsArray);
            cstmt.setArray(10, labLlnsArray);
            cstmt.setArray(11, labUlnsArray);
            cstmt.setArray(12, labStatusArray);
            cstmt.setArray(13, labAbnrsArray);
            cstmt.setArray(14, labCatIdsArray);
            cstmt.setArray(15, labAdvNamesArray);
            cstmt.setArray(16, labGradesArray);
            cstmt.setArray(17, labNotesArray);
            cstmt.setArray(18, labLResultsArray);
            cstmt.setString(19, user);
            cstmt.setString(20, ipAdd);
            cstmt.setArray(21, stdPhaseArray);
            cstmt.setString(22, studyId);
            cstmt.setString(23, specimenId);
            cstmt.registerOutParameter(24, java.sql.Types.INTEGER);
            cstmt.execute();

            Rlog.debug("lab",
                    "LabDao:insertLab-after execute of sp_insert_insertLab");
            ret = cstmt.getInt(23);
            Rlog.debug("lab", "LabDao:insertLab-after execute of RETURN VALUE"
                    + ret);

            return ret;

        } catch (Exception e) {
            Rlog.fatal("lab",
                    "EXCEPTION in Labdao:insertlab, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    public int deleteLab(String[] deleteIds, String tableName, String colName,
            String type) {

        int ret = 1;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            ArrayDescriptor inIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY deleteIdsArray = new ARRAY(inIds, conn, deleteIds);
            cstmt = conn
                    .prepareCall("{call PKG_LAB.SP_DELETE_PATLABS(?,?,?,?,?)}");
            Rlog.debug("lab",
                    "Labdao:deleteLab-after prepare call of sp_delete_patlabs");
            cstmt.setArray(1, deleteIdsArray);
            Rlog.debug("lab",
                    "Labdao:deleteLab-after prepare call of sp_delete_patlabs"
                            + deleteIdsArray);
            cstmt.setString(2, tableName);
            Rlog.debug("lab",
                    "Labdao:deleteLab-after prepare call of sp_delete_patlabs"
                            + tableName);
            cstmt.setString(3, colName);
            Rlog.debug("lab",
                    "Labdao:deleteLab-after prepare call of sp_delete_patlabs"
                            + colName);
            cstmt.setString(4, type);
            Rlog.debug("lab",
                    "Labdao:deleteLab-after prepare call of sp_delete_patlabs typoe"
                            + type);
            cstmt.registerOutParameter(5, java.sql.Types.INTEGER);
            cstmt.execute();
            Rlog.debug("lab",
                    "LabDao:deleteLab-after execute of sp_delete_patLab");
            ret = cstmt.getInt(5);
            Rlog.debug("lab", "LabDao:deleteLab-after execute of RETURN VALUE"
                    + ret);

            return ret;

        } catch (Exception e) {
            Rlog.fatal("lab",
                    "EXCEPTION in Labdao:deleteLab, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    public void retLabUnit(String[] testIds) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String idStr = "(";
        try {
            conn = getConnection();
            for (int i = 0; i < testIds.length; i++) {
                if (idStr.equals("("))
                    idStr = idStr + testIds[i];
                else
                    idStr = idStr + "," + testIds[i];
            }
            idStr = idStr + ")";
            Rlog.debug("lab", "LabDao:retLabUnit-idStr" + idStr);
            String sql = "SELECT pk_labunits, labunits_unit,fk_labtest "
                    + " FROM er_labunits where fk_labtest in "
                    + idStr
                    + " union "
                    + "select pk_labunits, labunits_unit,fk_labtest  FROM er_labunits "
                    + "where fk_labtest=0 order by fk_labtest";
            Rlog.debug("lab", "sql" + sql);
            pstmt = conn.prepareStatement(sql);
            // pstmt.setInt(1,id);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                setLabUnits(rs.getInt("fk_labtest") + ";"
                        + rs.getString("labunits_unit"));
            }
        } catch (SQLException e) {
            Rlog.fatal("Lab", "{SQLEXCEPTION IN LabDao:retLabUnit()}" + e);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void retLabUnit(String testId) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean notFound = true;
        int labPk = StringUtil.stringToNum(testId);
        try {
            conn = getConnection();

            String sql = "SELECT labunits_unit FROM er_labunits where fk_labtest = "
                    + testId;
            Rlog.debug("lab", "sql" + sql);
            pstmt = conn.prepareStatement(sql);
            // pstmt.setInt(1,id);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                setLabUnits(rs.getString("labunits_unit"));
                notFound = false;
            }
            if (notFound) {
                sql = "SELECT labunits_unit FROM er_labunits where fk_labtest =0";
                Rlog.debug("lab", "sql for default values " + sql);
                pstmt = conn.prepareStatement(sql);
                // pstmt.setInt(1,id);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    setLabUnits(rs.getString("labunits_unit"));

                }

            }

        } catch (SQLException e) {
            Rlog.fatal("Lab", "{SQLEXCEPTION IN LabDao:retLabUnit()}" + e);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getClobData(String testId) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int labPk = StringUtil.stringToNum(testId);
        CLOB lresult = null, notes = null;
        try {
            conn = getConnection();
            Rlog.debug("lab", "LabDao:getClobData" + labPk);
            String sql = "select test_result_tx,notes from er_patlabs where pk_patlabs="
                    + labPk;
            Rlog.debug("lab", "sql" + sql);
            pstmt = conn.prepareStatement(sql);
            // pstmt.setInt(1,id);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                lresult = (CLOB) rs.getClob(1);
                notes = (CLOB) rs.getClob(2);
                if (lresult != null)
                    setLongResult(lresult.getSubString(1, (int) lresult
                            .length()));
                if (notes != null)
                    setNotes(notes.getSubString(1, (int) notes.length()));

            }
        } catch (SQLException e) {
            Rlog.fatal("Lab", "{SQLEXCEPTION IN LabDao:getClobData()}" + e);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    private static final String COMMA = ",";
    public void checkFDARegulatedLabs(String[] labIds) {
    	hasFDAItem = false;
    	if (labIds == null) { return; }
    	StringBuffer sb1 = new StringBuffer();
        String checkFDARegulatedSql = " select S.FDA_REGULATED_STUDY from ER_PATLABS PL, " +
        		" ER_STUDY S where PL.FK_STUDY = S.PK_STUDY " +
        		" and PL.PK_PATLABS in (  ";
        sb1.append(checkFDARegulatedSql);
    	for (int iX=0; iX<labIds.length; iX++) {
        	sb1.append(StringUtil.stringToNum(labIds[iX])).append(COMMA); // using StringUtil to prevent SQL injection 
    	}
    	if (sb1.length() > 0) {
    		sb1.setLength(sb1.length()-1);
    	}
    	sb1.append(" ) ");
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(sb1.toString());
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	Integer fdaStudy = rs.getInt(1);
            	if (fdaStudy != null && fdaStudy == 1) {
            		hasFDAItem = true;
            		break;
            	}
            }
        } catch (SQLException e) {
            Rlog.fatal("Lab", "Exception in LabDao.checkFDARegulatedLabs(): " + e);
        } finally {
            if (pstmt != null) {
                try { pstmt.close(); } catch (Exception e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (Exception e) {}
            }
        }
    }
    
    public int  updateUserBeforeDelete(int patLabPk, int userid){
    	PreparedStatement pstmt = null;
        Connection conn = null;
        String mainSql="";
        int operationFlag=0;
        PreparedStatement pstm = null;
        Connection con = null;
        String Sql="";
        int opFlag=0;
        int raid=0;
        int rid=0;
        AuditRowEresJB auditRow= new AuditRowEresJB();
        
        try{
        	mainSql="update  er_patlabs set  LAST_MODIFIED_BY=? where PK_PATLABS=?";
        	
        	conn = getConnection();
        	pstmt = conn
                    .prepareStatement(mainSql);
        	
        	pstmt.setInt(1, userid);
        	pstmt.setInt(2, patLabPk);
        	operationFlag=pstmt.executeUpdate();
        	
        	
        }catch(SQLException e){
        	Rlog.fatal("Lab","EXCEPTION in updateUserBeforeDelete of LabDao class " + e);
        } 
        
    	try{
    		rid = getPatLabsRID(patLabPk);
    		raid=auditRow.findLatestRaidLab(rid,userid);
    		Sql = "delete from audit_row where raid=? and action=?";
    		
    		con = getConnection();
        	pstm = con
                    .prepareStatement(Sql);
        	
        	pstm.setInt(1, raid);
        	pstm.setString(2, "U");
        	opFlag=pstm.executeUpdate();
        	
        	
    	}catch(SQLException se){
    		Rlog.fatal("Lab","EXCEPTION in updateUserBeforeDelete delete sql of LabDao class " + se);
    	}
        finally {
    		try {
    			pstmt.close();
    			conn.close();
    		} catch (SQLException e) {
    			e.printStackTrace();
    		}
    	}
             
    	return operationFlag;
    	
    	
    }
    
    public int getPatLabsRID(int patLabPk) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int rid=0;
        
        try {
            conn = getConnection();

            String sql = "select rid from er_patlabs where PK_PATLABS=?";
            Rlog.debug("lab", "sql" + sql);
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1,patLabPk);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	rid = rs.getInt("rid");
            }

        } catch (SQLException e) {
            Rlog.fatal("Lab", "{SQLEXCEPTION IN LabDao:getPatLabsRID()}" + e);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        
        return rid;

    }

}
// end of class

