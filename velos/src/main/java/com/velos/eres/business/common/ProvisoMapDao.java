package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

public class ProvisoMapDao extends CommonDAO implements java.io.Serializable {
    
    private static final long serialVersionUID = 8651959741033438436L;

    public int getFKReport(String fkAccount, String fkReviewBoard, String reviewType,
            String submissionStatus) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlBuffer = new StringBuffer();
        StringBuffer whereClause = new StringBuffer(" where ");
        if (fkAccount != null) {
            whereClause.append(" ( fk_account = ").append(StringUtil.stringToNum(fkAccount));
            whereClause.append("   or fk_account is null ) ");
        } else {
            whereClause.append("  fk_account is null ");
        }
        whereClause.append(" and ");
        if (fkReviewBoard != null) {
            whereClause.append(" ( ','||fk_review_board||',' like '%,");
            whereClause.append(StringUtil.stringToNum(fkReviewBoard)).append(",%' ");
            whereClause.append("   or fk_review_board is null ) ");
        } else {
            whereClause.append("  fk_review_board is null ");
        }
        whereClause.append(" and ");
        if (reviewType != null) {
            whereClause.append(" ( ','||review_type||',' like '%,");
            whereClause.append(StringUtil.stringToNum(reviewType)).append(",%' ");
            whereClause.append("   or review_type is null ) ");
        } else {
            whereClause.append("  review_type is null ");
        }
        whereClause.append(" and ");
        if (submissionStatus != null) {
            whereClause.append(" ( ','||submission_status||',' like '%,");
            whereClause.append(StringUtil.stringToNum(submissionStatus)).append(",%' ");
            whereClause.append("   or submission_status is null ) ");
        } else {
            whereClause.append("  submission_status is null ");
        }
        int fkReport = -1;
        try {
            conn = getConnection();
            sqlBuffer.append(" select FK_REPORT from ER_REVIEW_PROVISO_MAP ");
            sqlBuffer.append(whereClause);
            sqlBuffer.append(" order by fk_account nulls last, fk_review_board nulls last, ");
            sqlBuffer.append(" review_type nulls last, submission_status nulls last ");
            pstmt = conn.prepareStatement(sqlBuffer.toString());
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                fkReport = rs.getInt(1);
                break;
            }
        } catch (SQLException ex) {
            Rlog.fatal("ProvisoMapDao","ProvisoMapDao.getFKReport"+ ex);
        } finally {
            try {
                if (pstmt != null) { pstmt.close(); }
            } catch (Exception e) {}
            try {
                if (conn != null) { conn.close(); }
            } catch (Exception e) {}
        }
        return fkReport;
    }
    
 public int getFKSubmissionStatus(int fk_submission,int fk_submission_board,int fk_submission_status) {
	    

	    PreparedStatement pstmt = null;
	    Connection conn = null;
	    
	    int pkStatus = 0;
	    
	    try {
	        conn = getConnection();
	        StringBuffer sql = new StringBuffer();
	        
	        sql.append("select max(PK_SUBMISSION_STATUS) as SUBMISSION_STATUS from er_submission_status  ");
	        sql.append(" where pk_submission_status < (select pk_submission_status from er_submission_status where pk_submission_status=?)");
	        sql.append("and fk_submission = ?  and fk_submission_board = ? ");
	        sql.append("and submission_status in(select pk_codelst from er_codelst where codelst_type = 'subm_status' and codelst_custom_col1 = 'overall,final')");
	        
	        pstmt = conn.prepareStatement(sql.toString());
	        pstmt.setInt(1, fk_submission_status);
	        pstmt.setInt(2, fk_submission);
	        pstmt.setInt(3,fk_submission_board);
	        //pstmt.setInt(1, submissionBoardPK);
	        
	        ResultSet rs = pstmt.executeQuery();
	        while (rs.next()) {
	       	 
	        	pkStatus = rs.getInt("SUBMISSION_STATUS");
	       	 
	       	 return pkStatus;
	       	 
	        }
	        
	    } catch(SQLException e) {
	        Rlog.fatal("EIRBDao", " error in getFKSubmissionStatus() "+e);
	    } finally {
	        try {
	            if (pstmt != null) pstmt.close();
	        } catch (Exception e) {}
	        try {
	            if (conn != null) conn.close();
	        } catch (Exception e) {}
	    
	        return pkStatus;
	    }
	}
public void updateblob(int pksubmission_status , byte[] buff){
	 
	 PreparedStatement pstmt = null;
	    Connection conn = null;
	    try{
	    	
	    	 conn = getConnection();
	    	 StringBuffer sql = new StringBuffer();
	    	 sql.append("update er_submission_status");
	    	 sql.append(" set FILE_DATA = ? , TEMPLATE_FLAG = 'Y'");
	    	 sql.append(" where  pk_submission_status = ?");
	    	 
	    	 pstmt = conn.prepareStatement(sql.toString());
	    	 //PreparedStatement prepareStatement = con.prepareStatement("INSERT INTO SMD_DATESTREEMAP VALUES(?,?)");
	    	 
	    	 pstmt.setBytes(1, buff);
	    	 pstmt.setInt(2, pksubmission_status);
	    	 pstmt.executeUpdate();
	    	 pstmt.close();
	    }
	    catch(SQLException e){
	    	 Rlog.fatal("EIRBDao", " error in updateblob() "+e);
	    }
	    finally {
	        try {
	            if (pstmt != null) pstmt.close();
	        } catch (Exception e) {}
	        try {
	            if (conn != null) conn.close();
	        } catch (Exception e) {}
	    
	    }
	 
 }
 public int gettemplateflag(int pk_submission_status){
	  int count = 0;
	  PreparedStatement pstmt = null;
	    Connection conn = null;
	    String TEMPLATEFLAG= "";
	    try{
	    	conn = getConnection();
	    	StringBuffer sql = new StringBuffer();
	    	sql.append("select TEMPLATE_FLAG from er_submission_status where pk_submission_status = ?  and TEMPLATE_FLAG = 'Y' ");
	    	//sql.append("");
	    	pstmt = conn.prepareStatement(sql.toString());
	    	pstmt.setInt(1, pk_submission_status);
	    	ResultSet rs = pstmt.executeQuery();
	        if(rs.next()) {
	       	 
	        	TEMPLATEFLAG = rs.getString("TEMPLATE_FLAG");
	       	  if(TEMPLATEFLAG.equals("Y")){
	       		count = 1; 
	       	  }
	       	  }
	       	 return count;
	       	 
	        
	    }
	    catch(SQLException e){
	    	Rlog.fatal("EIRBDao", " error in gettemplateflag() "+e);
	    }finally {
	 	        try {
	 	            if (pstmt != null) pstmt.close();
	 	        } catch (Exception e) {}
	 	        try {
	 	            if (conn != null) conn.close();
	 	        } catch (Exception e) {}
	    }
	    
	 

	    return count;

}
}