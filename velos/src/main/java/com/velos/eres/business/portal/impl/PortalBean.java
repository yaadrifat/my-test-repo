  /*
 * Classname			PortalBean.class
 * 
 * Version information
 *
 * Date					04/05/2007 
 * 
 * Copyright notice
 */

package com.velos.eres.business.portal.impl;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The Portal BMP entity bean.<br>
 * <br>
 * 
 * @author Manimaran
 * @version 1.0 04/05/2007
 */
@Entity
@Table(name = "er_portal")
@NamedQueries( 
		{
         @NamedQuery(name = "checkDuplicateName", query = "SELECT OBJECT(p) FROM PortalBean p where trim(lower(portal_name))= trim(lower(:pname)) and fk_account = :fkacc "),
         @NamedQuery(name = "checkCreateLoginFlag", query = "SELECT OBJECT(p) FROM PortalBean p where  fk_account = :fkacc and portal_createlogins = 1")
        }
      )
public class PortalBean implements Serializable {
   
    private static final long serialVersionUID = 3761410811205333553L;

    /**
     * Portal Id
     */
    public int portalId;

    /**
     * Portal Name
     */
    public String portalName;

    /**
     * Account Id
     */
    public Integer accountId;

    /**
     * Portal creator
     */
    public Integer portalCreatedBy;

    /**
     * Portal Description
     */
    public String portalDesc;

    /**
     * Portal Level
     */
    public String portalLevel;

    /**
     * Portal Notification Flag
     */
    public Integer portalNotificationFlag;

    /**
     * Portal Status
     */
    public String portalStatus;

    /**
     * Portal Display Type
     */
    public String portalDispType;

    /**
     * Portal Background Color
     */
    public String portalBgColor;

    /**
     * Portal Text Color
     */
    public String portalTxtColor;

    /**
     * Portal Header
     */
    public String portalHeader;

    /**
     * Portal Footer
     */
    public String portalFooter;

    /**
     * Portal Audit User
     */
    public Integer portalAuditUser;

    /**
     * the record creator
     */
    public Integer creator;

    /**
     * last modified by
     */
    public Integer modifiedBy;

    /**
     * the IP Address
     */
    public String ipAdd;

    
    /**
     * the study linked with the portal (if any)
     */
    public String portalStudy;
    
    /** 
     * Flag to indicate if user will be automatically logged out after answering any of portal forms.Possible values: 0:dont logout,1:Self logout
    */

    public String portalSelfLogout;
    
    /**
     *  Flag to indicate if logins will be created by default.Possible values: 0:no,1:yes
     * 
     */
    public String portalCreateLogins;
    
    /**
     *  Default password for all logins that will be created for this portal
     * 
     */
    public String portalDefaultPassword;
    
    /**
     *  Portal consenting form
     * 
     */
    public String portalConsentingForm;
    
    
    // GETTER SETTER METHODS

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "seq_er_portal", allocationSize=1)
    
    @Column(name = "PK_PORTAL")
    public int getPortalId() {
        return this.portalId;
    }

    public void setPortalId(int portalId) {
        this.portalId = portalId;
    }

    @Column(name = "PORTAL_NAME")
    public String getPortalName() {
        return this.portalName;
    }

    public void setPortalName(String portalName) {
        this.portalName = portalName;
    }
  

    @Column(name = "FK_ACCOUNT")
    public String getAccountId() {
    	return StringUtil.integerToString(this.accountId);
    }

    public void setAccountId(String accountId) {
    	if (accountId != null && accountId.trim().length() > 0)
            this.accountId = Integer.valueOf(accountId);
    }

    @Column(name = "PORTAL_CREATED_BY")
    public String getPortalCreatedBy() {
    	return StringUtil.integerToString(this.portalCreatedBy);
    }

    
    public void setPortalCreatedBy(String portalCreatedBy) {
        this.portalCreatedBy = Integer.valueOf(portalCreatedBy);
    }

    @Column(name = "PORTAL_DESC")
    public String getPortalDesc() {
        return this.portalDesc;
    }

    public void setPortalDesc(String portalDesc) {
        this.portalDesc = portalDesc;
    }

    
    @Column(name = "PORTAL_LEVEL")
    public String getPortalLevel() {
        return this.portalLevel;
    }

    public void setPortalLevel(String portalLevel) {
        this.portalLevel = portalLevel;
    }

    
    @Column(name = "PORTAL_NOTIFICATION_FLAG")
    public String getPortalNotificationFlag() {
    	return StringUtil.integerToString(this.portalNotificationFlag);
    }

    public void setPortalNotificationFlag(String portalNotificationFlag) {
    	this.portalNotificationFlag = Integer.valueOf(portalNotificationFlag);
    }

    @Column(name = "PORTAL_STATUS")
    public String getPortalStatus() {
        return this.portalStatus;
    }

    public void setPortalStatus(String portalStatus) {
        this.portalStatus = portalStatus;
    }
    
    @Column(name = "PORTAL_DISP_TYPE")
    public String getPortalDispType() {
        return this.portalDispType;
    }

    public void setPortalDispType(String portalDispType) {
        this.portalDispType = portalDispType;
    }
    
    @Column(name = "PORTAL_BGCOLOR")
    public String getPortalBgColor() {
        return this.portalBgColor;
    }

    public void setPortalBgColor(String portalBgColor) {
        this.portalBgColor = portalBgColor;
    }
    
    @Column(name = "PORTAL_TEXTCOLOR")
    public String getPortalTxtColor() {
        return this.portalTxtColor;
    }

    public void setPortalTxtColor(String portalTxtColor) {
        this.portalTxtColor = portalTxtColor;
    }
    
    @Column(name = "PORTAL_HEADER")
    public String getPortalHeader() {
        return this.portalHeader;
    }

    public void setPortalHeader(String portalHeader) {
        this.portalHeader = portalHeader;
    }

    @Column(name = "PORTAL_FOOTER")
    public String getPortalFooter() {
        return this.portalFooter;
    }

    public void setPortalFooter(String portalFooter) {
        this.portalFooter = portalFooter;
    }

    
    
    
    
    @Column(name = "PORTAL_AUDITUSER")
    public String getPortalAuditUser() {
        return StringUtil.integerToString(this.portalAuditUser);
    }

    public void setPortalAuditUser(String portalAuditUser) {
        if (portalAuditUser != null) {
            this.portalAuditUser = Integer.valueOf(portalAuditUser);
        }
    }
    
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    
    // END OF GETTER SETTER METHODS

    public int updatePortal(PortalBean apsk) {
        try {
        	setPortalId(apsk.getPortalId());
        	setPortalName(apsk.getPortalName());
        	setAccountId(apsk.getAccountId());
        	setPortalCreatedBy(apsk.getPortalCreatedBy());
        	setPortalDesc(apsk.getPortalDesc());
        	setPortalLevel(apsk.getPortalLevel());
        	setPortalNotificationFlag(apsk.getPortalNotificationFlag());
        	setPortalStatus(apsk.getPortalStatus());
        	setPortalDispType(apsk.getPortalDispType());
        	setPortalBgColor(apsk.getPortalBgColor());
        	setPortalTxtColor(apsk.getPortalTxtColor());
        	setPortalHeader(apsk.getPortalHeader());
        	setPortalFooter(apsk.getPortalFooter());
        	setPortalAuditUser(apsk.getPortalAuditUser());
        	setCreator(apsk.getCreator());
            setModifiedBy(apsk.getModifiedBy());
            setIpAdd(apsk.getIpAdd());
            setPortalStudy(apsk.getPortalStudy());
            setPortalSelfLogout(apsk.getPortalSelfLogout());
            
            setPortalCreateLogins(apsk.getPortalCreateLogins());
            setPortalDefaultPassword(apsk.getPortalDefaultPassword());
            setPortalConsentingForm(apsk.getPortalConsentingForm());
            
            Rlog.debug("portal", "PortalBean.updatePortal");
            return 0;
        } catch (Exception e) {
        	
            Rlog.fatal("portal", " error in PortalBean.updatePortal");
            return -2;
        }
    }

    public PortalBean() {

    }

    public PortalBean(int portalId, String portalName, String accountId,
            String portalCreatedBy, String portalDesc, String portalLevel, String portalNotificationFlag,
            String portalStatus, String portalDispType, String portalBgColor,
            String portalTxtColor, String portalHeader, String portalFooter, String portalAuditUser, String creator, String modifiedBy, String ipAdd,
            String study,String selflogout,String createlogins,String defpass,String form) {
        super();
        // TODO Auto-generated constructor stub
        setPortalId(portalId);
        setPortalName(portalName);
        setAccountId(accountId);
        setPortalCreatedBy(portalCreatedBy);
        setPortalDesc(portalDesc);
        setPortalLevel(portalLevel);
        setPortalNotificationFlag(portalNotificationFlag);
        setPortalStatus(portalStatus);
        setPortalDispType(portalDispType);
        setPortalBgColor(portalBgColor);
        setPortalTxtColor(portalTxtColor);
        setPortalHeader(portalHeader);
        setPortalFooter(portalFooter);
        setPortalAuditUser(portalAuditUser);             
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setPortalStudy(study);
        setPortalSelfLogout(selflogout);
        setPortalCreateLogins(createlogins);
        setPortalDefaultPassword(defpass);
        setPortalConsentingForm(form);
    }

    @Column(name = "FK_STUDY")
	public String getPortalStudy() {
		return portalStudy;
	}

	public void setPortalStudy(String portalStudy) {
		this.portalStudy = portalStudy;
	}

	@Column(name = "portal_selflogout")
	public String getPortalSelfLogout() {
		return portalSelfLogout;
	}

	public void setPortalSelfLogout(String portalSelfLogout) {
		this.portalSelfLogout = portalSelfLogout;
	}

	@Column(name = "portal_createlogins")
	public String getPortalCreateLogins() {
		return portalCreateLogins;
	}

	public void setPortalCreateLogins(String portalCreateLogins) {
		this.portalCreateLogins = portalCreateLogins;
	}

	
	
	@Column(name = "portal_defaultpass")
	public String getPortalDefaultPasswordPersist() {
		return portalDefaultPassword;
	}

	public void setPortalDefaultPasswordPersist(String portalDefaultPassword) {
		this.portalDefaultPassword = portalDefaultPassword;
		
	}
	
	@Transient
	public String getPortalDefaultPassword() {
		if (! StringUtil.isEmpty(this.portalDefaultPassword)) {
            return (this.portalDefaultPassword);
        } else
        {
            return "";
        }  
		
	}

	public void setPortalDefaultPassword(String portalDefaultPassword) {
		
		 if (! StringUtil.isEmpty(portalDefaultPassword) ) {
	            this.portalDefaultPassword = (portalDefaultPassword);
	        }
		 else
		 {
			 this.portalDefaultPassword = null;
		 }
			
		
	}

	@Column(name = "portal_consenting_form")
	public String getPortalConsentingForm() {
		return portalConsentingForm;
	}

	public void setPortalConsentingForm(String portalConsentingForm) {
		 
		if (StringUtil.isEmpty(portalConsentingForm))
		{
			this.portalConsentingForm = null;
		}
		else
		{
			this.portalConsentingForm = portalConsentingForm;
		}	
	}
	
	
	

}


