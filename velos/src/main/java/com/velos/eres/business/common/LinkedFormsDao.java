/*
 * Classname			LinkedFormsDao
 *
 * Version information 	1.0
 *
 * Date					07/28/2003
 *
 * Copyright notice		Velos, Inc.
 *r
 * Author 				Sonia Sahni
 * Modified by Vishal Abrol Date:081904 for bugzilla issue# 1521
 */

package com.velos.eres.business.common;

/* IMPORT STATEMENTS */

import java.sql.Array;
import java.sql.BatchUpdateException;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.service.util.LC;
import org.json.JSONObject;

/* END OF IMPORT STATEMENTS */

/**
 * LinkedFormsDao for getting 'Linked Forms' records based on various retrieval
 * criteria
 *
 * @author Sonia Sahni
 * @version : 1.0 07/28/2003
 */

public class LinkedFormsDao extends CommonDAO implements java.io.Serializable {

    /*
     * Array list for Linked Form ID (primary key)
     */
    private ArrayList LFId;

    /*
     * Array list for formId
     */
    private ArrayList formId;

    /*
     * Array list for catlib
     */
    private ArrayList catlib;

    /*
     * Array list for creator
     */

    private ArrayList creator;

    /*
     * Array list for created_on
     */

    private ArrayList createdon;

    /*
     * Array list for last_modified_by
     */

    private ArrayList modifiedby;

    /*
     * Array list for last_modified_on
     */

    private ArrayList modifiedon;

    /***************************************************************************
     * * for code list status ID's
     **************************************************************************/

    private ArrayList statCodeId;

    /*
     * Array list for formStatId
     */
    private ArrayList formStatId;

    /*
     * Array list for form display type
     */
    private ArrayList formDisplayType;

    /*
     * Array list for form display type Value
     */
    private ArrayList formLFDisplayType;

    /*
     * Array list for form entry characteristics
     */
    private ArrayList formEntryChar;

    /*
     * Array list for form study
     */
    private ArrayList formStudy;

    /*
     * Array list for 'Linked From' code
     */
    private ArrayList formLinkedFrom;

    /*
     * Array list for calendar
     */
    private ArrayList formCalendar;

    /*
     * Array list for event
     */
    private ArrayList formEvent;

    /*
     * Array list for CRF
     */

    private ArrayList formCRF;

    /*
     * Array list for form name
     */

    private ArrayList formName;

    /*
     * Array list for form saved entries
     */

    private ArrayList formSavedEntriesNum;

    /*
     * Array list for form last entry date
     */

    private ArrayList formLastEntryDate;

    /*
     * Array list for form description
     */

    private ArrayList formDescription;

    /*
     * Array list for form status
     */

    private ArrayList formStatus;

    /*
     * Array list for data count
     */

    private ArrayList lfDataCnt;

    /*
     * Array list for hide
     */

    private ArrayList lfHides;

    /*
     * Array list for display sequence
     */

    private ArrayList lfDispSeqs;

    /*
     * Array list for display in patient profile
     */

    private ArrayList lfDispPatProfiles;

    /*
     * Array list for display in sepecimen management
     */

    private ArrayList lfDispSepecimens;


    /*
     * Array list for catlib name
     */

    private ArrayList catlibName;

    /*
     * This stores the number of elements in the arraylists
     */

    private int cRows;

    /*
     * String arrays used while linking forms to study/account
     */
    private String[] saFormIds;

    private String[] saFormNames;

    private String[] saCatlibNames;

    private String[] saFormDescs;

    private String[] saDispLinks;

    private String[] saChars;

    private String[] saOrgs;

    // sonika pahse II
    private String[] saGrps;

    private String[] saStudyIds;

    private String accountId;

    private String userId;

    private String ipAdd;

    private String lnkFrom;

    /**
     * To get the comma separated names of the organizations corresponding to a
     * LinkedForm
     */

    private String orgNames;

    private ArrayList orgNamesArray;

    private String orgIds;

    private ArrayList orgIdsArray;
    
    private String oldOrgIds;

    /**
     * To get the comma separated names of the Groups corresponding to a
     * LinkedForm
     */

    private String grpNames;

    private ArrayList grpNamesArray;

    private String grpIds;

    private ArrayList grpIdsArray;
    
    private String oldGrpIds;
    
    private ArrayList arFormResponseDao;
    
    private ArrayList fkPerArray;

    public void setFormId(String[] saFormIds) {
        this.saFormIds = saFormIds;
    }

    public void setFormName(String[] saFormNames) {
        this.saFormNames = saFormNames;
    }

    public void setCatlibName(String[] saCatlibNames) {
        this.saCatlibNames = saCatlibNames;
    }

    public void setFormDescription(String[] saFormDescs) {
        this.saFormDescs = saFormDescs;
    }

    public void setStudyId(String[] saStudyIds) {
        this.saStudyIds = saStudyIds;
    }

    public void setFormDisplayType(String[] saDispLinks) {
        this.saDispLinks = saDispLinks;
    }

    public void setFormEntryChar(String[] saChars) {
        this.saChars = saChars;
    }

    public void setFormOrg(String[] saOrgs) {
        this.saOrgs = saOrgs;
    }

    public void setFormGrp(String[] saGrps) {
        this.saGrps = saGrps;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setLnkFrom(String lnkFrom) {
        this.lnkFrom = lnkFrom;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // / for getting the organization names and ids

    public ArrayList getOrgIdsArray() {
        return this.orgIdsArray;
    }

    public void setOrgIdsArray(ArrayList orgIdsArray) {
        this.orgIdsArray = orgIdsArray;
    }

    public void setOrgIdsArray(Integer orgIdsArray) {
        this.orgIdsArray.add(orgIdsArray);
    }

    public String getOrgNames() {
        return this.orgNames;
    }

    public void setOrgNames(String orgNames) {
        this.orgNames = orgNames;
    }

    public String getOrgIds() {
        return this.orgIds;
    }

    public void setOrgIds(String orgIds) {
        this.orgIds = orgIds;
    }

    public ArrayList getOrgNamesArray() {
        return this.orgNamesArray;
    }

    public void setOrgNamesArray(ArrayList orgNamesArray) {
        this.orgNamesArray = orgNamesArray;
    }

    public void setOrgNamesArray(String orgNamesArray) {
        this.orgNamesArray.add(orgNamesArray);
    }

    // / for getting the group names and ids

    public ArrayList getGrpIdsArray() {
        return this.grpIdsArray;
    }

    public void setGrpIdsArray(ArrayList grpIdsArray) {
        this.grpIdsArray = grpIdsArray;
    }

    public void setGrpIdsArray(Integer grpIdsArray) {
        this.grpIdsArray.add(grpIdsArray);
    }

    public String getGrpNames() {
        return this.grpNames;
    }

    public void setGrpNames(String grpNames) {
        this.grpNames = grpNames;
    }

    public String getGrpIds() {
        return this.grpIds;
    }

    public void setGrpIds(String grpIds) {
        this.grpIds = grpIds;
    }

    public ArrayList getGrpNamesArray() {
        return this.grpNamesArray;
    }

    public void setGrpNamesArray(ArrayList grpNamesArray) {
        this.grpNamesArray = grpNamesArray;
    }

    public void setGrpNamesArray(String grpNamesArray) {
        this.grpNamesArray.add(grpNamesArray);
    }

    public ArrayList getLfHides() {
        return this.lfHides;
    }

    public void setLfHides(Integer lfHide) {
        this.lfHides.add(lfHide);
    }

    public ArrayList getLfDispSeqs() {
        return this.lfDispSeqs;
    }

    public void setLfDispSeqs(Integer lfDispSeq) {
        this.lfDispSeqs.add(lfDispSeq);
    }

    public ArrayList getLfDispPatProfiles() {
        return this.lfDispPatProfiles;
    }

    public void setLfDispPatProfiles(Integer lfDispPatProfile) {
        this.lfDispPatProfiles.add(lfDispPatProfile);
    }
    
    public ArrayList getFkPerArray() {
    	return this.fkPerArray;
    }
    
    public void setFkPerArray(String fkPer) {
    	this.fkPerArray.add(fkPer);
    }

    /**
     * Defines a LinkedFormDao object with empty array lists
     */
    public LinkedFormsDao() {

        LFId = new ArrayList();
        formId = new ArrayList();
        catlib = new ArrayList();
        formStatId = new ArrayList();
        statCodeId = new ArrayList();
        formDisplayType = new ArrayList();
        formLFDisplayType = new ArrayList();
        formEntryChar = new ArrayList();
        formStudy = new ArrayList();
        formLinkedFrom = new ArrayList();
        formCalendar = new ArrayList();
        formEvent = new ArrayList();
        formCRF = new ArrayList();
        formName = new ArrayList();
        catlibName = new ArrayList();
        formSavedEntriesNum = new ArrayList();
        formLastEntryDate = new ArrayList();
        formDescription = new ArrayList();
        formStatus = new ArrayList();
        lfDataCnt = new ArrayList();
        lfDispSeqs = new ArrayList();
        lfHides = new ArrayList();
        lfDispPatProfiles = new ArrayList();
        orgNamesArray = new ArrayList();
        orgIds = null;
        orgIdsArray = new ArrayList();
        orgNames = null;

        grpNamesArray = new ArrayList();
        grpIds = null;
        grpIdsArray = new ArrayList();
        grpNames = null;
        // Added by gopu for Nov.2005 Enhancement #6
        creator = new ArrayList();
        createdon = new ArrayList();
        modifiedby = new ArrayList();
        modifiedon = new ArrayList();
        lfDispSepecimens = new ArrayList();
        arFormResponseDao= new ArrayList();
        fkPerArray = new ArrayList();
    }

    // Getter and Setter methods
    public ArrayList getFormCalendar() {
        return this.formCalendar;
    }

    public void setFormCalendar(ArrayList formCalendar) {
        this.formCalendar = formCalendar;
    }

    public ArrayList getFormCRF() {
        return this.formCRF;
    }

    public void setFormCRF(ArrayList formCRF) {
        this.formCRF = formCRF;
    }

    public ArrayList getFormDescription() {
        return this.formDescription;
    }

    public void setFormDescription(ArrayList formDescription) {
        this.formDescription = formDescription;
    }

    public ArrayList getFormDisplayType() {
        return this.formDisplayType;
    }

    public void setFormDisplayType(ArrayList formDisplayType) {
        this.formDisplayType = formDisplayType;
    }

    public ArrayList getFormLFDisplayType() {
        return this.formLFDisplayType;
    }

    public void setFormLFDisplayType(ArrayList formLFDisplayType) {
        this.formLFDisplayType = formLFDisplayType;
    }

    public ArrayList getFormStatus() {
        return this.formStatus;
    }

    public void setFormStatus(ArrayList formStatus) {
        this.formStatus = formStatus;
    }

    public ArrayList getLfDataCnt() {
        return this.lfDataCnt;
    }

    public void setLfDataCnt(ArrayList lfDataCnt) {
        this.lfDataCnt = lfDataCnt;
    }

    public void setFormStatus(String formStatus) {
        this.formStatus.add(formStatus);
    }

    // end by salil
    public ArrayList getFormEntryChar() {
        return this.formEntryChar;
    }

    public void setFormEntryChar(ArrayList formEntryChar) {
        this.formEntryChar = formEntryChar;
    }

    public ArrayList getFormEvent() {
        return this.formEvent;
    }

    public void setFormEvent(ArrayList formEvent) {
        this.formEvent = formEvent;
    }

    public ArrayList getFormId() {
        return this.formId;
    }
    public ArrayList getCatlib() {
        return this.catlib;
    }
    // Added by gopu for Nov.2005 Enhancement #6

    public ArrayList getCreator() {
        return this.creator;
    }

    public void setCreator(ArrayList creator) {
       this.creator = creator;
    }

    public void setCreator(String creatorStr){
    	this.creator.add(creatorStr);
    }

    public ArrayList getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(ArrayList createdon) {
       this.createdon = createdon;
    }

    public void setCreatedon(String createdonStr){
    	this.createdon.add(createdonStr);
    }

    public ArrayList getModifiedby() {
        return this.modifiedby;
    }

    public void setModifiedby(ArrayList modifiedby) {
       this.modifiedby = modifiedby;
    }

    public void setModifiedby(String modifiedbyStr){
    	this.modifiedby.add(modifiedbyStr);
    }

    public ArrayList getModifiedon() {
        return this.modifiedon;
    }

    public void setModifiedon(ArrayList modifiedon) {
       this.modifiedon = modifiedon;
    }

    public void setModifiedon(String modifiedonStr){
    	this.modifiedon.add(modifiedonStr);
    }


    //

    public void setFormId(ArrayList formId) {
        this.formId = formId;
    }
    public void setCatlib(ArrayList catlib) {
        this.catlib = catlib;
    }
    public ArrayList getStatCodeId() {
        return this.statCodeId;
    }

    public void setStatCodeId(ArrayList statCodeId) {
        this.statCodeId = statCodeId;
    }

    public ArrayList getFormStatId() {
        return this.formStatId;
    }

    public void setFormStatId(ArrayList formStatId) {
        this.formStatId = formStatId;
    }

    public ArrayList getFormLastEntryDate() {
        return this.formLastEntryDate;
    }

    public void setFormLastEntryDate(ArrayList formLastEntryDate) {
        this.formLastEntryDate = formLastEntryDate;
    }

    public ArrayList getFormLinkedFrom() {
        return this.formLinkedFrom;
    }

    public void setFormLinkedForm(ArrayList formLinkedFrom) {
        this.formLinkedFrom = formLinkedFrom;
    }

    public ArrayList getFormName() {
        return this.formName;
    }

    public void setFormName(ArrayList formName) {
        this.formName = formName;
    }

    public ArrayList getCatlibName() {
        return this.catlibName;
    }

    public void setCatlibName(ArrayList catlibName) {
        this.catlibName = catlibName;
    }

    public ArrayList getFormSavedEntriesNum() {
        return this.formSavedEntriesNum;
    }

    public void setFormSavedEntriesNum(ArrayList formSavedEntriesNum) {
        this.formSavedEntriesNum = formSavedEntriesNum;
    }

    public ArrayList getFormStudy() {
        return this.formStudy;
    }

    public void setFormStudy(ArrayList formStudy) {
        this.formStudy = formStudy;
    }

    public ArrayList getLFId() {
        return this.LFId;
    }

    public void setLFId(ArrayList LFId) {
        this.LFId = LFId;
    }

    public void setLFId(Integer LFId) {
        this.LFId.add(LFId);
    }

    public void setFormId(Integer formId) {
        this.formId.add(formId);
    }
    public void setCatlib(Integer catlib) {
        this.catlib.add(catlib);
    }
    public void setFormStatId(Integer formStatId) {
        this.formStatId.add(formStatId);
    }

    public void setLfDataCnt(Integer lfDataCnt) {
        this.lfDataCnt.add(lfDataCnt);
    }

    public void setStatCodeId(Integer statCodeId) {
        this.statCodeId.add(statCodeId);
    }

    public void setFormDisplayType(String formDisplayType) {
        this.formDisplayType.add(formDisplayType);
    }

    public void setFormLFDisplayType(String formLFDisplayType) {
        this.formLFDisplayType.add(formLFDisplayType);
    }

    public void setFormEntryChar(String formEntryChar) {
        this.formEntryChar.add(formEntryChar);
    }

    public void setFormStudy(Integer formStudy) {
        this.formStudy.add(formStudy);
    }

    public void setFormLinkedFrom(String formLinkedFrom) {
        this.formLinkedFrom.add(formLinkedFrom);
    }

    public void setFormCalendar(Integer formCalendar) {
        this.formCalendar.add(formCalendar);
    }

    public void setFormEvent(Integer formEvent) {
        this.formEvent.add(formEvent);
    }

    public void setFormCRF(Integer formCRF) {
        this.formCRF.add(formCRF);
    }

    public void setFormName(String formName) {
        this.formName.add(StringUtil.escapeSpecialCharHTML(formName));
    }

    public void setCatlibName(String catlibName) {
        this.catlibName.add(catlibName);
    }

    public void setFormSavedEntriesNum(Integer formSavedEntriesNum) {
        this.formSavedEntriesNum.add(formSavedEntriesNum);
    }

    public void setFormLastEntryDate(String formLastEntryDate) {
        this.formLastEntryDate.add(formLastEntryDate);
    }

    public void setFormDescription(String formDescription) {
        this.formDescription.add(formDescription);
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    // end of getter and setter methods

    /**
     * Gets the list of patient specific forms and respective number of times
     * they have been filled - Patient Level For displaying the Account forms
     * for data entry, the primary organization of the logged in user is matched
     * with the organizations selected in the filter and all the groups that the
     * user belongs to are matched with the selected filter groups. The hidden
     * forms are not displayed and forms are displayed as per the sequence
     * entered by user Forms which are linked to patients on all studies and
     * have display in patient profile flag set to 1 are also displayed
     * Retrieves Active as well as Lockdown Forms
     *
     * @param accountId
     *            The accountId for which the Forms are to be fetched
     * @param patient
     *            The patientId for which the Forms are to be fetched
     * @param userId
     *            The user for which the Forms are to be fetched
     * @param siteId
     *            The primary organization of loggedin user
     */

    public void getPatientForms(int accountId, int patient, int userId,
            int siteId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";

        try {
            conn = getConnection();


            sql = getPatientFormsSQL(null);
            //System.out.println(sql);

            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, patient);
            pstmt.setInt(2, patient);
            pstmt.setInt(3, accountId);
            pstmt.setInt(4, userId);
            pstmt.setInt(5, userId);
            pstmt.setInt(6, patient);
            pstmt.setInt(7, patient);
            pstmt.setInt(8, accountId);
            pstmt.setInt(9, userId);
            pstmt.setInt(10, userId);

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("lnkform", "after execute inside getPatientForms" + rs);

            while (rs.next()) {

                setLFId(new Integer(rs.getInt("PK_LF")));
                setFormId(new Integer(rs.getInt("FK_FORMLIB")));
                setFormEntryChar(rs.getString("LF_ENTRYCHAR"));
                setFormName(rs.getString("FORM_NAME"));
                setFormDescription(rs.getString("FORM_DESC"));
                setFormSavedEntriesNum(new Integer(rs.getInt("SAVEDCOUNT")));
                setFormDisplayType(rs.getString("LF_DISPLAYTYPE").trim());
                setFormLinkedFrom(rs.getString("LF_LNKFROM"));
                setLfDispPatProfiles(new Integer(rs.getInt("LF_DISPLAY_INPAT")));
                setLfDispSepecimens(rs.getString("lf_display_inspec"));

                rows++;

                Rlog.debug("lnkform", "LinkedFormsDao.getPatientForms " + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("lnkform", "LinkedFormsDao.getPatientForms EXCEPTION"
                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Gets the list of study specific forms for patient and respective number
     * of times they have been filled - Specific Study-Patient Level For
     * displaying the Account forms for data entry, the primary organization of
     * the logged in user is matched with the organizations selected in the
     * filter and all the groups that the user belongs to are matched with the
     * selected filter groups. The hidden forms are not displayed and forms are
     * displayed as per the sequence entered by user 'Patients on All Studies'
     * forms are displayed below the forms associated to "Patient Specific
     * Study' Retrieves Active and Lockdown forms
     *
     * @param accountId
     *            The accountId for which the Forms are to be fetched
     * @param patId
     *            The patient Id for which the Forms are to be fetched
     * @param studyId
     *            The Study for which Form is associated
     * @param userId
     *            The user for which the Forms are to be fetched
     * @param siteId
     *            The primary organization of loggedin user
     */

    public void getPatientStudyForms(int accountId, int patId, int studyId,
            int userId, int siteId) {
    	this.getPatientStudyForms(accountId, patId, studyId, userId, siteId, false);
    }
    
    public void getPatientStudyForms(int accountId, int patId, int studyId,
            int userId, int siteId, boolean includeHidden) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";

        try {
            conn = getConnection();
            
            Hashtable hashParams = new Hashtable();
            if (includeHidden) {
                hashParams.put("includeHidden", "Y");
            }
            sql = getPatientStudyFormsSQL(hashParams);

            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, patId);
            pstmt.setInt(2, accountId);
            pstmt.setInt(3, studyId);
            pstmt.setInt(4, userId);
            pstmt.setInt(5, userId);
            pstmt.setInt(6, studyId);
            pstmt.setInt(7, patId);
            pstmt.setInt(8, accountId);
            pstmt.setInt(9, studyId);
            pstmt.setInt(10, userId);
            pstmt.setInt(11, userId);
            pstmt.setInt(12, studyId);

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("lnkform", "after execute inside getPatientStudyForms"
                    + rs);

            while (rs.next()) {

                setLFId(new Integer(rs.getInt("PK_LF")));
                setFormId(new Integer(rs.getInt("FK_FORMLIB")));
                setFormDisplayType(rs.getString("LF_DISPLAYTYPE").trim());
                setFormEntryChar(rs.getString("LF_ENTRYCHAR"));
                setFormLinkedFrom(rs.getString("LF_LNKFROM"));
                setFormName(rs.getString("FORM_NAME"));
                setFormDescription(rs.getString("FORM_DESC"));
                setFormSavedEntriesNum(new Integer(rs.getInt("SAVEDCOUNT")));
                setLfDispSepecimens(rs.getString("lf_display_inspec"));

                rows++;

                Rlog.debug("lnkform", "LinkedFormsDao.getPatientStudyForms "
                        + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("lnkform",
                    "LinkedFormsDao.getPatientStudyForms EXCEPTION" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Gets the list of account specific forms and respective number of times
     * they have been filled - Account Level For displaying the Account forms
     * for data entry, the primary organization of the logged in user is matched
     * with the organizations selected in the filter and all the groups that the
     * user belongs to are matched with the selected filter groups. Retrieves
     * Active and Lockdown forms
     *
     * @param accountId
     *            The accountId for which the Forms are to be fetched
     * @param userId
     *            The user for which the Forms are to be fetched
     * @param siteId
     *            The primary organization of loggedin user
     */

    public void getAccountForms(int accountId, int userId, int siteId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        String sql = "";

        try {

            conn = getConnection();
            sql = getAccountFormsSQL(null);

            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, accountId);
            pstmt.setInt(2, accountId);
            pstmt.setInt(3, userId);
            pstmt.setInt(4, userId);
            pstmt.setInt(5, accountId);
            pstmt.setInt(6, accountId);
            pstmt.setInt(7, userId);
            pstmt.setInt(8, userId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setLFId(new Integer(rs.getInt("PK_LF")));
                setFormId(new Integer(rs.getInt("FK_FORMLIB")));
                setFormEntryChar(rs.getString("LF_ENTRYCHAR"));
                setFormName(rs.getString("FORM_NAME"));
                setFormDescription(rs.getString("FORM_DESC"));
                setFormSavedEntriesNum(new Integer(rs.getInt("SAVEDCOUNT")));
                setFormDisplayType(rs.getString("LF_DISPLAYTYPE").trim());
                setFormLinkedFrom(rs.getString("LF_LNKFROM"));
                setLfDispSepecimens(rs.getString("lf_display_inspec"));
                rows++;
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("lnkform", "LinkedFormsDao.getAccountForms EXCEPTION"
                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    public void getAccountGenericForms(int accountId, int userId, int siteId,String displayType) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        String sql = "";

        try {

            conn = getConnection();
            sql = getAccountGenFormsSQL(null,displayType);
            
            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, accountId);
            pstmt.setInt(2, accountId);
            pstmt.setInt(3, userId);
            pstmt.setInt(4, userId);
            pstmt.setInt(5, accountId);
            pstmt.setInt(6, accountId);
            pstmt.setInt(7, userId);
            pstmt.setInt(8, userId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setLFId(new Integer(rs.getInt("PK_LF")));
                setFormId(new Integer(rs.getInt("FK_FORMLIB")));
                setFormEntryChar(rs.getString("LF_ENTRYCHAR"));
                setFormName(rs.getString("FORM_NAME"));
                setFormDescription(rs.getString("FORM_DESC"));
                setFormSavedEntriesNum(new Integer(rs.getInt("SAVEDCOUNT")));
                setFormDisplayType(rs.getString("LF_DISPLAYTYPE").trim());
                setFormLinkedFrom(rs.getString("LF_LNKFROM"));
                setLfDispSepecimens(rs.getString("lf_display_inspec"));
                rows++;
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("lnkform", "LinkedFormsDao.getAccountForms EXCEPTION"
                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    public void getOrganisationForms(int accountId, int userId, int siteId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        String sql = "";

        try {

            conn = getConnection();
            sql = getOrganisationForms(null);

            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, accountId);
            pstmt.setInt(2, accountId);
            pstmt.setInt(3, userId);
            pstmt.setInt(4, userId);
            pstmt.setInt(5, accountId);
            pstmt.setInt(6, accountId);
            pstmt.setInt(7, userId);
            pstmt.setInt(8, userId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setLFId(new Integer(rs.getInt("PK_LF")));
                setFormId(new Integer(rs.getInt("FK_FORMLIB")));
                setFormEntryChar(rs.getString("LF_ENTRYCHAR"));
                setFormName(rs.getString("FORM_NAME"));
                setFormDescription(rs.getString("FORM_DESC"));
                setFormSavedEntriesNum(new Integer(rs.getInt("SAVEDCOUNT")));
                setFormDisplayType(rs.getString("LF_DISPLAYTYPE").trim());
                setFormLinkedFrom(rs.getString("LF_LNKFROM"));
                setLfDispSepecimens(rs.getString("lf_display_inspec"));
                rows++;
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("lnkform", "LinkedFormsDao.getAccountForms EXCEPTION"
                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    public void getUserForms(int accountId, int userId, int siteId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        String sql = "";

        try {

            conn = getConnection();
            sql = getUserForms(null);
            
            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, accountId);
            pstmt.setInt(2, accountId);
            pstmt.setInt(3, userId);
            pstmt.setInt(4, userId);
            pstmt.setInt(5, accountId);
            pstmt.setInt(6, accountId);
            pstmt.setInt(7, userId);
            pstmt.setInt(8, userId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setLFId(new Integer(rs.getInt("PK_LF")));
                setFormId(new Integer(rs.getInt("FK_FORMLIB")));
                setFormEntryChar(rs.getString("LF_ENTRYCHAR"));
                setFormName(rs.getString("FORM_NAME"));
                setFormDescription(rs.getString("FORM_DESC"));
                setFormSavedEntriesNum(new Integer(rs.getInt("SAVEDCOUNT")));
                setFormDisplayType(rs.getString("LF_DISPLAYTYPE").trim());
                setFormLinkedFrom(rs.getString("LF_LNKFROM"));
                setLfDispSepecimens(rs.getString("lf_display_inspec"));
                rows++;
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("lnkform", "LinkedFormsDao.getAccountForms EXCEPTION"
                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    public void getNetworkForms(int accountId, int userId, int siteId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        String sql = "";

        try {

            conn = getConnection();
            sql = getNetworkForms(null);

            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, accountId);
            pstmt.setInt(2, accountId);
            pstmt.setInt(3, userId);
            pstmt.setInt(4, userId);
            pstmt.setInt(5, accountId);
            pstmt.setInt(6, accountId);
            pstmt.setInt(7, userId);
            pstmt.setInt(8, userId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setLFId(new Integer(rs.getInt("PK_LF")));
                setFormId(new Integer(rs.getInt("FK_FORMLIB")));
                setFormEntryChar(rs.getString("LF_ENTRYCHAR"));
                setFormName(rs.getString("FORM_NAME"));
                setFormDescription(rs.getString("FORM_DESC"));
                setFormSavedEntriesNum(new Integer(rs.getInt("SAVEDCOUNT")));
                setFormDisplayType(rs.getString("LF_DISPLAYTYPE").trim());
                setFormLinkedFrom(rs.getString("LF_LNKFROM"));
                setLfDispSepecimens(rs.getString("lf_display_inspec"));
                rows++;
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("lnkform", "LinkedFormsDao.getAccountForms EXCEPTION"
                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    public void getSNetworkForms(int accountId, int userId, int siteId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        String sql = "";

        try {

            conn = getConnection();
            sql = getSNetworkForms(null);

            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, accountId);
            pstmt.setInt(2, accountId);
            pstmt.setInt(3, userId);
            pstmt.setInt(4, userId);
            pstmt.setInt(5, accountId);
            pstmt.setInt(6, accountId);
            pstmt.setInt(7, userId);
            pstmt.setInt(8, userId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setLFId(new Integer(rs.getInt("PK_LF")));
                setFormId(new Integer(rs.getInt("FK_FORMLIB")));
                setFormEntryChar(rs.getString("LF_ENTRYCHAR"));
                setFormName(rs.getString("FORM_NAME"));
                setFormDescription(rs.getString("FORM_DESC"));
                setFormSavedEntriesNum(new Integer(rs.getInt("SAVEDCOUNT")));
                setFormDisplayType(rs.getString("LF_DISPLAYTYPE").trim());
                setFormLinkedFrom(rs.getString("LF_LNKFROM"));
                setLfDispSepecimens(rs.getString("lf_display_inspec"));
                rows++;
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("lnkform", "LinkedFormsDao.getAccountForms EXCEPTION"
                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    public void getLinkedFormByFormId(int accountId, int formId, String type) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = null;

        try {
            conn = getConnection();
            StringBuffer sb = new StringBuffer();
            sb.append(" select * from ER_LINKEDFORMS where FK_ACCOUNT = ? and FK_FORMLIB = ? ");
            sql = sb.toString();
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, accountId);
            pstmt.setInt(2, formId);
            ResultSet rs = pstmt.executeQuery();
            
            while (rs.next()) {
                setLFId(new Integer(rs.getInt("PK_LF")));
                setFormId(new Integer(rs.getInt("FK_FORMLIB")));
                setFormDisplayType(rs.getString("LF_DISPLAYTYPE").trim());
                setFormEntryChar(rs.getString("LF_ENTRYCHAR"));
                setFormLinkedFrom(rs.getString("LF_LNKFROM"));
                setLfDispSepecimens(rs.getString("LF_DISPLAY_INSPEC"));
                rows++;
            }
            
            setCRows(rows);
        } catch(Exception e) {
            Rlog.fatal("lnkform", "LinkedFormsDao.getLinkedFormByFormId EXCEPTION:"+e);
        } finally {
        	try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
        	try { if (conn != null) { conn.close(); } } catch (Exception e) {}
        }
        
    }
    
    
    public void getFormResponseFkPer(int filledFormId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = null;
        try {
            conn = getConnection();
            StringBuffer sb = new StringBuffer();
            sb.append(" select FK_PER, CREATOR from ER_PATFORMS where PK_PATFORMS = ? ");
            sql = sb.toString();
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, filledFormId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	setFkPerArray(String.valueOf(rs.getInt("FK_PER")));
                setCreator(String.valueOf(rs.getInt("CREATOR")));
                rows++;
            }
            setCRows(rows);
        } catch(Exception e) {
            Rlog.fatal("lnkform", "LinkedFormsDao.getFormResponseFkPer EXCEPTION:"+e);
        } finally {
        	try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
        	try { if (conn != null) { conn.close(); } } catch (Exception e) {}
        }
    }
    
    public void getFormResponseCreator(int filledFormId, int formId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = null;
        try {
            conn = getConnection();
            StringBuffer sb = new StringBuffer();
            sb.append(" select CREATOR from er_formslinear where FK_FILLEDFORM = ? and FK_FORM = ? ");
            sql = sb.toString();
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, filledFormId);
            pstmt.setInt(2, formId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCreator(String.valueOf(rs.getInt("CREATOR")));
                rows++;
            }
            setCRows(rows);
        } catch(Exception e) {
            Rlog.fatal("lnkform", "LinkedFormsDao.getFormResponseCreator EXCEPTION:"+e);
        } finally {
        	try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
        	try { if (conn != null) { conn.close(); } } catch (Exception e) {}
        }
    }
    
    public int getUserAccess(int formId, int userId, int creator) {
    	int ret = 0;
    	PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn
                    .prepareCall("select PKG_FILLEDFORM.fn_getUserAccess(?,?,?) from dual");
            pstmt.setInt(1, formId);
            pstmt.setInt(2, userId);
            pstmt.setInt(3, creator);
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()) {
            	ret = rs.getInt(1);
            }
        } catch(Exception e) {
            Rlog.fatal("lnkform", "LinkedFormsDao.getUserAccess EXCEPTION:"+e);
        } finally {
        	try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
        	try { if (conn != null) { conn.close(); } } catch (Exception e) {}
        }
    	return ret;
    }



    /**
     * Gets the list of Filled study specific forms and respective number of
     * times they have been filled - Study Level/Specific Study Level For
     * displaying the Study forms for data entry, the primary organization of
     * the logged in user is matched with the organizations selected in the
     * filter and all the groups that the user belongs to are matched with the
     * selected filter groups.
     *
     * @param accountId
     *            The accountId for which the Forms are to be fetched
     * @param studyId
     *            The studyId for which the Forms are to be fetched
     * @param userId
     *            The user for which the Forms are to be fetched
     * @param siteId
     *            The primary organization of loggedin user
     */

    public void getStudyForms(int accountId, int studyId, int userId, int siteId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlBuf = new StringBuffer();

        try {
            conn = getConnection();

            // get only Active forms without adding any organization or group
            // filter
            if (userId == 0 && siteId == 0) {
                sqlBuf
                        .append(" SELECT  b.PK_LF as PK_LF,b.FK_FORMLIB as FK_FORMLIB , LF_DISPLAYTYPE,LF_ENTRYCHAR ,FK_STUDY , LF_LNKFROM , FK_CALENDAR, ");
                sqlBuf.append("	FK_EVENT , FK_CRF , FORM_NAME, FORM_DESC ,  ");
                sqlBuf
                        .append("	(SELECT COUNT(*) FROM ER_STUDYFORMS sf WHERE sf.FK_FORMLIB = a.FK_FORMLIB AND sf.FK_STUDY = ? and sf.RECORD_TYPE <> 'D') SAVEDCOUNT , ");
                sqlBuf
                        .append("	(SELECT to_char(MAX(STUDYFORMS_FILLDATE),PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_STUDYFORMS WHERE FK_FORMLIB = a.FK_FORMLIB AND FK_STUDY = ? ) SAVEDON ");
                sqlBuf
                        .append(" , lf_display_inspec FROM ER_LINKEDFORMS b, ER_FORMLIB ,er_formstat a ");
                sqlBuf
                        .append(" WHERE ER_FORMLIB.FK_ACCOUNT = ? AND a.fk_formlib = pk_formlib and ");
                sqlBuf.append(" b.FK_FORMLIB = PK_FORMLIB AND  FK_STUDY = ? ");
                sqlBuf.append(" and b.RECORD_TYPE <> 'D' ");
                sqlBuf.append(" and a.formstat_enddate IS NULL  ");
                sqlBuf
                        .append(" and (a.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) ");
                sqlBuf.append(" order by lower(ER_FORMLIB.form_name) ");

                Rlog.debug("lnkform", "after query inside getStudyForms"
                        + sqlBuf.toString());
                pstmt = conn.prepareStatement(sqlBuf.toString());

                pstmt.setInt(1, studyId);
                pstmt.setInt(2, studyId);
                pstmt.setInt(3, accountId);
                pstmt.setInt(4, studyId);
            } else {
                sqlBuf
                        .append(" SELECT  b.PK_LF as PK_LF,b.FK_FORMLIB as FK_FORMLIB , LF_DISPLAYTYPE,LF_ENTRYCHAR ,FK_STUDY , LF_LNKFROM , FK_CALENDAR, ");
                sqlBuf
                        .append("	FK_EVENT , FK_CRF , FORM_NAME, FORM_DESC , LOWER(FORM_NAME) FRMNAME,  ");
                sqlBuf
                        .append("	(SELECT COUNT(*) FROM ER_STUDYFORMS sf WHERE sf.FK_FORMLIB = a.FK_FORMLIB AND sf.FK_STUDY = ? and sf.RECORD_TYPE <> 'D') SAVEDCOUNT , ");
                sqlBuf
                        .append("	(SELECT to_char(MAX(STUDYFORMS_FILLDATE),PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_STUDYFORMS WHERE FK_FORMLIB = a.FK_FORMLIB AND FK_STUDY = ? ) SAVEDON ");
                sqlBuf
                        .append(" , lf_display_inspec FROM ER_LINKEDFORMS b, ER_FORMLIB ,er_formstat a, er_formorgacc d , (select distinct fk_formlib from er_usrgrp l ,er_formgrpacc m where fk_user=? and l.fk_grp = m.fk_group)  e  ");
                sqlBuf
                        .append(" WHERE ER_FORMLIB.FK_ACCOUNT = ? AND a.fk_formlib = pk_formlib and ");
                sqlBuf
                        .append(" b.FK_FORMLIB = PK_FORMLIB AND (( FK_STUDY = ? AND LF_DISPLAYTYPE = 'S' ) OR ");
                sqlBuf.append(" (LF_DISPLAYTYPE = 'SA' )) ");
                sqlBuf.append(" and b.RECORD_TYPE <> 'D' ");
                sqlBuf.append(" and a.formstat_enddate IS NULL  ");
                sqlBuf
                        .append(" and (a.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) ");
                sqlBuf
                        .append(" and b.fk_formlib = d.fk_formlib(+) and (d.fk_site = ? or d.fk_site is null) and pk_formlib= e.fk_formlib ");

                sqlBuf.append(" union ");

                sqlBuf
                        .append(" (SELECT  b.PK_LF as PK_LF,b.FK_FORMLIB as FK_FORMLIB , LF_DISPLAYTYPE,LF_ENTRYCHAR ,FK_STUDY , LF_LNKFROM , FK_CALENDAR, ");
                sqlBuf
                        .append("	FK_EVENT , FK_CRF , FORM_NAME, FORM_DESC , LOWER(FORM_NAME) FRMNAME, ");
                sqlBuf
                        .append("	(SELECT COUNT(*) FROM ER_STUDYFORMS sf WHERE sf.FK_FORMLIB = a.FK_FORMLIB AND sf.FK_STUDY = ? and sf.RECORD_TYPE <> 'D') SAVEDCOUNT , ");
                sqlBuf
                        .append("	(SELECT to_char(MAX(STUDYFORMS_FILLDATE),PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_STUDYFORMS WHERE FK_FORMLIB = a.FK_FORMLIB AND FK_STUDY = ? ) SAVEDON ");
                sqlBuf
                        .append(" , lf_display_inspec FROM ER_LINKEDFORMS b, ER_FORMLIB ,er_formstat a ");
                sqlBuf
                        .append(" WHERE ER_FORMLIB.FK_ACCOUNT = ? AND a.fk_formlib = pk_formlib and ");
                sqlBuf
                        .append(" b.FK_FORMLIB = PK_FORMLIB AND (( FK_STUDY = ? AND LF_DISPLAYTYPE = 'S' ) OR ");
                sqlBuf.append(" (LF_DISPLAYTYPE = 'SA' )) ");
                sqlBuf.append(" and b.RECORD_TYPE <> 'D' ");
                sqlBuf.append(" and a.formstat_enddate IS NULL  ");
                sqlBuf
                        .append(" and (a.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) ");
                sqlBuf
                        .append(" and not exists (select null from er_formgrpacc where pk_formlib = fk_formlib) )");

                sqlBuf.append(" order by FRMNAME ");

                Rlog.debug("lnkform", "after query inside getStudyForms"
                        + sqlBuf.toString());

                pstmt = conn.prepareStatement(sqlBuf.toString());

                pstmt.setInt(1, studyId);
                pstmt.setInt(2, studyId);
                pstmt.setInt(3, userId);
                pstmt.setInt(4, accountId);
                pstmt.setInt(5, studyId);
                pstmt.setInt(6, siteId);

                pstmt.setInt(7, studyId);
                pstmt.setInt(8, studyId);
                pstmt.setInt(9, accountId);
                pstmt.setInt(10, studyId);
            }

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("lnkform", "after execute inside getStudyForms" + rs);

            while (rs.next()) {

                setLFId(new Integer(rs.getInt("PK_LF")));
                setFormId(new Integer(rs.getInt("FK_FORMLIB")));
                setFormDisplayType(rs.getString("LF_DISPLAYTYPE").trim());
                setFormEntryChar(rs.getString("LF_ENTRYCHAR"));
                setFormStudy(new Integer(rs.getInt("FK_STUDY")));
                setFormLinkedFrom(rs.getString("LF_LNKFROM"));
                setFormCalendar(new Integer(rs.getInt("FK_CALENDAR")));
                setFormEvent(new Integer(rs.getInt("FK_EVENT")));
                setFormCRF(new Integer(rs.getInt("FK_CRF")));
                setFormName(rs.getString("FORM_NAME"));
                setFormDescription(rs.getString("FORM_DESC"));
                setFormSavedEntriesNum(new Integer(rs.getInt("SAVEDCOUNT")));
                setFormLastEntryDate(rs.getString("SAVEDON"));
                setLfDispSepecimens(rs.getString("lf_display_inspec"));

                rows++;

            }

            setCRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("lnkform", "LinkedFormsDao.getStudyForms EXCEPTION"
                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Overloaded method Gets the list of Filled study specific forms and
     * respective number of times they have been filled - Study Level/Specific
     * Study Level For displaying the Study forms for data entry, the primary
     * organization of the logged in user is matched with the organizations
     * selected in the filter and all the groups that the user belongs to are
     * matched with the selected filter groups. The hidden forms are not
     * displayed and forms are displayed as per the sequence entered by user The
     * study forms are displayed before All Study Forms Retrieves Active and
     * Lockdown forms
     *
     * @param accountId
     *            The accountId for which the Forms are to be fetched
     * @param studyId
     *            The studyId for which the Forms are to be fetched
     * @param userId
     *            The user for which the Forms are to be fetched
     * @param siteId
     *            The primary organization of loggedin user
     * @param grpRight
     *            The group right for All Study Forms access
     * @param stdRight
     *            The study team right for Study Forms access
     * @param isIrb
     *            true=only get IRB forms; false=get all forms
     * @param submissionType
     *            Submission type for IRB application (study) 
     * @param formSubtype
     *            form subtype for IRB application (study) 
     */

    public void getStudyForms(int accountId, int studyId, int userId,
            int siteId, int grpRight, int stdRight, 
            boolean isIrb, String submissionType, String formSubtype) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";

        try {
            conn = getConnection();
            
            Hashtable htParams = null;
            if (isIrb) {
                htParams = new Hashtable();
                htParams.put("IRB", "1");
            }
            if (submissionType != null && submissionType.length()>0) {
                if (htParams == null) {
                    htParams = new Hashtable();
                }
                htParams.put("sType", submissionType);
            }
            if (formSubtype != null && formSubtype.length()>0) {
                if (htParams == null) {
                    htParams = new Hashtable();
                }
                htParams.put("fSubtype", formSubtype);
            }
            
            if ( (formSubtype == null || StringUtil.isEmpty(formSubtype)) && isIrb == true )
            {
            	  htParams.put("getALLChecked", "true");
            	
            }

            sql = getStudyFormsSQL(stdRight,grpRight,htParams);
            //System.out.println("sql"+sql);
            
            pstmt = conn.prepareStatement(sql);

            if ((stdRight == 0) && (grpRight >= 4)) {
                pstmt.setInt(1, studyId);
                pstmt.setInt(2, accountId);
                pstmt.setInt(3, userId);
                pstmt.setInt(4, userId);
                pstmt.setInt(5, studyId);
                pstmt.setInt(6, studyId);
                pstmt.setInt(7, accountId);
                pstmt.setInt(8, userId);
                pstmt.setInt(9, userId);
                pstmt.setInt(10, studyId);
            } else {
                pstmt.setInt(1, studyId);
                pstmt.setInt(2, accountId);
                pstmt.setInt(3, studyId);
                pstmt.setInt(4, userId);
                pstmt.setInt(5, userId);
                pstmt.setInt(6, studyId);
                pstmt.setInt(7, studyId);
                pstmt.setInt(8, accountId);
                pstmt.setInt(9, studyId);
                pstmt.setInt(10, userId);
                pstmt.setInt(11, userId);
                pstmt.setInt(12, studyId);
            }

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("lnkform", "after execute inside getStudyForms" + rs);

            while (rs.next()) {

                setLFId(new Integer(rs.getInt("PK_LF")));
                setFormId(new Integer(rs.getInt("FK_FORMLIB")));
                setFormDisplayType(rs.getString("LF_DISPLAYTYPE").trim());
                setFormEntryChar(rs.getString("LF_ENTRYCHAR"));
                setFormLinkedFrom(rs.getString("LF_LNKFROM"));
                setFormName(rs.getString("FORM_NAME"));
                setFormDescription(rs.getString("FORM_DESC"));
                setFormSavedEntriesNum(new Integer(rs.getInt("SAVEDCOUNT")));
                setLfDispSepecimens(rs.getString("lf_display_inspec"));

                rows++;

            }

            setCRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("lnkform", "LinkedFormsDao.getStudyForms  EXCEPTION"
                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Method to get all the Forms in the Form Library and the Forms already
     * linked with a particular study
     *
     * @param accountId
     * @param userId
     * @param formName
     * @param formType
     * @param studyId
     */
    //Query modified by Manimaran for Enhancement F4.
    //"siteIdAcc" Parameter Added by Gopu to fix the bugzilla Issue #2406
    public void getFormListForLinkToStudy(int accountId, int userId,
            String formName, int formType, int studyId, String siteIdAcc) {

        int ret = 1;
        int rows = 0;
        PreparedStatement stmt = null;
        Connection conn = null;
        String sql = "";
        String str1, str2, str3, str4;
        String mainsql = "";
        try {
            conn = getConnection();
            Rlog.debug("lnkform", "In getFormListForLinkToStudy () ");
            if (studyId == 0) {
                str1 = " select pk_formlib, form_name, " + " cl.catlib_name, "
                	    + " NVL(form_desc,'-') form_desc, cd.codelst_desc as form_status "    //KM
                        + " from  er_formlib fl, er_catlib cl, er_codelst cd  "
                        + " where cd.pk_codelst=fl.form_status "
                        + " and cd.codelst_type = 'frmlibstat' "
                        //+ " and cd.codelst_subtyp = 'F' "
                        + " and fl.fk_account = " + accountId
                        + " and  fl.record_type <> 'D' "
                        + " and fl.fk_account=cl.fk_account "
                        + " and fl.fk_catlib = cl.pk_catlib  "
                        + " and cl.catlib_type='T' " + " and exists "
                        + " (select osh.fk_object "
                        + " from er_objectshare osh "
                        //+ " where osh.fk_objectshare_id = " + userId
                        + " where osh.fk_objectshare_id in (" + userId +","+ siteIdAcc +") "
                        //+ " and osh.objectshare_type = 'U' "
                        + " and osh.objectshare_type in ('U','O') "
                        + " and osh.object_number = 1"
                        + " and osh.fk_object = fl.pk_formlib ) ";

            } else

            {
                // For Bug Fix 1927
                /*
                 * str1 =" select pk_formlib, form_name, " + " cl.catlib_name," + "
                 * NVL(form_desc,'-') form_desc " + " from er_formlib fl,
                 * er_catlib cl, er_linkedforms lnkfrm " + " where fl.fk_account =
                 * "+accountId + " and fl.record_type <> 'D' " + " and
                 * lnkfrm.record_type <> 'D' " + " and
                 * fl.fk_account=cl.fk_account " + " and fl.fk_catlib =
                 * cl.pk_catlib " + " and cl.catlib_type='T' " + " and
                 * lnkfrm.fk_formlib = fl.pk_formlib " + " and lnkfrm.fk_study = " +
                 * studyId ;
                 */
                str1 = " select pk_formlib, form_name, "
                        + "(select cl.catlib_name from er_catlib cl "
                        + " where pk_catlib = fl.fk_catlib and cl.catlib_type='T' "
                        + " and fl.fk_account=cl.fk_account ) catlib_name, "
                        + " NVL(form_desc,'-') form_desc,(select codelst_desc from er_codelst where pk_codelst = fl.form_status ) as form_status from er_formlib fl, "
                        + " er_linkedforms lnkfrm " + " where fl.fk_account = "
                        + accountId + " and fl.record_type <> 'D' "
                        + " and lnkfrm.record_type <> 'D' "
                        + " and lnkfrm.fk_formlib = fl.pk_formlib "
                        + " and lnkfrm.fk_study = " + studyId;

            }

            if (formName.equals("")) {
                str2 = " ";
            }

            else {
                str2 = " and lower(fl.form_name) like lower('%" + formName.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' ";
            }

            if (formType == 0 || formType == -1) {
                str3 = " ";
            }

            else {
                str3 = " and fl.fk_catlib=" + formType;
            }

            Rlog.debug("lnkform", "SQL in getFormListForLinkToStudy " + str1
                    + str2 + str3);

            //System.out.println("SQL in getFormListForLinkToStudy " + str1    + str2 + str3);

            ResultSet rs = null;
            try {
                mainsql = str1 + str2 + str3;
                stmt = conn.prepareStatement(mainsql);
                // stmt.setInt(1,accountId);
                // stmt.setInt(2,userId);
                stmt.execute();
                rs = stmt.getResultSet();
            } catch (Exception ex) {
                Rlog.fatal("lnkform",
                        "Exception in getFormListForLinkToStudy in LinkedFormsDao"
                                + ex);
            }

            if (rs != null) {

                while (rs.next()) {
                    setFormId(new Integer(rs.getInt("PK_FORMLIB")));
                    setFormName(rs.getString("FORM_NAME"));
                    setFormDescription(rs.getString("FORM_DESC"));
                    setCatlibName(rs.getString("CATLIB_NAME"));
                    setFormStatus(rs.getString("FORM_STATUS"));//KM
                    rows++;
                }
                setCRows(rows);
            }
        } catch (Exception ex) {
            Rlog
                    .fatal("lnkform", "EXCEPTION IN getFormListForLinkToStudy"
                            + ex);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * This is a function that will take the study id and return the forms
     * linked with the study
     *
     * @param studyId
     */
    public void getStudyLinkedForms(int studyId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlBuf = new StringBuffer();

        try {
            conn = getConnection();
        // Query modified by gopu for Nov.2005 Enhancement #6
            sqlBuf
                    .append("	select case when(ab.lf_displaytype='S') then '"+LC.Std_Study+"' ");
            sqlBuf
                    .append("	when(ab.lf_displaytype='SP') then '"+LC.Pat_Patient+"' end as displaytype");
            sqlBuf
                    .append(" ,ab.lf_displaytype, nvl(ab.lf_hide,0) lf_hide, nvl(ab.lf_seq,0) lf_seq, ab.pk_lf,b.form_name,b.form_desc,b.fk_catlib,ab.lf_datacnt as lfDataCnt,ab.fk_formlib as");
            sqlBuf
                    .append(" formLibId,(select codelst_desc  from er_codelst where pk_codelst = e.fk_codelst_stat  )");
            sqlBuf
                    .append(" as form_status ,ab.lf_lnkfrom as linkfrom, e.pk_formstat as formStatId , e.fk_codelst_stat as  statCodeId, (select usr_firstname ||''||usr_lastname from er_user where pk_user=b.creator) as");
            sqlBuf
            		.append(" creator,to_char(b.created_on) as created_on,(select usr_firstname ||''||usr_lastname from er_user where pk_user=b.last_modified_by) as ");
            sqlBuf
    				.append(" last_modified_by,to_char(b.last_modified_date) as last_modified_date");

            sqlBuf
                    .append(" from er_linkedforms  ab , er_formlib  b,er_formStat e");
            sqlBuf.append("  where");
            sqlBuf.append("  ab.Fk_study= ? and");
            sqlBuf
                    .append("  ab.FK_FORMLIB=b.pk_formlib and e.FK_FORMLIB=b.pk_formlib and");
            sqlBuf
//          Query modified by Gopu to fix the bugzilla issue #2592
                    .append("  ab.record_type <> 'D'  and e.formstat_enddate IS NULL  and e.pk_formstat=(select max(pk_formstat) from er_formstat where fk_formlib=b.pk_formlib) ");
            sqlBuf.append(" order by lower(b.form_name) ");

            Rlog.debug("lnkform", "LinkedFormsDaoSQL##" + sqlBuf.toString());

            pstmt = conn.prepareStatement(sqlBuf.toString());

            pstmt.setInt(1, studyId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setLFId(new Integer(rs.getInt("PK_LF")));
                setCatlib(new Integer(rs.getInt("FK_CATLIB")));
                setFormId(new Integer(rs.getInt("formLibId")));
                setStatCodeId(new Integer(rs.getInt("statCodeId")));
                setFormStatId(new Integer(rs.getInt("formStatId")));
                setFormDisplayType(rs.getString("DISPLAYTYPE"));
                setFormLFDisplayType(rs.getString("LF_DISPLAYTYPE").trim());
                setFormName(rs.getString("FORM_NAME"));
                setFormDescription(rs.getString("FORM_DESC"));
                setFormStatus(rs.getString("FORM_STATUS"));
                setLfDataCnt(new Integer(rs.getInt("lfDataCnt")));
                setFormLinkedFrom(rs.getString("linkfrom"));
                setLfHides(new Integer(rs.getInt("lf_hide")));
                setLfDispSeqs(new Integer(rs.getInt("lf_seq")));
                // Added by gopu for Nov.2005 Enhancement #6
                setCreator(rs.getString("creator"));
                setCreatedon(rs.getString("created_on"));
                setModifiedby(rs.getString("last_modified_by"));
                setModifiedon(rs.getString("last_modified_date"));
                rows++;

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("lnkform",
                    "LinkedFormsDao.getStudyLinkedForms EXCEPTION" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * This method takes the study id and returns only the Active forms linked
     * with the study
     *
     * @param studyId
     */
    public void getStudyLinkedActiveForms(int studyId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlBuf = new StringBuffer();

        try {
            conn = getConnection();

            sqlBuf
                    .append("	select ab.lf_displaytype, nvl(ab.lf_hide,0) lf_hide, nvl(ab.lf_seq,0) lf_seq, nvl(ab.lf_display_inpat,0) lf_display_inpat, ab.pk_lf,b.form_name,b.form_desc ,ab.fk_formlib as formLibId, ab.lf_lnkfrom as linkfrom , lf_display_inspec ");
            sqlBuf
                    .append(" from er_linkedforms  ab , er_formlib  b,er_formstat e");
            sqlBuf.append(" where ab.Fk_study= ? and");
            sqlBuf
                    .append(" ab.FK_FORMLIB=b.pk_formlib and e.FK_FORMLIB=b.pk_formlib and");
            sqlBuf
                    .append(" ab.record_type <> 'D'  and e.formstat_enddate IS NULL ");
            sqlBuf
                    .append(" and (b.form_status = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat') or ");
            sqlBuf
                    .append(" b.form_status = (select pk_codelst from er_codelst where  codelst_subtyp='L' and codelst_type='frmstat')) ");
            sqlBuf.append(" order by lower(b.form_name) ");

            pstmt = conn.prepareStatement(sqlBuf.toString());

            pstmt.setInt(1, studyId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setLFId(new Integer(rs.getInt("PK_LF")));
                setFormId(new Integer(rs.getInt("formLibId")));
                setFormLFDisplayType(rs.getString("LF_DISPLAYTYPE").trim());
                setFormName(rs.getString("FORM_NAME"));
                setFormDescription(rs.getString("FORM_DESC"));
                setFormLinkedFrom(rs.getString("linkfrom"));
                setLfHides(new Integer(rs.getInt("lf_hide")));
                setLfDispSeqs(new Integer(rs.getInt("lf_seq")));
                setLfDispPatProfiles(new Integer(rs.getInt("lf_display_inpat")));
                setLfDispSepecimens(rs.getString("lf_display_inspec"));
                rows++;

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("lnkform",
                    "LinkedFormsDao.getStudyLinkedActiveForms EXCEPTION" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * This method takes the study id and returns only the Active forms linked
     * with the study
     *
     * @param studyId
     */
    public void getStudyLinkedActiveForms(int studyId, int accountId, int userId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";

        try {
            conn = getConnection();

            sql = "SELECT * FROM (SELECT  a.lf_displaytype, NVL(a.lf_hide,0) lf_hide, NVL(a.lf_seq,0) lf_seq "
                    + ", NVL(a.lf_display_inpat,0) lf_display_inpat, a.pk_lf,b.form_name ,b.form_desc ,a.fk_formlib AS formLibId "
                    + ", a.lf_lnkfrom AS linkfrom , lf_display_inspec FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,ER_FORMSTAT c "
                    + " WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB =PK_FORMLIB    AND c.FK_FORMLIB = PK_FORMLIB AND  "
                    +
                    // " (( FK_STUDY = ? AND LF_DISPLAYTYPE = 'S' ) OR
                    // (LF_DISPLAYTYPE = 'SA' OR LF_DISPLAYTYPE = 'PS')) "+
                    " ((LF_DISPLAYTYPE = 'SA'  OR  LF_DISPLAYTYPE = 'PS' OR LF_DISPLAYTYPE = 'PR' )) "
                    + "	AND  a.record_type <> 'D' AND NVL(a.lf_hide,0)=0  AND c.formstat_enddate IS NULL "
                    + "	AND ((c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE  codelst_subtyp='A' AND codelst_type='frmstat')) "
                    + " OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')) ) "
                    + " AND a.fk_formlib NOT IN (SELECT fk_formlib FROM ER_FORMORGACC  WHERE fk_site NOT IN "
                    + " (SELECT fk_site FROM ER_USERSITE WHERE fk_user=? AND usersite_right <> 0)) AND  a.fk_formlib NOT IN "
                    + " (SELECT fk_formlib FROM ER_FORMGRPACC WHERE fk_group NOT IN (SELECT fk_grp FROM ER_USRGRP WHERE fk_user=? )) "
                    + " UNION  (SELECT  a.lf_displaytype, NVL(a.lf_hide,0) lf_hide, NVL(a.lf_seq,0) lf_seq, NVL(a.lf_display_inpat,0) lf_display_inpat, a.pk_lf "
                    + " ,b.form_name ,b.form_desc ,a.fk_formlib AS formLibId , a.lf_lnkfrom AS linkfrom, lf_display_inspec  FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,ER_FORMSTAT c "
                    + "  WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB    AND c.FK_FORMLIB = PK_FORMLIB "
                    + "  AND  ( (LF_DISPLAYTYPE = 'SA' OR  LF_DISPLAYTYPE = 'PS' OR LF_DISPLAYTYPE = 'PR'))  "
                    + " AND  a.record_type <> 'D' AND NVL(a.lf_hide,0)=0 AND c.formstat_enddate IS NULL  AND "
                    + " ((c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE  codelst_subtyp='A' AND codelst_type='frmstat')) "
                    + " OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')) ) "
                    + " AND (a.fk_formlib IN (SELECT fk_formlib FROM ER_FORMGRPACC WHERE fk_group IN (SELECT fk_grp FROM ER_USRGRP WHERE fk_user=? )) "
                    + " OR a.fk_formlib IN (SELECT fk_formlib FROM ER_FORMORGACC WHERE fk_site IN (SELECT fk_site FROM ER_USERSITE WHERE fk_user=? AND usersite_right <> 0))) )  ) "
                    + "  ORDER BY LOWER(form_name) ";

            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, accountId);
            // pstmt.setInt(2,studyId);
            pstmt.setInt(2, userId);
            pstmt.setInt(3, userId);
            pstmt.setInt(4, accountId);
            // pstmt.setInt(6,studyId);
            pstmt.setInt(5, userId);
            pstmt.setInt(6, userId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setLFId(new Integer(rs.getInt("PK_LF")));
                setFormId(new Integer(rs.getInt("formLibId")));
                setFormLFDisplayType(rs.getString("LF_DISPLAYTYPE").trim());
                setFormName(rs.getString("FORM_NAME"));
                setFormDescription(rs.getString("FORM_DESC"));
                setFormLinkedFrom(rs.getString("linkfrom"));
                setLfHides(new Integer(rs.getInt("lf_hide")));
                setLfDispSeqs(new Integer(rs.getInt("lf_seq")));
                setLfDispPatProfiles(new Integer(rs.getInt("lf_display_inpat")));
                setLfDispSepecimens(rs.getString("lf_display_inspec"));
                rows++;

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("lnkform",
                    "LinkedFormsDao.getStudyLinkedActiveForms(3) EXCEPTION"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Method to get Active and Lockdown Forms linked with a particular Account
     *
     * @param accountId
     * @param linkedTo
     */
    public void getActiveFormsLinkedToAccount(int accountId, String linkedTo) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        String sqlstr = "select F1.PK_FORMLIB,F2.PK_LF,F1.FORM_NAME,F1.FORM_DESC, F2.LF_LNKFROM as linkfrom , "
                + " F2.lf_displaytype,nvl(F2.lf_hide,0) lf_hide, nvl(F2.lf_seq,0) lf_seq, nvl(F2.lf_display_inpat,0) lf_display_inpat,lf_display_inspec  "
                + " FROM ER_FORMLIB F1  , ER_LINKEDFORMS  F2 , er_formstat F4 "
                + " WHERE  F2.FK_ACCOUNT = "
                + accountId
                + " AND F2.FK_FORMLIB = F1.PK_FORMLIB "
                + " and F2.record_type <> 'D'   AND F4.FK_FORMLIB = F1.PK_FORMLIB "
                + " and F4.formstat_enddate IS NULL "
                + " and (F1.form_status = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat') or "
                + " F1.form_status = (select pk_codelst from er_codelst where  codelst_subtyp='L' and codelst_type='frmstat')) "
                + " and lf_displaytype IN ("
                + linkedTo
                + ")"
                + " order by lower(F1.form_name) ";

        try {
            conn = getConnection();
            Rlog.debug("lnkform",
                    "In getActiveFormsLinkedToAccount() in LinkedFormsDao sqlstr "
                            + sqlstr);

            ResultSet rs = null;
            pstmt = conn.prepareStatement(sqlstr);
            rs = pstmt.executeQuery();

            if (rs != null) {

                while (rs.next()) {
                    setFormId(new Integer(rs.getInt("PK_FORMLIB")));
                    setLFId(new Integer(rs.getInt("PK_LF")));
                    setFormName(rs.getString("FORM_NAME"));
                    setFormLinkedFrom(rs.getString("linkfrom"));
                    setFormDescription(rs.getString("FORM_DESC"));
                    setFormLFDisplayType(rs.getString("lf_displaytype").trim());
                    setLfHides(new Integer(rs.getInt("lf_hide")));
                    setLfDispSeqs(new Integer(rs.getInt("lf_seq")));
                    setLfDispPatProfiles(new Integer(rs
                            .getInt("lf_display_inpat")));

                    setLfDispSepecimens(rs.getString("lf_display_inspec"));
                    rows++;
                }
                setCRows(rows);
            }
        } catch (Exception ex) {
            Rlog.fatal("lnkform",
                    "EXCEPTION IN getActiveFormsLinkedToAccount in LinkedFormsDao"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Method to Link forms to a Study/Account multiple forms are linked to
     * study/account through a stored procedure All the form ids and respective
     * data is set in LinkedFormsDao attributes
     *
     * @returns -1 in case of error, 0 for success
     */

    public int linkFormsToStudyAccount(LinkedFormsDao linkedFormsDao) {
        int ret = 0;

        CallableStatement cstmt = null;
        Connection conn = null;

        String[] arForms = null;

        try {
            conn = getConnection();
            ArrayDescriptor formIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY formsArray = new ARRAY(formIds, conn,
                    linkedFormsDao.saFormIds);
            
            ArrayDescriptor formNames = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY formNamesArray = new ARRAY(formNames, conn,
                    linkedFormsDao.saFormNames);
            ArrayDescriptor formDescs = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY formDescArray = new ARRAY(formDescs, conn,
                    linkedFormsDao.saFormDescs);

            ArrayDescriptor dispLinks = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY dispLinkArray = new ARRAY(dispLinks, conn,
                    linkedFormsDao.saDispLinks);
            ArrayDescriptor studyIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            
            ARRAY studyIdArray = new ARRAY(studyIds, conn,
                    linkedFormsDao.saStudyIds);
            ArrayDescriptor chars = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY charsArray = new ARRAY(chars, conn, linkedFormsDao.saChars);
            ArrayDescriptor orgs = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY orgsArray = new ARRAY(orgs, conn, linkedFormsDao.saOrgs);

            Rlog.debug("lnkform", "In LinkedFormsDao sp_linkform saGrps "
                    + linkedFormsDao.saGrps[0]);

            ArrayDescriptor grps = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY grpsArray = new ARRAY(grps, conn, linkedFormsDao.saGrps);
            cstmt = conn
                    .prepareCall("{call PKG_LNKFORM.SP_LINKFORM(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

            cstmt.setString(1, linkedFormsDao.accountId);
            cstmt.setArray(2, studyIdArray);
            cstmt.setArray(3, formsArray);
            cstmt.setArray(4, formNamesArray);
            cstmt.setArray(5, formDescArray);
            cstmt.setArray(6, dispLinkArray);
            cstmt.setArray(7, charsArray);
            cstmt.setArray(8, orgsArray);
            cstmt.setArray(9, grpsArray);
            cstmt.setString(10, linkedFormsDao.lnkFrom);
            cstmt.setString(11, linkedFormsDao.userId);
            cstmt.setString(12, linkedFormsDao.ipAdd);
            cstmt.registerOutParameter(13, java.sql.Types.INTEGER);

            // Sonia Sahni, 05/28/04 for field actions
            cstmt
                    .registerOutParameter(14, java.sql.Types.ARRAY,
                            "ARRAY_STRING");
            //

            cstmt.execute();

            ret = cstmt.getInt(13);

            // Sonia Sahni, 05/28/04 for field actions
            Array outNewFormIdArray = cstmt.getArray(14);

            if (outNewFormIdArray != null) {
                String[] newFormIdArray = (String[]) outNewFormIdArray
                        .getArray();
                updateFormFieldActionsScript(newFormIdArray);

            }

            //

            Rlog.debug("lnkform", " after execute of sp_linkform ret" + ret);

            return ret;
        } catch (Exception ex) {
            Rlog.fatal("lnkform", "EXCEPTION IN linkFormsToStudyAccount" + ex);
            
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
    }

    /**
     * Method to get all the Forms in linked with a particular Account also
     * helps in searching the required form
     *
     * @param accountId
     * @param formName
     * @param linkedTo
     * @param siteId
     * @param studyId
     * @param GroupId
     */
    public void getFormsLinkedToAccount(int accountId, String formName,
            int studyId, String linkedTo, int siteId, int groupId) {
        int rows = 0;
        PreparedStatement stmt = null;
        Connection conn = null;

        String str = "select F1.PK_FORMLIB,F1.FK_CATLIB,F2.PK_LF,F1.FORM_NAME,F4.pk_formstat as formStatId,F1.FORM_DESC, F4.fk_codelst_stat as statCodeId , F2.LF_LNKFROM as linkfrom , F2.fk_study as formstudy , "
                + " F2.lf_datacnt as lfDataCnt,F2.lf_displaytype,nvl(F2.lf_hide,0) lf_hide, nvl(F2.lf_seq,0) lf_seq,"
                + " (select codelst_desc  from er_codelst where pk_codelst=F4.fk_codelst_stat) as form_status , "
                + " case when(F2.lf_displaytype='S') then 'Study'"
                + "  when(F2.lf_displaytype='A') then 'Account'"
                + "  when(F2.lf_displaytype='SA') then 'All Studies'"
                + "  when(F2.lf_displaytype='PA') then 'All Patients'"
                + "  when(F2.lf_displaytype='PS') then 'Patient (All Studies)'"
                + "  when(F2.lf_displaytype='PR') then 'Patient (All Studies - Restricted)'"
                + "  when(F2.lf_displaytype='SP') then 'Patient (Specific Study)' end as displaytype";
        String str1 = "";
        String str2 = "";
        String str3 = "";
        String str4 = "";
        String str5 = "";
        String str7 = "";
        String str6 = "";
        String str8 = "";
        String mainsql;
        try {
            conn = getConnection();
            Rlog.debug("lnkform", "In getFormsLinkedToAccount() ");
            if (siteId == 0) {
                str1 = "  FROM ER_FORMLIB F1  , ER_LINKEDFORMS  F2 , er_formstat F4 ";

            } else

            {
                str1 = "  FROM ER_FORMLIB F1  , ER_LINKEDFORMS  F2 ,er_formorgacc F3 , er_formstat F4 ";
                str5 = " and F3.FK_SITE=" + siteId;
                str4 = " and F3.FK_FORMLIB = F1.PK_FORMLIB ";
            }

            if (groupId > 0) {
                str1 = str1 + "  , ER_FORMGRPACC F5 ";
                str5 = str5 + " and F5.FK_GROUP=" + groupId;
                str4 = str4 + " and F5.FK_FORMLIB = F1.PK_FORMLIB ";
            }

            str2 = "  WHERE  F2.FK_ACCOUNT = "
                    + accountId
                    + " AND F2.FK_FORMLIB = F1.PK_FORMLIB"
                    + " and F2.record_type <> 'D'   AND F4.FK_FORMLIB = F1.PK_FORMLIB   and F4.formstat_enddate IS NULL";

            if (studyId == 0) {
                str6 = " ";
            } else {
                str6 = " and F2.FK_STUDY = " + studyId;
            }
            if (formName.equals("")) {
                str3 = " ";
            }

            else {
                str3 = " and lower(F1.form_name) like lower('%" + formName
                        + "%') ";
            }

            if (linkedTo.equals("")) {
                str7 = " ";
            }

            else {
                str7 = " and lf_displaytype IN ( " + linkedTo + " )";
            }

            str8 = " order by lower(F1.form_name) ";

            Rlog.debug("lnkform", "SQL in getFormsLinkedToAccount " + str
                    + str1 + str2 + str3 + str4 + str5 + str6 + str7 + str8);

            ResultSet rs = null;
            try {
                mainsql = str + str1 + str2 + str3 + str4 + str5 + str6 + str7
                        + str8;
                stmt = conn.prepareStatement(mainsql);
                stmt.execute();
                rs = stmt.getResultSet();
            } catch (Exception ex) {
                Rlog.fatal("lnkform",
                        "Exception in getFormsLinkedToAccount in LinkedFormsDao"
                                + ex);
            }

            if (rs != null) {

                while (rs.next()) {
                    setFormId(new Integer(rs.getInt("PK_FORMLIB")));
                    setCatlib(new Integer(rs.getInt("FK_CATLIB")));
                    setLFId(new Integer(rs.getInt("PK_LF")));
                    setFormStatId(new Integer(rs.getInt("formStatId")));
                    setStatCodeId(new Integer(rs.getInt("statCodeId")));
                    setFormName(rs.getString("FORM_NAME"));
                    setFormLinkedFrom(rs.getString("linkfrom"));
                    setFormStatus(rs.getString("form_status"));
                    setFormDescription(rs.getString("FORM_DESC"));
                    setFormStudy(new Integer(rs.getInt("formstudy")));
                    setFormDisplayType(rs.getString("displaytype"));
                    setFormLFDisplayType(rs.getString("lf_displaytype").trim());
                    setLfDataCnt(new Integer(rs.getInt("lfDataCnt")));
                    setLfHides(new Integer(rs.getInt("lf_hide")));
                    setLfDispSeqs(new Integer(rs.getInt("lf_seq")));
                    rows++;
                }
                setCRows(rows);
            }
        } catch (Exception ex) {
            Rlog.fatal("lnkform", "EXCEPTION IN getFormLinkedToAccount" + ex);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // ///////////////////////////////////////////////////
    public int copyFormsForStudy(String[] idArray, String formType,
            String studyId, String formName, String creator, String ipAdd) {

        int ret = 0;
        CallableStatement cstmt = null;
        Connection conn = null;
        String[] arForms = new String[1];

        Rlog
                .debug("lnkform",
                        "Inside the copyFormsForStudy in LinkedFormsDao ");

        try {
            conn = getConnection();

            ArrayDescriptor inIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inIdArray = new ARRAY(inIds, conn, idArray);

            Rlog
                    .fatal("lnkform",
                            "Inside the copyFormsForStudy , before procedure call in LinkedFormsDao ");
            cstmt = conn
                    .prepareCall("{call PKG_LNKFORM.SP_COPY_MULTIPLE_STUDY_FORMS(?,?,?,?,?,?,?)}");
            cstmt.setArray(1, inIdArray);
            cstmt.setInt(2, Integer.parseInt(formType));
            cstmt.setInt(3, Integer.parseInt(studyId));
            cstmt.setInt(4, Integer.parseInt(creator));
            cstmt.setString(5, formName);
            cstmt.setString(6, ipAdd);
            cstmt.registerOutParameter(7, java.sql.Types.INTEGER);

            cstmt.execute();
            Rlog.debug("lnkform",
                    "Inside the copyFormsForStudy , after procedure execute ");
            ret = cstmt.getInt(7);
            Rlog.debug("lnkform", "Inside the copyFormsForStudy, return value"
                    + ret);

            // Sonia Sahni, 05/28/04 for field actions

            arForms[0] = String.valueOf(ret);
            updateFormFieldActionsScript(arForms);

            return ret;
        } catch (Exception e) {
            e.printStackTrace();
            Rlog
                    .fatal(
                            "lnkform",
                            "EXCEPTION in LinkedFormsDao function copyFormsForStudy(), excecuting Stored Procedure "
                                    + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    // ////////////////////////////
    public int copyFormsForAccount(String[] idArray, String formType,
            String formName, String creator, String ipAdd) {

        int ret = 0;
        CallableStatement cstmt = null;
        Connection conn = null;
        String arForms[] = new String[1];

        Rlog.fatal("lnkform",
                "Inside the copyFormsForAccount in LinkedFormsDao ");

        try {
            conn = getConnection();

            ArrayDescriptor inIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inIdArray = new ARRAY(inIds, conn, idArray);

            Rlog
                    .debug("lnkform",
                            "Inside the copyFormsForAccount , before procedure call in LinkedFormsDao ");
            cstmt = conn
                    .prepareCall("{call PKG_LNKFORM.SP_COPY_MULTIPLE_ACCOUNT_FORMS(?,?,?,?,?,?)}");
            cstmt.setArray(1, inIdArray);
            cstmt.setInt(2, Integer.parseInt(formType));
            cstmt.setInt(3, Integer.parseInt(creator));
            cstmt.setString(4, formName);
            cstmt.setString(5, ipAdd);
            cstmt.registerOutParameter(6, java.sql.Types.INTEGER);

            cstmt.execute();
            Rlog
                    .debug("lnkform",
                            "Inside the copyFormsForAccount , after procedure execute ");
            ret = cstmt.getInt(6);
            Rlog.debug("lnkform",
                    "Inside the copyFormsForAccount, return value" + ret);

            // Sonia Sahni, 05/28/04 for field actions
            arForms[0] = String.valueOf(ret);
            updateFormFieldActionsScript(arForms);

            return ret;
        } catch (Exception e) {
            e.printStackTrace();
            Rlog
                    .fatal(
                            "lnkform",
                            "EXCEPTION in LinkedFormsDao function copyFormsForAccount(), excecuting Stored Procedure "
                                    + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    // ////////////
    public void getOrgNamesIds(int pkLf, int fkFormlib) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = null;
        Rlog.debug("lnkform", "LinkedFormsDao.getOrgNamesIds ");

        sql = "  select fk_site , site_name  from "
                + "  er_formorgacc  forg , er_site , er_linkedforms lf "
                + "  where pk_lf = ?" + "  and lf.fk_formlib = ?"
                + "  and forg.fk_site = pk_site "
                + "  and forg.fk_formlib = lf.fk_formlib ";

        try {
            conn = getConnection();

            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, pkLf);
            pstmt.setInt(2, fkFormlib);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setOrgIdsArray(new Integer(rs.getInt("fk_site")));
                setOrgNamesArray(rs.getString("site_name"));

                if (rows == 0) {
                    String tmp = rs.getString("site_name");

                    setOrgNames(tmp);
                    String tmp1 = String.valueOf(rs.getInt("fk_site"));

                    setOrgIds(tmp1);
                    rows++;
                } else {
                    setOrgNames(getOrgNames().concat(
                            "," + rs.getString("site_name")));
                    setOrgIds(getOrgIds().concat(
                            "," + String.valueOf(rs.getInt("fk_site"))));

                    rows++;
                }
                Rlog.debug("lnkform", "LinkedFormsDao.getOrgNamesIds " + rows);

            }

            setCRows(rows);
        } catch (Exception ex) {
            Rlog.fatal("lnkform",
                    "LinkedFormsDao.getOrgNamesIds EXCEPTION" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // ////////////////////

    public int updateOrgIds(int pkLf, String fkForm, String orgIds, String oldOrgIds, String userId, String ipAdd) {
    	int ret = 0, cnt = 0;
        CallableStatement cstmt = null;
        Connection conn = null;
        String[] idArray = null;
        String[] oldIdArray = null;
        StringTokenizer strToken;
        String tmp="";

        Rlog.debug("lnkform", "Inside the updateOrgIds in LinkedFormsDao ");
        if (oldOrgIds != null) {
            strToken = new StringTokenizer(oldOrgIds, ",");
            oldIdArray = new String[strToken.countTokens()];
            while (strToken.hasMoreTokens()) {
                tmp = strToken.nextToken();
                oldIdArray[cnt] = tmp;
                cnt = cnt + 1;

            }
        }
        cnt = 0;
        if (orgIds != null) {
           strToken = new StringTokenizer(orgIds, ",");
            idArray = new String[strToken.countTokens()];
            while (strToken.hasMoreTokens()) {
               tmp = strToken.nextToken();
                idArray[cnt] = tmp;
                cnt = cnt + 1;

            }
        }
        
        List<String> createIdList = new ArrayList(Arrays.asList(idArray));
        List<String> delIdList = new ArrayList();
        for (String s : oldIdArray) 
        {
            if (createIdList.contains(s)) 
            {
            	createIdList.remove(s);
            } 
            else
            {
            	delIdList.add(s);
            }
        }    
       idArray = createIdList.toArray(new String[createIdList.size()]);
       oldIdArray = delIdList.toArray(new String[delIdList.size()]);
        try {
            conn = getConnection();

            ArrayDescriptor inIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inIdArray = new ARRAY(inIds, conn, idArray);
            ArrayDescriptor delIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY delIdArray = new ARRAY(delIds, conn, oldIdArray);

            Rlog
                    .debug("lnkform",
                            "Inside the updateOrgIds , before procedure call in LinkedFormsDao ");
            cstmt = conn
                    .prepareCall("{call PKG_LNKFORM.SP_UPDATE_FORMORGACC(?,?,?,?,?,?,?)}");
            cstmt.setInt(1, pkLf);
            cstmt.setInt(2, Integer.parseInt(fkForm));
            cstmt.setInt(3,Integer.parseInt(userId));
            cstmt.setString(4, ipAdd);
            cstmt.setArray(5, inIdArray);
            cstmt.setArray(6, delIdArray);
            cstmt.registerOutParameter(7, java.sql.Types.INTEGER);

            cstmt.execute();

            ret = cstmt.getInt(7);

            return ret;
        } catch (Exception e) {
            e.printStackTrace();
            Rlog
                    .fatal(
                            "lnkform",
                            "EXCEPTION in LinkedFormsDao function updateOrgIds(), excecuting Stored Procedure "
                                    + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /**
     * Method to insert values for forms linked to CRF
     *
     * @return 0 for success, -1 for error
     */
    public int insertCrfForms(String[] formIds, String accId,
            String[] crfNames, String[] crfNumbers, String calId,
            String eventId, String user, String ipAdd) {

        int ret = 0, cnt = 0;
        CallableStatement cstmt = null;
        Connection conn = null;

        Rlog.debug("lnkform", "Inside the insertCrfForms in LinkedFormsDao ");

        try {
            conn = getConnection();

            ArrayDescriptor inFormIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY formIdsArray = new ARRAY(inFormIds, conn, formIds);

            ArrayDescriptor inCrfNames = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY crfNamesArray = new ARRAY(inCrfNames, conn, crfNames);

            ArrayDescriptor inCrfNumbers = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY crfNumbersArray = new ARRAY(inCrfNumbers, conn, crfNumbers);

            cstmt = conn
                    .prepareCall("{call PKG_LNKFORM.SP_LINK_CRFFORMS(?,?,?,?,?,?,?,?,?)}");
            cstmt.setArray(1, formIdsArray);
            cstmt.setString(2, accId);
            cstmt.setArray(3, crfNamesArray);
            cstmt.setArray(4, crfNumbersArray);
            cstmt.setInt(5, Integer.parseInt(calId));
            cstmt.setInt(6, Integer.parseInt(eventId));
            cstmt.setString(7, user);
            cstmt.setString(8, ipAdd);
            cstmt.registerOutParameter(9, java.sql.Types.INTEGER);

            cstmt.execute();

            ret = cstmt.getInt(9);
            Rlog.debug("lnkform", "Inside the insertCrfForms, return value"
                    + ret);

            return ret;
        } catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("lnkform",
                    "EXCEPTION in LinkedFormsDao insertCrforms(), excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /*
     * Method to get the Group Names and Ids of Linked Form
     */
    public void getGrpNamesIds(int pkLf, int fkFormlib) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = null;
        Rlog.debug("lnkform", "LinkedFormsDao.getGrpNamesIds ");

        sql = "  select fk_group , grp_name  from "
                + "  er_formgrpacc  fgrp , er_grps , er_linkedforms lf "
                + "  where pk_lf = ?" + "  and lf.fk_formlib = ?"
                + "  and fgrp.fk_group = pk_grp "
                + "  and fgrp.fk_formlib = lf.fk_formlib ";

        try {
            conn = getConnection();

            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, pkLf);
            pstmt.setInt(2, fkFormlib);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setGrpIdsArray(new Integer(rs.getInt("fk_group")));
                setGrpNamesArray(rs.getString("grp_name"));

                if (rows == 0) {
                    String tmp = rs.getString("grp_name");

                    setGrpNames(tmp);
                    String tmp1 = String.valueOf(rs.getInt("fk_group"));

                    setGrpIds(tmp1);
                    rows++;
                } else {
                    setGrpNames(getGrpNames().concat(
                            "," + rs.getString("grp_name")));
                    setGrpIds(getGrpIds().concat(
                            "," + String.valueOf(rs.getInt("fk_group"))));

                    rows++;
                }
                Rlog.debug("lnkform", "LinkedFormsDao.getGrpNamesIds rows"
                        + rows);

            }

            setCRows(rows);
        } catch (Exception ex) {
            Rlog.fatal("lnkform", "LinkedFormsDao.getGrpNamesIds EXCEPTION"
                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /*
     * Method to update the Group Names and Ids of Linked Form
     */

    public int updateGrpIds(int pkLf, String fkForm, String grpIds, String oldGrpIds, String userId, String ipAdd) {
        int ret = 0, cnt = 0;
        CallableStatement cstmt = null;
        Connection conn = null;
        String[] idArray = null;
        String[] oldIdArray = null;
        StringTokenizer strToken;
        String tmp="";

        Rlog.debug("lnkform", "Inside the updateGrpIds in LinkedFormsDao ");
        if (oldGrpIds != null) {
            strToken = new StringTokenizer(oldGrpIds, ",");
            oldIdArray = new String[strToken.countTokens()];
            while (strToken.hasMoreTokens()) {
                tmp = strToken.nextToken();
                oldIdArray[cnt] = tmp;
                cnt = cnt + 1;

            }
        }
        
        cnt=0;
        if (grpIds != null) {
            strToken = new StringTokenizer(grpIds, ",");
            idArray = new String[strToken.countTokens()];
            while (strToken.hasMoreTokens()) {
                tmp = strToken.nextToken();
                idArray[cnt] = tmp;
                cnt = cnt + 1;

            }
        }
        
        List<String> createIdList = new ArrayList(Arrays.asList(idArray));
        List<String> delIdList = new ArrayList();
        for (String s : oldIdArray) 
        {
            if (createIdList.contains(s)) 
            {
            	createIdList.remove(s);
            } 
            else
            {
            	delIdList.add(s);
            }
        }
        
        idArray = createIdList.toArray(new String[createIdList.size()]);
        oldIdArray = delIdList.toArray(new String[delIdList.size()]);
        
        try {
            conn = getConnection();

            ArrayDescriptor inIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inIdArray = new ARRAY(inIds, conn, idArray);
            ArrayDescriptor delIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY delIdArray = new ARRAY(delIds, conn, oldIdArray);

            cstmt = conn
                    .prepareCall("{call PKG_LNKFORM.SP_UPDATE_FORMGRPACC(?,?,?,?,?,?,?)}");
            cstmt.setInt(1, pkLf);
            cstmt.setInt(2, Integer.parseInt(fkForm));
            cstmt.setInt(3, Integer.parseInt(userId));
            cstmt.setString(4, ipAdd);
            cstmt.setArray(5, inIdArray);
            cstmt.setArray(6, delIdArray);
            cstmt.registerOutParameter(7, java.sql.Types.INTEGER);

            cstmt.execute();
            Rlog.debug("lnkform",
                    "Inside the updateGrpIds , after procedure execute ");
            ret = cstmt.getInt(7);
            Rlog
                    .debug("lnkform", "Inside the updateGrpIds, return value"
                            + ret);

            return ret;
        } catch (Exception e) {
            e.printStackTrace();
            Rlog
                    .fatal(
                            "lnkform",
                            "EXCEPTION in LinkedFormsDao function updateGrpIds(), excecuting Stored Procedure "
                                    + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /**
     * Method to update the hide, sequence fields and display in patient profile
     * of Linked Forms
     *
     * @param Array
     *            of pkLF
     * @param Array
     *            of form hide values
     * @param Array
     *            of form sequence
     * @param Array
     *            of display in patient profile
     * @param ipAdd
     * @param user
     */

    public int updateHideSeqOfLF(String[] pkLF, String[] hide, String[] seq,
            String[] patProfile, String ipAdd, int user, String[] dispInSpec)

    {

        PreparedStatement pstmtUpdateLF = null;
        Connection conn = null;
        int retVal = -1;
        int keyPlaceHolder = 0;

        int i = 0;

        try {

            conn = getConnection();
            int len = pkLF.length;

            //System.out.println("dispInSpec.length" + dispInSpec.length);

            if (dispInSpec.length > 0)
            {
            	pstmtUpdateLF = conn.prepareStatement(" update er_linkedforms "
                    + "  set LF_HIDE = ?, LF_SEQ = ?, LF_DISPLAY_INPAT = ?,"
                    + "  LAST_MODIFIED_BY = ?,IP_ADD = ? ,lf_display_inspec = ?"
                    + "  where PK_LF = ? ");

            	keyPlaceHolder  = 7;
            }
            else
            {
            	pstmtUpdateLF = conn.prepareStatement(" update er_linkedforms "
                        + "  set LF_HIDE = ?, LF_SEQ = ?, LF_DISPLAY_INPAT = ?,"
                        + "  LAST_MODIFIED_BY = ?,IP_ADD = ? "
                        + "  where PK_LF = ? ");

            	keyPlaceHolder  = 6;

            }



            for (i = 0; i < len; i++) {

                pstmtUpdateLF.setInt(1, StringUtil.stringToNum((String) hide[i]));

                pstmtUpdateLF.setInt(2, StringUtil.stringToNum((String) seq[i]));

                pstmtUpdateLF.setInt(3, StringUtil
                        .stringToNum((String) patProfile[i]));

                pstmtUpdateLF.setInt(4, user);

                pstmtUpdateLF.setString(5, ipAdd);

                if (dispInSpec.length > 0)
                {
                	pstmtUpdateLF.setString(6, dispInSpec[i]);
                }

                pstmtUpdateLF.setInt(keyPlaceHolder, StringUtil.stringToNum((String) pkLF[i]));

                Rlog.debug("lnkform",
                        "**StringUtil.stringToNum((String) patProfile[i])"
                                + StringUtil.stringToNum((String) patProfile[i]));
                pstmtUpdateLF.addBatch();

            }
            try {
                Rlog
                        .debug("lnkform",
                                "in updateHideSeqOfLF before executeBatch of LinkedFormsDao");
                int[] upDateCounts = pstmtUpdateLF.executeBatch();

                if (conn.getAutoCommit() == false)
                    conn.commit();

            } catch (BatchUpdateException batEx) {
                retVal = -1;
                Rlog
                        .fatal("lnkform",
                                "Exception in updateHideSeqOfLF in LinkedFormsDao "
                                        + batEx);
                batEx.printStackTrace();
                return retVal;
            }

        } catch (Exception ex) {
            Rlog.fatal("lnkform",
                    "Ex in updateHideSeqOfLF in LinkedFormsDao " + ex);
            ex.printStackTrace();

            return -1;
        } finally {
            try {
                if (pstmtUpdateLF != null)
                    pstmtUpdateLF.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();

            } catch (Exception e) {
            }
        }
        return 0;

    }

    /**
     * Method to delete Form versions
     *
     * @param formLibId
     */

    public void deleteFormVersions(String formLibId) {

        PreparedStatement pstmt = null;
        Connection conn = null;

        try {

            conn = getConnection();

            pstmt = conn
                    .prepareStatement("delete from er_formlibver where fk_formlib = ? ");

            Rlog.debug("lnkform", "in deleteFormVersions of LinkedFormsDao");

            pstmt.setInt(1, StringUtil.stringToNum(formLibId));

            ResultSet rs = pstmt.executeQuery();

        } catch (Exception ex) {
            Rlog.fatal("lnkform",
                    "Error in deleteFormVersions in LinkedFormsDao " + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();

            } catch (Exception e) {
            }
        }

    }

    /*
     * Modified by Sonia Abrol, 08/31/05, check the ctrl table setting for form
     * refresh, if form should not be refreshed, then do not generate interfield
     * action javascript
     */
    public void updateFormFieldActionsScript(String[] formIds) {
        /**
         * Sonia Sahni Date : 05/28/04 Generates Form Fields Action Script for
         * each form in formIds
         */
        String formId = "";
        FormLibDao fd = new FormLibDao();
        int formRefreshValue = 1;
        int saveFormatting = 0;

        //KM-#3780
        FieldActionDao fldActionDao = new FieldActionDao();
        if (formIds != null && formIds.length > 0) {
	        for (int i = 0; i < formIds.length; i++) {
	            formId = formIds[i];
	            saveFormatting = fd.getFormSaveFormat(Integer.parseInt(formId));
	            
	            if (saveFormatting == 1) {
	                formRefreshValue = 0;
	            } else {
	                formRefreshValue = 1;
	            }
	            
	            if (formRefreshValue == 1) {
	            	fldActionDao.generateFieldActionsScript(formId);
	            }
	        }
        }
    }

    
    //added by sonia Abrol, to check if user has right a form

    /** checks if user has access to the form (evenif the form is hidden)
     *
     * @param accountId
     *            The form Id
     * @param userId
     *            The user for which access rights are checked
     *
     */

    public static boolean hasFormAccess(int formId, int userId ){

        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlBuf = new StringBuffer();
        	int id;

        try {

            conn = getConnection();


            sqlBuf.append(" SELECT a.PK_LF  FROM ER_LINKEDFORMS  a, er_formstat c ");
           	sqlBuf.append(" WHERE a.FK_FORMLIB = ? ");
           	sqlBuf.append(" AND c.FK_FORMLIB = a.FK_FORMLIB   and  a.record_type <> 'D'  and c.formstat_enddate IS NULL  ");
           	sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or ");
           	sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) )");
           	sqlBuf.append(" and a.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user= ? and usersite_right <> 0)) and ");
           	sqlBuf.append(" a.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user= ?  ))");
           	sqlBuf.append(" union ");
           	sqlBuf.append(" SELECT a.PK_LF FROM ER_LINKEDFORMS  a, er_formstat c WHERE a.FK_FORMLIB = ? AND c.FK_FORMLIB = a.FK_FORMLIB   AND  ");
           	sqlBuf.append(" a.record_type <> 'D'  and c.formstat_enddate IS NULL ");
           	sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or");
           	sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) )");
           	sqlBuf.append(" and (a.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user =  ?    ))");
        	sqlBuf.append(" or a.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user= ?  and usersite_right <> 0))) ") ;

        	Rlog.debug("lnkform", "after query inside hasFormAccess" + sqlBuf.toString());

            pstmt = conn.prepareStatement(sqlBuf.toString());

            pstmt.setInt(1, formId);
            pstmt.setInt(2, userId);
            pstmt.setInt(3, userId);
            pstmt.setInt(4, formId);
            pstmt.setInt(5, userId);
            pstmt.setInt(6, userId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                id = rs.getInt(1);

                if (id > 0 )
                	return true;
                else
                	return false;
            }

            return false;

        } catch (SQLException ex) {
            Rlog.fatal("lnkform", "LinkedFormsDao.hasFormAccess  EXCEPTION"
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }


//  Added by Manimaran for the May-June requirement.
    /**
    * This method takes the study id and returns only the Active and Lockdown forms linked
    * with the study
    *
    * @param studyId
    */
   public void getStudyActiveLockdownForms(int studyId, int accountId) {
       int rows = 0;
       PreparedStatement pstmt = null;
       Connection conn = null;
       String sql = "";

       try {
           conn = getConnection();
           //Query modified by Manimaran to fix the Bug 2667.
           sql = "select pk_formlib,a.form_name,b.pk_lf,trim(b.lf_displaytype) As lf_displaytype from er_formlib a,er_linkedforms b where a.fk_account= ?  and  ( (b.fk_study = ?  and b.lf_displaytype = 'SP' and b.record_type <>'D') or ( b.lf_displaytype in ('PS','PR') and fk_study is null and b.record_type <>'D')) and a.pk_formlib=b.fk_formlib and a.form_status in (select pk_codelst from er_codelst where trim(codelst_type) = 'frmstat' and codelst_subtyp in ('A','L')) order by b.lf_displaytype desc, a.form_name ";
           pstmt = conn.prepareStatement(sql);
           pstmt.setInt(1, accountId);
           pstmt.setInt(2, studyId);

           ResultSet rs = pstmt.executeQuery();

           while (rs.next()) {
               setFormName(rs.getString("form_name"));
               setFormId(new Integer(rs.getInt("pk_formlib")));
               setFormLFDisplayType(rs.getString("lf_displaytype"));
               rows++;

           }
           setCRows(rows);
       } catch (SQLException ex) {
           Rlog.fatal("lnkform",
                   "LinkedFormsDao.getStudyActiveLockdownForms(3) EXCEPTION"
                           + ex);
       } finally {
           try {
               if (pstmt != null)
                   pstmt.close();
           } catch (Exception e) {
           }
           try {
               if (conn != null)
                   conn.close();
           } catch (Exception e) {
           }

       }

   }

   ///////////////////

// get latest linked form type
//	Added by Sonia Abrol 11/22/06
	public String getLinkedFormType(int form) {
		String fomType = "";
		String dispType = "";
       PreparedStatement stmt = null;
       Connection conn = null;
       String sql = "";
       try {

           conn = getConnection();
           sql = " select lf_displaytype from er_linkedforms where fk_formlib = ? ";

           ResultSet rs = null;
           try {

               stmt = conn.prepareStatement(sql);
               stmt.setInt(1, form);
               stmt.execute();

               rs = stmt.getResultSet();

           } catch (Exception ex) {
               Rlog.fatal("formlib", "Exception in FormLib.getLinkedFormType "
                       + ex);
           }

           if (rs != null) {

               while (rs.next()) {
            	   dispType = rs.getString("lf_displaytype");

               }

            if (! StringUtil.isEmpty(dispType))
            {
            	dispType = dispType.trim();
            }
            else
            {
            	dispType = "";
            }

           if (dispType.equals("S") || dispType.equals("SA"))
           {
        	   fomType = "S";
           }
           else if (dispType.equals("A") )
           {
        	   fomType = "A";
           }
           else
           {
        	   fomType = "P";
           }

           }

           return fomType;
       } catch (Exception ex) {
           Rlog.fatal("formlib",
                   "Exception in FormLib.getLinkedFormType EXCEPTION IN FETCHING FROM table"
                           + ex);
       } finally {
           try {
               if (stmt != null)
                   stmt.close();
           } catch (Exception e) {
           }
           try {
               if (conn != null)
                   conn.close();
           } catch (Exception e) {
           }
           return fomType;
       }
   }

	public ArrayList getLfDispSepecimens() {
		return lfDispSepecimens;
	}

	public void setLfDispSepecimens(ArrayList lfDispSepecimens) {
		this.lfDispSepecimens = lfDispSepecimens;
	}

	public void setLfDispSepecimens(String lfDispSepecimen) {
		this.lfDispSepecimens.add(lfDispSepecimen);
	}
	private String getAccountFormsSQL(Hashtable htParams)
	{
		StringBuffer sqlBuf = new StringBuffer();
		String specimenCheck = "";
		String specimenCheckVal = "";

		if (htParams != null)
		{
			if (htParams.containsKey("specimen"))
			{
				specimenCheckVal = (String) htParams.get("specimen");

				specimenCheck = " and nvl(lf_display_inspec,0) = " + specimenCheckVal;
			}
		}

		sqlBuf.append(" SELECT a.PK_LF as PK_LF ,a.FK_FORMLIB as FK_FORMLIB , LF_ENTRYCHAR, LF_DISPLAYTYPE,LF_LNKFROM, LF_SEQ,");
		sqlBuf.append(" FORM_NAME, FORM_DESC ,LOWER(FORM_NAME) FRMNAME,  ");
		sqlBuf.append(" (SELECT COUNT(*) FROM ER_ACCTFORMS af WHERE af.FK_FORMLIB = a.FK_FORMLIB AND af.FK_ACCOUNT = ? and af.RECORD_TYPE <> 'D') SAVEDCOUNT ");
		sqlBuf.append(" , lf_display_inspec FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c ");
		sqlBuf.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB   ");
		sqlBuf.append(" AND c.FK_FORMLIB = PK_FORMLIB   AND LF_DISPLAYTYPE = 'A' and  a.record_type <> 'D'  and nvl(a.lf_hide,0)=0 and c.formstat_enddate IS NULL  ");
		sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or ");
		sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) ) ");
		sqlBuf.append(" and a.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0)) and ");
		sqlBuf.append(" a.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user=? ))");

		sqlBuf.append(specimenCheck);

		sqlBuf.append(" union  ");
		sqlBuf.append(" SELECT a.PK_LF as PK_LF ,a.FK_FORMLIB as FK_FORMLIB , LF_ENTRYCHAR , LF_DISPLAYTYPE,LF_LNKFROM,LF_SEQ,");
		sqlBuf.append(" FORM_NAME, FORM_DESC ,LOWER(FORM_NAME) FRMNAME, ");
		sqlBuf.append(" (SELECT COUNT(*) FROM ER_ACCTFORMS ");
		sqlBuf.append(" WHERE FK_FORMLIB = a.FK_FORMLIB AND FK_ACCOUNT = ? and RECORD_TYPE <> 'D') SAVEDCOUNT      ");
		sqlBuf.append(", lf_display_inspec  FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c  ");
		sqlBuf.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB    AND c.FK_FORMLIB = PK_FORMLIB   AND LF_DISPLAYTYPE = 'A' and  a.record_type <> 'D' and nvl(a.lf_hide,0)=0 ");
		sqlBuf.append(" and c.formstat_enddate IS NULL ");
		sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or ");
		sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) ) ");
		sqlBuf.append(" and (a.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user=? ))");
		sqlBuf.append(" or a.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0))) ");

		sqlBuf.append(specimenCheck);

		sqlBuf.append(" order by LF_SEQ, FRMNAME ");

		return sqlBuf.toString();

	}
	private String getAccountGenFormsSQL(Hashtable htParams,String displayType)
	{
		StringBuffer sqlBuf = new StringBuffer();
		String specimenCheck = "";
		String specimenCheckVal = "";

		if (htParams != null)
		{
			if (htParams.containsKey("specimen"))
			{
				specimenCheckVal = (String) htParams.get("specimen");

				specimenCheck = " and nvl(lf_display_inspec,0) = " + specimenCheckVal;
			}
		}

		sqlBuf.append(" SELECT a.PK_LF as PK_LF ,a.FK_FORMLIB as FK_FORMLIB , LF_ENTRYCHAR, LF_DISPLAYTYPE,LF_LNKFROM, LF_SEQ,");
		sqlBuf.append(" FORM_NAME, FORM_DESC ,LOWER(FORM_NAME) FRMNAME,  ");
		sqlBuf.append(" (SELECT COUNT(*) FROM ER_ACCTFORMS af WHERE af.FK_FORMLIB = a.FK_FORMLIB AND af.FK_ACCOUNT = ? and af.RECORD_TYPE <> 'D') SAVEDCOUNT ");
		sqlBuf.append(" , lf_display_inspec FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c ");
		sqlBuf.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB   ");
		sqlBuf.append(" AND c.FK_FORMLIB = PK_FORMLIB   AND LF_DISPLAYTYPE = '"+displayType+"' and  a.record_type <> 'D'  and nvl(a.lf_hide,0)=0 and c.formstat_enddate IS NULL  ");
		sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or ");
		sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) ) ");
		sqlBuf.append(" and a.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0)) and ");
		sqlBuf.append(" a.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user=? ))");

		sqlBuf.append(specimenCheck);

		sqlBuf.append(" union  ");
		sqlBuf.append(" SELECT a.PK_LF as PK_LF ,a.FK_FORMLIB as FK_FORMLIB , LF_ENTRYCHAR , LF_DISPLAYTYPE,LF_LNKFROM,LF_SEQ,");
		sqlBuf.append(" FORM_NAME, FORM_DESC ,LOWER(FORM_NAME) FRMNAME, ");
		sqlBuf.append(" (SELECT COUNT(*) FROM ER_ACCTFORMS ");
		sqlBuf.append(" WHERE FK_FORMLIB = a.FK_FORMLIB AND FK_ACCOUNT = ? and RECORD_TYPE <> 'D') SAVEDCOUNT      ");
		sqlBuf.append(", lf_display_inspec  FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c  ");
		sqlBuf.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB    AND c.FK_FORMLIB = PK_FORMLIB   AND LF_DISPLAYTYPE = '"+displayType+"' and  a.record_type <> 'D' and nvl(a.lf_hide,0)=0 ");
		sqlBuf.append(" and c.formstat_enddate IS NULL ");
		sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or ");
		sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) ) ");
		sqlBuf.append(" and (a.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user=? ))");
		sqlBuf.append(" or a.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0))) ");

		sqlBuf.append(specimenCheck);

		sqlBuf.append(" order by LF_SEQ, FRMNAME ");

		return sqlBuf.toString();

	}
	private String getOrganisationForms(Hashtable htParams)
	{
		StringBuffer sqlBuf = new StringBuffer();
		String specimenCheck = "";
		String specimenCheckVal = "";

		if (htParams != null)
		{
			if (htParams.containsKey("specimen"))
			{
				specimenCheckVal = (String) htParams.get("specimen");

				specimenCheck = " and nvl(lf_display_inspec,0) = " + specimenCheckVal;
			}
		}

		sqlBuf.append(" SELECT a.PK_LF as PK_LF ,a.FK_FORMLIB as FK_FORMLIB , LF_ENTRYCHAR, LF_DISPLAYTYPE,LF_LNKFROM, LF_SEQ,");
		sqlBuf.append(" FORM_NAME, FORM_DESC ,LOWER(FORM_NAME) FRMNAME,  ");
		sqlBuf.append(" (SELECT COUNT(*) FROM ER_ACCTFORMS af WHERE af.FK_FORMLIB = a.FK_FORMLIB AND af.FK_ACCOUNT = ? and af.RECORD_TYPE <> 'D') SAVEDCOUNT ");
		sqlBuf.append(" , lf_display_inspec FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c ");
		sqlBuf.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB   ");
		sqlBuf.append(" AND c.FK_FORMLIB = PK_FORMLIB   AND LF_DISPLAYTYPE = 'ORG' and  a.record_type <> 'D'  and nvl(a.lf_hide,0)=0 and c.formstat_enddate IS NULL  ");
		sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or ");
		sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) ) ");
		sqlBuf.append(" and a.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0)) and ");
		sqlBuf.append(" a.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user=? ))");

		sqlBuf.append(specimenCheck);

		sqlBuf.append(" union  ");
		sqlBuf.append(" SELECT a.PK_LF as PK_LF ,a.FK_FORMLIB as FK_FORMLIB , LF_ENTRYCHAR , LF_DISPLAYTYPE,LF_LNKFROM,LF_SEQ,");
		sqlBuf.append(" FORM_NAME, FORM_DESC ,LOWER(FORM_NAME) FRMNAME, ");
		sqlBuf.append(" (SELECT COUNT(*) FROM ER_ACCTFORMS ");
		sqlBuf.append(" WHERE FK_FORMLIB = a.FK_FORMLIB AND FK_ACCOUNT = ? and RECORD_TYPE <> 'D') SAVEDCOUNT      ");
		sqlBuf.append(", lf_display_inspec  FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c  ");
		sqlBuf.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB    AND c.FK_FORMLIB = PK_FORMLIB   AND LF_DISPLAYTYPE = 'ORG' and  a.record_type <> 'D' and nvl(a.lf_hide,0)=0 ");
		sqlBuf.append(" and c.formstat_enddate IS NULL ");
		sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or ");
		sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) ) ");
		sqlBuf.append(" and (a.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user=? ))");
		sqlBuf.append(" or a.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0))) ");

		sqlBuf.append(specimenCheck);

		sqlBuf.append(" order by LF_SEQ, FRMNAME ");

		return sqlBuf.toString();

	}
	private String getUserForms(Hashtable htParams)
	{
		StringBuffer sqlBuf = new StringBuffer();
		String specimenCheck = "";
		String specimenCheckVal = "";

		if (htParams != null)
		{
			if (htParams.containsKey("specimen"))
			{
				specimenCheckVal = (String) htParams.get("specimen");

				specimenCheck = " and nvl(lf_display_inspec,0) = " + specimenCheckVal;
			}
		}

		sqlBuf.append(" SELECT a.PK_LF as PK_LF ,a.FK_FORMLIB as FK_FORMLIB , LF_ENTRYCHAR, LF_DISPLAYTYPE,LF_LNKFROM, LF_SEQ,");
		sqlBuf.append(" FORM_NAME, FORM_DESC ,LOWER(FORM_NAME) FRMNAME,  ");
		sqlBuf.append(" (SELECT COUNT(*) FROM ER_ACCTFORMS af WHERE af.FK_FORMLIB = a.FK_FORMLIB AND af.FK_ACCOUNT = ? and af.RECORD_TYPE <> 'D') SAVEDCOUNT ");
		sqlBuf.append(" , lf_display_inspec FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c ");
		sqlBuf.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB   ");
		sqlBuf.append(" AND c.FK_FORMLIB = PK_FORMLIB   AND LF_DISPLAYTYPE = 'USR' and  a.record_type <> 'D'  and nvl(a.lf_hide,0)=0 and c.formstat_enddate IS NULL  ");
		sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or ");
		sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) ) ");
		sqlBuf.append(" and a.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0)) and ");
		sqlBuf.append(" a.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user=? ))");

		sqlBuf.append(specimenCheck);

		sqlBuf.append(" union  ");
		sqlBuf.append(" SELECT a.PK_LF as PK_LF ,a.FK_FORMLIB as FK_FORMLIB , LF_ENTRYCHAR , LF_DISPLAYTYPE,LF_LNKFROM,LF_SEQ,");
		sqlBuf.append(" FORM_NAME, FORM_DESC ,LOWER(FORM_NAME) FRMNAME, ");
		sqlBuf.append(" (SELECT COUNT(*) FROM ER_ACCTFORMS ");
		sqlBuf.append(" WHERE FK_FORMLIB = a.FK_FORMLIB AND FK_ACCOUNT = ? and RECORD_TYPE <> 'D') SAVEDCOUNT      ");
		sqlBuf.append(", lf_display_inspec  FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c  ");
		sqlBuf.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB    AND c.FK_FORMLIB = PK_FORMLIB   AND LF_DISPLAYTYPE = 'USR' and  a.record_type <> 'D' and nvl(a.lf_hide,0)=0 ");
		sqlBuf.append(" and c.formstat_enddate IS NULL ");
		sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or ");
		sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) ) ");
		sqlBuf.append(" and (a.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user=? ))");
		sqlBuf.append(" or a.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0))) ");

		sqlBuf.append(specimenCheck);

		sqlBuf.append(" order by LF_SEQ, FRMNAME ");

		return sqlBuf.toString();

	}

	private String getNetworkForms(Hashtable htParams)
	{
		StringBuffer sqlBuf = new StringBuffer();
		String specimenCheck = "";
		String specimenCheckVal = "";

		if (htParams != null)
		{
			if (htParams.containsKey("specimen"))
			{
				specimenCheckVal = (String) htParams.get("specimen");

				specimenCheck = " and nvl(lf_display_inspec,0) = " + specimenCheckVal;
			}
		}

		sqlBuf.append(" SELECT a.PK_LF as PK_LF ,a.FK_FORMLIB as FK_FORMLIB , LF_ENTRYCHAR, LF_DISPLAYTYPE,LF_LNKFROM, LF_SEQ,");
		sqlBuf.append(" FORM_NAME, FORM_DESC ,LOWER(FORM_NAME) FRMNAME,  ");
		sqlBuf.append(" (SELECT COUNT(*) FROM ER_ACCTFORMS af WHERE af.FK_FORMLIB = a.FK_FORMLIB AND af.FK_ACCOUNT = ? and af.RECORD_TYPE <> 'D') SAVEDCOUNT ");
		sqlBuf.append(" , lf_display_inspec FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c ");
		sqlBuf.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB   ");
		sqlBuf.append(" AND c.FK_FORMLIB = PK_FORMLIB   AND LF_DISPLAYTYPE = 'NTW' and  a.record_type <> 'D'  and nvl(a.lf_hide,0)=0 and c.formstat_enddate IS NULL  ");
		sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or ");
		sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) ) ");
		sqlBuf.append(" and a.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0)) and ");
		sqlBuf.append(" a.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user=? ))");

		sqlBuf.append(specimenCheck);

		sqlBuf.append(" union  ");
		sqlBuf.append(" SELECT a.PK_LF as PK_LF ,a.FK_FORMLIB as FK_FORMLIB , LF_ENTRYCHAR , LF_DISPLAYTYPE,LF_LNKFROM,LF_SEQ,");
		sqlBuf.append(" FORM_NAME, FORM_DESC ,LOWER(FORM_NAME) FRMNAME, ");
		sqlBuf.append(" (SELECT COUNT(*) FROM ER_ACCTFORMS ");
		sqlBuf.append(" WHERE FK_FORMLIB = a.FK_FORMLIB AND FK_ACCOUNT = ? and RECORD_TYPE <> 'D') SAVEDCOUNT      ");
		sqlBuf.append(", lf_display_inspec  FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c  ");
		sqlBuf.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB    AND c.FK_FORMLIB = PK_FORMLIB   AND LF_DISPLAYTYPE = 'NTW' and  a.record_type <> 'D' and nvl(a.lf_hide,0)=0 ");
		sqlBuf.append(" and c.formstat_enddate IS NULL ");
		sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or ");
		sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) ) ");
		sqlBuf.append(" and (a.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user=? ))");
		sqlBuf.append(" or a.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0))) ");

		sqlBuf.append(specimenCheck);

		sqlBuf.append(" order by LF_SEQ, FRMNAME ");

		return sqlBuf.toString();

	}
	private String getSNetworkForms(Hashtable htParams)
	{
		StringBuffer sqlBuf = new StringBuffer();
		String specimenCheck = "";
		String specimenCheckVal = "";

		if (htParams != null)
		{
			if (htParams.containsKey("specimen"))
			{
				specimenCheckVal = (String) htParams.get("specimen");

				specimenCheck = " and nvl(lf_display_inspec,0) = " + specimenCheckVal;
			}
		}

		sqlBuf.append(" SELECT a.PK_LF as PK_LF ,a.FK_FORMLIB as FK_FORMLIB , LF_ENTRYCHAR, LF_DISPLAYTYPE,LF_LNKFROM, LF_SEQ,");
		sqlBuf.append(" FORM_NAME, FORM_DESC ,LOWER(FORM_NAME) FRMNAME,  ");
		sqlBuf.append(" (SELECT COUNT(*) FROM ER_ACCTFORMS af WHERE af.FK_FORMLIB = a.FK_FORMLIB AND af.FK_ACCOUNT = ? and af.RECORD_TYPE <> 'D') SAVEDCOUNT ");
		sqlBuf.append(" , lf_display_inspec FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c ");
		sqlBuf.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB   ");
		sqlBuf.append(" AND c.FK_FORMLIB = PK_FORMLIB   AND LF_DISPLAYTYPE = 'SNW' and  a.record_type <> 'D'  and nvl(a.lf_hide,0)=0 and c.formstat_enddate IS NULL  ");
		sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or ");
		sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) ) ");
		sqlBuf.append(" and a.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0)) and ");
		sqlBuf.append(" a.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user=? ))");

		sqlBuf.append(specimenCheck);

		sqlBuf.append(" union  ");
		sqlBuf.append(" SELECT a.PK_LF as PK_LF ,a.FK_FORMLIB as FK_FORMLIB , LF_ENTRYCHAR , LF_DISPLAYTYPE,LF_LNKFROM,LF_SEQ,");
		sqlBuf.append(" FORM_NAME, FORM_DESC ,LOWER(FORM_NAME) FRMNAME, ");
		sqlBuf.append(" (SELECT COUNT(*) FROM ER_ACCTFORMS ");
		sqlBuf.append(" WHERE FK_FORMLIB = a.FK_FORMLIB AND FK_ACCOUNT = ? and RECORD_TYPE <> 'D') SAVEDCOUNT      ");
		sqlBuf.append(", lf_display_inspec  FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c  ");
		sqlBuf.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB    AND c.FK_FORMLIB = PK_FORMLIB   AND LF_DISPLAYTYPE = 'SNW' and  a.record_type <> 'D' and nvl(a.lf_hide,0)=0 ");
		sqlBuf.append(" and c.formstat_enddate IS NULL ");
		sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or ");
		sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) ) ");
		sqlBuf.append(" and (a.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user=? ))");
		sqlBuf.append(" or a.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0))) ");

		sqlBuf.append(specimenCheck);

		sqlBuf.append(" order by LF_SEQ, FRMNAME ");

		return sqlBuf.toString();

	}

	
    public void getAccountFormsForSpecimen(int accountId, int userId, int siteId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        String sql = "";
        Hashtable htParam = new Hashtable();

        try {

        	htParam.put("specimen","1");

            conn = getConnection();
            sql = getAccountFormsSQL(htParam);

            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, accountId);
            pstmt.setInt(2, accountId);
            pstmt.setInt(3, userId);
            pstmt.setInt(4, userId);
            pstmt.setInt(5, accountId);
            pstmt.setInt(6, accountId);
            pstmt.setInt(7, userId);
            pstmt.setInt(8, userId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setLFId(new Integer(rs.getInt("PK_LF")));
                setFormId(new Integer(rs.getInt("FK_FORMLIB")));
                setFormEntryChar(rs.getString("LF_ENTRYCHAR"));
                setFormName(rs.getString("FORM_NAME"));
                setFormDescription(rs.getString("FORM_DESC"));
                setFormSavedEntriesNum(new Integer(rs.getInt("SAVEDCOUNT")));
                setFormDisplayType(rs.getString("LF_DISPLAYTYPE").trim());
                setFormLinkedFrom(rs.getString("LF_LNKFROM"));
                setLfDispSepecimens(rs.getString("lf_display_inspec"));
                rows++;
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("lnkform", "LinkedFormsDao.getAccountForms EXCEPTION"
                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    private String getStudyFormsSQL(int stdRight,int grpRight,Hashtable htParams)
    {
    	StringBuffer sqlBuf = new StringBuffer();

    	String specimenCheck = "";
		String specimenCheckVal = "";
		boolean fSubtypeExists = false;
		
		String catlibsubtyp="";
		
		catlibsubtyp = EIRBDao.getIRBFormCatlibSubtype();

		if (htParams != null)
		{
			if (htParams.containsKey("specimen"))
			{
				specimenCheckVal = (String) htParams.get("specimen");

				specimenCheck = " and nvl(lf_display_inspec,0) = " + specimenCheckVal;
			}
            if (htParams.containsKey("IRB"))
            {
                specimenCheck += " and nvl(lf_isirb,0) = 1 ";
            }
            if (htParams.containsKey("sType"))
            {
                String stype = "";
                
                stype = (String)htParams.get("sType");
                
                if (!stype.contains(","))//','|| esb.submission_reviewer || ',' like '%," + user+ ",%'
                {
                	specimenCheck += " and  ',' || lower(lf_submission_type)|| ',' like lower ('%," + stype+",%') ";
                } else
                {
                	if (stype.contains("lf_submission_type")){
                	specimenCheck += " and ("+stype+") ";
                	}else{
                		specimenCheck += " and lower(lf_submission_type) in ("+stype+") ";
                	}
                	
                }
                
            }
            if (htParams.containsKey("fSubtype"))
            {
                fSubtypeExists = true;
                specimenCheck += " and a.FK_FORMLIB = b.PK_FORMLIB and b.FK_CATLIB = d.PK_CATLIB "+
                " and ',' || lower(d.catlib_subtype) ||',' like lower('%,"+htParams.get("fSubtype")+",%') ";
            }
            
            //in this case there is no one form category subtype will be passed,get all that are configured in ER_IRBFORMS_SETUP
            if (htParams.containsKey("getALLChecked") && ( ((String)htParams.get("getALLChecked")).equals("true") ) )
            {
                fSubtypeExists = true;
                specimenCheck += " and a.FK_FORMLIB = b.PK_FORMLIB and b.FK_CATLIB = d.PK_CATLIB  and ("+catlibsubtyp+ ")";
                
            }


		}


			sqlBuf.append("select a.PK_LF as PK_LF ,c.FK_FORMLIB as FK_FORMLIB , LF_ENTRYCHAR , LF_DISPLAYTYPE,LF_LNKFROM,LF_SEQ,");
			sqlBuf.append(" FORM_NAME, FORM_DESC , LOWER(FORM_NAME) FRMNAME, ");
			sqlBuf.append("	(SELECT COUNT(*) FROM ER_STUDYFORMS sf WHERE sf.FK_FORMLIB = a.FK_FORMLIB AND sf.FK_STUDY = ? and sf.RECORD_TYPE <> 'D') SAVEDCOUNT  ");
			sqlBuf.append(" , lf_display_inspec FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c ");
			if (fSubtypeExists) { sqlBuf.append(", er_catlib d "); }
			sqlBuf.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB   ");
			sqlBuf.append(" AND c.FK_FORMLIB = PK_FORMLIB AND ");

			// user has the group right for all study forms access
			if ((stdRight == 0) && (grpRight >= 4)) {
			    sqlBuf.append(" (LF_DISPLAYTYPE = 'SA' ) ");
			}

			// user has the study team right for study forms access
			if ((stdRight >= 4) && (grpRight == 0)) {
			    sqlBuf.append(" (FK_STUDY = ? AND LF_DISPLAYTYPE = 'S'  ) ");
			}

			// user has both group and study team right for study forms access
			if ((stdRight >= 4) && (grpRight >= 4)) {
			    sqlBuf.append(" (( FK_STUDY = ? AND LF_DISPLAYTYPE = 'S' ) OR (LF_DISPLAYTYPE = 'SA' )) ");
			}
			sqlBuf.append(specimenCheck);


			sqlBuf.append("	and  a.record_type <> 'D' and nvl(a.lf_hide,0)=0  and c.formstat_enddate IS NULL  ");
			sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or ");
			sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) ) ");
			sqlBuf.append(" and a.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0)) and ");
			sqlBuf.append(" a.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user=? ))");
			sqlBuf.append(" and a.pk_lf not in (select pkg_util.f_to_number(settings_value) from er_settings where settings_modname='3' AND settings_modnum=? AND settings_keyword='FORM_HIDE')");

			sqlBuf.append(" union ");

			sqlBuf.append(" (select a.PK_LF as PK_LF ,c.FK_FORMLIB as FK_FORMLIB , LF_ENTRYCHAR , LF_DISPLAYTYPE,LF_LNKFROM,LF_SEQ,");
			sqlBuf.append(" FORM_NAME, FORM_DESC , LOWER(FORM_NAME) FRMNAME, ");
			sqlBuf.append("	(SELECT COUNT(*) FROM ER_STUDYFORMS sf WHERE sf.FK_FORMLIB = a.FK_FORMLIB AND sf.FK_STUDY = ? and sf.RECORD_TYPE <> 'D') SAVEDCOUNT  ");
			sqlBuf.append(" , lf_display_inspec FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c ");
            if (fSubtypeExists) { sqlBuf.append(", er_catlib d "); }
			sqlBuf.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB   ");
			sqlBuf.append(" AND c.FK_FORMLIB = PK_FORMLIB AND ");

			// user has the group right for all study forms access
			if ((stdRight == 0) && (grpRight >= 4)) {
			    sqlBuf.append(" (LF_DISPLAYTYPE = 'SA' ) ");
			}

			// user has the study team right for study forms access
			if ((stdRight >= 4) && (grpRight == 0)) {
			    sqlBuf.append(" (FK_STUDY = ? AND LF_DISPLAYTYPE = 'S'  ) ");
			}

			// user has both group and study team right for study forms access
			if ((stdRight >= 4) && (grpRight >= 4)) {
			    sqlBuf.append(" (( FK_STUDY = ? AND LF_DISPLAYTYPE = 'S' ) OR (LF_DISPLAYTYPE = 'SA' )) ");
			}

			sqlBuf.append(specimenCheck);

			sqlBuf.append("	and  a.record_type <> 'D' and nvl(a.lf_hide,0)=0 and c.formstat_enddate IS NULL  ");
			sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or ");
			sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) ) ");
			sqlBuf.append(" and (a.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user=? ))");
			sqlBuf.append(" or a.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0)))");
			sqlBuf.append("  and a.pk_lf not in (select pkg_util.f_to_number(settings_value) from er_settings where settings_modname='3' AND settings_modnum=? AND settings_keyword='FORM_HIDE'))");
			sqlBuf.append(" order by LF_DISPLAYTYPE, LF_SEQ, FRMNAME ");

			return sqlBuf.toString();

    }


    /**
     * Overloaded method Gets the list of Filled study specific forms and
     * respective number of times they have been filled - Study Level/Specific
     * Study Level For displaying the Study forms for data entry, the primary
     * organization of the logged in user is matched with the organizations
     * selected in the filter and all the groups that the user belongs to are
     * matched with the selected filter groups. The hidden forms are not
     * displayed and forms are displayed as per the sequence entered by user The
     * study forms are displayed before All Study Forms Retrieves Active and
     * Lockdown forms.
     * Gets forms available for specimen management
     *
     * @param accountId
     *            The accountId for which the Forms are to be fetched
     * @param studyId
     *            The studyId for which the Forms are to be fetched
     * @param userId
     *            The user for which the Forms are to be fetched
     * @param siteId
     *            The primary organization of loggedin user
     * @param grpRight
     *            The group right for All Study Forms access
     * @param stdRight
     *            The study team right for Study Forms access
     */

    public void getStudyFormsForSpecimen(int accountId, int studyId, int userId,
            int siteId, int grpRight, int stdRight) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        Hashtable htParam = new Hashtable();

        try {
            conn = getConnection();
        	htParam.put("specimen","1");



            sql = getStudyFormsSQL(stdRight,grpRight,htParam);

            pstmt = conn.prepareStatement(sql);

            if ((stdRight == 0) && (grpRight >= 4)) {
                pstmt.setInt(1, studyId);
                pstmt.setInt(2, accountId);
                pstmt.setInt(3, userId);
                pstmt.setInt(4, userId);
                pstmt.setInt(5, studyId);
                pstmt.setInt(6, studyId);
                pstmt.setInt(7, accountId);
                pstmt.setInt(8, userId);
                pstmt.setInt(9, userId);
                pstmt.setInt(10, studyId);
            } else {
                pstmt.setInt(1, studyId);
                pstmt.setInt(2, accountId);
                pstmt.setInt(3, studyId);
                pstmt.setInt(4, userId);
                pstmt.setInt(5, userId);
                pstmt.setInt(6, studyId);
                pstmt.setInt(7, studyId);
                pstmt.setInt(8, accountId);
                pstmt.setInt(9, studyId);
                pstmt.setInt(10, userId);
                pstmt.setInt(11, userId);
                pstmt.setInt(12, studyId);
            }

            ResultSet rs = pstmt.executeQuery();


            while (rs.next()) {

                setLFId(new Integer(rs.getInt("PK_LF")));
                setFormId(new Integer(rs.getInt("FK_FORMLIB")));
                setFormDisplayType(rs.getString("LF_DISPLAYTYPE").trim());
                setFormEntryChar(rs.getString("LF_ENTRYCHAR"));
                setFormLinkedFrom(rs.getString("LF_LNKFROM"));
                setFormName(rs.getString("FORM_NAME"));
                setFormDescription(rs.getString("FORM_DESC"));
                setFormSavedEntriesNum(new Integer(rs.getInt("SAVEDCOUNT")));
                setLfDispSepecimens(rs.getString("lf_display_inspec"));

                rows++;

            }

            setCRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("lnkform", "LinkedFormsDao.getStudyFormsForSpecimen EXCEPTION"
                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    private String getPatientFormsSQL(Hashtable htParams)
    {
    	StringBuffer sqlBuf = new StringBuffer();

    	String specimenCheck = "";
		String specimenCheckVal = "";

		if (htParams != null)
		{
			if (htParams.containsKey("specimen"))
			{
				specimenCheckVal = (String) htParams.get("specimen");

				specimenCheck = " and nvl(lf_display_inspec,0) = " + specimenCheckVal;
			}
		}


    	sqlBuf.append("select a.PK_LF as PK_LF ,c.FK_FORMLIB as FK_FORMLIB , LF_ENTRYCHAR , LF_DISPLAYTYPE,LF_LNKFROM,LF_SEQ,nvl(LF_DISPLAY_INPAT,0) LF_DISPLAY_INPAT,");
    	sqlBuf.append(" FORM_NAME, FORM_DESC , LOWER(FORM_NAME) FRMNAME, ");
    	sqlBuf.append(" CASE WHEN (nvl(LF_DISPLAY_INPAT,0)=1) THEN ");
    	sqlBuf.append(" (select count(*) from ER_PATFORMS pf where pf.Fk_formlib = a.FK_FORMLIB and pf.Fk_per = ? and pf.RECORD_TYPE <> 'D' AND FK_PATPROT IS NULL) ");
       	sqlBuf.append(" ELSE ");
    	sqlBuf.append(" (select count(*) from ER_PATFORMS pf where pf.Fk_formlib = a.FK_FORMLIB and pf.Fk_per = ? and pf.RECORD_TYPE <> 'D') ");
    	sqlBuf.append(" END AS SAVEDCOUNT ");  	
    	//sqlBuf.append(" (select count(*) from ER_PATFORMS where Fk_formlib = a.FK_FORMLIB and Fk_per = ? and RECORD_TYPE <> 'D') SAVEDCOUNT  ");
    	sqlBuf.append(" , lf_display_inspec FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c ");
    	sqlBuf.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB   ");
    	sqlBuf.append(" AND c.FK_FORMLIB = PK_FORMLIB   AND (LF_DISPLAYTYPE = 'PA' or (LF_DISPLAYTYPE = 'PS' and nvl(LF_DISPLAY_INPAT,0)=1) OR (LF_DISPLAYTYPE = 'PR' and nvl(LF_DISPLAY_INPAT,0)=1))    and  a.record_type <> 'D' and nvl(a.lf_hide,0)=0 and c.formstat_enddate IS NULL  ");
    	sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or ");
    	sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) ) ");
    	sqlBuf .append(" and a.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0)) and ");
    	sqlBuf .append(" a.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user=? ))");

    	sqlBuf.append(specimenCheck);

    	sqlBuf.append(" union  ");
    	sqlBuf.append(" select a.PK_LF as PK_LF ,c.FK_FORMLIB as FK_FORMLIB , LF_ENTRYCHAR ,LF_DISPLAYTYPE,LF_LNKFROM,LF_SEQ,nvl(LF_DISPLAY_INPAT,0) LF_DISPLAY_INPAT,");
    	sqlBuf.append(" FORM_NAME, FORM_DESC , LOWER(FORM_NAME) FRMNAME, ");
    	sqlBuf.append(" CASE WHEN (nvl(LF_DISPLAY_INPAT,0)=1) THEN ");
    	sqlBuf.append(" (select count(*) from ER_PATFORMS pf where pf.Fk_formlib = a.FK_FORMLIB and pf.Fk_per = ? and pf.RECORD_TYPE <> 'D' AND FK_PATPROT IS NULL) ");
       	sqlBuf.append(" ELSE ");
    	sqlBuf.append(" (select count(*) from ER_PATFORMS pf where pf.Fk_formlib = a.FK_FORMLIB and pf.Fk_per = ? and pf.RECORD_TYPE <> 'D') ");
    	sqlBuf.append(" END AS SAVEDCOUNT ");
    	sqlBuf.append(", lf_display_inspec FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c  ");
    	sqlBuf.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB  ");
    	sqlBuf.append(" AND c.FK_FORMLIB = PK_FORMLIB   AND (LF_DISPLAYTYPE = 'PA' or (LF_DISPLAYTYPE = 'PS'and nvl(LF_DISPLAY_INPAT,0)=1) OR (LF_DISPLAYTYPE = 'PR' and nvl(LF_DISPLAY_INPAT,0)=1)) and  a.record_type <> 'D' and nvl(a.lf_hide,0)=0 ");
    	sqlBuf.append(" and c.formstat_enddate IS NULL ");
    	sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or ");
    	sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) ) ");
    	sqlBuf.append(" and (a.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user=? ))");
    	sqlBuf.append(" or a.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0))) ");

    	sqlBuf.append(specimenCheck);

    	sqlBuf.append(" order by LF_SEQ, FRMNAME ");

    	return sqlBuf.toString();

    }


    /**
     * Gets the list of patient specific forms and respective number of times
     * they have been filled - Patient Level For displaying the Account forms
     * for data entry, the primary organization of the logged in user is matched
     * with the organizations selected in the filter and all the groups that the
     * user belongs to are matched with the selected filter groups. The hidden
     * forms are not displayed and forms are displayed as per the sequence
     * entered by user Forms which are linked to patients on all studies and
     * have display in patient profile flag set to 1 are also displayed
     * Retrieves Active as well as Lockdown Forms. looks for forms marked as display in speciment management
     *
     * @param accountId
     *            The accountId for which the Forms are to be fetched
     * @param patient
     *            The patientId for which the Forms are to be fetched
     * @param userId
     *            The user for which the Forms are to be fetched
     * @param siteId
     *            The primary organization of loggedin user
     */

    public void getPatientFormsForSpecimen(int accountId, int patient, int userId,
            int siteId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";

        Hashtable htParam = new Hashtable();

        try {
            conn = getConnection();

            htParam.put("specimen","1");

            sql = getPatientFormsSQL(htParam);

            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, patient);
            pstmt.setInt(2, patient);
            pstmt.setInt(3, accountId);
            pstmt.setInt(4, userId);
            pstmt.setInt(5, userId);
            pstmt.setInt(6, patient);
            pstmt.setInt(7, patient);
            pstmt.setInt(8, accountId);
            pstmt.setInt(9, userId);
            pstmt.setInt(10, userId);

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("lnkform", "after execute inside getPatientFormsForSpecimen" + rs);

            while (rs.next()) {

                setLFId(new Integer(rs.getInt("PK_LF")));
                setFormId(new Integer(rs.getInt("FK_FORMLIB")));
                setFormEntryChar(rs.getString("LF_ENTRYCHAR"));
                setFormName(rs.getString("FORM_NAME"));
                setFormDescription(rs.getString("FORM_DESC"));
                setFormSavedEntriesNum(new Integer(rs.getInt("SAVEDCOUNT")));
                setFormDisplayType(rs.getString("LF_DISPLAYTYPE").trim());
                setFormLinkedFrom(rs.getString("LF_LNKFROM"));
                setLfDispPatProfiles(new Integer(rs.getInt("LF_DISPLAY_INPAT")));
                setLfDispSepecimens(rs.getString("lf_display_inspec"));

                rows++;



            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("lnkform", "LinkedFormsDao.getPatientFormsForSpecimen EXCEPTION"
                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    private String getPatientStudyFormsSQL(Hashtable htParams)
    {
    	StringBuffer sqlBuf = new StringBuffer();

    	String specimenCheck = "";
		String specimenCheckVal = "";
		String formDispType = "";
		String entryChar = "";
		String displayForPRandE="";
		String patProtId ="";
		String includeHidden = "";
		
		if (htParams != null)
		{
			if (htParams.containsKey("specimen"))
			{
				specimenCheckVal = (String) htParams.get("specimen");

				specimenCheck = " and nvl(lf_display_inspec,0) = " + specimenCheckVal;
			}
			//Added For Bug# 9255 :Date 14 May 2012 :By YPS
			if (htParams.containsKey("formDispType") && htParams.containsKey("entryChar")){
				formDispType = (String) htParams.get("formDispType");
				entryChar = (String) htParams.get("entryChar");
			}
			if(formDispType.equals("PR") && entryChar.equals("E")){
				patProtId = (String) htParams.get("patProtId");
				if(!patProtId.equals("0"))
					displayForPRandE=" AND pf.FK_PATPROT = " + patProtId;
			}
			if (htParams.containsKey("includeHidden") && "Y".equals(htParams.get("includeHidden"))) {
				includeHidden = "Y";
			}
		}

    	// SP - patient enrolled to a specific study
        // PS - Patient Level (All Studies)

        sqlBuf.append("select a.PK_LF as PK_LF ,c.FK_FORMLIB as FK_FORMLIB , LF_ENTRYCHAR , LF_DISPLAYTYPE,LF_LNKFROM,LF_SEQ,");
        sqlBuf.append(" FORM_NAME, FORM_DESC , LOWER(FORM_NAME) FRMNAME, ");
        sqlBuf.append(" (select count(*) from ER_PATFORMS pf where pf.Fk_formlib = a.FK_FORMLIB and pf.FK_PER = ? and pf.RECORD_TYPE <> 'D' " + displayForPRandE + " ) SAVEDCOUNT  ");
        sqlBuf.append(", lf_display_inspec FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c ");
        sqlBuf.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB   ");
        sqlBuf.append(" AND c.FK_FORMLIB = PK_FORMLIB  ");
        sqlBuf.append(" AND ( (LF_DISPLAYTYPE = 'SP' and FK_STUDY =?) OR (LF_DISPLAYTYPE = 'PS') OR (LF_DISPLAYTYPE = 'PR')) ");
        if ("Y".equals(includeHidden)) {
            sqlBuf.append(" and  a.record_type <> 'D' and c.formstat_enddate IS NULL  ");
        } else {
            sqlBuf.append(" and  a.record_type <> 'D' and nvl(a.lf_hide,0)=0 and c.formstat_enddate IS NULL  ");
        }
        sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or ");
        sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) ) ");
        sqlBuf.append(" and a.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0)) and ");
        sqlBuf.append(" a.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user=? ))");
        if ("Y".equals(includeHidden)) {
            sqlBuf.append("  and a.pk_lf not in (select pkg_util.f_to_number(settings_value) from er_settings where settings_modname='3' AND settings_modnum=? AND settings_keyword='IGNORE_ME')");
        } else {
            sqlBuf.append("  and a.pk_lf not in (select pkg_util.f_to_number(settings_value) from er_settings where settings_modname='3' AND settings_modnum=? AND settings_keyword='FORM_HIDE')");
        }
       	sqlBuf.append(specimenCheck);

        sqlBuf.append(" union  ");
        sqlBuf.append(" select a.PK_LF as PK_LF ,c.FK_FORMLIB as FK_FORMLIB , LF_ENTRYCHAR ,LF_DISPLAYTYPE,LF_LNKFROM,LF_SEQ,");
        sqlBuf.append(" FORM_NAME, FORM_DESC , LOWER(FORM_NAME) FRMNAME, ");
        sqlBuf.append(" (select count(*) from ER_PATFORMS pf where pf.Fk_formlib = a.FK_FORMLIB and pf.FK_PER = ? and pf.RECORD_TYPE <> 'D' " + displayForPRandE + " ) SAVEDCOUNT  ");
        sqlBuf.append(" , lf_display_inspec FROM ER_LINKEDFORMS  a, ER_FORMLIB  b,er_formstat c ");
        sqlBuf.append(" WHERE b.FK_ACCOUNT = ?  AND a.FK_FORMLIB = PK_FORMLIB   ");
        sqlBuf.append(" AND c.FK_FORMLIB = PK_FORMLIB  ");
        sqlBuf.append(" AND ( (LF_DISPLAYTYPE = 'SP' and FK_STUDY =?) OR (LF_DISPLAYTYPE = 'PS') OR (LF_DISPLAYTYPE = 'PR')) ");
        if ("Y".equals(includeHidden)) {
            sqlBuf.append(" and  a.record_type <> 'D' and c.formstat_enddate IS NULL  ");
        } else {
            sqlBuf.append(" and  a.record_type <> 'D' and nvl(a.lf_hide,0)=0 and c.formstat_enddate IS NULL  ");
        }
        sqlBuf.append(" and ((c.fk_codelst_stat = (select pk_codelst from er_codelst where  codelst_subtyp='A' and codelst_type='frmstat')) or ");
        sqlBuf.append(" (c.fk_codelst_stat = (select pk_codelst from er_codelst where codelst_subtyp='L'  and codelst_type='frmstat')) ) ");
        sqlBuf.append(" and (a.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user=? ))");
        sqlBuf.append(" or a.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user=? and usersite_right <> 0))) ");
        if ("Y".equals(includeHidden)) {
            sqlBuf.append(" and  a.pk_lf not in (select pkg_util.f_to_number(settings_value) from er_settings where settings_modname='3' AND settings_modnum=? AND settings_keyword='IGNORE_ME') ");
        } else {
            sqlBuf.append(" and  a.pk_lf not in (select pkg_util.f_to_number(settings_value) from er_settings where settings_modname='3' AND settings_modnum=? AND settings_keyword='FORM_HIDE') ");
        }
       	sqlBuf.append(specimenCheck);

        sqlBuf.append(" order by  LF_DISPLAYTYPE desc, LF_SEQ , FRMNAME asc ");

    	return sqlBuf.toString();

    }


    public void getPatientStudyFormsForSpecimen(int accountId, int patId, int studyId,
            int userId, int siteId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        Hashtable htParam = new Hashtable();

        try {
            conn = getConnection();

            htParam.put("specimen","1");

            sql = getPatientStudyFormsSQL(htParam);
            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, patId);
            pstmt.setInt(2, accountId);
            pstmt.setInt(3, studyId);
            pstmt.setInt(4, userId);
            pstmt.setInt(5, userId);
            pstmt.setInt(6, studyId);
            pstmt.setInt(7, patId);
            pstmt.setInt(8, accountId);
            pstmt.setInt(9, studyId);
            pstmt.setInt(10, userId);
            pstmt.setInt(11, userId);
            pstmt.setInt(12, studyId);

            ResultSet rs = pstmt.executeQuery();


            while (rs.next()) {

                setLFId(new Integer(rs.getInt("PK_LF")));
                setFormId(new Integer(rs.getInt("FK_FORMLIB")));
                setFormDisplayType(rs.getString("LF_DISPLAYTYPE").trim());
                setFormEntryChar(rs.getString("LF_ENTRYCHAR"));
                setFormLinkedFrom(rs.getString("LF_LNKFROM"));
                setFormName(rs.getString("FORM_NAME"));
                setFormDescription(rs.getString("FORM_DESC"));
                setFormSavedEntriesNum(new Integer(rs.getInt("SAVEDCOUNT")));
                setLfDispSepecimens(rs.getString("lf_display_inspec"));

                rows++;

                Rlog.debug("lnkform", "LinkedFormsDao.getPatientStudyFormsForSpecimen "
                        + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("lnkform",
                    "LinkedFormsDao.getPatientStudyFormsForSpecimen EXCEPTION" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    ////////////////
 /** get latest response for a study form 
  *  @author Sonia Abrol
  *  @return Hashtable of data with response attributes
  *  	possible keys:
  *  
  *  	formlibver - form version for the response
  *     pkstudyforms - response PK
  *     
  *  07/02/09 */
    
    public static Hashtable getLatestStudyFormResponeData(String formId, String studyId){
    	return getLatestStudyFormResponeData(StringUtil.stringToNum(formId), StringUtil.stringToNum(studyId));	
    }
 
    public static Hashtable getLatestStudyFormResponeData(int formId, int studyId){
 	   if (formId <= 0 || studyId <= 0) { return null; }
        PreparedStatement stmt = null;
        Connection conn = null;
        String sql = "";
        
        String data = "";
        String formlibver = "";
        Hashtable htReturn = new Hashtable();
        
        try {

            conn = getConnection();
            sql = " select i.pk_studyforms,(select formlibver_number from er_formlibver where pk_formlibver = o.fk_formlibver) formlibver_number, " +
            " o.form_completed, (select codelst_desc from er_codelst where pk_codelst = o.form_completed) form_status_desc, " +
            " (o.studyforms_xml).getClobVal() as studyforms_xml "+
    		   " from (select max(pk_studyforms) pk_studyforms "+
            " from er_studyforms where fk_formlib = ? and fk_study = ? and record_type <> 'D') i, er_studyforms o where o.pk_studyforms = i.pk_studyforms ";

            ResultSet rs = null;
            try {

                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, formId);
                stmt.setInt(2, studyId);
                
                stmt.execute();

                rs = stmt.getResultSet();

            } catch (Exception ex) {
                Rlog.fatal("formlib", "Exception in getLatestStudyFormResponeData(String formId, String studyId) "
                        + ex);
            }

            if (rs != null) {

                while (rs.next()) {
                    htReturn.put("pkstudyforms", rs.getString("pk_studyforms"));
                    htReturn.put("formlibver", rs.getString("formlibver_number"));
                    htReturn.put("form_completed", rs.getString("form_completed"));
                    htReturn.put("form_status_desc", rs.getString("form_status_desc"));
                    
                    Clob clob_xml=rs.getClob("studyforms_xml");
                    htReturn.put("studyforms_xml", 
                 		   clob_xml.getSubString(new Integer(1).longValue(),new Long(clob_xml.length()).intValue()));
                    return htReturn;
                }

            }
        } catch (Exception ex) {
            Rlog.fatal("formlib",
                    "Exception in FormLib.getLatestStudyFormResponeData :"
                            + ex);
            ex.printStackTrace();
            
 	           
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
            return htReturn;
        }
    }


    /**
     * Overloaded method Gets the list of Filled study specific forms and
     * respective number of times they have been filled - Study Level/Specific
     * Study Level For displaying the Study forms for data entry, the primary
     * organization of the logged in user is matched with the organizations
     * selected in the filter and all the groups that the user belongs to are
     * matched with the selected filter groups. The hidden forms are not
     * displayed and forms are displayed as per the sequence entered by user The
     * study forms are displayed before All Study Forms Retrieves Active and
     * Lockdown forms. The method also gets the response data and sets it within the LinkedFormsDao
     *
     * @param accountId
     *            The accountId for which the Forms are to be fetched
     * @param studyId
     *            The studyId for which the Forms are to be fetched
     * @param userId
     *            The user for which the Forms are to be fetched
     * @param siteId
     *            The primary organization of loggedin user
     * @param grpRight
     *            The group right for All Study Forms access
     * @param stdRight
     *            The study team right for Study Forms access
     * @param isIrb
     *            true=only get IRB forms; false=get all forms
     * @param submissionType
     *            Submission type for IRB application (study) 
     * @param formSubtype
     *            form subtype for IRB application (study) 
     */

    public void getStudyFormsWithResponseData(int accountId, int studyId, int userId,
            int siteId, int grpRight, int stdRight, 
            boolean isIrb, String submissionType, String formSubtype)
    {
    	try
    	{
		    	/*call getStudyFormsWithResponseData(int accountId, int studyId, int userId,
		        int siteId, int grpRight, int stdRight, 
		        boolean isIrb, String submissionType, String formSubtype)*/
		    	
    		getStudyForms(accountId, studyId, userId,
		                siteId, grpRight, stdRight, isIrb, submissionType, formSubtype);
		    	
		    	//iterate through the returned forms and get response data 
		    	ArrayList frmIds = new ArrayList();
		    	
		    	int totalforms = 0;
		    	int ctr = 0;
		    	String stdIDStr = "";
		    	
		    	stdIDStr = String.valueOf(studyId);
		    	
		    	frmIds = this.getFormId();
		    	
		    	totalforms = frmIds.size();
		    	
		    	for (ctr= 0;ctr < totalforms; ctr++)
		    	{
		    		FormResponseDao respD = new FormResponseDao();
		    		
		    		respD.getStudyFormResponeData(((Integer) frmIds.get(ctr)).intValue(), stdIDStr );
		    		
		    		this.setArFormResponseDao(respD);
		    		
		    	}
    	} catch (Exception ex)
    	{
    		Rlog.fatal("formlib",
                    "Exception in FormLib.getStudyFormsWithResponseData :"
                            + ex);
            ex.printStackTrace();
            
    	}
    }

	public ArrayList getArFormResponseDao() {
		return arFormResponseDao;
	}

	public void setArFormResponseDao(ArrayList arFormResponseDao) {
		this.arFormResponseDao = arFormResponseDao;
	}
	
	public void setArFormResponseDao(FormResponseDao formResponseDao) {
		this.arFormResponseDao.add(formResponseDao);
	}
	
	/**
     * Gets the list of study specific forms for patient Restricted and Single Entry
     * of times they have been filled - Specific Study-Patient Level For
     * displaying the Account forms for data entry, the primary organization of
     * the logged in user is matched with the organizations selected in the
     * filter and all the groups that the user belongs to are matched with the
     * selected filter groups. The hidden forms are not displayed and forms are
     * displayed as per the sequence entered by user 'Patients on All Studies'
     * forms are displayed below the forms associated to "Patient Specific
     * Study' Retrieves Active and Lockdown forms
     *
     * @param accountId
     *            The accountId for which the Forms are to be fetched
     * @param patId
     *            The patient Id for which the Forms are to be fetched
     * @param studyId
     *            The Study for which Form is associated
     * @param userId
     *            The user for which the Forms are to be fetched
     * @param siteId
     *            The primary organization of loggedin user
     */

    public void getPatStdFormsForPatRestrAndSingleEntry(int accountId, int patId, int studyId,
            int userId, int siteId,Hashtable<String, String> htParams) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";

        try {
            conn = getConnection();

            sql = getPatientStudyFormsSQL(htParams);

            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, patId);
            pstmt.setInt(2, accountId);
            pstmt.setInt(3, studyId);
            pstmt.setInt(4, userId);
            pstmt.setInt(5, userId);
            pstmt.setInt(6, studyId);
            pstmt.setInt(7, patId);
            pstmt.setInt(8, accountId);
            pstmt.setInt(9, studyId);
            pstmt.setInt(10, userId);
            pstmt.setInt(11, userId);
            pstmt.setInt(12, studyId);

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("lnkform", "after execute inside getPatStdFormsForPatRestrAndSingleEntry"
                    + rs);

            while (rs.next()) {

                setLFId(new Integer(rs.getInt("PK_LF")));
                setFormId(new Integer(rs.getInt("FK_FORMLIB")));
                setFormDisplayType(rs.getString("LF_DISPLAYTYPE").trim());
                setFormEntryChar(rs.getString("LF_ENTRYCHAR"));
                setFormLinkedFrom(rs.getString("LF_LNKFROM"));
                setFormName(rs.getString("FORM_NAME"));
                setFormDescription(rs.getString("FORM_DESC"));
                setFormSavedEntriesNum(new Integer(rs.getInt("SAVEDCOUNT")));
                setLfDispSepecimens(rs.getString("lf_display_inspec"));

                rows++;

                Rlog.debug("lnkform", "LinkedFormsDao.getPatStdFormsForPatRestrAndSingleEntry "
                        + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("lnkform",
                    "LinkedFormsDao.getPatStdFormsForPatRestrAndSingleEntry EXCEPTION" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    private static String formNameCountSQLForS =
    		" select count(*) from ER_LINKEDFORMS L, ER_FORMLIB F where "
    				+ " L.FK_FORMLIB = F.PK_FORMLIB and "
    				+ " F.FORM_NAME = ( select FORM_NAME from ER_FORMLIB F2 where "
    				+ " F2.PK_FORMLIB = (select FK_FORMLIB from ER_LINKEDFORMS L2 where L2.PK_LF = ?) ) and "
    				+ " NVL(L.record_type,'Z') <> 'D' and L.FK_ACCOUNT = ? and "
    				+ " trim(L.LF_DISPLAYTYPE) = 'S' and L.FK_STUDY = ? " ;
    						//and "
					/*hlodhwal update for bug 20762*/										/*Code commented for #24205 bug*/
    				/*+ " L.PK_LF <> ? and "
    				+ " L.FK_FORMLIB = ( select FK_FORMLIB from ER_LINKEDFORMS L2 where L2.PK_LF = ? )";*/
    private static String formNameCountSQLForSA =
    		" select count(*) from ER_LINKEDFORMS L, ER_FORMLIB F where "
    				+ " L.FK_FORMLIB = F.PK_FORMLIB and "
    				+ " F.FORM_NAME = ( select FORM_NAME from ER_FORMLIB F2 where "
    				+ " F2.PK_FORMLIB = (select FK_FORMLIB from ER_LINKEDFORMS L2 where L2.PK_LF = ?) ) and "
    				+ " NVL(L.record_type,'Z') <> 'D' and L.FK_ACCOUNT = ? and "
    				+ " L.LF_DISPLAYTYPE = 'SA' " ;
    						//and "
					/*hlodhwal update for bug 20762*/										/*Code commented for #24205 bug*/
    				/*+ " L.PK_LF <> ?  and "
    				+ " L.FK_FORMLIB = ( select FK_FORMLIB from ER_LINKEDFORMS L2 where L2.PK_LF = ? )";*/
    private static String formNameCountSQLForA =
    		" select count(*) from ER_LINKEDFORMS L, ER_FORMLIB F where "
    				+ " L.FK_FORMLIB = F.PK_FORMLIB and "
    				+ " F.FORM_NAME = ( select FORM_NAME from ER_FORMLIB F2 where "
    				+ " F2.PK_FORMLIB = (select FK_FORMLIB from ER_LINKEDFORMS L2 where L2.PK_LF = ?) ) and "
    				+ " NVL(L.record_type,'Z') <> 'D' and L.FK_ACCOUNT = ? and "
    				+ " trim(L.LF_DISPLAYTYPE) = 'A' " ;
    						//and "
					/*hlodhwal update for bug 20762*/										/*Code commented for #24205 bug*/
    				/*+ " L.PK_LF <> ?  and "
    				+ " L.FK_FORMLIB = ( select FK_FORMLIB from ER_LINKEDFORMS L2 where L2.PK_LF = ? )";*/    
    private static String formNameCountSQLForPA =
		" select count(*) from ER_LINKEDFORMS L, ER_FORMLIB F where "
				+ " L.FK_FORMLIB = F.PK_FORMLIB and "
				+ " F.FORM_NAME = ( select FORM_NAME from ER_FORMLIB F2 where "
				+ " F2.PK_FORMLIB = (select FK_FORMLIB from ER_LINKEDFORMS L2 where L2.PK_LF = ?) ) and "
				+ " NVL(L.record_type,'Z') <> 'D' and L.FK_ACCOUNT = ? and "
				+ " trim(L.LF_DISPLAYTYPE) = 'PA' " ;
					//and "
				/*hlodhwal update for bug 20762*/											/*Code commented for #24205 bug*/
				/*+ " L.PK_LF <> ? and "
				+ " L.FK_FORMLIB = ( select FK_FORMLIB from ER_LINKEDFORMS L2 where L2.PK_LF = ? )";*/
    private static String formNameCountSQLForPS =
		" select count(*) from ER_LINKEDFORMS L, ER_FORMLIB F where "
				+ " L.FK_FORMLIB = F.PK_FORMLIB and "
				+ " F.FORM_NAME = ( select FORM_NAME from ER_FORMLIB F2 where "
				+ " F2.PK_FORMLIB = (select FK_FORMLIB from ER_LINKEDFORMS L2 where L2.PK_LF = ?) ) and "
				+ " NVL(L.record_type,'Z') <> 'D' and L.FK_ACCOUNT = ? and "
				+ " trim(L.LF_DISPLAYTYPE) = 'PS' ";
						//and "
				/*hlodhwal update for bug 20762*/											/*Code commented for #24205 bug*/
				/*+ " L.PK_LF <> ? and "
				+ " L.FK_FORMLIB = ( select FK_FORMLIB from ER_LINKEDFORMS L2 where L2.PK_LF = ? )";*/
    private static String formNameCountSQLForPR =
		" select count(*) from ER_LINKEDFORMS L, ER_FORMLIB F where "
				+ " L.FK_FORMLIB = F.PK_FORMLIB and "
				+ " F.FORM_NAME = ( select FORM_NAME from ER_FORMLIB F2 where "
				+ " F2.PK_FORMLIB = (select FK_FORMLIB from ER_LINKEDFORMS L2 where L2.PK_LF = ?) ) and "
				+ " NVL(L.record_type,'Z') <> 'D' and L.FK_ACCOUNT = ? and "
				+ " trim(L.LF_DISPLAYTYPE) = 'PR' ";
						//and "
				/*hlodhwal update for bug 20762*/                                            /*Code commented for #24205 bug*/
				/*+ " L.PK_LF <> ? and "
				+ " L.FK_FORMLIB = ( select FK_FORMLIB from ER_LINKEDFORMS L2 where L2.PK_LF = ? )";*/
    private static String formNameCountSQLForSP =
    		" select count(*) from ER_LINKEDFORMS L, ER_FORMLIB F where "
    				+ " L.FK_FORMLIB = F.PK_FORMLIB and "
    				+ " F.FORM_NAME = ( select FORM_NAME from ER_FORMLIB F2 where "
    				+ " F2.PK_FORMLIB = (select FK_FORMLIB from ER_LINKEDFORMS L2 where L2.PK_LF = ?) ) and "
    				+ " NVL(L.record_type,'Z') <> 'D' and L.FK_ACCOUNT = ? and "
    				+ " L.LF_DISPLAYTYPE = 'SP' and L.FK_STUDY = ? " ;
    						//and "
					/*hlodhwal update for bug 20762*/										/*Code commented for #24205 bug*/
    				/*+ " L.PK_LF <> ? and "
    				+ " L.FK_FORMLIB = ( select FK_FORMLIB from ER_LINKEDFORMS L2 where L2.PK_LF = ? )";*/
    /**
     * Get the count of linked forms that have the same name as that of the linked form
     * identified by parameter fkLinkedForm. Used when the Display Type is changed, not when 
     * the form name is changed.
     * 
     * @param fkLinkedForm
     * @param displayType
     * @param fkAccount
     * @param fkStudy
     * @return count of linked forms with the same name
     */
    public int getFormNameCount(Integer fkLinkedForm, String displayType, 
    		Integer fkAccount, Integer fkStudy) {
        PreparedStatement pstmt = null;
        Connection conn = null;
    	int ret = 0;
    	try {
            conn = getConnection();
            if (StringUtil.trueValue(displayType).trim().equals("S")) {
            	pstmt = conn.prepareStatement(formNameCountSQLForS);
            	pstmt.setInt(1, fkLinkedForm);
            	pstmt.setInt(2, fkAccount);
            	pstmt.setInt(3, fkStudy);
            	//pstmt.setInt(4, fkLinkedForm);						/*Code commented for #24205 bug*/
            	//pstmt.setInt(5, fkLinkedForm);
            	
            } else if (StringUtil.trueValue(displayType).trim().equals("SA")) {
            	pstmt = conn.prepareStatement(formNameCountSQLForSA);
            	pstmt.setInt(1, fkLinkedForm);
            	pstmt.setInt(2, fkAccount);
            	//pstmt.setInt(3, fkLinkedForm);						/*Code commented for #24205 bug*/
            	//pstmt.setInt(4, fkLinkedForm);
            } else if (StringUtil.trueValue(displayType).trim().equals("A")) {
            	pstmt = conn.prepareStatement(formNameCountSQLForA);
            	pstmt.setInt(1, fkLinkedForm);
            	pstmt.setInt(2, fkAccount);
            	//pstmt.setInt(3, fkLinkedForm);						/*Code commented for #24205 bug*/
            	//pstmt.setInt(4, fkLinkedForm);
            }else if (StringUtil.trueValue(displayType).contains("SP")) {
            	pstmt = conn.prepareStatement(formNameCountSQLForSP);
            	pstmt.setInt(1, fkLinkedForm);
            	pstmt.setInt(2, fkAccount);
            	pstmt.setInt(3, fkStudy);
            	//pstmt.setInt(4, fkLinkedForm);						/*Code commented for #24205 bug*/
            	//pstmt.setInt(5, fkLinkedForm);
            }else if (StringUtil.trueValue(displayType).contains("PA")) {
            	pstmt = conn.prepareStatement(formNameCountSQLForPA);
            	pstmt.setInt(1, fkLinkedForm);
            	pstmt.setInt(2, fkAccount);
            	//pstmt.setInt(3, fkLinkedForm);						/*Code commented for #24205 bug*/
            	//pstmt.setInt(4, fkLinkedForm);
            }else if (StringUtil.trueValue(displayType).contains("PS")) {
            	pstmt = conn.prepareStatement(formNameCountSQLForPS);
            	pstmt.setInt(1, fkLinkedForm);
            	pstmt.setInt(2, fkAccount);
            	//pstmt.setInt(3, fkLinkedForm);						/*Code commented for #24205 bug*/
            	//pstmt.setInt(4, fkLinkedForm);
            }else if (StringUtil.trueValue(displayType).contains("PR")) {
            	pstmt = conn.prepareStatement(formNameCountSQLForPR);
            	pstmt.setInt(1, fkLinkedForm);
            	pstmt.setInt(2, fkAccount);
            	//pstmt.setInt(3, fkLinkedForm);						/*Code commented for #24205 bug*/
            	//pstmt.setInt(4, fkLinkedForm);
            }
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	ret = rs.getInt(1);
            }
    	} catch(Exception e) {
    		Rlog.fatal("lnkform","Exception in LinkedFormsDao.getFormNameCount(): "+e);
    		ret = 0;
    	} finally {
    		try { if (pstmt != null) { pstmt.close(); } } catch(Exception e) {}
    		try { if (conn != null) { conn.close(); } } catch(Exception e) {}
    	}
    	return ret;
    }
    
    private static String formNameCountByNameSQLForS =
    		" select count(*) from ER_LINKEDFORMS L, ER_FORMLIB F where "
    				+ " L.FK_FORMLIB = F.PK_FORMLIB and "
    				+ " F.FORM_NAME = ? and "
    				+ " NVL(L.record_type,'Z') <> 'D' and L.FK_ACCOUNT = ? and "
    				+ " trim(L.LF_DISPLAYTYPE) = 'S' and L.FK_STUDY = ? ";
    private static String formNameCountByNameSQLForSA =
    		" select count(*) from ER_LINKEDFORMS L, ER_FORMLIB F where "
    				+ " L.FK_FORMLIB = F.PK_FORMLIB and "
    				+ " F.FORM_NAME = ? and "
    				+ " NVL(L.record_type,'Z') <> 'D' and L.FK_ACCOUNT = ? and "
    				+ " L.LF_DISPLAYTYPE = 'SA' ";
    private static String formNameCountByNameSQLForA =
    		" select count(*) from ER_LINKEDFORMS L, ER_FORMLIB F where "
    				+ " L.FK_FORMLIB = F.PK_FORMLIB and "
    				+ " F.FORM_NAME = ? and "
    				+ " NVL(L.record_type,'Z') <> 'D' and L.FK_ACCOUNT = ? and "
    				+ " trim(L.LF_DISPLAYTYPE) = 'A' ";
    private static String formNameCountByNameSQLForSP =
    		" select count(*) from ER_LINKEDFORMS L, ER_FORMLIB F where "
    				+ " L.FK_FORMLIB = F.PK_FORMLIB and "
    				+ " F.FORM_NAME = ? and "
    				+ " NVL(L.record_type,'Z') <> 'D' and L.FK_ACCOUNT = ? and "
    				+ " L.LF_DISPLAYTYPE = 'SP' and L.FK_STUDY = ? ";
    private static String formNameCountByNameSQLForPR =
		" select count(*) from ER_LINKEDFORMS L, ER_FORMLIB F where "
				+ " L.FK_FORMLIB = F.PK_FORMLIB and "
				+ " F.FORM_NAME = ? and "
				+ " NVL(L.record_type,'Z') <> 'D' and L.FK_ACCOUNT = ? and "
				+ " L.LF_DISPLAYTYPE = 'PR' ";
    private static String formNameCountByNameSQLForPA =
		" select count(*) from ER_LINKEDFORMS L, ER_FORMLIB F where "
				+ " L.FK_FORMLIB = F.PK_FORMLIB and "
				+ " F.FORM_NAME = ? and "
				+ " NVL(L.record_type,'Z') <> 'D' and L.FK_ACCOUNT = ? and "
				+ " L.LF_DISPLAYTYPE = 'PA' ";
    private static String formNameCountByNameSQLForPS =
		" select count(*) from ER_LINKEDFORMS L, ER_FORMLIB F where "
				+ " L.FK_FORMLIB = F.PK_FORMLIB and "
				+ " F.FORM_NAME = ? and "
				+ " NVL(L.record_type,'Z') <> 'D' and L.FK_ACCOUNT = ? and "
				+ " L.LF_DISPLAYTYPE = 'PS' ";
    /**
     * Get the count of linked forms that have the same name as the parameter formName.
     * Used when the Form Name is changed but not when the Display Type is changed
     * although the Display Type is used to distinguish among various types.
     * 
     * @param formName
     * @param displayType
     * @param fkAccount
     * @param fkStudy
     * @return count of linked forms with the same name
     */
    public int getFormNameCountByName(String formName, String displayType, 
    		Integer fkAccount, Integer fkStudy) {
        PreparedStatement pstmt = null;
        Connection conn = null;
    	int ret = 0;
    	try {
            conn = getConnection();
            if (StringUtil.trueValue(displayType).trim().equals("S")) {
            	pstmt = conn.prepareStatement(formNameCountByNameSQLForS);
            	pstmt.setString(1, formName);
            	pstmt.setInt(2, fkAccount);
            	pstmt.setInt(3, fkStudy);
            } else if (StringUtil.trueValue(displayType).trim().equals("SA")) {
            	pstmt = conn.prepareStatement(formNameCountByNameSQLForSA);
            	pstmt.setString(1, formName);
            	pstmt.setInt(2, fkAccount);
            } else if (StringUtil.trueValue(displayType).trim().equals("A")) {
            	pstmt = conn.prepareStatement(formNameCountByNameSQLForA);
            	pstmt.setString(1, formName);
            	pstmt.setInt(2, fkAccount);
            } else if (StringUtil.trueValue(displayType).contains("SP")) {
            	pstmt = conn.prepareStatement(formNameCountByNameSQLForSP);
            	pstmt.setString(1, formName);
            	pstmt.setInt(2, fkAccount);
            	pstmt.setInt(3, fkStudy);
            } else if (StringUtil.trueValue(displayType).contains("PA")) {
            	pstmt = conn.prepareStatement(formNameCountByNameSQLForPA);
            	pstmt.setString(1, formName);
            	pstmt.setInt(2, fkAccount);
            } else if (StringUtil.trueValue(displayType).contains("PR")) {
            	pstmt = conn.prepareStatement(formNameCountByNameSQLForPR);
            	pstmt.setString(1, formName);
            	pstmt.setInt(2, fkAccount);
            } else if (StringUtil.trueValue(displayType).contains("PS")) {
            	pstmt = conn.prepareStatement(formNameCountByNameSQLForPS);
            	pstmt.setString(1, formName);
            	pstmt.setInt(2, fkAccount);
            }
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	ret = rs.getInt(1);
            }
    	} catch(Exception e) {
    		Rlog.fatal("lnkform","Exception in LinkedFormsDao.getFormNameCountByName(): "+e);
    		ret = 0;
    	} finally {
    		try { if (pstmt != null) { pstmt.close(); } } catch(Exception e) {}
    		try { if (conn != null) { conn.close(); } } catch(Exception e) {}
    	}
    	return ret;
    }

	public String getUserId() {
		return userId;
	}

	public String getOldOrgIds() {
		return oldOrgIds;
	}

	public void setOldOrgIds(String oldOrgIds) {
		this.oldOrgIds = oldOrgIds;
	}

	public String getOldGrpIds() {
		return oldGrpIds;
	}

	public void setOldGrpIds(String oldGrpIds) {
		this.oldGrpIds = oldGrpIds;
	}

	public String getIpAdd() {
		return ipAdd;
	}

	public void getFormResponseAuditInfo(int formId, int filledFormId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = null;
        try {
            conn = getConnection();
            StringBuffer sb = new StringBuffer();
            sb.append("select (select USR_LASTNAME || ', ' || USR_FIRSTNAME from eres.er_user u where pk_user = f.CREATOR) as CREATOR, ")
              .append("to_char(CREATED_ON, PKG_DATEUTIL.f_get_dateformat || ' ' || PKG_DATEUTIL.f_get_timeformat) as CREATED_ON, ")
              .append("(select USR_LASTNAME || ', ' || USR_FIRSTNAME from eres.er_user u where pk_user = f.LAST_MODIFIED_BY) as LAST_MODIFIED_BY, ")
              .append("to_char(LAST_MODIFIED_DATE, PKG_DATEUTIL.f_get_dateformat || ' ' || PKG_DATEUTIL.f_get_timeformat) as LAST_MODIFIED_DATE ")
              .append("from er_formslinear f where FK_FILLEDFORM = ? and FK_FORM = ? ");
            sql = sb.toString();
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, filledFormId);
            pstmt.setInt(2, formId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCreator(rs.getString("CREATOR"));
                setCreatedon(rs.getString("CREATED_ON"));
                setModifiedby(rs.getString("LAST_MODIFIED_BY"));
                setModifiedon(rs.getString("LAST_MODIFIED_DATE"));
                rows++;
            }
            setCRows(rows);
        } catch(Exception e) {
            Rlog.fatal("lnkform", "LinkedFormsDao.getFormResponseAuditInfo EXCEPTION:"+e);
        } finally {
        	try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
        	try { if (conn != null) { conn.close(); } } catch (Exception e) {}
        }
    }
	
	public String getICFForm() {
		StringBuffer icfform= new StringBuffer();
        PreparedStatement pstmt = null;
        PreparedStatement pstmt1 = null;
        Connection conn = null;
        String sql = null;
        String sql1 = null;
        String WA_ONCLICK_JSON="";
        int formid=0;
        try {
            conn = getConnection();
            StringBuffer sb = new StringBuffer();
            sb.append("select WA_ONCLICK_JSON from workflow_activity where WA_ONCLICK_JSON is not null");
              
            sql = sb.toString();
            pstmt = conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                WA_ONCLICK_JSON = rs.getString("WA_ONCLICK_JSON");
                if(!WA_ONCLICK_JSON.equals("") || WA_ONCLICK_JSON!=null){
                JSONObject jObject  = new JSONObject(WA_ONCLICK_JSON); // json
                JSONObject data = jObject.getJSONObject("moduleDet"); // get data object
                String formId = data.getString("formId"); 
                formid = Integer.parseInt(formId);
                sql1="select form_name from er_formlib where pk_formlib='"+formid+"'";  
                pstmt1 = conn.prepareStatement(sql1);
                ResultSet rs1 = pstmt1.executeQuery();
                while(rs1.next()){
                	icfform.append("<OPTION value='"+formid+"*F'>"+rs1.getString("form_name")+"</OPTION>");
                }
                } 
            }
        } catch(Exception e) {
            Rlog.fatal("lnkform", "LinkedFormsDao.getICFForm EXCEPTION:"+e);
        } finally {
        	try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
        	try { if (conn != null) { conn.close(); } } catch (Exception e) {}
        }
        
        return icfform.toString();
    }
	
	public int checkIsIRBform(int formId){
		PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = null;
        int lf_isIRB = 0;
        try {
            conn = getConnection();
            sql= "select LF_ISIRB from er_linkedforms where fk_formlib="+formId;
            pstmt= conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()){
            	lf_isIRB=Integer.parseInt(rs.getString("LF_ISIRB"));
            }
        }catch(Exception e){
        	Rlog.fatal("lnkform", "LinkedFormsDao.getICFForm EXCEPTION:"+e);
        }
        return lf_isIRB;
	}
	
	
}// end of class

