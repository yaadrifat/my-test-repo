/*
 * Classname			PerIdDao.class
 * 
 * Version information 	1.0
 *
 * Date					16/02/04
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * PerIdDao for getting PerId records
 * 
 * @author Anu Khanna
 * @version : 1.0 16/02/04 Modified Date : 11/30/2004 Modified by : Vishal Abrol
 *          Comment : Added support for Dropdown and date fields.
 */

public class PerIdDao extends CommonDAO implements java.io.Serializable {
    private ArrayList id;

    private ArrayList perIdType;

    /*Code-list subtype*/
    private ArrayList alternateIdKey;

    private ArrayList perIdTypesDesc;

    private ArrayList alternateId;

    private ArrayList per;

    private ArrayList recordType;

    private ArrayList dispType;

    private ArrayList dispData;

    private int cRows;
    
    private String forGroup;

    public PerIdDao() {
        id = new ArrayList();
        perIdType = new ArrayList();
        alternateIdKey = new ArrayList();
        perIdTypesDesc = new ArrayList();
        alternateId = new ArrayList();
        per = new ArrayList();
        recordType = new ArrayList();
        dispType = new ArrayList();
        dispData = new ArrayList();

    }

    // Getter and Setter methods
    public void setId(ArrayList id) {
        this.id = id;
    }

    public void setPerIdType(ArrayList perIdType) {
        this.perIdType = perIdType;
    }

    public void setPerIdTypesDesc(ArrayList perIdTypesDesc) {
        this.perIdTypesDesc = perIdTypesDesc;
    }

    public void setAlternateId(ArrayList alternateId) {
        this.alternateId = alternateId;
    }

    public void setPer(ArrayList per) {
        this.per = per;
    }

    public ArrayList getId() {
        return id;
    }

    public ArrayList getPerIdType() {
        return perIdType;
    }

    public ArrayList getPerIdTypesDesc() {
        return perIdTypesDesc;
    }

    public ArrayList getAlternateId() {
        return alternateId;
    }

    public ArrayList getPer() {
        return per;
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public void setId(Integer id) {
        this.id.add(id);
    }

    public void setPerIdType(Integer perIdType) {
        this.perIdType.add(perIdType);
    }

    public void setPerIdTypesDesc(String perIdTypesDesc) {
        this.perIdTypesDesc.add(perIdTypesDesc);
    }

    public void setAlternateId(String alternateId) {
        this.alternateId.add(alternateId);
    }

    public void setPer(Integer per) {
        this.per.add(per);
    }

    public void setRecordType(ArrayList recordType) {
        this.recordType = recordType;
    }

    public ArrayList getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType.add(recordType);
    }

    public void setDispType(ArrayList dispType) {
        this.dispType = dispType;
    }

    public ArrayList getDispType() {
        return dispType;
    }

    public void setDispType(String disp) {
        this.dispType.add(disp);
    }

    /**
     * Returns the value of dispData.
     * 
     * @return an ArrayList of dispData (String objects)
     */
    public ArrayList getDispData() {
        return dispData;
    }

    /**
     * Sets the value of dispData.
     * 
     * @param dispData
     *            The value to assign dispData.
     */
    public void setDispData(ArrayList dispData) {
        this.dispData = dispData;
    }

    /**
     * Add a value of dispData.
     * 
     * @param dispData
     *            The value to add to dispData.
     */
    public void setDispData(String data) {
        this.dispData.add(data);
    }

    public ArrayList getAlternateIdKey() {
		return alternateIdKey;
	}

	public void setAlternateIdKey(ArrayList alternateIdKey) {
		this.alternateIdKey = alternateIdKey;
	}

	public void setAlternateIdKey(String alternateIdKey) {
		this.alternateIdKey.add(alternateIdKey);
	}
    // end of getter and setter methods

    /**
     * Gets all Per Ids. Sets the values in the class attributes
     * 
     * @param perPK
     */

    public void getPerIds(int perPk) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sbSQL = new StringBuffer();
        
        int grpId = 0;
        
        grpId = StringUtil.stringToNum(getForGroup());
       
        
        try {
            conn = getConnection();

            sbSQL.append(" Select pk_perid, pk_codelst, codelst_subtyp, codelst_desc, PERID_ID,'M' record_type, codelst_seq,codelst_custom_col");
            sbSQL.append(" ,codelst_custom_col1 from  pat_perid, er_codelst ");
            sbSQL.append(" where fk_per = ? and  pk_codelst = FK_CODELST_IDTYPE  and ");
            sbSQL.append(" codelst_type = 'peridtype' ");
            sbSQL.append("  UNION ");
            sbSQL.append("  Select 0 pk_perid , pk_codelst, codelst_subtyp, codelst_desc,' ' PERID_ID ,'N' record_type, codelst_seq,codelst_custom_col");
            sbSQL.append(" ,codelst_custom_col1 from  er_codelst  where pk_codelst not in ");
            sbSQL.append("(Select FK_CODELST_IDTYPE from pat_perid where   fk_per = ? ) and ");
            sbSQL.append(" codelst_type = 'peridtype'");
            
            if (grpId > 0) //append group check
            {
            	sbSQL.append(" and not exists (select * from er_codelst_hide h where h.codelst_type = 'peridtype' and h.fk_grp = ? and h.fk_codelst = pk_codelst and h.CODELST_HIDE_TABLE = 1) ");
            	
            }
            sbSQL.append(" order by codelst_seq ");

            pstmt = conn.prepareStatement(sbSQL.toString());

            pstmt.setInt(1, perPk);
            pstmt.setInt(2, perPk);
            
            if (grpId > 0) //append group check
            {
            	pstmt.setInt(3, grpId);
            }

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setId(new Integer(rs.getInt("pk_perid")));
                setPerIdType(new Integer(rs.getInt("pk_codelst")));
                setAlternateIdKey(rs.getString("codelst_subtyp"));
                setPerIdTypesDesc(rs.getString("codelst_desc"));
                setAlternateId(rs.getString("PERID_ID"));
                setRecordType(rs.getString("record_type"));
                setDispType(rs.getString("codelst_custom_col"));
                setDispData(rs.getString("codelst_custom_col1"));
                rows++;
                Rlog.debug("perId", "perIDDao.getPerIds rows " + rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("perId", "perIdDao.getPerIds EXCEPTION IN FETCHING rows"
                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

	public String getForGroup() {
		return forGroup;
	}

	public void setForGroup(String forGroup) {
		this.forGroup = forGroup;
	}

    // end of class
}
