package com.velos.eres.business.userSession.impl;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ER_USERSESSIONTOKEN")
public class UserSessionTokenBean implements Serializable {
	private static final long serialVersionUID = -8976936854506953774L;
	private Integer id;
	private Integer fkUserSession;
	private Integer fkUser;
	private Date ustExpirationTime;
	private String ustToken;
	private String ipAdd;
	
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_USERSESSIONTOKEN", allocationSize=1)
    @Column(name = "PK_UST")
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    @Column(name = "FK_USERSESSION")
	public Integer getFkUserSession() {
		return fkUserSession;
	}
	
	public void setFkUserSession(Integer fkUserSession) {
		this.fkUserSession = fkUserSession;
	}
	
    @Column(name = "FK_USER")
	public Integer getFkUser() {
		return fkUser;
	}
	
	public void setFkUser(Integer fkUser) {
		this.fkUser = fkUser;
	}
	
    @Column(name = "UST_EXPIRATION_TIME")
	public Date getUstExpirationTime() {
		return ustExpirationTime;
	}
	
	public void setUstExpirationTime(Date ustExpirationTime) {
		this.ustExpirationTime = ustExpirationTime;
	}
	
    @Column(name = "UST_TOKEN")
	public String getUstToken() {
		return ustToken;
	}
	
	public void setUstToken(String ustToken) {
		this.ustToken = ustToken;
	}

    @Column(name = "UST_IP_ADD")
	public String getIpAdd() {
		return ipAdd;
	}

	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
	
}
