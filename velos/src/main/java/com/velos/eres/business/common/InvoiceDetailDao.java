package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * InvoiceDao - data Access class for Invoice module
 *
 * @author Sonia Abrol
 * @created 11/04/2005
 */
public class InvoiceDetailDao extends CommonDAO implements java.io.Serializable {


	/**
	 * the Invoice DetailPrimary Key
	 */
	private ArrayList <String>  id;

	/**
	 * the main Invoice PK
	 */
	private ArrayList <String>  invPk;

	/**
	 * the associated milestone
	 */
	private ArrayList <String>  milestone;

	/**
	 * the associated patient
	 */
	private ArrayList <String>  patient;

	/**
	 * the associated study
	 */
	private ArrayList <String>  study;

	/**
	 * the associated patient enrollment
	 */
	private ArrayList <String>  patProt;

	/**
	 * Amount due
	 */
	private ArrayList <String>  amountDue;

	/**
	 * the amount invoiced
	 */
	private ArrayList <String>  amountInvoiced;
	/**
	 * the amount holdback
	 */
	private ArrayList <String>  amountHoldback;
	/**
	 * the holdback flag
	 */
	private ArrayList <String>  holdbackFlag;

	/**
	 * Payment due date
	 */
	private ArrayList <String>  paymentDueDate;


	/**
	 * the id of the user who created the record
	 */
	private ArrayList <String>  creator;

	/**
	 * Id of the user who last modified the record
	 */
	private ArrayList <String>  lastModifiedBy;

	/**
	 * the IP address of the user who created or modified the record
	 */
	private ArrayList <String>  iPAdd;

	/** indicates if it is high level or detailed record*/
	private  ArrayList <String>  detailType;

	/** indicates if detail records will be displayed in final invoice*/
	private ArrayList <String>  displayDetail;

	/** the number of milestones achieved for the invoice detail (will only be stored for the high level record)*/
	private ArrayList <String>  milestonesAchievedCount;


	/** the milestones description */
	private ArrayList <String>  milestonesDesc;

	/** the milestone achievment date */

	private ArrayList <String>  achDate;

	/** the patient code*/

	private ArrayList <String>  patCode;

	private ArrayList <String>  totalAmount;

	private double invoiceTotal;


	/**
	 *ArrayList  FK to er_mileachieved, will be used for milestone related records. <br>
	 * (this will help identify the exact milestone achievement record for multiple achievements for the same patient and milestone combination)
	 */
	public ArrayList linkMileAchieved;

    public double getInvoiceTotal() {
		return invoiceTotal;
	}




	public void setInvoiceTotal(double invoiceTotal) {
		this.invoiceTotal = invoiceTotal;
	}




	public ArrayList<String> getAmountDue() {
		return amountDue;
	}




	public void setAmountDue(ArrayList<String> amountDue) {
		this.amountDue = amountDue;
	}


	public void setAmountDue(String amountDue) {
		this.amountDue.add(amountDue);
	}


	public ArrayList<String> getAmountInvoiced() {
		return amountInvoiced;
	}




	public void setAmountInvoiced(ArrayList<String> amountInvoiced) {
		this.amountInvoiced = amountInvoiced;
	}

	public void setAmountInvoiced(String amountInvoiced) {
		this.amountInvoiced.add(amountInvoiced);
	}
	
	public ArrayList<String> getAmountHoldback() {
		return amountHoldback;
	}

	public void setAmountHoldback(ArrayList<String> amountHoldback) {
		this.amountHoldback = amountHoldback;
	}

	public void setAmountHoldback(String amountHoldback) {
		this.amountHoldback.add(amountHoldback);
	}

	public ArrayList<String> getCreator() {
		return creator;
	}




	public void setCreator(ArrayList<String> creator) {
		this.creator = creator;
	}


	public void setCreator( String  creator) {
		this.creator.add(creator);
	}



	public ArrayList <String> getDetailType() {
		return detailType;
	}




	public void setDetailType(ArrayList <String>  detailType) {
		this.detailType = detailType;
	}

	public void setDetailType(String  detailType) {
		this.detailType.add(detailType);
	}


	public ArrayList<String> getDisplayDetail() {
		return displayDetail;
	}




	public void setDisplayDetail(ArrayList<String> displayDetail) {
		this.displayDetail = displayDetail;
	}

	public void setDisplayDetail(String displayDetail) {
		this.displayDetail.add(displayDetail);
	}



	public ArrayList<String> getId() {
		return id;
	}




	public void setId(ArrayList<String> id) {
		this.id = id;
	}

	public void setId(String id) {
		this.id.add(id);
	}



	public ArrayList<String> getInvPk() {
		return invPk;
	}




	public void setInvPk(ArrayList<String> invPk) {
		this.invPk = invPk;
	}

	public void setInvPk(String invPk) {
		this.invPk.add(invPk);
	}



	public ArrayList<String> getIPAdd() {
		return iPAdd;
	}




	public void setIPAdd(ArrayList<String> add) {
		iPAdd = add;
	}

	public void setIPAdd(String add) {
		iPAdd.add(add);
	}



	public ArrayList<String> getLastModifiedBy() {
		return lastModifiedBy;
	}




	public void setLastModifiedBy(ArrayList<String> lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy.add(lastModifiedBy);
	}



	public ArrayList<String> getMilestone() {
		return milestone;
	}




	public void setMilestone(ArrayList<String> milestone) {
		this.milestone = milestone;
	}

	public void setMilestone(String milestone) {
		this.milestone.add(milestone);
	}



	public ArrayList<String> getMilestonesAchievedCount() {
		return milestonesAchievedCount;
	}




	public void setMilestonesAchievedCount(ArrayList<String> milestonesAchievedCount) {
		this.milestonesAchievedCount = milestonesAchievedCount;
	}

	public void setMilestonesAchievedCount(String milestonesAchievedCount) {
		this.milestonesAchievedCount.add(milestonesAchievedCount);
	}



	public ArrayList<String> getPatient() {
		return patient;
	}




	public void setPatient(ArrayList<String> patient) {
		this.patient = patient;
	}

	public void setPatient(String patient) {
		this.patient.add(patient);
	}



	public ArrayList<String> getPatProt() {
		return patProt;
	}

	public void setPatProt(ArrayList<String> patProt) {
		this.patProt = patProt;
	}

	public void setPatProt(String patProt) {
		this.patProt.add(patProt);
	}



	public ArrayList<String> getPaymentDueDate() {
		return paymentDueDate;
	}




	public void setPaymentDueDate(ArrayList<String> paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}

	public void setPaymentDueDate(String paymentDueDate) {
		this.paymentDueDate.add(paymentDueDate);
	}



	public ArrayList<String> getStudy() {
		return study;
	}




	public void setStudy(ArrayList<String> study) {
		this.study = study;
	}

	public void setStudy(String study) {
		this.study.add(study);
	}




	public ArrayList<String> getMilestonesDesc() {
		return milestonesDesc;
	}




	public void setMilestonesDesc(ArrayList<String> milestonesDesc) {
		this.milestonesDesc = milestonesDesc;
	}

	public void setMilestonesDesc(String milestonesDesc) {
		this.milestonesDesc.add(milestonesDesc);
	}




	public ArrayList<String> getAchDate() {
		return achDate;
	}




	public void setAchDate(ArrayList<String> achDate) {
		this.achDate = achDate;
	}


	public void setAchDate(String achDate) {
		this.achDate.add(achDate);
	}


	public ArrayList<String> getPatCode() {
		return patCode;
	}




	public void setPatCode(ArrayList<String> patCode) {
		this.patCode = patCode;
	}

	public void setPatCode(String patCode) {
		this.patCode.add(patCode);
	}
	
	public ArrayList<String> getHoldbackFlag() {
		return holdbackFlag;
	}

	public void setHoldbackFlag(ArrayList<String> holdbackFlag) {
		this.holdbackFlag = holdbackFlag;
	}
	
	public void setHoldbackFlag(String holdbackFlag) {
		this.holdbackFlag.add(holdbackFlag);
	}


	public InvoiceDetailDao() {
		super();
		patCode = new ArrayList<String>();
		achDate = new ArrayList<String>();
		milestonesDesc = new ArrayList<String>();
		this.amountDue = new ArrayList<String>();
		this.amountInvoiced = new ArrayList<String>();
		this.amountHoldback = new ArrayList<String>();
		this.creator = new ArrayList<String>();
		this.id = new ArrayList<String>();
		this.invPk = new ArrayList<String>();
		this.iPAdd = new ArrayList<String>();
		this.lastModifiedBy = new ArrayList<String>();
		this.milestone = new ArrayList<String>();
		this.patient = new ArrayList<String>();
		this.patProt = new ArrayList<String>();
		this.paymentDueDate = new ArrayList<String>() ;
		this.study = new ArrayList<String>();
		displayDetail = new ArrayList<String>();
		detailType = new ArrayList<String>();
		milestonesAchievedCount = new ArrayList<String>();
		totalAmount = new ArrayList<String>();
		linkMileAchieved = 	new ArrayList();
	}


	/////////////////////////

	/*Added by Sonia Abrol. 11/10/05 */

    /** returns InvoiceDetailDao with details of a generated invoice (for a milestone type)
     * @param invId : Invoice Id
     * @param milestoneType milestone type : Pm,EM,VM,SM
     * @param htAdditionalParam hashtable of additional parameters.
     * <br>possible values: key: "calledFrom" ; values: INV, PAY
     * */
	public void getInvoiceDetailsForMilestoneType(int invId, String mileStoneType, Hashtable htAdditionalParam) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        StringBuffer sbSQL = new StringBuffer();

        String milestoneRuleDesc = "";
        String ruleDesc = "";
        String patientStatusDesc = "";
        String visitName = "";
        String eventName = "";
        String eventStatus = "";
        String patientCountString = "";
        String patientCount = "";
        String calassocto = "";
        String calName = "";
        double totalInvoiced = 0;
        String detailType = "";

        try {
            conn = getConnection();



            if (mileStoneType.equals("PM") || mileStoneType.equals("SM") || mileStoneType.equals("AM"))
            {
            	sbSQL.append("select pk_invdetail,i.fk_mileachieved, fk_per, a.PK_MILESTONE, a.MILESTONE_COUNT, ");

            	if (!mileStoneType.equals("AM"))
            	    sbSQL.append(" b.codelst_desc patientStatus ,milestone_limit ,  ");

            	sbSQL.append(" i.milestones_achieved ,TO_CHAR(i.amount_due,'99999999990.00') amount_due, TO_CHAR(i.amount_invoiced,'99999999990.00') amount_invoiced, TO_CHAR(i.AMOUNT_HOLDBACK,'99999999990.00') AMOUNT_HOLDBACK, detail_type,  display_detail ");

            	//Modified by Manimaran for the Enh #FIN10
                if (mileStoneType.equals("PM"))
                {
                	sbSQL.append(" , (DECODE( (SELECT EVENT_CALASSOCTO FROM event_assoc WHERE event_id=fk_cal),'S', NVL((SELECT PATPROT_PATSTDID FROM er_patprot patprot WHERE patprot.fk_per = i.fk_per AND patprot.fk_study = i.fk_study AND PATPROT_STAT     =1  ), ' '), NVL(  (SELECT PATPROT_PATSTDID  FROM er_patprot patprot  WHERE patprot.fk_per = i.fk_per  AND patprot.fk_study = i.fk_study  AND PATPROT_STAT     =1  ), '<I>(Patient removed from study)</I>'))) patcodes,to_char(i.achieved_on,PKG_DATEUTIL.F_GET_DATEFORMAT) achieved_on ");

                }
                else if(mileStoneType.equals("SM"))
                {
                	sbSQL.append(" , ' ' patcodes,to_char(i.achieved_on,PKG_DATEUTIL.F_GET_DATEFORMAT) achieved_on ");
                }
                else if(mileStoneType.equals("AM")){
                	sbSQL.append(" , a.milestone_description patientStatus, ' ' patcodes,to_char(i.achieved_on,PKG_DATEUTIL.F_GET_DATEFORMAT) achieved_on ");
                }


                if (!mileStoneType.equals("AM"))
                	sbSQL.append("  from ER_MILESTONE a , er_codelst b ,er_invoice_detail i ");
               	else
               		sbSQL.append("  from ER_MILESTONE a, er_invoice_detail i ");

                sbSQL.append( "where i.fk_inv = ? and fk_milestone = pk_milestone ");
               	sbSQL.append(" and a.MILESTONE_TYPE  = '"+mileStoneType+"' and (display_detail = 1 or detail_type = 'H' ) ");

               	if (!mileStoneType.equals("AM"))
               	    sbSQL.append( "and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' and b.pk_codelst = a.milestone_status order by a.PK_MILESTONE, pk_invdetail");
               	else
               		sbSQL.append( "and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' order by a.PK_MILESTONE, pk_invdetail");



            }

            if (mileStoneType.equals("VM"))
            {
            	sbSQL.append("select pk_invdetail,i.fk_mileachieved, fk_per, a.PK_MILESTONE,  a.FK_CAL, c.EVENT_CALASSOCTO, c.name  cal_desc,  a.FK_CODELST_RULE, ");
            	sbSQL.append(" b.codelst_desc  rule_desc,  ");
            	sbSQL.append(" a.MILESTONE_COUNT, ");
            	sbSQL.append(" a.fk_visit fk_visit ,nvl((select v.visit_name from sch_protocol_visit v where a.fk_visit = v.pk_protocol_visit ),'All') visit_name , ");
            	sbSQL.append(" (select codelst_desc from er_codelst where pk_codelst = milestone_status ) patientStatus , milestone_limit ,");
            	sbSQL.append(" (select codelst_desc from sch_codelst where pk_codelst = milestone_eventstatus ) eventStatus , i.milestones_achieved ,");
            	sbSQL.append("  TO_CHAR(i.amount_due,'99999999990.00') amount_due, TO_CHAR(i.amount_invoiced,'99999999990.00') amount_invoiced , TO_CHAR(i.AMOUNT_HOLDBACK,'99999999990.00') AMOUNT_HOLDBACK, detail_type , display_detail,(DECODE( (SELECT EVENT_CALASSOCTO FROM event_assoc WHERE event_id=fk_cal),'S', NVL((SELECT PATPROT_PATSTDID FROM er_patprot patprot WHERE patprot.fk_per = i.fk_per AND patprot.fk_study = i.fk_study AND PATPROT_STAT     =1  ), ' '), NVL(  (SELECT PATPROT_PATSTDID  FROM er_patprot patprot  WHERE patprot.fk_per = i.fk_per  AND patprot.fk_study = i.fk_study  AND PATPROT_STAT     =1  ), '<I>(Patient removed from study)</I>'))) patcodes ");
            	sbSQL.append(" ,to_char(i.achieved_on,PKG_DATEUTIL.F_GET_DATEFORMAT) achieved_on  from ER_MILESTONE a, er_codelst b , event_Assoc c ,er_invoice_detail i " );

            	sbSQL.append( " where i.fk_inv = ? and fk_milestone = pk_milestone ");
               	sbSQL.append(" and a.MILESTONE_TYPE  = '"+mileStoneType+"' and (display_detail = 1 or detail_type = 'H' ) ");
            	sbSQL.append(" and a.FK_CODELST_RULE = b.pk_codelst  and a.fk_cal  = c.event_id and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' order by a.PK_MILESTONE, pk_invdetail");
            }


            if (mileStoneType.equals("EM"))
            {
            	sbSQL.append("select pk_invdetail,i.fk_mileachieved, fk_per, a.PK_MILESTONE,  a.FK_CAL, c.EVENT_CALASSOCTO, c.name  cal_desc,  a.FK_CODELST_RULE, ");
            	sbSQL.append(" b.codelst_desc  rule_desc,  a.FK_EVENTASSOC,");
            	sbSQL.append(" a.MILESTONE_COUNT,  ");
            	sbSQL.append(" a.fk_visit fk_visit ,nvl((select v.visit_name from sch_protocol_visit v where a.fk_visit = v.pk_protocol_visit ),'All') visit_name , ");
            	sbSQL.append(" (select codelst_desc from er_codelst where pk_codelst = milestone_status ) patientStatus , milestone_limit, ");
            	sbSQL.append(" (select codelst_desc from sch_codelst where pk_codelst = milestone_eventstatus ) eventStatus ,");
            	sbSQL.append(" d.name event_desc , i.milestones_achieved ,");
            	sbSQL.append("  TO_CHAR(i.amount_due,'99999999990.00') amount_due, TO_CHAR(i.amount_invoiced,'99999999990.00') amount_invoiced , TO_CHAR(i.AMOUNT_HOLDBACK,'99999999990.00') AMOUNT_HOLDBACK, detail_type, display_detail,(DECODE( (SELECT EVENT_CALASSOCTO FROM event_assoc WHERE event_id=fk_cal),'S', NVL((SELECT PATPROT_PATSTDID FROM er_patprot patprot WHERE patprot.fk_per = i.fk_per AND patprot.fk_study = i.fk_study AND PATPROT_STAT     =1  ), ' '), NVL(  (SELECT PATPROT_PATSTDID  FROM er_patprot patprot  WHERE patprot.fk_per = i.fk_per  AND patprot.fk_study = i.fk_study  AND PATPROT_STAT     =1  ), '<I>(Patient removed from study)</I>'))) patcodes ");
            	sbSQL.append(" ,to_char(i.achieved_on,PKG_DATEUTIL.F_GET_DATEFORMAT) achieved_on from ER_MILESTONE a, er_codelst b , event_Assoc c , event_Assoc d ,er_invoice_detail i " );

            	sbSQL.append( " where i.fk_inv = ? and fk_milestone = pk_milestone ");
               	sbSQL.append(" and a.MILESTONE_TYPE  = '"+mileStoneType+"' and (display_detail = 1 or detail_type = 'H' ) ");

            	sbSQL.append(" and a.FK_EVENTASSOC  = d.event_id  and a.FK_CODELST_RULE = b.pk_codelst  and a.fk_cal  = c.event_id and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' order by a.PK_MILESTONE, pk_invdetail ");
            }


            Rlog.debug("milestone", sbSQL.toString());

            pstmt = conn.prepareStatement(sbSQL.toString());
            pstmt.setInt(1, invId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {


                patientCount =rs.getString("MILESTONE_COUNT");

                if (StringUtil.isEmpty(patientCount))
                {
                	patientCount = "";

                }

                patientStatusDesc = rs.getString("patientStatus");

                //Modified by Manimaran to fix the issue #3300
                if (StringUtil.isEmpty(patientStatusDesc))
                {

                patientStatusDesc = "";

                }
                else if(mileStoneType.equals("AM"))
                {
                	patientStatusDesc =  patientStatusDesc ;

                }
                else
                {
                	patientStatusDesc = "'" + patientStatusDesc  + "'";

                }

                if (mileStoneType.equals("EM") || mileStoneType.equals("VM"))
                {
                	ruleDesc = rs.getString("RULE_DESC");
                    visitName = rs.getString("VISIT_NAME");
                    calName =  rs.getString("CAL_DESC");
                    calassocto = rs.getString("EVENT_CALASSOCTO");

                    ruleDesc = ", "+LC.L_Rule+": " + ruleDesc;
                    eventStatus = rs.getString("eventStatus");

                    if (StringUtil.isEmpty(eventStatus))
                    	eventStatus = "";
                }


                if (mileStoneType.equals("EM"))
                {

                	eventName = rs.getString("EVENT_DESC");

                }

                if(!"SM".equals(mileStoneType)){
	                if (!"S".equals(calassocto)){
		                 if (StringUtil.stringToNum(patientCount) <= 0)
		                 {
		                	 patientCountString = LC.L_Every+" " + patientStatusDesc + " " + LC.Pat_Patient_Lower;//KM-for November Enhancement #FIN6.
		                	 //patientCountString = "Every " + patientStatusDesc + " patient ";
		                 }else
		                 {
		                	 patientCountString = patientCount + " " + patientStatusDesc + " " + LC.Pat_Patients_Lower;
		                 }
	                }
                
	                if (calassocto.trim().equals("S")){
	                	patientCountString = "";
	                }
                } else{	
                  	 patientCountString = LC.L_Every+" " + patientStatusDesc; 
                }
                
                 if (! StringUtil.isEmpty(eventName))
                 {
                	 eventName = ", "+LC.L_Event+" : " + eventName;
                 }


                if (mileStoneType.equals("EM") || mileStoneType.equals("VM"))
                {
                	if (!"S".equals(calassocto)){
                		milestoneRuleDesc = LC.L_Calendar +": " + calName + ", " + LC.L_Visit +": " + visitName + eventName + ruleDesc +  eventStatus + " [" + patientCountString + "]";
                	} else {
                		milestoneRuleDesc = LC.L_Calendar +": " + calName + ", " + LC.L_Visit +": " + visitName + eventName + ruleDesc +  eventStatus;
                	}
                }
                else
                {
                	 milestoneRuleDesc =  patientCountString ;
                }

                if(mileStoneType.equals("AM"))
                	milestoneRuleDesc =  patientStatusDesc;


                 setMilestone(rs.getString("PK_MILESTONE"));
                 this.setMilestonesDesc(milestoneRuleDesc);
                 this.setMilestonesAchievedCount(rs.getString("milestones_achieved"));
                 this.setAchDate(rs.getString("achieved_on"));
                 this.setAmountDue(rs.getString("amount_due"));

                 this.setAmountInvoiced(rs.getString("amount_invoiced"));
                 this.setAmountHoldback(rs.getString("AMOUNT_HOLDBACK"));
                 if (!"S".equals(calassocto)){
                	 this.setPatCode( rs.getString("patcodes"));
                 } else {
                	 this.setPatCode("");
                 }

                 detailType = rs.getString("detail_type");

                 if (detailType.equals("H"))
                 {
                	 totalInvoiced = totalInvoiced +  Double.parseDouble(rs.getString("amount_invoiced"));
                 }

                 this.setDetailType(detailType) ;
                 this.setDisplayDetail(rs.getString("display_detail"));
                 this.setPatient(rs.getString("fk_per"));

                 this.setId(rs.getString("pk_invdetail"));
                 this.setLinkMileAchieved(rs.getString("fk_mileachieved"));

                 //this.setTotalAmount(rs.getString("total_amount_invoiced"));

                rows++;
                Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows "+ rows);
            }

            setInvoiceTotal(totalInvoiced);
        } catch (SQLException ex) {
            Rlog.fatal("milestone","MilestoneDao.getMilestoneRows EXCEPTION IN FETCHING FROM Milestone table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

	/**Added by Raviesh for FIN-20599 
	 * returns InvoiceDetailDao with details of a generated invoice (for Event and Additional milestone type)*/
	public void getInvoiceDetailsForMilestoneTypeEMAM(int invId, String mileStoneType, Hashtable htAdditionalParam) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        StringBuffer sbSQL = new StringBuffer();

        String milestoneRuleDesc = "";
        String ruleDesc = "";
        String patientStatusDesc = "";
        String visitName = "";
        String eventName = "";
        String eventStatus = "";
        String patientCountString = "";
        String patientCount = "";
        String calName = "";
        double totalInvoiced = 0;
        String detailType = "";
        String CoverType = "";
        String calassocto = "";

        try {
            conn = getConnection();



            if (mileStoneType.equals("AM"))
            {
            	sbSQL.append("select pk_invdetail,i.fk_mileachieved, fk_per, a.PK_MILESTONE, a.MILESTONE_COUNT, ' ' EVENT_CALASSOCTO, ");
            	sbSQL.append(" i.milestones_achieved ,TO_CHAR(i.amount_due,'99999999990.00') amount_due, TO_CHAR(i.amount_invoiced,'99999999990.00') amount_invoiced, TO_CHAR(i.AMOUNT_HOLDBACK,'99999999990.00') AMOUNT_HOLDBACK,  detail_type,  display_detail ");
            	sbSQL.append(" , a.milestone_description patientStatus, ' ' patcodes,to_char(i.achieved_on,PKG_DATEUTIL.F_GET_DATEFORMAT) achieved_on ");
                sbSQL.append(", ('-') as CoverType ");
                sbSQL.append(" from ER_MILESTONE a, er_invoice_detail i ");
                sbSQL.append( "where i.fk_inv = ? and fk_milestone = pk_milestone ");
               	sbSQL.append(" and a.MILESTONE_TYPE  = '"+mileStoneType+"' and (display_detail = 1 or detail_type = 'H' ) ");
            	sbSQL.append( "and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' order by a.PK_MILESTONE, pk_invdetail");
            }
            
            if (mileStoneType.equals("EM"))
            {
            	sbSQL.append("select pk_invdetail,i.fk_mileachieved, i.fk_per fk_per, a.PK_MILESTONE,  a.FK_CAL, ");
            	sbSQL.append("(select c.EVENT_CALASSOCTO from event_Assoc c  where c.event_id = a.fk_cal) EVENT_CALASSOCTO, ");
            	sbSQL.append("(select c.name from event_Assoc c  where c.event_id = a.fk_cal) cal_desc,  a.FK_CODELST_RULE, ");
            	sbSQL.append(" b.codelst_desc  rule_desc,  a.FK_EVENTASSOC,");
            	sbSQL.append(" a.MILESTONE_COUNT,  ");
            	sbSQL.append(" a.fk_visit fk_visit ,nvl((select v.visit_name from sch_protocol_visit v where a.fk_visit = v.pk_protocol_visit ),'All') visit_name , ");
            	sbSQL.append(" (select codelst_desc from er_codelst where pk_codelst = milestone_status ) patientStatus , milestone_limit, ");
            	sbSQL.append(" (select codelst_desc from sch_codelst where pk_codelst = milestone_eventstatus ) eventStatus ,");
            	sbSQL.append(" d.name event_desc , i.milestones_achieved ,");
            	sbSQL.append("  TO_CHAR(i.amount_due,'99999999990.00') amount_due, TO_CHAR(i.amount_invoiced,'99999999990.00') amount_invoiced , TO_CHAR(i.AMOUNT_HOLDBACK,'99999999990.00') AMOUNT_HOLDBACK, detail_type, display_detail,(DECODE( (SELECT EVENT_CALASSOCTO FROM event_assoc WHERE event_id=fk_cal),'S', NVL((SELECT PATPROT_PATSTDID FROM er_patprot patprot WHERE patprot.fk_per = i.fk_per AND patprot.fk_study = i.fk_study AND PATPROT_STAT     =1  ), ' '), NVL(  (SELECT PATPROT_PATSTDID  FROM er_patprot patprot  WHERE patprot.fk_per = i.fk_per  AND patprot.fk_study = i.fk_study  AND PATPROT_STAT     =1  ), '<I>(Patient removed from study)</I>'))) patcodes ");
            	sbSQL.append(" ,to_char(i.achieved_on,PKG_DATEUTIL.F_GET_DATEFORMAT) achieved_on," );

            	sbSQL.append(" ('-') as CoverType ");

            	sbSQL.append(" from ER_MILESTONE a, er_codelst b , event_Assoc d, er_invoice_detail i " );
            	sbSQL.append( " where i.fk_inv = ? and i.fk_milestone = pk_milestone");
               	sbSQL.append(" and a.MILESTONE_TYPE  = '"+mileStoneType+"' and (display_detail = 1 or detail_type = 'H' ) ");                  
            	sbSQL.append(" and a.FK_EVENTASSOC  = d.event_id and a.FK_CODELST_RULE = b.pk_codelst and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y'" );
            	sbSQL.append(" order by a.PK_MILESTONE, pk_invdetail ");
            }


            Rlog.debug("milestone", sbSQL.toString());

            pstmt = conn.prepareStatement(sbSQL.toString());
            pstmt.setInt(1, invId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {


                patientCount =rs.getString("MILESTONE_COUNT");

                if (StringUtil.isEmpty(patientCount))
                {
                	patientCount = "";

                }

                patientStatusDesc = rs.getString("patientStatus");

                //Modified by Manimaran to fix the issue #3300
                if (StringUtil.isEmpty(patientStatusDesc))
                {

                patientStatusDesc = "";

                }
                else if(mileStoneType.equals("AM"))
                {
                	patientStatusDesc =  patientStatusDesc ;

                }
                else
                {
                	patientStatusDesc = "'" + patientStatusDesc  + "'";

                }

                if (mileStoneType.equals("EM"))
                {
                	ruleDesc = rs.getString("RULE_DESC");
                    visitName = rs.getString("VISIT_NAME");
                    calName =  rs.getString("CAL_DESC");
                    calassocto = rs.getString("EVENT_CALASSOCTO");

                    ruleDesc = ", "+ LC.L_Rule +": " + ruleDesc;
                    eventStatus = rs.getString("eventStatus");

                    if (StringUtil.isEmpty(eventStatus))
                    	eventStatus = "";
                }


                if (mileStoneType.equals("EM"))
                {
                	eventName = rs.getString("EVENT_DESC");
                	CoverType = rs.getString("CoverType");
                }

                calassocto = rs.getString("EVENT_CALASSOCTO");
                //TO  handle the null pointer Exception Bug#16811
                if(StringUtil.isEmpty(calassocto))calassocto="";
                if (calassocto!="" && !"S".equals(calassocto) ){
	                 if (StringUtil.stringToNum(patientCount) <= 0)
	                 {
	                	 patientCountString = LC.L_Every+" " + patientStatusDesc + " " + LC.L_Patient;

	                 }else
	                 {
	                	 patientCountString = patientCount + " " + patientStatusDesc + " " + LC.L_Patients;
	                  }
                 }
                if (calassocto.trim().equals("S")){
                	patientCountString = "";
                }

                 if (! StringUtil.isEmpty(eventName))
                 {
                	 eventName = ", "+LC.L_Event+": " + eventName;
                 }


                if (mileStoneType.equals("EM"))
                {
                	if ("S".equals(calassocto)){
                		milestoneRuleDesc = LC.L_Calendar +": " + calName + ", " + LC.L_Visit +": " + visitName + eventName + ruleDesc +  eventStatus;
                	} else {
                		if(StringUtil.isEmpty(calassocto)){
                			milestoneRuleDesc = LC.L_Calendar +": " + calName + ", " + LC.L_Visit +": " + visitName + eventName + ruleDesc +  eventStatus;
                		}else{
                			milestoneRuleDesc = LC.L_Calendar +": " + calName + ", " + LC.L_Visit +": " + visitName + eventName + ruleDesc +  eventStatus + " [" + patientCountString + "]";	
                		}
                	}
                }
                else
                {
                	 milestoneRuleDesc =  patientCountString ;
                }

                if(mileStoneType.equals("AM")){
           	 		milestoneRuleDesc =  patientStatusDesc;
                }
                 this.setMilestone(rs.getString("PK_MILESTONE"));
                 this.setMilestonesDesc(milestoneRuleDesc);
                 this.setMilestonesAchievedCount(rs.getString("milestones_achieved"));
                 this.setAchDate(rs.getString("achieved_on"));
                 this.setAmountDue(rs.getString("amount_due"));

                 this.setAmountInvoiced(rs.getString("amount_invoiced"));
                 this.setAmountHoldback(rs.getString("AMOUNT_HOLDBACK"));
                 this.setPatCode( rs.getString("patcodes"));

                 detailType = rs.getString("detail_type");

                 if (detailType.equals("H"))
                 {
                	 totalInvoiced = totalInvoiced +  Double.parseDouble(rs.getString("amount_invoiced"));
                 }

                 this.setDetailType(detailType) ;
                 this.setDisplayDetail(rs.getString("display_detail"));
                 this.setPatient(rs.getString("fk_per"));

                 this.setId(rs.getString("pk_invdetail"));
                 this.setLinkMileAchieved(rs.getString("fk_mileachieved"));

                 //this.setTotalAmount(rs.getString("total_amount_invoiced"));

                rows++;
                Rlog.debug("milestone", "MilestoneDao.getInvoiceDetailsForMilestoneTypeEMAM rows "+ rows);
            }

            setInvoiceTotal(totalInvoiced);
        } catch (SQLException ex) {
            Rlog.fatal("milestone","MilestoneDao.getInvoiceDetailsForMilestoneTypeEMAM EXCEPTION IN FETCHING FROM Milestone table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }



	public ArrayList<String> getTotalAmount() {
		return totalAmount;
	}




	public void setTotalAmount(ArrayList<String> totalAmount) {
		this.totalAmount = totalAmount;
	}


	public void setTotalAmount(String totalAmount) {
		this.totalAmount.add(totalAmount);
	}

	public ArrayList getLinkMileAchieved() {
		return linkMileAchieved;
	}


	public void setLinkMileAchieved(ArrayList linkMileAchieved) {
		this.linkMileAchieved = linkMileAchieved;
	}

	public void setLinkMileAchieved(String lma) {
		this.linkMileAchieved.add(lma);
	}

	/*
     * Method to synchronize the amount_invoiced in heaader/footer recordd after an invoice update
     */

    public int synchInvoicedAmount(int invPK, int user, String ipAdd) {
        int retValue = 0;


        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            cstmt = conn.prepareCall("{call Pkg_Milestone_New.SP_SYNCH_INV_DETAILS (?,?,?,?)}");
            cstmt.setInt(1, invPK);
            cstmt.setInt(2, user);
            cstmt.setString(3, ipAdd);

            cstmt.registerOutParameter(4, java.sql.Types.INTEGER);

            cstmt.execute();
            retValue = cstmt.getInt(4);

            if (retValue < 0)
            {
            	Rlog.fatal("milestone",
                        "EXCEPTION in InvoiceDetailDao.synchInvoicedAmount, excecuting Stored Procedure ,check database log table");

            }

            return retValue;

        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "EXCEPTION in InvoiceDetailDao.synchInvoicedAmount, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }


    // end of class

}
