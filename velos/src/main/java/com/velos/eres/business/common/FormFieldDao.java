/*
 * Classname			FormField.class
 * 
 * Version information 	1.0
 *
 * Date					24/07/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

/**
 * FormFieldDao for getting FormField records
 * 
 * @author Sonia Kaura
 * @version : 1.0 05/07/2003
 */

public class FormFieldDao extends CommonDAO implements java.io.Serializable {

    private ArrayList formFldId;

    private ArrayList formFldMandatory;

    private ArrayList formFldBrowserFlg;

    private ArrayList formFldSeq;

    private ArrayList formSecId;;
    private ArrayList fldSystemId;
    private ArrayList formFldDataType;
    private ArrayList formFldLinesNos;
    private int rows; // to get the number of rows of the incoming result set

    public void FormFieldDao() {

        formFldId = new ArrayList();
        formFldMandatory = new ArrayList();
        formFldBrowserFlg = new ArrayList();
        formFldSeq = new ArrayList();
        formSecId = new ArrayList();
        fldSystemId = new ArrayList();
        formFldDataType = new ArrayList();
        formFldLinesNos=new ArrayList();
    }
    public FormFieldDao(){
    	formFldId = new ArrayList();
        formFldMandatory = new ArrayList();
        formFldBrowserFlg = new ArrayList();
        formFldSeq = new ArrayList();
        formSecId = new ArrayList();
        fldSystemId = new ArrayList();
        formFldDataType = new ArrayList();	
        formFldLinesNos=new ArrayList();
    }
    // /////////////////////////////////////
    // SETTERS AND GETTERS

    public ArrayList getFormFldId() {
        return this.formFldId;
    }

    public void setFormFldId(ArrayList formFldId) {
        this.formFldId = formFldId;
    }

    public ArrayList getFormFldMandatory() {
        return this.formFldMandatory;
    }

    public void setFormFldMandatory(ArrayList formFldMandatory) {
        this.formFldMandatory = formFldMandatory;
    }

    public ArrayList getFormFldBrowserFlg() {
        return this.formFldBrowserFlg;
    }

    public void setFormFldBrowserFlg(ArrayList formFldBrowserFlg) {
        this.formFldBrowserFlg = formFldBrowserFlg;
    }

    public ArrayList getFormFldSeq() {
        return this.formFldSeq;
    }

    public void setFormFldSeq(ArrayList formFldSeq) {
        this.formFldSeq = formFldSeq;
    }

    public int getRows() {
        return this.rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public ArrayList getFormSecId() {
        return this.formSecId;
    }

    public void setFormSecId(ArrayList formSecId) {
        this.formSecId = formSecId;
    }
    public ArrayList getFldSystemId() {
		return fldSystemId;
	}

	public void setFldSystemId(ArrayList fldSystemId) {
		this.fldSystemId = fldSystemId;
	}
	public ArrayList getFormFldDataType() {
		return formFldDataType;
	}

	public void setFormFldDataType(ArrayList formFldDataType) {
		this.formFldDataType = formFldDataType;
	}
	
	
	public ArrayList getFormFldLinesNos() {
		return formFldLinesNos;
	}

	public void setFormFldLinesNos(ArrayList formFldLinesNos) {
		this.formFldLinesNos = formFldLinesNos;
	}
    // // // // // /
    // ////////////////////////////////////////////////////////////////////

    // END OF SETTERS AND GETTERS

    // //////////////////////////////////////////////////////////
    // SINGLE ELEMENT GETTERS AND SETTERS

    public Object getFormFldId(int i) {
        return this.formFldId.get(i);
    }

    public void setFormFldId(Integer formFldId) {
        this.formFldId.add(formFldId);
    }

    public Object getFormFldMandatory(int i) {
        return this.formFldMandatory.get(i);
    }

    public void setFormFldMandatory(String formFldMandatory) {
        this.formFldMandatory.add(formFldMandatory);
    }

    public Object getFormFldBrowserFlg(int i) {
        return this.formFldBrowserFlg.get(i);
    }

    public void setFormFldBrowserFlg(String formFldBrowserFlg) {
        this.formFldBrowserFlg.add(formFldBrowserFlg);
    }

    public Object getFormFldSeq(int i) {
        return this.formFldSeq.get(i);
    }

    public void setFormFldSeq(String formFldSeq) {
        this.formFldSeq.add(formFldSeq);
    }

    public Object getFormSecId(int i) {
        return this.formSecId.get(i);
    }

    public void setFormSecId(String formSecId) {
        this.formSecId.add(formSecId);
    }
    public void setFldSystemId(String fldSystemId) {
    	this.fldSystemId.add(fldSystemId);
	}
    public void setFormFldDataType(String formFldDType) {
		this.formFldDataType.add(formFldDType);
	}
    public Object getFormFldLinesNos(int i) {
		return this.formFldLinesNos.get(i);
	}
    
    public void setFormFldLinesNos(String formFldLinesNos) {
        this.formFldLinesNos.add(formFldLinesNos);
    }
    // /////////////////////////////////////////////////////////////////////

    public void getMandatoryAndSequenceOfField(int fldId, int formSecId) {

        int rows = 0;

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("SELECT  PK_FORMFLD, FK_FORMSEC  , FORMFLD_MANDATORY , FORMFLD_SEQ  "
                            + " FORMFLD_BROWSERFLG FROM ER_FORMFLD "
                            + " WHERE FK_FIELD = ? AND  FK_FORMSEC= ? "
                            + " AND RECORD_TYPE <> 'D'");

            pstmt.setInt(1, fldId);
            pstmt.setInt(2, formSecId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setFormFldId(new Integer(rs.getInt("PK_FORMFLD")));
                setFormSecId((new Integer(rs.getInt("FK_FORMSEC"))).toString());
                setFormFldMandatory((new Integer(rs.getInt("FORMFLD_MANDATORY")))
                        .toString());
                setFormFldSeq((new Integer(rs.getInt("FORMFLD_SEQ")))
                        .toString());
                setFormFldBrowserFlg((new Integer(rs
                        .getInt("FORMFLD_BROWSERFLG"))).toString());
                rows++;
                Rlog.debug("formfield",
                        "FormFldDao.getMandatoryAndSequenceOfField rows "
                                + rows);
            }

            setRows(rows);

        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "formfield",
                            "FormFldDao.getMandatoryAndSequenceOfField EXCEPTION IN FETCHING FROM ER_FORMFLD TABLE"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public int repeatLookupField(int pkField, int formField, int section) {

        int ret = 0;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            Rlog.debug("fieldlib", "In repeatLookupField() ");

            cstmt = conn
                    .prepareCall("{call PKG_LOOKUP.SP_REPEAT_LOOKUP(?,?,?,?)}");

            cstmt.setInt(1, pkField);
            cstmt.setInt(2, formField);
            cstmt.setInt(3, section);

            cstmt.registerOutParameter(4, java.sql.Types.INTEGER);
            cstmt.execute();
            ret = cstmt.getInt(4);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "EXCEPTION in repeatLookupField, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    public void findFieldByUID(int formId, String fieldUID) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("SELECT PK_FORMFLD, FK_FORMSEC, FORMFLD_MANDATORY, FORMFLD_SEQ"
            + " FORMFLD_BROWSERFLG FROM ER_FORMFLD "
            + " WHERE EXISTS (SELECT * FROM ER_FORMSEC WHERE FK_FORMLIB = ? AND PK_FORMSEC = FK_FORMSEC) "
            + " AND EXISTS (SELECT * FROM ER_FIELDLIB WHERE PK_FIELD = FK_FIELD AND FLD_UNIQUEID = ?) "
            + " AND RECORD_TYPE <> 'D'");

            pstmt.setInt(1, formId);
            pstmt.setString(2, fieldUID);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setFormFldId(new Integer(rs.getInt("PK_FORMFLD")));
                setFormSecId((new Integer(rs.getInt("FK_FORMSEC"))).toString());
                setFormFldMandatory((new Integer(rs.getInt("FORMFLD_MANDATORY"))).toString());
                setFormFldSeq((new Integer(rs.getInt("FORMFLD_SEQ"))).toString());
                setFormFldBrowserFlg((new Integer(rs.getInt("FORMFLD_BROWSERFLG"))).toString());
                rows++;
                Rlog.debug("formfield", "FormFldDao.getMandatoryAndSequenceOfField rows "+ rows);
            }

            setRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal(
                "formfield",
                "FormFldDao.findFieldByUID EXCEPTION IN FETCHING FROM ER_FORMFLD TABLE"+ ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    public void getFormfieldDetails(int formId){
    	PreparedStatement pstmt = null;
        Connection conn = null;
        String sql="";
        try {
        	 conn = getConnection();
        	 sql=" select pk_field,fld_systemid, fld_datatype, fld_linesno from er_mapform INNER JOIN er_fldlib ON  MP_PKFLD = pk_field where FK_FORM = ? order by pk_field";
        	 pstmt = conn.prepareStatement(sql);
        	 pstmt.setInt(1, formId);
        	 ResultSet rs = pstmt.executeQuery();
        	 while (rs.next()) {
       		setFldSystemId(rs.getString("fld_systemid"));
       		setFormFldDataType(rs.getString("fld_datatype"));
       		setFormFldId(new Integer(rs.getInt("pk_field")));
       		setFormFldLinesNos(new Integer(rs.getInt("fld_linesno")).toString());
        	 }
        } catch (Exception ex) {
            Rlog.fatal(
                    "formfield",
                    "FormFldDao.findFieldByUID EXCEPTION IN FETCHING FROM ER_FORMFLD TABLE"+ ex);
            } finally {
                try {
                    if (pstmt != null)
                        pstmt.close();
                } catch (Exception e) {
                }
                try {
                    if (conn != null)
                        conn.close();
                } catch (Exception e) {
                }

            }
    }
    public int findFieldDetails(int reponseId ,String fieldId, String formType){
   	  int tempField=0;
        PreparedStatement pstmt = null;
        Connection conn = null;
       String selectStr="";
       String fromStr="";
       String joinStr="";
       String whereStr="";
        if("PS".equals(formType)){
        	selectStr=" select fpat.fk_formlib, ";	
        	fromStr=" from er_patforms fpat ";
        	joinStr=" fpat.fk_formlib ";
        	whereStr=" where pk_patforms ";
        }
        else if("S".equals(formType)){
        	selectStr=" select fstudy.fk_formlib, ";
        	fromStr=" FROM er_studyforms fstudy ";
        	joinStr=" fstudy.fk_formlib ";
        	whereStr=" where pk_studyforms ";
        }
        
        String sql = "";
        try{
        	 conn = getConnection();
        	sql= selectStr+" fsec.pk_formsec,fsec.formsec_name "
        	+" ,frmfld.fk_field,fld.fld_systemid "
        	+ fromStr+" inner join er_formsec fsec on fsec.fk_formlib="+joinStr+" inner join er_formfld frmfld "
        	+" on frmfld.fk_formsec=fsec.pk_formsec inner join er_fldlib fld "
        	+" on fld.pk_field=frmfld.fk_field "
        	+whereStr+" =? and fld.fld_systemid=? ";
        	 pstmt = conn.prepareStatement(sql);
        	 pstmt.setInt(1, reponseId);
        	 pstmt.setString(2, fieldId);
        	 ResultSet rs = pstmt.executeQuery();
        	 while (rs.next()) {
        		 tempField=rs.getInt("fk_field");
        	 }
        }
        catch (Exception ex) {
            Rlog.fatal(
                    "formfield",
                    "FormFldDao.findFieldByUID EXCEPTION IN FETCHING FROM ER_FORMFLD TABLE"+ ex);
            } finally {
                try {
                    if (pstmt != null)
                        pstmt.close();
                } catch (Exception e) {
                }
                try {
                    if (conn != null)
                        conn.close();
                } catch (Exception e) {
                }

            }
   	return tempField;
   }
}
