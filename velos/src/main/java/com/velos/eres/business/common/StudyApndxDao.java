package com.velos.eres.business.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.sql.rowset.serial.SerialBlob;

import org.apache.commons.io.IOUtils;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFFooter;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTText;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STHdrFtr;

import oracle.sql.BLOB;
import oracle.jdbc.OracleResultSet;

import com.aithent.file.uploadDownload.Configuration;
import com.aspose.words.Document;
import com.aspose.words.FontSettings;
import com.aspose.words.License;
import com.aspose.words.SaveFormat;
import com.itextpdf.awt.geom.Rectangle;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.parser.FilteredTextRenderListener;
import com.itextpdf.text.pdf.parser.LocationTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.itextpdf.text.pdf.parser.RegionTextRenderFilter;
import com.itextpdf.text.pdf.parser.RenderFilter;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;
import com.velos.controller.ConfigLoader;
import com.velos.eres.service.util.Bundles;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.ReportIO;
import com.velos.eres.service.util.Rlog;

public class StudyApndxDao extends CommonDAO implements java.io.Serializable {

	private static final long serialVersionUID = 1794672892034414268L;

	private static final String COMMA_STR = ",";
	
	private static final String GetUriByPkSql = "select STUDYAPNDX_URI from ER_STUDYAPNDX where PK_STUDYAPNDX = ? ";
	
	private static final String GetNonNullBlobSql = " select PK_STUDYAPNDX from ER_STUDYAPNDX where "
			+ " nvl(length(STUDYAPNDX_FILEOBJ), 0) <> 0 and PK_STUDYAPNDX in (%s) ";
	
	private static final String GetStudyApndxPksForVrsnSql = "SELECT sa.PK_STUDYAPNDX"
			+ " FROM ER_STUDYVER a ,"
			+ " (SELECT CODELST_DESC, CODELST_SUBTYP, PK_STATUS, STATUS_MODPK, FK_CODELST_STAT"
			+ " FROM er_studyver i, ER_STATUS_HISTORY, er_codelst c"
			+ " WHERE i.fk_study     = ?  AND i.PK_STUDYVER    = STATUS_MODPK  AND STATUS_MODTABLE  = 'er_studyver'"
			+ " AND STATUS_END_DATE IS NULL  AND c.pk_codelst     = FK_CODELST_STAT  ) stat,  ER_STUDYAPNDX sa"
			+ " WHERE a.FK_STUDY       = ? AND STATUS_MODPK (+) = pk_studyver AND a.PK_STUDYVER = sa.FK_STUDYVER"
			+ " AND f_to_number(a.studyver_number) = f_to_number(?)";
	
	private static final String GetStudyApndxPksForAllVrsnSql = "SELECT sa.PK_STUDYAPNDX"
			+ " FROM ER_STUDYVER a ,"
			+ " (SELECT CODELST_DESC, CODELST_SUBTYP, PK_STATUS, STATUS_MODPK, FK_CODELST_STAT"
			+ " FROM er_studyver i, ER_STATUS_HISTORY, er_codelst c"
			+ " WHERE i.fk_study     = ?  AND i.PK_STUDYVER    = STATUS_MODPK  AND STATUS_MODTABLE  = 'er_studyver'"
			+ " AND STATUS_END_DATE IS NULL  AND c.pk_codelst     = FK_CODELST_STAT  ) stat,  ER_STUDYAPNDX sa"
			+ " WHERE a.FK_STUDY       = ? AND STATUS_MODPK (+) = pk_studyver AND a.PK_STUDYVER = sa.FK_STUDYVER";
	
	private static final String GetStudyApndxMaxVrsnSql = "SELECT MAX(F_TO_NUMBER(STUDYVER_NUMBER)) STUDYVER_NUMBER"
			+ " FROM ER_STUDYVER a , (SELECT CODELST_DESC, CODELST_SUBTYP, PK_STATUS, STATUS_MODPK, FK_CODELST_STAT"
			+ " FROM er_studyver i, ER_STATUS_HISTORY, er_codelst c WHERE i.fk_study     = ? AND i.PK_STUDYVER    = STATUS_MODPK"
			+ " AND STATUS_MODTABLE  = 'er_studyver' AND STATUS_END_DATE IS NULL AND c.pk_codelst     = FK_CODELST_STAT ) stat,  ER_STUDYAPNDX sa"
			+ " WHERE a.FK_STUDY       = ? AND STATUS_MODPK (+) = pk_studyver AND a.PK_STUDYVER = sa.FK_STUDYVER";
;
	private static final String checkNumericVrsnsSql = "SELECT COUNT(*) FROM"
			+ " (SELECT REGEXP_SUBSTR(STUDYVER_NUMBER,'^\\d*\\.{0,1}\\d+$') STUDYVER_NUMBER"
			+ " FROM ER_STUDYVER a , (SELECT CODELST_DESC, CODELST_SUBTYP, PK_STATUS, STATUS_MODPK, FK_CODELST_STAT"
			+ " FROM er_studyver i, ER_STATUS_HISTORY, er_codelst c WHERE i.fk_study     = ?"
			+ " AND i.PK_STUDYVER    = STATUS_MODPK AND STATUS_MODTABLE  = 'er_studyver' AND STATUS_END_DATE IS NULL"
			+ " AND c.pk_codelst     = FK_CODELST_STAT ) stat WHERE a.FK_STUDY     = ?"
			+ " AND STATUS_MODPK (+) = pk_studyver ) STUDYVER WHERE STUDYVER_NUMBER IS NULL" ;
	
	private static final String GetStudyApndxDetails = "select "
			+ " a.pk_studyapndx, v.studyver_number, c.codelst_subtyp, a.studyapndx_uri, a.studyapndx_desc, "
			+ " (select codelst_subtyp from er_codelst where pk_codelst = v.studyver_type) type "
			+ " from er_studyapndx a, er_studyver v, er_codelst c where c.pk_codelst = v.studyver_category and "
			+ " a.fk_studyver = v.pk_studyver and a.pk_studyapndx in (%s) ";
	
	private static final String GetFileBlobsforStamping = "select studyapndx_fileobj, pk_studyapndx, stamp_flag, stamp_ref, PRESTAMP_FILEOBJ, PRESTAMP_FILEURI, studyapndx_uri, studyapndx_file from er_studyapndx where fk_studyver=? ";
	
	private static final String GetFileBlobsforPreStamping = "select studyapndx_fileobj, pk_studyapndx, stamp_flag, stamp_ref, PRESTAMP_FILEOBJ, PRESTAMP_FILEURI, studyapndx_uri, studyapndx_file from er_studyapndx where stamp_ref=? ";
	
	private String studyApndxUri = null;
	private ArrayList<Integer> studyApndxPkList = new ArrayList<Integer>();
	private ArrayList<String> studyApndxVersionList = new ArrayList<String>();
	private ArrayList<String> studyApndxCategorySubtypeList = new ArrayList<String>();
	private ArrayList<String> studyApndxTypeSubtypeList = new ArrayList<String>();
	private ArrayList<String> studyApndxFilenameList = new ArrayList<String>();
	private ArrayList<String> studyApndxDescList = new ArrayList<String>();
	
	public String getStudyApndxUri() { return studyApndxUri; }
	public ArrayList<Integer> getStudyApndxPkList() { return studyApndxPkList; }
	public ArrayList<String> getStudyApndxVersionList() { return studyApndxVersionList; }
	public ArrayList<String> getStudyApndxCategorySubtypeList() { return studyApndxCategorySubtypeList; }
	public ArrayList<String> getStudyApndxTypeSubtypeList() { return studyApndxTypeSubtypeList; }
	public ArrayList<String> getStudyApndxFilenameList() { return studyApndxFilenameList; }
	public ArrayList<String> getStudyApndxDescList() { return studyApndxDescList; }
	
	public boolean hasNonNullBlob(List<Integer> pkList) {
		if (pkList == null || pkList.size() < 1) {
			return false;
		}
		StringBuffer pkListSB = new StringBuffer();
		for (Integer aPk : pkList) {
			pkListSB.append(aPk).append(COMMA_STR);
		}
		if (pkListSB.length() > 1) {
			pkListSB.deleteCharAt(pkListSB.length()-1);
		}
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection conn = null;
        boolean foundNonNull = false;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(String.format(GetNonNullBlobSql, pkListSB.toString()));
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	rs.getInt(1);
            	foundNonNull = true;
            	break;
            }
        } catch(Exception e) {
        	Rlog.fatal("StudyApndxDao", "Exception in StudyApndxDao.hasNonNullBlob:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
        return foundNonNull;
	}
	
	public void getStudyApndxDetails(List<Integer> pkList) {
		if (pkList == null || pkList.size() < 1) {
			return;
		}
		StringBuffer pkListSB = new StringBuffer();
		for (Integer aPk : pkList) {
			pkListSB.append(aPk).append(COMMA_STR);
		}
		if (pkListSB.length() > 1) {
			pkListSB.deleteCharAt(pkListSB.length()-1);
		}
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(String.format(GetStudyApndxDetails, pkListSB.toString()));
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	studyApndxPkList.add(rs.getInt(1));
            	studyApndxVersionList.add(rs.getString(2));
            	studyApndxCategorySubtypeList.add(rs.getString(3));
            	studyApndxFilenameList.add(rs.getString(4));
            	studyApndxDescList.add(rs.getString(5));
            	studyApndxTypeSubtypeList.add(rs.getString(6));
            }
        } catch(Exception e) {
        	Rlog.fatal("objectMap", "Exception in StudyApndxDao.getStudyApndxDetails:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
	}
	
	public void getUriByPk(int pk) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(GetUriByPkSql);
            pstmt.setInt(1, pk);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	studyApndxUri = rs.getString(1);
            }
        } catch(Exception e) {
        	Rlog.fatal("objectMap", "Exception in StudyApndxDao.getUriByPk:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
	}
	
	public List<Integer> getStudyApndxPks(int studyPK, String version) {
		List<Integer> pk_List = new ArrayList<Integer>();
		PreparedStatement pstmt = null;
		Connection connCheckNumeric = null;
		Connection connAllVersions = null;
		Connection connMaxVersion = null;
		Connection connVersion = null;
		ResultSet rs = null;
		int nonNumericVersion = 0;
		try {
			connCheckNumeric = getConnection();

			pstmt = connCheckNumeric.prepareStatement(checkNumericVrsnsSql);
			pstmt.setInt(1, studyPK);
			pstmt.setInt(2, studyPK);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				nonNumericVersion = rs.getInt(1);
			}

			if (nonNumericVersion > 0 || version == null || "".equals(version)) {
				connAllVersions = getConnection();
				pstmt = connAllVersions
						.prepareStatement(GetStudyApndxPksForAllVrsnSql);
				pstmt.setInt(1, studyPK);
				pstmt.setInt(2, studyPK);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					pk_List.add(rs.getInt(1));
				}
			} else {
				connVersion = getConnection();
				pstmt = connVersion
						.prepareStatement(GetStudyApndxPksForVrsnSql);
				pstmt.setInt(1, studyPK);
				pstmt.setInt(2, studyPK);
				pstmt.setString(3, version);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					pk_List.add(rs.getInt(1));
				}
			}
		} catch (Exception e) {
			Rlog.fatal("objectMap",
					"Exception in StudyApndxDao.getStudyApndxPks:" + e);
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (Exception e) {
			}
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (connCheckNumeric != null)
					connCheckNumeric.close();
			} catch (Exception e) {
			}
			try {
				if (connAllVersions != null)
					connAllVersions.close();
			} catch (Exception e) {
			}
			try {
				if (connVersion != null)
					connVersion.close();
			} catch (Exception e) {
			}
			try {
				if (connMaxVersion != null)
					connMaxVersion.close();
			} catch (Exception e) {
			}
		}
		return pk_List;
	}
	
	public ArrayList<Integer> getPkByStudyIdAndFkStudyVer(int studyId, int studyVerNum) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        int studyApndxPk=0;
        
        ArrayList<Integer> arList = new ArrayList<Integer>();
        try {
            conn = getConnection();
            String sql = "select pk_studyapndx from er_studyapndx where fk_study=? and fk_studyver=?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);
            pstmt.setInt(2, studyVerNum);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	studyApndxPk = rs.getInt("pk_studyapndx");
            	arList.add(new Integer(studyApndxPk));
            }
        } catch(Exception e) {
        	Rlog.fatal("objectMap", "Exception in StudyApndxDao.getPkByStudyIdAndFkStudyVer:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
        return arList;
	}
	
	public BLOB getStudyApndixFileObject(int studyId, int pkStudyApndx) {	       
        PreparedStatement pstmt = null;
        Connection conn = null;
        BLOB fileObj=null;
        try {
            conn = getConnection();            
            pstmt = conn.prepareStatement("select studyapndx_fileobj from er_studyapndx where fk_study=? and pk_studyapndx=?");
           
            pstmt.setInt(1, studyId);
            pstmt.setInt(2, pkStudyApndx);
            ResultSet rs = pstmt.executeQuery();           
            while (rs.next()) {
               fileObj = (BLOB)rs.getObject(1);   
            }           
        } catch (SQLException ex) {
            Rlog.fatal("Appendix", "AppendixDao.getStudyApndixFileObject EXCEPTION IN FETCHING FROM Appendix table"  + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return fileObj;
    }
    
  
	public void updateStudyApndixFileObject(int studyId, int newpk , String path, BLOB blobObj) {
		PreparedStatement pstmt = null;
        Connection conn = null;     
      
    	try {
			File file = new File(path);         
	        InputStream istr = new FileInputStream(file);	       
	        conn = getConnection(); 
	        pstmt = conn.prepareStatement("Update er_studyapndx set studyapndx_fileobj = empty_blob() where pk_studyapndx = " + newpk);
	        pstmt.execute();
	        pstmt.close();
	        // get the empty blob from the row saved above
	        Statement b_stmt = conn.createStatement();	  
	        String sqlStr = "SELECT studyapndx_fileobj from er_studyapndx where pk_studyapndx = " + newpk + " FOR UPDATE";
	         
	        ResultSet sBlob = b_stmt.executeQuery(sqlStr);
	        	        
	        if (sBlob.next()) {
	            // Get the BLOB locator and open output stream for the BLOB
	            BLOB filBLOB = ((OracleResultSet) sBlob).getBLOB(1);
	            @SuppressWarnings("deprecation")
				OutputStream l_blobOutputStream = filBLOB
	                    .getBinaryOutputStream();
	            byte[] l_buffer = new byte[10 * 1024];
	            int l_nread = 0; // Number of bytes read
	            while ((l_nread = istr.read(l_buffer)) != -1)
	                // Read from file
	                l_blobOutputStream.write(l_buffer, 0, l_nread); // Write to
	            // BLOB
	            // Close both streams
	            istr.close();
	            l_blobOutputStream.close();	            
	        }  
	        conn.commit();	        
	        sBlob.close();
	        b_stmt.close();	       
	        // delete the temporary file that was created on the server
	        file.delete();
    	} catch (Exception ex) {
            Rlog.fatal("Appendix", "AppendixDao.updateStudyApndixFileObject EXCEPTION : " + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }		
	} 
	
	public HashMap<String, Integer> getStudyApndxIdForFileBlob(String version , String prevVersion, int studyId) {		
		HashMap<String, Integer> hMap = new HashMap<String, Integer>();
    	PreparedStatement pstmt = null;
		PreparedStatement pstmt1 = null;
        Connection conn = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        int studyApndxPk=0;
        int studyApndxPk1=0;
        String filePath="";
               
        try {
            conn = getConnection();
            String sql = "select pk_studyapndx , studyapndx_file from er_studyver es , er_studyapndx esa where es.pk_studyver = esa.fk_studyver  and es.fk_study =? and studyver_number=?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);
            pstmt.setString(2, prevVersion);
            rs = pstmt.executeQuery();           
            while (rs.next()) {
            	studyApndxPk = rs.getInt("pk_studyapndx");
            	filePath = rs.getString("studyapndx_file");
            	String sql1 = "select pk_studyapndx from er_studyver es , er_studyapndx esa where  es.pk_studyver=esa.fk_studyver and es.fk_study=? and esa.studyapndx_file=? and es.studyver_number=?";
                pstmt1 = conn.prepareStatement(sql1);
                pstmt1.setInt(1, studyId);
                pstmt1.setString(2, filePath);
                pstmt1.setString(3, version);
                rs1 = pstmt1.executeQuery();
            	
                while (rs1.next()) {
                	studyApndxPk1 = rs1.getInt("pk_studyapndx");
                }
            	hMap.put(new Integer(studyApndxPk).toString() + "*" + filePath , new Integer(studyApndxPk1) );
            }           
        } catch(Exception e) {
        	Rlog.fatal("objectMap", "Exception in StudyApndxDao.getStudyApndxIdForFileBlob:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}
        	try { if (rs1 != null) rs1.close(); } catch (Exception e) {}
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
        	try { if (pstmt1 != null) pstmt1.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }       
		return hMap;		
	}
	
	public ArrayList<String> getStudyVerNumber(int studyId) {		
	 	PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        String studyverNumber="";	        
        Double sVerNum=0.0;	        
        ArrayList<String> arList = new ArrayList<String>();
        try {
            conn = getConnection();
            String sql = "select distinct studyver_number, STUDYVER_MAJORVER, STUDYVER_MINORVER from er_studyver where "
            		+ " fk_study=? order by STUDYVER_MAJORVER desc, STUDYVER_MINORVER desc, f_to_number(studyver_number) desc ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);	           
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	studyverNumber = rs.getString("studyver_number");
            	arList.add(studyverNumber);
            }
        } catch(Exception e) {
        	Rlog.fatal("objectMap", "Exception in StudyApndxDao.getStudyVerNumber:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
        return arList;		
	} 
	
	public Integer addStampingforApproved(int studyVerId,String studyNumber,String approvalDate,String statusId) {		
	 	PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        Blob fileObj = null;
        Blob bkpFileObj = null;
        String bkpFileName = "";
        int pkStudyApndx = 0;
        int ret=0;
        ArrayList<String> arList = new ArrayList<String>();
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(GetFileBlobsforStamping);
            
            pstmt.setInt(1, studyVerId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	if(rs.getInt("stamp_flag")==1){
            		continue;
            	}
            	fileObj = rs.getBlob("studyapndx_fileobj");
            	bkpFileObj = fileObj;
            	pkStudyApndx = rs.getInt("pk_studyapndx");
            	String filename = rs.getString("studyapndx_uri");
            	bkpFileName=filename;
            	String apndx_filename = rs.getString("studyapndx_file");
            	System.out.println("pkStudyApndx---->"+pkStudyApndx);
            	String fnameWOext = filename.substring(0, filename.indexOf("."));
            	InputStream initialStream = fileObj.getBinaryStream();
            	/*byte[] buffer = new byte[initialStream.available()];
                initialStream.read(buffer);
             String srcFilePath = Configuration.UPLOADFOLDER+"\\"+filename;
                File sourceFile = new File(srcFilePath);
                OutputStream outStream = new FileOutputStream(sourceFile);
                outStream.write(buffer);
                String tgtFilePath = Configuration.UPLOADFOLDER+"\\"+fnameWOext+".pdf";
                File targetFile=new File(tgtFilePath);*/
                
            	/*if(filename.toLowerCase().endsWith(".doc") || filename.toLowerCase().endsWith(".docx")){
            		if(isClass("com.aspose.words.License")){
            			Class<?> LicenseClass = Class.forName("com.aspose.words.License");
            			Constructor licConstructor = LicenseClass.getConstructor();
            			Object invoker = licConstructor.newInstance();
            			Method licenseMethod = LicenseClass.getMethod("setLicense", new Class[] { InputStream.class });
            			InputStream licenseStream = null;
            			try{
            				int sType = EJBUtil.getSystemType();
            				String eHome = com.velos.eres.service.util.Configuration.ERES_HOME;
            				String licenseFileName="";
            				 if (sType == 0) {
            					 licenseFileName = eHome + "Aspose.Words.lic";
            				 } else {
            					 licenseFileName = eHome + "Aspose.Words.lic";
            				 }
            				 System.out.println("licenseFileName:::::"+licenseFileName);
            				licenseStream = new FileInputStream(licenseFileName);
            			}catch(FileNotFoundException fne){
            				fne.printStackTrace();
            			}
            			
            			try{
            				if(licenseStream!=null){
            					licenseMethod.invoke(invoker,new Object[] { licenseStream });
            				}
            			}catch (IllegalArgumentException e) { System.out.println("IllegalArgumentException:::"+e.getMessage()); }
            			  catch (IllegalAccessException e) { System.out.println("IllegalAccessException::::"+e.getMessage()); }
            			  catch (InvocationTargetException e) { e.printStackTrace(); }
            			System.out.println("Sys 5");
            			Class<?> DocumentClass = Class.forName("com.aspose.words.Document");
            			Constructor docConstructor = DocumentClass.getConstructor(new Class[] { InputStream.class });
            			Object docInvoker = docConstructor.newInstance(new Object[] { initialStream });
            		    Method saveMethod = DocumentClass.getMethod("save",new Class[] { OutputStream.class, int.class });
            			OutputStream os = new ByteArrayOutputStream();
            			saveMethod.invoke(docInvoker, new Object[] { os,40 });
            			ByteArrayOutputStream bos = (ByteArrayOutputStream) os;
            			byte[] b = bos.toByteArray();
            			fileObj = new SerialBlob(b);
            			apndx_filename = apndx_filename.replace(filename, fnameWOext + ".pdf");
            			filename = fnameWOext + ".pdf";
            			
            			
            		}*/
            		if(filename.toLowerCase().endsWith(".doc") || filename.toLowerCase().endsWith(".docx")){
                		boolean hasLicense = true;
                			InputStream licenseStream = null;
                			System.out.println("After change in Aspose call");
                			try{
                				String eHome = com.velos.eres.service.util.Configuration.ERES_HOME;
                				String licenseFileName=eHome + "Aspose.Words.lic";
                				licenseStream = new FileInputStream(licenseFileName);
                			}catch(FileNotFoundException fne){
                				hasLicense=false;
                			}	
                			System.out.println("hasLicense==="+hasLicense);
                			if(hasLicense){
	                			License license= new License();
	                			license.setLicense(licenseStream);	 
	                			Document doc = new Document(initialStream);
	                		   	OutputStream os = new ByteArrayOutputStream();
	                		   	int sType = EJBUtil.getSystemType();
	                		   	String fontsDirPath="";
	            				if(sType==0){
	            					//Windows
	            					fontsDirPath=Bundles.contextPath+"jsp\\styles\\mscorefonts\\";
	            				}else{
	            					//Linux
	            					fontsDirPath=Bundles.contextPath+"jsp/styles/mscorefonts/";
	            				}
	                		   	FontSettings.getDefaultInstance().setFontsFolder(fontsDirPath, false);
	                		   	HandleDocumentWarnings callback = new HandleDocumentWarnings();
	                		   	System.out.println("fontsDirPath==="+fontsDirPath);
	                		   	doc.setWarningCallback(callback);
	
	                			doc.save(os,SaveFormat.PDF);
	                			ByteArrayOutputStream bos = (ByteArrayOutputStream) os;
	                			byte[] b = bos.toByteArray();
	                			fileObj = new SerialBlob(b);
	                			apndx_filename = apndx_filename.replace(filename, fnameWOext + ".pdf");
	                			filename = fnameWOext + ".pdf";
                			
                			}
            		
            		/*System.out.println("URL:::::"+CFG.FORMAT_CONVERSION_SERVER_URL);
            		if(!"TBD".equals(CFG.FORMAT_CONVERSION_SERVER_URL)){
            			System.out.println("Inside FORMAT_CONVERSION");
            			IConverter converter = RemoteConverter.builder()
            	                .baseFolder(new File(Configuration.UPLOADFOLDER))
            	                .workerPool(20, 25, 2, TimeUnit.SECONDS)
            	                .requestTimeout(10, TimeUnit.SECONDS)
            	                .baseUri("http://192.168.1.37:8083")
            	                .build();
            			try {
            					
            				OutputStream os = new ByteArrayOutputStream();
            			Future<Boolean> conversion = converter
            		            .convert(initialStream).as(DocumentType.MS_WORD)
            		            .to(os).as(DocumentType.PDF)
            		            .prioritizeWith(1000) // optional
            		            .schedule();
            			ByteArrayOutputStream baos = (ByteArrayOutputStream) os;
            			System.out.println("Byte Array Size:::"+baos.size());
            			fileObj = new SerialBlob(baos.toByteArray());
            			filename = fnameWOext + ".pdf";
            			apndx_filename = Configuration.DOWNLOADFOLDER + filename;
            			} catch (Exception e) {
            				
            				// TODO Auto-generated catch block
            				e.printStackTrace();
            			}
            		}else{
            		
		            	OfficeManager officeManager = null;
		                try {
		                    officeManager = new DefaultOfficeManagerConfiguration()
		                            .setOfficeHome(new File("D:\\LibreOffice"))
		                            .buildOfficeManager();
		                    officeManager.start();
		         
		                    // 2) Create JODConverter converter
		                    OfficeDocumentConverter converter = new OfficeDocumentConverter(
		                            officeManager);
		         
		                    // 3) Create PDF
		                    createPDF(converter,sourceFile,targetFile);
		                    //createPDF(converter);
		         
		                } finally {
		                    // 4) Stop LibreOffice in headless mode.
		                    if (officeManager != null) {
		                        officeManager.stop();
		                    }
		                }
		                if(targetFile.exists()){
		                	fileObj = new SerialBlob(convertFileContentToBlob(tgtFilePath));
		                }
            		}*/
            	}
            	if(filename.toLowerCase().endsWith(".pdf")){
            		ReportIO ReportIO= new ReportIO();
                    ReportIO.pdfWrite(fileObj, studyNumber, approvalDate,filename,apndx_filename,pkStudyApndx,statusId);
            	}
                	                 	
               }
        } catch(Exception e) {
        	Rlog.fatal("objectMap", "Exception in StudyApndxDao.addStampingforApproved:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
        return ret;		
	} 
	
	public boolean isClass(String className) {
	    try  {
	        Class.forName(className);
	        return true;
	    }  catch (ClassNotFoundException e) {
	        return false;
	    }
	}
	
	public int updateFileObj(int pkStudyApndx , byte[] data, String filename, String filename_uri,String statusId,int stampFlag ) {
    	int result = 0;    	   	
    	CallableStatement cstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
        	
            conn = getConnection();           
            cstmt = conn.prepareCall("{call PKG_STUDYSTAT.SP_CREATE_STAMPED_APNDX(?,?,?,?,?,?,?,?)}");
            InputStream in = new ByteArrayInputStream(data);
            cstmt.setInt(1, pkStudyApndx);
            cstmt.setBinaryStream(2, in, data.length);
            cstmt.setInt(3, data.length);
            cstmt.setString(4, filename);
            cstmt.setString(5, filename_uri);
            cstmt.setInt(6, Integer.parseInt(statusId));
            cstmt.setInt(7, stampFlag);
            cstmt.registerOutParameter(8, java.sql.Types.INTEGER);
            cstmt.execute();
            result = cstmt.getInt(8);
        } catch(Exception e) {
        	Rlog.fatal("studyVer", "Exception in StudyApndxDao.updateFileObj:"+e);
        	result = 0;
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}
        	try { if (cstmt != null) cstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
    	return result;
    }
	public int updateBkpFileObj(int pkStudyApndx , String filename, String filename_uri,int stampFlag ) {
    	int result = 0;    	   	
    	CallableStatement cstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
        	
            conn = getConnection();           
            cstmt = conn.prepareCall("{call PKG_STUDYSTAT.SP_UPDATE_PRESTAMP_APNDX(?,?,?,?)}");
            cstmt.setInt(1, pkStudyApndx);
            cstmt.setString(2, filename);
            cstmt.setString(3, filename_uri);
            cstmt.registerOutParameter(4, java.sql.Types.INTEGER);
            cstmt.execute();
            result = cstmt.getInt(4);
        } catch(Exception e) {
        	Rlog.fatal("studyVer", "Exception in StudyApndxDao.updateFileObj:"+e);
        	result = 0;
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}
        	try { if (cstmt != null) cstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
    	return result;
    }
	public void removeStampFromPDF(int statusId, String studyVerId) {
		// TODO Auto-generated method stub
		PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        int pkStudyApndx = 0;
        int ret=0;
        int stamp_ref =0;
        ArrayList<String> arList = new ArrayList<String>();
        try {
        	conn = getConnection();
            pstmt = conn.prepareStatement(GetFileBlobsforPreStamping);
            pstmt.setInt(1, statusId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	String filename = rs.getString("studyapndx_uri");
            	String bkpFileName = rs.getString("PRESTAMP_FILEURI");
            	if(rs.getInt("STAMP_FLAG")!=1 && filename.toLowerCase().endsWith(".pdf")){
            		continue;
            	}else{
	            	pkStudyApndx = rs.getInt("pk_studyapndx");
	            	String apndx_filename = rs.getString("studyapndx_file");
	            	System.out.println("Before Replace apndx_filename--->"+apndx_filename);
	            	apndx_filename = apndx_filename.replace(filename,bkpFileName);
	            	System.out.println("After Replace apndx_filename--->"+apndx_filename);
	            	
	     			stamp_ref = rs.getInt("stamp_ref");
	            	
	            	updateBkpFileObj(pkStudyApndx, apndx_filename, bkpFileName,0);
	            	
	            
            	}
            	
            }
	
            } catch(Exception e) {
            	Rlog.fatal("objectMap", "Exception in StudyApndxDao.addStampingforApproved:"+e);
            } finally {
            	try { if (rs != null) rs.close(); } catch (Exception e) {}
            	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
            	try { if (conn != null) conn.close(); } catch (Exception e) {}
            }
     }

}
