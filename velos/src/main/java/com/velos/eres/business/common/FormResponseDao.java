/*
 * Classname : 			FormResponseDao
 *
 * Version information: 1.0
 *
 * Date:    			06/29/2010
 *
 * Copyright notice: 	Velos, Inc
 *
 * Author: 				Sonia Abrol
 */

package com.velos.eres.business.common;

import java.sql.CallableStatement;
/* Import Statements */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;

import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The Form responses Dao.<br>
 * <br>
 * DAO for getting basic information about form responses
 *
 * @author Sonia Abrol
 * @vesion 1.0
 */

public class FormResponseDao extends CommonDAO implements java.io.Serializable {

    private ArrayList arPKResponseID;

    private ArrayList arFilledFormDate;

    private String formType;

    private ArrayList arVersionNumber;
    
    private String formID;

    private ArrayList arFormStatusPK;

    private ArrayList arFormStatusDesc;
    
    private int cRows;


    public FormResponseDao() {
        arPKResponseID = new ArrayList();
        arFilledFormDate = new ArrayList();
        arVersionNumber = new ArrayList();
        arFormStatusPK = new ArrayList();
        arFormStatusDesc = new ArrayList();
        cRows = 0;
        formType = "";
		formID = "";
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    
    public void getStudyFormResponeData(int formId, String studyId) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlb = new StringBuffer();
        
        try {
            conn = getConnection();
            
            sqlb.append(" select pk_studyforms,to_char(studyforms_filldate,PKG_DATEUTIL.F_GET_DATEFORMAT) filldate , (select formlibver_number from er_formlibver where pk_formlibver = fk_formlibver) formlibver_number, "); 
            sqlb.append(" form_completed, (select codelst_desc from er_codelst where pk_codelst = form_completed) form_status_desc ");
            
            sqlb.append("  from er_studyforms where fk_formlib = ? and fk_study = ? and record_type <> 'D' order by pk_studyforms desc ");

            
            pstmt = conn
                    .prepareStatement(sqlb.toString());

            pstmt.setInt(1, formId);
            pstmt.setString(2, studyId);
            
            ResultSet rs = pstmt.executeQuery();
            
            this.setFormID(String.valueOf(formId));
            this.setFormType("S");

            while (rs.next()) {
             
            	this.setArFilledFormDate(rs.getString("filldate") );
            	this.setArPKResponseID( rs.getString("pk_studyforms"));
            	this.setArVersionNumber( rs.getString("formlibver_number"));
            	this.setArFormStatusPK(rs.getString("form_completed"));
            	this.setArFormStatusDesc(rs.getString("form_status_desc"));
            	rows++;
                Rlog.debug("FormResponseDao", "FormResponseDao.getStudyFormResponeData(String formId, String studyId)  rows "
                        + rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("fldresp",
                    " EXCEPTION IN FormResponseDao.getStudyFormResponeData(String formId, String studyId)"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

	public ArrayList getArPKResponseID() {
		return arPKResponseID;
	}

	public void setArPKResponseID(ArrayList arPKResponseID) {
		this.arPKResponseID = arPKResponseID;
	}

	public void setArPKResponseID(String pKResponseID) {
		this.arPKResponseID.add(pKResponseID);
	}

	
	public ArrayList getArFilledFormDate() {
		return arFilledFormDate;
	}

	public void setArFilledFormDate(ArrayList arFilledFormDate) {
		this.arFilledFormDate = arFilledFormDate;
	}

	public void setArFilledFormDate(String filledFormDate) {
		this.arFilledFormDate.add(filledFormDate);
	}

	
	public String getFormType() {
		return formType;
	}

	public void setFormType(String formType) {
		this.formType = formType;
	}

	public ArrayList getArVersionNumber() {
		return arVersionNumber;
	}

	public void setArVersionNumber(ArrayList arVersionNumber) {
		this.arVersionNumber = arVersionNumber;
	}

	public void setArVersionNumber(String versionNumber) {
		this.arVersionNumber.add(versionNumber);
	}

	
	public String getFormID() {
		return formID;
	}

	public void setFormID(String formID) {
		this.formID = formID;
	}

	public ArrayList getArFormStatusPK() {
		return arFormStatusPK;
	}
	public void setArFormStatusPK(ArrayList arFormStatusPK) {
		this.arFormStatusPK = arFormStatusPK;
	}
	public void setArFormStatusPK(String arFormStatusPK) {
		this.arFormStatusPK.add(arFormStatusPK);
	}

	public ArrayList getArFormStatusDesc() {
		return arFormStatusDesc;
	}

	public void setArFormStatusDesc(ArrayList arFormStatus) {
		this.arFormStatusDesc = arFormStatus;
	}

	public void setArFormStatusDesc(String formStatus) {
		this.arFormStatusDesc.add(formStatus);
	}
    

    
    /** Returns SQL to get form responses
     * @param p_id the main ID form is linked with
     * 	When form is linked with patient,p_id = patient PK
     * 	When form is linked with account,p_id = account PK
     * 	When form is linked with study,p_id = study PK
     *  @param Form PK
     *  @param p_link_from String for form response location
     *  @param p_orderby Order By field
     *  @param p_ordertype Order By Desc or Asc
     *  @param p_studyID studyPk if form is linked with a study
     *  @param loggedInUserID Logged in user PK 
     *  @param htMoreParams - Any additional parameters, supported parameters are:
     *       responseId - PK of response (passed from front end).For Form to Form Response linking (implemented but feature not in use)
     *       dateFilter - date filter for form responses as passed from front end
     *  */
	
	
    public String getFormResponsesSQL(int p_id , int p_form , String p_link_from,  String p_orderby,
    		String p_ordertype, String studyId, int loggedInUserID, Hashtable htMoreParams)
	{
	
    	SaveFormDao sfDao = new SaveFormDao(); 
    	Calendar formFillDtCal = Calendar.getInstance();
    	
    	String v_dt_whereclause =  "ALL";
    	String specOnly = null;
    	String linkFrom = "";
    	int schIndex = -1;
    	String v_sch_eventpk = "";
    	int v_index_velsep = -1;
    	String v_patprot = "";
    	String v_orderbydefault = " fk_filledform desc, filldate desc "; 
    	String studyNumberSQL= " (select s1.study_number  from er_study s1 where s1.pk_study=id ) as studyNumber ";//wonder why we need it~!
    	String studyNumberSQLForPat = "( SELECT s1.study_number FROM ER_STUDY s1 WHERE s1.pk_study=(SELECT pp1.fk_study FROM ER_PATPROT pp1 WHERE pp1.pk_patprot= fk_patprot))  AS studyNumber "; //not sure why we need it
    	String additionalFields= "";
    
    	String respAccessRightWhereClause = " and ( pkg_filledform.fn_getUserAccess (fk_form," + loggedInUserID +",creator) > 0 )"	;
    	
    	ArrayList arBrowserColumns = new ArrayList();
    	ArrayList arBrowserColumnsDataTypes = new ArrayList();
    	ArrayList arBrowserColumnsDisplayNames = new ArrayList();
    	//String orderByColumnName = "";
        String browserColName= "";
        String orderByColumnDesc = "";
        //KM-#5942
        String orderByDateDesc = "";
        
        String colDisplayName = "";
        StringBuffer sbColumnList = new StringBuffer();
        String colDataType = "";
        int vbrowsercount = 0;
        
        String formLinkCol = "";
        String responseId = "";
        
        SaveFormDao saveDao=new SaveFormDao();
        

    	//load the form browser columns data
    	
    	sfDao.getFormResponseBrowserColumns(String.valueOf(p_form));
    	
    	StringBuffer whereClause=new StringBuffer();
    
    	StringBuffer selectSQL= new StringBuffer();
        
    	StringBuffer sbSQL= new StringBuffer();
    	
    	String versionPKColumnSQL = "";
    	//append patient/study/account ID
    	
    	//append form
    	
    	whereClause.append( " Where fk_form = " + p_form);
    	
    	//append ID - module info 
    	whereClause.append( " and id = " + p_id);
    	
    	//append form response access SQL
    	whereClause.append(respAccessRightWhereClause);
    	
    	linkFrom  = p_link_from;

    	schIndex = linkFrom.indexOf("[VELSCHPK]"); 
    	
    	if (schIndex > 0)
    	{
    		v_sch_eventpk = linkFrom.substring(schIndex+10);
    		linkFrom = linkFrom.substring(0, schIndex);
    		
    	}
        
    	v_index_velsep = linkFrom.indexOf("[VELSEP]");
    	
    	if (v_index_velsep > 0)
    	{
    		v_patprot = linkFrom.substring(v_index_velsep+8);
    		linkFrom = linkFrom.substring(0, v_index_velsep);
    		
    	}
    
    	
    if (htMoreParams != null)
    {
    
    	if (htMoreParams.containsKey("responseId"))
    	{
    		responseId = (String) htMoreParams.get("responseId");
    		
    		formLinkCol=saveDao.getChildDetails(p_form,linkFrom,StringUtil.stringToNum(responseId));
    	    formLinkCol=(formLinkCol==null)?"":formLinkCol;

    	}
    	
    	if (htMoreParams.containsKey("dateFilter"))
    	{
    		v_dt_whereclause = (String) htMoreParams.get("dateFilter");
    		
    		if (StringUtil.isEmpty(v_dt_whereclause))
    		{
    			v_dt_whereclause = "ALL";
    			
    		}
    	}
//    	FIXED BUG 6875 ALSO FIXED THE MULTI SPECIMENT CASE
    	if (htMoreParams.containsKey("specRecOnly"))
    	{
    		String valueOnly = (String) htMoreParams.get("specRecOnly");
    		
    		if (!StringUtil.isEmpty(valueOnly))
    		{    			
    			specOnly = " and fk_specimen = "+valueOnly;
    			
    		}
    	}
    	
    }
        
    
    if (linkFrom.equals("A")||linkFrom.equals("ORG")||linkFrom.equals("USR")||linkFrom.equals("NTW")||linkFrom.equals("SNW")) 
    {
        additionalFields = ",  '' as studyNumber" ;
        versionPKColumnSQL = getAccountFormsVersionPKSQL();
    }
    else if (linkFrom.equals("S") || linkFrom.equals("SA"))
    {
    	additionalFields = ", " + studyNumberSQL ;
    	versionPKColumnSQL = getStudyFormsVersionPKSQL();
    	
    }
    else if (linkFrom.equals("SP") )
    {
    	additionalFields = ", " + studyNumberSQLForPat ;

    	  	
    	if (! StringUtil.isEmpty(v_patprot))
    	{
    		whereClause.append( " and fk_patprot in (select pk_patprot from er_patprot pp where pp.fk_study = "+studyId+" and " +
    				" pp.fk_per = "+ p_id+")"); 
    	}
    	else
    	{
    		whereClause.append( " and fk_patprot is not null"); 
    		//Bug#8966 :Yogendra:18 April2012
    		String patProtId="";
    		String formDispType ="";
    		if (htMoreParams.containsKey("patProtId")){
    			if(htMoreParams.containsKey("formDispType"))
    				formDispType = (String) htMoreParams.get("formDispType");
    			
    			patProtId = (String) htMoreParams.get("patProtId");
    			if(formDispType.equalsIgnoreCase("PR")){
    				whereClause.append( " and fk_patprot="+patProtId+" " );
    			}
        	}
    		
    	}
    	
    	
    }
    if(!StringUtil.isEmpty(specOnly)){
    	whereClause.append( specOnly);	
    }
    
    
    if (linkFrom.equals("SP") || linkFrom.equals("PR") || linkFrom.equals("PA") || linkFrom.equals("PS"))
    {
    	if (! StringUtil.isEmpty(v_sch_eventpk))
    	{
    		whereClause.append( " and fk_sch_events1 = " + v_sch_eventpk);
    	}
    	
    	versionPKColumnSQL = getPatientFormsVersionPKSQL();
    }
    
    if (linkFrom.equals("PR") || linkFrom.equals("PA") )
    {
    	whereClause.append( " and ( ( fk_patprot is null and exists (select * from er_linkedforms f where " +
    			"f.fk_formlib = fk_form and nvl(LF_DISPLAY_INPAT,0)=1 ) ) or exists ( select * from er_linkedforms f where " +
    			"f.fk_formlib = fk_form and nvl(LF_DISPLAY_INPAT,0)=0 ) ) " );
    	
    }

    //Prepare Select String 
    
     
    // check formLinkCol 
    
    if (! StringUtil.isEmpty(formLinkCol))
    {
    	whereClause.append( " and EXISTS (SELECT * FROM ER_FORMSLINEAR l WHERE l.fk_form="+ p_form +
    			"  AND l.fk_filledform=ot.fk_filledform AND " + formLinkCol.trim() +"="+responseId +") "); 
    	
    }
    
    if ( !v_dt_whereclause.equals("ALL")) {
    	
    	whereClause.append( " and filldate  " + StringUtil.getRelativeTime(formFillDtCal, v_dt_whereclause) + " ");
     
    }
 
     // iterate through the loaded SaveFormDao object to get the details of browser columns
     
     arBrowserColumns = sfDao.getColNames();
     arBrowserColumnsDataTypes = sfDao.getColTypes();
     arBrowserColumnsDisplayNames = sfDao.getColDisplayNames();
     
          
     if (arBrowserColumns != null)
     {
	     for (int k = 0; k< arBrowserColumns.size() ; k++)
	    	 {
	    	 	vbrowsercount = vbrowsercount+1;
	    	 	
	    	 	browserColName = (String)arBrowserColumns.get(k);
	    	 	
	    	 	colDisplayName = (String)arBrowserColumnsDisplayNames.get(k);
	    	 	
	    	 	colDisplayName = StringUtil.htmlEncodeXss(colDisplayName);
	    	 	
	    	 	colDataType  = (String)arBrowserColumnsDataTypes.get(k); 
	    	 		
	    	 	System.out.println("p_orderby"+p_orderby+"*");
	    	 	System.out.println("browserColName"+browserColName+"*");
	    	 	
	    	 	
	    	 	if ((!StringUtil.isEmpty(p_orderby)) && browserColName.equalsIgnoreCase(p_orderby))
	    	 	{
	    	 		//orderByColumnName = "sort_" + p_orderby;
	    	 		
	    	 		if (colDataType.equals("ED"))
	    	 		{
	    	 			orderByColumnDesc = " f_to_date("+ browserColName +")";
	    	 			//KM-#5942
	    	 			orderByDateDesc = " , hidden_fk_filledform ";
	    	 		}
	    	 		else if(colDataType.equals("EN"))
	    	 		{
	    	 			orderByColumnDesc = " f_to_number("+ browserColName +")";
	    	 			
	    	 		} 
	    	 		else 
	    	 		{
	    	 			orderByColumnDesc = " lower("+ p_orderby +")";
	    	 		}	
	    	 		
	    	 		
	    	 	    
	    	 	}
	    	 	
	    	 	sbColumnList.append(",'" + colDisplayName + "' HEADER"+ vbrowsercount + " , "); //one comma added for data colum name before
	    	 	
	    	 	
	    	 	if (colDataType.equals("MD") || colDataType.equals("MC") || colDataType.equals("MR"))
    	 		{
	    	 		sbColumnList.append("F_GET_MCFIELDVAL ( "+  browserColName + " ) " + browserColName); //add column with header name
    	 		}
	    	 	else
	    	 	{
	    	 		sbColumnList.append( browserColName ); //add column with header name
	    	 	}
	    	 	
	    	      	 	
	    	 	
	    	 }
	   }   
	 
     selectSQL.append(" SELECT fk_filledform as hidden_fk_filledform , (Select CODELST_SUBTYP from er_codelst where PK_CODELST = form_completed)  AS hidden_formstat_subtype, "+versionPKColumnSQL);
    
    
     selectSQL.append(" , form_version hidden_form_version, filldate hidden_filldate, pk_formslinear as rowcount, id hidden_id");
     
     selectSQL.append(additionalFields);
   
     if (! StringUtil.isEmpty(sbColumnList.toString()))
     {
    	 selectSQL.append(sbColumnList.toString());
    	 
     }
   
     selectSQL.append(", '"+ LC.L_Form_ResponseStatus +"' header, F_CODELST_DESC(form_completed) form_status ");
     
     
     
     //From Clause 
     selectSQL.append(" from er_formslinear ot ");
     
     
     //append select clause to main SQL
     sbSQL.append(selectSQL.toString());
     
     //Where clause
     sbSQL.append(whereClause.toString());
     
     
     if (!StringUtil.isEmpty(p_orderby) && p_orderby.equalsIgnoreCase("form_status")) // this is a special case as form status is not part of browser columnns
     {
    	 orderByColumnDesc = p_orderby + " " ;
    	 
     }
   //append order by column name
     if ( StringUtil.isEmpty(orderByColumnDesc))
     {
    	 orderByColumnDesc  = v_orderbydefault;
     }	 
     else
     {
    	 orderByColumnDesc  = orderByColumnDesc  +  p_ordertype;
    	 
    	 //KM-#5942
    	 if (!StringUtil.isEmpty(orderByDateDesc))
    	 {
    		orderByColumnDesc = orderByColumnDesc + orderByDateDesc + p_ordertype; 
    	 }
     }
     
     sbSQL.append(" Order By "+  orderByColumnDesc );
     
     return sbSQL.toString();   
    	
    	
	}
 
    private String getStudyFormsVersionPKSQL()
    {
    	return " (Select fk_formlibver from er_studyforms where pk_studyforms = ot.fk_filledform) hidden_fk_formlibver ";
    	
    }
    
    private String getPatientFormsVersionPKSQL()
    {
    	return " (Select fk_formlibver from er_patforms where pk_patforms = ot.fk_filledform) hidden_fk_formlibver ";
    	
    }
    
    private String getAccountFormsVersionPKSQL()
    {
    	return " (Select fk_formlibver from er_acctforms where pk_acctforms = ot.fk_filledform) hidden_fk_formlibver ";
    	
    }
    public int getStudyOfFormResponse(int filledFormId){
    	int output = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlb = new StringBuffer();
         
        try {
             conn = getConnection();
             
             sqlb.append("select PK_PATFORMS,FK_PATPROT, ");
             sqlb.append("(select FK_STUDY from ER_PATPROT where PK_PATPROT = FK_PATPROT) linkedStudy ");
             sqlb.append("from er_patforms where PK_PATFORMS = ? and record_type <> 'D'");

             pstmt = conn.prepareStatement(sqlb.toString());
             pstmt.setInt(1, filledFormId);
             
             ResultSet rs = pstmt.executeQuery();
             if (rs.next()) {
            	 output = StringUtil.stringToNum(rs.getString("linkedStudy"));
             }
         } catch (SQLException ex) {
             Rlog.fatal("fldresp"," EXCEPTION IN FormResponseDao.getStudyOfFormResponse(int filledFormId)"+ ex);
         } finally {
             try {
                 if (pstmt != null)
                     pstmt.close();
             } catch (Exception e) {
             }
             try {
                 if (conn != null)
                     conn.close();
             } catch (Exception e) {
             }
         }
    	return output;
	}

    public HashMap getFormRespByColData(int userId, int formId, ArrayList getColumns, ArrayList matchColumns, ArrayList matchWiths){
    	HashMap retHashMap = new HashMap();
    	StringBuffer whereClause=new StringBuffer();
		StringBuffer selectSQL= new StringBuffer();
		String respAccessRightWhereClause = " and (pkg_filledform.fn_getUserAccess (fk_form," + userId +",creator) > 0)";
		String v_orderbydefault = " ORDER BY fk_filledform asc"; 
		StringBuffer sbSQL= new StringBuffer();

		selectSQL.append("SELECT PK_FORMSLINEAR");

		FormCompareUtilityDao fcDao = new FormCompareUtilityDao();
		fcDao.setFormId(formId);
		fcDao.getFormFieldMap();
		ArrayList mpUIDs = fcDao.getColUIds();
		ArrayList mpColNames = fcDao.getColNames();

    	if (getColumns.size() > 0){
	    	for (int iCol=0; iCol < getColumns.size(); iCol++){
	    		String getColumn = ((String)getColumns.get(iCol)).trim();
	    		getColumn = StringUtil.stripScript(getColumn);

	    		String alias = getColumn;
	    		if (!StringUtil.isEmpty(getColumn)){
		    		if (getColumn.equalsIgnoreCase("fk_per") || getColumn.equalsIgnoreCase("fk_study") || getColumn.equalsIgnoreCase("fk_patprot")
		    				|| getColumn.equalsIgnoreCase("ID") || getColumn.equalsIgnoreCase("fk_sch_events1"))
		    			continue;

		    		int indx = mpUIDs.indexOf(getColumn);
		    		if (indx > -1){
		    			getColumn = ((String)mpColNames.get(indx)).trim();
			    		selectSQL.append(", " + getColumn +" as \"" + alias + "\"");
					} else {
						return null;
					}
	    		}
	    	}
    	}

    	selectSQL.append(" FROM er_formslinear ");
    	whereClause.append("WHERE FK_FORM = "+ formId);
    	if (matchColumns.size() > 0 && matchColumns.size() == matchWiths.size()){
	    	for (int iCol=0; iCol < matchColumns.size(); iCol++){
	    		String matchColumn = ((String)matchColumns.get(iCol)).trim();
	    		matchColumn = StringUtil.stripScript(matchColumn);
	    		if (!StringUtil.isEmpty(matchColumn)){
		    		if (matchColumn.equalsIgnoreCase("fk_per") || matchColumn.equalsIgnoreCase("fk_study") || matchColumn.equalsIgnoreCase("fk_patprot")
		    			|| matchColumn.equalsIgnoreCase("fk_sch_events1"))
		    			continue;
		    		if (!matchColumn.equalsIgnoreCase("ID")){ 
			    		int indx = mpUIDs.indexOf(matchColumn);
			    		if (indx > -1){
			    			matchColumn = ((String)mpColNames.get(indx)).trim();
						} else {
							return null;
						}
		    		}
		    		String matchWith = ((String)matchWiths.get(iCol)).trim();
		    		matchWith = StringUtil.stripScript(matchWith);
		    		whereClause.append(" and " +matchColumn + "='" + matchWith+"'");
	    		}
	    	}
    	}

		whereClause.append(respAccessRightWhereClause);
		
		//append Select clause to main SQL
		sbSQL.append(selectSQL);
		//Where clause
		sbSQL.append(whereClause);
		//Order By
		sbSQL.append(v_orderbydefault);
		 
		PreparedStatement pstmt = null;
		Connection conn = null;

	    try {
	    	conn = getConnection();

	    	pstmt = conn.prepareStatement(sbSQL.toString());
             
	    	ResultSet rs = pstmt.executeQuery();
	    	if (rs.next()) {
	    		HashMap rowHashMap = new HashMap();
	    		String pk = (String)rs.getString("PK_FORMSLINEAR");
	    		rowHashMap.put("PK_FORMSLINEAR", StringUtil.stringToNum(pk));

	    		if (getColumns.size() > 0){
	    			for (int iCol=0; iCol < getColumns.size(); iCol++){
	    				String getColumn = ((String)getColumns.get(iCol)).trim();
	    				getColumn = StringUtil.stripScript(getColumn);
	    				if (!StringUtil.isEmpty(getColumn)){
	    					if (getColumn.equalsIgnoreCase("fk_per") || getColumn.equalsIgnoreCase("fk_study") || getColumn.equalsIgnoreCase("fk_patprot")
	    						|| getColumn.equalsIgnoreCase("ID") || getColumn.equalsIgnoreCase("fk_sch_events1"))
	    						continue;
	    					rowHashMap.put(getColumn, rs.getString(getColumn));
	    				}
	    			}
	    		}
	    		retHashMap.put(pk, rowHashMap);
             }
         } catch (SQLException ex) {
             Rlog.fatal("fldresp"," EXCEPTION IN FormResponseDao.getFormRespByColData "+ ex);
         } finally {
             try {
                 if (pstmt != null)
                     pstmt.close();
             } catch (Exception e) {
             }
             try {
                 if (conn != null)
                     conn.close();
             } catch (Exception e) {
             }
         }
    	return retHashMap;
    }
    
    private static final String getFkAccountOfFilledAcctForm = 
    		" select FK_ACCOUNT from ER_ACCTFORMS where PK_ACCTFORMS = ? ";
    public int getFkAccountOfFilledAcctForm(int filledFormId) {
    	int fkAccount = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(getFkAccountOfFilledAcctForm);
            pstmt.setInt(1, filledFormId);
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()) {
            	fkAccount = rs.getInt(1);
            }
        } catch (Exception e) {
            Rlog.fatal("fldresp","Exception in FormResponseDao.getFkAccountOfFilledForm: "+ e);
            fkAccount = 0;
        } finally {
            try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
            try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
    	return fkAccount;
    }

    public void setStudyFormResponeStatus(Object...args) {
    	int formId = StringUtil.stringToNum(args[0].toString());
    	int studyId = StringUtil.stringToNum(args[1].toString());
    	int userId = StringUtil.stringToNum(args[2].toString());
    	int formStatus = StringUtil.stringToNum(args[3].toString());
    	String studyFormsPK = getStudyFormsPK(formId,studyId);
    	if (studyId < 0 || formStatus < 0 || studyFormsPK.equals("")){ return; }
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
	        pstmt = conn.prepareStatement("Update er_studyforms set form_completed = ?, last_modified_by = ? where PK_STUDYFORMS in ("+studyFormsPK.trim()+") ");
	        int paramSeq = 1;
	        pstmt.setInt(paramSeq++, formStatus);
	        pstmt.setInt(paramSeq++, userId);
            pstmt.executeUpdate();
            pstmt.close();
	        pstmt = conn.prepareStatement("Update er_formslinear set form_completed = ?, last_modified_by = ? where FK_FORM = ? and FK_FILLEDFORM in ("+studyFormsPK.trim()+")");
	        paramSeq = 1;
	        pstmt.setInt(paramSeq++, formStatus);
	        pstmt.setInt(paramSeq++, userId);
            pstmt.setInt(paramSeq++, formId);
            pstmt.executeUpdate();
	        pstmt.close();
        } catch (SQLException ex) {
            Rlog.fatal("fldresp",
                    " EXCEPTION IN FormResponseDao.setStudyFormResponeStatus(Object...args)"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    private String getStudyFormsPK(int formlibId, int studyId) {
    	// TODO Auto-generated method stub
    	String pkStudyForms = "";
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement("select rowtocol('select pk_studyforms from ER_STUDYFORMS where FK_FORMLIB="+formlibId+" and FK_STUDY="+studyId+"') as pk_studyforms from dual");
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()) {
            	pkStudyForms = rs.getString(1);
            }
        } catch (Exception e) {
            Rlog.fatal("fldresp","Exception in FormResponseDao.getStudyFormsPK: "+ e);
            e.printStackTrace();
            pkStudyForms = "";
        } finally {
            try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
            try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
    	return pkStudyForms;
	}

	public void unlockFormResponse(int studyId ) {
    	int result = 0;    	   	
    	CallableStatement cstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
        	
            conn = getConnection();           
            cstmt = conn.prepareCall("{call PKG_STUDYSTAT.SP_UNFREEZE_FORM_RESPONSES(?,?)}");
            cstmt.setInt(1, studyId);
            cstmt.setInt(2, 1);
            cstmt.execute();
        } catch(Exception e) {
        	Rlog.fatal("FormResponse", "Exception in FormResponseDao.unlockFormResponse:"+e);
        	result = 0;
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}
        	try { if (cstmt != null) cstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
    }
}