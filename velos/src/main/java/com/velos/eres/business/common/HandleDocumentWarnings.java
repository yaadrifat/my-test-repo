package com.velos.eres.business.common;

import com.aspose.words.IWarningCallback;
import com.aspose.words.WarningInfo;
import com.aspose.words.WarningType;

public class HandleDocumentWarnings implements IWarningCallback {

	@Override
	public void warning(WarningInfo info) {
		// TODO Auto-generated method stub
		if (info.getWarningType() == WarningType.FONT_SUBSTITUTION) {
			System.out.println("Font substitution: " + info.getDescription());
		}
	}

}
