/*
 *  Classname			FieldLibDao.class
 *
 *  Version information 	1.0
 *
 *  Date					09/07/2003
 *
 *  Copyright notice		Velos, Inc.
 */
package com.velos.eres.business.common;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.velos.eres.business.fieldLib.impl.FieldLibBean;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.fieldLib.FieldLibJB;
import com.velos.eres.web.formField.FormFieldJB;

/**
 * FieldLibDao for getting FieldLib records
 *
 * @author Sonia Kaura
 * @created May 31, 2005
 * @version : 1.0 09/07/2003
 */

public class FieldLibDao extends CommonDAO implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3257852086359308601L;

    /**
     * The primary key for the Field Library Table : pk_field
     */
    // int
    private ArrayList fieldLibId;

    /**
     * The foreign key reference to the Category Library: FK_CATLIB
     */
    // Integer
    private ArrayList catLibId;

    /**
     * The Library Flag to display if the field is within the library or belongs
     * <br>
     * to a form : FLD_LIBFLAG
     */
    // String
    private ArrayList libFlag;

    /**
     * The Name of the Field Component : FLD_NAME
     */
    // String
    private ArrayList fldName;

    /**
     * The description of the Field Component: FLD_DESC
     */
    // String
    private ArrayList fldDesc;

    /**
     * The Field Component Unique ID : FLD_UNIQUEID
     */
    // String
    private ArrayList fldUniqId;

    /**
     * The Field Component System ID: FLD_SYSTEMID
     */
    // String
    private ArrayList fldSysID;

    /**
     * The Field Component Key Word :FLD_KEYWORD
     */
    // String
    private ArrayList fldKeyword;

    /**
     * The Field Type of the Field Component
     */
    // String
    private ArrayList fldType;

    /**
     * The Datatype of the Field Component :FLD_DATATYPE
     */
    // String
    private ArrayList fldDataType;

    /**
     * The Instructions for the Field Component : FLD_INSTRUCTIONS
     */
    // String
    private ArrayList fldInstructions;

    /**
     * The attribute of the field component of format of the field: FLD_FORMAT
     */
    // String
    private ArrayList fldFormat;

    /**
     * The attribute of the field component of the number of times it repeats:
     * FLD_REPEATFLAG
     */
    // Integer
    private ArrayList fldRepeatFlag;

    /**
     * The attribute of the field component of the number of times it repeats:
     * CATLIB_NAME
     */
    // String
    private ArrayList fldCatName;

    private ArrayList fldBrowserFlag;

    /**
     * To get the number of rows
     */

    private int rows;

    /**
     * To get the value for the ER_FORMFLD corresponding to the FK_FORMSEC
     */
    private String formSecId;

    /**
     * To get the value for the ER_FORMFLD corresponding to the FORMFLD_SEQ
     */
    private String formfldSeq;

    /**
     * To get the value for the ER_FORMFLD corresponding to the
     * FORMFLD_MANDATORY
     */
    private String formfldMandatory;

    /**
     * To get the value for the ER_FORMFLD corresponding to the
     * FORMFLD_BROWSERFLG
     */
    private String formFldBrowserFlg;

    /**
     * To get the value for the ER_FORMFLD corresponding to the FORMFLD_XSL
     */
    private String formfldXsl;

    private ArrayList recordType;

    private ArrayList creator;

    private ArrayList ipAdd;

    private ArrayList aStyle;

    private ArrayList fldLinesNo;

    private ArrayList fldCharsNo;

    private ArrayList fldDefResp;

    private ArrayList accountId;

    private ArrayList fldLength;

    private ArrayList formFldId;

    private int cRows;

    /**
     * Changes for the Phase 2 Made By : Sonia Kaura Date : 11/06/2003
     *
     * @return The cRows value
     */

    // private ArrayList fldTodayCheck ;
    // private ArrayList fldOverride ;
    // ///////////////////////////////////////////////////////
    // GETTERS AND SUPPORT
    public int getCRows() {
        return this.cRows;
    }

    /**
     * Sets the cRows attribute of the FieldLibDao object
     *
     * @param cRows
     *            The new cRows value
     */
    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    /**
     * @return fieldLibId
     */
    public ArrayList getFieldLibId() {
        return this.fieldLibId;
    }

    /**
     * @param fieldLibId
     */
    public void setFieldLibId(ArrayList fieldLibId) {
        this.fieldLibId = fieldLibId;
    }

    /**
     * @return catLibId
     */
    public ArrayList getCatLibId() {
        return this.catLibId;
    }

    /**
     * @param catLibId
     */
    public void setCatLibId(ArrayList catLibId) {
        this.catLibId = catLibId;
    }

    /**
     * @return libFlag
     */
    public ArrayList getLibFlag() {
        return this.libFlag;
    }

    /**
     * @return fldName
     */
    public ArrayList getFldName() {
        return this.fldName;
    }

    /**
     * @param fldName
     */
    public void setFldName(ArrayList fldName) {
        this.fldName = fldName;
    }

    /**
     * @return fldDesc
     */
    public ArrayList getFldDesc() {
        return this.fldDesc;
    }

    /**
     * @param fldDesc
     */
    public void setFldDesc(ArrayList fldDesc) {
        this.fldDesc = fldDesc;
    }

    /**
     * @return fldUniqId
     */
    public ArrayList getFldUniqId() {
        return this.fldUniqId;
    }

    /**
     * @param fldUniqId
     */
    public void setFldUniqId(ArrayList fldUniqId) {
        this.fldUniqId = fldUniqId;
    }

    /**
     * @return fldSysID
     */
    public ArrayList getFldSysID() {
        return this.fldSysID;
    }

    /**
     * @param fldSysID
     */
    public void setFldSysID(ArrayList fldSysID) {
        this.fldSysID = fldSysID;
    }

    /**
     * @return fldKeyword
     */
    public ArrayList getFldKeyword() {
        return this.fldKeyword;
    }

    /**
     * @param fldKeyword
     */
    public void setFldKeyword(ArrayList fldKeyword) {
        this.fldKeyword = fldKeyword;
    }

    /**
     * @return fldType
     */
    public ArrayList getFldType() {
        return this.fldType;
    }

    /**
     * @param fldType
     */
    public void setFldType(ArrayList fldType) {
        this.fldType = fldType;
    }

    /**
     * @return fldDataType
     */
    public ArrayList getFldDataType() {
        return this.fldDataType;
    }

    /**
     * @param fldDataType
     */
    public void setFldDataType(ArrayList fldDataType) {
        this.fldDataType = fldDataType;
    }

    /**
     * @return fldInstructions
     */
    public ArrayList getFldInstructions() {
        return this.fldInstructions;
    }

    /**
     * @param fldInstructions
     */
    public void setFldInstructions(ArrayList fldInstructions) {
        this.fldInstructions = fldInstructions;
    }

    /**
     * @return fldFormat
     */
    public ArrayList getFldFormat() {
        return this.fldFormat;
    }

    /**
     * @param fldFormat
     */
    public void setFldFormat(ArrayList fldFormat) {
        this.fldFormat = fldFormat;
    }

    /**
     * @return fldRepeatFlag
     */
    public ArrayList getFldRepeatFlag() {
        return this.fldRepeatFlag;
    }

    /**
     * @param fldRepeatFlag
     */
    public void setFldRepeatFlag(ArrayList fldRepeatFlag) {
        this.fldRepeatFlag = fldRepeatFlag;
    }

    /**
     * @return fldRepeatFlag
     */
    public ArrayList getFldCatName() {
        return this.fldCatName;
    }

    /**
     * @param fldCatName
     */
    public void setFldCatName(ArrayList fldCatName) {
        this.fldCatName = fldCatName;
    }

    /**
     * Gets the fldBrowserFlag attribute of the FieldLibDao object
     *
     * @return The fldBrowserFlag value
     */
    public ArrayList getFldBrowserFlag() {
        return this.fldBrowserFlag;
    }

    /**
     * Sets the fldBrowserFlag attribute of the FieldLibDao object
     *
     * @param fldBrowserFlag
     *            The new fldBrowserFlag value
     */
    public void setFldBrowserFlag(ArrayList fldBrowserFlag) {
        this.fldBrowserFlag = fldBrowserFlag;
    }

    /**
     * @return rows
     */
    public int getRows() {
        return this.rows;
    }

    /**
     * @param rows
     */
    public void setRows(int rows) {
        this.rows = rows;
    }

    /**
     * Sets the formFldXsl attribute of the FieldLibDao object
     *
     * @param formfldXsl
     *            The new formFldXsl value
     */
    public void setFormFldXsl(String formfldXsl) {
        this.formfldXsl = formfldXsl;
    }

    /**
     * Gets the recordType attribute of the FieldLibDao object
     *
     * @return The recordType value
     */
    public ArrayList getRecordType() {
        return this.recordType;
    }

    /**
     * Sets the recordType attribute of the FieldLibDao object
     *
     * @param recordType
     *            The new recordType value
     */
    public void setRecordType(ArrayList recordType) {
        this.recordType = recordType;
    }

    /**
     * Gets the creator attribute of the FieldLibDao object
     *
     * @return The creator value
     */
    public ArrayList getCreator() {
        return this.creator;
    }

    /**
     * Sets the creator attribute of the FieldLibDao object
     *
     * @param creator
     *            The new creator value
     */
    public void setCreator(ArrayList creator) {
        this.creator = creator;
    }

    /**
     * Gets the ipAdd attribute of the FieldLibDao object
     *
     * @return The ipAdd value
     */
    public ArrayList getIpAdd() {
        return this.ipAdd;
    }

    /**
     * Sets the ipAdd attribute of the FieldLibDao object
     *
     * @param ipAdd
     *            The new ipAdd value
     */
    public void setIpAdd(ArrayList ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * Gets the aStyle attribute of the FieldLibDao object
     *
     * @return The aStyle value
     */
    public ArrayList getAStyle() {
        return this.aStyle;
    }

    /**
     * Sets the aStyle attribute of the FieldLibDao object
     *
     * @param AStyle
     *            The new aStyle value
     */
    public void setAStyle(ArrayList AStyle) {
        this.aStyle = AStyle;
    }

    /**
     * Gets the fldLinesNo attribute of the FieldLibDao object
     *
     * @return The fldLinesNo value
     */
    public ArrayList getFldLinesNo() {
        return this.fldLinesNo;
    }

    /**
     * Sets the fldLinesNo attribute of the FieldLibDao object
     *
     * @param fldLinesNo
     *            The new fldLinesNo value
     */
    public void setFldLinesNo(ArrayList fldLinesNo) {
        this.fldLinesNo = fldLinesNo;
    }

    /**
     * Gets the fldCharsNo attribute of the FieldLibDao object
     *
     * @return The fldCharsNo value
     */
    public ArrayList getFldCharsNo() {
        return this.fldCharsNo;
    }

    /**
     * Sets the fldCharsNo attribute of the FieldLibDao object
     *
     * @param fldCharsNo
     *            The new fldCharsNo value
     */
    public void setFldCharsNo(ArrayList fldCharsNo) {
        this.fldCharsNo = fldCharsNo;
    }

    /**
     * Gets the fldDefResp attribute of the FieldLibDao object
     *
     * @return The fldDefResp value
     */
    public ArrayList getFldDefResp() {
        return this.fldDefResp;
    }

    /**
     * Sets the fldDefResp attribute of the FieldLibDao object
     *
     * @param fldDefResp
     *            The new fldDefResp value
     */
    public void setFldDefResp(ArrayList fldDefResp) {
        this.fldDefResp = fldDefResp;
    }

    /**
     * Gets the accountId attribute of the FieldLibDao object
     *
     * @return The accountId value
     */
    public ArrayList getAccountId() {
        return this.accountId;
    }

    /**
     * Sets the accountId attribute of the FieldLibDao object
     *
     * @param accountId
     *            The new accountId value
     */
    public void setAccountId(ArrayList accountId) {
        this.accountId = accountId;
    }

    /**
     * Gets the fldLength attribute of the FieldLibDao object
     *
     * @return The fldLength value
     */
    public ArrayList getFldLength() {
        return this.fldLength;
    }

    /**
     * Sets the fldLength attribute of the FieldLibDao object
     *
     * @param fldLength
     *            The new fldLength value
     */
    public void setFldLength(ArrayList fldLength) {
        this.fldLength = fldLength;
    }

    /**
     * Gets the formFldId attribute of the FieldLibDao object
     *
     * @return The formFldId value
     */
    public ArrayList getFormFldId() {
        return this.formFldId;
    }

    /**
     * Sets the formFldId attribute of the FieldLibDao object
     *
     * @param formFldId
     *            The new formFldId value
     */
    public void setFormFldId(ArrayList formFldId) {
        this.formFldId = formFldId;
    }

    /**
     * Changes for the Phase 2 Made By : Sonia Kaura Date : 11/06/2003
     *
     * @param fieldLibId
     *            The new fieldLibId value
     */

    /**
     * @param fieldLibId
     *            The new fieldLibId value
     */
    /*
     * public ArrayList getFldTodayCheck() { return this.fldTodayCheck; }
     */

    /**
     * @param fieldLibId
     *            The new fieldLibId value
     */
    /*
     * public void setFldTodayCheck(ArrayList fldTodayCheck) {
     * this.fldTodayCheck = fldTodayCheck; }
     */

    /**
     * @param fieldLibId
     *            The new fieldLibId value
     */
    /*
     * public ArrayList getFldOverride() { return this.fldOverride; }
     */

    /**
     * @param fieldLibId
     *            The new fieldLibId value
     */
    /*
     * public void setFldOverride(ArrayList fldOverride) { this.fldOverride =
     * fldOverride; }
     */

    // END OF GETTERS AND SETTER (ARRAY LIST)
    // //////////////////////////////////////////////////////////////////////
    // GETTERS AND SETTER (INDIVIDUAL ELEMENT)
    /**
     * @param fieldLibId
     */
    public void setFieldLibId(Integer fieldLibId) {

        this.fieldLibId.add(fieldLibId);
    }

    /**
     * @param catLibId
     */
    public void setCatLibId(Integer catLibId) {
        this.catLibId.add(catLibId);
    }

    /**
     * @param libFlag
     */
    public void setLibFlag(String libFlag) {
        this.libFlag.add(libFlag);
    }

    /**
     * @param fldName
     */
    public void setFldName(String fldName) {
        this.fldName.add(StringUtil.escapeSpecialCharHTML(fldName));
    }

    /**
     * @param fldDesc
     */
    public void setFldDesc(String fldDesc) {
        this.fldDesc.add(fldDesc);
    }

    /**
     * @param fldUniqId
     */
    public void setFldUniqId(String fldUniqId) {
        this.fldUniqId.add(StringUtil.escapeSpecialCharHTML(fldUniqId));
    }

    /**
     * @param fldSysID
     */
    public void setFldSysID(String fldSysID) {
        this.fldSysID.add(fldSysID);
    }

    /**
     * @param fldKeyword
     */
    public void setFldKeyword(String fldKeyword) {
        this.fldKeyword.add(fldKeyword);
    }

    /**
     * @param fldType
     */
    public void setFldType(String fldType) {
        this.fldType.add(fldType);
    }

    /**
     * @param fldInstructions
     */
    public void setFldInstructions(String fldInstructions) {
        this.fldInstructions.add(fldInstructions);
    }

    /**
     * @param fldFormat
     */
    public void setFldFormat(String fldFormat) {
        this.fldFormat.add(fldFormat);
    }

    /**
     * @param fldRepeatFlag
     */
    public void setFldRepeatFlag(Integer fldRepeatFlag) {
        this.fldRepeatFlag.add(fldRepeatFlag);
    }

    /**
     * @param fldCatName
     */
    public void setFldCatName(String fldCatName) {
        this.fldCatName.add(fldCatName);
    }

    /**
     * @param fldDataType
     */
    public void setFldDataType(String fldDataType) {
        this.fldDataType.add(fldDataType);
    }

    /**
     * Sets the fldBrowserFlag attribute of the FieldLibDao object
     *
     * @param fldBrowserFlag
     *            The new fldBrowserFlag value
     */
    public void setFldBrowserFlag(String fldBrowserFlag) {
        this.fldBrowserFlag.add(fldBrowserFlag);
    }

    /**
     * Changes for the Phase 2 Made By : Sonia Kaura Date : 11/06/2003
     *
     * @param count
     *            Description of the Parameter
     * @return The fieldLibId value
     */

    /**
     * @param count
     *            Description of the Parameter
     * @return The fieldLibId value
     */
    /*
     * public void setFldTodayCheck(Integer fldTodayCheck) {
     * this.fldTodayCheck.add ( fldTodayCheck ); }
     */
    /**
     * @param count
     *            Description of the Parameter
     * @return The fieldLibId value
     */
    /*
     * public void setFldOverride(Integer fldOverride) { this.fldOverride.add (
     * fldOverride ); }
     */

    // ///////////////////////////////////////////////////////////
    // GETTERS AND SETTER (INDIVIDUAL ELEMENT)
    /**
     * @param count
     *            Description of the Parameter
     * @return fieldLibId
     */
    public Object getFieldLibId(int count) {

        return this.fieldLibId.get(count);
    }

    /**
     * @param count
     *            Description of the Parameter
     * @return catLibId
     */
    public Object getCatLibId(int count) {
        return this.catLibId.get(count);
    }

    /**
     * @param count
     *            Description of the Parameter
     * @return libFlag
     */
    public Object getLibFlag(int count) {
        return this.libFlag.get(count);
    }

    /**
     * @param count
     *            Description of the Parameter
     * @return fldName
     */
    public Object getFldName(int count) {
        return this.fldName.get(count);
    }

    /**
     * @param count
     *            Description of the Parameter
     * @return fldDesc
     */
    public Object getFldDesc(int count) {
        return this.fldDesc.get(count);
    }

    /**
     * @param count
     *            Description of the Parameter
     * @return fldUniqId
     */
    public Object getFldUniqId(int count) {
        return this.fldUniqId.get(count);
    }

    /**
     * @param count
     *            Description of the Parameter
     * @return fldSysID
     */
    public Object getFldSysID(int count) {
        return this.fldSysID.get(count);
    }

    /**
     * @param count
     *            Description of the Parameter
     * @return fldKeyword
     */
    public Object getFldKeyword(int count) {
        return this.fldKeyword.get(count);
    }

    /**
     * @param count
     *            Description of the Parameter
     * @return fldType
     */
    public Object getFldType(int count) {
        return this.fldType.get(count);
    }

    /**
     * @param count
     *            Description of the Parameter
     * @return fldInstructions
     */
    public Object getFldInstructions(int count) {
        return this.fldInstructions.get(count);
    }

    /**
     * @param count
     *            Description of the Parameter
     * @return fldFormat
     */
    public Object getFldFormat(int count) {
        return this.fldFormat.get(count);
    }

    /**
     * @param count
     *            Description of the Parameter
     * @return fldRepeatFlag
     */
    public Object getFldRepeatFlag(int count) {
        return this.fldRepeatFlag.get(count);
    }

    /**
     * @param count
     *            Description of the Parameter
     * @return FldCatName
     */
    public Object getFldCatName(int count) {
        return this.fldCatName.get(count);
    }

    /**
     * @param count
     *            Description of the Parameter
     * @return FldDataType
     */
    public Object getFldDataType(int count) {
        return this.fldDataType.get(count);
    }

    /**
     * @return
     */
    public String getFormSecId() {
        return this.formSecId;
    }

    /**
     * Sets the formSecId attribute of the FieldLibDao object
     *
     * @param formSecId
     *            The new formSecId value
     */
    public void setFormSecId(String formSecId) {
        this.formSecId = formSecId;
    }

    /**
     * @return formfldSeq
     */
    public String getFormFldSeq() {
        return this.formfldSeq;
    }

    /**
     * @param formfldSeq
     */
    public void setFormFldSeq(String formfldSeq) {
        this.formfldSeq = formfldSeq;
    }

    /**
     * @return formfldMandatory
     */
    public String getFormFldMandatory() {
        return this.formfldMandatory;
    }

    /**
     * @param formfldMandatory
     */
    public void setFormFldMandatory(String formfldMandatory) {
        this.formfldMandatory = formfldMandatory;
    }

    /**
     * @return formFldBrowserFlg
     */
    public String getFormFldBrowserFlg() {
        return this.formFldBrowserFlg;
    }

    /**
     * @param formFldBrowserflg
     */
    public void setFormFldBrowserFlg(String formFldBrowserflg) {
        this.formFldBrowserFlg = formFldBrowserFlg;
    }

    /**
     * @return formfldXsl
     */
    public String getFormFldXsl() {
        return this.formfldXsl;
    }

    /**
     * @param i
     *            Description of the Parameter
     * @return The recordType value
     */

    public Object getRecordType(int i) {
        return this.recordType.get(i);
    }

    /**
     * Sets the recordType attribute of the FieldLibDao object
     *
     * @param recordType
     *            The new recordType value
     */
    public void setRecordType(String recordType) {
        this.recordType.add(recordType);
    }

    /**
     * Gets the creator attribute of the FieldLibDao object
     *
     * @param i
     *            Description of the Parameter
     * @return The creator value
     */
    public Object getCreator(int i) {
        return this.creator.get(i);
    }

    /**
     * Sets the creator attribute of the FieldLibDao object
     *
     * @param creator
     *            The new creator value
     */
    public void setCreator(String creator) {
        this.creator.add(creator);
    }

    /**
     * Gets the ipAdd attribute of the FieldLibDao object
     *
     * @param i
     *            Description of the Parameter
     * @return The ipAdd value
     */
    public Object getIpAdd(int i) {
        return this.ipAdd.get(i);
    }

    /**
     * Sets the ipAdd attribute of the FieldLibDao object
     *
     * @param ipAdd
     *            The new ipAdd value
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd.add(ipAdd);
    }

    /**
     * Gets the fldLinesNo attribute of the FieldLibDao object
     *
     * @param i
     *            Description of the Parameter
     * @return The fldLinesNo value
     */
    public Object getFldLinesNo(int i) {
        return this.fldLinesNo.get(i);
    }

    /**
     * Sets the fldLinesNo attribute of the FieldLibDao object
     *
     * @param fldLinesNo
     *            The new fldLinesNo value
     */
    public void setFldLinesNo(String fldLinesNo) {
        this.fldLinesNo.add(fldLinesNo);
    }

    /**
     * Gets the fldCharsNo attribute of the FieldLibDao object
     *
     * @param i
     *            Description of the Parameter
     * @return The fldCharsNo value
     */
    public Object getFldCharsNo(int i) {
        return this.fldCharsNo.get(i);
    }

    /**
     * Sets the fldCharsNo attribute of the FieldLibDao object
     *
     * @param fldCharsNo
     *            The new fldCharsNo value
     */
    public void setFldCharsNo(String fldCharsNo) {
        this.fldCharsNo.add(fldCharsNo);
    }

    /**
     * Gets the fldDefResp attribute of the FieldLibDao object
     *
     * @param i
     *            Description of the Parameter
     * @return The fldDefResp value
     */
    public Object getFldDefResp(int i) {
        return this.fldDefResp.get(i);
    }

    /**
     * Sets the fldDefResp attribute of the FieldLibDao object
     *
     * @param fldDefResp
     *            The new fldDefResp value
     */
    public void setFldDefResp(String fldDefResp) {
        this.fldDefResp.add(fldDefResp);
    }

    /**
     * Gets the accountId attribute of the FieldLibDao object
     *
     * @param i
     *            Description of the Parameter
     * @return The accountId value
     */
    public Object getAccountId(int i) {
        return this.accountId.get(i);
    }

    /**
     * Sets the accountId attribute of the FieldLibDao object
     *
     * @param accountId
     *            The new accountId value
     */
    public void setAccountId(String accountId) {
        this.accountId.add(accountId);
    }

    /**
     * Gets the fldLength attribute of the FieldLibDao object
     *
     * @param i
     *            Description of the Parameter
     * @return The fldLength value
     */
    public Object getFldLength(int i) {
        return this.fldLength.get(i);
    }

    /**
     * Sets the fldLength attribute of the FieldLibDao object
     *
     * @param fldLength
     *            The new fldLength value
     */
    public void setFldLength(String fldLength) {
        this.fldLength.add(fldLength);
    }

    /**
     * Sets the formFldId attribute of the FieldLibDao object
     *
     * @param formFldId
     *            The new formFldId value
     */
    public void setFormFldId(String formFldId) {
        this.formFldId.add(formFldId);
    }

    /**
     * Gets the formFldId attribute of the FieldLibDao object
     *
     * @param i
     *            Description of the Parameter
     * @return The formFldId value
     */
    public Object getFormFldId(int i) {
        return this.formFldId.get(i);
    }

    // END OF GETTERS AND SETTER (INDIVIDUAL ELEMENTS)
    // //////////////////////////////////////////////////////////////////////

    /**
     * Constructor for the FieldLibDao object
     */
    public FieldLibDao() {
        fieldLibId = new ArrayList();
        catLibId = new ArrayList();
        libFlag = new ArrayList();
        fldName = new ArrayList();
        fldDesc = new ArrayList();
        fldUniqId = new ArrayList();
        fldSysID = new ArrayList();
        fldKeyword = new ArrayList();
        fldType = new ArrayList();
        fldDataType = new ArrayList();
        fldInstructions = new ArrayList();
        fldFormat = new ArrayList();
        fldRepeatFlag = new ArrayList();
        fldCatName = new ArrayList();
        recordType = new ArrayList();
        creator = new ArrayList();
        ipAdd = new ArrayList();
        aStyle = new ArrayList();
        fldLinesNo = new ArrayList();
        fldCharsNo = new ArrayList();
        fldDefResp = new ArrayList();
        accountId = new ArrayList();
        fldLength = new ArrayList();
        formFldId = new ArrayList();
        fieldLibId = new ArrayList();
        fldBrowserFlag = new ArrayList();

        // fldTodayCheck = new ArrayList();
        // fldOverride = new ArrayList();

    }

    /**
     * Gets the fieldlib table attributes depending upon the various search
     * criteria
     *
     * @param fkCatlib
     * @param fkAccount
     * @param fkFormId
     * @param name
     * @param keyword
     */
    public void getFieldsFromSearch(int fkCatlib, int fkAccount, int fkFormId,
            String name, String keyword) {

        int rows = 0;

        Statement stmt = null;
        Connection conn = null;
        String str1;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        try {
            conn = getConnection();

            if (fkFormId == 0) {

                str1 = "SELECT NVL(FL.PK_FIELD, -999)   PK_FIELD,  FL.FK_CATLIB  , NVL(FL.FLD_LIBFLAG,'-') FLD_LIBFLAG ,"
                        + "  NVL(FL.FLD_NAME,'-' ) FLD_NAME , NVL(FL.FLD_DESC, '-' ) FLD_DESC, NVL(FL.FLD_UNIQUEID,'-') FLD_UNIQUEID , "
                        + "  NVL(FL.FLD_SYSTEMID,'-') FLD_SYSTEMID, NVL(FL.FLD_KEYWORD, '-') FLD_KEYWORD,NVL(FL.FLD_TYPE, '-') FLD_TYPE,"
                        + "  NVL(FL.FLD_DATATYPE,'-' ) FLD_DATATYPE,NVL(FL.FLD_INSTRUCTIONS,'-') FLD_INSTRUCTIONS,NVL(FL.FLD_FORMAT,'-') FLD_FORMAT ,"
                        + "  NVL(FL.FLD_REPEATFLAG,0)FLD_REPEATFLAG , CAT.CATLIB_NAME CATLIB_NAME";

                str2 = "  FROM ER_FLDLIB FL ,  ER_CATLIB CAT  WHERE FL.FK_ACCOUNT=  "
                        + fkAccount
                        + " AND  FL.RECORD_TYPE <> 'D'   AND CAT.RECORD_TYPE <> 'D'  "
                        + " AND   FL.FK_CATLIB = CAT.PK_CATLIB  "
                        + " AND FL.FLD_LIBFLAG = 'L' ";

            } else {

                str1 = "SELECT VF.PK_FIELD,VF.FORM_CATEGORY,VF.FLD_LIBFLAG,"
                        + "  VF.FLD_NAME,  VF.FLD_DESC,VF.FLD_UNIQUEID, "
                        + "  VF.FLD_SYSTEMID,  VF.FLD_KEYWORD,VF.FLD_TYPE,"
                        + "  VF.FLD_DATATYPE,FL.FLD_INSTRUCTIONS,FL.FLD_FORMAT,"
                        + "  NVL(VF.FLD_REPEATFLAG,0), VF.FLD_CATLIB_NAME  ";

                str2 = "   FROM  ERV_FORMFLDS VF,  ER_FLDLIB FL   WHERE "
                        + "  VF.PK_FIELD = FL.PK_FIELD AND  "
                        + "  VF.FK_ACCOUNT = FL.FK_ACCOUNT = " + fkAccount;

            }

            if (!name.equals("")) {
                str3 = " AND lower(FLD_NAME) LIKE lower('%" + name + "%')";
            } else {
                str3 = " ";
            }

            if (!keyword.equals("")) {
                str4 = " AND lower(FLD_KEYWORD)  LIKE lower('%" + keyword
                        + "%') ";
            } else {
                str4 = " ";
            }

            if (fkCatlib == 0 || fkCatlib == -1) {
                str5 = "  ";

            } else {
                str5 = " AND FK_CATLIB = " + fkCatlib;
            }

            if (fkCatlib == 0 || fkCatlib == -1) {
                str6 = "  UNION  (   SELECT  NULL   , PK_CATLIB  ,NULL, "
                        + "  NULL 	, NULL , NULL ,"
                        + "  NULL , NULL , NULL , "
                        + "  NULL , NULL , NULL , "
                        + "  NULL , CATLIB_NAME  "
                        + "  FROM ER_CATLIB  WHERE FK_ACCOUNT =  "
                        + fkAccount
                        + "  AND CATLIB_TYPE='C' AND RECORD_TYPE <> 'D' "
                        + "  AND PK_CATLIB  NOT IN ( SELECT FK_CATLIB "
                        + "	 FROM ER_FLDLIB F , ER_CATLIB C    "
                        + "  WHERE   F.FK_ACCOUNT  = "
                        + fkAccount
                        + "   AND "
                        + "  F.RECORD_TYPE <> 'D' AND FK_CATLIB = PK_CATLIB  )) ";

            } else {
                str6 = "  ";
            }

            if (fkFormId == 0) {
                str7 = "ORDER BY ( FK_CATLIB )";
            } else {
                str7 = "ORDER BY (FLD_CATEGORY)";
            }

            stmt = conn.createStatement();

            Rlog.debug("fieldlib", "in DAO getFieldsFromSearch()###" + str1
                    + str2 + str3 + str4 + str5 + str6 + str7);
            ResultSet rs = null;
            try {
                stmt.execute(str1 + str2 + str3 + str4 + str5 + str6 + str7);
                rs = stmt.getResultSet();
            } catch (Exception ex) {
                Rlog.fatal("fieldlib",
                        "FieldLib. getFieldsFromSearch IN THE EXCECUTE $$$$$###"
                                + ex);
            }

            if (rs != null) {

                while (rs.next()) {

                    setFieldLibId(new Integer(rs.getInt("PK_FIELD")));

                    setCatLibId(new Integer(rs.getInt("FK_CATLIB")));

                    setLibFlag(rs.getString("FLD_LIBFLAG"));

                    setFldName(rs.getString("FLD_NAME"));

                    setFldDesc(rs.getString("FLD_DESC"));

                    setFldUniqId(rs.getString("FLD_UNIQUEID"));

                    setFldSysID(rs.getString("FLD_SYSTEMID"));

                    setFldKeyword(rs.getString("FLD_KEYWORD"));

                    setFldType(rs.getString("FLD_TYPE"));

                    setFldDataType(rs.getString("FLD_DATATYPE"));

                    setFldInstructions(rs.getString("FLD_INSTRUCTIONS"));

                    setFldFormat(rs.getString("FLD_FORMAT"));

                    setFldRepeatFlag(new Integer(rs.getInt("FLD_REPEATFLAG")));

                    if (fkFormId == 0) {
                        String s = rs.getString("CATLIB_NAME");
                        setFldCatName(s);

                    } else {
                        setFldCatName(rs.getString(14));

                    }

                    rows++;

                }
            }

            setRows(rows);

        } catch (Exception ex) {
            Rlog.fatal("fieldlib",
                    "FieldLib. getFieldsFromSearch EXCEPTION IN FETCHING FROM ER_FIELDLIB TABLE"
                            + ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

    /**
     * This method calls a stored procedure to copy a field passed to this
     * method as a <br>
     * parameter
     *
     * @param pkField
     * @param libFlag
     * @param user
     * @param ipAdd
     * @return int
     */
    public int updateFieldLibRecord(int pkField, String libFlag, String user,
            String ipAdd) {

        int ret = 1;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            Rlog.debug("fieldlib",
                    "In updateFieldLibRecord updateFieldLibRecord() ");
            cstmt = conn
                    .prepareCall("{call PKG_FORM.SP_COPYFIELD_TOLIB(?,?,?,?,?)}");
            cstmt.setInt(1, pkField);
            cstmt.setString(2, libFlag);
            cstmt.setString(3, user);
            cstmt.setString(4, ipAdd);
            cstmt.registerOutParameter(5, java.sql.Types.INTEGER);
            cstmt.execute();
            ret = cstmt.getInt(5);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "EXCEPTION in updateFieldLibRecord, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }

    }

    // public void insertToFormField( )
    // {

    //
    // setFieldLibId(fieldLibId);
    // setCatLibId( catLibId );
    // setLibFlag( libFlag );
    // setFldName( fldName );
    // setFldDesc(fldDesc );
    // setFldUniqId( fldUniqId );
    // setFldSysID( fldSysID );
    // setFldKeyword( fldKeyword );
    // setFldType( fldType );
    // setFldDataType( fldDataType );
    // setFldInstructions( fldInstructions);
    // setFldFormat( fldFormat );
    // setFldRepeatFlag( fldRepeatFlag );
    // setFldCatName(fldCatName );
    // setFormSecId(formSecId );
    // setFormFldSeq( formfldSeq );
    // setFormFldMandatory( formfldMandatory );
    // setFormFldBrowserFlg( formFldBrowserFlg );
    // setRecordType(recordType);
    // setCreator(creator);
    // setIpAdd(ipAdd);
    // setAStyle(aStyle);
    //
    //
    //

    // }

    /**
     * @param secId
     * @param idArray
     * @param creator
     *            Description of the Parameter
     * @param ipAdd
     *            Description of the Parameter
     * @return int
     */

    public int copyMultipleFields(String secId, String[] idArray,
            String creator, String ipAdd) {

        int ret = 0;
        // String[] pkFormFldIds =null ;
        // String[] pkFieldLibIds=null;

        // Array outFormIdArray = null;

        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            ArrayDescriptor inIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inIdArray = new ARRAY(inIds, conn, idArray);

            Rlog.debug("fieldlib", "In SEC ID  " + secId);
            Rlog.debug("fieldlib", "In ARRAY ID  " + idArray[0]);
            Rlog.debug("fieldlib", "In creator ID  " + creator);
            Rlog.debug("fieldlib", "In ipAdd  " + ipAdd);
            Rlog.debug("fieldlib",
                    "In copyMultipleFields updateFieldLibRecord() ");

            cstmt = conn
                    .prepareCall("{call PKG_FORM.SP_COPY_MULTIPLE_FLDS_FROM_LIB(?,?,?,?,?,?,?)}");

            Rlog.debug("fieldlib", "Before setting SecID  ");
            cstmt.setString(1, secId);
            Rlog.debug("fieldlib", "Before setting IDARRAY  ");
            cstmt.setArray(2, inIdArray);
            Rlog.debug("fieldlib", "Before setting CREATOR  ");
            cstmt.setString(3, creator);
            Rlog.debug("fieldlib", "Before setting IPADD  ");
            cstmt.setString(4, ipAdd);
            Rlog.debug("fieldlib", "Before registering 55  ");
            cstmt.registerOutParameter(5, java.sql.Types.INTEGER);
            Rlog.debug("fieldlib", "Before registering 66  ");

            cstmt.registerOutParameter(6, java.sql.Types.ARRAY, "ARRAY_STRING");

            Rlog.debug("fieldlib", "Before registering 77  ");

            cstmt.registerOutParameter(7, java.sql.Types.ARRAY, "ARRAY_STRING");

            cstmt.execute();
            Rlog.debug("fieldlib", "Before getting 55  ");
            ret = cstmt.getInt(5);
            Rlog.debug("fieldlib", "Before getting 66  ");

            Array outFormFIdArray = cstmt.getArray(6);

            Rlog.debug("fieldlib", "Before getting 67  ");

            Array outFieldIdArray = cstmt.getArray(7);

            //ResultSet formIdResultSet = outFormIdArray.getResultSet();
            //ResultSet fieldLibIdResultSet = outFieldIdArray.getResultSet();

            Rlog.debug("fieldlib", "outFormFIdArray  " + outFormFIdArray);
            if (outFormFIdArray != null) {


                String[] newFormFldIdArray = (String[]) outFormFIdArray.getArray();

                Rlog.debug("fieldlib", "newFormFldIdArray  not null" + newFormFldIdArray.length);

                for (int k = 0; k < newFormFldIdArray.length; k++)
                {
                	setFormFldId(newFormFldIdArray[k]);
                }

            }

            Rlog.debug("fieldlib", "outFieldIdArray  " + outFieldIdArray);
            if (outFieldIdArray != null) {
                String[] neFldIdArray = (String[]) outFieldIdArray.getArray();

                Rlog.debug("fieldlib", "neFldIdArray  not null" + neFldIdArray.length);

                for (int k = 0; k < neFldIdArray.length; k++)
                {

                	setFieldLibId(Integer.valueOf(neFldIdArray[k]));
                }

            }


            /*while (formIdResultSet.next()) {
            	Rlog.debug("fieldlib", "field copied......" + formIdResultSet.getString(2));
                setFormFldId(formIdResultSet.getString(2));
            }

            while (fieldLibIdResultSet.next()) {
                setFieldLibId(Integer.valueOf(fieldLibIdResultSet.getString(2)));

            }*/

            copyFieldsToFormField();

            Rlog.debug("fieldlib", "Before getting 68  ");

            return ret;
        } catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("fieldlib",
                    "EXCEPTION in copyMultipleFields, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }

    }

    /**
     */
    public void copyFieldsToFormField() {

        int ret = 0;

        int size = 0;

        try {

            Rlog.debug("fieldlib", "Thesize of the FieldLibArray "
                    + fieldLibId.size());
            Rlog.debug("fieldlib", "Thesize of the FormFldIdArrya "
                    + formFldId.size());
            Rlog.debug("fieldlib", " FieldLibID "
                    + fieldLibId.get(0).toString());
            Rlog.debug("fieldlib", "FormFldId " + formFldId.get(0).toString());

            size = fieldLibId.size();
            for (int cnt = 0; cnt < size; cnt++) {

                FieldLibJB fieldLibJB = new FieldLibJB();
                FieldLibBean fieldLsk = new FieldLibBean();
                fieldLibJB.setFieldLibId(StringUtil.stringToNum(getFieldLibId(cnt)
                        .toString()));
                fieldLsk = fieldLibJB.getFieldLibDetails();
                fieldLsk.setFormFldId(getFormFldId(cnt).toString());

                ret = fieldLibJB.updateToFormField(fieldLsk);

            }

            Rlog.debug("fieldlib",
                    "AFTer the For LOOP IN THE  Copy To FormFLd "
                            + fieldLibId.size());

        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "EXCEPTION in copyFieldsToFormField CALLING FORMFLD " + e);

        }

    }

    /**
     * Description of the Method
     *
     * @param pk_fieldId
     *            Description of the Parameter
     * @param pk_formFldId
     *            Description of the Parameter
     */
    public void copyFieldFromFormToForm(int pk_fieldId, int pk_formFldId) {

        int ret = 0;

        Rlog
                .debug("fieldlib",
                        "Start of the copyFieldFromFormToForm(pkFieldId, pk_FormFldId) ");
        try {

            Rlog.debug("fieldlib", "FIELD LIB ID " + pk_fieldId);
            Rlog.debug("fieldlib", "FORM ID " + pk_formFldId);

            FieldLibJB fieldLibJB = new FieldLibJB();
            FieldLibBean fieldLsk = new FieldLibBean();
            fieldLibJB.setFieldLibId(pk_fieldId);
            fieldLsk = fieldLibJB.getFieldLibDetails();

            /*
             * modified by sonia abrol, get other attributes
             */
            FormFieldJB formFieldJB = new FormFieldJB();
            formFieldJB.setFormFieldId(pk_formFldId);
            formFieldJB.getFormFieldDetails();

            fieldLsk.setFormFldId(String.valueOf(pk_formFldId));
            fieldLsk.setFormFldMandatory(formFieldJB.getFormFldMandatory());

            ret = fieldLibJB.updateToFormField(fieldLsk);

            Rlog.debug("fieldlib",
                    "After updateToFormField in copyFieldFromFormToForm() ");

        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "Exception in copyFieldsFromFormToForm(pkField, pkFormFld)  "
                            + e);

        }
    }

    /**
     * Gets the fieldNames attribute of the FieldLibDao object
     *
     * @param i
     *            Description of the Parameter
     */
    public void getFieldNames(int i) {
        Rlog.debug("fieldlib", "anuformid " + i);
        int j = 0;
        String sql = "";
        PreparedStatement preparedstatement = null;
        Connection connection = null;
        try {
            connection = getConnection();

            sql = "select  "
                    + "fd.fld_name || ' [' || nvl(fld_uniqueid,case when fld_type='C' then 'Comment Type' when fld_type ='H' then 'Horizontal Rule' end)  || '] ' fld_name ,fd.pk_field "
                    + "  from er_fldlib fd,er_formlib fl,er_formsec fs,er_formfld ff  where "
                    + " fl.pk_formlib=fs.fk_formlib  and fs.pk_formsec=ff.fk_formsec  and ff.fk_field=fd.pk_field  and nvl(fd.fld_datatype,'-') <> 'ML' and  "
                    + " nvl ( fd.record_type,'Z') <> 'D'  and NVL(fd.fld_repeatflag,0)=0  and fl.pk_formlib=? "
                    + " order by formsec_seq , formfld_seq ,pk_formsec ";

            preparedstatement = connection.prepareStatement(sql);
            preparedstatement.setInt(1, i);
            Rlog.debug("fieldlib", "sql::: " + sql);
            Rlog.debug("fieldlib", "pstmt::: " + preparedstatement);
            ResultSet resultset = preparedstatement.executeQuery();
            Rlog.debug("fieldlib", "rs::: " + resultset);
            while (resultset.next()) {
                Rlog.debug("fieldlib", "getFieldNames.getFieldNames rows " + j);
                setFieldLibId(new Integer(resultset.getInt("pk_field")));
                setFldName(resultset.getString("fld_name"));
                j++;
            }

            setCRows(j);
        } catch (SQLException sqlexception) {
            Rlog.fatal("fieldlib",
                    "getFieldNames.getFieldNames EXCEPTION IN FETCHING FROM ER_FLDRESP table"
                            + sqlexception);
        } finally {
            try {
                if (preparedstatement != null) {
                    preparedstatement.close();
                }
            } catch (Exception exception1) {
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception exception2) {
            }
        }
    }

    /**
     * Gets the fltrdFieldNames attribute of the FieldLibDao object
     *
     * @param i
     *            Primary key of the form
     */
    public void getFltrdFieldNames(int i) {
        Rlog.debug("fieldlib", "anuformid " + i);
        int j = 0;
        String sql = "";
        PreparedStatement preparedstatement = null;
        Connection connection = null;
        try {
            connection = getConnection();

            sql = "select  "
                    + "fd.fld_name || ' [' || nvl(fld_uniqueid,case when fld_type='C' then 'Comment Type' when fld_type ='H' then 'Horizontal Rule' end)  || '] ' fld_name ,fd.pk_field, "
                    + "fd.fld_type as fld_type,fd.fld_uniqueid as uniqid "
                    + " from er_fldlib fd,er_formlib fl,er_formsec fs,er_formfld ff  where "
                    + " fl.pk_formlib=fs.fk_formlib  and fs.pk_formsec=ff.fk_formsec  and ff.fk_field=fd.pk_field  and nvl(fd.fld_datatype,'-') <> 'ML' and  "
                    + " nvl ( fd.record_type,'Z') <> 'D'  and NVL(fd.fld_repeatflag,0)=0  and fl.pk_formlib=? "
                    + "  and (fd.fld_type not in ('H','S')) and ((fd.fld_uniqueid<>'er_def_date_01') or (fld_uniqueid IS NULL) )  "
                    + "  order by formsec_seq , formfld_seq ,pk_formsec ";

            preparedstatement = connection.prepareStatement(sql);
            preparedstatement.setInt(1, i);
            Rlog.debug("fieldlib", "sql::: " + sql);
            Rlog.debug("fieldlib", "pstmt::: " + preparedstatement);
            ResultSet resultset = preparedstatement.executeQuery();
            Rlog.debug("fieldlib", "rs::: " + resultset);
            while (resultset.next()) {
                Rlog.debug("fieldlib", "getFieldNames.getFieldNames rows " + j);
                setFieldLibId(new Integer(resultset.getInt("pk_field")));
                setFldName(resultset.getString("fld_name"));
                setFldType(resultset.getString("fld_type"));
                setFldUniqId(resultset.getString("uniqid"));
                j++;
            }

            setCRows(j);
        } catch (SQLException sqlexception) {
            Rlog.fatal("fieldlib",
                    "getFieldNames.getFieldNames EXCEPTION IN FETCHING FROM ER_FLDRESP table"
                            + sqlexception);
        } finally {
            try {
                if (preparedstatement != null) {
                    preparedstatement.close();
                }
            } catch (Exception exception1) {
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception exception2) {
            }
        }
    }

    /**
     * Description of the Method
     *
     * @param browserOne
     *            Description of the Parameter
     * @param browserZero
     *            Description of the Parameter
     * @param lastModBy
     *            Description of the Parameter
     * @param recordType
     *            Description of the Parameter
     * @param ipAdd
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public int updateBrowserFlag(String browserOne, String browserZero,
            int lastModBy, String recordType, String ipAdd) {

        Rlog.debug("fieldlib", "form inside updateBrowserFlag");
        int ret = 1;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            Rlog.debug("fieldlib",
                    "In updateBrowserFlag calling SP updateBrowserFlag() ");
            cstmt = conn
                    .prepareCall("{call PKG_FORM.SP_SETFORMFLD_BROWSERFLAG(?,?,?,?,?,?)}");
            Rlog.debug("fieldlib",
                    "after prepare call of SP_SETFORMFLD_BROWSERFLAG");

            cstmt.setString(1, browserOne);
            cstmt.setString(2, browserZero);
            cstmt.setInt(3, lastModBy);
            cstmt.setString(4, recordType);
            cstmt.setString(5, ipAdd);

            cstmt.registerOutParameter(6, java.sql.Types.INTEGER);
            cstmt.execute();
            ret = cstmt.getInt(6);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "EXCEPTION in updateBrowserFlag, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }

    }

    /**
     * Gets the fieldsInformation attribute of the FieldLibDao object
     *
     * @param formId
     *            Description of the Parameter
     */
    public void getFieldsInformation(int formId) {

        Rlog.debug("fieldlib", "inside getFieldsInformation");
        int ret = 1;
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        try {

            conn = getConnection();
            Rlog.debug("fieldlib", "In getFieldsInformation () ");
            sql = " select pk_formfld, fld_name, pk_field, "
                    + " fld_uniqueid, fld_type, fld_systemid , "
                    + " formfld_browserflg " + " from erv_formflds "
                    + " where pk_formlib= ? ";

            Rlog.debug("fieldlib", " sql" + sql);

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, formId);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setFormFldId(rs.getString("PK_FORMFLD"));
                setFldName(rs.getString("FLD_NAME"));
                setFieldLibId(new Integer(rs.getInt("PK_FIELD")));
                setFldUniqId(rs.getString("FLD_UNIQUEID"));
                setFldSysID(rs.getString("fld_systemid"));
                setFldType(rs.getString("FLD_TYPE"));
                setFldBrowserFlag(rs.getString("FORMFLD_BROWSERFLG"));
                rows++;
            }
            setCRows(rows);
            Rlog.debug("fieldlib", "number of rows" + rows);

        } catch (SQLException ex) {
            Rlog.fatal("fieldlib",
                    "getFieldsInformation EXCEPTION IN FETCHING FROM table"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

    // ///////////////////////////////////////
    /**
     * Gets the fieldlib table attributes depending upon the various search
     * criteria
     *
     * @param fkCatlib
     * @param fkAccount
     * @param name
     * @param keyword
     */
    public void getFieldsForCopy(int fkCatlib, int fkAccount, String name,
            String keyword) {

        int rows = 0;

        Statement stmt = null;
        Connection conn = null;
        String str1;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        try {
            conn = getConnection();

            str1 = "SELECT NVL(FL.PK_FIELD, -999)   PK_FIELD,  FL.FK_CATLIB  , NVL(FL.FLD_LIBFLAG,'-') FLD_LIBFLAG ,"
                    + "  NVL(FL.FLD_NAME,'-' ) FLD_NAME , NVL(FL.FLD_DESC, '-' ) FLD_DESC, NVL(FL.FLD_UNIQUEID,'-') FLD_UNIQUEID , "
                    + "  NVL(FL.FLD_SYSTEMID,'-') FLD_SYSTEMID, NVL(FL.FLD_KEYWORD, '-') FLD_KEYWORD,NVL(FL.FLD_TYPE, '-') FLD_TYPE,"
                    + "  NVL(FL.FLD_DATATYPE,'-' ) FLD_DATATYPE,NVL(FL.FLD_INSTRUCTIONS,'-') FLD_INSTRUCTIONS,NVL(FL.FLD_FORMAT,'-') FLD_FORMAT ,"
                    + "  NVL(FL.FLD_REPEATFLAG,0)FLD_REPEATFLAG , CAT.CATLIB_NAME CATLIB_NAME";

            str2 = "  FROM ER_FLDLIB FL ,  ER_CATLIB CAT  WHERE FL.FK_ACCOUNT=  "
                    + fkAccount
                    + " AND  FL.RECORD_TYPE <> 'D'  AND CAT.RECORD_TYPE <> 'D'  "
                    + " AND   FL.FK_CATLIB = CAT.PK_CATLIB  "
                    + " AND FL.FLD_LIBFLAG = 'L' ";

            if (!name.equals("")) {
                str3 = " AND lower(FLD_NAME) LIKE lower('%" + name + "%')";
            } else {
                str3 = " ";
            }

            if (!keyword.equals("")) {
                str4 = " AND lower(FLD_KEYWORD)  LIKE lower('%" + keyword
                        + "%') ";
            } else {
                str4 = " ";
            }

            if (fkCatlib == 0 || fkCatlib == -1) {
                str5 = "  ";

            } else {
                str5 = " AND FK_CATLIB = " + fkCatlib;
            }

            str6 = "  ORDER BY ( FK_CATLIB )";

            stmt = conn.createStatement();

            Rlog.debug("fieldlib", "in DAO getFieldsFromSearch()###" + str1
                    + str2 + str3 + str4 + str5 + str6);
            ResultSet rs = null;
            try {
                stmt.execute(str1 + str2 + str3 + str4 + str5 + str6);
                rs = stmt.getResultSet();
            } catch (Exception ex) {
                Rlog.fatal("fieldlib",
                        "FieldLib. getFieldsForCopy IN THE EXCECUTE $$$$$###"
                                + ex);
            }

            if (rs != null) {

                while (rs.next()) {

                    setFieldLibId(new Integer(rs.getInt("PK_FIELD")));

                    setCatLibId(new Integer(rs.getInt("FK_CATLIB")));

                    setLibFlag(rs.getString("FLD_LIBFLAG"));

                    setFldName(rs.getString("FLD_NAME"));

                    setFldDesc(rs.getString("FLD_DESC"));

                    setFldUniqId(rs.getString("FLD_UNIQUEID"));

                    setFldSysID(rs.getString("FLD_SYSTEMID"));

                    setFldKeyword(rs.getString("FLD_KEYWORD"));

                    setFldType(rs.getString("FLD_TYPE"));

                    setFldDataType(rs.getString("FLD_DATATYPE"));

                    setFldInstructions(rs.getString("FLD_INSTRUCTIONS"));

                    setFldFormat(rs.getString("FLD_FORMAT"));

                    setFldRepeatFlag(new Integer(rs.getInt("FLD_REPEATFLAG")));

                    setFldCatName(rs.getString("CATLIB_NAME"));

                    rows++;

                }
            }

            setRows(rows);

        } catch (Exception ex) {
            Rlog.fatal("fieldlib",
                    "FieldLib. getFieldsForCopy EXCEPTION IN FETCHING FROM ER_FIELDLIB TABLE"
                            + ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

    // ////////////////////

    /**
     * Gets the fieldNamesForSection attribute of the FieldLibDao object
     *
     * @param formId
     *            Description of the Parameter
     * @param sectionId
     *            Description of the Parameter
     * @param fldType
     *            Description of the Parameter
     */
    public void getFieldNamesForSection(int formId, int sectionId,
            String fldType) {

        int count = 0;
        PreparedStatement preparedstatement = null;
        Connection connection = null;
        String sqlStr = "";

        try {
            if (StringUtil.isEmpty(fldType)) {
                fldType = "A";
            }

            connection = getConnection();

            if (fldType.equals("A")) {
                // get all fields

                sqlStr = "select  fd.fld_name || ' [' || fld_uniqueid || '] ' fld_name ,fd.pk_field, fld_systemid  from er_fldlib fd,er_formlib fl,er_formsec fs,er_formfld ff  where fl.pk_formlib=fs.fk_formlib  and ff.fk_formsec = ? and  fs.pk_formsec=ff.fk_formsec  and ff.fk_field=fd.pk_field  and fd.record_type <> 'D'  and NVL(fd.fld_repeatflag,0)=0  and fl.pk_formlib= ? and fd.fld_isvisible <> 1 order by ff.formfld_seq";
            } else {
                // get all fields available for lookup

                sqlStr = "select  fd.fld_name || ' [' || fld_uniqueid || '] ' fld_name ,fd.pk_field, fld_systemid  from er_fldlib fd,er_formlib fl,er_formsec fs,er_formfld ff  where fl.pk_formlib=fs.fk_formlib  and ff.fk_formsec = ? and  fs.pk_formsec=ff.fk_formsec  and ff.fk_field=fd.pk_field  and fd.record_type <> 'D'  and NVL(fd.fld_repeatflag,0)=0  and fl.pk_formlib= ?  and  fd.fld_type not in ('C','H') and fd.fld_datatype not in ('ML','MR','MC') and fd.fld_isvisible <> 1 order by ff.formfld_seq";
            }

            preparedstatement = connection.prepareStatement(sqlStr);
            preparedstatement.setInt(1, sectionId);
            preparedstatement.setInt(2, formId);

            Rlog.debug("fieldlib", " - sqlStr" + sqlStr);

            ResultSet resultset = preparedstatement.executeQuery();

            while (resultset.next()) {
                setFldName(resultset.getString("fld_name"));
                setFieldLibId(new Integer(resultset.getInt("pk_field")));
                setFldSysID(resultset.getString("fld_systemid"));
                count++;
            }

            setCRows(count);
        } catch (SQLException sqlexception) {
            Rlog.fatal("fieldlib",
                    "FieldLibDao.getFieldNamesForSection. EXCEPTION IN FETCHING data"
                            + sqlexception);
        } finally {
            try {
                if (preparedstatement != null) {
                    preparedstatement.close();
                }
            } catch (Exception exception1) {
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception exception2) {
            }
        }
    }

    /**
     * Method to update all the field sequences of all fields in a section of a
     * form(when a duplicate seq is entered) by incrementing the formfld_seq of
     * the duplicate and all other field sequences in a section of a form whose
     * seq are greater than the duplicate seq
     *
     * @param formSecId -
     *            field's section
     * @param formFldId -
     *            form field
     * @param fldseq -
     *            sequence to update.
     * @return 0 if successful
     */
    /*
     * History********************************* Modified by : Anu Modified
     * On:06/14/2004 Comment: Added a new parameter formFldId for bug #1544
     * END****************************************
     */
    public int updateFieldSeq(int formSecId, int formFldId, int fldseq, int usr) {
        int retValue = 0;
        Rlog.debug("fieldlib", "In updateFieldSeq  formSecId" + formSecId);
        Rlog.debug("fieldlib", "In updateFieldSeq  formFldId" + formFldId);
        Rlog.debug("fieldlib", "In updateFieldSeq fldseq" + fldseq);

        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            Rlog.debug("fieldlib", "In updateFieldSeq  ");
            //KM-#3634
            cstmt = conn
                    .prepareCall("{call PKG_SECFLD.SP_UPDATE_FIELD_SEQ (?,?,?,?,?)}");
            cstmt.setInt(1, formSecId);
            cstmt.setInt(2, formFldId);
            cstmt.setInt(3, fldseq);
            cstmt.setInt(4, usr);
            cstmt.registerOutParameter(5, java.sql.Types.INTEGER);

            cstmt.execute();
            retValue = cstmt.getInt(5);

            return retValue;
        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "EXCEPTION in updateFieldSeq, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }

    }

    /**
     * Method to retreive edit and multiple choice section fields of a form
     *
     * @param formId -
     *            form id
     */

    public void getEditMultipleFields(int formId) {

        Rlog.debug("fieldlib", "inside getFieldsInformation");
        int ret = 1;
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        try {

            conn = getConnection();
            Rlog.debug("fieldlib", "In getFieldsInformation () ");
            sql = " select fld_name, pk_field " + " from erv_formflds "
                    + " where pk_formlib= ? "
                    + " and nvl(FLD_DATATYPE,'N') <> 'ML' "
                    + " and nvl(FLD_TYPE,'N') <> 'C' "
                    + " and nvl(FLD_TYPE,'N') <> 'F' "
                    + " and nvl(FLD_TYPE,'N') <> 'H' "
                    + " and nvl(FLD_TYPE,'N') <> 'S' ";

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, formId);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {

                setFldName(rs.getString("FLD_NAME"));
                setFieldLibId(new Integer(rs.getInt("PK_FIELD")));

                rows++;
            }
            setCRows(rows);
            Rlog.debug("fieldlib", "number of rows" + rows);

        } catch (SQLException ex) {
            Rlog.fatal("fieldlib",
                    "getFieldsInformation EXCEPTION IN FETCHING FROM table"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }
    
    public void getFieldLibDetails(int fieldId) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = " select FLD_REPEATFLAG " + " from ER_FLDLIB "
                + " where PK_FIELD = ? ";
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, fieldId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setFldRepeatFlag(rs.getInt("FLD_REPEATFLAG"));
            }
        } catch (SQLException ex) {
            Rlog.fatal("fieldlib", "getFieldLibDetails EXCEPTION IN FETCHING FROM table"+ex);
        } finally {
            try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
            try { if (conn != null) { conn.close(); } } catch (Exception e) {}
        }
    }

}
