package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.velos.eres.service.util.Rlog;

public class StudyINDIDEDAO extends CommonDAO implements java.io.Serializable{

	/**
	 * Generated  serialVersionUID
	 */
	private static final long serialVersionUID = 5393696226070471068L;
	
	
	private ArrayList<Integer> idIndIdeList;
    private ArrayList<Integer> typeIndIdeList;
    private ArrayList<String> numberIndIdeList;
    private ArrayList<Integer> grantorList;
    private ArrayList<Integer> holderList;
    private ArrayList<Integer> programCodeList;
    private ArrayList<Integer> expandedAccessList;
    private ArrayList<Integer> accessTypeList;
    private ArrayList<Integer> exemptIndIdeList;
    
    private static final String codeLstDataSql = "select CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC from "+
    		" ER_CODELST where PK_CODELST = ? ";
    
    public static final String CODELST_TYPE = "CODELST_TYPE", CODELST_SUBTYP = "CODELST_SUBTYP", CODELST_DESC = "CODELST_DESC";
    
   
    public StudyINDIDEDAO() {
    	
    	idIndIdeList = new ArrayList<Integer>();
        typeIndIdeList = new ArrayList<Integer>();
        numberIndIdeList = new ArrayList<String>();
        grantorList = new ArrayList<Integer>();
        holderList = new ArrayList<Integer>();
        programCodeList = new ArrayList<Integer>();
        expandedAccessList = new ArrayList<Integer>();
        accessTypeList = new ArrayList<Integer>();
        exemptIndIdeList = new ArrayList<Integer>();

   }
	
    public ArrayList<Integer> getIdIndIdeList() {
		return idIndIdeList;
	}

	public void setIdIndIdeList(ArrayList<Integer> idIndIdeList) {
		this.idIndIdeList = idIndIdeList;
	}

	public void setIdIndIdeList(Integer idIndIdeList) {
		this.idIndIdeList.add(idIndIdeList);
	}





	public ArrayList<Integer> getTypeIndIdeList() {
		return typeIndIdeList;
	}

	public void setTypeIndIdeList(ArrayList<Integer> typeIndIdeList) {
		this.typeIndIdeList = typeIndIdeList;
	}

	public void setTypeIndIdeList(Integer typeIndIdeList) {
		this.typeIndIdeList.add(typeIndIdeList);
	}





	public ArrayList<String> getNumberIndIdeList() {
		return numberIndIdeList;
	}

	public void setNumberIndIdeList(ArrayList<String> numberIndIdeList) {
		this.numberIndIdeList = numberIndIdeList;
	}

	public void setNumberIndIdeList(String numberIndIdeList) {
		this.numberIndIdeList.add(numberIndIdeList);
	}






	public ArrayList<Integer> getGrantorList() {
		return grantorList;
	}
	
	public void setGrantorList(ArrayList<Integer> grantorList) {
		this.grantorList = grantorList;
	}

	public void setGrantorList(Integer grantorList) {
		this.grantorList.add(grantorList);
	}





	public ArrayList<Integer> getHolderList() {
		return holderList;
	}

	public void setHolderList(ArrayList<Integer> holderList) {
		this.holderList = holderList;
	}
	
	public void setHolderList(Integer holderList) {
		this.holderList.add(holderList);
	}







	public ArrayList<Integer> getProgramCodeList() {
		return programCodeList;
	}
	public void setProgramCodeList(ArrayList<Integer> programCodeList) {
		this.programCodeList = programCodeList;
	}
	public void setProgramCodeList(Integer programCodeList) {
		this.programCodeList.add(programCodeList);
	}







	public ArrayList<Integer> getExpandedAccessList() {
		return expandedAccessList;
	}
	public void setExpandedAccessList(ArrayList<Integer> expandedAccessList) {
		this.expandedAccessList = expandedAccessList;
	}
	public void setExpandedAccessList(Integer expandedAccessList) {
		this.expandedAccessList.add(expandedAccessList);
	}






	public ArrayList<Integer> getAccessTypeList() {
		return accessTypeList;
	}
	public void setAccessTypeList(ArrayList<Integer> accessTypeList) {
		this.accessTypeList = accessTypeList;
	}
	public void setAccessTypeList(Integer accessTypeList) {
		this.accessTypeList.add(accessTypeList);
	}




	public ArrayList<Integer> getExemptIndIdeList() {
		return exemptIndIdeList;
	}
	public void setExemptIndIdeList(ArrayList<Integer> exemptIndIdeList) {
		this.exemptIndIdeList = exemptIndIdeList;
	}
	public void setExemptIndIdeList(Integer exemptIndIdeList) {
		this.exemptIndIdeList.add(exemptIndIdeList);
	}

	
	
	
	public int getStudyIndIdeInfo(int studyId) {
		
		int rows=0;
    	PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement("SELECT PK_STUDY_INDIIDE, INDIDE_TYPE,INDIDE_NUMBER, FK_CODELST_INDIDE_GRANTOR"
            		+ ", FK_CODELST_INDIDE_HOLDER, FK_CODELST_PROGRAM_CODE, INDIDE_EXPAND_ACCESS, FK_CODELST_ACCESS_TYPE"
            		+ ", INDIDE_EXEMPT FROM ER_STUDY_INDIDE WHERE FK_STUDY = ?");
                      
            
            pstmt.setInt(1, studyId);
          
            ResultSet rs = pstmt.executeQuery();
            
            while (rs.next()) {
            	setIdIndIdeList(rs.getInt("PK_STUDY_INDIIDE"));
            	setTypeIndIdeList(rs.getInt("INDIDE_TYPE"));
            	setNumberIndIdeList(rs.getString("INDIDE_NUMBER"));
            	setGrantorList(rs.getInt("FK_CODELST_INDIDE_GRANTOR"));
            	setHolderList(rs.getInt("FK_CODELST_INDIDE_HOLDER"));
            	setProgramCodeList(rs.getInt("FK_CODELST_PROGRAM_CODE"));
            	setExpandedAccessList(rs.getInt("INDIDE_EXPAND_ACCESS"));
            	setAccessTypeList(rs.getInt("FK_CODELST_ACCESS_TYPE"));
            	setExemptIndIdeList(rs.getInt("INDIDE_EXEMPT"));
            	rows++;
            }
                        
       } catch (SQLException ex) {
            Rlog.fatal("Study",
                    "StudyINDIDEDAO.getStudyIndIdeInfo EXCEPTION IN FETCHING FROM ER_STUDY_INDIDE table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return rows;
    }

	public HashMap<String, String> getCodelstData(int pkCode) {
		HashMap<String, String> returnMap = new HashMap<String, String>();
    	PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(codeLstDataSql);
            pstmt.setInt(1, pkCode);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	returnMap.put(CODELST_TYPE, rs.getString(CODELST_TYPE));
            	returnMap.put(CODELST_SUBTYP, rs.getString(CODELST_SUBTYP));
            	returnMap.put(CODELST_DESC, rs.getString(CODELST_DESC));
            }
        } catch (SQLException ex) {
        	Rlog.fatal("Study", "Error in StudyINDIDEDAO.getCodelstData for pkCode="+pkCode+": "+ex);
        } finally {
            try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
            try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
		return returnMap;
	}
	
	public ArrayList<String> getIndIdeData(int studyPk) {
        
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sbSQL = new StringBuffer();
        JSONObject indIdeJSON = null; 
        ArrayList<String> arList = new ArrayList<String>();
       
        try {
            conn = getConnection();
            sbSQL.append("SELECT PK_STUDY_INDIIDE, INDIDE_TYPE,INDIDE_NUMBER, FK_CODELST_INDIDE_GRANTOR"
            		+ ", FK_CODELST_INDIDE_HOLDER, FK_CODELST_PROGRAM_CODE, INDIDE_EXPAND_ACCESS, FK_CODELST_ACCESS_TYPE"
            		+ ", INDIDE_EXEMPT FROM ER_STUDY_INDIDE WHERE FK_STUDY = ?");                   
            pstmt = conn.prepareStatement(sbSQL.toString());
            Rlog.debug("studyId", "SQL- " + sbSQL);
            pstmt.setInt(1, studyPk);          
           
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {    
            	CodeDao cDao= new CodeDao();	
            	String INDIDEGrantor="";
            	String INDIDEHolder="";
            	String INDIDEAccess="";
            	String ProgramCode="";
            	if( rs.getInt("FK_CODELST_INDIDE_GRANTOR") == 0) {
            		INDIDEGrantor = "";
    			} else {				
    				INDIDEGrantor= cDao.getCodeDescription(((Integer)(rs.getInt("FK_CODELST_INDIDE_GRANTOR"))).intValue());
    			}			
    			if( rs.getInt("FK_CODELST_INDIDE_HOLDER") == 0 ) {				
    				INDIDEHolder = "";
    			} else {
    				INDIDEHolder = cDao.getCodeDescription(((Integer)(rs.getInt("FK_CODELST_INDIDE_HOLDER"))).intValue());
    			}			
    			if( rs.getInt("FK_CODELST_ACCESS_TYPE") == 0 ) {
    				INDIDEAccess = "";
    			} else {				
    				INDIDEAccess = cDao.getCodeDescription(((Integer)(rs.getInt("FK_CODELST_ACCESS_TYPE"))).intValue());
    			}
    			if( rs.getInt("FK_CODELST_PROGRAM_CODE") == 0 ) {
    				ProgramCode = "";	
    			} else {				
    				ProgramCode= cDao.getCodeDescription(((Integer)(rs.getInt("FK_CODELST_PROGRAM_CODE"))).intValue());	
    			}
    			indIdeJSON = new JSONObject(); 
	            try {
	        	   indIdeJSON.put("INDIDEType", rs.getInt("INDIDE_TYPE"));
		           indIdeJSON.put("INDIDENumber", rs.getString("INDIDE_NUMBER"));	         
		           indIdeJSON.put("INDIDEGrantor", INDIDEGrantor);
		           indIdeJSON.put("INDIDEHolder", INDIDEHolder);
		           indIdeJSON.put("ProgramCode", ProgramCode);
		           indIdeJSON.put("INDIDEExpandAccess", rs.getInt("INDIDE_EXPAND_ACCESS"));	          
		           indIdeJSON.put("INDIDEAccess", INDIDEAccess);
		           indIdeJSON.put("INDIDEExempt", rs.getInt("INDIDE_EXEMPT"));
	               arList.add(indIdeJSON.toString());
	            } catch (JSONException e) {
	        	   e.printStackTrace();
	            }
            }
        } catch (SQLException ex) {
            Rlog.fatal("StudyINDIDEDAO", "EXCEPTION IN FETCHING getPredefIndData" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }        
        return arList;
    }
	
}
