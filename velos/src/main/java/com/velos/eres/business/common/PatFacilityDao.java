/*
 * Classname			PerIdDao.class
 * 
 * Version information 	1.0
 *
 * Date					09/23/05
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

/**
 * PatFacilityDao for getting Patient Facility records
 * 
 * @author Sonia Abrol
 * @version : 1.0 09/23/05 
 */

public class PatFacilityDao extends CommonDAO implements java.io.Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -4527666206330939922L;

		/**
     * the Patient Facility Primary Key
     */
    public ArrayList<String> id	;

    /**
     * the Patient Facility Id
     */
    public ArrayList<String> patientFacilityId	;

    /**
     * the patient Primary Key
     */
    public ArrayList<String> patientPK	;

    /**
     * the patient Site
     */
    public ArrayList<String> patientSite;

    /**
     * the registration date
     */
    public ArrayList<String> regDate		;

    /**
     * the id of the user who registered the patient
     */
    public ArrayList<String> registeredBy;
    
    /**
     * the person who registered the patient (and is not a user in the system)
     */
    public ArrayList<String> registeredByOther	;
    

    /**
     * a comma separated String of specialty ids that have access to the patient. 
     <br> If no specialty is selected, that means all the users of the registering facility can access the patient 
     */
    public ArrayList<String> patSpecialtylAccess;
    
    /**
     * Flag to indicate the access level of the registering site's users
     * <br> Currently we support only two levels:
     * <br> 0 : No Access
     * <br> 7 : Full Access
     */
    public ArrayList<String> patAccessFlag	;
    
    /**
     * Flag to indicate if this site is the default site for the patient
     * <br> Possible Values:
     * <br> 0 : Not Default
     * <br> 1 : Default
     */
    public ArrayList<String> isDefault	;
    
    /**
     * the patient Site Name
     */
    public ArrayList<String> patientSiteName;

       

    public PatFacilityDao() {
      id = new ArrayList<String>();
	patientFacilityId = new ArrayList <String>();
	patientPK = new ArrayList<String>();
	patientSite = new ArrayList<String>();
	regDate = new ArrayList<String>();
	registeredBy = new ArrayList<String>();
	registeredByOther = new ArrayList<String>();
	patSpecialtylAccess = new ArrayList<String>();
	patAccessFlag = new ArrayList<String>();
	isDefault = new ArrayList<String>();
	patientSiteName = new ArrayList<String>();
    }
    
    /**
	 * Returns the value of id.
	 */
	public ArrayList getId()
	{
		return id;
	}

    /**
	 * Returns the value of id.
	 */
	public String getId(int index)
	{
		String ret = ""; 
		if (id.size() > index)
		{
			ret = id.get(index) ;
		}	
		
		return ret;
	}
	
	/**
	 * Sets the value of id.
	 * @param id The value to assign id.
	 */
	public void setId(ArrayList<String> id)
	{
		this.id = id;
	}
	
	/**
	 * Sets the value of id.
	 * @param id The value to add to id.
	 */
	public void setId(String idS)
	{
		this.id.add(idS);
	}


	/**
	 * Returns the value of patientFacilityId.
	 */
	public ArrayList getPatientFacilityId()
	{
		return patientFacilityId;
	}
	
	/**
	 * Returns the value of patientFacilityId.
	 */
	public String getPatientFacilityId(int index)
	{
		String ret = ""; 
		if (patientFacilityId.size() > index)
		{
			ret = patientFacilityId.get(index) ;
		}	
		
		return ret;
	}
	

	/**
	 * Sets the value of patientFacilityId.
	 * @param patientFacilityId The value to assign patientFacilityId.
	 */
	public void setPatientFacilityId(ArrayList<String> patientFacilityId)
	{
		this.patientFacilityId = patientFacilityId ;
	}
	
	/**
	 * Sets the value of patientFacilityId.
	 * @param patientFacilityId The value to add to patientFacilityId.
	 */
	public void setPatientFacilityId(String patientFacilityId)
	{
		this.patientFacilityId.add(patientFacilityId);
	}

	/**
	 * Returns the value of patientPK.
	 */
	public ArrayList getPatientPK()
	{
		return patientPK;
	}

	/**
	 * Returns the value of patientPK.
	 */
	public String getPatientPK(int index)
	{
		String ret = ""; 
		if (patientPK.size() > index)
		{
			ret = patientPK.get(index) ;
		}	
		
		return ret;
	}
	
	
	/**
	 * Sets the value of patientPK.
	 * @param patientPK The value to assign patientPK.
	 */
	public void setPatientPK(ArrayList<String> patientPK)
	{
		this.patientPK = patientPK;
	}
	
	/**
	 * Sets the value of patientPK.
	 * @param patientPK The value to assign patientPK.
	 */
	public void setPatientPK(String patientPK)
	{
		this.patientPK.add(patientPK);
	}

	/**
	 * Returns the value of patientSite.
	 */
	public ArrayList getPatientSite()
	{
		return patientSite;
	}
	/**
	 * Returns the value of patientSite.
	 */
	public String getPatientSite(int index)
	{
		String ret = ""; 
		if (patientSite.size() > index)
		{
			ret = patientSite.get(index) ;
		}	
		
		return ret;
	}
	

	/**
	 * Sets the value of patientSite.
	 * @param patientSite The value to assign patientSite.
	 */
	public void setPatientSite(ArrayList<String> patientSite)
	{
		this.patientSite = patientSite;
	}
	
	/**
	 * Sets the value of patientSite.
	 * @param patientSite The value to add to patientSite.
	 */
	public void setPatientSite(String patientSite)
	{
		this.patientSite.add(patientSite);
	}
	
	/**
	 * Returns the value of regDate.
	 */
	public ArrayList getRegDate()
	{
		return regDate;
	}

	/**
	 * Returns the value of regDate.
	 */
	public String getRegDate(int index)
	{
		String ret = ""; 
		if (regDate.size() > index)
		{
			ret = regDate.get(index) ;
		}	
		
		return ret;
	}
	
	/**
	 * Sets the value of regDate.
	 * @param regDate The value to assign regDate.
	 */
	public void setRegDate(ArrayList<String> regDate)
	{
		this.regDate = regDate;
	}
	
	/**
	 * Sets the value of regDate.
	 * @param regDate The value to add to regDate.
	 */
	public void setRegDate(String regDate)
	{
		this.regDate.add(regDate);
	}
	

/**
	 * Returns the value of registeredBy.
	 */
	public ArrayList registeredBy()
	{
		return registeredBy;
	}
	
	/**
	 * Returns the value of patientFacilityId.
	 */
	public String getRegisteredBy(int index)
	{
		String ret = ""; 
		if (registeredBy.size() > index)
		{
			ret = registeredBy.get(index) ;
		}	
		
		return ret;
	}
	

	/**
	 * Sets the value of registeredBy.
	 * @param registeredBy The value to assign registeredBy.
	 */
	public void setRegisteredBy(ArrayList<String> registeredBy)
	{
		this.registeredBy = registeredBy;
	}
	
	
	/**
	 * Sets the value of registeredBy.
	 * @param registeredBy The value to add to registeredBy.
	 */
	public void setRegisteredBy(String registeredBy)
	{
		this.registeredBy.add(registeredBy);
	}
	

/**
	 * Returns the value of registeredByOther.
	 */
	public ArrayList getRegisteredByOther()
	{
		return registeredByOther;
	}
	
	/**
	 * Returns the value of registeredByOther.
	 */
	public String getRegisteredByOther(int index)
	{
		String ret = ""; 
		if (registeredByOther.size() > index)
		{
			ret = registeredByOther.get(index) ;
		}	
		
		return ret;
	}
	

	/**
	 * Sets the value of registeredByOther.
	 * @param registeredByOther The value to assign registeredByOther.
	 */
	public void setRegisteredByOther(ArrayList<String> registeredByOther)
	{
		this.registeredByOther = registeredByOther;
	}
	
	
	/**
	 * Sets the value of registeredByOther.
	 * @param registeredByOther The value to assign registeredByOther.
	 */
	public void setRegisteredByOther(String registeredByOther)
	{
		this.registeredByOther.add(registeredByOther);
	}
	

	/**
	 * Returns the value of patSpecialtylAccess.
	 */
	public ArrayList getPatSpecialtylAccess()
	{
		return patSpecialtylAccess;
	}

	
	/**
	 * Returns the value of patSpecialtylAccess.
	 */
	public String getPatSpecialtylAccess(int index)
	{
		String ret = ""; 
		if (patSpecialtylAccess.size() > index)
		{
			ret = patSpecialtylAccess.get(index) ;
		}	
		
		return ret;
	}
	
	
	/**
	 * Sets the value of patSpecialtylAccess.
	 * @param patSpecialtylAccess The value to assign patSpecialtylAccess.
	 */
	public void setPatSpecialtylAccess(ArrayList<String> patSpecialtylAccess)
	{
		this.patSpecialtylAccess = patSpecialtylAccess;
	}
	
	/**
	 * Sets the value of patSpecialtylAccess.
	 * @param patSpecialtylAccess The value to add to patSpecialtylAccess.
	 */
	public void setPatSpecialtylAccess(String patSpecialtylAccess)
	{
		this.patSpecialtylAccess.add(patSpecialtylAccess);
	}

	/**
	 * Returns the value of patAccessFlag.
	 */
	public ArrayList getPatAccessFlag()
	{
		return patAccessFlag;
	}

	/**
	 * Returns the value of patAccessFlag.
	 */
	public String getPatAccessFlag(int index)
	{
		String ret = ""; 
		if (patAccessFlag.size() > index)
		{
			ret = patAccessFlag.get(index) ;
		}	
		
		return ret;
	}
	
	
	/**
	 * Sets the value of patAccessFlag.
	 * @param patAccessFlag The value to assign patAccessFlag.
	 */
	public void setPatAccessFlag(ArrayList<String> patAccessFlag)
	{
		this.patAccessFlag = patAccessFlag;
	}
	
	/**
	 * Sets the value of patAccessFlag.
	 * @param patAccessFlag The value to assign patAccessFlag.
	 */
	public void setPatAccessFlag(String patAccessFlag)
	{
		this.patAccessFlag.add(patAccessFlag);
	}


	/**
	 * Returns the value of isDefault.
	 */
	public ArrayList getIsDefault()
	{
		return isDefault;
	}
	
	/**
	 * Returns the value of isDefault.
	 */
	public String getIsDefault(int index)
	{
		String ret = ""; 
		if (isDefault.size() > index)
		{
			ret = isDefault.get(index) ;
		}	
		
		return ret;
	}
	

	/**
	 * Sets the value of isDefault.
	 * @param isDefault The value to assign isDefault.
	 */
	public void setIsDefault(ArrayList <String> isDefault)
	{
		this.isDefault = isDefault;
	}

	/**
	 * Sets the value of isDefault.
	 * @param isDefault The value to add to isDefault.
	 */
	public void setIsDefault(String isDefault)
	{
		this.isDefault.add(isDefault);
	}
	
	public ArrayList<String> getPatientSiteName() {
		return patientSiteName;
	}

	/**
	 * Returns the value of siteName.
	 */
	public String getPatientSiteName(int index)
	{
		String ret = ""; 
		if (patientSiteName.size() > index)
		{
			ret = patientSiteName.get(index) ;
		}	
		
		return ret;
	}
	
	public void setPatientSiteName(ArrayList<String> patientSiteName) {
		this.patientSiteName = patientSiteName;
	}
	
	public void setPatientSiteName(String patientSiteName) {
		this.patientSiteName.add(patientSiteName);
	}

    // end of getter and setter methods

    /**
     * Gets patient facilities
     * 
     * @param perPK
     */

    public void getPatientFacilities(int perPk) {
          PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sbSQL = new StringBuffer();
        try {
            conn = getConnection();

            sbSQL.append(" Select pk_patfacility,fk_per , fk_site , pat_facilityid ,  nvl(to_char(patfacility_regdate,PKG_DATEUTIL.F_GET_DATEFORMAT),' ') patfacility_regdate ," );
            sbSQL.append(" nvl(usr_lst(patfacility_provider),' ') patfacility_provider ,  patfacility_otherprovider , nvl(Code_Lst_Names(patfacility_splaccess),' ') patfacility_splaccess , ");
            sbSQL.append(" patfacility_accessright ,  patfacility_default, site_name from er_patfacility, er_site where er_patfacility.fk_per = ? and fk_site = pk_site"); 
            
            pstmt = conn.prepareStatement(sbSQL.toString());

            pstmt.setInt(1, perPk);
            
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	Rlog.debug("PatFacility", " got rows for pk:" + rs.getString("pk_patfacility"));
                setId(rs.getString("pk_patfacility"));
                
				setPatientPK(rs.getString("fk_per"));
				
				setPatientSite(rs.getString("fk_site"));
				
				setPatientFacilityId(rs.getString("pat_facilityid"));
				
				setRegDate(rs.getString("patfacility_regdate"));
				
				setRegisteredBy(rs.getString("patfacility_provider"));
				
				setRegisteredByOther(rs.getString("patfacility_otherprovider"));
				
				setPatSpecialtylAccess(rs.getString("patfacility_splaccess"));
				
				setPatAccessFlag(rs.getString("patfacility_accessright"));
				
				setIsDefault(rs.getString("patfacility_default"));
				
				setPatientSiteName(rs.getString("site_name"));
				Rlog.debug("PatFacility", " got rows ");
                 
            }
	    	try {
		if (rs != null)
                rs.close();
		} catch (Exception e) {
		}	
            
        } catch (SQLException ex) {
            Rlog.fatal("PatFacility", " In getPatientFacilities EXCEPTION IN FETCHING rows"
                    + ex);
        } finally {
		
		
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
				
            } catch (Exception e) {
            }

        }

    }
      
    public ArrayList<String> getPatientFacilitiesSiteIds(int perPk,int userId) {
        PreparedStatement pstmt = null;
      Connection conn = null;
      StringBuffer sbSQL = new StringBuffer();
      ArrayList<String> userSites=new ArrayList<String>();
      try {
          conn = getConnection();

          sbSQL.append(" Select site_name from er_patfacility, er_site, er_usersite where er_patfacility.fk_per = ? and " );
          sbSQL.append(" er_patfacility.fk_site = pk_site and fk_user=? AND (usersite_right=5 OR usersite_right=7) and pk_site=er_usersite.fk_site "); 
          
          pstmt = conn.prepareStatement(sbSQL.toString());

          pstmt.setInt(1, perPk);
          pstmt.setInt(2, userId);
          
          ResultSet rs = pstmt.executeQuery();

          while (rs.next()) {
        	  userSites.add(rs.getString("site_name"));      
          }
	    	try {
		if (rs != null)
              rs.close();
		} catch (Exception e) {
		}	
          
      } catch (SQLException ex) {
          Rlog.fatal("PatFacility", " In getPatientFacilities EXCEPTION IN FETCHING rows"
                  + ex);
      } finally {
		
		
          try {
              if (pstmt != null)
                  pstmt.close();
          } catch (Exception e) {
          }
          try {
              if (conn != null)
                  conn.close();
				
          } catch (Exception e) {
          }
      }
      return userSites;
  }

    public void getPatientFacilitiesSiteIds(int perPk) {
      PreparedStatement pstmt = null;
      Connection conn = null;
      StringBuffer sbSQL = new StringBuffer();
      try {
          conn = getConnection();
          //Modified by Gopu for May-June 2006 Enhancement on 06-May-2006 column 'pat_facilityid' added
          sbSQL.append(" Select pk_patfacility,pat_facilityid,fk_site,site_name from er_patfacility ,er_site where er_patfacility.fk_per = ? and pk_site = fk_site  "); 
          
          pstmt = conn.prepareStatement(sbSQL.toString());
          pstmt.setInt(1, perPk);
   
          ResultSet rs = pstmt.executeQuery();
          while (rs.next()) {
          	Rlog.debug("PatFacility", " got rows for pk:" + rs.getString("pk_patfacility"));
              setId(rs.getString("pk_patfacility"));
              			
				setPatientSite(rs.getString("fk_site"));
				setPatientSiteName(rs.getString("site_name"));
				setPatientFacilityId(rs.getString("pat_facilityid"));
				Rlog.debug("PatFacility", " got rows ");
               
          }
	    	try {
		if (rs != null)
              rs.close();
		} catch (Exception e) {
		}	
          
      } catch (SQLException ex) {
          Rlog.fatal("PatFacility", " In getPatientFacilitiesSiteIds EXCEPTION IN FETCHING rows"
                  + ex);
      } finally {
		
		
          try {
              if (pstmt != null)
                  pstmt.close();
          } catch (Exception e) {
          }
          try {
              if (conn != null)
                  conn.close();
				
          } catch (Exception e) {
          }

      }

  }

    /////////
    /** Returns the maximum right the user has on any of patient facilities. User's user site access rights are also
     * checked for all the patient facilities*/
    public int getUserPatientFacilityRight(int user , int perPk) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sbSQL = new StringBuffer();
        int right = 0;
        try {
            conn = getConnection();
            sbSQL.append(" Select nvl(max(USERSITE_RIGHT),0) USERSITE_RIGHT from er_usersite usr, er_patfacility fac ");
            sbSQL.append(" where fac.fk_per = ? and fac.patfacility_accessright > 0 and fac.fk_site = usr.fk_site  and usr.fk_user = ?"); 
            
            pstmt = conn.prepareStatement(sbSQL.toString());
            pstmt.setInt(1, perPk);
            pstmt.setInt(2, user);
     
            ResultSet rs = pstmt.executeQuery();
            
            while (rs.next()) {
            	           	
            	right = rs.getInt("USERSITE_RIGHT");
            	Rlog.debug("PatFacility", " getUserPatientFacilityRight got rows : right :" + right );
                 
            }
                     
  	    	try {
  		if (rs != null)
                rs.close();
  		} catch (Exception e) {
  		}
  		
  		return right;
  		
        } catch (SQLException ex) {
            Rlog.fatal("PatFacility", " In getUserPatientFacilityRight EXCEPTION IN FETCHING rows"
                    + ex);
            return right;
        } finally {
  		
  		
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
  				
            } catch (Exception e) {
            }

        }

    }

    /////////
    /** Returns the access right flag for user's specialty. Specialty filters for all patient facilities are checked.
      
     * @param splId Speciality Id
     * */
    
    public int getUserFacilitySpecialtyAccessRight(int splId, int perPk) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sbSQL = new StringBuffer();
        int right = 0;
        String strSpl = "";
        try {
            conn = getConnection();
            strSpl = "," + splId + ",";
            
            sbSQL.append(" Select count(pk_patfacility) ct from er_patfacility fac ");
            sbSQL.append(" where fac.fk_per = ? and fac.patfacility_accessright > 0 and (patfacility_splaccess is null or (',' || patfacility_splaccess || ',' like '%" + strSpl +  "%' )) "); 
            
            Rlog.debug("PatFacility", " getUserFacilitySpecialtyAccessRight sbSQL: " + sbSQL.toString());
            
            
            
            pstmt = conn.prepareStatement(sbSQL.toString());
            pstmt.setInt(1, perPk);
            //pstmt.setString(2, strSpl);
     
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("PatFacility", " getUserFacilitySpecialtyAccessRight executed: " );
            while (rs.next()) {
            	           	
            	right = rs.getInt("ct");
            	Rlog.debug("PatFacility", " getUserFacilitySpecialtyAccessRight got rows : right :" + right );
                 
            }
                     
  	    	try {
  		if (rs != null)
                rs.close();
  		} catch (Exception e) {
  		}
  		
  		return right;
  		
        } catch (SQLException ex) {
            Rlog.fatal("PatFacility", " In getUserFacilitySpecialtyAccessRight EXCEPTION IN FETCHING rows"
                    + ex);
            return right;
        } finally {
  		
  		
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
  				
            } catch (Exception e) {
            }

        }

    }
    
    // end of class
}
