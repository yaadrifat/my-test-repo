/*
 * Classname			FldRespBean
 * 
 * Version information : 1.0
 *
 * Date					07/02/2003
 * 
 * Copyright notice :   Velos Inc
 */

package com.velos.eres.business.fldResp.impl;

/* IMPORT STATEMENTS */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* END OF IMPORT STATEMENTS */

/**
 * The field response CMP entity bean.<br>
 * <br>
 * 
 * @author Anu Khanna
 * 
 * @version 1.0, 07/02/2003
 * 
 * @ejbHome FldRespHome
 * 
 * @ejbRemote FldRespRObj
 */

@Entity
@Table(name = "er_fldresp")
public class FldRespBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3256718502822819384L;

    /*
     * the field response Id
     */
    public int fldRespId;

    /*
     * the field
     */
    public Integer field;

    /*
     * the field Sequence
     */
    public Integer seq;

    /*
     * the display value
     */
    public String dispVal;

    /*
     * the data value
     */
    public String dataVal;

    /*
     * the score
     */
    public Integer score;

    /*
     * the field category default
     */
    public Integer fldRespDefault;

    /*
     * the creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer lastModifiedBy;

    /*
     * the IP address of the creator
     */
    public String ipAdd;

    /*
     * Record Type
     */

    public String recordType;

    // GETTER SETTER METHODS
    public FldRespBean() {

    }

    public FldRespBean(int fldRespId, String field, String seq, String dispVal,
            String dataVal, String score, String fldRespDefault,
            String creator, String lastModifiedBy, String ipAdd,
            String recordType) {
        super();
        // TODO Auto-generated constructor stub
        setFldRespId(fldRespId);
        setField(field);
        setSequence(seq);
        setDispVal(dispVal);
        setDataVal(dataVal);
        setScore(score);
        setFldRespDefault(fldRespDefault);
        setCreator(creator);
        setLastModifiedBy(lastModifiedBy);
        setIPAdd(ipAdd);
        setRecordType(recordType);
    }

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "seq_er_fldresp", allocationSize=1)
    @Column(name = "PK_FLDRESP")
    public int getFldRespId() {
        return this.fldRespId;
    }

    public void setFldRespId(int id) {
        this.fldRespId = id;
    }

    @Column(name = "FK_FIELD")
    public String getField() {
        return StringUtil.integerToString(this.field);

    }

    public void setField(String field) {
        if (field != null && field.trim().length() > 0)
            this.field = Integer.valueOf(field);

    }

    @Column(name = "FLDRESP_SEQ")
    public String getSequence() {
        return StringUtil.integerToString(this.seq);

    }

    public void setSequence(String seq) {
        if (seq != null && seq.trim().length() > 0)
            this.seq = Integer.valueOf(seq.trim());

    }

    @Column(name = "FLDRESP_DATAVAL")
    public String getDataVal() {
        return this.dataVal;
    }

    public void setDataVal(String dataVal) {

        this.dataVal = dataVal;

    }

    @Column(name = "FLDRESP_DISPVAL")
    public String getDispVal() {
        return this.dispVal;
    }

    public void setDispVal(String dispVal) {

        this.dispVal = dispVal;

    }

    @Column(name = "FLDRESP_SCORE")
    public String getScore() {
        return StringUtil.integerToString(this.score);
    }

    public void setScore(String score) {
        if (score != null && score.trim().length() > 0)
            this.score = Integer.valueOf(score.trim());
    }

    @Column(name = "FLDRESP_ISDEFAULT")
    public String getFldRespDefault() {
        return StringUtil.integerToString(this.fldRespDefault);
    }

    public void setFldRespDefault(String fldRespDefault) {
        if (fldRespDefault != null && fldRespDefault.trim().length() > 0)
            this.fldRespDefault = Integer.valueOf(fldRespDefault.trim());
    }

    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);

    }

    public void setCreator(String creator) {
        if (creator != null)
            this.creator = Integer.valueOf(creator);

    }

    @Column(name = "LAST_MODIFIED_BY")
    public String getLastModifiedBy() {
        return StringUtil.integerToString(this.lastModifiedBy);
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        if (lastModifiedBy != null)
            this.lastModifiedBy = Integer.valueOf(lastModifiedBy);
    }

    @Column(name = "IP_ADD")
    public String getIPAdd() {
        return this.ipAdd;
    }

    public void setIPAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Column(name = "RECORD_TYPE")
    public String getRecordType() {
        return this.recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Retrieves the details of a field response in FldRespStateKeeper Object
     * 
     * @return FldRespStateKeeper consisting of the field Response details
     * 
     * @see FldRespStateKeeper
     */
    /*
     * public FldRespStateKeeper getFldRespStateKeeper() { Rlog.debug("fldresp",
     * "FldRespBean.getFldRespStateKeeper getFldRespId() " + getFldRespId());
     * return (new FldRespStateKeeper(getFldRespId(), getField(), getSequence(),
     * getDispVal(), getDataVal(), getScore(), getFldRespDefault(),
     * getCreator(), getLastModifiedBy(), getIPAdd(), getRecordType())); }
     */

    /**
     * Sets up a state keeper containing details of field response
     */
    /*
     * public void setFldRespStateKeeper(FldRespStateKeeper frsk) { GenerateId
     * generateId = null; try { Connection conn = null; conn = getConnection();
     * Rlog.debug("fldresp", "FldRespBean.setFldRespStateKeeper() Connection :" +
     * conn); this.fldRespId = generateId.getId("SEQ_ER_FLDRESP", conn);
     * conn.close();
     * 
     * setField(frsk.getField()); setSequence(frsk.getSequence());
     * setDispVal(frsk.getDispVal()); setDataVal(frsk.getDataVal());
     * setScore(frsk.getScore()); setFldRespDefault(frsk.getFldRespDefault());
     * setCreator(frsk.getCreator());
     * setLastModifiedBy(frsk.getLastModifiedBy()); setIPAdd(frsk.getIPAdd());
     * setRecordType(frsk.getRecordType()); } catch (Exception e) {
     * Rlog.fatal("fldresp", "Error in setFldRespStateKeeper() in FldRespBean " +
     * e); } }
     */

    /**
     * Saves the field response details in the FldRespStateKeeper to the
     * database and returns 0 if the update is successful and -2 if it fails
     * 
     * @param frsk
     *            field response State Keeper with the updated field response
     *            Information
     * 
     * @return 0 - if update is successful <br>
     *         -2 - if update fails
     * 
     * @see FldRespStateKeeper
     * 
     */
    public int updateFldResp(FldRespBean frsk) {
        try {
            ;
            setField(frsk.getField());
            setSequence(frsk.getSequence());
            setDispVal(frsk.getDispVal());
            setDataVal(frsk.getDataVal());
            setScore(frsk.getScore());
            setFldRespDefault(frsk.getFldRespDefault());
            setCreator(frsk.getCreator());
            setLastModifiedBy(frsk.getLastModifiedBy());
            setIPAdd(frsk.getIPAdd());
            setRecordType(frsk.getRecordType());
            Rlog.debug("fldresp", "FldRespBean.updateFldResp");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("fldresp", " error in FldRespBean.updateFldResp" + e);
            return -2;
        }
    }

}// end of class
