/**
 * 
 */
package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.EnvUtil;

/**
 * @author dylan
 *
 */
public class PatSearchDAO extends CommonDAO {

	public PatSearchDAO() {
		super();
	}
	
	/**
	 * Searches to see if a patient is in the repository by taking patient id, facility code, account.
	 * @param patId free text patient id field, from epat.person.person_code
	 * @param facilityCd facility code from er_patfacility.fk_site
	 * @param account account to search in
	 * @param studyId primary key of the study
	 * @return List of PatientHolder instances matching the search
	 */
	public List<PatientHolder> searchPatInDB(int userId, String patId, int facilityCd, int account, int studyId){ 
		Connection conn = EnvUtil.getConnection();
		PreparedStatement pstmt = null;
		try{ 
			int rows = 0; 
	        //Fixed 4663 on 2/1/10 DRM: search should not be case sensitive
	        StringBuilder sbSQL = new StringBuilder(); 
			//sbSQL.append("select er_patfacility.pat_facilityid, pk_per, person_fname, person_lname, person_dob");
	        // @Ankit #6019 04-19-2011
	        sbSQL.append("SELECT usersite_right, person.pk_person, person_code, er_patfacility.pat_facilityid, person_fname,  person_lname,  person_dob,  pk_patprot");
			sbSQL.append(" FROM er_usersite, er_patfacility, person LEFT OUTER JOIN (select MAX(pk_patprot) pk_patprot,fk_per,fk_study from er_patprot group by fk_per,fk_study) er_patprot1 ON (person.pk_person = er_patprot1.fk_per AND er_patprot1.fk_study = ?)");  
			sbSQL.append(" where person.pk_person = er_patfacility.fk_per");
			sbSQL.append(" AND er_patfacility.patfacility_accessright > 0 AND er_patfacility.fk_site= ?");
			sbSQL.append(" AND er_usersite.fk_user=? and er_usersite.fk_site = er_patfacility.fk_site");
			sbSQL.append(" AND person.fk_account = ? AND (upper(er_patfacility.pat_facilityid) LIKE upper(?)");
			sbSQL.append(" OR (upper(person.person_code) like upper(?)))");
			
			pstmt = conn.prepareStatement(sbSQL.toString());
			
			pstmt.setInt(1, studyId);
			pstmt.setInt(2, facilityCd);
			pstmt.setInt(3, userId);
			pstmt.setInt(4, account);
			pstmt.setString(5, patId);
			pstmt.setString(6, patId);
			
			ResultSet rs = pstmt.executeQuery();
			ArrayList<PatientHolder> pats = new ArrayList<PatientHolder>();
			while (rs.next()){
				PatientHolder pat = new PatientHolder();
				pat.setPk_per(rs.getInt("pk_person"));
				pat.setPatId(rs.getString("person_code"));
				pat.setDOB(rs.getDate("person_dob"));
				pat.setFirstName(rs.getString("person_fname"));
				pat.setLastName(rs.getString("person_lname"));
				pat.setPatFacilityId(rs.getString("pat_facilityid"));
				pat.setOnStudy((rs.getString("pk_patprot") == null) ? false : true);
				HashMap<Integer, Integer> patProtMap = new HashMap<Integer, Integer>();
				patProtMap.put(studyId, rs.getInt("pk_patprot"));
				pat.setPatProtMap(patProtMap);
				pat.setUserSiteRights(rs.getInt("usersite_right"));
				pats.add(pat);
				
			}
			
		 	try{
				if (rs != null)
		            rs.close();
		 	}
			catch (Exception e) {
			}
			return pats;
	        
	    } catch (SQLException ex) {
	        Rlog.fatal("PatFacility", " In getPatientFacilities EXCEPTION IN FETCHING rows"
	                + ex);
	    } 
	    	
	    finally {
	        try {
	            if (pstmt != null)
	                pstmt.close();
	        } catch (Exception e) {
	        }
	        //1/26/10 DRM - uncommented code to fix issue with pooled connections
	        //running out.
	        try {
	            if (conn != null)
	                conn.close();
				
	        } catch (Exception e) {
	        }

	    }
		return new ArrayList<PatientHolder>();
	}

	/**
	 * Searches to see if a patient is in the repository and on a study by taking patient id, facility code, account and study id.
	 * @param patId free text patient id field, from epat.person.person_code
	 * @param facilityCd facility code from er_patfacility.fk_site
	 * @param account account to search in
	 * @param studyId primary key of the study
	 * @return List of PatientHolder instances matching the search
	 */
	/*
	public List<PatientHolder> searchPatInStudy(String patId, int facilityCd, int account, int studyId){ 
		Connection conn = EnvUtil.getConnection();
		PreparedStatement pstmt = null;
		try{
			int rows = 0;

	        StringBuilder sbSQL = new StringBuilder();
	        //join facility, er_per, and pat prot
	        //Fixed 4663 on 2/1/10 DRM: search should not be case sensitive
			sbSQL.append("SELECT  person.pk_person, person_code, er_patfacility.pat_facilityid, person_fname,  person_lname,  person_dob,  pk_patprot");
			sbSQL.append(" from  er_patfacility, person, er_patprot");
			sbSQL.append(" WHERE  person.pk_person     = er_patfacility.fk_per AND er_patprot.fk_per = person.pk_person");//joins
			sbSQL.append(" AND er_patfacility.patfacility_accessright > 0 and er_patfacility.fk_per = person.pk_person"); //joins
			sbSQL.append(" AND (upper(person_code) LIKE upper(?) or upper(er_patfacility.pat_facilityid) like upper(?)) "); //inputs
			sbSQL.append("AND er_patfacility.fk_site = ? AND person.fk_account = ? AND er_patprot.fk_study = ?"); //inputs
	      
	         
			pstmt = conn.prepareStatement(sbSQL.toString());
			pstmt.setString(1, patId);
			pstmt.setString(2, patId);
			pstmt.setInt(3, facilityCd);
			pstmt.setInt(4, account);
			pstmt.setInt(5, studyId);
			
			ResultSet rs = pstmt.executeQuery();
			ArrayList<PatientHolder> pats = new ArrayList<PatientHolder>();
			while (rs.next()){
				PatientHolder pat = new PatientHolder();
				pat.setPk_per(rs.getInt("pk_person"));
				pat.setPatId(rs.getString("person_code"));
				pat.setDOB(rs.getDate("person_dob"));
				pat.setFirstName(rs.getString("person_fname"));
				pat.setLastName(rs.getString("person_lname"));
				pat.setPatFacilityId(rs.getString("pat_facilityid"));
				HashMap<Integer, Integer> patProtMap = new HashMap<Integer, Integer>();
				patProtMap.put(studyId, rs.getInt("pk_patprot"));
				pat.setPatProtMap(patProtMap);
				pats.add(pat);
			}
			
		 	try{
				if (rs != null)
		            rs.close();
		 	}
			catch (Exception e) {
			}
			return pats;
	        
	    } catch (SQLException ex) {
	        Rlog.fatal("PatFacility", " In getPatientFacilities EXCEPTION IN FETCHING rows"
	                + ex);
	    } 
	    	
	    finally {
	        try {
	            if (pstmt != null)
	                pstmt.close();
	        } catch (Exception e) {
	        }
	        //1/26/10 DRM - uncommented code to fix issue with pooled connections
	        //running out.
	        try {
	            if (conn != null)
	                conn.close();
				
	        } catch (Exception e) {
	        }

	    }
		return new ArrayList<PatientHolder>();
	}
	*/
	public class PatientHolder{
		
		public PatientHolder(){}
		Integer pk_per;
		//map between study id and patient's patprot id
		Map<Integer, Integer> patProtMap;
		
		public boolean isOnStudy() {
			return isOnStudy;
		}
		public void setOnStudy(boolean isOnStudy) {
			this.isOnStudy = isOnStudy;
		}
		boolean isOnStudy;
		String patId;
		public String getPatId() {
			return patId;
		}
		public void setPatId(String patId) {
			this.patId = patId;
		}
		String firstName;
		String lastName;
		Date DOB;
		String patFacilityId;
		int userSiteRights;
		
		
		public String getPatFacilityId() {
			return patFacilityId;
		}
		public void setPatFacilityId(String patFacilityId) {
			this.patFacilityId = patFacilityId;
		}
		public Integer getPk_per() {
			return pk_per;
		}
		public void setPk_per(Integer pkPer) {
			pk_per = pkPer;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public long getDOB() {
			return DOB.getTime();
		}
		public void setDOB(Date dOB) {
			DOB = dOB;
		}
		public Map<Integer, Integer> getPatProtMap() {
			return patProtMap;
		}
		public void setPatProtMap(Map<Integer, Integer> patProtMap) {
			this.patProtMap = patProtMap;
		}
		public int getUserSiteRights() {
			return userSiteRights;
		}
		public void setUserSiteRights(int userSiteRights) {
			this.userSiteRights = userSiteRights;
		}
		
	}
}
