/*
 * Classname			FieldActionDao.class
 *
 * Version information 	1.0
 *
 * Date					05/12/2004
 *
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import com.velos.eres.business.fieldAction.FieldActionStateKeeper;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.tags.VFieldAction;

import com.velos.eres.service.util.DateUtil;
import com.aithent.audittrail.reports.AuditUtils;
import com.velos.esch.business.audit.impl.AuditBean;

/**
 * FieldActionDao for Form Inter Field Actions
 *
 * @author Sonia Sahni
 * @version : 1.0 05/12/2004
 */

/*
 * Modified by Sonia, 09/30/04, added a method deleteFieldAction() for deleting
 * field actions , some javadoc comments
 */
public class FieldActionDao extends CommonDAO implements java.io.Serializable {
    private ArrayList fldActionIds;

    private ArrayList fldForms;

    private ArrayList fldActionTypes;

    private ArrayList fldActionConditions;

    private ArrayList fldActionInfos;

    private ArrayList fldIds;

    private ArrayList fldNames;

    private ArrayList fldActionNames;

    private ArrayList fldUniqIds;
    
    private ArrayList fldTypes;
    
    private ArrayList fldDataTypes;
    
    private ArrayList fldSysIds;
    
    private int cRows;

    public FieldActionDao() {
        fldActionIds = new ArrayList();
        fldForms = new ArrayList();
        fldActionTypes = new ArrayList();
        fldActionConditions = new ArrayList();
        fldActionInfos = new ArrayList();
        fldIds = new ArrayList();
        fldActionNames = new ArrayList();
        fldNames = new ArrayList();
        fldUniqIds = new ArrayList();
        fldTypes = new ArrayList();
        fldDataTypes = new ArrayList();
        fldSysIds = new ArrayList();
    }

    // Getter and Setter methods

    /**
     * Returns the value of fldActionIds.
     */
    public ArrayList getFldActionIds() {
        return fldActionIds;
    }

    /**
     * Sets the value of fldActionIds.
     *
     * @param fldActionIds
     *            The value to assign fldActionIds.
     */
    public void setFldActionIds(ArrayList fldActionIds) {
        this.fldActionIds = fldActionIds;
    }

    /**
     * Sets the value of fldActionIds.
     *
     * @param fldActionIds
     *            The value to assign fldActionIds.
     */
    public void setFldActionIds(Integer fldActionId) {
        this.fldActionIds.add(fldActionId);
    }

    /**
     * Returns the value of fldForms.
     */
    public ArrayList getFldForms() {
        return fldForms;
    }

    /**
     * Sets the value of fldForms.
     *
     * @param fldForms
     *            The value to assign fldForms.
     */
    public void setFldForms(ArrayList fldForms) {
        this.fldForms = fldForms;
    }

    /**
     * Sets the value of fldForms.
     *
     * @param fldForms
     *            The value to assign fldForms.
     */
    public void setFldForms(String fldForm) {
        this.fldForms.add(fldForm);
    }

    /**
     * Returns the value of fldActionTypes.
     */
    public ArrayList getFldActionTypes() {
        return fldActionTypes;
    }

    /**
     * Sets the value of fldActionTypes.
     *
     * @param fldActionTypes
     *            The value to assign fldActionTypes.
     */
    public void setFldActionTypes(ArrayList fldActionTypes) {
        this.fldActionTypes = fldActionTypes;
    }

    /**
     * Sets the value of fldActionTypes.
     *
     * @param fldActionTypes
     *            The value to assign fldActionTypes.
     */
    public void setFldActionTypes(String fldActionType) {
        this.fldActionTypes.add(fldActionType);
    }

    /**
     * Returns the value of fldActionConditions.
     */
    public ArrayList getFldActionConditions() {
        return fldActionConditions;
    }

    /**
     * Sets the value of fldActionConditions.
     *
     * @param fldActionConditions
     *            The value to assign fldActionConditions.
     */
    public void setFldActionConditions(ArrayList fldActionConditions) {
        this.fldActionConditions = fldActionConditions;
    }

    /**
     * Sets the value of fldActionConditions.
     *
     * @param fldActionConditions
     *            The value to assign fldActionConditions.
     */
    public void setFldActionConditions(String fldActionCondition) {
        this.fldActionConditions.add(fldActionCondition);
    }

    /**
     * Returns the value of fldActionInfos.
     */
    public ArrayList getFldActionInfos() {
        return fldActionInfos;
    }

    /**
     * Sets the value of fldActionInfos.
     *
     * @param fldActionInfos
     *            The value to assign fldActionInfos.
     */
    public void setFldActionInfos(ArrayList fldActionInfos) {
        this.fldActionInfos = fldActionInfos;
    }

    /**
     * Sets the value of fldActionInfos.
     *
     * @param fldActionInfos
     *            The value to assign fldActionInfos.
     */
    public void setFldActionInfos(Hashtable fldActionInfo) {
        this.fldActionInfos.add(fldActionInfo);
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    /**
     * Returns the value of fldIds.
     */
    public ArrayList getFldIds() {
        return fldIds;
    }

    /**
     * Sets the value of fldIds.
     *
     * @param fldIds
     *            The value to assign fldIds.
     */
    public void setFldIds(ArrayList fldIds) {
        this.fldIds = fldIds;
    }

    /**
     * Sets the value of fldIds.
     *
     * @param fldIds
     *            The value to assign fldIds.
     */
    public void setFldIds(String fldId) {
        this.fldIds.add(fldId);
    }
    
    private void setFldIdInt(Integer fldId) {
        this.fldIds.add(fldId);
    }
    
    public ArrayList getFldUniqIds() {
    	return this.fldUniqIds;
    }
    
    private void setFldUniqIds(String fldUniqId) {
    	this.fldUniqIds.add(fldUniqId);
    }
    
    public ArrayList getFldTypes() {
    	return this.fldTypes;
    }
    
    private void setFldTypes(String fldType) {
    	this.fldTypes.add(fldType);
    }
    
    public ArrayList getFldDataTypes() {
    	return this.fldDataTypes;
    }
    
    private void setFldDataTypes(String fldDataType) {
    	this.fldDataTypes.add(fldDataType);
    }
    
    public ArrayList getFldSysIds() {
    	return this.fldSysIds;
    }
    
    private void setFldSysIds(String fldSysId) {
    	this.fldSysIds.add(fldSysId);
    }

    /**
     * Returns the value of fldNames.
     */
    public ArrayList getFldNames() {
        return fldNames;
    }

    /**
     * Sets the value of fldNames.
     *
     * @param fldNames
     *            The value to assign fldNames.
     */
    public void setFldNames(ArrayList fldNames) {
        this.fldNames = fldNames;
    }

    /**
     * Returns the value of fldActionNames.
     */
    public ArrayList getFldActionNames() {
        return fldActionNames;
    }

    /**
     * Sets the value of fldActionNames.
     *
     * @param fldActionNames
     *            The value to assign fldActionNames.
     */
    public void setFldActionNames(ArrayList fldActionNames) {
        this.fldActionNames = fldActionNames;
    }

    /**
     * Sets the value of fldActionNames.
     *
     * @param fldActionName
     *            The value to add to fldActionNames.
     */
    public void setFldActionNames(String fldActionName) {
        this.fldActionNames.add(fldActionName);
    }

    /**
     * Sets the value of fldNames.
     *
     * @param fldName
     *            The value to add to fldNames.
     */
    public void setFldNames(String fldName) {
        this.fldNames.add(fldName);
    }

    // end of getter and setter methods

    public int setFieldActionData(FieldActionStateKeeper fsk, String mode) {
        int rows = 0;
        PreparedStatement pstmt = null;
        PreparedStatement pstmtFldActionSeq = null;
        PreparedStatement pstmtFldActionInfoSeq = null;
        PreparedStatement pstmtFldActionInfo = null;
        PreparedStatement pstmtDeleteFldActionInfo = null;
        PreparedStatement pstmtFldActionInfoUpdateBeforeDelete = null;
        Connection conn = null;

        String fldForm = "";
        String fldActionType = "";
        String fldActionCondition = "";
        String fldId = "";
        int pkFldAction = 0;
        Hashtable fldActionInfo = new Hashtable();
        int fldrow = 0;
        int retValue = 0;
        int pkFldActionInfo = 0;
        int fldInfoRow = 0;
        String usr = "";
        String ipAdd = "";

        try {

            fldForm = fsk.getFldForm();
            fldActionType = fsk.getFldActionType();
            fldActionCondition = fsk.getFldActionCondition();
            fldActionInfo = fsk.getFldActionInfo();
            fldId = fsk.getFldId();
            ipAdd = fsk.getIpAdd();

            if (mode.equals("M")) {
                pkFldAction = fsk.getFldActionId();
            }

            conn = getConnection();

            if (mode.equals("N")) {
                pstmtFldActionSeq = conn
                        .prepareStatement("Select seq_er_fldaction.nextval from dual");
                ResultSet rsFldActionSeq = pstmtFldActionSeq.executeQuery();

                while (rsFldActionSeq.next()) {
                    pkFldAction = rsFldActionSeq.getInt(1);
                }

                if (rsFldActionSeq != null)
                    rsFldActionSeq.close();
                if (pstmtFldActionSeq != null)
                    pstmtFldActionSeq.close();
            }

            // Insert Field Action record
            if (pkFldAction > 0) {
                if (mode.equals("N")) {
                    //KM-#3635
                	usr = fsk.getCreator();
                	pstmt = conn
                            .prepareStatement("INSERT INTO  er_fldaction (PK_FLDACTION, FK_FORM, FLDACTION_TYPE,  FK_FIELD, FLDACTION_CONDITION, CREATOR, IP_ADD 	) Values (?,?,?,?,?,?,?)  ");
                    pstmt.setInt(1, pkFldAction);
                    pstmt.setString(2, fldForm);
                    pstmt.setString(3, fldActionType);
                    pstmt.setString(4, fldId);
                    pstmt.setString(5, fldActionCondition);
                    pstmt.setInt(6, StringUtil.stringToNum(usr));
                    pstmt.setString(7, ipAdd);
                } else {
                	usr = fsk.getModifiedBy();
                    pstmt = conn
                            .prepareStatement("Update  er_fldaction set FLDACTION_TYPE = ?,  FK_FIELD = ?, FLDACTION_CONDITION = ?, LAST_MODIFIED_BY = ?, IP_ADD = ? Where PK_FLDACTION = ? ");
                    pstmt.setString(1, fldActionType);
                    pstmt.setString(2, fldId);
                    pstmt.setString(3, fldActionCondition);
                    pstmt.setInt(4, StringUtil.stringToNum(usr));
                    pstmt.setString(5, ipAdd);
                    pstmt.setInt(6, pkFldAction);
                }

                fldrow = pstmt.executeUpdate();

                // Insert child records for field Action Information

                if (fldrow > 0) {
                    // for modify mode, delete previous records first

                    if (mode.equals("M")) {
                    	
                    	pstmtFldActionInfoUpdateBeforeDelete = conn.prepareStatement("Update er_fldactioninfo set LAST_MODIFIED_BY = ? Where FK_FLDACTION = ?");
                    	pstmtFldActionInfoUpdateBeforeDelete.setInt(1, StringUtil.stringToNum(usr));
                    	pstmtFldActionInfoUpdateBeforeDelete.setInt(2, pkFldAction);
                        fldrow = pstmtFldActionInfoUpdateBeforeDelete.executeUpdate();
                        
                        pstmtDeleteFldActionInfo = conn
                                .prepareStatement("Delete from er_fldactioninfo Where FK_FLDACTION = ? ");
                        pstmtDeleteFldActionInfo.setInt(1, pkFldAction);
                        fldrow = pstmtDeleteFldActionInfo.executeUpdate();
                    }

                    pstmtFldActionInfo = conn
                            .prepareStatement("INSERT INTO  er_fldactioninfo (PK_FLDACTIONINFO ,FK_FLDACTION ,INFO_TYPE, INFO_VALUE, CREATOR, IP_ADD ) Values (?,?,?,?,?,?)  ");
                    pstmtFldActionInfoSeq = conn
                            .prepareStatement("Select seq_er_fldactioninfo.nextval from dual");

                    // loop through

                    for (Enumeration e = fldActionInfo.keys(); e
                            .hasMoreElements();) {
                        String htFieldkey = "";
                        String infoValue = "";
                        htFieldkey = (String) e.nextElement();

                        ArrayList arValues = new ArrayList();
                        arValues = (ArrayList) fldActionInfo.get(htFieldkey);

                        for (int i = 0; i < arValues.size(); i++) {
                        	if (arValues.get(i) != null){
                        		// get the value
                            	System.out.println("Inserting new response:" +arValues.get(i));
	                            infoValue = (String) arValues.get(i);

	                            // insert a row for the value

	                            // get primary key for info
	                            pkFldActionInfo = 0;
	                            ResultSet rsFldActionInfoSeq = pstmtFldActionInfoSeq
	                                    .executeQuery();
	                            while (rsFldActionInfoSeq.next()) {
	                                pkFldActionInfo = rsFldActionInfoSeq.getInt(1);
	                            }

	                            if (pkFldActionInfo > 0) {
	                                pstmtFldActionInfo.setInt(1, pkFldActionInfo);
	                                pstmtFldActionInfo.setInt(2, pkFldAction);
	                                pstmtFldActionInfo.setString(3, htFieldkey);
	                                if (htFieldkey.equals("[VELCONDITIONVALUE]"))
	                                    pstmtFldActionInfo.setString(4, StringUtil
	                                            .htmlEncode(infoValue));
	                                else
	                                    pstmtFldActionInfo.setString(4, infoValue);

	                                pstmtFldActionInfo.setInt(5,StringUtil.stringToNum(usr));
	                                pstmtFldActionInfo.setString(6,ipAdd);
	                                fldInfoRow = pstmtFldActionInfo.executeUpdate();
	                            }
                        	}

                        }
                    }

                    // end of loop for hashtable
                }

            }
            if (!conn.getAutoCommit()) {
                conn.commit();
            }
        } catch (SQLException ex) {
            Rlog.fatal("formlib",
                    "FieldActionDao.setFieldActionData  EXCEPTION IN SAVING FIELD ACTION: "
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
                if (pstmtFldActionInfo != null)
                    pstmtFldActionInfo.close();
                if (pstmtFldActionInfoSeq != null)
                    pstmtFldActionInfoSeq.close();
                if (pstmtDeleteFldActionInfo != null)
                    pstmtDeleteFldActionInfo.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return retValue;

    }

    // ////////////////////////

    public int getFieldsForFieldAction(int formId) {

        int ret = 1;
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlBuffer = new StringBuffer();
        try {

            conn = getConnection();

            sqlBuffer
                    .append(" Select  pk_field, fld_uniqueid, fld_name, fld_type, fld_datatype,fld_systemid ");
            sqlBuffer
                    .append(" from er_formsec sec, er_formfld frmfld, er_fldlib fld ");
            sqlBuffer
                    .append(" where fk_formlib = ? and nvl(sec.record_type,'Z') <> 'D' and ");
            sqlBuffer.append(" frmfld.fk_formsec =  sec.pk_formsec and  ");
            sqlBuffer.append(" nvl(frmfld.record_type, 'Z') <> 'D' and ");
            sqlBuffer.append(" nvl(fld.record_type,'Z') <> 'D' and");
            sqlBuffer.append(" frmfld.fk_field =  fld.pk_field and ");
            sqlBuffer.append(" nvl(fld.fld_repeatflag,0) = 0  ");
            sqlBuffer.append("  and sec.formsec_repno = 0 ");
            sqlBuffer.append("  and fld.fld_type IN ('E','M')");
            sqlBuffer.append("  and fld.fld_systemid <> 'er_def_date_01' ");
            sqlBuffer.append("  and fld.fld_datatype <> 'ML' ");
            sqlBuffer
                    .append("  order by formsec_seq , formfld_seq ,pk_formsec  ");

            pstmt = conn.prepareStatement(sqlBuffer.toString());
            pstmt.setInt(1, formId);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setFldIdInt(new Integer(rs.getInt("PK_FIELD")));
                setFldUniqIds(rs.getString("FLD_UNIQUEID"));
                setFldNames(rs.getString("FLD_NAME"));
                setFldTypes(rs.getString("FLD_TYPE"));
                setFldDataTypes(rs.getString("FLD_DATATYPE"));
                setFldSysIds(rs.getString("fld_systemid"));
                rows++;
            }
            setCRows(rows);
            Rlog.debug("formlib",
                    "FieldActionDao.getFieldsForFieldAction() number of rows"
                            + rows);
            return rows;

        } catch (SQLException ex) {
            Rlog.fatal("formlib",
                    "FieldActionDao.getFieldsForFieldAction() EXCEPTION IN FETCHING DATA"
                            + ex);
            return -1;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Populates the data access object with field actions for a form
     *
     * @param formId
     *            the id of the form
     */

    public void getFieldActionsBrowserInfo(String formId) {

        /* returns a FieldActionDao with FieldAction Browser information */

        StringBuffer sbFfldSQL = new StringBuffer();

        sbFfldSQL
                .append("Select FK_FIELD,PK_FLDACTION,FLDACTION_TYPE, FLDACTION_CONDITION, FLDACTIONLIB_NAME ");
        sbFfldSQL
                .append(" from er_fldaction, er_fldactionlib Where FK_FORM  = ? and FLDACTION_TYPE = FLDACTIONLIB_TYPE  Order by PK_FLDACTION ");

        PreparedStatement pstmtFld = null;
        Connection conn = null;

        String field = "";
        int fldPkAction = 0;

        String fldActionType;
        String fldActionCondition;
        String fldActionTypeName = "";
        String sourceFldName = "";
        int totalActionRows = 0;

        try {
            conn = getConnection();

            pstmtFld = conn.prepareStatement(sbFfldSQL.toString());

            pstmtFld.setString(1, formId);
            ResultSet rs = pstmtFld.executeQuery();

            Rlog
                    .debug("formlib",
                            "VFieldActionDao.getFieldActionsBrowserInfo method started");

            while (rs.next()) {
                totalActionRows++;

                field = rs.getString("FK_FIELD");
                fldPkAction = rs.getInt("PK_FLDACTION");
                fldActionType = rs.getString("FLDACTION_TYPE");
                fldActionCondition = rs.getString("FLDACTION_CONDITION");
                fldActionTypeName = rs.getString("FLDACTIONLIB_NAME");
                // sourceFldName = rs.getString("FLD_NAME");

                // setFldNames(sourceFldName);
                setFldActionNames(fldActionTypeName);
                setFldIds(field);
                setFldActionConditions(fldActionCondition);
                setFldActionTypes(fldActionType);
                setFldForms(formId);
                setFldActionIds(new Integer(fldPkAction));

            }
            setCRows(totalActionRows);

        } catch (Exception ex) {
            Rlog.fatal("formlib",
                    "VFieldActionDao.getFieldActionsBrowserInfo -- > Exception "
                            + ex);

        } finally {
            try {
                if (pstmtFld != null)
                    pstmtFld.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

        // end of method

    }

    // ////////////////////////////////////////////////////////////////////

    /**
     * Gets complete information for a field action
     *
     * @param fldAction
     *            Field Action Id
     * @return returns a FieldActionStateKeeper object with field Action
     *         information
     */
    public FieldActionStateKeeper getFieldActionDetails(int fldAction) {
        /* Populates complete FieldAction info for a field action */

        StringBuffer sbFfldSQL = new StringBuffer();
        sbFfldSQL
                .append("Select FK_FIELD,FLDACTION_TYPE, FLDACTION_CONDITION ");
        sbFfldSQL.append(" from er_fldaction Where PK_FLDACTION  = ? ");

        PreparedStatement pstmtFld = null;
        Connection conn = null;

        String fldActionType;
        String fldActionCondition;

        String sourceFld = "";

        String fldSysId;

        FieldActionStateKeeper fsk = new FieldActionStateKeeper();

        try {
            conn = getConnection();

            pstmtFld = conn.prepareStatement(sbFfldSQL.toString());

            pstmtFld.setInt(1, fldAction);
            ResultSet rs = pstmtFld.executeQuery();

            VFieldAction fldActionObj = new VFieldAction();

            Rlog.debug("formlib",
                    "VFieldActionDao.getFieldActionDetails method started");

            while (rs.next()) {

                sourceFld = rs.getString("FK_FIELD");
                fldActionType = rs.getString("FLDACTION_TYPE");
                fldActionCondition = rs.getString("FLDACTION_CONDITION");

                Hashtable htActionInfo = new Hashtable();

                fsk.setFldId(sourceFld);
                fsk.setFldActionId(fldAction);
                fsk.setFldActionType(fldActionType);
                fsk.setFldActionCondition(fldActionCondition);

                // get keyword information for this field Action

                htActionInfo = fldActionObj.getFieldActionKeywords(fldAction);

                fsk.setFldActionInfo(htActionInfo);

            }
            return fsk;

        } catch (Exception ex) {
            Rlog.fatal("formlib",
                    "VFieldActionDao.getFieldActionDetails -- > Exception "
                            + ex);
            return fsk;
        } finally {
            try {
                if (pstmtFld != null)
                    pstmtFld.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

        // end of method

    }

    /**
     * gets script from VFieldAction.getFormFieldActionScripts(). Method returns
     * a Hastable with: key: customjs (its value will have form's custom js)
     * key: activationjs (its value will have form's activation js) Updates
     * field action scripts in table er_formlib
     *
     * @param formId
     *            form id for which field action script is to be generated
     *
     */
    public void generateFieldActionsScript(String formId) {

        Hashtable htScripts = new Hashtable();
        String customJS = "";
        String activationJS = "";
        Connection conn = null;
        PreparedStatement stmt = null;
        String sqlStr = "";

        try {

            htScripts = VFieldAction.getFormFieldActionScripts(formId);

            if (htScripts != null) {
                if (htScripts.containsKey("customjs")) {
                    customJS = (String) htScripts.get("customjs");
                }

                if (htScripts.containsKey("activationjs")) {
                    activationJS = (String) htScripts.get("activationjs");
                }

            }

            conn = getConnection();

            sqlStr = " Update er_formlib set form_custom_js = ?, FORM_ACTIVATION_JS = ? , FORM_XSLREFRESH  = ? where pk_formlib = ?";

            stmt = conn.prepareStatement(sqlStr);
            stmt.setClob(1, getCLOB(customJS, conn));
            stmt.setClob(2, getCLOB(activationJS, conn));
            stmt.setInt(3, 1);
            stmt.setString(4, formId);
            stmt.execute();

            if (!conn.getAutoCommit()) {
                conn.commit();
            }

        } catch (Exception ex) {
            System.out.println("Could not update form_custom_js" + ex);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
            }

            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

        // end of update
        // end of method

    }

    /**
     * Deletes a field action and regerates the form field action script
     *
     * @param fldActionId
     *            id of Field Action to be deleted
     * @param formId
     *            id of the Field Action's Form
     */
    public int deleteFieldAction(String fldActionId, String formId) {

        Connection conn = null;
        PreparedStatement stmtActionInfo = null;
        PreparedStatement stmtAction = null;
        String sqlStrActionInfo = "";
        String sqlStrAction = "";

        try {

            conn = getConnection();

            sqlStrActionInfo = " delete from er_fldactioninfo Where fk_fldaction = ? ";
            sqlStrAction = " delete from er_fldaction Where pk_fldaction = ? ";

            stmtActionInfo = conn.prepareStatement(sqlStrActionInfo);
            stmtActionInfo.setString(1, fldActionId);
            stmtActionInfo.execute();

            stmtAction = conn.prepareStatement(sqlStrAction);
            stmtAction.setString(1, fldActionId);
            stmtAction.execute();

            // re generate the inter field action script for the form
            generateFieldActionsScript(formId);

            if (!conn.getAutoCommit()) {
                conn.commit();
            }

        } catch (Exception ex) {
            Rlog
                    .fatal(
                            "formlib",
                            "VFieldActionDao.deleteFieldAction -- > Could not delete interfield action : Exception "
                                    + ex);
            return -2;
        } finally {
            try {
                if (stmtActionInfo != null)
                    stmtActionInfo.close();
                if (stmtAction != null)
                    stmtAction.close();
            } catch (Exception e) {
            }

            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return 0;
        // end of method

    }
    
    //Overloaded for INF-18183 ::: Ankit
    public int deleteFieldAction(String fldActionId, String formId, Hashtable<String, String> auditInfo) {

        Connection conn = null;
        PreparedStatement stmtActionInfo = null;
        PreparedStatement stmtAction = null;
        String sqlStrActionInfo = "";
        String sqlStrAction = "";
        Connection connAudit = null;
        try {

            conn = getConnection();
            
            AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String getRidValue= AuditUtils.getRidValue("er_fldaction","eres","pk_fldaction="+fldActionId);/*Fetches the RID/PK_VALUE*/
            Hashtable<String, ArrayList<String>> resultHT = new Hashtable<String, ArrayList<String>>();
            ArrayList<String> pkValue = new ArrayList<String>();pkValue.add(fldActionId);
            ArrayList<String> ridValue = new ArrayList<String>();ridValue.add(getRidValue);
            resultHT.put(AuditUtils.ROW_PK_KEY, pkValue);
            resultHT.put(AuditUtils.ROW_RID_KEY, ridValue);
            String moduleName = (String)auditInfo.get(AuditUtils.APP_MODULE);
            connAudit = AuditUtils.insertAuditRow("er_fldaction",resultHT,moduleName,auditInfo,"eres");

            sqlStrActionInfo = " delete from er_fldactioninfo Where fk_fldaction = ? ";
            sqlStrAction = " delete from er_fldaction Where pk_fldaction = ? ";

            stmtActionInfo = conn.prepareStatement(sqlStrActionInfo);
            stmtActionInfo.setString(1, fldActionId);
            stmtActionInfo.execute();

            stmtAction = conn.prepareStatement(sqlStrAction);
            stmtAction.setString(1, fldActionId);
            stmtAction.execute();

            // re generate the inter field action script for the form
            generateFieldActionsScript(formId);
            
            AuditUtils.commitOrRollback(connAudit, true);
            if (!conn.getAutoCommit()) {
                conn.commit();
            }

        } catch (Exception ex) {
        	Rlog
                    .fatal(
                            "formlib",
                            "VFieldActionDao.deleteFieldAction -- > Could not delete interfield action : Exception "
                                    + ex);
        	try{
        		AuditUtils.commitOrRollback(connAudit, false);
        	}catch(Exception e){
        		Rlog.fatal("formlib","Exception occurs at AuditUtils.commitOrRollback()" + e);
        	}
            return -2;
        } finally {
            try {
                if (stmtActionInfo != null)
                    stmtActionInfo.close();
                if (stmtAction != null)
                    stmtAction.close();
            } catch (Exception e) {
            }

            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return 0;
        // end of method

    }

    ///////////////////////////////////
    /**
     * returns true if field as any inter field action associated with the field
     *
     * @param fieldId
     *
     *
     */

    public static boolean hasInterFieldActions(String fieldId) {

        /* returns a FieldActionDao with FieldAction Browser information */

        StringBuffer sbFfldSQL = new StringBuffer();

        sbFfldSQL.append("Select count(9) from er_fldactioninfo where (trim(info_type) = '[VELSOURCEFLDID]' or trim(info_type) = '[VELTARGETID]') and trim(info_value) = ?");


        PreparedStatement pstmtFld = null;
        Connection conn = null;


        int total = 0;

        try {
            conn = getConnection();

            pstmtFld = conn.prepareStatement(sbFfldSQL.toString());

            pstmtFld.setString(1, fieldId);
            ResultSet rs = pstmtFld.executeQuery();

            Rlog.debug("formlib","VFieldActionDao.hasInterFieldActions method started" + sbFfldSQL.toString());

            while (rs.next()) {

            	total = rs.getInt(1);
                      }

            if (total > 0)
            	return true;
            else
            	return false;

        } catch (Exception ex) {
            Rlog.fatal("formlib",
                    "VFieldActionDao.hasInterFieldActions -- > Exception "
                            + ex);
            return true;

        } finally {
            try {
                if (pstmtFld != null)
                    pstmtFld.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

        // end of method

    }


///////////////////////////////////
    /**
     * returns true if field as any inter field action associated with the field
     *
     * @param sectionId
     *
     *
     */

    public static boolean sectionHasInterFieldActions(String sectionId) {

        /* returns a FieldActionDao with FieldAction Browser information */

        StringBuffer sbFfldSQL = new StringBuffer();

        sbFfldSQL.append("Select count(9) from er_fldactioninfo where (trim(info_type) = '[VELSOURCEFLDID]' or trim(info_type) = '[VELTARGETID]') and trim(info_value) in (select to_char(fk_field) from er_formfld where fk_formsec = ?)");


        PreparedStatement pstmtFld = null;
        Connection conn = null;


        int total = 0;

        try {
            conn = getConnection();

            pstmtFld = conn.prepareStatement(sbFfldSQL.toString());

            pstmtFld.setString(1, sectionId);
            ResultSet rs = pstmtFld.executeQuery();

            Rlog.debug("formlib","VFieldActionDao.sectionHasInterFieldActions method started" + sbFfldSQL.toString());

            while (rs.next()) {

            	total = rs.getInt(1);
                      }

            if (total > 0)
            	return true;
            else
            	return false;

        } catch (Exception ex) {
            Rlog.fatal("formlib",
                    "VFieldActionDao.sectionHasInterFieldActions -- > Exception "
                            + ex);
            return true;

        } finally {
            try {
                if (pstmtFld != null)
                    pstmtFld.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

        // end of method

    }


    // end of class
}
