/*
 * Classname : UserPwdHistoryBean
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Raviesh
 */

package com.velos.eres.business.userPwdHistory.impl;

/* Import Statements */
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.Security;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The UserPwdHistoryBean CMP entity bean.
 * 
 * @author Raviesh
 */
@Entity
@Table(name = "ER_USRPWD_HISTORY")
@NamedQueries( {
        @NamedQuery(name = "findUserPwdRotation", query = "SELECT user FROM UserPwdHistoryBean user where  " +
        		" userId=? and  fkAccount =? order by createdOn Desc")
  })
public class UserPwdHistoryBean implements Serializable {

	private static final long serialVersionUID = 3256725069878539570L;

    @Id 
    @Column(name = "PK_USRPWD_HISTORY", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long pwdId;
    
    @Column(name = "FK_ER_USER")
    private Integer userId;
    
    @Column(name = "FK_ACCOUNT")
    private Integer fkAccount;
    
    @Column(name = "USR_PWD")
    private String userPwd;
    
    @Column(name = "RID")
    private Integer rid;
    
    @Column(name = "CREATOR")
    private Integer creator;
    
    @Column(name = "IP_ADD")
    private String ipAddress;
    
    @Column(name = "CREATED_ON")
    private Date createdOn;
    
	public long getPwdId() {
		return pwdId;
	}
	public void setPwdId(long pwdId) {
		this.pwdId = pwdId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getFkAccount() {
		return fkAccount;
	}
	public void setFkAccount(Integer fkAccount) {
		this.fkAccount = fkAccount;
	}
	public String getUserPwd() {
		return userPwd;
	}
	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}
	public Integer getRid() {
		return rid;
	}
	public void setRid(Integer rid) {
		this.rid = rid;
	}
	public Integer getCreator() {
		return creator;
	}
	public void setCreator(Integer creator) {
		this.creator = creator;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
    
}
