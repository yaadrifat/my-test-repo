/*
 * Classname		UserSessionLogBean.class
 * 
 * Version information	1.0
 *
 * Date					03/10/2003
 * 
 * Copyright notice    Velos Inc.
 */

package com.velos.eres.business.userSessionLog.impl;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The UserSessionLog CMP entity bean.<br>
 * <br>
 * 
 * @author Sonia Sahni
 * @version 1.0
 * @ejbHome UserSessionLogHome
 * @ejbRemote UserSessionLogRObj
 */
@Entity
@Table(name = "er_USERSESSIONLOG")
public class UserSessionLogBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3545794390531388464L;

    /**
     * the UserSessionLog Id
     */
    public int id;

    /**
     * the the user session id
     */
    public Integer userSession;

    /**
     * the access date time of URL
     */

    public Timestamp uslAccessTime;

    /**
     * the URL parameters
     */

    public String uslURLParam;

    /*
     * the URL module name
     */
    public String uslURLModule;

    /*
     * IP Address
     */
    public String uslURL;
    
    /*
     * Primary key of the Entity user has viewed/changed
     */
    public Long entityId;
    
    /*
     * Type of the entity
     */
    public String entityType;
    
    /*
     * Sub Module User Focused On
     */
    public String entityIdentifier;
    
    /*
     * Code of the event performed(Add,View,Edit,Delete)
     */
    public String eventCode;

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_USERSESSIONLOG", allocationSize=1)
    @Column(name = "PK_USL")
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        Rlog.debug("usl", "UserSessionLogBean.setId " + id);
        this.id = id;
    }

    @Column(name = "FK_USERSESSION")
    public String getUserSession() {
        return StringUtil.integerToString(this.userSession);
    }

    public void setUserSession(String userSession) {
        Rlog.debug("usl", "UserSessionLogBean.setUserSession " + userSession);
        if (userSession != null) {
            this.userSession = Integer.valueOf(userSession);
        }
    }

    @Column(name = "USL_ACCESSTIME")
    public Timestamp getUslAccessTimeStamp() {
        return this.uslAccessTime;

    }

    public void setUslAccessTimeStamp(Timestamp uslAccessTime) {
        this.uslAccessTime = uslAccessTime;
    }

    @Transient
    public String getUslAccessTime() {
        String pDate;
        pDate = "";
        pDate = DateUtil.dateToString(getUslAccessTimeStamp());
        return pDate;
    }

    public void setUslAccessTime(String uslAccessTime) {

        setUslAccessTimeStamp(DateUtil.stringToTimeStamp(uslAccessTime,
                null));

    }

    @Column(name = "USL_URL")
    public String getUslURL() {
        return this.uslURL;
    }

    public void setUslURL(String uslURL) {
        Rlog.debug("usl", "UserSessionLogBean.setUslURL " + uslURL);
        if (uslURL != null) {
            this.uslURL = uslURL;
        }
    }

    @Column(name = "USL_MODNAME")
    public String getUslURLModule() {
        return this.uslURL;
    }

    public void setUslURLModule(String uslURLModule) {
        Rlog.debug("usl", "UserSessionLogBean.setUslURLModule " + uslURLModule);
        if (uslURLModule != null) {
            this.uslURLModule = uslURLModule;
        }
    }

    @Column(name = "USL_URLPARAM")
    public String getUslURLParam() {
        return this.uslURLParam;
    }

    public void setUslURLParam(String uslURLParam) {
        Rlog.debug("usl", "UserSessionLogBean.setUslURLParam " + uslURLParam);
        if (uslURLParam != null) {
            this.uslURLParam = uslURLParam;
        }
    }
    @Column(name = "USL_ENTITY_ID")
    public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	@Column(name = "USL_ENTITY_TYPE")
    public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	@Column(name = "USL_ENTITY_IDENTIFIER")
    public String getEntityIdentifier() {
		return entityIdentifier;
	}

	public void setEntityIdentifier(String entityIdentifier) {
		this.entityIdentifier = entityIdentifier;
	}

	@Column(name = "USL_ENTITY_EVENT_CODE")
    public String getEventCode() {
		return eventCode;
	}

	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}
    
    

    // END OF GETTER SETTER METHODS

    /**
     * @return state holder object associated with this UserSessionLog
     */
    /*
     * public UserSessionLogStateKeeper getUserSessionLogStateKeeper() {
     * 
     * return (new UserSessionLogStateKeeper(getId(), getUserSession(),
     * getUslURL(), getUslAccessTime(), getUslURLParam(), getUslURLModule())); }
     */

    /**
     * Inserts the UserSessionLog in the Database
     * 
     * @param usk
     *            State Keeper of UserSessionLog
     * @return 0 when updation is successful and -2 when some exception occurs
     */
    /*
     * public int setUserSessionLogStateKeeper(UserSessionLogStateKeeper usk) {
     * 
     * GenerateId genId = null;
     * 
     * try { Connection conn = null; conn = getConnection(); Rlog.debug("usl",
     * "UserSessionLogBean.setUserSessionLogStateKeeper line 1"); id =
     * genId.getId("SEQ_ER_USERSESSIONLOG", conn); Rlog.debug("usl",
     * "UserSessionLogBean.setUserSessionLogStateKeeper line 2"); conn.close();
     * 
     * setId(id); Rlog.debug("usl",
     * "UserSessionLogBean.setUserSessionLogStateKeeper line 3");
     * setUserSession(usk.getUserSession()); Rlog.debug("usl",
     * "UserSessionLogBean.setUserSessionLogStateKeeper line 4");
     * setUslAccessTime(usk.getUslAccessTime()); Rlog.debug("usl",
     * "UserSessionLogBean.setUserSessionLogStateKeeper line 5");
     * setUslURL(usk.getUslURL()); Rlog.debug("usl",
     * "UserSessionLogBean.setUserSessionLogStateKeeper line 6");
     * setUslURLModule(usk.getUslURLModule()); Rlog.debug("usl",
     * "UserSessionLogBean.setUserSessionLogStateKeeper line 7");
     * setUslURLParam(usk.getUslURLParam()); Rlog.debug("usl",
     * "UserSessionLogBean.setUserSessionLogStateKeeper line 8"); return 0; }
     * catch (Exception e) { Rlog.fatal("usl", "EXCEPTION IN STORING
     * usersessionlog" + e); return -2; } }
     */

    /**
     * Updates the usl in the Database
     * 
     * @param usk
     *            State Keeper of USL
     * @return 0 when updation is successful and -2 when some exception occurs
     */
    public int updateUserSessionLog(UserSessionLogBean usk) {

        try {
            setUserSession(usk.getUserSession());
            setUslAccessTime(usk.getUslAccessTime());
            setUslURL(usk.getUslURL());
            setUslURLModule(usk.getUslURLModule());
            setUslURLParam(usk.getUslURLParam());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("usl",
                    "EXCEPTION IN UPDATING usersessionlog IN DATABASE " + e);
            return -2;
        }
    }

    public UserSessionLogBean() {

    }

    public UserSessionLogBean(int id, String userSession, String uslAccessTime,
            String uslURLParam, String uslURLModule, String uslURL) {
        super();
        // TODO Auto-generated constructor stub
        setId(id);
        setUserSession(userSession);
        setUslAccessTime(uslAccessTime);
        setUslURLParam(uslURLParam);
        setUslURLModule(uslURLModule);
        setUslURL(uslURL);
    }	
    
    public UserSessionLogBean(int id, String userSession, String uslAccessTime,
            String uslURLParam, String uslURLModule, String uslURL,Long entityId,String entityType,String entityIdentifier,String eventCode) {
        super();
        // TODO Auto-generated constructor stub
        setId(id);
        setUserSession(userSession);
        setUslAccessTime(uslAccessTime);
        setUslURLParam(uslURLParam);
        setUslURLModule(uslURLModule);
        setUslURL(uslURL);
        setEntityId(entityId);
        setEntityType(entityType);
        setEntityIdentifier(entityIdentifier);
        setEventCode(eventCode);
    }	

}// end of class

