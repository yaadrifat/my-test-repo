/*
 * Classname			InvoiceDetailBean
 * 
 * Version information : 1.0
 *
 * Date					09/22/2005
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.business.invoice;

/* IMPORT STATEMENTS */

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* END OF IMPORT STATEMENTS */

/**
 * The Invoice POJO.<br>
 * <br>
 * 
 * @author Sonia Abrol
 * 
 * @version 1.0, 10/18/2005
 */
@Entity
@Table(name = "er_invoice_detail")
@NamedQueries({
    @NamedQuery(name = "findInvDetailByMileIdAndMileAchId", 
    		query = " SELECT OBJECT(invoice) FROM InvoiceDetailBean invoice "+
    		" where FK_MILESTONE = :FK_MILESTONE and FK_MILEACHIEVED = :FK_MILEACHIEVED and FK_INV = :FK_INV")
})
public class InvoiceDetailBean implements Serializable {

	 
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 6245159726381161255L;

	/**
	 * the Invoice Primary Key
	 */
	public int id;

	/**
	 * the main Invoice PK 
	 */
	public String invPk;

	/**
	 * the associated milestone
	 */
	public String milestone;

	/**
	 * the associated patient
	 */
	public String patient;

	/**
	 * the associated study
	 */
	public String study;

	/**
	 * the associated patient enrollment
	 */
	public String patProt;

	/**
	 * Amount due
	 */
	public String amountDue;

	/**
	 * the amount invoiced 
	 */
	public String amountInvoiced;
	/**
	 * the amount Holdback 
	 */
	public String amountHoldback;

	/**
	 * Payment due date
	 */
	public Date paymentDueDate;
	
	 
	/**
	 * the id of the user who created the record
	 */
	public String creator;

	/**
	 * Id of the user who last modified the record
	 */
	public String lastModifiedBy;

	/**
	 * the IP address of the user who created or modified the record
	 */
	public String iPAdd;
	
	/** indicates if it is high level or detailed record*/
	public String detailType;
	
	/** indicates if detail records will be displayed in final invoice*/
	public String displayDetail;
	
	/** the number of milestones achieved for the invoice detail (will only be stored for the high level record)*/
	public String milestonesAchievedCount;
	
	/**
	 * Achieved on
	 */
	public Date achievedOn;
	
	/**
	 * FK to er_mileachieved, will be used for milestone related records. <br>
	 * (this will help identify the exact milestone achievement record for multiple achievements for the same patient and milestone combination)
	 */
	public String linkMileAchieved;
	

	@Column(name = "achieved_on")
	public Date getAchievedOn() {
		return achievedOn;
	}

	public void setAchievedOn(Date achievedOn) {
		this.achievedOn = achievedOn;
	}

	// GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
	@SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_INVOICE_DETAIL", allocationSize=1)
	@Column(name = "pk_invdetail")
	
	public int getId() {
		return this.id;
	}

	public void setId(int Id) {
		this.id =Id;
	}
	
	
	@Column(name = "amount_due")
	public String getAmountDue() {
		return amountDue;
	}

	public void setAmountDue(String amountDue) {
		this.amountDue = amountDue;
	}

	@Column(name = "amount_invoiced")
	public String getAmountInvoiced() {
		return amountInvoiced;
	}

	public void setAmountInvoiced(String amountInvoiced) {
		this.amountInvoiced = amountInvoiced;
	}

	@Column(name = "fk_inv")
	public String getInvPk() {
		return invPk;
	}

	public void setInvPk(String invPk) {
		this.invPk = invPk;
	}

	@Column(name = "fk_milestone")
	public String getMilestone() {
		return milestone;
	}

	public void setMilestone(String milestone) {
		this.milestone = milestone;
	}

	@Column(name = "fk_per")
	public String getPatient() {
		return patient;
	}

	public void setPatient(String patient) {
		this.patient = patient;
	}

	@Column(name = "fk_patprot")
	public String getPatProt() {
		return patProt;
	}

	public void setPatProt(String patProt) {
		this.patProt = patProt;
	}

	@Column(name = "payment_duedate")
	public Date getPaymentDueDate() {
		return paymentDueDate;
	}

	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}

	@Column(name = "fk_study")
	public String getStudy() {
		return study;
	}

	public void setStudy(String study) {
		this.study = study;
	}

	@Column(name = "CREATOR")
	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	
	@Column(name = "ip_add")
	public String getIPAdd() {
		return iPAdd;
	}

	public void setIPAdd(String add) {
		iPAdd = add;
	}
	
	@Column(name = "LAST_MODIFIED_BY")
	
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	
	@Column(name = "detail_type")
	public String getDetailType() {
		return detailType;
	}

	public void setDetailType(String detailType) {
		this.detailType = detailType;
	}

	@Column(name = "display_detail")
	public String getDisplayDetail() {
		return displayDetail;
	}

	public void setDisplayDetail(String displayDetail) {
		this.displayDetail = displayDetail;
	}

//	 END OF GETTER SETTER METHODS
	
	/**
	 * Updates the attributes of current insatance with the attributes of the
	 * one passed as argument
	 * 
	 * @param iBean
	 *            InvoiceDetailBean with the Invoice Detail Information
	 * 
	 * @return 0 - if update is successful <br>
	 *         -2 - if update fails
	 * 
	 */
	public int updateInvoiceDetail(InvoiceDetailBean iBean) {
		try {
			 
				setAmountDue(iBean.getAmountDue()); 
				setAmountInvoiced(iBean.getAmountInvoiced()); 
				setAmountHoldback(iBean.getAmountHoldback());
				setInvPk(iBean.getInvPk()) ;
				setMilestone(iBean.getMilestone()); 
				setPatient(iBean.getPatient()) ;
				setPatProt(iBean.getPatProt()) ;
				setPaymentDueDate(iBean.getPaymentDueDate()); 
				setStudy(iBean.getStudy()) ;
				setCreator(iBean.getCreator()); 
				setIPAdd(iBean.getIPAdd()) ;
				setLastModifiedBy(iBean.getLastModifiedBy()); 	
									
				setIPAdd(iBean.getIPAdd()) ;
				setLastModifiedBy(iBean.getLastModifiedBy()) ;
				setCreator(iBean.getCreator());
				setDisplayDetail(iBean.getDisplayDetail());
				setDetailType(iBean.getDetailType());
				setMilestonesAchievedCount(iBean.getMilestonesAchievedCount());
				
				setAchievedOn( iBean.getAchievedOn());
				 setLinkMileAchieved(iBean.getLinkMileAchieved());	
			
			Rlog.debug("invoice", "InvoiceDetailBean.updateInvoiceDetail");
			return 0;
		} catch (Exception e) {
			Rlog.fatal("InvoiceDetail",
					"Exception in InvoiceDetailBean.updateInvoiceDetail " + e);
			return -2;
		}
	}
   
	
	/** Default constructor */
		
	public InvoiceDetailBean()
	{
		
	}

	public InvoiceDetailBean(String amountDue, String amountInvoiced, String amountHoldback, String creator, int id, String invPk, String iPAdd, String lastModifiedBy, String milestone, String patient, String patProt, Date paymentDueDate , String study,
			String displayDetail, String detailType, String achCount, Date achievedOn,String mileach) {
		super();
		// TODO Auto-generated constructor stub
		this.amountDue = amountDue;
		this.amountInvoiced = amountInvoiced;
		this.amountHoldback = amountHoldback;
		this.creator = creator;
		this.id = id;
		this.invPk = invPk;
		this.iPAdd = iPAdd;
		this.lastModifiedBy = lastModifiedBy;
		this.milestone = milestone;
		this.patient = patient;
		this.patProt = patProt;
		this.paymentDueDate = paymentDueDate ;
		this.study = study;
		setDisplayDetail(displayDetail);
		setDetailType(detailType);
		setMilestonesAchievedCount(achCount);
		setAchievedOn( achievedOn);
		setLinkMileAchieved(mileach);
		
	}

	@Column(name = "milestones_achieved")
	public String getMilestonesAchievedCount() {
		return milestonesAchievedCount;
	}

	public void setMilestonesAchievedCount(String milestonesAchievedCount) {
		this.milestonesAchievedCount = milestonesAchievedCount;
	}
	
	@Column(name = "FK_MILEACHIEVED")
	public String getLinkMileAchieved() {
		return linkMileAchieved;
	}

	public void setLinkMileAchieved(String linkMileAchieved) {
		this.linkMileAchieved = linkMileAchieved;
	}
	@Column(name = "AMOUNT_HOLDBACK")
	public String getAmountHoldback() {
		return amountHoldback;
	}

	public void setAmountHoldback(String amountHoldback) {
		this.amountHoldback = amountHoldback;
	}

	

	/** Full Argument constructor */
	
	


}// end of class
