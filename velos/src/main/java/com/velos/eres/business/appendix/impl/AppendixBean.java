/*
 * Classname			appendixBean.class
 * 
 * Version information
 *
 * Date					03/03/2001
 * 
 * Copyright notice Aithent
 */

package com.velos.eres.business.appendix.impl;

/**
 * 
 */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

@Entity
@Table(name = "er_studyapndx")
public class AppendixBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3906088940661715000L;

    /**
     * the Appendix Id
     */
    public int id;

    /**
     * appendix type
     */
    public String type;

    /**
     * the url/file name
     */
    public String url_file;

    /**
     * appendix description
     */

    public String desc;

    /**
     * appendix Public Flag
     */

    public String pubflag;

    /**
     * appendix study
     */

    public Integer study;

    /**
     * appendix study Version Id
     */

    public Integer studyVerId;

    //
    public String filename;

    public Integer fileSize;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;
    /*
     * Stamp Flag
     */
    public Integer stampFlag;
    /*
     * Stamp Reference
     */
    public Integer stampRef;
    /*
     * Backup file name before stamping
     */
    public String preStamp_FileUri;

	// GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_STUDYAPNDX", allocationSize=1)
    @Column(name = "pk_studyapndx")
    public int getAppendixId() {
        return this.id;
    }

    public void setAppendixId(int id) {
        this.id = id;
    }

    @Column(name = "studyapndx_type")
    public String getAppendixType() {
        return this.type;
    }

    public void setAppendixType(String type) {
        this.type = type;
    }

    @Column(name = "studyapndx_pubflag")
    public String getAppendixPubFlag() {
        return this.pubflag;
    }

    public void setAppendixPubFlag(String pubflag) {
        this.pubflag = pubflag;
    }

    @Column(name = "fk_study")
    public String getAppendixStudy() {
        return StringUtil.integerToString(this.study);
    }

    public void setAppendixStudy(String study) {
        if (study != null)
            this.study = Integer.valueOf(study);
    }

    @Column(name = "FK_STUDYVER")
    public String getAppendixStudyVer() {
        return StringUtil.integerToString(this.studyVerId);
    }

    public void setAppendixStudyVer(String studyVerId) {
        if (studyVerId != null)
            this.studyVerId = Integer.valueOf(studyVerId);
    }

    @Column(name = "studyapndx_uri")
    public String getAppendixUrl_File() {
        return this.url_file;
    }

    public void setAppendixUrl_File(String url_file) {
        this.url_file = url_file;
    }

    @Column(name = "studyapndx_desc")
    public String getAppendixDescription() {
        return this.desc;
    }

    public void setAppendixDescription(String desc) {
        this.desc = desc;
    }

    @Column(name = "studyapndx_file")
    public String getAppendixSavedFileName() {
        return this.filename;
    }

    public void setAppendixSavedFileName(String filename) {
        this.filename = filename;
    }

    @Column(name = "STUDYAPNDX_FILESIZE")
    public String getFileSize() {
        return StringUtil.integerToString(this.fileSize);
    }

    public void setFileSize(String fileSize) {
        if (fileSize != null) {
            this.fileSize = Integer.valueOf(fileSize);
        }
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    
    @Column(name = "STAMP_FLAG")
    public Integer getStampFlag() {
		return stampFlag;
	}

	public void setStampFlag(Integer stampFlag) {
		this.stampFlag = stampFlag;
	}

	@Column(name = "STAMP_REF")
	public Integer getStampRef() {
		return stampRef;
	}
	public void setStampRef(Integer stampRef) {
		this.stampRef = stampRef;
	}
	
	@Column(name = "PRESTAMP_FILEURI")
	public String getPreStamp_FileUri() {
		return preStamp_FileUri;
	}

	public void setPreStamp_FileUri(String preStamp_FileUri) {
		this.preStamp_FileUri = preStamp_FileUri;
	}
    // END OF GETTER SETTER METHODS

    /**
     * returns the state holder object associated with this study
     */
    /*
     * public AppendixStateKeeper getAppendixStateKeeper() {
     * 
     * AppendixPK pk = (AppendixPK) context.getPrimaryKey();
     * 
     * int id = pk.id; AppendixStateKeeper ask = null;
     * 
     * Rlog.debug("Appendix", "CHECKING GET appendix KEEPER " + id);
     * 
     * try { ask = new AppendixStateKeeper(id, getAppendixType(),
     * getAppendixUrl_File(), getAppendixDescription(), getAppendixPubFlag(),
     * getAppendixStudy(), getAppendixStudyVer(), getAppendixSavedFileName(),
     * getFileSize(), getCreator(), getModifiedBy(), getIpAdd()); return ask; }
     * catch (Exception e) { System.out.println("EXCEPTION " + e);
     * Rlog.debug("Appendix", "EXCEPTION IN getAppendixStateKeeper()" + e); }
     * return ask; }
     */

	/**
     * sets up a state holder containing details of the study appendix
     */
    /*
     * public void setAppendixStateKeeper(AppendixStateKeeper ask) {
     * Rlog.debug("Appendix", "IN ENTITY BEAN - setAppendixStateKeeper");
     * 
     * GenerateId genId = null;
     * 
     * try {
     * 
     * Connection conn = null; conn = getConnection();
     * 
     * Rlog.debug("Appendix", "Connection :" + conn);
     * 
     * id = genId.getId("SEQ_ER_STUDYAPNDX", conn); conn.close();
     * 
     * Rlog.debug("Appendix", "GOT app ID" + id);
     * 
     * setAppendixStudy(ask.getAppendixStudy()); Rlog.debug("Appendix",
     * "appendix STUDY" + this.study);
     * 
     * setAppendixStudyVer(ask.getAppendixStudyVer()); Rlog.debug("Appendix",
     * "appendix STUDYVER" + this.studyVerId);
     * 
     * setAppendixType(ask.getAppendixType()); Rlog.debug("appendix",
     * this.type); setAppendixUrl_File(ask.getAppendixUrl_File());
     * Rlog.debug("appendix", this.url_file);
     * setAppendixPubFlag(ask.getAppendixPubFlag()); Rlog.debug("appendix",
     * this.pubflag); setAppendixDescription(ask.getAppendixDescription());
     * Rlog.debug("appendix", this.desc);
     * setAppendixSavedFileName(ask.getAppendixSavedFileName());
     * Rlog.debug("appendix", this.filename); setFileSize(ask.getFileSize());
     * setCreator(ask.getCreator()); setModifiedBy(ask.getModifiedBy());
     * setIpAdd(ask.getIpAdd()); } catch (Exception e) {
     * System.out.println("EXCEPTION " + e); Rlog.debug("Appendix", "EXCEPTION
     * IN CREATING NEW STUDY appendix " + e); } }
     */

    public int updateAppendix(AppendixBean ask) {
        try {
            setAppendixStudy(ask.getAppendixStudy());
            setAppendixStudyVer(ask.getAppendixStudyVer());
            setAppendixType(ask.getAppendixType());
            setAppendixUrl_File(ask.getAppendixUrl_File());
            setAppendixPubFlag(ask.getAppendixPubFlag());
            setAppendixDescription(ask.getAppendixDescription());
            setAppendixSavedFileName(ask.getAppendixSavedFileName());
            setFileSize(ask.getFileSize());
            setCreator(ask.getCreator());
            setModifiedBy(ask.getModifiedBy());
            setIpAdd(ask.getIpAdd());
            Rlog.debug("appendix", "AppendixBean.updateAppendix");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("appendix", " error in AppendixBean.updateAppendix");
            return -2;
        }
    }

    public AppendixBean() {
    }

    public AppendixBean(int id, String type, String url_file, String desc,
            String pubflag, String study, String studyVerId, String filename,
            String fileSize, String creator, String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setAppendixId(id);
        setAppendixType(type);
        this.setAppendixUrl_File(url_file);
        this.setAppendixDescription(desc);
        this.setAppendixPubFlag(pubflag);
        this.setAppendixStudy(study);
        this.setAppendixStudyVer(studyVerId);
        this.setAppendixSavedFileName(filename);
        this.setFileSize(fileSize);
        this.setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

}// end of class
