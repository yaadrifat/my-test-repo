/*
 * Classname			UserSiteDao
 * 
 * Version information	1.0
 *
 * Date					04/23/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.velos.eres.service.util.Rlog;

/**
 * UserSiteDao class
 * 
 * @author Sonika
 * @version 1.0 04/23/2003
 */

public class UserSiteDao extends CommonDAO implements java.io.Serializable {

    private ArrayList userSiteIds; // pk_usersite

    private ArrayList userSiteUserIds;

    private ArrayList userSiteSiteIds;// fk_site

    private ArrayList userSiteRights;

    private ArrayList userSiteNames;

    private ArrayList userSiteIndents;
    
    private ArrayList userSiteHidden;

    /*
     * userSiteParents - this is id of the main head parent site whose sub sites
     * have been formed eg if B is child of A and C is child of B then the main
     * parent is A for both
     */
    private ArrayList userSiteParents;
    
    private ArrayList userSiteAltIds;

    private int cRows;

    public UserSiteDao() {
        userSiteIds = new ArrayList();
        userSiteUserIds = new ArrayList();
        userSiteSiteIds = new ArrayList();
        userSiteRights = new ArrayList();
        userSiteNames = new ArrayList();
        userSiteHidden = new ArrayList();
        userSiteIndents = new ArrayList();
        userSiteParents = new ArrayList();
        userSiteAltIds= new ArrayList();
    }

    // Getter and Setter methods

    public ArrayList getUserSiteAltIds() {
		return userSiteAltIds;
	}

	public void setUserSiteAltIds(ArrayList userSiteAltIds) {
		this.userSiteAltIds = userSiteAltIds;
	}
	
	public void setUserSiteAltIds(String userSiteAltId) {
		this.userSiteAltIds.add(userSiteAltId);
	}

	public ArrayList getUserSiteIds() {
        return this.userSiteIds;
    }

    public void setUserSiteIds(ArrayList userSiteIds) {
        this.userSiteIds = userSiteIds;
    }

    public ArrayList getUserSiteNames() {
        return this.userSiteNames;
    }

    public void setUserSiteNames(ArrayList userSiteNames) {
        this.userSiteNames = userSiteNames;
    }

    public ArrayList getUserSiteHidden() {
		return userSiteHidden;
	}

	public void setUserSiteHidden(ArrayList userSiteHidden) {
		this.userSiteHidden = userSiteHidden;
	}
	
	public void setUserSiteHidden(String userSiteHidden) {
        this.userSiteHidden.add(userSiteHidden);
    }

	public ArrayList getUserSiteRights() {
        return this.userSiteRights;
    }

    public void setUserSiteRights(ArrayList userSiteRights) {
        this.userSiteRights = userSiteRights;
    }

    public ArrayList getUserSiteSiteIds() {
        return this.userSiteSiteIds;
    }

    public void setUserSiteSiteIds(ArrayList userSiteSiteIds) {
        this.userSiteSiteIds = userSiteSiteIds;
    }

    public ArrayList getUserSiteUserIds() {
        return this.userSiteUserIds;
    }

    public void setUserSiteUserIds(ArrayList userSiteUserIds) {
        this.userSiteUserIds = userSiteUserIds;
    }

    public ArrayList getUserSiteIndents() {
        return this.userSiteIndents;
    }

    public void setUserSiteIndents(ArrayList userSiteIndents) {
        this.userSiteIndents = userSiteIndents;
    }

    public ArrayList getUserSiteParents() {
        return this.userSiteParents;
    }

    public void setUserSiteParents(ArrayList userSiteParents) {
        this.userSiteParents = userSiteParents;
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public void setUserSiteIds(Integer siteId) {
        this.userSiteIds.add(siteId);
    }

    public void setUserSiteNames(String siteName) {
        this.userSiteNames.add(siteName);
    }

    public void setUserSiteRights(String siteRight) {
        this.userSiteRights.add(siteRight);
    }

    public void setUserSiteSiteIds(String siteSiteId) {
        this.userSiteSiteIds.add(siteSiteId);
    }

    public void setUserSiteUserIds(String siteUserId) {
        this.userSiteUserIds.add(siteUserId);
    }

    public void setUserSiteIndents(String siteUserIndent) {
        this.userSiteIndents.add(siteUserIndent);
    }

    public void setUserSiteParents(String userSiteParent) {
        this.userSiteParents.add(userSiteParent);
    }

    /**
     * To get the parent child hierarchy of organizations for an account user
     * calls Stored procedure
     * 
     * @param int
     *            accId - Account Id
     * @param int
     *            userId - User Id
     */
    public void getUserSiteTree(int accId, int userId) {
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call PKG_USER.SP_SITETREE(?,?,?)}");
            cstmt.setInt(1, accId);
            cstmt.setInt(2, userId);
            cstmt.registerOutParameter(3, OracleTypes.CURSOR);
            cstmt.execute();
            ResultSet rs = (ResultSet) cstmt.getObject(3);
            while (rs.next()) {
                setUserSiteIds(new Integer(rs.getInt("siteid")));
                setUserSiteSiteIds(rs.getString("usersiteid"));
                setUserSiteRights(rs.getString("siteright"));
                setUserSiteNames(rs.getString("sitename"));
                setUserSiteIndents(rs.getString("indent"));
                /*
                 * parentid is id of the main head parent site whose sub sites
                 * have been formed eg if B is child of A and C is child of B
                 * then the main parent is A for both
                 */
                setUserSiteParents(rs.getString("parentid"));
            }

            rs.close();

        } catch (Exception e) {
            Rlog.fatal("usersite",
                    "EXCEPTION in getUserSiteTree, excecuting Stored Procedure "
                            + e);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /**
     * To update the rights for all the organizations of an account user, large #
     * of rows are updated simultaneously by calling a stored procedure and
     * passing the rows to db through arrays
     * 
     * @param Array
     *            of pkUserSites
     * @param Array
     *            of corresponding site rights
     * @param Logged
     *            in user Id
     * @param IP
     *            of logged in user
     * @returns 0 for successful update and -1 for error
     */
    public int updateUserSite(String[] pkUserSites, String[] rights,
            int userId, String ipAdd) {
        int ret = 1;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            Rlog.debug("userSite", "In UserSiteDao updateUserSite() ");

            ArrayDescriptor inPkUserSites = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);

            ARRAY pkUserSitesArray = new ARRAY(inPkUserSites, conn, pkUserSites);

            ArrayDescriptor inRights = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY rightsArray = new ARRAY(inRights, conn, rights);

            cstmt = conn
                    .prepareCall("{call PKG_USER.SP_UPDATESITERIGHTS(?,?,?,?,?)}");
            cstmt.setArray(1, pkUserSitesArray);
            cstmt.setArray(2, rightsArray);
            cstmt.setInt(3, userId);
            cstmt.setString(4, ipAdd);
            cstmt.registerOutParameter(5, java.sql.Types.INTEGER);
            cstmt.execute();
            ret = cstmt.getInt(5);

            return ret;
        } catch (Exception e) {
            Rlog.fatal("usersite",
                    "EXCEPTION in updateUserSite, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }
    /**
     * To update the rights for all the organizations of an account user, large #
     * of rows are updated simultaneously by calling a stored procedure and
     * passing the rows to db through arrays
     * 
     * Overloaded
     * @param Array
     *            of pkUserSites
     * @param Array
     *            of corresponding site rights
     * @param Logged
     *            in user Id
     * @param IP
     *            of logged in user
     * @returns 0 for successful update and -1 for error
     */
    public int updateUserSite(String[] pkUserSites, String[] rights,
            int userId,int userID, String ipAdd) {
        int ret = 1;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            Rlog.debug("userSite", "In UserSiteDao updateUserSite() ");

            ArrayDescriptor inPkUserSites = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);

            ARRAY pkUserSitesArray = new ARRAY(inPkUserSites, conn, pkUserSites);

            ArrayDescriptor inRights = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY rightsArray = new ARRAY(inRights, conn, rights);

            cstmt = conn
                    .prepareCall("{call PKG_USER.SP_UPDATESITERIGHTS(?,?,?,?,?,?)}");
            cstmt.setArray(1, pkUserSitesArray);
            cstmt.setArray(2, rightsArray);
            cstmt.setInt(3, userId);
            cstmt.setInt(4, userID);
            cstmt.setString(5, ipAdd);
            cstmt.registerOutParameter(6, java.sql.Types.INTEGER);
            cstmt.execute();
            ret = cstmt.getInt(6);

            return ret;
        } catch (Exception e) {
            Rlog.fatal("usersite",
                    "EXCEPTION in updateUserSite, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /**
     * Gets all sites with New right for an account user
     * 
     * @param Account
     *            Id
     * @param User
     *            Id
     */
    public void getSitesWithNewRight(int accId, int userId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("select us.pk_site, us.site_name, NVL(s.site_hidden,0) site_hidden from erv_usersites us, er_site s where us.pk_site = s.pk_site and  us.fk_account = ? and us.fk_user = ? and (usersite_right = 5 or usersite_right = 7) order by  us.SITE_NAME ");
            pstmt.setInt(1, accId);
            pstmt.setInt(2, userId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUserSiteSiteIds(rs.getString("pk_site"));
                setUserSiteNames(rs.getString("site_name"));
                setUserSiteHidden(rs.getString("site_hidden"));;
            }

        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "codelst",
                            "EXCEPTION IN FETCHING FROM ERV_USERSITE in getSitesWithNewRight in UserSiteDao "
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
    
    public void getSitesWithNewRightRestricted(int accId, int userId) {
        int rows = 0;
        PreparedStatement pstmt = null; 
        Connection conn = null;

        try {
        	this.reset();
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("select pk_site, site_name,site_altid from erv_usersites where fk_account = ? and fk_user = ? and (usersite_right = 5 or usersite_right = 7)" +
                    		" and pk_site in (select pk_site from er_site where (site_restrict!='Y') or (site_restrict is null)) order by  SITE_NAME ");
            pstmt.setInt(1, accId);
            pstmt.setInt(2, userId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUserSiteSiteIds(rs.getString("pk_site"));
                setUserSiteNames(rs.getString("site_name"));
                this.setUserSiteAltIds(rs.getString("site_altid")==null?"":rs.getString("site_altid"));
            }

        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "codelst",
                            "EXCEPTION IN FETCHING FROM ERV_USERSITE in getSitesWithNewRight in UserSiteDao "
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }



    /**
     * Get organizations with view access for an account user (right>=4)
     * 
     * @param int
     *            accId - Account Id
     * @param int
     *            userId - User Id
     */
    public void getSitesWithViewRight(int accId, int userId) {
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("select us.pk_usersite,us.pk_site,us.site_name "
                            + "from erv_usersites us, er_site s "
                            + "where us.pk_site = s.pk_site and us.fk_account=? and us.fk_user=? and usersite_right>=4 and s.site_hidden<>1 "
                            + " order by lower(us.site_name)");
            pstmt.setInt(1, accId);
            pstmt.setInt(2, userId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setUserSiteIds(new Integer(rs.getInt("pk_usersite")));
                setUserSiteSiteIds(rs.getString("pk_site"));
                setUserSiteNames(rs.getString("site_name"));
            }

            rs.close();
        } catch (Exception e) {
            Rlog.fatal("usersite", "EXCEPTION in getSitesWithViewRight " + e);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    // ------------

    /**
     * Calls pkg_user.SP_CREATE_USERSITE_DATA() * to create er_usersite data for
     * all the organizations(sites) for user's account * The default site gets
     * rights same as user's default group's Manage Patients rights * Author:
     * Sonia Sahni 12th May 2003 *
     * 
     * @param user :
     *            Pk_user *
     * @param account :
     *            pk of user account *
     * @param def_group :
     *            pk of user default group *
     * @param old_site :
     *            pk of previous user site *
     * @param def_site :
     *            pk of user site *
     * @param creator :
     *            pk of creator (fk_user) *
     * @paramip : ip add of client machine * returns : 0 for successful update,
     *          -1 for error
     */

    public int createUserSiteData(int user, int account, int def_group,
            int old_site, int def_site, int creator, String ip, String mode) {
        CallableStatement cstmt = null;
        Connection conn = null;
        int ret = 0;

        Rlog.debug("usersite", "*****parameters***" + user + "*" + account
                + "*" + def_group + "* old site " + old_site + " *new site "
                + def_site + "*" + creator + "*" + ip + "*");

        try {
            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_USER.SP_CREATE_USERSITE_DATA(?,?,?,?,?,?,?,?,?)}");
            cstmt.setInt(1, user);
            cstmt.setInt(2, account);
            cstmt.setInt(3, def_group);
            cstmt.setInt(4, old_site);
            cstmt.setInt(5, def_site);
            cstmt.setInt(6, creator);
            cstmt.setString(7, ip);
            cstmt.setString(8, mode);

            cstmt.registerOutParameter(9, java.sql.Types.INTEGER);

            cstmt.execute();

            Rlog.debug("usersite",
                    "*****in createUserSiteData after procedure***");

            ret = cstmt.getInt(9);

            Rlog.debug("usersite",
                    "*****in createUserSiteData after procedure ret val***"
                            + ret);
            return ret;

        } catch (Exception e) {
            Rlog.fatal("usersite",
                    "EXCEPTION in SP_CREATE_USERSITE_DATA, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    // ------------

    /**
     * Get all organizations with view access in user profile and in any of the
     * study
     * 
     * @param int
     *            accId - Account Id
     * @param int
     *            userId - User Id
     */
    public void getAllSitesWithViewRight(int accId, int userId) {
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("select pk_site,site_name, lower(site_name) sn "
                            + " from erv_usersites "
                            + " where fk_account= ?  "
                            + " and fk_user = ? "
                            + " and usersite_right>=4 "
                            + " union "
                            + " select distinct r.fk_site, "
                            + " (select site_name from er_site where pk_site = r.fk_site) site_name, "
                            + " (select lower(site_name) from er_site where pk_site = r.fk_site) sn  "
                            + " from er_study_site_rights  r , er_study s  "
                            + " where "
                            + " fk_study = pk_study "
                            + " and s.fk_account = ? "
                            + " and fk_user = ? "
                            + " and USER_STUDY_SITE_RIGHTS = 1  "
                            + "  order by sn ");

            pstmt.setInt(1, accId);
            pstmt.setInt(2, userId);
            pstmt.setInt(3, accId);
            pstmt.setInt(4, userId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setUserSiteSiteIds(rs.getString("pk_site"));
                setUserSiteNames(rs.getString("site_name"));
            }

            rs.close();
        } catch (Exception e) {
            Rlog
                    .fatal("usersite", "EXCEPTION in getAllSitesWithViewRight "
                            + e);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    // ///
    /**
     * Get all organizations with access rights given for a study
     * 
     * @param int
     *            studyId - Study Id
     * @param int
     *            accId - Account Id
     * @param int
     *            userId - User Id
     *            @deprecated
     */
    public void getSitesWithRightForStudy(int studyId, int accId, int userId) {
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            /*
             * pstmt = conn.prepareStatement("select distinct r.fk_site, " + "
             * (select site_name from er_site where pk_site = r.fk_site)
             * site_name " + " from er_study_site_rights r , er_study s ,
             * er_usersite s " + " where " + " r.fk_site = s.fk_site and " + "
             * r.fk_user = s.fk_user and " + " USERSITE_RIGHT >=4 and " + "
             * fk_study = pk_study " + " and pk_study = ? " + " and s.fk_account = ? " + "
             * and r.fk_user = ? " +" and r.fk_site in( select fk_site from
             * er_studysites where fk_study = ?) " + " and
             * USER_STUDY_SITE_RIGHTS =1 " + " union " + " select u.fk_site, " + "
             * (select site_name from er_site where pk_site = u.fk_site)
             * site_name " + " from er_study_site_rights r ,er_studyteam s,
             * er_usersite u " + " where s.fk_user = u.fk_user " + " and
             * r.fk_site = u.fk_site " + " and r.fk_study = s.fk_study " + " and
             * r.fk_user = s.fk_user " + " and r.fk_user = u.fk_user " + " and
             * USERSITE_RIGHT >=4 " + " and s.fk_study = ? " + " and u.fk_site
             * in( select fk_site from er_studysites where fk_study = ?) " + "
             * and nvl(STUDY_TEAM_USR_TYPE,'Y')='S'");
             */
            pstmt = conn
                    .prepareStatement("select distinct r.fk_site, "
                            + "  (select site_name from er_site where pk_site = r.fk_site) site_name  "
                            + " from er_study_site_rights  r , er_study s , er_usersite s "
                            + " where  "
                            + " r.fk_site = s.fk_site and   "
                            + " r.fk_user = s.fk_user and  "
                            + " USERSITE_RIGHT >=4 and  "
                            + " fk_study = pk_study  "
                            + " and pk_study = ? "
                            + " and s.fk_account = ? "
                            + " and r.fk_user = ? "
                            + " and r.fk_site in( select fk_site from er_studysites where fk_study = ?) "
                            + " and USER_STUDY_SITE_RIGHTS =1 "
                            + " union "
                            + " select u.fk_site, "
                            + " (select site_name from er_site where pk_site = u.fk_site) site_name  "
                            + " from er_study_site_rights  r , er_studyteam s, er_usersite u 		"
                            + " where s.fk_user = u.fk_user "
                            + " and  r.fk_site = u.fk_site "
                            + " and r.fk_study = s.fk_study "
                            + " and r.fk_user = s.fk_user "
                            + " and r.fk_user = u.fk_user and r.fk_user = ? "
                            + " and USERSITE_RIGHT >=4   "
                            + " and s.fk_study = ? "
                            + " and u.fk_site in( select fk_site from er_studysites where fk_study = ?) "
                            + " and USER_STUDY_SITE_RIGHTS =1   "
                            + " and nvl(STUDY_TEAM_USR_TYPE,'Y')='S' ");

            pstmt.setInt(1, studyId);
            pstmt.setInt(2, accId);
            pstmt.setInt(3, userId);
            pstmt.setInt(4, studyId);
            pstmt.setInt(5, studyId);
            pstmt.setInt(6, studyId);
            pstmt.setInt(7, userId);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setUserSiteSiteIds(rs.getString("fk_site"));
                setUserSiteNames(rs.getString("site_name"));
            }

            rs.close();
        } catch (Exception e) {
            Rlog.fatal("usersite", "EXCEPTION in getSitesWithRightForStudy "
                    + e);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    // ///
    /**
     * @param int
     *            studyId - Study Id
     * @param int
     *            accId - Account Id
     * @param int
     *            userId - User Id
     */
    
    /* modified by sonia for revised super user implementation*/
    
    public void getSitesToEnrollPat(int studyId, int accId, int userId) {
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("select distinct r.fk_site, "
                            + " (select site_name from er_site where pk_site = r.fk_site) site_name,lower(site_name) sn "
                            + " from er_studysites s, er_study_site_rights r, erv_usersites u "
                            + " where s.fk_study = r.fk_study "
                            + " and u.fk_user  = r.fk_user "
                            + " and r.fk_site = u.pk_site "
                            + " and s.fk_site = u.pk_site	"
                            + " and s.fk_study = ? "
                            + " and fk_account = ? "
                            + " and u.fk_user = ? "
                            + " and USER_STUDY_SITE_RIGHTS = 1 "
                            + " and USERSITE_RIGHT  >= 4 	"
                            + " union "
                            + " select u.pk_site, "
                            + " (select site_name from er_site where pk_site=u.pk_site ) as site_name,lower(site_name) sn "
                            + " from erv_usersites u, er_studysites s "
                            + " where  s.fk_study = ? and pkg_superuser.F_Is_Superuser(? , ?) = 1 and u.fk_user = ?  and u.pk_site = s.fk_site  "
                             + " and USERSITE_RIGHT >= 4 and not exists (select * from er_studyteam tt where tt.fk_study = ? and tt.fk_user=? and study_team_usr_type='D')	"
                            + " and fk_account= ? "
                            + " order by sn ");

            pstmt.setInt(1, studyId);
            pstmt.setInt(2, accId);
            pstmt.setInt(3, userId);

            pstmt.setInt(4, studyId);
            pstmt.setInt(5, userId);
            pstmt.setInt(6, studyId);
            pstmt.setInt(7, userId);
            
            pstmt.setInt(8, studyId);
            pstmt.setInt(9, userId);
             
            pstmt.setInt(10, accId);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setUserSiteSiteIds(rs.getString("fk_site"));
                setUserSiteNames(rs.getString("site_name"));
            }

            rs.close();
        } catch (Exception e) {
            Rlog.fatal("usersite", "EXCEPTION in getSitesToEnrollPat " + e);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }
    
    //////////////////
    
    /**
     *  Function to get user's right in  patient's registering sites for a given study patient enrollments
     *  
     * @param int
     *            userId - User Id
     * @param int
     *            patprot -patprot id
     */
    
    public int getMaxRightForStudyPatient(int user, int pat, int study) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int right = 0;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement("select pkg_user.f_chk_studyright_using_pat(?,?,?) accRight from dual ");
            
            
            pstmt.setInt(1, pat);
            pstmt.setInt(2, study);
            pstmt.setInt(3, user);
            
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	right = rs.getInt("accRight");
                
            }
            if (rs!=null)
            {
            	rs.close();
            }	
            return right;
        } catch (Exception e) {
            Rlog.fatal("usersite", "EXCEPTION in getMaxRightForStudyPatient "
                            + e);
            return right;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }
    
/**JM: 032406: method to find all the sites for an user account without checking any access rights
 * @Param int
 * 			  Account Id	
 */  
    
public void getAllSitesInAccount(int accId){
	PreparedStatement ps = null;
	Connection conn = null;
	try{
		conn = getConnection();
		ps= conn.prepareStatement("select pk_site, site_name from ER_SITE where fk_account = ? and SITE_HIDDEN <> 1 order by  SITE_NAME " );		
		
		ps.setInt(1,accId);		
		
		ResultSet rs = ps.executeQuery();
		
		while (rs.next()){
			 setUserSiteSiteIds(rs.getString("pk_site"));
             setUserSiteNames(rs.getString("site_name"));
		}
		rs.close();
		
	}
	catch(Exception e){
		Rlog.fatal("usersite", "EXCEPTION in getAllSitesInAccount.. " + e);
	}
	finally{
		try{
			if(ps!=null)	
			ps.close();			
		}
		catch(Exception e){
		
		}
		try{
			if(conn!=null)
			conn.close();	
		}
		catch(Exception e){
	
		}
	}	
}

public void reset()
{
	userSiteSiteIds.clear();
	userSiteNames.clear();
}

} // end of main class

