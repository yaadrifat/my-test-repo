/*
 * Classname : LkpFilterDao
 *
 * Version information: 1.0
 *
 * Date: 02/16/2011
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Isaac Huang
 */

package com.velos.eres.business.common;

/* IMPORT STATEMENTS */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */
public class LkpFilterDao extends CommonDAO implements java.io.Serializable {

    /**
     * Generated serialVersionUID
     */
    private static final long serialVersionUID = -8573075170891879787L;
    private ArrayList<Integer> pkLkpFilter;
    private ArrayList<String> lkpFilterKey;
    private ArrayList<String> lkpFilterSQL;
    private ArrayList<String> lkpFilterParamType;

    public LkpFilterDao () {
        pkLkpFilter =  new ArrayList<Integer>();
        lkpFilterKey = new ArrayList<String>();
        lkpFilterSQL = new ArrayList<String>();
        lkpFilterParamType = new ArrayList<String>();
    }

    //Then the getter and setter methods goes here
    public ArrayList<Integer> getPkLkpFilter() {
        return this.pkLkpFilter;
    }
    private void setPkLkpFilter(Integer pkLkpFilter) {
        this.pkLkpFilter.add(pkLkpFilter);
    }

    public ArrayList<String> getLkpFilterKey() {
        return this.lkpFilterKey;
    }
    private void setLkpFilterKey(String lkpFilterKey) {
        this.lkpFilterKey.add(lkpFilterKey);
    }

    public ArrayList<String> getLkpFilterSQL() {
        return this.lkpFilterSQL;
    }
    private void setLkpFilterSQL(String lkpFilterSQL) {
        this.lkpFilterSQL.add(lkpFilterSQL);
    }

    public ArrayList<String> getLkpFilterParamType() {
        return this.lkpFilterParamType;
    }
    public void setLkpFilterParamType(String lkpFilterParamType) {
        this.lkpFilterParamType.add(lkpFilterParamType);
    }
    //end of getter and setter methods

    public void getLkpFilterData() {
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            pstmt = conn.prepareStatement("select PK_LKPFILTER, LKPFILTER_KEY, LKPFILTER_SQL, "
                    + " LKPFILTER_PARAM_TYPE from ER_LKPFILTER ");
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("LkpFilter", "LkpFilterDao.getLkpFilterData... ");
            while (rs.next()) {
                setPkLkpFilter(rs.getInt("PK_LKPFILTER"));
                setLkpFilterKey(rs.getString("LKPFILTER_KEY"));
                setLkpFilterSQL(rs.getString("LKPFILTER_SQL"));
                setLkpFilterParamType(rs.getString("LKPFILTER_PARAM_TYPE"));
            }
        } catch(SQLException ex){
            Rlog.fatal("LkpFilter",
                    "LkpFilterDao.getLkpFilterData EXCEPTION IN FETCHING FROM ER_LkpFilter table... "
                    + ex);
        } finally {
            try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
            try { if (conn != null) { conn.close(); } } catch (Exception e) {}
        }
    }
}
