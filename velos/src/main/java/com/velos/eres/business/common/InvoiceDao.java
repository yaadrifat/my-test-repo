

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.velos.eres.service.util.BrowserRows;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * InvoiceDao - data Access class for Invoice module
 *
 * @author Sonia Abrol
 * @created 11/04/2005
 */
public class InvoiceDao extends CommonDAO implements java.io.Serializable {

	 /**
	 *
	 */
	private static final long serialVersionUID = 7212503437124169558L;

	/**
	 * the Invoice Primary Key
	 */
	private ArrayList <String> id;

	/**
	 * the Invoice Number
	 */
	private ArrayList <String> invNumber;

	/**
	 * the Invoice addressed to
	 */
	private ArrayList <String> invAddressedto;

	/**
	 * the Invoice sent from
	 */
	private ArrayList <String> invSentFrom;

	/**
	 * the invoice header
	 */
	private ArrayList <String> invHeader;

	/**
	 * the invoice footer
	 */
	private ArrayList <String> invFooter;

	/**
	 * Milestone rule status type
	 */
	private ArrayList <String> invMileStatusType;

	/**
	 * the invoice interval type
	 */
	private ArrayList <String> invIntervalType;

	/**
	 * Invoice interval from
	 */
	private ArrayList <String> invIntervalFrom;

	/**
	 * Invoice interval To
	 */
	private ArrayList  <String> invIntervalTo;

	/**
	 * Invoice Date
	 */
	private ArrayList <String> invDate;

	/**
	 * the invoice Notes
	 */
	private ArrayList <String> invNotes ;

	/**
	 * the invoice Other user
	 */
	private ArrayList <String> invOtherUser;



	private ArrayList  <String> invPayDueBy;

	private ArrayList <String> invPayUnit;

	private ArrayList <String> creator;
	
	private ArrayList <String> userData;

	private ArrayList <String> invStats;

	private ArrayList <String> historyIds;

	private ArrayList <String> subTypes;
	
	private ArrayList <String> covType;
	
	private ArrayList <String> amtInv;
	
	private Map<String,Object> rowsCount;
	

    public InvoiceDao() {
		super();
		// TODO Auto-generated constructor stub
		this.id = new ArrayList <String>();
		invAddressedto = new ArrayList<String>();
		invDate = new ArrayList <String>();
		invFooter = new ArrayList<String>();
		invHeader = new ArrayList <String>();
		invIntervalFrom = new ArrayList <String>();
		invIntervalTo = new ArrayList <String>();
		invIntervalType = new ArrayList <String>();
		invMileStatusType = new ArrayList <String>();
		invNotes = new ArrayList<String>();
		invNumber = new ArrayList <String>();
		invOtherUser = new ArrayList <String>();
		invPayDueBy = new ArrayList <String>();
		invPayUnit = new ArrayList<String>();
		invSentFrom = new ArrayList<String>();
		creator = new ArrayList<String>();
		userData = new ArrayList<String>();
		invStats = new ArrayList<String>();
		historyIds = new ArrayList<String>();
		subTypes = new ArrayList<String>();
		covType = new ArrayList<String>();
		amtInv = new ArrayList<String>();
		rowsCount = new HashMap<String,Object>();

	}



    // end of getter and setter methods

    public ArrayList<String> getCreator() {
		return creator;
	}



	public void setCreator(ArrayList<String> creator) {
		this.creator = creator;
	}

	public void setCreator(String creator) {
		this.creator.add(creator);
	}

	
	   public ArrayList<String> getUserData() {
			return userData;
		}



		public void setUserData(ArrayList<String> userData) {
			this.userData = userData;
		}

		public void setUserData(String userData) {
			this.userData.add(userData);
		}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}



	public ArrayList<String> getId() {
		return id;
	}



	public void setId(ArrayList<String> id) {
		this.id = id;
	}

	public void setId(String id) {
		this.id.add(id);
	}

	public ArrayList<String> getInvAddressedto() {
		return invAddressedto;
	}



	public void setInvAddressedto(ArrayList<String> invAddressedto) {
		this.invAddressedto = invAddressedto;
	}

	public void setInvAddressedto(String invAddressedto) {
		this.invAddressedto.add(invAddressedto);
	}


	public ArrayList<String> getInvDate() {
		return invDate;
	}



	public void setInvDate(ArrayList<String> invDate) {
		this.invDate = invDate;
	}

	public void setInvDate(String invDate) {
		this.invDate.add(invDate);
	}


	public ArrayList<String> getInvFooter() {
		return invFooter;
	}



	public void setInvFooter(ArrayList<String> invFooter) {
		this.invFooter = invFooter;
	}

	public void setInvFooter(String invFooter) {
		this.invFooter.add(invFooter);
	}


	public ArrayList<String> getInvHeader() {
		return invHeader;
	}



	public void setInvHeader(ArrayList<String> invHeader) {
		this.invHeader = invHeader;
	}

	public void setInvHeader(String invHeader) {
		this.invHeader.add(invHeader);
	}


	public ArrayList<String> getInvIntervalFrom() {
		return invIntervalFrom;
	}



	public void setInvIntervalFrom(ArrayList<String> invIntervalFrom) {
		this.invIntervalFrom = invIntervalFrom;
	}



	public void setInvIntervalFrom(String invIntervalFrom) {
		this.invIntervalFrom.add(invIntervalFrom);
	}


	public ArrayList<String> getInvIntervalTo() {
		return invIntervalTo;
	}



	public void setInvIntervalTo(ArrayList<String> invIntervalTo) {
		this.invIntervalTo = invIntervalTo;
	}

	public void setInvIntervalTo(String invIntervalTo) {
		this.invIntervalTo.add(invIntervalTo);
	}

	public ArrayList<String> getInvIntervalType() {
		return invIntervalType;
	}



	public void setInvIntervalType(ArrayList<String> invIntervalType) {
		this.invIntervalType = invIntervalType;
	}


	public void setInvIntervalType(String invIntervalType) {
		this.invIntervalType.add(invIntervalType);
	}

	public ArrayList<String> getInvMileStatusType() {
		return invMileStatusType;
	}



	public void setInvMileStatusType(ArrayList<String> invMileStatusType) {
		this.invMileStatusType = invMileStatusType;
	}


	public void setInvMileStatusType(String invMileStatusType) {
		this.invMileStatusType.add(invMileStatusType);
	}


	public ArrayList<String> getInvNotes() {
		return invNotes;
	}



	public void setInvNotes(ArrayList<String> invNotes) {
		this.invNotes = invNotes;
	}


	public void setInvNotes(String invNotes) {
		this.invNotes.add(invNotes);
	}

	public ArrayList<String> getInvNumber() {
		return invNumber;
	}



	public void setInvNumber(ArrayList<String> invNumber) {
		this.invNumber = invNumber;
	}

	public void setInvNumber(String invNumber) {
		this.invNumber.add(invNumber);
	}


	public ArrayList<String> getInvOtherUser() {
		return invOtherUser;
	}



	public void setInvOtherUser(ArrayList<String> invOtherUser) {
		this.invOtherUser = invOtherUser;
	}

	public void setInvOtherUser(String invOtherUser) {
		this.invOtherUser.add(invOtherUser);
	}


	public ArrayList<String> getInvPayDueBy() {
		return invPayDueBy;
	}



	public void setInvPayDueBy(ArrayList<String> invPayDueBy) {
		this.invPayDueBy = invPayDueBy;
	}

	public void setInvPayDueBy(String invPayDueBy) {
		this.invPayDueBy.add(invPayDueBy);
	}

	public ArrayList<String> getInvPayUnit() {
		return invPayUnit;
	}



	public void setInvPayUnit(ArrayList<String> invPayUnit) {
		this.invPayUnit = invPayUnit;
	}

	public void setInvPayUnit(String invPayUnit) {
		this.invPayUnit.add(invPayUnit);
	}


	public ArrayList<String> getInvSentFrom() {
		return invSentFrom;
	}



	public void setInvSentFrom(ArrayList<String> invSentFrom) {
		this.invSentFrom = invSentFrom;
	}

	public void setInvSentFrom(String invSentFrom) {
		this.invSentFrom.add(invSentFrom);
	}

	//JM: invStats and historyIds
	public ArrayList<String> getInvStats() {
    	return this.invStats;
    }

    public void setInvStats(ArrayList<String> invStats) {
        this.invStats = invStats;
    }

    public void setInvStats(String invStat) {
        this.invStats.add(invStat);
    }


    public ArrayList<String> getHistoryIds() {
    	return this.historyIds;
    }

    public void setHistoryIds(ArrayList<String> historyIds) {
        this.historyIds = historyIds;
    }

    public void setHistoryIds(String historyId) {
        this.historyIds.add(historyId);
    }



    public ArrayList<String> getSubTypes() {
    	return this.subTypes;
    }

    public void setSubTypes(ArrayList<String> subTypes) {
        this.subTypes = subTypes;
    }

    public void setSubTypes(String subType) {
        this.subTypes.add(subType);
    }
    
	/**
	 * @param covType the covType to set
	 */
	public void setCovType(String covType) {
		this.covType.add(covType);
	}
	/**
	 * @return the covType
	 */
	public ArrayList <String> getCovType() {
		return covType;
	}
	
	
	 public ArrayList<String> getAmtInv() {
		return amtInv;
	}

	public void setAmtInv(String amtInv) {
		this.amtInv.add(amtInv);
	}
	
	/**
	 * @param getRowsCount the getRowsCount to set
	 */
	public void setRowsCount(Map<String,Object> rowsCount) {
		this.rowsCount = rowsCount;
	}



	/**
	 * @return the getRowsCount
	 */
	public Map<String,Object> getRowsCount() {
		return rowsCount;
	}
	
	
	/**
     * Gets the saved invoices for a study
     *
     * @param accountId
     *            Description of the Parameter
     */
    public void getSavedInvoices(int studyId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            //JM: 21Feb2008: changed for #Fin11: Feb2008 Enhancements
            pstmt = conn.prepareStatement(" select pk_invoice,COVERAGE_TYPE_CRITERIA, inv_number,to_char(inv_date,PKG_DATEUTIL.F_GET_DATEFORMAT) inv_date, "
            		+ " usr_lst(inv.creator) creator,inv_notes,to_char(inv_intervalfrom,PKG_DATEUTIL.F_GET_DATEFORMAT)inv_intervalfrom, "
            		+ " to_char(inv_intervalto,PKG_DATEUTIL.F_GET_DATEFORMAT) inv_intervalto, "
            		+ " (select codelst_desc from er_codelst where pk_codelst = FK_CODELST_STAT) as invoice_Status, "
            		+ " pk_status,  CODELST_SUBTYP from er_invoice inv, "
            		+ " (Select CODELST_DESC, CODELST_SUBTYP, PK_STATUS, STATUS_MODPK, FK_CODELST_STAT from er_invoice i, "
            		+ " ER_STATUS_HISTORY, er_codelst cd Where i.fk_study = ? and i.pk_invoice = STATUS_MODPK "
            		+ " and STATUS_MODTABLE = 'er_invoice' and STATUS_END_DATE is null and cd.pk_codelst = FK_CODELST_STAT ) stat "
            		+ " where fk_study = ? "
            		+ " and  status_modpk(+) = pk_invoice "
            		+ " order by to_date(inv_date,PKG_DATEUTIL.F_GET_DATEFORMAT) desc");

            pstmt.setInt(1, studyId);
            pstmt.setInt(2, studyId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setId( rs.getString("PK_INVOICE"));
                this.setInvNumber( rs.getString("inv_number"));
                this.setInvDate(rs.getString("inv_date"));
                this.setCreator( rs.getString("creator"));
                this.setInvIntervalFrom(rs.getString("inv_intervalfrom"));
                this.setInvIntervalTo(rs.getString("inv_intervalto"));
                this.setInvNotes(rs.getString("inv_notes"));
                //JM: 21Feb2008
                this.setInvStats(rs.getString("invoice_Status"));
                this.setHistoryIds(rs.getString("pk_status"));
                this.setSubTypes(rs.getString("CODELST_SUBTYP"));
                this.setCovType(rs.getString("COVERAGE_TYPE_CRITERIA"));


                Rlog.debug("invoice", "InvoiceDao.getSavedInvoices Invoices count"
                        + id.size());

            }


        } catch (Exception ex) {
            Rlog.fatal("invoice",
                    "InvoiceDao.getSavedInvoices EXCEPTION IN FETCHING FROM er_invoice table"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }
    
    /**
     * Gets Saved Invoices and parameters to be used to apply pagination in invoice browser
     * Added By : Raviesh
     */
    public void getSavedInvoicesWithPagination(int studyId,String orderBy,String orderType,Map<String,Object> rowsCount,int curPage) {

        StringBuffer sbcountSQL = new StringBuffer();
        StringBuffer sbinvBrSQL = new StringBuffer();
        String orderByIntervalFrom = null;
        String orderByIntervalTo = null;
        long rowsPerPage=0;
    	long totalPages=0;	
 	    long rowsReturned = 0;
 	    long showPages = 0;
 	   
 	    boolean hasMore = false;
 	    boolean hasPrevious = false;
 	    long firstRec = 0;
 	    long lastRec = 0;	   
 	    long totalRows = 0;     	 
 	    long startPage = 1;     	 
        try {
            sbinvBrSQL.append("SELECT distinct pk_invoice,COVERAGE_TYPE_CRITERIA,inv_number,inv_date as invdate,TO_CHAR(inv_date,PKG_DATEUTIL.F_GET_DATEFORMAT) inv_date,usr_lst(inv.creator) creator,(SELECT er_user.usr_firstname||' '||er_user.USR_MIDNAME||' '||er_user.usr_lastname||' '||er_add.add_email||' '||er_user.USR_LOGNAME FROM er_user inner join ER_ADD on er_user.FK_PERADD = ER_ADD.PK_ADD  WHERE er_user.pk_user=inv.creator) userData,inv_notes,TO_CHAR(inv_intervalfrom,PKG_DATEUTIL.F_GET_DATEFORMAT)inv_intervalfrom,TO_CHAR(inv_intervalto,PKG_DATEUTIL.F_GET_DATEFORMAT) inv_intervalto,");
            sbinvBrSQL.append("(SELECT codelst_desc FROM er_codelst WHERE pk_codelst = FK_CODELST_STAT) AS invoice_Status,INV_PAYUNIT,TO_NUMBER(INV_PAYMENT_DUEBY) INV_PAYMENT_DUEDT,");
             sbinvBrSQL.append("pk_status,(select sum(NVL(amount_invoiced, 0) + nvl(amount_holdback,0)) amount_invoiced from er_invoice_detail where fk_inv = pk_invoice and detail_type = 'H') as amount_invoiced,CODELST_SUBTYP FROM er_invoice inv,er_invoice_detail invdet,");
             sbinvBrSQL.append("(SELECT CODELST_DESC,CODELST_SUBTYP,PK_STATUS,STATUS_MODPK,FK_CODELST_STAT FROM er_invoice i,ER_STATUS_HISTORY,er_codelst cd");
             sbinvBrSQL.append(" WHERE i.fk_study = "+studyId+" AND i.pk_invoice = STATUS_MODPK AND STATUS_MODTABLE  = 'er_invoice' AND STATUS_END_DATE IS NULL AND cd.pk_codelst    = FK_CODELST_STAT) stat");
             sbinvBrSQL.append(" WHERE inv.fk_study = "+studyId+" AND invdet.fk_study = "+studyId+" AND invdet.DETAIL_TYPE='H' AND invdet.fk_inv = pk_invoice AND status_modpk(+)   = pk_invoice");
             
             if(orderBy.equalsIgnoreCase("invdate")){
              	orderBy = "invdate";
             }else if(orderBy.equalsIgnoreCase("inv_number")){
             	orderBy = "inv_number";
             }else if(orderBy.equalsIgnoreCase("amount_invoiced")){
             	orderBy = "amount_invoiced";
             }else if(orderBy.equalsIgnoreCase("creator")){
             	orderBy = "creator";
             }else if(orderBy.equalsIgnoreCase("userData")){
              	orderBy = "userData";
             }else if(orderBy.equalsIgnoreCase("invoice_Status")){
             	orderBy = "invoice_Status";
             }else if(orderBy.equalsIgnoreCase("inv_notes")){
             	orderBy = "inv_notes";
             }else if(orderBy.equalsIgnoreCase("inv_intervalfrom")){
             	orderByIntervalFrom = "inv_intervalfrom";
             	orderByIntervalTo = "inv_intervalto";
             }
             
          	if(orderBy!=null){
         		if(!orderBy.equalsIgnoreCase("inv_intervalfrom")){
         			if(orderBy.equalsIgnoreCase("amount_invoiced") || orderBy.equalsIgnoreCase("invdate") || orderBy.equalsIgnoreCase("inv_number"))
         				sbinvBrSQL.append( " order by " +  orderBy + " " + orderType);
         			else if(orderBy.equalsIgnoreCase("Payment_DueDt"))
         				sbinvBrSQL.append( " order by INV_PAYUNIT "+orderType+" ,"+"INV_PAYMENT_DUEDT "+orderType+" ,"+"invdate"+ " " + orderType);
         			else
         				sbinvBrSQL.append( " order by lower(" +  orderBy + ") " + orderType);
         		}else if(orderBy.equalsIgnoreCase("inv_intervalfrom")){
         			sbinvBrSQL.append( " order by lower(" +  orderByIntervalFrom + ") " + orderType + ", lower(" + orderByIntervalTo + ")" + orderType);             	
         		}
     		}
         	
         	//Query used to retrieve invoice data to be sent to getPageRows() method of BrowserRows
         	String invSql = sbinvBrSQL.toString();
         	
        	sbcountSQL.append("SELECT count(distinct pk_invoice) FROM er_invoice inv,er_invoice_detail invdet,");
        	sbcountSQL.append("(SELECT CODELST_DESC,CODELST_SUBTYP,PK_STATUS,STATUS_MODPK,FK_CODELST_STAT FROM er_invoice i,ER_STATUS_HISTORY,er_codelst cd");
        	sbcountSQL.append(" WHERE i.fk_study = "+studyId+" AND i.pk_invoice = STATUS_MODPK AND STATUS_MODTABLE  = 'er_invoice' AND STATUS_END_DATE IS NULL AND cd.pk_codelst    = FK_CODELST_STAT) stat");
        	sbcountSQL.append(" WHERE inv.fk_study = "+studyId+" AND invdet.fk_study = "+studyId+" AND invdet.DETAIL_TYPE='H' AND invdet.fk_inv = pk_invoice AND status_modpk(+)   = pk_invoice");
        	
        	//Query used to count retrieved invoice data to be sent to getPageRows() method of BrowserRows
        	String countSql = sbcountSQL.toString();
        	
     		rowsPerPage =  Configuration.ROWSPERBROWSERPAGE ;
    		totalPages = Configuration.PAGEPERBROWSER ;
    		
           BrowserRows br = new BrowserRows();
           
           br.getPageRows(curPage,rowsPerPage,invSql,totalPages,countSql,"",null);
           rowsReturned = br.getRowReturned();
     	   showPages = br.getShowPages();
     	   startPage = br.getStartPage();
     	   hasMore = br.getHasMore();
     	   hasPrevious = br.getHasPrevious();	 
     	   totalRows = br.getTotalRows();	   
     	   firstRec = br.getFirstRec();
     	   lastRec = br.getLastRec();	
     	   
     	 for(int counter = 1;counter<=rowsReturned;counter++)
     		{	
		     	setId( br.getBValues(counter,"PK_INVOICE"));
		        this.setInvNumber( br.getBValues(counter,"inv_number"));
		        this.setInvDate(br.getBValues(counter,"inv_date"));
		        this.setCreator( br.getBValues(counter,"creator"));
		        this.setUserData(br.getBValues(counter, "userData"));
		        this.setInvIntervalFrom(br.getBValues(counter,"inv_intervalfrom"));
		        this.setInvIntervalTo(br.getBValues(counter,"inv_intervalto"));
		        this.setInvNotes(br.getBValues(counter,"inv_notes"));
		        this.setInvStats(br.getBValues(counter,"invoice_Status"));
		        this.setHistoryIds(br.getBValues(counter,"pk_status"));
		        this.setSubTypes(br.getBValues(counter,"CODELST_SUBTYP"));
		        this.setCovType(br.getBValues(counter,"COVERAGE_TYPE_CRITERIA"));
		        this.setAmtInv(br.getBValues(counter,"amount_invoiced"));
     		}
     	  
     	 rowsCount.put("showPages" , br.getShowPages());
     	 rowsCount.put("startPage" , br.getStartPage());
     	 rowsCount.put("hasMore" , br.getHasMore());
     	 rowsCount.put("hasPrevious" , br.getHasPrevious());	 
     	 rowsCount.put("totalRows" , br.getTotalRows());	   
     	 rowsCount.put("firstRec" , br.getFirstRec());
     	 rowsCount.put("lastRec" , br.getLastRec());
     	 setRowsCount(rowsCount);
            
        } catch (Exception ex) {
            Rlog.fatal("invoice",
                    "InvoiceDao.getSavedInvoicesWithPagination EXCEPTION "
                            + ex);
        }
    }

    ///
    /**
     * Gets the total amount invoiced for an invoice
     *
     * @param inv Invoice PK
     *            Description of the Parameter
     */
    public double getTotalAmountInvoiced(int inv) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        double total = 0;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select sum(amount_invoiced) totalInvAmount, sum(amount_holdback) totalHoldAmount from er_invoice_detail where fk_inv = ? and detail_type = 'H'");
            pstmt.setInt(1, inv);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            		total = Double.parseDouble((rs.getString("totalInvAmount"))) + Double.parseDouble((rs.getString("totalHoldAmount")));

            }

            return total ;

        } catch (Exception ex) {
            Rlog.fatal("invoice",
                    "InvoiceDao.getTotalAmountInvoiced EXCEPTION IN FETCHING FROM er_invoice_detail table"
                            + ex);
            return total;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }


    /**
     * deletes the invoice details
     *
     * @param inv Invoice PK
     *            Description of the Parameter
     */
    public int deleteInvoiceDetails(int inv) {
        int ret= 0;

        PreparedStatement pstmt = null;
        Connection conn = null;


        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("Delete from er_invoice_detail where fk_inv = ? ");
            pstmt.setInt(1, inv);

           	if (pstmt.executeUpdate() >=0 )
           	{
           		ret = 0;
           	  Rlog.fatal("invoice",
                      "InvoiceDao.deleteInvoiceDetails data deleted, returning 0");
           	}
           	else
           	{
           		ret = -1;
           		Rlog.fatal("invoice",
                "InvoiceDao.deleteInvoiceDetails data not deleted -1");

           	}
           	conn.commit();

            return ret ;

        } catch (Exception ex) {
            Rlog.fatal("invoice",
                    "InvoiceDao.deleteInvoiceDetails EXCEPTION IN deleting FROM er_invoice_detail table"
                            + ex);
            return ret;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

    /**
     * checks if there any dependencies of the invoice
     *
     * @param inv Invoice PK
     *            Description of the Parameter
     */
    public boolean hasDependencies(int inv) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        int total = 0;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select count(9) ct from er_milepayment_details where mp_linkto_type = 'I' and mp_linkto_id = ? ");
            pstmt.setInt(1, inv);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            		total = rs.getInt(1);

            }

            if (total > 0)
            {
            	return true;
            }
            else
            {
            	return false;

            }


        } catch (Exception ex) {
            Rlog.fatal("invoice",
                    "InvoiceDao.hasDependencies EXCEPTION IN FETCHING FROM er_invoice_detail table"
                            + ex);
            return true;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }


    /**
     * Gets the invoice number sequence from the er_invoice table
     *
     * @param studyId
     *            Study Id
     *            JM: 111706
     */
    public String getInvoiceNumber() {

        PreparedStatement pstmt = null;
        Connection conn = null;
        String invNumber=null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select SEQ_ER_INVOICE_NUM.nextval INV_NUMBER from dual");
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                invNumber =  rs.getString("INV_NUMBER");

            }
            return invNumber;

        } catch (Exception ex) {
            Rlog.fatal("invoice",
                    "InvoiceDao.getInvoiceNumber EXCEPTION IN FETCHING FROM SEQ_ER_INVOICE_NUM.nextval"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }
        return invNumber;

    }
    
    /**
     * Check the invoice number sequence is existed or not from the er_invoice table
     * @param invNumber
     * Added by Sudhir Shukla for FIN-22382_0926012 Enhancement
     * @return
     */
    public boolean getInvoiceNumber(String invNumber) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        boolean flag=false;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select INV_NUMBER from ER_INVOICE where INV_NUMBER=? ");
            pstmt.setString(1, invNumber);
            ResultSet rs = pstmt.executeQuery();
            
            if (rs.next()) {
            	flag=true;
                //invNumber =  rs.getString("INV_NUMBER");
            }
            else
            	flag=false;
            
            return flag;

        } catch (Exception ex) {
            Rlog.fatal("invoice",
                    "InvoiceDao.getInvoiceNumber EXCEPTION IN FETCHING FROM SEQ_ER_INVOICE_NUM.nextval"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }
        return flag;

    }
    
    public boolean getInvoiceFlag(String invNumber) {

        PreparedStatement pstmt = null;
        PreparedStatement pstmt1 = null;
        Connection conn = null;
        String studyNumber = "";
        boolean flag=false;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select substr(INV_NUMBER,0,INSTR(INV_NUMBER,'-"+invNumber+"')-1) STUDYNUMBER from ER_INVOICE where INV_NUMBER like '%-"+invNumber+"' ");
            //pstmt.setString(1, invNumber);
            ResultSet rs = pstmt.executeQuery();
            
            if (rs.next()) {
            	studyNumber = rs.getString("STUDYNUMBER");
            	pstmt1 = conn.prepareStatement("select study_number from ER_STUDY where study_number = '"+studyNumber+"' ");
            	ResultSet rs1 = pstmt1.executeQuery();
            	if (rs1.next()) {
            		flag=true;
            	}else
            		flag=false;
            	//flag=true;
                //invNumber =  rs.getString("INV_NUMBER");
            }
            else
            	flag=false;
            
            return flag;

        } catch (Exception ex) {
            Rlog.fatal("invoice",
                    "InvoiceDao.getInvoiceNumber EXCEPTION IN FETCHING FROM SEQ_ER_INVOICE_NUM.nextval"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }
        return flag;

    }
    
    public int getInvoiceId(String invNumber) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        int invId = 0;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select PK_INVOICE from ER_INVOICE where INV_NUMBER=? ");
            pstmt.setString(1, invNumber);
            ResultSet rs = pstmt.executeQuery();
            
            if (rs.next()) {
            	invId=rs.getInt("PK_INVOICE");
            }
            
            return invId;

        } catch (Exception ex) {
            Rlog.fatal("invoice",
                    "InvoiceDao.getInvoiceNumber EXCEPTION IN FETCHING FROM SEQ_ER_INVOICE_NUM.nextval"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }
        return invId;

    }
    // end of class
    /**
     * Gets the max invoice number sequence from the er_invoice table
     *
     * @param studyId
     *            Study Id
     */
    public int getMaxInvoiceNumber(int studyID) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        int invNumber = 0;
        try {
        	StringBuffer sqlStr = new StringBuffer();
        	sqlStr.append("select MAX(to_number(invnum))+1 invnum from (SELECT ERES.F_IS_NUMBER(SUBSTR(INV_NUMBER,instr(INV_NUMBER,'-',(select LENGTH(study_number) ")
        	.append("from er_study WHERE PK_study = FK_STUDY))+1)) is_number,SUBSTR(INV_NUMBER,instr(INV_NUMBER,'-',(select LENGTH(study_number) ")
        	.append("from er_study WHERE PK_study = FK_STUDY))+1) invnum FROM er_invoice WHERE FK_STUDY = ?)  where is_number>0 ");
            conn = getConnection();

            pstmt = conn.prepareStatement(sqlStr.toString());
            pstmt.setInt(1, studyID);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

            	invNumber =  StringUtil.stringToInteger(rs.getString("invnum"));
                //System.out.println("rs.getInt(INV_NUMBER)   "+rs.getInt("INV_NUMBER"));

            }            
            	
            return invNumber;

        } catch (Exception ex) {
            Rlog.fatal("invoice",
                    "InvoiceDao.getMaxInvoiceNumber EXCEPTION IN FETCHING FROM SEQ_ER_INVOICE_NUM.nextval"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }
        return invNumber;

    }
    
    public String getInvoiceNumberAuto() {

        PreparedStatement pstmt = null;
        Connection conn = null;
        String invoiceNumber=null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select seq_er_invoice_id.nextval INVOICENUM from dual");
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

            	invoiceNumber =  rs.getString("INVOICENUM");

            }
            return invoiceNumber;

        } catch (Exception ex) {
            Rlog.fatal("invoice",
                    "InvoiceDao.getInvoiceNumberAuto EXCEPTION IN FETCHING FROM seq_er_invoice_id.nextval"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

        return invoiceNumber;

    }
   
}
