package com.velos.eres.gems.business;

public class QueryConstants {
	public static final String SELECT_WFACONDITION = "from WFActivityCondition wfac where wfac.FkWorkflowactivity=:wfaid";
	public static final String QUERY_PARAM_WFACID = "wfaid";
	public static final String SELECT_PENDINGTASK = " from Task t where t.taskCompleteflag = 0";
	public static final String SELECT_USER = "from SecUser su";
	public static final String SELECT_ROLE = "from SecRole ro";
	public static final String SELECT_USER_ID = "from SecUser u where u.id=:uid";
	public static final String QUERY_PARAM_USERID = "uid";
	public static final String GET_ACTIVITY = " from Activity a";
	public static final String GET_TASK = " from Task t ";
	public static final String GET_ACTIVITY_BY_ACTIVITYCODE = " from Activity c where c.id = :id and deletedflag=0";
	public static final String QUERY_PARAM_ACTIVITYCODE = "id";
	public static final String QUERY_PARAM_ACTIVITYNAME = "activityName";
	public static final String SELECT_SECUSER = "from SEC_USER sc where sc.user_loginname=:userId and sc.user_password=:pswd";
	public static final String USER_ID = "userId";
	public static final String USER_PASSWORD = "pswd";
	public static final String SELECT_WORKFLOW = "from Workflow w ";
	public static final String QUERY_PARAM_FIRSTNAME = "ufirstname";
	public static final String QUERY_PARAM_LASTNAME = "ulastname";
	public static final String SELECT_SECROLE = "from SecRole sr order by sr.Id asc";
	public static final String QUERY_PARAM_WORKFLOWID = "wid";
	public static final String QUERY_PARAM_ACTIVITYID = "aid";
	public static final String SELECT_ACTIVITY = "from Activity a order by a.id asc";
	public static final String SELECT_WORKFLOW_ID = "from Workflow wf where wf.id=:wid";
	public static final String SELECT_WORKFLOW_ACTIVITY_ID = "from WorkflowActivity wa where wa.FkWorkflowid=:wid";
	public static final String SELECT_WORKFLOWACTIVITY_ID = "from WorkflowActivity wfa where wfa.FkWorkflowid=:wid and wfa.FkActivityid=:aid";
	public static final String QUERY_PARAM_WORKFLOWACTIVITY_FKWORKFLOWID = "workflowactivityFkworkflowid";
	public static final String QUERY_PARAM_WORKFLOWACTIVITY_FKACTIVITYID = "workflowactivityFkactivityid";
	public static final String SELECT_WORKFLOWACTIVITY = "from WorkflowActivity wf where wf.FkWorkflowid=:workflowactivityFkworkflowid and wf.FkActivityid=:workflowactivityFkactivityid";
}