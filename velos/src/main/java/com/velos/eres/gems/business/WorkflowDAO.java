package com.velos.eres.gems.business;

import java.rmi.RemoteException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.StringUtil;

public class WorkflowDAO {

	public List fetchNextTask(Workflow objWorkflow) throws RemoteException,
			Exception {

		List resultList = new ArrayList();
		CallableStatement cSt = null;
		Connection connection = null;
		try {

			connection = CommonDAO.getConnection();

			cSt = connection.prepareCall("{call sp_workflow(?,?,?,?)}");

			// cSt.setString(1, objWorkflow.getEntityId());
			// cSt.setString(2, objWorkflow.getEntityType());
			// cSt.setInt(3, objWorkflow.getUserId());
			cSt.setString(4, " ");
			cSt.registerOutParameter(4, 1);
			cSt.execute();
			String strAction = cSt.getString(4);
			String[] strArray = new String[1];
			strArray[0] = strAction;
			resultList.add(strArray);
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			try {
				cSt.close();
				connection.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return resultList;

	}

	public WorkflowInstance getWorkflowInstanceStatus(String entityId,String workflowType) {
		Connection connection = null;
		PreparedStatement stmt =null;
		String sql = null;
		ResultSet rs = null;
		WorkflowInstance wfi = new WorkflowInstance();
		wfi.setFkEntityId(entityId);
		
		try {
			connection = CommonDAO.getConnection();
		 
			sql = "SELECT PK_WFINSTANCEID, WI_STATUSFLAG FROM WORKFLOW_INSTANCE wfi where FK_ENTITYID=:1 AND FK_WORKFLOWID=(SELECT PK_WORKFLOW from Workflow where WORKFLOW_TYPE=:2)";
			stmt = connection.prepareStatement(sql);
			
			stmt.setInt(1, StringUtil.stringToNum(entityId));
			stmt.setString(2, workflowType);
			stmt.execute();
		
			rs = stmt.getResultSet();
			
			while (rs.next()) {
				wfi.setPkWfInstanceId(rs.getInt("PK_WFINSTANCEID"));
				wfi.setWfIStatusFlag(rs.getInt("WI_STATUSFLAG"));
				return wfi;
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			wfi = null;
		} finally {
			try {
				rs.close();
				stmt.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return wfi;
	}
	
	public ArrayList getWorkflowInstanceTasks(int workflowInstanceId) {
		ArrayList workflowTasks = new ArrayList();
		
		String query = "SELECT PK_TASKID, WA_NAME, TASK_COMPLETEFLAG, wa_keytable, wfa.pk_workflowactivity, wfa.wa_sequence, wfa.wa_terminateflag, wfi.fk_workflowid, t.pk_taskid, t.fk_entity, t.fk_wfinstanceid, wfa.WA_DISPLAYQUERY,"
				+ " WA_ONCLICK_JSON"
				+ " FROM Workflow wf, workflow_instance wfi, workflow_activity wfa, task t WHERE wf.PK_WORKFLOW = wfi.FK_WORKFLOWID AND wfi.PK_WFINSTANCEID = :1 "
				+ "AND wfi.pk_wfinstanceid = t.fk_wfinstanceid AND wfa.pk_workflowactivity = t.fk_workflowactivity ORDER BY wa_sequence ";

		Connection conn = null; 
		PreparedStatement stmt = null; 
		ResultSet rs = null; 
		
		try {
			conn = CommonDAO.getConnection();
			stmt = conn.prepareStatement(query); 
			stmt.setInt(1, workflowInstanceId); 
			rs = stmt.executeQuery();

			workflowTasks = new ArrayList();
			String columnLabel = null;
					
			while (rs.next()) {
				HashMap<String, Object> record = new HashMap<String, Object>();
				columnLabel = "PK_TASKID";
				record.put (columnLabel, rs.getInt(columnLabel));
				
				columnLabel = "WA_NAME";
				String actWorkflowName =rs.getString(columnLabel);
				if(actWorkflowName!=null){
					String wfName = "";
					Connection conn1 = null; 
					PreparedStatement stmt1 = null; 
					ResultSet rs1 = null;
					try {
						conn1 = CommonDAO.getConnection();
						stmt1 = conn1.prepareStatement(actWorkflowName);  
						rs1 = stmt1.executeQuery();
						//System.out.println("Workflow Name Query:::"+actWorkflowName);
						while (rs1.next()) {
							wfName = rs1.getString(1);
							//System.out.println("wfName:::"+wfName);
						}
	
					}catch (Exception e) {
						e.printStackTrace();
					}finally{
						try {
							rs1.close();
							stmt1.close();
							conn1.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					record.put (columnLabel, wfName);
				}else{
					record.put (columnLabel, actWorkflowName);
				}
				
				columnLabel = "TASK_COMPLETEFLAG";
				record.put (columnLabel, rs.getString(columnLabel));

				columnLabel = "WA_ONCLICK_JSON";
				record.put (columnLabel, rs.getString(columnLabel));

				workflowTasks.add(record);
			}
			return workflowTasks;

		} catch (Exception e) {
			e.printStackTrace();
			workflowTasks = null;
		}finally{
			try {
				rs.close();
				stmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return workflowTasks;
	}

	public Integer getWorkflowStatus(String entityId, String workflowType) {

		Connection connection = null;
		String query = "SELECT PK_WFINSTANCEID FROM Workflow_Instance where FK_ENTITYID="
				+ entityId
				+ " AND FK_WORKFLOWID=(SELECT PK_WORKFLOW from Workflow where WORKFLOW_TYPE='"
				+ workflowType + "')";
		Integer result = 0;
		Statement s =null;
		ResultSet rs =null;
		try {
			connection = CommonDAO.getConnection();
			 s = connection.createStatement();
			 rs = s.executeQuery(query);

			while (rs.next()) {

				result = rs.getInt("PK_WFINSTANCEID");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				rs.close();
				s.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	public Integer updateWorkflow(String entityId, String workflowType) {
		Connection connection = null;
		CallableStatement s = null;
		Integer pendingTasks= 0;
		ResultSet rs=null;
		try{
			connection = CommonDAO.getConnection();
			s = connection.prepareCall("call PKG_WORKFLOW.SP_UPDATE_WORKFLOW(?,?,?,?)");
			s.registerOutParameter(4, java.sql.Types.INTEGER);
			s.setInt(1, StringUtil.stringToInteger(entityId));
			s.setString(2, workflowType);
			s.setString(3, "");
			s.setInt(4, pendingTasks);
			
			s.executeQuery();
			
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				s.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return pendingTasks;
	}

	public Integer updateWorkflow(String entityId, String workflowType, String[] paramArray) {
		Connection connection = null;
		CallableStatement s = null;
		Integer pendingTasks= 0;
		ResultSet rs=null;
		try{
			connection = CommonDAO.getConnection();
			
			ArrayDescriptor inParams = ArrayDescriptor.createDescriptor("ARRAY_STRING", connection);
    		ARRAY paramArr = new ARRAY(inParams,connection,paramArray);

			s = connection.prepareCall("call PKG_WORKFLOW.SP_UPDATE_WORKFLOW(?,?,?,?,?)");
			s.setInt(1, StringUtil.stringToInteger(entityId));
			s.setString(2, workflowType);
			s.setArray(3,paramArr);
			s.setString(4, "");
			s.registerOutParameter(5, java.sql.Types.INTEGER);
			s.executeQuery();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				s.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return pendingTasks;
	}
	
	public Integer createWorkflow(String entityId,String userId) {
		Connection connection = null;
		CallableStatement s = null;
		Integer pendingTasks= 0;
		ResultSet rs=null;
		try{
			connection = CommonDAO.getConnection();
			String workflowTypes=CFG.Workflow_FlexStudy_WfTypes;
			
			String[] paramArray = workflowTypes.split(",");
			System.out.println("paramArray size==="+paramArray.length);
			ArrayDescriptor inParams = ArrayDescriptor.createDescriptor("ARRAY_STRING", connection);
    		ARRAY paramArr = new ARRAY(inParams,connection,paramArray);

			s = connection.prepareCall("call PKG_WORKFLOW.SP_CREATE_WORKFLOW(?,?,?,?)");
			s.setInt(1, StringUtil.stringToInteger(entityId));
			s.setArray(2,paramArr);
			s.setInt(3, StringUtil.stringToInteger(userId));
			s.registerOutParameter(4, java.sql.Types.INTEGER);
			
			s.executeQuery();
			int completeFlag=s.getInt(4);
			if(completeFlag==1){
			for(String workflowType: paramArray){
				String[] updateParams = new String[1];
				updateParams[0]=String.valueOf(entityId);
				updateWorkflow(entityId, workflowType, updateParams);
			}
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				s.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return pendingTasks;
	}

	public Integer fetchNextTaskInQueue(Integer entityId, String workflowType) {

		Connection connection = null;
		String query = "SELECT pk_taskid from (SELECT t1.pk_taskid FROM task t1,WORKFLOW_ACTIVITY wfa1"
				+ " WHERE t1.FK_WORKFLOWACTIVITY = wfa1.PK_WORKFLOWACTIVITY AND t1.PK_TASKID IN"
				+ " (SELECT t0.PK_TASKID FROM TASK t0 WHERE t0.TASK_COMPLETEFLAG=0 AND FK_ENTITY=?1"
				+ " AND t0.FK_WFINSTANCEID    = (SELECT PK_WFINSTANCEID FROM WORKFLOW_INSTANCE WHERE WORKFLOW_INSTANCE.FK_ENTITYID   =?2 AND FK_WORKFLOWID ="
				+ " (SELECT PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE=?3))) ORDER BY wfa1.WA_SEQUENCE)"
				+ " WHERE ROWNUM=1";

		Integer taskId = 0;
		ResultSet rs =null;
		PreparedStatement s = null;
		try {
			connection = CommonDAO.getConnection();
			 s = connection.prepareStatement(query);
			s.setInt(1,entityId);
			s.setInt(2, entityId);
			s.setString(3, workflowType);
			rs = s.executeQuery();

			while (rs.next()) {

				taskId = rs.getInt("pk_taskid");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				rs.close();
				s.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return taskId;
	}
	
	public Integer fetchFirstPendingTaskInQueue(Integer entityId, String workflowType) {

		Connection connection = null;
		String query = "SELECT pk_taskid from (SELECT t1.pk_taskid FROM task t1,WORKFLOW_ACTIVITY wfa1"
				+ " WHERE t1.FK_WORKFLOWACTIVITY = wfa1.PK_WORKFLOWACTIVITY AND t1.PK_TASKID IN"
				+ " (SELECT t0.PK_TASKID FROM TASK t0 WHERE t0.TASK_COMPLETEFLAG=0 AND FK_ENTITY=?1"
				+ " AND t0.FK_WFINSTANCEID    = (SELECT PK_WFINSTANCEID FROM WORKFLOW_INSTANCE WHERE WORKFLOW_INSTANCE.FK_ENTITYID   =?2 AND FK_WORKFLOWID ="
				+ " (SELECT PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE=?3))) ORDER BY wfa1.WA_SEQUENCE DESC)"
				+ " WHERE ROWNUM=1";

		Integer taskId = 0;
		ResultSet rs =null;
		PreparedStatement s = null;
		try {
			connection = CommonDAO.getConnection();
			 s = connection.prepareStatement(query);
			s.setInt(1,entityId);
			s.setInt(2, entityId);
			s.setString(3, workflowType);
			rs = s.executeQuery();

			while (rs.next()) {

				taskId = rs.getInt("pk_taskid");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				rs.close();
				s.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return taskId;
	}
	
	public Map<String,String> fetchTaskDetails(Integer taskId) {

		Connection connection = null;
		String query = "SELECT wfa.WA_CALLACTION,wfa.WA_DEFAULTFORWARDACTION,WA_KEYCOLUMN,wfa.WA_KEYTABLE,"+
		"wfa.WA_NAME,wfa.WA_SEQUENCE,t.TASK_NAME,act.ACTIVITY_NAME,act.ACTIVITY_DESC,WA_SUCESSFORWARDACTION,WA_ONCLICK_JSON "+
		" FROM workflow wf,workflow_activity wfa,workflow_instance wfi,task t,activity act WHERE t.PK_TASKID=?1"+
		" AND t.FK_WFINSTANCEID = wfi.PK_WFINSTANCEID AND t.FK_WORKFLOWACTIVITY = wfa.PK_WORKFLOWACTIVITY"+
		" AND wfi.FK_WORKFLOWID = wf.PK_WORKFLOW AND wfa.FK_ACTIVITYID = act.PK_ACTIVITY";

		ResultSet rs =null;
		PreparedStatement s = null;
		Map<String,String> resultMap = new HashMap<String,String>();
		try {
			connection = CommonDAO.getConnection();
			s = connection.prepareStatement(query);
			s.setInt(1,taskId);
			rs = s.executeQuery();

			while (rs.next()) {
				resultMap.put("WA_CALLACTION", rs.getString("WA_CALLACTION"));
				resultMap.put("WA_DEFAULTFORWARDACTION", rs.getString("WA_DEFAULTFORWARDACTION"));
				resultMap.put("WA_KEYCOLUMN", rs.getString("WA_KEYCOLUMN"));
				resultMap.put("WA_NAME", rs.getString("WA_NAME"));
				resultMap.put("WA_SEQUENCE", rs.getString("WA_SEQUENCE"));
				resultMap.put("TASK_NAME", rs.getString("TASK_NAME"));
				resultMap.put("ACTIVITY_NAME", rs.getString("ACTIVITY_NAME"));
				resultMap.put("ACTIVITY_DESC", rs.getString("ACTIVITY_DESC"));
				resultMap.put("WA_SUCESSFORWARDACTION", rs.getString("WA_SUCESSFORWARDACTION"));
				resultMap.put("WA_ONCLICK_JSON", rs.getString("WA_ONCLICK_JSON"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				rs.close();
				s.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return resultMap;
	}

	public ArrayList getWorkflowInstanceForms(String entityId,String workflowType){
		ArrayList wfFormIds = new ArrayList();

		WorkflowInstance workflowInstance = this.getWorkflowInstanceStatus(entityId, workflowType);
		if (null == workflowInstance) return null;

		Integer workflowInstanceId = workflowInstance.getPkWfInstanceId();
		ArrayList workflowTasksList = this.getWorkflowInstanceTasks(workflowInstanceId);
		if (null == workflowTasksList) return null;

		HashMap<String, Object> record = null;
		for (int taskIndx = 0; taskIndx < workflowTasksList.size(); taskIndx++){
			record = (HashMap<String, Object>) workflowTasksList.get(taskIndx);
			try {
				if (null == record.get("WA_ONCLICK_JSON")){	continue; }
				JSONObject onclickJSON = new JSONObject((record.get("WA_ONCLICK_JSON")).toString());
				int formId = (onclickJSON.getJSONObject("moduleDet")).getInt("formId");
				if (wfFormIds.indexOf(formId) < 0){
					wfFormIds.add(formId);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		return wfFormIds;
	}
	
	public ArrayList getWorkflowInstanceFormsIds(String entityId){
		ArrayList<Integer> wfFormIds = new ArrayList<Integer>();
		String workflow_types =CFG.Workflow_FlexStudy_WfTypes;
		if(!workflow_types.equals("[Workflow_FlexStudy_WfTypes]") && !workflow_types.equals("")){
		if(workflow_types.contains(",")){
		String wfTypeArr[] =  workflow_types.split(",");
		if(wfTypeArr.length>0){
		for(String workflowType: wfTypeArr){
		WorkflowInstance workflowInstance = this.getWorkflowInstanceStatus(entityId, workflowType);
		if (null == workflowInstance) return null;

		Integer workflowInstanceId = workflowInstance.getPkWfInstanceId();
		ArrayList workflowTasksList = this.getWorkflowInstanceTasks(workflowInstanceId);
		if (null == workflowTasksList) return null;

		HashMap<String, Object> record = null;
		for (int taskIndx = 0; taskIndx < workflowTasksList.size(); taskIndx++){
			record = (HashMap<String, Object>) workflowTasksList.get(taskIndx);
			try {
				if (null == record.get("WA_ONCLICK_JSON")){	continue; }
				JSONObject onclickJSON = new JSONObject((record.get("WA_ONCLICK_JSON")).toString());
				int formId = (onclickJSON.getJSONObject("moduleDet")).getInt("formId");
				if (wfFormIds.indexOf(formId) < 0){
					wfFormIds.add(formId);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		}
	}
		}else{
			WorkflowInstance workflowInstance = this.getWorkflowInstanceStatus(entityId, workflow_types);
			if (null == workflowInstance) return null;

			Integer workflowInstanceId = workflowInstance.getPkWfInstanceId();
			ArrayList workflowTasksList = this.getWorkflowInstanceTasks(workflowInstanceId);
			if (null == workflowTasksList) return null;

			HashMap<String, Object> record = null;
			for (int taskIndx = 0; taskIndx < workflowTasksList.size(); taskIndx++){
				record = (HashMap<String, Object>) workflowTasksList.get(taskIndx);
				try {
					if (null == record.get("WA_ONCLICK_JSON")){	continue; }
					JSONObject onclickJSON = new JSONObject((record.get("WA_ONCLICK_JSON")).toString());
					int formId = (onclickJSON.getJSONObject("moduleDet")).getInt("formId");
					if (wfFormIds.indexOf(formId) < 0){
						wfFormIds.add(formId);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}
		}
		}
		return wfFormIds;
	}

	public Integer getStudyIDbyNumber(String studyNumber) {
		Connection connection = null;
		String query = "SELECT PK_STUDY FROM ER_STUDY where STUDY_NUMBER=?";

		Integer studyId = 0;
		ResultSet rs =null;
		PreparedStatement s = null;
		try {
			connection = CommonDAO.getConnection();
			s = connection.prepareStatement(query);
			s.setString(1, studyNumber);
			rs = s.executeQuery();

			while (rs.next()) {

				studyId = rs.getInt("PK_STUDY");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				rs.close();
				s.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return studyId;
	}

}