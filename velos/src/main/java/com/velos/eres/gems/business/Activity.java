package com.velos.eres.gems.business;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.Date;


/**
 * The persistent class for the ACTIVITY database table.
 * 
 */
@Entity
@Table(name="ACTIVITY")
@NamedQuery(name="Activity.findAll", query="SELECT a FROM Activity a")
public class Activity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Integer pkActivity;
	private String activityDesc;
	private String activityName;
	private Integer auditid;
	private Integer createdby;
	private Date createdon;
	private Integer deletedflag;
	private Integer organizationid;

	public Activity() {
	}

	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SQ_ACTIVITY", allocationSize=1)    
	@Column(name="PK_ACTIVITY")
	public Integer getPkActivity() {
		return this.pkActivity;
	}

	public void setPkActivity(Integer pkActivity) {
		this.pkActivity = pkActivity;
	}


	@Column(name="ACTIVITY_DESC", length=200)
	public String getActivityDesc() {
		return this.activityDesc;
	}

	public void setActivityDesc(String activityDesc) {
		this.activityDesc = activityDesc;
	}


	@Column(name="ACTIVITY_NAME", length=100)
	public String getActivityName() {
		return this.activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}


	@Column(name="AUDITID")
	public Integer getAuditid() {
		return this.auditid;
	}

	public void setAuditid(Integer auditid) {
		this.auditid = auditid;
	}


	@Column(name="CREATEDBY")
	public Integer getCreatedby() {
		return this.createdby;
	}

	public void setCreatedby(Integer createdby) {
		this.createdby = createdby;
	}

	
	@Column(name="CREATEDON")
	public Date getCreatedon() {
		return this.createdon;
	}

	public void setCreatedon(Timestamp createdon) {
		this.createdon = createdon;
	}


	@Column(name="DELETEDFLAG")
	public Integer getDeletedflag() {
		return this.deletedflag;
	}

	public void setDeletedflag(Integer deletedflag) {
		this.deletedflag = deletedflag;
	}


	@Column(name="ORGANIZATIONID")
	public Integer getOrganizationid() {
		return this.organizationid;
	}

	public void setOrganizationid(Integer organizationid) {
		this.organizationid = organizationid;
	}

}