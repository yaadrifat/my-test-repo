package com.velos.eres.gems.service;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.velos.eres.gems.business.*;

import javax.ejb.Remote;

@Remote
public interface WorkflowInstanceAgentRObj {
	public List<Task> fetchTasks(String paramString1,
			String paramString2) throws RemoteException, Exception;

	public WorkflowInstance getWorkflowInstanceStatus(String entityId,String workflowType)
			throws RemoteException, Exception;

	public ArrayList getWorkflowInstanceTasks(int workflowInstanceId)
			throws RemoteException, Exception;
	
	public Integer startWorkflowInstance(String entityId,String workflowType)
			throws RemoteException, Exception;
	
	public List<Task> fetchPendingTasks(String entityId, String workflowType) throws RemoteException,Exception;
		
	public Integer updateWorkflowInstance(String entityId, String workflowType) throws RemoteException,Exception;
	
	public Integer updateWorkflowInstance(String entityId, String workflowType, String[] paramArray) throws RemoteException,Exception;
		
}