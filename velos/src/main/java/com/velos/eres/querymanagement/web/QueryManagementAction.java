package com.velos.eres.querymanagement.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.StringUtil;

public class QueryManagementAction extends ActionSupport {
	
	private HttpServletRequest request = null;
	private HttpSession tSession = null;
	private ArrayList<HashMap<String, Object>> aaData = new ArrayList<HashMap<String,Object>>();
	private ArrayList<HashMap<String, Object>> tempData = new ArrayList<HashMap<String,Object>>();
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	int iDisplayLength;
	int iDisplayStart ;
	int iSortCol ;
	String iSortDir ;
	private String sEcho;
	public ArrayList<HashMap<String, Object>> getAaData() {
		return aaData;
	}

	public void setAaData(ArrayList<HashMap<String, Object>> aaData) {
		this.aaData = aaData;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
	public String getsEcho() {
		return sEcho;
	}

	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}

	private Integer userId = null;
	private Integer accountId = null;
	public static final String DATA_NOT_SAVED = "dataNotSaved";
	private Map errorMap = null;

	public Map getErrorMap() {
		return errorMap;
	}

	public void setErrorMap(Map errorMap) {
		this.errorMap = errorMap;
	}

	public QueryManagementAction() {
		ActionContext ac = ActionContext.getContext();
		request = (HttpServletRequest) ac
				.get(ServletActionContext.HTTP_REQUEST);
		tSession = request.getSession(false);

		userId = StringUtil.stringToInteger((String) tSession
				.getAttribute("userId"));
		accountId = StringUtil.stringToInteger((String) tSession
				.getAttribute("accountId"));

	}
	
	public String fetchFormQueries() {
		this.iDisplayLength = Integer.parseInt(request.getParameter("iDisplayLength"));
		this.iDisplayStart = Integer.parseInt(request.getParameter("iDisplayStart"));
		this.iSortCol = Integer.parseInt(request.getParameter("iSortCol_0"));
		this.iSortDir = request.getParameter("sSortDir_0");
		String studyIds = request.getParameter("selectedStudies");
		String qryStatus[] = request.getParameterValues("qryStatus");
		String noFilter="true";
		//R&D
		String qryStatusRed = request.getParameter("qryStatusRed");
		String qryStatusOrange = request.getParameter("qryStatusOrange");
		String qryStatusYellow = request.getParameter("qryStatusYellow");
		String qryStatusPurple = request.getParameter("qryStatusPurple");
		String qryStatusWhite = request.getParameter("qryStatusWhite");
		String qryStatusGreen = request.getParameter("qryStatusGreen");
		//String  = request.getParameter("");
		
		///R&D
		String qryStatusId="";
		if(null != qryStatus) {
		for(int i=0;i<qryStatus.length;i++)
		{
			qryStatusId=qryStatus[i];
		} }
		String orgIds  = request.getParameter("selectedOrgs");
		String formId = request.getParameter("selectedForms");
		String formStatusId = request.getParameter("selectedFormStatues");
		String patientId = request.getParameter("selectedPatientId");
		String patientStdId = request.getParameter("selectedPatientStudyId");
		String dataEntryDateFrom = request.getParameter("selectedDataEntryDateFrom");
		String dataEntryDateTo = request.getParameter("selectedDataEntryDateTo");		
		String patientCalId = request.getParameter("selectedCalName");
		String patientVisitId = request.getParameter("selectedVisitName");
		String patientEventId = request.getParameter("selectedEventName");
		String eventDate = request.getParameter("selectedEventDate");
		String queryCreatorId = request.getParameter("selectedQueryCreator");
		String queryStatusId = request.getParameter("selectedQueryStatues");
		String selectedTotalRespQuery = request.getParameter("selectedTotalRespQuery");
		String selectedqueryStatuess = request.getParameter("selectedqueryStatuess");
		String sSearch=request.getParameter("sSearch");
		
		HashMap<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("userId", StringUtil.integerToString(userId));
		paramMap.put("accountId", StringUtil.integerToString(accountId));
		paramMap.put("studyIds", studyIds);
		//paramMap.put("qryStatusId", qryStatusId);
		if(null != qryStatusRed && !qryStatusRed.equalsIgnoreCase(""))
		    { noFilter = "false";
		    paramMap.put("qryStatusRed", qryStatusRed); }
		if(null != qryStatusOrange && !qryStatusOrange.equalsIgnoreCase(""))
		    { noFilter = "false";
			paramMap.put("qryStatusOrange", qryStatusOrange); }
		if(null != qryStatusYellow && !qryStatusYellow.equalsIgnoreCase(""))
	    { noFilter = "false";
		paramMap.put("qryStatusYellow", qryStatusYellow); }
		if(null != qryStatusPurple && !qryStatusPurple.equalsIgnoreCase(""))
		    { noFilter = "false"; 
			paramMap.put("qryStatusPurple", qryStatusPurple); }
	    if(null != qryStatusWhite && !qryStatusWhite.equalsIgnoreCase(""))
	        { noFilter = "false";
	        paramMap.put("qryStatusWhite", qryStatusWhite); }
		if(null != qryStatusGreen && !qryStatusGreen.equalsIgnoreCase(""))
		    { noFilter = "false";
		    paramMap.put("qryStatusGreen", qryStatusGreen); }
		
		paramMap.put("noFilter", noFilter);
		paramMap.put("orgIds", orgIds);
		paramMap.put("formId", formId);
		paramMap.put("formStatusId", formStatusId);
		paramMap.put("dataEntryDateFrom", dataEntryDateFrom);
		paramMap.put("dataEntryDateTo", dataEntryDateTo);
		paramMap.put("patientId", patientId);
		paramMap.put("patientStdId", patientStdId);
		paramMap.put("patientCalId", patientCalId);
		paramMap.put("patientVisitId", patientVisitId);
		paramMap.put("patientEventId", patientEventId);
		paramMap.put("eventDate", eventDate);
		paramMap.put("queryCreatorId", queryCreatorId);
		paramMap.put("queryStatusId", queryStatusId);
		paramMap.put("selectedTotalRespQuery", selectedTotalRespQuery);
		paramMap.put("selectedqueryStatuess", selectedqueryStatuess);
		paramMap.put("sSearch", sSearch);
		tempData = new QueryManagementJB().fetchFormQueries(paramMap,iDisplayLength,iDisplayStart,iSortCol,iSortDir);
		this.setAaData(tempData);
		if(tempData.size()==0){
			this.setiTotalDisplayRecords(tempData.size());
			this.setiTotalRecords(tempData.size());	
		}else{
		this.setiTotalDisplayRecords(Integer.parseInt(tempData.get(0).get("rows").toString()));
		this.setiTotalRecords(Integer.parseInt(tempData.get(0).get("rows").toString()));
		}
		return SUCCESS;
	}
	
	public String fetchAdvEventQueriesData() {
		this.sEcho = request.getParameter("sEcho");
		this.iDisplayLength = Integer.parseInt(request.getParameter("iDisplayLength"));
		this.iDisplayStart = Integer.parseInt(request.getParameter("iDisplayStart"));
		this.iSortCol = Integer.parseInt(request.getParameter("iSortCol_0"));
		this.iSortDir = request.getParameter("sSortDir_0");
		String sSearch=request.getParameter("sSearch");
		String studyIds = request.getParameter("selectedStudies");
		String orgIds  = request.getParameter("selectedOrgs");
		String formId = request.getParameter("selectedForms");
		String advEventStatId = request.getParameter("selectedAdvEveStatus");
		String dataEntryDateFrom = request.getParameter("selectedDataEntryDateFrom");
		String dataEntryDateTo = request.getParameter("selectedDataEntryDateTo");
		String patientId = request.getParameter("selectedPatientId");
		String queryStatusId = request.getParameter("selectedQueryStatues");
		String patientStdId = request.getParameter("selectedPatientStudyId");
		String queryCreatorId = request.getParameter("selectedQueryCreator");
		String selectedTotalRespQuery = request.getParameter("selectedTotalRespQuery");
		String selectedqueryStatuess = request.getParameter("selectedqueryStatuess");
		String noFilter="true";
		String qryStatusRed = request.getParameter("qryStatusRed");
		String qryStatusOrange = request.getParameter("qryStatusOrange");
		String qryStatusYellow = request.getParameter("qryStatusYellow");
		String qryStatusPurple = request.getParameter("qryStatusPurple");
		String qryStatusWhite = request.getParameter("qryStatusWhite");
		String qryStatusGreen = request.getParameter("qryStatusGreen");
		HashMap<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("userId", StringUtil.integerToString(userId));
		paramMap.put("accountId", StringUtil.integerToString(accountId));
		paramMap.put("studyIds", studyIds);
		paramMap.put("sSearch", sSearch);
		paramMap.put("orgIds", orgIds);
		paramMap.put("formId", formId);
		paramMap.put("advEventStatId", advEventStatId);
		paramMap.put("dataEntryDateFrom", dataEntryDateFrom);
		paramMap.put("dataEntryDateTo", dataEntryDateTo);
		paramMap.put("patientId", patientId);
		paramMap.put("queryStatusId", queryStatusId);	
		paramMap.put("patientStdId", patientStdId);
		paramMap.put("queryCreatorId", queryCreatorId);
		paramMap.put("selectedTotalRespQuery", selectedTotalRespQuery);
		paramMap.put("selectedqueryStatuess", selectedqueryStatuess);
			if(null != qryStatusRed && !qryStatusRed.equalsIgnoreCase(""))
	    { noFilter = "false";
	    paramMap.put("qryStatusRed", qryStatusRed); }
	if(null != qryStatusOrange && !qryStatusOrange.equalsIgnoreCase(""))
	    { noFilter = "false";
		paramMap.put("qryStatusOrange", qryStatusOrange); }
	if(null != qryStatusYellow && !qryStatusYellow.equalsIgnoreCase(""))
    { noFilter = "false";
	paramMap.put("qryStatusYellow", qryStatusYellow); }
	if(null != qryStatusPurple && !qryStatusPurple.equalsIgnoreCase(""))
	    { noFilter = "false"; 
		paramMap.put("qryStatusPurple", qryStatusPurple); }
    if(null != qryStatusWhite && !qryStatusWhite.equalsIgnoreCase(""))
        { noFilter = "false";
        paramMap.put("qryStatusWhite", qryStatusWhite); }
	if(null != qryStatusGreen && !qryStatusGreen.equalsIgnoreCase(""))
	    { noFilter = "false";
	    paramMap.put("qryStatusGreen", qryStatusGreen); }
	paramMap.put("noFilter", noFilter);
		tempData = new QueryManagementJB().fetchAdvEventQueriesData(paramMap,iDisplayLength,iDisplayStart,iSortCol,iSortDir);
		this.setAaData(tempData);
		if(tempData.size()==0){
			this.setiTotalDisplayRecords(tempData.size());
			this.setiTotalRecords(tempData.size());	
		}else{
		this.setiTotalDisplayRecords(Integer.parseInt(tempData.get(0).get("rows").toString()));
		this.setiTotalRecords(Integer.parseInt(tempData.get(0).get("rows").toString()));}		
		return SUCCESS;
	}
	public String fetchPatStdStatesData() {
		this.sEcho = request.getParameter("sEcho");
		this.iDisplayLength = Integer.parseInt(request.getParameter("iDisplayLength"));
		this.iDisplayStart = Integer.parseInt(request.getParameter("iDisplayStart"));
		this.iSortCol = Integer.parseInt(request.getParameter("iSortCol_0"));
		String sSearch=request.getParameter("sSearch");
		this.iSortDir = request.getParameter("sSortDir_0");
		String studyIds = request.getParameter("selectedStudies");
		String orgIds  = request.getParameter("selectedOrgs");
		String FormId = request.getParameter("selectedPatientFormStatues");
		String dataEntryDateFrom = request.getParameter("selectedDataEntryDateFrom");
		String dataEntryDateTo = request.getParameter("selectedDataEntryDateTo");
		String patientId = request.getParameter("selectedPatientId");
		String queryStatusId = request.getParameter("selectedQueryStatues");
		String patientStdId = request.getParameter("selectedPatientStudyId");
		String queryCreatorId = request.getParameter("selectedQueryCreator");
		String selectedTotalRespQuery = request.getParameter("selectedTotalRespQuery");
		String selectedqueryStatuess = request.getParameter("selectedqueryStatuess");
		String noFilter="true";
		String qryStatusRed = request.getParameter("qryStatusRed");
		String qryStatusOrange = request.getParameter("qryStatusOrange");
		String qryStatusYellow = request.getParameter("qryStatusYellow");
		String qryStatusPurple = request.getParameter("qryStatusPurple");
		String qryStatusWhite = request.getParameter("qryStatusWhite");
		String qryStatusGreen = request.getParameter("qryStatusGreen");
		HashMap<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("userId", StringUtil.integerToString(userId));
		paramMap.put("sSearch", sSearch);
		paramMap.put("accountId", StringUtil.integerToString(accountId));
		paramMap.put("studyIds", studyIds);
		paramMap.put("orgIds", orgIds);
		paramMap.put("formId", FormId);
		paramMap.put("dataEntryDateFrom", dataEntryDateFrom);
		paramMap.put("dataEntryDateTo", dataEntryDateTo);
		paramMap.put("patientId", patientId);
		paramMap.put("queryStatusId", queryStatusId);
		paramMap.put("patientStdId", patientStdId);
		paramMap.put("queryCreatorId", queryCreatorId);
		paramMap.put("selectedTotalRespQuery", selectedTotalRespQuery);
		paramMap.put("selectedqueryStatuess", selectedqueryStatuess);
		if(null != qryStatusRed && !qryStatusRed.equalsIgnoreCase(""))
	    { noFilter = "false";
	    paramMap.put("qryStatusRed", qryStatusRed); }
	if(null != qryStatusOrange && !qryStatusOrange.equalsIgnoreCase(""))
	    { noFilter = "false";
		paramMap.put("qryStatusOrange", qryStatusOrange); }
	if(null != qryStatusYellow && !qryStatusYellow.equalsIgnoreCase(""))
    { noFilter = "false";
	paramMap.put("qryStatusYellow", qryStatusYellow); }
	if(null != qryStatusPurple && !qryStatusPurple.equalsIgnoreCase(""))
	    { noFilter = "false"; 
		paramMap.put("qryStatusPurple", qryStatusPurple); }
    if(null != qryStatusWhite && !qryStatusWhite.equalsIgnoreCase(""))
        { noFilter = "false";
        paramMap.put("qryStatusWhite", qryStatusWhite); }
	if(null != qryStatusGreen && !qryStatusGreen.equalsIgnoreCase(""))
	    { noFilter = "false";
	    paramMap.put("qryStatusGreen", qryStatusGreen); }
	paramMap.put("noFilter", noFilter);
		tempData = new QueryManagementJB().fetchPatStdStatesData(paramMap,iDisplayLength,iDisplayStart,iSortCol,iSortDir);
		this.setAaData(tempData);
		if(tempData.size()==0){
			this.setiTotalDisplayRecords(tempData.size());
			this.setiTotalRecords(tempData.size());	
		}else{
		this.setiTotalDisplayRecords(Integer.parseInt(tempData.get(0).get("rows").toString()));
		this.setiTotalRecords(Integer.parseInt(tempData.get(0).get("rows").toString()));}
		return SUCCESS;
	}
	public String fetchPatDemographicsData() {
		this.sEcho = request.getParameter("sEcho");
		this.iDisplayLength = Integer.parseInt(request.getParameter("iDisplayLength"));
		this.iDisplayStart = Integer.parseInt(request.getParameter("iDisplayStart"));
		this.iSortCol = Integer.parseInt(request.getParameter("iSortCol_0"));
		this.iSortDir = request.getParameter("sSortDir_0");
		String studyIds = request.getParameter("selectedStudies");
		String orgIds  = request.getParameter("selectedOrgs");
		String dataEntryDateFrom = request.getParameter("selectedDataEntryDateFrom");
		String dataEntryDateTo = request.getParameter("selectedDataEntryDateTo");
		String patientId = request.getParameter("selectedPatientId");
		String patientStdId = request.getParameter("selectedPatientStudyId");
		String queryCreatorId = request.getParameter("selectedQueryCreator");
		HashMap<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("userId", StringUtil.integerToString(userId));
		paramMap.put("accountId", StringUtil.integerToString(accountId));
		paramMap.put("studyIds", studyIds);
		paramMap.put("orgIds", orgIds);
		paramMap.put("dataEntryDateFrom", dataEntryDateFrom);
		paramMap.put("dataEntryDateTo", dataEntryDateTo);
		paramMap.put("patientId", patientId);
		paramMap.put("patientStdId", patientStdId);
		paramMap.put("queryCreatorId", queryCreatorId);
		tempData = new QueryManagementJB().fetchPatDemographicsData(paramMap,iDisplayLength,iDisplayStart,iSortCol,iSortDir);
		this.setAaData(tempData);
		if(tempData.size()==0){
			this.setiTotalDisplayRecords(tempData.size());
			this.setiTotalRecords(tempData.size());	
		}else{
		this.setiTotalDisplayRecords(Integer.parseInt(tempData.get(0).get("rows").toString()));
		this.setiTotalRecords(Integer.parseInt(tempData.get(0).get("rows").toString()));}
		return SUCCESS;
	}
	
	public String updateBulkFormStatusData() {
		
		String filledFormIds = request.getParameter("filledformids");
		String filledFormStat = request.getParameter("filledformstat");
		String formType = request.getParameter("formtype");
		String tabSelected = request.getParameter("tabSelected");
		String tableName = "ER_ACCTFORMS";
		String pkTable = "PK_ACCTFORMS";
		if(formType.equals("A")){
			tableName = "ER_ACCTFORMS";
			pkTable = "PK_ACCTFORMS";}
		else if(formType.equals("P")){
			tableName = "ER_PATFORMS";
			pkTable = "PK_PATFORMS";}
		else{
			tableName = "ER_STUDYFORMS";
			pkTable = "PK_STUDYFORMS";}
		if(Integer.parseInt(tabSelected) == 1){
			tableName = "SCH_ADVERSEVE";
			pkTable = "PK_ADVEVE";
		}
			
		
		HashMap<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("userId", StringUtil.integerToString(userId));
		paramMap.put("accountId", StringUtil.integerToString(accountId));
		paramMap.put("filledFormIds", filledFormIds);
		paramMap.put("filledFormStat", filledFormStat);
		paramMap.put("tableName", tableName);
		paramMap.put("pkTable", pkTable);
		paramMap.put("tabSelected", tabSelected);
		Integer output = (new QueryManagementJB().updateBulkFormStatusData(paramMap));
		if(output<0)
		{	Map<String, String> errorMap = new HashMap<String, String>();
			errorMap.put(DATA_NOT_SAVED, MC.M_DataNotSvd_Succ);
			this.errorMap=errorMap;
			return INPUT;
		}
		return SUCCESS;
	}
	
	
}