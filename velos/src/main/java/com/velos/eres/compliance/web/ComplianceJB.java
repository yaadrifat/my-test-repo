package com.velos.eres.compliance.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import com.velos.eres.business.common.StudyApndxDao;
import com.velos.eres.business.common.StudyDao;
import com.velos.eres.business.common.StudySiteDao;
import com.velos.eres.business.common.StudyStatusDao;
import com.velos.eres.business.studyVer.impl.StudyVerBean;
import com.velos.eres.compliance.business.ComplianceDAO;
import com.velos.eres.compliance.service.ComplianceAgent;
import com.velos.eres.compliance.service.ComplianceAgentBean;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.widget.business.common.UIFlxPageDao;
import com.velos.eres.widget.service.util.FlxPageArchive;
import com.velos.esch.service.util.Rlog;

@SuppressWarnings("rawtypes")
public class ComplianceJB {
	private boolean isBlockedForSubmission = false;
	private boolean isLockedStudy = false;
	private boolean isIrbApproved = false;
	private boolean isIrbDisapproved = false;
	private static final String MED_ANC_REV_PEND = "medAncRevPend";
	private static final String MED_ANC_REV_CMP = "medAncRevCmp";
	private static final String CRC_MOD_REQ = "crcReturn";
	private static final String IRB_MOD_REQ = "irb_add_info";
	private static final String CRC_SUBMITTED = "crcSubmitted";
	private static final String CRC_RESUBMITTED = "crcResubmitted";
	private static final String NEW_AMENDMENT = "crc_amend";
	
	// Getters and setters
	public boolean getIsBlockedForSubmission() {
		return isBlockedForSubmission;
	}

	public void setBlockedForSubmission(boolean isBlockedForSubmission) {
		this.isBlockedForSubmission = isBlockedForSubmission;
	}

	public boolean getIsLockedStudy() {
		return isLockedStudy;
	}

	public void setLockedStudy(boolean isLockedStudy) {
		this.isLockedStudy = isLockedStudy;
	}
	
	public boolean getIsIrbApproved() {
		return isIrbApproved;
	}

	public void setIrbAppoved(boolean isIrbApproved) {
		this.isIrbApproved = isIrbApproved;
	}
	
	public boolean getIsIrbDisapproved() {
		return isIrbDisapproved;
	}

	public void setIrbDisappoved(boolean isIrbApproved) {
		this.isIrbDisapproved = isIrbApproved;
	}
	// End of getters and setters
	
	public ArrayList<HashMap<String, Object>> fetchStdSubmissionData(
			HashMap<String, String> paramMap) {
		// TODO route through EJB Service
		ComplianceDAO compDAO = new ComplianceDAO();
		//return compDAO.fetchStdSubmissionData(paramMap);
		return compDAO.fetchStdStatData(paramMap);
	}
	
	private static final String ER_STUDY = "er_study";
	
	public void retrieveBlockSubmissionFlag(HashMap<String, Object> paramMap) {
		int studyId = (Integer)paramMap.get("studyId");
		int userId = (Integer)paramMap.get("userId");
		int accountId = (Integer)paramMap.get("accountId");
	    StudySiteDao ssDao = new StudySiteDao();
	    ssDao.getAllStudyTeamSites(studyId, accountId);
	    ArrayList siteIdList = ssDao.getSiteIds();
	    //ArrayList siteNameList = ssDao.getSiteNames();
	    //String defaultOrgName = StringUtil.trueValue(CFG.EIRB_DEFAULT_STUDY_STAT_ORG);
	    int primarySiteId = ssDao.getPrimarySiteByUserId(userId);
	    isBlockedForSubmission = false;
	    // Go thru history to determine blocking 
	    for (int iX = 0; iX < siteIdList.size(); iX++) {
	    	if (primarySiteId!=(Integer)siteIdList.get(iX)) {
	    		continue;
	    	}
	    	int siteId = (Integer)siteIdList.get(iX);
			StudyStatusDao studyStatusDao = new StudyStatusDao();
			studyStatusDao.getStudyStatusDescNonFuture(studyId, siteId, userId, accountId);
			ArrayList subtypeList = studyStatusDao.getSubTypeStudyStats();
			for (int iY = 0; iY < subtypeList.size(); iY++) {
				String studyStatSubtype = (String)subtypeList.get(iY);
				if (NEW_AMENDMENT.equals(studyStatSubtype)) {
					isBlockedForSubmission = false;
					return;
				}
				if (CRC_SUBMITTED.equals(studyStatSubtype) || 
						CRC_RESUBMITTED.equals(studyStatSubtype)) {
					isBlockedForSubmission = true;
					return;
				}
				if (CRC_MOD_REQ.equals(studyStatSubtype)) {
					isBlockedForSubmission = false;
					return;
				}
				if (IRB_MOD_REQ.equals(studyStatSubtype)) {
					isBlockedForSubmission = false;
					return;
				}
				if (MED_ANC_REV_CMP.equals(studyStatSubtype)) {
					isBlockedForSubmission = false;
					return;
				}
				if (MED_ANC_REV_PEND.equals(studyStatSubtype)) {
					isBlockedForSubmission = true;
					return;
				}
			}
	    }
	}

	public void retrieveLockStudyFlag(HashMap<String, Object> paramMap) {
		int studyId = (Integer)paramMap.get("studyId");
		int userId = (Integer)paramMap.get("userId");
		int accountId = (Integer)paramMap.get("accountId");
	    StudySiteDao ssDao = new StudySiteDao();
	    ssDao.getAllStudyTeamSites(studyId, accountId);
	    ArrayList siteIdList = ssDao.getSiteIds();
	    //ArrayList siteNameList = ssDao.getSiteNames();
	    //String defaultOrgName = StringUtil.trueValue(CFG.EIRB_DEFAULT_STUDY_STAT_ORG);
	    int primarySiteId = ssDao.getPrimarySiteByUserId(userId);
	    isLockedStudy = false;
	    isIrbApproved = false;
		isIrbDisapproved = false;
	    UIFlxPageDao uiFlxDao = new UIFlxPageDao();
	    if(uiFlxDao.chkPageSubmited(studyId)){
	    // Go thru history to determine locking 
	    for (int iX = 0; iX < siteIdList.size(); iX++) {
	    	if (primarySiteId!=(Integer)siteIdList.get(iX)) {
	    		continue;
	    	}
	    	int siteId = (Integer)siteIdList.get(iX);
			StudyStatusDao studyStatusDao = new StudyStatusDao();
			studyStatusDao.getStudyStatusDescNonFuture(studyId, siteId, userId, accountId);
			ArrayList subtypeList = studyStatusDao.getSubTypeStudyStats();
			for (int iY = 0; iY < subtypeList.size(); iY++) {
				String studyStatSubtype = (String)subtypeList.get(iY);				
				if (CFG.EIRB_APPROVED_SUBTYP.equals(studyStatSubtype)) {
					isIrbApproved = true;
					isLockedStudy = true;
					return;
				}
				if (CFG.EIRB_DISAPPROVED_SUBTYP.equals(studyStatSubtype)) {
					isIrbDisapproved = true;
					isLockedStudy = true;
					return;
				}
				if (NEW_AMENDMENT.equals(studyStatSubtype)) {
					//Check Study locked flag here
					StudyDao studyDao = new StudyDao();
					isLockedStudy = studyDao.isStudyLocked(studyId);
					return;
				}
				if (CRC_SUBMITTED.equals(studyStatSubtype) || 
						CRC_RESUBMITTED.equals(studyStatSubtype)) {
					isLockedStudy = true;
					return;
				}
				if (CRC_MOD_REQ.equals(studyStatSubtype)) {
					//Check Study locked flag here
					StudyDao studyDao = new StudyDao();
					isLockedStudy = studyDao.isStudyLocked(studyId);
					return;
				}
				if (IRB_MOD_REQ.equals(studyStatSubtype)) {
					StudyDao studyDao = new StudyDao();
					isLockedStudy = studyDao.isStudyLocked(studyId);
					return;
				}
				if (MED_ANC_REV_PEND.equals(studyStatSubtype)) {
					isLockedStudy = false;
					return;
				}
			}
	    }
	    }
	}

	public boolean isIrbApprovalExistsInStudyStatusHistory(HashMap<String, Object> paramMap) {		
		int studyId = (Integer)paramMap.get("studyId");
		int userId = (Integer)paramMap.get("userId");
		int accountId = (Integer)paramMap.get("accountId");
	    StudySiteDao ssDao = new StudySiteDao();
	    ssDao.getAllStudyTeamSites(studyId, accountId);
	    ArrayList siteIdList = ssDao.getSiteIds();	    
	   
	    for (int iX = 0; iX < siteIdList.size(); iX++) {
	    	int siteId = (Integer)siteIdList.get(iX);
	    	String siteName = ssDao.getSiteNameForSiteId(siteId);
	    	int primarySiteId = ssDao.getPrimarySiteByUserId(userId);
	    	//if(CFG.EIRB_DEFAULT_STUDY_STAT_ORG.equals(siteName)) {
	    	if(siteId==primarySiteId){
	    		StudyStatusDao studyStatusDao = new StudyStatusDao();
				studyStatusDao.getStudyStatusDescNonFuture(studyId, siteId, userId, accountId);
				ArrayList subtypeList = studyStatusDao.getSubTypeStudyStats();
				for (int iY = 0; iY < subtypeList.size(); iY++) {
					String studyStatSubtype = (String)subtypeList.get(iY);
						
					if (CFG.EIRB_APPROVED_SUBTYP.equals(studyStatSubtype)) {
						isIrbApproved = true;						
						break;
					}
				}
				
	    	}
	    }	
	   
	    return isIrbApproved;
	}
	
	public int addStudyStatus(HashMap paramMap) {		
		int pk=0;
		try {
			int studyId = ((Integer)paramMap.get("studyId")).intValue();    	
			Integer usrId = (Integer)paramMap.get("userId");			
			
			ComplianceAgent compAgent = (ComplianceAgent)EJBUtil.getComplianceAgentHome();	 
	    	pk = compAgent.addStudyStatusForAmendment(studyId, usrId.intValue());	    	
	    	//System.out.println("Inside addStudyStatus:::"+pk);
	    	if( pk != 0 ) {
	    		ComplianceJB compB = new ComplianceJB();	    		
	    		compAgent.addSubmissionForAmendment(studyId, usrId, pk);	    		
	    		
	    		boolean isFrozen = getIsLockedStudy();
				if(isFrozen) {
					//System.out.println("before unlock study");
					unlockStudy(paramMap);
					Rlog.info("ComplianceJB:addStudyStatus", "Unlocked Study successfully");
				}
	    		compAgent.makeAttachmentsEditableForAmendment(studyId, usrId);
	    		//updateFileBlobObject(String.valueOf(studyId));

	    		/* Form unlocking. */
		    	paramMap.put(ComplianceAgentBean.SOURCE_STR, ComplianceAgentBean.VERSION_STR);
		    	this.unfreezeFormResponses(studyId, paramMap);

	    		boolean isBlockedForSubmission = getIsBlockedForSubmission();
			    if(isBlockedForSubmission) {
			    	unblockStudy(paramMap);
			    	Rlog.info("ComplianceJB:addStudyStatus", "Unblocked Study successfully.");
			    }
	    	}
		} catch(Exception e) {
			Rlog.error("ComplianceJB:addStudyStatus", "Exception : " + e.getMessage());
		}
    	return pk;
    }
	
	public void unlockStudy(HashMap paramMap) {
		int studyId = (Integer)paramMap.get("studyId");
		int userId = (Integer)paramMap.get("userId");
		int accountId = (Integer)paramMap.get("accountId");
	    StudySiteDao ssDao = new StudySiteDao();
	    ssDao.getAllStudyTeamSites(studyId, accountId);
	    ArrayList siteIdList = ssDao.getSiteIds();
	   
	    for (int iX = 0; iX < siteIdList.size(); iX++) {
	    	int siteId = (Integer)siteIdList.get(iX);
	    	//String siteName = ssDao.getSiteNameForSiteId(siteId);
	    	int primarySiteId = ssDao.getPrimarySiteByUserId(userId);
	    	//if(CFG.EIRB_DEFAULT_STUDY_STAT_ORG.equals(siteName)) {
	    	if(primarySiteId==siteId){
	    		StudyStatusDao studyStatusDao = new StudyStatusDao();
				studyStatusDao.getStudyStatusDescNonFuture(studyId, siteId, userId, accountId);
				ArrayList subtypeList = studyStatusDao.getSubTypeStudyStats();
				for (int iY = 0; iY < subtypeList.size(); iY++) {
					String studyStatSubtype = (String)subtypeList.get(iY);
					if (NEW_AMENDMENT.equals(studyStatSubtype)) {
						isLockedStudy = false;
						return;
					}
				}
	    	}
	    }	
	}
	
	public void unblockStudy(HashMap paramMap) {
		int studyId = (Integer)paramMap.get("studyId");
		int userId = (Integer)paramMap.get("userId");
		int accountId = (Integer)paramMap.get("accountId");
		StudySiteDao ssDao = new StudySiteDao();
	    ssDao.getAllStudyTeamSites(studyId, accountId);
	    ArrayList siteIdList = ssDao.getSiteIds();
	   
	    for (int iX = 0; iX < siteIdList.size(); iX++) {
	    	int siteId = (Integer)siteIdList.get(iX);
	    	//String siteName = ssDao.getSiteNameForSiteId(siteId);
	    	int primarySiteId = ssDao.getPrimarySiteByUserId(userId);
	    	//if(CFG.EIRB_DEFAULT_STUDY_STAT_ORG.equals(siteName)) {
	    	if(siteId==primarySiteId) {
	    		StudyStatusDao studyStatusDao = new StudyStatusDao();
				studyStatusDao.getStudyStatusDescNonFuture(studyId, siteId, userId, accountId);
				ArrayList subtypeList = studyStatusDao.getSubTypeStudyStats();
				for (int iY = 0; iY < subtypeList.size(); iY++) {
					String studyStatSubtype = (String)subtypeList.get(iY);
					if (NEW_AMENDMENT.equals(studyStatSubtype)) {
						isBlockedForSubmission = false;
						return;
					}
				}
	    	}
	    }	
		
	}
	
	public String getCurrentStudyStatus(HashMap paramMap) {
		int studyId = (Integer)paramMap.get("studyId");
		int userId = (Integer)paramMap.get("userId");
		int accountId = (Integer)paramMap.get("accountId");
		String studyStatSubtype="";
		StudySiteDao ssDao = new StudySiteDao();
	    ssDao.getAllStudyTeamSites(studyId, accountId);
	    ArrayList siteIdList = ssDao.getSiteIds();
	   
	    for (int iX = 0; iX < siteIdList.size(); iX++) {
	    	int siteId = (Integer)siteIdList.get(iX);
	    	/*String siteName = ssDao.getSiteNameForSiteId(siteId);
	    	if(CFG.EIRB_DEFAULT_STUDY_STAT_ORG.equals(siteName)) {*/
	    	int primarySiteId = ssDao.getPrimarySiteByUserId(userId);
	    	if(siteId==primarySiteId) {
	    		StudyStatusDao studyStatusDao = new StudyStatusDao();
				studyStatusDao.getStudyStatusDescNonFuture(studyId, siteId, userId, accountId);
				ArrayList subtypeList = studyStatusDao.getSubTypeStudyStats();
				
				if(subtypeList.size() > 0 ) {
					studyStatSubtype = (String)subtypeList.get(0);
					break;
				}				
	    	}
	    }
	    return studyStatSubtype;		
	}
	
	public int getAmendCount(HashMap<String, Object> paramMap) {		
		int studyId = (Integer)paramMap.get("studyId");
		int userId = (Integer)paramMap.get("userId");
		int accountId = (Integer)paramMap.get("accountId");
	    StudySiteDao ssDao = new StudySiteDao();
	    ssDao.getAllStudyTeamSites(studyId, accountId);
	    ArrayList siteIdList = ssDao.getSiteIds();
	    
	    int amendCount=0;
	    for (int iX = 0; iX < siteIdList.size(); iX++) {
	    	int siteId = (Integer)siteIdList.get(iX);
	    	/*String siteName = ssDao.getSiteNameForSiteId(siteId);
	    	if(CFG.EIRB_DEFAULT_STUDY_STAT_ORG.equals(siteName)) {*/
	    	int primarySiteId = ssDao.getPrimarySiteByUserId(userId);
	    	if(siteId==primarySiteId) {
	    		StudyStatusDao studyStatusDao = new StudyStatusDao();
				studyStatusDao.getStudyStatusDescNonFuture(studyId, siteId, userId, accountId);
				ArrayList subtypeList = studyStatusDao.getSubTypeStudyStats();
				amendCount=0;
				for (int iY = 0; iY < subtypeList.size(); iY++) {
					String studyStatSubtype = (String)subtypeList.get(iY);
					if("crc_amend".equals(studyStatSubtype)) {
						amendCount++;
					}					
					if (CFG.EIRB_APPROVED_SUBTYP.equals(studyStatSubtype)) {											
						break;
					}
				}
				
	    	}
	    }	    
	    return amendCount;
	}
	
	public int getIrbApprovedCount(HashMap<String, Object> paramMap) {		
		int studyId = (Integer)paramMap.get("studyId");
		int userId = (Integer)paramMap.get("userId");
		int accountId = (Integer)paramMap.get("accountId");
	    StudySiteDao ssDao = new StudySiteDao();
	    ssDao.getAllStudyTeamSites(studyId, accountId);
	    ArrayList siteIdList = ssDao.getSiteIds();
	    
	    int irbApprovedCount=0;
	    for (int iX = 0; iX < siteIdList.size(); iX++) {
	    	int siteId = (Integer)siteIdList.get(iX);
	    	/*String siteName = ssDao.getSiteNameForSiteId(siteId);
	    	if(CFG.EIRB_DEFAULT_STUDY_STAT_ORG.equals(siteName)) {*/
	    	int primarySiteId = ssDao.getPrimarySiteByUserId(userId);
	    	if(siteId==primarySiteId) {
	    		StudyStatusDao studyStatusDao = new StudyStatusDao();
				studyStatusDao.getStudyStatusDescNonFuture(studyId, siteId, userId, accountId);
				ArrayList subtypeList = studyStatusDao.getSubTypeStudyStats();
				irbApprovedCount=0;
				for (int iY = 0; iY < subtypeList.size(); iY++) {
					String studyStatSubtype = (String)subtypeList.get(iY);
					if (CFG.EIRB_APPROVED_SUBTYP.equals(studyStatSubtype)) {
						irbApprovedCount++;
					}
				}
				
	    	}
	    }	    
	    return irbApprovedCount;
	}
	
	public boolean isIrbDisapprovalExistsInStudyStatusHistory(HashMap<String, Object> paramMap) {		
		int studyId = (Integer)paramMap.get("studyId");
		int userId = (Integer)paramMap.get("userId");
		int accountId = (Integer)paramMap.get("accountId");
	    StudySiteDao ssDao = new StudySiteDao();
	    ssDao.getAllStudyTeamSites(studyId, accountId);
	    ArrayList siteIdList = ssDao.getSiteIds();	    
	   
	    for (int iX = 0; iX < siteIdList.size(); iX++) {
	    	int siteId = (Integer)siteIdList.get(iX);
	    	/*String siteName = ssDao.getSiteNameForSiteId(siteId);
	    	if(CFG.EIRB_DEFAULT_STUDY_STAT_ORG.equals(siteName)) {*/
	    	int primarySiteId = ssDao.getPrimarySiteByUserId(userId);
	    	if(siteId==primarySiteId) {
	    		StudyStatusDao studyStatusDao = new StudyStatusDao();
				studyStatusDao.getStudyStatusDescNonFuture(studyId, siteId, userId, accountId);
				ArrayList subtypeList = studyStatusDao.getSubTypeStudyStats();
				for (int iY = 0; iY < subtypeList.size(); iY++) {
					String studyStatSubtype = (String)subtypeList.get(iY);
						
					if (CFG.EIRB_DISAPPROVED_SUBTYP.equals(studyStatSubtype)) {
						isIrbDisapproved = true;						
						break;
					}
				}
				
	    	}
	    }	
	   
	    return isIrbApproved;
	}
	
	public void updateFileBlobObject(String studyId) {
		int studyPk = Integer.valueOf(studyId);
		HashMap<String, Integer> hMap= new HashMap<String, Integer>();
		ComplianceAgent compAgent = (ComplianceAgent)EJBUtil.getComplianceAgentHome();		
		StudyApndxDao sadpndx = new StudyApndxDao();
		ArrayList<String> arList = sadpndx.getStudyVerNumber(Integer.valueOf(studyId));
		String verStr = (String)arList.get(0);
		String prevVerStr = (String)arList.get(1);
		// Check to make sure the previous version has at least one non-empty attachment
		// If not, go to the previous previous version and so on.
		List<StudyVerBean> studyVerList = compAgent.getHighestStudyVersions(studyPk);
		StudyApndxDao sApndxdao = new StudyApndxDao();
		boolean fileFound = false;
		for (StudyVerBean studyVerBean : studyVerList) {
			prevVerStr = studyVerBean.getStudyVerNumber();
         	int verPk = studyVerBean.getStudyVerId();
         	ArrayList apndxIds = (ArrayList) sApndxdao.getPkByStudyIdAndFkStudyVer(studyPk, verPk);
         	if (apndxIds == null || apndxIds.size() == 0) { continue; }
         	fileFound = sApndxdao.hasNonNullBlob(apndxIds);
         	if (fileFound) { break; }
		}
		hMap = sadpndx.getStudyApndxIdForFileBlob(verStr, prevVerStr, Integer.parseInt(studyId));
    	compAgent.updateFileBlob(hMap, Integer.valueOf(studyId));
	}

	public void unfreezeFormResponses(int studyPK, HashMap<String, Object> paramMap) {
		ComplianceAgentBean compliance = new ComplianceAgentBean();
    	paramMap.put(ComplianceAgentBean.SOURCE_STR, ComplianceAgentBean.VERSION_STR);
    	compliance.unfreezeFormResponses(studyPK, paramMap);
	}

	public void updateFileBlobObjectForIrbApproved(String studyId) {
		if (Integer.valueOf(studyId) != -43215) {
			// This method to copy attachments to x.0 version is no longer required per spec change.
			// Purposely returning here to do nothing.
			return;
		}
		HashMap<String, Integer> hMap= new HashMap<String, Integer>();
		ComplianceAgent compAgent = (ComplianceAgent)EJBUtil.getComplianceAgentHome();	
		StudyApndxDao sadpndx = new StudyApndxDao();
		ArrayList arList = sadpndx.getStudyVerNumber(Integer.valueOf(studyId));
		String prevVer = (String)arList.get(0);
		StringTokenizer str = new StringTokenizer(prevVer, ".");
		String ver="";
		String dec ="";
		while(str.hasMoreElements()) {
			ver = str.nextToken();
			dec = str.nextToken();
		}		
		String version = ver + "." + "0";
		hMap = sadpndx.getStudyApndxIdForFileBlob(version ,prevVer.toString(), Integer.parseInt(studyId));
    	compAgent.updateFileBlob(hMap, Integer.valueOf(studyId));
	}

}
