/*
 * Classname : ComplianceAgent
 * 
 * Version information: 1.0
 *
 * Date: 07/31/2014
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Ashwani Godara
 */

package com.velos.eres.compliance.service;
import javax.ejb.Remote;

import com.velos.eres.business.studyVer.impl.StudyVerBean;

import java.util.HashMap;
import java.util.List;

@Remote
public interface ComplianceAgent
{
	 public int addStudyStatusForAmendment(int studyId, int usrId);
	 public void makeAttachmentsEditableForAmendment(int studyId, int usrId);
	 public void addSubmissionForAmendment(int studyId, int usrId, int studyStatId);
	 public void updateFileBlob(HashMap<String, Integer> hMap , int studyId );
	 public List<StudyVerBean> getHighestStudyVersions(int studyId);
}