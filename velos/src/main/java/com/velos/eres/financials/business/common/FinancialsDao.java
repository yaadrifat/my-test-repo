package com.velos.eres.financials.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.Rlog;

/**
 * @author Sampada Mhalagi
 * Class to hold all gadget DAO methods and to cache all the UI Gadget data into memory.
 *
 */
public class FinancialsDao extends CommonDAO implements java.io.Serializable {
	private static final long serialVersionUID = 4538323410508265062L;
	
	ArrayList studies;
	ArrayList invoiceables;
	ArrayList invoiced;
	ArrayList collected;
	ArrayList receipts;
	ArrayList disbursements;
	
	ArrayList invoices;
	ArrayList invoiceNumbers;
	ArrayList invoiceDates;
	ArrayList paymentDueDates;
	ArrayList paymentDates;
	ArrayList paymentDescs;
	ArrayList paymentNotes;

	public FinancialsDao() {
		studies = new ArrayList();
		invoiceables = new ArrayList();
		invoiced = new ArrayList();
		collected = new ArrayList();
		receipts = new ArrayList();
		disbursements = new ArrayList();

		invoices = new ArrayList();
		invoiceNumbers = new ArrayList();
		invoiceDates = new ArrayList();
		paymentDueDates = new ArrayList();
		paymentDates = new ArrayList();
		paymentDescs = new ArrayList();
		paymentNotes = new ArrayList();
	}

	public ArrayList getStudies(){
		return studies;
	}
	
	void setStudies(String Study){
		studies.add(Study);
	}
	
	void setStudies(ArrayList arrStudies){
		studies = arrStudies;
	}
	
	public ArrayList getInvoiceables(){
		return invoiceables;
	}
	
	void setInvoiceables(String amtInvoiceable){
		invoiceables.add(amtInvoiceable);
	}
	
	void setInvoiceables(ArrayList arrInvoiceables){
		invoiceables = arrInvoiceables;
	}
	
	public ArrayList getInvoiced(){
		return invoiced;
	}
	
	void setInvoiced(String amtInvoiced){
		invoiced.add(amtInvoiced);
	}
	
	void setInvoiced(ArrayList arrInvoiced){
		invoiced = arrInvoiced;
	}

	public ArrayList getCollected(){
		return collected;
	}
	
	void setCollected(String amtCollected){
		collected.add(amtCollected);
	}
	
	void setCollected(ArrayList arrCollected){
		collected = arrCollected;
	}

	public ArrayList getInvoices(){
		return invoices;
	}
	
	void setInvoices(String invoice){
		invoices.add(invoice);
	}
	
	void setInvoices(ArrayList arrInvoices){
		invoices = arrInvoices;
	}
	
	public ArrayList getInvoiceNumbers(){
		return invoiceNumbers;
	}
	
	void setInvoiceNumbers(String invoiceNumber){
		invoiceNumbers.add(invoiceNumber);
	}
	
	void setInvoiceNumbers(ArrayList arrInvoiceNumbers){
		invoiceNumbers = arrInvoiceNumbers;
	}

	public ArrayList getInvoiceDates(){
		return invoiceDates;
	}
	
	void setInvoiceDates(String invoiceDate){
		invoiceDates.add(invoiceDate);
	}
	
	void setInvoiceDates(ArrayList arrInvoiceDates){
		invoiceDates = arrInvoiceDates;
	}

	public ArrayList getPaymentDueDates(){
		return paymentDueDates;
	}
	
	void setPaymentDueDates(String paymentDueDate){
		paymentDueDates.add(paymentDueDate);
	}
	
	void setPaymentDueDates(ArrayList arrPaymentDueDates){
		paymentDueDates = arrPaymentDueDates;
	}
	
	public ArrayList getPaymentDates(){
		return paymentDates;
	}
	
	void setPaymentDates(String paymentDate){
		paymentDates.add(paymentDate);
	}
	
	void setPaymentDates(ArrayList arrPaymentDates){
		paymentDates = arrPaymentDates;
	}
	
	public ArrayList getPaymentDesc(){
		return paymentDescs;
	}
	
	void setPaymentDesc(String paymentDesc){
		paymentDescs.add(paymentDesc);
	}
	
	void setPaymentDesc(ArrayList arrPaymentDesc){
		paymentDescs = arrPaymentDesc;
	}
	
	public ArrayList getPaymentNotes(){
		return paymentNotes;
	}
	
	void setPaymentNotes(String paymentNote){
		paymentNotes.add(paymentNote);
	}
	
	void setPaymentNotes(ArrayList arrPaymentNotes){
		paymentNotes = arrPaymentNotes;
	}
	
	public ArrayList getReceipts(){
		return receipts;
	}
	
	void setReceipts(String amtReceipts){
		receipts.add(amtReceipts);
	}
	
	void setReceipts(ArrayList arrReceipts){
		receipts = arrReceipts;
	}
	
	public ArrayList getDisbursements(){
		return disbursements;
	}
	
	void setDisbursements(String amtDisbursement){
		disbursements.add(amtDisbursement);
	}
	
	void setDisbursements(ArrayList arrDisbursements){
		disbursements = arrDisbursements;
	}

	public void getMyInvoiceables(String studyIds) {
		int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlDetail = new StringBuffer();
        StringBuffer sqlTotal = new StringBuffer();
        try {

            conn = getConnection();
            
            sqlDetail.append("select pk_mileAchieved, pk_milestone, m.fk_study as fk_study, "
            		+ "case " 
            		+ "when (NVL(MILESTONE_COUNT,1) >= -1 AND NVL(MILESTONE_COUNT,1) <= 1) then MILESTONE_AMOUNT "
            		+ "else MILESTONE_AMOUNT/NVL(MILESTONE_COUNT,1) " 
            		+ "end as milestone_amount " 
                	+ "from er_mileAchieved a , er_milestone m " 
                	+ "where pk_milestone = fk_milestone and a.fk_study = m.fk_study "
                    + "and FK_CODELST_MILESTONE_STAT = (SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'milestone_stat' AND codelst_subtyp = 'A') "
                	+ "and m.milestone_paytype in " 
                	+ "(select pk_codelst from er_codelst where codelst_type = 'milepaytype' " 
                	+ "and codelst_subtyp  in ('rec','inv')) and m.fk_study in ("+studyIds+") "
                	+ "and milestone_amount > 0 and NVL(m.MILESTONE_DELFLAG,'Z') <> 'Y' AND IS_COMPLETE = 1");
            sqlDetail.append("UNION ");
            sqlDetail.append("select 0 as pk_mileAchieved, pk_milestone, m.fk_study as fk_study, "
            		+ "MILESTONE_AMOUNT milestone_amount " 
                	+ "from er_milestone m " 
                	+ "where m.MILESTONE_TYPE = 'AM' "
                    + "and FK_CODELST_MILESTONE_STAT = (SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'milestone_stat' AND codelst_subtyp = 'A') "
                	+ "and m.milestone_paytype in " 
                	+ "(select pk_codelst from er_codelst where codelst_type = 'milepaytype' " 
                	+ "and codelst_subtyp  in ('rec','inv')) and m.fk_study in ("+studyIds+")"
                	+ "and milestone_amount > 0 and NVL(m.MILESTONE_DELFLAG,'Z') <> 'Y' ");
            
            sqlTotal.append("select fk_study, TO_CHAR(sum(milestone_amount), '9999999990.99') as invoiceables " 
            	+ "from (");
            sqlTotal.append(sqlDetail);
            sqlTotal.append(") "
            	+ "group by fk_study ");
            
            pstmt = conn.prepareStatement(sqlTotal.toString());
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	setStudies(rs.getString("fk_study"));
            	setInvoiceables(rs.getString("invoiceables"));
                rows++;
                
            }

        } catch (Exception ex) {
            Rlog.fatal("milestone",
                    "FinancialsDao.getMyInvoiceables EXCEPTION IN FETCHING invoiceables "
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }
	}
	
	public void getMyInvoiced(String studyIds) {
		int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlDetail = new StringBuffer();
        try {

            conn = getConnection();
            
            sqlDetail.append("select i.fk_study as fk_study, "
            	+ "TO_CHAR(sum(nvl(AMOUNT_INVOICED,0) + nvl(AMOUNT_HOLDBACK,0)), '9999999990.99') invoiced " 
            	+ "from er_invoice i , er_invoice_detail d , er_milestone m " 
            	+ "where pk_invoice = d.fk_inv and i.fk_study in ("+studyIds+") "
            	+ "and m.pk_milestone = d.fk_milestone and d.detail_type = 'H' and m.FK_CODELST_MILESTONE_STAT = "
            	+ "(SELECT pk_codelst FROM er_codelst WHERE CODELST_TYPE = 'milestone_stat' AND CODELST_SUBTYP = 'A') group by i.fk_study ");
            
            pstmt = conn.prepareStatement(sqlDetail.toString());
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	setStudies(rs.getString("fk_study"));
            	setInvoiced(rs.getString("invoiced"));
                rows++;
                
            }
        } catch (Exception ex) {
            Rlog.fatal("milestone",
                    "FinancialsDao.getMyInvoiced EXCEPTION IN FETCHING invoiced "
                    		+ ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
	}

	public void getMyCollected(String studyIds) {
		int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlDetail = new StringBuffer();
        try {

            conn = getConnection();
            sqlDetail.append("select p.FK_STUDY as fk_study, TO_CHAR(sum(pdet.mp_amount), '9999999990.99') collected " 
            	+ "from er_invoice i, er_milepayment p, er_milepayment_details pdet "
        		+ "WHERE PK_MILEPAYMENT = FK_MILEPAYMENT "
        		+ "AND i.FK_STUDY in ("+studyIds+") AND p.FK_STUDY in ("+studyIds+") "
        		+ "AND pdet.MP_LINKTO_TYPE = 'I' "
        		+ "AND pdet.MP_LINKTO_ID = i.pk_invoice "
            	+ "AND p.MILEPAYMENT_TYPE in (select pk_codelst from er_Codelst where codelst_type = 'paymentCat' and codelst_subtyp = 'payrec')"
            	+ "AND NVL(p.MILEPAYMENT_DELFLAG, 'Z') != 'Y' "
            	+ "group by p.fk_study");
            
            pstmt = conn.prepareStatement(sqlDetail.toString());
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	setStudies(rs.getString("fk_study"));
            	setCollected(rs.getString("collected"));
                rows++;
                
            }
        } catch (Exception ex) {
            Rlog.fatal("milestone",
                    "FinancialsDao.getMyCollected EXCEPTION IN FETCHING collected "
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
	}
	
	/**  
	 * This method will be used for both Receipts and Disbursements
	 * ***/
	public void getMyPayments(String studyIds, String calledFrom) {
		int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlDetail = new StringBuffer();
        try {

            conn = getConnection();
            sqlDetail.append("SELECT p.FK_STUDY as fk_study, TO_CHAR(sum(milepayment_amt), '9999999990.99') mp_amount " 
            	+ "FROM er_milepayment p "
        		+ "WHERE p.FK_STUDY in ("+studyIds+") "
            	+ "AND p.MILEPAYMENT_TYPE in (select pk_codelst from er_Codelst where codelst_type = 'paymentCat' and codelst_subtyp = ?)"
            	+ "AND NVL(p.MILEPAYMENT_DELFLAG, 'Z') != 'Y' "
            	+ "group by p.fk_study");
            
            pstmt = conn.prepareStatement(sqlDetail.toString());
            pstmt.setString(1, ("receipts".equals(calledFrom))?"payrec":"paymade");
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	setStudies(rs.getString("fk_study"));
            	if ("receipts".equals(calledFrom)){
            		setReceipts(rs.getString("mp_amount"));
            	}
            	if ("disbursements".equals(calledFrom)){
            		setDisbursements(rs.getString("mp_amount"));
            	}
                rows++;
                
            }
        } catch (Exception ex) {
            Rlog.fatal("milestone",
                    "FinancialsDao.getMyReceipts EXCEPTION IN FETCHING receipts "
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
	}

	public void getMyDisbursements(String studyIds) {
		int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlDetail = new StringBuffer();
        try {

            conn = getConnection();
            sqlDetail.append("select p.FK_STUDY as fk_study, TO_CHAR(sum(pdet.mp_amount), '9999999990.99') disbursed " 
            	+ "from er_milepayment p, er_milepayment_details pdet "
        		+ "WHERE PK_MILEPAYMENT = FK_MILEPAYMENT "
        		+ "AND p.FK_STUDY in ("+studyIds+") "
        		+ "AND pdet.MP_LINKTO_TYPE = 'I' "
        		+ "AND idet.DETAIL_TYPE = 'H'"
            	+ "AND p.MILEPAYMENT_TYPE in (select pk_codelst from er_Codelst where codelst_type = 'paymentCat' and codelst_subtyp = 'paymade')"
            	+ "AND NVL(p.MILEPAYMENT_DELFLAG, 'Z') != 'Y' "
            	+ "group by p.fk_study");
            
            pstmt = conn.prepareStatement(sqlDetail.toString());
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	setStudies(rs.getString("fk_study"));
            	setDisbursements(rs.getString("disbursed"));
                rows++;
                
            }
        } catch (Exception ex) {
            Rlog.fatal("milestone",
                    "FinancialsDao.getMyDisbursements EXCEPTION IN FETCHING disbursed "
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
	}

	public void getCollectionDrillDownData(int studyId, String calledFrom) {
		int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlDetail = new StringBuffer();
        try {

            conn = getConnection();
            
            if ("invoiced".equals(calledFrom)){
            	sqlDetail.append("select i.FK_STUDY AS fk_study, i.pk_invoice, i.inv_number, ")
	        	.append("TO_CHAR(i.inv_date, PKG_DATEUTIL.F_GET_DATEFORMAT) as inv_date, ")
	        	.append("TO_CHAR((i.inv_date + i.inv_payment_dueby), PKG_DATEUTIL.F_GET_DATEFORMAT) as inv_payment_dueby, ")
	        	.append("TO_CHAR(sum(nvl(amount_invoiced,0) + nvl(amount_holdback,0)), '9999999990.99') amount_invoiced ")
	        	.append("FROM er_invoice i, er_invoice_detail idet ")
	        	.append("WHERE i.pk_invoice = idet.fk_inv ")
	        	.append("AND i.FK_STUDY = ? ")
	        	.append("AND idet.DETAIL_TYPE = 'H' ")
	        	.append("GROUP BY i.FK_STUDY, i.pk_invoice, i.inv_number, inv_date, inv_payment_dueby ");
	            pstmt = conn.prepareStatement(sqlDetail.toString());
	            pstmt.setInt(1, studyId);
            } else {
	            sqlDetail.append("select i.FK_STUDY AS fk_study, i.pk_invoice, i.inv_number, ")
	        	.append("TO_CHAR(milepayment_date, PKG_DATEUTIL.F_GET_DATEFORMAT) as milepayment_date, ")
	        	.append("nvl(pdet.MP_AMOUNT, 0) amount_collected ")
	        	.append("FROM er_invoice i, er_milepayment p, er_milepayment_details pdet ")
	        	.append("WHERE PK_MILEPAYMENT = FK_MILEPAYMENT ")
	        	.append("AND i.FK_STUDY = ? AND p.FK_STUDY = ? ")
	        	.append("AND NVL(pdet.MP_AMOUNT, 0) != 0 ")
	        	.append("AND p.MILEPAYMENT_TYPE in (select pk_codelst from er_Codelst where codelst_type = 'paymentCat' and codelst_subtyp = 'payrec') ")
	        	.append("AND NVL(p.MILEPAYMENT_DELFLAG, 'Z') != 'Y' ")
	        	.append("AND pdet.MP_LINKTO_TYPE = 'I' ")
	        	.append("AND pdet.MP_LINKTO_ID = i.pk_invoice ")
	        	.append("order by i.pk_invoice, p.milepayment_date desc ");
	            pstmt = conn.prepareStatement(sqlDetail.toString());
	            pstmt.setInt(1, studyId);
	            pstmt.setInt(2, studyId);
            }

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	setStudies(rs.getString("fk_study"));
            	setInvoices(rs.getString("pk_invoice"));
            	setInvoiceNumbers(rs.getString("inv_number"));

            	if ("invoiced".equals(calledFrom)){
                	setInvoiceDates(rs.getString("inv_date"));
            		setInvoiced(rs.getString("amount_invoiced"));
            		setPaymentDueDates(rs.getString("inv_payment_dueby"));
            	} else {
                	setPaymentDates(rs.getString("milepayment_date"));
	            	setCollected(rs.getString("amount_collected"));
            	}
                rows++;
                
            }
        } catch (Exception ex) {
            Rlog.fatal("milestone",
                    "FinancialsDao.getCollectionDrillDownData EXCEPTION IN FETCHING collectionDrillDownData "
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
	}

	public void getPaymentsDrillDownData(int studyId, String calledFrom) {
		int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer paymentsSql = new StringBuffer();
        StringBuffer netCashSql = new StringBuffer();
        try {

            conn = getConnection();

            int paramSeq = 1;
            if (!"netCash".equals(calledFrom)){
                paymentsSql.append("select p.FK_STUDY as fk_study, "
                	+ "TO_CHAR(milepayment_date, PKG_DATEUTIL.F_GET_DATEFORMAT) as milepayment_date, "
                	+ "MILEPAYMENT_DESC, MILEPAYMENT_COMMENTS, "
                	+ "TO_CHAR(sum(MILEPAYMENT_AMT), '9999999990.99') mp_amount " 
                	+ "from er_milepayment p "
            		+ "WHERE p.FK_STUDY = ? "
                	+ "AND p.MILEPAYMENT_TYPE in (select pk_codelst from er_Codelst where codelst_type = 'paymentCat' and codelst_subtyp = ?)"
                	+ "AND NVL(p.MILEPAYMENT_DELFLAG, 'Z') != 'Y' "
                	+ "AND NVL(p.MILEPAYMENT_AMT, 0) != 0 "
                	+ "group by p.FK_STUDY, milepayment_date, MILEPAYMENT_DESC, MILEPAYMENT_COMMENTS "
                	+ "order by sum(MILEPAYMENT_AMT) desc");

            	pstmt = conn.prepareStatement(paymentsSql.toString());
                pstmt.setInt(paramSeq++, studyId);               
                pstmt.setString(paramSeq++, ("receipts".equals(calledFrom))?"payrec":"paymade");
            } else {
                paymentsSql.append("select fk_study, milepayment_date, receipts, disbursements from "
                	+ "(select p.FK_STUDY as fk_study, milepayment_date, "
                	+ "sum(MILEPAYMENT_AMT) receipts, 0 disbursements " 
                	+ "from er_milepayment p "
            		+ "WHERE p.FK_STUDY = ? "
                	+ "AND NVL(p.MILEPAYMENT_AMT, 0) != 0 "
                	+ "AND p.MILEPAYMENT_TYPE in (select pk_codelst from er_Codelst where codelst_type = 'paymentCat' and codelst_subtyp = ?)"
                	+ "AND NVL(p.MILEPAYMENT_DELFLAG, 'Z') != 'Y' "
                	+ "group by fk_study, milepayment_date)");
                paymentsSql.append(" UNION ");
                paymentsSql.append("select fk_study, milepayment_date, receipts, disbursements  from "
                	+ "(select p.FK_STUDY as fk_study, milepayment_date, "
                	+ "0 receipts, sum(MILEPAYMENT_AMT) disbursements " 
                	+ "from er_milepayment p "
            		+ "WHERE p.FK_STUDY = ? "
                	+ "AND NVL(p.MILEPAYMENT_AMT, 0) != 0 "
                	+ "AND p.MILEPAYMENT_TYPE in (select pk_codelst from er_Codelst where codelst_type = 'paymentCat' and codelst_subtyp = ?)"
                	+ "AND NVL(p.MILEPAYMENT_DELFLAG, 'Z') != 'Y' "
                	+ "group by fk_study, milepayment_date) ");
                
            	netCashSql.append("select distinct fk_study, milepayment_date, "
            		+ "TO_CHAR(milepayment_date, PKG_DATEUTIL.F_GET_DATEFORMAT) as milepayment_date_formatted, "
            		+ "TO_CHAR(SUM(receipts), '9999999990.99') receipts, "
            		+ "TO_CHAR(SUM(disbursements), '9999999990.99') disbursements from (")           		
            		.append(paymentsSql)
            		.append(") ")
            		.append("group by fk_study, milepayment_date, TO_CHAR(milepayment_date, PKG_DATEUTIL.F_GET_DATEFORMAT) ")
            		.append("order by milepayment_date desc")
            		;
            	pstmt = conn.prepareStatement(netCashSql.toString());
            	pstmt.setInt(paramSeq++, studyId);
                pstmt.setString(paramSeq++, "payrec");
                pstmt.setInt(paramSeq++, studyId);
                pstmt.setString(paramSeq++, "paymade");
            }

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	setStudies(rs.getString("fk_study"));
            	
            	if (!"netCash".equals(calledFrom)){
                	setPaymentDates(rs.getString("milepayment_date"));
            		setPaymentDesc(rs.getString("MILEPAYMENT_DESC"));
            		setPaymentNotes(rs.getString("MILEPAYMENT_COMMENTS"));
            	} else {
                	setPaymentDates(rs.getString("milepayment_date_formatted"));
            	}
            	if ("receipts".equals(calledFrom)){
            		setReceipts(rs.getString("mp_amount"));
            	}
            	if ("netCash".equals(calledFrom)){
            		setReceipts(rs.getString("receipts"));
            	}
            	if ("disbursements".equals(calledFrom)){
            		setDisbursements(rs.getString("mp_amount"));
            	}
            	if ("netCash".equals(calledFrom)){
            		setDisbursements(rs.getString("disbursements"));
            	}
                rows++;
                
            }
        } catch (Exception ex) {
            Rlog.fatal("milestone",
                    "FinancialsDao.getPaymentsDrillDownData EXCEPTION IN FETCHING collectionDrillDownData "
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
	}
}

