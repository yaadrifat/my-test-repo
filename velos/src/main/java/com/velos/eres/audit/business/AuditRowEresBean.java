package com.velos.eres.audit.business;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;

@Entity
@Table(schema="ERES", name = "AUDIT_ROW")
@NamedQueries( {
    @NamedQuery(name = "findLatestAuditRowEresByUserIdRID",
    		query = "SELECT OBJECT(auditRow) FROM AuditRowEresBean auditRow " +
    		" where auditRow.RID = :rid " +
    		" AND (auditRow.userName = :userId OR auditRow.userName like :userIdLike) " +
    		" ORDER BY 1 DESC")
})
public class AuditRowEresBean implements Serializable {
	
	private Integer id;
    private String tableName;
	private Integer RID;
    private String action;
    private Date timeStamp;
    private String userName;

    public AuditRowEresBean(){}
    
    public AuditRowEresBean(Integer id, String tableName, Integer rID,
			String action, Date timeStamp, String userName) {
		super();
		this.id = id;
		this.tableName = tableName;
		RID = rID;
		this.action = action;
		this.timeStamp = timeStamp;
		this.userName = userName;
	}



	public void setId(Integer id) {
        this.id = id;
    }

	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_AUDIT", allocationSize=1)
    @Column(name = "RAID")
    public Integer getId() {
        return id;
    }

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	@Column(name = "TABLE_NAME")
	public String getTableName() {
		return tableName;
	}

	public void setRID(Integer rid) {
		this.RID = rid;
	}
	@Column(name = "RID")
	public Integer getRID() {
		return RID;
	}

	public void setAction(String action) {
		this.action = action;
	}
	@Column(name = "ACTION")
	public String getAction() {
		return action;
	}
	
	public void setTimeStamp(Date timeStamp){
		this.timeStamp=timeStamp;
	}
	@Column(name = "TIMESTAMP")
	public Date getTimeStamp(){
		return this.timeStamp;
	}
	
	public void setUserName(String userName){
		this.userName=userName;
	}
	@Column(name = "USER_NAME")
	public String getUserName(){
		return this.userName;
	}
	
	public int updateAuditRow(AuditRowEresBean ar) {
        try {
        	setId(ar.id);
        	setAction(ar.action);
        	setRID(ar.RID);
        	setTableName(ar.tableName);
        	setTimeStamp(ar.timeStamp);
        	setUserName(ar.userName);

            return 0;
        } catch (Exception e) {
            Rlog.fatal("auditRow", " error in AuditRowEresBean.updateAuditRow : " + e);
            return -2;
        }
    }
}
