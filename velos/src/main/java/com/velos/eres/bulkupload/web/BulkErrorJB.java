package com.velos.eres.bulkupload.web;

import com.velos.eres.bulkupload.business.BulkErrBean;
import com.velos.eres.bulkupload.service.BulkErrorAgent;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

public class BulkErrorJB {
    public int getPkErr() {
		return pkErr;
	}
	public void setPkErr(int pkErr) {
		this.pkErr = pkErr;
	}
	public String getFkBulkUpload() {
		return fkBulkUpload;
	}
	public void setFkBulkUpload(String fkBulkUpload) {
		this.fkBulkUpload = fkBulkUpload;
	}
	public String getFileRowNum() {
		return fileRowNum;
	}
	public void setFileRowNum(String fileRowNum) {
		this.fileRowNum = fileRowNum;
	}
	public String getFileHeaderRowData() {
		return fileHeaderRowData;
	}
	public void setFileHeaderRowData(String fileHeaderRowData) {
		this.fileHeaderRowData = fileHeaderRowData;
	}
	public String getFileErrRowData() {
		return fileErrRowData;
	}
	public void setFileErrRowData(String fileErrRowData) {
		this.fileErrRowData = fileErrRowData;
	}
	private  int pkErr;
    private  String fkBulkUpload;
    private  String fileRowNum;
    private  String fileHeaderRowData;
    private  String fileErrRowData;
    private  String ipAdd;
    private  String creator;
    public String getIpAdd() {
		return ipAdd;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
	/**
     * Default Constructor
     */

    public BulkErrorJB() {
        Rlog.debug("bulkupload", "BulkErrorJBBulkErrorJB() ");
    }
//Parameterized constructor

		public BulkErrorJB(int pkErr,String fkBulkUpload,String fileRowNum,String fileHeaderRowData,String fileErrRowData,String ipAdd,String creator) {
			setPkErr(pkErr);
			setFkBulkUpload(fkBulkUpload);
			setFileRowNum(fileRowNum);
			setFileHeaderRowData(fileHeaderRowData);
			setFileErrRowData(fileErrRowData);
			setIpAdd(ipAdd);
			setCreator(creator);
	}


	  /**
     * Retrieves the details of a bulkUpload in BulkUploadStateKeeper Object
     *
     * @return BulkUploadStateKeeper consisting of the bulkUpload details
     * @see BulkUploadStateKeeper
     */

    public BulkErrBean createErrRowStateKeeper() {
    	Rlog.debug("bulkUpload", "createBulkUploadStateKeeper");
        return new BulkErrBean(this.pkErr, this.fkBulkUpload, this.fileRowNum,
        	    this.fileHeaderRowData, this.fileErrRowData,this.creator,this.ipAdd);
    }
    public BulkErrBean getBulkErrDetails() {
    	BulkErrBean spsk = null;
        try {
        	BulkErrorAgent bulkUploadAgent = EJBUtil.getBulkErrHome();
            spsk = bulkUploadAgent.getBulkErrDetails(this.pkErr);
            Rlog.debug("bulkUpload", "In CsvBulkUpload.getBulkUploadDetails() " + spsk);
        } catch (Exception e) {
            Rlog.fatal("bulkUpload", "Error in getBulkErrDetails()() in BulkErrorJB " + e);
        }
        if (spsk != null) {
        	this.pkErr = spsk.getPkErr();
    	    this.fkBulkUpload = spsk.getFkBulkUpload();
    	    this.fileRowNum = spsk.getFileRowNum();
    	    this.fileHeaderRowData = spsk.getFileHeaderRowData();
    	    this.fileErrRowData= spsk.getFileErrRowData();
    	    this.creator=spsk.getCreator();
    	    this.ipAdd=spsk.getIpAdd();
        }
        return spsk;
    }
    public int setBulkErrDetails() {
        int output = 0;
        try {
        	BulkErrorAgent bulkUploadAgent = EJBUtil.getBulkErrHome();
            output = bulkUploadAgent.setBulkErrDetails(this.createErrRowStateKeeper());
            this.setPkErr(output);
        } catch (Exception e) {
            Rlog.fatal("bulkUpload", "Error in setBulkErrDetails() in BulkErrorJB "   + e);
            return -2;
        }
        return output;
    }


    public int updateBulkUpload() {
        int output;
        try {
        	BulkErrorAgent bulkUploadAgent = EJBUtil.getBulkErrHome();
            output = bulkUploadAgent.updateBulkErr(this.createErrRowStateKeeper());
        } catch (Exception e) {
            Rlog.debug("bulkupload", "Exception setting bulkupload Error details to the BulkErrorJB" + e.getMessage());
            return -2;
        }
        return output;
    }
	

}
