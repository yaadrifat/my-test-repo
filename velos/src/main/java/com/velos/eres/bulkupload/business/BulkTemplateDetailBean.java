package com.velos.eres.bulkupload.business;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.velos.eres.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;
@Entity
@Table(name = "ER_BULK_TEMPLATE_DETAIL")
public class BulkTemplateDetailBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private  int pkTemplateDet;
	private  Integer fkBulkTemplate;
    private  Integer fkBulkEntityDet;
    private  String fileFldName;
    private  Integer lastModBy;
    private  String ipAdd;
    private  String colNum;
    private  String creator;
    
    @Column(name = "creator")
    public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
   
	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "seq_er_bulk_template_detail", allocationSize=1)
    @Column(name = "PK_BULK_TEMPLATE_DETAIL")
	public int getPkTemplateDet() {
		return this.pkTemplateDet;
	}
	public void setPkTemplateDet(int pkTemplateDet) {
		this.pkTemplateDet = pkTemplateDet;
	}
	 @Column(name = "FK_BULK_TEMPLATE_MASTER")
	public String getFkBulkTemplate() {
		return StringUtil.integerToString(this.fkBulkTemplate);
	}
	public void setFkBulkTemplate(String fkBulkTemplate) {
		this.fkBulkTemplate = StringUtil.stringToInteger(fkBulkTemplate);
	}
	 @Column(name = "FK_BULK_ENTITY_DETAIL")
	public String getFkBulkEntityDet() {
		return StringUtil.integerToString(this.fkBulkEntityDet);
	}
	public void setFkBulkEntityDet(String fkBulkEntityDet) {
		this.fkBulkEntityDet = StringUtil.stringToInteger(fkBulkEntityDet);
	}
	 @Column(name = "FILE_FIELD_NAME")
	public String getFileFldName() {
		return this.fileFldName;
	}
	public void setFileFldName(String fileFldName) {
		this.fileFldName = fileFldName;
	}
	 @Column(name = "LAST_MODIFIED_BY")
	public String getLastModBy() {
		return StringUtil.integerToString(this.lastModBy);
	}
	public void setLastModBy(String lastModBy) {
		this.lastModBy = StringUtil.stringToInteger(lastModBy);
	}
	 @Column(name = "IP_ADD")
	public String getIpAdd() {
		return this.ipAdd;
	}
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
	@Column(name = "FILE_FIELD_COLNUM")
	public String getColNum() {
		return colNum;
	}
	public void setColNum(String colNum) {
		this.colNum = colNum;
	}
	 public BulkTemplateDetailBean() {
	    }
	 public BulkTemplateDetailBean( int  pkTemplateDet,String fkBulkTemplate,String fkBulkEntityDet,String fileFldName,String lastModBy,String creator,String ipAdd,String colnum) {
		 		setPkTemplateDet(pkTemplateDet);
		 		setFkBulkTemplate(fkBulkTemplate);
		 		setFkBulkEntityDet(fkBulkEntityDet);
		 		setFileFldName(fileFldName);
		 		setLastModBy(lastModBy);
		 		setIpAdd(ipAdd);
		 		setColNum(colnum);
		 		setCreator(creator);
	}
		    public int updateBulktemplateDet(BulkTemplateDetailBean spsk) {
		        try {
		        	setPkTemplateDet(spsk.getPkTemplateDet());
		        	setFkBulkTemplate(spsk.getFkBulkTemplate());
		        	setFkBulkEntityDet(spsk.getFkBulkEntityDet());
		        	setFileFldName(spsk.getFileFldName());
		        	setLastModBy(spsk.getLastModBy());
		        	setIpAdd(spsk.getIpAdd());
		        	setColNum(spsk.getColNum());
		        	setCreator(spsk.getCreator());
		            return spsk.getPkTemplateDet();
		        } catch (Exception e) {
		            Rlog.fatal("bulkuploadtempalteDet", " error in BulkTemplateDetailBean.updateBulktemplateDet" + e);
		            e.printStackTrace();
		            return -2;
		        }
		    }
}
	