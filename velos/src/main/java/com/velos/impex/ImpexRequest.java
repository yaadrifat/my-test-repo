/**
 * __astcwz_cmpnt_det: Author: Sonia Sahni Date and Time Created: Tue Feb 10
 * 12:07:23 IST 2004 Note: DO NOT DELETE OR MODIFY KONESA CODEWIZARD COMMENTS
 * STARTING WITH ///__astcwz_ and /**__astcwz_
 */
// /__astcwz_model_idt#(0)
// /__astcwz_maxim_idt#(2073)
// /__astcwz_packg_idt#(1046,1047,1048~,,)
package com.velos.impex;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

// /__astcwz_class_idt#(1757!n)
public class ImpexRequest implements Serializable {
    // /__astcwz_attrb_idt#(1758)
    private int reqId;

    // /__astcwz_attrb_idt#(1759)
    private String reqDateTime;

    // /__astcwz_attrb_idt#(1760)
    private String reqStatus;

    // /__astcwz_attrb_idt#(1762)
    private String reqBy;

    // /__astcwz_attrb_idt#(1763)
    private ArrayList expSites;

    // /__astcwz_attrb_idt#(1766)
    private String reqDetails;

    // /__astcwz_opern_idt#(1775)

    private String msgStatus;

    private String siteCode;

    private Hashtable htData;

    private int dataExpId; // will be used for reverse Import. store export id

    // from the SITE

    public void setDataExpId(int dataExpId) {
        this.dataExpId = dataExpId;
    }

    public int getDataExpId() {
        return dataExpId;
    }

    public void setSiteCode(String siteCode) {
        this.siteCode = siteCode;
    }

    public String getSiteCode() {
        return siteCode;
    }

    public void setMsgStatus(String msgStatus) {
        this.msgStatus = msgStatus;
    }

    public String getMsgStatus() {
        return msgStatus;
    }

    public ArrayList getReqModules() {
        return null;
    }

    // /__astcwz_default_constructor
    public ImpexRequest() {
        expSites = new ArrayList();

    }

    // /__astcwz_opern_idt#()
    public int getReqId() {
        return reqId;
    }

    // /__astcwz_opern_idt#()
    public String getReqDateTime() {
        return reqDateTime;
    }

    // /__astcwz_opern_idt#()
    public String getReqStatus() {
        return reqStatus;
    }

    // /__astcwz_opern_idt#()
    public String getReqBy() {
        return reqBy;
    }

    // /__astcwz_opern_idt#()
    public ArrayList getExpSites() {
        return expSites;
    }

    // /__astcwz_opern_idt#()
    public String getReqDetails() {
        return reqDetails;
    }

    // /__astcwz_opern_idt#()
    public void setReqId(int areqId) {
        reqId = areqId;
    }

    // /__astcwz_opern_idt#()
    public void setReqDateTime(String areqDateTime) {
        reqDateTime = areqDateTime;
    }

    // /__astcwz_opern_idt#()
    public void setReqStatus(String areqStatus) {
        reqStatus = areqStatus;
    }

    // /__astcwz_opern_idt#()
    public void setReqBy(String areqBy) {
        reqBy = areqBy;
    }

    // /__astcwz_opern_idt#()
    public void setExpSites(ArrayList aexpSites) {
        expSites = aexpSites;
    }

    // /__astcwz_opern_idt#()
    public void setReqDetails(String areqDetails) {
        reqDetails = areqDetails;
    }

    public void setExpSites(String expSite) {
        expSites.add(expSite);
    }

    public String getExpSites(int idx) {
        if (idx < expSites.size()) {
            return (String) expSites.get(idx);
        } else {
            return "";

        }

    }

    public String[] getExpSitesArray() {

        int size = expSites.size();

        String[] arSite = new String[size];

        for (int i = 0; i < size; i++) {
            arSite[i] = getExpSites(i);

        }

        return arSite;

    }

    public void setHtData(Hashtable htData) {
        this.htData = htData;
    }

    public Hashtable getHtData() {
        return htData;
    }

    /**
     * Resets the object attributes to null so that they can be garbage
     * collected
     */
    public void resetMe() {
        reqDateTime = null;
        reqStatus = null;
        reqBy = null;
        reqDetails = null;
        msgStatus = null;
        siteCode = null;

        // clean hashtable
        if (htData != null) {
            for (Enumeration e = htData.elements(); e.hasMoreElements();) {
                htData.remove(e.nextElement());
            }
        }
        htData = null;
        if (expSites != null) {

            for (int i = 0; i < expSites.size(); i++) {
                expSites.remove(i);
            }
        }
        expSites = null;
    }

}
