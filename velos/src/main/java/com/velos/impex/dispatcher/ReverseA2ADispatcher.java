package com.velos.impex.dispatcher;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.jms.ObjectMessage;
import javax.swing.Timer;

import com.velos.eres.service.util.BrowserRows;
import com.velos.eres.service.util.EJBUtil;
import com.velos.impex.Impex;
import com.velos.impex.ImpexRequest;
import com.velos.jms.Publisher;

public class ReverseA2ADispatcher {
    static ReverseA2ADispatcherInvoker invoker=null;
    static int instanceCount;
    

    public static int getInstanceCount() {
           return instanceCount ;

    }

    public static void startDispatcher() {
        invoker = new ReverseA2ADispatcherInvoker();
        instanceCount = instanceCount + 1;

    }
    public static void stopDispatcher() {
         invoker.StopTimer();
         if (instanceCount > 0 )
         {
        	 instanceCount = instanceCount - 1 ;
         }	 

    }

    public static void main(String arg[]) {

        ReverseA2ADispatcherInvoker invoker = new ReverseA2ADispatcherInvoker();
        try {
            while (System.in.read() != '\n')
                ;
        } catch (IOException ioe) {
            System.out.println(ioe);

        }
    }
}

class ReverseA2ADispatcherInvoker {
    int delay = 10000; // 10 seconds

    int count = 0;

    DispatcherImpl dispatcher = new DispatcherImpl();

    Timer ti = new Timer(delay, new DAL());

    ReverseA2ADispatcherInvoker() {
        System.out.println("Reverse A2A Dispatcher Invoker Activated");
        ti.start();
    }

    public void StopTimer() {
        ti.stop();
        System.out.println("Reverse A2A Dispatcher Invoker Stopped");
    }

    class DAL implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            System.out.println("occurs every 60 secs.");

            long totStudies = 0;

            // get studies and modules for which export is due (er_modexp_setup)
            BrowserRows brStudies = new BrowserRows();
            brStudies = Impex.getExpStudiesInfo();
            totStudies = brStudies.getRowReturned();

            if (totStudies > 0) {
                String study = new String();

                int prevStudyId = 0;
                int studyId = 0;
                int prevRevSponsorPkInt = 0;
                int revSponsorPkInt = 0;

                String module;
                String moduleSubtype;
                String pkRS;
                String dataCount;
                String interval;
                String intervalUnit;
                String retStr;
                String siteCode;
                String studyBackupMsgFolder = "";
                String filePath = "";
                int intStudies = 0;
                int selectedModules = 0;
                String expModule = new String();
                int reqId = 0;
                Hashtable htStudyExport = new Hashtable();
                String revStudySponsorPk = "";

                intStudies = (int) totStudies;

                String[] arStudy = new String[intStudies];
                String[] arModule = new String[intStudies];
                String[] arInterval = new String[intStudies];
                String[] arIntervalUnit = new String[intStudies];
                String[] arRevStudySponsorPk = new String[intStudies];
                ArrayList reqModules = new ArrayList();
                Hashtable htStudyModules = new Hashtable();
                Hashtable htStudySponsorModules = new Hashtable();
                Hashtable htStudySiteCodes = new Hashtable();

                for (int i = 1; i <= totStudies; i++) {
                    study = brStudies.getBValues(i, "FK_STUDY");
                    module = brStudies.getBValues(i, "FK_MODULE");
                    moduleSubtype = brStudies.getBValues(i, "MODULE_SUBTYPE");
                    pkRS = brStudies.getBValues(i, "PK_RS");
                    dataCount = brStudies.getBValues(i, "DATA_COUNT");
                    interval = brStudies.getBValues(i, "RS_INTERVAL");
                    intervalUnit = brStudies.getBValues(i, "RS_INTERVAL_UNIT");

                    siteCode = brStudies.getBValues(i, "SITE_CODE");
                    studyBackupMsgFolder = brStudies.getBValues(i,
                            "BACKUP_MSGFOLDER");
                    revStudySponsorPk = brStudies.getBValues(i,
                            "PK_REV_STUDYSPONSOR");

                    // check if there is any data available for the study and
                    // selected modules

                    dispatcher.sysout("revStudySponsorPk *** : "
                            + revStudySponsorPk);
                    dispatcher.sysout("dataCount *** : " + dataCount);
                    dispatcher.sysout("*************");

                    arStudy[i - 1] = study;
                    arModule[i - 1] = module;
                    arInterval[i - 1] = interval;
                    arIntervalUnit[i - 1] = intervalUnit;
                    arRevStudySponsorPk[i - 1] = revStudySponsorPk;

                    // hold arRevStudySponsorPk - site code mappings
                    htStudySiteCodes.put(revStudySponsorPk, siteCode);

                    studyId = Integer.parseInt(study);
                    revSponsorPkInt = Integer.parseInt(revStudySponsorPk);

                    if (i == 1) {
                        prevStudyId = studyId;
                        prevRevSponsorPkInt = revSponsorPkInt;
                    }

                    if ((prevRevSponsorPkInt != revSponsorPkInt)) {
                        if (reqModules.size() > 0) // if for the study there
                        // are any modules ready for
                        // export
                        {
                            htStudySponsorModules.put(String
                                    .valueOf(prevRevSponsorPkInt),
                                    new ArrayList(reqModules));
                            htStudyModules.put(String
                                    .valueOf(prevRevSponsorPkInt), String
                                    .valueOf(prevStudyId));
                            reqModules.clear();
                        }
                    }

                    // there's any data to be exported for the module or data is
                    // not associated with any study
                    // (studyId == 0)
                    if (EJBUtil.stringToNum(dataCount) > 0 || studyId == 0) {
                        dispatcher.sysout("Got module*************"
                                + moduleSubtype);
                        reqModules.add(moduleSubtype);
                    }

                    prevRevSponsorPkInt = revSponsorPkInt; // for a control
                    // break on study
                    // sponsor pk
                    prevStudyId = studyId;
                } // end of for

                // for the last studysponsor in the loop
                if (reqModules.size() > 0) // if for the study there are any
                // modules ready for export
                {
                    htStudySponsorModules.put(revStudySponsorPk, reqModules);
                    htStudyModules.put(revStudySponsorPk, study);
                    dispatcher
                            .sysout("Added arraylist Last study sponsor pk*************"
                                    + reqModules.size());
                }

                // ///////////////////////////////////

                dispatcher.sysout("htStudySiteCodes.size*************"
                        + htStudySiteCodes.size());

                // iterate through the enumeration of studies

                // get current date time

                Calendar now = Calendar.getInstance();
                String requestDate = "";
                requestDate = "" + now.get(now.DAY_OF_MONTH)
                        + now.get(now.MONTH) + (now.get(now.YEAR) - 1900);
                Calendar calToday = Calendar.getInstance();
                String format = "yyyy-MM-dd HH:mm:ss";
                SimpleDateFormat todayFormat = new SimpleDateFormat(format);
                requestDate = todayFormat.format(calToday.getTime());

                for (Enumeration studySponsorEnum = htStudySponsorModules
                        .keys(); studySponsorEnum.hasMoreElements();) {

                    String expStudy = new String();
                    String expStudySponsor = new String();
                    String expSiteCode = new String();
                    String jmsConnFactory = new String();
                    String jmsTopic = new String();
                    String sponsorAccount = new String();

                    StringBuffer sbReqXML = new StringBuffer();
                    Hashtable htStudyData = new Hashtable();

                    Hashtable htSponsorInfo = new Hashtable();

                    ArrayList expData = new ArrayList();

                    expStudySponsor = (String) studySponsorEnum.nextElement();

                    dispatcher.sysout("Got expStudySponsor*************"
                            + expStudySponsor);

                    ArrayList arSelectedModules = new ArrayList();

                    arSelectedModules = (ArrayList) htStudySponsorModules
                            .get(expStudySponsor);

                    selectedModules = arSelectedModules.size();

                    dispatcher.sysout("Got selectedModules*************"
                            + selectedModules);

                    // prepare export Request

                    expSiteCode = (String) htStudySiteCodes
                            .get(expStudySponsor);
                    expStudy = (String) htStudyModules.get(expStudySponsor);

                    sbReqXML.append("<exp><expStudySponsor>" + expStudySponsor
                            + "</expStudySponsor><study>" + expStudy
                            + "</study><siteCode>" + expSiteCode
                            + "</siteCode><modules>");

                    for (int j = 0; j < selectedModules; j++) {
                        expModule = (String) arSelectedModules.get(j);
                        sbReqXML.append("<module name = \"" + expModule
                                + "\" expAll=\"Y\"></module>");
                    }

                    sbReqXML.append("</modules></exp>");

                    dispatcher.sysout("Export Request for expStudySponsor : "
                            + expStudySponsor + " and study: " + expStudy
                            + "**" + sbReqXML.toString());

                    // Prepare export Request

                    ImpexRequest expReq = new ImpexRequest();
                    Impex impexObj = new Impex();
                    dispatcher.sysout("$$$$impexObj*************isdaemon?"
                            + impexObj.isDaemon());
                    if (impexObj.isDaemon())
                        impexObj.setDaemon(false);

                    expReq.setReqDetails(sbReqXML.toString());
                    expReq.setReqStatus("0"); // export not started
                    expReq.setMsgStatus("0"); // Message Not Delivered
                    impexObj.setRequestType("ER"); // for Reverse Export
                    // set datetime

                    expReq.setReqDateTime(requestDate);

                    // Log this export request

                    impexObj.setImpexRequest(expReq);
                    reqId = impexObj.recordExportRequest();

                    // Add expStudySponsor - expId mapping to htStudyExport

                    htStudyExport.put(expStudySponsor, String.valueOf(reqId));

                    dispatcher.sysout("Export Request for expStudySponsor : "
                            + expStudySponsor + " , study  " + expStudy
                            + "**ID" + reqId);

                    // calls a wrapper method, synchronized, the method will
                    // execute the request
                    // according to the Request Type

                    impexObj.start();
                    try {
                        impexObj.join(); // wait for the thread to complete
                        // its work
                        expData = impexObj.getExportData();
                    } catch (InterruptedException ie) {
                        dispatcher.sysout("got InterruptedException" + ie);
                    }

                    // expData = Impex.impexWrapper(impexObj);

                    // got data in htStudyData !

                    htStudyData.put("data", expData);

                    // get sponsor information for the study

                    htSponsorInfo = Impex
                            .getStudySponsorInformation(expStudySponsor);

                    jmsConnFactory = (String) htSponsorInfo
                            .get("sponsorJMSFactoryPath");
                    jmsTopic = (String) htSponsorInfo
                            .get("sponsorJMSTopicPath");
                    sponsorAccount = (String) htSponsorInfo
                            .get("sponsorAccount");

                    if (!jmsConnFactory.equalsIgnoreCase("localhost")) {
                        // prepare JMS MESSAGE
                        try {

                            dispatcher.sysout("Preparing to send MSG to "
                                    + jmsTopic);
                            Publisher publisher = new Publisher(jmsConnFactory,
                                    jmsTopic);
                            ObjectMessage objMessage = (publisher.getSession())
                                    .createObjectMessage();

                            objMessage.setStringProperty("velosExpId", String
                                    .valueOf(reqId));
                            objMessage.setStringProperty("velosSourceSiteCode",
                                    expSiteCode);
                            objMessage.setStringProperty("sponsorAccount",
                                    sponsorAccount);

                            dispatcher.sysout("DATA SIZE #####**ID"
                                    + htStudyData.size());
                            objMessage.setObject(htStudyData);

                            // before publishing the message, backup!
                            // /////////////////////////////////////////////////////////
                            filePath = studyBackupMsgFolder + "\\expjms"
                                    + String.valueOf(reqId) + ".vel";
                            System.out.println("backup filePath" + filePath);

                            FileOutputStream fos = null;
                            ObjectOutputStream out1 = null;
                            try {
                                fos = new FileOutputStream(filePath);
                                out1 = new ObjectOutputStream(fos);
                                out1.writeObject(objMessage);
                                out1.close();
                                // clean up
                                fos = null;
                                out1 = null;
                            } catch (IOException ex) {
                                System.out
                                        .println("exception in taking MSG backup"
                                                + ex);
                            }
                            System.out.println("Message for export id " + reqId
                                    + " backed up");
                            // Publish message
                            publisher.PublishMessage(objMessage);

                            // Close down your publisher
                            publisher.closeJMSTopicPublisher();
                            publisher.closeJMSConnection();

                            // clean up
                            objMessage = null;

                        } catch (Exception ex) {
                            dispatcher
                                    .sysout("An exception occurred while sending message via JMS: "
                                            + ex);
                            ex.printStackTrace();
                        }
                    } // end if JMS ConnectionFactory path is provided
                    else {
                        // ConnectionFactory is not provided as target may be
                        // accross the firewall

                        dispatcher.sysout("Data Target is not provided ");

                        // create a Hashtable object, set the data

                        Hashtable htRevExpData = new Hashtable();

                        htRevExpData.put("velosExpId", String.valueOf(reqId));
                        htRevExpData.put("velosSourceSiteCode", expSiteCode);
                        htRevExpData.put("sponsorAccount", sponsorAccount);
                        htRevExpData.put("data", htStudyData);
                        dispatcher.sysout("DATA SIZE #####**ID"
                                + htStudyData.size());

                        // before publishing the message, backup!
                        // /////////////////////////////////////////////////////////
                        filePath = studyBackupMsgFolder + "\\expManual"
                                + String.valueOf(reqId) + ".vel";
                        System.out.println("backup filePath" + filePath);

                        FileOutputStream fos = null;
                        ObjectOutputStream out1 = null;
                        try {
                            fos = new FileOutputStream(filePath);
                            out1 = new ObjectOutputStream(fos);
                            out1.writeObject(htRevExpData);
                            out1.close();
                            // clean up
                            fos = null;
                            out1 = null;
                        } catch (IOException ex) {
                            System.out.println("exception in taking MSG backup"
                                    + ex);
                        }
                        System.out.println("Message for export id " + reqId
                                + " backed up");

                        // clean up

                        if (htRevExpData != null) {
                            for (Enumeration x = htRevExpData.elements(); x
                                    .hasMoreElements();) {
                                htRevExpData.remove(x.nextElement());
                            }

                        }
                        htRevExpData = null;

                    }

                    // ***********clean up
                    // time!!!!**************************************
                    impexObj.resetMe();
                    expReq.resetMe();
                    if (expData != null) {

                        for (int i = 0; i < expData.size(); i++) {
                            expData.remove(i);
                        }

                    }

                    impexObj = null;
                    expData = null;
                    expReq = null;

                    if (htSponsorInfo != null) {
                        for (Enumeration x = htSponsorInfo.elements(); x
                                .hasMoreElements();) {
                            htSponsorInfo.remove(x.nextElement());
                        }
                    }
                    htSponsorInfo = null;

                    if (htStudyData != null) {
                        for (Enumeration x = htStudyData.elements(); x
                                .hasMoreElements();) {
                            htStudyData.remove(x.nextElement());
                        }

                    }
                    htStudyData = null;
                    // end of clean
                    // up****************************************************

                } // end of for loop for study enumeration

                // calculate the next start date for the study module, update db
                retStr = Impex.updateRevNextExportData(arRevStudySponsorPk,
                        arModule, arInterval, arIntervalUnit);

                if (retStr != null && (retStr.trim().length() > 0)) {
                    dispatcher
                            .sysout(" Error in updating Next Export Status : "
                                    + retStr);
                }

                // //transfer exported data to sent data, update message, export
                // status

                for (Enumeration studySponsorPkEnum = htStudyExport.keys(); studySponsorPkEnum
                        .hasMoreElements();) {
                    String expReqId = null;
                    String expStudySponsor = null;
                    expStudySponsor = (String) studySponsorPkEnum.nextElement();
                    expReqId = (String) htStudyExport.get(expStudySponsor);

                    retStr = Impex.backupRevExportedData(EJBUtil
                            .stringToNum(expReqId), EJBUtil
                            .stringToNum(expStudySponsor));

                    if (retStr != null && (retStr.trim().length() > 0)) {
                        dispatcher
                                .sysout(" Error in Impex.backupRevExportedData for study sponsor pk: "
                                        + expStudySponsor
                                        + ", expId : "
                                        + expReqId + retStr);
                    }

                }

                // ///////////////////////////////

            } // end of if studies > 0

            dispatcher.sysout(".............Completed");

        }
    }
}

class DispatcherImpl {

    public void sysout(String outString) {
        System.out.println("A2A Dispatcher --> " + outString);
    }

    public DispatcherImpl() {
    }
}
