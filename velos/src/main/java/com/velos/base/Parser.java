package com.velos.base;

import java.io.File;
import java.util.ArrayList;

/**
 * Parser deocdes the messages based on encoding specified. To write a decoding
 * routine,implement <code>Parser</code> and process in getTokens e.g. if the
 * encoding specified is 'delimited' for a file,<code>readLine()</code> read
 * the line and <code>getTokens</code> can decode the message based on
 * encoding specified. one scenario would be to extend VelosIO which already
 * implement all the processing for file reading/writing and make change in
 * getTokens to process file.
 * 
 * @see com.velos.base.DlmParser
 * @see com.velos.base.VelosIO
 * 
 * 
 */
public interface Parser {
    public String readLine();

    public void writeLine(String line);

    public void close();

    public boolean EOF();
    
    public ArrayList getTokens();

    /**
     * Method set the Parser with incoming file name(dataset) and a delimiter(if
     * specified)
     * 
     * @param fileName -
     *            full absolute path to the file.
     * @param delimiter -
     *            delimiter specified for file,optional.
     */
    public void setProperties(String fileName, String delimiter);

    /**
     * Method set the Parser with incoming file name(dataset) and operation
     * mode.
     * 
     * @param fileName -
     *            full absolute path to the file.
     * @param operation -
     *            Whether file is used for reading=1 or writing=2
     */
    public void setProperties(String fileName, int operation);

    /**
     * Return the current dataset element<-> File row in processed.
     */
    public String getCurrentBuffer();

    public void setCurrentBuffer(String currentBuffer);

    public boolean MoveFile(String sourceFile, String destFile, String Path);

    public boolean MoveFile(String sourceFile, String Path);

    public boolean MoveFile(File sourceFile, String destFile, String Path);

}
