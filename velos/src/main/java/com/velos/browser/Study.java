package com.velos.browser;
 
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.group.GroupJB;

public class Study extends base{

public Study()
 {
  
 }

  public  void getAdvSearchSQL(HttpServletRequest request)
	    {
	   int spacePos = -1;
	    String searchFilter="",searchFname="",searchLname="";
	     if (request==null)
	     {
	    	 this.setMainSQL("");
	    	 this.setCountSQL("");
	    	 
	     }
	 	String searchCriteria = StringUtil.trueValue(request.getParameter("searchCriteria1"));
	 	String tarea=  StringUtil.trueValue(request.getParameter("Tarea"));
	 	String team=  StringUtil.trueValue(request.getParameter("team")).trim();
	 	String sec=StringUtil.trueValue(request.getParameter("sec"));
	 	String kywrd= StringUtil.trueValue(request.getParameter("kywrd"));
	 	String dmg= StringUtil.trueValue(request.getParameter("dmg")).trim();
	 	String appndx= StringUtil.trueValue(request.getParameter("appndx"));
	 	String stdtype= StringUtil.trueValue(request.getParameter("stdtype"));
	 	String stdnumb= StringUtil.trueValue(request.getParameter("stdnumb"));
	 	String stdphase= StringUtil.trueValue(request.getParameter("stdphase"));
	 	String stdstatus= StringUtil.trueValue(request.getParameter("stdstatus"));
	 	String sponsor= StringUtil.trueValue(request.getParameter("sponsor"));
	 	String psites= StringUtil.trueValue(request.getParameter("psites"));
	 	String morestddetails = StringUtil.trueValue(request.getParameter("morestddetails"));
	 	String excldprmtclsr =  StringUtil.trueValue(request.getParameter("excldprmtclsr"));
	 	String userId= StringUtil.trueValue((String)request.getSession(false).getAttribute("userId"));
	 	String accountId= StringUtil.trueValue((String)request.getSession(false).getAttribute("accountId"));
	 	String pmtclsId=StringUtil.trueValue(request.getParameter("pmtclsId"));
	 	String speciRights=StringUtil.trueValue(request.getParameter("speciRight"));
	 	String fnclRights=StringUtil.trueValue(request.getParameter("fnclRight"));
	 	String invRight=request.getParameter("invRight");
	 	// Added by Ganapathy on 04-20-05 for Organization Search
	 	String org=StringUtil.trueValue(request.getParameter("org"));
	 	//FIX #5685 -Introduced for a PI filter
	 	String onlyPIsearchFilter = "";
	 	
		GroupJB groupJB = new GroupJB();
		String superuserRights =  StringUtil.trueValue(groupJB.getDefaultStudySuperUserRights(userId));
	 	String currentStatus=StringUtil.trueValue(request.getParameter("currentStat"));
	 	String statusType=StringUtil.trueValue(request.getParameter("statusType"));
	 	String diseaseSite=StringUtil.trueValue(request.getParameter("diseaseSite"));
	 	String agent_device=StringUtil.trueValue(request.getParameter("agent_device"));
	 	String researchType = StringUtil.trueValue(request.getParameter("researchType"));
	 	String division = StringUtil.trueValue(request.getParameter("division"));
		String studysites = StringUtil.trueValue(request.getParameter("studysites"));
		String excldinactive = StringUtil.trueValue(request.getParameter("excldinactive"));
	 	
	     String studySql="";
	     String countSql;
	     	
 
	   
	   
	   CodeDao  cd = new CodeDao();
	   String codeLst = cd.getValForSpecificSubType();

	   //System.out.println("codeLst" + codeLst);



	   		//------------Added on Ganapathy  on 04-22-05 for Organization Search-------------------
	   		if (org.length()>0) {            
	   			
	   	              
	   	                if (searchFilter.length()>0 ){
	   	            searchFilter= searchFilter + " and lower(d.SITE_NAME) like lower( '%" + org.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' " ; 
	   	            
	   	                    } //end if searchfilter-org
	   	            else {
	   	            searchFilter= " where lower(d.SITE_NAME) like lower( '%" + org.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' " ;     
	   	            
	   	            } //end else org

	   	        } //end org			
	   			 	
	   	        if (tarea.length()>0) { 
	   	            
	   	            if (searchFilter.length()>0 ) {
	   	            searchFilter= searchFilter + " and lower(study_tarea) like lower( '%" + tarea.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' " ; 
	   	            
	   	            } //end if searchfilter-tarea
	   	            else  {
	   	            searchFilter= " where lower(study_tarea) like lower( '%" + tarea.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' " ;     
	   	            
	   	            } //end else  searchfilter-tarea
	   	        } //end tarea
	   	        
	   	        
	   	        if (diseaseSite.length()>0) {
	   	        	
	   	        	if (searchFilter.length()>0 ){
	 	   	      	   searchFilter= searchFilter +   " and " ;
	 	   	      }else
	 	   	      	  {
	 	   	      	   searchFilter= searchFilter +   " Where " ;
	 	   	      	   
	 	   	      	  }
	   	        	
	   	        	searchFilter = searchFilter + "   lower(STUDY_DISEASE_SITE) like lower( '%" + diseaseSite.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' " ;
	   	        	
	   	             
	   	        }
	   	        
	   	      if (division.length()>0) {
	   	        	
	   	        	if (searchFilter.length()>0 ){
	 	   	      	   searchFilter= searchFilter +   " and " ;
	 	   	      }else
	 	   	      	  {
	 	   	      	   searchFilter= searchFilter +   " Where " ;
	 	   	      	   
	 	   	      	  }
	   	        	
	   	        	searchFilter = searchFilter + "   lower(STUDY_DIVISION_desc) like lower( '%" + division.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' " ;
	   	        	
	   	             
	   	        }
	   	      
	   	   if (researchType.length()>0) {
  	        	
  	        	if (searchFilter.length()>0 ){
	   	      	   searchFilter= searchFilter +   " and " ;
	   	      }else
	   	      	  {
	   	      	   searchFilter= searchFilter +   " Where " ;
	   	      	   
	   	      	  }
  	        	
  	        	searchFilter = searchFilter + "   lower(study_restype) like lower( '%" + researchType.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' " ;
  	        	
  	             
  	        }
	   	     
	   	     if (agent_device.length()>0) {
	   	        	
	   	        	if (searchFilter.length()>0 ){
	 	   	      	   searchFilter= searchFilter +   " and " ;
	 	   	      }else
	 	   	      	  {
	 	   	      	   searchFilter= searchFilter +   " Where " ;
	 	   	      	   
	 	   	      	  }
	   	        	
	   	        	searchFilter = searchFilter + "   lower(agent_device) like lower( '%" + agent_device.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' " ;
	   	        	
	   	             
	   	        }
	   	     
	   	  //include study sites filter
		   	 
	 	   	if (studysites.length()>0) {
	 	   		
	 	   	if (searchFilter.length()>0 ){
	   	      	   searchFilter= searchFilter +   " and " ;
	   	      }else
	   	      	  {
	   	      	   searchFilter= searchFilter +   " Where " ;
	   	      	   
	   	      	  }
	 	   		
	 	   		searchFilter = searchFilter + " lower(study_sites) like lower('%"+studysites.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' " ;
	 	   	}
	   	     
	   	        if (kywrd.length()>0) {
	   	             if (searchFilter.length()>0 ) {
	   	            searchFilter= searchFilter + " and lower(d.study_keywrds) like lower( '%" + kywrd.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' " ; 
	   	            
	   	                 } //end if searchfilter-kywrd
	   	            else {
	   	            searchFilter= " where lower(d.study_keywrds) like lower( '%" + kywrd.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' " ;     
	   	            
	   	            } //end else kywrd
	   	        } //end kywrd
	   	        
	   	        if (! StringUtil.isEmpty(searchCriteria)) //search in title,study number and keywords
	   	        {
	   	        	
	   	        	if (searchFilter.length()>0 ) {
		   	            searchFilter= searchFilter + " and  " ; 
		   	            
		   	                 } 
		   	            else {
		   	            	searchFilter= searchFilter + " Where " ;
		   	            
		   	            } //end else 
	   	        	
	   	         //17/5/2010 - bug no 4000 fixed by BK 
	   	 		    String newSearchCriteria = StringUtil.replace(searchCriteria,"'","''");	
	   	         	searchFilter= searchFilter+ "  ( lower(d.study_keywrds) like lower( '%" + newSearchCriteria.replaceAll("%","/%").replaceAll("_","/_") + "%') escape '/' " +
	   	         			" or lower(d.study_number) like lower( '%" + newSearchCriteria.replaceAll("%","/%").replaceAll("_","/_") + "%') escape '/' or " +
	   	         					" lower(d.study_title) like lower( '%" + newSearchCriteria.replaceAll("%","/%").replaceAll("_","/_") + "%') escape '/'  )" ;
	   	        	
	   	        	
	   	        }
	   	        
	   	        //incase of search of Principal Investigator, the name should be searched in study summary, other and study team
	   	        if (dmg.length()>0){
	   	        	
	   	        	spacePos = dmg.lastIndexOf(" ");

			   	  	   if (spacePos==-1) 
			   	  	   {
			   	  	     searchLname = dmg.toLowerCase().trim();
			   	  	     searchFname = dmg.toLowerCase().trim();
			   	  	   }
			   	  	   else 
			   	  	   {
			   	  	     searchFname = dmg.substring(0,spacePos);  
			   	  	     searchLname = dmg.substring(spacePos);
			   	  	     
			   	  	     searchFname = searchFname.toLowerCase().trim();
			   	  	     searchLname = searchLname.toLowerCase().trim();  
			   	  	   }

	   	            //FIX #5685
			   	  	onlyPIsearchFilter = "  and ( exists (select * from er_user xx where xx.pk_user = a.study_prinv and"
	   	       		+ " (lower(xx.usr_firstname) like ('%"+searchFname.replaceAll("'", "''").replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' or lower(xx.usr_lastname) like ('%"+searchLname.replaceAll("'", "''").replaceAll("_","/_").replaceAll("%","/%")+"%') escape '/' ) ) or"
	   	       		+ " lower(a.study_otherprinv) like lower('%"+dmg.replaceAll("'", "''").replaceAll("_","/_").replaceAll("%","/%")+"%') escape '/' )";
	   	    		 
	   	        }//dmg end
	   	        
	   	        if (stdtype.length()>0) { 
	   	                if (searchFilter.length()>0 ){
	   	            searchFilter= searchFilter + " and lower(d.study_type) like lower( '%" + stdtype.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' " ; 

	   	                    } //end searchfilter-stdtype
	   	            else {
	   	            searchFilter= " where lower(d.study_type) like lower( '%" + stdtype.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' " ;     
	   	            
	   	            }//else searchfilter-stdtype
	   	        } //end stdtype
	   	        
	   	        if (sponsor.length()>0) { 
	   	                if (searchFilter.length()>0 ){
	   	            searchFilter= searchFilter + " and  (lower(d.study_sponsor) like lower( '%" + sponsor.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' " ;
	   	             
	   	                    } // end searchfilter-sponsor
	   	            else {
	   	            searchFilter= " where ( lower(d.study_sponsor) like lower( '%" + sponsor.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' " ;     
	   	            
	   	            
	   	            } //end else sponsor
	   	            
	   	            searchFilter= searchFilter + " or lower(f_codelst_desc(d.fk_codelst_sponsor)) like lower( '%" + sponsor.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' ";
	   	            searchFilter= searchFilter + " or lower(d.study_sponsorid) like lower( '%" + sponsor.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' ) " ;
	   	            
	   	            
	   	            
	   	        } //end sponsor
	   	        if (stdnumb.length()>0) { 
	   	                if (searchFilter.length()>0 ){
	   	            searchFilter= searchFilter + " and lower(d.study_number) like lower( '%" + stdnumb.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' " ; 
	   	            
	   	                    } //end if searchfilter-stdnumb
	   	            else {
	   	            searchFilter= " where lower(d.study_number) like lower( '%" + stdnumb.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' " ;     
	   	            
	   	            } //end else stdnumb
	   	        } //end stdnumb
	   	        
	   	        
	   	        if (psites.length()>0) { 
	   	                if (searchFilter.length()>0 )
	   	                {
	   	            searchFilter= searchFilter + " and lower(d.study_partcntr) like lower( '%" + psites.replaceAll("_","/_").replaceAll("%","/%")+ "%') escape '/' " ; 
	   	            
	   	                    } //end if searchfilter-psites
	   	            else {
	   	            searchFilter= " where lower(d.study_partcntr) like lower( '%" + psites.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' " ;     
	   	            
	   	            } //end else psites
	   	        } //end psites
	   	        
	   	        if (team.length()>0) 
	   	        {
	   	        	  spacePos = team.lastIndexOf(" ");

			   	  	   if (spacePos==-1) 
			   	  	   {
			   	  	     searchLname = team.toLowerCase().trim();
			   	  	     searchFname = team.toLowerCase().trim();
			   	  	   }
			   	  	   else 
			   	  	   {
			   	  	     searchLname = team.substring(spacePos);  
			   	  	     searchFname = team.substring(0,spacePos);
			   	  	     
			   	  	     searchFname = searchFname.toLowerCase().trim();
			   	  	     searchLname = searchLname.toLowerCase().trim();  
			   	  	   }
	   	        
			   	        if (searchFilter.length()>0 )
			   	        {
			   	        	searchFilter= searchFilter + " and  " ;
			   	        }
			   	        else
			   	        {
			   	        	searchFilter= searchFilter + " Where   " ;
			   	        	
			   	        }
		 	   	        	     
			   	        	searchFilter= searchFilter + "  ( exists (select * from er_studyteam,er_user where fk_study = pk_study and fk_user = pk_user and "+
			   			"( lower(usr_firstname) like lower('%"+searchFname.replaceAll("'", "''").replaceAll("_","/_").replaceAll("%","/%")+"%') escape '/' or lower(usr_lastname) like  lower('%"+searchLname.replaceAll("'", "''").replaceAll("_","/_").replaceAll("%","/%")+"%') escape '/' or "+
                        "  lower(usr_firstname) like lower('%"+searchLname.replaceAll("'", "''").replaceAll("_","/_").replaceAll("%","/%")+"%') escape '/' or lower(usr_lastname) like  lower('%"+searchFname.replaceAll("'", "''").replaceAll("_","/_").replaceAll("%","/%")+"%') escape '/' "+
			   			"  ) ) ) ";  
			   			
	   	             
	   	        } //end team
	   	        
	   	    if (sec.length()>0){ //modified by sonia abrol 02/02/06, to append the filter to main chdSql
	   	        
	   	    	if (searchFilter.length()>0 ){
	   	      	   searchFilter= searchFilter +   " and " ;
	   	      }else
	   	      	  {
	   	      	   searchFilter= searchFilter +   " Where " ;
	   	      	   
	   	      	  } 
	   	    		   	      	  
	   	            searchFilter= searchFilter
	   	            + " exists ( select * from er_studysec asec where asec.fk_study = pk_study and (lower(asec.studysec_name) like lower('%" + sec.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' or lower(asec.studysec_text) like lower('%" + sec.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' )) "    ;
	   	        
	   	        
	   		} //end sec
	   	        

	   	/********************************************************************************** */

	   	if (morestddetails.length()>0) {     
	   	    
	   	            
	   	            if (searchFilter.length()>0 ){
	   	      	   searchFilter= searchFilter +   " and " ;
	   	      }else
	   	      	  {
	   	      	   searchFilter= searchFilter +   " Where " ;
	   	      	   
	   	      	  } 
	   	         //Ashu modified for BUG#5812(9Feb11).
	   	            searchFilter= searchFilter +	" exists ( select * from er_studyid i, er_Codelst c where 	pk_study = i.fk_study and " + 	
	   				" c.codelst_type = 'studyidtype' and i.fk_codelst_idtype = c.pk_codelst " +
	   				" and ((lower(i.studyid_id) like lower('%" + morestddetails.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' )  or ((lower(c.codelst_desc) like lower('%" + morestddetails.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' ) and i.studyid_id = 'Y') )  )";
	   					
	   	            
	   	        } //end morestddetails



	   	/*********************************************************************************** */
 	   	 
	   	if (statusType.length()>0) {
	   	      
	   	      
	   	       if (searchFilter.length()>0 ){
	   	      	   searchFilter= searchFilter +   " and " ;
	   	      	   
	   	      }else
	   	      	  {
	   	      	   searchFilter= searchFilter +   " Where " ;
	   	      	   
	   	      	  } 
	   	      	  
	   	     searchFilter=searchFilter + " (lower(status_type) like lower('%" + statusType.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' ) " ;
	   	       
	   	 }
	   	 
	   	  if (appndx.length()>0) 
	   	  {
	   	      
	   	      
	   	      if (searchFilter.length()>0 ){
	   	      	   searchFilter= searchFilter +   " and " ;
	   	      	   
	   	      }else
	   	      	  {
	   	      	   searchFilter= searchFilter +   " Where " ;
	   	      	   
	   	      	  } 
	   	           searchFilter= searchFilter +   " exists ( select * from er_studyapndx aapndx,er_studyver ver  where pk_study = ver.fk_study " + 
	   	  	" and ver.pk_studyver = aapndx.fk_studyver and ( lower(aapndx.studyapndx_uri) like lower('%"+appndx.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' or     lower(aapndx.studyapndx_desc) like lower('%"+appndx.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' " +
	   	 " or     lower(aapndx.studyapndx_uri) like lower('%"+appndx.replaceAll("_","/_").replaceAll("%","/%") + "%') escape '/' ))";
	   		
	   		  
	   	 } // end if for appndx     
	   	        

	   

	   	//modified by sonia abrol, 02/02/06, to remove unnecessary studyteam join
	   	//Modified by Manimaran to fix the Bug2706
	  studySql= "select  distinct pk_study as rowcount,pk_study,study_number,study_title,phase,tarea,site_name,status,status_subtype,studystat_note,study_actualdt as study_actualdt_datesort, studystat_date as studystat_date_datesort, "+
	   	 	 "  (nvl((select study_team_rights from er_studyteam where fk_user = " + userId +" and " 
	   		+ " fk_study = pk_study  and study_team_usr_type = 'D'), '"+superuserRights+"' ) )  study_team_rights  ,'"+speciRights+"' as specimen_rights,'"+fnclRights+"' as financial_rights,'"+invRight+"' as inv_rights, sponsor_name , study_restype, "+
	   		" STUDY_DIVISION_desc,STUDY_DISEASE_SITE, PI_NAME, PI_DETAILS, study_type ,agent_device ,study_sites,status_type ,current_stat_desc ," +
	   		" patcount as patcount_numsort, ae_count as ae_count_numsort, sae_count as sae_count_numsort" +
	   		" from ( " + 
	   		" SELECT  a.study_title as study_title, a.pk_study,  " +
	   		"  (select ER_CODELST.CODELST_DESC from ER_CODELST where ER_CODELST.PK_CODELST=a.fk_codelst_phase) phase,"+
	   		" (select ER_CODELST.CODELST_DESC from ER_CODELST where ER_CODELST.PK_CODELST=a.fk_codelst_tarea) tarea,  "
//	  sonia 02/4/08 - new columns
	   	+ " F_Get_Codelstdesc(a.fk_codelst_sponsor)  sponsor_name,  "
	   	+ " F_Get_Codelstdesc(a.STUDY_DIVISION) STUDY_DIVISION_desc, "
	   	+ " F_Getdis_Site(STUDY_DISEASE_SITE) STUDY_DISEASE_SITE , "
	   	+ "   study_prodname as agent_device,F_Get_Studysites(a.pk_study) study_sites, (select F_Get_Codelstdesc(xx.fk_codelst_studystat) from er_studystat xx where xx.fk_study = a.pk_study and xx.current_stat = 1  and rownum < 2) current_stat_desc, "
	   	+ " ( select count(distinct fk_per) from er_patprot pp where pp.fk_study = a.pk_study and patprot_stat = 1) patcount,"
	   	+ " ( select count(pk_adveve) from sch_adverseve pp where pp.fk_study = a.pk_study ) ae_count,"
	   	+ " ( select count(pk_adveve) from sch_adverseve pp , sch_codelst where pp.fk_study = a.pk_study and pk_codelst = fk_codlst_aetype and trim(codelst_subtyp) = 'al_sadve') sae_count,"
	   	//KM-to fix Bug2537
	   	+ "(case when (e.fk_site=(select fk_siteid from er_user where pk_user="+userId+") and e.fk_codelst_studystat  in ("+codeLst+")) then (select site_name from er_site where pk_site=e.fk_site) else '-' end) as site_name," 

	   	 
	   	+" ( case when  (select max(studystat_date) from er_studystat r  where fk_study=e.fk_study" 
	   	+ " and e.fk_codelst_studystat  in("+codeLst+") and fk_site=(select fk_siteid from er_user where pk_user="+userId+") ) "
	   		+ " is not null then  (select codelst_desc from er_codelst where pk_codelst in("+codeLst+") "
	   		+ " and pk_codelst = e.fk_codelst_studystat)   else  '..' end) "
	   		+ " as status, " 
	   	 
	   		  +" trim(c.CODELST_SUBTYP) status_subtype,"
	   	 
	   	     //KM-to fix Bug2537
	   	     +" (case when (e.fk_site=(select fk_siteid from er_user where pk_user="+userId+") and e.fk_codelst_studystat  in ("+codeLst+")) then e.studystat_note else '-' end) as studystat_note,"
	   		  	  
	   	     +" a.study_actualdt,"
	   	     +" (case when (e.fk_site=(select fk_siteid from er_user where pk_user="+userId+") and e.fk_codelst_studystat  in ("+codeLst+")) then nvl(F_Get_Codelstdesc(e.status_type),'-')  else '-' end) as status_type," 
	   	    
	   	    //KM-to fix Bug2536
	   	    +" (case when (e.fk_site=(select fk_siteid from er_user where pk_user="+userId+") and e.fk_codelst_studystat  in ("+codeLst+")) then to_char(e.studystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) else ' ' end) as studystat_date,"
	   	    
	   	    
	   	    //JM: 28Sept2006: modified a.study_obj,a.study_sum fields chnaged to a.study_obj_clob,a.study_sum_clob
	   	    +" a.study_number , a.study_obj_clob, "  + 
	   	    "  a.study_sum_clob, a.study_keywrds, "+
	   	    " (select USR_FIRSTNAME || ' '|| USR_LASTNAME from er_user where pk_user = a.study_prinv) PI_name, " + 
	   	    " a.study_otherprinv, " +      
	   	    " (SELECT USR_FIRSTNAME||' '|| USR_MIDNAME ||' '|| USR_LASTNAME||' '|| ad.ADD_EMAIL||' '||USR_LOGNAME FROM er_user u, er_add ad WHERE u.pk_user=a.study_prinv and u.fk_peradd=ad.pk_add) PI_DETAILS, "+
	   	     "   (select codelst_desc from er_codelst where pk_codelst =a.fk_codelst_tarea) study_tarea,"+
	   	    " a.study_partcntr,"+
	   	 "  (select codelst_desc from er_codelst where pk_codelst = a.FK_CODELST_restype) study_restype"+
	   	    "   ,(select codelst_desc from er_codelst where pk_codelst = FK_CODELST_TYPE) study_type,a.study_sponsor,a.fk_codelst_sponsor,a.study_sponsorid  " + 
	   	    " FROM er_study a ,  er_Codelst  c, er_studystat e     "+
	   	   "  WHERE a.pk_study = e.fk_study   ";

	 	//FIX #5685 -Moved code above to improve performance
	   	studySql = studySql + " and a.fk_account = " + accountId + " and  ( exists ( select * from er_studyteam where fk_study = pk_study and " 
   		+ " fk_user = "+userId+" and  nvl(study_team_usr_type,'D')='D') or pkg_superuser.F_Is_Superuser("+userId+", pk_study) = 1 )  " 
   		+ onlyPIsearchFilter;
	   	
	   	studySql = studySql + " and e.FK_CODELST_STUDYSTAT = c.pk_codelst and c.codelst_type = 'studystat'  "+
	   	// anu 27th may

	   	//Modified by Manimaran to fix Bug 2528-Login User's Defaults Organization's Status to be displayed in browsers.
	   	 " AND e.pk_studystat = "
	   	+ " (CASE WHEN (SELECT COUNT(*) FROM ER_STUDYSTAT WHERE fk_study = e.fk_study AND fk_codelst_studystat IN ( "+codeLst+") and fk_site=(select fk_siteid from er_user where pk_user="+userId+")) > 0 THEN"
	   	+ " (SELECT MAX(pk_studystat) FROM ER_STUDYSTAT WHERE fk_study = e.fk_study and fk_site=(select fk_siteid from er_user where pk_user="+userId+")  AND fk_codelst_studystat IN ( "+codeLst+")  AND studystat_date = (SELECT MAX(studystat_date) FROM ER_STUDYSTAT WHERE  fk_codelst_studystat IN ( "+codeLst+")   AND fk_study = e.fk_study and fk_site=(select fk_siteid from er_user where pk_user="+userId+")))"
	   	  
	   	+"WHEN (SELECT COUNT(*) FROM ER_STUDYSTAT WHERE fk_study = e.fk_study AND fk_codelst_studystat NOT IN ("+codeLst+")  and fk_site=(select fk_siteid from er_user where pk_user="+userId+")) > 0 THEN (SELECT MAX(pk_studystat) FROM ER_STUDYSTAT WHERE fk_study = e.fk_study  and fk_site=(select fk_siteid from er_user where pk_user="+userId+")  AND fk_codelst_studystat NOT IN ("+codeLst+") AND studystat_date = (SELECT MAX(studystat_date) FROM ER_STUDYSTAT WHERE  fk_codelst_studystat NOT IN ("+codeLst+")   AND fk_study = e.fk_study and fk_site=(select fk_siteid from er_user where pk_user="+userId+")))  "

	   	+ " ELSE"
	   	+ " (SELECT MAX(pk_studystat) FROM ER_STUDYSTAT WHERE fk_study = e.fk_study)"
	   	+ " END)";
	   	  
	   	//modified by sonia abrol, to filter studies with permanent cl. status (even if its not the latest status)
	   	 
	   	//Modified by Manimaran, to fix the Bug2551-Advance Search, option Exclude <Closed> Status should consider login user default Org.
	   	 if (excldprmtclsr.length()>0){
	   	studySql = studySql + " and not exists (select * from er_studystat where fk_study = pk_study  and fk_codelst_studystat = "+pmtclsId+ " and fk_site=(select fk_siteid from er_user where pk_user="+userId+")) ";
	   	}
	   	
	   	 if (excldinactive.length()>0){
		   	studySql = studySql + " and STUDY_ACTUALDT is not null ";
		   	} 
	   	
	   	if (stdphase.length()>0) { 
            studySql= studySql + " and  fk_codelst_phase = " + stdphase  ; 
             
       } //end stdphase
	   	
	   	if (stdstatus.length()>0) {
	   	  
	   		studySql = studySql + " and e.fk_codelst_studystat = " + stdstatus + "  and e.fk_site=(select fk_siteid from er_user where pk_user="+userId+")" ;
	   	       
	   	 } // end if for stdstatus
	   	
	   	if (currentStatus.length()>0) 
	   	{
	        	
	   		studySql = studySql + "   and  exists ( select * from er_studystat ss where " +
	   				" ss.fk_study = a.pk_study and ss.current_stat = 1  and fk_codelst_studystat = "+ currentStatus + " )" ;
	        	  
	     }
	     
	   	 
	   	 
       
	   		   	
	   	 
	   	//-- anu 27th may
	   	//Modified by Manimaran to fix the Bug2706 
	   	studySql = studySql +  ")d " +  StringUtil.htmlUnicodePoint(searchFilter) ; 
	   	     
	   	countSql= "select count(*) from ( " + studySql  + " )";
	   		   	
	     this.setMainSQL(studySql);
	     
	     //System.out.println("studySql" +studySql);
	     this.setCountSQL(countSql);
	   	 
	    }

	 public void getStudyCtrpDrafts(HttpServletRequest request)
	 {

	    String searchFilter="";
		     if (request==null)
		     {
		    	 this.setMainSQL("");
		    	 this.setCountSQL("");
		    	 
		     }
		    //display studies with 
		 	String ctrpDraftCheck = StringUtil.trueValue(request.getParameter("ctrpDraftSearch"));
		 	/*Added By Sudhir For CTRP-22473*/
		 	String crrntStudyStatus = StringUtil.trueValue(request.getParameter("protocolStatus"));
		 	
		 	//22472
		 	String nciTrialIden = StringUtil.trueValue(request.getParameter("nciTrialIden"));//er_study.NCI_TRIAL_IDENTIFIER
		 	String ctrpDraftStatuses = StringUtil.trueValue(request.getParameter("ctrpDraftStatuses"));
		 	String ctrpDraftAction = StringUtil.trueValue(request.getParameter("ctrpDraftAction"));
		 	
		 	String draftActFrmDt = StringUtil.trueValue(request.getParameter("draftActFrmDt"));
		 	String draftActToDt = StringUtil.trueValue(request.getParameter("draftActToDt"));
		 	String nciTrialStatus = StringUtil.trueValue(request.getParameter("nciTrialStatus"));//
		 	
		 	String searchCriteria = StringUtil.trueValue(request.getParameter("searchCriteria"));
		 	String userId=StringUtil.trueValue((String)request.getSession(false).getAttribute("userId"));
		 	String accountId=StringUtil.trueValue((String)request.getSession(false).getAttribute("accountId"));
			GroupJB groupJB = new GroupJB();
		 	String superuserRights = StringUtil.trueValue(groupJB.getDefaultStudySuperUserRights(userId));
		    
		    String studySql="";
		    String countSql;
		   
 		    CodeDao  cd = new CodeDao();
 		    String codeLst = cd.getValForSpecificSubType();
 		    String notInCodelSt=codeLst;
 		    if(crrntStudyStatus!=null && crrntStudyStatus!=""){
	 		 codeLst = crrntStudyStatus; 
 		    }
 		  //  System.out.println("value-"+StringUtil.stringToNum(ctrpDraftCheck));
		    ctrpDraftCheck =cd.getCodeSubtype(StringUtil.stringToNum(ctrpDraftCheck));
		    ctrpDraftCheck=ctrpDraftCheck==null?"":ctrpDraftCheck;
		    CodeDao cdDraftStatus = new CodeDao();
		    //String statusWIP = "";/*WIP and all custom statuses*/
		    //String statusNotWIP = "";
		    
	    	cdDraftStatus.getCodeValues("ctrpDraftStatus");
		    if (! StringUtil.isEmpty(searchCriteria)) //search in title,study number and keywords
		      {
		   	        	
   	        	if (searchFilter.length()>0 ) {
	   	            searchFilter= searchFilter + " and  " ; 
	   	            
	   	                 } 
	   	            else {
	   	            	searchFilter= searchFilter + " Where " ;
	   	            
	   	            } //end else 
   	        	

   	 		   // String newSearchCriteria = StringUtil.replace(searchCriteria,"'","''");
   	 		searchFilter= searchFilter+ " d.pk_study=" + searchCriteria+"" ;
   	 		    
   	         /*	searchFilter= searchFilter+ "  ( lower(d.study_keywrds) like lower( '%" + newSearchCriteria + "%')  " +
   	         			" or lower(d.study_number) like lower( '%" + newSearchCriteria + "%') or " +
   	         					" lower(d.study_title) like lower( '%" + newSearchCriteria + "%')   )" ;*/
		   	        	
		   	        	
		   	   }

		      
		   //Modified for Bug#7913:Akshi
		   studySql = "SELECT DISTINCT pk_ctrp_draft, pk_study AS rowcount, pk_study,study_number,study_title,site_name,status,studystat_note,studystat_date as studystat_date_datesort, "
			   +"  (NVL( (SELECT study_team_rights FROM er_studyteam WHERE fk_user = " + userId +"    AND "
			   +"  fk_study = pk_study AND study_team_usr_type = 'D' ), '"+superuserRights+"  ' ) ) study_team_rights , "
			   +"  RESEARCH_TYPE, CTRP_DRAFT_TYPE , current_stat_desc, FK_CODELST_STAT,CTRP_DRAFT_TYPE_STAT,FK_CODELST_STAT_SUBTYPE,"
			   +"NCI_TRIAL_IDEN,DRAFT_NUM,CTRP_NCI_PROC_STAT,DRAFT_LST_DW_DT,DRAFT_LST_SUB_DT,STUDY_PI_NAME,'' VW_DRFT_HSTRY,'' CREATE_DRFT"
			   +"  FROM (";
		   
		   if(ctrpDraftCheck.equals("CTRPDESIG")){
			   studySql = studySql+"(";
		   }
		   
		   if(ctrpDraftCheck.equals("SWD")){
			   studySql = studySql +"SELECT 0 as pk_ctrp_draft,";
		   }else{
			   studySql = studySql +"SELECT ct.pk_ctrp_draft as pk_ctrp_draft,";
		   }
		   
		   //Modified for Bug#7913:Akshi
		   studySql = studySql
			   +"  a.study_title AS study_title, a.pk_study, "
			   +"  (SELECT F_Get_Codelstdesc(xx.fk_codelst_studystat) FROM er_studystat xx WHERE xx.fk_study = a.pk_study AND xx.current_stat = 1"; 
			   
		   if(crrntStudyStatus!=null && crrntStudyStatus!=""){
				   studySql = studySql +" AND xx.FK_CODELST_STUDYSTAT= "+crrntStudyStatus+" "; 
			
		   }
			   studySql = studySql+" AND rownum < 2 ) current_stat_desc, "
			   +"  (NVL((SELECT site_name FROM er_site WHERE pk_site=(SELECT xx.fk_site FROM er_studystat xx WHERE xx.fk_study = a.pk_study AND xx.current_stat = 1 AND rownum < 2 ) ), '-')) AS site_name, "
			   +"  (NVL((SELECT xx.studystat_note FROM er_studystat xx WHERE xx.fk_study = a.pk_study AND xx.current_stat = 1 AND rownum < 2 ), '-' )) as studystat_note,"
			   +"  (NVL((SELECT F_Get_Codelstdesc(xx.fk_codelst_studystat) FROM er_studystat xx WHERE xx.fk_study = a.pk_study AND xx.current_stat = 1 "; 
			   
			   if(crrntStudyStatus!=null && crrntStudyStatus!=""){
				   studySql = studySql +" AND xx.FK_CODELST_STUDYSTAT= "+crrntStudyStatus+" "; 
			   }
			   
			   studySql = studySql+" and xx.fk_site= (SELECT fk_siteid FROM er_user WHERE pk_user=" + userId +"   ) AND rownum < 2),'...')) AS status, "
			   +"  (CASE WHEN (e.fk_site= (SELECT fk_siteid FROM er_user WHERE pk_user=" + userId +"   ) AND e.fk_codelst_studystat IN ("+codeLst+"  )) THEN NVL(F_Get_Codelstdesc(e.status_type),'-') ELSE '-' END) AS status_type, "
			   +" (NVL (to_char((SELECT xx.studystat_date FROM er_studystat xx WHERE xx.fk_study = a.pk_study AND xx.current_stat = 1 AND rownum < 2 ),PKG_DATEUTIL.F_GET_DATEFORMAT) , '-' )) as studystat_date,"
			   +"  a.study_number , "
			   +"  a.study_keywrds, "
			   +"  a.study_otherprinv, ";
			   
			   //22472
			   if (ctrpDraftCheck.equals("SWD")) { 
				   studySql = studySql+"  (select codelst_desc from er_codelst where pk_codelst = a.FK_CODELST_restype) as RESEARCH_TYPE, -1 as CTRP_DRAFT_TYPE,"
				   +" (select codelst_subtyp from er_codelst where pk_codelst = a.FK_CODELST_restype) CTRP_DRAFT_TYPE_STAT," 
				   +" null FK_CODELST_STAT, "
				   +" null FK_CODELST_STAT_SUBTYPE,"
				   +" a.NCI_TRIAL_IDENTIFIER as NCI_TRIAL_IDEN,null DRAFT_NUM,'' CTRP_NCI_PROC_STAT,null DRAFT_LST_DW_DT,null DRAFT_LST_SUB_DT"
				   +",(Select USR_FIRSTNAME || ' '||USR_LASTNAME from er_user u where u.PK_USER=a.STUDY_PRINV) STUDY_PI_NAME";
			   }else {
				   studySql = studySql +"  (select codelst_desc from er_codelst where pk_codelst = a.FK_CODELST_restype) as RESEARCH_TYPE, ct.CTRP_DRAFT_TYPE as CTRP_DRAFT_TYPE,"
				   +" (select codelst_subtyp from er_codelst where pk_codelst = a.FK_CODELST_restype) CTRP_DRAFT_TYPE_STAT,"
				 +"  (Select F_Get_Codelstdesc(eh.fk_codelst_stat) from er_status_history eH where " +
				 		" eH.status_modpk = ct.pk_ctrp_draft " +
				 		" AND NVL(ct.DELETED_FLAG,0)  <> 1"+
				 		" AND eH.STATUS_MODTABLE = 'er_ctrp_draft'" +
				 		" AND eH.status_iscurrent =1) FK_CODELST_STAT, "
				 +" (SELECT codelst_subtyp FROM er_codelst WHERE pk_codelst =(Select eh.fk_codelst_stat FROM er_status_history eH " +
				 		"WHERE eH.status_modpk = ct.pk_ctrp_draft " +
				 		" AND NVL(ct.DELETED_FLAG,0)  <> 1"+
				 		" AND eH.STATUS_MODTABLE = 'er_ctrp_draft'" +
				 		" AND eH.status_iscurrent =1 )) FK_CODELST_STAT_SUBTYPE"+
				 		",a.NCI_TRIAL_IDENTIFIER as NCI_TRIAL_IDEN,DRAFT_NUM," +
				 		"(Select MAX(eh.STATUS_DATE) from er_status_history eh where eH.status_modpk = ct.pk_ctrp_draft AND FK_CODELST_STAT=" +
				 		"(Select PK_CODELST from er_codelst where CODELST_TYPE='ctrpDraftStatus' AND CODELST_SUBTYP='DWLD' ) ) DRAFT_LST_DW_DT"+//yet to implement
				 		",(Select F_Get_Codelstdesc(eh.fk_codelst_stat) from er_status_history eH" +
				 		" WHERE eH.STATUS_MODTABLE = 'er_ctrp_nci_stat' AND eH.RECORD_TYPE<>'D' AND eh.status_modpk = ct.pk_ctrp_draft" +
				 		" AND eh.status_iscurrent=1) CTRP_NCI_PROC_STAT"+
				 		",(Select MAX(eh.STATUS_DATE) from er_status_history eh where eH.status_modpk = ct.pk_ctrp_draft AND FK_CODELST_STAT=" +
				 		"(Select PK_CODELST from er_codelst where CODELST_TYPE='ctrpDraftStatus' AND CODELST_SUBTYP='submitted' ) ) DRAFT_LST_SUB_DT"+
				 		",(Select USR_FIRSTNAME || ' '||USR_LASTNAME from er_user u where u.PK_USER=a.STUDY_PRINV) STUDY_PI_NAME";
			   }
			    
			   studySql = studySql +"  FROM er_study a , er_Codelst c, er_studystat e" ;
			   
			   if (!ctrpDraftCheck.equals("SWD")) { 
				   studySql = studySql +", er_ctrp_draft ct ";
			   } 
			   
			   studySql = studySql +"  WHERE a.pk_study = e.fk_study ";
			  
			   if (!ctrpDraftCheck.equals("SWD")) { 
				   studySql = studySql +" AND a.pk_study = ct.fk_study "
				   + " and nvl(ct.DELETED_FLAG,0) <> 1 ";
			   } 
			   
			   studySql = studySql + " AND a.fk_account = " + accountId + ""
			   +" AND STUDY_CTRP_REPORTABLE =1 ";
			   
			   if(crrntStudyStatus!=null && crrntStudyStatus!=""){
					studySql = studySql	+" AND e.FK_CODELST_STUDYSTAT in (SELECT (xx.fk_codelst_studystat) FROM er_studystat xx	WHERE xx.fk_study  = a.pk_study"
						+" AND xx.current_stat = 1 AND xx.FK_CODELST_STUDYSTAT= "+crrntStudyStatus+" AND xx.fk_site = (SELECT fk_siteid FROM er_user WHERE pk_user=" + userId +" )"
						+" AND rownum < 2)";
				}
			   
			   studySql = studySql	+" AND ( EXISTS (SELECT * FROM er_studyteam WHERE fk_study = pk_study AND fk_user = " + userId +"   AND NVL(study_team_usr_type,'D')='D' ) "
			   +"  OR pkg_superuser.F_Is_Superuser(" + userId +"  , pk_study) = 1 ) ";

			   if(crrntStudyStatus!=null && crrntStudyStatus!=""){
					  studySql = studySql +" AND e.FK_CODELST_STUDYSTAT= "+crrntStudyStatus+" and current_stat = 1 "; 
		    	}else{
			   studySql = studySql + " AND e.FK_CODELST_STUDYSTAT = c.pk_codelst ";
		    	}
			   
			   studySql = studySql +"  AND c.codelst_type = 'studystat' "
			   +"  AND e.pk_studystat = ( "
			   +"  CASE WHEN (SELECT COUNT(*) FROM ER_STUDYSTAT WHERE fk_study = e.fk_study AND fk_codelst_studystat IN ( "+codeLst+"  ) ";
			   if(crrntStudyStatus!=null && crrntStudyStatus!=""){
				   studySql = studySql + " and current_stat = 1 ";
			   }
			   studySql = studySql +" AND fk_site = (SELECT fk_siteid FROM er_user WHERE pk_user=" + userId +"   )) > 0 "
			   +"  THEN (SELECT MAX(pk_studystat) FROM ER_STUDYSTAT WHERE fk_study = e.fk_study AND fk_site = (SELECT fk_siteid FROM er_user WHERE pk_user=" + userId +"   ) AND fk_codelst_studystat IN ( "+codeLst+"  ) ";
			   if(crrntStudyStatus!=null && crrntStudyStatus!=""){
				   studySql = studySql + " and current_stat = 1 ";
			   }
			   studySql = studySql +" AND studystat_date = (SELECT MAX(studystat_date) FROM ER_STUDYSTAT WHERE fk_codelst_studystat IN ( "+codeLst+"  ) ";
			   if(crrntStudyStatus!=null && crrntStudyStatus!=""){
				   studySql = studySql + " and current_stat = 1 ";
			   }
			   studySql = studySql +" AND fk_study = e.fk_study AND fk_site = (SELECT fk_siteid FROM er_user WHERE pk_user=" + userId +"   ) ) ) "
			   +"  WHEN (SELECT COUNT(*) FROM ER_STUDYSTAT WHERE fk_study = e.fk_study AND fk_codelst_studystat NOT IN ("+notInCodelSt+"  ) AND fk_site = (SELECT fk_siteid FROM er_user WHERE pk_user=" + userId +"   )) > 0 THEN "
			   +"  (SELECT MAX(pk_studystat) FROM ER_STUDYSTAT WHERE fk_study = e.fk_study AND fk_site = (SELECT fk_siteid FROM er_user WHERE pk_user=" + userId +"   ) AND fk_codelst_studystat NOT IN ("+notInCodelSt+"  ) AND "
			   +"  studystat_date = (SELECT MAX(studystat_date) FROM ER_STUDYSTAT WHERE fk_codelst_studystat NOT IN ("+notInCodelSt+"  ) AND fk_study = e.fk_study AND fk_site = (SELECT fk_siteid FROM er_user WHERE pk_user=" + userId +"   ) ) ) "
			   +"  ELSE (SELECT MAX(pk_studystat) FROM ER_STUDYSTAT WHERE fk_study = e.fk_study AND fk_codelst_studystat IN ( "+codeLst+" ) AND current_stat = 1) END) ";
			   //FIX #8600
			   /*
			   +"  AND NOT EXISTS "
			   +"  (SELECT * FROM er_studystat WHERE fk_study = pk_study AND fk_codelst_studystat = "+ pmtclsId+ " AND fk_site = (SELECT fk_siteid FROM er_user WHERE pk_user=" + userId +"   ) ) ";
			   */
				
			   
			   
				if(!StringUtil.isEmpty(nciTrialIden)){
					String newNciTrialIden = StringUtil.replace(nciTrialIden,"'","''");
				   	 studySql= studySql + " AND lower(a.NCI_TRIAL_IDENTIFIER) LIKE lower('%"+newNciTrialIden.replace("%","/%").replace("_", "/_")+"%') escape '/' ";
				}
				   	
				if(!StringUtil.isEmpty(ctrpDraftStatuses)){
			   		
					studySql= studySql + " AND ((Select FK_CODELST_STAT from er_status_history eh where STATUS_MODPK=ct.PK_CTRP_DRAFT"+
					" AND eh.STATUS_MODTABLE = 'er_ctrp_draft'"+
					" AND eh.status_iscurrent=1)="+ctrpDraftStatuses+") ";
				}
				
				
			   	if(!StringUtil.isEmpty(ctrpDraftAction)){
				   	
			   		String codeSubType = cd.getCodeSubtype(Integer.parseInt(ctrpDraftAction));
			   					   		
			   		if(codeSubType.equals("DWNDT")){//Last Down-loaded
			   			if(!StringUtil.isEmpty(draftActFrmDt)){
				   			
				   			if(!StringUtil.isEmpty(draftActToDt)){
						   		studySql= studySql + " AND (" +
						   		"(Select MAX(STATUS_DATE) from er_status_history esh where esh.STATUS_MODTABLE='er_ctrp_draft'"+
						   		" AND esh.STATUS_MODPK=ct.PK_CTRP_DRAFT AND NVL(ct.DELETED_FLAG,0)<>1 AND esh.FK_CODELST_STAT= ( SELECT PK_CODELST  FROM er_codelst    " +
						   		" WHERE CODELST_TYPE='ctrpDraftStatus'   AND CODELST_SUBTYP='DWLD')  ) " +
						   		" BETWEEN to_date('"+draftActFrmDt+"',PKG_DATEUTIL.F_GET_DATEFORMAT) " +
						   		" AND to_date('"+draftActToDt+"',PKG_DATEUTIL.F_GET_DATEFORMAT))";
				   			}else{
				   				studySql= studySql + " AND (" +
				   				"(Select MAX(STATUS_DATE) from er_status_history esh where esh.STATUS_MODTABLE='er_ctrp_draft'" +
				   				" AND esh.STATUS_MODPK=ct.PK_CTRP_DRAFT AND NVL(ct.DELETED_FLAG,0)<>1 AND esh.FK_CODELST_STAT= (SELECT PK_CODELST FROM er_codelst   " +
				   				" WHERE CODELST_TYPE='ctrpDraftStatus' AND CODELST_SUBTYP='DWLD') )"+
				   				" BETWEEN to_date('"+draftActFrmDt+"',PKG_DATEUTIL.F_GET_DATEFORMAT) " +
				   				" AND to_date(PKG_DATEUTIL.F_GET_FUTURE_NULL_DATE_STR,PKG_DATEUTIL.F_GET_DATEFORMAT))";
				   			}
				   		}else if(!StringUtil.isEmpty(draftActToDt)){
				   			studySql= studySql + " AND (" +
			   				"(Select MAX(STATUS_DATE) from er_status_history esh where esh.STATUS_MODTABLE='er_ctrp_draft'" +
			   				" AND esh.STATUS_MODPK=ct.PK_CTRP_DRAFT AND NVL(ct.DELETED_FLAG,0)<>1 AND FK_CODELST_STAT=  ( SELECT PK_CODELST   FROM er_codelst  " +
			   				" WHERE CODELST_TYPE='ctrpDraftStatus'   AND CODELST_SUBTYP='DWLD')    )"+
			   				" BETWEEN to_date(PKG_DATEUTIL.F_GET_NULL_DATE_STR,PKG_DATEUTIL.F_GET_DATEFORMAT) " +
			   				" AND to_date('"+draftActToDt+"',PKG_DATEUTIL.F_GET_DATEFORMAT))";
				   		}
				   		
			   		}else if(codeSubType.equals("SUBDT")){//Last Submitted

				   		if(!StringUtil.isEmpty(draftActFrmDt)){
		   			
				   			if(!StringUtil.isEmpty(draftActToDt)){
						   		studySql= studySql + " AND (" +
						   		"(Select MAX(STATUS_DATE) from er_status_history esh where esh.STATUS_MODTABLE='er_ctrp_draft'"+
						   		" AND esh.STATUS_MODPK=ct.PK_CTRP_DRAFT AND NVL(ct.DELETED_FLAG,0)<>1 AND esh.FK_CODELST_STAT= (SELECT PK_CODELST FROM er_codelst  " +
						   		" WHERE CODELST_TYPE='ctrpDraftStatus'  AND CODELST_SUBTYP='submitted'  ) )"+
						   		" BETWEEN to_date('"+draftActFrmDt+"',PKG_DATEUTIL.F_GET_DATEFORMAT)"+
						   		" AND to_date('"+draftActToDt+"',PKG_DATEUTIL.F_GET_DATEFORMAT))";
				   			}else{
				   				studySql= studySql + " AND (" +
				   				"(Select MAX(STATUS_DATE) from er_status_history esh where esh.STATUS_MODTABLE='er_ctrp_draft'" +
				   				" AND esh.STATUS_MODPK=ct.PK_CTRP_DRAFT AND NVL(ct.DELETED_FLAG,0)<>1 AND esh.FK_CODELST_STAT= (SELECT PK_CODELST  FROM er_codelst   " +
				   				" WHERE CODELST_TYPE='ctrpDraftStatus'      AND CODELST_SUBTYP='submitted' )  )"+
				   				" BETWEEN to_date('"+draftActFrmDt+"',PKG_DATEUTIL.F_GET_DATEFORMAT)"+
				   				" AND to_date(PKG_DATEUTIL.F_GET_FUTURE_NULL_DATE_STR,PKG_DATEUTIL.F_GET_DATEFORMAT))";
				   			}
				   		}else if(!StringUtil.isEmpty(draftActToDt)){
				   			studySql= studySql + " AND (" +
			   				"(Select MAX(STATUS_DATE) from er_status_history esh where esh.STATUS_MODTABLE='er_ctrp_draft'" +
			   				" AND esh.STATUS_MODPK=ct.PK_CTRP_DRAFT AND NVL(ct.DELETED_FLAG,0)<>1 AND esh.FK_CODELST_STAT=   (SELECT PK_CODELST  FROM er_codelst   " +
			   				" WHERE CODELST_TYPE='ctrpDraftStatus'   AND CODELST_SUBTYP='submitted' ) )"+
			   				" BETWEEN to_date(PKG_DATEUTIL.F_GET_NULL_DATE_STR,PKG_DATEUTIL.F_GET_DATEFORMAT)"+
			   				" AND to_date('"+draftActToDt+"',PKG_DATEUTIL.F_GET_DATEFORMAT))";
				   		}
					}
			   		
			   		//studySql= studySql + ") ";
			   		
			   	}

			   	if(!StringUtil.isEmpty(nciTrialStatus)){
			   		
					studySql= studySql + " AND ((Select FK_CODELST_STAT from er_status_history esh where STATUS_MODPK=ct.PK_CTRP_DRAFT"+
					" AND esh.STATUS_MODTABLE = 'er_ctrp_nci_stat' AND esh.RECORD_TYPE<>'D' AND NVL(ct.DELETED_FLAG,0)<>1"+
					" AND eSh.status_iscurrent=1)="+nciTrialStatus+") ";
				}
			   	
			   	
			 /*   if(ctrpDraftCheck.equals("MRDRFT")){//Most recent Drafts
			    	
			    	studySql= studySql +" AND ((SELECT MAX(DRAFT_NUM) from er_ctrp_draft ecd where ct.PK_CTRP_DRAFT=ecd.PK_CTRP_DRAFT)=ct.DRAFT_NUM)"+
			    	 	" and EXISTS(Select * from er_ctrp_draft tt, er_status_history eH " +
					   " where tt.fk_study = a.pk_study AND eh.status_modpk = tt.pk_ctrp_draft" +
					   " and nvl(tt.DELETED_FLAG,0) <> 1 and nvl(eH.RECORD_TYPE,'N') <> 'D' " +
					   " and eH.STATUS_MODTABLE = 'er_ctrp_draft'" +
					   " and eh.status_iscurrent =1) "  ; 
		       }*/
			    // Need work around
			   /* if(ctrpDraftCheck.equals("CTRPDESIG")){//CTRP designation option
			    	studySql= studySql +" AND ((SELECT MAX(DRAFT_NUM) from er_ctrp_draft ecd where ct.PK_CTRP_DRAFT=ecd.PK_CTRP_DRAFT)=ct.DRAFT_NUM)"+
			    	" OR ( NOT EXISTS(Select * from er_ctrp_draft tt, er_status_history eH " +
	           		" where tt.fk_study = a.pk_study AND eh.status_modpk = tt.pk_ctrp_draft" +
	           		" and nvl(tt.DELETED_FLAG,0) <> 1 and nvl(eH.RECORD_TYPE,'N') <> 'D' " +
	           		" and eH.STATUS_MODTABLE = 'er_ctrp_draft'" +
	            	" and eh.status_iscurrent =1)) " ;
			 	}*/
			   
			   	if (ctrpDraftCheck.equals("SWD")) { //SWD- Studies without Draft
			   		studySql= studySql + " and NOT EXISTS(Select * from er_ctrp_draft tt, er_status_history eH " +
	           		" where tt.fk_study = a.pk_study AND eh.status_modpk = tt.pk_ctrp_draft" +
	           		" and nvl(tt.DELETED_FLAG,0) <> 1 and nvl(eH.RECORD_TYPE,'N') <> 'D' " +
	           		" and eH.STATUS_MODTABLE = 'er_ctrp_draft'" +
	            	" and eh.status_iscurrent =1)"  ;
			   }
			   	
			   if(ctrpDraftCheck.equals("MRDRFT") || ctrpDraftCheck.equals("CTRPDESIG")){
					   studySql= studySql +" AND (ct.DRAFT_NUM=(Select MAX(DRAFT_NUM) from er_ctrp_draft ecd1 where ecd1.FK_STUDY=a.PK_STUDY AND NVL(ecd1.DELETED_FLAG,0) <> 1))" +
					   " AND EXISTS(Select * from er_ctrp_draft tt, er_status_history eH " +
					   " where tt.fk_study = a.pk_study AND eh.status_modpk = tt.pk_ctrp_draft" +
					   " and nvl(tt.DELETED_FLAG,0) <> 1 and nvl(eH.RECORD_TYPE,'N') <> 'D' " +
					   " and eH.STATUS_MODTABLE = 'er_ctrp_draft'" +
					   " and eh.status_iscurrent =1) "  ; 
			   }
			   	
			   if(ctrpDraftCheck.equals("SD")){
				   studySql= studySql +" and EXISTS(Select * from er_ctrp_draft tt, er_status_history eH " +
				   " where tt.fk_study = a.pk_study AND eh.status_modpk = tt.pk_ctrp_draft" +
				   " and nvl(tt.DELETED_FLAG,0) <> 1 and nvl(eH.RECORD_TYPE,'N') <> 'D' " +
				   " and eH.STATUS_MODTABLE = 'er_ctrp_draft'" +
				   " and eh.status_iscurrent =1) "  ; 
			   }
			   String codeSubType=null;
			   if(!StringUtil.isEmpty(ctrpDraftAction)){
				   codeSubType = cd.getCodeSubtype(Integer.parseInt(ctrpDraftAction));
			   }
			   if(StringUtil.isEmpty(codeSubType))codeSubType="";
			   if(! codeSubType.equals("DWNDT") && ! codeSubType.equals("SUBDT") ){
			   if(ctrpDraftCheck.equals("CTRPDESIG")){
				   studySql= studySql +" UNION (SELECT 0 AS pk_ctrp_draft, a.study_title AS study_title, a.pk_study" +
				   ", (SELECT F_Get_Codelstdesc(xx.fk_codelst_studystat) FROM er_studystat xx WHERE xx.fk_study = a.pk_study";
				
				   if(crrntStudyStatus!=null && crrntStudyStatus!=""){
					   studySql = studySql +" AND xx.FK_CODELST_STUDYSTAT= "+crrntStudyStatus+" "; 
				
				   }
				   studySql= studySql + " AND xx.current_stat = 1 AND rownum < 2) current_stat_desc"+
				   ", (NVL((SELECT site_name FROM er_site WHERE pk_site=(SELECT xx.fk_site FROM er_studystat xx WHERE xx.fk_study = a.pk_study" +
				   " AND xx.current_stat = 1 AND rownum < 2)), '-')) AS site_name"+
				   ", (NVL((SELECT xx.studystat_note FROM er_studystat xx WHERE xx.fk_study = a.pk_study AND xx.current_stat = 1" +
				   " AND rownum < 2), '-' )) AS studystat_note" +
				   ", (NVL((SELECT F_Get_Codelstdesc(xx.fk_codelst_studystat) FROM er_studystat xx WHERE xx.fk_study = a.pk_study" +
				   " AND xx.current_stat = 1";
				   
				   if(crrntStudyStatus!=null && crrntStudyStatus!=""){
					   studySql = studySql +" AND xx.FK_CODELST_STUDYSTAT= "+crrntStudyStatus+" "; 
				   }

				   studySql= studySql + " AND xx.fk_site =(SELECT fk_siteid FROM er_user WHERE pk_user="+userId+
				   ") AND rownum < 2),'...')) AS status,(CASE WHEN (e.fk_site=(SELECT fk_siteid FROM er_user WHERE pk_user="+userId+
				   ") AND e.fk_codelst_studystat IN ("+codeLst+")) THEN NVL(F_Get_Codelstdesc(e.status_type),'-') ELSE '-'"+
				   " END) AS status_type,(NVL (TO_CHAR((SELECT xx.studystat_date FROM er_studystat xx WHERE xx.fk_study = a.pk_study" +
				   " AND xx.current_stat = 1 AND rownum < 2),PKG_DATEUTIL.F_GET_DATEFORMAT) , '-' )) AS studystat_date" +
				   ", a.study_number, a.study_keywrds, a.study_otherprinv,(SELECT codelst_desc FROM er_codelst WHERE pk_codelst" +
				   "= a.FK_CODELST_restype)AS RESEARCH_TYPE,-1 AS CTRP_DRAFT_TYPE,(select codelst_subtyp from er_codelst where pk_codelst"+
				   "=a.FK_CODELST_restype) CTRP_DRAFT_TYPE_STAT,null AS FK_CODELST_STAT,null AS FK_CODELST_STAT_SUBTYPE";
				   
				   studySql= studySql+", NCI_TRIAL_IDENTIFIER AS NCI_TRIAL_IDEN,null AS DRAFT_NUM,null AS DRAFT_LST_DW_DT,null CTRP_NCI_PROC_STAT,null DRAFT_LST_SUB_DT"+
				   ", (SELECT USR_FIRSTNAME || ' ' ||USR_LASTNAME FROM er_user u WHERE u.PK_USER=a.STUDY_PRINV) STUDY_PI_NAME" +
				   " FROM er_study a,er_Codelst c,er_studystat e WHERE A.PK_STUDY=E.FK_STUDY AND a.fk_account="+accountId+
				   " AND STUDY_CTRP_REPORTABLE=1";
				   
					if(crrntStudyStatus!=null && crrntStudyStatus!=""){
						studySql = studySql	+" AND e.FK_CODELST_STUDYSTAT in (SELECT (xx.fk_codelst_studystat) FROM er_studystat xx	WHERE xx.fk_study  = a.pk_study"
							+" AND xx.current_stat = 1 AND xx.FK_CODELST_STUDYSTAT= "+crrntStudyStatus+" AND xx.fk_site = (SELECT fk_siteid FROM er_user WHERE pk_user=" + userId +" )"
							+" AND rownum < 2)";
					}
					
				   if(!StringUtil.isEmpty(nciTrialIden)){
						String newNciTrialIden = StringUtil.replace(nciTrialIden,"'","''");
					   	 studySql= studySql + " AND lower(a.NCI_TRIAL_IDENTIFIER) LIKE lower('%"+newNciTrialIden.replace("%", "/%").replace("_", "/_")+"%') escape '/' ";
					}
				   
				   studySql= studySql + " AND (EXISTS(SELECT * FROM er_studyteam WHERE fk_study = pk_study AND fk_user="+userId+
				   " AND NVL(study_team_usr_type,'D')='D') OR pkg_superuser.F_Is_Superuser("+userId+",pk_study) = 1)";
				   
				   
				   if(crrntStudyStatus!=null && crrntStudyStatus!=""){
						studySql = studySql +" AND e.FK_CODELST_STUDYSTAT= "+crrntStudyStatus+" and current_stat = 1 "; 
			    	}else{
			    		studySql = studySql + " AND e.FK_CODELST_STUDYSTAT = c.pk_codelst ";
			    	}
				   
				   studySql= studySql+ " AND c.codelst_type='studystat' AND e.pk_studystat = ("+
				   " CASE WHEN (SELECT COUNT(*) FROM ER_STUDYSTAT WHERE fk_study=e.fk_study AND fk_codelst_studystat IN ("+codeLst+")";
				   
				   if(crrntStudyStatus!=null && crrntStudyStatus!=""){
					   studySql = studySql + " and current_stat = 1 ";
				   }		   
				   
				   studySql = studySql + " AND fk_site =(SELECT fk_siteid FROM er_user WHERE pk_user="+userId+")) > 0"+
				   " THEN(SELECT MAX(pk_studystat) FROM ER_STUDYSTAT WHERE fk_study = e.fk_study AND fk_site=(SELECT fk_siteid"+
				   " FROM er_user WHERE pk_user="+userId+") AND fk_codelst_studystat IN ("+codeLst+")";
				   		
				   if(crrntStudyStatus!=null && crrntStudyStatus!=""){
					   studySql = studySql + " and current_stat = 1 ";
				   }
				   
				   
				   studySql = studySql + " AND studystat_date="+
				   "(SELECT MAX(studystat_date) FROM ER_STUDYSTAT WHERE fk_codelst_studystat IN ("+codeLst+") AND fk_study=e.fk_study";
				   if(crrntStudyStatus!=null && crrntStudyStatus!=""){
					   studySql = studySql + " and current_stat = 1";
				   }
				   studySql=studySql+" AND fk_site=(SELECT fk_siteid FROM er_user WHERE pk_user="+userId+"))) WHEN (SELECT COUNT(*) FROM ER_STUDYSTAT"+
				   " WHERE fk_study =e.fk_study AND fk_codelst_studystat NOT IN ("+codeLst+") AND fk_site=(SELECT fk_siteid FROM"+
				   " er_user WHERE pk_user="+userId+")) > 0 THEN (SELECT MAX(pk_studystat) FROM ER_STUDYSTAT WHERE fk_study=e.fk_study"+
				   " AND fk_site=(SELECT fk_siteid FROM er_user WHERE pk_user="+userId+") AND fk_codelst_studystat NOT IN ("+codeLst+
				   ") AND studystat_date=(SELECT MAX(studystat_date) FROM ER_STUDYSTAT WHERE fk_codelst_studystat NOT IN ("+codeLst+
				   ") AND fk_study=e.fk_study AND fk_site=(SELECT fk_siteid FROM er_user WHERE pk_user="+userId+")))"+
				   " ELSE (SELECT MAX(pk_studystat) FROM ER_STUDYSTAT WHERE fk_study=e.fk_study AND fk_codelst_studystat IN ("+codeLst+
				   ") AND current_stat=1) END) AND NOT EXISTS (SELECT * FROM er_ctrp_draft tt,er_status_history eH WHERE tt.fk_study="+
				   "a.pk_study AND eh.status_modpk=tt.pk_ctrp_draft AND NVL(tt.DELETED_FLAG,0)<>1 AND NVL(eH.RECORD_TYPE,'N')<>'D'"+
				   " AND eH.STATUS_MODTABLE='er_ctrp_draft' AND eh.status_iscurrent=1))";
				   
			   }
	 		   }	
			   if(ctrpDraftCheck.equals("CTRPDESIG")){
				   studySql = studySql +  ")d )" +  StringUtil.htmlUnicodePoint(searchFilter) ;
			   }else{
				   studySql = studySql +  ")d " +  StringUtil.htmlUnicodePoint(searchFilter) ;
			   }
			   
			   
		   	     
		   	countSql= "select count(*) from ( " + studySql  + " )";
		   		   	
		     this.setMainSQL(studySql);
		     
		     //System.out.println("studySql" +studySql);
		     this.setCountSQL(countSql);
		   	 
		    
	 }

	public void getCtrpSubjectAccrual(HttpServletRequest request){
		
		String studySql = "";
		String countSql;

		if(request == null){
			this.setMainSQL("");
			this.setCountSQL("");
		}
		
		String searchCriteria = StringUtil.trueValue(request.getParameter("searchCriteria"));
		String crrntStudyStatus = StringUtil.trueValue(request.getParameter("protocolStatus"));
		String nciTrialIden = StringUtil.trueValue(request.getParameter("nciTrialIden"));// er_study.NCI_TRIAL_IDENTIFIER
		
	 	String userId=StringUtil.trueValue((String)request.getSession(false).getAttribute("userId"));
	 	String accountId=StringUtil.trueValue((String)request.getSession(false).getAttribute("accountId"));
		GroupJB groupJB = new GroupJB();
	 	String superuserRights = StringUtil.trueValue(groupJB.getDefaultStudySuperUserRights(userId));
		
		studySql = "SELECT DISTINCT pk_study AS rowcount,pk_study,study_number,study_title"
				+ ",studystat_date,(NVL((SELECT study_team_rights FROM er_studyteam WHERE fk_user = " + userId
				+ " AND fk_study = pk_study AND study_team_usr_type = 'D'),'" + superuserRights + "')) study_team_rights"
				+ ",RESEARCH_TYPE,NCI_TRIAL_IDEN,STATUS,STUDYSTAT_NOTE,SITE_NAME,STUDYSTAT_DATE_DATESORT,LAST_GEN_DATE" 
				+ ",CTRP_CUTOFF_DATE,(NVL((SELECT USR_FIRSTNAME ||' ' ||USR_LASTNAME FROM ER_user WHERE pk_user=CTRP_LAST_DOWNLOAD_BY ),'')) CTRP_LAST_DOWNLOAD_BY,RESEARCH_SUB_TYPE"
				+ ", '' AS VALDT  FROM (";
		
		studySql = studySql	+ "SELECT a.pk_study,a.study_number,a.study_title";

		studySql = studySql + ", (NVL (to_char((SELECT xx.studystat_date FROM er_studystat xx WHERE xx.fk_study = a.pk_study AND xx.current_stat = 1 AND rownum < 2 ),PKG_DATEUTIL.F_GET_DATEFORMAT) , '-' )) as studystat_date,";

		studySql = studySql
				+ "  (select codelst_desc from er_codelst where pk_codelst = a.FK_CODELST_restype) as RESEARCH_TYPE"
				+ ",a.NCI_TRIAL_IDENTIFIER NCI_TRIAL_IDEN";
		studySql = studySql +
		",NVL((SELECT F_Get_Codelstdesc(xx.fk_codelst_studystat) FROM er_studystat xx WHERE xx.fk_study=a.pk_study AND xx.current_stat=1"
		+" AND xx.fk_site= (SELECT fk_siteid FROM er_user WHERE pk_user=" + userId +"   ) AND rownum < 2),'...') STATUS"; 
		
		studySql = studySql + ",(NVL((SELECT xx.studystat_note FROM er_studystat xx WHERE xx.fk_study = a.pk_study AND xx.current_stat = 1"
		+ " AND rownum < 2), '-' )) AS STUDYSTAT_NOTE,(NVL((SELECT site_name FROM er_site WHERE pk_site=" +
		"(SELECT xx.fk_site FROM er_studystat xx WHERE xx.fk_study = a.pk_study AND xx.current_stat = 1 AND rownum < 2)), '-')) AS site_name"
		+",(NVL (to_char((SELECT xx.studystat_date FROM er_studystat xx WHERE xx.fk_study = a.pk_study AND xx.current_stat = 1 AND rownum < 2 )"
		+",PKG_DATEUTIL.F_GET_DATEFORMAT) , '-' )) as STUDYSTAT_DATE_DATESORT " ;
		
		studySql = studySql + ", NVL( to_char ( a.CTRP_ACCRUAL_GEN_DATE , PKG_DATEUTIL.F_GET_DATEFORMAT) , '-') as LAST_GEN_DATE"
		+",CTRP_CUTOFF_DATE,CTRP_LAST_DOWNLOAD_BY,(select codelst_subtyp from er_codelst where pk_codelst = a.FK_CODELST_RESTYPE) RESEARCH_SUB_TYPE";
		
		studySql = studySql	+ " FROM er_study a , er_Codelst c, er_studystat e WHERE a.pk_study = e.fk_study AND a.fk_account=" + accountId 
		+ " AND a.NCI_TRIAL_IDENTIFIER IS NOT NULL AND a.STUDY_CTRP_REPORTABLE=1";
		
		if(crrntStudyStatus!=null && crrntStudyStatus!=""){
			studySql = studySql	+" AND e.FK_CODELST_STUDYSTAT in (SELECT (xx.fk_codelst_studystat) FROM er_studystat xx	WHERE xx.fk_study  = a.pk_study"
				+" AND xx.current_stat = 1 AND xx.FK_CODELST_STUDYSTAT= "+crrntStudyStatus+" AND xx.fk_site = (SELECT fk_siteid FROM er_user WHERE pk_user=" + userId +" )"
				+" AND rownum < 2)";
		}
		
		studySql = studySql	+ " AND ((select CODELST_CUSTOM_COL from er_codelst where pk_codelst = a.FK_CODELST_restype) IN ('nonIndustrial','industrial'))";
		
		studySql = studySql + " and e.FK_CODELST_STUDYSTAT = c.pk_codelst and c.codelst_type = 'studystat'"; 
		
		if(crrntStudyStatus!=null && crrntStudyStatus!=""){
			studySql = studySql +" AND e.FK_CODELST_STUDYSTAT= "+crrntStudyStatus+" and e.current_stat = 1 "; 
    	}		
		
		if (!StringUtil.isEmpty(nciTrialIden)){
			String newNciTrialIden = StringUtil.replace(nciTrialIden, "'", "''");
			studySql = studySql	+ " AND lower(a.NCI_TRIAL_IDENTIFIER) LIKE lower('%" + newNciTrialIden.replace("%", "/%").replace("_", "/_") + "%') escape '/' ";
		}
	
		String searchFilter = "";
		if (!StringUtil.isEmpty(searchCriteria)){ // search in title,study number and keywords
			String newSearchCriteria = StringUtil.replace(searchCriteria,"'","''");
			searchFilter= " where (lower(d.study_number) like lower( '%" + newSearchCriteria.replace("%", "/%").replace("_", "/_") + "%') escape '/' )";
		}

		studySql = studySql	+ " AND ( EXISTS (SELECT * FROM er_studyteam WHERE fk_study = pk_study AND fk_user = "
				+ userId + "   AND NVL(study_team_usr_type,'D')='D' ) "	+ "  OR pkg_superuser.F_Is_Superuser(" + userId
				+ "  , pk_study) = 1 ) ";

		studySql = studySql + ")d " + StringUtil.htmlUnicodePoint(searchFilter);
		countSql = "select count(*) from ( " + studySql + " )";

		this.setMainSQL(studySql);
		this.setCountSQL(countSql);
	}
}
