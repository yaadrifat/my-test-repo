package com.velos.browser;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.velos.eres.business.common.CtrlDao;
import com.velos.eres.business.common.PatientDao;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.group.GroupJB;

public class Patient {

 private String mainSQL;
 private String countSQL;
 private HashMap paramMap;
 private String startDate;
 private String endDate;






public Patient()
 {
   mainSQL="";
   countSQL="";
   paramMap=new HashMap();
 }

/**
 * @return the mainSQL
 */
public String getMainSQL() {
	return mainSQL;
}

/**
 * @param mainSQL the mainSQL to set
 */
public void setMainSQL(String mainSQL) {
	this.mainSQL = StringUtil.htmlUnicodePoint(mainSQL);
}

/**
 * @return the countSQL
 */
public String getCountSQL() {
	return countSQL;
}

/**
 * @param countSQL the countSQL to set
 */
public void setCountSQL(String countSQL) {
	this.countSQL = countSQL;
}

/**
 * @return the paramMap
 */
public HashMap getParamMap() {
	return paramMap;
}

/**
 * @param paramMap the paramMap to set
 */
public void setParamMap(HashMap paramMap) {
	this.paramMap = paramMap;
}

public String getStartDate() {
	return startDate;
}

public void setStartDate(String startDate) {
	this.startDate = startDate;
}

public String getEndDate() {
	return endDate;
}

public void setEndDate(String endDate) {
	this.endDate = endDate;
}
  public void getAllPatientsSQL()
	    {

	     String accountId="",regBy="",patName="",pstat="",siteId="",speciality="",patCode="",gender="";
	     String userId="",age="",idStudy="",maxDate="",minDate="",grpId = "";
	     String ageFilter="";
	    Date dt1 = new java.util.Date();
	 	Date dt2= new java.util.Date();

	 	Calendar cal = new GregorianCalendar();

	 	PatientDao pdao = new PatientDao();

	 	Format formatter = new SimpleDateFormat("yyyy/MM/dd");

	 	int minPHIRight = 0;

	     accountId=(String)paramMap.get("accountId");
	     regBy=(String)paramMap.get("regBy");
	     patName=(String)paramMap.get("patName");
	     pstat=(String)paramMap.get("pstat");
	     siteId=(String)paramMap.get("siteId");
	     speciality=(String)paramMap.get("speciality");
	     patCode=(String)paramMap.get("patCode");
	     gender=(String)paramMap.get("gender");
	     userId=(String)paramMap.get("userId");
	     age=(String)paramMap.get("age");
	     idStudy=(String)paramMap.get("studyId");
	     grpId=(String)paramMap.get("grpId");



	     minPHIRight =  pdao.getViewPHIRight(EJBUtil.stringToNum(userId),EJBUtil.stringToNum(grpId));

	     String patientSql="";
	     String countSql;

	   	StringBuffer sbSelect = new StringBuffer();
	   	StringBuffer sbFrom = new StringBuffer();
	   	StringBuffer sbWhere = new StringBuffer();


	   	//Create Age String

	   	if (!EJBUtil.isEmpty(age))
		 {

			 if (age.equals("0,15")) {
			 dt1.setYear(dt1.getYear() - 0);
			 dt2.setYear(dt2.getYear() -16);
			 maxDate= formatter.format(dt1);
	    	 minDate=formatter.format(dt2);

			 ageFilter=" and (((to_char(a.person_dob,'YYYY/MM/DD')) > '"+ minDate + "'  and (to_char(a.person_dob,'YYYY/MM/DD')) <='" + maxDate+ "'  AND a.person_deathdt IS NULL) or  trunc(months_between(a.person_deathdt,a.person_dob))/12 between 0 and 15.99)" ;

			 }//end for ae.equals("1,15")



			  if (age.equals("16,30")) {
			 dt1.setYear(dt1.getYear() - 16);
			 dt2.setYear(dt2.getYear() -31);
			 maxDate = formatter.format(dt1);
	    		 minDate=formatter.format(dt2);

			 ageFilter=" and (((to_char(a.person_dob,'YYYY/MM/DD')) > '"+ minDate + "'  and (to_char(a.person_dob,'YYYY/MM/DD')) <='" + maxDate+ "'  AND a.person_deathdt IS NULL)  or trunc(months_between(a.person_deathdt,a.person_dob))/12 between 16 and 30.99)" ;
			 }//end for age.equals("16,30")



			if (age.equals("31,45")) {

			 dt1.setYear(dt1.getYear() -31);
			 dt2.setYear(dt2.getYear() -46);
			 maxDate = formatter.format(dt1);
	    		 minDate=formatter.format(dt2);

			 ageFilter=" and (((to_char(a.person_dob,'YYYY/MM/DD')) > '"+ minDate + "'  and (to_char(a.person_dob,'YYYY/MM/DD')) <='" + maxDate+ "'  AND a.person_deathdt IS NULL) or   trunc(months_between(a.person_deathdt,a.person_dob))/12 between 31 and 45.99)" ;
			 }//end for age.equals("31,46")


			 if (age.equals("46,60")) {

			 dt1.setYear(dt1.getYear() - 46);
			 dt2.setYear(dt2.getYear() -61);
			 maxDate = formatter.format(dt1);
	    		 minDate=formatter.format(dt2);

			 ageFilter=" and (((to_char(a.person_dob,'YYYY/MM/DD')) > '"+ minDate + "'  and (to_char(a.person_dob,'YYYY/MM/DD')) <='" + maxDate+ "'  AND a.person_deathdt IS NULL) or  trunc(months_between(a.person_deathdt,a.person_dob))/12 between 46 and 60.99)" ;

			     }//end for age.equals("46,60")

			 /*
			 if (age.equals("46,60")) {
			      lowLimit= cal.get(Calendar.YEAR) - EJBUtil.stringToNum("46");
			      highLimit= cal.get(Calendar.YEAR) - EJBUtil.stringToNum("60");
			      ageFilter=" and ((SUBSTR(TO_CHAR(a.person_dob,'YYYYMMDD'),0,4) >= '"+ highLimit + "' ) and (SUBSTR(TO_CHAR(a.person_dob,'YYYYMMDD'),0,4) <='" + lowLimit+ "'))  AND a.person_deathdt IS NULL" ;
			 }//end for age.equals("46,60")

			 */


			 if (age.equals("61,75")) {
			 dt1.setYear(dt1.getYear() - 61);
			 dt2.setYear(dt2.getYear() -76);
			 maxDate = formatter.format(dt1);
	    		 minDate=formatter.format(dt2);

			 ageFilter=" and (((to_char(a.person_dob,'YYYY/MM/DD')) > '"+ minDate + "'  and (to_char(a.person_dob,'YYYY/MM/DD')) <='" + maxDate+ "'  AND a.person_deathdt IS NULL) or trunc(months_between(a.person_deathdt,a.person_dob))/12 between 61 and 75.99)" ;
			 }//end for age.equals("61,75")

			   /*
			   if (age.equals("61,75")) {
				  lowLimit= cal.get(Calendar.YEAR) - EJBUtil.stringToNum("61");
			      highLimit= cal.get(Calendar.YEAR) - EJBUtil.stringToNum("75");
			      ageFilter=" and ((SUBSTR(TO_CHAR(a.person_dob,'YYYYMMDD'),0,4) >= '"+ highLimit + "' ) and (SUBSTR(TO_CHAR(a.person_dob,'YYYYMMDD'),0,4) <='" + lowLimit+ "'))" ;
			 }//end for age.equals("61,75")

			 */

			  if (age.equals("76,Over")) {

			 dt1.setYear(dt1.getYear() - 76);
			 maxDate = formatter.format(dt1);

			 ageFilter=" and (((to_char(a.person_dob,'YYYY/MM/DD')) <='" + maxDate+ "'  AND a.person_deathdt IS NULL) or   trunc(months_between(a.person_deathdt,a.person_dob))/12 >= 76)" ;

			 }//end for age.equals("76,Over")

			 /*
			 if (age.equals("76,Over")) {
				  lowLimit= cal.get(Calendar.YEAR) - EJBUtil.stringToNum("76");
			      ageFilter=" and (SUBSTR(TO_CHAR(a.person_dob,'YYYYMMDD'),0,4) <= '"+ lowLimit + "' )" ;
			 }//end for age.equals("1,15")
			 */



		 } //end for age!=null
		 else
		 {
		    ageFilter = "";
		 }

	   	//End Age String


	   	//JM: 04October2006: Modified
	   	//sbSelect.append("Select distinct lower(PERSON_CODE) person_code_lower, a.person_code ,(SELECT  count(fk_study) from er_patprot e") ;
	   	sbSelect.append("Select distinct a.rowid as rowcount, lower(PERSON_CODE) person_code_lower, a.person_code ,(SELECT  count(fk_study) from er_patprot e") ;
	   	sbSelect.append(" 	where e.fk_per = a.PK_PERSON and  e.PATPROT_ENROLDT is not null and ");
	   	sbSelect.append(" PATPROT_STAT = 1)  Count,");
	   	// JM: 12Mayo5 -- query modified for displaying the organization name for each patient
	   	sbSelect.append("a.pk_person,floor(NVL(a.person_deathdt,sysdate) - a.person_dob) no_of_days_diff,");
	   	sbSelect.append("decode(a.person_deathdt,null,pkg_util.f_datediff(a.person_dob),pkg_util.f_datediff(a.person_dob,a.person_deathdt)) mask_person_dob,a.person_dob mask_person_dob_datesort, a.person_fname mask_pfname,a.person_lname mask_plname,");
	   	//km
	   	sbSelect.append("a.person_address1 mask_padd1,a.person_address2 mask_padd2,a.person_city mask_pcity,");
	   	sbSelect.append("a.person_state mask_pstate,a.person_zip mask_pzip,a.person_country mask_pcountry,");
	   	//JM: 04October2006: Modified person_notes to person_notes_clob
	   	//Query modified by Manimaran to fix the Bug2739.
	   	sbSelect.append("a.person_hphone mask_phphone,a.person_bphone mask_pbphone,replace(replace(dbms_lob.substr(a.PERSON_NOTES_CLOB,2000,1),CHR(13),''),CHR(10),'') mask_pnotes,replace(replace(dbms_lob.substr(a.PERSON_NOTES_CLOB,2000,2001),CHR(13),''),CHR(10),'') mask_pnotes1,");
	   	//km

	   	// query changed by Jnanamay Majumdar  usr_firstname ||' '|| usr_lastname to usr_lastname || ', ' || usr_firstname

	   	//SV, 8/4/04, fix for bug #1535: show names as <first name> <last name>
	   	// changed the following from usr_lastname || ', ' || usr_firstname to usr_firstname ||' '|| usr_lastname
	   	//reg_by:

	   	sbSelect.append(" (select ER_CODELST.CODELST_DESC from ER_CODELST where ER_CODELST.PK_CODELST=a.FK_CODELST_GENDER) Person_gender ,");
	   	sbSelect.append(" (select ER_CODELST.CODELST_DESC from ER_CODELST where ER_CODELST.PK_CODELST=a.FK_CODELST_PSTAT) person_status, ");
	   	sbSelect.append("f_getspl_access(a.person_splaccess) speciality, ");
	   	sbSelect.append(" lower(a.person_lname || ' ' || a.person_fname ) mask_pname_sort, ");
	   	// query changed by Jnanamay Majumdar  usr_firstname ||' '|| usr_lastname to usr_lastname || ', ' || usr_firstname
	   	//JM: 12May05
	   	//changed by Sonia Abrol, to get the site and pat facility for each site
	   	//changed by Sonia Abrol, to get the date of death

	   	//Modified by gopu
	   	//sbSelect.append(" pkg_patient.f_get_patsites(pk_person,"+siteId+") sitename ,to_char( person_deathdt ,'mm/dd/yyyy') person_deathdt");
	   	sbSelect.append("to_char( person_deathdt ,PKG_DATEUTIL.F_GET_DATEFORMAT) person_deathdt");

	   	if (minPHIRight >= 4 )
	   	{
	   		sbSelect.append(", 4 as right_mask");
	   	}
	   	else
	   	{
	   		sbSelect.append(", pkg_studystat.f_get_patientright("+userId+", "+grpId+", pk_person ) right_mask");
	   	}

	   	sbFrom.append(" from epat.person a , ER_PATFACILITY fac, er_usersite usr ");


	   	//modified by Sonia Abrol, 09/30/05, to get patients according to patient facility registration
	   	sbWhere.append(" WHERE a.fk_account = " + accountId  +" and fk_user ="+userId +" AND usersite_right>=4 AND usr.fk_site = fac.fk_site ");

	   	if (!EJBUtil.isEmpty(siteId)){
	   	sbWhere.append("  AND fac.fk_site= "+ siteId);
	   	}

	   	if (!EJBUtil.isEmpty(regBy))
	   	{
//	   	sbWhere.append("  AND (select lower(usr_lastname||' '||usr_firstname) from er_user where pk_user=fac.patfacility_provider) like  lower('%"+ regBy+"%')");

	   	sbWhere.append(" and exists (select * from er_user where pk_user = patfacility_provider and ((lower(usr_lastname||' '||usr_firstname) like  lower('%"+ regBy+"%')) or (lower(usr_firstname||' '||usr_lastname) like  lower('%"+ regBy+"%'))) and fac.patfacility_provider is not null)" );
	   	}

	   	//Sonia Abrol, 09/30/05, changed specialty search, now it searches in patient facility
	   	if (!EJBUtil.isEmpty(speciality)){

	   	sbWhere.append(" and ',' || fac.patfacility_splaccess || ',' like ('%,"+speciality+",%')");

	   	}

	     	sbWhere.append(" AND fac.patfacility_accessright > 0 AND fk_per = pk_person " ) ;


	   	//check for patCode filter
	   	if (! EJBUtil.isEmpty(patCode))
	   	{
	   		sbWhere.append(" and ( lower(person_code) like lower('%"+patCode.replaceAll("%","/%").replaceAll("_","/_")+"%') escape '/' or ");
	   		//modified by Sonia Abrol, 09/30/05, to get patients according to patient facility registreation
	   	sbWhere.append("  lower(fac.pat_facilityid) like lower('%"+patCode.replaceAll("%","/%").replaceAll("_","/_")+"%') escape '/' " ) ;
	   	sbWhere.append(" )");
	   	}

	   	//check for gender filter
	   	if (! EJBUtil.isEmpty(gender))
	   	{
	   		sbWhere.append(" and a.fk_codelst_gender  = " + gender );
	   	}

	   	//check for status filter
	   	if (! EJBUtil.isEmpty(pstat))
	   	{
	   		sbWhere.append(" and  a.fk_codelst_pstat = "+ pstat   );
	   	}
	   	//age filter

	   	sbWhere.append(ageFilter);

	   	//regBy filter


	   	//patName filter
	   			if  (! EJBUtil.isEmpty(patName) )
	   	{
	   	   String[] names1 = patName.trim().split(" ");
	   	   sbWhere.append(" and  (  lower( a.person_fname) like lower('%"+patName.trim()+"%') or ");
	   	   if (names1 != null && names1.length > 0) {
	   	       sbWhere.append(" lower(a.person_fname) like lower('%"+names1[0]+"%')  or ");
	   	   }
           if (names1 != null && names1.length > 0) {
               sbWhere.append(" lower(a.person_lname) like lower('%"+names1[names1.length-1]+"%')  or ");
           }
           if (names1 != null && names1.length > 1) {
               sbWhere.append(" lower(a.person_mname) like lower('%"+names1[1]+"%')  or ");
           }
           sbWhere.append(" lower(a.person_lname) like lower('%"+patName.trim()+"%')  or ");
	   	   sbWhere.append(" lower(a.person_mname) like lower('%"+patName.trim()+"%')     ") ;
	   	   sbWhere.append(" ) ");
	   	}

	   	// enrolled in study filter
	   	if  (!EJBUtil.isEmpty(idStudy))
	   	{
	   	   sbWhere.append(" and exists (Select * from er_patprot b Where a.pk_person = b.fk_per and b.fk_study = " + idStudy + ")") ;
	   	}



	   	//Link the ROWNUM and higher limit here.
	   	 patientSql = sbSelect.toString() + sbFrom.toString() + sbWhere.toString();
	   	countSql = " Select count(*) from ( " + patientSql + " )";
	     this.setMainSQL(patientSql);
	     this.setCountSQL(countSql);
	   	 //System.out.println("patientsqlpage" +patientSql );
	   	 //return patientSql;
	    }



  public void getStudyPatientsSQL(HttpServletRequest request){


		StringBuffer sbSQL = new StringBuffer();
		String finalSQL = "";

		String grpId = StringUtil.trueValue((String)request.getSession(false).getAttribute("defUserGroup"));

		String pstudy = StringUtil.trueValue(request.getParameter("dStudy"));
		int pStudyIntValue = EJBUtil.stringToNum(pstudy);

		String userId = StringUtil.trueValue((String)request.getSession(false).getAttribute("userId"));

		//String superUserRights = StringUtil.trueValue(request.getParameter("superUserRights"));

		GroupJB groupJB = new GroupJB();
		String superUserRights = groupJB.getDefaultStudySuperUserRights(userId);


		String orgStrr = StringUtil.trueValue(request.getParameter("dPatSite"));
		if(orgStrr==null || orgStrr.equals("")){
			orgStrr="0";
        	}
		//String siteForPatFacility = StringUtil.trueValue(request.getParameter("dPatSite"));

		String filEnrDt = StringUtil.trueValue(request.getParameter("filterEnrDate")) ;
		String filVisit = StringUtil.trueValue(request.getParameter("filterLastVisit")) ;
		String filNext = StringUtil.trueValue(request.getParameter("filterNextVisit"));

		Calendar calEnrolDt = Calendar.getInstance();
		Calendar calNextVisitDate = Calendar.getInstance();

		String excludeNotEnrolled = StringUtil.trueValue(request.getParameter("cbxExcludeNotEnrolled"));

		String pstat = request.getParameter("patientstatus") ;
		if(pstat == null)	   pstat = "";
		int iPstat = EJBUtil.stringToNum(pstat);

		String patStudyId = StringUtil.trueValue(request.getParameter("pstudyid"));
		String pid = StringUtil.trueValue(request.getParameter("patientid"));



	   sbSQL.append(" Select distinct p.rowid as rowcount,erv.fk_per, p.person_lname as mask_person_lname, p.person_fname as mask_person_fname, per_site siteId,");
 	   sbSQL.append(" (select site_name from ER_SITE where pk_site = per_site) site_name,");
 	  sbSQL.append(" pkg_studystat.f_get_patientright("+userId+", "+grpId+", erv.fk_per ) right_mask, ");
 	   sbSQL.append("	erv.study_number, erv.pk_patprot, erv.fk_study,lower(per_code) lowerpercode, erv.per_code,");
	   sbSQL.append(" decode(status_browser_flag,1,patstudystat_desc,'-')  patstudystat_desc,");
	   sbSQL.append(" erv.PATSTUDYSTAT_SUBTYPE, erv.PK_PATSTUDYSTAT,");
	   //Ashu Added for 'Requiremnt_Spec_COMP_REQ_1' requirement 2Feb11.
	   
	   sbSQL.append("nvl((select codelst_desc from er_codelst where pk_codelst=(select fk_codelst_stat from er_patstudystat where fk_per= erv.fk_per and fk_study=erv.fk_study and current_stat=1)");
	   sbSQL.append(" and codelst_type='patStatus'and codelst_custom_col='browser'), '-') patstudycur_stat,");	

	   sbSQL.append(" decode(status_browser_flag,1,patstudystat_date,null)  patstudystat_date_datesort,");

	   sbSQL.append(" decode(status_browser_flag,1,patstudystat_note,'-')  patstudystat_note,");

	   sbSQL.append(" decode(status_browser_flag,1,patstudystat_reason_desc,'-')  patstudystat_reason,");

	   sbSQL.append(" erv.patprot_enroldt as patprot_enroldt_datesort,");
	   sbSQL.append(" lower(erv.last_visit_name) lowerlastvisit,erv.last_visit_name,erv.cur_visit, erv.cur_visit_date, erv.next_visit as next_visit_datesort, ");
	   sbSQL.append(" lower(p.person_lname || p.person_fname) mask_patnamelower,lower(erv.PATPROT_PATSTDID) lowerpatstdid,erv.PATPROT_PATSTDID,");
	   sbSQL.append(" ('[VELSEP]'||p.PERSON_HPHONE||'[VELSEP]'||p.PERSON_BPHONE||'[VELSEP]') mask_PERSON_PHONE ,('[VELSEP]'||p.PERSON_ADDRESS1||'[VELSEP]'|| ");
	   sbSQL.append(" p.PERSON_ADDRESS2||'[VELSEP]'||p.PERSON_CITY||'[VELSEP]'|| p.PERSON_STATE||'[VELSEP]'||p.PERSON_ZIP||'[VELSEP]'||p.PERSON_COUNTRY||'[VELSEP]') ");
	   sbSQL.append(" as mask_pataddress  , (select codelst_desc from ER_CODELST where pk_codelst=p.FK_CODELST_RACE) patrace  ,");
	   sbSQL.append(" (select codelst_desc from ER_CODELST where pk_codelst=p.FK_CODELST_ETHNICITY) patethnicity ,");
	   sbSQL.append(" dbms_lob.substr(p.PERSON_NOTES_CLOB,4000,1) personnotes, ");
	   sbSQL.append(" lower(p.person_lname) mask_lowerplname,lower(p.person_fname) mask_lowerpfname,");
	   sbSQL.append(" assignedto_name,physician_name,enrolledby_name,treatingorg_name,");
	   sbSQL.append(" (select USR_FIRSTNAME||' '|| USR_MIDNAME ||' '|| USR_LASTNAME||' '|| ad.ADD_EMAIL||' '||USR_LOGNAME from er_user,er_patprot pt,er_add ad where pk_user=pt.FK_USER and fk_peradd=pk_add and pt.pk_patprot=erv.pk_patprot) ENROL_BY, ");
	   sbSQL.append(" (select USR_FIRSTNAME||' '|| USR_MIDNAME ||' '|| USR_LASTNAME||' '|| ad.ADD_EMAIL||' '||USR_LOGNAME from er_user,er_patprot pt,er_add ad where pk_user=pt.FK_USERASSTO and fk_peradd=pk_add and pt.pk_patprot=erv.pk_patprot) ASGN_TO, ");
	   sbSQL.append(" (select USR_FIRSTNAME||' '|| USR_MIDNAME ||' '|| USR_LASTNAME||' '|| ad.ADD_EMAIL||' '||USR_LOGNAME from er_user,er_patprot pt,er_add ad where pk_user=pt.PATPROT_PHYSICIAN and fk_peradd=pk_add and pt.pk_patprot=erv.pk_patprot) PHYSICIAN, ");
	   
	   sbSQL.append(" (select tx_name from er_studytxarm where pk_studytxarm=(select Z.fk_studytxarm from er_pattxarm Z where Z.pk_pattxarm = (  ") ;
	   sbSQL.append(" select max(X.pk_pattxarm) from er_pattxarm X where X.FK_PATPROT=erv.PK_PATPROT and X.tx_start_date=(select max(Y.tx_start_date) from er_pattxarm Y where Y.FK_PATPROT=erv.PK_PATPROT )) )  )  current_tx_arm,") ;

	   sbSQL.append(" (select count(*) from esch.sch_adverseve eve where eve.fk_study=erv.fk_study and eve.fk_per=erv.fk_per and eve.fk_codlst_aetype in  ") ;
	   sbSQL.append("  (select pk_codelst from sch_codelst cl where cl.codelst_type='adve_type')) AE_COUNT_NUMSORT, ");
	   
	   sbSQL.append("  (select count(*) from esch.sch_adverseve eve where eve.fk_study=erv.fk_study and eve.fk_per=erv.fk_per and eve.fk_codlst_aetype=  ");
	   sbSQL.append("  (select pk_codelst from sch_codelst cl where cl.codelst_type='adve_type' and cl.codelst_subtyp='al_sadve')) SAE_COUNT_NUMSORT, ");

	   sbSQL.append( " (select count(*) from er_patforms pf where pf.fk_per=erv.fk_per");
	   sbSQL.append( " AND pf.fk_patprot in (select pk_patprot from er_patprot pt where PT.FK_STUDY=erv.fk_study AND pt.fk_per=erv.fk_per) and  record_type<>'D') form_count_numsort ");

	   //sbSQL.append("  pkg_patient.f_get_patsites(erv.fk_per,"+siteForPatFacility+") pat_facilities ");

	   sbSQL.append("  from erv_studypat_by_visit erv ,  erv_person p where P.pk_person = erv.fk_per ");

	   if (pStudyIntValue > 0)
	   {
	   		sbSQL.append(" and erv.fk_study = " + pStudyIntValue);
	   } //removed else as All option is removed from study dd.
	   
        //append other FILTERS

	    if (!filEnrDt.equals("ALL") && (filEnrDt.length()>0)) {
            sbSQL.append(" and PATPROT_ENROLDT " + EJBUtil.getRelativeTime(calEnrolDt, filEnrDt) + " ");
        }
		else if (excludeNotEnrolled.equals("1"))
		 {
		   		sbSQL.append("  and PATPROT_ENROLDT IS NOT NULL  ");
		 }


		if (!filNext.equals("ALL") && (filNext.length()>0)) {
              // sbSQL.append(" and NEXT_VISIT " + EJBUtil.getRelativeTime(calNextVisitDate, filNext) + " ");
              sbSQL.append(" and ( " + EJBUtil.getRelativeTimeLong("NEXT_VISIT", calNextVisitDate, filNext) + " ) ");
        }
         if (filVisit.length() > 0) {

               sbSQL.append(" and lower(LAST_VISIT_NAME) like '%" + filVisit.toLowerCase() + "%'");
        }

        if (iPstat > 0)
        {
        	sbSQL.append(" and PATSTUDYSTAT_ID = " + iPstat );
        }

         if (! StringUtil.isEmpty(patStudyId))
	        {
	           sbSQL.append(" and UPPER(PATPROT_PATSTDID) LIKE UPPER('%" + patStudyId.replace("%", "/%").replace("_", "/_")+ "%') escape '/' ");
	        }


        if (! StringUtil.isEmpty(pid))
        {
        	sbSQL.append("  and (UPPER(PER_CODE) LIKE UPPER('%" + pid.replace("%", "/%").replace("_", "/_")
                 + "%') escape '/'  OR  exists (select * from ER_PATFACILITY fac where fac.fk_per = erv.fk_per  ");

        	sbSQL.append(" and fac.fk_site in( " + orgStrr + ") and fac.patfacility_accessright > 0 and lower(pat_facilityid) like lower('%"
        	+ pid.replace("%", "/%").replace("_", "/_")+"%') escape '/' )  ) ");
        }
		// end of additional filters


    	sbSQL.append("  and exists (select * from ER_PATFACILITY fac where fac.fk_per = erv.fk_per  ");
        sbSQL.append(" and fac.fk_site in( " + orgStrr + ") and fac.patfacility_accessright > 0 ) ");




 	   //if study is not selected, append the check for manage patients study right
 	   if (pStudyIntValue == 0)
 	   {
 	   		CtrlDao ctrlRight = new CtrlDao();
 	   		int studyRightForPat = 0;
 	   		ArrayList arSeq = new ArrayList();

 	   		ctrlRight.getControlValues("study_rights", "STUDYMPAT");
 	   		arSeq = ctrlRight.getCSeq();

 	   		if (arSeq.size() > 0)
 	   		{
 	   			studyRightForPat =  ((Integer) arSeq.get(0)).intValue();
 	   		}



 	   		finalSQL = "SELECT * FROM ( " + sbSQL.toString() + "  ) M WHERE pkg_util.f_getStudyRight(m.study_team_rights , " + studyRightForPat +") > 0 ";
 	   }
 	   else
 	   {
 	   		finalSQL =  sbSQL.toString();
 	   }

 	  String countSql = " Select count(*) from ( " + finalSQL + " )";
 	 //finalSQL=finalSQL+ " and ROWNUM<{LASTREC}";
 	 //System.out.println("finalSQL" + finalSQL);
	       this.setMainSQL(finalSQL);
	       this.setCountSQL(countSql);






  }

  public void getAllSchedulesSQL(HttpServletRequest request){


		StringBuffer sbSQL = new StringBuffer();
		String finalSQL = "";

		String grpId = StringUtil.trueValue(request.getParameter("grpId"));
		if (StringUtil.isEmpty(grpId)){
			grpId = StringUtil.trueValue((String)request.getAttribute("gdtGroupId"));
		} else {
			grpId = StringUtil.trueValue((String)request.getSession(false).getAttribute("defUserGroup"));
		}
		request.removeAttribute("gdtGroupId");
		
		String userId = StringUtil.trueValue(request.getParameter("userId"));
		if (StringUtil.isEmpty(userId)){
			userId = StringUtil.trueValue((String)request.getAttribute("gdtUserId"));
		} else {
			userId = StringUtil.trueValue((String)request.getSession(false).getAttribute("userId"));
		}
		request.removeAttribute("gdtUserId");
			
		String filNext = StringUtil.trueValue(request.getParameter("filterNextVisit"));
		if (StringUtil.isEmpty(filNext)){
			filNext = StringUtil.trueValue((String)request.getAttribute("gdtVisitFilter"));
		}
		request.removeAttribute("gdtVisitFilter");

		//String superUserRights = StringUtil.trueValue(request.getParameter("superUserRights"));

		GroupJB groupJB = new GroupJB();
		String superUserRights = groupJB.getDefaultStudySuperUserRights(userId);


		//String siteForPatFacility = StringUtil.trueValue(request.getParameter("dPatSite"));

		Calendar calEnrolDt = Calendar.getInstance();
		Calendar calNextVisitDate = Calendar.getInstance();


	   sbSQL.append(" Select distinct p.rowid as rowcount,erv.fk_per, p.person_lname as mask_person_lname, p.person_fname as mask_person_fname, per_site siteId,");
	   sbSQL.append(" (select site_name from ER_SITE where pk_site = per_site) site_name,");
	  sbSQL.append(" pkg_studystat.f_get_patientright("+userId+", "+grpId+", erv.fk_per ) right_mask, ");
	   sbSQL.append("	erv.study_number, erv.pk_patprot, erv.fk_study,lower(per_code) lowerpercode, erv.per_code,");
	   sbSQL.append(" decode(status_browser_flag,1,patstudystat_desc,'-')  patstudystat_desc,"); 
	   
	   sbSQL.append(" erv.PATSTUDYSTAT_SUBTYPE, erv.PK_PATSTUDYSTAT,");


	   sbSQL.append(" decode(status_browser_flag,1,trim(patstudystat_date),null)  patstudystat_date_datesort,");

	   sbSQL.append(" decode(status_browser_flag,1,patstudystat_note,'-')  patstudystat_note,");

	   sbSQL.append(" decode(status_browser_flag,1,patstudystat_reason,'-')  patstudystat_reason,");

	   sbSQL.append(" erv.patprot_enroldt as patprot_enroldt_datesort,");
	   sbSQL.append(" to_date(to_char(to_date(trim(erv.next_visit),'dd-Mon-yy')," 
		   + "PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) as next_visit_datesort, ");
	   
	   sbSQL.append(" erv.next_visit_name, ");
	                         
	   sbSQL.append(" lower(p.person_lname || p.person_fname) mask_patnamelower,lower(erv.PATPROT_PATSTDID) lowerpatstdid,erv.PATPROT_PATSTDID,");
	   sbSQL.append(" ('[VELSEP]'||p.PERSON_HPHONE||'[VELSEP]'||p.PERSON_BPHONE||'[VELSEP]') mask_PERSON_PHONE ,('[VELSEP]'||p.PERSON_ADDRESS1||'[VELSEP]'|| ");
	   sbSQL.append(" p.PERSON_ADDRESS2||'[VELSEP]'||p.PERSON_CITY||'[VELSEP]'|| p.PERSON_STATE||'[VELSEP]'||p.PERSON_ZIP||'[VELSEP]'||p.PERSON_COUNTRY||'[VELSEP]') ");
	   sbSQL.append(" as mask_pataddress  , (select codelst_desc from ER_CODELST where pk_codelst=p.FK_CODELST_RACE) patrace  ,");
	   sbSQL.append(" (select codelst_desc from ER_CODELST where pk_codelst=p.FK_CODELST_ETHNICITY) patethnicity ,");
	   sbSQL.append(" dbms_lob.substr(p.PERSON_NOTES_CLOB,1000,1) personnotes, ");
	   sbSQL.append(" lower(p.person_lname) mask_lowerplname,lower(p.person_fname) mask_lowerpfname,");
	   
	   sbSQL.append(" PI,assignedto_name,");
	   sbSQL.append(" (nvl((select study_team_rights from ER_STUDYTEAM T where T.fk_user = "+ userId+" and ");
	   sbSQL.append(" T.fk_study = erv.fk_study  and study_team_usr_type = 'D'), '"+superUserRights+"' ) ) STUDY_TEAM_RIGHTS ");

	   sbSQL.append("  from eres.ERV_STUDYPAT_UPCOMING_VISIT erv ,  erv_person p where P.pk_person = erv.fk_per ");

   		/*sbSQL.append(" and 	(exists  (select * from ER_STUDY_SITE_RIGHTS r, ER_STUDYTEAM T ");
   		sbSQL.append("	where  T.fk_user = " + userId+ " and ");
   		sbSQL.append("	T.fk_study = r.fk_study and T.fk_user = r.fk_user   and r.fk_study = erv.fk_study  and ");
   		sbSQL.append("	r.fk_user = "+ userId+" and USER_STUDY_SITE_RIGHTS = 1  and r.fk_site in (");
   		
   		sbSQL.append(" SELECT DISTINCT ss.fk_site ");
   		sbSQL.append(" FROM er_studysites ss, er_site ");
   		sbSQL.append(" WHERE ss.fk_study = erv.fk_study AND pk_site = ss.fk_site");
   		sbSQL.append(" and ( pkg_user.f_chk_right_for_studysite(ss.fk_study,"+ userId+" ,ss.fk_site) > 0  )");
   		
   		sbSQL.append(" ) and  nvl(T.study_team_usr_type,'D')='D'");
   	    sbSQL.append(" ) or pkg_superuser.F_Is_Superuser("+userId+", erv.fk_study) = 1 )" );*/
	   
	   sbSQL.append(" and p.fk_account = (select u.fk_account from er_user u where pk_user = "+ userId +") ");
	   sbSQL.append(" and ( exists ( select * from er_studyteam ss where ss.fk_study = erv.fk_study and " );
	   sbSQL.append(" ss.fk_user = "+ userId +" and  nvl(ss.study_team_usr_type,'D')='D') or pkg_superuser.F_Is_Superuser("+ userId +", erv.fk_study) = 1 )");  

		if (!filNext.equals("ALL") && (filNext.length()>0)) {
            // sbSQL.append(" and NEXT_VISIT " + EJBUtil.getRelativeTime(calNextVisitDate, filNext) + " ");
            sbSQL.append(" and ( " + EJBUtil.getRelativeTimeLong("NEXT_VISIT", calNextVisitDate, filNext) + " ) ");
		}

		sbSQL.append("  and exists (select * from ER_PATFACILITY fac where fac.fk_per = erv.fk_per  ");
		sbSQL.append(" and fac.patfacility_accessright > 0 ");
		sbSQL.append(" and ( pkg_user.f_chk_right_for_studysite(erv.fk_study,"+ userId+" ,fac.fk_site) > 0  ))");


	   //if study is not selected, append the check for manage patients study right
   		CtrlDao ctrlRight = new CtrlDao();
   		int studyRightForPat = 0;
   		ArrayList arSeq = new ArrayList();

   		ctrlRight.getControlValues("study_rights", "STUDYMPAT");
   		arSeq = ctrlRight.getCSeq();

   		if (arSeq.size() > 0)
   		{
   			studyRightForPat =  ((Integer) arSeq.get(0)).intValue();
   		}



   		finalSQL = "SELECT * FROM ( " + sbSQL.toString() + "  ) M WHERE pkg_util.f_getStudyRight(m.study_team_rights , " + studyRightForPat +") > 0 ";

   		String countSql = " Select count(*) from ( " + finalSQL + " )";
		 //finalSQL=finalSQL+ " and ROWNUM<{LASTREC}";
		 //System.out.println("finalSQL" + finalSQL);
   		this.setMainSQL(finalSQL);
   		this.setCountSQL(countSql);

  	}
  //Method Added by Bikash for INVP2.8
  /**
   * Forms the query for retrieval of all scheduled events with storage kit.
   * 
   * @param pRequest request from servlet which includes all parameters.
   */
  public void getAllSchedulesWithKitSql(HttpServletRequest pRequest){


		StringBuffer sbSQL = new StringBuffer();
		StringBuffer sbSQL1 = new StringBuffer();
		String finalSQL = "";
		 String accountId=(String)paramMap.get("accountId");   
	     String siteId=(String)paramMap.get("siteId");
	     String userId=(String)paramMap.get("userId");
	     String grpId=(String)paramMap.get("grpId");
	     String patCode=(String)paramMap.get("patCode");
	     String patStdCode=(String)paramMap.get("patStdCode");    
	     String studyId=(String)paramMap.get("studyId");
	     String year=(String)paramMap.get("year");
	     String month=(String)paramMap.get("month");
	     String year1=(String)paramMap.get("year1");	     	     
	     String fromDate=(String)paramMap.get("fromDate");
	     String toDate=(String)paramMap.get("toDate");
	     String dateChoice=(String)paramMap.get("dateChoice");
	     String eventstatus=(String)paramMap.get("eventstatus");
	     String chkPrevPrepRec=(String)paramMap.get("chkPrevPrepRec");
	     if(dateChoice!=""){
	     this.decideDateRange(dateChoice, year, year1, month, fromDate, toDate);
	     }
		//String superUserRights = StringUtil.trueValue(request.getParameter("superUserRights"));

		GroupJB groupJB = new GroupJB();
		String superUserRights = groupJB.getDefaultStudySuperUserRights(userId);
		
	   sbSQL.append("select distinct erv.fk_per || erv.EVENT_ID as rowcount ,erv.person_code,erv.study_number," );
	   sbSQL.append("erv.protocol_name,erv.visit_name,erv.event_schdate as event_schdate_datesort ,erv.event_name,"); //AK:Fixed BUG#5956 (30thMar2011)
	   sbSQL.append("(select storage_name from er_storage where pk_storage = kit.fk_storage and fk_account ="+accountId+") STORAGE_KIT,kit.fk_storage PK_STORAGE,");
	   sbSQL.append("erv.fk_study FK_STUDY,erv.fk_per FK_PER,erv.fk_site FK_SITE, erv.pat_studyid ,erv.event_status_desc ,erv.site_name,erv.start_date_time,erv.event_status ,");
	   //sbSQL.append("nvl((select Distinct(CASE PREPFLAG WHEN 'P' THEN 'Y' END) FROM er_specimen s  WHERE s.fk_visit = FK_VISIT AND s.fk_study =erv.fk_study AND s.fk_sch_events1= erv.EVENT_ID AND s.fk_per= erv.fk_per AND s.PREPFLAG='P'  and s.PREPAREKITID= kit.fk_storage),'N') AS Prepared_Sample, ");
	   sbSQL.append(" nvl((SELECT DISTINCT(CASE WHEN PREPFLAG in('P','Y') THEN 'Y' end)  FROM er_specimen s  WHERE s.fk_visit = FK_VISIT  AND s.fk_study  =erv.fk_study AND s.fk_sch_events1= erv.EVENT_ID  AND s.fk_per  = erv.fk_per  and prepflag in('P','Y')  AND s.PREPAREKITID  = kit.fk_storage OR ");
	   sbSQL.append(" s.PREPAREKITID  in  ( select PREPAREKITID from er_specimen ss where ((select count(pk_Storage) from ER_STORAGE o where fk_storage = kit.fk_storage) = (select count(PREPAREKITID) from er_specimen where  PREPAREKITID in (select pk_storage from er_storage where fk_storage=kit.fk_storage) "); 
	   sbSQL.append(" and fk_visit = FK_VISIT  AND fk_study  =erv.fk_study AND fk_sch_events1= erv.EVENT_ID  AND fk_per  = erv.fk_per and prepflag='Y'))  and fk_visit = FK_VISIT  AND fk_study  =erv.fk_study AND fk_sch_events1= erv.EVENT_ID  AND fk_per  = erv.fk_per and prepflag='Y')),'N') as Prepared_Sample, ");
			  
	   sbSQL.append("(select fk_visit from erv_scheve ers where ers.EVENT_ID = erv.EVENT_ID) FK_VISIT,erv.EVENT_ID FK_SCH_EVENTS1,");
	   sbSQL.append("(nvl((select study_team_rights from ER_STUDYTEAM T where T.fk_user = "+ userId+" and ");
	   sbSQL.append("T.fk_study = erv.fk_study  and study_team_usr_type = 'D'), '"+superUserRights+"' ) ) STUDY_TEAM_RIGHTS ");
	   sbSQL.append("from erv_patsch erv,er_patfacility fac,sch_event_kit kit ");
	   sbSQL.append("where erv.fk_per = fac.fk_per and kit.fk_event = erv.fk_assoc ");
	   sbSQL.append("and erv.fk_account="+accountId+" ");
	   if(!EJBUtil.isEmpty(dateChoice)){
	   sbSQL.append("and (event_schdate >= to_date('"+this.getStartDate()+"','mm/dd/yyyy') ");
	   sbSQL.append("and event_schdate <= to_date('"+this.getEndDate()+"','mm/dd/yyyy'))");	 
	   }
	 //check for patCode filter
	   	if (! EJBUtil.isEmpty(patCode))
	   	{
	   		sbSQL.append(" and ( lower(erv.person_code) like lower('%"+patCode.replace("%", "/%").replace("_", "/_")+"%') escape '/' or ");	   		
	   		sbSQL.append(" lower(fac.pat_facilityid) like lower('%"+patCode.replace("%", "/%").replace("_", "/_")+"%') escape '/' " ) ;
	   		sbSQL.append(" )");
	   	}
	   	if (! EJBUtil.isEmpty(patStdCode))
	   	{
	   		sbSQL.append(" and ( lower(erv.pat_studyid) like lower('%"+patStdCode.replace("%", "/%").replace("_", "/_")+"%') escape '/' ) ");	
	   		//fix 5831 Feb-10-2011
	   	}
	   	if (!EJBUtil.isEmpty(siteId)){
		   	sbSQL.append("  AND erv.fk_site in ("+ siteId+")"); //AK:Fixed BUG#5985(1stApril11)
		   	}
	   	if (!EJBUtil.isEmpty(studyId)){
		   	sbSQL.append("  AND erv.fk_study in ("+ studyId+")"); //Ashu:Modified for BUG#5794
		   	}

	   	if (! EJBUtil.isEmpty(eventstatus))
	   	{
	   		sbSQL.append(" and erv.event_status="+eventstatus+"");	
	   	}
	   	
		
	   	
	   //if study is not selected, append the check for manage patients study right
 		CtrlDao ctrlRight = new CtrlDao();
 		int studyRightForPat = 0;
 		ArrayList arSeq = new ArrayList();

 		ctrlRight.getControlValues("study_rights", "STUDYMPAT");
 		arSeq = ctrlRight.getCSeq();

 		if (arSeq.size() > 0)
 		{
 			studyRightForPat =  ((Integer) arSeq.get(0)).intValue();
 		}

		if (EJBUtil.isEmpty(chkPrevPrepRec))
	   	{
	   		sbSQL1.append(" and Prepared_Sample='N' ");	
	   	}
		

 		finalSQL = "SELECT * FROM ( " + sbSQL.toString() + "  ) M WHERE pkg_util.f_getStudyRight(m.study_team_rights , " + studyRightForPat +") > 0 "+sbSQL1+" ";
 		
 		String countSql = " Select count(*) from ( " + finalSQL + " )";		
 		this.setMainSQL(finalSQL);
 		this.setCountSQL(countSql); 		

	}
  //Method Added by Bikash for INVP2.8
  /**
   * This Method decides the start date and end date for forming the query for retrieval.
   * 
   * @param pDateChoice
   * @param pYear
   * @param pYear1
   * @param pMonth
   */
  private void decideDateRange(String pDateChoice,String pYear,String pYear1,String pMonth,String pStartDate, String pEndDate){
	  Calendar calendar = Calendar.getInstance();
	  SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy"); 	    
	  int year,month,maxDay;
	  Date tempDate = new Date();	  
	  switch(Integer.parseInt(pDateChoice)){  
	  
  case 1:	 this.setStartDate(sdf.format(new Date()));
		     this.setEndDate(sdf.format(new Date()));
		     break;
	  
	case 2:
		  	year = Integer.parseInt(pYear);		     
		  	calendar.set(year,0,1);
		  	tempDate = calendar.getTime();
		  	this.setStartDate(sdf.format(tempDate));
		  	calendar.set(year,11,31);
		  	tempDate = calendar.getTime();		      
		  	this.setEndDate(sdf.format(tempDate));
		  	break;

	  
	case 3:
		   month = Integer.parseInt(pMonth)- 1;
		   year = Integer.parseInt(pYear1);
		   calendar.set(year,month,1);
		   tempDate = calendar.getTime();
		   this.setStartDate(sdf.format(tempDate));		   
		   maxDay = calendar.getActualMaximum(calendar.DAY_OF_MONTH);		   
		   calendar.set(year,month,maxDay);
		   tempDate = calendar.getTime();
		   this.setEndDate(sdf.format(tempDate));
		   break;
	  
	case 4:
		   Date tempStartDate = DateUtil.stringToDate(pStartDate);
		   Date tempEndDate = DateUtil.stringToDate(pEndDate);		   
		   this.setStartDate(sdf.format(tempStartDate));
		   this.setEndDate(sdf.format(tempEndDate));
		  
	  	}
	  }
  }
  
