package com.velos.controller;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.velos.eres.service.util.Bundles;
import com.velos.eres.service.util.NumberUtil;

/**
 * ConfigLoader class can be used to perform actions on context init.
 * @author AGodara
 * 
 */
public class ConfigLoader implements ServletContextListener {

	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
	}

	public void contextInitialized(ServletContextEvent servletContextEvent) {
		// Load the Number Format settings on context init
		NumberUtil.setCodeLstNumFormat();
		
		String contextPath = servletContextEvent.getServletContext().getRealPath("/");
		Bundles.generateLableBundleJS(contextPath);
		System.setProperty("javax.xml.transform.TransformerFactory","com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl"); 

		System.setProperty("javax.xml.parsers.DocumentBuilderFactory","org.apache.xerces.jaxp.DocumentBuilderFactoryImpl"); 

		System.setProperty("javax.xml.parsers.SAXParserFactory","org.apache.xerces.jaxp.SAXParserFactoryImpl"); 

	}
}
