//sajal dt 03/29/2001

package com.velos.epat.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.persistence.NamedQuery;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.service.util.Rlog;

public class AuditRowEpatDao extends CommonDAO implements java.io.Serializable {

	private static final long serialVersionUID = 7158101413194792668L;
	
	public AuditRowEpatDao() {    	
    
    }
    public int getRID(int pkey, String tableName, String pkColumnName) {
    	 PreparedStatement pstmt = null;
         Connection conn = null;
         int retInt=0;
         String sql="";
         try{
        	 if (StringUtil.isEmpty(tableName) ||
        		StringUtil.isEmpty(pkColumnName) || pkey < 0){
        		 retInt = -1;
        	 }else{
	        	 conn = getPatConnection();
	        	 sql="SELECT RID FROM epat."+ tableName +" WHERE "+pkColumnName+" = " + pkey +"";
	        	 pstmt = conn.prepareStatement(sql);
	             ResultSet rs = pstmt.executeQuery();

	             while (rs.next()) {
	            	 retInt = (new Integer(rs.getInt("RID")));
	            	 break;
	             }
        	 }
        	 return retInt;
         }catch (SQLException ex) {
             Rlog.fatal("","AuditRowEpatDao.getRID EXCEPTION IN READING "+ tableName +" table "+ ex);
             return retInt;
         } finally {
             try {
                 if (pstmt != null)
                     pstmt.close();
             } catch (Exception e) {
             }
             try {
                 if (conn != null)
                     conn.close();
             } catch (Exception e) {
             }
         }
    }
    
    public ArrayList getAllLatestRaids(int rid, int userId, int lastRaid) {
    	ArrayList listRaids = new ArrayList();
    	PreparedStatement pstmt = null;
        Connection conn = null;
        String sql="";
        int rowCount = 0;
        try{
       	 if (rid < 0 || userId < 0){
       		listRaids = null;
       	 }else{
	        	 conn = getPatConnection();
	        	 sql = "SELECT * FROM epat.AUDIT_ROW auditRow " +
					" where auditRow.RID = " + rid +
					" AND EXISTS (SELECT auColumn.RAID FROM epat.AUDIT_COLUMN auColumn" +
					" WHERE auditRow.RAID = auColumn.RAID "+
					" AND  auditRow.RAID > " + lastRaid +
					" AND (auColumn.REMARKS IS NULL OR LENGTH(auColumn.REMARKS)=0)) " +
					" AND (auditRow.USER_NAME = '" + userId + "' OR auditRow.USER_NAME like '" + userId + ",%') " +
					" ORDER BY 1 DESC";
	        	 pstmt = conn.prepareStatement(sql);
	             ResultSet rs = pstmt.executeQuery();

	             while (rs.next()) {
	            	 listRaids.add(rs.getInt("RAID"));
	            	 rowCount++;
	             }
       	 }
       	 return listRaids;
        }catch (SQLException ex) {
            Rlog.fatal("","AuditRowEpatDao.getAllLatestRaids EXCEPTION IN READING epat.AUDIT_ROW table "+ ex);
            return listRaids;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
   }
    
    public int setReasonForChange(int raid, String remarks) {
   	 	PreparedStatement pstmt = null;
        Connection conn = null;
        int ret=0;
        String sql="";
        try{
       	 if (StringUtil.isEmpty(remarks) || raid < 0){
       		 ret = -1;
       	 }else{
        	 conn = getPatConnection();
        	 sql="UPDATE epat.AUDIT_COLUMN set REMARKS ='"+ remarks +"' WHERE RAID ="+raid
        	 +" AND (REMARKS IS NULL OR LENGTH(REMARKS)=0)";
        	 pstmt = conn.prepareStatement(sql);
       		 ret=pstmt.executeUpdate();
       		 conn.commit();
       		 ret = 1;
       	 }
       	 return ret;
        }catch (SQLException ex) {
            Rlog.fatal("","AuditRowEpatDao.getRID EXCEPTION IN UPDATING AUDIT_COLUMN table " + ex);
            return ret;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
   	
   }
}  
