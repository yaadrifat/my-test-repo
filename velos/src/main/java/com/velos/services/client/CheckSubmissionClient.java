package com.velos.services.client;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.util.JNDINames;
import com.velos.services.OperationException;
import com.velos.services.checksubmission.SubmissionLogicService;
import com.velos.services.model.ReviewBoards;
import com.velos.services.model.StudyIdentifier;

public class CheckSubmissionClient {

	public static ReviewBoards getStudyCheckAndSubmitStatusResponse(StudyIdentifier studyIdentifier) 
			throws OperationException{
		// TODO Auto-generated method stub
		SubmissionLogicService csService = getCheckSubmitHome();
		return csService.getStudyCheckAndSubmitStatusResponse(studyIdentifier);
	}
	
	public static SubmissionLogicService getCheckSubmitHome(){
		SubmissionLogicService csService = null;
		InitialContext ic;
		try{
			ic = new InitialContext();
			csService = (SubmissionLogicService)ic.lookup(JNDINames.SubmissionLogicHome);
			
		}catch(NamingException ne){
			
		}
		return csService;
	}

}
