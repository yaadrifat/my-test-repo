package com.velos.services.client;

import java.util.Date;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.OperationException;
import com.velos.services.OutboundMessage.OutboundMessageService;
import com.velos.services.model.ChangesList;
import com.velos.services.util.JNDINames;

public class OutboundMessageClient {
	
	private static OutboundMessageService getOutboundMessagesRemote()
	throws OperationException{
		
	OutboundMessageService outboundMessageRemote = null;
	InitialContext ic;
	try{
		ic = new InitialContext();
		outboundMessageRemote =
			(OutboundMessageService) ic.lookup(JNDINames.OutboundMessageServiceImpl);
		}
		catch(NamingException e){
			throw new OperationException(e);
		}
		return outboundMessageRemote;
	}
	
	public static ChangesList getOutboundMessages(Date fromDate,Date toDate,String messageModule,String calledFrom) 
	throws OperationException{
		OutboundMessageService outboundMessageService = getOutboundMessagesRemote();
		return outboundMessageService.getOutboundMessages(fromDate,toDate,messageModule,calledFrom);
	}

}
