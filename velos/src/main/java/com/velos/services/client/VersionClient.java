package com.velos.services.client;



import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.Versions;
import com.velos.services.util.JNDINames;
import com.velos.services.version.VersionService;

public class VersionClient {
	private static ResponseHolder response = new ResponseHolder();
	
	private static VersionService getVersionRemote() throws OperationException
	{
		VersionService versionService = null;
		InitialContext ic;
		try
		{
			ic = new InitialContext();
			versionService =(VersionService) ic.lookup(JNDINames.VersionServiceImpl);
		}
		catch(NamingException e){
			throw new OperationException(e);
		}
		return versionService;
	}
	
	public static Versions getStudyVesrion(StudyIdentifier studyIdent,boolean currentStatus) throws OperationException
	{
		VersionService versionService = getVersionRemote();
		return versionService.getStudyVersion(studyIdent,currentStatus);
	}
	
	public static ResponseHolder createVersions(Versions versions) throws OperationException {
		VersionService versionService = getVersionRemote();
		return versionService.createVersions(versions);
	}

}
