package com.velos.services.client;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.util.JNDINames;
import com.velos.services.workflow.WorkflowService;

public class WorkflowClient {
	/**
	 * 
	 * @param studyId
	 * @return
	 * @throws OperationException
	 */
	public static ResponseHolder calculateWorkFlow(
			StudyIdentifier studyId) 
	throws OperationException{
		System.out.println("Inside WorkflowClient");
		WorkflowService studyRemote = getWorkflowRemote();
		return studyRemote.calculateWorkFlow(studyId);
	}
	
	/**
	 * 
	 * @return
	 * @throws NamingException
	 */
	private static WorkflowService getWorkflowRemote()
		throws OperationException{
		
		WorkflowService workflowRemote = null;
		InitialContext ic;
		try{
			ic = new InitialContext();
			workflowRemote =
				(WorkflowService) ic.lookup(JNDINames.WorkflowServiceImpl);
		}
		catch(NamingException e){
			throw new OperationException(e);
		}
		return workflowRemote;
	}
}
