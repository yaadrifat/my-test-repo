package com.velos.services.client;

import java.util.Date;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.EventStatus;
import com.velos.services.model.EventStatusHistory;
import com.velos.services.model.MEventStatuses;
import com.velos.services.model.MPatientScheduleList;
import com.velos.services.model.MPatientSchedules;
import com.velos.services.model.MultipleEvents;
import com.velos.services.model.PatientProtocolIdentifier;
import com.velos.services.model.PatientSchedule;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientSchedules;
import com.velos.services.model.ScheduleEventStatuses;
import com.velos.services.model.SitesOfService;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.VisitIdentifier;
import com.velos.services.patientschedule.PatientScheduleService;
import com.velos.services.patientschedule.PatientScheduleServiceBMT;
import com.velos.services.util.JNDINames;
/***
 * Client Class/Remote object for Patient Schedule Services 
 * @author Virendra
 *
 */
public class PatientScheduleClient{
	/**
	 * Invokes the remote object.
	 * @return remote PtientDemo service
	 * @throws OperationException
	 */
	
	private static PatientScheduleService getPatScheduleRemote()
	throws OperationException{
		
	PatientScheduleService PatScheduleRemote = null;
	InitialContext ic;
	try{
		ic = new InitialContext();
		PatScheduleRemote =
			(PatientScheduleService) ic.lookup(JNDINames.PatientScheduleServiceImpl);
		}
		catch(NamingException e){
			throw new OperationException(e);
		}
		return PatScheduleRemote;
	}
	
	private static PatientScheduleServiceBMT getPatScheduleBMTRemote()
	throws OperationException{
		
	PatientScheduleServiceBMT PatScheduleRemote = null;
	InitialContext ic;
	try{
		ic = new InitialContext();
		PatScheduleRemote =
			(PatientScheduleServiceBMT) ic.lookup(JNDINames.PatientScheduleServiceBMTImpl);
		}
		catch(NamingException e){
			throw new OperationException(e);
		}
		return PatScheduleRemote;
	}
	/**
	 * Calls getPatientSchedule on getPatientSchedule Remote object
	 * to return PatientSchedule 
	 * @param patientId
	 * @param studyIdentifier
	 * @return PatSchedule
	 * @throws OperationException
	 */

	
	public static PatientSchedules getPatientScheduleList(PatientIdentifier patientId, StudyIdentifier studyIdentifier) 
	throws OperationException{
		PatientScheduleService patScheduleService = getPatScheduleRemote();
		return patScheduleService.getPatientScheduleList(patientId, studyIdentifier);
	}
	
	public static PatientSchedule getPatientSchedule(PatientProtocolIdentifier scheduleOID,VisitIdentifier visitIdentifier,String visitName)
	throws OperationException{
		PatientScheduleService patScheduleService = getPatScheduleRemote();
		return patScheduleService.getPatientSchedule(scheduleOID,visitIdentifier,visitName);
	}
	
	public static PatientSchedule getPatientScheduleVisits(PatientProtocolIdentifier scheduleOID)
	throws OperationException{
		PatientScheduleService patScheduleService = getPatScheduleRemote();
		return patScheduleService.getPatientScheduleVisits(scheduleOID);
	}
	
	public static PatientSchedule getCurrentPatientSchedule(PatientIdentifier patientId, StudyIdentifier studyIdentifier,Date startDate,Date endDate)
	throws OperationException{
		PatientScheduleService patScheduleService = getPatScheduleRemote();
		return patScheduleService.getCurrentPatientSchedule(patientId, studyIdentifier, startDate, endDate);
	}
	
	public static ResponseHolder addScheduleEventStatus(EventIdentifier eventIdentifier, EventStatus eventStatus)
	throws OperationException{
		PatientScheduleService patScheduleService = getPatScheduleRemote();
		return patScheduleService.addScheduleEventStatus(eventIdentifier, eventStatus);
	}
	
	public static SitesOfService getSitesOfService()
	throws OperationException{
		PatientScheduleService patScheduleService = getPatScheduleRemote();
		return patScheduleService.getSitesOfService();
	}
	/**
	 * @param scheduleID
	 * @param eventIdentifiers
	 * @param eventAttributes
	 * @param visitIdentifier
	 * @return
	 */
	public static ResponseHolder addUnscheduledEvent(
			PatientProtocolIdentifier scheduleID, StudyIdentifier studyIdentifier, 
			PatientIdentifier patientIdentifier,
			MultipleEvents eventIdentifiers,
			VisitIdentifier visitIdentifier)throws OperationException{
		PatientScheduleServiceBMT patScheduleService = getPatScheduleBMTRemote();
		return patScheduleService.addUnscheduledEvent(scheduleID, studyIdentifier, patientIdentifier, eventIdentifiers, visitIdentifier);

	}
	
//	public static ResponseHolder addAdminUnscheduledEvent(CalendarIdentifier scheduleID, EventIdentifiers eventIdentifiers, EventAttributes eventAttributes, VisitIdentifier visitIdentifier)
//	throws OperationException{
//		PatientScheduleServiceBMT patScheduleService = getPatScheduleBMTRemote(); 
//		return patScheduleService.addUnscheduledEvent(scheduleID, eventIdentifiers, eventAttributes, visitIdentifier);
//	}
	
	public static ResponseHolder addAdminUnscheduledEvent(CalendarIdentifier scheduleID, MultipleEvents events, VisitIdentifier visitIdentifier)
	throws OperationException{
		PatientScheduleServiceBMT patScheduleService = getPatScheduleBMTRemote(); 
		return patScheduleService.addUnscheduledEvent(scheduleID, events, visitIdentifier);
	}

	public static ResponseHolder createMEventStatus(
			MEventStatuses mEventStatuses) throws OperationException {
		PatientScheduleService patScheduleService = getPatScheduleRemote();
		return patScheduleService.createMEventStatus(mEventStatuses);
	}
	
	public static ResponseHolder updateMEventStatus(ScheduleEventStatuses scheduleEventStatusIdentifiers)
	throws OperationException{
		PatientScheduleService patScheduleService = getPatScheduleRemote();
		return patScheduleService.updateMEventStatus(scheduleEventStatusIdentifiers);
	}
	
	public static MPatientScheduleList addMPatientSchedules(MPatientSchedules mPatientSchedules)
	throws OperationException{
		PatientScheduleService patScheduleService = getPatScheduleRemote();
		return patScheduleService.addMPatientSchedules(mPatientSchedules);
	}
	
	public static EventStatusHistory getEventStatusHistory(EventIdentifier eventIdentifier, String sortBy, String orderBy)
	throws OperationException{
		PatientScheduleService patScheduleService = getPatScheduleRemote();
		return patScheduleService.getEventStatusHistory(eventIdentifier, sortBy, orderBy);
	}
}