/**
 * Created On Dec 19, 2011
 */
package com.velos.services.client;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.formresponse.FormResponseService;
import com.velos.services.model.AccountFormRespns;
import com.velos.services.model.AccountFormResponse;
import com.velos.services.model.AccountFormResponseIdentifier;
import com.velos.services.model.AccountFormResponses;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.FieldNameValIdentifier;
import com.velos.services.model.FormDisplayTypeIdentifier;
import com.velos.services.model.FormIdentifier;
import com.velos.services.model.FormRespns;
import com.velos.services.model.FormResponse;
import com.velos.services.model.PatientFormRespns;
import com.velos.services.model.PatientFormResponse;
import com.velos.services.model.PatientFormResponseIdentifier;
import com.velos.services.model.PatientFormResponses;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.StudyFormRespns;
import com.velos.services.model.StudyFormResponse;
import com.velos.services.model.StudyFormResponseIdentifier;
import com.velos.services.model.StudyFormResponses;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatientFormResponse;
import com.velos.services.model.StudyPatientFormResponseIdentifier;
import com.velos.services.model.StudyPatientFormResponses;
import com.velos.services.model.StudyPatientScheduleFormRespns;
import com.velos.services.model.StudyPatientScheduleFormResponse;
import com.velos.services.model.VisitIdentifier;
import com.velos.services.util.JNDINames;

/**
 * @author Kanwaldeep
 *
 */
public class FormResponseRemoteObj {

	private static FormResponseService getFormResponseRemote() throws OperationException
	{
		FormResponseService formResponseRemote = null; 
		InitialContext ic; 
		
		try
		{
			ic = new InitialContext(); 
			formResponseRemote = (FormResponseService) ic.lookup(JNDINames.FormResponseServiceImpl);
			
			
		}catch(NamingException e){
				throw new OperationException(e);
		}
		
		return formResponseRemote; 
	}
	
	
	public static ResponseHolder createStudyFormResponse(
			StudyFormResponse studyFormResponse)
	throws OperationRolledBackException, OperationException {
		
		FormResponseService formResponseService = getFormResponseRemote(); 
		 return formResponseService.createStudyFormResponse(studyFormResponse); 
		
	}
	
	public static StudyFormResponse getStudyFormResponse(StudyFormResponseIdentifier studyFormResponseIdentifier)
	throws OperationRolledBackException, OperationException
	{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.getStudyFormResponse(studyFormResponseIdentifier); 
	}
	
	public static ResponseHolder createStudyPatientFormResponse(StudyPatientFormResponse studyPatientFormResponse) throws OperationException
	{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.createStudyPatientFormResponse(studyPatientFormResponse); 
	}
	
	
	public static StudyPatientFormResponse getStudyPatientFormResponse(StudyPatientFormResponseIdentifier formResponseIdentifier, boolean includeDesign) throws OperationException
	{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.getStudyPatientFormResponse(formResponseIdentifier); 
	}


	/**
	 * @param formResponseIdentifier
	 * @param studyPatientFormResponse
	 * @return
	 */
	public static ResponseHolder updateStudyPatientFormResponse(
			StudyPatientFormResponseIdentifier formResponseIdentifier,
			FormRespns studyPatientFormResponse) throws OperationException{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.updateStudyPatientFormResponse(formResponseIdentifier, studyPatientFormResponse);
	}
	
	public static ResponseHolder removeStudyPatientFormResponse(
			StudyPatientFormResponseIdentifier formResponseIdentifier) throws OperationException{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.removeStudyPatientFormResponse(formResponseIdentifier);
	}


	/**
	 * @param studyPatientScheduleFormResponse
	 * @param calendarIdentifier
	 * @param calendarName
	 * @param visitIdentifier
	 * @param visitName
	 * @param eventIdentifier
	 * @param eventName
	 * @return
	 * @throws OperationException 
	 */
	public static ResponseHolder createStudyPatientScheduleFormResponse(
			StudyPatientScheduleFormResponse studyPatientScheduleFormResponse,
			CalendarIdentifier calendarIdentifier, String calendarName,
			VisitIdentifier visitIdentifier, String visitName,
			EventIdentifier eventIdentifier, String eventName) throws OperationException {
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.createStudyPatientScheduleFormResponse(studyPatientScheduleFormResponse, calendarIdentifier, calendarName, visitIdentifier, visitName, eventIdentifier, eventName);
	}


	/**
	 * @param studyPatientFormResponseIdentifier
	 * @return
	 */
	public static StudyPatientScheduleFormResponse getStudyPatientScheduleFormResponse(
			StudyPatientFormResponseIdentifier studyPatientFormResponseIdentifier) throws OperationException{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.getStudyPatientScheduleFormResponse(studyPatientFormResponseIdentifier); 
		
	}
	
	public static ResponseHolder updateStudyPatientScheduleFormResponse(
			StudyPatientFormResponseIdentifier studyPatientFormResponseIdentifier,
			StudyPatientScheduleFormRespns studyPatientScheduleFormResponse) throws OperationException {
		
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.updateStudyPatientScheduleFormResponse(studyPatientFormResponseIdentifier, studyPatientScheduleFormResponse); 
		
	}


	/**
	 * @param studyPatientFormResponseIdentifier
	 * @return
	 */
	public static ResponseHolder removeStudyPatientScheduleFormResponse(
			StudyPatientFormResponseIdentifier studyPatientFormResponseIdentifier) throws OperationException{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.removeStudyPatientScheduleFormResponse(studyPatientFormResponseIdentifier);
	}


	/**
	 * @param formResponseIdentifier
	 * @param studyFormResponse
	 */
	//Kavitha: #30863
	//changing StudyFormResponse from StudyFormRespns.
	public static ResponseHolder updateStudyFormResponse(
			StudyFormResponseIdentifier formResponseIdentifier,
			StudyFormResponse studyFormResponse) 
	throws OperationException{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.updateStudyFormResponse(formResponseIdentifier, studyFormResponse); 
		
	}
	
	/**
	 * @param formResponseIdentifier
	 * @param accountFormResponse
	 * @param formDisplayTypeIdentifier 
	 */
	public static ResponseHolder updateAccountFormResponse(
			AccountFormResponseIdentifier formResponseIdentifier,
			AccountFormRespns accountFormResponse, FormDisplayTypeIdentifier formDisplayTypeIdentifier) 
	throws OperationException{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.updateAccountFormResponse(formResponseIdentifier, accountFormResponse,formDisplayTypeIdentifier); 
		
	}
	
	
	public static ResponseHolder removeStudyFormResponse(
			StudyFormResponseIdentifier formResponseIdentifier)
	throws OperationException
	{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.removeStudyFormResponse(formResponseIdentifier); 
	}
	
	public static StudyPatientFormResponses getListOfStudyPatientFormResponses(
			FormIdentifier formIdentifier,
			FieldNameValIdentifier fieldNameValIdentifier,
			PatientIdentifier patientIdentifier,
			StudyIdentifier studyIdentifier,
			int pageNumber,
			int pageSize) throws OperationException {
		
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.getListOfStudyPatientFormResponses(formIdentifier,fieldNameValIdentifier, patientIdentifier, studyIdentifier, pageNumber, pageSize); 
	}


	public static StudyFormResponses getListOfStudyFormResponses(
			FormIdentifier formIdentifier, FieldNameValIdentifier fieldNameValIdentifier, StudyIdentifier studyIdentifier,
			int pageNumber, int pageSize) throws OperationException {
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.getListOfStudyFormResponses(formIdentifier,fieldNameValIdentifier, studyIdentifier, pageNumber, pageSize);
		
	}
	
	public static PatientFormResponses getListOfPatientFormResponses(
			FormIdentifier formIdentifier, FieldNameValIdentifier fieldNameValIdentifier, PatientIdentifier patientIdentifier,
			int pageNumber, int pageSize) throws OperationException {
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.getListOfPatientFormResponses(formIdentifier,fieldNameValIdentifier,patientIdentifier,pageNumber,pageSize);
		
	}
	
	/**
	 * @param patientFormResponse
	 */
	public static ResponseHolder createPatientFormResponse(
			PatientFormResponse patientFormResponse)
	throws OperationRolledBackException, OperationException {
		
		FormResponseService formResponseService = getFormResponseRemote(); 
		 return formResponseService.createPatientFormResponse(patientFormResponse); 
		
	}
	
	/**
	 * @param formResponseIdentifier
	 * @param studyFormResponse
	 */
	public static ResponseHolder updatePatientFormResponse(
			PatientFormResponseIdentifier formResponseIdentifier,
			PatientFormRespns patientFormResponse) 
	throws OperationException{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.updatePatientFormResponse(formResponseIdentifier, patientFormResponse); 
		
	}
	
	public static PatientFormResponse getPatientFormResponse(PatientFormResponseIdentifier patientFormResponseIdentifier)
	throws OperationRolledBackException, OperationException
	{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.getPatientFormResponse(patientFormResponseIdentifier);
	}
	
	public static ResponseHolder removePatientFormResponse(
			PatientFormResponseIdentifier formResponseIdentifier)
	throws OperationException
	{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.removePatientFormResponse(formResponseIdentifier); 
	}
	
	/**
	 * @param AccountFormResponse
	 */
	public static AccountFormResponse getAccountFormResponse(AccountFormResponseIdentifier accountFormResponseIdentifier)
	throws OperationRolledBackException, OperationException
	{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.getAccountFormResponse(accountFormResponseIdentifier);
	}
	
	/**
	 * @param formDisplayTypeIdentifier 
	 * @param AccountFormResponse
	 */
	public static ResponseHolder removeAccountFormResponse(
			AccountFormResponseIdentifier accountFormResponseIdentifier)
	throws OperationException
	{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.removeAccountFormResponse(accountFormResponseIdentifier); 
	}
	
	/**
	 * @param AccountFormResponse
	 */
	public static ResponseHolder createAccountFormResponse(
			AccountFormResponse accountFormResponse,
			FormDisplayTypeIdentifier formDisplayTypeIdentifier)
	throws OperationRolledBackException, OperationException {
		
		FormResponseService formResponseService = getFormResponseRemote(); 
		 return formResponseService.createAccountFormResponse(accountFormResponse,formDisplayTypeIdentifier); 
		
	}
	
	/**
	 * @param AccountFormResponse
	 */
	public static AccountFormResponses getListOfAccountFormResponses(
			FormIdentifier formIdentifier,FormDisplayTypeIdentifier formDisplayTypeIdentifier,FieldNameValIdentifier fieldNameValIdentifier, int pageNumber, int pageSize) throws OperationException {
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.getListOfAccountFormResponses(formIdentifier,formDisplayTypeIdentifier,fieldNameValIdentifier,pageNumber,pageSize);
		
	}
		
}
