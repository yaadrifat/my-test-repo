package com.velos.services.milestone;

import static javax.ejb.TransactionAttributeType.REQUIRED;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.study.impl.StudyBean;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.milestoneAgent.MilestoneAgentRObj;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Milestone;
import com.velos.services.model.MilestoneIdentifier;
import com.velos.services.model.MilestoneList;
import com.velos.services.model.MilestoneVDAPojo;
import com.velos.services.model.Milestonee;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.util.ObjectLocator;

@Stateless
@Remote(MilestoneService.class)
public class MilestoneServiceImpl 
extends AbstractService 
implements MilestoneService {
private static Logger logger = Logger.getLogger(MilestoneService.class.getName());
	
	@EJB
	GrpRightsAgentRObj groupRightsAgent;

	@Resource
	private SessionContext sessionContext;

	@EJB
	private UserAgentRObj userAgent;

	@EJB
	private ObjectMapService objectMapService;
	
	@EJB
	private MilestoneAgentRObj milestoneAgent;
	
	@EJB
	private StudyAgent studyAgent;
	
	/*private static final String PATIENT_MILESTONE="PM";
	private static final String VISIT_MILESTONE="VM";
	private static final String EVENT_MILESTONE="EM";
	private static final String STUDY_MILESTONE="SM";
	private static final String ADDITIONAL_MILESTONE="AM";*/
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Milestone getStudyMilestones(StudyIdentifier studyIdent) throws OperationException
	{
		try
		{  
			if(studyIdent==null || (StringUtil.isEmpty(studyIdent.getOID()) 
					&& (studyIdent.getPK()==null || studyIdent.getPK()<=0) 
					&& StringUtil.isEmpty(studyIdent.getStudyNumber())))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to getMilestones"));
			throw new OperationException();
			}
			
			Integer studyPK = locateStudyPK(studyIdent);
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdent.getOID() + " StudyNumber " + studyIdent.getStudyNumber()));
				throw new OperationException();
			}
			
			StudyBean sb = studyAgent.getStudyDetails(studyPK);
			if (sb == null || (! sb.getAccount().equals(callingUser.getUserAccountId()))){
				StringBuffer errorMessage = new StringBuffer("Study not found for:"); 
				if(studyIdent.getOID() != null && studyIdent.getOID().length() > 0 )
				{
					errorMessage.append(" OID:" + studyIdent.getOID()); 
				}
				if(studyIdent.getStudyNumber() != null && studyIdent.getStudyNumber().length() > 0)
				{
					errorMessage.append( " Study Number:" + studyIdent.getStudyNumber()); 
				}
				if(!StringUtil.isEmpty(StringUtil.integerToString(studyIdent.getPK())))
				{
					errorMessage.append( " Study Pk: " + studyIdent.getPK());
				}
				Issue issue = new Issue(IssueTypes.STUDY_NOT_FOUND, errorMessage.toString());
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(); 
			}
			//Get the user's permission for viewing protocols
			GroupAuthModule groupAuth = 
				new GroupAuthModule(this.callingUser, groupRightsAgent);

			Integer manageMilestonePriv = 
				groupAuth.getAppMilestonesPrivileges();

			boolean hasViewManageProt = 
				GroupAuthModule.hasViewPermission(manageMilestonePriv);
			if (logger.isDebugEnabled()) logger.debug("user manage Milestones priv: " + manageMilestonePriv);
			if (!hasViewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view study Milestones"));
				throw new AuthorizationException("User Not Authorized to view study Milestones");
			}
		  	    
			Map<String, Object> parameters = new HashMap<String, Object>(); 

			parameters.put("milestoneAgent", milestoneAgent);
			parameters.put("sessionContext", sessionContext); 
			parameters.put("userAgent", userAgent);
			parameters.put("objectMapService", objectMapService);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response); 
			parameters.put("studyAgent", studyAgent);
			List<MilestoneVDAPojo> milestoneList = new ArrayList<MilestoneVDAPojo>();
	        MilestoneServiceDao milestoneDao= new MilestoneServiceDao();
	        milestoneList = (ArrayList<MilestoneVDAPojo>)milestoneDao.getMilestoneDetails(String.valueOf(studyPK),parameters);

	        Milestone milestones=new Milestone();
	        /*StudyIdentifier studyId = new StudyIdentifier(sb.getStudyNumber());
			studyId.setStudyNumber(sb.getStudyNumber()); 
			studyId.setPK(studyPK);
			ObjectMap studyMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK); 
			studyId.setOID(studyMap.getOID());
	        milestones.setStudyIdentifier(studyId);*/
	        milestones.setMilestone(milestoneList);
	        return milestones;  
		}
		catch(OperationException e){
			e.printStackTrace();
			if (logger.isDebugEnabled()) logger.debug("MilestoneServiceImpl retrieved ", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("MilestoneServiceImpl retrieved", t);
			throw new OperationException(t);
		}
				
	}
	
	@TransactionAttribute(REQUIRED)
	public ResponseHolder createMMilestones(
			MilestoneList milestones)
			throws OperationException {
		try
		{
		
			System.out.println(" ---inside the createMMilestones method----");
			//--------Checking group authorization-------------//
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
			Integer MilestoneAccess = authModule.getAppMilestonesPrivileges();
			if(!GroupAuthModule.hasViewPermission(MilestoneAccess))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to edit Milestone details")); 
				throw new AuthorizationException(); 
			}
			if(milestones == null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid MilestoneInfo is required")); 
				throw new OperationException(); 
			}
			if(milestones.getMilestone()== null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid CreateMMilestones is required")); 
				throw new OperationException(); 
			}
		
			Map<String, Object> parameters = new HashMap<String, Object>(); 

			parameters.put("milestoneAgent", milestoneAgent);
			parameters.put("sessionContext", sessionContext); 
			parameters.put("userAgent", userAgent);
			parameters.put("objectMapService", objectMapService);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response); 
			parameters.put("studyAgent", studyAgent);

			
			
			List<Milestonee> createMMilestoneList = milestones.getMilestone();
			StudyIdentifier studyIdent=milestones.getStudyIdentifier();
			StudyBean studyBean = null;
			List<Milestonee> milestoneList=new ArrayList<Milestonee>();
			Integer studyPK =null;
			if(createMMilestoneList.size() > 0){
			
			for(Milestonee milestoneDetail : createMMilestoneList)
			{
				if(milestoneDetail == null)
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "milestoneDetail is required to addMilestone"));
					continue;
				}

				
				// get StudyPK for checking Team rights for Study
				studyPK=null;
				studyPK = locateStudyPK(studyIdent);
				if(studyPK == null || studyPK == 0){
					addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for Milestone"));
					continue;
				}

				TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyPK); 
				Integer manageMilestonePriv = teamAuth.getStudyMilestonesPrivileges(); 
				boolean hasEnrollPatientPriv = TeamAuthModule.hasNewPermission(manageMilestonePriv); 
				
				if(!hasEnrollPatientPriv)
				{
					addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User Not Authorized to add milsetone"));
					continue;
				}

				studyBean =null;
				studyBean = studyAgent.getStudyDetails(studyPK);
				if(studyBean==null)
				{
					((ResponseHolder)parameters.get("ResponseHolder")).addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND,"Study Not found"));
					continue;
				}
				
				
				milestoneList.add(milestoneDetail);
					}
			}
			else{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid MilestoneInfo is required")); 
				throw new OperationException();
			}
			MilestoneServiceDao handler = new MilestoneServiceDao(); 
			String[] values=handler.saveMileStones(studyPK,milestoneList,parameters);
			for( int i = 0; i < values.length; i++ )
			{
				int ret=StringUtil.stringToNum(values[i]);
				System.out.println("Returned is "+ret);
				if(ret>0)
				{
					ObjectMap milestoneMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_MILESTONE, ret); 
					MilestoneIdentifier mileIdentifier = new MilestoneIdentifier(); 
					mileIdentifier.setOID(milestoneMap.getOID()); 
					mileIdentifier.setPK(ret);
					response.addObjectCreatedAction(mileIdentifier);
				}
				else
				{
					response.addIssue(new Issue(IssueTypes.ERROR_CREATE_MILESTONE, values[i]));
				}
			}
			
		}
		catch(OperationException e){
			if (logger.isDebugEnabled()) logger.debug("MilestoneServiceImpl createMilestones", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("MilestoneServiceImpl createMilestones", t);
			throw new OperationException(t);
		}
		// TODO Auto-generated method stub
		return response;
	}
	
	/*private void milestoneIntoBean(
			Integer studyPK,
			Milestonee mileSummary, 
			MilestonePojo persistentMileBean)
	throws 
	OperationException{
		
		persistentMileBean.setIP_ADD(IP_ADDRESS_FIELD_VALUE);
		
		persistentMileBean.setCREATOR(callingUser.getUserId());
		persistentMileBean.setFK_STUDY(studyPK);
		persistentMileBean.setMILESTONE_AMOUNT(mileSummary.getMILESTONE_AMOUNT());
		persistentMileBean.setMILESTONE_COUNT(mileSummary.getMILESTONE_COUNT());
		persistentMileBean.setMILESTONE_DELFLAG(mileSummary.getMILESTONE_DELFLAG());
		persistentMileBean.setMILESTONE_TYPE(mileSummary.getMILESTONE_TYPE());
		persistentMileBean.setMILESTONE_DESCRIPTION(mileSummary.getMILESTONE_DESCRIPTION());
		persistentMileBean.setMILESTONE_LIMIT(mileSummary.getMILESTONE_LIMIT());
		persistentMileBean.setMILESTONE_HOLDBACK(mileSummary.getMILESTONE_HOLDBACK());
		//persistentMileBean.setMILESTONE_ISACTIVE(mileSummary.getMILESTONE_ISACTIVE());
		//persistentMileBean.setCREATED_ON();
		//persistentStudyBean.setMILESTONE_DATE_TO(mILESTONE_DATE_TO);
		//persistentStudyBean.setMILESTONE_DATE_FROM(mILESTONE_DATE_FROM);
		try{//TODO Milestone_Status is required, but if we can't find a code, what to do?
			if(mileSummary.getMILESTONE_TYPE().equals(PATIENT_MILESTONE)){
			persistentMileBean.setMILESTONE_STATUS(
					StringUtil.stringToInteger(dereferenceCodeStr(mileSummary.getMILESTONE_STATUS(), CodeCache.CODE_TYPE_PATPROT_STATUS, callingUser)));}
			else if(mileSummary.getMILESTONE_TYPE().equals(STUDY_MILESTONE)){
				persistentMileBean.setMILESTONE_STATUS(
						StringUtil.stringToInteger(dereferenceCodeStr(mileSummary.getMILESTONE_STATUS(), CodeCache.CODE_TYPE_STATUS, callingUser)));}
			else if(mileSummary.getMILESTONE_TYPE().equals(ADDITIONAL_MILESTONE)){
				persistentMileBean.setMILESTONE_STATUS(
						StringUtil.stringToInteger(dereferenceCodeStr(mileSummary.getMILESTONE_STATUS(), CodeCache.CODE_TYPE_MILESTONE_STATUS, callingUser)));}
			else{}
		}catch(CodeNotFoundException e){
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "patient, study Status Code Not Found: "));
		}
		try{//TODO Milestone_Status is required, but if we can't find a code, what to do?
			persistentMileBean.setFK_CODELST_MILESTONE_STAT(
					StringUtil.stringToInteger(dereferenceCodeStr(mileSummary.getFK_CODELST_MILESTONE_STAT(), CodeCache.CODE_TYPE_PATPROT_STATUS, callingUser)));
		}catch(CodeNotFoundException e){
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Milestone_Status Code Not Found: "));
		}
		try{//TODO Milestone_PayFor is required, but if we can't find a code, what to do?
			persistentMileBean.setMILESTONE_PAYFOR(
					StringUtil.stringToInteger(dereferenceCodeStr(mileSummary.getMILESTONE_PAYFOR(), CodeCache.CODE_TYPE_PATPROT_STATUS, callingUser)));
		}catch(CodeNotFoundException e){
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Milestone_PayFor Code Not Found: "));
		}
		try{//TODO Milestone_EventStatus is required, but if we can't find a code, what to do? EM VM
			persistentMileBean.setMILESTONE_EVENTSTATUS(
					StringUtil.stringToInteger(dereferenceCodeStr(mileSummary.getMILESTONE_EVENTSTATUS(), CodeCache.CODE_TYPE_PATPROT_STATUS, callingUser)));
		}catch(CodeNotFoundException e){
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Milestone_EventStatus Code Not Found: "));
		}
		try{//TODO Milestone_Rule is required, but if we can't find a code, what to do? EM VM
			persistentMileBean.setFK_CODELST_RULE(
					StringUtil.stringToInteger(dereferenceCodeStr(mileSummary.getFK_CODELST_RULE(), CodeCache.CODE_TYPE_PATPROT_STATUS, callingUser)));
		}catch(CodeNotFoundException e){
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Milestone_Rule Code Not Found: "));
		}
		try{//TODO Milestone_PayType is required, but if we can't find a code, what to do?
			persistentMileBean.setMILESTONE_PAYTYPE(
					StringUtil.stringToInteger(dereferenceCodeStr(mileSummary.getMILESTONE_PAYTYPE(), CodeCache.CODE_TYPE_PATPROT_STATUS, callingUser)));
		}catch(CodeNotFoundException e){
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Milestone_PayType Code Not Found: "));
		}
		
	}*/
	
	private Integer locateStudyPK(StudyIdentifier studyIdent) 
			throws OperationException{
				//Virendra:#6123,added OID in if clause
				if(studyIdent==null || (StringUtil.isEmpty(studyIdent.getOID()) 
						&& (studyIdent.getPK()==null || studyIdent.getPK()<=0) 
						&& StringUtil.isEmpty(studyIdent.getStudyNumber())))
				{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studydentifier is required to addMilestones"));
				throw new OperationException();
				}
				
				Integer studyPK =
						ObjectLocator.studyPKFromIdentifier(this.callingUser, studyIdent,objectMapService);
				if (studyPK == null || studyPK == 0){
					StringBuffer errorMessage = new StringBuffer("Study not found for:"); 
					if(studyIdent.getOID() != null && studyIdent.getOID().length() > 0 )
					{
						errorMessage.append(" OID: " + studyIdent.getOID()); 
					}
					if(studyIdent.getStudyNumber() != null && studyIdent.getStudyNumber().length() > 0)
					{
						errorMessage.append( " Study Number: " + studyIdent.getStudyNumber()); 
					}
					addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, errorMessage.toString()));
					throw new OperationException();
				}
				return studyPK;
			}
	
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}

}
