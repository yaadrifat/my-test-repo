package com.velos.services.milestone;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import com.velos.eres.business.milestone.impl.MilestoneBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.util.SVC;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.account.AccountJB;
import com.velos.services.AbstractService;
import com.velos.services.CodeNotFoundException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.ResponseHolder;
import com.velos.services.ValidationException;
import com.velos.services.model.Milestonee;
import com.velos.services.util.CodeCache;

public class MilestoneHandler extends AbstractService {
	private static Logger logger = Logger.getLogger(MilestoneHandler.class);
	public static final String PATIENT_MILESTONE="PM";
	public static final String VISIT_MILESTONE="VM";
	public static final String EVENT_MILESTONE="EM";
	public static final String STUDY_MILESTONE="SM";
	public static final String ADDITIONAL_MILESTONE="AM";
	
	public static void validateMilestoneDetails(Integer studyPK,
			Milestonee mileSummary, 
			MilestoneBean persistentMileBean, UserBean callingUser,Map<String, Object> parameters,List<Issue> validationIssues) throws ValidationException,IOException,HibernateException
	{
		AccountJB acctJB = new AccountJB(); 
		acctJB.setAccId(StringUtil.stringToInteger(callingUser.getUserAccountId())); 
		acctJB.getAccountDetails(); 
		

		//List<Issue> validationIssues = new ArrayList<Issue>();
		
		persistentMileBean.setCreator(StringUtil.integerToString(callingUser.getUserId()));
		persistentMileBean.setMilestoneStudyId(StringUtil.integerToString(studyPK));
		if(mileSummary.getMILESTONE_AMOUNT()!=null && !mileSummary.getMILESTONE_AMOUNT().equals("")){
		persistentMileBean.setMilestoneAmount(mileSummary.getMILESTONE_AMOUNT());}
		else{
			validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_Milestone_MileAmount_Mandatory));
		}
		persistentMileBean.setMilestoneDelFlag(mileSummary.getMILESTONE_DELFLAG());
		if(persistentMileBean.getMilestoneDelFlag()==null || persistentMileBean.getMilestoneDelFlag().equals(""))
			persistentMileBean.setMilestoneDelFlag("N");
		if(mileSummary.getMILESTONE_TYPE()!=null && mileSummary.getMILESTONE_TYPE()!="")
			persistentMileBean.setMilestoneType(mileSummary.getMILESTONE_TYPE());
		if( persistentMileBean.getMilestoneType() == null)
		{
			validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_Milestone_MileType_Mandatory));
			((ResponseHolder) parameters.get("ResponseHolder"))
			.getIssues().addAll(validationIssues); 
			
			if (logger.isDebugEnabled()) logger.debug("Validation Issues found for MilestoneDetails");
			throw new ValidationException();
		}
		if( !(persistentMileBean.getMilestoneType().equals(PATIENT_MILESTONE) ||
				persistentMileBean.getMilestoneType().equals(VISIT_MILESTONE)||
				persistentMileBean.getMilestoneType().equals(EVENT_MILESTONE)||
				persistentMileBean.getMilestoneType().equals(STUDY_MILESTONE)||
				persistentMileBean.getMilestoneType().equals(ADDITIONAL_MILESTONE)))
		{
			validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Not Valid Milestone Type"));
			((ResponseHolder) parameters.get("ResponseHolder"))
			.getIssues().addAll(validationIssues); 
			
			if (logger.isDebugEnabled()) logger.debug("Validation Issues found for MilestoneDetails");
			throw new ValidationException();
		}
		persistentMileBean.setMilestoneCount(StringUtil.integerToString(mileSummary.getMILESTONE_COUNT()));
		if(persistentMileBean.getMilestoneCount()!=null && ((persistentMileBean.getMilestoneType().equals(STUDY_MILESTONE)
				|| persistentMileBean.getMilestoneType().equals(ADDITIONAL_MILESTONE)))){
			validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Milsetone Count is not required for SM and AM"));
		}
		persistentMileBean.setMilestoneDescription(mileSummary.getMILESTONE_DESCRIPTION());
		persistentMileBean.setMilestoneLimit(StringUtil.integerToString(mileSummary.getMILESTONE_LIMIT()));
		if(persistentMileBean.getMilestoneType().equals(ADDITIONAL_MILESTONE) && persistentMileBean.getMilestoneLimit()!=null){
			validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Milsetone Limit is not required for AM"));
		}
		if(mileSummary.getMILESTONE_HOLDBACK()!=null){
		persistentMileBean.setHoldBack(StringUtil.stringToFloat(mileSummary.getMILESTONE_HOLDBACK()));
		}
		//persistentMileBean.setCREATED_ON(new Date());
		persistentMileBean.setIpAdd(IP_ADDRESS_FIELD_VALUE);
		if(!persistentMileBean.getMilestoneType().equals(ADDITIONAL_MILESTONE)){
		if(mileSummary.getMILESTONE_DATE_TO()!=null)
			persistentMileBean.setMilestoneDtTo(mileSummary.getMILESTONE_DATE_TO());
		if(mileSummary.getMILESTONE_DATE_FROM()!=null)
			persistentMileBean.setMilestoneDtFrom(mileSummary.getMILESTONE_DATE_FROM());}
		if(persistentMileBean.getMilestoneType().equals(VISIT_MILESTONE)||persistentMileBean.getMilestoneType().equals(EVENT_MILESTONE)){
			try{//TODO Milestone_Rule is required, but if we can't find a code, what to do? EM VM
				if(persistentMileBean.getMilestoneType().equals(VISIT_MILESTONE))
				persistentMileBean.setMilestoneRuleId(
						StringUtil.integerToString(dereferenceCode(mileSummary.getPK_CODELST_RULE(), CodeCache.CODE_TYPE_VM_RULE, callingUser)));
				else
					persistentMileBean.setMilestoneRuleId(
							StringUtil.integerToString(dereferenceCode(mileSummary.getPK_CODELST_RULE(), CodeCache.CODE_TYPE_EM_RULE, callingUser)));
			}catch(CodeNotFoundException e){
				validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Milestone_Rule Code Not Found: "));
				((ResponseHolder) parameters.get("ResponseHolder"))
				.getIssues().addAll(validationIssues); 
				
				if (logger.isDebugEnabled()) logger.debug("Validation Issues found for MilestoneDetails");
				throw new ValidationException();
			}
			
			if(mileSummary.getCalendarNameIdent()!=null  && mileSummary.getCalendarNameIdent().getCalendarAssocTo()!=null && mileSummary.getCalendarNameIdent().getCalendarName()!=null){
				persistentMileBean.setMilestoneCalId(
						StringUtil.integerToString(MilestoneServiceDao.getCalenderPKByXMLInputString(mileSummary.getCalendarNameIdent().getCalendarName(),mileSummary.getCalendarNameIdent().getCalendarAssocTo(), StringUtil.integerToString(studyPK))));
				if(StringUtil.stringToInteger(persistentMileBean.getMilestoneCalId())<=0){
					validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Calendar Not Found: EM or VM milestone"));
					((ResponseHolder) parameters.get("ResponseHolder"))
					.getIssues().addAll(validationIssues); 
					
					if (logger.isDebugEnabled()) logger.debug("Validation Issues found for MilestoneDetails");
					throw new ValidationException();
				}else
				{
				if(mileSummary.getVisitNameIdent()!=null){
				persistentMileBean.setMilestoneVisitFK(StringUtil.integerToString(MilestoneServiceDao.getVisitPKByXMLInputString(mileSummary.getVisitNameIdent().getVisitName(), 
						persistentMileBean.getMilestoneCalId())));}
				else{
					persistentMileBean.setMilestoneVisitFK("-2");
				}
			if(persistentMileBean.getMilestoneType().equals(EVENT_MILESTONE)){
				if(StringUtil.stringToInteger(persistentMileBean.getMilestoneVisitFK())<=0){
					validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Visit Not Found for EM Milestone: "));
					((ResponseHolder) parameters.get("ResponseHolder"))
					.getIssues().addAll(validationIssues); 
					
					if (logger.isDebugEnabled()) logger.debug("Validation Issues found for MilestoneDetails");
					throw new ValidationException();
				}
			}else{
				 if(StringUtil.stringToInteger(persistentMileBean.getMilestoneVisitFK())<=0)
					persistentMileBean.setMilestoneVisitFK("-1");
				}
			
			if(persistentMileBean.getMilestoneType().equals(EVENT_MILESTONE) && mileSummary.getEventNameIdent()!=null){
				persistentMileBean.setMilestoneEvent(StringUtil.integerToString(MilestoneServiceDao.getEventPKByXMLInputString(mileSummary.getEventNameIdent().getEventName(), 
						persistentMileBean.getMilestoneCalId(),persistentMileBean.getMilestoneVisitFK())));
				if(StringUtil.stringToInteger(persistentMileBean.getMilestoneEvent())<=0){
					validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Event Not Found for: EM milestone"));
					((ResponseHolder) parameters.get("ResponseHolder"))
					.getIssues().addAll(validationIssues); 
					
					if (logger.isDebugEnabled()) logger.debug("Validation Issues found for MilestoneDetails");
					throw new ValidationException();
				}
			 }
			}
				
			}else
			{
				validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "CalendarName and CalAssocTo Not Found: EM or VM milestone: "));
				((ResponseHolder) parameters.get("ResponseHolder"))
				.getIssues().addAll(validationIssues); 
				
				if (logger.isDebugEnabled()) logger.debug("Validation Issues found for MilestoneDetails");
				throw new ValidationException();
			}
		}
		
		if(persistentMileBean.getMilestoneType().equals(ADDITIONAL_MILESTONE)){
			if(mileSummary.getEventNameIdent()!=null && mileSummary.getCalendarNameIdent()!=null && mileSummary.getVisitNameIdent()!=null){
				String pk_cal="0";
				String fk_visit="0";
				if(mileSummary.getCalendarNameIdent().getCalendarName()!=null && mileSummary.getCalendarNameIdent().getCalendarAssocTo()!=null){
				pk_cal=	StringUtil.integerToString(MilestoneServiceDao.getCalenderPKByXMLInputString(mileSummary.getCalendarNameIdent().getCalendarName(),mileSummary.getCalendarNameIdent().getCalendarAssocTo(), StringUtil.integerToString(studyPK)));
				}else{
					validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "CalendarName and CalAssocTo Not Found: AM milestone: "));
					((ResponseHolder) parameters.get("ResponseHolder"))
					.getIssues().addAll(validationIssues); 
					
					if (logger.isDebugEnabled()) logger.debug("Validation Issues found for MilestoneDetails");
					throw new ValidationException();
				}
				if(mileSummary.getVisitNameIdent().getVisitName()!=null){
				fk_visit= StringUtil.integerToString(MilestoneServiceDao.getVisitPKByXMLInputString(mileSummary.getVisitNameIdent().getVisitName(), 
						pk_cal));
				}else{
					validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Visit Not Found for AM Milestone: "));
					((ResponseHolder) parameters.get("ResponseHolder"))
					.getIssues().addAll(validationIssues); 
					
					if (logger.isDebugEnabled()) logger.debug("Validation Issues found for MilestoneDetails");
					throw new ValidationException();
				}
				if((pk_cal!=null && StringUtil.stringToInteger(pk_cal)<=0) && (fk_visit!=null && StringUtil.stringToInteger(fk_visit)<=0)){
					validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Calendar And Visit Not Found for: AM milestone"));
					((ResponseHolder) parameters.get("ResponseHolder"))
					.getIssues().addAll(validationIssues); 
					
					if (logger.isDebugEnabled()) logger.debug("Validation Issues found for MilestoneDetails");
					throw new ValidationException();
				}else
				{  	persistentMileBean.setMilestoneEvent(StringUtil.integerToString(MilestoneServiceDao.getEventPKByXMLAMInputString(mileSummary.getEventNameIdent().getEventName(), 
						pk_cal,fk_visit)));
				if(StringUtil.stringToInteger(persistentMileBean.getMilestoneEvent())<=0){
					validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Event Not Found for: AM milestone"));
					((ResponseHolder) parameters.get("ResponseHolder"))
					.getIssues().addAll(validationIssues); 
					
					if (logger.isDebugEnabled()) logger.debug("Validation Issues found for MilestoneDetails");
					throw new ValidationException();
				}
				}
			
			}else if(mileSummary.getEventNameIdent()!=null && (mileSummary.getCalendarNameIdent()==null || mileSummary.getVisitNameIdent()==null)){
				validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Calendar, Visit Info Not Found for: AM milestone"));
				((ResponseHolder) parameters.get("ResponseHolder"))
				.getIssues().addAll(validationIssues); 
				
				if (logger.isDebugEnabled()) logger.debug("Validation Issues found for MilestoneDetails");
				throw new ValidationException();
			}
		}
		
		
		if(persistentMileBean.getMilestoneType().equals(VISIT_MILESTONE)||persistentMileBean.getMilestoneType().equals(EVENT_MILESTONE)){
		if((mileSummary.getBgtCalNameIdentifier()!=null && mileSummary.getBgtCalNameIdentifier().getBgtCalName()!="")&&
				(mileSummary.getBudgetIdentifier()!=null && mileSummary.getBudgetIdentifier().getBudgetName()!="")&&
				(mileSummary.getBgtSectionNameIdentifier()!=null && mileSummary.getBgtSectionNameIdentifier().getBgtSectionName()!="")){
			persistentMileBean.setFk_Budget(StringUtil.integerToString(MilestoneServiceDao.getBudgetPKByXMLInputString(mileSummary.getBudgetIdentifier().getBudgetName(), StringUtil.integerToString(studyPK))));
			if(StringUtil.stringToInteger(persistentMileBean.getFk_Budget())>0){
					persistentMileBean.setFk_BgtCal(StringUtil.integerToString(MilestoneServiceDao.getBudgetFKCal(persistentMileBean.getFk_Budget(), persistentMileBean.getMilestoneCalId())));
					if(StringUtil.stringToInteger(persistentMileBean.getFk_BgtCal())>0){
							persistentMileBean.setFk_BgtSection(StringUtil.integerToString(MilestoneServiceDao.getBudgetSectionPKByXMLInputStr(mileSummary.getBgtSectionNameIdentifier().getBgtSectionName(), persistentMileBean.getFk_BgtCal())));
							if(StringUtil.stringToInteger(persistentMileBean.getFk_BgtSection())>0 && (persistentMileBean.getMilestoneType().equals(EVENT_MILESTONE))){
									persistentMileBean.setFk_LineItem(
											StringUtil.integerToString(MilestoneServiceDao.getLineItemPKByXMLInputStr(mileSummary.getLineItemNameIdentifier().getLineItemName(), persistentMileBean.getFk_BgtSection())));
								if(StringUtil.stringToInteger(persistentMileBean.getFk_LineItem())>0){}
								else{
									validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Budget LineItem Not Found for Event Milestone: "));
									((ResponseHolder) parameters.get("ResponseHolder"))
									.getIssues().addAll(validationIssues); 
									
									if (logger.isDebugEnabled()) logger.debug("Validation Issues found for MilestoneDetails");
									throw new ValidationException();
								}
							}else{
							}
						
					}else{
						validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Budget Calendar Not Found for Event or Visit Milestone: "));
						((ResponseHolder) parameters.get("ResponseHolder"))
						.getIssues().addAll(validationIssues); 
						
						if (logger.isDebugEnabled()) logger.debug("Validation Issues found for MilestoneDetails");
						throw new ValidationException();
					}
			}else{
				validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Budget Not Found for Event or Visit Milestone: "));
				((ResponseHolder) parameters.get("ResponseHolder"))
				.getIssues().addAll(validationIssues); 
				
				if (logger.isDebugEnabled()) logger.debug("Validation Issues found for MilestoneDetails");
				throw new ValidationException();
				}
			}
		}
		
		if(persistentMileBean.getMilestoneType().equals(ADDITIONAL_MILESTONE)){
		if((mileSummary.getBgtCalNameIdentifier()!=null && mileSummary.getBgtCalNameIdentifier().getBgtCalName()!="")&&
				(mileSummary.getBudgetIdentifier()!=null && mileSummary.getBudgetIdentifier().getBudgetName()!="")&&
				(mileSummary.getBgtSectionNameIdentifier()!=null && mileSummary.getBgtSectionNameIdentifier().getBgtSectionName()!="") &&
				(mileSummary.getLineItemNameIdentifier()!=null && mileSummary.getLineItemNameIdentifier().getLineItemName()!="")){
			persistentMileBean.setFk_Budget(StringUtil.integerToString(MilestoneServiceDao.getBudgetPKByXMLInputString(mileSummary.getBudgetIdentifier().getBudgetName(), StringUtil.integerToString(studyPK))));
			if(StringUtil.stringToInteger(persistentMileBean.getFk_Budget())>0){
					persistentMileBean.setFk_BgtCal(StringUtil.integerToString(MilestoneServiceDao.getBudgetFKCalAM(persistentMileBean.getFk_Budget())));
					if(StringUtil.stringToInteger(persistentMileBean.getFk_BgtCal())>0){
							persistentMileBean.setFk_BgtSection(StringUtil.integerToString(MilestoneServiceDao.getBudgetSectionPKByXMLInputStr(mileSummary.getBgtSectionNameIdentifier().getBgtSectionName(), persistentMileBean.getFk_BgtCal())));
							if(StringUtil.stringToInteger(persistentMileBean.getFk_BgtSection())>0){
									persistentMileBean.setFk_LineItem(
											StringUtil.integerToString(MilestoneServiceDao.getLineItemPKByXMLInputStr(mileSummary.getLineItemNameIdentifier().getLineItemName(), persistentMileBean.getFk_BgtSection())));
								if(StringUtil.stringToInteger(persistentMileBean.getFk_LineItem())>0){}
								else{
									validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Budget Lineitem Not Found for  Additional Milestone: "));
									((ResponseHolder) parameters.get("ResponseHolder"))
									.getIssues().addAll(validationIssues); 
									
									if (logger.isDebugEnabled()) logger.debug("Validation Issues found for MilestoneDetails");
									throw new ValidationException();
								}
							}else{
								validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Budget Section Not Found for  Additional Milestone: "));
								((ResponseHolder) parameters.get("ResponseHolder"))
								.getIssues().addAll(validationIssues); 
								
								if (logger.isDebugEnabled()) logger.debug("Validation Issues found for MilestoneDetails");
								throw new ValidationException();
							}
						
					}else{
						validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Budget Calendar Not Found for  Additional Milestone: "));
						((ResponseHolder) parameters.get("ResponseHolder"))
						.getIssues().addAll(validationIssues); 
						
						if (logger.isDebugEnabled()) logger.debug("Validation Issues found for MilestoneDetails");
						throw new ValidationException();
					}
			}else{
				validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Budget Not Found for  Additional Milestone: "));
				((ResponseHolder) parameters.get("ResponseHolder"))
				.getIssues().addAll(validationIssues); 
				
				if (logger.isDebugEnabled()) logger.debug("Validation Issues found for MilestoneDetails");
				throw new ValidationException();
				}
			}
		}
		
		try{//TODO Milestone_Status is required, but if we can't find a code, what to do?
			if(mileSummary.getMILESTONE_TYPE().equals(PATIENT_MILESTONE) || mileSummary.getMILESTONE_TYPE().equals(VISIT_MILESTONE) || 
					mileSummary.getMILESTONE_TYPE().equals(EVENT_MILESTONE)){
			persistentMileBean.setMilestoneStatus(
					StringUtil.integerToString(dereferenceCode(mileSummary.getMILESTONE_PATSTUDY_STAT(), CodeCache.CODE_TYPE_PATPROT_STATUS, callingUser)));}
			
		}catch(CodeNotFoundException e){
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "patient Status Code Not Found: "));
		}
		try{//TODO Milestone_Status is required, but if we can't find a code, what to do?
			if(mileSummary.getMILESTONE_TYPE().equals(STUDY_MILESTONE)){
				persistentMileBean.setMilestoneStatus(
						StringUtil.integerToString(dereferenceCode(mileSummary.getMILESTONE_PATSTUDY_STAT(), CodeCache.CODE_TYPE_STATUS, callingUser)));}
		}catch(CodeNotFoundException e){
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "study Status Code Not Found: "));
		}
		try{//TODO Milestone_Status is required, but if we can't find a code, what to do?
			 if(mileSummary.getMILESTONE_TYPE().equals(ADDITIONAL_MILESTONE)){
				persistentMileBean.setMilestoneStatus(
						StringUtil.integerToString(dereferenceCode(mileSummary.getMILESTONE_PATSTUDY_STAT(), CodeCache.CODE_TYPE_MILESTONE_STATUS, callingUser)));}
		}catch(CodeNotFoundException e){
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Milestone Status Code Not Found: "));
		}
		try{//TODO Milestone_Status is required, but if we can't find a code, what to do?
			persistentMileBean.setMilestoneStatFK(
					StringUtil.integerToString(dereferenceCode(mileSummary.getPK_CODELST_MILESTONE_STAT(), CodeCache.CODE_TYPE_MILESTONE_STATUS, callingUser)));
		}catch(CodeNotFoundException e){
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Milestone_Status Code Not Found: "));
		}
		/*if(mileSummary.getPK_CODELST_MILESTONE_STAT().getCode().equals("IA")){
			persistentMileBean.setMILESTONE_ISACTIVE(true);
		}else{
			persistentMileBean.setMILESTONE_ISACTIVE(false);
		}*/
		try{//TODO Milestone_PayType is required, but if we can't find a code, what to do?
			persistentMileBean.setMilestonePayType(
					StringUtil.integerToString(dereferenceCode(mileSummary.getMILESTONE_PAYTYPE(), CodeCache.CODE_TYPE_PAY_TYPE, callingUser)));
		}catch(CodeNotFoundException e){
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Milestone_PayType Code Not Found: "));
		}
		try{//TODO Milestone_PayFor is required, but if we can't find a code, what to do?
			persistentMileBean.setMilestonePayFor(
					StringUtil.integerToString(dereferenceCode(mileSummary.getMILESTONE_PAYFOR(), CodeCache.CODE_TYPE_PAY_FOR, callingUser)));
		}catch(CodeNotFoundException e){
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Milestone_PayFor Code Not Found: "));
		}
		try{//TODO Milestone_EventStatus is required, but if we can't find a code, what to do? EM VM
			persistentMileBean.setMilestoneEventStatus(
					StringUtil.integerToString(dereferenceSchCode(mileSummary.getMILESTONE_EVENTSTATUS(), CodeCache.CODE_TYPE_EVENT_STATUS_TYPE, callingUser)));
		}catch(CodeNotFoundException e){
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Milestone_EventStatus Code Not Found: "));
		}
				
			
			if( persistentMileBean.getMilestoneStatFK() == null || StringUtil.stringToInteger(persistentMileBean.getMilestoneStatFK())<=0)
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_Milestone_MileStat_Mandatory)); 
			}
			if( persistentMileBean.getMilestonePayType() == null || StringUtil.stringToInteger(persistentMileBean.getMilestonePayType())<=0)
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_Milestone_MilePayType_Mandatory)); 
			}
			if(persistentMileBean.getMilestoneType().equals(PATIENT_MILESTONE)  && (persistentMileBean.getMilestoneStatus() == null || StringUtil.stringToInteger(persistentMileBean.getMilestoneStatus())<=0)){
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_Milestone_MilePatStat_Mandatory));
			}
			if( persistentMileBean.getMilestoneType().equals(STUDY_MILESTONE) && (persistentMileBean.getMilestoneStatus() == null || StringUtil.stringToInteger(persistentMileBean.getMilestoneStatus())<=0)){
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_Milestone_MileStudyStat_Mandatory));
			}
			if( persistentMileBean.getMilestoneType().equals(VISIT_MILESTONE) && persistentMileBean.getMilestoneStatus() == null && mileSummary.getCalendarNameIdent().getCalendarAssocTo()=="P"){
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Milestone Patient Status may not be null for VM."));
			}
			if( persistentMileBean.getMilestoneType().equals(PATIENT_MILESTONE)  && (persistentMileBean.getMilestoneCount() == null)){
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_Milestone_MileCount_Mandatory));
			}
			if( persistentMileBean.getMilestoneType().equals(ADDITIONAL_MILESTONE)  && persistentMileBean.getMilestoneDescription() == null){
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_Milestone_MileDesc_Mandatory));
			}
			else{
				if(persistentMileBean.getMilestoneDescription() != null && !persistentMileBean.getMilestoneType().equals(ADDITIONAL_MILESTONE))
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Milestone Description Not Required for PM,SM,EM and VM."));
			}
			if(persistentMileBean.getMilestoneType().equals(EVENT_MILESTONE) && persistentMileBean.getMilestoneType().equals(VISIT_MILESTONE)){
				if(persistentMileBean.getMilestoneCalId()==null || StringUtil.stringToInteger(persistentMileBean.getMilestoneCalId())<=0){
					validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_Milestone_MileCal_Mandatory));
				}
				if(persistentMileBean.getMilestoneRuleId()==null || StringUtil.stringToInteger(persistentMileBean.getMilestoneRuleId())<=0){
					validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_Milestone_MileRule_Mandatory));
				}
				if(persistentMileBean.getMilestoneType().equals(EVENT_MILESTONE) && (persistentMileBean.getMilestoneEvent()==null || StringUtil.stringToInteger(persistentMileBean.getMilestoneEvent())<=0)){
					validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_Milestone_MileEvent_Mandatory));
				}
				if(persistentMileBean.getMilestoneType().equals(EVENT_MILESTONE) && (persistentMileBean.getMilestoneEventStatus()==null || StringUtil.stringToInteger(persistentMileBean.getMilestoneEventStatus())<=0)){
					validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_Milestone_EventStat_Mandatory));
				}
				if(persistentMileBean.getMilestoneVisitFK()==null || StringUtil.stringToInteger(persistentMileBean.getMilestoneVisitFK())<=0){
					validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_Milestone_MileVisit_Mandatory));
				}
			}


		if (validationIssues.size() > 0){
			((ResponseHolder) parameters.get("ResponseHolder"))
			.getIssues().addAll(validationIssues); 
			
			if (logger.isDebugEnabled()) logger.debug("Validation Issues found for MilestoneDetails");
			throw new ValidationException();
		}
		
		
	}

}
