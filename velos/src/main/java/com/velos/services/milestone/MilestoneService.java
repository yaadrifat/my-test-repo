package com.velos.services.milestone;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.Milestone;
import com.velos.services.model.MilestoneList;
import com.velos.services.model.StudyIdentifier;

@Remote
public interface MilestoneService {
	public static final String TABLE_NAME="er_milestone";
	public Milestone getStudyMilestones(StudyIdentifier studyIdent) throws OperationException;
	public ResponseHolder createMMilestones(MilestoneList milestone) throws OperationException;

}
