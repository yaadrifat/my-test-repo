/**
 * Created On Mar 31, 2011
 */
package com.velos.services.mbean;

import javax.management.MXBean;

import org.jboss.system.ServiceMBean;

/**
 * @author Kanwaldeep
 *
 */
@MXBean
public interface OutBoundAuditorMBean extends ServiceMBean {

}
