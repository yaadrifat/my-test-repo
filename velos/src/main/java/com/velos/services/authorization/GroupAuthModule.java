/**
 * 
 */
package com.velos.services.authorization;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.velos.eres.business.grpRights.impl.GrpRightsBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;

/**
 * Class for testing authorization information obtained from a user's group
 * membership.
 * 
 * Each instantiation of this module grabs a user's privilages for a certain set
 * of rights.
 * 
 * {@code 

		//Get the user's permission for viewing protocols
		GroupAuthModule groupAuth = 
			new GroupAuthModule(callingUser, groupRightsAgent);
		
		int manageProtocolPriv = 
			groupAuth.getAppManageProtocolsPrivileges();
					
		boolean hasViewManageProt = 
			GroupAuthModule.hasNewPermission(manageProtocolPriv);
 * }
 * 
 * Group Privileges can be set by membership in a Group. Group membership is at
 * a single level; a group cannot contain a group.
 * 
 * Group privileges are always set for a user based on the user's default group
 * (er_user.fk_grp_default). If a user belongs to multiple groups, authorization
 * is only checked based on the default group. Super User Groups
 * 
 * Each group can has a super-user designation. Within that group, the
 * privileges for super users can be defined. The list of privileges for super
 * users mirrors the privileges for groups. So, essentially, a super user is
 * like a role within a group. User's that are super users in the group inherit
 * the group's super user privileges. Super user privileges follow regular group
 * privileges in that they only apply to the use's default group.
 * 
 * @author dylan
 * 
 */
public class GroupAuthModule extends AbstractAuthModule {
	public static final String A2A= "A2A"; //A2A and Reverse A2A
	public static final String ACTFRMSACC= "ACTFRMSACC"; //Account Forms Access
	public static final String ADHOC= "ACTFRMSACC"; //Ad-hoc Queries
	public static final String ASSIGNUSERS= "ASSIGNUSERS"; //Assign Users to Group
	public static final String BUDGT= "BUDGT"; //Budget
	public static final String BUDTEMPL= "BUDTEMPL"; //Save Template
	public static final String CALLIB= "CALLIB"; //Calendar Library
	public static final String CGRP= "CGRP"; //Manage Groups
	public static final String DASH= "DASH"; //Dashboard
	public static final String DSM= "DSM"; //Data Safety Monitoring
	public static final String EVLIB= "EVLIB"; //Event Library
	public static final String MACCFRMS= "MACCFRMS"; //Account Form Management
	public static final String MACCLINKS= "MACCLINKS"; //Manage Account Links
	public static final String MFLDLIB= "MFLDLIB"; //Field Library
	public static final String MFRMLIB= "MFRMLIB"; //Form Library
	public static final String MGRPRIGHTS= "MGRPRIGHTS"; //Manage Group Rights
	public static final String MILEST= "MILEST"; //Milestones
	public static final String MIRB_APP= "MIRB_APP"; //Manage Applications
	public static final String MIRB_AR= "MIRB_AR"; //&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Action Required
	public static final String MIRB_AT= "MIRB_AT"; //Admin Tools
	public static final String MIRB_MM= "MIRB_MM"; //Manage Meetings
	public static final String MIRB_MS= "MIRB_MS"; //Monitor Studies
	public static final String MIRB_ON= "MIRB_ON"; //Ongoing Studies
	public static final String MIRB_OUT= "MIRB_OUT"; //Final Outcome
	public static final String MIRB_PR= "MIRB_PR"; //Items Pending Review/ Approval
	public static final String MIRB_PREV= "MIRB_PREV"; //Pending Review
	public static final String MIRB_RACR= "MIRB_RACR"; //Ancillary Committee Reviews
	public static final String MIRB_RFR= "MIRB_RFR"; //Full Committee Reviews
	public static final String MIRB_RPR= "MIRB_RPR"; //Pending Reviews
	public static final String MIRB_RXER= "MIRB_RXER"; //Exempt Reviews
	public static final String MIRB_RXPR= "MIRB_RXPR"; //Expedited Reviews
	public static final String MIRB_SAS= "MIRB_SAS"; //Pre-review
	public static final String MIRB_SCS= "MIRB_SCS"; //Review Assignment
	public static final String MIRB_SNS= "MIRB_SNS"; //New Submissions
	public static final String MIRB_SPI= "MIRB_SPI"; //Pending PI Response
	public static final String MIRB_SPRS= "MIRB_SPRS"; //Post Review Processing
	public static final String MIRB_SSUM= "MIRB_SSUM"; //Activity Summary
	public static final String MPATIENTS= "MPATIENTS"; //Manage Patients
	public static final String MSITES= "MSITES"; //Manage Organizations
	public static final String MSPEC= "MSPEC"; //Manage Specimens
	public static final String MSTORAGE= "MSTORAGE"; //Manage Storages
	public static final String MUSERS= "MUSERS"; //Manage Users
	public static final String NPROTOCOL= "NPROTOCOL"; //Manage Protocols
	public static final String PATFRMSACC= "PATFRMSACC"; //All Patient Forms Access
	public static final String PATLABS= "PATLABS"; //Patient Labs
	public static final String PORTALAD= "PORTALAD"; //Portal Admin
	public static final String REPORTS= "REPORTS"; //Reports
	public static final String STUDYFRMSACC= "STUDYFRMSACC"; //All Study Forms Access
	public static final String VIEWPATCOM= "VIEWPATCOM"; //View Complete Patient Data

	private GrpRightsBean groupRights;

	private Hashtable<String, Integer> rightsTable;
	
	public GroupAuthModule(UserBean user, GrpRightsAgentRObj groupRightsAgent) {

		// populate the private group rights bean
		groupRights = 
			groupRightsAgent.getGrpRightsDetails(
					Integer.valueOf(user.getUserGrpDefault()));
		//get a list of the values (mprotocol, etc.)
		ArrayList values = groupRights.getGrValue();
		//get a list of feature rights
		ArrayList featureRights = groupRights.getFtrRights();
		
		rightsTable = new Hashtable<String, Integer>();
		for (int x = 0; x < values.size(); x++){
			String key = (String)values.get(x);
			
			//build up the table, filtering out headers, that represent display
			if (!key.contains(HEADER_PREFIX)){
				Integer value = Integer.valueOf((String)featureRights.get(x));
				rightsTable.put(key, value);
			}
		}
		
	}

	/**
	 * Returns the bitflag containing the user's manage organiztations
	 * privileges.
	 * 
	 * @return
	 */
	public Integer  getAdminManageOrganizationsPrivileges() {
		return getRights(MSITES);
	}

	/**
	 * Returns the bitflag containing the user's manage groups privileges.
	 * 
	 * @return
	 */
	public Integer  getAdminManageGroupsPrivileges() {
		return getRights(CGRP);
	}

	/**
	 * Returns the bitflag containing the user's assign users to group
	 * privileges.
	 * 
	 * @return
	 */
	public Integer  getAdminAssignUsersToGroupsPrivileges() {
		return getRights(ASSIGNUSERS);
	}

	/**
	 * Returns the bitflag containing the user's manage group rights privileges.
	 * 
	 * @return
	 */
	public Integer  getAdminManageGroupRightsPrivileges() {
		return getRights(MGRPRIGHTS);
	}

	/**
	 * Returns the bitflag containing the user's manage users privileges.
	 * 
	 * @return
	 */
	public Integer  getAdminManageUsersPrivileges() {
		return getRights(MUSERS);
	}

	/**
	 * Returns the bitflag containing the user's manage account links
	 * privileges.
	 * 
	 * @return
	 */
	public Integer  getAdminManageAccountLinksPrivileges() {
		return getRights(MACCLINKS);
	}

	/**
	 * Returns the bitflag containing the user's portal admin privileges.
	 * 
	 * @return
	 */
	public Integer  getAdminPortalAdminPrivileges() {
		return getRights(PORTALAD);
	}

	/**
	 * Returns the bitflag containing the user's manage protocols privileges.
	 * 
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Integer  getAppManageProtocolsPrivileges() {
		return getRights(NPROTOCOL);
	}

	/**
	 * Returns the bitflag containing the user's calendar library privileges.
	 * 
	 * @return
	 */
	public Integer  getAppCalendarLibraryPrivileges() {
		return getRights(CALLIB);
	}

	/**
	 * Returns the bitflag containing the user's event library privileges.
	 * 
	 * @return
	 */
	public Integer  getAppEventLibraryPrivileges() {
		return getRights(EVLIB);
	}

	/**
	 * Returns the bitflag containing the user's manage patients privileges.
	 * 
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Integer  getAppManagePatientsPrivileges() {
		return getRights(MPATIENTS);
	}

	/**
	 * Returns the bitflag containing the user's view complete patient data
	 * privileges.
	 * 
	 * @return
	 */
	public Integer  getAppViewCompletePatientDataPrivileges() {
		return getRights(VIEWPATCOM);
	}

	/**
	 * Returns the bitflag containing the user's budget privileges.
	 * 
	 * @return
	 */
	public Integer  getAppBudgetPrivileges() {
		return getRights(BUDGT);
	}

	/**
	 * Returns the bitflag containing the user's save template privileges.
	 * 
	 * @return
	 */
	public Integer  getAppSaveTemplatePrivileges() {
		return getRights(BUDTEMPL);
	}

	/**
	 * Returns the bitflag containing the user's milesontes privileges.
	 * 
	 * @return
	 */
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Integer  getAppMilestonesPrivileges() {
		return getRights(MILEST);
	}

	/**
	 * Returns the bitflag containing the user's reports privileges.
	 * 
	 * @return
	 */
	public Integer  getAppReportsPrivileges() {
		return getRights(REPORTS);
	}

	/**
	 * Returns the bitflag containing the user's data safety monitoring
	 * privileges.
	 * 
	 * @return
	 */
	public Integer  getAppDataSafetyMonitoringPrivileges() {
		return getRights(DSM);
	}

	/**
	 * Returns the bitflag containing the user's ad-hoc query privileges.
	 * 
	 * @return
	 */
	public Integer  getAppAdHocQueryPrivileges() {
		return getRights(ADHOC);
	}

	/**
	 * Returns the bitflag containing the user's dashboard privileges.
	 * 
	 * @return
	 */
	public Integer  getAppDashboardPrivileges() {
		return getRights(DASH);
	}

	/**
	 * Returns the bitflag containing the user's patient labs privileges.
	 * 
	 * @return
	 */
	public Integer  getAppPatientLabsPrivileges() {
		return getRights(PATLABS);
	}

	/**
	 * Returns the bitflag containing the user's form library privileges.
	 * 
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Integer  getFormsFormLibraryPrivileges() {
		return getRights(MFRMLIB);
	}

	/**
	 * Returns the bitflag containing the user's field library privileges.
	 * 
	 * @return
	 */
	public Integer  getFormsFieldLibraryPrivileges() {
		return getRights(MFLDLIB);
	}

	/**
	 * Returns the bitflag containing the user's account form management
	 * privileges.
	 * 
	 * @return
	 */
	public Integer  getFormsAccountFormManagementPrivileges() {
		return getRights(MACCFRMS);
	}

	/**
	 * Returns the bitflag containing the user's account form access privileges.
	 * This controls whether the user can create form responses.
	 * 
	 * @return
	 */
	public Integer  getFormsAccountFormAccessPrivileges() {
		return getRights(ACTFRMSACC);
	}

	/**
	 * Returns the bitflag containing the user's all sutyd forms privileges.
	 * 
	 * @return
	 */
	public Integer  getFormsAllStudyFormsPrivileges() {
		return getRights(STUDYFRMSACC);
	}

	/**
	 * Returns the bitflag containing the user's all patient forms privileges.
	 * 
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Integer  getFormsAllPatientFormsPrivileges() {
		return getRights(PATFRMSACC);
	}

	/**
	 * Returns the bitflag containing the user's manage storage privileges.
	 * 
	 * @return
	 */
	public Integer  getInventoryManageStoragePrivileges() {
		return getRights(MSTORAGE);
	}

	/**
	 * Returns the bitflag containing the user's manage specimens privileges.
	 * 
	 * @return
	 */
	public Integer  getInventoryManageSpecimensPrivileges() {
		return getRights(MSPEC);
	}

	/**
	 * Returns the bitflag containing the user's manage research compliance 
	 * applications privileges.
	 * 
	 * @return
	 */
	public Integer  getResCompManageApplicationsPrivileges() {
		return getRights(MIRB_APP);
	}

	/**
	 * Returns the bitflag containing the user's action requiredprivileges.
	 * 
	 * @return
	 */
	public Integer  getResCompActionRequiredPrivileges() {
		return getRights(MIRB_AR);
	}

	/**
	 * Returns the bitflag containing the user's items pending privileges.
	 * 
	 * @return
	 */
	public Integer  getResCompItemsPendingPrivileges() {
		return getRights(MIRB_PR);
	}

	/**
	 * Returns the bitflag containing the user's ongoing studies privileges.
	 * 
	 * @return
	 */
	public Integer  getResCompOngoingStudiesPrivileges() {
		return getRights(MIRB_ON);
	}

	/**
	 * Returns the bitflag containing the user's activity summary privileges.
	 * 
	 * @return
	 */
	public Integer  getSumbissionsActivitySummaryPrivileges() {
		return getRights(MIRB_SSUM);
	}

	/**
	 * Returns the bitflag containing the user's new submissions privileges.
	 * 
	 * @return
	 */
	public Integer  getSumbissionsNewSubmissionsPrivileges() {
		return getRights(MIRB_SNS);
	}

	/**
	 * Returns the bitflag containing the user's pre-review privileges.
	 * 
	 * @return
	 */
	public Integer  getSumbissionsPreReviewPrivileges() {
		return getRights(MIRB_SAS);
	}

	/**
	 * Returns the bitflag containing the user's review assignment privileges.
	 * 
	 * @return
	 */
	public Integer  getSumbissionsReviewAssignmentPrivileges() {
		return getRights(MIRB_SCS);
	}

	/**
	 * Returns the bitflag containing the user's pending review privileges.
	 * 
	 * @return
	 */
	public Integer  getSumbissionsPendingReviewPrivileges() {
		return getRights(MIRB_PREV);
	}

	/**
	 * Returns the bitflag containing the user's post review processing
	 * privileges.
	 * 
	 * @return
	 */
	public Integer  getReviewerPostReviewProcessingPrivileges() {
		return getRights(MIRB_SPRS);
	}

	/**
	 * Returns the bitflag containing the user's final outcome privileges.
	 * 
	 * @return
	 */
	public Integer  getReviewerFinalOutcomePrivileges() {
		return getRights(MIRB_OUT);
	}

	/**
	 * Returns the bitflag containing the user's pending please response
	 * privileges.
	 * 
	 * @return
	 */
	public Integer  getReviewerPendingPlsRespondPrivileges() {
		return getRights(MIRB_SPI);
	}

	/**
	 * Returns the bitflag containing the user's reviewe pending reviews
	 * privileges.
	 * 
	 * @return
	 */
	public Integer  getReviewerPendingReviewsPrivileges() {
		return getRights(MIRB_RPR);
	}

	/**
	 * Returns the bitflag containing the user's full committee reviews privileges.
	 * 
	 * @return
	 */
	public Integer  getReviewerFullCommitteeReviewsPrivileges() {
		return getRights(MIRB_RFR);
	}
	
	/**
	 * Returns the bitflag containing the user's expedited reviews privileges.	
	 * @return
	 */
	public Integer  getReviewerExpeditedReviewsPrivileges(){
		return getRights(MIRB_RXPR);
	}
	
	/**
	 * Returns the bitflag containing the user's exempt reviews privileges.	
	 * @return
	 */
	public Integer  getReviewerExemptReviewsPrivileges(){
		return getRights(MIRB_RXER);
	}
	
	/**
	 * Returns the bitflag containing the user's ancillary committee reviews privileges.	
	 * @return
	 */
	public Integer  getReviewerAncillaryCommitteeReviewsPrivileges(){
		return getRights(MIRB_RACR);
	}
	
	/**
	 * Returns the bitflag containing the user's manage meetings privileges.	
	 * @return
	 */
	public Integer  getReviewerManageMeetingsPrivileges(){
		return getRights(MIRB_MM);
	}
	
	/**
	 * Returns the bitflag containing the user's monitor studies privileges.	
	 * @return
	 */
	public Integer  getReviewerMonitorStudiesPrivileges(){
		return getRights(MIRB_MS);
	}
	
	/**
	 * Returns the bitflag containing the user's admin tools privileges.	
	 * @return
	 */
	public Integer  getReviewerAdminToolsPrivileges(){
		return getRights(MIRB_AT);
	}
	
	private Integer getRights(String key){
		if (rightsTable.containsKey(key)){
			return rightsTable.get(key);
		}
		return 0;
	}
}
