/**
 * Created On Nov 5, 2012
 */
package com.velos.services.systemadmin;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.model.CodeTypes;
import com.velos.services.model.Codes;
import com.velos.services.model.ObjectMaps;
import com.velos.services.model.ObjectInfos;

/**
 * @author Kanwaldeep
 *
 */
@Remote
public interface CodeListService {
	
	public Codes getCodeList(String codeListType) throws OperationException;
	
	public CodeTypes getCodeTypes() throws OperationException;

	/**
	 * @param tableInfos
	 * @return
	 */
	public ObjectMaps getOrCreateObjectMap(ObjectInfos tableInfos) throws OperationException; 
}
