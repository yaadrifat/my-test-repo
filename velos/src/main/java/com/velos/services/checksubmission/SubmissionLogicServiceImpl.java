package com.velos.services.checksubmission;

import java.util.ArrayList;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.EIRBDao;
import com.velos.eres.business.common.StudyDao;
import com.velos.eres.gems.business.WorkflowJB;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.ReviewBoard;
import com.velos.services.model.ReviewBoards;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.SubmissionCondition;
import com.velos.services.model.SubmissionConditions;
import com.velos.services.util.ObjectLocator;

@Stateless
@Remote(SubmissionLogicService.class)
public class SubmissionLogicServiceImpl extends AbstractService implements SubmissionLogicService{
	private static Logger logger = Logger.getLogger(SubmissionLogicServiceImpl.class.getName());
	@EJB
	private ObjectMapService objectMapService;
	@EJB
	private UserAgentRObj userAgent;
	@EJB
	GrpRightsAgentRObj groupRightsAgent;
	@Resource 
	private SessionContext sessionContext;
	ArrayList boardNameList = null;
	ArrayList boardIdList = null;
	EIRBDao eIrbDao = new EIRBDao();
	ReviewBoard reviewBoard = new ReviewBoard();
	SubmissionConditions submissionConditions;
	SubmissionCondition submissionCondition;
	ReviewBoards csResponse = new ReviewBoards();
	ArrayList<ReviewBoard> reviewBoardList = new ArrayList<ReviewBoard>();
	@Override
	public ReviewBoards getStudyCheckAndSubmitStatusResponse(StudyIdentifier studyIdentifier)
			throws OperationException {
		try{
			int entityId = 0;
			if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
					&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
					&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
			{
				csResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to calculate submission logic"));//fix for 28746 bug
			throw new OperationException();
			}
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);

			Integer manageProtocolPriv = 
				groupAuth.getAppManageProtocolsPrivileges();
			System.out.println("manageProtocolPriv:::--->"+manageProtocolPriv);
			boolean hasViewManageProt = 
				GroupAuthModule.hasViewPermission(manageProtocolPriv);
			System.out.println("hasNewManageProt:::--->"+hasViewManageProt);
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasViewManageProt){
				csResponse.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view studies"));
				throw new AuthorizationException("User Not Authorized to edit studies");
			}
			Integer entityIdObj=locateStudyPKLcl(studyIdentifier);
			
			if(entityIdObj == null || entityIdObj.intValue()==0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdentifier.getOID() + " StudyNumber " + studyIdentifier.getStudyNumber()));
				throw new OperationException();
			}else{
				entityId=entityIdObj.intValue();
			}
			TeamAuthModule teamAuth = 
					new TeamAuthModule(callingUser.getUserId(), entityId);
				
			    int studySummaryAuth = teamAuth.getStudySummaryPrivileges();
				boolean hasViewStudySummaryPermission = 
					TeamAuthModule.hasViewPermission(studySummaryAuth);

			if (!hasViewStudySummaryPermission){
				csResponse.addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User Not Authorized to view studies"));
				throw new AuthorizationException("User Not Authorized to edit studies");
			}
			String grpId = callingUser.getUserGrpDefault();
			String accountId = callingUser.getUserAccountId();
			eIrbDao.getReviewBoards(EJBUtil.stringToNum(accountId),EJBUtil.stringToNum(grpId));
			boardNameList = eIrbDao.getBoardNameList();
		    boardIdList = eIrbDao.getBoardIdList();
		    System.out.println("Total Boards:::"+boardNameList.size());
		    for (int iY=0; iY<boardIdList.size(); iY++) {
		    	
		    	int reviewBoardComplete =0;
		    	reviewBoard = new ReviewBoard();
		    	reviewBoard.setName(boardNameList.get(iY).toString());
		    	System.out.println("Board Name::::"+boardNameList.get(iY).toString());
		    	EIRBDao eIrbDaoLogic = new EIRBDao();
		    	eIrbDaoLogic.checkSubmission(entityId, EJBUtil
		                .stringToNum(boardIdList.get(iY).toString()));
		        ArrayList resultFlags = eIrbDaoLogic.getResultFlags();
		        ArrayList resultTexts = eIrbDaoLogic.getResultTexts();
		        ArrayList testTexts = eIrbDaoLogic.getTestTexts();
		        if(resultFlags.size()>0){
		        	submissionConditions = new SubmissionConditions();
		        	ArrayList<SubmissionCondition> tmpSubmCondList = new ArrayList<SubmissionCondition>();
		        	int isApplicationComplete = -2;
		            boolean thereIsAPass = false;
		            for(int iResult=0; iResult<resultFlags.size(); iResult++) {
		            		            	
		                if ( ((Integer)resultFlags.get(iResult)).intValue() == -1 ) {
		                    isApplicationComplete = -1;
		                } else if ( ((Integer)resultFlags.get(iResult)).intValue() == 0 ) {
		                    thereIsAPass = true;
		                }
		                System.out.println(resultTexts.get(iResult)+"::::"+isApplicationComplete+"::::"+thereIsAPass);
		                System.out.println("testTexts.get(iResult)----->"+testTexts.get(iResult).toString());
		                submissionCondition = new SubmissionCondition();
		            	submissionCondition.setConditionName(testTexts.get(iResult).toString());
		            	if(EJBUtil.stringToNum(resultFlags.get(iResult).toString())==0){
		            		submissionCondition.setConditionMeets(1);
		            	}else{
		            		submissionCondition.setConditionMeets(0);
		            	}
		            	submissionCondition.setConditionResult(resultTexts.get(iResult).toString());
		            	tmpSubmCondList.add(submissionCondition);
		            }
		            
		            if (isApplicationComplete == -2 && thereIsAPass) {
		                isApplicationComplete = 0;
		            }
		            
		            if(isApplicationComplete==0){
		            	reviewBoardComplete=1;
		            }else{
		            	reviewBoardComplete=0;
		            }
		            submissionConditions.setSubmissionCondition(tmpSubmCondList);
		            reviewBoard.setSubmissionConditions(submissionConditions); 
		        }
		       
		        System.out.println("reviewBoardComplete---->"+reviewBoardComplete);
		        reviewBoard.setSubmissionReady(reviewBoardComplete);
		        CodeDao submittedCode = new CodeDao();
		        System.out.println("Before Calling");
		        String submissionDate = eIrbDaoLogic.getRecentSubmissionDate(entityId,EJBUtil.stringToNum(boardIdList.get(iY).toString()),submittedCode.getCodeId("subm_status", "submitted"));
		        
		        System.out.println("After Calling");
		        //if(!submissionDate.equals("")){  //for 28856 bug
		        	reviewBoard.setLastSubmittedOn(submissionDate);	
		       // }
		        System.out.println("reviewBoardComplete---->"+submissionDate);
		        reviewBoardList.add(reviewBoard);
		        System.out.println("reviewBoardList size--->"+reviewBoardList.size());
		        
		    }
		    System.out.println("Outer reviewBoardList size--->"+reviewBoardList.size());
		    csResponse.setReviewBoard(reviewBoardList);
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("SubmissionLogicServiceImpl getStudyCheckAndSubmitStatusResponse", e);
		}catch(Exception e){
			e.printStackTrace();
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("SubmissionLogicServiceImpl getStudyCheckAndSubmitStatusResponse", t);

		}
		return csResponse;
	}
		private Integer locateStudyPKLcl(StudyIdentifier studyIdentifier) 
				throws OperationException{
					//Virendra:#6123,added OID in if clause
					if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
							&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
							&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
					{
						addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to addStudyPatientStatus"));
						throw new OperationException();
					}
					Integer studyPK =
							ObjectLocator.studyPKFromIdentifier(this.callingUser, studyIdentifier,objectMapService);
					if (studyPK == null || studyPK.intValue() == 0){
						StringBuffer errorMessage = new StringBuffer("Study not found for:"); 
						if(studyIdentifier.getOID() != null && studyIdentifier.getOID().length() > 0 )
						{
							errorMessage.append(" OID: " + studyIdentifier.getOID()); 
						}
						if(studyIdentifier.getStudyNumber() != null && studyIdentifier.getStudyNumber().length() > 0)
						{
							errorMessage.append( " Study Number: " + studyIdentifier.getStudyNumber()); 
						}
						if(studyIdentifier.getPK()!=null){
							errorMessage.append( " PK: " + studyIdentifier.getPK()); 
						}
						csResponse.addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, errorMessage.toString()));
						throw new OperationException();
					}
					return studyPK;
				}
		@AroundInvoke
		public Object myInterceptor(InvocationContext ctx) throws Exception {
			csResponse = new ReviewBoards();
			
			reviewBoardList = new ArrayList<ReviewBoard>();
			response = new ResponseHolder();
			callingUser = 
				getLoggedInUser(
						sessionContext,
						userAgent);
			return ctx.proceed();

		}
	
}
