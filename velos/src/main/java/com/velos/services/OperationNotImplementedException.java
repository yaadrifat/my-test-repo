/**
 * 
 */
package com.velos.services;

/**
 * @author dylan
 *
 */
public class OperationNotImplementedException extends OperationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3747078187423827228L;

	public OperationNotImplementedException() {
		super();
	}

	public OperationNotImplementedException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public OperationNotImplementedException(String arg0) {
		super(arg0);
	}

	public OperationNotImplementedException(Throwable arg0) {
		super(arg0);
	}

	

}
