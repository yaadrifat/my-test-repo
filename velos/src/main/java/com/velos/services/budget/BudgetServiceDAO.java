package com.velos.services.budget;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.SessionContext;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.xml.sax.SAXException;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.TeamDao;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.esch.business.bgtSection.impl.BgtSectionBean;
import com.velos.esch.business.budget.impl.BudgetBean;
import com.velos.esch.business.budgetcal.impl.BudgetcalBean;
import com.velos.esch.business.common.BudgetUsersDao;
import com.velos.esch.business.lineitem.impl.LineitemBean;
import com.velos.esch.web.eventassoc.EventAssocJB;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.ValidationException;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMapService;
import com.velos.services.milestone.MilestoneServiceDao;
import com.velos.services.model.BgtCalPojo;
import com.velos.services.model.BgtLiniItemDetail;
import com.velos.services.model.BgtSectionDetail;
import com.velos.services.model.BgtSectionNameIdentifier;
import com.velos.services.model.BgtTemplate;
import com.velos.services.model.BudgetCalIdentifier;
import com.velos.services.model.BudgetCalList;
import com.velos.services.model.BudgetDetail;
import com.velos.services.model.BudgetIdentifier;
import com.velos.services.model.BudgetLineItemPojo;
import com.velos.services.model.BudgetPojo;
import com.velos.services.model.BudgetSectionPojo;
import com.velos.services.model.CalendarNameIdentifier;
import com.velos.services.model.EventNameIdentfier;
import com.velos.services.model.LineItemNameIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.VisitNameIdentifier;
import com.velos.services.util.HibernateUtil;

public class BudgetServiceDAO extends CommonDAO{
	
	/**
	 * 
	 */
	private static Logger logger = Logger.getLogger(BudgetServiceDAO.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//By Virendra for budget status
	/**
	 * 
	 * @param budgetName
	 * @return
	 */
	public Integer locateBudgetPK(String budgetName, String versionNumber, String accountNumber) throws MultipleObjectsFoundException{
		Integer budgetPK = 0;
		Connection conn = null; 
		PreparedStatement stmt = null ; 
		ResultSet rs = null; 
		
		StringBuffer sql = new StringBuffer(); 
		sql.append("select pk_budget from sch_budget where budget_name = ? ");  
		sql.append(" and FK_ACCOUNT = ?"); 
		if(versionNumber != null && versionNumber.length() > 0){
			sql.append(" and  BUDGET_VERSION = ?"); 
		}else
		{
			sql.append(" and BUDGET_VERSION is null"); 
		}
		
		sql.append(" and (BUDGET_DELFLAG is null or BUDGET_DELFLAG = 'N')"); 
				
		
		try
		{
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql.toString()); 
			stmt.setString(1, budgetName); 
			stmt.setString(2, accountNumber); 
			if(versionNumber != null && versionNumber.length() > 0) stmt.setString(3, versionNumber);
						
			rs = stmt.executeQuery(); 
			
			int ct = 0; 
			while(rs.next())
			{
				budgetPK = rs.getInt(1); 
				ct++; 
			}
			
			if(ct > 1) throw new MultipleObjectsFoundException("Found multiple Objects for BudgetName "+ budgetName + " and BudgetVersion "+ versionNumber);
					
		
		}catch(SQLException sqe)
		{
			// TODO Auto-generated catch block
			sqe.printStackTrace();
		}finally
		{
			closeResultSet(rs); 
			closeStatement(stmt); 
			returnConnection(conn); 
		}
		return budgetPK;
	}
	
	// Check these rights after checking Group rights for non-combined budgets and study team rights for combined Budgets . 
	public String getAccessRightsForBudget(BudgetBean budgetBean, UserBean user)
	{
		String budgetRights = "000"; 		
		PreparedStatement stmt = null; 
		Connection conn = null ; 
		ResultSet rs = null; 
		
		try
		{
			String sql = "select grp_SUPBUD_rights from er_grps where pk_grp=? and grp_supbud_flag = 1" ;
			
			conn = getConnection(); 

			stmt = conn.prepareStatement(sql); 

			stmt.setString(1, user.getUserGrpDefault()); 		

			rs = stmt.executeQuery();

			if(rs.next())
			{				
				budgetRights = rs.getString(1);				
			}else
			{
				if(budgetBean.getBudgetCombinedFlag() == null)
				{
					String scope = budgetBean.getBudgetRScope(); 
					if(!StringUtil.isEmpty(scope))
					{
						if(scope.equalsIgnoreCase("A"))
						{
							if(budgetBean.getBudgetAccountId().equalsIgnoreCase(user.getUserAccountId()))
							{
								budgetRights = budgetBean.getBudgetRights();								
							}
						}else if(scope.equalsIgnoreCase("O"))
						{
							if(budgetBean.getBudgetSiteId().equalsIgnoreCase(user.getUserSiteId()))
							{
								budgetRights = budgetBean.getBudgetRights(); 							
							}
						}else if(scope.equalsIgnoreCase("S"))
						{
							// check if user is in studyTeam. 
							String studyID = budgetBean.getBudgetStudyId(); 
							TeamDao teamDao = new TeamDao(); 
							teamDao.findUserInTeam(EJBUtil.stringToNum(budgetBean.getBudgetStudyId()),user.getUserId());
							if (teamDao.getCRows() > 0) {
								budgetRights = budgetBean.getBudgetRights(); 							
							}else
							{
								teamDao.getSuperUserTeam(EJBUtil.stringToNum(budgetBean.getBudgetStudyId())); 
								if(teamDao.getUserIds().contains(user.getUserId()))
								{
									
									budgetRights = budgetBean.getBudgetRights(); 									
								}
							}

						}
					}	
				}
			}
			//individual user access right overrides grpRight and scopeRight - not applicable to combined Budgets
			if(budgetBean.getBudgetCombinedFlag() == null)
			{
				BudgetUsersDao bgtUsersDao = new BudgetUsersDao();	            
	            String userRightStr = bgtUsersDao.getBudgetUserRight(budgetBean.getBudgetId(), user.getUserId());
			    if (!StringUtil.isEmpty(userRightStr)) {
			    	budgetRights = userRightStr; 			    
			    }	           
			}			
		}catch(SQLException sqe)
		{
			
		}finally
		{
			closeResultSet(rs); 
			closeStatement(stmt); 
			returnConnection(conn); 
		}		
		return budgetRights;
	}
	
	private void closeStatement(PreparedStatement stmt)
	{
		if(stmt != null)
		{
			try{
				stmt.close(); 
			}catch(SQLException sqe)
			{
				
			}
		}
	}
	
	private void closeResultSet(ResultSet rs)
	{
		if(rs != null)
		{
			try
			{
				rs.close(); 
			}catch(SQLException sqe)
			{
				
			}
		}
		
	}
	public static BudgetDetail getStudyBudgets(Integer studyId, Map<String, Object> parameters) throws IOException, SAXException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, OperationException{
		/*ResponseHolder responseHolder = (ResponseHolder) parameters.get("ResponseHolder");
		BudgetBean schBgtPojo = new BudgetBean();
		List<BudgetcalBean> schBudgetCalList = new ArrayList<BudgetcalBean>();
		List<BgtSectionBean> schBudgetSectionList = new ArrayList<BgtSectionBean>();
		BudgetCalDetail bgtCalDetail=null ;
		List<LineitemBean> schLineItemList = new ArrayList<LineitemBean>();
		BgtSectionDetail bgtSectionDetail=null ;
		List<BudgetCalDetail> bgtCalList = null;
		List<BgtSectionDetail> bgtSectionList = null;
		BudgetDetail budgetDetailPojo = new BudgetDetail();
		Session session = HibernateUtil.getEschConnection().openSession();
		Criteria criteria = session.createCriteria(BudgetBean.class);
		criteria.add(Restrictions.eq("FK_STUDY", studyId)).add(Restrictions.eq("BUDGET_COMBFLAG", "Y"));
		schBgtPojo=(BudgetBean) criteria.uniqueResult();
		try{
		if(schBgtPojo == null){
			responseHolder.addIssue(
					new Issue(
							IssueTypes.BUDGET_NOT_FOUND ,
					"Budget not found"));
			throw new OperationException();
		}}
		catch(OperationException e){
			e.setIssues(responseHolder.getIssues());
			throw e;
		}
		budgetDetailPojo.setBudgetInfo(schBgtPojo);
		criteria = session.createCriteria(BudgetcalBean.class);
		criteria.add(Restrictions.eq("FK_BUDGET", schBgtPojo.getPK_BUDGET())).addOrder(Order.asc("PK_BGTCAL"));
		schBudgetCalList=criteria.list();
		if(schBudgetCalList!=null && schBudgetCalList.size()>0){
		bgtCalList=new ArrayList<BudgetCalDetail>();
		for (BudgetcalBean schBudgetCalPojo : schBudgetCalList) {
			criteria = session.createCriteria(BgtSectionBean.class);
			criteria.add(Restrictions.eq("FK_BGTCAL", schBudgetCalPojo.getPK_BGTCAL())).addOrder(Order.asc("PK_BUDGETSEC"));
			schBudgetSectionList=criteria.list();
			bgtCalDetail = new BudgetCalDetail();
			bgtCalDetail.setBudgetCalendarInfo(schBudgetCalPojo);
			if(schBudgetSectionList!=null && schBudgetSectionList.size()>0){
			bgtSectionList = new ArrayList<BgtSectionDetail>();
			for (BgtSectionBean schBudgetSectionPojo : schBudgetSectionList) {
				criteria = session.createCriteria(LineitemBean.class);
				criteria.add(Restrictions.eq("FK_BGTSECTION", schBudgetSectionPojo.getPK_BUDGETSEC())).addOrder(Order.asc("PK_LINEITEM"));
				schLineItemList=criteria.list();
				bgtSectionDetail = new BgtSectionDetail();
				bgtSectionDetail.setBudgetSectionInfo(schBudgetSectionPojo);
				bgtSectionDetail.setLineItemInfo(schLineItemList);
				bgtSectionList.add(bgtSectionDetail);
					}
			}
			bgtCalDetail.setBudgetSection(bgtSectionList);
			bgtCalList.add(bgtCalDetail);
			}
		}
		budgetDetailPojo.setBudgetCalendar(bgtCalList);
		HibernateUtil.closeSession(session);*/
		//return budgetDetailPojo;
		return new BudgetDetail();

	}
	public String saveBudgets(BudgetDetail budgetDetail, Map<String, Object> parameters) throws HibernateException, IOException, SAXException, OperationException{
		String retStrings = new String();
		/*Integer bgtId = 0;
		Integer bgtCalId=0;
		Integer bgtSecId=0;
		Integer budCount=1;
		Integer budCalCount=1;
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("objectMapService");
		UserAgentRObj userAgent = (UserAgentRObj) parameters.get("userAgent"); 
		SessionContext sessionContext  = (SessionContext) parameters.get("sessionContext"); 
		ResponseHolder responseHolder = (ResponseHolder) parameters.get("ResponseHolder");
		UserBean callingUser = (UserBean) parameters.get("callingUser");
		Session session = (Session) HibernateUtil.getEschConnection().openSession();
		BudgetcalBean sCHBudgetCalPojo=null;
		BudgetBean schBudget = new BudgetBean();
		BgtSectionBean schBudgetSectionPojo = null;
		List<BudgetCalDetail> bgtCalList = new ArrayList<BudgetCalDetail>();
		
		
		BudgetCalDetail bgtCalDetail=null;
		List<LineitemBean> schLineItemList = new ArrayList<LineitemBean>();
		BgtSectionDetail bgtSectionDetail=null;
		LineitemBean bgtLineItemPojo=null;
		List<BgtSectionDetail> bgtSectionList = new ArrayList<BgtSectionDetail>();
		
		schBudget = budgetDetail.getBudgetInfo();
		bgtCalList=budgetDetail.getBudgetCalendar();

		try{
			if(schBudget.getFK_STUDY()==null || schBudget.getFK_STUDY()==0){
				responseHolder.addIssue(
						new Issue(
								IssueTypes.DATA_VALIDATION ,
						"StudiId is a mandatory field."));
				throw new OperationException();
				
			}
			Transaction tx = session.beginTransaction();
			try{
				BudgetHandler.validateBudgetDetails(schBudget, callingUser, parameters); 
			}
			catch(ValidationException e)
			{
				System.out.println(((ResponseHolder) parameters.get("ResponseHolder"))
						.getIssues().getIssue().get(0));
				throw new OperationException();
			}
			if(bgtCalList==null || bgtCalList.size()==0){
				responseHolder.addIssue(
						new Issue(
								IssueTypes.DATA_VALIDATION ,
						"Budget Must Contains Calendar."));
				throw new OperationException();
				
			}
			if(bgtCalList!=null && bgtCalList.size()>0){
			Iterator<BudgetCalDetail> bgtCalITR=bgtCalList.iterator();
			
			while(bgtCalITR.hasNext()){
				sCHBudgetCalPojo = new BudgetcalBean();
				bgtCalDetail = new BudgetCalDetail();
				bgtCalDetail = (BudgetCalDetail)bgtCalITR.next();
				sCHBudgetCalPojo = (BudgetcalBean)bgtCalDetail.getBudgetCalendarInfo();
				if(sCHBudgetCalPojo==null){
					responseHolder.addIssue(
							new Issue(
									IssueTypes.DATA_VALIDATION ,
							"Budget Must Contains Calendar."));
					throw new OperationException();
					
				}
				
				bgtSectionList = bgtCalDetail.getBudgetSection();
				if(bgtSectionList==null || bgtSectionList.size()==0){
					responseHolder.addIssue(
							new Issue(
									IssueTypes.DATA_VALIDATION ,
							"Budget Must Contains Budget Section."));
					throw new OperationException();
					
				}
				budCalCount=1;
				if(bgtSectionList!=null && bgtSectionList.size()>0){
				Iterator<BgtSectionDetail> bgtSecITR = bgtSectionList.iterator();
				while(bgtSecITR.hasNext()){
					schBudgetSectionPojo=new BgtSectionBean();
					bgtSectionDetail = new BgtSectionDetail();
					bgtSectionDetail = (BgtSectionDetail)bgtSecITR.next();
					schBudgetSectionPojo = (BgtSectionBean)bgtSectionDetail.getBudgetSectionInfo();
					if(schBudgetSectionPojo==null){
						responseHolder.addIssue(
								new Issue(
										IssueTypes.DATA_VALIDATION ,
								"Budget Must Contains Calendar."));
						throw new OperationException();
						
					}
					if(budCount==1){
					session.saveOrUpdate(schBudget);
					bgtId = schBudget.getPK_BUDGET();
					budCount++;
					}
					if(budCalCount==1){
					sCHBudgetCalPojo.setFK_BUDGET(bgtId);
					session.saveOrUpdate(sCHBudgetCalPojo);
					bgtCalId = sCHBudgetCalPojo.getPK_BGTCAL();
					budCalCount++;
					}
					schBudgetSectionPojo.setFK_BGTCAL(bgtCalId);
					session.saveOrUpdate(schBudgetSectionPojo);
					bgtSecId = schBudgetSectionPojo.getPK_BUDGETSEC();
					schLineItemList = bgtSectionDetail.getLineItemInfo();
					
					if(schLineItemList!=null && schLineItemList.size()>0){
					Iterator<LineitemBean> bgtLineITR = schLineItemList.iterator();
					while(bgtLineITR.hasNext()){
						bgtLineItemPojo = new LineitemBean();
						bgtLineItemPojo = (LineitemBean)bgtLineITR.next();
						bgtLineItemPojo.setLineitemBgtSection(bgtSecId);
						session.saveOrUpdate(bgtLineItemPojo);
							}
						}
					}
				}
			}
		}
			retStrings=String.valueOf(schBudget.getPK_BUDGET());
			tx.commit();
			
		}finally{
			HibernateUtil.closeSession(session);
		}*/
		
		return retStrings;
	}
	
	public static BudgetDetail getStudyCalBudget(StudyIdentifier studyIdent,CalendarNameIdentifier CalIdent, Map<String, Object> parameters) throws SecurityException, IllegalArgumentException, IOException, SAXException, NoSuchFieldException, IllegalAccessException, OperationException {
		ResponseHolder responseHolder = (ResponseHolder) parameters.get("ResponseHolder");
		UserBean callingUser =	(UserBean)parameters.get("callingUser");
		ObjectMapService objectMapService = (ObjectMapService)parameters.get("objectMapService");
		BudgetIdentifier budgetIdentifier = new BudgetIdentifier();
		StudyIdentifier studyIdentifier = new StudyIdentifier();
		CalendarNameIdentifier calendarIdentifier = new CalendarNameIdentifier();
		BudgetCalIdentifier budgetCalIdent = new BudgetCalIdentifier();
		BgtSectionNameIdentifier bgtSectionIdent = new BgtSectionNameIdentifier();
		VisitNameIdentifier visitNameIdent =new VisitNameIdentifier();
		LineItemNameIdentifier lineItemIdent = new LineItemNameIdentifier();
		EventNameIdentfier eventNameIdent = new EventNameIdentfier();
		Integer accountId = StringUtil.stringToInteger(callingUser.getUserAccountId());
		BudgetDetail budgetDetailPojo = new BudgetDetail();
		BgtTemplate bgtTemp = new BgtTemplate();
		BudgetBean schBgtPojo = new BudgetBean();
		BudgetPojo budgetPojo = new BudgetPojo();
		List<BudgetcalBean> schBudgetCalList = new ArrayList<BudgetcalBean>();
		List<BgtCalPojo> bgCalList = new ArrayList<BgtCalPojo>();
		BudgetCalList bgtCalList =new BudgetCalList();
		BgtCalPojo bgtCalPojo=null;
		List<BgtSectionBean> schBudgetSectionList = new ArrayList<BgtSectionBean>();
		List<BudgetSectionPojo> bgSecList = new ArrayList<BudgetSectionPojo>();
		BgtSectionDetail bgtSecList =new BgtSectionDetail();
		BudgetSectionPojo bgtSecPojo=null;
		List<LineitemBean> schBudgetLItemList = new ArrayList<LineitemBean>();
		List<BudgetLineItemPojo> bgLItemList = new ArrayList<BudgetLineItemPojo>();
		BgtLiniItemDetail bgtLItemList =new BgtLiniItemDetail();
		BudgetLineItemPojo bgtLItemPojo=null;
		
		schBgtPojo=(BudgetBean) getBudgetInfo(studyIdent.getPK(),CalIdent.getPK());
		try{
		if(schBgtPojo == null){
			responseHolder.addIssue(
					new Issue(
							IssueTypes.BUDGET_NOT_FOUND ,
					"Budget not found"));
			throw new OperationException();
		}}
		catch(OperationException e){
			e.setIssues(responseHolder.getIssues());
			throw e;
		}
		budgetDetailPojo.setStudyIdentifier(studyIdent);
		if(CalIdent!=null)
		budgetDetailPojo.setCalendarIdentifier(CalIdent);
		BudgetHandler.copyTOBgtIdent(schBgtPojo,budgetIdentifier,objectMapService);
		budgetDetailPojo.setBudgetIdentifier(budgetIdentifier);
		BudgetHandler.copyTOBudgetPojo(schBgtPojo,budgetPojo,accountId);
		if(budgetPojo.getBUDGET_TEMPLATE()==null){
			bgtTemp = getStudyCalBgtTemp(StringUtil.stringToInteger(schBgtPojo.getBudgetTemplate()),parameters);
			budgetDetailPojo.setBgtTemplate(bgtTemp);
		}
		schBudgetCalList =  getBudgetCalList(budgetIdentifier.getPK());
		if(schBudgetCalList!=null && schBudgetCalList.size()>0){
		for (BudgetcalBean schBudgetCalPojo : schBudgetCalList) {
			bgtCalPojo=new BgtCalPojo();
			budgetCalIdent = new BudgetCalIdentifier();
			calendarIdentifier = new CalendarNameIdentifier();
			BudgetHandler.copyTOBudgetCalPojo(schBudgetCalPojo,bgtCalPojo,accountId);
			BudgetHandler.copyTOBgtCalIdent(schBudgetCalPojo,budgetCalIdent,objectMapService);
			bgtCalPojo.setBudgetCalIdent(budgetCalIdent);
			if(schBudgetCalPojo.getBudgetProtId()!=null){
			BudgetHandler.copyTOCalNameIdent(schBudgetCalPojo,calendarIdentifier,objectMapService);
			bgtCalPojo.setCalNameIdent(calendarIdentifier);}
			
			schBudgetSectionList = getBudgetCalSecList(schBudgetCalPojo.getBudgetcalId());
		
			if(schBudgetSectionList!=null && schBudgetSectionList.size()>0){
				 bgSecList = new ArrayList<BudgetSectionPojo>();
				 bgtSecList =new BgtSectionDetail();
			for (BgtSectionBean schBudgetSectionPojo : schBudgetSectionList) {
				bgtSecPojo=new BudgetSectionPojo();
				bgtSectionIdent = new BgtSectionNameIdentifier();
				visitNameIdent = new VisitNameIdentifier();
				BudgetHandler.copyTOBudgetSecPojo(schBudgetSectionPojo,bgtSecPojo,accountId);
				BudgetHandler.copyTOBgtSecIdent(schBudgetSectionPojo,bgtSectionIdent,objectMapService);
				bgtSecPojo.setBgtSectionName(bgtSectionIdent);
				if(schBudgetSectionPojo.getBgtSectionFkVisit()!=null){
				BudgetHandler.copyTOVisitIdent(schBudgetSectionPojo,visitNameIdent,objectMapService);
				bgtSecPojo.setVisitIdent(visitNameIdent);}
				schBudgetLItemList= getBudgetLineItemList(schBudgetSectionPojo.getBgtSectionId());
				if(schBudgetLItemList!=null && schBudgetLItemList.size()>0){
					bgLItemList = new ArrayList<BudgetLineItemPojo>();
					bgtLItemList =new BgtLiniItemDetail();
				for (LineitemBean schLineItemPojo : schBudgetLItemList) {
					bgtLItemPojo=new BudgetLineItemPojo();
					lineItemIdent = new LineItemNameIdentifier();
					eventNameIdent = new EventNameIdentfier();
					BudgetHandler.copyTOBudgetLItemPojo(schLineItemPojo,bgtLItemPojo,accountId);
					BudgetHandler.copyTOLItemIdent(schLineItemPojo,lineItemIdent,objectMapService);
					bgtLItemPojo.setLineItemIdent(lineItemIdent);
					if(schLineItemPojo.getLineItemFkEvent()!=null){
					BudgetHandler.copyTOEventIdent(schLineItemPojo,eventNameIdent,objectMapService);
					bgtLItemPojo.setEventNameIdent(eventNameIdent);}
					bgLItemList.add(bgtLItemPojo);
						}
				bgtLItemList.setBudgetLineItemInfo(bgLItemList);
				bgtSecPojo.setLineItems(bgtLItemList);
				}
				bgSecList.add(bgtSecPojo);
				}
			bgtSecList.setBudgetSectionInfo(bgSecList);
			}
			bgtCalPojo.setBudgetSections(bgtSecList);
			bgCalList.add(bgtCalPojo);
			}
			bgtCalList.setBudgetCalendar(bgCalList);
			budgetPojo.setBudgetCalendars(bgtCalList);
		}
		budgetDetailPojo.setBudgetInfo(budgetPojo);
		return budgetDetailPojo;
	}
	
	public static BgtTemplate getStudyCalBgtTemp(Integer BgtId, Map<String, Object> parameters) throws OperationException, SecurityException, IllegalArgumentException, IOException, SAXException, NoSuchFieldException, IllegalAccessException
	{
		ResponseHolder responseHolder = (ResponseHolder) parameters.get("ResponseHolder");
		UserBean callingUser =	(UserBean)parameters.get("callingUser");
		ObjectMapService objectMapService = (ObjectMapService)parameters.get("objectMapService");
		BudgetIdentifier budgetIdentifier = new BudgetIdentifier();
		StudyIdentifier studyIdentifier = new StudyIdentifier();
		CalendarNameIdentifier calendarIdentifier = new CalendarNameIdentifier();
		BudgetCalIdentifier budgetCalIdent = new BudgetCalIdentifier();
		BgtSectionNameIdentifier bgtSectionIdent = new BgtSectionNameIdentifier();
		VisitNameIdentifier visitNameIdent =new VisitNameIdentifier();
		LineItemNameIdentifier lineItemIdent = new LineItemNameIdentifier();
		EventNameIdentfier eventNameIdent = new EventNameIdentfier();
		Integer accountId = StringUtil.stringToInteger(callingUser.getUserAccountId());
		BgtTemplate budgetDetailPojo = new BgtTemplate();
		BudgetBean schBgtPojo = new BudgetBean();
		BudgetPojo budgetPojo = new BudgetPojo();
		List<BudgetcalBean> schBudgetCalList = new ArrayList<BudgetcalBean>();
		List<BgtCalPojo> bgCalList = new ArrayList<BgtCalPojo>();
		BudgetCalList bgtCalList =new BudgetCalList();
		BgtCalPojo bgtCalPojo=null;
		List<BgtSectionBean> schBudgetSectionList = new ArrayList<BgtSectionBean>();
		List<BudgetSectionPojo> bgSecList = new ArrayList<BudgetSectionPojo>();
		BgtSectionDetail bgtSecList =new BgtSectionDetail();
		BudgetSectionPojo bgtSecPojo=null;
		List<LineitemBean> schBudgetLItemList = new ArrayList<LineitemBean>();
		List<BudgetLineItemPojo> bgLItemList = new ArrayList<BudgetLineItemPojo>();
		BgtLiniItemDetail bgtLItemList =new BgtLiniItemDetail();
		BudgetLineItemPojo bgtLItemPojo=null;
		
		schBgtPojo=(BudgetBean) getBudgetInfoById(BgtId);
		try{
		if(schBgtPojo == null){
			responseHolder.addIssue(
					new Issue(
							IssueTypes.BUDGET_NOT_FOUND ,
					"Budget not found"));
			throw new OperationException();
		}}
		catch(OperationException e){
			e.setIssues(responseHolder.getIssues());
			throw e;
		}
		BudgetHandler.copyTOBgtIdent(schBgtPojo,budgetIdentifier,objectMapService);
		budgetDetailPojo.setBudgetIdentifier(budgetIdentifier);
		BudgetHandler.copyTOBudgetPojo(schBgtPojo,budgetPojo,accountId);
		
		schBudgetCalList =  getBudgetCalList(budgetIdentifier.getPK());
		if(schBudgetCalList!=null && schBudgetCalList.size()>0){
		for (BudgetcalBean schBudgetCalPojo : schBudgetCalList) {
			bgtCalPojo=new BgtCalPojo();
			budgetCalIdent = new BudgetCalIdentifier();
			calendarIdentifier = new CalendarNameIdentifier();
			calendarIdentifier = new CalendarNameIdentifier();
			BudgetHandler.copyTOBudgetCalPojo(schBudgetCalPojo,bgtCalPojo,accountId);
			BudgetHandler.copyTOBgtCalIdent(schBudgetCalPojo,budgetCalIdent,objectMapService);
			bgtCalPojo.setBudgetCalIdent(budgetCalIdent);
			if(schBudgetCalPojo.getBudgetProtId()!=null){
			BudgetHandler.copyTOCalNameIdent(schBudgetCalPojo,calendarIdentifier,objectMapService);
			bgtCalPojo.setCalNameIdent(calendarIdentifier);}
			
			schBudgetSectionList = getBudgetCalSecList(schBudgetCalPojo.getBudgetcalId());
		
			if(schBudgetSectionList!=null && schBudgetSectionList.size()>0){
				 bgSecList = new ArrayList<BudgetSectionPojo>();
				 bgtSecList =new BgtSectionDetail();
			for (BgtSectionBean schBudgetSectionPojo : schBudgetSectionList) {
				bgtSecPojo=new BudgetSectionPojo();
				bgtSectionIdent = new BgtSectionNameIdentifier();
				visitNameIdent = new VisitNameIdentifier();
				BudgetHandler.copyTOBudgetSecPojo(schBudgetSectionPojo,bgtSecPojo,accountId);
				BudgetHandler.copyTOBgtSecIdent(schBudgetSectionPojo,bgtSectionIdent,objectMapService);
				bgtSecPojo.setBgtSectionName(bgtSectionIdent);
				if(schBudgetSectionPojo.getBgtSectionFkVisit()!=null){
				BudgetHandler.copyTOVisitIdent(schBudgetSectionPojo,visitNameIdent,objectMapService);
				bgtSecPojo.setVisitIdent(visitNameIdent);}
				schBudgetLItemList= getBudgetLineItemList(schBudgetSectionPojo.getBgtSectionId());
				if(schBudgetLItemList!=null && schBudgetLItemList.size()>0){
					bgLItemList = new ArrayList<BudgetLineItemPojo>();
					bgtLItemList =new BgtLiniItemDetail();
				for (LineitemBean schLineItemPojo : schBudgetLItemList) {
					bgtLItemPojo=new BudgetLineItemPojo();
					lineItemIdent = new LineItemNameIdentifier();
					eventNameIdent = new EventNameIdentfier();
					BudgetHandler.copyTOBudgetLItemPojo(schLineItemPojo,bgtLItemPojo,accountId);
					BudgetHandler.copyTOLItemIdent(schLineItemPojo,lineItemIdent,objectMapService);
					bgtLItemPojo.setLineItemIdent(lineItemIdent);
					if(schLineItemPojo.getLineItemFkEvent()!=null){
					BudgetHandler.copyTOEventIdent(schLineItemPojo,eventNameIdent,objectMapService);
					bgtLItemPojo.setEventNameIdent(eventNameIdent);}
					bgLItemList.add(bgtLItemPojo);
						}
				bgtLItemList.setBudgetLineItemInfo(bgLItemList);
				bgtSecPojo.setLineItems(bgtLItemList);
				}
				bgSecList.add(bgtSecPojo);
				}
			bgtSecList.setBudgetSectionInfo(bgSecList);
			}
			bgtCalPojo.setBudgetSections(bgtSecList);
			bgCalList.add(bgtCalPojo);
			}
			bgtCalList.setBudgetCalendar(bgCalList);
			budgetPojo.setBudgetCalendars(bgtCalList);
		}
		budgetDetailPojo.setBudgetInfo(budgetPojo);
		return budgetDetailPojo;
	}
	
	public static BudgetBean getBudgetInfo(Integer studyId,Integer calId) throws IOException, SAXException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, OperationException{
		BudgetBean schBgtPojo = new BudgetBean();
		Session session = HibernateUtil.eschSessionFactory.openSession();
		Criteria criteria = session.createCriteria(BudgetBean.class);
		criteria.add(Restrictions.eq("budgetStudyId", StringUtil.integerToString(studyId))).add(Restrictions.eq("budgetDefaultCalendar", StringUtil.integerToString(calId))).add(Restrictions.or(Restrictions.ne("budgetDelFlag", "Y"),Restrictions.isNull("budgetDelFlag")));
		schBgtPojo = (BudgetBean) criteria.uniqueResult();
		HibernateUtil.closeSession(session);
		return schBgtPojo;
	}
	
	public static BudgetBean getBudgetInfoById(Integer bgtId) throws IOException, SAXException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, OperationException{
		BudgetBean schBgtPojo = new BudgetBean();
		Session session = HibernateUtil.eschSessionFactory.openSession();
		Criteria criteria = session.createCriteria(BudgetBean.class);
		criteria.add(Restrictions.eq("budgetId", bgtId)).add(Restrictions.or(Restrictions.ne("budgetDelFlag", "Y"),Restrictions.isNull("budgetDelFlag")));
		schBgtPojo = (BudgetBean) criteria.uniqueResult();
		HibernateUtil.closeSession(session);
		return schBgtPojo;
	}
	
	public static List<BudgetcalBean> getBudgetCalList(Integer bgtId) throws IOException, SAXException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, OperationException{
		List<BudgetcalBean> bgtCalList = new ArrayList<BudgetcalBean>();
		Session session = HibernateUtil.eschSessionFactory.openSession();
		Criteria criteria = session.createCriteria(BudgetcalBean.class);
		criteria.add(Restrictions.eq("budgetId", StringUtil.integerToString(bgtId))).add(Restrictions.or(Restrictions.ne("budgetDelFlag", "Y"),Restrictions.isNull("budgetDelFlag"))).addOrder(Order.asc("budgetcalId"));
		bgtCalList = criteria.list();
		HibernateUtil.closeSession(session);
		return bgtCalList;
	}
	
	public static List<BgtSectionBean> getBudgetCalSecList(Integer bgtCalId) throws IOException, SAXException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, OperationException{
		List<BgtSectionBean> bgtCalSecList = new ArrayList<BgtSectionBean>();
		Session session = HibernateUtil.eschSessionFactory.openSession();
		Criteria criteria = session.createCriteria(BgtSectionBean.class);
		criteria.add(Restrictions.eq("bgtCal", StringUtil.integerToString(bgtCalId))).add(Restrictions.or(Restrictions.ne("bgtSectionDelFlag", "Y"),Restrictions.isNull("bgtSectionDelFlag"))).addOrder(Order.asc("bgtSectionId"));
		bgtCalSecList = criteria.list();
		HibernateUtil.closeSession(session);
		return bgtCalSecList;
	}
	
	public static List<LineitemBean> getBudgetLineItemList(Integer bgtSecId) throws IOException, SAXException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, OperationException{
		List<LineitemBean> bgtLineItemList = new ArrayList<LineitemBean>();
		Session session = HibernateUtil.eschSessionFactory.openSession();
		Criteria criteria = session.createCriteria(LineitemBean.class);
		criteria.add(Restrictions.eq("lineitemBgtSection", StringUtil.integerToString(bgtSecId))).add(Restrictions.or(Restrictions.ne("lineitemDelFlag", "Y"),Restrictions.isNull("lineitemDelFlag"))).addOrder(Order.asc("lineitemId"));
		bgtLineItemList = criteria.list();
		HibernateUtil.closeSession(session);
		return bgtLineItemList;
	}
	
	public String saveStudyCalBudget(BudgetDetail budgetDetail, Map<String, Object> parameters) throws HibernateException, IOException, SAXException, OperationException, MultipleObjectsFoundException{
		String[] retStrings = new String[2];
		Integer bgtId = 0;
		Integer bgtCalId=0;
		Integer bgtSecId=0;
		Integer budCount=1;
		Integer budCalCount=1;
		Integer bgtCalNullCheckCount=0;
		Integer bgtCalNullCheckFlag=0;
		Session session = (Session) HibernateUtil.eschSessionFactory.openSession();
		Transaction tx=null;
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("objectMapService");
		UserAgentRObj userAgent = (UserAgentRObj) parameters.get("userAgent"); 
		SessionContext sessionContext  = (SessionContext) parameters.get("sessionContext"); 
		ResponseHolder responseHolder = (ResponseHolder) parameters.get("ResponseHolder");
		UserBean callingUser = (UserBean) parameters.get("callingUser");
		BudgetIdentifier budgetIdentifier = new BudgetIdentifier();
		StudyIdentifier studyIdentifier = new StudyIdentifier();
		CalendarNameIdentifier calendarIdentifier = new CalendarNameIdentifier();
		BudgetCalIdentifier budgetCalIdent = new BudgetCalIdentifier();
		BgtSectionNameIdentifier bgtSectionIdent = new BgtSectionNameIdentifier();
		VisitNameIdentifier visitNameIdent =new VisitNameIdentifier();
		LineItemNameIdentifier lineItemIdent = new LineItemNameIdentifier();
		EventNameIdentfier eventNameIdent = new EventNameIdentfier();
		List<Issue> validationIssues = new ArrayList<Issue>();
		Integer accountId = StringUtil.stringToInteger(callingUser.getUserAccountId());
		BudgetDetail budgetDetailPojo = new BudgetDetail();
		BgtTemplate budgetTemplate = new BgtTemplate();
		BudgetBean schBgtPojo = new BudgetBean();
		BudgetcalBean schBgtCalPojo = new BudgetcalBean();
		BgtSectionBean schBgtSecPojo = new BgtSectionBean();
		LineitemBean schLineItemPojo = new LineitemBean();
		BudgetPojo budgetPojo = new BudgetPojo();
		List<BgtCalPojo> bgtCalList = new ArrayList<BgtCalPojo>();
		List<BudgetSectionPojo> bgtSecList = new ArrayList<BudgetSectionPojo>();
		List<BudgetLineItemPojo> bgtLItemList = new ArrayList<BudgetLineItemPojo>();
		try{
		budgetTemplate = budgetDetail.getBgtTemplate();
		if(budgetTemplate==null && budgetTemplate.getBudgetInfo()==null && budgetTemplate.getBudgetIdentifier()==null){
			responseHolder.addIssue(
					new Issue(
							IssueTypes.DATA_VALIDATION ,
					"BudgetTemplate is a mandatory."));
			throw new OperationException();
			
		}
		budgetIdentifier = budgetTemplate.getBudgetIdentifier();
		if(budgetIdentifier.getBudgetName()==null){
			responseHolder.addIssue(
					new Issue(
							IssueTypes.DATA_VALIDATION ,
					"Budget Name is a mandatory."));
			throw new OperationException();
			
		}
		
		bgtId = locateBudgetPK(budgetIdentifier.getBudgetName(),budgetIdentifier.getBudgetVersion(),StringUtil.integerToString(accountId));
		if(bgtId <= 0){
		tx = session.beginTransaction();
		schBgtPojo.setBudgetName(budgetTemplate.getBudgetIdentifier().getBudgetName());
		schBgtPojo.setBudgetVersion(budgetTemplate.getBudgetIdentifier().getBudgetVersion());
		budgetPojo = budgetTemplate.getBudgetInfo();
		BudgetHandler.copyToSchBudgetPojo(budgetPojo,schBgtPojo,callingUser,validationIssues,responseHolder);
		if(budgetPojo.getBudgetCalendars()==null){
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "BUDGET CALENDARs Not Found: "));
			((ResponseHolder) responseHolder)
			.getIssues().addAll(validationIssues); 
			
			if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
			throw new ValidationException();
		}else
			bgtCalList = budgetPojo.getBudgetCalendars().getBudgetCalendar();
		if(bgtCalList==null || bgtCalList.size()==0){
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "BUDGET CALENDAR Not Found: "));
			((ResponseHolder) responseHolder)
			.getIssues().addAll(validationIssues); 
			
			if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
			throw new ValidationException();
		}
		else{
			for (BgtCalPojo bgtCalPojo : bgtCalList) {
			schBgtCalPojo=new BudgetcalBean();
			BudgetHandler.copyToSchBudgetCalPojo(bgtCalPojo,schBgtCalPojo,callingUser,validationIssues,responseHolder);
			if(bgtCalPojo.getBudgetSections()==null){
				validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "BUDGET Sections Not Found: "));
				((ResponseHolder) responseHolder)
				.getIssues().addAll(validationIssues); 
				
				if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
				throw new ValidationException();
			}else
			bgtSecList = bgtCalPojo.getBudgetSections().getBudgetSectionInfo();
			if(bgtSecList==null || bgtSecList.size()==0){
				validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "BUDGET Section Not Found: "));
				((ResponseHolder) responseHolder)
				.getIssues().addAll(validationIssues); 
				
				if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
				throw new ValidationException();
			}
			else{budCalCount=1;
				for (BudgetSectionPojo bgtSecPojo : bgtSecList) {
					schBgtSecPojo=new BgtSectionBean();
					BudgetHandler.copyToSchBudgetSecPojo(bgtSecPojo,schBgtSecPojo,callingUser,validationIssues,responseHolder);
					if(bgtSecPojo.getBgtSectionName()!=null && bgtSecPojo.getBgtSectionName().getBgtSectionName()!=null)
					schBgtSecPojo.setBgtSectionName(bgtSecPojo.getBgtSectionName().getBgtSectionName());
					else{
						validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "BUDGET SectionIdent Not Found: "));
						((ResponseHolder) responseHolder)
						.getIssues().addAll(validationIssues); 
						
						if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
						throw new ValidationException();
					}
					if(budCount==1){
						session.saveOrUpdate(schBgtPojo);
						bgtId = schBgtPojo.getBudgetId();
						budCount++;
						session.flush();
				        session.clear();
						}
					if(budCalCount==1){
						schBgtCalPojo.setBudgetId(StringUtil.integerToString(bgtId));
						session.saveOrUpdate(schBgtCalPojo);
						bgtCalId = schBgtCalPojo.getBudgetcalId();
						budCalCount++;
						session.flush();
				        session.clear();
						}
					schBgtSecPojo.setBgtCal(StringUtil.integerToString(bgtCalId));
					session.saveOrUpdate(schBgtSecPojo);
					bgtSecId = schBgtSecPojo.getBgtSectionId();
					session.flush();
			        session.clear();
					if(bgtSecPojo.getLineItems()==null){
						
					}else{
					bgtLItemList = bgtSecPojo.getLineItems().getBudgetLineItemInfo();
					if(bgtLItemList==null || bgtLItemList.size()==0){
						validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "BUDGET LineItem Not Found: "));
						((ResponseHolder) responseHolder)
						.getIssues().addAll(validationIssues); 
						
						if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
						throw new ValidationException();
					}
					else{for (BudgetLineItemPojo bgtLItemPojo : bgtLItemList) {
						schLineItemPojo=new LineitemBean();
						BudgetHandler.copyToSchBudgetLItemPojo(bgtLItemPojo,schLineItemPojo,callingUser,validationIssues,responseHolder);
						if(bgtLItemPojo.getLineItemIdent()!=null && bgtLItemPojo.getLineItemIdent().getLineItemName()!=null)
						schLineItemPojo.setLineitemName(bgtLItemPojo.getLineItemIdent().getLineItemName());
						else{
							validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "BUDGET LineItemIdent Not Found: "));
							((ResponseHolder) responseHolder)
							.getIssues().addAll(validationIssues); 
							
							if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
							throw new ValidationException();
						}
						schLineItemPojo.setLineitemBgtSection(StringUtil.integerToString(bgtSecId));
						session.saveOrUpdate(schLineItemPojo);
						session.flush();
				        session.clear();
							
					}
				}
						
			}
						
		  }
				}
				
			}
		}
		tx.commit();
		}
		
		}catch (RuntimeException e) {
			if(tx!=null){
				tx.rollback();
				logger.error("****************ROLLING BACK Inserts ==============>>>>HibernateException**************"+e.getMessage());
				return retStrings[1];
			}
		}
		
		Transaction txn=null;
		try{
		retStrings[0] = String.valueOf(bgtId);
		schBgtPojo = new BudgetBean();
		
		schBgtPojo.setBudgetName(budgetDetail.getBudgetIdentifier().getBudgetName());
		schBgtPojo.setBudgetVersion(budgetDetail.getBudgetIdentifier().getBudgetVersion());
		schBgtPojo.setBudgetTemplate(StringUtil.integerToString(bgtId));
		bgtId = 0;
		bgtCalId=0;
		bgtSecId=0;
		budCount=1;
		budCalCount=1;
		schBgtPojo.setBudgetStudyId(StringUtil.integerToString(budgetDetail.getStudyIdentifier().getPK()));
		schBgtPojo.setBudgetDefaultCalendar(StringUtil.integerToString(budgetDetail.getCalendarIdentifier().getPK()));
		budgetPojo = budgetDetail.getBudgetInfo();
		BudgetHandler.copyToSchBudgetPojo(budgetPojo,schBgtPojo,callingUser,validationIssues,responseHolder);
		if(budgetPojo.getBudgetCalendars()==null){
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "BUDGET CALENDARs Not Found: "));
			((ResponseHolder) responseHolder)
			.getIssues().addAll(validationIssues); 
			
			if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
			throw new ValidationException();
		}else
			bgtCalList = budgetPojo.getBudgetCalendars().getBudgetCalendar();
		if(bgtCalList==null || bgtCalList.size()==0){
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "BUDGET CALENDAR Not Found: "));
			((ResponseHolder) responseHolder)
			.getIssues().addAll(validationIssues); 
			
			if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
			throw new ValidationException();
		}
		else{
			for (BgtCalPojo bgtCalPojo : bgtCalList) {bgtCalNullCheckCount++;
			schBgtCalPojo=new BudgetcalBean();
			if(bgtCalPojo.getCalNameIdent()==null && bgtCalNullCheckCount==1){
				bgtCalNullCheckFlag++;
			}
			if( bgtCalNullCheckCount>2){
				validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Calendar Budget should not contain More than 2 Budget Calendars: "));
				((ResponseHolder) responseHolder)
				.getIssues().addAll(validationIssues); 
				
				if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
				throw new ValidationException();
			}
			if(bgtCalNullCheckFlag==1){
				if(bgtCalPojo.getCalNameIdent()!=null){
					schBgtCalPojo.setBudgetProtId(StringUtil.integerToString(budgetDetail.getCalendarIdentifier().getPK()));
				}
				BudgetHandler.copyToSchBudgetCalPojo(bgtCalPojo,schBgtCalPojo,callingUser,validationIssues,responseHolder);
			}else
			{
				validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "First Budget Calendar should not contain any Calendar from Study: "));
				((ResponseHolder) responseHolder)
				.getIssues().addAll(validationIssues); 
				
				if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
				throw new ValidationException();	
			}
			
			if(bgtCalPojo.getBudgetSections()==null){
				validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "BUDGET Sections Not Found: "));
				((ResponseHolder) responseHolder)
				.getIssues().addAll(validationIssues); 
				
				if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
				throw new ValidationException();
			}else
			bgtSecList = bgtCalPojo.getBudgetSections().getBudgetSectionInfo();
			if(bgtSecList==null || bgtSecList.size()==0){
				validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "BUDGET Section Not Found: "));
				((ResponseHolder) responseHolder)
				.getIssues().addAll(validationIssues); 
				
				if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
				throw new ValidationException();
			}
			else{budCalCount=1;
				for (BudgetSectionPojo bgtSecPojo : bgtSecList) {
					schBgtSecPojo=new BgtSectionBean();
					if(bgtSecPojo.getVisitIdent()!=null){
						if(bgtSecPojo.getVisitIdent().getVisitName()!=null){
							schBgtSecPojo.setBgtSectionFkVisit(
							MilestoneServiceDao.getVisitPKByXMLInputString(bgtSecPojo.getVisitIdent().getVisitName(), StringUtil.integerToString(budgetDetail.getCalendarIdentifier().getPK())));
							if(schBgtSecPojo.getBgtSectionFkVisit()<=0){
								validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "BUDGET Section Visit Not Found in Calendar: "));
								((ResponseHolder) responseHolder)
								.getIssues().addAll(validationIssues); 
								
								if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
								throw new ValidationException();
							}else{
								bgtSecPojo.getVisitIdent().setPK(schBgtSecPojo.getBgtSectionFkVisit());
							}
						}else{
							validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "BUDGET Section Visit Name Not Found: "));
							((ResponseHolder) responseHolder)
							.getIssues().addAll(validationIssues); 
							
							if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
							throw new ValidationException();
						}
					}
					BudgetHandler.copyToSchBudgetSecPojo(bgtSecPojo,schBgtSecPojo,callingUser,validationIssues,responseHolder);
					if(bgtSecPojo.getBgtSectionName()!=null && bgtSecPojo.getBgtSectionName().getBgtSectionName()!=null)
					schBgtSecPojo.setBgtSectionName(bgtSecPojo.getBgtSectionName().getBgtSectionName());
					else{
						validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "BUDGET SectionIdent Not Found: "));
						((ResponseHolder) responseHolder)
						.getIssues().addAll(validationIssues); 
						
						if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
						throw new ValidationException();
					}
					txn=session.beginTransaction();
					if(budCount==1){
						session.saveOrUpdate(schBgtPojo);
						bgtId = schBgtPojo.getBudgetId();
						budCount++;
						session.flush();
				        session.clear();
						}
					if(budCalCount==1){
						schBgtCalPojo.setBudgetId(StringUtil.integerToString(bgtId));
						session.saveOrUpdate(schBgtCalPojo);
						bgtCalId = schBgtCalPojo.getBudgetcalId();
						budCalCount++;
						session.flush();
				        session.clear();
						}
					schBgtSecPojo.setBgtCal(StringUtil.integerToString(bgtCalId));
					session.saveOrUpdate(schBgtSecPojo);
					bgtSecId = schBgtSecPojo.getBgtSectionId();
					session.flush();
			        session.clear();
					if(bgtSecPojo.getLineItems()==null){
						
					}else{
					bgtLItemList = bgtSecPojo.getLineItems().getBudgetLineItemInfo();
					if(bgtLItemList==null || bgtLItemList.size()==0){
						validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "BUDGET LineItem Not Found: "));
						((ResponseHolder) responseHolder)
						.getIssues().addAll(validationIssues); 
						
						if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
						throw new ValidationException();
					}
					else{for (BudgetLineItemPojo bgtLItemPojo : bgtLItemList) {
						schLineItemPojo=new LineitemBean();
						if(bgtLItemPojo.getEventNameIdent()!=null){
							if(bgtLItemPojo.getEventNameIdent().getEventName()!=null){
								schLineItemPojo.setLineItemFkEvent(
										MilestoneServiceDao.getEventPKByXMLInputString(
												bgtLItemPojo.getEventNameIdent().getEventName(),StringUtil.integerToString(budgetDetail.getCalendarIdentifier().getPK()),
												StringUtil.integerToString(bgtSecPojo.getVisitIdent().getPK())));
										if(schLineItemPojo.getLineItemFkEvent()<=0){
											validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "BUDGET LineItem Event Not Found in Calendar: "));
											((ResponseHolder) responseHolder)
											.getIssues().addAll(validationIssues); 
											
											if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
											throw new ValidationException();
										}
							}else{
								validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "BUDGET LineItem Event Name Not Found: "));
								((ResponseHolder) responseHolder)
								.getIssues().addAll(validationIssues); 
								
								if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
								throw new ValidationException();
							}
						}
						BudgetHandler.copyToSchBudgetLItemPojo(bgtLItemPojo,schLineItemPojo,callingUser,validationIssues,responseHolder);
						if(bgtLItemPojo.getLineItemIdent()!=null && bgtLItemPojo.getLineItemIdent().getLineItemName()!=null)
						schLineItemPojo.setLineitemName(bgtLItemPojo.getLineItemIdent().getLineItemName());
						else{
							validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "BUDGET LineItemIdent Not Found: "));
							((ResponseHolder) responseHolder)
							.getIssues().addAll(validationIssues); 
							
							if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
							throw new ValidationException();
						}
						schLineItemPojo.setLineitemBgtSection(StringUtil.integerToString(bgtSecId));
						session.saveOrUpdate(schLineItemPojo);
						session.flush();
				        session.clear();
							
					}
				}
						
			}
						
		  }
				}
				
			}
		}
		
		/*Object obj=session.load(EventAssocPojo.class,StringUtil.integerToString(budgetDetail.getCalendarIdentifier().getPK()));
		EventAssocPojo eventAssocPojo=(EventAssocPojo)obj;
		tx=session.beginTransaction();	
		eventAssocPojo.setBUDGET_TEMPLATE(StringUtil.integerToString(bgtId)); 
		session.saveOrUpdate(eventAssocPojo);
		tx.commit();
		tx=session.beginTransaction();
		*/
		txn.commit();		
		if(bgtId>0){
		retStrings[1] = String.valueOf(bgtId);////change here=============
		EventAssocJB eventAssocJb = new EventAssocJB();
		eventAssocJb.setEvent_id(budgetDetail.getCalendarIdentifier().getPK());
		eventAssocJb.getEventAssocDetails();
		//eventAssocBean = (EventAssocBean) eventAssocJb.getEventAssocDetails();
		//eventAssocBean.setBudgetTemplate(StringUtil.integerToString(bgtId));
		eventAssocJb.setBudgetTemplate(retStrings[0]);
		eventAssocJb.updateEventAssoc();
		}
		
		}catch (HibernateException e) {
			if(txn!=null){
				txn.rollback();
			logger.error("****************ROLLING BACK Inserts ==============>>>>HibernateException**************"+e.getMessage());
			return retStrings[1];
					}		
		}catch (RuntimeException e) {
			if(txn!=null){
				txn.rollback();
			logger.error("****************ROLLING BACK Inserts ==============>>>>RuntimeException**************"+e.getMessage());
			return retStrings[1];
			}
		}catch (Exception e) {
			if(txn!=null){
				txn.rollback();
				logger.error("****************ROLLING BACK Inserts ==============>>>>Exception**************"+e.getMessage());
				return retStrings[1];
			}
		}finally{
			HibernateUtil.closeSession(session);
		}
		return retStrings[1];
	}

}
