package com.velos.services.budget;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.util.SVC;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.account.AccountJB;
import com.velos.esch.business.bgtSection.impl.BgtSectionBean;
import com.velos.esch.business.budget.impl.BudgetBean;
import com.velos.esch.business.budgetcal.impl.BudgetcalBean;
import com.velos.esch.business.lineitem.impl.LineitemBean;
import com.velos.esch.web.eventassoc.EventAssocJB;
import com.velos.esch.web.protvisit.ProtVisitJB;
import com.velos.services.AbstractService;
import com.velos.services.CodeNotFoundException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.ResponseHolder;
import com.velos.services.ValidationException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.BgtCalPojo;
import com.velos.services.model.BgtSectionNameIdentifier;
import com.velos.services.model.BudgetCalIdentifier;
import com.velos.services.model.BudgetIdentifier;
import com.velos.services.model.BudgetLineItemPojo;
import com.velos.services.model.BudgetPojo;
import com.velos.services.model.BudgetSectionPojo;
import com.velos.services.model.CalendarNameIdentifier;
import com.velos.services.model.Code;
import com.velos.services.model.EventNameIdentfier;
import com.velos.services.model.LineItemNameIdentifier;
import com.velos.services.model.VisitNameIdentifier;
import com.velos.services.util.CodeCache;

public class BudgetHandler extends AbstractService {
	

	private static Logger logger = Logger.getLogger(BudgetHandler.class);
	
	public static void validateBudgetDetails(BudgetBean bPojo, UserBean callingUser,Map<String, Object> parameters) throws ValidationException
	{
		AccountJB acctJB = new AccountJB(); 
		acctJB.setAccId(StringUtil.stringToInteger(callingUser.getUserAccountId())); 
		acctJB.getAccountDetails(); 

		List<Issue> validationIssues = new ArrayList<Issue>();
				
			if( bPojo.getBudgetName() == null)
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_Budget_Name_Mandatory));
			}
			if( bPojo.getBudgetCreator() == null)
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_Budget_Creator_Mandatory)); 
			}
			if( bPojo.getBudgetCurrency() == null)
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_Budget_Currency_Mandatory)); 
			}
			if( bPojo.getBudgetTemplate() == null)
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_Budget_Template_Mandatory)); 
			}
			if( bPojo.getBudgetStatus() == null)
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_Budget_Status_Mandatory)); 
			}
			if( bPojo.getBudgetCombinedFlag() == null)
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_Budget_CombFlag_Mandatory)); 
			}
			if( bPojo.getBudgetType() == null)
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_Budget_Type_Mandatory)); 
			}
			
			

		if (validationIssues.size() > 0){
			((ResponseHolder) parameters.get("ResponseHolder"))
			.getIssues().addAll(validationIssues); 
			
			if (logger.isDebugEnabled()) logger.debug("Validation Issues found for MilestoneDetails");
			throw new ValidationException();
		}
		
		
	}
	
	public static void copyTOBudgetPojo(BudgetBean schBgtPojo,BudgetPojo budgetPojo,Integer callingUserAccId){
		budgetPojo.setBUDGET_CALFLAG(schBgtPojo.getBudgetCFlag());
		budgetPojo.setBUDGET_COMBFLAG(schBgtPojo.getBudgetCombinedFlag());
		budgetPojo.setBUDGET_DELFLAG(schBgtPojo.getBudgetDelFlag());
		CodeCache codeCache = CodeCache.getInstance();
		if(schBgtPojo.getBudgetCurrency()!=null){
			Code bgtCurrency = codeCache.getSchCodeSubTypeByPK(CodeCache.CODE_BUDGET_CURRENCY, StringUtil.stringToInteger(schBgtPojo.getBudgetCurrency()),callingUserAccId);
			budgetPojo.setBUDGET_CURRENCY(bgtCurrency);}
		if(schBgtPojo.getBudgetCodeListStatus()!=null){
			Code bgtStat = codeCache.getSchCodeSubTypeByPK(CodeCache.CODE_BUDGET_STATUS, StringUtil.stringToInteger(schBgtPojo.getBudgetCodeListStatus()),callingUserAccId);
			budgetPojo.setPK_CODELST_STATUS(bgtStat);}
		if(schBgtPojo.getBudgetTemplate()!=null){
			Code bgtTemp = codeCache.getSchCodeSubTypeByPK(CodeCache.CODE_BUDGET_TEMPLATE, StringUtil.stringToInteger(schBgtPojo.getBudgetTemplate()),callingUserAccId);
			budgetPojo.setBUDGET_TEMPLATE(bgtTemp);}
		budgetPojo.setBUDGET_DESC(schBgtPojo.getBudgetdesc());
		budgetPojo.setBUDGET_RIGHTS(schBgtPojo.getBudgetRights());
		budgetPojo.setBUDGET_RIGHTSCOPE(schBgtPojo.getBudgetRScope());
		budgetPojo.setBUDGET_SITEFLAG(schBgtPojo.getBudgetSFlag());
		/*budgetPojo.setBUDGET_STATUS(schBgtPojo.getBUDGET_STATUS());*/
		budgetPojo.setBUDGET_TYPE(schBgtPojo.getBudgetType());
		budgetPojo.setBUDGET_CREATOR(StringUtil.stringToInteger(schBgtPojo.getBudgetCreator()));	
	}
	
	public static void copyTOBgtIdent(BudgetBean schBgtPojo,BudgetIdentifier bgtIdent,ObjectMapService objectMapService){
		bgtIdent.setBudgetName(schBgtPojo.getBudgetName());
		bgtIdent.setBudgetVersion(schBgtPojo.getBudgetVersion());
		bgtIdent.setPK(schBgtPojo.getBudgetId());
		ObjectMap bgtMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, schBgtPojo.getBudgetId()); 
		bgtIdent.setOID(bgtMap.getOID());
	}
	public static void copyTOBgtCalIdent(BudgetcalBean schBgtCalPojo,BudgetCalIdentifier bgtCalIdent,ObjectMapService objectMapService){
		bgtCalIdent.setPK(schBgtCalPojo.getBudgetcalId());
		ObjectMap bgtCalMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_BUDGET_CALENDAR, schBgtCalPojo.getBudgetcalId()); 
		bgtCalIdent.setOID(bgtCalMap.getOID());
	}
	public static void copyTOCalNameIdent(BudgetcalBean schBgtCalPojo,CalendarNameIdentifier bgtCalNameIdent,ObjectMapService objectMapService){
		bgtCalNameIdent.setPK(StringUtil.stringToInteger(schBgtCalPojo.getBudgetProtId()));
		ObjectMap bgtCalMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, StringUtil.stringToInteger(schBgtCalPojo.getBudgetProtId())); 
		bgtCalNameIdent.setOID(bgtCalMap.getOID());
		EventAssocJB eventAssoc = new EventAssocJB();
		eventAssoc.setEvent_id(StringUtil.stringToInteger(schBgtCalPojo.getBudgetProtId()));
		eventAssoc.getEventAssocDetails();
		bgtCalNameIdent.setCalendarName(eventAssoc.getName());
		bgtCalNameIdent.setCalendarAssocTo(eventAssoc.getCalAssocTo());
		
	}
	
	public static void copyTOBudgetCalPojo(BudgetcalBean schBgtCalPojo,BgtCalPojo budgetCalPojo,Integer callingUserAccId){
		budgetCalPojo.setBGTCAL_CLINICFLAG(schBgtCalPojo.getClFlag());
		budgetCalPojo.setBGTCAL_CLINICOHEAD(StringUtil.stringToDouble(schBgtCalPojo.getClOverHead()));
		budgetCalPojo.setBGTCAL_DELFLAG(schBgtCalPojo.getBudgetDelFlag());
		budgetCalPojo.setBGTCAL_DISCOUNT(StringUtil.stringToDouble(schBgtCalPojo.getBudgetDiscount()));
		budgetCalPojo.setBGTCAL_DISCOUNTFLAG(StringUtil.stringToInteger(schBgtCalPojo.getBudgetDiscountFlag()));
		budgetCalPojo.setBGTCAL_EXCLDSOCFLAG(StringUtil.stringToInteger(schBgtCalPojo.getBudgetExcldSOCFlag()));
		budgetCalPojo.setBGTCAL_FRGBENEFIT(StringUtil.stringToDouble(schBgtCalPojo.getBudgetFrgBenefit()));
		budgetCalPojo.setBGTCAL_FRGFLAG(StringUtil.stringToInteger(schBgtCalPojo.getBudgetFrgFlag()));
		budgetCalPojo.setBGTCAL_INDIRECTCOST(StringUtil.stringToDouble(schBgtCalPojo.getBudgetIndirectCost()));
		budgetCalPojo.setBGTCAL_PROTTYPE(schBgtCalPojo.getBudgetProtType());
		budgetCalPojo.setBGTCAL_SP_OVERHEAD(StringUtil.stringToDouble(schBgtCalPojo.getBudgetSPOverHead()));
		budgetCalPojo.setBGTCAL_SP_OVERHEAD_FLAG(StringUtil.stringToInteger(schBgtCalPojo.getBudgetSPFlag()));
		budgetCalPojo.setBGTCAL_SPONSORFLAG(schBgtCalPojo.getSpFlag());
		budgetCalPojo.setBGTCAL_SPONSOROHEAD(StringUtil.stringToDouble(schBgtCalPojo.getSpOverHead()));
		budgetCalPojo.setGRAND_DISC_COST(schBgtCalPojo.getBudgetDiscontinueCost());
		budgetCalPojo.setGRAND_DISC_TOTALCOST(schBgtCalPojo.getBudgetDiscontinueTotalCost());
		budgetCalPojo.setGRAND_FRINGE_COST(schBgtCalPojo.getBudgetFringeCost());
		budgetCalPojo.setGRAND_FRINGE_TOTALCOST(schBgtCalPojo.getBudgetFringeTotalCost());
		budgetCalPojo.setGRAND_IND_COST(schBgtCalPojo.getBudgetIndirectCost());
		budgetCalPojo.setGRAND_IND_TOTALCOST(schBgtCalPojo.getBudgetIndirectTotalCost());
		budgetCalPojo.setGRAND_RES_COST(schBgtCalPojo.getBudgetResearchCost());
		budgetCalPojo.setGRAND_RES_SPONSOR(schBgtCalPojo.getBudgetResearchSponsorAmount());
		budgetCalPojo.setGRAND_RES_TOTALCOST(schBgtCalPojo.getBudgetResearchTotalCost());
		budgetCalPojo.setGRAND_RES_VARIANCE(schBgtCalPojo.getBudgetResearchVariance());
		budgetCalPojo.setGRAND_RES_VIARIANCE(schBgtCalPojo.getBudgetResViariance());
		budgetCalPojo.setGRAND_SALARY_COST(schBgtCalPojo.getBudgetSalaryCost());
		budgetCalPojo.setGRAND_SALARY_TOTALCOST(schBgtCalPojo.getBudgetSalaryTotalCost());
		budgetCalPojo.setGRAND_SOC_COST(schBgtCalPojo.getBudgetSOCCost());
		budgetCalPojo.setGRAND_SOC_SPONSOR(schBgtCalPojo.getBudgetSOCSponsorAmount());
		budgetCalPojo.setGRAND_SOC_TOTALCOST(schBgtCalPojo.getBudgetSOCTotalCost());
		budgetCalPojo.setGRAND_SOC_VARIANCE(schBgtCalPojo.getBudgetSOCVariance());
		budgetCalPojo.setGRAND_TOTAL_COST(schBgtCalPojo.getBudgetTotalCost());
		budgetCalPojo.setGRAND_TOTAL_SPONSOR(schBgtCalPojo.getBudgetTotalSponsorAmount());
		budgetCalPojo.setGRAND_TOTAL_TOTALCOST(schBgtCalPojo.getBudgetTotalTotalCost());
		budgetCalPojo.setGRAND_TOTAL_VARIANCE(schBgtCalPojo.getBudgetTotalVariance());
	}
	
	public static void copyTOBudgetSecPojo(BgtSectionBean schBgtSecPojo,BudgetSectionPojo bgtSecPojo,Integer callingUserAccId){
		bgtSecPojo.setBGTSECTION_DELFLAG(schBgtSecPojo.getBgtSectionDelFlag());
		bgtSecPojo.setBGTSECTION_NOTES(schBgtSecPojo.getBgtSectionNotes());
		bgtSecPojo.setBGTSECTION_PATNO(schBgtSecPojo.getBgtSectionPatNo());
		bgtSecPojo.setBGTSECTION_PERSONLFLAG(schBgtSecPojo.getBgtSectionPersonlFlag());
		bgtSecPojo.setBGTSECTION_SEQUENCE(schBgtSecPojo.getBgtSectionSequence());
		bgtSecPojo.setBGTSECTION_TYPE(schBgtSecPojo.getBgtSectionType());
		bgtSecPojo.setBGTSECTION_VISIT(schBgtSecPojo.getBgtSectionVisit());
		bgtSecPojo.setSOC_COST_GRANDTOTAL(schBgtSecPojo.getSocCostGrandTotal());
		bgtSecPojo.setSOC_COST_SPONSOR(schBgtSecPojo.getSocCostSponsorAmount());
		bgtSecPojo.setSOC_COST_TOTAL(schBgtSecPojo.getSocCostTotal());
		bgtSecPojo.setSOC_COST_VARIANCE(schBgtSecPojo.getSocCostVariance());
		bgtSecPojo.setSRT_COST_GRANDTOTAL(schBgtSecPojo.getSrtCostGrandTotal());
		bgtSecPojo.setSRT_COST_SPONSOR(schBgtSecPojo.getSrtCostSponsorAmount());
		bgtSecPojo.setSRT_COST_TOTAL(schBgtSecPojo.getSrtCostTotal());
		bgtSecPojo.setSRT_COST_VARIANCE(schBgtSecPojo.getSrtCostVariance());
		
	}
	
	public static void copyTOBgtSecIdent(BgtSectionBean schBgtSecPojo,BgtSectionNameIdentifier bgtSecIdent,ObjectMapService objectMapService){
		bgtSecIdent.setPK(schBgtSecPojo.getBgtSectionId());
		ObjectMap bgtSecMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_BUDGET_SECTION, schBgtSecPojo.getBgtSectionId()); 
		bgtSecIdent.setOID(bgtSecMap.getOID());
		bgtSecIdent.setBgtSectionName(schBgtSecPojo.getBgtSectionName());
	}
	public static void copyTOVisitIdent(BgtSectionBean schBgtSecPojo,VisitNameIdentifier visitNameIdent,ObjectMapService objectMapService){
		visitNameIdent.setPK(schBgtSecPojo.getBgtSectionFkVisit());
		ObjectMap visitMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_VISIT, schBgtSecPojo.getBgtSectionFkVisit()); 
		visitNameIdent.setOID(visitMap.getOID());
		ProtVisitJB protJb = new ProtVisitJB();
		protJb.setVisit_id(schBgtSecPojo.getBgtSectionFkVisit());
		protJb.getProtVisitDetails();
		visitNameIdent.setVisitName(protJb.getName());
	}
	
	public static void copyTOBudgetLItemPojo(LineitemBean schBgtLItemPojo,BudgetLineItemPojo bgtLItemPojo,Integer callingUserAccId){
		bgtLItemPojo.setLINEITEM_APPLYINDIRECTS(schBgtLItemPojo.getLineitemAppIndirects());
		bgtLItemPojo.setLINEITEM_APPLYINFUTURE(schBgtLItemPojo.getLineitemAppInFuture());
		bgtLItemPojo.setLINEITEM_APPLYPATIENTCOUNT(schBgtLItemPojo.getLineitemApplyPatientCount());
		bgtLItemPojo.setLINEITEM_CDM(schBgtLItemPojo.getLineitemCDM());
		bgtLItemPojo.setLINEITEM_CLINICNOFUNIT(schBgtLItemPojo.getLineitemClinicNOfUnit());
		bgtLItemPojo.setLINEITEM_CPTCODE(schBgtLItemPojo.getLineitemCptCode());
		bgtLItemPojo.setLINEITEM_DELFLAG(schBgtLItemPojo.getLineitemDelFlag());
		bgtLItemPojo.setLINEITEM_DESC(schBgtLItemPojo.getLineitemDesc());
		bgtLItemPojo.setLINEITEM_INCOSTDISC(schBgtLItemPojo.getLineitemInCostDisc());
		bgtLItemPojo.setLINEITEM_INPERSEC(schBgtLItemPojo.getLineitemInPerSec());
		bgtLItemPojo.setLINEITEM_INVCOST(schBgtLItemPojo.getLineitemInvCost());
		bgtLItemPojo.setLINEITEM_NOTES(schBgtLItemPojo.getLineitemNotes());
		bgtLItemPojo.setLINEITEM_OTHERCOST(schBgtLItemPojo.getLineitemOtherCost());
		bgtLItemPojo.setLINEITEM_PARENTID(schBgtLItemPojo.getLineitemParentId());
		bgtLItemPojo.setLINEITEM_REPEAT(schBgtLItemPojo.getLineitemRepeat());
		bgtLItemPojo.setLINEITEM_SEQ(schBgtLItemPojo.getLineItemSequence());
		bgtLItemPojo.setLINEITEM_SPONSORAMOUNT(schBgtLItemPojo.getLineItemSponsorAmount());
		bgtLItemPojo.setLINEITEM_SPONSORUNIT(schBgtLItemPojo.getLineitemSponsorUnit());
		bgtLItemPojo.setLINEITEM_STDCARECOST(schBgtLItemPojo.getLineitemStdCareCost());
		bgtLItemPojo.setLINEITEM_TMID(schBgtLItemPojo.getLineitemTMID());
		bgtLItemPojo.setLINEITEM_TOTALCOST(schBgtLItemPojo.getLineitemTotalCost());
		bgtLItemPojo.setLINEITEM_VARIANCE(schBgtLItemPojo.getLineItemVariance());
		bgtLItemPojo.setSUBCOST_ITEM_FLAG(schBgtLItemPojo.getLineitemSubCostFlag());
		CodeCache codeCache = CodeCache.getInstance();
		if(schBgtLItemPojo.getLineitemCategory()!=null){
			Code lItemCat = codeCache.getSchCodeSubTypeByPK(CodeCache.CODE_LINEITEM_CATEGORY, StringUtil.stringToInteger(schBgtLItemPojo.getLineitemCategory()),callingUserAccId);
			bgtLItemPojo.setPK_CODELST_CATEGORY(lItemCat);}
		if(schBgtLItemPojo.getLineItemCostType()!=null){
			Code lItemCType = codeCache.getSchCodeSubTypeByPK(CodeCache.CODE_LINEITEM_COST_DESC, StringUtil.stringToInteger(schBgtLItemPojo.getLineItemCostType()),callingUserAccId);
			bgtLItemPojo.setPK_CODELST_COST_TYPE(lItemCType);}
	}
	
	
	public static void copyTOLItemIdent(LineitemBean schBgtLItemPojo,LineItemNameIdentifier bgtLItemIdent,ObjectMapService objectMapService){
		bgtLItemIdent.setPK(schBgtLItemPojo.getLineitemId());
		ObjectMap milestoneMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_BUDGET_LINEITEM, schBgtLItemPojo.getLineitemId()); 
		bgtLItemIdent.setOID(milestoneMap.getOID());
		bgtLItemIdent.setLineItemName(schBgtLItemPojo.getLineitemName());
	}
	public static void copyTOEventIdent(LineitemBean schBgtLItemPojo,EventNameIdentfier eventNameIdent,ObjectMapService objectMapService){
		eventNameIdent.setPK(schBgtLItemPojo.getLineItemFkEvent());
		ObjectMap milestoneMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, schBgtLItemPojo.getLineItemFkEvent()); 
		eventNameIdent.setOID(milestoneMap.getOID());
		EventAssocJB eventAssoc = new EventAssocJB();
		eventAssoc.setEvent_id(schBgtLItemPojo.getLineItemFkEvent());
		eventAssoc.getEventAssocDetails();
		eventNameIdent.setEventName(eventAssoc.getName());	
	}

	public static void copyToSchBudgetPojo(BudgetPojo budgetPojo,BudgetBean schBgtPojo,UserBean callingUser,List<Issue> validationIssues,ResponseHolder responseHolder) throws ValidationException{

	schBgtPojo.setBudgetCFlag(budgetPojo.getBUDGET_CALFLAG());
	schBgtPojo.setBudgetCombinedFlag(budgetPojo.getBUDGET_COMBFLAG());
	schBgtPojo.setBudgetDelFlag(budgetPojo.getBUDGET_DELFLAG());
	try{//TODO BUDGET_CURRENCY is required, but if we can't find a code, what to do?
		schBgtPojo.setBudgetCurrency(
				StringUtil.integerToString(dereferenceSchCode(budgetPojo.getBUDGET_CURRENCY(), CodeCache.CODE_BUDGET_CURRENCY, callingUser)));
	}catch(CodeNotFoundException e){
		validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "BUDGET_CURRENCY Code Not Found: "));
	}
	try{//TODO BUDGET_STATUS is required, but if we can't find a code, what to do?
		schBgtPojo.setBudgetCodeListStatus(
				StringUtil.integerToString(dereferenceSchCode(budgetPojo.getPK_CODELST_STATUS(), CodeCache.CODE_BUDGET_STATUS, callingUser)));
	}catch(CodeNotFoundException e){
		validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "BUDGET_STATUS Code Not Found: "));
	}
	if(schBgtPojo.getBudgetTemplate()==null){
	try{//TODO BUDGET_CURRENCY is required, but if we can't find a code, what to do?
		schBgtPojo.setBudgetTemplate(
				StringUtil.integerToString(dereferenceSchCode(budgetPojo.getBUDGET_TEMPLATE(), CodeCache.CODE_BUDGET_TEMPLATE, callingUser)));
	}catch(CodeNotFoundException e){
		validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "BUDGET_TEMPLATE Code Not Found: "));
	}
	}
	schBgtPojo.setBudgetdesc(budgetPojo.getBUDGET_DESC());
	schBgtPojo.setBudgetRights(budgetPojo.getBUDGET_RIGHTS());
	schBgtPojo.setBudgetRScope(budgetPojo.getBUDGET_RIGHTSCOPE());
	schBgtPojo.setBudgetSFlag(budgetPojo.getBUDGET_SITEFLAG());
	schBgtPojo.setBudgetType(budgetPojo.getBUDGET_TYPE());
	schBgtPojo.setIpAdd(IP_ADDRESS_FIELD_VALUE);
	schBgtPojo.setBudgetCreator(StringUtil.integerToString(callingUser.getUserId()));
	schBgtPojo.setBudgetAccountId(callingUser.getUserAccountId());
	schBgtPojo.setCreator(StringUtil.integerToString(callingUser.getUserId()));
	if(schBgtPojo.getBudgetCurrency()==null ){
		validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	SVC.Svc_Budget_Currency_Mandatory));
	}
	if(schBgtPojo.getBudgetCodeListStatus()==null){
		validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	SVC.Svc_Budget_Status_Mandatory));
	}else{
	schBgtPojo.setBudgetStatus(budgetPojo.getPK_CODELST_STATUS().getCode());
	}
	if(schBgtPojo.getBudgetTemplate()==null){
		validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	SVC.Svc_Budget_Template_Mandatory));
	}
	if(schBgtPojo.getBudgetType()==null){
		validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	SVC.Svc_Budget_Type_Mandatory));
	}
	if(schBgtPojo.getBudgetStatus()==null){
		validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"Budget Status may not be null."));
	}
	if(schBgtPojo.getBudgetSFlag()==null){
		validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"Budget Site Flag may not be null either 0 or 1."));
		}else if(schBgtPojo.getBudgetSFlag().equals("1")){
			schBgtPojo.setBudgetSiteId(callingUser.getUserSiteId());
		}
	if (validationIssues.size() > 0){
		((ResponseHolder) responseHolder)
		.getIssues().addAll(validationIssues); 
		
		if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
		throw new ValidationException();
	}
	
	}
	
	public static void copyToSchBudgetCalPojo(BgtCalPojo budgetCalPojo,BudgetcalBean schBgtCalPojo,UserBean callingUser,List<Issue> validationIssues,ResponseHolder responseHolder) throws ValidationException{
		schBgtCalPojo.setClFlag(budgetCalPojo.getBGTCAL_CLINICFLAG());
		schBgtCalPojo.setClOverHead(StringUtil.doubleToString(budgetCalPojo.getBGTCAL_CLINICOHEAD()));
		schBgtCalPojo.setBudgetDelFlag(budgetCalPojo.getBGTCAL_DELFLAG());
		schBgtCalPojo.setBudgetDiscount(StringUtil.doubleToString(budgetCalPojo.getBGTCAL_DISCOUNT()));
		schBgtCalPojo.setBudgetDiscountFlag(StringUtil.integerToString(budgetCalPojo.getBGTCAL_DISCOUNTFLAG()));
		schBgtCalPojo.setBudgetExcldSOCFlag(StringUtil.integerToString(budgetCalPojo.getBGTCAL_EXCLDSOCFLAG()));
		schBgtCalPojo.setBudgetFrgBenefit(StringUtil.doubleToString(budgetCalPojo.getBGTCAL_FRGBENEFIT()));
		schBgtCalPojo.setBudgetFrgFlag(StringUtil.integerToString(budgetCalPojo.getBGTCAL_FRGFLAG()));
		schBgtCalPojo.setBudgetIndirectCost(StringUtil.doubleToString(budgetCalPojo.getBGTCAL_INDIRECTCOST()));
		schBgtCalPojo.setBudgetProtType(budgetCalPojo.getBGTCAL_PROTTYPE());
		schBgtCalPojo.setBudgetSPOverHead(StringUtil.doubleToString(budgetCalPojo.getBGTCAL_SP_OVERHEAD()));
		schBgtCalPojo.setBudgetSPFlag(StringUtil.integerToString(budgetCalPojo.getBGTCAL_SP_OVERHEAD_FLAG()));
		schBgtCalPojo.setSpFlag(budgetCalPojo.getBGTCAL_SPONSORFLAG());
		schBgtCalPojo.setSpOverHead(StringUtil.doubleToString(budgetCalPojo.getBGTCAL_SPONSOROHEAD()));
		schBgtCalPojo.setCreator(StringUtil.integerToString(callingUser.getUserId()));
		schBgtCalPojo.setBudgetDiscontinueCost(budgetCalPojo.getGRAND_DISC_COST());
		schBgtCalPojo.setBudgetDiscontinueTotalCost(budgetCalPojo.getGRAND_DISC_TOTALCOST());
		schBgtCalPojo.setBudgetFringeCost(budgetCalPojo.getGRAND_FRINGE_COST());
		schBgtCalPojo.setBudgetFringeTotalCost(budgetCalPojo.getGRAND_FRINGE_TOTALCOST());
		schBgtCalPojo.setBudgetIndirectCost(budgetCalPojo.getGRAND_IND_COST());
		schBgtCalPojo.setBudgetIndirectTotalCost(budgetCalPojo.getGRAND_IND_TOTALCOST());
		schBgtCalPojo.setBudgetResearchCost(budgetCalPojo.getGRAND_RES_COST());
		schBgtCalPojo.setBudgetResearchSponsorAmount(budgetCalPojo.getGRAND_RES_SPONSOR());
		schBgtCalPojo.setBudgetResearchTotalCost(budgetCalPojo.getGRAND_RES_TOTALCOST());
		schBgtCalPojo.setBudgetResearchVariance(budgetCalPojo.getGRAND_RES_VARIANCE());
		schBgtCalPojo.setBudgetResViariance(budgetCalPojo.getGRAND_RES_VIARIANCE());
		schBgtCalPojo.setBudgetSalaryCost(budgetCalPojo.getGRAND_SALARY_COST());
		schBgtCalPojo.setBudgetSalaryTotalCost(budgetCalPojo.getGRAND_SALARY_TOTALCOST());
		schBgtCalPojo.setBudgetSOCCost(budgetCalPojo.getGRAND_SOC_COST());
		schBgtCalPojo.setBudgetSOCSponsorAmount(budgetCalPojo.getGRAND_SOC_SPONSOR());
		schBgtCalPojo.setBudgetSOCTotalCost(budgetCalPojo.getGRAND_SOC_TOTALCOST());
		schBgtCalPojo.setBudgetSOCVariance(budgetCalPojo.getGRAND_SOC_VARIANCE());
		schBgtCalPojo.setBudgetTotalCost(budgetCalPojo.getGRAND_TOTAL_COST());
		schBgtCalPojo.setBudgetTotalSponsorAmount(budgetCalPojo.getGRAND_TOTAL_SPONSOR());
		schBgtCalPojo.setBudgetTotalTotalCost(budgetCalPojo.getGRAND_TOTAL_TOTALCOST());
		schBgtCalPojo.setBudgetTotalVariance(budgetCalPojo.getGRAND_TOTAL_VARIANCE());
		schBgtCalPojo.setIpAdd(IP_ADDRESS_FIELD_VALUE);
		
		if(schBgtCalPojo.getBudgetDelFlag()==null){
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"Budget Calendar DELFLAG may not be null. Either Y/N"));
		}
		
		if (validationIssues.size() > 0){
			((ResponseHolder) responseHolder)
			.getIssues().addAll(validationIssues); 
			
			if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
			throw new ValidationException();
		}
		
		}
	
	public static void copyToSchBudgetSecPojo(BudgetSectionPojo budgetSecPojo,BgtSectionBean schBgtSecPojo,UserBean callingUser,List<Issue> validationIssues,ResponseHolder responseHolder) throws ValidationException{
		schBgtSecPojo.setBgtSectionDelFlag(budgetSecPojo.getBGTSECTION_DELFLAG());
		schBgtSecPojo.setBgtSectionNotes(budgetSecPojo.getBGTSECTION_NOTES());
		schBgtSecPojo.setBgtSectionPatNo(budgetSecPojo.getBGTSECTION_PATNO());
		schBgtSecPojo.setBgtSectionPersonlFlag(budgetSecPojo.getBGTSECTION_PERSONLFLAG());
		schBgtSecPojo.setBgtSectionSequence(budgetSecPojo.getBGTSECTION_SEQUENCE());
		schBgtSecPojo.setBgtSectionType(budgetSecPojo.getBGTSECTION_TYPE());
		schBgtSecPojo.setBgtSectionVisit(budgetSecPojo.getBGTSECTION_VISIT());
		schBgtSecPojo.setCreator(StringUtil.integerToString(callingUser.getUserId()));
		schBgtSecPojo.setIpAdd(IP_ADDRESS_FIELD_VALUE);
		schBgtSecPojo.setSocCostGrandTotal(budgetSecPojo.getSOC_COST_GRANDTOTAL());
		schBgtSecPojo.setSocCostSponsorAmount(budgetSecPojo.getSOC_COST_SPONSOR());
		schBgtSecPojo.setSocCostTotal(budgetSecPojo.getSOC_COST_TOTAL());
		schBgtSecPojo.setSocCostVariance(budgetSecPojo.getSOC_COST_VARIANCE());
		schBgtSecPojo.setSrtCostGrandTotal(budgetSecPojo.getSRT_COST_GRANDTOTAL());
		schBgtSecPojo.setSrtCostSponsorAmount(budgetSecPojo.getSRT_COST_SPONSOR());
		schBgtSecPojo.setSrtCostTotal(budgetSecPojo.getSRT_COST_TOTAL());
		schBgtSecPojo.setSrtCostVariance(budgetSecPojo.getSRT_COST_VARIANCE());		
		if (validationIssues.size() > 0){
			((ResponseHolder) responseHolder)
			.getIssues().addAll(validationIssues); 
			
			if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
			throw new ValidationException();
			}
		
		}
	
	public static void copyToSchBudgetLItemPojo(BudgetLineItemPojo budgetLItemPojo,LineitemBean schLItemPojo,UserBean callingUser,List<Issue> validationIssues,ResponseHolder responseHolder) throws ValidationException{
		schLItemPojo.setCreator(StringUtil.integerToString(callingUser.getUserId()));
		schLItemPojo.setIpAdd(IP_ADDRESS_FIELD_VALUE);
		schLItemPojo.setLineitemAppIndirects(budgetLItemPojo.getLINEITEM_APPLYINDIRECTS());
		schLItemPojo.setLineitemAppInFuture(budgetLItemPojo.getLINEITEM_APPLYINFUTURE());
		schLItemPojo.setLineitemApplyPatientCount(budgetLItemPojo.getLINEITEM_APPLYPATIENTCOUNT());
		schLItemPojo.setLineitemCDM(budgetLItemPojo.getLINEITEM_CDM());
		schLItemPojo.setLineitemClinicNOfUnit(budgetLItemPojo.getLINEITEM_CLINICNOFUNIT());
		schLItemPojo.setLineitemCptCode(budgetLItemPojo.getLINEITEM_CPTCODE());
		schLItemPojo.setLineitemDelFlag(budgetLItemPojo.getLINEITEM_DELFLAG());
		schLItemPojo.setLineitemDesc(budgetLItemPojo.getLINEITEM_DESC());
		schLItemPojo.setLineitemInCostDisc(budgetLItemPojo.getLINEITEM_INCOSTDISC());
		schLItemPojo.setLineitemInPerSec(budgetLItemPojo.getLINEITEM_INPERSEC());
		schLItemPojo.setLineitemInvCost(budgetLItemPojo.getLINEITEM_INVCOST());
		schLItemPojo.setLineitemNotes(budgetLItemPojo.getLINEITEM_NOTES());
		schLItemPojo.setLineitemOtherCost(budgetLItemPojo.getLINEITEM_OTHERCOST());
		schLItemPojo.setLineitemParentId(budgetLItemPojo.getLINEITEM_PARENTID());
		schLItemPojo.setLineitemRepeat(budgetLItemPojo.getLINEITEM_REPEAT());
		schLItemPojo.setLineItemSequence(budgetLItemPojo.getLINEITEM_SEQ());
		schLItemPojo.setLineItemSponsorAmount(budgetLItemPojo.getLINEITEM_SPONSORAMOUNT());
		schLItemPojo.setLineitemSponsorUnit(budgetLItemPojo.getLINEITEM_SPONSORUNIT());
		schLItemPojo.setLineitemStdCareCost(budgetLItemPojo.getLINEITEM_STDCARECOST());
		schLItemPojo.setLineitemTMID(budgetLItemPojo.getLINEITEM_TMID());
		schLItemPojo.setLineitemTotalCost(budgetLItemPojo.getLINEITEM_TOTALCOST());
		schLItemPojo.setLineItemVariance(budgetLItemPojo.getLINEITEM_VARIANCE());
		schLItemPojo.setLineitemSubCostFlag(budgetLItemPojo.getSUBCOST_ITEM_FLAG());
		try{//TODO BUDGET_CURRENCY is required, but if we can't find a code, what to do?
			schLItemPojo.setLineitemCategory(
					StringUtil.integerToString(dereferenceSchCode(budgetLItemPojo.getPK_CODELST_CATEGORY(), CodeCache.CODE_LINEITEM_CATEGORY, callingUser)));
		}catch(CodeNotFoundException e){
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "LineItem Category Code Not Found: "));
		}
		try{//TODO BUDGET_CURRENCY is required, but if we can't find a code, what to do?
			schLItemPojo.setLineItemCostType(
					StringUtil.integerToString(dereferenceSchCode(budgetLItemPojo.getPK_CODELST_COST_TYPE(), CodeCache.CODE_LINEITEM_COST_DESC, callingUser)));
		}catch(CodeNotFoundException e){
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "LineItem Cost Desc Code Not Found: "));
		}
		if(schLItemPojo.getLineitemCategory()==null ){
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"LineItem Category may not be null."));
		}
		if(schLItemPojo.getLineItemCostType()==null){
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"LineItem Cost Desc may not be null."));
		}
		
		if (validationIssues.size() > 0){
			((ResponseHolder) responseHolder)
			.getIssues().addAll(validationIssues); 
			
			if (logger.isDebugEnabled()) logger.debug("Validation Issues found for BudgetDetails");
			throw new ValidationException();
			}
		
		}
}
