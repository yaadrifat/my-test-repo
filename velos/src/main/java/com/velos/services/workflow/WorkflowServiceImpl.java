package com.velos.services.workflow;

import java.util.ArrayList;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.StudyDao;
import com.velos.eres.gems.business.Workflow;
import com.velos.eres.gems.business.WorkflowInstance;
import com.velos.eres.gems.business.WorkflowInstanceJB;
import com.velos.eres.gems.business.WorkflowJB;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.CRUDAction;
import com.velos.services.CompletedAction;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.util.ObjectLocator;
@Stateless
@Remote(WorkflowService.class)
public class WorkflowServiceImpl extends AbstractService implements WorkflowService {
	private static Logger logger = Logger.getLogger(WorkflowServiceImpl.class.getName());
	@EJB
	private ObjectMapService objectMapService;
	@EJB
	private UserAgentRObj userAgent;
	@EJB
	GrpRightsAgentRObj groupRightsAgent;
	@Resource 
	private SessionContext sessionContext;
	int instanceFlag = 0;
	@Override
	public ResponseHolder calculateWorkFlow(StudyIdentifier studyIdentifier) throws OperationException {
		// TODO Auto-generated method stub
		try{
		int entityId = 0;
		if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
				&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
				&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
		{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to calculateWorkFlow"));
		throw new OperationException();
		}
		GroupAuthModule groupAuth = 
			new GroupAuthModule(callingUser, groupRightsAgent);

		Integer manageProtocolPriv = 
			groupAuth.getAppManageProtocolsPrivileges();
		System.out.println("manageProtocolPriv:::--->"+manageProtocolPriv);
		boolean hasNewManageProt = 
			GroupAuthModule.hasEditPermission(manageProtocolPriv);
		System.out.println("hasNewManageProt:::--->"+hasNewManageProt);
		if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
		if (!hasNewManageProt){
			response.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to edit studies"));
			throw new AuthorizationException("User Not Authorized to edit studies");
		}
		if(CFG.Workflow_FlexStudy_WfTypes==null || CFG.Workflow_FlexStudy_WfTypes.equals("") ||
				CFG.Workflows_Enabled==null || CFG.Workflows_Enabled.equals("")){
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Missing workflow configuration "));
			throw new OperationException();
		}
		String workflow_types = CFG.Workflow_FlexStudy_WfTypes;
		ArrayList<Integer> wfInstanceIds = new ArrayList<Integer>();
		
		Integer entityIdObj=locateStudyPKLcl(studyIdentifier);
		
		if(entityIdObj == null || entityIdObj.equals(0)){
			addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdentifier.getOID() + " StudyNumber " + studyIdentifier.getStudyNumber()));
			throw new OperationException();
		}else{
			entityId=entityIdObj.intValue();
		String [] workflowTypes = (StringUtil.isEmpty(workflow_types))? 
				null : workflow_types.split(",");

		if (workflowTypes.length <= 0 || CFG.Workflow_FlexStudy_WfTypes.equals("[Workflow_FlexStudy_WfTypes]")) {
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No workflow types configured"));
			throw new OperationException();
		}

		String moreParams = String.valueOf(entityId);
		
		Integer prevWorkflowInstanceId = 0;

		if ("Y".equals(CFG.Workflows_Enabled)) {

			for(int wfIndx=0; wfIndx < workflowTypes.length; wfIndx++){
				String workflowType = workflowTypes[wfIndx];
				if (StringUtil.isEmpty(workflowType)) continue;

				if (entityId < 0) continue;
				
				WorkflowJB workflowJB = new WorkflowJB();
				Workflow wf = new Workflow();
				wf = workflowJB.findWorkflowByType(workflowType);
				
				if (null == wf) {//add issue
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Configured workflow type does not exists"));
					throw new OperationException();
				}
				
				WorkflowInstanceJB workflowInstanceJB = new WorkflowInstanceJB();
				WorkflowInstance workflowInstance = workflowInstanceJB.getWorkflowInstanceStatus(String.valueOf(entityId), workflowType);

				Integer workflowInstanceId = (null == workflowInstance)? 0 : workflowInstance.getPkWfInstanceId();
				
				Integer workflowInstanceStatus = (null == workflowInstance)? 0 : workflowInstance.getWfIStatusFlag();
				workflowInstanceStatus = (null == workflowInstanceStatus)? 0 : workflowInstanceStatus;
		
				if (workflowInstanceId == null || workflowInstanceId == 0) {
					workflowInstanceId = workflowInstanceJB.startWorkflowInstance(String.valueOf(entityId), workflowType);
					if (entityId > 0){
						workflowInstanceJB.updateWorkflowInstance(String.valueOf(entityId), workflowType, moreParams);
						if (workflowInstanceId != null || workflowInstanceId != 0) {
							//nextDestination = workflowInstanceJB.fetchNextTask(entityId, workflowType);
						}
					}
					instanceFlag = 1;
				} else {
					wfInstanceIds.add(workflowInstanceId);
					if (entityId > 0){
						workflowInstanceJB.updateWorkflowInstance(String.valueOf(entityId), workflowType, moreParams);
						instanceFlag = 2;
						//nextDestination = workflowInstanceJB.fetchNextTask(entityId, workflowType);
					}
				}
		
	}
		}
		if(instanceFlag==1){
			response.addAction(new CompletedAction(studyIdentifier, CRUDAction.CREATE));
		}else if(instanceFlag==2){
			response.addAction(new CompletedAction(studyIdentifier, CRUDAction.UPDATE));
		}
		}
		
	}
	catch(OperationException e){
		sessionContext.setRollbackOnly();
		if (logger.isDebugEnabled()) logger.debug("WorkflowServiceImpl calculateWorkFlow", e);
		throw new OperationRolledBackException(response.getIssues());
	}
	catch(Throwable t){
		sessionContext.setRollbackOnly();
		addUnknownThrowableIssue(t);
		
		if (logger.isDebugEnabled()) logger.debug("WorkflowServiceImpl calculateWorkFlow", t);
		throw new OperationRolledBackException(response.getIssues());

	}
	return response;
}
	private Integer locateStudyPKLcl(StudyIdentifier studyIdentifier) 
			throws OperationException{
				//Virendra:#6123,added OID in if clause
				if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
						&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
						&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to addStudyPatientStatus"));
					throw new OperationException();
				}
				Integer studyPK = 0;
				if (studyIdentifier.getStudyNumber() != null){
					if(studyIdentifier.getPK()!=null){
						StudyDao studyDao = new StudyDao();
						if(studyDao.isStudyPKexists(studyIdentifier.getPK())){
							System.out.println("Inside if true::::");
							studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier,objectMapService);
						}
					}else{
						WorkflowJB workflowJB = new WorkflowJB();
						studyPK = workflowJB.getStudyIDbyNumber(studyIdentifier.getStudyNumber());
						if(studyPK != null && studyPK.intValue() != 0)
						{
							objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK);
						}
					}
				}else{
					if(studyIdentifier.getPK()!=null){
						StudyDao studyDao = new StudyDao();
						System.out.println("After Changes");
						if(studyDao.isStudyPKexists(studyIdentifier.getPK())){
							System.out.println("Inside if true::::"+studyIdentifier.getPK());
							studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier,objectMapService);
						}
					}else{
						studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier,objectMapService);
					}
					if(studyPK != null && studyPK.intValue() != 0)
					{
						objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK);
					}
				}
				
						
				System.out.println("Inside locateStudyPK after studyPKFromIdentifier");
				if (studyPK == null || studyPK.intValue() == 0){
					StringBuffer errorMessage = new StringBuffer("Study not found for:"); 
					if(studyIdentifier.getOID() != null && studyIdentifier.getOID().length() > 0 )
					{
						errorMessage.append(" OID: " + studyIdentifier.getOID()); 
					}
					if(studyIdentifier.getStudyNumber() != null && studyIdentifier.getStudyNumber().length() > 0)
					{
						errorMessage.append( " Study Number: " + studyIdentifier.getStudyNumber()); 
					}
					if(studyIdentifier.getPK()!=null && studyPK.intValue() == 0)
					{
						errorMessage.append(" Pk: " + studyIdentifier.getPK());
					}
					System.out.println("errorMessage::::"+errorMessage);
					addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, errorMessage.toString()));
					throw new OperationException();
				}
				return studyPK;
			}
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}
}
