/**
 * Created On Aug 4, 2011
 */
package com.velos.services.calendar;

import java.util.Comparator;

import com.velos.services.model.CalendarEvent;

/**
 * @author Kanwaldeep
 *
 */
public class EventSequenceCompartor implements Comparator<CalendarEvent> {

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(CalendarEvent event1, CalendarEvent event2) {
		
		if(event1.getCalendarEventSummary().getSequence() != null && event2.getCalendarEventSummary().getSequence() != null)
		{
			if(event1.getCalendarEventSummary().getSequence() < event2.getCalendarEventSummary().getSequence()) return -1; 
			if(event1.getCalendarEventSummary().getSequence() > event2.getCalendarEventSummary().getSequence()) return 1; 
		}
		return 0;
	}

}
