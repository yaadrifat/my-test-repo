/**
 * 
 */
package com.velos.services.map;

/**
 * @author dylan
 *
 */
public class MultipleObjectsFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5609890803889431808L;

	public MultipleObjectsFoundException() {
		super();
	}

	public MultipleObjectsFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public MultipleObjectsFoundException(String message) {
		super(message);
	}

	public MultipleObjectsFoundException(Throwable cause) {
		super(cause);
	}

}
