/**
 * 
 */
package com.velos.services.map;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author dylan
 *
 */

@XmlRootElement(name="ObjectMap")
@XmlType(name="ObjectMapType")
@Entity
@Table(name = "ER_OBJECT_MAP")
//added named query "findTablePkByObjectId" by virendra
@NamedQueries({
	@NamedQuery(name = "findMapByTablePK", 
		query = "SELECT OBJECT(objectMap) FROM ObjectMap objectMap where UPPER(trim(table_name)) = UPPER(trim(:table_name)) AND table_pk = :table_pk"),
	@NamedQuery(name = "findTablePkByObjectId", 
		query = "SELECT tablePK FROM ObjectMap where UPPER(trim(OID)) = UPPER(trim(:system_id))"),
	@NamedQuery(name = "findObjectMapByObjectId", 
		query = "SELECT OBJECT(objectMap) FROM ObjectMap objectMap where UPPER(trim(OID)) = UPPER(trim(:system_id))")
})
public class ObjectMap implements Serializable{
	public static final String NAMED_QUERY_TABLE_PK = "findMapByTablePK";
	public static final String QUERY_PARAM_TABLE_PK= "table_pk";	
	public static final String QUERY_PARAM_TABLE_NAME= "table_name";
	//virendra
	public static final String QUERY_PARAM_OID= "system_id";
	public static final String NAMED_QUERY_TABLE_PK_FROM_OID = "findTablePkByObjectId";
	public static final String NAMED_QUERY_OBJECTMAP_FROM_OID = "findObjectMapByObjectId"; 
	/**
	 * 
	 */
	private static final long serialVersionUID = 4216591432231487690L;

	private String tableName;
	
	private int tablePK;
	
	private String OID;

	@Column(name = "table_name")
	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	@Column (name="table_pk")
	public int getTablePK() {
		return tablePK;
	}

	public void setTablePK(int tablePK) {
		this.tablePK = tablePK;
	}
	
    @Id
	@Column (name="system_id")
	public String getOID() {
		return OID;
	}

	public void setOID(String OID) {
		this.OID = OID;
	}
	
	
	
}
