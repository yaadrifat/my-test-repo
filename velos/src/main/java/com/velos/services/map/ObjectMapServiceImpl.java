/**
 * 
 */
package com.velos.services.map;

import java.util.ArrayList;
import java.util.UUID;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.velos.services.model.ObjectInfo;

/**
 * @author dylan
 *
 */
@Stateless
public class ObjectMapServiceImpl  implements ObjectMapService{
	private static Logger logger = Logger.getLogger("ObjectMapServiceImpl");
	@PersistenceContext(unitName = "eres")
    protected EntityManager em;
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ObjectMap getObjectMapFromId(String id) 
	throws 
		MultipleObjectsFoundException{
		
		return (ObjectMap)em.find(ObjectMap.class, id);

	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	protected ObjectMap createObjectMap(String tableName, Integer tablePK)throws InterruptedException {
		logger.debug("========================================INSIDE  createObjectMap() METHOD ====================");
			
		ObjectMap objectMap = new ObjectMap();
		UUID uuid = UUID.randomUUID();
		objectMap.setOID(uuid.toString());
		objectMap.setTableName(tableName);
		objectMap.setTablePK(tablePK);
		logger.debug("========================================OBJECT MAP VALUES SET AND TRYING TO SAVE RECORD IN ER_OBJECT_MAP TABLE. TABLE NAME::"+tableName+" TABLE PK::"+tablePK);
		em.persist(objectMap);
		logger.debug("========================================OBJECT MAP SAVED IN ER_OBJECT_MAP TABLE. TABLE NAME::"+tableName+" TABLE PK::"+tablePK);
		//String externalId = uuid.toString();
		return objectMap;
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ObjectMap getOrCreateObjectMapFromPK(String tableName, Integer tablePK) {
		Query query = em
		.createNamedQuery(ObjectMap.NAMED_QUERY_TABLE_PK);
		query.setParameter(ObjectMap.QUERY_PARAM_TABLE_NAME, tableName);
		query.setParameter(ObjectMap.QUERY_PARAM_TABLE_PK, tablePK);
		ObjectMap map = null; 
		try{
			logger.debug("========================  ================GETTING RECORD FROM ER_OBJECT_MAP FOR TBALE_NAME::"+tableName+" AND TABLE_PK::"+tablePK);
			ArrayList<ObjectMap> list = (ArrayList<ObjectMap>) query.getResultList();
			if (list == null || list.size() == 0) { 
				map = createObjectMap(tableName, tablePK); 
			}else{
				map =  (ObjectMap)query.getResultList().get(0);//Fix::[Bug 27222] duplicate OID getting created in ER_Object Map table
				}
			logger.debug("========================================GOT RECORD FROM ER_OBJECT_MAP FOR TBALE_NAME::"+tableName+" AND TABLE_PK::"+tablePK);
		}catch(NoResultException nre)
		{
			try {
				logger.debug("========================================TRYING TO SAVE RECORD FOR NoResultException CATCH BLOCK FOR TBALE_NAME::"+tableName+" AND TABLE_PK::"+tablePK);

				map = createObjectMap(tableName, tablePK);
				logger.debug("========================================RECORD SAVED SUCCESSFULLY FOR NoResultException CATCH BLOCK FOR TBALE_NAME::"+tableName+" AND TABLE_PK::"+tablePK);

			} catch (InterruptedException e) {
				
				logger.error("========================================ERROR IN SAVING RECORD FROM NoResultException CATCH BLOCK FOR TBALE_NAME::"+tableName+" AND TABLE_PK::"+tablePK);
				e.printStackTrace();
			} 
			
		}catch(EntityNotFoundException etn)
		{
			try {
				logger.debug("========================================TRYING TO SAVE RECORD FOR EntityNotFoundException CATCH BLOCK FOR TBALE_NAME::"+tableName+" AND TABLE_PK::"+tablePK);

				map = createObjectMap(tableName, tablePK);
				logger.debug("========================================RECORD SAVED SUCCESSFULLY FOR EntityNotFoundException CATCH BLOCK FOR TBALE_NAME::"+tableName+" AND TABLE_PK::"+tablePK);

			} catch (InterruptedException e) {
				logger.error("========================================ERROR IN SAVING RECORD FROM EntityNotFoundException CATCH BLOCK FOR TBALE_NAME::"+tableName+" AND TABLE_PK::"+tablePK);

				e.printStackTrace();
			} 
			
		}catch(NonUniqueResultException nue)
		{
			logger.error("No Unique record found for"+ tableName + " " +tablePK, nue);
			
		}catch(Throwable t){
			logger.error("Error occured while getOrCreateObjectMapFromPK:: TableName "+tableName+ " TablePK "+ tablePK,t);
		}
		return map;

	}
	//added by virendra to retrieve tablePk from OID
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Integer getObjectPkFromOID(String OID){
		Query query = em
		.createNamedQuery(ObjectMap.NAMED_QUERY_TABLE_PK_FROM_OID);
		query.setParameter(ObjectMap.QUERY_PARAM_OID, OID);
		Integer tablePk = 0;
		try{
			tablePk =  (Integer)query.getSingleResult();
		}
		catch(EntityNotFoundException etn){
			logger.error("No tablePk found for OID"+ OID );
		}
		catch(NonUniqueResultException nue){
			logger.error("No Unique record found for"+ OID );
		}
		catch(Throwable t){
			logger.error("Error occured while retrieving tablePk for OID:: TableName "+OID);
		}
		return tablePk;
		
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ObjectInfo getObjectInfoFromOID(String OID){
		ObjectMap oMap = null;
		ObjectInfo oInfo = null;
		
		Query query = em.createNamedQuery(ObjectMap.NAMED_QUERY_OBJECTMAP_FROM_OID);
		query.setParameter(ObjectMap.QUERY_PARAM_OID, OID);

		try{
			oMap =  (ObjectMap)query.getSingleResult();
			oInfo = new ObjectInfo();
			oInfo.setTablePk(oMap.getTablePK());
			oInfo.setTableName(oMap.getTableName());
		}
		catch(EntityNotFoundException etn){
			logger.error("No ObjectMap found for OID "+ OID );
		}
		catch(NonUniqueResultException nue){
			logger.error("No Unique record found for OID "+ OID );
		}
		catch(Throwable t){
			logger.error("Error occured while retrieving ObjectMap for OID: "+OID);
		}
		return oInfo;
		
	}
	
}
