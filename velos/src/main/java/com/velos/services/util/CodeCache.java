/**
 * 
 */
package com.velos.services.util;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.velos.services.CodeNotFoundException;
import com.velos.services.model.Code;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.service.util.EJBUtil;
import com.velos.esch.business.common.SchCodeDao;

/**
 * @author dylan
 *
 */
public class CodeCache implements Cacheable{
	public static final String CODE_TYPE_DISEASE_SITE = "disease_site";
	public static final String CODE_TYPE_DIVISION = "study_division";
	public static final String CODE_TYPE_THERAPUTIC_AREA = "tarea";
	public static final String CODE_TYPE_GENDER = "gender";
	public static final String CODE_TYPE_RACE = "race";
	public static final String CODE_TYPE_ETHNICITY = "ethnicity";
	public static final String CODE_TYPE_PHASE = "phase";
	public static final String CODE_TYPE_RESEARCH_TYPE = "research_type";
	public static final String CODE_TYPE_PRIMARY_PURPOSE = "studyPurpose";
	public static final String CODE_TYPE_STUDY_SCOPE = "studyscope";
	public static final String CODE_TYPE_STUDY_TYPE = "study_type";
	public static final String CODE_TYPE_BLINDING = "blinding";
	public static final String CODE_TYPE_RANDOMIZATION = "randomization";
	public static final String CODE_TYPE_SPONSOR_NAME = "sponsor";
	public static final String CODE_TYPE_ROLE = "role";
	public static final String CODE_TYPE_TEAM_STATUS = "teamstatus";
	public static final String CODE_TYPE_STUDY_ORG_TYPE = "studySiteType";
	public static final String CODE_TYPE_MARITAL_STATUS = "marital_st";
	//virendra
	public static final String CODE_TYPE_STATUS = "studystat";
	public static final String CODE_TYPE_STATUS_TYPE = "studystat_type";
	public static final String CODE_TYPE_REVIEW_BOARD = "rev_board";
	public static final String CODE_TYPE_OUTCOME = "studystat_out";
	public static final String CODE_TYPE_STUDY_ID_TYPE="studyidtype";
	public static final String CODE_TYPE_Patient_ID_TYPE="peridtype";
	public static final String CODE_TYPE_PATIENT_SURVIVAl_STATUS="patient_status";
	//kanwal
	public static final String CODE_TYPE_PRIMARY_SPECIALTY="prim_sp";
	public static final String CODE_TYPE_JOB_TYPE="job_type";
	
	public static final String CODE_TYPE_PATIENT_STUDY_STATUS_REASON_ENROLL_APPR = "enroll_appr";
	public static final String CODE_TYPE_PATIENT_STUDY_STATUS_REASON_ENROLL_PENDING = "enroll_pending";
	public static final String CODE_TYPE_PATIENT_STUDY_STATUS_REASON_ENROLL_DENIED = "enroll_denied";
	public static final String CODE_TYPE_PATIENT_STUDY_STATUS_REASON_FOLLOWUP = "followup";
	public static final String CODE_TYPE_PATIENT_STUDY_STATUS_REASON_OFFSTUDY = "offstudy";
	public static final String CODE_TYPE_PATIENT_STUDY_STATUS_REASON_OFFTREAT = "offtreat";
	
	public static final String CODE_TYPE_PATIENT_STUDY_STATUS_REASON_INFCONSENT = "infConsent";
	public static final String CODE_TYPE_PATIENT_STUDY_STATUS_REASON_BEGSTUDYACT = "begStudyAct";
	public static final String CODE_TYPE_PATIENT_STUDY_STATUS_REASON_SCRFAIL = "scrfail";
	public static final String CODE_TYPE_PATIENT_STUDY_STATUS_REASON_ENROLLED = "enrolled";
	public static final String CODE_TYPE_PATIENT_STUDY_STATUS_REASON_ENDBILLING = "endBilling";
	public static final String CODE_TYPE_PATIENT_STUDY_STATUS_REASON_COMPSTUDY = "completedStudy";
	public static final String CODE_TYPE_PATIENT_STUDY_STATUS_REASON_ACTIVESTUDY = "activeStudy";
	public static final String CODE_TYPE_PATIENT_STUDY_STATUS_REASON_WITHDRAWN = "withdrawn";
	public static final String CODE_TYPE_PATIENT_STUDY_STATUS_REASON_NOTMEETELIGIBLE = "notmeetEligible";
	public static final String CODE_TYPE_PATIENT_STUDY_STATUS_REASON_PHYSDICRETION = "physdiscretion";
	public static final String CODE_TYPE_PATIENT_STUDY_STATUS_REASON_WITHDREWCON = "withdrewCon";

	//FOR SCH_CODELST(PATIENT SCHEDULE OPERATIONS)
	public static final String CODE_TYPE_COVERAGE_TYPE="coverage_type";
	public static final String CODE_TYPE_EVENT_STATUS_TYPE="eventstatus";
	public static final String CODE_TYPE_STUDY_CALENDAR_STATUS = "calStatStd"; 
	public static final String CODE_TYPE_CURRENCY = "currency"; 
	public static final String CODE_TYPE_COST_DESC = "cost_desc";
	public static final String CODE_TYPE_STUDY_CALENDAR_STATUS_LIB = "L";
	public static final String CODE_EVENT_TYPE = "A";
	public static final String CODE_STUDY_CALENDAR_TYPE="P";
	public static final String CODE_STUDY_PATIENT_CALENDAR_TYPE="P";
	public static final String CODE_CALENDAR_BUDGET_STATUS_TYPE="budget_stat";
	public static final String CODE_EVENT_LIBRARY_TYPE = "lib_type";
	
	//For PAT PROT
	public static final String CODE_TYPE_PATPROT_TREATMENT_LOCATION = "treatloc"; 
	public static final String CODE_TYPE_PATPROT_EVALUATION_FLAG = "ptst_eval_flag"; 
	public static final String CODE_TYPE_PATPROT_EVALATION_STATUS = "ptst_eval"; 
	public static final String CODE_TYPE_PATPROT_INEVALATION_STATUS="ptst_ineval"; 
	public static final String CODE_TYPE_PATPROT_SURVIVAL_STATUS="ptst_survival"; 
	public static final String CODE_TYPE_PATPROT_STUDY_RELATED_DEATH="ptst_dth_stdrel";
	public static final String CODE_TYPE_PATPROT_STATUS = "patStatus"; 
	public static final String CODE_TYPE_PATPROT_SCREENING_OUTCOME = "screenOutcome"; 
	
	public static final String CODE_TYPE_PATIENT_DEATH_CAUSE="pat_dth_cause"; 
	
	public static final String CODE_TYPE_TIMEZONE="timezone"; 
	
	
	
	// Form related codes
	public static final String CODE_TYPE_FORM_STATUS = "frmlibstat"; 
	// Linked Form Status codes
	public static final String CODE_TYPE_LINKED_FORM_STATUS = "frmstat"; 
	public static final String CODE_TYPE_FILLED_FORM_STATUS = "fillformstat"; 
	//public static final String CODE_TYPE_FORM_
	
	//Milestone related codes
	public static final String CODE_TYPE_MILESTONE_STATUS = "milestone_stat";
	public static final String CODE_TYPE_VM_RULE = "VM";
	public static final String CODE_TYPE_EM_RULE = "EM";
	public static final String CODE_TYPE_PAY_TYPE = "milepaytype";
	public static final String CODE_TYPE_PAY_FOR = "milePayfor";
	//Study Version codes
	public static final String CODE_TYPE_STUDY_VERCAT = "studyvercat";
	public static final String CODE_TYPE_STUDY_VER = "studyvertype";
	public static final String CODE_TYPE_VER_STAT = "versionStatus";
	//Study Budget codes
	public static final String CODE_BUDGET_CURRENCY = "currency";
	public static final String CODE_BUDGET_STATUS = "budget_stat";
	public static final String CODE_LINEITEM_COST_DESC = "cost_desc";
	public static final String CODE_LINEITEM_CATEGORY = "category";
	public static final String CODE_BUDGET_TEMPLATE = "budget_template";
	//INDIDE Codes
	public static final String CODE_INDIDE_GRANTOR = "INDIDEGrantor";
	public static final String CODE_INDIDE_HOLDER = "INDIDEHolder";
	public static final String CODE_INDIDE_ACCESS = "INDIDEAccess";
	public static final String CODE_INDIDE_PROGRAM = "ProgramCode";
	//NIHGRant Codes
	public static final String CODE_NIH_FUNDMECH = "NIHFundMech";
	public static final String CODE_NIH_INST = "NIHInstCode";
	public static final String CODE_TYPE_USER = "user";
	
	public static final String CODE_TYPE_ORG = "org";
	
	public static final String CODE_TYPE_ORGG="site_type";
	
	public static final String CODE_NET_RELNTYP="relnshipTyp";
	
	/** Cache of CodeDao instances. Key is the name of the code list to fetch **/
	private static Hashtable<String, CodeDao> codesTable = new Hashtable<String, CodeDao>(); 
	private static Hashtable<String, SchCodeDao> schCodesTable = new Hashtable<String, SchCodeDao>();
	
	private static CodeCache codeCache = null;
	
	private CodeCache(){
		
	}
	
	public static synchronized CodeCache getInstance(){
		if (codeCache == null){ 
			codeCache = new CodeCache();
		}
		return codeCache;
	}
	
	/**
	 * Retrieve a code list, fetched by the name and custom. The 
	 * name and custom combination are concatenated to represent a unique
	 * entry in the CodeDao cache 
	 * @param codeType corresponds to er_codelst.codelst_type
	 * @param custom corresponds to er_codelst.custom
	 * @return
	 */
	public CodeDao getCodes(String codeType, String custom, int accountId){
		String key = createCustomKey(codeType, custom, accountId);
		
		CodeDao codeDao = codesTable.get(key);
		if (codeDao == null){
			codeDao = new CodeDao();
			codeDao.getCodeValues(codeType, custom);
			codesTable.put(codeType, codeDao);
		}
		return codeDao;
	}
	
	/**
	 * Retrieve a code list, fetched by the name
	 * 
	 * @param codeType corresponds to er_codelst.codelst_type
	 * @return
	 */
	public CodeDao getCodes(String codeType, int accountId){

		CodeDao codeDao = codesTable.get(codeType);
		if (codeDao == null){
			codeDao = new CodeDao();
			codeDao.getCodeValues(codeType);
			codesTable.put(codeType, codeDao);
		}
		return codeDao;
	}
	
	public  Code getCodeSubTypeByPK(String codeType, String custom, Integer codePK, Integer accountId){
		CodeDao codeDao= getCodes(codeType, custom, accountId);
		return getCodeSubTypeByPK(codeDao, codePK);
	}
	public  Code getCodeSubTypeByPK(String codeType, String codePKStr, int accountId) throws NumberFormatException{
		if (codePKStr == null || codePKStr.length() < 1) return null;
		
		Integer codePK = Integer.valueOf(codePKStr);
		return getCodeSubTypeByPK(codeType, codePK, accountId);
	}
	public  Code getCodeSubTypeByPK(String codeType, Integer codePK, int accountId){
		CodeDao codeDao = getCodes(codeType, accountId);
		return getCodeSubTypeByPK(codeDao, codePK);
	}
	
	public  List<Code> getCodeSubTypeByMultiPKs(String codeType, String[] codePKs, int accountId){
		ArrayList<Code> codes = new ArrayList<Code>();
		if (codePKs.length > 0){
			CodeDao codeDao = getCodes(codeType, accountId);
			
			for (String codePK : codePKs){
				Code code = getCodeSubTypeByPK(codeDao, EJBUtil.stringToInteger(codePK));
				codes.add(code);
			}
			
		}
		
		return codes;
	}
	
	public  Code getCodeSubTypeByPK(CodeDao codeDao, Integer codePK){
	
		ArrayList<Integer> pks = codeDao.getCId();
		if (pks.size() == 0) return null;
		Integer pkLoc = null;
		for (int x=0; x < pks.size(); x++){
			int pk = ((Integer)pks.get(x)).intValue();
			if (pk == codePK){
				pkLoc = new Integer(x);
				break;
			}
			
		}
		if (pkLoc == null) return null;
		
		
		Code code = new Code();
		code.setType(codeDao.getCType());
		code.setCode((String)codeDao.getCSubType().get(pkLoc));
		code.setDescription((String)codeDao.getCDesc().get(pkLoc));
		return code;
		
	}
	
	public Integer dereferenceCode(Code code, String codeType, int accountId) throws CodeNotFoundException{
		
		CodeDao codeDAO = getCodes( codeType, accountId);
		//Integer codePK = codeDAO.getCodeId(codeType, code.getCode());
		Integer codePK = null;
		ArrayList codePKs = codeDAO.getCId();
		ArrayList codeSubTypes = codeDAO.getCSubType();
		ArrayList codeDescriptions = codeDAO.getCDesc();
		for (int x=0; x<codeSubTypes.size();x++){
			//first check if the codeType is a match
			String codeSubTypeStr = (String)codeSubTypes.get(x);
			if (codeSubTypeStr != null &&
					codeSubTypeStr.trim().equalsIgnoreCase(code.getCode().trim())){
				
				codePK = (Integer)codePKs.get(x);
				break;
			}

		}
		
		if (codePK == null || codePK < 1){
			throw new CodeNotFoundException(code);
		}

		return codePK;
		
	}
	
	
	public String createCustomKey(String codeType, String custom, Integer accountId){
		return accountId.toString() + codeType + "_" + custom;
	}
	
	public synchronized void flush(){
		codesTable.clear();
		schCodesTable.clear(); 
	}
	
	//METHODS ADDED FOR SCH_CODELST BY VIRENDRA TO SUPPORT CODE OPERATIONS RELATED TO SCH_CODELST
	public  Code getSchCodeSubTypeByPK(String codeType, Integer codePK, int accountId){
		SchCodeDao schCodeDao = getSchCodes(codeType, accountId);
		return getSchCodeSubTypeByPK(schCodeDao, codePK);
	}
	public SchCodeDao getSchCodes(String codeType, int accountId){

		SchCodeDao schCodeDao = schCodesTable.get(codeType);
		if (schCodeDao == null){
			schCodeDao = new SchCodeDao();
			schCodeDao.getCodeValues(codeType);
			schCodesTable.put(codeType, schCodeDao);
		}
		return schCodeDao;
	}
	public  Code getSchCodeSubTypeByPK(SchCodeDao schCodeDao, Integer codePK){
		
		ArrayList<Integer> pks = schCodeDao.getCId();
		if (pks.size() == 0) return null;
		Integer pkLoc = null;
		for (int x=0; x < pks.size(); x++){
			int pk = ((Integer)pks.get(x)).intValue();
			if (pk == codePK){
				pkLoc = new Integer(x);
				break;
			}
			
		}
		if (pkLoc == null) return null;
		
		
		Code code = new Code();
		code.setType(schCodeDao.getCType());
		code.setCode((String)schCodeDao.getCSubType().get(pkLoc));
		code.setDescription((String)schCodeDao.getCDesc().get(pkLoc));
		return code;
		
	}
	
	public Integer dereferenceSchCode(Code code, String codeType, int accountId) throws CodeNotFoundException{
		
		SchCodeDao codeDAO = getSchCodes( codeType, accountId);
		//Integer codePK = codeDAO.getCodeId(codeType, code.getCode());
		Integer codePK = null;
		ArrayList codePKs = codeDAO.getCId();
		ArrayList codeSubTypes = codeDAO.getCSubType();
		ArrayList codeDescriptions = codeDAO.getCDesc();
		for (int x=0; x<codeSubTypes.size();x++){
			//first check if the codeType is a match
			String codeSubTypeStr = (String)codeSubTypes.get(x);
			if (codeSubTypeStr != null &&
					codeSubTypeStr.trim().equalsIgnoreCase(code.getCode().trim())){
				
				codePK = (Integer)codePKs.get(x);
				break;
			}

		}
		
		if (codePK == null || codePK < 1){
			throw new CodeNotFoundException(code);
		}

		return codePK;
		
	}

	//Raman
	private CodeDao getTzCodes(String codeType, Integer accountId) {

		CodeDao codeDAO = codesTable.get(codeType);
		if (codeDAO == null) {
			codeDAO = new CodeDao();
			codeDAO.getCodeValuesForTimeZone(codeType);
			codesTable.put(codeType, codeDAO);
		}
		return codeDAO;
	}
	
	//Raman
	public Integer dereferenceTzCode(Code code, String codeType, Integer accountId) throws CodeNotFoundException{
		
		CodeDao codeDAO = getTzCodes( codeType, accountId);
		
		//Integer codePK = codeDAO.getCodeId(codeType, code.getCode());
		Integer codePK = null;
		ArrayList codePKs = codeDAO.getCId();
		ArrayList codeSubTypes = codeDAO.getCSubType();
		ArrayList codeDescriptions = codeDAO.getCDesc();
		for (int x=0; x<codeSubTypes.size();x++){
			//first check if the codeType is a match
			String codeSubTypeStr = (String)codeSubTypes.get(x);
			if (codeSubTypeStr != null &&
					codeSubTypeStr.trim().equalsIgnoreCase(code.getCode().trim())){
				
				codePK = (Integer)codePKs.get(x);
				break;
			}

		}
		
		if (codePK == null || codePK < 1){
			throw new CodeNotFoundException(code);
		}

		return codePK;
		
	}
	
	//Raman
	public  Code getTzCodeSubTypeByPK(CodeDao codeDao, Integer codePK){
		
		ArrayList<Integer> pks = codeDao.getCId();
		if (pks.size() == 0) return null;
		Integer pkLoc = null;
		for (int x=0; x < pks.size(); x++){
			int pk = ((Integer)pks.get(x)).intValue();
			if (pk == codePK){
				pkLoc = new Integer(x);
				break;
			}
			
		}
		if (pkLoc == null) return null;
		
		
		Code code = new Code();
		code.setType(codeDao.getCType());
		code.setCode((String)codeDao.getCSubType().get(pkLoc));
		code.setDescription((String)codeDao.getCDesc().get(pkLoc));
		return code;
		
	}
	
	//Raman
	public  Code getTzCodeSubTypeByPK(String codeType, Integer codePK, int accountId){
		CodeDao codeDao = getTzCodes(codeType, accountId);
		return getTzCodeSubTypeByPK(codeDao, codePK);
	}
}
