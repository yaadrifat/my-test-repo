package com.velos.services.util;

import com.velos.eres.service.util.EnvUtil;
import com.velos.eres.service.util.StringUtil;

public class HibernateUtilHelper {
	
	public static final String VDA_CONFIG   = "hibernate-vda.cfg.xml";
    public static final String ERES_CONFIG = "hibernate-eres.cfg.xml";
    public static final String ESCH_CONFIG = "hibernate-esch.cfg.xml";
    public static final String PROPERTY_FILE = "erespool.properties";
    public static String eHome = "";
    
    private HibernateUtilHelper(){}
	
	static {
        synchronized(HibernateUtilHelper.class) {
            // Get ERES_HOME where the Hibernate Configuration files are stored
            try {
                eHome = EnvUtil.getEnvVariable("ERES_HOME");
            } catch(Exception e) {
                eHome = "";
            }
            if (eHome == null || eHome.trim().equals("%ERES_HOME%")) {
                eHome = StringUtil.trueValue(System.getProperty("ERES_HOME"));
            }
            eHome = eHome.trim();
                        
        }
    }
	
	public static String getVDAConfigFilePath() {
        String fullName = eHome+VDA_CONFIG;
        return fullName;
    }
	public static String getERESConfigFilePath() {
        String fullName = eHome+ERES_CONFIG;
        return fullName;
    }
	public static String getESCHConfigFilePath() {
        String fullName = eHome+ESCH_CONFIG;
        return fullName;
    }
	public static String getPropertyFilePath() {
        String fullName = eHome+PROPERTY_FILE;
        return fullName;
    }
}
