/**
 * Created On Dec 9, 2011
 */
package com.velos.services.util;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Kanwaldeep
 *
 */
public class EnumUtil {
	
	private EnumUtil(){}
	
	private static HashMap<Object, Map<String, ? extends Enum>> enumMap = new HashMap<Object, Map<String, ? extends Enum>>(); 
	
	
	/**This method is used to return the enum for String value.
	 *  Make sure that any enumType you are using to create Map between 
	 *  enum and string value has its toString() overridden in order to get value for which you want to lookup enum. 
	 * @param <T>
	 * @param enumType
	 * @param name
	 * @return
	 */
	public static <T extends Enum<T>> T getEnumType(Class<T> enumType, String name)
	{
		if(name == null) return null; 
		Map<String, T> lookup = null; 
	
		if(enumMap.containsKey(enumType))
		{
			 lookup = (Map<String, T>) enumMap.get(enumType); 
			
		}else
		{
			lookup = new HashMap<String,T>(); 
			for(T s: EnumSet.allOf(enumType))
				lookup.put(s.toString(), s);	
			enumMap.put(enumType, lookup); 
		}
	
		return (T) lookup.get(name.trim()); 		 
	}
	
	
	

}
