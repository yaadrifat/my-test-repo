/**
 * 
 */
package com.velos.services;

import java.util.List;

/**
 * @author dylan
 *
 */
public class AuthenticationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8051694117009931646L;

	public AuthenticationException (String message){
		super(message);
	}
	
}
