package com.velos.services.patientdemographics;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.services.OperationException;

public class PatientFacilityDAO extends CommonDAO
{
	private static final long serialVersionUID = 7198180460967944211L;
	private static Logger logger = Logger.getLogger(PatientFacilityDAO.class);
	
	public boolean isFacilityIdExists(String facilityId, int siteId) throws OperationException{
		String sqlStr ="Select count(pk_patfacility)  as facilitycount  " +
				"from er_patfacility " +
				"where fk_site = ? and lower(PAT_FACILITYID)= ? ";
	
		PreparedStatement pstmt = null;
		Connection conn = null;
		int recordCount = 0;
		try {
			conn = getConnection();

			if (logger.isDebugEnabled())
				logger.debug(" sql:" + sqlStr);
			 
			pstmt = conn.prepareStatement(sqlStr);
			pstmt.setInt(1,siteId);
			pstmt.setString(2,facilityId);
			
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				recordCount = rs.getInt("facilitycount");
			}
			System.out.println("Facility ID Count for Duplicates: " + recordCount);
			if(recordCount == 0){
				return false;
			} 
		} catch (Throwable t) {
			t.printStackTrace(); 
			throw new OperationException();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception localException) {
				localException.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception localException1) {
				localException1.printStackTrace();
			}
		}
		return true;
	}
	
	public int getPatientFacilityID(Integer organizationID, Integer patientID) throws OperationException
	{
		String sqlString = "select pk_patfacility from er_patfacility where fk_per = ? and fk_site = ?"; 
		PreparedStatement stmt = null; 
		ResultSet rs = null; 
		Connection conn = null; 
		int patfacility = 0; 
		
		try{
			conn = getConnection(); 
			stmt = conn.prepareStatement(sqlString); 
			
			stmt.setInt(1, patientID); 
			stmt.setInt(2, organizationID); 
			
			rs = stmt.executeQuery(); 
			
			while(rs.next())
			{
				patfacility = rs.getInt(1); 
			}
			
			
		}catch(SQLException sqe)
		{
			sqe.printStackTrace(); 
		}finally{

			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					returnConnection(conn); 
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return patfacility; 
	}
	

}
