package com.velos.services.patientdemographics;


import static javax.ejb.TransactionAttributeType.REQUIRED;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.patProt.impl.PatProtBean;
import com.velos.eres.business.person.impl.PersonBean;
import com.velos.eres.business.site.impl.SiteBean;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.patFacilityAgent.PatFacilityAgentRObj;
import com.velos.eres.service.patProtAgent.PatProtAgentRObj;
import com.velos.eres.service.perIdAgent.PerIdAgentRObj;
import com.velos.eres.service.personAgent.PersonAgentRObj;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Code;
import com.velos.services.model.Codes;
import com.velos.services.model.NVPair;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.Patient;
import com.velos.services.model.PatientDemographics;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientIdentifiers;
import com.velos.services.model.PatientSurvivalStatus;
import com.velos.services.model.PatientSurvivalStatuses;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;

@Stateless
@Remote(PatientDemographicsService.class)
public class PatientDemographicsServiceImpl 
extends AbstractService 
implements PatientDemographicsService{
@EJB
GrpRightsAgentRObj groupRightsAgent;
@EJB
private PersonAgentRObj personAgent;
@Resource 
private SessionContext sessionContext;
@EJB
private UserAgentRObj userAgent;
@EJB
private SiteAgentRObj siteAgent;
@EJB
private ObjectMapService objectMapService;
@EJB
private PatProtAgentRObj patProtAgent;
@EJB
private UserSiteAgentRObj userSiteAgent;
@EJB
private PatFacilityAgentRObj patFacilityAgent;

@EJB
private PatientFacilityService patFacilityService; 
@EJB
private PerIdAgentRObj perIdAgent;


Integer personPK=0;
Boolean hasViewManageCompletePatient= true;

private static Logger logger = Logger.getLogger(PatientDemographicsServiceImpl.class.getName());
/**
 * @author Virendra
 * Implementation class for Monitoring Services operations.
 *
 */
public PatientDemographics getPatientDemographics(PatientIdentifier patientId)
			throws OperationException {
	
		try{
		    //fetching personPK from objectlocator with patientId
		    //		PersonDao personDao = new PersonDao();
			
			if(patientId == null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifier is required")); 
				throw new OperationException(); 
			}
			
			if(((patientId.getOID() == null || patientId.getOID().length() == 0) && (patientId.getPK() == null || patientId.getPK()<=0)) 
					&& ((patientId.getPatientId() == null || patientId.getPatientId().length() == 0)
							|| (patientId.getOrganizationId() == null) ) )
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifier with OID or PatientPK or PatientID and OrganizationIdentifier is required"));
				throw new OperationException(); 
			}
		    Integer personPK = null; 
		    
		    try{
		    	personPK = ObjectLocator.personPKFromPatientIdentifier(
		    			callingUser, 
		    			patientId, 
		    			objectMapService);
		    } catch (MultipleObjectsFoundException e) {
		    	addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, 
		    			"Multiple Patients found")); 
		    	throw new OperationException(); 
		    }


		    //if person not found, add an issue
		    if(personPK == null || personPK ==0){
		        addIssue(new Issue(
		                IssueTypes.PATIENT_NOT_FOUND, 
		                "Patient not found for code: "+patientId.getPatientId()+ " for given OrganizationId"));
		        throw new OperationException();
		    }
            PersonBean personBean = personAgent.getPersonDetails(personPK);
            if (personBean == null){
                addIssue(new Issue(
                        IssueTypes.PATIENT_NOT_FOUND, 
                        "Patient not found for code: "+patientId.getPatientId()+" OID: "+patientId.getOID()));
                throw new OperationException();
            }
            if (callingUser.getUserAccountId() == null
                    || !callingUser.getUserAccountId().equals(personBean.getPersonAccount())){
                addIssue(new Issue(
                        IssueTypes.PATIENT_NOT_FOUND, 
                        "Patient not found for OID: "+patientId.getOID()));
                throw new OperationException();
            }
            
			//checking for group rights
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);
			
			Integer managePatientPriv =groupAuth.getAppManagePatientsPrivileges();//getAppManagePatientsPrivileges();//getAppManagePatientsPrivileges();
			boolean hasViewManagePatient = 
				GroupAuthModule.hasViewPermission(managePatientPriv);
			
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + managePatientPriv);
			if (!hasViewManagePatient){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User not authorized to view patient data"));
				throw new AuthorizationException("User not authorized to view patient data");
			}
			
			// #6228: Check patient facility rights; this is the max rights among all facilities of this patient
			Integer patFacilityRights = userSiteAgent.getUserPatientFacilityRight(callingUser.getUserId(), personPK);
			if (!GroupAuthModule.hasViewPermission(patFacilityRights)) {
                if (logger.isDebugEnabled()) logger.debug("User not authorized to access data of this patient");
                addIssue(
                        new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, 
                                "User not authorized to access data of this patient"));
                throw new AuthorizationException("User not authorized to access data of this patient");
			}
			
			ArrayList<Integer> lstPatProtPK = ObjectLocator.getPatProtPkFromPersonPk(personPK, objectMapService);
			for(Integer patProtPK: lstPatProtPK){
				
				PatProtBean patProtBean =   patProtAgent.getPatProtDetails(patProtPK);
				Integer studyPK =  EJBUtil.stringToInteger(patProtBean.getPatProtStudyId());
				
				//Virendra:#6073
				TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
				int viewPatientPrivileges = teamAuthModule.getPatientViewDataPrivileges();
				if(!TeamAuthModule.hasViewPermission(viewPatientPrivileges) ){
					hasViewManageCompletePatient=false;
					break;
				}
				
			}
			//Checking if user has view patient complete data rights
			//if then show complete patient data otherwise show only
			//PHI data.
			Integer managePatientPrivComplete = 
				groupAuth.getAppViewCompletePatientDataPrivileges();
			Integer CompleteDetailsAccessRight = 
				personAgent.getPatientCompleteDetailsAccessRight(
						callingUser.getUserId(),Integer.parseInt(callingUser.getUserGrpDefault()) ,
						personPK);
			//setting boolean hasViewManageCompletePatient to false if user does not have
			//privilege to view complete data.
			if(managePatientPrivComplete == 3 || CompleteDetailsAccessRight== 0 ){
				hasViewManageCompletePatient=false;
			}
				
			PatientDemographics patientDemographics =
				marshalPatientDemographics(personBean);
			
			return patientDemographics;
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
			throw new OperationException(t);
		}

	}
	/**
	 * marshals patientDemographics with input of personBean
	 * @param personBean
	 * @return
	 * @throws OperationException
	 * @throws AuthorizationException
	 * @throws ParseException
	 */

	private PatientDemographics marshalPatientDemographics(PersonBean personBean) 
	throws 
	OperationException, 
	AuthorizationException, ParseException{
		
		PatientDemographics returnPatientDemographics = new PatientDemographics();
		Integer callingUserAccountId = Integer.valueOf(callingUser.getUserAccountId());
		CodeCache codeCache = CodeCache.getInstance();
		//Integer userAccountPK = EJBUtil.stringToInteger(callingUser.getUserAccountId());
		PatientIdentifier patiIdentifier = marshallPatientIdentifier(personBean);
		//patiIdentifier.setPatientId(personBean.getPersonPId());
		//patiIdentifier.setOID(personBean.get)
		returnPatientDemographics.setPatientIdentifier(patiIdentifier);
				
		Collection<NVPair> morePatientDetailsCollection =
			MorePatientDetailsDAO.getMorePatientDetails(
					personBean.getPersonPKId(),
					EJBUtil.stringToInteger(callingUser.getUserGrpDefault())).values();
		
		if (morePatientDetailsCollection != null){
			List<NVPair> morePatientDetailsList = new ArrayList<NVPair>();
			
			morePatientDetailsList.addAll(morePatientDetailsCollection);

			returnPatientDemographics.setMorePatientDetails(morePatientDetailsList);
		}
		
		Code cdSurvivalStatus = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATIENT_SURVIVAl_STATUS, personBean.getPersonStatus(), callingUserAccountId);
		returnPatientDemographics.setSurvivalStatus(cdSurvivalStatus);
		
		Code cdEthnicity = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_ETHNICITY, personBean.getPersonEthnicity(), callingUserAccountId);
		returnPatientDemographics.setEthnicity(cdEthnicity);
		
		Code cdRace = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_RACE, personBean.getPersonRace(), callingUserAccountId);
		returnPatientDemographics.setRace(cdRace);
		
	//	Integer personLocationPK = EJBUtil.stringToInteger(personBean.getPersonLocation());
		Integer personLocationPK = StringUtil.stringToInteger(personBean.getPersonLocation());
		SiteBean siteBean =siteAgent.getSiteDetails(personLocationPK);
		OrganizationIdentifier patOrg= marshallOrganizationIdentifier(siteBean);
		returnPatientDemographics.setOrganization(patOrg);
		
		returnPatientDemographics.setRegistrationDate(personBean.getPersonRegDt());
		
		if(hasViewManageCompletePatient){
			
		returnPatientDemographics.setFirstName(personBean.getPersonFname());
		returnPatientDemographics.setLastName(personBean.getPersonLname());
		returnPatientDemographics.setMiddleName(personBean.getPersonMname());
		returnPatientDemographics.setZipCode(personBean.getPersonZip());
				
		Code cdGender = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_GENDER, personBean.getPersonGender(), callingUserAccountId);
		returnPatientDemographics.setGender(cdGender);
		
		Code cdMaritalStatus = codeCache.getCodeSubTypeByPK("marital_st", personBean.getPersonMarital(), callingUserAccountId);
		returnPatientDemographics.setMaritalStatus(cdMaritalStatus);
		
		Code cdBloodGroup = codeCache.getCodeSubTypeByPK("bloodgr", personBean.getPersonBloodGr(), callingUserAccountId);
		returnPatientDemographics.setBloodGroup(cdBloodGroup);
					
		Code cdTimeZone = codeCache.getTzCodeSubTypeByPK(CodeCache.CODE_TYPE_TIMEZONE, StringUtil.stringToInteger(personBean.getTimeZoneId()), callingUserAccountId);
		returnPatientDemographics.setTimeZone(cdTimeZone);
		
		Code cdEducation = codeCache.getCodeSubTypeByPK("education", personBean.getPersonEducation(), callingUserAccountId);
		returnPatientDemographics.setEducation(cdEducation);
		
		Code cdEmployement = codeCache.getCodeSubTypeByPK("employment", personBean.getPersonEmployment(), callingUserAccountId);
		returnPatientDemographics.setEmployment(cdEmployement);
		
		if(personBean.getPersonMultiBirth() != null && personBean.getPersonMultiBirth().equals("1"))
		{
			returnPatientDemographics.setSSNNot(PatientDemographics.enumSSNNotAppl.YES);
		}
		else
		{
			returnPatientDemographics.setSSNNot(PatientDemographics.enumSSNNotAppl.NO);
		}
		
		// _commaPersAth
		
		if(personBean.getPersonAddEthnicity() != null )
		{	
			List<Code> addethninicity = new ArrayList<Code>();
			String _commaPersAth=personBean.getPersonAddEthnicity();
			String []args=_commaPersAth.split(",");
			Code code=null;
			CodeDao  codeDao=new CodeDao();
			for(String s:args)
			{
				code=new Code();
				code.setCode(codeDao.getCodeSubtype(StringUtil.stringToNum(s)));
				code.setDescription(codeDao.getCodeDescription(StringUtil.stringToNum(s)));
				code.setType(CodeCache.CODE_TYPE_ETHNICITY);
				addethninicity.add(code);
			}
			Codes codes = new Codes();
			codes.addAll(addethninicity);
			returnPatientDemographics.setAdditionalEthnicity(codes);
		}
		// _commaPersRace
		
		if(personBean.getPersonAddRace()!= null)
		{	
			List<Code> addRace = new ArrayList<Code>();
			String _commaPersRace=personBean.getPersonAddRace();
			String []argss=_commaPersRace.split(",");
			Code cd=null;
			CodeDao  codDao=new CodeDao();
			for(String s:argss)
			{
				cd=new Code();
				cd.setCode(codDao.getCodeSubtype(StringUtil.stringToNum(s)));
				cd.setDescription(codDao.getCodeDescription(StringUtil.stringToNum(s)));
				cd.setType(CodeCache.CODE_TYPE_RACE);
				addRace.add(cd);
			}
			Codes cods = new Codes();
			cods.addAll(addRace);
			returnPatientDemographics.setAdditionalRace(cods);
		}
		
		Code cdDeathCause = codeCache.getCodeSubTypeByPK("pat_dth_cause", personBean.getPatDthCause(), callingUserAccountId);
		returnPatientDemographics.setDeathCause(cdDeathCause);
		returnPatientDemographics.setDeathCauseOther(personBean.getDthCauseOther());

		
		returnPatientDemographics.setDateOfBirth(personBean.getPersonDb());
		returnPatientDemographics.setDeathDate(personBean.getPersonDeathDt());
		returnPatientDemographics.seteMail(personBean.getPersonEmail());
		returnPatientDemographics.setAddress1(personBean.getPersonAddress1());
		returnPatientDemographics.setAddress2(personBean.getPersonAddress2());
		returnPatientDemographics.setCity(personBean.getPersonCity());
		returnPatientDemographics.setState(personBean.getPersonState());
		returnPatientDemographics.setCounty(personBean.getPersonCounty());
		returnPatientDemographics.setCountry(personBean.getPersonCountry());
		returnPatientDemographics.setHomePhone(personBean.getPersonHphone());
		returnPatientDemographics.setWorkPhone(personBean.getPersonBphone());
		returnPatientDemographics.setPatFacilityId(personBean.getPatientFacilityId());
		returnPatientDemographics.setNotes(personBean.getPersonNotes());
		returnPatientDemographics.setSSN(personBean.getPersonSSN());
		
		}else{
			returnPatientDemographics.setFirstName("*");
			returnPatientDemographics.setLastName("*");
			returnPatientDemographics.setMiddleName("*");
	//		Code cdGender = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_GENDER, personBean.getPersonGender(), callingUserAccountId);
	//		returnPatientDemographics.setGender();
			
	//		Code cdEthnicity = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_ETHNICITY, personBean.getPersonEthnicity(), callingUserAccountId);
	//		returnPatientDemographics.setEthnicity(cdEthnicity);
	//		
	//		Code cdRace = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_RACE, personBean.getPersonRace(), callingUserAccountId);
	//		returnPatientDemographics.setRace(cdRace);
			
			//returnPatientDemographics.setDateOfBirth(Date.valueOf("*"));
			
	//		Code cdSurvivalStatus = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATIENT_SURVIVAl_STATUS, personBean.getPersonStatus(), callingUserAccountId);
	//		returnPatientDemographics.setSurvivalStatus(cdSurvivalStatus);
			
			//returnPatientDemographics.setDeathDate(Date.valueOf("*"));
			returnPatientDemographics.seteMail("*");
			returnPatientDemographics.setAddress1("*");
			returnPatientDemographics.setAddress2("*");
			returnPatientDemographics.setCity("*");
			returnPatientDemographics.setState("*");
			returnPatientDemographics.setCounty("*");
			returnPatientDemographics.setCountry("*");
			returnPatientDemographics.setHomePhone("*");
			returnPatientDemographics.setWorkPhone("*");
			returnPatientDemographics.setSSN("*");
					
			
			Code cdGender = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_GENDER, personBean.getPersonGender(), callingUserAccountId);
			returnPatientDemographics.setGender(cdGender);
			
			Code cdMaritalStatus = codeCache.getCodeSubTypeByPK("marital_st", personBean.getPersonMarital(), callingUserAccountId);
			returnPatientDemographics.setMaritalStatus(cdMaritalStatus);
			
			Code cdBloodGroup = codeCache.getCodeSubTypeByPK("bloodgr", personBean.getPersonBloodGr(), callingUserAccountId);
			returnPatientDemographics.setBloodGroup(cdBloodGroup);
						
			Code cdTimeZone = codeCache.getTzCodeSubTypeByPK(CodeCache.CODE_TYPE_TIMEZONE, StringUtil.stringToInteger(personBean.getTimeZoneId()), callingUserAccountId);
			returnPatientDemographics.setTimeZone(cdTimeZone);
			
			Code cdEducation = codeCache.getCodeSubTypeByPK("education", personBean.getPersonEducation(), callingUserAccountId);
			returnPatientDemographics.setEducation(cdEducation);
			
			Code cdEmployement = codeCache.getCodeSubTypeByPK("employment", personBean.getPersonEmployment(), callingUserAccountId);
			returnPatientDemographics.setEmployment(cdEmployement);   
			
			if("1".equals(personBean.getPersonMultiBirth()))
			{
				returnPatientDemographics.setSSNNot(PatientDemographics.enumSSNNotAppl.YES);
			}
			else
			{
				returnPatientDemographics.setSSNNot(PatientDemographics.enumSSNNotAppl.NO);
			}
			
			// _commaPersAth
			
			if(personBean.getPersonAddEthnicity() != null )
			{	
				List<Code> addethninicity = new ArrayList<Code>();
				String _commaPersAth=personBean.getPersonAddEthnicity();
				String []args=_commaPersAth.split(",");
				Code code=null;
				CodeDao  codeDao=new CodeDao();
				for(String s:args)
				{
					code=new Code();
					code.setCode(codeDao.getCodeSubtype(StringUtil.stringToNum(s)));
					code.setDescription(codeDao.getCodeDescription(StringUtil.stringToNum(s)));
					code.setType(CodeCache.CODE_TYPE_ETHNICITY);
					addethninicity.add(code);
				}
				Codes codes = new Codes();
				codes.addAll(addethninicity);
				returnPatientDemographics.setAdditionalEthnicity(codes);
			}
			// _commaPersRace
			
			if(personBean.getPersonAddRace()!= null)
			{	
				List<Code> addRace = new ArrayList<Code>();
				String _commaPersRace=personBean.getPersonAddRace();
				String []argss=_commaPersRace.split(",");
				Code cd=null;
				CodeDao  codDao=new CodeDao();
				for(String s:argss)
				{
					cd=new Code();
					cd.setCode(codDao.getCodeSubtype(StringUtil.stringToNum(s)));
					cd.setDescription(codDao.getCodeDescription(StringUtil.stringToNum(s)));
					cd.setType(CodeCache.CODE_TYPE_RACE);
					addRace.add(cd);
				}
				Codes cods = new Codes();
				cods.addAll(addRace);
				returnPatientDemographics.setAdditionalRace(cods);
			}
			
			
			
	//		OrganizationIdentifier patOrg= new OrganizationIdentifier();
	//		Integer personLocationPK = EJBUtil.stringToInteger(personBean.getPersonLocation());
	//		SiteBean siteBean =siteAgent.getSiteDetails(personLocationPK);
	//		patOrg.setSiteName(siteBean.getSiteName());
	//		returnPatientDemographics.setOrganization(patOrg);
				
			returnPatientDemographics.setPatFacilityId("*");
	//		returnPatientDemographics.setRegistrationDate(personBean.getPersonRegDt());
			
		}
		
		return returnPatientDemographics;
	}
	/**
	 * marshalls patientIdentifier
	 * @param personBean
	 * @return
	 */
	private PatientIdentifier marshallPatientIdentifier(PersonBean personBean) {
		PatientIdentifier patientId = new PatientIdentifier(); 
		ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, personBean.getPersonPKId()); 
		patientId.setOID(map.getOID());
		patientId.setPK( personBean.getPersonPKId());
		patientId.setPatientId(personBean.getPersonPId()); 
		
		return patientId; 
	}
	/**
	 * marshalls OrganizationIdentifier
	 * @param siteBean
	 * @return
	 */
	private OrganizationIdentifier marshallOrganizationIdentifier(SiteBean siteBean) {
		OrganizationIdentifier orgId = new OrganizationIdentifier(); 
		ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, siteBean.getSiteId()); 
		orgId.setOID(map.getOID());
		orgId.setPK(siteBean.getSiteId());
		orgId.setSiteName(siteBean.getSiteName()); 
		
		return orgId; 
	}
	/* (non-Javadoc)
	 * @see com.velos.services.patientdemographics.PatientDemographicsService#createPatient(com.velos.services.model.PatientDemographics)
	 */
	@TransactionAttribute(REQUIRED) 
	public ResponseHolder createPatient(Patient patient)
			throws OperationException {
		
		try{
		
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent);
			int managePatients = authModule.getAppManagePatientsPrivileges();
			boolean hasNewPatientPermissions = GroupAuthModule.hasNewPermission(managePatients); 
			if(!hasNewPatientPermissions)
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User not authorized to add patient data"));
				throw new AuthorizationException("User not authorized to add patient data");
			}
			
			validate(patient);
			Map<String, Object> parameters = new HashMap<String, Object>(); 
			
			parameters.put("personAgent", personAgent);
			parameters.put("sessionContext", sessionContext); 
			parameters.put("userAgent", userAgent);
			parameters.put("siteAgent", siteAgent);
			parameters.put("objectMapService", objectMapService);
			parameters.put("patProtAgent", patProtAgent) ;
			parameters.put("userSiteAgent", userSiteAgent);
			parameters.put("patFacilityAgent", patFacilityAgent);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response); 
			parameters.put("perIdAgent", perIdAgent);

			PatientDemographicsHelper helper = new PatientDemographicsHelper(); 
			int patientPK = helper.createPatient(patient, parameters); 
			
			helper.persistsPatientMoreStudyDetails(patient,patientPK,parameters);


			ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, patientPK); 
	
			PatientIdentifier patientIdentifier = new PatientIdentifier(); 
			patientIdentifier.setOID(map.getOID()); 
			patientIdentifier.setPK(patientPK);
			patientIdentifier.setPatientId(patient.getPatientDemographics().getPatientCode()); 
			
			
			response.addObjectCreatedAction(patientIdentifier);


		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientDemographicsServiceImpl create", e);
			throw new OperationRolledBackException(response.getIssues());

		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientDemographicsServiceImpl create", t);
			throw new OperationRolledBackException(response.getIssues());

		}
		return response;
		
	}
	
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}
	@Override
	public PatientSurvivalStatuses getMPatientSurvivalStatus(PatientIdentifiers patientIdentifiers) throws OperationException 
	{
		try
		{
			if(patientIdentifiers == null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifiers field is required")); 
				throw new OperationException(); 
			}
			//checking for group rights
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);
			
			Integer managePatientPriv =groupAuth.getAppManagePatientsPrivileges();//getAppManagePatientsPrivileges();//getAppManagePatientsPrivileges();
			boolean hasViewManagePatient = 
				GroupAuthModule.hasViewPermission(managePatientPriv);
			
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + managePatientPriv);
			if (!hasViewManagePatient){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User not authorized to view patient data"));
				throw new AuthorizationException("User not authorized to view patient data");
			}
			Integer personPK = null; 
			PersonBean personBean=null;
			Integer patFacilityRights=null;
			PatientSurvivalStatuses patientSurvivalStatuses=new PatientSurvivalStatuses();
			PatientSurvivalStatus patientSurvivalStatus=null;
		    for(PatientIdentifier patientId:patientIdentifiers.getPatientIdentifier())
		    {
		    	if((patientId==null || (StringUtil.isEmpty(patientId.getOID())
						&& 	(StringUtil.isEmpty(patientId.getPatientId()) 
								|| (patientId.getOrganizationId()==null 
									|| (StringUtil.isEmpty(patientId.getOrganizationId().getOID())
										&& StringUtil.isEmpty(patientId.getOrganizationId().getSiteName())
										&& StringUtil.isEmpty(patientId.getOrganizationId().getSiteAltId())
										&& (patientId.getOrganizationId().getPK()==null || patientId.getOrganizationId().getPK()==0))))
						&&(patientId.getPK()==null || patientId.getPK()==0))))
				{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifier with OID or PatientPK or PatientID and OrganizationIdentifier is required"));
				throw new OperationException();
				}
		    	try
		    	{
			    	personPK = ObjectLocator.personPKFromPatientIdentifier(callingUser,patientId,objectMapService);
			    } 
		    	catch (MultipleObjectsFoundException e)
		    	{
			    	addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND,"Multiple Patients found")); 
			    	throw new OperationException(); 
			    }
		    	//if person not found, add an issue
			    if(personPK == null || personPK ==0)
			    {
			        addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND,"Patient not found for given details"));
			        throw new OperationException();
			    }
	            personBean = personAgent.getPersonDetails(personPK);
	            if (personBean == null)
	            {
			        addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND,"Patient not found for given details"));
				throw new OperationException();
				}
				patFacilityRights = userSiteAgent.getUserPatientFacilityRight(callingUser.getUserId(), personPK);
				if (!GroupAuthModule.hasViewPermission(patFacilityRights)) {
		            if (logger.isDebugEnabled()) logger.debug("User not authorized to access data of this patient");
		            addIssue(new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,"User not authorized to access data of this patient"));
		            throw new AuthorizationException("User not authorized to access data of this patient");
				}
				
				patientSurvivalStatus=new PatientSurvivalStatus();
				patientId.setPK(personPK);
				ObjectMap mapPerson = this.objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON,personPK);
				patientId.setOID(mapPerson.getOID());
				patientId.setPatientId(personBean.getPersonPId());
				OrganizationIdentifier organizationIdentifier=new OrganizationIdentifier();
				organizationIdentifier.setPK(StringUtil.stringToInteger(personBean.getPersonLocation()));
				ObjectMap mapOrg = this.objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION,StringUtil.stringToInteger(personBean.getPersonLocation()));
				organizationIdentifier.setOID(mapOrg.getOID());
				SiteBean sb= siteAgent.getSiteDetails(StringUtil.stringToInteger(personBean.getPersonLocation()));
				organizationIdentifier.setSiteName(""+sb.getSiteName());
				patientId.setOrganizationId(organizationIdentifier);
				patientSurvivalStatus.setPatientIdentifier(patientId);
				CodeDao codeDao=new CodeDao();
				//Bug#14413   Raman
				//Code survivalStatus=new Code(CodeCache.CODE_TYPE_PATIENT_SURVIVAl_STATUS, codeDao.getCodeSubtype(StringUtil.stringToNum(personBean.getPersonStatus())));
				Code survivalStatus=new Code(CodeCache.CODE_TYPE_PATIENT_SURVIVAl_STATUS, codeDao.getCodeSubtype(StringUtil.stringToNum(personBean.getPersonStatus())),codeDao.getCodeDescription(StringUtil.stringToNum(personBean.getPersonStatus())));
				patientSurvivalStatus.setSurvivalStatus(survivalStatus);
				patientSurvivalStatuses.addPatientSurvivalStatus(patientSurvivalStatus);
		    }
			return patientSurvivalStatuses;
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl getMPatientSurvivalStatus", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl getMPatientSurvivalStatus", t);
			throw new OperationException(t);
		}
	}
	
	public Patient getPatient(PatientIdentifier patientId)
	throws OperationException {
		try{
			if(patientId == null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifier is required")); 
				throw new OperationException(); 
			}

			if(((patientId.getOID() == null || patientId.getOID().length() == 0) && (patientId.getPK() == null || patientId.getPK()<=0)) 
					&& ((patientId.getPatientId() == null || patientId.getPatientId().length() == 0)
							|| (patientId.getOrganizationId() == null) ) )
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifier with OID or PatientPK or PatientID and OrganizationIdentifier is required"));
				throw new OperationException(); 
			}
			Integer personPK = null; 

			try{
				personPK = ObjectLocator.personPKFromPatientIdentifier(
						callingUser, 
						patientId, 
						objectMapService);
			} catch (MultipleObjectsFoundException e) {
				addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, 
						"Multiple Patients found")); 
				throw new OperationException(); 
			}


			//if person not found, add an issue
			if(personPK == null || personPK ==0){
				addIssue(new Issue(
						IssueTypes.PATIENT_NOT_FOUND, 
						"Patient not found for code: "+patientId.getPatientId()+ " for given OrganizationId"));
				throw new OperationException();
			}
			PersonBean personBean = personAgent.getPersonDetails(personPK);
			if (personBean == null){
				addIssue(new Issue(
						IssueTypes.PATIENT_NOT_FOUND, 
						"Patient not found for code: "+patientId.getPatientId()+" OID: "+patientId.getOID()));
				throw new OperationException();
			}
			if (callingUser.getUserAccountId() == null
					|| !callingUser.getUserAccountId().equals(personBean.getPersonAccount())){
				addIssue(new Issue(
						IssueTypes.PATIENT_NOT_FOUND, 
						"Patient not found for OID: "+patientId.getOID()));
				throw new OperationException();
			}

			//checking for group rights
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);

			Integer managePatientPriv =groupAuth.getAppManagePatientsPrivileges();//getAppManagePatientsPrivileges();//getAppManagePatientsPrivileges();
			boolean hasViewManagePatient = 
				GroupAuthModule.hasViewPermission(managePatientPriv);

			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + managePatientPriv);
			if (!hasViewManagePatient){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User not authorized to view patient data"));
				throw new AuthorizationException("User not authorized to view patient data");
			}

			// #6228: Check patient facility rights; this is the max rights among all facilities of this patient
			Integer patFacilityRights = userSiteAgent.getUserPatientFacilityRight(callingUser.getUserId(), personPK);
			if (!GroupAuthModule.hasViewPermission(patFacilityRights)) {
				if (logger.isDebugEnabled()) logger.debug("User not authorized to access data of this patient");
				addIssue(
						new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, 
								"User not authorized to access data of this patient"));
				throw new AuthorizationException("User not authorized to access data of this patient");
			}

	
			//Checking if user has view patient complete data rights
			//if then show complete patient data otherwise show only
			//PHI data.
			Integer managePatientPrivComplete = 
				groupAuth.getAppViewCompletePatientDataPrivileges();
			Integer CompleteDetailsAccessRight = 
				personAgent.getPatientCompleteDetailsAccessRight(
						callingUser.getUserId(),Integer.parseInt(callingUser.getUserGrpDefault()) ,
						personPK);
			//setting boolean hasViewManageCompletePatient to false if user does not have
			//privilege to view complete data.
			if(managePatientPrivComplete == 3 || CompleteDetailsAccessRight== 0 ){
				hasViewManageCompletePatient=false;
			}
			
			Patient patient =  new Patient();
			PatientDemographics patientDemographics = marshalPatientDemographics(personBean);
			patient.setPatientDemographics(patientDemographics);
			patient.setPatientOrganizations(patFacilityService.getPatientOrganizations(personPK));
			PatientIdentifier patiIdentifier = marshallPatientIdentifier(personBean);
			patient.setPatientIdentifier(patiIdentifier);

			return patient;
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl get", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			t.printStackTrace();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl get", t);
			throw new OperationException(t);
		}
	}
	
} 


	
