package com.velos.services.patientdemographics;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.services.OperationException;
import com.velos.services.model.NVPair;
import com.velos.services.model.PatientDemographics;
import com.velos.services.model.StudySummary;

public class MorePatientDetailsDAO extends CommonDAO{
	/**
	 * @author Virendra
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(MorePatientDetailsDAO.class);

	private static String morePatientDetailsSQL = "select pk_perid, pk_codelst, codelst_subtyp,codelst_desc, PERID_ID," +
			"codelst_seq,codelst_custom_col,codelst_custom_col1 from  pat_perid, er_codelst where fk_per = ?" +
			" and  pk_codelst = FK_CODELST_IDTYPE  and codelst_type = 'peridtype' and not exists " +
			"(select * from er_codelst_hide h where h.codelst_type = 'studyidtype' and h.fk_grp = ? " +
			"and h.fk_codelst = pk_codelst and h.CODELST_HIDE_TABLE = 1)";
	/**
	 * getMorePatientDetails
	 * @param PersonPK
	 * @param usersDefaultGroup
	 * @return Map<Integer, NVPair>
	 * @throws OperationException
	 */

	public static Map<Integer, NVPair> getMorePatientDetails(int PersonPK, int usersDefaultGroup) 
	throws OperationException{

        PreparedStatement pstmt = null;
		Connection conn = null;
        try
        {
        	conn = getConnection();
			if (logger.isDebugEnabled()) logger.debug("com.velos.services.patientdemographics.MorePatientDetailsDAO.getStudyMorePatientDetails sql:" + morePatientDetailsSQL);
			
			pstmt = conn.prepareStatement(morePatientDetailsSQL);
			pstmt.setInt(1, PersonPK);
			pstmt.setInt(2, usersDefaultGroup);
			
			ResultSet rs = pstmt.executeQuery();
			
			Map<Integer, NVPair> MPDMap = new HashMap<Integer, NVPair>();
			
			while (rs.next()) {
				String MPDKey = rs.getString("CODELST_SUBTYP");
				if (MPDKey != null && MPDKey.length() > 0){
					Integer MSDPK = rs.getInt("PK_PERID");
					NVPair item = new NVPair();
					String MPDValue = rs.getString("PERID_ID");
					item.setKey(MPDKey);
					item.setValue(MPDValue);
					item.setType(PatientDemographics.MORE_PATIENT_DETAILS_NS);
					item.setDescription(rs.getString("CODELST_DESC"));
					MPDMap.put(MSDPK, item);
				}
			}
			return MPDMap;
        }
        catch (SQLException ex) {
			logger.fatal("MorePatientDetailsDAO.getStudyMorePatientDetails EXCEPTION IN FETCHING FROM pat_perid AND/OR er_codelst"
					+ ex);
			throw new OperationException(ex);
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}

 }
	
	
	
}