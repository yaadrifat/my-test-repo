package com.velos.services.version;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.Versions;

@Remote
public interface VersionService {
	public Versions getStudyVersion(StudyIdentifier studyIdent,boolean currentStatus) throws OperationException;
	public ResponseHolder createVersions(Versions version) throws OperationException;

}
