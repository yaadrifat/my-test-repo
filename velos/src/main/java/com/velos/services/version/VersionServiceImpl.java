package com.velos.services.version;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.study.impl.StudyBean;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.studyVerAgent.StudyVerAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.VersionIdentifier;
import com.velos.services.model.Versions;
import com.velos.services.util.ObjectLocator;

@Stateless
@Remote(VersionService.class)
public class VersionServiceImpl 
extends AbstractService 
implements VersionService {
private static Logger logger = Logger.getLogger(VersionService.class.getName());
	
	@EJB
	GrpRightsAgentRObj groupRightsAgent;

	@Resource
	private SessionContext sessionContext;

	@EJB
	private UserAgentRObj userAgent;

	@EJB
	private ObjectMapService objectMapService;
	
	@EJB
	private StudyVerAgentRObj versionAgent;
	
	@EJB
	private StudyAgent studyAgent;
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Versions getStudyVersion(StudyIdentifier studyIdent,boolean currentStatus) throws OperationException
	{
		try
		{  
			if(studyIdent==null || (StringUtil.isEmpty(studyIdent.getOID()) 
					&& (studyIdent.getPK()==null || studyIdent.getPK()<=0) 
					&& StringUtil.isEmpty(studyIdent.getStudyNumber())))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to get Version"));
			throw new OperationException();
			}
			//VEL-592(chenchumohan)
			System.out.println(" ---studyIdent----"+studyIdent);
			if(studyIdent.getVersion()== null)
			{
				System.out.println(" ---studyIdent---+++++-"+studyIdent.getVersion());
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Version is required in the input")); 
				throw new OperationException(); 
			}
			
			Integer studyPK = locateStudyPK(studyIdent);
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdent.getOID() + " StudyNumber " + studyIdent.getStudyNumber()));
				throw new OperationException();
			}
			
			StudyBean sb = studyAgent.getStudyDetails(studyPK);
			if (sb == null || (! sb.getAccount().equals(callingUser.getUserAccountId()))){
				StringBuffer errorMessage = new StringBuffer("Study not found for:"); 
				if(studyIdent.getOID() != null && studyIdent.getOID().length() > 0 )
				{
					errorMessage.append(" OID:" + studyIdent.getOID()); 
				}
				if(studyIdent.getStudyNumber() != null && studyIdent.getStudyNumber().length() > 0)
				{
					errorMessage.append( " Study Number:" + studyIdent.getStudyNumber()); 
				}
				if(!StringUtil.isEmpty(StringUtil.integerToString(studyIdent.getPK())))
				{
					errorMessage.append( " Study Pk: " + studyIdent.getPK());
				}
				Issue issue = new Issue(IssueTypes.STUDY_NOT_FOUND, errorMessage.toString());
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(); 
			}
			
			GroupAuthModule groupAuth = 
				new GroupAuthModule(this.callingUser, groupRightsAgent);

			Integer manageVersionPriv = 
				groupAuth.getAppManageProtocolsPrivileges();

			boolean hasViewManageVersion = 
				GroupAuthModule.hasViewPermission(manageVersionPriv);
			if (logger.isDebugEnabled()) logger.debug("user manage Version priv: " + hasViewManageVersion);
			if(!hasViewManageVersion)
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to View Version details")); 
				throw new AuthorizationException(); 
			}
			
			TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyPK); 
			Integer manageStudyVersionPriv = teamAuth.getStudyVersionPrivileges(); 
			boolean hasViewVersionPriv = TeamAuthModule.hasViewPermission(manageStudyVersionPriv); 
			
			if(!hasViewVersionPriv)
			{
				addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User Not Authorized to View versions"));
				throw new OperationException();
			}
		  	    
			Map<String, Object> parameters = new HashMap<String, Object>(); 

			parameters.put("versionAgent", versionAgent);
			parameters.put("sessionContext", sessionContext); 
			parameters.put("userAgent", userAgent);
			parameters.put("objectMapService", objectMapService);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response); 
			parameters.put("studyAgent", studyAgent);
			//parameters.put("studyVerNumber", studyVerNumber);
	        VersionServiceDao versionDao= new VersionServiceDao();
	        Versions version = versionDao.getVersion(studyPK,parameters,currentStatus,studyIdent.getVersion());
	        System.out.println("version===============>>>>>"+version);
	        
	        StudyIdentifier studyId = new StudyIdentifier(sb.getStudyNumber());
			studyId.setStudyNumber(sb.getStudyNumber()); 
			studyId.setPK(studyPK);
			ObjectMap studyMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK); 
			studyId.setOID(studyMap.getOID());
			version.setStudyIdentifier(studyId);
   
	        return version;  
		}
		catch(OperationException e){
			e.printStackTrace();
			if (logger.isDebugEnabled()) logger.debug("VersionServiceImpl retrieved ", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("VersionServiceImpl retrieved", t);
			throw new OperationException(t);
		}
				
	}
	
	@Override
	public ResponseHolder createVersions(
			Versions versions)
			throws OperationException {
		try
		{
			StudyIdentifier studyIdent= versions.getStudyIdentifier();
			if(studyIdent==null || (StringUtil.isEmpty(studyIdent.getOID()) 
					&& (studyIdent.getPK()==null || studyIdent.getPK()<=0) 
					&& StringUtil.isEmpty(studyIdent.getStudyNumber())))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to Create Study Version"));
			throw new OperationException();
			}
			
			Integer studyPK = locateStudyPK(studyIdent);
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdent.getOID() + " StudyNumber " + studyIdent.getStudyNumber()));
				throw new OperationException();
			}
			
			StudyBean sb = studyAgent.getStudyDetails(studyPK);
			if (sb == null || (! sb.getAccount().equals(callingUser.getUserAccountId()))){
				StringBuffer errorMessage = new StringBuffer("Study not found for:"); 
				if(studyIdent.getOID() != null && studyIdent.getOID().length() > 0 )
				{
					errorMessage.append(" OID:" + studyIdent.getOID()); 
				}
				if(studyIdent.getStudyNumber() != null && studyIdent.getStudyNumber().length() > 0)
				{
					errorMessage.append( " Study Number:" + studyIdent.getStudyNumber()); 
				}
				if(!StringUtil.isEmpty(StringUtil.integerToString(studyIdent.getPK())))
				{
					errorMessage.append( " Study Pk: " + studyIdent.getPK());
				}
				Issue issue = new Issue(IssueTypes.STUDY_NOT_FOUND, errorMessage.toString());
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(); 
			}
			
			System.out.println(" ---inside the createVersions method----");
			//--------Checking group authorization-------------//
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
			Integer manageVersionPriv = authModule.getAppManageProtocolsPrivileges();

			boolean hasViewManageVersion = 
				GroupAuthModule.hasNewPermission(manageVersionPriv);
			if (logger.isDebugEnabled()) logger.debug("user manage Version priv: " + hasViewManageVersion);
			if(!hasViewManageVersion)
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to edit Version details")); 
				throw new AuthorizationException(); 
			}
			if(versions == null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid VersionInfo is required")); 
				throw new OperationException(); 
			}
			if(versions.getVersion()== null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid CreateVersion is required")); 
				throw new OperationException(); 
			}
		
			Map<String, Object> parameters = new HashMap<String, Object>(); 

			parameters.put("versionAgent", versionAgent);
			parameters.put("sessionContext", sessionContext); 
			parameters.put("userAgent", userAgent);
			parameters.put("objectMapService", objectMapService);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response); 
			parameters.put("studyAgent", studyAgent);
			

				TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyPK); 
				Integer manageStudyVersionPriv = teamAuth.getStudyVersionPrivileges(); 
				boolean hasNewVersionPriv = TeamAuthModule.hasNewPermission(manageStudyVersionPriv); 
				
				if(!hasNewVersionPriv)
				{
					addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User Not Authorized to add versions"));
					throw new OperationException();
				}

			List<Issue> validationIssues = new ArrayList<Issue>();
			VersionServiceDao handler = new VersionServiceDao(); 
			String[] values=handler.saveVersions(studyPK,versions,parameters,validationIssues);
			for( int i = 0; i < values.length; i++ )
			{
				int ret=StringUtil.stringToNum(values[i]);
				System.out.println("Returned is "+ret);
				if(ret>0)
				{
					ObjectMap versionMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_VERSION, ret); 
					VersionIdentifier verIdentifier = new VersionIdentifier(); 
					verIdentifier.setOID(versionMap.getOID()); 
					verIdentifier.setPK(ret);
					response.addObjectCreatedAction(verIdentifier);
				}else
				{
					response.addIssue(new Issue(IssueTypes.ERROR_CREATE_VERSION, values[i]));
				}
			}
			
			if (validationIssues.size() > 0){
				((ResponseHolder) parameters.get("ResponseHolder"))
				.getIssues().addAll(validationIssues); 
				
				if (logger.isDebugEnabled()) logger.debug("Validation Issues found for VersionDetails");
			}
			
		}
		catch(OperationException e){
			if (logger.isDebugEnabled()) logger.debug("VersionServiceImpl createVersions", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("VersionServiceImpl createVersions", t);
			throw new OperationException(t);
		}
		// TODO Auto-generated method stub
		return response;
	}
	
	private Integer locateStudyPK(StudyIdentifier studyIdent) 
			throws OperationException{
				//Virendra:#6123,added OID in if clause
				if(studyIdent==null || (StringUtil.isEmpty(studyIdent.getOID()) 
						&& (studyIdent.getPK()==null || studyIdent.getPK()<=0) 
						&& StringUtil.isEmpty(studyIdent.getStudyNumber())))
				{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studydentifier is required to addMilestones"));
				throw new OperationException();
				}
				
				Integer studyPK =
						ObjectLocator.studyPKFromIdentifier(this.callingUser, studyIdent,objectMapService);
				if (studyPK == null || studyPK == 0){
					StringBuffer errorMessage = new StringBuffer("Study not found for:"); 
					if(studyIdent.getOID() != null && studyIdent.getOID().length() > 0 )
					{
						errorMessage.append(" OID: " + studyIdent.getOID()); 
					}
					if(studyIdent.getStudyNumber() != null && studyIdent.getStudyNumber().length() > 0)
					{
						errorMessage.append( " Study Number: " + studyIdent.getStudyNumber()); 
					}
					addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, errorMessage.toString()));
					throw new OperationException();
				}
				return studyPK;
			}
	
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}

}
