package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="NetworkLevelSite")
public class NetworkLevelSite 	implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5152159038393218959L;
	private String networkPK;
	private String networkName;
	private String level;
	
	public String getNetworkPK() {
		return networkPK;
	}
	public void setNetworkPK(String NetworkPK) {
		networkPK = NetworkPK;
	}
	
	public String getNetworkName() {
		if(networkName!=null  && !networkName.equals("") && networkName.contains("'")){
			try{
				char c =networkName.charAt(networkName.indexOf("'")+1);
				if(c!='\''){
					networkName=networkName.replaceAll("'", "''");
				}
				}catch(IndexOutOfBoundsException e){
					//do nothing
				}
		}
		return networkName;
	}
	public void setNetworkName(String NetworkName) {
		networkName = NetworkName;
	}
	
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	
	
	
	
}
