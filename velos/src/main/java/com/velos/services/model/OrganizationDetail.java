package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;
@XmlRootElement(name="OrganizationDetail")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrganizationDetail implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4579747787612751183L;

	public static QName ORG_MORE_DETAILS_NS = new QName("www.velos.com","OrganizationMoreDetails",	"Map");
	
	private OrganizationIdentifier organizationIdentifier;
	private String Organization_Name;
	private String Site_Id;
	private Code Organization_Type;
	private String Description;
	private OrganizationIdentifier Parent_Organization;
	private String NCI_PO_ID;
	private AddressIdentifier addressIdentifier;
	private String Address; 
	private String City;
	private String State; 
	private String Zip_Code; 
	private String Country; 
	private String Contact_Phone; 
	private String Contact_Email;
	private String Notes;
	private String Hide_Flag;
	private String IP_Add;
	protected List<NVPair> moreOrganizationDetails;
	private String CTEPID;
	
	public String getDescription() {
		return Description;
	}
	public String getNCI_PO_ID() {
		return NCI_PO_ID;
	}
	public String getAddress() {
		return Address;
	}
	public String getCity() {
		return City;
	}
	public String getState() {
		return State;
	}
	public String getZip_Code() {
		return Zip_Code;
	}
	public String getCountry() {
		return Country;
	}
	public String getContact_Phone() {
		return Contact_Phone;
	}
	public String getContact_Email() {
		return Contact_Email;
	}
	public String getNotes() {
		return Notes;
	}
	public String getHide_Flag() {
		return Hide_Flag;
	}
	public List<NVPair> getMoreOrganizationDetails() {
		return moreOrganizationDetails;
	}
	
	public void setDescription(String description) {
		Description = description;
	}
	public void setNCI_PO_ID(String nCI_PO_ID) {
		NCI_PO_ID = nCI_PO_ID;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public void setCity(String city) {
		City = city;
	}
	public void setState(String state) {
		State = state;
	}
	public void setZip_Code(String zip_Code) {
		Zip_Code = zip_Code;
	}
	public void setCountry(String country) {
		Country = country;
	}
	public void setContact_Phone(String contact_Phone) {
		Contact_Phone = contact_Phone;
	}
	public void setContact_Email(String contact_Email) {
		Contact_Email = contact_Email;
	}
	public void setNotes(String notes) {
		Notes = notes;
	}
	public String getOrganization_Name() {
		return Organization_Name;
	}
	public void setOrganization_Name(String organization_Name) {
		Organization_Name = organization_Name;
	}
	public Code getOrganization_Type() {
		return Organization_Type;
	}
	public void setOrganization_Type(Code organization_Type) {
		Organization_Type = organization_Type;
	}
	public void setHide_Flag(String hide_Flag) {
		Hide_Flag = hide_Flag;
	}
	public void setMoreOrganizationDetails(List<NVPair> moreOrganizationDetails) {
		this.moreOrganizationDetails = moreOrganizationDetails;
	}
	public String getIP_Add() {
		return IP_Add;
	}
	public void setIP_Add(String iP_Add) {
		IP_Add = iP_Add;
	}
	
	public AddressIdentifier getAddressIdentifier() {
		return addressIdentifier;
	}
	public void setAddressIdentifier(AddressIdentifier addressIdentifier) {
		this.addressIdentifier = addressIdentifier;
	}
	public OrganizationIdentifier getOrganizationIdentifier() {
		return organizationIdentifier;
	}
	public void setOrganizationIdentifier(OrganizationIdentifier organizationIdentifier) {
		this.organizationIdentifier = organizationIdentifier;
	}
	public String getSite_Id() {
		return Site_Id;
	}
	public void setSite_Id(String site_Id) {
		Site_Id = site_Id;
	}
	public OrganizationIdentifier getParent_Organization() {
		return Parent_Organization;
	}
	public void setParent_Organization(OrganizationIdentifier parent_Organization) {
		Parent_Organization = parent_Organization;
	}
	public String getCTEPID() {
		return CTEPID;
	}
	public void setCTEPID(String cTEPID) {
		CTEPID = cTEPID;
	}

}
