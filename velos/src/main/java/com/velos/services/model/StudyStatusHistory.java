package com.velos.services.model;



import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="StudyHistory")
@XmlAccessorType (XmlAccessType.FIELD)
public class StudyStatusHistory implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private StudyVerStatusIdentifier studyVerStatIdent;
	/*
	private Integer STATUS_MODPK;*/
	/*
	private String STATUS_MODTABLE;*/
	
	private Code PK_CODELST_STAT;
	
	private Date STATUS_DATE;
	
	private Date STATUS_END_DATE;
	
	private String STATUS_NOTES;
	
	private String STATUS_CUSTOM1;
	
	private String RECORD_TYPE;
	
	private Integer STATUS_ISCURRENT;
	/*
	private String STATUSENDDATE;*/
	
	public StudyVerStatusIdentifier getStudyVerStatIdent() {
		return studyVerStatIdent;
	}
	public void setStudyVerStatIdent(StudyVerStatusIdentifier studyVerStatIdent) {
		this.studyVerStatIdent = studyVerStatIdent;
	}
	
	/*public Integer getSTATUS_MODPK() {
		return STATUS_MODPK;
	}
	public void setSTATUS_MODPK(Integer sTATUSMODPK) {
		STATUS_MODPK = sTATUSMODPK;
	}*/
	/*public String getSTATUS_MODTABLE() {
		return STATUS_MODTABLE;
	}
	public void setSTATUS_MODTABLE(String sTATUSMODTABLE) {
		STATUS_MODTABLE = sTATUSMODTABLE;
	}*/
	public Code getPK_CODELST_STAT() {
		return PK_CODELST_STAT;
	}
	public void setPK_CODELST_STAT(Code pKCODELSTSTAT) {
		PK_CODELST_STAT = pKCODELSTSTAT;
	}
	public Date getSTATUS_DATE() {
		return STATUS_DATE;
	}
	public void setSTATUS_DATE(Date sTATUSDATE) {
		STATUS_DATE = sTATUSDATE;
	}
	public Date getSTATUS_END_DATE() {
		return STATUS_END_DATE;
	}
	public void setSTATUS_END_DATE(Date sTATUS_END_DATE) {
		STATUS_END_DATE = sTATUS_END_DATE;
	}
	public String getSTATUS_NOTES() {
		return STATUS_NOTES;
	}
	public void setSTATUS_NOTES(String sTATUSNOTES) {
		STATUS_NOTES = sTATUSNOTES;
	}
	public String getSTATUS_CUSTOM1() {
		return STATUS_CUSTOM1;
	}
	public void setSTATUS_CUSTOM1(String sTATUSCUSTOM1) {
		STATUS_CUSTOM1 = sTATUSCUSTOM1;
	}
	public String getRECORD_TYPE() {
		return RECORD_TYPE;
	}
	public void setRECORD_TYPE(String rECORDTYPE) {
		RECORD_TYPE = rECORDTYPE;
	}
	public Integer getSTATUS_ISCURRENT() {
		return STATUS_ISCURRENT;
	}
	public void setSTATUS_ISCURRENT(Integer sTATUSISCURRENT) {
		STATUS_ISCURRENT = sTATUSISCURRENT;
	}
	/*public String getSTATUSENDDATE() {
		return STATUSENDDATE;
	}
	public void setSTATUSENDDATE(String sTATUSENDDATE) {
		STATUSENDDATE = sTATUSENDDATE;
	}*/
	

}
