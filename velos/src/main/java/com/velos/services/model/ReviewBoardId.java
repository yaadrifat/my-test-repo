package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="reviewBoardId")
public class ReviewBoardId implements Serializable{
	private static final long serialVersionUID = 6761450731373866511L;
	protected String reviewBoardName;
	protected Integer PK;
	
	public Integer getPK() {
		return PK;
	}
	public void setPK(Integer pK) {
		PK = pK;
	}
	public String getReviewBoardName() {
		return reviewBoardName;
	}
	public void setReviewBoardName(String reviewBoardName) {
		this.reviewBoardName = reviewBoardName;
	}
	
	public ReviewBoardId(){
		super();
	}
	
}
