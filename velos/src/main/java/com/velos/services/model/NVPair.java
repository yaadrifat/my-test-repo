
package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;

/**
 * Class that represents name-value pairs for transmitting field data in and out of service calls.
 * 
 * One common use of this class is {@link StudySummary#setMoreStudyDetails(java.util.List)}, which is
 * takes a list of NVPairs. This is used as the list of fields in More Study Details is configurable
 * by installation, so there is no way to define that set in the service API.
 * @author dylan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="NVPair")
public class NVPair 
	implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 6672597953158479182L;

	protected String key;

    protected Object value;

    protected QName type;
    
    protected String description;
    
    public NVPair(){
    	
    }
    
    /**
     * Gets the value of the key property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setValue(Object value) {
        this.value = value;
    }

	public QName getType() {
		return type;
	}

	public void setType(QName type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
