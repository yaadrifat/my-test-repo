package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="updateNetworkSite")
@XmlAccessorType(XmlAccessType.FIELD)
public class UpdatNetworkSiteDetails implements Serializable{

	private static final long serialVersionUID = 8148082350056721577L;
	private List<NetSiteDetails> networkSite=new ArrayList<NetSiteDetails>();
	
	public List<NetSiteDetails> getNetworkSiteDetailsList() {
		return networkSite;
	}
	public void setNetworkSiteDetailsList(List<NetSiteDetails> networkSite) {
		this.networkSite = networkSite;
	}
	
}
