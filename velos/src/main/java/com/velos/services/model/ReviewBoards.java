package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.velos.services.ResponseHolder;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="reviewBoards")
public class ReviewBoards extends ResponseHolder implements Serializable{
	protected List<ReviewBoard> reviewBoard;

	public List<ReviewBoard> getReviewBoard() {
		return reviewBoard;
	}

	public void setReviewBoard(List<ReviewBoard> reviewBoard) {
		this.reviewBoard = reviewBoard;
	}
}
