/**
 * Created On Oct 31, 2011
 */
package com.velos.services.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="EventSummary")
public class CalendarEventSummary implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6154772765037349541L;
	protected EventIdentifier eventIdentifier; 
	protected String eventName;
	protected String description;
	protected String CPTCode;
	protected Integer sequence;
	protected OrganizationIdentifier facility;
	protected OrganizationIdentifier siteOfService;
	protected Code coverageType;
	protected Duration eventDuration; 
	protected Duration eventWindowBefore;
	protected Duration eventWindowAfter;
	protected String notes;
	protected String coverageNotes;
	
	@NotNull
	public String getEventName() {
		return eventName;
	}


	public void setEventName(String eventName) {
		this.eventName = eventName;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getCPTCode() {
		return CPTCode;
	}


	public void setCPTCode(String cPTCode) {
		CPTCode = cPTCode;
	}


	public Integer getSequence() {
		return sequence;
	}


	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}




	public OrganizationIdentifier getFacility() {
		return facility;
	}


	public void setFacility(OrganizationIdentifier facility) {
		this.facility = facility;
	}


	public OrganizationIdentifier getSiteOfService() {
		return siteOfService;
	}


	public void setSiteOfService(OrganizationIdentifier siteOfService) {
		this.siteOfService = siteOfService;
	}


	public Code getCoverageType() {
		return coverageType;
	}


	public void setCoverageType(Code coverageType) {
		this.coverageType = coverageType;
	}


	public Duration getEventWindowBefore() {
		return eventWindowBefore;
	}


	public void setEventWindowBefore(Duration eventWindowBefore) {
		this.eventWindowBefore = eventWindowBefore;
	}


	public Duration getEventWindowAfter() {
		return eventWindowAfter;
	}


	public void setEventWindowAfter(Duration eventWindowAfter) {
		this.eventWindowAfter = eventWindowAfter;
	}


	public String getNotes() {
		return notes;
	}


	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	public EventIdentifier getEventIdentifier() {
		return eventIdentifier;
	}


	public void setEventIdentifier(EventIdentifier eventIdentifier) {
		this.eventIdentifier = eventIdentifier;
	}	
	
	public Duration getEventDuration() {
		return eventDuration;
	}

	public void setEventDuration(Duration eventDuration) {
		this.eventDuration = eventDuration;
	}


	public String getCoverageNotes() {
		return coverageNotes;
	}


	public void setCoverageNotes(String coverageNotes) {
		this.coverageNotes = coverageNotes;
	}

}
