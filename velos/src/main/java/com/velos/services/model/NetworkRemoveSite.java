package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="networkSite")
@XmlAccessorType(XmlAccessType.FIELD)
public class NetworkRemoveSite implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6289081998801858750L;

	private String networkName;
	
	private SimpleIdentifier networkIdentifier;
		
	public String getNetworkName() {
		if(networkName!=null  && !networkName.equals("") && networkName.contains("'")){
			try{
				char c =networkName.charAt(networkName.indexOf("'")+1);
				if(c!='\''){
					networkName=networkName.replaceAll("'", "''");
				}
				}catch(IndexOutOfBoundsException e){
					//do nothing
				}
		}
		return networkName;
	}
	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}
	
	public SimpleIdentifier getNetworkIdentifier() {
		return networkIdentifier;
	}

	public void setNetworkIdentifier(SimpleIdentifier networkIdentifier) {
		this.networkIdentifier = networkIdentifier;
	}


}
