package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientSearchResponse")
public class PatientSearchResponse implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4248454119997827485L;
	/**
	 * 
	 */
	List<PatientDataBean> patDataBean = null;
	int totalCount = 0;
	int pageNumber = 0;
	int pageSize = 0;

	public List<PatientDataBean> getPatDataBean() {
		return patDataBean;
	}
	public void setPatDataBean(List<PatientDataBean> patDataBean) {
		this.patDataBean = patDataBean;
	}
	
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	

	/*public int gettotalCount() {
		return totalCount;
	}
	public void settotalCount(int totalCount) {
		this.totalCount = totalCount;
	}*/
}
