package com.velos.services.model;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="removeNetworkSiteUser")
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(propOrder={"network", "networkName","siteLevelDetails","relationshipPK","userPKDetails"})
public class NetworkSiteUserListDetail implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8148082350056721577L;
	private List<NetworkSiteUserDetail> networkSiteUser=new ArrayList<NetworkSiteUserDetail>();
	
	public List<NetworkSiteUserDetail> getNetworkSiteDetailsList() {
		return networkSiteUser;
	}
	public void setNetworkSiteDetailsList(List<NetworkSiteUserDetail> networkSiteDetailsList) {
		this.networkSiteUser = networkSiteDetailsList;
	}
	
}
