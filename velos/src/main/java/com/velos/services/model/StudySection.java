package com.velos.services.model;

public class StudySection extends ServiceObjects {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected String sectionId;
	
	protected String sectionContent;
	
	protected String studySectionType;

	public String getSectionId() {
		return sectionId;
	}

	public void setSectionId(String sectionId) {
		this.sectionId = sectionId;
	}

	public String getSectionContent() {
		return sectionContent;
	}

	public void setSectionContent(String sectionContent) {
		this.sectionContent = sectionContent;
	}

	public String getStudySectionType() {
		return studySectionType;
	}

	public void setStudySectionType(String studySectionType) {
		this.studySectionType = studySectionType;
	}

}
