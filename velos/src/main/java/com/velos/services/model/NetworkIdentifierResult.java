package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="NetworkIdentifier")
public class NetworkIdentifierResult implements Serializable{

	
	private static final long serialVersionUID = 8031445360324587120L;
	private String relationshipPK;
	private String OID;
	private String siteName;
	private String level;
	public String getRelationshipPK() {
		return relationshipPK;
	}
	public void setRelationshipPK(String relationshipPK) {
		this.relationshipPK = relationshipPK;
	}
	public String getOID() {
		return OID;
	}
	public void setOID(String oID) {
		OID = oID;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
}
