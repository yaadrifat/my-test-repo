/**
 * Created On Jun 30, 2011
 */
package com.velos.services.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Kanwaldeep
 *
 */
public class PatientEnrollmentDetails implements Serializable{
	

	private static final long serialVersionUID = 8680795314719525049L;

	//status details
	protected Code status; 
	
	protected Code statusReason; 
	
	protected Date statusDate; 
	
	protected boolean isCurrentStatus = true;
	
	protected String notes; 
	
	//Enrollment Details
	
	protected String randomizationNumber;
	
	protected UserIdentifier enrolledBy; 
	
	// Informed Constent Details
	
	protected String InformedConsentVersionNumber; 
	
	//Follow-up Details
	protected Date nextFollowUpDate; 
	
	//Screening Details
	protected String screenNumber; 
	
	protected UserIdentifier screenedBy; 
	
	protected Code screeningOutcome;
	
	//AdditionalInformation
	
	protected String patientStudyId; 
	
	protected OrganizationIdentifier enrollingSite; 	
	
	protected UserIdentifier assignedTo; 
	
	protected UserIdentifier physician; 
	
	//'treatloc'
	protected Code treatmentLocation; 
	
	protected OrganizationIdentifier treatingOrganization; 
	
	//evaluation status
	
	//pst_eval_flag
	protected Code evaluationFlag; 
	
	//ptst_eval
	protected Code evaluationStatus; 
	
	//ptst_ineval
	protected Code inevaluationStatus; 

	//Patient Status	
	//ptst_survival
	protected Code survivalStatus; 
	
	protected Date dateOfDeath;  
	//pat_dth_cause
	protected Code causeOfDeath; 
	//Cause of Death Select an OptionOther 
	protected String specifyCause; 
	
	//ptst_dth_stdrel
	protected Code deathRelatedToTrial; 
	
	protected String reasonOfDeathRelatedToTrial; 	
	
	protected boolean emptyNextFollowUpDate;
	
	protected boolean emptyDeathDate;
	
	protected String reasonForChange;
	
	
	/**
	 * @return the reasonForChange
	 */
	public String getReasonForChange() {
		return reasonForChange;
	}
	/**
	 * @param reasonForChange the reasonForChange to set
	 */
	public void setReasonForChange(String reasonForChange) {
		this.reasonForChange = reasonForChange;
	}
	
	public boolean isEmptyNextFollowUpDate() {
		return emptyNextFollowUpDate;
	}
	public void setEmptyNextFollowUpDate(boolean emptyNextFollowUpDate) {
		this.emptyNextFollowUpDate = emptyNextFollowUpDate;
	}
	
	public boolean isEmptyDeathDate() {
		return emptyDeathDate;
	}
	public void setEmptyDeathDate(boolean emptyDeathDate) {
		this.emptyDeathDate = emptyDeathDate;
	}
	public String getPatientStudyId() {
		return patientStudyId;
	}

	public void setPatientStudyId(String patientStudyId) {
		this.patientStudyId = patientStudyId;
	}

	public Date getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}

	public Code getStatusReason() {
		return statusReason;
	}

	public void setStatusReason(Code statusReason) {
		this.statusReason = statusReason;
	}

	public OrganizationIdentifier getEnrollingSite() {
		return enrollingSite;
	}

	public void setEnrollingSite(OrganizationIdentifier enrollingSite) {
		this.enrollingSite = enrollingSite;
	}

	public boolean isCurrentStatus() {
		return isCurrentStatus;
	}

	public void setCurrentStatus(boolean isCurrentStatus) {
		this.isCurrentStatus = isCurrentStatus;
	}

	public UserIdentifier getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(UserIdentifier assignedTo) {
		this.assignedTo = assignedTo;
	}

	public UserIdentifier getPhysician() {
		return physician;
	}

	public void setPhysician(UserIdentifier physician) {
		this.physician = physician;
	}

	public Code getTreatmentLocation() {
		return treatmentLocation;
	}

	public void setTreatmentLocation(Code treatmentLocation) {
		this.treatmentLocation = treatmentLocation;
	}

	public OrganizationIdentifier getTreatingOrganization() {
		return treatingOrganization;
	}

	public void setTreatingOrganization(OrganizationIdentifier treatingOrganization) {
		this.treatingOrganization = treatingOrganization;
	}

	public Code getEvaluationFlag() {
		return evaluationFlag;
	}

	public void setEvaluationFlag(Code evaluationFlag) {
		this.evaluationFlag = evaluationFlag;
	}

	public Code getEvaluationStatus() {
		return evaluationStatus;
	}

	public void setEvaluationStatus(Code evaluationStatus) {
		this.evaluationStatus = evaluationStatus;
	}

	public Code getInevaluationStatus() {
		return inevaluationStatus;
	}

	public void setInevaluationStatus(Code inevaluationStatus) {
		this.inevaluationStatus = inevaluationStatus;
	}

	public Code getSurvivalStatus() {
		return survivalStatus;
	}

	public void setSurvivalStatus(Code survivalStatus) {
		this.survivalStatus = survivalStatus;
	}

	public Date getDateOfDeath() {
		return dateOfDeath;
	}

	public void setDateOfDeath(Date dateOfDeath) {
		this.dateOfDeath = dateOfDeath;
	}

	public Code getDeathRelatedToTrial() {
		return deathRelatedToTrial;
	}

	public void setDeathRelatedToTrial(Code deathRelatedToTrial) {
		this.deathRelatedToTrial = deathRelatedToTrial;
	}

	public String getReasonOfDeathRelatedToTrial() {
		return reasonOfDeathRelatedToTrial;
	}

	public void setReasonOfDeathRelatedToTrial(String reasonOfDeathRelatedToTrial) {
		this.reasonOfDeathRelatedToTrial = reasonOfDeathRelatedToTrial;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Code getStatus() {
		return status;
	}

	public void setStatus(Code status) {
		this.status = status;
	}

	public String getRandomizationNumber() {
		return randomizationNumber;
	}

	public void setRandomizationNumber(String randomizationNumber) {
		this.randomizationNumber = randomizationNumber;
	}

	public UserIdentifier getEnrolledBy() {
		return enrolledBy;
	}

	public void setEnrolledBy(UserIdentifier enrolledBy) {
		this.enrolledBy = enrolledBy;
	}

	public Code getCauseOfDeath() {
		return causeOfDeath;
	}

	public void setCauseOfDeath(Code causeOfDeath) {
		this.causeOfDeath = causeOfDeath;
	}

	public String getSpecifyCause() {
		return specifyCause;
	}

	public void setSpecifyCause(String specifyCause) {
		this.specifyCause = specifyCause;
	}

	public String getInformedConsentVersionNumber() {
		return InformedConsentVersionNumber;
	}

	public void setInformedConsentVersionNumber(String informedConsentVersionNumber) {
		InformedConsentVersionNumber = informedConsentVersionNumber;
	}

	public Date getNextFollowUpDate() {
		return nextFollowUpDate;
	}

	public void setNextFollowUpDate(Date nextFollowUpDate) {
		this.nextFollowUpDate = nextFollowUpDate;
	}

	public String getScreenNumber() {
		return screenNumber;
	}

	public void setScreenNumber(String screenNumber) {
		this.screenNumber = screenNumber;
	}

	public UserIdentifier getScreenedBy() {
		return screenedBy;
	}

	public void setScreenedBy(UserIdentifier screenedBy) {
		this.screenedBy = screenedBy;
	}

	public Code getScreeningOutcome() {
		return screeningOutcome;
	}

	public void setScreeningOutcome(Code screeningOutcome) {
		this.screeningOutcome = screeningOutcome;
	} 
	
}
