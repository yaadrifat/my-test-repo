package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="network")
@XmlAccessorType(XmlAccessType.FIELD)
public class NetSiteUser implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4367244145877405027L;

	private NetIdentifier network;
	private String networkName;
	private SiteLevelDetail siteLevelDetail;
	private String relationshipPK;
	private List<UsersNetworkSites> userNetSite;
	
	
	
	public NetIdentifier getNetworkIdentifier() {
		return network;
	}
	public void setNetworkIdentifier(NetIdentifier network) {
		this.network = network;
	}
	public String getNetworkName() {
		return networkName;
	}
	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}
	public SiteLevelDetail getSiteLevelDetail() {
		return siteLevelDetail;
	}
	public void setSiteLevelDetail(SiteLevelDetail siteLevelDetail) {
		this.siteLevelDetail = siteLevelDetail;
	}
	public String getRelationshipPK() {
		return relationshipPK;
	}
	public void setRelationshipPK(String relationshipPK) {
		this.relationshipPK = relationshipPK;
	}
	public List<UsersNetworkSites> getUserNetSite() {
		return userNetSite;
	}
	public void setUserNetSite(List<UsersNetworkSites> userNetSite) {
		this.userNetSite = userNetSite;
	}
	
	
	
	
}
