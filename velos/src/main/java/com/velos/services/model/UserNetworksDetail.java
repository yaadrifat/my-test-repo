package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="UserNetworksDetail")
public class UserNetworksDetail implements Serializable {
	private String userLoginName;
	protected Integer userPK;
	public Integer getPK() {
		return userPK;
	}

	public void setPK(Integer userPK) {
		this.userPK = userPK;
	}
	public String getUserLoginName() {
		return userLoginName;
	}

	public void setUserLoginName(String userId) {
		this.userLoginName = userId;
	}	
	
}
