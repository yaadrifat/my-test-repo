/**
 * 
 */
package com.velos.services.model;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/** * Stores meta-data fields about a Calendar.
 * @author dylan
 *
 */
@XmlRootElement(name="CalendarSummary")
@XmlAccessorType(XmlAccessType.FIELD)
public class CalendarSummary extends ServiceObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4914344220244393620L;
	protected String calendarName;
	protected String calendarDescription;
	protected Code calendarType;
	protected Duration calendarDuration;
	protected Code calendarStatus;
	protected Date statusDate;
	protected UserIdentifier statusChangedBy; 
	protected String statusNotes; 

	public Date getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}

	public UserIdentifier getStatusChangedBy() {
		return statusChangedBy;
	}

	public void setStatusChangedBy(UserIdentifier statusChangedBy) {
		this.statusChangedBy = statusChangedBy;
	}

	public String getStatusNotes() {
		return statusNotes;
	}

	public void setStatusNotes(String statusNotes) {
		this.statusNotes = statusNotes;
	}

	public CalendarSummary(){
		
	}

	@NotNull
	public Code getCalendarStatus() {
		return calendarStatus;
	}

	public void setCalendarStatus(Code calendarStatus) {
		this.calendarStatus = calendarStatus;
	}
	

	@NotNull
	public String getCalendarName() {
		return calendarName;
	}

	public void setCalendarName(String calendarName) {
		this.calendarName = calendarName;
	}

	public String getCalendarDescription() {
		return calendarDescription;
	}

	public void setCalendarDescription(String calendarDescription) {
		this.calendarDescription = calendarDescription;
	}

	@NotNull
	public Code getCalendarType() {
		return calendarType;
	}

	public void setCalendarType(Code calendarType) {
		this.calendarType = calendarType;
	}


	public void setCalendarDuration(Duration calendarDuration) {
		this.calendarDuration = calendarDuration;
	}
	
	@NotNull
	@Valid
	public Duration getCalendarDuration() {
		return calendarDuration;
	}
	
	public ParentIdentifier getParentIdentifier()
	{
		return parentIdentifier; 
	}
	
	public void setParentIdentifier(ParentIdentifier parentIdentifier)
	{
		this.parentIdentifier = parentIdentifier; 
	}

}
