package com.velos.services.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyNIHGrantIdentifier")
public class StudyNIHGrantIdentifier extends SimpleIdentifier
implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * the NIH grant Serial Number
     */
	@NotNull
	private String nihGrantSerial;
	
	public String getNihGrantSerial() {
		return nihGrantSerial;
	}
	public void setNihGrantSerial(String nihGrantSerial) {
		this.nihGrantSerial = nihGrantSerial;
	}

}
