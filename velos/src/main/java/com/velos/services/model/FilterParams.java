package com.velos.services.model;

import java.io.Serializable;

public class FilterParams implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1046649618575367271L;
	
	private Integer formPK;
	private FieldID fieldIDs;
	
	public Integer getFormPK() {
		return formPK;
	}
	public void setFormPK(Integer formPK) {
		this.formPK = formPK;
	}
	public FieldID getFieldIDs() {
		return fieldIDs;
	}
	public void setFieldIDs(FieldID fieldIDs) {
		this.fieldIDs = fieldIDs;
	}

	
}
