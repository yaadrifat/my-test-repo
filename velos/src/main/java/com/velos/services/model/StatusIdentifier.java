package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="studyStatus")
public class StatusIdentifier {
	 StudyStatusIdentifier studyStatusIdentifier;

	public StudyStatusIdentifier getStudyStatusIdentifier() {
		return studyStatusIdentifier;
	}

	public void setStudyStatusIdentifier(StudyStatusIdentifier studyStatusIdentifier) {
		this.studyStatusIdentifier = studyStatusIdentifier;
	}
	
	
}
