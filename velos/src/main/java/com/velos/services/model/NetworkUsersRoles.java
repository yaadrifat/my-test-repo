package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Role")
public class NetworkUsersRoles implements Serializable{


	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2449655414179676728L;
	private String action;
	private Code roleName;
	private Code roleStatus;
	private String roleStatusDate;
	private String roleStatusNotes;
	private String isAdditionalRole;
	
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}


	
	public Code getRoleName() {
		return roleName;
	}

	public void setRoleName(Code roleName) {
		this.roleName = roleName;
	}

	
   public void setRoleStatus(Code rolestatus){
			this.roleStatus=rolestatus;
			}

		public Code getRoleStatus(){
		  return roleStatus;
	    }
		public String getRoleStatusDate() {
			return roleStatusDate;
		}

		public void setRoleStatusDate(String roleStatusDate) {
			this.roleStatusDate = roleStatusDate;
		}

		public String getRoleStatusNotes() {
			return roleStatusNotes;
		}

		public void setRoleStatusNotes(String roleStatusNotes) {
			this.roleStatusNotes = roleStatusNotes;
		}

		public String getIsAdditionalRole() {
			return isAdditionalRole;
		}

		public void setIsAdditionalRole(String isAdditionalRole) {
			this.isAdditionalRole = isAdditionalRole;
		}

		
}
