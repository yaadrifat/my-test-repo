package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Tarandeep Singh Bali
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyOrganizationIdentifier")
public class StudyOrganizationIdentifier extends SimpleIdentifier {

	/**
	 * 
	 */
	private static final long serialVersionUID = 673611412683188859L;
	
}