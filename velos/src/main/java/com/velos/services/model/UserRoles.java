package com.velos.services.model;

import java.io.Serializable;

public class UserRoles implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -480778034555124801L;
	private Code roleName;
	private Code roleStatus;
	private String roleStatusDate;
	private String roleStatusNotes;
	private String isAdditionalRole;
	public Code getRoleName() {
		return roleName;
	}
	public void setRoleName(Code roleName) {
		this.roleName = roleName;
	}
	public Code getRoleStatus() {
		return roleStatus;
	}
	public void setRoleStatus(Code roleStatus) {
		this.roleStatus = roleStatus;
	}
	public String getRoleStatusDate() {
		return roleStatusDate;
	}
	public void setRoleStatusDate(String roleStatusDate) {
		this.roleStatusDate = roleStatusDate;
	}
	public String getRoleStatusNotes() {
		return roleStatusNotes;
	}
	public void setRoleStatusNotes(String roleStatusNotes) {
		this.roleStatusNotes = roleStatusNotes;
	}
	public String getIsAdditionalRole() {
		return isAdditionalRole;
	}
	public void setIsAdditionalRole(String isAdditionalRole) {
		this.isAdditionalRole = isAdditionalRole;
	}
	
	
	

}
