package com.velos.services.model;
import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="createNetworkSiteUser")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreateNetworkSiteUser implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2756534505669694422L;
	private NetworkDetails network;
	private List<UserDetails> user;
	
	
	public NetworkDetails getNetwork() {
		return network;
	}
	public void setNetwork(NetworkDetails network) {
		this.network = network;
	}
	public List<UserDetails> getUser() {
		return user;
	}
	public void setUser(List<UserDetails> user) {
		this.user = user;
	}
	

}
