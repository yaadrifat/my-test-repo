package com.velos.services.model;


public class OutboundIdentifier extends SimpleIdentifier {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8588951780676910282L;
	protected String studyNumber;
	protected String formName;
	protected String calendarName;
	protected String budgetName;
	protected OutboundStudyIdentifier studyIdentifier;
	protected OutboundPatientIdentifier patientIdentifier;
	
	public OutboundIdentifier(){
		super();
	}

	public OutboundIdentifier(String studyNumber) {
		this.studyNumber = studyNumber;
	}

	/**
	 * Gets the study number, which uniquely identifies a study for a particular account.
	 * @return
	 */
	public String getStudyNumber() {
		return studyNumber;
	}

	/**
	 * Sets the study number, which uniquely identifies a study for a particular account.
	 * @return
	 */
	public void setStudyNumber(String studyNumber) {
		this.studyNumber = studyNumber;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public String getCalendarName() {
		return calendarName;
	}

	public void setCalendarName(String calendarName) {
		this.calendarName = calendarName;
	}

	public String getBudgetName() {
		return budgetName;
	}

	public void setBudgetName(String budgetName) {
		this.budgetName = budgetName;
	}

	public OutboundStudyIdentifier getStudyIdentifier() {
		return studyIdentifier;
	}

	public void setStudyIdentifier(OutboundStudyIdentifier studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	}

	public OutboundPatientIdentifier getPatientIdentifier() {
		return patientIdentifier;
	}

	public void setPatientIdentifier(OutboundPatientIdentifier patientIdentifier) {
		this.patientIdentifier = patientIdentifier;
	}

	@Override
	public String toString(){
		return studyNumber;
	}
}
