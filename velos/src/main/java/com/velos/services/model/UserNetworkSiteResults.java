package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import com.velos.services.ResponseHolder;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="networks")
public class UserNetworkSiteResults implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1739541305318161153L;
	private ResponseHolder response;

	private List<UserNetworks> network=new ArrayList<UserNetworks>();
	
	//private Integer pageNumber = null;
	
	//private Long pageSize = null;
	
	//private Long totalCount = null;
	
	public List<UserNetworks> getNetwork() {
		return network;
	}
	public void setNetwork(List<UserNetworks> network) {
		this.network = network;
	}
	
	/*public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Long getPageSize() {
		return pageSize;
	}

	public void setPageSize(Long pageSize) {
		this.pageSize = pageSize;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}*/
	public void addNetwork(UserNetworks network){
		this.network.add(network);
	}
	
	public ResponseHolder getResponse() {
		return response;
	}
	public void setResponse(ResponseHolder response) {
		this.response = response;
	}
	
}
