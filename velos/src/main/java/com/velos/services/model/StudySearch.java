
package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudySearch")

/**
 * Model class for fields in the Study Search
 */
public class StudySearch
    implements Serializable
{
	/**
     * 
     */
    private static final long serialVersionUID = -356950809796989200L;

    public StudySearch(){
        
    }

    /** Theraputic Area **/
    protected Code therapeuticArea;
    
    protected UserIdentifier principalInvestigator;

    protected String studyNumber;

    protected String studyTitle;

    protected Code division;
    
    protected Code phase;
    
    protected OrganizationIdentifier studyOrganization;
    
    protected String moreStudyDetails;
    
    protected Code currentStudyStatus;
    
    protected Code displayedStudyStatus;
    
    //Pagination Fields
    protected Integer pageSize;
    
    protected Integer pageNumber;
	
    //Sorting Fields
    protected String sortBy;
    
    protected String sortOrder;

	/**
	 * @return the therapeuticArea
	 */
	public Code getTherapeuticArea() {
		return therapeuticArea;
	}

	/**
	 * @param therapeuticArea the therapeuticArea to set
	 */
	public void setTherapeuticArea(Code therapeuticArea) {
		this.therapeuticArea = therapeuticArea;
	}

	/**
	 * @return the principalInvestigator
	 */
	public UserIdentifier getPrincipalInvestigator() {
		return principalInvestigator;
	}

	/**
	 * @param principalInvestigator the principalInvestigator to set
	 */
	public void setPrincipalInvestigator(UserIdentifier principalInvestigator) {
		this.principalInvestigator = principalInvestigator;
	}

	/**
	 * @return the studyNumber
	 */
	public String getStudyNumber() {
		return studyNumber;
	}

	/**
	 * @param studyNumber the studyNumber to set
	 */
	public void setStudyNumber(String studyNumber) {
		this.studyNumber = studyNumber;
	}

	/**
	 * @return the studyTitle
	 */
	public String getStudyTitle() {
		return studyTitle;
	}

	/**
	 * @param studyTitle the studyTitle to set
	 */
	public void setStudyTitle(String studyTitle) {
		this.studyTitle = studyTitle;
	}

	/**
	 * @return the division
	 */
	public Code getDivision() {
		return division;
	}

	/**
	 * @param division the division to set
	 */
	public void setDivision(Code division) {
		this.division = division;
	}

	/**
	 * @return the phase
	 */
	public Code getPhase() {
		return phase;
	}

	/**
	 * @param phase the phase to set
	 */
	public void setPhase(Code phase) {
		this.phase = phase;
	}

	/**
	 * @return the studyOrganization
	 */
	public OrganizationIdentifier getStudyOrganization() {
		return studyOrganization;
	}

	/**
	 * @param studyOrganization the studyOrganization to set
	 */
	public void setStudyOrganization(OrganizationIdentifier studyOrganization) {
		this.studyOrganization = studyOrganization;
	}

	/**
	 * @return the moreStudyDetails
	 */
	public String getMoreStudyDetails() {
		return moreStudyDetails;
	}

	/**
	 * @param moreStudyDetails the moreStudyDetails to set
	 */
	public void setMoreStudyDetails(String moreStudyDetails) {
		this.moreStudyDetails = moreStudyDetails;
	}

	/**
	 * @return the currentStudyStatus
	 */
	public Code getCurrentStudyStatus() {
		return currentStudyStatus;
	}

	/**
	 * @param currentStudyStatus the currentStudyStatus to set
	 */
	public void setCurrentStudyStatus(Code currentStudyStatus) {
		this.currentStudyStatus = currentStudyStatus;
	}

	/**
	 * @return the displayedStudyStatus
	 */
	public Code getDisplayedStudyStatus() {
		return displayedStudyStatus;
	}

	/**
	 * @param displayedStudyStatus the displayedStudyStatus to set
	 */
	public void setDisplayedStudyStatus(Code displayedStudyStatus) {
		this.displayedStudyStatus = displayedStudyStatus;
	}

	/**
	 * @return the pageSize
	 */
	public Integer getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the pageNumber
	 */
	public Integer getPageNumber() {
		return pageNumber;
	}

	/**
	 * @param pageNumber the pageNumber to set
	 */
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	/**
	 * @return the sortBy
	 */
	public String getSortBy() {
		return sortBy;
	}

	/**
	 * @param sortBy the sortBy to set
	 */
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	/**
	 * @return the sortOrder
	 */
	public String getSortOrder() {
		return sortOrder;
	}

	/**
	 * @param sortOrder the sortOrder to set
	 */
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}
    
	//Added by Rajasekhar Reddy
	
	protected String msdCodeSubType;

	public String getMsdCodeSubType() {
		return msdCodeSubType;
	}

	public void setMsdCodeSubType(String msdCodeSubType) {
		this.msdCodeSubType = msdCodeSubType;
	}

		
	//Added by Rajasekhar Reddy
    
}
