package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="StudyApndxIdentifier")
@XmlAccessorType (XmlAccessType.FIELD)
public class StudyApndxIdentifier extends SimpleIdentifier implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
