/**
 * Created On January 2, 2013
 */
package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author Raman
 *
 */

@XmlRootElement(name="Patients")
public class Patients implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4573949502407701466L;
	
	private List<UpdatePatientDemographics> patient = new ArrayList<UpdatePatientDemographics>();

	
	/**
	 * @return the patients
	 */
	public List<UpdatePatientDemographics> getPatient() {
		return patient;
	}
	/**
	 * @param patients the patients to set
	 */
	public void setPatient(List<UpdatePatientDemographics> patients) {
		this.patient = patients;
	}


	public void addPatient(UpdatePatientDemographics patientDemographics)
	{
		this.patient.add(patientDemographics); 
	}

}
