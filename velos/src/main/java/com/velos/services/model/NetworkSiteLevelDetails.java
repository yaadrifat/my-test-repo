package com.velos.services.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="NetworkSiteLevelDetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class NetworkSiteLevelDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1475038510992509642L;
	
	private String networkName;
	private String networkPK;
	private String siteName;
	private String sitePK;
	
	public String getNetworkName() {
		return networkName;
	}
	public void setNetworkName(String network_Name) {
		networkName = network_Name;
	}
	public String getNetworkPK() {
		return networkPK;
	}
	public void setNetworkPK(String network_PK) {
		networkPK = network_PK;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String site_Name) {
		siteName = site_Name;
	}
	public String getSitePK() {
		return sitePK;
	}
	public void setSitePK(String site_PK) {
		sitePK = site_PK;
	}
	
}