package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;
@XmlRootElement(name="NetworksDetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class NetworksDetails implements Serializable {
	


	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7518244979995357439L;

	/**
	 * 
	 */
	///private static final long serialVersionUID = -4579747787612751183L;

	private String networkName;
	private String sitePK;
	private String siteName;
	private String networkPK;
	
	
	public String getNetworkName() {
		if(networkName!=null  && !networkName.equals("") && networkName.contains("'")){
			try{
				char c =networkName.charAt(networkName.indexOf("'")+1);
				if(c!='\''){
					networkName=networkName.replaceAll("'", "''");
				}
				}catch(IndexOutOfBoundsException e){
					//do nothing
				}
		}
		return networkName;
	}
	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}
	public String getSitePK() {
		return sitePK;
	}
	public void setSitePK(String sitePK) {
		this.sitePK = sitePK;
	}
	public String getSiteName() {
		if(siteName!=null && !siteName.equals("") && siteName.contains("'") ){
			try{
			char c =siteName.charAt(siteName.indexOf("'")+1);
			if(c!='\''){
				siteName=siteName.replaceAll("'", "''");
			}
			}catch(IndexOutOfBoundsException e){
				//do nothing
			}
			
		}
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getNetworkPK() {
		return networkPK;
	}
	public void setNetworkPK(String networkPK) {
		this.networkPK = networkPK;
	}
	
	
			
}
