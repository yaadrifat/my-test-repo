package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Tarandeep Singh Bali
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="EventIdentifier")

public class EventNameIdentifier extends EventIdentifier {
	

/**
	 * 
	 */
	private static final long serialVersionUID = 6874929432458647515L;
protected String eventName;
protected SimpleIdentifier parentOID;
protected EventCostIdentifiers eventCostIdentifiers;//Bug Fix : 15987
   
   public EventNameIdentifier(){
		
	}
	   
   public String getEventName(){
	   
	   return eventName;
   }
   
   public void setEventName(String eventName){
	   
	   this.eventName=eventName;
   }
   //Begin Bug Fix: 10079 : Tarandeep Singh Bali
   public SimpleIdentifier getParentOID(){
	   
	   return parentOID;
   }
   
   public void setParentOID(SimpleIdentifier parentOID){
	   
	   this.parentOID=parentOID;
   }
   //End Bug Fix: 10079 : Tarandeep Singh Bali

   public EventCostIdentifiers getEventCostIdentifiers() {
	   return eventCostIdentifiers;
   }

   public void setEventCostIdentifiers(EventCostIdentifiers eventCostIdentifiers) {
	   this.eventCostIdentifiers = eventCostIdentifiers;
   }

   
}