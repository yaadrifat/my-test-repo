package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

public class EventStatuses implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5694997329398108442L;
	
	private List<EventStatusDetails> eventStatusDetails;

	public List<EventStatusDetails> getEventStatusDetails() {
		return eventStatusDetails;
	}

	public void setEventStatusDetails(List<EventStatusDetails> eventStatusDetails) {
		this.eventStatusDetails = eventStatusDetails;
	}
	
	public void addEventStatusDetails(EventStatusDetails eventStatusDetails){
		this.eventStatusDetails.add(eventStatusDetails);
	}

}
