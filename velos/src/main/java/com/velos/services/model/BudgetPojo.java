package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="budgetInfo")
@XmlAccessorType (XmlAccessType.FIELD)
public class BudgetPojo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	
	private String BUDGET_DESC;
	
	private Integer BUDGET_CREATOR;
	
	private String BUDGET_TYPE;
	/*
	private String BUDGET_STATUS;*/
	
	private Code BUDGET_CURRENCY;
	
	private String BUDGET_SITEFLAG;
	
	private String BUDGET_CALFLAG;
	
	private String BUDGET_RIGHTSCOPE;
	
	private String BUDGET_RIGHTS;
	
	private Code BUDGET_TEMPLATE;
	
	private String BUDGET_DELFLAG;
	
	private Code PK_CODELST_STATUS;
	
	private String BUDGET_COMBFLAG;
	private BudgetCalList budgetCalendars;
	
	public String getBUDGET_DESC() {
		return BUDGET_DESC;
	}
	public void setBUDGET_DESC(String bUDGETDESC) {
		BUDGET_DESC = bUDGETDESC;
	}
	public Integer getBUDGET_CREATOR() {
		return BUDGET_CREATOR;
	}
	public void setBUDGET_CREATOR(Integer bUDGETCREATOR) {
		BUDGET_CREATOR = bUDGETCREATOR;
	}
	public String getBUDGET_TYPE() {
		return BUDGET_TYPE;
	}
	public void setBUDGET_TYPE(String bUDGETTYPE) {
		BUDGET_TYPE = bUDGETTYPE;
	}
	/*public String getBUDGET_STATUS() {
		return BUDGET_STATUS;
	}
	public void setBUDGET_STATUS(String bUDGETSTATUS) {
		BUDGET_STATUS = bUDGETSTATUS;
	}*/
	public Code getBUDGET_CURRENCY() {
		return BUDGET_CURRENCY;
	}
	public void setBUDGET_CURRENCY(Code bUDGETCURRENCY) {
		BUDGET_CURRENCY = bUDGETCURRENCY;
	}
	public String getBUDGET_SITEFLAG() {
		return BUDGET_SITEFLAG;
	}
	public void setBUDGET_SITEFLAG(String bUDGETSITEFLAG) {
		BUDGET_SITEFLAG = bUDGETSITEFLAG;
	}
	public String getBUDGET_CALFLAG() {
		return BUDGET_CALFLAG;
	}
	public void setBUDGET_CALFLAG(String bUDGETCALFLAG) {
		BUDGET_CALFLAG = bUDGETCALFLAG;
	}
	public String getBUDGET_RIGHTSCOPE() {
		return BUDGET_RIGHTSCOPE;
	}
	public void setBUDGET_RIGHTSCOPE(String bUDGETRIGHTSCOPE) {
		BUDGET_RIGHTSCOPE = bUDGETRIGHTSCOPE;
	}
	public String getBUDGET_RIGHTS() {
		return BUDGET_RIGHTS;
	}
	public void setBUDGET_RIGHTS(String bUDGETRIGHTS) {
		BUDGET_RIGHTS = bUDGETRIGHTS;
	}
	public Code getBUDGET_TEMPLATE() {
		return BUDGET_TEMPLATE;
	}
	public void setBUDGET_TEMPLATE(Code bUDGETTEMPLATE) {
		BUDGET_TEMPLATE = bUDGETTEMPLATE;
	}
	public String getBUDGET_DELFLAG() {
		return BUDGET_DELFLAG;
	}
	public void setBUDGET_DELFLAG(String bUDGETDELFLAG) {
		BUDGET_DELFLAG = bUDGETDELFLAG;
	}
	public Code getPK_CODELST_STATUS() {
		return PK_CODELST_STATUS;
	}
	public void setPK_CODELST_STATUS(Code PKCODELSTSTATUS) {
		PK_CODELST_STATUS = PKCODELSTSTATUS;
	}
	public String getBUDGET_COMBFLAG() {
		return BUDGET_COMBFLAG;
	}
	public void setBUDGET_COMBFLAG(String bUDGETCOMBFLAG) {
		BUDGET_COMBFLAG = bUDGETCOMBFLAG;
	}
	public BudgetCalList getBudgetCalendars() {
		return budgetCalendars;
	}
	public void setBudgetCalendars(BudgetCalList budgetCalendars) {
		this.budgetCalendars = budgetCalendars;
	}

}
