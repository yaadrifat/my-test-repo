package com.velos.services.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Changes implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3267735221750829519L;
	protected Set<Change> Change = new HashSet<Change>();
	protected OutboundPatientProtocolIdentifier parentIdentifier; 	
	
	public OutboundPatientProtocolIdentifier getParentIdentifier() {
		return parentIdentifier;
	}


	public void setParentIdentifier(OutboundPatientProtocolIdentifier parentIdentifier) {
		this.parentIdentifier = parentIdentifier;
	}


	public Set<Change> getChange() {
		return Change;
	}

	
	public void setChange(Set<Change> change) {
		Change = change;
	}	
	
	public void addChange(Change change) {
		Change.add(change);
	}
	
}