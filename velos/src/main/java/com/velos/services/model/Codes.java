package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Codes implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3450128781797481041L;

	protected List<Code> codes = new ArrayList<Code>();
	 
    public Codes() {}
    
    public void add(Code codes){
        this.codes.add(codes);
    }
    
    public void addAll(List<Code> codes) {
        this.codes = codes;
    }
		
	public List<Code> getCodes() {
		return codes;
	}

	public void setCodes(List<Code> codes) {
		this.codes = codes;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}


