package com.velos.services.model;

import java.io.Serializable;
import java.util.HashMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="StudyFilterFormResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class StudyFilterFormResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7425138024062708918L;
	
	protected HashMap responeMap;

	public HashMap getResponeMap() {
		return responeMap;
	}

	public void setResponeMap(HashMap responeMap) {
		this.responeMap = responeMap;
	}
	
	

}
