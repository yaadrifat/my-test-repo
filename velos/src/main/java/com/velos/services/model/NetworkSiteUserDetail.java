package com.velos.services.model;
import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.velos.services.model.Netwrk;
import com.velos.services.model.SiteLevelDetail;
@XmlRootElement(name="networkSiteUser")
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(propOrder={"network", "networkName","siteLevelDetails","relationshipPK","userPKDetails"})
public class NetworkSiteUserDetail implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1365381990599553970L;
	private Netwrk network;
	private String networkName;
	private SiteLevelDetail siteLevelDetails;
	private String relationshipPK;
	private List<UserNetworkDetail> userIdentifier;
	
	
	
	public Netwrk getNetwork() {
		return network;
	}
	public void setNetwork(Netwrk network) {
		this.network = network;
	}
	public String getNetworkName() {
		return networkName;
	}
	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}
	public SiteLevelDetail getSiteLevelDetails() {
		return siteLevelDetails;
	}
	public void setSiteLevelDetails(SiteLevelDetail siteLevelDetails) {
		this.siteLevelDetails = siteLevelDetails;
	}
	public String getRelationshipPK() {
		return relationshipPK;
	}
	public void setRelationshipPK(String relationshipPK) {
		this.relationshipPK = relationshipPK;
	}
	public List<UserNetworkDetail> getUserIdentifier() {
		return userIdentifier;
	}
	public void setUserIdentifier(List<UserNetworkDetail> userIdentifier) {
		this.userIdentifier = userIdentifier;
	}

	
	
	
}
