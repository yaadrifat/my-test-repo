package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Tarandeep Singh Bali
 *
 */

public class ScheduleEventStatusIdentifiers implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3084221417974018293L;
	
	private List<ScheduleEventStatusIdentifier> scheduleEventStatusIdentifier;

	public List<ScheduleEventStatusIdentifier> getScheduleEventStatusIdentifier() {
		return scheduleEventStatusIdentifier;
	}

	public void setScheduleEventStatusIdentifier(
			List<ScheduleEventStatusIdentifier> scheduleEventStatusIdentifier) {
		this.scheduleEventStatusIdentifier = scheduleEventStatusIdentifier;
	}
	
	public void addScheduleEventStatusIdentifier(ScheduleEventStatusIdentifier
			                                     scheduleEventStatusIdentifier){
		
		this.scheduleEventStatusIdentifier.add(scheduleEventStatusIdentifier);
		
	}

}
