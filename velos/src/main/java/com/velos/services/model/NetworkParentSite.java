package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="network")
public class NetworkParentSite implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8622654236325396408L;
	private NetworkIdentifier networkIdentifier;
	private String networkName;
	private String relationshipPK;
	private Site parentSiteLevelDetails;
	public NetworkIdentifier getNetworkIdentifier() {
		return networkIdentifier;
	}
	public void setNetworkIdentifier(NetworkIdentifier networkIdentifier) {
		this.networkIdentifier = networkIdentifier;
	}
	public String getNetworkName() {
		if(networkName!=null  && !networkName.equals("") && networkName.contains("'")){
			try{
				char c =networkName.charAt(networkName.indexOf("'")+1);
				if(c!='\''){
					networkName=networkName.replaceAll("'", "''");
				}
				}catch(IndexOutOfBoundsException e){
					//do nothing
				}
		}
		return networkName;
	}
	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}
	public String getRelationshipPK() {
		return relationshipPK;
	}
	public void setRelationshipPK(String relationshipPK) {
		this.relationshipPK = relationshipPK;
	}
	public Site getParentSiteLevelDetails() {
		return parentSiteLevelDetails;
	}
	public void setParentSiteLevelDetails(Site parentSiteLevelDetails) {
		this.parentSiteLevelDetails = parentSiteLevelDetails;
	}
}
