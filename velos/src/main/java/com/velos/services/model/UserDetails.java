package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(namespace="user")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8246075645951661751L;
	private UserIdentifiers userIdentifier;
	private List<UserRoles> role;
	private List<NVPair> moreNetworkUserDetails;

	public UserIdentifiers getUserIdentifier() {
		return userIdentifier;
	}
	public void setUserIdentifier(UserIdentifiers userIdentifier) {
		this.userIdentifier = userIdentifier;
	}
	public List<UserRoles> getRole() {
		return role;
	}
	public void setRole(List<UserRoles> role) {
		this.role = role;
	}
	public List<NVPair> getMoreNetworkUserDetails() {
		return moreNetworkUserDetails;
	}
	public void setMoreNetworkUserDetails(List<NVPair> moreNetworkUserDetails) {
		this.moreNetworkUserDetails = moreNetworkUserDetails;
	}
}
