package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="BudgetCalList")
@XmlAccessorType (XmlAccessType.FIELD)
public class BudgetCalList implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<BgtCalPojo> budgetCalendar ;
	public List<BgtCalPojo> getBudgetCalendar() {
		return budgetCalendar;
	}
	public void setBudgetCalendar(List<BgtCalPojo> budgetCalendar) {
		this.budgetCalendar = budgetCalendar;
	}
	
}
