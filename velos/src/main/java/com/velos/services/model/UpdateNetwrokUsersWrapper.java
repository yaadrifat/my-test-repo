package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="updateNetworkSiteUser")
@XmlAccessorType(XmlAccessType.FIELD)
public class UpdateNetwrokUsersWrapper implements Serializable{


	private static final long serialVersionUID = -5089441044488065254L;
	
	private List<NetSiteUser> networkSiteUser=new ArrayList<NetSiteUser>();
	
	public List<NetSiteUser> getNetworkSiteUser() {
		return networkSiteUser;
	}
	public void setNetworkSiteUser(List<NetSiteUser> networkSiteUser) {
		this.networkSiteUser = networkSiteUser;
	}
	
}
