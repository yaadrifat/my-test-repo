package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="createNetworkSite")
@XmlAccessorType(XmlAccessType.FIELD)
public class NetworkSiteWrapper implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8622654246315497508L;
	private NetworkParentSite network;
	private List<Site> site;
	
	public NetworkParentSite getNetwork() {
		return network;
	}
	public void setNetwork(NetworkParentSite network) {
		this.network = network;
	}
	public List<Site> getSite() {
		return site;
	}
	public void setSite(List<Site> site) {
		this.site = site;
	}
}
