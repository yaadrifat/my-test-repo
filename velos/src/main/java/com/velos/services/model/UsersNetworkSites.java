package com.velos.services.model;

import java.io.Serializable;
import java.util.List;



public class UsersNetworkSites implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5385200038415014252L;

	private UserNetworkDetail userIdentifier;
	private List<NetworkUsersRoles> role;
	private List<NVPair> moreUsersDetails;
	
	public UserNetworkDetail getUserIdentifier() {
		return userIdentifier;
	}
	public void setUserIdentifier(UserNetworkDetail userIdentifier) {
		this.userIdentifier = userIdentifier;
	}
	public List<NetworkUsersRoles> getRole() {
		return role;
	}
	public void setRole(List<NetworkUsersRoles> role) {
		this.role = role;
	}
	public List<NVPair> getMoreUsersDetails() {
		return moreUsersDetails;
	}
	public void setMoreUsersDetails(List<NVPair> moreUsersDetails) {
		this.moreUsersDetails = moreUsersDetails;
	}
	
	
	
}
