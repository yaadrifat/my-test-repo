package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.velos.services.form.FormDesignDAO;

public class PatientForms implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -528778118234310698L;

	protected List<LinkedFormDesign> patientForms = new ArrayList<LinkedFormDesign>();

	protected List<FormDesignDAO> frmDesignDao= new ArrayList<FormDesignDAO>();
	
	protected int formCount;
	
	/**
	 * @return the formCount
	 */
	public int getFormCount() {
		return formCount;
	}

	/**
	 * @param formCount the formCount to set
	 */
	public void setFormCount(int formCount) {
		this.formCount = formCount;
	}

	public List<LinkedFormDesign> getPatientForms() {
		return patientForms;
	}

	public void setPatientForms(List<LinkedFormDesign> patientForms) {
		this.patientForms = patientForms;
	}
	

	public void addAll(List<LinkedFormDesign> patientForms) {
		this.patientForms = patientForms;
	}
	
	public void add(LinkedFormDesign patientForms) {
		this.patientForms.add(patientForms);
	}
	
	public void add(FormDesignDAO frmDesignDao) {
		this.frmDesignDao.add(frmDesignDao);
	}


}
