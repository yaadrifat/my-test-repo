package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Options")
public class Options  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7548657618084605489L;
	private List<Option> option = new ArrayList<Option>();

	public List<Option> getOption() {
		return option;
	}

	public void setOption(List<Option> option) {
		this.option = option;
	}
	
	public void addOption(Option option) {
		this.option.add(option);
	}
	

}
