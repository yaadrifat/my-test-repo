package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Milestones")
@XmlAccessorType(XmlAccessType.FIELD)
public class MilestoneList implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private StudyIdentifier studyIdentifier;
	
	protected List<Milestonee> milestone = new ArrayList<Milestonee>();
	public List<Milestonee> getMilestone() {
		return milestone;
	}
	public void setMilestone(List<Milestonee> milestone) {
		this.milestone = milestone;
	}
	
	public void add(Milestonee milestone){
		this.milestone.add(milestone);
	}
	public void addAll(List<Milestonee> milestone) {
		this.milestone = milestone;
	}
	public StudyIdentifier getStudyIdentifier() {
		return studyIdentifier;
	}
	public void setStudyIdentifier(StudyIdentifier studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	}

}
