
package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Data transfer object representing a study status.
 * @author dylan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyStatuses")
public class StudyStatuses
    implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = -8643891583943218535L;
    /**
     * Generated serialVersionUID
     */
 
    protected List<StudyStatus> studyStatus;
    
    protected StudyIdentifier parentIdentifier;
    
    @NotNull 
    @Size(min=1)
    @Valid
	public List<StudyStatus> getStudyStatus() {
        return studyStatus;
    }

    public void setStudyStatus(List<StudyStatus> studyStatus) {
        this.studyStatus = studyStatus;
    }
    
    public StudyIdentifier getParentIdentifier() {
		return parentIdentifier;
	}


	public void setParentIdentifier(StudyIdentifier parentIdentifier) {
		this.parentIdentifier = parentIdentifier;
	}
    
}
