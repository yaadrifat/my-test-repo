/**
 * Created On Sep 26, 2011
 */
package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyTeamIdentifier")
public class StudyTeamIdentifier extends SimpleIdentifier {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1935467852791918927L;

}
