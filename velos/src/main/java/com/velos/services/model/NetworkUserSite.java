package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Site")
public class NetworkUserSite implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5647717652994226380L;
	private List<NetworkSites> site=new ArrayList<NetworkSites>();
	
	public List<NetworkSites> getSites() {
		return site;
	}
	public void setSites(List<NetworkSites> site) {
		this.site = site;
	}
}
