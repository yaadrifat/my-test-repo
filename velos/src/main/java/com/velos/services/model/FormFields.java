/**
 * Created On Aug 25, 2011
 */
package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Fields")
public class FormFields implements Serializable {
	
	private List<FormField> field = new ArrayList<FormField>();

	/**
	 * @param field the field to set
	 */
	public void setField(List<FormField> field) {
		this.field = field;
	}

	/**
	 * @return the field
	 */
	public List<FormField> getField() {
		return field;
	} 
	
	
	public void addField(FormField field)
	{
	    this.field.add(field); 
	}

}
