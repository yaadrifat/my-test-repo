package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="createNetwork")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreateNetwork implements Serializable {

	private static final long serialVersionUID = 1774865068615434507L;
	private List<NetworkSiteData> networkSite;

	public List<NetworkSiteData> getNetworkSite() {
		return networkSite;
	}

	public void setNetworkSite(List<NetworkSiteData> networkSite) {
		this.networkSite = networkSite;
	}

}
