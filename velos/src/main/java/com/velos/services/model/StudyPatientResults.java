package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyPatientResults")
public class StudyPatientResults implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2310984770669007203L;

	protected ArrayList<StudyPatient> studyPatient = new ArrayList<StudyPatient>();
	
	private Integer pageNumber = null;
	
	private Long pageSize = null;
	
	private Long totalCount = null;
	
	public ArrayList<StudyPatient> getStudyPatient() {
		return studyPatient;
	}

	public void setStudyPatient(ArrayList<StudyPatient> studyPatient) {
		this.studyPatient = studyPatient;
	}

	public void addStudyPatient(StudyPatient studyPatient){
		this.studyPatient.add(studyPatient);
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Long getPageSize() {
		return pageSize;
	}

	public void setPageSize(Long pageSize) {
		this.pageSize = pageSize;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}

}
