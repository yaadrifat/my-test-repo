package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientSchedules")
public class PatientSchedules implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7051232166825395993L;
	protected List<PatientScheduleSummary> patientSchedule = new ArrayList<PatientScheduleSummary>();
	protected PatientIdentifier patientIdentifier;
	protected StudyIdentifier studyIdentifier;
	public List<PatientScheduleSummary> getPatientSchedule() {
		return patientSchedule;
	}
	public void setPatientSchedule(List<PatientScheduleSummary> patientSchedule) {
		this.patientSchedule = patientSchedule;
	}
	public PatientIdentifier getPatientIdentifier() {
		return patientIdentifier;
	}
	public void setPatientIdentifier(PatientIdentifier patientIdentifier) {
		this.patientIdentifier = patientIdentifier;
	}
	public StudyIdentifier getStudyIdentifier() {
		return studyIdentifier;
	}
	public void setStudyIdentifier(StudyIdentifier studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	}
	
	public void addPatientSchedule(PatientScheduleSummary patientSchedule){
		this.patientSchedule.add(patientSchedule);
	}
}
