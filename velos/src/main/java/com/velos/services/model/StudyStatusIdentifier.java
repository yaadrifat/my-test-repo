package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="studyStatusIdentifier")
public class StudyStatusIdentifier extends SimpleIdentifier {

	/**
	 * @author Kanwal
	 */
	private static final long serialVersionUID = -9136985562622615665L;

}
