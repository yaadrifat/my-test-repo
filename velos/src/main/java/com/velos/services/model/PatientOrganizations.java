/**
 * Created On Jun 17, 2011
 */
package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

/**
 * @author Kanwaldeep
 *
 */
public class PatientOrganizations implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2685380176926660810L;
	protected List<PatientOrganization> organization = new ArrayList<PatientOrganization>();

	@Valid
	public List<PatientOrganization> getOrganization() {
		return organization;
	}

	public void setOrganization(List<PatientOrganization> organization) {
		this.organization = organization;
	} 	
	
	public void addOrganization(PatientOrganization patientOrganization){
		this.organization.add(patientOrganization);
	}

}
