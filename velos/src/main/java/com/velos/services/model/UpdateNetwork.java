package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="updateNetwork")
@XmlAccessorType(XmlAccessType.FIELD)
public class UpdateNetwork implements Serializable{
	
	private static final long serialVersionUID = -9120377574985303736L;
	private List<NetSiteDetails> networkSite=new ArrayList<NetSiteDetails>();
	
	public List<NetSiteDetails> getNetworkSiteDetailsList() {
		return networkSite;
	}
	public void setNetworkSiteDetailsList(List<NetSiteDetails> networkSite) {
		this.networkSite = networkSite;
	}

}
