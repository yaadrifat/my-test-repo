package com.velos.services.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.velos.services.model.SiteLevelDetail;

@XmlRootElement(namespace="network")
@XmlAccessorType(XmlAccessType.FIELD)
public class NetworkDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9216511212400545952L;
	private SimpleIdentifier networkIdentifier;
	private String networkName;
	private String relationshipPK;
	private SiteLevelDetail siteLevelDetails;
	
	public SimpleIdentifier getNetworkIdentifier(){
		return networkIdentifier;
	}
	
	public void setNetworkIdentifier(SimpleIdentifier networkIdentifier){
		this.networkIdentifier = networkIdentifier;
	}
	
	public String getName() {
		return networkName;
	}

	public void setName(String networkName) {
		this.networkName = networkName;
	}

	public String getRelationshipPK() {
		return relationshipPK;
	}

	public void setRelationshipPK(String relationshipPK) {
		this.relationshipPK = relationshipPK;
	}

	public SiteLevelDetail getSiteLevelDetails() {
		return siteLevelDetails;
	}

	public void setSiteLevelDetails(SiteLevelDetail siteLevelDetails) {
		this.siteLevelDetails = siteLevelDetails;
	}

}
