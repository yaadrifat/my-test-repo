package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="network")
@XmlAccessorType(XmlAccessType.FIELD)
public class NetSiteDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6990294635060115085L;
	/**
	 * 
	 */
	private NetIdentifier network;
	private SimpleIdentifier networkIdentifier;
	private String networkName;
	private SiteLevelDetail siteLevelDetail;
	private String relationshipPK;
	private Code siteStatus;
	private String siteStatusDate;
	private String siteStatusNotes;
	private Code newSiteRelationshipType;
	private List<NVPair> moreSiteDetailsFields;
	
	
	public NetIdentifier getNetworkIdentifier() {
		return network;
	}
	public void setNetworkIdentifier(NetIdentifier network) {
		this.network = network;
	}
	public String getNetworkName() {
		if(networkName!=null  && !networkName.equals("") && networkName.contains("'")){
			try{
				char c =networkName.charAt(networkName.indexOf("'")+1);
				if(c!='\''){
					networkName=networkName.replaceAll("'", "''");
				}
				}catch(IndexOutOfBoundsException e){
					//do nothing
				}
		}
		return networkName;
	}
	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}
	public SiteLevelDetail getSiteLevelDetail() {
		return siteLevelDetail;
	}
	public void setSiteLevelDetail(SiteLevelDetail siteLevelDetail) {
		this.siteLevelDetail = siteLevelDetail;
	}
	public String getRelationshipPK() {
		return relationshipPK;
	}
	public void setRelationshipPK(String relationshipPK) {
		this.relationshipPK = relationshipPK;
	}
	public Code getSiteStatus() {
		return siteStatus;
	}
	public void setSiteStatus(Code siteStatus) {
		this.siteStatus = siteStatus;
	}
	public String getSiteStatusDate() {
		return siteStatusDate;
	}
	public void setSiteStatusDate(String siteStatusDate) {
		this.siteStatusDate = siteStatusDate;
	}
	public String getSiteStatusNotes() {
		return siteStatusNotes;
	}
	public void setSiteStatusNotes(String siteStatusNotes) {
		this.siteStatusNotes = siteStatusNotes;
	}
	public Code getNewSiteRelationshipType() {
		return newSiteRelationshipType;
	}
	public void setNewSiteRelationshipType(Code newSiteRelationshipType) {
		this.newSiteRelationshipType = newSiteRelationshipType;
	}
	public List<NVPair> getMoreSiteDetails() {
		return moreSiteDetailsFields;
	}
	public void setMoreSiteDetails(List<NVPair> moreSiteDetailsFields) {
		this.moreSiteDetailsFields = moreSiteDetailsFields;
	}
	public SimpleIdentifier getNetIdentifier() {
		return networkIdentifier;
	}
	public void setNetIdentifier(SimpleIdentifier networkIdentifier) {
		this.networkIdentifier = networkIdentifier;
	}
}
