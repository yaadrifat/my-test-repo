package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="siteLevelDetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class SiteLevelDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1387968019338272376L;
	private String siteName;
	private String  level;
	private Code siteRelationshipType;
	
	public String getSiteName() {
		if(siteName!=null  && !siteName.equals("") && siteName.contains("'")){
			try{
				char c =siteName.charAt(siteName.indexOf("'")+1);
				if(c!='\''){
					siteName=siteName.replaceAll("'", "''");
				}
				}catch(IndexOutOfBoundsException e){
					//do nothing
				}
		}
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public Code getRelationshipType() {
		return siteRelationshipType;
	}
	public void setRelationshipType(Code siteRelationshipType) {
		this.siteRelationshipType = siteRelationshipType;
	}
	
}
