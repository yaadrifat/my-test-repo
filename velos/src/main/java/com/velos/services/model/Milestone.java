package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Milestone")
@XmlAccessorType(XmlAccessType.FIELD)
public class Milestone implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Milestone()
	{
			}
	protected List<MilestoneVDAPojo> milestone = new ArrayList<MilestoneVDAPojo>();
	public List<MilestoneVDAPojo> getMilestone() {
		return milestone;
	}
	public void setMilestone(List<MilestoneVDAPojo> milestone) {
		this.milestone = milestone;
	}
	
	public void add(MilestoneVDAPojo milestone){
		this.milestone.add(milestone);
	}
	public void addAll(List<MilestoneVDAPojo> milestone) {
		this.milestone = milestone;
	}
	

}
