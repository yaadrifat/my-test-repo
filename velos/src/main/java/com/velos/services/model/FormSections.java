/**
 * Created On Aug 25, 2011
 */
package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Sections")
public class FormSections implements Serializable {
	
	private List<FormSection> section = new ArrayList();

	/**
	 * @param section the section to set
	 */
	public void setSection(List<FormSection> section) {
		this.section = section;
	}

	/**
	 * @return the section
	 */
	public List<FormSection> getSection() {
		return section;
	} 
	
	
	public void addSection(FormSection section)
	{
		if(section != null)	this.section.add(section); 
	}

}
