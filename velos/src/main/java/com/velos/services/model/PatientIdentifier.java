/**
 * 
 */
package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**Identifier Object for Patient, Identifies a patient uniquely in eResearch
 * @author virendra
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientIdentifier")
public class PatientIdentifier 
	extends SimpleIdentifier
	implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1427599014082534019L;

	private String patientId = null;
	private OrganizationIdentifier organizationId = null; 

	public PatientIdentifier(){
	}
	
	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	/**
	 * @param organizationId the organizationId to set
	 */
	public void setOrganizationId(OrganizationIdentifier organizationId) {
		this.organizationId = organizationId;
	}

	/**
	 * @return the organizationId
	 */
	public OrganizationIdentifier getOrganizationId() {
		return organizationId;
	}

}
