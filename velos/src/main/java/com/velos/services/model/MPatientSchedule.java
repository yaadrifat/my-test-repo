package com.velos.services.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="MPatientSchedule")
public class MPatientSchedule implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7544980761312853287L;
	
	private PatientIdentifier patientIdentifier;
	private StudyIdentifier studyIdentifier;
	private String calendarName;
	private CalendarIdentifier calendarIdentifier;
	private Date startDate;
	private VisitIdentifier visitIdentifier;
	public PatientIdentifier getPatientIdentifier() {
		return patientIdentifier;
	}
	public void setPatientIdentifier(PatientIdentifier patientIdentifier) {
		this.patientIdentifier = patientIdentifier;
	}
	public StudyIdentifier getStudyIdentifier() {
		return studyIdentifier;
	}
	public void setStudyIdentifier(StudyIdentifier studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	}
	public String getCalendarName() {
		return calendarName;
	}
	public void setCalendarName(String calendarName) {
		this.calendarName = calendarName;
	}
	public CalendarIdentifier getCalendarIdentifier() {
		return calendarIdentifier;
	}
	public void setCalendarIdentifier(CalendarIdentifier calendarIdentifier) {
		this.calendarIdentifier = calendarIdentifier;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public VisitIdentifier getVisitIdentifier() {
		return visitIdentifier;
	}
	public void setVisitIdentifier(VisitIdentifier visitIdentifier) {
		this.visitIdentifier = visitIdentifier;
	}
	
	

}
