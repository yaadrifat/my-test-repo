package com.velos.services.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * model class for StudyPatient, Identifies a Patient on a study
 * @author Virendra
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyPatient")
public class StudyPatient implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	protected PatientIdentifier patientIdentifier;
	protected String studyPatId;
	protected String studyPatFirstName;
	protected String studyPatLastName;
	protected OrganizationIdentifier studyPatEnrollingSite;
	protected Date studyPatEnrollDate;
	protected String studyPatLastVisit;
	protected Date studyPatDoneOn;
	protected Date studyPatNextDue;
	protected Code studyPatStatus;
	protected PatientStudyStatusIdentifier studyPatStatId;
	protected PatientProtIdentifier patprotId;
	protected UserIdentifier studyPatEnrolledBy;
	protected UserIdentifier studyPatAssignedTo;
	protected UserIdentifier studyPatPhysician;
	protected OrganizationIdentifier StudyPatTreatmentOrganization;
	protected StudyIdentifier patStudyIdentifier;
	protected Integer orgRights;
	protected Integer studyPatRights;
	
	public PatientIdentifier getPatientIdentifier() {
		return patientIdentifier;
	}
	public void setPatientIdentifier(PatientIdentifier patientIdentifier) {
		this.patientIdentifier = patientIdentifier;
	}
	public String getStudyPatId() {
		return studyPatId;
	}
	public void setStudyPatId(String studyPatId) {
		this.studyPatId = studyPatId;
	}
	public String getStudyPatFirstName() {
		return studyPatFirstName;
	}
	public void setStudyPatFirstName(String studyPatFirstName) {
		this.studyPatFirstName = studyPatFirstName;
	}
	public String getStudyPatLastName() {
		return studyPatLastName;
	}
	public void setStudyPatLastName(String studyPatLastName) {
		this.studyPatLastName = studyPatLastName;
	}
	public OrganizationIdentifier getStudyPatEnrollingSite() {
		return studyPatEnrollingSite;
	}
	public void setStudyPatEnrollingSite(
			OrganizationIdentifier studyPatEnrollingSite) {
		this.studyPatEnrollingSite = studyPatEnrollingSite;
	}
	public Date getStudyPatEnrollDate() {
		return studyPatEnrollDate;
	}
	public void setStudyPatEnrollDate(Date studyPatEnrollDate) {
		this.studyPatEnrollDate = studyPatEnrollDate;
	}
	public String getStudyPatLastVisit() {
		return studyPatLastVisit;
	}
	public void setStudyPatLastVisit(String studyPatLastVisit) {
		this.studyPatLastVisit = studyPatLastVisit;
	}
	public Date getStudyPatDoneOn() {
		return studyPatDoneOn;
	}
	public void setStudyPatDoneOn(Date studyPatDoneOn) {
		this.studyPatDoneOn = studyPatDoneOn;
	}
	public Date getStudyPatNextDue() {
		return studyPatNextDue;
	}
	public void setStudyPatNextDue(Date studyPatNextDue) {
		this.studyPatNextDue = studyPatNextDue;
	}
	public Code getStudyPatStatus() {
		return studyPatStatus;
	}
	public void setStudyPatStatus(Code studyPatStatus) {
		this.studyPatStatus = studyPatStatus;
	}
	public UserIdentifier getStudyPatEnrolledBy() {
		return studyPatEnrolledBy;
	}
	public void setStudyPatEnrolledBy(UserIdentifier studyPatEnrolledBy) {
		this.studyPatEnrolledBy = studyPatEnrolledBy;
	}
	public UserIdentifier getStudyPatAssignedTo() {
		return studyPatAssignedTo;
	}
	public void setStudyPatAssignedTo(UserIdentifier studyPatAssignedTo) {
		this.studyPatAssignedTo = studyPatAssignedTo;
	}
	public UserIdentifier getStudyPatPhysician() {
		return studyPatPhysician;
	}
	public void setStudyPatPhysician(UserIdentifier studyPatPhysician) {
		this.studyPatPhysician = studyPatPhysician;
	}
	public OrganizationIdentifier getStudyPatTreatmentOrganization() {
		return StudyPatTreatmentOrganization;
	}
	public void setStudyPatTreatmentOrganization(
			OrganizationIdentifier studyPatTreatmentOrganization) {
		StudyPatTreatmentOrganization = studyPatTreatmentOrganization;
	}
	public StudyIdentifier getPatStudyIdentifier() {
		return patStudyIdentifier;
	}
	public void setPatStudyIdentifier(StudyIdentifier patStudyIdentifier) {
		this.patStudyIdentifier = patStudyIdentifier;
	}
	public PatientStudyStatusIdentifier getStudyPatStatId() {
		return studyPatStatId;
	}
	public void setStudyPatStatId(PatientStudyStatusIdentifier studyPatStatId) {
		this.studyPatStatId = studyPatStatId;
	}
	public PatientProtIdentifier getPatprotId() {
		return patprotId;
	}
	public void setPatprotId(PatientProtIdentifier patprotId) {
		this.patprotId = patprotId;
	}
	public Integer getOrgRights() {
		return orgRights;
	}
	public void setOrgRights(Integer orgRights) {
		this.orgRights = orgRights;
	}
	public Integer getStudyPatRights() {
		return studyPatRights;
	}
	public void setStudyPatRights(Integer studyPatRights) {
		this.studyPatRights = studyPatRights;
	}
}