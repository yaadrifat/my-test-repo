/**
 * Created On Jun 17, 2011
 */
package com.velos.services.model;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

/**
 * @author Kanwaldeep
 *
 */
public class PatientOrganization extends ServiceObjects{
	/**
	 * 
	 */
	private static final long serialVersionUID = 821500830146546231L;
	
	protected SimpleIdentifier systemId;
	
	protected OrganizationIdentifier organizationID; 
	
	protected String facilityID; 
	
	protected Date registrationDate; 
	//Raman bug#11974
	protected boolean emptyRegistrationDate;
	
	protected UserIdentifier provider; 
	
	protected String otherProvider; 
	
	protected SpecialityAccess specialtyAccess;
	
	protected AccessRights access; 
	
	protected boolean Default;

	
	public boolean isEmptyRegistrationDate() {
		return emptyRegistrationDate;
	}

	public void setEmptyRegistrationDate(boolean emptyRegistrationDate) {
		this.emptyRegistrationDate = emptyRegistrationDate;
	}

	@NotNull
	public OrganizationIdentifier getOrganizationID() {
		return this.organizationID;
	}

	public void setOrganizationID(OrganizationIdentifier organizationID) {
		this.organizationID = organizationID;
	}

	@NotNull
	public String getFacilityID() {
		return facilityID;
	}

	public void setFacilityID(String facilityID) {
		this.facilityID = facilityID;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public UserIdentifier getProvider() {
		return provider;
	}

	public void setProvider(UserIdentifier provider) {
		this.provider = provider;
	}

	public String getOtherProvider() {
		return otherProvider;
	}

	public void setOtherProvider(String otherProvider) {
		this.otherProvider = otherProvider;
	}

	public SpecialityAccess getSpecialtyAccess() {
		return specialtyAccess;
	}

	public void setSpecialtyAccess(SpecialityAccess specialtyAccess) {
		this.specialtyAccess = specialtyAccess;
	}
	
	public AccessRights getAccess() {
		return access;
	}

	public void setAccess(AccessRights access) {
		this.access = access;
	}

	@NotNull
	public boolean isDefault() {
		return this.Default;
	}

	public void setDefault(boolean isDefault) {
		this.Default = isDefault;
	}

	public SimpleIdentifier getSystemId() {
		return systemId;
	}

	public void setSystemId(SimpleIdentifier systemId) {
		this.systemId = systemId;
	} 

}
