package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="LineItemName")
public class LineItemNameIdentifier extends SimpleIdentifier{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String lineItemName;

	public String getLineItemName() {
		return lineItemName;
	}

	public void setLineItemName(String lineItemName) {
		this.lineItemName = lineItemName;
	}

}
