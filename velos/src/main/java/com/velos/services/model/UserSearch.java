/**
 * 
 */
package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessorType;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlRootElement;

import com.velos.esch.service.util.StringUtil;
import com.velos.services.model.FormField.SortOrder;
import com.velos.services.util.EnumUtil;

/**
 * Search criteria for User Search
 */
@XmlRootElement(name="UserSearch")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserSearch implements Serializable {
	private static final long serialVersionUID = -6203544386177036406L;
	
	private int userPK;
	private String firstName;
	private String lastName;
	private String loginName;
	private String email;
	private Code jobType;
	private OrganizationIdentifier organization;
	protected List<MoreDetailValue> usermoredetails;
	private GroupIdentifier group;
	private StudyTeamIdentifier studyTeam;
	private int pageNumber;
	private int pageSize;
	private UserSearchOrderBy sortBy;
	private SortOrder sortOrder;
	
	public enum UserSearchOrderBy {
		firstName,
		lastName,
		email,
		siteName,
		userStatus,
		userLoginName,
		PK
	}
	
	public int getUserPK() {
		return userPK;
	}
	public void setUserPK(int userPK) {
		this.userPK = userPK;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Code getJobType() {
		return jobType;
	}
	public void setJobType(Code jobType) {
		this.jobType = jobType;
	}
	public GroupIdentifier getGroup() {
		return group;
	}
	public void setGroup(GroupIdentifier group) {
		this.group = group;
	}
	public StudyTeamIdentifier getStudyTeam() {
		return studyTeam;
	}
	public void setStudyTeam(StudyTeamIdentifier studyTeam) {
		this.studyTeam = studyTeam;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public UserSearchOrderBy getSortBy() {
		return sortBy;
	}
	public void setSortBy(UserSearchOrderBy sortBy) {
		this.sortBy = sortBy;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	public void setSortOrder(String sortOrder) {
		this.sortOrder = EnumUtil.getEnumType(SortOrder.class, sortOrder);
	}
	public OrganizationIdentifier getOrganization() {
		return organization;
	}
	public void setOrganization(OrganizationIdentifier organization) {
		this.organization = organization;
	}
	
	public List<MoreDetailValue> getMoreUserDetails() {
		return usermoredetails;
	}
	public void setMoreUserDetails(List<MoreDetailValue> moreUserDetails) {
		this.usermoredetails = moreUserDetails;
	}

}
