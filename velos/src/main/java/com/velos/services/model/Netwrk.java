package com.velos.services.model;


import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="network")
@XmlAccessorType(XmlAccessType.FIELD)
public class Netwrk implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5910214281803464414L;
	
	 private SimpleIdentifier networkIdentifier;

	public SimpleIdentifier getNetworkIdentifier() {
		return networkIdentifier;
	}

	public void setNetworkIdentifier(SimpleIdentifier networkIdentifier) {
		this.networkIdentifier = networkIdentifier;
	}

	
	

}
