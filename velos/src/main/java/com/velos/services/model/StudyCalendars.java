package com.velos.services.model;

import java.io.Serializable;

public class StudyCalendars implements Serializable{

	/**
	 * @author navneet
	 */
	private static final long serialVersionUID = 1844769611777533543L;
	
	protected CalendarIdentifier calendarIdentifier;
	protected String calendarName;
	protected Code calendarStatus;
	public CalendarIdentifier getCalendarIdentifier() {
		return calendarIdentifier;
	}
	public void setCalendarIdentifier(CalendarIdentifier calendarIdentifier) {
		this.calendarIdentifier = calendarIdentifier;
	}
	public String getCalendarName() {
		return calendarName;
	}
	public void setCalendarName(String calendarName) {
		this.calendarName = calendarName;
	}
	public Code getCalendarStatus() {
		return calendarStatus;
	}
	public void setCalendarStatus(Code calendarStatus) {
		this.calendarStatus = calendarStatus;
	}


}
