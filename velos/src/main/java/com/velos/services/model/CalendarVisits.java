/**
 * Created On May 18, 2011
 */
package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlRootElement(name="Visits")
@XmlAccessorType(XmlAccessType.FIELD)
public class CalendarVisits  extends ServiceObjects{	
	
	private static final long serialVersionUID = -1726967087017467972L;
	
	private List<CalendarVisit> visit = new ArrayList<CalendarVisit>();

	/**
	 * @param visits the visits to set
	 */
	public void setVisit(List<CalendarVisit> visit) {
		this.visit = visit;
	}

	/**
	 * @return the visits
	 */
	@Valid
	public List<CalendarVisit> getVisit() {
		return visit;
	} 
	
	public void setVisit(CalendarVisit visitin)
	{
		visit.add(visitin); 
	}
	

}
