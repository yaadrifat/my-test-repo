package com.velos.services.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientScheduleSummary")
public class PatientScheduleSummary implements Serializable {

	private static final long serialVersionUID = -3880305272496672940L;
	
	private Date startDate;
	private String calendarName;
	private boolean isCurrent;
	private PatientProtocolIdentifier scheduleIdentifier;
	
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public String getCalendarName() {
		return calendarName;
	}
	public void setCalendarName(String calendarName) {
		this.calendarName = calendarName;
	}
	public boolean isCurrent() {
		return isCurrent;
	}
	public void setCurrent(boolean isCurrent) {
		this.isCurrent = isCurrent;
	}
	public PatientProtocolIdentifier getScheduleIdentifier() {
		return scheduleIdentifier;
	}
	public void setScheduleIdentifier(PatientProtocolIdentifier scheduleIdentifier) {
		this.scheduleIdentifier = scheduleIdentifier;
	}

}
