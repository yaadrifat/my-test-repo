package com.velos.services.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyStatus")
public class StudyStatusParent implements Serializable {
	private static final long serialVersionUID = 2564378396863581078L;

	protected StudyStatusIdentifier studyStatusIdentifier; 

	protected String documented_by;

    protected String assigned_to;

    protected String status_valid_From;

    protected String status_valid_until;

    protected String meeting_date;
    
    protected Code review_board;

    protected Code outcome;

    protected String notes;
 
    protected String current_flag;
    
    protected String org_current_rep_status;

	public StudyStatusIdentifier getStudyStatusIdentifier() {
		return studyStatusIdentifier;
	}

	public void setStudyStatusIdentifier(StudyStatusIdentifier studyStatusIdentifier) {
		this.studyStatusIdentifier = studyStatusIdentifier;
	}

	public String getDocumented_by() {
		return documented_by;
	}

	public void setDocumented_by(String documented_by) {
		this.documented_by = documented_by;
	}

	public String getAssigned_to() {
		return assigned_to;
	}

	public void setAssigned_to(String assigned_to) {
		this.assigned_to = assigned_to;
	}

	public String getStatus_valid_From() {
		return status_valid_From;
	}

	public void setStatus_valid_From(String status_valid_From) {
		this.status_valid_From = status_valid_From;
	}

	public String getStatus_valid_until() {
		return status_valid_until;
	}

	public void setStatus_valid_until(String status_valid_until) {
		this.status_valid_until = status_valid_until;
	}

	public String getMeeting_date() {
		return meeting_date;
	}

	public void setMeeting_date(String meeting_date) {
		this.meeting_date = meeting_date;
	}

	public Code getReview_board() {
		return review_board;
	}

	public void setReview_board(Code review_board) {
		this.review_board = review_board;
	}

	public Code getOutcome() {
		return outcome;
	}

	public void setOutcome(Code outcome) {
		this.outcome = outcome;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getCurrent_flag() {
		return current_flag;
	}

	public void setCurrent_flag(String current_flag) {
		this.current_flag = current_flag;
	}

	public String getOrg_current_rep_status() {
		return org_current_rep_status;
	}

	public void setOrg_current_rep_status(String org_current_rep_status) {
		this.org_current_rep_status = org_current_rep_status;
	}
    
}
