package com.velos.services.model;
import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="removeNetwork")
@XmlAccessorType(XmlAccessType.FIELD)
public class RemoveNetwork implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8986700031970670689L;
	/**
	 * 
	 */
	
	List<NetworkRemoveSite> networkSite=new ArrayList<NetworkRemoveSite>();

	public List<NetworkRemoveSite> getNetworkSiteDetailList() {
		return networkSite;
	}

	public void setNetworkSiteDetail(List<NetworkRemoveSite> networkSite) {
		this.networkSite = networkSite;
	}

}