package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="AddressIdentifier")
public class AddressIdentifier extends SimpleIdentifier  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5861933928610669491L;

}
