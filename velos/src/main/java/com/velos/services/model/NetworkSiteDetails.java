package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="NetworkSiteDetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class NetworkSiteDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1789522870966714885L;
	
	private String Network_Name;
	private String Network_PK;
	private String Site_Name;
	private String  Site_PK;
	private String CTEP_ID;
	private String Relationship_PK; 
	private Code Relationship_Type;
	protected List<NVPair> moreSiteDetails;
	
	//private int pageNumber;
	//private int pageSize;
	
	public String getNetwork_Name() {
		if(Network_Name!=null  && !Network_Name.equals("") && Network_Name.contains("'")){
			try{
				char c =Network_Name.charAt(Network_Name.indexOf("'")+1);
				if(c!='\''){
					Network_Name=Network_Name.replaceAll("'", "''");
				}
				}catch(IndexOutOfBoundsException e){
					//do nothing
				}
		}
		return Network_Name;
	}
	public void setNetwork_Name(String network_Name) {
		Network_Name = network_Name;
	}
	public String getNetwork_PK() {
		return Network_PK;
	}
	public void setNetwork_PK(String network_PK) {
		Network_PK = network_PK;
	}
	public String getSite_Name() {
		
		if(Site_Name!=null && !Site_Name.equals("") && Site_Name.contains("'") ){
			try{
			char c =Site_Name.charAt(Site_Name.indexOf("'")+1);
			if(c!='\''){
				Site_Name=Site_Name.replaceAll("'", "''");
			}
			}catch(IndexOutOfBoundsException e){
				//do nothing
			}
			
		}
		return Site_Name;
	}
	public void setSite_Name(String site_Name) {
		Site_Name = site_Name;
	}
	public String getSite_PK() {
		return Site_PK;
	}
	public void setSite_PK(String site_PK) {
		Site_PK = site_PK;
	}
	public String getRelationship_PK() {
		return Relationship_PK;
	}
	public void setRelationship_PK(String relationship_PK) {
		Relationship_PK = relationship_PK;
	}
	public Code getRelationship_Type() {
		return Relationship_Type;
	}
	public void setRelationship_Type(Code relationship_Type) {
		Relationship_Type = relationship_Type;
	}
	
	/*public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}*/
	public List<NVPair> getMoreSiteDetails() {
		return moreSiteDetails;
	}
	public void setMoreSiteDetails(List<NVPair> moreSiteDetails) {
		this.moreSiteDetails = moreSiteDetails;
	}
	public String getCTEP_ID() {
		return CTEP_ID;
	}
	public void setCTEP_ID(String cTEP_ID) {
		CTEP_ID = cTEP_ID;
	}

}
