/**
 * Created On Nov 14, 2012
 */
package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author Kanwaldeep
 *
 */

@XmlRootElement(name="StudyPatientStatuses")
public class StudyPatientStatuses implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4573949502407701466L;
	
	private List<StudyPatientStatus> studyPatientStatus = new ArrayList<StudyPatientStatus>();

	/**
	 * @param studyPatientStatus the studyPatientStatus to set
	 */
	public void setStudyPatientStatus(List<StudyPatientStatus> studyPatientStatus) {
		this.studyPatientStatus = studyPatientStatus;
	}

	/**
	 * @return the studyPatientStatus
	 */
	public List<StudyPatientStatus> getStudyPatientStatus() {
		return studyPatientStatus;
	}
	
	public void addStudyPatientStatus(StudyPatientStatus studyPatientStatus)
	{
		this.studyPatientStatus.add(studyPatientStatus); 
	}

}
