package com.velos.services.model;

import java.io.Serializable;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Network")
public class UserNetworkSite implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8031445360320287120L;
	private NetworkIdentifier networkIdentifier;
	private String Name;
	private Code NetworkStatus;
	private NetworkUsersSite sites;

	public NetworkIdentifier getNetworkIdentifier() {
		return networkIdentifier;
	}
	public void setNetworkIdentifier(NetworkIdentifier networkIdentifier) {
		this.networkIdentifier = networkIdentifier;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public Code getNetworkStatus() {
		return NetworkStatus;
	}
	public void setNetworkStatus(Code networkStatus) {
		NetworkStatus = networkStatus;
	}
	public NetworkUsersSite getSites() {
		return sites;
	}
	public void setSites(NetworkUsersSite sites) {
		this.sites = sites;
	}

}
