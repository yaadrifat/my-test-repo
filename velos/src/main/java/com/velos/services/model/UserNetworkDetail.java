package com.velos.services.model;
import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.velos.services.model.Netwrk;
import com.velos.services.model.SiteLevelDetail;
@XmlRootElement(name="userIdentifier")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserNetworkDetail implements Serializable {
	/**
	 * 
	 */
	
	private String OID;
	private Integer PK;
	private String loginName;
	private String networkSiteUserPK;
	public String getOID() {
		return OID;
	}

	public void setOID(String OID) {
		this.OID = OID;
	}

	public Integer getPK() {
		return PK;
	}

	public void setPK(Integer PK) {
		this.PK = PK;
	}
	
	
	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getNetworkSiteUserPK() {
		return networkSiteUserPK;
	}

	public void setNetworkSiteUserPK(String networkSiteUserPK) {
		this.networkSiteUserPK = networkSiteUserPK;
	}
	
}
