package com.velos.services.model;

import java.io.Serializable;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="network")
public class UserNetwork implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8031445360320287120L;
	private NetworkIdentifier networkIdentifier;
	private String name;
	//private String NetworkStatus;
	private Code networkStatus;
	private String relationshipPK;
	private Code networkRelationshipType;
	private NetworkUserSite sites;
	

	public NetworkIdentifier getNetworkIdentifier() {
		return networkIdentifier;
	}
	public void setNetworkIdentifier(NetworkIdentifier networkIdentifier) {
		this.networkIdentifier = networkIdentifier;
	}
	public String getName() {
		return name;
	}
	public void setName(String Name) {
		name = Name;
	}
	public Code getNetworkStatus() {
		return networkStatus;
	}
	public void setNetworkStatus(Code network_Status) {
		networkStatus = network_Status;
	}
	/*public String getNetworkStatus() {
		return NetworkStatus;
	}
	public void setNetworkStatus(String networkStatus) {
		NetworkStatus = networkStatus;
	}*/
	public NetworkUserSite getSites() {
		return sites;
	}
	public void setSites(NetworkUserSite sites) {
		this.sites = sites;
	}
	public String getRelationShip_PK() {
		return relationshipPK;
	}
	public void setRelationShip_PK(String relationShip_PK) {
		relationshipPK = relationShip_PK;
	}
	public Code getNetworkRelationShipType() {
		return networkRelationshipType;
	}
	public void setNetworkRelationShipType(Code networkRelationShipType) {
		networkRelationshipType = networkRelationShipType;
	}

}
