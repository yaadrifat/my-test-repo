package com.velos.services.model;
/**
 * 
 * @author Tarandeep Singh Bali
 *
 */

public class MultipleEventIdentifier extends SimpleIdentifier {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4557429064430321911L;
	
	private EventAttributes eventAttributes;

	public EventAttributes getEventAttributes() {
		return eventAttributes;
	}

	public void setEventAttributes(EventAttributes eventAttributes) {
		this.eventAttributes = eventAttributes;
	}
	
	
	

}
