package com.velos.services.model;
import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import com.velos.services.model.Netwrk;
import com.velos.services.model.SiteLevelDetail;

@XmlRootElement(name="networks")
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(propOrder={"network", "networkName","siteLevelDetails","relationshipPK"})
public class NetworkSiteDetail implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1365381990599553970L;
	private Netwrk network;
	private String networkName;
	private SiteLevelDetail siteLevelDetails;
	private int relationshipPK;
	
	public Netwrk getNetwork() {
		return network;
	}
	public void setNetwork(Netwrk network) {
		this.network = network;
	}
	public String getNetworkName() {
		if(networkName!=null  && !networkName.equals("") && networkName.contains("'")){
			try{
				char c =networkName.charAt(networkName.indexOf("'")+1);
				if(c!='\''){
					networkName=networkName.replaceAll("'", "''");
				}
				}catch(IndexOutOfBoundsException e){
					//do nothing
				}
		}
		return networkName;
	}
	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}
	public SiteLevelDetail getSiteLevelDetails() {
		return siteLevelDetails;
	}
	public void setSiteLevelDetails(SiteLevelDetail siteLevelDetails) {
		this.siteLevelDetails = siteLevelDetails;
	}
	public int getRelationshipPK() {
		return relationshipPK;
	}
	public void setRelationshipPK(int relationshipPK) {
		this.relationshipPK = relationshipPK;
	}
	
	
}
