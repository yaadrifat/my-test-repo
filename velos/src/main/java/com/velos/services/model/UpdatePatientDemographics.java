/**
 * Created On Aug 27, 2012
 */
package com.velos.services.model;

import javax.validation.constraints.NotNull;

/**
 * @author Kanwaldeep
 *
 */
public class UpdatePatientDemographics extends PatientDemographics {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5623552583489260278L;

	protected String reasonForChange; 
	@NotNull
	public String getReasonForChange() {
		return reasonForChange;
	}
	public void setReasonForChange(String reasonForChange) {
		this.reasonForChange = reasonForChange;
	}	

}
