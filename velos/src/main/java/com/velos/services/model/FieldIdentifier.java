/**
 * Created On Apr 18, 2012
 */
package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="FieldIdentifier")
public class FieldIdentifier extends SimpleIdentifier {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4140961354706133977L;

}
