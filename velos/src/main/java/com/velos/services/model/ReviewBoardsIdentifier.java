package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="ReviewBoardsIdentifier")
public class ReviewBoardsIdentifier implements Serializable {
	

	private static final long serialVersionUID = 6761450731373866512L;
	protected List<ReviewBoardId> reviewBoardId;
	protected StudyIdentifier studyIdentifier;
	
	public StudyIdentifier getStudyIdentifier() {
		return studyIdentifier;
	}

	public void setStudyIdentifier(StudyIdentifier studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	}

	public List<ReviewBoardId> getReviewBoardId() {
		return reviewBoardId;
	}

	public void setReviewBoard(List<ReviewBoardId> reviewBoardId) {
		this.reviewBoardId = reviewBoardId;
	}
	
}