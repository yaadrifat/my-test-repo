package com.velos.services.model;

public class StudyServiceObject extends SimpleIdentifier{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5657400151183630346L;
	protected String studyNumber;
	protected String version;
	
	public StudyServiceObject(){
		super();
	}

	public StudyServiceObject(String studyNumber) {
		this.studyNumber = studyNumber;
	}

	/**
	 * Gets the study number, which uniquely identifies a study for a particular account.
	 * @return
	 */
	public String getStudyNumber() {
		return studyNumber;
	}

	/**
	 * Sets the study number, which uniquely identifies a study for a particular account.
	 * @return
	 */
	public void setStudyNumber(String studyNumber) {
		this.studyNumber = studyNumber;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString(){
		return studyNumber;
	}

}
