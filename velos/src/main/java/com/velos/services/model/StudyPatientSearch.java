package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="StudyPatientSearch")
@XmlAccessorType(XmlAccessType.FIELD)
public class StudyPatientSearch implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6688024820589394734L;
	private StudyIdentifier studyIdentifier;
	private int pageNumber;
	private int pageSize;
	protected String sortBy;
    
    protected String sortOrder;
    
    protected SearchBy searchBy;
	/*private StudyPatSearchOrderBy sortBy;
	private SortOrder sortOrder;
	
	public enum StudyPatSearchOrderBy {
		lowerpatstdid,
		mask_person_fname,
		mask_person_lname,
		enrollingorg_name,
		patprot_enroldt_datesort,
		PATSTUDYSTAT_DESC,
		assignedto_name,
		last_visit_name,
		next_visit_datesort
	}
	
	public enum SortOrder {
		DESCENDING("DESC"),
		ASCENDING("ASC"); 
		
		private String value;
		private SortOrder(String value)
		{
			this.value = value; 
		}
		public String toString()
		{
			return value; 
		}
	}*/

	public StudyIdentifier getStudyIdentifier() {
		return studyIdentifier;
	}

	public void setStudyIdentifier(StudyIdentifier studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public SearchBy getSearchBy() {
		return searchBy;
	}

	public void setSearchBy(SearchBy searchBy) {
		this.searchBy = searchBy;
	}

	/*public StudyPatSearchOrderBy getSortBy() {
		return sortBy;
	}

	public void setSortBy(StudyPatSearchOrderBy sortBy) {
		this.sortBy = sortBy;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	public void setSortOrder(String sortOrder) {
		this.sortOrder = EnumUtil.getEnumType(SortOrder.class, sortOrder);
	}*/

}
