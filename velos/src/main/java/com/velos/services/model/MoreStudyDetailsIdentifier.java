/**
 * Created On Sep 27, 2011
 */
package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="MoreStudyDetailsIdentifier")
public class MoreStudyDetailsIdentifier extends SimpleIdentifier {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4820534406304444772L;

}
