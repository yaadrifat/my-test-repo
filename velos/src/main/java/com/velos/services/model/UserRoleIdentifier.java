package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author dylan
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="UserRoleIdentifier")
public class UserRoleIdentifier 
	implements Serializable{
	
	private static final long serialVersionUID = 4732997687729050740L;
	
	private String loginName = null;
	private String firstName = null;
	private String lastName = null;
	private String PK = null;
	private Roles roles;
	private UserIdentifier userIdentifier;
	private List<NetworkRole> role=new ArrayList<NetworkRole>();
	
	
	private List<UserRoleIdentifier> user=new ArrayList<UserRoleIdentifier>();
	
	public UserRoleIdentifier(){
		
	}
	
	public List<UserRoleIdentifier> getUser() {
		return user;
	}
	public void setUser(List<UserRoleIdentifier> user) {
		this.user = user;
	}
	
	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String userId) {
		this.loginName = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public UserIdentifier getUid() {
		return userIdentifier;
	}

	public void setUid(UserIdentifier userIdentifier) {
		this.userIdentifier = userIdentifier;
	}

	public void setRoles(Roles roles){
		this.roles=roles;
	}

    public Roles getRoles(){
	  return roles;
    }

    public List<NetworkRole> getRole() {
		return role;
	}
	public void setRole(List<NetworkRole> role) {
		this.role = role;
	}

	public String getPK() {
		return PK;
	}

	public void setPK(String pK) {
		PK = pK;
	}

}
