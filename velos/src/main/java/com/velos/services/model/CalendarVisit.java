/**
 * Created On May 6, 2011
 */
package com.velos.services.model;

import java.io.Serializable;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlRootElement(name="Visit")
@XmlAccessorType(XmlAccessType.FIELD)
public class CalendarVisit implements Serializable{
	
	private static final long serialVersionUID = -2136767714648967858L;
	
	protected CalendarVisitSummary calendarVisitSummary; 
	
	protected CalendarEvents events;

	

	public CalendarVisit(){
		
	}
	

	@Valid
	public CalendarEvents getEvents() {
		return events;
	}

	public void setEvents(CalendarEvents events) {
		this.events = events;
	}

	@Valid
	public CalendarVisitSummary getCalendarVisitSummary() {
		return calendarVisitSummary;
	}


	public void setCalendarVisitSummary(CalendarVisitSummary calendarVisitSummary) {
		this.calendarVisitSummary = calendarVisitSummary;
	}	

}
