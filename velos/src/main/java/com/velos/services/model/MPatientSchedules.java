package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="MPatientSchedules")
public class MPatientSchedules implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6571829101880378959L;
	
	private List<MPatientSchedule> mPatientSchedule;

	public List<MPatientSchedule> getmPatientSchedule() {
		return mPatientSchedule;
	}

	public void setmPatientSchedule(List<MPatientSchedule> mPatientSchedule) {
		this.mPatientSchedule = mPatientSchedule;
	}
	
	public void addMPatientSchedule(MPatientSchedule mPatientSchedule){

		this.mPatientSchedule.add(mPatientSchedule);

	}

}
