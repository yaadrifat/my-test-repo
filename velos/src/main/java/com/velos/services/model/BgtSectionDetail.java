package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name="BudgetSections")
@XmlAccessorType (XmlAccessType.FIELD)
public class BgtSectionDetail implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<BudgetSectionPojo> budgetSectionInfo;
	
	public List<BudgetSectionPojo> getBudgetSectionInfo() {
		return budgetSectionInfo;
	}
	public void setBudgetSectionInfo(List<BudgetSectionPojo> budgetSectionInfo) {
		this.budgetSectionInfo = budgetSectionInfo;
	}
	
}
