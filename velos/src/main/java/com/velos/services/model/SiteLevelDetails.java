package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="SiteLevelDetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class SiteLevelDetails implements Serializable{
	private static final long serialVersionUID = 1789522870966717656L;
	private String networkName;
	private String networkPK;
	private String siteName;
	private String  sitePK;
	private String relationshipPK;
	private String level;
	
	public String getNetworkName() {
		if(networkName!=null  && !networkName.equals("") && networkName.contains("'")){
			try{
				char c =networkName.charAt(networkName.indexOf("'")+1);
				if(c!='\''){
					networkName=networkName.replaceAll("'", "''");
				}
				}catch(IndexOutOfBoundsException e){
					//do nothing
				}
		}
		return networkName;
	}
	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}
	public String getNetworkPK() {
		return networkPK;
	}
	public void setNetworkPK(String networkPK) {
		this.networkPK = networkPK;
	}
	public String getSiteName() {
		if(siteName!=null && !siteName.equals("") && siteName.contains("'") ){
			try{
			char c =siteName.charAt(siteName.indexOf("'")+1);
			if(c!='\''){
				siteName=siteName.replaceAll("'", "''");
			}
			}catch(IndexOutOfBoundsException e){
				//do nothing
			}
			
		}
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getSitePK() {
		return sitePK;
	}
	public void setSitePK(String sitePK) {
		this.sitePK = sitePK;
	}
	public String getRelationshipPK() {
		return relationshipPK;
	}
	public void setRelationshipPK(String relationshipPK) {
		this.relationshipPK = relationshipPK;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
}
