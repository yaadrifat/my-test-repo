package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="networkSiteData")
public class NetworkSiteData extends Site implements Serializable  {
	private static final long serialVersionUID = 3795665456995244551L;
	/*private String siteName;
	private Code siteStatus;
	private String siteStatusDate;
	private String siteStatusNotes;
	private Code siteRelationshipType;
	protected List<NVPair> moreSiteDetailsFields;
	private SiteIdentifier siteIdentifier;
	
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public Code getSiteStatus() {
		return siteStatus;
	}
	public void setSiteStatus(Code siteStatus) {
		this.siteStatus = siteStatus;
	}
	public String getSiteStatusDate() {
		return siteStatusDate;
	}
	public void setSiteStatusDate(String siteStatusDate) {
		this.siteStatusDate = siteStatusDate;
	}
	public String getSiteStatusNotes() {
		return siteStatusNotes;
	}
	public void setSiteStatusNotes(String siteStatusNotes) {
		this.siteStatusNotes = siteStatusNotes;
	}
	public Code getSiteRelationshipType() {
		return siteRelationshipType;
	}
	public void setSiteRelationshipType(Code siteRelationshipType) {
		this.siteRelationshipType = siteRelationshipType;
	}
	public List<NVPair> getMoreSiteDetailsFields() {
		return moreSiteDetailsFields;
	}
	public void setMoreSiteDetailsFields(List<NVPair> moreSiteDetailsFields) {
		this.moreSiteDetailsFields = moreSiteDetailsFields;
	}
	public SiteIdentifier getSiteIdentifier() {
		return siteIdentifier;
	}
	public void setSiteIdentifier(SiteIdentifier siteIdentifier) {
		this.siteIdentifier = siteIdentifier;
	}*/
	
}
	