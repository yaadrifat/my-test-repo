package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="searchBy")
public class SearchBy implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -102128466109637139L;
	protected String searchByColumn;
	protected String value;
	public String getSearchByColumn() {
		return searchByColumn;
	}
	public void setSearchByColumn(String searchByColumn) {
		this.searchByColumn = searchByColumn;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

}
