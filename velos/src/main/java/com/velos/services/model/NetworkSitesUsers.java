package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="NetworkSitesUsers")
public class NetworkSitesUsers implements Serializable{

	
	private static final long serialVersionUID = 4732997687729050740L;
	private SiteIdentifier siteIdentifier;
	private String name;
	private Code sitestatus;
	private String CTEP_ID;
	private String relationship_PK;
	private Code siteRelationShipType;
	private String level;
	private UserRoleIdentifier users;
	private List<NetworkSitesUsers> site;
	

	
	
	public Code getSiteStatus() {
		return sitestatus;
	}
	public void setSiteStatus(Code sitestatus) {
		this.sitestatus = sitestatus;
	}
	
	public SiteIdentifier getSiteIdentifier() {
		return siteIdentifier;
	}
	public void setSiteIdentifier(SiteIdentifier siteIdentifier) {
		this.siteIdentifier = siteIdentifier;
	}
	
	public void setName(String name){
		this.name=name;
	}
	
	public String getName(){
		return name;
	}
	
	public void setLevel(String level){
		this.level=level;
		}

    public String getLevel(){
	  return level;
     }
 
    public void setUserRoleIdentifier(UserRoleIdentifier users){
		this.users=users;
		}

    public UserRoleIdentifier getUserRoleIdentifier(){
	  return users;
    }
	public List<NetworkSitesUsers> getSites() {
		return site;
	}
	public void setSites(List<NetworkSitesUsers> sites) {
		this.site = sites;
	}
	public String getCTEP_ID() {
		return CTEP_ID;
	}
	public void setCTEP_ID(String cTEP_ID) {
		CTEP_ID = cTEP_ID;
	}
	public String getRelationship_PK() {
		return relationship_PK;
	}
	public void setRelationship_PK(String relationship_PK) {
		this.relationship_PK = relationship_PK;
	}
	public Code getSiteRelationShipType() {
		return siteRelationShipType;
	}
	public void setSiteRelationShipType(Code siteRelationShipType) {
		this.siteRelationShipType = siteRelationShipType;
	}
  
}