package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyNIHGrants")
public class StudyNIHGrants  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<StudyNIHGrantInfo> studyNIHGrantInfo;

	public List<StudyNIHGrantInfo> getStudyNIHGrantInfo() {
		return studyNIHGrantInfo;
	}

	public void setStudyNIHGrantInfo(List<StudyNIHGrantInfo> studyNIHGrantInfo) {
		this.studyNIHGrantInfo = studyNIHGrantInfo;
	}

}
