/**
 * 
 */
package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**model class for Study a patient belongs to.
 * @author virendra
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientStudy")
public class PatientStudy
	implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected StudyIdentifier studyIdentifier;
	
	protected PatientStudyId patientStudyId;
	
    protected Boolean isFdaRegulated;

	
	public PatientStudy() {
		super();
	}
	
	public StudyIdentifier getStudyIdentifier() {
		return studyIdentifier;
	}

	public void setStudyIdentifier(StudyIdentifier studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	}
	
	public String getPatientStudyId() {
		if (this.patientStudyId == null) { return ""; }
		return this.patientStudyId.patientStudyId;
	}
	
	public void setPatientStudyId(String patientStudyId) {
		if (this.patientStudyId == null) {
			this.patientStudyId = new PatientStudyId();
		}
		this.patientStudyId.setPatientStudyId(patientStudyId);
	}
	
	public Boolean getIsFdaRegulated() {
		return isFdaRegulated;
	}
	
	public void setIsFdaRegulated(Boolean isFdaRegulated) {
		this.isFdaRegulated = isFdaRegulated;
	}

}