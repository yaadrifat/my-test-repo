package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="BgtSectionName")
public class BgtSectionNameIdentifier extends SimpleIdentifier{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String bgtSectionName;

	public String getBgtSectionName() {
		return bgtSectionName;
	}

	public void setBgtSectionName(String bgtSectionName) {
		this.bgtSectionName = bgtSectionName;
	}

}
