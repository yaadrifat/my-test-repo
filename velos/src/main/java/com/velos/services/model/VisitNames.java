/**
 * Created On Jul 5, 2011
 */
package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author Kanwaldeep
 *
 */
public class VisitNames implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8828866364960059725L;
	private List<String> visitName;

	public List<String> getVisitName() {
		return visitName;
	}

	public void setVisitName(List<String> visitName) {
		this.visitName = visitName;
	} 
	
	public void addVisitName(String visitName)
	{
		this.visitName.add(visitName); 
	}

}
