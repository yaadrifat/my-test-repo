package com.velos.services.model;

import java.util.Date;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="Milestone")
@XmlAccessorType(XmlAccessType.FIELD)
public class MilestoneVDAPojo extends SimpleIdentifier{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String STUDY_NUMBER;
	
	private String STUDY_TITLE;
	
	private String MSRUL_CATEGORY;
	
	private String MILESTONE_AMOUNT;
	
	private String MSRUL_PT_COUNT;
	
	private String MILESTONE_VISIT;//
	//
	//private String MILESTONE_DELFLAG;//
	
	private String MILESTONE_USERSTO;//
	
	private String RID;//
	
	private String IP_ADD;//
	
	private String MSRUL_PT_STATUS;
	
	private String MSRUL_LIMIT;
	
	private String MILESTONE_PAYDUEBY;//
	
	private String MILESTONE_PAYBYUNIT;//
	
	private String MSRUL_PAY_TYPE;
	
	private String MSRUL_PAY_FOR;
	
	private String MILESTONE_EVENTSTATUS;//
	
	private String MSRUL_STATUS;
	
	private String MSRUL_PROT_CAL;
	
	private String MS_PROT_CALASSOCTO;
	
	private String MILESTONE_ISACTIVE;//
	
	private String MSRUL_VISIT;
	
	private String MSRUL_MS_RULE;
	
	private String MSRUL_EVENT_STAT;
	
	private String MSRUL_STUDY_STATUS;
	
	private String FK_ACCOUNT;
	
	private String PK_MILESTONE;
	
	private String CREATOR;
	
	private String LAST_MODIFIED_BY;
	
	private String LAST_MODIFIED_DATE;
	
	private String CREATED_ON;
	
	private String FK_STUDY;
	
	private String EVENT_NAME;
	
	private String MILESTONE_ACHIEVEDCOUNT;
	//private String MILESTONE_ACHIEVEDAMOUNT;////
	
	private String MILESTONE_DESCRIPTION;
	
	private String FK_BUDGET;
	
	private String FK_BGTCAL;
	
	private String FK_BGTSECTION;
	
	private String FK_LINEITEM;
	
	private Date MILESTONE_DATE_FROM;
	
	private Date MILESTONE_DATE_TO;
	
	private String MILESTONE_HOLDBACK;
	
	private String MILESTONE_DESC_CALCULATED;
	
	private String FK_VISIT;
	
	private String FK_EVENTASSOC;
	
	private String LAST_MODIFIED_BY_FK;
	
	private String CREATOR_FK;
	
	private String MILERULE_SUBTYP;
	
	private String MILEPS_SUBTYP;
	
	private String MILEPAY_SUBTYP;
	
	private String MILEPAYFR_SUBTYPE;
	
	private String MILESTAT_SUBTYP;
	
	private String MILEVISIT_SUBTYP;
	
	private String BUDGET_NAME;
	
	private String CALENDAR_NAME;
	
	private String BUDGET_SECTION_NAME;
	
	private String LINEITEM_NAME;
	
	

	public String getMILESTONE_DESC_CALCULATED() {
		return MILESTONE_DESC_CALCULATED;
	}
	
	public void setMILESTONE_DESC_CALCULATED(String mILESTONE_DESC_CALCULATED) {
		MILESTONE_DESC_CALCULATED = mILESTONE_DESC_CALCULATED;
	}

	public String getFK_VISIT() {
		return FK_VISIT;
	}
	
	public void setFK_VISIT(String fK_VISIT) {
		FK_VISIT = fK_VISIT;
	}

	public String getFK_EVENTASSOC() {
		return FK_EVENTASSOC;
	}
	
	public void setFK_EVENTASSOC(String fK_EVENTASSOC) {
		FK_EVENTASSOC = fK_EVENTASSOC;
	}

	public String getLAST_MODIFIED_BY_FK() {
		return LAST_MODIFIED_BY_FK;
	}
	
	public void setLAST_MODIFIED_BY_FK(String lAST_MODIFIED_BY_FK) {
		LAST_MODIFIED_BY_FK = lAST_MODIFIED_BY_FK;
	}

	public String getCREATOR_FK() {
		return CREATOR_FK;
	}
	
	public void setCREATOR_FK(String cREATOR_FK) {
		CREATOR_FK = cREATOR_FK;
	}

	public String getMILERULE_SUBTYP() {
		return MILERULE_SUBTYP;
	}
	
	public void setMILERULE_SUBTYP(String mILERULE_SUBTYP) {
		MILERULE_SUBTYP = mILERULE_SUBTYP;
	}

	public String getMILEPS_SUBTYP() {
		return MILEPS_SUBTYP;
	}
	
	public void setMILEPS_SUBTYP(String mILEPS_SUBTYP) {
		MILEPS_SUBTYP = mILEPS_SUBTYP;
	}

	public String getMILEPAY_SUBTYP() {
		return MILEPAY_SUBTYP;
	}
	
	public void setMILEPAY_SUBTYP(String mILEPAY_SUBTYP) {
		MILEPAY_SUBTYP = mILEPAY_SUBTYP;
	}

	public String getMILEPAYFR_SUBTYPE() {
		return MILEPAYFR_SUBTYPE;
	}
	
	public void setMILEPAYFR_SUBTYPE(String mILEPAYFR_SUBTYPE) {
		MILEPAYFR_SUBTYPE = mILEPAYFR_SUBTYPE;
	}

	public String getMILESTAT_SUBTYP() {
		return MILESTAT_SUBTYP;
	}
	
	public void setMILESTAT_SUBTYP(String mILESTAT_SUBTYP) {
		MILESTAT_SUBTYP = mILESTAT_SUBTYP;
	}

	public String getMILEVISIT_SUBTYP() {
		return MILEVISIT_SUBTYP;
	}
	
	public void setMILEVISIT_SUBTYP(String mILEVISIT_SUBTYP) {
		MILEVISIT_SUBTYP = mILEVISIT_SUBTYP;
	}

	public String getBUDGET_NAME() {
		return BUDGET_NAME;
	}

	public void setBUDGET_NAME(String bUDGET_NAME) {
		BUDGET_NAME = bUDGET_NAME;
	}

	public String getCALENDAR_NAME() {
		return CALENDAR_NAME;
	}

	public void setCALENDAR_NAME(String cALENDAR_NAME) {
		CALENDAR_NAME = cALENDAR_NAME;
	}

	public String getBUDGET_SECTION_NAME() {
		return BUDGET_SECTION_NAME;
	}

	public void setBUDGET_SECTION_NAME(String bUDGET_SECTION_NAME) {
		BUDGET_SECTION_NAME = bUDGET_SECTION_NAME;
	}

	public String getLINEITEM_NAME() {
		return LINEITEM_NAME;
	}

	public void setLINEITEM_NAME(String lINEITEM_NAME) {
		LINEITEM_NAME = lINEITEM_NAME;
	}


	public String getSTUDY_NUMBER() {
		return STUDY_NUMBER;
	}
	
	public void setSTUDY_NUMBER(String sTUDY_NUMBER) {
		STUDY_NUMBER = sTUDY_NUMBER;
	}

	public String getSTUDY_TITLE() {
		return STUDY_TITLE;
	}
	
	public void setSTUDY_TITLE(String sTUDY_TITLE) {
		STUDY_TITLE = sTUDY_TITLE;
	}

	public String getMSRUL_CATEGORY() {
		return MSRUL_CATEGORY;
	}
	
	public void setMSRUL_CATEGORY(String mSRUL_CATEGORY) {
		MSRUL_CATEGORY = mSRUL_CATEGORY;
	}

	public String getMSRUL_PT_COUNT() {
		return MSRUL_PT_COUNT;
	}
	
	public void setMSRUL_PT_COUNT(String mSRUL_PT_COUNT) {
		MSRUL_PT_COUNT = mSRUL_PT_COUNT;
	}

	public String getMSRUL_PT_STATUS() {
		return MSRUL_PT_STATUS;
	}
	
	public void setMSRUL_PT_STATUS(String mSRUL_PT_STATUS) {
		MSRUL_PT_STATUS = mSRUL_PT_STATUS;
	}

	public String getMSRUL_LIMIT() {
		return MSRUL_LIMIT;
	}
	
	public void setMSRUL_LIMIT(String mSRUL_LIMIT) {
		MSRUL_LIMIT = mSRUL_LIMIT;
	}

	public String getMSRUL_PAY_TYPE() {
		return MSRUL_PAY_TYPE;
	}
	
	public void setMSRUL_PAY_TYPE(String mSRUL_PAY_TYPE) {
		MSRUL_PAY_TYPE = mSRUL_PAY_TYPE;
	}

	public String getMSRUL_PAY_FOR() {
		return MSRUL_PAY_FOR;
	}
	
	public void setMSRUL_PAY_FOR(String mSRUL_PAY_FOR) {
		MSRUL_PAY_FOR = mSRUL_PAY_FOR;
	}

	public String getMSRUL_STATUS() {
		return MSRUL_STATUS;
	}
	
	public void setMSRUL_STATUS(String mSRUL_STATUS) {
		MSRUL_STATUS = mSRUL_STATUS;
	}

	public String getMSRUL_PROT_CAL() {
		return MSRUL_PROT_CAL;
	}
	
	public void setMSRUL_PROT_CAL(String mSRUL_PROT_CAL) {
		MSRUL_PROT_CAL = mSRUL_PROT_CAL;
	}

	public String getMS_PROT_CALASSOCTO() {
		return MS_PROT_CALASSOCTO;
	}

	public void setMS_PROT_CALASSOCTO(String mS_PROT_CALASSOCTO) {
		MS_PROT_CALASSOCTO = mS_PROT_CALASSOCTO;
	}

	public String getMSRUL_VISIT() {
		return MSRUL_VISIT;
	}
	
	public void setMSRUL_VISIT(String mSRUL_VISIT) {
		MSRUL_VISIT = mSRUL_VISIT;
	}

	public String getMSRUL_MS_RULE() {
		return MSRUL_MS_RULE;
	}
	
	public void setMSRUL_MS_RULE(String mSRUL_MS_RULE) {
		MSRUL_MS_RULE = mSRUL_MS_RULE;
	}

	public String getMSRUL_EVENT_STAT() {
		return MSRUL_EVENT_STAT;
	}
	
	public void setMSRUL_EVENT_STAT(String mSRUL_EVENT_STAT) {
		MSRUL_EVENT_STAT = mSRUL_EVENT_STAT;
	}

	public String getMSRUL_STUDY_STATUS() {
		return MSRUL_STUDY_STATUS;
	}
	
	public void setMSRUL_STUDY_STATUS(String mSRUL_STUDY_STATUS) {
		MSRUL_STUDY_STATUS = mSRUL_STUDY_STATUS;
	}

	public String getFK_ACCOUNT() {
		return FK_ACCOUNT;
	}
	
	public void setFK_ACCOUNT(String fK_ACCOUNT) {
		FK_ACCOUNT = fK_ACCOUNT;
	}

	public String getPK_MILESTONE() {
		return PK_MILESTONE;
	}
	
	public void setPK_MILESTONE(String pK_MILESTONE) {
		PK_MILESTONE = pK_MILESTONE;
	}

	public String getCREATOR() {
		return CREATOR;
	}
	
	public void setCREATOR(String cREATOR) {
		CREATOR = cREATOR;
	}

	public String getLAST_MODIFIED_BY() {
		return LAST_MODIFIED_BY;
	}
	
	public void setLAST_MODIFIED_BY(String lAST_MODIFIED_BY) {
		LAST_MODIFIED_BY = lAST_MODIFIED_BY;
	}

	public String getLAST_MODIFIED_DATE() {
		return LAST_MODIFIED_DATE;
	}
	
	public void setLAST_MODIFIED_DATE(String lAST_MODIFIED_DATE) {
		LAST_MODIFIED_DATE = lAST_MODIFIED_DATE;
	}

	public String getCREATED_ON() {
		return CREATED_ON;
	}
	
	public void setCREATED_ON(String cREATED_ON) {
		CREATED_ON = cREATED_ON;
	}

	public String getFK_STUDY() {
		return FK_STUDY;
	}
	
	public void setFK_STUDY(String fK_STUDY) {
		FK_STUDY = fK_STUDY;
	}

	public String getEVENT_NAME() {
		return EVENT_NAME;
	}
	
	public void setEVENT_NAME(String eVENT_NAME) {
		EVENT_NAME = eVENT_NAME;
	}

	public String getMILESTONE_ACHIEVEDCOUNT() {
		return MILESTONE_ACHIEVEDCOUNT;
	}
	
	public void setMILESTONE_ACHIEVEDCOUNT(String mILESTONE_ACHIEVEDCOUNT) {
		MILESTONE_ACHIEVEDCOUNT = mILESTONE_ACHIEVEDCOUNT;
	}
	/*@Column(name="MILESTONE_ACHIEVEDAMOUNT")
	public String getMILESTONE_ACHIEVEDAMOUNT() {
		return MILESTONE_ACHIEVEDAMOUNT;
	}
	
	public void setMILESTONE_ACHIEVEDAMOUNT(String mILESTONE_ACHIEVEDAMOUNT) {
		MILESTONE_ACHIEVEDAMOUNT = mILESTONE_ACHIEVEDAMOUNT;
	}*/

	public String getMILESTONE_DESCRIPTION() {
		return MILESTONE_DESCRIPTION;
	}
	
	public void setMILESTONE_DESCRIPTION(String mILESTONE_DESCRIPTION) {
		MILESTONE_DESCRIPTION = mILESTONE_DESCRIPTION;
	}

	public String getFK_BUDGET() {
		return FK_BUDGET;
	}
	
	public void setFK_BUDGET(String fK_BUDGET) {
		FK_BUDGET = fK_BUDGET;
	}

	public String getFK_BGTCAL() {
		return FK_BGTCAL;
	}
	
	public void setFK_BGTCAL(String fK_BGTCAL) {
		FK_BGTCAL = fK_BGTCAL;
	}

	public String getFK_BGTSECTION() {
		return FK_BGTSECTION;
	}
	
	public void setFK_BGTSECTION(String fK_BGTSECTION) {
		FK_BGTSECTION = fK_BGTSECTION;
	}

	public String getFK_LINEITEM() {
		return FK_LINEITEM;
	}
	
	public void setFK_LINEITEM(String fK_LINEITEM) {
		FK_LINEITEM = fK_LINEITEM;
	}

	public Date getMILESTONE_DATE_FROM() {
		return MILESTONE_DATE_FROM;
	}
	
	public void setMILESTONE_DATE_FROM(Date mILESTONE_DATE_FROM) {
		MILESTONE_DATE_FROM = mILESTONE_DATE_FROM;
	}

	public Date getMILESTONE_DATE_TO() {
		return MILESTONE_DATE_TO;
	}
	
	public void setMILESTONE_DATE_TO(Date mILESTONE_DATE_TO) {
		MILESTONE_DATE_TO = mILESTONE_DATE_TO;
	}

	public String getMILESTONE_HOLDBACK() {
		return MILESTONE_HOLDBACK;
	}
	
	public void setMILESTONE_HOLDBACK(String mILESTONE_HOLDBACK) {
		MILESTONE_HOLDBACK = mILESTONE_HOLDBACK;
	}

	public String getMILESTONE_VISIT() {
		return MILESTONE_VISIT;
	}
	
	public void setMILESTONE_VISIT(String mILESTONE_VISIT) {
		MILESTONE_VISIT = mILESTONE_VISIT;
	}
	/*@Column(name="MILESTONE_DELFLAG")
	public String getMILESTONE_DELFLAG() {
		return MILESTONE_DELFLAG;
	}
	
	public void setMILESTONE_DELFLAG(String mILESTONE_DELFLAG) {
		MILESTONE_DELFLAG = mILESTONE_DELFLAG;
	}*/

	public String getMILESTONE_USERSTO() {
		return MILESTONE_USERSTO;
	}
	
	public void setMILESTONE_USERSTO(String mILESTONE_USERSTO) {
		MILESTONE_USERSTO = mILESTONE_USERSTO;
	}

	public String getRID() {
		return RID;
	}
	
	public void setRID(String rID) {
		RID = rID;
	}

	public String getIP_ADD() {
		return IP_ADD;
	}
	
	public void setIP_ADD(String iP_ADD) {
		IP_ADD = iP_ADD;
	}

	public String getMILESTONE_PAYDUEBY() {
		return MILESTONE_PAYDUEBY;
	}
	
	public void setMILESTONE_PAYDUEBY(String mILESTONE_PAYDUEBY) {
		MILESTONE_PAYDUEBY = mILESTONE_PAYDUEBY;
	}

	public String getMILESTONE_PAYBYUNIT() {
		return MILESTONE_PAYBYUNIT;
	}
	
	public void setMILESTONE_PAYBYUNIT(String mILESTONE_PAYBYUNIT) {
		MILESTONE_PAYBYUNIT = mILESTONE_PAYBYUNIT;
	}

	public String getMILESTONE_EVENTSTATUS() {
		return MILESTONE_EVENTSTATUS;
	}
	
	public void setMILESTONE_EVENTSTATUS(String mILESTONE_EVENTSTATUS) {
		MILESTONE_EVENTSTATUS = mILESTONE_EVENTSTATUS;
	}

	public String getMILESTONE_ISACTIVE() {
		return MILESTONE_ISACTIVE;
	}
	
	public void setMILESTONE_ISACTIVE(String mILESTONE_ISACTIVE) {
		MILESTONE_ISACTIVE = mILESTONE_ISACTIVE;
	}

	public String getMILESTONE_AMOUNT() {
		return MILESTONE_AMOUNT;
	}
	
	public void setMILESTONE_AMOUNT(String mILESTONE_AMOUNT) {
		MILESTONE_AMOUNT = mILESTONE_AMOUNT;
	}
	
	public MilestoneVDAPojo() {
		super();
	}
	public MilestoneVDAPojo(String sTUDY_NUMBER, String sTUDY_TITLE,
			String mSRUL_CATEGORY, String mILESTONE_AMOUNT,
			String mSRUL_PT_COUNT, String mILESTONE_VISIT,
			//String mILESTONE_DELFLAG,
			String mILESTONE_USERSTO, String rID,
			String iP_ADD, String mSRUL_PT_STATUS, String mSRUL_LIMIT,
			String mILESTONE_PAYDUEBY, String mILESTONE_PAYBYUNIT,
			String mSRUL_PAY_TYPE, String mSRUL_PAY_FOR,
			String mILESTONE_EVENTSTATUS, String mSRUL_STATUS,
			String mSRUL_PROT_CAL, String mS_PROT_CALASSOCTO, String mILESTONE_ISACTIVE,
			String mSRUL_VISIT, String mSRUL_MS_RULE, String mSRUL_EVENT_STAT,
			String mSRUL_STUDY_STATUS, String fK_ACCOUNT, String pK_MILESTONE,
			String cREATOR, String lAST_MODIFIED_BY, String lAST_MODIFIED_DATE,
			String cREATED_ON, String fK_STUDY, String eVENT_NAME,
			String mILESTONE_ACHIEVEDCOUNT, String mILESTONE_DESCRIPTION,
			String fK_BUDGET, String fK_BGTCAL, String fK_BGTSECTION,
			String fK_LINEITEM, Date mILESTONE_DATE_FROM,
			Date mILESTONE_DATE_TO, String mILESTONE_HOLDBACK,
			String mILESTONE_DESC_CALCULATED, String fK_VISIT,
			String fK_EVENTASSOC, String lAST_MODIFIED_BY_FK,
			String cREATOR_FK, String mILERULE_SUBTYP, String mILEPS_SUBTYP,
			String mILEPAY_SUBTYP, String mILEPAYFR_SUBTYPE,
			String mILESTAT_SUBTYP, String mILEVISIT_SUBTYP) {
		super();
		STUDY_NUMBER = sTUDY_NUMBER;
		STUDY_TITLE = sTUDY_TITLE;
		MSRUL_CATEGORY = mSRUL_CATEGORY;
		MILESTONE_AMOUNT = mILESTONE_AMOUNT;
		MSRUL_PT_COUNT = mSRUL_PT_COUNT;
		MILESTONE_VISIT = mILESTONE_VISIT;
		//MILESTONE_DELFLAG = mILESTONE_DELFLAG;
		MILESTONE_USERSTO = mILESTONE_USERSTO;
		RID = rID;
		IP_ADD = iP_ADD;
		MSRUL_PT_STATUS = mSRUL_PT_STATUS;
		MSRUL_LIMIT = mSRUL_LIMIT;
		MILESTONE_PAYDUEBY = mILESTONE_PAYDUEBY;
		MILESTONE_PAYBYUNIT = mILESTONE_PAYBYUNIT;
		MSRUL_PAY_TYPE = mSRUL_PAY_TYPE;
		MSRUL_PAY_FOR = mSRUL_PAY_FOR;
		MILESTONE_EVENTSTATUS = mILESTONE_EVENTSTATUS;
		MSRUL_STATUS = mSRUL_STATUS;
		MSRUL_PROT_CAL = mSRUL_PROT_CAL;
		MS_PROT_CALASSOCTO = mS_PROT_CALASSOCTO;
		MILESTONE_ISACTIVE = mILESTONE_ISACTIVE;
		MSRUL_VISIT = mSRUL_VISIT;
		MSRUL_MS_RULE = mSRUL_MS_RULE;
		MSRUL_EVENT_STAT = mSRUL_EVENT_STAT;
		MSRUL_STUDY_STATUS = mSRUL_STUDY_STATUS;
		FK_ACCOUNT = fK_ACCOUNT;
		PK_MILESTONE = pK_MILESTONE;
		CREATOR = cREATOR;
		LAST_MODIFIED_BY = lAST_MODIFIED_BY;
		LAST_MODIFIED_DATE = lAST_MODIFIED_DATE;
		CREATED_ON = cREATED_ON;
		FK_STUDY = fK_STUDY;
		EVENT_NAME = eVENT_NAME;
		MILESTONE_ACHIEVEDCOUNT = mILESTONE_ACHIEVEDCOUNT;
		MILESTONE_DESCRIPTION = mILESTONE_DESCRIPTION;
		FK_BUDGET = fK_BUDGET;
		FK_BGTCAL = fK_BGTCAL;
		FK_BGTSECTION = fK_BGTSECTION;
		FK_LINEITEM = fK_LINEITEM;
		MILESTONE_DATE_FROM = mILESTONE_DATE_FROM;
		MILESTONE_DATE_TO = mILESTONE_DATE_TO;
		MILESTONE_HOLDBACK = mILESTONE_HOLDBACK;
		MILESTONE_DESC_CALCULATED = mILESTONE_DESC_CALCULATED;
		FK_VISIT = fK_VISIT;
		FK_EVENTASSOC = fK_EVENTASSOC;
		LAST_MODIFIED_BY_FK = lAST_MODIFIED_BY_FK;
		CREATOR_FK = cREATOR_FK;
		MILERULE_SUBTYP = mILERULE_SUBTYP;
		MILEPS_SUBTYP = mILEPS_SUBTYP;
		MILEPAY_SUBTYP = mILEPAY_SUBTYP;
		MILEPAYFR_SUBTYPE = mILEPAYFR_SUBTYPE;
		MILESTAT_SUBTYP = mILESTAT_SUBTYP;
		MILEVISIT_SUBTYP = mILEVISIT_SUBTYP;
	}
	
	
	
	
	
	
}