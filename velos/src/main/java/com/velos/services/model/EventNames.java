/**
 * Created On Jul 5, 2011
 */
package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Kanwaldeep
 *
 */
public class EventNames implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3824675785307121819L;
	private List<String> eventName = new ArrayList<String>();

	public List<String> getEventName() {
		return eventName;
	}

	public void setEventName(List<String> eventName) {
		this.eventName = eventName;
	} 
	
	public void addEventName(String eventName)
	{
		this.eventName.add(eventName); 
	}
	
}
