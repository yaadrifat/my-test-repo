package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="networkIdentifier")
public class NetIdentifier implements Serializable{
	
	private static final long serialVersionUID = 1456562568486339045L;
	private SimpleIdentifier networkIdentfier;
	
	public SimpleIdentifier getNetworkIdentfier() {
		return networkIdentfier;
	}
	public void setNetworkIdentfier(SimpleIdentifier networkIdentfier) {
		this.networkIdentfier = networkIdentfier;
	}
}
