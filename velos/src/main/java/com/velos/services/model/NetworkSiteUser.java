package com.velos.services.model;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.velos.services.model.Netwrk;
import com.velos.services.model.SiteLevelDetail;
@XmlRootElement(namespace="networkSiteUserP")
@XmlType(propOrder={"networkSiteUser"})
public class NetworkSiteUser implements Serializable {
	/**
	 * 
	 */
	
	private String userSitePK;
	
	public String getUserSitePK() {
		return userSitePK;
	}
	public void setUserSitePK(String userSitePK) {
		this.userSitePK = userSitePK;
	}
	
	
	
}
