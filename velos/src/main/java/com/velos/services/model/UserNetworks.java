package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Network")
public class UserNetworks implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8031445360320287120L;
	private NetworkIdentifier networkIdentifier;
	private String name;
	private String description;
	private String relationshipPK;
	private Code networkStatus;
	private Code networkRelationshipType;
	
	//private String NetworkStatus;
	private UserSites sites;

	public NetworkIdentifier getNetworkIdentifier() {
		return networkIdentifier;
	}
	public void setNetworkIdentifier(NetworkIdentifier networkIdentifier) {
		this.networkIdentifier = networkIdentifier;
	}
	public String getName() {
		return name;
	}
	public void setName(String Name) {
		name = Name;
	}
	public Code getNetworkStatus() {
		return networkStatus;
	}
	public void setNetworkStatus(Code NetworkStatus) {
		networkStatus = NetworkStatus;
	}
	public String getRelationshipPK() {
		return relationshipPK;
	}
	public void setRelationshipPK(String relationShip_PK) {
		relationshipPK = relationShip_PK;
	}
	public Code getNetworkRelationShipType() {
		return networkRelationshipType;
	}
	public void setNetworkRelationShipType(Code networkRelationShipType) {
		networkRelationshipType = networkRelationShipType;
	}
	/*public String getNetworkStatus() {
		return NetworkStatus;
	}*/
	/*public void setNetworkStatus(String networkStatus) {
		NetworkStatus = networkStatus;
	}*/
	public UserSites getSites() {
		return sites;
	}
	public void setSites(UserSites sites) {
		this.sites = sites;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
