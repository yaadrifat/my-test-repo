
package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Virendra
 *
 */
@XmlRootElement(name="VersionIdentifier")
@XmlAccessorType(XmlAccessType.FIELD)
public class VersionIdentifier extends SimpleIdentifier implements Serializable{

	private static final long serialVersionUID = 6611022357398961541L;

	protected String versionName;
	//protected String budgetVersion; 	

	public String getVersionName() {
		return versionName;
	}

	public void setBudgetName(String versionName) {
		this.versionName = versionName;
	}
	
	/*public String getBudgetVersion() {
		return budgetVersion;
	}

	public void setBudgetVersion(String budgetVersion) {
		this.budgetVersion = budgetVersion;
	}*/
	
	

}
