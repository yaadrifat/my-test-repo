package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;
@XmlRootElement(name="NetworksDetailsResult")
@XmlAccessorType(XmlAccessType.FIELD)
public class NetworkSiteDetailsResult implements Serializable{
	private static final long serialVersionUID = 8232983964685718366L;
	
	public static QName ORG_MORE_DETAILS_NS = new QName("www.velos.com","OrganizationMoreDetails",	"Map");
		
		
		
		private String name;
		private String description;
		private Code networkStatus;
		private String relationshipPK;
		private Code networkRelationshipType;
		private NetworkIdentifier networkIdentifier;
		protected List<NVPair> networkMoreDetails;
		//private String NetworkStatus;
		private Sites sites;
		

		public NetworkIdentifier getNetworkIdentifier() {
			return networkIdentifier;
		}
		public void setNetworkIdentifier(NetworkIdentifier networkIdentifier) {
			this.networkIdentifier = networkIdentifier;
		}
		public String getName() {
			return name;
		}
		public void setName(String Name) {
			name = Name;
		}
		public Code getNetworkStatus() {
			return networkStatus;
		}
		public void setNetworkStatus(Code NetworkStatus) {
			networkStatus = NetworkStatus;
		}
		public String getRelationshipPK() {
			return relationshipPK;
		}
		public void setRelationshipPK(String relationShip_PK) {
			relationshipPK = relationShip_PK;
		}
		public Code getNetworkRelationShipType() {
			return networkRelationshipType;
		}
		public void setNetworkRelationShipType(Code networkRelationShipType) {
			networkRelationshipType = networkRelationShipType;
		}
		/*public String getNetworkStatus() {
			return NetworkStatus;
		}*/
		/*public void setNetworkStatus(String networkStatus) {
			NetworkStatus = networkStatus;
		}*/
		public Sites getSites() {
			return sites;
		}
		public void setSites(Sites sites) {
			this.sites = sites;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}

		public List<NVPair> getMoreNetworkDetails() {
			return networkMoreDetails;
		}
		public void setMoreNetworkDetails(List<NVPair> NetworkMoreDetails) {
			this.networkMoreDetails = NetworkMoreDetails;
		}
}
