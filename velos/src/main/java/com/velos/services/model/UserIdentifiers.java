package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="userIdentifier")
public class UserIdentifiers extends SimpleIdentifier{

	/**
	 * 
	 */
	private static final long serialVersionUID = -565803713332783699L;
	private String loginName;
	
	public String getLoginName(){
		return loginName;
	}
	
	public void setLoginName(String loginName){
		this.loginName = loginName;
	}

}
