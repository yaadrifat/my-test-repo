package com.velos.services.model;
import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import com.velos.services.model.NetworkSiteDetail;

@XmlRootElement(name="removeNetworkSite")
@XmlAccessorType(XmlAccessType.FIELD)



public class RemoveNetworkSite implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7409589582181296226L;
	List<NetworkSiteDetail> networkSite=new ArrayList<NetworkSiteDetail>();

	public List<NetworkSiteDetail> getNetworkSiteDetailList() {
		return networkSite;
	}

	public void setNetworkSiteDetail(List<NetworkSiteDetail> networkSite) {
		this.networkSite = networkSite;
	}

}