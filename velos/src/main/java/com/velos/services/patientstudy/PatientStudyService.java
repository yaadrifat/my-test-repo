package com.velos.services.patientstudy;

import java.util.List;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientProtocolIdentifier;
import com.velos.services.model.PatientStudy;
import com.velos.services.model.PatientStudyId;
import com.velos.services.model.StudyIdentifier;
/**
 * Remote Interface with declarations of PatientStudies methods 
 * @author Virendra
 *
 */
@Remote
public interface PatientStudyService{
	/**
	 * @param patientId
	 * @return List<PatientStudy>
	 * @throws OperationException
	 */
	public List<PatientStudy> getPatientStudies(PatientIdentifier patientId)
	throws OperationException;
	
	/**
	 * 
	 * @param patientId
	 * @param studyId
	 * @return PatientStudyId which is a string
	 * @throws OperationException
	 */
	public PatientStudyId getPatientStudyId(PatientIdentifier patientId,
			StudyIdentifier studyId) throws OperationException;
	
	/**
	 * 
	 * @param patProtId
	 * @return PatientProtocolIdentifier
	 * @throws OperationException
	 */
	public PatientProtocolIdentifier getPatientStudyInfo(Integer patProtId) throws OperationException;
}