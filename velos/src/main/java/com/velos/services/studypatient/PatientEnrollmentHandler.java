/**
 * Created On Jun 30, 2011
 */
package com.velos.services.studypatient;


import java.text.SimpleDateFormat;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.SessionContext;

import org.apache.log4j.Logger;

import com.velos.epat.audit.service.AuditRowEpatAgent;
import com.velos.epat.business.common.AuditRowEpatDao;
import com.velos.eres.audit.service.AuditRowEresAgent;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.PatFacilityDao;
import com.velos.eres.business.common.PatStudyStatDao;
import com.velos.eres.business.common.SiteDao;
import com.velos.eres.business.patProt.impl.PatProtBean;
import com.velos.eres.business.patStudyStat.impl.PatStudyStatBean;
import com.velos.eres.business.person.impl.PersonBean;
import com.velos.eres.business.study.impl.StudyBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.patFacilityAgent.PatFacilityAgentRObj;
import com.velos.eres.service.patStudyStatAgent.PatStudyStatAgentRObj;
import com.velos.eres.service.personAgent.PersonAgentRObj;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.service.util.SVC;
import com.velos.eres.web.account.AccountJB;
import com.velos.eres.web.user.ConfigDetailsObject;
import com.velos.eres.web.user.ConfigFacade;
import com.velos.services.AbstractService;
import com.velos.services.CRUDAction;
import com.velos.services.CodeNotFoundException;
import com.velos.services.CompletedAction;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.ValidationException;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Patient;
import com.velos.services.model.PatientEnrollmentDetails;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientProtocolIdentifier;
import com.velos.services.model.PatientStudyStatusIdentifier;
import com.velos.services.model.UpdateMStudyPatientStatus;
import com.velos.services.patientdemographics.PatientDemographicsHelper;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;

/**
 * @author Kanwaldeep
 *
 */
public class PatientEnrollmentHandler {
	
	private static Logger logger = Logger.getLogger(PatientEnrollmentHandler.class); 
	
	private int enrollPatientToStudy(PatientEnrollmentDetails patientEnrollmentDetails
			,Map<String,Object> parameters, int patientPK, Integer studyPK, Map<String, Object> patientData) throws OperationException
	{
		
				
     	PatProtBean patProtBean = new PatProtBean();
     	UserBean callingUser = (UserBean) parameters.get("callingUser"); 
    	
		patProtBean.setCreator(StringUtil.integerToString(callingUser.getUserId())); 
		patProtBean.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE); 
		//Add new patient get status as "1" to identify current record		
		patProtBean.setPatProtStat("1"); 
		return addStudyPatientStatus(patientEnrollmentDetails, parameters, patientPK, studyPK, patientData, patProtBean); 	
		
		
	}	
	
	public int enrollPatientToStudy(PatientIdentifier patientIdentifier, Integer studyPK, PatientEnrollmentDetails patientEnrollmentDetails, Map<String,Object> parameters)
	throws OperationException{

		int enrollmentPK = 0; 
		ResponseHolder responseHolder = (ResponseHolder) parameters.get("ResponseHolder");
		PersonAgentRObj personAgent = (PersonAgentRObj) parameters.get("personAgent");
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("objectMapService");
		UserBean callingUser = (UserBean) parameters.get("callingUser");
		StudyAgent studyAgent = (StudyAgent) parameters.get("studyAgent");
		PatStudyStatAgentRObj patStudyStatAgent = (PatStudyStatAgentRObj) parameters.get("patStudyStatAgent");
		Integer patientPK = null;
		
		if(patientIdentifier != null && (patientIdentifier.getOID() != null || 
				(patientIdentifier.getPatientId() != null && patientIdentifier.getOrganizationId() != null)||
				(patientIdentifier.getPK()!=null || patientIdentifier.getPK()!=0)))
		{
			try {
				patientPK = ObjectLocator.personPKFromPatientIdentifier(callingUser, patientIdentifier, objectMapService);
			} catch (MultipleObjectsFoundException e) {
				responseHolder.addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, 
						"Multiple Patients found")); 
				throw new OperationException(); 
			}
		}else{
			responseHolder.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Patient OID or PatientId and OrganizationIdentifier is required")); 
			throw new OperationException(); 
		}
				
		if(patientPK == null || patientPK == 0)
		{
			responseHolder.addIssue(
					new Issue(
						IssueTypes.PATIENT_NOT_FOUND, 
						"Patient not Found for PatientIdentifier OID: " + patientIdentifier.getOID() + " patientID: " + patientIdentifier.getPatientId()));
			throw new OperationException(); 
		}
		
		List<Integer> patientList = StudyPatientDAO.getAllPatients(callingUser.getUserId()); 		
		if(!patientList.contains(patientPK))
		{
			responseHolder.addIssue(	
					new Issue(
					IssueTypes.DATA_VALIDATION, 
					"User is not authorized to enroll given patient"));
			throw new OperationException(); 
		}
	
		
		if(isPatientAlreadyEnrolledToThisStudy(patientPK, studyPK, studyAgent, callingUser.getUserId()))
		{
			responseHolder.addIssue(new Issue(IssueTypes.PATIENT_ALREADY_ENROLLED, "Patient is already enrolled to this Study")); 
			throw new OperationException(); 
			
		}
		
		PersonBean personBean = personAgent.getPersonDetails(patientPK); 
		Map<String, Object> patientData = new HashMap<String,Object>(); 
		
		PatFacilityAgentRObj patFacilityAgent = (PatFacilityAgentRObj) parameters.get("patFacilityAgent"); 
		PatFacilityDao patFacilityDao = patFacilityAgent.getPatientFacilitiesSiteIds(patientPK); 
	
		ArrayList<Integer> siteToEnroll = new ArrayList<Integer>(); 
		
		for( int j = 0; j < patFacilityDao.getPatientSite().size(); j++)
		{
			siteToEnroll.add(StringUtil.stringToInteger(patFacilityDao.getPatientSite().get(j).toString())); 
		}
		
		patientData.put("patientOrganizations", siteToEnroll); 
		patientData.put("patientDOB",  personBean.getPersonDb()); 
		patientData.put("patientCode", personBean.getPersonPId()); 
	
		enrollmentPK = enrollPatientToStudy(patientEnrollmentDetails, parameters, patientPK, studyPK, patientData); 
		
		PatStudyStatBean protStudyStatbean = new PatStudyStatBean();
		protStudyStatbean = patStudyStatAgent.getPatStudyStatDetails(enrollmentPK); 
		
		ObjectMap patProtMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATPROT, protStudyStatbean.getPatProtObj().getPatProtId()); 
		PatientProtocolIdentifier patprotIdentifier = new PatientProtocolIdentifier(); 
		patprotIdentifier.setOID(patProtMap.getOID());
		patprotIdentifier.setPK(protStudyStatbean.getPatProtObj().getPatProtId());
		responseHolder.addObjectCreatedAction(patprotIdentifier); 
		
		return enrollmentPK; 
	}
	
	
	public int addStudyPatientStatus(PatientEnrollmentDetails patientEnrollmentDetails,
			Map<String,Object> parameters, Integer patientPK, Integer studyPK) throws OperationException
	{
		int patStudyStatusPK = 0;
		PatStudyStatAgentRObj patStudyStatAgent = (PatStudyStatAgentRObj) parameters.get("patStudyStatAgent"); 
		PersonAgentRObj personAgent = (PersonAgentRObj) parameters.get("personAgent"); 
		UserBean callingUser = (UserBean) parameters.get("callingUser"); 
		PersonBean personBean = personAgent.getPersonDetails(patientPK);
		if(personBean==null)
		{
			((ResponseHolder)parameters.get("ResponseHolder")).addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND,"Patient Not found"));
			throw new OperationException();
		}
		
		PatStudyStatBean patStudystatBean = 
			patStudyStatAgent.getPatStudyStatDetails(StudyPatientDAO.getCurrentPatientStatus(patientPK, studyPK)); 
		
		PatProtBean patProtBean = patStudystatBean.getPatProtObj();
		
		 
		Map<String, Object> patientData = new HashMap<String,Object>(); 
		
		PatFacilityAgentRObj patFacilityAgent = (PatFacilityAgentRObj) parameters.get("patFacilityAgent"); 
		PatFacilityDao patFacilityDao = patFacilityAgent.getPatientFacilitiesSiteIds(patientPK); 
	
		ArrayList<Integer> siteToEnroll = new ArrayList<Integer>(); 
		
		for( int j = 0; j < patFacilityDao.getPatientSite().size(); j++)
		{
			siteToEnroll.add(StringUtil.stringToInteger(patFacilityDao.getPatientSite().get(j).toString())); 
		}
		
		patientData.put("patientOrganizations", siteToEnroll); 
		patientData.put("patientDOB",  personBean.getPersonDb()); 
		patientData.put("patientCode", personBean.getPersonPId());
		patientData.put("patientStudyId", patProtBean.getPatStudyId());
	
		patStudyStatusPK = addStudyPatientStatus(patientEnrollmentDetails, parameters, patientPK, studyPK, patientData, patProtBean); 
		
		//patStudystatBean = patStudyStatAgent.getPatStudyStatDetails(StudyPatientDAO.getCurrentPatientStatus(patientPK, studyPK)); 
		/*if (patientEnrollmentDetails.isCurrentStatus()) {
			patStudystatBean.setCurrentStat("0");
			patStudystatBean.setModifiedBy(StringUtil.integerToString(callingUser.getUserId()));
			patStudyStatAgent.updatePatStudyStat(patStudystatBean);;
		}*/
		return patStudyStatusPK; 
	}
	
	private int addStudyPatientStatus(PatientEnrollmentDetails patientEnrollmentDetails,
			Map<String,Object> parameters, int patientPK, Integer studyPK, Map<String, Object> patientData, PatProtBean patProtBean) throws OperationException
	{
		PersonAgentRObj personAgent = (PersonAgentRObj) parameters.get("personAgent");
		PersonBean personB = personAgent.getPersonDetails(patientPK); 		
		PatStudyStatBean protStudyStatbean = new PatStudyStatBean(); 	
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("objectMapService"); 
		UserBean callingUser = (UserBean) parameters.get("callingUser"); 
		Date patientDOB = (Date) patientData.get("patientDOB"); 
		String patientCode = patientData.get("patientCode").toString();
		String patientStudyId =(patientData.get("patientStudyId")==null)?"":patientData.get("patientStudyId").toString();
		if(patientEnrollmentDetails.getPatientStudyId() == null || patientEnrollmentDetails.getPatientStudyId().length() == 0)
		{
			patientEnrollmentDetails.setPatientStudyId(patientCode);
		}
		validatePatientEnrollmentDetails(patientEnrollmentDetails, callingUser, parameters, patientDOB); 
		UserAgentRObj userAgent = (UserAgentRObj) parameters.get("userAgent"); 
		SessionContext sessionContext  = (SessionContext) parameters.get("sessionContext"); 
		ResponseHolder responseHolder = (ResponseHolder) parameters.get("ResponseHolder"); 
		//PatProt data
		patProtBean.setPatProtPersonId(StringUtil.integerToString(patientPK));
		patProtBean.setPatProtStudyId(StringUtil.integerToString(studyPK)); 
		//Enrollment Details - if status = enrolled
		
		if(patientEnrollmentDetails.getStatus().getCode().equals("enrolled"))
		{
			patProtBean.setPatProtRandomNumber(patientEnrollmentDetails.getRandomizationNumber());
			
			patProtBean.setPatProtEnrolDt(patientEnrollmentDetails.getStatusDate()); 

			if(patientEnrollmentDetails.getEnrolledBy() != null)
			{
				UserBean enrollingUserBean; 			
				try{
					enrollingUserBean = ObjectLocator.userBeanFromIdentifier(callingUser, patientEnrollmentDetails.getEnrolledBy(), userAgent, sessionContext, objectMapService);
					if(enrollingUserBean != null)
					{
						patProtBean.setPatProtUserId(StringUtil.integerToString(enrollingUserBean.getUserId())); 
					}else
					{
						responseHolder.addIssue(
								new Issue(
										IssueTypes.USER_NOT_FOUND ,
								"EnrolledBy User Not found"));
						throw new OperationException();
					}
				} catch (MultipleObjectsFoundException e) {

					responseHolder.addIssue(
							new Issue(
									IssueTypes.MULTIPLE_OBJECTS_FOUND, 
							"Found multiple objects that matched Enrolling User"));
					throw new OperationException();
				} 

			}
		}
		
		//Informed Consent Details
		if(patientEnrollmentDetails.getStatus().getCode().equals("infConsent"))
		{
			 protStudyStatbean.setPtstConsentVer(patientEnrollmentDetails.getInformedConsentVersionNumber()) ; 	
		}

		//Follow-up Details
		if(patientEnrollmentDetails.getStatus().getCode().equals("followup"))
		{
			protStudyStatbean.setPtstNextFlwupPersistent(patientEnrollmentDetails.getNextFollowUpDate());
		}
		
		//Screening Details
		if(patientEnrollmentDetails.getStatus().getCode().equals("screening"))
		{
			protStudyStatbean.setScreenNumber(patientEnrollmentDetails.getScreenNumber());
			
			if(patientEnrollmentDetails.getScreenedBy() != null)
			{
				UserBean screeningUserBean; 			
				try{
					screeningUserBean = ObjectLocator.userBeanFromIdentifier(callingUser, patientEnrollmentDetails.getScreenedBy(), userAgent, sessionContext, objectMapService);
					if(screeningUserBean != null)
					{
						protStudyStatbean.setScreenedBy(StringUtil.integerToString(screeningUserBean.getUserId())); 
					}else
					{
						responseHolder.addIssue(
								new Issue(
										IssueTypes.USER_NOT_FOUND, 
								"ScreenBy User not found"));
						throw new OperationException();
					}
				} catch (MultipleObjectsFoundException e) {

					responseHolder.addIssue(
							new Issue(
									IssueTypes.MULTIPLE_OBJECTS_FOUND, 
							"Found multiple objects that matched ScreenedBy User"));
					throw new OperationException();
				} 

			}
			
			
			if(patientEnrollmentDetails.getScreeningOutcome() != null)
			{
				String screeningOutcome = null; 
				try{
					screeningOutcome = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getScreeningOutcome(),
						CodeCache.CODE_TYPE_PATPROT_SCREENING_OUTCOME, callingUser); 
				
				}catch(CodeNotFoundException e)
				{
					responseHolder.addIssue(
							new Issue(
								IssueTypes.DATA_VALIDATION, 
								"ScreeningOutcome Code Not Found: " + patientEnrollmentDetails.getScreeningOutcome().getCode()));
					throw new OperationException();
				}
				
				protStudyStatbean.setScreeningOutcome(screeningOutcome);				
			}
		
			
		}


		//additional information
		if("".equals(patientStudyId) || patientStudyId==null){
			patientStudyId = patientEnrollmentDetails.getPatientStudyId(); 
			if(patientStudyId == null || patientStudyId.length() == 0) patientStudyId = patientCode; 
			patProtBean.setPatStudyId(patientStudyId); 
		}
		
		
		
		if(patientEnrollmentDetails.getEnrollingSite() != null)
		{
			// Get Enrolling Sites for this Study
			//Bug Fix 6758,6789,6757
			
			Integer enrollingSite;
			List<Integer> siteToEnroll = (List<Integer>) patientData.get("patientOrganizations"); 
			try {
				enrollingSite = ObjectLocator.sitePKFromIdentifier(callingUser, patientEnrollmentDetails.getEnrollingSite(), sessionContext, objectMapService);
				
				if(enrollingSite != null && enrollingSite != 0)
				{
					if(siteToEnroll.contains(enrollingSite))
					{
						patProtBean.setEnrollingSite(StringUtil.integerToString(enrollingSite)); 
					}else
					{
						responseHolder.addIssue(
								new Issue(
										IssueTypes.ORGANIZATION_AUTHORIZATION, 
								"EnrollingSite is not in list of Study Organizations where user can enroll patient"));
						throw new OperationException(); 
					}
				}else
				{
					responseHolder.addIssue(
							new Issue(
									IssueTypes.ORGANIZATION_NOT_FOUND,
									"EnrollingSite not found")); 
					throw new OperationException(); 
					
				}
			} catch (MultipleObjectsFoundException e) {
				responseHolder.addIssue(
						new Issue(
								IssueTypes.MULTIPLE_OBJECTS_FOUND, 
								"Found multiple objects that matched Enrolling Site"));
				throw new OperationException();
			} 
			
		}
		
		if(patientEnrollmentDetails.getAssignedTo() != null )
		{
			try{
			UserBean assignToBean = ObjectLocator.userBeanFromIdentifier(callingUser, patientEnrollmentDetails.getAssignedTo(), userAgent, sessionContext, objectMapService);
			if(assignToBean != null)
			{
				patProtBean.setAssignTo(StringUtil.integerToString(assignToBean.getUserId())); 
			}else
			{
				responseHolder.addIssue(
						new Issue(
								IssueTypes.USER_NOT_FOUND ,
						"AssignedTo User Not found"));
				throw new OperationException();
			}
			}catch(MultipleObjectsFoundException e)
			{
				responseHolder.addIssue(
						new Issue(
								IssueTypes.MULTIPLE_OBJECTS_FOUND, 
								"Found multiple objects that matched Assigned to User"));
				throw new OperationException();
			}
		}
		
		
		if(patientEnrollmentDetails.getPhysician() != null)
		{
			UserBean physicianBean;
			try {
				physicianBean = ObjectLocator.userBeanFromIdentifier(callingUser, patientEnrollmentDetails.getPhysician(), userAgent, sessionContext, objectMapService);
				if(physicianBean != null)
				{
					patProtBean.setPatProtPhysician(StringUtil.integerToString(physicianBean.getUserId())); 
				}else
				{
					responseHolder.addIssue(
							new Issue(
									IssueTypes.USER_NOT_FOUND ,
							"Physician Not found"));
					throw new OperationException();
				}
			
			} catch (MultipleObjectsFoundException e) {
				responseHolder.addIssue(
						new Issue(
								IssueTypes.MULTIPLE_OBJECTS_FOUND, 
								"Found multiple objects that matched Physician"));
				throw new OperationException();
			} 
			
		}
		
		if(patientEnrollmentDetails.getTreatmentLocation() != null )
		{
			try{
				 String treatmentlocation = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getTreatmentLocation(), CodeCache.CODE_TYPE_PATPROT_TREATMENT_LOCATION, callingUser); 
				 patProtBean.setTreatmentLoc(treatmentlocation); 
			}catch(CodeNotFoundException ce)
			{
				responseHolder.addIssue(
						new Issue(
								IssueTypes.DATA_VALIDATION, 
								"TreatmentLocation Code Not Found: " + patientEnrollmentDetails.getTreatmentLocation().getCode()));
					throw new OperationException();
			}
		}
		
		if(patientEnrollmentDetails.getTreatingOrganization() != null)
		{
			try {
				
				
				Integer treatingOrganization = ObjectLocator.sitePKFromIdentifier(callingUser, patientEnrollmentDetails.getTreatingOrganization(), sessionContext, objectMapService);
				
				if(treatingOrganization != null && treatingOrganization != 0)
				{
					List<Integer> treatingOrg = new ArrayList<Integer>(); 			
					SiteAgentRObj siteAgentBean = (SiteAgentRObj) parameters.get("siteAgent"); 				 
					SiteDao siteDao = siteAgentBean.getByAccountId(StringUtil.stringToInteger(callingUser.getUserAccountId()));
					//TODO SiteAgentBean .get SiteValues(acctID); 

					for( int j = 0; j < siteDao.getSiteIds().size(); j++)
					{
						treatingOrg.add(StringUtil.stringToInteger(siteDao.getSiteIds().get(j).toString())); 
					}
					
					if(treatingOrg.contains(treatingOrganization))
					{					
						patProtBean.setpatOrg(StringUtil.integerToString(treatingOrganization));
					}else
					{
						responseHolder.addIssue(
								new Issue(
										IssueTypes.ORGANIZATION_AUTHORIZATION, 
								"TreatingOrganization is not in list of possible Treating Organizations"));
						throw new OperationException(); 
					}
				}else
				{
					responseHolder.addIssue(
							new Issue(
									IssueTypes.ORGANIZATION_NOT_FOUND, 
							"TreatingOrganization not found"));
					throw new OperationException(); 
				}
				 
				
				
				
			} catch (MultipleObjectsFoundException e) {
				responseHolder.addIssue(
						new Issue(
								IssueTypes.MULTIPLE_OBJECTS_FOUND, 
								"Found multiple objects that matched Treating Organization"));
				throw new OperationException();
			} 
		}
		
		
		//Evaluable Status
		
		if(patientEnrollmentDetails.getDateOfDeath() != null)
		{
			patProtBean.setPtstDeathDatePersistent(patientEnrollmentDetails.getDateOfDeath()); 
		}

		patProtBean.setPatProtStudyId(StringUtil.integerToString(studyPK)); 
		if(patientEnrollmentDetails.getEvaluationFlag() != null)
		{
			try{
				String evaluationFlag = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getEvaluationFlag(),
						CodeCache.CODE_TYPE_PATPROT_EVALUATION_FLAG, callingUser); 
				if(evaluationFlag != null)
				{
					patProtBean.setPtstEvalFlag(evaluationFlag); 
				}else
				{
					throw new OperationException(); 
				}
			}catch(CodeNotFoundException ce)
			{
				responseHolder.addIssue(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"EvaluationFlag Code Not Found: " + patientEnrollmentDetails.getEvaluationFlag().getCode()));
				throw new OperationException();
			}
		}

		if(patientEnrollmentDetails.getEvaluationStatus() != null)
		{
			String evaluationStatus = null; 
			try{
			evaluationStatus = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getEvaluationStatus(),
					CodeCache.CODE_TYPE_PATPROT_EVALATION_STATUS, callingUser); 
			
			}catch(CodeNotFoundException e)
			{
				responseHolder.addIssue(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"EvaluationStatus Code Not Found: " + patientEnrollmentDetails.getEvaluationStatus().getCode()));
				throw new OperationException();
			}		
			patProtBean.setPtstEval(evaluationStatus); 	
			
			
		}
		
		if(patientEnrollmentDetails.getInevaluationStatus() != null)
		{
			try{
				String inEval = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getInevaluationStatus(),
						CodeCache.CODE_TYPE_PATPROT_INEVALATION_STATUS, callingUser);
				
				patProtBean.setPtstInEval(inEval); 
			}catch(CodeNotFoundException e){
				responseHolder.addIssue(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"InevaluationStatus Code Not Found: " + patientEnrollmentDetails.getInevaluationStatus().getCode()));
				throw new OperationException();
			}
			
		}
	
		
			
		//Patient Status
		
		if(patientEnrollmentDetails.getSurvivalStatus() != null)
		{
			try{
				String survivalStatus = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getSurvivalStatus(), CodeCache.CODE_TYPE_PATPROT_SURVIVAL_STATUS, callingUser); 
				patProtBean.setPtstSurvival(survivalStatus);
				
				
				if(personB.getPersonStatus().equals(survivalStatus) &&  patientEnrollmentDetails.getSurvivalStatus().getCode().equals("dead"))
				{
					personB.setPersonStatus(survivalStatus); 
					if(patientEnrollmentDetails.getDateOfDeath() != null) personB.setPersonDeathDt(patientEnrollmentDetails.getDateOfDeath());
				}				
				
			}catch(CodeNotFoundException ce)
			{
				responseHolder.addIssue(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"Survival Status Code Not Found: " + patientEnrollmentDetails.getSurvivalStatus().getCode()));
				throw new OperationException();
			}
			
		}
		
		
		if(patientEnrollmentDetails.getDateOfDeath() != null) patProtBean.setPtstDeathDatePersistent(patientEnrollmentDetails.getDateOfDeath()); 
		if(patientEnrollmentDetails.getSpecifyCause() != null) personB.setDthCauseOther(patientEnrollmentDetails.getSpecifyCause()); 
		if(patientEnrollmentDetails.getCauseOfDeath() != null)
		{
			try{
				String patDeathCause = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getCauseOfDeath(), 
						CodeCache.CODE_TYPE_PATIENT_DEATH_CAUSE, callingUser); 
				personB.setPatDthCause(patDeathCause); 
			}catch(CodeNotFoundException e)
			{
				responseHolder.addIssue(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"CauseOfDeath Code Not Found: " + patientEnrollmentDetails.getCauseOfDeath().getCode()));
				throw new OperationException(); 
			}
		}
		
		if(patientEnrollmentDetails.getDeathRelatedToTrial() != null)
		{
			try{
				String deathRelatedToTrial = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getDeathRelatedToTrial(), 
						CodeCache.CODE_TYPE_PATPROT_STUDY_RELATED_DEATH, callingUser); 
				patProtBean.setPtstDthStdRel(deathRelatedToTrial); 
			}catch(CodeNotFoundException e)
			{
				responseHolder.addIssue(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"DeathRelatedToTrial Code Not Found: " + patientEnrollmentDetails.getDeathRelatedToTrial().getCode()));
				throw new OperationException(); 
			}
		}
		
		patProtBean.setPtstDthStdRelOther(patientEnrollmentDetails.getReasonOfDeathRelatedToTrial()); 
		
			
		protStudyStatbean.setPatientId(StringUtil.integerToString(patientPK)); 
		protStudyStatbean.setStudyId(StringUtil.integerToString(studyPK)); 
		protStudyStatbean.setPatProtObj(patProtBean); 
		
		if(patientEnrollmentDetails.getStatus() != null)
		{
			String status = ""; 
			try{
			 status = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getStatus(), CodeCache.CODE_TYPE_PATPROT_STATUS, callingUser); 
			//Bug fix 6836
			
			}catch(CodeNotFoundException e)
			{
				responseHolder.addIssue(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"status Code Not Found: " + patientEnrollmentDetails.getStatus().getCode()));
				throw new OperationException(); 
			}
			protStudyStatbean.setPatStudyStat(status);
			if(patientEnrollmentDetails.getStatusReason() != null)
			{
				String  statusReason;  
				try
				{
					statusReason= AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getStatusReason(),
							 patientEnrollmentDetails.getStatus().getCode(), callingUser); 
				}catch(CodeNotFoundException ce)
				{
					responseHolder.addIssue(
							new Issue(
								IssueTypes.DATA_VALIDATION, 
								"status Code Not Found: " + patientEnrollmentDetails.getStatusReason().getCode()));
					throw new OperationException(); 
				}
								
				protStudyStatbean.setPatStudyStatReason(statusReason); 
			}
		}
		
		personB.setModifiedBy(StringUtil.integerToString(callingUser.getUserId())); 
		int patientSuccessfullyUpdated = personAgent.updatePerson(personB); 
		if(patientSuccessfullyUpdated != 0 )
		{
			responseHolder.addIssue(new Issue(IssueTypes.PATIENT_ERROR_UPDATE_DEMOGRAPHICS));
			throw new OperationException(); 
		}
		
		if(patientEnrollmentDetails.getStatusDate() != null) protStudyStatbean.setStartDate(patientEnrollmentDetails.getStatusDate()); 
		protStudyStatbean.setCreator(StringUtil.integerToString(callingUser.getUserId())); 
		protStudyStatbean.setCurrentStat(patientEnrollmentDetails.isCurrentStatus()? "1": "0"); 
		protStudyStatbean.setNotes(patientEnrollmentDetails.getNotes()); 
		protStudyStatbean.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE); 
				
		PatStudyStatAgentRObj patStudyStatAgent = (PatStudyStatAgentRObj) parameters.get("patStudyStatAgent"); 
		int enrollmentPK = patStudyStatAgent.setPatStudyStatDetails(protStudyStatbean); 
		if(enrollmentPK < 0)
		{
			responseHolder.addIssue(new Issue(IssueTypes.ERROR_CREATE_PATIENT_ENROLLMENT, "Failed to addPatientStudyStatus"));
			if(enrollmentPK == -3)
			{
				responseHolder.addIssue(new Issue(IssueTypes.ERROR_CREATE_PATIENT_ENROLLMENT, "Enrolled Status already exists"));
				throw new OperationException(); 
			}
			throw new OperationException(); 
			
		}
		
		ObjectMap patProtStatusMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATSTUDYSTAT, enrollmentPK); 
		PatientStudyStatusIdentifier patStudyStatusIdentifier = new PatientStudyStatusIdentifier(); 
		patStudyStatusIdentifier.setOID(patProtStatusMap.getOID()); 
		patStudyStatusIdentifier.setPK(enrollmentPK);
		responseHolder.addObjectCreatedAction(patStudyStatusIdentifier); 
		
		return enrollmentPK; 
	}

	private boolean isPatientAlreadyEnrolledToThisStudy(Integer patientPK, Integer studyPK, StudyAgent studyAgent, Integer userID) throws OperationException
	{
		PatStudyStatDao dao = studyAgent.getPatientStudies(patientPK, userID); 
		List enrolledInStudies = dao.getStudyIds(); 
		
		for(int i = 0 ; i < enrolledInStudies.size(); i++)
		{
			Integer enrolledInStudy = StringUtil.stringToInteger(enrolledInStudies.get(i).toString()); 
			if( enrolledInStudy.equals(studyPK))
			{
				return true; 
			}
		}		
		return false; 
	}

	public int createAndEnrollPatient(Patient patient, Integer studyPK, PatientEnrollmentDetails patientEnrollmentDetails, Map<String, Object> parameters)
	throws OperationException
	{
		int enrollmentPK = 0; 
		ResponseHolder response =(ResponseHolder) parameters.get("ResponseHolder"); 
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("objectMapService"); 
		PatientDemographicsHelper patientDemographicHelper = new PatientDemographicsHelper(); 
		Integer patientPK = patientDemographicHelper.createPatient(patient, parameters);	
		if(patientPK == null || patientPK == 0)
		{
			throw new OperationException(); 
		}
		
			
		Map<String, Object> patientData = new HashMap<String,Object>(); 
		patientData.put("patientDOB",  patient.getPatientDemographics().getDateOfBirth()); 
		patientData.put("patientCode", patient.getPatientDemographics().getPatientCode());
		patientData.put("patientOrganizations", parameters.get("patientOrganizations")); 
		
	
		enrollmentPK = enrollPatientToStudy(patientEnrollmentDetails, parameters, patientPK, studyPK, patientData);
		
		ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, patientPK); 
		
		PatientIdentifier patientIdentifier = new PatientIdentifier(); 
		patientIdentifier.setOID(map.getOID()); 
		patientIdentifier.setPK(patientPK);
		patientIdentifier.setPatientId(patient.getPatientDemographics().getPatientCode()); 
		
		
		response.addObjectCreatedAction(patientIdentifier);
	
		return enrollmentPK; 
	}
	
	
	private void validatePatientEnrollmentDetails(PatientEnrollmentDetails patientEnrollmentDetails, UserBean callingUser,Map<String, Object> parameters, Date patientDOB) throws ValidationException
	{
		
		ConfigDetailsObject configDetails
		= ConfigFacade.getConfigFacade().populateObject(StringUtil.stringToInteger(callingUser.getUserAccountId()), "patstudystatus");
		AccountJB acctJB = new AccountJB(); 
		acctJB.setAccId(StringUtil.stringToInteger(callingUser.getUserAccountId())); 
		acctJB.getAccountDetails(); 
	
		List<Issue> validationIssues = new ArrayList<Issue>();
		
		if(configDetails != null ){
			if(isMandatory(configDetails, "patstatus") && (patientEnrollmentDetails.getStatus() == null || patientEnrollmentDetails.getStatus().getCode() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_StatusCode_Mandatory)); 
			}
			
			
			if(isMandatory(configDetails, "statusdate") && patientEnrollmentDetails.getStatusDate() == null)
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_StatusDate_Mandatory)); 
			}
			
			if(isMandatory(configDetails, "reason") && (patientEnrollmentDetails.getStatusReason() == null || patientEnrollmentDetails.getStatusReason().getCode() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_StatusReason_Mandatory)); 
			}
			
			if(isMandatory(configDetails, "notes") && (patientEnrollmentDetails.getNotes() == null || patientEnrollmentDetails.getNotes().length() == 0))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_Notes_Mandatory)); 
			}
			
			if(patientEnrollmentDetails.getStatus() != null && patientEnrollmentDetails.getStatus().getCode() != null && patientEnrollmentDetails.getStatus().getCode().equals("enrolled"))
			{

				if(isMandatory(configDetails, "randomnumber") && (patientEnrollmentDetails.getRandomizationNumber() == null || patientEnrollmentDetails.getRandomizationNumber().length() == 0))
				{
					validationIssues.add(new Issue( IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_RandomNumber_Mandatory)); 
				}


				if(isMandatory(configDetails, "enrolledby") && patientEnrollmentDetails.getEnrolledBy() == null)
				{
					validationIssues.add(new Issue( IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_EnrolledBy_Mandatory)); 
				}

			}
			
			
			//Informed Consent Details
			if(patientEnrollmentDetails.getStatus() != null && patientEnrollmentDetails.getStatus().getCode() != null && patientEnrollmentDetails.getStatus().getCode().equals("infConsent"))
			{
				if(isMandatory(configDetails, "icversionnumber") && patientEnrollmentDetails.getInformedConsentVersionNumber() == null)
				{
					validationIssues.add(new Issue( IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_ICVersionNumber_Mandatory));
				}
			}

			//Follow-up Details
			if(patientEnrollmentDetails.getStatus() != null && patientEnrollmentDetails.getStatus().getCode() != null && patientEnrollmentDetails.getStatus().getCode().equals("followup"))
			{
				if(isMandatory(configDetails, "followupdate") && patientEnrollmentDetails.getNextFollowUpDate() == null)
				{
					validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_NextFollowUpDate_Mandatory)); 
				}
			}
			
			//Screening Details
			if(patientEnrollmentDetails.getStatus() != null && patientEnrollmentDetails.getStatus().getCode() != null && patientEnrollmentDetails.getStatus().getCode().equals("screening"))
			{			
			
				if(isMandatory(configDetails, "screennumber") && patientEnrollmentDetails.getScreenNumber() == null)
				{
				
					validationIssues.add(new Issue( IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_ScreenNumber_Mandatory)); 
					
				}
				
				if(isMandatory(configDetails, "screenedby") && patientEnrollmentDetails.getScreenedBy() == null)
				{
					validationIssues.add(new Issue( IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_ScreenBy_Mandatory)); 
				}
				
				if(isMandatory(configDetails, "screeningoutcome") && patientEnrollmentDetails.getScreenedBy() == null)
				{
					validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_ScreeningOutcome_Mandatory)); 
				}
			}
			// Additional Information
			
			if(isMandatory(configDetails,"enrollingsite" )
					&& (patientEnrollmentDetails.getEnrollingSite() == null || 
							(patientEnrollmentDetails.getEnrollingSite().getOID() == null 
									&& patientEnrollmentDetails.getEnrollingSite().getSiteAltId() == null 
									&& patientEnrollmentDetails.getEnrollingSite().getSiteName() == null)))
			{

				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_EnrollingSite_Mandatory)); 

			}
			
			if(isMandatory(configDetails, "assignedto") && patientEnrollmentDetails.getAssignedTo() == null)
			{
				validationIssues.add(new Issue( IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_AssignedTo_Mandatory)); 
			}
			
			if(isMandatory(configDetails, "physician") && patientEnrollmentDetails.getPhysician() == null)
			{
				validationIssues.add(new Issue( IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_Physician_Mandatory)); 
			}
			
			if(isMandatory(configDetails, "treatmentlocation") && (patientEnrollmentDetails.getTreatmentLocation() == null ||patientEnrollmentDetails.getTreatmentLocation().getCode() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_TreatmentLocation_Mandatory)); 
			}
			
			if(isMandatory(configDetails,"treatingorg" )
					&& (patientEnrollmentDetails.getTreatingOrganization() == null || 
							(patientEnrollmentDetails.getTreatingOrganization().getOID() == null 
									&& patientEnrollmentDetails.getTreatingOrganization().getSiteAltId() == null 
									&& patientEnrollmentDetails.getTreatingOrganization().getSiteName() == null)))
			{

				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_TreatingOrg_Mandatory)); 

			}
			
			if(isMandatory(configDetails, "evaluableflag") && (patientEnrollmentDetails.getEvaluationFlag() == null || patientEnrollmentDetails.getEvaluationFlag().getCode() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_EvaluationFlag_Mandatory)); 
			}
			
			if(isMandatory(configDetails, "evaluablestatus") && (patientEnrollmentDetails.getEvaluationStatus() == null || patientEnrollmentDetails.getEvaluationStatus().getCode() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_EvaluationStatusCode_Mandatory)); 
			}
			
			if(isMandatory(configDetails, "inevaluablestatus") && (patientEnrollmentDetails.getInevaluationStatus() == null || patientEnrollmentDetails.getInevaluationStatus().getCode() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_InevaluationStatusCode_Mandatory)); 
			}
			
			if(isMandatory(configDetails, "survivalstatus") && (patientEnrollmentDetails.getSurvivalStatus() == null || patientEnrollmentDetails.getSurvivalStatus().getCode() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_SurvivalStatus_Mandatory)); 
			}
			
			if(isMandatory(configDetails, "dod") && patientEnrollmentDetails.getDateOfDeath() == null)
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_DateOfDeath_Mandatory)); 
			}
			
			if(isMandatory(configDetails, "cod") && (patientEnrollmentDetails.getCauseOfDeath() == null || patientEnrollmentDetails.getCauseOfDeath().getCode() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_CauseOfDeath_Mandatory)); 
			}
			if(isMandatory(configDetails, "specifycause") && (patientEnrollmentDetails.getSpecifyCause() == null || patientEnrollmentDetails.getSpecifyCause().length() == 0))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_SpecifyCause_Mandatory)); 
			}
			if(isMandatory(configDetails, "deathrelated") && (patientEnrollmentDetails.getDeathRelatedToTrial() == null || patientEnrollmentDetails.getDeathRelatedToTrial().getCode() == null))
			{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_DeathRelatedToTrial_Mandatory)); 
			}


		}
		
		
			
// more Validation
		if(patientEnrollmentDetails.getDateOfDeath() != null && patientDOB != null && (patientEnrollmentDetails.getDateOfDeath().compareTo(patientDOB) < 0))
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_DeathDtBirthDt_Camparison));			
		}
		
		if(patientEnrollmentDetails.getStatusDate() != null && patientDOB != null && (patientEnrollmentDetails.getStatusDate().compareTo(patientDOB) < 0))
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_BirthDtStatusDt_Camparison)); 
		}
		
// Death related validation 
		
		if(patientEnrollmentDetails.getDateOfDeath() != null)
		{
			if(patientEnrollmentDetails.getSurvivalStatus() != null && patientEnrollmentDetails.getSurvivalStatus().getCode() != null )
			{
				if(!patientEnrollmentDetails.getSurvivalStatus().getCode().equals("dead"))
				{
					validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_DeathDate_SurvivalStatus)); 
				}
			}else
			{
				validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_DeathDate_SurvivalStatus)); 
			}
			
		}
		
		if(patientEnrollmentDetails.getSurvivalStatus() != null && patientEnrollmentDetails.getSurvivalStatus().getCode() != null && patientEnrollmentDetails.getSurvivalStatus().getCode().equals("dead"))
		{
			if(patientEnrollmentDetails.getDateOfDeath() == null) 
			{
				validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "dateOfDeath is required for dead Patient"));
			}
		}
		
		if(((patientEnrollmentDetails.getCauseOfDeath() != null  && patientEnrollmentDetails.getCauseOfDeath().getCode() != null)
				||(patientEnrollmentDetails.getSpecifyCause() != null && patientEnrollmentDetails.getSpecifyCause().length() > 0))
				&& (patientEnrollmentDetails.getSurvivalStatus() == null || !patientEnrollmentDetails.getSurvivalStatus().getCode().equals("dead")))
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_CauseOfDeath_SurvivalStatus)); 
		}
		if(((patientEnrollmentDetails.getReasonOfDeathRelatedToTrial() != null && patientEnrollmentDetails.getReasonOfDeathRelatedToTrial().length() > 0 )|| (patientEnrollmentDetails.getDeathRelatedToTrial() != null && patientEnrollmentDetails.getDeathRelatedToTrial().getCode() != null )) && (patientEnrollmentDetails.getSurvivalStatus() == null || !patientEnrollmentDetails.getSurvivalStatus().getCode().equals("dead")))
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_DeathRelatedToTrial_SurvivalStatus)); 
		}
		

		if (validationIssues.size() > 0){
			((ResponseHolder) parameters.get("ResponseHolder"))
			.getIssues().addAll(validationIssues); 
			
		//	response.getIssues().addAll(validationIssues);
			if (logger.isDebugEnabled()) logger.debug("Validation Issues found for PatientEnrollmentDetails");
			throw new ValidationException();
		}
		
		
	}
	
	private boolean isMandatory(ConfigDetailsObject configDetails, String field)
	{
		boolean isMandatory = false;
		ArrayList fields = configDetails.getPcfField(); 
		ArrayList mandatory = configDetails.getPcfMandatory();
		
		if(fields == null || fields.indexOf(field) == -1)
		{
			
			if(field.equals("patstatus")) return true; 
			if(field.equals("statusdate")) return true; 
			if(field.equals("enrolledby")) return true;
			if(field.equals("patstdid")) return true; 
			
		}else
		{
			if(mandatory.get(fields.indexOf(field))!=null && mandatory.get(fields.indexOf(field)).toString().equals("1")) return true; 
		}
	
		
		return isMandatory; 
	}

	public void updateStudyPatientStatus(PatStudyStatBean patStudyStatBean,
			PatientEnrollmentDetails patientEnrollmentDetails, Map<String, Object> parameters) throws OperationException {
		
		PatFacilityAgentRObj patFacilityAgent = (PatFacilityAgentRObj) parameters.get("patFacilityAgent"); 
		PersonAgentRObj personAgent = (PersonAgentRObj) parameters.get("personAgent");
		StudyAgent studyAgent=(StudyAgent) parameters.get("studyAgent");
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("objectMapService"); 
		SessionContext sessionContext  = (SessionContext) parameters.get("sessionContext"); 
		PatStudyStatAgentRObj patStudyStatAgent= (PatStudyStatAgentRObj) parameters.get("patStudyStatAgent");
		UserBean callingUser = (UserBean) parameters.get("callingUser"); 
		Integer patientPK=(Integer) parameters.get("perPK");
		Integer studyPK=(Integer) parameters.get("studyPK");
		ResponseHolder response=(ResponseHolder) parameters.get("response");
		ConfigDetailsObject configDetails=ConfigFacade.getConfigFacade().populateObject(StringUtil.stringToInteger(callingUser.getUserAccountId()), "patstudystatus");
		CodeDao codeDao=new CodeDao();
		PatProtBean patProtBean= patStudyStatBean.getPatProtObj();
		UserAgentRObj userAgent=(UserAgentRObj) parameters.get("userAgent");
		PersonBean personB = personAgent.getPersonDetails(patientPK);
		String statusType= codeDao.getCodeSubtype(StringUtil.stringToInteger(patStudyStatBean.getPatStudyStat()));
		PatStudyStatBean pstbn = new PatStudyStatBean();
		
		
		boolean isPersonBUpdated=false;
		//Patient Study Status
		//Reason
		if(patientEnrollmentDetails.getStatusReason()!=null)
		{
			if(StringUtil.isEmpty(patientEnrollmentDetails.getStatusReason().getCode())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getStatusReason().getDescription())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getStatusReason().getType()))
			{
				if(isMandatory(configDetails, "reason"))
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_StatusReason_Mandatory));
					throw new OperationException();
				}
				else
				{
					//pstbn.setPatStudyStatReason("");
					patStudyStatBean.setPatStudyStatReason("");
				}
			}
			else
			{
				String statusReason="";
				try
				{
					statusReason= AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getStatusReason(), statusType, callingUser);		
				}
				catch(CodeNotFoundException ce)
				{
					response.addIssue(new Issue(IssueTypes.CODE_NOT_FOUND,"Reason Code Not Found: " + patientEnrollmentDetails.getStatusReason().getCode()));
					throw new OperationException(); 
				}
			//	pstbn.setPatStudyStatReason(statusReason); 
				patStudyStatBean.setPatStudyStatReason(statusReason);
			}
		}
		//Status Date
		if(null!=patientEnrollmentDetails.getStatusDate())
		{
			patStudyStatBean.setStartDate(patientEnrollmentDetails.getStatusDate());
		}
		//Current Status
		if(patientEnrollmentDetails.isCurrentStatus())
		{
			/*PatStudyStatBean currentPatStatBean = patStudyStatAgent.getPatStudyCurrentStatus(StringUtil.integerToString(studyPK),StringUtil.integerToString(patientPK));
			currentPatStatBean.setCurrentStat("0");
			currentPatStatBean.setModifiedBy(StringUtil.integerToString(callingUser.getUserId()));
			if(patStudyStatAgent.updatePatStudyStat(currentPatStatBean)<0)
			{
				response.addIssue(new Issue(IssueTypes.ERROR_UPDATING_STUDY_PATIENT_STATUS, "Study Patient status could not be updated successfully")); 
				throw new OperationException();
			}
			patStudyStatBean.setCurrentStat("1");*/
			
			PatStudyStatDao ps = new PatStudyStatDao();
			 ps.getPatStatHistory(patientPK,studyPK);
			 ArrayList patStatPks = new ArrayList();
			 patStatPks = ps.getPatientStatusPk();
			 ArrayList currentStats = ps.getCurrentStats();
			 Integer currentStatusPK=null;
		     for (int i=0;i<patStatPks.size();i++) {
			    if (currentStats.get(i).toString().equals("1")) {
					  currentStatusPK = (Integer) patStatPks.get(i);
			     }
			 }
		     PatStudyStatBean currentPatStatBean = patStudyStatAgent.getPatStudyStatDetails(currentStatusPK);
		    		 currentPatStatBean.setCurrentStat("0");
				currentPatStatBean.setModifiedBy(StringUtil.integerToString(callingUser.getUserId()));
				if(patStudyStatAgent.updatePatStudyStat(currentPatStatBean)<0)
				{
					response.addIssue(new Issue(IssueTypes.ERROR_UPDATING_STUDY_PATIENT_STATUS, "Study Patient status could not be updated successfully")); 
					throw new OperationException();
				}
				patStudyStatBean.setCurrentStat("1");    
		}
		//Notes
		if(!StringUtil.isEmpty(patientEnrollmentDetails.getNotes()))
		{
			patStudyStatBean.setNotes(patientEnrollmentDetails.getNotes());
		}
		else 
		{
			if(patientEnrollmentDetails.getNotes()!=null)
			{
				if(isMandatory(configDetails, "notes"))
				{		response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_Notes_Mandatory));
						throw new OperationException();
				}
				else
				{
					patStudyStatBean.setNotes("");
				}
			}
		}
		//Setting Additional Information
		//Patient Study ID
		if(patientEnrollmentDetails.getPatientStudyId()!=null)
		{
			if(patientEnrollmentDetails.getPatientStudyId().trim().length()==0)
			{
				if(isMandatory(configDetails, "patstdid"))
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"Field patientStudyId may not be null"));
					throw new OperationException();
				}
				else
				{
					patProtBean.setPatStudyId("")	;
				}
			}
			else
			{
				if(!patientEnrollmentDetails.getPatientStudyId().equalsIgnoreCase(patProtBean.getPatStudyId()))
				{
					//if(patStudyStatAgent.getCountPatStudyId(studyPK, patientEnrollmentDetails.getPatientStudyId())>0){}
					patProtBean.setPatStudyId(patientEnrollmentDetails.getPatientStudyId())	;
				}
			}
		}
		//Enrolling Site
		if(patientEnrollmentDetails.getEnrollingSite()!=null)
		{
			if(StringUtil.isEmpty(patientEnrollmentDetails.getEnrollingSite().getOID())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getEnrollingSite().getSiteAltId())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getEnrollingSite().getSiteName()))
			{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_EnrollingSite_Mandatory)); 
					throw new OperationException();
			}
			Integer enrollingSite;
			try {
					enrollingSite = ObjectLocator.sitePKFromIdentifier(callingUser, patientEnrollmentDetails.getEnrollingSite(), sessionContext, objectMapService);
					if(enrollingSite != null && enrollingSite != 0)
					{
						PatFacilityDao patFacilityDao = patFacilityAgent.getPatientFacilitiesSiteIds(patientPK); 
						List siteToEnroll = patFacilityDao.getPatientSite(); 
						if(siteToEnroll.contains(StringUtil.integerToString(enrollingSite)))
						{
							patProtBean.setEnrollingSite(StringUtil.integerToString(enrollingSite)); 
						}else
						{
							response.addIssue(new Issue(IssueTypes.ORGANIZATION_AUTHORIZATION,"EnrollingSite is not in list of Study Organizations where user can enroll patient"));
							throw new OperationException(); 
						}
					}else
					{
						response.addIssue(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND,"EnrollingSite not found")); 
						throw new OperationException(); 
					}
				} catch (MultipleObjectsFoundException e) 
				{
					response.addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND,"Found multiple objects that matched Enrolling Site"));
					throw new OperationException();
				}
		}
		//Assigned To
		if(patientEnrollmentDetails.getAssignedTo()!=null)
		{
			if( (StringUtil.isEmpty(patientEnrollmentDetails.getAssignedTo().getFirstName())) &&
			        (StringUtil.isEmpty(patientEnrollmentDetails.getAssignedTo().getLastName())) &&
			        (StringUtil.isEmpty(patientEnrollmentDetails.getAssignedTo().getUserLoginName())) &&
			        (StringUtil.isEmpty(patientEnrollmentDetails.getAssignedTo().getOID())))
			{
				if(isMandatory(configDetails, "assignedto"))
				{
					response.addIssue(new Issue( IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_AssignedTo_Mandatory));
					throw new OperationException();
				}
				else
				{
					patProtBean.setAssignTo("");
				}
			}
			else
			{
				try
				{
					UserBean assignToBean = ObjectLocator.userBeanFromIdentifier(callingUser, patientEnrollmentDetails.getAssignedTo(), userAgent, sessionContext, objectMapService);
					if(assignToBean != null)
					{
						patProtBean.setAssignTo(StringUtil.integerToString(assignToBean.getUserId())); 
					}else
					{
						response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND ,"AssignedTo User Not found"));
						throw new OperationException();
					}
				}catch(MultipleObjectsFoundException e)
				{
					response.addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND,"Found multiple objects that matched Assigned to User"));
					throw new OperationException();
				}
			}
		}
		//Physician
		if(patientEnrollmentDetails.getPhysician()!=null)
		{
			if( (StringUtil.isEmpty(patientEnrollmentDetails.getPhysician().getFirstName())) &&
		        (StringUtil.isEmpty(patientEnrollmentDetails.getPhysician().getLastName())) &&
		        (StringUtil.isEmpty(patientEnrollmentDetails.getPhysician().getUserLoginName())) &&
		        (StringUtil.isEmpty(patientEnrollmentDetails.getPhysician().getOID())))
				{
					if(isMandatory(configDetails, "physician"))
					{
						response.addIssue(new Issue( IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_Physician_Mandatory));
						throw new OperationException();
					}
					else
					{
						patProtBean.setPatProtPhysician("");
					}
				}	
		    else
			{
				UserBean physicianBean;
				try {
						physicianBean = ObjectLocator.userBeanFromIdentifier(callingUser, patientEnrollmentDetails.getPhysician(), userAgent, sessionContext, objectMapService);
						if(physicianBean != null)
						{
							patProtBean.setPatProtPhysician(StringUtil.integerToString(physicianBean.getUserId())); 
						}else
						{
							response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND ,"Physician Not found"));
							throw new OperationException();
						}
					} 
					catch (MultipleObjectsFoundException e) 
					{
						response.addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND,"Found multiple objects that matched Physician"));
						throw new OperationException();
					}	
			}
		}
		//Treatment Location
		if(patientEnrollmentDetails.getTreatmentLocation()!=null)
		{
			if(StringUtil.isEmpty(patientEnrollmentDetails.getTreatmentLocation().getCode())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getTreatmentLocation().getDescription())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getTreatmentLocation().getType()))
			{
				if(isMandatory(configDetails, "treatmentlocation"))
				{
					response.addIssue(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_TreatmentLocation_Mandatory));
					throw new OperationException();
				}
				else
				{
					patProtBean.setTreatmentLoc("");
				}
			}
			else
			{
				try{
					 String treatmentlocation = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getTreatmentLocation(), CodeCache.CODE_TYPE_PATPROT_TREATMENT_LOCATION, callingUser); 
					 patProtBean.setTreatmentLoc(treatmentlocation); 
				}catch(CodeNotFoundException ce)
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"TreatmentLocation Code Not Found: " + patientEnrollmentDetails.getTreatmentLocation().getCode()));
					throw new OperationException();
				}
			}
		}
		// Treating Organization
		if(patientEnrollmentDetails.getTreatingOrganization()!=null)
		{
			if(StringUtil.isEmpty(patientEnrollmentDetails.getTreatingOrganization().getOID())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getTreatingOrganization().getSiteAltId())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getTreatingOrganization().getSiteName()))
			{
				if(isMandatory(configDetails,"treatingorg" ))
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_TreatingOrg_Mandatory)); 
				}
				else
				{
					patProtBean.setpatOrg("");
				}
			}
			else
			{
				try
				{
					Integer treatingOrganization = ObjectLocator.sitePKFromIdentifier(callingUser, patientEnrollmentDetails.getTreatingOrganization(), sessionContext, objectMapService);
					if(treatingOrganization != null && treatingOrganization != 0)
					{
						List<Integer> treatingOrg = new ArrayList<Integer>(); 			
						SiteAgentRObj siteAgentBean = (SiteAgentRObj) parameters.get("siteAgent"); 				 
						SiteDao siteDao = siteAgentBean.getByAccountId(StringUtil.stringToInteger(callingUser.getUserAccountId()));
						//TODO SiteAgentBean .get SiteValues(acctID);
						for( int j = 0; j < siteDao.getSiteIds().size(); j++)
						{
							treatingOrg.add(StringUtil.stringToInteger(siteDao.getSiteIds().get(j).toString())); 
						}
						if(treatingOrg.contains(treatingOrganization))
						{					
							patProtBean.setpatOrg(StringUtil.integerToString(treatingOrganization));
						}else
						{
							response.addIssue(new Issue(IssueTypes.ORGANIZATION_AUTHORIZATION,"TreatingOrganization is not in list of possible Treating Organizations"));
							throw new OperationException(); 
						}
					}else
					{
						response.addIssue(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND,"TreatingOrganization not found"));
						throw new OperationException(); 
					}
				} catch (MultipleObjectsFoundException e) {
					response.addIssue(
							new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND,"Found multiple objects that matched Treating Organization"));
					throw new OperationException();
				} 
			}
		}
		//Evaluable Status
		//Evaluable Flag
		if(patientEnrollmentDetails.getEvaluationFlag()!=null)
		{
			if(StringUtil.isEmpty(patientEnrollmentDetails.getEvaluationFlag().getCode())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getEvaluationFlag().getDescription())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getEvaluationFlag().getType()))
			{
				if(isMandatory(configDetails, "evaluableflag"))
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_EvaluationFlag_Mandatory));
					throw new OperationException();
				}
				else
				{
					patProtBean.setPtstEvalFlag("");
				}
			}
			else
			{
				try{
					String evaluationFlag = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getEvaluationFlag(),CodeCache.CODE_TYPE_PATPROT_EVALUATION_FLAG, callingUser); 
					if(evaluationFlag != null)
					{
						patProtBean.setPtstEvalFlag(evaluationFlag); 
					}else
					{
						throw new OperationException(); 
					}
				}catch(CodeNotFoundException ce)
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"EvaluationFlag Code Not Found: " + patientEnrollmentDetails.getEvaluationFlag().getCode()));
					throw new OperationException();
				}
			}
		}
		//Evaluable Status
		if(patientEnrollmentDetails.getEvaluationStatus()!=null)
		{
			if(StringUtil.isEmpty(patientEnrollmentDetails.getEvaluationStatus().getCode())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getEvaluationStatus().getDescription())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getEvaluationStatus().getType()))
			{
				if(isMandatory(configDetails, "evaluablestatus"))
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_EvaluationStatusCode_Mandatory));
					throw new OperationException();
				}
				else
				{
					patProtBean.setPtstEval("");
				}
			}
			else
			{
				String evaluationStatus = null; 
				try{
				evaluationStatus = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getEvaluationStatus(),CodeCache.CODE_TYPE_PATPROT_EVALATION_STATUS, callingUser); 
				}catch(CodeNotFoundException e)
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"EvaluationStatus Code Not Found: " + patientEnrollmentDetails.getEvaluationStatus().getCode()));
					throw new OperationException();
				}		
				patProtBean.setPtstEval(evaluationStatus); 
			}
		}
		//Unevaluable Status
		if(patientEnrollmentDetails.getInevaluationStatus()!=null)
		{
			if(StringUtil.isEmpty(patientEnrollmentDetails.getInevaluationStatus().getCode())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getInevaluationStatus().getDescription())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getInevaluationStatus().getType()))
			{
				if(isMandatory(configDetails, "inevaluablestatus"))
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_InevaluationStatusCode_Mandatory));
					throw new OperationException();
				}
				else
				{
					patProtBean.setPtstInEval("");
				}
			}
			else
			{
				try{
					String inEval = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getInevaluationStatus(),CodeCache.CODE_TYPE_PATPROT_INEVALATION_STATUS, callingUser);
					patProtBean.setPtstInEval(inEval); 
				}catch(CodeNotFoundException e){
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"InevaluationStatus Code Not Found: " + patientEnrollmentDetails.getInevaluationStatus().getCode()));
					throw new OperationException();
				}
			}
		}
		//Status dependent extra details
		//Enrollment Details - if status = enrolled
		if(statusType.equalsIgnoreCase("enrolled"))
		{
			if(patientEnrollmentDetails.getRandomizationNumber()!=null)
			{
				if(patientEnrollmentDetails.getRandomizationNumber().trim().length()==0)
				{
					if(isMandatory(configDetails, "randomnumber"))
					{
						response.addIssue(new Issue( IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_RandomNumber_Mandatory)); 
					}
					else
					{
						patProtBean.setPatProtRandomNumber("");						
					}
				}
				else
				{
					patProtBean.setPatProtRandomNumber(patientEnrollmentDetails.getRandomizationNumber());
				}
			}
			if(patientEnrollmentDetails.getEnrolledBy() != null)
			{
				if( (StringUtil.isEmpty(patientEnrollmentDetails.getEnrolledBy().getFirstName())) &&
				        (StringUtil.isEmpty(patientEnrollmentDetails.getEnrolledBy().getLastName())) &&
				        (StringUtil.isEmpty(patientEnrollmentDetails.getEnrolledBy().getUserLoginName())) &&
				        (StringUtil.isEmpty(patientEnrollmentDetails.getEnrolledBy().getOID())))
						{
							if(isMandatory(configDetails, "enrolledby"))
							{
								response.addIssue(new Issue( IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_EnrolledBy_Mandatory));
								throw new OperationException();
							}
							else
							{
								patProtBean.setPatProtUserId("");
							}
						}	
				    else
					{
				    	UserBean enrollingUserBean;
				    	try{
				    		enrollingUserBean = ObjectLocator.userBeanFromIdentifier(callingUser, patientEnrollmentDetails.getEnrolledBy(), userAgent, sessionContext, objectMapService);
				    		if(enrollingUserBean != null)
				    		{
				    			patProtBean.setPatProtUserId(StringUtil.integerToString(enrollingUserBean.getUserId())); 
				    		}else
				    		{
				    			response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND ,"EnrolledBy User Not found"));
				    			throw new OperationException();
				    		}
				    	} catch (MultipleObjectsFoundException e)
				    	{
				    		response.addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND,"Found multiple objects that matched Enrolling User"));
				    		throw new OperationException();
				    	} 
					}
			}
		}
		//Follow-up Details - if status = followup
		if(statusType.equalsIgnoreCase("followup"))
		{
			if(patientEnrollmentDetails.getNextFollowUpDate()!=null&&patientEnrollmentDetails.isEmptyNextFollowUpDate())
			{
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"emptyNextFollowUpDate field is set to true along with nextFollowUpDate"));
	    		throw new OperationException();
			}
			if(patientEnrollmentDetails.getNextFollowUpDate()!=null)
			{
				patStudyStatBean.setPtstNextFlwupPersistent(patientEnrollmentDetails.getNextFollowUpDate());
			}
			if(patientEnrollmentDetails.isEmptyNextFollowUpDate())
			{
				patStudyStatBean.setPtstNextFlwupPersistent(null);
			}
		}
		//Informed Consent Details - if status = infConsent
		if(statusType.equalsIgnoreCase("infConsent"))
		{
			if(patientEnrollmentDetails.getInformedConsentVersionNumber()!=null)
			{
				if(patientEnrollmentDetails.getInformedConsentVersionNumber().trim().length()==0)
				{
					if(isMandatory(configDetails, "icversionnumber"))
					{
						response.addIssue(new Issue( IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_ICVersionNumber_Mandatory)); 
					}
					else
					{
						//Bug#13940 Raman
						//patProtBean.setPtstConsentVer("");
						patStudyStatBean.setPtstConsentVer("");					
					}
				}
				else
				{
					//Bug#13940 Raman
					//patProtBean.setPtstConsentVer(patientEnrollmentDetails.getInformedConsentVersionNumber());
					patStudyStatBean.setPtstConsentVer(patientEnrollmentDetails.getInformedConsentVersionNumber());
				}
			}	
		}
		//Screening Details - if status = screening
		if(statusType.equalsIgnoreCase("screening"))
		{
			if(patientEnrollmentDetails.getScreenNumber()!=null)
			{
				if(patientEnrollmentDetails.getScreenNumber().trim().length()==0)
				{
					if(isMandatory(configDetails, "screennumber"))
					{
						response.addIssue(new Issue( IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_ScreenNumber_Mandatory)); 
						throw new OperationException();
					}
					else
					{
						patStudyStatBean.setScreenNumber("");						
					}
				}
				else
				{
					patStudyStatBean.setScreenNumber(patientEnrollmentDetails.getScreenNumber());
				}
			}
			if(patientEnrollmentDetails.getScreenedBy() != null)
			{
				if( (StringUtil.isEmpty(patientEnrollmentDetails.getScreenedBy().getFirstName())) &&
				        (StringUtil.isEmpty(patientEnrollmentDetails.getScreenedBy().getLastName())) &&
				        (StringUtil.isEmpty(patientEnrollmentDetails.getScreenedBy().getUserLoginName())) &&
				        (StringUtil.isEmpty(patientEnrollmentDetails.getScreenedBy().getOID())))
						{
							if(isMandatory(configDetails, "screenedby"))
							{
								response.addIssue(new Issue( IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_ScreenBy_Mandatory));
								throw new OperationException();
							}
							else
							{
								patStudyStatBean.setScreenedBy("");
							}
					}	
				else
				{
				    UserBean screeningUserBean; 			
			    	try
			    	{
			    		screeningUserBean = ObjectLocator.userBeanFromIdentifier(callingUser, patientEnrollmentDetails.getScreenedBy(), userAgent, sessionContext, objectMapService);
			    		if(screeningUserBean != null)
			    		{
			    			patStudyStatBean.setScreenedBy(StringUtil.integerToString(screeningUserBean.getUserId())); 
			    		}
			    		else
			    		{
			    			response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND,"ScreenBy User not found"));
			    			throw new OperationException();
			    		}
			    	}
			    	catch (MultipleObjectsFoundException e)
			    	{
			    		response.addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND,"Found multiple objects that matched ScreenedBy User"));
			    		throw new OperationException();
			    	}	 
				}
			}
			if(patientEnrollmentDetails.getScreeningOutcome() != null)
			{
				if(StringUtil.isEmpty(patientEnrollmentDetails.getScreeningOutcome().getCode())&&
						StringUtil.isEmpty(patientEnrollmentDetails.getScreeningOutcome().getDescription())&&
						StringUtil.isEmpty(patientEnrollmentDetails.getScreeningOutcome().getType()))
				{
					if(isMandatory(configDetails, "screeningoutcome"))
					{
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_ScreeningOutcome_Mandatory));
						throw new OperationException();
					}
					else
					{
						patStudyStatBean.setScreeningOutcome("");
					}
				}
				else
				{
					String screeningOutcome = null; 
					try{
						screeningOutcome = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getScreeningOutcome(),CodeCache.CODE_TYPE_PATPROT_SCREENING_OUTCOME, callingUser); 
					}catch(CodeNotFoundException e)
					{
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"ScreeningOutcome Code Not Found: " + patientEnrollmentDetails.getScreeningOutcome().getCode()));
						throw new OperationException();
					}
					patStudyStatBean.setScreeningOutcome(screeningOutcome);				
				}
			}
		}
		//Patient Status
		//Survival Status
		int  deadStatPk = codeDao.getCodeId("ptst_survival","dead");
		if(patientEnrollmentDetails.getSurvivalStatus()!=null)
		{
			if(StringUtil.isEmpty(patientEnrollmentDetails.getSurvivalStatus().getCode())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getSurvivalStatus().getDescription())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getSurvivalStatus().getType()))
			{
				if(isMandatory(configDetails, "survivalstatus"))
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_SurvivalStatus_Mandatory));
					throw new OperationException();
				}
				else
				{
					patProtBean.setPtstSurvival("");
				}
			}
			else
			{
				try{
					String survivalStatus=null; 
					try{
						survivalStatus = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getSurvivalStatus(), CodeCache.CODE_TYPE_PATPROT_SURVIVAL_STATUS, callingUser);
					}
					catch (CodeNotFoundException e) {
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"survivalStatus Code Not Found: " + patientEnrollmentDetails.getSurvivalStatus().getCode()));
						throw new OperationException();
					}
					String prevSurvivalStatus=patProtBean.getPtstSurvival();
					if(StringUtil.stringToNum(prevSurvivalStatus)!= StringUtil.stringToNum(survivalStatus))
					{
						patProtBean.setPtstSurvival(survivalStatus);
						if(patientEnrollmentDetails.getSurvivalStatus().getCode().equals("dead"))
						{
							if(patientEnrollmentDetails.getDateOfDeath()==null)
							{
								response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"deathDate field may not be null when survival status provided as dead"));
								throw new OperationException();
							}
							if(patientEnrollmentDetails.isEmptyDeathDate())
							{
								response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"emptyDeathDate field may not be set to True when survival status provided as dead"));
								throw new OperationException();
							}
							if((StringUtil.stringToInteger(survivalStatus)==deadStatPk))
							{
								personB.setPersonStatus(String.valueOf(codeDao.getCodeId("patient_status","dead")));
								isPersonBUpdated=true;
							}
						}
						else
						{
							if(StringUtil.stringToNum(prevSurvivalStatus)==deadStatPk)
							{
								//checking death fields for !null
								//deathDate
								if(patientEnrollmentDetails.getDateOfDeath()!=null)
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"deathDate field must be null when survival status provided as "+patientEnrollmentDetails.getSurvivalStatus().getCode()));
									throw new OperationException();
								}
								if(patProtBean.getPtstDeathDatePersistent()!=null&&!patientEnrollmentDetails.isEmptyDeathDate())
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"survivalStatus may not be set as "+patientEnrollmentDetails.getSurvivalStatus().getCode()+" because Death Date is already provided"));
									throw new OperationException();
								}
								//CauseOfDeath
								if(patientEnrollmentDetails.getCauseOfDeath()!=null && (
										!StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getCode())||
										!StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getDescription())||
										!StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getType())
										))
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"causeOfDeath field must be null when survival status provided as "+patientEnrollmentDetails.getSurvivalStatus().getCode()));
									throw new OperationException();
								}
								if(!StringUtil.isEmpty(personB.getPatDthCause())
										&&(patientEnrollmentDetails.getCauseOfDeath()==null ||(
												!StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getCode())||
												!StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getDescription())||
												!StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getType())
												)
											))
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"survivalStatus may not be set as "+patientEnrollmentDetails.getSurvivalStatus().getCode()+" because cause of death is already provided"));
									throw new OperationException();
								}
								//Specify Cause
								if(patientEnrollmentDetails.getSpecifyCause()!=null && patientEnrollmentDetails.getSpecifyCause().trim().length()!=0)
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"specifyCause field must be null when survival status provided as "+patientEnrollmentDetails.getSurvivalStatus().getCode()));
									throw new OperationException();
								}
								if(!StringUtil.isEmpty(personB.getDthCauseOther())&& (patientEnrollmentDetails.getSpecifyCause()==null||patientEnrollmentDetails.getSpecifyCause().trim().length()!=0))
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"survivalStatus may not be set as "+patientEnrollmentDetails.getSurvivalStatus().getCode()+" because specifyCause is already provided"));
									throw new OperationException();
								}
								//Death Related to Study
								if(patientEnrollmentDetails.getDeathRelatedToTrial()!=null && (
										!StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getCode())||
										!StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getDescription())||
										!StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getType())
										))
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"deathRelatedToTrial field must be null when survival status provided as "+patientEnrollmentDetails.getSurvivalStatus().getCode()));
									throw new OperationException();
								}
								if(!StringUtil.isEmpty(patProtBean.getPtstDthStdRel())
										&&(patientEnrollmentDetails.getDeathRelatedToTrial()==null ||(
												!StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getCode())||
												!StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getDescription())||
												!StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getType())
												)
											))
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"survivalStatus may not be set as "+patientEnrollmentDetails.getSurvivalStatus().getCode()+" because deathRelatedToTrial is already provided"));
									throw new OperationException();
								}
								//Reason of Death Related to Study
								if(patientEnrollmentDetails.getReasonOfDeathRelatedToTrial()!=null)
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"raesonOfDeathRelatedToTrial field must be null when survival status provided as "+patientEnrollmentDetails.getSurvivalStatus().getCode()));
									throw new OperationException();
								}
								if(!StringUtil.isEmpty(patProtBean.getPtstDthStdRelOther())&& (patientEnrollmentDetails.getReasonOfDeathRelatedToTrial()==null||patientEnrollmentDetails.getReasonOfDeathRelatedToTrial().trim().length()!=0))
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"survivalStatus may not be set as "+patientEnrollmentDetails.getSurvivalStatus().getCode()+" because raesonOfDeathRelatedToTrial is already provided"));
									throw new OperationException();
								}
								personB.setPersonStatus(String.valueOf(codeDao.getCodeId("patient_status","A")));
								isPersonBUpdated=true;
							}
						}
					}
					else
					{
						//status provided is already person's current survival status.Do nothing.
					}
				}catch(CodeNotFoundException ce)
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"Survival Status Code Not Found: " + patientEnrollmentDetails.getSurvivalStatus().getCode()));
					throw new OperationException();
				}
			}
		}
		//Death Date
		if(patientEnrollmentDetails.getDateOfDeath()!=null)
		{
			if(StringUtil.stringToNum(patProtBean.getPtstSurvival())!=deadStatPk)
			{
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"dateOfDeath field may not be provided along with person survivalStatus as " +codeDao.getCodeSubtype(StringUtil.stringToNum(patProtBean.getPtstSurvival())) ));
				throw new OperationException();
			}
			if (patientEnrollmentDetails.getDateOfDeath().before(personB.getPersonDb()))
		    {
		        response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "DeathDate cannot be before Date of Birth"));
		        throw new OperationException();
		    }
	    	Date date = new Date();
	    	if (patientEnrollmentDetails.getDateOfDeath().after(date))
	    	{
	    	      response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Date of death should not be after present day"));
	    	      throw new OperationException();
	    	}
			patProtBean.setPtstDeathDatePersistent(patientEnrollmentDetails.getDateOfDeath());
			personB.setPersonDeathDate(DateUtil.dateToString(patientEnrollmentDetails.getDateOfDeath()));
			isPersonBUpdated=true;
		}
		//emptyDeathDate
		if(patientEnrollmentDetails.isEmptyDeathDate())
		{
			if(StringUtil.stringToNum(patProtBean.getPtstSurvival())==deadStatPk)
			{
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"emptyDeathDate field may not be set to true along with person survivalStatus as " +codeDao.getCodeSubtype(StringUtil.stringToNum(patProtBean.getPtstSurvival())) ));
				throw new OperationException();
			}
			if(patientEnrollmentDetails.getDateOfDeath()!=null)
			{
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"emptyDeathDate field may not be set to true along with person dateOfDeath provided" ));
				throw new OperationException();
			}
			patProtBean.setPtstDeathDatePersistent(null);
			personB.setPersonDeathDate(null);
			isPersonBUpdated=true;
		}
		//Cause of death
		if(patientEnrollmentDetails.getCauseOfDeath()!=null)
		{
			if(StringUtil.stringToNum(patProtBean.getPtstSurvival())!=deadStatPk&&(
					!StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getCode())||
					!StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getDescription())||
					!StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getType())
					)
				)
			{
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"causeOfDeath field may not be provided along with person survivalStatus as " +codeDao.getCodeSubtype(StringUtil.stringToNum(patProtBean.getPtstSurvival())) ));
				throw new OperationException();
			}
			if(StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getCode())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getDescription())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getCauseOfDeath().getType()))
			{
				if(isMandatory(configDetails, "cod"))
				{
					response.addIssue(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_CauseOfDeath_Mandatory));
					throw new OperationException();
				}
				else
				{
					personB.setPatDthCause("");
					isPersonBUpdated=true;
				}
			}
			else
			{
				try{
					personB.setPatDthCause(AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getCauseOfDeath(), CodeCache.CODE_TYPE_PATIENT_DEATH_CAUSE, callingUser));
				}
				catch (CodeNotFoundException e) {
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"causeOfDeath Code Not Found: " + patientEnrollmentDetails.getCauseOfDeath().getCode()));
					throw new OperationException();
				}
				isPersonBUpdated=true;
			}
		}
		//specify cause
		if(patientEnrollmentDetails.getSpecifyCause()!=null)
		{
			if(StringUtil.stringToNum(patProtBean.getPtstSurvival())!=deadStatPk && patientEnrollmentDetails.getSpecifyCause().trim().length()!=0)
			{
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"specifyCause field may not be provided along with person survivalStatus as " +codeDao.getCodeSubtype(StringUtil.stringToNum(patProtBean.getPtstSurvival())) ));
				throw new OperationException();
			}
			if(patientEnrollmentDetails.getSpecifyCause().trim().length()==0)
			{
				if(isMandatory(configDetails, "specifycause"))
				{
					response.addIssue(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_SpecifyCause_Mandatory)); 
				}
				else
				{
					personB.setDthCauseOther("");	
					isPersonBUpdated=true;
				}
			}
			else
			{
				personB.setDthCauseOther(patientEnrollmentDetails.getSpecifyCause());
				isPersonBUpdated=true;
			}
		}
		//Death related to study
		if(patientEnrollmentDetails.getDeathRelatedToTrial()!=null)
		{
			if(StringUtil.stringToNum(patProtBean.getPtstSurvival())!=deadStatPk&&(
												!StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getCode())||
												!StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getDescription())||
												!StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getType())
												))
			{
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"deathRelatedToTrial field may not be provided along with person survivalStatus as " +codeDao.getCodeSubtype(StringUtil.stringToNum(patProtBean.getPtstSurvival())) ));
				throw new OperationException();
			}
			
			if(StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getCode())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getDescription())&&
					StringUtil.isEmpty(patientEnrollmentDetails.getDeathRelatedToTrial().getType()))
			{
				if(isMandatory(configDetails, "deathrelated"))
				{
					response.addIssue(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_DeathRelatedToTrial_Mandatory));
					throw new OperationException();
				}
				else
				{
					patProtBean.setPtstDthStdRel("");
				}
			}
			else
			{
				try{
					patProtBean.setPtstDthStdRel(AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getDeathRelatedToTrial(), CodeCache.CODE_TYPE_PATPROT_STUDY_RELATED_DEATH, callingUser));
				}
				catch (CodeNotFoundException e) {
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"deathRelatedToTrial Code Not Found: " + patientEnrollmentDetails.getDeathRelatedToTrial().getCode()));
					throw new OperationException();
				}
			}
		}
		//Reason of Death Related to Study
		if(patientEnrollmentDetails.getReasonOfDeathRelatedToTrial()!=null)
		{
			if(StringUtil.stringToNum(patProtBean.getPtstSurvival())!=deadStatPk&&patientEnrollmentDetails.getReasonOfDeathRelatedToTrial().trim().length()!=0)
			{
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"reasonOfDeathRelatedToTrial field may not be provided along with person survivalStatus as " +codeDao.getCodeSubtype(StringUtil.stringToNum(patProtBean.getPtstSurvival())) ));
				throw new OperationException();
			}
			
			
			if(patientEnrollmentDetails.getReasonOfDeathRelatedToTrial().trim().length()==0)
			{
				if(isMandatory(configDetails, "reasonofdeathrel"))
				{
					response.addIssue(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_DeathRelatedToTrial_Mandatory)); 
					throw new OperationException();
				}
				else
				{
					patProtBean.setPtstDthStdRelOther("");					
				}
			}
			else
			{
				patProtBean.setPtstDthStdRelOther(patientEnrollmentDetails.getReasonOfDeathRelatedToTrial());
			}
		}
		
		int ridPat=0,latestRaid=0;
		AuditRowEpatAgent auditRowEpatAgent=(AuditRowEpatAgent) parameters.get("auditRowEpatAgent");
		ridPat = auditRowEpatAgent.getRID(personB.personPKId, "PERSON", "PK_PERSON");
		latestRaid=auditRowEpatAgent.findLatestRaid(ridPat);
		personB.setModifiedBy(StringUtil.integerToString(callingUser.getUserId()));
		personB.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE);
		if(personAgent.updatePerson(personB)!=0)
		{
			response.addIssue(new Issue(IssueTypes.ERROR_UPDATING_STUDY_PATIENT_STATUS,"Error in updating Study Patient Status"));
			throw new OperationException();
		}
    	patStudyStatBean.setPatProtObj(patProtBean);
		if(patStudyStatAgent.updatePatStudyStat(patStudyStatBean)==0)
		{
			if(StringUtil.isEmpty(patientEnrollmentDetails.getReasonForChange()))
			{
				StudyBean sb=studyAgent.getStudyDetails(studyPK.intValue());
				if("1".equalsIgnoreCase(sb.getFdaRegulatedStudy()))
				{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"reasonForChange field may not be null when study is marked as FDA regulated"));
					throw new OperationException();
				}
			}
			else
			{
				AuditRowEresAgent auditRowEresAgnet=(AuditRowEresAgent) parameters.get("auditRowEresAgnet");
				int ridERes =auditRowEresAgnet.getRID(patStudyStatBean.getId(), "ER_PATSTUDYSTAT", "PK_PATSTUDYSTAT");
				int raid = auditRowEresAgnet.findLatestRaid(ridERes, callingUser.getUserId().intValue());
				int ret=auditRowEresAgnet.setReasonForChange(raid, patientEnrollmentDetails.getReasonForChange());
				if (!(raid > 0)||(ret!=1)){
					response.addIssue(new Issue(IssueTypes.ERROR_UPDATING_STUDY_PATIENT_STATUS,"Error in updating Study Patient Status"));
					throw new OperationException();
				}
				AuditRowEpatDao auditDao = new AuditRowEpatDao();
				ArrayList listRaids = auditDao.getAllLatestRaids(ridPat, callingUser.getUserId().intValue(), latestRaid);
		        System.out.println("listRaid size is "+listRaids.size());
		        if (null != listRaids){
			        for (int i=0; i<listRaids.size();i++){
			        	raid = StringUtil.stringToNum(""+listRaids.get(i));
			        	
			        	if(raid>0)
			        	{
			        		((AuditRowEpatAgent)parameters.get("auditRowEpatAgent")).setReasonForChange(raid, patientEnrollmentDetails.getReasonForChange());
			        	}
			        }
		        }
			}
		}
		
		
		
		
	}


	public String[] createMStudyPatientStatus(
			List<Map<String, Object>> patDataList,
			Map<String, Object> parameters) throws OperationException
	{
		// TODO Auto-generated method stub
		PersonAgentRObj personAgent = (PersonAgentRObj) parameters.get("personAgent");
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("objectMapService");
		UserAgentRObj userAgent = (UserAgentRObj) parameters.get("userAgent"); 
		SessionContext sessionContext  = (SessionContext) parameters.get("sessionContext"); 
		ResponseHolder responseHolder = (ResponseHolder) parameters.get("ResponseHolder");
		UserBean callingUser = (UserBean) parameters.get("callingUser"); 
		SimpleDateFormat sf=new SimpleDateFormat("MM/dd/yyyy");
		Integer patientPK=null;
		Integer studyPK=null;
		PatStudyStatBean patStudyStatbean =null;
		Date patientDOB=null;
		String patientCode = null;
		PatientEnrollmentDetails patientEnrollmentDetails=null;
		PersonBean personB=null;
		PatProtBean patProtBean=null;
		StringBuffer patStudyStatsb = null;
		StringBuffer patProtsb = null;
		boolean commaChkPatProt=false;
		StringBuffer personsb = null;
		
		String PK_PATSTUDYSTAT = null;
		String FK_CODELST_STAT = null;
		String FK_PER = null;
		String FK_STUDY = null;
		String PATSTUDYSTAT_DATE = null;
		String PATSTUDYSTAT_ENDT = null;
		String PATSTUDYSTAT_NOTE = null;
		String RID = null;
		String CREATOR = null;
		String LAST_MODIFIED_BY = null;
		String LAST_MODIFIED_DATE = null;
		String CREATED_ON = null;
		String IP_ADD = null;
		String PATSTUDYSTAT_REASON = null;
		String SCREEN_NUMBER = null;
		String SCREENED_BY = null;
		String SCREENING_OUTCOME = null;
		String NEXT_FOLLOWUP_ON = null;
		String INFORM_CONSENT_VER = null;
		String CURRENT_STAT = null;
		List<String> personList=new ArrayList<String>();
		List<String> patProtList=new ArrayList<String>();
		List<String> patStudyStatList=new ArrayList<String>();
		List<String> fkList=new ArrayList<String>();
		for(Map<String,Object> patientData:patDataList )
		{
			PK_PATSTUDYSTAT = null;
			FK_CODELST_STAT = null;
			FK_PER = null;
			FK_STUDY = null;
			PATSTUDYSTAT_DATE = null;
			PATSTUDYSTAT_ENDT = null;
			PATSTUDYSTAT_NOTE = null;
			RID = null;
			CREATOR = null;
			LAST_MODIFIED_BY = null;
			LAST_MODIFIED_DATE = null;
			CREATED_ON = null;
			IP_ADD = null;
			PATSTUDYSTAT_REASON = null;
			SCREEN_NUMBER = null;
			SCREENED_BY = null;
			SCREENING_OUTCOME = null;
			NEXT_FOLLOWUP_ON = null;
			INFORM_CONSENT_VER = null;
			CURRENT_STAT = null;

			
			
			
			patientPK=null;
			patientPK=(Integer) patientData.get("patientPK");
			
			studyPK=null;
			studyPK=(Integer) patientData.get("studyPK");
			
			personB=null;
			personB = personAgent.getPersonDetails(patientPK); 
			
			patStudyStatbean=null;
			patStudyStatbean = new PatStudyStatBean();
			
			patientDOB=null;
			patientDOB = (Date) patientData.get("patientDOB"); 
			
			patientCode = null;
			patientCode = patientData.get("patientCode").toString();
			
			patientEnrollmentDetails=null;
			patientEnrollmentDetails=(PatientEnrollmentDetails) patientData.get("patientEnrollmentDetails");
			
			if(patientEnrollmentDetails.getPatientStudyId() == null || patientEnrollmentDetails.getPatientStudyId().length() == 0)
			{
				//patientEnrollmentDetails.setPatientStudyId(patientCode);
				//Bug #: 15428 Raman
				responseHolder.addIssue(
						new Issue(
								IssueTypes.DATA_VALIDATION ,
						"patientStudyId is a mandatory field."));
				//throw new OperationException();
				continue;
			}
			try{
				validatePatientEnrollmentDetails(patientEnrollmentDetails, callingUser, parameters, patientDOB); 
			}
			catch(ValidationException e)
			{
				System.out.println(((ResponseHolder) parameters.get("ResponseHolder"))
						.getIssues().getIssue().get(0));
				continue;
			}
			
			patProtBean=null;
			patProtBean=(PatProtBean) patientData.get("patProtBean");
			//PatProt data
			patProtBean.setPatProtPersonId(StringUtil.integerToString(patientPK));
			patProtBean.setPatProtStudyId(StringUtil.integerToString(studyPK)); 
			
			
			patStudyStatsb = new StringBuffer();
			patProtsb = new StringBuffer();
			personsb = new StringBuffer();
			
			patProtsb.append("UPDATE ERES.ER_PATPROT SET ");
			personsb.append("UPDATE EPAT.PERSON SET ");
			
			
			
			
			
			
			//Enrollment Details - if status = enrolled
			if(patientEnrollmentDetails.getStatus().getCode().equals("enrolled"))
			{
				//patProtBean.setPatProtRandomNumber(patientEnrollmentDetails.getRandomizationNumber());
				
				if(patientEnrollmentDetails.getRandomizationNumber()==null)
				{
					patProtsb.append(" PATPROT_RANDOM ='', ");
				}else
				{
					patProtsb.append(" PATPROT_RANDOM ='"+patientEnrollmentDetails.getRandomizationNumber()+"', ");
				}
				//patProtBean.setPatProtEnrolDt(patientEnrollmentDetails.getStatusDate()); 
				patProtsb.append(" PATPROT_ENROLDT=to_date('"+sf.format(patientEnrollmentDetails.getStatusDate())+"','MM/dd/yyyy'), ");

				if(patientEnrollmentDetails.getEnrolledBy() != null)
				{
					UserBean enrollingUserBean; 			
					try{
						enrollingUserBean = ObjectLocator.userBeanFromIdentifier(callingUser, patientEnrollmentDetails.getEnrolledBy(), userAgent, sessionContext, objectMapService);
						if(enrollingUserBean != null)
						{
							//patProtBean.setPatProtUserId(StringUtil.integerToString(enrollingUserBean.getUserId())); 
							patProtsb.append(" FK_USER='"+StringUtil.integerToString(enrollingUserBean.getUserId())+"', ");
						}else
						{
							responseHolder.addIssue(
									new Issue(
											IssueTypes.USER_NOT_FOUND ,
									"EnrolledBy User Not found"));
							//throw new OperationException();
							continue;
						}
					} catch (MultipleObjectsFoundException e) {

						responseHolder.addIssue(
								new Issue(
										IssueTypes.MULTIPLE_OBJECTS_FOUND, 
								"Found multiple objects that matched Enrolling User"));
						//throw new OperationException();
						continue;
					} 

				}
			}
			
			//Informed Consent Details
			if(patientEnrollmentDetails.getStatus().getCode().equals("infConsent"))
			{
				 //patStudyStatbean.setPtstConsentVer(patientEnrollmentDetails.getInformedConsentVersionNumber()) ; 	
				 if(patientEnrollmentDetails.getInformedConsentVersionNumber()!=null){
					 INFORM_CONSENT_VER=" '"+patientEnrollmentDetails.getInformedConsentVersionNumber()+"' ";
				 }
			}

			//Follow-up Details
			if(patientEnrollmentDetails.getStatus().getCode().equals("followup"))
			{
				//patStudyStatbean.setPtstNextFlwupPersistent(patientEnrollmentDetails.getNextFollowUpDate());
				if(patientEnrollmentDetails.getNextFollowUpDate()!=null){
					NEXT_FOLLOWUP_ON=" to_date('"+sf.format(patientEnrollmentDetails.getNextFollowUpDate())+"','MM/dd/yyyy')) ";   
				}
			}
			
			//Screening Details
			if(patientEnrollmentDetails.getStatus().getCode().equals("screening"))
			{
				//patStudyStatbean.setScreenNumber(patientEnrollmentDetails.getScreenNumber());
				SCREEN_NUMBER=" '"+patientEnrollmentDetails.getScreenNumber()+"' ";
				
				if(patientEnrollmentDetails.getScreenedBy() != null)
				{
					UserBean screeningUserBean; 			
					try{
						screeningUserBean = ObjectLocator.userBeanFromIdentifier(callingUser, patientEnrollmentDetails.getScreenedBy(), userAgent, sessionContext, objectMapService);
						if(screeningUserBean != null)
						{
							//patStudyStatbean.setScreenedBy(StringUtil.integerToString(screeningUserBean.getUserId()));
							SCREENED_BY=" '"+StringUtil.integerToString(screeningUserBean.getUserId())+"' ";
						}else
						{
							responseHolder.addIssue(
									new Issue(
											IssueTypes.USER_NOT_FOUND, 
									"ScreenBy User not found"));
							//throw new OperationException();
							continue;
						}
					} catch (MultipleObjectsFoundException e) {

						responseHolder.addIssue(
								new Issue(
										IssueTypes.MULTIPLE_OBJECTS_FOUND, 
								"Found multiple objects that matched ScreenedBy User"));
						//throw new OperationException();
						continue;
					} 

				}
				
				
				if(patientEnrollmentDetails.getScreeningOutcome() != null)
				{
					String screeningOutcome = null; 
					try{
						screeningOutcome = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getScreeningOutcome(),
							CodeCache.CODE_TYPE_PATPROT_SCREENING_OUTCOME, callingUser); 
					
					}catch(CodeNotFoundException e)
					{
						responseHolder.addIssue(
								new Issue(
									IssueTypes.DATA_VALIDATION, 
									"ScreeningOutcome Code Not Found: " + patientEnrollmentDetails.getScreeningOutcome().getCode()));
						//throw new OperationException();
						continue;
					}
					
					//patStudyStatbean.setScreeningOutcome(screeningOutcome);	
					SCREENING_OUTCOME=" '"+screeningOutcome+"' ";
				}
			
				
			}


			//additional information
			String patientStudyId = patientEnrollmentDetails.getPatientStudyId(); 
			//Bug #: 15428 Raman
			//if(patientStudyId == null || patientStudyId.length() == 0) patientStudyId = patientCode; 
			//patProtBean.setPatStudyId(patientStudyId); 
			patProtsb.append(" patprot_patstdid='"+patientStudyId+"', ");
			
			
			if(patientEnrollmentDetails.getEnrollingSite() != null)
			{
				// Get Enrolling Sites for this Study
				Integer enrollingSite;
				List<Integer> siteToEnroll = (List<Integer>) patientData.get("patientOrganizations"); 
				try {
					enrollingSite = ObjectLocator.sitePKFromIdentifier(callingUser, patientEnrollmentDetails.getEnrollingSite(), sessionContext, objectMapService);
					
					if(enrollingSite != null && enrollingSite != 0)
					{
						if(siteToEnroll.contains(enrollingSite))
						{
							//patProtBean.setEnrollingSite(StringUtil.integerToString(enrollingSite)); 
							patProtsb.append(" FK_SITE_ENROLLING='"+StringUtil.integerToString(enrollingSite)+"', ");
						}else
						{
							responseHolder.addIssue(
									new Issue(
											IssueTypes.ORGANIZATION_AUTHORIZATION, 
									"EnrollingSite is not in list of Study Organizations where user can enroll patient"));
							//throw new OperationException();
							continue; 
						}
					}else
					{
						responseHolder.addIssue(
								new Issue(
										IssueTypes.ORGANIZATION_NOT_FOUND,
										"EnrollingSite not found")); 
						//throw new OperationException();
						continue; 
						
					}
				} catch (MultipleObjectsFoundException e) {
					responseHolder.addIssue(
							new Issue(
									IssueTypes.MULTIPLE_OBJECTS_FOUND, 
									"Found multiple objects that matched Enrolling Site"));
					//throw new OperationException();
					continue;
				} 
				
			}
			
			if(patientEnrollmentDetails.getAssignedTo() != null )
			{
				//Bug 15432 Raman
				if((StringUtil.isEmpty(patientEnrollmentDetails.getAssignedTo().getOID())) && (StringUtil.isEmpty(patientEnrollmentDetails.getAssignedTo().getUserLoginName())) && (patientEnrollmentDetails.getAssignedTo().getPK() == null || patientEnrollmentDetails.getAssignedTo().getPK() == 0)
						&& (StringUtil.isEmpty(patientEnrollmentDetails.getAssignedTo().getFirstName()) && 
								StringUtil.isEmpty(patientEnrollmentDetails.getAssignedTo().getLastName())))
				{
					patProtsb.append(" FK_USERASSTO='', ");
				}
				else
				{
					try{
						UserBean assignToBean = ObjectLocator.userBeanFromIdentifier(callingUser, patientEnrollmentDetails.getAssignedTo(), userAgent, sessionContext, objectMapService);
						if(assignToBean != null)
						{
							//patProtBean.setAssignTo(StringUtil.integerToString(assignToBean.getUserId())); 
							patProtsb.append(" FK_USERASSTO='"+StringUtil.integerToString(assignToBean.getUserId())+"', ");
						}
						
						else
						{
							responseHolder.addIssue(
									new Issue(
											IssueTypes.USER_NOT_FOUND ,
									"AssignedTo User Not found"));
							//throw new OperationException();
							continue;
						}
						}catch(MultipleObjectsFoundException e)
						{
							responseHolder.addIssue(
									new Issue(
											IssueTypes.MULTIPLE_OBJECTS_FOUND, 
											"Found multiple objects that matched Assigned to User"));
							//throw new OperationException();
							continue;
						}
				}
				
				
			}
			
			
			if(patientEnrollmentDetails.getPhysician() != null)
			{
				//Bug 15433 Raman
				if((StringUtil.isEmpty(patientEnrollmentDetails.getPhysician().getOID())) && (StringUtil.isEmpty(patientEnrollmentDetails.getPhysician().getUserLoginName())) && (patientEnrollmentDetails.getPhysician().getPK() == null || patientEnrollmentDetails.getPhysician().getPK() == 0)
						&& (StringUtil.isEmpty(patientEnrollmentDetails.getPhysician().getFirstName()) && 
								StringUtil.isEmpty(patientEnrollmentDetails.getPhysician().getLastName())))
				{
					patProtsb.append(" PATPROT_PHYSICIAN='', ");
				}
				else
				{
					UserBean physicianBean;
					try {
						physicianBean = ObjectLocator.userBeanFromIdentifier(callingUser, patientEnrollmentDetails.getPhysician(), userAgent, sessionContext, objectMapService);
						if(physicianBean != null)
						{
							//patProtBean.setPatProtPhysician(StringUtil.integerToString(physicianBean.getUserId())); 
							patProtsb.append(" PATPROT_PHYSICIAN='"+StringUtil.integerToString(physicianBean.getUserId())+"', ");
						}else
						{
							responseHolder.addIssue(
									new Issue(
											IssueTypes.USER_NOT_FOUND ,
									"Physician Not found"));
							//throw new OperationException();
							continue;
						}
					
					} catch (MultipleObjectsFoundException e) {
						responseHolder.addIssue(
								new Issue(
										IssueTypes.MULTIPLE_OBJECTS_FOUND, 
										"Found multiple objects that matched Physician"));
						//throw new OperationException();
						continue;
					}
				}
				
			}
			
			if(patientEnrollmentDetails.getTreatmentLocation() != null )
			{
				try{
					 String treatmentlocation = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getTreatmentLocation(), CodeCache.CODE_TYPE_PATPROT_TREATMENT_LOCATION, callingUser); 
					 //patProtBean.setTreatmentLoc(treatmentlocation); 
					 patProtsb.append(" FK_CODELSTLOC='"+treatmentlocation+"', ");
				}catch(CodeNotFoundException ce)
				{
					responseHolder.addIssue(
							new Issue(
									IssueTypes.DATA_VALIDATION, 
									"TreatmentLocation Code Not Found: " + patientEnrollmentDetails.getTreatmentLocation().getCode()));
					//throw new OperationException();
					continue;
				}
			}
			
			if(patientEnrollmentDetails.getTreatingOrganization() != null)
			{
				if(StringUtil.isEmpty(patientEnrollmentDetails.getTreatingOrganization().getOID()) && 
						StringUtil.isEmpty(patientEnrollmentDetails.getTreatingOrganization().getSiteAltId()) &&
						StringUtil.isEmpty(patientEnrollmentDetails.getTreatingOrganization().getSiteName()) &&
						(patientEnrollmentDetails.getTreatingOrganization().getPK()==null || patientEnrollmentDetails.getTreatingOrganization().getPK()<=0) )
				{
					patProtsb.append(" PATPROT_TREATINGORG='', ");
				}
				else
				{
					try {
						Integer treatingOrganization = ObjectLocator.sitePKFromIdentifier(callingUser, patientEnrollmentDetails.getTreatingOrganization(), sessionContext, objectMapService);
						if(treatingOrganization != null && treatingOrganization != 0)
						{
							List<Integer> treatingOrg = new ArrayList<Integer>(); 			
							SiteAgentRObj siteAgentBean = (SiteAgentRObj) parameters.get("siteAgent"); 				 
							SiteDao siteDao = siteAgentBean.getByAccountId(StringUtil.stringToInteger(callingUser.getUserAccountId()));
							//TODO SiteAgentBean .get SiteValues(acctID); 
	
							for( int j = 0; j < siteDao.getSiteIds().size(); j++)
							{
								treatingOrg.add(StringUtil.stringToInteger(siteDao.getSiteIds().get(j).toString())); 
							}
							
							if(treatingOrg.contains(treatingOrganization))
							{					
								//patProtBean.setpatOrg(StringUtil.integerToString(treatingOrganization));
								patProtsb.append(" PATPROT_TREATINGORG='"+StringUtil.integerToString(treatingOrganization)+"', ");
							}else
							{
								responseHolder.addIssue(
										new Issue(
												IssueTypes.ORGANIZATION_AUTHORIZATION, 
										"TreatingOrganization is not in list of possible Treating Organizations"));
								//throw new OperationException();
								continue; 
							}
						}else
						{
							responseHolder.addIssue(
									new Issue(
											IssueTypes.ORGANIZATION_NOT_FOUND, 
									"TreatingOrganization not found"));
							//throw new OperationException();
							continue; 
						}
						 
						
						
						
					} catch (MultipleObjectsFoundException e) {
						responseHolder.addIssue(
								new Issue(
										IssueTypes.MULTIPLE_OBJECTS_FOUND, 
										"Found multiple objects that matched Treating Organization"));
						//throw new OperationException();
						continue;
					} 
				}
			}
			
			
			//Evaluable Status
			
			if(patientEnrollmentDetails.getDateOfDeath() != null)
			{
				//patProtBean.setPtstDeathDatePersistent(patientEnrollmentDetails.getDateOfDeath());
				patProtsb.append(" DATE_OF_DEATH=to_date('"+sf.format(patientEnrollmentDetails.getDateOfDeath())+"','MM/dd/yyyy'),");
			}

			//patProtBean.setPatProtStudyId(StringUtil.integerToString(studyPK));
			patProtsb.append(" FK_STUDY='"+StringUtil.integerToString(studyPK)+"', ");
			if(patientEnrollmentDetails.getEvaluationFlag() != null )
			{
				if(StringUtil.isEmpty(patientEnrollmentDetails.getEvaluationFlag().getCode()))
				{
					patProtsb.append(" FK_CODELST_PTST_EVAL_FLAG='', ");
				}
				else
				{
					try{
						String evaluationFlag = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getEvaluationFlag(),
								CodeCache.CODE_TYPE_PATPROT_EVALUATION_FLAG, callingUser); 
						if(evaluationFlag != null)
						{
							//patProtBean.setPtstEvalFlag(evaluationFlag); 
							patProtsb.append(" FK_CODELST_PTST_EVAL_FLAG='"+evaluationFlag+"', ");
						}else
						{
							//throw new OperationException();
							continue; 
						}
					}catch(CodeNotFoundException ce)
					{
						responseHolder.addIssue(
								new Issue(
									IssueTypes.DATA_VALIDATION, 
									"EvaluationFlag Code Not Found: " + patientEnrollmentDetails.getEvaluationFlag().getCode()));
						//throw new OperationException();
						continue;
					}
				}
			}

			if(patientEnrollmentDetails.getEvaluationStatus() != null)
			{
				
				String evaluationStatus = null; 
				if(StringUtil.isEmpty(patientEnrollmentDetails.getEvaluationStatus().getCode()))
				{
					evaluationStatus="";
				}
				else
				{
					try{
					evaluationStatus = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getEvaluationStatus(),
							CodeCache.CODE_TYPE_PATPROT_EVALATION_STATUS, callingUser); 
					
					}catch(CodeNotFoundException e)
					{
						responseHolder.addIssue(
								new Issue(
									IssueTypes.DATA_VALIDATION, 
									"EvaluationStatus Code Not Found: " + patientEnrollmentDetails.getEvaluationStatus().getCode()));
						//throw new OperationException();
						continue;
					}	
				}
				//patProtBean.setPtstEval(evaluationStatus); 	
				patProtsb.append(" FK_CODELST_PTST_EVAL='"+evaluationStatus+"', ");
				
			}
			
			if(patientEnrollmentDetails.getInevaluationStatus() != null)
			{
				String inEval="";
				if(StringUtil.isEmpty(patientEnrollmentDetails.getInevaluationStatus().getCode()))
				{
					inEval="";
				}
				else
				{
						try{
					
						inEval = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getInevaluationStatus(),
								CodeCache.CODE_TYPE_PATPROT_INEVALATION_STATUS, callingUser);
						
						
					}catch(CodeNotFoundException e){
						responseHolder.addIssue(
								new Issue(
									IssueTypes.DATA_VALIDATION, 
									"InevaluationStatus Code Not Found: " + patientEnrollmentDetails.getInevaluationStatus().getCode()));
						//throw new OperationException();
						continue;
					}
				}
				//patProtBean.setPtstInEval(inEval); 
				patProtsb.append(" FK_CODELST_PTST_INEVAL='"+inEval+"', ");
			}
		
			
				
			//Patient Status
			
			if(patientEnrollmentDetails.getSurvivalStatus() != null)
			{
				
				String survivalStatus = null; 
				if(StringUtil.isEmpty(patientEnrollmentDetails.getSurvivalStatus().getCode()))
				{
					survivalStatus="";
				}
				else
				{
					try{
						survivalStatus = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getSurvivalStatus(), CodeCache.CODE_TYPE_PATPROT_SURVIVAL_STATUS, callingUser);
						if((!personB.getPersonStatus().equals(survivalStatus)) &&  patientEnrollmentDetails.getSurvivalStatus().getCode().equals("dead"))
						{
							//personB.setPersonStatus(survivalStatus);
							System.out.println("Setting survival status to dead");
							personsb.append(" FK_CODELST_PSTAT='"+survivalStatus+"', ");
							if(patientEnrollmentDetails.getDateOfDeath() != null) 
							{	//personB.setPersonDeathDt(patientEnrollmentDetails.getDateOfDeath());
								System.out.println("Setting death date");
								personsb.append(" PERSON_DEATHDT=to_date('"+sf.format(patientEnrollmentDetails.getDateOfDeath())+"','MM/dd/yyyy'),");
							}
						}
					}catch(CodeNotFoundException e)
					{
						responseHolder.addIssue(
								new Issue(
									IssueTypes.DATA_VALIDATION, 
									"SurvivalStatus Code Not Found: " + patientEnrollmentDetails.getSurvivalStatus().getCode()));
						//throw new OperationException();
						continue;
					}	
				}
				//patProtBean.setPtstEval(evaluationStatus); 	
				patProtsb.append(" FK_CODELST_PTST_SURVIVAL='"+survivalStatus+"', ");
				
				
			}
			
			
			/*if(patientEnrollmentDetails.getDateOfDeath() != null)
				{
					//patProtBean.setPtstDeathDatePersistent(patientEnrollmentDetails.getDateOfDeath()); 
					patProtsb.append(" DATE_OF_DEATH='"+patientEnrollmentDetails.getDateOfDeath()+"', ");
				}*/
			if(patientEnrollmentDetails.getSpecifyCause() != null) 
				{
					//personB.setDthCauseOther(patientEnrollmentDetails.getSpecifyCause()); 
					personsb.append(" CAUSE_OF_DEATH_OTHER='"+patientEnrollmentDetails.getSpecifyCause()+"', ");
				}
			if(patientEnrollmentDetails.getCauseOfDeath() != null)
			{
				try{
					String patDeathCause = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getCauseOfDeath(), 
							CodeCache.CODE_TYPE_PATIENT_DEATH_CAUSE, callingUser); 
					//personB.setPatDthCause(patDeathCause); 
					personsb.append(" FK_CODELST_PAT_DTH_CAUSE='"+patDeathCause+"', ");
				}catch(CodeNotFoundException e)
				{
					responseHolder.addIssue(
							new Issue(
								IssueTypes.DATA_VALIDATION, 
								"CauseOfDeath Code Not Found: " + patientEnrollmentDetails.getCauseOfDeath().getCode()));
					//throw new OperationException();
					continue; 
				}
			}
			
			if(patientEnrollmentDetails.getDeathRelatedToTrial() != null)
			{
				try{
					String deathRelatedToTrial = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getDeathRelatedToTrial(), 
							CodeCache.CODE_TYPE_PATPROT_STUDY_RELATED_DEATH, callingUser); 
					//patProtBean.setPtstDthStdRel(deathRelatedToTrial);
					patProtsb.append(" FK_CODELST_PTST_DTH_STDREL='"+deathRelatedToTrial+"', ");
				}catch(CodeNotFoundException e)
				{
					responseHolder.addIssue(
							new Issue(
								IssueTypes.DATA_VALIDATION, 
								"DeathRelatedToTrial Code Not Found: " + patientEnrollmentDetails.getDeathRelatedToTrial().getCode()));
					//throw new OperationException();
					continue; 
				}
			}
			
			//patProtBean.setPtstDthStdRelOther(patientEnrollmentDetails.getReasonOfDeathRelatedToTrial()); 
			patProtsb.append(" DEATH_STD_RLTD_OTHER='"+patientEnrollmentDetails.getReasonOfDeathRelatedToTrial()+"', ");
				
			//patStudyStatbean.setPatientId(StringUtil.integerToString(patientPK));
			FK_PER=StringUtil.integerToString(patientPK);
			//patStudyStatbean.setStudyId(StringUtil.integerToString(studyPK)); 
			FK_STUDY=StringUtil.integerToString(studyPK);
			
			
			
			patStudyStatbean.setPatProtObj(patProtBean); 
			
			if(patientEnrollmentDetails.getStatus() != null)
			{
				String status = ""; 
				try{
				 status = AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getStatus(), CodeCache.CODE_TYPE_PATPROT_STATUS, callingUser); 
				//Bug fix 6836
				
				}catch(CodeNotFoundException e)
				{
					responseHolder.addIssue(
							new Issue(
								IssueTypes.DATA_VALIDATION, 
								"status Code Not Found: " + patientEnrollmentDetails.getStatus().getCode()));
					//throw new OperationException();
					continue; 
				}
				//patStudyStatbean.setPatStudyStat(status);
				FK_CODELST_STAT=status;
				if(patientEnrollmentDetails.getStatusReason() != null)
				{
					String  statusReason;  
					try
					{
						statusReason= AbstractService.dereferenceCodeStr(patientEnrollmentDetails.getStatusReason(),
								 patientEnrollmentDetails.getStatus().getCode(), callingUser); 
					}catch(CodeNotFoundException ce)
					{
						responseHolder.addIssue(
								new Issue(
									IssueTypes.DATA_VALIDATION, 
									"Status reason code not found : " + patientEnrollmentDetails.getStatusReason().getCode()));
						//throw new OperationException();
						continue; 
					}
									
					//patStudyStatbean.setPatStudyStatReason(statusReason); 
					PATSTUDYSTAT_REASON=" '"+statusReason+"' ";
				}
			}
			
			//personB.setModifiedBy(StringUtil.integerToString(callingUser.getUserId()));
			personsb.append(" last_modified_by='"+StringUtil.integerToString(callingUser.getUserId())+"'  ");
			personsb.append(" WHERE  PK_PERSON = "+patientPK);
			patProtsb.append(" last_modified_by='"+StringUtil.integerToString(callingUser.getUserId())+"'  ");
			patProtsb.append(" WHERE PK_PATPROT = "+patProtBean.getPatProtId());
			///------------------------------------------------------------------------------------------------------------------------
			//int patientSuccessfullyUpdated = personAgent.updatePerson(personB); 
			//if(patientSuccessfullyUpdated != 0 )
			//{
			//	responseHolder.addIssue(new Issue(IssueTypes.PATIENT_ERROR_UPDATE_DEMOGRAPHICS));
			//	throw new OperationException(); 
			//}
			//----------------------------------------------------------------------------------------------------------------------------
			
			if(patientEnrollmentDetails.getStatusDate() != null) {
				//patStudyStatbean.setStartDate(patientEnrollmentDetails.getStatusDate()); 
				PATSTUDYSTAT_DATE="to_date('"+sf.format(patientEnrollmentDetails.getStatusDate())+"','MM/dd/yyyy')";
			}
			//patStudyStatbean.setCreator(StringUtil.integerToString(callingUser.getUserId()));
			CREATOR=" '"+StringUtil.integerToString(callingUser.getUserId())+"' ";
			//patStudyStatbean.setCurrentStat(patientEnrollmentDetails.isCurrentStatus()? "1": "0");
			String cur=patientEnrollmentDetails.isCurrentStatus()? "1": "0";
			CURRENT_STAT=" '"+cur+"' ";
			//patStudyStatbean.setNotes(patientEnrollmentDetails.getNotes());
			//Bug #: 15431 Raman
			if(patientEnrollmentDetails.getNotes()==null)
			{
				PATSTUDYSTAT_NOTE=" '' ";
			}else
			{
				PATSTUDYSTAT_NOTE=" '"+patientEnrollmentDetails.getNotes()+"' ";
			}
			//patStudyStatbean.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE); 
			IP_ADD=" '"+AbstractService.IP_ADDRESS_FIELD_VALUE+"' ";
			
			
			patStudyStatsb.append("INSERT INTO ERES.ER_PATSTUDYSTAT " +
					" ( PK_PATSTUDYSTAT, FK_CODELST_STAT, FK_PER,FK_STUDY, PATSTUDYSTAT_DATE, " +
					" PATSTUDYSTAT_ENDT,PATSTUDYSTAT_NOTE, RID, CREATOR,LAST_MODIFIED_BY, " +
					" LAST_MODIFIED_DATE, CREATED_ON,IP_ADD, PATSTUDYSTAT_REASON, SCREEN_NUMBER," +
					" SCREENED_BY, SCREENING_OUTCOME, NEXT_FOLLOWUP_ON," +
					" INFORM_CONSENT_VER, CURRENT_STAT) " +
					" VALUES (  :1 ,'" +
					FK_CODELST_STAT +"','" +
					FK_PER+"','" +
					FK_STUDY +"'," +
					PATSTUDYSTAT_DATE +"," +
					PATSTUDYSTAT_ENDT +"," +
					PATSTUDYSTAT_NOTE +"," +
					RID+"," +
					CREATOR+"," +
					LAST_MODIFIED_BY+"," +
					LAST_MODIFIED_DATE+"," +
					"sysdate," +
					IP_ADD+"," +
					PATSTUDYSTAT_REASON+"," +
					SCREEN_NUMBER+"," +
					SCREENED_BY+"," +
					SCREENING_OUTCOME+"," +
					NEXT_FOLLOWUP_ON+"," +
					INFORM_CONSENT_VER+"," +
					CURRENT_STAT+") ");
			
			//SEQ_ER_PATSTUDYSTAT.NEXTVAL
			
			personList.add(personsb.toString());
			patProtList.add(patProtsb.toString());
			patStudyStatList.add(patStudyStatsb.toString());
			//fkList.add(FK_CODELST_STAT+","+FK_PER+","+FK_STUDY+","+PATSTUDYSTAT_DATE);
			fkList.add(FK_CODELST_STAT+","+FK_PER+","+FK_STUDY);
			}
		StudyPatientDAO studyPatientDAO= new StudyPatientDAO();
		String[] person=new String[personList.size()];
		String[] patProt=new String[personList.size()];
		String[] patStudyStat=new String[personList.size()];
		String[] fkArr=new String[personList.size()];
		
		person = personList.toArray(person);
		patProt = patProtList.toArray(patProt);
		patStudyStat = patStudyStatList.toArray(patStudyStat);
		fkArr=fkList.toArray(fkArr);
		String[] retMsgsFromDAO = studyPatientDAO.createMStudyPatientStatus(person,patProt,patStudyStat,fkArr,callingUser.getUserId());
		ArrayList<String> validationMsgList =new  ArrayList<String>();
		if (retMsgsFromDAO == null) {
			for (Issue issue : responseHolder.getIssues().getIssue()) {
				validationMsgList.add(issue.getMessage());
			}
			return validationMsgList.toArray(new String[validationMsgList.size()]);
		}
		return retMsgsFromDAO;
	}
	


	public ResponseHolder updateMStudyPatientStatus(
			List<PatStudyStatBean> patStudyStatB,
			List<UpdateMStudyPatientStatus> updateMPatientStudyStatusListChecked,
			Map<String, Object> parameters) throws OperationException, SQLException
	{
		PatFacilityAgentRObj patFacilityAgent = (PatFacilityAgentRObj) parameters.get("patFacilityAgent"); 
		PersonAgentRObj personAgent = (PersonAgentRObj) parameters.get("personAgent");
		StudyAgent studyAgent=(StudyAgent) parameters.get("studyAgent");
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("objectMapService"); 
		SessionContext sessionContext  = (SessionContext) parameters.get("sessionContext"); 
		UserBean callingUser = (UserBean) parameters.get("callingUser"); 
		Integer patientPK=(Integer) parameters.get("perPK");
		UserAgentRObj userAgent=(UserAgentRObj) parameters.get("userAgent");
		Integer studyPK=(Integer) parameters.get("studyPK");
		ResponseHolder response=(ResponseHolder) parameters.get("response");
		ConfigDetailsObject configDetails=ConfigFacade.getConfigFacade().populateObject(StringUtil.stringToInteger(callingUser.getUserAccountId()), "patstudystatus");
		CodeDao codeDao=new CodeDao();
		PersonBean personB = personAgent.getPersonDetails(patientPK);
		PatProtBean patProtBean = null;
		UpdateMStudyPatientStatus mPatientStudyStatus = null;
		PatStudyStatBean patStudyStatBean = null;
 		List<Integer> patStudyPK = new ArrayList<Integer>();
		
		StringBuffer patStudyStatsb = new StringBuffer();
		StringBuffer patProtsb = new StringBuffer();
		StringBuffer personsb = new StringBuffer();
				
		List<String> mainSQL = new ArrayList<String>();
		List<String> PKSQL = new ArrayList<String>();
		 		
		SimpleDateFormat sf=new SimpleDateFormat("MM/dd/yyyy");
		
		for(int i =0;i<updateMPatientStudyStatusListChecked.size();i++)
		{	
			mPatientStudyStatus = updateMPatientStudyStatusListChecked.get(i);
			
			patStudyStatsb.append("UPDATE ERES.ER_PATSTUDYSTAT SET ");
			patProtsb.append("UPDATE ERES.ER_PATPROT SET ");
			personsb.append("UPDATE EPAT.PERSON SET ");
						
			if(mPatientStudyStatus == null)
			{
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"valid PatientStudyStatus object required "));
				throw new OperationException();
			}	
			if(mPatientStudyStatus.getPatientEnrollmentDetails() == null )
			{
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"valid PatientEnrollmentDetails object required "));
				throw new OperationException();
			}
					
			patStudyStatBean = patStudyStatB.get(i);
			patStudyPK.add(patStudyStatBean.getId());
			patProtBean= patStudyStatB.get(i).getPatProtObj();

			/*if(!(stdPK.equals(patStudyStatB.get(i).getStudyId()) && perPK.equals(patStudyStatB.get(i).getPatientId())))
			{
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"Only  status of one Patient can be updated at the same time "));
				throw new OperationException();
			}*/	
			
			boolean isPersonBUpdated=false;
			//Patient Study Status
			//Reason
			String statusType= codeDao.getCodeSubtype(StringUtil.stringToInteger(patStudyStatBean.getPatStudyStat()));
						
			if(mPatientStudyStatus.getPatientEnrollmentDetails().getStatusReason()!=null)
			{
				if(StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getStatusReason().getCode())&&
						StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getStatusReason().getDescription())&&
						StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getStatusReason().getType()))
				{
					if(isMandatory(configDetails, "reason"))
					{
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_StatusReason_Mandatory));
						throw new OperationException();
					}
					else
					{
						//patStudyStatBean.setPatStudyStatReason("");
						patStudyStatsb.append("FK_CODELST_STAT = null");
					}
				}
				else
				{
					String statusReason="";
					try
					{
						statusReason= AbstractService.dereferenceCodeStr(mPatientStudyStatus.getPatientEnrollmentDetails().getStatusReason(), statusType, callingUser);		
					}
					catch(CodeNotFoundException ce)
					{
						response.addIssue(new Issue(IssueTypes.CODE_NOT_FOUND,"Reason Code Not Found: " + mPatientStudyStatus.getPatientEnrollmentDetails().getStatusReason().getCode()));
						throw new OperationException(); 
					}
				//	patStudyStatBean.setPatStudyStatReason(statusReason);
					patStudyStatsb.append("FK_CODELST_STAT="+statusReason);
				}
			}
		//	patStudyPat.append(",");
			//Status Date
			if(null!=mPatientStudyStatus.getPatientEnrollmentDetails().getStatusDate())
			{
				//patStudyStatBean.setStartDate(mPatientStudyStatus.getPatientEnrollmentDetails().getStatusDate());
				if(mPatientStudyStatus.getPatientEnrollmentDetails().getStatusReason()!=null)
				{	
					patStudyStatsb.append(",PATSTUDYSTAT_DATE=to_date('"+sf.format(mPatientStudyStatus.getPatientEnrollmentDetails().getStatusDate())+"'"+","+"'MM/dd/yyyy'"+")");
				}
				else
					patStudyStatsb.append(" PATSTUDYSTAT_DATE=to_date('"+sf.format(mPatientStudyStatus.getPatientEnrollmentDetails().getStatusDate())+"'"+","+"'MM/dd/yyyy'"+")");
			}
			//Current Status
			
			if(mPatientStudyStatus.getPatientEnrollmentDetails().isCurrentStatus())
			{
				  	if(null!=mPatientStudyStatus.getPatientEnrollmentDetails().getStatusDate() || (mPatientStudyStatus.getPatientEnrollmentDetails().getStatusReason()!=null))
					{	
						patStudyStatsb.append(",CURRENT_STAT = 1");

					}
					else
					{
						patStudyStatsb.append(" CURRENT_STAT = 1");
						
					}	
							
				}
					
			//Notes
			if(!StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getNotes()))
			{
				if(null!=mPatientStudyStatus.getPatientEnrollmentDetails().getStatusDate() || (mPatientStudyStatus.getPatientEnrollmentDetails().getStatusReason()!=null) || (mPatientStudyStatus.getPatientEnrollmentDetails().isCurrentStatus()))
				{	
					patStudyStatsb.append(",PATSTUDYSTAT_NOTE = '"+mPatientStudyStatus.getPatientEnrollmentDetails().getNotes()+"'");
				}
				else
				{
					patStudyStatsb.append(" PATSTUDYSTAT_NOTE = '"+mPatientStudyStatus.getPatientEnrollmentDetails().getNotes()+"'");
				}
				
			}
			else 
			{
				if(mPatientStudyStatus.getPatientEnrollmentDetails().getNotes()!=null)
				{
					if(isMandatory(configDetails, "notes"))
					{		response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_Notes_Mandatory));
							throw new OperationException();
					}
					else
					{
						if(mPatientStudyStatus.getPatientEnrollmentDetails().isCurrentStatus())
						{	
							patStudyStatsb.append(",PATSTUDYSTAT_NOTE =null");
						}
						else
						{
							patStudyStatsb.append("PATSTUDYSTAT_NOTE = null");
						}
						
					}
				}
			}
					
			//Setting Additional Information
			//Patient Study ID
			if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null)
			{
				if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId().trim().length()==0)
				{
					if(isMandatory(configDetails, "patstdid"))
					{
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"Field patientStudyId may not be null"));
						throw new OperationException();
					}
					else
					{
						//patProtBean.setPatStudyId("");
						patProtsb.append("patprot_patstdid = null");
					}
				}
				else
				{
					if(!mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId().equalsIgnoreCase(patProtBean.getPatStudyId()))
					{
						//if(patStudyStatAgent.getCountPatStudyId(studyPK, patientEnrollmentDetails.getPatientStudyId())>0){}
						//patProtBean.setPatStudyId(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId());
						patProtsb.append("patprot_patstdid ='"+mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()+"'");
					}else
						patProtsb.append("patprot_patstdid ='"+patProtBean.getPatStudyId()+"'");
					
				}
			}
			
			
			//Enrolling Site
			if(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)
			{
				if(StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite().getOID())&&
						StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite().getSiteAltId())&&
						StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite().getSiteName()) &&
						((mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite().getPK()== null) || (mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite().getPK()<=0)))
				{
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_EnrollingSite_Mandatory)); 
						throw new OperationException();
				}
				Integer enrollingSite;
				try {
						enrollingSite = ObjectLocator.sitePKFromIdentifier(callingUser, mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite(), sessionContext, objectMapService);
						if(enrollingSite != null && enrollingSite != 0)
						{
							PatFacilityDao patFacilityDao = patFacilityAgent.getPatientFacilitiesSiteIds(patientPK); 
							List siteToEnroll = patFacilityDao.getPatientSite(); 
							if(siteToEnroll.contains(StringUtil.integerToString(enrollingSite)))
							{
								if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null)
								{	
									//patProtBean.setEnrollingSite(StringUtil.integerToString(enrollingSite)); 
									patProtsb.append(",FK_SITE_ENROLLING = "+StringUtil.integerToString(enrollingSite));
								}
								else
									patProtsb.append(" FK_SITE_ENROLLING = "+StringUtil.integerToString(enrollingSite));
							}else
							{
								response.addIssue(new Issue(IssueTypes.ORGANIZATION_AUTHORIZATION,"EnrollingSite is not in list of Study Organizations where user can enroll patient"));
								throw new OperationException(); 
							}
						}else
						{
							response.addIssue(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND,"EnrollingSite not found")); 
							throw new OperationException(); 
						}
					} catch (MultipleObjectsFoundException e) 
					{
						response.addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND,"Found multiple objects that matched Enrolling Site"));
						throw new OperationException();
					}
			}
			//Assigned To
			if(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null)
			{
				if( (StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo().getFirstName())) &&
				        (StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo().getLastName())) &&
				        (StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo().getUserLoginName())) &&
				        (StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo().getOID())) &&
				        ((mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo().getPK()== null) || (mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo().getPK()<=0)))
				{
					if(isMandatory(configDetails, "assignedto"))
					{
						response.addIssue(new Issue( IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_AssignedTo_Mandatory));
						throw new OperationException();
					}
					else
					{
						patProtBean.setAssignTo("");
						if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null || (mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null))
						{	
							patProtsb.append(",FK_USERASSTO = null");
						}else
							patProtsb.append(" FK_USERASSTO = null");
					}
				}
				else
				{
					try
					{
						UserBean assignToBean = ObjectLocator.userBeanFromIdentifier(callingUser, mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo(), userAgent, sessionContext, objectMapService);
						if(assignToBean != null)
						{
							if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null || (mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null))
							{	
							//	patProtBean.setAssignTo(StringUtil.integerToString(assignToBean.getUserId())); 
								patProtsb.append(",FK_USERASSTO ="+StringUtil.integerToString(assignToBean.getUserId()));
							}
							else
								patProtsb.append("FK_USERASSTO ="+StringUtil.integerToString(assignToBean.getUserId()));
						}else
						{
							response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND ,"AssignedTo User Not found"));
							throw new OperationException();
						}
					}catch(MultipleObjectsFoundException e)
					{
						response.addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND,"Found multiple objects that matched Assigned to User"));
						throw new OperationException();
					}
				}
			}
				
			//Physician
			if(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null)
			{
				if( (StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician().getFirstName())) &&
			        (StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician().getLastName())) &&
			        (StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician().getUserLoginName())) &&
			        (StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician().getOID())) &&
			        ((mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician().getPK()== null) || (mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician().getPK()<=0)))
					{
						if(isMandatory(configDetails, "physician"))
						{
							response.addIssue(new Issue( IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_Physician_Mandatory));
							throw new OperationException();
						}
						else
						{
							if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null || (mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null) ||(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null))
							{	
								//patProtBean.setPatProtPhysician("");
								patProtsb.append(",PATPROT_PHYSICIAN = null");
							}
							else
								patProtsb.append(" PATPROT_PHYSICIAN = null");
						}
					}	
			    else
				{
					UserBean physicianBean;
					try {
							physicianBean = ObjectLocator.userBeanFromIdentifier(callingUser, mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician(), userAgent, sessionContext, objectMapService);
							if(physicianBean != null)
							{
								if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)
										||(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null))
								{	
									//patProtBean.setPatProtPhysician("");
									patProtsb.append(",PATPROT_PHYSICIAN = "+StringUtil.integerToString(physicianBean.getUserId()));
								}
								else
									patProtsb.append(" PATPROT_PHYSICIAN = "+StringUtil.integerToString(physicianBean.getUserId()));
								
								
							}else
							{
								response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND ,"Physician Not found"));
								throw new OperationException();
							}
						} 
						catch (MultipleObjectsFoundException e) 
						{
							response.addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND,"Found multiple objects that matched Physician"));
							throw new OperationException();
						}	
				}
			}
			//Treatment Location
			if(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation()!=null)
			{
				if(StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation().getCode())&&
						StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation().getDescription())&&
						StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation().getType()))
				{
					if(isMandatory(configDetails, "treatmentlocation"))
					{
						response.addIssue(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_TreatmentLocation_Mandatory));
						throw new OperationException();
					}
					else
					{
						if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)
								||(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) || (mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null))
						{	
							//patProtBean.setTreatmentLoc("");FK_CODELSTLOC
							patProtsb.append(",FK_CODELSTLOC = null");
						}else
							patProtsb.append("FK_CODELSTLOC = null");
					}
				}
				else
				{
					try{
						 String treatmentlocation = AbstractService.dereferenceCodeStr(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation(), CodeCache.CODE_TYPE_PATPROT_TREATMENT_LOCATION, callingUser); 
						 if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)
									||(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) ||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null))
							{	
								//patProtBean.setTreatmentLoc("");FK_CODELSTLOC
								patProtsb.append(",FK_CODELSTLOC = "+treatmentlocation);
							}else
								patProtsb.append(" FK_CODELSTLOC = "+treatmentlocation);
						 
						 
					}catch(CodeNotFoundException ce)
					{
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"TreatmentLocation Code Not Found: " + mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation().getCode()));
						throw new OperationException();
					}
				}
			}
			// Treating Organization
			if(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization()!=null)
			{
				if(StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization().getOID())&&
						StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization().getSiteAltId())&&
						StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization().getSiteName()) &&
						((mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization().getPK()== null) || (mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization().getPK()<=0)))
				{
					if(isMandatory(configDetails,"treatingorg" ))
					{
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_TreatingOrg_Mandatory)); 
					}
					else
					{
						//patProtBean.setpatOrg("");
						if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)
								||(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null) || (mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation()!=null))
						{
							patProtsb.append(",PATPROT_TREATINGORG = null");
						}else
							patProtsb.append(" PATPROT_TREATINGORG = null");
					}
				}
				else
				{
					try
					{
						Integer treatingOrganization = ObjectLocator.sitePKFromIdentifier(callingUser, mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization(), sessionContext, objectMapService);
						if(treatingOrganization != null && treatingOrganization != 0)
						{
							List<Integer> treatingOrg = new ArrayList<Integer>(); 			
							SiteAgentRObj siteAgentBean = (SiteAgentRObj) parameters.get("siteAgent"); 				 
							SiteDao siteDao = siteAgentBean.getByAccountId(StringUtil.stringToInteger(callingUser.getUserAccountId()));
							//TODO SiteAgentBean .get SiteValues(acctID);
							for( int j = 0; j < siteDao.getSiteIds().size(); j++)
							{
								treatingOrg.add(StringUtil.stringToInteger(siteDao.getSiteIds().get(j).toString())); 
							}
							if(treatingOrg.contains(treatingOrganization))
							{					
								//patProtBean.setpatOrg(StringUtil.integerToString(treatingOrganization));
								if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)
										||(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation()!=null))
								{
									patProtsb.append(",PATPROT_TREATINGORG = "+StringUtil.integerToString(treatingOrganization));
								}else
									patProtsb.append(" PATPROT_TREATINGORG = "+StringUtil.integerToString(treatingOrganization));
							}else
							{
								response.addIssue(new Issue(IssueTypes.ORGANIZATION_AUTHORIZATION,"TreatingOrganization is not in list of possible Treating Organizations"));
								throw new OperationException(); 
							}
						}else
						{
							response.addIssue(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND,"TreatingOrganization not found"));
							throw new OperationException(); 
						}
					} catch (MultipleObjectsFoundException e) {
						response.addIssue(
								new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND,"Found multiple objects that matched Treating Organization"));
						throw new OperationException();
					} 
				}
			}
			
			//Evaluable Status
			//Evaluable Flag
			if(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag()!=null)
			{
				if(StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag().getCode())&&
						StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag().getDescription())&&
						StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag().getType()))
				{
					if(isMandatory(configDetails, "evaluableflag"))
					{
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_EvaluationFlag_Mandatory));
						throw new OperationException();
					}
					else
					{
						//patProtBean.setPtstEvalFlag("");
						if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)
								||(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization()!=null))
						{
							patProtsb.append(",FK_CODELST_PTST_EVAL_FLAG = null");
						}else
							patProtsb.append(" FK_CODELST_PTST_EVAL_FLAG = null");
					}
				}
				else
				{
					try{
						String evaluationFlag = AbstractService.dereferenceCodeStr(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag(),CodeCache.CODE_TYPE_PATPROT_EVALUATION_FLAG, callingUser); 
						if(evaluationFlag != null)
						{
							if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)
									||(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) ||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null) ||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation()!=null) ||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization()!=null))
							{
								patProtsb.append(",FK_CODELST_PTST_EVAL_FLAG = "+evaluationFlag);
							}else
								patProtsb.append(" FK_CODELST_PTST_EVAL_FLAG = "+evaluationFlag);
							//patProtBean.setPtstEvalFlag(evaluationFlag); 
						}else
						{
							throw new OperationException(); 
						}
					}catch(CodeNotFoundException ce)
					{
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"EvaluationFlag Code Not Found: " + mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag().getCode()));
						throw new OperationException();
					}
				}
			}
			//Evaluable Status
			if(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationStatus()!=null)
			{
				if(StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationStatus().getCode())&&
						StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationStatus().getDescription())&&
						StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationStatus().getType()))
				{
					if(isMandatory(configDetails, "evaluablestatus"))
					{
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_EvaluationStatusCode_Mandatory));
						throw new OperationException();
					}
					else
					{
						//patProtBean.setPtstEval("");
						if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)
								||(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization()!=null)||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag()!=null))
						{
							patProtsb.append(",FK_CODELST_PTST_EVAL = null");
						}else
							patProtsb.append(" FK_CODELST_PTST_EVAL = null");
					}
				}
				else
				{
					String evaluationStatus = null; 
					try{
					evaluationStatus = AbstractService.dereferenceCodeStr(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationStatus(),CodeCache.CODE_TYPE_PATPROT_EVALATION_STATUS, callingUser); 
					}catch(CodeNotFoundException e)
					{
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"EvaluationStatus Code Not Found: " + mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationStatus().getCode()));
						throw new OperationException();
					}
					if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
							(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)
							||(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) ||
							(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null) ||
							(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation()!=null) ||
							(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization()!=null)||
							(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag()!=null))
					{
						patProtsb.append(",FK_CODELST_PTST_EVAL = "+evaluationStatus);
					}else
						patProtsb.append(" FK_CODELST_PTST_EVAL = "+evaluationStatus);
					
					//patProtBean.setPtstEval(evaluationStatus); 
				}
			}
			//Unevaluable Status
			if(mPatientStudyStatus.getPatientEnrollmentDetails().getInevaluationStatus()!=null)
			{
				if(StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getInevaluationStatus().getCode())&&
						StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getInevaluationStatus().getDescription())&&
						StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getInevaluationStatus().getType()))
				{
					if(isMandatory(configDetails, "inevaluablestatus"))
					{
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_InevaluationStatusCode_Mandatory));
						throw new OperationException();
					}
					else
					{
						//patProtBean.setPtstInEval("");
						if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)
								||(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization()!=null)||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag()!=null)||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationStatus()!=null))
						{
							patProtsb.append(",FK_CODELST_PTST_INEVAL = null");
						}else
							patProtsb.append(" FK_CODELST_PTST_INEVAL = null");
					}
				}
				else
				{
					try{
						String inEval = AbstractService.dereferenceCodeStr(mPatientStudyStatus.getPatientEnrollmentDetails().getInevaluationStatus(),CodeCache.CODE_TYPE_PATPROT_INEVALATION_STATUS, callingUser);
						if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization()!=null)||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag()!=null)||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationStatus()!=null))
						{
							patProtsb.append(",FK_CODELST_PTST_INEVAL = "+inEval);
						}else
							patProtsb.append(" FK_CODELST_PTST_INEVAL = "+inEval);
						//patProtBean.setPtstInEval(inEval); 
					}catch(CodeNotFoundException e){
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"InevaluationStatus Code Not Found: " + mPatientStudyStatus.getPatientEnrollmentDetails().getInevaluationStatus().getCode()));
						throw new OperationException();
					}
				}
			}
			
			//Status dependent extra details
			//Enrollment Details - if status = enrolled
			if(statusType.equalsIgnoreCase("enrolled"))
			{
				if(mPatientStudyStatus.getPatientEnrollmentDetails().getRandomizationNumber()!=null)
				{
					if(mPatientStudyStatus.getPatientEnrollmentDetails().getRandomizationNumber().trim().length()==0)
					{
						if(isMandatory(configDetails, "randomnumber"))
						{
							response.addIssue(new Issue( IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_RandomNumber_Mandatory)); 
						}
						else
						{
							//patProtBean.setPatProtRandomNumber("");
							if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) ||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null) ||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation()!=null) ||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization()!=null)||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag()!=null)||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationStatus()!=null) ||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getInevaluationStatus()!=null))
							{
								patProtsb.append(",PATPROT_RANDOM = null");
							}else
								patProtsb.append(" PATPROT_RANDOM = null");
						}
					}
					else
					{
						//patProtBean.setPatProtRandomNumber(mPatientStudyStatus.getPatientEnrollmentDetails().getRandomizationNumber());
						if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization()!=null)||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag()!=null)||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationStatus()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getInevaluationStatus()!=null))
						{
							patProtsb.append(",PATPROT_RANDOM = "+mPatientStudyStatus.getPatientEnrollmentDetails().getRandomizationNumber());
						}else
							patProtsb.append(" PATPROT_RANDOM = "+mPatientStudyStatus.getPatientEnrollmentDetails().getRandomizationNumber());
					}
				}
				if(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrolledBy() != null)
				{
					if( (StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrolledBy().getFirstName())) &&
					        (StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrolledBy().getLastName())) &&
					        (StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrolledBy().getUserLoginName())) &&
					        (StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrolledBy().getOID())) &&
					        ((mPatientStudyStatus.getPatientEnrollmentDetails().getEnrolledBy().getPK()== null) || (mPatientStudyStatus.getPatientEnrollmentDetails().getEnrolledBy().getPK()<=0)))
							{
								if(isMandatory(configDetails, "enrolledby"))
								{
									response.addIssue(new Issue( IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_EnrolledBy_Mandatory));
									throw new OperationException();
								}
								else
								{
									//patProtBean.setPatProtUserId("");
									if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) ||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null) ||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation()!=null) ||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization()!=null)||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag()!=null)||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationStatus()!=null) ||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getInevaluationStatus()!=null)||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getRandomizationNumber()!=null))
									{
										patProtsb.append(",FK_USER = null");
									}else
										patProtsb.append(" FK_USER = null");
								}
							}	
					    else
						{
					    	UserBean enrollingUserBean;
					    	try{
					    		enrollingUserBean = ObjectLocator.userBeanFromIdentifier(callingUser, mPatientStudyStatus.getPatientEnrollmentDetails().getEnrolledBy(), userAgent, sessionContext, objectMapService);
					    		if(enrollingUserBean != null)
					    		{
					    			//patProtBean.setPatProtUserId(StringUtil.integerToString(enrollingUserBean.getUserId())); 
					    			if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) ||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null) ||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation()!=null) ||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization()!=null)||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag()!=null)||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationStatus()!=null) ||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getInevaluationStatus()!=null)||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getRandomizationNumber()!=null))
									{
										patProtsb.append(",FK_USER = "+StringUtil.integerToString(enrollingUserBean.getUserId()));
									}else
										patProtsb.append(" FK_USER = "+StringUtil.integerToString(enrollingUserBean.getUserId()));
					    		}else
					    		{
					    			response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND ,"EnrolledBy User Not found"));
					    			throw new OperationException();
					    		}
					    	} catch (MultipleObjectsFoundException e)
					    	{
					    		response.addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND,"Found multiple objects that matched Enrolling User"));
					    		throw new OperationException();
					    	} 
						}
				}
			}	
			
			   //Follow-up Details - if status = followup
				if(statusType.equalsIgnoreCase("followup"))
				{
					if(mPatientStudyStatus.getPatientEnrollmentDetails().getNextFollowUpDate()!=null&&mPatientStudyStatus.getPatientEnrollmentDetails().isEmptyNextFollowUpDate())
					{
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"emptyNextFollowUpDate field is set to true along with nextFollowUpDate"));
			    		throw new OperationException();
					}
					if(mPatientStudyStatus.getPatientEnrollmentDetails().getNextFollowUpDate()!=null)
					{
						//patStudyStatBean.setPtstNextFlwupPersistent(mPatientStudyStatus.getPatientEnrollmentDetails().getNextFollowUpDate());
						if((mPatientStudyStatus.getPatientEnrollmentDetails().getStatusReason() == null)&&(null == mPatientStudyStatus.getPatientEnrollmentDetails().getStatusDate()) && 
								(!mPatientStudyStatus.getPatientEnrollmentDetails().isCurrentStatus())
								&& (StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getNotes())))
						{
								//patStudyStatsb.append(" NEXT_FOLLOWUP_ON ="+mPatientStudyStatus.getPatientEnrollmentDetails().getNextFollowUpDate());
							patStudyStatsb.append(" NEXT_FOLLOWUP_ON =to_date('"+sf.format(mPatientStudyStatus.getPatientEnrollmentDetails().getNextFollowUpDate())+"'"+","+"'MM/dd/yyyy'"+")");
						}
						else
							
							//patStudyStatsb.append(" ,NEXT_FOLLOWUP_ON ="+mPatientStudyStatus.getPatientEnrollmentDetails().getNextFollowUpDate());
							patStudyStatsb.append(" ,NEXT_FOLLOWUP_ON =to_date('"+sf.format(mPatientStudyStatus.getPatientEnrollmentDetails().getNextFollowUpDate())+"'"+","+"'MM/dd/yyyy'"+")");
						
					}
					if(mPatientStudyStatus.getPatientEnrollmentDetails().isEmptyNextFollowUpDate())
					{
						//patStudyStatBean.setPtstNextFlwupPersistent(null);
						if((mPatientStudyStatus.getPatientEnrollmentDetails().getStatusReason() == null)&&(null == mPatientStudyStatus.getPatientEnrollmentDetails().getStatusDate()) && 
								(!mPatientStudyStatus.getPatientEnrollmentDetails().isCurrentStatus())
								&& (StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getNotes())))
						{	
							patStudyStatsb.append("NEXT_FOLLOWUP_ON = null");
						}
						else
							patStudyStatsb.append(",NEXT_FOLLOWUP_ON = null");
					}
				}
				
				//Informed Consent Details - if status = infConsent
				if(statusType.equalsIgnoreCase("infConsent"))
				{
					if(mPatientStudyStatus.getPatientEnrollmentDetails().getInformedConsentVersionNumber()!=null)
					{
						if(mPatientStudyStatus.getPatientEnrollmentDetails().getInformedConsentVersionNumber().trim().length()==0)
						{
							if(isMandatory(configDetails, "icversionnumber"))
							{
								response.addIssue(new Issue( IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_ICVersionNumber_Mandatory)); 
							}
							else
							{
								//patStudyStatBean.setPtstConsentVer("");
								if(null!=mPatientStudyStatus.getPatientEnrollmentDetails().getStatusDate() || (mPatientStudyStatus.getPatientEnrollmentDetails().getStatusReason()!=null) || (mPatientStudyStatus.getPatientEnrollmentDetails().isCurrentStatus()) || (mPatientStudyStatus.getPatientEnrollmentDetails().getNotes()!=null))
								{
									patStudyStatsb.append(",INFORM_CONSENT_VER = null");
								}
								else
									patStudyStatsb.append(" INFORM_CONSENT_VER = null");
							}
						}
						else
						{
							//patStudyStatBean.setPtstConsentVer(mPatientStudyStatus.getPatientEnrollmentDetails().getInformedConsentVersionNumber());
							if(null!=mPatientStudyStatus.getPatientEnrollmentDetails().getStatusDate() || (mPatientStudyStatus.getPatientEnrollmentDetails().getStatusReason()!=null) || (mPatientStudyStatus.getPatientEnrollmentDetails().isCurrentStatus()) || (mPatientStudyStatus.getPatientEnrollmentDetails().getNotes()!=null))
							{	
								patStudyStatsb.append(",INFORM_CONSENT_VER ="+mPatientStudyStatus.getPatientEnrollmentDetails().getInformedConsentVersionNumber());
							}
							else
								patStudyStatsb.append(" INFORM_CONSENT_VER ="+mPatientStudyStatus.getPatientEnrollmentDetails().getInformedConsentVersionNumber());
						}
					}	
				}
				
				//Screening Details - if status = screening
				if(statusType.equalsIgnoreCase("screening"))
				{
					if(mPatientStudyStatus.getPatientEnrollmentDetails().getScreenNumber()!=null)
					{
						if(mPatientStudyStatus.getPatientEnrollmentDetails().getScreenNumber().trim().length()==0)
						{
							if(isMandatory(configDetails, "screennumber"))
							{
								response.addIssue(new Issue( IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_ScreenNumber_Mandatory)); 
								throw new OperationException();
							}
							else
							{
								//patStudyStatBean.setScreenNumber("");
								if(null!=mPatientStudyStatus.getPatientEnrollmentDetails().getStatusDate() || (mPatientStudyStatus.getPatientEnrollmentDetails().getStatusReason()!=null) || (mPatientStudyStatus.getPatientEnrollmentDetails().isCurrentStatus()) || (mPatientStudyStatus.getPatientEnrollmentDetails().getNotes()!=null))
								{
									patStudyStatsb.append(",SCREEN_NUMBER = null");
								}
							}
						}
						else
						{
							patStudyStatBean.setScreenNumber(mPatientStudyStatus.getPatientEnrollmentDetails().getScreenNumber());
							if(null!=mPatientStudyStatus.getPatientEnrollmentDetails().getStatusDate() || (mPatientStudyStatus.getPatientEnrollmentDetails().getStatusReason()!=null) || (mPatientStudyStatus.getPatientEnrollmentDetails().isCurrentStatus()) || (mPatientStudyStatus.getPatientEnrollmentDetails().getNotes()!=null))
							{
								patStudyStatsb.append(",SCREEN_NUMBER = "+mPatientStudyStatus.getPatientEnrollmentDetails().getScreenNumber());
							}
							else
								patStudyStatsb.append(" SCREEN_NUMBER = "+mPatientStudyStatus.getPatientEnrollmentDetails().getScreenNumber());
						
						}
					}
					
					if(mPatientStudyStatus.getPatientEnrollmentDetails().getScreenedBy() != null)
					{
						if( (StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getScreenedBy().getFirstName())) &&
						        (StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getScreenedBy().getLastName())) &&
						        (StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getScreenedBy().getUserLoginName())) &&
						        (StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getScreenedBy().getOID())))
								{
									if(isMandatory(configDetails, "screenedby"))
									{
										response.addIssue(new Issue( IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_ScreenBy_Mandatory));
										throw new OperationException();
									}
									else
									{
										//patStudyStatBean.setScreenedBy("");
										if(null!=mPatientStudyStatus.getPatientEnrollmentDetails().getStatusDate() || (mPatientStudyStatus.getPatientEnrollmentDetails().getStatusReason()!=null) || (mPatientStudyStatus.getPatientEnrollmentDetails().isCurrentStatus()) ||
												(mPatientStudyStatus.getPatientEnrollmentDetails().getNotes()!=null) ||(mPatientStudyStatus.getPatientEnrollmentDetails().getScreenNumber()!=null))
										{
											patStudyStatsb.append(",SCREENED_BY = null");
										}
									
									}
							}	
						else
						{
						    UserBean screeningUserBean; 			
					    	try
					    	{
					    		screeningUserBean = ObjectLocator.userBeanFromIdentifier(callingUser, mPatientStudyStatus.getPatientEnrollmentDetails().getScreenedBy(), userAgent, sessionContext, objectMapService);
					    		if(screeningUserBean != null)
					    		{
					    			//patStudyStatBean.setScreenedBy(StringUtil.integerToString(screeningUserBean.getUserId()));
					    			if(null!=mPatientStudyStatus.getPatientEnrollmentDetails().getStatusDate() || (mPatientStudyStatus.getPatientEnrollmentDetails().getStatusReason()!=null) || (mPatientStudyStatus.getPatientEnrollmentDetails().isCurrentStatus()) ||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getNotes()!=null) ||(mPatientStudyStatus.getPatientEnrollmentDetails().getScreenNumber()!=null))
									{
										patStudyStatsb.append(",SCREENED_BY = "+StringUtil.integerToString(screeningUserBean.getUserId()));
									}
					    			else
					    				patStudyStatsb.append(" SCREENED_BY = "+StringUtil.integerToString(screeningUserBean.getUserId()));
					    			
					    		}
					    		else
					    		{
					    			response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND,"ScreenBy User not found"));
					    			throw new OperationException();
					    		}
					    	}
					    	catch (MultipleObjectsFoundException e)
					    	{
					    		response.addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND,"Found multiple objects that matched ScreenedBy User"));
					    		throw new OperationException();
					    	}	 
						}
					}
					if(mPatientStudyStatus.getPatientEnrollmentDetails().getScreeningOutcome() != null)
					{
						if(StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getScreeningOutcome().getCode())&&
								StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getScreeningOutcome().getDescription())&&
								StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getScreeningOutcome().getType()))
						{
							if(isMandatory(configDetails, "screeningoutcome"))
							{
								response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, SVC.Svc_PatientEnrollment_ScreeningOutcome_Mandatory));
								throw new OperationException();
							}
							else
							{
								//patStudyStatBean.setScreeningOutcome("");
								if(null!=mPatientStudyStatus.getPatientEnrollmentDetails().getStatusDate() || (mPatientStudyStatus.getPatientEnrollmentDetails().getStatusReason()!=null) || (mPatientStudyStatus.getPatientEnrollmentDetails().isCurrentStatus()) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getNotes()!=null) ||(mPatientStudyStatus.getPatientEnrollmentDetails().getScreenNumber()!=null)|| (mPatientStudyStatus.getPatientEnrollmentDetails().getScreenedBy() != null))
								{
									patStudyStatsb.append(",SCREENING_OUTCOME = null");
								}
								
							}
						}
						else
						{
							String screeningOutcome = null; 
							try{
								screeningOutcome = AbstractService.dereferenceCodeStr(mPatientStudyStatus.getPatientEnrollmentDetails().getScreeningOutcome(),CodeCache.CODE_TYPE_PATPROT_SCREENING_OUTCOME, callingUser); 
							}catch(CodeNotFoundException e)
							{
								response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"ScreeningOutcome Code Not Found: " + mPatientStudyStatus.getPatientEnrollmentDetails().getScreeningOutcome().getCode()));
								throw new OperationException();
							}
							//patStudyStatBean.setScreeningOutcome(screeningOutcome);
							if(null!=mPatientStudyStatus.getPatientEnrollmentDetails().getStatusDate() || (mPatientStudyStatus.getPatientEnrollmentDetails().getStatusReason()!=null) || (mPatientStudyStatus.getPatientEnrollmentDetails().isCurrentStatus()) ||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getNotes()!=null) ||(mPatientStudyStatus.getPatientEnrollmentDetails().getScreenNumber()!=null)|| (mPatientStudyStatus.getPatientEnrollmentDetails().getScreenedBy() != null))
							{
								patStudyStatsb.append(",SCREENING_OUTCOME = "+screeningOutcome);
							}
							else
								patStudyStatsb.append(" SCREENING_OUTCOME = "+screeningOutcome);
						}
					}
											
				}
					
					
				//Patient Status
					//Survival Status
					int  deadStatPk = codeDao.getCodeId("ptst_survival","dead");
					if(mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus()!=null)
					{
						if(StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus().getCode())&&
								StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus().getDescription())&&
								StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus().getType()))
						{
							if(isMandatory(configDetails, "survivalstatus"))
							{
								response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_SurvivalStatus_Mandatory));
								throw new OperationException();
							}
							else
							{
								//patProtBean.setPtstSurvival("");
								if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization()!=null)||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag()!=null)||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationStatus()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getInevaluationStatus()!=null)||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getRandomizationNumber()!=null)||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrolledBy() != null))
								{
									patProtsb.append(",FK_CODELST_PTST_SURVIVAL = null");
								}else
									patProtsb.append(" FK_CODELST_PTST_SURVIVAL = null");
							}
						}
						else
						{
							try{
								String survivalStatus=null; 
								try{
									survivalStatus = AbstractService.dereferenceCodeStr(mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus(), CodeCache.CODE_TYPE_PATPROT_SURVIVAL_STATUS, callingUser);
								}
								catch (CodeNotFoundException e) {
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"survivalStatus Code Not Found: " + mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus().getCode()));
									throw new OperationException();
								}
								String prevSurvivalStatus=patProtBean.getPtstSurvival();
								if(StringUtil.stringToNum(prevSurvivalStatus)!= StringUtil.stringToNum(survivalStatus))
								{
									//patProtBean.setPtstSurvival(survivalStatus);
									if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) ||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null) ||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation()!=null) ||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization()!=null)||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag()!=null)||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationStatus()!=null) ||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getInevaluationStatus()!=null)||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getRandomizationNumber()!=null)||
											(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrolledBy() != null))
									{
										patProtsb.append(",FK_CODELST_PTST_SURVIVAL = "+survivalStatus);
									}else
										patProtsb.append(" FK_CODELST_PTST_SURVIVAL = "+survivalStatus);
									
									if(mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus().getCode().equals("dead"))
									{
										if(mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath()==null)
										{
											response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"deathDate field may not be null when survival status provided as dead"));
											throw new OperationException();
										}
										if(mPatientStudyStatus.getPatientEnrollmentDetails().isEmptyDeathDate())
										{
											response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"emptyDeathDate field may not be set to True when survival status provided as dead"));
											throw new OperationException();
										}
										if((StringUtil.stringToInteger(survivalStatus)==deadStatPk))
										{
											//personB.setPersonStatus(String.valueOf(codeDao.getCodeId("patient_status","dead")));
											personsb.append(" FK_CODELST_PSTAT = "+String.valueOf(codeDao.getCodeId("patient_status","dead")));
											isPersonBUpdated=true;
										}
									}
									else
									{
										if(StringUtil.stringToNum(prevSurvivalStatus)==deadStatPk)
										{
											//checking death fields for !null
											//deathDate
											if(mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath()!=null)
											{
												response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"deathDate field must be null when survival status provided as "+mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus().getCode()));
												throw new OperationException();
											}
											if(patProtBean.getPtstDeathDatePersistent()!=null&&!mPatientStudyStatus.getPatientEnrollmentDetails().isEmptyDeathDate())
											{
												response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"survivalStatus may not be set as "+mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus().getCode()+" because Death Date is already provided"));
												throw new OperationException();
											}
											//CauseOfDeath
											if(mPatientStudyStatus.getPatientEnrollmentDetails().getCauseOfDeath()!=null && (
													!StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getCauseOfDeath().getCode())||
													!StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getCauseOfDeath().getDescription())||
													!StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getCauseOfDeath().getType())
													))
											{
												response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"causeOfDeath field must be null when survival status provided as "+mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus().getCode()));
												throw new OperationException();
											}
											if(!StringUtil.isEmpty(personB.getPatDthCause())
													&&(mPatientStudyStatus.getPatientEnrollmentDetails().getCauseOfDeath()==null ||(
															!StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getCauseOfDeath().getCode())||
															!StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getCauseOfDeath().getDescription())||
															!StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getCauseOfDeath().getType())
															)
														))
											{
												response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"survivalStatus may not be set as "+mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus().getCode()+" because cause of death is already provided"));
												throw new OperationException();
											}
											//Specify Cause
											if(mPatientStudyStatus.getPatientEnrollmentDetails().getSpecifyCause()!=null && mPatientStudyStatus.getPatientEnrollmentDetails().getSpecifyCause().trim().length()!=0)
											{
												response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"specifyCause field must be null when survival status provided as "+mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus().getCode()));
												throw new OperationException();
											}
											if(!StringUtil.isEmpty(personB.getDthCauseOther())&& (mPatientStudyStatus.getPatientEnrollmentDetails().getSpecifyCause()==null||mPatientStudyStatus.getPatientEnrollmentDetails().getSpecifyCause().trim().length()!=0))
											{
												response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"survivalStatus may not be set as "+mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus().getCode()+" because specifyCause is already provided"));
												throw new OperationException();
											}
											//Death Related to Study
											if(mPatientStudyStatus.getPatientEnrollmentDetails().getDeathRelatedToTrial()!=null && (
													!StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getDeathRelatedToTrial().getCode())||
													!StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getDeathRelatedToTrial().getDescription())||
													!StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getDeathRelatedToTrial().getType())
													))
											{
												response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"deathRelatedToTrial field must be null when survival status provided as "+mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus().getCode()));
												throw new OperationException();
											}
											if(!StringUtil.isEmpty(patProtBean.getPtstDthStdRel())
													&&(mPatientStudyStatus.getPatientEnrollmentDetails().getDeathRelatedToTrial()==null ||(
															!StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getDeathRelatedToTrial().getCode())||
															!StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getDeathRelatedToTrial().getDescription())||
															!StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getDeathRelatedToTrial().getType())
															)
														))
											{
												response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"survivalStatus may not be set as "+mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus().getCode()+" because deathRelatedToTrial is already provided"));
												throw new OperationException();
											}
											//Reason of Death Related to Study
											if(mPatientStudyStatus.getPatientEnrollmentDetails().getReasonOfDeathRelatedToTrial()!=null)
											{
												response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"raesonOfDeathRelatedToTrial field must be null when survival status provided as "+mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus().getCode()));
												throw new OperationException();
											}
											if(!StringUtil.isEmpty(patProtBean.getPtstDthStdRelOther())&& (mPatientStudyStatus.getPatientEnrollmentDetails().getReasonOfDeathRelatedToTrial()==null||mPatientStudyStatus.getPatientEnrollmentDetails().getReasonOfDeathRelatedToTrial().trim().length()!=0))
											{
												response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"survivalStatus may not be set as "+mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus().getCode()+" because raesonOfDeathRelatedToTrial is already provided"));
												throw new OperationException();
											}
											//personB.setPersonStatus(String.valueOf(codeDao.getCodeId("patient_status","A")));
											personsb.append(" FK_CODELST_PSTAT = "+String.valueOf(codeDao.getCodeId("patient_status","A")));
											isPersonBUpdated=true;
										}
									}
								}
								else
								{
									//status provided is already person's current survival status.Do nothing.
									if(!mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus().getCode().equals("dead"))
									{
										personsb.append(" FK_CODELST_PSTAT = "+String.valueOf(codeDao.getCodeId("patient_status","A")));
									}else
										personsb.append(" FK_CODELST_PSTAT = null");
									
								}
							}catch(CodeNotFoundException ce)
							{
								response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"Survival Status Code Not Found: " + mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus().getCode()));
								throw new OperationException();
							}
						}
					}
					
					//Death Date
					if(mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath()!=null)
					{
						if(StringUtil.stringToNum(patProtBean.getPtstSurvival())!=deadStatPk)
						{
							response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"dateOfDeath field may not be provided along with person survivalStatus as " +codeDao.getCodeSubtype(StringUtil.stringToNum(patProtBean.getPtstSurvival())) ));
							throw new OperationException();
						}
						if (mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath().before(personB.getPersonDb()))
					    {
					        response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "DeathDate cannot be before Date of Birth"));
					        throw new OperationException();
					    }
				    	Date date = new Date();
				    	if (mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath().after(date))
				    	{
				    	      response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Date of death should not be after present day"));
				    	      throw new OperationException();
				    	}
						//patProtBean.setPtstDeathDatePersistent(mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath());
						
						if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization()!=null)||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag()!=null)||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationStatus()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getInevaluationStatus()!=null)||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getRandomizationNumber()!=null)||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrolledBy() != null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus()!=null))
						{
							patProtsb.append(",DATE_OF_DEATH = to_date('"+sf.format(mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath())+"'"+","+"'MM/dd/yyyy'"+")");
							//patStudyStatsb.append(" PATSTUDYSTAT_DATE=to_date('"+sf.format(mPatientStudyStatus.getPatientEnrollmentDetails().getStatusDate())+"'"+","+"'MM/dd/yyyy'"+")");
							
						}else
						{
							patProtsb.append(" DATE_OF_DEATH = to_date('"+sf.format(mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath())+"'"+","+"'MM/dd/yyyy'"+")");
						}
						//personB.setPersonDeathDate(DateUtil.dateToString(mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath()));
						if(mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus()!=null)
						//	if(StringUtil.stringToNum(prevSurvivalStatus)!= StringUtil.stringToNum(survivalStatus))
						{	
							
							personsb.append(",PERSON_DEATHDT = to_date('"+sf.format(mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath())+"'"+","+"'MM/dd/yyyy'"+")");
							//patStudyStatsb.append(" PATSTUDYSTAT_DATE=to_date('"+sf.format(mPatientStudyStatus.getPatientEnrollmentDetails().getStatusDate())+"'"+","+"'MM/dd/yyyy'"+")");
							//patProtsb.append(",DATE_OF_DEATH = to_date('"+mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath()+"'"+","+"'MM/dd/yyyy'"+")");
						}
						else
						{
							//personsb.append(" PERSON_DEATHDT = "+DateUtil.dateToString(mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath()));
							personsb.append(" PERSON_DEATHDT = to_date('"+sf.format(mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath())+"'"+","+"'MM/dd/yyyy'"+")");
						}	
						isPersonBUpdated=true;
					}
					//emptyDeathDate
					if(mPatientStudyStatus.getPatientEnrollmentDetails().isEmptyDeathDate())
					{
						if(StringUtil.stringToNum(patProtBean.getPtstSurvival())==deadStatPk)
						{
							response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"emptyDeathDate field may not be set to true along with person survivalStatus as " +codeDao.getCodeSubtype(StringUtil.stringToNum(patProtBean.getPtstSurvival())) ));
							throw new OperationException();
						}
						if(mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath()!=null)
						{
							response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"emptyDeathDate field may not be set to true along with person dateOfDeath provided" ));
							throw new OperationException();
						}
						//patProtBean.setPtstDeathDatePersistent(null);
						if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization()!=null)||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag()!=null)||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationStatus()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getInevaluationStatus()!=null)||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getRandomizationNumber()!=null)||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrolledBy() != null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus()!=null) ||
								(mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath()!=null))
						{
							patProtsb.append(",DATE_OF_DEATH = null");
						}else
							patProtsb.append(" DATE_OF_DEATH = null");
						
						if(mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus()!=null || (mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath()!=null) )
						{	
							
							personsb.append(",PERSON_DEATHDT = null");
						}
						else
						{
							personsb.append(" PERSON_DEATHDT = null");
						}	
						
						//personB.setPersonDeathDate(null);
						isPersonBUpdated=true;
					}
					//Cause of death
					if(mPatientStudyStatus.getPatientEnrollmentDetails().getCauseOfDeath()!=null)
					{
						if(StringUtil.stringToNum(patProtBean.getPtstSurvival())!=deadStatPk&&(
								!StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getCauseOfDeath().getCode())||
								!StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getCauseOfDeath().getDescription())||
								!StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getCauseOfDeath().getType())
								)
							)
						{
							response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"causeOfDeath field may not be provided along with person survivalStatus as " +codeDao.getCodeSubtype(StringUtil.stringToNum(patProtBean.getPtstSurvival())) ));
							throw new OperationException();
						}
						if(StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getCauseOfDeath().getCode())&&
								StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getCauseOfDeath().getDescription())&&
								StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getCauseOfDeath().getType()))
						{
							if(isMandatory(configDetails, "cod"))
							{
								response.addIssue(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_CauseOfDeath_Mandatory));
								throw new OperationException();
							}
							else
							{
								//personB.setPatDthCause("");
								if(mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus()!=null || (mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath()!=null) || (mPatientStudyStatus.getPatientEnrollmentDetails().isEmptyDeathDate()))
								{	
										personsb.append(",FK_CODELST_PAT_DTH_CAUSE = null");
								}
								else
								{
									personsb.append(" FK_CODELST_PAT_DTH_CAUSE = null");
								}
								
								isPersonBUpdated=true;
							}
						}
						else
						{
							try{
								//personB.setPatDthCause(AbstractService.dereferenceCodeStr(mPatientStudyStatus.getPatientEnrollmentDetails().getCauseOfDeath(), CodeCache.CODE_TYPE_PATIENT_DEATH_CAUSE, callingUser));
								if(mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus()!=null || (mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath()!=null) || (mPatientStudyStatus.getPatientEnrollmentDetails().isEmptyDeathDate()))
								{	
										personsb.append(",FK_CODELST_PAT_DTH_CAUSE = "+AbstractService.dereferenceCodeStr(mPatientStudyStatus.getPatientEnrollmentDetails().getCauseOfDeath(), CodeCache.CODE_TYPE_PATIENT_DEATH_CAUSE, callingUser));
								}
								else
								{
									personsb.append(" FK_CODELST_PAT_DTH_CAUSE = "+AbstractService.dereferenceCodeStr(mPatientStudyStatus.getPatientEnrollmentDetails().getCauseOfDeath(), CodeCache.CODE_TYPE_PATIENT_DEATH_CAUSE, callingUser));
								}
							}
							catch (CodeNotFoundException e) {
								response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"causeOfDeath Code Not Found: " + mPatientStudyStatus.getPatientEnrollmentDetails().getCauseOfDeath().getCode()));
								throw new OperationException();
							}
							isPersonBUpdated=true;
						}
					}
					//specify cause
					if(mPatientStudyStatus.getPatientEnrollmentDetails().getSpecifyCause()!=null)
					{
						if(StringUtil.stringToNum(patProtBean.getPtstSurvival())!=deadStatPk && mPatientStudyStatus.getPatientEnrollmentDetails().getSpecifyCause().trim().length()!=0)
						{
							response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"specifyCause field may not be provided along with person survivalStatus as " +codeDao.getCodeSubtype(StringUtil.stringToNum(patProtBean.getPtstSurvival())) ));
							throw new OperationException();
						}
						if(mPatientStudyStatus.getPatientEnrollmentDetails().getSpecifyCause().trim().length()==0)
						{
							if(isMandatory(configDetails, "specifycause"))
							{
								response.addIssue(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_SpecifyCause_Mandatory)); 
							}
							else
							{
								//personB.setDthCauseOther("");
								if(mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus()!=null ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath()!=null)||
										(mPatientStudyStatus.getPatientEnrollmentDetails().isEmptyDeathDate()) || 
										(mPatientStudyStatus.getPatientEnrollmentDetails().getCauseOfDeath()!=null))
								{	
										personsb.append(",CAUSE_OF_DEATH_OTHER = null");
								}
								else
								{
									personsb.append(" CAUSE_OF_DEATH_OTHER = null");
								}
								
								isPersonBUpdated=true;
							}
						}
						else
						{
							//personB.setDthCauseOther(mPatientStudyStatus.getPatientEnrollmentDetails().getSpecifyCause());
							if(mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus()!=null ||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath()!=null)||
									(mPatientStudyStatus.getPatientEnrollmentDetails().isEmptyDeathDate()) || 
									(mPatientStudyStatus.getPatientEnrollmentDetails().getCauseOfDeath()!=null))
							{	
									personsb.append(",CAUSE_OF_DEATH_OTHER = '"+mPatientStudyStatus.getPatientEnrollmentDetails().getSpecifyCause()+"'");
							}
							else
							{
								personsb.append(" CAUSE_OF_DEATH_OTHER = '"+mPatientStudyStatus.getPatientEnrollmentDetails().getSpecifyCause()+"'");
							}
							isPersonBUpdated=true;
						}
					}
					// modified by 
					//personsb.append(",IP_ADD = "+AbstractService.IP_ADDRESS_FIELD_VALUE);
					if(personsb.length() == 23)
					{
						personsb.append(" last_modified_by = "+callingUser.getUserId());
					}
					else
					{
						personsb.append(",last_modified_by = "+callingUser.getUserId());
					}	
					//IP_ADDRESS
					personsb.append(",IP_ADD = '"+AbstractService.IP_ADDRESS_FIELD_VALUE+"'");
					
					//Death related to study personB.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE);
					if(mPatientStudyStatus.getPatientEnrollmentDetails().getDeathRelatedToTrial()!=null)
					{
						if(StringUtil.stringToNum(patProtBean.getPtstSurvival())!=deadStatPk&&(
															!StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getDeathRelatedToTrial().getCode())||
															!StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getDeathRelatedToTrial().getDescription())||
															!StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getDeathRelatedToTrial().getType())
															))
						{
							response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"deathRelatedToTrial field may not be provided along with person survivalStatus as " +codeDao.getCodeSubtype(StringUtil.stringToNum(patProtBean.getPtstSurvival())) ));
							throw new OperationException();
						}
						
						if(StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getDeathRelatedToTrial().getCode())&&
								StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getDeathRelatedToTrial().getDescription())&&
								StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getDeathRelatedToTrial().getType()))
						{
							if(isMandatory(configDetails, "deathrelated"))
							{
								response.addIssue(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_DeathRelatedToTrial_Mandatory));
								throw new OperationException();
							}
							else
							{
								//patProtBean.setPtstDthStdRel("");
								if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization()!=null)||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag()!=null)||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationStatus()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getInevaluationStatus()!=null)||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getRandomizationNumber()!=null)||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrolledBy() != null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath()!=null))
								{
									patProtsb.append(",FK_CODELST_PTST_DTH_STDREL = null");
								}else
									patProtsb.append(" FK_CODELST_PTST_DTH_STDREL = null");
							}
						}
						else
						{
							try{
								//patProtBean.setPtstDthStdRel(AbstractService.dereferenceCodeStr(mPatientStudyStatus.getPatientEnrollmentDetails().getDeathRelatedToTrial(), CodeCache.CODE_TYPE_PATPROT_STUDY_RELATED_DEATH, callingUser));
								if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization()!=null)||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag()!=null)||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationStatus()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getInevaluationStatus()!=null)||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getRandomizationNumber()!=null)||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrolledBy() != null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath()!=null))
								{
									patProtsb.append(",FK_CODELST_PTST_DTH_STDREL = "+AbstractService.dereferenceCodeStr(mPatientStudyStatus.getPatientEnrollmentDetails().getDeathRelatedToTrial(), CodeCache.CODE_TYPE_PATPROT_STUDY_RELATED_DEATH, callingUser));
								}else
									patProtsb.append(" FK_CODELST_PTST_DTH_STDREL = "+AbstractService.dereferenceCodeStr(mPatientStudyStatus.getPatientEnrollmentDetails().getDeathRelatedToTrial(), CodeCache.CODE_TYPE_PATPROT_STUDY_RELATED_DEATH, callingUser));
							}
							catch (CodeNotFoundException e) {
								response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"deathRelatedToTrial Code Not Found: " + mPatientStudyStatus.getPatientEnrollmentDetails().getDeathRelatedToTrial().getCode()));
								throw new OperationException();
							}
						}
					}
					//Reason of Death Related to Study
					if(mPatientStudyStatus.getPatientEnrollmentDetails().getReasonOfDeathRelatedToTrial()!=null)
					{
						if(StringUtil.stringToNum(patProtBean.getPtstSurvival())!=deadStatPk&&mPatientStudyStatus.getPatientEnrollmentDetails().getReasonOfDeathRelatedToTrial().trim().length()!=0)
						{
							response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"reasonOfDeathRelatedToTrial field may not be provided along with person survivalStatus as " +codeDao.getCodeSubtype(StringUtil.stringToNum(patProtBean.getPtstSurvival())) ));
							throw new OperationException();
						}
						
						
						if(mPatientStudyStatus.getPatientEnrollmentDetails().getReasonOfDeathRelatedToTrial().trim().length()==0)
						{
							if(isMandatory(configDetails, "reasonofdeathrel"))
							{
								response.addIssue(new Issue(	IssueTypes.DATA_VALIDATION,	SVC.Svc_PatientEnrollment_DeathRelatedToTrial_Mandatory)); 
								throw new OperationException();
							}
							else
							{
								//patProtBean.setPtstDthStdRelOther("");
								if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization()!=null)||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag()!=null)||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationStatus()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getInevaluationStatus()!=null)||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getRandomizationNumber()!=null)||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrolledBy() != null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath()!=null) ||
										(mPatientStudyStatus.getPatientEnrollmentDetails().getDeathRelatedToTrial()!=null))
								{
									patProtsb.append(",DEATH_STD_RLTD_OTHER = null");
								}else
									patProtsb.append(" DEATH_STD_RLTD_OTHER = null");	
							}
						}
						else
						{
							//patProtBean.setPtstDthStdRelOther(mPatientStudyStatus.getPatientEnrollmentDetails().getReasonOfDeathRelatedToTrial());
							if(mPatientStudyStatus.getPatientEnrollmentDetails().getPatientStudyId()!=null ||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrollingSite()!=null)||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getAssignedTo()!=null) ||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getPhysician()!=null) ||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatmentLocation()!=null) ||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getTreatingOrganization()!=null)||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationFlag()!=null)||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getEvaluationStatus()!=null) ||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getInevaluationStatus()!=null)||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getRandomizationNumber()!=null)||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getEnrolledBy() != null) ||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getSurvivalStatus()!=null) ||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getDateOfDeath()!=null) ||
									(mPatientStudyStatus.getPatientEnrollmentDetails().getDeathRelatedToTrial()!=null))
							{
								patProtsb.append(",DEATH_STD_RLTD_OTHER = '"+mPatientStudyStatus.getPatientEnrollmentDetails().getReasonOfDeathRelatedToTrial()+"'");
							}else
								patProtsb.append(" DEATH_STD_RLTD_OTHER = '"+mPatientStudyStatus.getPatientEnrollmentDetails().getReasonOfDeathRelatedToTrial()+"'");	
						}
					}
					
					//patStudyStatBean.getId()  patStudyStatsb.append(" where  PK_PATSTUDYSTAT ="+updateMPatientStudyStatusListChecked.get(i).getPatientStudyStatusIdentifier().getPK());
					//patStudyStatsb.append(" where  PK_PATSTUDYSTAT ="+parameters.get("patStudyPK"));
					patStudyStatsb.append(" where  PK_PATSTUDYSTAT ="+patStudyStatBean.getId());
					patProtsb.append(" where PK_PATPROT = "+patProtBean.getPatProtId());
					personsb.append(" where PK_PERSON = "+patStudyStatBean.getPatientId());
					
					if(patProtsb.length()>50)
					{	
						mainSQL.add(patProtsb.toString());
						PKSQL.add(StringUtil.integerToString(patStudyStatBean.getId()));
						
					}
						
						mainSQL.add(personsb.toString());
						PKSQL.add(StringUtil.integerToString(patStudyStatBean.getId()));
				
					if(patStudyStatsb.length()>60)
					{	
						mainSQL.add(patStudyStatsb.toString());
						PKSQL.add(StringUtil.integerToString(patStudyStatBean.getId()));
					}
					patStudyStatsb.delete(0, patStudyStatsb.length());
					patProtsb.delete(0, patProtsb.length());
					personsb.delete(0, personsb.length());
					
					
					if(mainSQL.size()<=0)
					{
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,	"Input parameter requires to upadate Patient Study Status"));
						throw new OperationException();
					}	
			}	

		
		int ridPat=0,latestRaid=0;
		AuditRowEpatAgent auditRowEpatAgent=(AuditRowEpatAgent) parameters.get("auditRowEpatAgent");
		ridPat = auditRowEpatAgent.getRID(personB.personPKId, "PERSON", "PK_PERSON");
		latestRaid=auditRowEpatAgent.findLatestRaid(ridPat);
		
		String [] updatesCount = StudyPatientDAO.updateMStudyPatientStatus(mainSQL,PKSQL);
		for(int j=0;j<updatesCount.length;j++)
		{	
				if(updatesCount[j].contains("Error"))
				{
					response.addIssue(new Issue(null,updatesCount[j]));
				
				}
				else
				{
					ObjectMap map = objectMapService
							.getOrCreateObjectMapFromPK("er_patstudystat",
									Integer.valueOf(updatesCount[j]));
					
							response.addAction(new CompletedAction("StudyPatientStatus with OID :"+map.getOID() +" Pk : "+updatesCount[j]+" is updated successfully", CRUDAction.UPDATE));
								//response.addAction(new CompletedAction("StudyPatientStatus is updated successfully", CRUDAction.UPDATE));
							mPatientStudyStatus = updateMPatientStudyStatusListChecked.get(j);
							if(StringUtil.isEmpty(mPatientStudyStatus.getPatientEnrollmentDetails().getReasonForChange()))
							{
								StudyBean sb=studyAgent.getStudyDetails(studyPK.intValue());
								if(sb.getFdaRegulatedStudy().equalsIgnoreCase("1"))
								{
									response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,"reasonForChange field may not be null when study is marked as FDA regulated"));
									throw new OperationException();
								}
							}	
							else
							{
									AuditRowEresAgent auditRowEresAgnet=(AuditRowEresAgent) parameters.get("auditRowEresAgnet");
									int ridERes =auditRowEresAgnet.getRID(patStudyStatBean.getId(), "ER_PATSTUDYSTAT", "PK_PATSTUDYSTAT");
									int raid = auditRowEresAgnet.findLatestRaid(ridERes, callingUser.getUserId().intValue());
									int ret=auditRowEresAgnet.setReasonForChange(raid, mPatientStudyStatus.getPatientEnrollmentDetails().getReasonForChange());
									if (!(raid > 0)||(ret!=1)){
										response.addIssue(new Issue(IssueTypes.ERROR_UPDATING_STUDY_PATIENT_STATUS,"Error in updating Study Patient Status"));
										throw new OperationException();
									}
									AuditRowEpatDao auditDao = new AuditRowEpatDao();
									ArrayList listRaids = auditDao.getAllLatestRaids(ridPat, callingUser.getUserId().intValue(), latestRaid);
							        System.out.println("listRaid size is "+listRaids.size());
							        if (null != listRaids){
								        for (int i=0; i<listRaids.size();i++){
								        	raid = StringUtil.stringToNum(""+listRaids.get(i));
								        	
								        	if(raid>0)
								        	{
								        		((AuditRowEpatAgent)parameters.get("auditRowEpatAgent")).setReasonForChange(raid, mPatientStudyStatus.getPatientEnrollmentDetails().getReasonForChange());
								        	}
								        }
							        }
								}
											
				
				}	
				
			}
		
			return response;
		
			}



}
