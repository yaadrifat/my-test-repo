package com.velos.services.studypatient;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.BrowserRows;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.FilterUtil;
import com.velos.esch.service.util.StringUtil;
import com.velos.services.OperationException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Code;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientProtIdentifier;
import com.velos.services.model.PatientStudyStatusIdentifier;
import com.velos.services.model.StudyPatient;
import com.velos.services.model.StudyPatientResults;
import com.velos.services.model.StudyPatientSearch;
import com.velos.services.model.StudyPatientStatus;
import com.velos.services.model.UserIdentifier;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ServicesUtil;

public class StudyPatientDAO extends CommonDAO{
	
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(StudyPatientDAO.class);
	
	private static final String SPACE_STR = " ";
	private static final String ASC_STR = "asc";
	private static final String DEFAULT_ORDER_BY = "lower(enrollingorg_name)";
	private static final String NULL_STR = "null";
	private static final String EMPTY_STR = "";
	private static final String COL_FK_PER = "fk_per";
	private static final String COL_FIRSTNAME = "mask_person_fname";
	private static final String COL_LASTNAME = "mask_person_lname";
	private static final String COL_PER_CODE = "per_code";
	private static final String COL_ENROLLINGORG_NAME = "enrollingorg_name";
	private static final String COL_ENROLLING_DATE = "patprot_enroldt_datesort";
	private static final String COL_PATSTDID = "PATPROT_PATSTDID";
	private static final String COL_CURSTATID = "currentstatid";
	private static final String COL_PK_PATPROT = "PK_PATPROT";
	private static final String COL_LVISIT_NAME = "last_visit_name";
	private static final String COL_NVISIT_DATE = "next_visit_datesort";
	private static final String COL_PATSTDSTAT_SUBTYP = "PATSTUDYSTAT_SUBTYPE";
	private static final String COL_PATSTDSTAT_DESC = "PATSTUDYSTAT_DESC";
	private static final String COL_CURPATSTDSTAT_SUBTYP = "ptstdcurstat_subtype";
	private static final String COL_CURPATSTDSTAT_DESC = "ptstdycur_stat";
	private static final String COL_ASSIGNTO_NAME = "assignedto_name";
	private static final String COL_ORGRIGHTS = "accRight";
	public static final String KEY_PAGE = "page";
	public static final String KEY_PAGE_SIZE = "pageSize";
	public static final String KEY_ORDER_BY = "orderBy";
	public static final String KEY_ORDER_TYPE = "orderType";
	
	private Long totalCount = 0L;
	private Long pageSize = 0L;
	private Integer pageNumber = 0;
	
	/*private ArrayList<String> fkPerList = new ArrayList<String>();
	private ArrayList<String> firstNameList = new ArrayList<String>();
	private ArrayList<String> lastNameList = new ArrayList<String>();
	private ArrayList<String> perCodeList = new ArrayList<String>();
	private ArrayList<String> enrollingOrgList = new ArrayList<String>();
	private ArrayList<String> enrollingDateList = new ArrayList<String>();
	private ArrayList<String> patStdIdList = new ArrayList<String>();
	private ArrayList<String> curStatIdList = new ArrayList<String>();
	private ArrayList<String> pkPatprotList = new ArrayList<String>();
	private ArrayList<String> lVisitNameList = new ArrayList<String>();
	private ArrayList<String> nVisitDateList = new ArrayList<String>();
	private ArrayList<String> patStdStatSubTypList = new ArrayList<String>();
	private ArrayList<String> patStdStatDescList = new ArrayList<String>();
	private ArrayList<String> assignToNameList = new ArrayList<String>();
	
	public ArrayList<String> getFirstNameList() { return firstNameList; }
	public ArrayList<String> getLastNameList() { return lastNameList; }
	public ArrayList<String> getOrgNameList() { return orgNameList; }
	public ArrayList<String> getPkUserList() { return pkUserList; }
	public ArrayList<String> getMiddleNameList() { return middleNameList; }
	public ArrayList<String> getLoginNameList() { return loginNameList; }
	public ArrayList<String> getUsrStatList() { return usrStatList; }
	public ArrayList<String> getUsrTypeList() { return usrTypeList; }
	public ArrayList<String> getAddressList() { return addressList; }
	public ArrayList<String> getAddCityList() { return addCityList; }
	public ArrayList<String> getAddStateList() { return addStateList; }
	public ArrayList<String> getAddZipList() { return addZipList; }
	public ArrayList<String> getAddEmailList() { return addEmailList; }
	public ArrayList<String> getAddPhoneList() { return addPhoneList; }
	public ArrayList<String> getFkDefaultGroupList() { return fkDefaultGroupList; }
	public ArrayList<String> getDefaultGroupList() { return defaultGroupList; }
	
	public Long getTotalCount() { return totalCount; }
	public Long getPageSize() { return pageSize; }
	public Integer getPageNumber() { return pageNumber; }*/
	
	public static String getSQLString(){
		StringBuilder sbSQL = new StringBuilder();
		sbSQL.append(" Select distinct p.rowid as rowcount,erv.fk_per, p.person_lname as mask_person_lname, p.person_fname as mask_person_fname, "); 
		sbSQL.append(" erv.per_code,");
		sbSQL.append(" erv.PATSTUDYSTAT_SUBTYPE,erv.PATSTUDYSTAT_DESC,erv.PER_SITE, ");
		sbSQL.append(" (select site_name from ER_SITE where pk_site = erv.PER_SITE) enrollingorg_name,");  
		sbSQL.append(" erv.patprot_enroldt as patprot_enroldt_datesort,"); 
		sbSQL.append(" erv.last_visit_name,erv.cur_visit, erv.cur_visit_date, erv.next_visit as next_visit_datesort, ");
		sbSQL.append(" lower(p.person_lname || p.person_fname) mask_patnamelower,lower(erv.PATPROT_PATSTDID) lowerpatstdid,erv.PATPROT_PATSTDID,");
		sbSQL.append(" assignedto_name,physician_name,enrolledby_name,treatingorg_name,erv.pk_patstudystat currentstatid,erv.PK_PATPROT ");
		sbSQL.append("  from erv_studypat_by_visit erv ,  erv_person p where P.pk_person = erv.fk_per ");
		sbSQL.append(" and erv.fk_study = ? ");
        sbSQL.append(" and exists (select * from ER_PATFACILITY fac where fac.fk_per = erv.fk_per  ");
        sbSQL.append(" and fac.fk_site in ( ? ) and fac.patfacility_accessright > 0 ) ");
		String masterSql = sbSQL.toString();
		return masterSql;
	}
	
public static List<StudyPatient> getStudyPatientByStudyPK(Integer studyPK, Integer sitePK) throws OperationException {
		
		PreparedStatement pstmt = null;
		Connection conn = null;
		ArrayList<StudyPatient> lstStudyPatient = new ArrayList<StudyPatient>();
		
		try{
			
			conn = getConnection();

			//if (logger.isDebugEnabled()) logger.debug(" sql:" + sql);
			StudyPatient studyPatient = null;
			pstmt = conn.prepareStatement(getSQLString());
			pstmt.setInt(1, studyPK);
			pstmt.setInt(2, sitePK);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				studyPatient =  new StudyPatient();
				PatientIdentifier patientIdentifier = new PatientIdentifier();
				//create or get person object map for studyPatient
				ObjectMapService objectMapService = ServicesUtil.getObjectMapService(); 
				ObjectMap obj = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, rs.getInt("fk_per"));
				String strOID = obj.getOID();
				
				patientIdentifier.setPatientId(rs.getString("per_code"));
				patientIdentifier.setOID(strOID);
				patientIdentifier.setPK( rs.getInt("fk_per"));
				studyPatient.setPatientIdentifier(patientIdentifier);
				
				studyPatient.setStudyPatId(rs.getString("PATPROT_PATSTDID"));
				studyPatient.setStudyPatFirstName(rs.getString("mask_person_fname"));
				studyPatient.setStudyPatLastName(rs.getString("mask_person_lname"));
				studyPatient.setStudyPatLastVisit(rs.getString("last_visit_name"));
				studyPatient.setStudyPatNextDue(rs.getDate("next_visit_datesort"));
				PatientStudyStatusIdentifier studyPatStatId = new PatientStudyStatusIdentifier();
				obj = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATSTUDYSTAT, rs.getInt("currentstatid"));
				strOID = obj.getOID();
				studyPatStatId.setOID(strOID);
				studyPatStatId.setPK(rs.getInt("currentstatid"));
				studyPatient.setStudyPatStatId(studyPatStatId);
				PatientProtIdentifier patProtId = new PatientProtIdentifier();
				obj = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATPROT, rs.getInt("PK_PATPROT"));
				strOID = obj.getOID();
				patProtId.setOID(strOID);
				patProtId.setPK(rs.getInt("PK_PATPROT"));
				studyPatient.setPatprotId(patProtId);
				studyPatient.setStudyPatEnrollDate(rs.getDate("patprot_enroldt_datesort"));
				Code studyPatStatus = new Code(CodeCache.CODE_TYPE_PATPROT_STATUS, rs.getString("PATSTUDYSTAT_SUBTYPE"), rs.getString("PATSTUDYSTAT_DESC"));
				studyPatient.setStudyPatStatus(studyPatStatus);
				OrganizationIdentifier studyPatEnrollingSite = new OrganizationIdentifier(rs.getString("enrollingorg_name"), ""); /// for UCSD SPA done like that
				studyPatient.setStudyPatEnrollingSite(studyPatEnrollingSite);
				UserIdentifier studyPatAssignedTo = new UserIdentifier(rs.getString("assignedto_name"));  // for UCSD SPA done like that
				studyPatient.setStudyPatAssignedTo(studyPatAssignedTo);
				
				lstStudyPatient.add(studyPatient);
			}
			
		}
		catch(Throwable t){
			t.printStackTrace();
			throw new OperationException();
			
		}
		finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
		return lstStudyPatient;
		
	}

public static Integer getStudyPatientCountByStudyPK(Integer studyPK, ArrayList siteIdList) throws OperationException {
	
	PreparedStatement pstmt = null;
	Connection conn = null;
	Integer count = 0;
	try{
		
		conn = getConnection();
		String siteIds = (String)siteIdList.toString();
		siteIds = siteIds.substring(1, siteIds.length() - 1).replace(", ", ",");

		StringBuilder sbSQL = new StringBuilder();
		sbSQL.append(" Select count(*) countPatOnStudy");
		sbSQL.append("  from erv_studypat_by_visit erv ,  erv_person p where P.pk_person = erv.fk_per ");
		sbSQL.append(" and erv.fk_study = ? ");
        sbSQL.append(" and exists (select * from ER_PATFACILITY fac where fac.fk_per = erv.fk_per  ");
        sbSQL.append(" and fac.fk_site in ( "+siteIds+" ) and fac.patfacility_accessright > 0 ) ");
		pstmt = conn.prepareStatement(sbSQL.toString());
		pstmt.setInt(1, studyPK);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			count = rs.getInt("countPatOnStudy");
		}
		
	}
	catch(Throwable t){
		t.printStackTrace();
		throw new OperationException();
		
	}
	finally {
		try {
			if (pstmt != null)
				pstmt.close();
		} catch (Exception e) {
		}
		try {
			if (conn != null)
				conn.close();
		} catch (Exception e) {
		}

	}
	return count;
	
}

	
	public StudyPatientResults getStudyPatientByStudyPK(Integer studyPK, ArrayList siteIdList, StudyPatientSearch StudyPatSearch,Integer userId,Integer patientManagePrivileges) throws OperationException {
		
		/*if (StudyPatSearch.getSortBy() == null && StudyPatSearch.getSortOrder() == null) {
			StudyPatSearch.setSortBy(StudyPatSearchOrderBy.PK);
			StudyPatSearch.setSortOrder(SortOrder.DESCENDING);
		} else if (StudyPatSearch.getSortOrder() == null) {
			StudyPatSearch.setSortOrder(SortOrder.ASCENDING);
		}*/
		int curPage = StudyPatSearch.getPageNumber();
		if (curPage < 1) {
			curPage = 1;
		}
		pageNumber = curPage;
		long rowsPerPage = Configuration.MOREBROWSERROWS;
		String inputPageSize = String.valueOf(StudyPatSearch.getPageSize());
		if (!StringUtil.isEmpty(inputPageSize)) {
			try {
				rowsPerPage = Long.parseLong(inputPageSize);
			} catch(Exception e) {
				rowsPerPage = Configuration.MOREBROWSERROWS;
			}
		}
		/*if (rowsPerPage > 50L) {
			rowsPerPage = 50L;
		}*/
		String orderBy = "";
		if(StudyPatSearch.getSortBy()!=null)
			orderBy = FilterUtil.sanitizeTextForSQL(StudyPatSearch.getSortBy().toString());
		if (StringUtil.isEmpty(orderBy)) {
			orderBy = DEFAULT_ORDER_BY;
		}else{
			if(orderBy.equals("studyPatId"))
				orderBy="lowerpatstdid";
			else if(orderBy.equals("studyPatFirstName"))
				orderBy="lower(mask_person_fname)";
			else if(orderBy.equals("studyPatLastName"))
				orderBy="lower(mask_person_lname)";
			else if(orderBy.equals("studyPatEnrollingSite.siteName"))
				orderBy="lower(enrollingorg_name)";
			else if(orderBy.equals("studyPatEnrollDate"))
				orderBy="patprot_enroldt_datesort";
			else if(orderBy.equals("studyPatStatus.description"))
				orderBy="lower(ptstdycur_stat)";
			else if(orderBy.equals("studyPatAssignedTo.userLoginName"))
				orderBy="lower(assignedto_name)";
			else if(orderBy.equals("studyPatLastVisit"))
				orderBy="lower(last_visit_name)";
			else if(orderBy.equals("studyPatNextDue"))
				orderBy="next_visit_datesort";
			else
				orderBy="lower(enrollingorg_name)";
		}
		String orderType = "";
		if(StudyPatSearch.getSortOrder()!=null)
			orderType = FilterUtil.sanitizeTextForSQL(StudyPatSearch.getSortOrder().toString());
		if (StringUtil.isEmpty(orderType)) {
			orderType = ASC_STR;
		}
		StringBuilder searchByClause = new StringBuilder();
		if(StudyPatSearch.getSearchBy()!=null && StudyPatSearch.getSearchBy().getSearchByColumn()!=null && StudyPatSearch.getSearchBy().getValue()!=null){
			if(!StringUtil.isEmpty(StudyPatSearch.getSearchBy().getSearchByColumn())){
				String[] searchByCols = StudyPatSearch.getSearchBy().getSearchByColumn().split(",");
				String[] searchByColsVal = StudyPatSearch.getSearchBy().getValue().split(",");
				for(int i=0;i<searchByCols.length;i++){
					if(searchByCols[i].equals("studyPatId"))
						searchByCols[i]="PATPROT_PATSTDID";
					else if(searchByCols[i].equals("studyPatFirstName"))
						searchByCols[i]="mask_person_fname";
					else if(searchByCols[i].equals("studyPatLastName"))
						searchByCols[i]="mask_person_lname";
					else if(searchByCols[i].equals("studyPatEnrollingSite.siteName"))
						searchByCols[i]="enrollingorg_name";
					else if(searchByCols[i].equals("studyPatEnrollDate"))
						searchByCols[i]="patprot_enroldt_datesort";
					else if(searchByCols[i].equals("studyPatStatus.description"))
						searchByCols[i]="ptstdycur_stat";
					else if(searchByCols[i].equals("studyPatAssignedTo.userLoginName"))
						searchByCols[i]="assignedto_name";
					else if(searchByCols[i].equals("studyPatLastVisit"))
						searchByCols[i]="last_visit_name";
					else if(searchByCols[i].equals("studyPatNextDue"))
						searchByCols[i]="next_visit_datesort";
					if(!StringUtil.isEmpty(searchByColsVal[i].trim())){
						if(i==0){
							if("patprot_enroldt_datesort".equals(searchByCols[i]) || "next_visit_datesort".equals(searchByCols[i])){
								if(searchByColsVal[i].indexOf("T")>0)
									searchByClause.append(" Where "+searchByCols[i].toLowerCase()+" = TO_DATE('"+searchByColsVal[i].substring(0, searchByColsVal[i].indexOf("T")).toLowerCase()+"','yyyy-MM-dd')");
								else
									searchByClause.append(" Where "+searchByCols[i].toLowerCase()+" = TO_DATE('"+searchByColsVal[i].toLowerCase()+"','yyyy-MM-dd')");
							}else
								searchByClause.append(" Where lower("+searchByCols[i].toLowerCase()+") like '%"+searchByColsVal[i].toLowerCase()+"%'");
							}
						else{
							if("patprot_enroldt_datesort".equals(searchByCols[i]) || "next_visit_datesort".equals(searchByCols[i])){
								if(searchByColsVal[i].indexOf("T")>0)
									searchByClause.append(" And "+searchByCols[i].toLowerCase()+" = TO_DATE('"+searchByColsVal[i].substring(0, searchByColsVal[i].indexOf("T")).toLowerCase()+"','yyyy-MM-dd')");
								else
									searchByClause.append(" And "+searchByCols[i].toLowerCase()+" = TO_DATE('"+searchByColsVal[i].toLowerCase()+"','yyyy-MM-dd')");
							}else
								searchByClause.append(" And lower("+searchByCols[i].toLowerCase()+") like '%"+searchByColsVal[i].toLowerCase()+"%'");
						}
					}
				}	
			}
		}
		ArrayList<StudyPatient> lstStudyPatient = new ArrayList<StudyPatient>();
		String siteIds = (String)siteIdList.toString();
		siteIds = siteIds.substring(1, siteIds.length() - 1).replace(", ", ",");
		StringBuilder sbSQL = new StringBuilder();
		sbSQL.append(" Select * from (Select distinct p.rowid as rowcount,erv.fk_per, p.person_lname as mask_person_lname, p.person_fname as mask_person_fname, "); 
		sbSQL.append(" erv.per_code,");
		sbSQL.append(" erv.PATSTUDYSTAT_SUBTYPE,erv.PATSTUDYSTAT_DESC,erv.PER_SITE, ");
		sbSQL.append(" (select site_name from ER_SITE where pk_site = erv.PER_SITE) enrollingorg_name,");  
		sbSQL.append(" erv.patprot_enroldt as patprot_enroldt_datesort,"); 
		sbSQL.append(" erv.last_visit_name,erv.cur_visit, erv.cur_visit_date, erv.next_visit as next_visit_datesort, ");
		sbSQL.append(" lower(p.person_lname || p.person_fname) mask_patnamelower,lower(erv.PATPROT_PATSTDID) lowerpatstdid,erv.PATPROT_PATSTDID,");
		sbSQL.append(" assignedto_name,physician_name,enrolledby_name,treatingorg_name,erv.pk_patstudystat lateststatid,erv.PK_PATPROT, codelst_desc ptstdycur_stat, codelst_subtyp ptstdcurstat_subtype, ps.pk_patstudystat currentstatid,pkg_user.f_chk_studyright_using_pat(erv.fk_per,erv.fk_study,"+userId+") accRight ");
		sbSQL.append("  from erv_studypat_by_visit erv ,  erv_person p , er_patstudystat ps, er_codelst where P.pk_person = erv.fk_per ");
		sbSQL.append(" and erv.fk_study = "+studyPK);
		sbSQL.append(" and ps.fk_per = erv.fk_per  and ps.fk_study =erv.fk_study  and current_stat=1  and pk_codelst= ps.fk_codelst_stat and codelst_type ='patStatus' and codelst_custom_col='browser' ");
        sbSQL.append(" and exists (select * from ER_PATFACILITY fac where fac.fk_per = erv.fk_per  ");
        sbSQL.append(" and fac.fk_site in ( "+siteIds+" ) and fac.patfacility_accessright > 0 )) ");
        sbSQL.append(searchByClause.toString());
		String masterSql = sbSQL.toString();
		String countSql = "select count(*) from  ( " + sbSQL.toString() + ")";
		long totalPages = Configuration.PAGEPERBROWSER;

		BrowserRows br = new BrowserRows();
		br.getPageRows(curPage, rowsPerPage, masterSql, totalPages, countSql, orderBy, orderType);
		
		long rowsReturned = br.getRowReturned();
		StudyPatient studyPatient = null;
		for (int iX = 1; iX <= rowsReturned; iX++) {
			studyPatient =  new StudyPatient();
			PatientIdentifier patientIdentifier = new PatientIdentifier();
			//create or get person object map for studyPatient
			ObjectMapService objectMapService = ServicesUtil.getObjectMapService();
			int fkPer=StringUtil.stringToNum(br.getBValues(iX, COL_FK_PER));
			ObjectMap obj = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, fkPer);
			String strOID = obj.getOID();
			
			patientIdentifier.setPatientId(br.getBValues(iX, COL_PER_CODE));
			patientIdentifier.setOID(strOID);
			patientIdentifier.setPK(fkPer);
			studyPatient.setPatientIdentifier(patientIdentifier);
			studyPatient.setStudyPatRights(patientManagePrivileges);
			Integer orgRight=0;
			if(StringUtil.stringToInteger(br.getBValues(iX, COL_ORGRIGHTS))>0)
				orgRight=7;
			studyPatient.setOrgRights(orgRight);
			
			
			studyPatient.setStudyPatId(br.getBValues(iX, COL_PATSTDID));
			studyPatient.setStudyPatFirstName(br.getBValues(iX, COL_FIRSTNAME));
			studyPatient.setStudyPatLastName(br.getBValues(iX, COL_LASTNAME));
			studyPatient.setStudyPatLastVisit(br.getBValues(iX, COL_LVISIT_NAME));
			if(br.getBValues(iX, COL_NVISIT_DATE)!=null)
				studyPatient.setStudyPatNextDue(DateUtil.stringToDate(br.getBValues(iX, COL_NVISIT_DATE),"yyyy-MM-dd"));
			PatientStudyStatusIdentifier studyPatStatId = new PatientStudyStatusIdentifier();
			obj = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATSTUDYSTAT, StringUtil.stringToNum(br.getBValues(iX, COL_CURSTATID)));
			strOID = obj.getOID();
			studyPatStatId.setOID(strOID);
			studyPatStatId.setPK(StringUtil.stringToNum(br.getBValues(iX, COL_CURSTATID)));
			studyPatient.setStudyPatStatId(studyPatStatId);
			PatientProtIdentifier patProtId = new PatientProtIdentifier();
			obj = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATPROT, StringUtil.stringToNum(br.getBValues(iX, COL_PK_PATPROT)));
			strOID = obj.getOID();
			patProtId.setOID(strOID);
			patProtId.setPK(StringUtil.stringToNum(br.getBValues(iX, COL_PK_PATPROT)));
			studyPatient.setPatprotId(patProtId);
			if(br.getBValues(iX, COL_ENROLLING_DATE)!=null)
				studyPatient.setStudyPatEnrollDate(DateUtil.stringToDate(br.getBValues(iX, COL_ENROLLING_DATE),"yyyy-MM-dd"));
			Code studyPatStatus = new Code(CodeCache.CODE_TYPE_PATPROT_STATUS, br.getBValues(iX, COL_CURPATSTDSTAT_SUBTYP), br.getBValues(iX, COL_CURPATSTDSTAT_DESC));
			studyPatient.setStudyPatStatus(studyPatStatus);
			OrganizationIdentifier studyPatEnrollingSite = new OrganizationIdentifier(br.getBValues(iX, COL_ENROLLINGORG_NAME), ""); /// for UCSD SPA done like that
			studyPatient.setStudyPatEnrollingSite(studyPatEnrollingSite);
			UserIdentifier studyPatAssignedTo = new UserIdentifier(br.getBValues(iX, COL_ASSIGNTO_NAME));  // for UCSD SPA done like that
			studyPatient.setStudyPatAssignedTo(studyPatAssignedTo);
			
			lstStudyPatient.add(studyPatient);
		}
		totalCount = br.getTotalRows();
		pageSize = rowsReturned;
		StudyPatientResults studyPatients = new StudyPatientResults();
		studyPatients.setStudyPatient(lstStudyPatient);
		studyPatients.setPageNumber(pageNumber);
		studyPatients.setPageSize(pageSize);
		studyPatients.setTotalCount(totalCount);
		//studyPatients.setStudyPatRights(patientManagePrivileges);
		return studyPatients;
		
	}
	
	
	public static List<Integer> getAllPatients(Integer userID) throws OperationException
	{
		PreparedStatement stmt = null ;
		ResultSet rs = null; 
		Connection conn = null; 
		ArrayList<Integer> patientList = new ArrayList<Integer>(); 
		
		try
		{
			StringBuffer sql = new StringBuffer(); 
			sql.append("select fk_per from ER_PATFACILITY fac, er_usersite usr" ); 
			sql.append(" where usersite_right>= ? AND usr.fk_site = fac.fk_site"); 
			sql.append(" and fk_user = ?  AND fac.patfacility_accessright > ?"); 
			
			conn = getConnection(); 

			stmt = conn.prepareStatement(sql.toString()); 
			stmt.setInt(1, 4); 
			stmt.setInt(2, userID);
			stmt.setInt(3, 0); 

			rs = stmt.executeQuery(); 

			while(rs.next())
			{
				patientList.add(rs.getInt(1)); 
			}

		}catch(SQLException sqe)
		{
			sqe.printStackTrace(); 
			throw new OperationException(); 
		}
		finally {
			try {
				if (rs != null)
					rs.close();
			} catch (Exception e) {
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
		
		return patientList; 
	}

	public String getPatientStatusHistoryString(){
		StringBuilder sbSQL = new StringBuilder();
		sbSQL.append("select PK_PATSTUDYSTAT, FK_CODELST_STAT, PATSTUDYSTAT_DATE, ");
		sbSQL.append(" PATSTUDYSTAT_ENDT, PATSTUDYSTAT_NOTE, er_codelst.codelst_desc, ");
		sbSQL.append(" patstudystat_reason, CURRENT_STAT ");
		sbSQL.append(" from er_patstudystat,  er_codelst  ");
		sbSQL.append(" where fk_per = ? and fk_study = ? and FK_CODELST_STAT = pk_codelst order by  PATSTUDYSTAT_DATE DESC,PK_PATSTUDYSTAT DESC  ");
		String masterSql = sbSQL.toString();
		return masterSql;
	}
	
	
	private static String getCurrentPatientStatusSQL(){
		StringBuilder sbSQL = new StringBuilder();
		sbSQL.append("select PK_PATSTUDYSTAT");
		sbSQL.append(" from er_patstudystat ");
		//sbSQL.append(" where fk_per = ? and fk_study = ? and current_stat = 1 and PATSTUDYSTAT_ENDT is null");		
		sbSQL.append(" where fk_per = ? and fk_study = ? and current_stat = 1 ");		
		return sbSQL.toString();
	}

	
	public List<StudyPatientStatus> getPatStatusHistory(Integer patPk, Integer studyPk, Integer accId) throws OperationException {
		
		PreparedStatement pstmt = null;
		Connection conn = null;
		ArrayList<StudyPatientStatus> lstPatientStat = new ArrayList<StudyPatientStatus>();
		
		try{
			boolean isCurrentStatStr = false;
			conn = getConnection();
			CodeCache codeCache = CodeCache.getInstance();
			StudyPatientStatus studyPatientStatus = null;
			pstmt = conn.prepareStatement(getPatientStatusHistoryString());
			pstmt.setInt(1, patPk);
			pstmt.setInt(2, studyPk);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				studyPatientStatus =  new StudyPatientStatus();
				
				ObjectMapService  mapService = ServicesUtil.getObjectMapService();
				PatientStudyStatusIdentifier studyPatStatId = new PatientStudyStatusIdentifier(); 
				ObjectMap map1 = mapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATSTUDYSTAT, rs.getInt("PK_PATSTUDYSTAT"));
				studyPatStatId.setOID(map1.getOID());
				studyPatStatId.setPK(rs.getInt("PK_PATSTUDYSTAT"));
				studyPatientStatus.setStudyPatStatId(studyPatStatId); 
				
				Code studyStatusCode = 
					codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATPROT_STATUS, 
							rs.getInt("FK_CODELST_STAT"),
							accId);
				
				studyPatientStatus.setStudyPatStatus(studyStatusCode);
				studyPatientStatus.setStatusDate(rs.getDate(3));
				studyPatientStatus.setStatusNote(rs.getString(5));

				Code statusReasonCode = 
					codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_FOLLOWUP, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_ENROLL_APPR, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_ENROLL_PENDING, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_ENROLL_DENIED, 
							rs.getInt("patstudystat_reason"),
							accId);
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_OFFSTUDY, 
							rs.getInt("patstudystat_reason"),
							accId);
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_OFFTREAT, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				//
				if(statusReasonCode == null) 
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_INFCONSENT, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_BEGSTUDYACT, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_SCRFAIL, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_ENROLLED, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_ENDBILLING, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_COMPSTUDY, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_ACTIVESTUDY, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_WITHDRAWN, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_NOTMEETELIGIBLE, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_PHYSDICRETION, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				if(statusReasonCode == null)
					statusReasonCode = codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_PATIENT_STUDY_STATUS_REASON_WITHDREWCON, 
							rs.getInt("patstudystat_reason"),
							accId);
				
				
				//
				
				
					
				studyPatientStatus.setStatusReason(statusReasonCode);
				
				isCurrentStatStr = rs.getInt("CURRENT_STAT") == 1 ? true : false;
				studyPatientStatus.setCurrentStatus(isCurrentStatStr);
				
				lstPatientStat.add(studyPatientStatus);
			}
		}
		catch(Throwable t){
			t.printStackTrace();
			throw new OperationException();
			
		}
		finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
		return lstPatientStat;
		
	}
	
public static int getCurrentPatientStatus(Integer patPk, Integer studyPk) throws OperationException {
		
		PreparedStatement pstmt = null;
		Connection conn = null;
		int currentStudyPatStatus = 0;
		
		try{
			boolean isCurrentStatStr = false;
			conn = getConnection();
			CodeCache codeCache = CodeCache.getInstance();
			StudyPatientStatus studyPatientStatus = null;
			pstmt = conn.prepareStatement(getCurrentPatientStatusSQL());
			pstmt.setInt(1, patPk);
			pstmt.setInt(2, studyPk);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
			   currentStudyPatStatus = rs.getInt(1); 
			}
				
		}
		catch(Throwable t){
			t.printStackTrace();
			throw new OperationException();
			
		}
		finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
		return currentStudyPatStatus;
		
	}



public static String[] updateMStudyPatientStatus(List<String> updateSQL,List<String> pKSQL) throws OperationException,SQLException
{
	// TODO Auto-generated method stub
	
	Connection conn = null;
	CallableStatement csmt = null;
	
	try
	{
		conn = getConnection();
		conn.setAutoCommit(false);
			
		String [] updateString = new String[updateSQL.size()];
		updateString = updateSQL.toArray(updateString);
				
		String [] pk_sql = new String[pKSQL.size()];
		pk_sql = pKSQL.toArray(pk_sql);
		
       ArrayDescriptor des = ArrayDescriptor.createDescriptor("ERES.ARRAY_STRING", conn);
       
       ARRAY passing_array = new ARRAY(des,conn,updateString);
       ARRAY pk_sql_to_pass = new ARRAY(des,conn,pk_sql);
         
        csmt = conn.prepareCall("call ERES.SP_UPDATE_MUL_STDY_PAT_STATUS(?,?,?)");
     // Passing an arrays to the procedure -
        csmt.setArray(1, passing_array);
        csmt.setArray(2, pk_sql_to_pass);
        csmt.registerOutParameter(3,OracleTypes.ARRAY,"ERES.ARRAY_STRING");
        csmt.execute();
        
        // Retrieving array from the result set of the procedure after execution -
        ARRAY arr1 = ((OracleCallableStatement)csmt).getARRAY(3);
        String[] receivedArray = (String[])(arr1.getArray());

     	return receivedArray;
	}
	catch(SQLException se){
		se.printStackTrace();
		//conn.rollback();
		throw new OperationException(se.getMessage());
		
	}
	finally {
		try {
			if (csmt != null)
				csmt.close();
		} catch (Exception e) {
		}
		try {
			if (conn != null)
				conn.close();
		} catch (Exception e) {
		}

	}
}

private static final String STR_ZERO = "0";

private String executeConstructedSqls(Connection conn, String person, 
		String patProt, String patStudyStat) {
	PreparedStatement personStmt = null;
	int updateCount = 0;
	try {
		personStmt = conn.prepareStatement(person);
		updateCount = personStmt.executeUpdate();
	} catch (Exception e) {
		logger.fatal("In StudyPatientDAO.executeConstructedSqls, this personStmt failed to run: "+person+" due to "+e);
		updateCount = -1;
	} finally {
		try {
			personStmt.close();
		} catch(Exception e) {}
	}
	if (updateCount < 0) {
		return STR_ZERO;
	}
	
	PreparedStatement patProtStmt = null;
	try {
		patProtStmt = conn.prepareStatement(patProt);
		updateCount = patProtStmt.executeUpdate();
	} catch (Exception e) {
		logger.fatal("In StudyPatientDAO.executeConstructedSqls, this patProtStmt failed to run: "+patProt+" due to "+e);
		updateCount = -1;
	} finally {
		try {
			patProtStmt.close();
		} catch(Exception e) {}
	}
	if (updateCount < 0) {
		return STR_ZERO;
	}
	
	int nextPk = -1;
	PreparedStatement patStudyStatPkStmt = null;
	try {
		patStudyStatPkStmt = conn.prepareStatement(" select SEQ_ER_PATSTUDYSTAT.NEXTVAL from dual ");
		ResultSet rs = patStudyStatPkStmt.executeQuery();
		while(rs.next()) {
			nextPk = rs.getInt(1);
		}
	} catch (Exception e) {
		logger.fatal("In StudyPatientDAO.executeConstructedSqls, could not get SEQ_ER_PATSTUDYSTAT.NEXTVAL:"+e);
		nextPk = -1;
	}
	if (nextPk < 0) {
		return STR_ZERO;
	}
	
	PreparedStatement patStudyStatStmt = null;
	try {
		patStudyStatStmt = conn.prepareStatement(patStudyStat.replace(":1", " ? "));
		patStudyStatStmt.setInt(1, nextPk);
		updateCount = patStudyStatStmt.executeUpdate();
	} catch (Exception e) {
		logger.fatal("In StudyPatientDAO.executeConstructedSqls, this patStudyStatStmt failed to run: "+patStudyStat+" due to "+e);
		updateCount = -1;
	} finally {
		try {
			patStudyStatStmt.close();
		} catch(Exception e) {}
	}
	if (updateCount < 0) {
		return STR_ZERO;
	}
	return String.valueOf(nextPk);
}

private static final String findEnrolledInCodelstSql = " select PK_CODELST from ER_CODELST where "
		+"CODELST_TYPE = 'patStatus' and CODELST_SUBTYP = 'enrolled' ";
private static final String findPreviouslyEnrolledStatusSql = " select PK_PATSTUDYSTAT from ERES.ER_PATSTUDYSTAT where "
		+" FK_PER = ? and FK_STUDY = ? and FK_CODELST_STAT = ? and PK_PATSTUDYSTAT <> ? ";
private static final String findOneOfPostEnrolledStatusesInCodelst = "select CODELST_SUBTYP from ER_CODELST where "
		+ " CODELST_TYPE = 'patStatus' and  CODELST_SUBTYP in ('active', 'followup','offtreat','offstudy','lockdown')"
		+ " and PK_CODELST = ? ";

private String handlePreviouslyEnrolledStatus(Connection conn, String newlyCreatedPk, String p_fkArr) {
	String[] inputParts = p_fkArr.split(",");
	int pkCodelstEnrolled = 0;
	PreparedStatement findEnrolledStmt = null;
	try {
		findEnrolledStmt =  conn.prepareStatement(findEnrolledInCodelstSql);
		ResultSet rs = findEnrolledStmt.executeQuery();
		while(rs.next()) {
			pkCodelstEnrolled = rs.getInt(1);
		}
	} catch(Exception e) {
		pkCodelstEnrolled = 0;
	} finally {
		try { findEnrolledStmt.close(); } catch(Exception e) {}
	}
	
	if (pkCodelstEnrolled < 1) {
		return "The codelist for the patStatus type is defective.";
	}
	
	PreparedStatement findPreviouslyEnrolledStmt = null;
	int pkPatStudyStatOfPreviouslyEnrolled = 0;
	try {
		findPreviouslyEnrolledStmt = conn.prepareStatement(findPreviouslyEnrolledStatusSql);
		findPreviouslyEnrolledStmt.setInt(1, StringUtil.stringToNum(inputParts[1]));
		findPreviouslyEnrolledStmt.setInt(2, StringUtil.stringToNum(inputParts[2]));
		findPreviouslyEnrolledStmt.setInt(3, pkCodelstEnrolled);
		findPreviouslyEnrolledStmt.setInt(4, StringUtil.stringToNum(newlyCreatedPk));
		ResultSet rs = findPreviouslyEnrolledStmt.executeQuery();
		while(rs.next()) {
			pkPatStudyStatOfPreviouslyEnrolled = rs.getInt(1);
		}
	} catch(Exception e) {
		logger.fatal("In StudyPatientDAO.handlePreviouslyEnrolledStatus, faield to run findPreviouslyEnrolledStatusSql due to "+e);
		pkPatStudyStatOfPreviouslyEnrolled = 0;
	} finally {
		try { findPreviouslyEnrolledStmt.close(); } catch(Exception e) {}
	}
	
	// If the newly created status is 'enrolled', it must not have a previous 'enrolled' status
	if (StringUtil.stringToNum(inputParts[0]) == pkCodelstEnrolled
			&& pkPatStudyStatOfPreviouslyEnrolled > 0) {
		return "Cannot add 'enrolled' patient study status because a previous 'enrolled' status already exists for fkPer="
			+inputParts[1]+" and fkStudy="+inputParts[2];
	}
	
	// If the newly created status is one of ('active', 'followup', 'offtreat', 'offstudy', 'lockdown'),
	// then it must have a previously enrolled status
	PreparedStatement findOneOfPostEnrolledStmt = null;
	String subTypeOfOneOfPostEnrolled = null;
	try {
		findOneOfPostEnrolledStmt = conn.prepareStatement(findOneOfPostEnrolledStatusesInCodelst);
		findOneOfPostEnrolledStmt.setInt(1, StringUtil.stringToNum(inputParts[0]));
		ResultSet rs = findOneOfPostEnrolledStmt.executeQuery();
		while(rs.next()) {
			subTypeOfOneOfPostEnrolled = rs.getString(1);
		}
	} catch(Exception e) {
		subTypeOfOneOfPostEnrolled = null;
	} finally {
		try { findOneOfPostEnrolledStmt.close(); } catch(Exception e) {}
	}
	if (subTypeOfOneOfPostEnrolled != null && pkPatStudyStatOfPreviouslyEnrolled < 1) {
		return "Cannot add '"+subTypeOfOneOfPostEnrolled+"' patient study status for fkPer="
				+inputParts[1]+" and fkStudy="+inputParts[2]+" because it does not have a previous 'enrolled' status";
	}
	
	return newlyCreatedPk;
}

private static final String checkCurrentStatusSql = " select CURRENT_STAT from ER_PATSTUDYSTAT where PK_PATSTUDYSTAT = ? ";

private String handleCurrentStatus(Connection conn, String newlyCreatedPk, String p_fkArr, int userId) {
	String[] inputParts = p_fkArr.split(",");
	String retString = null;
	PreparedStatement checkCurrentStatusStmt = null;
	int isCurrentStatus = 0;
	try {
		checkCurrentStatusStmt = conn.prepareStatement(checkCurrentStatusSql);
		checkCurrentStatusStmt.setInt(1, StringUtil.stringToNum(newlyCreatedPk));
		ResultSet rs = checkCurrentStatusStmt.executeQuery();
		while(rs.next()) {
			isCurrentStatus = rs.getInt(1);
		}
	} catch(Exception e) {
		logger.fatal("In StudyPatientDAO.handleCurrentStatus, checkCurrentStatusStmt failed:"+e);
		isCurrentStatus = 0;
	} finally {
		try { checkCurrentStatusStmt.close(); } catch(Exception e) {}
	}
	
	if (isCurrentStatus < 1) {
		return newlyCreatedPk; // No need to update anything for current status; just return
	}
	
	// The newly created status is set as the "current status" => change the end date 
	// and last modified date and last modified by of the previous "current status"
	CallableStatement cstmt = null;
	int outCstmt = 0;
	try {
		cstmt = conn.prepareCall("{call SP_UPDATE_STATDT_PATSTUDYSTAT(?,?,?,?,?)}");
		cstmt.setInt(1, StringUtil.stringToNum(newlyCreatedPk));
		cstmt.setInt(2, StringUtil.stringToNum(inputParts[1]));
		cstmt.setInt(3, StringUtil.stringToNum(inputParts[2]));
		cstmt.setInt(4, userId);
		cstmt.registerOutParameter(5, OracleTypes.INTEGER);
		cstmt.execute();
		cstmt.getInt(5);
	} catch(Exception e) {
		logger.fatal("In StudyPatientDAO.handleCurrentStatus, SP_UPDATE_STATDT_PATSTUDYSTAT faied:"+e);
	} finally {
		try { cstmt.close(); } catch(Exception e) {}
	}
	if (outCstmt < 0) {
		logger.warn("In StudyPatientDAO.handleCurrentStatus, SP_UPDATE_STATDT_PATSTUDYSTAT returned:"+outCstmt);
	}
	
	return newlyCreatedPk;
}


public String[] createMStudyPatientStatus(String[] person, String[] patProt, String[] patStudyStat, String[] p_fkArr, int userId) throws OperationException
{
	String[] retStrings = new String[patStudyStat.length];
	if (patStudyStat == null || patStudyStat.length < 1) {
		return null;
	}
	
	// Get connection unset auto-commit
	Connection conn = null;
	try {
		conn = getConnection();
		conn.setAutoCommit(false);
	} catch (Exception e) {
		logger.fatal("In StudyPatientDAO.createMStudyPatientStatus, failed to get connection:"+e);
		return null;
	}
	
	// Start of main loop
	for (int iX = 0; iX < patStudyStat.length; iX++) {
		// Execute constructed SQLs
		retStrings[iX] = this.executeConstructedSqls(conn, person[iX], patProt[iX], patStudyStat[iX]);
		// retStrings[iX] = "388"; // test data
		String[] inputParts = p_fkArr[iX].split(",");
		
		// If last execution failed, rollback and skip
		if (STR_ZERO.equals(retStrings[iX])) {
			try {
				conn.rollback();
			} catch(Exception e) {
				logger.fatal("In StudyPatientDAO.createMStudyPatientStatus, failed to rollback for pkPer="
					+inputParts[1]+" and pkStudy="+inputParts[2]+" and status code="+inputParts[0]+" due to "+e);
				break;
			}
			retStrings[iX] += "; no action taken on record #"+(iX+1)+".";
			continue;
		}
		
		// Handle previously enrolled status
		retStrings[iX] = this.handlePreviouslyEnrolledStatus(conn, retStrings[iX], p_fkArr[iX]);
		
		// If last execution failed, rollback and skip
		if (StringUtil.stringToNum(retStrings[iX]) < 1) {
			try {
				conn.rollback();
			} catch(Exception e) {
				logger.fatal("In StudyPatientDAO.createMStudyPatientStatus, failed to rollback for pkPer="
					+inputParts[1]+" and pkStudy="+inputParts[2]+" and status code="+inputParts[0]+" due to "+e);
				break;
			}
			retStrings[iX] += "; no action taken on record #"+(iX+1)+".";
			continue;
		}
		
		// Handle current status
		retStrings[iX] = this.handleCurrentStatus(conn, retStrings[iX], p_fkArr[iX], userId);

		// If last execution failed, rollback and skip
		if (StringUtil.stringToNum(retStrings[iX]) < 1) {
			try {
				conn.rollback();
			} catch(Exception e) {
				logger.fatal("In StudyPatientDAO.createMStudyPatientStatus, failed to rollback for pkPer="
					+inputParts[1]+" and pkStudy="+inputParts[2]+" and status code="+inputParts[0]+" due to "+e);
				break;
			}
			retStrings[iX] += "; no action taken on record #"+(iX+1)+".";
			continue;
		}

		// Commit after each iteration
		try {
			conn.commit();
		} catch(Exception e) {
			logger.fatal("In StudyPatientDAO.createMStudyPatientStatus, failed to commit for pkPer="
					+inputParts[1]+" and pkStudy="+inputParts[2]+" and status code="+inputParts[0]+" due to "+e);
		}
	}
	// End of main loop
	
	// Close connection
	try {
		if (conn != null) { conn.close(); }
	} catch(Exception e) {}
	
	return retStrings;
}

public String[] createMStudyPatientStatus_old(String[] person, String[] patProt, String[] patStudyStat, String[] p_fkArr) throws OperationException
{
	Array ret ;
    CallableStatement cstmt = null;
    Connection conn = null;

    try {
        conn = getConnection();
   
        ArrayDescriptor adPerson = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
        ARRAY personSqls = new ARRAY(adPerson, conn, person);
        
        ArrayDescriptor adPatProt = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
        ARRAY patProtSqls = new ARRAY(adPatProt, conn, patProt);
        
        ArrayDescriptor adPatStudyStat = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
        ARRAY patStudyStatSqls = new ARRAY(adPatStudyStat, conn, patStudyStat);
        
        ArrayDescriptor adFkArr = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
        ARRAY fkArr = new ARRAY(adFkArr, conn, p_fkArr);
        
        cstmt = conn.prepareCall("{call SP_CREATE_MUL_PAT_STUDY_STATUS(?,?,?,?,?)}");
        
        cstmt.setArray(1, personSqls);
        cstmt.setArray(2, patProtSqls);
        cstmt.setArray(3, patStudyStatSqls);
        cstmt.setArray(4, fkArr);
        cstmt.registerOutParameter(5,OracleTypes.ARRAY,"ERES.ARRAY_STRING");
        cstmt.execute();
        ret = ((OracleCallableStatement)cstmt).getARRAY(5);
        //String[] receivedArray = (String[])(arr1.getArray());
        String[] values = (String[])ret.getArray();
        

        return values;

    } catch (Throwable t) {
    	t.printStackTrace();
		throw new OperationException();
    } finally {
        try {
            if (cstmt != null)
                cstmt.close();
        } catch (Exception e) {
        }
        try {
            if (conn != null)
                conn.close();
        } catch (Exception e) {
        }
    }
    
}
//Bug Fix : 16693
public static boolean ifStudyExists(int studyPK) throws OperationException{
	
	String sql = "select count(*) as Count from er_study where pk_study = ?";
	PreparedStatement pstmt  = null;
	Connection conn =  null;
	boolean flag = false;
	try{
		conn = getConnection();
		pstmt = conn.prepareStatement(sql);
    	pstmt.setInt(1, studyPK);
    	
    	ResultSet rs = pstmt.executeQuery();
    			
    	while(rs.next()){
    		if(rs.getInt("Count") ==1)
			   flag = true;
			else 
			   flag =  false;
    	}
    	return flag;
	}catch(Throwable t){
		t.printStackTrace();
		throw new OperationException();
	}
	
	finally{
		try{
			if(pstmt!=null){
				pstmt.close();
			}
		}catch(Exception e){
			
		}
		try{
			if(conn!=null){
				conn.close();
			}
		}catch(Exception e){
			
		}
	}
}


}
