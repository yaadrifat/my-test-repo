/**
 * 
 */
package com.velos.services;

/**
 * @author dylan
 *
 */
public enum IssueTypes { 
	  /**
	   * Multiple objects were found for a request that expected a single object.
	  */
	  MULTIPLE_OBJECTS_FOUND("MULTIPLE_OBJECTS_FOUND", "Multiple objects were found for a request that expected a single object."),
	  
	  /**
	   * A code was not found in the database
	   */
	  CODE_NOT_FOUND("CODE_NOT_FOUND", "A code was not found in the database"),
	  
	  /**
	   * Data validation problem.
	   */
	  DATA_VALIDATION("DATA_VALIDATION", "Data validation problem."),
	  
	  /**
	   * This user already exists.
	   */
	  DUPLICATE_USER("DUPLICATE_USER", "This user already exists."),
	  
	  /**
	   * Invaild Sort By Option
	   */
	  INVALID_SORT_BY_OPTION("INVALID_SORT_BY_OPTION","Invalid sort by option"),
	  
	  /**
	   * Invaild Order By Option
	   */
	  INVALID_ORDER_BY_OPTION("INVALID_ORDER_BY_OPTION","Invalid sort by option"),
	  
	  /**
	   * Invalid Site of Service 
	   */
	  INVALID_SITE_OF_SERVICE("INVALID_SITE_OF_SERVICE","Invalid site of service"),
	  
	  /**
	   * Invalid Facility ID
	   */
	  INVALID_FACILITY_ID("INVALID_FACILITY_ID","Invalid Facility ID"),
	  
	  /**
	   * Invalid Event Category
	   */
	  INVALID_EVENT_CATEGORY("INVALID_EVENT_CATEGORY","Invalid Event Category"),
	  
	  /**
	   * Event Category Does Not Exist
	   */
	  EVENT_CATEGORY_DOES_NOT_EXIST("EVENT_CATEGORY_DOES_NOT_EXIST","Event Category Does Not Exist"),
	  
	  /**
	   * Schedule not found
	   */
	  SCHEDULE_NOT_FOUND("SCHEDULE_NOT_FOUND", "Schedule not found"),

	  /**
	   * Study author not found
	   */
	  STUDY_AUTHOR_NOT_FOUND("STUDY_AUTHOR_NOT_FOUND", "Study author not found"),
	  
	  /**
	   * Study Principal Investigator not found
	   */
	  STUDY_PRINCIPAL_INVESTIGATOR_NOT_FOUND("STUDY_PRINCIPAL_INVESTIGATOR_NOT_FOUND", "Study Principal Investigator not found"),
	  
	  /**
	   * Study Coordinator not found
	   */
	  STUDY_COORDINATOR_NOT_FOUND("STUDY_COORDINATOR_NOT_FOUND", "Study Coordinator not found"),
	  
	  /**
	   * The study number already exists
	   */
	  STUDY_NUMBER_EXISTS("STUDY_NUMBER_EXISTS", "The study number already exists"),
	  
	  /**
	   * The study number may not null
	   */
	  STUDY_NUMBER_NOT_FOUND("STUDY_NUMBER_NOT_FOUND", "The study number may not null"),
	  
	  /**
	   * The study number may not null
	   */
	  STUDY_NUMBER_SIZE_ISSUE("STUDY_NUMBER_SIZE_ISSUE", "Study Number size must be between 1 and 100"),
	  
	  /**
	   * The Study could not be found
	   */
	  STUDY_NOT_FOUND("STUDY_NOT_FOUND", "The Study could not be found"),
	  
	  /**
	   * The Study More Detail could not  be found
	   */
	  
	  KEY_NOT_FOUND("KEY_NOT_FOUND","Key could not be found to this study"),
	  
	  /**
	   * The Study Organization could not be found
	   */
	  STUDY_ORG_NOT_FOUND("STUDY_ORG_NOT_FOUND", "The Study Organization could not be found"),
	  
	  /**
	   * Error occurred removing a study organization
	   */
	  STUDY_ERROR_REMOVE_ORGANIZATION("STUDY_ERROR_REMOVE_ORGANIZATION", "An error occured removing a study organization"),
	  
	  /**
	   * Error occurred removing a study organization
	   */
	  STUDY_ERROR_REMOVE_ORGANIZATION_CURRENT_STUDYSTATUS("STUDY_ERROR_REMOVE_ORGANIZATION_CURRENT_STUDYSTATUS","An error occured removing a study organization"),
	  
	  /**
	   * Error occurred removing a study organization
	   */
	  STUDY_ERROR_REMOVE_ORGANIZATION_ENROLLED_PATIENT("STUDY_ERROR_REMOVE_ORGANIZATION_ENROLLED_PATIENT","An error occured removing a study organization"),
	  
	  /**
	   * Error occurred creating a study organization
	   */
	  STUDY_ERROR_CREATE_ORGANIZATION("STUDY_ERROR_CREATE_ORGANIZATION", "An error occured creating a study organization"),
	  
	  /**
	   * Error occurred updating a study organization
	   */
	  STUDY_ERROR_UPDATE_ORGANIZATION("STUDY_ERROR_UPDATE_ORGANIZATION", "An  error occured updating a study organization"),
	  
	  /**
	   * Error occurred creating a study status.
	   */
	  STUDY_ERROR_CREATE_STATUS("STUDY_ERROR_CREATE_STATUS", "Error occured creating a study status."),
	  
	  /**
	   * Error occurred updating a study status.
	   */
	  STUDY_ERROR_UPDATE_STATUS("STUDY_ERROR_UPDATE_STATUS", "Error occured updating a study status."),
	  
	  /**
	   * Error occurred creating a study team member.
	   */
	  STUDY_ERROR_CREATE_TEAM_MEMBER("STUDY_ERROR_CREATE_TEAM_MEMBER", "Error occured creating a study team member."),
	  
	  /**
	   * Error occurred updating a study team member.
	   */
	  STUDY_ERROR_UPDATE_TEAM_MEMBER("STUDY_ERROR_UPDATE_TEAM_MEMBER", "Error occured updating a study team member."),
	  
	  /**
	   * Error occurred removing a study team member.
	   */
	  STUDY_ERROR_REMOVE_TEAM_MEMBER("STUDY_ERROR_REMOVE_TEAM_MEMBER", "Error occured removing a study team member."),
	  
	  /**
	   * Data Manager can not be removed from Study Team.
	   */
	  STUDY_REMOVE_DATA_MANAGER("STUDY_REMOVE_DATA_MANAGER", "Data Manager can not be removed from Study Team."),
	  
	  /**
	   * Error occurred creating a More Study Details value.
	   */
	  STUDY_ERROR_CREATE_MSD("STUDY_ERROR_CREATE_MSD", "Error occured creating a More Study Details value."),
	  
	  /**
	   * Error occurred updating a More Study Details value.
	   */
	  STUDY_ERROR_UPDATE_MSD("STUDY_ERROR_UPDATE_MSD", "Error occured updating a More Study Details value."),
	  
	  /**
	   * Error occurred creating a More Patient Details value.
	   */
	  PATIENT_ERROR_CREATE_MSD("PATIENT_ERROR_CREATE_MSD", "Error occured creating a More Patient Details value."),
	  
	  /**
	   * Error occurred updating a More Patient Details value.
	   */
	  PATIENT_ERROR_UPDATE_MSD("PATIENT_ERROR_UPDATE_MSD", "Error occured updating a More Patient Details value."),
	  /**
	   * Error occurred creating a More Study Details value.
	   */
	  ERROR_CREATE_MD("ERROR_CREATE_MD", "Error occured creating a More Details value."),
	  
	  /**
	   * Error occurred updating a More Study Details value.
	   */
	  ERROR_UPDATE_MD("ERROR_UPDATE_MD", "Error occured updating a More Details value."),
	  
	  /**
	   * Request contained a method not implemented
	   */
	  NOT_IMPLEMENTED("NOT_IMPLEMENTED", "A request contained a method not implemented"),
	  
	  /**
	   * Unknown (throwable) error was found
	   */
	  UNKNOWN_THROWABLE("UNKNOWN_THROWABLE", "A throwable was found."),
	  
	  /**
	   * Error occurred creating a study team member. Already exists
	   */
	  STUDY_ERROR_TEAM_MEMBER_ALREADY_EXISTS("STUDY_ERROR_TEAM_MEMBER_ALREADY_EXISTS", "Error occurred creating a study team member. Already exists"),
	  
	  /**
	   * Enrollment Notification User not found
	   */
	  ENROLLMENT_NOTIFICATION_USER_NOT_FOUND("ENROLLMENT_NOTIFICATION_USER_NOT_FOUND", "Enrollment Notification User not found"),
	  
	  /**
	   * Approve Enrollment User not found
	   */
	  GET_APPROVE_ENROLLMENT_NOTIFICATION_USER_NOT_FOUND("GET_APPROVE_ENROLLMENT_NOTIFICATION_USER_NOT_FOUND", "Approve Enrollment User not found"),
	  
	  /**
	   * Get Assigned to User not found:
	   */
	  GET_ASSIGNED_TO_USERID_NOT_FOUND("GET_ASSIGNED_TO_USERID_NOT_FOUND", "Get Assigned to User not found:"),

	  /**
	   * Get Documented by User not found:
	   */
	  GET_DOCUMENTED_BY_USERID_NOT_FOUND("GET_DOCUMENTED_BY_USERID_NOT_FOUND" , "Get Documented by User not found:"),
	  
	  /**
	   * User is not authorized via team membership
	   */
	  STUDY_TEAM_AUTHORIZATION("STUDY_TEAM_AUTHORIZATION", "User is not authorized via team membership"), 
	  /**
	   * Group not found
	   */
	  GROUP_NOT_FOUND("GROUP_NOT_FOUND", "Group not found"),
	  
	  /**
	   * User not authorized via group membership
	   */
	  GROUP_AUTHORIZATION("GROUP_AUTHORIZATION", "User not authorized via group membership"),
		/**
		 * User not Authorized for this Organization 
		 */
	  ORGANIZATION_AUTHORIZATION("ORGANIZATION_AUTHORIZATION", "User not authorized via organization rights"),
	  
	  /**
	   * Organization not found
	   */
	  ORGANIZATION_NOT_FOUND("ORGANIZATION_NOT_FOUND", "Organization not found"),
	  
	  /**
	   * Study Organization already exists in study
	   */
	  STUDY_ERROR_CREATE_DUPLICATE_ORGANIZATION("STUDY_ERROR_CREATE_DUPLICATE_ORGANIZATION","Study Organization already exists in study"),
	  
	  /**
	   * Study Status Organization was not found in the study
	   */
	  STUDY_STATUS_ORG_NOT_IN_STUDY("STUDY_STATUS_ORG_NOT_IN_STUDY", "The study statis organization was not found in the study"),
	  
	  /**
	   * User not found
	   */
	  USER_NOT_FOUND("USER_NOT_FOUND", "User not found"),
	  
	  /**
	   * System User not found
	   */
	  SYSTEM_USER_NOT_FOUND("SYSTEM_USER_NOT_FOUND", "System User not found"),
	  
	  /**
	   * User does not have permission to kill the session
	   */
	  DOES_NOT_HAVE_RIGHTS_TO_KILL_SESSION("DOES_NOT_HAVE_RIGHTS_TO_KILL_SESSION", "User does not have the right to kill somebody else's session"),
	  
		/**
		  * User session not killed via the web service
		  */
		  USER_SESSION_NOT_KILLED("USER_SESSION_NOT_KILLED", "User was not logged out of the system successfully"),
		/**
		  * User not logged in to the application
		  */
		USER_NOT_LOGGED_IN("USER_NOT_LOGGED_IN", "User is not currently logged in so no active session to kill"),
	  
	  /**
	   * @deprecated hidden users will be allowed to be specified in Velos eResearch
	   */
	  USER_HIDDEN("USER_HIDDEN","User is hidden"), 
	  
	  /**
	   * Heart Beat(runtime) not found
	   */
	  HEART_BEAT_NOT_FOUND("HEART_BEAT_NOT_FOUND" , "No Heart Beat"),
	 
	  /**
	   * Database connection not found
	   */
	  DATABASE_CONNECTION_NOT_FOUND("DATABASE_CONNECTION_NOT_FOUND","Database connection not found"),
	  
	  /**
	   * Exception while reading manifest file
	   */
	  EXECPTION_WHILE_READING_MANIFEST_FILE("EXECPTION_WHILE_READING_MANIFEST_FILE","Exception while reading manifest file:"),
	  
	  /**
	   * Exception while getting attributes from manifest file:
	   */
	  EXECPTION_WHILE_GETTING_ATTRIBUTES_FROM_MANIFEST_FILE("EXECPTION_WHILE_GETTING_ATTRIBUTES_FROM_MANIFEST_FILE","Exception while getting attributes from manifest file:"),
	  
	  /**
	   * Exception while getting remote Monitoring Services
	   */
	  EXECPTION_GETTING_REMOTE_MONITORING_SERVICES("EXECPTION_GETTING_REMOTE_MONITORING_SERVICES","exception while getting remote Monitoring Services"),
	  
	  /**
	   * Patient not found
	   */
	  PATIENT_NOT_FOUND("PATIENT_NOT_FOUND","Patient not found"),
	  
	  /**
	   * Exception while accessing patient data
	   */
	  PATIENT_DATA_AUTHORIZATION("PATIENT_DATA_AUTHORIZATION","User not authorized to access data of this patient"),
	  
	  /**
	   * Exception while getting studies for patient
	   */
	  STUDIES_FOR_PATIENT_NOT_FOUND("STUDIES_FOR_PATIENT_NOT_FOUND","Studies for Patient not found"),
	  
	  /**
	   * Exception while getting remote patient schedule Services
	   */
	  PATIENT_SCHEDULE_NOT_FOUND("PATIENT_SCHEDULE_NOT_FOUND","Patient schedule not found"),
	  
	  /**
	   * Patient already registered to this Organization
	   */
	  PATIENT_ALREADY_REGISTERED_TO_THIS_ORGANIZATION("PATIENT_ALREADY_REGISTERED_TO_THIS_ORGANIZATION", "Patient already registered to this Organization"),
	  /**
	   * Exception while getting remote Monitoring Services
	   */
	  EXECPTION_GETTING_REMOTE_PATIENT_DEMOGRAPHICS_SERVICES("EXECPTION_GETTING_REMOTE_PATIENT_DEMOGRAPHICS_SERVICES","exception getting remote PatientDemographics Services"),
	  
	  /**
	   * Exception while getting Study Status
	   */
	  STUDY_STATUS_NOT_FOUND("STUDY_STATUS_NOT_FOUND","Study status not found"),
	  
	  /**
	   * Calendar not found
	   */
	  CALENDAR_NOT_FOUND("CALENDAR_NOT_FOUND", "Calendar could not be found"),
	  
	  /**
	   * Calendar not active
	   */
	  CALENDAR_NOT_ACTIVE("CALENDAR_NOT_ACTIVE", "Calendar is not Active"),
	  
	  /**
	   * Event not found
	   */
	  EVENT_NOT_FOUND("EVENT_NOT_FOUND", "Event could not be found"),
	  
	  /**
	   * Event Status Identifier not found
	   */
	  EVENT_STATUS_IDENTIFIER_NOT_FOUND("EVENT_STATUS_IDENTIFIER_NOT_FOUND", "Event Status Identifier could not be found"),
	  
	  /**
	   * Visit not found
	   */
	  VISIT_NOT_FOUND("VISIT_NOT_FOUND", "Visit could not be found"),
	  
	  /**
	   * Form Field not found
	   */
	  FORM_FIELD_NOT_FOUND("FORM_FIELD_NOT_FOUND", "Form Field could not be found"),
	  
	  /**
	   * Currency not found
	   */
	  CURRENCY_NOT_FOUND("CURRENCY_NOT_FOUND", "Currency could not be found"),
	  
	  /**
	   * Exception while creating study calendar
	   */
	  ERROR_CREATING_STUDY_CALENDAR("ERROR_CREATING_STUDY_CALENDAR","Error Creating Study Calendar"),
	  
	  /**
	   * Exception while creating event
	   */
	  ERROR_CREATING_STUDY_CALENDAR_EVENT("ERROR_CREATING_STUDY_CALENDAR_EVENT","Error creating study calendar event"),
	  
	  /**
	   * Exception while creating study calendar event cost
	   */
	  ERROR_CREATING_STUDY_CALENDAR_EVENT_COST("ERROR_CREATING_STUDY_CALENDAR_EVENT_COST","Error creating study calendar event cost"),
	  
	  /**
	   * Exception while creating study calendar event cost
	   */
	  STUDY_CALENDAR_ERROR_CREATE_EVENT("STUDY_CALENDAR_ERROR_CREATE_EVENT","Error creating study calendar event"),
	  
	  /**
	   * Patient Schedule Does Not Exist
	   */
	  PATIENT_SCHEDULE_DOES_NOT_EXIST("PATIENT_SCHEDULE_DOES_NOT_EXIST", "Patient schedule does notv exist"),
	  
	  /**
	   *Exception while creating study calendar
	   */
	  CALENDAR_NAME_ALREADY_EXISTS("CALENDAR_NAME_ALREADY_EXISTS", "Calendar name already exists"),
	  
      /**
       * Visit name already exists
       */
	  VISIT_NAME_ALREADY_EXISTS("VISIT_NAME_ALREADY_EXISTS", "Visit name already exists"),
	  
      /**
       * Visit interval exceeds calendar duration
       */
	  VISIT_INTERVAL_EXCEEDS_CALENDAR_DURATION("VISIT_INTERVAL_EXCEEDS_CALENDAR_DURATION", "visit's interval is past Calendar duration"),
	  
	  /**
	   * Visit identifier is invalid
	   */
	  VISIT_IDENTIFIER_INVALID("VISIT_IDENTIFIER_INVALID", "Visit identifier is invalid"),
	  
	  /**
	   *Event identifier is invalid 
	   */
	  EVENT_IDENTIFIER_INAVLID("EVENT_IDENTIFIER_INVALID", "Event identifier is invalid"),
	  
	/**
	 * Duplicate visit name in request
	 */
	DUPLICATE_VISIT_NAME("DUPLICATE_VISIT_NAME", "Duplicate visit name in request"), 
	
	/**
	 * Duplicate event name in request
	 */
	DUPLICATE_EVENT_NAME("DUPLICATE_EVENT_NAME", "Duplicate event name in request")	,
	/**
	 * Patient ID already exists
	 */
	PATIENT_ID_ALREADY_EXISTS("PATIENT_ID_ALREADY_EXISTS", "PatientID already exists"), 
	
	/**
	 * Patient already exists
	 */
	PATIENT_ALREADY_EXISTS("PATIENT_ALREADY_EXISTS", "Patient already exists"),
	/**
	 * Patient Identifier is not valid
	 */
	PATIENT_IDENTIFIER_INVALID("PATIENT_IDENTIFIER_INVALID", "Patient identifier is invalid"),
	/**
	 * Error creating patient
	 */
	PATIENT_ERROR_CREATE_DEMOGRAPHICS("PATIENT_ERROR_CREATE_DEMOGRAPHICS", "An error occured creating a patient demographics"), 
	/**
	 * Error while adding organization to patient
	 */
	PATIENT_ERROR_ADD_ORGANIZATION("PATIENT_ERROR_ADD_ORGANIZATION", "An error occured while adding an organization to Patient"),
	/**
	 * Error while removing study calendar visit
	 */
	STUDY_CALENDAR_ERROR_REMOVE_VISIT("STUDY_CALENDAR_ERROR_REMOVE_VISIT", "Error while removing study calendar visit"),
	/**
	 * Study Identifier is not valid
	 */
	STUDY_IDENTIFIER_INVALID("STUDY_IDENTIFIER_INVALID", "Study identifier is not valid"),
	 /**
	  * Error while retrieving budget
	  */
	BUDGET_NOT_FOUND("BUDGET_NOT_FOUND", "Budget not found"),
	/**
	  * Error while retrieving study calendar event
	  */
	ERROR_REMOVING_STUDY_CALENDAR_EVENT("ERROR_REMOVING_STUDY_CALENDAR_VISIT", "Error while removing study calendar event"),
	/**
	  * Error while removing study calendar
	  */
	ERROR_REMOVING_STUDY_CALENDAR("ERROR_REMOVING_STUDY_CALENDAR", "Error while removing study calendar"),
	/**
	  * Error while removing study calendar
	  */
	CALENDAR_ACTIVE_NOT_UPDATEABLE("CALENDAR_ACTIVE_NOT_UPDATEABLE","Study Calendar status active, not updateable"), 
	
	
	PATIENT_ERROR_UPDATE_DEMOGRAPHICS("PATIENT_ERROR_UPDATE_DEMOGRAPHICS", "Error while updating Patient Data"),
	
	ERROR_CREATE_PATIENT_ENROLLMENT("ERROR_CREATE_PATIENT_ENROLLMENT", "Error while enrolling patient to Study"),
	
	ERROR_CREATE_MILESTONE("ERROR_CREATE_MILESTONE", "Error while creating milestone to Study"),
	
	ERROR_CREATE_BUDGET("ERROR_CREATE_BUDGET", "Error while creating budget to Study"),
	
	ERROR_CREATE_VERSION("ERROR_CREATE_VERSION", "Error while creating Version to Study"),
	
	PATIENT_ALREADY_ENROLLED("PATIENT_ALREADY_ENROLLED", "Patient is already enrolled to Study"), 
	
	DUPLICATE_ORGANIZATION("DUPLICATE_ORGANIZATION", "Duplicate organization found"),
	
	ERROR_UPDATING_STUDY_CALENDAR("ERROR_UPDATING_STUDY_CALENDAR","Error while updating study calendar"),
	
	ERROR_UPDATING_STUDY_CALENDAR_EVENT("ERROR_UPDATING_STUDY_CALENDAR_EVENT","Error while updating study calendar event"),
	
	INVALID_EVENT_DURATION("INVALID_EVENT_DURATION","Invalid Event Duration"),
	
	INVALID_VISIT_DURATION("INVALID_VISIT_DURATION","Invalid Visit Duration"),
	
	INVALID_FILTER_FIELD_ID("INVALID_FILTER_FIELD_ID", "Please enter a valid field ID to filter response"),
	
	FORM_NOT_FOUND("FORM_NOT_FOUND", "Form not found"),
	
	FORM_DISPLAYTYPE_MISMATCH("FORM_TYPE_MISMATCH","Form displaytype either missing or having mismatch"),
	
	FORM_RESPONSE_DOES_NOT_EXIST("FORM_RESPONSE_DOES_NOT_EXIT", "Form Response does not exist"),
	
	EVENT_COST_NOT_FOUND("EVENT_COST_NOT_FOUND","Event Cost not found"),
	
	ERROR_REMOVING_EVENT_COST("ERROR_REMOVING_EVENT_COST","Error removing Event Cost"),
	
	ERROR_UPDATING_EVENT_COST("ERROR_UPDATING_EVENT_COST","Error updating Event Cost"),
	
	CALENDAR_STATUS_NOT_UPDATEABLE("CALENDAR_STATUS_NOT_UPDATEABLE","Calendar Status not updateable"), 
	
	FORM_TYPE_MISMATCH("FORM_TYPE_MISMATCH", "FormType not matched with method you sent request for"), 
	
	FORM_DESIGN_AUTHORIZATION("FORM_DESIGN_AUTHORIZATION", "User is not authorized"), 
	
	PATIENT_NOT_ONSTUDY("PATIENT_NOT_ONSTUDY" , "Patient is not linked to Study"),
	  
	STUDY_PATIENT_NOT_FOUND("STUDY_PATIENT_NOT_FOUND", "Study Patient not found"),
	
	PATIENT_PROTOCOL_INACTIVE("PATIENT_PROTOCOL_INACTIVE", "Patient Protocol is inactive"),
	
	FORM_ACCESS_AUTHORIZATION("FORM_ACCESS_AUTHORIZATION", "User is not authorized"), 
	
	FORM_RESPONSE_NOT_FOUND("FORM_RESPONSE_NOT_FOUND", "Form Response not found"), 
	
	FORM_RESPONSE_DOES_NOT_EXIT("FORM_RESPONSE_DOES_NOT_EXIT", "Form Response does not exit"),
	
	FORM_RESPONSE_LOCKDOWN("FORM_RESPONSE_LOCKDOWN", "Form Response is in Lockdown status"), 
	
	CRF_NOT_LINKED_WITH_CALENDAR_EVENT("CRF_NOT_LINKED_WITH_CALENDAR_EVENT", "CRF link doesn't exist between event and form"),
	  
	STUDY_PATIENT_NOT_ON_CALENDAR("STUDY_PATIENT_NOT_ON_CALENDAR", "Study Patient is not enrolled to this calendar"),
	
	PATIENT_SCHEDULE_EVENT_NOT_FOUND("PATIENT_SCHEDULE_EVENT_NOT_FOUND", "Patient protocol schedule event not found"), 
	
	PATIENT_SCHEDULE_IDENTIFIER_NOT_FOUND("PATIENT_SCHEDULE_IDENTIFIER_NOT_FOUND", "Patient protocol schedule not found"),
	
	SINGLE_ENTRY_FORM_RESPONSE_ALREADY_EXISTS("SINGLE_ENTRY_FORM_RESPONSE_ALREADY_EXISTS","Response already exists for Single entry form"),
	  
	PATIENT_STATUS_LOCKDOWN("PATIENT_STATUS_LOCKDOWN", "Patient Study status is in lockdown status"), 
	
	ERROR_CREATING_FORM_RESPONSE("ERROR_CREATING_FORM_RESPONSE", "Error occured while create Form Response"), 
	
	ERROR_UPDATING_USER_ACCOUNT_STATUS("ERROR_UPDATING_USER_ACCOUNT_STATUS","Error occurred while updating User account status"),
	
	EXECPTION_GETTING_REMOTE_USER_SERVICES("EXECPTION_GETTING_REMOTE_USER_SERVICES","Exception getting remote User services"),

	PATIENT_ORGANIZATION_ACCESS("PATIENT_ORGANIZATION_ACCESS", "User does not have access to any of the Patient's Organization"),
	
	PATIENT_FACILITY_ID_ALREADY_EXISTS("PATIENT_FACILITY_ID_ALREADY_EXISTS", "Patient facility id already exists"),
	
	SPECIALITY_ACCESS_AUTHORIZATION("SPECIALITY_ACCESS_AUTHORIZATION", "User is not authorized"),
	
	PATIENT_ERROR_UPDATE_ORGANIZATION("PATIENT_ERROR_UPDATE_ORGANIZATION", "Patient error update organization"), 
	
	ERROR_REMOVING_STUDY_PATIENT_STATUS("ERROR_REMOVING_STUDY_PATIENT_STATUS", "Error while removing Study Patient status"),
	
	STUDY_PATIENT_STATUS_NOT_FOUND("STUDY_PATIENT_STATUS_NOT_FOUND", "Study Patient status not found."),
	
	ERROR_UPDATING_STUDY_PATIENT_STATUS("ERROR_UPDATING_STUDY_PATIENT_STATUS", "Error while updating Study Patient status"),
	ERROR_UPDATING_FORM_RESPONSE("ERROR_UPDATING_FORM_RESPONSE","Error while updating form Patient response"),
	
	CODE_LIST_TYPE_NOT_FOUND("CODE_LIST_TYPE_NOT_FOUND", "Code list type not found"), 
	EVENT_ERROR_CREATE_STATUS("EVENT_ERROR_CREATE_STATUS","Event error create status"),
	/**
	 * Account not found
	 */
	ACCOUNT_NOT_FOUND("ACCOUNT_NOT_FOUND", "Account not found"),
	/**
	 * Account not found
	 */
	USER_ACCOUNT_NOT_MATCHED("USER_ACCOUNT_NOT_MATCHED", "Account not matched with user account"),
	INVALID_ESIGNATURE("INVALID_ESIGNATURE", "Invalid eSignature"),
	MILESTONE_STATUS_NOT_FOUND("MILESTONE_STATUS_NOT_FOUND", "Milestone status not found"),
	DUPLICATE_BUDGET_NAME("DUPLICATE_BUDGET_NAME", "Duplicate Budget name in request"),
	DUPLICATE_STUDYVER_NUMBER("DUPLICATE_STUDYVER_NUMBER", "Duplicate Study Version Number in request"),
	BUDGET_ALREADY_EXISTS("BUDGET_ALREADY_EXISTS", "Budget already exists with Calendar"),
	STUDY_CURRENT_STATUS_NOT_FOUND("STUDY_CURRENT_STATUS_NOT_FOUND", "Study Current Status not found"),
	USER_AUTHENTICATION("USER_REQUIRED","Authentication Required"),
	INVALID_USER_PASSWORD("INVALID_USER_PASSWORD","Invalid UserName/Password"),  
	NETWORK_ISSUE("NO NETWORK FOUND","Network not found with during request"),
	NO_NETWORK_ISSUE("NO ISSUES FOUND","NO ISSUES FOUND IN NETWORK"),
	BOARD_NOT_FOUND("BOARD_NOT_FOUND", "The Review board could not be found"),
	REVIEW_BOARD_AUTHORIZATION("REVIEW_BOARD_AUTHORIZATION", "User Account not have access to Review board"),
	BOARD_CRITERIA_NOT_MET("BOARD_CRITERIA_NOT_MET","Pre-Conditions for this review board not Met, Please check submission for More Details"),
	CANNOT_SUBMIT_PROTOCOL("CANNOT_SUBMIT_PROTOCOL","This protocol cannot be submitted at this time while review is pending."),
	INTERNAL_SERVER_ERROR("INTERNAL_SERVER_ERROR","INTERNAL_SERVER_ERROR"),
	NETWORK_NOT_FOUND("NETWORK_NOT_FOUND","No Networks found"),
	NETWORK_USER_NOT_FOUND("NETWORK_USER_NOT_FOUND","Network user not found"),
	DUPLICATE_NWUSER_ROLE("DUPLICATE_NWUSER_ROLE","Duplicate network user role in request.");
	
	  private  String code;
	  private  String message;

	  IssueTypes(String code, String message) {
	     this.code = code;
	     this.message = message;
	  }

	  public String getCode() { return code; }
	  public String getMessage() { return message; }
	  @Override
	  public String toString() {
	    return code + ": " + message;
	  }
}
