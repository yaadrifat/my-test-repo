/**
 * Created On Sep 9, 2011
 */
package com.velos.services.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.LinkedFormsDao;
import com.velos.eres.business.common.NetworkDao;
import com.velos.eres.business.common.SiteDao;
import com.velos.eres.business.common.UserDao;
import com.velos.eres.business.formLib.impl.FormLibBean;
import com.velos.eres.business.linkedForms.impl.LinkedFormsBean;
import com.velos.eres.business.patProt.impl.PatProtBean;
import com.velos.eres.business.person.impl.PersonBean;
import com.velos.eres.service.formLibAgent.FormLibAgentRObj;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.linkedFormsAgent.LinkedFormsAgentRObj;
import com.velos.eres.service.patProtAgent.PatProtAgentRObj;
import com.velos.eres.service.personAgent.PersonAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.network.NetworkJB;
import com.velos.eres.web.site.SiteJB;
import com.velos.eres.web.user.UserJB;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.AccountIdentifier;
import com.velos.services.model.FormDesign;
import com.velos.services.model.FormDisplayTypeIdentifier;
import com.velos.services.model.FormIdentifier;
import com.velos.services.model.FormInfo;
import com.velos.services.model.FormList;
import com.velos.services.model.FormType;
import com.velos.services.model.LinkedFormDesign;
import com.velos.services.model.LinkedFormDesign.DisplayType;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.StudyFormDesign;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatientFormDesign;
import com.velos.services.model.StudyPatientScheduleFormDesign;
import com.velos.services.util.ObjectLocator;

/**
 * @author Kanwaldeep
 *
 */
@Stateless
@Remote(FormDesignService.class)
public class FormDesignServiceImpl extends AbstractService implements FormDesignService {
	
	private static Logger logger = Logger.getLogger(FormDesignServiceImpl.class); 
	@EJB
	private ObjectMapService objectMapService;
	
	@EJB
	private UserAgentRObj userAgent;
	
	@EJB
	private PersonAgentRObj personAgent;
	
	@EJB
	GrpRightsAgentRObj groupRightsAgent;
	
	@EJB
	private LinkedFormsAgentRObj linkedFormAgent; 
	
	@EJB
	private FormLibAgentRObj formLibAgent;
	
	@EJB
	private PatProtAgentRObj patProtAgent;
	
	@EJB
	private UserSiteAgentRObj userSiteAgent;


	
	@Resource 
	private SessionContext sessionContext;	
	
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}

	/* (non-Javadoc)
	 * @see com.velos.services.form.FormDesignService#getLibraryFormDesign(com.velos.services.model.FormIdentifier)
	 */
	public FormDesign getLibraryFormDesign(FormIdentifier formIdentifier, boolean includeFormatting, String formName)
			throws OperationException {
		
		try{
		FormDesignDAO dao = new FormDesignDAO();
		//check permissions for user to view Forms
		checkLibraryFormDesignViewPermissions(); 
		Integer formPK;
		try {
			formPK = ObjectLocator.formPKFromIdentifier(callingUser, formIdentifier, objectMapService,formName, FormType.LIBRARY, null);
			if(formPK == null || formPK ==0)
			{
				addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
				throw new OperationException(); 
			}
			
			if(!dao.getLibraryFormListUserHasAccess(callingUser.getUserId()).contains(formPK))
			{
				addIssue(new Issue(IssueTypes.FORM_DESIGN_AUTHORIZATION, "User is not authorized to view this form")); 
				throw new OperationException(); 
			}
			FormLibBean formlibBean = formLibAgent.getFormLibDetails(formPK); 
			if(!(callingUser.getUserAccountId().equals(formlibBean.getAccountId())))
			{
				addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
				throw new OperationException(); 
			}
			
			//Now check type if type is same as method or not	
			if(!formlibBean.getFormLibLinkTo().equals(FormType.LIBRARY.getValue()))
			{
				addIssue(new Issue(IssueTypes.FORM_TYPE_MISMATCH, "Form not found")); 
				throw new OperationException();
			}
			
					
		} catch (MultipleObjectsFoundException e) {
			addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Mulitple forms found with information provided"));
		  throw new OperationException(); 
		} 
		if(formPK == null || formPK == 0)
		{
			addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
			throw new OperationException(); 
		}
		
	
		FormDesign formDesign = new FormDesign();
		return dao.getLibraryFormDesign(formPK, includeFormatting, callingUser, objectMapService, formDesign); 
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl getStudySummary", e); 
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl getStudySummary", t);
			throw new OperationException(t);
		}
		
	}

	/* (non-Javadoc)
	 * @see com.velos.services.form.FormDesignService#getStudyFormDesign(com.velos.services.model.FormIdentifier, com.velos.services.model.StudyIdentifier)
	 */
	public StudyFormDesign getStudyFormDesign(FormIdentifier formIdentifier,
			StudyIdentifier studyIdentifier, String formName, boolean includeFormatting) throws OperationException {
		StudyFormDesign studyFormDesign = new StudyFormDesign();
		FormDesignDAO formDesignDAO = new FormDesignDAO(); 
		try{
			
			//Dependent on Bug 7377. Bug need to be Fixed. My Service will return first form it will find with this name 
			if((formIdentifier == null || ((formIdentifier.getPK() == null || formIdentifier.getPK() <= 0) &&(formIdentifier.getOID() == null || formIdentifier.getOID().length() == 0 )&&(formIdentifier.getFormName() == null || formIdentifier.getFormName().length() == 0 )))
					&& (formName == null || formName.length() == 0 || studyIdentifier == null || (studyIdentifier.getPK() == null && studyIdentifier.getOID() == null && studyIdentifier.getStudyNumber() == null)))
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "FormIdentifier and StudyIdentifier is required to retrieve StudyForm"));
				throw new OperationException();
			}
			Integer formPK=0; 
			
			try{	
			
				formPK  = ObjectLocator.formPKFromIdentifier(callingUser, formIdentifier, objectMapService, formName, FormType.STUDY, studyIdentifier) ;	
			}catch(OperationException oe)
			{
				addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
				throw new OperationException(); 				
			}
			if(formPK == null || formPK ==0){
				try{	
				
					formPK  = ObjectLocator.formPKFromIdentifier(callingUser, formIdentifier, objectMapService, formName, FormType.STUDYALL, studyIdentifier) ;	
				}catch(OperationException oe)
				{
					addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
					throw new OperationException(); 				
				}
			}
			
			if(formPK == null || formPK ==0)
			{
				addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
				throw new OperationException(); 
			}
			// get StudyID from Linked Form 
			LinkedFormsBean formBean = linkedFormAgent.findByFormId(formPK); 
			Integer studyID = null; 
			if(formBean != null)
			{
				if(!formBean.getAccountId().equals(callingUser.getUserAccountId()))
				{
					addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
					throw new OperationException(); 				
				}
				studyID = StringUtil.stringToInteger(formBean.getStudyId()); 
			}
			
			if(formBean == null || !(formBean.getLFDisplayType().trim().equals(DisplayType.STUDY.toString())
					|| formBean.getLFDisplayType().trim().equals(DisplayType.ALL_STUDIES.toString())))
			{
				addIssue(new Issue(IssueTypes.FORM_TYPE_MISMATCH, "Requested Form is not StudyForm")); 
				throw new OperationException();
			}
			//Integer studyID = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService); 
			if(studyID != null) checkStudyFormDesignViewPermissions(studyID); 
						
			boolean hasFormAccessFromStudy = false; 
			// This form can be viewed from FormManagement no matter whether user has StudyTeam Rights to view FormDesign. 
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent);				 		
			Integer manageAccountFormManagement = authModule.getFormsAccountFormManagementPrivileges(); 
				
			boolean hasFormManagementAccess = GroupAuthModule.hasViewPermission(manageAccountFormManagement); 
					
					

			
			if(hasFormManagementAccess == false)
			{
				Integer managePatient = authModule.getAppManagePatientsPrivileges(); 
				if(GroupAuthModule.hasViewPermission(managePatient))
				{
					LinkedFormsDao linkedformsDao = linkedFormAgent.getStudyForms(StringUtil.stringToInteger(callingUser.getUserAccountId()), studyID, callingUser.getUserId(), StringUtil.stringToInteger(callingUser.getUserSiteId())); 
					if(linkedformsDao.getFormId().contains(formPK)) hasFormAccessFromStudy = true; 
				}
				
		
			}
			
			if(hasFormManagementAccess || hasFormAccessFromStudy)
			{
				formDesignDAO.getStudyPatientFormDesign(formPK, callingUser, objectMapService, studyFormDesign, studyID, includeFormatting);
					

			}else{
				addIssue(new Issue(IssueTypes.FORM_DESIGN_AUTHORIZATION, "User not authorized to view this form design")); 
				throw new OperationException(); 
			}	
			
	}catch(OperationException e){
		sessionContext.setRollbackOnly();
		if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getStudyFormDesign", e); 
		e.setIssues(response.getIssues());
		throw e;
	}
	catch(Throwable t){
		sessionContext.setRollbackOnly();
		this.addUnknownThrowableIssue(t);
		if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getStudyFormDesign", t);
		throw new OperationException(t);
	}
	
		return studyFormDesign;
	}

	/* (non-Javadoc)
	 * @see com.velos.services.form.FormDesignService#getAccountFormDesign(com.velos.services.model.FormIdentifier)
	 */
	public LinkedFormDesign getAccountFormDesign(FormIdentifier formIdentifier, AccountIdentifier accountIdentifier, FormDisplayTypeIdentifier formDisplayTypeIdentifier, String formName, boolean includeFormatting)
			throws OperationException {

		LinkedFormDesign linkedFormDesign = new LinkedFormDesign(); 
		
		FormDesignDAO formDesignDAO = new FormDesignDAO(); 
		Integer formPK; 
		int modPk=0;
		int sitePk=0;
		int userPk=0;
		int networkPk=0;
		String displayType=DisplayType.ACCOUNT.toString();
		try{
			if(formDisplayTypeIdentifier!=null &&
					formDisplayTypeIdentifier.getType().equals(DisplayType.USER.toString())){
				
				
				displayType=DisplayType.USER.toString();
			}
			if(formDisplayTypeIdentifier!=null && formDisplayTypeIdentifier.getType().equals(DisplayType.ORGANIZATION.toString())){
				
				displayType=DisplayType.ORGANIZATION.toString();
			}
			if(formDisplayTypeIdentifier!=null && formDisplayTypeIdentifier.getType().equals(DisplayType.NETWORK.toString())){
				
				displayType=DisplayType.NETWORK.toString();
			}
			if(formIdentifier == null && accountIdentifier == null)
			{	
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid formIdentifier or accountIdentifier  is required to get accountFormDesign")); 
				throw new OperationException(); 
			}else if(formIdentifier == null && accountIdentifier != null){
				if(accountIdentifier != null &&((accountIdentifier.getPK() == null || accountIdentifier.getPK()<=0) && (accountIdentifier.getOID() == null || accountIdentifier.getOID().trim().length() == 0)))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid accountIdentifier  is required to get accountFormDesign")); 
					throw new OperationException();
				}
			}else if(formIdentifier != null && accountIdentifier == null){
				if(formIdentifier != null &&((formIdentifier.getPK() == null || formIdentifier.getPK()<=0) && (formIdentifier.getOID() == null || formIdentifier.getOID().trim().length() == 0) && (formIdentifier.getFormName()==null || StringUtil.isEmpty(formIdentifier.getFormName()))))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid formIdentifier  is required to get accountFormDesign")); 
					throw new OperationException();
				}
			}
			formPK = ObjectLocator.formPKAccountIdentifier(callingUser, formIdentifier, objectMapService, formName, FormType.ACCOUNT, accountIdentifier,displayType); 
			System.out.println("formPK==="+formPK);
			System.out.println("displayType=="+displayType);
			if(formPK == null || formPK ==0)
			{
				addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
				throw new OperationException(); 
			}
			if(accountIdentifier.getPK() ==null || accountIdentifier.getPK() == 0)
			{
				addIssue(new Issue(IssueTypes.ACCOUNT_NOT_FOUND, "Account not found")); 
				throw new OperationException(); 
			}
			
			formDesignDAO.getAccountFormDesignWS(formPK, displayType, callingUser, objectMapService, linkedFormDesign, includeFormatting,response); 
			
		}catch(OperationException e){
			if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(MultipleObjectsFoundException m)
		{
			m.printStackTrace(); 
		}
		
		return linkedFormDesign;
	}

	/* (non-Javadoc)
	 * @see com.velos.services.form.FormDesignService#getPatientFormDesign(com.velos.services.model.FormIdentifier)
	 */
	/*public LinkedFormDesign getPatientFormDesign(FormIdentifier formIdentifier, String formName)
			throws OperationException {
		// TODO Auto-generated method stub
		return null;
	}*/

	/* (non-Javadoc)
	 * @see com.velos.services.form.FormDesignService#getStudyPatientFormDesign(com.velos.services.model.FormIdentifier, com.velos.services.model.StudyIdentifier)
	 */
	public StudyPatientFormDesign getStudyPatientFormDesign(FormIdentifier formIdentifier,
			StudyIdentifier studyIdentifier, String formName, boolean includeFormatting) throws OperationException {

		StudyPatientFormDesign studyPatientFormDesign  = new StudyPatientFormDesign(); 
		FormDesignDAO formDesignDAO = new FormDesignDAO(); 
		try{
			//Dependent on Bug 7377. Bug need to be Fixed. My Service will return first form it will find with this name 
			if((formIdentifier == null || ((formIdentifier.getOID() == null || formIdentifier.getOID().length() == 0) && (formIdentifier.getPK() == null || formIdentifier.getPK() == 0) && (formIdentifier.getFormName() == null || formIdentifier.getFormName().length() == 0)))
					&& (formName == null || formName.length() == 0 || studyIdentifier == null || (studyIdentifier.getPK() == null && studyIdentifier.getOID() == null && studyIdentifier.getStudyNumber() == null)))
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "FormIdentifier or FormName and StudyIdentifier is required to retrieve StudyPatientForm"));
				throw new OperationException();
			}
			Integer formPK; 
			
			try{		
				formPK  = ObjectLocator.formPKFromIdentifier(callingUser, formIdentifier, objectMapService, formName, FormType.STUDYPATIENT, studyIdentifier ) ;	
			}catch(OperationException oe)
			{
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found")); 
				throw new OperationException(); 				
			}

			if(formPK == null || formPK ==0)
			{
				addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
				throw new OperationException(); 
			}
			// get StudyID from Linked Form 
			LinkedFormsBean formBean = linkedFormAgent.findByFormId(formPK); 
			Integer studyID = null; 
			if(formBean != null)
			{
				if(!formBean.getAccountId().equals(callingUser.getUserAccountId()))
				{
					addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
					throw new OperationException(); 				
				}
				studyID = StringUtil.stringToInteger(formBean.getStudyId()); 
			}
			
			if(formBean == null || !(formBean.getLFDisplayType().trim().equals(DisplayType.PATIENT_ENROLLED_TO_SPECIFIC_STUDY.toString())
					|| formBean.getLFDisplayType().trim().equals(DisplayType.PATIENT_LEVEL_ALL_STUDIES.toString())
							|| formBean.getLFDisplayType().trim().equals(DisplayType.PATIENT_LEVEL_ALL_STUDIES_RESTRICTED.toString())))
			{
				addIssue(new Issue(IssueTypes.FORM_TYPE_MISMATCH, "Requested Form is not StudyPatientForm")); 
				throw new OperationException();
			}
			//Integer studyID = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService); 
			if(studyID != null) checkStudyFormDesignViewPermissions(studyID); 
						
			boolean hasFormAccessFromStudy = false; 
			// This form can be viewed from FormManagement no matter whether user has StudyTeam Rights to view FormDesign. 
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent);				 		
			Integer manageAccountFormManagement = authModule.getFormsAccountFormManagementPrivileges(); 
				
			boolean hasFormManagementAccess = GroupAuthModule.hasViewPermission(manageAccountFormManagement); 
					
					
//			if(hasFormManagementAccess == false && studyID != null)
//			{
//			
//					if(checkPatientStudyFormDesignViewPermissions(studyID) == true)
//					{
//						if(formDesignDAO.getStudyPatientFormListUserHasAccess(callingUser, studyID).contains(formPK)) hasFormAccessFromStudy = true; 
//					}
//				
//			}
			
			if(hasFormManagementAccess == false)
			{
				Integer managePatient = authModule.getAppManagePatientsPrivileges(); 
				if(GroupAuthModule.hasViewPermission(managePatient))
				{
					if(formDesignDAO.getStudyPatientFormListUserHasAccess(callingUser, studyID, false).contains(formPK)) hasFormAccessFromStudy = true; 
				}
				
		
			}
			
			if(hasFormManagementAccess || hasFormAccessFromStudy)
			{
				formDesignDAO.getStudyPatientFormDesign(formPK, callingUser, objectMapService, studyPatientFormDesign, studyID, includeFormatting);
					

			}else{
				addIssue(new Issue(IssueTypes.FORM_DESIGN_AUTHORIZATION, "User not authorized to view this form design")); 
				throw new OperationException(); 
			}
			
			
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getStudyPatientFormDesign", e); 
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getStudyPatientFormDesign", t);
			throw new OperationException(t);
		}
		
			return studyPatientFormDesign;
	}

	/* (non-Javadoc)
	 * @see com.velos.services.form.FormDesignService#getAllFormsForStudy(com.velos.services.model.StudyIdentifier)
	 */
	public FormList getAllFormsForStudy(
			StudyIdentifier studyIdentifier) throws OperationException {
		
		Integer studyId = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService); 
		List<FormInfo> formInfoList = new ArrayList<FormInfo>();
	
		
		if(studyId == null || studyId == 0)	
		{
			//TODO throw exception
		}
		
		
		GroupAuthModule groupAuthModule = new GroupAuthModule(callingUser, groupRightsAgent); 
		Integer manageProtocolPriv = groupAuthModule.getAppManageProtocolsPrivileges();
		
		
		Integer manageStudyFormsPriv = groupAuthModule.getFormsAllStudyFormsPrivileges(); 
		
		
		TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyId);

		
		
		//TODO check group rights and study rights
		
		 LinkedFormsDao lnkFrmDao = new LinkedFormsDao();
		 lnkFrmDao = linkedFormAgent.getStudyForms(StringUtil.stringToInteger(callingUser.getUserAccountId()), studyId, callingUser.getUserId(), StringUtil.stringToInteger(callingUser.getUserSiteId())); 
		 
		 
		
		 
		 List formIDs  = lnkFrmDao.getFormId();		 
		 List formNames = lnkFrmDao.getFormName();
		 
		 for(int i =0; i < formIDs.size(); i++)
		 {
			 FormInfo formInfo = new FormInfo();  
			 ObjectMap objectMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_FORM_LIBRARY, (Integer) formIDs.get(i)); 
			 FormIdentifier formIdentifier = new FormIdentifier(); 
			 formIdentifier.setOID(objectMap.getOID()); 
			 formIdentifier.setPK((Integer) formIDs.get(i));
			 
			 formInfo.setFormIdentifier(formIdentifier); 
			 formInfo.setFormName(formNames.get(i).toString()); 
			 formInfoList.add(formInfo); 
			 
		 }
		
		 FormList formList = new FormList(); 
		 formList.setFormInfo(formInfoList); 
		 return formList ;
	}
	
	private boolean checkLibraryFormDesignViewPermissions() throws AuthorizationException
	{
		GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
		Integer manageFormLibrary = authModule.getFormsFormLibraryPrivileges(); 		
		Integer manageAccountFormManagement = authModule.getFormsAccountFormManagementPrivileges(); 
		
		boolean hasViewPermissions = GroupAuthModule.hasViewPermission(manageFormLibrary) || GroupAuthModule.hasNewPermission(manageAccountFormManagement); 
		if(!hasViewPermissions)
		{		
			addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Form Design"));
				throw new AuthorizationException("User Not Authorized to view Form Design");
		
		}	
		
		return hasViewPermissions; 
	}
	
	private boolean checkStudyFormDesignViewPermissions(Integer studyPK) throws AuthorizationException
	{			
		TeamAuthModule teamModule = new TeamAuthModule(callingUser.getUserId(), studyPK);		
		Integer formsManagement = teamModule.getFormsStudyManagementPrivileges(); 
		Integer formsAccess = teamModule.getFormsStudyAccessPrivileges(); 
		
		boolean hasViewPermissions = TeamAuthModule.hasViewPermission(formsManagement|formsAccess); 
		
		if(!hasViewPermissions)
		{
			addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User Not Authorized for Study"));
			throw new AuthorizationException("User Not Authorized to view Form Design for this Study");
	
		}
		
		return hasViewPermissions; 
		
	}

	public StudyPatientScheduleFormDesign getStudyPatientScheduleFormDesign(
			FormIdentifier formIdentifier, StudyIdentifier studyIdentifier,
			String formName, boolean includeFormatting)
			throws OperationException {
		
		StudyPatientScheduleFormDesign studyPatientScheduleFormDesign  = new StudyPatientScheduleFormDesign(); 
		FormDesignDAO formDesignDAO = new FormDesignDAO(); 
		try{
			//Dependent on Bug 7377. Bug need to be Fixed. My Service will return first form it will find with this name 
			if((formIdentifier == null || ((formIdentifier.getPK()== null || formIdentifier.getPK() == 0) && (formIdentifier.getOID() == null || formIdentifier.getOID().length() == 0) && (formIdentifier.getFormName() == null || formIdentifier.getFormName().length() == 0)))
					&& (formName == null || formName.length() == 0 || studyIdentifier == null || (studyIdentifier.getOID() == null && studyIdentifier.getStudyNumber() == null && studyIdentifier.getPK() == null)))
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "FormIdentifier or FormName and StudyIdentifier is required to retrieve StudyPatientForm"));
				throw new OperationException();
			}
			Integer formPK; 
			
			try{		
				formPK  = ObjectLocator.formPKFromIdentifier(callingUser, formIdentifier, objectMapService, formName, FormType.STUDYPATIENT, studyIdentifier) ;	
			}catch(OperationException oe)
			{
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found")); 
				throw new OperationException(); 				
			}

			if(formPK == null || formPK ==0)
			{
				addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
				throw new OperationException(); 
			}
			// get StudyID from Linked Form 
			LinkedFormsBean formBean = linkedFormAgent.findByFormId(formPK); 
			Integer studyID = null; 
			if(formBean != null)
			{
				if(!formBean.getAccountId().equals(callingUser.getUserAccountId()))
				{
					addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
					throw new OperationException(); 				
				}
				studyID = StringUtil.stringToInteger(formBean.getStudyId()); 
			}
			
			if(!formDesignDAO.isFormLinkedToAnyEvent(formPK) || formBean == null || !(formBean.getLFDisplayType().trim().equals(DisplayType.PATIENT_ENROLLED_TO_SPECIFIC_STUDY.toString())
					|| formBean.getLFDisplayType().trim().equals(DisplayType.PATIENT_LEVEL_ALL_STUDIES.toString())
							|| formBean.getLFDisplayType().trim().equals(DisplayType.PATIENT_LEVEL_ALL_STUDIES_RESTRICTED.toString())))
			{
				addIssue(new Issue(IssueTypes.FORM_TYPE_MISMATCH, "Requested Form is not StudyPatientScheduleForm")); 
				throw new OperationException();
			}
			//Integer studyID = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService); 
			if(studyID != null) checkStudyFormDesignViewPermissions(studyID); 
						
			boolean hasFormAccessFromStudy = false; 
			// This form can be viewed from FormManagement no matter whether user has StudyTeam Rights to view FormDesign. 
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent);				 		
			Integer manageAccountFormManagement = authModule.getFormsAccountFormManagementPrivileges(); 
				
			boolean hasFormManagementAccess = GroupAuthModule.hasViewPermission(manageAccountFormManagement); 
					

			
			if(hasFormManagementAccess == false)
			{
				Integer managePatient = authModule.getAppManagePatientsPrivileges(); 
				if(GroupAuthModule.hasViewPermission(managePatient))
				{
					if(formDesignDAO.getStudyPatientFormListUserHasAccess(callingUser, studyID, false).contains(formPK)) hasFormAccessFromStudy = true; 
				}
				
		
			}
			
			if(hasFormManagementAccess || hasFormAccessFromStudy)
			{
				formDesignDAO.getStudyPatientFormDesign(formPK, callingUser, objectMapService, studyPatientScheduleFormDesign, studyID, includeFormatting);
					

			}else{
				addIssue(new Issue(IssueTypes.FORM_DESIGN_AUTHORIZATION, "User not authorized to view this form design")); 
				throw new OperationException(); 
			}
			
			
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getStudyPatientFormDesign", e); 
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getStudyPatientFormDesign", t);
			throw new OperationException(t);
		}
		
			return studyPatientScheduleFormDesign;
		
	
	}
	public LinkedFormDesign getPatientFormDesign(FormIdentifier formIdentifier,
			String formName,boolean includeFormatting)
			throws OperationException 
			{
		LinkedFormDesign linkedformDesign = new LinkedFormDesign();
		    FormDesignDAO dao =new FormDesignDAO();
			try
			{
				if((formIdentifier == null || ((formIdentifier.getPK() == null || formIdentifier.getPK() <= 0) && (formIdentifier.getOID() == null || formIdentifier.getOID().trim().length() == 0) && (formIdentifier.getFormName() == null || formIdentifier.getFormName().trim().length() == 0))) && (formName == null || formName.trim().length() == 0))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "FormIdentifier or FormName is required to retrieve PatientForm"));
					throw new OperationException();
				}
				Integer formPK;
				//check permissions for user to view Forms

				checkPatientFormDesignViewPermissions();
			
				try
				{
					/*if(!StringUtil.isEmpty(formIdentifier.getFormName()) && (formIdentifier.getPK()==null || formIdentifier.getPK()==0) && (StringUtil.isEmpty(formIdentifier.getOID()))) {
						int formpk=lfdao.getFormPK(formIdentifier.getFormName(),DisplayType.ALL_PATIENTS.toString());
						formIdentifier.setPK(formpk);
					}*/
					
					formPK = ObjectLocator.formPKFromIdentifier(callingUser, formIdentifier, objectMapService,formName, FormType.PATIENT, null);
					if(formPK == null || formPK ==0)
					{
						addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
						throw new OperationException(); 
					}
					LinkedFormsBean formBean = linkedFormAgent.findByFormId(formPK); 
					if(formBean != null)
					{
						if(!formBean.getAccountId().equals(callingUser.getUserAccountId()))
						{
							addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
							throw new OperationException(); 				
						}
					}
					//check if Requested Form is not Patient Form
					
					if(formBean == null || !(formBean.getLFDisplayType().trim().equals(DisplayType.ALL_PATIENTS.toString())))
					{
						addIssue(new Issue(IssueTypes.FORM_TYPE_MISMATCH, "Requested Form is not Patient Form")); 
						throw new OperationException();
					}
					
					GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent);				 		
					Integer manageAccountFormManagement = authModule.getFormsAccountFormManagementPrivileges(); 
						
					boolean hasFormManagementAccess = GroupAuthModule.hasViewPermission(manageAccountFormManagement);
					linkedformDesign = dao.getPatientFormDesign(formPK,includeFormatting,null,callingUser, objectMapService, linkedformDesign);
					if(!hasFormManagementAccess  && (linkedformDesign.getFormStatus().getCode().equals("W") || linkedformDesign.getFormStatus().getCode().equals("O") || linkedformDesign.getFormStatus().getCode().equals("D")))
					{
					//	dao.getLibraryFormDesign(formPK, includeFormatting, callingUser, objectMapService, linkedformDesign);
						//linkedformDesign = dao.getPatientFormDesign(formPK,includeFormatting,null,callingUser, objectMapService, linkedformDesign);
						addIssue(new Issue(IssueTypes.FORM_DESIGN_AUTHORIZATION, "User not authorized to view this form design")); 
						throw new OperationException();
					}
						
				}
				catch (MultipleObjectsFoundException e) {
					addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Mulitple forms found with information provided"));
				  throw new OperationException(); 
				} 
			
			}
				catch(OperationException e){
				sessionContext.setRollbackOnly();
				if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getPatientSummary", e); 
				e.setIssues(response.getIssues());
				throw e;
			}
			catch(Throwable t){
				sessionContext.setRollbackOnly();
				this.addUnknownThrowableIssue(t);
				if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getPatientSummary", t);
				throw new OperationException(t);
			}
						
		return linkedformDesign;
		
		// TODO Auto-generated method stub
		
	}
	
	private boolean checkPatientFormDesignViewPermissions() throws AuthorizationException
	{
		GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
		Integer managePatientForm = authModule.getFormsAllPatientFormsPrivileges(); 
		Integer manageFormLibrary = authModule.getFormsFormLibraryPrivileges(); 
		boolean hasViewPermissions = GroupAuthModule.hasViewPermission(managePatientForm) || GroupAuthModule.hasViewPermission(manageFormLibrary); 
		if(!hasViewPermissions)
		{		
			addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Patient Form Design"));
				throw new AuthorizationException("User Not Authorized to view Patient Form Design");
		
		}	
		
		return hasViewPermissions; 
		
	}
	
/*	@Override
	public FormDesign getPatientFormDesign(FormIdentifier formIdentifier,
			String formName, boolean includeFormatting)
			throws OperationException 
			{
		    FormDesign formDesign = new FormDesign();
		    FormDesignDAO dao =new FormDesignDAO();
			try
			{
				if((formIdentifier == null || ((formIdentifier.getPK() == null || formIdentifier.getPK() < 0) && (formIdentifier.getOID() == null || formIdentifier.getOID().trim().length() == 0))) && (formName == null || formName.trim().length() == 0))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "FormIdentifier or FormName is required to retrieve PatientForm"));
					throw new OperationException();
				}	
				Integer formPK;
				//check permissions for user to view Forms

				checkPatientFormDesignViewPermissions();
			
				try
				{
					formPK = ObjectLocator.formPKFromIdentifier(callingUser, formIdentifier, objectMapService,formName, FormType.PATIENT, null);
					if(formPK == null || formPK ==0)
					{
						addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
						throw new OperationException(); 
					}
					LinkedFormsBean formBean = linkedFormAgent.findByFormId(formPK); 
					if(formBean != null)
					{
						if(!formBean.getAccountId().equals(callingUser.getUserAccountId()))
						{
							addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
							throw new OperationException(); 				
						}
					}
				//check if Requested Form is not Patient Form
					
					if(formBean == null || !(formBean.getLFDisplayType().trim().equals(DisplayType.ALL_PATIENTS.toString())))
					{
						addIssue(new Issue(IssueTypes.FORM_TYPE_MISMATCH, "Requested Form is not Patient Form")); 
						throw new OperationException();
					}
					
					GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent);				 		
					Integer manageAccountFormManagement = authModule.getFormsAccountFormManagementPrivileges(); 
						
					boolean hasFormManagementAccess = GroupAuthModule.hasViewPermission(manageAccountFormManagement); 
					if(hasFormManagementAccess)
					{
						dao.getLibraryFormDesign(formPK, includeFormatting, callingUser, objectMapService, formDesign);
					}
					else{
						addIssue(new Issue(IssueTypes.FORM_DESIGN_AUTHORIZATION, "User not authorized to view this form design")); 
						throw new OperationException(); 
					}
					
				}
				catch (MultipleObjectsFoundException e) {
					addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Mulitple forms found with information provided"));
				  throw new OperationException(); 
				} 
			
			}
			catch(OperationException e){
				sessionContext.setRollbackOnly();
				if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getPatientSummary", e); 
				e.setIssues(response.getIssues());
				throw e;
			}
			catch(Throwable t){
				sessionContext.setRollbackOnly();
				this.addUnknownThrowableIssue(t);
				if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getPatientSummary", t);
				throw new OperationException(t);
			}
			
		return formDesign;
		
		// TODO Auto-generated method stub
		
	}
	
*/	
	
	public FormList getListOfStudyPatientForms(PatientIdentifier patientIdentifier, StudyIdentifier studyIdentifier, int maxNumberOfResults, boolean formHasResponses) throws OperationException
	{
		try
		{
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent);
			Integer patientManagmentAccess = authModule.getAppManagePatientsPrivileges(); 
			if(!(GroupAuthModule.hasViewPermission(patientManagmentAccess)))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to view complete Patient data")); 
				throw new AuthorizationException();
			}
			//--------Checking StudyIdentifier for null---------//
		   if((studyIdentifier==null) || (StringUtil.isEmpty(studyIdentifier.getOID()) && StringUtil.isEmpty(studyIdentifier.getStudyNumber()) && (studyIdentifier.getPK() == null || studyIdentifier.getPK() == 0)))
	 	   {
		    addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid StudyIdentifier with OID or studyNumber is required.")); 
		    throw new OperationException();
		   }
		   else
		   {
		    //----------Finding studyPk------------------//
		    Integer studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService);		
		    if(studyPK==null || studyPK.intValue()==0)
		    {
		     addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for OID : " + studyIdentifier.getOID() + " and studyNumber : "+studyIdentifier.getStudyNumber())); 
		     throw new OperationException();
		    }
		    //--------Checking PatientIdentifier for null---------//
		    if((null==patientIdentifier) || (StringUtil.isEmpty(patientIdentifier.getOID()) && (null==patientIdentifier.getPK() || patientIdentifier.getPK() == 0)
		      && (StringUtil.isEmpty(patientIdentifier.getPatientId()) || (null==patientIdentifier.getOrganizationId()))))
		    {
		     addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifier with OID or PatientPK or PatientID and OrganizationIdentifier is required")); 
		     throw new OperationException();
		    }
		    //---------Finding personPk------------//
		    Integer perPk=null;
		    try {
		     perPk = ObjectLocator.personPKFromPatientIdentifier(callingUser, patientIdentifier, objectMapService);
		    } catch (MultipleObjectsFoundException e) {
		    	addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "MULTIPLE OBJECTS FOUND for patient details")); 
			     throw new OperationException();
		    }
		    if(perPk==null || perPk.intValue()==0)
		    {
		     addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Patient not found for OID : " + patientIdentifier.getOID() + " and patientId : "+patientIdentifier.getPatientId())); 
		     throw new OperationException();
		    }
		    //---------------Checking if patient is linked to Study or not ----------------// 
		    PatProtBean  patProtBean = patProtAgent.findCurrentPatProtDetails(studyPK, perPk); 
		    if(patProtBean.getPatProtId() == 0)
		    {
		     addIssue(new Issue(IssueTypes.PATIENT_NOT_ONSTUDY, "Given patient is not linked to study provided")); 
		     throw new OperationException(); 
		    }
		    Integer patProtID = patProtBean.getPatProtId(); 
		    
		    //check patient level access
		    Integer userMaxStudyPatientRight = userSiteAgent.getMaxRightForStudyPatient(callingUser.getUserId(), perPk, studyPK);
		    if(userMaxStudyPatientRight == 0)
		    {
		     addIssue(new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, "User is not authorized to view Form  for patient")); 
		     throw new OperationException();
		    }
		    //check Study level access
		    TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyPK); 
		    Integer studyPatientManagement = teamAuth.getPatientManagePrivileges(); 
		    Integer studyFormAccess = teamAuth.getFormsStudyAccessPrivileges(); 
		    if(!TeamAuthModule.hasViewPermission(studyPatientManagement))
		    {
		     addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to view Form  for study")); 
		     throw new OperationException(); 
		    }
		    if(!TeamAuthModule.hasViewPermission(studyFormAccess))
		    {
		     addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to view Form  for study")); 
		     throw new OperationException(); 
		    }
		    int limit=0;
		    if(maxNumberOfResults<=100 && maxNumberOfResults!=0)limit=maxNumberOfResults;
		    else {limit=100;}
		    FormList formList=null;		    
		    FormDesignDAO frmDesignDao=new FormDesignDAO();
		    Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("objectMapService", this.objectMapService);
			parameters.put("callingUser",callingUser);
			formList=frmDesignDao.getListOfStudyPatientForms(StringUtil.stringToInteger(callingUser.getUserAccountId()), perPk, studyPK, callingUser.getUserId(),parameters,limit,formHasResponses);
			if(formList==null)
		    {
		    	 addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "No forms were found for data provided.")); 
			     throw new OperationException();
		    }
		    return formList;
		   }
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getListOfStudyPatientForms", e); 
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getListOfStudyPatientForms", t);
			throw new OperationException(t);
	}
	
}

	@Override
	public FormList getListOfPatientForms(
			PatientIdentifier patientIdentifier, boolean formHasResponses, boolean ignoreDisplayInPatFlag,
			int pageNumber, int pageSize) throws OperationException 
	{
		try {
			//--------Checking group authorization-------------//
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
			Integer patientManagmentAccess = authModule.getAppManagePatientsPrivileges(); 
			Integer patientFormAccess=authModule.getFormsAllPatientFormsPrivileges();
			if(!GroupAuthModule.hasViewPermission(patientManagmentAccess))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to view patient data")); 
				throw new AuthorizationException(); 
			}
			if(!GroupAuthModule.hasViewPermission(patientFormAccess))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to view patient data")); 
				throw new AuthorizationException(); 
			}
			//--------Checking PatientIdentifier for null---------//
			if((patientIdentifier==null) || (StringUtil.isEmpty(patientIdentifier.getOID()) && (patientIdentifier.getPK()==null||patientIdentifier.getPK()==0) &&
					(StringUtil.isEmpty(patientIdentifier.getPatientId()) || (patientIdentifier.getOrganizationId() == null))))
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifier with OID or PatientPK or PatientID and OrganizationIdentifier is required")); 
				throw new OperationException();
			}
			//---------Finding personPk------------//
			Integer perPk=null;
			try {
				perPk = ObjectLocator.personPKFromPatientIdentifier(callingUser, patientIdentifier, objectMapService);
			} catch (MultipleObjectsFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(perPk==null || perPk.intValue()==0)
			{
				addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Patient not found for code: "+patientIdentifier.getPatientId()+
							" organization : "+patientIdentifier.getOrganizationId()+
							" pk : "+patientIdentifier.getPK()+
							" OID : "+patientIdentifier.getOID())); 
				throw new OperationException();
			}
			PersonBean personBean = personAgent.getPersonDetails(perPk);
            if (personBean == null){
				addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Patient not found for code: "+patientIdentifier.getPatientId()+
						" organization : "+patientIdentifier.getOrganizationId()+
						" pk : "+patientIdentifier.getPK()+
						" OID : "+patientIdentifier.getOID())); 
			throw new OperationException();
            }
            //bug 14444
	         // ---	PatientFacilityRight checks ---- 			
			Integer userPatientFacilityRight = userSiteAgent.getUserPatientFacilityRight(callingUser.getUserId(), perPk); 
			if(userPatientFacilityRight < 4)
			{
				addIssue(new Issue(IssueTypes.PATIENT_ORGANIZATION_ACCESS, "User is not authorized to access patient data")); 
				throw new OperationException(); 
			}
			//------Checking pageNumber,pageSize for zer0-------//
			if(pageSize==0)
	    	{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "pageSize field is required"));
	        	throw new OperationException();
	    	}
			if(pageNumber==0)
	    	{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "pageNumber field is required"));
	        	throw new OperationException();
	    	}
			FormDesignDAO designDAO=new FormDesignDAO();
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("objectMapService", this.objectMapService);
			parameters.put("callingUser",callingUser);
			return designDAO.getListOfPatientForms(StringUtil.stringToInteger(callingUser.getUserAccountId()), perPk, callingUser.getUserId(),parameters, pageNumber, pageSize, formHasResponses, ignoreDisplayInPatFlag);
			
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl create", t);
			throw new OperationException(t);
		}
		
	}	
	
	
/*	private boolean checkPatientFormDesignViewPermissions() throws AuthorizationException
	{
		GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
		Integer managePatientForm = authModule.getFormsAllPatientFormsPrivileges(); 
		Integer manageFormLibrary = authModule.getFormsFormLibraryPrivileges(); 
		boolean hasViewPermissions = GroupAuthModule.hasViewPermission(managePatientForm) || GroupAuthModule.hasViewPermission(manageFormLibrary); 
		if(!hasViewPermissions)
		{		
			addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Patient Form Design"));
				throw new AuthorizationException("User Not Authorized to view Patient Form Design");
		
		}	
		
		return hasViewPermissions; 
		
	}
*/	
//	private boolean checkPatientStudyFormDesignViewPermissions(Integer studyPK) throws AuthorizationException
//	{			
//		TeamAuthModule teamModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
//	//	Integer patientManagement = teamModule.getPatientManagePrivileges(); 
//		Integer formsManagement = teamModule.getFormsStudyManagementPrivileges(); 
//		Integer formsAccess = teamModule.getFormsStudyAccessPrivileges(); 
//		
//		boolean hasViewPermissions = TeamAuthModule.hasViewPermission(formsManagement|formsAccess); 
//		
//		if(!hasViewPermissions)
//		{
//			addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User Not Authorized to view Form Design"));
//			throw new AuthorizationException("User Not Authorized to view Form Design");
//	
//		}
//		
//		return hasViewPermissions; 
//		
//	}
//	
	

}
