
package com.velos.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Issues implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = -766020278646001289L;
    
    protected List<Issue> issue = new ArrayList<Issue>();
 
    public Issues() {}
    
    public void add(Issue issue){
        this.issue.add(issue);
    }
    
    public void addAll(List<Issue> issue) {
        this.issue = issue;
    }
    
    public List<Issue> getIssue() {
        return this.issue;
    }
    
    public void setIssue(List<Issue> issue) {
        this.issue = issue;
    }
}
