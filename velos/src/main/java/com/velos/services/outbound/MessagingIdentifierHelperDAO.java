/**
 * Created On Mar 29, 2011
 */
package com.velos.services.outbound;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;


import com.velos.eres.business.common.CommonDAO;

/**
 * @author Kanwaldeep
 *
 */
public class MessagingIdentifierHelperDAO extends CommonDAO{
	private static Logger logger = Logger.getLogger(MessagingIdentifierHelperDAO.class); 
	
	private String GET_STUDY_SQL = "select study_number from er_study where pk_study = ?"; 
	
	private String GET_FORM_SQL = "select form_name from ER_FORMLIB where PK_FORMLIB=? and RECORD_TYPE <>'D'"; 
	
	private String GET_USER_SQL = "select usr_lastname, usr_firstname, usr_logname from er_user where pk_user = ?"; 
	
	private String GET_PAT_ASSOC_STUDY_SQL = "select study_number from er_study where pk_study = (select fk_study from er_patprot where pk_patprot = ?)";
	  
	private String GET_PAT_ID_SQL = "select per_code from er_per where pk_per = (select fk_per from er_patprot where pk_patprot = ?)";
	  
	private String GET_SITE_NAME_SQL = "select site_name from er_site where pk_site = (select fk_site_enrolling from er_patprot where pk_patprot=?)";
	
	public String getStudyNumber(int pkStudy)
	{
		PreparedStatement stmt = null; 
		ResultSet rs = null; 
		Connection conn = null ;
		String studyNumber = null; 
		try
		{
			conn = getConnection(); 
			stmt = conn.prepareStatement(GET_STUDY_SQL); 
			stmt.setInt(1, pkStudy); 
			
			rs = stmt.executeQuery(); 
			
			if(rs.next())
			{
				studyNumber = rs.getString("study_number"); 
			}
			
		}catch(SQLException sqe)
		{
			logger.error("Unable to get Study Number for study "+sqe); 
		}finally
		{
			if(rs != null)
			{
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
			returnConnection(conn); 
		}
		
		return studyNumber; 
	}
	
	public String getFormName(int pkForm)
	{
		PreparedStatement stmt = null; 
		ResultSet rs = null; 
		Connection conn = null ;
		String formName = null; 
		try
		{
			conn = getConnection(); 
			stmt = conn.prepareStatement(GET_FORM_SQL); 
			stmt.setInt(1, pkForm); 
			
			rs = stmt.executeQuery(); 
			
			if(rs.next())
			{
				formName = rs.getString("form_name"); 
			}
			
		}catch(SQLException sqe)
		{
			logger.error("Unable to get Form Name for form "+sqe); 
		}finally
		{
			if(rs != null)
			{
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
			returnConnection(conn); 
		}
		
		return formName; 
	}
	
	public Map<String, String> getUserInformation(int pkUser)
	{
		Map<String, String> userinfo = new HashMap<String, String>(); 
		PreparedStatement stmt = null; 
		ResultSet rs = null; 
		Connection conn = null ;
		String studyNumber = null; 
		try
		{
			conn = getConnection(); 
			stmt = conn.prepareStatement(GET_USER_SQL); 
			stmt.setInt(1, pkUser); 
			
			rs = stmt.executeQuery(); 
			if(rs.next())
			{
				userinfo.put("firstname", rs.getString(2)); 
				userinfo.put("lastname", rs.getString(1)); 
				userinfo.put("logname", rs.getString(3)); 
			}
			
		}catch(SQLException sqe)
		{
			logger.error("Unable to get Study Number for study "+sqe); 
		}finally
		{
			if(rs != null)
			{
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
			returnConnection(conn); 
		}
		
		return userinfo; 	
	}
	
	public String getPatientAssocStudyNumber(int pkPatProt)
	  {
	    System.out.println("********MessagingIdentifierHelperDAO-getPatientAssocStudyNumber()");
	    PreparedStatement stmt = null;
	    ResultSet rs = null;
	    Connection conn = null;
	    String studyNumber = null;
	    try
	    {
	      conn = getConnection();
	      stmt = conn.prepareStatement(GET_PAT_ASSOC_STUDY_SQL);
	      stmt.setInt(1, pkPatProt);
	      
	      rs = stmt.executeQuery();
	      
	      if (rs.next())
	      {
	        studyNumber = rs.getString("study_number");
	        System.out.println("********MessagingIdentifierHelperDAO-getPatientAssocStudyNumber()==" + studyNumber);
	      }
	    }
	    catch (SQLException sqe)
	    {
	      logger.error("Unable to get Patient associated Study Number " + sqe);
	    }
	    finally {
	      if (rs != null) {
	        try
	        {
	          rs.close();
	        }
	        catch (SQLException e) {
	          e.printStackTrace();
	        }
	      }
	      if (stmt != null) {
	        try
	        {
	          stmt.close();
	        }
	        catch (SQLException e) {
	          e.printStackTrace();
	        }
	      }
	      returnConnection(conn);
	    }
	    
	    return studyNumber;
	  }
	  
	  public String getPatientId(int pkPatProt) {
	    System.out.println("********MessagingIdentifierHelperDAO-getPatientID()");
	    PreparedStatement stmt = null;
	    ResultSet rs = null;
	    Connection conn = null;
	    String patientId = null;
	    try
	    {
	      conn = getConnection();
	      stmt = conn.prepareStatement(GET_PAT_ID_SQL);
	      stmt.setInt(1, pkPatProt);
	      
	      rs = stmt.executeQuery();
	      
	      if (rs.next())
	      {
	        patientId = rs.getString("per_code");
	        System.out.println("********MessagingIdentifierHelperDAO-getPatientID()==" + patientId);
	      }
	    }
	    catch (SQLException sqe)
	    {
	      logger.error("Unable to get Patient ID " + sqe);
	    }
	    finally {
	      if (rs != null) {
	        try
	        {
	          rs.close();
	        }
	        catch (SQLException e) {
	          e.printStackTrace();
	        }
	      }
	      if (stmt != null) {
	        try
	        {
	          stmt.close();
	        }
	        catch (SQLException e) {
	          e.printStackTrace();
	        }
	      }
	      returnConnection(conn);
	    }
	    
	    return patientId;
	  }
	  
	  public String getSiteName(int pkPatProt) {
	    System.out.println("********MessagingIdentifierHelperDAO-getSiteName()");
	    PreparedStatement stmt = null;
	    ResultSet rs = null;
	    Connection conn = null;
	    String siteName = null;
	    try
	    {
	      conn = getConnection();
	      stmt = conn.prepareStatement(GET_SITE_NAME_SQL);
	      stmt.setInt(1, pkPatProt);
	      
	      rs = stmt.executeQuery();
	      
	      if (rs.next())
	      {
	        siteName = rs.getString("site_name");
	        System.out.println("********MessagingIdentifierHelperDAO-getSiteName()==" + siteName);
	      }
	    }
	    catch (SQLException sqe)
	    {
	      logger.error("Unable to get Study Number for study " + sqe);
	    }
	    finally {
	      if (rs != null) {
	        try
	        {
	          rs.close();
	        }
	        catch (SQLException e) {
	          e.printStackTrace();
	        }
	      }
	      if (stmt != null) {
	        try
	        {
	          stmt.close();
	        }
	        catch (SQLException e) {
	          e.printStackTrace();
	        }
	      }
	      returnConnection(conn);
	    }
	    
	    return siteName;
	  }
	  
	  public String getFormResType(int formResPK)
		{
			PreparedStatement stmt = null; 
			ResultSet rs = null; 
			Connection conn = null ;
			String FormResType = ""; 
			try
			{
				conn = getConnection(); 
				stmt = conn.prepareStatement("select FK_PER,FK_PATPROT,FK_SCH_EVENTS1 from er_patforms where PK_PATFORMS=?"); 
				stmt.setInt(1, formResPK); 
				
				rs = stmt.executeQuery(); 
				
				if(rs.next())
				{   Integer patPK = rs.getInt("FK_PER");
					Integer patProtPK = rs.getInt("FK_PATPROT");
					Integer patSCHPK = rs.getInt("FK_SCH_EVENTS1");
					if(patSCHPK>0 && patProtPK>0)
						FormResType = "PS";
					else if(patProtPK>0 && patPK>0)
						FormResType = "SP";
					else
						FormResType = "P";
				}
				
			}catch(SQLException sqe)
			{
				logger.error("Unable to get Study Number for study "+sqe); 
			}finally
			{
				if(rs != null)
				{
					try {
						rs.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
				}
				if(stmt != null)
				{
					try{
						stmt.close(); 
					}catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
				}
				returnConnection(conn); 
			}
			
			return FormResType; 
		}

}
