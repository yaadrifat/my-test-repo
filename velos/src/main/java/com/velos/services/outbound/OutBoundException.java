/**
 * Created On Mar 31, 2011
 */
package com.velos.services.outbound;

/**
 * @author Kanwaldeep
 *
 */
public class OutBoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6611540234554300781L;
	
	private int code = 0;  

	public OutBoundException (int code , String message){
		super(message);
		this.code = code; 		
	}
	
	public int getErrorCode()
	{
		return code; 
	}
}
