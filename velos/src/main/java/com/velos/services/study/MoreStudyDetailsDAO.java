/*Virendra
 * 
 * 
 * 
 */

package com.velos.services.study;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.services.OperationException;
import com.velos.services.model.MoreDetailValue;
import com.velos.services.model.MoreStudyDetails;
import com.velos.services.model.NVPair;
import com.velos.services.model.StudySummary;
import com.velos.services.util.CodeCache;


public class MoreStudyDetailsDAO extends CommonDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8410547449723597051L;
	private static Logger logger = Logger.getLogger(MoreStudyDetailsDAO.class);

	private static String studyStatusSql = "select pk_studyid, pk_codelst, codelst_subtyp," +
			" codelst_desc, STUDYID_ID," +
			" codelst_seq,codelst_custom_col " +
			",codelst_custom_col1 from  er_studyid, er_codelst "  +
			" where fk_study = ? and  pk_codelst = FK_CODELST_IDTYPE  and" +
			" codelst_type = 'studyidtype'" +
			"and not exists (select * from er_codelst_hide h where h.codelst_type = 'studyidtype' and h.fk_grp = ? and h.fk_codelst = pk_codelst and h.CODELST_HIDE_TABLE = 1) ";
	
	 public static Map<Integer, NVPair> getStudyMoreStudyDetails(int studyPK, int usersDefaultGroup) 
	 
	 throws 
	 OperationException{

	        PreparedStatement pstmt = null;
			Connection conn = null;
	        try
	        {
	        	conn = getConnection();
				if (logger.isDebugEnabled()) logger.debug("com.velos.services.study.StudyStatusDAO.getStudy sql:" + studyStatusSql);
				
				pstmt = conn.prepareStatement(studyStatusSql);
				pstmt.setInt(1, studyPK);
				pstmt.setInt(2, usersDefaultGroup);
				
				ResultSet rs = pstmt.executeQuery();
				
				Map<Integer, NVPair> MSDMap = new HashMap<Integer, NVPair>();
				
				while (rs.next()) {
					String MSDKey = rs.getString("codelst_subtyp");
					if (MSDKey != null && MSDKey.length() > 0){
						Integer MSDPK = rs.getInt("pk_studyid");
						NVPair item = new NVPair();
						String MSDValue = rs.getString("STUDYID_ID");
						item.setKey(MSDKey);
						item.setValue(MSDValue);
						item.setType(StudySummary.MORE_STUDY_DETAILS_NS);
						item.setDescription(rs.getString("CODELST_DESC"));
						MSDMap.put(MSDPK, item);
					}
				}
				return MSDMap;
	        }
	        catch (SQLException ex) {
				logger.fatal("StudyStatusDAO.getStudy EXCEPTION IN FETCHING FROM er_studystatus"
						+ ex);
				throw new OperationException(ex);
			} finally {
				try {
					if (pstmt != null)
						pstmt.close();
				} catch (Exception e) {
				}
				try {
					if (conn != null)
						conn.close();
				} catch (Exception e) {
				}

			}

	 }
	        
	
	public static MoreDetailValue  getStudyMoreDetailValue(int studyPK, int usersDefaultGroup,String searchParameter )
			throws 
			 OperationException{
		boolean resultData=false;
		MoreDetailValue moreDetailVlaue=null;
		String MSDSql="select pk_studyid, pk_codelst, codelst_subtyp," +
				" codelst_desc, STUDYID_ID," +
				" codelst_seq,codelst_custom_col " +
				",codelst_custom_col1 from  er_studyid, er_codelst "  +
				" where fk_study = ? and  pk_codelst = FK_CODELST_IDTYPE  and" +
				" codelst_type = 'studyidtype' " +
				" and codelst_subtyp=?"+
				"and not exists (select * from er_codelst_hide h where h.codelst_type = 'studyidtype' and h.fk_grp = ? and h.fk_codelst = pk_codelst and h.CODELST_HIDE_TABLE = 1) ";
		
		System.out.println("reached in getstudymoredetailvalue");
		PreparedStatement pstmt = null;
		Connection conn = null;
	    try
        {
        	conn = getConnection();
			if (logger.isDebugEnabled()) logger.debug("com.velos.services.study.StudyStatusDAO.getStudy sql:" + studyStatusSql);
			
			pstmt = conn.prepareStatement(MSDSql);
			pstmt.setInt(1, studyPK);
			pstmt.setString(2, searchParameter);
			pstmt.setInt(3, usersDefaultGroup);
			
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
				moreDetailVlaue=new MoreDetailValue();
				resultData=true;
				String MSDKey = rs.getString("codelst_subtyp");
				if (MSDKey != null && MSDKey.length() > 0){
					Integer MSDPK = rs.getInt("pk_studyid");
					String MSDValue = rs.getString("STUDYID_ID");
					moreDetailVlaue.setKey(MSDKey);
					moreDetailVlaue.setValue(MSDValue);
				}
			}/*
			if(resultData!=true){
				moreDetailVlaue.setKey(searchParameter);
				moreDetailVlaue.setValue("");
			}*/
			
        }
        catch (SQLException ex) {
			logger.fatal("StudyStatusDAO.getStudy EXCEPTION IN FETCHING FROM er_studystatus"
					+ ex);
			
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
		return moreDetailVlaue;
	}
	
	public static NVPair getUserMoreDetailValue(int userPK, int usersDefaultGroup,String searchParameter )
			throws 
			 OperationException{
		boolean resultData=false;
		NVPair moreDetailList=new NVPair();
		NVPair moreDetailVlaue=null;
		String MSDSql="Select pk_moredetails, pk_codelst, codelst_subtyp, codelst_desc, md_modelementdata,'M' record_type, codelst_seq,codelst_custom_col" +
				" ,codelst_custom_col1 from  er_moredetails, er_codelst " +
				"where fk_modpk = ? and  pk_codelst = md_modelementpk  and  " +
				"codelst_type = 'user' and codelst_subtyp=? ";
				
		
		System.out.println("reached in getUserMoreDetailvalue");
		PreparedStatement pstmt = null;
		Connection conn = null;
	    try
        {
        	conn = getConnection();
			if (logger.isDebugEnabled()) logger.debug("com.velos.services.study.StudyStatusDAO.getStudy sql:" + studyStatusSql);
			
			pstmt = conn.prepareStatement(MSDSql);
			pstmt.setInt(1, userPK);
			pstmt.setString(2, searchParameter);
			
			
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
				moreDetailVlaue=new NVPair();
				resultData=true;
				String MSDKey = rs.getString("codelst_subtyp");
				if (MSDKey != null && MSDKey.length() > 0){
					//Integer MSDPK = rs.getInt("codelst_subtyp");
					String MSDValue = rs.getString("md_modelementdata");
					moreDetailVlaue.setKey(MSDKey);
					moreDetailVlaue.setValue(MSDValue);
				}
			}
			if(resultData!=true){
				moreDetailVlaue=new NVPair();
				moreDetailVlaue.setKey(searchParameter);
				moreDetailVlaue.setValue("");
			}
			
        }
        catch (SQLException ex) {
			logger.fatal("StudyStatusDAO.getStudy EXCEPTION IN FETCHING FROM er_studystatus"
					+ ex);
			
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
		return moreDetailVlaue;
	}
	
	public static List<NVPair> getUserMoreDetailValue(int userPK, int usersDefaultGroup,String searchParameter, String value )
			throws 
			 OperationException{
		boolean resultData=false;
		List<NVPair> moreDetailList=new ArrayList<NVPair>();
		NVPair moreDetailVlaue=null;
		String MSDSql="Select pk_moredetails, pk_codelst, codelst_subtyp, codelst_desc, md_modelementdata,'M' record_type, codelst_seq,codelst_custom_col" +
				" ,codelst_custom_col1 from  er_moredetails, er_codelst " +
				"where fk_modpk = ? and  pk_codelst = md_modelementpk  and  " +
				"codelst_type = 'user' ";
		if(!searchParameter.equals("")){
			MSDSql=MSDSql+" and codelst_subtyp in ("+searchParameter+")";
		}
		if(!value.equals("")){
			MSDSql=MSDSql+" and md_modelementdata in ("+value+")";
		}
		System.out.println("MSDSql==="+MSDSql);
		System.out.println("reached in getUserMoreDetailvalue");
		PreparedStatement pstmt = null;
		Connection conn = null;
	    try
        {
        	conn = getConnection();
			if (logger.isDebugEnabled()) logger.debug("com.velos.services.study.StudyStatusDAO.getStudy sql:" + studyStatusSql);
			
			pstmt = conn.prepareStatement(MSDSql);
			pstmt.setInt(1, userPK);
			
			
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
				moreDetailVlaue=new NVPair();
				resultData=true;
				String MSDKey = rs.getString("codelst_subtyp");
				System.out.println("MSDKey==="+MSDKey);
				if (MSDKey != null && MSDKey.length() > 0){
					//Integer MSDPK = rs.getInt("codelst_subtyp");
					String MSDValue = rs.getString("md_modelementdata");
					moreDetailVlaue.setKey(MSDKey);
					System.out.println("MSDValue==="+MSDValue);
					moreDetailVlaue.setValue(MSDValue);
				}
				//Kavitha: #28969
				//condition is to avoid empty <moreDetails/> response
				if(moreDetailVlaue.getKey()!=null){
				  moreDetailList.add(moreDetailVlaue);
				}
			}
			//Kavitha: #28969
			/*if(resultData!=true){
				System.out.println("adding empty key values--------");
				moreDetailVlaue=new NVPair();
				moreDetailVlaue.setKey(searchParameter);
				moreDetailVlaue.setValue("");
				moreDetailList.add(moreDetailVlaue);
			}*/
			
			
        }
        catch (SQLException ex) {
			logger.fatal("StudyStatusDAO.getStudy EXCEPTION IN FETCHING FROM er_studystatus"
					+ ex);
			
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
		return moreDetailList;
	}
	
	public static List<NVPair> getStudyTeamMDValue(Integer teamMemberPK, String searchParameter, String value )
			throws 
			 OperationException{
		boolean resultData=false;
		List<NVPair> moreDetailList=new ArrayList<NVPair>();
		NVPair moreDetailVlaue=null;
		String MSDSql="Select pk_moredetails, pk_codelst, codelst_subtyp, codelst_desc, md_modelementdata,'M' record_type, codelst_seq,codelst_custom_col" +
				" ,codelst_custom_col1 from  er_moredetails, er_codelst " +
				"where fk_modpk = ? and  pk_codelst = md_modelementpk  and  " +
				"codelst_type = 'studyteam' ";
		if(!searchParameter.equals("")){
			MSDSql=MSDSql+" and codelst_subtyp in ("+searchParameter+")";
		}
		if(!value.equals("")){
			MSDSql=MSDSql+" and md_modelementdata in ("+value+")";
		}
		System.out.println("MSDSql==="+MSDSql);
		System.out.println("reached in getUserMoreDetailvalue");
		PreparedStatement pstmt = null;
		Connection conn = null;
	    try
        {
        	conn = getConnection();
			if (logger.isDebugEnabled()) logger.debug("com.velos.services.study.StudyStatusDAO.getStudy sql:" + studyStatusSql);
			
			pstmt = conn.prepareStatement(MSDSql);
			pstmt.setInt(1, teamMemberPK);
			
			
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
				moreDetailVlaue=new NVPair();
				resultData=true;
				String MSDKey = rs.getString("codelst_subtyp");
				System.out.println("MSDKey==="+MSDKey);
				if (MSDKey != null && MSDKey.length() > 0){
					//Integer MSDPK = rs.getInt("codelst_subtyp");
					String MSDValue = rs.getString("md_modelementdata");
					moreDetailVlaue.setKey(MSDKey);
					System.out.println("MSDValue==="+MSDValue);
					moreDetailVlaue.setValue(MSDValue);
				}
				moreDetailList.add(moreDetailVlaue);
			}
			/*if(resultData!=true){
				moreDetailVlaue=new NVPair();
				moreDetailVlaue.setKey(searchParameter);
				moreDetailVlaue.setValue("");
				moreDetailList.add(moreDetailVlaue);
			}*/
			
			
        }
        catch (SQLException ex) {
			logger.fatal("StudyStatusDAO.getStudy EXCEPTION IN FETCHING FROM er_studystatus"
					+ ex);
			
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
		return moreDetailList;
	}
	
	public static List<NVPair>  getNetworkMoreDetailValue(int networkPK, HashMap<String,Object> searchMap, String level ,String usersDefaultGroup )
			throws 
			 OperationException{
		boolean resultData=false;
		String key=(String)searchMap.get("key");
		String networkUser=(String)searchMap.get("user");
		networkUser=networkUser==null?"":networkUser;
		key=key==null?"":key;
		String data=""; 
		Object value=(Object)searchMap.get("value");
		if(value!=null && value!=""){
		org.apache.xerces.dom.ElementNSImpl val=(org.apache.xerces.dom.ElementNSImpl)value;
		org.w3c.dom.NodeList node= val.getChildNodes();
        for(int j=0;j< node.getLength();j++)
           {
        	  org.w3c.dom.Node node1=node.item(j);
        	  data=node1.getNodeValue()==null?"":node1.getNodeValue();
           }
		}
		List<NVPair> moreDetailList=new ArrayList<NVPair>();
		NVPair moreDetailVlaue=null;
		String MSDSql="";
		if(!networkUser.equals("")){
			MSDSql="Select pk_moredetails, pk_codelst, codelst_subtyp, codelst_desc, md_modelementdata,'M' record_type, codelst_seq,codelst_custom_col" +
					" ,codelst_custom_col1 from  er_moredetails, er_codelst " +
					"where fk_modpk = ? and  pk_codelst = md_modelementpk  and  " +
					"codelst_type = 'user_"+level+"' and codelst_subtyp=? and lower(MD_MODELEMENTDATA) like lower('%"+data+"%')"+
					" and not exists (select * from er_codelst_hide h where h.codelst_type = 'user_"+level+"' and h.fk_grp = ? and h.fk_codelst = pk_codelst and h.CODELST_HIDE_TABLE = 1) ";	
		
		}else{
				if(key.equals("")){
					MSDSql="Select pk_moredetails, pk_codelst, codelst_subtyp, codelst_desc, md_modelementdata,'M' record_type, codelst_seq,codelst_custom_col" +
							" ,codelst_custom_col1 from  er_moredetails, er_codelst " +
							"where fk_modpk = ? and  pk_codelst = md_modelementpk  and  " +
							"codelst_type = 'org_"+level+"'"+
							" and not exists (select * from er_codelst_hide h where h.codelst_type = 'org_"+level+"' and h.fk_grp = ? and h.fk_codelst = pk_codelst and h.CODELST_HIDE_TABLE = 1) ";
				}else{
					MSDSql="Select pk_moredetails, pk_codelst, codelst_subtyp, codelst_desc, md_modelementdata,'M' record_type, codelst_seq,codelst_custom_col" +
							" ,codelst_custom_col1 from  er_moredetails, er_codelst " +
							"where fk_modpk = ? and  pk_codelst = md_modelementpk  and  " +
							"codelst_type = 'org_"+level+"' and codelst_subtyp=? and lower(MD_MODELEMENTDATA) like lower('%"+data+"%')"+
							" and not exists (select * from er_codelst_hide h where h.codelst_type = 'org_"+level+"' and h.fk_grp = ? and h.fk_codelst = pk_codelst and h.CODELST_HIDE_TABLE = 1) ";	
				}}
		
		System.out.println("reached in getNetworkMoreDetails");
		PreparedStatement pstmt = null;
		Connection conn = null;
	    try
        {
        	conn = getConnection();
			if (logger.isDebugEnabled()) logger.debug("com.velos.services.study.StudyStatusDAO.getStudy sql:" + studyStatusSql);
			
			pstmt = conn.prepareStatement(MSDSql);
			pstmt.setInt(1, networkPK);
			if(key.equalsIgnoreCase("") ){
				pstmt.setString(2, usersDefaultGroup);
				}
			if(!key.equalsIgnoreCase("") ){
			pstmt.setString(2, key);
			pstmt.setString(3, usersDefaultGroup);
			}
			
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
				moreDetailVlaue=new NVPair();
				resultData=true;
				String MSDKey = rs.getString("codelst_subtyp");
				if (MSDKey != null && MSDKey.length() > 0){
					//Integer MSDPK = rs.getInt("codelst_subtyp");
					String MSDValue = rs.getString("md_modelementdata");
					moreDetailVlaue.setKey(MSDKey);
					moreDetailVlaue.setValue(MSDValue);
					moreDetailList.add(moreDetailVlaue);
				}
			}
			// fix for 28951 bug
			/*if(resultData!=true){
				moreDetailVlaue=new NVPair();
				moreDetailVlaue.setKey(key);
				moreDetailVlaue.setValue("");
				moreDetailList.add(moreDetailVlaue);
			}*/
			
        }
        catch (SQLException ex) {
			logger.fatal("StudyStatusDAO.getStudy EXCEPTION IN FETCHING FROM er_studystatus"
					+ ex);
			
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
		return moreDetailList;
	}
	

}
