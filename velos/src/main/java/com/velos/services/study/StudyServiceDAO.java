/**
 * 
 * Data Access Object responsible for fetching  StudyService
 * 
 */
package com.velos.services.study;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.UserDao;
import com.velos.eres.business.site.impl.SiteBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.StudySearch;
import com.velos.services.model.UserIdentifier;
import com.velos.services.util.CodeCache;


public class StudyServiceDAO extends CommonDAO {

	
	private static final long serialVersionUID = 7548543001098480469L;
	private static Logger logger = Logger.getLogger(StudyServiceDAO.class);
	
	public static List<StudySearch> searchStudy(String mainSql, Map<String, Object> parameters) throws OperationException{

 	        Rlog.debug("studystatus", "In DAO: com.velos.services.study.StudyStatusDAO;");
 	        String countSql= "select count(*) from ( " + mainSql  + " )";
	        PreparedStatement pstmt = null;
			Connection conn = null;
	        try
	        {
	        	conn = getConnection();
	        	CodeCache codeCache = CodeCache.getInstance();
	        	UserDao userDao=new UserDao();
	        	
				if (logger.isDebugEnabled()) logger.debug("com.velos.services.study.StudyServiceDAO.searchStudy sql:" + mainSql);
				
				pstmt = conn.prepareStatement(mainSql);
				ResultSet rs = pstmt.executeQuery();
				
				ArrayList<StudySearch> studySearchs = new ArrayList<StudySearch>();
				StudySearch studySearch=null;
				while (rs.next()) 
				{
					
					/*Code studyStatusCodeType = 
						codeCache.getCodeSubTypeByPK(
								CodeCache.CODE_TYPE_STATUS_TYPE, 
								rs.getInt("STATUS_TYPE"),
								userAccountPK1);
					*/
					studySearch=new StudySearch();
					studySearch.setCurrentStudyStatus(codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_STATUS, rs.getString("CURRENT_STAT_DESC"), StringUtil.stringToNum(((UserBean) parameters.get("callingUser")).getUserAccountId())));
					studySearch.setDisplayedStudyStatus(codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_STATUS, rs.getString("STATUS"), StringUtil.stringToNum(((UserBean) parameters.get("callingUser")).getUserAccountId())));
					studySearch.setDivision(codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_DIVISION, rs.getString("STUDY_DIVISION_DESC"), StringUtil.stringToNum(((UserBean) parameters.get("callingUser")).getUserAccountId())));
					studySearch.setPhase(codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PHASE, rs.getString("PHASE"), StringUtil.stringToNum(((UserBean) parameters.get("callingUser")).getUserAccountId())));
					UserBean userBean =((UserAgentRObj) parameters.get("userAgent")).getUserDetails(rs.getInt("PI_NAME"));
					if(userBean!=null)
					{
						UserIdentifier userId=new UserIdentifier();
						userId.setUserLoginName(userBean.getUserLoginName());
						userId.setOID(((ObjectMapService) parameters.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_USER, userBean.getUserId()).getOID());
						userId.setPK(userBean.getUserId());
						userId.setFirstName(userBean.getUserFirstName());
						userId.setLastName(userBean.getUserLastName());
						studySearch.setPrincipalInvestigator(userId);
					}
					SiteBean siteBean=((SiteAgentRObj) parameters.get("siteAgent")).getSiteDetails(rs.getInt("STUDY_SITES"));
					if(siteBean!=null)
					{
						OrganizationIdentifier organization= new OrganizationIdentifier();
						organization.setSiteName(siteBean.getSiteName());
						organization.setOID(((((ObjectMapService) parameters.get("objectMapService")).getOrCreateObjectMapFromPK("er_site",siteBean.getSiteId() )).getOID()));
						organization.setPK(siteBean.getSiteId()); 
						studySearch.setStudyOrganization(organization);
						
					}
					studySearch.setStudyNumber(rs.getString("STUDY_NUMBER"));
					studySearch.setStudyTitle(rs.getString("STUDY_TITLE"));
					studySearch.setTherapeuticArea(codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_THERAPUTIC_AREA, rs.getString("TAREA"), StringUtil.stringToNum(((UserBean) parameters.get("callingUser")).getUserAccountId())));
					
					//studySearch.setPageNumber(0);
					//studySearch.setPageSize(0);
					
					
					studySearchs.add(studySearch);
					
				}
				return studySearchs;
	        }
	        catch (SQLException ex) {
				logger.fatal("StudyStatusDAO.getStudy " +
						"EXCEPTION IN FETCHING FROM er_studystatus"
						+ ex);
				throw new OperationException(ex);
			} finally {
				try {
					if (pstmt != null)
						pstmt.close();
				} catch (Exception e) {
				}
				try {
					if (conn != null)
						conn.close();
				} catch (Exception e) {
				}

			}

	 }
	
	public static Integer getStudyNumAutoGenFlag(String mainSql, String accId) throws OperationException{

        PreparedStatement pstmt = null;
		Connection conn = null;
		Integer autoGenFlag=0;
        try
        {
        	conn = getConnection();
        	
			if (logger.isDebugEnabled()) logger.debug("com.velos.services.study.StudyServiceDAO.getStudyNumAutoGenFlag sql:" + mainSql);
			
			pstmt = conn.prepareStatement(mainSql);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) 
			{
				autoGenFlag = rs.getInt("AC_AUTOGEN_STUDY");
			}
			return autoGenFlag;
        }
        catch (SQLException ex) {
			logger.fatal("StudyServiceDAO.getStudyNumAutoGenFlag " +
					"EXCEPTION IN FETCHING FROM er_account"
					+ ex);
			throw new OperationException(ex);
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}

 }
	        
	public static String getStudyNum(String accId,ResponseHolder response) throws OperationException{

		CallableStatement cstmt = null;
		Connection conn = null;
		String studyNum = "";
        try
        {
        	conn = getConnection();
        	
			if (logger.isDebugEnabled()) logger.debug("com.velos.services.study.StudyServiceDAO.getStudyNum AccountId:" + accId);
			
			cstmt = conn.prepareCall("{call SP_AUTOGEN_STUDY(?,?)}");
            cstmt.setInt(1, StringUtil.stringToInteger(accId));
            cstmt.registerOutParameter(2, java.sql.Types.VARCHAR);
            cstmt.execute();
            studyNum = cstmt.getString(2);
			return studyNum;
        }
        catch (SQLException ex) {
			logger.fatal("StudyServiceDAO.getStudyNum " +
					"EXCEPTION IN FETCHING FROM SP_AUTOGEN_STUDY"
					+ ex);
			response.addIssue(new Issue(IssueTypes.STUDY_NUMBER_NOT_FOUND,"Study Number May not be null."));
			throw new OperationException(ex);
		} finally {
			try {
				if (cstmt != null)
					cstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}

 }

	
	

}
