package com.velos.services.user;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import com.velos.eres.business.common.UserDao;
import com.velos.eres.business.moreDetails.impl.MoreDetailsBean;
import com.velos.eres.business.site.impl.SiteBean;
import com.velos.eres.business.statusHistory.impl.StatusHistoryBean;
import com.velos.eres.service.statusHistoryAgent.impl.StatusHistoryAgentBean;
import com.velos.eres.service.userAgent.UserAgentRObj;

import org.apache.poi.util.SystemOutLogger;
import org.apache.xerces.dom.ElementNSImpl;
import org.json.JSONObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.MoreDetailsDao;
import com.velos.eres.business.common.NetworkDao;
import com.velos.eres.business.common.StatusHistoryDao;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.util.BrowserRows;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.EnvUtil;
import com.velos.eres.service.util.FilterUtil;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.moreDetails.MoreDetailsJB;
import com.velos.eres.web.site.SiteJB;
import com.velos.services.AbstractService;
import com.velos.services.CRUDAction;
import com.velos.services.CodeNotFoundException;
import com.velos.services.CompletedAction;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.NetworkSiteDetails;
import com.velos.services.model.NetworkSiteLevelDetails;
import com.velos.services.model.NetworkSiteSearchResults;
import com.velos.services.model.NetworkSiteUserDetails;
import com.velos.services.model.Networks;
import com.velos.services.model.NetworksDetails;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.RemoveNetwork;
import com.velos.services.model.RemoveNetworkSite;
import com.velos.services.model.Site;
import com.velos.services.model.SiteIdentifier;
import com.velos.services.model.SiteLevelDetails;
import com.velos.services.model.Sites;
import com.velos.services.model.UpdatNetworkSiteDetails;
import com.velos.services.model.UpdateNetwork;
import com.velos.services.model.UserDetails;
import com.velos.services.model.UserIdentifier;
import com.velos.services.model.UserNetworkSiteResults;
import com.velos.services.model.UserNetworkSites;
import com.velos.services.model.UserNetworks;
import com.velos.services.model.UserNetworksDetail;
import com.velos.services.model.UserRoleIdentifier;
import com.velos.services.model.UserRoles;
import com.velos.services.model.UserSite;
import com.velos.services.model.UserSites;

import com.velos.services.model.Code;
import com.velos.services.model.CreateNetworkSiteUser;
import com.velos.services.model.MoreNetworkUserDetails;
import com.velos.services.model.NVPair;
import com.velos.services.model.NetSiteDetails;

import com.velos.services.model.Network;
import com.velos.services.model.NetworkIdentifier;
import com.velos.services.model.NetworkIdentifierResult;
import com.velos.services.model.NetworkLevelSite;
import com.velos.services.model.NetworkParentSite;
import com.velos.services.model.NetworkRemoveSite;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;

import java.sql.CallableStatement;
import java.sql.Connection;
import com.velos.services.model.NetworkRole;
import com.velos.services.model.NetworkSite;
import com.velos.services.model.NetworkSiteData;
import com.velos.services.model.NetworkSiteDetail;
import com.velos.services.model.NetworkSites;
import com.velos.services.model.NetworkSitesUsers;
import com.velos.services.model.NetworkUserDetail;
import com.velos.services.model.NetworkUserDetails;

import com.velos.services.model.Roles;
import com.velos.services.model.SimpleIdentifier;
import com.velos.services.model.UpdateNetwrokUsersWrapper;
import com.velos.services.model.UsersNetworkSites;
import com.velos.services.model.NetSiteUser;
import com.velos.services.model.NetworkUsersRoles;
public class NetworkServiceDao extends CommonDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6495019937807678233L;
	
	public static final String KEY_ACCOUNT_ID = "accountId";
	//public static final String KEY_USER_ID = "userId";
	public static final String KEY_PAGE_SIZE = "pageSize";
	public static final String KEY_ORDER_BY = "orderBy";
	public static final String KEY_ORDER_TYPE = "orderType";
	
	/*For Networks*/
	private static final String ASC_STR = "asc";
	private static final String EMPTY_STR = "";
	private static final String NULL_STR = "null";
	
	
	/*For Networks*/
	private ArrayList<String> NetWorksNameList = new ArrayList<String>();
	private ArrayList<String> NetWorksStatusList = new ArrayList<String>();
	private ArrayList<String> NetWorksRelationshipTypeList = new ArrayList<String>();
	private ArrayList<String> NetWorksCTEPIDList = new ArrayList<String>();
	private ArrayList<String> NetWorksPKList = new ArrayList<String>();
	private ArrayList<String> NetWorksTypeList = new ArrayList<String>();
	private ArrayList<String> NetWorksSubtypeList = new ArrayList<String>();
	private ArrayList<String> NetWorksRelationshipTypeTypeList = new ArrayList<String>();
	private ArrayList<String> NetWorksRelationshipTypeSubtypeList = new ArrayList<String>();
	
	/*For Networks*/
	private static final String COL_NET_NAME = "Network_Name";
	private static final String COL_STATUS_NAME = "Network_Status";
	private static final String COL_RELATIONSHITYPE_NAME = "NETWORK_RELATION";
	private static final String COL_CTEPID_NAME = "CTEP_ID";
	private static final String COL_NTWPK_NAME = "pk_nwsites";
	private static final String COL_STATUS_Type= "Network_Type";
	private static final String COL_STATUS_Subtype= "Network_Subtyp";
	private static final String NETWORK_RELATIONSHITYPE_TYPE_TYPE = "NETWORK_RELATION_TYPE";
	private static final String NETWORK_RELATIONSHITYPE_TYPE_SUBTYPE = "NETWORK_RELATION_SUBTYPE";
	
	
	/*For Networks*/
	public ArrayList<String> getNetWorksNameList() { return NetWorksNameList; }
	public ArrayList<String> getNetWorksStatusList() { return NetWorksStatusList; }
	public ArrayList<String> getNetWorksRelationshipTypeList() { return NetWorksRelationshipTypeList; }
	public ArrayList<String> getNetWorksCTEPIDList() { return NetWorksCTEPIDList; }
	public ArrayList<String> getNetWorksPKList() { return NetWorksPKList; }
	public ArrayList<String> getNetWorksTypeList() { return NetWorksTypeList; }
	public ArrayList<String> getNetWorksSubtypeList() { return NetWorksSubtypeList; }
	public ArrayList<String> getNetWorksRelationshipTypeTypeList() { return NetWorksRelationshipTypeTypeList; }
	public ArrayList<String> getNetWorksRelationshipTypeSubtypeList() { return NetWorksRelationshipTypeSubtypeList; }
	
	
	private Long totalCount = 0L;
	private Long pageSize = 0L;
	private ResponseHolder response=null;

	private List<String> pkNetList=new ArrayList<String>();
	private List<String> fkSiteList=new ArrayList<String>();
	private List<String> pkRelnShipType=new ArrayList<String>();
	private List<String> netNameList=new ArrayList<String>();
	private List<String> netInfoList=new ArrayList<String>();
	private List<String> ctepList = new ArrayList<String>();
	//private List<String> netStatList=new ArrayList<String>();
	private List<Code> netStatList=new ArrayList<Code>();
	private List<Code> siteRelnShipType=new ArrayList<Code>();
	private List<String> siteNameList=new ArrayList<String>();
	private List<String> siteInfoList=new ArrayList<String>();
	private List<String> netLevelList=new ArrayList<String>();
	private List<String> netUserList=new ArrayList<String>();
	private List<ArrayList<Site>> sitesList=new ArrayList<ArrayList<Site>>();
	private List<ArrayList<UserSite>> sitesLists=new ArrayList<ArrayList<UserSite>>();
	private List<ArrayList<NetworkSites>> UsersitesList=new ArrayList<ArrayList<NetworkSites>>();
	private List<ArrayList<NetworkSitesUsers>> NetworksitesusersList=new ArrayList<ArrayList<NetworkSitesUsers>>();
	private List<String> fkuserList=new ArrayList<String>();
	private List<String> pkNwUserList=new ArrayList<String>();
	private Map<String,String> relationShipPkMap = new HashMap<String,String>();

	private Integer pageNumber = 0;
	
	private String roleName = "";
	private String roleType = "";
	private String roleCode = "";
	private String roleDescription = "";
	
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getRoleType() {
		return roleType;
	}
	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public String getRoleDescription() {
		return roleDescription;
	}
	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	public List<String> getPkNetList() {
		return pkNetList;
	}

	public List<String> getFkSiteList() {
		return fkSiteList;
	}

	public List<String> getPkRelnShipType() {
		return pkRelnShipType;
	}
	
	public List<String> getFkUserList() {
		return fkuserList;
	}
	public List<String> getNetNameList() {
		return netNameList;
	}
	public List<String> getNetInfoList() {
		return netInfoList;
	}
	
	public List<Code> getNetStatList() {
		return netStatList;
	}

	public List<Code> getSiteRelnShipType() {
		return siteRelnShipType;
	}
	/*public List<String> getNetStatList() {
		return netStatList;
	}*/
	
	public List<String> getSiteNameList() {
		return siteNameList;
	}
	
	public List<String> getSiteInfoList() {
		return siteInfoList;
	}
	
	public List<String> getNetLevelList() {
		return netLevelList;
	}
	public List<ArrayList<Site>> getSitesList() {
		return sitesList;
	}
	public List<String> getnetUserList() {
		return netUserList;
	}
	
	public Integer getPageNumber() {
		return pageNumber; 
		}
	public Long getTotalCount() {
		return totalCount;
	}
	public Long getPageSize() {
		return pageSize;
	}
public List<ArrayList<NetworkSites>> getSiteList() {
		return UsersitesList;
	}  

	public List<ArrayList<NetworkSitesUsers>> getSiteUsersList() {
		return NetworksitesusersList;
	} 


	public ResponseHolder getResponse() {
		return response;
	}
	
	public ResponseHolder getNetworkResponse() {
		return response;
	}
	
	public List<String> getpkNwUserList() {
		return pkNwUserList;
	}
	public void searchNetworkSites(NetworkSiteDetails networkSiteDetails, HashMap<String, Object> params) throws OperationException {
		Connection conn=null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int count=0;
		String iaccId = (String)params.get(KEY_ACCOUNT_ID);
		UserBean userBean=(UserBean)params.get("callingUser");
		String networkName = (networkSiteDetails.getNetwork_Name()==null)?"":networkSiteDetails.getNetwork_Name();
		String siteName = (networkSiteDetails.getSite_Name()==null)?"":networkSiteDetails.getSite_Name();
		String sitePk= (networkSiteDetails.getSite_PK()==null)?"":networkSiteDetails.getSite_PK();
		String networkPk = (networkSiteDetails.getNetwork_PK()==null)?"":networkSiteDetails.getNetwork_PK();
		String CTEP_ID = (networkSiteDetails.getCTEP_ID()==null)?"":networkSiteDetails.getCTEP_ID();
		String pkCode="";
		
		if(networkSiteDetails.getRelationship_Type() != null && networkSiteDetails.getRelationship_Type().getCode() != null){
			CodeDao cd = new CodeDao();
			pkCode = StringUtil.integerToString(cd.getCodeId(networkSiteDetails.getRelationship_Type().getType(), networkSiteDetails.getRelationship_Type().getCode()));
		}
		
		String str = "";
		String pk_main_network = "";
		String net_pks="";
		List<NVPair> moredetails=networkSiteDetails.getMoreSiteDetails();
		//Collection<NVPair> moreSiteDetails =networkSiteDetails.getMoreSiteDetails();
		//HashMap<String,Object> keySearch=new HashMap<String,Object>();
		
		if(moredetails!=null && moredetails.size()>0){
			net_pks=getNetPkMoreDetails(moredetails,"siteDetails");
			/*networkSiteDetails.setMoreSiteDetails((java.util.List<NVPair>) moreSiteDetails);
			if(moreSiteDetails!=null){
				NVPair getDetail=new NVPair();
				for(int iY=0;iY<networkSiteDetails.getMoreSiteDetails().size();iY++){
					getDetail = networkSiteDetails.getMoreSiteDetails().set(iY, getDetail);
					keySearch.put("key",getDetail.getKey()==null?"":getDetail.getKey());
					keySearch.put("value", getDetail.getValue()==null?"":getDetail.getValue());
				}
				moreSiteDetails.addAll(moreSiteDetails);	
			}*/
		}
		
		
		if(!(siteName.equals("")) || (networkSiteDetails.getRelationship_Type() != null && networkSiteDetails.getRelationship_Type().getCode() != null ) || (moredetails!=null && moredetails.size()>0)){
			str="SELECT distinct nvl(FK_NWSITES_MAIN,pk_nwsites) fk_nwsites_main FROM er_nwsites,er_site WHERE pk_site=fk_site and er_site.fk_account=? ";
			if(!siteName.equals("")){	
				str=str+" and lower(site_name) like lower('%"+siteName+"%') ";
			}
			if(!CTEP_ID.equals("")){
				str=str+" and lower(ctep_id) like lower('%"+CTEP_ID+"%') ";
			}
			if(net_pks.length()>0){
				
					str=str+" and pk_nwsites in ("+net_pks+") ";
				
			}
			if(!(pkCode.equals("0")) && !(pkCode.equals(""))){
				str=str+" and nw_membertype="+pkCode;
				}
			pk_main_network = pkMainNetwork(str,iaccId);
			System.out.println("pk_main_network==="+pk_main_network);
			System.out.println("str==="+str);
		}
		System.out.println("networkPk==="+networkPk);
		System.out.println("networkName==="+networkName);
		
		if(networkPk.equals("") && networkName.equals("") && networkSiteDetails.getRelationship_PK()!=null && !networkSiteDetails.getRelationship_PK().equals("")){
			String getPrimaryNetwork="select fk_nwsites_main FROM er_nwsites WHERE pk_nwsites="+ EJBUtil.stringToNum(networkSiteDetails.getRelationship_PK());
			try {
				conn = CommonDAO.getConnection();
				pstmt = conn.prepareStatement(getPrimaryNetwork);
				rs=pstmt.executeQuery();
				while(rs.next()){
					pk_main_network=String.valueOf(rs.getInt("fk_nwsites_main"));
				}
				System.out.println("new condition pk_main_network==="+pk_main_network);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		if(!networkPk.equals("") && (!pk_main_network.equals("0") || pk_main_network.equals("")) && (pk_main_network.indexOf(networkPk)>-1 || pk_main_network.equals(""))){
			pk_main_network=networkPk;
		}
		
		System.out.println("final pk_main_network==="+pk_main_network);
		
 	 	String str1 = "select pk_nwsites,fk_site,site_name ,site_name as network_name,ctep_id, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,(SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,NW_LEVEL+1 as nw_level from er_nwsites,er_site,er_codelst "+
 	 				  " where pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? and nw_level=0 ";
 	 	if(!(networkName.equals(""))){
 	 			 	 str1=str1+" and lower(site_name) like lower('%"+networkName+"%') ";
 	 		}
 	 	//if(networkName.equals("") && pk_main_network.length()>0)
 	 	if(pk_main_network.length()>0)
 	 	{
 	 		str1=str1+" and pk_nwsites in("+pk_main_network+") ";
 	 	}
 	 	System.out.println("str1==="+str1);
	try {
		conn = CommonDAO.getConnection();
		pstmt = conn.prepareStatement(str1);
		pstmt.setInt(1, StringUtil.stringToNum(iaccId));
		rs = pstmt.executeQuery();
		Code code=null;
		while (rs.next()) {
			code=new Code();
			pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
			fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
			if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
				pkRelnShipType.add(rs.getString("pk_nwsites"));
			}
			else{
				pkRelnShipType.add("0");
			}
			netNameList.add(rs.getString("network_name"));
			code.setType(rs.getString("stat_type"));
			code.setCode(rs.getString("code"));
			code.setDescription(rs.getString("description"));
			netStatList.add(code);
			if(rs.getString("SiteRelationshipType")!=null){
			code=new Code();
			code.setType(rs.getString("SiteRelationshipType"));
			code.setCode(rs.getString("SiteRelationshipSubType"));
			code.setDescription(rs.getString("SiteRelationshipDesc"));
			siteRelnShipType.add(code);
			}
			else{
				siteRelnShipType.add(null);
			}
			siteNameList.add(rs.getString("site_name"));
			netLevelList.add(String.valueOf(rs.getInt("nw_level")));
			if(rs.getString("ctep_id")!=null){
				ctepList.add(rs.getString("ctep_id"));
			}else{
				ctepList.add("");
			}
			count++;
		}
		if(count==0)
		{
			this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found")); 
			throw new OperationException();
		}
		
		String chlSql="";
		Site site=null;
		SiteIdentifier siteIdentifier=null;
		ObjectMap map=null;
		Site sitelvl2=null;
		Site sitelvl3=null;
		Site sitelvl4=null;
		Site sitelvl5=null;
		Site sitelvl6=null;
		Site sitelvl7=null;
		
		List<Site> siteslvl2=null;
		List<Site> siteslvl3=null;
		List<Site> siteslvl4=null;
		List<Site> siteslvl5=null;
		List<Site> siteslvl6=null;
		List<Site> siteslvl7=null;
		MoreDetailsJB mdJB=null;
		MoreDetailsDao mdDao;
		ArrayList idList = new ArrayList();
		ArrayList modElementDescList = new ArrayList();
		ArrayList modElementDataList = new ArrayList();
		ArrayList modElementKeysList = new ArrayList();
		List<NVPair> listNVPair=null;
		NVPair nvpair = null;
		List<Site> siteList=null;
		for(int i=0;i<pkNetList.size();i++){
			siteList=new ArrayList<Site>();
			listNVPair=new ArrayList<NVPair>();
			site=new Site();
			siteIdentifier=new SiteIdentifier();
			map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
			siteIdentifier.setOID(map.getOID());
			siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
			site.setSiteIdentifier(siteIdentifier);
			site.setLevel(String.valueOf(netLevelList.get(i)));
			site.setName(siteNameList.get(i));
			site.setSiteStatus(netStatList.get(i));
			if(!pkRelnShipType.get(i).equals("0") && siteRelnShipType.get(i)!=null){
			site.setRelationshipPK(pkRelnShipType.get(i));
			site.setSiteRelationShipType(siteRelnShipType.get(i));
			}
			if(!ctepList.get(i).equals("")){
				site.setCtepId(ctepList.get(i));
			}
			//if(moreSiteDetails==null){
			mdJB = new MoreDetailsJB();
			mdDao = new MoreDetailsDao();
			mdDao = mdJB.getMoreDetails(EJBUtil.stringToNum(pkNetList.get(i)),"org_"+(EJBUtil.stringToNum(netLevelList.get(i))-1),userBean.getUserGrpDefault());
			idList = mdDao.getId();
			modElementDescList = mdDao.getMdElementDescs();
			modElementDataList = mdDao.getMdElementValues();
			modElementKeysList = mdDao.getMdElementKeys();
			for(int iY=0; iY<idList.size(); iY++){
				if((Integer)idList.get(iY)>0){
				nvpair = new NVPair();
				nvpair.setKey((String) modElementKeysList.get(iY));
				nvpair.setValue((String)modElementDataList.get(iY));
				listNVPair.add(nvpair);
				}
			}
			if(net_pks.equals("")){
				net_pks= pkNetList.get(i).toString();
			}
			//}
			/*else{		
					listNVPair=new UserServiceImpl().getNetworkMoreDetailValue(pkNetList.get(i),keySearch,(EJBUtil.stringToNum(netLevelList.get(i))-1),iaccId);
				}*/
			
			site.setMoreSiteDetailsFields(listNVPair);
			//siteList.add(site);
			
			//chlSql="select pk_nwsites,fk_site,lower(site_name) site_name,er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,nw_memberType,(SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, nw_level+1 as nw_level from er_nwsites,er_site,er_codelst where pk_site=fk_site and pk_codelst=nw_status "+
			//	" and er_site.fk_account=? and FK_NWSITES_MAIN=? ";
			String pks_childNtw="";
			
			//chlSql="select pk_nwsites,site_name from er_nwsites,er_site,er_codelst where pk_site=fk_site and pk_codelst=nw_status "+
			//		" and er_site.fk_account=? and FK_NWSITES_MAIN=? ";
			
			chlSql="select pk_nwsites, fk_nwsites, fk_site, site_name,ctep_id,er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,(SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, nw_level+1 as nw_level "+
					" FROM er_nwsites,er_site,er_codelst WHERE pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? AND pk_nwsites!=? ";
			
		if(!siteName.equals("")){
			chlSql=chlSql+" and lower(site_name) like lower('%"+siteName+"%') ";
		}
		
		if(!CTEP_ID.equals("")){
			chlSql=chlSql+" and lower(ctep_id) like lower('%"+CTEP_ID+"%') ";
		}
		
		if(moredetails!=null && moredetails.size()>0 && (net_pks.length()>0)){
			if(networkSiteDetails!=null && networkSiteDetails.getRelationship_PK()!=null && !networkSiteDetails.getRelationship_PK().equals(""))
			{
				chlSql=chlSql+" and pk_nwsites in ("+net_pks+","+networkSiteDetails.getRelationship_PK()+") ";
			}else{
				chlSql=chlSql+" and pk_nwsites in ("+net_pks+")";
			}
						
		}else{
			if(networkSiteDetails!=null && networkSiteDetails.getRelationship_PK()!=null && !networkSiteDetails.getRelationship_PK().equals(""))
			{
				chlSql=chlSql+" and pk_nwsites in ("+networkSiteDetails.getRelationship_PK()+") ";
			}
		}
		if(!sitePk.equals("")){
			chlSql=chlSql+" and fk_site ="+sitePk;
		}
		
		
		if(!(pkCode.equals("0")) && !(pkCode.equals(""))){
			chlSql=chlSql+" and nw_membertype="+pkCode;
 	 	}
		System.out.println("chlSql change==="+chlSql);
		chlSql=chlSql+" START WITH pk_nwsites=? CONNECT BY prior pk_nwsites=FK_NWSITES ORDER SIBLINGS BY lower(site_name)";
		pstmt = conn.prepareStatement(chlSql);
		pstmt.setInt(1, StringUtil.stringToNum(iaccId));
		pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
		pstmt.setInt(3, StringUtil.stringToNum(pkNetList.get(i)));
		rs = pstmt.executeQuery();
		siteslvl2=new ArrayList<Site>();
		siteslvl3=new ArrayList<Site>();
		siteslvl4=new ArrayList<Site>();
		siteslvl5=new ArrayList<Site>();
		siteslvl6=new ArrayList<Site>();
		siteslvl7=new ArrayList<Site>();
		while(rs.next()){
			
			if((!siteName.equals("") || !CTEP_ID.equals("") || !sitePk.equals("") || networkSiteDetails.getRelationship_PK()!=null || networkSiteDetails.getRelationship_Type()!=null) && (pk_main_network!=null && !pk_main_network.equals("0"))){
				pks_childNtw=pks_childNtw+rs.getInt("pk_nwsites")+",";
			}
			else{
				code=new Code();
				listNVPair=new ArrayList<NVPair>();
				siteIdentifier=new SiteIdentifier();
				map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
				siteIdentifier.setOID(map.getOID());
				siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
				code.setType(rs.getString("stat_type"));
				code.setCode(rs.getString("code"));
				code.setDescription(rs.getString("description"));
				mdJB = new MoreDetailsJB();
				mdDao = new MoreDetailsDao();
				mdDao = mdJB.getMoreDetails(rs.getInt("pk_nwsites"),"org_"+(rs.getInt("nw_level")-1),userBean.getUserGrpDefault());
				idList = mdDao.getId();
				modElementDescList = mdDao.getMdElementDescs();
				modElementDataList = mdDao.getMdElementValues();
				modElementKeysList = mdDao.getMdElementKeys();
				for(int iY=0; iY<idList.size(); iY++){
					if((Integer)idList.get(iY)>0){
					nvpair = new NVPair();
					nvpair.setKey((String) modElementKeysList.get(iY));
					nvpair.setValue((String)modElementDataList.get(iY));
					listNVPair.add(nvpair);
					}
				}
				
				if(rs.getInt("nw_level")==2){
					sitelvl2=new Site();
					siteslvl3=new ArrayList<Site>();
					sitelvl2.setSiteIdentifier(siteIdentifier);
					sitelvl2.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl2.setName(rs.getString("site_name"));
					sitelvl2.setSiteStatus(code);
					sitelvl2.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl2.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl2.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl2.setMoreSiteDetailsFields(listNVPair);
					siteslvl2.add(sitelvl2);
					site.setCSites(siteslvl2);		
				}
				else if(rs.getInt("nw_level")==3){
					sitelvl3=new Site();
					siteslvl4=new ArrayList<Site>();
					sitelvl3.setSiteIdentifier(siteIdentifier);
					sitelvl3.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl3.setName(rs.getString("site_name"));
					sitelvl3.setSiteStatus(code);
					sitelvl3.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl3.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl3.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl3.setMoreSiteDetailsFields(listNVPair);
					siteslvl3.add(sitelvl3);
					sitelvl2.setCSites(siteslvl3);
				}
				else if(rs.getInt("nw_level")==4){
					sitelvl4=new Site();
					siteslvl5=new ArrayList<Site>();
					sitelvl4.setSiteIdentifier(siteIdentifier);
					sitelvl4.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl4.setName(rs.getString("site_name"));
					sitelvl4.setSiteStatus(code);
					sitelvl4.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl4.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl4.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl4.setMoreSiteDetailsFields(listNVPair);
					siteslvl4.add(sitelvl4);
					sitelvl3.setCSites(siteslvl4);
				}
				else if(rs.getInt("nw_level")==5){
						sitelvl5=new Site();
						siteslvl6=new ArrayList<Site>();
						sitelvl5.setSiteIdentifier(siteIdentifier);
						sitelvl5.setLevel(String.valueOf(rs.getInt("nw_level")));
						sitelvl5.setName(rs.getString("site_name"));
						sitelvl5.setSiteStatus(code);
						sitelvl5.setCtepId(rs.getString("ctep_id"));
						if(rs.getString("SiteRelationshipType")!=null){
						code=new Code();
						code.setType(rs.getString("SiteRelationshipType"));
						code.setCode(rs.getString("SiteRelationshipSubType"));
						code.setDescription(rs.getString("SiteRelationshipDesc"));
						sitelvl5.setSiteRelationShipType(code);
						}
						if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
							sitelvl5.setRelationshipPK(rs.getString("pk_nwsites"));
						}
						sitelvl5.setMoreSiteDetailsFields(listNVPair);						
						siteslvl5.add(sitelvl5);
						sitelvl4.setCSites(siteslvl5);
				}
				else if(rs.getInt("nw_level")==6){
					sitelvl6=new Site();
					siteslvl7=new ArrayList<Site>();
					sitelvl6.setSiteIdentifier(siteIdentifier);
					sitelvl6.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl6.setName(rs.getString("site_name"));
					sitelvl6.setSiteStatus(code);
					sitelvl6.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl6.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl6.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl6.setMoreSiteDetailsFields(listNVPair);		
					siteslvl6.add(sitelvl6);
					sitelvl5.setCSites(siteslvl6);
				}
				else if(rs.getInt("nw_level")==7){
					sitelvl7=new Site();
					sitelvl7.setSiteIdentifier(siteIdentifier);
					sitelvl7.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl7.setName(rs.getString("site_name"));
					sitelvl7.setSiteStatus(code);
					sitelvl7.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl7.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl7.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl7.setMoreSiteDetailsFields(listNVPair);
					siteslvl7.add(sitelvl7);
					sitelvl6.setCSites(siteslvl7);
				}
			}
		}
		pks_childNtw=pks_childNtw.length()>0?pks_childNtw.substring(0,pks_childNtw.length()-1):"0";
		
		if(!pks_childNtw.equals("0") && !pks_childNtw.equals(""))
		{
		chlSql="SELECT distinct pk_nwsites,site_name,fk_site,ctep_id,er_codelst.CODELST_DESC AS description,er_codelst.codelst_type AS stat_type,er_codelst.codelst_subtyp AS code,er_codelst.codelst_hide AS stat_hidden, "
				+" (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
				+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, "
				+" nw_level+1 AS nw_level FROM er_nwsites,er_site,er_codelst where fk_site=pk_site AND pk_codelst=nw_status AND er_site.fk_account=? and pk_nwsites <> ? "
				+" START WITH pk_nwsites in ("+pks_childNtw+") CONNECT BY PRIOR fk_nwsites=pk_nwsites ORDER BY nw_level";
		
		pstmt = conn.prepareStatement(chlSql);
		pstmt.setInt(1, StringUtil.stringToNum(iaccId));
		pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
		rs = pstmt.executeQuery();
		siteslvl2=new ArrayList<Site>();
		siteslvl3=new ArrayList<Site>();
		siteslvl4=new ArrayList<Site>();
		siteslvl5=new ArrayList<Site>();
		siteslvl6=new ArrayList<Site>();
		siteslvl7=new ArrayList<Site>();
		
		while (rs.next()) {
			
			code=new Code();
			listNVPair=new ArrayList<NVPair>();
			siteIdentifier=new SiteIdentifier();
			map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
			siteIdentifier.setOID(map.getOID());
			siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
			code.setType(rs.getString("stat_type"));
			code.setCode(rs.getString("code"));
			code.setDescription(rs.getString("description"));
			mdJB = new MoreDetailsJB();
			mdDao = new MoreDetailsDao();
			mdDao = mdJB.getMoreDetails(rs.getInt("pk_nwsites"),"org_"+(rs.getInt("nw_level")-1),userBean.getUserGrpDefault());
			idList = mdDao.getId();
			modElementDescList = mdDao.getMdElementDescs();
			modElementDataList = mdDao.getMdElementValues();
			modElementKeysList = mdDao.getMdElementKeys();
			for(int iY=0; iY<idList.size(); iY++){
				if((Integer)idList.get(iY)>0){
				nvpair = new NVPair();
				nvpair.setKey((String) modElementKeysList.get(iY));
				nvpair.setValue((String)modElementDataList.get(iY));
				listNVPair.add(nvpair);
				}
			}
			
			if(rs.getInt("nw_level")==2){
				sitelvl2=new Site();
				sitelvl2.setSiteIdentifier(siteIdentifier);
				sitelvl2.setLevel(String.valueOf(rs.getInt("nw_level")));
				sitelvl2.setName(rs.getString("site_name"));
				sitelvl2.setSiteStatus(code);
				sitelvl2.setCtepId(rs.getString("ctep_id"));
				if(rs.getString("SiteRelationshipType")!=null){
				code=new Code();
				code.setType(rs.getString("SiteRelationshipType"));
				code.setCode(rs.getString("SiteRelationshipSubType"));
				code.setDescription(rs.getString("SiteRelationshipDesc"));
				sitelvl2.setSiteRelationShipType(code);
				}
				if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
					sitelvl2.setRelationshipPK(rs.getString("pk_nwsites"));
				}
				sitelvl2.setMoreSiteDetailsFields(listNVPair);
				siteslvl2.add(sitelvl2);
				site.setCSites(siteslvl2);
			}
			else if(rs.getInt("nw_level")==3){
				sitelvl3=new Site();
				sitelvl3.setSiteIdentifier(siteIdentifier);
				sitelvl3.setLevel(String.valueOf(rs.getInt("nw_level")));
				sitelvl3.setName(rs.getString("site_name"));
				sitelvl3.setSiteStatus(code);
				sitelvl3.setCtepId(rs.getString("ctep_id"));
				if(rs.getString("SiteRelationshipType")!=null){
				code=new Code();
				code.setType(rs.getString("SiteRelationshipType"));
				code.setCode(rs.getString("SiteRelationshipSubType"));
				code.setDescription(rs.getString("SiteRelationshipDesc"));
				sitelvl3.setSiteRelationShipType(code);
				}
				if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
					sitelvl3.setRelationshipPK(rs.getString("pk_nwsites"));
				}
				sitelvl3.setMoreSiteDetailsFields(listNVPair);
				siteslvl3.add(sitelvl3);
				sitelvl2.setCSites(siteslvl3);
			}
			else if(rs.getInt("nw_level")==4){
				sitelvl4=new Site();
				sitelvl4.setSiteIdentifier(siteIdentifier);
				sitelvl4.setLevel(String.valueOf(rs.getInt("nw_level")));
				sitelvl4.setName(rs.getString("site_name"));
				sitelvl4.setSiteStatus(code);
				sitelvl4.setCtepId(rs.getString("ctep_id"));
				if(rs.getString("SiteRelationshipType")!=null){
				code=new Code();
				code.setType(rs.getString("SiteRelationshipType"));
				code.setCode(rs.getString("SiteRelationshipSubType"));
				code.setDescription(rs.getString("SiteRelationshipDesc"));
				sitelvl4.setSiteRelationShipType(code);
				}
				if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
					sitelvl4.setRelationshipPK(rs.getString("pk_nwsites"));
				}
				sitelvl4.setMoreSiteDetailsFields(listNVPair);
				siteslvl4.add(sitelvl4);
				sitelvl3.setCSites(siteslvl4);
			}
			else if(rs.getInt("nw_level")==5){
				sitelvl5=new Site();
				sitelvl5.setSiteIdentifier(siteIdentifier);
				sitelvl5.setLevel(String.valueOf(rs.getInt("nw_level")));
				sitelvl5.setName(rs.getString("site_name"));
				sitelvl5.setSiteStatus(code);
				sitelvl5.setCtepId(rs.getString("ctep_id"));
				if(rs.getString("SiteRelationshipType")!=null){
				code=new Code();
				code.setType(rs.getString("SiteRelationshipType"));
				code.setCode(rs.getString("SiteRelationshipSubType"));
				code.setDescription(rs.getString("SiteRelationshipDesc"));
				sitelvl5.setSiteRelationShipType(code);
				}
				if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
					sitelvl5.setRelationshipPK(rs.getString("pk_nwsites"));
				}
				sitelvl5.setMoreSiteDetailsFields(listNVPair);
				siteslvl5.add(sitelvl5);
				sitelvl4.setCSites(siteslvl5);
			}
			else if(rs.getInt("nw_level")==6){
				sitelvl6=new Site();
				sitelvl6.setSiteIdentifier(siteIdentifier);
				sitelvl6.setLevel(String.valueOf(rs.getInt("nw_level")));
				sitelvl6.setName(rs.getString("site_name"));
				sitelvl6.setSiteStatus(code);
				sitelvl6.setCtepId(rs.getString("ctep_id"));
				if(rs.getString("SiteRelationshipType")!=null){
				code=new Code();
				code.setType(rs.getString("SiteRelationshipType"));
				code.setCode(rs.getString("SiteRelationshipSubType"));
				code.setDescription(rs.getString("SiteRelationshipDesc"));
				sitelvl6.setSiteRelationShipType(code);
				}
				if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
					sitelvl6.setRelationshipPK(rs.getString("pk_nwsites"));
				}
				sitelvl6.setMoreSiteDetailsFields(listNVPair);
				siteslvl6.add(sitelvl6);
				sitelvl5.setCSites(siteslvl6);
			}
			else if(rs.getInt("nw_level")==7){
				sitelvl7=new Site();
				siteIdentifier=new SiteIdentifier();
				sitelvl7.setSiteIdentifier(siteIdentifier);
				sitelvl7.setLevel(String.valueOf(rs.getInt("nw_level")));
				sitelvl7.setName(rs.getString("site_name"));
				sitelvl7.setSiteStatus(code);
				sitelvl7.setCtepId(rs.getString("ctep_id"));
				if(rs.getString("SiteRelationshipType")!=null){
				code=new Code();
				code.setType(rs.getString("SiteRelationshipType"));
				code.setCode(rs.getString("SiteRelationshipSubType"));
				code.setDescription(rs.getString("SiteRelationshipDesc"));
				sitelvl7.setSiteRelationShipType(code);
				}
				if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
					sitelvl7.setRelationshipPK(rs.getString("pk_nwsites"));
				}
				sitelvl7.setMoreSiteDetailsFields(listNVPair);
				siteslvl7.add(sitelvl7);
				sitelvl6.setCSites(siteslvl7);
			}
			}
		chlSql="SELECT pk_nwsites,fk_nwsites,fk_site,site_name,ctep_id,er_codelst.CODELST_DESC AS description,er_codelst.codelst_type AS stat_type,er_codelst.codelst_subtyp AS code,er_codelst.codelst_hide AS stat_hidden, "
				+" (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
				+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, "
				+" nw_level+1 AS nw_level FROM er_nwsites,er_site,er_codelst where fk_site=pk_site AND pk_codelst=nw_status AND er_site.fk_account=? and pk_nwsites <> ? "
				+" START WITH pk_nwsites in ("+pks_childNtw+") CONNECT BY PRIOR pk_nwsites=fk_nwsites ORDER SIBLINGS BY lower(site_name)";
		pstmt = conn.prepareStatement(chlSql);
		pstmt.setInt(1, StringUtil.stringToNum(iaccId));
		pstmt.setInt(2, StringUtil.stringToNum(pks_childNtw));
		rs = pstmt.executeQuery();
		while (rs.next()) {
				
				code=new Code();
				listNVPair=new ArrayList<NVPair>();
				siteIdentifier=new SiteIdentifier();
				map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
				siteIdentifier.setOID(map.getOID());
				siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
				code.setType(rs.getString("stat_type"));
				code.setCode(rs.getString("code"));
				code.setDescription(rs.getString("description"));
				mdJB = new MoreDetailsJB();
				mdDao = new MoreDetailsDao();
				mdDao = mdJB.getMoreDetails(rs.getInt("pk_nwsites"),"org_"+(rs.getInt("nw_level")-1),userBean.getUserGrpDefault());
				idList = mdDao.getId();
				modElementDescList = mdDao.getMdElementDescs();
				modElementDataList = mdDao.getMdElementValues();
				modElementKeysList = mdDao.getMdElementKeys();
				for(int iY=0; iY<idList.size(); iY++){
					if((Integer)idList.get(iY)>0){
					nvpair = new NVPair();
					nvpair.setKey((String) modElementKeysList.get(iY));
					nvpair.setValue((String)modElementDataList.get(iY));
					listNVPair.add(nvpair);
					}
				}
				
				if(rs.getInt("nw_level")==3){
					sitelvl3=new Site();
					siteslvl4=new ArrayList<Site>();
					sitelvl3.setSiteIdentifier(siteIdentifier);
					sitelvl3.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl3.setName(rs.getString("site_name"));
					sitelvl3.setSiteStatus(code);
					sitelvl3.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl3.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl3.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl3.setMoreSiteDetailsFields(listNVPair);
					siteslvl3.add(sitelvl3);
					sitelvl2.setCSites(siteslvl3);
				}
				else if(rs.getInt("nw_level")==4){
					sitelvl4=new Site();
					siteslvl5=new ArrayList<Site>();
					sitelvl4.setSiteIdentifier(siteIdentifier);
					sitelvl4.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl4.setName(rs.getString("site_name"));
					sitelvl4.setSiteStatus(code);
					sitelvl4.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl4.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl4.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl4.setMoreSiteDetailsFields(listNVPair);
					siteslvl4.add(sitelvl4);
					sitelvl3.setCSites(siteslvl4);
				}
				else if(rs.getInt("nw_level")==5){
					sitelvl5=new Site();
					siteslvl6=new ArrayList<Site>();
					sitelvl5.setSiteIdentifier(siteIdentifier);
					sitelvl5.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl5.setName(rs.getString("site_name"));
					sitelvl5.setSiteStatus(code);
					sitelvl5.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl5.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl5.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl5.setMoreSiteDetailsFields(listNVPair);
					siteslvl5.add(sitelvl5);
					sitelvl4.setCSites(siteslvl5);
				}
				else if(rs.getInt("nw_level")==6){
					sitelvl6=new Site();
					siteslvl7=new ArrayList<Site>();
					sitelvl6.setSiteIdentifier(siteIdentifier);
					sitelvl6.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl6.setName(rs.getString("site_name"));
					sitelvl6.setSiteStatus(code);
					sitelvl6.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl6.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl6.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl6.setMoreSiteDetailsFields(listNVPair);
					siteslvl6.add(sitelvl6);
					sitelvl5.setCSites(siteslvl6);
				}
				else if(rs.getInt("nw_level")==7){
					sitelvl7=new Site();
					siteIdentifier=new SiteIdentifier();
					sitelvl7.setSiteIdentifier(siteIdentifier);
					sitelvl7.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl7.setName(rs.getString("site_name"));
					sitelvl7.setSiteStatus(code);
					sitelvl7.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl7.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl7.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl7.setMoreSiteDetailsFields(listNVPair);
					siteslvl7.add(sitelvl7);
					sitelvl6.setCSites(siteslvl7);
				}
				}
		
		}siteList.add(site);
		sitesList.add((ArrayList<Site>) siteList);
		}

	}catch(OperationException ex){
		throw ex;
	}catch(SQLException ex){
		ex.printStackTrace();
	}catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}



	
public void searchNetworks(NetworksDetails networksDetails,HashMap<String, Object> params, String from) throws OperationException {
		
	
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	int count=0;
	String str = "";
	String pk_main_network = "";
	String networkName = (networksDetails.getNetworkName()!=null)?networksDetails.getNetworkName():"";
	String sitePK = (networksDetails.getSitePK()!=null)?networksDetails.getSitePK():"";
	String siteName = (networksDetails.getSiteName()!=null)?networksDetails.getSiteName():"";
	String networkPK = (networksDetails.getNetworkPK()!=null)?networksDetails.getNetworkPK():"";
	String iaccId = params.get(KEY_ACCOUNT_ID).toString();
	
	
	if(networkPK.equals("") &&
			(!siteName.equals("") || !sitePK.equals(""))){
		String networkPKBySite = "select nvl(fk_nwsites_main,pk_nwsites) as fk_nwsites_main from er_nwsites,er_site where pk_site=fk_site";
		if(!siteName.equals("")){
			networkPKBySite=networkPKBySite+ " and lower(site_name) = lower('"+siteName+"')";
		}
		if(!sitePK.equals("")){
			networkPKBySite=networkPKBySite+ " and fk_site = "+StringUtil.stringToNum(sitePK);
		}
		
		networkPKBySite=networkPKBySite+" order by pk_nwsites";
		System.out.println("networkPKBySite==="+networkPKBySite);
		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(networkPKBySite);
			rs=pstmt.executeQuery();
			while(rs.next()){
				if(pk_main_network.equals("")){
					pk_main_network=String.valueOf(rs.getInt("fk_nwsites_main"));
				}else{
					pk_main_network=pk_main_network+","+String.valueOf(rs.getInt("fk_nwsites_main"));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		if(pk_main_network.equals("")){
			pk_main_network="0";
		}
	}else if(!networkPK.equals("")){
		pk_main_network=networkPK;
	}
try {
if (!networkName.equals("") || !pk_main_network.equals("")){
	System.out.println("final pk_main_network==="+pk_main_network);
	
	 	String str1 = "select pk_nwsites,fk_site,site_name ,site_info,site_info as network_info,nw_status,site_name as network_name,ctep_id, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,er_nwsites.NW_MEMBERTYPE as member_type,(SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,nw_level from er_nwsites,er_site,er_codelst "+
	 				  " where pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? and nw_level=0 ";
	 	if(!(networkName.equals(""))){
	 			 	 str1=str1+" and lower(site_name) like lower('%"+networkName+"%') ";
	 		}
	 	//if(networkName.equals("") && pk_main_network.length()>0)
	 	if(pk_main_network.length()>0)
	 	{
	 		str1=str1+" and pk_nwsites in("+pk_main_network+") ";
	 	}
	 	System.out.println("str1==="+str1);

	conn = CommonDAO.getConnection();
	pstmt = conn.prepareStatement(str1);
	pstmt.setInt(1, StringUtil.stringToNum(iaccId));
	rs = pstmt.executeQuery();
	Code code=null;
	while (rs.next()) {
		code=new Code();
		pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
		fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
		if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
			pkRelnShipType.add(rs.getString("pk_nwsites"));
		}
		else{
			pkRelnShipType.add("0");
		}
		netNameList.add(rs.getString("network_name"));
		netInfoList.add(rs.getString("network_info"));
		code.setPk(String.valueOf(rs.getInt("nw_status")));
		code.setType(rs.getString("stat_type"));
		code.setCode(rs.getString("code"));
		code.setDescription(rs.getString("description"));
		netStatList.add(code);
		if(rs.getString("SiteRelationshipType")!=null){
		code=new Code();
		code.setPk(String.valueOf(rs.getInt("member_type")));
		code.setType(rs.getString("SiteRelationshipType"));
		code.setCode(rs.getString("SiteRelationshipSubType"));
		code.setDescription(rs.getString("SiteRelationshipDesc"));
		siteRelnShipType.add(code);
		}
		else{
			siteRelnShipType.add(null);
		}
		siteNameList.add(rs.getString("site_name"));
		siteInfoList.add(rs.getString("site_info"));
		netLevelList.add(String.valueOf(rs.getInt("nw_level")));
		if(rs.getString("ctep_id")!=null){
			ctepList.add(rs.getString("ctep_id"));
		}else{
			ctepList.add("");
		}
		count++;
	}
	if(count==0)
	{
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found")); 
		throw new OperationException();
	}
	String chlSql="";
	Site site=null;
	SiteIdentifier siteIdentifier=null;
	ObjectMap map=null;
	
	UserBean userBean=(UserBean)params.get("callingUser");
	List<Site> siteslvl1=null;
	List<Site> siteslvl2=null;
	List<Site> siteslvl3=null;
	List<Site> siteslvl4=null;
	List<Site> siteslvl5=null;
	List<Site> siteslvl6=null;
	Sites psiteslvl0=null;
	Sites psiteslvl1=null;
	Sites psiteslvl2=null;
	Sites psiteslvl3=null;
	Sites psiteslvl4=null;
	Sites psiteslvl5=null;
	MoreDetailsJB mdJB=null;
	MoreDetailsDao mdDao;
	ArrayList idList = new ArrayList();
	ArrayList modElementDescList = new ArrayList();
	ArrayList modElementDataList = new ArrayList();
	ArrayList modElementKeysList = new ArrayList();
	List<NVPair> listNVPair=null;
	NVPair nvpair = null;
	List<Site> siteList=null;
	String net_pks = "";
	for(int i=0;i<pkNetList.size();i++){
		
		//siteList.add(site);
		
		//chlSql="select pk_nwsites,fk_site,lower(site_name) site_name,er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,nw_memberType,(SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, nw_level+1 as nw_level from er_nwsites,er_site,er_codelst where pk_site=fk_site and pk_codelst=nw_status "+
		//	" and er_site.fk_account=? and FK_NWSITES_MAIN=? ";
		String pks_childNtw="";
		
		//chlSql="select pk_nwsites,site_name from er_nwsites,er_site,er_codelst where pk_site=fk_site and pk_codelst=nw_status "+
		//		" and er_site.fk_account=? and FK_NWSITES_MAIN=? ";
		
		
		List<String> relationshipPKList = new ArrayList<String>();
		if(!siteName.equals("") || !sitePK.equals("")){
		String getRelationshipPk = "select pk_nwsites from er_nwsites,er_site where (fk_nwsites_main="+StringUtil.stringToNum(pkNetList.get(i))+" or pk_nwsites="+StringUtil.stringToNum(pkNetList.get(i))+") and pk_site=fk_site";
		if(!siteName.equals("")){
			getRelationshipPk=getRelationshipPk+ " and lower(site_name) = lower('"+siteName+"')";
		}
		if(!sitePK.equals("")){
			getRelationshipPk=getRelationshipPk+ " and fk_site = "+StringUtil.stringToNum(sitePK);
		}
		
		getRelationshipPk=getRelationshipPk+" order by pk_nwsites";
		pstmt = conn.prepareStatement(getRelationshipPk);
		rs=pstmt.executeQuery();
		while(rs.next()){
			if(rs.getString("pk_nwsites")!=null){
				relationshipPKList.add(String.valueOf(rs.getInt("pk_nwsites")));
			}
			
		}
			System.out.println("getRelationshipPk==="+getRelationshipPk);
			
		
	}
	
	if(relationshipPKList.size()>0){
		
		siteList=new ArrayList<Site>();
		
		for(String relationPK: relationshipPKList){
			siteslvl1=new ArrayList<Site>();
			siteslvl2=new ArrayList<Site>();
			siteslvl3=new ArrayList<Site>();
			siteslvl4=new ArrayList<Site>();
			siteslvl5=new ArrayList<Site>();
			siteslvl6=new ArrayList<Site>();
			psiteslvl0=new Sites();
			psiteslvl1=new Sites();
			psiteslvl2=new Sites();
			psiteslvl3=new Sites();
			psiteslvl4=new Sites();
			psiteslvl5=new Sites();
			Site sitelvl1=null;
			Site sitelvl2=null;
			Site sitelvl3=null;
			Site sitelvl4=null;
			Site sitelvl5=null;
			Site sitelvl6=null;
			
			listNVPair=new ArrayList<NVPair>();
			site=new Site();
			siteIdentifier=new SiteIdentifier();
			map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
			siteIdentifier.setOID(map.getOID());
			siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
			site.setSiteIdentifier(siteIdentifier);
			site.setLevel(String.valueOf(netLevelList.get(i)));
			site.setName(siteNameList.get(i));
			site.setDescription(siteInfoList.get(i));
			site.setSiteStatus(netStatList.get(i));
			if(!pkRelnShipType.get(i).equals("0") && siteRelnShipType.get(i)!=null){
			site.setRelationshipPK(pkRelnShipType.get(i));
			site.setSiteRelationShipType(siteRelnShipType.get(i));
			
			}
			if(!ctepList.get(i).equals("")){
				site.setCtepId(ctepList.get(i));
			}
			
		
				pks_childNtw = relationPK;
			
			System.out.println("pks_childNtw==="+pks_childNtw);
			if(!pks_childNtw.equals("0") && !pks_childNtw.equals(""))
			{
			chlSql="SELECT distinct pk_nwsites,site_name,site_info,fk_site,ctep_id,er_codelst.CODELST_DESC AS description,er_codelst.codelst_type AS stat_type,er_codelst.codelst_subtyp AS code,er_codelst.codelst_hide AS stat_hidden, "
					+" nw_status, er_nwsites.NW_MEMBERTYPE as member_type, (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
					+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, "
					+" nw_level FROM er_nwsites,er_site,er_codelst where fk_site=pk_site AND pk_codelst=nw_status AND er_site.fk_account=? and pk_nwsites <> ? "
					+" and pk_nwsites in ("+ pks_childNtw +") ORDER BY nw_level";
			System.out.println("chlSql==="+chlSql);
			System.out.println("iaccId==="+iaccId);
			System.out.println("pkNetList.get(i)==="+pkNetList.get(i));
			pstmt = conn.prepareStatement(chlSql);
			pstmt.setInt(1, StringUtil.stringToNum(iaccId));
			pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
			rs = pstmt.executeQuery();
			
			
			while (rs.next()) {
				
				code=new Code();
				listNVPair=new ArrayList<NVPair>();
				siteIdentifier=new SiteIdentifier();
				map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
				siteIdentifier.setOID(map.getOID());
				siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
				code.setPk(String.valueOf(rs.getInt("nw_status")));
				code.setType(rs.getString("stat_type"));
				code.setCode(rs.getString("code"));
				code.setDescription(rs.getString("description"));
				mdJB = new MoreDetailsJB();
				mdDao = new MoreDetailsDao();
				mdDao = mdJB.getMoreDetails(rs.getInt("pk_nwsites"),"org_"+(rs.getInt("nw_level")),userBean.getUserGrpDefault());//fix for 28951 bug 
				idList = mdDao.getId();
				modElementDescList = mdDao.getMdElementDescs();
				modElementDataList = mdDao.getMdElementValues();
				modElementKeysList = mdDao.getMdElementKeys();
				for(int iY=0; iY<idList.size(); iY++){
					if((Integer)idList.get(iY)>0){
					nvpair = new NVPair();
					nvpair.setKey((String) modElementKeysList.get(iY));
					nvpair.setValue((String)modElementDataList.get(iY));
					listNVPair.add(nvpair);
					}
				}
				
				if(rs.getInt("nw_level")==1){
					sitelvl1=new Site();
					sitelvl1.setSiteIdentifier(siteIdentifier);
					sitelvl1.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl1.setName(rs.getString("site_name"));
					sitelvl1.setDescription(rs.getString("site_info"));
					sitelvl1.setSiteStatus(code);
					sitelvl1.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl1.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl1.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl1.setMoreSiteDetailsFields(listNVPair);//Kavitha: #28951				
					siteslvl1.add(sitelvl1);
					//siteslvl2=new ArrayList<Site>();
					psiteslvl0.setSites(siteslvl1);
					site.setSites(psiteslvl0);
				}
				else if(rs.getInt("nw_level")==2){
					sitelvl2=new Site();
					sitelvl2.setSiteIdentifier(siteIdentifier);
					sitelvl2.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl2.setName(rs.getString("site_name"));
					sitelvl2.setDescription(rs.getString("site_info"));
					System.out.println("level 2 description==="+sitelvl2.getDescription());
					sitelvl2.setSiteStatus(code);
					sitelvl2.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl2.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl2.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl2.setMoreSiteDetailsFields(listNVPair);//Kavitha: #28951
					siteslvl2.add(sitelvl2);
					//siteslvl3=new ArrayList<Site>();
					if(sitelvl1!=null){
						System.out.println("Inside If");
						psiteslvl1.setSites(siteslvl2);
						sitelvl1.setSites(psiteslvl1);
					}else{
						System.out.println("Inside Else");
						psiteslvl0.setSites(siteslvl2);
						site.setSites(psiteslvl0);
					}
					
					
				}
				else if(rs.getInt("nw_level")==3){
					sitelvl3=new Site();
					sitelvl3.setSiteIdentifier(siteIdentifier);
					sitelvl3.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl3.setName(rs.getString("site_name"));
					sitelvl3.setDescription(rs.getString("site_info"));
					System.out.println("level 3 description==="+sitelvl3.getDescription());
					sitelvl3.setSiteStatus(code);
					sitelvl3.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl3.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl3.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl3.setMoreSiteDetailsFields(listNVPair);//Kavitha: #28951			
					siteslvl3.add(sitelvl3);
					//siteslvl4=new ArrayList<Site>();
					if(sitelvl2!=null){
						psiteslvl2.setSites(siteslvl3);
						sitelvl2.setSites(psiteslvl2);
					}else{
						psiteslvl0.setSites(siteslvl3);
						site.setSites(psiteslvl0);
					}
				}
				else if(rs.getInt("nw_level")==4){
					sitelvl4=new Site();
					sitelvl4.setSiteIdentifier(siteIdentifier);
					sitelvl4.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl4.setName(rs.getString("site_name"));
					sitelvl4.setDescription(rs.getString("site_info"));
					sitelvl4.setSiteStatus(code);
					sitelvl4.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl4.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl4.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl4.setMoreSiteDetailsFields(listNVPair);//Kavitha: #28951
					siteslvl4.add(sitelvl4);
					//siteslvl5=new ArrayList<Site>();
					if(sitelvl3!=null){
						psiteslvl3.setSites(siteslvl4);
						sitelvl3.setSites(psiteslvl3);
					}else{
						psiteslvl0.setSites(siteslvl4);
						site.setSites(psiteslvl0);
					}
					
				}
				else if(rs.getInt("nw_level")==5){
					sitelvl5=new Site();
					sitelvl5.setSiteIdentifier(siteIdentifier);
					sitelvl5.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl5.setName(rs.getString("site_name"));
					sitelvl5.setDescription(rs.getString("site_info"));
					sitelvl5.setSiteStatus(code);
					sitelvl5.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl5.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl5.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl5.setMoreSiteDetailsFields(listNVPair);//Kavitha: #28951
					siteslvl5.add(sitelvl5);
					//siteslvl6=new ArrayList<Site>();
					if(sitelvl4!=null){
						psiteslvl4.setSites(siteslvl5);
						sitelvl4.setSites(psiteslvl4);
					}else{
						psiteslvl0.setSites(siteslvl5);
						site.setSites(psiteslvl0);
					}
				}
				else if(rs.getInt("nw_level")==6){
					sitelvl6=new Site();
					siteIdentifier=new SiteIdentifier();
					sitelvl6.setSiteIdentifier(siteIdentifier);
					sitelvl6.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl6.setName(rs.getString("site_name"));
					sitelvl6.setDescription(rs.getString("site_info"));
					sitelvl6.setSiteStatus(code);
					sitelvl6.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl6.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl6.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl6.setMoreSiteDetailsFields(listNVPair);//Kavitha: #28951
					siteslvl6.add(sitelvl6);
					if(sitelvl5!=null){
						psiteslvl5.setSites(siteslvl6);
						sitelvl5.setSites(psiteslvl5);
					}else{
						psiteslvl0.setSites(siteslvl6);
						site.setSites(psiteslvl0);
					}
				}
				}
			}
			siteList.add(site);
		}
		sitesList.add((ArrayList<Site>) siteList);
		}else{
			siteList=new ArrayList<Site>();
			site=new Site();
			siteIdentifier=new SiteIdentifier();
			map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
			siteIdentifier.setOID(map.getOID());
			siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
			site.setSiteIdentifier(siteIdentifier);
			site.setLevel(String.valueOf(netLevelList.get(i)));
			site.setName(siteNameList.get(i));
			site.setDescription(siteInfoList.get(i));
			site.setSiteStatus(netStatList.get(i));
			if(!pkRelnShipType.get(i).equals("0") && siteRelnShipType.get(i)!=null){
			site.setRelationshipPK(pkRelnShipType.get(i));
			site.setSiteRelationShipType(siteRelnShipType.get(i));
			
			}
			if(!ctepList.get(i).equals("")){
				site.setCtepId(ctepList.get(i));
			}
			siteList.add(site);
			sitesList.add((ArrayList<Site>) siteList);
		}
	
	}
}else{
	this.response=new ResponseHolder();
	this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
	this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found")); 
	throw new OperationException();
}
}catch(OperationException ex){
	throw ex;
}catch(SQLException ex){
	ex.printStackTrace();
}catch (Exception e) {
	e.printStackTrace();
} finally {
	try {
		rs.close();
		pstmt.close();
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}

	}
 public void searchNetworkUsers(NetworkUserDetail networkuser, HashMap<String, Object> params) throws OperationException {
		Connection conn=null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ResultSet rs1=null;
		int userLoginPK = 0;
		int userPK=0;
		int userOIDPK=0;
		String pkCode="";
		int count=0;
		String net_pks="";
		ObjectMap map=null;
		UserBean userBeanPk = null;
		String networkName="";
		String networkPk="";
		String relationShipPK="";
		String sitePK="";
		String siteName="";
		UserAgentRObj userAgent = (UserAgentRObj) params.get("userAgent");
		UserBean userBean=(UserBean)params.get("callingUser");
		String iaccId = (String)params.get(KEY_ACCOUNT_ID);
		//System.out.println("networkuser.getNetworkDetails().getNetworkName()=="+networkuser.getNetworkDetails().getNetworkName());
	 if(networkuser.getNetworkDetails()!=null){
       if(networkuser.getNetworkDetails().getNetworkName()!=null){
		 networkName = networkuser.getNetworkDetails().getNetworkName()==null?"":networkuser.getNetworkDetails().getNetworkName();     
       }
       if(networkuser.getNetworkDetails().getNetworkPK()!=null){
  		 networkPk = networkuser.getNetworkDetails().getNetworkPK()==null?"":networkuser.getNetworkDetails().getNetworkPK();     
        }
       if(networkuser.getNetworkDetails().getRelationshipPK()!=null){
    	   relationShipPK = networkuser.getNetworkDetails().getRelationshipPK()==null?"":networkuser.getNetworkDetails().getRelationshipPK();     
          }
       if(networkName.equals("") && networkPk.equals("") && relationShipPK.equals("")){
			this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid Network identifier is Required.")); 
			throw new OperationException();
		}
       if((networkuser.getNetworkDetails().getSiteName()!=null)|| (networkuser.getNetworkDetails().getRelationshipType() != null && networkuser.getNetworkDetails().getRelationshipType().getCode() != null)){
    
    	   if(networkName.equals("") && networkPk.equals("") && relationShipPK.equals("")){
    		    this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network identifier is mandatory.")); 
				throw new OperationException();
			}      
       }
       
       if(networkuser.getNetworkDetails().getSiteName()!=null){
    	   siteName = networkuser.getNetworkDetails().getSiteName()==null?"":networkuser.getNetworkDetails().getSiteName();
  		if(siteName.equals("")){
  			    this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid Site Name is Required.")); 
				throw new OperationException();
  			}     
         }
       if(networkuser.getNetworkDetails().getSitePK()!=null){
    	   sitePK = networkuser.getNetworkDetails().getSitePK()==null?"":networkuser.getNetworkDetails().getSitePK();
  		if(sitePK.equals("")){
  			    this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid Site PK is Required.")); 
				throw new OperationException();
  			}     
         }
       if(networkuser.getNetworkDetails().getRelationshipType() != null && networkuser.getNetworkDetails().getRelationshipType().getCode() != null)
		{
			try
			{
				CodeDao cd = new CodeDao();
		    	
		   pkCode = StringUtil.integerToString(cd.getCodeId(networkuser.getNetworkDetails().getRelationshipType().getType(), networkuser.getNetworkDetails().getRelationshipType().getCode()));
				if(pkCode.equals("0") ){
					    this.response=new ResponseHolder();
						this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
						this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Site Type Code Not Found:")); 
						throw new OperationException();
				}
			}
			catch(CodeNotFoundException e)
			{
				System.err.print(e.getMessage());
			}
			
		}
		
		}
     System.out.println("Here");
     if(networkuser.getPK()!=null){
    	int usr=networkuser.getPK();
    	userBeanPk = userAgent.getUserDetails(usr);
		if(userBeanPk==null){
			     this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid User Object is required with User PK.")); 
				throw new OperationException();	
		}else{
			userPK = userBeanPk.getUserId();
		}
	}
				
		if(networkuser.getUserLoginName()!=null){
			UserDao userDao = new UserDao();
			userDao.getUserValuesByLoginName(networkuser.getUserLoginName());
			if(userDao.getUsrIds().size()==0){
				this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid User Object is required with loginName.")); 
				throw new OperationException();	
			}else
			{
				userLoginPK = (Integer)userDao.getUsrIds().get(0);
			}
		}
	
			
	if (networkuser.getOID() != null){
				try{
		    ObjectMap objectMap =((ObjectMapService)params.get("objectMapService")).getObjectMapFromId(networkuser.getOID());
		    if(objectMap == null){
				this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid User Object is required with OID.")); 
				throw new OperationException();
		    }
			if(objectMap != null){
				    UserBean userBeanFromOID = userAgent.getUserDetails(objectMap.getTablePK());
					if (userBeanFromOID != null && userBeanFromOID.getUserAccountId().equals(iaccId)) {
						userOIDPK=userBeanFromOID.getUserId();
					}
				}}catch(MultipleObjectsFoundException e)
				{
					System.err.print(e.getMessage());
				}
				
		}	
	if( userPK>0 && userLoginPK>0 ){
		if( userPK!=userLoginPK ){
			this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User PK is Not Matched with User Login Name.")); 
			throw new OperationException();	
		}}
	else if(userPK>0 && userOIDPK>0){
		
		if( userPK!=userOIDPK ){
			this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User PK is Not Matched with User OID.")); 
			throw new OperationException();	
		}}
	else if(userOIDPK>0 && userLoginPK>0 ){
		if( userOIDPK!=userLoginPK ){
			this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User Name is  Not Matched with User OID.")); 
			throw new OperationException();	
		}
	}
	
	List<NVPair> moredetails=networkuser.getmoreUserDetails();
	Collection<NVPair> moreUserDetails =networkuser.getmoreUserDetails();
	HashMap<String,Object> keySearch=new HashMap<String,Object>();
	
	if(moredetails!=null && moredetails.size()>0){
		 net_pks=getNetPkMoreDetails(networkuser.getmoreUserDetails(),"networkUserDetail");
		networkuser.setmoreUserDetails((java.util.List<NVPair>) moreUserDetails);
		if(moreUserDetails!=null){
			NVPair getDetail=new NVPair();
			for(int iY=0;iY<networkuser.getmoreUserDetails().size();iY++){
				getDetail = networkuser.getmoreUserDetails().set(iY, getDetail);
				keySearch.put("key",getDetail.getKey()==null?"":getDetail.getKey());
				keySearch.put("value", getDetail.getValue()==null?"":getDetail.getValue());
				keySearch.put("user","user");
			}
			moreUserDetails.addAll(moreUserDetails);	
		}
	}
	StringBuffer sqlbuf = new StringBuffer();
	String fk_user="";
	String FK_NWSITES_MAIN="";
	
	if(userOIDPK>0 || userLoginPK>0 || userPK>0 || (!siteName.equals("")) || (!networkPk.equals("")) || (!relationShipPK.equals("")) || (!sitePK.equals("")) || (networkuser.getNetworkDetails() !=null && networkuser.getNetworkDetails().getRelationshipType() != null && networkuser.getNetworkDetails().getRelationshipType().getCode() != null) || (moredetails!=null && moredetails.size()>0)){
	
	 sqlbuf.append(" SELECT distinct(nvl(ns.FK_NWSITES_MAIN,pk_nwsites)) as fk_nwsites_main FROM er_nwsites ns,er_nwusers nu,er_site s WHERE  nu.FK_NWSITES=ns.PK_NWSITES and ns.fk_site=s.pk_site and s.fk_account=? "); 			
 	 	
	if(!(siteName.equals(""))) {
		sqlbuf.append(" and lower(site_name) like lower('%"+siteName+"%')");
		 }
	if(!sitePK.equals("")){
		sqlbuf.append(" and er_nwsites.fk_site="+sitePK+"");
	}
	if(!(pkCode.equals(""))){
		sqlbuf.append(" and nw_membertype="+pkCode+"");
 	 	}
	if(!net_pks.equals("") && !net_pks.equals("0")){
		sqlbuf.append(" and nu.PK_NWUSERS in ("+net_pks+")");
	}
     if(userOIDPK>0 && userLoginPK>0 && userPK>0){
	   sqlbuf.append(" and fk_user="+userPK+"");
	 	}
	 else{
	 	 	if(userOIDPK>0){
	 	 		sqlbuf.append(" and fk_user="+userOIDPK+"");
	 	 	}
	 	 	if(userLoginPK>0){
	 	 		sqlbuf.append(" and fk_user="+userLoginPK+"");
	 	 	}
	 	 	if(userPK>0){
	 	 		sqlbuf.append(" and fk_user="+userPK+"");
	 	 	}
	  }
     if(networkPk.equals("") && relationShipPK.equals("")){
    FK_NWSITES_MAIN = pkMainNetwork(sqlbuf.toString(),iaccId);
     }else{
    	 if(!networkPk.equals("")){
    	 FK_NWSITES_MAIN= networkPk;
    	 }else{
    		 String getPrimaryNetwork="select fk_nwsites_main FROM er_nwsites WHERE pk_nwsites="+ EJBUtil.stringToNum(relationShipPK);
 			try {
 				pstmt = conn.prepareStatement(getPrimaryNetwork);
 				rs=pstmt.executeQuery();
 				while(rs.next()){
 					FK_NWSITES_MAIN=String.valueOf(rs.getInt("fk_nwsites_main"));
 				}
 				System.out.println("new condition pk_main_network==="+FK_NWSITES_MAIN);
 			}catch(Exception e){
 				e.printStackTrace();
 			}
    		 
    	 }
     }
	  }
	System.out.println("FK_NWSITES_MAIN==="+FK_NWSITES_MAIN);
		 System.out.println("sqlbuf==="+sqlbuf.toString());
	StringBuffer sqlbuf1 = new StringBuffer();
		
	 sqlbuf1.append( "SELECT pk_nwsites,fk_user,pk_nwusers,  fk_site,  nw_membertype,site_name,site_name as network_name,er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,");
     sqlbuf1.append(" er_nwsites.NW_LEVEL+1 AS network_level FROM er_site,er_nwsites,er_nwusers,er_codelst WHERE pk_site=fk_site and pk_codelst=nw_status AND er_site.fk_account=? AND er_nwusers.fk_nwsites=er_nwsites.pk_nwsites");

 	 
 	if(!(networkName.equals(""))){
	 		sqlbuf1.append(" and lower(site_name) like lower('%"+networkName+"%') ");
 	}
 	if(!FK_NWSITES_MAIN.equalsIgnoreCase("") && !FK_NWSITES_MAIN.equals("0")){
 	 	sqlbuf1.append(" and pk_nwsites in ("+FK_NWSITES_MAIN+")");
 	}
 	 System.out.println("sqlbuf1==="+sqlbuf1.toString());
 	 System.out.println("network name=="+networkName);
	List<String> tmpPkNetList=new ArrayList<String>();
	List<String> tmpFkUserList=new ArrayList<String>();
	List<String> tmpFkSiteList=new ArrayList<String>();
	List<String> tmpPkNetUserList=new ArrayList<String>();
	try {
		pstmt = conn.prepareStatement(sqlbuf1.toString());
		pstmt.setInt(1, StringUtil.stringToNum(iaccId));
		rs = pstmt.executeQuery();
		
		while (rs.next()) {
			if(!tmpPkNetList.contains(String.valueOf(rs.getInt("pk_nwsites")))){
				tmpPkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
				tmpFkSiteList.add(String.valueOf(rs.getInt("fk_site")));
				tmpFkUserList.add(String.valueOf(rs.getInt("fk_user")));
				tmpPkNetUserList.add(String.valueOf(rs.getInt("pk_nwusers")));
				count++;
			}
		}
		if(count==0)
		{
			this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Data is not found")); 
			throw new OperationException();
		}

	
			//pkNwUserList.add(String.valueOf(rs.getInt("pk_nwusers")));
			NetworkSites site=null;
			SiteIdentifier siteIdentifier=null;	
			NetworkSites sitelvl2=null;
			NetworkSites sitelvl3=null;
			NetworkSites sitelvl4=null;
			NetworkSites sitelvl5=null;
			NetworkSites sitelvl6=null;
			NetworkSites sitelvl7=null;
			
			List<NetworkSites> siteList=null;
			List<NetworkSites> siteslvl2=null;
			List<NetworkSites> siteslvl3=null;
			List<NetworkSites> siteslvl4=null;
			List<NetworkSites> siteslvl5=null;
			List<NetworkSites> siteslvl6=null;
			List<NetworkSites> siteslvl7=null;
			for(int i=0;i<tmpPkNetList.size();i++){
				
				/*site=new NetworkSites();
				siteList=new ArrayList<NetworkSites>();
				siteIdentifier=new SiteIdentifier();
				map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
				siteIdentifier.setOID(map.getOID());
				siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
				site.setSiteIdentifier(siteIdentifier);
				site.setName(siteNameList.get(i));
				site.setLevel(String.valueOf(netLevelList.get(i)));
				site.setSiteStatus(netStatList.get(i));
	            fk_user=netUserList.get(i);
				HashMap<String,Object> param=new HashMap<String,Object>();
				param.put("networkuser", networkuser);
				param.put("site", site);
				param.put("keySearch", keySearch);
				param.put("iaccId", iaccId);
				param.put("parent", "parent");
				param.put("fk_user", fk_user);
				
				// calling function for Main Network UserMoreDetails Fields.
				networkUserMoreDetails(EJBUtil.stringToNum(pkNwUserList.get(i)),"user_"+(EJBUtil.stringToNum(netLevelList.get(i))-1),userBean.getUserGrpDefault(),param);
				siteList.add(site);
				//Calling function for Main Network User Roles.
				networkRoles(pkNetList.get(i),fk_user,iaccId,site);*/
				
				
				StringBuffer sqlbuf2 = new StringBuffer();
				sqlbuf2.append("SELECT distinct pk_nwsites,  fk_site,  site_name, ctep_id,   site_name AS network_name, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,er_nwsites.NW_LEVEL+1 AS network_level FROM er_nwsites,er_site,er_nwusers,er_codelst WHERE pk_site=fk_site and pk_codelst=nw_status AND er_site.fk_account=? "); 
				if(!(siteName.equals(""))) {
					sqlbuf2.append(" and lower(site_name) like lower('%"+siteName+"%')");
					 }
				if(!sitePK.equals("")){
					sqlbuf2.append(" and er_nwsites.fk_site="+sitePK+"");
				}
				if(!(pkCode.equals(""))){
					sqlbuf2.append(" and nw_membertype="+pkCode+"");
			 	 	}
				if(!net_pks.equals("") && !net_pks.equals("0")){
					sqlbuf2.append(" and PK_NWUSERS in ("+net_pks+")");
				}
				if(!(relationShipPK.equals("")))
				{
					sqlbuf2.append(" and pk_nwsites in ("+relationShipPK+") ");
				}
			     if(userOIDPK>0 && userLoginPK>0 && userPK>0){
				   sqlbuf2.append(" and fk_user="+userPK+"");
				 	}
				 else{
				 	 	if(userOIDPK>0){
				 	 		sqlbuf2.append(" AND pk_nwsites in (select fk_nwsites from er_nwusers where fk_user="+userOIDPK+")");
				 	 	}
				 	 	if(userLoginPK>0){
				 	 		sqlbuf2.append(" AND pk_nwsites in (select fk_nwsites from er_nwusers where fk_user="+userLoginPK+")");
				 	 	}
				 	 	if(userPK>0){
				 	 		sqlbuf2.append(" AND pk_nwsites in (select fk_nwsites from er_nwusers where fk_user="+userPK+")");
				 	 	}
				  }
				sqlbuf2.append(" START WITH er_nwsites.pk_nwsites in (?) CONNECT BY PRIOR er_nwsites.pk_nwsites=er_nwsites.fk_nwsites ORDER SIBLINGS BY lower(site_name)");
				
				System.out.println("sqlbuf2==="+sqlbuf2.toString());
				pstmt = conn.prepareStatement(sqlbuf2.toString());
					pstmt.setInt(1, StringUtil.stringToNum(iaccId));
					pstmt.setInt(2, StringUtil.stringToNum(tmpPkNetList.get(i)));
					rs = pstmt.executeQuery();
					String pkNetList1="";
					siteList=new ArrayList<NetworkSites>();
					siteslvl2=new ArrayList<NetworkSites>();
					siteslvl3=new ArrayList<NetworkSites>();
					siteslvl4=new ArrayList<NetworkSites>();
					siteslvl5=new ArrayList<NetworkSites>();
					siteslvl6=new ArrayList<NetworkSites>();
					siteslvl7=new ArrayList<NetworkSites>();
					if(userOIDPK>0){
			 	 		fk_user=String.valueOf(userOIDPK);
			 	 	}
					else if(userLoginPK>0){
			 	 		fk_user=String.valueOf(userLoginPK);
			 	 	}
					else if(userPK>0){
			 	 		fk_user=String.valueOf(userPK);
			 	 	}else{
			 	 		fk_user="";
			 	 	}
					String fk_site=tmpFkSiteList.get(i);
					String pk_nwusers= "";
					Code code=null;
					String pks_childNtw= "";
					while (rs.next()) {
						if(userOIDPK>0 || userLoginPK>0 || userPK>0 || (!siteName.equals("")) || (!networkPk.equals("")) || (!relationShipPK.equals("")) || (!sitePK.equals("")) || (networkuser.getNetworkDetails() !=null && networkuser.getNetworkDetails().getRelationshipType() != null && networkuser.getNetworkDetails().getRelationshipType().getCode() != null) || (moredetails!=null && moredetails.size()>0)){
							pks_childNtw=pks_childNtw+rs.getInt("pk_nwsites")+",";
						}else{
						System.out.println("Site Name==="+rs.getString("site_name"));
						code=new Code();
						pkNetList1=(String.valueOf(rs.getInt("pk_nwsites")));
						site=new NetworkSites();
						siteIdentifier=new SiteIdentifier();
						map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
						siteIdentifier.setOID(map.getOID());
						siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
						
						if(rs.getString("network_level").equals("1")){
							
							pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
							fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
							netNameList.add(rs.getString("network_name"));
							code.setCode(rs.getString("code"));
							code.setType(rs.getString("stat_type"));
							code.setDescription(rs.getString("description"));
							//code.setHidden(rs.getString("stat_hidden").equals("N")?false:true);
							netStatList.add(code);
							
							//netStatList.add(rs.getString("description"));
							siteNameList.add(rs.getString("site_name"));
							netLevelList.add(String.valueOf(rs.getInt("network_level")));
							
						
							site.setSiteIdentifier(siteIdentifier);
							site.setLevel(String.valueOf(rs.getInt("network_level")));
							site.setName(rs.getString("site_name"));
							
							//code.setHidden(rs.getString("stat_hidden").equals("N")?false:true);
							site.setSiteStatus(code);
							//site.setSiteStatus(rs.getString("description"));	
							HashMap<String,Object> param=new HashMap<String,Object>();
								param.put("networkuser", networkuser);
								param.put("site", site);
								param.put("keySearch", keySearch);
								param.put("iaccId", iaccId);
								pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,site);
								// calling function for Child Network UserMoreDetails Fields.
								if(!pk_nwusers.equals("")){
									networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
								}
								
								//Calling funcion for Child Network User Roles.
								
							
						}
						if(rs.getInt("network_level")==2){
							sitelvl2=new NetworkSites();
							siteslvl3=new ArrayList<NetworkSites>();
							sitelvl2.setSiteIdentifier(siteIdentifier);
							sitelvl2.setLevel(String.valueOf(rs.getInt("network_level")));
							sitelvl2.setName(rs.getString("site_name"));
							sitelvl2.setSiteStatus(code);
							sitelvl2.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("code")!=null){
							code=new Code();
							code.setCode(rs.getString("code"));
							code.setType(rs.getString("stat_type"));
							code.setDescription(rs.getString("description"));
							sitelvl2.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl2.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							HashMap<String,Object> param=new HashMap<String,Object>();
							param.put("networkuser", networkuser);
							param.put("site", site);
							param.put("keySearch", keySearch);
							param.put("iaccId", iaccId);
							pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl2);
							// calling function for Child Network UserMoreDetails Fields.
							if(!pk_nwusers.equals("")){
								networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
							}
							siteslvl2.add(sitelvl2);
							site.setSite(siteslvl2);		
						}
						else if(rs.getInt("network_level")==3){
							sitelvl3=new NetworkSites();
							siteslvl4=new ArrayList<NetworkSites>();
							sitelvl3.setSiteIdentifier(siteIdentifier);
							sitelvl3.setLevel(String.valueOf(rs.getInt("network_level")));
							sitelvl3.setName(rs.getString("site_name"));
							sitelvl3.setSiteStatus(code);
							sitelvl3.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("code")!=null){
							code=new Code();
							code.setCode(rs.getString("code"));
							code.setType(rs.getString("stat_type"));
							code.setDescription(rs.getString("description"));
							sitelvl3.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl3.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							HashMap<String,Object> param=new HashMap<String,Object>();
							param.put("networkuser", networkuser);
							param.put("site", site);
							param.put("keySearch", keySearch);
							param.put("iaccId", iaccId);
							pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl3);
							// calling function for Child Network UserMoreDetails Fields.
							if(!pk_nwusers.equals("")){
								networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
							}
							siteslvl3.add(sitelvl3);
							sitelvl2.setSite(siteslvl3);
						}
						else if(rs.getInt("network_level")==4){
							sitelvl4=new NetworkSites();
							siteslvl5=new ArrayList<NetworkSites>();
							sitelvl4.setSiteIdentifier(siteIdentifier);
							sitelvl4.setLevel(String.valueOf(rs.getInt("network_level")));
							sitelvl4.setName(rs.getString("site_name"));
							sitelvl4.setSiteStatus(code);
							sitelvl4.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("code")!=null){
							code=new Code();
							code.setCode(rs.getString("code"));
							code.setType(rs.getString("stat_type"));
							code.setDescription(rs.getString("description"));
							sitelvl4.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl4.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							HashMap<String,Object> param=new HashMap<String,Object>();
							param.put("networkuser", networkuser);
							param.put("site", site);
							param.put("keySearch", keySearch);
							param.put("iaccId", iaccId);
							pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl4);
							// calling function for Child Network UserMoreDetails Fields.
							if(!pk_nwusers.equals("")){
								networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
							}
							siteslvl4.add(sitelvl4);
							sitelvl3.setSite(siteslvl4);
						}
						else if(rs.getInt("network_level")==5){
								sitelvl5=new NetworkSites();
								siteslvl6=new ArrayList<NetworkSites>();
								sitelvl5.setSiteIdentifier(siteIdentifier);
								sitelvl5.setLevel(String.valueOf(rs.getInt("network_level")));
								sitelvl5.setName(rs.getString("site_name"));
								sitelvl5.setSiteStatus(code);
								sitelvl5.setCTEP_ID(rs.getString("ctep_id"));
								if(rs.getString("code")!=null){
								code=new Code();
								code.setCode(rs.getString("code"));
								code.setType(rs.getString("stat_type"));
								code.setDescription(rs.getString("description"));
								sitelvl5.setSiteRelationShipType(code);
								}
								if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
									sitelvl5.setRelationship_PK(rs.getString("pk_nwsites"));
								}
								HashMap<String,Object> param=new HashMap<String,Object>();
								param.put("networkuser", networkuser);
								param.put("site", site);
								param.put("keySearch", keySearch);
								param.put("iaccId", iaccId);
								pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl5);
								// calling function for Child Network UserMoreDetails Fields.
								if(!pk_nwusers.equals("")){
									networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
								}
								siteslvl5.add(sitelvl5);
								sitelvl4.setSite(siteslvl5);
						}
						else if(rs.getInt("network_level")==6){
							sitelvl6=new NetworkSites();
							siteslvl7=new ArrayList<NetworkSites>();
							sitelvl6.setSiteIdentifier(siteIdentifier);
							sitelvl6.setLevel(String.valueOf(rs.getInt("network_level")));
							sitelvl6.setName(rs.getString("site_name"));
							sitelvl6.setSiteStatus(code);
							sitelvl6.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("code")!=null){
							code=new Code();
							code.setCode(rs.getString("code"));
							code.setType(rs.getString("stat_type"));
							code.setDescription(rs.getString("description"));
							sitelvl6.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl6.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							HashMap<String,Object> param=new HashMap<String,Object>();
							param.put("networkuser", networkuser);
							param.put("site", site);
							param.put("keySearch", keySearch);
							param.put("iaccId", iaccId);
							pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl6);
							// calling function for Child Network UserMoreDetails Fields.
							if(!pk_nwusers.equals("")){
								networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
							}
							siteslvl6.add(sitelvl6);
							sitelvl5.setSite(siteslvl6);
						}
						else if(rs.getInt("network_level")==7){
							sitelvl7=new NetworkSites();
							sitelvl7.setSiteIdentifier(siteIdentifier);
							sitelvl7.setLevel(String.valueOf(rs.getInt("network_level")));
							sitelvl7.setName(rs.getString("site_name"));
							sitelvl7.setSiteStatus(code);
							sitelvl7.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("code")!=null){
							code=new Code();
							code.setCode(rs.getString("code"));
							code.setType(rs.getString("stat_type"));
							code.setDescription(rs.getString("description"));
							sitelvl7.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl7.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							HashMap<String,Object> param=new HashMap<String,Object>();
							param.put("networkuser", networkuser);
							param.put("site", site);
							param.put("keySearch", keySearch);
							param.put("iaccId", iaccId);
							pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl7);
							// calling function for Child Network UserMoreDetails Fields.
							if(!pk_nwusers.equals("")){
								networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
							}
							siteslvl7.add(sitelvl7);
							sitelvl6.setSite(siteslvl7);
						}
						
						}
						pks_childNtw=pks_childNtw.length()>0?pks_childNtw.substring(0,pks_childNtw.length()-1):"0";
						if(!pks_childNtw.equals("0") && !pks_childNtw.equals(""))
						{
							StringBuffer sqlbuf3 = new StringBuffer();
							sqlbuf3.append("SELECT distinct pk_nwsites,  fk_site,  site_name, ctep_id,   site_name AS network_name, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,er_nwsites.NW_LEVEL+1 AS network_level FROM er_nwsites,er_site,er_nwusers,er_codelst WHERE pk_site=fk_site and pk_codelst=nw_status AND er_site.fk_account=? "); 
							sqlbuf3.append(" START WITH er_nwsites.pk_nwsites in ("+pks_childNtw+") CONNECT BY PRIOR er_nwsites.fk_nwsites=er_nwsites.pk_nwsites ORDER BY network_level");
							System.out.println("sqlbuf3==="+sqlbuf3.toString());
							pstmt = conn.prepareStatement(sqlbuf3.toString());
								pstmt.setInt(1, StringUtil.stringToNum(iaccId));
								rs = pstmt.executeQuery();
								pkNetList1="";
								siteList=new ArrayList<NetworkSites>();
								siteslvl2=new ArrayList<NetworkSites>();
								siteslvl3=new ArrayList<NetworkSites>();
								siteslvl4=new ArrayList<NetworkSites>();
								siteslvl5=new ArrayList<NetworkSites>();
								siteslvl6=new ArrayList<NetworkSites>();
								siteslvl7=new ArrayList<NetworkSites>();
								
								pk_nwusers= "";
								code=null;
								pks_childNtw= "";
								site=new NetworkSites();
								while (rs.next()) {
									System.out.println("Site Name==="+rs.getString("site_name"));
									code=new Code();
									pkNetList1=(String.valueOf(rs.getInt("pk_nwsites")));
									fk_site=String.valueOf(rs.getInt("fk_site"));
									siteIdentifier=new SiteIdentifier();
									map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
									siteIdentifier.setOID(map.getOID());
									siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
									
									if(rs.getString("network_level").equals("1")){
										
										pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
										fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
										netNameList.add(rs.getString("network_name"));
										code.setCode(rs.getString("code"));
										code.setType(rs.getString("stat_type"));
										code.setDescription(rs.getString("description"));
										//code.setHidden(rs.getString("stat_hidden").equals("N")?false:true);
										netStatList.add(code);
										
										//netStatList.add(rs.getString("description"));
										siteNameList.add(rs.getString("site_name"));
										netLevelList.add(String.valueOf(rs.getInt("network_level")));
										
									
										site.setSiteIdentifier(siteIdentifier);
										site.setLevel(String.valueOf(rs.getInt("network_level")));
										site.setName(rs.getString("site_name"));
										
										//code.setHidden(rs.getString("stat_hidden").equals("N")?false:true);
										site.setSiteStatus(code);
										//site.setSiteStatus(rs.getString("description"));	
										HashMap<String,Object> param=new HashMap<String,Object>();
										param.put("networkuser", networkuser);
										param.put("site", site);
										param.put("keySearch", keySearch);
										param.put("iaccId", iaccId);
										pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,site);
										// calling function for Child Network UserMoreDetails Fields.
										if(!pk_nwusers.equals("")){
											networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
										}
										
									}
									if(rs.getInt("network_level")==2){
										sitelvl2=new NetworkSites();
										siteslvl3=new ArrayList<NetworkSites>();
										sitelvl2.setSiteIdentifier(siteIdentifier);
										sitelvl2.setLevel(String.valueOf(rs.getInt("network_level")));
										sitelvl2.setName(rs.getString("site_name"));
										sitelvl2.setSiteStatus(code);
										sitelvl2.setCTEP_ID(rs.getString("ctep_id"));
										if(rs.getString("code")!=null){
										code=new Code();
										code.setCode(rs.getString("code"));
										code.setType(rs.getString("stat_type"));
										code.setDescription(rs.getString("description"));
										sitelvl2.setSiteRelationShipType(code);
										}
										if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
											sitelvl2.setRelationship_PK(rs.getString("pk_nwsites"));
										}
										System.out.println("Out");
										HashMap<String,Object> param=new HashMap<String,Object>();
										param.put("networkuser", networkuser);
										param.put("site", site);
										param.put("keySearch", keySearch);
										param.put("iaccId", iaccId);
										pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl2);
										// calling function for Child Network UserMoreDetails Fields.
										if(!pk_nwusers.equals("")){
											networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
										}
										siteslvl2.add(sitelvl2);
										site.setSite(siteslvl2);		
									}
									else if(rs.getInt("network_level")==3){
										sitelvl3=new NetworkSites();
										siteslvl4=new ArrayList<NetworkSites>();
										sitelvl3.setSiteIdentifier(siteIdentifier);
										sitelvl3.setLevel(String.valueOf(rs.getInt("network_level")));
										sitelvl3.setName(rs.getString("site_name"));
										sitelvl3.setSiteStatus(code);
										sitelvl3.setCTEP_ID(rs.getString("ctep_id"));
										if(rs.getString("code")!=null){
										code=new Code();
										code.setCode(rs.getString("code"));
										code.setType(rs.getString("stat_type"));
										code.setDescription(rs.getString("description"));
										sitelvl3.setSiteRelationShipType(code);
										}
										if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
											sitelvl3.setRelationship_PK(rs.getString("pk_nwsites"));
										}
										System.out.println("Out");
										HashMap<String,Object> param=new HashMap<String,Object>();
										param.put("networkuser", networkuser);
										param.put("site", site);
										param.put("keySearch", keySearch);
										param.put("iaccId", iaccId);
										pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl3);
										// calling function for Child Network UserMoreDetails Fields.
										if(!pk_nwusers.equals("")){
											networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
										}
										siteslvl3.add(sitelvl3);
										sitelvl2.setSite(siteslvl3);
									}
									else if(rs.getInt("network_level")==4){
										sitelvl4=new NetworkSites();
										siteslvl5=new ArrayList<NetworkSites>();
										sitelvl4.setSiteIdentifier(siteIdentifier);
										sitelvl4.setLevel(String.valueOf(rs.getInt("network_level")));
										sitelvl4.setName(rs.getString("site_name"));
										sitelvl4.setSiteStatus(code);
										sitelvl4.setCTEP_ID(rs.getString("ctep_id"));
										if(rs.getString("code")!=null){
										code=new Code();
										code.setCode(rs.getString("code"));
										code.setType(rs.getString("stat_type"));
										code.setDescription(rs.getString("description"));
										sitelvl4.setSiteRelationShipType(code);
										}
										if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
											sitelvl4.setRelationship_PK(rs.getString("pk_nwsites"));
										}
										HashMap<String,Object> param=new HashMap<String,Object>();
										param.put("networkuser", networkuser);
										param.put("site", site);
										param.put("keySearch", keySearch);
										param.put("iaccId", iaccId);
										pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl4);
										// calling function for Child Network UserMoreDetails Fields.
										if(!pk_nwusers.equals("")){
											networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
										}
										siteslvl4.add(sitelvl4);
										sitelvl3.setSite(siteslvl4);
									}
									else if(rs.getInt("network_level")==5){
											sitelvl5=new NetworkSites();
											siteslvl6=new ArrayList<NetworkSites>();
											sitelvl5.setSiteIdentifier(siteIdentifier);
											sitelvl5.setLevel(String.valueOf(rs.getInt("network_level")));
											sitelvl5.setName(rs.getString("site_name"));
											sitelvl5.setSiteStatus(code);
											sitelvl5.setCTEP_ID(rs.getString("ctep_id"));
											if(rs.getString("code")!=null){
											code=new Code();
											code.setCode(rs.getString("code"));
											code.setType(rs.getString("stat_type"));
											code.setDescription(rs.getString("description"));
											sitelvl5.setSiteRelationShipType(code);
											}
											if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
												sitelvl5.setRelationship_PK(rs.getString("pk_nwsites"));
											}
											HashMap<String,Object> param=new HashMap<String,Object>();
											param.put("networkuser", networkuser);
											param.put("site", site);
											param.put("keySearch", keySearch);
											param.put("iaccId", iaccId);
											pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl5);
											// calling function for Child Network UserMoreDetails Fields.
											if(!pk_nwusers.equals("")){
												networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
											}
											siteslvl5.add(sitelvl5);
											sitelvl4.setSite(siteslvl5);
									}
									else if(rs.getInt("network_level")==6){
										sitelvl6=new NetworkSites();
										siteslvl7=new ArrayList<NetworkSites>();
										sitelvl6.setSiteIdentifier(siteIdentifier);
										sitelvl6.setLevel(String.valueOf(rs.getInt("network_level")));
										sitelvl6.setName(rs.getString("site_name"));
										sitelvl6.setSiteStatus(code);
										sitelvl6.setCTEP_ID(rs.getString("ctep_id"));
										if(rs.getString("code")!=null){
										code=new Code();
										code.setCode(rs.getString("code"));
										code.setType(rs.getString("stat_type"));
										code.setDescription(rs.getString("description"));
										sitelvl6.setSiteRelationShipType(code);
										}
										if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
											sitelvl6.setRelationship_PK(rs.getString("pk_nwsites"));
										}
										HashMap<String,Object> param=new HashMap<String,Object>();
										param.put("networkuser", networkuser);
										param.put("site", site);
										param.put("keySearch", keySearch);
										param.put("iaccId", iaccId);
										pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl6);
										// calling function for Child Network UserMoreDetails Fields.
										if(!pk_nwusers.equals("")){
											networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
										}
										siteslvl6.add(sitelvl6);
										sitelvl5.setSite(siteslvl6);
									}
									else if(rs.getInt("network_level")==7){
										sitelvl7=new NetworkSites();
										sitelvl7.setSiteIdentifier(siteIdentifier);
										sitelvl7.setLevel(String.valueOf(rs.getInt("network_level")));
										sitelvl7.setName(rs.getString("site_name"));
										sitelvl7.setSiteStatus(code);
										sitelvl7.setCTEP_ID(rs.getString("ctep_id"));
										if(rs.getString("code")!=null){
										code=new Code();
										code.setCode(rs.getString("code"));
										code.setType(rs.getString("stat_type"));
										code.setDescription(rs.getString("description"));
										sitelvl7.setSiteRelationShipType(code);
										}
										if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
											sitelvl7.setRelationship_PK(rs.getString("pk_nwsites"));
										}
										HashMap<String,Object> param=new HashMap<String,Object>();
										param.put("networkuser", networkuser);
										param.put("site", site);
										param.put("keySearch", keySearch);
										param.put("iaccId", iaccId);
										pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl7);
										// calling function for Child Network UserMoreDetails Fields.
										if(!pk_nwusers.equals("")){
											networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
										}
										siteslvl7.add(sitelvl7);
										sitelvl6.setSite(siteslvl7);
									}

								}
						}
						siteList.add(site);
			}
						
					UsersitesList.add((ArrayList<NetworkSites>) siteList);
					
				
			
			}
	}catch(OperationException ex){
		throw ex;
	}
	catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			rs.close();
			if(rs1 != null){
			rs1.close();
		     }
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
	
	public String getNetPkMoreDetails(List<NVPair> moredetails,String flag){
		
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		String key="";
		String value="";
		String net_pks="";
		String sql="";

		try{
			conn = CommonDAO.getConnection();
			for(NVPair nvpair:moredetails){
				key=nvpair.getKey();
				
				ElementNSImpl val=(ElementNSImpl)nvpair.getValue();
				if(val!=null){
				NodeList node= val.getChildNodes();
		        for(int j=0;j< node.getLength();j++)
		           {
		        	  Node node1=node.item(j);
		        	  value=node1.getNodeValue();
		           }
				}
				StringBuffer sqlbuf = new StringBuffer();
					 
						 sqlbuf.append("SELECT fk_modpk FROM ER_MOREDETAILS,ER_CODELST WHERE MD_MODELEMENTPK=PK_CODELST AND CODELST_SUBTYP=? and codelst_type");
						 
						if(flag.equalsIgnoreCase("siteDetails")){
							 sqlbuf.append(" like 'org/__' escape '/' ");
						 }
						 if(flag.equalsIgnoreCase("networkUserDetail")){
							 sqlbuf.append(" like 'user/__' escape '/' ");
						 }
						 if(!value.equalsIgnoreCase("")){
						 sqlbuf.append(" AND  lower(MD_MODELEMENTDATA) LIKE lower('%"+value+"%')");
						 }
				System.out.println("Inside getNetPkMoreDetails sqlbuf==="+sqlbuf.toString());
				System.out.println("Key==="+key);
		        pstmt = conn.prepareStatement(sqlbuf.toString());
				pstmt.setString(1,key);
				rs = pstmt.executeQuery();
				while(rs.next()){
					net_pks=net_pks+rs.getString("fk_modpk")+",";
				}	
			}
			net_pks=net_pks.length()>0?net_pks.substring(0,net_pks.length()-1):"0";
			}
			catch(SQLException ex){
				ex.printStackTrace();
			}
			finally{
				try {
					rs.close();
					pstmt.close();
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		
		return net_pks;
	}
	public String pkMainNetwork(String sql,String iaccId){
		
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		String pk_main_network="";
		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(sql);
			if(iaccId != null){
			pstmt.setInt(1, StringUtil.stringToNum(iaccId));
			}
			rs = pstmt.executeQuery();
		
			while (rs.next()) {
				pk_main_network=pk_main_network+rs.getInt("fk_nwsites_main")+",";
			}
			pk_main_network=pk_main_network.length()>0?pk_main_network.substring(0,pk_main_network.length()-1):"0";
		}catch(SQLException ex){
			ex.printStackTrace();
		}
		finally{
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return pk_main_network;
	}
	
	
public String networkRoles(String pkNetwork,String fk_user,String iaccId,NetworkSites site){
		
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		String pkNwUsers = "";
		try {
			conn = CommonDAO.getConnection();

			StringBuffer sqlbuff = new StringBuffer();
			sqlbuff.append("SELECT pk_nwsites,pk_nwusers,fk_user,(select codelst_subtyp from er_codelst where pk_codelst=NWU_MEMBERTROLE) as role_subtype, NWU_MEMBERTROLE as role_PK, f_codelst_desc(NWU_MEMBERTROLE) AS role_name,cd.CODELST_DESC   AS description,cd.codelst_type   AS stat_type, cd.codelst_subtyp AS code,  cd.codelst_hide   AS stat_hidden FROM er_nwusers nwu, er_nwsites ns,er_site s,er_codelst cd WHERE nwu.FK_NWSITES=ns.PK_NWSITES and ns.PK_NWSITES=? AND  s.FK_ACCOUNT=? AND s.PK_SITE=ns.FK_SITE AND pk_codelst=nwu.NWU_STATUS  ");
			if(!fk_user.equalsIgnoreCase("")){
			sqlbuff.append(" and nwu.FK_USER= "+fk_user+"");
			}
			System.out.println("1st==="+sqlbuff.toString());
			System.out.println("pkNetwork==="+pkNetwork);
			pstmt = conn.prepareStatement(sqlbuff.toString());
			pstmt.setInt(1, StringUtil.stringToNum(pkNetwork));
			pstmt.setInt(2, StringUtil.stringToNum(iaccId));
			rs=pstmt.executeQuery();
			
			NetworkRole role=null;
			Roles roles=new Roles();
			Code code=null;
			List<NetworkRole> rolelist=new ArrayList<NetworkRole>();
			
			while(rs.next()){
				 code=new Code();
				 pkNwUsers= String.valueOf(rs.getInt("pk_nwusers"));
			    role=new NetworkRole();
			    role.setName(rs.getString("role_name"));
			    role.setPK(String.valueOf(rs.getInt("role_PK")));
			    role.setSubType(rs.getString("role_subtype"));
			    code.setCode(rs.getString("code"));
				code.setType(rs.getString("stat_type"));
				code.setDescription(rs.getString("description"));
				role.setRoleStatus(code);
				rolelist.add(role);		
			}
			if(!pkNwUsers.equals("")){
			StringBuffer sqlbuff1 = new StringBuffer();
			
			sqlbuff1.append("SELECT f_codelst_desc(NWU_MEMBERADDLTROLE) AS role_name,(select codelst_subtyp from er_codelst where pk_codelst=NWU_MEMBERADDLTROLE) as role_subtype, NWU_MEMBERADDLTROLE as role_PK, cd.CODELST_DESC AS description,  cd.codelst_type AS stat_type,  cd.codelst_subtyp AS code,  cd.codelst_hide AS stat_hidden FROM ER_NWUSERS_ADDNLROLES nwadr, er_codelst cd WHERE nwadr.FK_NWUSERS=?	AND cd.pk_codelst      =nwadr.NWU_ADTSTATUS");
			
			pstmt = conn.prepareStatement(sqlbuff1.toString());
			pstmt.setInt(1, StringUtil.stringToNum(pkNwUsers));
			rs=pstmt.executeQuery();
			System.out.println("2nd==="+sqlbuff.toString());
			System.out.println("pkNwUsers==="+pkNwUsers);
			while(rs.next()){
				 code=new Code();
			    role=new NetworkRole();
			    role.setName(rs.getString("role_name"));
			    role.setPK(String.valueOf(rs.getInt("role_PK")));
			    role.setSubType(rs.getString("role_subtype"));
			    code.setCode(rs.getString("code"));
				code.setType(rs.getString("stat_type"));
				code.setDescription(rs.getString("description"));
				role.setRoleStatus(code);
				rolelist.add(role);		
			}
			}
			roles.setRoles(rolelist);
			site.setRoles(roles);
				
			
			
		}catch(SQLException ex){
			ex.printStackTrace();
		}
		finally{
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return pkNwUsers;
		
	}

public void networkUserMoreDetails(int pkNWuser,String modName,String defUserGroup,HashMap<String,Object> param) throws OperationException{
	
	MoreDetailsJB mdJB=null;
	MoreDetailsDao mdDao;
	List<NVPair> listNVPair=new ArrayList<NVPair>();
	NVPair nvpair = null;
	ArrayList idList = new ArrayList();
	ArrayList modElementDescList = new ArrayList();
	ArrayList modElementDataList = new ArrayList();
	ArrayList modElementKeysList = new ArrayList();
	NetworkUserDetail networkuser=(NetworkUserDetail) param.get("networkuser");
	String iaccId = (String)param.get("iaccId");
	NetworkSites site =(NetworkSites) param.get("site");
	HashMap<String,Object> KeySearch=(HashMap<String, Object>) param.get("keySearch");
	Collection<NVPair> moreUserDetails =networkuser.getmoreUserDetails();
	System.out.println("Outside If");
	if(moreUserDetails==null){
		System.out.println("Inside If");
		mdJB = new MoreDetailsJB();
		mdDao = new MoreDetailsDao();
		System.out.println("pkNWuser==="+pkNWuser);
		System.out.println("modName==="+modName);
		System.out.println("defUserGroup==="+defUserGroup);
		mdDao = mdJB.getMoreDetails(pkNWuser,modName,defUserGroup);
		idList = mdDao.getId();
		modElementDescList = mdDao.getMdElementDescs();
		modElementDataList = mdDao.getMdElementValues();
		modElementKeysList = mdDao.getMdElementKeys();
		for(int iY=0; iY<idList.size(); iY++){
			if((Integer)idList.get(iY)>0){
			nvpair = new NVPair();
			nvpair.setKey((String) modElementKeysList.get(iY));
			nvpair.setValue((String)modElementDataList.get(iY));
			listNVPair.add(nvpair);
			}
		}
		}
		else{
			
			listNVPair=new UserServiceImpl().getNetworkMoreDetailValue(EJBUtil.integerToString(pkNWuser),KeySearch,StringUtil.stringToNum(modName.substring(modName.lastIndexOf("_")+1)),iaccId);
		}
		site.setUserDetailsFields(listNVPair);
	
	
}

@SuppressWarnings("resource")
public void searchNetworkUsersDetails(NetworkUserDetails networkuser, HashMap<String, Object> params) throws OperationException {
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	ResultSet rs1=null;
	int userPkCountN=0;
	int userPkCountS=0;
	String pkCode="";
	int count=0;
	String net_pks="";
	ObjectMap map=null;
	UserBean userBean = null;
	String networkName="";
	String siteName="";
	String networkPK = "";
	String sitePK = "";
	String relationShipPK = "";
	UserAgentRObj userAgent = (UserAgentRObj) params.get("userAgent");
	String iaccId = (String)params.get(KEY_ACCOUNT_ID);
	String CTEP_ID="";
	
	String relation_pks="";
	
	if((networkuser.getUserPK()!=null && !networkuser.getUserPK().equals("")) || 
			(networkuser.getUserLoginName()!=null && !networkuser.getUserLoginName().equals("")) || 
			(networkuser.getRolePK()!=null && !networkuser.getRolePK().equals(""))){
		relation_pks=getRelationPksFromUser(networkuser);
		if(relation_pks.equals("")){
			this.response=new ResponseHolder();
			this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User or Role Identifier Not Found")); 
			throw new OperationException();
		}
	}
	System.out.println("relation_pks==="+relation_pks);
	if(networkuser.getNetworkDetails()!=null){
		
    if(networkuser.getNetworkDetails().getNetworkName() != null ){
    	networkName = networkuser.getNetworkDetails().getNetworkName()==null?"":networkuser.getNetworkDetails().getNetworkName();
    }
    if(networkuser.getNetworkDetails().getNetworkPK() != null ){
    	networkPK = networkuser.getNetworkDetails().getNetworkPK()==null?"":networkuser.getNetworkDetails().getNetworkPK();
    	System.out.println("networkPK=="+networkPK);
    }
    if(networkuser.getNetworkDetails().getSitePK() != null ){
    	sitePK = networkuser.getNetworkDetails().getSitePK()==null?"":networkuser.getNetworkDetails().getSitePK();
    }
    if(networkuser.getNetworkDetails().getRelationshipPK() != null ){
    	relationShipPK = networkuser.getNetworkDetails().getRelationshipPK()==null?"":networkuser.getNetworkDetails().getRelationshipPK();
    }
    if(networkuser.getNetworkDetails().getSiteName() != null ){
    	siteName = networkuser.getNetworkDetails().getSiteName()==null?"":networkuser.getNetworkDetails().getSiteName();
    }
    if(networkuser.getNetworkDetails().getCTEP_ID() != null ){
    	CTEP_ID = networkuser.getNetworkDetails().getCTEP_ID()==null?"":networkuser.getNetworkDetails().getCTEP_ID();
    }
	if(networkuser.getNetworkDetails().getRelationshipType() != null && networkuser.getNetworkDetails().getRelationshipType().getCode() != null)
	{
		 CodeDao cd = new CodeDao();
		 pkCode = StringUtil.integerToString(cd.getCodeId(networkuser.getNetworkDetails().getRelationshipType().getType(), networkuser.getNetworkDetails().getRelationshipType().getCode()));
	}
	if(networkName.equals("") && networkPK.equals("")){
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network Identifier is Required.")); 
		throw new OperationException();
	}
	}
 
 if(networkuser.getMoreNetworkUserDetails()!=null){
	 net_pks=getNetPkMoreNetworkUsersDetails(networkuser.getMoreNetworkUserDetails(),"networkUserDetail");
 }
 if(!relationShipPK.equals("") && !relation_pks.equals("")){
	 relationShipPK=relationShipPK+","+relation_pks;
 }else if(!relation_pks.equals("")){
	 relationShipPK=relation_pks;
 }
 System.out.println("relationShipPK==="+relationShipPK);
 if(networkName.equals("") && networkPK.equals("") && sitePK.equals("") && relationShipPK.equals("") && siteName.equals("") && (pkCode.equals("")) && (networkuser.getMoreNetworkUserDetails() == null)){
	this.response=new ResponseHolder();
	this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
	this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid data is Required.")); 
	throw new OperationException();
 }     
			
	

StringBuffer sqlbuf = new StringBuffer();
String fk_user="";
String FK_NWSITES_MAIN="";
String str="";
String pk_main_network="";

if(!(siteName.equals("")) || !(sitePK.equals("")) || (!(pkCode.equals(""))) || !(networkPK.equals("")) || !(relationShipPK.equals(""))){
	str="SELECT distinct nvl(FK_NWSITES_MAIN,pk_nwsites) fk_nwsites_main FROM er_nwsites,er_site WHERE pk_site=fk_site and er_site.fk_account=? ";
	
	if(!(siteName.equals(""))){
		str=str+" and lower(site_name) like lower('%"+siteName+"%') ";
	}
	if(!(sitePK.equals(""))){
		str=str+" and fk_site="+sitePK;
	}
	if(!(CTEP_ID.equals(""))){
		str=str+" and lower(ctep_id) like lower('%"+CTEP_ID+"%') ";
	}
	if(!(pkCode.equals("0")) && !(pkCode.equals(""))){
		str=str+" and nw_membertype="+pkCode;
		}
	System.out.println("Inside networkPK==="+networkPK);
	if(networkPK.equals("") && relationShipPK.equals("")){
		pk_main_network = pkMainNetwork(str,iaccId);	
	}else{
		if(!networkPK.equals("")){
			pk_main_network=networkPK;
			System.out.println("pk_main_network Inside If==="+pk_main_network);
		}else{
			
			String getPrimaryNetwork="select rowtocol('select distinct nvl(fk_nwsites_main,pk_nwsites) FROM er_nwsites WHERE pk_nwsites in ("+relationShipPK+")') as fk_nwsites_main from dual";
			System.out.println("getPrimaryNetwork sql==="+getPrimaryNetwork);
 			try {
 				conn = CommonDAO.getConnection();
 				pstmt = conn.prepareStatement(getPrimaryNetwork);
 				rs=pstmt.executeQuery();
 				while(rs.next()){
 					if(rs.getString("fk_nwsites_main")!=null){
 						pk_main_network=rs.getString("fk_nwsites_main");
 					}
 				}
 				System.out.println("new condition pk_main_network==="+pk_main_network);
 			}catch(Exception e){
 				e.printStackTrace();
 			}
		}
	}
}

if((networkuser.getMoreNetworkUserDetails()!=null && networkuser.getMoreNetworkUserDetails().size()>0) && ( (networkName.equals("")) && (CTEP_ID.equals("")) && (networkPK.equals("")) && (sitePK.equals("")) && (relationShipPK.equals("")) && (siteName.equals("")) && ((pkCode.equals(""))))){
	String sql="select nvl(er_nwsites.fk_nwsites_main,pk_nwsites) as fk_nwsites_main  from er_nwsites, er_nwusers where pk_nwusers in ("+net_pks+") and er_nwusers.fk_nwsites=pk_nwsites";
	
	pk_main_network = pkMainNetwork(sql,null);
	
}
net_pks=net_pks.equals("")?net_pks:"";
System.out.println("pk_main_network==="+pk_main_network);
	String str1 = "select pk_nwsites,fk_site,site_name ,site_name as network_name,NW_LEVEL+1 as nw_level,er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden, "
			+" (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
			+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, CTEP_ID "
			+ "from er_nwsites,er_site,er_codelst "+
				  "where pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? and nw_level=0 ";
	if(!(networkName.equals(""))){
			 	 str1=str1+" and lower(site_name) like lower('%"+networkName+"%') ";
		}
	//if(networkName.equals("") && pk_main_network.length()>0)
	if(pk_main_network.length()>0)
	{
		str1=str1+" and pk_nwsites in("+pk_main_network+") ";
	}
try {
	System.out.println("str1==="+str1.toString());
conn = CommonDAO.getConnection();
pstmt = conn.prepareStatement(str1);
pstmt.setInt(1, StringUtil.stringToNum(iaccId));
rs = pstmt.executeQuery();
Code code=null;
Code code1=null;
while (rs.next()) {
	System.out.println("Inside While");
	code = new Code();
	pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
	fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
	netNameList.add(rs.getString("network_name"));
	code.setType(rs.getString("stat_type"));
	code.setCode(rs.getString("code"));
	code.setDescription(rs.getString("description"));
	netStatList.add(code);
	ctepList.add(rs.getString("CTEP_ID"));
	if(rs.getString("SiteRelationshipType")!=null){
		code1=new Code();
		code1.setType(rs.getString("SiteRelationshipType"));
		code1.setCode(rs.getString("SiteRelationshipSubType"));
		code1.setDescription(rs.getString("SiteRelationshipDesc"));
	}
	siteNameList.add(rs.getString("site_name"));
	netLevelList.add(String.valueOf(rs.getInt("nw_level")));
	count++;
}
System.out.println("count==="+count);
if(count==0)
{
	this.response=new ResponseHolder();
	this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
	this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network not found")); 
	throw new OperationException();
}

	NetworkSitesUsers site=null;
	List<NetworkSitesUsers> siteList=null;
	SiteIdentifier siteIdentifier=null;	
	ObjectMapService objectMapService;
	
	for(int i=0;i<pkNetList.size();i++){
		site=new NetworkSitesUsers();
		siteList=new ArrayList<NetworkSitesUsers>();
		siteIdentifier=new SiteIdentifier();
		map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
		siteIdentifier.setOID(map.getOID());
		siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
		siteIdentifier.setName(siteNameList.get(i).toString());
		site.setSiteIdentifier(siteIdentifier);
		site.setLevel(String.valueOf(netLevelList.get(i)));
		site.setSiteStatus(netStatList.get(i));
		site.setCTEP_ID(ctepList.get(i).toString());
		if(code1!=null){
			site.setSiteRelationShipType(code1);
		}
		objectMapService = ((ObjectMapService)params.get("objectMapService"));
		params.put("objectMapService", objectMapService);
		userPkCountN = networkUsersRoles(pkNetList.get(i),iaccId,site,params,net_pks);
		System.out.println("userPkCountN==="+userPkCountN);
		String pks_childNtw="";
		if(!(CTEP_ID.equals("")) || !(relationShipPK.equals("")) || !(siteName.equals("")) || !(sitePK.equals("")) || (!(pkCode.equals("")))){
			
			StringBuffer sqlbuf2 = new StringBuffer();
		sqlbuf2.append("SELECT pk_nwsites, fk_site,  site_name , site_name AS network_name, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,er_nwsites.NW_LEVEL+1 AS nw_level, "
				+" (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
				+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide "
				+ "FROM er_nwsites,er_site,er_codelst WHERE pk_site=fk_site AND er_site.fk_account=? AND (er_nwsites.FK_NWSITES_MAIN =? or er_nwsites.PK_NWSITES =?) and pk_codelst=nw_status ");
		if(!(pkCode.equals(""))){
 	 		sqlbuf2.append(" and nw_membertype="+pkCode+"");
 	 	}
		if(!(siteName.equals("")))
	    {
		sqlbuf2.append(" and lower(site_name) like lower('%" +siteName+ "%')");
		 }
		if(!sitePK.equals("")){
			sqlbuf2.append(" and er_nwsites.fk_site="+sitePK+"");
		}
		if(!(CTEP_ID.equals(""))){
			sqlbuf2.append(" and lower(ctep_id) like lower('%"+CTEP_ID+"%') ");
		}
		if(!(relationShipPK.equals("")))
		{
			if(!relation_pks.equals("")){
				sqlbuf2.append(" and pk_nwsites in ("+relationShipPkMap.get(pkNetList.get(i)).toString()+") ");
			}else{
				sqlbuf2.append(" and pk_nwsites in ("+relationShipPK+") ");
			}
		}
		System.out.println("sqlbuf2==="+sqlbuf2.toString());
		System.out.println("pkNetList.get(i)==="+pkNetList.get(i));
		
		pstmt = conn.prepareStatement(sqlbuf2.toString());
		pstmt.setInt(1, StringUtil.stringToNum(iaccId));
		pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
		pstmt.setInt(3, StringUtil.stringToNum(pkNetList.get(i)));
		rs = pstmt.executeQuery();
	
		
		while (rs.next()) {
			pks_childNtw=pks_childNtw+rs.getInt("pk_nwsites")+",";
		}
		pks_childNtw=pks_childNtw.length()>0?pks_childNtw.substring(0,pks_childNtw.length()-1):"0";
	}
		
		if(!pks_childNtw.equals("0") && !pks_childNtw.equals("")){
			
			System.out.println("Inside If");
		NetworkSitesUsers sitelvl2=null;
		NetworkSitesUsers sitelvl3=null;
		NetworkSitesUsers sitelvl4=null;
		NetworkSitesUsers sitelvl5=null;
		NetworkSitesUsers sitelvl6=null;
		NetworkSitesUsers sitelvl7=null;

		List<NetworkSitesUsers> siteslvl2=null;
		List<NetworkSitesUsers> siteslvl3=null;
		List<NetworkSitesUsers> siteslvl4=null;
		List<NetworkSitesUsers> siteslvl5=null;
		List<NetworkSitesUsers> siteslvl6=null;
		List<NetworkSitesUsers> siteslvl7=null;	
		StringBuffer sqlbuf3 = new StringBuffer();
		sqlbuf3.append("SELECT pk_nwsites, fk_site,  site_name ,ctep_id, site_name AS network_name, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,er_nwsites.NW_LEVEL+1 AS nw_level, "
				+" (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
				+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide "
				+ "FROM er_nwsites,er_site,er_codelst WHERE pk_site=fk_site AND er_site.fk_account=? AND er_nwsites.FK_NWSITES_MAIN =? and pk_codelst=nw_status "); 
		
		sqlbuf3.append(" and pk_nwsites <> ? START WITH er_nwsites.pk_nwsites in (?) CONNECT BY PRIOR er_nwsites.fk_nwsites=er_nwsites.pk_nwsites ORDER BY nw_level");
		System.out.println("sqlbuf3==="+sqlbuf3.toString());
		System.out.println("pks_childNtw==="+pks_childNtw);
		System.out.println("pkNetList.get(i)==="+pkNetList.get(i));
		    pstmt = conn.prepareStatement(sqlbuf3.toString());
			pstmt.setInt(1, StringUtil.stringToNum(iaccId));
			pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
			pstmt.setInt(3, StringUtil.stringToNum(pkNetList.get(i)));
			pstmt.setInt(4, StringUtil.stringToNum(pks_childNtw));
			rs = pstmt.executeQuery();
			String pkNetList1="";
			Code cod=null;
			siteslvl2=new ArrayList<NetworkSitesUsers>();
			siteslvl3=new ArrayList<NetworkSitesUsers>();
			siteslvl4=new ArrayList<NetworkSitesUsers>();
			siteslvl5=new ArrayList<NetworkSitesUsers>();
			siteslvl6=new ArrayList<NetworkSitesUsers>();
			siteslvl7=new ArrayList<NetworkSitesUsers>();
			
			while (rs.next()) {
				
				code=new Code();
				siteIdentifier=new SiteIdentifier();
				map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
				siteIdentifier.setOID(map.getOID());
				siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
				siteIdentifier.setName(rs.getString("site_name"));
				code.setType(rs.getString("stat_type"));
				code.setCode(rs.getString("code"));
				code.setDescription(rs.getString("description"));
				
				System.out.println("site name==="+rs.getString("site_name"));
				if(rs.getInt("nw_level")==2){
					sitelvl2=new NetworkSitesUsers();
					siteslvl3=new ArrayList<NetworkSitesUsers>();
					sitelvl2.setSiteIdentifier(siteIdentifier);
					sitelvl2.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl2.setSiteStatus(code);
					sitelvl2.setCTEP_ID(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
						code=new Code();
						code.setType(rs.getString("SiteRelationshipType"));
						code.setCode(rs.getString("SiteRelationshipSubType"));
						code.setDescription(rs.getString("SiteRelationshipDesc"));
						sitelvl2.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl2.setRelationship_PK(rs.getString("pk_nwsites"));
					}
					String pk_nwsites=rs.getString("pk_nwsites");
					userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl2,params,"");
					siteslvl2.add(sitelvl2);
					site.setSites(siteslvl2);
				}
				else if(rs.getInt("nw_level")==3){
					sitelvl3=new NetworkSitesUsers();
					siteslvl4=new ArrayList<NetworkSitesUsers>();
					sitelvl3.setSiteIdentifier(siteIdentifier);
					sitelvl3.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl3.setSiteStatus(code);
					sitelvl3.setCTEP_ID(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
						code=new Code();
						code.setType(rs.getString("SiteRelationshipType"));
						code.setCode(rs.getString("SiteRelationshipSubType"));
						code.setDescription(rs.getString("SiteRelationshipDesc"));
						sitelvl3.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl3.setRelationship_PK(rs.getString("pk_nwsites"));
					}
					String pk_nwsites=rs.getString("pk_nwsites");
					userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl3,params,"");
					siteslvl3.add(sitelvl3);
					sitelvl2.setSites(siteslvl3);
				}
				else if(rs.getInt("nw_level")==4){
					sitelvl4=new NetworkSitesUsers();
					siteslvl5=new ArrayList<NetworkSitesUsers>();
					sitelvl4.setSiteIdentifier(siteIdentifier);
					sitelvl4.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl4.setSiteStatus(code);
					sitelvl4.setCTEP_ID(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
						code=new Code();
						code.setType(rs.getString("SiteRelationshipType"));
						code.setCode(rs.getString("SiteRelationshipSubType"));
						code.setDescription(rs.getString("SiteRelationshipDesc"));
						sitelvl4.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl4.setRelationship_PK(rs.getString("pk_nwsites"));
					}
					String pk_nwsites=rs.getString("pk_nwsites");
					userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl4,params,"");
					siteslvl4.add(sitelvl4);
					sitelvl3.setSites(siteslvl4);
				}
				else if(rs.getInt("nw_level")==5){
					sitelvl5=new NetworkSitesUsers();
					siteslvl6=new ArrayList<NetworkSitesUsers>();
					sitelvl5.setSiteIdentifier(siteIdentifier);
					sitelvl5.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl5.setSiteStatus(code);
					sitelvl5.setCTEP_ID(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
						code=new Code();
						code.setType(rs.getString("SiteRelationshipType"));
						code.setCode(rs.getString("SiteRelationshipSubType"));
						code.setDescription(rs.getString("SiteRelationshipDesc"));
						sitelvl5.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl5.setRelationship_PK(rs.getString("pk_nwsites"));
					}
					String pk_nwsites=rs.getString("pk_nwsites");
					userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl5,params,"");
					siteslvl5.add(sitelvl5);
					sitelvl4.setSites(siteslvl5);
				}
				else if(rs.getInt("nw_level")==6){
					sitelvl6=new NetworkSitesUsers();
					siteslvl7=new ArrayList<NetworkSitesUsers>();
					sitelvl6.setSiteIdentifier(siteIdentifier);
					sitelvl6.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl6.setSiteStatus(code);
					sitelvl6.setCTEP_ID(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
						code=new Code();
						code.setType(rs.getString("SiteRelationshipType"));
						code.setCode(rs.getString("SiteRelationshipSubType"));
						code.setDescription(rs.getString("SiteRelationshipDesc"));
						sitelvl6.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl6.setRelationship_PK(rs.getString("pk_nwsites"));
					}
					
					String pk_nwsites=rs.getString("pk_nwsites");
					userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl6,params,"");
					siteslvl6.add(sitelvl6);
					sitelvl5.setSites(siteslvl6);
				}
				else if(rs.getInt("nw_level")==7){
					sitelvl7=new NetworkSitesUsers();
					siteIdentifier=new SiteIdentifier();
					sitelvl7.setSiteIdentifier(siteIdentifier);
					sitelvl7.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl7.setSiteStatus(code);
					sitelvl7.setCTEP_ID(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
						code=new Code();
						code.setType(rs.getString("SiteRelationshipType"));
						code.setCode(rs.getString("SiteRelationshipSubType"));
						code.setDescription(rs.getString("SiteRelationshipDesc"));
						sitelvl7.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl7.setRelationship_PK(rs.getString("pk_nwsites"));
					}
					String pk_nwsites=rs.getString("pk_nwsites");
					userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl7,params,"");
					siteslvl7.add(sitelvl7);
					sitelvl6.setSites(siteslvl7);
				}
			}
				StringBuffer sqlbuf4 = new StringBuffer();
				sqlbuf4.append("SELECT pk_nwsites, fk_site,  site_name ,ctep_id, site_name AS network_name, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,er_nwsites.NW_LEVEL+1 AS nw_level, "
						+" (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
						+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide "
						+ "FROM er_nwsites,er_site,er_codelst WHERE pk_site=fk_site AND er_site.fk_account=? AND er_nwsites.FK_NWSITES_MAIN =? and pk_codelst=nw_status "); 
				
				sqlbuf4.append(" and pk_nwsites not in (?) START WITH er_nwsites.pk_nwsites in (?) CONNECT BY PRIOR er_nwsites.pk_nwsites=er_nwsites.fk_nwsites ORDER SIBLINGS BY lower(site_name)");
				System.out.println("sqlbuf4==="+sqlbuf4.toString());
				System.out.println("pks_childNtw==="+pks_childNtw);
			pstmt = conn.prepareStatement(sqlbuf4.toString());
			pstmt.setInt(1, StringUtil.stringToNum(iaccId));
			pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
			pstmt.setInt(3, StringUtil.stringToNum(pks_childNtw));
			pstmt.setInt(4, StringUtil.stringToNum(pks_childNtw));
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
					
					code=new Code();
					siteIdentifier=new SiteIdentifier();
					map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
					siteIdentifier.setOID(map.getOID());
					siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
					siteIdentifier.setName(rs.getString("site_name"));
					code.setType(rs.getString("stat_type"));
					code.setCode(rs.getString("code"));
					code.setDescription(rs.getString("description"));
					
					if(rs.getInt("nw_level")==2){
						sitelvl2=new NetworkSitesUsers();
						siteslvl3=new ArrayList<NetworkSitesUsers>();
						sitelvl2.setSiteIdentifier(siteIdentifier);
						sitelvl2.setLevel(String.valueOf(rs.getInt("nw_level")));
						sitelvl2.setSiteStatus(code);
						sitelvl2.setCTEP_ID(rs.getString("ctep_id"));
						if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl2.setSiteRelationShipType(code);
						}
						if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
							sitelvl2.setRelationship_PK(rs.getString("pk_nwsites"));
						}
						String pk_nwsites=rs.getString("pk_nwsites");
						userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl2,params,"");
						siteslvl2.add(sitelvl2);
						site.setSites(siteslvl2);
					}
					else if(rs.getInt("nw_level")==3){
						sitelvl3=new NetworkSitesUsers();
						siteslvl4=new ArrayList<NetworkSitesUsers>();
						sitelvl3.setSiteIdentifier(siteIdentifier);
						sitelvl3.setLevel(String.valueOf(rs.getInt("nw_level")));
						sitelvl3.setSiteStatus(code);
						sitelvl3.setCTEP_ID(rs.getString("ctep_id"));
						if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl3.setSiteRelationShipType(code);
						}
						if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
							sitelvl3.setRelationship_PK(rs.getString("pk_nwsites"));
						}
						String pk_nwsites=rs.getString("pk_nwsites");
						userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl3,params,"");
						siteslvl3.add(sitelvl3);
						sitelvl2.setSites(siteslvl3);
					}
					else if(rs.getInt("nw_level")==4){
						sitelvl4=new NetworkSitesUsers();
						siteslvl5=new ArrayList<NetworkSitesUsers>();
						sitelvl4.setSiteIdentifier(siteIdentifier);
						sitelvl4.setLevel(String.valueOf(rs.getInt("nw_level")));
						sitelvl4.setSiteStatus(code);
						sitelvl4.setCTEP_ID(rs.getString("ctep_id"));
						if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl4.setSiteRelationShipType(code);
						}
						if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
							sitelvl4.setRelationship_PK(rs.getString("pk_nwsites"));
						}
						String pk_nwsites=rs.getString("pk_nwsites");
						userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl4,params,"");
						siteslvl4.add(sitelvl4);
						sitelvl3.setSites(siteslvl4);
					}
					else if(rs.getInt("nw_level")==5){
						sitelvl5=new NetworkSitesUsers();
						siteslvl6=new ArrayList<NetworkSitesUsers>();
						sitelvl5.setSiteIdentifier(siteIdentifier);
						sitelvl5.setLevel(String.valueOf(rs.getInt("nw_level")));
						sitelvl5.setSiteStatus(code);
						sitelvl5.setCTEP_ID(rs.getString("ctep_id"));
						if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl5.setSiteRelationShipType(code);
						}
						if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
							sitelvl5.setRelationship_PK(rs.getString("pk_nwsites"));
						}
						String pk_nwsites=rs.getString("pk_nwsites");
						userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl5,params,"");
						siteslvl5.add(sitelvl5);
						sitelvl4.setSites(siteslvl5);
					}
					else if(rs.getInt("nw_level")==6){
						sitelvl6=new NetworkSitesUsers();
						siteslvl7=new ArrayList<NetworkSitesUsers>();
						sitelvl6.setSiteIdentifier(siteIdentifier);
						sitelvl6.setLevel(String.valueOf(rs.getInt("nw_level")));
						sitelvl6.setSiteStatus(code);
						sitelvl6.setCTEP_ID(rs.getString("ctep_id"));
						if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl6.setSiteRelationShipType(code);
						}
						if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
							sitelvl6.setRelationship_PK(rs.getString("pk_nwsites"));
						}
						String pk_nwsites=rs.getString("pk_nwsites");
						userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl6,params,"");
						siteslvl6.add(sitelvl6);
						sitelvl5.setSites(siteslvl6);
					}
					else if(rs.getInt("nw_level")==7){
						sitelvl7=new NetworkSitesUsers();
						sitelvl7.setSiteIdentifier(siteIdentifier);
						sitelvl7.setLevel(String.valueOf(rs.getInt("nw_level")));
						sitelvl7.setSiteStatus(code);
						sitelvl7.setCTEP_ID(rs.getString("ctep_id"));
						if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl7.setSiteRelationShipType(code);
						}
						if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
							sitelvl7.setRelationship_PK(rs.getString("pk_nwsites"));
						}
						String pk_nwsites=rs.getString("pk_nwsites");
						userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl7,params,"");
						siteslvl7.add(sitelvl7);
						sitelvl6.setSites(siteslvl7);
					}
					
			
			}
		}
		
			else{
				System.out.println("Inside else");
				StringBuffer sqlbuf3 = new StringBuffer();
				sqlbuf3.append("SELECT pk_nwsites, fk_site,  site_name ,ctep_id, site_name AS network_name, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,er_nwsites.NW_LEVEL+1 AS nw_level, "
						+" (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
						+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide "
						+ "FROM er_nwsites,er_site,er_codelst WHERE pk_site=fk_site AND er_site.fk_account=? AND er_nwsites.FK_NWSITES_MAIN =? and pk_codelst=nw_status "); 
				
				sqlbuf3.append(" and pk_nwsites <> ? START WITH er_nwsites.pk_nwsites in (?) CONNECT BY PRIOR er_nwsites.pk_nwsites=er_nwsites.fk_nwsites ORDER SIBLINGS BY lower(site_name)");
				System.out.println("sqlbuf3==="+sqlbuf3.toString());
				System.out.println("pkNetList.get(i)==="+pkNetList.get(i));
				    pstmt = conn.prepareStatement(sqlbuf3.toString());
					pstmt.setInt(1, StringUtil.stringToNum(iaccId));
					pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
					pstmt.setInt(3, StringUtil.stringToNum(pkNetList.get(i)));
					pstmt.setInt(4, StringUtil.stringToNum(pkNetList.get(i)));
					rs = pstmt.executeQuery();
					String pkNetList1="";
					Code cod=null;
					NetworkSitesUsers sitelvl2=null;
					NetworkSitesUsers sitelvl3=null;
					NetworkSitesUsers sitelvl4=null;
					NetworkSitesUsers sitelvl5=null;
					NetworkSitesUsers sitelvl6=null;
					NetworkSitesUsers sitelvl7=null;

					List<NetworkSitesUsers> siteslvl2=null;
					List<NetworkSitesUsers> siteslvl3=null;
					List<NetworkSitesUsers> siteslvl4=null;
					List<NetworkSitesUsers> siteslvl5=null;
					List<NetworkSitesUsers> siteslvl6=null;
					List<NetworkSitesUsers> siteslvl7=null;	
					siteslvl2=new ArrayList<NetworkSitesUsers>();
					siteslvl3=new ArrayList<NetworkSitesUsers>();
					siteslvl4=new ArrayList<NetworkSitesUsers>();
					siteslvl5=new ArrayList<NetworkSitesUsers>();
					siteslvl6=new ArrayList<NetworkSitesUsers>();
					siteslvl7=new ArrayList<NetworkSitesUsers>();
					while (rs.next()) {
						
						code=new Code();
						siteIdentifier=new SiteIdentifier();
						map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
						siteIdentifier.setOID(map.getOID());
						siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
						siteIdentifier.setName(rs.getString("site_name"));
						code.setType(rs.getString("stat_type"));
						code.setCode(rs.getString("code"));
						code.setDescription(rs.getString("description"));
						
						if(rs.getInt("nw_level")==2){
							sitelvl2=new NetworkSitesUsers();
							siteslvl3=new ArrayList<NetworkSitesUsers>();
							sitelvl2.setSiteIdentifier(siteIdentifier);
							sitelvl2.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl2.setSiteStatus(code);
							sitelvl2.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
								code=new Code();
								code.setType(rs.getString("SiteRelationshipType"));
								code.setCode(rs.getString("SiteRelationshipSubType"));
								code.setDescription(rs.getString("SiteRelationshipDesc"));
								sitelvl2.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl2.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							String pk_nwsites=rs.getString("pk_nwsites");
							userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl2,params,"");
							siteslvl2.add(sitelvl2);
							site.setSites(siteslvl2);
						}
						else if(rs.getInt("nw_level")==3){
							sitelvl3=new NetworkSitesUsers();
							siteslvl4=new ArrayList<NetworkSitesUsers>();
							sitelvl3.setSiteIdentifier(siteIdentifier);
							sitelvl3.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl3.setSiteStatus(code);
							sitelvl3.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
								code=new Code();
								code.setType(rs.getString("SiteRelationshipType"));
								code.setCode(rs.getString("SiteRelationshipSubType"));
								code.setDescription(rs.getString("SiteRelationshipDesc"));
								sitelvl3.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl3.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							String pk_nwsites=rs.getString("pk_nwsites");
							userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl3,params,"");
							siteslvl3.add(sitelvl3);
							sitelvl2.setSites(siteslvl3);
						}
						else if(rs.getInt("nw_level")==4){
							sitelvl4=new NetworkSitesUsers();
							siteslvl5=new ArrayList<NetworkSitesUsers>();
							sitelvl4.setSiteIdentifier(siteIdentifier);
							sitelvl4.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl4.setSiteStatus(code);
							sitelvl4.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
								code=new Code();
								code.setType(rs.getString("SiteRelationshipType"));
								code.setCode(rs.getString("SiteRelationshipSubType"));
								code.setDescription(rs.getString("SiteRelationshipDesc"));
								sitelvl4.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl4.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							String pk_nwsites=rs.getString("pk_nwsites");
							userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl4,params,"");
							siteslvl4.add(sitelvl4);
							sitelvl3.setSites(siteslvl4);
						}
						else if(rs.getInt("nw_level")==5){
							sitelvl5=new NetworkSitesUsers();
							siteslvl6=new ArrayList<NetworkSitesUsers>();
							sitelvl5.setSiteIdentifier(siteIdentifier);
							sitelvl5.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl5.setSiteStatus(code);
							sitelvl5.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
								code=new Code();
								code.setType(rs.getString("SiteRelationshipType"));
								code.setCode(rs.getString("SiteRelationshipSubType"));
								code.setDescription(rs.getString("SiteRelationshipDesc"));
								sitelvl5.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl5.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							String pk_nwsites=rs.getString("pk_nwsites");
							userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl5,params,"");
							siteslvl5.add(sitelvl5);
							sitelvl4.setSites(siteslvl5);
						}
						else if(rs.getInt("nw_level")==6){
							sitelvl6=new NetworkSitesUsers();
							siteslvl7=new ArrayList<NetworkSitesUsers>();
							sitelvl6.setSiteIdentifier(siteIdentifier);
							sitelvl6.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl6.setSiteStatus(code);
							sitelvl6.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
								code=new Code();
								code.setType(rs.getString("SiteRelationshipType"));
								code.setCode(rs.getString("SiteRelationshipSubType"));
								code.setDescription(rs.getString("SiteRelationshipDesc"));
								sitelvl6.setSiteRelationShipType(code);
							}
							String pk_nwsites=rs.getString("pk_nwsites");
							userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl6,params,"");
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl6.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							siteslvl6.add(sitelvl6);
							sitelvl5.setSites(siteslvl6);
						}
						else if(rs.getInt("nw_level")==7){
							sitelvl7=new NetworkSitesUsers();
							siteIdentifier=new SiteIdentifier();
							sitelvl7.setSiteIdentifier(siteIdentifier);
							sitelvl7.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl7.setSiteStatus(code);
							sitelvl7.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
								code=new Code();
								code.setType(rs.getString("SiteRelationshipType"));
								code.setCode(rs.getString("SiteRelationshipSubType"));
								code.setDescription(rs.getString("SiteRelationshipDesc"));
								sitelvl7.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl7.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							String pk_nwsites=rs.getString("pk_nwsites");
							userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl7,params,"");
							siteslvl7.add(sitelvl7);
							sitelvl6.setSites(siteslvl7);
						}
						
					}
			}
		System.out.println("Here");
		siteList.add(site);
			/*while (rs.next()) {
				cod = new Code();
				pkNetList1=(String.valueOf(rs.getInt("pk_nwsites")));
				site=new NetworkSitesUsers();
				siteIdentifier=new SiteIdentifier();
				map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
				siteIdentifier.setOID(map.getOID());
				siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
				site.setSiteIdentifier(siteIdentifier);
				site.setLevel(String.valueOf(rs.getInt("network_level")));
				site.setName(rs.getString("site_name"));
				//site.setSiteStatus(rs.getString("network_status"));
				cod.setType(rs.getString("stat_type"));
				cod.setCode(rs.getString("code"));
				cod.setDescription(rs.getString("description"));
				site.setSiteStatus(cod);
				siteList.add(site);
				userPkCountS = networkUsersRoles(pkNetList1,iaccId,site,params,net_pks);
			
			}*/
		
				
		/*if(userPkCountN==0 && userPkCountS==0){
			this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND, "No User is found in this network.")); 
			throw new OperationException();
		}else{*/
			NetworksitesusersList.add((ArrayList<NetworkSitesUsers>) siteList);
		/*}*/
			
	
	

}}catch(OperationException ex){
	throw ex;
}
catch (Exception e) {
	e.printStackTrace();
} finally {
	try {
		rs.close();
		if(rs1 != null){
		rs1.close();
	     }
		pstmt.close();
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
}


public void getNetworkSiteLevel(NetworkSiteLevelDetails networkSiteLevelDetails, HashMap<String, Object> params) throws OperationException {
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	String iaccId = (String)params.get(KEY_ACCOUNT_ID);
	//UserBean userBean=(UserBean)params.get("callingUser");
	String networkName = (networkSiteLevelDetails.getNetworkName()==null)?"":networkSiteLevelDetails.getNetworkName();
	String siteName = (networkSiteLevelDetails.getSiteName()==null)?"":networkSiteLevelDetails.getSiteName();
	String sitePk= (networkSiteLevelDetails.getSitePK()==null)?"":networkSiteLevelDetails.getSitePK();
	String networkPk = (networkSiteLevelDetails.getNetworkPK()==null)?"":networkSiteLevelDetails.getNetworkPK();
	
	String str = "";
	String pk_main_network = "";
	
try {
	
	str="SELECT distinct nvl(FK_NWSITES_MAIN,pk_nwsites) fk_nwsites_main FROM er_nwsites,er_site WHERE er_site.fk_account=? and pk_site=fk_site ";
	
	if(!(networkPk.equals(""))){
		str=str+" and nvl(FK_NWSITES_MAIN,pk_nwsites)="+networkPk;
	}
	
	if(!networkName.equals("")){
		str=str+"and nvl(FK_NWSITES_MAIN,pk_nwsites) in (select pk_nwsites from er_nwsites,er_site where pk_site=fk_site and site_name like '"+networkName+"' and nw_level=0)";
	}
	
	if(!(sitePk.equals(""))){
		str=str +" and fk_site="+sitePk;
	}
	
	if(!(siteName.equals(""))){
		str=str +" and site_name like '"+siteName+"'";
	}
	
	pk_main_network = pkMainNetwork(str,iaccId);
	
	if(pk_main_network.equals("0")){
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found")); 
		throw new OperationException();
	}
	
	 String str1 = "select pk_nwsites,fk_site,site_name ,site_name as network_name,ctep_id,nvl(site_info,'-') as network_info, er_codelst.pk_codelst as pkStat, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden, (SELECT pk_codelst FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) as pkRelnType, (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType, nw_level from er_nwsites,er_site,er_codelst "+
	 			" where pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? and nw_level=0 ";
	 		 	
	 	if(pk_main_network.length()>0 && !pk_main_network.equals("0"))
	 	{
	 		str1=str1+" and pk_nwsites in("+pk_main_network+") ";
	 	}

	conn = CommonDAO.getConnection();
	pstmt = conn.prepareStatement(str1);
	pstmt.setInt(1, StringUtil.stringToNum(iaccId));
	rs = pstmt.executeQuery();
	Code code=null;
	while (rs.next()) {
		code=new Code();
		pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
		fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
		if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
			pkRelnShipType.add(rs.getString("pk_nwsites"));
		}
		else{
			pkRelnShipType.add("0");
		}
		netNameList.add(rs.getString("network_name"));
		netInfoList.add(rs.getString("network_info"));
		code.setPk(rs.getString("pkStat"));
		code.setType(rs.getString("stat_type"));
		code.setCode(rs.getString("code"));
		code.setDescription(rs.getString("description"));
		netStatList.add(code);
		if(rs.getString("SiteRelationshipType")!=null){
		code=new Code();
		code.setPk(rs.getString("pkRelnType"));
		code.setType(rs.getString("SiteRelationshipType"));
		code.setCode(rs.getString("SiteRelationshipSubType"));
		code.setDescription(rs.getString("SiteRelationshipDesc"));
		siteRelnShipType.add(code);
		}
		else{
			siteRelnShipType.add(null);
		}
		siteNameList.add(rs.getString("site_name"));
		netLevelList.add(String.valueOf(rs.getInt("nw_level")));
		if(rs.getString("ctep_id")!=null){
			ctepList.add(rs.getString("ctep_id"));
		}else{
			ctepList.add("");
		}
	}
	
	String chlSql="";
	Site site=null;
	SiteIdentifier siteIdentifier=null;
	ObjectMap map=null;
	Site sitelvl=null;
	Sites sites=null;
	List<Site> sitesLvlList=null;
	List<Site> siteList=null;
	for(int i=0;i<pkNetList.size();i++){
		siteList=new ArrayList<Site>();
		site=new Site();
		siteIdentifier=new SiteIdentifier();
		map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
		siteIdentifier.setOID(map.getOID());
		siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
		site.setSiteIdentifier(siteIdentifier);
		site.setLevel(String.valueOf(netLevelList.get(i)));
		site.setName(siteNameList.get(i));
		if(!netInfoList.get(i).equals("-")){
			site.setDescription(netInfoList.get(i));
		}
		site.setSiteStatus(netStatList.get(i));
		if(!pkRelnShipType.get(i).equals("0") && siteRelnShipType.get(i)!=null){
		site.setRelationshipPK(pkRelnShipType.get(i));
		site.setSiteRelationShipType(siteRelnShipType.get(i));
		}
		if(!ctepList.get(i).equals("")){
			site.setCtepId(ctepList.get(i));
		}

		chlSql="select pk_nwsites, fk_nwsites, fk_site, site_name,ctep_id,nvl(site_info,'-') as site_info, er_codelst.pk_codelst as pkStat, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,(SELECT pk_codelst FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) as pkRelnType, (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, nw_level "+
				" FROM er_nwsites,er_site,er_codelst WHERE pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? ";
	
	if(!sitePk.equals("")){
		chlSql=chlSql+" and fk_site ="+sitePk;
	}
	
	if(!siteName.equals("")){
		chlSql=chlSql+" and site_name like '"+siteName+"'";
	}
	
	chlSql=chlSql+" START WITH pk_nwsites=? CONNECT BY prior pk_nwsites=FK_NWSITES ORDER SIBLINGS BY lower(site_name)";
	pstmt = conn.prepareStatement(chlSql);
	pstmt.setInt(1, StringUtil.stringToNum(iaccId));
	pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
	rs = pstmt.executeQuery();
	
	sitesLvlList=new ArrayList<Site>();
	while(rs.next()){		
		    sites=new Sites();
			code=new Code();
			siteIdentifier=new SiteIdentifier();
			map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
			siteIdentifier.setOID(map.getOID());
			siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
			code.setPk(rs.getString("pkStat"));
			code.setType(rs.getString("stat_type"));
			code.setCode(rs.getString("code"));
			code.setDescription(rs.getString("description"));
			sitelvl=new Site();
			sitelvl.setSiteIdentifier(siteIdentifier);
			sitelvl.setLevel(String.valueOf(rs.getInt("nw_level")));
			sitelvl.setName(rs.getString("site_name"));
			if(!rs.getString("site_info").equals("-")){
				sitelvl.setDescription(rs.getString("site_info"));
			}
			sitelvl.setSiteStatus(code);
			sitelvl.setCtepId(rs.getString("ctep_id"));
			if(rs.getString("SiteRelationshipType")!=null){
				code=new Code();
				code.setPk(rs.getString("pkRelnType"));
				code.setType(rs.getString("SiteRelationshipType"));
				code.setCode(rs.getString("SiteRelationshipSubType"));
				code.setDescription(rs.getString("SiteRelationshipDesc"));
				sitelvl.setSiteRelationShipType(code);
			}
			if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
				sitelvl.setRelationshipPK(rs.getString("pk_nwsites"));
			}
			sitesLvlList.add(sitelvl);
			sites.setSites(sitesLvlList);
			site.setSites(sites);
		}
	siteList.add(site);
	sitesList.add((ArrayList<Site>) siteList);
	}

}catch(OperationException ex){
	throw ex;
}catch (SQLException e) {
	e.printStackTrace();
}catch (Exception e) {
	e.printStackTrace();
} finally {
	try {
		if(rs!=null)
		rs.close();
		if(pstmt!=null)
		pstmt.close();
		if(conn!=null)
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
}


private String getRelationPksFromUser(NetworkUserDetails networkuser) {
	// TODO Auto-generated method stub
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	String relationPKs = "";
	try {
		conn = CommonDAO.getConnection();
		StringBuffer sqlbuff = new StringBuffer();
		System.out.println("After");
		sqlbuff.append("select distinct nvl(er_nwsites.FK_NWSITES_MAIN,pk_nwsites) as FK_NWSITES_MAIN,er_nwusers.FK_NWSITES as fk_nwsites from er_nwusers,ER_NWUSERS_ADDNLROLES,er_nwsites where er_nwusers.fk_nwsites is not null and er_nwsites.pk_nwsites=er_nwusers.FK_NWSITES");
		if(networkuser.getUserPK()!=null && !networkuser.getUserPK().equals("")){
			sqlbuff.append(" and fk_user="+networkuser.getUserPK());
		}
		if(networkuser.getUserLoginName()!=null && !networkuser.getUserLoginName().equals("")){
			sqlbuff.append(" and fk_user=(select pk_user from er_user where usr_logname='"+networkuser.getUserLoginName()+"')");
		}
		if(networkuser.getRolePK()!=null && !networkuser.getRolePK().equals("")){
			sqlbuff.append(" and (NWU_MEMBERTROLE="+networkuser.getRolePK()+" or NWU_MEMBERADDLTROLE="+networkuser.getRolePK()+")");
		}
		
		System.out.println("getRelationPksFromUser==="+sqlbuff.toString());
		pstmt = conn.prepareStatement(sqlbuff.toString());
		rs=pstmt.executeQuery();
		List<String> mainNetwork=new ArrayList<String>();
		while(rs.next()){
			if(!mainNetwork.contains(rs.getString("FK_NWSITES_MAIN"))){
				System.out.println("Incoming FK_NWSITES_MAIN=="+rs.getString("FK_NWSITES_MAIN"));
				mainNetwork.add(rs.getString("FK_NWSITES_MAIN"));
				if(relationPKs.equals("")){
					relationPKs=rs.getString("fk_nwsites");
				}else{
					relationPKs=relationPKs+","+rs.getString("fk_nwsites");
				}
				relationShipPkMap.put(rs.getString("FK_NWSITES_MAIN"), rs.getString("fk_nwsites"));
			}
		}
	}catch(Exception e){
		e.printStackTrace();
	}
	return relationPKs;
}
public String getNetPkMoreNetworkUsersDetails(List<MoreNetworkUserDetails> moredetails,String flag){
	
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	String key="";
	String value="";
	String net_pks="";
	String sql="";

	try{
		conn = CommonDAO.getConnection();
		for(NVPair nvpair:moredetails){
			key=nvpair.getKey();
			
			ElementNSImpl val=(ElementNSImpl)nvpair.getValue();
			if(val!=null){
			NodeList node= val.getChildNodes();
	        for(int j=0;j< node.getLength();j++)
	           {
	        	  Node node1=node.item(j);
	        	  value=node1.getNodeValue();
	           }
			}
			StringBuffer sqlbuf = new StringBuffer();
				 
					 sqlbuf.append("SELECT fk_modpk FROM ER_MOREDETAILS,ER_CODELST WHERE MD_MODELEMENTPK=PK_CODELST AND CODELST_SUBTYP=? and (codelst_type");
					 
					if(flag.equalsIgnoreCase("siteDetails")){
						 sqlbuf.append(" like 'org/__' escape '/' or codelst_type='org')");
					 }
					 if(flag.equalsIgnoreCase("networkUserDetail")){
						 sqlbuf.append(" like 'user/__' escape '/' or codelst_type='user')");
					 }
					 if(!value.equalsIgnoreCase("")){
					 sqlbuf.append(" AND  lower(MD_MODELEMENTDATA) LIKE lower('%"+value+"%')");
					 }
			System.out.println("Inside getNetPkMoreNetworkUsersDetails");
			System.out.println("sqlbuf==="+sqlbuf.toString());
			System.out.println("key==="+key);
	        pstmt = conn.prepareStatement(sqlbuf.toString());
			pstmt.setString(1,key);
			rs = pstmt.executeQuery();
			while(rs.next()){
				net_pks=net_pks+rs.getString("fk_modpk")+",";
			}	
		}
		net_pks=net_pks.length()>0?net_pks.substring(0,net_pks.length()-1):"0";
		}
		catch(SQLException ex){
			ex.printStackTrace();
		}
		finally{
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	
	return net_pks;
}

public int networkUsersRoles(String pkNetwork,String iaccId,NetworkSitesUsers site,HashMap<String, Object> params,String nUser_pks){
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	int userPkCount = 0;
	try {
		conn = CommonDAO.getConnection();

		StringBuffer sqlbuff = new StringBuffer();
		
		sqlbuff.append("SELECT pk_nwsites,pk_nwusers,fk_user,usr_logname,usr_firstname,usr_lastname,NWU_MEMBERTROLE,f_codelst_desc(NWU_MEMBERTROLE) AS role_name,c.CODELST_DESC as description,c.codelst_type as stat_type,c.codelst_subtyp as code,c.codelst_hide as stat_hidden FROM er_nwusers nwu, er_nwsites ns,er_site s,er_user u,er_codelst c WHERE nwu.FK_NWSITES=ns.PK_NWSITES and ns.PK_NWSITES=? AND  s.FK_ACCOUNT=? AND s.PK_SITE=ns.FK_SITE AND nwu.fk_user=u.pk_user AND c.pk_codelst=nwu.NWU_STATUS ");
		if(nUser_pks.length()>0){
			sqlbuff.append(" and pk_nwusers in ("+nUser_pks+") ");
		}
		
		System.out.println("sqlbuff==="+sqlbuff.toString());
		System.out.println("pkNetwork==="+pkNetwork);
		System.out.println("nUser_pks==="+nUser_pks);
		pstmt = conn.prepareStatement(sqlbuff.toString());
		pstmt.setInt(1, StringUtil.stringToNum(pkNetwork));
		pstmt.setInt(2, StringUtil.stringToNum(iaccId));
		rs=pstmt.executeQuery();
		
		UserRoleIdentifier user = null;
		UserRoleIdentifier user1 = new UserRoleIdentifier();
		UserIdentifier uid = null;
		NetworkRole roles=null;
		
		/*Roles role=new Roles();
		List<NetworkRole> rolelist=new ArrayList<NetworkRole>();
		List<UserIdentifier> userlist=new ArrayList<UserIdentifier>();*/
		List<UserRoleIdentifier> userrolelist=new ArrayList<UserRoleIdentifier>();
		Code code = null;
		int pk_nwusers=0;
		while(rs.next()){
			
			code = new Code();
			user = new UserRoleIdentifier();
			uid = new UserIdentifier();
			List<NetworkRole> ntwUsrRoles= new ArrayList<NetworkRole>();
			user.setLoginName(rs.getString("usr_logname"));
			user.setFirstName(rs.getString("usr_firstname"));
			user.setLastName(rs.getString("usr_lastname"));
			uid.setPK(rs.getInt("fk_user"));
			ObjectMap map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_USER, Integer.valueOf(uid.getPK()));
			uid.setOID(map.getOID());
			
			user.setUid(uid);
			
			if(rs.getString("NWU_MEMBERTROLE")!=null){
			    roles=new NetworkRole();
			    roles.setName(rs.getString("role_name"));
			    roles.setPK(rs.getString("NWU_MEMBERTROLE"));
			    code.setType(rs.getString("stat_type"));
				code.setCode(rs.getString("code"));
				code.setDescription(rs.getString("description"));
				
				roles.setRoleStatus(code);
				ntwUsrRoles.add(roles);
			}
			
			pk_nwusers=rs.getInt("pk_nwusers");
			networkUsersAdtnlRoles(pk_nwusers,ntwUsrRoles);
			user.setRole(ntwUsrRoles);
			userrolelist.add(user);		
			user1.setUser(userrolelist);
			site.setUserRoleIdentifier(user1);
			userPkCount++;
		}
			
		
	}catch(SQLException ex){
		ex.printStackTrace();
	}
	finally{
		try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	return userPkCount;
}

public int networkUsersRoles(String pkNetwork,String iaccId,UserSite site,HashMap<String, Object> params,String nUser_pks){
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	int userPkCount = 0;
	try {
		conn = CommonDAO.getConnection();

		StringBuffer sqlbuff = new StringBuffer();
		
		sqlbuff.append("SELECT pk_nwsites,pk_nwusers,fk_user,usr_logname,nwu.NWU_STATUS as nwu_status,usr_firstname,usr_lastname,(select codelst_type from er_codelst where pk_codelst=NWU_MEMBERTROLE) as roleType, (select codelst_subtyp from er_codelst where pk_codelst=NWU_MEMBERTROLE) as roleCode, NWU_MEMBERTROLE,f_codelst_desc(NWU_MEMBERTROLE) AS role_name,c.CODELST_DESC as description,c.codelst_type as stat_type,c.codelst_subtyp as code,c.codelst_hide as stat_hidden FROM er_nwusers nwu, er_nwsites ns,er_site s,er_user u,er_codelst c WHERE nwu.FK_NWSITES=ns.PK_NWSITES and ns.PK_NWSITES=? AND  s.FK_ACCOUNT=? AND s.PK_SITE=ns.FK_SITE AND nwu.fk_user=u.pk_user AND c.pk_codelst=nwu.NWU_STATUS ");
		if(!nUser_pks.equals("")){
			sqlbuff.append(" and fk_user="+nUser_pks);
		}
		
		System.out.println("sqlbuff==="+sqlbuff.toString());
		System.out.println("pkNetwork==="+pkNetwork);
		pstmt = conn.prepareStatement(sqlbuff.toString());
		pstmt.setInt(1, StringUtil.stringToNum(pkNetwork));
		pstmt.setInt(2, StringUtil.stringToNum(iaccId));
		rs=pstmt.executeQuery();
		
		UserRoleIdentifier user = null;
		UserRoleIdentifier user1 = new UserRoleIdentifier();
		UserIdentifier uid = null;
		Roles roles =null;
		NetworkRole role =null;
		
		/*Roles role=new Roles();
		List<NetworkRole> rolelist=new ArrayList<NetworkRole>();
		List<UserIdentifier> userlist=new ArrayList<UserIdentifier>();*/
		List<UserRoleIdentifier> userrolelist=new ArrayList<UserRoleIdentifier>();
		Code code = null;
		int pk_nwusers=0;
		while(rs.next()){
			
			code = new Code();
			user = new UserRoleIdentifier();
			uid = new UserIdentifier();
			List<NetworkRole> ntwUsrRoles= new ArrayList<NetworkRole>();
			user.setLoginName(rs.getString("usr_logname"));
			user.setFirstName(rs.getString("usr_firstname"));
			user.setLastName(rs.getString("usr_lastname"));
			user.setPK(String.valueOf(rs.getInt("fk_user")));
			/*ObjectMap map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_USER, Integer.valueOf(uid.getPK()));
			uid.setOID(map.getOID());
			
			user.setUid(uid);*/
			
			if(rs.getString("NWU_MEMBERTROLE")!=null){
			    
			    role= new NetworkRole();
			    role.setName(rs.getString("role_name"));
			    role.setPK(rs.getString("NWU_MEMBERTROLE"));
			    role.setRoleType(rs.getString("roleType"));
			    role.setRoleCode(rs.getString("roleCode"));
			    code.setType(rs.getString("stat_type"));
				code.setCode(rs.getString("code"));
				code.setDescription(rs.getString("description"));
				code.setPk(String.valueOf(rs.getInt("nwu_status")));
				role.setRoleStatus(code);
				ntwUsrRoles.add(role);
			}
			
			pk_nwusers=rs.getInt("pk_nwusers");
			networkUsersAdtnlRoles(pk_nwusers,ntwUsrRoles);
			if(ntwUsrRoles.size()>0){
				roles=new Roles();
				roles.setRoles(ntwUsrRoles);
				user.setRoles(roles);
			}
			userrolelist.add(user);		
			user1.setUser(userrolelist);
			site.setUsers(user1);
			userPkCount++;
		}
			
		
	}catch(SQLException ex){
		ex.printStackTrace();
	}
	finally{
		try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	return userPkCount;
}

public void networkUsersAdtnlRoles(int pkNWUsers){
	
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	NetworkServiceDao nwdao = null;
	try {
		conn = CommonDAO.getConnection();

		StringBuffer sqlbuff = new StringBuffer();
		
		sqlbuff.append("select f_codelst_desc(NWU_MEMBERADDLTROLE) name,(select codelst_type from er_codelst where pk_codelst=nwu_adtstatus) type, f_codelst_subtyp(nwu_adtstatus) code, f_codelst_desc(nwu_adtstatus) description from ER_NWUSERS_ADDNLROLES where FK_NWUSERS=?  ");
		
		pstmt = conn.prepareStatement(sqlbuff.toString());
		pstmt.setInt(1, pkNWUsers);
		rs=pstmt.executeQuery();
		
		while(rs.next()){
			nwdao.setRoleName(rs.getString("role_name"));
			nwdao.setRoleType(rs.getString("stat_type"));
			nwdao.setRoleCode(rs.getString("code"));
			nwdao.setRoleDescription(rs.getString("description"));		
		}
			
		
	}catch(SQLException ex){
		ex.printStackTrace();
	}
	finally{
		try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
}

public void networkUsersAdtnlRoles(int pkNWUsers,List<NetworkRole> ntwUsrRoles){
	
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	NetworkRole roles=null;
	Code code = new Code();
	try {
		conn = CommonDAO.getConnection();

		StringBuffer sqlbuff = new StringBuffer();
		
		sqlbuff.append("select NWU_MEMBERADDLTROLE,f_codelst_desc(NWU_MEMBERADDLTROLE) name,(select codelst_type from er_codelst where pk_codelst=nwu_adtstatus) type, f_codelst_subtyp(nwu_adtstatus) code, f_codelst_desc(nwu_adtstatus) description, nwu_adtstatus, (select codelst_type from er_codelst where pk_codelst=NWU_MEMBERADDLTROLE) as roleType, (select codelst_subtyp from er_codelst where pk_codelst=NWU_MEMBERADDLTROLE) as roleCode from ER_NWUSERS_ADDNLROLES where FK_NWUSERS=?  ");
		
		pstmt = conn.prepareStatement(sqlbuff.toString());
		pstmt.setInt(1, pkNWUsers);
		rs=pstmt.executeQuery();
		
		while(rs.next()){
			if(rs.getString("NWU_MEMBERADDLTROLE")!=null){
			code = new Code();
			roles=new NetworkRole();
			roles.setName(rs.getString("name"));
			roles.setPK(rs.getString("NWU_MEMBERADDLTROLE"));
			roles.setRoleType(rs.getString("roleType"));
		    roles.setRoleCode(rs.getString("roleCode"));
			code.setCode(rs.getString("code"));
			code.setType(rs.getString("type"));
			code.setDescription(rs.getString("description"));
			code.setPk(String.valueOf(rs.getInt("nwu_adtstatus")));
			roles.setRoleStatus(code);
			ntwUsrRoles.add(roles);
			}
		}
			
		
	}catch(SQLException ex){
		ex.printStackTrace();
	}
	finally{
		try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
}

public void getNetworkSiteParent(SiteLevelDetails siteLevelDetails, Map<String, Object> parameters) throws OperationException{
	// TODO Auto-generated method stub
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	int count=0;
	String str = "";
	String pk_main_network = "";
	String networkPK = (siteLevelDetails.getNetworkPK()!=null)?siteLevelDetails.getNetworkPK():"";
	String networkName = (siteLevelDetails.getNetworkName()!=null)?siteLevelDetails.getNetworkName():"";
	String sitePK = (siteLevelDetails.getSitePK()!=null)?siteLevelDetails.getSitePK():"";
	String siteName = (siteLevelDetails.getSiteName()!=null)?siteLevelDetails.getSiteName():"";
	String level = (siteLevelDetails.getLevel()!=null)?siteLevelDetails.getLevel():"";
	String relationshipPK = (siteLevelDetails.getRelationshipPK()!=null)?siteLevelDetails.getRelationshipPK():"";
	String iaccId = parameters.get(KEY_ACCOUNT_ID).toString();
	if(!relationshipPK.equals("")){
		String getPrimaryNetwork="select nvl(fk_nwsites_main,0) as fk_nwsites_main,nw_level FROM er_nwsites WHERE pk_nwsites="+ EJBUtil.stringToNum(relationshipPK);
		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(getPrimaryNetwork);
			rs=pstmt.executeQuery();
			while(rs.next()){
				pk_main_network=String.valueOf(rs.getInt("fk_nwsites_main"));
				level=String.valueOf(rs.getInt("nw_level"));
			}
			System.out.println("new condition pk_main_network==="+pk_main_network);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	if(level.equals("0")){
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Parent site details not applicable for root network site")); 
		throw new OperationException();
	}
	if(pk_main_network.equals("") && !networkPK.equals("")){
		pk_main_network = networkPK;
	}
	System.out.println("final pk_main_network==="+pk_main_network);
	
	 	String str1 = "select pk_nwsites,fk_site,site_name ,site_info,site_info as network_info,nw_status,site_name as network_name,ctep_id, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,er_nwsites.NW_MEMBERTYPE as member_type,(SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,nw_level from er_nwsites,er_site,er_codelst "+
	 				  " where pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? and nw_level=0 ";
	 	if(!(networkName.equals(""))){
	 			 	 str1=str1+" and lower(site_name) = lower('"+networkName+"') ";
	 		}
	 	//if(networkName.equals("") && pk_main_network.length()>0)
	 	if(pk_main_network.length()>0)
	 	{
	 		str1=str1+" and pk_nwsites in("+pk_main_network+") ";
	 	}
	 	System.out.println("str1==="+str1);
try {
	conn = CommonDAO.getConnection();
	pstmt = conn.prepareStatement(str1);
	pstmt.setInt(1, StringUtil.stringToNum(iaccId));
	rs = pstmt.executeQuery();
	Code code=null;
	while (rs.next()) {
		code=new Code();
		pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
		fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
		if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
			pkRelnShipType.add(rs.getString("pk_nwsites"));
		}
		else{
			pkRelnShipType.add("0");
		}
		netNameList.add(rs.getString("network_name"));
		netInfoList.add(rs.getString("network_info"));
		code.setPk(String.valueOf(rs.getInt("nw_status")));
		code.setType(rs.getString("stat_type"));
		code.setCode(rs.getString("code"));
		code.setDescription(rs.getString("description"));
		netStatList.add(code);
		if(rs.getString("SiteRelationshipType")!=null){
		code=new Code();
		code.setPk(String.valueOf(rs.getInt("member_type")));
		code.setType(rs.getString("SiteRelationshipType"));
		code.setCode(rs.getString("SiteRelationshipSubType"));
		code.setDescription(rs.getString("SiteRelationshipDesc"));
		siteRelnShipType.add(code);
		}
		else{
			siteRelnShipType.add(null);
		}
		siteNameList.add(rs.getString("site_name"));
		siteInfoList.add(rs.getString("site_info"));
		netLevelList.add(String.valueOf(rs.getInt("nw_level")));
		if(rs.getString("ctep_id")!=null){
			ctepList.add(rs.getString("ctep_id"));
		}else{
			ctepList.add("");
		}
		count++;
	}
	if(count==0)
	{
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found")); 
		throw new OperationException();
	}
	String chlSql="";
	Site site=null;
	SiteIdentifier siteIdentifier=null;
	ObjectMap map=null;
	Site sitelvl1=null;
	Site sitelvl2=null;
	Site sitelvl3=null;
	Site sitelvl4=null;
	Site sitelvl5=null;
	Site sitelvl6=null;
	UserBean userBean=(UserBean)parameters.get("callingUser");
	List<Site> siteslvl1=null;
	List<Site> siteslvl2=null;
	List<Site> siteslvl3=null;
	List<Site> siteslvl4=null;
	List<Site> siteslvl5=null;
	List<Site> siteslvl6=null;
	Sites psiteslvl0=null;
	Sites psiteslvl1=null;
	Sites psiteslvl2=null;
	Sites psiteslvl3=null;
	Sites psiteslvl4=null;
	Sites psiteslvl5=null;
	MoreDetailsJB mdJB=null;
	MoreDetailsDao mdDao;
	ArrayList idList = new ArrayList();
	ArrayList modElementDescList = new ArrayList();
	ArrayList modElementDataList = new ArrayList();
	ArrayList modElementKeysList = new ArrayList();
	List<NVPair> listNVPair=null;
	NVPair nvpair = null;
	List<Site> siteList=null;
	String net_pks = "";
	for(int i=0;i<pkNetList.size();i++){
		
		//siteList.add(site);
		
		//chlSql="select pk_nwsites,fk_site,lower(site_name) site_name,er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,nw_memberType,(SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, nw_level+1 as nw_level from er_nwsites,er_site,er_codelst where pk_site=fk_site and pk_codelst=nw_status "+
		//	" and er_site.fk_account=? and FK_NWSITES_MAIN=? ";
		String pks_childNtw="";
		
		//chlSql="select pk_nwsites,site_name from er_nwsites,er_site,er_codelst where pk_site=fk_site and pk_codelst=nw_status "+
		//		" and er_site.fk_account=? and FK_NWSITES_MAIN=? ";
		
		
		List<String> relationshipPKList = new ArrayList<String>();
		if(relationshipPK.equals("")){
		String getRelationshipPk = "select pk_nwsites from er_nwsites,er_site where (fk_nwsites_main="+StringUtil.stringToNum(pkNetList.get(i))+" or fk_nwsites_main is null) and pk_site=fk_site";
		if(!siteName.equals("")){
			getRelationshipPk=getRelationshipPk+ " and lower(site_name) = lower('"+siteName+"')";
		}
		if(!sitePK.equals("")){
			getRelationshipPk=getRelationshipPk+ " and fk_site = "+StringUtil.stringToNum(sitePK);
		}
		if(!level.equals("")){
			getRelationshipPk=getRelationshipPk+ " and nw_level = "+(StringUtil.stringToNum(level));
		}
		getRelationshipPk=getRelationshipPk+" order by pk_nwsites";
		try {
			pstmt = conn.prepareStatement(getRelationshipPk);
			rs=pstmt.executeQuery();
			while(rs.next()){
				if(rs.getString("pk_nwsites")!=null){
					relationshipPKList.add(String.valueOf(rs.getInt("pk_nwsites")));
				}
				
			}
			System.out.println("getRelationshipPk==="+getRelationshipPk);
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}else{
		relationshipPKList.add(relationshipPK);
	}
	String immediateParentPKs = "";
	
	if(relationshipPKList.size()>0){
		
		siteList=new ArrayList<Site>();
		
		for(String relationPK: relationshipPKList){
			siteslvl1=new ArrayList<Site>();
			siteslvl2=new ArrayList<Site>();
			siteslvl3=new ArrayList<Site>();
			siteslvl4=new ArrayList<Site>();
			siteslvl5=new ArrayList<Site>();
			siteslvl6=new ArrayList<Site>();
			psiteslvl0=new Sites();
			psiteslvl1=new Sites();
			psiteslvl2=new Sites();
			psiteslvl3=new Sites();
			psiteslvl4=new Sites();
			psiteslvl5=new Sites();
			
			listNVPair=new ArrayList<NVPair>();
			site=new Site();
			siteIdentifier=new SiteIdentifier();
			map = ((ObjectMapService)parameters.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
			siteIdentifier.setOID(map.getOID());
			siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
			site.setSiteIdentifier(siteIdentifier);
			site.setLevel(String.valueOf(netLevelList.get(i)));
			site.setName(siteNameList.get(i));
			site.setDescription(siteInfoList.get(i));
			site.setSiteStatus(netStatList.get(i));
			if(!pkRelnShipType.get(i).equals("0") && siteRelnShipType.get(i)!=null){
			site.setRelationshipPK(pkRelnShipType.get(i));
			site.setSiteRelationShipType(siteRelnShipType.get(i));
			
			}
			if(!ctepList.get(i).equals("")){
				site.setCtepId(ctepList.get(i));
			}
			//if(moreSiteDetails==null){
			mdJB = new MoreDetailsJB();
			mdDao = new MoreDetailsDao();
			mdDao = mdJB.getMoreDetails(EJBUtil.stringToNum(pkNetList.get(i)),"org_"+EJBUtil.stringToNum(netLevelList.get(i)),userBean.getUserGrpDefault());
			idList = mdDao.getId();
			modElementDescList = mdDao.getMdElementDescs();
			modElementDataList = mdDao.getMdElementValues();
			modElementKeysList = mdDao.getMdElementKeys();
			for(int iY=0; iY<idList.size(); iY++){
				if((Integer)idList.get(iY)>0){
				nvpair = new NVPair();
				nvpair.setKey((String) modElementKeysList.get(iY));
				nvpair.setValue((String)modElementDataList.get(iY));
				listNVPair.add(nvpair);
				}
			}
			if(net_pks.equals("")){
				net_pks= pkNetList.get(i).toString();
			}
			//}
			/*else{		
					listNVPair=new UserServiceImpl().getNetworkMoreDetailValue(pkNetList.get(i),keySearch,(EJBUtil.stringToNum(netLevelList.get(i))-1),iaccId);
				}*/
			
			site.setMoreSiteDetailsFields(listNVPair);
		String getImmediateParentPKs = "select rowtocol('select nvl(fk_nwsites,pk_nwsites) from er_nwsites where (fk_nwsites_main="+StringUtil.stringToNum(pkNetList.get(i))+" or fk_nwsites_main is null) and pk_nwsites in ("+relationPK+") order by fk_nwsites') as fk_nwsites from dual";
		try {
			pstmt = conn.prepareStatement(getImmediateParentPKs);
			rs=pstmt.executeQuery();
			while(rs.next()){
				immediateParentPKs=rs.getString("fk_nwsites");
			}
			System.out.println("getimmediateParentPKs==="+getImmediateParentPKs);
			System.out.println("immediateParentPKs==="+immediateParentPKs);
		}catch(Exception e){
			e.printStackTrace();
		}
	
			if(!immediateParentPKs.equals("")){
				pks_childNtw = relationPK + "," + immediateParentPKs;
			}else{
				pks_childNtw = relationPK;
			}
			System.out.println("pks_childNtw==="+pks_childNtw);
			if(!pks_childNtw.equals("0") && !pks_childNtw.equals(""))
			{
			chlSql="SELECT distinct pk_nwsites,site_name,site_info,fk_site,ctep_id,er_codelst.CODELST_DESC AS description,er_codelst.codelst_type AS stat_type,er_codelst.codelst_subtyp AS code,er_codelst.codelst_hide AS stat_hidden, "
					+" nw_status, er_nwsites.NW_MEMBERTYPE as member_type, (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
					+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, "
					+" nw_level FROM er_nwsites,er_site,er_codelst where fk_site=pk_site AND pk_codelst=nw_status AND er_site.fk_account=? and pk_nwsites <> ? "
					+" and pk_nwsites in ("+ pks_childNtw +") ORDER BY nw_level";
			System.out.println("chlSql==="+chlSql);
			System.out.println("iaccId==="+iaccId);
			System.out.println("pkNetList.get(i)==="+pkNetList.get(i));
			pstmt = conn.prepareStatement(chlSql);
			pstmt.setInt(1, StringUtil.stringToNum(iaccId));
			pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
			rs = pstmt.executeQuery();
			
			
			while (rs.next()) {
				
				code=new Code();
				listNVPair=new ArrayList<NVPair>();
				siteIdentifier=new SiteIdentifier();
				map = ((ObjectMapService)parameters.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
				siteIdentifier.setOID(map.getOID());
				siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
				code.setPk(String.valueOf(rs.getInt("nw_status")));
				code.setType(rs.getString("stat_type"));
				code.setCode(rs.getString("code"));
				code.setDescription(rs.getString("description"));
				mdJB = new MoreDetailsJB();
				mdDao = new MoreDetailsDao();
				mdDao = mdJB.getMoreDetails(rs.getInt("pk_nwsites"),"org_"+rs.getInt("nw_level"),userBean.getUserGrpDefault());
				idList = mdDao.getId();
				modElementDescList = mdDao.getMdElementDescs();
				modElementDataList = mdDao.getMdElementValues();
				modElementKeysList = mdDao.getMdElementKeys();
				for(int iY=0; iY<idList.size(); iY++){
					if((Integer)idList.get(iY)>0){
					nvpair = new NVPair();
					nvpair.setKey((String) modElementKeysList.get(iY));
					nvpair.setValue((String)modElementDataList.get(iY));
					listNVPair.add(nvpair);
					}
				}
				
				if(rs.getInt("nw_level")==1){
					sitelvl1=new Site();
					sitelvl1.setSiteIdentifier(siteIdentifier);
					sitelvl1.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl1.setName(rs.getString("site_name"));
					sitelvl1.setDescription(rs.getString("site_info"));
					sitelvl1.setSiteStatus(code);
					sitelvl1.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl1.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl1.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl1.setMoreSiteDetailsFields(listNVPair);
					siteslvl1.add(sitelvl1);
					//siteslvl2=new ArrayList<Site>();
					psiteslvl0.setSites(siteslvl1);
					site.setSites(psiteslvl0);
				}
				else if(rs.getInt("nw_level")==2){
					sitelvl2=new Site();
					sitelvl2.setSiteIdentifier(siteIdentifier);
					sitelvl2.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl2.setName(rs.getString("site_name"));
					sitelvl2.setDescription(rs.getString("site_info"));
					System.out.println("level 2 description==="+sitelvl2.getDescription());
					sitelvl2.setSiteStatus(code);
					sitelvl2.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl2.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl2.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl2.setMoreSiteDetailsFields(listNVPair);
					siteslvl2.add(sitelvl2);
					//siteslvl3=new ArrayList<Site>();
					if(sitelvl1!=null){
						psiteslvl1.setSites(siteslvl2);
						sitelvl1.setSites(psiteslvl1);
					}else{
						psiteslvl0.setSites(siteslvl2);
						site.setSites(psiteslvl0);
					}
					
					
				}
				else if(rs.getInt("nw_level")==3){
					sitelvl3=new Site();
					sitelvl3.setSiteIdentifier(siteIdentifier);
					sitelvl3.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl3.setName(rs.getString("site_name"));
					sitelvl3.setDescription(rs.getString("site_info"));
					System.out.println("level 3 description==="+sitelvl3.getDescription());
					sitelvl3.setSiteStatus(code);
					sitelvl3.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl3.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl3.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl3.setMoreSiteDetailsFields(listNVPair);
					siteslvl3.add(sitelvl3);
					//siteslvl4=new ArrayList<Site>();
					if(sitelvl2!=null){
						psiteslvl2.setSites(siteslvl3);
						sitelvl2.setSites(psiteslvl2);
					}else{
						psiteslvl0.setSites(siteslvl3);
						site.setSites(psiteslvl0);
					}
				}
				else if(rs.getInt("nw_level")==4){
					sitelvl4=new Site();
					sitelvl4.setSiteIdentifier(siteIdentifier);
					sitelvl4.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl4.setName(rs.getString("site_name"));
					sitelvl4.setDescription(rs.getString("site_info"));
					sitelvl4.setSiteStatus(code);
					sitelvl4.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl4.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl4.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl4.setMoreSiteDetailsFields(listNVPair);
					siteslvl4.add(sitelvl4);
					//siteslvl5=new ArrayList<Site>();
					if(sitelvl3!=null){
						psiteslvl3.setSites(siteslvl4);
						sitelvl3.setSites(psiteslvl3);
					}else{
						psiteslvl0.setSites(siteslvl4);
						site.setSites(psiteslvl0);
					}
					
				}
				else if(rs.getInt("nw_level")==5){
					sitelvl5=new Site();
					sitelvl5.setSiteIdentifier(siteIdentifier);
					sitelvl5.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl5.setName(rs.getString("site_name"));
					sitelvl5.setDescription(rs.getString("site_info"));
					sitelvl5.setSiteStatus(code);
					sitelvl5.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl5.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl5.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl5.setMoreSiteDetailsFields(listNVPair);
					siteslvl5.add(sitelvl5);
					//siteslvl6=new ArrayList<Site>();
					if(sitelvl4!=null){
						psiteslvl4.setSites(siteslvl5);
						sitelvl4.setSites(psiteslvl4);
					}else{
						psiteslvl0.setSites(siteslvl5);
						site.setSites(psiteslvl0);
					}
				}
				else if(rs.getInt("nw_level")==6){
					sitelvl6=new Site();
					siteIdentifier=new SiteIdentifier();
					sitelvl6.setSiteIdentifier(siteIdentifier);
					sitelvl6.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl6.setName(rs.getString("site_name"));
					sitelvl6.setDescription(rs.getString("site_info"));
					sitelvl6.setSiteStatus(code);
					sitelvl6.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl6.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl6.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl6.setMoreSiteDetailsFields(listNVPair);
					siteslvl6.add(sitelvl6);
					if(sitelvl5!=null){
						psiteslvl5.setSites(siteslvl6);
						sitelvl5.setSites(psiteslvl5);
					}else{
						psiteslvl0.setSites(siteslvl6);
						site.setSites(psiteslvl0);
					}
				}
				}
			}
			siteList.add(site);
		}
		sitesList.add((ArrayList<Site>) siteList);
		}
	
	}
}catch(OperationException ex){
	throw ex;
}catch(SQLException ex){
	ex.printStackTrace();
}catch (Exception e) {
	e.printStackTrace();
} finally {
	try {
		rs.close();
		pstmt.close();
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
}
public void getNetworkSiteChildren(SiteLevelDetails siteLevelDetails, Map<String, Object> parameters) throws OperationException{
	// TODO Auto-generated method stub
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	int count=0;
	String str = "";
	String pk_main_network = "";
	String networkPK = (siteLevelDetails.getNetworkPK()!=null)?siteLevelDetails.getNetworkPK():"";
	String networkName = (siteLevelDetails.getNetworkName()!=null)?siteLevelDetails.getNetworkName():"";
	String sitePK = (siteLevelDetails.getSitePK()!=null)?siteLevelDetails.getSitePK():"";
	String siteName = (siteLevelDetails.getSiteName()!=null)?siteLevelDetails.getSiteName():"";
	String level = (siteLevelDetails.getLevel()!=null)?siteLevelDetails.getLevel():"";
	String relationshipPK = (siteLevelDetails.getRelationshipPK()!=null)?siteLevelDetails.getRelationshipPK():"";
	String iaccId = parameters.get(KEY_ACCOUNT_ID).toString();
	if(!relationshipPK.equals("")){
		String getPrimaryNetwork="select fk_nwsites_main,nw_level FROM er_nwsites WHERE pk_nwsites="+ EJBUtil.stringToNum(relationshipPK);
		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(getPrimaryNetwork);
			rs=pstmt.executeQuery();
			while(rs.next()){
				pk_main_network=String.valueOf(rs.getInt("fk_nwsites_main"));
				level=String.valueOf(rs.getInt("nw_level"));
			}
			System.out.println("new condition pk_main_network==="+pk_main_network);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	if(pk_main_network.equals("") && !networkPK.equals("")){
		pk_main_network = networkPK;
	}
	System.out.println("final pk_main_network==="+pk_main_network);
	
	 	String str1 = "select pk_nwsites,fk_site,site_name ,site_info, site_info as network_info,nw_status,site_name as network_name,ctep_id, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,er_nwsites.NW_MEMBERTYPE as member_type,(SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,nw_level from er_nwsites,er_site,er_codelst "+
	 				  " where pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? and nw_level=0 ";
	 	if(!(networkName.equals(""))){
	 			 	 str1=str1+" and lower(site_name) = lower('"+networkName+"') ";
	 		}
	 	//if(networkName.equals("") && pk_main_network.length()>0)
	 	if(pk_main_network.length()>0)
	 	{
	 		str1=str1+" and pk_nwsites in("+pk_main_network+") ";
	 	}
	 	System.out.println("str1==="+str1);
try {
	conn = CommonDAO.getConnection();
	pstmt = conn.prepareStatement(str1);
	pstmt.setInt(1, StringUtil.stringToNum(iaccId));
	rs = pstmt.executeQuery();
	Code code=null;
	while (rs.next()) {
		code=new Code();
		pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
		fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
		if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
			pkRelnShipType.add(rs.getString("pk_nwsites"));
		}
		else{
			pkRelnShipType.add("0");
		}
		netNameList.add(rs.getString("network_name"));
		netInfoList.add(rs.getString("network_info"));
		code.setPk(String.valueOf(rs.getInt("nw_status")));
		code.setType(rs.getString("stat_type"));
		code.setCode(rs.getString("code"));
		code.setDescription(rs.getString("description"));
		netStatList.add(code);
		if(rs.getString("SiteRelationshipType")!=null){
		code=new Code();
		code.setPk(String.valueOf(rs.getInt("member_type")));
		code.setType(rs.getString("SiteRelationshipType"));
		code.setCode(rs.getString("SiteRelationshipSubType"));
		code.setDescription(rs.getString("SiteRelationshipDesc"));
		siteRelnShipType.add(code);
		}
		else{
			siteRelnShipType.add(null);
		}
		siteNameList.add(rs.getString("site_name"));
		siteInfoList.add(rs.getString("site_info"));
		netLevelList.add(String.valueOf(rs.getInt("nw_level")));
		if(rs.getString("ctep_id")!=null){
			ctepList.add(rs.getString("ctep_id"));
		}else{
			ctepList.add("");
		}
		count++;
	}
	if(count==0)
	{
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found")); 
		throw new OperationException();
	}
	String chlSql="";
	Site site=null;
	SiteIdentifier siteIdentifier=null;
	ObjectMap map=null;
	Site sitelvl1=null;
	Site sitelvl2=null;
	Site sitelvl3=null;
	Site sitelvl4=null;
	Site sitelvl5=null;
	Site sitelvl6=null;
	UserBean userBean=(UserBean)parameters.get("callingUser");
	List<Site> siteslvl1=null;
	List<Site> siteslvl2=null;
	List<Site> siteslvl3=null;
	List<Site> siteslvl4=null;
	List<Site> siteslvl5=null;
	List<Site> siteslvl6=null;
	Sites psiteslvl0=null;
	Sites psiteslvl1=null;
	Sites psiteslvl2=null;
	Sites psiteslvl3=null;
	Sites psiteslvl4=null;
	Sites psiteslvl5=null;
	MoreDetailsJB mdJB=null;
	MoreDetailsDao mdDao;
	ArrayList idList = new ArrayList();
	ArrayList modElementDescList = new ArrayList();
	ArrayList modElementDataList = new ArrayList();
	ArrayList modElementKeysList = new ArrayList();
	List<NVPair> listNVPair=null;
	NVPair nvpair = null;
	List<Site> siteList=null;
	String net_pks = "";
	for(int i=0;i<pkNetList.size();i++){
		
		//siteList.add(site);
		
		//chlSql="select pk_nwsites,fk_site,lower(site_name) site_name,er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,nw_memberType,(SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, nw_level+1 as nw_level from er_nwsites,er_site,er_codelst where pk_site=fk_site and pk_codelst=nw_status "+
		//	" and er_site.fk_account=? and FK_NWSITES_MAIN=? ";
		String pks_childNtw="";
		
		//chlSql="select pk_nwsites,site_name from er_nwsites,er_site,er_codelst where pk_site=fk_site and pk_codelst=nw_status "+
		//		" and er_site.fk_account=? and FK_NWSITES_MAIN=? ";
		
	List<String> relationshipPKList = new ArrayList<String>();
		if(relationshipPK.equals("")){
		String getRelationshipPk = "select pk_nwsites from er_nwsites,er_site where (fk_nwsites_main="+StringUtil.stringToNum(pkNetList.get(i))+" or fk_nwsites_main is null) and pk_site=fk_site";
		if(!siteName.equals("")){
			getRelationshipPk=getRelationshipPk+ " and lower(site_name) = lower('"+siteName+"')";
		}
		if(!sitePK.equals("")){
			getRelationshipPk=getRelationshipPk+ " and fk_site = "+StringUtil.stringToNum(sitePK);
		}
		if(!level.equals("")){
			getRelationshipPk=getRelationshipPk+ " and nw_level = "+(StringUtil.stringToNum(level));
		}
		getRelationshipPk=getRelationshipPk+ " order by pk_nwsites";
		try {
			pstmt = conn.prepareStatement(getRelationshipPk);
			rs=pstmt.executeQuery();
			while(rs.next()){
				relationshipPKList.add(String.valueOf(rs.getInt("pk_nwsites")));
			}
			System.out.println("getRelationshipPk==="+getRelationshipPk);
			System.out.println("relationshipPK size==="+relationshipPKList.size());
		}catch(Exception e){
			e.printStackTrace();
		}
	}else{
		relationshipPKList.add(relationshipPK);
	}
		
	if(relationshipPKList.size()>0){
		siteList=new ArrayList<Site>();
		
		for(String relationPK: relationshipPKList){
			siteslvl1=new ArrayList<Site>();
			siteslvl2=new ArrayList<Site>();
			siteslvl3=new ArrayList<Site>();
			siteslvl4=new ArrayList<Site>();
			siteslvl5=new ArrayList<Site>();
			siteslvl6=new ArrayList<Site>();
			psiteslvl0=new Sites();
			psiteslvl1=new Sites();
			psiteslvl2=new Sites();
			psiteslvl3=new Sites();
			psiteslvl4=new Sites();
			psiteslvl5=new Sites();
			
			listNVPair=new ArrayList<NVPair>();
			site=new Site();
			siteIdentifier=new SiteIdentifier();
			map = ((ObjectMapService)parameters.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
			siteIdentifier.setOID(map.getOID());
			siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
			site.setSiteIdentifier(siteIdentifier);
			site.setLevel(String.valueOf(netLevelList.get(i)));
			site.setName(siteNameList.get(i));
			site.setDescription(siteInfoList.get(i));
			site.setSiteStatus(netStatList.get(i));
			if(!pkRelnShipType.get(i).equals("0") && siteRelnShipType.get(i)!=null){
			site.setRelationshipPK(pkRelnShipType.get(i));
			site.setSiteRelationShipType(siteRelnShipType.get(i));
			
			}
			if(!ctepList.get(i).equals("")){
				site.setCtepId(ctepList.get(i));
			}
			//if(moreSiteDetails==null){
			mdJB = new MoreDetailsJB();
			mdDao = new MoreDetailsDao();
			mdDao = mdJB.getMoreDetails(EJBUtil.stringToNum(pkNetList.get(i)),"org_"+EJBUtil.stringToNum(netLevelList.get(i)),userBean.getUserGrpDefault());
			idList = mdDao.getId();
			modElementDescList = mdDao.getMdElementDescs();
			modElementDataList = mdDao.getMdElementValues();
			modElementKeysList = mdDao.getMdElementKeys();
			for(int iY=0; iY<idList.size(); iY++){
				if((Integer)idList.get(iY)>0){
				nvpair = new NVPair();
				nvpair.setKey((String) modElementKeysList.get(iY));
				nvpair.setValue((String)modElementDataList.get(iY));
				listNVPair.add(nvpair);
				}
			}
			if(net_pks.equals("")){
				net_pks= pkNetList.get(i).toString();
			}
			//}
			/*else{		
					listNVPair=new UserServiceImpl().getNetworkMoreDetailValue(pkNetList.get(i),keySearch,(EJBUtil.stringToNum(netLevelList.get(i))-1),iaccId);
				}*/
			
			site.setMoreSiteDetailsFields(listNVPair);

			
			String immediateChildPKs = "";	
			String getImmediateChildPKs = "select rowtocol('select pk_nwsites from er_nwsites where fk_nwsites_main="+StringUtil.stringToNum(pkNetList.get(i))+" and fk_nwsites in ("+relationPK+") order by pk_nwsites') as pk_nwsites from dual";
				try {
					pstmt = conn.prepareStatement(getImmediateChildPKs);
					rs=pstmt.executeQuery();
					while(rs.next()){
						if(rs.getString("pk_nwsites")!=null){
							immediateChildPKs=rs.getString("pk_nwsites");
						}
					}
					System.out.println("getimmediateChildPKs==="+getImmediateChildPKs);
					System.out.println("immediateChildPKs==="+immediateChildPKs);
				}catch(Exception e){
					e.printStackTrace();
				}
			
					if(!immediateChildPKs.equals("")){
						pks_childNtw = relationPK + "," + immediateChildPKs;
					}else{
						pks_childNtw = relationPK;
					}
					System.out.println("pks_childNtw==="+pks_childNtw);
					if(!pks_childNtw.equals("0") && !pks_childNtw.equals(""))
					{
					chlSql="SELECT distinct pk_nwsites,site_name,site_info,fk_site,ctep_id,er_codelst.CODELST_DESC AS description,er_codelst.codelst_type AS stat_type,er_codelst.codelst_subtyp AS code,er_codelst.codelst_hide AS stat_hidden, "
							+" nw_status, er_nwsites.NW_MEMBERTYPE as member_type, (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
							+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, "
							+" nw_level FROM er_nwsites,er_site,er_codelst where fk_site=pk_site AND pk_codelst=nw_status AND er_site.fk_account=? and pk_nwsites <> ? "
							+" and pk_nwsites in ("+ pks_childNtw +") ORDER BY nw_level";
					System.out.println("chlSql==="+chlSql);
					System.out.println("iaccId==="+iaccId);
					System.out.println("pkNetList.get(i)==="+pkNetList.get(i));
					pstmt = conn.prepareStatement(chlSql);
					pstmt.setInt(1, StringUtil.stringToNum(iaccId));
					pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
					rs = pstmt.executeQuery();
					
					
					while (rs.next()) {
						
						code=new Code();
						listNVPair=new ArrayList<NVPair>();
						siteIdentifier=new SiteIdentifier();
						map = ((ObjectMapService)parameters.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
						siteIdentifier.setOID(map.getOID());
						siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
						code.setPk(String.valueOf(rs.getInt("nw_status")));
						code.setType(rs.getString("stat_type"));
						code.setCode(rs.getString("code"));
						code.setDescription(rs.getString("description"));
						mdJB = new MoreDetailsJB();
						mdDao = new MoreDetailsDao();
						mdDao = mdJB.getMoreDetails(rs.getInt("pk_nwsites"),"org_"+rs.getInt("nw_level"),userBean.getUserGrpDefault());
						idList = mdDao.getId();
						modElementDescList = mdDao.getMdElementDescs();
						modElementDataList = mdDao.getMdElementValues();
						modElementKeysList = mdDao.getMdElementKeys();
						for(int iY=0; iY<idList.size(); iY++){
							if((Integer)idList.get(iY)>0){
							nvpair = new NVPair();
							nvpair.setKey((String) modElementKeysList.get(iY));
							nvpair.setValue((String)modElementDataList.get(iY));
							listNVPair.add(nvpair);
							}
						}
						
						if(rs.getInt("nw_level")==1){
							sitelvl1=new Site();
							sitelvl1.setSiteIdentifier(siteIdentifier);
							sitelvl1.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl1.setName(rs.getString("site_name"));
							sitelvl1.setDescription(rs.getString("site_info"));
							sitelvl1.setSiteStatus(code);
							sitelvl1.setCtepId(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setPk(rs.getString("member_type"));
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl1.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl1.setRelationshipPK(rs.getString("pk_nwsites"));
							}
							sitelvl1.setMoreSiteDetailsFields(listNVPair);
							siteslvl1.add(sitelvl1);
							//siteslvl2=new ArrayList<Site>();
							psiteslvl0.setSites(siteslvl1);
							site.setSites(psiteslvl0);
						}
						else if(rs.getInt("nw_level")==2){
							sitelvl2=new Site();
							sitelvl2.setSiteIdentifier(siteIdentifier);
							sitelvl2.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl2.setName(rs.getString("site_name"));
							sitelvl2.setDescription(rs.getString("site_info"));
							System.out.println("level 2 description==="+sitelvl2.getDescription());
							sitelvl2.setSiteStatus(code);
							sitelvl2.setCtepId(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setPk(rs.getString("member_type"));
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl2.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl2.setRelationshipPK(rs.getString("pk_nwsites"));
							}
							sitelvl2.setMoreSiteDetailsFields(listNVPair);
							siteslvl2.add(sitelvl2);
							//siteslvl3=new ArrayList<Site>();
							if(sitelvl1!=null){
								psiteslvl1.setSites(siteslvl2);
								sitelvl1.setSites(psiteslvl1);
							}else{
								psiteslvl0.setSites(siteslvl2);
								site.setSites(psiteslvl0);
							}
							
							
						}
						else if(rs.getInt("nw_level")==3){
							sitelvl3=new Site();
							sitelvl3.setSiteIdentifier(siteIdentifier);
							sitelvl3.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl3.setName(rs.getString("site_name"));
							sitelvl3.setDescription(rs.getString("site_info"));
							System.out.println("level 3 description==="+sitelvl3.getDescription());
							sitelvl3.setSiteStatus(code);
							sitelvl3.setCtepId(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setPk(rs.getString("member_type"));
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl3.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl3.setRelationshipPK(rs.getString("pk_nwsites"));
							}
							sitelvl3.setMoreSiteDetailsFields(listNVPair);
							siteslvl3.add(sitelvl3);
							//siteslvl4=new ArrayList<Site>();
							if(sitelvl2!=null){
								psiteslvl2.setSites(siteslvl3);
								sitelvl2.setSites(psiteslvl2);
							}else{
								psiteslvl0.setSites(siteslvl3);
								site.setSites(psiteslvl0);
							}
						}
						else if(rs.getInt("nw_level")==4){
							sitelvl4=new Site();
							sitelvl4.setSiteIdentifier(siteIdentifier);
							sitelvl4.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl4.setName(rs.getString("site_name"));
							sitelvl4.setDescription(rs.getString("site_info"));
							sitelvl4.setSiteStatus(code);
							sitelvl4.setCtepId(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setPk(rs.getString("member_type"));
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl4.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl4.setRelationshipPK(rs.getString("pk_nwsites"));
							}
							sitelvl4.setMoreSiteDetailsFields(listNVPair);
							siteslvl4.add(sitelvl4);
							//siteslvl5=new ArrayList<Site>();
							if(sitelvl3!=null){
								psiteslvl3.setSites(siteslvl4);
								sitelvl3.setSites(psiteslvl3);
							}else{
								psiteslvl0.setSites(siteslvl4);
								site.setSites(psiteslvl0);
							}
							
						}
						else if(rs.getInt("nw_level")==5){
							sitelvl5=new Site();
							sitelvl5.setSiteIdentifier(siteIdentifier);
							sitelvl5.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl5.setName(rs.getString("site_name"));
							sitelvl5.setDescription(rs.getString("site_info"));
							sitelvl5.setSiteStatus(code);
							sitelvl5.setCtepId(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setPk(rs.getString("member_type"));
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl5.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl5.setRelationshipPK(rs.getString("pk_nwsites"));
							}
							sitelvl5.setMoreSiteDetailsFields(listNVPair);
							siteslvl5.add(sitelvl5);
							//siteslvl6=new ArrayList<Site>();
							if(sitelvl4!=null){
								psiteslvl4.setSites(siteslvl5);
								sitelvl4.setSites(psiteslvl4);
							}else{
								psiteslvl0.setSites(siteslvl5);
								site.setSites(psiteslvl0);
							}
						}
						else if(rs.getInt("nw_level")==6){
							sitelvl6=new Site();
							siteIdentifier=new SiteIdentifier();
							sitelvl6.setSiteIdentifier(siteIdentifier);
							sitelvl6.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl6.setName(rs.getString("site_name"));
							sitelvl6.setDescription(rs.getString("site_info"));
							sitelvl6.setSiteStatus(code);
							sitelvl6.setCtepId(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setPk(rs.getString("member_type"));
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl6.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl6.setRelationshipPK(rs.getString("pk_nwsites"));
							}
							sitelvl6.setMoreSiteDetailsFields(listNVPair);
							siteslvl6.add(sitelvl6);
							if(sitelvl5!=null){
								psiteslvl5.setSites(siteslvl6);
								sitelvl5.setSites(psiteslvl5);
							}else{
								psiteslvl0.setSites(siteslvl6);
								site.setSites(psiteslvl0);
							}
						}
						}
					}
					siteList.add(site);
				}
		
		sitesList.add((ArrayList<Site>) siteList);
		}
	}
}catch(OperationException ex){
	throw ex;
}catch(SQLException ex){
	ex.printStackTrace();
}catch (Exception e) {
	e.printStackTrace();
} finally {
	try {
		rs.close();
		pstmt.close();
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
}

public void getNetworkLevelSites(NetworkLevelSite networklevelsite, HashMap<String, Object> params) throws OperationException {

	UserAgentRObj userAgent = (UserAgentRObj) params.get("userAgent");
	String networkName = (String) params.get("networkName");
	String networkPk = (String) params.get("networkPk");
	String networkLevel = (String) params.get("networkLevel");
	UserBean userBean=(UserBean)params.get("callingUser");
	String iaccId = (String)params.get(KEY_ACCOUNT_ID);
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	NetworkRole roles=null;
	Code code = new Code();
	int count=0;
	int count1=0;
	String pk_main_network="";
	
	try {
		conn = CommonDAO.getConnection();

		
		StringBuffer sqlbuff = new StringBuffer();
		
		sqlbuff.append(" SELECT distinct nvl(FK_NWSITES_MAIN,pk_nwsites) fk_nwsites_main FROM er_nwsites,er_site WHERE er_site.fk_account=? and pk_site=fk_site ");
		
		if(!(networkPk.equals(""))){
			sqlbuff.append(" and nvl(FK_NWSITES_MAIN,pk_nwsites)="+networkPk+"");
		}
		
		if(!networkName.equals("") && networkPk.equals("")){
			sqlbuff.append(" and nvl(FK_NWSITES_MAIN,pk_nwsites) in (select pk_nwsites from er_nwsites,er_site where pk_site=fk_site and site_name like '"+networkName+"' and nw_level=0)" );
		}
		if(!networkLevel.equals("")){
			sqlbuff.append(" and nw_level="+networkLevel+" ");
		}
		
		
		pk_main_network = pkMainNetwork(sqlbuff.toString(),iaccId);
		
		if(pk_main_network.equals("0")){
			this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found")); 
			throw new OperationException();
		}
		
		StringBuffer sqlbuff1 = new StringBuffer();
		sqlbuff1.append(" SELECT pk_nwsites, fk_site,site_info as network_info, site_name ,(SELECT CODELST_DESC  FROM er_codelst  WHERE PK_CODELST=er_nwsites.NW_STATUS )   network_status,site_name AS network_name, ctep_id, er_codelst.CODELST_DESC   AS description,  er_codelst.codelst_type   AS stat_type, er_codelst.codelst_subtyp AS code, er_codelst.codelst_hide   AS stat_hidden, (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE)");
		sqlbuff1.append("  AS SiteRelationshipDesc, (SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE ) AS SiteRelationshipType, (SELECT CODELST_SUBTYP FROM er_codelst  WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE)   AS SiteRelationshipSubType,  NW_LEVEL AS nw_level FROM er_nwsites, er_site, er_codelst  where pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? and nw_level=0");  
				 	
		 	if(pk_main_network.length()>0 && !pk_main_network.equals("0"))
		 	{
		 		sqlbuff1.append(" and pk_nwsites in("+pk_main_network+") ");
		 	}

	
		pstmt = conn.prepareStatement(sqlbuff1.toString());
		pstmt.setInt(1, StringUtil.stringToNum(iaccId));
		rs=pstmt.executeQuery();
		Network network;
		while(rs.next()){
			network=new Network();
			code=new Code();
			pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
			fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
			netNameList.add(rs.getString("network_name"));
			netInfoList.add(rs.getString("network_info"));
			code.setType(rs.getString("stat_type"));
			code.setCode(rs.getString("code"));
			code.setDescription(rs.getString("description"));
			netStatList.add(code);
			if(rs.getString("SiteRelationshipType")!=null){
			code=new Code();
			code.setType(rs.getString("SiteRelationshipType"));
			code.setCode(rs.getString("SiteRelationshipSubType"));
			code.setDescription(rs.getString("SiteRelationshipDesc"));
			siteRelnShipType.add(code);
			}
			else{
				siteRelnShipType.add(null);
			}
			siteNameList.add(rs.getString("site_name"));
			netLevelList.add(String.valueOf(rs.getInt("nw_level")));
			siteInfoList.add(rs.getString("network_info"));
			if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
				pkRelnShipType.add(rs.getString("pk_nwsites"));
			}
			else{
				pkRelnShipType.add("0");
			}
			if(rs.getString("ctep_id")!=null){
				ctepList.add(rs.getString("ctep_id"));
			}else{
				ctepList.add("");
			}
			count++;
			
			
		}
		if(count==0)
		{
			this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found")); 
			throw new OperationException();
		}	
		List<Site> siteList=null;
		Site site=null;
		SiteIdentifier siteIdentifier=null;
		ObjectMap map=null;
		String child_Pk="";
		for(int i=0;i<pkNetList.size();i++){
			siteList=new ArrayList<Site>();
			site=new Site();
			siteIdentifier=new SiteIdentifier();
			map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
			siteIdentifier.setOID(map.getOID());
			siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
			site.setSiteIdentifier(siteIdentifier);
			site.setLevel(String.valueOf(netLevelList.get(i)));
			site.setDescription(siteInfoList.get(i));
			site.setName(siteNameList.get(i));
			site.setSiteStatus(netStatList.get(i));
			if(!pkRelnShipType.get(i).equals("0") && siteRelnShipType.get(i)!=null){
				site.setRelationshipPK(pkRelnShipType.get(i));
				site.setSiteRelationShipType(siteRelnShipType.get(i));
				}
			if(!ctepList.get(i).equals("")){
				site.setCtepId(ctepList.get(i));
			}
		StringBuffer sqlbuff2 = new StringBuffer();
		sqlbuff2.append("select pk_nwsites, fk_nwsites, fk_site,site_info, site_name,ctep_id, nw_status,er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,(SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, nw_level as nw_level, NW_MEMBERTYPE as relationShip_pk ");
		sqlbuff2.append(" FROM er_nwsites,er_site,er_codelst WHERE pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? AND  er_nwsites.FK_NWSITES_MAIN=? ");
		if(!networkLevel.equals("")){
			sqlbuff2.append(" and nw_level="+networkLevel+" ");
		}
		
		pstmt = conn.prepareStatement(sqlbuff2.toString());
		pstmt.setInt(1, StringUtil.stringToNum(iaccId));
		pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
		rs=pstmt.executeQuery();
		
		Site site1=null;
		Sites sites=null;
		List<Site> sitelst=null;
		sitelst=new ArrayList<Site>();
		child_Pk="";
		while (rs.next()) {
			site1=new Site();
			sites=new Sites();
			code=new Code();
			siteIdentifier=new SiteIdentifier();
			map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
			siteIdentifier.setOID(map.getOID());
			siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
			site1.setLevel(String.valueOf(rs.getInt("nw_level")));
			site1.setSiteIdentifier(siteIdentifier);
			site1.setName(rs.getString("site_name"));
			site1.setDescription(rs.getString("site_info"));
			code.setPk(String.valueOf(rs.getInt("nw_status")));
			code.setType(rs.getString("stat_type"));
			code.setCode(rs.getString("code"));
			code.setDescription(rs.getString("description"));
			site1.setSiteStatus(code);
			site1.setCtepId(rs.getString("ctep_id"));
			
		if(rs.getString("SiteRelationshipType")!=null){
		code=new Code();
		code.setType(rs.getString("SiteRelationshipType"));
		code.setCode(rs.getString("SiteRelationshipSubType"));
		code.setDescription(rs.getString("SiteRelationshipDesc"));
		code.setPk(rs.getString("relationShip_pk"));
		site1.setSiteRelationShipType(code);
		}
		if(rs.getString("pk_nwsites")!=null){
			site1.setRelationshipPK(rs.getString("pk_nwsites"));
		}
		
		sitelst.add(site1);
		sites.setSites(sitelst);
		site.setSites(sites);
		}
		
		siteList.add(site);
		sitesList.add((ArrayList<Site>) siteList);
		
		
		}
	
	}catch(OperationException ex){
		throw ex;
	}catch(SQLException ex){
		ex.printStackTrace();
	}catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			if(rs!=null)
			rs.close();
			if(pstmt!=null)
			pstmt.close();
			if(conn!=null)
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

public UserNetworkSiteResults userNetworkSites(UserNetworkSites userNetworkSites, HashMap<String, Object> params) throws OperationException {
	UserNetworkSiteResults networkSiteSearchResults = new UserNetworkSiteResults();
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	Connection conn1=null;
	PreparedStatement pstmt1 = null;
	ResultSet rs1 = null;
	int count=0;
	String iaccId = (String)params.get(KEY_ACCOUNT_ID);
	UserBean userBean=(UserBean)params.get("callingUser");
	UserBean userBeanPk = null;
	UserAgentRObj userAgent = (UserAgentRObj) params.get("userAgent");
	String networkName = (userNetworkSites.getNetworkName()==null)?"":userNetworkSites.getNetworkName();
	String userName = (userNetworkSites.getUserLoginName()==null)?"":userNetworkSites.getUserLoginName();
	int userPk= (userNetworkSites.getUserPK()==null)?0:userNetworkSites.getUserPK();
	int networkPk = (userNetworkSites.getNetworkPK()==null)?0:userNetworkSites.getNetworkPK();
	String pkCode="";
	int userPK=0;
	String userNAME="";
	String str = "";
	String pk_main_network = "";
	int row=0;
	
try {
	
	if(userPk!=0){
    	userBeanPk = userAgent.getUserDetails(userPk);
		if(userBeanPk==null){
			if(userName.equals("")){
			    this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User PK not found.")); 
				throw new OperationException();	
			}
		}else{
			userNAME=userBeanPk.getUserLoginName();
			if(!userNAME.equalsIgnoreCase(userName) && !userName.equals("") && !userNAME.equals("")){
				this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User Login name does not match with User PK.")); 
				throw new OperationException();	
			}
		}
	}
				
		if(!userName.equals("")){
			UserDao userDao = new UserDao();
			userDao.getUserValuesByLoginName(userName);
			if(userDao.getUsrIds().size()==0){
				this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User LoginName not found.")); 
				throw new OperationException();	
			}else
			{
				userPK = (Integer)userDao.getUsrIds().get(0);
				if(userPK!=userPk && userPk!=0 && userPK!=0){
					this.response=new ResponseHolder();
					this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User PK does not match with User login name.")); 
					throw new OperationException();	
				}
			}
		}
	
	
	str="SELECT distinct nvl(nws.FK_NWSITES_MAIN,nws.pk_nwsites) fk_nwsites_main FROM er_nwsites nws, er_site s, er_nwusers nwu, er_user u WHERE s.fk_account=? AND s.pk_site=nws.fk_site AND nws.pk_nwsites=nwu.FK_NWSITES AND nwu.fk_user=u.pk_user ";
	
	if(networkPk!=0){
		str=str+" and nvl(nws.FK_NWSITES_MAIN,nws.pk_nwsites)="+networkPk;
	}
	
	if(!networkName.equals("") && networkPk==0){
		str=str+"and nvl(nws.FK_NWSITES_MAIN,nws.pk_nwsites) in (select ns.pk_nwsites from er_nwsites ns, er_site es where es.pk_site=ns.fk_site and es.site_name like '"+networkName+"' and ns.nw_level=0)";
	}
	if(userPk!=0){
		str=str+"AND u.pk_user="+userPk;
	}
	if(!userName.equals("")){
		str=str+"AND u.USR_LOGNAME='"+userName+"' ";
	}
	
	pk_main_network = pkMainNetwork(str,iaccId);
	
	if(pk_main_network.equals("0") && networkPk!=0 && networkName.equals("")){
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network PK not found")); 
		throw new OperationException();
	}else if(pk_main_network.equals("0") && !networkName.equals("") && networkPk==0){
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network Name not found")); 
		throw new OperationException();
	}else if(pk_main_network.equals("0")){
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network not found")); 
		throw new OperationException();
	}
	
	 String str1 = "select pk_nwsites,fk_site,site_name ,site_name as network_name,ctep_id,nvl(site_info,'-') as network_info, er_codelst.pk_codelst as pkStat, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden, (SELECT pk_codelst FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) as pkRelnType, (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType, nw_level from er_nwsites,er_site,er_codelst "+
	 			" where pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? and nw_level=0 ";
	 		 	
	 	if(pk_main_network.length()>0 && !pk_main_network.equals("0"))
	 	{
	 		str1=str1+" and pk_nwsites in("+pk_main_network+") ";
	 	}
	 	if(!networkName.equals("")){
	 		str1=str1+" and site_name like '"+networkName+"'";
	 	}

	conn = CommonDAO.getConnection();
	pstmt = conn.prepareStatement(str1);
	pstmt.setInt(1, StringUtil.stringToNum(iaccId));
	rs = pstmt.executeQuery();
	Code code=null;
	while (rs.next()) {
		code=new Code();
		pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
		fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
		if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
			pkRelnShipType.add(rs.getString("pk_nwsites"));
		}
		else{
			pkRelnShipType.add("0");
		}
		netNameList.add(rs.getString("network_name"));
		netInfoList.add(rs.getString("network_info"));
		code.setPk(rs.getString("pkStat"));
		code.setType(rs.getString("stat_type"));
		code.setCode(rs.getString("code"));
		code.setDescription(rs.getString("description"));
		netStatList.add(code);
		if(rs.getString("SiteRelationshipType")!=null){
		code=new Code();
		code.setPk(rs.getString("pkRelnType"));
		code.setType(rs.getString("SiteRelationshipType"));
		code.setCode(rs.getString("SiteRelationshipSubType"));
		code.setDescription(rs.getString("SiteRelationshipDesc"));
		siteRelnShipType.add(code);
		}
		else{
			siteRelnShipType.add(null);
		}
		siteNameList.add(rs.getString("site_name"));
		netLevelList.add(String.valueOf(rs.getInt("nw_level")));
		if(rs.getString("ctep_id")!=null){
			ctepList.add(rs.getString("ctep_id"));
		}else{
			ctepList.add("");
		}
		count=count+1;
	}
	System.out.println("count = " +count);
	if(count==0 && !networkName.equals("")){
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network not found")); 
		throw new OperationException();
	}
	
	
		
	
	
	String chlSql="";
	UserSite site=null;
	SiteIdentifier siteIdentifier=null;
	ObjectMap map=null;
	UserSite sitelvl=null;
	UserSites sites=null;
	List<UserSite> sitesLvlList=null;
	List<UserSite> siteList=null;
	UserNetworks network = null;
	UserSites sites1=null;
	for(int i=0;i<pkNetList.size();i++){
		System.out.println("i = "+i);
		row=0;
		siteList=new ArrayList<UserSite>();
		site=new UserSite();
		siteIdentifier=new SiteIdentifier();
		map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
		siteIdentifier.setOID(map.getOID());
		siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
		site.setSiteIdentifier(siteIdentifier);
		site.setLevel(String.valueOf(netLevelList.get(i)));
		site.setName(siteNameList.get(i));
		if(!netInfoList.get(i).equals("-")){
			site.setDescription(netInfoList.get(i));
		}
		site.setSiteStatus(netStatList.get(i));
		if(!pkRelnShipType.get(i).equals("0") && siteRelnShipType.get(i)!=null){
		site.setRelationshipPK(pkRelnShipType.get(i));
		site.setSiteRelationShipType(siteRelnShipType.get(i));
		}
		if(!ctepList.get(i).equals("")){
			site.setCtepId(ctepList.get(i));
		}

		chlSql="SELECT nws.pk_nwsites,nws.nw_membertype relationship_pk, f_codelst_desc(nws.nw_membertype) relationship_desc, "
				+ "f_codelst_subtyp(nws.nw_membertype) relationship_subtyp, (select codelst_type from er_codelst where "
		 		+ "pk_codelst=nws.NW_MEMBERTYPE) relationship_type, s.site_name network_name, s.pk_site fk_site, nvl(s.site_info,'-') description, "
		 		+ "nws.nw_level nw_level,nws.nw_status, f_codelst_desc(nws.nw_status) nw_status_desc, "
		 		+ "f_codelst_subtyp(nws.nw_status) nw_status_subtyp, (select codelst_type from er_codelst where pk_codelst=nws.nw_status) nw_status_type, "
		 		+ "s.ctep_id  FROM er_nwsites nws, er_nwusers nwu, er_user u, er_site s WHERE nws.pk_nwsites  = nwu.FK_NWSITES "
		 		+ "AND nws.fk_site = s.pk_site AND nwu.fk_user = u.pk_user AND (u.pk_user = ? OR u.USR_LOGNAME =?) "
		 		+ "AND (nws.pk_nwsites in (?) OR nws.FK_NWSITES_MAIN in (?)) AND s.fk_account=? order by nw_level";
																													
	pstmt = conn.prepareStatement(chlSql);
	pstmt.setInt(1, userPk);
	pstmt.setString(2, userName);
	pstmt.setInt(3, StringUtil.stringToNum(pkNetList.get(i)));
	pstmt.setInt(4, StringUtil.stringToNum(pkNetList.get(i)));
	/*if(!networkName.equals("")){
		pstmt.setString(5, networkName);
	}else{
		pstmt.setString(5, netNameList.get(i));
	}*/
	pstmt.setInt(5, StringUtil.stringToNum(iaccId));
	rs = pstmt.executeQuery();
	
	sitesLvlList=new ArrayList<UserSite>();
	while(rs.next()){

		    sites=new UserSites();
			code=new Code();
			siteIdentifier=new SiteIdentifier();
			map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
			siteIdentifier.setOID(map.getOID());
			siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
			code.setPk(rs.getString("nw_status"));
			code.setType(rs.getString("nw_status_type"));
			code.setCode(rs.getString("nw_status_subtyp"));
			code.setDescription(rs.getString("nw_status_desc"));
			sitelvl=new UserSite();
			sitelvl.setSiteIdentifier(siteIdentifier);
			sitelvl.setLevel(String.valueOf(rs.getInt("nw_level")));
			sitelvl.setName(rs.getString("network_name"));
			if(!rs.getString("description").equals("-")){
				sitelvl.setDescription(rs.getString("description"));
			}
			sitelvl.setSiteStatus(code);
			sitelvl.setCtepId(rs.getString("ctep_id"));
			if(rs.getString("relationship_type")!=null){
				code=new Code();
				code.setPk(rs.getString("relationship_pk"));
				code.setType(rs.getString("relationship_type"));
				code.setCode(rs.getString("relationship_subtyp"));
				code.setDescription(rs.getString("relationship_desc"));
				sitelvl.setSiteRelationShipType(code);
			}
			if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
				sitelvl.setRelationshipPK(rs.getString("pk_nwsites"));
			}
			sitesLvlList.add(sitelvl);
			sites.setSites(sitesLvlList);
			site.setSites(sites);
			row++;
		}
	if(row==0 && network==null && i==pkNetList.size()-1)
	{
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found")); 
		throw new OperationException();
	}	
	if(row>0){
	siteList.add(site);
	sitesLists.add((ArrayList<UserSite>) siteList);
	
	
			network = new UserNetworks();
			sites1 = new UserSites();
			network.setName(netNameList.get(i));
			NetworkIdentifier netIdentifier = new NetworkIdentifier();
			ObjectMap map1 = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_NETWORK, Integer.valueOf(pkNetList.get(i)));
			netIdentifier.setOID(map1.getOID());
			netIdentifier.setPK(Integer.valueOf(pkNetList.get(i)));
			network.setNetworkIdentifier(netIdentifier);
			network.setNetworkStatus(netStatList.get(i));
			network.setNetworkRelationShipType(siteRelnShipType.get(i));
			sites1.setSites(sitesLists.get(i));
			network.setSites(sites1);
			networkSiteSearchResults.addNetwork(network);
	}
	}
	

}catch(OperationException ex){
	throw ex;
}catch (SQLException e) {
	e.printStackTrace();
}catch (Exception e) {
	e.printStackTrace();
} finally {
	try {
		if(rs!=null)
		rs.close();
		if(pstmt!=null)
		pstmt.close();
		if(conn!=null)
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
return networkSiteSearchResults;

	
}
public void searchUserNetworks(UserNetworksDetail networkuser, HashMap<String, Object> params) throws OperationException {
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	ResultSet rs1=null;
	int userLoginPK = 0;
	int userPK=0;
	int userOIDPK=0;
	String pkCode="";
	int count=0;
	String net_pks="";
	String userLoginName="";
	ObjectMap map=null;
	UserBean userBeanPk = null;
	String networkName="";
	String networkPk="";
	String relationShipPK="";
	String sitePK="";
	String siteName="";
	UserAgentRObj userAgent = (UserAgentRObj) params.get("userAgent");
	UserBean userBean=(UserBean)params.get("callingUser");
	String iaccId = (String)params.get(KEY_ACCOUNT_ID);
	//System.out.println("networkuser.getNetworkDetails().getNetworkName()=="+networkuser.getNetworkDetails().getNetworkName());
	NetworkServiceDao networks=new NetworkServiceDao();
System.out.println("Here");

if(networkuser.getPK()!=null ){
	int usr=networkuser.getPK();
	userBeanPk = userAgent.getUserDetails(usr);
	if(userBeanPk==null ){
		if(usr!=0)
		{
		     this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND, "User does not exist.")); 
			throw new OperationException();	
	}
	}else{
		userPK = userBeanPk.getUserId();
	}
}
userLoginName=networkuser.getUserLoginName()==null?"":networkuser.getUserLoginName();
	if(!userLoginName.equals("")){
		UserDao userDao = new UserDao();
		userDao.getUserValuesByLoginName(networkuser.getUserLoginName());
		if(userDao.getUsrIds().size()==0){
			this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND, "User does not exist.")); 
			throw new OperationException();	
		}else
		{
			userLoginPK = (Integer)userDao.getUsrIds().get(0);
		}
	}
//	if (networkuser.getOID() != null){
//		try{
//    ObjectMap objectMap =((ObjectMapService)params.get("objectMapService")).getObjectMapFromId(networkuser.getOID());
//    if(objectMap == null){
//		this.response=new ResponseHolder();
//		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
//		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid User Object is required with OID.")); 
//		throw new OperationException();
//    }
//	if(objectMap != null){
//		    UserBean userBeanFromOID = userAgent.getUserDetails(objectMap.getTablePK());
//			if (userBeanFromOID != null && userBeanFromOID.getUserAccountId().equals(iaccId)) {
//				userOIDPK=userBeanFromOID.getUserId();
//			}
//	}}catch(MultipleObjectsFoundException e)
//		{
//			System.err.print(e.getMessage());
//		}
//		
//}	
if( userPK>0 && userLoginPK>0 ){
	if( userPK!=userLoginPK ){
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND, "User PK is not matched with User Login Name.")); 
		throw new OperationException();	
	}}
//else if(userPK>0 && userOIDPK>0){
//	
//	if( userPK!=userOIDPK ){
//		this.response=new ResponseHolder();
//		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
//		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User PK is Not Matched with User OID.")); 
//		throw new OperationException();	
//	}}
//else if(userOIDPK>0 && userLoginPK>0 ){
//	if( userOIDPK!=userLoginPK ){
//		this.response=new ResponseHolder();
//		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
//		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User Name is  Not Matched with User OID.")); 
//		throw new OperationException();	
//	}
//}
//

StringBuffer sqlbuf = new StringBuffer();
String fk_user="";
String FK_NWSITES_MAIN="";
try
{
 sqlbuf.append("SELECT distinct case when nws.FK_NWSITES_MAIN is not null then nws.FK_NWSITES_MAIN else nws.pk_nwsites end as fk_nwsites_main FROM er_nwsites nws,  er_nwusers nwu,  er_user u WHERE nws.pk_nwsites=nwu.FK_NWSITES AND nwu.fk_user=u.pk_user AND (u.pk_user="+userPK+" OR u.USR_LOGNAME='"+userLoginName + "') and fk_account=? "  ); 			
 System.out.println("sqlbuf==="+sqlbuf.toString());
 FK_NWSITES_MAIN = pkMainNetwork(sqlbuf.toString(),iaccId);

}
catch (Exception e) {
	e.printStackTrace();
}
StringBuffer sqlbuf1 = new StringBuffer();
//FK_NWSITES_MAIN = pkMainNetwork(sqlbuf.toString(),iaccId);

 try
 {
	 sqlbuf1.append("SELECT ns.pk_nwsites,s.site_name  AS network_name,ns.NW_MEMBERTYPE, f_codelst_desc(ns.NW_MEMBERTYPE) relationship_desc,f_codelst_subtyp(ns.NW_MEMBERTYPE) AS code, (select codelst_type from er_codelst where pk_codelst=ns.NW_MEMBERTYPE) relationship_type, ns.nw_status, f_codelst_desc(ns.nw_status) status_desc, f_codelst_subtyp(ns.nw_status) status_subtyp,(select codelst_type from er_codelst where pk_codelst=ns.nw_status) status_type FROM er_nwsites ns, er_site s WHERE pk_nwsites IN ( "+ FK_NWSITES_MAIN+" )and s.fk_account="+iaccId+" and s.pk_site=ns.fk_site");

	 System.out.println("sqlbuf1==="+sqlbuf1.toString());
	 System.out.println("network name=="+networkName);
	 conn = CommonDAO.getConnection();
	 pstmt = conn.prepareStatement(sqlbuf1.toString());
		rs = pstmt.executeQuery();
		Code code=null;
		while (rs.next()) {
			code=new Code();
			pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
			if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
				pkRelnShipType.add(rs.getString("pk_nwsites"));
			}
			else{
				pkRelnShipType.add("0");
			}
			netNameList.add(rs.getString("network_name"));
			//netInfoList.add(rs.getString("network_info"));
			code.setPk(rs.getString("nw_status"));
			code.setType(rs.getString("status_type"));
			code.setCode(rs.getString("status_subtyp"));
			code.setDescription(rs.getString("status_desc"));
			netStatList.add(code);
			if(rs.getString("relationship_type")!=null){
			code=new Code();
			code.setPk(rs.getString("NW_MEMBERTYPE"));
			code.setType(rs.getString("relationship_type"));
			code.setCode(rs.getString("code"));
			code.setDescription(rs.getString("relationship_desc"));
			siteRelnShipType.add(code);
			}
			else{
				siteRelnShipType.add(null);
			}
			//siteNameList.add(rs.getString("site_name"));
			//netLevelList.add(String.valueOf(rs.getInt("nw_level")));
		//	if(rs.getString("ctep_id")!=null){
		//		ctepList.add(rs.getString("ctep_id"));
		//	}else{
		//		ctepList.add("");
		//	}
			count++;
		}
		
		
	if(count==0)
	{
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found")); 
		throw new OperationException();
	}
	}
 catch (SQLException e) {
		e.printStackTrace();
	}

finally{
	try {
		rs.close();
		if(rs1 != null){
		rs1.close();
	     }
		pstmt.close();
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
}

public UserNetworkSiteResults getNetworkUser(NetworkSiteUserDetails networkSiteUserDetails, HashMap<String, Object> params) throws OperationException{
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	ResultSet rs1=null;
	int userPkCountN=0;
	int userPkCountS=0;
	String pkCode="";
	int count=0;
	String net_pks="";
	ObjectMap map=null;
	UserBean userBean = null;
	String networkName="";
	String siteName="";
	String networkPK = "";
	String sitePK = "";
	String relationShipPK = "";
	UserAgentRObj userAgent = (UserAgentRObj) params.get("userAgent");
	String iaccId = (String)params.get(KEY_ACCOUNT_ID);
	String CTEP_ID="";
	String level="";
	String userPK="";
	String userLoginName="";
	String userLoginPK="";
	String relation_pks="";
	UserNetworkSiteResults networkSiteSearchResults = new UserNetworkSiteResults();
	
	System.out.println("relation_pks==="+relation_pks);
	if(networkSiteUserDetails!=null){
		
    if(networkSiteUserDetails.getNetworkName() != null && !networkSiteUserDetails.getNetworkName().equals("")){
    	networkName = networkSiteUserDetails.getNetworkName();
    }
    if(networkSiteUserDetails.getNetworkPK() != null && !networkSiteUserDetails.getNetworkPK().equals("")){
    	networkPK = networkSiteUserDetails.getNetworkPK();
    	System.out.println("networkPK=="+networkPK);
    }
    if(networkSiteUserDetails.getSitePK() != null && !networkSiteUserDetails.getSitePK().equals("")){
    	sitePK = networkSiteUserDetails.getSitePK();
    }
    if(networkSiteUserDetails.getRelationshipPK() != null && !networkSiteUserDetails.getRelationshipPK().equals("")){
    	relationShipPK = networkSiteUserDetails.getRelationshipPK();
    }
    if(networkSiteUserDetails.getSiteName() != null && !networkSiteUserDetails.getSiteName().equals("")){
    	siteName = networkSiteUserDetails.getSiteName();
    }
    if(networkSiteUserDetails.getLevel()!=null && !networkSiteUserDetails.getLevel().equals("")){
    	level = networkSiteUserDetails.getLevel();
    }
    if(networkSiteUserDetails.getUserPK()!=null){
    	int usr=EJBUtil.stringToNum(networkSiteUserDetails.getUserPK());
    	UserBean userBeanPk = userAgent.getUserDetails(usr);
		if(userBeanPk==null){
			     this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND, "UserPK not exists")); 
				throw new OperationException();	
		}else{
			userPK = String.valueOf(userBeanPk.getUserId());
		}
	}
				
		if(networkSiteUserDetails.getUserLoginName()!=null){
			UserDao userDao = new UserDao();
			userDao.getUserValuesByLoginName(networkSiteUserDetails.getUserLoginName());
			if(userDao.getUsrIds().size()==0){
				this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND, "UserLoginName not exists")); 
				throw new OperationException();	
			}else
			{
				if(userPK.equals("")){
					userPK = userDao.getUsrIds().get(0).toString();
				}else if(!userPK.equals(userDao.getUsrIds().get(0).toString())){
					this.response=new ResponseHolder();
					this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "UserLoginName not matching with UserPK")); 
					throw new OperationException();	
				}
			}
		}
    
	if(networkName.equals("") && networkPK.equals("") && relationShipPK.equals("")){
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network Identifier or relationshipPK is Required.")); 
		throw new OperationException();
	}
	}
 


 if(networkName.equals("") && networkPK.equals("") && sitePK.equals("") && siteName.equals("") && relationShipPK.equals("") && level.equals("") && userPK.equals("")){
	this.response=new ResponseHolder();
	this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
	this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid data is Required.")); 
	throw new OperationException();
 }     
			
	

StringBuffer sqlbuf = new StringBuffer();
String fk_user="";
String FK_NWSITES_MAIN="";
String str="";
String pk_main_network="";


		if(!networkPK.equals("")){
			pk_main_network=networkPK;
			System.out.println("pk_main_network Inside If==="+pk_main_network);
		}else if(!relationShipPK.equals("")){
			
			String getPrimaryNetwork="select rowtocol('select distinct nvl(fk_nwsites_main,pk_nwsites) FROM er_nwsites WHERE pk_nwsites in ("+relationShipPK+")') as fk_nwsites_main from dual";
			System.out.println("getPrimaryNetwork sql==="+getPrimaryNetwork);
 			try {
 				conn = CommonDAO.getConnection();
 				pstmt = conn.prepareStatement(getPrimaryNetwork);
 				rs=pstmt.executeQuery();
 				while(rs.next()){
 					if(rs.getString("fk_nwsites_main")!=null){
 						pk_main_network=rs.getString("fk_nwsites_main");
 					}
 				}
 				System.out.println("new condition pk_main_network==="+pk_main_network);
 			}catch(Exception e){
 				e.printStackTrace();
 			}
		}
	



net_pks=net_pks.equals("")?net_pks:"";
System.out.println("pk_main_network==="+pk_main_network);
	String str1 = "select pk_nwsites,fk_site,site_name ,nw_status,site_info,site_name as network_name,er_nwsites.nw_level as nw_level,er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden, nw_membertype,"
			+" (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
			+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, CTEP_ID "
			+ "from er_nwsites,er_site,er_codelst "+
				  "where pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? ";
	if(!(networkName.equals(""))){
			 	 str1=str1+" and lower(site_name) like lower('"+networkName+"') ";
		}
	if(level.equals("0")){
		str1 = str1+" and er_nwsites.nw_level="+level;
		if(!userPK.equals("")){
			
		}
	}
	//if(networkName.equals("") && pk_main_network.length()>0)
	if(pk_main_network.length()>0)
	{
		str1=str1+" and pk_nwsites in("+pk_main_network+") ";
	}
try {
	System.out.println("str1==="+str1.toString());
conn = CommonDAO.getConnection();
pstmt = conn.prepareStatement(str1);
pstmt.setInt(1, StringUtil.stringToNum(iaccId));
rs = pstmt.executeQuery();
Code code=null;
Code code1=null;
while (rs.next()) {
	System.out.println("Inside While");
	code = new Code();
	pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
	fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
	netNameList.add(rs.getString("network_name"));
	netInfoList.add(rs.getString("site_info"));
	if(rs.getString("nw_status")!=null){
	code.setType(rs.getString("stat_type"));
	code.setCode(rs.getString("code"));
	code.setDescription(rs.getString("description"));
	code.setPk(String.valueOf(rs.getInt("nw_status")));
	netStatList.add(code);
	}
	ctepList.add(rs.getString("CTEP_ID"));
	if(rs.getString("SiteRelationshipType")!=null){
		code1=new Code();
		code1.setType(rs.getString("SiteRelationshipType"));
		code1.setCode(rs.getString("SiteRelationshipSubType"));
		code1.setDescription(rs.getString("SiteRelationshipDesc"));
		siteRelnShipType.add(code1);
	}
	
	siteNameList.add(rs.getString("site_name"));
	netLevelList.add(String.valueOf(rs.getInt("nw_level")));
	count++;
}
System.out.println("count==="+count);
if(count==0)
{
	this.response=new ResponseHolder();
	this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
	this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network not found")); 
	throw new OperationException();
}

String chlSql="";
UserSite site=null;
SiteIdentifier siteIdentifier=null;
ObjectMapService objectMapService=null;
UserSite sitelvl=null;
UserSites sites=null;
List<UserSite> sitesLvlList=null;
List<UserSite> siteList=null;
UserNetworks network = null;
UserSites sites1=null;
	
	for(int i=0;i<pkNetList.size();i++){
		site=new UserSite();
		siteList=new ArrayList<UserSite>();
		siteIdentifier=new SiteIdentifier();
		map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
		siteIdentifier.setOID(map.getOID());
		siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
		site.setName(siteNameList.get(i).toString());
		site.setSiteIdentifier(siteIdentifier);
		site.setLevel(String.valueOf(netLevelList.get(i)));
		site.setSiteStatus(netStatList.get(i));
		site.setCtepId(ctepList.get(i));
		site.setDescription(netInfoList.get(i));
		if(code1!=null){
			site.setSiteRelationShipType(code1);
		}
		objectMapService = ((ObjectMapService)params.get("objectMapService"));
		params.put("objectMapService", objectMapService);
		System.out.println("userPkCountN==="+userPkCountN);
		String pks_childNtw="";
			
			StringBuffer sqlbuf2 = new StringBuffer();
		sqlbuf2.append("SELECT distinct(pk_nwsites) as pk_nwsites FROM er_nwsites,er_site,er_codelst,er_nwusers nu WHERE pk_site=fk_site AND er_site.fk_account=? AND (er_nwsites.FK_NWSITES_MAIN =? or er_nwsites.PK_NWSITES =?) and pk_codelst=nw_status and nu.fk_nwsites=pk_nwsites ");
		if(!level.equals("0")){
			if(!(siteName.equals("")))
		    {
			sqlbuf2.append(" and lower(site_name) like lower('" +siteName+ "')");
			 }
			if(!sitePK.equals("")){
				sqlbuf2.append(" and er_nwsites.fk_site="+sitePK+"");
			}
		}
		if(!relationShipPK.equals("")){
			sqlbuf2.append(" and pk_nwsites="+relationShipPK+"");
		}
		if(!level.equals("")){
			sqlbuf2.append(" and er_nwsites.nw_level="+level+"");
		}
		if(!userPK.equals("")){
			sqlbuf2.append(" and nu.fk_user="+userPK+"");
		}
		
		
		
		System.out.println("sqlbuf2==="+sqlbuf2.toString());
		System.out.println("pkNetList.get(i)==="+pkNetList.get(i));
		
		pstmt = conn.prepareStatement(sqlbuf2.toString());
		pstmt.setInt(1, StringUtil.stringToNum(iaccId));
		pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
		pstmt.setInt(3, StringUtil.stringToNum(pkNetList.get(i)));
		rs = pstmt.executeQuery();
	
		while (rs.next()) {
			pks_childNtw=pks_childNtw+rs.getInt("pk_nwsites")+",";
		}
		pks_childNtw=pks_childNtw.length()>0?pks_childNtw.substring(0,pks_childNtw.length()-1):"0";
		System.out.println("pks_childNtw==="+pks_childNtw);
		if(!pks_childNtw.equals("0")){
		
		StringBuffer sqlbuf3 = new StringBuffer();
		sqlbuf3.append("SELECT distinct pk_nwsites, fk_site,  site_name ,site_info,ctep_id, site_name AS network_name,nw_status, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,er_nwsites.NW_LEVEL AS nw_level,er_nwsites.NW_MEMBERTYPE as relationShip_pk, "
				+" (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
				+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide "
				+ "FROM er_nwsites,er_site,er_codelst,er_nwusers nu WHERE pk_site=fk_site AND er_site.fk_account=? AND (er_nwsites.FK_NWSITES_MAIN =? OR er_nwsites.pk_nwsites=?) and pk_codelst=nw_status and nu.fk_nwsites=er_nwsites.pk_nwsites "); 
		if(!userPK.equals("")){
			sqlbuf3.append(" and nu.fk_user="+userPK+"");
		}
		sqlbuf3.append(" and pk_nwsites in("+pks_childNtw+")");
		
		System.out.println("sqlbuf3==="+sqlbuf3.toString());
		System.out.println("pkNetList.get(i)==="+pkNetList.get(i));
		    pstmt = conn.prepareStatement(sqlbuf3.toString());
			pstmt.setInt(1, StringUtil.stringToNum(iaccId));
			pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
			pstmt.setInt(3, StringUtil.stringToNum(pkNetList.get(i)));
			rs = pstmt.executeQuery();
			sitesLvlList=new ArrayList<UserSite>();
			while(rs.next()){

				    sites=new UserSites();
					code=new Code();
					siteIdentifier=new SiteIdentifier();
					map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
					siteIdentifier.setOID(map.getOID());
					siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
					code.setPk(rs.getString("nw_status"));
					code.setType(rs.getString("stat_type"));
					code.setCode(rs.getString("code"));
					code.setDescription(rs.getString("description"));
					sitelvl=new UserSite();
					sitelvl.setSiteIdentifier(siteIdentifier);
					sitelvl.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl.setName(rs.getString("network_name"));
					if(rs.getString("site_info")!=null){
						sitelvl.setDescription(rs.getString("site_info"));
					}
					if(!userPK.equals("")){
						networkUsersRoles(rs.getString("pk_nwsites"),iaccId,sitelvl,params,userPK);
					}else{
						networkUsersRoles(rs.getString("pk_nwsites"),iaccId,sitelvl,params,"");
					}
					
					sitelvl.setSiteStatus(code);
					sitelvl.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("relationship_pk")!=null){
						code=new Code();
						code.setPk(rs.getString("relationship_pk"));
						code.setType(rs.getString("SiteRelationshipType"));
						code.setCode(rs.getString("SiteRelationshipSubType"));
						code.setDescription(rs.getString("SiteRelationshipDesc"));
						sitelvl.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitesLvlList.add(sitelvl);
					sites.setSites(sitesLvlList);
					site.setSites(sites);
					
				}
			
			
			siteList.add(site);
			sitesLists.add((ArrayList<UserSite>) siteList);
			
			
					network = new UserNetworks();
					sites1 = new UserSites();
					network.setName(netNameList.get(i));
					NetworkIdentifier netIdentifier = new NetworkIdentifier();
					ObjectMap map1 = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_NETWORK, Integer.valueOf(pkNetList.get(i)));
					netIdentifier.setOID(map1.getOID());
					netIdentifier.setPK(Integer.valueOf(pkNetList.get(i)));
					network.setNetworkIdentifier(netIdentifier);
					if(netStatList.size()>0 && netStatList.get(i)!=null){
					network.setNetworkStatus(netStatList.get(i));
					}
					if(code1!=null){
					network.setNetworkRelationShipType(code1);
					}
					sites1.setSites(sitesLists.get(sitesLists.size()-1));
					network.setSites(sites1);
					
						networkSiteSearchResults.addNetwork(network);
					}
					
					
			
	}
}catch(OperationException ex){
	throw ex;
}
catch (Exception e) {
	e.printStackTrace();
} finally {
	try {
		rs.close();
		if(rs1 != null){
		rs1.close();
	     }
		pstmt.close();
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
return networkSiteSearchResults;
	
}
public int removeNetworkSite(RemoveNetworkSite removeNetworkSite, HashMap<String, Object> params) throws OperationException, SQLException {
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;	
	ResultSet rs1 = null;
	String networkName="";
	int relationshipPK=0;
	String getPrimaryNetwork="";
	int pk_main_network=0;
	String ipAdd="";
	String siteName="";
	String siteLevel="";
	String siteRelationshipType="";
	String siteRelationshipCode="";
	String siteRelationshipDesc="";
	String siteRelationshipPK="";
	String networkPK="";
	String networkOID="";
	String sql="";
	StringBuffer sqlbuf4 = new StringBuffer();
	conn = CommonDAO.getConnection();
	boolean siteLvlDet=false;
	int count=0;
	int countPK=0;
	UserBean userBean=(UserBean)params.get("callingUser");
	NetworkDao nwdao=new NetworkDao();
	String userId =(userBean.getUserId()).toString();
	String status_Type="";
	userId=userId==null?"":userId;
			
	String iaccId = ((String)params.get(KEY_ACCOUNT_ID)==null)?"":(String)params.get(KEY_ACCOUNT_ID);
	ResponseHolder response = (ResponseHolder)params.get("ResponseHolder");				
	if(removeNetworkSite!=null)	{
				if(removeNetworkSite!=null)	{
		List<NetworkSiteDetail> networkSiteDetail=removeNetworkSite.getNetworkSiteDetailList();
		NetworkSiteDetail networkSite=new NetworkSiteDetail();
		try {
		for(int iX=0;iX<networkSiteDetail.size();iX++)
		{
			networkSite=networkSiteDetail.get(iX);
			if(networkSite.getSiteLevelDetails()!=null && !(networkSite.getSiteLevelDetails().equals("")))
			{
			siteName=networkSite.getSiteLevelDetails().getSiteName()==null?"":networkSite.getSiteLevelDetails().getSiteName();
			siteLevel=((networkSite.getSiteLevelDetails().getLevel()==null)?"":(networkSite.getSiteLevelDetails().getLevel()));
			}
			
			if(networkSite.getNetwork()!=null){
				if(networkSite.getNetwork().getNetworkIdentifier().getPK()!=null){
					networkPK = String.valueOf(networkSite.getNetwork().getNetworkIdentifier().getPK());
				}
				 if(networkSite.getNetwork().getNetworkIdentifier().getOID()!=null){
					networkPK = String.valueOf(((ObjectMapService)params.get("objectMapService")).getObjectPkFromOID(networkSiteDetail.get(iX).getNetwork().getNetworkIdentifier().getOID()));
				}
			}
			if(networkSite.getSiteLevelDetails()!=null){
				siteRelationshipType=networkSite.getSiteLevelDetails().getRelationshipType().getType()==null?"":networkSite.getSiteLevelDetails().getRelationshipType().getType();
				siteRelationshipCode=networkSite.getSiteLevelDetails().getRelationshipType().getCode()==null?"":networkSite.getSiteLevelDetails().getRelationshipType().getCode();
				siteRelationshipPK=networkSite.getSiteLevelDetails().getRelationshipType().getPK()==null?"":networkSite.getSiteLevelDetails().getRelationshipType().getPK();
				siteRelationshipDesc=networkSite.getSiteLevelDetails().getRelationshipType().getDescription()==null?"":networkSite.getSiteLevelDetails().getRelationshipType().getDescription();														   
			}
			
			networkName = (networkSite.getNetworkName()==null)?"": networkSite.getNetworkName();
			relationshipPK = networkSiteDetail.get(iX).getRelationshipPK();
			
			NetworkServiceDao networkServiceDao=new NetworkServiceDao();
			
	
			if(AbstractService.IP_ADDRESS_FIELD_VALUE!=null)
			{
				ipAdd=	AbstractService.IP_ADDRESS_FIELD_VALUE;
			}
	
			if(relationshipPK!=0 )
			{
				try
				{
					sql ="select count(*) as countNwPK from er_nwsites where pk_nwsites='"+relationshipPK+"'";
					pstmt = conn.prepareStatement(sql);
				}
				catch(NumberFormatException e)
				{
					this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION," Not a valid number")); 
				}
			rs1=pstmt.executeQuery();
		    while(rs1.next())
			{
				countPK=rs1.getInt("countNwPK");
			}

		  if(countPK>0)
		  {
			    status_Type=nwdao.getNetworkStatusSubtyp(relationshipPK);
				if(status_Type.equalsIgnoreCase("notPending"))
				{
					response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,   MC.M_NtwStnt_Peding)); 
					throw new OperationException();
				}
				else if( nwdao.getStudyNetworkCount(pk_main_network)>0)
				{
					response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, MC.M_NtAss_Stdy)); 
					throw new OperationException();
				}
				else
				{
					pk_main_network=(relationshipPK);
					count=nwdao.deleteNetwork(userId, ipAdd, pk_main_network);
				
					if(count==0)
					{
					response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					response.addIssue(new Issue(IssueTypes.NETWORK_ISSUE, "Network site was not deleted successfully"));
					throw new OperationException();
					}
				}
		  }
		 
		  else
		     {
			  		response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			  		response.addIssue(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "No data found")); 
			  		throw new OperationException();  
		     }
		}
		
	  else if(networkSite.getNetworkName()!=null || networkSite.getNetwork()!=null && networkSite.getSiteLevelDetails()!=null)
	     {
		  sqlbuf4.append ("SELECT distinct(nw.pk_nwsites) AS pk_nwsites FROM er_nwsites nw ,er_site st,ER_CODELST c WHERE nw.fk_site =st.pk_site "
		  +" and st.fk_account= "+iaccId+" AND c.pk_codelst=nw.nw_membertype  ");
		
		  if(networkSite.getNetworkName()!=null && networkSite.getNetwork()==null && networkSite.getSiteLevelDetails()!=null)
		   {
		    sqlbuf4.append (" AND nw.FK_NWSITES_MAIN IN(SELECT nws.pk_nwsites AS pk_nwsites FROM er_nwsites nws , er_site s WHERE nws.fk_site=s.pk_site AND lower(s.site_name)  =trim(lower('"+networkName+"')) AND nws.NW_LEVEL =0 )");
		   }
		  if(networkSite.getNetwork()!=null)
		   {
			  sqlbuf4.append (" AND nw.FK_NWSITES_MAIN="+networkPK+"");
		   }
	
		  if(networkSite.getSiteLevelDetails()!=null)
		  {										
			  if(networkSite.getSiteLevelDetails().getSiteName()!=null)
			  {
				  sqlbuf4.append (" AND lower(st.site_name)  =trim(lower('"+siteName+"'))");
			  }
			  if(networkSite.getSiteLevelDetails().getLevel()!=null)
			  {
				  sqlbuf4.append (" AND nw.NW_LEVEL  ='"+siteLevel+"'");
			  }
			  if(networkSite.getSiteLevelDetails().getRelationshipType().getCode()!=null && networkSite.getSiteLevelDetails().getRelationshipType().getType()!=null)
			  {
				  sqlbuf4.append (" AND c.CODELST_SUBTYP='"+siteRelationshipCode+"'");
			  }
			  if(networkSite.getSiteLevelDetails().getRelationshipType().getType()!=null)
			  {
				  sqlbuf4.append (" AND c.CODELST_TYPE='"+siteRelationshipType+"'");
			  }
			  if(networkSite.getSiteLevelDetails().getRelationshipType().getPK()!=null)
			  {
				  sqlbuf4.append (" AND c.PK_CODELST='"+siteRelationshipPK+"'");
			  }
			  if(networkSite.getSiteLevelDetails().getRelationshipType().getDescription()!=null && networkSite.getSiteLevelDetails().getRelationshipType().getType()!=null)
			  {
				  sqlbuf4.append (" AND c.CODELST_DESC='"+siteRelationshipDesc+"'");
			  }
           }
		 getPrimaryNetwork=sqlbuf4.toString();
			
			
	pstmt = conn.prepareStatement(getPrimaryNetwork);
	rs=pstmt.executeQuery();
	while(rs.next()){
		if(rs.getString("pk_nwsites")!=null){
			pk_main_network=rs.getInt("pk_nwsites");
		}
		
  }
		if(pk_main_network==0 )
		{
			response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			response.addIssue(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "No data found")); 
			throw new OperationException();
		}
		else
		{
			status_Type=nwdao.getNetworkStatusSubtyp(pk_main_network);
			if(status_Type.equalsIgnoreCase("notPending"))
			{
				response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION,   MC.M_NtwStnt_Peding)); 
				throw new OperationException();
			}
			else if( nwdao.getStudyNetworkCount(pk_main_network)>0){
			response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, MC.M_NtAss_Stdy)); 
			throw new OperationException();
			}
			else
			{
			count=nwdao.deleteNetwork(userId, ipAdd, pk_main_network);
			}
			if(count==0)
			{
				response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				response.addIssue(new Issue(IssueTypes.NETWORK_ISSUE, "Network site was not deleted successfully"));
				throw new OperationException();
			}
		}

	}

  }
 }
	catch(OperationException e){
		throw e;
	}
		
	catch (SQLException e) {
		throw	e;
	}
	catch(Exception e){
		throw e;
	}
		finally
		{
			try {
				if(rs != null){
				rs.close();
				}
				if(rs1 != null){
					rs1.close();
				     }
				if(pstmt!=null){
				pstmt.close();
				}
				if(conn!=null)
				{
				conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	
  }
 }
	return count;
}

public int removeNetworkUserSite(String usernetpk,int userId,int accountid, String ipAdd) throws OperationException {
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	int count=0;
	try {
		conn = CommonDAO.getConnection();
		String sqlbuf = "";
		
		
			sqlbuf ="select count(PK_NWUSERS) as count from ER_NWUSERS  where PK_NWUSERS=?";
			System.out.println("sqlbuf"+sqlbuf);
			pstmt = conn.prepareStatement(sqlbuf);
	        pstmt.setInt(1, EJBUtil.stringToNum(usernetpk));
	        
	         rs = pstmt.executeQuery();
	        if(rs.next()){
	        	count=rs.getInt("count");
	        	if(count!=0){
	        count=deleteUserNetworkWS(EJBUtil.stringToNum(usernetpk),userId,ipAdd);
	        	
	        	}
	        	}}


catch(Exception e){
	e.printStackTrace();
}
	return count;
}
public int removeNetworkUserSite(String logName,int userId,String useroid,int relPkint, int accountid, String ipAdd) throws OperationException {
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	int pk_nwuser=0;
	int count=0;
	try {
		conn = CommonDAO.getConnection();
		String sqlbuf = "";
		
			sqlbuf ="Select PK_NWUSERS from ER_NWUSERS,ER_USER where pk_user=fk_user and FK_NWSITES=? and (pk_user =? or usr_logname='"+logName+"' or pk_user = (select table_pk from ER_OBJECT_MAP where SYSTEM_ID='"+useroid+"' and TABLE_NAME='er_user'  )) and fk_account=?";
			System.out.println("sqlbuf"+sqlbuf);
			System.out.println("relPkint=="+relPkint);
			System.out.println("userId=="+userId);
			System.out.println("logName=="+logName);
			System.out.println("useroid=="+useroid);
			System.out.println("accountid=="+accountid);
			pstmt = conn.prepareStatement(sqlbuf);
	        pstmt.setInt(1,relPkint);
	        pstmt.setInt(2,userId);
	        pstmt.setInt(3,accountid);
	         rs = pstmt.executeQuery();
	        if(rs.next()){
	        	pk_nwuser=rs.getInt("PK_NWUSERS");
	        	System.out.println("pk_nwuser==="+pk_nwuser);
	        	count=deleteUserNetworkWS(pk_nwuser,userId,ipAdd);
	        	System.out.println("After call count==="+count);
	        	
	        }
	        			}


catch(Exception e){
	e.printStackTrace();
}
	return count;
}
public int removeNetworkUserSite(String logName,int userId,String useroid , String netOid,String netName,String netPk, String siteName,int level, String codelttyp,String subtyp,String pk_codelst, int accountid, String ipAdd) throws OperationException {
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	int pkcodelst=0;
	int networkPk=0;
	int count = 0;
	if(netPk.equals("")){
		networkPk=networkPk;
	}else{
		networkPk=EJBUtil.stringToNum(netPk);
	}
	if(pk_codelst.equals("")){
		pkcodelst=pkcodelst;
	} else{
		pkcodelst=EJBUtil.stringToNum(pk_codelst);
	}
	int pk_nwuser=0;
	int pk_nwsite=0;
	int newlevel = level;
	try {
		conn = CommonDAO.getConnection();
		String sqlbuf = "";
		
			sqlbuf ="SELECT DISTINCT(nw.pk_nwsites) AS pk_nwsites FROM er_nwsites nw , er_site st,ER_CODELST c ";
			sqlbuf+= " WHERE nw.fk_site=st.pk_site AND st.site_name=? AND ";
			sqlbuf+= " nw.FK_NWSITES_MAIN in (SELECT nws.pk_nwsites AS pk_nwsites FROM er_nwsites nws , er_site s WHERE nws.fk_site=s.pk_site  AND (s.site_name  =? or nws.PK_NWSITES=? or pk_nwsites =((select table_pk from ER_OBJECT_MAP where SYSTEM_ID=? and TABLE_NAME='er_nwsites'  ))) AND nws.NW_LEVEL =0)";
			sqlbuf+= " AND nw.NW_LEVEL =? AND c.pk_codelst=nw.nw_membertype  AND (c.CODELST_TYPE=? AND c.CoDELST_SUBTYP =? or c.pk_codelst=?)  AND st.fk_account =? ";
			System.out.println("sqlbuf"+sqlbuf);
			pstmt = conn.prepareStatement(sqlbuf);
	        pstmt.setString(1,siteName);
	        pstmt.setString(2,netName);
	        pstmt.setInt(3,networkPk);	
	        pstmt.setString(4,netOid);
	        pstmt.setInt(5,newlevel);
	        pstmt.setString(6,codelttyp);
	        pstmt.setString(7,subtyp);
	        pstmt.setInt(8,pkcodelst);
	        pstmt.setInt(9,accountid);
	        System.out.println("siteName==="+siteName);
	        System.out.println("netName==="+netName);
	        System.out.println("networkPk==="+networkPk);
	        System.out.println("netOid==="+netOid);
	        System.out.println("newlevel==="+newlevel);
	        System.out.println("codelttyp==="+codelttyp);
	        System.out.println("subtyp==="+subtyp);
	        System.out.println("pkcodelst==="+pkcodelst);
	        System.out.println("accountid==="+accountid);
	         rs = pstmt.executeQuery();
	        while(rs.next()){
	        	pk_nwsite=rs.getInt("pk_nwsites");
	        	
	        count=removeNetworkUserSite(logName,userId,useroid,pk_nwsite,accountid,ipAdd);
	        }}


catch(Exception e){
	e.printStackTrace();
	
}
	return count;
}
public int  deleteUserNetworkWS(int nwUserPk,int userId,String ipAdd){
	CallableStatement cstmt = null;
    Connection conn = null; 
    int count=0;
    
     try{
    System.out.println("Inside try");
   	conn  = EnvUtil.getConnection();
   	cstmt = conn.prepareCall("{call DELETE_NETWORK(?,?,?,?)}");
   	
   	cstmt.setInt(1,nwUserPk);
   	cstmt.setInt(2,userId);
   	cstmt.setString(3,ipAdd);
   	cstmt.setString(4, "NU");
   	cstmt.execute();
   	count=1;
   	Rlog.debug("network", "NetworkServiceDao.deleteUserNetworkWS()");
    
     }
     catch(Exception e) {
   			Rlog.fatal("Network","EXCEPTION in DELETE_NETWORK_USER, excecuting Stored Procedure " + e);
   			e.printStackTrace();
     }
     finally{
   		try
   		{
   			if (cstmt!= null) cstmt.close();
   		}
   		catch (Exception e) 
   		{
   		}
   		try 
   		{
   			if (conn!=null) conn.close();
   		}
   		catch (Exception e) {}
     
     }
     System.out.println("count==="+count);
     return count;
}

public String createNetworkSite(NetworkParentSite network, List<Site> site, Map<String, Object> parameters) throws OperationException {
	// TODO Auto-generated method stub
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	Connection conn1=null;
	PreparedStatement pstmt1 = null;
	ResultSet rs1 = null;
	ResponseHolder networkResponse = (ResponseHolder)parameters.get("ResponseHolder");
	int count=0;
	String iaccId = (String)parameters.get(KEY_ACCOUNT_ID);
	String savedNtwIds = "";
	UserBean userBean=(UserBean)parameters.get("callingUser");
	UserBean userBeanPk = null;
	UserAgentRObj userAgent = (UserAgentRObj) parameters.get("userAgent");
	
	if(network.getNetworkIdentifier()==null && network.getNetworkName()==null && network.getRelationshipPK()==null){
		networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid networkIdentifier is required")); 
		throw new OperationException();	
	}
	
	StringBuffer sqlbuf = new StringBuffer();
	String FK_NWSITES_MAIN="";
	String str="";
	String networkPK = "";
	String relationshipPK = "";
	String networkName = "";
	if(network.getNetworkIdentifier()!=null){
		if(network.getNetworkIdentifier().getPK()!=null){
			networkPK = String.valueOf(network.getNetworkIdentifier().getPK());
		}else if(network.getNetworkIdentifier().getOID()!=null){
			networkPK = String.valueOf(((ObjectMapService)parameters.get("objectMapService")).getObjectPkFromOID(network.getNetworkIdentifier().getOID()));
		}
	}else if(network.getNetworkName()!=null){
		networkPK = getNetworkIdbyName(network.getNetworkName());
	}
	
	if(network.getRelationshipPK()!=null){
		relationshipPK = network.getRelationshipPK();
	}
		

if(network.getParentSiteLevelDetails()!=null || !relationshipPK.equals("") || !networkPK.equals("")){

	String siteName="";
	String relationshipType = "";
	String level = "";
	String relationType ="";
	if(network.getParentSiteLevelDetails()!=null){
	if(network.getParentSiteLevelDetails().getSiteName()!=null &&
			!network.getParentSiteLevelDetails().getSiteName().equals("")){
		
		siteName = network.getParentSiteLevelDetails().getSiteName();
		System.out.println("");
	}
	if(network.getParentSiteLevelDetails().getSiteRelationShipType()!=null){
		relationType = String.valueOf(getCodeByMultiInput(network.getParentSiteLevelDetails().getSiteRelationShipType(),"relnshipTyp"));
		if(relationType.equals(0)){
			networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "siteRelationshipType input is not valid")); 
			throw new OperationException();
		}
	}
	if(network.getParentSiteLevelDetails().getLevel()!=null){
		level = network.getParentSiteLevelDetails().getLevel();
	}
	}
	System.out.println("networkPK==="+networkPK);
	System.out.println("relationShipPK==="+relationshipPK);
	String str1 = "select distinct nvl(fk_nwsites_main,pk_nwsites) as fk_nwsites_main, pk_nwsites as relationshipPK, nw_level "
			+ "from er_nwsites,er_site,er_codelst "+
				  "where pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account= ?";
	
	
	
	if(!networkPK.equals("") && !networkPK.equals(relationshipPK))
	{
		if(!relationshipPK.equals("") || (network.getParentSiteLevelDetails()!=null && !level.equals("0"))){
			str1=str1+" and fk_nwsites_main in("+networkPK+")";
		}else{
			str1=str1+" and pk_nwsites in("+networkPK+") ";
		}
	}	
	if(!siteName.equals("")){
		str1=str1+" and lower(site_name) like lower('"+siteName+"')";
	}	
	if(!level.equals("")){
		str1=str1+" and nw_level="+level;
	}
	if(!relationType.equals("")){
		str1=str1+" and NW_MEMBERTYPE="+relationType;
	}
	if(!relationshipPK.equals("")){
		str1=str1+" and pk_nwsites="+relationshipPK;
	}
try {
	System.out.println("str1==="+str1.toString());
	System.out.println("iaccId==="+iaccId);
conn = CommonDAO.getConnection();
pstmt = conn.prepareStatement(str1);
pstmt.setInt(1, StringUtil.stringToNum(iaccId));
rs = pstmt.executeQuery();
Code code=null;
Code code1=null;
while (rs.next()) {
	
	fkSiteList.add(String.valueOf(rs.getInt("relationshipPK")));
	pkNetList.add(rs.getString("fk_nwsites_main"));
	netLevelList.add(String.valueOf(rs.getInt("nw_level")));
	System.out.println("Inside loop");
	count++;
}
System.out.println("count==="+count);
}catch (SQLException e) {
	e.printStackTrace();
}

finally{
try {
	rs.close();
	if(rs1 != null){
	rs1.close();
     }
	pstmt.close();
	conn.close();
} catch (SQLException e) {
	e.printStackTrace();
}
}
if(count>0){
	if(site== null || site.size()==0){
		networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No site information provided to create")); 
		throw new OperationException();	
	}
	
for(int i=0;i<pkNetList.size();i++){
	String pkNetwork= pkNetList.get(i);
	
	String relationPK = "";
	if(relationshipPK.equals("")){
		relationPK= fkSiteList.get(i);
	}else{
		relationPK= relationshipPK;
	}
	int nwLevel= Integer.parseInt(netLevelList.get(i))+1;
	Integer netType = null;
	Integer netStatus = null;
	
	UserBean userObj = (UserBean)parameters.get("callingUser");
	for(Site s: site){
		if(s.getSiteIdentifier()==null && s.getName()==null){
			networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			networkResponse.addIssue(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "Valid site identifier or name of the site is required")); 
			throw new OperationException();	
		}
		
		int pkSite = getPKSiteFromIdentifier(s, StringUtil.stringToNum(iaccId),"createNetworkSite");
		
		
		
		if(pkSite==0){
			networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			networkResponse.addIssue(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "Valid siteIdentifier or name of the site is required")); 
			throw new OperationException();
		}
		
		if(s.getSiteRelationShipType()!=null){
			netType=getCodeByMultiInput(s.getSiteRelationShipType(),"relnshipTyp");
			if(netType.intValue()==0){
				networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "siteRelationshipType input is not valid")); 
				throw new OperationException();
			}
		}
		if(s.getSiteStatus()!=null){
			netStatus=getCodeByMultiInput(s.getSiteStatus(),"networkstat");
			if(netStatus.intValue()==0){
				networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "siteStatus input is not valid")); 
				throw new OperationException();
			}
		}
		if(s.getSiteStatusDate()!=null){
			if(DateUtil.stringToDate(s.getSiteStatusDate(), null)==null){
				networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "SiteStatusDate format is incorrect")); 
				throw new OperationException();
			}
		}
		
		if(s.getMoreSiteDetailsFields()!=null){
			
		for (NVPair nv :s.getMoreSiteDetailsFields()){
			int mdId = 0;
			MoreDetailsBean mdbMain = new MoreDetailsBean();
			if(nv.getKey()!=null){
				String type= "org_"+nwLevel;
				Code code = new Code();
				code.setCode(nv.getKey());
				code.setType(type);
				mdId = getCodeByMultiInput(code,type).intValue();
				
				if(mdId==0){
					networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Key mentioned in MoreSiteDetailsField is not valid")); 
					throw new OperationException();
				}
			}
		}
	}
	}
	saveMultipleNtwSites(pkNetwork,relationPK,nwLevel,site,parameters,networkResponse);
	
	}
}else{
	networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
	networkResponse.addIssue(new Issue(IssueTypes.NETWORK_NOT_FOUND, "No Networks found")); 
	throw new OperationException();
}

}

return savedNtwIds;
}
private String saveMultipleNtwSites(String pkNetwork, String relationPK, int nwLevel, List<Site> site, Map<String, Object> parameters,ResponseHolder networkResponse) throws OperationException {
	String savedNtwIds ="";
	String iaccId = (String)parameters.get(KEY_ACCOUNT_ID);
	ArrayList<String> sitesToInsert = new ArrayList<String>();
	for(Site s: site){
		sitesToInsert = new ArrayList<String>();
		
		
		int pkSite=getPKSiteFromIdentifier(s,StringUtil.stringToNum(iaccId),"createNetworkSite");
		
		
		
			SiteJB sJB = new SiteJB();
			sJB.setSiteId(pkSite);
			SiteBean sb=sJB.getSiteDetails();
			String siteName = "";
			if(sb.getSiteName()!=null && !sb.getSiteName().equals("")){
				siteName = sb.getSiteName();
			}
		System.out.println("pkSite==-="+String.valueOf(pkSite));
		System.out.println("siteName===="+siteName);
		sitesToInsert.add(String.valueOf(pkSite)); 
		
		Integer netType = 0;
		Integer netStatus = 0;
		if(s.getSiteRelationShipType()!=null){
			netType=getCodeByMultiInput(s.getSiteRelationShipType(),"relnshipTyp");
			
		}
		if(s.getSiteStatus()!=null){
			netStatus=getCodeByMultiInput(s.getSiteStatus(),"networkstat");
		}else{
			//Setting pending as default status when status not provided in the input 
			Code code = new Code();
			code.setType("networkstat");
			code.setCode("pending");
			netStatus=getCodeByMultiInput(code,"networkstat");
		}
		
		
		String[] sitesArr = sitesToInsert.stream().toArray(String[]::new);
		
		NetworkDao ndao = new NetworkDao();
		ndao.setChld_ntw_ids(null);
		UserBean user = (UserBean)parameters.get("callingUser");
		ndao.saveMultipleNetwork(sitesArr, String.valueOf(nwLevel), String.valueOf(user.getUserId()), "", String.valueOf(netType), pkNetwork, relationPK, String.valueOf(netStatus));
		
		
		if(s.getMoreSiteDetailsFields()!=null){
		saveMoreSiteDetails(s.getMoreSiteDetailsFields(),ndao.getChld_ntw_ids(),nwLevel,user );
		}
		
		if(ndao.getChld_ntw_ids()!=null && !ndao.getChld_ntw_ids().equals("") &&
			
				(s.getSiteStatusDate()!=null || s.getSiteStatusNotes()!=null)){
			updateNetStatus(ndao.getChld_ntw_ids(),netStatus,s.getSiteStatusDate(),s.getSiteStatusNotes());
		}
		if(ndao.getChld_ntw_ids()!=null && !ndao.getChld_ntw_ids().equals("")){
			ObjectMap map = ((ObjectMapService) parameters.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_NETWORK, StringUtil.stringToNum(ndao.getChld_ntw_ids()));
			OrganizationIdentifier oid = new OrganizationIdentifier();
			oid.setOID(map.getOID());
			oid.setPK(Integer.parseInt(ndao.getChld_ntw_ids()));
			
			networkResponse.addAction(new CompletedAction(oid,siteName,String.valueOf(nwLevel), CRUDAction.CREATE));
		 
		}
			
		}
	return savedNtwIds;
}
private int getPKSiteFromIdentifier(Site s, int accountId, String calledFrom){
	int pkSite = 0;
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	String siteQuery = "select PK_SITE from ER_SITE where fk_account="+accountId;
	if(s.getSiteIdentifier()!=null){
		if(s.getSiteIdentifier().getPK()!=null && !s.getSiteIdentifier().getPK().equals("")){
			siteQuery += " and PK_SITE="+s.getSiteIdentifier().getPK();
		}
		if(s.getSiteIdentifier().getOID()!=null && !s.getSiteIdentifier().getOID().equals("")){
			siteQuery += " and PK_SITE=(select table_pk from er_object_map where system_id='"+s.getSiteIdentifier().getOID()+"' and table_name='er_site')";
		}
	}
	if(calledFrom.equals("createNetworkSite")){
		if(s.getName()!=null && !s.getName().equals("")){
			siteQuery += " and lower(site_name)=lower('"+s.getName()+"')";
		}
	}else{
		if(s.getSiteName()!=null && !s.getSiteName().equals("")){
			siteQuery += " and lower(site_name)=lower('"+s.getSiteName()+"')";
		}
	}
	System.out.println("siteQuery=="+siteQuery);
	try{
		conn = CommonDAO.getConnection();
		pstmt = conn.prepareStatement(siteQuery);
		rs = pstmt.executeQuery();
		while (rs.next()) {
			pkSite = rs.getInt("PK_SITE");
		}
	}catch (SQLException e) {
		e.printStackTrace();
	}

	finally{
	try {
		rs.close();
		
		pstmt.close();
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
	}
	return pkSite;
	
}
private void updateNetStatus(String chld_ntw_ids, Integer netStatus, String siteStatusDate, String siteStatusNotes) {
	
	StatusHistoryAgentBean sBean = new StatusHistoryAgentBean();
	StatusHistoryDao shd =sBean.getStatusHistoryInfo(Integer.valueOf(chld_ntw_ids), "er_nwsites");
	System.out.println("statusId size==="+shd.getStatusIds().size());
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	for (Object o:shd.getStatusIds()){
		StatusHistoryBean shb = new StatusHistoryBean();
		String str1 = "update ER_STATUS_HISTORY set STATUS_DATE=? , STATUS_NOTES=? where PK_STATUS=?";
		Integer statusId = (Integer) o;
		
		conn = CommonDAO.getConnection();
		try {
		pstmt = conn.prepareStatement(str1);
		Date startDate = DateUtil.stringToDate(siteStatusDate);
		
		pstmt.setDate(1, DateUtil.dateToSqlDate(startDate));
		pstmt.setString(2, siteStatusNotes);
		pstmt.setInt(3, statusId);
		int success=pstmt.executeUpdate();
		System.out.println("success==="+success);
		} catch (SQLException e) {
			System.out.println("Inside catch1");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				System.out.println("Inside catch2");
				e.printStackTrace();
			}
			}
		
	}
	
}
public int saveMoreSiteDetails(List<NVPair> moreSiteList,String childID,int nwLevel,UserBean user ){
	int success = 0;
	for (NVPair nv :moreSiteList){
		int mdId = 0;
		MoreDetailsBean mdbMain = new MoreDetailsBean();
		if(nv.getKey()!=null){
			String type= "org_"+nwLevel;
			Code code = new Code();
			code.setCode(nv.getKey());
			code.setType(type);
			System.out.println("mdId = getCodeByMultiInput(code,type).intValue():::"+nv.getKey()+"   "+type);
			mdId = getCodeByMultiInput(code,type).intValue();
			
				MoreDetailsBean mdbTemp = new MoreDetailsBean();
				String value ="";
				ElementNSImpl val=(ElementNSImpl)nv.getValue();
				if(val!=null){
				NodeList node= val.getChildNodes();
		        for(int j=0;j< node.getLength();j++)
		           {
		        	  Node node1=node.item(j);
		        	  value=node1.getNodeValue();
		           }
				}
				if   ( !StringUtil.isEmpty (value))
				{
					mdbTemp.setModId(childID) ;
					mdbTemp.setModName(type) ;
					mdbTemp.setModElementId(String.valueOf(mdId));
					mdbTemp.setModElementData(value);
					mdbTemp.setRecordType("N");
					mdbTemp.setCreator(String.valueOf(user.getUserId()));
					
					mdbMain.setMoreDetailBeans(mdbTemp);
				}
			}
			MoreDetailsJB moreDetails = new MoreDetailsJB();
			
			 success = moreDetails.createMultipleMoreDetails(mdbMain,"-1");
			}
	return success;
}
private String getNetworkIdbyName(String name) {
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	String ntwId = "";
	String str="select pk_nwsites from ER_NWSITES,er_site where pk_site=fk_site and lower(site_name)=lower('"+name+"') and nw_level=0";
	try {
		System.out.println("str==="+str.toString());
		
	conn = CommonDAO.getConnection();
	pstmt = conn.prepareStatement(str);
	rs = pstmt.executeQuery();
	
	while (rs.next()) {
		if(ntwId.equals("")){
			ntwId=String.valueOf(rs.getInt("pk_nwsites"));
		}else{
			ntwId=ntwId+","+String.valueOf(rs.getInt("pk_nwsites"));
		}
	}
	System.out.println("ntwId==="+ntwId);
	}catch (SQLException e) {
		e.printStackTrace();
	}

	finally{
	try {
		rs.close();
		
		pstmt.close();
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
	}
	return ntwId;
}
public Integer getCodeByMultiInput(Code code, String type){
	boolean invalidCodeFlag = true;
	CodeDao cdao = new CodeDao();
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	int rTypePK=0;
	StringBuffer str=new StringBuffer();
	str.append("select PK_CODELST,CODELST_TYPE from er_CODELST WHERE CODELST_HIDE <> 'Y' ");
	if(code.getPK()!=null && !code.getPK().equals("")){
		str.append(" and PK_CODELST="+code.getPK());
	}
	if(code.getType()!=null && !code.getType().equals("")){
		str.append(" and trim(CODELST_TYPE)='"+code.getType()+"'");
	}
	if(code.getCode()!=null && !code.getCode().equals("")){
		str.append(" and trim(CODELST_SUBTYP)='"+code.getCode()+"'");
	}
	if(code.getDescription()!=null && !code.getDescription().equals("")){
		str.append(" and trim(CODELST_DESC)='"+code.getDescription()+"'");
	}
	System.out.println(str.toString());
	try{
	conn = CommonDAO.getConnection();
	pstmt = conn.prepareStatement(str.toString());
	rs = pstmt.executeQuery();
	int rows=0;
	while (rs.next()) {
		String codelstType = "";
		if(rs.getString("CODELST_TYPE")!=null && !rs.getString("CODELST_TYPE").equals("")){
			codelstType = rs.getString("CODELST_TYPE");
		}
		if(codelstType.equals(type)){
			rTypePK =rs.getInt("PK_CODELST");
		}
		rows++;
	}
	System.out.println("ntwId==="+rTypePK);
	if(rows==0){
		rTypePK=0;
	}
	}catch (SQLException e) {
		e.printStackTrace();
	}

	finally{
	try {
		rs.close();
		
		pstmt.close();
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
	}
	return rTypePK;
}

public void updateNetworkSite(UpdatNetworkSiteDetails updateNetworkSiteDetail,HashMap<String, Object> params) throws OperationException{
	
	UserBean userBean=(UserBean)params.get("callingUser");
	int userId=userBean.getUserId();
	ArrayList<NetSiteDetails> updateNetworkSite=(ArrayList<NetSiteDetails>) updateNetworkSiteDetail.getNetworkSiteDetailsList();
	UserServiceHelper userServiceHelper=null;
	String pk_network="";
	NetSiteDetails networkDetails = null;
	//String pkRelationshipType=(String)params.get("pkRelationshipType");
	//String pkNewRelationshipType=(String)params.get("pkNewRelationshipType");
	//String pkStatus = (String) params.get("pkStatus");
	String iaccId = (String)params.get(KEY_ACCOUNT_ID);
	String ipAdd=	AbstractService.IP_ADDRESS_FIELD_VALUE;
	response = (ResponseHolder)params.get("ResponseHolder");
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	int count=0;
	String pk_main_network="";
	String siteLevel="";
	String sitePK="";
	String siteName="";
	String flag="updateNetworkSite";
	
	int dataFound=validateExistingData(updateNetworkSiteDetail,params,flag);
	
	try {
		conn = CommonDAO.getConnection();
		StringBuffer sqlbuff = null;
		if(updateNetworkSite!=null && updateNetworkSite.size()>0){
			//this.response=new ResponseHolder();
		for(int i=0;i<updateNetworkSite.size();i++){
			networkDetails=updateNetworkSite.get(i);
			if(networkDetails.getRelationshipPK()!=null){
				sqlbuff = new StringBuffer();
				sqlbuff.append("SELECT pk_nwsites,NW_LEVEL,fk_site FROM er_nwsites,er_site WHERE er_site.fk_account=? and pk_site=fk_site and pk_nwsites=?");
			}
			else if((networkDetails.getNetworkIdentifier()!=null || networkDetails.getNetworkName()!=null) && networkDetails.getSiteLevelDetail()!=null){
				
				if(networkDetails.getNetworkIdentifier()!=null){
					if(networkDetails.getNetworkIdentifier().getNetworkIdentfier().getPK()!=null){
						pk_main_network=(networkDetails.getNetworkIdentifier().getNetworkIdentfier().getPK()==null)?"":networkDetails.getNetworkIdentifier().getNetworkIdentfier().getPK().toString();
					}
					else if(networkDetails.getNetworkIdentifier().getNetworkIdentfier().getOID()!=null){
						pk_main_network = String.valueOf(((ObjectMapService)params.get("objectMapService")).getObjectPkFromOID(networkDetails.getNetworkIdentifier().getNetworkIdentfier().getOID()));
					}
				}
				
				sqlbuff = new StringBuffer();
				sqlbuff.append(" SELECT DISTINCT NVL(FK_NWSITES_MAIN,pk_nwsites) fk_nwsites_main,pk_nwsites,nw_level,fk_site FROM er_nwsites,er_site WHERE er_site.fk_account=? and pk_site=fk_site ");
				
				if(!(pk_main_network.equals(""))){
					sqlbuff.append(" and nvl(FK_NWSITES_MAIN,pk_nwsites)="+pk_main_network+"");
				}
				
				if(networkDetails.getNetworkName()!=null && pk_main_network.equals("")){
					sqlbuff.append(" and nvl(FK_NWSITES_MAIN,pk_nwsites) in (select pk_nwsites from er_nwsites,er_site where pk_site=fk_site and lower(site_name) like lower('"+networkDetails.getNetworkName()+"') and nw_level=0)" );
				}
				
				if(networkDetails.getSiteLevelDetail()!=null){
					sqlbuff.append(" and pk_nwsites in (SELECT pk_nwsites FROM er_nwsites,er_site WHERE er_site.fk_account = ?");
					sqlbuff.append(" and pk_site=fk_site AND lower(site_name) LIKE lower('"+networkDetails.getSiteLevelDetail().getSiteName()+"') AND nw_level=? AND NW_MEMBERTYPE="+params.get("pkRelationshipType_"+i)+")");
				}
				
			}
				pstmt = conn.prepareStatement(sqlbuff.toString());
				pstmt.setInt(1, StringUtil.stringToNum(iaccId));
				if(networkDetails.getRelationshipPK()!=null){
					pstmt.setInt(2, StringUtil.stringToNum(networkDetails.getRelationshipPK()));
				}
				else{
				pstmt.setInt(2, StringUtil.stringToNum(iaccId));
				pstmt.setInt(3, StringUtil.stringToNum(networkDetails.getSiteLevelDetail().getLevel()));
				}
				rs=pstmt.executeQuery();
				
				while(rs.next()){
					pk_network=String.valueOf(rs.getInt("pk_nwsites"));
					siteLevel=String.valueOf(rs.getInt("NW_LEVEL"));
					sitePK = String.valueOf(rs.getInt("fk_site"));
					count++;
				}
				if(networkDetails.getSiteLevelDetail()!=null && networkDetails.getSiteLevelDetail().getSiteName()!=null && !networkDetails.getSiteLevelDetail().getSiteName().equals("")){
					siteName=networkDetails.getSiteLevelDetail().getSiteName();
				}else{
					SiteJB sJB = new SiteJB();
					sJB.setSiteId(Integer.parseInt(sitePK));
					SiteBean sb=sJB.getSiteDetails();
					if(sb.getSiteName()!=null && !sb.getSiteName().equals("")){
						siteName = sb.getSiteName();
					}
				}
				System.out.println("count==="+count);
				if(count==0){
					response.addAction(new CompletedAction("No Networks found to be updated", CRUDAction.UPDATE));
				}
				System.out.println("siteLevel==="+siteLevel);
				
			if(!pk_network.equals(""))
			{
				if(networkDetails.getNewSiteRelationshipType()!=null){
					sqlbuff = new StringBuffer();
					sqlbuff.append("update er_nwsites set NW_MEMBERTYPE=?,LAST_MODIFIED_BY=? where PK_NWSITES=?");
					pstmt = conn.prepareStatement(sqlbuff.toString());
					pstmt.setInt(1, StringUtil.stringToNum((String)params.get("pkNewRelationshipType_"+i)));
					pstmt.setInt(2, userId);
					pstmt.setInt(3, StringUtil.stringToNum(pk_network));
					int tmpCount=pstmt.executeUpdate();
				}
				
				if(networkDetails.getSiteStatus()!=null){
					sqlbuff = new StringBuffer();
					sqlbuff.append("update er_status_history set STATUS_END_DATE=sysdate,LAST_MODIFIED_DATE=sysdate,STATUS_ISCURRENT=0 where STATUS_MODPK=? and STATUS_END_DATE is null");
					pstmt = conn.prepareStatement(sqlbuff.toString());
					pstmt.setInt(1, StringUtil.stringToNum(pk_network));
					count=pstmt.executeUpdate();
					if(count>0){
						sqlbuff = new StringBuffer();
						java.util.Date statusEnteredDate=DateUtil.stringToDate(networkDetails.getSiteStatusDate(), null);
						sqlbuff.append("INSERT INTO er_status_history(PK_STATUS,STATUS_MODPK,STATUS_MODTABLE,FK_CODELST_STAT,STATUS_DATE,STATUS_NOTES,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD,STATUS_ISCURRENT) ");
						sqlbuff.append("VALUES(SEQ_ER_STATUS_HISTORY.nextval,?,?,?,?,?,?,'N',sysdate,?,?)");
						pstmt = conn.prepareStatement(sqlbuff.toString());
						pstmt.setInt(1, StringUtil.stringToNum(pk_network));
						pstmt.setString(2,"er_nwsites");
						pstmt.setInt(3, StringUtil.stringToNum((String)params.get("pkStatus_"+i)));
						pstmt.setDate(4, DateUtil.dateToSqlDate(statusEnteredDate));
						pstmt.setString(5,networkDetails.getSiteStatusNotes());
						pstmt.setInt(6, userId);
						pstmt.setString(7, ipAdd);
						pstmt.setInt(8, 1);
						int tmpCount=pstmt.executeUpdate();
						if(count>0){
							NetworkDao ntDao=new NetworkDao();
							ntDao.saveNetworkStatusUpdate(StringUtil.stringToNum(pk_network),StringUtil.stringToNum((String)params.get("pkStatus_"+i)),userId);
							//count=ntDao.saveNetworkStatus(StringUtil.stringToNum(pk_network),StringUtil.stringToNum((String)params.get("pkStatus_"+i)),networkDetails.getSiteStatusDate(),networkDetails.getSiteStatusNotes());
						}
					  }
		    		}
				
				else if(networkDetails.getSiteStatusDate()!=null || networkDetails.getSiteStatusNotes()!=null){
					sqlbuff = new StringBuffer();
					sqlbuff.append("update er_status_history set status_date=to_date('"+networkDetails.getSiteStatusDate()+"','mm/dd/yyyy'), status_notes=? where STATUS_MODPK=? and STATUS_MODTABLE='er_nwsites' and STATUS_ISCURRENT=1 ");
					pstmt = conn.prepareStatement(sqlbuff.toString());
					pstmt.setString(1, networkDetails.getSiteStatusNotes());
					pstmt.setInt(2,StringUtil.stringToInteger(pk_network));
					int tmpCount=pstmt.executeUpdate();
				}

				if(networkDetails.getMoreSiteDetails()!=null){
						userServiceHelper=new UserServiceHelper();
						userServiceHelper.persistsMoreDetails(networkDetails.getMoreSiteDetails(),StringUtil.stringToNum(pk_network),userBean,params,CodeCache.CODE_TYPE_ORG+"_"+siteLevel);
				}			
			}
			ObjectMap map = ((ObjectMapService) params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_NETWORK, StringUtil.stringToNum(pk_network));
			OrganizationIdentifier oid = new OrganizationIdentifier();
			
			oid.setOID(map.getOID());
			oid.setPK(Integer.parseInt(pk_network));
			
			
			response.addAction(new CompletedAction(oid,siteName,siteLevel, CRUDAction.UPDATE));
		}
		
		
	}
	
	}catch(OperationException ex){
		throw ex;
	}catch(SQLException ex){
		ex.printStackTrace();
	}catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			if(rs!=null)
			rs.close();
			if(pstmt!=null)
			pstmt.close();
			if(conn!=null)
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}

public int validateExistingData(Object object,HashMap<String, Object> params,String flag)throws OperationException{
	
	UpdatNetworkSiteDetails updateNetworkSiteDetail=null;
	ArrayList<NetSiteDetails> updateNetworkSite=null;
	UpdateNetwork updateNetworkDetails=null;
	ArrayList<NetSiteDetails> updateNetworks=null;
	if(flag.equalsIgnoreCase("updateNetworkSite")){
		updateNetworkSiteDetail=UpdatNetworkSiteDetails.class.cast(object);
		updateNetworkSite=(ArrayList<NetSiteDetails>) updateNetworkSiteDetail.getNetworkSiteDetailsList();
	}
	else if(flag.equalsIgnoreCase("updateNetwork")){
		updateNetworkDetails=UpdateNetwork.class.cast(object);
		updateNetworks=(ArrayList<NetSiteDetails>) updateNetworkDetails.getNetworkSiteDetailsList();
	}
	String pk_network="";
	NetSiteDetails networkDetails = null;
	//String pkRelationshipType=(String)params.get("pkRelationshipType");
	String iaccId = (String)params.get(KEY_ACCOUNT_ID);
	ResponseHolder responseHolder = (ResponseHolder)params.get("ResponseHolder");
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	int count=0;
	String pk_main_network="";
	String siteLevel="";
	
	try{
		
	StringBuffer sqlbuff = null;
	if(updateNetworkSite!=null && updateNetworkSite.size()>0 && flag.equalsIgnoreCase("updateNetworkSite")){
		conn = CommonDAO.getConnection();
	for(int i=0;i<updateNetworkSite.size();i++){
		networkDetails=updateNetworkSite.get(i);
		pk_main_network="";
		if(networkDetails.getRelationshipPK()!=null){
			sqlbuff = new StringBuffer();
			sqlbuff.append("SELECT pk_nwsites,NW_LEVEL FROM er_nwsites,er_site WHERE er_site.fk_account=? and pk_site=fk_site and pk_nwsites=?");
		}
		else if((networkDetails.getNetworkIdentifier()!=null || networkDetails.getNetworkName()!=null) && networkDetails.getSiteLevelDetail()!=null){
			
			if(networkDetails.getNetworkIdentifier()!=null){
				if(networkDetails.getNetworkIdentifier().getNetworkIdentfier().getPK()!=null){
					pk_main_network=(networkDetails.getNetworkIdentifier().getNetworkIdentfier().getPK()==null)?"":networkDetails.getNetworkIdentifier().getNetworkIdentfier().getPK().toString();
				}
				else if(networkDetails.getNetworkIdentifier().getNetworkIdentfier().getOID()!=null){
					pk_main_network = String.valueOf(((ObjectMapService)params.get("objectMapService")).getObjectPkFromOID(networkDetails.getNetworkIdentifier().getNetworkIdentfier().getOID()));
				}
			}
			
			sqlbuff = new StringBuffer();
			sqlbuff.append(" SELECT DISTINCT NVL(FK_NWSITES_MAIN,pk_nwsites) fk_nwsites_main,pk_nwsites,nw_level FROM er_nwsites,er_site WHERE er_site.fk_account=? and pk_site=fk_site ");
			
			if(!(pk_main_network.equals(""))){
				sqlbuff.append(" and nvl(FK_NWSITES_MAIN,pk_nwsites)="+pk_main_network+"");
			}
			
			if(networkDetails.getNetworkName()!=null && pk_main_network.equals("")){
				sqlbuff.append(" and nvl(FK_NWSITES_MAIN,pk_nwsites) in (select pk_nwsites from er_nwsites,er_site where pk_site=fk_site and lower(site_name) like lower('"+networkDetails.getNetworkName()+"') and nw_level=0)" );
			}
			
			if(networkDetails.getSiteLevelDetail()!=null){
				sqlbuff.append(" and pk_nwsites in (SELECT pk_nwsites FROM er_nwsites,er_site WHERE er_site.fk_account = ?");
				sqlbuff.append(" and pk_site=fk_site AND lower(site_name) LIKE lower('"+networkDetails.getSiteLevelDetail().getSiteName()+"') AND nw_level=? AND NW_MEMBERTYPE="+params.get("pkRelationshipType_"+i)+")");
			}
			
		}
		System.out.println("sql==="+sqlbuff.toString());
			pstmt = conn.prepareStatement(sqlbuff.toString());
			pstmt.setInt(1, StringUtil.stringToNum(iaccId));
			if(networkDetails.getRelationshipPK()!=null){
				pstmt.setInt(2, StringUtil.stringToNum(networkDetails.getRelationshipPK()));
			}
			else{
				pstmt.setInt(2, StringUtil.stringToNum(iaccId));
				pstmt.setInt(3, StringUtil.stringToNum(networkDetails.getSiteLevelDetail().getLevel()));
			}
			rs=pstmt.executeQuery();
			
			while(rs.next()){
				pk_network=String.valueOf(rs.getInt("pk_nwsites"));
				siteLevel=String.valueOf(rs.getInt("NW_LEVEL"));
				count++;
			}
			if(count==0){			
				responseHolder.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				responseHolder.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found"));
					throw new OperationException();
			}
			
			if(networkDetails.getMoreSiteDetails()!=null){		
				for (NVPair nv :networkDetails.getMoreSiteDetails()){
					if(nv.getKey()!=null){
						String type="";
						if(nv.getType()!=null){
							javax.xml.namespace.QName qname=nv.getType();
							type=qname.getLocalPart();
						}
						else{
							type="org_"+siteLevel;
						}
						String subType= nv.getKey();
						CodeDao code = new CodeDao();
						String pkCode = StringUtil.integerToString(code.getCodeId(type, subType ));
						if(pkCode.equals("0")){
							responseHolder.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
							responseHolder.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Key mentioned in MoreSiteDetailsField is not valid")); 
							throw new OperationException();
						}
					}
				}
			}
			count=0;
		}	
	}
	
	else if(updateNetworks!=null && updateNetworks.size()>0 && flag.equalsIgnoreCase("updateNetwork")){

	conn = CommonDAO.getConnection();
	for(int i=0;i<updateNetworks.size();i++){
		networkDetails=updateNetworks.get(i);
		pk_main_network="";
		if((networkDetails.getNetIdentifier()!=null || networkDetails.getNetworkName()!=null)){
			
			if(networkDetails.getNetIdentifier()!=null){
				if(networkDetails.getNetIdentifier().getPK()!=null){
					pk_main_network=(networkDetails.getNetIdentifier().getPK()==null)?"":networkDetails.getNetIdentifier().getPK().toString();
				}
				else if(networkDetails.getNetIdentifier().getOID()!=null){
					pk_main_network = String.valueOf(((ObjectMapService)params.get("objectMapService")).getObjectPkFromOID(networkDetails.getNetIdentifier().getOID()));
				}
			}
			
			sqlbuff = new StringBuffer();
			sqlbuff.append(" SELECT pk_nwsites,nw_level FROM er_nwsites,er_site WHERE er_site.fk_account=? and pk_site=fk_site and nw_level=0 ");
			
			if(!(pk_main_network.equals(""))){
				sqlbuff.append(" and pk_nwsites="+pk_main_network+"");
			}
			
			if(networkDetails.getNetworkName()!=null && pk_main_network.equals("")){
				sqlbuff.append(" and pk_nwsites in (select pk_nwsites from er_nwsites,er_site where pk_site=fk_site and lower(site_name) like lower('"+networkDetails.getNetworkName()+"') and nw_level=0)" );
			}
		}
			pstmt = conn.prepareStatement(sqlbuff.toString());
			pstmt.setInt(1, StringUtil.stringToNum(iaccId));
			
			rs=pstmt.executeQuery();
			
			while(rs.next()){
				pk_network=String.valueOf(rs.getInt("pk_nwsites"));
				siteLevel=String.valueOf(rs.getInt("NW_LEVEL"));
				count++;
			}
			if(count==0){			
				responseHolder.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				responseHolder.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found"));
					throw new OperationException();
			}
			
			else if(count>1){
				responseHolder.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				responseHolder.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Duplicate networks found with same name"));
					throw new OperationException();
			}
			
			if(networkDetails.getMoreSiteDetails()!=null){		
				for (NVPair nv :networkDetails.getMoreSiteDetails()){
					if(nv.getKey()!=null){
						String type="";
						if(nv.getType()!=null){
							javax.xml.namespace.QName qname=nv.getType();
							type=qname.getLocalPart();
						}
						else{
							type="org_"+siteLevel;
						}
						String subType= nv.getKey();
						CodeDao code = new CodeDao();
						String pkCode = StringUtil.integerToString(code.getCodeId(type, subType ));
						if(pkCode.equals("0")){
							responseHolder.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
							responseHolder.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Key mentioned in MoreSiteDetailsField is not valid")); 
							throw new OperationException();
						}
					}
				}
			}
			count=0;
		}	
	}
	
	
	
	
	}catch(OperationException ex){
		throw ex;
	}catch(SQLException ex){
		ex.printStackTrace();
	}
	catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			if(rs!=null)
				rs.close();
			if(pstmt!=null)
				pstmt.close();
			if(conn!=null)
				conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return count;
}
public int checkUserExistence(String userlog, String userpk, String useroid)throws OperationException {
	
	// TODO Auto-generated method stub
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	String ntwId = "";
	int userId = 0;
	String str="select pk_user from er_user";
	if(!userlog.equals("") || !userpk.equals("0") || !useroid.equals("")){
		str=str+ " where ";
	}
	if(!userpk.equals("0")){
		str=str+ "pk_user  ="+userpk;
	}
	if(!userlog.equals("") && !userpk.equals("0")){
		str=str+ " AND ";
	}
	if(!userlog.equals("")){
		str=str+ "usr_logname='"+userlog+"'";
	}
	if((!userlog.equals("") || !userpk.equals("0")) && (!useroid.equals(""))){
		str=str+ " AND ";
	}
	if(!useroid.equals("")){
		str=str+ "pk_user = (SELECT table_pk FROM ER_OBJECT_MAP WHERE SYSTEM_ID='"+useroid+"' AND TABLE_NAME ='er_user')";
	}
			
	try {
		System.out.println("str==="+str.toString());
		
	conn = CommonDAO.getConnection();
	pstmt = conn.prepareStatement(str);
	rs = pstmt.executeQuery();
	
	while (rs.next()) {
		
		userId=rs.getInt("pk_user");
		
	}
	System.out.println("userId==="+userId);
	}catch (SQLException e) {
		e.printStackTrace();
	}

	finally{
	try {
		rs.close();
		
		pstmt.close();
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
	}
	return userId;
	
}



public void createNetworkSiteUser(CreateNetworkSiteUser createNetworkSiteUser, Map<String, Object> parameters) throws OperationException {
	// TODO Auto-generated method stub
		String test="";
		Connection conn=null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection conn1=null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		int count=0;
		String iaccId = (String)parameters.get(KEY_ACCOUNT_ID);
		String savedNtwIds = "";
		UserBean userBean=(UserBean)parameters.get("callingUser");
		int calinUserId=userBean.getUserId();
		UserBean userBeanPk=null;

		UserAgentRObj userAgent = (UserAgentRObj) parameters.get("userAgent");
		ResponseHolder response = (ResponseHolder)parameters.get("ResponseHolder");
		StringBuffer sqlbuf = new StringBuffer();
		String FK_NWSITES_MAIN="";
		String str="";
		String networkPK = "";
		String relationshipPK = "";
		String networkName = "";
		UserServiceHelper userServiceHelper=null;								   
		List<UserDetails> user=createNetworkSiteUser.getUser();
		networkName=createNetworkSiteUser.getNetwork().getName()==null?"":createNetworkSiteUser.getNetwork().getName();
		if(!networkName.equals("")){
			networkPK = getNetworkIdbyName(networkName);																																		  
		}
		if(networkPK.equals("") && !(networkName.equals("")))
		{
			response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			response.addIssue(new Issue(IssueTypes.NETWORK_NOT_FOUND, "Valid networkName is required."));
			throw new OperationException(); 									   
	    }

		if(createNetworkSiteUser.getNetwork().getNetworkIdentifier()!=null){
			if(createNetworkSiteUser.getNetwork().getNetworkIdentifier().getPK()!=null && !(createNetworkSiteUser.getNetwork().getNetworkIdentifier().getPK().equals(0))){
				networkPK = String.valueOf(createNetworkSiteUser.getNetwork().getNetworkIdentifier().getPK());
			}else if(createNetworkSiteUser.getNetwork().getNetworkIdentifier().getOID()!=null && !(createNetworkSiteUser.getNetwork().getNetworkIdentifier().getOID().equals(""))){
				networkPK = String.valueOf(((ObjectMapService)parameters.get("objectMapService")).getObjectPkFromOID(createNetworkSiteUser.getNetwork().getNetworkIdentifier().getOID()));
			}
		}

		if(createNetworkSiteUser.getNetwork().getRelationshipPK()!=null && !(createNetworkSiteUser.getNetwork().getRelationshipPK().equals(""))){
			relationshipPK = createNetworkSiteUser.getNetwork().getRelationshipPK();
		}
		if(createNetworkSiteUser.getNetwork().getSiteLevelDetails()!=null || !relationshipPK.equals("") || (!networkPK.equals("") && !networkPK.equals(0))){

		String siteName="";
		String relationshipType = "";
		String level = "";
		String relationType ="";
		if(createNetworkSiteUser.getNetwork().getSiteLevelDetails()!=null){
		if(createNetworkSiteUser.getNetwork().getSiteLevelDetails().getSiteName()!=null && !createNetworkSiteUser.getNetwork().getSiteLevelDetails().getSiteName().equals("")){
			siteName = createNetworkSiteUser.getNetwork().getSiteLevelDetails().getSiteName();

		}
		if(createNetworkSiteUser.getNetwork().getSiteLevelDetails().getRelationshipType()!=null){
			relationType = String.valueOf(getCodeByMultiInput(createNetworkSiteUser.getNetwork().getSiteLevelDetails().getRelationshipType(),"relnshipTyp"));
			if(relationType.equals("0")){
				response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				response.addIssue(new Issue(IssueTypes.CODE_NOT_FOUND, "RelationshipType input is not valid")); 
				throw new OperationException();

			}
		}
		if(createNetworkSiteUser.getNetwork().getSiteLevelDetails().getLevel()!=null){
			level = createNetworkSiteUser.getNetwork().getSiteLevelDetails().getLevel();
		}
		}
		System.out.println("networkPK==="+networkPK);
	System.out.println("relationShipPK==="+relationshipPK);
	String str1="";
	if(createNetworkSiteUser.getNetwork().getRelationshipPK()!=null){
		
		str1="SELECT pk_nwsites,NW_LEVEL,fk_site FROM er_nwsites,er_site WHERE er_site.fk_account=? and pk_site=fk_site and pk_nwsites="+relationshipPK;
	}else{
	str1 = "select pk_nwsites, nw_level "
			+ "from er_nwsites,er_site,er_codelst "+
				  "where pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account= ?";
	
	if(!networkPK.equals(""))
	{
		if(createNetworkSiteUser.getNetwork().getSiteLevelDetails()!=null && !level.equals("0")){
			str1=str1+" and fk_nwsites_main in("+networkPK+")";
		}else{
			str1=str1+" and pk_nwsites in("+networkPK+") and nw_level=0 ";
		}
	}	
	if(!siteName.equals("")){
		str1=str1+" and lower(site_name) like lower('"+siteName+"')";
	}	
	if(!level.equals("")){
		str1=str1+" and nw_level="+level;
	}

	if(!relationType.equals("")){
		str1=str1+" and NW_MEMBERTYPE="+relationType;
	}
	}
	try {
		System.out.println("str1==="+str1.toString());
		System.out.println("iaccId==="+iaccId);
	conn = CommonDAO.getConnection();
	pstmt = conn.prepareStatement(str1);
	pstmt.setInt(1, StringUtil.stringToNum(iaccId));
	rs = pstmt.executeQuery();
	Code code=null;
	Code code1=null;
	while (rs.next()) {
		pkNetList.add(rs.getString("pk_nwsites"));
		netLevelList.add(String.valueOf(rs.getInt("nw_level")));
		System.out.println("Inside loop");
		count++;
	}
	System.out.println("count==="+count);
	}catch (SQLException e) {
		e.printStackTrace();

	}

	finally{
	try {
		rs.close();
		if(rs1 != null){
		rs1.close();
	     }

		pstmt.close();
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}

	}
	if(count==0)
	{
		response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		response.addIssue(new Issue(IssueTypes.NETWORK_NOT_FOUND, "No Networks found."));
		throw new OperationException(); 									   
    }else{
    	for(int i=0;i<pkNetList.size();i++){
    		String networkId=pkNetList.get(i);
    		for(UserDetails usr: user){
    			if(usr.getUserIdentifier()==null || ((usr.getUserIdentifier().getPK()==null || usr.getUserIdentifier().getPK().equals("")) && (usr.getUserIdentifier().getLoginName()==null || usr.getUserIdentifier().getLoginName().equals("")) && (usr.getUserIdentifier().getOID()==null || usr.getUserIdentifier().getOID().equals("")))){
    				response.addAction(new CompletedAction("User Issues", CRUDAction.RETRIEVE));
    				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid user identifier is required."));
    				throw new OperationException();
    			}
    			String usrLogName=usr.getUserIdentifier().getLoginName()==null?"":usr.getUserIdentifier().getLoginName();
    			int usrPk=usr.getUserIdentifier().getPK()==null?0:usr.getUserIdentifier().getPK();
    			String usrOid=usr.getUserIdentifier().getOID()==null?"":usr.getUserIdentifier().getOID();
    			int usrID=checkUserExistence(usrLogName,String.valueOf(usrPk),usrOid);
    			if(usrID==0){
    				response.addAction(new CompletedAction("User Issues", CRUDAction.RETRIEVE));
        			response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND, "User does not exist."));
        			throw new OperationException();
    			}
    			int nwUserPk=getNetworkUsersPK(Integer.parseInt(networkId),usrID);
				int nwUserAdnlRolPk=0;			 
				if(nwUserPk!=0){
					response.addAction(new CompletedAction("User Issues", CRUDAction.RETRIEVE));
        			response.addIssue(new Issue(IssueTypes.DUPLICATE_USER, "This user already exists."));
        			throw new OperationException();
				}
				List<UserRoles> role=usr.getRole();					  
				if(role==null){
    				response.addAction(new CompletedAction("User Issues", CRUDAction.RETRIEVE));
        			response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Atleast one role is required."));
        			throw new OperationException();
    			}	  
    			if(role.size()>0){
    			String prevAdnlRole="";
    			String adtnRole="";
    			String roleNamePk="0", roleStatusPk="0";
    			String statusDate="";
    			String statusNotes="";
    			List<String> roleExistence = new ArrayList<String>();
    			System.out.println(roleExistence);
    			for(UserRoles us:role){
    				adtnRole=us.getIsAdditionalRole()==null?"":us.getIsAdditionalRole();
    				statusDate=us.getRoleStatusDate()==null?"":us.getRoleStatusDate();
    				statusNotes=us.getRoleStatusNotes()==null?"":us.getRoleStatusNotes();
    				String currAdnlRole=us.getIsAdditionalRole()==null?"":us.getIsAdditionalRole();
    				
    				if(currAdnlRole.equalsIgnoreCase(prevAdnlRole) && us.getRoleName()!=null){
    					response.addAction(new CompletedAction("User Issues", CRUDAction.RETRIEVE));
            			response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "More than one primary role cannot be added."));
            			throw new OperationException();
    				}
    				if(currAdnlRole.equalsIgnoreCase("N")){
    					prevAdnlRole=currAdnlRole;
    				}
    				
    				if(adtnRole.equals("")){
    					response.addAction(new CompletedAction("Data Issues", CRUDAction.RETRIEVE));
            			response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "IsAdditionRole element is required."));
            			throw new OperationException();
    				}	 
    				if(us.getRoleStatus()!=null){
    					roleStatusPk=String.valueOf(getCodeByMultiInput(us.getRoleStatus(),"networkuserstat"));
    					if(roleStatusPk.equals("0")){
    						response.addAction(new CompletedAction("User Issues", CRUDAction.RETRIEVE));
    						response.addIssue(new Issue(IssueTypes.CODE_NOT_FOUND, "RoleStatus input is not valid")); 
    						throw new OperationException();
    					}
    				}
    				if(us.getRoleName()!=null){
    					roleNamePk=String.valueOf(getCodeByMultiInput(us.getRoleName(),"nwusersrole"));
    					if(roleNamePk.equals("0")){
    						response.addAction(new CompletedAction("User Issues", CRUDAction.RETRIEVE));
    						response.addIssue(new Issue(IssueTypes.CODE_NOT_FOUND, "RoleName input is not valid")); 
    						throw new OperationException();
    					}else{
    						if(roleExistence!=null && roleExistence.contains(roleNamePk)){
    							response.addAction(new CompletedAction("User Issues", CRUDAction.RETRIEVE));
        						response.addIssue(new Issue(IssueTypes.DUPLICATE_NWUSER_ROLE, "Duplicate network user role in request.")); 
        						throw new OperationException();
    						}else{
    							roleExistence.add(roleNamePk);
    						}
    					}
    				}
    			}
    			}
    		} 		
    		for(UserDetails usr: user){
    			String usrLogName=usr.getUserIdentifier().getLoginName()==null?"":usr.getUserIdentifier().getLoginName();
    			int usrPk=usr.getUserIdentifier().getPK()==null?0:usr.getUserIdentifier().getPK();
    			String usrOid=usr.getUserIdentifier().getOID()==null?"":usr.getUserIdentifier().getOID();
    			int usrID=checkUserExistence(usrLogName,String.valueOf(usrPk),usrOid);
    			NetworkDao nwdao = new NetworkDao();
    			int nwUserPk=getNetworkUsersPK(Integer.parseInt(networkId),usrID);
				int nwUserAdnlRolPk=0;			 
    			List<UserRoles> role=usr.getRole();
				if(role==null){
    				response.addAction(new CompletedAction("User Issues", CRUDAction.RETRIEVE));
        			response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Atleast one role is required."));
        			throw new OperationException();
    			}	  
    			if(role.size()>0){
    			String prevAdnlRole="";
    			String adtnRole="";
    			String roleNamePk="0", roleStatusPk="0";
    			String statusDate="";
    			String statusNotes="";  			
    			if(usr.getMoreNetworkUserDetails()!=null){
    				
    				for (NVPair nv :usr.getMoreNetworkUserDetails()){
    					int mdId = 0;
    					MoreDetailsBean mdbMain = new MoreDetailsBean();
    					if(nv.getKey()!=null){
    						String type= "user_"+level;
    						Code code = new Code();
    						code.setCode(nv.getKey());
    						code.setType(type);
    						mdId = getCodeByMultiInput(code,type).intValue();
    						
    						if(mdId==0){
    							response.addAction(new CompletedAction("User Issues", CRUDAction.RETRIEVE));
    							response.addIssue(new Issue(IssueTypes.CODE_NOT_FOUND,"More Details field not found with key: " + nv.getKey())); 
    							throw new OperationException();
    						}
    					}
    				}
    			}	
    			for(UserRoles usRole:role){
    				adtnRole=usRole.getIsAdditionalRole()==null?"":usRole.getIsAdditionalRole();
    				statusDate=usRole.getRoleStatusDate()==null?"":usRole.getRoleStatusDate();
    				statusNotes=usRole.getRoleStatusNotes()==null?"":usRole.getRoleStatusNotes();
    				
    				if(usRole.getRoleName()!=null){
    					roleNamePk=String.valueOf(getCodeByMultiInput(usRole.getRoleName(),"nwusersrole"));
    				}
    				if(usRole.getRoleStatus()!=null){
    					roleStatusPk=String.valueOf(getCodeByMultiInput(usRole.getRoleStatus(),"networkuserstat"));
    				}
								
    				if(nwUserPk==0){
    					nwdao.saveUserNetwork(Integer.parseInt(networkId),usrID,calinUserId);
    				}
    				nwUserPk=getNetworkUsersPK(Integer.parseInt(networkId),usrID);
    				if(adtnRole.equalsIgnoreCase("N")){
    					if(!roleNamePk.equals(0)){
    						nwdao.updateUserNetwork(nwUserPk,Integer.parseInt(roleNamePk),calinUserId);
    					}
    					if(!roleStatusPk.equals(0)){
    						nwdao.updateUserNetworkUserStatus(nwUserPk,Integer.parseInt(roleStatusPk),"er_nwusers",calinUserId);
    						updateNetwrkSiteUserStatus(nwUserPk,"er_nwusers",Integer.parseInt(roleStatusPk),statusDate,statusNotes);
    					}
    				}else if((!roleNamePk.equals(0) && !roleStatusPk.equals(0)) && adtnRole.equalsIgnoreCase("Y")){
    					nwUserAdnlRolPk=saveAdditionalNwUserRole(nwUserPk,calinUserId,Integer.parseInt(roleNamePk),Integer.parseInt(roleStatusPk));
    					updateNetwrkSiteUserStatus(nwUserAdnlRolPk,"er_nwusers_addnlroles",0,statusDate,statusNotes);
    				}	
    			}
    			}
    			if(usr.getMoreNetworkUserDetails()!=null){
					userServiceHelper=new UserServiceHelper();
					int userLevel=Integer.parseInt(netLevelList.get(i));
					userServiceHelper.persistsMoreDetails(usr.getMoreNetworkUserDetails(),nwUserPk,userBean,parameters,CodeCache.CODE_TYPE_USER+"_"+userLevel);
    			}	

    			ObjectMap map = ((ObjectMapService) parameters.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_NETWORK, nwUserPk);
    			SimpleIdentifier sid = new SimpleIdentifier();
    			sid.setOID(map.getOID());
    			sid.setPK(nwUserPk);
    			
    			response.addAction(new CompletedAction(sid, CRUDAction.CREATE));
    			
    		}
    		
    	}	
    }											   
	}											  
	}

public int getNetworkUsersPK(int networkId,int userId){
	int nwUserPk = 0;
	PreparedStatement pstmt = null;
    Connection conn = null;
    ResultSet rs = null;
    String mainSql="";
    
    	mainSql="SELECT pk_nwusers FROM er_nwusers where fk_nwsites=? and fk_user=?";
  
    try{
    	conn = getConnection();
    	pstmt = conn
                .prepareStatement(mainSql);
    	pstmt.setInt(1, networkId);
    	pstmt.setInt(2, userId);
    	rs = pstmt.executeQuery();
    	while (rs.next()) {
    		nwUserPk = rs.getInt("pk_nwusers");
    	}
    	
    }catch(SQLException e){
    	Rlog.fatal("Network","EXCEPTION in getNetworkUsersPK of NetworkServiceDao class " + e);	


    } finally {
		try {
			rs.close();



			pstmt.close();

			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
    return nwUserPk;
}



public int saveAdditionalNwUserRole(int nwUserPk,int pkuser,int roleNamePk,int roleStatusPk){
	int adtnlRoleStatus = 0;
	int operationFlag = 0;
	PreparedStatement pstmt = null;
	Connection conn = null;
	ResultSet rs = null;
	String mainSql="";
	    try{
	    	conn = getConnection();
	    	if(roleStatusPk==0){
	    		mainSql = "select pk_codelst from er_codelst where codelst_type='networkuserstat' and codelst_subtyp='Pending'";

	    		
	    		pstmt = conn.prepareStatement(mainSql);
	    	
	    		rs = pstmt.executeQuery();
	    		while(rs.next()){
	    			adtnlRoleStatus = rs.getInt("pk_codelst");	
	    		}
	    	}
	    	mainSql="insert into ER_NWUSERS_ADDNLROLES (PK_NWUSERS_ADDROLES,FK_NWUSERS,NWU_MEMBERADDLTROLE,NWU_ADTSTATUS,creator) values(ERES.SEQ_ER_NWUSERS_ADDNLROLES.nextval,?,?,?,?)";
	    	pstmt = conn
	                .prepareStatement(mainSql);
	    	
	    	pstmt.setInt(1, nwUserPk);
	    	pstmt.setInt(2, roleNamePk);
	    	if(roleStatusPk==0){
	    		pstmt.setInt(3, adtnlRoleStatus);
	    	}else{
	    		pstmt.setInt(3, roleStatusPk);
	    	}
	    	pstmt.setInt(4, pkuser);
	    	operationFlag=pstmt.executeUpdate();
	    	
            mainSql = "select max(PK_NWUSERS_ADDROLES) nwUserAdnlPk from ER_NWUSERS_ADDNLROLES where FK_NWUSERS=?";	
    		pstmt = conn.prepareStatement(mainSql);
    		pstmt.setInt(1, nwUserPk);
    		rs = pstmt.executeQuery();
    		while(rs.next()){
    			operationFlag = rs.getInt("nwUserAdnlPk");	
    		}
    		  
	    }
	catch(SQLException e){
		e.printStackTrace();
		Rlog.fatal("Network","EXCEPTION in saveAdditionalNwUserRole of NetworkServiceDao class " + e);
	
    } finally {
		try {
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}}
	
	return operationFlag;
}
 
public void  updateNetwrkSiteUserStatus(int modulePK, String moduleTable, int statusPk, String statusDate, String statusNotes){
	PreparedStatement pstmt = null;
    Connection conn = null;
    StringBuffer mainsql=new StringBuffer();
    int nwstatus=0;
    int statusHistoryPk=0;
    String tableName="";
    StatusHistoryDao sdao = new StatusHistoryDao();
    try{
    	conn=getConnection();
    	statusHistoryPk = sdao.getLatestStatusId(modulePK,moduleTable);
    
    mainsql=new StringBuffer();
    if(moduleTable.equalsIgnoreCase("er_nwusers_addnlroles")){
    	mainsql=mainsql.append("update ER_STATUS_HISTORY SET STATUS_DATE=nvl(to_date('"+statusDate+"','mm/dd/yyyy'),sysdate), STATUS_NOTES=? where PK_STATUS=? ");
    }else{
    	mainsql=mainsql.append("update ER_STATUS_HISTORY SET FK_CODELST_STAT =?, STATUS_DATE=nvl(to_date('"+statusDate+"','mm/dd/yyyy'),sysdate), STATUS_NOTES=? where PK_STATUS=? ");
    }
    
    pstmt = conn.prepareStatement(mainsql.toString());
    if(moduleTable.equalsIgnoreCase("er_nwusers_addnlroles")){
        pstmt.setString(1, statusNotes);
        pstmt.setInt(2, statusHistoryPk);
    }else{
    	pstmt.setInt(1, statusPk);
    	pstmt.setString(2, statusNotes);
    	pstmt.setInt(3, statusHistoryPk);
    }
    pstmt.executeUpdate();
    }catch(SQLException e){
    	Rlog.fatal("Network","EXCEPTION in updateNetwrkSiteUserStatus of NetworkServiceDao class " + e);
    } finally {
		try {
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}


public void updateNetworkSiteUser(UpdateNetwrokUsersWrapper networkSiteUser,HashMap<String, Object> params) throws OperationException{


	UserBean userBean=(UserBean)params.get("callingUser");
	int userId=userBean.getUserId();
	ArrayList<NetSiteUser> updateNetworkSiteUser=(ArrayList<NetSiteUser>)networkSiteUser.getNetworkSiteUser();
	NetSiteUser networkUserdetail = null;
	String pkRelationshipType=(String)params.get("pkRelationshipType");
	String iaccId = (String)params.get(KEY_ACCOUNT_ID);
    String ipAdd=	AbstractService.IP_ADDRESS_FIELD_VALUE;
	ResponseHolder networkResponse =(ResponseHolder)params.get("response");
	UserAgentRObj userAgent = (UserAgentRObj) params.get("userAgent");
    Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	int count=0;
	String userPK="";
	String userLogName="";
	String userOID="";
	String userNetworkPK="";
	String validUserId="";
	
	UsersNetworkSites userNetSite=null;
	NetworkUsersRoles netUserRole=null;
		

	try {
		conn = CommonDAO.getConnection();
		StringBuffer sqlbuff = null;
		
	
		
		if(updateNetworkSiteUser!=null && updateNetworkSiteUser.size()>0){
		  for(int i=0;i<updateNetworkSiteUser.size();i++){
			networkUserdetail=updateNetworkSiteUser.get(i);
			int relationType=0;
			
			 if(networkUserdetail.getRelationshipPK()==null){
				  if(networkUserdetail.getNetworkIdentifier()==null && networkUserdetail.getNetworkName()==null && networkUserdetail.getSiteLevelDetail()==null ){
				 networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				 networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Either combination of networkName and siteLevelDetails or relationshipPK is required")); 
				throw new OperationException();
						}
			 else if((networkUserdetail.getNetworkIdentifier()==null && networkUserdetail.getNetworkName()==null && networkUserdetail.getSiteLevelDetail()!=null) ||
				 ((networkUserdetail.getNetworkIdentifier()!=null || networkUserdetail.getNetworkName()!=null) && networkUserdetail.getSiteLevelDetail()==null) 
			 					
					){
					networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Either networkIdentifier or networkName with siteLevelDetail  is required")); 
					throw new OperationException();
					}
					else if(networkUserdetail.getSiteLevelDetail().getSiteName() == null || networkUserdetail.getSiteLevelDetail().getLevel() == null || networkUserdetail.getSiteLevelDetail().getRelationshipType() == null)
							{
					networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "siteName,siteLevel and siteRelationshipType of siteLevelDetail is required")); 
					throw new OperationException();
						}
					  
					else if(networkUserdetail.getSiteLevelDetail().getRelationshipType()!=null){
						relationType =getCodeByMultiInput(networkUserdetail.getSiteLevelDetail().getRelationshipType(),"relnshipTyp");
						if(relationType==0){
							networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
							networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "RelationshipType input is not valid")); 
							throw new OperationException();
						}
					}
				
						}
				
		 if(networkUserdetail.getUserNetSite()!=null && networkUserdetail.getUserNetSite().size()>0){
			 for(int j=0;j<networkUserdetail.getUserNetSite().size();j++)	{	
				 userNetSite=networkUserdetail.getUserNetSite().get(j);
				 
				 
				 
					if(userNetSite.getUserIdentifier()!=null && !(userNetSite.getUserIdentifier().equals(""))){
						 userOID=userNetSite.getUserIdentifier().getOID()==null?"":userNetSite.getUserIdentifier().getOID();
						 userPK=StringUtil.integerToString(userNetSite.getUserIdentifier().getPK()==null?0:userNetSite.getUserIdentifier().getPK());
						 userLogName =userNetSite.getUserIdentifier().getLoginName()==null?"":userNetSite.getUserIdentifier().getLoginName();
						 userNetworkPK=userNetSite.getUserIdentifier().getNetworkSiteUserPK()==null?"":userNetSite.getUserIdentifier().getNetworkSiteUserPK();
					
						}	
					if(userNetworkPK.equals("")){
						
						if((userNetSite.getUserIdentifier()==null) || (userOID.equals("") && userPK.equals("0") && userLogName.equals("") )
							
						){
						networkResponse.addAction(new CompletedAction("Network User Issues", CRUDAction.RETRIEVE));
						networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Either User OID or UserName or UserLogin Name  are required")); 
				    	throw new OperationException();
									
						}	
						else{
							 validUserId=StringUtil.integerToString(checkUserExistence(userLogName,String.valueOf(userPK),userOID));
							 if(validUserId.equals("0")){
							 networkResponse.addAction(new CompletedAction("Network User Issues", CRUDAction.RETRIEVE));
						     networkResponse.addIssue(new Issue(IssueTypes.USER_NOT_FOUND, "User Not Exist.")); 
							 throw new OperationException();	
											}
								}
						}	
					params.put("userNetworkPK",userNetworkPK);
					params.put("userNetSite",userNetSite);
					params.put("isValidate","isValidate");
				    Map<String, Object> pkNet=validateExistingUserData(networkUserdetail,params,validUserId);
			
	 
         List<String> existRole = new ArrayList<String>();	 
         if(userNetSite.getRole()!=null && userNetSite.getRole().size()>0){
				 for(int k=0;k<userNetSite.getRole().size();k++){	
					 netUserRole=userNetSite.getRole().get(k);
					 
					
					if(netUserRole.getAction()==null || netUserRole.getIsAdditionalRole()==null ){
						
						networkResponse.addAction(new CompletedAction("Network User Issues", CRUDAction.RETRIEVE));
					    networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Role Action and IsAdditionalRole is required")); 
					   throw new OperationException();	
						
						
					}
					
					int roleNamePk=0;
					int roleStatusPk=0;				
					if(netUserRole.getRoleName()!=null){
						
						roleNamePk =getCodeByMultiInput(netUserRole.getRoleName(),"nwusersrole");
						if(roleNamePk==0){
							networkResponse.addAction(new CompletedAction("Network Role Issues", CRUDAction.RETRIEVE));
							networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "RoleName input is not valid")); 
							throw new OperationException();
						}
                  if(existRole!=null && existRole.contains(StringUtil.integerToString(roleNamePk))){
							networkResponse.addAction(new CompletedAction("User Issues", CRUDAction.RETRIEVE));
							networkResponse.addIssue(new Issue(IssueTypes.DUPLICATE_NWUSER_ROLE, "Duplicate network user role in request.")); 
    						throw new OperationException();
						}
						else{
							existRole.add(StringUtil.integerToString(roleNamePk));
						}
						
					}
					if(netUserRole.getRoleStatus()!=null){
						roleStatusPk =getCodeByMultiInput(netUserRole.getRoleStatus(),"networkuserstat");
						if(roleStatusPk==0){
							networkResponse.addAction(new CompletedAction("Network Role Issues", CRUDAction.RETRIEVE));
							networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "RoleStatus input is not valid")); 
							throw new OperationException();
						}
					}
					
				
	         if(netUserRole.getIsAdditionalRole().equalsIgnoreCase("N") && netUserRole.getAction().equalsIgnoreCase("remove")){
							networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
							networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Primary Roles  can not be remove "));
								throw new OperationException();
						}
	         
	          if(netUserRole.getIsAdditionalRole().equalsIgnoreCase("N") && netUserRole.getAction().equalsIgnoreCase("add")){
					networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "You can not perform add Action on Primary Roles"));
						throw new OperationException();
				}
						
						if(netUserRole.getRoleName()==null ){
							networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
							networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Role Name is required."));
							throw new OperationException();	
						}
						/*if((!netUserRole.getIsAdditionalRole().equalsIgnoreCase("N") && !netUserRole.getAction().equalsIgnoreCase("update"))){
							networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
							networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "IsAdditionalRole and Role Action is required with valid values"));
							throw new OperationException();	
						
						
						}*/
						if(networkUserdetail.getUserNetSite()!=null && userNetSite.getMoreUsersDetails()!=null){
							
							for (NVPair nv :userNetSite.getMoreUsersDetails()){

								if(nv.getKey()!=null){
									String type="";
									if(nv.getType()!=null){
										javax.xml.namespace.QName qname=nv.getType();
										type=qname.getLocalPart();
									}
									else{
										networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
										networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Type in moreUsersDetails is not valid")); 
										throw new OperationException();
									}
									String subType= nv.getKey();
									CodeDao code = new CodeDao();
									String pkCode = StringUtil.integerToString(code.getCodeId(type, subType ));
									if(pkCode.equals("0")){
										networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
										networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Key mentioned in moreUsersDetails is not valid")); 
										throw new OperationException();
									}
									}
						}	    				
		        	}
								
				 }
				
				 
				 }} 
					 
			 
			 
		 
			 
	for(int l=0;l<networkUserdetail.getUserNetSite().size();l++)	{	
		 userNetSite=networkUserdetail.getUserNetSite().get(l);
		 
		 userOID=userNetSite.getUserIdentifier().getOID()==null?"":userNetSite.getUserIdentifier().getOID();
		 userPK=StringUtil.integerToString(userNetSite.getUserIdentifier().getPK()==null?0:userNetSite.getUserIdentifier().getPK());
		 userLogName =userNetSite.getUserIdentifier().getLoginName()==null?"":userNetSite.getUserIdentifier().getLoginName();
		 userNetworkPK=userNetSite.getUserIdentifier().getNetworkSiteUserPK()==null?"":userNetSite.getUserIdentifier().getNetworkSiteUserPK();
	
		 if(userNetworkPK.equals("")){
		 validUserId=StringUtil.integerToString(checkUserExistence(userLogName,String.valueOf(userPK),userOID));
		 }	
		 
			params.put("userNetworkPK",userNetworkPK);
			params.put("userNetSite",userNetSite);
			params.put("isValidate","");
		    Map<String, Object> pkNet=validateExistingUserData(networkUserdetail,params,validUserId);
		    
		    
	}
		    
		}}}
		
	}catch(OperationException ex){
		throw ex;
	}catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			if(rs!=null)
			rs.close();
			if(pstmt!=null)
			pstmt.close();
			if(conn!=null)
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}



}
public Map<String, Object> validateExistingUserData(NetSiteUser updateNetworkSiteUserDetail,HashMap<String, Object> params,String userId)throws OperationException{
	
	String pk_network="";
	NetSiteUser networkUserDetails = null;
	String iaccId = (String)params.get(KEY_ACCOUNT_ID);
	ResponseHolder networkResponseHolder = (ResponseHolder)params.get("response");
	String userNetworkPK=(String)params.get("userNetworkPK");
	UsersNetworkSites usrNetSites=(UsersNetworkSites)params.get("userNetSite");
	String isValidate=(String)params.get("isValidate");
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	int count=0;
	String pk_main_network="";
	String level="";
	String fk_user="";
	Map<String, Object> parameters = null;
	String pk_nwusers="";
	NetworkUsersRoles netUserRole=null;
	ResponseHolder networkResponse =(ResponseHolder)params.get("response");
	UserServiceHelper userServiceHelper=null;
	UserBean userBean=(UserBean)params.get("callingUser");
	 List<String> pkNetList=new ArrayList<String>();
	 List<String> pkNwUserList=new ArrayList<String>();
	 List<String> netLevelList=new ArrayList<String>();
	 List<String> fkuserList=new ArrayList<String>();
	
	try{
		
	StringBuffer sqlbuff = null;
    parameters = new HashMap<String, Object>();
	if(updateNetworkSiteUserDetail!=null ){
		conn = CommonDAO.getConnection();
		networkUserDetails=updateNetworkSiteUserDetail;
		if(networkUserDetails.getRelationshipPK()!=null){
			sqlbuff = new StringBuffer();
		  sqlbuff.append("SELECT nu.pk_nwusers as pk_nwusers,pk_nwsites,fk_user,ns.NW_LEVEL as site_level FROM er_nwsites ns,er_site s,er_nwusers nu WHERE s.fk_account=? and s.pk_site=ns.fk_site and ns.pk_nwsites=? AND ns.pk_nwsites=nu.fk_nwsites   ");
		
		}

		else if((networkUserDetails.getNetworkIdentifier()!=null || networkUserDetails.getNetworkName()!=null) && networkUserDetails.getSiteLevelDetail()!=null){
			
			if(networkUserDetails.getNetworkIdentifier()!=null){
				if(networkUserDetails.getNetworkIdentifier().getNetworkIdentfier().getPK()!=null){
					pk_main_network=(networkUserDetails.getNetworkIdentifier().getNetworkIdentfier().getPK()==null)?"":networkUserDetails.getNetworkIdentifier().getNetworkIdentfier().getPK().toString();
				}
				else if(networkUserDetails.getNetworkIdentifier().getNetworkIdentfier().getOID()!=null){
					pk_main_network = String.valueOf(((ObjectMapService)params.get("objectMapService")).getObjectPkFromOID(networkUserDetails.getNetworkIdentifier().getNetworkIdentfier().getOID()));
				}
			}
			
			sqlbuff = new StringBuffer();
			sqlbuff.append(" SELECT DISTINCT NVL(ns.FK_NWSITES_MAIN,ns.pk_nwsites) fk_nwsites_main,nu.pk_nwusers as pk_nwusers,ns.pk_nwsites,ns.NW_LEVEL as site_level, fk_user FROM er_nwsites ns,er_site s,er_nwusers nu WHERE s.fk_account=? and s.pk_site=ns.fk_site AND ns.pk_nwsites=nu.fk_nwsites  "); 
					
			if(!(pk_main_network.equals(""))){
				sqlbuff.append(" and nvl(ns.FK_NWSITES_MAIN,ns.pk_nwsites)="+pk_main_network+"");
			}
			
			if(networkUserDetails.getNetworkName()!=null && pk_main_network.equals("")){
				sqlbuff.append(" and nvl(ns.FK_NWSITES_MAIN,ns.pk_nwsites) in (select pk_nwsites from er_nwsites,er_site where pk_site=fk_site and lower(site_name) like lower('"+networkUserDetails.getNetworkName()+"') and nw_level=0)" );
			}
			
			if(networkUserDetails.getSiteLevelDetail()!=null){
				sqlbuff.append(" and ns.pk_nwsites in (SELECT pk_nwsites FROM er_nwsites,er_site WHERE er_site.fk_account = ?");
				sqlbuff.append(" and s.pk_site=ns.fk_site AND lower(site_name) LIKE lower(trim('"+networkUserDetails.getSiteLevelDetail().getSiteName()+"')) AND nw_level=? AND NW_MEMBERTYPE="+networkUserDetails.getSiteLevelDetail().getRelationshipType().getPK()+") AND ns.pk_nwsites=nu.fk_nwsites ");
			}
		
			
		}
          if(userNetworkPK.equals("")){



		  sqlbuff.append("  AND fk_user="+userId+" ");
            }else{
           sqlbuff.append("  AND nu.pk_nwusers="+userNetworkPK+" ");
                 }
		
		System.out.println("sql==="+sqlbuff.toString());
			pstmt = conn.prepareStatement(sqlbuff.toString());
			pstmt.setInt(1, StringUtil.stringToNum(iaccId));
			if(networkUserDetails.getRelationshipPK()!=null){
				pstmt.setInt(2, StringUtil.stringToNum(networkUserDetails.getRelationshipPK()));
			}
			else{
				pstmt.setInt(2, StringUtil.stringToNum(iaccId));
				pstmt.setInt(3, StringUtil.stringToNum(networkUserDetails.getSiteLevelDetail().getLevel()));
			}
			rs=pstmt.executeQuery();
			
			while(rs.next()){
				pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
				pkNwUserList.add(String.valueOf(rs.getInt("pk_nwusers")));
				netLevelList.add(String.valueOf(rs.getInt("site_level")));
				fkuserList.add(String.valueOf(rs.getInt("fk_user")));
				count++;
				
			}if(count==0){			
				networkResponseHolder.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				networkResponseHolder.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network User Not Found"));

					throw new OperationException();
			}
			
		if(isValidate.equals("")){
 		for(int i=0;i<pkNetList.size();i++){
			
				pk_network=pkNetList.get(i);
	    		level=netLevelList.get(i);
	    		fk_user=fkuserList.get(i);
	    		pk_nwusers=pkNwUserList.get(i);
	    if(usrNetSites.getRole()!=null && usrNetSites.getRole().size()>0){
		for(int k=0;k<usrNetSites.getRole().size();k++){	
					 netUserRole=usrNetSites.getRole().get(k);
		
		int roleNamePk=0;
		int roleStatusPk=0;				
		roleNamePk =getCodeByMultiInput(netUserRole.getRoleName(),"nwusersrole");
		roleStatusPk =getCodeByMultiInput(netUserRole.getRoleStatus(),"networkuserstat");
						
					
	   
						
					params.put("pk_Network", pk_network);
					params.put("siteLevel", level);
					params.put("fk_user", fk_user);
					params.put("roleStatusPk", roleStatusPk);
					params.put("roleNamePk", roleNamePk);
                    int checkExistRole=0;
					if(netUserRole.getIsAdditionalRole().equalsIgnoreCase("Y") && netUserRole.getAction().equalsIgnoreCase("add")){
					 checkExistRole=validateExistingUsrRoles(StringUtil.stringToInteger(pk_network),StringUtil.stringToInteger(fk_user),"additionalRole", roleNamePk);
					}else if(netUserRole.getIsAdditionalRole().equalsIgnoreCase("N") && netUserRole.getAction().equalsIgnoreCase("update")){
				     checkExistRole=validateExistingUsrRoles(StringUtil.stringToInteger(pk_network),StringUtil.stringToInteger(fk_user),"primaryRole", roleNamePk);
								
					}
					if(checkExistRole>0){
						networkResponse.addAction(new CompletedAction("User Issues", CRUDAction.RETRIEVE));
						networkResponse.addIssue(new Issue(IssueTypes.DUPLICATE_NWUSER_ROLE, "Duplicate network user role in request."));
						throw new OperationException();
					}
					networkUserRoles(netUserRole,params);
					
				
					
				 }
				if(usrNetSites.getMoreUsersDetails()!=null){
						userServiceHelper=new UserServiceHelper();
						int userLevel=Integer.parseInt(level);
						userServiceHelper.persistsMoreDetails(usrNetSites.getMoreUsersDetails(),StringUtil.stringToInteger(pk_nwusers),userBean,params,CodeCache.CODE_TYPE_USER+"_"+userLevel);
				} 
				 
				 }		
	     		
			}
			}
			
			
			
	
	}
	}catch(OperationException ex){
		throw ex;
	}catch(SQLException ex){
		ex.printStackTrace();
	}
	catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			if(rs!=null)
				rs.close();
			if(pstmt!=null)
				pstmt.close();
			if(conn!=null)
				conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}


	}
	return parameters;
}



public int networkUserRoles(NetworkUsersRoles netUserRole,HashMap<String, Object> params)throws OperationException{
	
	
	
	UserBean userBean=(UserBean)params.get("callingUser");
	int userId=userBean.getUserId();
	ResponseHolder networkResponse = (ResponseHolder)params.get("response");
	String pk_Network=(String)params.get("pk_Network");
	String fk_user=(String)params.get("fk_user");
	int roleStatusPk=(int)params.get("roleStatusPk");
	int roleNamePk=(int)params.get("roleNamePk");
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	int count=0;
	
	try{
		
		String pk_nwusers="";
		int existrole=0;
		int existstatus=0;
		StringBuffer sqlbuff = new StringBuffer();
		conn = CommonDAO.getConnection();
		NetworkDao netdao=new NetworkDao();
          if(netUserRole.getIsAdditionalRole().equalsIgnoreCase("N") && netUserRole.getAction().equalsIgnoreCase("update")){
			sqlbuff = new StringBuffer();
			sqlbuff.append(" select pk_nwusers,nwu_membertrole,nwu_status from er_nwusers where fk_nwsites="+pk_Network+" and  fk_user="+fk_user+"  ");
			pstmt = conn.prepareStatement(sqlbuff.toString());
			rs=pstmt.executeQuery();
			while(rs.next()){
				pk_nwusers=String.valueOf(rs.getInt("pk_nwusers"));
				existrole=rs.getInt("nwu_membertrole");
				existstatus=rs.getInt("nwu_status");
			 
			 }
		if(netUserRole.getRoleName()!=null && existrole!=roleNamePk){
			 netdao=new NetworkDao();
			 int roleflag=0;
			roleflag=netdao.updateUserNetwork(StringUtil.stringToInteger(pk_nwusers), roleNamePk, StringUtil.stringToInteger(fk_user));
			
		}
		if(netUserRole.getRoleStatus()!=null && existstatus!=roleStatusPk){
			
				int roleStatusFlag=0;
				netdao=new NetworkDao();
				roleStatusFlag=netdao.updateUserNetworkUserStatus(StringUtil.stringToInteger(pk_nwusers), roleStatusPk, "er_nwusers", userId);
				
				if(roleStatusFlag!=0 && netUserRole.getRoleStatus()!=null){
				statusHistoryUpdate(netUserRole,params,pk_nwusers);
				}
						
				
		}else if((netUserRole.getRoleStatusDate()!=null || netUserRole.getRoleStatusNotes()!=null)){
			
			sqlbuff = new StringBuffer();
			sqlbuff.append("update er_status_history set status_date=to_date('"+netUserRole.getRoleStatusDate()+"','mm/dd/yyyy'), status_notes=? where STATUS_MODPK=? and STATUS_MODTABLE='er_nwusers' and STATUS_ISCURRENT=1 ");
			pstmt = conn.prepareStatement(sqlbuff.toString());
			pstmt.setString(1, netUserRole.getRoleStatusNotes());
			pstmt.setInt(2,StringUtil.stringToInteger(pk_nwusers));
			int tmpCount=pstmt.executeUpdate();
		}

		
			
			ObjectMap map = ((ObjectMapService) params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ER_NWUSERS, StringUtil.stringToNum(pk_nwusers));
			SimpleIdentifier sid = new SimpleIdentifier();
			sid.setOID(map.getOID());
			sid.setPK(StringUtil.stringToInteger(pk_nwusers));
			networkResponse.addAction(new CompletedAction(sid, CRUDAction.UPDATE));
		 }
          
          
			if(netUserRole.getIsAdditionalRole().equalsIgnoreCase("Y") && netUserRole.getAction().equalsIgnoreCase("update")){

				sqlbuff = new StringBuffer();
				String exist_pk_addtnrole="";
				int existAddtnlRole=0;
				
				int existAddroleStat=0;
				sqlbuff.append(" select nu.pk_nwusers as pk_nwusers,pk_nwusers_addroles,nwu_memberaddltrole,nwu_adtstatus from er_nwusers nu,er_nwusers_addnlroles nwr where nu.fk_nwsites="+pk_Network+"  and  nu.fk_user="+fk_user+" and nu.pk_nwusers=nwr.fk_nwusers and NWU_MEMBERADDLTROLE="+roleNamePk+"  ");
				pstmt = conn.prepareStatement(sqlbuff.toString());
				rs=pstmt.executeQuery();
				while(rs.next()){
					pk_nwusers=String.valueOf(rs.getInt("pk_nwusers"));
					exist_pk_addtnrole=String.valueOf(rs.getInt("pk_nwusers_addroles"));
					existAddtnlRole=rs.getInt("nwu_memberaddltrole");
					existAddroleStat=rs.getInt("nwu_adtstatus");
					
					if(!exist_pk_addtnrole.equals("")){




						 netdao=new NetworkDao();
						 int roleflag=0;
					if(existAddtnlRole!=roleNamePk){
						 roleflag=netdao.updateUserAdditional(StringUtil.stringToNum(exist_pk_addtnrole),roleNamePk,userId);
					}
					if(existAddroleStat!=roleStatusPk)	{
					int roleStatusFlag=0;
							netdao=new NetworkDao();
							roleStatusFlag=netdao.updateUserNetworkUserStatus(StringUtil.stringToInteger(exist_pk_addtnrole), roleStatusPk, "er_nwusers_addnlroles", userId);
							
							if(existAddroleStat!=0 && netUserRole.getRoleStatus()!=null){	
							statusHistoryUpdate(netUserRole,params,exist_pk_addtnrole);
						}
					}
					
					if((netUserRole.getRoleStatusDate()!=null || netUserRole.getRoleStatusNotes()!=null) && existAddtnlRole==roleNamePk && existAddroleStat==roleStatusPk){
						
						sqlbuff = new StringBuffer();
						sqlbuff.append("update er_status_history set status_date=to_date('"+netUserRole.getRoleStatusDate()+"','mm/dd/yyyy'), status_notes=? where STATUS_MODPK=? and STATUS_MODTABLE='er_nwusers_addnlroles' and STATUS_ISCURRENT=1 ");
						pstmt = conn.prepareStatement(sqlbuff.toString());
						pstmt.setString(1, netUserRole.getRoleStatusNotes());
						pstmt.setInt(2,StringUtil.stringToInteger(exist_pk_addtnrole));
						int tmpCount=pstmt.executeUpdate();
					}
					
					}
					count++;	
					
				 }
				if(count==0){
					networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Additional Roles or Role Name are not exist"));
						throw new OperationException();
					
				}else{
				ObjectMap map = ((ObjectMapService) params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ER_NWUSERS, StringUtil.stringToNum(pk_nwusers));
				SimpleIdentifier sid = new SimpleIdentifier();
				sid.setOID(map.getOID());
				sid.setPK(StringUtil.stringToInteger(pk_nwusers));
				networkResponse.addAction(new CompletedAction(sid, CRUDAction.UPDATE));
				
			}
			}
				
				
				if(netUserRole.getIsAdditionalRole().equalsIgnoreCase("Y") && netUserRole.getAction().equalsIgnoreCase("add")){
					 pk_nwusers="";
					 existrole=0;
					 existstatus=0;
					 int pk_addtnrole=0;
					sqlbuff = new StringBuffer();
					sqlbuff.append(" select pk_nwusers,nwu_membertrole,nwu_status from er_nwusers where fk_nwsites="+pk_Network+" and  fk_user="+fk_user+"  ");
					pstmt = conn.prepareStatement(sqlbuff.toString());
					rs=pstmt.executeQuery();
					while(rs.next()){
						pk_nwusers=String.valueOf(rs.getInt("pk_nwusers"));
						existrole=rs.getInt("nwu_membertrole");
						existstatus=rs.getInt("nwu_status");
					 
					 if(!pk_nwusers.equals("")){

						 pk_addtnrole=saveAdditionalNwUserRole(StringUtil.stringToInteger(pk_nwusers),userId,roleNamePk,roleStatusPk);
					 }
					if(pk_addtnrole>0){
					 if((netUserRole.getRoleStatusDate()!=null || netUserRole.getRoleStatusNotes()!=null)){
							
							sqlbuff = new StringBuffer();
							sqlbuff.append("update er_status_history set status_date=to_date('"+netUserRole.getRoleStatusDate()+"','mm/dd/yyyy'), status_notes=? where STATUS_MODPK=? and STATUS_MODTABLE='er_nwusers_addnlroles' and STATUS_ISCURRENT=1 ");
							pstmt = conn.prepareStatement(sqlbuff.toString());
							pstmt.setString(1, netUserRole.getRoleStatusNotes());
							pstmt.setInt(2,pk_addtnrole);
							int tmpCount=pstmt.executeUpdate();
					 }
					 }
					}
					ObjectMap map = ((ObjectMapService) params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ER_NWUSERS, StringUtil.stringToNum(pk_nwusers));
					SimpleIdentifier sid = new SimpleIdentifier();
					sid.setOID(map.getOID());
					sid.setPK(StringUtil.stringToInteger(pk_nwusers));
					networkResponse.addAction(new CompletedAction(sid, CRUDAction.UPDATE));
				}
				
				if(netUserRole.getIsAdditionalRole().equalsIgnoreCase("Y") && netUserRole.getAction().equalsIgnoreCase("remove")){
					 String exist_addtnRolePK="";
					 int existAddtnrole=0;
					 int existAddtnstatus=0;
					 netdao=new NetworkDao();
					sqlbuff = new StringBuffer();
					sqlbuff.append(" select PK_NWUSERS_ADDROLES,NWU_MEMBERADDLTROLE,NWU_ADTSTATUS from er_nwusers nu,er_nwusers_addnlroles nwr where nu.fk_nwsites="+pk_Network+"  and  nu.fk_user="+fk_user+" and nu.pk_nwusers=nwr.fk_nwusers and NWU_MEMBERADDLTROLE="+roleNamePk+"  ");
					
					if(netUserRole.getRoleStatus()!=null ){
						sqlbuff.append("and  NWU_ADTSTATUS="+roleStatusPk+"");
					}


					pstmt = conn.prepareStatement(sqlbuff.toString());
					rs=pstmt.executeQuery();
					while(rs.next()){
						exist_addtnRolePK=String.valueOf(rs.getInt("PK_NWUSERS_ADDROLES"));
						existAddtnrole=rs.getInt("NWU_MEMBERADDLTROLE");
						existAddtnstatus=rs.getInt("NWU_ADTSTATUS");
					 
					 if(!exist_addtnRolePK.equals("")){
						int success= netdao.deleteStatusHistoryEnteries(StringUtil.stringToInteger(exist_addtnRolePK),"er_nwusers_addnlroles");
					   if(success>0){
						   netdao.deleteUserNetworkaDAddRole(StringUtil.stringToInteger(exist_addtnRolePK));
					   }
					 }
					 count++;
					 }
				if(count==0){
					networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Additional Roles  details are not exist "));
						throw new OperationException();
					
				}else{
			        
					ObjectMap map = ((ObjectMapService) params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ER_NWUSERS, StringUtil.stringToNum(pk_nwusers));
					SimpleIdentifier sid = new SimpleIdentifier();
					sid.setOID(map.getOID());
					sid.setPK(StringUtil.stringToInteger(pk_nwusers));
					networkResponse.addAction(new CompletedAction(sid, CRUDAction.REMOVE));
				}	
          }
			
		
	}catch(OperationException ex){
		throw ex;
	}

catch(SQLException ex){
		ex.printStackTrace();
	}

	catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			if(rs!=null)
				rs.close();
			if(pstmt!=null)
				pstmt.close();
			if(conn!=null)
				conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return count;
}
public int statusHistoryUpdate(NetworkUsersRoles netUserRole,HashMap<String, Object> params,String pkNetworkUser)throws OperationException{
	
	String ipAdd=	AbstractService.IP_ADDRESS_FIELD_VALUE;
	UserBean userBean=(UserBean)params.get("callingUser");
	int userId=userBean.getUserId();
	ResponseHolder networkResponse = (ResponseHolder)params.get("response");
	String pk_Network=(String)params.get("pk_Network");
	String fk_user=(String)params.get("fk_user");
	int roleStatusPk=(int)params.get("roleStatusPk");
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	int count=0;
		
	
	try{
		
		String pk_nwusers="";
		int existrole=0;
		int existstatus=0;
		StringBuffer sqlbuff = new StringBuffer();
		conn = CommonDAO.getConnection();
         		
				
				   
				   sqlbuff = new StringBuffer();
				   java.util.Date statusEnteredDate=DateUtil.stringToDate(netUserRole.getRoleStatusDate(), "mm/dd/yyyy");
				   
					sqlbuff.append("update er_status_history set STATUS_END_DATE=to_date('"+netUserRole.getRoleStatusDate()+"','mm/dd/yyyy'),LAST_MODIFIED_DATE=sysdate,STATUS_ISCURRENT=0 where STATUS_MODPK=? and STATUS_END_DATE is null");
					pstmt = conn.prepareStatement(sqlbuff.toString());
					pstmt.setInt(1, StringUtil.stringToNum(pkNetworkUser));
					count=pstmt.executeUpdate();
					if(count>0){
						sqlbuff = new StringBuffer();
						sqlbuff.append("INSERT INTO er_status_history(PK_STATUS,STATUS_MODPK,STATUS_MODTABLE,FK_CODELST_STAT,STATUS_DATE,STATUS_NOTES,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD,STATUS_ISCURRENT) ");
						sqlbuff.append("VALUES(SEQ_ER_STATUS_HISTORY.nextval,?,?,?,?,?,?,'N',sysdate,?,?)");
						pstmt = conn.prepareStatement(sqlbuff.toString());
						pstmt.setInt(1, StringUtil.stringToNum(pkNetworkUser));
						if(netUserRole.getIsAdditionalRole().equalsIgnoreCase("N")){
						pstmt.setString(2,"er_nwusers");
						}else if(netUserRole.getIsAdditionalRole().equalsIgnoreCase("Y")){
							pstmt.setString(2,"er_nwusers_addnlroles");	
						}
						pstmt.setInt(3, roleStatusPk);
                        pstmt.setDate(4, DateUtil.dateToSqlDate(statusEnteredDate));
						pstmt.setString(5,netUserRole.getRoleStatusNotes());
						pstmt.setInt(6, userId);
						pstmt.setString(7, ipAdd);
						pstmt.setInt(8, 1);
						int tmpCount=pstmt.executeUpdate();
						
					  
		    		}
			
	
	}
catch(SQLException ex){
		ex.printStackTrace();
	}

	catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {

			if(rs!=null)
				rs.close();
			if(pstmt!=null)
				pstmt.close();
			if(conn!=null)
				conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return count;
}

public Integer removeNetwork(RemoveNetwork removeNetworkSite, HashMap<String, Object> params) throws OperationException, SQLException 
{
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;	
	String networkName="";
	String ipAdd="";
	String networkPK="",OIDnetworkPK="";
	String networkOID="";
	String sql="";
	conn = CommonDAO.getConnection();
	UserBean userBean=(UserBean)params.get("callingUser");
	NetworkDao nwdao=new NetworkDao();
	String userId =(userBean.getUserId()).toString();
	userId=userId==null?"":userId;
	String netpk="";String deleteNetid="";
	List<Integer> networkPKList=new ArrayList<Integer>();
	Integer networkPKTemp=0;
    Map<Integer, Integer> counts = new HashMap<Integer, Integer>();
    
	String iaccId = ((String)params.get(KEY_ACCOUNT_ID)==null)?"":(String)params.get(KEY_ACCOUNT_ID);
			ResponseHolder response = (ResponseHolder)params.get("ResponseHolder");				
			
		if(removeNetworkSite!=null)	{
		List<NetworkRemoveSite> networkSiteDetail=removeNetworkSite.getNetworkSiteDetailList();
		NetworkRemoveSite networkSite=new NetworkRemoveSite();
		try {
 		for(int iX=0;iX<networkSiteDetail.size();iX++)
		{
			 networkPK=""; OIDnetworkPK=""; networkOID=""; networkName=""; netpk="";
			 
			networkSite=networkSiteDetail.get(iX);
			networkName = (networkSiteDetail.get(iX).getNetworkName()==null)?"": networkSiteDetail.get(iX).getNetworkName();
			
			if(AbstractService.IP_ADDRESS_FIELD_VALUE!=null)
			{ 
						ipAdd=	AbstractService.IP_ADDRESS_FIELD_VALUE;
			}
			
			if(networkSite.getNetworkIdentifier()==null && (networkSite.getNetworkName()==null || networkSite.getNetworkName().equals(""))){
				response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "networkIdentifier or networkName is required")); 
				throw new OperationException();				
			}
		if((networkSite.getNetworkName()==null || networkSite.getNetworkName().equals("")) && (networkSite.getNetworkIdentifier().getOID()==null || networkSite.getNetworkIdentifier().getOID().equals("")) &&  (networkSite.getNetworkIdentifier().getPK()==null || networkSite.getNetworkIdentifier().getPK().equals(0)))	{
			
			response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "networkIdentifier or networkName is required")); 
			throw new OperationException();
			
		}
		if(networkSite.getNetworkIdentifier()!=null){
			
			if(networkSite.getNetworkIdentifier().getOID()!=null && !networkSite.getNetworkIdentifier().getOID().equals("")){
				
				OIDnetworkPK = String.valueOf(((ObjectMapService)params.get("objectMapService")).getObjectPkFromOID(networkSiteDetail.get(iX).getNetworkIdentifier().getOID()));
			}
			
		  if(networkSite.getNetworkIdentifier().getPK()!=null && !networkSite.getNetworkIdentifier().getPK().equals(0) ){
				networkPK = String.valueOf(networkSite.getNetworkIdentifier().getPK());
			}
		}
		if(!OIDnetworkPK.equals("") && !networkPK.equals("")){
		if(OIDnetworkPK.equals(networkPK)){
			networkPK=networkPK;	
			
		}else {
			response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "PK is Not Matching with OID.")); 
			throw new OperationException();
		}}
		
		if(!networkName.equalsIgnoreCase("")){
			networkName=networkSite.getNetworkName();
			netpk=getNetworkIdbyName(networkName);
			
			int multiNetIds=netpk.indexOf(",");
			
			if(multiNetIds>0){
				response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Multiple Networks Founds with networkName.")); 
				throw new OperationException();
			}
			
			if(netpk.equals("")){
				response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Correct networkName is Required.")); 
				throw new OperationException();	
			}
		}
		
		if(!networkPK.equals("")&& !netpk.equals("") ){
		if(networkPK.equals(netpk)){
			networkPK=netpk;	
			
		}else if (!networkPK.equals(netpk)) {
			response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Either NetworkPk or NetworkOID is Not Matching with Network Name.")); 
			throw new OperationException();
		}
		}
		
	
		params.put("networkPK", networkPK);
		
		networkPKTemp=getMainNetworkIdbyNtId(networkSite,params);
		
		networkPKList.add(networkPKTemp);
		
					
	}	


		for(Integer intNetworkId:networkPKList){

			if(nwdao.getNetworkStatusSubtyp(intNetworkId).equalsIgnoreCase("notPending")){
				response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, MC.M_NtwStnt_Peding)); 
				throw new OperationException();
			}else if( nwdao.getStudyNetworkCount(intNetworkId)>0){
				response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, MC.M_NtAss_Stdy)); 
				throw new OperationException();
			}else{			
				System.out.println("Network ID  "+intNetworkId);
				nwdao.deleteNetwork(userId, ipAdd,intNetworkId);
				System.out.println("Network Deleted using PK OR OID OR NAME");
			}
		}
	}
		catch(Exception e){
		throw e;
	}
		finally
		{
			try {
				if(rs != null){
				rs.close();
				}
				if(pstmt!=null){
				pstmt.close();
				}
				if(conn!=null)
				{
				conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	
			
			}
	return networkPKTemp;
}

public Integer getMainNetworkIdbyNtId(NetworkRemoveSite networkSite,HashMap<String, Object> params) throws OperationException {
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	String ntwId = "";
	String OIDnetworkPK="";
	String networkPK="";
	String netIDPk="";
	ResponseHolder response = (ResponseHolder)params.get("ResponseHolder");
	Integer PkNtList=null;
	
	if(networkSite.getNetworkIdentifier()!=null){
	if(networkSite.getNetworkIdentifier().getPK()!=null && !networkSite.getNetworkIdentifier().getPK().equals(0) ){
	if(params.get("networkPK").toString()!=null){
	 netIDPk=params.get("networkPK").toString();
			}
		}
	}
	String networkName="";
	String netpk="";
	Integer count=0;
	String netName="";
	try {
	String str="select * from er_nwsites where  ";
     
	if(netIDPk.equals("")){
	if(networkSite.getNetworkIdentifier()!=null){
	if(networkSite.getNetworkIdentifier().getOID()!=null && !networkSite.getNetworkIdentifier().getOID().equals("")){
			
    	OIDnetworkPK = String.valueOf(((ObjectMapService)params.get("objectMapService")).getObjectPkFromOID(networkSite.getNetworkIdentifier().getOID()));
		}
		
	  if(networkSite.getNetworkIdentifier().getPK()!=null && !networkSite.getNetworkIdentifier().getPK().equals(0) ){
		networkPK = String.valueOf(networkSite.getNetworkIdentifier().getPK());
		}
	   
	}
	 netName=networkSite.getNetworkName()==null?"":networkSite.getNetworkName();
		if(!netName.equals("") ){
				networkName=networkSite.getNetworkName();
				netpk=getNetworkIdbyName(networkName);
		}
	if(!OIDnetworkPK.equals("")){
		str=str+" PK_NWSITES="+OIDnetworkPK+"";
		
	}else if(!networkPK.equals("")){
		str=str+" PK_NWSITES="+networkPK+"";
	}else if(!netpk.equals("")){
		
		str=str+" PK_NWSITES="+netpk+"";
	}
	}
	else{
		str=str+" PK_NWSITES="+netIDPk+"";
	}
	
	str=str+ " and NW_LEVEL='0'";
	
	System.out.println("str==="+str.toString());
		
	conn = CommonDAO.getConnection();
	pstmt = conn.prepareStatement(str);
	rs = pstmt.executeQuery();
	
	while (rs.next()) {
		
		PkNtList=rs.getInt("pk_nwsites");
		count++;
		}
	if(count==0){
		response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No Network data found")); 
		throw new OperationException();
		
	}
	ntwId=count.toString();
	System.out.println("ntwId==="+ntwId);
	}
	catch(OperationException ex){
		throw ex;
	}catch(SQLException ex){
		ex.printStackTrace();
	}
	catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			if(rs!=null)
				rs.close();
			if(pstmt!=null)
				pstmt.close();
			if(conn!=null)
				conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}


	}
	return PkNtList;
}

public String createNetwork(List<NetworkSiteData> networkSites, Map<String, Object> parameters) throws OperationException {
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	Connection conn1=null;
	PreparedStatement pstmt1 = null;
	ResultSet rs1 = null;
	ResponseHolder networkResponse = (ResponseHolder)parameters.get("ResponseHolder");
	int count=0;
	String iaccId = (String)parameters.get(KEY_ACCOUNT_ID);
	String savedNtwIds = "";
	List chkDuplSites=new ArrayList();
	for (NetworkSiteData networkSite : networkSites) {
		if ((networkSite.getSiteName() != null && !networkSite.getSiteName().trim().equals(""))
				|| (networkSite.getSiteIdentifier() != null && ((networkSite.getSiteIdentifier().getPK() != null
						&& networkSite.getSiteIdentifier().getPK().intValue()!=0)
						|| (networkSite.getSiteIdentifier().getOID() != null
								&& !networkSite.getSiteIdentifier().getOID().trim().equals(""))))) {
	   int pkSite = getPKSiteFromIdentifier(networkSite, StringUtil.stringToNum(iaccId),"");		
	   if(pkSite==0){
			networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			networkResponse.addIssue(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, " Organization not found")); 
			throw new OperationException();	   
	   } else{
		   try {
			    if(chkDuplSites.contains(pkSite)){
					networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Duplicate sites are not allowed")); 
					throw new OperationException();
				}else{
				 chkDuplSites.add(pkSite);				
				}
				String str2 = "select count(*) as NW_LEVEL_COUNT from ER_NWSITES where FK_SITE=? and NW_LEVEL= 0";
				conn = CommonDAO.getConnection();
				pstmt = conn.prepareStatement(str2);
				pstmt.setInt(1,pkSite);
				rs = pstmt.executeQuery();
				int nwLevelCount=0;
				while(rs.next()){
					nwLevelCount=rs.getInt("NW_LEVEL_COUNT");							
					count++;
				}				
				int relationshipType=0;
				if(nwLevelCount > 0){
					networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network(s) already exist")); 
					throw new OperationException();		
				}else{
					Integer siteStatus=0;
					if(networkSite.getSiteStatus()!=null){
						siteStatus=getCodeByMultiInput(networkSite.getSiteStatus(),"networkstat");	
						if(siteStatus.intValue()==0){
							networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
							networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Select a valid Site Status")); 
							throw new OperationException();	
						}
					}
					if(networkSite.getSiteStatusDate()!=null){
						if(DateUtil.stringToDate(networkSite.getSiteStatusDate(), null)==null){
							networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
							networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "SiteStatusDate format is incorrect")); 
							throw new OperationException();
						}
					}
					
					if(networkSite.getSiteRelationShipType()!=null){
						relationshipType=getCodeByMultiInput(networkSite.getSiteRelationShipType(),"relnshipTyp");							
						if(relationshipType==0){
							networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
							networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Select a valid Relationship type")); 
							throw new OperationException();	
						}
					}
					if(networkSite.getMoreSiteDetailsFields()!=null){							
						for (NVPair nv :networkSite.getMoreSiteDetailsFields()){
							int mdId = 0;
							if(nv.getKey()!=null){
								String type= "org";
								Code code = new Code();
								code.setCode(nv.getKey());
								code.setType(type);
								mdId = getCodeByMultiInput(code,type).intValue();									
								if(mdId==0){
									networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
									networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Key mentioned in MoreSiteDetailsField is not valid")); 
									throw new OperationException();
								}
							}
						}
					}					
				}					  
			}catch (SQLException e) {
				e.printStackTrace();
			}finally{
				try {
					rs.close();
					if(rs1 != null){
					   rs1.close();
				    }
					pstmt.close();
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		    } 
		}	   
	  }else{
		networkResponse.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		networkResponse.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid siteIdentifier or name of the site is required")); 
		throw new OperationException();	 
	  }
	}	
	saveMultipleNetworks(0,networkSites,parameters,networkResponse);
	return savedNtwIds;
}

private String saveMultipleNetworks(int nwLevel, List<NetworkSiteData> site, Map<String, Object> parameters,ResponseHolder networkResponse) throws OperationException {
	String savedNtwIds ="";
	String iaccId = (String)parameters.get(KEY_ACCOUNT_ID);
	ArrayList<String> sitesToInsert = new ArrayList<String>();
	for(NetworkSiteData s: site){
		sitesToInsert = new ArrayList<String>();		
		int pkSite=getPKSiteFromIdentifier(s,StringUtil.stringToNum(iaccId),"");
		SiteJB sJB = new SiteJB();
		sJB.setSiteId(pkSite);
		SiteBean sb=sJB.getSiteDetails();
		String siteName = "";
		if(sb.getSiteName()!=null && !sb.getSiteName().equals("")){
			siteName = sb.getSiteName();
		}
		sitesToInsert.add(String.valueOf(pkSite)); 		
		Integer netType = 0;
		Integer netStatus = 0;
		if(s.getSiteRelationShipType()!=null){
			netType=getCodeByMultiInput(s.getSiteRelationShipType(),"relnshipTyp");		
		}
		if(s.getSiteStatus()!=null){
			netStatus=getCodeByMultiInput(s.getSiteStatus(),"networkstat");
		}else{
			Code code = new Code();
			code.setType("networkstat");
			code.setCode("pending");
			netStatus=getCodeByMultiInput(code,"networkstat");
		}		
		String[] sitesArr = sitesToInsert.stream().toArray(String[]::new);		
		NetworkDao ndao = new NetworkDao();
		ndao.setChld_ntw_ids(null);
		UserBean user = (UserBean)parameters.get("callingUser");
		int ret=ndao.saveMultipleNetwork(sitesArr, String.valueOf(nwLevel), String.valueOf(user.getUserId()), "", String.valueOf(netType), null, null, String.valueOf(netStatus));
		if(s.getMoreSiteDetailsFields()!=null){
		    saveMoreSiteDetails(s.getMoreSiteDetailsFields(),String.valueOf(ret),nwLevel,user );
		}		
		if(ret!=0 &&(s.getSiteStatusDate()!=null || s.getSiteStatusNotes()!=null)){
			System.out.println("NetworkServiceDao:::saveMultipleNetworks:::s.getMoreSiteDetailsFields()::"+s.getMoreSiteDetailsFields());
			updateNetStatus(String.valueOf(ret),netStatus,s.getSiteStatusDate(),s.getSiteStatusNotes());
		}
		if(ret!=0){
			ObjectMap map = ((ObjectMapService) parameters.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_NETWORK, ret);
			OrganizationIdentifier oid = new OrganizationIdentifier();
			oid.setOID(map.getOID());
			oid.setPK(ret);			
			networkResponse.addAction(new CompletedAction(oid,siteName,String.valueOf(nwLevel), CRUDAction.CREATE));		 
		}			
	}
	return savedNtwIds;
}

public void updateNetwork(UpdateNetwork updateNetworkDetails,HashMap<String, Object> params) throws OperationException{
	
	UserBean userBean=(UserBean)params.get("callingUser");
	int userId=userBean.getUserId();
	ArrayList<NetSiteDetails> updateNetwork=(ArrayList<NetSiteDetails>) updateNetworkDetails.getNetworkSiteDetailsList();
	UserServiceHelper userServiceHelper=null;
	String pk_network="";
	NetSiteDetails networkDetails = null;
	String iaccId = (String)params.get(KEY_ACCOUNT_ID);
	String ipAdd=	AbstractService.IP_ADDRESS_FIELD_VALUE;
	response = (ResponseHolder)params.get("ResponseHolder");
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	int count=0;
	String pk_main_network="";
	String siteLevel="";
	String siteName="";
	String flag="updateNetwork";
	int dataFound=validateExistingData(updateNetworkDetails,params,flag);
	
	try {
		conn = CommonDAO.getConnection();
		StringBuffer sqlbuff = null;
		if(updateNetwork!=null && updateNetwork.size()>0){
			//this.response=new ResponseHolder();
		for(int i=0;i<updateNetwork.size();i++){
			networkDetails=updateNetwork.get(i);
			pk_main_network="";
			if((networkDetails.getNetIdentifier()!=null || networkDetails.getNetworkName()!=null)){
				
				if(networkDetails.getNetIdentifier()!=null){
					if(networkDetails.getNetIdentifier().getPK()!=null){
						pk_main_network=(networkDetails.getNetIdentifier().getPK()==null)?"":networkDetails.getNetIdentifier().getPK().toString();
					}
					else if(networkDetails.getNetIdentifier().getOID()!=null){
						pk_main_network = String.valueOf(((ObjectMapService)params.get("objectMapService")).getObjectPkFromOID(networkDetails.getNetIdentifier().getOID()));
					}
				}
				
				sqlbuff = new StringBuffer();
				sqlbuff.append(" SELECT pk_nwsites, nw_level,fk_site FROM er_nwsites,er_site WHERE er_site.fk_account=? and pk_site=fk_site and nw_level=0 ");
				
				if(!(pk_main_network.equals(""))){
					sqlbuff.append(" and pk_nwsites="+pk_main_network+"");
				}
				
				if(networkDetails.getNetworkName()!=null && pk_main_network.equals("")){
					sqlbuff.append(" and pk_nwsites in (select pk_nwsites from er_nwsites,er_site where pk_site=fk_site and lower(site_name) like lower('"+networkDetails.getNetworkName()+"') and nw_level=0)" );
				}
				
			}
				pstmt = conn.prepareStatement(sqlbuff.toString());
				pstmt.setInt(1, StringUtil.stringToNum(iaccId));

				rs=pstmt.executeQuery();
				
				while(rs.next()){
					pk_network=String.valueOf(rs.getInt("pk_nwsites"));
					siteLevel=String.valueOf(rs.getInt("NW_LEVEL"));
					count++;
				}
				if(count==0){
					response.addAction(new CompletedAction("No Networks found to be updated", CRUDAction.UPDATE));
				}
				
			if(!pk_network.equals(""))
			{
				if(networkDetails.getNewSiteRelationshipType()!=null){
					sqlbuff = new StringBuffer();
					sqlbuff.append("update er_nwsites set NW_MEMBERTYPE=?,LAST_MODIFIED_BY=? where PK_NWSITES=?");
					pstmt = conn.prepareStatement(sqlbuff.toString());
					pstmt.setInt(1, (Integer)params.get("pkNewRelationshipType_"+i));
					pstmt.setInt(2, userId);
					pstmt.setInt(3, StringUtil.stringToNum(pk_network));
					int tmpCount=pstmt.executeUpdate();
				}
				
				if(networkDetails.getSiteStatus()!=null){
					sqlbuff = new StringBuffer();
					sqlbuff.append("update er_status_history set STATUS_END_DATE=sysdate,LAST_MODIFIED_DATE=sysdate,STATUS_ISCURRENT=0 where STATUS_MODPK=? and STATUS_END_DATE is null");
					pstmt = conn.prepareStatement(sqlbuff.toString());
					pstmt.setInt(1, StringUtil.stringToNum(pk_network));
					count=pstmt.executeUpdate();
					if(count>0){
						sqlbuff = new StringBuffer();
						java.util.Date statusEnteredDate=DateUtil.stringToDate(networkDetails.getSiteStatusDate(), null);
						sqlbuff.append("INSERT INTO er_status_history(PK_STATUS,STATUS_MODPK,STATUS_MODTABLE,FK_CODELST_STAT,STATUS_DATE,STATUS_NOTES,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD,STATUS_ISCURRENT) ");
						sqlbuff.append("VALUES(SEQ_ER_STATUS_HISTORY.nextval,?,?,?,?,?,?,'N',sysdate,?,?)");
						pstmt = conn.prepareStatement(sqlbuff.toString());
						pstmt.setInt(1, StringUtil.stringToNum(pk_network));
						pstmt.setString(2,"er_nwsites");
						pstmt.setInt(3, (Integer)params.get("pkStatus_"+i));
						pstmt.setDate(4, DateUtil.dateToSqlDate(statusEnteredDate));
						pstmt.setString(5,networkDetails.getSiteStatusNotes());
						pstmt.setInt(6, userId);
						pstmt.setString(7, ipAdd);
						pstmt.setInt(8, 1);
						int tmpCount=pstmt.executeUpdate();
						if(count>0){
							NetworkDao ntDao=new NetworkDao();
							ntDao.saveNetworkStatusUpdate(StringUtil.stringToNum(pk_network),((Integer)params.get("pkStatus_"+i)).intValue(),userId);
							//count=ntDao.saveNetworkStatus(StringUtil.stringToNum(pk_network),((Integer)params.get("pkStatus_"+i)).intValue(),networkDetails.getSiteStatusDate(),networkDetails.getSiteStatusNotes());
						}
					  }
		    		}
				
				else if(networkDetails.getSiteStatusDate()!=null || networkDetails.getSiteStatusNotes()!=null){
					sqlbuff = new StringBuffer();
					sqlbuff.append("update er_status_history set status_date=to_date('"+networkDetails.getSiteStatusDate()+"','mm/dd/yyyy'), status_notes=? where STATUS_MODPK=? and STATUS_MODTABLE='er_nwsites' and STATUS_ISCURRENT=1 ");
					pstmt = conn.prepareStatement(sqlbuff.toString());
					pstmt.setString(1, networkDetails.getSiteStatusNotes());
					pstmt.setInt(2,StringUtil.stringToInteger(pk_network));
					int tmpCount=pstmt.executeUpdate();
				}

				if(networkDetails.getMoreSiteDetails()!=null){
					userServiceHelper=new UserServiceHelper();
					userServiceHelper.persistsMoreDetails(networkDetails.getMoreSiteDetails(),StringUtil.stringToNum(pk_network),userBean,params,CodeCache.CODE_TYPE_ORG+"_"+siteLevel);
				}
				
				ObjectMap map = ((ObjectMapService) params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_NETWORK, StringUtil.stringToNum(pk_network));
				SimpleIdentifier sid = new SimpleIdentifier();
				sid.setOID(map.getOID());
				sid.setPK(Integer.parseInt(pk_network));
				
				response.addAction(new CompletedAction(sid, CRUDAction.UPDATE));
			}
			
		}
		
		
	}
	
	}catch(OperationException ex){
		throw ex;
	}catch(SQLException ex){
		ex.printStackTrace();
	}catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			if(rs!=null)
			rs.close();
			if(pstmt!=null)
			pstmt.close();
			if(conn!=null)
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
public int validateExistingUsrRoles(int networkId,int userId,String roleName,int rolePk){
	int nwUserPk = 0;
	int pk_nwusers=0;
	PreparedStatement pstmt = null;
    Connection conn = null;
    ResultSet rs = null;
    int count=0;
    StringBuffer mainSql = new StringBuffer();
    
    mainSql.append(" select  pk_nwusers  from er_nwusers where  fk_nwsites =? AND fk_user =? ");
      try{
    	conn = getConnection();
    	pstmt = conn.prepareStatement(mainSql.toString());
    	pstmt.setInt(1, networkId);
    	pstmt.setInt(2, userId);
    	rs = pstmt.executeQuery();
    	while (rs.next()) {
    	 pk_nwusers=rs.getInt("pk_nwusers");
    	 
    	 if(pk_nwusers!=0){
    	 mainSql = new StringBuffer();	
    	 
    	 if(roleName.equals("additionalRole")){		
         mainSql.append(" (SELECT pk_nwusers as net_User  FROM er_nwusers  WHERE PK_NWUSERS    = "+pk_nwusers+"  AND NWU_MEMBERTROLE="+rolePk+")  UNION ");
    	 }
         mainSql.append(" (SELECT  PK_NWUSERS_ADDROLES as net_User   FROM er_nwusers_addnlroles  WHERE er_nwusers_addnlroles.FK_NWUSERS="+pk_nwusers+"  AND NWU_MEMBERADDLTROLE  = "+rolePk+") ");
         
         pstmt = conn.prepareStatement(mainSql.toString());
         rs = pstmt.executeQuery();
         while (rs.next()) {
        	 nwUserPk=rs.getInt("net_User");
        	 count++;
        	 
         }       
    	}    	
    	}
    	
    }catch(SQLException e){
    	Rlog.fatal("Network","EXCEPTION in validateExistingUsrRoles of NetworkServiceDao class " + e);	


    } finally {
		try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
    return count;
}

}
