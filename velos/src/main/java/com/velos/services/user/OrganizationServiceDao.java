package com.velos.services.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.ElementNSImpl;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.util.BrowserRows;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.FilterUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.model.FormField.SortOrder;
import com.velos.services.model.Code;
import com.velos.services.model.MoreDetailValue;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.OrganizationSearch;
import com.velos.services.model.OrganizationSearchResults;
import com.velos.services.model.UserSearch;

public class OrganizationServiceDao extends CommonDAO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6245749424889851001L;

	private static Logger logger = Logger.getLogger(OrganizationServiceDao.class);
	
	private static final String SPACE_STR = " ";
	private static final String ASC_STR = "asc";
	private static final String DEFAULT_ORDER_BY = "lower(site_name)";
	private static final String NULL_STR = "null";
	private static final String EMPTY_STR = "";
	private static final String COL_PK_SITE = "pk_site";
	private static final String COL_SITE_NAME = "site_name";
	private static final String COL_SITE_ID = "site_id";
	private static final String COL_SITE_TYPE = "SITE_TYPE";
	private static final String COL_SITE_TYPE_PK = "FK_CODELST_TYPE";
	private static final String COL_SITE_DESC = "SITE_INFO";
	private static final String COL_SITE_NOTES = "SITE_NOTES";
	private static final String COL_NCI_POID = "NCI_POID";
	private static final String COL_SITE_HIDDEN = "SITE_HIDDEN";
	private static final String COL_ADDRESS = "ADDRESS";
	private static final String COL_ADD_CITY = "ADD_CITY";
	private static final String COL_ADD_STATE = "ADD_STATE";
	private static final String COL_ADD_ZIPCODE = "ADD_ZIPCODE";
	private static final String COL_ADD_EMAIL = "ADD_EMAIL";
	private static final String COL_ADD_PHONE = "ADD_PHONE";
	private static final String COL_PARENT_SITE = "SITE_PARENT";
	private static final String COL_PARENT_SITE_NAME = "SITE_PARENT_NAME";
	private static final String COL_CTEP_ID="CTEP_ID";
	private static final String KEY_ORG_EXCEPTON="KEY_ORG_EXCEPTON";//Kavitha: #30101
	/*public static final String COL_MD_MODELEMENTPK = "CODELST_SUBTYP";
	public static final String COL_MD_MODELEMENTDATA = "MD_MODELEMENTDATA";
	public static final String COL_CODELST_TYPE = "CODELST_TYPE";
	public static final String COL_CODELST_DESC = "CODELST_DESC";
	public static final String COL_FK_MODPK = "FK_MODPK";
	public static final String COL_MD_MODNAME = "MD_MODNAME";*/
	
	
	public static final String KEY_ACCOUNT_ID = "accountId";
	public static final String KEY_SITE_PK = "pk_site";
	public static final String KEY_SITE_NAME = "siteName";
	public static final String KEY_PAGE = "page";
	public static final String KEY_PAGE_SIZE = "pageSize";
	public static final String KEY_ORDER_BY = "orderBy";
	public static final String KEY_ORDER_TYPE = "orderType";
	
	private Long totalCount = 0L;
	private Long pageSize = 0L;
	private Integer pageNumber = 0;
	
	private ArrayList<String> pkSiteList = new ArrayList<String>();
	private ArrayList<String> siteNameList = new ArrayList<String>();
	private ArrayList<String> siteTypeList = new ArrayList<String>();
	private ArrayList<String> siteTypePkList = new ArrayList<String>();
	private ArrayList<String> siteIdList = new ArrayList<String>();
	private ArrayList<String> siteDescList = new ArrayList<String>();
	private ArrayList<String> siteNotesList = new ArrayList<String>();
	private ArrayList<String> nciPOIDList = new ArrayList<String>();
	private ArrayList<String> siteHideList = new ArrayList<String>();
	private ArrayList<String> siteParentList = new ArrayList<String>();
	private ArrayList<String> siteParentNameList = new ArrayList<String>();
	private ArrayList<String> addressList = new ArrayList<String>();
	private ArrayList<String> addCityList = new ArrayList<String>();
	private ArrayList<String> addStateList = new ArrayList<String>();
	private ArrayList<String> addZipList = new ArrayList<String>();
	private ArrayList<String> addEmailList = new ArrayList<String>();
	private ArrayList<String> addPhoneList = new ArrayList<String>();
	private ArrayList<String> ctepList = new ArrayList<String>();
	
	/*private ArrayList<String> codelst_pk_key = new ArrayList<String>();
	private ArrayList<String> more_value = new ArrayList<String>();
	private ArrayList<String> codelst_typ = new ArrayList<String>();
	private ArrayList<String> more_description = new ArrayList<String>();*/
	
	
	/*public ArrayList<String> getKey() { return codelst_pk_key; }
	public ArrayList<String> getValue() { return more_value; }
	public ArrayList<String> getType() { return codelst_typ; }
	public ArrayList<String> getDescription() { return more_description; }*/

	public Long getTotalCount() { return totalCount; }
	public ArrayList<String> getPkSiteList() {
		return pkSiteList;
	}
	public ArrayList<String> getSiteNameList() {
		return siteNameList;
	}
	public ArrayList<String> getSiteTypeList() {
		return siteTypeList;
	}
	
	public ArrayList<String> getSiteTypePkList() {
		return siteTypePkList;
	}
	public ArrayList<String> getSiteIdList() {
		return siteIdList;
	}
	public ArrayList<String> getSiteDescList() {
		return siteDescList;
	}
	public ArrayList<String> getSiteNotesList() {
		return siteNotesList;
	}
	public ArrayList<String> getNciPOIDList() {
		return nciPOIDList;
	}
	public ArrayList<String> getSiteHideList() {
		return siteHideList;
	}
	public ArrayList<String> getSiteParentList() {
		return siteParentList;
	}
	public ArrayList<String> getSiteParentNameList() {
		return siteParentNameList;
	}
	public ArrayList<String> getAddressList() {
		return addressList;
	}
	public ArrayList<String> getAddCityList() {
		return addCityList;
	}
	public ArrayList<String> getAddStateList() {
		return addStateList;
	}
	public ArrayList<String> getAddZipList() {
		return addZipList;
	}
	public ArrayList<String> getAddEmailList() {
		return addEmailList;
	}
	public ArrayList<String> getAddPhoneList() {
		return addPhoneList;
	}
	public ArrayList<String> getCtepList() {
		return ctepList;
	}
	public void setCtepList(ArrayList<String> ctepList) {
		this.ctepList = ctepList;
	}
	public Long getPageSize() { return pageSize; }
	public Integer getPageNumber() { return pageNumber; }
	//Kavitha: #30101
	public void searchOrganizations(OrganizationSearch organizationSearch, HashMap<String, Object> params) throws OperationException{
		System.out.println("Inside Dao");
		int iaccId = StringUtil.stringToNum(params.get(KEY_ACCOUNT_ID).toString());

		int sitePk = 0;
		String sitename = "";
		String siteid = "";
		String siteCtepId = "";
		if(organizationSearch.getOrganizationIdent()!=null){
			System.out.println("Inside not null");
		if (organizationSearch.getOrganizationIdent().getPK()!= null && organizationSearch.getOrganizationIdent().getPK() >0) {
			System.out.println("Inside organizationSearch==="+organizationSearch);
			sitePk = organizationSearch.getOrganizationIdent().getPK();
			System.out.println("sitePk==="+sitePk);
		}
		/*String orgName = null;
		String orgAltId = null;
		Integer orgPk = null;
		if (organizationSearch.getOrganizationIdent() != null) {
			orgPk = organizationSearch.getOrganizationIdent().getPK();
			if (orgPk == null || orgPk < 1) {
				orgName = organizationSearch.getOrganizationIdent().getSiteName();
				if (StringUtil.isEmpty(orgName)) {
					orgAltId = organizationSearch.getOrganizationIdent().getSiteAltId();
				}
			}
		}*/

		
		if(organizationSearch.getOrganizationIdent().getSiteAltId()!=null &&
				!organizationSearch.getOrganizationIdent().getSiteAltId().equals("")){
			System.out.println("organizationSearch if==="+sitePk);
			siteid = organizationSearch.getOrganizationIdent().getSiteAltId();
			System.out.println("sitePk==="+sitePk);
		}
		if(organizationSearch.getOrganizationIdent().getSiteName()!=null &&
				!organizationSearch.getOrganizationIdent().getSiteName().equals("")){
			sitename = organizationSearch.getOrganizationIdent().getSiteName();
		}
		if(organizationSearch.getOrganizationIdent().getCtepId()!=null &&
				!organizationSearch.getOrganizationIdent().getCtepId().equals("")){
			siteCtepId = organizationSearch.getOrganizationIdent().getCtepId();
			System.out.println("siteCtepId=="+siteCtepId);
		}
		}
		 System.out.println("After CTEP");
		String keySearch = "";	
		String keyValue = "";
		if(organizationSearch.getMoreOrgDetails()!=null){
		Collection<MoreDetailValue> moreOrgDetails =organizationSearch.getMoreOrgDetails();
		System.out.println("moreDetails size"+moreOrgDetails.size());
		MoreDetailValue obj=new MoreDetailValue();
		
		OrganizationSearch organizationSearch1= new OrganizationSearch();
		System.out.println("Here");
		if(moreOrgDetails!=null){
			System.out.println("Inside not null");
			organizationSearch1.setMoreOrgDetails((java.util.List<MoreDetailValue>) moreOrgDetails);
			for(int i=0;i<organizationSearch1.getMoreOrgDetails().size();i++){
				System.out.println("Inside loop");
				obj=	organizationSearch1.getMoreOrgDetails().set(i, obj);
				if(keySearch.equals("")){
					keySearch	=obj.getKey()==null?"":obj.getKey();
					keySearch = "'"+obj.getKey()+"'";
				}else{
					String tmpKey = obj.getKey()==null?"":obj.getKey();
					keySearch = keySearch+",'"+tmpKey+"'";
				}
				//Kavitha: #30101
				String type= "org";
				int mdId = 0;
				OrganizationSearchResults organizationSearchResults = (OrganizationSearchResults)params.get("organizationSearchResults");					
				Code code = new Code();
				code.setCode(keySearch.contains("'")?keySearch.replace("'",""):keySearch);
				code.setType(type);
				NetworkServiceDao networkServiceObj=new NetworkServiceDao();
				mdId = networkServiceObj.getCodeByMultiInput(code,type).intValue();									
				if(mdId==0){	
					organizationSearchResults.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Organization not found")); 
					throw new OperationException();
				}
				if(obj.getValue()!=null){
				if(obj.getValue() instanceof ElementNSImpl){
				  ElementNSImpl value=(ElementNSImpl)obj.getValue();
				  
				  if(value.getChildNodes()!=null){
					 NodeList node=  	value.getChildNodes();

			          //String getvalue="";
			          for(int j=0;j< node.getLength();j++)
			           {
			           Node node1=node.item(j);
			           if(keyValue.equals("")){
			        	   if(node1.getNodeValue()!=null){
			        		   keyValue="'"+node1.getNodeValue()+"'";
			        	   }
			           }else{
			        	   if(node1.getNodeValue()!=null){
			        		   keyValue=keyValue+",'"+node1.getNodeValue()+"'";
			        	   }
			           }
			            }
			          
			          System.out.println("Inside If--->"+keyValue);
			          
				  }else{
					if(value.getNodeValue()!=null && !value.getNodeValue().equals("")){
						if(keyValue.equals("")){
				        	   if(value.getNodeValue()!=null){
				        		   keyValue="'"+value.getNodeValue()+"'";
				        	   }
				           }else{
				        	   if(value.getNodeValue()!=null){
				        		   keyValue=keyValue+",'"+value.getNodeValue()+"'";
				        	   }
				           }
					}
					  System.out.println("Inside Else--->"+keyValue);
				  }
				}
				else{
					if(keyValue.equals("")){
			        	keyValue="'"+String.valueOf(obj.getValue())+"'";
			        }else{
			        	 keyValue=keyValue+",'"+String.valueOf(obj.getValue())+"'";
			        }
					System.out.println("Inside Else1--->"+keyValue);
					
				}
				}
			}
			
			
		}
		}
		int curPage = organizationSearch.getPageNumber();
		if (curPage < 1) {
			curPage = 1;
		}
		pageNumber = curPage;
		long rowsPerPage = Configuration.MOREBROWSERROWS;
		String inputPageSize = params.get(KEY_PAGE_SIZE).toString();
		if (!StringUtil.isEmpty(inputPageSize)) {
			try {
				rowsPerPage = Long.parseLong(inputPageSize);
			} catch(Exception e) {
				rowsPerPage = Configuration.MOREBROWSERROWS;
			}
		}
		if (rowsPerPage > 50L) {
			rowsPerPage = 50L;
		}
		//Kavitha: #30101
		String orderBy = "";
		if(params.containsKey(KEY_ORDER_BY)){
			orderBy=FilterUtil.sanitizeTextForSQL(params.get(KEY_ORDER_BY).toString());
		}				
		if (StringUtil.isEmpty(orderBy)) {
			orderBy = DEFAULT_ORDER_BY;
		}
		//Kavitha: #30101
		String orderType = "";
		if(params.containsKey(KEY_ORDER_TYPE)){
			orderType=FilterUtil.sanitizeTextForSQL(params.get(KEY_ORDER_TYPE).toString());
		}				
		if (StringUtil.isEmpty(orderType)) {
			orderType = ASC_STR;
		}
		
		String lWhere = null;
		String lnWhere = null;
		String mWhere = null;
 	 	String fWhere = null;
		String oWhere = null;
		String rWhere = null;
		String aWhere = null;
		String gWhere = null;
		String sWhere = null;
		String keyWhere = null;
		String mdWhere = null;
		String cWhere = null;

 	 	StringBuffer completeSelect = new StringBuffer();
 	     //Kavitha: #30101
 	 	//String searchSiteNameForSQL= StringUtil.escapeSpecialCharSQL(sitename);
 	 	String searchSiteNameForSQL="";
 	 	if (sitename.indexOf("'") >= 0){
 	 		sitename = StringUtil.replace(sitename, "'", "''");
 	 	} 	 	
 	 	//String searchSiteIDForSQL= StringUtil.escapeSpecialCharSQL(siteid);
 	 	String searchSiteIDForSQL="";
 	 	if (siteid.indexOf("'") >= 0){
 	 		siteid = StringUtil.replace(siteid, "'", "''");
 	 	} 	 	
 	 	//String searchSiteCtepForSQL= StringUtil.escapeSpecialCharSQL(siteCtepId);
 	 	String searchSiteCtepForSQL="";
 	 	if (siteCtepId.indexOf("'") >= 0){
 	 		siteCtepId = StringUtil.replace(siteCtepId, "'", "''");
 	 	}
 	 	//String searchLoginNameForSQL= StringUtil.escapeSpecialCharSQL(uloginname);
 	 	//String searchUserEMailIDForSQL = StringUtil.escapeSpecialCharSQL(uemailId);
 	 	String searchkey1ForSQL=  keySearch;
 	 	String addMoreTables ="";
 	 	if(!keySearch.equals("")){
 	 		addMoreTables=", er_moredetails md,  ER_CODELST cd ";
 	 	}
 	 	String str1 = "SELECT distinct s.PK_SITE, s.site_name, s.site_id, (select CODELST_DESC from er_codelst where PK_CODELST = s.FK_CODELST_TYPE) as SITE_TYPE, s.FK_CODELST_TYPE, "
 				  + "  s.SITE_INFO,s.SITE_NOTES,s.NCI_POID, s.SITE_HIDDEN, s.CTEP_ID"
 				  + " , a.ADDRESS, a.ADD_CITY, a.ADD_STATE, a.ADD_ZIPCODE, a.ADD_EMAIL, a.ADD_PHONE "
 				  + " from  ER_ADD a, ER_SITE s ";
 				 if(!addMoreTables.equals("")){
 					  str1 += addMoreTables;
 				  }
 	 	 		  str1 += " Where  s.fk_peradd = a.pk_add ";
 	 	 		  str1 += " and s.SITE_HIDDEN <> 1";
 	 	
 	 	if (sitePk > 0) {
 	 		oWhere = " and s.PK_SITE = "+sitePk;
 	 	}
 	 	/*if (orgPk != null && orgPk > 0) {
 	 		oWhere = " and s.PK_SITE = "+orgPk;
 	 	} else {
 	 		if ((orgName != null) && (!orgName.trim().equals(EMPTY_STR) && !orgName.trim().equals(NULL_STR))) {
 	 			oWhere  = " and UPPER(s.SITE_NAME) = UPPER('" ;
 	 			oWhere += FilterUtil.sanitizeTextForSQL(orgName);
 	 			oWhere += "')";
 	 		} else if (!StringUtil.isEmpty(orgAltId)) {
 	 			oWhere  = " and UPPER(s.SITE_ALTID) = UPPER('" ;
 	 			oWhere += FilterUtil.sanitizeTextForSQL(orgAltId);
 	 			oWhere += "')";
 	 		}
 	 	}*/
		
		if ((sitename != null) && (!sitename.trim().equals(EMPTY_STR) && !sitename.trim().equals(NULL_STR))) {
			fWhere = " and UPPER(s.SITE_NAME) like UPPER('" ;
			fWhere+=searchSiteNameForSQL.trim();
			fWhere+="%')";
		}
		if ((siteid != null) && (!siteid.trim().equals(EMPTY_STR)  && !siteid.trim().equals(NULL_STR))) {
			lWhere = " and UPPER(s.SITE_ID) like UPPER('";
			lWhere+=searchSiteIDForSQL.trim();
			lWhere+="%')";
		}
		System.out.println("searchSiteCtepForSQL==="+searchSiteCtepForSQL);
		//VEL-591(chenchumohan)
		if ((siteCtepId != null) && (!siteCtepId.trim().equals(EMPTY_STR) && !siteCtepId.trim().equals(NULL_STR))) {
			cWhere = " and UPPER(s.CTEP_ID) like UPPER('%";
			cWhere+=siteCtepId.trim();
			cWhere+="%')";
		}
		/*if ((uloginname != null) && (!uloginname.trim().equals(EMPTY_STR)  && !uloginname.trim().equals(NULL_STR))) {
			lnWhere = " and u.USR_LOGNAME = '";
			lnWhere+=searchLoginNameForSQL.trim()+"'";
		}
		if ((uemailId != null) && (!uemailId.trim().equals(EMPTY_STR)  && !uemailId.trim().equals(NULL_STR))) {
			mWhere = " and a.ADD_EMAIL = '";
			mWhere+=searchUserEMailIDForSQL.trim()+"'";
		}*/
		
		
		completeSelect.append(str1);
		if (iaccId > 0) {
			aWhere = " and s.fk_account = " + iaccId;
			completeSelect.append(aWhere);
		}
		if (oWhere != null) {
			completeSelect.append(oWhere);
		}
		if (fWhere != null) {
			completeSelect.append(fWhere);
		}
		if (lWhere != null) {
			completeSelect.append(lWhere);
		}
		if (cWhere != null) {
			System.out.println("Inside adding cWhere");
			completeSelect.append(cWhere);
		}
		System.out.println("Inside KeySearch==="+keySearch.equals(""));
		if(!keySearch.equals("")){
			System.out.println("Inside KeySearch"+keySearch.equals(""));
			mdWhere=" AND md.fk_modpk        = s.PK_SITE";
			mdWhere+=" AND cd.pk_codelst        = md.md_modelementpk";
			mdWhere+=" AND cd.codelst_type      = 'org'";
			mdWhere+=" AND cd.codelst_subtyp    in ("+FilterUtil.sanitizeTextForSQL(keySearch)+")";
			
			if(!keyValue.equals("")){
				mdWhere+=" AND md.md_modelementdata in ("+FilterUtil.sanitizeTextForSQL(keyValue)+")";
				
			}
			completeSelect.append(mdWhere);
		}
	
		
		completeSelect.append(formSortOrder(organizationSearch));

		String formSql = completeSelect.toString();
		System.out.println("formSql===="+formSql);
		String countSql = "select count(*) from  ( " + completeSelect.toString() + ")";
		System.out.println("countSql===="+countSql);
		long totalPages = Configuration.PAGEPERBROWSER;

		BrowserRows br = new BrowserRows();
		br.getPageRows(curPage, rowsPerPage, formSql, totalPages, countSql, orderBy, orderType);
		
		long rowsReturned = br.getRowReturned();
		System.out.println("befopre foer===="+rowsReturned);
		for (int iX = 1; iX <= rowsReturned; iX++) {
			System.out.println("inside foer===="+iX);
			pkSiteList.add(br.getBValues(iX, COL_PK_SITE));
			siteNameList.add(br.getBValues(iX, COL_SITE_NAME));
			siteTypeList.add(br.getBValues(iX, COL_SITE_TYPE));
			siteIdList.add(br.getBValues(iX, COL_SITE_ID));
			siteTypePkList.add(br.getBValues(iX, COL_SITE_TYPE_PK));
			siteDescList.add(br.getBValues(iX, COL_SITE_DESC));
			siteNotesList.add(br.getBValues(iX, COL_SITE_NOTES));
			nciPOIDList.add(br.getBValues(iX, COL_NCI_POID));
			if(br.getBValues(iX, COL_SITE_HIDDEN).equals("1")){
				
				siteHideList.add("true");
			}
			else{
				
				siteHideList.add("false");
			}
			siteParentList.add(br.getBValues(iX, COL_PARENT_SITE));
			siteParentNameList.add(br.getBValues(iX, COL_PARENT_SITE_NAME));
			addressList.add(br.getBValues(iX, COL_ADDRESS));
			addCityList.add(br.getBValues(iX, COL_ADD_CITY));
			addStateList.add(br.getBValues(iX, COL_ADD_STATE));
			addZipList.add(br.getBValues(iX, COL_ADD_ZIPCODE));
			addEmailList.add(br.getBValues(iX, COL_ADD_EMAIL));
			addPhoneList.add(br.getBValues(iX, COL_ADD_PHONE));
			ctepList.add(br.getBValues(iX, COL_CTEP_ID));
			System.out.println(" ctepList==="+ctepList);
			System.out.println(" iX==="+iX);
			
			/*codelst_pk_key.add(br.getBValues(iX, COL_MD_MODELEMENTPK));
			more_value.add(br.getBValues(iX, COL_MD_MODELEMENTDATA));
			codelst_typ.add(br.getBValues(iX, COL_CODELST_TYPE));
			more_description.add(br.getBValues(iX, COL_CODELST_DESC));*/
			}
		totalCount = br.getTotalRows();
		pageSize = rowsReturned;
	}
	
	private String formSortOrder(OrganizationSearch organizationSearch) {
		System.out.println("inside formSortOrder===");
		if (organizationSearch.getSortBy() == null) {
			return SPACE_STR;
		}
		StringBuffer sb1 = new StringBuffer(" order by ");
		if (organizationSearch.getSortBy() == OrganizationSearch.OrgSearchOrderBy.PK) {
			sb1.append("s.pk_site");
		} else if (organizationSearch.getSortBy() == OrganizationSearch.OrgSearchOrderBy.siteName) {
			sb1.append("UPPER(s.site_name)");
		}
		
		if (organizationSearch.getSortOrder() == SortOrder.ASCENDING) {
			sb1.append(" ASC NULLS LAST ");
		} else {
			sb1.append(" DESC NULLS FIRST ");
		}
		return sb1.toString();
		
	}
	//Kavitha: #30101
	public static Integer getSitePKFromIdentifier(UserBean callingUser,OrganizationIdentifier orgIdentifier)throws MultipleObjectsFoundException{
		Connection conn=null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;		
		Integer sitePK = null;				
		String sqlQuery="select PK_SITE from ER_SITE where fk_account="+callingUser.getUserAccountId();
		if(orgIdentifier.getPK() != null  && !orgIdentifier.getPK().equals("")){
		 sqlQuery += " and PK_SITE="+orgIdentifier.getPK();
		}
		if(orgIdentifier.getOID()!=null && !orgIdentifier.getOID().trim().equals("")){
		 sqlQuery += " and PK_SITE=(select table_pk from er_object_map where system_id='"+orgIdentifier.getOID().trim()+"' and table_name='er_site')";
		}
		if(orgIdentifier.getSiteName()!=null && !orgIdentifier.getSiteName().trim().equals("")){
		 sqlQuery += " and lower(site_name)=lower('"+orgIdentifier.getSiteName().trim()+"')";
		}
		System.out.println("sqlQuery=="+sqlQuery);
		try{
		conn = CommonDAO.getConnection();
		pstmt = conn.prepareStatement(sqlQuery);
		rs = pstmt.executeQuery();
		while (rs.next()) {
			sitePK = rs.getInt("PK_SITE");
		}
		}catch (SQLException e) {
		e.printStackTrace();
		}
		
		finally{
		try {
		rs.close();
		
		pstmt.close();
		conn.close();
		} catch (SQLException e) {
		e.printStackTrace();
		}
		}
		return sitePK;		
		}
}
