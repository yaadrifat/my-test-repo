package com.velos.services.user;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.SessionContext;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.ElementNSImpl;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.velos.eres.business.address.impl.AddressBean;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.CtrlDao;
import com.velos.eres.business.common.MoreDetailsDao;
import com.velos.eres.business.common.SettingsDao;
import com.velos.eres.business.common.SiteDao;
import com.velos.eres.business.moreDetails.impl.MoreDetailsBean;
import com.velos.eres.business.site.impl.SiteBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.business.usrGrp.impl.UsrGrpBean;
import com.velos.eres.service.addressAgent.AddressAgentRObj;
import com.velos.eres.service.commonAgent.CommonAgentRObj;
import com.velos.eres.service.moreDetailsAgent.MoreDetailsAgentRObj;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.usrGrpAgent.UsrGrpAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.Security;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.site.SiteJB;
import com.velos.eres.web.user.UserJB;
import com.velos.services.AbstractService;
import com.velos.services.CRUDAction;
import com.velos.services.CodeNotFoundException;
import com.velos.services.CompletedAction;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.ValidationException;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Code;
import com.velos.services.model.NVPair;
import com.velos.services.model.NonSystemUser;
import com.velos.services.model.OrganizationDetail;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.OrganizationSearch;
import com.velos.services.model.OrganizationSearchResults;
import com.velos.services.model.User;
import com.velos.services.patientdemographics.PatientDemographicsHelper;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;

public class UserServiceHelper extends AbstractService
{

	private static Logger logger = Logger.getLogger(PatientDemographicsHelper.class);

	public void validateNonSystemUser(NonSystemUser nonSystemUser,Map<String, Object> parameters) throws ValidationException
	{
		
		List<Issue> validationIssues = new ArrayList<Issue>();
		if(nonSystemUser.getFirstName()== null || nonSystemUser.getFirstName().trim().length() == 0)
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"First Name may not be null"));
		}
		if(nonSystemUser.getLastName()== null || nonSystemUser.getLastName().trim().length() == 0)
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"Last Name may not be null"));
		}
		//Bug#15358 Raman
		if(nonSystemUser.getOrganization() == null ||((nonSystemUser.getOrganization().getOID()== null || nonSystemUser.getOrganization().getOID().length() == 0 )
				&&(nonSystemUser.getOrganization().getSiteAltId() == null || nonSystemUser.getOrganization().getSiteAltId().length() == 0) 
			&& (nonSystemUser.getOrganization().getSiteName() == null || nonSystemUser.getOrganization().getSiteName().length() == 0)
			&& (nonSystemUser.getOrganization().getPK() == null || nonSystemUser.getOrganization().getPK() < 1)))
		{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Organization may not be null")); 
		}
		
		if (validationIssues.size() > 0)
		{
			((ResponseHolder) parameters.get("ResponseHolder"))
			.getIssues().addAll(validationIssues); 
		if (logger.isDebugEnabled()) logger.debug("Validation Issues found for UserServiceImpl");
			throw new ValidationException();
		}
	}
	
	public int createNonSystemUser(NonSystemUser nonSystemUser,Map<String, Object> parameters) throws OperationException 
	{
		
		UserBean callingUser = (UserBean) parameters.get("callingUser");
		SiteAgentRObj siteAgent = (SiteAgentRObj) parameters.get("siteAgent");
		ObjectMapService objectMapService= (ObjectMapService) parameters.get("objectMapService");
		SessionContext sessionContext = (SessionContext) parameters.get("sessionContext"); 
		
		validateNonSystemUser(nonSystemUser,parameters);
		
		 String userAddId = null;
		 String userId = null;
		 
		AddressBean addressSKUser = new AddressBean();
		UserBean userSK = new UserBean();
				
		addressSKUser.setAddCity(nonSystemUser.getCity());
		addressSKUser.setAddPri(nonSystemUser.getAddress());
		addressSKUser.setAddState(nonSystemUser.getState());
		addressSKUser.setAddZip(nonSystemUser.getZip());
		addressSKUser.setAddCountry(nonSystemUser.getCountry());
		addressSKUser.setAddPhone(nonSystemUser.getPhone());
		
		if(nonSystemUser.getEmail() != null && nonSystemUser.getEmail().length() != 0)
		{	
			try 
			{
				String email = nonSystemUser.getEmail();
				InternetAddress emailAddr = new InternetAddress(email);
				emailAddr.validate();
				addressSKUser.setAddEmail(email);
			} 
			catch (AddressException ex)
			{
				((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
                           new Issue(
                           IssueTypes.DATA_VALIDATION, "Invalid Email Address : " + nonSystemUser.getEmail()));
                         throw new OperationException(); 
			}
		}
	
		userSK.setUserFirstName(nonSystemUser.getFirstName());
		userSK.setUserLastName(nonSystemUser.getLastName());
		
		if((nonSystemUser.getUserStatus() == null) || (nonSystemUser.getUserStatus().toString().length()==0))
		{	
			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
                     new Issue(
                     IssueTypes.DATA_VALIDATION, "User Status field is required. " ));
                   throw new OperationException();
			    
		}
	
		if(nonSystemUser.getUserStatus().getLegacyString().equals("B"))
		{
			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
                    new Issue(
                    IssueTypes.DATA_VALIDATION, "User Status can not be 'BLOCKED' " ));
                  throw new OperationException();
		}	
		userSK.setUserStatus(nonSystemUser.getUserStatus().getLegacyString());
		if(nonSystemUser.getJobType() != null && nonSystemUser.getJobType().getCode() != null)
		{
			try
			{	
				userSK.setUserCodelstJobtype(AbstractService.dereferenceCodeStr(nonSystemUser.getJobType(), CodeCache.CODE_TYPE_JOB_TYPE, callingUser));
			}
			catch(CodeNotFoundException e)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"User Job Type Code Not Found: " ));
				throw new OperationException();
			}
			
		}	
				
		if(nonSystemUser.getPrimarySpecialty() != null && nonSystemUser.getPrimarySpecialty().getCode() != null)
		{
			try
			{			
				userSK.setUserCodelstSpl(AbstractService.dereferenceCodeStr(nonSystemUser.getPrimarySpecialty(), CodeCache.CODE_TYPE_PRIMARY_SPECIALTY, callingUser));
			}
			catch(CodeNotFoundException e)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"Primary Speciality Code Not Found: " ));
				throw new OperationException();
			}
			
		}
		 userSK.setUserType(ObjectLocator.USER_TYPE_NON_SYSTEM);
		 userSK.setUserAccountId(callingUser.getUserAccountId());
		
	// --- checking site of User sending -----	 
		Integer sitePK = 0;
		try
		{
			 sitePK = ObjectLocator.sitePKFromIdentifier(callingUser, nonSystemUser.getOrganization(), sessionContext, objectMapService);
		}
		catch(MultipleObjectsFoundException moe)
		{
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "organizations not found for  OID:"
					+ nonSystemUser.getOrganization().getOID() + " siteAltID:" + nonSystemUser.getOrganization().getSiteAltId() + " siteName:" + nonSystemUser.getOrganization().getSiteName()));
			
			throw new OperationException();
		}
		if(sitePK==null || sitePK == 0)
		{
		((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "organizations  not found for  OID :"
					+ nonSystemUser.getOrganization().getOID() + " siteAltID:" + nonSystemUser.getOrganization().getSiteAltId() + " siteName:" + nonSystemUser.getOrganization().getSiteName()));
			
			throw new OperationException();
		}
		SiteBean sitebn = siteAgent.getSiteDetails(sitePK);
		if(sitebn == null)
		{
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "organizations  not found for sitePK :"
					+ nonSystemUser.getOrganization().getPK()));
			
			throw new OperationException();
		}	
			
//	 ---checking organization access rights of callingUser -----		
		SiteDao siteDao = new SiteDao();
		siteDao.getSitesByUser(callingUser.getUserId());
		boolean hasAccessright = false;
		for(int siteCount=0;siteCount<siteDao.getSiteIds().size();siteCount++)
		{
			if(sitePK.equals(siteDao.getSiteIds().get(siteCount)))
			{
				userSK.setUserSiteId((StringUtil.integerToString(sitePK)));
				hasAccessright=true;
				break;
			}
			
		}	
		/*if(!hasAccessright)
		{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.ORGANIZATION_AUTHORIZATION, "Calling User not authorised to add new user for site OID :"
						+ nonSystemUser.getOrganization().getOID() + " siteAltID:" + nonSystemUser.getOrganization().getSiteAltId() + " siteName:" + nonSystemUser.getOrganization().getSiteName()));
				throw new OperationException();
		}*/
		 try {
	            AddressAgentRObj addressUserAgentRObj = EJBUtil
	                    .getAddressAgentHome();
	            userAddId = String.valueOf(addressUserAgentRObj
	                    .setAddressDetails(addressSKUser));
	           	           
	            Rlog.debug("UserServiceHelper",
	                    "In UserServiceHelper.createUSer for user address");
	        } catch (Exception e) {
	            Rlog.fatal("accountWrapper",
	                    "Error in UserServiceHelper.createUser for user address "
	                            + e);
	            return -2;
	        }
		 userSK.setUserPerAddressId(userAddId);
		 try
		 {
	            UserAgentRObj userAgentRObj1 = EJBUtil.getUserAgentHome();
	            userId = String.valueOf(userAgentRObj1.setUserDetails(userSK));
	                 
	            Rlog.debug("UserServiceHelper",
	                    "In UserServiceHelper.createUser for user");
	     } catch (Exception e) {
	            Rlog
	                    .fatal("UserServiceHelper",
	                            "Error in UserServiceHelper.createUser for user "
	                                    + e);
	            return -2;
	        } 
		//Call User_More_Detail function 
		 if(nonSystemUser.getUserMoreDetails()!=null){
		 persistMoreDetails(nonSystemUser.getUserMoreDetails() , StringUtil.stringToNum(userId),callingUser,parameters,CodeCache.CODE_TYPE_USER);	 		 
		 }	
		
			 		 
		return (StringUtil.stringToNum(userId));
	}

	public void validateUser(User user, Map<String, Object> parameters) throws ValidationException
	{
		List<Issue> validationIssues = new ArrayList<Issue>();
		if(user.getFirstName()== null || user.getFirstName().trim().length() == 0)
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"First Name may not be null"));
		}
		if(user.getLastName()== null || user.getLastName().trim().length() == 0)
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"Last Name may not be null"));
		}
		if(user.getEmail() == null || user.getEmail().trim().length() == 0)
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"Email may not be null"));
		}	
		/*if(user.getTimeZoneId().getCode() == null || user.getTimeZoneId().getCode().trim().length() == 0)
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Time Zone may not be null"));
		}*/	
		if(user.getOrganization() == null ||((user.getOrganization().getOID()== null || user.getOrganization().getOID().length() == 0 )
				&&(user.getOrganization().getSiteAltId() == null || user.getOrganization().getSiteAltId().length() == 0) 
			&& (user.getOrganization().getSiteName() == null || user.getOrganization().getSiteName().length() == 0)))
		{
				validationIssues.add(new Issue(	IssueTypes.DATA_VALIDATION,	"Organization may not be null")); 
		}
		if((user.getUserDefaultGroup()== null) || ((user.getUserDefaultGroup().getOID() == null || user.getUserDefaultGroup().getOID().length() == 0) && (user.getUserDefaultGroup().getGroupName() == null || user.getUserDefaultGroup().getGroupName().trim().length() == 0)))
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "User Default Group may not be null"));
		}	
		if(user.getUserLoginName()== null || user.getUserLoginName().trim().length() == 0)
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"Login Name may not be null"));
		}
		if(user.getUserPassword()== null || user.getUserPassword().trim().length() == 0)
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"Password required "));
		}
		if(user.getUserESign()== null || user.getUserESign().trim().length() == 0)
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"eSign required "));
		}
		if (validationIssues.size() > 0)
		{
			((ResponseHolder) parameters.get("ResponseHolder"))
			.getIssues().addAll(validationIssues); 
		if (logger.isDebugEnabled()) logger.debug("Validation Issues found for UserServiceImpl");
			throw new ValidationException();
		}
		
		
	}
	
	public void validateOrg(OrganizationDetail organizationDetail, Map<String, Object> parameters) throws ValidationException{
		
		List<Issue> validationIssues = new ArrayList<Issue>();
		if(organizationDetail.getOrganization_Name()== null || organizationDetail.getOrganization_Name().trim().length() == 0)
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"Organization Name may not be null"));
		}
		if(organizationDetail.getOrganization_Type()== null || organizationDetail.getOrganization_Type().getCode().length() == 0)
		{
			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION,	"Organization type may not be null"));
		}
		if (validationIssues.size() > 0)
		{
			((ResponseHolder) parameters.get("ResponseHolder"))
			.getIssues().addAll(validationIssues); 
		if (logger.isDebugEnabled()) logger.debug("Validation Issues found for UserServiceImpl");
			throw new ValidationException();
		}
		
	}
	
	public int createUser(User user,Map<String, Object> parameters) throws OperationException, ParseException
	{
		
		UserBean callingUser = (UserBean) parameters.get("callingUser");
		ObjectMapService objectMapService= (ObjectMapService) parameters.get("objectMapService");
		SessionContext sessionContext = (SessionContext) parameters.get("sessionContext"); 
		UserAgentRObj userAgent = (UserAgentRObj) parameters.get("userAgent");
		UserSiteAgentRObj userSiteAgent = (UserSiteAgentRObj) parameters.get("userSiteAgent");
		validateUser(user,parameters);
	
		 String userAddId = null;
		 String userId = null;
		 String usrGrpId = null;
		 String uTimeZone ="";
		 String logouttime="";
		 String pwdexpires="";
		 String esignexpires="";
		 String siteUrl = "";
		 int rows = 0;
				 
		AddressBean addressSKUser = new AddressBean();
		UserBean userSK = new UserBean();
		UsrGrpBean usrGrpSK = new UsrGrpBean();
				
		addressSKUser.setAddCity(user.getCity());
		addressSKUser.setAddPri(user.getAddress());
		addressSKUser.setAddState(user.getState());
		addressSKUser.setAddZip(user.getZip());
		addressSKUser.setAddCountry(user.getCountry());
		addressSKUser.setAddPhone(user.getPhone());
	  
		// -- default setting for user account 

					CommonAgentRObj commonAgentRObj = EJBUtil.getCommonAgentHome();
					SettingsDao settingsDao = commonAgentRObj.getSettingsInstance();
					int modname=1;
					int accId = (StringUtil.stringToNum(callingUser.getUserAccountId()));
					settingsDao.retrieveSettings((accId),modname);
					ArrayList keywords=settingsDao.getSettingKeyword();
					CodeDao codeDao = new CodeDao();
					codeDao.getTimeZones();
					ArrayList setvalues=settingsDao.getSettingValue();
					HashMap keywordHash = new HashMap();
					for(int i=0;i<keywords.size();i++)
					{	
						keywordHash.put(keywords.get(i).toString(),(setvalues.get(i)==null)?"":setvalues.get(i).toString()) ;
					}
					uTimeZone = String.valueOf(keywordHash.get("ACC_USER_TZ"));
					logouttime =  String.valueOf(keywordHash.get("ACC_SESSION_TIMEOUT"));
					pwdexpires = String.valueOf(keywordHash.get("ACC_DAYS_PWD_EXP"));
					esignexpires = String.valueOf(keywordHash.get("ACC_DAYS_ESIGN_EXP"));
						//ACC_DAYS_PWD_EXP_OVERRIDE
		
	// -----getting the groupPK from ObjectLocator ----
		
		Integer groupPK = null;
		try
		{
			groupPK = ObjectLocator.groupPKFromGroupIdentifier(callingUser, user.getUserDefaultGroup(), objectMapService);
		}
		catch(MultipleObjectsFoundException e)
		{
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
			new Issue(IssueTypes.DATA_VALIDATION,"Multiple records found for group :" +user.getUserDefaultGroup().getGroupName()));
			throw new OperationException();
		}
	
		if(groupPK == null)
		{
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.GROUP_NOT_FOUND, "Group not found for  OID : "+user.getUserDefaultGroup().getOID()+"  Group not found for  group name : "+user.getUserDefaultGroup().getGroupName()));
			
			throw new OperationException();
		}	
		else
		{
			userSK.setUserGrpDefault(groupPK.toString());
		}	
		
	// --- Validating  email	
		if(user.getEmail() != null && user.getEmail().length() != 0)
		{	
			try 
			{
				String email = user.getEmail();
				InternetAddress emailAddr = new InternetAddress(email);
				emailAddr.validate();
				addressSKUser.setAddEmail(email);
			} 
			catch (AddressException ex)
			{
				
				((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
                           new Issue(
                           IssueTypes.DATA_VALIDATION, "Invalid Email Address : " + user.getEmail()));
                         throw new OperationException(); 
			}
			
		}	
	
	      // --eSign checks -----	
			String eSign = user.getUserESign();
			boolean esignValue = false;
			if(eSign.matches("\\d+"))
			{
				esignValue=true;
			}
			else
			{
				esignValue=false;
			}	
			if(!esignValue)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(IssueTypes.DATA_VALIDATION,"e-Signature must be numeric. Please enter again. " ));
				throw new OperationException();
			}
			
			if(eSign.trim().length()<4)
			{

				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(IssueTypes.DATA_VALIDATION,"e-Signature must be atleast 4 characters long. Please enter again." ));
				throw new OperationException();
			}	
		
		// ---UserPassword checking ------	
			if(user.getUserPassword().trim().length()< 8)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(IssueTypes.DATA_VALIDATION," Password must be atleast 8 characters long. Please enter again." ));
				throw new OperationException();
			}	
		// --if userpassword and UserLogin name are same -------- 	
			if(user.getUserPassword().equals(user.getUserLoginName()))
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
									new Issue(IssueTypes.DATA_VALIDATION," Password cannot be same as Login Name. Please enter again. " ));
				throw new OperationException();
			}	
		
		//--- pwd expire date
			Date todayDate=new Date();
		    Calendar cal = Calendar.getInstance();
			cal.setTime(todayDate);
			cal.add(Calendar.DATE, (StringUtil.stringToNum(pwdexpires)));  // number of days to add to pwd expire date
			todayDate = cal.getTime(); 
			userSK.setUserPwdExpiryDate(DateUtil.dateToString(todayDate));
						
			//--- eSign expire date
			Date todayDate1=new Date();
		    Calendar cal1 = Calendar.getInstance();
			cal1.setTime(todayDate1);
			cal1.add(Calendar.DATE, (StringUtil.stringToNum(esignexpires)));  // number of days to add to eSign expire date
			todayDate1 = cal.getTime();
			userSK.setUserESignExpiryDate(DateUtil.dateToString(todayDate1));
		   			
			userSK.setUserFirstName(user.getFirstName());
			userSK.setUserLastName(user.getLastName());
			userSK.setUserSecQues(user.getUserSecurityQuestion());
			userSK.setUserAnswer(user.getUserSecurityAnswer());
			userSK.setUserPhaseInv(user.getUserTrialPhaseInvolvement());
			userSK.setUserWrkExp(user.getUserWorkExperience());
			userSK.setUserPwd(Security.encryptSHA(user.getUserPassword()));
			userSK.setUserESign(user.getUserESign());
			userSK.setUserSessionTime(logouttime);
			userSK.setUserPwdExpiryDays(pwdexpires);
			userSK.setUserESignExpiryDays(esignexpires);
			userSK.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE);
			userSK.setCreator(StringUtil.integerToString(callingUser.getUserId()));		
		
	// --- checks for more than one login name in DB ------		
			int count = 0;
			 try
			 {
		           userAgent = EJBUtil.getUserAgentHome();
		           count = userAgent.getCount(user.getUserLoginName());
		           Rlog.debug("user", "UserServiceHelper.getCount after Dao");
		     }
			 catch (Exception e) {
		            Rlog.fatal("user", "Error in getCount(String userLogin) in UserServiceHelper "
		                    + e);
		        }
			 if(count > 0)
			 {
				 ((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(IssueTypes.DATA_VALIDATION," The Login ID you have entered already exists. Please enter a new Login Id.***** " ));
					throw new OperationException();
			 }
			 else
			 {
				 userSK.setUserLoginName(user.getUserLoginName());
			 }
	
			 // --- checks for UserStatus ----		 
			
			if((user.getUserStatus() == null) || (user.getUserStatus().toString().length()==0))
			{	
				((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
	                     new Issue(
	                     IssueTypes.DATA_VALIDATION, "User Status field is required. " ));
	                   throw new OperationException();
				    
			}
			userSK.setUserStatus(user.getUserStatus().getLegacyString());
			
			if(user.getJobType() != null && user.getJobType().getCode() != null)
			{
				try
				{	
					userSK.setUserCodelstJobtype(AbstractService.dereferenceCodeStr(user.getJobType(), CodeCache.CODE_TYPE_JOB_TYPE, callingUser));
				}
				catch(CodeNotFoundException e)
				{
					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(
								IssueTypes.DATA_VALIDATION, 
								"User Job Type Code Not Found: " ));
					throw new OperationException();
				}
				
			}	
				
			
		//--- checks for User TimeZone -----
			if(user.getTimeZoneId() != null && user.getTimeZoneId().getCode() != null)
			{
				try
				{	
					userSK.setTimeZoneId(AbstractService.dereferenceTzCodeStr(user.getTimeZoneId(), "timezone", callingUser));
				}
				catch(CodeNotFoundException e)
				{
					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(
								IssueTypes.DATA_VALIDATION, 
								"Time Zone Code Not Found: " ));
					throw new OperationException();
				}
				
			}
		// ---- default timeZone --------	
			else
			{
				userSK.setTimeZoneId(uTimeZone);
						
			}	
		// --- checking primary specialty -----------	
			
			if(user.getPrimarySpecialty() != null && user.getPrimarySpecialty().getCode() != null)
			{
				try
				{			
					userSK.setUserCodelstSpl(AbstractService.dereferenceCodeStr(user.getPrimarySpecialty(), CodeCache.CODE_TYPE_PRIMARY_SPECIALTY, callingUser));
				}
				catch(CodeNotFoundException e)
				{
					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(
								IssueTypes.DATA_VALIDATION, 
								"Primary Speciality Code Not Found: " ));
					throw new OperationException();
				}
				
			}
			 userSK.setUserType(ObjectLocator.USER_TYPE_SYSTEM);
			 userSK.setUserAccountId(callingUser.getUserAccountId());
			  Integer sitePK = 0;
				try
				{
					 sitePK = ObjectLocator.sitePKFromIdentifier(callingUser, user.getOrganization(), sessionContext, objectMapService);
				}
				catch(MultipleObjectsFoundException moe)
				{
					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "organizations not found for  OID:"
							+ user.getOrganization().getOID() + " siteAltID:" + user.getOrganization().getSiteAltId() + " siteName:" + user.getOrganization().getSiteName()));
					
					throw new OperationException();
				}
				if(sitePK == null || sitePK == 0)
				{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "organizations not found for OID :"
							+ user.getOrganization().getOID() + " siteAltID:" + user.getOrganization().getSiteAltId() + " siteName:" + user.getOrganization().getSiteName()));
					
					throw new OperationException();
				}	
			// ---checking organization access rights of callingUser -----					
				
				SiteDao siteDao = new SiteDao();
				siteDao.getSiteValues(StringUtil.stringToInteger(callingUser.getUserAccountId()));
				boolean hasAccessright = false;
				for(int siteCount=0;siteCount<siteDao.getSiteIds().size();siteCount++)
				{
					if(sitePK.equals(siteDao.getSiteIds().get(siteCount)))
					{
						userSK.setUserSiteId((StringUtil.integerToString(sitePK)));
						hasAccessright=true;
						break;
					}
					
				}	
				if(!hasAccessright)
				{
						((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.ORGANIZATION_AUTHORIZATION, "User not authorised to add new user for site OID :"
								+ user.getOrganization().getOID() + " siteAltID:" + user.getOrganization().getSiteAltId() + " siteName:" + user.getOrganization().getSiteName()));
						throw new OperationException();
				}
				 try {
			            AddressAgentRObj addressUserAgentRObj = EJBUtil
			                    .getAddressAgentHome();
			            userAddId = String.valueOf(addressUserAgentRObj
			                    .setAddressDetails(addressSKUser));
			           	           
			            Rlog.debug("UserServiceHelper",
			                    "In UserServiceHelper.createUSer for user address");
			        } catch (Exception e) {
			            Rlog.fatal("accountWrapper",
			                    "Error in UserServiceHelper.createUser for user address "
			                            + e);
			            return -2;
			        }
				 userSK.setUserPerAddressId(userAddId);
				 try
				 {
			            UserAgentRObj userAgentRObj1 = EJBUtil.getUserAgentHome();
			            userId = String.valueOf(userAgentRObj1.setUserDetails(userSK));
			            
			            if (StringUtil.stringToInteger(userId) > 0) {
			            	userSiteAgent.createUserSiteData(StringUtil.stringToInteger(userId), StringUtil.stringToInteger(callingUser.getUserAccountId()),
			            			groupPK, 0, sitePK, callingUser.getUserId(), AbstractService.IP_ADDRESS_FIELD_VALUE, "N");
			            }
			                 
			            Rlog.debug("UserServiceHelper",
			                    "In UserServiceHelper.createUser for user");
			     } catch (Exception e) {
			            Rlog.fatal("UserServiceHelper",
			                            "Error in UserServiceHelper.createUser for user "
			                                    + e);
			           throw new OperationException();
			        } 
				
				    usrGrpSK.setUsrGrpUserId(userId);
			        usrGrpSK.setUsrGrpGroupId(groupPK.toString());	       
			        usrGrpSK.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE);
			        usrGrpSK.setCreator((StringUtil.integerToString(callingUser.getUserId())));
			        try {
			            UsrGrpAgentRObj usrGrpAgentRObj = EJBUtil.getUsrGrpAgentHome();
			            usrGrpId = String.valueOf(usrGrpAgentRObj
			                    .setUsrGrpDetails(usrGrpSK));
			            Rlog.debug("accountWrapper",
			                    "In AccountWrapperAgentBean.createUserGroup for user"
			                            + usrGrpId);
			        } catch (Exception e) {
			            Rlog.fatal("accountWrapper",
			                    "Error in AccountWrapperAgentBean.createUserGroup for user "
			                            + e);
			            throw new OperationException();
			        }
			        
			 // --- checks for sending notification to newly created user        
			    	
			        if(user.isSendNotifaction()!=null && user.isSendNotifaction()==true)
					{
						CtrlDao urlCtrl = new CtrlDao();	
						urlCtrl.getControlValues("site_url");
						rows = urlCtrl.getCRows();
						if (rows > 0) {
							siteUrl = (String) urlCtrl.getCValue().get(0);
						}
						 userAgent = EJBUtil.getUserAgentHome();
						 userAgent.notifyUserResetInfo(user.getUserPassword(), siteUrl, user.getUserESign(),StringUtil.stringToNum(userId), user.getUserLoginName(), "ID",(StringUtil.integerToString(callingUser.getUserId()))); //Modified By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.
						 userAgent.notifyUserResetInfo(user.getUserPassword(), siteUrl, user.getUserESign(),StringUtil.stringToNum(userId), user.getUserLoginName(), "P",(StringUtil.integerToString(callingUser.getUserId())));  //Modified By Aakriti for bug #17843 - Application Module - Audit trail shows a "New user/User Id/ERES" instead of the real user name.
										
					}
			        //Call User_More_Detail function 
					if(user.getUserMoreDetails()!=null){
						persistMoreDetails(user.getUserMoreDetails(),StringUtil.stringToNum(userId),callingUser,parameters,CodeCache.CODE_TYPE_USER);
					}
			    	 return (StringUtil.stringToNum(userId));
	}
	
	public int createOrganization(OrganizationDetail org,Map<String, Object> parameters) throws OperationException, ParseException
	{
		UserBean callingUser = (UserBean) parameters.get("callingUser");
		ObjectMapService objectMapService= (ObjectMapService) parameters.get("objectMapService");
		//SessionContext sessionContext = (SessionContext) parameters.get("sessionContext"); 
		SiteAgentRObj siteAgent = (SiteAgentRObj) parameters.get("siteAgent");
		validateOrg(org,parameters);
	
		 String orgAddId = null;
		 String siteId = null;
				 
		AddressBean addressSKOrg = new AddressBean();
		SiteBean orgSK = new SiteBean();
				
		addressSKOrg.setAddCity(org.getCity());
		addressSKOrg.setAddPri(org.getAddress());
		addressSKOrg.setAddState(org.getState());
		addressSKOrg.setAddZip(org.getZip_Code());
		addressSKOrg.setAddCountry(org.getCountry());
		addressSKOrg.setAddPhone(org.getContact_Phone());
		//validate org Address
		
		try{
		validOrgAdd(addressSKOrg,parameters);
		
		}catch(OperationException e){
			/*((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
                    new Issue(
                    IssueTypes.DATA_VALIDATION, "Invalid Email Address : vivek" ));*/
                  throw new OperationException(); 
		}
		
		
	// --- Validating  email	
		if(org.getContact_Email() != null && org.getContact_Email().length() != 0)
		{	
			try 
			{
				String email = org.getContact_Email();
				InternetAddress emailAddr = new InternetAddress(email);
				emailAddr.validate();
				addressSKOrg.setAddEmail(email);
			} 
			catch (AddressException ex)
			{
				
				((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
                           new Issue(
                           IssueTypes.DATA_VALIDATION, "Invalid Email Address : " + org.getContact_Email()));
                         throw new OperationException(); 
			}
			
		}					
		   			
			//orgSK.setSiteCodelstType(org.getType());
		if(org.getOrganization_Type() != null && org.getOrganization_Type().getCode() != null)
		{
			try
			{	
				orgSK.setSiteCodelstType(AbstractService.dereferenceCodeStr(org.getOrganization_Type(), CodeCache.CODE_TYPE_ORGG, callingUser));
			}
			catch(CodeNotFoundException e)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"Site Type Code Not Found: " ));
				throw new OperationException();
			}
			
		}
		
		if(org.getDescription()!=null && org.getDescription().length()>2000)
		{
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
					new Issue(IssueTypes.DATA_VALIDATION,"DESCRIPTION : Too Long Size" ));
			throw new OperationException();
		}
			orgSK.setSiteInfo(org.getDescription());
			if(org.getParent_Organization()!=null && org.getParent_Organization().getPK()!=null && org.getParent_Organization().getPK()>0)
				orgSK.setSiteParent(StringUtil.integerToString(org.getParent_Organization().getPK()));
			//orgSK.setSiteIdentifier(org.getOrg_id());
			if(org.getNCI_PO_ID()!=null && org.getNCI_PO_ID().length()>1020)
			{
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(IssueTypes.DATA_VALIDATION,"NCI_POID : Too Long Size" ));
				throw new OperationException();
			}
			orgSK.setPoIdentifier(org.getNCI_PO_ID());
			orgSK.setSiteNotes(org.getNotes());
			orgSK.setIpAdd(callingUser.getIpAdd());
			orgSK.setSiteAccountId(callingUser.getUserAccountId());
			orgSK.setCreator(StringUtil.integerToString(callingUser.getUserId()));
			
			
			
			if(org.getHide_Flag()!=null){
				if("true".equals(org.getHide_Flag()))
					orgSK.setSiteHidden("1");
				else{
					orgSK.setSiteHidden("0");
				}
			}
			
			if(org.getCTEPID()!=null){
				orgSK.setCtepId(org.getCTEPID());
			}
		
	// --- checks for more than one Organizaion name in DB ------		
			int count = 0;
			 try
			 {
		           siteAgent = EJBUtil.getSiteAgentHome();
		           count = siteAgent.findBySiteName(callingUser.getUserAccountId(),org.getOrganization_Name());
		           Rlog.debug("user", "UserServiceHelper.getCount after Dao");
		     }
			 catch (Exception e) {
		            Rlog.fatal("user", "Error in getCount(String userLogin) in UserServiceHelper "
		                    + e);
		        }
			 if(count > 0)
			 {
				 ((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(IssueTypes.DATA_VALIDATION," The Organization name you have entered already exists. Please enter a unique Organization name.***** " ));
					throw new OperationException();
			 }
			 else
			 {
				 orgSK.setSiteName(org.getOrganization_Name());
			 }
	
			 count = siteAgent.findBySiteIdentifier(callingUser.getUserAccountId(),org.getSite_Id());
			 
			 if(count > 0)
			 {
				 ((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(IssueTypes.DATA_VALIDATION," The Site ID you have entered already exists. Please enter a unique Site Id.***** " ));
					throw new OperationException();
			 }
			 else
			 {
				 orgSK.setSiteIdentifier(org.getSite_Id());
			 }
			
				 try {
			            AddressAgentRObj addressOrgAgentRObj = EJBUtil
			                    .getAddressAgentHome();
			            orgAddId = String.valueOf(addressOrgAgentRObj
			                    .setAddressDetails(addressSKOrg));
			           	           
			            Rlog.debug("UserServiceHelper",
			                    "In UserServiceHelper.createOrganization for Organization address");
			        } catch (Exception e) {
			            Rlog.fatal("accountWrapper",
			                    "Error in UserServiceHelper.createOrganization for Organization address "
			                            + e);
			            return -2;
			        }
				 
				 orgSK.setSitePerAdd(orgAddId);
				 
				 try
				 {
			            SiteAgentRObj siteAgentRObj1 = EJBUtil.getSiteAgentHome();
			            siteId = String.valueOf(siteAgentRObj1.setSiteDetails(orgSK));
			                 
			            Rlog.debug("UserServiceHelper",
			                    "In UserServiceHelper.createOrganization for Organization");
			     } catch (Exception e) {
			            Rlog.fatal("UserServiceHelper",
			                            "Error in UserServiceHelper.createOrganization for Organization "
			                                    + e);
			           throw new OperationException();
			        }
				 
				 ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, StringUtil.stringToNum(siteId));
					OrganizationIdentifier orgIdentifier = new OrganizationIdentifier();
					orgIdentifier.setOID(map.getOID());
					orgIdentifier.setPK(StringUtil.stringToNum(siteId));
					
				 
				 if(org.getMoreOrganizationDetails()!=null){
					 persistsMoreDetails(org.getMoreOrganizationDetails(),StringUtil.stringToNum(siteId),callingUser,parameters,CodeCache.CODE_TYPE_ORG);
				 }  
				 ((ResponseHolder) parameters.get("ResponseHolder")).addAction(new CompletedAction(orgIdentifier, CRUDAction.CREATE));
			     return (StringUtil.stringToNum(siteId));
	}
	
	public void persistMoreDetails(List<NVPair> moreDetails,int entityId,UserBean callingUser,Map<String, Object> parameters, String code_type) throws OperationException{
		
		int moreDetailsId = 0;
		
		CodeCache codes = CodeCache.getInstance();
			
		for (NVPair nvPair : moreDetails ){
			Code tempCode = new Code(code_type, nvPair.getKey(),nvPair.getDescription());
			String getvalue="";
			if(nvPair.getValue() instanceof ElementNSImpl){
		     ElementNSImpl value=(ElementNSImpl)nvPair.getValue();
		     
		     if(value!=null && value.getChildNodes()!=null){
			 NodeList nodeList=value.getChildNodes();
	          for(int i=0;i< nodeList.getLength();i++)
	           {
	           Node node=nodeList.item(i);
	           getvalue=node.getNodeValue();
	            }
		     }else{
		    	 if(value.getNodeValue()!=null && !value.getNodeValue().equals("")){
		    		 getvalue=value.getNodeValue();
		    	 }
		     }
			}else{
				getvalue=String.valueOf(nvPair.getValue());
			}
		  	 Integer codeListPK = null;
				try{
					codeListPK = codes.dereferenceCode(tempCode,code_type,EJBUtil.stringToNum(callingUser.getUserAccountId()));
				}
				catch(CodeNotFoundException e){
					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(IssueTypes.CODE_NOT_FOUND,"More_Details_Field_Not_Found: " + nvPair.getKey()));
						throw new OperationException();
					
					
				}
				
				MoreDetailsBean moreDetailBeans=new MoreDetailsBean();
				moreDetailBeans.setModId(EJBUtil.integerToString(entityId));
				moreDetailBeans.setModElementId(EJBUtil.integerToString(codeListPK));
				moreDetailBeans.setModName(code_type);
				moreDetailBeans.setModElementData(getvalue);
				moreDetailBeans.setCreator(callingUser.getUserId().toString());
				moreDetailBeans.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE);
				 try
				 {
			            MoreDetailsAgentRObj moreDetailsAgentRObj = EJBUtil.getMoreDetailsAgentHome();
			            moreDetailsId = moreDetailsAgentRObj.setMoreDetails(moreDetailBeans);
			                 
			            Rlog.debug("UserServiceHelper",
			                    "In UserServiceHelper.persistMoreDetails for MoreDetails");
			     } catch (Exception e) {
			            Rlog.fatal("UserServiceHelper",
			                            "Error in UserServiceHelper.persistMoreDetails for MoreDetails "
			                                    + e);
			           throw new OperationException();
			        }
									
			}
	}
	
	
	public int updateOrganisation(OrganizationDetail organizationDetail,Map<String, Object> parameters) throws OperationException, ParseException
	{
		UserBean callingUser = (UserBean) parameters.get("callingUser");
		ObjectMapService objectMapService= (ObjectMapService) parameters.get("objectMapService");
		//SessionContext sessionContext = (SessionContext) parameters.get("sessionContext"); 
		SiteAgentRObj siteAgent = (SiteAgentRObj) parameters.get("siteAgent");
		//validateOrg(organizationDetail,parameters);
	
		 String orgAddId = null;
		 String siteId = null;
				 
		AddressBean addressSKOrg = new AddressBean();
		SiteDao siteDao = new SiteDao();
		SiteBean orgSK = new SiteBean();
		addressSKOrg.setAddCity(organizationDetail.getCity());
		addressSKOrg.setAddPri(organizationDetail.getAddress());
		addressSKOrg.setAddState(organizationDetail.getState());
		addressSKOrg.setAddZip(organizationDetail.getZip_Code());
		addressSKOrg.setAddCountry(organizationDetail.getCountry());
		addressSKOrg.setAddPhone(organizationDetail.getContact_Phone());
		
	// --- Validating  email	
		if(organizationDetail.getContact_Email() != null && organizationDetail.getContact_Email().length() != 0)
		{	
			try 
			{
				String email = organizationDetail.getContact_Email();
				InternetAddress emailAddr = new InternetAddress(email);
				emailAddr.validate();
				addressSKOrg.setAddEmail(email);
			} 
			catch (AddressException ex)
			{
				
				((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
                           new Issue(
                           IssueTypes.DATA_VALIDATION, "Invalid Email Address : " + organizationDetail.getContact_Email()));
                         throw new OperationException(); 
			}
			
		}					
		   			
			//orgSK.setSiteCodelstType(AbstractService.dereferenceCodeStr(user.getJobType(), CodeCache.CODE_TYPE_JOB_TYPE, callingUser));
			if(organizationDetail.getOrganization_Type() != null && organizationDetail.getOrganization_Type().getCode() != null)
			{
				try
				{	
					orgSK.setSiteCodelstType(AbstractService.dereferenceCodeStr(organizationDetail.getOrganization_Type(), CodeCache.CODE_TYPE_ORGG, callingUser));
				}
				catch(CodeNotFoundException e)
				{
					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(
								IssueTypes.DATA_VALIDATION, 
								"Site Type Code Not Found: " ));
					throw new OperationException();
				}
				
			}	
			orgSK.setSiteInfo(organizationDetail.getDescription());
			if(organizationDetail.getParent_Organization()!=null && organizationDetail.getParent_Organization().getPK()!=null && organizationDetail.getParent_Organization().getPK()>0)
				orgSK.setSiteParent(StringUtil.integerToString(organizationDetail.getParent_Organization().getPK()));
			orgSK.setPoIdentifier(organizationDetail.getNCI_PO_ID());
			orgSK.setSiteNotes(organizationDetail.getNotes());
			orgSK.setIpAdd(IP_ADDRESS_FIELD_VALUE);
			orgSK.setSiteAccountId(callingUser.getUserAccountId());
			orgSK.setCreator(StringUtil.integerToString(callingUser.getUserId()));	
			
			if(organizationDetail.getHide_Flag()!=null){
				if("true".equals(organizationDetail.getHide_Flag()))
					orgSK.setSiteHidden("1");
				else{
					orgSK.setSiteHidden("0");
				}
			}
			
			if(organizationDetail.getCTEPID()!=null){
				orgSK.setCtepId(organizationDetail.getCTEPID());
			}
		
	// --- checks for more than one Organizaion name in DB ------
			int count = 0;
			if(organizationDetail.getOrganization_Name()!=null && organizationDetail.getOrganization_Name().length()>0){
			 try
			 {
		           siteAgent = EJBUtil.getSiteAgentHome();
		           count = siteAgent.findBySiteName(callingUser.getUserAccountId(),organizationDetail.getOrganization_Name());
		           Rlog.debug("user", "UserServiceHelper.getCount after Dao");
		     }
			 catch (Exception e) {
		            Rlog.fatal("user", "Error in getCount(String userLogin) in UserServiceHelper "
		                    + e);
		        }
			 int sitebysitePk = siteDao.getSitePKBySiteName(organizationDetail.getOrganization_Name(), StringUtil.stringToNum(callingUser.getUserAccountId()));
			 if(count > 0 && sitebysitePk!=organizationDetail.getOrganizationIdentifier().getPK())
			 {
				 ((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(IssueTypes.DATA_VALIDATION," Multiple Organization exists with same name: "+organizationDetail.getOrganization_Name() ));
					throw new OperationException();
			 }
			}
			if(organizationDetail.getOrganization_Name()!=null)
			 {
				 orgSK.setSiteName(organizationDetail.getOrganization_Name());
			 }
			if(organizationDetail.getSite_Id()!=null && organizationDetail.getSite_Id().length()>0){
			 count = siteAgent.findBySiteIdentifier(callingUser.getUserAccountId(),organizationDetail.getSite_Id());
			 int sitPkbysiteId = siteDao.getSitePK(organizationDetail.getSite_Id(), StringUtil.stringToInteger(callingUser.getUserAccountId()));
			 if(count > 0 && sitPkbysiteId!=organizationDetail.getOrganizationIdentifier().getPK())
			 {
				 ((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(IssueTypes.DATA_VALIDATION," Multiple Organization exists with same Site Id: "+organizationDetail.getSite_Id() ));
					throw new OperationException();
			 }
			 else
			 {
					orgSK.setSiteIdentifier(organizationDetail.getSite_Id());
				
			 }
			}else{
				orgSK.setSiteIdentifier("");
			}
			 siteDao = new SiteDao();
			 int sitePk = 0;
			 if(orgSK.getSiteName()!=null && orgSK.getSiteName().length()>0){
				 sitePk = siteDao.getSitePKBySiteName(orgSK.getSiteName(), StringUtil.stringToNum(callingUser.getUserAccountId()));
			 }else if(orgSK.getSiteIdentifier()!=null && orgSK.getSiteIdentifier().length()>0){
				 sitePk = siteDao.getSitePKBySiteName(orgSK.getSiteIdentifier(), StringUtil.stringToNum(callingUser.getUserAccountId()));
			 }
			 
			 if(sitePk>0){
				 organizationDetail.getOrganizationIdentifier().setPK(sitePk);
			 }
			 
			 if((organizationDetail.getOrganizationIdentifier().getPK()!=null && organizationDetail.getOrganizationIdentifier().getPK()>0) && sitePk<=0){
				 sitePk = organizationDetail.getOrganizationIdentifier().getPK();
			 }else{
				 if(sitePk<=0){
				 ((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(IssueTypes.DATA_VALIDATION,"Valid Organizaion Object is required" ));
					throw new OperationException();
				 }
			 }
			 			 
			 orgSK.setSiteId(sitePk);
			 SiteJB siteJB = new SiteJB();
			 siteJB.setSiteId(sitePk);
			 siteJB.getSiteDetails();
			 if((orgSK.getSiteName()==null || orgSK.getSiteName().length()<=0) && sitePk>0){
				 orgSK.setSiteName(siteJB.getSiteName());
			 }
			 if((orgSK.getSiteNotes()==null || orgSK.getSiteNotes().length()<=0) && sitePk>0){
				 orgSK.setSiteNotes(siteJB.getSiteNotes());
			 }
			 addressSKOrg.setAddId(StringUtil.stringToNum(siteJB.getSitePerAdd()));
			
				 try {
			            AddressAgentRObj addressOrgAgentRObj = EJBUtil
			                    .getAddressAgentHome();
			            orgAddId = String.valueOf(addressOrgAgentRObj
			                    .updateAddress(addressSKOrg));
			           	           
			            Rlog.debug("UserServiceHelper",
			                    "In UserServiceHelper.updateOrganisation for Organization address");
			        } catch (Exception e) {
			            Rlog.fatal("accountWrapper",
			                    "Error in UserServiceHelper.updateOrganisation for Organization address "
			                            + e);
			            return -2;
			        }
				 if(StringUtil.stringToNum(orgAddId)>0)
					 orgSK.setSitePerAdd(orgAddId);
				 try
				 {
			            SiteAgentRObj siteAgentRObj1 = EJBUtil.getSiteAgentHome();
			            siteId = String.valueOf(siteAgentRObj1.updateSite(orgSK));
			                 
			            Rlog.debug("UserServiceHelper",
			                    "In UserServiceHelper.updateOrganisation for Organization");
			     } catch (Exception e) {
			            Rlog.fatal("UserServiceHelper",
			                            "Error in UserServiceHelper.updateOrganisation for Organization "
			                                    + e);
			           throw new OperationException();
			        }
				 ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(sitePk));
					OrganizationIdentifier orgIdentifier = new OrganizationIdentifier();
					orgIdentifier.setOID(map.getOID());
					orgIdentifier.setPK(sitePk);
				 if(organizationDetail.getMoreOrganizationDetails()!=null){
					 persistsMoreDetails(organizationDetail.getMoreOrganizationDetails(),sitePk,callingUser,parameters,CodeCache.CODE_TYPE_ORG);

				 } 
					((ResponseHolder) parameters.get("ResponseHolder")).addAction(new CompletedAction(orgIdentifier, CRUDAction.UPDATE));

			     return (sitePk);
	}
	
	
	public int updateUserDetails(User user,Map<String, Object> parameters, String usrType) throws OperationException, ParseException
	{
		System.out.println("updateUserDetails===");
		UserBean callingUser = (UserBean) parameters.get("callingUser");
		ObjectMapService objectMapService= (ObjectMapService) parameters.get("objectMapService");
		SessionContext sessionContext = (SessionContext) parameters.get("sessionContext"); 
		UserAgentRObj userAgent = (UserAgentRObj) parameters.get("userAgent");
		//validateUser(user,parameters);
	
		 String userAddId = null;
		 String userId = null;
		 String usrGrpId = null;
		 String uTimeZone ="";
		 String logouttime="";
		 String pwdexpires="";
		 String esignexpires="";
		 String siteUrl = "";
		 int rows = 0;
		 
			// -- default setting for user account 

			CommonAgentRObj commonAgentRObj = EJBUtil.getCommonAgentHome();
			SettingsDao settingsDao = commonAgentRObj.getSettingsInstance();
			int modname=1;
			int accId = (StringUtil.stringToNum(callingUser.getUserAccountId()));
			settingsDao.retrieveSettings((accId),modname);
			ArrayList keywords=settingsDao.getSettingKeyword();
			CodeDao codeDao = new CodeDao();
			codeDao.getTimeZones();
			ArrayList setvalues=settingsDao.getSettingValue();
			HashMap keywordHash = new HashMap();
			for(int i=0;i<keywords.size();i++)
			{	
				keywordHash.put(keywords.get(i).toString(),(setvalues.get(i)==null)?"":setvalues.get(i).toString()) ;
			}
			uTimeZone = String.valueOf(keywordHash.get("ACC_USER_TZ"));
			logouttime =  String.valueOf(keywordHash.get("ACC_SESSION_TIMEOUT"));
			pwdexpires = String.valueOf(keywordHash.get("ACC_DAYS_PWD_EXP"));
			esignexpires = String.valueOf(keywordHash.get("ACC_DAYS_ESIGN_EXP"));
				//ACC_DAYS_PWD_EXP_OVERRIDE
			
		UserBean userSK = userAgent.getUserDetails(user.getPK());
		AddressBean addressSKUser = new AddressBean();
		
		if(userSK.getUserPerAddressId()!=null){
			try {
				AddressAgentRObj addressUserAgentRObj = EJBUtil
	                    .getAddressAgentHome();
			addressSKUser = addressUserAgentRObj.getAddressDetails(StringUtil.stringToInteger(userSK.getUserPerAddressId()));
			//fix for 29748 bug
			if(user.getCity()!=null && !user.getCity().trim().equals("")){
			 addressSKUser .setAddCity(user.getCity());
			}
			if(user.getAddress()!=null && !user.getAddress().trim().equals("")){
			 addressSKUser.setAddPri(user.getAddress());
			}
			if(user.getState()!=null && !user.getState().trim().equals("")){
			 addressSKUser.setAddState(user.getState());
			}
			if(user.getZip()!=null && !user.getZip().trim().equals("")){
			 addressSKUser.setAddZip(user.getZip());
			}
			if(user.getCountry()!=null && !user.getCountry().trim().equals("")){
			 addressSKUser.setAddCountry(user.getCountry());
			}
			if(user.getPhone()!=null && !user.getPhone().trim().equals("")){
			 addressSKUser.setAddPhone(user.getPhone());
			}
			// --- Validating  email	
			if(user.getEmail() != null && user.getEmail().length() != 0)
			{	
				try 
				{
					String email = user.getEmail();
					InternetAddress emailAddr = new InternetAddress(email);
					emailAddr.validate();
					addressSKUser.setAddEmail(email);
				} 
				catch (AddressException ex)
				{
					
					((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
	                           new Issue(
	                           IssueTypes.DATA_VALIDATION, "Invalid Email Address : " + user.getEmail()));
	                         throw new OperationException(); 
				}
				
			}	
			
	            userAddId = String.valueOf(addressUserAgentRObj
	                    .updateAddress(addressSKUser));
	            
	            userAddId = userSK.getUserPerAddressId();
	           	           
	            Rlog.debug("UserServiceHelper",
	                    "In UserServiceHelper.createUSer for user address");
	        } catch (Exception e) {
	        	((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"Address not Updated. " ));
	            Rlog.fatal("accountWrapper",
	                    "Error in UserServiceHelper.createUser for user address "
	                            + e);
	            return -2;
	        }
		}		
		
		Integer groupPK = null;
		if( user.getUserDefaultGroup() != null && (!user.getUserDefaultGroup().equals(""))){
			System.out.println("user.getUserDefaultGroup()==="+user.getUserDefaultGroup());
		try
		{
			if(!usrType.equals("N")){
				System.out.println("inside if usertype===");
				groupPK = ObjectLocator.groupPKFromGroupIdentifier(callingUser, user.getUserDefaultGroup(), objectMapService);
				System.out.println("groupPK==="+groupPK);
			}
		}
		catch(MultipleObjectsFoundException e)
		{
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
			new Issue(IssueTypes.DATA_VALIDATION,"Multiple records found for group :" +user.getUserDefaultGroup().getGroupName()));
			throw new OperationException();
		}
	
		if(groupPK == null)
		{
			System.out.println("inside if groupPK===");
			if(!usrType.equals("N")){
				System.out.println("inside if usrType===");
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.GROUP_NOT_FOUND, "Group not found for  OID : "+user.getUserDefaultGroup().getOID()+"  Group not found for  group name : "+user.getUserDefaultGroup().getGroupName()));
				System.out.println("ResponseHolder===");
				throw new OperationException();
			}
		}	
		else
		{
			System.out.println("inside else=========?>");
			userSK.setUserGrpDefault(groupPK.toString());
			System.out.println("userSK==="+userSK);
		}	
		}else{
			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
			new Issue(IssueTypes.DATA_VALIDATION,"userdefaultgroup is required in the input"));
			throw new OperationException();
		}
		
		  // --eSign checks -----	
		/*String eSign = user.getUserESign();
		boolean esignValue = false;
		if(eSign.matches("\\d+"))
		{ 
			esignValue=true;
		}
		else
		{
			esignValue=false;
		}	
		if(!esignValue)
		{
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
					new Issue(IssueTypes.DATA_VALIDATION,"e-Signature must be numeric. Please enter again. " ));
			throw new OperationException();
		}
		
		if(eSign.trim().length()<4)
		{

			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
					new Issue(IssueTypes.DATA_VALIDATION,"e-Signature must be atleast 4 characters long. Please enter again." ));
			throw new OperationException();
		}*/	
	
	// ---UserPassword checking ------	
		/*if(user.getUserPassword().trim().length()< 8)
		{
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
					new Issue(IssueTypes.DATA_VALIDATION," Password must be atleast 8 characters long. Please enter again." ));
			throw new OperationException();
		}	*/
	// --if userpassword and UserLogin name are same -------- 	
		/*if(user.getUserPassword().equals(user.getUserLoginName()))
		{
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
								new Issue(IssueTypes.DATA_VALIDATION," Password cannot be same as Login Name. Please enter again. " ));
			throw new OperationException();
		}*/	
		
		
		//--- pwd expire date
		Date todayDate=new Date();
	    Calendar cal = Calendar.getInstance();
		cal.setTime(todayDate);
		cal.add(Calendar.DATE, (StringUtil.stringToNum(pwdexpires)));  // number of days to add to pwd expire date
		todayDate = cal.getTime(); 
		//userSK.setUserPwdExpiryDate(DateUtil.dateToString(todayDate));
					
		//--- eSign expire date
		Date todayDate1=new Date();
	    Calendar cal1 = Calendar.getInstance();
		cal1.setTime(todayDate1);
		cal1.add(Calendar.DATE, (StringUtil.stringToNum(esignexpires)));  // number of days to add to eSign expire date
		todayDate1 = cal.getTime();
		//userSK.setUserESignExpiryDate(DateUtil.dateToString(todayDate1));
		//userSK.setUserFirstName(user.getFirstName());
		if(user.getFirstName()!=null && !user.getFirstName().trim().equals("")){
		  userSK.setUserFirstName(user.getFirstName());
		}	
		if(user.getLastName()!=null && !user.getLastName().trim().equals("")){
		  userSK.setUserLastName(user.getLastName());
		}
		//userSK.setUserSecQues(user.getUserSecurityQuestion());
		//userSK.setUserAnswer(user.getUserSecurityAnswer());
		if(user.getUserTrialPhaseInvolvement()!=null && !user.getUserTrialPhaseInvolvement().trim().equals("")){
		  userSK.setUserPhaseInv(user.getUserTrialPhaseInvolvement());
		}
		if(user.getUserWorkExperience()!=null && !user.getUserWorkExperience().trim().equals("")){
		  userSK.setUserWrkExp(user.getUserWorkExperience());
		}
		//userSK.setUserPwd(Security.encryptSHA(user.getUserPassword()));
		//userSK.setUserESign(user.getUserESign());
		//userSK.setUserSessionTime(logouttime);
		//userSK.setUserPwdExpiryDays(pwdexpires);
		//userSK.setUserESignExpiryDays(esignexpires);
		userSK.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE);
		userSK.setCreator(StringUtil.integerToString(callingUser.getUserId()));
		
		
		// --- checks for more than one login name in DB ------		
					int count = 0;
					 try
					 {
				           userAgent = EJBUtil.getUserAgentHome();
				           count = userAgent.getCount(user.getUserLoginName());
				           Rlog.debug("user", "UserServiceHelper.getCount after Dao");
				     }
					 catch (Exception e) {
				            Rlog.fatal("user", "Error in getCount(String userLogin) in UserServiceHelper "
				                    + e);
				        }
					 if(count > 1)
					 {
						 ((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
									new Issue(IssueTypes.DATA_VALIDATION," The Login ID you have entered already exists. Please enter a new Login Id.***** " ));
							throw new OperationException();
					 }
					 else
					 {
						 //userSK.setUserLoginName(user.getUserLoginName());
					 }
		
					// --- checks for UserStatus ----		 
						if((user.getUserStatus() == null) || (user.getUserStatus().toString().length()==0))
						{	
							((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
				                     new Issue(
				                     IssueTypes.DATA_VALIDATION, "User Status field is required. " ));
				                   throw new OperationException();
							    
						}
						userSK.setUserStatus(user.getUserStatus().getLegacyString());
						
						if(user.getJobType() != null && user.getJobType().getCode() != null)
						{
							try
							{	
								userSK.setUserCodelstJobtype(AbstractService.dereferenceCodeStr(user.getJobType(), CodeCache.CODE_TYPE_JOB_TYPE, callingUser));
							}
							catch(CodeNotFoundException e)
							{
								((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
										new Issue(
											IssueTypes.DATA_VALIDATION, 
											"User Job Type Code Not Found: " ));
								throw new OperationException();
							}
							
						}	
						//--- checks for User TimeZone -----
						if(user.getTimeZoneId() != null && user.getTimeZoneId().getCode() != null)
						{
							try
							{	
								userSK.setTimeZoneId(AbstractService.dereferenceTzCodeStr(user.getTimeZoneId(), "timezone", callingUser));
							}
							catch(CodeNotFoundException e)
							{
								((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
										new Issue(
											IssueTypes.DATA_VALIDATION, 
											"Time Zone Code Not Found: " ));
								throw new OperationException();
							}
							
						}
					// ---- default timeZone --------	
						else
						{
							userSK.setTimeZoneId(uTimeZone);
									
						}	// --- checking primary specialty -----------	
						
						if(user.getPrimarySpecialty() != null && user.getPrimarySpecialty().getCode() != null)
						{
							try
							{			
								userSK.setUserCodelstSpl(AbstractService.dereferenceCodeStr(user.getPrimarySpecialty(), CodeCache.CODE_TYPE_PRIMARY_SPECIALTY, callingUser));
							}
							catch(CodeNotFoundException e)
							{
								((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
										new Issue(
											IssueTypes.DATA_VALIDATION, 
											"Primary Speciality Code Not Found: " ));
								throw new OperationException();
							}
							
						}
						 //userSK.setUserType(ObjectLocator.USER_TYPE_SYSTEM);
						 userSK.setUserAccountId(callingUser.getUserAccountId());
						  Integer sitePK = 0;
						  if(user.getOrganization() != null && user.getOrganization().equals("")){
						  try
							{
								 sitePK = ObjectLocator.sitePKFromIdentifier(callingUser, user.getOrganization(), sessionContext, objectMapService);
							}
							catch(MultipleObjectsFoundException moe)
							{
								((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "organizations not found for  OID:"
										+ user.getOrganization().getOID() + " siteAltID:" + user.getOrganization().getSiteAltId() + " siteName:" + user.getOrganization().getSiteName()));
								
								throw new OperationException();
							}
							if(sitePK == null || sitePK == 0)
							{
							((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "organizations not found for OID :"
										+ user.getOrganization().getOID() + " siteAltID:" + user.getOrganization().getSiteAltId() + " siteName:" + user.getOrganization().getSiteName()));
								
								throw new OperationException();
							}
							
							// ---checking organization access rights of callingUser -----	
							if(!userSK.getUserSiteId().equals(sitePK)){
								userSK.setUserSiteFlag("S");
							 }
							
							SiteDao siteDao = new SiteDao();
							siteDao.getSiteValues(StringUtil.stringToInteger(callingUser.getUserAccountId()));
							boolean hasAccessright = false;
							for(int siteCount=0;siteCount<siteDao.getSiteIds().size();siteCount++)
							{
								if(sitePK.equals(siteDao.getSiteIds().get(siteCount)))
								{
									userSK.setUserSiteId((StringUtil.integerToString(sitePK)));
									hasAccessright=true;
									break;
								}
								
							}	
							if(!hasAccessright)
							{
									((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.ORGANIZATION_AUTHORIZATION, "User not authorised to add new user for site OID :"
											+ user.getOrganization().getOID() + " siteAltID:" + user.getOrganization().getSiteAltId() + " siteName:" + user.getOrganization().getSiteName()));
									throw new OperationException();
							}
						  } 
							 userSK.setUserPerAddressId(userAddId);
							 if(user.getHideUserInLookup()!=null && user.getHideUserInLookup())
								 userSK.setUserHidden("1");
							 else
								 userSK.setUserHidden("0");
							 try
							 {
						            UserAgentRObj userAgentRObj1 = EJBUtil.getUserAgentHome();
						            userId = String.valueOf(userAgentRObj1.updateUser(userSK));
						                 
						            Rlog.debug("UserServiceHelper",
						                    "In UserServiceHelper.createUser for user");
						     } catch (Exception e) {
						    	 ((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
											new Issue(
												IssueTypes.DATA_VALIDATION, 
												"User Not Updated" ));
						            Rlog.fatal("UserServiceHelper",
						                            "Error in UserServiceHelper.createUser for user "
						                                    + e);
						           throw new OperationException();
						        }
							 if(groupPK!=null && groupPK>0){
								 try {
								UsrGrpAgentRObj usrGrpAgentRObj = EJBUtil.getUsrGrpAgentHome();
							    UsrGrpBean usrGrpSK = new UsrGrpBean();
							    usrGrpSK = usrGrpAgentRObj.getUsrGrpDetails(groupPK);
							    if(usrGrpSK!=null){
							    usrGrpSK.setUsrGrpUserId(userId);
						        usrGrpSK.setUsrGrpGroupId(groupPK.toString());	       
						        usrGrpSK.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE);
						        usrGrpSK.setCreator((StringUtil.integerToString(callingUser.getUserId())));
						        usrGrpId = String.valueOf(usrGrpAgentRObj
					                    .updateUsrGrp(usrGrpSK));
							    }else{
							    	usrGrpSK = new UsrGrpBean();
							    	usrGrpSK.setUsrGrpUserId(userId);
							        usrGrpSK.setUsrGrpGroupId(groupPK.toString());	       
							        usrGrpSK.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE);
							        usrGrpSK.setCreator((StringUtil.integerToString(callingUser.getUserId())));
							        usrGrpId = String.valueOf(usrGrpAgentRObj
						                    .setUsrGrpDetails(usrGrpSK));
							    }
						          
						        } catch (Exception e) {
						        	((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
											new Issue(
												IssueTypes.DATA_VALIDATION, 
												"User Group not updated" ));
						            Rlog.fatal("accountWrapper",
						                    "Error in AccountWrapperAgentBean.createUserGroup for user "
						                            + e);
						            throw new OperationException();
						        }
							 }
		   			
			
			// SiteDao siteDao = new SiteDao();
			 int userPK = 0;
			 //if(orgSK.getSiteName()!=null && orgSK.getSiteName().length()>0){
				// sitePk = siteDao.getSitePKBySiteName(orgSK.getSiteName(), StringUtil.stringToNum(callingUser.getUserAccountId()));
			 //}else if(orgSK.getSiteIdentifier()!=null && orgSK.getSiteIdentifier().length()>0){
				// sitePk = siteDao.getSitePKBySiteName(orgSK.getSiteIdentifier(), StringUtil.stringToNum(callingUser.getUserAccountId()));
			 //}
			 
			 if((user.getPK()!=null && user.getPK()>0) || userPK<=0){
				 userPK = user.getPK();
			 }else{
				 addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid User Object is required")); 
					throw new OperationException();
			 }
			 			 
			 userSK.setUserId(userPK);
			 
			 UserJB userJB = new UserJB();
			 userJB.setUserId(userPK);
			 userJB.getUserDetails();
			 if((userSK.getUserFirstName()==null || userSK.getUserFirstName().length()<=0) && userPK>0){
				 userSK.setUserFirstName(userJB.getUserFirstName());
			 }
			 
			 if((userSK.getUserLastName()==null || userSK.getUserLastName().length()<=0) && userPK>0){
				 userSK.setUserLastName(userJB.getUserLastName());
			 }
			
				 if(user.getUserMoreDetails()!=null)
					 persistsMoreDetails(user.getUserMoreDetails(),userPK,callingUser,parameters,CodeCache.CODE_TYPE_USER);
			        
			     return (userPK);
	}
	public void persistsMoreDetails(List<NVPair> moreDetails,int siteId,UserBean callingUser,Map<String, Object> parameters, String code_type) throws OperationException{
		
		int moreDetailsId = 0;
		if (moreDetails.size()>0){
		CodeCache codes = CodeCache.getInstance();
		MoreDetailsAgentRObj siteMoreDetailsAgentRObj = EJBUtil.getMoreDetailsAgentHome();
		MoreDetailsBean moreDetailBeans=new MoreDetailsBean();
		MoreDetailsDao mdao = new MoreDetailsDao();
		mdao.getMoreDetails(siteId, code_type, "0");
			
		for (NVPair nvPair : moreDetails ){
			boolean isModify = false;
			Code tempCode = new Code(code_type, nvPair.getKey(),nvPair.getDescription());
		 //  System.out.println(tempCode.);
			String getvalue="";
			if(nvPair.getValue() instanceof ElementNSImpl){
		     ElementNSImpl value=(ElementNSImpl)nvPair.getValue();
		     
		     if(value!=null && value.getChildNodes()!=null){
			 NodeList nodeList=value.getChildNodes();
	          for(int i=0;i< nodeList.getLength();i++)
	           {
	           Node node=nodeList.item(i);
	           getvalue=node.getNodeValue();
	            }
		     }else{
		    	 if(value.getNodeValue()!=null && !value.getNodeValue().equals("")){
		    		 getvalue=value.getNodeValue();
		    		
		    	 }
		     }
			}
			else{
				getvalue=String.valueOf(nvPair.getValue());
				
			}
			System.out.println("getvalue==="+getvalue);
		  	 Integer codeListPK = null;
				try{
					codeListPK = codes.dereferenceCode(tempCode,code_type,EJBUtil.stringToNum(callingUser.getUserAccountId()));
				
				}
				catch(CodeNotFoundException e){
					((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
							new Issue(IssueTypes.CODE_NOT_FOUND,"More_Details_Field_Not_Found: " + nvPair.getKey()));
						throw new OperationException();
					
				}
				//System.out.println("befor calling code values");
			
				/**More details field validation(chkbox,radio)**/
				CodeDao codeDaoTemp=new CodeDao();
				codeDaoTemp.getCodeValuesByIds(EJBUtil.integerToString(codeListPK));
				if("radio".equals(codeDaoTemp.getCodeCustom().get(0))){
					//System.out.println("ghhhyrtyr");
				     if(codeDaoTemp.getCodeCustom1().get(0)!=null){
				    	 //System.out.println("fhngfhnjhj");
				    	 boolean chFlag=false;
				    	 String dataValue="";
				    	String customCol1= (String)codeDaoTemp.getCodeCustom1().get(0);
				    	try {
				    		JSONArray jsonArr=new JSONArray();;
							//JSONArray jsonArr = new JSONArray("[ "+customCol1+" ]");
							JSONObject htmlObj=new JSONObject(customCol1);
							if("checkbox".equals(codeDaoTemp.getCodeCustom().get(0)))
							 jsonArr=htmlObj.getJSONArray("chkArray");
							else
								 jsonArr=htmlObj.getJSONArray("radioArray");
							for (int i=0;i<jsonArr.length();i++){
							//	checkObj=jsonArr.getJSONObject(i);
								if(getvalue.equals(((JSONObject)jsonArr.getJSONObject(i)).getString("data")))
								chFlag=true;
								dataValue=dataValue+((JSONObject)jsonArr.getJSONObject(i)).getString("data")+",";
							}
							if(!chFlag)
							{
								((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
										new Issue(IssueTypes.DATA_VALIDATION,"Please Select Any one Value(" + dataValue+") For Key: "+nvPair.getKey()));
									throw new OperationException();	
							}
						} catch (JSONException e) {
							((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
									new Issue(IssueTypes.DATA_VALIDATION,"Data not Configured"));
								throw new OperationException();	
						}
				      }
				   
				}
			
				else if("checkbox".equals(codeDaoTemp.getCodeCustom().get(0))){
					//System.out.println("chkbox");
					 if(codeDaoTemp.getCodeCustom1().get(0)!=null){
						// System.out.println("inside checkbox");
						 boolean chFlag=false;
						 String existChk="";
				    	 String dataValue="";
				    	String customCol1= (String)codeDaoTemp.getCodeCustom1().get(0); 
				    	try {
				    		JSONArray jsonArr=new JSONArray();;
							//JSONArray jsonArr = new JSONArray("[ "+customCol1+" ]");
							JSONObject htmlObj=new JSONObject(customCol1);
							String [] getvalueArr=getvalue.split(",");
							jsonArr=htmlObj.getJSONArray("chkArray");
							for (int i=0;i<jsonArr.length();i++){
								for(int j=0;j<getvalueArr.length;j++){
									if(getvalueArr[j].equals(((JSONObject)jsonArr.getJSONObject(i)).getString("data")))
										existChk=existChk+getvalueArr[j]+",";
									//System.out.println("existChk"+existChk);
								}
								dataValue=dataValue+((JSONObject)jsonArr.getJSONObject(i)).getString("data")+",";
							}
							if(!"".equals(existChk))
							existChk=existChk.substring(0,existChk.length()-1);
							if(!existChk.equals(getvalue)){
								((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
										new Issue(IssueTypes.DATA_VALIDATION,"Please Select Any one Value(" + dataValue+") For Key: "+nvPair.getKey()));
									throw new OperationException();	
							}
							
				    	}catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					 }
					 
					else if(codeDaoTemp.getCodeCustom1().get(0)==null){
						 if(!"Y".equals(getvalue)){
							 if(!"N".equals(getvalue))
							 ((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
										new Issue(IssueTypes.DATA_VALIDATION,"Please Select Any one Value(Y OR N) For Key: "+nvPair.getKey()));
									throw new OperationException();	
						 }
									else if(!"N".equals(getvalue)){
										if(!"Y".equals(getvalue))
										((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
												new Issue(IssueTypes.DATA_VALIDATION,"Please Select Any one Value(Y OR N) For Key: "+nvPair.getKey()));
											throw new OperationException();	
									}
					 }
				}
				
				/**More details field validation(chkbox,radio)--End**/
				
				for (int x=0; x< mdao.getId().size(); x++){
					Integer msdPK = (Integer)mdao.getId().get(x);
					Integer msdCodePK = (Integer)mdao.getMdElementIds().get(x);

					
					if (msdCodePK.equals(codeListPK)){
						//if msdPK is is null, we know we have a new entry
						if (msdPK.equals(0)){
							moreDetailBeans = new MoreDetailsBean();
							isModify = false;
							break;
						}
						//get the legacy bean so we can update it
						moreDetailBeans =
								siteMoreDetailsAgentRObj.getMoreDetailsBean(msdPK);
						isModify = true;
						break;
					}
				
				}
				
				
				moreDetailBeans.setModId(EJBUtil.integerToString(siteId));
				moreDetailBeans.setModElementId(EJBUtil.integerToString(codeListPK));
				moreDetailBeans.setModName(code_type);
				moreDetailBeans.setModElementData(getvalue);
				moreDetailBeans.setCreator(callingUser.getUserId().toString());
				moreDetailBeans.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE);
				
				if (isModify){
					moreDetailsId = siteMoreDetailsAgentRObj.updateMoreDetails(moreDetailBeans);
					if (moreDetailsId == -2){
						((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
								new Issue(IssueTypes.ERROR_UPDATE_MD));
						throw new OperationException();
					}
				} 
				else{
					moreDetailsId = siteMoreDetailsAgentRObj.setMoreDetails(moreDetailBeans);
					if (moreDetailsId == -2){
						((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
								new Issue(IssueTypes.ERROR_CREATE_MD));
						throw new OperationException();
					}
					
				}
				
									
			}
		
		}	
	}
public void validOrgAdd(AddressBean addressBean,Map<String, Object> parameters) throws OperationException{
	if (addressBean.getAddZip()!=null && addressBean.getAddZip().length()>15){
		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
                new Issue(
                IssueTypes.DATA_VALIDATION, "ZIP :Too Long Size" ));
		throw new OperationException();
	}

	if (addressBean.getAddCity()!=null && addressBean.getAddCity().length()>30){
		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
                new Issue(
                IssueTypes.DATA_VALIDATION, "CITY :Too Song Size" ));
		throw new OperationException();
	}
	if (addressBean.getAddPri()!=null && addressBean.getAddPri().length()>50){
		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
                new Issue(
                IssueTypes.DATA_VALIDATION, "ADDRESS :Too Song Size" ));
		throw new OperationException();
	}
	if (addressBean.getAddState()!=null && addressBean.getAddState().length()>30){
		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
                new Issue(
                IssueTypes.DATA_VALIDATION, "STATE :Too Long Size" ));
		throw new OperationException();
	}
	if (addressBean.getAddCountry()!=null && addressBean.getAddCountry().length()>30){
		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
                new Issue(
                IssueTypes.DATA_VALIDATION, "COUNTRY :Too Long Size" ));
		throw new OperationException();
	}
	if (addressBean.getAddPhone()!=null && addressBean.getAddPhone().length()>30){
		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
                new Issue(
                IssueTypes.DATA_VALIDATION, "PHONE :Too Long Size" ));
		throw new OperationException();
	}
}
}

