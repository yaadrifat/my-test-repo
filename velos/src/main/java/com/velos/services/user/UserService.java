/**
 * 
 */
package com.velos.services.user;


import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.CreateNetwork;
import com.velos.services.model.CreateNetworkSiteUser;
import com.velos.services.model.Groups;
import com.velos.services.model.NetworkLevelSite;
import com.velos.services.model.NetworkSiteDetails;
import com.velos.services.model.NetworkSiteLevelDetails;
//import com.velos.services.model.NetworkSiteLevelSearchResult;
import com.velos.services.model.NetworkSiteSearchResults;
import com.velos.services.model.NetworkSiteUserDetails;
import com.velos.services.model.NetworkSiteUserListDetail;
import com.velos.services.model.NetworkSiteUserResult;
import com.velos.services.model.NetworkSiteWrapper;
import com.velos.services.model.Networks;
import com.velos.services.model.NetworksDetails;
import com.velos.services.model.NonSystemUser;
import com.velos.services.model.OrganizationDetail;
import com.velos.services.model.OrganizationSearch;
import com.velos.services.model.OrganizationSearchResults;
import com.velos.services.model.Organizations;
import com.velos.services.model.RemoveNetwork;
import com.velos.services.model.RemoveNetworkSite;
import com.velos.services.model.SiteLevelDetails;
import com.velos.services.model.UpdatNetworkSiteDetails;
import com.velos.services.model.UpdateNetwork;
import com.velos.services.model.UpdateNetwrokUsersWrapper;
import com.velos.services.model.User;
import com.velos.services.model.User.UserStatus;
import com.velos.services.model.UserIdentifier;
import com.velos.services.model.UserNetworkSiteResults;
import com.velos.services.model.UserNetworkSites;
import com.velos.services.model.UserNetworksDetail;
import com.velos.services.model.UserSearch;
import com.velos.services.model.UserSearchResults;
import com.velos.services.model.NetworkUserDetail;
import com.velos.services.model.NetworkUserDetails;
import com.velos.services.model.NetworkUsersSearchResult;
/**
 * @author dylan
 *
 */
@Remote
public interface UserService
{
	public static final String TABLE_NAME="er_user";
	public ResponseHolder changeUserStatus(UserIdentifier userId,UserStatus userStat) throws OperationException;
	public Groups getAllGroups() throws OperationException;
	public Groups getUserGroups(UserIdentifier userId) throws OperationException;
	public ResponseHolder createNonSystemUser(NonSystemUser nonSystemUser) throws OperationException; 
	public Organizations getAllOrganizations() throws OperationException;
	public ResponseHolder killUserSession(UserIdentifier userId) throws OperationException; 
    public ResponseHolder createUser(User user) throws OperationException ;
    public ResponseHolder updateUserDetails(User user) throws OperationException ;
    public ResponseHolder updateOrganisation(OrganizationDetail organizationDetail) throws OperationException ;
    public OrganizationSearchResults searchOrganisations(OrganizationSearch organizationSearch) throws OperationException ;
	public ResponseHolder checkESignature(String eSignature) throws OperationException;
	public UserSearchResults searchUser(UserSearch userSearch) throws OperationException;
	public ResponseHolder createOrganization(OrganizationDetail org)throws OperationException;
	public NetworkSiteSearchResults getNetworkSiteDetails(NetworkSiteDetails networkSiteDetails)throws OperationException;
	public Networks searchNetworks(NetworksDetails networkD)throws OperationException;
	public NetworkUsersSearchResult networkuser(NetworkUserDetail networkuser)throws OperationException;
	public NetworkUsersSearchResult networkusers(UserNetworksDetail networkuser)throws OperationException;
	public NetworkUsersSearchResult getNetworkUserDetails(NetworkUserDetails networkUserDetails)throws OperationException;
	public NetworkSiteSearchResults getNetworkSiteChildren(SiteLevelDetails siteLevelDetails)throws OperationException;
	public NetworkSiteSearchResults getNetworkSiteParent(SiteLevelDetails siteLevelDetails)throws OperationException;
	public NetworkSiteSearchResults getNetworkLevelSites(NetworkLevelSite networklevelsite)throws OperationException;
	public NetworkSiteSearchResults getNetworkSiteLevel(NetworkSiteLevelDetails networkSiteLevelDetails)throws OperationException;
	public UserNetworkSiteResults getUserNetworkSites(UserNetworkSites userNetworkSites)throws OperationException;
	public UserNetworkSiteResults getNetworkUser(NetworkSiteUserDetails networkSiteUserDetails)throws OperationException;
	public ResponseHolder removeNetworkSite(RemoveNetworkSite  networkSiteDetail)throws OperationException;
	public ResponseHolder removeNetworkUserSite(NetworkSiteUserListDetail networkSiteUserListDetail)throws OperationException;
	public void refreshMenuTabs() throws OperationException;
	public ResponseHolder createNetworkSite(NetworkSiteWrapper networkSite) throws OperationException;
	public ResponseHolder updateNetworkSite(UpdatNetworkSiteDetails networkSiteDetail)throws OperationException;
	public ResponseHolder createNetworkSiteUser(CreateNetworkSiteUser createNetworkSiteUser) throws OperationException;	
    public ResponseHolder updateNetworkSiteUser(UpdateNetwrokUsersWrapper networkSiteUser)throws OperationException;
    public ResponseHolder removeNetwork(RemoveNetwork  networkSiteDetail)throws OperationException;
    public ResponseHolder createNetwork(CreateNetwork network) throws OperationException;
    public ResponseHolder updateNetwork(UpdateNetwork updateNetworkDetails)throws OperationException;
}
