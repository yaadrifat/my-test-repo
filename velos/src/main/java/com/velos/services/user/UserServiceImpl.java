package com.velos.services.user;

import static javax.ejb.TransactionAttributeType.REQUIRED;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.xml.namespace.QName;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.GroupDao;
import com.velos.eres.business.common.MoreDetailsDao;
import com.velos.eres.business.common.SiteDao;
import com.velos.eres.business.common.UserDao;
import com.velos.eres.business.site.impl.SiteBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.codelstAgent.CodelstAgentRObj;
import com.velos.eres.service.groupAgent.GroupAgentRObj;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.ES;
import com.velos.eres.service.util.FilterUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.SVC;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.service.util.VelosResourceBundle;
import com.velos.eres.web.moreDetails.MoreDetailsJB;
import com.velos.eres.web.objectSettings.ObjectSettingsCache;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.CRUDAction;
import com.velos.services.CompletedAction;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Code;
import com.velos.services.model.CreateNetwork;
import com.velos.services.model.CreateNetworkSiteUser;
import com.velos.services.model.FormField.SortOrder;
import com.velos.services.model.GroupIdentifier;
import com.velos.services.model.Groups;
import com.velos.services.model.NVPair;
import com.velos.services.model.NetSiteDetails;
import com.velos.services.model.NetSiteUser;
import com.velos.services.model.Network;
import com.velos.services.model.NetworkIdentifier;
import com.velos.services.model.NetworkLevelSite;
import com.velos.services.model.NetworkSiteDetail;
import com.velos.services.model.NetworkSiteDetails;
import com.velos.services.model.NetworkSiteDetailsResult;
import com.velos.services.model.NetworkSiteLevelDetails;
import com.velos.services.model.NetworkSiteSearchResults;
import com.velos.services.model.NetworkSiteUserDetail;
import com.velos.services.model.NetworkSiteUserDetails;
import com.velos.services.model.NetworkSiteUserListDetail;
import com.velos.services.model.NetworkSiteWrapper;
//import com.velos.services.model.NetworkSiteLevelSearchResult;
import com.velos.services.model.NetworkSites;
import com.velos.services.model.NetworkSitesUsers;
import com.velos.services.model.NetworkUserDetail;
import com.velos.services.model.NetworkUserDetails;
import com.velos.services.model.NetworkUserSite;
import com.velos.services.model.NetworkUsersSearchResult;
import com.velos.services.model.NetworkUsersSite;
import com.velos.services.model.Networks;
import com.velos.services.model.NetworksDetails;
import com.velos.services.model.NonSystemUser;
import com.velos.services.model.OrganizationDetail;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.OrganizationSearch;
import com.velos.services.model.OrganizationSearch.OrgSearchOrderBy;
import com.velos.services.model.OrganizationSearchResults;
import com.velos.services.model.Organizations;
import com.velos.services.model.RemoveNetwork;
import com.velos.services.model.RemoveNetworkSite;
import com.velos.services.model.Site;
import com.velos.services.model.SiteIdentifier;
import com.velos.services.model.SiteLevelDetails;
import com.velos.services.model.Sites;
import com.velos.services.model.UpdatNetworkSiteDetails;
import com.velos.services.model.UpdateNetwork;
import com.velos.services.model.User;
import com.velos.services.model.User.UserStatus;
import com.velos.services.model.UserIdentifier;
import com.velos.services.model.UserNetwork;
import com.velos.services.model.UserNetworkDetail;
import com.velos.services.model.UserNetworkSite;
import com.velos.services.model.UserNetworkSiteResults;
import com.velos.services.model.UserNetworkSites;
import com.velos.services.model.UserNetworksDetail;
import com.velos.services.model.UserSearch;
import com.velos.services.model.UserSearch.UserSearchOrderBy;
import com.velos.services.model.UserSearchResults;
import com.velos.services.model.UpdateNetwrokUsersWrapper;
import com.velos.services.model.UsersNetworkSites;
import com.velos.services.study.MoreStudyDetailsDAO;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;


@Stateless
@Remote(UserService.class)
public class UserServiceImpl extends AbstractService implements UserService
{
	private static Logger logger = Logger.getLogger(UserService.class.getName());
	public static final String KEY_ACCOUNT_ID = "accountId";
	@EJB
	GrpRightsAgentRObj groupRightsAgent;

	@Resource
	private SessionContext sessionContext;

	@EJB
	private UserAgentRObj userAgent;

	@EJB
	private SiteAgentRObj siteAgent;
	@EJB
	private ObjectMapService objectMapService;
	
	@EJB
	private CodelstAgentRObj codeAgent;

	@EJB
	private GroupAgentRObj groupAgentRObj;
	
	@EJB
	private UserSiteAgentRObj userSiteAgent;

	public ResponseHolder changeUserStatus(UserIdentifier userId, UserStatus userStat)throws OperationException 
	{
		try
		{
				//-----------checking if UserIdentifier is null or not.
				if(userId==null)
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid User Identifier is required with OID or User Login Name or First Name and Last Name is required."));
					throw new OperationException(); 
				}
				//-----------checking if UserIdentifier is blank or not.
				//Bug#15349 Raman
				if((StringUtil.isEmpty(userId.getOID())) && (StringUtil.isEmpty(userId.getUserLoginName())) && (userId.getPK() == null || userId.getPK() == 0)
						&& (StringUtil.isEmpty(userId.getFirstName()) && 
								StringUtil.isEmpty(userId.getLastName())))
				{
			    	addIssue(new Issue(IssueTypes.USER_NOT_FOUND, " Valid UserIdentifier with OID or userLoginName or userPk or First and Last name is required "));
					throw new OperationException(); 
			    }	
				//------------------Checking calling user's group rights for manage Users------------------------------------------------------------------------------
				GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
				int manageUsers = authModule.getAdminManageUsersPrivileges().intValue();
				boolean hasEditUserPermissions = GroupAuthModule.hasEditPermission(Integer.valueOf(manageUsers));
				if (!hasEditUserPermissions)
				{
					addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION,"User is not authorized to update user's data"));
					throw new AuthorizationException("User is not authorized to edit user's data");
				}
				//--------checking if User account for the status change can be identified or not.
				UserBean userBean=null;
				try {
					userBean=ObjectLocator.userBeanFromIdentifier(callingUser, userId,userAgent, sessionContext, objectMapService);
					
				} catch(MultipleObjectsFoundException mce)
				{	
					addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, mce.getMessage()));
					throw new OperationException();
				}
				//--------checking if userBean is null or not OR User account for status change is System or Non System user
				//if((userBean==null) || (!userBean.getUserType().equalsIgnoreCase("S")))
				//--------checking if userBean is null or not.
				if((userBean==null))
				{
					addIssue(new Issue(IssueTypes.USER_NOT_FOUND,"User account for the status change not found."));
					throw new OperationException("User account for the status change not found.");
				}
				
				if((userStat==null) || (userStat.toString().length()==0))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION,"User Status field is required."));
					throw new OperationException("User Status field is required.");
				}
				if((userBean.getUserType().equalsIgnoreCase("N"))&&(userStat.getLegacyString().equalsIgnoreCase("B")))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION,"User Status can not be changed to BLOCKED"));
					throw new OperationException("User Status can not be changed to BLOCKED");
				}
				userBean.setUserStatus(userStat.getLegacyString());
			
				int chk=userAgent.updateUser(userBean);
				if(chk==-2)
				{
					addIssue(new Issue(IssueTypes.ERROR_UPDATING_USER_ACCOUNT_STATUS,"Error occured while updating user account status with OID : " +
							(objectMapService.getOrCreateObjectMapFromPK(TABLE_NAME, userBean.getUserId())).getOID()));
					throw new OperationException("Error occured while updating user account status ");
				}
				if(chk==0)
				{
					userId.setUserLoginName(userBean.getUserLoginName());
					userId.setOID(objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_USER, userBean.getUserId()).getOID());
					userId.setPK(userBean.getUserId());
					userId.setFirstName(userBean.getUserFirstName());
					userId.setLastName(userBean.getUserLastName());
					
				}
				
				
				response.addAction(new CompletedAction(userId, CRUDAction.UPDATE)); 
				return response;
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl update", e);
			throw new OperationRolledBackException(response.getIssues());
	
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl update", t);
			throw new OperationRolledBackException(response.getIssues());
	
		}
	}

	public Groups getAllGroups() throws OperationException 
	{
		try
		{
				//------------------Checking calling user's group rights for manage Users------------------------------------------------------------------------------
				GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
				int manageUsers = authModule.getAdminManageGroupsPrivileges().intValue();
				boolean hasViewUserPermissions = GroupAuthModule.hasViewPermission(Integer.valueOf(manageUsers));
				if (!hasViewUserPermissions)
				{
					addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION,"User is not authorized to view data"));
					throw new AuthorizationException("User is not authorized to view data");
				}
			
				GroupDao groupDao=groupAgentRObj.getByAccountId(StringUtil.stringToInteger(callingUser.getUserAccountId()));
				List<GroupIdentifier> grpList=new ArrayList<GroupIdentifier>();
				for(int i=0;i<groupDao.getGrpNames().size();i++)
				{
					GroupIdentifier group=new GroupIdentifier();
					group.setGroupName((groupDao.getGrpNames()).get(i).toString());
					group.setOID((objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_GROUPS,(Integer) groupDao.getGrpIds().get(i) )).getOID());
					group.setPK((Integer) groupDao.getGrpIds().get(i) );
					grpList.add(group);
				}
				Groups groups=new Groups();
				groups.addAll(grpList);
				return groups;
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl Retrieve", e);
			throw new OperationRolledBackException(response.getIssues());
	
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl Retrieve", t);
			throw new OperationRolledBackException(response.getIssues());
	
		}
				
	}
	public Groups getUserGroups(UserIdentifier userId) throws OperationException
	{
		ArrayList<GroupIdentifier> usrgrpList = new ArrayList<GroupIdentifier>();
		try
		{  
		    //------------------Checking calling user's group rights for manage users & manage groups ------------------------------------------------------------------------------
		    GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
			int manageUsers = authModule.getAdminManageUsersPrivileges().intValue();
			int manageGroups = authModule.getAdminManageGroupsPrivileges().intValue();
			boolean hasManageUserViewPermission = GroupAuthModule.hasViewPermission((Integer.valueOf(manageGroups)));
			boolean hasManageGroupsViewPermissions = GroupAuthModule.hasViewPermission((Integer.valueOf(manageUsers)));
					
			if (!hasManageUserViewPermission && !hasManageGroupsViewPermissions)
	 		{ 
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," CallingUser is not authorized to view user data "));
				throw new AuthorizationException(" Calling User is not authorized to view user data ");
			}
			
			if(userId == null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, " Valid UserIdentifier is required ")); 
				throw new OperationException();
			}
			//Bug#15365 Raman
			if((StringUtil.isEmpty(userId.getOID())) && (StringUtil.isEmpty(userId.getUserLoginName())) && (userId.getPK() == null || userId.getPK() == 0)
					&& (StringUtil.isEmpty(userId.getFirstName()) && 
							StringUtil.isEmpty(userId.getLastName())))
			{
		    	addIssue(new Issue(IssueTypes.USER_NOT_FOUND, " Valid UserIdentifier with OID or userLoginName or userPk or First and Last name is required "));
				throw new OperationException(); 
		    }	
		    	
		    UserBean usrbn = null;
		    try
		    {
		    	usrbn = ObjectLocator.userBeanFromIdentifier(callingUser, userId, userAgent, sessionContext, objectMapService);
		    }
		    catch (MultipleObjectsFoundException e) {
		    	addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, 
		    			"Multiple Users found")); 
		    	throw new OperationException(); 
		    }
		    if(usrbn == null ){
		        addIssue(new Issue(
		                IssueTypes.USER_NOT_FOUND, 
		                "User not found for given details."));
		        throw new OperationException();
		    }
		    int userID = usrbn.getUserId();
		    int acctID = (StringUtil.stringToNum(usrbn.getUserAccountId()));
		  	    
			CodeDao cd = new CodeDao();
	        cd.getUserGroups(userID, acctID);
	        cd.getCId();
	        cd.getCDesc();
	        for(int counter=0;counter<cd.getCId().size();counter++)
	        {
	           	GroupIdentifier usrgrp = new GroupIdentifier();
	        	usrgrp.setGroupName(cd.getCDesc().get(counter).toString());	        	
	        	usrgrp.setOID((objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_GROUPS, (Integer)cd.getCId().get(counter))).getOID());
	        	usrgrp.setPK((Integer)cd.getCId().get(counter));
	        	usrgrpList.add(usrgrp);
	        }
	        Groups userGroups=new Groups();
	        userGroups.addAll(usrgrpList);
	        return userGroups;  
		}
		catch(OperationException e){
			e.printStackTrace();
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl retrieved ", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl retrieved", t);
			throw new OperationException(t);
		}
				
	}
	
      //---------creating NonSystemUser ----------------------

	public ResponseHolder createNonSystemUser(NonSystemUser nonSystemUser)
			throws OperationException 
	{
		
	   try
	   {
		   //------------------Checking calling user's group rights for manage users------------------------------------------------------------------------------
		   
		GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
		int manageUsers = authModule.getAdminManageUsersPrivileges().intValue();
		boolean hasNewPermissions = GroupAuthModule.hasNewPermission((Integer.valueOf(manageUsers)));
		/*if (!hasNewPermissions)
		{
			addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," CallingUser is not authorized to view user data "));
			throw new AuthorizationException(" User is not authorized to view user data ");
		}*/
		if(nonSystemUser == null)
		{	
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid nonSystemUser Object is required")); 
			throw new OperationException(); 
		}

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("sessionContext", sessionContext); 
		parameters.put("userAgent", userAgent);
		parameters.put("objectMapService", objectMapService);
		parameters.put("callingUser", callingUser);
		parameters.put("ResponseHolder", response); 
		parameters.put("siteAgent", siteAgent);
		
		UserServiceHelper userServiceHelper = new UserServiceHelper();
		int nonSystemUserPK = userServiceHelper.createNonSystemUser(nonSystemUser,parameters);
	 		
		ObjectMap map = this.objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_USER, Integer.valueOf(nonSystemUserPK));
		UserIdentifier userIdentifier = new UserIdentifier();
		userIdentifier.setOID(map.getOID());
		userIdentifier.setPK(nonSystemUserPK);
		this.response.addAction(new CompletedAction(userIdentifier, CRUDAction.CREATE));
	}
	   catch(OperationException e){
			e.printStackTrace();
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created ", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created", t);
			throw new OperationException(t);
		}
	   
	   return response;
	}
	
	
		public Organizations getAllOrganizations() throws OperationException{
		
		try{
			
		        boolean isUserAUthorised=false;
		        GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
		        int manageUsers=authModule.getAdminManageUsersPrivileges().intValue();
		        if((GroupAuthModule.hasNewPermission(manageUsers) || (GroupAuthModule.hasEditPermission(manageUsers)) ))
		        {
		        	
		        	isUserAUthorised=true;
		        }
		        else 
		        {
		        	
		        	int manageOrganisation= authModule.getAdminManageOrganizationsPrivileges().intValue();
		        	if((GroupAuthModule.hasViewPermission(manageOrganisation)) || (GroupAuthModule.hasEditPermission(manageOrganisation)))
		        			{
			        			
			        			isUserAUthorised=true;
		        			}
		        
		        	
		        }
		        if(!isUserAUthorised)
		        {
		        	//throw exception
		        	addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION,"User is not authorized to view organization"));
					throw new AuthorizationException("User is not authorized to view organization");
		        	
		        }
		        
		  
			SiteDao siteDao= siteAgent.getByAccountId(StringUtil.stringToInteger(callingUser.getUserAccountId()));
			List<OrganizationIdentifier> orgList= new ArrayList<OrganizationIdentifier>();
			for(int counter=0;counter<siteDao.getSiteNames().size();counter++)
			{
				OrganizationIdentifier organization= new OrganizationIdentifier();
				organization.setSiteName(siteDao.getSiteNames().get(counter).toString());
				organization.setOID((objectMapService.getOrCreateObjectMapFromPK("er_site",(Integer) siteDao.getSiteIds().get(counter) )).getOID());
				organization.setPK((Integer) siteDao.getSiteIds().get(counter) ); 
				orgList.add(organization);
				
			}
			
			Organizations organizations=new Organizations();
			organizations.addAll(orgList);
			return organizations;
				
		}
		catch(OperationException e){
			e.printStackTrace();
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl retrieved ", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl Retrieve", t);
			throw new OperationRolledBackException(response.getIssues());
	
		}
		
	}
		
		
		
		public ResponseHolder updateUserDetails(User user) throws OperationException{
			
			try
				{
					  //------------------Checking calling user's group rights for manage users------------------------------------------------------------------------------
								
					GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
					int manageOrg = authModule.getAdminManageOrganizationsPrivileges().intValue();
					boolean hasNewPermissions = GroupAuthModule.hasNewPermission((Integer.valueOf(manageOrg)));
					if (!hasNewPermissions)
					{
						response.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to Update organizaion data "));
						throw new AuthorizationException(" User is not authorized to Update organizaion.");
					}
					if(user == null)
					{	
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid User Object is required")); 
						throw new OperationException(); 
					}
					if(user.getUserLoginName()==null && user.getPK()==null){
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid User Object is required with PK or loginName")); 
						throw new OperationException(); 
					}
					Integer userPK = 0;
					String usrType = "";
					UserBean userBean = null;
					if(user.getPK()!=null && user.getPK()>0){
						userBean = userAgent.getUserDetails(user.getPK());
						if(userBean==null){
							response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid User Object is required with PK")); 
							throw new OperationException(); 
						}else{
							userPK = userBean.getUserId();
							usrType= userBean.getUserType();
							user.setPK(userPK);
						}
					}
					if(user.getUserLoginName()!=null){
						UserDao userDao = new UserDao();
						userDao.getUserValuesByLoginName(user.getUserLoginName());
						if(userDao.getUsrIds().size()==0){
							response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid User Object is required with loginName")); 
							throw new OperationException();
						}else
						{
							userPK = (Integer)userDao.getUsrIds().get(0);
							user.setPK(userPK);
							usrType= (String) userDao.getUsrTypes().get(0);
						}
						
					}
					
					Map<String, Object> parameters = new HashMap<String, Object>();
					//parameters.put("sessionContext", sessionContext);
					parameters.put("objectMapService", objectMapService);
					parameters.put("callingUser", callingUser);
					parameters.put("siteAgent", siteAgent);
					parameters.put("ResponseHolder", response);
					parameters.put("userAgent", userAgent);
					
					UserServiceHelper userServiceHelper = new UserServiceHelper();
					int userID = userServiceHelper.updateUserDetails(user,parameters,usrType);
								
					ObjectMap map = this.objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_USER, Integer.valueOf(userPK));
					UserIdentifier userIdentifier = new UserIdentifier();
					userIdentifier.setOID(map.getOID());
					userIdentifier.setPK(userPK);
					this.response.addAction(new CompletedAction(userIdentifier, CRUDAction.UPDATE));
					
					
				}
				
				catch(OperationException e){
					e.printStackTrace();
					sessionContext.setRollbackOnly();
					if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created ", e);
				}
				catch(Throwable t){
					this.addUnknownThrowableIssue(t);
					if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created", t);
				}
				return response;
			}
		public ResponseHolder updateOrganisation(OrganizationDetail organizationDetail) throws OperationException{
			
		try
			{
				  //------------------Checking calling user's group rights for manage users------------------------------------------------------------------------------
							
				GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
				int manageOrg = authModule.getAdminManageOrganizationsPrivileges().intValue();
				boolean hasNewPermissions = GroupAuthModule.hasNewPermission((Integer.valueOf(manageOrg)));
				if (!hasNewPermissions)
				{
					response.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to Update organizaion data "));
					throw new AuthorizationException(" User is not authorized to Update organizaion.");
				}
				if(organizationDetail == null)
				{	
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid Organizaion Object is required")); 
					throw new OperationException(); 
				}
				// Organization Identifier
				if(organizationDetail.getOrganizationIdentifier()!=null){
				Integer sitePK = 0;
				try
				{
					 sitePK = ObjectLocator.sitePKFromIdentifier(callingUser, organizationDetail.getOrganizationIdentifier(), sessionContext, objectMapService);
				}
				catch(MultipleObjectsFoundException moe)
				{
					response.getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "organization not found for  OID:"
							+ organizationDetail.getOrganizationIdentifier().getOID() + " siteName:" + organizationDetail.getOrganizationIdentifier().getSiteName()));
					
					throw new OperationException();
				}
				if(sitePK==null || sitePK == 0)
				{
					response.getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "organization  not found for  OID :"
							+ organizationDetail.getOrganizationIdentifier().getOID() + " siteName:" + organizationDetail.getOrganizationIdentifier().getSiteName()));
					
					throw new OperationException();
				}else{
					organizationDetail.getOrganizationIdentifier().setPK(sitePK);
				}
				SiteBean sitebn = siteAgent.getSiteDetails(sitePK);
				if(sitebn == null)
				{
					response.getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "organization  not found for sitePK :"
							+ organizationDetail.getOrganizationIdentifier().getPK()));
					
					throw new OperationException();
				}
				}else{
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid Organizaion Identifier is required")); 
					throw new OperationException();
				}
				// Parent Organization Identifier
				if(organizationDetail.getParent_Organization()!=null){
				Integer siteParentPK = 0;
				try
				{
					 siteParentPK = ObjectLocator.sitePKFromIdentifier(callingUser, organizationDetail.getParent_Organization(), sessionContext, objectMapService);
				}
				catch(MultipleObjectsFoundException moe)
				{
					response.getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "Parent organization not found for  OID:"
							+ organizationDetail.getParent_Organization().getOID() + " siteName:" + organizationDetail.getParent_Organization().getSiteName()));
					
					throw new OperationException();
				}
				if(siteParentPK==null || siteParentPK == 0)
				{
					response.getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "Parent organization  not found for  OID :"
							+ organizationDetail.getParent_Organization().getOID() + " siteName:" + organizationDetail.getParent_Organization().getSiteName()));
					
					throw new OperationException();
				}else{
					organizationDetail.getParent_Organization().setPK(siteParentPK);
				}
				SiteBean sitebn = siteAgent.getSiteDetails(siteParentPK);
				if(sitebn == null)
				{
					response.getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "Parent organization  not found for sitePK :"
							+ organizationDetail.getParent_Organization().getPK()));
					
					throw new OperationException();
				}
				}
				if(organizationDetail.getHide_Flag()!=null){
					 if(!"true".equals(organizationDetail.getHide_Flag())){
						 if(!"false".equals(organizationDetail.getHide_Flag())){
							 response.getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Organization Hide Flag for User(true/false):"
										+ organizationDetail.getHide_Flag()));
						 throw new OperationException();
						 }
					 }
								else if(!"false".equals(organizationDetail.getHide_Flag())){
									if(!"true".equals(organizationDetail.getHide_Flag())){
										response.getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Organization Hide Flag for User(true/false):"
												+ organizationDetail.getHide_Flag()));
									throw new OperationException();
									}
								}
				}
				
				Map<String, Object> parameters = new HashMap<String, Object>();
				//parameters.put("sessionContext", sessionContext);
				parameters.put("objectMapService", objectMapService);
				parameters.put("callingUser", callingUser);
				parameters.put("siteAgent", siteAgent);
				parameters.put("ResponseHolder", response);
				
				UserServiceHelper userServiceHelper = new UserServiceHelper();
				int orgPK = userServiceHelper.updateOrganisation(organizationDetail,parameters);
				
				
			}
			
			catch(OperationException e){
				sessionContext.setRollbackOnly();
				if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created ", e);
				//throw new OperationRolledBackException(response.getIssues());
			}
			catch(Throwable t){
				sessionContext.setRollbackOnly();
				this.addUnknownThrowableIssue(t);
				if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created", t);
				//throw new OperationRolledBackException(response.getIssues());
			}
			return response;
		}
		//Kavitha: #30101
		public OrganizationSearchResults searchOrganisations(OrganizationSearch organizationSearch) throws OperationException{
			
			OrganizationSearchResults organizationSearchResults = new OrganizationSearchResults();
			OrganizationServiceDao organizationServiceDao = new OrganizationServiceDao();
			try
				{
					  //------------------Checking calling user's group rights for manage users------------------------------------------------------------------------------
				HashMap<String, Object> parameters = new HashMap<String, Object>();			
					GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
					int manageOrg = authModule.getAdminManageOrganizationsPrivileges().intValue();
					boolean hasNewPermissions = GroupAuthModule.hasNewPermission((Integer.valueOf(manageOrg)));
					if (!hasNewPermissions)
					{
						organizationSearchResults.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to Update organizaion data "));
						throw new AuthorizationException(" User is not authorized to Update organizaion.");
					}
					if(organizationSearch == null)
					{	
						organizationSearchResults.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid organizationSearch Object is required")); 
						throw new OperationException(); 
					}
					if(organizationSearch.getOrganizationIdent()==null && organizationSearch.getMoreOrgDetails()==null){
						organizationSearchResults.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid OrganizationIdentifier Object is required")); 
						throw new OperationException();
					}
					System.out.println("After If");
					//VEL-591(chenchumohan)
					if(organizationSearch.getOrganizationIdent()!=null 
						&& ( (organizationSearch.getOrganizationIdent().getPK()!=null && organizationSearch.getOrganizationIdent().getPK()>0)
						   ||(organizationSearch.getOrganizationIdent().getOID()!=null && !organizationSearch.getOrganizationIdent().getOID().trim().equals("")) 
						   || (organizationSearch.getOrganizationIdent().getSiteName()!=null && ! organizationSearch.getOrganizationIdent().getSiteName().trim().equals("")))
						|| (organizationSearch.getOrganizationIdent().getCtepId()!=null && !organizationSearch.getOrganizationIdent().getCtepId().trim().equals("")))
					{
					Integer sitePK = 0;
					if(organizationSearch.getOrganizationIdent()!=null 
							&& ( (organizationSearch.getOrganizationIdent().getPK()!=null && organizationSearch.getOrganizationIdent().getPK()>0)
							   ||(organizationSearch.getOrganizationIdent().getOID()!=null && !organizationSearch.getOrganizationIdent().getOID().trim().equals("")) 
							   || (organizationSearch.getOrganizationIdent().getSiteName()!=null && ! organizationSearch.getOrganizationIdent().getSiteName().trim().equals("")))){
							sitePK = organizationServiceDao.getSitePKFromIdentifier(callingUser, organizationSearch.getOrganizationIdent());
					}
					organizationSearch.getOrganizationIdent().setPK(sitePK);
					if((sitePK==null || sitePK==0) && 
							(organizationSearch.getOrganizationIdent().getCtepId()==null || organizationSearch.getOrganizationIdent().getCtepId().trim().equals(""))){
					  organizationSearchResults.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Organization not found")); 
					  throw new OperationException();							   	
					}
						// Organization Identifier
						parameters.put(OrganizationServiceDao.KEY_ACCOUNT_ID, callingUser.getUserAccountId());
						parameters.put(OrganizationServiceDao.KEY_PAGE_SIZE,organizationSearch.getPageSize()!=0 ? String.valueOf(organizationSearch.getPageSize()):"");
						if (organizationSearch.getSortBy() == null && organizationSearch.getSortOrder() == null) {
							organizationSearch.setSortBy(OrgSearchOrderBy.PK);
							organizationSearch.setSortOrder(SortOrder.DESCENDING);
						} else if (organizationSearch.getSortOrder() == null) {
							organizationSearch.setSortOrder(SortOrder.ASCENDING);
						}

						if (organizationSearch.getOrganizationIdent()!=null && organizationSearch.getOrganizationIdent().getPK()!=null && organizationSearch.getOrganizationIdent().getPK() > 0) {
							parameters.put(OrganizationServiceDao.KEY_SITE_PK,String.valueOf(organizationSearch.getOrganizationIdent().getPK()));
						}
						parameters.put("organizationSearchResults",organizationSearchResults);
						System.out.println("Before Dao");						
						organizationServiceDao.searchOrganizations(organizationSearch, parameters);
						 ArrayList<String> pkSiteList = organizationServiceDao.getPkSiteList();
						 ArrayList<String> siteNameList = organizationServiceDao.getSiteNameList();
						 ArrayList<String> siteTypeList = organizationServiceDao.getSiteTypeList();
						 ArrayList<String> siteTypePkList = organizationServiceDao.getSiteTypePkList();
						 ArrayList<String> siteIdList = organizationServiceDao.getSiteIdList();
						 ArrayList<String> siteDescList = organizationServiceDao.getSiteDescList();
						 ArrayList<String> siteNotesList = organizationServiceDao.getSiteNotesList();
						 ArrayList<String> nciPOIDList = organizationServiceDao.getNciPOIDList();
						 ArrayList<String> siteHideList = organizationServiceDao.getSiteHideList();
						 ArrayList<String> siteParentList = organizationServiceDao.getSiteParentList();
						 ArrayList<String> siteParentNameList = organizationServiceDao.getSiteParentNameList();
						 ArrayList<String> addressList = organizationServiceDao.getAddressList();
						 ArrayList<String> addCityList = organizationServiceDao.getAddCityList();
						 ArrayList<String> addStateList = organizationServiceDao.getAddStateList();
						 ArrayList<String> addZipList = organizationServiceDao.getAddZipList();
						 ArrayList<String> addEmailList = organizationServiceDao.getAddEmailList();
						 ArrayList<String> addPhoneList = organizationServiceDao.getAddPhoneList();
						 ArrayList<String> ctepList = organizationServiceDao.getCtepList();
						 MoreDetailsDao mdDao = new MoreDetailsDao();
						 MoreDetailsJB mdJB = new MoreDetailsJB();
						 ArrayList idList = new ArrayList();
						 ArrayList modElementDescList = new ArrayList();
						 ArrayList modElementDataList = new ArrayList();
						 ArrayList modElementKeysList = new ArrayList();
						 List<NVPair> listNVPair=new ArrayList<NVPair>();
						 NVPair nvpair = new NVPair();
						 OrganizationDetail organizationDetail = new OrganizationDetail();

							for (int iX=0; iX<siteNameList.size(); iX++) {
								 organizationDetail = new OrganizationDetail();
								 mdJB = new MoreDetailsJB();
								 modElementDescList = new ArrayList();
								 modElementDataList = new ArrayList();
								 modElementKeysList = new ArrayList();
								 idList = new ArrayList();
								organizationDetail.setOrganization_Name(siteNameList.get(iX));
								OrganizationIdentifier orgIdentifier = new OrganizationIdentifier();
								ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(pkSiteList.get(iX)));
								orgIdentifier.setOID(map.getOID());
								orgIdentifier.setPK(StringUtil.stringToInteger(pkSiteList.get(iX)));
								orgIdentifier.setSiteName(siteNameList.get(iX));
								organizationDetail.setOrganizationIdentifier(orgIdentifier);
								organizationDetail.setSite_Id(siteIdList.get(iX));
								organizationDetail.setNCI_PO_ID(nciPOIDList.get(iX));
								organizationDetail.setDescription(siteDescList.get(iX));
								organizationDetail.setNotes(siteNotesList.get(iX));
								if(!StringUtil.isEmpty(siteParentList.get(iX))){
								orgIdentifier = new OrganizationIdentifier();
								map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(siteParentList.get(iX)));
								orgIdentifier.setOID(map.getOID());
								orgIdentifier.setPK(StringUtil.stringToInteger(siteParentList.get(iX)));
								orgIdentifier.setSiteName(siteParentNameList.get(iX));
								organizationDetail.setParent_Organization(orgIdentifier);
								}
								CodeCache codeCache = CodeCache.getInstance();
								if(siteTypePkList.get(iX)!=null && callingUser.getUserAccountId()!=null){
									Code siteTypeFk = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_ORGG, (String)siteTypePkList.get(iX),StringUtil.stringToInteger(callingUser.getUserAccountId()));
									
									if(siteTypeFk!=null){
										organizationDetail.setOrganization_Type(siteTypeFk);
										}
									}
								organizationDetail.setHide_Flag(siteHideList.get(iX));
								organizationDetail.setCTEPID(ctepList.get(iX));
								organizationDetail.setAddress(addressList.get(iX));
								organizationDetail.setCity(addCityList.get(iX));
								organizationDetail.setState(addStateList.get(iX));
								organizationDetail.setZip_Code(addZipList.get(iX));
								organizationDetail.setContact_Email(addEmailList.get(iX));
								organizationDetail.setContact_Phone(addPhoneList.get(iX));
								mdJB = new MoreDetailsJB();
								mdDao = mdJB.getMoreDetails(EJBUtil.stringToNum(pkSiteList.get(iX)),"org",callingUser.getUserGrpDefault());
								idList = mdDao.getId();
								modElementDescList = mdDao.getMdElementDescs();
								modElementDataList = mdDao.getMdElementValues();
								modElementKeysList = mdDao.getMdElementKeys();
								listNVPair=new ArrayList<NVPair>();
								for(int iY=0; iY<idList.size(); iY++){
									if((Integer)idList.get(iY)>0){
									nvpair = new NVPair();
									nvpair.setKey((String) modElementKeysList.get(iY));
									nvpair.setValue((String)modElementDataList.get(iY));
									listNVPair.add(nvpair);
									
									}
								}
								if(listNVPair.size()>0){
									organizationDetail.setMoreOrganizationDetails(listNVPair);
								}
								organizationSearchResults.addOrganization(organizationDetail);
								}
							//user.setUserMoreDetails(listNVPair);
							organizationSearchResults.setPageNumber(organizationServiceDao.getPageNumber());
							organizationSearchResults.setPageSize(organizationServiceDao.getPageSize());
							organizationSearchResults.setTotalCount(organizationServiceDao.getTotalCount());
							
					}else{
						organizationSearchResults.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid OrganizationIdentifier Object is required")); 
						throw new OperationException();	
					}
					
					return organizationSearchResults;
					
				}
				
				catch(OperationException e){
					sessionContext.setRollbackOnly();
					if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created ", e);
					//throw new OperationRolledBackException(response.getIssues());
				}
				catch(Throwable t){
					sessionContext.setRollbackOnly();
					this.addUnknownThrowableIssue(t);
					if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created", t);
					//throw new OperationRolledBackException(response.getIssues());
				}
				return organizationSearchResults;
			}
	
	
	//Method to Kill the User Session
		public ResponseHolder killUserSession(UserIdentifier userId) throws OperationException {
			
			try{
				UserAgentRObj userAgentRObj = EJBUtil.getUserAgentHome();
				UserBean userBean = new UserBean();	
				int result = 0;
				
				if (callingUser.getUserId() == null || callingUser.getUserId() == 0){
					
					addIssue(new Issue(IssueTypes.USER_NOT_FOUND,"Valid calling user required for killing the session of another user"));
					throw new OperationException();
				}
				
				if(userId == null)
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, " Valid UserIdentifier is required ")); 
					throw new OperationException();
				}
				//Bug Fix : 15336
				if((StringUtil.isEmpty(userId.getOID())) && StringUtil.isEmpty((EJBUtil.integerToString(userId.getPK())))
						&& (StringUtil.isEmpty(userId.getUserLoginName()))
						&&(StringUtil.isEmpty(userId.getFirstName()) && StringUtil.isEmpty(userId.getLastName())))
					{
						addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid User Identifier is required with OID or PK or User Login Name or First Name and Last Name is required."));
						throw new OperationException();
					}
				
				try {
					userBean = ObjectLocator.userBeanFromIdentifier(callingUser, userId, userAgent, sessionContext, objectMapService);
				} catch (MultipleObjectsFoundException mce) {
					// TODO Auto-generated catch block
					addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, mce.getMessage()));
					throw new OperationException();
				}
				
				if((userBean==null))
				{
					addIssue(new Issue(IssueTypes.SYSTEM_USER_NOT_FOUND,"User account not found for which the session is to be killed"));
					throw new OperationException("User account not found for which the session is to be killed");
				}
				
				if(userBean.getUserType().compareTo("N") == 0){
					
					addIssue(new Issue(IssueTypes.SYSTEM_USER_NOT_FOUND,"System User account not found for which the session is to be killed"));
					throw new OperationException("System User account not found for which the session is to be killed");
				}
				
				
				GroupAgentRObj groupAgentRObj = EJBUtil.getGroupAgentHome();
				
				if(groupAgentRObj.getGroupDetails(EJBUtil.stringToNum(callingUser.getUserGrpDefault())).getGroupName().compareTo("Admin") != 0){
					
					if(callingUser.getUserId() != userBean.getUserId()){
						addIssue(new Issue(IssueTypes.DOES_NOT_HAVE_RIGHTS_TO_KILL_SESSION,"User does not have the right to kill somebody else's session"));
						throw new OperationException("User does not have the right to kill somebody else's session");
					}
				}
					
				if (userBean.getUserLoginFlag()==null || userBean.getUserLoginFlag().compareTo("0") == 0){
					addIssue(new Issue(IssueTypes.USER_NOT_LOGGED_IN,"User is not currently logged in so no active session to kill"));
					throw new OperationException("User is not currently logged in so no active session to kill");
				}
			    userBean.setUserCurrntSsID("");
			    userBean.setUserLoginFlag("0");
			    result = userAgentRObj.updateUser(userBean);
			    userAgentRObj.flush();
			    if (result ==0){
					userId.setUserLoginName(userBean.getUserLoginName());//Bug Fix : 12253
					userId.setOID(objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_USER, userBean.getUserId()).getOID());//Bug Fix: 12253
					userId.setPK(userBean.getUserId());
			    	response.addAction(new CompletedAction(userId, CRUDAction.UPDATE));
			    }
			    else if (result == -2){
			    	addIssue(new Issue(IssueTypes.USER_SESSION_NOT_KILLED,"User was not logged out of the system successfully"));
					throw new OperationException("User was not logged out of the system successfully");
			    }
			    
				return response;
				}catch(OperationException e){
					sessionContext.setRollbackOnly();
					if (logger.isDebugEnabled()) logger.debug("UserServiceImpl Retrieve", e);
					throw new OperationRolledBackException(response.getIssues());
			
				}
				catch(Throwable t){
					sessionContext.setRollbackOnly();
					addUnknownThrowableIssue(t);
					if (logger.isDebugEnabled()) logger.debug("UserServiceImpl Retrieve", t);
					throw new OperationRolledBackException(response.getIssues());
			
				}
				
			}
     

		//  ------- creating system User -------------
			
		public ResponseHolder createUser(User user) throws OperationException 
		{
			try
			{
				  //------------------Checking calling user's group rights for manage users------------------------------------------------------------------------------
							
				GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
				int manageUsers = authModule.getAdminManageUsersPrivileges().intValue();
				boolean hasNewPermissions = GroupAuthModule.hasNewPermission((Integer.valueOf(manageUsers)));
				if (!hasNewPermissions)
				{
					addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," CallingUser is not authorized to create user data "));
					throw new AuthorizationException(" User is not authorized to create new user.");
				}
				if(user == null)
				{	
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid User Object is required")); 
					throw new OperationException(); 
				}
				
				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put("sessionContext", sessionContext); 
				parameters.put("userAgent", userAgent);
				parameters.put("userSiteAgent", userSiteAgent);
				parameters.put("objectMapService", objectMapService);
				parameters.put("callingUser", callingUser);
				parameters.put("siteAgent", siteAgent);
				parameters.put("ResponseHolder", response);
				
				UserServiceHelper userServiceHelper = new UserServiceHelper();
				int userPK = userServiceHelper.createUser(user,parameters);
							
				ObjectMap map = this.objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_USER, Integer.valueOf(userPK));
				UserIdentifier userIdentifier = new UserIdentifier();
				userIdentifier.setOID(map.getOID());
				userIdentifier.setPK(userPK);
				this.response.addAction(new CompletedAction(userIdentifier, CRUDAction.CREATE));
				
				
			}
			
			catch(OperationException e){
				e.printStackTrace();
				sessionContext.setRollbackOnly();
				if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created ", e);
				e.setIssues(response.getIssues());
				throw e;
			}
			catch(Throwable t){
				this.addUnknownThrowableIssue(t);
				if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created", t);
				throw new OperationException(t);
			}
			
			
			return response;
		}
		
		public ResponseHolder createOrganization(OrganizationDetail org) throws OperationException 
		{
			try
			{
				  //------------------Checking calling user's group rights for manage organization------------------------------------------------------------------------------
							
				GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
				int manageOrg = authModule.getAdminManageOrganizationsPrivileges().intValue();
				boolean hasNewPermissions = GroupAuthModule.hasNewPermission((Integer.valueOf(manageOrg)));
				if (!hasNewPermissions)
				{
					response.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to create organizaion data "));
					throw new AuthorizationException(" User is not authorized to create new organizaion.");
				}
				if(org == null)
				{	
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid Organizaion Object is required")); 
					throw new OperationException(); 
				}
				
				// Parent Organization Identifier
				if(org.getParent_Organization()!=null){
				Integer siteParentPK = 0;
				try
				{
					 siteParentPK = ObjectLocator.sitePKFromIdentifier(callingUser, org.getParent_Organization(), sessionContext, objectMapService);
				}
				catch(MultipleObjectsFoundException moe)
				{
					response.getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "Parent organization not found for  OID:"
							+ org.getParent_Organization().getOID() + " siteName:" + org.getParent_Organization().getSiteName()));
					
					throw new OperationException();
				}
				if(siteParentPK==null || siteParentPK == 0)
				{
					response.getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "Parent organization  not found for  OID :"
							+ org.getParent_Organization().getOID() + " siteName:" + org.getParent_Organization().getSiteName()));
					
					throw new OperationException();
				}else{
					org.getParent_Organization().setPK(siteParentPK);
				}
				SiteBean sitebn = siteAgent.getSiteDetails(siteParentPK);
				if(sitebn == null)
				{
					response.getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "Parent organization  not found for sitePK :"
							+ org.getParent_Organization().getPK()));
					
					throw new OperationException();
				}
				}
				if(org.getHide_Flag()!=null){
					 if(!"true".equals(org.getHide_Flag())){
						 if(!"false".equals(org.getHide_Flag())){
							 response.getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Organization Hide Flag for User(true/false):"
										+ org.getHide_Flag()));
						 throw new OperationException();
						 }
					 }
								else if(!"false".equals(org.getHide_Flag())){
									if(!"true".equals(org.getHide_Flag())){
										response.getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Organization Hide Flag for User(true/false):"
												+ org.getHide_Flag()));
									throw new OperationException();
									}
								}
				}
				
				Map<String, Object> parameters = new HashMap<String, Object>();
				//parameters.put("sessionContext", sessionContext);
				parameters.put("objectMapService", objectMapService);
				parameters.put("callingUser", callingUser);
				parameters.put("siteAgent", siteAgent);
				parameters.put("ResponseHolder", response);
				
				UserServiceHelper userServiceHelper = new UserServiceHelper();
				int orgPK = userServiceHelper.createOrganization(org,parameters);
							
				//ObjectMap map = this.objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(orgPK));
				//OrganizationIdentifier orgIdentifier = new OrganizationIdentifier();
				//orgIdentifier.setOID(map.getOID());
				//orgIdentifier.setPK(orgPK);
				//this.response.addAction(new CompletedAction(orgIdentifier, CRUDAction.CREATE));
				
				
			}
			
			catch(OperationException e){
				sessionContext.setRollbackOnly();
				if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created ", e);
			}
			catch(Throwable t){
				sessionContext.setRollbackOnly();
				this.addUnknownThrowableIssue(t);
				if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created", t);
			}
			return response;
		}
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		this.response = new ResponseHolder();
		this.callingUser = getLoggedInUser(this.sessionContext, this.userAgent);
		return ctx.proceed();
	}


	public ResponseHolder checkESignature(String eSignature)
			throws OperationException {
	
		try{
			
			if(callingUser.getUserESign().equals(eSignature))
			{
				CompletedAction completed = new CompletedAction(); 
				completed.setObjectName("ESignature Validated"); 
				response.addAction(completed); 			
				
			}else
			{
				response.addIssue(new Issue(IssueTypes.INVALID_ESIGNATURE, "Invalid ESignature")); 
				throw new OperationException(); 
			}	
			
			
		}catch(OperationException e){
			e.printStackTrace();
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created ", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created", t);
			throw new OperationException(t);
		}
		
		return response; 
	}

	public UserSearchResults searchUser(UserSearch userSearch)
			throws OperationException {
		UserServiceDAO userServiceDAO = new UserServiceDAO();
		HashMap<String, String> params = new HashMap<String, String>();
		params.put(UserServiceDAO.KEY_ACCOUNT_ID, callingUser.getUserAccountId());
		//Kavitha: #28969
		params.put(UserServiceDAO.KEY_PAGE_SIZE, userSearch.getPageSize()!=0 ? String.valueOf(userSearch.getPageSize()):"");
		if (userSearch.getSortBy() == null && userSearch.getSortOrder() == null) {
			userSearch.setSortBy(UserSearchOrderBy.PK);
			userSearch.setSortOrder(SortOrder.DESCENDING);
		} else if (userSearch.getSortOrder() == null) {
			userSearch.setSortOrder(SortOrder.ASCENDING);
		}

		if (userSearch.getUserPK() > 0) {
			params.put(UserServiceDAO.KEY_USER_PK, 
					String.valueOf(userSearch.getUserPK()));
		}
		if (userSearch.getOrganization() != null) {
			if (userSearch.getOrganization().getOID() != null) {
				userSearch.getOrganization().setPK(
						objectMapService.getObjectPkFromOID(
								userSearch.getOrganization().getOID()));
			}
		}
		if (userSearch.getJobType() != null) {
			CodeDao codeDao = codeAgent.getCodeDaoInstance();
			codeDao.getCodeValues(userSearch.getJobType().getType());
			if (StringUtil.isEmpty(userSearch.getJobType().getCode())) {
				ArrayList jobDescripList = codeDao.getCDesc();
				for (int iX = 0; iX < jobDescripList.size(); iX++) {
					if (StringUtil.trueValue(
							userSearch.getJobType().getDescription()).equals(
									jobDescripList.get(iX))) {
						params.put(UserServiceDAO.KEY_JOB_CODE_PK, 
								String.valueOf(codeDao.getCId().get(iX)));
						break;
					}
				}
			} else {
				ArrayList jobSubTypeList = codeDao.getCSubType();
				for (int iX = 0; iX < jobSubTypeList.size(); iX++) {
					if (StringUtil.trueValue(
							userSearch.getJobType().getCode()).equals(
									StringUtil.trueValue(
											(String)jobSubTypeList.get(iX)).trim())) {
						params.put(UserServiceDAO.KEY_JOB_CODE_PK, 
								String.valueOf(codeDao.getCId().get(iX)));
						break;
					}
				}
			}
		}
		if (userSearch.getGroup() != null) {
			if (!StringUtil.isEmpty(userSearch.getGroup().getOID())) {
				params.put(UserServiceDAO.KEY_USER_GROUP, 
						String.valueOf(objectMapService.getObjectPkFromOID(userSearch.getGroup().getOID())));
			} else if (userSearch.getGroup().getPK() != null && userSearch.getGroup().getPK() > 0) {
				params.put(UserServiceDAO.KEY_USER_GROUP, String.valueOf(userSearch.getGroup().getPK()));
			}
		}
		if (userSearch.getStudyTeam() != null) {
			if (!StringUtil.isEmpty(userSearch.getStudyTeam().getOID())) {
				params.put(UserServiceDAO.KEY_STUDY_TEAM,
						String.valueOf(objectMapService.getObjectPkFromOID(userSearch.getStudyTeam().getOID())));
			} else if (userSearch.getStudyTeam().getPK() != null && userSearch.getStudyTeam().getPK() > 0) {
				params.put(UserServiceDAO.KEY_STUDY_TEAM,
						String.valueOf(userSearch.getStudyTeam().getPK()));
			}
		}
		Map<String,String> moreDetailsMap=new HashMap<String,String>();
		moreDetailsMap.putAll(userServiceDAO.searchUserWS(userSearch, params));
		UserSearchResults userSearchResults = new UserSearchResults();
		ArrayList<String> pkUserList = userServiceDAO.getPkUserList();
		ArrayList<String> lastNameList = userServiceDAO.getLastNameList();
		ArrayList<String> firstNameList = userServiceDAO.getFirstNameList();
		ArrayList<String> orgNameList = userServiceDAO.getOrgNameList();
		ArrayList<String> loginNameList = userServiceDAO.getLoginNameList();
		ArrayList<String> userStatList = userServiceDAO.getUsrStatList();
		ArrayList<String> addressList = userServiceDAO.getAddressList();
		ArrayList<String> addCityList = userServiceDAO.getAddCityList();
		ArrayList<String> addStateList = userServiceDAO.getAddStateList();
		ArrayList<String> addZipList = userServiceDAO.getAddZipList();
		ArrayList<String> addEmailList = userServiceDAO.getAddEmailList();
		ArrayList<String> addPhoneList = userServiceDAO.getAddPhoneList();
		ArrayList<String> fkDefaultGroupList = userServiceDAO.getFkDefaultGroupList();
		ArrayList<String> defaultGroupList = userServiceDAO.getDefaultGroupList();
		/*ArrayList<String> key = userServiceDAO.getKey();
		ArrayList<String> value = userServiceDAO.getValue();
		ArrayList<String> type = userServiceDAO.getType();
		ArrayList<String> description = userServiceDAO.getDescription();*/
		
		
		for (int iX=0; iX<lastNameList.size(); iX++) {
			User user = new User();
			List<NVPair> listNVPair=new ArrayList<NVPair>();
			user.setPK(StringUtil.stringToInteger(pkUserList.get(iX)));
			user.setUserLoginName(loginNameList.get(iX));
			System.out.println("login name==="+loginNameList.get(iX));
			user.setLastName(lastNameList.get(iX));
			user.setFirstName(firstNameList.get(iX));
			setUserStatusByString(user, userStatList.get(iX));
			OrganizationIdentifier orgId= new OrganizationIdentifier();
			orgId.setSiteName(orgNameList.get(iX));
			user.setAddress(addressList.get(iX));
			user.setCity(addCityList.get(iX));
			user.setState(addStateList.get(iX));
			user.setZip(addZipList.get(iX));
			user.setEmail(addEmailList.get(iX));
			user.setPhone(addPhoneList.get(iX));
			user.setOrganization(orgId);

			GroupIdentifier usrDefaultGrp = new GroupIdentifier();
			if(fkDefaultGroupList.get(iX)!=null){
			Integer fkGrp = StringUtil.stringToInteger(fkDefaultGroupList.get(iX));
			usrDefaultGrp.setPK(fkGrp);
			usrDefaultGrp.setOID((objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_GROUPS, fkGrp)).getOID());
			usrDefaultGrp.setGroupName(defaultGroupList.get(iX).toString());
			user.setUserDefaultGroup(usrDefaultGrp);
			}
			String keySearch="";
			String keyValue="";
			//Kavitha: #28969
			listNVPair.addAll(getUserMoreDetailValue(pkUserList.get(iX),keySearch,keyValue));
			if(listNVPair.size()>0){
				user.setUserMoreDetails(listNVPair);
			}
			userSearchResults.addUser(user);
		}
		
		userSearchResults.setPageNumber(userServiceDAO.getPageNumber());
		userSearchResults.setPageSize(userServiceDAO.getPageSize());
		userSearchResults.setTotalCount(userServiceDAO.getTotalCount());
		return userSearchResults;
	}
	
	
	public NetworkSiteSearchResults getNetworkSiteDetails(NetworkSiteDetails networkSiteDetails)
			throws OperationException {
			
		NetworkSiteSearchResults networkSiteSearchResults = new NetworkSiteSearchResults();
		NetworkServiceDao networkServiceDao = new NetworkServiceDao();
		try
			{
				  //------------------Checking calling user's group rights for manage users------------------------------------------------------------------------------
			HashMap<String, Object> parameters = new HashMap<String, Object>();			
				GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
				int manageOrg = authModule.getAdminManageOrganizationsPrivileges().intValue();
				boolean hasNewPermissions = GroupAuthModule.hasNewPermission((Integer.valueOf(manageOrg)));
				if (!hasNewPermissions)
				{
					addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to Update organizaion data "));
					throw new AuthorizationException(" User is not authorized to Update organizaion.");
				}
				if(networkSiteDetails == null)
				{	
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid networkSearch Object is required")); 
					throw new OperationException(); 
				}
				if((networkSiteDetails.getNetwork_Name()==null || networkSiteDetails.getNetwork_Name().equals("")) && 
						   (networkSiteDetails.getSite_Name()==null || networkSiteDetails.getSite_Name().equals("")) && 
						   (networkSiteDetails.getRelationship_Type()==null || networkSiteDetails.getRelationship_Type().getCode().equals("")) &&
						   (networkSiteDetails.getMoreSiteDetails()==null) && (networkSiteDetails.getRelationship_PK()==null || networkSiteDetails.getRelationship_PK().equals("")) &&
						   (networkSiteDetails.getNetwork_PK()==null || networkSiteDetails.getNetwork_PK().equals("")) &&
						   (networkSiteDetails.getSite_PK()==null || networkSiteDetails.getSite_PK().equals("")) &&
						   (networkSiteDetails.getCTEP_ID()==null || networkSiteDetails.getCTEP_ID().equals(""))){
							response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
							response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid Data is required")); 
							throw new OperationException();
				}
				
				if(((networkSiteDetails.getNetwork_Name()==null || networkSiteDetails.getNetwork_Name().equals("")) && 
						(networkSiteDetails.getNetwork_PK()==null || networkSiteDetails.getNetwork_PK().equals("")) && 
						(networkSiteDetails.getRelationship_PK()==null || networkSiteDetails.getRelationship_PK().equals(""))) &&
						((networkSiteDetails.getMoreSiteDetails()!=null || (networkSiteDetails.getSite_Name()!=null && !networkSiteDetails.getSite_Name().equals("")) || (networkSiteDetails.getRelationship_Type()!=null && !networkSiteDetails.getRelationship_Type().getCode().equals("")) ||
				        (networkSiteDetails.getSite_PK()!=null && !networkSiteDetails.getSite_PK().equals("")) ||
				        (networkSiteDetails.getCTEP_ID()!=null && !networkSiteDetails.getCTEP_ID().equals(""))))){
					response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network Identifier is required")); 
					throw new OperationException();
				}
				
				if(networkSiteDetails.getRelationship_Type() != null && networkSiteDetails.getRelationship_Type().getCode() != null)
				{
					CodeDao cd = new CodeDao();	
				    String pkCode = StringUtil.integerToString(cd.getCodeId(networkSiteDetails.getRelationship_Type().getType(), networkSiteDetails.getRelationship_Type().getCode()));
					if(pkCode.equals("0")){
						response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Relationship Type Code Not Found")); 
						throw new OperationException();
					}
				}
				
				parameters.put("objectMapService", objectMapService);
				parameters.put("callingUser", callingUser);
				parameters.put("ResponseHolder", response);
				parameters.put(NetworkServiceDao.KEY_ACCOUNT_ID, callingUser.getUserAccountId());

				 networkServiceDao.searchNetworkSites(networkSiteDetails, parameters);
				 ArrayList<String> pkNetList = (ArrayList<String>) networkServiceDao.getPkNetList();
				 ArrayList<String> fkSiteList = (ArrayList<String>) networkServiceDao.getFkSiteList();
				 ArrayList<String> pkRelnshipType=(ArrayList<String>) networkServiceDao.getPkRelnShipType();
				 ArrayList<String> netNameList = (ArrayList<String>) networkServiceDao.getNetNameList();
				 //ArrayList<String> netStatList = (ArrayList<String>) networkServiceDao.getNetStatList();
				 ArrayList<Code> netStatList = (ArrayList<Code>) networkServiceDao.getNetStatList();
				 ArrayList<Code> netRelnShipType=(ArrayList<Code>) networkServiceDao.getSiteRelnShipType();
				 ArrayList<String> siteNameList = (ArrayList<String>) networkServiceDao.getSiteNameList();
				 ArrayList<ArrayList<Site>> sitesList = (ArrayList<ArrayList<Site>>) networkServiceDao.getSitesList();
				 
				 Network network = null;
				 Sites sites=null;
					for (int iX=0; iX<netNameList.size(); iX++) {
						network = new Network();
						sites = new Sites();
						network.setName(netNameList.get(iX));
						NetworkIdentifier netIdentifier = new NetworkIdentifier();
						ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_NETWORK, Integer.valueOf(pkNetList.get(iX)));
						netIdentifier.setOID(map.getOID());
						netIdentifier.setPK(Integer.valueOf(pkNetList.get(iX)));
						network.setNetworkIdentifier(netIdentifier);
						network.setNetworkStatus(netStatList.get(iX));
						if(pkRelnshipType.get(iX)!=null && !pkRelnshipType.get(iX).equals("0")){
							network.setRelationshipPK(pkRelnshipType.get(iX));
						}
						network.setNetworkRelationShipType(netRelnShipType.get(iX));
						sites.setSites(sitesList.get(iX));
						network.setSites(sites);
						networkSiteSearchResults.addNetwork(network);
						}
			}
			
			catch(OperationException e){
				if(networkServiceDao.getResponse()!=null){
					networkSiteSearchResults.setResponse(networkServiceDao.getResponse());
				}
				else{
				networkSiteSearchResults.setResponse(response);
				}
				sessionContext.setRollbackOnly();
				if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created ", e);
				e.setIssues(response.getIssues());
				//throw e;
				//throw new OperationRolledBackException(response.getIssues());
			}
			catch(Throwable t){
				sessionContext.setRollbackOnly();
				this.addUnknownThrowableIssue(t);
				if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created", t);
				//throw new OperationRolledBackException(response.getIssues());
			}
		return networkSiteSearchResults;
	}
	
	
	private static final String LETTER_A = "A";
	private static final String LETTER_B = "B";
	private static final String LETTER_D = "D";
	
	private void setUserStatusByString(User user, String statusString) {
		if (LETTER_A.equals(statusString)) {
			user.setUserStatus(UserStatus.ACTIVE);
		} else if (LETTER_B.equals(statusString)) {
			user.setUserStatus(UserStatus.BLOCKED);
		} else if (LETTER_D.equals(statusString)) {
			user.setUserStatus(UserStatus.DEACTIVATED);
		}
	}

	@TransactionAttribute(REQUIRED)
	public NVPair getUserMoreDetailValue(String Userpk, String moreUserDetails) throws OperationException{
		System.out.println("-----------Reached in searchStudy Service----------------");
		NVPair moreUserDetailValue= new NVPair();
		//System.out.println("key value="+moreUserDetails.getKey());
		
	//Integer studyPK = locateStudyPK(Userpk);
	
		if(Userpk == null || Userpk == ""){
			addIssue(new Issue(IssueTypes.USER_NOT_FOUND, "User not found" ));
			throw new OperationException();
		}
		
	
		int pkuser=StringUtil.stringToInteger(Userpk);
		
		moreUserDetailValue=	MoreStudyDetailsDAO.getUserMoreDetailValue(
				pkuser,
				EJBUtil.stringToInteger(callingUser.getUserGrpDefault()),moreUserDetails);
		return moreUserDetailValue;
		
	}
	
	@TransactionAttribute(REQUIRED)
	public List<NVPair> getUserMoreDetailValue(String Userpk, String moreUserDetails,String value) throws OperationException{
		System.out.println("-----------Reached in searchStudy Service----------------");
		List<NVPair> moreUserDetailValue= new ArrayList<NVPair>();
		//System.out.println("key value="+moreUserDetails.getKey());
		
	//Integer studyPK = locateStudyPK(Userpk);
	
		if(Userpk == null || Userpk == ""){
			addIssue(new Issue(IssueTypes.USER_NOT_FOUND, "User not found" ));
			throw new OperationException();
		}
		
	
		int pkuser=StringUtil.stringToInteger(Userpk);
		
		moreUserDetailValue.addAll(	MoreStudyDetailsDAO.getUserMoreDetailValue(
				pkuser,
				EJBUtil.stringToInteger(callingUser.getUserGrpDefault()),moreUserDetails,value));
		return moreUserDetailValue;
		
	}
	
	public Networks searchNetworks(NetworksDetails networkD) throws OperationException 
	{
		//ResponseHolder response=new ResponseHolder();
		Networks returnNetwrk=new Networks();
		boolean isUserAUthorised=false;
        GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
        int manageUsers=authModule.getAdminManageUsersPrivileges().intValue();
        NetworkServiceDao ndao=new NetworkServiceDao();
        
        
		try{
			if((GroupAuthModule.hasNewPermission(manageUsers) || (GroupAuthModule.hasEditPermission(manageUsers)) ))
	        {
	        	
	        	isUserAUthorised=true;
	        }
	        else 
	        {
	        	
	        	int manageOrganisation= authModule.getAdminManageOrganizationsPrivileges().intValue();
	        	if((GroupAuthModule.hasViewPermission(manageOrganisation)) || (GroupAuthModule.hasEditPermission(manageOrganisation)))
	        			{
		        			
		        			isUserAUthorised=true;
	        			}
	        
	        	
	        }
	        if(!isUserAUthorised)
	        {
	        	//throw exception
	        	addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION,"User is not authorized to view organization"));
				throw new AuthorizationException("User is not authorized to view organization");
	        	
	        }
	        	
	        	if(networkD==null){
		        	addIssue(new Issue(IssueTypes.NETWORK_ISSUE, "No Network Found"));
					throw new OperationException("Please enter params");
		        }
	        if((networkD.getNetworkPK()==null || networkD.getNetworkPK().equals("")) &&
	        		(networkD.getNetworkName()==null || networkD.getNetworkName().equals("")) &&
	        		(networkD.getSitePK()==null || networkD.getSitePK().equals("")) &&
	        		(networkD.getSiteName()==null || networkD.getSiteName().equals(""))){
	        	addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Either network or Site Identifier is required in the input"));
				throw new OperationException("Please enter params");
	        }
			
	        int userPK=  this.callingUser.getUserId();
	      	System.out.println("user pk   sdasf "+userPK);
			ObjectMap map = this.objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_NETWORK, Integer.valueOf(userPK));
			NetworkSiteDetailsResult ntwdetresult = null;
			List<NVPair> listNVPair=new ArrayList<NVPair>();
			
			HashMap<String, Object> params = new HashMap<String, Object>();	 
			params.put("objectMapService", objectMapService);
			params.put("callingUser", callingUser);
			params.put("siteAgent", siteAgent);
			params.put("ResponseHolder", response);
			
			
			params.put(UserServiceDAO.KEY_ACCOUNT_ID, callingUser.getUserAccountId());
			ndao.searchNetworks(networkD, params, "searchNetworks");
			ArrayList<String> ntwname= (ArrayList<String>) ndao.getNetNameList();
			ArrayList<Code> netRelnShipType=(ArrayList<Code>) ndao.getSiteRelnShipType();
			ArrayList<String> ntwPK= (ArrayList<String>) ndao.getPkNetList();
			ArrayList<String> netInfoList = (ArrayList<String>) ndao.getNetInfoList();
			ArrayList<Code> netStatList = (ArrayList<Code>) ndao.getNetStatList();
			ArrayList<ArrayList<Site>> sitesList = (ArrayList<ArrayList<Site>>) ndao.getSitesList();
			HashMap <String,Object> searchKey=new HashMap<String,Object>();
			
			
			
			
				
				
	            List<Integer> newList = new ArrayList<Integer>(ntwPK.size()) ;
	            for (String myInt : ntwPK) 
	            { 
	              newList.add(Integer.valueOf(myInt)); 
	            }
			
			Code code=null;
			Code code1=null;
			Sites sites=null;
			for(int i=0;i<ntwname.size();i++)
			{
					code =new Code();
					code1 =new Code();
					sites = new Sites();
				    ntwdetresult=new NetworkSiteDetailsResult();
					NetworkIdentifier netwkid=new NetworkIdentifier();
					netwkid.setOID(map.getOID());
					netwkid.setPK(newList.get(i));
					ntwdetresult.setName(ntwname.get(i));
					ntwdetresult.setDescription(netInfoList.get(i));
					//ntwdetresult.setNetworkStatus(ntwStatus.get(i));
					ntwdetresult.setNetworkStatus(netStatList.get(i));
					//ntwdetresult.setRelationShip_Type(ntwRelation.get(i));
					ntwdetresult.setNetworkRelationShipType(netRelnShipType.get(i));
					ntwdetresult.setNetworkIdentifier(netwkid);			
					
					
					int[] netwrkspks = new int[newList.size()];
					  for (int iY = 0; iY < netwrkspks.length; iY++) {
						  netwrkspks[iY] = newList.get(iY);
					      listNVPair	=MoreStudyDetailsDAO.getNetworkMoreDetailValue(netwrkspks[iY],searchKey,"0",callingUser.getUserGrpDefault()  )	;				
					      
					 }
					  sites.setSites(sitesList.get(i));
					  ntwdetresult.setSites(sites);
					  //fix for 28951 bug
					  if(listNVPair != null && listNVPair.size()>0){
						  ntwdetresult.setMoreNetworkDetails(listNVPair);
					  }
					  returnNetwrk.addNetwork(ntwdetresult);
			}
			
		}
		catch(OperationException e){
			System.out.println("Inside catch");
			if(ndao.getResponse()!=null){
				returnNetwrk.setResponse(ndao.getResponse());
			}
			else{
				returnNetwrk.setResponse(response);
			}
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created ", e);
			e.setIssues(response.getIssues());
		
		}
		catch(Throwable t){
			System.out.println("Inside throw");
			sessionContext.setRollbackOnly();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created", t);
			
		}
		
		return returnNetwrk ;
	
	}
	
	
	@TransactionAttribute(REQUIRED)
	public List<NVPair> getNetworkMoreDetailValue(String Networkpk, HashMap <String,Object>  moreNetworkDetails,int level,String grpId) throws OperationException{
		
		System.out.println("-----------Reached in Network Service----------------");
		List<NVPair> moreUserDetailValue=new ArrayList<NVPair>();
		//System.out.println("key value="+moreUserDetails.getKey());
		
	//Integer studyPK = locateStudyPK(Userpk);
	
		if(Networkpk == null || Networkpk == ""){
			addIssue(new Issue(IssueTypes.USER_NOT_FOUND, "User not found" ));
			throw new OperationException();
		}
		
	
		int pkNetwork=StringUtil.stringToInteger(Networkpk);
	
		moreUserDetailValue=	MoreStudyDetailsDAO.getNetworkMoreDetailValue(
				pkNetwork,moreNetworkDetails,EJBUtil.integerToString(level),grpId);
		return moreUserDetailValue;
		
	}
public NetworkUsersSearchResult networkuser(NetworkUserDetail networkuser)
			throws OperationException {
		NetworkUsersSearchResult networkUsersSearchResult = new NetworkUsersSearchResult();
		NetworkServiceDao networkServiceDao = new NetworkServiceDao();
		NetworkSiteDetails NetworkSiteDetails = new NetworkSiteDetails();
		try
			{
				  //------------------Checking calling user's Organization rights for network users------------------------------------------------------------------------------
			
		    GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
			int manageOrg = authModule.getAdminManageUsersPrivileges().intValue();
			boolean hasNewPermissions = GroupAuthModule.hasNewPermission((Integer.valueOf(manageOrg)));
			if (!hasNewPermissions)
			{
				response.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to view organizaion data "));
				throw new AuthorizationException(" User is not authorized to view  organizaion data.");
			}

			if((networkuser.getOID()==null || (StringUtil.isEmpty(networkuser.getOID())))&& (StringUtil.isEmpty(networkuser.getUserLoginName())) && (networkuser.getPK() == null || networkuser.getPK() == 0) && networkuser.getmoreUserDetails()==null)
			{
		    	addIssue(new Issue(IssueTypes.USER_NOT_FOUND, " Valid UserIdentifier with OID or userLoginName or userPk  or userMoreDetails  is required. "));
				throw new OperationException(); 
		    }
			
				Map<String, Object> parameters = new HashMap<String, Object>();
			    parameters.put("sessionContext", sessionContext);
				parameters.put("objectMapService", objectMapService);
				parameters.put("callingUser", callingUser);
				parameters.put("ResponseHolder", response);
				parameters.put("userAgent", userAgent);
				
				
				
				parameters.put(NetworkServiceDao.KEY_ACCOUNT_ID, callingUser.getUserAccountId());


				if ( networkuser.getPK()!=null && networkuser.getPK() > 0) {
					parameters.put(OrganizationServiceDao.KEY_SITE_PK,String.valueOf(networkuser.getPK()));
				}
				
				
				 networkServiceDao.searchNetworkUsers(networkuser, (HashMap<String, Object>) parameters);
				 ArrayList<String> pkNetList = (ArrayList<String>) networkServiceDao.getPkNetList();
				 ArrayList<String> netNameList = (ArrayList<String>) networkServiceDao.getNetNameList();
				 //ArrayList<String> netStatList = (ArrayList<String>) networkServiceDao.getNetStatList();
				 ArrayList<Code> netStatList = (ArrayList<Code>) networkServiceDao.getNetStatList();
				 ArrayList<ArrayList<NetworkSites>> sitesList = (ArrayList<ArrayList<NetworkSites>>) networkServiceDao.getSiteList();
				 ArrayList<String> pkRelnshipType=(ArrayList<String>) networkServiceDao.getPkRelnShipType();
				 ArrayList<Code> netRelnShipType=(ArrayList<Code>) networkServiceDao.getSiteRelnShipType();
				 
				 UserNetwork network = null;
				 NetworkUserSite site=null;
				 //
				 System.out.println("network Size==="+netNameList.size());
				 if(netNameList.size()==0){
					 response.addIssue(new Issue(IssueTypes.NETWORK_ISSUE, "No Network Found"));
					 response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					 throw new OperationException();
				 }
					for (int iX=0; iX<netNameList.size(); iX++) {
						 network = new UserNetwork();
						 site=new NetworkUserSite();
						 NetworkIdentifier netIdentifier = new NetworkIdentifier();
						 
		
						network.setName(netNameList.get(iX));
						System.out.println("pkNetList.get(iX)==="+pkNetList.get(iX));
						ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_NETWORK, Integer.valueOf(pkNetList.get(iX)));
						netIdentifier.setOID(map.getOID());
						netIdentifier.setPK(StringUtil.stringToInteger(pkNetList.get(iX)));
						network.setNetworkIdentifier(netIdentifier);
						
						/*network.setNetworkStatus(netStatList.get(iX));
						if(pkRelnshipType.get(iX)!=null && !pkRelnshipType.get(iX).equals("0")){
							network.setRelationShip_PK(pkRelnshipType.get(iX));
						}
						network.setNetworkRelationShipType(netRelnShipType.get(iX));
						
						network.setNetworkStatus(netStatList.get(iX));*/
						site.setSites(sitesList.get(iX));
						network.setSites(site);
						
						System.out.println("Inside adding network for ==="+network.getName());
						networkUsersSearchResult.addNetwork(network);
			}
		
					
			}
			
			
		catch(OperationException e){
			System.out.println("Inside catch");
			
			if(networkServiceDao.getResponse()!=null){
				networkUsersSearchResult.setResponse(networkServiceDao.getResponse());
			}
			else{
				networkUsersSearchResult.setResponse(response);
			}
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl NetworkUsersSearchResult ", e);
			e.setIssues(response.getIssues());
			//throw e;
			//throw new OperationRolledBackException(response.getIssues());
		}
			catch(Throwable t){
				sessionContext.setRollbackOnly();
				this.addUnknownThrowableIssue(t);
				if (logger.isDebugEnabled()) logger.debug("UserServiceImpl NetworkUsersSearchResult", t);
			}
		
			return networkUsersSearchResult;
		
					

		}


public NetworkUsersSearchResult getNetworkUserDetails(NetworkUserDetails networkUserDetails)
		throws OperationException {
		
	NetworkUsersSearchResult networkUsersSearchResult = new NetworkUsersSearchResult();
	NetworkServiceDao networkServiceDao = new NetworkServiceDao();
	try
		{
			  //------------------Checking calling user's Organization rights for network users------------------------------------------------------------------------------
		
	    GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
		int manageOrg = authModule.getAdminManageUsersPrivileges().intValue();
		boolean hasNewPermissions = GroupAuthModule.hasNewPermission((Integer.valueOf(manageOrg)));
		if (!hasNewPermissions)
		{
			response.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to view organizaion data "));
			throw new AuthorizationException(" User is not authorized to view  organizaion data.");
		}

		if(networkUserDetails==null)
		{
	    	addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid data is Required."));
			throw new OperationException(); 
	    }
		
			Map<String, Object> parameters = new HashMap<String, Object>();
		    parameters.put("sessionContext", sessionContext);
			parameters.put("objectMapService", objectMapService);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response);
			parameters.put("userAgent", userAgent);
			
			
			
			parameters.put(NetworkServiceDao.KEY_ACCOUNT_ID, callingUser.getUserAccountId());


			
			
			
			 networkServiceDao.searchNetworkUsersDetails(networkUserDetails, (HashMap<String, Object>) parameters);
			 ArrayList<String> pkNetList = (ArrayList<String>) networkServiceDao.getPkNetList();
			 ArrayList<String> netNameList = (ArrayList<String>) networkServiceDao.getNetNameList();
			 ArrayList<Code> netStatList = (ArrayList<Code>) networkServiceDao.getNetStatList();
			 ArrayList<ArrayList<NetworkSitesUsers>> sitesList = (ArrayList<ArrayList<NetworkSitesUsers>>) networkServiceDao.getSiteUsersList();
			 
			 UserNetworkSite network = null;
			 NetworkUsersSite site=null;
			 for (int iX=0; iX<netNameList.size(); iX++) {
					
					 network = new UserNetworkSite();
					 site=new NetworkUsersSite();
					 NetworkIdentifier netIdentifier = new NetworkIdentifier();
					 SiteIdentifier siteIdentifier=new SiteIdentifier();
	
					
					ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_NETWORK, Integer.valueOf(pkNetList.get(iX)));
					netIdentifier.setOID(map.getOID());
					netIdentifier.setPK(StringUtil.stringToInteger(pkNetList.get(iX)));
					netIdentifier.setName(netNameList.get(iX));
					siteIdentifier.setOID(map.getOID());
					siteIdentifier.setPK(StringUtil.stringToInteger(pkNetList.get(iX)));
					network.setNetworkIdentifier(netIdentifier);
					network.setNetworkStatus(netStatList.get(iX));
					site.setSites(sitesList.get(iX));
					network.setSites(site);
					
					
					networkUsersSearchResult.addNetworks(network);
		}
	
				
		}
		
		
		catch(OperationException e){
		if(networkServiceDao.getResponse()!=null){
			networkUsersSearchResult.setResponse(networkServiceDao.getResponse());
		}
		else{
			networkUsersSearchResult.setResponse(response);
		}
		sessionContext.setRollbackOnly();
		if (logger.isDebugEnabled()) logger.debug("UserServiceImpl NetworkUsersSearchResult ", e);
		e.setIssues(response.getIssues());
		//throw e;
		//throw new OperationRolledBackException(response.getIssues());
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl NetworkUsersSearchResult", t);
		}
	
		return networkUsersSearchResult;
	
				

	}

@Override
public NetworkSiteSearchResults getNetworkSiteChildren(SiteLevelDetails siteLevelDetails) throws OperationException {
	NetworkSiteSearchResults networkSiteSearchResults = new NetworkSiteSearchResults();
	NetworkServiceDao networkServiceDao = new NetworkServiceDao();
	try
	{
		  //------------------Checking calling user's Organization rights for network users------------------------------------------------------------------------------
	
    GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
	int manageOrg = authModule.getAdminManageUsersPrivileges().intValue();
	boolean hasViewPermissions = GroupAuthModule.hasViewPermission((Integer.valueOf(manageOrg)));
	if (!hasViewPermissions)
	{
		response.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to view organizaion data "));
		throw new AuthorizationException(" User is not authorized to view  organizaion data.");
	}
	if(siteLevelDetails==null)
	{
    	addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid data is Required."));
		throw new OperationException(); 
    }
	if(siteLevelDetails.getRelationshipPK()==null || siteLevelDetails.getRelationshipPK().equals("")){
		if(((siteLevelDetails.getNetworkName()==null || siteLevelDetails.getNetworkName().equals("")) &&
				(siteLevelDetails.getNetworkPK()==null || siteLevelDetails.getNetworkPK().equals(""))) ||
				((siteLevelDetails.getSitePK()==null || siteLevelDetails.getSitePK().equals("")) && 
				(siteLevelDetails.getSiteName()==null || siteLevelDetails.getSiteName().equals(""))) ||
				(siteLevelDetails.getLevel()==null || siteLevelDetails.getLevel().equals(""))){
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Either relationshipPK or combination of Network,Site Identifier with Level is required in the input"));
					throw new OperationException();
				}
	}
	Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("sessionContext", sessionContext);
	parameters.put("objectMapService", objectMapService);
	parameters.put("callingUser", callingUser);
	parameters.put("ResponseHolder", response);
	parameters.put("userAgent", userAgent);
	
	
	
	parameters.put(NetworkServiceDao.KEY_ACCOUNT_ID, callingUser.getUserAccountId());
	networkServiceDao.getNetworkSiteChildren(siteLevelDetails,parameters);
	ArrayList<String> pkNetList = (ArrayList<String>) networkServiceDao.getPkNetList();
	 ArrayList<String> fkSiteList = (ArrayList<String>) networkServiceDao.getFkSiteList();
	 ArrayList<String> pkRelnshipType=(ArrayList<String>) networkServiceDao.getPkRelnShipType();
	 ArrayList<String> netNameList = (ArrayList<String>) networkServiceDao.getNetNameList();
	 ArrayList<String> netInfoList = (ArrayList<String>) networkServiceDao.getNetInfoList();
	 //ArrayList<String> netStatList = (ArrayList<String>) networkServiceDao.getNetStatList();
	 ArrayList<Code> netStatList = (ArrayList<Code>) networkServiceDao.getNetStatList();
	 ArrayList<Code> netRelnShipType=(ArrayList<Code>) networkServiceDao.getSiteRelnShipType();
	 ArrayList<String> siteNameList = (ArrayList<String>) networkServiceDao.getSiteNameList();
	 ArrayList<ArrayList<Site>> sitesList = (ArrayList<ArrayList<Site>>) networkServiceDao.getSitesList();
	 
	 Network network = null;
	 Sites sites=null;
		for (int iX=0; iX<netNameList.size(); iX++) {
			network = new Network();
			sites = new Sites();
			network.setName(netNameList.get(iX));
			network.setDescription(netInfoList.get(iX));
			System.out.println("Network Description==="+netInfoList.get(iX));
			NetworkIdentifier netIdentifier = new NetworkIdentifier();
			ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_NETWORK, Integer.valueOf(pkNetList.get(iX)));
			netIdentifier.setOID(map.getOID());
			netIdentifier.setPK(Integer.valueOf(pkNetList.get(iX)));
			network.setNetworkIdentifier(netIdentifier);
			network.setNetworkStatus(netStatList.get(iX));
			if(pkRelnshipType.get(iX)!=null && !pkRelnshipType.get(iX).equals("0")){
				network.setRelationshipPK(pkRelnshipType.get(iX));
			}
			network.setNetworkRelationShipType(netRelnShipType.get(iX));
			sites.setSites(sitesList.get(iX));
			network.setSites(sites);
			networkSiteSearchResults.addNetwork(network);
			}
	}catch(OperationException e){
		if(networkServiceDao.getResponse()!=null){
			networkSiteSearchResults.setResponse(networkServiceDao.getResponse());
		}
		else{
			networkSiteSearchResults.setResponse(response);
		}
		sessionContext.setRollbackOnly();
		if (logger.isDebugEnabled()) logger.debug("UserServiceImpl getNetworkSiteChildren ", e);
		e.setIssues(response.getIssues());
		//throw e;
		//throw new OperationRolledBackException(response.getIssues());
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl getNetworkSiteChildren", t);
		}
	return networkSiteSearchResults;
}
		
@Override
public NetworkSiteSearchResults getNetworkSiteParent(SiteLevelDetails siteLevelDetails) throws OperationException {
	NetworkSiteSearchResults networkSiteSearchResults = new NetworkSiteSearchResults();
	NetworkServiceDao networkServiceDao = new NetworkServiceDao();
	try
	{
		  //------------------Checking calling user's Organization rights for network users------------------------------------------------------------------------------
	
    GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
	int manageOrg = authModule.getAdminManageUsersPrivileges().intValue();
	boolean hasViewPermissions = GroupAuthModule.hasViewPermission((Integer.valueOf(manageOrg)));
	if (!hasViewPermissions)
	{
		response.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to view organizaion data "));
		throw new AuthorizationException(" User is not authorized to view  organizaion data.");
	}
	if(siteLevelDetails==null)
	{
    	addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid data is Required."));
		throw new OperationException(); 
    }
	if(siteLevelDetails.getRelationshipPK()==null || siteLevelDetails.getRelationshipPK().equals("")){
		if(((siteLevelDetails.getNetworkName()==null || siteLevelDetails.getNetworkName().equals("")) &&
				(siteLevelDetails.getNetworkPK()==null || siteLevelDetails.getNetworkPK().equals(""))) ||
				((siteLevelDetails.getSitePK()==null || siteLevelDetails.getSitePK().equals("")) && 
				(siteLevelDetails.getSiteName()==null || siteLevelDetails.getSiteName().equals(""))) ||
				(siteLevelDetails.getLevel()==null || siteLevelDetails.getLevel().equals(""))){
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Either relationshipPK or combination of Network,Site Identifier with Level is required in the input"));
					throw new OperationException();
				}
	}
	Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("sessionContext", sessionContext);
	parameters.put("objectMapService", objectMapService);
	parameters.put("callingUser", callingUser);
	parameters.put("ResponseHolder", response);
	parameters.put("userAgent", userAgent);
	
	
	
	parameters.put(NetworkServiceDao.KEY_ACCOUNT_ID, callingUser.getUserAccountId());
	networkServiceDao.getNetworkSiteParent(siteLevelDetails,parameters);
	ArrayList<String> pkNetList = (ArrayList<String>) networkServiceDao.getPkNetList();
	 ArrayList<String> fkSiteList = (ArrayList<String>) networkServiceDao.getFkSiteList();
	 ArrayList<String> pkRelnshipType=(ArrayList<String>) networkServiceDao.getPkRelnShipType();
	 ArrayList<String> netNameList = (ArrayList<String>) networkServiceDao.getNetNameList();
	 ArrayList<String> netInfoList = (ArrayList<String>) networkServiceDao.getNetInfoList();
	 //ArrayList<String> netStatList = (ArrayList<String>) networkServiceDao.getNetStatList();
	 ArrayList<Code> netStatList = (ArrayList<Code>) networkServiceDao.getNetStatList();
	 ArrayList<Code> netRelnShipType=(ArrayList<Code>) networkServiceDao.getSiteRelnShipType();
	 ArrayList<String> siteNameList = (ArrayList<String>) networkServiceDao.getSiteNameList();
	 ArrayList<ArrayList<Site>> sitesList = (ArrayList<ArrayList<Site>>) networkServiceDao.getSitesList();
	 
	 Network network = null;
	 Sites sites=null;
		for (int iX=0; iX<netNameList.size(); iX++) {
			network = new Network();
			sites = new Sites();
			network.setName(netNameList.get(iX));
			network.setDescription(netInfoList.get(iX));
			System.out.println("Network Description==="+netInfoList.get(iX));
			NetworkIdentifier netIdentifier = new NetworkIdentifier();
			ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_NETWORK, Integer.valueOf(pkNetList.get(iX)));
			netIdentifier.setOID(map.getOID());
			netIdentifier.setPK(Integer.valueOf(pkNetList.get(iX)));
			network.setNetworkIdentifier(netIdentifier);
			network.setNetworkStatus(netStatList.get(iX));
			if(pkRelnshipType.get(iX)!=null && !pkRelnshipType.get(iX).equals("0")){
				network.setRelationshipPK(pkRelnshipType.get(iX));
			}
			network.setNetworkRelationShipType(netRelnShipType.get(iX));
			sites.setSites(sitesList.get(iX));
			network.setSites(sites);
			networkSiteSearchResults.addNetwork(network);
			}
	}catch(OperationException e){
		if(networkServiceDao.getResponse()!=null){
			networkSiteSearchResults.setResponse(networkServiceDao.getResponse());
		}
		else{
			networkSiteSearchResults.setResponse(response);
		}
		sessionContext.setRollbackOnly();
		if (logger.isDebugEnabled()) logger.debug("UserServiceImpl getNetworkSiteParent ", e);
		e.setIssues(response.getIssues());
		//throw e;
		//throw new OperationRolledBackException(response.getIssues());
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl getNetworkSiteParent", t);
		}
	return networkSiteSearchResults;
}
public NetworkSiteSearchResults getNetworkLevelSites(NetworkLevelSite networklevelsite)
		throws OperationException {
	NetworkSiteSearchResults networkSiteSearchResults = new NetworkSiteSearchResults();
	NetworkServiceDao networkServiceDao = new NetworkServiceDao();
	
	try
		{
			  //------------------Checking calling user's Organization rights for network users------------------------------------------------------------------------------
		
	    GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
		int manageOrg = authModule.getAdminManageUsersPrivileges().intValue();
		boolean hasNewPermissions = GroupAuthModule.hasNewPermission((Integer.valueOf(manageOrg)));
		if (!hasNewPermissions)
		{
			response.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to view organizaion data "));
			throw new AuthorizationException(" User is not authorized to view  organizaion data.");
		}
		

		if(networklevelsite==null)
		{
	    	addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid data is Required."));
			throw new OperationException(); 
	    }
			String networkPk=networklevelsite.getNetworkPK()==null?"":networklevelsite.getNetworkPK();
			String networkName=networklevelsite.getNetworkName()==null?"":networklevelsite.getNetworkName();
			String networkLevel=networklevelsite.getLevel()==null?"":networklevelsite.getLevel();
		
		
	
		if((networkLevel.equals("") && networkName.equals("") &&  networkPk.equals("")) || (!networkLevel.equals("") &&  networkName.equals("") &&  networkPk.equals("") ) || (networkLevel.equals("") &&  !networkName.equals("") &&  !networkPk.equals("")) || (networkLevel.equals("") &&  !networkName.equals("") || networkLevel.equals("") && !networkPk.equals(""))  ){
			this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Either NetworkName or NetworkPK with Network level is Required.")); 
			throw new OperationException();
		
		
			}
		
			Map<String, Object> parameters = new HashMap<String, Object>();
		    parameters.put("sessionContext", sessionContext);
			parameters.put("objectMapService", objectMapService);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response);
			parameters.put("userAgent", userAgent);
			parameters.put("networkName", networkName);
			parameters.put("networkPk", networkPk);
			parameters.put("networkLevel", networkLevel);
			
			
			
			parameters.put(NetworkServiceDao.KEY_ACCOUNT_ID, callingUser.getUserAccountId());
			 networkServiceDao.getNetworkLevelSites(networklevelsite, (HashMap<String, Object>) parameters);
			 ArrayList<String> pkNetList = (ArrayList<String>) networkServiceDao.getPkNetList();
			 ArrayList<String> netNameList = (ArrayList<String>) networkServiceDao.getNetNameList();
			 ArrayList<Code> netStatList = (ArrayList<Code>) networkServiceDao.getNetStatList();
			 ArrayList<ArrayList<Site>> sitesList = (ArrayList<ArrayList<Site>>) networkServiceDao.getSitesList();
			 ArrayList<String> netInfoList = (ArrayList<String>) networkServiceDao.getNetInfoList();
			 ArrayList<String> pkRelnshipType=(ArrayList<String>) networkServiceDao.getPkRelnShipType();
			 ArrayList<Code> netRelnShipType=(ArrayList<Code>) networkServiceDao.getSiteRelnShipType();
			 Network network = null;
			 Sites site=null;
			 for (int iX=0; iX<netNameList.size(); iX++) {
					
					 network = new Network();
					 site=new Sites();
					 NetworkIdentifier netIdentifier = new NetworkIdentifier();
					 SiteIdentifier siteIdentifier=new SiteIdentifier();
	
					
					ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_NETWORK, Integer.valueOf(pkNetList.get(iX)));
					netIdentifier.setOID(map.getOID());
					netIdentifier.setPK(StringUtil.stringToInteger(pkNetList.get(iX)));
					network.setDescription(netInfoList.get(iX));
					network.setName(netNameList.get(iX));
					siteIdentifier.setOID(map.getOID());
					siteIdentifier.setPK(StringUtil.stringToInteger(pkNetList.get(iX)));
					network.setNetworkIdentifier(netIdentifier);
					network.setNetworkStatus(netStatList.get(iX));
					
					if(pkRelnshipType.get(iX)!=null && !pkRelnshipType.get(iX).equals("0")){
						network.setRelationshipPK(pkRelnshipType.get(iX));
					}
					network.setNetworkRelationShipType(netRelnShipType.get(iX));
					site.setSites(sitesList.get(iX));
					network.setSites(site);			
					networkSiteSearchResults.addNetwork(network);
		}
		}
		
		
	catch(OperationException e){
		System.out.println("Inside catch");
		
		if(networkServiceDao.getResponse()!=null){
			networkSiteSearchResults.setResponse(networkServiceDao.getResponse());
		}
		else{
			networkSiteSearchResults.setResponse(response);
		}
		sessionContext.setRollbackOnly();
		if (logger.isDebugEnabled()) logger.debug("UserServiceImpl getNetworkLevelSites ", e);
		e.setIssues(response.getIssues());
		
	}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl getNetworkLevelSites", t);
		}
	
	
		return networkSiteSearchResults;
	
				

	}

	public NetworkSiteSearchResults getNetworkSiteLevel(NetworkSiteLevelDetails networkSiteLevelDetails)
			throws OperationException {
		
			NetworkSiteSearchResults networkSiteSearchResults = new NetworkSiteSearchResults();
			NetworkServiceDao networkServiceDao = new NetworkServiceDao();
			try
			{
			  //------------------Checking calling user's group rights for manage users------------------------------------------------------------------------------
				HashMap<String, Object> parameters = new HashMap<String, Object>();			
				GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
				int manageOrg = authModule.getAdminManageOrganizationsPrivileges().intValue();
				boolean hasNewPermissions = GroupAuthModule.hasNewPermission((Integer.valueOf(manageOrg)));
				if (!hasNewPermissions)
				{
					addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to Update Network data "));
					throw new AuthorizationException(" User is not authorized to Update Network.");
				}
				if(networkSiteLevelDetails == null)
				{	
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid networkSiteLevel Object is required")); 
					throw new OperationException(); 
				}
			
				if((networkSiteLevelDetails.getNetworkName()==null || networkSiteLevelDetails.getNetworkName().equals("")) && 
				   (networkSiteLevelDetails.getNetworkPK()==null || networkSiteLevelDetails.getNetworkPK().equals(""))){
					response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network PK/Name is required")); 
					throw new OperationException();
				}
			
				if((networkSiteLevelDetails.getSiteName()==null || networkSiteLevelDetails.getSiteName().equals("")) && 
					(networkSiteLevelDetails.getSitePK()==null || networkSiteLevelDetails.getSitePK().equals(""))){
					response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Site PK/Name is required")); 
					throw new OperationException();
				}
			
				parameters.put("objectMapService", objectMapService);
				parameters.put("callingUser", callingUser);
				parameters.put("ResponseHolder", response);
				parameters.put(NetworkServiceDao.KEY_ACCOUNT_ID, callingUser.getUserAccountId());

				networkServiceDao.getNetworkSiteLevel(networkSiteLevelDetails, parameters);
				ArrayList<String> pkNetList = (ArrayList<String>) networkServiceDao.getPkNetList();
				ArrayList<String> fkSiteList = (ArrayList<String>) networkServiceDao.getFkSiteList();
				ArrayList<String> pkRelnshipType=(ArrayList<String>) networkServiceDao.getPkRelnShipType();
				ArrayList<String> netNameList = (ArrayList<String>) networkServiceDao.getNetNameList();
				ArrayList<String> netDescList= (ArrayList<String>) networkServiceDao.getNetInfoList();
				ArrayList<Code> netStatList = (ArrayList<Code>) networkServiceDao.getNetStatList();
				ArrayList<Code> netRelnShipType=(ArrayList<Code>) networkServiceDao.getSiteRelnShipType();
				ArrayList<String> siteNameList = (ArrayList<String>) networkServiceDao.getSiteNameList();
				ArrayList<ArrayList<Site>> sitesList = (ArrayList<ArrayList<Site>>) networkServiceDao.getSitesList();
			 
				Network network = null;
				Sites sites=null;
				for (int iX=0; iX<netNameList.size(); iX++) {
					network = new Network();
					sites = new Sites();
					network.setName(netNameList.get(iX));
					NetworkIdentifier netIdentifier = new NetworkIdentifier();
					ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_NETWORK, Integer.valueOf(pkNetList.get(iX)));
					netIdentifier.setOID(map.getOID());
					netIdentifier.setPK(Integer.valueOf(pkNetList.get(iX)));
					network.setNetworkIdentifier(netIdentifier);
					if(netDescList.get(iX)!=null && !netDescList.get(iX).equals("-")){
						network.setDescription(netDescList.get(iX));
					}
					network.setNetworkStatus(netStatList.get(iX));
					if(pkRelnshipType.get(iX)!=null && !pkRelnshipType.get(iX).equals("0")){
						network.setRelationshipPK(pkRelnshipType.get(iX));
					}
					network.setNetworkRelationShipType(netRelnShipType.get(iX));
					sites.setSites(sitesList.get(iX));
					network.setSites(sites);
					networkSiteSearchResults.addNetwork(network);
					}
				}
		
			catch(OperationException e){
				if(networkServiceDao.getResponse()!=null){
					networkSiteSearchResults.setResponse(networkServiceDao.getResponse());
				}
				else{
					networkSiteSearchResults.setResponse(response);
				}
				sessionContext.setRollbackOnly();
				if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created ", e);
					e.setIssues(response.getIssues());
					//throw e;
					//throw new OperationRolledBackException(response.getIssues());
				}
			catch(Throwable t){
				sessionContext.setRollbackOnly();
				this.addUnknownThrowableIssue(t);
				if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created", t);
				//throw new OperationRolledBackException(response.getIssues());
				}
			return networkSiteSearchResults;
		}
	
	public UserNetworkSiteResults getUserNetworkSites(UserNetworkSites userNetworkSites)
			throws OperationException {
			
		UserNetworkSiteResults networkSiteSearchResults = new UserNetworkSiteResults();
		NetworkServiceDao networkServiceDao = new NetworkServiceDao();
		String networkName="";
		String userName="";
		int networkPk=0;
		int userPk=0;
		try
			{
				  //------------------Checking calling user's group rights for manage users------------------------------------------------------------------------------
			HashMap<String, Object> parameters = new HashMap<String, Object>();			
				GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
				int manageOrg = authModule.getAdminManageOrganizationsPrivileges().intValue();
				boolean hasNewPermissions = GroupAuthModule.hasNewPermission((Integer.valueOf(manageOrg)));
				if (!hasNewPermissions)
				{
					addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to Update organizaion data "));
					throw new AuthorizationException(" User is not authorized to Update organizaion.");
				}
				if(userNetworkSites == null)
				{	
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid networkSearch Object is required")); 
					throw new OperationException(); 
				}
				networkName=userNetworkSites.getNetworkName()==null?"":userNetworkSites.getNetworkName();
				userName=userNetworkSites.getUserLoginName()==null?"":userNetworkSites.getUserLoginName();
				networkPk=userNetworkSites.getNetworkPK()==null?0:userNetworkSites.getNetworkPK();
				userPk=userNetworkSites.getUserPK()==null?0:userNetworkSites.getUserPK();
				System.out.println("userPk = "+userPk);
				
				if(((networkName.equals("")) && (networkPk==0))&& 
						   ((userName.equals("")) && (userPk==0))){
							response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
							response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid Data is required")); 
							throw new OperationException();
				}
				
				if(((networkName.equals("")) && (networkPk==0)) 
						&& ((!(userName.equals(""))) || (userPk!=0))){
					response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network Name/PK is required"));
					throw new OperationException();
				}
				
				
				
				if(((!(networkName.equals(""))) || (networkPk!=0)) 
						&& ((userName.equals("")) && (userPk==0))){
					response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User Name/PK is required"));
					throw new OperationException();
				}

				
				parameters.put("objectMapService", objectMapService);
				parameters.put("callingUser", callingUser);
				parameters.put("ResponseHolder", response);
				parameters.put(NetworkServiceDao.KEY_ACCOUNT_ID, callingUser.getUserAccountId());
				parameters.put("userAgent", userAgent);
				// fetch multiple networks having same name
				networkSiteSearchResults=networkServiceDao.userNetworkSites(userNetworkSites, parameters);
			}
			
			catch(OperationException e){
				if(networkServiceDao.getResponse()!=null){
					networkSiteSearchResults.setResponse(networkServiceDao.getResponse());
				}
				else{
				networkSiteSearchResults.setResponse(response);
				}
				sessionContext.setRollbackOnly();
				if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created ", e);
				e.setIssues(response.getIssues());
				//throw e;
				//throw new OperationRolledBackException(response.getIssues());
			}
			catch(Throwable t){
				sessionContext.setRollbackOnly();
				this.addUnknownThrowableIssue(t);
				if (logger.isDebugEnabled()) logger.debug("UserServiceImpl created", t);
				//throw new OperationRolledBackException(response.getIssues());
			}
		return networkSiteSearchResults;
	}
	public NetworkUsersSearchResult networkusers(UserNetworksDetail networkuser) throws OperationException {
		NetworkUsersSearchResult userNetworksSearchResult = new NetworkUsersSearchResult();
		NetworkServiceDao networkServiceDao = new NetworkServiceDao();
		NetworkSiteDetails NetworkSiteDetails = new NetworkSiteDetails();
		String userLoginName="";
		try {
			// ------------------Checking calling user's Organization rights for
			// network
			// users------------------------------------------------------------------------------

			GroupAuthModule authModule = new GroupAuthModule(this.callingUser, groupRightsAgent);
			int manageOrg = authModule.getAdminManageUsersPrivileges().intValue();
			boolean hasNewPermissions = GroupAuthModule.hasNewPermission((Integer.valueOf(manageOrg)));
			if (!hasNewPermissions) {
				response.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION,
						" Calling User is not authorized to view organizaion data "));
				throw new AuthorizationException(" User is not authorized to view  organizaion data.");
			}
			userLoginName=networkuser.getUserLoginName()==null?"":networkuser.getUserLoginName();
			if (userLoginName.equals("")
					&& (networkuser.getPK() == null || networkuser.getPK() == 0)) {
				addIssue(new Issue(IssueTypes.DATA_VALIDATION,
						"UserPk or UserLoginName is required. "));
				throw new OperationException();
			}

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("sessionContext", sessionContext);
			parameters.put("objectMapService", objectMapService);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response);
			parameters.put("userAgent", userAgent);

			parameters.put(NetworkServiceDao.KEY_ACCOUNT_ID, callingUser.getUserAccountId());

			if (networkuser.getPK() != null && networkuser.getPK() > 0) {
				parameters.put(OrganizationServiceDao.KEY_SITE_PK, String.valueOf(networkuser.getPK()));
			}

			networkServiceDao.searchUserNetworks(networkuser, (HashMap<String, Object>) parameters);
			ArrayList<String> pkNetList = (ArrayList<String>) networkServiceDao.getPkNetList();
			ArrayList<String> netNameList = (ArrayList<String>) networkServiceDao.getNetNameList();
			 //ArrayList<String> netStatList = (ArrayList<String>)networkServiceDao.getNetStatList();
			 ArrayList<Code> netStatList = (ArrayList<Code>)
			 networkServiceDao.getNetStatList();
			//ArrayList<ArrayList<NetworkSites>> sitesList =
			// (ArrayList<ArrayList<NetworkSites>>)
			// networkServiceDao.getSiteList();
			 ArrayList<String> pkRelnshipType=(ArrayList<String>)
			 networkServiceDao.getPkRelnShipType();
			 ArrayList<Code> netRelnShipType=(ArrayList<Code>)
			 networkServiceDao.getSiteRelnShipType();

			UserNetwork network = null;
			//NetworkUserSite site = null;
			//
			System.out.println("network Size===" + netNameList.size());
			if (netNameList.size() == 0) {
				response.addIssue(new Issue(IssueTypes.NETWORK_ISSUE, "No Network Found"));
				response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				throw new OperationException();
			}
			for (int iX = 0; iX < netNameList.size(); iX++) {
				network = new UserNetwork();
				//site = new NetworkUserSite();
				NetworkIdentifier netIdentifier = new NetworkIdentifier();

				network.setName(netNameList.get(iX));
				network.setNetworkStatus(netStatList.get(iX));
				network.setNetworkRelationShipType(netRelnShipType.get(iX));
				System.out.println("pkNetList.get(iX)===" + pkNetList.get(iX));
				 ObjectMap map =
				 objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_NETWORK,
				 Integer.valueOf(pkNetList.get(iX)));
				 netIdentifier.setOID(map.getOID());
				netIdentifier.setPK(StringUtil.stringToInteger(pkNetList.get(iX)));
				network.setNetworkIdentifier(netIdentifier);

				/*
				 * network.setNetworkStatus(netStatList.get(iX));
				 * if(pkRelnshipType.get(iX)!=null &&
				 * !pkRelnshipType.get(iX).equals("0")){
				 * network.setRelationShip_PK(pkRelnshipType.get(iX)); }
				 * network.setNetworkRelationShipType(netRelnShipType.get(iX));
				 * 
				 * network.setNetworkStatus(netStatList.get(iX));
				 */
				// site.setSites(sitesList.get(iX));
				// network.setSites(site);

				System.out.println("Inside adding network for ===" + network.getName());
				 userNetworksSearchResult.addNetwork(network);
			}

		}

		catch (OperationException e) {
			System.out.println("Inside catch");

			if (networkServiceDao.getResponse() != null) {
				userNetworksSearchResult.setResponse(networkServiceDao.getResponse());
			} else {
				userNetworksSearchResult.setResponse(response);
			}
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled())
				logger.debug("UserServiceImpl NetworkUsersSearchResult ", e);
			e.setIssues(response.getIssues());
			// throw e;
			// throw new OperationRolledBackException(response.getIssues());
		} catch (Throwable t) {
			sessionContext.setRollbackOnly();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled())
				logger.debug("UserServiceImpl NetworkUsersSearchResult", t);
		}

		return userNetworksSearchResult;

	}

	@Override
	public UserNetworkSiteResults getNetworkUser(NetworkSiteUserDetails networkSiteUserDetails)
			throws OperationException {
		UserNetworkSiteResults userNetworkSiteResults = new UserNetworkSiteResults();
		NetworkServiceDao networkServiceDao = new NetworkServiceDao();
		try
			{
				  //------------------Checking calling user's Organization rights for network users------------------------------------------------------------------------------
			
		    GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
			int manageOrg = authModule.getAdminManageUsersPrivileges().intValue();
			boolean hasNewPermissions = GroupAuthModule.hasNewPermission((Integer.valueOf(manageOrg)));
			if (!hasNewPermissions)
			{
				response.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to view organizaion data "));
				throw new AuthorizationException(" User is not authorized to view  organizaion data.");
			}

			if(networkSiteUserDetails==null)
			{
		    	addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid data is Required."));
				throw new OperationException(); 
		    }
			
				Map<String, Object> parameters = new HashMap<String, Object>();
			    parameters.put("sessionContext", sessionContext);
				parameters.put("objectMapService", objectMapService);
				parameters.put("callingUser", callingUser);
				parameters.put("ResponseHolder", response);
				parameters.put("userAgent", userAgent);
				
				
				
				parameters.put(NetworkServiceDao.KEY_ACCOUNT_ID, callingUser.getUserAccountId());


				
				
				
				userNetworkSiteResults=networkServiceDao.getNetworkUser(networkSiteUserDetails, (HashMap<String, Object>) parameters);
				 
				if(userNetworkSiteResults.getNetwork().size()==0){
					 response.addIssue(new Issue(IssueTypes.NETWORK_ISSUE, "No Networks Found"));
					 response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					 throw new OperationException();
				 }
		
					
			}
			
			
			catch(OperationException e){
			if(networkServiceDao.getResponse()!=null){
				userNetworkSiteResults.setResponse(networkServiceDao.getResponse());
			}
			else{
				userNetworkSiteResults.setResponse(response);
			}
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl NetworkUsersSearchResult ", e);
			e.setIssues(response.getIssues());
			//throw e;
			//throw new OperationRolledBackException(response.getIssues());
			}
			catch(Throwable t){
				sessionContext.setRollbackOnly();
				this.addUnknownThrowableIssue(t);
				if (logger.isDebugEnabled()) logger.debug("UserServiceImpl NetworkUsersSearchResult", t);
			}
		
			return userNetworkSiteResults;
	}
	
	public void refreshMenuTabs() throws OperationException 
	{
		try
		{
			ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
		    objCache.populateObjectSettings();
		    FilterUtil.fillParamHashFromLkpFilterDao();
		    FilterUtil.fillViewHashFromLkpViewDao();
		    VelosResourceBundle.reloadProperties();
		    LC.reload();
		    MC.reload();
		    ES.reload();
		    SVC.reload();
		    CFG.reload();
		    CodeCache.getInstance().flush(); 
				
		}catch(Exception e){
			e.printStackTrace();
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl Retrieve", t);
			throw new OperationRolledBackException(response.getIssues());
	
		}
				
	}

	@Override
	public ResponseHolder removeNetworkSite(RemoveNetworkSite  removeNetworkSite) throws OperationException {
		
		NetworkServiceDao networkServiceDao = new NetworkServiceDao();
		
		try
		{
			  //------------------Checking calling user's group rights for manage users------------------------------------------------------------------------------
			HashMap<String, Object> parameters = new HashMap<String, Object>();			
			GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
			int manageOrg = authModule.getAdminManageOrganizationsPrivileges().intValue();
			boolean hasEditPermissions = GroupAuthModule.hasEditPermission((Integer.valueOf(manageOrg)));
			String networkName="";
			int relationshipPK=0;
			String siteName="";
			String siteLevel="";
			String siteRelationshipType="";
			String siteRelationshipCode="";
			String siteRelationshipDesc="";
			String siteRelationshipPK="";
			String networkPK="";
			String networkOID="";
			int count=0;
			
			
			if (!hasEditPermissions)
			{
				response.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to Update organizaion data "));
				throw new AuthorizationException(" User is not authorized to Update organizaion.");
			}
			
			parameters.put("objectMapService", objectMapService);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response);
			parameters.put(NetworkServiceDao.KEY_ACCOUNT_ID, callingUser.getUserAccountId());
			
			if(removeNetworkSite!=null)	{
			
			List<NetworkSiteDetail> networkSiteDetail=removeNetworkSite.getNetworkSiteDetailList();
			NetworkSiteDetail networkSite=new NetworkSiteDetail();
			
			for(int iX=0;iX<networkSiteDetail.size();iX++)
			{
				networkSite=networkSiteDetail.get(iX);
				if(networkSite.getSiteLevelDetails()!=null && !(networkSite.getSiteLevelDetails().equals("")))
				{
				siteName=networkSite.getSiteLevelDetails().getSiteName()==null?"":networkSite.getSiteLevelDetails().getSiteName();
				siteLevel=((networkSite.getSiteLevelDetails().getLevel()==null)?"":(networkSite.getSiteLevelDetails().getLevel()));

				}
				
				
			networkName = (networkSite.getNetworkName()==null)?"": networkSite.getNetworkName();
			relationshipPK = networkSite.getRelationshipPK();
			
			if(networkSite==null)
			{	
				response.addIssue(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "No data found")); 
				throw new OperationException(); 
			}
			if(networkSite.getNetwork()!=null){
				if(networkSite.getNetwork().getNetworkIdentifier().getPK()!=null){
					networkPK = String.valueOf(networkSite.getNetwork().getNetworkIdentifier().getPK());
				}
				 if(networkSite.getNetwork().getNetworkIdentifier().getOID()!=null){
					networkPK = String.valueOf(((ObjectMapService)parameters.get("objectMapService")).getObjectPkFromOID(networkSiteDetail.get(iX).getNetwork().getNetworkIdentifier().getOID()));
				}
				 networkPK=networkPK==null?"": networkPK;
			}
			if(networkPK.equals("") && networkName.equals("") && networkSite.getRelationshipPK()==0){
				response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid network identifier is required")); 
				throw new OperationException();	
			}
			if(( !(networkName.equals("")) ||( !networkPK.equals("")) )&& (siteName.equals("")|| siteLevel.equals(""))&& relationshipPK==0 )
			{
					  
						response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, " Valid siteleveldetail is required")); 
						throw new OperationException();
			}

			if(( !(networkName.equals("")) ||( !networkPK.equals("")) )&& siteName.equals("")&& (siteLevel.equals("")) && (!(siteRelationshipType.equals(""))||!(siteRelationshipCode.equals("")))&& relationshipPK==0)
			{
				response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, " Valid siteleveldetail is  required")); 
				throw new OperationException();
			}
		
			
			
			}
			count= networkServiceDao.removeNetworkSite(removeNetworkSite, parameters);
			if(count>0)
						{
							response.addAction(new CompletedAction("Network site deleted successfully ", CRUDAction.REMOVE));
						}
			else
						{
							response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
							response.addIssue(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "No data found")); 
							throw new OperationException();
				
						}
						}
		
			
		}
		
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl remove", e);
			
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl remove", t);
			
		}
				   
				 
		  return response;		
		
		}
	@Override
	public ResponseHolder removeNetworkUserSite(NetworkSiteUserListDetail networkSiteUserListDetail) throws OperationException {
		ResponseHolder response = new ResponseHolder();
		NetworkServiceDao networkServiceDao = new NetworkServiceDao();
		int count=0;
		try
		{
			System.out.println("removeNetworkUserSite");
			  //------------------Checking calling user's group rights for manage users------------------------------------------------------------------------------
		HashMap<String, Object> parameters = new HashMap<String, Object>();			
			GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
			int manageOrg = authModule.getAdminManageOrganizationsPrivileges().intValue();
			ArrayList<NetworkSiteUserDetail> networkSiteUserDetailList= (ArrayList<NetworkSiteUserDetail>) networkSiteUserListDetail.getNetworkSiteDetailsList();
			boolean hasEditPermissions = GroupAuthModule.hasEditPermission((Integer.valueOf(manageOrg)));
			if (!hasEditPermissions)
			{
				response.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to remove user from Network "));
				throw new AuthorizationException(" User is not authorized to remove user from Network.");
			}
		if(networkSiteUserListDetail == null){	
			           response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid removeNetworkSite Object is required")); 
	                   throw new OperationException(); 
	                                          }
		
			String netPk="";
			String netOid="";
			String siteName="";
			String level="";
			String siteReltype="";
			String siteRelCod="";
			String siteRelPk="";
			
			String relPK="";
			int noOfUsersDeleted=0;
			
			for(int i=0;i<networkSiteUserDetailList.size();i++){

			if(networkSiteUserDetailList.get(i).getNetwork()!=null && !(networkSiteUserDetailList.get(i).getNetwork().equals(""))){
				if(networkSiteUserDetailList.get(i).getNetwork().getNetworkIdentifier()!=null){
					netOid =networkSiteUserDetailList.get(i).getNetwork().getNetworkIdentifier().getOID()==null?"":networkSiteUserDetailList.get(i).getNetwork().getNetworkIdentifier().getOID();
					netPk = networkSiteUserDetailList.get(i).getNetwork().getNetworkIdentifier().getPK()==null?"":networkSiteUserDetailList.get(i).getNetwork().getNetworkIdentifier().getPK()+"";
				}
			}
			String netName=networkSiteUserDetailList.get(i).getNetworkName()==null?"":networkSiteUserDetailList.get(i).getNetworkName();
			if(networkSiteUserDetailList.get(i).getSiteLevelDetails()!=null && !(networkSiteUserDetailList.get(i).getSiteLevelDetails().equals(""))){
			if(networkSiteUserDetailList.get(i).getSiteLevelDetails().getSiteName()==null ||
					networkSiteUserDetailList.get(i).getSiteLevelDetails().getLevel()==null ||
					networkSiteUserDetailList.get(i).getSiteLevelDetails().getRelationshipType()==null){
				response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Combination of level,siteRelationshipType and siteName is required in siteLevelDetails")); 
				throw new OperationException();
			}
			siteName=networkSiteUserDetailList.get(i).getSiteLevelDetails().getSiteName()==null?"":networkSiteUserDetailList.get(i).getSiteLevelDetails().getSiteName();
			 level=networkSiteUserDetailList.get(i).getSiteLevelDetails().getLevel()==null?"":networkSiteUserDetailList.get(i).getSiteLevelDetails().getLevel();
			 siteReltype=networkSiteUserDetailList.get(i).getSiteLevelDetails().getRelationshipType().getType()==null?"":networkSiteUserDetailList.get(i).getSiteLevelDetails().getRelationshipType().getType();
			 siteRelCod=networkSiteUserDetailList.get(i).getSiteLevelDetails().getRelationshipType().getCode()==null?"":networkSiteUserDetailList.get(i).getSiteLevelDetails().getRelationshipType().getCode();
			 siteRelPk=networkSiteUserDetailList.get(i).getSiteLevelDetails().getRelationshipType().getPK()==null?"":networkSiteUserDetailList.get(i).getSiteLevelDetails().getRelationshipType().getPK();
			}
			relPK=networkSiteUserDetailList.get(i).getRelationshipPK()==null?"":networkSiteUserDetailList.get(i).getRelationshipPK();
			
			if(networkSiteUserDetailList.get(i).getUserIdentifier()!=null && networkSiteUserDetailList.get(i).getUserIdentifier().size()>0){
			for(UserNetworkDetail userdetails:networkSiteUserDetailList.get(i).getUserIdentifier()){
				String useroid="";
				String userlog="";
				int pkuseratt=0;
				String usernetpk="";
			if(userdetails!=null && !userdetails.equals("")){
			 useroid=userdetails.getOID()==null?"":userdetails.getOID();
			 pkuseratt=userdetails.getPK()==null?0:userdetails.getPK();
			 userlog = userdetails.getLoginName()==null?"":userdetails.getLoginName();
			 usernetpk=userdetails.getNetworkSiteUserPK()==null?"":userdetails.getNetworkSiteUserPK();
			 
			}
			
			 
			String ipAdd="";
			if(AbstractService.IP_ADDRESS_FIELD_VALUE!=null)
			{
			ipAdd=	AbstractService.IP_ADDRESS_FIELD_VALUE;
			}
			UserBean userBean=(UserBean)callingUser;
			int userId =(userBean.getUserId());
			String accountid=userBean.getUserAccountId();
			int accountId= EJBUtil.stringToNum(accountid);
			System.out.println("userId"+userId+"accountId"+accountId+"ipAdd"+ipAdd);
			
			if(!userlog.equals("") || pkuseratt!=0 || !useroid.equals("")){
				int usercount=networkServiceDao.checkUserExistence(userlog,String.valueOf(pkuseratt),useroid);
				if(usercount==0){
					response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND, "User not exist")); 
					throw new OperationException();
				}
			}
			if(netOid.equals("") && netPk.equals("") && siteName.equals("") && level.equals("") && siteReltype.equals("") && siteRelCod.equals("") && siteRelPk.equals("") && useroid.equals("") && userlog.equals("") && usernetpk.equals("") && relPK.equals("") && useroid.equals("") && userlog.equals("") && usernetpk.equals("") && relPK.equals("") && pkuseratt==0)
			{
				response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid input is required")); 
				throw new OperationException();
				
			}
			if(!usernetpk.equals("")){
				

				count= networkServiceDao.removeNetworkUserSite(usernetpk,pkuseratt,accountId,ipAdd);
				if(count>0){
					noOfUsersDeleted=noOfUsersDeleted+1;
					}else{
						response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
						response.addIssue(new Issue(IssueTypes.NETWORK_USER_NOT_FOUND, "Network user not found")); 
						throw new OperationException();
						
					}
				
			}
			
			else{
				
				if(!userlog.equals("") || pkuseratt!=0 || !useroid.equals("")){
					if(!relPK.equals("")){									

						count=networkServiceDao.removeNetworkUserSite(userlog,pkuseratt,useroid,EJBUtil.stringToNum(relPK),accountId,ipAdd);
						
						if(count>0){
							noOfUsersDeleted=noOfUsersDeleted+1;
							}else{
								response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
								response.addIssue(new Issue(IssueTypes.NETWORK_USER_NOT_FOUND, "Network user not found")); 
								throw new OperationException();
								
							}
						
					}
					else if(!netName.equals("") || !netPk.equals("")|| !netOid.equals("")){
						if(!siteName.equals("") && !level.equals("")){														
							if((!siteReltype.equals("") && !siteRelCod.equals("")) ||!siteRelPk.equals("") ){
								
								count=networkServiceDao.removeNetworkUserSite(userlog,pkuseratt,useroid,netOid,netName,netPk,siteName,EJBUtil.stringToNum(level),siteReltype,siteRelCod,siteRelPk,accountId,ipAdd);									
								
								if(count>0){
									noOfUsersDeleted=noOfUsersDeleted+1;
									}else{
										response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
										response.addIssue(new Issue(IssueTypes.NETWORK_USER_NOT_FOUND, "Network user not found")); 
										throw new OperationException();
										
									}
							}
							else{
								response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
								response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Relationship Type Code Not Found")); 
								throw new OperationException();
							}
							
						}
						else{
							response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
							response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Site Name/Level Identifier are required ")); 
							throw new OperationException();
							
						}
						
						
						
						
						
					}
					else{
						response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "relationshipPK or NetworkIdentifier or NetworkName required ")); 
						throw new OperationException();
					}
					
					
				} else {
					
					response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User logName,UserPK are Identifier  required ")); 
					throw new OperationException();
				}
				
				
			}
			
			}
			}else{
				response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "userIdentifier is required in the input ")); 
				throw new OperationException();
			}
			}
			if(noOfUsersDeleted>0){
				response.addAction(new CompletedAction("Network site user deleted successfully ", CRUDAction.REMOVE));
			}
		}
		
			
		catch(OperationException e){
			System.out.println("Inside catch");
			

			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl removeNetworkUserSite ", e);
			e.setIssues(response.getIssues());
			
		}
			catch(Throwable t){
				sessionContext.setRollbackOnly();
				this.addUnknownThrowableIssue(t);
				if (logger.isDebugEnabled()) logger.debug("UserServiceImpl removeNetworkUserSite", t);
			}
		
		return response;
	
	}
	@Override
	public ResponseHolder createNetworkSite(NetworkSiteWrapper networkSite) throws OperationException {
		try
		{
			  //------------------Checking calling user's Organization rights for network users------------------------------------------------------------------------------
			
	    GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
		int manageOrg = authModule.getAdminManageUsersPrivileges().intValue();
		boolean hasNewPermissions = GroupAuthModule.hasNewPermission((Integer.valueOf(manageOrg)));
		if (!hasNewPermissions)
		{
			response.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to view organizaion data "));
			throw new AuthorizationException(" User is not authorized to view  organizaion data.");
		}

		if(networkSite.getNetwork()==null || (networkSite.getSite()==null  || networkSite.getSite().size()==0))
		{
			response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid data is required."));
			throw new OperationException(); 
	    }
		
			Map<String, Object> parameters = new HashMap<String, Object>();
		    parameters.put("sessionContext", sessionContext);
			parameters.put("objectMapService", objectMapService);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response);
			parameters.put("userAgent", userAgent);
			parameters.put(NetworkServiceDao.KEY_ACCOUNT_ID, callingUser.getUserAccountId());
			NetworkServiceDao nsDao = new NetworkServiceDao();
			nsDao.createNetworkSite(networkSite.getNetwork(),networkSite.getSite(),parameters);
			
		}catch(OperationException e){
		
		sessionContext.setRollbackOnly();
		if (logger.isDebugEnabled()) logger.debug("UserServiceImpl createNetworkSite ", e);
		e.setIssues(response.getIssues());
		//throw e;
		//throw new OperationRolledBackException(response.getIssues());
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl createNetworkSite", t);
		}	
		return response;
	}
	
	public ResponseHolder updateNetworkSite(UpdatNetworkSiteDetails updateNetworkSiteDetail)
			throws OperationException {
		NetSiteDetails networkDetails = null;
		NetworkServiceDao networkServiceDao = new NetworkServiceDao();
		try
			{
				  //------------------Checking calling user's Organization rights for network users------------------------------------------------------------------------------
			
		    GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
			int manageOrg = authModule.getAdminManageUsersPrivileges().intValue();
			boolean hasEditPermissions = GroupAuthModule.hasEditPermission((Integer.valueOf(manageOrg)));
			if (!hasEditPermissions)
			{
				response.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to Edit Network Site data "));
				throw new AuthorizationException(" User is not authorized to Edit Network Site data.");
			}

			ArrayList<NetSiteDetails> networkList=(ArrayList<NetSiteDetails>)updateNetworkSiteDetail.getNetworkSiteDetailsList();
			Map<String, Object> parameters = new HashMap<String, Object>();
			if(networkList==null || networkList.size()<0){
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid data is Required."));
				throw new OperationException();
			}
			else{
				for(int i=0;i<networkList.size();i++){
					networkDetails=networkList.get(i);
					if(networkDetails.getRelationshipPK()==null){
						if(networkDetails.getNetworkIdentifier()==null && networkDetails.getNetworkName()==null && networkDetails.getSiteLevelDetail()==null){
							response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
							response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Either combination of networkName and siteLevelDetails or relationshipPK is required")); 
							throw new OperationException();
						}
						else if((networkDetails.getNetworkIdentifier()==null && networkDetails.getNetworkName()==null && networkDetails.getSiteLevelDetail()!=null) ||
						   ((networkDetails.getNetworkIdentifier()!=null || networkDetails.getNetworkName()!=null) && networkDetails.getSiteLevelDetail()==null)){
							response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
							response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Either networkIdentifier or networkName with siteLevelDetails is required")); 
								throw new OperationException();
						}
						else if(networkDetails.getSiteLevelDetail().getSiteName() == null || networkDetails.getSiteLevelDetail().getLevel() == null || networkDetails.getSiteLevelDetail().getRelationshipType() == null)
						{
							response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
							response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "All values of siteLevelDetails is required")); 
							throw new OperationException();
						}
						else if(networkDetails.getSiteLevelDetail().getRelationshipType() != null){
							CodeDao cd = new CodeDao();
							String pkCode="0";
							if(networkDetails.getSiteLevelDetail().getRelationshipType().getPK()!=null){
								String code_type = cd.getCodeType(StringUtil.stringToNum((networkDetails.getSiteLevelDetail().getRelationshipType().getPK())));
								if(code_type.equals("relnshipTyp")){
									pkCode=networkDetails.getSiteLevelDetail().getRelationshipType().getPK();
								}
							}
							else if(networkDetails.getSiteLevelDetail().getRelationshipType().getType()!=null && networkDetails.getSiteLevelDetail().getRelationshipType().getCode()!=null){
								pkCode = StringUtil.integerToString(cd.getCodeId(networkDetails.getSiteLevelDetail().getRelationshipType().getType(), networkDetails.getSiteLevelDetail().getRelationshipType().getCode()));
							}
							else if(networkDetails.getSiteLevelDetail().getRelationshipType().getType()!=null && networkDetails.getSiteLevelDetail().getRelationshipType().getDescription()!=null){
								String code_type=networkDetails.getSiteLevelDetail().getRelationshipType().getType();
								if(code_type.equals("relnshipTyp")){
									pkCode=StringUtil.integerToString(cd.getCodeIdFromDesc(code_type,networkDetails.getSiteLevelDetail().getRelationshipType().getDescription()));
								}
							}

							if(pkCode.equals("0")){
								response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
								response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "siteRelationshipType code not found for siteLevelDetails")); 
								throw new OperationException();
							}
							else{
								parameters.put("pkRelationshipType_"+i, pkCode);
							}
						}
					}
					
					if(networkDetails.getNewSiteRelationshipType() != null){
						CodeDao cd = new CodeDao();
						String pkCode="0";
						
						if(networkDetails.getNewSiteRelationshipType().getPK()!=null){
							String code_type = cd.getCodeType(StringUtil.stringToNum((networkDetails.getNewSiteRelationshipType().getPK())));
							if(code_type.equals("relnshipTyp")){
								pkCode=networkDetails.getNewSiteRelationshipType().getPK();
							}
						}
						else if(networkDetails.getNewSiteRelationshipType().getType()!=null && networkDetails.getNewSiteRelationshipType().getCode()!=null){
							pkCode = StringUtil.integerToString(cd.getCodeId(networkDetails.getNewSiteRelationshipType().getType(), networkDetails.getNewSiteRelationshipType().getCode()));
						}
						else if(networkDetails.getNewSiteRelationshipType().getType()!=null && networkDetails.getNewSiteRelationshipType().getDescription()!=null){
							String code_type=networkDetails.getNewSiteRelationshipType().getType();
							if(code_type.equals("relnshipTyp")){
								pkCode=StringUtil.integerToString(cd.getCodeIdFromDesc(code_type,networkDetails.getNewSiteRelationshipType().getDescription()));
							}
						}

					    if(pkCode.equals("0")){
					    	response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					    	response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "newSiteRelationshipType code not found for network site")); 
							throw new OperationException();
						}
						else{
						parameters.put("pkNewRelationshipType_"+i, pkCode);
						}
					}
					
					if(networkDetails.getSiteStatus() != null){
						CodeDao cd = new CodeDao();
						String pkCode="0";
						
						if(networkDetails.getSiteStatus().getPK()!=null){
							String code_type = cd.getCodeType(StringUtil.stringToNum((networkDetails.getSiteStatus().getPK())));
							if(code_type.equals("networkstat")){
								pkCode=networkDetails.getSiteStatus().getPK();
							}
						}
						else if(networkDetails.getSiteStatus().getType()!=null && networkDetails.getSiteStatus().getCode()!=null){
					    	pkCode = StringUtil.integerToString(cd.getCodeId(networkDetails.getSiteStatus().getType(), networkDetails.getSiteStatus().getCode()));
						}
						
						else if(networkDetails.getSiteStatus().getType()!=null && networkDetails.getSiteStatus().getDescription()!=null){
							String code_type=networkDetails.getSiteStatus().getType();
							if(code_type.equals("networkstat")){
								pkCode=StringUtil.integerToString(cd.getCodeIdFromDesc(code_type,networkDetails.getSiteStatus().getDescription()));
							}
						}

					    if(pkCode.equals("0")){
					    	response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					    	response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "siteStatus code not found for network site")); 
							throw new OperationException();
						}
						else{			
						parameters.put("pkStatus_"+i, pkCode);
						}
					}
				}
			}
			
		    parameters.put("sessionContext", sessionContext);
			parameters.put("objectMapService", objectMapService);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response);
			parameters.put("userAgent", userAgent);
			parameters.put(NetworkServiceDao.KEY_ACCOUNT_ID, callingUser.getUserAccountId());
			
			networkServiceDao.updateNetworkSite(updateNetworkSiteDetail, (HashMap<String, Object>) parameters);
			}catch(OperationException e){
			
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl NetworkUsersSearchResult ", e);
			e.setIssues(response.getIssues());
			}
			catch(Throwable t){
				sessionContext.setRollbackOnly();
				this.addUnknownThrowableIssue(t);
				if (logger.isDebugEnabled()) logger.debug("UserServiceImpl NetworkUsersSearchResult", t);
			}
		
			return response;
		
	
}
	

	public ResponseHolder createNetworkSiteUser(CreateNetworkSiteUser createNetworkSiteUser) throws OperationException {
		try
		{
			  //------------------Checking calling user's Organization rights for network users------------------------------------------------------------------------------	
	    GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
		int manageOrg = authModule.getAdminManageUsersPrivileges().intValue();
		boolean hasNewPermissions = GroupAuthModule.hasNewPermission((Integer.valueOf(manageOrg)));

		if (!hasNewPermissions)
		{
			response.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to view organizaion data "));
			throw new AuthorizationException(" User is not authorized to view  organizaion data.");

		}
		if(createNetworkSiteUser.getNetwork()==null && createNetworkSiteUser.getUser()==null)
		{
			response.addAction(new CompletedAction("Data Issues", CRUDAction.RETRIEVE));
			response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid data is required."));
			throw new OperationException(); 
	    }
		if(createNetworkSiteUser.getNetwork()==null)
		{
			response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid network identifier is required."));
			throw new OperationException(); 
	    }
		if(createNetworkSiteUser.getUser()==null)
		{
			response.addAction(new CompletedAction("User Issues", CRUDAction.RETRIEVE));
			response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid user identifier is required."));
			throw new OperationException(); 
	    }
		Integer networkPk=0;
		String networkOid="";
		String networkName=createNetworkSiteUser.getNetwork().getName()==null?"":createNetworkSiteUser.getNetwork().getName();
		if(createNetworkSiteUser.getNetwork().getNetworkIdentifier()!=null){
		networkPk=createNetworkSiteUser.getNetwork().getNetworkIdentifier().getPK()==null?0:createNetworkSiteUser.getNetwork().getNetworkIdentifier().getPK();
		networkOid=createNetworkSiteUser.getNetwork().getNetworkIdentifier().getOID()==null?"":createNetworkSiteUser.getNetwork().getNetworkIdentifier().getOID();
		}
		String relationshipPk=createNetworkSiteUser.getNetwork().getRelationshipPK()==null?"":createNetworkSiteUser.getNetwork().getRelationshipPK();
		
		if((networkName.equals("")) && (createNetworkSiteUser.getNetwork().getNetworkIdentifier()==null || createNetworkSiteUser.getNetwork().getNetworkIdentifier().equals("")) && (relationshipPk.equals(""))){
			response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid networkIdentifier or networkName or relationshipPK is required."));
			throw new OperationException(); 
		}
		if((networkName.equals("")) && (networkOid.equals("")) && (networkPk.equals(0)) && (relationshipPk.equals(""))){
			response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid networkIdentifier or networkName or relationshipPK is required."));
			throw new OperationException(); 
		}
		
			Map<String, Object> parameters = new HashMap<String, Object>();

		    parameters.put("sessionContext", sessionContext);
			parameters.put("objectMapService", objectMapService);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response);
			parameters.put("userAgent", userAgent);
			parameters.put(NetworkServiceDao.KEY_ACCOUNT_ID, callingUser.getUserAccountId());
			NetworkServiceDao nsDao = new NetworkServiceDao();
			nsDao.createNetworkSiteUser(createNetworkSiteUser,parameters);

		}catch(OperationException e){

		sessionContext.setRollbackOnly();
		if (logger.isDebugEnabled()) logger.debug("UserServiceImpl createNetworkSiteUser ", e);
		e.setIssues(response.getIssues());
		//throw e;
		//throw new OperationRolledBackException(response.getIssues());
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl createNetworkSiteUser", t);
		}	
		return response;
	}
	

	public ResponseHolder updateNetworkSiteUser(UpdateNetwrokUsersWrapper networkSiteUser) throws OperationException {
		
		NetworkServiceDao networkServiceDao = new NetworkServiceDao();
		try
			{

				  //------------------Checking calling user's Organization rights for network users------------------------------------------------------------------------------
			
		    GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
			int manageOrg = authModule.getAdminManageUsersPrivileges().intValue();
			boolean hasEditPermissions = GroupAuthModule.hasEditPermission((Integer.valueOf(manageOrg)));
			if (!hasEditPermissions)
			{


				response.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to Edit Network Site User data "));
				throw new AuthorizationException(" User is not authorized to Edit Network Site User data.");
			}


			ArrayList<NetSiteUser> networkUserList=(ArrayList<NetSiteUser>)networkSiteUser.getNetworkSiteUser();



			Map<String, Object> parameters = new HashMap<String, Object>();
			if(networkUserList==null || networkUserList.size()<0){
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid data is Required."));
				throw new OperationException();
			}
			
			
			parameters.put("sessionContext", sessionContext);
			parameters.put("objectMapService", objectMapService);
			parameters.put("callingUser", callingUser);
			parameters.put("response", response);
			parameters.put("userAgent", userAgent);
			parameters.put(NetworkServiceDao.KEY_ACCOUNT_ID, callingUser.getUserAccountId());
			networkServiceDao.updateNetworkSiteUser(networkSiteUser, (HashMap<String, Object>) parameters);
			}catch(OperationException e){
			
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl UpdateNetworkSiteUser ", e);
			e.setIssues(response.getIssues());
			}



			catch(Throwable t){
				sessionContext.setRollbackOnly();
				this.addUnknownThrowableIssue(t);
				if (logger.isDebugEnabled()) logger.debug("UserServiceImpl UpdateNetworkSiteUser", t);
			}
		
			return response;
		


	
}
	
	@Override
	public ResponseHolder removeNetwork(RemoveNetwork  removeNetworkSite) throws OperationException {
		
		try
		{
			  //------------------Checking calling user's group rights for manage users------------------------------------------------------------------------------
			HashMap<String, Object> parameters = new HashMap<String, Object>();			
			GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
			int manageOrg = authModule.getAdminManageOrganizationsPrivileges().intValue();
			boolean hasEditPermissions = GroupAuthModule.hasEditPermission((Integer.valueOf(manageOrg)));
			
			int count=0;
			Integer pkNetList=0;
			
			
			if (!hasEditPermissions)
			{
				response.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to Update organizaion data "));
				throw new AuthorizationException(" User is not authorized to Update organizaion.");
			}
			
			parameters.put("objectMapService", objectMapService);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response);
			parameters.put(NetworkServiceDao.KEY_ACCOUNT_ID, callingUser.getUserAccountId());
			NetworkServiceDao networkServiceDao = new NetworkServiceDao();
			pkNetList=networkServiceDao.removeNetwork(removeNetworkSite, parameters);
		
			
			if(pkNetList>0){
				response.addAction(new CompletedAction("Network deleted successfully ", CRUDAction.REMOVE));
			}else{
				response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "networkIdentifier or networkName is required")); 
				throw new OperationException();	
			}
			
		}
		
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl remove", e);
			
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl remove", t);
			
		}
				   
				 
		  return response;		
		
		}
	@Override
	public ResponseHolder createNetwork(CreateNetwork network) throws OperationException {
		try{
		    GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
			int manageOrg = authModule.getAdminManageUsersPrivileges().intValue();
			boolean hasNewPermissions = GroupAuthModule.hasNewPermission((Integer.valueOf(manageOrg)));
			if (!hasNewPermissions)
			{
				response.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to view organizaion data "));
				throw new AuthorizationException(" User is not authorized to view  organizaion data.");
			}
			if(network.getNetworkSite()==null  || network.getNetworkSite().size()==0)
			{
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid data is required."));
				throw new OperationException(); 
		    }
			Map<String, Object> parameters = new HashMap<String, Object>();
		    parameters.put("sessionContext", sessionContext);
			parameters.put("objectMapService", objectMapService);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response);
			parameters.put("userAgent", userAgent);
			parameters.put(NetworkServiceDao.KEY_ACCOUNT_ID, callingUser.getUserAccountId());
			NetworkServiceDao nsDao = new NetworkServiceDao();
			nsDao.createNetwork(network.getNetworkSite(),parameters);
		}catch(OperationException e){		
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl createNetwork ", e);
			e.setIssues(response.getIssues());
			
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl createNetwork", t);
		}	
		return response;	
	}
	
	public ResponseHolder updateNetwork(UpdateNetwork updateNetworkDetails)
			throws OperationException {
		NetSiteDetails networkDetails = null;
		NetworkServiceDao networkServiceDao = new NetworkServiceDao();
		try
			{
				  //------------------Checking calling user's Organization rights for network users------------------------------------------------------------------------------
			
		    GroupAuthModule authModule = new GroupAuthModule(this.callingUser,groupRightsAgent);
			int manageOrg = authModule.getAdminManageUsersPrivileges().intValue();
			boolean hasEditPermissions = GroupAuthModule.hasEditPermission((Integer.valueOf(manageOrg)));
			if (!hasEditPermissions)
			{
				response.addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION," Calling User is not authorized to Edit Network Site data "));
				throw new AuthorizationException(" User is not authorized to Edit Network Site data.");
			}

			ArrayList<NetSiteDetails> networkList=(ArrayList<NetSiteDetails>)updateNetworkDetails.getNetworkSiteDetailsList();
			Map<String, Object> parameters = new HashMap<String, Object>();
			if(networkList==null || networkList.size()<0){
				response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid data is Required."));
				throw new OperationException();
			}
			else{
				for(int i=0;i<networkList.size();i++){
					networkDetails=networkList.get(i);

					if(networkDetails.getNetIdentifier()==null && networkDetails.getNetworkName()==null){
						response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
						response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Either networkIdentifier or networkName is required")); 
						throw new OperationException();
					}
					
					if(networkDetails.getNewSiteRelationshipType() != null){
						int pkCode=networkServiceDao.getCodeByMultiInput(networkDetails.getNewSiteRelationshipType(), "relnshipTyp");

					    if(pkCode==0){
					    	response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					    	response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "newSiteRelationshipType code not found for network site")); 
							throw new OperationException();
						}
						else{
						parameters.put("pkNewRelationshipType_"+i, pkCode);
						}
					}
					
					if(networkDetails.getSiteStatus() != null){
						int pkCode=networkServiceDao.getCodeByMultiInput(networkDetails.getSiteStatus(), "networkstat");

					    if(pkCode==0){
					    	response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					    	response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "siteStatus code not found for network site")); 
							throw new OperationException();
						}
						else{			
						parameters.put("pkStatus_"+i, pkCode);
						}
					}
				}
			}
			
		    parameters.put("sessionContext", sessionContext);
			parameters.put("objectMapService", objectMapService);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response);
			parameters.put("userAgent", userAgent);
			parameters.put(NetworkServiceDao.KEY_ACCOUNT_ID, callingUser.getUserAccountId());
			
			networkServiceDao.updateNetwork(updateNetworkDetails, (HashMap<String, Object>) parameters);
			}catch(OperationException e){
			
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("UserServiceImpl NetworkUsersSearchResult ", e);
			e.setIssues(response.getIssues());
			}
			catch(Throwable t){
				sessionContext.setRollbackOnly();
				this.addUnknownThrowableIssue(t);
				if (logger.isDebugEnabled()) logger.debug("UserServiceImpl NetworkUsersSearchResult", t);
			}
		
			return response;
	}
		
}
		

	


