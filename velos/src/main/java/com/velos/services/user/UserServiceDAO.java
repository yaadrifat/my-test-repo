package com.velos.services.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.ElementNSImpl;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.BrowserRows;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.FilterUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.model.FormField.SortOrder;
import com.velos.services.model.MoreDetailValue;
import com.velos.services.model.MoreStudyDetails;
import com.velos.services.model.UserSearch;

public class UserServiceDAO extends CommonDAO {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(UserServiceDAO.class);
	
	private static final String SPACE_STR = " ";
	private static final String ASC_STR = "asc";
	private static final String DEFAULT_ORDER_BY = "lower(usr_lastname)";
	private static final String NULL_STR = "null";
	private static final String EMPTY_STR = "";
	private static final String COL_PK_USER = "pk_user";
	private static final String COL_FIRSTNAME = "USR_FIRSTNAME";
	private static final String COL_LASTNAME = "USR_LASTNAME";
	private static final String COL_MIDDLENAME = "USR_MIDNAME";
	private static final String COL_LOGNAME = "USR_LOGNAME";
	private static final String COL_SITE_NAME = "site_name";
	private static final String COL_USR_STAT = "usr_stat";
	private static final String COL_USR_TYPE = "usr_type";
	private static final String COL_ADDRESS = "ADDRESS";
	private static final String COL_ADD_CITY = "ADD_CITY";
	private static final String COL_ADD_STATE = "ADD_STATE";
	private static final String COL_ADD_ZIPCODE = "ADD_ZIPCODE";
	private static final String COL_ADD_EMAIL = "ADD_EMAIL";
	private static final String COL_ADD_PHONE = "ADD_PHONE";
	private static final String COL_FK_GRP_DEFAULT = "FK_GRP_DEFAULT";
	private static final String COL_GRP_NAME = "GRP_NAME";
	
	/*public static final String COL_MD_MODELEMENTPK = "CODELST_SUBTYP";
	public static final String COL_MD_MODELEMENTDATA = "MD_MODELEMENTDATA";
	public static final String COL_CODELST_TYPE = "CODELST_TYPE";
	public static final String COL_CODELST_DESC = "CODELST_DESC";
	public static final String COL_FK_MODPK = "FK_MODPK";
	public static final String COL_MD_MODNAME = "MD_MODNAME";*/
	
	
	public static final String KEY_ACCOUNT_ID = "accountId";
	public static final String KEY_USER_PK = "pk_user";
	public static final String KEY_USER_LOGIN = "user_logname";
	public static final String KEY_ORG_NAME = "orgName";
	public static final String KEY_USER_GROUP = "userGroup";
	public static final String KEY_STUDY_TEAM = "studyTeam";
	public static final String KEY_JOB_CODE_PK = "jobPk";
	public static final String KEY_PAGE = "page";
	public static final String KEY_PAGE_SIZE = "pageSize";
	public static final String KEY_ORDER_BY = "orderBy";
	public static final String KEY_ORDER_TYPE = "orderType";
	
	private Long totalCount = 0L;
	private Long pageSize = 0L;
	private Integer pageNumber = 0;
	
	private ArrayList<String> pkUserList = new ArrayList<String>();
	private ArrayList<String> firstNameList = new ArrayList<String>();
	private ArrayList<String> lastNameList = new ArrayList<String>();
	private ArrayList<String> middleNameList = new ArrayList<String>();
	private ArrayList<String> loginNameList = new ArrayList<String>();
	private ArrayList<String> orgNameList = new ArrayList<String>();
	private ArrayList<String> usrStatList = new ArrayList<String>();
	private ArrayList<String> usrTypeList = new ArrayList<String>();
	private ArrayList<String> addressList = new ArrayList<String>();
	private ArrayList<String> addCityList = new ArrayList<String>();
	private ArrayList<String> addStateList = new ArrayList<String>();
	private ArrayList<String> addZipList = new ArrayList<String>();
	private ArrayList<String> addEmailList = new ArrayList<String>();
	private ArrayList<String> addPhoneList = new ArrayList<String>();
	private ArrayList<String> fkDefaultGroupList = new ArrayList<String>();
	private ArrayList<String> defaultGroupList = new ArrayList<String>();
	
	/*private ArrayList<String> codelst_pk_key = new ArrayList<String>();
	private ArrayList<String> more_value = new ArrayList<String>();
	private ArrayList<String> codelst_typ = new ArrayList<String>();
	private ArrayList<String> more_description = new ArrayList<String>();*/
	
	
	public ArrayList<String> getFirstNameList() { return firstNameList; }
	public ArrayList<String> getLastNameList() { return lastNameList; }
	public ArrayList<String> getOrgNameList() { return orgNameList; }
	public ArrayList<String> getPkUserList() { return pkUserList; }
	public ArrayList<String> getMiddleNameList() { return middleNameList; }
	public ArrayList<String> getLoginNameList() { return loginNameList; }
	public ArrayList<String> getUsrStatList() { return usrStatList; }
	public ArrayList<String> getUsrTypeList() { return usrTypeList; }
	public ArrayList<String> getAddressList() { return addressList; }
	public ArrayList<String> getAddCityList() { return addCityList; }
	public ArrayList<String> getAddStateList() { return addStateList; }
	public ArrayList<String> getAddZipList() { return addZipList; }
	public ArrayList<String> getAddEmailList() { return addEmailList; }
	public ArrayList<String> getAddPhoneList() { return addPhoneList; }
	public ArrayList<String> getFkDefaultGroupList() { return fkDefaultGroupList; }
	public ArrayList<String> getDefaultGroupList() { return defaultGroupList; }
	
	/*public ArrayList<String> getKey() { return codelst_pk_key; }
	public ArrayList<String> getValue() { return more_value; }
	public ArrayList<String> getType() { return codelst_typ; }
	public ArrayList<String> getDescription() { return more_description; }*/

	public Long getTotalCount() { return totalCount; }
	public Long getPageSize() { return pageSize; }
	public Integer getPageNumber() { return pageNumber; }
	
	public void searchUser(UserSearch userSearch, HashMap<String, String> params) {
		int iaccId = StringUtil.stringToNum(params.get(KEY_ACCOUNT_ID));

		int userPk = 0;
		if (userSearch.getUserPK() >0) {
			userPk = userSearch.getUserPK();
		}
		String orgName = null;
		String orgAltId = null;
		Integer orgPk = null;
		if (userSearch.getOrganization() != null) {
			orgPk = userSearch.getOrganization().getPK();
			if (orgPk == null || orgPk < 1) {
				orgName = userSearch.getOrganization().getSiteName();
				if (StringUtil.isEmpty(orgName)) {
					orgAltId = userSearch.getOrganization().getSiteAltId();
				}
			}
		}
		String grpPk = StringUtil.trueValue(params.get(KEY_USER_GROUP));
		String gName = null;
		if (userSearch.getGroup()!=null && userSearch.getGroup().getGroupName() != null) {
			gName = userSearch.getGroup().getGroupName();
		}
		String stTeam = StringUtil.trueValue(params.get(KEY_STUDY_TEAM));
		String ufname = userSearch.getFirstName();
		String ulname = userSearch.getLastName();
		String uloginname = userSearch.getLoginName();
		String uemailId = userSearch.getEmail();
		String keySearch = "";	
		/*
		Collection<MoreStudyDetails> moreUserDetails =userSearch.getMoreUserDetails();
		MoreStudyDetails obj=new MoreStudyDetails();
			
		userSearch.setMoreUserDetails((java.util.List<MoreStudyDetails>) moreUserDetails);
		if(moreUserDetails!=null){
				String keyVlaue="'";
				for(int i=0;i<userSearch.getMoreUserDetails().size();i++){
					obj=	userSearch.getMoreUserDetails().set(i, obj);
					keySearch	=obj.getKey()==null?"":obj.getKey();
					keyVlaue=keyVlaue.concat(keySearch).concat("','");				
								
				}
				int keylenth=keyVlaue.length();
				keySearch=keyVlaue.substring(0, keylenth-2);
				moreUserDetails.addAll(moreUserDetails);
				
			}*/
		String jobCodePk = params.get(KEY_JOB_CODE_PK);
		int curPage = userSearch.getPageNumber();
		if (curPage < 1) {
			curPage = 1;
		}
		pageNumber = curPage;
		long rowsPerPage = Configuration.MOREBROWSERROWS;
		String inputPageSize = params.get(KEY_PAGE_SIZE);
		if (!StringUtil.isEmpty(inputPageSize)) {
			try {
				rowsPerPage = Long.parseLong(inputPageSize);
			} catch(Exception e) {
				rowsPerPage = Configuration.MOREBROWSERROWS;
			}
		}
		if (rowsPerPage > 50L) {
			rowsPerPage = 50L;
		}
		String orderBy = FilterUtil.sanitizeTextForSQL(params.get(KEY_ORDER_BY));
		if (StringUtil.isEmpty(orderBy)) {
			orderBy = DEFAULT_ORDER_BY;
		}
		String orderType = FilterUtil.sanitizeTextForSQL(params.get(KEY_ORDER_TYPE));
		if (StringUtil.isEmpty(orderType)) {
			orderType = ASC_STR;
		}
		
		String uWhere = null;
		String lWhere = null;
		String lnWhere = null;
		String mWhere = null;
 	 	String fWhere = null;
		String oWhere = null;
		String rWhere = null;
		String aWhere = null;
		String gWhere = null;
		String sWhere = null;
		String keyWhere = null;

 	 	StringBuffer completeSelect = new StringBuffer();
 	 	String searchFNameForSQL= StringUtil.escapeSpecialCharSQL(ufname);
 	 	String searchLNameForSQL= StringUtil.escapeSpecialCharSQL(ulname);
 	 	String searchLoginNameForSQL= StringUtil.escapeSpecialCharSQL(uloginname);
 	 	String searchUserEMailIDForSQL = StringUtil.escapeSpecialCharSQL(uemailId);
 	 	String searchkey1ForSQL=  keySearch;
 	 	String str1 = "SELECT distinct u.pk_user,u.USR_LASTNAME, "
 				  + " s.PK_SITE, s.site_name, "
 				  + " u.USR_FIRSTNAME, u.USR_MIDNAME, u.USR_LOGNAME, u.USR_STAT, u.USR_TYPE "
 				  + ", u.FK_GRP_DEFAULT, (select GRP_NAME from ER_GRPS where PK_GRP = u.FK_GRP_DEFAULT) GRP_NAME "
 				  + " , a.ADDRESS, a.ADD_CITY, a.ADD_STATE, a.ADD_ZIPCODE, a.ADD_EMAIL, a.ADD_PHONE "
 				  + " from  ER_USER u, ER_ADD a, ER_SITE s "
 	 	 		  + " Where s.PK_SITE  = u.FK_SITEID and u.fk_peradd = a.pk_add and u.USR_STAT in('A','B','D') AND "
 				  + "(u.USR_TYPE <>'X' and u.USR_TYPE <> 'P' ) and u.USER_HIDDEN <> 1";
 	 	if (userPk > 0) {
 	 		uWhere = " and u.PK_USER = "+userPk;
 	 	}
 	 	if (orgPk != null && orgPk > 0) {
 	 		oWhere = " and s.PK_SITE = "+orgPk;
 	 	} else {
 	 		if ((orgName != null) && (!orgName.trim().equals(EMPTY_STR) && !orgName.trim().equals(NULL_STR))) {
 	 			oWhere  = " and UPPER(s.SITE_NAME) = UPPER('" ;
 	 			oWhere += FilterUtil.sanitizeTextForSQL(orgName);
 	 			oWhere += "')";
 	 		} else if (!StringUtil.isEmpty(orgAltId)) {
 	 			oWhere  = " and UPPER(s.SITE_ALTID) = UPPER('" ;
 	 			oWhere += FilterUtil.sanitizeTextForSQL(orgAltId);
 	 			oWhere += "')";
 	 		}
 	 	}
		if ((jobCodePk != null) && (!jobCodePk.trim().equals(EMPTY_STR) && !jobCodePk.trim().equals(NULL_STR))) {
			rWhere = " and UPPER(u.FK_CODELST_JOBTYPE) = UPPER(" ;
			rWhere+=FilterUtil.sanitizeNumberForSQL(jobCodePk);
			rWhere+=")";
		}
		if ((ufname != null) && (!ufname.trim().equals(EMPTY_STR) && !ufname.trim().equals(NULL_STR))) {
			fWhere = " and UPPER(u.USR_FIRSTNAME) like UPPER('" ;
			fWhere+=searchFNameForSQL.trim();
			fWhere+="%')";
		}
		if ((ulname != null) && (!ulname.trim().equals(EMPTY_STR)  && !ulname.trim().equals(NULL_STR))) {
			lWhere = " and UPPER(u.USR_LASTNAME) like UPPER('";
			lWhere+=searchLNameForSQL.trim();
			lWhere+="%')";
		}
		if ((uloginname != null) && (!uloginname.trim().equals(EMPTY_STR)  && !uloginname.trim().equals(NULL_STR))) {
			lnWhere = " and u.USR_LOGNAME = '";
			lnWhere+=searchLoginNameForSQL.trim()+"'";
		}
		if ((uemailId != null) && (!uemailId.trim().equals(EMPTY_STR)  && !uemailId.trim().equals(NULL_STR))) {
			mWhere = " and a.ADD_EMAIL = '";
			mWhere+=searchUserEMailIDForSQL.trim()+"'";
		}
		if ((grpPk != null) && (!grpPk.trim().equals(EMPTY_STR) && !grpPk.trim().equals(NULL_STR))) {
			gWhere=" and u.pk_user in(select fk_user from er_usrgrp where upper(fk_grp)=upper(";
			gWhere +=FilterUtil.sanitizeNumberForSQL(grpPk);
			gWhere +="))";
		} else if (!StringUtil.isEmpty(gName)) {
			gWhere=" and u.pk_user in (select fk_user from er_usrgrp where fk_grp in (select pk_grp from er_grps where fk_account="
					+iaccId+" and grp_name like '%";
			gWhere += FilterUtil.sanitizeTextForSQL(gName);
			gWhere +="%'))";
		}
		if ((stTeam != null) && (!stTeam.trim().equals(EMPTY_STR) && !stTeam.trim().equals(NULL_STR))) {
   			sWhere =" and ( exists (select * from er_studyteam t where t.fk_user = pk_user and t.fk_study =  ";
			sWhere +=FilterUtil.sanitizeNumberForSQL(stTeam);
			sWhere +=" ) or pkg_superuser.F_Is_Superuser(pk_user, "+FilterUtil.sanitizeNumberForSQL(stTeam)+") = 1  )";
		}
		if ( (searchkey1ForSQL != null) && (!searchkey1ForSQL.trim().equals(EMPTY_STR) && !searchkey1ForSQL.trim().equals(NULL_STR))) {
			keyWhere = " and cd.CODELST_SUBTYP IN (" ;
			keyWhere+=FilterUtil.sanitizeTextForSQL(searchkey1ForSQL);
			keyWhere+=")";
 	 	}

		completeSelect.append(str1);
		if (iaccId > 0) {
			aWhere = " and u.fk_account = " + iaccId;
			completeSelect.append(aWhere);
		}
		if (uWhere != null) {
			completeSelect.append(uWhere);
		}
		if (fWhere != null) {
			completeSelect.append(fWhere);
		}
		if (lWhere != null) {
			completeSelect.append(lWhere);
		}
		if (lnWhere != null) {
			completeSelect.append(lnWhere);
		}
		if (mWhere != null) {
			completeSelect.append(mWhere);
		}
		if (oWhere != null) {
			completeSelect.append(oWhere);
		}
		if (rWhere != null) {
			completeSelect.append(rWhere);
	 	}
		if (gWhere != null) {
			completeSelect.append(gWhere);
		}
		if (sWhere != null) {
			completeSelect.append(sWhere);
		}
		if (keyWhere!=null) {
			completeSelect.append(keyWhere);
		}
		completeSelect.append(formSortOrder(userSearch));

		String formSql = completeSelect.toString();
		String countSql = "select count(*) from  ( " + completeSelect.toString() + ")";
		long totalPages = Configuration.PAGEPERBROWSER;

		BrowserRows br = new BrowserRows();
		br.getPageRows(curPage, rowsPerPage, formSql, totalPages, countSql, orderBy, orderType);
		
		long rowsReturned = br.getRowReturned();
		for (int iX = 1; iX <= rowsReturned; iX++) {
			pkUserList.add(br.getBValues(iX, COL_PK_USER));
			firstNameList.add(br.getBValues(iX, COL_FIRSTNAME));
			lastNameList.add(br.getBValues(iX, COL_LASTNAME));
			middleNameList.add(br.getBValues(iX, COL_MIDDLENAME));
			loginNameList.add(br.getBValues(iX, COL_LOGNAME));
			orgNameList.add(br.getBValues(iX, COL_SITE_NAME));
			usrStatList.add(br.getBValues(iX, COL_USR_STAT));
			usrTypeList.add(br.getBValues(iX, COL_USR_TYPE));
			addressList.add(br.getBValues(iX, COL_ADDRESS));
			addCityList.add(br.getBValues(iX, COL_ADD_CITY));
			addStateList.add(br.getBValues(iX, COL_ADD_STATE));
			addZipList.add(br.getBValues(iX, COL_ADD_ZIPCODE));
			addEmailList.add(br.getBValues(iX, COL_ADD_EMAIL));
			addPhoneList.add(br.getBValues(iX, COL_ADD_PHONE));
			fkDefaultGroupList.add(br.getBValues(iX, COL_FK_GRP_DEFAULT));
			defaultGroupList.add(br.getBValues(iX, COL_GRP_NAME));
			/*codelst_pk_key.add(br.getBValues(iX, COL_MD_MODELEMENTPK));
			more_value.add(br.getBValues(iX, COL_MD_MODELEMENTDATA));
			codelst_typ.add(br.getBValues(iX, COL_CODELST_TYPE));
			more_description.add(br.getBValues(iX, COL_CODELST_DESC));*/
			}
		totalCount = br.getTotalRows();
		pageSize = rowsReturned;
	}
	
	public Map<String,String> searchUserWS(UserSearch userSearch, HashMap<String, String> params) {
		int iaccId = StringUtil.stringToNum(params.get(KEY_ACCOUNT_ID));
		Map<String,String> moreDetailsMap = new HashMap<String,String>();
		int userPk = 0;
		if (userSearch.getUserPK() >0) {
			userPk = userSearch.getUserPK();
		}
		String orgName = null;
		String orgAltId = null;
		Integer orgPk = null;
		if (userSearch.getOrganization() != null) {
			orgPk = userSearch.getOrganization().getPK();
			if (orgPk == null || orgPk < 1) {
				orgName = userSearch.getOrganization().getSiteName();
				if (StringUtil.isEmpty(orgName)) {
					orgAltId = userSearch.getOrganization().getSiteAltId();
				}
			}
		}
		String grpPk = StringUtil.trueValue(params.get(KEY_USER_GROUP));
		String gName = null;
		if (userSearch.getGroup()!=null && userSearch.getGroup().getGroupName() != null) {
			gName = userSearch.getGroup().getGroupName();
		}
		String stTeam = StringUtil.trueValue(params.get(KEY_STUDY_TEAM));
		String ufname = userSearch.getFirstName();
		String ulname = userSearch.getLastName();
		String uloginname = userSearch.getLoginName();
		String uemailId = userSearch.getEmail();
		String keySearch = "";
		String keyValue = "";
		Collection<MoreDetailValue> moreUserDetails =userSearch.getMoreUserDetails();
		
		MoreDetailValue obj=new MoreDetailValue();
		
		UserSearch userSearch1= new UserSearch();
		if(moreUserDetails!=null){
			userSearch1.setMoreUserDetails((java.util.List<MoreDetailValue>) moreUserDetails);
			for(int i=0;i<userSearch1.getMoreUserDetails().size();i++){
				obj=	userSearch1.getMoreUserDetails().set(i, obj);
				if(keySearch.equals("")){
					keySearch	=obj.getKey()==null?"":obj.getKey();
					//Kavitha: #28969
					if(!keySearch.equals("")){
					  keySearch = "'"+obj.getKey()+"'";
					}
				}else{
					String tmpKey = obj.getKey()==null?"":obj.getKey();
					if(!tmpKey.equals("")){
						keySearch = keySearch+",'"+tmpKey+"'";
					}

				}
				if(obj.getValue()!=null){
				if(obj.getValue() instanceof ElementNSImpl){
				  ElementNSImpl value=(ElementNSImpl)obj.getValue();
				  
				  if(value.getChildNodes()!=null){
					 NodeList node=  	value.getChildNodes();

			          //String getvalue="";
			          for(int j=0;j< node.getLength();j++)
			           {
			           Node node1=node.item(j);
			           if(keyValue.equals("")){
			        	   if(node1.getNodeValue()!=null){
			        		   keyValue="'"+node1.getNodeValue()+"'";
			        	   }
			           }else{
			        	   if(node1.getNodeValue()!=null){
			        		   keyValue=keyValue+",'"+node1.getNodeValue()+"'";
			        	   }
			           }
			            }
			          
			          System.out.println("Inside If--->"+keyValue);
			          
				  }else{
					if(value.getNodeValue()!=null && !value.getNodeValue().equals("")){
						if(keyValue.equals("")){
				        	   if(value.getNodeValue()!=null){
				        		   keyValue="'"+value.getNodeValue()+"'";
				        	   }
				           }else{
				        	   if(value.getNodeValue()!=null){
				        		   keyValue=keyValue+",'"+value.getNodeValue()+"'";
				        	   }
				           }
					}
					  System.out.println("Inside Else--->"+keyValue);
				  }
				}
				else{
					if(keyValue.equals("")){
			        	keyValue="'"+String.valueOf(obj.getValue())+"'";
			        }else{
			        	 keyValue=keyValue+",'"+String.valueOf(obj.getValue())+"'";
			        }
					System.out.println("Inside Else1--->"+keyValue);
					
				}
				}
			}
			
			
		}
		
		String jobCodePk = params.get(KEY_JOB_CODE_PK);
		int curPage = userSearch.getPageNumber();
		if (curPage < 1) {
			curPage = 1;
		}
		pageNumber = curPage;
		long rowsPerPage = Configuration.MOREBROWSERROWS;
		String inputPageSize = params.get(KEY_PAGE_SIZE);
		if (!StringUtil.isEmpty(inputPageSize)) {
			try {
				rowsPerPage = Long.parseLong(inputPageSize);
			} catch(Exception e) {
				rowsPerPage = Configuration.MOREBROWSERROWS;
			}
		}
		if (rowsPerPage > 50L) {
			rowsPerPage = 50L;
		}
		String orderBy = FilterUtil.sanitizeTextForSQL(params.get(KEY_ORDER_BY));
		if (StringUtil.isEmpty(orderBy)) {
			orderBy = DEFAULT_ORDER_BY;
		}
		String orderType = FilterUtil.sanitizeTextForSQL(params.get(KEY_ORDER_TYPE));
		if (StringUtil.isEmpty(orderType)) {
			orderType = ASC_STR;
		}
		
		String uWhere = null;
		String lWhere = null;
		String lnWhere = null;
		String mWhere = null;
 	 	String fWhere = null;
		String oWhere = null;
		String rWhere = null;
		String aWhere = null;
		String gWhere = null;
		String sWhere = null;
		String keyWhere = null;
		String mdWhere = null;

 	 	StringBuffer completeSelect = new StringBuffer();
 	 	String searchFNameForSQL= StringUtil.escapeSpecialCharSQL(ufname);
 	 	String searchLNameForSQL= StringUtil.escapeSpecialCharSQL(ulname);
 	 	String searchLoginNameForSQL= StringUtil.escapeSpecialCharSQL(uloginname);
 	 	String searchUserEMailIDForSQL = StringUtil.escapeSpecialCharSQL(uemailId);
 	 	String searchkey1ForSQL=  keySearch;
 	 	String addMoreTables ="";
 	 	if(!keySearch.equals("")){
 	 		addMoreTables=", er_moredetails md,  ER_CODELST cd ";
 	 	}
 	 	String str1 = "SELECT distinct u.pk_user,u.USR_LASTNAME, "
 				  + " s.PK_SITE, s.site_name, "
 				  + " u.USR_FIRSTNAME, u.USR_MIDNAME, u.USR_LOGNAME, u.USR_STAT, u.USR_TYPE "
 				  + ", u.FK_GRP_DEFAULT, (select GRP_NAME from ER_GRPS where PK_GRP = u.FK_GRP_DEFAULT) GRP_NAME "
 				  + " , a.ADDRESS, a.ADD_CITY, a.ADD_STATE, a.ADD_ZIPCODE, a.ADD_EMAIL, a.ADD_PHONE "
 				  + " from  ER_USER u, ER_ADD a, ER_SITE s ";
 				  if(!addMoreTables.equals("")){
 					  str1 += addMoreTables;
 				  }
 				 str1 += " Where s.PK_SITE  = u.FK_SITEID and u.fk_peradd = a.pk_add and u.USR_STAT in('A','B','D') AND ";
 				 str1 += "(u.USR_TYPE <>'X' and u.USR_TYPE <> 'P' ) and u.USER_HIDDEN <> 1";
 	 	if (userPk > 0) {
 	 		uWhere = " and u.PK_USER = "+userPk;
 	 	}
 	 	if (orgPk != null && orgPk > 0) {
 	 		oWhere = " and s.PK_SITE = "+orgPk;
 	 	} else {
 	 		if ((orgName != null) && (!orgName.trim().equals(EMPTY_STR) && !orgName.trim().equals(NULL_STR))) {
 	 			oWhere  = " and UPPER(s.SITE_NAME) = UPPER('" ;
 	 			oWhere += FilterUtil.sanitizeTextForSQL(orgName);
 	 			oWhere += "')";
 	 		} else if (!StringUtil.isEmpty(orgAltId)) {
 	 			oWhere  = " and UPPER(s.SITE_ALTID) = UPPER('" ;
 	 			oWhere += FilterUtil.sanitizeTextForSQL(orgAltId);
 	 			oWhere += "')";
 	 		}
 	 	}
		if ((jobCodePk != null) && (!jobCodePk.trim().equals(EMPTY_STR) && !jobCodePk.trim().equals(NULL_STR))) {
			rWhere = " and UPPER(u.FK_CODELST_JOBTYPE) = UPPER(" ;
			rWhere+=FilterUtil.sanitizeNumberForSQL(jobCodePk);
			rWhere+=")";
		}
		if ((ufname != null) && (!ufname.trim().equals(EMPTY_STR) && !ufname.trim().equals(NULL_STR))) {
			fWhere = " and UPPER(u.USR_FIRSTNAME) like UPPER('" ;
			fWhere+=searchFNameForSQL.trim();
			fWhere+="%')";
		}
		if ((ulname != null) && (!ulname.trim().equals(EMPTY_STR)  && !ulname.trim().equals(NULL_STR))) {
			lWhere = " and UPPER(u.USR_LASTNAME) like UPPER('";
			lWhere+=searchLNameForSQL.trim();
			lWhere+="%')";
		}
		if ((uloginname != null) && (!uloginname.trim().equals(EMPTY_STR)  && !uloginname.trim().equals(NULL_STR))) {
			lnWhere = " and u.USR_LOGNAME = '";
			lnWhere+=searchLoginNameForSQL.trim()+"'";
		}
		if ((uemailId != null) && (!uemailId.trim().equals(EMPTY_STR)  && !uemailId.trim().equals(NULL_STR))) {
			mWhere = " and a.ADD_EMAIL = '";
			mWhere+=searchUserEMailIDForSQL.trim()+"'";
		}
		if ((grpPk != null) && (!grpPk.trim().equals(EMPTY_STR) && !grpPk.trim().equals(NULL_STR))) {
			gWhere=" and u.pk_user in(select fk_user from er_usrgrp where upper(fk_grp)=upper(";
			gWhere +=FilterUtil.sanitizeNumberForSQL(grpPk);
			gWhere +="))";
		} else if (!StringUtil.isEmpty(gName)) {
			gWhere=" and u.pk_user in (select fk_user from er_usrgrp where fk_grp in (select pk_grp from er_grps where fk_account="
					+iaccId+" and grp_name like '%";
			gWhere += FilterUtil.sanitizeTextForSQL(gName);
			gWhere +="%'))";
		}
		if ((stTeam != null) && (!stTeam.trim().equals(EMPTY_STR) && !stTeam.trim().equals(NULL_STR))) {
   			sWhere =" and ( exists (select * from er_studyteam t where t.fk_user = pk_user and t.fk_study =  ";
			sWhere +=FilterUtil.sanitizeNumberForSQL(stTeam);
			sWhere +=" ) or pkg_superuser.F_Is_Superuser(pk_user, "+FilterUtil.sanitizeNumberForSQL(stTeam)+") = 1  )";
		}
		
		if(!keySearch.equals("")){
			mdWhere=" AND md.fk_modpk        = u.PK_USER";
			mdWhere+=" AND cd.pk_codelst        = md.md_modelementpk";
			mdWhere+=" AND cd.codelst_type      = 'user'";
			mdWhere+=" AND cd.codelst_subtyp    in ("+FilterUtil.sanitizeTextForSQL(keySearch)+")";
			moreDetailsMap.put("key", FilterUtil.sanitizeTextForSQL(keySearch));
			if(!keyValue.equals("")){
				mdWhere+=" AND md.md_modelementdata in ("+FilterUtil.sanitizeTextForSQL(keyValue)+")";
				moreDetailsMap.put("value", FilterUtil.sanitizeTextForSQL(keyValue));
			}
		}

		completeSelect.append(str1);
		if (iaccId > 0) {
			aWhere = " and u.fk_account = " + iaccId;
			completeSelect.append(aWhere);
		}
		if (uWhere != null) {
			completeSelect.append(uWhere);
		}
		if (fWhere != null) {
			completeSelect.append(fWhere);
		}
		if (lWhere != null) {
			completeSelect.append(lWhere);
		}
		if (lnWhere != null) {
			completeSelect.append(lnWhere);
		}
		if (mWhere != null) {
			completeSelect.append(mWhere);
		}
		if (oWhere != null) {
			completeSelect.append(oWhere);
		}
		if (rWhere != null) {
			completeSelect.append(rWhere);
	 	}
		if (gWhere != null) {
			completeSelect.append(gWhere);
		}
		if (sWhere != null) {
			completeSelect.append(sWhere);
		}
		if (keyWhere!=null) {
			completeSelect.append(keyWhere);
		}
		if (mdWhere!=null) {
			completeSelect.append(mdWhere);
		}
		completeSelect.append(formSortOrder(userSearch));

		String formSql = completeSelect.toString();
		String countSql = "select count(*) from  ( " + completeSelect.toString() + ")";
		long totalPages = Configuration.PAGEPERBROWSER;

		BrowserRows br = new BrowserRows();
		br.getPageRows(curPage, rowsPerPage, formSql, totalPages, countSql, orderBy, orderType);
		System.out.println("formSql==="+formSql);
		long rowsReturned = br.getRowReturned();
		for (int iX = 1; iX <= rowsReturned; iX++) {
			pkUserList.add(br.getBValues(iX, COL_PK_USER));
			firstNameList.add(br.getBValues(iX, COL_FIRSTNAME));
			lastNameList.add(br.getBValues(iX, COL_LASTNAME));
			middleNameList.add(br.getBValues(iX, COL_MIDDLENAME));
			loginNameList.add(br.getBValues(iX, COL_LOGNAME));
			orgNameList.add(br.getBValues(iX, COL_SITE_NAME));
			usrStatList.add(br.getBValues(iX, COL_USR_STAT));
			usrTypeList.add(br.getBValues(iX, COL_USR_TYPE));
			addressList.add(br.getBValues(iX, COL_ADDRESS));
			addCityList.add(br.getBValues(iX, COL_ADD_CITY));
			addStateList.add(br.getBValues(iX, COL_ADD_STATE));
			addZipList.add(br.getBValues(iX, COL_ADD_ZIPCODE));
			addEmailList.add(br.getBValues(iX, COL_ADD_EMAIL));
			addPhoneList.add(br.getBValues(iX, COL_ADD_PHONE));
			fkDefaultGroupList.add(br.getBValues(iX, COL_FK_GRP_DEFAULT));
			defaultGroupList.add(br.getBValues(iX, COL_GRP_NAME));
			/*codelst_pk_key.add(br.getBValues(iX, COL_MD_MODELEMENTPK));
			more_value.add(br.getBValues(iX, COL_MD_MODELEMENTDATA));
			codelst_typ.add(br.getBValues(iX, COL_CODELST_TYPE));
			more_description.add(br.getBValues(iX, COL_CODELST_DESC));*/
			}
		totalCount = br.getTotalRows();
		pageSize = rowsReturned;
		return moreDetailsMap;
	}
	
	private String formSortOrder(UserSearch userSearch) {
		if (userSearch.getSortBy() == null) {
			return SPACE_STR;
		}
		StringBuffer sb1 = new StringBuffer(" order by ");
		if (userSearch.getSortBy() == UserSearch.UserSearchOrderBy.PK) {
			sb1.append("u.pk_user");
		} else if (userSearch.getSortBy() == UserSearch.UserSearchOrderBy.lastName) {
			sb1.append("UPPER(u.USR_LASTNAME)");
		} else if (userSearch.getSortBy() == UserSearch.UserSearchOrderBy.firstName) {
			sb1.append("UPPER(u.USR_FIRSTNAME)");
		} else if (userSearch.getSortBy() == UserSearch.UserSearchOrderBy.email) {
			sb1.append("UPPER(a.ADD_EMAIL)");
		} else if (userSearch.getSortBy() == UserSearch.UserSearchOrderBy.siteName) {
			sb1.append("UPPER(s.site_name)");
		} else if (userSearch.getSortBy() == UserSearch.UserSearchOrderBy.userLoginName) {
			sb1.append("UPPER(u.USR_LOGNAME)");
		} else if (userSearch.getSortBy() == UserSearch.UserSearchOrderBy.userStatus) {
			sb1.append("u.USR_STAT");
		}
		
		if (userSearch.getSortOrder() == SortOrder.ASCENDING) {
			sb1.append(" ASC NULLS LAST ");
		} else {
			sb1.append(" DESC NULLS FIRST ");
		}
		return sb1.toString();
	}
	
}
