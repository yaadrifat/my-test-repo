package com.velos.remoteservice;

/**
 * This exception is thrown when a caller of a remote service implementation
 * calls a method that is not currently supported.
 * @author dylan
 *
 */
public class RemoteMemberNotImplementedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5097664613775326575L;

	public RemoteMemberNotImplementedException() {
		super();
	}

	public RemoteMemberNotImplementedException(String message,
			Throwable cause) {
		super(message, cause);
		}

	public RemoteMemberNotImplementedException(String message) {
		super(message);
	}

	public RemoteMemberNotImplementedException(Throwable cause) {
		super(cause);
	}

}
