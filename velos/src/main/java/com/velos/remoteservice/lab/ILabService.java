package com.velos.remoteservice.lab;

import java.util.Date;
import java.util.List;

import com.velos.remoteservice.IRemoteService;
import com.velos.remoteservice.VelosServiceException;
/**
 * Interface for lab services
 * @author dylan
 *
 */
public interface ILabService extends IRemoteService{

	public String test() throws VelosServiceException;
	
	public List<ILabTest> getPatientTests(
			String MRN, 
			Date startDate,
			Date endDate, 
			List<String> labCodes)
	throws 
		VelosServiceException, IllegalStateException;
}
