/**
 * 
 */
package com.velos.remoteservice;

/**
 * @author dylan
 *
 */
public interface RemoteField {
	
	public boolean isSource();
	
	public String getName();
	
	public String getValue();
	
	public String toString();
}
