package com.velos.login.authenticator;
//Warning: This code has been marked up for HTML

/*******************************************************************************
 * $Novell: GetAuthenticated.java,v 1.13 2002/07/29 21:17:42 $
 * Copyright (c) 2000 Novell, Inc. All Rights Reserved.
 *
 * THIS WORK IS SUBJECT TO U.S. AND INTERNATIONAL COPYRIGHT LAWS AND
 * TREATIES. USE AND REDISTRIBUTION OF THIS WORK IS SUBJECT TO THE LICENSE
 * AGREEMENT ACCOMPANYING THE SOFTWARE DEVELOPMENT KIT (SDK) THAT CONTAINS
 * THIS WORK. PURSUANT TO THE SDK LICENSE AGREEMENT, NOVELL HEREBY GRANTS TO
 * DEVELOPER A ROYALTY-FREE, NON-EXCLUSIVE LICENSE TO INCLUDE NOVELL'S SAMPLE
 * CODE IN ITS PRODUCT. NOVELL GRANTS DEVELOPER WORLDWIDE DISTRIBUTION RIGHTS
 * TO MARKET, DISTRIBUTE, OR SELL NOVELL'S SAMPLE CODE AS A COMPONENT OF
 * DEVELOPER'S PRODUCTS. NOVELL SHALL HAVE NO OBLIGATIONS TO DEVELOPER OR
 * DEVELOPER'S CUSTOMERS WITH RESPECT TO THIS CODE.
 *
 * $name:         GetAuthenticated.java 
 * $description:  GetAuthenticated shows different kinds of bind.
 *                   -- anonymous bind 
 *                   -- simple bind
 *                   -- simple bind with connection method
 *                   -- SSL bind
 ******************************************************************************/
import java.io.UnsupportedEncodingException;
import java.security.Security;

import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPJSSESecureSocketFactory;
import com.velos.eres.service.util.Rlog;

public class LDAPAuthenticator {


	private static void anonymousBind(LDAPConnection conn, String host, int port) {
		try {
		
			// connect to the server
			conn.connect(host, port);
			// disconnect with the server
			conn.disconnect();
		} catch (LDAPException e) {
			Rlog.fatal("ldap", " error in LDAPAuthenticator.anonymousBind"+e.getMessage());
			e.printStackTrace();
		}
		return;
	}

	public static boolean Bind(LDAPConnection conn, String host, int port,
			String dn, String passwd) {
		try {
			
			// connect to the server
			conn.connect(host, port);
			// authenticate to the server
			try {
				conn.bind(LDAPConnection.LDAP_V3, dn, passwd.getBytes("UTF8"));
			} catch (UnsupportedEncodingException u) {
				throw new LDAPException("UTF8 Invalid Encoding",
						LDAPException.LOCAL_ERROR, (String) null, u);
			}

			// disconnect with the server
			conn.disconnect();
			return true;
		} catch (LDAPException e) {
			Rlog.fatal("ldap", " error in LDAPAuthenticator.anonymousBind"+e.getMessage());
			e.printStackTrace();

		}
		return false;
	}

	public static boolean Bind(String host, int port,
			String dn, String passwd) {
		try {
			LDAPConnection conn=new LDAPConnection();
			
			// connect to the server
			conn.connect(host, port);
			// authenticate to the server
			try {
				conn.bind(LDAPConnection.LDAP_V3, dn, passwd.getBytes("UTF8"));
			} catch (UnsupportedEncodingException u) {
				throw new LDAPException("UTF8 Invalid Encoding",
						LDAPException.LOCAL_ERROR, (String) null, u);
			}

			
			// disconnect with the server
			conn.disconnect();
			return true;
		} catch (LDAPException e) {
			Rlog.fatal("ldap", " error in LDAPAuthenticator.anonymousBind"+e.getMessage());
			e.printStackTrace();

		}
		return false;
	}

	private static void simpleBind2(int version, LDAPConnection conn,
			String host, int port, String dn, String passwd) {
		try {
			System.out.println("Simple bind with connection method...");
			// connect to the server
			conn.connect(host, port);
			// authenticate to the server with the connection method
			try {
				conn.bind(version, dn, passwd.getBytes("UTF8"));
			} catch (UnsupportedEncodingException u) {
				throw new LDAPException("UTF8 Invalid Encoding",
						LDAPException.LOCAL_ERROR, (String) null, u);
			}

			System.out
					.println((conn.isBound()) ? "\n\tAuthenticated to the server ( simple )\n"
							: "\n\tNot authenticated to the server\n");

			// disconnect with the server
			conn.disconnect();
		} catch (LDAPException e) {
			System.out.println("Error: " + e.toString());
		}
		return;
	}

	public static boolean SSLBind(String host, int SSLPort,
			String dn, String passwd) {
		
		Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());	
		// Set the socket factory for this connection only
		LDAPJSSESecureSocketFactory ssf = new LDAPJSSESecureSocketFactory();
        LDAPConnection.setSocketFactory(ssf);
		LDAPConnection conn = new LDAPConnection();

		try {
			System.out.println("SSL bind...");
			// connect to the server
			conn.connect(host, SSLPort);
			// authenticate to the server with the connection method
			try {
				conn.bind(LDAPConnection.LDAP_V3, dn, passwd.getBytes("UTF8"));
			} catch (UnsupportedEncodingException u) {
				throw new LDAPException("UTF8 Invalid Encoding",
						LDAPException.LOCAL_ERROR, (String) null, u);
			}

			System.out
					.println((conn.isBound()) ? "\n\tAuthenticated to the server ( ssl )\n"
							: "\n\tNot authenticated to the server\n");

			// disconnect with the server
			conn.disconnect();
			
		} catch (LDAPException e) {
			System.out.println("Error: " + e.toString());
			e.printStackTrace();
			return false;
		}
		return true;
	}
	private static void SSLBind(int version, String host, int SSLPort,
			String dn, String passwd) {

		Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());	
		// Set the socket factory for this connection only
		LDAPJSSESecureSocketFactory ssf = new LDAPJSSESecureSocketFactory();
        LDAPConnection.setSocketFactory(ssf);
		LDAPConnection conn = new LDAPConnection();
		
		try {
			System.out.println("SSL bind...");
			// connect to the server
			conn.connect(host, SSLPort);
			// authenticate to the server with the connection method
			try {
				conn.bind(version, dn, passwd.getBytes("UTF8"));
			} catch (UnsupportedEncodingException u) {
				throw new LDAPException("UTF8 Invalid Encoding",
						LDAPException.LOCAL_ERROR, (String) null, u);
			}

			System.out
					.println((conn.isBound()) ? "\n\tAuthenticated to the server ( ssl )\n"
							: "\n\tNot authenticated to the server\n");

			// disconnect with the server
			conn.disconnect();
		} catch (LDAPException e) {
			System.out.println("Error: " + e.toString());
		}
		return;
	}
}
