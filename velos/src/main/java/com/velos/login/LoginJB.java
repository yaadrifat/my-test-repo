package com.velos.login;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.velos.eres.business.common.CtrlDao;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.EresSessionBinding;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.account.AccountJB;
import com.velos.eres.web.grpRights.GrpRightsJB;
import com.velos.eres.web.user.UserJB;
import com.velos.esch.business.common.SchCodeDao;

public class LoginJB {
	HttpServletRequest request = null;
	HttpSession session = null;
	UserJB userJB = null;
	
	@SuppressWarnings("unused")
	private LoginJB() {}
	public LoginJB(HttpServletRequest request, UserJB userJB) {
		this.request = request;
		this.session = request.getSession(true);
		this.userJB = userJB;
	}
	
	public void fillSessionDataAfterLoginConfirmation() {
		if (session == null || userJB == null) { return; }
		session.setAttribute("currentUser", userJB);
		session.setAttribute("loginname", userJB.getUserLoginName());
		AccountJB accountJB = new AccountJB();
		accountJB.setAccId(StringUtil.stringToNum(userJB.getUserAccountId()));
		accountJB.getAccountDetails();
		session.setAttribute("totalUsersAllowed", accountJB.getAccMaxUsr());
		session.setAttribute("totalPortalsAllowed", accountJB.getAccMaxPortal());
		session.setAttribute("accMaxStorage", accountJB.getAccMaxStorage());
		String accName = accountJB.getAccName();
		if (accName == null) { accName = "default"; }
		session.setAttribute("accName", accName );
		String accSkin  = accountJB.getAccSkin();
		session.setAttribute("accSkin", accSkin);
		String uName = userJB.getUserFirstName() + " " + userJB.getUserLastName();
		session.setAttribute("userName", uName );
		String modRight = accountJB.getAccModRight();
		session.setAttribute("modRight", modRight);
		char protocolManagementRight = '0';
		int protocolMgmtSeq = 0, protocolMgmtModSeq = 0;
		CtrlDao modCtlDao = new CtrlDao();
		modCtlDao.getControlValues("module");
		ArrayList modCtlDaofeature =  modCtlDao.getCValue();
		ArrayList modCtlDaoftrSeq = modCtlDao.getCSeq();
		protocolMgmtModSeq = modCtlDaofeature.indexOf("MODPROTOCOL");
		protocolMgmtSeq = ((Integer) modCtlDaoftrSeq.get(protocolMgmtModSeq)).intValue();
		protocolManagementRight = modRight.charAt(protocolMgmtSeq - 1);
		session.setAttribute("protocolManagementRight", String.valueOf(protocolManagementRight) );
		String eSign = userJB.getUserESign();
		String password=userJB.getUserPwd();
		String sessionTime = userJB.getUserSessionTime() ;
		/**Password as eSign implementation code***/
		if("userpxd".equals(Configuration.eSignConf))
			session.setAttribute("eSign", (String)request.getAttribute("pass") );
		else
		session.setAttribute("eSign", eSign );
		session.setAttribute("pwd", password );
		/**Password as eSign implementation code***/
		session.setAttribute("ipAdd",request.getRemoteAddr());
		session.setAttribute("userId", String.valueOf(userJB.getUserId()));
		session.setAttribute("accountId", userJB.getUserAccountId());
		session.setAttribute("userSession", sessionTime);
		session.setAttribute("studyId","");
	   	session.setAttribute("studyNo","");
		session.setAttribute("userAddressId", userJB.getUserPerAddressId());
		session.setAttribute("sessionName","er");
		session.setAttribute("defUserGroup",userJB.getUserGrpDefault());
		SchCodeDao schCode = new SchCodeDao();
		String tzDesc = schCode.getDefaultTimeZone();
		if (StringUtil.isEmpty(tzDesc)) {
			tzDesc = "";
		}
		session.setAttribute("defTZDesc",tzDesc);
		EresSessionBinding eresSession = new EresSessionBinding();
		session.setAttribute("eresSessionBinder",eresSession);
		int userSession = Integer.parseInt(sessionTime) * 60;
		session.setMaxInactiveInterval(userSession);
		GrpRightsJB grpRights = new GrpRightsJB();
		grpRights.setId(Integer.parseInt(userJB.getUserGrpDefault()));
		grpRights.getGrpRightsDetails();
		session.setAttribute("GRights", grpRights);
		session.setAttribute("defGroupPK",userJB.getUserGrpDefault());
	}
}
