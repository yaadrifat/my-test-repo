/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.velos.login.jwt;

/**
 *
 * @author Ajit Parmar
 */
import java.security.InvalidKeyException;
import java.security.SignatureException;
import java.util.Calendar;
import java.util.List;

import net.oauth.jsontoken.JsonToken;
import net.oauth.jsontoken.JsonTokenParser;
import net.oauth.jsontoken.crypto.HmacSHA256Signer;
import net.oauth.jsontoken.crypto.HmacSHA256Verifier;
import net.oauth.jsontoken.crypto.SignatureAlgorithm;
import net.oauth.jsontoken.crypto.Verifier;
import net.oauth.jsontoken.discovery.VerifierProvider;
import net.oauth.jsontoken.discovery.VerifierProviders;

//import org.apache.commons.lang3.StringUtils;
//import org.bson.types.ObjectId;
//import org.joda.time.DateTime;

import com.google.common.collect.Lists;
import com.google.gson.JsonObject;


/**
 * Provides static methods for creating and verifying access tokens and such. 
 *
 */
public class AuthHelper {

    private static final String AUDIENCE = "NotReallyImportant";

    private static final String ISSUER = "VeloseResearch";

    private static final String SIGNING_KEY = "LongAndHardToGuessValueWithSpecialCharacters@^($%*$%";

    /**
     * Creates a json web token which is a digitally signed token that contains a payload (e.g. userId to identify 
     * the user). The signing key is secret. That ensures that the token is authentic and has not been modified.
     * Using a jwt eliminates the need to store authentication session information in a database.
     * @param userId
     * @param password
     * @param durationDays
     * @return
     */
    public static String createJsonWebToken(String userId, String password, Long durationDays)    {
        
        if (userId == null)
            userId = "";
        
        if (password == null)
            password = "";
        
        //Current time and signing algorithm
        Calendar cal = Calendar.getInstance();
        HmacSHA256Signer signer;
        try {
            signer = new HmacSHA256Signer(ISSUER, null, SIGNING_KEY.getBytes());
        } catch (InvalidKeyException e) {
            throw new RuntimeException(e);
        }

        //Configure JSON token
        JsonToken token = new net.oauth.jsontoken.JsonToken(signer);
        token.setAudience(AUDIENCE);
        token.setIssuedAt(new org.joda.time.Instant(cal.getTimeInMillis()));
        token.setExpiration(new org.joda.time.Instant(cal.getTimeInMillis() + 1000L * 60L * 60L * 24L * durationDays));

        //Configure request object, which provides information of the item
        JsonObject request = new JsonObject();
        request.addProperty("userId", userId);
        request.addProperty("password", password);

        
      //  System.out.println("request - " + request);
        JsonObject payload = token.getPayloadAsJsonObject();
     //   System.out.println("payload - " + payload);
        payload.add("info", request);

        try {
            return token.serializeAndSign();
        } catch (SignatureException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Verifies a json web token's validity and extracts the user id and other information from it. 
     * @param token
     * @return
     * @throws SignatureException
     * @throws InvalidKeyException
     */
    public static TokenInfo verifyToken(String token)  
    {
        try {
            final Verifier hmacVerifier = new HmacSHA256Verifier(SIGNING_KEY.getBytes());

            VerifierProvider hmacLocator = new VerifierProvider() {

                @Override
                public List<Verifier> findVerifier(String id, String key){
                    return Lists.newArrayList(hmacVerifier);
                }
            };
            VerifierProviders locators = new VerifierProviders();
            locators.setVerifierProvider(SignatureAlgorithm.HS256, hmacLocator);
            net.oauth.jsontoken.Checker checker = new net.oauth.jsontoken.Checker(){

                @Override
                public void check(JsonObject payload) throws SignatureException {
                    // don't throw - allow anything
                }

            };
            //Ignore Audience does not mean that the Signature is ignored
            JsonTokenParser parser = new JsonTokenParser(locators,
                    checker);
            JsonToken jt;
            try {
                jt = parser.verifyAndDeserialize(token);
            } catch (SignatureException e) {
                throw new RuntimeException(e);
            }
            JsonObject payload = jt.getPayloadAsJsonObject();
            TokenInfo t = new TokenInfo();
            String issuer = payload.getAsJsonPrimitive("iss").getAsString();
            String userIdString =  payload.getAsJsonObject("info").getAsJsonPrimitive("userId").getAsString();
            String passwordString =  payload.getAsJsonObject("info").getAsJsonPrimitive("password").getAsString();
          //  if (issuer.equals(ISSUER) && !StringUtils.isBlank(userIdString))
          if (issuer.equals(ISSUER) && !userIdString.isEmpty())
            {
//                System.out.println("userID - " + userIdString);
//                System.out.println("password - " + passwordString);
//                System.out.println("Token Issed at - " + new DateTime(payload.getAsJsonPrimitive("iat").getAsLong()));
//                System.out.println("Token expiring at - " + new DateTime(payload.getAsJsonPrimitive("exp").getAsLong()));
                t.setUserId(userIdString);
                t.setPassword(passwordString);
                t.setIssued(payload.getAsJsonPrimitive("iat").getAsLong());
                t.setExpires(payload.getAsJsonPrimitive("exp").getAsLong());
                
                 System.out.println("userID - " + t.getUserId());
                System.out.println("password - " + t.getPassword());
                System.out.println("Token Issed at - " + t.getIssued());
                System.out.println("Token expiring at - " + t.getExpires());
                return t;
            }
            else
            {
                System.out.println(" Error : Username or Password not provided");
                return null;
            }
        } catch (InvalidKeyException e1) {
            throw new RuntimeException(e1);
        }
    }
    
    public static void main(String[] args) {
        
      String token =  AuthHelper.createJsonWebToken("velosadmin", "velos123", Long.parseLong("2"));
        
        System.out.println("TOKEN : " + token);
        
        AuthHelper.verifyToken(token);
        
        }


}

