
/**
 * @author Yogesh Kumar
 * Utility file for Tracking Deleted by Through Application.
 */
package com.aithent.audittrail.reports;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.servlet.http.HttpSession;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.esch.service.util.Rlog;


public final class AuditUtils extends CommonDAO implements java.io.Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -5023831979795847535L;
	
	public static final String USER_ID_KEY = "USER_ID_KEY";
	public static final String IP_ADD_KEY = "IP_ADD_KEY";
	
	public static final String ROW_PK_KEY = "ROW_PK_KEY";
	public static final String ROW_RID_KEY = "ROW_RID_KEY";
	
	public static final String REASON_FOR_DEL = "REASON_FOR_DEL";
	public static final String APP_MODULE = "APP_MODULE";

	/**
	 * getRowValues
	 * Gets the PK value and RID value from the DB for a particular row based on the condition
	 * @param table name, column id (index value), condition
	 * @return Arraylist of pk value and rid
	 */
	public static Hashtable<String, ArrayList<String>> getRowValues(String tableName, String condition, String schema){
    	ArrayList<String> rowPK = new ArrayList<String>();
    	ArrayList<String> rowRID = new ArrayList<String>();
    	Hashtable<String, ArrayList<String>> resultHT = new Hashtable<String, ArrayList<String>>();
    	Connection conn = null;
    	Statement colNameStmt=null; 
    	PreparedStatement rowValuesPstmt=null; 
     	String sql_ColName = "";
    	String sql_Query = "";
    	String col_Name="";
    	conn = getConnection(schema);
    	tableName = tableName.toUpperCase();
       	try {
       		
       		sql_ColName="SELECT COLUMN_NAME FROM USER_TAB_COLUMNS WHERE TABLE_NAME=\'"+tableName+"\' AND COLUMN_ID = 1 ";
			colNameStmt = conn.createStatement();
//			colNameStmt.setString(1, tableName);
			ResultSet rs = colNameStmt.executeQuery(sql_ColName);
			 while (rs.next()) {
				 col_Name=rs.getString("COLUMN_NAME");
			 }
			sql_Query="SELECT "+col_Name+" AS PK_VALUE, RID FROM "+tableName+" WHERE " +condition;
			rowValuesPstmt = conn.prepareStatement(sql_Query);
			ResultSet rs_val = rowValuesPstmt.executeQuery();
			
			while (rs_val.next()) {
				 rowPK.add(rs_val.getString("PK_VALUE"));
				 rowRID.add(rs_val.getString("RID"));
			 }
			 resultHT.put(ROW_PK_KEY, rowPK);
			 resultHT.put(ROW_RID_KEY, rowRID);
		 return resultHT;
		 
		} catch (SQLException e) {
			Rlog.fatal("Audit","Exception Caught in getting values in AuditUtils-->> getRowValues" + e);
			return null;
		} finally 
		{
			try {
				rowValuesPstmt.close();
				colNameStmt.close();
			} catch (SQLException e) {
				Rlog.fatal("Audit","Exception Caught in Closing PreparedStatement in AuditUtils-->> getRowValues" + e);
			}
			try {
				conn.close();
			} catch (SQLException e) {
				Rlog.fatal("Audit","Exception Caught in Closing Connection in AuditUtils-->> getRowValues" + e);
			}
			
		}
			
    }
	
	
	/**
	 * getRid
	 * Gets the RID value from the DB for a particular row based on the condition
	 * @param table name,  condition
	 * @return rid
	 */
	public static String getRidValue(String tableName, String dbSchema, String condition){
		Connection conn = null;
		PreparedStatement pstmt=null; 
		String sql_Query = "";
		String rID="";
		conn = getConnection(dbSchema);
		try {
			sql_Query="SELECT RID FROM "+tableName+" WHERE " +condition;
			pstmt = conn.prepareStatement(sql_Query);
			ResultSet rs_val = pstmt.executeQuery();
			while (rs_val.next()) {
				rID=rs_val.getString("RID");
			}
			
			return rID;	
		} catch (SQLException e) {
			Rlog.fatal("Audit","Exception Caught in getting values in AuditUtils-->> getRidValues" + e);
			return null;
		} finally 
		{
			try {
				pstmt.close();
			} catch (SQLException e) {
				Rlog.fatal("Audit","Exception Caught in AuditUtils-->> getRidValues" + e);
			}
			try {
				conn.close();
			} catch (SQLException e) {
				Rlog.fatal("Audit","Exception Caught in Closing Connection in AuditUtils-->> getRidValues" + e);
			}
			
		}
		
	}

	/**
	 * insertAuditforDeletedRow
	 * Inserts in to AUDIT_DELETELOG with the values of the rows being deleted.
	 * @param tableName, tablePk, rid, deletedBy, appModule, ipAdd
	 * @return Connection Object for roll back or commit.
	 */

	public static Connection insertAuditRow(String tableName,Hashtable<String, ArrayList<String>> rowValues , String appModule, Hashtable<String, String> args, String schema ) throws SQLException, Exception{
		PreparedStatement insertAudit = null;
		Connection conn = null;
		conn = getConnection(schema);
		ArrayList<String> rowPK  = (ArrayList<String>)rowValues.get(ROW_PK_KEY);
		ArrayList<String> rowRID  = (ArrayList<String>)rowValues.get(ROW_RID_KEY);
		
		String ipAdd = (String)args.get(IP_ADD_KEY);
		int usr = EJBUtil.stringToNum((String)args.get(USER_ID_KEY));
		String reason =  args.get(REASON_FOR_DEL);
		try {
			conn.setAutoCommit(false);    
			String insertSql="INSERT INTO ERES.AUDIT_DELETELOG(PK_APP_DL,TABLE_NAME,TABLE_PK,TABLE_RID,DELETED_BY,DELETED_ON,APP_MODULE,IP_ADDRESS,REASON_FOR_DELETION) " +
					" VALUES(ERES.SEQ_AUDIT_DELETELOG.nextval,?,?,?,?,sysdate,?,?,?)";
			insertAudit = conn.prepareStatement(insertSql);
			for(int i=0; i<rowPK.size(); i++)
			{
				insertAudit.setString(1, tableName.toUpperCase());
				insertAudit.setLong(2, Long.parseLong(rowPK.get(i)));
				insertAudit.setLong(3, Long.parseLong(rowRID.get(i)));
				insertAudit.setInt(4, usr);
				insertAudit.setString(5, appModule);
				insertAudit.setString(6, ipAdd);
				insertAudit.setString(7, reason);
				insertAudit.execute();
			}	
			
		} catch (SQLException e) {
			Rlog.fatal("Audit","Exception Caught in AuditUtils-->> insertAuditRow" + e);
			
		}	catch (Exception e) {  
			Rlog.fatal("Audit","Exception Caught in AuditUtils-->> insertAuditRow" + e);
				
		}  
		return conn;
	}
	/**
	 * getRowValues
	 * Commit or Rollback according to given Status in parameter  
	 * @param conn , status
	 * @return Arraylist of pk value and rid
	 */
	public static int commitOrRollback(Connection conn,boolean status) throws SQLException, Exception{
				 
		try {
			if(status)
			{
				conn.commit();
				return 0;
			}else{
			conn.rollback();
			return 0;
			}
		} catch (SQLException e) {
			conn.close();
			Rlog.fatal("Audit","SQLException Caught in AuditUtils-->> commitOrRollback" + e);
			return -1;
			
		}	catch (Exception e) {  
			conn.close();
			Rlog.fatal("Audit","Exception Caught in AuditUtils-->> commitOrRollback" + e);
			return -1;
				
		}  
		
	}
	
	/**
     * Enhancement: INF-18183
     * Purpose    : Prepare hashtable of UserID,Ip-address and reason from session
     * @param 	  :	session, reason
     * @return    : hashtable;
     */
	public static Hashtable<String, String>  createArgs(HttpSession tSession, String reason){
			
		Hashtable<String, String> argsHT = new Hashtable<String, String>();
		try {
			String userId = (String) tSession.getAttribute("userId");
			String ipAdd  = (String) tSession.getAttribute("ipAdd");
			argsHT.put(USER_ID_KEY, userId);
			argsHT.put(IP_ADD_KEY, ipAdd);
			argsHT.put(REASON_FOR_DEL,reason);
			return argsHT;	

		} catch (Exception e) {  
			Rlog.fatal("Audit","Exception Caught in AuditUtils-->> createArgs" + e);
			return argsHT;	
		}  
		
	}
	
	
	public static Hashtable<String, String>  createArgs(HttpSession tSession, String reason, String appModule){
		
		Hashtable<String, String> argsHT = new Hashtable<String, String>();
		try {
			String userId = (String) tSession.getAttribute("userId");
			String ipAdd  = (String) tSession.getAttribute("ipAdd");
			argsHT.put(USER_ID_KEY, userId);
			argsHT.put(IP_ADD_KEY, ipAdd);
			argsHT.put(REASON_FOR_DEL,reason);
			argsHT.put(APP_MODULE,appModule);
			return argsHT;	

		} catch (Exception e) {  
			Rlog.fatal("Audit","Exception Caught in AuditUtils-->> createArgs" + e);
			return argsHT;	
		}  
		
	}
		
public static int updateAuditROw(String schema,String userName,String rid,String action){
	PreparedStatement updateAudit = null;
	Connection conn = null;
	int updateFlag=0;
	conn = getConnection(schema);
	try {
		String updatesql="update AUDIT_ROW set user_name=? where  rid=? and action=?";
		updateAudit=conn.prepareStatement(updatesql);
		updateAudit.setString(1, userName);
		updateAudit.setString(2,rid);
		updateAudit.setString(3,action);
		updateFlag=updateAudit.executeUpdate();
	}catch(Exception e){
		Rlog.fatal("Audit","Exception Caught in updateUserNameInAuditROw-->>" + e);
	}finally {
		try {
			updateAudit.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return updateFlag;
}
public static Connection updateAuditROw(String schema,String userName,ArrayList rid,String action){
	PreparedStatement updateAudit = null;
	Connection conn = null;
	int updateFlag=0;
	
	try {
		conn = getConnection(schema);
		conn.setAutoCommit(false);    
		String updatesql="update AUDIT_ROW set user_name=? where  rid=? and action=?";
		updateAudit=conn.prepareStatement(updatesql);
		for(int i=0; i<rid.size(); i++){
		updateAudit.setString(1, userName);
		updateAudit.setInt(2,(Integer)rid.get(i));
		updateAudit.setString(3,action);
		updateFlag=updateAudit.executeUpdate();
		}
	}catch(Exception e){
		e.printStackTrace();
		Rlog.fatal("Audit","Exception Caught in updateUserNameInAuditROw-->>" + e);
	}
	return conn;
}

@SuppressWarnings("resource")
public void deleteRaidList(String schema, int rid, String userId, String action){    //this method will delete those raids from audit_row that do not have any reference in audit_column (for CLOB fields) 
	PreparedStatement updateAudit = null;
	Connection conn = null;
	int updateFlag=0;
	conn = getConnection(schema);
	try {
		String sql="delete FROM AUDIT_ROW ar WHERE ar.rid =? AND ar.action=? AND ar.user_name LIKE ? AND NOT EXISTS (SELECT raid FROM audit_column WHERE raid IN ar.RAID)";
		updateAudit = conn.prepareStatement(sql);
		updateAudit.setInt(1,rid);
		updateAudit.setString(2,action);
		updateAudit.setString(3, userId+",%");
		updateFlag = updateAudit.executeUpdate();
		}catch(Exception e){
		Rlog.fatal("Audit","Exception Caught in deleteRaidList-->>" + e);
	}finally {
		try {
			updateAudit.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
}
    // END OF CLASS

}
