/**
 * Extends HttpServlet class, used to upload a file to the database. The file can be uploaded only to a Blob type column.
 *
 * @author Sajal
 *
 * @version 1.0
 *
 */

package com.aithent.file.uploadDownload;

// import statements
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.user.UserJB;
import com.velos.esch.web.eventdoc.EventdocJB;

/**
 * Extends HttpServlet class, used to upload a file to the database. The file
 * can be uploaded only to a Blob type column.
 * 
 */
public class EventApndx extends FileUploadServlet {

    public void init(ServletConfig sc) throws ServletException {

    }

    /**
     * Overrides the doPost method of HttpServlet. Extracts the parameters sent
     * by the servlet and uploads the specified file with the specified name
     * from the temporary path specified in the XML file to the table specified.
     * 
     * @param httpservletrequest
     *            an HttpServletRequest object that contains the request the
     *            client has made of the servlet
     * 
     * @param httpservletresponse
     *            an HttpServletResponse object that contains the response the
     *            servlet sends to the client
     * 
     */
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if (session == null ||
        		(StringUtil.stringToNum((String)session.getAttribute(USER_ID)) < 1 &&
                		session.getAttribute(PP_IGNORE_ALL_RIGHTS) == null)) {
        	showForbiddenMessage(res.getOutputStream());
        	Rlog.fatal("fileUpload", "Forbidden_error: invalid_session");
        	return;
        }

        if (!req.getContentType().toLowerCase().startsWith(
                "multipart/form-data")) {
            // Since this servlet only handles File Upload, bail out
            // Note: this isn't strictly legal - I should send
            // a custom error message
            return;
        }
        int ind = req.getContentType().indexOf("boundary=");
        if (ind == -1) {
            return;
        }
        String boundary = req.getContentType().substring(ind + 9);
        if (boundary == null) {
            return;
        }
        try {
            table = parseMulti(boundary, req.getInputStream(), res.getOutputStream());
        } catch (Throwable t) {
            t.printStackTrace();
            return;
        }

        String[] eSign = new String[1];
        String[] userId = new String[1];
        String[] nextPage = new String[1];
        String[] ipAdd = new String[1];
        String[] eventId = new String[1];
        String[] networkFlag = new String[1];
        String[] networkId = new String[1];
        String[] siteId = new String[1];
        String [] userPk = new String[1]; 
        //for event propagation
        
        String[] p_protocol_id = new String[1];
        String[] propagateInVisitFlag = new String[1];
        String[] propagateInEventFlag = new String[1]; 
        String[] propagateMode = new String[1];
        String[] propagateCalType  = new String[1];
        

        eSign = (String[]) table.get("eSign");
        userId = (String[]) table.get("userId");
        nextPage = (String[]) table.get("nextPage");
        ipAdd = (String[]) table.get("ipAdd");
        networkFlag = (String[]) table.get("networkFlag");
        networkId =(String[]) table.get("networkId");
        siteId =(String[]) table.get("siteId");
        userPk =(String[]) table.get("userPk");
        eventId = (String[]) table.get("eventId");       
        p_protocol_id = (String[]) table.get("protocolId");
        propagateInVisitFlag = (String[]) table.get("propagateInVisitFlag");
        propagateInEventFlag = (String[]) table.get("propagateInEventFlag");
        propagateMode = (String[]) table.get("propagateMode");
        propagateCalType = (String[]) table.get("calassoc");
        
        System.out.println("in side do post of EventApnd: p_protocol_id[0]" + p_protocol_id[0]);
        System.out.println("in side do post of EventApnd: propagateInVisitFlag[0]" + propagateInVisitFlag[0]);
        System.out.println("in side do post of EventApnd: propagateInEventFlag[0]" + propagateInEventFlag[0]);
        System.out.println("in side do post of EventApnd: propagateMode[0]" + propagateMode[0]);
        System.out.println("in side do post of EventApnd: propagateCalType[0]" + propagateCalType[0]);
        System.out.println("in side do post of EventApnd: eventId[0]" + eventId[0]);
        
        UserJB userB = new UserJB();
        userB.setUserId(EJBUtil.stringToNum(userId[0]));
        userB.getUserDetails();
        String actualESign = (String) session.getValue("eSign");//userB.getUserESign();
        out = res.getOutputStream();

        if (eSign[0].equals(actualESign)) {

            System.out.println("EventApndx.doPost line 1");
            EventdocJB eventdocJB = new EventdocJB();
            System.out.println("EventApndx.doPost line 2");
            eventdocJB.setCreator(userId[0]);
            eventdocJB.setIpAdd(ipAdd[0]);
            eventdocJB.setEventId(eventId[0]);
            if(networkFlag[0].equals("Org_doc")||networkFlag[0].equals("User_doc")){
            	if(networkFlag[0].equals("Org_doc")){
            	     eventdocJB.setNetworkId(siteId[0]);}
            	     else{
            	    	 eventdocJB.setNetworkId(userPk[0]); 
            	    	 }
            	     
            }else{
            	eventdocJB.setNetworkId(networkId[0]);	
            }
            eventdocJB.setNetworkFlag(networkFlag[0]);
            eventdocJB.setEventdocDetails();
            int eventdocId = eventdocJB.getDocId();
            System.out.println("EventApndx.doPost line 3");
            pkBase[0] = String.valueOf(eventdocId);
            System.out.println("EventApndx.doPost line 4");

            System.out.println("in side do post of EventApndxdsdsdsdsds");
            System.out.println(pkBase[0]);
            System.out.println("EventApndx.doPost line 5");

            super.doPost(req, res);
            if (deleteFlag == false) {
                System.out.println("EventApndx.doPost line 6");

                String[] desc = (String[]) table.get("desc");
                System.out.println("EventApndx.doPost line 7");
                String[] type = (String[]) table.get("type");
                System.out.println("EventApndx.doPost line 8");
                String[] version = (String[]) table.get("version");
                System.out.println("EventApndx.doPost line 9");

                System.out.println(file_name);
                System.out.println(uploadFileSize);
                System.out.println("EventApndx.doPost line 10");
                eventdocJB.setDocId(eventdocId);
                System.out.println("EventApndx.doPost line 11");
                eventdocJB.getEventdocDetails();
                System.out.println("EventApndx.doPost line 12");
                eventdocJB.setDocDesc(desc[0]);
                System.out.println("EventApndx.doPost line 13");
                eventdocJB.setDocType(type[0]);
                System.out.println("EventApndx.doPost line 14");
                eventdocJB.setDocName(file_name);
                eventdocJB.setDocSize((new Long(uploadFileSize)).toString());
                if(!networkFlag[0].equals("") || !"".equalsIgnoreCase(networkFlag[0])){
                eventdocJB.setDocVersion(version[0]);
                System.out.println("EventApndx.doPost line 15");
                }
                eventdocJB.updateEventdoc();
                
                //logic for propagation
        		if ((propagateInVisitFlag[0] == null) || (propagateInVisitFlag[0].equals("")))
    				propagateInVisitFlag[0] = "N";

    			if ((propagateInEventFlag[0] == null) || (propagateInEventFlag[0].equals("")))
    				propagateInEventFlag[0] = "N";
    			
    			if ( (propagateInVisitFlag[0].equals("Y")) || (propagateInEventFlag[0].equals("Y"))) {
    				//docmode = "N" - New/Add, "M"- modify/update, and calType = "P" - protocol calendar, S: study cal
    				eventdocJB.setEventId(eventId[0]);
    				eventdocJB.propagateEventdoc(EJBUtil.stringToNum(p_protocol_id[0]), propagateInVisitFlag[0], propagateInEventFlag[0], propagateMode[0], propagateCalType[0]);  
    				System.out.println("EventApndx.... appendix in related events updated");
    			}
                
                
                /////////////////////////////////////////
                
            } else {
                eventdocJB.removeEventdoc(eventdocId, "U");
            }
            /*
             * out.println("<HTML>"); out.println("<BODY>"); out.println("<META
             * HTTP-EQUIV=Refresh CONTENT=\"0;"+path+"\""); out.println("</BODY>");
             * out.println("</HTML>");
             */
            res.sendRedirect("../../" + path);
        } else {
            /*
             * out.println("<HTML>"); out.println("<BODY>"); out.println("<META
             * HTTP-EQUIV=Refresh
             * CONTENT=\"0;URL=../../eres/jsp/incorrectesign.jsp\"");
             * out.println("</BODY>"); out.println("</HTML>");
             */
            res.sendRedirect("../../velos/jsp/incorrectesign.jsp");
        }

    }

    /**
     * 
     * 
     * Obtain information on this servlet.
     * 
     * 
     * @return String describing this servlet.
     * 
     * 
     */

    public String getServletInfo() {

        return "File upload servlet -- used to receive files";

    }

}
