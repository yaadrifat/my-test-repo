CREATE OR REPLACE TRIGGER "ER_STATUS_HISTORY_BI0" BEFORE INSERT ON ER_STATUS_HISTORY
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  insert_data CLOB;
     BEGIN
    BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||',' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;


                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
      :NEW.rid := erid ;
      SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'ER_STATUS_HISTORY',erid, 'I',usr);
    -- Added by Ganapathy on 06/21/05 for Audit inserting
	-- Added the column status_iscurrent for july-August enhancement #S4
       insert_data:= :NEW.CREATOR||'|'||:NEW.RECORD_TYPE||'|'||
    :NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
    TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD||'|'|| :NEW.PK_STATUS||'|'||
    :NEW.STATUS_MODPK||'|'||:NEW.STATUS_MODTABLE||'|'|| :NEW.FK_CODELST_STAT||'|'||
    TO_CHAR(:NEW.STATUS_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
    TO_CHAR(:NEW.STATUS_END_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.STATUS_NOTES||'|'||
    :NEW.STATUS_CUSTOM1||'|'|| :NEW.STATUS_ENTERED_BY||'|'||:NEW.STATUS_ISCURRENT||'|'||
     :NEW.RID;
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END ;
/


