create or replace
TRIGGER "ERES"."ER_STORAGE_AU0" AFTER UPDATE ON ERES.ER_STORAGE FOR EACH ROW
DECLARE
  raid NUMBER(10);
  new_usr VARCHAR2(100);
  old_usr VARCHAR2(100);
  usr VARCHAR2(100);
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  old_codetype VARCHAR2(200) ;
  new_codetype VARCHAR2(200) ;
BEGIN
--Created by Manimaran for Audit Update
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);
  
 --KM-#3897 - To avoid insertion of Update records in audit_row table while handling clob column in new and edit mode 
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN   
    audit_trail.record_transaction
    (raid, 'ER_STORAGE', :OLD.rid, 'U', usr);
 end if;
  
  IF NVL(:OLD.PK_STORAGE,0) !=
     NVL(:NEW.PK_STORAGE,0) THEN
     audit_trail.column_update
       (raid, 'PK_STORAGE',
       :OLD.PK_STORAGE, :NEW.PK_STORAGE);
  END IF;

  IF NVL(:OLD.STORAGE_ID,' ') !=
     NVL(:NEW.STORAGE_ID,' ') THEN
     audit_trail.column_update
       (raid, 'STORAGE_ID',
       :OLD.STORAGE_ID, :NEW.STORAGE_ID);
  END IF;

  IF NVL(:OLD.STORAGE_NAME,' ') !=
     NVL(:NEW.STORAGE_NAME,' ') THEN
     audit_trail.column_update
       (raid, 'STORAGE_NAME',
       :OLD.STORAGE_NAME, :NEW.STORAGE_NAME);
  END IF;

  IF NVL(:OLD.FK_CODELST_STORAGE_TYPE,0) !=
     NVL(:NEW.FK_CODELST_STORAGE_TYPE,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_STORAGE_TYPE',
       :OLD.FK_CODELST_STORAGE_TYPE, :NEW.FK_CODELST_STORAGE_TYPE);
  END IF;

  IF NVL(:OLD.FK_STORAGE,0) !=
     NVL(:NEW.FK_STORAGE,0) THEN
     audit_trail.column_update
       (raid, 'FK_STORAGE',
       :OLD.FK_STORAGE, :NEW.FK_STORAGE);
  END IF;

  IF NVL(:OLD.STORAGE_CAP_NUMBER,0) !=
     NVL(:NEW.STORAGE_CAP_NUMBER,0) THEN
     audit_trail.column_update
       (raid, 'STORAGE_CAP_NUMBER',
       :OLD.STORAGE_CAP_NUMBER, :NEW.STORAGE_CAP_NUMBER);
  END IF;

  IF NVL(:OLD.FK_CODELST_CAPACITY_UNITS,0) !=
     NVL(:NEW.FK_CODELST_CAPACITY_UNITS,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_CAPACITY_UNITS',
       :OLD.FK_CODELST_CAPACITY_UNITS, :NEW.FK_CODELST_CAPACITY_UNITS);
  END IF;

 --KM--Removal of storage_status column in the table.

 IF NVL(:OLD.STORAGE_DIM1_CELLNUM,0) !=
     NVL(:NEW.STORAGE_DIM1_CELLNUM,0) THEN
     audit_trail.column_update
       (raid, 'STORAGE_DIM1_CELLNUM',
       :OLD.STORAGE_DIM1_CELLNUM, :NEW.STORAGE_DIM1_CELLNUM);
  END IF;

  IF NVL(:OLD.STORAGE_DIM1_NAMING,' ') !=
     NVL(:NEW.STORAGE_DIM1_NAMING,' ') THEN
     audit_trail.column_update
       (raid, 'STORAGE_DIM1_NAMING',
       :OLD.STORAGE_DIM1_NAMING, :NEW.STORAGE_DIM1_NAMING);
  END IF;

  IF NVL(:OLD.STORAGE_DIM1_ORDER,' ') !=
     NVL(:NEW.STORAGE_DIM1_ORDER,' ') THEN
     audit_trail.column_update
       (raid, 'STORAGE_DIM1_ORDER',
       :OLD.STORAGE_DIM1_ORDER, :NEW.STORAGE_DIM1_ORDER);
  END IF;

  IF NVL(:OLD.STORAGE_DIM2_CELLNUM,0) !=
     NVL(:NEW.STORAGE_DIM2_CELLNUM,0) THEN
     audit_trail.column_update
       (raid, 'STORAGE_DIM2_CELLNUM',
       :OLD.STORAGE_DIM2_CELLNUM, :NEW.STORAGE_DIM2_CELLNUM);
  END IF;

  IF NVL(:OLD.STORAGE_DIM2_NAMING,' ') !=
     NVL(:NEW.STORAGE_DIM2_NAMING,' ') THEN
     audit_trail.column_update
       (raid, 'STORAGE_DIM2_NAMING',
       :OLD.STORAGE_DIM2_NAMING, :NEW.STORAGE_DIM2_NAMING);
  END IF;

  IF NVL(:OLD.STORAGE_DIM2_ORDER,' ') !=
     NVL(:NEW.STORAGE_DIM2_ORDER,' ') THEN
     audit_trail.column_update
       (raid, 'STORAGE_DIM2_ORDER',
       :OLD.STORAGE_DIM2_ORDER, :NEW.STORAGE_DIM2_ORDER);
  END IF;

  IF NVL(:OLD.STORAGE_AVAIL_UNIT_COUNT,0) !=
     NVL(:NEW.STORAGE_AVAIL_UNIT_COUNT,0) THEN
     audit_trail.column_update
       (raid, 'STORAGE_AVAIL_UNIT_COUNT',
       :OLD.STORAGE_AVAIL_UNIT_COUNT, :NEW.STORAGE_AVAIL_UNIT_COUNT);
  END IF;

  IF NVL(:OLD.STORAGE_COORDINATE_X,0) !=
     NVL(:NEW.STORAGE_COORDINATE_X,0) THEN
     audit_trail.column_update
       (raid, 'STORAGE_COORDINATE_X',
       :OLD.STORAGE_COORDINATE_X, :NEW.STORAGE_COORDINATE_X);
  END IF;

  IF NVL(:OLD.STORAGE_COORDINATE_Y,0) !=
     NVL(:NEW.STORAGE_COORDINATE_Y,0) THEN
     audit_trail.column_update
       (raid, 'STORAGE_COORDINATE_Y',
       :OLD.STORAGE_COORDINATE_Y, :NEW.STORAGE_COORDINATE_Y);
  END IF;

  IF NVL(:OLD.STORAGE_LABEL,' ') !=
     NVL(:NEW.STORAGE_LABEL,' ') THEN
     audit_trail.column_update
       (raid, 'STORAGE_LABEL',
       :OLD.STORAGE_LABEL, :NEW.STORAGE_LABEL);
  END IF;

  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM er_user
     WHERE pk_user = :OLD.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM er_user
     WHERE pk_user = :NEW.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_modby := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       :OLD.last_modified_date, :NEW.last_modified_date);
  END IF;
  IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
       :OLD.created_on, :NEW.created_on);
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
  --Added by Manimaran on 092007
  if nvl(:old.FK_ACCOUNT,0) !=
     NVL(:new.FK_ACCOUNT,0) then
     audit_trail.column_update
       (raid, 'FK_ACCOUNT',
       :old.FK_ACCOUNT, :new.FK_ACCOUNT);
  end if;
  --Added by Manimaran on 040108
  IF NVL(:OLD.STORAGE_ISTEMPLATE,0) !=
     NVL(:NEW.STORAGE_ISTEMPLATE,0) THEN
     audit_trail.column_update
       (raid, 'STORAGE_ISTEMPLATE',
       :OLD.STORAGE_ISTEMPLATE, :NEW.STORAGE_ISTEMPLATE);
  END IF;

 IF NVL(:OLD.STORAGE_TEMPLATE_TYPE,0) !=
     NVL(:NEW.STORAGE_TEMPLATE_TYPE,0) THEN
     audit_trail.column_update
       (raid, 'STORAGE_TEMPLATE_TYPE',
       :OLD.STORAGE_TEMPLATE_TYPE, :NEW.STORAGE_TEMPLATE_TYPE);
  END IF;

  IF NVL(:OLD.STORAGE_ALTERNALEID,' ') !=
     NVL(:NEW.STORAGE_ALTERNALEID,' ') THEN
     audit_trail.column_update
       (raid, 'STORAGE_ALTERNALEID',
       :OLD.STORAGE_ALTERNALEID, :NEW.STORAGE_ALTERNALEID);
  END IF;


 IF NVL(:OLD.STORAGE_UNIT_CLASS,0) !=
     NVL(:NEW.STORAGE_UNIT_CLASS,0) THEN
     audit_trail.column_update
       (raid, 'STORAGE_UNIT_CLASS',
       :OLD.STORAGE_UNIT_CLASS, :NEW.STORAGE_UNIT_CLASS);
  END IF;

  IF NVL(:OLD.STORAGE_KIT_CATEGORY,0) !=
     NVL(:NEW.STORAGE_KIT_CATEGORY,0) THEN
     audit_trail.column_update
       (raid, 'STORAGE_KIT_CATEGORY',
       :OLD.STORAGE_KIT_CATEGORY, :NEW.STORAGE_KIT_CATEGORY);
  END IF;


END;

/


