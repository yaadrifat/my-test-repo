CREATE OR REPLACE TRIGGER "ER_PATTXARM_AD0" 
AFTER DELETE
ON ER_PATTXARM
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction
    (raid, 'ER_PATTXARM', :OLD.rid, 'D');
  deleted_data :=
  TO_CHAR(:OLD.pk_pattxarm) || '|' ||
  TO_CHAR(:OLD.fk_patprot) || '|' ||
  TO_CHAR(:OLD.fk_studytxarm) || '|' ||
  :OLD.tx_drug_info || '|' ||
  TO_CHAR(:OLD.tx_start_date) || '|' ||
  TO_CHAR(:OLD.tx_end_date) || '|' ||
  :OLD.notes || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on);
INSERT INTO AUDIT_DELETE
(raid, row_data) VALUES (raid, deleted_data);
END;
/


