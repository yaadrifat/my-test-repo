create or replace TRIGGER "ERES"."ER_DYNREP_BI0" BEFORE INSERT ON ER_DYNREP
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
--Created by Manimaran for Audit Insert
BEGIN
   BEGIN
	  --KM -#3635	
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM er_user
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;

   audit_trail.record_transaction(raid, 'ER_DYNREP',erid, 'I',usr);
   insert_data:= :NEW.PK_DYNREP||'|'|| :NEW.FK_FORM||'|'||:NEW.DYNREP_NAME||'|'||:NEW.DYNREP_DESC||'|'||:NEW.DYNREP_FILTER||'|'||:NEW.DYNREP_ORDERBY||'|'||
   :NEW.FK_USER||'|'||:NEW.FK_ACCOUNT||'|'||:NEW.DYNREP_HDRNAME||'|'||:NEW.DYNREP_FTRNAME||'|'||:NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||
   TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD||'|'||:NEW.REPORT_TYPE||'|'||:NEW.FK_STUDY||'|'||
   :NEW.FK_PERSON||'|'||:NEW.DYNREP_SHAREWITH||'|'||:NEW.DYNREP_TYPE||'|'||:NEW.DYNREP_FORMLIST||'|'||:NEW.DYNREP_USEUNIQUEID||'|'||:NEW.DYNREP_USEDATAVAL;
   INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END ;
/


