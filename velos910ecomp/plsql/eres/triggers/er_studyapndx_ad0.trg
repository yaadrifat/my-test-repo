CREATE OR REPLACE TRIGGER "ER_STUDYAPNDX_AD0" 
AFTER DELETE
ON ER_STUDYAPNDX
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(2000);
begin
  select seq_audit.nextval into raid from dual;
  audit_trail.record_transaction
    (raid, 'ER_STUDYAPNDX', :old.rid, 'D');
  deleted_data :=
  to_char(:old.pk_studyapndx) || '|' ||
  to_char(:old.fk_study) || '|' ||
  :old.studyapndx_uri || '|' ||
  :old.studyapndx_desc || '|' ||
  :old.studyapndx_pubflag || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.studyapndx_file || '|' ||
  :old.studyapndx_type || '|' ||
  :old.ip_add;
insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


