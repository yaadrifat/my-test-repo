CREATE OR REPLACE TRIGGER "ER_PATFLABS_AU_REV" 
AFTER UPDATE
ON ER_PATLABS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE

/* **********************************
   **
   ** Author: Sonia Sahni 05/05/2004
   ** for reverse A2A
   ** Insert a record in er_rev_pendingdata
   *************************************
*/

v_key NUMBER;
v_pk NUMBER;
v_tabname VARCHAR2(50);
v_per NUMBER;
v_account NUMBER;
v_site_code VARCHAR2(255);
v_sponsor_study NUMBER;
v_err VARCHAR2(4000);
v_module_name VARCHAR2(20);
v_sponsor_account NUMBER;
v_site_study NUMBER;
v_count NUMBER;

BEGIN


  v_site_study := :NEW.fk_study;
  -- check if its a networked study and data needs to be sent back

  PKG_IMPEX_REVERSE.SP_GET_STUDYSPONSORINFO(v_site_study, v_sponsor_study , v_site_code, v_sponsor_account, v_err);

  IF LENGTH(trim(v_site_code)) >  0 AND v_sponsor_study > 0 THEN

  --insert data for patient filled form

   v_tabname := 'er_patlabs';
   v_module_name := 'pat_labs';

   v_pk := :NEW.PK_PATLABS ;

  SELECT pkg_impex_reverse.getRevPendingDataCount(v_module_name,     v_site_study,  v_site_code, v_tabname,v_pk)
  INTO v_count FROM dual;

 IF v_count <= 0 THEN

   SELECT SEQ_ER_REV_PENDINGDATA.NEXTVAL
   INTO v_key FROM dual;

   INSERT INTO ER_REV_PENDINGDATA (pk_rp, rp_tablename, rp_tablepk, RP_SITECODE,FK_STUDY,RP_MODULE)
   VALUES (v_key ,v_tabname,v_pk,v_site_code,v_site_study,v_module_name);

END IF;
  END IF; -- for v_site_code



END;
/


