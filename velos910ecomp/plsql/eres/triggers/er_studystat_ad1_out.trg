create or replace
TRIGGER ER_STUDYSTAT_AD1_OUT 
AFTER DELETE ON ER_STUDYSTAT 
FOR EACH ROW
DECLARE 
fkaccount NUMBER; 
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL  => Plog.LFATAL);
BEGIN
  SELECT fk_account 
  INTO fkaccount
  FROM er_study
  WHERE pk_study = :OLD.fk_study; 
  
  
  PKG_MSG_QUEUE.SP_POPULATE_STUDY_STATUS_MSG( :OLD.pk_studystat,
  fkaccount,
  'D',
  to_char(:OLD.fk_study),
  :OLD.FK_CODELST_STUDYSTAT);  
  EXCEPTION  WHEN OTHERS THEN
  Plog.FATAL(pCTX,'exception in ER_STUDYSTAT_AD1_OUT ' || SQLERRM);
END;
/
