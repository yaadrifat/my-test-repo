create or replace TRIGGER "ER_FLDACTION_BI0" BEFORE INSERT ON ER_FLDACTION
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
BEGIN
   BEGIN
      --KM-#3635
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM er_user
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;
   audit_trail.record_transaction(raid, 'ER_FLDACTION',erid, 'I',usr);
   insert_data:= :NEW.PK_FLDACTION||'|'||:NEW.FK_FORM||'|'||:NEW.FLDACTION_TYPE||'|'||:NEW.FK_FIELD||'|'||:NEW.FLDACTION_CONDITION||'|'||:NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||:NEW.IP_ADD;

   INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END ;
/


