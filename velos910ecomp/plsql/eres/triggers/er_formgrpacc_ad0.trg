CREATE OR REPLACE TRIGGER "ER_FORMGRPACC_AD0" 
AFTER UPDATE--DELETE
ON ER_FORMGRPACC
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);

rtype CHAR;
BEGIN
  rtype:=:NEW.record_type;
-- Added by Ganapathy on 28/06/05 for Audit Delete
 IF(rtype='D') THEN


  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'ER_FORMGRPACC', :OLD.rid, 'D');

  deleted_data :=
  TO_CHAR(:OLD.pk_formgrpacc) || '|' ||
  TO_CHAR(:OLD.fk_group) || '|' ||
  TO_CHAR(:OLD.fk_formlib) || '|' ||
  :OLD.record_type || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  :OLD.ip_add;

INSERT INTO audit_delete
(raid, row_data) VALUES (raid, deleted_data);
END IF;
END;
/


