CREATE OR REPLACE TRIGGER "ER_REPFLDVALIDATE_AD0" 
AFTER DELETE
ON ER_REPFLDVALIDATE
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
--Created by Manimaran for Audit delete.
declare
  raid number(10);
  deleted_data varchar2(2000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'ER_REPFLDVALIDATE', :old.rid, 'D');

  deleted_data :=
  to_char(:old.PK_REPFLDVALIDATE) || '|' ||
  to_char(:old.FK_REPFORMFLD) || '|' ||
  to_char(:old.FK_FIELD) || '|' ||
  :old.REPFLDVALIDATE_JAVASCR || '|' ||
  :old.RECORD_TYPE || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.ip_add  || '|' ||
  to_char(:old.rid);

  insert into audit_delete(raid, row_data) values (raid, deleted_data);
end;
/


