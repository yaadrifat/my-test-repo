CREATE OR REPLACE TRIGGER "ER_USR_LNKS_AD0" 
AFTER DELETE
ON ER_USR_LNKS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(2000);
begin
  select seq_audit.nextval into raid from dual;
  audit_trail.record_transaction
    (raid, 'ER_USR_LNKS', :old.rid, 'D');
  deleted_data :=
  to_char(:old.pk_usr_lnks) || '|' ||
  to_char(:old.fk_account) || '|' ||
  to_char(:old.fk_user) || '|' ||
  :old.link_uri || '|' ||
  :old.link_desc || '|' ||
  :old.link_grpname || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.ip_add;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


