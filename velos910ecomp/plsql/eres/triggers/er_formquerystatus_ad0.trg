CREATE OR REPLACE TRIGGER "ER_FORMQUERYSTATUS_AD0" 
  AFTER DELETE
  ON er_formquerystatus
  FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'ER_FORMQUERYSTATUS', :OLD.rid, 'D');

  deleted_data :=
  TO_CHAR(:OLD.pk_formquerystatus) || '|' ||
  TO_CHAR(:OLD.fk_formquery) || '|' ||
  TO_CHAR(:OLD.formquery_type) || '|' ||
  TO_CHAR(:OLD.fk_codelst_querytype) || '|' ||
   :OLD.query_notes || '|' ||
   TO_CHAR(:OLD.entered_by) || '|' ||
  TO_CHAR(:OLD.entered_on) || '|' ||
    TO_CHAR(:OLD.fk_codelst_querystatus) || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add;

INSERT INTO AUDIT_DELETE
(raid, row_data) VALUES (raid, deleted_data);
END;
/


