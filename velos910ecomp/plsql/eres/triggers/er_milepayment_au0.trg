create or replace
TRIGGER ER_MILEPAYMENT_AU0
AFTER UPDATE OF CREATED_ON,CREATOR,FK_MILESTONE,FK_STUDY,IP_ADD,MILEPAYMENT_AMT,MILEPAYMENT_DATE,MILEPAYMENT_DELFLAG,MILEPAYMENT_DESC,PK_MILEPAYMENT,RID
ON ER_MILEPAYMENT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr varchar2(2000);
BEGIN
  --KM-#3634
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.LAST_MODIFIED_BY);
 
  --KM-#3634
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.record_transaction
    (raid, 'ER_MILEPAYMENT', :OLD.rid, 'U', usr);
  END IF;
  
 
  IF NVL(:OLD.pk_milepayment,0) !=
     NVL(:NEW.pk_milepayment,0) THEN
     Audit_Trail.column_update
       (raid, 'PK_MILEPAYMENT',
       :OLD.pk_milepayment, :NEW.pk_milepayment);
  END IF;
  IF NVL(:OLD.fk_study,0) !=
     NVL(:NEW.fk_study,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_STUDY',
       :OLD.fk_study, :NEW.fk_study);
  END IF;
  IF NVL(:OLD.fk_milestone,0) !=
     NVL(:NEW.fk_milestone,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_MILESTONE',
       :OLD.fk_milestone, :NEW.fk_milestone);
  END IF;
  IF NVL(:OLD.milepayment_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.milepayment_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update
       (raid, 'MILEPAYMENT_DATE',
       to_char(:OLD.milepayment_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.milepayment_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.milepayment_amt,0) !=
     NVL(:NEW.milepayment_amt,0) THEN
     Audit_Trail.column_update
       (raid, 'MILEPAYMENT_AMT',
       :OLD.milepayment_amt, :NEW.milepayment_amt);
  END IF;
  IF NVL(:OLD.milepayment_desc,' ') !=
     NVL(:NEW.milepayment_desc,' ') THEN
     Audit_Trail.column_update
       (raid, 'MILEPAYMENT_DESC',
       :OLD.milepayment_desc, :NEW.milepayment_desc);
  END IF;
  IF NVL(:OLD.milepayment_delflag,' ') !=
     NVL(:NEW.milepayment_delflag,' ') THEN
     Audit_Trail.column_update
       (raid, 'MILEPAYMENT_DELFLAG',
       :OLD.milepayment_delflag, :NEW.milepayment_delflag);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     Audit_Trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     Audit_Trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     Audit_Trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
   IF NVL(:OLD.milepayment_comments,' ') !=
     NVL(:NEW.milepayment_comments,' ') THEN
     Audit_Trail.column_update
       (raid, 'milepayment_comments',
       :OLD.milepayment_comments, :NEW.milepayment_comments);
  END IF;
  IF NVL(:OLD.milepayment_type,0) !=
     NVL(:NEW.milepayment_type,0) THEN
     Audit_Trail.column_update
       (raid, 'milepayment_type',
       :OLD.milepayment_type, :NEW.milepayment_type);
  END IF;

END;
/