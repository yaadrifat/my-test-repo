CREATE OR REPLACE TRIGGER "ER_SAVEDREP_BI0" BEFORE INSERT ON ER_SAVEDREP
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
  erid NUMBER(10);

  raid NUMBER(10);
  insert_data CLOB;
     BEGIN
    SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

audit_trail.record_transaction(raid, 'ER_SAVEDREP',erid, 'I',:NEW.LAST_MODIFIED_BY);
  -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.PK_SAVEDREP||'|'|| :NEW.FK_STUDY||'|'||
    :NEW.FK_REPORT||'|'||:NEW.SAVEDREP_NAME||'|'||
    TO_CHAR(:NEW.SAVEDREP_ASOFDATE,PKG_DATEUTIL.f_get_dateformat)||'|'|| :NEW.FK_USER||'|'||
    :NEW.FK_PAT||'|'|| :NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
    TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
    TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD||'|'||
  :NEW.SAVEDREP_FILTER;
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END;
/


