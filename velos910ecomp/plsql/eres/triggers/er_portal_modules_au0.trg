CREATE OR REPLACE TRIGGER ER_PORTAL_MODULES_AU0 AFTER UPDATE OF RID,FK_ID,PM_TO,IP_ADD,PM_FROM,PM_TYPE,PM_ALIGN,FK_PORTAL,PM_TO_UNIT,PM_FROM_UNIT,LAST_MODIFIED_BY,PK_PORTAL_MODULES,LAST_MODIFIED_DATE,PM_FORM_AFTER_RESP ON ER_PORTAL_MODULES FOR EACH ROW
declare
   raid number(10);
   usr varchar2(100);
   old_modby varchar2(100);
   new_modby varchar2(100);
begin
  select seq_audit.nextval into raid from dual;
   usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_PORTAL_MODULES', :old.rid, 'U', usr);
  if nvl(:old.PK_PORTAL_MODULES,0) !=
     NVL(:new.PK_PORTAL_MODULES,0) then
     audit_trail.column_update
       (raid, 'PK_PORTAL_MODULES',
       :old.PK_PORTAL_MODULES, :new.PK_PORTAL_MODULES);
  end if;
  if nvl(:old.FK_PORTAL,0) !=
     NVL(:new.FK_PORTAL,0) then
     audit_trail.column_update
       (raid, 'FK_PORTAL',
       :old.FK_PORTAL, :new.FK_PORTAL);
  end if;
  if nvl(:old.FK_ID,' ') !=
     NVL(:new.FK_ID,' ') then
     audit_trail.column_update
       (raid, 'FK_ID',
       :old.FK_ID, :new.FK_ID);
  end if;
  if nvl(:old.PM_TYPE,' ') !=
     NVL(:new.PM_TYPE,' ') then
     audit_trail.column_update
       (raid, 'PM_TYPE',
       :old.PM_TYPE, :new.PM_TYPE);
  end if;
  if nvl(:old.PM_ALIGN,' ') !=
     NVL(:new.PM_ALIGN,' ') then
     audit_trail.column_update
       (raid, 'PM_ALIGN',
       :old.PM_ALIGN, :new.PM_ALIGN);
  end if;
  if nvl(:old.PM_FROM,0) !=
     NVL(:new.PM_FROM,0) then
     audit_trail.column_update
       (raid, 'PM_FROM',
       :old.PM_FROM, :new.PM_FROM);
  end if;
  if nvl(:old.PM_TO,0) !=
     NVL(:new.PM_TO,0) then
     audit_trail.column_update
       (raid, 'PM_TO',
       :old.PM_TO, :new.PM_TO);
  end if;
  if nvl(:old.PM_FROM_UNIT,' ') !=
     NVL(:new.PM_FROM_UNIT,' ') then
     audit_trail.column_update
       (raid, 'PM_FROM_UNIT',
       :old.PM_FROM_UNIT, :new.PM_FROM_UNIT);
  end if;
  if nvl(:old.PM_TO_UNIT,' ') !=
     NVL(:new.PM_TO_UNIT,' ') then
     audit_trail.column_update
       (raid, 'PM_TO_UNIT',
       :old.PM_TO_UNIT, :new.PM_TO_UNIT);
  end if;
  if nvl(:old.PM_FORM_AFTER_RESP,' ') !=
     NVL(:new.PM_FORM_AFTER_RESP,' ') then
     audit_trail.column_update
       (raid, 'PM_FORM_AFTER_RESP',
       :old.PM_FORM_AFTER_RESP, :new.PM_FORM_AFTER_RESP);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;

  if nvl(:old.LAST_MODIFIED_BY,0) !=
     NVL(:new.LAST_MODIFIED_BY,0) then
	Begin
		Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
		into old_modby from er_user  where pk_user = :old.last_modified_by ;
		Exception When NO_DATA_FOUND then
			old_modby := null;
	End ;
	Begin
		Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
		into new_modby   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
		Exception When NO_DATA_FOUND then
			new_modby := null;
	 End ;
	audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
   end if;

   if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
      audit_trail.column_update
      (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
    end if;

    if nvl(:old.ip_add,' ') !=
       NVL(:new.ip_add,' ') then
       audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
    end if;

end;
/


