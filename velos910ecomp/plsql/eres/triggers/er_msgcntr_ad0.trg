CREATE OR REPLACE TRIGGER "ER_MSGCNTR_AD0" 
AFTER DELETE
ON ER_MSGCNTR
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(2000);
begin
  select seq_audit.nextval into raid from dual;
  audit_trail.record_transaction
    (raid, 'ER_MSGCNTR', :old.rid, 'D');
  deleted_data :=
  to_char(:old.pk_msgcntr) || '|' ||
  to_char(:old.fk_study) || '|' ||
  to_char(:old.fk_userx) || '|' ||
  to_char(:old.fk_userto) || '|' ||
  :old.msgcntr_text || '|' ||
  :old.msgcntr_reqtype || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.msgcntr_stat || '|' ||
  :old.msgcntr_perm || '|' ||
  :old.ip_add;
insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


