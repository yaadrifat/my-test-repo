CREATE OR REPLACE TRIGGER "ER_PATTXARM_BI0" BEFORE INSERT ON ER_PATTXARM
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
 erid NUMBER(10);
 usr VARCHAR(2000);
 raid NUMBER(10);
 insert_data CLOB;
     BEGIN
    usr := getuser(:NEW.creator);
                 SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'ER_PATTXARM',erid, 'I',usr);
    -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.PK_PATTXARM||'|'|| :NEW.FK_PATPROT||'|'||
    :NEW.FK_STUDYTXARM||'|'||:NEW.TX_DRUG_INFO||'|'||
     TO_CHAR(:NEW.TX_START_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
  TO_CHAR(:NEW.TX_END_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.NOTES||'|'||
  :NEW.RID||'|'|| :NEW.CREATOR||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||
  :NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat);
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END ;
/


