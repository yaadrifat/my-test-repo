CREATE OR REPLACE TRIGGER "ER_PATPROT_AU1" 
AFTER UPDATE OF FK_PROTOCOL
ON ER_PATPROT REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
WHEN (
OLD.fk_protocol IS NULL AND  NEW.FK_PROTOCOL IS NOT NULL
      )
DECLARE
  v_new_fk_study NUMBER;
  v_new_fk_protocol NUMBER;
  v_new_pk_patprot  NUMBER;
  v_new_creator NUMBER;
  v_new_created_on DATE;
  v_new_ip_add  VARCHAR2 (15) ;

/**************************************************************************************************
   **
   ** Author: Sonia Sahni 04/08/2005
   ** insert default alert notifications for patient enrollment
   ** according to the study's global settings - global flag and edit lock
*/

BEGIN

  v_new_fk_study := :NEW.FK_STUDY;
  v_new_fk_protocol :=  :NEW.FK_PROTOCOL ;
  v_new_pk_patprot  := :NEW.PK_PATPROT;
  v_new_creator := :NEW.CREATOR;
  v_new_created_on := :NEW.CREATED_ON;
  v_new_ip_add :=  :NEW.IP_ADD;

 IF (:OLD.FK_PROTOCOL IS NULL AND NVL(:OLD.FK_PROTOCOL,0) <> :NEW.FK_PROTOCOL) THEN
   DELETE FROM sch_alertnotify WHERE fk_patprot = :NEW.pk_patprot;

    pkg_alnot.set_alertnotify_for_enrollment(v_new_fk_study ,  v_new_fk_protocol,  v_new_pk_patprot , v_new_creator , v_new_created_on , v_new_ip_add );

END IF;

END;
/


