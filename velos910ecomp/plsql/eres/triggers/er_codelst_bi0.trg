create or replace TRIGGER "ERES"."ER_CODELST_BI0" BEFORE INSERT ON ER_CODELST
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW  WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
 erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  v_new_creator NUMBER;
  insert_data CLOB;
     BEGIN
    IF (:NEW.creator IS NULL) THEN
    usr := NULL;

  ELSE

  v_new_creator := :NEW.creator;


  SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' ||usr_firstname
  INTO usr FROM er_user WHERE pk_user = v_new_creator ;

  END IF;

                    SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;



  audit_trail.record_transaction(raid, 'ER_CODELST',erid, 'I',usr);
  -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.PK_CODELST||'|'|| :NEW.FK_ACCOUNT||'|'||
    :NEW.CODELST_TYPE||'|'||:NEW.CODELST_SUBTYP||'|'||:NEW.CODELST_DESC||'|'||
    :NEW.CODELST_HIDE||'|'||
    :NEW.CODELST_SEQ||'|'||:NEW.CODELST_MAINT||'|'|| :NEW.RID||'|'||
    :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
    TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
    TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD||'|'||
    :NEW.CODELST_CUSTOM_COL||'|'||:NEW.CODELST_CUSTOM_COL1||'|'||:NEW.CODELST_STUDY_ROLE;
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END;

   /