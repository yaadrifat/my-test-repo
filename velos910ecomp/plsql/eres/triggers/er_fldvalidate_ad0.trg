CREATE OR REPLACE TRIGGER "ER_FLDVALIDATE_AD0" 
AFTER  UPDATE--DELETE
ON ER_FLDVALIDATE
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);

rtype CHAR;
BEGIN
  rtype:=:NEW.record_type;
-- Added by Ganapathy on 28/06/05 for Audit Delete
 IF(rtype='D') THEN


  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'ER_FLDVALIDATE', :OLD.rid, 'D');

  deleted_data :=
  TO_CHAR(:OLD.pk_fldvalidate) || '|' ||
  TO_CHAR(:OLD.fk_fldlib) || '|' ||
  :OLD.fldvalidate_op1 || '|' ||
  :OLD.fldvalidate_val1 || '|' ||
  :OLD.fldvalidate_logop1 || '|' ||
  :OLD.fldvalidate_op2 || '|' ||
  :OLD.fldvalidate_val2 || '|' ||
  :OLD.fldvalidate_logop2 || '|' ||
  :OLD.fldvalidate_javascr || '|' ||
  :OLD.record_type || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add || '|' ||
  TO_CHAR(:OLD.rid);

INSERT INTO audit_delete
(raid, row_data) VALUES (raid, deleted_data);
END IF;
END;
/


