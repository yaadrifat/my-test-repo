create or replace TRIGGER "ER_STUDYSEC_BI0"
BEFORE INSERT
ON ER_STUDYSEC
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
 WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      ) DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  USR VARCHAR2(100);
  insert_data CLOB;


BEGIN
 BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
  INTO usr FROM ER_USER WHERE pk_user =:NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;

SELECT TRUNC(seq_rid.NEXTVAL) INTO erid FROM dual;
	   :NEW.rid := erid ;
	   SELECT seq_audit.NEXTVAL INTO raid FROM dual;

Audit_Trail.record_transaction(raid, 'ER_STUDYSEC', erid, 'I', USR );
-- Added by JMajumdar on 23-06-05 for Audit Insert

--modified by Sonia Abrol, change dthe field to studysec_text
insert_data := :NEW.PK_STUDYSEC||'|'||:NEW.FK_STUDY||'|'||:NEW.STUDYSEC_NAME||'|';

insert_data := insert_data || :NEW.STUDYSEC_PUBFLAG||'|'||:NEW.STUDYSEC_SEQ||'|'||
			  :NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||
			  TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
			  TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
			  :NEW.IP_ADD||'|'|| :NEW.STUDYSEC_NUM||'|'||
			  :NEW.FK_STUDYVER ||'|';

--JM: commented and modified in the new line
--insert_data := insert_data || 	 dbms_lob.substr(:NEW.STUDYSEC_TEXT,1,4000) ||'|';
insert_data := insert_data || 	 dbms_lob.SUBSTR(:NEW.STUDYSEC_TEXT,4000,1);



INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
END;
/


