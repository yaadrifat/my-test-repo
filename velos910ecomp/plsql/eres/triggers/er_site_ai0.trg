CREATE OR REPLACE TRIGGER "ER_SITE_AI0" 
AFTER INSERT
ON ER_SITE
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
BEGIN

Insert into
er_usersite (PK_USERSITE ,
              FK_USER,
              FK_SITE,
              USERSITE_RIGHT ,
              CREATOR,
              CREATED_ON,
              IP_ADD)
Select seq_er_usersite.nextval,u.pk_user,
       :new.pk_site, 0,:new.creator,
       :new.created_on,:new.ip_add
from er_user u
where u.fk_account = :new.fk_account and u.usr_type<>'N';
-- JM: 03/15/2006: modified above line with "and u.usr_type<>'N'"

END;
/


