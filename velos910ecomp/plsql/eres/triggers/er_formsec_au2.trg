CREATE OR REPLACE TRIGGER "ER_FORMSEC_AU2" 
AFTER UPDATE OF RECORD_TYPE
ON ER_FORMSEC
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
new.RECORD_TYPE = 'D'
      )
BEGIN



/*

sets the record_type to 'D' for all the fields in this section

*/

          update er_formfld

		set record_type = 'D'

		where fk_formsec = :new.pk_formsec ;



          update er_formlib

          set FORM_XSLREFRESH  = 1

          where pk_formlib = :old.fk_formlib ;





	 END;
/


