CREATE OR REPLACE TRIGGER "ER_MOREDETAILS_AD0" 
AFTER DELETE
ON ER_MOREDETAILS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
--Created by Manimaran for Audit delete.
declare
  raid number(10);
  deleted_data varchar2(2000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'ER_MOREDETAILS', :old.rid, 'D');

  deleted_data :=
  to_char(:old.PK_MOREDETAILS) || '|' ||
  to_char(:old.FK_MODPK) || '|' ||
  :old.MD_MODNAME || '|' ||
  to_char(:old.MD_MODELEMENTPK) || '|' ||
  :old.MD_MODELEMENTDATA || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.created_on) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  :old.ip_add;


  insert into audit_delete(raid, row_data) values (raid, deleted_data);
end;
/


