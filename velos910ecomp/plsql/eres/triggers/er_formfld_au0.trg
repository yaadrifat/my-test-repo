CREATE OR REPLACE TRIGGER ER_FORMFLD_AU0
  after update of
  pk_formfld,
  fk_formsec,
  fk_field,
  formfld_seq,
  formfld_mandatory,
  formfld_browserflg,
  formfld_xsl,
  formfld_javascr,
  last_modified_by,
  record_type,
  last_modified_date,
  ip_add,
  rid
  on er_formfld
  for each row
declare
  raid number(10);

   usr varchar2(100);

   old_modby varchar2(100);

   new_modby varchar2(100);

begin
  select seq_audit.nextval into raid from dual;

   usr := getuser(:new.last_modified_by);

  audit_trail.record_transaction
    (raid, 'ER_FORMFLD', :old.rid, 'U', usr);

  if nvl(:old.pk_formfld,0) !=
     NVL(:new.pk_formfld,0) then
     audit_trail.column_update
       (raid, 'PK_FORMFLD',
       :old.pk_formfld, :new.pk_formfld);
  end if;
  if nvl(:old.fk_formsec,0) !=
     NVL(:new.fk_formsec,0) then
     audit_trail.column_update
       (raid, 'FK_FORMSEC',
       :old.fk_formsec, :new.fk_formsec);
  end if;
  if nvl(:old.fk_field,0) !=
     NVL(:new.fk_field,0) then
     audit_trail.column_update
       (raid, 'FK_FIELD',
       :old.fk_field, :new.fk_field);
  end if;
  if nvl(:old.formfld_seq,0) !=
     NVL(:new.formfld_seq,0) then
     audit_trail.column_update
       (raid, 'FORMFLD_SEQ',
       :old.formfld_seq, :new.formfld_seq);
  end if;
  if nvl(:old.formfld_mandatory,0) !=
     NVL(:new.formfld_mandatory,0) then
     audit_trail.column_update
       (raid, 'FORMFLD_MANDATORY',
       :old.formfld_mandatory, :new.formfld_mandatory);
  end if;
  if nvl(:old.formfld_browserflg,0) !=
     NVL(:new.formfld_browserflg,0) then
     audit_trail.column_update
       (raid, 'FORMFLD_BROWSERFLG',
       :old.formfld_browserflg, :new.formfld_browserflg);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.formfld_xsl,' ') !=
     NVL(:new.formfld_xsl,' ') then
     audit_trail.column_update
       (raid, 'FORMFLD_XSL',
       :old.formfld_xsl, :new.formfld_xsl);
  end if;
  if nvl(:old.formfld_javascr,' ') !=
     NVL(:new.formfld_javascr,' ') then
     audit_trail.column_update
       (raid, 'FORMFLD_JAVASCR',
       :old.formfld_javascr, :new.formfld_javascr);
  end if;
  if nvl(:old.record_type,' ') !=
     NVL(:new.record_type,' ') then
     audit_trail.column_update
       (raid, 'RECORD_TYPE',
       :old.record_type, :new.record_type);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ip_add,' ') !=
     NVL(:new.ip_add,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
  end if;
 if nvl(:old.LAST_MODIFIED_BY,0) !=
 NVL(:new.LAST_MODIFIED_BY,0) then
	Begin
		Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
		into old_modby from er_user  where pk_user = :old.last_modified_by ;
		Exception When NO_DATA_FOUND then
			old_modby := null;
	End ;
	Begin
		Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
		into new_modby   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
		Exception When NO_DATA_FOUND then
			new_modby := null;
	 End ;
	audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
 end if;

end;
/


