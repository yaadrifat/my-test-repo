create or replace TRIGGER ER_STUDYSITES_APNDX_AU
AFTER UPDATE
ON ER_STUDYSITES_APNDX REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  usr VARCHAR2(100);
BEGIN
    select seq_audit.nextval into raid from dual;
    usr := getuser(:new.last_modified_by);
   

  --KM-#3635-To avoid insertion of Update records in audit_row table while handling clob column in new and edit mode
  IF NVL(:OLD.last_modified_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
    audit_trail.record_transaction
    (raid, 'ER_STUDYSITES_APNDX', :OLD.rid, 'U', usr);
 end if;

if nvl(:old.PK_STUDYSITES_APNDX,0) !=
     NVL(:new.PK_STUDYSITES_APNDX,0) then
     audit_trail.column_update
       (raid, 'PK_STUDYSITES_APNDX',
       :old.PK_STUDYSITES_APNDX, :new.PK_STUDYSITES_APNDX);
  end if;

if nvl(:old.FK_STUDYSITES,0) !=
     NVL(:new.FK_STUDYSITES,0) then
     audit_trail.column_update
       (raid, 'FK_STUDYSITES',
       :old.FK_STUDYSITES, :new.FK_STUDYSITES);
  end if;

if nvl(:old.APNDX_TYPE,' ') !=
     NVL(:new.APNDX_TYPE,' ') then
     audit_trail.column_update
       (raid, 'APNDX_TYPE',
       :old.APNDX_TYPE, :new.APNDX_TYPE);
  end if;


if nvl(:old.APNDX_NAME,' ') !=
     NVL(:new.APNDX_NAME,' ') then
     audit_trail.column_update
       (raid, 'APNDX_NAME',
       :old.APNDX_NAME, :new.APNDX_NAME);
  end if;

if nvl(:old.APNDX_DESCRIPTION,' ') !=
     NVL(:new.APNDX_DESCRIPTION,' ') then
     audit_trail.column_update
       (raid, 'APNDX_DESCRIPTION',
       :old.APNDX_DESCRIPTION, :new.APNDX_DESCRIPTION);
  end if;

if nvl(:old.APNDX_FILESIZE,0) !=
     NVL(:new.APNDX_FILESIZE,0) then
     audit_trail.column_update
       (raid, 'APNDX_FILESIZE',
       :old.APNDX_FILESIZE, :new.APNDX_FILESIZE);
  end if;

if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;

if nvl(:old.CREATOR,0) !=
     NVL(:new.CREATOR,0) then
     audit_trail.column_update
       (raid, 'CREATOR',
       :old.CREATOR, :new.CREATOR);
   end if;

if nvl(:old.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;

if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
   Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into old_modby
     from er_user
     where pk_user = :old.last_modified_by ;
     Exception When NO_DATA_FOUND then
      old_modby := null ;
    End ;
    Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into new_modby
     from er_user
     where pk_user = :new.last_modified_by ;
     Exception When NO_DATA_FOUND then
      new_modby := null ;
    End ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  end if;

if nvl(:old.last_modified_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_ON',
       to_char(:OLD.last_modified_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;

   if nvl(:old.IP_ADD,' ') !=
     NVL(:new.IP_ADD,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.IP_ADD, :new.IP_ADD);
  end if;
END;
/


