CREATE OR REPLACE TRIGGER "ER_PATFACILITY_BI0" BEFORE INSERT ON ER_PATFACILITY
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
 erid NUMBER(10);
 usr VARCHAR(2000);
 raid NUMBER(10);
 insert_data CLOB;
BEGIN
    usr := Getuser(:NEW.creator);


 SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;

  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  Audit_Trail.record_transaction(raid, 'ER_PATFACILITY',erid, 'I',usr);

 insert_data:= :NEW.PK_PATFACILITY||'|'|| :NEW.FK_PER||'|'|| :NEW.FK_SITE ||'|'|| :NEW.PAT_FACILITYID ||'|'|| TO_CHAR(:NEW.PATFACILITY_REGDATE ,PKG_DATEUTIL.f_get_dateformat)
 ||'|'|| :NEW.PATFACILITY_PROVIDER ||'|'|| :NEW.PATFACILITY_OTHERPROVIDER    ||'|'|| :NEW.PATFACILITY_SPLACCESS
||'|'|| :NEW.PATFACILITY_ACCESSRIGHT      ||'|'|| :NEW.PATFACILITY_DEFAULT    ||'|'|| :NEW.CREATOR ||'|'|| :NEW.IP_ADD
||'|'|| :NEW.RID   ||'|'|| :NEW.CREATED_ON   ;

 INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);


  END ;
/


