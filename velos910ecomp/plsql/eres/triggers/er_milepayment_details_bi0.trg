CREATE OR REPLACE TRIGGER er_milepayment_details_BI0 BEFORE INSERT ON ER_MILEPAYMENT_DETAILS
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
 erid NUMBER(10);
 usr VARCHAR(2000);
 raid NUMBER(10);
 insert_data CLOB;
BEGIN
    usr := Getuser(:NEW.creator);


 SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;

  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  Audit_Trail.record_transaction(raid, 'er_milepayment_details',erid, 'I',usr);

 insert_data:= :NEW.pk_mp_details ||'|'|| :NEW.fk_milepayment ||'|'|| :NEW.mp_linkto_type ||'|'|| :NEW.mp_linkto_id||'|'|| :NEW.mp_amount
  ||'|'|| :NEW.CREATOR ||'|'|| :NEW.IP_ADD ||'|'|| :NEW.RID   ||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)  || '|'||:new.FK_MILEACHIEVED  ;

 INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);

  END ;
/


