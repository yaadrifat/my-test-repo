CREATE OR REPLACE TRIGGER ER_CATLIB_AU0
  after update of
  pk_catlib,
  fk_account,
  catlib_type,
  catlib_name,
  catlib_desc,
  record_type,
  last_modified_by,
  last_modified_date,
  ip_add,
  catlib_subtype,
  rid
  ON ER_CATLIB   for each row
declare
  raid number(10);

   usr varchar2(100);

   old_modby varchar2(100);

   new_modby varchar2(100);

begin
  select seq_audit.nextval into raid from dual;

   usr := getuser(:new.last_modified_by);

  audit_trail.record_transaction
    (raid, 'ER_CATLIB', :old.rid, 'U', usr);

  if nvl(:old.pk_catlib,0) !=
     NVL(:new.pk_catlib,0) then
     audit_trail.column_update
       (raid, 'PK_CATLIB',
       :old.pk_catlib, :new.pk_catlib);
  end if;
  if nvl(:old.fk_account,0) !=
     NVL(:new.fk_account,0) then
     audit_trail.column_update
       (raid, 'FK_ACCOUNT',
       :old.fk_account, :new.fk_account);
  end if;
  if nvl(:old.catlib_type,' ') !=
     NVL(:new.catlib_type,' ') then
     audit_trail.column_update
       (raid, 'CATLIB_TYPE',
       :old.catlib_type, :new.catlib_type);
  end if;
  if nvl(:old.catlib_name,' ') !=
     NVL(:new.catlib_name,' ') then
     audit_trail.column_update
       (raid, 'CATLIB_NAME',
       :old.catlib_name, :new.catlib_name);
  end if;
  if nvl(:old.catlib_desc,' ') !=
     NVL(:new.catlib_desc,' ') then
     audit_trail.column_update
       (raid, 'CATLIB_DESC',
       :old.catlib_desc, :new.catlib_desc);
  end if;
  if nvl(:old.record_type,' ') !=
     NVL(:new.record_type,' ') then
     audit_trail.column_update
       (raid, 'RECORD_TYPE',
       :old.record_type, :new.record_type);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ip_add,' ') !=
     NVL(:new.ip_add,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
  end if;
  if nvl(:old.catlib_subtype,' ') !=
     NVL(:new.catlib_subtype,' ') then
     audit_trail.column_update
       (raid, 'CATLIB_SUBTYPE',
       :old.catlib_subtype, :new.catlib_subtype);
  end if;
 if nvl(:old.LAST_MODIFIED_BY,0) !=
 NVL(:new.LAST_MODIFIED_BY,0) then
	Begin
		Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
		into old_modby from er_user  where pk_user = :old.last_modified_by ;
		Exception When NO_DATA_FOUND then
			old_modby := null;
	End ;
	Begin
		Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
		into new_modby   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
		Exception When NO_DATA_FOUND then
			new_modby := null;
	 End ;
	audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
 end if;

end;
/


