CREATE OR REPLACE TRIGGER ER_INVOICE_BI0 BEFORE INSERT ON ER_INVOICE
FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
 erid NUMBER(10);
 usr VARCHAR(2000);
 raid NUMBER(10);
 insert_data CLOB;
BEGIN
    usr := Getuser(:NEW.creator);

 SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;

  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  Audit_Trail.record_transaction(raid, 'er_invoice',erid, 'I',usr);

 insert_data:= :NEW.pk_invoice ||'|'|| :NEW.inv_number ||'|'|| :NEW.inv_addressedto ||'|'|| :NEW.inv_sentFrom ||'|'|| :NEW.inv_header
 ||'|'|| :NEW.inv_footer   ||'|'|| :NEW.inv_milestatustype ||'|'|| :NEW.inv_intervaltype
 ||'|'|| TO_CHAR(:NEW.inv_intervalfrom  ,PKG_DATEUTIL.f_get_dateformat) ||'|'|| TO_CHAR(:NEW.inv_intervalto  ,PKG_DATEUTIL.f_get_dateformat)
 ||'|'|| TO_CHAR(:NEW.inv_date  ,PKG_DATEUTIL.f_get_dateformat)
 ||'|'|| :NEW.inv_notes ||'|'|| :NEW.int_acc_num  ||'|'|| :NEW.inv_other_user ||'|'|| :NEW.CREATOR ||'|'|| :NEW.IP_ADD ||'|'|| :NEW.RID   ||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.inv_payunit||'|'||
 :NEW.fk_study||'|'|| :NEW.fk_site||'|'|| :NEW.inv_payment_dueby||'|'||	:NEW.inv_status ;  --KM

INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);

END;
/


