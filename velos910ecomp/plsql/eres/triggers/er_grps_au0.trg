CREATE OR REPLACE TRIGGER ER_GRPS_AU0 AFTER UPDATE ON ER_GRPS FOR EACH ROW
declare
  raid number(10);
  old_modby varchar2(100) ;
  new_modby varchar2(100) ;
  old_account varchar2(30) ;
  new_account varchar2(30) ;
  usr varchar2(100);
begin
  select seq_audit.nextval into raid from dual;

  usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_GRPS', :old.rid, 'U', usr);
  if nvl(:old.pk_grp,0) !=
     NVL(:new.pk_grp,0) then
     audit_trail.column_update
       (raid, 'PK_GRP',
       :old.pk_grp, :new.pk_grp);
  end if;
  if nvl(:old.fk_account,0) !=
     NVL(:new.fk_account,0) then
    Begin
     select ac_name into old_account
     from er_account where pk_account = :old.fk_account ;
    Exception When NO_DATA_FOUND then
     old_account := null ;
    End ;
    Begin
     select ac_name into new_account
     from er_account where pk_account = :new.fk_account ;
    Exception When NO_DATA_FOUND then
     new_account := null ;
    End ;
     audit_trail.column_update
       (raid, 'FK_ACCOUNT',
 old_account, new_account);
  end if;
  if nvl(:old.grp_name,' ') !=
     NVL(:new.grp_name,' ') then
     audit_trail.column_update
       (raid, 'GRP_NAME',
       :old.grp_name, :new.grp_name);
  end if;
  if nvl(:old.grp_desc,' ') !=
     NVL(:new.grp_desc,' ') then
     audit_trail.column_update
       (raid, 'GRP_DESC',
       :old.grp_desc, :new.grp_desc);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
    Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into old_modby
     from er_user
     where pk_user = :old.last_modified_by ;
    Exception When NO_DATA_FOUND then
     old_modby := null ;
    End ;
    Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into new_modby
     from er_user
     where pk_user = :new.last_modified_by ;
    Exception When NO_DATA_FOUND then
     new_modby := null ;
    End ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:old.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.grp_rights,0) !=
     NVL(:new.grp_rights,0) then
     audit_trail.column_update
       (raid, 'GRP_RIGHTS',
       :old.grp_rights, :new.grp_rights);
  end if;
  if nvl(:old.IP_ADD,' ') !=
     NVL(:new.IP_ADD,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.IP_ADD, :new.IP_ADD);
  end if;
    if nvl(:old.grp_supusr_rights,0) !=
     NVL(:new.grp_supusr_rights,0) then
     audit_trail.column_update
       (raid, 'grp_supusr_rights',
       :old.grp_supusr_rights, :new.grp_supusr_rights);
  end if;
  if nvl(:old.GRP_SUPUSR_FLAG,0) !=
     NVL(:new.GRP_SUPUSR_FLAG,0) then
     audit_trail.column_update
       (raid, 'GRP_SUPUSR_FLAG',
       :old.GRP_SUPUSR_FLAG, :new.GRP_SUPUSR_FLAG);
  end if;

  --Added by Manimaran for Enh.#U11
  if nvl(:old.GRP_HIDDEN,0) !=
     NVL(:new.GRP_HIDDEN,0) then
     audit_trail.column_update
       (raid, 'GRP_HIDDEN',
       :old.GRP_HIDDEN, :new.GRP_HIDDEN);
  end if;
  
  
  if nvl(:old.FK_CODELST_ST_ROLE,0) !=
     NVL(:new.FK_CODELST_ST_ROLE,0) then
     audit_trail.column_update
       (raid, 'FK_CODELST_ST_ROLE',
       :old.FK_CODELST_ST_ROLE, :new.FK_CODELST_ST_ROLE);
  end if;
  
end;
/


