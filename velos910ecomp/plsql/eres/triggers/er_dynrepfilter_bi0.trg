CREATE OR REPLACE TRIGGER "ER_DYNREPFILTER_BI0" BEFORE INSERT ON ER_DYNREPFILTER
FOR EACH ROW
DECLARE
      raid NUMBER(10);
         erid NUMBER(10);
        usr VARCHAR(2000);
         insert_data CLOB;

     BEGIN
    BEGIN
        SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||',' || usr_firstname INTO usr FROM er_user
      WHERE pk_user = :NEW.creator ;
 EXCEPTION
WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;

             SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;


  audit_trail.record_transaction(raid, 'ER_DYNREP',erid, 'I',usr);
         insert_data:= :NEW.PK_DYNREPFILTER||'|'|| :NEW.FK_DYNREP||'|'||:NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||
TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||:NEW.IP_ADD||'|'||:NEW.DYNREPFILTER_NAME||'|'||:NEW.FK_FORM||'|'||:NEW.FK_PARENTFILTER||'|'||:NEW.FORM_TYPE||'|'||:NEW.DATERANGE_TYPE||'|'||:NEW.DATERANGE_FROM||'|'||:NEW.DATERANGE_TO;

INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END ;
/


