CREATE OR REPLACE TRIGGER "ER_CATLIB_AD0"
-- Added by Ganapathy on 28/06/05 for Audit Delete
AFTER UPDATE
  --DELETE -- will not actually be deleted, just marked as D
  ON ER_CATLIB REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);
  rtype CHAR;
BEGIN
  rtype:=:NEW.record_type;
-- Added by Ganapathy on 28/06/05 for Audit Delete
 IF(rtype='D') THEN

  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'ER_CATLIB', :OLD.rid, 'D');

  deleted_data :=
  TO_CHAR(:OLD.pk_catlib) || '|' ||
  TO_CHAR(:OLD.fk_account) || '|' ||
  :OLD.catlib_type || '|' ||
  :OLD.catlib_name || '|' ||
  :OLD.catlib_desc || '|' ||
  :OLD.record_type || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add || '|' ||
  :OLD.catlib_subtype;

INSERT INTO audit_delete
(raid, row_data) VALUES (raid, deleted_data);
END IF;
END;
/


