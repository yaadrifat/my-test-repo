CREATE OR REPLACE TRIGGER "ER_PATFORMS_AD0" 
AFTER UPDATE--DELETE
ON ER_PATFORMS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);
  usr VARCHAR(2000);
  rtype CHAR;

BEGIN
  rtype:=:NEW.record_type;
-- Added by Ganapathy on 28/06/05 for Audit Delete
 IF(rtype='D') THEN

 usr := Getuser(:new.last_modified_by);

  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  Audit_Trail.record_transaction
    (raid, 'ER_PATFORMS', :OLD.rid, 'D',usr);

  deleted_data :=
  TO_CHAR(:OLD.pk_patforms) || '|' ||
  TO_CHAR(:OLD.fk_formlib) || '|' ||
  TO_CHAR(:OLD.fk_per) || '|' ||
  TO_CHAR(:OLD.fk_patprot) || '|' ||
  TO_CHAR(:OLD.patforms_filldate) || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.form_completed) || '|' ||
  TO_CHAR(:OLD.notification_sent) || '|' ||
  TO_CHAR(:OLD.process_data) || '|' ||
  TO_CHAR(:OLD.fk_formlibver) || '|' ||
  :OLD.record_type || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.isvalid) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add;

INSERT INTO AUDIT_DELETE
(raid, row_data) VALUES (raid, deleted_data);
END IF;
END;
/


