CREATE OR REPLACE TRIGGER "ER_ACCOUNT_BI0" 
BEFORE INSERT
ON ER_ACCOUNT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  USR VARCHAR2(100);

BEGIN

  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  Audit_Trail.record_transaction
    (raid, 'ER_ACCOUNT', erid, 'I', 'New User'  );
END;
/


