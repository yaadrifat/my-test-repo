CREATE OR REPLACE TRIGGER "ER_ER_FORMSTAT_BU_LM" 
BEFORE UPDATE ON ER_FORMSTAT REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
begin
:new.last_modified_date := sysdate ;
end;
/


