CREATE OR REPLACE TRIGGER "ER_MILEPAYMENT_AD0" 
AFTER DELETE
ON ER_MILEPAYMENT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);
begin
  select seq_audit.nextval into raid from dual;
  audit_trail.record_transaction
    (raid, 'ER_MILEPAYMENT', :old.rid, 'D');
  deleted_data :=
  to_char(:old.pk_milepayment) || '|' ||
  to_char(:old.fk_study) || '|' ||
  to_char(:old.fk_milestone) || '|' ||
  to_char(:old.milepayment_date) || '|' ||
  to_char(:old.milepayment_amt) || '|' ||
  :old.milepayment_desc || '|' ||
  :old.milepayment_delflag || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.ip_add;
insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


