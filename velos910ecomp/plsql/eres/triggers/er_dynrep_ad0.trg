CREATE OR REPLACE TRIGGER ER_DYNREP_AD0 AFTER DELETE ON ER_DYNREP
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);

begin
  select seq_audit.nextval into raid from dual;
audit_trail.record_transaction
    (raid, 'ER_DYNREP', :old.rid, 'D');
--Created by Manimaran for Audit delete
deleted_data :=
to_char(:old.PK_DYNREP) || '|' ||
to_char(:old.FK_FORM) || '|' ||
:old.DYNREP_NAME || '|' ||
:old.DYNREP_DESC || '|' ||
substr(:old.DYNREP_FILTER,1, 600 ) || '|' ||
substr(:old.DYNREP_ORDERBY, 1, 600) || '|' ||
to_char(:old.FK_USER) || '|' ||
to_char(:old.FK_ACCOUNT) || '|' ||
substr(:old.DYNREP_HDRNAME, 1, 600) || '|' ||
substr(:old.DYNREP_FTRNAME, 1, 600) || '|' ||
to_char(:old.RID) || '|' ||
to_char(:old.CREATOR) || '|' ||
to_char(:old.LAST_MODIFIED_BY) || '|' ||
to_char(:old.LAST_MODIFIED_DATE) || '|' ||
to_char(:old.CREATED_ON) || '|' ||
:old.IP_ADD || '|' ||
:old.REPORT_TYPE || '|' ||
to_char(:old.FK_STUDY) || '|' ||
to_char(:old.FK_PERSON) || '|' ||
:old.DYNREP_SHAREWITH || '|' ||
:old.DYNREP_TYPE || '|' ||
:old.DYNREP_FORMLIST || '|' ||
to_char(:old.DYNREP_USEUNIQUEID) || '|' ||
to_char(:old.DYNREP_USEDATAVAL);

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


