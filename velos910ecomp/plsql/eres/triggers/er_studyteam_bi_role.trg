CREATE OR REPLACE TRIGGER "ER_STUDYTEAM_BI_ROLE" 
BEFORE INSERT
ON ER_STUDYTEAM
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
Declare
  v_rights varchar2(200);
  v_defrights varchar2(100);
begin
 BEGIN

--get the rights from control table according to role, only for new study team members
--while study copy, the team rights have to be copied as such so no need to get from control table
if :new.STUDY_TEAM_RIGHTS IS NULL then
  for i in ( select CTRL_VALUE   from er_ctrltab  where CTRL_KEY ='study_rights'
          order by CTRL_SEQ )
   LOOP
      v_defrights := v_defrights || '0';
   END LOOP;

    if :new.FK_CODELST_TMROLE > 0 then -- get rights according to selected role
      Select nvl(ct.ctrl_value,v_defrights)
      into v_rights
      from er_ctrltab ct, er_codelst cd
      Where cd.pk_codelst = :new.FK_CODELST_TMROLE and
      trim(cd.codelst_subtyp) = trim(ct.ctrl_key) ;

     :new.STUDY_TEAM_RIGHTS := v_rights;
    end if;

end if ;

    Exception When NO_DATA_FOUND then
      v_rights:= v_defrights;
    END ;

end;
/


