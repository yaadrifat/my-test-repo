CREATE OR REPLACE PROCEDURE        "SP_AUTOGEN_STUDY" (p_account number, p_studynum out varchar2 )
as
v_studynumber varchar2(100);
v_sql varchar2(4000);
v_last_id number;
Begin
    select studynum_autogen_sql,(studynum_lastid + 1) as studynum_lastid into v_sql,v_last_id from er_account_settings where fk_account = p_account;
    update er_account_settings set studynum_lastid = v_last_id  where fk_account = p_account;
    execute immediate v_sql into v_studynumber;
    p_studynum := v_studynumber || v_last_id;
End ;
/


