CREATE OR REPLACE PROCEDURE        "SP_COPYSTUDY" (
   p_study IN NUMBER,
   p_xuser IN NUMBER,
   p_author IN NUMBER,
   p_permgranted IN CHAR,
   p_msgcntr IN NUMBER,
   p_newstudy OUT NUMBER,
   p_cursor OUT TYPES.cursortype
   )
AS
   /****************************************************************************************************
   ** Procedure to copy a study for an external user in case of Modify permission Granted and copy the
   ** user to study team in case of view permission. This will copy the study details, sections and appendix
   ** Parameter Description
   ** p_study 	the Study which has to be copied for the external user
   ** p_xuser 	the uexternal user, user to whom the copy is being given
   ** p_author the author of the study, user from whom the copy is requested
   ** p_permgranted the permission, granted by the author of the study. Permission can be 'M' (Modify),
   ** 'V' (View) or 'D' (Deny)
   ** p_newstusy 	the identificaition of the new study for the external user
   ** p_dt 	the date in mmddyyhhmiss format which will be appended to the filename for storing in the DB/app
   ** p_cursor the files names from study apndx is passed from this ref cursor
   ** If Permission = 'M' Then
   ** Delete the user from the study team (in case user has already given a view permission)
   ** Make a copy of the complete study. Copy all the records for the orignal study for
   ** the new xternal user
   ** Update the permission in message center as 'M' for the request
   ** Change all the previous permissions (in Message Center) given to the external user as 'D'
   ** Insert an acknowledgement for the external user in Message center.
   ** The MSGCNTR_STAT column will store the requested
   ** permission and the MSGCNTR_PERM will contain the granted premission
   **
   ** If Permission = 'V' Then
   ** Add external user to the study team of the study
   ** Update the permission in message center as 'V'
   ** Insert an acknowledgement for the external user in Message center.
   ** The MSGCNTR_STAT column will store the requested permission
   **
   ** If Permission = 'D' Then
   ** Update the permission in message center as 'D'
   ** Insert an acknowledgement for the external user in Message center. The MSGCNTR_STAT column will store the requested
   ** permission
   **
   ** p_msgcntr - this is the id of the request message from  the external user
   **
   ** Author: Charanjiv S Kalha 27th April 2001
   **
   ** Modification History
   **
   ** Modified By         Date     		Remarks
   **
   ** Deepali Sachdeva	1st May 2001		Changed for adding permission with the copy
   ** Charanjiv 	     2nd May 2001 		Insert SQL for er_xusright table added for V premission along
   **								with the Study team rights
   **								Corrected the UPDATE spelling :)) Deeps !!!
   ** Charanjiv 		3rd May 2001 		Inserted the external user in the study team of new study when request is for 'M'
   **								Changed the Insert for acknowledgment column MSGCNTR_STAT will contain the
   **								requested permission and the MSGCNTR_PERM column will contain the granted
   **								premission.
   **								Added check for msgcntr_perm to be null when updating for View permission.
   **								This is needed if the user has requested for both View and Modify premission
   **								and the modify request was processed first.
   ** Charanjiv 		5th May 2001 		The OUT parameter was changed from p_dt (date format of study appendix files
   **								to a ref cursor (p_cursor from the package TYPES). This cursor will return the
   **								old and new files names of the appendix (studyapndx_type = 'file')
   ** Charanjiv          29th May 2001       Put in check for msgcntr is. Copying the study for a non-public study
   **                                        the msgcntr id will be zero. And all dml for msgcntr tables will be skiped.
   ** Charanjiv          25th July 2001      Changed the rights string generation, when the copy is with 'M'odify
   **                                        right the STUDYTEAM right will be with 'V'iew (4) only
   ** Charanjiv          14th Aug  2001      study_pubcollst - the public column list has increased to 5, thus the
   **                                        insert statement for Modified permission has been changed to '00000'
   ** Charanjiv          17th Aug 2001       Changed the process for the VIEW permission to incorporate the STUDY VIEW
   **                                        Concept. Instead of putting the ext user in the study team now a VIEW
   **                                        of the study is give to him in table ER_STUDYVIEW.
   ** Charanjiv          23rd Aug 2001       Added the copy protocol procedure.
   ** Charanjiv          26th Sep 2001       Added an extra column in the msg cntr table which references
   **                                        the study view table. This column will point to the studyview
   **                                        given to the user through which message.
   ** Charanjiv          12th Oct 20001      Added the column STUDY_VER_NO to the table ER_STUDYVIEW, the
   **                                        corresponding INSERT sql in the proc changed. Ver No got from
   **                                        study table
   ** Known Bug List
   ** Double view permissions                If the xternal user has view permission and requests for
   **                                        Modify premission and the Admin gives him only view. The procedure
   **                                        will add another record in the study team for the same user.
   **
   ** Sonia Sahni         28 Aug 2002        Assign the team rights selectively
   ** Sonia Sahni         10 Sept 2002       Select study currency for new study
   ** Sonia Sahni         27 Sept 2002       Assign the team rights selectively - for "create a new version"
   ** Sonika Talwar       16 Jan 2003        Change section and appendix copy, link them with study version
    ** Modified by Sonia Sahni 9th Aug 04 to add a default version status history record
   *****************************************************************************************************
   */
   v_newstudy                    NUMBER;
   v_dt                          CHAR(12);
   v_account                     NUMBER;
   v_cnt                         NUMBER;
   v_rights                      VARCHAR2(20);
   v_req			             VARCHAR2(10);
   v_right                       VARCHAR2(200) ;
   -- v_studynum                    VARCHAR2(20) ;
      v_studynum                    VARCHAR2(100) ;--JM: 120606
   v_studytitle                  VARCHAR2(1000) ;
   v_studyview                   NUMBER ;
   v_rtstr                       VARCHAR2(2) ;
   v_newstudyver                 NUMBER;
   v_studyver                    NUMBER;

   CURSOR cur_study_rigth IS
   SELECT trim(CTRL_VALUE)
     FROM er_ctrltab
    WHERE CTRL_KEY ='study_rights'
 ORDER BY CTRL_SEQ DESC  ;
BEGIN
 DBMS_OUTPUT.PUT_LINE ('Study to copy ' || TO_CHAR(p_study) ) ;

   -- the DateTime format that will be appended to the file names in the Study Appendix where
   -- the apndx type is 'file'. This is an OUT parameter
   SELECT TO_CHAR (SYSDATE, 'DDMMYYHHMISS')
     INTO v_dt
     FROM dual;

   IF p_msgcntr > 0 THEN
     BEGIN
	    -- Get the requested premission from the msgcntr. This will be used in the acknowledgment sent back to the user.
   SELECT UPPER(trim(msgcntr_reqtype))
     INTO v_req
     FROM er_msgcntr
    WHERE pk_msgcntr = p_msgcntr;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         RAISE_APPLICATION_ERROR (
            -20000,
            'No Permission requested for this message. Pl. Check with Sys Admin'
         );
      WHEN OTHERS THEN
         RAISE_APPLICATION_ERROR (
            -20000,
            'Unhandled Oracle error in requested premission ' || SUBSTR (SQLERRM, 1, 100)
         );
   END;
  END IF ;

	BEGIN
      -- This will give the account id of the external user, which can be used for all INSERT
      SELECT fk_account
        INTO v_account
        FROM er_user
       WHERE pk_user = p_xuser;
   -- Exception for when their is not account information or anyother possible problem
   -- with the statement
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         RAISE_APPLICATION_ERROR (
            -20000,
            'The External user does not exist, pl. check'
         );
      WHEN OTHERS THEN
         RAISE_APPLICATION_ERROR (
            -20000,
            'Unhandled Oracle error in getting account info ' || SUBSTR (SQLERRM, 1, 100)
         );
   END;
    DBMS_OUTPUT.PUT_LINE ('Permission ' || p_permgranted ) ;
   -- In case of Modify 'M' Permission
   IF  UPPER(trim(p_permgranted )) = 'M' THEN

   -- The new study id generated from the seq_er_study sequence. This is stored in the p_newstudy
   -- variable to be used for all the INSERT/SELECT. This is an OUT parameter
   SELECT seq_er_study.NEXTVAL
     INTO p_newstudy
     FROM dual;
   DBMS_OUTPUT.PUT_LINE ('new study  ' || TO_CHAR(p_newstudy) ) ;

      BEGIN
         -- Delete the External user from study team
         DELETE
           FROM er_studyteam
          WHERE fk_study = p_study
            AND fk_user = p_xuser;
         -- Make a Copy of the study
         --     These columns have ben updated with the same values for all INSERTs
         --	CREATOR                 the external user id is updated here
         --	LAST_MODIFIED_BY        the external user id is updated here
         --	LAST_MODIFIED_DATE      the current sysdate
         --	CREATED_ON              the current sysdate
         -- The first insert, to copy the study details
         --	PK_STUDY		is geneated
         --	FK_SITE_SPONSOR		the site sponsor is updated to null
         --	FK_ACCOUNT              the account id is updated to the account of the xternal user
         --	FK_USER_CONTACT         the user contact is nullified
         --	STUDY_PARENTID          the study parent is the id of the original study
         --	FK_AUTHOR               the author of the new study is the external user
         BEGIN
           INSERT INTO er_study (
                        pk_study,
                        study_sponsor,
                        fk_account,
                        study_contact,
                        study_pubflag,
                        study_title,
                        study_obj_clob,
                        study_sum_clob,
                        study_prodname,
                        study_samplsize,
                        study_dur,
                        study_durunit,
                        study_estbegindt,
                        study_actualdt,
                        study_prinv,
                        study_partcntr,
                        study_keywrds,
                        study_pubcollst,
                        study_parentid,
                        fk_author,
                        fk_codelst_tarea,
                        fk_codelst_phase,
                        fk_codelst_blind,
                        fk_codelst_random,
                        creator,
                        study_current,
                        last_modified_by,
                        last_modified_date,
                        created_on,
                        study_number,
                        fk_codelst_restype,
						FK_CODELST_CURRENCY,
					   	study_advlkp_ver,
						study_maj_auth,
						study_disease_site,
						fk_codelst_scope,
						study_assoc
                     )
           SELECT p_newstudy,
                    NULL,
                    v_account,
                    NULL,
                    'N',
                    study_title,
                    study_obj_clob,
                    study_sum_clob,
                    study_prodname,
                    study_samplsize,
                    study_dur,
                    study_durunit,
                    study_estbegindt,
                    NULL,
                    study_prinv,
                    study_partcntr,
                    study_keywrds,
                    '00000',
                    p_study,
                    p_xuser,
                    fk_codelst_tarea,
                    fk_codelst_phase,
                    fk_codelst_blind,
                    fk_codelst_random,
                    p_xuser,
                    study_current,
                    p_xuser,
                    SYSDATE,
                    SYSDATE,
                    study_number,
                    fk_codelst_restype,
				FK_CODELST_CURRENCY,
				study_advlkp_ver,
				study_maj_auth,
				study_disease_site,
				fk_codelst_scope,
				study_assoc
           FROM er_study
           WHERE pk_study = p_study;
             IF SQL%FOUND THEN
    	           DBMS_OUTPUT.PUT_LINE ('Insert into study team ' || TO_CHAR(1)) ;
             ELSE
    	           DBMS_OUTPUT.PUT_LINE ('Error in sharing study. ' ) ;
              RAISE_APPLICATION_ERROR (
            -20000,
            'Error in sharing study. If error persists Pl. Check with Sys Admin'
         );
	     END IF;
          END ;

	    --insert all versions of study

	      FOR i IN (SELECT PK_STUDYVER
		       FROM er_studyver
		       WHERE fk_study=p_study) LOOP
          BEGIN
		  p('study ver no ' || i.pk_studyver);
		  v_studyver := i.pk_studyver;
            -- The new study version id generated from the seq_er_studyver sequence.
            -- This is stored in the v_newstudyver variable to be used for all the INSERT/SELECT.
            SELECT seq_er_studyver.NEXTVAL
            INTO v_newstudyver
            FROM dual;
            DBMS_OUTPUT.PUT_LINE ('new study  ver' || TO_CHAR(v_newstudyver) ) ;


         BEGIN
          INSERT INTO ER_STUDYVER
            (PK_STUDYVER,FK_STUDY,STUDYVER_NUMBER,
            STUDYVER_STATUS,STUDYVER_NOTES,ORIG_STUDY,
            CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
              CREATED_ON)
          SELECT v_newstudyver,p_newstudy,STUDYVER_NUMBER,
	           'W',STUDYVER_NOTES,ORIG_STUDY,
			   p_xuser,p_xuser,SYSDATE,
               SYSDATE
	   	FROM er_studyver
		WHERE pk_studyver=v_studyver;

		-- insert record into er_status_history for version history

	    INSERT INTO ER_STATUS_HISTORY( PK_STATUS ,
					STATUS_MODPK ,	STATUS_MODTABLE,
					FK_CODELST_STAT ,
					STATUS_DATE ,
					CREATOR , RECORD_TYPE, CREATED_ON,STATUS_ENTERED_BY)

        VALUES ( seq_er_status_history.NEXTVAL,
			   v_newstudyver,'er_studyver',
			   pkg_util.f_getCodePk('W','versionStatus'),
			   SYSDATE,
			   p_xuser,'N',SYSDATE,p_xuser);


          IF SQL%FOUND THEN
             DBMS_OUTPUT.PUT_LINE ('Insert into study version ' || TO_CHAR(1)) ;
          ELSE
             DBMS_OUTPUT.PUT_LINE ('Error in sharing study version. ' ) ;
             RAISE_APPLICATION_ERROR (
            -20000,
            'Error in sharing study version. If error persists Pl. Check with Sys Admin'
          );
         END IF;
	   END;

         -- The second insert for all the study sections
         -- 	PK_STUDYSEC            the Primary key is generated
         --	FK_STUDYVER            the version id is updated with the new version id
        BEGIN
         INSERT INTO er_studysec (
                        pk_studysec,
                        fk_study,
                        studysec_name,
				    studysec_num,
                        studysec_text,
                        studysec_pubflag,
                        studysec_seq,
                        creator,
                        last_modified_by,
                        last_modified_date,
                        created_on,
				    fk_studyver
                     )
             SELECT seq_er_studysec.NEXTVAL,
                    NULL,
                    studysec_name,
				studysec_num,
                    studysec_text,
                    'N',
                    studysec_seq,
                    p_xuser,
                    p_xuser,
                    SYSDATE,
                    SYSDATE,
				v_newstudyver
               FROM er_studysec
              WHERE fk_studyver = v_studyver;
         IF SQL%FOUND THEN
          DBMS_OUTPUT.PUT_LINE ('Insert into study section ' || TO_CHAR(1)) ;
         ELSE
          DBMS_OUTPUT.PUT_LINE ('Error in sharing study section. ' ) ;
          /*raise_application_error (
            -20000,
            'Error in sharing study section. If error persists Pl. Check with Sys Admin'
         );*/
        END IF;
      END;

         -- 	PK_STUDYAPNDX           the pk is generated from seq_er_studyapndx
         -- 	FK_STUDY                the new study id is updated here
         -- 	STUDYAPNDX_FILE         the FILE name is changed for all appendix where type is 'file'
         -- 				a 'ddmmyyhhmiss' format has been appended to the file name
        BEGIN
         INSERT INTO er_studyapndx (
                        pk_studyapndx,
                        fk_study,
                        studyapndx_uri,
                        studyapndx_desc,
                        studyapndx_pubflag,
                        creator,
                        last_modified_by,
                        last_modified_date,
                        created_on,
                        studyapndx_file,
                        studyapndx_type,
				    studyapndx_fileobj,
                        studyapndx_filesize,
				    fk_studyver
                     )
             SELECT seq_er_studyapndx.NEXTVAL,
                    NULL,
                    studyapndx_uri,
                    studyapndx_desc,
                    'N',
                    p_xuser,
                    p_xuser,
                    SYSDATE,
                    SYSDATE,
                    DECODE (
                       trim (studyapndx_type),
                       'file', SUBSTR (
			          studyapndx_uri,
                                  1,
                                   (INSTR (studyapndx_uri, '.', -1) - 1)
                               ) ||
                               v_dt ||
                               SUBSTR (
                                  studyapndx_uri,
                                  INSTR (studyapndx_uri, '.', -1)
                               ),
                       studyapndx_uri
                    ),
                    studyapndx_type,
                    studyapndx_fileobj,
                    studyapndx_filesize,
 			     v_newstudyver
               FROM er_studyapndx
              WHERE fk_studyver = v_studyver ;

         IF SQL%FOUND THEN
          DBMS_OUTPUT.PUT_LINE ('Insert into study appendix ' || TO_CHAR(1)) ;
         ELSE
          DBMS_OUTPUT.PUT_LINE ('Error in sharing study appendix. ' ) ;
          /*raise_application_error (
            -20000,
            'Error in sharing study appendix. If error persists Pl. Check with Sys Admin' ); */
        END IF;
	  END;
     END;
	END LOOP;

	-- Insert the external user in the study team for rhe new study
		-- Get the no of features from control table and create a string for the modify, view  and new rights '7'
		-- except for STUDYTEAM which will reman  '4' (View)
		 OPEN cur_study_rigth ;
            LOOP
             FETCH cur_study_rigth INTO v_right ;
              EXIT WHEN cur_study_rigth%NOTFOUND  ;

		    v_rtstr := '7';

               IF UPPER(v_right) = 'STUDYTEAM' OR UPPER(v_right) = 'STUDYREP'  THEN
                 v_rtstr := '4';
                 --v_rights := '4' || v_rights;
			 END IF ;

                IF UPPER(v_right) = 'STUDYSUM' OR UPPER(v_right) = 'STUDYVPDET'  THEN
			   v_rtstr := '6';
                 --v_rights := '6' || v_rights;
     	      END IF ;

               IF UPPER(v_right) = 'STUDYNOTIFY'  OR UPPER(v_right) = 'STUDYVER'  THEN
                 v_rtstr := '5';
		       --v_rights := '5' || v_rights;
         	    END IF ;

		    v_rights := v_rtstr || v_rights;

            END LOOP;

	    -- Insert a record in the study team for the new (copied) study with the external user as a team member,
	    -- with modify and view rigths
         INSERT INTO er_studyteam (
                        pk_studyteam,
                        fk_user,
                        fk_study,
                        study_team_rights,
						study_team_usr_type
                     )
         VALUES (
			seq_er_studyteam.NEXTVAL,
			p_xuser,
			p_newstudy,
			v_rights,
			'D'
		      );

         -- Update all the previous permissions to 'D'. This is done so that for one study the external user will have
	    -- only one record with the granted permission 'V' or 'M'
       IF p_msgcntr > 0 THEN
         UPDATE er_msgcntr
            SET msgcntr_perm = 'D',
		      msgcntr_stat = 'R'
          WHERE fk_userx = p_xuser
            AND fk_study = p_study;

         --Update the Permission in Message Center to 'M' and Status to Read 'R'
         UPDATE er_msgcntr
            SET msgcntr_perm = 'M',
                msgcntr_stat = 'R'
          WHERE pk_msgcntr = p_msgcntr;
         -- Inserting a new row in Message center for sending an acknowledgemnet to the  External users
         INSERT INTO er_msgcntr (
                        pk_msgcntr,
                        fk_study,
                        fk_userx,
                        fk_userto,
                        msgcntr_text,
                        msgcntr_reqtype,
                        msgcntr_stat,
                        msgcntr_perm
                     )
              VALUES (
                 seq_er_msgcntr.NEXTVAL,
                 p_study,
                 p_author,
                 p_xuser,
                 'You have been Granted a Copy of the Study ',
                 v_req,
                 'A',
                 'M'
              );
    END IF ;
    -- ADD the protocol copy procedure here
    -- This procedure is owned by the eSch schema, thus we will use the DB Link to call the proc

   sp_cpstudyprot (p_study, p_newstudy, p_xuser) ;

   END;
   END IF;
   -- In case of View 'V' Permission
   IF  (UPPER(trim(p_permgranted)) = 'V') THEN
    DBMS_OUTPUT.PUT_LINE ('In View part ') ;
      BEGIN
       SELECT seq_er_studyview.NEXTVAL
	    INTO v_studyview
 	    FROM dual ;

       BEGIN
        SELECT study_number,
               study_title
          INTO v_studynum ,
               v_studytitle
          FROM er_study
         WHERE pk_study = p_study ;

        INSERT INTO er_Studyview (
                      PK_STUDYVIEW           ,
                      FK_USER                ,
                      FK_STUDY               ,
                      STUDYVIEW_NUM          ,
                      STUDYVIEW_TITLE         ,
                      STUDYVIEW
                    )
                    (SELECT v_studyview,
                            p_xuser,
                            fk_study,
                            v_studynum ,
                            v_studytitle ,
                            STUDYVIEW
                       FROM er_Studyview
                      WHERE fk_study = p_study
                        AND fk_user IS NULL
                     ) ;
		 EXCEPTION
		 WHEN OTHERS THEN
         		RAISE_APPLICATION_ERROR (
            		-20000,
            		'Unhandled Oracle error ' || SUBSTR (SQLERRM, 1, 100)
         			);
         END ;

       IF p_msgcntr > 0 THEN
         --Update the Permission in Message Center to 'V' and Status to Read 'R'
         UPDATE er_msgcntr
            SET msgcntr_perm = 'V',
                msgcntr_stat = 'R',
			 fk_studyview = v_studyview
          WHERE pk_msgcntr = p_msgcntr
		  AND msgcntr_perm IS NULL;
         -- Inserting a new row in Message center for sending an acknowledgemnet to the External users
          DBMS_OUTPUT.PUT_LINE ('msg cntr requested prem ' || v_req ) ;
         INSERT INTO er_msgcntr (
                        pk_msgcntr,
                        fk_study,
                        fk_userx,
                        fk_userto,
                        msgcntr_text,
                        msgcntr_reqtype,
                        msgcntr_stat,
                        msgcntr_perm,
				    fk_studyview
                     )
              VALUES (
                 seq_er_msgcntr.NEXTVAL,
                 p_study,
                 p_author,
                 p_xuser,
                 'You have been Granted a View Permission on the Study',
                 v_req,
                 'A',
                 'V',
			  v_studyview
                   );
        END IF;
		EXCEPTION
		WHEN OTHERS THEN
         		RAISE_APPLICATION_ERROR (
            		-20000,
            		'Unhandled Oracle error ' || SUBSTR (SQLERRM, 1, 100)
         			);

    END;
   END IF;
   -- In case of Deny 'D'
   IF  UPPER(trim(p_permgranted)) = 'D' AND p_msgcntr > 0 THEN
     BEGIN
         --Update the Permission in Message Center to 'D' and Status to Read 'R'
         UPDATE er_msgcntr
            SET msgcntr_perm = 'D',
                msgcntr_stat = 'R'
          WHERE pk_msgcntr = p_msgcntr;
         -- Inserting a new row in Message center for sending an acknowledgemnet to the External users
         INSERT INTO er_msgcntr (
                        pk_msgcntr,
                        fk_study,
                        fk_userx,
                        fk_userto,
                        msgcntr_text,
                        msgcntr_reqtype,
                        msgcntr_stat,
                        msgcntr_perm
                     )
              VALUES (
                 seq_er_msgcntr.NEXTVAL,
                 p_study,
                 p_author,
                 p_xuser,
                 'The Permission has been Denied',
                 v_req,
                 'A',
                 'D'
              );
      END;
   END IF;
/**********************************
TEST
set serveroutput on
variable c refcursor
declare
ns number ;
dt char ;
begin
usage sp_copystudy (to_copy_study, xternal_user , author_of_study, perm_granted, req_msg_id , new_study_id OUT ,c OUT )
sp_copystudy(324,577,123,'M',123,ns,:c) ;
dbms_output.put_line(to_char(ns) ) ;
print c ;
end ;

**************************************/

END;
/


CREATE SYNONYM ESCH.SP_COPYSTUDY FOR SP_COPYSTUDY;


CREATE SYNONYM EPAT.SP_COPYSTUDY FOR SP_COPYSTUDY;


GRANT EXECUTE, DEBUG ON SP_COPYSTUDY TO EPAT;

GRANT EXECUTE, DEBUG ON SP_COPYSTUDY TO ESCH;

