CREATE OR REPLACE PROCEDURE        "SP_GET_NEXT_RECORDS" 
 (
  p_start  NUMBER,
  p_recs_per_page NUMBER,
  p_sql VARCHAR2,
  p_countsql VARCHAR2,
  o_res OUT Gk_Cv_Types.GenericCursorType,
  o_rows OUT NUMBER


 )
IS
-- Find out the first and last record we want
FirstRec NUMBER;
LastRec NUMBER;
V_POS      NUMBER         := 0;
V_FROM VARCHAR2(500);
--V_CTSELECT Varchar2(4000);
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'Sp_Get_Next_Records', pLEVEL  => Plog.LFATAL);

BEGIN

FirstRec := p_start - 1 ;

LastRec := p_start  +p_recs_per_page;

-- Now, return the set of paged records


BEGIN


OPEN o_res FOR 'select * from ( select a.*, rownum rnum
                                FROM (' || p_sql ||') a
						  WHERE ROWNUM < ' || LastRec || ' )
			 WHERE rnum > ' || FirstRec ;





  EXECUTE IMMEDIATE p_countsql INTO o_rows;

	EXCEPTION WHEN OTHERS THEN
	  PLOG.FATAL(PCTX,'Sp_Get_Page_Records SQLERRM :' || SQLERRM);
END	 ;

END Sp_Get_Next_Records;
/


