
SELECT disease_site,SUM(COUNT) AS COUNT FROM (
SELECT codelst_desc AS disease_site, 0 AS COUNT FROM ER_CODELST WHERE codelst_type = 'disease_site'
UNION
SELECT disease_site,SUM(COUNT) AS COUNT FROM
(
SELECT DECODE(INSTR(study_disease_site,','),0,(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = study_disease_site),'MULTIPLE') AS disease_site,
(SELECT COUNT(*) FROM ER_PATSTUDYSTAT WHERE patstudystat_date BETWEEN '~2' AND '~3' AND
fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'patStatus' AND codelst_subtyp = 'enrolled') AND fk_study = pk_study) AS COUNT
FROM ER_STUDY,ER_STUDYID
WHERE pk_study = fk_study AND fk_account = ~1 AND
fk_codelst_idtype = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'sum4_agent') AND studyId_id='Y'
)
GROUP BY disease_site)
GROUP BY disease_site
