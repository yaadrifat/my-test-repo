SELECT
study_number,study_title,TO_CHAR(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) AS study_actualdt,(SELECT site_name FROM ER_SITE WHERE pk_site = fk_site) AS site_name,
(SELECT patprot_patstdid FROM ER_PATPROT WHERE fk_per = pk_per AND fk_study = pk_study AND patprot_stat=1) AS pat_studyid,
(SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst = fk_codlst_aetype) AS TYPE,
ae_grade, ae_name ,
TO_CHAR(ae_stdate,PKG_DATEUTIL.F_GET_DATEFORMAT) AS ae_stdate,TO_CHAR(ae_enddate,PKG_DATEUTIL.F_GET_DATEFORMAT) AS ae_enddate,
(SELECT codelst_desc FROM ESCH.SCH_CODELST WHERE pk_codelst = ae_relationship) AS ae_rel,
DECODE(SUBSTR(ae_addinfo,(SELECT codelst_seq FROM ESCH.sch_codelst WHERE codelst_type = 'adve_info' AND codelst_subtyp = 'adv_dopped'),1),'1','Yes','NO') AS dropped,
DECODE(SUBSTR(ae_addinfo,(SELECT codelst_seq FROM ESCH.sch_codelst WHERE codelst_type = 'adve_info' AND codelst_subtyp = 'adv_unexp'),1),'1','Yes','NO') AS unexpected,
DECODE(SUBSTR(ae_addinfo,(SELECT codelst_seq FROM ESCH.sch_codelst WHERE codelst_type = 'adve_info' AND codelst_subtyp = 'adv_violation'),1),'1','Yes','NO') AS violation,
DECODE(TO_NUMBER(ae_outtype),0,'',f_getae_outcome(pk_adveve,TO_CHAR(ae_outdate,PKG_DATEUTIL.F_GET_DATEFORMAT))) AS outcome
FROM ESCH.sch_adverseve, ER_STUDY, ER_PER
WHERE pk_study = fk_study AND
pk_per = fk_per AND
fk_study = ~2 AND
fk_site = ~1 AND
AE_STDATE BETWEEN '~3'AND '~4'
ORDER BY pat_studyid,ae_stdate