select
 (SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study) AS study_number,
 (SELECT site_name FROM ER_SITE WHERE pk_site = fk_site) AS Org,
 TO_CHAR(studystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) AS valid_from,
 (SELECT usr_firstname || ' ' || usr_lastname FROM ER_USER WHERE pk_user = fk_user_docby)AS doc_by,
 codelst_desc,
 DECODE (current_stat,1,'Yes','') AS current_stat,
 TO_CHAR(studystat_validt,PKG_DATEUTIL.F_GET_DATEFORMAT) AS valid_to,
 trim(TO_CHAR (studystat_validt, 'MONTH YYYY')) AS MONTH,
 studystat_note
 FROM ER_CODELST, ER_STUDYSTAT, ER_STUDY, ER_SITE WHERE fk_codelst_studystat IN
(SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'studystat'
AND codelst_subtyp IN (SELECT ctrl_value FROM ER_CTRLTAB WHERE ctrl_key = 'irb_app_stat'))
AND pk_codelst = ER_STUDYSTAT.fk_codelst_studystat AND
ER_STUDYSTAT.fk_study = ER_STUDY.pk_study AND ER_STUDYSTAT.fk_site = ER_SITE.pk_site
AND  fk_study IN (:studyId)  AND
         fk_site IN (:orgId) AND
         studystat_validt BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
ORDER BY study_number, org
