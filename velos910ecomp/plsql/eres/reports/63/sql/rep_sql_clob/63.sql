select
a.ac_type,
        u.USR_LASTNAME,
        u.USR_FIRSTNAME,
        u.USR_WRKEXP,
        u.USR_PAHSEINV,
        s.SITE_NAME,
        sa.address site_add,
        sa.add_city site_city,
        sa.add_state site_state,
        sa.add_zipcode site_zip,
        sa.add_country site_country,
        sa.add_phone site_phone,
        sa.add_email site_email,
        ua.address user_add,
        ua.add_city user_city,
        ua.add_state user_state,
        ua.add_zipcode user_zip,
        ua.add_country user_country,
        ua.add_phone user_phone,
        ua.add_email user_email,
        (select codelst_desc from er_codelst
 where pk_codelst = u.fk_codelst_spl) pri_spl,
  (select codelst_desc from er_codelst where pk_codelst = u.fk_codelst_jobtype) jobtype,                                        (select codelst_desc	from er_codelst where pk_codelst = a.FK_CODELST_ROLE) user_role,
                         u.usr_stat user_status
        from    er_account a,
        er_user u,
        er_site s,
        er_add sa,
        er_add ua
        where   u.pk_user = a.ac_usrcreator
        and     u.fk_siteid = s.pk_site
        and     s.FK_PERADD = sa.pk_add
        and     u.FK_PERADD = ua.pk_add
        order by  UPPER(u.usr_firstname),UPPER(u.usr_lastname)
