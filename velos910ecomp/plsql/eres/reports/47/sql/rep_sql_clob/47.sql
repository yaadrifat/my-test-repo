select
  PERSON_CODE ,  FK_PER , PAT_STUDYID, STUDY_NUMBER,  SITE_NAME, STUDY_TITLE ,
  to_char(STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_ACTUALDT,
   PROTOCOL_NAME,  PATPROT_ENROLDT,
 to_char(PATPROT_START,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_START,
  EVENT_NAME, MONTH, to_char(EVENT_SCHDATE,'YYYY') Year ,
  to_char(EVENT_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) START_DATE_TIME,
EVENT_STATUS ,
  to_char(EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT) EVENT_EXEON,
  (select max(codelst_desc)
     from SCH_EVENTCOST@LNK2SCH A, SCH_CODELST@LNK2SCH B
    where  A.FK_CURRENCY = B.PK_CODELST
      and A.fk_event = fk_assoc  ) cost_currency,
  trim(to_char(tot_cost@lnk2sch(fk_assoc),0999999999999999.99))
evecost,visit_name as visit,
  lst_cost@lnk2sch(fk_assoc) event_costlst,
  lst_costdesc@lnk2sch(fk_assoc) event_costdesc ,
  trim(to_char(sum(tot_cost@lnk2sch(fk_assoc)) OVER (PARTITION By person_code),0999999999999999.99))  as tot,
  trim(to_char(sum(tot_cost@lnk2sch(fk_assoc)) OVER (PARTITION By PROTOCOL_NAME),0999999999999999.99)) as prot_tot,
  FK_PATPROT ,
  trim(to_char(sum(tot_cost@lnk2sch(fk_assoc)) OVER (PARTITION By FK_PATPROT),0999999999999999.99)) as tot_patprot
 from erv_patsch
 where protocolid in (:protCalId)
   and fk_per in (:patientId)
   and EVENT_SCHDATE between TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
   and event_status = 'Done' Order by VISIT,EVENT_SCHDATE
