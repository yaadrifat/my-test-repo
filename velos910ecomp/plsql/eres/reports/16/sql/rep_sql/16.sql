select er_study.study_number,
    er_site.site_name,
    to_char(er_study.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT)
    study_actualdt,
        er_study.study_title,
        PER_CODE patient_id,
        patprot_patstdid,
    to_char(er_patprot.PATPROT_ENROLDT,PKG_DATEUTIL.F_GET_DATEFORMAT) pe,
    (select name         from event_assoc@lnk2sch        where
    EVENT_ID = er_patprot.fk_protocol) protocol,
    to_char(er_patprot.patprot_start,PKG_DATEUTIL.F_GET_DATEFORMAT) ps,
    ( select codelst_desc
      from er_codelst
      where pk_codelst = (select FK_CODELST_STAT   from
      er_patstudystat
      where fk_per = er_patprot.fk_per
      and fk_study = er_study.pk_study
      and PATSTUDYSTAT_ENDT is null )
     ) patient_status
from er_patprot, er_study,  er_per  , er_site
where   er_patprot.PATPROT_STAT = 1
and     er_patprot.fk_study = ~1
and     er_per.PK_PER = er_patprot.fk_per
and     er_per.fk_site in (~4)
and     er_per.fk_site = er_site.pk_site
and     er_study.pk_study = er_patprot.fk_study
and     er_patprot.PATPROT_ENROLDT >= '~2'
and     er_patprot.PATPROT_ENROLDT <= '~3'