SELECT
 ST.STUDY_NUMBER, ST.STUDY_TITLE, PK_SUBMISSION_PROVISO,
 FK_USER_ENTEREDBY,usr_lst(FK_USER_ENTEREDBY) User_enteredby_name,
 PROVISO_DATE, PROVISO from er_submission_proviso p inner join er_submission sub
 on P.FK_SUBMISSION = SUB.PK_SUBMISSION inner join er_study st
 on SUB.FK_STUDY = ST.PK_STUDY where
 p.fk_submission = :submissionPK and
 p.fk_submission_board = :submissionBoardPK order by p.PROVISO_DATE desc
