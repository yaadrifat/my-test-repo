 SELECT * FROM (
SELECT DECODE(trial_type_agent,'Y',1,DECODE(trial_type_nonagent,'Y',2,DECODE(trial_type_epi,'Y',3,DECODE(trial_type_comp,'Y',4)))) AS s1,
DECODE(trial_type_agent,'Y','Agent OR Device',DECODE(trial_type_nonagent,'Y','Trials Involving other Interventions',DECODE(trial_type_epi,'Y','Epidemiologic OR other Observational Studies',DECODE(trial_type_comp,'Y','Companion, ANCILLARY OR Correlative Studies','NA')))) AS trial_type,
RESEARCH_TYPE,STUDY_TYPE,STUDY_SCOPE,tharea,STUDY_SPONSOR,DISEASE_SITE,STUDY_NUMBER,PI,STUDY_MAJ_AUTH,TAREA,DATE_OPEN,DATE_CLOSED,STUDY_PHASE,STUDY_TITLE,TARGET,CTR_TO_DATE,CTR_12_MOS, CLOSED_DATE,
F_Extsum4(ps,'G', 'male', TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),NULL) AS gm,
F_Extsum4(ps,'G', 'female', TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),NULL) AS gf,
F_Extsum4(ps,'G', NULL, TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),NULL) AS g0,
F_Extsum4(ps,'R', 'race_asian', TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),NULL) AS ra,
F_Extsum4(ps,'R', 'race_white', TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),NULL) AS rw ,
F_Extsum4(ps,'R', 'race_notrep', TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),NULL) AS rn ,
F_Extsum4(ps,'R', 'race_indala', TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),NULL) AS ri ,
F_Extsum4(ps,'R', 'race_blkafr', TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),NULL) AS rb ,
F_Extsum4(ps,'R', 'race_hwnisl', TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),NULL) AS rh ,
F_Extsum4(ps,'R', 'race_unknown', TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),NULL) AS ru ,
F_Extsum4(ps,'R', NULL, TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),NULL) AS r0 ,  F_Extsum4(ps,'E', 'hispanic', TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),NULL) AS eh ,
F_Extsum4(ps,'E', 'nonhispanic', TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),NULL) AS en ,
F_Extsum4(ps,'E', 'notreported', TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),NULL) AS er ,
F_Extsum4(ps,'E', 'Unknown', TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),NULL) AS eu ,
F_Extsum4(ps,'E', NULL, TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),NULL) AS e0, F_Extsum4(ps,'R', 'race_asian', TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),'hispanic') AS ha,
F_Extsum4(ps,'R', 'race_white', TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),'hispanic') AS hw ,
F_Extsum4(ps,'R', 'race_notrep', TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),'hispanic') AS hn ,
F_Extsum4(ps,'R', 'race_indala', TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),'hispanic') AS hi ,
F_Extsum4(ps,'R', 'race_blkafr', TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),'hispanic') AS hb ,
F_Extsum4(ps,'R', 'race_hwnisl', TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),'hispanic') AS hh ,F_Extsum4(ps,'R', 'race_unknown', TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),'hispanic') AS hu ,
F_Extsum4(ps,'R', NULL, TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT),'hispanic') AS h0 FROM (  SELECT
a.pk_study AS ps,  F_Get_Sum4_Data(a.pk_study,'sum4_agent') AS trial_type_agent,
F_Get_Sum4_Data(a.pk_study,'sum4_beh') AS trial_type_nonagent,
F_Get_Sum4_Data(a.pk_study,'sum4_na') AS trial_type_epi,
F_Get_Sum4_Data(a.pk_study,'sum4_comp') AS trial_type_comp,
F_Codelst_Desc(a.fk_codelst_restype) AS research_type,
F_Codelst_Desc(a.fk_codelst_type) AS study_type,
F_Codelst_Desc(a.fk_codelst_scope) AS study_scope,
F_Codelst_Desc(a.fk_codelst_tarea) AS tharea,
decode(fk_codelst_sponsor,null,study_sponsor,( select codelst_desc from er_codelst where pk_codelst = fk_codelst_sponsor)) study_sponsor
,F_Getdis_Site(a.study_disease_site) AS disease_site, study_number,
(SELECT usr_lastname || ', ' || usr_firstname FROM ER_USER WHERE pk_user = a.study_prinv) || ' ' || study_otherprinv AS PI,
study_maj_auth,
(SELECT studyid_id FROM ER_STUDYID WHERE fk_study = a.pk_study AND fk_codelst_idtype = F_Codelst_Id('studyidtype','sum4_prg')) AS tarea,
TO_CHAR(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) AS date_open,
(CASE WHEN study_end_date >= TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND study_end_date <= TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
THEN TO_CHAR(study_end_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ELSE NULL
END) AS date_closed,
F_Codelst_Desc(a.fk_codelst_phase) AS study_phase,
SUBSTR(study_title,1,125) AS study_title,
(SELECT studysite_lsamplesize FROM ER_STUDYSITES r WHERE r.fk_study = a.pk_study AND fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = :sessAccId AND site_stat = 'Y')) AS target,
(SELECT COUNT(*) FROM ER_PATPROT WHERE fk_study = a.pk_study AND patprot_stat = 1 AND PATPROT_ENROLDT IS NOT NULL) AS ctr_to_date,
(SELECT COUNT(*) FROM ER_PATPROT WHERE fk_study = a.pk_study AND patprot_stat = 1 AND patprot_enroldt BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT) ) AS ctr_12_mos,
TO_CHAR((SELECT MAX(studystat_date) FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_codelst_studystat = F_Codelst_Id('studystat','active_cls')),PKG_DATEUTIL.F_GET_DATEFORMAT) AS closed_date
FROM ER_STUDY a, ER_STUDYSTAT b  WHERE pk_study = fk_study AND 
a.fk_account = :sessAccId AND
fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type =  'studystat' AND codelst_subtyp = 'active') AND
TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) <= NVL((SELECT MIN(studystat_date) FROM ER_STUDYSTAT WHERE fk_study = pk_study AND fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = :sessAccId AND site_stat = 'Y') AND fk_codelst_studystat IN (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type =  'studystat' AND (codelst_subtyp = 'prmnt_cls' OR codelst_subtyp = 'active_cls'))),TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT)) AND
TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT) >= (SELECT MIN(studystat_date) FROM ER_STUDYSTAT WHERE fk_study = pk_study AND fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = :sessAccId AND site_stat = 'Y') AND fk_codelst_studystat IN (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type =  'studystat' AND codelst_subtyp = 'active'))
)
)
WHERE trial_type <> 'NA'
ORDER BY s1,research_type,tarea,pi 
