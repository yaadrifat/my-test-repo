select
ceil(dts.days/7) weeks ,
decode(mod(dts.days,7),0,7,mod(dts.days,7)) days ,
EVENT_ID  ,
CHAIN_ID ,
EVENT_TYPE,
NAME ,
DISPLACEMENT   ,
org_id ,
prot_name.pname  ,
EVENTCOST_DESC,
EVENTCOST_VALUE,
s.sv ,
snum.sn ,
to_char(snum.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt,
sum_event
from (select a.name pname from erv_eveassoc a where a.event_id = ~1 ) prot_name,
er_tmprep ,
( select rownum days
    from all_objects
   where rownum < (select max(displacement)+1 days
    from er_tmprep )
) dts ,
 (select trim(min(eventcost_curr) || ' ' || sum(eve_value)) sv from er_tmprep ) s  ,
 (select study_number sn , study_actualdt from er_study where pk_study = ~2 ) snum
where   displacement(+)=dts.days and name is not null
order by 1,2 
