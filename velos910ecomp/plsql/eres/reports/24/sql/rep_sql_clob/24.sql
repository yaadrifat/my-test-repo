 select codelst_desc study_stat,
    TO_CHAR(STUDYSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) stat_date ,
    ER_STUDY.study_number,
    TO_CHAR(ER_STUDY.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt,studystat_note,
    DECODE(current_stat,1,(SELECT (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_studystat) FROM ER_STUDYSTAT a WHERE a.fk_study = pk_study AND current_stat=1 AND ROWNUM=1)) AS current_stat,
DECODE(current_stat,1,'Yes') AS current_stat_flag,
    (SELECT site_name FROM ER_SITE WHERE pk_site = fk_site) AS site_name
      FROM ER_CODELST,
           ER_STUDYSTAT ,
           ER_STUDY
      WHERE ER_STUDYSTAT.FK_STUDY IN (:studyId)
        AND ER_STUDY.pk_study = ER_STUDYSTAT.fk_study
        AND pk_codelst = FK_CODELST_STUDYSTAT
        AND CODELST_TYPE= 'studystat' AND ER_STUDYSTAT.FK_SITE IN (:orgId) AND fk_codelst_studystat IN (:studyStatusId)
    and STUDYSTAT_DATE between TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
      ORDER BY study_number ASC,fk_site, STUDYSTAT_DATE DESC 
