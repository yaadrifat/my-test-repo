select
       STUDY_NUMBER,
       STUDY_TITLE,
        to_char(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt ,
       TA,
       STUDY_AUTHOR
  from erv_study
 where fk_account = :sessAccId
   and STUDY_PUBFLAG = 'Y'
   AND STUDY_ACTUALDT <= TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
 order by TA
