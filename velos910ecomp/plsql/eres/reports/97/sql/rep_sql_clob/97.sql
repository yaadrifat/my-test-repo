 SELECT study_number,
study_title,
DECODE(study_prinv,NULL,DECODE(study_otherprinv,NULL,'',study_otherprinv),(SELECT usr_lastname || ',' || usr_firstname FROM ER_USER WHERE pk_user = study_prinv) || DECODE(study_otherprinv,NULL,'','; ' || study_otherprinv)) AS PI,
(SELECT usr_firstname || ' ' || usr_lastname FROM ER_USER WHERE pk_user = study_coordinator) AS study_contact,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_tarea) AS tarea,
F_Getdis_Site(study_disease_site) AS disease_site,
F_Getlocal_Samplesize(pk_study) AS study_samplsize,
study_nsamplsize,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_phase) AS phase,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_restype) AS restype,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_scope) AS SCOPE,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_type) AS study_type,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_blind) AS blind,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_random) AS RANDOM,
DECODE(fk_codelst_sponsor,NULL,study_sponsor,( SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_sponsor)) study_sponsor,
F_Getmore_Studydetails(pk_study) AS study_details,
(SELECT TO_CHAR(min(studystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT
WHERE fk_study = a.pk_study AND  fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = :sessAccId AND site_stat = 'Y') AND
fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'studystat' AND codelst_subtyp = 'active')
) AS open_for_enroll_dt,
(SELECT TO_CHAR(min(studystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT  WHERE fk_study = a.pk_study AND  fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = :sessAccId AND site_stat = 'Y') AND
fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'studystat' AND codelst_subtyp = 'active_cls')
) AS closed_to_accr_dt,
(SELECT TO_CHAR(min(studystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT
WHERE fk_study = a.pk_study AND  fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = :sessAccId AND site_stat = 'Y') AND
fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'studystat' AND codelst_subtyp = 'prmnt_cls')
) AS retired_dt,
(SELECT TO_CHAR(max(studystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT
WHERE fk_study = a.pk_study AND  fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = :sessAccId AND site_stat = 'Y') AND 
fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'studystat' AND codelst_subtyp = (select ctrl_value from ER_CTRLTAB where ctrl_key = 'irb_app_stat'))
) AS chrstat_validfrm_dt,
(SELECT TO_CHAR(max(studystat_validt),PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT  WHERE fk_study = a.pk_study AND  fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = :sessAccId AND site_stat = 'Y') AND
fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'studystat' AND codelst_subtyp = (select ctrl_value from ER_CTRLTAB where ctrl_key = 'irb_app_stat'))
) AS chrstat_validuntil_dt,
(SELECT COUNT(*) FROM ER_PATPROT WHERE fk_study = a.pk_study AND patprot_stat = 1 AND PATPROT_ENROLDT IS NOT NULL) AS patients_accrued, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = study_division) AS division
FROM ER_STUDY a
WHERE fk_account = :sessAccId AND
 ( EXISTS (SELECT fk_study FROM ER_STUDYTEAM WHERE fk_study = pk_study AND fk_user IN (:sessUserId))
     or Pkg_Superuser.F_Is_Superuser(:sessUserId, pk_study) = 1
 )
 AND pk_study IN (:studyId) AND fk_codelst_tarea IN (:tAreaId) AND ((study_division IN (:studyDivId)) OR (study_division IS NULL))
AND ( study_prinv IS NULL OR study_prinv IN (:userId) OR EXISTS (SELECT * FROM ER_STUDYTEAM WHERE pk_study = fk_study AND fk_user IN (:userId) AND fk_codelst_tmrole =(SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='role_prin')))
ORDER BY study_number 
