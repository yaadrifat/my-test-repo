select  USR_LASTNAME, USR_FIRSTNAME, add_phone, add_email,
f_get_usersites(pk_user) AS ORGANIZATION,
 ( SELECT codelst_desc  FROM  ER_CODELST WHERE pk_codelst = FK_CODELST_JOBTYPE) AS jobType,
 ( SELECT codelst_desc  FROM  ER_CODELST WHERE pk_codelst = fk_codelst_spl) AS primary_spl,
 DECODE(usr_stat, 'A','Active', 'D','Deactivated', 'B', 'Blocked') AS usr_stat,
f_get_usergrp(pk_user) AS user_groups
 FROM  ER_USER, ER_ADD
 WHERE  usr_stat IN ('A', 'D', 'B')
  AND fk_account =:sessAccId
 AND  fk_peradd = pk_add AND usr_type = 'S' ORDER
 BY USR_FIRSTNAME,USR_LASTNAME 
