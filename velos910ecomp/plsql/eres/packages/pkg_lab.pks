CREATE OR REPLACE PACKAGE        "PKG_LAB"
IS
PROCEDURE SP_INSERT_PATLABS(p_pat_id VARCHAR2,p_site_id VARCHAR2,p_patprot_id VARCHAR2, p_lab_date ARRAY_STRING, p_lab_id ARRAY_STRING,
              p_accn_num ARRAY_STRING,p_lab_result ARRAY_STRING,p_test_type ARRAY_STRING, p_lab_unit ARRAY_STRING,p_lab_lln ARRAY_STRING,
			  p_lab_uln ARRAY_STRING,p_lab_status ARRAY_STRING,p_lab_abnr ARRAY_STRING,p_lab_cat ARRAY_STRING,p_lab_advname ARRAY_STRING,
			  p_lab_grade ARRAY_STRING,p_lab_notes ARRAY_CLOB,p_lab_lresults ARRAY_CLOB, p_user VARCHAR2, p_ipadd VARCHAR2,p_stdPhase ARRAY_STRING,p_studyId Varchar2, p_specId varchar2, o_ret OUT NUMBER);
PROCEDURE SP_DELETE_PATLABS(p_ids ARRAY_STRING,p_tablename varchar2,p_col varchar2,p_datatype varchar2,o_ret OUT NUMBER);

END PKG_LAB;
/


CREATE SYNONYM ESCH.PKG_LAB FOR PKG_LAB;


CREATE SYNONYM EPAT.PKG_LAB FOR PKG_LAB;


GRANT EXECUTE, DEBUG ON PKG_LAB TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_LAB TO ESCH;

