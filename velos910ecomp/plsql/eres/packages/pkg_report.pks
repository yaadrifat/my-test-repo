CREATE OR REPLACE PACKAGE        "PKG_REPORT" AS
PROCEDURE Sp_GenReport(
   P_REPID          NUMBER,
   P_ACCID          NUMBER,
   P_PARAM         ARRAY_STRING,
   P_PARAMVAL ARRAY_STRING,
   P_SXML     OUT   CLOB
);
FUNCTION F_Gen_PatDump(p_study VARCHAR2,p_site VARCHAR2,p_patient VARCHAR2,p_txarm VARCHAR2,p_startdate VARCHAR2, p_enddate VARCHAR2) RETURN CLOB;
FUNCTION F_Gen_UsrProfile(p_user VARCHAR2) RETURN CLOB;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_REPORT', pLEVEL  => Plog.LFATAL);
END Pkg_Report;
/


