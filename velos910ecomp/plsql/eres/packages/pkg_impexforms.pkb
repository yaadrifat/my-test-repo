CREATE OR REPLACE PACKAGE BODY        "PKG_IMPEXFORMS" 
AS
 PROCEDURE SP_GET_FORMFLD_EXP_SQLS (
      P_FORMS          LONG,
      O_FORMSEC     OUT  LONG,
  	  O_FORMFLD     OUT  LONG,
	  O_REPFORMFLD OUT  LONG,
	  O_FLDRESP OUT  LONG,
	  O_FLDVALIDATE OUT  LONG,
	  O_REPFLDVALIDATE OUT  LONG,
	  O_FLDACTION OUT LONG,
	  O_FLDACTION_INFO OUT LONG
   )
   /* Author: Sonia Sahni
   Date: 25rd Jan 2004
   Purpose - Returns SQLS for forms data export
   */

   AS
   	 V_FORMS LONG;
	 v_success NUMBER;
    BEGIN

	 V_FORMS := P_FORMS;



	IF P_FORMS IS NULL OR P_FORMS = '' OR LENGTH(trim(P_FORMS)) = 0  THEN
	   		   V_FORMS := '-1';

	END IF	;

		 -- get form sections
   		 O_FORMSEC  := ' Select PK_FORMSEC, FK_FORMLIB,  FORMSEC_NAME  , FORMSEC_SEQ,  FORMSEC_FMT,
				    FORMSEC_REPNO, RECORD_TYPE, FORMSEC_OFFLINE  FROM ER_FORMSEC
					WHERE fk_formlib IN (' || V_FORMS  || ') AND NVL(record_type,''N'') <> ''D'' ' ;


		--get form fields


		O_FORMFLD := 'Select PK_FIELD, PK_FORMFLD ,FK_FORMSEC,FORMFLD_SEQ , FORMFLD_MANDATORY ,FORMFLD_BROWSERFLG ,
		 FORMFLD_XSL , FORMFLD_JAVASCR , FORMFLD_OFFLINE,   FLD_LIBFLAG  , FLD_NAME , FLD_DESC , FLD_UNIQUEID ,
		 FLD_SYSTEMID , FLD_KEYWORD , FLD_TYPE , FLD_DATATYPE , FLD_INSTRUCTIONS  , FLD_LENGTH , FLD_DECIMAL  ,
		 FLD_LINESNO , FLD_CHARSNO  , FLD_DEFRESP  , FK_LOOKUP ,  FLD_ISUNIQUE  ,   FLD_ISREADONLY   , FLD_ISVISIBLE ,
		 FLD_COLCOUNT , FLD_FORMAT , FLD_REPEATFLAG , FLD_BOLD ,FLD_ITALICS , FLD_SAMELINE , FLD_ALIGN , FLD_UNDERLINE  ,
		 FLD_COLOR , FLD_FONT , FLD_FONTSIZE , FLD_LKPDATAVAL,FLD_LKPDISPVAL , FLD_TODAYCHECK  , FLD_OVERRIDE_MANDATORY, FLD_LKPTYPE,
		 FLD_EXPLABEL,FLD_OVERRIDE_FORMAT, FLD_OVERRIDE_RANGE,FLD_OVERRIDE_DATE ,
		 FLD_HIDELABEL,FLD_HIDERESPLABEL,FLD_DISPLAY_WIDTH , FLD_RESPALIGN,FLD_NAME_FORMATTED
		 FROM ER_FORMSEC, ER_FORMFLD, ER_FLDLIB
		 WHERE fk_formlib IN (' || V_FORMS || ') AND PK_FORMSEC = FK_FORMSEC AND
		 fk_field = pk_field AND NVL(ER_FORMSEC.record_type,''N'') <> ''D'' AND
		 NVL(ER_FORMFLD.record_type,''N'') <> ''D'' ';



		 O_REPFORMFLD := 'Select  PK_FIELD, PK_REPFORMFLD , FK_FORMFLD , FK_FORMSEC , REPFORMFLD_SEQ , REPFORMFLD_XSL,
		 REPFORMFLD_JAVASCR, REPFORMFLD_SET ,FLD_LIBFLAG  , FLD_NAME , FLD_DESC , FLD_UNIQUEID ,
		 FLD_SYSTEMID , FLD_KEYWORD , FLD_TYPE , FLD_DATATYPE , FLD_INSTRUCTIONS  , FLD_LENGTH , FLD_DECIMAL  ,
		 FLD_LINESNO , FLD_CHARSNO  , FLD_DEFRESP  , FK_LOOKUP ,  FLD_ISUNIQUE  ,   FLD_ISREADONLY   , FLD_ISVISIBLE ,
		 FLD_COLCOUNT , FLD_FORMAT , FLD_REPEATFLAG , FLD_BOLD ,FLD_ITALICS , FLD_SAMELINE , FLD_ALIGN , FLD_UNDERLINE  ,
		 FLD_COLOR , FLD_FONT , FLD_FONTSIZE , FLD_LKPDATAVAL,FLD_LKPDISPVAL , FLD_TODAYCHECK  , FLD_OVERRIDE_MANDATORY,
		 FLD_LKPTYPE,   FLD_EXPLABEL, FLD_OVERRIDE_FORMAT, FLD_OVERRIDE_RANGE,FLD_OVERRIDE_DATE ,
FLD_HIDELABEL,FLD_HIDERESPLABEL,FLD_DISPLAY_WIDTH , FLD_RESPALIGN,FLD_NAME_FORMATTED
		 FROM ER_FORMSEC, ER_REPFORMFLD, ER_FLDLIB
		 WHERE fk_formlib IN (' || V_FORMS || ') AND PK_FORMSEC = FK_FORMSEC AND
		 fk_field = pk_field AND NVL(ER_FORMSEC.record_type,''N'') <> ''D'' AND
		 NVL(ER_REPFORMFLD.record_type,''N'') <> ''D'' ';


		 O_FLDRESP := 'Select resp.PK_FLDRESP , resp.FK_FIELD , resp.FLDRESP_SEQ , resp.FLDRESP_DISPVAL , resp.FLDRESP_DATAVAL ,
		 		 resp.FLDRESP_SCORE , resp.FLDRESP_ISDEFAULT, resp.FLDRESP_OFFLINE,resp.record_type
		 FROM ER_FORMSEC, ER_FORMFLD , ER_FLDRESP resp
		 WHERE fk_formlib IN (' || V_FORMS || ') AND NVL(ER_FORMSEC.record_type,''N'') <> ''D'' AND
		 PK_FORMSEC = FK_FORMSEC AND NVL(ER_FORMFLD.record_type,''N'') <> ''D'' AND
		 resp.fk_field = ER_FORMFLD.FK_FIELD AND  NVL(resp.record_type,''N'') <> ''D''
		 UNION
		 SELECT resp.PK_FLDRESP , resp.FK_FIELD , resp.FLDRESP_SEQ , resp.FLDRESP_DISPVAL , resp.FLDRESP_DATAVAL ,
		 		 resp.FLDRESP_SCORE , resp.FLDRESP_ISDEFAULT, resp.FLDRESP_OFFLINE,resp.record_type
		 FROM ER_FORMSEC, ER_REPFORMFLD , ER_FLDRESP resp
		 WHERE fk_formlib IN (' || V_FORMS || ') AND NVL(ER_FORMSEC.record_type,''N'') <> ''D'' AND
		 PK_FORMSEC = FK_FORMSEC AND NVL(ER_REPFORMFLD.record_type,''N'') <> ''D'' AND
		 resp.fk_field = ER_REPFORMFLD.FK_FIELD AND  NVL(resp.record_type,''N'') <> ''D''
		 ';

		 O_FLDVALIDATE := 'Select PK_FLDVALIDATE, FK_FLDLIB, FLDVALIDATE_OP1, FLDVALIDATE_VAL1,FLDVALIDATE_LOGOP1,
		 FLDVALIDATE_OP2 ,  FLDVALIDATE_VAL2, FLDVALIDATE_LOGOP2, FLDVALIDATE_JAVASCR, FLD_SYSTEMID
		 FROM ER_FORMSEC, ER_FORMFLD , ER_FLDVALIDATE, ER_FLDLIB
		 WHERE fk_formlib IN (' || V_FORMS || ') AND NVL(ER_FORMSEC.record_type,''N'') <> ''D'' AND
		 PK_FORMSEC = FK_FORMSEC AND NVL(ER_FORMFLD.record_type,''N'') <> ''D'' AND
		 fk_fldlib = ER_FORMFLD.FK_FIELD AND  NVL(ER_FLDVALIDATE.record_type,''N'') <> ''D'' AND
		 pk_field = ER_FORMFLD.FK_FIELD ';


		 O_REPFLDVALIDATE := 'Select rep.PK_REPFLDVALIDATE  , rep.FK_REPFORMFLD  , rep.FK_FIELD , rep.REPFLDVALIDATE_JAVASCR, FLD_SYSTEMID
		 FROM  ER_FORMSEC, ER_REPFORMFLD , ER_REPFLDVALIDATE rep, ER_FLDLIB
		 WHERE fk_formlib IN (' || V_FORMS || ') AND NVL(ER_FORMSEC.record_type,''N'') <> ''D'' AND
		 PK_FORMSEC = FK_FORMSEC AND NVL(ER_REPFORMFLD.record_type,''N'') <> ''D'' AND
		 rep.fk_field = ER_REPFORMFLD.FK_FIELD AND NVL(rep.record_type,''N'') <> ''D'' AND
		 pk_field = ER_REPFORMFLD.FK_FIELD ';

		 O_FLDACTION := 'Select PK_FLDACTION , FK_FORM,  FLDACTION_TYPE, FK_FIELD, FLDACTION_CONDITION
		 			 	 FROM ER_FLDACTION WHERE fk_form IN (' || V_FORMS || ')';

		 O_FLDACTION_INFO := 'Select PK_FLDACTIONINFO,  FK_FLDACTION,  INFO_TYPE,  INFO_VALUE
		 				  	  FROM ER_FLDACTION , ER_FLDACTIONINFO WHERE fk_form IN (' || V_FORMS || ') AND
							  FK_FLDACTION = PK_FLDACTION';


    END SP_GET_FORMFLD_EXP_SQLS;

	PROCEDURE SP_IMP_FORMFLDS(P_MODULE VARCHAR2,P_EXPID NUMBER,p_expdatils CLOB, O_ERR OUT VARCHAR2, O_SUCCESS OUT NUMBER)
	AS

	/* Date: 02/25/04
	   Author: Sonia Sahni
	   Purpose: Imports form fields data from temp tables to actual tables */

     v_modkeys Types.SMALL_STRING_ARRAY := NEW Types.SMALL_STRING_ARRAY();
	 v_orig_form NUMBER;
	 v_new_form NUMBER;
	 v_err VARCHAR2(4000);
     v_success NUMBER;
	 v_sitecode VARCHAR2(10);
	 v_originating_sitecode VARCHAR2(10);

  	 v_count NUMBER;
	 v_creator VARCHAR2(50);
	 v_ipAdd VARCHAR2(15);


  	 v_account VARCHAR2(100);

	 v_tablename VARCHAR2(100);
	 v_sql LONG;

    v_cur Gk_Cv_Types.GenericCursorType;


	v_orig_sec NUMBER;
	v_new_sec NUMBER;
	v_formsec_name  VARCHAR2(50);
	v_formsec_seq NUMBER;
	v_formsec_fmt CHAR(1);
	v_formsec_repno NUMBER;
	v_formsec_offline NUMBER;

    v_orig_field NUMBER;
    v_orig_formfld NUMBER;
    v_new_field NUMBER;
    v_new_formfld NUMBER;
    v_formfld_seq NUMBER;
    v_formfld_mandatory NUMBER;
	v_formfld_browser NUMBER;
	v_formfld_xsl VARCHAR2(4000);
	v_formfld_javascr VARCHAR2(4000);
	v_formfld_offline NUMBER;
	v_fld_libflag CHAR(1);
	v_fld_name VARCHAR2(4000);
	v_fldname_formatted  VARCHAR(4000);
	v_fld_desc VARCHAR2(1000);
	v_fld_uniqueid VARCHAR2(50);
	v_fld_systemid VARCHAR2(50);
--	v_new_systemid VARCHAR2(50);
	v_fld_kword VARCHAR2(255);
	v_fld_type CHAR(1);
	v_fld_datatype VARCHAR2(2);
	v_fld_inst VARCHAR2(4000);
	v_fld_length NUMBER;
	v_fld_decimal NUMBER;
	v_fld_linesno NUMBER;
	v_fld_charsno NUMBER;
	v_fld_defresp VARCHAR2(500);
	v_fld_fklookup NUMBER;
	v_fld_isunique NUMBER;
	v_fld_readonly NUMBER;
	v_fld_isvisible NUMBER;
	v_fld_colcount NUMBER;
	v_fld_format VARCHAR2(100);
	v_fld_repeatflag NUMBER;
	v_fld_bold NUMBER;
	v_fld_italics NUMBER;
	v_fld_sameline NUMBER;
	v_fld_align VARCHAR2(16);
	v_fld_underline NUMBER;
	v_fld_color VARCHAR2(15);
	v_fld_font VARCHAR2(50);
	v_fld_fontsize NUMBER;
	v_fld_lkpdataval  VARCHAR2(1000);
	v_fld_lkpdispval VARCHAR2(1000);
	v_fld_todaycheck NUMBER;


	v_orig_repformfld NUMBER;
	v_new_repformfld  NUMBER;
	v_repformfld_seq NUMBER;
	v_repformfld_xsl VARCHAR2(4000);
	v_repformfld_javascr VARCHAR2(4000);
	v_repformfld_set NUMBER;


	v_orig_resp NUMBER;
	v_new_resp NUMBER;
	v_fldresp_seq  NUMBER;
	v_fldresp_dispval  VARCHAR2(300);
	v_fldresp_dataval  VARCHAR2(300);
	v_fldresp_score NUMBER;
	v_fldresp_isdefault NUMBER;
	v_fldresp_offline NUMBER;

	v_orig_fldvalidate NUMBER;
	v_fldvalidate_op1 VARCHAR2(2);
	v_fldvalidate_val1 VARCHAR2(100);
	v_fldvalidate_logop1 VARCHAR2(10);
	v_fldvalidate_op2 VARCHAR2(2);
	v_fldvalidate_val2 VARCHAR2(100);
	v_fldvalidate_logop2 VARCHAR2(10);
	v_fldvalidate_javascr VARCHAR2(4000);
	v_new_fldvalidate NUMBER;

	v_orig_repfldvalidate NUMBER;
	v_repfldvalidate_javascr VARCHAR2(4000);
	v_new_repfldvalidate NUMBER;
	v_fld_lkptype CHAR(1);
	v_fld_explabel NUMBER;

	v_orig_fldaction NUMBER;
	v_new_fldaction NUMBER;
	v_fldaction_type VARCHAR2(20);
    v_fldaction_condition VARCHAR2(100);

	v_orig_fldactioninfo NUMBER;
	v_new_fldactioninfo NUMBER;
	v_info_type VARCHAR2(100);
	v_info_value VARCHAR2(4000);

	v_fld_override_format NUMBER;
	v_fld_override_range  NUMBER;
	v_fld_override_date  NUMBER;
    v_fld_hidelabel  NUMBER;
	v_fld_hideresplabel  NUMBER;
	v_fld_display_width  NUMBER;
	v_fld_respalign VARCHAR2(16);
	v_fld_override_mandatory NUMBER;
	v_resp_record_type VARCHAR2(3);

	v_old_info_value  VARCHAR2(4000);
	v_keyword_type VARCHAR2(20);
	BEGIN

 	    Pkg_Impexcommon.SP_GETELEMENT_VALUES (p_expdatils,'targetSite',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_sitecode);
		END IF;


		--get originating site_code

		Pkg_Impexcommon.SP_GETELEMENT_VALUES (p_expdatils,'originatingSiteCode',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_originating_sitecode);
		END IF;

		--get creator

		Pkg_Impexcommon.SP_GETELEMENT_VALUES (p_expdatils,'impUserId',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,	v_creator);
		END IF;

		-- get ipAdd

		Pkg_Impexcommon.SP_GETELEMENT_VALUES (p_expdatils,'impIpAdd',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,	v_ipAdd);
		END IF;

		--get import account

		Pkg_Impexcommon.SP_GETELEMENT_VALUES (p_expdatils,'impAccountId',v_modkeys);

   		v_count  := v_modkeys.COUNT();

		IF v_count > 0 THEN
		 	 Pkg_Impexcommon.SP_ARRAY2STRING(v_modkeys,v_account);
		END IF;

		-- get data from the temp tables

		--get formsections

		IF P_MODULE = 'study_forms' THEN
		   v_tablename := 'impstudy_forms3' ;
		ELSIF  P_MODULE = 'study_cal' THEN
		   v_tablename := 'impstudy_cal6' ;
		ELSIF P_MODULE = 'sp_patform' THEN
		   v_tablename := 'impsp_patform3' ;

		END IF;

		 v_sql := 'Select PK_FORMSEC, FK_FORMLIB , FORMSEC_NAME, FORMSEC_SEQ, FORMSEC_FMT, FORMSEC_REPNO,
		  		   FORMSEC_OFFLINE FROM ' || v_tablename ;

		 OPEN v_cur FOR v_sql;

		 LOOP
		 	 FETCH v_cur INTO  v_orig_sec, v_orig_form, v_formsec_name, v_formsec_seq, v_formsec_fmt,
			 	   		 v_formsec_repno, v_formsec_offline;

			 EXIT WHEN v_cur  %NOTFOUND;

			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_form, 'er_formlib',v_sitecode, v_new_form,v_err,v_originating_sitecode);
			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_sec, 'er_formsec',v_sitecode, v_new_sec,v_err,v_originating_sitecode);

			 IF v_new_form <= 0 THEN
			  	  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','MISSING FK','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS Could not create record for er_formsec, parent Form not found. original PK_FORMLIB : ' || v_orig_form, v_success );
			 ELSE
			 	 IF v_new_sec = 0 THEN -- new record
		  		 	 SELECT seq_er_formsec.NEXTVAL
						INTO v_new_sec	FROM dual;

					 	Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'debug','MSG','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS - New Form Section record with original PK:' || v_orig_sec, v_success );

						 Pkg_Impexcommon.SP_SET_SITEPK(v_orig_sec, 'er_formsec',v_sitecode,  v_new_sec,v_err,v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS Could not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN

								INSERT INTO ER_FORMSEC(PK_FORMSEC ,FK_FORMLIB, FORMSEC_NAME, FORMSEC_SEQ,
								 		FORMSEC_FMT, FORMSEC_REPNO, RECORD_TYPE, CREATOR, CREATED_ON, IP_ADD,
										FORMSEC_OFFLINE )
								VALUES (v_new_sec,v_new_form,v_formsec_name, v_formsec_seq,
								v_formsec_fmt,v_formsec_repno,'N', v_creator,SYSDATE,v_ipAdd,v_formsec_offline) ;

							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS:Could not create Form Section record for Sponsor PK ' || v_orig_sec || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					 ELSIF v_new_sec > 0 THEN -- old record

					 	Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'debug','MSG','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS - Old Form Section with original PK:' || v_orig_sec , v_success );

						UPDATE ER_FORMSEC
						SET FORMSEC_NAME = v_formsec_name , FORMSEC_SEQ = v_formsec_seq ,
								 		FORMSEC_FMT = v_formsec_fmt, FORMSEC_REPNO = v_formsec_repno,
										RECORD_TYPE = 'M', IP_ADD = v_ipAdd,
										FORMSEC_OFFLINE = v_formsec_offline, LAST_MODIFIED_BY = v_creator ,
										LAST_MODIFIED_DATE = SYSDATE
					    WHERE pk_formsec  = v_new_sec;

					 ELSIF v_new_sec = -1 THEN

					  	 Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS:' || v_err, v_success );
						 o_err := v_err;
						 o_success :=  v_success;

					END IF;
			END IF; -- for parent Form
		 END LOOP;
	 CLOSE v_cur ;
	--end of form sections import


	  --get form fields

		IF P_MODULE = 'study_forms' THEN
		   v_tablename := 'impstudy_forms4' ;
   		ELSIF  P_MODULE = 'study_cal' THEN
		   v_tablename := 'impstudy_cal7' ;
   		ELSIF P_MODULE = 'sp_patform' THEN
		   v_tablename := 'impsp_patform4' ;
		END IF;


		 v_sql := 'Select PK_FIELD ,PK_FORMFLD, FK_FORMSEC, FORMFLD_SEQ, FORMFLD_MANDATORY, FORMFLD_BROWSERFLG,
		 	   	  FORMFLD_XSL, FORMFLD_JAVASCR, FORMFLD_OFFLINE, FLD_LIBFLAG, FLD_NAME, FLD_DESC, FLD_UNIQUEID,
				  FLD_SYSTEMID, FLD_KEYWORD, FLD_TYPE, FLD_DATATYPE,FLD_INSTRUCTIONS, FLD_LENGTH, FLD_DECIMAL,
				  FLD_LINESNO,FLD_CHARSNO, FLD_DEFRESP, FK_LOOKUP,FLD_ISUNIQUE,FLD_ISREADONLY, FLD_ISVISIBLE,
				  FLD_COLCOUNT, FLD_FORMAT, FLD_REPEATFLAG, FLD_BOLD,FLD_ITALICS, FLD_SAMELINE, FLD_ALIGN,
				  FLD_UNDERLINE,FLD_COLOR, FLD_FONT, FLD_FONTSIZE, FLD_LKPDATAVAL, FLD_LKPDISPVAL,
				  FLD_TODAYCHECK, FLD_OVERRIDE_MANDATORY, FLD_LKPTYPE,fld_explabel,
				  FLD_OVERRIDE_FORMAT, FLD_OVERRIDE_RANGE,FLD_OVERRIDE_DATE ,
				  FLD_HIDELABEL,FLD_HIDERESPLABEL,FLD_DISPLAY_WIDTH , FLD_RESPALIGN,FLD_NAME_FORMATTED
				  FROM ' || v_tablename ;



		 OPEN v_cur FOR v_sql;

		 LOOP


		 	 FETCH v_cur INTO  v_orig_field, v_orig_formfld, v_orig_sec, v_formfld_seq, v_formfld_mandatory,
			 	   		 v_formfld_browser, v_formfld_xsl, v_formfld_javascr, v_formfld_offline, v_fld_libflag,
						 v_fld_name, v_fld_desc, v_fld_uniqueid, v_fld_systemid, v_fld_kword, v_fld_type,
						 v_fld_datatype, v_fld_inst, v_fld_length,v_fld_decimal,v_fld_linesno, v_fld_charsno,
						 v_fld_defresp, v_fld_fklookup, v_fld_isunique,v_fld_readonly,v_fld_isvisible,
						 v_fld_colcount, v_fld_format, v_fld_repeatflag,v_fld_bold, v_fld_italics, v_fld_sameline,
						 v_fld_align, v_fld_underline, v_fld_color, v_fld_font, v_fld_fontsize, v_fld_lkpdataval,
						 v_fld_lkpdispval,v_fld_todaycheck,v_fld_override_mandatory, v_fld_lkptype,v_fld_explabel,
						 v_fld_override_format ,	v_fld_override_range, v_fld_override_date ,
						 v_fld_hidelabel ,v_fld_hideresplabel  ,v_fld_display_width ,v_fld_respalign,v_fldname_formatted  ;


			 EXIT WHEN v_cur  %NOTFOUND;

 			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_sec, 'er_formsec',v_sitecode, v_new_sec,v_err,v_originating_sitecode);
			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_field, 'er_fldlib',v_sitecode, v_new_field,v_err,v_originating_sitecode);
			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_formfld, 'er_formfld',v_sitecode, v_new_formfld,v_err,v_originating_sitecode);

			 --v_new_systemid :=	substr('N' || v_sitecode || v_fld_systemid,1,50);
			 --v_formfld_xsl := replace(v_formfld_xsl,v_fld_systemid,v_new_systemid);
			 --v_formfld_javascr := replace(v_formfld_javascr ,v_fld_systemid,v_new_systemid);

			 IF (v_fld_type = 'M' AND v_fld_datatype = 'ML') THEN
		 		    IF LENGTH(trim(NVL(v_fld_lkptype,''))) <= 0 THEN
						v_fld_lkptype := 'L';
					END IF;
			 END IF;

			 IF v_new_sec <= 0 THEN
			  	  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','MISSING FK','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS Could not create record for er_formfld, parent Form Section not found. original PK_FORMSEC : ' || v_orig_sec, v_success );
			 ELSE
			 	 IF v_new_field = 0 THEN -- new record
		  		 	SELECT seq_er_fldlib.NEXTVAL
					INTO v_new_field	FROM dual;

					SELECT seq_er_formfld.NEXTVAL
					INTO v_new_formfld FROM dual;

					 	Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'debug','MSG','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS - New Field record with original PK:' || v_orig_field, v_success );

						Pkg_Impexcommon.SP_SET_SITEPK(v_orig_field, 'er_fldlib',v_sitecode,  v_new_field,v_err,v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS Could not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN

								-- INSERT NEW field



								INSERT INTO ER_FLDLIB( PK_FIELD , FK_ACCOUNT , FLD_LIBFLAG , FLD_NAME ,FLD_DESC,
								FLD_UNIQUEID , FLD_SYSTEMID , FLD_KEYWORD , FLD_TYPE ,FLD_DATATYPE , FLD_INSTRUCTIONS ,
								FLD_LENGTH , FLD_DECIMAL , FLD_LINESNO , FLD_CHARSNO , FLD_DEFRESP  , FK_LOOKUP ,
								FLD_ISUNIQUE  ,  FLD_ISREADONLY , FLD_ISVISIBLE  , FLD_COLCOUNT , FLD_FORMAT,
								FLD_REPEATFLAG , FLD_BOLD , FLD_ITALICS  , FLD_SAMELINE, FLD_ALIGN , FLD_UNDERLINE ,
								FLD_COLOR ,  FLD_FONT , RECORD_TYPE , FLD_FONTSIZE   , FLD_LKPDATAVAL, FLD_LKPDISPVAL ,
								FLD_TODAYCHECK , FLD_OVERRIDE_MANDATORY  , CREATOR , CREATED_ON , IP_ADD,FLD_LKPTYPE ,fld_explabel ,
								FLD_OVERRIDE_FORMAT, FLD_OVERRIDE_RANGE,FLD_OVERRIDE_DATE ,
								FLD_HIDELABEL,FLD_HIDERESPLABEL,FLD_DISPLAY_WIDTH , FLD_RESPALIGN,FLD_NAME_FORMATTED)
								VALUES (v_new_field,v_account ,v_fld_libflag, v_fld_name, v_fld_desc,
								v_fld_uniqueid, v_fld_systemid, v_fld_kword, v_fld_type, v_fld_datatype, v_fld_inst,
								v_fld_length,v_fld_decimal,v_fld_linesno, v_fld_charsno, v_fld_defresp, v_fld_fklookup,
								v_fld_isunique,v_fld_readonly,v_fld_isvisible, v_fld_colcount, v_fld_format,
								v_fld_repeatflag,v_fld_bold, v_fld_italics, v_fld_sameline, v_fld_align, v_fld_underline,
								v_fld_color, v_fld_font,'N', v_fld_fontsize, v_fld_lkpdataval, v_fld_lkpdispval,
								v_fld_todaycheck,v_fld_override_mandatory, v_creator,SYSDATE,v_ipAdd,v_fld_lkptype,v_fld_explabel,
								v_fld_override_format ,	v_fld_override_range, v_fld_override_date ,
								v_fld_hidelabel ,v_fld_hideresplabel  ,v_fld_display_width ,
								v_fld_respalign,v_fldname_formatted ) ;

								-- INSERT FORM field

								Pkg_Impexcommon.SP_SET_SITEPK(v_orig_formfld, 'er_formfld',v_sitecode,v_new_formfld ,v_err,v_originating_sitecode);

								IF LENGTH(trim(v_err)) > 0 THEN

								   Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS Could not create record for er_sitepkmap :' || v_err, v_success );

								ELSE

									INSERT INTO ER_FORMFLD(PK_FORMFLD, FK_FORMSEC, FK_FIELD, FORMFLD_SEQ, FORMFLD_MANDATORY,
									FORMFLD_BROWSERFLG,  FORMFLD_XSL, FORMFLD_JAVASCR,FORMFLD_OFFLINE , CREATOR, RECORD_TYPE,
									CREATED_ON, IP_ADD  )
									VALUES(v_new_formfld, v_new_sec, v_new_field, v_formfld_seq,v_formfld_mandatory,
				 	   		 		v_formfld_browser, v_formfld_xsl, v_formfld_javascr, v_formfld_offline,v_creator,
									'N',SYSDATE,v_ipAdd );

								END IF;

							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS:Could not create Form Field and Field record for FIELD Sponsor PK ' || v_orig_field || ', FORMFLD Sponsor PK ' || v_orig_formfld || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					 ELSIF v_new_field > 0 THEN -- old record

					 	Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'debug','MSG','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS - Old Field with original PK:' || v_orig_field , v_success );

						UPDATE ER_FLDLIB
						SET  FLD_LIBFLAG = v_fld_libflag , FLD_NAME = v_fld_name ,FLD_DESC = v_fld_desc,
								FLD_UNIQUEID = v_fld_uniqueid , FLD_SYSTEMID = v_fld_systemid ,
								FLD_KEYWORD = v_fld_kword , FLD_TYPE = v_fld_type, FLD_DATATYPE = v_fld_datatype ,
								FLD_INSTRUCTIONS = v_fld_inst, FLD_LENGTH = v_fld_length  ,
								FLD_DECIMAL = v_fld_decimal , FLD_LINESNO = v_fld_linesno,
								FLD_CHARSNO = v_fld_charsno, FLD_DEFRESP = v_fld_defresp ,
								FK_LOOKUP = v_fld_fklookup , FLD_ISUNIQUE = v_fld_isunique ,
								FLD_ISREADONLY =  v_fld_readonly, FLD_ISVISIBLE = v_fld_isvisible  ,
								FLD_COLCOUNT = v_fld_colcount, FLD_FORMAT = v_fld_format,
								FLD_REPEATFLAG = v_fld_repeatflag, FLD_BOLD = v_fld_bold, FLD_ITALICS = v_fld_italics ,
								FLD_SAMELINE = v_fld_sameline , FLD_ALIGN = v_fld_align , FLD_UNDERLINE = v_fld_underline ,
								FLD_COLOR = v_fld_color,  FLD_FONT = v_fld_font , RECORD_TYPE = 'M', FLD_FONTSIZE = v_fld_fontsize,
								FLD_LKPDATAVAL = v_fld_lkpdataval, FLD_LKPDISPVAL = v_fld_lkpdispval,
								FLD_TODAYCHECK = v_fld_todaycheck, FLD_OVERRIDE_MANDATORY = v_fld_override_mandatory ,
								IP_ADD = v_ipAdd, LAST_MODIFIED_BY = v_creator ,
								LAST_MODIFIED_DATE = SYSDATE, FLD_LKPTYPE = v_fld_lkptype,
								fld_explabel = v_fld_explabel,
								FLD_OVERRIDE_FORMAT = v_fld_override_format, FLD_OVERRIDE_RANGE = v_fld_override_range,
								FLD_OVERRIDE_DATE =  v_fld_override_date,  FLD_HIDELABEL = v_fld_hidelabel,
								FLD_HIDERESPLABEL = v_fld_hideresplabel , FLD_DISPLAY_WIDTH = v_fld_display_width,
								FLD_RESPALIGN = v_fld_respalign,
								FLD_NAME_FORMATTED=v_fldname_formatted
						  WHERE pk_field  = v_new_field;


						UPDATE ER_FORMFLD SET FK_FORMSEC = v_new_sec, FORMFLD_SEQ = v_formfld_seq,
							    FORMFLD_MANDATORY = v_formfld_mandatory ,
								FORMFLD_BROWSERFLG = v_formfld_browser,  FORMFLD_XSL = v_formfld_xsl,
								FORMFLD_JAVASCR = v_formfld_javascr,
								FORMFLD_OFFLINE = v_formfld_offline,RECORD_TYPE = 'M', IP_ADD = v_ipAdd,
								LAST_MODIFIED_BY = v_creator ,	LAST_MODIFIED_DATE = SYSDATE
						WHERE fk_field = v_new_field;

					 ELSIF v_new_field = -1 THEN

					  	 Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS:' || v_err, v_success );
						 o_err := v_err;
						 o_success :=  v_success;

					END IF;
			END IF; -- for parent Section
		 END LOOP;
	 CLOSE v_cur ;

	 -- end of fields


 	  --get rep form fields

		IF P_MODULE = 'study_forms' THEN
		   v_tablename := 'impstudy_forms5' ;
   		ELSIF  P_MODULE = 'study_cal' THEN
		   v_tablename := 'impstudy_cal8' ;
		ELSIF P_MODULE = 'sp_patform' THEN
		   v_tablename := 'impsp_patform5' ;
		END IF;

		 v_sql := 'Select PK_FIELD , PK_REPFORMFLD , FK_FORMFLD,FK_FORMSEC,REPFORMFLD_SEQ, REPFORMFLD_XSL,
		 	      REPFORMFLD_JAVASCR,  REPFORMFLD_SET , FLD_LIBFLAG, FLD_NAME, FLD_DESC, FLD_UNIQUEID,
				  FLD_SYSTEMID, FLD_KEYWORD, FLD_TYPE, FLD_DATATYPE,FLD_INSTRUCTIONS, FLD_LENGTH, FLD_DECIMAL,
				  FLD_LINESNO,FLD_CHARSNO, FLD_DEFRESP, FK_LOOKUP,FLD_ISUNIQUE,FLD_ISREADONLY, FLD_ISVISIBLE,
				  FLD_COLCOUNT, FLD_FORMAT, FLD_REPEATFLAG, FLD_BOLD,FLD_ITALICS, FLD_SAMELINE, FLD_ALIGN,
				  FLD_UNDERLINE,FLD_COLOR, FLD_FONT, FLD_FONTSIZE, FLD_LKPDATAVAL, FLD_LKPDISPVAL,
				  FLD_TODAYCHECK, FLD_OVERRIDE_MANDATORY,FLD_LKPTYPE, fld_explabel,FLD_OVERRIDE_FORMAT,
				  FLD_OVERRIDE_RANGE,FLD_OVERRIDE_DATE , FLD_HIDELABEL,FLD_HIDERESPLABEL,FLD_DISPLAY_WIDTH ,
				  FLD_RESPALIGN,FLD_NAME_FORMATTED   FROM ' || v_tablename ;


		 OPEN v_cur FOR v_sql;

		 LOOP

		 	 FETCH v_cur INTO  v_orig_field, v_orig_repformfld, v_orig_formfld, v_orig_sec, v_repformfld_seq,
			 	   		 v_repformfld_xsl, v_repformfld_javascr, v_repformfld_set, v_fld_libflag,
						 v_fld_name, v_fld_desc, v_fld_uniqueid, v_fld_systemid, v_fld_kword, v_fld_type,
						 v_fld_datatype, v_fld_inst, v_fld_length,v_fld_decimal,v_fld_linesno, v_fld_charsno,
						 v_fld_defresp, v_fld_fklookup, v_fld_isunique,v_fld_readonly,v_fld_isvisible,
						 v_fld_colcount, v_fld_format, v_fld_repeatflag,v_fld_bold, v_fld_italics, v_fld_sameline,
						 v_fld_align, v_fld_underline, v_fld_color, v_fld_font, v_fld_fontsize, v_fld_lkpdataval,
						 v_fld_lkpdispval,v_fld_todaycheck,v_fld_override_mandatory,v_fld_lkptype, v_fld_explabel,
						 v_fld_override_format ,	v_fld_override_range, v_fld_override_date ,
						 v_fld_hidelabel ,v_fld_hideresplabel  ,v_fld_display_width ,
						 v_fld_respalign,v_fldname_formatted ;
			 EXIT WHEN v_cur  %NOTFOUND;

 			 --v_new_systemid :=	substr('N' || v_sitecode || v_fld_systemid,1,50);

			 --v_repformfld_xsl := replace(v_repformfld_xsl,v_fld_systemid,v_new_systemid);
			 --v_repformfld_javascr := replace(v_repformfld_javascr ,v_fld_systemid,v_new_systemid);

 			 IF (v_fld_type = 'M' AND v_fld_datatype = 'ML') THEN
		 		    IF LENGTH(trim(NVL(v_fld_lkptype,''))) <= 0  THEN
						v_fld_lkptype := 'L';
					END IF;
			 END IF;


 			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_sec, 'er_formsec',v_sitecode, v_new_sec,v_err,v_originating_sitecode);
			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_formfld, 'er_formfld',v_sitecode, v_new_formfld,v_err,v_originating_sitecode);

			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_field, 'er_fldlib',v_sitecode, v_new_field,v_err,v_originating_sitecode);
			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_repformfld, 'er_repformfld',v_sitecode, v_new_repformfld,v_err,v_originating_sitecode);


			 IF v_new_sec <= 0 THEN
			  	  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','MISSING FK','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS Could not create record for er_repformfld, parent Form Section not found. original PK_FORMSEC : ' || v_orig_sec, v_success );
			 ELSIF v_new_formfld <= 0 THEN
		 	   	  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','MISSING FK','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS Could not create record for er_repformfld, parent Form Field not found. original PK_FORMSEC : ' || v_orig_formfld, v_success );
			 ELSE
			 	 IF v_new_field = 0 THEN -- new record
		  		 	 SELECT seq_er_fldlib.NEXTVAL
					INTO v_new_field	FROM dual;

					SELECT seq_er_repformfld.NEXTVAL
					INTO v_new_repformfld FROM dual;


					 	Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'debug','MSG','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS - New Field record with original PK:' || v_orig_field, v_success );

						Pkg_Impexcommon.SP_SET_SITEPK(v_orig_field, 'er_fldlib',v_sitecode,  v_new_field,v_err,v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS Could not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN

								-- INSERT NEW field

								INSERT INTO ER_FLDLIB( PK_FIELD , FK_ACCOUNT , FLD_LIBFLAG , FLD_NAME ,FLD_DESC,
								FLD_UNIQUEID , FLD_SYSTEMID , FLD_KEYWORD , FLD_TYPE ,FLD_DATATYPE , FLD_INSTRUCTIONS ,
								FLD_LENGTH , FLD_DECIMAL , FLD_LINESNO , FLD_CHARSNO , FLD_DEFRESP  , FK_LOOKUP ,
								FLD_ISUNIQUE  ,  FLD_ISREADONLY , FLD_ISVISIBLE  , FLD_COLCOUNT , FLD_FORMAT,
								FLD_REPEATFLAG , FLD_BOLD , FLD_ITALICS  , FLD_SAMELINE, FLD_ALIGN , FLD_UNDERLINE ,
								FLD_COLOR ,  FLD_FONT , RECORD_TYPE , FLD_FONTSIZE   , FLD_LKPDATAVAL, FLD_LKPDISPVAL ,
								FLD_TODAYCHECK , FLD_OVERRIDE_MANDATORY , CREATOR , CREATED_ON , IP_ADD ,FLD_LKPTYPE,fld_explabel,
								FLD_OVERRIDE_FORMAT, FLD_OVERRIDE_RANGE,FLD_OVERRIDE_DATE ,
								FLD_HIDELABEL,FLD_HIDERESPLABEL,FLD_DISPLAY_WIDTH , FLD_RESPALIGN,FLD_NAME_FORMATTED
								 )
								VALUES (v_new_field,v_account ,v_fld_libflag, v_fld_name, v_fld_desc,
								v_fld_uniqueid, v_fld_systemid, v_fld_kword, v_fld_type, v_fld_datatype, v_fld_inst,
								v_fld_length,v_fld_decimal,v_fld_linesno, v_fld_charsno, v_fld_defresp, v_fld_fklookup,
								v_fld_isunique,v_fld_readonly,v_fld_isvisible, v_fld_colcount, v_fld_format,
								v_fld_repeatflag,v_fld_bold, v_fld_italics, v_fld_sameline, v_fld_align, v_fld_underline,
								v_fld_color, v_fld_font,'N', v_fld_fontsize, v_fld_lkpdataval, v_fld_lkpdispval,
								v_fld_todaycheck,v_fld_override_mandatory, v_creator,SYSDATE,v_ipAdd,v_fld_lkptype, v_fld_explabel,
								v_fld_override_format ,	v_fld_override_range, v_fld_override_date ,
								v_fld_hidelabel ,v_fld_hideresplabel  ,v_fld_display_width ,v_fld_respalign,v_fldname_formatted ) ;


								-- INSERT FORM field


								Pkg_Impexcommon.SP_SET_SITEPK(v_orig_repformfld, 'er_repformfld',v_sitecode,v_new_repformfld ,v_err,v_originating_sitecode);

								IF LENGTH(trim(v_err)) > 0 THEN

								   Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS Could not create record for er_sitepkmap :' || v_err, v_success );

								ELSE

									INSERT INTO ER_REPFORMFLD( PK_REPFORMFLD, FK_FORMFLD, FK_FORMSEC,  FK_FIELD,
									REPFORMFLD_SEQ, REPFORMFLD_XSL,REPFORMFLD_JAVASCR, REPFORMFLD_SET, CREATOR, RECORD_TYPE,
									CREATED_ON, IP_ADD  )
									VALUES(v_new_repformfld, v_new_formfld ,v_new_sec, v_new_field, v_repformfld_seq,
									 v_repformfld_xsl, v_repformfld_javascr, v_repformfld_set,v_creator,
									'N',SYSDATE,v_ipAdd );
								END IF;

							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS:Could not create Form Field and Field record for FIELD Sponsor PK ' || v_orig_field || ', REPFORMFLD Sponsor PK ' || v_orig_repformfld || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					 ELSIF v_new_field > 0 THEN -- old record

					 	Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'debug','MSG','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS - Old Field with original PK:' || v_orig_field , v_success );


						UPDATE ER_FLDLIB
						SET  FLD_LIBFLAG = v_fld_libflag , FLD_NAME = v_fld_name ,FLD_DESC = v_fld_desc,
								FLD_UNIQUEID = v_fld_uniqueid , FLD_SYSTEMID = v_fld_systemid ,
								FLD_KEYWORD = v_fld_kword , FLD_TYPE = v_fld_type, FLD_DATATYPE = v_fld_datatype ,
								FLD_INSTRUCTIONS = v_fld_inst, FLD_LENGTH = v_fld_length  ,
								FLD_DECIMAL = v_fld_decimal , FLD_LINESNO = v_fld_linesno,
								FLD_CHARSNO = v_fld_charsno, FLD_DEFRESP = v_fld_defresp ,
								FK_LOOKUP = v_fld_fklookup , FLD_ISUNIQUE = v_fld_isunique ,
								FLD_ISREADONLY =  v_fld_readonly, FLD_ISVISIBLE = v_fld_isvisible  ,
								FLD_COLCOUNT = v_fld_colcount, FLD_FORMAT = v_fld_format,
								FLD_REPEATFLAG = v_fld_repeatflag, FLD_BOLD = v_fld_bold, FLD_ITALICS = v_fld_italics ,
								FLD_SAMELINE = v_fld_sameline , FLD_ALIGN = v_fld_align , FLD_UNDERLINE = v_fld_underline ,
								FLD_COLOR = v_fld_color,  FLD_FONT = v_fld_font , RECORD_TYPE = 'M', FLD_FONTSIZE = v_fld_fontsize,
								FLD_LKPDATAVAL = v_fld_lkpdataval, FLD_LKPDISPVAL = v_fld_lkpdispval,
								FLD_TODAYCHECK = v_fld_todaycheck, FLD_OVERRIDE_MANDATORY = v_fld_override_mandatory ,
								IP_ADD = v_ipAdd, LAST_MODIFIED_BY = v_creator ,
								LAST_MODIFIED_DATE = SYSDATE, FLD_LKPTYPE = v_fld_lkptype,
								fld_explabel = v_fld_explabel,
								FLD_OVERRIDE_FORMAT = v_fld_override_format, FLD_OVERRIDE_RANGE = v_fld_override_range,
								FLD_OVERRIDE_DATE =  v_fld_override_date,  FLD_HIDELABEL = v_fld_hidelabel,
								FLD_HIDERESPLABEL = v_fld_hideresplabel , FLD_DISPLAY_WIDTH = v_fld_display_width,
								FLD_RESPALIGN = v_fld_respalign
								,FLD_NAME_FORMATTED=v_fldname_formatted
					    WHERE pk_field  = v_new_field;


						UPDATE ER_REPFORMFLD SET FK_FORMSEC = v_new_sec, REPFORMFLD_SEQ = v_repformfld_seq,
							    REPFORMFLD_XSL = v_repformfld_xsl,REPFORMFLD_JAVASCR = v_repformfld_javascr,
								REPFORMFLD_SET = v_repformfld_set,RECORD_TYPE = 'M', IP_ADD = v_ipAdd,
								LAST_MODIFIED_BY = v_creator ,	LAST_MODIFIED_DATE = SYSDATE
						WHERE fk_field = v_new_field;

					 ELSIF v_new_field = -1 THEN

					  	 Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS:' || v_err, v_success );
						 o_err := v_err;
						 o_success :=  v_success;

					END IF;
			END IF; -- for parent section, formfld
		 END LOOP;
	 CLOSE v_cur ;

	 -- end of repfields

	  --get fields responses

		IF P_MODULE = 'study_forms' THEN
		   v_tablename := 'impstudy_forms6' ;
	    ELSIF  P_MODULE = 'study_cal' THEN
		   v_tablename := 'impstudy_cal9' ;
		ELSIF P_MODULE = 'sp_patform' THEN
		   v_tablename := 'impsp_patform6' ;
		END IF;

		 v_sql := 'Select PK_FLDRESP, FK_FIELD, FLDRESP_SEQ, FLDRESP_DISPVAL, FLDRESP_DATAVAL, FLDRESP_SCORE,
		 	   FLDRESP_ISDEFAULT,  FLDRESP_OFFLINE , record_type FROM ' || v_tablename ;

		 OPEN v_cur FOR v_sql;

		 LOOP
		 	 FETCH v_cur INTO  v_orig_resp, v_orig_field, v_fldresp_seq, v_fldresp_dispval,v_fldresp_dataval,
			 v_fldresp_score, v_fldresp_isdefault,v_fldresp_offline	, v_resp_record_type ;

			 EXIT WHEN v_cur  %NOTFOUND;



			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_field, 'er_fldlib',v_sitecode, v_new_field,v_err,v_originating_sitecode);
			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_resp, 'er_fldresp',v_sitecode, v_new_resp,v_err,v_originating_sitecode);


			 IF v_new_field <= 0 THEN
			  	  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','MISSING FK','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS Could not create record for er_fldresp, parent Field not found. original PK_FIELD : ' || v_orig_field, v_success );

			 ELSE
			 	 IF v_new_resp = 0 THEN -- new record
		  		 	 SELECT seq_er_fldresp.NEXTVAL
						INTO v_new_resp	FROM dual;

					 	Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'debug','MSG','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS - New Field Response record with original PK:' || v_orig_resp, v_success );

						Pkg_Impexcommon.SP_SET_SITEPK(v_orig_resp, 'er_fldresp',v_sitecode,  v_new_resp,v_err,v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS Could not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN

								-- INSERT NEW field

								 SELECT  DECODE(NVL(v_resp_record_type,'N'),'H','H','N')
								 INTO v_resp_record_type FROM dual;


								INSERT INTO ER_FLDRESP( PK_FLDRESP,  FK_FIELD,  FLDRESP_SEQ,  FLDRESP_DISPVAL,
									    FLDRESP_DATAVAL,  FLDRESP_SCORE,  FLDRESP_ISDEFAULT,  FLDRESP_OFFLINE,
										RECORD_TYPE, CREATOR, CREATED_ON,IP_ADD)
 								VALUES(v_new_resp,v_new_field,v_fldresp_seq, v_fldresp_dispval,
								v_fldresp_dataval, v_fldresp_score, v_fldresp_isdefault,v_fldresp_offline,
								v_resp_record_type, v_creator,SYSDATE,v_ipAdd) ;

							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS:Could not create Field Response record for Sponsor PK ' || v_orig_resp || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					 ELSIF v_new_resp > 0 THEN -- old record

					 	Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'debug','MSG','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS - Old Field Response with original PK:' || v_orig_resp , v_success );

						 SELECT  DECODE(NVL(v_resp_record_type,'M'),'H','H','M')
								 INTO v_resp_record_type FROM dual;



						UPDATE ER_FLDRESP
						SET FLDRESP_SEQ = v_fldresp_seq,
						FLDRESP_DISPVAL = v_fldresp_dispval , FLDRESP_DATAVAL = v_fldresp_dataval,
						FLDRESP_SCORE = v_fldresp_score, FLDRESP_ISDEFAULT = v_fldresp_isdefault,
						FLDRESP_OFFLINE = v_fldresp_offline, RECORD_TYPE = v_resp_record_type,
						IP_ADD = v_ipAdd , LAST_MODIFIED_BY = v_creator ,LAST_MODIFIED_DATE = SYSDATE
					    WHERE pk_fldresp  = v_new_resp;

					 ELSIF v_new_resp = -1 THEN

					  	 Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS:' || v_err, v_success );
						 o_err := v_err;
						 o_success :=  v_success;

					END IF;
			END IF; -- for parent field
		 END LOOP;
	 CLOSE v_cur ;

	 -- end of fldresp


	  --get fields validations

		IF P_MODULE = 'study_forms' THEN
			   v_tablename := 'impstudy_forms7' ;
	    ELSIF  P_MODULE = 'study_cal' THEN
		   v_tablename := 'impstudy_cal10' ;
		ELSIF P_MODULE = 'sp_patform' THEN
		   v_tablename := 'impsp_patform7' ;
 		 END IF;

		 v_sql := 'Select PK_FLDVALIDATE, FK_FLDLIB, FLDVALIDATE_OP1,  FLDVALIDATE_VAL1,    FLDVALIDATE_LOGOP1,
		 	   	  FLDVALIDATE_OP2,  FLDVALIDATE_VAL2,  FLDVALIDATE_LOGOP2, FLDVALIDATE_JAVASCR, FLD_SYSTEMID
				  FROM ' || v_tablename ;

			 OPEN v_cur FOR v_sql;

				 LOOP
				 	 FETCH v_cur INTO  v_orig_fldvalidate, v_orig_field, v_fldvalidate_op1, v_fldvalidate_val1,
					 v_fldvalidate_logop1, v_fldvalidate_op2,v_fldvalidate_val2,v_fldvalidate_logop2,v_fldvalidate_javascr, v_fld_systemid;

					 EXIT WHEN v_cur  %NOTFOUND;

		  			 --v_new_systemid :=	substr('N' || v_sitecode || v_fld_systemid,1,50);
					 --v_fldvalidate_javascr := replace(v_fldvalidate_javascr,v_fld_systemid,v_new_systemid);



					 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_field, 'er_fldlib',v_sitecode, v_new_field,v_err,v_originating_sitecode);
					 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_fldvalidate, 'er_fldvalidate',v_sitecode, v_new_fldvalidate,v_err,v_originating_sitecode);


					 IF v_new_field <= 0 THEN
					  	  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','MISSING FK','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS Could not create record for er_fldvalidate, parent Field not found. original PK_FIELD : ' || v_orig_field, v_success );

					 ELSE
					 	 IF v_new_fldvalidate = 0 THEN -- new record
				  		 	 SELECT seq_er_fldvalidate.NEXTVAL
								INTO v_new_fldvalidate FROM dual;

							 	Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'debug','MSG','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS - New Field Validation Record with original PK:' || v_orig_fldvalidate, v_success );

								Pkg_Impexcommon.SP_SET_SITEPK(v_orig_fldvalidate, 'er_fldvalidate',v_sitecode,  v_new_fldvalidate,v_err,v_originating_sitecode);

								IF LENGTH(trim(v_err)) > 0 THEN

								  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS Could not create record for er_sitepkmap :' || v_err, v_success );

								ELSE
									BEGIN

										-- INSERT NEW field validation record

										INSERT INTO ER_FLDVALIDATE( PK_FLDVALIDATE,FK_FLDLIB,FLDVALIDATE_OP1,
										FLDVALIDATE_VAL1,FLDVALIDATE_LOGOP1,FLDVALIDATE_OP2,FLDVALIDATE_VAL2,
										FLDVALIDATE_LOGOP2,FLDVALIDATE_JAVASCR,RECORD_TYPE,CREATOR,CREATED_ON,
										IP_ADD)
		 								VALUES(v_new_fldvalidate,v_new_field,v_fldvalidate_op1, v_fldvalidate_val1,
					 					v_fldvalidate_logop1, v_fldvalidate_op2,v_fldvalidate_val2,v_fldvalidate_logop2,
										v_fldvalidate_javascr,'N', v_creator,SYSDATE,v_ipAdd) ;

									EXCEPTION WHEN OTHERS THEN
									   Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS:Could not create Field Validation record for Sponsor PK ' || v_orig_fldvalidate || ' Error: ' || SQLERRM, v_success );
									END;
				  				END IF;

							 ELSIF v_new_fldvalidate > 0 THEN -- old record

							 	Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'debug','MSG','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS - Old Field Validation record with original PK:' || v_orig_fldvalidate , v_success );

								UPDATE ER_FLDVALIDATE
								SET
								FLDVALIDATE_OP1 = v_fldvalidate_op1 ,
								FLDVALIDATE_VAL1 = v_fldvalidate_val1,FLDVALIDATE_LOGOP1 = v_fldvalidate_logop1,
								FLDVALIDATE_OP2 = v_fldvalidate_op2 ,FLDVALIDATE_VAL2 = v_fldvalidate_val2,
								FLDVALIDATE_LOGOP2 = v_fldvalidate_logop2,FLDVALIDATE_JAVASCR = v_fldvalidate_javascr,
								RECORD_TYPE = 'M',IP_ADD = v_ipAdd , LAST_MODIFIED_BY = v_creator ,
								LAST_MODIFIED_DATE = SYSDATE
							    WHERE PK_FLDVALIDATE  = v_new_fldvalidate;

							 ELSIF v_new_fldvalidate = -1 THEN

							  	 Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS:' || v_err, v_success );
								 o_err := v_err;
								 o_success :=  v_success;

							END IF;
					END IF; -- for parent field
				 END LOOP;
			 CLOSE v_cur ;

		  --get rep fields validations

		IF P_MODULE = 'study_forms' THEN
			   v_tablename := 'impstudy_forms8' ;
		ELSIF  P_MODULE = 'study_cal' THEN
		   v_tablename := 'impstudy_cal11' ;
		ELSIF P_MODULE = 'sp_patform' THEN
		   v_tablename := 'impsp_patform8' ;
 		 END IF;

		 v_sql := 'Select PK_REPFLDVALIDATE, FK_REPFORMFLD, FK_FIELD, REPFLDVALIDATE_JAVASCR, FLD_SYSTEMID
				  FROM ' || v_tablename ;

			 OPEN v_cur FOR v_sql;

				 LOOP
				 	 FETCH v_cur INTO  v_orig_repfldvalidate, v_orig_repformfld, v_orig_field, v_repfldvalidate_javascr, v_fld_systemid;

					 EXIT WHEN v_cur  %NOTFOUND;

 		  			 --v_new_systemid :=	substr('N' || v_sitecode || v_fld_systemid,1,50);
					 --v_repfldvalidate_javascr := replace(v_repfldvalidate_javascr,v_fld_systemid,v_new_systemid);



					 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_repformfld, 'er_repformfld',v_sitecode,v_new_repformfld,v_err,v_originating_sitecode);
 					 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_field, 'er_fldlib',v_sitecode,v_new_field,v_err,v_originating_sitecode);
					 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_repfldvalidate, 'er_repfldvalidate',v_sitecode, v_new_repfldvalidate,v_err,v_originating_sitecode);


					 IF v_new_repformfld <= 0 THEN
					  	  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','MISSING FK','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS Could not create record for er_repfldvalidate, parent REPFORMFLD not found. original PK_REPFORMFLD : ' || v_orig_repformfld, v_success );

					 ELSE
					 	 IF v_new_repfldvalidate = 0 THEN -- new record

				  		 	 SELECT seq_er_repfldvalidate.NEXTVAL
								INTO v_new_repfldvalidate FROM dual;

							 	Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'debug','MSG','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS - New Rep Field Validation Record with original PK:' || v_orig_repfldvalidate, v_success );

								Pkg_Impexcommon.SP_SET_SITEPK(v_orig_repfldvalidate, 'er_repfldvalidate',v_sitecode,  v_new_repfldvalidate,v_err,v_originating_sitecode);

								IF LENGTH(trim(v_err)) > 0 THEN

								  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS Could not create record for er_sitepkmap :' || v_err, v_success );

								ELSE
									BEGIN

										-- INSERT NEW field validation record

										INSERT INTO ER_REPFLDVALIDATE( PK_REPFLDVALIDATE, FK_REPFORMFLD, FK_FIELD,
											    REPFLDVALIDATE_JAVASCR, RECORD_TYPE,  CREATOR, CREATED_ON ,
											 IP_ADD  )
		 								VALUES(v_new_repfldvalidate,v_new_repformfld,v_new_field,
										v_repfldvalidate_javascr,'N', v_creator,SYSDATE,v_ipAdd) ;

									EXCEPTION WHEN OTHERS THEN
									   Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS:Could not create Rep Field Validation record for Sponsor PK ' || v_orig_repfldvalidate || ' Error: ' || SQLERRM, v_success );
									END;
				  				END IF;

							 ELSIF v_new_repfldvalidate > 0 THEN -- old record

							 	Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'debug','MSG','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS - Old Rep Field Validation record with original PK:' || v_orig_repfldvalidate , v_success );

								UPDATE ER_REPFLDVALIDATE
								SET
								REPFLDVALIDATE_JAVASCR = v_repfldvalidate_javascr,
								RECORD_TYPE = 'M',IP_ADD = v_ipAdd , LAST_MODIFIED_BY = v_creator ,
								LAST_MODIFIED_DATE = SYSDATE
							    WHERE PK_REPFLDVALIDATE  = v_new_repfldvalidate;

							 ELSIF v_new_repfldvalidate = -1 THEN

							  	 Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS:' || v_err, v_success );
								 o_err := v_err;
								 o_success :=  v_success;

							END IF;
					END IF; -- for parent field
				 END LOOP;
			 CLOSE v_cur ;

		-- import form field actions

		IF P_MODULE = 'study_forms' THEN
			   v_tablename := 'impstudy_forms10' ;
		ELSIF  P_MODULE = 'study_cal' THEN
		   v_tablename := 'impstudy_cal13' ;
		ELSIF P_MODULE = 'sp_patform' THEN
		   v_tablename := 'impsp_patform10' ;
 		 END IF;


		 v_sql := 'Select PK_FLDACTION , FK_FORM,  FLDACTION_TYPE, FK_FIELD, FLDACTION_CONDITION
		 			FROM ' || v_tablename ;

		 OPEN v_cur FOR v_sql;

		 LOOP
		 	 FETCH v_cur INTO  v_orig_fldaction, v_orig_form, v_fldaction_type, v_orig_field, v_fldaction_condition;

			 EXIT WHEN v_cur  %NOTFOUND;

			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_form, 'er_formlib',v_sitecode, v_new_form,v_err,v_originating_sitecode);

			 IF v_orig_field IS NOT NULL AND v_orig_field > 0 THEN
			 		 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_field, 'er_fldlib',v_sitecode, v_new_field,v_err,v_originating_sitecode);
			 ELSE
			 		 v_new_field := NULL;
			 END IF;

 			 Pkg_Impexcommon.SP_GET_SITEPK( v_orig_fldaction, 'er_fldaction',v_sitecode, v_new_fldaction,v_err,v_originating_sitecode);

			 IF v_new_form <= 0 THEN
			  	  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','MISSING FK','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS Could not create record for er_fldaction, parent Form not found. original PK_FORMLIB : ' || v_orig_form, v_success );
			 ELSIF v_orig_field IS NOT NULL AND v_new_field <= 0 THEN
			 	  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','MISSING FK','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS Could not create record for er_fldaction, parent Field not found. original PK_FORMLIB : ' || v_orig_field, v_success );
			 ELSE


			 	 IF v_new_fldaction = 0 THEN -- new record

		  		 	 SELECT seq_er_fldaction.NEXTVAL
						INTO v_new_fldaction FROM dual;

					 	Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'debug','MSG','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS - New Form Field Action record with original PK:' || v_orig_fldaction, v_success );

						 Pkg_Impexcommon.SP_SET_SITEPK(v_orig_fldaction, 'er_fldaction',v_sitecode,  v_new_fldaction,v_err,v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS Could not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN

							 INSERT INTO ER_FLDACTION(PK_FLDACTION, FK_FORM,   FLDACTION_TYPE,  FK_FIELD, FLDACTION_CONDITION  )
							 VALUES (v_new_fldaction,v_new_form, v_fldaction_type, v_new_field,v_fldaction_condition ) ;

							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS:Could not create Form Field Action record for Sponsor PK ' || v_orig_fldaction || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					 ELSIF v_new_fldaction > 0 THEN -- old record

					 	Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'debug','MSG','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS - Old Form Field Action with original PK:' || v_orig_fldaction , v_success );

						UPDATE ER_FLDACTION
						SET  FK_FORM = v_new_form ,   FLDACTION_TYPE = v_fldaction_type,  FK_FIELD = v_new_field, FLDACTION_CONDITION =  v_fldaction_condition
					    WHERE PK_FLDACTION = v_new_fldaction;

						DELETE FROM ER_SITEPKMAP
						WHERE SITEPKMAP_TABLE = 'er_fldactioninfo' AND
						SITEPKMAP_SITECODE     = v_sitecode AND SITEPKMAP_SITEPK  IN
						(SELECT pk_fldactioninfo FROM ER_FLDACTIONINFO WHERE fk_fldaction = v_new_fldaction );

						DELETE FROM ER_FLDACTIONINFO WHERE fk_fldaction = v_new_fldaction;

					 ELSIF v_new_fldaction = -1 THEN

					  	 Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS:' || v_err, v_success );
						 o_err := v_err;
						 o_success :=  v_success;

					END IF;
			END IF; -- for parent Form
		 END LOOP;
	 CLOSE v_cur ;
	--end of form field action import

	-- import form field action info

		IF P_MODULE = 'study_forms' THEN
			   v_tablename := 'impstudy_forms11' ;
		ELSIF  P_MODULE = 'study_cal' THEN
		   v_tablename := 'impstudy_cal14' ;
		ELSIF P_MODULE = 'sp_patform' THEN
		   v_tablename := 'impsp_patform11' ;
 		 END IF;


		 v_sql := 'Select PK_FLDACTIONINFO,FK_FLDACTION, INFO_TYPE, INFO_VALUE
		 			FROM ' || v_tablename ;

		 OPEN v_cur FOR v_sql;

		 LOOP
		 	 FETCH v_cur INTO  v_orig_fldactioninfo, v_orig_fldaction, v_info_type, v_old_info_value ;

			 EXIT WHEN v_cur  %NOTFOUND;

			 Pkg_Impexcommon.SP_GET_SITEPK(v_orig_fldactioninfo, 'er_fldactioninfo',v_sitecode, v_new_fldactioninfo,v_err,v_originating_sitecode);
 			 Pkg_Impexcommon.SP_GET_SITEPK( v_orig_fldaction, 'er_fldaction',v_sitecode, v_new_fldaction,v_err,v_originating_sitecode);

			 -- check the info_type, if its PK type, get the new field Pk
			 SELECT keyword_type
			INTO v_keyword_type
			FROM ER_KEYWORD
			WHERE keyword = v_info_type;

			 IF v_keyword_type = 'pkfield' THEN -- its a PK type keyword, replace with new values
			 	   	 IF v_old_info_value  IS NOT NULL AND v_old_info_value > 0 THEN
			 		 	Pkg_Impexcommon.SP_GET_SITEPK(v_old_info_value, 'er_fldlib',v_sitecode, v_info_value,v_err,v_originating_sitecode);
			 		 ELSE
			 		 v_info_value := NULL;
					  END IF;
			 ELSE -- its a regular keywordvalue
			 	  v_info_value :=  v_old_info_value;
			 END IF;

			 IF v_new_fldaction <= 0 THEN
			  	  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','MISSING FK','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS Could not create record for er_fldaction, parent Field Action record not found. original PK_FLDACTION : ' || v_orig_fldaction, v_success );
			 ELSE

			 	 IF v_new_fldactioninfo = 0 THEN -- new record

		  		 	 SELECT seq_er_fldactioninfo.NEXTVAL
						INTO v_new_fldactioninfo FROM dual;

					 	Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'debug','MSG','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS - New Form Field Action Info record with original PK:' || v_orig_fldactioninfo, v_success );

						Pkg_Impexcommon.SP_SET_SITEPK(v_orig_fldactioninfo, 'er_fldactioninfo',v_sitecode,  v_new_fldactioninfo,v_err,v_originating_sitecode);

						IF LENGTH(trim(v_err)) > 0 THEN

						  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS Could not create record for er_sitepkmap :' || v_err, v_success );

						ELSE
							BEGIN

							 INSERT INTO ER_FLDACTIONINFO(PK_FLDACTIONINFO, FK_FLDACTION ,INFO_TYPE ,INFO_VALUE )
							 VALUES (v_new_fldactioninfo,v_new_fldaction, v_info_type, v_info_value  ) ;

							EXCEPTION WHEN OTHERS THEN
							  Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS:Could not create Form Field Action Info record for Sponsor PK ' || v_orig_fldactioninfo || ' Error: ' || SQLERRM, v_success );
							END;
		  				END IF;

					 ELSIF v_new_fldactioninfo > 0 THEN -- old record

					 	Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'debug','MSG','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS - Old Form Field Action Info record with original PK:' || v_orig_fldactioninfo , v_success );

						UPDATE ER_FLDACTIONINFO
						SET  INFO_TYPE = v_info_type, INFO_VALUE = v_info_value
					    WHERE PK_FLDACTIONINFO = v_new_fldactioninfo;

					 ELSIF v_new_fldactioninfo = -1 THEN

					  	 Pkg_Impexlogging.SP_RECORDLOG (P_EXPID,'FATAL','EXCEPTION','IMPEX--> PKG_IMPEXFORMS.SP_IMP_FORMFLDS:' || v_err, v_success );
						 o_err := v_err;
						 o_success :=  v_success;

					END IF;
			END IF; -- for parent Form field action
		 END LOOP;
	 CLOSE v_cur ;
	 	   -- end of fldaction info report



		------------------


	END SP_IMP_FORMFLDS;

	/*By Sonia, 12/22/04
	 after complete forms import, regenerate mpform,xsls, xmls for each form passed in the array*/
	PROCEDURE sp_regen_formstruct(p_expid NUMBER, p_importedforms Types.SMALL_STRING_ARRAY, p_spname VARCHAR2)
	AS

	  v_importedforms Types.SMALL_STRING_ARRAY := NEW Types.SMALL_STRING_ARRAY();
	  v_formcounter NUMBER := 1;
	  v_formcount NUMBER ;
	  v_imp_form NUMBER;
	  v_formhtml CLOB;

	  v_refresh_xsl CLOB;
	  v_refresh_xml sys.XMLTYPE;
	  v_refersh_printxsl CLOB;
	  v_latest_version NUMBER;
	  v_success NUMBER;

	BEGIN
		v_importedforms := p_importedforms;
		v_formcount := v_importedforms.COUNT();

	   WHILE v_formcounter <= v_formcount
		 LOOP
		 	v_imp_form := v_importedforms(v_formcounter);
			v_formcounter := v_formcounter + 1;

			 --generate new xsl and xml
			 Pkg_Form.sp_clubformxsl(v_imp_form,v_formhtml);

			 --generate printer friendly xsl
			 Pkg_Filledform.SP_GENERATE_PRINTXSL(v_imp_form);

			 --create mapform
			 Pkg_Formrep.SP_GENFORMLAYOUT(v_imp_form);

			 -- update the latest formversion

			 BEGIN

				 SELECT form_xml,form_xsl,form_viewxsl
				 INTO v_refresh_xml, v_refresh_xsl,v_refersh_printxsl
				 FROM ER_FORMLIB WHERE pk_formlib =  v_imp_form;

	   			 SELECT MAX(pk_formlibver)
				 INTO v_latest_version
				 FROM ER_FORMLIBVER WHERE
				 fk_formlib = v_imp_form;

	 			 UPDATE ER_FORMLIBVER
				 SET  FORMLIBVER_XML = v_refresh_xml, FORMLIBVER_XSL = v_refresh_xsl,
				 FORMLIBVER_VIEWXSL = v_refersh_printxsl
				 WHERE pk_formlibver = v_latest_version;

			  EXCEPTION WHEN NO_DATA_FOUND THEN
			 	 Pkg_Impexlogging.SP_RECORDLOG (p_expid,'FATAL','EXCEPTION','IMPEX--> '|| p_spname ||':Could not get refreshed form data', v_success );
			END;

		 END LOOP;


	END sp_regen_formstruct;

  END Pkg_Impexforms;
/


CREATE SYNONYM ESCH.PKG_IMPEXFORMS FOR PKG_IMPEXFORMS;


CREATE SYNONYM EPAT.PKG_IMPEXFORMS FOR PKG_IMPEXFORMS;


GRANT EXECUTE, DEBUG ON PKG_IMPEXFORMS TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_IMPEXFORMS TO ESCH;

