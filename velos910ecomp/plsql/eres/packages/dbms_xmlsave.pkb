CREATE OR REPLACE PACKAGE BODY        "DBMS_XMLSAVE" AS

  FUNCTION newContext(targetTable IN VARCHAR2) RETURN ctxType
  as LANGUAGE JAVA NAME
   'oracle.xml.sql.dml.OracleXMLStaticSave.newContext(java.lang.String) return int';

  PROCEDURE closeContext(ctx IN ctxType)
  as LANGUAGE JAVA NAME
   'oracle.xml.sql.dml.OracleXMLStaticSave.closeContext(int)';


  PROCEDURE setRowTag(ctx IN ctxType, tag IN VARCHAR2)
  as LANGUAGE JAVA NAME
   'oracle.xml.sql.dml.OracleXMLStaticSave.setRowTag(int, java.lang.String)';


  PROCEDURE setIgnoreCase(ctx IN ctxType, flag IN NUMBER)
  as LANGUAGE JAVA NAME
   'oracle.xml.sql.dml.OracleXMLStaticSave.setIgnoreCase(int, byte)';


  PROCEDURE setDateFormat(ctx IN ctxType, mask IN VARCHAR2)
  as LANGUAGE JAVA NAME
   'oracle.xml.sql.dml.OracleXMLStaticSave.setDateFormat(int, java.lang.String)';


  PROCEDURE setBatchSize(ctx IN ctxType, batchSize IN NUMBER)
  as LANGUAGE JAVA NAME
   'oracle.xml.sql.dml.OracleXMLStaticSave.setBatchSize(int, int)';


  PROCEDURE setCommitBatch(ctx IN ctxType, batchSize IN NUMBER)
  as LANGUAGE JAVA NAME
   'oracle.xml.sql.dml.OracleXMLStaticSave.setCommitBatch(int, int)';


  PROCEDURE setUpdateColumn(ctx IN ctxType, colName IN VARCHAR2)
  as LANGUAGE JAVA NAME
   'oracle.xml.sql.dml.OracleXMLStaticSave.setUpdateColumn(int, java.lang.String)';


  PROCEDURE clearUpdateColumnList(ctx IN ctxType)
  as LANGUAGE JAVA NAME
   'oracle.xml.sql.dml.OracleXMLStaticSave.clearUpdateColumnList(int)';


  PROCEDURE setKeyColumn(ctx IN ctxType, colName IN VARCHAR2)
  as LANGUAGE JAVA NAME
   'oracle.xml.sql.dml.OracleXMLStaticSave.setKeyColumn(int, java.lang.String)';


  PROCEDURE clearKeyColumnList(ctx IN ctxType)
  as LANGUAGE JAVA NAME
   'oracle.xml.sql.dml.OracleXMLStaticSave.clearKeyColumnList(int)';


  ------------------- save ----------------------------------------------------
  FUNCTION  insertXML(ctx IN ctxType, xDoc IN VARCHAR2) RETURN NUMBER
  as LANGUAGE JAVA NAME
   'oracle.xml.sql.dml.OracleXMLStaticSave.insertXML(int, java.lang.String) return int';

  FUNCTION  insertXML(ctx IN ctxType, xDoc IN CLOB) RETURN NUMBER
  as LANGUAGE JAVA NAME
   'oracle.xml.sql.dml.OracleXMLStaticSave.insertXML(int, oracle.sql.CLOB) return int';


  FUNCTION  updateXML(ctx IN ctxType, xDoc IN VARCHAR2) RETURN NUMBER
  as LANGUAGE JAVA NAME
   'oracle.xml.sql.dml.OracleXMLStaticSave.updateXML(int, java.lang.String) return int';

  FUNCTION  updateXML(ctx IN ctxType, xDoc IN CLOB) RETURN NUMBER
  as LANGUAGE JAVA NAME
   'oracle.xml.sql.dml.OracleXMLStaticSave.updateXML(int, oracle.sql.CLOB) return int';


  FUNCTION  deleteXML(ctx IN ctxType, xDoc IN VARCHAR2) RETURN NUMBER
  as LANGUAGE JAVA NAME
   'oracle.xml.sql.dml.OracleXMLStaticSave.deleteXML(int, java.lang.String) return int';

  FUNCTION  deleteXML(ctx IN ctxType, xDoc IN CLOB) RETURN NUMBER
  as LANGUAGE JAVA NAME
   'oracle.xml.sql.dml.OracleXMLStaticSave.deleteXML(int, oracle.sql.CLOB) return int';


  ------------------- misc ----------------------------------------------------
  PROCEDURE private_propOrigExc(ctx IN ctxType, flag IN NUMBER)
  as LANGUAGE JAVA NAME
   'oracle.xml.sql.save.OracleXMLStaticSave.propagateOriginalException(int, byte)';
  PROCEDURE propagateOriginalException(ctx IN ctxType, flag IN BOOLEAN) is
  begin
    if flag = true then
      private_propOrigExc(ctx, 1);
    else
      private_propOrigExc(ctx, 0);
    end if;
  end;

  PROCEDURE getExceptionContent(ctx IN ctxType, errNo OUT NUMBER, errMsg OUT VARCHAR2)
  as LANGUAGE JAVA NAME
   'oracle.xml.sql.save.OracleXMLStaticSave.getExceptionContent(int, int[], java.lang.String[])';


END DBMS_XMLSAVE;
/


GRANT EXECUTE, DEBUG ON DBMS_XMLSAVE TO EPAT;

GRANT EXECUTE, DEBUG ON DBMS_XMLSAVE TO ESCH;

