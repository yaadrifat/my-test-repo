CREATE OR REPLACE PACKAGE BODY        "PKG_PATIENT" AS
/******************************************************************************
   NAME:       PKG_PATIENT
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        9/30/2005   Sonia Abrol          1. Created this package body.
******************************************************************************/
/* returns a comma separated list of sites to which the patient is registered. Each site name is also followed by the respective facility id of the patient in that site*/
/* fk_site: pass 0 for all sites, pass pk_site for a specific site*/
  FUNCTION f_get_patsites(p_per IN NUMBER,p_site IN NUMBER) RETURN VARCHAR2 IS
 v_sites VARCHAR(4000);
  BEGIN

 IF p_site =  0 THEN
 FOR i IN  (SELECT site_name,pat_facilityid FROM ER_SITE, ER_PATFACILITY WHERE fk_per =  p_per AND patfacility_accessright > 0  AND fk_site=pk_site )
 LOOP
  v_sites := v_sites || ',' || i.site_name || ' [' || i.pat_facilityid || ']';
 END LOOP;
 IF LENGTH(v_sites) > 1 THEN
  v_sites := SUBSTR(v_sites,2);
 END IF;

 ELSIF p_site > 0 THEN

   SELECT site_name || ' [' || pat_facilityid || ']'
    INTO  v_sites
   FROM ER_SITE, ER_PATFACILITY
   WHERE fk_per =  p_per AND fk_site = p_site AND patfacility_accessright > 0 AND fk_site=pk_site;

 END IF;

    RETURN v_sites;
  END;

    /* returns a comma separated list of all patient codes along with the sites to which the patient is registered. */
  FUNCTION f_get_patcodes(p_per IN NUMBER) RETURN VARCHAR2 IS
   v_codes  VARCHAR(4000);
  BEGIN


 FOR i IN  (SELECT site_name,pat_facilityid FROM ER_SITE, ER_PATFACILITY WHERE fk_per =  p_per AND patfacility_accessright > 0  AND fk_site=pk_site )
 LOOP
  v_codes := v_codes || ',' || i.pat_facilityid  || ' [' || i.site_name || ']';
 END LOOP;

 IF LENGTH(v_codes) > 1 THEN
  v_codes := SUBSTR(v_codes,2);
 END IF;


    RETURN v_codes;
  END;

/* returns 1 if patient's enrolling site is the p_site, if enrolling site is null, checks for patient's default site. */
  FUNCTION f_is_enrollingsite(p_per IN NUMBER, p_site IN NUMBER, p_study NUMBER) RETURN NUMBER
  IS
   v_patsite NUMBER;
  BEGIN
            SELECT NVL(fk_site_enrolling,0)
    INTO v_patsite
    FROM ER_PATPROT
    WHERE fk_per = p_per AND fk_study = p_study AND patprot_stat = 1;

    IF v_patsite = p_site THEN
            -- same AS enrolling site
           RETURN 1;
    ELSE
        IF v_patsite = 0 THEN -- patient has no enrolling site

               SELECT fk_site
            INTO  v_patsite
            FROM ER_PER WHERE pk_per = p_per;

             IF v_patsite = p_site THEN
                     -- patient's default site is p_site
                      RETURN 1;
             ELSE
                  RETURN 0;
             END IF;

              ELSE --  v_patsite <> 0

                 RETURN 0;

       END IF;

    END IF;

  END;


     /* returns patient's enrolling site , if enrolling site is null, returns patient's default site. */
  FUNCTION f_get_enrollingsite(p_per IN NUMBER,  p_study NUMBER) RETURN NUMBER
  IS
   v_patsite NUMBER;
  BEGIN
            BEGIN
     SELECT NVL(fk_site_enrolling,0)
     INTO v_patsite
     FROM ER_PATPROT
     WHERE fk_per = p_per AND fk_study = p_study AND patprot_stat = 1;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     v_patsite := 0;
    END;

      IF v_patsite <= 0 THEN -- patient has no enrolling site
       BEGIN
           SELECT fk_site
        INTO  v_patsite
        FROM ER_PER WHERE pk_per = p_per;

                     EXCEPTION WHEN NO_DATA_FOUND THEN
              v_patsite := 0;
      END;
       END IF;

       RETURN v_patsite;

  END;


    /* Sonia Abrol, 03/07/06
  generates a  apatient code using ac_key (er_account and patient PK*/
   FUNCTION f_generate_CCID ( p_per IN NUMBER) RETURN VARCHAR2
   IS
     v_code VARCHAR(20);
  v_acKey VARCHAR2(10);
   BEGIN
       BEGIN
          SELECT  NVL(ac_key,'')
       INTO v_acKey
       FROM  ER_PER, ER_ACCOUNT WHERE pk_per = p_per AND fk_account = pk_account;

       SELECT v_acKey || LPAD(p_per, 9,'0')
       INTO v_code
       FROM dual   ;

        EXCEPTION WHEN NO_DATA_FOUND THEN
           v_code := '';
    END ;

    RETURN v_code;
   END;

   /* By Sonia Abrol : for patient enrollment notifications

   Modified on : 05/18/06 - add logic for appending any special prefic and getting a particular patient site id (example PCCTC)
   */
PROCEDURE SP_PATENRMAIL (
   p_per NUMBER,
   p_patstudystatsubtype   VARCHAR2,
   p_study NUMBER,
   p_statdate VARCHAR2
   )
 AS
 v_cnt NUMBER ;
 v_patprot NUMBER;
 V_POS		NUMBER :=0 ;
 v_params VARCHAR2(1000);
 V_USER_STR VARCHAR2(500);


 v_patstat_site_enrolling  NUMBER;

 v_enr_status_users VARCHAR2(250);
  v_enr_users VARCHAR2(250);
  v_studysite_lsamplesize NUMBER;
  v_sitename VARCHAR2(100);
  v_national_samplesize NUMBER;
  v_site_enrcount NUMBER;
 v_all_enrcount NUMBER;
-- v_studyNumber varchar2(30);
v_studyNumber VARCHAR2(100);--JM:
 v_patprot_stdid   VARCHAR2(30);
 v_status_desc VARCHAR2(300);
 v_patient_ids VARCHAR2(2000);
 v_is_reviewbased NUMBER;
 v_message_template CLOB;
v_message_text CLOB;
 isEnrMessage BOOLEAN := FALSE;
  v_fparam VARCHAR2(500);

  isReviewBasedText VARCHAR2(20);

  v_alert VARCHAR2(400) := '  ' ;
 v_action_text VARCHAR2(50) := '';
 v_subject VARCHAR2(255);
 v_enrolled_by VARCHAR2(500);
 v_email_rec_names VARCHAR2(1000);
 v_time VARCHAR2(200);
 v_tz_name  VARCHAR2(100);

 v_special_site NUMBER;

 v_enrolled_codepk NUMBER;

 BEGIN

 	  -- get the patient enrolling site:
	  BEGIN

		  SELECT NVL(fk_site_enrolling,0)  , NVL(patprot_patstdid,' ' ) , NVL(Usr_Lst(fk_user),' ' ) INTO  v_patstat_site_enrolling ,v_patprot_stdid, v_enrolled_by
		  FROM ER_PATPROT
	     WHERE fk_per = p_per AND fk_study = p_study AND patprot_stat = 1;

		 -- get special site for patient site id
		 BEGIN
			 SELECT Pkg_Util.f_to_number(ctrl_value)
			 INTO v_special_site
			 FROM ER_CTRLTAB WHERE ctrl_key = 'enrmail_site';
		EXCEPTION WHEN NO_DATA_FOUND THEN
				  v_special_site := 0;
		END;




		 IF v_patstat_site_enrolling > 0 THEN
		 	-- get the email settings, from study site

		 	     -- get enrolling site enrollment settings
						  SELECT NVL(enr_notifyto,''), NVL(enr_stat_notifyto,''), NVL(studysite_lsamplesize,0) ,site_name, NVL(IS_REVIEWBASED_ENR,0)
						  INTO  v_enr_users, v_enr_status_users, v_studysite_lsamplesize ,v_sitename ,v_is_reviewbased
						  FROM ER_STUDYSITES, ER_SITE  WHERE fk_study = p_study AND fk_site = v_patstat_site_enrolling AND pk_site = fk_site ;

						  IF v_is_reviewbased = 0 THEN
						  	 isReviewBasedText := 'Automatic';
							ELSE
							isReviewBasedText  := 'Review Based';
							END IF;
				-- get National sample size

						SELECT NVL(study_nsamplsize,0) ,study_number
						INTO v_national_samplesize ,v_studyNumber
						FROM ER_STUDY WHERE pk_study = p_study;

						-- get patient status desc
						SELECT codelst_desc INTO    v_status_desc FROM ER_CODELST WHERE trim(codelst_type) = 'patStatus' AND trim(codelst_subtyp) = p_patstudystatsubtype ;

						  -- get patient ids

						  IF (v_special_site > 0) THEN

								BEGIN
											 -- get ID for special site only
											 SELECT pat_facilityid || ' [' || site_name  || ']'
											   INTO	 v_patient_ids  FROM
											 ER_PATFACILITY , ER_SITE WHERE fk_per =  p_per AND fk_site=v_special_site AND pk_site = fk_site;

									EXCEPTION WHEN NO_DATA_FOUND THEN

														plog.DEBUG(pCTX,' patient enrollment/enr approval status related email,exception' || SQLERRM || ' pat facility ID for special site not found');
														SELECT  NVL(f_get_patcodes(p_per),' ' )  INTO v_patient_ids FROM dual;

									END ;

						ELSE
					  				 SELECT  NVL(f_get_patcodes(p_per),' ' )  INTO v_patient_ids FROM dual;
						END IF;

						 -------------
						 												-- get enr count

						--get enrolled status

						SELECT Pkg_Impex.getcodepk('enrolled', 'patStatus')
						INTO v_enrolled_codepk
					    FROM dual;

						SELECT COUNT(DISTINCT pat.fk_per)
						INTO v_site_enrcount
						FROM ER_PATPROT pp, ER_PATSTUDYSTAT pat
						WHERE pat.fk_study = p_study AND fk_codelst_stat = v_enrolled_codepk AND
						pp.fk_study = pat.fk_study AND pp.fk_per = pat.fk_per AND
						pp.patprot_stat = 1 AND pp.fk_site_enrolling = v_patstat_site_enrolling AND pp.patprot_enroldt IS NOT NULL;

						SELECT COUNT(DISTINCT fk_per)
						INTO v_all_enrcount
						FROM ER_PATSTUDYSTAT WHERE fk_study = p_study AND fk_codelst_stat = v_enrolled_codepk;

						IF v_studysite_lsamplesize < v_site_enrcount THEN
						   v_alert := CHR(13) || 'Alert: Organization target accrual has been exceeded';
						  END IF;

						 IF v_national_samplesize <   v_all_enrcount THEN
						 			v_alert := v_alert || CHR(13) ||  'Alert: National target accrual has been exceeded';
						 END IF;

						 IF ( LENGTH(NVL(v_alert,'')) = 0 ) THEN
						 	v_alert := '  ';
						 END IF;


						 ---------------


						 IF (p_patstudystatsubtype = 'enrolled') THEN
						 			  V_USER_STR  :=v_enr_users ;

									  v_message_template := PKG_COMMON.SCH_GETMAILMSG('pat_enr');

									  IF v_is_reviewbased = 0 THEN --automatic
									    v_action_text:= 'None' ;
									 ELSE -- revie wbased
									 	v_action_text:= 'Registration Information Review/Approval';
									END IF;


									   isEnrMessage := TRUE;
					   ELSIF (p_patstudystatsubtype = 'enroll_appr' OR p_patstudystatsubtype = 'enroll_denied') THEN
	   		 						V_USER_STR  :=  v_enr_status_users;

									 v_message_template := PKG_COMMON.SCH_GETMAILMSG('p_enrstat');

									 isEnrMessage := FALSE;

									 IF (p_patstudystatsubtype = 'enroll_appr' ) THEN
									 			  v_action_text := 'None';
					  							  v_subject := 'Registration Completed for Protocol:  ' || v_studyNumber || ', Patient ID: ' || v_patient_ids;

									ELSE
												  v_action_text := 'Yes (contact registrar)';
												  v_subject := 'Registration Failed for Protocol:  ' || v_studyNumber || ', Patient ID: ' || v_patient_ids;
									END IF;

					END IF;


					SELECT NVL(Usr_Lst(V_USER_STR),' ') INTO v_email_rec_names FROM dual;

					SELECT TO_CHAR(SYSDATE,'dd/mm/yyyy hh24:mi:ss') INTO v_time FROM dual;

					BEGIN
							SELECT tz_name INTO v_tz_name
							FROM SCH_CODELST, SCH_TIMEZONES WHERE CODELST_TYPE = 'DBTimezone' AND
										pk_tz = codelst_subtyp;
					EXCEPTION WHEN NO_DATA_FOUND THEN
							  v_tz_name := '';
					END ;

					v_time := v_time || ' ' || v_tz_name;


						  --prepare message

					  IF isEnrMessage THEN --prepare enrollment message
		  	                    v_fparam :=  v_sitename || '~'||  isReviewBasedText   ||'~'|| v_patient_ids  ||'~'|| v_studyNumber  ||'~'||  v_status_desc
										 	 			||'~'||  Pkg_Util.date_to_char(p_statdate)   || '~' ||  v_national_samplesize || '~' || v_studysite_lsamplesize
														|| '~' ||  v_all_enrcount  	|| '~' ||     v_site_enrcount    	|| '~' ||     v_alert   || '~' || v_action_text || '~' ||   v_enrolled_by   || '~' ||  v_email_rec_names    || '~' || v_time         ;

									v_subject := '[' || isReviewBasedText || '] Registration Requested for Protocol ' || v_studyNumber || ', Patient ID: ' || v_patient_ids;

					  ELSE --prepare enrollment status message
					  	        v_fparam :=   v_patient_ids ||'~'||  v_studyNumber  ||'~'||  v_status_desc
										 	 			||'~'||  Pkg_Util.date_to_char(p_statdate)   || '~'|| v_action_text  || '~'||  v_sitename || '~' ||  v_national_samplesize || '~' || v_studysite_lsamplesize
														|| '~' ||  v_all_enrcount  	|| '~' ||     v_site_enrcount    	|| '~' ||     v_alert  || '~' ||  isReviewBasedText 	|| '~' ||   v_enrolled_by   || '~' ||  v_email_rec_names      || '~' ||  v_time     ;


					  END IF;

               	   v_message_text :=    PKG_COMMON.SCH_GETMAIL(v_message_template ,v_fparam);


		 END IF;  -- if enrolling site found


		   V_USER_STR :=  V_USER_STR|| ',' ;
		   v_params := V_USER_STR ;

        LOOP
		    V_CNT := V_CNT + 1;
		    V_POS := INSTR (V_USER_STR, ',');
		    V_PARAMS := SUBSTR (V_USER_STR, 1, V_POS - 1);
		    EXIT WHEN V_PARAMS IS NULL;

		   	INSERT INTO sch_dispatchmsg (
				PK_MSG  , MSG_SENDON  ,MSG_STATUS ,MSG_TYPE , 	MSG_TEXT ,FK_PAT    ,MSG_SUBJECT      )
			      VALUES ( SCH_DISPATCHMSG_SEQ1.NEXTVAL ,SYSDATE,0,'U',	 v_message_text,v_params,v_subject ) ;

			   V_USER_STR := SUBSTR (V_USER_STR, V_POS + 1);
           --			p('DONE');

		END LOOP ; /* end loop for multiple users*/

		EXCEPTION WHEN OTHERS THEN
				  plog.DEBUG(pCTX,'could not send patient enrollment/enr approval status related email,exception' || SQLERRM);
	END ;



	 END SP_PATENRMAIL;





   /* Sonia Abrol, 04/19/06
   return patient enrollment date in a study*/
   FUNCTION f_get_enrollment_date(p_per IN NUMBER, p_study NUMBER)
   RETURN DATE
   IS
   v_date DATE;
   BEGIN
   		 BEGIN
	   		 SELECT patprot_enroldt INTO v_date
			 FROM ER_PATPROT WHERE fk_study = p_study AND
			 fk_per = p_per AND patprot_stat = 1 AND ROWNUM < 2;

			 	 RETURN v_date;
		EXCEPTION WHEN NO_DATA_FOUND THEN
				  RETURN NULL;
		END;
   END;


        /* return patient study linking date*
		Sonia Abrol, 04/19/06 */
   FUNCTION f_get_study_linking_date(p_per IN NUMBER, p_study NUMBER) RETURN DATE
   IS
   v_date DATE;
   BEGIN
      BEGIN
         SELECT MIN (patstudystat_date)
           INTO v_date
           FROM ER_PATSTUDYSTAT
          WHERE fk_per = p_per
            AND fk_study = p_study
            AND ROWNUM < 2;

         v_date := TRUNC (v_date);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN NULL;
      END;

      RETURN v_date;

   END;

    /* returns 1 if p_site is patient facility site, return 0 if not
	 Sonia ABrol 08/07/06 */

  FUNCTION f_is_patfacilitysite(p_per IN NUMBER, p_site IN NUMBER) RETURN NUMBER
  IS
  	v_count NUMBER;
  	BEGIN
		 BEGIN
		 SELECT COUNT(*)
		 INTO v_count
		 FROM ER_PATFACILITY WHERE fk_per = p_per AND fk_site = p_site AND
		 patfacility_accessright > 0;
		 EXCEPTION WHEN NO_DATA_FOUND THEN
		   v_count := 0;
		 END;
		IF v_count > 0 THEN
		   v_count := 1;
		ELSE
		 	v_count := 0;

		END IF;

		RETURN v_count;
	END;

   /* returns 1 if p_code is a patient facility ID , return 0 if not*/
    FUNCTION f_is_patfacilityid(p_per IN NUMBER, p_code IN VARCHAR2) RETURN NUMBER
	  IS
  	v_count NUMBER;
  	BEGIN
		 BEGIN
		 SELECT COUNT(*)
		 INTO v_count
		 FROM ER_PATFACILITY WHERE fk_per = p_per AND trim(LOWER(pat_facilityid)) = trim(LOWER(p_code)) AND
		 patfacility_accessright > 0;
		 EXCEPTION WHEN NO_DATA_FOUND THEN
		   v_count := 0;
		 END;
		IF v_count > 0 THEN
		   v_count := 1;
		ELSE
		 	v_count := 0;

		END IF;

		RETURN v_count;
	END;



 PROCEDURE sp_notify_portal_patient (
   p_per Number,
   p_loginname VARCHAR2,
   p_loginpass Varchar2,
   p_usr Number,
   o_out OUT Varchar2
   )
   AS
   	v_message_template Clob;
	v_message_text Clob;
	v_fparam Varchar2(4000);
	v_subject Varchar2(1000);
	v_footer clob;
	v_portal_siteurl Varchar2(500);
   BEGIN
			BEGIN

				v_message_template := PKG_COMMON.SCH_GETMAILMSG('port_login');

				v_footer := PKG_COMMON.SCH_GETMAILMSG('foot_sign');

				begin
					select ctrl_value into
					v_portal_siteurl
					from ER_CTRLTAB where ctrl_key = 'port_url';
				exception when NO_DATA_FOUND then
					v_portal_siteurl := 'http://www.veloseresearch.com/eres/jsp/patientlogin.jsp';
				end;

				select msgtxt_subject
				into v_subject
				from esch.sch_msgtxt where msgtxt_type = 'port_login';


				v_fparam := v_portal_siteurl  || '~' || p_loginname || '~' || p_loginpass || '~' || v_footer;

				v_message_text :=    PKG_COMMON.SCH_GETMAIL(v_message_template ,v_fparam);

				INSERT INTO sch_dispatchmsg (
				PK_MSG  , MSG_SENDON  ,MSG_STATUS ,MSG_TYPE , 	MSG_TEXT ,FK_PAT    ,MSG_SUBJECT  ,msg_isclubbed    )
				 VALUES ( SCH_DISPATCHMSG_SEQ1.NEXTVAL ,SYSDATE,0,'PC',	 v_message_text,p_per,v_subject ,1) ;

				commit;
			Exception when OTHERS then
				o_out := 'Exception in sending notification for patient portal : ' || sqlerrm;

			END;

   END;

/* returns patprot for latest patient schedule for a calendar in  a study*/
  function fget_latest_sch_patprot(p_per IN NUMBER, p_protocol NUMBER) RETURN Number
  IS
   v_patprot Number;
   BEGIN
      BEGIN

		select max(pk_patprot)
		into v_patprot
		from er_patprot where fk_per = p_per
             and fk_protocol = p_protocol  ;

      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN NULL;
      END;

      RETURN v_patprot;

   END;


END Pkg_Patient;
/


CREATE SYNONYM ESCH.PKG_PATIENT FOR PKG_PATIENT;


CREATE SYNONYM EPAT.PKG_PATIENT FOR PKG_PATIENT;


GRANT EXECUTE, DEBUG ON PKG_PATIENT TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_PATIENT TO ESCH;

