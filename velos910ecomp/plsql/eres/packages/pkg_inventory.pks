CREATE OR REPLACE PACKAGE      Pkg_Inventory AS
/******************************************************************************
   NAME:       PKG_INVENTORY
   PURPOSE: repository of inventory reports

   REVISIONS: 1.0
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        08/28/2009  Sammie           1. Created this package for inventory reports.
   2.0        10/16/2009  Sammie           2. Updated package for specimen trail reports.
   2.1        10/29/2009  Sammie           3. Updated package for specimen trail reports.
******************************************************************************/
  /*This function gives Cummulative specCount of a storage*/
  function f_get_storageSpecCount(p_pk_storage Number) return number;

  /*This function gives Cummulative capacity of a storage*/
  function f_get_storageCapacity(p_pk_storage Number) return number;

  function f_get_storageinfo(p_pk_storage Number,p_RowCnt number,p_Level number) return clob;

  function f_get_storage_trail(p_accId number, p_storageId varchar) return clob;

  function f_get_specimeninfo(p_pk_specimen Number,p_RowCnt number,p_Level number) return clob;

  function f_get_specimen_trail(p_userId number, p_studyId varchar, p_fromDate date, p_toDate date) return clob;

  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'Pkg_Inventory', pLEVEL  => Plog.LFATAL);

END Pkg_Inventory;
/


