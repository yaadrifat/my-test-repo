CREATE OR REPLACE PACKAGE BODY        "PKG_IMPEXCOMMON" 
AS
   PROCEDURE TEST_SP_GETELEMENT_VALUE
 AS
   v_cl CLOB;
   v_val VARCHAR2(4000);
   v_modkeys TYPES.SMALL_STRING_ARRAY := NEW TYPES.SMALL_STRING_ARRAY();
   v_counter NUMBER := 1;
   v_sucess NUMBER;
     v_ret LONG;

 BEGIN
   v_cl := '<exp><study>256763</study><account>2222</account><modules><module name="study"><modulePK>23</modulePK>
     </MODULE><MODULE NAME="study_ver" expAll="Y"></MODULE><MODULE NAME="study_cal" expAll="N">
     <modulePK>2376</modulePK><modulePK>2388</modulePK></MODULE></modules></EXP>';

   --SP_GETELEMENT_VALUES (v_cl,'study',v_modkeys);
   SP_GETMODULEKEYS(sys.XMLTYPE.createXML(v_cl),'sftudy_cal',v_modkeys,v_sucess );

   --p('v_sucess ' || v_sucess );
   WHILE v_counter <= v_modkeys.COUNT()
   LOOP
   -- p( 'v_modkeys*' || v_modkeys(v_counter) || '*');
    v_counter := v_counter + 1;
   END LOOP;

   SP_ARRAY2STRING(v_modkeys,v_ret );


  --p(v_ret);

 END;

 PROCEDURE SP_GETMODULEKEYS(p_xml sys.XMLTYPE,p_modulename VARCHAR2,o_keys OUT TYPES.SMALL_STRING_ARRAY,o_success OUT NUMBER)
 AS
   /****************************************************************************************************
   ** ** Author: Sonia Sahni 28th Jan 2004
   ** Purpose ; Returns PKs for a module
   ********************************************************************************************************/

   v_mainxml sys.XMLTYPE ;
   v_modulefrag CLOB ;

   v_path VARCHAR2(200);

 BEGIN
  v_mainxml := p_xml;
  o_keys := NEW TYPES.SMALL_STRING_ARRAY();

  --prepare xpath for module name

  v_path := '/exp/modules/module[@name="'|| p_modulename ||'"]';

  --use this xpath to extract the relevant xml fragment
  SELECT EXTRACT(v_mainxml,v_path).getClobVal()
  INTO v_modulefrag
  FROM dual;

  -- check if  v_modulefrag is not null

  IF v_modulefrag IS NOT NULL THEN
   SP_GETELEMENT_VALUES(v_modulefrag,'modulePK',o_keys);
   --p(dbms_lob.substr(v_modulefrag,400,1));
   --p(o_keys.count());
   o_success := 0;
  ELSE
   o_success := -1;
  END IF;



 END;

PROCEDURE SP_GETELEMENT_VALUES (p_xml CLOB, p_searchparam VARCHAR2, o_values OUT TYPES.SMALL_STRING_ARRAY )
 AS
   /****************************************************************************************************
   ** ** Author: Sonia Sahni 27th Jan 2004
   ** Searches for search parameter in the xml clob and returns its value(s)
   ********************************************************************************************************/

    v_cnt NUMBER;
 i NUMBER := 1;
 v_param VARCHAR2(50);
 v_paramvalues VARCHAR2(4000);

 v_blankxml    CLOB;

 v_savexml CLOB;
 v_doc       dbms_xmldom.DOMDocument;

    ndoc      dbms_xmldom.DOMNode;
    buf       VARCHAR2(2000);
    nodelist  dbms_xmldom.DOMNodelist;
    docelem   dbms_xmldom.DOMElement;
 node      dbms_xmldom.DOMNode;
    childnode dbms_xmldom.DOMNode;

    nodelistSize NUMBER := 0;
    nodelistSizeCount NUMBER := 0;
    myParser    dbms_xmlparser.Parser;
 v_value VARCHAR2(4000);

    BEGIN

 o_values := NEW TYPES.SMALL_STRING_ARRAY();

  --get blank xml for the form

    v_blankxml := p_xml;

   -- Create DOMDocument handle:

 myParser := dbms_xmlparser.newParser; --**
 dbms_xmlparser.parseClob(myParser, v_blankxml);
 v_doc     := dbms_xmlparser.getDocument(myParser);
 docelem := dbms_xmldom.getDocumentElement( v_doc );


  -- Get First Child Of the Node
    v_param := p_searchparam;
    nodelist := dbms_xmldom.getElementsByTagName(docelem, v_param);

   nodelistSize := dbms_xmldom.getLength(nodelist) ;
   --p('nodelistSize' || nodelistSize);
   nodelistSizeCount := 0;

   WHILE nodelistSizeCount < nodelistSize LOOP

    node := dbms_xmldom.item(nodelist,nodelistSizeCount);

   IF  (dbms_xmldom.isNull(node) = FALSE)  THEN
         --p('got node');
      childnode := dbms_xmldom.getFirstChild(node);
      IF  (dbms_xmldom.isNull(childnode) = FALSE)  THEN

       --p(dbms_xmldom.getNodeValue(childnode));
       v_value := dbms_xmldom.getNodeValue(childnode);

     o_values.EXTEND();
  o_values(nodelistSizeCount + 1) := v_value ;

    --p('v_value' || v_value);
     END IF;
   END IF;
    nodelistSizeCount :=  nodelistSizeCount + 1;
   END LOOP;
 END;


PROCEDURE SP_ARRAY2STRING (p_arr TYPES.SMALL_STRING_ARRAY, o_str OUT LONG)
AS
/* Author - Sonia Sahni
   Date - 28 Jan 04
  Purpose - Converts a small_string_array to a comma separated String */

  v_arr TYPES.SMALL_STRING_ARRAY := NEW TYPES.SMALL_STRING_ARRAY();

  v_count NUMBER;
  v_counter NUMBER := 1;

  v_return_String LONG;

BEGIN
--p('in proc');
  v_arr := p_arr;
  v_count := v_arr.COUNT();
  --p('v_count' || v_count);

   WHILE v_counter <= v_count
   LOOP
    v_return_String := v_return_String || ',' || v_arr(v_counter);
      --p('v_return_String' || v_return_String);
   v_counter := v_counter + 1;
   END LOOP;

   v_return_String := SUBSTR(v_return_String,2);

  o_str := v_return_String;

END;

PROCEDURE SP_CREATE_DB_OBJECT(p_object_type VARCHAR2, p_object_name VARCHAR2, p_object_ddl VARCHAR2, o_success OUT NUMBER)
AS
  v_dropsql VARCHAR2(4000);
BEGIN

  BEGIN
      v_dropsql := 'drop ' || p_object_type || ' ' || p_object_name;

     EXECUTE IMMEDIATE v_dropsql;

  EXCEPTION WHEN OTHERS THEN
      Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> SP_CREATE_DB_OBJECT executing:'  || SQLERRM, o_success );
  END;

 BEGIN
   Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> SP_CREATE_DB_OBJECT executing:' || p_object_ddl, o_success );

   EXECUTE IMMEDIATE p_object_ddl ;

   o_success := 0;

   EXCEPTION WHEN OTHERS THEN
    Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> EXCEPTION IN SP_CREATE_DB_OBJECT ' || SQLERRM , o_success );
       o_success := -1;
 END;



END SP_CREATE_DB_OBJECT;

PROCEDURE SP_CREATE_EXPREQUEST( p_req_by IN STRING, p_req_datetime IN DATE, p_site_arr IN ARRAY_STRING, p_req_details IN CLOB, p_req_status IN String, p_msg_status IN String, o_exp_id OUT NUMBER)
AS
v_exp NUMBER;
v_success NUMBER;

v_expsql LONG;

v_explogpk_sql VARCHAR2(100);

v_sitesql LONG;
v_sitepk NUMBER;

v_sitepk_sql VARCHAR2(100);
v_site NUMBER;

v_xml SYS.XMLTYPE;


BEGIN

 BEGIN

  v_explogpk_sql := 'Select SEQ_ER_EXPREQLOG.nextval from dual';

  EXECUTE IMMEDIATE v_explogpk_sql INTO v_exp;


  -- log export request

  v_expsql := 'INSERT INTO ER_EXPREQLOG (PK_EXPREQLOG,EXP_DATETIME , EXP_STATUS , EXP_REQUESTEDBY , EXP_XML, EXP_MSG_STATUS)  VALUES (:1,:2,:3,:4,:5,:6)';


  SELECT XMLTYPE.createXML(p_req_details)
  INTO v_xml
  FROM dual;


  EXECUTE IMMEDIATE v_expsql USING v_exp, p_req_datetime,p_req_status,p_req_by,v_xml, p_msg_status;

  -- insert records for sites

  v_sitesql := ' INSERT INTO  er_expsitelog ( PK_EXPSITELOG, FK_EXPREQLOG , EXP_ACK_FROMSITE , FK_A2ASITES )
    VALUES(:1,:2,:3,:4) ' ;

   FOR i IN 1..p_site_arr.COUNT
   LOOP

      v_site := p_site_arr(i) ;

      v_sitepk_sql := 'Select SEQ_ER_EXPSITELOG.nextval from dual';

    EXECUTE IMMEDIATE v_sitepk_sql INTO v_sitepk ;

      EXECUTE IMMEDIATE v_sitesql USING  v_sitepk ,v_exp,'N',v_site;

   END LOOP;

  COMMIT;

  o_exp_id := v_exp;

 EXCEPTION
 WHEN OTHERS THEN
  o_exp_id := -1;

  P(SQLERRM);

    Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> EXCEPTION IN SP_CREATE_EXPREQUEST ' || SQLERRM , v_success );

  RAISE_APPLICATION_ERROR (-20000,
              'Unhandled Oracle error ' || SUBSTR (SQLERRM, 1, 100)
            );
 END;


END;

PROCEDURE SP_CREATE_IMPREQUEST( p_req_by IN STRING, p_req_datetime IN DATE, p_req_details IN CLOB, p_req_status IN String,p_siteCode IN VARCHAR2, o_imp_id OUT NUMBER)
AS
v_imp NUMBER;
v_success NUMBER;

v_impsql LONG;

v_implogpk_sql VARCHAR2(100);
v_xml SYS.XMLTYPE;


BEGIN

 BEGIN

  v_implogpk_sql := 'Select SEQ_ER_IMPREQLOG.nextval from dual';

  EXECUTE IMMEDIATE v_implogpk_sql INTO v_imp;

  -- log export request

  v_impsql := 'INSERT INTO ER_IMPREQLOG (PK_IMPREQLOG,IMP_DATETIME , IMP_STATUS , IMP_REQUESTEDBY , EXP_XML,SITE_CODE)
  VALUES (:1,:2,:3,:4,:5,:6)'  ;

  IF p_req_details <> NULL THEN

     SELECT XMLTYPE.createXML(p_req_details)
    INTO v_xml
   FROM dual;
   END IF;

  EXECUTE IMMEDIATE v_impsql USING v_imp, p_req_datetime,p_req_status,p_req_by,v_xml,p_siteCode;


  COMMIT;

  o_imp_id := v_imp;

 EXCEPTION
 WHEN OTHERS THEN
  o_imp_id := -1;
    Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> EXCEPTION IN SP_CREATE_IMPREQUEST ' || SQLERRM , v_success );
       RAISE_APPLICATION_ERROR (-20000,
              'Unhandled Oracle error ' || SUBSTR (SQLERRM, 1, 100)
            );
 END;


END;

PROCEDURE SP_UPDATE_IMPREQUEST( p_impId IN NUMBER , p_expId IN NUMBER, p_status IN NUMBER, p_exp IN CLOB, o_success OUT NUMBER)
AS

v_success NUMBER;

v_sql LONG;
v_xml sys.XMLTYPE ;
v_action CHAR(1);


BEGIN

 BEGIN

  v_sql := 'Update er_impreqlog set IMP_STATUS = :1, exp_xml = :2 , exp_id = :3  where PK_IMPREQLOG = :4';


  BEGIN
    SELECT XMLTYPE.createXML(p_exp)
    INTO v_xml
    FROM dual;
     EXCEPTION
  WHEN OTHERS THEN
    Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> EXCEPTION IN SP_UPDATE_IMPREQUEST ' || SQLERRM , v_success );
  END;

  EXECUTE IMMEDIATE v_sql USING p_status, v_xml, p_expId , p_impId;


  COMMIT;

  o_success := 0;

 EXCEPTION
 WHEN OTHERS THEN
   o_success := -1;

    Pkg_Impexlogging.SP_RECORDLOG ('IMPEX--> EXCEPTION IN SP_UPDATE_IMPREQUEST ' || SQLERRM , v_success );
       RAISE_APPLICATION_ERROR (-20000,
              'Unhandled Oracle error ' || SUBSTR (SQLERRM, 1, 100)
            );
 END;


END;

PROCEDURE SP_GETELEMENT_VALUES_STRING (p_xml CLOB, p_searchparam VARCHAR2, o_values OUT VARCHAR2 )
AS
  v_modkeys TYPES.SMALL_STRING_ARRAY := NEW TYPES.SMALL_STRING_ARRAY();
  v_count NUMBER;
BEGIN

   SP_GETELEMENT_VALUES (p_xml,p_searchparam,v_modkeys);
       v_count  := v_modkeys.COUNT();

  IF v_count > 0 THEN
     SP_ARRAY2STRING(v_modkeys, o_values);
  END IF;

END;



 PROCEDURE SP_GET_SITEPK(P_ORIGPK NUMBER, P_TABLE VARCHAR2, P_SITECODE VARCHAR2, O_PK OUT NUMBER , O_ERRMSG OUT VARCHAR2,p_originating_sitecode IN VARCHAr2)
 AS
 /** Author : Sonia Sahni
    Date: 02/20/04
  Purpose: Used in import procedures. Finds out the new SITE PK generated for exported data's SPONSOR PK
  Returns -1 if the pk_doesnot exist
  */
  v_sql LONG;
  v_sitepk NUMBER;

 BEGIN
  IF P_ORIGPK IS NOT NULL AND P_ORIGPK > 0 THEN
    v_sql := 'Select SITEPKMAP_SITEPK from er_sitepkmap
        WHERE SITEPKMAP_TABLE = :1 AND SITEPKMAP_SITECODE = :2 AND SITEPKMAP_SPONSORPK  = :3 AND ORIGINATING_SITECODE = :4';

 BEGIN
     EXECUTE IMMEDIATE v_sql INTO v_sitepk USING P_TABLE, P_SITECODE , P_ORIGPK,p_originating_sitecode ;
        O_PK := v_sitepk ;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
     O_ERRMSG := SQLERRM;
   O_PK := 0;
   WHEN OTHERS THEN
   O_ERRMSG := SQLERRM;
   O_PK := -1;
 END;
END IF;
 END;

 PROCEDURE SP_SET_SITEPK(P_ORIGPK NUMBER, P_TABLE VARCHAR2, P_SITECODE VARCHAR2, V_NEWPK NUMBER, O_ERRMSG OUT VARCHAR2,
 p_originating_sitecode IN VARCHAr2)
 AS
 /** Author : Sonia Sahni
    Date: 02/20/04
  Purpose: Used in import procedures. Creates a mapping for the new SITE PK generated for exported data's SPONSOR PK
  Returns Error message if any
  */
  v_sql LONG;

  v_seq_sql VARCHAR2(200);
  v_success NUMBER;
  v_sitepkmap NUMBER;

 BEGIN

    v_seq_sql := 'Select SEQ_er_sitepkmap.nextval from dual' ;



    v_sql := 'INSERT INTO er_sitepkmap (PK_SITEPKMAP, SITEPKMAP_TABLE ,SITEPKMAP_SITECODE, SITEPKMAP_SPONSORPK , SITEPKMAP_SITEPK,ORIGINATING_SITECODE
                   )
        VALUES(:1,:2,:3,:4,:5,:6 ) ';


 BEGIN

  EXECUTE IMMEDIATE v_seq_sql INTO v_sitepkmap ;
  EXECUTE IMMEDIATE v_sql USING v_sitepkmap, P_TABLE ,P_SITECODE, P_ORIGPK,V_NEWPK ,p_originating_sitecode ;

   EXCEPTION

 WHEN OTHERS THEN
  O_ERRMSG := SQLERRM;

 END;

 -- the record is not committed here, it will be comitted with the rest of the data
 END;


 PROCEDURE SP_GET_SPONSORPK(P_ORIGPK NUMBER, P_TABLE VARCHAR2, P_SITECODE VARCHAR2, O_PK OUT NUMBER , O_ERRMSG OUT VARCHAR2, p_account IN NUMBER)
 AS
 /** Author : Sonia Sahni
    Date: 04/26/04
  Purpose: Used in import procedures. Finds out the new SPONSOR PK generated for SITE's data
  Returns -1 if the pk_doesnot exist
  Modified by Sonia Abrol 03/15/06 , include sponsor's fk_account in query
  */
  v_sql LONG;
  v_sponsorpk NUMBER;

 BEGIN
   IF P_ORIGPK IS NOT NULL AND P_ORIGPK > 0 THEN
         v_sql := 'Select SPONSORPKMAP_SPONSORPK from er_sponsorpkmap
         WHERE SPONSORPKMAP_TABLE = :1 AND SPONSORPKMAP_SITECODE = :2 AND SPONSORPKMAP_SITEPK  = :3 and fk_account =  :4';

     BEGIN
      EXECUTE IMMEDIATE v_sql INTO v_sponsorpk USING P_TABLE, P_SITECODE , P_ORIGPK,P_ACCOUNT ;
      O_PK := v_sponsorpk ;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        O_ERRMSG := SQLERRM;
        O_PK :=  0;
      WHEN OTHERS THEN
        O_ERRMSG := SQLERRM;
        O_PK :=  -1;
       END;
 END IF;
 END;


 PROCEDURE SP_SET_SPONSORPK(P_ORIGPK NUMBER, P_TABLE VARCHAR2, P_SITECODE VARCHAR2, V_NEWPK NUMBER, O_ERRMSG OUT VARCHAR2,p_account IN NUMBER)
 AS
 /** Author : Sonia Sahni
    Date: 04/27/04
  Purpose: Used in import procedures. Creates a mapping for the new SPONSOR PK generated for SITE'S data
  Returns Error message if any

    Modified by Sonia Abrol 03/15/06 , include sponsor's fk_account in the mapping record

  */
  v_sql LONG;

  v_seq_sql VARCHAR2(200);
  v_success NUMBER;
  v_sponsorpkmap NUMBER;

 BEGIN

    v_seq_sql := 'Select seq_er_sponsorpkmap.nextval from dual' ;



    v_sql := 'INSERT INTO er_sponsorpkmap (PK_SPONSORPKMAP, SPONSORPKMAP_TABLE ,SPONSORPKMAP_SITECODE, SPONSORPKMAP_SPONSORPK , SPONSORPKMAP_SITEPK,
         FK_ACCOUNT
                   )
        VALUES(:1,:2,:3,:4,:5,:6 ) ';
 BEGIN

  EXECUTE IMMEDIATE v_seq_sql INTO v_sponsorpkmap ;
  EXECUTE IMMEDIATE v_sql USING v_sponsorpkmap, P_TABLE ,P_SITECODE,V_NEWPK,P_ORIGPK,P_ACCOUNT ;

   EXCEPTION

 WHEN OTHERS THEN
  O_ERRMSG := SQLERRM;

 END;

 -- the record is not committed here, it will be comitted with the rest of the data
 END;


END Pkg_Impexcommon;
/


CREATE SYNONYM ESCH.PKG_IMPEXCOMMON FOR PKG_IMPEXCOMMON;


CREATE SYNONYM EPAT.PKG_IMPEXCOMMON FOR PKG_IMPEXCOMMON;


GRANT EXECUTE, DEBUG ON PKG_IMPEXCOMMON TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_IMPEXCOMMON TO ESCH;

