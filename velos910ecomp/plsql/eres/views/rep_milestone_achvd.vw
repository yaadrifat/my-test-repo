/* Formatted on 2/9/2010 1:39:56 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_MILESTONE_ACHVD
(
   MSACH_RESPONSE_ID,
   MSACH_MILESTONE_DESC,
   MSACH_PATIENT_ID,
   MSACH_STUDY_NUMBER,
   MSACH_PTSTUDY_ID,
   MSACH_ACH_DATE,
   RID,
   CREATOR,
   CREATED_ON,
   LAST_MODIFIED_DATE,
   LAST_MODIFIED_BY,
   MSACH_IS_COMPLETE,
   FK_STUDY,
   FK_ACCOUNT
)
AS
   SELECT   PK_MILEACHIEVED MSACH_RESPONSE_ID,
            Pkg_Milestone_New.f_getMilestoneDesc (FK_MILESTONE)
               MSACH_MILESTONE_DESC,
            (SELECT   PER_CODE
               FROM   ER_PER
              WHERE   PK_PER = FK_PER)
               MSACH_PATIENT_ID,
            (SELECT   STUDY_NUMBER
               FROM   ER_STUDY
              WHERE   PK_STUDY = FK_STUDY)
               MSACH_STUDY_NUMBER,
            (SELECT   PATPROT_PATSTDID
               FROM   ER_PATPROT pp
              WHERE       pp.fk_per = a.fk_per
                      AND pp.fk_study = a.fk_study
                      AND pp.patprot_stat = 1
                      AND ROWNUM < 2)
               MSACH_PTSTUDY_ID,
            TRUNC (ACH_DATE) MSACH_ACH_DATE,
            RID,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = a.CREATOR)
               creator,
            CREATED_ON,
            LAST_MODIFIED_DATE,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = a.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            F_GET_YESNO (IS_COMPLETE) MSACH_IS_COMPLETE,
            fk_study,
            (SELECT   fk_account
               FROM   ER_STUDY
              WHERE   pk_study = fk_study)
               fk_account
     FROM   ER_MILEACHIEVED a
    WHERE   IS_COMPLETE = 1;


