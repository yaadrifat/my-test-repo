CREATE OR REPLACE FUNCTION        "F_GETAE_NOTIFY" (p_adveve NUMBER)
RETURN VARCHAR
IS
v_retvalues VARCHAR2(4000);
v_counter NUMBER := 0 ;
BEGIN
  FOR i IN (SELECT (SELECT codelst_desc FROM sch_codelst WHERE pk_codelst = fk_codelst_not_type) || ' - '  || TO_CHAR(advnotify_date,PKG_DATEUTIL.F_GET_DATEFORMAT) AS adv_notify
 FROM sch_advnotify
 WHERE fk_adverseve = p_adveve AND
 advnotify_value = 1)
  LOOP
	  	  v_counter := v_counter + 1;
		  IF v_counter > 1 THEN
		  	 v_retvalues :=  v_retvalues || ', ';
		  END IF;
		  v_retvalues := v_retvalues || '[' || i.adv_notify || ']';
  END LOOP;
  RETURN v_retvalues ;
END ;
/


CREATE SYNONYM ESCH.F_GETAE_NOTIFY FOR F_GETAE_NOTIFY;


CREATE SYNONYM EPAT.F_GETAE_NOTIFY FOR F_GETAE_NOTIFY;


GRANT EXECUTE, DEBUG ON F_GETAE_NOTIFY TO EPAT;

GRANT EXECUTE, DEBUG ON F_GETAE_NOTIFY TO ESCH;

