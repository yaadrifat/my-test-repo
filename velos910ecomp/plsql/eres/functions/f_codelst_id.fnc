CREATE OR REPLACE FUNCTION        "F_CODELST_ID" (p_type VARCHAR2, p_subtype VARCHAR2)
RETURN NUMBER
IS
  v_pk_codelst NUMBER;
BEGIN
	 SELECT pk_codelst INTO v_pk_codelst FROM ER_CODELST WHERE codelst_type = p_type AND codelst_subtyp = p_subtype;
  RETURN v_pk_codelst ;
END ;
/


