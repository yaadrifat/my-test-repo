CREATE OR REPLACE FUNCTION        "F_GETLOCAL_SAMPLESIZE" (p_study NUMBER)
RETURN VARCHAR2
AS
v_retval VARCHAR2(4000);
BEGIN
	 FOR i IN (SELECT (SELECT site_name FROM ER_SITE WHERE pk_site = fk_site) ||
DECODE(fk_codelst_studysitetype, NULL, '', ' (' || (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_studysitetype) || ') ') ||
DECODE(studysite_lsamplesize, NULL, '', ' - ' || studysite_lsamplesize) AS local_sample
FROM ER_STUDYSITES
WHERE fk_study = p_study AND
studysite_repinclude = 1 AND
studysite_lsamplesize IS NOT NULL
)
	 LOOP
	 	 v_retval := v_retval || '[' || i.local_sample || '] ' || ';';
	 END LOOP;
	 v_retval := substr(v_retval,1,instr(v_retval,';',-1)-1) ;
	 RETURN v_retval;
END ;
/


CREATE SYNONYM ESCH.F_GETLOCAL_SAMPLESIZE FOR F_GETLOCAL_SAMPLESIZE;


CREATE SYNONYM EPAT.F_GETLOCAL_SAMPLESIZE FOR F_GETLOCAL_SAMPLESIZE;


GRANT EXECUTE, DEBUG ON F_GETLOCAL_SAMPLESIZE TO EPAT;

GRANT EXECUTE, DEBUG ON F_GETLOCAL_SAMPLESIZE TO ESCH;

