create or replace
function populatesql(strsql varchar, strvalue varchar  ) Return Varchar As

	resultstr varchar2(1000);
        tmp_value varchar2(1000);
        tmp_repkey varchar2(10);
        tmp_repval varchar2(1000);
        tmp_count number;

Begin
    resultstr := strsql;
	tmp_value := strvalue || ',';
             tmp_count:=1;
             if(instr(tmp_value,',') > 0) then
                 while ( instr(tmp_value,',') > 0)
                 loop

                    tmp_repval := substr(tmp_value,0,instr(tmp_value,',')-1);
                    tmp_repkey := '#' || tmp_count || 'a';
                    resultstr := replace(resultstr,tmp_repkey,tmp_repval) ;
                    tmp_value := substr(tmp_value,instr(tmp_value,',')+1);
                    tmp_count := tmp_count + 1;
                 end loop;
              end if;
	Return resultstr;

End;
/
