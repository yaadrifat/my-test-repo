CREATE OR REPLACE FUNCTION        "F_CALC_DAYS" (p_unitVALUE CHAR, p_days NUMBER)
RETURN NUMBER
AS
v_days Number;
v_unit Char(2);

BEGIN

  v_days := nvl(p_days,0);
  v_unit := nvl(p_unitVALUE,'D');

  if v_days = 0 then
	return v_days;
  end if;

  CASE V_unit
    WHEN 'D' THEN RETURN v_days ;
    WHEN 'W' THEN RETURN v_days *7;
    WHEN 'M' THEN RETURN v_days *30 ;
    WHEN 'Y' THEN RETURN v_days *365;
    ELSE RETURN 0;
  END CASE;
END;
/


