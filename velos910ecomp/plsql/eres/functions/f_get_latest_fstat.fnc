CREATE OR REPLACE FUNCTION      F_GET_LATEST_FSTAT (p_study NUMBER, p_formlib NUMBER) RETURN NUMBER IS
tmpVar NUMBER;
/******************************************************************************
   NAME:       F_GET_LATEST_FSTAT
   PURPOSE:    Given a study PK and formlib PK, find the latest pk_studyforms value
               that is not marked as deleted in ER_SUTDYFORMS

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        9/3/2009          1. Created this function.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     F_GET_LATEST_FSTAT
      Sysdate:         9/3/2009
      Date and Time:   9/3/2009, 3:25:49 PM, and 9/3/2009 3:25:49 PM
      Username:        Isaac Huang
      Table Name:      ER_STUDYFORMS

******************************************************************************/
BEGIN
   tmpVar := 0;
   select pk_studyforms into tmpVar from (
   select stf.pk_studyforms from er_studyforms stf
   where stf.fk_study = p_study and
   stf.fk_formlib = p_formlib and stf.record_type <> 'D'
   order by pk_studyforms desc
   ) where rownum <= 1;
   RETURN tmpVar;
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
       return 0;
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;
END F_GET_LATEST_FSTAT;
/


