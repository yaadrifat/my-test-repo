CREATE OR REPLACE TRIGGER "EVENT_DEF_AI1" AFTER INSERT ON EVENT_DEF
FOR EACH ROW
WHEN (
new.event_type='P' or new.event_type='A'
      )
DECLARE
v_return number;

BEGIN
     if (:new.event_type='P') then
        PKG_BGT.sp_createProtocolBudget( :new.event_id,'L',:new.name, :new.creator , :new.ip_Add , v_return , 0,NULL );
     end if;

    if (:new.event_type='A') then
        PKG_BGT.sp_addToProtocolBudget( :new.event_id,'L',:new.chain_id, :new.creator , :new.ip_Add , v_return , 0 ,:new.fk_visit,
        :new.name,:new.event_cptcode,:new.event_line_category,:new.description,:new.event_sequence);
     end if;



END;
/


