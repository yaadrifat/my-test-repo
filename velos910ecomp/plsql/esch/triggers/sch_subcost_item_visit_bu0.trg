CREATE OR REPLACE TRIGGER "ESCH"."SCH_SUBCOST_ITEM_VISIT_BU0" 
BEFORE UPDATE
ON SCH_SUBCOST_ITEM_VISIT
FOR EACH ROW
when (NEW.last_modified_by IS NOT NULL) 
--Created by Manimaran for Audit Update
begin 
	:new.last_modified_date := sysdate;
end;
/