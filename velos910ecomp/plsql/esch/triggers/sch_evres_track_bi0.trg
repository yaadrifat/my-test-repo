CREATE OR REPLACE TRIGGER "SCH_EVRES_TRACK_BI0"
BEFORE INSERT
ON SCH_EVRES_TRACK REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  USR VARCHAR2(70);
  v_new_creator NUMBER;
  insert_data CLOB;
BEGIN
 BEGIN
  v_new_creator := :NEW.creator ;
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' ||usr_firstname
   INTO usr FROM er_user WHERE pk_user = v_new_creator;
  EXCEPTION WHEN NO_DATA_FOUND THEN
  USR := 'New User' ;
 END ;
  SELECT TRUNC(seq_rid.NEXTVAL) INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'SCH_EVRES_TRACK', erid, 'I', USR );
-- JM: on 02/14/08 for Audit insert
   insert_data:= :NEW.PK_EVRES_TRACK ||'|'||:new.FK_EVENTSTAT ||'|'||:new.FK_CODELST_ROLE||'|'||
	:NEW.EVRES_TRACK_DURATION||'|'|| :NEW.RID||'|'|| :NEW.CREATOR||'|'||
        :NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
        TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD;

     INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END;
/


