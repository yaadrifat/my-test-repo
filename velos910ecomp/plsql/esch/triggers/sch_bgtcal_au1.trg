CREATE OR REPLACE TRIGGER "SCH_BGTCAL_AU1" 
AFTER UPDATE OF BGTCAL_DELFLAG
ON SCH_BGTCAL
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
begin
  if nvl(:old.bgtcal_delflag,' ') !=  NVL(:new.bgtcal_delflag,' ') then
     if  NVL(:new.bgtcal_delflag,' ') = 'Y' then
        pkg_bgt.sp_update_bgtcal_children (:new.pk_bgtcal,
        :new.LAST_MODIFIED_BY, :new.IP_ADD);
     end if;
  end if;
end;
/


