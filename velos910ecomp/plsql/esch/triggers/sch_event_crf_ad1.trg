CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENT_CRF_AD1" 
AFTER DELETE
ON SCH_EVENT_CRF
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW

BEGIN

  --delete the forms/events combination from the sch_portal_forms table 

  DELETE from SCH_PORTAL_FORMS 
  WHERE 
  FK_EVENT = :OLD.FK_EVENT and FK_FORM = :OLD.FK_FORM;

END;
/

 