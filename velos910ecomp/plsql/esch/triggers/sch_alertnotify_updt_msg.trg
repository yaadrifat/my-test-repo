CREATE OR REPLACE TRIGGER "SCH_ALERTNOTIFY_UPDT_MSG" 
AFTER UPDATE OF ALNOT_FLAG
ON SCH_ALERTNOTIFY
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
Declare
v_patalcode NUMBER (10) ;
v_useralcode NUMBER (10);
v_type VARCHAR(2);
v_updateflag NUMBER (1);
Begin

if :new.fk_patprot > 0 or :new.fk_patprot <> null then

 if :new.ALNOT_TYPE = 'A' then
       Select PK_CODELST
       into v_patalcode
       from sch_codelst
       where CODELST_SUBTYP = 'n_patnot';
       Select PK_CODELST
       into v_useralcode
       from sch_codelst
       where CODELST_SUBTYP = 'n_usernot';
  if :new.FK_CODELST_AN =  v_patalcode then
      v_type := 'P';
      v_updateflag := 1;
  end if;
  if  :new.FK_CODELST_AN = v_useralcode then
      v_type := 'U';
      v_updateflag := 1;
  end if;

 if v_updateflag = 1  then

 PKG_GENSCH.SP_CHGMSGSTAT_TRAN(
 :new.fk_patprot,
 :new.alnot_flag,
 v_type ,
 :new.ip_add,
 :new.last_modified_by
 );
 end if;
 end if;
end if;
end ;
/


