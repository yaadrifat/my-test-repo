CREATE OR REPLACE FUNCTION        "LST_DOC" (event number ) return varchar is
cursor cur_doc
is
 select DOC_NAME
   from sch_docs , sch_eventdoc
  where sch_eventdoc.fk_event = event
    and sch_docs.PK_DOCS = sch_eventdoc.PK_DOCS  ;
doc_lst varchar2(4000) ;
begin
 for l_rec in cur_doc loop
  doc_lst :=   l_rec.doc_name || ', '|| doc_lst ;
 end loop ;
 doc_lst := substr(trim(doc_lst),1,length(trim(doc_lst))-1) ;
 dbms_output.put_line (substr(doc_lst,1,length(doc_lst)-1) ) ;
return doc_lst ;
end ;
/


CREATE SYNONYM ERES.LST_DOC FOR LST_DOC;


CREATE SYNONYM EPAT.LST_DOC FOR LST_DOC;


GRANT EXECUTE, DEBUG ON LST_DOC TO EPAT;

GRANT EXECUTE, DEBUG ON LST_DOC TO ERES;

