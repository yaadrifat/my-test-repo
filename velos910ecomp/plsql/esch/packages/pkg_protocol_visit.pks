CREATE OR REPLACE PACKAGE        "PKG_PROTOCOL_VISIT" IS
/******************************************************************************
	 NAME:       pkg_protocol_visit
	 PURPOSE:    To calculate the desired information.

	 REVISIONS:
	 Ver        Date        Author           Description
	 ---------  ----------  ---------------  ------------------------------------
	 1.0        10/23/2004             1. Created this package.

	 PARAMETERS:
	 INPUT:
	 OUTPUT:
	 RETURNED VALUE:
	 CALLED BY:
	 CALLS:
	 ASSUMPTIONS:
	 LIMITATIONS:
	 ALGORITHM:
	 NOTES:

	 Here is the complete list of available Auto Replace Keywords:
	 Object Name:     pkg_protocol_visit or pkg_protocol_visit
	 Date/Time:       10/23/2004 1:31:28 AM
******************************************************************************/


PROCEDURE sp_cascade_delete_visit_events(p_protocol_id IN NUMBER, p_protocol_visit IN NUMBER, p_source IN VARCHAR2) ;
PROCEDURE sp_update_visit_event(p_protocol_id IN NUMBER, p_protocol_visit IN NUMBER, p_source IN VARCHAR2) ;
PROCEDURE sp_delete_generated_visit(p_protocol_id IN NUMBER, p_source IN VARCHAR2) ;

FUNCTION validate_visit(p_protocol_id NUMBER, p_visit_id NUMBER,p_visit_name VARCHAR2 DEFAULT '',p_displacement NUMBER DEFAULT 0,p_days NUMBER,p_daySeven NUMBER) RETURN VARCHAR2;

FUNCTION validate_visit_name (p_protocol_id NUMBER, p_visit_id NUMBER, p_visit_name   VARCHAR2)   RETURN VARCHAR2;
FUNCTION validate_visit_displacement(p_protocol_id NUMBER, p_visit_id NUMBER,p_displacement   NUMBER)   RETURN VARCHAR2;
/* Added by Bikash kumar,Feb-17-2011 for Req D-FIN-25-DAY0 , Check whether month1 week1 day 7 or day0 applicaple to the visit or not */
FUNCTION validate_visit_dayzero(p_protocol_id NUMBER, p_visit_id NUMBER,p_displacement   NUMBER,p_days NUMBER,p_daySeven NUMBER) RETURN VARCHAR2;
PROCEDURE   sp_generate_ripple(p_protocol_visit IN NUMBER,p_called_from CHAR);
PROCEDURE sp_pushNoIntervalVisitsOut(p_protocol_visit IN NUMBER, p_ip IN VARCHAR2, p_lmBy IN NUMBER);

FUNCTION group_events(     p_protocol_id NUMBER,	p_displacment NUMBER	 )		RETURN VARCHAR2;

pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_PROTOCOL_VISIT', pLEVEL  => Plog.LDEBUG);

/* Added by Sonia Abrol, 10/19/06, updat ethe duration of the calendar when the maximum visit displacement is more than event duration. In this
case event duration unit will be set to days */

PROCEDURE sp_update_protocol_duration(p_protocol_visit  IN NUMBER, p_calledfrom IN VARCHAR2,o_new_duration OUT NUMBER);

-- Added by Manimaran for the Enh#C8.3
PROCEDURE sp_visit_copy(p_protocol_id IN NUMBER,p_frm_visitid IN NUMBER, p_to_visitid IN NUMBER,p_called_from IN VARCHAR2, usr IN varchar2, ipAdd IN varchar2,o_ret OUT NUMBER) ;

END Pkg_Protocol_Visit;
/


CREATE SYNONYM ERES.PKG_PROTOCOL_VISIT FOR PKG_PROTOCOL_VISIT;


CREATE SYNONYM EPAT.PKG_PROTOCOL_VISIT FOR PKG_PROTOCOL_VISIT;


GRANT EXECUTE, DEBUG ON PKG_PROTOCOL_VISIT TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_PROTOCOL_VISIT TO ERES;

