CREATE OR REPLACE PROCEDURE        "SP_DM_OBJECTSHAREWITH" 
AS

v_fk_account NUMBER;
v_pk_user NUMBER;
v_user NUMBER;
v_chain_id NUMBER;
v_ret NUMBER;
v_ip_add VARCHAR2(15);

CURSOR C1 IS
SELECT event_id, user_id
FROM EVENT_DEF
WHERE event_type = 'P'
ORDER BY event_id;


	 CURSOR C2 IS
	 SELECT pk_user, ip_add
	 FROM ER_USER
	 WHERE fk_account = v_fk_account;


BEGIN

	 v_ip_add := '';
	 v_user := NULL;

	 OPEN C1;
	 LOOP
	 	 FETCH C1 INTO v_chain_id, v_fk_account;
		 EXIT WHEN C1%NOTFOUND;
		 DBMS_OUTPUT.PUT_LINE('A=' || TO_CHAR(v_chain_id) || ',' || TO_CHAR(v_fk_account) || ',' || TO_CHAR(v_user));
--		 Sp_Objectsharewith( v_chain_id,'3', TO_CHAR(v_fk_account)  ,'A', TO_CHAR(v_user) , v_ip_add, v_ret, 'N' );
   		 INSERT INTO ER_OBJECTSHARE(PK_OBJECTSHARE,OBJECT_NUMBER,FK_OBJECT,FK_OBJECTSHARE_ID,OBJECTSHARE_TYPE,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD)
		 VALUES(SEQ_ER_OBJECTSHARE.NEXTVAL,3,v_chain_id, TO_CHAR(v_fk_account),'A',v_user,'N',SYSDATE,v_ip_add);

	 	 OPEN C2;

	 	 LOOP

	 	 FETCH C2 INTO v_user, v_ip_add;
	 	 EXIT WHEN C2%NOTFOUND;

		 DBMS_OUTPUT.PUT_LINE('U=' || TO_CHAR(v_chain_id) || ',' || TO_CHAR(v_user) || ',' || TO_CHAR(v_user));

--		 Sp_Objectsharewith( v_chain_id,'3', TO_CHAR(v_user)  ,'U', TO_CHAR(v_user) , v_IP_ADD , v_ret, 'N' );
	   		 INSERT INTO ER_OBJECTSHARE(PK_OBJECTSHARE,OBJECT_NUMBER,FK_OBJECT,FK_OBJECTSHARE_ID,OBJECTSHARE_TYPE,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD)
		 VALUES(SEQ_ER_OBJECTSHARE.NEXTVAL,3,v_chain_id, TO_CHAR(v_user),'U',v_user,'N',SYSDATE,v_ip_add);


		 END LOOP;
		 CLOSE C2;


	END LOOP;
	CLOSE C1;

END ;
/


CREATE SYNONYM ERES.SP_DM_OBJECTSHAREWITH FOR SP_DM_OBJECTSHAREWITH;


CREATE SYNONYM EPAT.SP_DM_OBJECTSHAREWITH FOR SP_DM_OBJECTSHAREWITH;


GRANT EXECUTE, DEBUG ON SP_DM_OBJECTSHAREWITH TO EPAT;

GRANT EXECUTE, DEBUG ON SP_DM_OBJECTSHAREWITH TO ERES;

