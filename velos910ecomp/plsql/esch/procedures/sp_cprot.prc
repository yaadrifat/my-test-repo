create or replace
PROCEDURE        "SP_CPROT" (
p_protocol IN NUMBER,
p_name IN VARCHAR,
p_newProtocol OUT NUMBER
)
AS

 p_chain_id         NUMBER;
 p_currval         NUMBER;
 p_count         NUMBER;
 p_user_id         NUMBER;
 
 v_codelst_pk number;
 
-- JM:		09FEB2011,  #D-FIN9: added FK_CODELST_CALSTAT and removed STATUS  


CURSOR C1 is
 select EVENT_ID,
      CHAIN_ID,
   EVENT_TYPE,
   NAME,
   NOTES,
   COST,
   COST_DESCRIPTION,
   DURATION,
   USER_ID,
   LINKED_URI,
   FUZZY_PERIOD,
   MSG_TO,
   fk_codelst_calstat,
   DESCRIPTION,
   DISPLACEMENT,
   ORG_ID
        FROM  event_def
 WHERE chain_id = p_protocol
     OrDer BY EVENT_TYPE desc ;

BEGIN
 select USER_ID
   into p_user_id
   from event_def
  where event_id = p_protocol;


 select count(event_id)
   into p_count
   from event_def
  where user_id = p_user_id
    and name    = p_name ;


 IF p_count > 0 THEN
    p_newProtocol:=-1;
    RETURN ;
 END IF;

 select EVENT_DEFINITION_SEQ.nextval
   into p_currval from dual;

 p_newProtocol:=p_currval  ;
 
 
 select pk_codelst into v_codelst_pk from sch_codelst where codelst_type='calStatLib' and CODELST_SUBTYP='W';

 insert into event_def
 (
  EVENT_ID               ,
  CHAIN_ID               ,
  EVENT_TYPE             ,
  NAME                   ,
  NOTES                  ,
  COST                   ,
  COST_DESCRIPTION       ,
  DURATION               ,
  USER_ID                ,
  LINKED_URI             ,
  FUZZY_PERIOD           ,
  MSG_TO                 ,
  fk_codelst_calstat     ,
  DESCRIPTION            ,
  DISPLACEMENT           ,
  ORG_ID                 ,
  EVENT_MSG              ,
  EVENT_RES              )
 select p_currval ,
        p_currval ,
        EVENT_TYPE,
 p_name,
 NOTES,
 COST,
 COST_DESCRIPTION,
 DURATION,
 USER_ID,
 LINKED_URI,
 FUZZY_PERIOD,
 MSG_TO,
 v_codelst_pk,
 DESCRIPTION,
 DISPLACEMENT,
 ORG_ID ,
 EVENT_MSG      ,
 EVENT_RES
        FROM  event_def
 WHERE chain_id = p_protocol AND
        event_type = 'P'  ;


 insert into event_def
 (
  EVENT_ID               ,
  CHAIN_ID               ,
  EVENT_TYPE             ,
  NAME                   ,
  NOTES                  ,
  COST                   ,
  COST_DESCRIPTION       ,
  DURATION               ,
  USER_ID                ,
  LINKED_URI             ,
  FUZZY_PERIOD           ,
  MSG_TO                 ,
  fk_codelst_calstat     ,
  DESCRIPTION            ,
  DISPLACEMENT           ,
  ORG_ID                 ,
  EVENT_MSG              ,
  EVENT_RES              )
 select EVENT_DEFINITION_SEQ.nextval ,
        p_currval ,
        EVENT_TYPE,
 p_NAME,
 NOTES,
 COST,
 COST_DESCRIPTION,
 DURATION,
 USER_ID,
 LINKED_URI,
 FUZZY_PERIOD,
 MSG_TO,
 fk_codelst_calstat, 
 DESCRIPTION,
 DISPLACEMENT,
 ORG_ID ,
 EVENT_MSG      ,
 EVENT_RES
        FROM  event_def
 WHERE chain_id = p_protocol AND
              event_type = 'A'  ;

/**********************************
TEST
set serveroutput on
declare
myevent_id number ;

begin
sp_copyprotocol(657,'dinesh',myevent_id) ;
dbms_output.put_line(to_char(myevent_id) ) ;
end ;
**************************************/
COMMIT;
END;
/


CREATE SYNONYM ERES.SP_CPROT FOR SP_CPROT;


CREATE SYNONYM EPAT.SP_CPROT FOR SP_CPROT;


GRANT EXECUTE, DEBUG ON SP_CPROT TO EPAT;

GRANT EXECUTE, DEBUG ON SP_CPROT TO ERES;

