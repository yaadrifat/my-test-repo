This is where JUnit test cases are stored. Use the same directory structure as in src\.

For example, to add a test case for a class in com.velos.eres.servce.util, place it in

test\com\velos\eres\service\util\

