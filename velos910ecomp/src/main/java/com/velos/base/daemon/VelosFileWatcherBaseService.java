package com.velos.base.daemon;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Observer;

import org.apache.log4j.Logger;

/**
 * This Service hold a pool of instance of com.velos.base.daemon.FileWatcher
 * Class. Each Thread are identified by a name and accessible through this name.
 * Threads are added in an Hashtable registry.
 * 
 * @author Vishal
 * @version 1.0
 */
public class VelosFileWatcherBaseService {
    private static Logger logger = Logger
            .getLogger(VelosFileWatcherBaseService.class.getName());

    /** The singelton instance of this class * */
    private static VelosFileWatcherBaseService m_Instance = null;

    /**
     * The Pool of Threads *
     * 
     * associates FileWatcher
     */
    private Hashtable m_Registry;

    /**
     * Protected Constructor
     */
    protected VelosFileWatcherBaseService() {

        m_Registry = new Hashtable();

    }

    /**
     * Use this method to get an instance of this class
     */
    public static synchronized VelosFileWatcherBaseService getInstance() {

        if (m_Instance == null) {
            m_Instance = new VelosFileWatcherBaseService();
        }
        return m_Instance;
    }

    /**
     * addFileWatcher
     * 
     * @param (String)
     *            threadName, the Name to identify this thread
     * @param (String)
     *            fullFolderPath, the real path to the folder to watch
     * @param (boolean)
     *            checkDate, check new file by last modif date or not
     * @param (long)
     *            interval, the interval in millis
     */
    public synchronized void addFileWatcher(String threadName,
            String fullFolderPath, boolean checkDate, long interval,
            boolean fileOnly) throws IOException {
        try {

            FileWatcher fw = new FileWatcher(fullFolderPath, checkDate,
                    interval, fileOnly, true);

            m_Registry.put(threadName, fw);

        } catch (IOException e) {

            logger.fatal("Error in addFileWatcher: " + e.getMessage());

        }

    }

    /**
     * Call the start method of the thread
     * 
     * @param (String)
     *            threadName, the Name to identify this thread
     */
    public void startFileWatcher(String threadName) throws IOException {

        try {

            synchronized (this) {

                getFileWatcher(threadName).start();
            }

        } catch (IOException e) {
            logger.error("Error in startFileWatcher:: " + e.getMessage());

        }
    }

    /**
     * Register an Observer Thread with an Observable Thread
     * 
     * @param threadName
     *            the Name of Observable object
     * @param obs
     *            the observer object
     */
    public void registerObserver(String threadName, Observer obs) {
        synchronized (this) {
            getFileWatcher(threadName).addObserver(obs);
        }
    }

    /**
     * getFileWatcher
     * 
     * @param (String)
     *            threadName, the Name to identify this thread
     * 
     * @return (FileWatcher) return the FileWatcher Thread or null if not in
     *         registry
     */
    protected synchronized FileWatcher getFileWatcher(String threadName) {

        return (FileWatcher) m_Registry.get(threadName);

    }

}