package com.velos.base;

import java.util.ArrayList;

/**
 * This class implements parser through VelosIO. All the delimited data objects
 * are parsed with this class.
 */
public class DlmParser extends VelosIO {
    public DlmParser() {

    }

    public void setProperties(String fileName, String delimiter) {
        super.setProperties(fileName, delimiter);
    }

    public void setProperties(String fileName, String delimiter, int operation) {
        super.setProperties(fileName, operation);
    }

    public String readLine() {
        return super.readLine();
    }

    public void writeLine(String line) {
        super.writeLine(line);
    }

    public void close() {

        super.close();
    }



    public ArrayList getTokens() {
        return super.getTokens();
    }
 
}