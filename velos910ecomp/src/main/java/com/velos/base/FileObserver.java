package com.velos.base;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.jconfig.Configuration;

import com.velos.base.daemon.VelosFileWatcherBaseService;
import com.velos.base.dbutil.DBEngine;

/**
 * An Observable object FileObserver object implements Observer Interface.
 * 
 * @author Vishal Abrol
 * @version 1.0
 */
public class FileObserver extends InterfaceService implements Observer {
    private static Logger logger = Logger.getLogger(FileObserver.class
            .getName());
    private static Logger log_cat = Logger.getLogger("log_cat");
    DataHolder dataHolder=DataHolder.getDataHolder();
    // private static org.apache.log4j.Logger logger =
    // org.apache.log4j.Logger.getLogger (TemplatesObserver.class);

    /** used to attach an observer with a site(Future use) * */
    private int mSiteID = -1;

    /** the path of the folder to watch * */
    private String mPath;

    /** check new file by last modif date or not * */
    private boolean mCheckDate = false;

    /** the interval * */
    private long mInterval;

    /** Check bot files and directory * */
    private boolean mFileOnly = true;

    /** Check if the file moved to location exist * */

    private boolean keepRunning = false;

    /** Initialize Ibnterface configuration to route the files.* */
    private InterfaceConfig config = new InterfaceConfig();

    /**
     * Constructor
     * 
     * @param int
     *            siteID, used to attach a observer with a site
     * @param String
     *            path, the path of directory to watch.
     * @param boolean
     *            checkDate, check by last modif date or not
     * @param long
     *            interval, the interval of check
     * @param boolean
     *            fileOnly, check both new files and directory
     */
    public FileObserver(int siteID, String path, boolean checkDate,
            long interval, boolean fileOnly) {

        mSiteID = siteID;
        mPath = path;
        mCheckDate = checkDate;
        mInterval = interval;
        mFileOnly = fileOnly;
        // Initialize FileWatcher Object Pool
        VelosFileWatcherBaseService fws = VelosFileWatcherBaseService
                .getInstance();

        if (fws != null) {

            try {
                fws.addFileWatcher(mPath, mPath, mCheckDate, mInterval,
                        mFileOnly);

                fws.registerObserver(mPath, this);
                fws.startFileWatcher(mPath);
                config.LoadProperties();
            } catch (Throwable t) {

                logger.fatal("Error in File observer initialization"
                        + t.getMessage());
                t.printStackTrace();
            }
        }
    }

    /**
     * Constructor *
     * 
     * @param String
     *            path, the path of directory to watch.
     * @param boolean
     *            checkDate, check by last modif date or not
     * @param long
     *            interval, the interval of check
     * @param boolean
     *            fileOnly, check both new files and directory
     */
    public FileObserver(String path, boolean checkDate, long interval,
            boolean fileOnly) {

        mPath = path;
        mCheckDate = checkDate;
        mInterval = interval;
        mFileOnly = fileOnly;

        VelosFileWatcherBaseService fws = VelosFileWatcherBaseService
                .getInstance();

        if (fws != null) {

            try {
                fws.addFileWatcher(mPath, mPath, mCheckDate, mInterval,
                        mFileOnly);

                fws.registerObserver(mPath, this);
                fws.startFileWatcher(mPath);
                config.LoadProperties();
            } catch (Throwable t) {

                logger.fatal("Error in File observer initialization"
                        + t.getMessage());
                t.printStackTrace();

            }
        }
    }

    /**
     * This method handles new file detection and route the file to proper
     * destination
     * 
     */
    public void update(Observable subject, Object args) {

        synchronized (args) {
            VelosIO velIO = new VelosIO();
            FileProcessor fprocess = null;
            Vector files = (Vector) args;
            File tempFile=null;
                       
            
            
            //Sort the Vector by last modified Date. 
            //That will make sure files are processed in sequence
            Collections.sort(files, new Comparator()
                    {
                            //must return an int
                            public int compare( Object a, Object b) //This is the only thing I had to change!
                            {

                                    File f1 = (File)a ;
                                    File f2 = (File)b ;
                                    // before casting you should check to see if they are valid file objects
                                    // But in your case you only uses this inner class for one little sort
                                    // Also you could have created an inner class that implementes Comparable
                                    //and did it that way.
                                    return (int) (f1.lastModified() - f2.lastModified());
                            }
                    }
                        );
            //End sorting
         
            File processFile = null;
            Configuration mapping = null, interfaceConfig = null;
            String event = "", encode = "";
            String fileName = "", filePath = "";
            String processedPath = "", unprocessedPath = "";
            String[] eventArray = null;
            boolean success = false;
            try {
                
//              Check if any of the files are locked
                //If they are, wait for it to finish
                for (Enumeration e = files.elements(); e.hasMoreElements();)  {
                    tempFile=(File)e.nextElement();
                    FileChannel channel=null;
                    FileLock lock=null;
                    try{
                       channel=
                        new RandomAccessFile(tempFile, "rw").getChannel();
                      
                    }catch(Exception ex)
                    {
                        logger.debug(ex.getMessage());
                        ex.printStackTrace();
                    }
                    lock = channel.tryLock(); 
                        if(lock != null)
                        {
                            
                         lock.release();
                         channel.close();
                        
                        }
                        else
                        {
                            logger.debug("Waiting for File " + tempFile +" to finish.")   ;
                            channel.close();
                            return ;
                        }
                       
                        {
                            
                        }
                   }
                
                for (Enumeration e = files.elements(); e.hasMoreElements();) {
                    processFile = (File) e.nextElement();
                    fileName = processFile.getName();
                    filePath = processFile.getParent();
                    logger.info("Processing File -- " + filePath + "\\"
                            + fileName);
                    dataHolder.setPROCESS_FILE(fileName);
                    dataHolder.setLASTID(0); // parent record for new file, no last ID
                    
                    log_cat.info("Processing File -- " + filePath + "\\"
                            + fileName);
                    if (!(config.IsConfigLoaded())) {
                        config.LoadProperties();
                    }
                    mapping = config.getMappingConfig();
                    interfaceConfig = config.getInterfaceConfig();
                    if (mapping == null) {
                        logger
                                .info("Cannot retrieve File Mapping, Please check the Path.Moving file to unprocessed.");
                        success = velIO.MoveFile(processFile, (unprocessedPath
                                .length() == 0) ? processFile.getParent()
                                + "\\unprocessed" : unprocessedPath);
                        if (success == false)
                            logger
                                    .info("Error Moving file to unprocessed folder."
                                            + "Please make sure corret path is specified in Configuration file.");
                        else
                            logger
                                    .info("File moved to unprocessed folder successfully.");
                        throw new ApplicationException(
                                "Error reading mapping information for file:"
                                        + fileName);

                    }
                    if (interfaceConfig == null) {
                        logger
                                .info("Cannot retrieve Interface Mapping, Please check the Path.Moving file to unprocessed.");
                        success = velIO.MoveFile(processFile, (unprocessedPath
                                .length() == 0) ? processFile.getParent()
                                + "\\unprocessed" : unprocessedPath);
                        if (success == false)
                            logger
                                    .info("Error Moving file to unprocessed folder."
                                            + "Please make sure corret path is specified in Configuration file.");
                        else
                            logger
                                    .info("File moved to unprocessed folder successfully.");
                        throw new ApplicationException(
                                "Error reading Interface Configuration");

                    }
                    //Preprocess the mapping to extract all the file names
                    String mappingCatArray[]=null;
                    String fileNameMap="";
                     if (mapping!=null) {
                          mappingCatArray=mapping.getCategoryNames();
                          dataHolder.getPatternMatcher().reset();
                    for (int i=0;i<mappingCatArray.length;i++)
                    {
                        if (mappingCatArray[i]!=null)
                            dataHolder.getPatternMatcher().addWildCardPattern(mappingCatArray[i]);
                    }
                    }
                    int index=dataHolder.getPatternMatcher().matches(fileName);
                    if (index>=0) {
                        fileNameMap=mappingCatArray[index];
                        eventArray = mapping.getArray("event", new String[] {},
                                fileNameMap);
                        encode = mapping. getProperty("encoding", "", fileName);
                    }else {
                        
                    eventArray = mapping.getArray("event", new String[] {},
                            fileName);
                    encode = mapping. getProperty("encoding", "", fileName);
                    }
                    
                    fprocess = new FileProcessor();
                    //FatalStat will capture status of processFile
                    //-3 return should stop the server immediately
                    int fatalStat=0;
                    for (int i = 0; i < eventArray.length; i++) {
                        // event=mapping.getProperty("event","",fileName);
                        event = (eventArray[i]).trim();
                        
                        if ((event.trim()).length() > 0) {
                        	this.dataHolder.setEventName(event);
                            logger
                                    .debug("*********************************************");
                            logger.debug("    Processing event : " + event);
                            logger
                                    .debug("*********************************************");
                            config.LoadEvent(event);
                            processedPath = config.getProcessedPath();
                            fprocess.Initialize(encode, event, config,
                                    filePath, fileName);
                            fatalStat=fprocess.processFile();
                            if (fatalStat==-3) {
                            dataHolder.setMSG_LEVEL("BAD");
                            logger.info("VEL-CRT-00 - Fatal Error Initializing the Application.System will exit now. Please fix the errors to continue.");
                            dataHolder.setMSG_LEVEL("");
                            dataHolder.setLASTID(dataHolder.getCURRENTID()); // parent record for current file, s
                            log_cat.info("Done");
                            dataHolder.reset();
                             System.exit(1);
                            }
                            if (fatalStat==-5)
                            {
                                dataHolder.setMSG_LEVEL("REJECT");
                                dataHolder.setRejectFlag(1);
                                logger
                                        .info("File did not pass Pre-Validation check. Routing to unprocessed folder");
                                unprocessedPath = config.getUnprocessedPath();
                                success = velIO.MoveFile(processFile, (unprocessedPath
                                        .length() == 0) ? processFile.getParent()
                                        + "\\unprocessed" : unprocessedPath);
                                dataHolder.setMSG_LEVEL("");
                            }
                        }
                    }
                    if (((event.trim()).length() > 0) && (fatalStat==1)) {
                        success = velIO.MoveFile(processFile, (processedPath
                                .length() == 0) ? filePath + "\\processed"
                                : processedPath);
                        if (success == false)
                            logger
                                    .error("Error moving file to processed folder.");
                        else
                            logger
                                    .debug("File processed and moved to processed folder successfully.");
                    } else if (fatalStat==0) {
                        dataHolder.setMSG_LEVEL("REJECT");
                        dataHolder.setRejectFlag(1);
                        logger
                                .info("This File type is not supported. Please check with your administrator.\n"
                                        + "File is being moved to unprocessed files folder.");
                        unprocessedPath = config.getUnprocessedPath();
                        success = velIO.MoveFile(processFile, (unprocessedPath
                                .length() == 0) ? processFile.getParent()
                                + "\\unprocessed" : unprocessedPath);
                        dataHolder.setMSG_LEVEL("");
                        if (success == false)
                            logger
                                    .error("Error Moving file to unprocessed folder."
                                            + "Please make sure corret path is specified in Configuration file.");
                        else
                            logger
                                    .info("File moved to unprocessed folder successfully.");
                    }
                    dataHolder.setLASTID(dataHolder.getCURRENTID()); // parent record for current file, s
                    log_cat.info("File Processed Successfully");
                    dataHolder.reset();
                }

            }catch (IOException io) {
                logger.fatal("Exception Occured" + io.getMessage());
                new ApplicationException(
                        "Locked File. Waiting for File to Finish "
                                + io.getMessage());
                io.printStackTrace();
                

            }
            catch (Exception t) {
                logger.fatal("Exception Occured" + t.getMessage());
                t.printStackTrace();
                new ApplicationException(
                        "Fatal error in File observer initialization"
                                + t.getMessage());
                 //t.printStackTrace();
                 

            }
            
        }
    }

    public void run() {
        long rest = 3000;
        while (keepRunning()) {
            // System.out.println("waiting");
            sleep();
        }
    }

    // Static method to run the thread
    public static void main(String[] args) {
    	String path="";
        String dbLogger="";
    	if (!(args==null)){
    	   if (args.length>0)
    	   {
    		   path=args[0];
    		path=(path==null)?"":path;
            
            //dbLogger=args[1];
            //dbLogger=(dbLogger==null)?"":dbLogger;
            
    	if (path.length()==0){
    		System.out.println("No Directory specified,Exiting...");
    		System.exit(1);
    	}
    	} else
    	{
    		System.out.println("No Directory specified,Exiting...");
        	System.exit(1);
    	}
    	}
    	else 
    	{
    		System.out.println("No Directory specified,Exiting...");
        	System.exit(1);	
    	}
        //logger.info("Initializing Logging Thread...");
        //org.apache.log4j.PropertyConfigurator.configure(System.getProperty("INTERFACE_HOME")+ "conf\\log4j.xml");
        DOMConfigurator.configure(System.getProperty("INTERFACE_HOME")+ "conf\\log4j_interface.xml");
                DBEngine dbengine = DBEngine.getEngine();
        dbengine.initializePool(System.getProperty("INTERFACE_HOME")
                + "conf\\interface.properties", "interface");
        /*if ((dbLogger.toLowerCase()).equals("enabledblog"))
        dbengine.initializePool(System.getProperty("INTERFACE_HOME")
                + "conf\\interface.properties", "logger");*/
        if (dbengine.IsInitialized())
            logger.info("Database Adapter started Sucessfully");
        else {
            logger
                    .info("Error in Database Adapter Intialization, Interface Exiting.");
            System.exit(1);
        }

        logger.info("Server Starting... ,Initializing embedded Threads");
        FileObserver fwatch = new FileObserver(path,false,10, true);
        fwatch.start();
        logger.info("Server Started Successfully, waiting for files...");
        log_cat.info("Server started and listening");
    }

} // end FilebserverService
