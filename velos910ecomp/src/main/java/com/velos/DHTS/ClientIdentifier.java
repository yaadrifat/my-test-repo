/**
 * ClientIdentifier.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.velos.DHTS;

public class ClientIdentifier  implements java.io.Serializable {
    private java.lang.String userID;

    private com.velos.DHTS.PersonName userName;

    private java.lang.String workstationID;

    public ClientIdentifier() {
    }

    public ClientIdentifier(
           java.lang.String userID,
           com.velos.DHTS.PersonName userName,
           java.lang.String workstationID) {
           this.userID = userID;
           this.userName = userName;
           this.workstationID = workstationID;
    }


    /**
     * Gets the userID value for this ClientIdentifier.
     * 
     * @return userID
     */
    public java.lang.String getUserID() {
        return userID;
    }


    /**
     * Sets the userID value for this ClientIdentifier.
     * 
     * @param userID
     */
    public void setUserID(java.lang.String userID) {
        this.userID = userID;
    }


    /**
     * Gets the userName value for this ClientIdentifier.
     * 
     * @return userName
     */
    public com.velos.DHTS.PersonName getUserName() {
        return userName;
    }


    /**
     * Sets the userName value for this ClientIdentifier.
     * 
     * @param userName
     */
    public void setUserName(com.velos.DHTS.PersonName userName) {
        this.userName = userName;
    }


    /**
     * Gets the workstationID value for this ClientIdentifier.
     * 
     * @return workstationID
     */
    public java.lang.String getWorkstationID() {
        return workstationID;
    }


    /**
     * Sets the workstationID value for this ClientIdentifier.
     * 
     * @param workstationID
     */
    public void setWorkstationID(java.lang.String workstationID) {
        this.workstationID = workstationID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientIdentifier)) return false;
        ClientIdentifier other = (ClientIdentifier) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.userID==null && other.getUserID()==null) || 
             (this.userID!=null &&
              this.userID.equals(other.getUserID()))) &&
            ((this.userName==null && other.getUserName()==null) || 
             (this.userName!=null &&
              this.userName.equals(other.getUserName()))) &&
            ((this.workstationID==null && other.getWorkstationID()==null) || 
             (this.workstationID!=null &&
              this.workstationID.equals(other.getWorkstationID())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUserID() != null) {
            _hashCode += getUserID().hashCode();
        }
        if (getUserName() != null) {
            _hashCode += getUserName().hashCode();
        }
        if (getWorkstationID() != null) {
            _hashCode += getWorkstationID().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientIdentifier.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "ClientIdentifier"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "UserID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "UserName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "PersonName"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workstationID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "WorkstationID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
