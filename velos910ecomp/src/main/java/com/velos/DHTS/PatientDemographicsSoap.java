/**
 * PatientDemographicsSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.velos.DHTS;

public interface PatientDemographicsSoap extends java.rmi.Remote {

    /**
     * Retrieve Patient Demographics by HCFacility and MRN
     */
    public com.velos.DHTS.GetPatientInfoResponseGetPatientInfoResult getPatientInfo(java.lang.String userID, java.lang.String pwd, java.lang.String HCFacility, java.lang.String MRN) throws java.rmi.RemoteException;

    /**
     * Retrieve Patient Demographics by HCFacility and MRN
     */
    public com.velos.DHTS.DemographicsResponse getPatientDemographics(com.velos.DHTS.DemographicsRequest rInput) throws java.rmi.RemoteException;

    /**
     * GetPatientInfoEx allows for more than one mrn at a time and
     * can return the current encounter
     */
    public com.velos.DHTS.GetPatientInfoExResponseGetPatientInfoExResult getPatientInfoEx(java.lang.String userID, java.lang.String password, java.lang.String HCFacilityList, java.lang.String MRNList, int howmanyEncounters) throws java.rmi.RemoteException;
}
