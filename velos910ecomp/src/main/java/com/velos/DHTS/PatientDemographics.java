/**
 * PatientDemographics.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.velos.DHTS;

public interface PatientDemographics extends javax.xml.rpc.Service {
    public java.lang.String getPatientDemographicsSoap12Address();

    public com.velos.DHTS.PatientDemographicsSoap getPatientDemographicsSoap12() throws javax.xml.rpc.ServiceException;

    public com.velos.DHTS.PatientDemographicsSoap getPatientDemographicsSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getPatientDemographicsSoapAddress();

    public com.velos.DHTS.PatientDemographicsSoap getPatientDemographicsSoap() throws javax.xml.rpc.ServiceException;

    public com.velos.DHTS.PatientDemographicsSoap getPatientDemographicsSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
