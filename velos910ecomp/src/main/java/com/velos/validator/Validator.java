package com.velos.validator;


import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.user.UserJB;

public class Validator
{
  String validationMessage;
  String validationFlag;
  String validateFld;
  String dataSet;
  String targetValue="";
  String[] dataArray;

public Validator(String validateField,String data,String targetVal)
 {
  validationMessage="";
  validationFlag="";
  validateFld=validateField;
  dataSet=data;
  targetValue=targetVal;
  dataArray=StringUtil.chopChop(dataSet,':');
 }
public Validator()
{
	
}

/**
 * @return the validationFlag
 */
public String getValidationFlag() {
	return validationFlag;
}

/**
 * @param validationFlag the validationFlag to set
 */
public void setValidationFlag(String validationFlag) {
	this.validationFlag = validationFlag;
}

/**
 * @return the validationMessage
 */
public String getValidationMessage() {
	return validationMessage;
}

/**
 * @param validationMessage the validationMessage to set
 */
public void setValidationMessage(String validationMessage) {
	this.validationMessage = validationMessage;
}


 
}