package com.velos.eres.service.util;

/**
 * @deprecated
 * Use MC.java instead.
 * 
 * Class to hold all the key strings in messageBundle.properties file.
 * Make all key strings public static final.
 * Start each key with a category prefix, and arrange them alphabetically.
 */
public class MsgConst {
    public static final String Pat_RestrictFutureDate = VelosResourceBundle.getMessageString("Pat_RestrictFutureDate");
    public static final String Usr_SessTimeout = VelosResourceBundle.getMessageString("Usr_SessTimeout");
    public static final String Usr_SessTimeoutShort = VelosResourceBundle.getMessageString("Usr_SessTimeoutShort");
}
