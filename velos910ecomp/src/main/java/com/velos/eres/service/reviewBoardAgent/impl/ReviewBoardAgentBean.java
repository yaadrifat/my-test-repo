package com.velos.eres.service.reviewBoardAgent.impl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.ReviewBoardDao;
import com.velos.eres.business.reviewboard.impl.ReviewBoardBean;
import com.velos.eres.service.reviewBoardAgent.ReviewBoardAgentRObj;
import com.velos.eres.service.util.Rlog;

/**
* The stateless session EJB acting as a facade for the entity BMP.<br>
* <br>
*
* @author 
* @version 1.0 01/20/2013
* @ejbHome ReviewBoardAgentHome
* @ejbRemote ReviewBoardAgentRObj
*/

@Stateless
public  class ReviewBoardAgentBean implements ReviewBoardAgentRObj {
	@PersistenceContext(unitName = "eres")
    protected EntityManager em;
	
 
	  public ReviewBoardBean getReviewBoardDetails(int reviewBoardId,int fkBoardId) {
		  if (reviewBoardId == 0) {
	            return null;
	        }
		  ReviewBoardBean reviewBoardBean = null;
	        try {
	        	reviewBoardBean = (ReviewBoardBean) em.find(ReviewBoardBean.class, new Integer(reviewBoardId));        	
	            ReviewBoardDao rbDao=new ReviewBoardDao();
	            rbDao.getReviewBoardValues(reviewBoardId);        
	            
	        } catch (Exception e) {
	        	Rlog.fatal("reviewBoard", "Error in getReviewBoardDetails() in ReviewBoardAgentBean"+ e);
	        }
	        return reviewBoardBean;
	    }
	  
	  
	  public ReviewBoardBean getReviewBoardNames(int reviewBoardId) {
		  if (reviewBoardId == 0)
		  {
	         return null;
	      }
		  ReviewBoardBean reviewBoardBean = null;
        
		  try 
          {
        	reviewBoardBean = (ReviewBoardBean) em.find(ReviewBoardBean.class, new Integer(reviewBoardId));        	
            ReviewBoardDao rbDao=new ReviewBoardDao();
            rbDao.getReviewBoardNames(reviewBoardId); 	            
          } 
          catch (Exception e) 
          {
        	Rlog.fatal("reviewBoard", "Error in getReviewBoardDetails() in ReviewBoardAgentBean"+ e);
          }
          return reviewBoardBean;
	    }
	  
	      
	  public int setReviewBoardDetails(ReviewBoardBean reviewboard) {
	       try 
	       {
	           ReviewBoardBean rwb = new ReviewBoardBean();
	           rwb.updateReviewBoard(reviewboard);
	           em.persist(rwb);
	           return rwb.getReviewBoardId();
	       } 
	       catch (Exception e)
	       {
	           Rlog.fatal("reviewboard", "Error in setReviewBoardDetails() in ReviewBoardAgentBean "+ e);
	           return -1;
	       }
	   }
			  	  
	  public ReviewBoardBean getReviewBoardAllDetails() {		  
		  ReviewBoardBean reviewBoardBean = null;
	        try 
	        {
	        	//reviewBoardBean =(ReviewBoardBean) em.find(ReviewBoardBean.class, 0);       	 
	        } 
	        catch (Exception e) 
	        {
	        	Rlog.fatal("reviewBoard", "Error in getReviewBoardDetails() in ReviewBoardAgentBean"+ e);
	        }
	        return reviewBoardBean;
	    }  
	  	  
	  
	  public int getRBNameCount(String rbName) 
	  {
	        int count = 0;
	        try 
	        {
	            Rlog.debug("reviewBoard","In getRBNameCount in ReviewBoardAgentBean line number 0");
	            ReviewBoardDao rbDao=new ReviewBoardDao();
	            Rlog.debug("reviewBoard","In getRBNameCount in ReviewBoardAgentBean line number 1");
	            count = rbDao.getRBNameCount(rbName);
	            Rlog.debug("reviewBoard","In getRBNameCount in ReviewBoardAgentBean line number 2");
	        } 
	        catch (Exception e)
	        {
	            Rlog.fatal("reviewBoard", "Exception In getRBNameCount in ReviewBoardAgentBean "+ e);
	        }
	        return count;

	    }


	@Override
	public void getReviewBoardAllValues() {
		// TODO Auto-generated method stub		
	}

	@Override
	public void getReviewBoardValues() {
		// TODO Auto-generated method stub		
	}

	@Override
	public int updateReviewBoard(ReviewBoardBean rsk) {
		// TODO Auto-generated method stub
		return 0;
	}	
	
	@Override
	public ReviewBoardBean getReviewBoardDetails(int reviewBoardId) {
		// TODO Auto-generated method stub
		return null;
	}  
}