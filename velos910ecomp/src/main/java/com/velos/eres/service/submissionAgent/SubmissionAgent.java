package com.velos.eres.service.submissionAgent;

import javax.ejb.Remote;

import com.velos.eres.business.submission.impl.SubmissionBean;

@Remote
public interface SubmissionAgent {
    public SubmissionBean getSubmissionDetails(int id);
    public Integer updateSubmissionByFkStudy(SubmissionBean submissionBean);
    public Integer createSubmission(SubmissionBean submissionBean);
    public Integer getExistingSubmissionByFkStudy(Integer fkStudy);
    public Integer getExistingSubmissionByFkStudyAndFlag(Integer fkStudy, Integer submissionFlag);
    public boolean getSubmissionAccess(String usrId, String submissionPK);
}
