package com.velos.eres.service.memberAgent;

import javax.ejb.Remote;
import com.velos.eres.business.common.MemberDao;
import com.velos.eres.business.memberBoard.impl.MemberBean;

@Remote
public interface MemberAgentRObj {

	public MemberBean getMemberDetails(int fkBoardId);   	
	
	public MemberBean getMemberDetailsById(int pkRBMember); 
	
    public int setMemberDetails(MemberBean memberBean);
    
    public int updateMember(MemberBean memberBean);
    
    public MemberDao getMemberValues(int studyId);
    
    public MemberDao getMemberRights(int studyId, int userId); 
    
    public int deleteFromBoard(MemberBean msk);
}
