/*
 * Classname : FormNotifyAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 24/06/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: SoniaKaura
 */

package com.velos.eres.service.formNotifyAgent.impl;

/* Import Statements */
import java.util.Hashtable;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.FormNotifyDao;
import com.velos.eres.business.common.UserDao;
import com.velos.eres.business.formLib.impl.FormLibBean;
import com.velos.eres.business.formNotify.impl.FormNotifyBean;
import com.velos.eres.service.formNotifyAgent.FormNotifyAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

/**
 * The stateless session EJB acting as a facade for the entity CMP -
 * FormNotifyAgentBean.<br>
 * <br>
 * 
 * @author SoniaKaura
 * @see FormNotifyBean
 * @version 1.0 24/06/2003
 * @ejbHome FormNotifyAgentHome
 * @ejbRemote FormNotifyAgentRObj
 */

@Stateless
public class FormNotifyAgentBean implements FormNotifyAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    
	@Resource
	private SessionContext context;

    /**
     * 
     * Looks up on the FormNotify id parameter passed in and constructs and
     * returns a FormNotify state keeper. containing all the details of the Form
     * Notification<br>
     * 
     * @param formNotifyId
     *            the Form Notify id
     */

    public FormNotifyBean getFormNotifyDetails(int formNotifyId) {
        FormNotifyBean retrieved = null;

        UserDao userDao = new UserDao();
        String userNameList = null;

        try {
            retrieved = (FormNotifyBean) em.find(FormNotifyBean.class,
                    new Integer(formNotifyId));

            String idList = retrieved.getUserList();
            Rlog.debug("formnotify", "FormNotifyAgentBean.getFormNotifyDetails"
                    + idList);
            // get the user ids from state keeper
            // pass it to the dao to get the name list
            userNameList = userDao.getUserNamesFromIds(idList);
            // set the name list in the state keeper before returning it
            retrieved.setNameList(userNameList);
            return retrieved;

        } catch (Exception e) {
            Rlog.fatal("formnotify",
                    "Error in getFormNotifyDetails() in FormNotifyAgentBean"
                            + e);
        }
        return retrieved;

    }

    /**
     * Creates Form Notify record.
     * 
     * @param a
     *            State Keeper containing the Form Notify attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */

    public int setFormNotifyDetails(FormNotifyBean formsk) {
        try {
            FormNotifyBean fb = new FormNotifyBean();
            fb.updateFormNotify(formsk);
            em.persist(fb);
            return fb.getFormNotifyId();
        } catch (Exception e) {
            Rlog.fatal("formnotify",
                    "Error in setFormNotifyDetails() in FormNotifyAgentBean "
                            + e);
            return -2;
        }

    }

    /**
     * Updates a FormNotify record.
     * 
     * @param a
     *            FormNotify state keeper containing FormNotify attributes to be
     *            set.
     * @return int for successful:0; e\Exception : -2
     */
    public int updateFormNotify(FormNotifyBean formsk) {

        FormNotifyBean retrieved = null; // Entity Bean Remote Object
        int output;

        try {

            retrieved = (FormNotifyBean) em.find(FormNotifyBean.class,
                    new Integer(formsk.getFormNotifyId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateFormNotify(formsk);
            em.merge(retrieved);
        } catch (Exception e) {
            Rlog.fatal("formnotify",
                    "Error in updateFormNotify in FormNotifyAgentBean" + e);
            return -2;
        }
        return output;
    }

    /**
     * Removes a FormNotify record.
     * 
     * @param a
     *            FormNotify Primary Key
     * @return int for successful:0; e\Exception : -2
     */

    public int removeFormNotify(int formNotifyId) {
        int output;
        FormNotifyBean retrieved = null;

        try {

            Rlog.debug("formnotify",
                    "in try bloc of FormNotify agent:removeFormNotify");

            retrieved = (FormNotifyBean) em.find(FormNotifyBean.class,
                    new Integer(formNotifyId));
            em.remove(retrieved);
            Rlog.debug("formnotify",
                    "Removed FormNotify row with FormNotifyID = "
                            + formNotifyId);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("formnotify", "Exception in removeFormNotify " + e);
            return -1;

        }

    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int removeFormNotify(int formNotifyId,Hashtable<String, String> auditInfo) {
        
        FormNotifyBean retrieved = null;
        AuditBean audit=null;
        String app_module;
        try {
        	String currdate =DateUtil.getCurrentDate();
        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String rid= AuditUtils.getRidValue("ER_FORMNOTIFY","eres","PK_FN="+formNotifyId); //Fetches the RID
            
            retrieved = (FormNotifyBean) em.find(FormNotifyBean.class,
                    new Integer(formNotifyId));
            int frmID = Integer.parseInt(retrieved.getFormLibId());
            FormLibBean formBean = em.find(FormLibBean.class, frmID);
            if(formBean.linkTo!=null&&formBean.linkTo.equalsIgnoreCase("L")){
            	app_module=LC.L_Form_Lib;
            }else{
            	app_module=LC.L_Form_Mgmt;
            }

            //POPULATE THE AUDIT BEAN 
            audit = new AuditBean("ER_FORMNOTIFY",String.valueOf(formNotifyId),rid,userID,currdate,app_module,ipAdd,reason);
            em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
            em.remove(retrieved);
            return 0;
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("formnotify", "Exception in removeFormNotify(int formNotifyId,Hashtable<String, String> auditInfo) " + e);
            return -1;
        }
    }
    

    /**
     * Calls getAllFormNotifications() on FormNotifyDao
     * 
     * @param formLibId
     * 
     * @return FormNotifyDao with resultset
     * @see FormNotifyDao
     */

    public FormNotifyDao getAllFormNotifications(int formLibId) {
        try {
            Rlog.debug("formnotify",
                    "In getAllFormNotificaions in FormNotifyAgentBean - 0");
            FormNotifyDao formNotifyDao = new FormNotifyDao();
            formNotifyDao.getAllFormNotifications(formLibId);
            return formNotifyDao;
        } catch (Exception e) {
            Rlog.fatal("formnotify",
                    "Exception In getAllFormNotifications in FormNotifyAgentBean "
                            + e);
        }
        return null;

    }

}
