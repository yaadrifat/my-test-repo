/*
 * Classname : StdSiteAddInfoAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 11/16/2004
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.service.stdSiteAddInfoAgent.impl;

/* Import Statements */

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.StdSiteAddInfoDao;
import com.velos.eres.business.stdSiteAddInfo.impl.StdSiteAddInfoBean;
import com.velos.eres.service.stdSiteAddInfoAgent.StdSiteAddInfoAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The stateless session EJB acting as a facade for the entity CMP -
 * StdSiteAddInfoAgentBean.<br>
 * <br>
 * 
 * @author Anu Khannna
 * @see stdSiteAddInfoAgentBean
 * @version 1.0 11/16/2004
 * @ejbHome stdSiteAddInfoAgentHome
 * @ejbRemote stdSiteAddInfoAgentRObj
 */
@Stateless
public class StdSiteAddInfoAgentBean implements StdSiteAddInfoAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * 
     * Looks up on the id parameter passed in and constructs and returns a
     * studyId state keeper. containing all the details of the studyId <br>
     * 
     * @param id
     */

    public StdSiteAddInfoBean getStdSiteAddInfoDetails(int id) {

        try {
            return (StdSiteAddInfoBean) em.find(StdSiteAddInfoBean.class,
                    new Integer(id));

        } catch (Exception e) {
            Rlog.fatal("stdSiteAddInfo",
                    "Exception in getStdSiteAddInfoDetails() in StdSiteAddInfAgentBean"
                            + e);
            return null;
        }

    }

    /**
     * Creates stdSiteAddInfo record.
     * 
     * @param a
     *            State Keeper containing the studyId attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */

    public int setStdSiteAddInfoDetails(StdSiteAddInfoBean ssk) {
        try {
            StdSiteAddInfoBean stdSiteBean = new StdSiteAddInfoBean();
            stdSiteBean.updateStdSiteAddInfo(ssk);
            em.merge(stdSiteBean);
            return stdSiteBean.getId();

        } catch (Exception e) {
            Rlog.fatal("stdSiteAddInfo",
                    "Error in setStdSiteAddInfoDetails() in StdSiteAddInfoAgentBean "
                            + e);
            return -2;
        }

    }

    /**
     * Updates a stdSiteAddInfo record.
     * 
     * @param a
     *            studyId state keeper containing stdSiteAddInfo attributes to
     *            be set.
     * @return int for successful:0 ; e\Exception : -2
     */
    public int updateStdSiteAddInfo(StdSiteAddInfoBean ssk) {

        StdSiteAddInfoBean retrieved = null; // Entity Bean Remote Object
        int output;
        try {
            retrieved = (StdSiteAddInfoBean) em.find(StdSiteAddInfoBean.class,
                    new Integer(ssk.getId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateStdSiteAddInfo(ssk);
        } catch (Exception e) {
            Rlog.fatal("stdSiteAddInfo",
                    "Error in updateStdSiteAddInfo in StdSiteAddInfoAgentBean"
                            + e);
            return -2;
        }
        return output;
    }

    /**
     * Removes a stdSiteAddInfo record.
     * 
     * @param a
     *            StdSiteAddInfo Primary Key
     * @return int for successful:0; e\Exception : -2
     */

    public int removeStdSiteAddInfo(int id) {
        int output;
        StdSiteAddInfoBean stdSiteAddInfoRObj = null;

        try {

            stdSiteAddInfoRObj = em.find(StdSiteAddInfoBean.class, new Integer(
                    id));
            em.remove(stdSiteAddInfoRObj);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("stdSiteAddInfo", "Exception in removeStdSiteAddInfo"
                    + e);
            return -1;

        }

    }

    /**
     * Creates multiple studyids
     * 
     * @param StudyIdStateKeeper
     * @return int
     */
    public int createMultipleStdSiteAddInfo(StdSiteAddInfoBean ssk) {
        try {

            StdSiteAddInfoDao sDao = new StdSiteAddInfoDao();
            ArrayList sskArray = ssk.getStdSiteAddInfoStateKeepers();
            ArrayList addInfoType = new ArrayList();
            ArrayList recordTypeList = new ArrayList();
            ArrayList stdSiteInfoPk = new ArrayList();
            int rows = sskArray.size();
            int studySiteId = 0, index = -1, idPk = 0;

            if (rows > 0) {
                studySiteId = StringUtil
                        .stringToNum(((StdSiteAddInfoBean) sskArray.get(0))
                                .getStudySiteId());
            }
            sDao = getStudySiteAddInfo(studySiteId);
            addInfoType = sDao.getAddInfoType();
            recordTypeList = sDao.getRecordType();
            stdSiteInfoPk = sDao.getId();

            int ret = 0;
            String recordType = "";
            String recType = "";
            String addInfoTypeId = "";

            for (int i = 0; i < rows; i++) {
                StdSiteAddInfoBean StdSiteAddInfoStateKeeperTemp = new StdSiteAddInfoBean();

                StdSiteAddInfoStateKeeperTemp = (StdSiteAddInfoBean) sskArray
                        .get(i);

                addInfoTypeId = StdSiteAddInfoStateKeeperTemp
                        .getAddCodelstInfo();

                index = addInfoType.indexOf(new Integer(addInfoTypeId));

                if (index >= 0) {
                    recordType = (String) recordTypeList.get(index);
                    idPk = ((Integer) stdSiteInfoPk.get(index)).intValue();
                }

                if (recordType == null)
                    recordType = "";
                if (recordType.length() == 0)
                    recordType = "N";

                if (recordType.equals("N"))
                    ret = setStdSiteAddInfoDetails(StdSiteAddInfoStateKeeperTemp);
                else if (recordType.equals("M")) {
                    StdSiteAddInfoStateKeeperTemp.setId(idPk);
                    StdSiteAddInfoStateKeeperTemp.setRecordType("M");
                    ret = updateStdSiteAddInfo(StdSiteAddInfoStateKeeperTemp);
                }

                Rlog.debug("stdSiteAddInfo",
                        "StdSiteAddInfoAgentBean.INSIDE ROWS AFTER SETTING###"
                                + ret);
            }

            return ret;
        } catch (Exception e) {
            Rlog.fatal("stdSiteAddInfo",
                    "Exception In createMultipleStdSiteAddInfo in StdSiteAddInfoAgentBean "
                            + e);
            e.printStackTrace();
            return -2;
        }

    }

    /**
     * Updates multiple Study Ids
     * 
     * @param StudyIdStateKeeper
     * @return int
     */
    /*
     * public int updateMultipleStudyIds(StudyIdStateKeeper ssk) { try {
     * Rlog.debug("studyId","StudyIdAgentBean.updateMultipleStudyIds");
     * 
     * ArrayList sskArray = ssk.getStudyIdStateKeepers() ;
     * 
     * int rows = sskArray.size(); int ret = 0;
     * 
     * 
     * for(int i =0 ; i<rows ; i++) {
     * 
     * StudyIdStateKeeper StudyIdStateKeeperTemp = new StudyIdStateKeeper();
     * StudyIdStateKeeperTemp = (StudyIdStateKeeper) sskArray.get(i);
     * 
     * //getStudyIdDetails(StudyIdStateKeeperTemp.getId()); ret =
     * updateStudyId(StudyIdStateKeeperTemp);
     * Rlog.debug("studyId","StudyIdAgentBean.INSIDE ROWS AFTER SETTING ###"+ret ); }
     * 
     * return ret; } catch(Exception e) { Rlog.fatal("studyId","Exception
     * StudyIdAgentBean.updateMultipleStudyIds " + e); return -2; } }
     * 
     */

    public StdSiteAddInfoDao getStudySiteAddInfo(int studySiteId) {

        try {
            StdSiteAddInfoDao sId = new StdSiteAddInfoDao();
            sId.getStudySiteAddInfo(studySiteId);
            return sId;

        } catch (Exception e) {
            Rlog
                    .fatal("stdSiteAddInfo", "Exception in getStudySiteAddInfo"
                            + e);
            return null;

        }
    }

}
