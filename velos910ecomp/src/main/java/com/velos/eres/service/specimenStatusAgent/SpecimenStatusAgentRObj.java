/*
 * Classname			SpecimenStatusAgentRObj
 *
 * Version information : 1.0
 *
 * Date					10/15/2007
 *
 * Copyright notice : Velos Inc
 *
 */

package com.velos.eres.service.specimenStatusAgent;

import java.util.Hashtable;
import javax.ejb.Remote;
import com.velos.eres.business.common.SpecimenStatusDao;

import com.velos.eres.business.specimenStatus.impl.SpecimenStatusBean;

/**
 * Remote interface for SpecimenStatusAgentBean session EJB
 *
 * @author Jnanamay
 * @version 1.0, 10/15/2007
 */
@Remote
public interface SpecimenStatusAgentRObj {
    public SpecimenStatusBean getSpecimenStatusDetails(int pkSpecStat);

    public int setSpecimenStatusDetails(SpecimenStatusBean specStat);

    public int updateSpecimenStatus(SpecimenStatusBean specStat);

    public SpecimenStatusDao getSpecimenStausValues(int specId) ;
    public SpecimenStatusDao getSpecimenStausProcessValues(int specId) ;


    public int delSpecimenStatus(int pkSpecimenStat);
    
    // Overloaded for INF-18183 ::: AGodara
    public int delSpecimenStatus(int pkSpecimenStat,Hashtable<String, String> auditInfo);

    public SpecimenStatusDao getLatestSpecimenStaus(int specimenId);
    
    // Ankit: Bug#9971,  Date# 02-July-2012
    public int updateSpecimenProcessType(SpecimenStatusBean ap);


}
