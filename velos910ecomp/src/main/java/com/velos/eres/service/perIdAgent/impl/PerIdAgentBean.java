/*
 * Classname : PerIdAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 16/02/04
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.service.perIdAgent.impl;

/* Import Statements */

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.PerIdDao;
import com.velos.eres.business.perId.impl.PerIdBean;
import com.velos.eres.service.perIdAgent.PerIdAgentRObj;
import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

/**
 * The stateless session EJB acting as a facade for the entity CMP -
 * PerIdAgentBean.<br>
 * <br>
 * 
 * @author Sonia Sahni
 * @see perIdAgentBean
 * @version 1.0 09/22/2003
  * @ejbRemote perIdAgentRObj
 */
@Stateless
public class PerIdAgentBean implements PerIdAgentRObj {
    @PersistenceContext(unitName = "epat")
    protected EntityManager em;

    /**
     * 
     */
    private static final long serialVersionUID = 3257850995504722484L;

    /**
     * 
     * Looks up on the id parameter passed in and constructs and returns a perId
     * state keeper. containing all the details of the perId <br>
     * 
     * @param id
     *            the perId id
     */

    public PerIdBean getPerIdDetails(int id) {

        try {
            return (PerIdBean) em.find(PerIdBean.class, new Integer(id));

        } catch (Exception e) {
            Rlog.fatal("perId",
                    "Exception in getStudIdDetails() in PerIdAgentBean" + e);
            return null;
        }

    }

    /**
     * Creates perId record.
     * 
     * @param a
     *            State Keeper containing the perId attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */

    public int setPerIdDetails(PerIdBean psk) {
        try {
            em.merge(psk);
            return psk.getId();
        } catch (Exception e) {
            // Rlog.fatal("perId","Error in setPerIdDetails() in PerIdAgentBean
            // " + e);
            return -2;
        }

    }

    /**
     * Updates a perId record.
     * 
     * @param a
     *            perId state keeper containing perId attributes to be set.
     * @return int for successful:0 ; e\Exception : -2
     */
    public int updatePerId(PerIdBean psk) {

        PerIdBean retrieved = null; // Entity Bean Remote Object
        int output;

        try {

            retrieved = (PerIdBean) em.find(PerIdBean.class, new Integer(psk
                    .getId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updatePerId(psk);

        } catch (Exception e) {
            // Rlog.fatal("perId","Error in updatePerId in PerIdAgentBean" + e);
            return -2;
        }
        return output;
    }

    /**
     * Removes a perId record.
     * 
     * @param a
     *            PerId Primary Key
     * @return int for successful:0; e\Exception : -2
     */

    public int removePerId(int id) {
        int output;
        PerIdBean perIdRObj = null;

        try {

            perIdRObj = (PerIdBean) em.find(PerIdBean.class, new Integer(id));
            em.remove(perIdRObj);
            return 0;
        } catch (Exception e) {
            // Rlog.fatal("perId","Exception in removePerId" + e);
            return -1;

        }

    }

    public PerIdDao getPerIds(int perId,String defUserGroup) {

        try {
            PerIdDao pId = new PerIdDao();
            pId.setForGroup(defUserGroup);
            pId.getPerIds(perId);
            return pId;

        } catch (Exception e) {
            Rlog.fatal("perId", "Exception in getPerIds" + e);
            return null;

        }
    }

    /**
     * Creates multiple perids
     * 
     * @param PerIdStateKeeper
     * @return int
     */
    public int createMultiplePerIds(PerIdBean psk) {
        try {
            Rlog.debug("perId", "PerIdAgentBean.createMultiplePerIds ");

            ArrayList pskArray = psk.getPerIdStateKeepers();

            int rows = pskArray.size();
            int ret = 0;
            String recordType = null;

            for (int i = 0; i < rows; i++) {
                PerIdBean PerIdStateKeeperTemp = new PerIdBean();

                PerIdStateKeeperTemp = (PerIdBean) pskArray.get(i);

                recordType = PerIdStateKeeperTemp.getRecordType();

                if (recordType == null)
                    recordType = "N";

                if (recordType.equals("N"))
                    ret = setPerIdDetails(PerIdStateKeeperTemp);
                else if (recordType.equals("M"))
                    ret = updatePerId(PerIdStateKeeperTemp);

                Rlog.debug("perId",
                        "PerIdAgentBean.INSIDE ROWS AFTER SETTING###" + ret);
            }

            return ret;
        } catch (Exception e) {
            Rlog.fatal("perId",
                    "Exception In createMultiplePerIds in PerIdAgentBean " + e);
            return -2;
        }

    }

}
