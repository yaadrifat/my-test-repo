package com.velos.eres.service.memberRightsAgent;

import javax.ejb.Remote;

import com.velos.eres.business.memberRights.impl.MemberRightsBean;

@Remote
public interface MemberRightsAgentRObj 
{
	public MemberRightsBean getMemberRightsDetails(int id);    
    public int updateMemberRights(MemberRightsBean mrk);
}
