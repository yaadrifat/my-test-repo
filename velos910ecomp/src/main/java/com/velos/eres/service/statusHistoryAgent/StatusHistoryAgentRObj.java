/*
 * Classname : StatusHistoryAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 08/09/2004
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.service.statusHistoryAgent;

/* Import Statements */

import java.util.Hashtable;
import javax.ejb.Remote;

import com.velos.eres.business.common.StatusHistoryDao;
import com.velos.eres.business.statusHistory.impl.StatusHistoryBean;

/* End of Import Statements */

/**
 * Remote interface for StatusHistoryAgent session EJB
 * 
 * @author Anu
 */

@Remote
public interface StatusHistoryAgentRObj {

    StatusHistoryBean getStatusHistoryDetails(int statusId);

    public int setStatusHistoryDetails(StatusHistoryBean hsk);
    
    public int updateStatusHistory(StatusHistoryBean hsk);
    //Added by Manimaran for the July-August Enhancement S4.
    public int setStatusHistoryDetailsWithEndDate(StatusHistoryBean hsk);
    
    public int updateStatusHistoryWithEndDate(StatusHistoryBean hsk);

    public int deleteStatusHistory(StatusHistoryBean hsk);
    
    // deleteStatusHistory() Overloaded for INF-18183 ::: Raviesh
    public int deleteStatusHistory(StatusHistoryBean hsk,Hashtable<String, String> args);

    public StatusHistoryDao getStatusHistoryInfo(int modulePK,
            String moduleTable);

    public String getLatestStatus(int modulePK, String moduleTable);
    
    public int getLatestStatusId(int modulePK, String moduleTable);
}
