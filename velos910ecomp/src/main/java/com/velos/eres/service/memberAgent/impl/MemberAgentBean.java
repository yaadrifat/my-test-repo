package com.velos.eres.service.memberAgent.impl;

import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.MemberDao;
import com.velos.eres.business.memberBoard.impl.MemberBean;
import com.velos.eres.service.memberAgent.MemberAgentRObj;
import com.velos.eres.service.util.Rlog;

@Stateless
@Remote({MemberAgentRObj.class})
public class MemberAgentBean implements MemberAgentRObj {
	
	@PersistenceContext(unitName = "eres")
    protected EntityManager em;
    
	@Resource
	private SessionContext context;
   

    public MemberBean getMemberDetails(int fkBoardId) {
        MemberBean memberBean = null;
        try {
            memberBean = (MemberBean) em.find(MemberBean.class, new Integer(fkBoardId));                      
            Rlog.debug("member", "got MemberDetails by Board Id." );
            return memberBean;
        } catch (Exception e) {
            Rlog.fatal("member", "EXCEPTION in getMemberDetails" + e);
            return memberBean;
        }
    }
    
    public MemberBean getMemberDetailsById(int pkRBMember) {
        MemberBean memberBean = null;
        try {
	            memberBean = (MemberBean) em.find(MemberBean.class, new Integer(pkRBMember));                       
	            Rlog.debug("member", "got MemberDetails by Member Id." );
	            return memberBean;
        } catch (Exception e) {
            Rlog.fatal("member", "EXCEPTION in getMemberDetailsById." + e);
            return memberBean;
        }
    }
    
    public int setMemberDetails(MemberBean memBean)
    {
       int pkRBMember = 0;     
       MemberBean mb = new MemberBean();        
        try {
        		Rlog.debug("member", "got setMemberDetails." );      	
	    		mb.updateMember(memBean);        		
	    		em.persist(mb);        		
	    		pkRBMember=mb.getPkRBMember();        		
	    		return pkRBMember;	           
	       } catch (Exception e) {
	    	   Rlog.fatal("member", "EXCEPTION in setMemberDetails." + e);
	           return 0;
	       }
	       
    }
    
    public int updateMember(MemberBean tsk) {
        int ret = 0;
        try {
	            MemberBean memberBean = (MemberBean) em.find(MemberBean.class, tsk.getPkRBMember());
	            if (memberBean == null)
	            {               
	            	return -2;
	            }
	            Rlog.debug("member", "got updateMember." );
	            ret = memberBean.updateMember(tsk);
	            em.merge(memberBean);	            
	            return ret;
        	} 
        catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("member", "EXCEPTION in updateMember." + e);
            return -2;
        }
    }
    
    // remove from team

    public int deleteFromBoard(MemberBean msk) {
        try {        	
            Rlog.debug("member", "in remove from board");
            MemberBean memberBean = (MemberBean) em.find(MemberBean.class, msk.getPkRBMember());
            if (memberBean == null) {
                return -2;
            }            
            em.remove(memberBean);
            Rlog.debug("member", "removed from board");            
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("member", "EXCEPTION IN DELETING FROM board" + e);
            return -2;
        }
    }
    
    @Override
	public MemberDao getMemberRights(int studyId, int userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MemberDao getMemberValues(int studyId) {
		// TODO Auto-generated method stub
		return null;
	}
}
