/*
 * Classname			MemberRightsAgentBean.class
 * 
 * Version information
 *
 * Date					01/07/2013
 * 
 * Copyright notice
 */

package com.velos.eres.service.memberRightsAgent.impl;

import java.util.ArrayList;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.velos.eres.business.common.CtrlDao;
import com.velos.eres.business.memberRights.impl.MemberRightsBean;
import com.velos.eres.service.memberRightsAgent.MemberRightsAgentRObj;
import com.velos.eres.service.util.Rlog;

/**
 * The stateless session EJB acting as a facade for the entity CMP.
 * 
 * @author
 */
@Stateless
public class MemberRightsAgentBean implements MemberRightsAgentRObj {
	
	@PersistenceContext(unitName = "eres")
    protected EntityManager em;
    
    public MemberRightsBean getMemberRightsDetails(int id) {

    	MemberRightsBean mrk = new MemberRightsBean();

        String memberRight = null;
        
        CtrlDao cntrl;
        
        ArrayList cVal, cDesc;
        
        cVal = new ArrayList();
        
        cVal = new ArrayList();
        
        int sLength = 0;
        int counter = 0;

        cntrl = new CtrlDao();
        try {
        	mrk = (MemberRightsBean) em.find(MemberRightsBean.class, new Integer(id));
        	
            memberRight = mrk.getMemRights(); 
            
            Rlog.debug("memberRights", "State Keeper for Member Rights" + mrk);
            
            sLength = memberRight.length();
            
            Rlog.debug("memberRights", "Length Of the Member Rights" + sLength);
            
            for (counter = 0; counter <= (sLength - 1); counter++) 
            {
                Rlog.debug("memberRights", "Counter" + counter);
                
                Rlog.debug("memberRights", "Member Rights at the current Counter" + String.valueOf(memberRight.charAt(counter)));
                
                mrk.setFtrRights(String.valueOf(memberRight.charAt(counter)));
                
                mrk.setGrSeq(Integer.toString(counter));
            }

            cntrl.getControlValues("member_rights");
            
            cVal = cntrl.getCValue();
            
            cDesc = cntrl.getCDesc();
            
            mrk.setGrValue(cVal);
            
            mrk.setGrDesc(cDesc);
                        
        } catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("memberRights", "EXCEPTION" + e);
        }
        Rlog.debug("memberRights", "RETURNING FROM SESSION BEAN GROUP RIGHTS"  + mrk);
        return mrk;
    }
    
    
    public int updateMemberRights(MemberRightsBean mrk) {
        int counter = 0;
        int sLength = 0;
        int result = 0;
        ArrayList memberRht = new ArrayList();
        MemberRightsBean memberRights = new MemberRightsBean();
        String rights = "";
        
        try {        	
        	memberRights = (MemberRightsBean) em.find(MemberRightsBean.class,new Integer(mrk.getId()));
        	
        	memberRht = mrk.getFtrRights();
        	
            Rlog.debug("memberRights", "Study Rights from State Keeper " + memberRht);
            
            sLength = memberRht.size();
            
            Rlog.debug("memberRights", "Size of Array List Rights " + sLength);
            
            for (counter = 0; counter <= (sLength - 1); counter++) 
            {
                rights += memberRht.get(counter).toString();
            }
            Rlog.debug("memberRights", "String of Rights " + rights);
            
            result = memberRights.updateMemberRights(mrk, rights);
            
            Rlog.debug("memberRights", "Updation of Member Rights" + result);
           
        } catch (Exception e) {
            Rlog.fatal("memberRights", "EXCEPTION" + e);
            e.printStackTrace();
            return -2;
        }
        return 0;
    }	
}
