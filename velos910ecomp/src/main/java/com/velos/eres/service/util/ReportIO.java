/*


 * Classname			ReportIO



 * Version information  1.0


 *


 * Date					05/12/2001	


 * 


 * Copyright notice		Velos Inc.


 * 


 * Author 				Sonia Sahni


 */

package com.velos.eres.service.util;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

/**
 * 
 * 
 * This class is used to save and access report output as temporary documents.
 * It was originally designed for report outputs but can be used for saving any
 * type of HTML output as a temporary file
 * 
 * @author Sonia Sahni
 * @version 1.0 05/12/2001
 * 
 * 
 */

/*
 * Modified on 4/Aug/04. This method now use fileUploadDownload.xml and files in
 * com.aithent.file.uploadDownload
 */

public final class ReportIO

{

    public static String STYLE_TAG = "";
    private BufferedWriter bWriter;
    private String fileName="";

    /**
     * Saves an HTML string as a temporary document. <br>
     * Uses settings in FileUploadDownload.xml to store and download the
     * temporary file.
     * 
     * @param reportStr
     *            HTML string
     * @param docType
     *            document type for the temporary document, possible values:
     *            <br>
     *            doc - MS Document <br>
     *            xls - MS Excel htm - HTML document
     * @param fileName
     *            First part of the temporary file name. The file name gets
     *            appended <br>
     *            with date/time information
     * @return returns download path for the temporary file created
     * 
     */

    public static String saveReportToDoc(String reportStr, String docType,
            String fileName)

    {

        String header = null;
        String footer = null;
        String contents = null;
        String filExt = "txt";
        String filePath = "null";
        String completeFileName = null;
        String downloadStr = null;
        String fileUrl="";
        int sType;

        Calendar now = Calendar.getInstance();

        com.aithent.file.uploadDownload.Configuration.readSettings("eres");

        com.aithent.file.uploadDownload.Configuration
                .readUploadDownloadParam(
                        com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD
                                + "fileUploadDownload.xml", null);

        filePath = com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER;

        if (docType.compareToIgnoreCase("doc") == 0) {
            header = "<html><head><title>Velos eResearch</title><meta http-equiv='Content-Type' content='application/ms-word; charset=UTF-8'>"
                    + STYLE_TAG + "</head><body>";
            filExt = ".doc";
        }

        if (docType.compareToIgnoreCase("xls") == 0) {
            header = "<html><head><title>Velos eResearch</title><meta http-equiv='Content-Type' content='application/vnd.ms-excel; charset=UTF-8'>"
                    + STYLE_TAG + "</head><body>";
            filExt = ".xls";
        }

        if (docType.compareToIgnoreCase("htm") == 0) {
            header = "<html><head><title>Velos eResearch</title><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>"
                    + STYLE_TAG + "</head><body>";
            filExt = ".html";
        }

        footer = "</body></html>";
        
        if (docType.compareToIgnoreCase("xml") == 0) {
            header = "<html><head><title>Velos eResearch</title><meta http-equiv='Content-Type' content='text/html'>"
                    + STYLE_TAG + "</head><body>";
            filExt = ".xml";
            footer = "";
            header = "";
        }
        

        Rlog.debug("common", "###header" + header);
        contents = header + reportStr + footer;

        Rlog.debug("common", "###contents" + contents);

        byte[] fileBytes = contents.getBytes();

        try {

            completeFileName = fileName + "[" +System.currentTimeMillis()+ "]" + filExt;

            sType = EJBUtil.getSystemType();

            FileOutputStream file;

            if (sType == 0) {

                file = new FileOutputStream(filePath + "\\" + completeFileName);
            } else {
                file = new FileOutputStream(filePath + "//" + completeFileName);
            }

            file.write(fileBytes, 0, fileBytes.length);

            file.close();

        } catch (Exception e) {
            Rlog.fatal("common", "Exception in saving report " + e);
            return null;
        }

        //downloadStr = com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET
         //       + "/" + completeFileName;
        downloadStr = com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET
               + "?file=" + completeFileName;

        return downloadStr;

    }
  
    //KLUDGE : This method can be generalized a bit and lot of information can be at class level.This needs to be done if time permits, ever
   public BufferedWriter initializeWriter(String docType, String fileName)
    {
	    String header="",filExt="";
	   Calendar now = Calendar.getInstance();

       com.aithent.file.uploadDownload.Configuration.readSettings("eres");

       com.aithent.file.uploadDownload.Configuration
               .readUploadDownloadParam(
                       com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD
                               + "fileUploadDownload.xml", null);

       String filePath = com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER;

       if (docType.compareToIgnoreCase("doc") == 0) {
           header = "<html><head><title>Velos eResearch</title><meta http-equiv='Content-Type' content='application/ms-word; charset=UTF-8'>"
                   + STYLE_TAG + "</head><body>";
           filExt = ".doc";
       }

       if (docType.compareToIgnoreCase("xls") == 0) {
           header = "<html><head><title>Velos eResearch</title><meta http-equiv='Content-Type' content='application/vnd.ms-excel; charset=UTF-8'>"
                   + STYLE_TAG + "</head><body>";
           filExt = ".xls";
       }

       if (docType.compareToIgnoreCase("htm") == 0) {
           header = "<html><head><title>Velos eResearch</title><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>"
                   + STYLE_TAG + "</head><body>";
           filExt = ".html";
       }
       String completeFileName = fileName + "[" + System.currentTimeMillis()+"]" + filExt;
       this.fileName=completeFileName;
       try {
      int sType = EJBUtil.getSystemType();
      if (sType == 0) {

           bWriter = new BufferedWriter(new FileWriter(filePath + "\\" + completeFileName, true));
       } else {
    	   bWriter = new BufferedWriter(new FileWriter(filePath + "//" + completeFileName, true));
       }
      	bWriter.write(header);
      	}catch(Exception e )
       {
    	   e.printStackTrace();
       }
	 return bWriter;  
    }
   
   public void write(String data)
   {
	  try {
	   bWriter.write(data);
	    } catch(Exception ie)
	    {
	    	ie.printStackTrace();
	    }
   }
   
   public String getFileUrl()
   {
	   return com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET
       + "?file=" + this.fileName;
   }
   
   
   
   //KLUDGE : Hard Coding for footer. Footer can be member of Class and allowed to be customized
   public void deactivateWriter()
   {
	   try{
	   String footer = "</body></html>";
	   bWriter.write(footer);
	   bWriter.close();
	   } catch (IOException ie) 
	   {
		   ie.printStackTrace();
	   }
   }
 }

