/*
 * Classname			MilepaymentAgentRObj
 * 
 * Version information 	1.0
 *
 * Date					05/28/2002
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.service.milepaymentAgent;

import java.util.Hashtable;
import javax.ejb.Remote;

import com.velos.eres.business.common.MilepaymentDao;
import com.velos.eres.business.milepayment.impl.MilepaymentBean;

/**
 * Remote interface for MilepaymentAgent session EJB
 * 
 * @author Sonika Talwar
 */
@Remote
public interface MilepaymentAgentRObj {
    /**
     * gets the milepayment details
     */
    public MilepaymentBean getMilepaymentDetails(int milepaymentId);

    /**
     * sets the milepayment details
     */
    public int setMilepaymentDetails(MilepaymentBean msk);

    public int updateMilepayment(MilepaymentBean ssk);
    // Overloaded for INF-18183 ::: AGodara	
    public int updateMilepayment(MilepaymentBean ssk,Hashtable<String, String> auditInfo);

    public MilepaymentDao getMilepaymentReceived(int studyId);

}
