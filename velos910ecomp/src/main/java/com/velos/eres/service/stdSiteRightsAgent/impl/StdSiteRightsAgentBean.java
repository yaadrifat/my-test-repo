/**
 * Classname			StdSiteRightsAgentBean
 * 
 * Version information 	1.0
 *
 * Date				11/17/2004
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.service.stdSiteRightsAgent.impl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.StdSiteRightsDao;
import com.velos.eres.business.common.UserSiteDao;
import com.velos.eres.business.stdSiteRights.impl.StdSiteRightsBean;
import com.velos.eres.service.stdSiteRightsAgent.StdSiteRightsAgentRObj;
import com.velos.eres.service.util.Rlog;

/**
 * The stateless session EJB acting as a facade for the entity CMP.<br>
 * <br>
 * 
 * @author Anu Khanna
 * @version 1.0 11/17/2004
 */
@Stateless
public class StdSiteRightsAgentBean implements StdSiteRightsAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * Calls getStdSiteRightsDetails() on entity bean - StdSiteRightsBean Looks
     * up on the study Site rights id parameter passed in and constructs and
     * returns a study Site rights state holder. containing all the summary
     * details of the study site rights<br>
     * 
     * @param stdSiteRightsId
     *            the Study Site rights id
     * @see StdSiteRightsBean
     */
    public StdSiteRightsBean getStdSiteRightsDetails(int stdSiteRightsId) {

        try {
            return (StdSiteRightsBean) em.find(StdSiteRightsBean.class,
                    new Integer(stdSiteRightsId));
        } catch (Exception e) {
            Rlog.fatal("stdSiteRights",
                    "Error in getStdSiteRightsDetails() in StdSiteRightsAgentBean"
                            + e);
            return null;
        }

    }

    /**
     * Creates a new a Study Site rights. Calls setStdSiteRightsDetails() on
     * entity bean StdSiteRightsBean
     * 
     * @param a
     *            study site rights state holder containing user site attributes
     *            to be set.
     * @return void
     * @see StdSiteRightsBean
     */

    public int setStdSiteRightsDetails(StdSiteRightsBean ssk) {
        try {
            StdSiteRightsBean stdRightBean = new StdSiteRightsBean();
            stdRightBean.updateStdSiteRights(ssk);
            em.persist(stdRightBean);
            return (stdRightBean.getStdSiteRightsId());
        } catch (Exception e) {
            Rlog.fatal("stdSiteRights",
                    "Error in setStdSiteRightsDetails() in StsSiteRightsAgentBean "
                            + e);
        }
        return 0;
    }

    /**
     * Updates Study Site Rights Details. Calls updateStdSiteRights() on entity
     * bean StdSiteRightsBean
     * 
     * @param a
     *            study site state holder containing user site attributes to be
     *            set.
     * @return int 0 for successful ; -2 for exception
     * @see StdSiteRightsBean
     */

    public int updateStdSiteRights(StdSiteRightsBean ssk) {

        StdSiteRightsBean retrieved = null; // Study site rights Entity Bean
        int output = 0; // Remote Object

        try {

            retrieved = (StdSiteRightsBean) em.find(StdSiteRightsBean.class,
                    new Integer(ssk.getStdSiteRightsId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateStdSiteRights(ssk);
        } catch (Exception e) {
            Rlog.fatal("stdSiteRights",
                    "Error in updateStdSiteRights in StdSiteRightsAgentBean"
                            + e);
            return -2;
        }
        return output;
    }

    /**
     * Calls updateUserSite of UserSiteDao Overloaded method
     * 
     * @param Array
     *            of pkUserSites
     * @param Array
     *            of corresponding rights for each user site
     * @param userId
     * @param IP
     *            Address
     * @returns 0 if successful, -1 exception
     * @see UserSiteDao
     */

    public int updateStudySite(String[] pkStudySites, String[] siteIds,
            String[] rights, int studyId, int userId, String ipAdd,String creator) {
        int ret = 0;
        StdSiteRightsDao stdSiteDao = new StdSiteRightsDao();
        try {
            ret = stdSiteDao.updateStudySite(pkStudySites, siteIds, rights,
                    studyId, userId, ipAdd,creator);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("stdSiteRights",
                    "Error in updateStudySite calling SP in AgentBean" + e);
            return -1;
        }
    }

    /**
     * Calls getUserSiteTree of UserSiteDao
     * 
     * @param Account
     *            Id
     * @param userId
     * @returns UserSiteDao
     * @see UserSiteDao
     */

    public StdSiteRightsDao getStudySiteTree(int accountId, int loginUser,
            int studyId) {
        StdSiteRightsDao stdSiteDao = new StdSiteRightsDao();
        try {
            stdSiteDao.getStudySiteTree(accountId, loginUser, studyId);
        } catch (Exception e) {
            Rlog.fatal("stdSiteRights",
                    "Error in getStudySiteTree in AgentBean" + e);
        }
        return stdSiteDao;
    }

    /**
     * Get organizations with view access for an account user for a study
     * 
     * @param Account
     *            Id
     * @param userId
     * @param studyId
     * @returns StdSiteRightsDao
     * @see StdSiteRightsDao
     */

    public StdSiteRightsDao getStudySitesWithViewRight(int accId, int userId,
            int studyId) {
        StdSiteRightsDao stdSiteDao = new StdSiteRightsDao();
        try {
            stdSiteDao.getStudySitesWithViewRight(accId, userId, studyId);
        } catch (Exception e) {
            Rlog.fatal("stdSiteRights",
                    "Error in getStudySitesWithViewRight in StdSiteRightsAgentBean"
                            + e);
        }
        return stdSiteDao;
    }

    /**
     * Get organizations with new access for an account user, calls
     * getSitesWithNewRight of Session Bean
     * 
     * @param Account
     *            Id
     * @param userId
     * @returns UserSiteDao
     * @see UserSiteDao
     */

    /*
     * public UserSiteDao getSitesWithNewRight(int accId, int userId) {
     * UserSiteDao userSiteDao = new UserSiteDao(); try {
     * userSiteDao.getSitesWithNewRight(accId,userId); } catch(Exception e){
     * Rlog.fatal("userSite","Error in getSitesWithNewRight in
     * UserSiteAgentBean" + e); } return userSiteDao; }
     */

    /**
     * Calls createUserSiteData() on UserSiteDao * to create er_usersite data
     * for all the organizations(sites) for user's account * The default site
     * gets rights same as user's default group's Manage Patients rights *
     * Author: Sonia Sahni 12th May 2003 *
     * 
     * @param user :
     *            Pk_user *
     * @param account :
     *            pk of user account *
     * @param def_group :
     *            pk of user default group *
     * @param old_site :
     *            pk of previous user site *
     * @param def_site :
     *            pk of user site *
     * @param creator :
     *            pk of creator (fk_user) *
     * @param ip :
     *            ip add of client machine *
     * @returns : 0 for successful update, -1 for error
     */

    public int createStudySiteRightsData(int user, int account, int studyId,
            int creator, String ipAdd) {

        StdSiteRightsDao stdSiteRightsDao = new StdSiteRightsDao();
        int ret = 0;

        try {
            ret = stdSiteRightsDao.createStudySiteRightsData(user, account,
                    studyId, creator, ipAdd);
        } catch (Exception e) {
            Rlog.fatal("stdSiteRights",
                    "Error in createStudySiteRightsData in StdSiteRightsAgentBean"
                            + e);
            return -1;
        }
        return ret;

    }

    public int deleteStdSiteRights(int studyId, int userId) {

        StdSiteRightsDao stdSiteRightsDao = new StdSiteRightsDao();
        int ret = 0;

        try {
            ret = stdSiteRightsDao.deleteStdSiteRights(studyId, userId);
        } catch (Exception e) {
            Rlog.fatal("stdSiteRights",
                    "Error in deleteStdSiteRights in StdSiteRightsAgentBean"
                            + e);
            return -1;
        }
        return ret;

    }
    /**
     * Get user site right
     * 
     * @param userId
     * @param siteId
     * @return userSite Right
     * @see UserSiteHome
     */

    /*
     * public int getRightForUserSite(int userId, int siteId){
     * 
     * UserSiteRObj userSiteRobj=null; UserSiteHome userSiteHome =
     * EJBUtil.getUserSiteHome(); int output = 0; try { userSiteRobj =
     * userSiteHome.findUserSiteRight(userId,siteId); if (userSiteRobj != null) {
     * output= EJBUtil.stringToNum(userSiteRobj.getUserSiteRight()); } }
     * catch(Exception e){ Rlog.fatal("userSite","Error in getRightForUserSite
     * in UserSiteAgentBean" + e); return output; } return output; }
     */

}// end of class
