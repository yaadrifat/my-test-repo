/*
 * Classname : FldValidateAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 11/03/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: SoniaKaura
 */

package com.velos.eres.service.fldValidateAgent.impl;

/* Import Statements */

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.eres.business.fldValidate.impl.FldValidateBean;
import com.velos.eres.business.formNotify.impl.FormNotifyBean;
import com.velos.eres.service.fldValidateAgent.FldValidateAgentRObj;
import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

/**
 * The stateless session EJB acting as a facade for the entity CMP -
 * FldValidateAgentBean.<br>
 * <br>
 * 
 * @author SoniaKaura
 * @see FormNotifyBean
 * @version 1.0 11/03/2003
 * @ejbHome FldValidateAgentHome
 * @ejbRemote FldValidateAgentRObj
 */

@Stateless
public class FldValidateAgentBean implements FldValidateAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * 
     * Looks up on the FldValidate id parameter passed in and constructs and
     * returns a FldValidate state keeper. containing all the details of the
     * Form Section<br>
     * 
     * @param fldValidateId
     *            the FldValidate id
     */

    public FldValidateBean getFldValidateDetails(int fldValidateId) {

        FldValidateBean toreturn = null;

        try {

            toreturn = (FldValidateBean) em.find(FldValidateBean.class,
                    new Integer(fldValidateId));

            return toreturn;

        } catch (Exception e) {
            Rlog.fatal("fldvalidate",
                    "Error in getFldValidateDetails() in FldValidateAgentBean"
                            + e);
        }
        Rlog
                .debug(
                        "fldvalidate",
                        "Returning from FldValidateAgentBean.getFormFeildDetails fldValidateAgentBean StateKeeper "
                                + toreturn);
        return toreturn;

    }

    /**
     * Creates FldValidate record.
     * 
     * @param a
     *            State Keeper containing the FldValidate attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */

    public int setFldValidateDetails(FldValidateBean fldValidatesk) {
        try {
            Rlog.debug("fldvalidate",
                    "FldValidateAgent:inside try of setFldValidateDetails");

            FldValidateBean fb = new FldValidateBean();

            fb.updateFldValidate(fldValidatesk);
            em.persist(fb);
            return fb.getFldValidateId();
        } catch (Exception e) {
            Rlog.fatal("fldvalidate",
                    "Error in setFldValidateDetails() in FldValidateAgentBean "
                            + e);
            return -2;
        }

    }

    /**
     * Updates a FldValidate record.
     * 
     * @param a
     *            FldValidate state keeper containing FldValidate attributes to
     *            be set.
     * @return int for successful:0; e\Exception : -2
     */
    public int updateFldValidate(FldValidateBean fldValidatesk) {

        FldValidateBean retrieved = null; // Entity Bean Remote Object
        int output;

        try {
            Rlog.debug("fldvalidate",
                    "FldValidateAgent:inside try of updateFldValidate");

            retrieved = (FldValidateBean) em.find(FldValidateBean.class,
                    new Integer(fldValidatesk.getFldValidateId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateFldValidate(fldValidatesk);
            em.merge(retrieved);

        } catch (Exception e) {
            Rlog.fatal("fldvalidate",
                    "Error in updateFldValidate in FldValidateAgentBean" + e);
            return -2;
        }
        return output;
    }

    /**
     * Removes a FldValidate record.
     * 
     * @param a
     *            FldValidate Primary Key
     * @return int for successful:0; e\Exception : -2
     */

    public int removeFldValidate(int fldValidateId) {
        int output;
        FldValidateBean retrieved = null;

        try {

            retrieved = (FldValidateBean) em.find(FldValidateBean.class,
                    new Integer(fldValidateId));
            em.remove(retrieved);
            Rlog.debug("fldvalidate",
                    "Removed FldValidate row with FldValidateID = "
                            + fldValidateId);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("fldvalidate", "Exception in removeFldValidate " + e);
            return -1;

        }

    }

    /**
     * 
     * 
     * @param userlogin
     * @return FldValidateStateKeeper object with fldvalidation details.
     */

    public FldValidateBean findByFieldId(int fldlibId) {

        Rlog
                .debug("fldvalidate",
                        "inside FldValidateAgentBean findByFieldId()");
        FldValidateBean retrieved = null; // User Entity Bean Remote Object

        try {
            Query query = em.createNamedQuery("findByFieldId");
            query.setParameter("fk_fldlib", fldlibId);

            retrieved = (FldValidateBean) query.getSingleResult();
            if (retrieved == null)
                retrieved = new FldValidateBean();

        } catch (Exception e) {
            Rlog.fatal("fldvalidate",
                    "Error in findByFieldId() in FldValidateAgentBean" + e);
            e.printStackTrace();
        }
        return retrieved;
    }

}
