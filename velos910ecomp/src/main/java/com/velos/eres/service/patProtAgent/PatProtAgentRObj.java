/*
 * Classname			PatProtAgentRObj
 * 
 * Version information : 1.0  
 *
 * Date					06/16/2001
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.patProtAgent;

import javax.ejb.Remote;

import com.velos.eres.business.patProt.impl.PatProtBean;
import com.velos.esch.business.common.ScheduleDao;
import java.util.Hashtable;

/**
 * Remote interface for PatProtAgentBean session EJB
 * 
 * @author sajal
 * @version 1.0, 06/16/2001
 */
/* modified by sonia, 08/25/04, added findCurrentProtDetails */
@Remote
public interface PatProtAgentRObj {
    public PatProtBean getPatProtDetails(int patProtId);

    public int setPatProtDetails(PatProtBean ppsk);

    public int updatePatProt(PatProtBean ppsk);

    public PatProtBean findCurrentPatProtDetails(int studyId, int patientId);
    
    public ScheduleDao getAvailableSchedules(String patientId, String studyId);
    //KV:BugNo:5111
    
    public int deleteSchedules(String[] deleteIds, int userId);
    
    // deleteSchedules() Overloaded for INF-18183 ::: Raviesh
    public int deleteSchedules(String[] deleteIds, int userId,Hashtable<String, String> args);
}
