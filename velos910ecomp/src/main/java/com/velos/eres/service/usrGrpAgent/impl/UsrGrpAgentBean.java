/*
 * Classname : UsrGrpAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 03/12/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sajal
 */

package com.velos.eres.service.usrGrpAgent.impl;

/* Import Statements */
import java.util.Hashtable;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.GroupDao;
import com.velos.eres.business.usrGrp.impl.UsrGrpBean;
import com.velos.eres.service.usrGrpAgent.UsrGrpAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

/**
 * The stateless session EJB acting as a facade for the entity CMP - UsrGrpBean.<br>
 * <br>
 * 
 * @author sajal
 * @see UsrGrpBean
 * @version 1.0 03/12/2001
 * @ejbHome UsrGrpAgentHome
 * @ejbRemote UsrGrpAgentRObj
 */
@Stateless
public class UsrGrpAgentBean implements UsrGrpAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    
    @Resource
    private SessionContext context;

    /**
     * 
     */
    private static final long serialVersionUID = 3906649695735658032L;

    /**
     * 
     * Looks up on the UsrGrp id parameter passed in and constructs and returns
     * a UsrGrp state keeper. containing all the details of the usrGrp<br>
     * 
     * @param usrGrpId
     *            the UsrGrp id
     */
    public UsrGrpBean getUsrGrpDetails(int usrGrpId) {
        UsrGrpBean toreturn = null;

        try {
            /*
             * UsrGrpPK pkUsrGrp; pkUsrGrp = new UsrGrpPK(usrGrpId); UsrGrpHome
             * usrGrpHome = EJBUtil.getUsrGrpHome(); retrieved =
             * usrGrpHome.findByPrimaryKey(pkUsrGrp); toreturn =
             * retrieved.getUsrGrpStateKeeper();
             */
            return (UsrGrpBean) em
                    .find(UsrGrpBean.class, new Integer(usrGrpId));

        } catch (Exception e) {
            Rlog.fatal("usrgrp",
                    "Error in getUsrGrpDetails() in UsrGrpAgentBean" + e);
        }
        Rlog.debug("usrgrp",
                "Returning from UsrGrpAgentBean.getUsrGrpDetails UsrGrpStateKeeper "
                        + toreturn);
        return toreturn;
    }

    /**
     * Creates a UsrGrp record.
     * 
     * @param an
     *            usrGrp state keeper containing usrGrp attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */

    public int setUsrGrpDetails(UsrGrpBean ugsk) {
        try {

            em.merge(ugsk);
            return ugsk.getUsrGrpId();
        } catch (Exception e) {
            Rlog.fatal("usrgrp",
                    "Error in setUsrGrpDetails() in UsrGrpAgentBean " + e);
        }
        return 0;
    }

    /**
     * Updates a UsrGrp record.
     * 
     * @param an
     *            usrGrp state keeper containing usrGrp attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */
    public int updateUsrGrp(UsrGrpBean ugsk) {

        UsrGrpBean usrGrpBean = null;
        int output;

        try {

            usrGrpBean = (UsrGrpBean) em.find(UsrGrpBean.class, new Integer(
                    ugsk.getUsrGrpId()));

            if (usrGrpBean == null) {
                return -2;
            }
            output = usrGrpBean.updateUsrGrp(ugsk);

        } catch (Exception e) {
            Rlog
                    .debug("usrgrp", "Error in updateUsrGrp in UsrGrpAgentBean"
                            + e);
            return -2;
        }
        return output;
    }

    public int removeUsrFromGrp(int usrGrpId) {
        int output;

        GroupDao gdao = new GroupDao();
        UsrGrpBean usrGrpBean = null;
        int success = 0;

        try {

            usrGrpBean = (UsrGrpBean) em.find(UsrGrpBean.class, new Integer(
                    usrGrpId));
            // call groupDao.revokeSupUserRightsFromUser to revoke super user
            // rights
            
            /*success = gdao.revokeSupUserRightsFromUser(usrGrpBean
                    .getUsrGrpUserId(), usrGrpBean.getUsrGrpGroupId(),
                    usrGrpBean.getCreator(), usrGrpBean.getIpAdd()); */

            em.remove(usrGrpBean);
            success = 0;
            return success;
        } catch (Exception e) {
            Rlog.debug("usrGrp", "Exception in removeUsrFromGrp " + e);
            return -1;

        }

    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int removeUsrFromGrp(int usrGrpId,Hashtable<String, String> auditInfo){
       
    	UsrGrpBean usrGrpBean = null;
    	AuditBean audit=null;
        int success = 0;

        try {
        	String currdate =DateUtil.getCurrentDate();
        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE);// Fetches the APP_Module from the Hashtable
            String rid= AuditUtils.getRidValue("ER_USRGRP","eres","PK_USRGRP="+usrGrpId); //Fetches the RID
            
            //POPULATE THE AUDIT BEAN 
            audit = new AuditBean("ER_USRGRP",String.valueOf(usrGrpId),rid,userID,currdate,app_module,ipAdd,reason);
            
            usrGrpBean = (UsrGrpBean) em.find(UsrGrpBean.class, new Integer(
                    usrGrpId));
           
            em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
            em.remove(usrGrpBean);
            success = 0;
            return success;
            
        }catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.debug("usrGrp", "Exception in removeUsrFromGrp(int usrGrpId,Hashtable<String, String> auditInfo) " + e);
            return -1;
        }
    }

    

}// end of class

