package com.velos.eres.service.reviewBoardAgent;

import javax.ejb.Remote;

import com.velos.eres.business.reviewboard.impl.ReviewBoardBean;
import com.velos.eres.service.reviewBoardAgent.impl.ReviewBoardAgentBean;

/**
 * Remote interface for ReviewBoardAgent session EJB
 * 
 * @author amit srivastava
 * @version 1.0 01/26/2013
 * @see ReviewBoardAgentBean
 */

@Remote
public interface ReviewBoardAgentRObj {

	public ReviewBoardBean getReviewBoardAllDetails();
	
	public ReviewBoardBean getReviewBoardDetails(int reviewBoardId);
	
	public ReviewBoardBean getReviewBoardNames(int rbId);	
	
	public int setReviewBoardDetails(ReviewBoardBean reviewBoard);

	public int updateReviewBoard(ReviewBoardBean rsk);
	
	public void getReviewBoardValues();
	
	public void getReviewBoardAllValues();	

	public int getRBNameCount(String rbName);
}

