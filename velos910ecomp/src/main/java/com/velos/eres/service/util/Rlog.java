/*
 * Classname			Rlog
 * 
 * Version information  1.0
 *
 * Date					03/07/2001
 * 
 * Copyright notice		Velos Inc.
 * 
 * Author 				Dinesh
 */

package com.velos.eres.service.util;

import org.apache.log4j.Category;
import org.apache.log4j.Priority;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * This class is just a helper class to make it handy to print,store,log,mail
 * debug statements This class implements log 4j which is an effective way of
 * tracking the debug messages to effectively track the errors.Log4j is a full
 * fleged logging service in itself
 * 
 * @author Dinesh
 * @version 1.0 03/07/2001
 */
public final class Rlog {

    public static Category cat = null;

    public static String logFile = null;

    public static String eHome = null;

    public static void init() { // to be initinalized in the login screen
         
        DOMConfigurator.configure(getLogFile());
    }

    public static void debug(String module, String msg) {
        if (cat == null) {
            DOMConfigurator.configure(getLogFile());
        }
        cat = Category.getInstance(module);
        cat.log("com.velos.eres.service.util.Rlog", Priority.DEBUG, msg, null);
    }

    public static void info(String module, String msg) {
        if (cat == null) {
            DOMConfigurator.configure(getLogFile());
        }
        cat = Category.getInstance(module);
        cat.log("com.velos.eres.service.util.Rlog", Priority.INFO, msg, null);
    }

    public static void warn(String module, String msg) {
        if (cat == null) {
            DOMConfigurator.configure(getLogFile());
        }
        cat = Category.getInstance(module);
        cat.log("com.velos.eres.service.util.Rlog", Priority.WARN, msg, null);

    }

    public static void error(String module, String msg) {
        if (cat == null) {
            DOMConfigurator.configure(getLogFile());
        }
        cat = Category.getInstance(module);
        cat.log("com.velos.eres.service.util.Rlog", Priority.ERROR, msg, null);
    }

    public static void fatal(String module, String msg) {
        if (cat == null) {
            DOMConfigurator.configure(getLogFile());
        }
        cat = Category.getInstance(module);
        cat.log("com.velos.eres.service.util.Rlog", Priority.FATAL, msg, null);
    }

    public static String getLogFile() {
        if (logFile == null) {
            try {
                eHome = EnvUtil.getEnvVariable("ERES_HOME");
                eHome = (eHome == null) ? "" : eHome;
                if (eHome.trim().equals("%ERES_HOME%"))
                    eHome = System.getProperty("ERES_HOME");
                eHome = eHome.trim();
                logFile = eHome + "log4j.xml";
                System.out.println("Name of the LogFile XML" + logFile);
            } catch (Exception e) {
                System.out.println("Exception in reading eres home for log4j"
                        + e);
            }
        }
        return logFile;
    }
}
// end of class here
