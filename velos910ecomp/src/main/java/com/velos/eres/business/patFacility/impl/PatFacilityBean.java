/*
 * Classname			PatFacilityBean
 * 
 * Version information : 1.0
 *
 * Date					09/22/2005
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.business.patFacility.impl;

/* IMPORT STATEMENTS */

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* END OF IMPORT STATEMENTS */

/**
 * The Patient Facility POJO.<br>
 * <br>
 * 
 * @author Sonia Abrol
 * 
 * @version 1.0, 09/22/2005
 */
@Entity
@Table(name = "er_patfacility")
@NamedQuery(name = "findPreviousDefaultSite", query = "SELECT OBJECT(pf) FROM PatFacilityBean pf where pf.patientPK  = :patientPk AND pf.isDefault = 1")
public class PatFacilityBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5502734977417416476L;

	/**
	 * the Patient Facility Primary Key
	 */
	public int id;

	/**
	 * the Patient Facility Id
	 */
	public String patientFacilityId;

	/**
	 * the patient Primary Key
	 */
	public String patientPK;

	/**
	 * the patient Site
	 */
	public String patientSite;

	/**
	 * the registration date
	 */
	public Date regDate;

	/**
	 * the id of the user who registered the patient
	 */
	public String registeredBy;

	/**
	 * the person who registered the patient (and is not a user in the system)
	 */
	public String registeredByOther;

	/**
	 * a comma separated string of specialty ids that have access to the
	 * patient. <br>
	 * If no specialty is selected, that means all the users of the registering
	 * facility can access the patient
	 */
	public String patSpecialtylAccess;

	/**
	 * Flag to indicate the access level of the registering site's users <br>
	 * Currently we support only two levels: <br>
	 * 0 : No Access <br>
	 * 7 : Full Access
	 */
	public String patAccessFlag;

	/**
	 * Flag to indicate if this site is the default site for the patient <br>
	 * Possible Values: <br>
	 * 0 : Not Default <br>
	 * 1 : Default
	 */
	public String isDefault;

	/**
	 * the id of the user who created the record
	 */
	public String creator;

	/**
	 * Id of the user who last modified the record
	 */
	public String lastModifiedBy;
	
	

	/**
	 * the IP address of the user who created or modified the record
	 */
	public String iPAdd;
	
	/**
	 * Flag to indicate if this is a read only record <br>
	 * Possible Values: <br>
	 * 0 : Not Readonly<br>
	 * 1 : ReadOnly
	 */
	public String isReadOnly;

	// GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
	@SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_PATFACILITY", allocationSize=1)
	@Column(name = "PK_PATFACILITY")
	public int getId() {
		return this.id;
	}

	public void setId(int Id) {
		this.id =Id;
	}

	@Column(name = "FK_PER")
	public String getPatientPK() {
		return this.patientPK;

	}

	public void setPatientPK(String patPK) {
		this.patientPK = patPK;

	}

	/**
	 * Returns the value of isDefault.
	 */
	@Column(name = "PATFACILITY_DEFAULT")
	public String getIsDefault() {
		return isDefault;
	}

	/**
	 * Sets the value of isDefault.
	 * 
	 * @param isDefault
	 *            The value to assign isDefault.
	 */
	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}

	/**
	 * Returns the value of patAccessFlag.
	 */
	@Column(name = "PATFACILITY_ACCESSRIGHT")
	public String getPatAccessFlag() {
		return patAccessFlag;
	}

	/**
	 * Sets the value of patAccessFlag.
	 * 
	 * @param patAccessFlag
	 *            The value to assign patAccessFlag.
	 */
	public void setPatAccessFlag(String patAccessFlag) {
		this.patAccessFlag = patAccessFlag;
	}

	/**
	 * Returns the value of patSpecialtylAccess.
	 */
	@Column(name = "PATFACILITY_SPLACCESS")
	public String getPatSpecialtylAccess() {
		return patSpecialtylAccess;
	}

	/**
	 * Sets the value of patSpecialtylAccess.
	 * 
	 * @param patSpecialtylAccess
	 *            The value to assign patSpecialtylAccess.
	 */
	public void setPatSpecialtylAccess(String patSpecialtylAccess) {
		this.patSpecialtylAccess = patSpecialtylAccess;
	}

	/**
	 * Returns the value of registeredByOther.
	 */
	@Column(name = "PATFACILITY_OTHERPROVIDER")
	public String getRegisteredByOther() {
		return registeredByOther;
	}

	/**
	 * Sets the value of registeredByOther.
	 * 
	 * @param registeredByOther
	 *            The value to assign registeredByOther.
	 */
	public void setRegisteredByOther(String registeredByOther) {
		this.registeredByOther = registeredByOther;
	}

	/**
	 * Returns the value of registeredBy.
	 */
	@Column(name = "PATFACILITY_PROVIDER")
	public String getRegisteredBy() {
		return registeredBy;
	}

	/**
	 * Sets the value of registeredBy.
	 * 
	 * @param registeredBy
	 *            The value to assign registeredBy.
	 */
	public void setRegisteredBy(String registeredBy) {
		String reg ="";
		reg = registeredBy;
		
		if (StringUtil.isEmpty(reg))
		{
			reg = "";
		}
		Rlog.debug("PatFacility", "PatFacilityBean.setRegisteredBy:" + reg);
			this.registeredBy = reg;
			Rlog.debug("PatFacility", "After PatFacilityBean.setRegisteredBy:");
			
	}

	/**
	 * Returns the value of regDate.
	 */
	@Column(name = "PATFACILITY_REGDATE")
	public Date getRegDate() {
		return regDate;
	}

	/**
	 * Sets the value of regDate.
	 * 
	 * @param regDate
	 *            The value to assign regDate.
	 */
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	/**
	 * Sets the value of regDate (in string format).
	 * 
	 * @param regDate
	 *            The value to assign regDate.
	 */
	public void updateRegDate(String strRegDate) {
		setRegDate(DateUtil.stringToDate(strRegDate, null));
	}

	/**
	 * Returns the value of regDate in String format
	 */
	@Transient
	public String returnRegDate() {
		return DateUtil.dateToString(getRegDate());

	}

	/**
	 * Returns the value of patientSite.
	 */
	@Column(name = "FK_SITE")
	public String getPatientSite() {
		return patientSite;
	}

	/**
	 * Sets the value of patientSite.
	 * 
	 * @param patientSite
	 *            The value to assign patientSite.
	 */
	public void setPatientSite(String patientSite) {
		this.patientSite = patientSite;
	}

	@Column(name = "CREATOR")
	public String getCreator() {
		return this.creator;

	}

	public void setCreator(String creator) {
		this.creator = creator;

	}

	@Column(name = "LAST_MODIFIED_BY")
	public String getLastModifiedBy() {
		return this.lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	@Column(name = "IP_ADD")
	public String getIPAdd() {
		return this.iPAdd;
	}

	public void setIPAdd(String iPAdd) {
		this.iPAdd = iPAdd;
	}

	/**
	 * Returns the value of patientFacilityId.
	 */
	@Column(name = "PAT_FACILITYID")
	public String getPatientFacilityId() {
		return patientFacilityId;
	}

	/**
	 * Sets the value of patientFacilityId.
	 * 
	 * @param patientFacilityId
	 *            The value to assign patientFacilityId.
	 */
	public void setPatientFacilityId(String patientFacilityId) {
		this.patientFacilityId = patientFacilityId;
	}

	// END OF GETTER SETTER METHODS

	/**
	 * Updates the attributes of current insatance with the attributes of the
	 * one passed as argument
	 * 
	 * @param pBean
	 *            PatFacilityBean with the Patient Facility Information
	 * 
	 * @return 0 - if update is successful <br>
	 *         -2 - if update fails
	 * 
	 */
	public int updatePatFacility(PatFacilityBean pBean) {
		try {
			setPatientPK(pBean.getPatientPK());
			setIsDefault(pBean.getIsDefault());
			setPatAccessFlag(pBean.getPatAccessFlag());
			setPatSpecialtylAccess(pBean.getPatSpecialtylAccess());
			setRegisteredByOther(pBean.getRegisteredByOther());
			setRegisteredBy(pBean.getRegisteredBy());
			updateRegDate(pBean.returnRegDate());
			setPatientSite(pBean.getPatientSite());
			setPatientFacilityId(pBean.getPatientFacilityId());
			setCreator(pBean.getCreator());
			setLastModifiedBy(pBean.getLastModifiedBy());
			setIPAdd(pBean.getIPAdd());
			setIsReadOnly(pBean.getIsReadOnly());
			Rlog.debug("PatFacility", "PatFacilityBean.updatPatFacility");
			return 0;
		} catch (Exception e) {
			Rlog.fatal("PatFacility",
					"Exception in PatFacilityBean.updatPatFacility " + e);
			return -2;
		}
	}

	/** Default constructor */
	public PatFacilityBean() {

	}

	/** Full Argument constructor */
	public PatFacilityBean(int id, String patientFacilityId, String patientPK,
			String patientSite, String regDate, String registeredBy,
			String registeredByOther, String patSpecialtylAccess,
			String patAccessFlag, String isDefault, String creator, String lastModifiedBy,
			String iPAdd, String isReadOnly) {
		super();
		setId(id);
		setPatientFacilityId(patientFacilityId);
		setPatientPK(patientPK);
		setPatientSite(patientSite);
		updateRegDate(regDate);
		setRegisteredBy(registeredBy);
		setRegisteredByOther(registeredByOther);
		setPatSpecialtylAccess(patSpecialtylAccess);
		setPatAccessFlag(patAccessFlag);
		setIsDefault(isDefault);
		setCreator(creator);
		setLastModifiedBy(lastModifiedBy);
		setIPAdd(iPAdd);
		setIsReadOnly(isReadOnly);

	}

	@Column(name = "IS_READONLY")
	public String getIsReadOnly() {
		return isReadOnly;
	}

	public void setIsReadOnly(String isReadOnly) {
		this.isReadOnly = isReadOnly;
	}

}// end of class
