package com.velos.eres.business.common;

import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.velos.eres.service.util.Rlog;
  
public class ReportDaoNew extends CommonDAO implements java.io.Serializable {

    // For XSL Data
    private ArrayList xslAccIds;

    private ArrayList xslRepIds;

    private ArrayList xsls;

    private ArrayList xslDescs;

    private ArrayList xslPks;

    private ArrayList xslNames;

    // for XML Data
    private ArrayList repXml;

    // for report browser Data
    private ArrayList pkReps;

    private ArrayList fkAccs;

    private ArrayList repName;

    private ArrayList repDesc;

    private ArrayList repColumns;

    private ArrayList repFilters;
    
    private ArrayList repFilterColumns;
    private ArrayList repMapFilterColumns;
    private ArrayList repFilterDisplayNames;
    private ArrayList repFilterMandatoryList;
    private ArrayList repFilterDependOnList;
    //for filter columns
    private ArrayList repFilterKeywords;
    
    private ArrayList repFilterApplicable;
    
    private ArrayList repFilterDispDiv;
    
    //for a report from er_report
    private ArrayList reportFilterKeywords;

    // for header and footer
    private byte[] hdrFile;

    private byte[] ftrFile;
    
    private Object[] filterKey;
    private Object[] filterValue;

    private int rows;

    // get,set methods for XSLs
    public ArrayList getRepMapFilterColumns() {
        return repMapFilterColumns;
    }
    public void setRepMapFilterColumns(ArrayList repMapFilterColumns) {
        this.repMapFilterColumns = repMapFilterColumns;
    }
    public void setRepMapFilterColumns(String repMapFilterColumn) {
        this.repMapFilterColumns.add(repMapFilterColumn);
    }
    public ArrayList getRepFilterDependOnList() {
        return repFilterDependOnList;
    }

    public void setRepFilterDependOnList(ArrayList repFilterDependOnList) {
        this.repFilterDependOnList = repFilterDependOnList;
    }
    
    public void setRepFilterDependOn(String repFilterDependOnList) {
        this.repFilterDependOnList.add(repFilterDependOnList);
    }

    public ArrayList getRepFilterMandatoryList() {
        return repFilterMandatoryList;
    }

    public void setRepFilterMandatoryList(ArrayList repFilterMandatoryList) {
        this.repFilterMandatoryList = repFilterMandatoryList;
    }
    public void setRepFilterMandatory(String repFilterMandatoryList) {
        this.repFilterMandatoryList.add(repFilterMandatoryList);
    }

    public ArrayList getRepFilterColumns() {
        return repFilterColumns;
    }

    public void setRepFilterColumns(ArrayList repFilterColumns) {
        this.repFilterColumns = repFilterColumns;
    }
    public void setRepFilterColumns(String repFilterColumn) {
        this.repFilterColumns.add(repFilterColumn);
    }

    public ArrayList getRepFilterDisplayNames() {
        return repFilterDisplayNames;
    }
    public void setRepFilterDisplayNames(String repFilterDisplayName) {
        this.repFilterDisplayNames.add(repFilterDisplayName);
    }
    public void setRepFilterDisplayName(ArrayList repFilterDisplayNames) {
        this.repFilterDisplayNames = repFilterDisplayNames;
    }
    public ArrayList getReportFilterKeywords() {
        return reportFilterKeywords;
    }

    public void setReportFilterKeywords(ArrayList reportFilterKeywords) {
        this.reportFilterKeywords = reportFilterKeywords;
    }
    public void setReportFilterKeywords(String repFilterKeyword) {
        this.reportFilterKeywords.add(repFilterKeyword);
    }
    
    public ArrayList getRepFilterKeywords() {
        return repFilterKeywords;
    }

    public void setRepFilterKeywords(ArrayList repFilterKeywords) {
        this.repFilterKeywords = repFilterKeywords;
    }
    
    public void setRepFilterKeywords(String repFilterKeyword) {
        this.repFilterKeywords.add(repFilterKeyword);
    }
	/**
	 * @return the repFilterDispDiv
	 */
	public ArrayList getRepFilterDispDiv() {
		return repFilterDispDiv;
	}
	/**
	 * @param repFilterDispDiv the repFilterDispDiv to set
	 */
	public void setRepFilterDispDiv(ArrayList repFilterDispDiv) {
		this.repFilterDispDiv = repFilterDispDiv;
	}
	
	public void setRepFilterDispDiv(String repFilterDisp) {
		this.repFilterDispDiv.add(repFilterDisp);
	}
	
	/**
	 * @return the repFilterApplicable
	 */
	public ArrayList getRepFilterApplicable() {
		return repFilterApplicable;
	}
	/**
	 * @param repFilterApplicable the repFilterApplicable to set
	 */
	public void setRepFilterApplicable(ArrayList repFilterApplicable) {
		this.repFilterApplicable = repFilterApplicable;
	}
	
	public void setRepFilterApplicable(String repFilterApplicable) {
		this.repFilterApplicable.add(repFilterApplicable);
	}

    public Object[] getFilterKey() {
        return filterKey;
    }

    public void setFilterKey(Object[] filterKey) {
        this.filterKey = filterKey;
    }

    public Object[] getFilterValue() {
        return filterValue;
    }

    public void setFilterValue(Object[] filterValue) {
        this.filterValue = filterValue;
    }

    public ArrayList getXslAccIds() {
        return this.xslAccIds;
    }

    public void setXslAccIds(String xslAccId) {
        xslAccIds.add(xslAccId);
    }

    public ArrayList getXslDescs() {
        return this.xslDescs;
    }

    public void setXslDescs(String xslDesc) {
        xslDescs.add(xslDesc);
    }

    public ArrayList getXslNames() {
        return this.xslNames;
    }

    public void setXslNames(String xslName) {
        xslNames.add(xslName);
    }

    public ArrayList getXslPks() {
        return this.xslPks;
    }

    public void setXslPks(Integer xslPk) {
        xslPks.add(xslPk);
    }

    public ArrayList getXslRepIds() {
        return this.xslRepIds;
    }

    public void setXslRepIds(String xslRepId) {
        xslRepIds.add(xslRepIds);
    }

    public ArrayList getXsls() {
        return this.xsls;
    }

    public void setXsls(String xsl) {
        xsls.add(xsl);
    }

    // get, set methods for XML
    public ArrayList getRepXml() {
        return this.repXml;
    }

    public void setRepXml(String Xml) {
        repXml.add(Xml);
    }

    // get, set methods for report browser
    public ArrayList getFkAccount() {
        return this.fkAccs;
    }

    public void setFkAccount(String fkAccnt) {
        fkAccs.add(fkAccnt);
    }

    public ArrayList getPkReport() {
        return this.pkReps;
    }

    public void setPkReport(Integer pkRep) {
        pkReps.add(pkRep);
    }

    public ArrayList getRepDesc() {
        return this.repDesc;
    }

    public void setRepDesc(String repDes) {
        repDesc.add(repDes);
    }

    public ArrayList getRepName() {
        return this.repName;
    }

    public void setRepName(String repNam) {
        repName.add(repNam);
    }

    public ArrayList getRepColumns() {
        return this.repColumns;
    }

    public void setRepColumns(String repColumn) {
        repColumns.add(repColumn);
    }

    public ArrayList getRepFilters() {
        return this.repFilters;
    }

    public void setRepFilters(String repFilter) {
        repFilters.add(repFilter);
    }

    public byte[] getFtrFile() {
        return this.ftrFile;
    }

    public void setFtrFile(byte[] ftrFile) {
        this.ftrFile = ftrFile;
    }

    public byte[] getHdrFile() {
        return this.hdrFile;
    }

    public void setHdrFile(byte[] hdrFile) {
        this.hdrFile = hdrFile;
    }

    public int getRows() {
        return this.rows;
    }

    public void setRows(int i) {
        this.rows = i;
    }
    

    // constructor
    public ReportDaoNew() {
        // Initialize XSL datakeppers
        xslAccIds = new ArrayList();
        xslRepIds = new ArrayList();
        xsls = new ArrayList();
        xslDescs = new ArrayList();
        xslPks = new ArrayList();
        xslNames = new ArrayList();
        // Initialize XML datakeepers
        repXml = new ArrayList();
        // Initialize report list keepers
        pkReps = new ArrayList();
        fkAccs = new ArrayList();
        repName = new ArrayList();
        repDesc = new ArrayList();
        repColumns = new ArrayList();
        repFilters = new ArrayList();
        
        repFilterColumns=new ArrayList();
        repMapFilterColumns = new ArrayList();
        repFilterDisplayNames=new ArrayList();
        repFilterKeywords=new ArrayList();
        repFilterMandatoryList=new ArrayList();
        repFilterDependOnList=new ArrayList();
        reportFilterKeywords=new ArrayList();
        
        this.repFilterApplicable=new ArrayList();
        
        this.repFilterDispDiv=new ArrayList();

        // intialize report hdrs and ftrs
        hdrFile = null;
        ftrFile = null;
        filterKey=null;
        filterValue=null;
        
    }

    // Get XMLs for a report id
    public void getXMLFromSProc(int reportId, int accountId, String params) {
        CallableStatement cstmt = null;
        PreparedStatement pstmt = null;
        int count = 0;
        Connection conn = null;
        Clob XmlClob = null;
        String XmlString = "";
        int counter = 0;
        int i = 0;

        // Rlog.debug("Report","In getXMLFromSProc in ReportDao line number 1");
        try {
            conn = getConnection();
            Rlog.debug("Report", "" + reportId);

            cstmt = conn.prepareCall("{call sp_genxml(?,?,?,?,?,?,?,?,?,?,?)}");
            // cstmt = conn.prepareCall("{call sp_genxml1(?,?,?,?)}");
            Rlog.debug("Report",
                    "In getXMLFromSProc in ReportDao line number 2");
            cstmt.setInt(1, reportId);
            // Rlog.debug("Report","In getXMLFromSProc in ReportDao line number
            // 3");
            cstmt.setInt(2, accountId);
            // Rlog.debug("Report","In getXMLFromSProc in ReportDao line number
            // 4");
            cstmt.setString(3, params);

            counter = 4;
            for (count = 0; count < 8; count++) {

                cstmt.registerOutParameter(counter, java.sql.Types.CLOB);
                counter++;
            }

            Rlog.debug("Report",
                    "In getXMLFromSProc in ReportDaoNew line number 5");
            cstmt.execute();
            Rlog.debug("Report",
                    "In getXMLFromSProc in ReportDaoNew line number 6");
            counter = 4;
            for (count = 0; count < 8; count++) {

                XmlClob = cstmt.getClob(counter);
                if (!(XmlClob == null)) {

                    XmlString = XmlClob.getSubString(1, (int) XmlClob.length());
                    this.setRepXml(XmlString);
                }
                counter++;
            }

            Rlog.debug("Report",
                    "In getXMLFromSProc in ReportDaoNew line number 7");

        } catch (Exception ex) {
            Rlog.fatal("Report", "ReportDaoNew.getXMLFromSProc EXCEPTION" + ex);
            ex.printStackTrace();
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public int getRepXsl(int xslId) {

        // get the XSL associated for reportId
        int rows = 0;
        Clob XslClob = null;
        String XslStr = "";
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            // Assuming that one record per report will be there.
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("Select repxsl from er_repxsl where pk_repxsl='"
                            + xslId + "'");

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                XslClob = rs.getClob(1);
                if (!(XslClob == null)) {

                    XslStr = XslClob.getSubString(1, (int) XslClob.length());
                    this.setXsls(XslStr);
                    rows++;
                }

            }

            return rows;
        } catch (SQLException ex) {
            Rlog.fatal("Report",
                    "ReportDaoNew.getRepXsl EXCEPTION IN FETCHING FROM xsl table"
                            + ex);
            return -1;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    public int getReportXsl(int xslId) {

        // get the XSL associated for reportId
        int rows = 0;
        Clob XslClob = null;
        String XslStr = "";
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            // Assuming that one record per report will be there.
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("Select repxsl_xsl from er_repxsl where pk_repxsl='"
                            + xslId + "'");

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                XslClob = rs.getClob(1);
                if (!(XslClob == null)) {

                    XslStr = XslClob.getSubString(1, (int) XslClob.length());
                    this.setXsls(XslStr);
                    rows++;
                }

            }

            return rows;
        } catch (SQLException ex) {
            Rlog.fatal("Report",
                    "ReportDaoNew.getRepXsl EXCEPTION IN FETCHING FROM xsl table"
                            + ex);
            return -1;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public int getFoXsl(int xslId) {

        // get the XSL associated for reportId
        int rows = 0;
        Clob XslClob = null;
        String XslStr = "";
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            // Assuming that one record per report will be there.
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("Select repxsl_fo from er_repxsl where pk_repxsl='"
                            + xslId + "'");

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                XslClob = rs.getClob(1);
                if (!(XslClob == null)) {

                    XslStr = XslClob.getSubString(1, (int) XslClob.length());
                    this.setXsls(XslStr);
                    rows++;
                }

            }

            return rows;
        } catch (SQLException ex) {
            Rlog.fatal("Report",
                    "ReportDaoNew.getFoXsl EXCEPTION IN FETCHING FROM xsl table"
                            + ex);
            return -1;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    public int getRepXslList(int reportId) {

        // get the XSL associated for reportId
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("Select pk_repxsl,fk_account, repxsl_name, repxsl_desc from er_repxsl where fk_report='"
                            + reportId + "'");

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setXslPks(new Integer(rs.getInt("pk_repxsl")));
                setXslAccIds(rs.getString("fk_account"));
                setXslRepIds(rs.getString("fk_report"));
                setXslNames(rs.getString("repxsl_name"));
                setXslDescs(rs.getString("repxsl_desc"));

                rows++;
                Rlog.debug("Reprot", "ReportDaoNew.getRepXslList get row# "
                        + rows);

            }
            return rows;

        } catch (SQLException ex) {
            Rlog.fatal("Report",
                    "ReportDaoNew.getRepXslList EXCEPTION IN FETCHING FROM xsl table"
                            + ex);
            return -1;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getAllReports(int accountId) {

        // get the XSL associated for reportId
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            // Assuming that one record per report will be there.
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("Select pk_report, fk_account, rep_name, rep_desc from er_report where (fk_account='"
                            + accountId
                            + "' or fk_account='0')  and rep_hide ='N'");

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setPkReport(new Integer(rs.getInt("pk_report")));
                setFkAccount(rs.getString("fk_account"));
                setRepName(rs.getString("rep_name"));
                setRepDesc(rs.getString("rep_desc"));

                rows++;
                // Rlog.debug("Reprot","ReportDao.getReports row# " + rows);

            }

        } catch (SQLException ex) {
            Rlog.fatal("Report",
                    "ReportDaoNew.getReports EXCEPTION IN FETCHING FROM report table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getAllRep(int catId, int accountId) {

        // get the XSL associated for reportId
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            // Assuming that one record per report will be there.
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("Select pk_report, fk_account, rep_name, rep_desc, rep_columns, rep_filterby from er_report where FK_CODELST_TYPE='"
                            + catId
                            + "' and (fk_account='"
                            + accountId
                            + "' or fk_account='0') and rep_hide ='N'");

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setPkReport(new Integer(rs.getInt("pk_report")));
                setFkAccount(rs.getString("fk_account"));
                setRepName(rs.getString("rep_name"));
                setRepDesc(rs.getString("rep_desc"));
                setRepColumns(rs.getString("rep_columns"));
                setRepFilters(rs.getString("rep_filterby"));

                rows++;
                // Rlog.debug("Reprot","ReportDao.getReports row# " + rows);

            }

        } catch (SQLException ex) {
            Rlog.fatal("Report",
                    "ReportDaoNew.getReports EXCEPTION IN FETCHING FROM report table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getReport(int accountId, int reportId) {

        // get the XSL associated for reportId
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            // Assuming that one record per report will be there.
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("Select PK_REPORT, FK_ACCOUNT, REP_NAME, REP_DESC, REP_COLUMNS, REP_FILTERBY, REP_FILTERKEYWORD "
                    		+ "from er_report where (fk_account='" + accountId + "' or fk_account='0') "
                    		+ "and pk_report = "+reportId +" and rep_hide ='N'");

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setPkReport(new Integer(rs.getInt("PK_REPORT")));
                setFkAccount(rs.getString("FK_ACCOUNT"));
                setRepName(rs.getString("REP_NAME"));
                setRepDesc(rs.getString("REP_DESC"));
                setRepColumns(rs.getString("REP_COLUMNS"));
                setRepFilterKeywords(rs.getString("REP_FILTERKEYWORD"));
                rows++;
            }

        } catch (SQLException ex) {
            Rlog.fatal("Report",
                    "ReportDaoNew.getReport EXCEPTION IN FETCHING FROM report table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    public void getReports(String repType, int accountId) {

        
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            // Assuming that one record per report will be there.
            conn = getConnection();
            String sql="Select pk_report, fk_account, rep_name, rep_desc, rep_columns, rep_filterby,rep_filterkeyword,rep_filterapplicable from er_report where REP_TYPE='"
                + repType
                + "' and (fk_account='"
                + accountId
                + "' or fk_account='0') and rep_hide ='N'";
            pstmt = conn
                    .prepareStatement(sql);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setPkReport(new Integer(rs.getInt("pk_report")));
                setFkAccount(rs.getString("fk_account"));
                setRepName(rs.getString("rep_name"));
                setRepDesc(rs.getString("rep_desc"));
                setRepColumns(rs.getString("rep_columns"));
                setRepFilters(rs.getString("rep_filterby"));
                setReportFilterKeywords(rs.getString("rep_filterkeyword"));
                this.setRepFilterApplicable((rs.getString("rep_filterapplicable")));
                rows++;
                // Rlog.debug("Reprot","ReportDao.getReports row# " + rows);

            }

        } catch (SQLException ex) {
            Rlog.fatal("Report",
                    "ReportDaoNew.getReports EXCEPTION IN FETCHING FROM report table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    

    public int getRepHdFtr(int repId, int accId, int userId) {

        // get the XSL associated for reportId
        int rows = 0;
        Blob hdrBlob = null;
        Blob ftrBlob = null;
        PreparedStatement pstmt = null;
        Connection conn = null;
        boolean rec;
        try {
            // Assuming that one record per report will be there.,repdtl_hdr,
            // repdtl_ftr
            conn = getConnection();
            byte[] hdr = null;
            byte[] ftr = null;
            String str1 = "select 1 no,  pk_repdtl, repdtl_hdr, repdtl_ftr from er_rephftr where fk_report='"
                    + repId + "' and fk_user='" + userId + "'";
            String str2 = "select 2 no,  pk_repdtl, repdtl_hdr, repdtl_ftr from er_rephftr where fk_report='"
                    + repId
                    + "' and  FK_USER  is null and fk_account ='"
                    + accId + "'";
            String str3 = "select 3 no,  pk_repdtl, repdtl_hdr, repdtl_ftr from er_rephftr where FK_USER  is null and fk_account ='"
                    + accId + "'";
            String str4 = "select 4 no,  pk_repdtl, repdtl_hdr, repdtl_ftr from er_rephftr where fk_report='"
                    + repId
                    + "' and FK_USER  is null and fk_account is null order by 1";
            String str = str1 + " union all " + str2 + " union all " + str3
                    + " union all " + str4;
            pstmt = conn.prepareStatement(str);
            Rlog.debug("report", str);

            ResultSet rs = pstmt.executeQuery();
            rec = rs.next();// only taking one record
            if (rec == true) {
                Rlog.debug("Report", "inside if");
                hdrBlob = (Blob) rs.getObject(3);
                ftrBlob = (Blob) rs.getObject(4);

                if (!(hdrBlob == null)) {
                    Rlog.debug("Report", "hdr not null");

                    int len = (int) hdrBlob.length();
                    byte[] byteArray = hdrBlob.getBytes(1, len);
                    Rlog.debug("Report", "after byte read ");
                    this.setHdrFile(byteArray);
                    this.setRows(1);
                } else {
                    Rlog.debug("Report", "header not found");
                }

                if (!(ftrBlob == null)) {
                    int len1 = (int) ftrBlob.length();
                    byte[] byteArray1 = ftrBlob.getBytes(1, len1);
                    Rlog.debug("Report", "after byte read ");

                    this.setFtrFile(byteArray1);
                    this.setRows(1);
                } else {
                    Rlog.debug("Report", "footer not found");
                    this.setRows(0);
                }
                return 1;
            } else {
                Rlog.debug("Report",
                        "No record found for this report in hdr/ftr table.");
                return 0;
            }

        } catch (SQLException ex) {
            Rlog.fatal("Report",
                    "ReportDaoNew.getRepXsl EXCEPTION IN FETCHING FROM xsl table"
                            + ex);
            this.setRows(-1);
            return -1;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * @param studyId
     *            study Id for which the milestone report is to be generated
     * @param orgId
     * @param startDate
     * @param endDate
     * @param Interval
     *            Type - M(Month), Q(Quarter), Y(Year) Get XML from stored
     *            procedure for milestone statistical reports
     */

    public void getMileXmlFromFinSProc(int studyId, int orgId,
            String startDate, String endDate, String intervalType) {
        CallableStatement cstmt = null;
        int count = 0;
        Connection conn = null;
        Clob XmlClob = null;
        String XmlString = "";
        int counter = 6;

        Rlog.debug("Report",
                "In getMileXmlFromFinSProc in ReportDao line number 1");
        try {
            conn = getConnection();
            Rlog.debug("Report", "Study Id for Mile report " + studyId);

            cstmt = conn
                    .prepareCall("call pkg_milestone_reports.sp_ms_financial(?,?,?,?,?,?)");

            cstmt.setInt(1, studyId);
            cstmt.setInt(2, orgId);
            cstmt.setString(3, startDate);
            cstmt.setString(4, endDate);
            cstmt.setString(5, intervalType);

            cstmt.registerOutParameter(counter, java.sql.Types.CLOB);

            cstmt.execute();

            XmlClob = cstmt.getClob(counter);
            Rlog.debug("Report",
                    "ReportDaoNew.getMileXmlFromFinSProc line x XmlString "
                            + XmlString);
            if (!(XmlClob == null)) {
                Rlog.debug("Report", "In getMileXmlFromFinSProc in XmlClob");
                XmlString = XmlClob.getSubString(1, (int) XmlClob.length());
                this.setRepXml(XmlString);
            }

            Rlog.debug("Report",
                    "ReportDaoNew.getMileXmlFromFinSProc line y XmlString "
                            + XmlString);

            Rlog.debug("Report",
                    "In getMileXmlFromFinSProc in ReportDaoNew line number 7");

        } catch (SQLException ex) {
            Rlog.fatal("Report",
                    "ReportDaoNew.getMileXmlFromFinSProc EXCEPTION" + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * @param studyId
     *            study Id for which the milestone report is to be generated
     * @param orgId
     *            Organization id
     * @param startDate
     * @param endDate
     *            Get XML from stored procedure for milestone study wise summary
     *            reports
     */

    public void getMileXmlFromStudySProc(int studyId, int orgId,
            String startDate, String endDate) {
        CallableStatement cstmt = null;
        int count = 0;
        Connection conn = null;
        Clob XmlClob = null;
        String XmlString = "";

        Rlog.debug("Report",
                "In getMileXmlFromStudySProc in ReportDao line number 1");
        try {
            conn = getConnection();
            Rlog.debug("Report", "Study Id for Mile report " + studyId);

            cstmt = conn
                    .prepareCall("call pkg_milestone_reports.sp_ms_summary_study(?,?,?,?,?)");

            Rlog.debug("Report",
                    "In getMileXmlFromStudySProc in ReportDao line number 2");
            cstmt.setInt(1, studyId);
            cstmt.setInt(2, orgId);
            cstmt.setString(3, startDate);
            cstmt.setString(4, endDate);

            cstmt.registerOutParameter(5, java.sql.Types.CLOB);

            Rlog
                    .debug("Report",
                            "In getMileXmlFromStudySProc in ReportDaoNew line number 5");
            cstmt.execute();
            Rlog
                    .debug("Report",
                            "In getMileXmlFromStudySProc in ReportDaoNew line number 6");

            XmlClob = cstmt.getClob(5);
            Rlog.debug("Report",
                    "ReportDaoNew.getMileXmlFromStudySProc line x XmlString "
                            + XmlString);
            if (!(XmlClob == null)) {
                Rlog.debug("Report", "In getMileXmlFromStudySProc in XmlClob");
                XmlString = XmlClob.getSubString(1, (int) XmlClob.length());
                this.setRepXml(XmlString);
            }

            Rlog.debug("Report",
                    "ReportDaoNew.getMileXmlFromStudySProc line y XmlString "
                            + XmlString);

            Rlog
                    .debug("Report",
                            "In getMileXmlFromStudySProc in ReportDaoNew line number 7");

        } catch (Exception ex) {
            Rlog.fatal("Report",
                    "ReportDaoNew.getMileXmlFromStudySProc EXCEPTION" + ex);
            ex.printStackTrace();
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * @param studyId
     *            study Id for which the milestone report is to be generated
     * @param patId
     *            patient id (0 means all patients)
     * @param startDate
     * @param endDate
     * @param organization
     *            id, would be 0 for all organizations Get XML from stored
     *            procedure for milestone patient wise summary reports
     */

    public void getMileXmlFromPatSProc(int studyId, int patId,
            String startDate, String endDate, int orgId) {
        CallableStatement cstmt = null;
        int count = 0;
        Connection conn = null;
        Clob XmlClob = null;
        String XmlString = "";
        int counter = 6;

        Rlog.debug("Report",
                "In getMileXmlFromPatSProc in ReportDao line number 1");
        try {
            conn = getConnection();
            Rlog.debug("Report", "Study Id for Mile report " + studyId);

            cstmt = conn
                    .prepareCall("call pkg_milestone_reports.sp_ms_summary_patient(?,?,?,?,?,?)");

            Rlog.debug("Report",
                    "In getMileXmlFromPatSProc in ReportDao line number 2");
            cstmt.setInt(1, studyId);
            cstmt.setInt(2, patId);
            cstmt.setString(3, startDate);
            cstmt.setString(4, endDate);
            cstmt.setInt(5, orgId);

            cstmt.registerOutParameter(counter, java.sql.Types.CLOB);

            cstmt.execute();
            Rlog.debug("Report",
                    "In getMileXmlFromPatSProc in ReportDaoNew line number 6");

            XmlClob = cstmt.getClob(counter);
            Rlog.debug("Report",
                    "ReportDaoNew.getMileXmlFromPatSProc line x XmlString "
                            + XmlString);
            if (!(XmlClob == null)) {
                Rlog.debug("Report", "In getMileXmlFromPatSProc in XmlClob");
                XmlString = XmlClob.getSubString(1, (int) XmlClob.length());
                this.setRepXml(XmlString);
            }

            Rlog.debug("Report",
                    "ReportDaoNew.getMileXmlFromPatSProc line y XmlString "
                            + XmlString);

        } catch (SQLException ex) {
            Rlog.fatal("Report",
                    "ReportDaoNew.getMileXmlFromPatSProc EXCEPTION" + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * @param studyId
     *            study Id for which the milestone report is to be generated
     * @param startDate
     * @param endDate
     * @param Interval
     *            Type - M(Month), Q(Quarter), Y(Year) Get XML from stored
     *            procedure for milestone statistical payment reports
     */

    public void getMileXmlFromFinPaymentSProc(int studyId, String startDate,
            String endDate, String intervalType) {
        CallableStatement cstmt = null;
        int count = 0;
        int orgId = 0;
        Connection conn = null;
        Clob XmlClob = null;
        String XmlString = "";
        int counter = 5;

        Rlog.debug("Report",
                "In getMileXmlFromFinPaymentSProc in ReportDao line number 1");
        try {
            conn = getConnection();
            Rlog.debug("Report", "Study Id for Mile report " + studyId);

            cstmt = conn
                    .prepareCall("call pkg_milestone_reports.sp_ms_financial_payment(?,?,?,?,?)");

            cstmt.setInt(1, studyId);
            cstmt.setString(2, startDate);
            cstmt.setString(3, endDate);
            cstmt.setString(4, intervalType);

            cstmt.registerOutParameter(counter, java.sql.Types.CLOB);

            cstmt.execute();

            XmlClob = cstmt.getClob(counter);
            Rlog.debug("Report",
                    "ReportDaoNew.getMileXmlFromFinPaymentSProc line x XmlString "
                            + XmlString);
            if (!(XmlClob == null)) {
                XmlString = XmlClob.getSubString(1, (int) XmlClob.length());
                this.setRepXml(XmlString);
            }

            Rlog.debug("Report",
                    "ReportDaoNew.getMileXmlFromFinPaymentSProc line y XmlString "
                            + XmlString);

        } catch (SQLException ex) {
            Rlog
                    .fatal("Report",
                            "ReportDaoNew.getMileXmlFromFinPaymentSProc EXCEPTION"
                                    + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * @param studyId
     *            study Id for which the milestone forecast report is to be
     *            generated
     * @param startDate
     * @param endDate
     * @param Interval
     *            Type - M(Month), Q(Quarter), Y(Year) Get XML from stored
     *            procedure for milestone forecast reports
     */

    public void getMileXmlFromForecastSProc(int studyId, String startDate,
            String endDate, String intervalType) {
        CallableStatement cstmt = null;
        int count = 0;
        Connection conn = null;
        Clob XmlClob = null;
        String XmlString = "";

        Rlog.debug("Report",
                "In getMileXmlFromForecastSProc in ReportDao line number 1");
        try {
            conn = getConnection();
            Rlog.debug("Report", "Study Id for Mile report " + studyId);

            cstmt = conn
                    .prepareCall("call pkg_mile_forecast.sp_ms_forecast(?,?,?,?,?)");

            cstmt.setInt(1, studyId);
            cstmt.setString(2, startDate);
            cstmt.setString(3, endDate);
            cstmt.setString(4, intervalType);

            cstmt.registerOutParameter(5, java.sql.Types.CLOB);

            cstmt.execute();

            XmlClob = cstmt.getClob(5);
            Rlog.debug("Report",
                    "ReportDaoNew.getMileXmlFromStudySProc line x XmlString "
                            + XmlString);

            if (!(XmlClob == null)) {
                XmlString = XmlClob.getSubString(1, (int) XmlClob.length());
                this.setRepXml(XmlString);
            }

        } catch (SQLException ex) {
            Rlog.fatal("Report",
                    "ReportDaoNew.getMileXmlFromForecastSProc EXCEPTION" + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    public int createReport(int repId,int accId)
    {
       
        
        Object[] filterKeyL = this.getFilterKey();
        Object[] filterValueL = this.getFilterValue();
       
        int ret = 1;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            
            ArrayDescriptor inFilterKey = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING",conn);
            ARRAY filterKeyArray = new ARRAY(inFilterKey, conn, filterKeyL);
            ArrayDescriptor inFilterValue = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY filterValueArray = new ARRAY(inFilterValue, conn, filterValue);
            
            cstmt = conn
                    .prepareCall("{call PKG_Report.SP_GenReport(?,?,?,?,?)}");
            
            cstmt.setInt(1, repId);
            cstmt.setInt(2, accId);
            cstmt.setArray(3, filterKeyArray);
            cstmt.setArray(4, filterValueArray);
            cstmt.registerOutParameter(5, java.sql.Types.CLOB);
            
            cstmt.execute();
            
            Clob xmlClob=cstmt.getClob(5);
            String xmlString="";
            if (!(xmlClob == null)) {

                xmlString = xmlClob.getSubString(1, (int) xmlClob.length());
                
                this.setRepXml(xmlString);
            }

            return 1;

        } catch (Exception e) {
            Rlog.fatal("Report",
                    "EXCEPTION in ReportDaONew:createReport, excecuting Stored Procedure "
                            + e);
            e.printStackTrace();
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
   
    public void getFilterColumns(String repCat) {

        
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        Clob colClob=null;
        String colString="";
        String dispDiv="";
        try {
            
            conn = getConnection();
            String sql="select  repfilter_column,repfilter_coldispname,repfiltermap_mandatory,repfiltermap_dependon,repfilter_keyword,repfiltermap_column,repfilter_dispdiv,repfiltermap_dispdiv from er_repfiltermap,er_repfilter " +
                     " where repfiltermap_repcat= ? and fk_repfilter=pk_repfilter order by repfiltermap_seq";
            pstmt = conn
                    .prepareStatement(sql);
            pstmt.setString(1,repCat);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                //get the category wide column definition
                colClob=rs.getClob("repfilter_column");
                if (!(colClob == null)) {
                    colString = colClob.getSubString(1, (int) colClob.length());
                }
                this.setRepFilterColumns(colString);
                //get the filter column mapping column defintion if specified
                colString="";
                colClob=rs.getClob("repfiltermap_column");
                if (!(colClob == null)) {
                    colString = colClob.getSubString(1, (int) colClob.length());
                }
                this.setRepMapFilterColumns(colString);
                this.setRepFilterDisplayNames(rs.getString("repfilter_coldispname"));
                this.setRepFilterMandatory(rs.getString("repfiltermap_mandatory"));
                this.setRepFilterDependOn(rs.getString("repfiltermap_dependon"));
                this.setRepFilterKeywords(rs.getString("repfilter_keyword"));
                 dispDiv=rs.getString("repfiltermap_dispdiv");
                 dispDiv=(dispDiv==null)?"":dispDiv;
                 if (dispDiv.length()>0)
                	 this.setRepFilterDispDiv(dispDiv);
                 else
                this.setRepFilterDispDiv(rs.getString("repfilter_dispdiv"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("Report",
                    "ReportDaoNew.getFilterColumns EXCEPTION IN FETCHING FROM Filter table"
                            + ex);
            ex.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    

    public void resetDao() {
        pkReps = new ArrayList();

        fkAccs = new ArrayList();

        repName = new ArrayList();

        repDesc = new ArrayList();

        repColumns = new ArrayList();

        repFilters = new ArrayList();
        reportFilterKeywords=new ArrayList();
        this.repFilterApplicable=new ArrayList();

    }
	


    
}