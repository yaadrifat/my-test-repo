/*
 * Classname			paymentDetailBean
 * 
 * Version information : 1.0
 *
 * Date					09/22/2005
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.business.milepayment.impl;

/* IMPORT STATEMENTS */

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* END OF IMPORT STATEMENTS */

/**
 * The PaymentDetail POJO.<br>
 * <br>
 * 
 * @author Sonia Abrol
 * 
 * @version 1.0, 10/18/2005
 */
@Entity
@Table(name = "er_milepayment_details")

public class PaymentDetailBean implements Serializable {

	  
	/**
	 * 
	 */
	private static final long serialVersionUID = -6160564627842716397L;

	/**
	 * the PaymentDetail Primary Key
	 */
	public int id;

	/**
	 * the payment
	 */
	public String fkPayment;

	/**
	 * payment detail linked to
	 */
	public String linkedTo;

	/**
	 * the payment link to id
	 */
	public String linkToId;
	
	/**
	 * the payment link to level 1 id
	 */
	public String linkToLevel1;
	
	/**
	 * the payment link to level 2 id
	 */
	public String linkToLevel2;


	/**
	 * the payment detail amount
	 */
	public String amount;

	/**
	 * the id of the user who created the record
	 */
	public String creator;

	/**
	 * Id of the user who last modified the record
	 */
	public String lastModifiedBy;

	/**
	 * the IP address of the user who created or modified the record
	 */
	public String iPAdd;
	
	/**
	 * FK to er_mileachieved, will be used for milestone related records. <br>
	 * (this will help identify the exact milestone achievement record for multiple achievements for the same patient and milestone combination)
	 */
	public String linkMileAchieved;

	// GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
	@SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_PAYMENT_DETAILS", allocationSize=1)
	@Column(name = "pk_mp_details")
	public int getId() {
		return this.id;
	}

	public void setId(int Id) {
		this.id =Id;
	}

	@Column(name = "CREATOR")
	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	  
	
	 @Column(name = "mp_amount")
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	 

	@Column(name = "fk_milepayment")
	public String getFkPayment() {
		return fkPayment;
	}

	public void setFkPayment(String fkPayment) {
		this.fkPayment = fkPayment;
	}

	@Column(name = "mp_linkto_type")
	public String getLinkedTo() {
		return linkedTo;
	}

	public void setLinkedTo(String linkedTo) {
		this.linkedTo = linkedTo;
	}
	
	@Column(name = "mp_linkto_id")
	public String getLinkToId() {
		return linkToId;
	}

	public void setLinkToId(String linkToId) {
		this.linkToId = linkToId;
	}

	
	@Column(name = "ip_add")
	public String getIPAdd() {
		return iPAdd;
	}

	public void setIPAdd(String add) {
		iPAdd = add;
	}
	
	@Column(name = "LAST_MODIFIED_BY")
	
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	@Column(name = "MP_LEVEL1_ID")
	public String getLinkToLevel1() {
		return linkToLevel1;
	}

	public void setLinkToLevel1(String linkToLevel1) {
		this.linkToLevel1 = linkToLevel1;
	}

	@Column(name = "MP_LEVEL2_ID")
	public String getLinkToLevel2() {
		return linkToLevel2;
	}

	public void setLinkToLevel2(String linkToLevel2) {
		this.linkToLevel2 = linkToLevel2;
	}
	

	
	// END OF GETTER SETTER METHODS

	/**
	 * Updates the attributes of current insatance with the attributes of the
	 * one passed as argument
	 * 
	 * @param iBean
	 *            PaymentDetailBean with the PaymentDetail Information
	 * 
	 * @return 0 - if update is successful <br>
	 *         -2 - if update fails
	 * 
	 */
	public int updatePaymentDetail(PaymentDetailBean iBean) {
		try {
						
			 setAmount(iBean.getAmount()); 
			 setFkPayment(iBean.getFkPayment()) ;
			 setLinkedTo(iBean.getLinkedTo()) ;
			 setLinkToId(iBean.getLinkToId() ) ;
			 setIPAdd(iBean.getIPAdd()) ;
			 setLastModifiedBy(iBean.getLastModifiedBy()) ;
 			 setCreator(iBean.getCreator());
 			 setLinkToLevel1(iBean.getLinkToLevel1());
 			 setLinkToLevel2(iBean.getLinkToLevel2());
 			 setLinkMileAchieved(iBean.getLinkMileAchieved());	
			
			Rlog.debug("paymentDetail", "PaymentDetailBean.updatePaymentDetail");
			return 0;
		} catch (Exception e) {
			Rlog.fatal("paymentDetail",
					"Exception in paymentDetailBean.updatpaymentDetail " + e);
			return -2;
		}
	}
   
	
	/** Default constructor */
		
	public PaymentDetailBean()
	{
		
	}

	/** Full Argument constructor */
	public PaymentDetailBean(String amount, String creator, String payment, int id, String iPAdd, String lastModifiedBy, 
			String linkedTo, String linkToId, String linkToLevel1, String linkToLevel2,String mileach) {
		super();
		// TODO Auto-generated constructor stub
		this.amount = amount;
		this.creator = creator;
		this.fkPayment = payment;
		this.id = id;
		this.iPAdd =  iPAdd;
		this.lastModifiedBy = lastModifiedBy;
		
		this.linkedTo = linkedTo;
		this.linkToId = linkToId ;
		setLinkToLevel1(linkToLevel1);
		setLinkToLevel2(linkToLevel2);
		setLinkMileAchieved(mileach);
			
	}

	@Column(name = "FK_MILEACHIEVED")
	public String getLinkMileAchieved() {
		return linkMileAchieved;
	}

	public void setLinkMileAchieved(String linkMileAchieved) {
		this.linkMileAchieved = linkMileAchieved;
	}

	


}// end of class
