/**
 * Classname	FormQueryDao.class
 *
 * Version information 	1.0
 *
 * Date	01/04/2005
 *
 * Author -- Anu
 *
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

public class FormQueryDao extends CommonDAO implements java.io.Serializable {

    private ArrayList formQueryIds;

    private ArrayList formQueryStatusIds;

    private ArrayList enteredOn;

    private ArrayList fieldIds;

    private ArrayList queryStatus;

    private ArrayList enteredBy;

    private ArrayList fieldNames;

    private ArrayList queryType;

    private ArrayList formQueryType;

    private ArrayList queryNotes;

    private ArrayList fldSysIds;

    public FormQueryDao() {

        formQueryIds = new ArrayList();
        formQueryStatusIds = new ArrayList();
        enteredOn = new ArrayList();
        enteredBy = new ArrayList();
        fieldIds = new ArrayList();
        queryStatus = new ArrayList();
        fieldNames = new ArrayList();
        formQueryType = new ArrayList();
        queryType = new ArrayList();
        queryNotes = new ArrayList();
        fldSysIds = new ArrayList();

    }

    // Getter and Setter methods

    public ArrayList getFormQueryIds() {
        return this.formQueryIds;
    }

    public void setFormQueryIds(ArrayList formQueryIds) {
        this.formQueryIds = formQueryIds;
    }

    public ArrayList getFormQueryStatusIds() {
        return this.formQueryStatusIds;
    }

    public void setFormQueryStatusIds(ArrayList formQueryStatusIds) {
        this.formQueryStatusIds = formQueryStatusIds;
    }

    public ArrayList getEnteredOn() {
        return this.enteredOn;
    }

    public void setEnteredOn(ArrayList enteredOn) {
        this.enteredOn = enteredOn;
    }

    public ArrayList getFieldIds() {
        return this.fieldIds;
    }

    public void setFieldIds(ArrayList fieldIds) {
        this.fieldIds = fieldIds;
    }

    public ArrayList getQueryStatus() {
        return this.queryStatus;
    }

    public void setQueryStatus(ArrayList queryStatus) {
        Rlog.debug("formQuery", "queryStatus::::::::::" + queryStatus);
        this.queryStatus = queryStatus;
    }

    public ArrayList getEnteredBy() {
        return this.enteredBy;
    }

    public void setEnteredBy(ArrayList enteredBy) {
        Rlog.debug("formQuery", "enteredBy::::::::::" + enteredBy);
        this.enteredBy = enteredBy;
    }

    public ArrayList getFieldNames() {
        return this.fieldNames;
    }

    public void setFieldNames(ArrayList fieldNames) {
        this.fieldNames = fieldNames;
    }

    public ArrayList getQueryType() {
        return this.queryType;
    }

    public void setQueryType(ArrayList queryType) {
        this.queryType = queryType;
    }

    public ArrayList getFormQueryType() {
        return this.formQueryType;
    }

    public void setFormQueryType(ArrayList formQueryType) {
        this.formQueryType = formQueryType;
    }

    public ArrayList getQueryNotes() {
        return this.queryNotes;
    }

    public void setQueryNotes(ArrayList queryNotes) {
        this.queryNotes = queryNotes;
    }

    public ArrayList getFldSysIds() {
        return this.fldSysIds;
    }

    public void setFldSysIds(ArrayList fldSysIds) {
        this.fldSysIds = fldSysIds;
    }

    // //

    public void setFormQueryIds(Integer formQueryId) {
        formQueryIds.add(formQueryId);
    }

    public void setFormQueryStatusIds(Integer formQueryStatusId) {
        formQueryStatusIds.add(formQueryStatusId);
    }

    public void setFieldIds(Integer fieldId) {
        fieldIds.add(fieldId);
    }

    public void setEnteredOn(String entrdOn) {
        enteredOn.add(entrdOn);
    }

    public void setQueryStatus(String queryStat) {
        queryStatus.add(queryStat);
    }

    public void setEnteredBy(String entrdBy) {
        enteredBy.add(entrdBy);
    }

    public void setFieldNames(String fieldName) {
        fieldNames.add(fieldName);
    }

    public void setQueryType(String queryTyp) {
        queryType.add(queryTyp);
    }

    public void setQueryNotes(String queryNote) {
        queryNotes.add(queryNote);
    }

    public void setFormQueryType(String formQueryTyp) {
        formQueryType.add(formQueryTyp);
    }

    public void setFldSysIds(String fldSysId) {
        fldSysIds.add(fldSysId);
    }

    // end of getter and setter methods

    /**
     * insert records in query and query status.
     *
     * @param filledFormId -
     *            PK of the filled form
     * @param calledFrom -
     *            module linked to (Stores 1- for patient forms, 2- for study
     *            forms, 3- for account forms)
     * @param queryType -
     *            (Stores 1- for System Query , 2- for Manual Query , 3- for
     *            Response)
     * @param queryTypeId -
     *            Stores codelst id for a query type
     * @param fldSysIds -
     *            field system id for which query is entered
     * @param notes -
     *            notes for the query entered
     * @param status -
     *            query status
     * @param p_creator -
     *            creator of the query
     * @param p_ip_Add -
     *            ip address
     * @return o_ret_number
     */

    //KM-#4016 & IH-#4988
    //SM Enh#EDC_AT5 introduced comments parameter, also status won't be hard-coded 
    //and will be sent from the queryModal.jsp now on.
    
    public int insertIntoFormQuery(int filledFormId, int formId, int calledFrom,
            int queryType, String[] queryTypeIds, String[] fldSysIds,
            String[] notes, String[] status, String[] comments,  
            String[] modes, String[] pkQueryStats, int creator,
            String ipAdd)

    {
        int ret = 0;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            ArrayDescriptor dQueryTypeIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inQueryTypeIds = new ARRAY(dQueryTypeIds, conn, queryTypeIds);

            ArrayDescriptor dFldSysIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inFldSysIds = new ARRAY(dFldSysIds, conn, fldSysIds);

            ArrayDescriptor dNotes = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inNotes = new ARRAY(dNotes, conn, notes); //Used for storing query desc

            ArrayDescriptor dStatus = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inStatus = new ARRAY(dStatus, conn, status);
            
            ArrayDescriptor dComments = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inComments = new ARRAY(dComments, conn, comments); //Used for storing query/response comments

            ArrayDescriptor dModes = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inModes = new ARRAY(dModes, conn, modes);

            //KM-#4016
            ArrayDescriptor dPkQueryStats = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inPkQueryStats = new ARRAY(dPkQueryStats, conn, pkQueryStats);

            cstmt = conn
                    .prepareCall("{call PKG_FILLEDFORM.SP_INSERT_FORM_QUERY(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

            cstmt.setInt(1, filledFormId);

            cstmt.setInt(2, formId);

            cstmt.setInt(3, calledFrom);

            cstmt.setInt(4, queryType);

            cstmt.setArray(5, inQueryTypeIds);

            cstmt.setArray(6, inFldSysIds);

            cstmt.setArray(7, inNotes);

            cstmt.setArray(8, inStatus);
            
            cstmt.setArray(9, inComments);

            cstmt.setArray(10, inModes);

            cstmt.setArray(11, inPkQueryStats);

            cstmt.setInt(12, creator);

            cstmt.setString(13, ipAdd);

            cstmt.registerOutParameter(14, java.sql.Types.INTEGER);

            cstmt.execute();

            ret = cstmt.getInt(14);

            return ret;

        }

        catch (SQLException ex) {
            Rlog.fatal("formQuery",
                    "In insertIntoFormQuery in FormQueryDao line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return ret;

    }

    // /////////
    /**
     * Retreives all queries for a form
     *
     * @param queryModuleId -
     *            filled form Id
     * @param moduleLinkedTo -
     *            form linked to study, patient, account)
     */

    public void getQueriesForForm(int formId, int queryModuleId, int moduleLinkedTo) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        try {
            conn = getConnection();

            /*
             * IH ---
             * The use of int moduleLinkedTo was removed for Bug 3186 (description part),
             * but that created Bug 4826 (P1). The use of moduleLinkStr below is to intended to
             * fix Bug 4826 while not causing Bug 3186 again. But Bug 3186 cannot be tested
             * because of Bug 4497 fix.
             */
            String moduleLinkStr = null;
            if (moduleLinkedTo == 1 || moduleLinkedTo == 4) {
                moduleLinkStr = "1,4";
            } else {
                moduleLinkStr = String.valueOf(moduleLinkedTo);
            }
            // IH ---
            
            sql = "SELECT pk_formquery, ls.pk_formquerystatus,"
                + " (Select codelst_desc from er_codelst WHERE pk_codelst = fs.FK_CODELST_QUERYTYPE) query_type,"
                + " TO_CHAR(q.CREATED_ON,PKG_DATEUTIL.f_get_datetimeformat)AS enteredOn, "
                + " fk_field, fs.FK_CODELST_QUERYTYPE ,ls.FK_CODELST_QUERYSTATUS, "
                + " (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst= ls.FK_CODELST_QUERYSTATUS) status, "
                + " CASE WHEN fs.ENTERED_BY=0 THEN 'system-generated'  ELSE "
                + " (SELECT USR_FIRSTNAME || ' ' || USR_LASTNAME usrname FROM ER_USER WHERE pk_user=fs.entered_by) END AS enteredBy, "
                + " (SELECT fld_name FROM ER_FLDLIB WHERE pk_field=fk_field) field_name "
                + " FROM ER_FORMQUERY q, ER_FORMQUERYSTATUS fs, ER_FORMQUERYSTATUS ls " 
                + "WHERE q.PK_FORMQUERY = fs.FK_FORMQUERY AND q.PK_FORMQUERY = ls.FK_FORMQUERY"
                + " AND ls.PK_FORMQUERYSTATUS = (select max(s1.PK_FORMQUERYSTATUS) from ER_FORMQUERYSTATUS s1"
                + " WHERE q.pk_formquery = s1.fk_formquery AND trim(s1.ENTERED_ON) = (select max(trim(s2.ENTERED_ON)) from ER_FORMQUERYSTATUS s2"
                + " WHERE q.pk_formquery = s2.fk_formquery))"
                + " AND fs.PK_FORMQUERYSTATUS = (select min(s1.PK_FORMQUERYSTATUS) from ER_FORMQUERYSTATUS s1"
                + " WHERE q.pk_formquery = s1.fk_formquery AND trim(s1.ENTERED_ON) = (select min(trim(s2.ENTERED_ON)) from ER_FORMQUERYSTATUS s2"
                + " WHERE q.pk_formquery = s2.fk_formquery))"                
                + " AND q.fk_querymodule = ? AND q.fk_querymodule in (select FK_FILLEDFORM from er_formslinear where fk_form = ?)"
                + " AND q.QUERYMODULE_LINKEDTO in ( "+moduleLinkStr+" ) " // --- added by IH
                //+ " AND fk_querymodule = ? AND QUERYMODULE_LINKEDTO = ? "
                //+ " GROUP BY pk_formquery "
                + " order by pk_formquery desc, lower(FIELD_NAME)";
            
            pstmt = conn.prepareStatement(sql);

            Rlog.debug("formQuery", "getQueriesForForm:SQL::" + sql);
            Rlog.debug("formQuery", "getQueriesForForm:queryModuleId::" + queryModuleId);
            Rlog.debug("formQuery", "getQueriesForForm:formId::" + formId);
            //Rlog.debug("formQuery", "getQueriesForForm:SQL::" + moduleLinkedTo);

            pstmt.setInt(1, queryModuleId);
            pstmt.setInt(2, formId);
            //pstmt.setInt(2, moduleLinkedTo);

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("formQuery", "after sql exec::");

            while (rs.next()) {

                setFormQueryIds(new Integer(rs.getInt("pk_formquery")));

                setFormQueryStatusIds(new Integer(rs
                        .getInt("pk_formquerystatus")));

                setQueryType(rs.getString("query_type"));

                setEnteredOn(rs.getString("enteredOn"));

                setFieldIds(new Integer(rs.getInt("fk_field")));

                setQueryStatus(rs.getString("status"));

                setEnteredBy(rs.getString("enteredBy"));

                setFieldNames(rs.getString("field_name"));

                rows++;

            }

        } catch (SQLException ex) {
            Rlog.fatal("formQuery",
                    "FormQueryDao.getQueriesForForm EXCEPTION IN FETCHING FROM TABLE"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
    }

    /**
     * Retreives all query history for a field
     *
     * @param formQueryId -
     *            query Id
     */
    public void getQueryHistoryForField(int formQueryId) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        try {
            conn = getConnection();
            sql = "select pk_formquerystatus,FORMQUERY_TYPE,QUERY_NOTES, "
                    + " case when ENTERED_BY=0 then 'system-generated' "
                    + " else (select USR_FIRSTNAME || ' ' || USR_LASTNAME usrname from er_user where pk_user=entered_by) end as enteredBy, "
                    + " to_char(entered_on,PKG_DATEUTIL.f_get_datetimeformat)as enteredOn, "
                    + " (Select codelst_desc from er_codelst where pk_codelst = FK_CODELST_QUERYTYPE) query_type, "
                    + " (Select codelst_desc from er_codelst where pk_codelst= FK_CODELST_QUERYSTATUS) status "
                    + " from er_formquerystatus "
                    + " where fk_formquery = ? order by entered_on desc,pk_formquerystatus desc";
            pstmt = conn.prepareStatement(sql);

            Rlog.debug("formQuery",
                    "befre sql exec in getQueryHistoryForField::" + sql);

            pstmt.setInt(1, formQueryId);

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("formQuery",
                    "after sql exec in getQueryHistoryForField::");

            while (rs.next()) {

                setFormQueryStatusIds(new Integer(rs
                        .getInt("pk_formquerystatus")));

                setEnteredOn(rs.getString("enteredOn"));

                setQueryStatus(rs.getString("status"));

                setEnteredBy(rs.getString("enteredBy"));

                setQueryType(rs.getString("query_type"));

                setFormQueryType(rs.getString("FORMQUERY_TYPE"));

                setQueryNotes(rs.getString("QUERY_NOTES"));

                rows++;

            }

        } catch (SQLException ex) {
            Rlog.fatal("formQuery",
                    "FormQueryDao.getQueriesForForm EXCEPTION IN FETCHING FROM TABLE"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * insert records in query and query status.
     *
     * @param filledFormId -
     *            PK of the filled form
     * @param calledFrom -
     *            module linked to (Stores 1- for patient forms, 2- for study
     *            forms, 3- for account forms)
     * @param fldId -
     *            field id for which query is entered
     * @param queryType -
     *            (Stores 1- for System Query , 2- for Manual Query , 3- for
     *            Response)
     * @param queryTypeId -
     *            Stores codelst id for a query type
     * @param status -
     *            query status id
     * @param comments -
     *            notes for the query entered
     * @param p_creator -
     *            creator of the query
     * @param p_ip_Add -
     *            ip address
     * @return o_ret_number
     */

    public int insertQueryResponse(int filledformId, int calledFrom, int fldId,
            int queryType, int queryTypeId, int queryStatusId, String comments,
            String enteredOn, int enteredBy, int creator, String ipAdd)

    {
        int ret = 0;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            cstmt = conn
                    .prepareCall("{call PKG_FILLEDFORM.SP_INSERT_QUERY_RESPONSE(?,?,?,?,?,?,?,?,?,?,?,?)}");

            cstmt.setInt(1, filledformId);

            cstmt.setInt(2, calledFrom);

            cstmt.setInt(3, fldId);

            cstmt.setInt(4, queryType);

            cstmt.setInt(5, queryTypeId);

            cstmt.setInt(6, queryStatusId);

            cstmt.setString(7, comments);

            cstmt.setString(8, enteredOn);

            cstmt.setInt(9, enteredBy);

            cstmt.setInt(10, creator);

            cstmt.setString(11, ipAdd);

            cstmt.registerOutParameter(12, java.sql.Types.INTEGER);

            cstmt.execute();

            ret = cstmt.getInt(12);

            return ret;

        }

        catch (SQLException ex) {
            Rlog.fatal("formQuery",
                    "In insertQueryResponse in lineitemdao line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return ret;

    }

    /**
     * Get all formquerystatus data for fields
     *
     * @param formQueryId -
     *            query Id
     */
    public void getSystemFormQueryStat(int filledFormId, String fldSysIds) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        try {
            conn = getConnection();
            sql = "select PK_FORMQUERYSTATUS, FK_CODELST_QUERYTYPE, FK_CODELST_QUERYSTATUS, "
                    + " (select FLD_SYSTEMID from er_fldlib where  pk_field = fk_field) sysid, "
                    + " (select sub1.QUERY_NOTES from er_formquerystatus sub1 " 
                    + " where pk_formquery = sub1.fk_formquery" 
                    + " and sub1.pk_formquerystatus = (select min(sub2.pk_formquerystatus) from er_formquerystatus sub2" 
                    + " where pk_formquery = sub2.fk_formquery)) sysNote, "
                    + "fk_formquery "
                    + "	from er_formquerystatus prim, er_formquery q "
                    + " where pk_formquery = fk_formquery "
                    + " and PK_FORMQUERYSTATUS = (select max(s1.PK_FORMQUERYSTATUS) from ER_FORMQUERYSTATUS s1"
                    + " WHERE q.pk_formquery = s1.fk_formquery AND (s1.ENTERED_ON) = (select max(s2.ENTERED_ON) from ER_FORMQUERYSTATUS s2"
                    + " WHERE q.pk_formquery = s2.fk_formquery))"
                    + " and fk_formquery in (select pk_formquery from er_formquery where "
                    + " FK_QUERYMODULE = "
                    + filledFormId
                    + " and fk_field in (SELECT  pk_field FROM ER_FLDLIB WHERE FLD_SYSTEMID "
                    + " in (" + fldSysIds + "))) and FORMQUERY_TYPE = 3 ";
            pstmt = conn.prepareStatement(sql);

            Rlog.debug("formQuery",
                    "befre sql exec in getSystemFormQueryStat::" + sql);

            // pstmt.setInt(1,filledFormId);
            // pstmt.setString(2,fldSysIds);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setFormQueryStatusIds(new Integer(rs
                        .getInt("PK_FORMQUERYSTATUS")));
                setQueryStatus(rs.getString("FK_CODELST_QUERYSTATUS"));
                setFormQueryType(rs.getString("FK_CODELST_QUERYTYPE"));

                setFldSysIds(rs.getString("sysid"));
                
                setQueryNotes(rs.getString("sysNote"));
                
                setFormQueryIds(rs.getInt("fk_formquery"));

                rows++;

            }

        } catch (SQLException ex) {
            Rlog.fatal("formQuery",
                    "FormQueryDao.getSystemFormQueryStat EXCEPTION IN FETCHING FROM TABLE"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    /**
     * Get all formquerystatus data for fields
     *
     * @param formQueryId - query Id
     * @param fldSysIds - field System Id
     * @param fldReasons - query Notes
     */
    public void getSystemFormQueryStat(int filledFormId, ArrayList fldSysIds, ArrayList fldReasons) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        try {
            conn = getConnection();
            sql = "select PK_FORMQUERYSTATUS, FK_CODELST_QUERYTYPE, FK_CODELST_QUERYSTATUS, fk_field, "
                    + " (select FLD_SYSTEMID from er_fldlib where  pk_field = fk_field) sysid, "
                    + " (select sub1.QUERY_NOTES from er_formquerystatus sub1 " 
                    + " where pk_formquery = sub1.fk_formquery" 
                    + " and sub1.pk_formquerystatus = (select min(sub2.pk_formquerystatus) from er_formquerystatus sub2" 
                    + " where pk_formquery = sub2.fk_formquery)) sysNote, "
                    + " fk_formquery "
                    + "	from er_formquerystatus prim, er_formquery q "
                    + " where pk_formquery = fk_formquery "
                    + " and PK_FORMQUERYSTATUS = (select max(s1.PK_FORMQUERYSTATUS) from ER_FORMQUERYSTATUS s1"
                    + " WHERE q.pk_formquery = s1.fk_formquery AND (s1.ENTERED_ON) = (select max(s2.ENTERED_ON) from ER_FORMQUERYSTATUS s2"
                    + " WHERE q.pk_formquery = s2.fk_formquery))"
                    + " and fk_formquery in (select pk_formquery from er_formquery where "
                    + " FK_QUERYMODULE = "+ filledFormId
            		+ ") and FORMQUERY_TYPE = 3 ";
            sql = "select PK_FORMQUERYSTATUS, FK_CODELST_QUERYTYPE, FK_CODELST_QUERYSTATUS, fk_field, " 
            	+ "sysid, sysNote, fk_formquery from ("+sql+")";
            
            for (int i=0; i<fldSysIds.size();i++){
            	if (i == 0){
            		sql += " where (";
            	}else{
            		sql += " or ";
            	}
            	sql += " (fk_field in (SELECT  pk_field FROM ER_FLDLIB WHERE FLD_SYSTEMID = '"+fldSysIds.get(i)+"')"
            		+  " and sysNote = '"+StringUtil.escapeSpecialCharSQL(""+fldReasons.get(i))+"')";
            }
            if (fldSysIds.size() > 0){
            	sql += ") ";
            }
            pstmt = conn.prepareStatement(sql);

            Rlog.debug("formQuery",
                    "befre sql exec in getSystemFormQueryStat::" + sql);

            // pstmt.setInt(1,filledFormId);
            // pstmt.setString(2,fldSysIds);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setFormQueryStatusIds(new Integer(rs
                        .getInt("PK_FORMQUERYSTATUS")));
                setFieldIds(StringUtil.stringToInteger(rs.getString("fk_field")));
                setQueryStatus(rs.getString("FK_CODELST_QUERYSTATUS"));
                setFormQueryType(rs.getString("FK_CODELST_QUERYTYPE"));

                setFldSysIds(rs.getString("sysid"));
                
                setQueryNotes(rs.getString("sysNote"));
                
                setFormQueryIds(rs.getInt("fk_formquery"));

                rows++;

            }

        } catch (SQLException ex) {
            Rlog.fatal("formQuery",
                    "FormQueryDao.getSystemFormQueryStat EXCEPTION IN FETCHING FROM TABLE"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    /**
     * The method mainly to retrieved the query notes (which are also called Validation for Field 
     * in querymodal.jsp and Comments in Query History). First, try to retrieve query_notes record.
     * If that's empty, retrieve the corresponding system-generated (formquery_type=1) note.
     * @param formQueryId
     */
    public void getSystemQueryNotes(int formQueryId) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        try {
            conn = getConnection();
            sql = "select s.pk_formquerystatus, s.FORMQUERY_TYPE, "
                    + " NVL(s.QUERY_NOTES, (select query_notes from er_formquerystatus " 
                    + " where fk_formquery = ? and formquery_type = 1)) QUERY_NOTES, "
                    + " q.FK_FIELD, (select NVL(FLD_SYSTEMID,'') FLD_SYSTEMID from ER_FLDLIB where PK_FIELD = q.FK_FIELD) FLD_SYSID "
                    + " from er_formquerystatus s, er_formquery q "
                    + " where s.fk_formquery = q.pk_formquery and "
                    + " s.fk_formquery = ?  "
                    + " order by s.entered_on desc, s.pk_formquerystatus desc ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, formQueryId);
            pstmt.setInt(2, formQueryId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setFormQueryStatusIds(new Integer(rs
                        .getInt("pk_formquerystatus")));
                setFormQueryType(rs.getString("FORMQUERY_TYPE"));
                setQueryNotes(rs.getString("QUERY_NOTES"));
                setFieldIds(rs.getInt("FK_FIELD"));
                setFldSysIds((rs.getString("FLD_SYSID")));
            }
        } catch (SQLException ex) {
            Rlog.fatal("formQuery",
                    "FormQueryDao.getSystemQueryNotes EXCEPTION:"+ex);
        } finally {
            try {
                if (pstmt != null) { pstmt.close(); }
            } catch (Exception e) {}
            try {
                if (conn != null) { conn.close(); }
            } catch (Exception e) {}
        }
    }

}
