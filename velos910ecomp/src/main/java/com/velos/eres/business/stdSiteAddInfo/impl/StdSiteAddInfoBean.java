/*
 * Classname : StdSiteAddInfoBean
 * 
 * Version information: 1.0
 *
 * Date: 11/15/2004
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.business.stdSiteAddInfo.impl;

/* Import Statements */
import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The StudyIdBean CMP entity bean.<br>
 * <br>
 * 
 * @author ANu Khanna
 * @vesion 1.0 11/15/2004
 * @ejbHome StdSiteAddInfoHome
 * @ejbRemote StdSiteAddInfoRObj
 */
@Entity
@Table(name = "ER_STUDY_SITE_ADD_INFO")
public class StdSiteAddInfoBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6717371323080685752L;

    /**
     * The primary key
     */

    public int id;

    /**
     * The study site pk
     */
    public Integer studySiteId;

    /**
     * The add codelst info id
     */

    public Integer addCodelstInfo;

    /**
     * data
     */
    public String data;

    /**
     * The creator
     */

    public Integer creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    public Integer modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    public String ipAdd;

    public ArrayList StdSiteAddInfoStateKeepers;

    public String recordType;

    // //////////////////////////////////////////////////////////////////////////////////////

    /**
     * 
     * 
     * @return stdSiteAddinfoId
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_STUDYSITES_ADDINFO", allocationSize=1)
    @Column(name = "PK_STUDYSITES_ADDINFO")
    public int getId() {
        return id;
    }

    /**
     * 
     * 
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * 
     * @return studySiteId
     */
    @Column(name = "FK_STUDYSITES")
    public String getStudySiteId() {
        return StringUtil.integerToString(studySiteId);

    }

    /**
     * 
     * @param study
     *            site Id
     */

    public void setStudySiteId(String studySiteId) {
        this.studySiteId = StringUtil.stringToInteger(studySiteId);
    }

    @Column(name = "FK_CODELST_SITEADDINFO")
    public String getAddCodelstInfo() {
        return StringUtil.integerToString(addCodelstInfo);

    }

    public void setAddCodelstInfo(String addCodelstInfo) {
        this.addCodelstInfo = StringUtil.stringToInteger(addCodelstInfo);
    }

    @Column(name = "STUDYSITE_DATA")
    public String getAddInfoData() {
        return data;
    }

    public void setAddInfoData(String data) {
        this.data = data;
    }

    /**
     * @return the Creator
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);

    }

    /**
     * @param takes
     *            a string
     */

    public void setCreator(String creator) {
        this.creator = StringUtil.stringToInteger(creator);

    }

    /**
     * @return the unique ID of the User
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {

        return StringUtil.integerToString(this.modifiedBy);

    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = StringUtil.stringToInteger(modifiedBy);

    }

    /**
     * @return the IP Address of the Form Notification request
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Notification request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Transient
    public ArrayList getStdSiteAddInfoStateKeepers() {
        return StdSiteAddInfoStateKeepers;
    }

    public void setStdSiteAddInfoStateKeepers(
            ArrayList StdSiteAddInfoStateKeepers) {
        this.StdSiteAddInfoStateKeepers = StdSiteAddInfoStateKeepers;
    }

    public void setStdSiteAddInfoStateKeepers(
            StdSiteAddInfoBean StdSiteAddInfoStateKeepers) {
        this.StdSiteAddInfoStateKeepers.add(StdSiteAddInfoStateKeepers);
    }

    @Transient
    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    // END OF THE GETTER AND SETTER METHODS

    /*
     * public StdSiteAddInfoStateKeeper getStdSiteAddInfoStateKeeper() {
     * Rlog.debug("stdSiteAddInfo",
     * "StdSiteIdBean.getStdSiteAddInfoStateKeeper"); return new
     * StdSiteAddInfoStateKeeper(getId(), getStudySiteId(), getAddCodelstInfo(),
     * getAddInfoData(), getCreator(), getModifiedBy(), getIpAdd()); }
     */

    /**
     * sets up a state keeper containing details of the study id
     */

    /*
     * public void setStdSiteAddInfoStateKeeper(StdSiteAddInfoStateKeeper ssk) {
     * GenerateId stId = null; try { Connection conn = null; conn =
     * getConnection();
     * 
     * Rlog.debug("stdSiteAddInfo", "StudyIdBean.setStudyIdStateKeeper()
     * Connection :" + conn);
     * 
     * this.id = stId.getId("SEQ_ER_STUDYSITES_ADDINFO", conn);
     * 
     * conn.close();
     * 
     * setStudySiteId(ssk.getStudySiteId());
     * setAddCodelstInfo(ssk.getAddCodelstInfo());
     * setAddInfoData(ssk.getAddInfoData()); setCreator(ssk.getCreator());
     * setIpAdd(ssk.getIpAdd()); } catch (Exception e) {
     * Rlog.fatal("stdSiteAddInfo", "Error in StdSiteAddInfoStateKeeper() in
     * studyBean " + e); } }
     */

    /**
     * updates the Form studyId Record
     * 
     * @param studyIdssk
     * @return int 0: in case of successful updation; -2 in caseof exception
     */
    public int updateStdSiteAddInfo(StdSiteAddInfoBean ssk) {
        try {
            setStudySiteId(ssk.getStudySiteId());
            setAddCodelstInfo(ssk.getAddCodelstInfo());
            setAddInfoData(ssk.getAddInfoData());
            setModifiedBy(ssk.getModifiedBy());
            setIpAdd(ssk.getIpAdd());
            setCreator(ssk.getCreator());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("stdSiteAddInfo", " error in studyIdBean.updateStudyId");
            return -2;
        }
    }

    public StdSiteAddInfoBean() {
        StdSiteAddInfoStateKeepers = new ArrayList();

    }

    public StdSiteAddInfoBean(String info, String creator, String data, int id,
            String ipadd, String modBy, String siteId) {
        super();
        StdSiteAddInfoStateKeepers = new ArrayList();
        // TODO Auto-generated constructor stub
        setAddCodelstInfo(info);
        setCreator(creator);
        this.setAddInfoData(data);
        setId(id);
        setIpAdd(ipadd);
        setModifiedBy(modBy);
        setStudySiteId(siteId);
    }

}
