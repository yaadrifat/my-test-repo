/*
 * Classname : PatLoginBean
 * 
 * Version information: 1.0
 *
 * Date: 04/05/07
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Jnanamay Majumdar
 */

package com.velos.eres.business.patLogin.impl;

/* Import Statements */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;


/* End of Import Statements */

/**
 * The PerIdBean CMP entity bean.<br>
 * <br>
 * 
 * @author Jnanamay Majumdar
 * @vesion 1.0 04/04/07
 * @ejbHome 
 * @ejbRemote 
 */
@Entity
@Table(name = "er_portal_logins") 
@NamedQueries( 
		{
         @NamedQuery(name = "findValidLogin", query = "SELECT OBJECT(patLogin) FROM PatLoginBean patLogin where lower(trim(pl_login)) = lower(trim(:pl_login)) "),
         @NamedQuery(name = "validateLogin", query = "SELECT OBJECT(patLogin) FROM PatLoginBean patLogin where trim(pl_login)=:pl_login  AND trim(pl_password)= :pl_password")
        }
      )




public class PatLoginBean implements Serializable{

    private static final long serialVersionUID = 4206646393056409909L;

    /**
     * The primary key
     */

    public int id;

    /**
     * The patient id or the user pk
     */
    public Integer plId;

    /**
     * The login Id Type
     */

    public String plIdType;

    /**
     * patient login id
     */
    public String plLogin;
        
    /**
     * Patient pass word
     */
    public String plPassword;
    
    /**
     * Patient status
     */
    public String plStatus;
    
    /**
     * Patient log out time
     */
    public Integer plLogOutTime;
    
    /**
     * foreign key to the er_portal
     */
    public Integer plFkPortal;
    
    /*
	 * Audit columns
	 */
    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

	
    public boolean isAutoLogin;
    /**
     * 
     * 
     * @return formSecId
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name 
    		= "SEQ_GEN", sequenceName = "eres.SEQ_ER_PORTAL_LOGINS", allocationSize=1)
    @Column(name = "PK_PORTAL_LOGIN")
    public int getId() {
        return this.id;
    }    
    public void setId(int id) {
        this.id = id;
    }
    @Column(name = "PL_ID")
    public String getPlId() {
        return StringUtil.integerToString(this.plId);
    }
    public void setPlId(String plId) {
        if (!StringUtil.isEmpty(plId))
            this.plId = Integer.valueOf(plId);
    }      

    @Column(name = "PL_ID_TYPE")
    public String getPlIdType() {
        return this.plIdType;
    }
    
    public void setPlIdType(String plIdType) {        
        this.plIdType = plIdType;
    }    
    
    @Column(name = "PL_LOGIN")
    public String getPlLogin() {    	
        return this.plLogin  ;
    }
    
    public void setPlLogin(String plLogin  ) {
        this.plLogin   = plLogin  ;
    }
    
    @Column(name = "PL_PASSWORD")
    public String getPlPasswordPersist () {
        return this.plPassword  ;
    }
    
    public void setPlPasswordPersist (String plPassword  ) {
        this.plPassword   = plPassword  ;
    }
    
    
    @Transient
    public String getPlPassword() {
        String ret = "";
        if (this.plPassword != null) {
            return (this.plPassword);
        } else
            return ret;
    }

    /**
     * 
     * 
     * @param userPwd
     *            User Password
     */
    public void setPlPassword(String plPwd) {
        if (plPwd != null) {
            this.plPassword = (plPwd);
        }
    }
    
    
    
    
    @Column(name = "PL_STATUS")
    public String getPlStatus () {
        return this.plStatus ;
    }
    
    public void setPlStatus (String plStatus  ) {        
        this.plStatus  = plStatus ;
    }
    
    @Column(name = "PL_LOGOUT_TIME")
    public String getPlLogOutTime() {
        return StringUtil.integerToString(this.plLogOutTime );

    }    
    public void setPlLogOutTime (String plLogOutTime) {
        if (!StringUtil.isEmpty(plLogOutTime ))
            this.plLogOutTime  = Integer.valueOf(plLogOutTime );
    }  
    
    @Column(name = "FK_PORTAL")
    public String getFkPortal() {
        return StringUtil.integerToString(this.plFkPortal);

    }    
    public void setFkPortal (String fkPortal ) {
        if (!StringUtil.isEmpty(fkPortal))
            this.plFkPortal   = Integer.valueOf(fkPortal  );
    }  
    
    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }
    
    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {

        this.creator = StringUtil.stringToInteger(creator);
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = StringUtil.stringToInteger(modifiedBy);
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    
    
    // END OF THE GETTER AND SETTER METHODS

   
    /**
     * updates the patLogin
     * 
     * @param psk
     * 
     * @return int 0: in case of successful updation; -2 in caseof exception
     */
    public int updatePatLogin(PatLoginBean psk) {
        try {
        	           
        	setId(psk.getId());
        	setPlId(psk.getPlId()); 
        	setPlIdType(psk.getPlIdType());        	
        	setPlLogin(psk.getPlLogin());
        	setPlPassword(psk.getPlPassword()); 
        	setPlStatus(psk.getPlStatus());
        	setPlLogOutTime(psk.getPlLogOutTime());
        	setFkPortal(psk.getFkPortal());
        	setCreator(psk.getCreator());
            setModifiedBy(psk.getModifiedBy());
            setIpAdd(psk.getIpAdd());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("patLogin", " error in patLoginBean.updatePatLogin");
            return -2;
        }
    }
//  EMPTY constructor
    public PatLoginBean() {
    	
    }

    public PatLoginBean(int id, String plId, String plIdType,String plLogin,String plPassword,String plStatus,String plLogOutTime,String fkPortal, String plCreator, String plModifiedBy, String plIpAdd   ) {
        super();
        // TODO Auto-generated constructor stub        
               
        setId(id);
    	setPlId(plId); 
    	setPlIdType(plIdType);    	
    	setPlLogin(plLogin);
    	setPlPassword(plPassword); 
    	setPlStatus(plStatus);
    	setPlLogOutTime(plLogOutTime);
    	setFkPortal(fkPortal);    	
    	setCreator(plCreator);
        setModifiedBy(plModifiedBy);
        setIpAdd(plIpAdd);
    	
    	
    }
    
    @Transient
	public boolean getIsAutoLogin() {
		return isAutoLogin;
	}
	public void setIsAutoLogin(boolean isAutoLogin) {
		this.isAutoLogin = isAutoLogin;
	}

}
