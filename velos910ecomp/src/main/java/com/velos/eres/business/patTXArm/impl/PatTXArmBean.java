/*
 * Classname : PatTXArmBean
 * 
 * Version information: 1.0
 *
 * Date: 05/02/2005
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.business.patTXArm.impl;

/* Import Statements */
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The patTXArm CMP entity bean.<br>
 * <br>
 * 
 * @author Anu
 * @vesion 1.0 05/02/2005
 * @ejbHome PatTXArmHome
 * @ejbRemote PatTXArmRObj
 */
@Entity
@Table(name = "ER_PATTXARM")
public class PatTXArmBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3258129137586682169L;

    /**
     * the Pat TX Arm id
     */
    public int patTXArmId;

    /**
     * the pat prot Id
     */
    public Integer patProtId;

    /**
     * the studyTXArm id
     */
    public Integer studyTXArmId;

    /**
     * the txDrugInfo
     */

    public String txDrugInfo;

    /**
     * the startDate
     */

    public Date startDate;

    /**
     * the endDate
     */

    public Date endDate;

    /**
     * the notes
     */

    public String notes;

    /**
     * creator
     */
    public Integer creator;

    /**
     * last modified by
     */
    public Integer modifiedBy;

    // GETTER SETTER METHODS

    public PatTXArmBean() {

    }

    public PatTXArmBean(int patTXArmId, String patProtId, String studyTXArmId,
            String txDrugInfo, String startDate, String endDate, String notes,
            String creator, String modifiedBy) {
        super();
        // TODO Auto-generated constructor stub
        setPatTXArmId(patTXArmId);
        setPatProtId(patProtId);
        setStudyTXArmId(studyTXArmId);
        setTXDrugInfo(txDrugInfo);
        setStartDate(startDate);
        setEndDate(endDate);
        setNotes(notes);
        setCreator(creator);
        setModifiedBy(modifiedBy);
    }

    /**
     * 
     * Get pat treatment Id
     * 
     * @return the patTXArmId
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_PAT_TX_ARM", allocationSize=1)
    @Column(name = "PK_PATTXARM")
    public int getPatTXArmId() {
        return this.patTXArmId;
    }

    public void setPatTXArmId(int armId) {
        this.patTXArmId = armId;

    }

    /**
     * Get patProt Id
     * 
     * @return patProtId
     */
    @Column(name = "FK_PATPROT")
    public String getPatProtId() {
        return StringUtil.integerToString(this.patProtId);
    }

    /**
     * sets patProt Id
     * 
     * @param patProtId
     */
    public void setPatProtId(String patProtId) {
        this.patProtId = StringUtil.stringToInteger(patProtId);
    }

    /**
     * Get studyTXArm Id
     * 
     * @return studyTXArmId
     */
    @Column(name = "FK_STUDYTXARM")
    public String getStudyTXArmId() {
        return StringUtil.integerToString(this.studyTXArmId);
    }

    /**
     * sets studyTXArm Id
     * 
     * @param studyTXArmId
     */
    public void setStudyTXArmId(String studyTXArmId) {
        this.studyTXArmId = StringUtil.stringToInteger(studyTXArmId);
    }

    /**
     * Get tx drug info
     * 
     * @return txDrugInfo
     */
    @Column(name = "TX_DRUG_INFO")
    public String getTXDrugInfo() {
        return this.txDrugInfo;
    }

    /**
     * sets txDrugInfo
     * 
     * @param txDrugInfo
     */
    public void setTXDrugInfo(String txDrugInfo) {
        this.txDrugInfo = txDrugInfo;
    }

    @Column(name = "TX_START_DATE")
    public Date getStartDt() {

        return this.startDate;
    }

    /**
     * @param startDate
     */
    public void setStartDt(Date startDate) {

        this.startDate = startDate;

    }

    @Transient
    public String getStartDate() {

        return DateUtil.dateToString(getStartDt());
    }

    /**
     * @param startDate
     */
    public void setStartDate(String startDate) {

        setStartDt(DateUtil.stringToDate(startDate, null));

    }

    @Column(name = "TX_END_DATE")
    public Date getEndDt() {

        return this.endDate;
    }

    /**
     * @param endDate
     */
    public void setEndDt(Date endDate) {

        this.endDate = endDate;

    }

    @Transient
    public String getEndDate() {

        return DateUtil.dateToString(getEndDt());
    }

    /**
     * @param endDate
     */
    public void setEndDate(String endDate) {

        setEndDt(DateUtil.stringToDate(endDate, null));

    }

    /**
     * Get treatment notes
     * 
     * @return String notes
     */
    @Column(name = "NOTES")
    public String getNotes() {
        return this.notes;
    }

    /**
     * sets notes
     * 
     * @param notes
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * Get Status creator
     * 
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {

        this.creator = StringUtil.stringToInteger(creator);
    }

    /**
     * Get Status modified by
     * 
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = StringUtil.stringToInteger(modifiedBy);
    }

    // END OF GETTER SETTER METHODS

    /**
     * @return the state holder object associated with this status
     */

    /*
     * public PatTXArmStateKeeper getPatTXArmStateKeeper() {
     * 
     * return new PatTXArmStateKeeper(getPatTXArmId(), getPatProtId(),
     * getStudyTXArmId(), getTXDrugInfo(), getStartDate(), getEndDate(),
     * getNotes(), getCreator(), getModifiedBy()); }
     */

    /**
     * sets up a state keeper containing details of the pat arm
     * 
     * @param fqk
     *            PatTXArmStateKeeper
     */

    /*
     * public void setPatTXArmStateKeeper(PatTXArmStateKeeper ptk) { GenerateId
     * patTXArmId = null;
     * 
     * try { Connection conn = null; conn = getConnection();
     * 
     * this.patTXArmId = patTXArmId.getId("SEQ_ER_PAT_TX_ARM", conn);
     * conn.close(); setPatProtId(ptk.getPatProtId());
     * setStudyTXArmId(ptk.getStudyTXArmId());
     * setTXDrugInfo(ptk.getTXDrugInfo()); setStartDate(ptk.getStartDate());
     * setEndDate(ptk.getEndDate()); setNotes(ptk.getNotes());
     * setCreator(ptk.getCreator()); setModifiedBy(ptk.getModifiedBy()); } catch
     * (Exception e) { Rlog.fatal("patTXArm", "Exception in
     * PatTXArmBean.setPatTXArmStateKeeper" + e); } }
     */
    /**
     * updates the study tx arm details
     * 
     * @param stk
     *            StudyTXArmStateKeeper
     * @return 0 if successful; -2 for exception
     */
    public int updatePatTXArm(PatTXArmBean ptk) {
        try {
            setPatProtId(ptk.getPatProtId());
            setStudyTXArmId(ptk.getStudyTXArmId());
            setTXDrugInfo(ptk.getTXDrugInfo());
            setStartDate(ptk.getStartDate());
            setEndDate(ptk.getEndDate());
            setNotes(ptk.getNotes());
            setCreator(ptk.getCreator());
            setModifiedBy(ptk.getModifiedBy());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("patTXArm", "Exception in  pattxarm.updatePatTXArm" + e);
            return -2;
        }
    }

}// end of class
