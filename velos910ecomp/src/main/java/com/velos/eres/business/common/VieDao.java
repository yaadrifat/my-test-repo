//Vishal dt 03/29/2001

package com.velos.eres.business.common;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.velos.eres.service.util.Rlog;

import cryptix.util.core.Hex;

/**
 * This class acts as layer between viewer and database for HL7 Interface
 * module.
 */

public class VieDao extends CommonDAO implements java.io.Serializable {

    private String msgValue = "";

    private int msgId = 0;

    private String notifyemail = "", server = "", port = "", notification = "",
            password = "", msgError = "";

    public VieDao() {

    }

    public String getMsgValue() {
        return this.msgValue;
    }

    public void setMsgValue(String msgValue) {
        this.msgValue = msgValue;
    }

    public String getNotification() {
        return this.notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public String getNotifyemail() {
        return this.notifyemail;
    }

    public void setNotifyemail(String notifyemail) {
        this.notifyemail = notifyemail;
    }

    public String getPort() {
        return this.port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getServer() {
        return this.server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    // end gettter/setter methods

    /**
     * Initialize the HL7 Representation of the message stored in the database.
     * 
     * @param int
     *            msgId - Id of the message to be retrieved.
     */
    public void getMessage(int msgId) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        Clob msgClob = null;
        try {
            String sql = "select message from vie_message where pk_message=?";
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, msgId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                msgClob = rs.getClob(1);
                if (msgClob != null) {
                    setMsgValue(msgClob.getSubString(1, (int) msgClob.length()));

                }

            }
        } catch (Exception e) {
            Rlog.fatal("common", "Error:VieDao:" + e);
        }finally
        {
            try
            {
                if (pstmt!=null) pstmt.close();
             }catch(Exception e)
             {
                 e.printStackTrace();
             }
             
             try
             {
                 if (conn!=null) conn.close();
              }catch(Exception e)
              {
                  e.printStackTrace();
              }
             
          }
    }

    /**
     * This method return Error return by messagge processor.
     * 
     * @param int
     *            msgId - Id of the message.
     * @return Error message
     */
    public String getMsgError(int msgId) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        Clob msgClob = null;
        try {
            String sql = "select log_text from vie_msglog where fk_message=?";
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, msgId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                msgError = rs.getString(1);

            }
            return msgError;
        } catch (Exception e) {
            Rlog.fatal("common", "Error:VieDao:getMsgError" + e);
            return "Error accessing message log.";
        }finally
        {
            try
            {
                if (pstmt!=null) pstmt.close();
             }catch(Exception e)
             {
                 e.printStackTrace();
             }
             
             try
             {
                 if (conn!=null) conn.close();
              }catch(Exception e)
              {
                  e.printStackTrace();
              }
             
          }
    }

    /**
     * This method stores the configuration specified by User in Interface
     * message browser.
     * 
     * @param String
     *            keyword - Keyword for which value is being stored
     * @param String
     *            keyword_value - value specified for the keyword.
     * @return int - return>0 if updation successful.
     */

    public int updateSetting(String keyword, String keyword_value) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        String sql = "";
        try {
            conn = getConnection();
            sql = "update vie_settings set keyword_value=? where keyword=?";

            pstmt = conn.prepareStatement(sql);

            if (keyword.equals("password")) {

                keyword_value = getMessageDigest(keyword_value);

            }
            pstmt.setString(1, keyword_value);
            pstmt.setString(2, keyword);
            pstmt.executeUpdate();
            conn.commit();
            return 1;
        } catch (SQLException e) {

            Rlog.fatal("Interface", "VieDao:updateSetting for:" + keyword + e);
            return -1;

        } finally {
            try { if (pstmt != null) { pstmt.close(); } } catch(Exception e) {}
            try { if (conn != null) { conn.close(); } } catch(Exception e) {}
        }

    }// endfor updatesetting

    /**
     * Return the password for interface administration module
     * 
     * @return encoded string representation of the password.
     */
    public String getUserPwd() {
        Connection conn = null;
        PreparedStatement pstmt = null;
        String sql = "";
        ResultSet rs = null;
        String password = "";
        try {
            conn = getConnection();
            sql = "select keyword_value from vie_settings where keyword=?";

            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, "password");
            rs = pstmt.executeQuery();
            while (rs.next()) {
                password = rs.getString(1);
            }

            return password;
        } catch (SQLException e) {
            Rlog.fatal("Interface", "VieDao:getUserPwd for:password" + e);
            return "error";

        } finally {
            try { if (pstmt != null) { pstmt.close(); } } catch(Exception e) {}
            try { if (conn != null) { conn.close(); } } catch(Exception e) {}
        }


    }

    /**
     * Return all the settings for a category.
     * 
     * @param String
     *            category - category for which values are retrieved.
     */
    public void getsettings(String category) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        String sql = "";
        ResultSet rs = null;
        String password = "", keyword = "";
        try {
            conn = getConnection();
            sql = "select * from vie_settings where category=?";

            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, category);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                keyword = (rs.getString("keyword")).toLowerCase();
                if (keyword.equals("server"))
                    setServer(rs.getString("keyword_value"));
                if (keyword.equals("port"))
                    setPort(rs.getString("keyword_value"));
                if (keyword.equals("notifyemail"))
                    setNotifyemail(rs.getString("keyword_value"));
                if (keyword.equals("notification"))
                    setNotification(rs.getString("keyword_value"));

            }

        } catch (SQLException e) {
            Rlog.fatal("Interface", "VieDao:getsettings :" + e);

        } finally {
            try { if (pstmt != null) { pstmt.close(); } } catch(Exception e) {}
            try { if (conn != null) { conn.close(); } } catch(Exception e) {}
        }

    }

    /**
     * Returs encoded representation of the password
     * 
     * @param String
     *            message - Ascii password to be encoded.
     */

    public String getMessageDigest(String message) {

        String msg = message;
        byte[] message_digest = null;
        String hex_string = "";

        try {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            byte[] buffer = msg.getBytes();
            int len = buffer.length;
            md.update(buffer, 0, len);
            message_digest = md.digest();
            hex_string = Hex.toString(message_digest);
        } catch (NoSuchAlgorithmException ae) {
            System.out.println(ae.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }

        return hex_string;
    }

}
// end of class

