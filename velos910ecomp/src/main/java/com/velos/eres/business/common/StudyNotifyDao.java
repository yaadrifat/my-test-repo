/*
 * Classname			StudyNotifyDao.class
 * 
 * Version information 	1.0
 *
 * Date					04/30/2001
 * 
 * Copyright notice		Velos, Inc.
 * by sonia sahni
 */

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

/**
 * StudyNotifyDao for getting Notification Signup records for a user
 * 
 * @author Sonia Sahni
 * @version : 1.0 04/30/2001
 */

public class StudyNotifyDao extends CommonDAO implements java.io.Serializable {
    private ArrayList studyNotifyIds;

    private ArrayList studyNotifyTArea;

    private ArrayList studyNotifyUsers;

    private ArrayList studyNotifyType;

    private ArrayList studyNotifyKeywords;

    private int cRows;

    public StudyNotifyDao() {
        studyNotifyIds = new ArrayList();
        studyNotifyTArea = new ArrayList();
        studyNotifyUsers = new ArrayList();
        studyNotifyType = new ArrayList();
        studyNotifyKeywords = new ArrayList();

    }

    // Getter and Setter methods

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public ArrayList getStudyNotifyIds() {
        return this.studyNotifyIds;
    }

    public void setStudyNotifyIds(ArrayList studyNotifyIds) {
        this.studyNotifyIds = studyNotifyIds;
    }

    public ArrayList getStudyNotifyKeywords() {
        return this.studyNotifyKeywords;
    }

    public void setStudyNotifyKeywords(ArrayList studyNotifyKeywords) {
        this.studyNotifyKeywords = studyNotifyKeywords;
    }

    public ArrayList getStudyNotifyTArea() {
        return this.studyNotifyTArea;
    }

    public void setStudyNotifyTArea(ArrayList studyNotifyTArea) {
        this.studyNotifyTArea = studyNotifyTArea;
    }

    public ArrayList getStudyNotifyType() {
        return this.studyNotifyType;
    }

    public void setStudyNotifyType(ArrayList studyNotifyType) {
        this.studyNotifyType = studyNotifyType;
    }

    public ArrayList getStudyNotifyUsers() {
        return this.studyNotifyUsers;
    }

    public void setStudyNotifyUsers(ArrayList studyNotifyUsers) {
        this.studyNotifyUsers = studyNotifyUsers;
    }

    //

    public void setStudyNotifyIds(Integer id) {
        studyNotifyIds.add(id);
    }

    public void setStudyNotifyKeywords(String kwrd) {
        studyNotifyKeywords.add(kwrd);
    }

    public void setStudyNotifyTArea(String tArea) {
        studyNotifyTArea.add(tArea);
    }

    public void setStudyNotifyType(String nType) {
        studyNotifyType.add(nType);
    }

    public void setStudyNotifyUsers(Integer userId) {
        studyNotifyUsers.add(userId);
    }

    // end of getter and setter methods

    /**
     * Gets all Therapeutic Area Subscriptions for a user. Sets the values in
     * the class attributes
     * 
     * @param userId -
     *            Id of the User
     */

    public void getUserSubs(int userId, String sType) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("select b.PK_STUDYNOTIFY, "
                            + "(select CODELST_DESC from er_codelst where PK_CODELST = b.FK_CODELST_TAREA) as FK_CODELST_TAREA, "
                            + "b.STUDYNOTIFY_KEYWRDS"
                            + " from ER_STUDYNOTIFY b where b.FK_USER = ? and rtrim(b.STUDYNOTIFY_TYPE) = ?");
            pstmt.setInt(1, userId);
            pstmt.setString(2, sType);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setStudyNotifyIds(new Integer(rs.getInt("PK_STUDYNOTIFY")));
                setStudyNotifyTArea(rs.getString("FK_CODELST_TAREA"));
                setStudyNotifyKeywords(rs.getString("STUDYNOTIFY_KEYWRDS"));
                setStudyNotifyType(sType);
                setStudyNotifyUsers(new Integer(userId));
                rows++;
                Rlog.debug("study", "StudyNotifyDao.getUserSubs " + rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("study",
                    "StudyNotifyDao.getUserSubs  EXCEPTION IN FETCHING FROM er_studynotify table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public int notifySubscribers(int studyPK) { 

        int ret = 0;
	 	CallableStatement cstmt = null;

        Rlog.debug("StudyNotifyDao", "In notifySubscribers");
        Connection conn = null;  
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call PKG_BROADCAST.SP_BROADCAST_STUDY(?,?)}");

            cstmt.setInt(1, studyPK);
            cstmt.registerOutParameter(2, java.sql.Types.INTEGER);

            cstmt.execute();

            Rlog.debug("updateschedules", " after execution of PKG_BROADCAST.SP_BROADCAST_STUDY");
            ret = cstmt.getInt(2);
            
            

        } catch (Exception ex) {
            Rlog.fatal("StudyNotifyDao",
                    "In notifySubscribers() in StudyNotifyDao: EXCEPTION IN calling the procedure"
                            + ex);
            
            ret = -1;

        } finally {
            try {
                if (cstmt != null)
                {
                    cstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                {
                    conn.close();}
            } catch (Exception e) {
            }
            
        }
        return ret;
    }



}
