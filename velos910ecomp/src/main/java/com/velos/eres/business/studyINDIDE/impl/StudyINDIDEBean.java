package com.velos.eres.business.studyINDIDE.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

@Entity
@Table(name = "ER_STUDY_INDIDE")
public class StudyINDIDEBean implements Serializable {

	/**
	 * Generated serialVersionUID
	 */
	private static final long serialVersionUID = -222903421892350007L;

	/**
	 * PK_STUDY_INDIIDE
	 */
	public int idINDIDE;

	/**
	 * FK_STUDY
	 */
	public Integer fkStudy;

	/**
	 * INDIDE_TYPE
	 */
	public Integer typeINDIDE;

	/**
	 * INDIDE_NUMBER
	 */
	public String numberINDIDE;

	/**
	 * FK_CODELST_INDIDE_GRANTOR
	 */
	public Integer grantor;

	/**
	 * FK_CODELST_INDIDE_HOLDER
	 */
	public Integer holder;

	/**
	 * FK_CODELST_PROGRAM_CODE
	 */
	public Integer programCode;

	/**
	 * INDIDE_EXPAND_ACCESS
	 */
	public Integer expandAccess;

	/**
	 * FK_CODELST_ACCESS_TYPE
	 */
	public Integer accessType;

	/**
	 * INDIDE_EXEMPT
	 */
	public Integer exemptINDIDE;

	/*
	 * the record creator
	 */
	public Integer creator;

	/*
	 * last modified by
	 */
	public Integer modifiedBy;

	/*
	 * the IP Address
	 */
	public String ipAdd;

	/**
	 * 
	 * @return studyINDIDE PK
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_GEN")
	@SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_STUDY_INDIDE", allocationSize = 1)
	@Column(name = "PK_STUDY_INDIIDE")
	public int getIdINDIDE() {
		return idINDIDE;
	}

	public void setIdINDIDE(int idINDIDE) {
		this.idINDIDE = idINDIDE;
	}

	/**
	 * 
	 * 
	 * @return FK_STUDY
	 */
	@Column(name = "FK_STUDY")
	public String getFkStudy() {

		return StringUtil.integerToString(this.fkStudy);
	}

	public void setFkStudy(String fkStudy) {

		if (fkStudy != null) {
			this.fkStudy = Integer.valueOf(fkStudy);
		}
	}

	/**
	 * 
	 * 
	 * @return INDIDE_TYPE
	 */
	@Column(name = "INDIDE_TYPE")
	public String getTypeINDIDE() {

		return StringUtil.integerToString(this.typeINDIDE);
	}

	public void setTypeINDIDE(String typeINDIDE) {
		if (typeINDIDE != null) {
			this.typeINDIDE = Integer.valueOf(typeINDIDE);
		}
	}

	/**
	 * 
	 * 
	 * @return INDIDE_NUMBER
	 */
	@Column(name = "INDIDE_NUMBER")
	public String getNumberINDIDE() {
		return numberINDIDE;
	}

	public void setNumberINDIDE(String numberINDIDE) {
		this.numberINDIDE = numberINDIDE;
	}

	/**
	 * 
	 * 
	 * @return FK_CODELST_INDIDE_GRANTOR
	 */
	@Column(name = "FK_CODELST_INDIDE_GRANTOR")
	public String getGrantor() {

		return StringUtil.integerToString(this.grantor);
	}

	public void setGrantor(String grantor) {
		if (grantor != null) {
			this.grantor = Integer.valueOf(grantor);
		}
	}

	/**
	 * 
	 * 
	 * @return FK_CODELST_INDIDE_HOLDER
	 */
	@Column(name = "FK_CODELST_INDIDE_HOLDER")
	public String getHolder() {
		return StringUtil.integerToString(this.holder);
	}

	public void setHolder(String holder) {
		if (holder != null) {
			this.holder = Integer.valueOf(holder);
		}
	}

	/**
	 * 
	 * 
	 * @return FK_CODELST_PROGRAM_CODE
	 */
	@Column(name = "FK_CODELST_PROGRAM_CODE")
	public String getProgramCode() {
		return StringUtil.integerToString(this.programCode);
	}

	public void setProgramCode(String programCode) {
		if (programCode != null && !programCode.isEmpty()) {
			this.programCode = Integer.valueOf(programCode);
		}
		//Added for Bug#7984 : Raviesh
		else if(programCode==null)
		{
			this.programCode=null;
		}
	}

	/**
	 * 
	 * 
	 * @return INDIDE_EXPAND_ACCESS
	 */
	@Column(name = "INDIDE_EXPAND_ACCESS")
	public String getExpandAccess() {
		return StringUtil.integerToString(this.expandAccess);
	}

	public void setExpandAccess(String expandAccess) {

		if (expandAccess != null) {
			this.expandAccess = Integer.valueOf(expandAccess);
		}
	}

	/**
	 * 
	 * 
	 * @return FK_CODELST_ACCESS_TYPE
	 */
	@Column(name = "FK_CODELST_ACCESS_TYPE")
	public String getAccessType() {
		return StringUtil.integerToString(this.accessType);
	}

	public void setAccessType(String accessType) {

		if (accessType != null && !accessType.isEmpty()) {
			this.accessType = Integer.valueOf(accessType);
		}
		//Added for Bug#7984 : Raviesh
		else if(accessType==null)
		{
			this.accessType=null;
		}
	}
	
	/**
	 * 
	 * 
	 * @return INDIDE_EXEMPT
	 */
	@Column(name = "INDIDE_EXEMPT")
	public String getExemptINDIDE() {
		return StringUtil.integerToString(this.exemptINDIDE);
	}

	public void setExemptINDIDE(String exemptINDIDE) {
		if (exemptINDIDE != null ) {
			this.exemptINDIDE = StringUtil.stringToInteger(exemptINDIDE);
		}
	}

	@Column(name = "CREATOR")
	public String getCreator() {
		return StringUtil.integerToString(this.creator);
	}

	public void setCreator(String creator) {
		if (creator != null && !creator.isEmpty()) {
			this.creator = Integer.valueOf(creator);
		}
	}

	/**
	 * @return The Id of the user modifying this record
	 */
	@Column(name = "LAST_MODIFIED_BY")
	public String getModifiedBy() {
		return StringUtil.integerToString(this.modifiedBy);
	}

	/**
	 * @param modifiedBy
	 *            The Id of the user modifying this record
	 */
	public void setModifiedBy(String modifiedBy) {
		if (modifiedBy != null && !modifiedBy.isEmpty()) {
			this.modifiedBy = Integer.valueOf(modifiedBy);
		}
	}

	/**
	 * @return IP Address
	 */
	@Column(name = "IP_ADD")
	public String getIpAdd() {
		return this.ipAdd;
	}

	/**
	 * @param ipAdd
	 *            The IP Address of the client machine
	 */
	public void setIpAdd(String ipAdd) {
		if (ipAdd != null ) {
			this.ipAdd = ipAdd;
		}
		
	}
	public StudyINDIDEBean(){
		
	}
	public StudyINDIDEBean(int idIndIde, String fkStudy, String typeIndIde,
			String numberIndIde, String grantor, String holder,
			String programCode, String expandAccess, String accessType,
			String exemptIndIde, String creator, String modifiedBy, String ipAdd) {

		setIdINDIDE(idIndIde);
		setFkStudy(fkStudy);
		setTypeINDIDE(typeIndIde);
		setNumberINDIDE(numberIndIde);
		setGrantor(grantor);
		setHolder(holder);
		setProgramCode(programCode);
		setExpandAccess(expandAccess);
		setAccessType(accessType);
		setExemptINDIDE(exemptIndIde);
		setCreator(creator);
		setModifiedBy(modifiedBy);
		setIpAdd(ipAdd);
	}

	public int updateIndIdeBean(StudyINDIDEBean indIdeBean) {

		try {
			setFkStudy(indIdeBean.getFkStudy());
			setTypeINDIDE(indIdeBean.getTypeINDIDE());
			setNumberINDIDE(indIdeBean.getNumberINDIDE());
			setGrantor(indIdeBean.getGrantor());
			setHolder(indIdeBean.getHolder());
			setProgramCode(indIdeBean.getProgramCode());
			setExpandAccess(indIdeBean.getExpandAccess());
			setAccessType(indIdeBean.getAccessType());
			setExemptINDIDE(indIdeBean.getExemptINDIDE());
			setCreator(indIdeBean.getCreator());
			setModifiedBy(indIdeBean.getModifiedBy());
			setIpAdd(indIdeBean.getIpAdd());
			Rlog.debug("StudyINDIDE",
					" error in StudyINDIDEBean.updateIndIdeBean");
			return 0;
		} catch (Exception e) {
			Rlog.fatal("StudyINDIDE",
					" error in StudyINDIDEBean.updateIndIdeBean");
			return -2;

		}

	}

}
