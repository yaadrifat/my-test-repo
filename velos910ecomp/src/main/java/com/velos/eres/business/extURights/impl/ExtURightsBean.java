/*
 * Classname			ExtURightsBean.class
 * 
 * Version information
 *
 * Date					03/16/2001
 * 
 * Copyright notice
 */

package com.velos.eres.business.extURights.impl;

/**
 * @ejbHomeJNDIname
 */

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The External User Rights
 * 
 * @author sonia sahni
 */

@Entity
@Table(name = "ER_XUSRIGHT")
public class ExtURightsBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3256718494249924406L;

    /**
     * the primary key of the Group Rights
     */
    public int id;

    /**
     * the Study id
     */
    public Integer studyId;

    /**
     * The user ID
     */
    public Integer userId;

    /**
     * The Rights Grantor
     */

    public Integer grantor;

    /**
     * The Rights
     */

    public Integer extRights;

    public String xStatus;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS

    /**
     * Array List of Feature Sequence
     */
    private ArrayList grSeq;

    /**
     * the Array List of Feature Rights
     */
    private ArrayList ftrRights;

    /**
     * the Array List containing reference name of the feature
     */
    private ArrayList grValue;

    /**
     * the Array List containing user friendly name of the feature
     */
    private ArrayList grDesc;

    /**
     * 
     * 
     * @return
     */
    public ExtURightsBean() {
        grSeq = new ArrayList();
        ftrRights = new ArrayList();
        grValue = new ArrayList();
        grDesc = new ArrayList();
    }

    public ExtURightsBean(int id, String studyId, String userId,
            String grantor, String xStatus, String creator, String modifiedBy,
            String ipAdd, ArrayList grSeq, ArrayList ftrRights,
            ArrayList grValue, ArrayList grDesc) {
        super();
        // TODO Auto-generated constructor stub
        setId(id);
        setStudyId(studyId);
        setUserId(userId);
        setGrantor(grantor);
        // setExtRights(extRights);
        setXStatus(xStatus);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setGrSeq(grSeq);
        setFtrRights(ftrRights);
        setGrValue(grValue);
        setGrDesc(grDesc);
    }

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_XUSRIGHT", allocationSize=1)
    @Column(name = "PK_XUSRIGHT")
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets Study Id
     * 
     * @return
     */
    @Column(name = "FK_STUDY")
    public String getStudyId() {
        return StringUtil.integerToString(this.studyId);
    }

    /**
     * Sets Study Id
     * 
     * 
     */
    public void setStudyId(String st) {
        if (st != null) {
            this.studyId = Integer.valueOf(st);
        }
    }

    /**
     * Get Ext Rights Rights
     * 
     * @return
     */
    @Column(name = "XUSRIGHT")
    public String getExtRights() {
        return StringUtil.integerToString(this.extRights);
    }

    /**
     * Set Ext Rights
     * 
     * @param user
     */
    public void setExtRights(String rights) {
        if (rights != null) {
            this.extRights = Integer.valueOf(rights);
        }

    }

    /**
     * Get User
     * 
     * @return
     */
    @Column(name = "FK_USERX")
    public String getUserId() {
        return StringUtil.integerToString(this.userId);
    }

    /**
     * Set User
     * 
     * @param
     */
    public void setUserId(String usr) {
        if (usr != null) {
            this.userId = Integer.valueOf(usr);
        }

    }

    /**
     * Get Rights Grantor
     * 
     * @return
     */
    @Column(name = "FK_USERGRANTOR")
    public String getGrantor() {
        return StringUtil.integerToString(this.grantor);
    }

    /**
     * Set User
     * 
     * @param
     */
    public void setGrantor(String rgrantor) {
        if (rgrantor != null) {
            this.grantor = Integer.valueOf(rgrantor);
        }

    }

    @Column(name = "XUSRIGHT_STAT")
    public String getXStatus() {
        return this.xStatus;
    }

    public void setXStatus(String xStatus) {
        this.xStatus = xStatus;
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * 
     * 
     * @return
     */
    @Transient
    public ArrayList getGrSeq() {
        return this.grSeq;
    }

    /**
     * 
     * 
     * @param seq
     */
    public void setGrSeq(String seq) {
        grSeq.add(seq);
    }

    /**
     * 
     * 
     * @param seq
     */
    public void setGrSeq(ArrayList seq) {
        this.grSeq = seq;
    }

    /**
     * 
     * 
     * @return
     */
    @Transient
    public ArrayList getFtrRights() {
        return this.ftrRights;
    }

    /**
     * Sets the Rights
     * 
     * @param rights
     */
    public void setFtrRights(String rights) {
        Rlog.debug("exturights", "Setting the ext user Rights in State Keeper"
                + rights);
        try {
            this.ftrRights.add(rights);
        } catch (Exception e) {
            Rlog.fatal("exturights", "EXCEPTION IN String Value Set"
                    + this.ftrRights + "*" + e);
        }
        Rlog.debug("studyrights", "String Value Set" + this.ftrRights);
    }

    /**
     * 
     * 
     * @param rights
     */
    public void setFtrRights(ArrayList rights) {
        Rlog.debug("exturights",
                "Setting the Rights in State Keeper (Array List)" + rights);
        this.ftrRights = rights;
        Rlog.debug("exturights", "Array List Value Set" + this.ftrRights);
    }

    /**
     * 
     * 
     * @return
     */
    @Transient
    public ArrayList getGrValue() {
        return this.grValue;
    }

    /**
     * 
     * 
     * @param val
     */

    public void setGrValue(String val) {
        grValue.add(val);
    }

    /**
     * 
     * 
     * @param val
     */
    public void setGrValue(ArrayList val) {
        this.grValue = val;
    }

    /**
     * 
     * 
     * @return
     */
    @Transient
    public ArrayList getGrDesc() {
        return this.grDesc;
    }

    /**
     * 
     * 
     * @param desc
     */
    public void setGrDesc(String desc) {
        grDesc.add(desc);
    }

    /**
     * 
     * 
     * @param desc
     */
    public void setGrDesc(ArrayList desc) {
        this.grDesc = desc;
    }

    // END OF GETTER SETTER METHODS
    /**
     * returns the state holder object associated with this external user study
     * rights
     * 
     * @returns ExtURightsStateKeeper
     */
    /*
     * public ExtURightsStateKeeper getExtURightsStateKeeper() {
     * Rlog.debug("exturights", "GET EXT USR RIGHTS KEEPER");
     * ExtURightsStateKeeper srk = new ExtURightsStateKeeper();
     * srk.setId(getId()); srk.setStudyId(getStudyId());
     * srk.setUserId(getUserId()); srk.setGrantor(getGrantor());
     * srk.setXStatus(getXStatus()); srk.setCreator(getCreator());
     * srk.setModifiedBy(getModifiedBy()); srk.setIpAdd(getIpAdd());
     * 
     * return srk; }
     */

    /**
     * Updates the EXT User Rights for a user in the Database
     * 
     * @param srk
     */
    /*
     * public void setExtURightsStateKeeper(ExtURightsStateKeeper srk) {
     * 
     * Rlog.debug("exturights", "IN Entity Bean - Ext User STATE HOLDER");
     * GenerateId genId = null;
     * 
     * try { Connection conn = null; conn = getConnection();
     * 
     * Rlog.debug("exturights", "Connection :" + conn); id =
     * genId.getId("SEQ_ER_XUSRIGHT", conn); conn.close();
     * 
     * setStudyId(srk.getStudyId()); // Features Rights are Session Bean
     * setUserId(srk.getUserId()); setGrantor(srk.getGrantor());
     * setXStatus(srk.getXStatus()); setCreator(srk.getCreator());
     * setModifiedBy(srk.getModifiedBy()); setIpAdd(srk.getIpAdd());
     * Rlog.debug("exturights", "GOT EXT USER STUDY Rights ID" + id); } catch
     * (Exception e) { Rlog.fatal("exturights", "EXCEPTION IN ASSOCIATING RIGHTS
     * TO XTERNAL USER" + e); } }
     */

    /**
     * 
     * Updates an existing record
     */

    public int updateExtURights(ExtURightsBean srk) {

        try {
            setStudyId(srk.getStudyId()); // Features Rights are Session Bean
            setUserId(srk.getUserId());
            setGrantor(srk.getGrantor());
            setExtRights(srk.getExtRights());
            setXStatus(srk.getXStatus());
            setCreator(srk.getCreator());
            setModifiedBy(srk.getModifiedBy());
            setIpAdd(srk.getIpAdd());
            Rlog.debug("exturights", "GOT EXT USER STUDY Rights ID" + id);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("exturights",
                    "EXCEPTION IN ASSOCIATING RIGHTS TO XTERNAL USER" + e);
            return -2;
        }

    }

}// end of class

