/*
 * Classname : FormQueryBean
 * 
 * Version information: 1.0
 *
 * Date: 01/03/2005
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.business.formQuery.impl;

/* Import Statements */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The StudySite CMP entity bean.<br>
 * <br>
 * 
 * @author Anu
 * @vesion 1.0 08/09/2001
 * @ejbHome FormQueryHome
 * @ejbRemote FormQueryRObj
 */

@Entity
@Table(name = "er_formquery")
public class FormQueryBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3689634704928290354L;

    /**
     * the form query Id
     */
    public int formQueryId;

    /**
     * the query module Id
     */
    public Integer queryModuleId;

    /**
     * the query module linked to
     */

    public Integer queryModuleLnkTo;

    /**
     * the field id
     */

    public Integer fieldId;

    /**
     * the query name
     */

    public String queryName;

    /**
     * the query desc
     */

    public String queryDesc;

    /**
     * creator
     */
    public Integer creator;

    /**
     * last modified by
     */
    public Integer modifiedBy;

    /**
     * IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS

    /**
     * 
     * Get form query Id
     * 
     * @return the formQueryId
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "seq_er_form_query", allocationSize=1)
    @Column(name = "PK_FORMQUERY")
    public int getFormQueryId() {
        return this.formQueryId;
    }

    /**
     * 
     * Get form query Id
     * 
     * @return the formQueryId
     */
    public void setFormQueryId(int id) {
        this.formQueryId = id;
    }

    /**
     * Get query Module Id
     * 
     * @return queryModuleId
     */

    @Column(name = "FK_QUERYMODULE")
    public String getQueryModuleId() {
        return StringUtil.integerToString(this.queryModuleId);
    }

    /**
     * sets query Module Id
     * 
     * @param queryModuleId
     */
    public void setQueryModuleId(String queryModuleId) {
        this.queryModuleId = StringUtil.stringToInteger(queryModuleId);
    }

    /**
     * Get query Module Linked To
     * 
     * @return queryModuleLnkTo
     */

    @Column(name = "QUERYMODULE_LINKEDTO")
    public String getQueryModuleLnkTo() {
        return StringUtil.integerToString(this.queryModuleLnkTo);
    }

    /**
     * sets query Module Linked To
     * 
     * @param queryModuleLnkTo
     */
    public void setQueryModuleLnkTo(String queryModuleLnkTo) {
        this.queryModuleLnkTo = StringUtil.stringToInteger(queryModuleLnkTo);
    }

    /**
     * Get field Id
     * 
     * @return fieldId
     */
    @Column(name = "FK_FIELD")
    public String getFieldId() {
        return StringUtil.integerToString(this.fieldId);
    }

    /**
     * sets Field Id
     * 
     * @param fieldId
     */
    public void setFieldId(String fieldId) {
        this.fieldId = StringUtil.stringToInteger(fieldId);
    }

    /**
     * Get query Name
     * 
     * @return String queryName
     */
    @Column(name = "QUERY_NAME")
    public String getQueryName() {
        return this.queryName;
    }

    /**
     * sets queryName
     * 
     * @param queryName
     */
    public void setQueryName(String queryName) {
        this.queryName = queryName;
    }

    /**
     * Get query Description
     * 
     * @return String queryDesc
     */
    @Column(name = "QUERY_DESC")
    public String getQueryDesc() {
        return this.queryDesc;
    }

    /**
     * sets queryDesc
     * 
     * @param queryDesc
     */
    public void setQueryDesc(String queryDesc) {
        this.queryDesc = queryDesc;
    }

    /**
     * Get Status creator
     * 
     * @return The Id of the creator of the record
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {

        this.creator = StringUtil.stringToInteger(creator);
    }

    /**
     * Get Status modified by
     * 
     * @return The Id of the user modifying this record
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = StringUtil.stringToInteger(modifiedBy);
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /**
     * @return the state holder object associated with this status
     */

    /*
     * public FormQueryStateKeeper getFormQueryStateKeeper() {
     * 
     * return new FormQueryStateKeeper(getFormQueryId(), getQueryModuleId(),
     * getQueryModuleLnkTo(), getFieldId(), getQueryName(), getQueryDesc(),
     * getCreator(), getModifiedBy(), getIpAdd()); }
     */

    /**
     * sets up a state keeper containing details of the form query
     * 
     * @param fqk
     *            FormQueryStateKeeper
     */

    /*
     * public void setFormQueryStateKeeper(FormQueryStateKeeper fqk) {
     * GenerateId formQueryId = null;
     * 
     * try { Connection conn = null; conn = getConnection();
     * 
     * this.formQueryId = formQueryId.getId("SEQ_ER_FORM_QUERY", conn);
     * conn.close(); setQueryModuleId(fqk.getQueryModuleId());
     * setQueryModuleLnkTo(fqk.getQueryModuleLnkTo());
     * setFieldId(fqk.getFieldId()); setQueryName(fqk.getQueryName());
     * setQueryDesc(fqk.getQueryDesc()); setCreator(fqk.getCreator());
     * setModifiedBy(fqk.getModifiedBy()); setIpAdd(fqk.getIpAdd()); } catch
     * (Exception e) { Rlog.fatal("formQuery", "Exception in
     * FormQueryBean.setFormQueryStateKeeper" + e); } }
     */

    /**
     * updates the from query details
     * 
     * @param fqk
     *            FormQueryStateKeeper
     * @return 0 if successful; -2 for exception
     */
    public int updateFormQuery(FormQueryBean fqk) {
        try {

            setQueryModuleId(fqk.getQueryModuleId());
            setQueryModuleLnkTo(fqk.getQueryModuleLnkTo());
            setFieldId(fqk.getFieldId());
            setQueryName(fqk.getQueryName());
            setQueryDesc(fqk.getQueryDesc());
            setCreator(fqk.getCreator());
            setModifiedBy(fqk.getModifiedBy());
            setIpAdd(fqk.getIpAdd());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("fromQuery",
                    "Exception in  FormQueryBean.updateFormQuery" + e);
            return -2;
        }
    }

    public FormQueryBean() {
    }

    public FormQueryBean(int formQueryId, String queryModuleId,
            String queryModuleLnkTo, String fieldId, String queryName,
            String queryDesc, String creator, String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setFormQueryId(formQueryId);
        setQueryModuleId(queryModuleId);
        setQueryModuleLnkTo(queryModuleLnkTo);
        setFieldId(fieldId);
        setQueryName(queryName);
        setQueryDesc(queryDesc);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

}// end of class
