package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

public class StudySiteApndxDao extends CommonDAO implements
        java.io.Serializable {
    private ArrayList studySiteApndxIds;

    private ArrayList studySiteIds;

    private ArrayList studySitesApndxNames;

    private ArrayList appendixdescriptions;

    private ArrayList appendixtypes;

    private int cRows;

    public StudySiteApndxDao() {
        studySiteApndxIds = new ArrayList();
        studySiteIds = new ArrayList();
        studySitesApndxNames = new ArrayList();
        appendixdescriptions = new ArrayList();
        appendixtypes = new ArrayList();
    }

    // Getter and Setter methods
    public ArrayList getStudySiteApndxIds() {
        return this.studySiteApndxIds;
    }

    public void setStudySiteApndxIds(ArrayList studySiteApndxIds) {
        this.studySiteApndxIds = studySiteApndxIds;
    }

    public ArrayList getStudySiteIds() {
        return this.studySiteIds;
    }

    public void setStudySiteIds(ArrayList studySiteIds) {
        this.studySiteIds = studySiteIds;
    }

    public ArrayList getAppendixTypes() {
        return this.appendixtypes;
    }

    public void setAppendixTypes(ArrayList appendixtypes) {
        this.appendixtypes = appendixtypes;
    }

    public ArrayList getAppendixDescriptions() {
        return this.appendixdescriptions;
    }

    public void setAppendixDescriptions(ArrayList appendixdescriptions) {
        this.appendixdescriptions = appendixdescriptions;
    }

    public ArrayList getAppendixNames() {
        return this.studySitesApndxNames;
    }

    public void setAppendixNames(ArrayList studySitesApndxNames) {
        this.studySitesApndxNames = studySitesApndxNames;
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public void setStudySiteApndxIds(Integer studySiteApndxId) {
        studySiteApndxIds.add(studySiteApndxId);
    }

    public void setStudySiteIds(String studySiteId) {
        studySiteIds.add(studySiteId);
    }

    public void setAppendixTypes(String appendixtype) {
        appendixtypes.add(appendixtype);
    }

    public void setAppendixNames(String studySitesApndxName) {
        studySitesApndxNames.add(studySitesApndxName);
    }

    public void setAppendixDescriptions(String appendixdescription) {
        appendixdescriptions.add(appendixdescription);
    }

    // end of getter and setter methods

    /**
     * Method to get the name, desc and pk of studysite apndx for a studySite
     * and type('U'-url, 'F'-file)
     * 
     * @param pk
     *            of study Site
     * @param Type :
     *            U for URL, F for File
     */

    public void getStudySitesApndxValues(int studySiteId, String type) {

        Rlog.debug("studySiteApndx", "Value of studySiteId " + studySiteId);
        Rlog.debug("studySiteApndx", "Value of type " + type);

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("select PK_STUDYSITES_APNDX,  APNDX_NAME,APNDX_DESCRIPTION from er_studysites_apndx where FK_STUDYSITES= ? and APNDX_TYPE= ? ");

            pstmt.setInt(1, studySiteId);
            pstmt.setString(2, type);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setStudySiteApndxIds(new Integer(rs
                        .getInt("PK_STUDYSITES_APNDX")));
                setAppendixNames(rs.getString("APNDX_NAME"));
                setAppendixDescriptions(rs.getString("APNDX_DESCRIPTION"));
                rows++;
                Rlog.debug("studySiteApndx",
                        "StudySiteApndxDao.getStudySitesApndxValues row# "
                                + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "studySiteApndx",
                            "StudySiteApndxDao.getStudySitesApndxValues EXCEPTION IN FETCHING FROM Appendix table"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

}
