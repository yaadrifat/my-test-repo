package com.velos.eres.business.memberRights.impl;

import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.Rlog;

@Entity
@Table(name = "er_review_board_member")
public class MemberRightsBean implements Serializable {
	
	private static final long serialVersionUID = 3256999951945709107L;

    public int id;
    
    public String memRights;

    private ArrayList grSeq;

    private ArrayList ftrRights;

    private ArrayList grValue;

    private ArrayList grDesc;
    
    private String ipAdd;
	
	private String creator;
	
	private String modifiedBy;

	    // GETTER SETTER METHODS
	    
    @Column(name = "PK_REVIEW_BOARD_MEMBER")
    @Id
    public int getId() {
        return this.id;
    }
    public void setId(int id) {
        this.id = id;
    }
   	  
    @Column(name = "BOARD_MEMBER_RIGHTS")
    public String getMemRights() {
        return this.memRights;
    }
    public void setMemRights(String rights) {
        if (rights != null) {
            this.memRights = rights;
        }
    }
   
    
    @Transient
    public ArrayList getGrSeq() {
        return this.grSeq;
    }
    public void setGrSeq(String seq) {
        grSeq.add(seq);
    }	    
    public void setGrSeq(ArrayList seq) {
        this.grSeq = seq;
    }
    
    @Transient
    public ArrayList getFtrRights() {
        return this.ftrRights;
    }
    public void setFtrRights(String rights) {
        Rlog.debug("memberRights", "Setting the Rights in State Keeper" + rights);
        try {
            this.ftrRights.add(rights);
        } catch (Exception e) {
            Rlog.fatal("memberRights", "EXCEPTION IN String Value Set"+ this.ftrRights + "*" + e);
        }
        Rlog.debug("memberRights", "String Value Set" + this.ftrRights);
    }
    public void setFtrRights(ArrayList rights) {
        Rlog.debug("memberRights","Setting the Rights in State Keeper (Array List)" + rights);
        this.ftrRights = rights;
        Rlog.debug("memberRights", "Array List Value Set" + this.ftrRights);
    }

    @Transient
    public ArrayList getGrValue() {
        return this.grValue;
    }
    public void setGrValue(String val) {
        grValue.add(val);
    }
    public void setGrValue(ArrayList val) {
        this.grValue = val;
    }


    @Transient
    public ArrayList getGrDesc() {
        return this.grDesc;
    } 
    public void setGrDesc(String desc) {
        grDesc.add(desc);
    }
    public void setGrDesc(ArrayList desc) {
        this.grDesc = desc;
    }
    
    
    @Column(name = "IP_ADD")
    public String getIpAdd() {
		return ipAdd;
	}
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
	
	@Column(name = "CREATOR")
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	
	@Column(name = "LAST_MODIFIED_BY")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
    
    //End Of Getter/Setter methods

    public MemberRightsBean() {
        grSeq = new ArrayList();
        ftrRights = new ArrayList();
        grValue = new ArrayList();
        grDesc = new ArrayList();
    }	   
    
    public MemberRightsBean(int id,ArrayList grSeqList,ArrayList ftrRightList, ArrayList grValueList,ArrayList grDescList,
    		String ipAdd,String creator,String modifiedBy)
    {
        super();	       
        grSeq = new ArrayList();
        ftrRights = new ArrayList();
        grValue = new ArrayList();
        grDesc = new ArrayList();	       
        setId(id);	        
        setMemRights(memRights);
        
        if(grSeqList!=null)
        this.setGrSeq(grSeqList);
        
        if(ftrRightList!=null)
        this.setFtrRights(ftrRightList);
        
        if(grValueList!=null)
        this.setGrValue(grValueList);
        
        if(grValueList!=null)
        this.setGrDesc(grDescList);
        
        setIpAdd(ipAdd);
		setCreator(creator);
		setModifiedBy(modifiedBy);
    }
    
    public int updateMemberRights(MemberRightsBean mrk, String rights) 
    {	    	
        Rlog.debug("memberRights", "IN study Rights STATE HOLDER");
        try 
        {
        	setId(mrk.getId());
        	setMemRights(rights);
			setModifiedBy(mrk.getModifiedBy());
            Rlog.debug("memberRights", "UPDATED");
            return 0;
        } 
        catch (Exception e) 
        {	
            Rlog.fatal("memberRights", "EXCEPTION IN UPDATING MEMBER RIGHTS " + e);
            return -2;
        }
    }
}