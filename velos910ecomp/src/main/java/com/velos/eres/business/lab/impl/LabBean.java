/*
 * Classname : LabBean
 *
 * Version information: 1.0
 *
 * Date: 09/28/2003
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Vishal Abrol
 */

package com.velos.eres.business.lab.impl;

/* Import Statements */
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The LabBean CMP entity bean.<br>
 * <br>
 *
 * @author vishal abrol
 * @vesion 1.0 09/28/2003
 * @ejbHome LabHome
 * @ejbRemote LabRObj
 */
@Entity
@Table(name = "ER_PATLABS")
public class LabBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3544670663844377397L;

    /**
     * The primary key
     */

    /**
     * The primary key:pk_patlabs
     */

    public int id;

    /**
     * The foreign key reference to labs lookup table.
     */
    public Integer testId;

    /**
     * The foreign key reference to table person
     */
    public Integer perId;

    /**
     * Test Date
     */
    public java.util.Date testDate;

    /**
     * Test Result
     */

    public String testResult;

    /**
     * The Test Type
     */
    public String testType;

    /**
     * The Test unit
     */
    public String testUnit;

    /**
     * The Test Range
     */
    public String testRange;

    /**
     * The Test LLN
     */
    public String testLLN;

    /**
     * The Test ULN
     */
    public String testULN;

    /**
     * The Foreing key for er_site
     */

    public Integer siteId;

    /**
     * The Test Status
     */

    public Integer testStatus;

    /**
     * The Accession number
     */

    public String accnNum;

    /**
     * The TestGroup
     */

    public Integer testgroupId;

    /**
     * The TestGroup
     */

    public Integer abnrFlag;

    /**
     * The foreign key for table er_patprot
     */

    public Integer patprotId;

    /**
     * The foreign key for table sch_events
     */

    public Integer eventId;

    /**
     * The creator
     */

    public Integer creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    public Integer modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    public String ipAdd;

    public String erName;

    public Integer erGrade;

    public Integer stdPhase;

    public Integer studyId;

    public Integer specimenId;

    // /////////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_PATLABS", allocationSize=1)
    @Column(name = "PK_PATLABS")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "FK_TESTID")
    public String getTestId() {
        return StringUtil.integerToString(testId);
    }

    public void setTestId(String testId) {
        if (!StringUtil.isEmpty(testId))
            this.testId = Integer.valueOf(testId);

    }

    @Column(name = "FK_PER")
    public String getPerId() {
        return StringUtil.integerToString(perId);
    }

    public void setPerId(String perId) {
        if (!StringUtil.isEmpty(perId))
            this.perId = Integer.valueOf(perId);
    }

    @Column(name = "TEST_DATE")
    public Date getTestDt() {
        return this.testDate;
    }

    public void setTestDt(Date testDate) {

        this.testDate = testDate;
    }

    @Transient
    public String getTestDate() {
        return DateUtil.dateToString(getTestDt());
    }

    public void setTestDate(String testDate) {
        setTestDt(DateUtil.stringToDate(testDate, null));
    }

    @Column(name = "TEST_RESULT")
    public String getTestResult() {
        return testResult;
    }

    public void setTestResult(String testResult) {
        this.testResult = testResult;
    }

    @Column(name = "TEST_TYPE")
    public String getTestType() {
        return testType;
    }

    public void setTestType(String testType) {

        this.testType = testType;

    }

    @Column(name = "TEST_UNIT")
    public String getTestUnit() {
        return testUnit;
    }

    public void setTestUnit(String testUnit) {
        this.testUnit = testUnit;
    }

    @Column(name = "TEST_RANGE")
    public String getTestRange() {
        return testRange;
    }

    public void setTestRange(String testRange) {
        this.testRange = testRange;
    }

    @Column(name = "TEST_LLN")
    public String getTestLLN() {
        return testLLN;
    }

    public void setTestLLN(String testLLN) {

        this.testLLN = testLLN;

    }

    @Column(name = "TEST_ULN")
    public String getTestULN() {
        return testULN;
    }

    public void setTestULN(String testULN) {

        this.testULN = testULN;

    }

    @Column(name = "FK_SITE")
    public String getSiteId() {
        return StringUtil.integerToString(this.siteId);
    }

    public void setSiteId(String siteId) {
        if (!StringUtil.isEmpty(siteId))
            this.siteId = Integer.valueOf(siteId);
    }

    @Column(name = "FK_CODELST_TSTSTAT")
    public String getTestStatus() {
        return StringUtil.integerToString(testStatus);
    }

    public void setTestStatus(String testStatus) {
        if (!StringUtil.isEmpty(testStatus))
            this.testStatus = Integer.valueOf(testStatus);

    }

    @Column(name = "ACCESSION_NUM")
    public String getAccnNum() {
        return accnNum;
    }

    public void setAccnNum(String accnNum) {
        this.accnNum = accnNum;
    }

    @Column(name = "FK_TESTGROUP")
    public String getTestgroupId() {
        return StringUtil.integerToString(testgroupId);
    }

    public void setTestgroupId(String testgroupId) {
        if (!StringUtil.isEmpty(testgroupId))
            this.testgroupId = Integer.valueOf(testgroupId);

    }

    @Column(name = "FK_CODELST_ABFLAG")
    public String getAbnrFlag() {
        return StringUtil.integerToString(abnrFlag);
    }

    public void setAbnrFlag(String abnrFlag) {
        if (!StringUtil.isEmpty(abnrFlag))
            this.abnrFlag = Integer.valueOf(abnrFlag);

    }

    @Column(name = "FK_PATPROT")
    public String getPatprotId() {
        return StringUtil.integerToString(patprotId);
    }

    public void setPatprotId(String patprotId) {
        if (!StringUtil.isEmpty(patprotId))
            this.patprotId = Integer.valueOf(patprotId);
    }

    @Column(name = "FK_EVENT_ID")
    public String getEventId() {
        return StringUtil.integerToString(eventId);
    }

    public void setEventId(String eventId) {
        if (!StringUtil.isEmpty(eventId))
            this.eventId = Integer.valueOf(eventId);
    }

    /**
     * @return the Creator
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);

    }

    /**
     * @param takes
     *            a string
     */

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }

    }

    /**
     * @return the unique ID of the User
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {

        return StringUtil.integerToString(this.modifiedBy);

    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {

        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }

    }

    /**
     * @return the IP Address of the Form Notification request
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Notification request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Column(name = "ER_Name")
    public String getErName() {
        return erName;
    }

    public void setErName(String erName) {
        if (erName != null)
            this.erName = erName;
    }

    @Column(name = "ER_GRADE")
    public String getErGrade() {
        return StringUtil.integerToString(erGrade);
    }

    public void setErGrade(String erGrade) {
        if (!StringUtil.isEmpty(erGrade))
            this.erGrade = Integer.valueOf(erGrade);
        else
            this.erGrade = null;
    }

    @Column(name = "FK_CODELST_STDPHASE")
    public String getStdPhase() {
        return StringUtil.integerToString(stdPhase);
    }

    public void setStdPhase(String stdPhase) {
        if (!StringUtil.isEmpty(stdPhase))
            this.stdPhase = Integer.valueOf(stdPhase);
        else
            this.stdPhase = null;
    }

    @Column(name = "FK_STUDY")
    public String getStudyId() {
        return StringUtil.integerToString(studyId);
    }

    public void setStudyId(String studyId) {
        if (!StringUtil.isEmpty(studyId))
            this.studyId = Integer.valueOf(studyId);
        else
            this.studyId = null;
    }

  //JM: 07Jul2009: #INVP2.11
    @Column(name = "FK_SPECIMEN")
    public String getSpecimenId() {
        return StringUtil.integerToString(specimenId);
    }

    public void setSpecimenId(String specimenId) {
        if (!StringUtil.isEmpty(specimenId))
            this.specimenId = Integer.valueOf(specimenId);
        else
            this.specimenId = null;
    }

    // END OF THE GETTER AND SETTER METHODS

    // ////////////////////////////////////////////////////////////////////

    /*
     * public LabStateKeeper getLabStateKeeper() { Rlog.debug("lab",
     * "LabBean.getLabStateKeeper");
     *
     * return new LabStateKeeper(getId(), getTestId(), getPerId(),
     * getTestDate(), getTestResult(), getTestType(), getTestUnit(),
     * getTestRange(), getTestLLN(), getTestULN(), getSiteId(), getTestStatus(),
     * getAccnNum(), getTestgroupId(), getAbnrFlag(), getPatprotId(),
     * getEventId(), getCreator(), getModifiedBy(), getIpAdd(), getErName(),
     * getErGrade(), getStdPhase(), getStudyId()); }
     */

    /**
     * sets up a state keeper containing details of the study id
     */

    /*
     * public void setLabStateKeeper(LabStateKeeper lsk) { GenerateId stId =
     * null; try { Connection conn = null; conn = getConnection();
     * Rlog.debug("lab", "LabBean.setLabStateKeeper() LABSTATEKEEPER :" + lsk);
     * Rlog .debug("lab", "LabBean.setLabStateKeeper() Connection :" + conn);
     *
     * this.id = stId.getId("SEQ_ER_PATLABS", conn); Rlog.debug("lab",
     * "LabBean.Id " + this.id); conn.close();
     *
     * setTestId(lsk.getTestId()); Rlog.debug("lab", "LabBean.testid() " +
     * lsk.getTestId()); setPerId(lsk.getPerId()); Rlog.debug("lab",
     * "LabBean.getPerId() " + lsk.getPerId()); setTestDate(lsk.getTestDate());
     *
     * Rlog.debug("lab", "LabBean.getTestDate() " + lsk.getTestDate());
     *
     * setTestResult(lsk.getTestResult());
     *
     * Rlog.debug("lab", "LabBean.getTestResult()() " + lsk.getTestResult());
     *
     * setTestType(lsk.getTestType());
     *
     * Rlog.debug("lab", "LabBean.getTestType() " + lsk.getTestType());
     *
     * setTestUnit(lsk.getTestUnit());
     *
     * Rlog.debug("lab", "LabBean.getTestUnit() " + lsk.getTestUnit());
     *
     * setTestRange(lsk.getTestRange());
     *
     * Rlog.debug("lab", "LabBean.getTestRange() " + lsk.getTestRange());
     *
     * setTestLLN(lsk.getTestLLN());
     *
     * Rlog.debug("lab", "LabBean.getTestLLN() " + lsk.getTestLLN());
     *
     * setTestULN(lsk.getTestULN());
     *
     * Rlog.debug("lab", "LabBean.getTestULN() " + lsk.getTestULN());
     *
     * setSiteId(lsk.getSiteId()); Rlog.debug("lab", "LabBean.getSiteId() " +
     * lsk.getSiteId());
     *
     * setTestStatus(lsk.getTestStatus()); Rlog.debug("lab",
     * "LabBean.getTestStatus() " + lsk.getTestStatus());
     *
     * setAccnNum(lsk.getAccnNum()); Rlog.debug("lab", "LabBean.getAccnNum() " +
     * lsk.getAccnNum());
     *
     * setTestgroupId(lsk.getTestgroupId()); Rlog.debug("lab",
     * "LabBean.getTestgroupId() " + lsk.getTestgroupId());
     *
     * setAbnrFlag(lsk.getAbnrFlag()); Rlog.debug("lab", "LabBean.getAbnrFlag() " +
     * lsk.getAbnrFlag());
     *
     * setPatprotId(lsk.getPatprotId()); Rlog.debug("lab",
     * "LabBean.getPatprotId() " + lsk.getPatprotId());
     *
     * setEventId(lsk.getEventId());
     *
     * Rlog.debug("lab", "LabBean.getEventId() " + lsk.getEventId());
     *
     * setCreator(lsk.getCreator()); Rlog.debug("lab", "LabBean.getCreator() " +
     * lsk.getCreator());
     *
     * setIpAdd(lsk.getIpAdd()); Rlog.debug("lab", "LabBean.getIpAdd() " +
     * lsk.getIpAdd());
     *
     * setErName(lsk.getErName()); Rlog.debug("lab", "LabBean.getErName() " +
     * lsk.getErName());
     *
     * setErGrade(lsk.getErGrade()); Rlog.debug("lab", "LabBean.getErGrade() " +
     * lsk.getErGrade());
     *
     * setStdPhase(lsk.getStdPhase()); Rlog.debug("lab", "LabBean.getstdPhase() " +
     * lsk.getStdPhase());
     *
     * setStudyId(lsk.getStudyId()); Rlog.debug("lab", "LabBean.getstudyId() " +
     * lsk.getStudyId()); } catch (Exception e) { Rlog.fatal("lab", "Error in
     * setLabStateKeeper() in LabBean " + e); } }
     */

    /**
     * updates the Form studyId Record
     *
     * @param Labssk
     * @return int 0: in case of successful updation; -2 in caseof exception
     */
    public int updateLab(LabBean lsk) {
        try {
            setPerId(lsk.getPerId());
            setTestDate(lsk.getTestDate());
            setTestResult(lsk.getTestResult());
            setTestType(lsk.getTestType());
            setTestUnit(lsk.getTestUnit());
            setTestRange(lsk.getTestRange());
            setTestLLN(lsk.getTestLLN());
            setTestULN(lsk.getTestULN());
            setSiteId(lsk.getSiteId());
            setTestStatus(lsk.getTestStatus());
            setAccnNum(lsk.getAccnNum());
            setTestgroupId(lsk.getTestgroupId());
            setAbnrFlag(lsk.getAbnrFlag());
            setPatprotId(lsk.getPatprotId());
            setEventId(lsk.getEventId());
            setModifiedBy(lsk.getModifiedBy());
            setIpAdd(lsk.getIpAdd());
            setErName(lsk.getErName());
            setErGrade(lsk.getErGrade());
            setStdPhase(lsk.getStdPhase());
            setStudyId(lsk.getStudyId());
            setCreator(lsk.getCreator());
            setSpecimenId(lsk.getSpecimenId());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("lab", " error in labBean.updateLab" + e);
            return -2;
        }
    }

    public LabBean() {

    }

    public LabBean(int id, String testId, String perId, String testDate,
            String testResult, String testType, String testUnit,
            String testRange, String testLLN, String testULN, String siteId,
            String testStatus, String accnNum, String testgroupId,
            String abnrFlag, String patprotId, String eventId, String creator,
            String modifiedBy, String ipAdd, String erName, String erGrade,
            String stdPhase, String studyId, String specimenId) {
        super();
        // TODO Auto-generated constructor stub
        setId(id);
        setTestId(testId);
        setPerId(perId);
        setTestDate(testDate);
        setTestResult(testResult);
        setTestType(testType);
        setTestUnit(testUnit);
        setTestRange(testRange);
        setTestLLN(testLLN);
        setTestULN(testULN);
        setSiteId(siteId);
        setTestStatus(testStatus);
        setAccnNum(accnNum);
        setTestgroupId(testgroupId);
        setAbnrFlag(abnrFlag);
        setPatprotId(patprotId);
        setEventId(eventId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setErName(erName);
        setErGrade(erGrade);
        setStdPhase(stdPhase);
        setStudyId(studyId);
        setSpecimenId(specimenId);
    }

}
