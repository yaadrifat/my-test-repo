package com.velos.eres.business.submission.impl;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @ejbHomeJNDIname ejb.ReviewMeetingTopic
 */

@Entity
@Table(name = "er_review_meeting_topic")
@NamedQueries( {
    @NamedQuery(name = "findByFkReviewMeeting",
            query = "SELECT OBJECT(t) FROM ReviewMeetingTopicBean t where "+
            " t.fkReviewMeeting = :fkReviewMeeting "),
    @NamedQuery(name = "findByFkReviewMeetingAndTopicNumber",
            query = "SELECT OBJECT(t) FROM ReviewMeetingTopicBean t where "+
            " t.fkReviewMeeting = :fkReviewMeeting and t.topicNumber = :topicNumber ")
})
public class ReviewMeetingTopicBean implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Integer id;
    private Integer fkReviewMeeting;
    private String  topicNumber;
    private String  meetingTopic;
    private Integer creator;
    private Integer lastModifiedBy;
    private Date    lastModifiedDate;
    private String  ipAdd;
    
    public ReviewMeetingTopicBean() {}
    
    public ReviewMeetingTopicBean(Integer id, Integer fkReviewMeeting,
            String topicNumber, String meetingTopic, 
            Integer creator, Integer lastModifiedBy, String ipAdd) {
        setId(id);
        setFkReviewMeeting(fkReviewMeeting);
        setTopicNumber(topicNumber);
        setMeetingTopic(meetingTopic);
        setCreator(creator);
        setLastModifiedBy(lastModifiedBy);
        setIpAdd(ipAdd);
    }
    
    public void setDetails(ReviewMeetingTopicBean anotherBean) {
        setFkReviewMeeting(anotherBean.getFkReviewMeeting());
        setTopicNumber(anotherBean.getTopicNumber());
        setMeetingTopic(anotherBean.getMeetingTopic());
        setCreator(anotherBean.getCreator());
        setLastModifiedBy(anotherBean.getLastModifiedBy());
        setIpAdd(anotherBean.getIpAdd());
    }
  
    
    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_REVIEW_MEETING_TOPIC", allocationSize=1)
    @Column(name = "PK_REVIEW_MEETING_TOPIC")
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "FK_REVIEW_MEETING")
    public Integer getFkReviewMeeting() {
        return fkReviewMeeting;
    }

    public void setFkReviewMeeting(Integer fkReviewMeeting) {
        this.fkReviewMeeting = fkReviewMeeting;
    }

    @Column(name = "TOPIC_NUMBER")
    public String getTopicNumber() {
        return topicNumber;
    }

    public void setTopicNumber(String topicNumber) {
        this.topicNumber = topicNumber;
    }

    @Column(name = "MEETING_TOPIC")
    public String getMeetingTopic() {
        return meetingTopic;
    }

    public void setMeetingTopic(String meetingTopic) {
        this.meetingTopic = meetingTopic;
    }

    @Column(name = "CREATOR")
    public Integer getCreator() {
        return creator;
    }

    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    @Column(name = "LAST_MODIFIED_DATE")
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Column(name = "LAST_MODIFIED_BY")
    public Integer getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(Integer lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    
}
