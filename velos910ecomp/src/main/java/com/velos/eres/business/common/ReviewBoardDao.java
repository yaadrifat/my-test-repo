package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

public class ReviewBoardDao extends CommonDAO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private ArrayList rbIds;	

	private ArrayList rbNames;

	private ArrayList rbDescriptions;
	
	private ArrayList rbAccIds;

	private ArrayList rbAccess;

	private ArrayList rbMomLogic;

	private ArrayList rbDefault;

	private int cRows;

	public ReviewBoardDao()
	{
		
		rbIds = new ArrayList();
		
		rbNames = new ArrayList();
		
		rbDescriptions = new ArrayList();
		
		rbAccIds = new ArrayList();
		
		rbAccess = new ArrayList();
		
		rbMomLogic = new ArrayList();
		
		rbDefault = new ArrayList();
	}

	// Getter and Setter methods
	
	public ArrayList getRbIds() {
		Rlog.debug("reviewboard", "ReviewBoardDao.getRbIds size "+ this.rbIds.size());
		return this.rbIds;
	}	
	public void setRbIds(ArrayList rbIds) {
		this.rbIds = rbIds;
	}
	public void setRbIds(int rbId) {
		this.rbIds.add(rbId);
	}

	
	
	public ArrayList getRbNames() {
		Rlog.debug("reviewboard", "ReviewBoardDao.getRbNames size "	+ this.rbNames.size());
		return this.rbNames;
	}	
	public void setRbNames(ArrayList rbNames) {
		this.rbNames = rbNames;
	}
	public void setRbNames(String rbName) {
		this.rbNames.add(rbName);
	}
	

	public ArrayList getRbDescriptions() {
		return rbDescriptions;
	}
	public void setRbDescriptions(ArrayList rbDescriptions) {
		this.rbDescriptions = rbDescriptions;
	}
	public void setRbDescriptions(String rbDescription) {
		this.rbDescriptions.add(rbDescription);
	}
	

	public ArrayList getRbAccIds() {
		Rlog.debug("reviewboard", "ReviewBoardDao.getRbAccIds size "+ this.rbAccIds.size());
		return this.rbAccIds;
	}	
	public void setRbAccIds(ArrayList rbAccIds) {
		this.rbAccIds = rbAccIds;
	}
	public void setRbAccIds(int rbAccId) {
		this.rbAccIds.add(rbAccId);
	}

	
	public ArrayList getRbAccess() {
		Rlog.debug("reviewboard", "ReviewBoardDao.getRbAcces size "	+ this.rbAccess.size());
		return this.rbAccess;
	}	
	public void setRbAccess(ArrayList rbAccess) {
		this.rbAccess = rbAccess;
	}
	public void setRbAccess(String rbAcces) {
		this.rbAccess.add(rbAcces);
	}

	
	public ArrayList getRbMomLogic() {
		Rlog.debug("reviewboard", "ReviewBoardDao.getRbMomLogic size "+ this.rbMomLogic.size());
		return this.rbMomLogic;
	}	
	public void setRbMomLogic(ArrayList rbMomLogic) {
		this.rbMomLogic = rbMomLogic;
	}
	public void setRbMomLogic(String rbMomLogi) {
		this.rbMomLogic.add(rbMomLogi);
	}

	
	public ArrayList getRbDefault() {
		Rlog.debug("reviewboard", "ReviewBoardDao.getRbDefault size "+ this.rbDefault.size());
		return this.rbDefault;
	}	
	public void setRbDefault(ArrayList rbDefault) {
		this.rbDefault = rbDefault;
	}
	public void setRbDefault(int rbDefaul) {
		this.rbDefault.add(rbDefaul);
	}

	
	public int getcRows() {
		return this.cRows;
	}	
	public void setcRows(int cRows) {
		this.cRows = cRows;
	}
	// end of getter and setter methods

	public void getReviewBoardValues(int rbId) {
		PreparedStatement pstmt = null;
		Connection conn = null;		
		try {
			conn = getConnection();	
			String Query = "Select PK_REVIEW_BOARD,REVIEW_BOARD_NAME,REVIEW_BOARD_DESCRIPTION,FK_ACCOUNT,BOARD_GROUP_ACCESS,BOARD_MOM_LOGIC," 
							+ "REVIEW_BOARD_DEFAULT from er_review_board where PK_REVIEW_BOARD=? order by REVIEW_BOARD_NAME";
			pstmt = conn.prepareStatement(Query);			
			pstmt.setInt(1, rbId);
			ResultSet rs = pstmt.executeQuery();			
			while (rs.next()) {				
				setRbIds(new Integer(rs.getInt("PK_REVIEW_BOARD")));				
				setRbNames(rs.getString("REVIEW_BOARD_NAME"));
				setRbDescriptions(rs.getString("REVIEW_BOARD_DESCRIPTION"));
				setRbAccIds(new Integer(rs.getString("FK_ACCOUNT")));
				setRbAccess(rs.getString("BOARD_GROUP_ACCESS"));
				setRbMomLogic(rs.getString("BOARD_MOM_LOGIC"));
				setRbDefault(new Integer(rs.getInt("REVIEW_BOARD_DEFAULT")));			
			}

		} catch (SQLException ex) {
			Rlog.fatal("reviewboard","ReviewBoardDao.getReviewBoardValues EXCEPTION IN FETCHING FROM Review Board table"+ ex);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (Exception e) {
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
			}
		}
	}	
	
	public void getReviewBoardNames(int rbIds) {
		PreparedStatement pstmt = null;
		Connection conn = null;			
		try {
			conn = getConnection();	
			String Query = "Select PK_REVIEW_BOARD,REVIEW_BOARD_NAME from er_review_board where PK_REVIEW_BOARD=?";
			pstmt = conn.prepareStatement(Query);			
			pstmt.setInt(1, rbIds);
			ResultSet rs = pstmt.executeQuery();			
			while (rs.next()) {				
				setRbIds(new Integer(rs.getInt("PK_REVIEW_BOARD")));				
				setRbNames(rs.getString("REVIEW_BOARD_NAME"));						
			}			
		} catch (SQLException ex) {
			Rlog.fatal("reviewboard","ReviewBoardDao.getReviewBoardNames EXCEPTION IN FETCHING FROM Review Board table"	+ ex);

		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (Exception e) {
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
			}
		}

	}	
	
	public void getReviewBoardAllValues() {
		PreparedStatement pstmt = null;
		Connection conn = null;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement("Select PK_REVIEW_BOARD,REVIEW_BOARD_NAME from er_review_board order by REVIEW_BOARD_NAME");			
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) 
			{
				setRbIds(new Integer(rs.getInt("PK_REVIEW_BOARD")));
				setRbNames(rs.getString("REVIEW_BOARD_NAME"));						
			}
		} catch (SQLException ex) {
			Rlog.fatal("reviewboard","ReviewBoardDao.getReviewBoardAllValues EXCEPTION IN FETCHING FROM Review Board table" + ex);

		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (Exception e) {
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
			}
		}
	}
	
	
	 public int getRBNameCount(String rbName) {
		 	PreparedStatement pstmt = null;
	        Connection conn = null;
	        int count = 0;
	        try {
	            conn = getConnection();
	            System.out.println("count"+count);
	            String Query = "select count(PK_REVIEW_BOARD) as PK_REVIEW_BOARD from ER_REVIEW_BOARD where lower(REVIEW_BOARD_NAME) = lower( ?)";	            
		        Rlog.debug("reviewBoard", "ReviewBoardDao.getRBNameCount prepared statement is "+ Query);
	            pstmt = conn.prepareStatement(Query);
	            pstmt.setString(1, rbName);
				ResultSet rs = pstmt.executeQuery();	
				
				while (rs.next()) {					
					count = rs.getInt("PK_REVIEW_BOARD");
	            }				
	            return count;	            
	            
	        } catch (SQLException ex) {        	
	        	Rlog.fatal("reviewBoard","ReviewBoardDao.getRBNameCount EXCEPTION IN FETCHING FROM ER_REVIEW_BOARD"+ ex);        

	        } finally {
	            try {
	                if (pstmt != null)
	                    pstmt.close();
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }
	        }
	        return count;
	    }
}
