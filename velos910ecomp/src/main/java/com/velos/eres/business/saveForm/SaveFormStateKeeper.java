/*
 * Classname			SaveFormStateKeeper
 * 
 * Version information	1.0
 *
 * Date					07/30/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.saveForm;

/* Import Statements */
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

/**
 * SaveFormStateKeeper class resulting from implementation of the StateKeeper
 * pattern
 * 
 * @author Sonia Sahni
 * @version 1.0 07/30/2003
 */

public class SaveFormStateKeeper implements java.io.Serializable {

    /**
     * the Id
     */

    private int id;

    private String formId;

    private String studyId;

    private String accountId;

    private String patientId;

    private String patprotId;

    private String formFillDate;

    private String formXML;

    private String formCompleted;

    private String isValid;

    private String creator;

    private String lastModifiedBy;

    private String recordType;

    private String lastModifiedDate;

    private String ipAdd;

    private ArrayList params;

    private ArrayList paramValues;

    private String displayType;

    private String fkcrf;

    private String schevent;

    private String formLibVer;
    
    private String specimenPk;
    

    // GETTER SETTER METHODS

    // salil
    public String getFkCrf() {
        return this.fkcrf;
    }

    public void setFkCrf(String fkcrf) {
        this.fkcrf = fkcrf;
    }

    public String getSchEvent() {
        return this.schevent;
    }

    public void setSchEvent(String schevent) {
        this.schevent = schevent;
    }

    public String getAccountId() {
        return this.accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getFormCompleted() {
        return this.formCompleted;
    }

    public void setFormCompleted(String formCompleted) {
        this.formCompleted = formCompleted;
    }

    public String getFormFillDate() {
        return this.formFillDate;
    }

    public void setFormFillDate(String formFillDate) {
        this.formFillDate = formFillDate;
    }

    public String getFormId() {
        return this.formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getFormXML() {
        return this.formXML;
    }

    public void setFormXML(String formXML) {
        this.formXML = formXML;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public String getIsValid() {
        return this.isValid;
    }

    public void setIsValid(String isValid) {
        this.isValid = isValid;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getLastModifiedDate() {
        return this.lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public ArrayList getParams() {
        return this.params;
    }

    public void setParams(ArrayList params) {
        this.params = params;
    }
    
    public void setParams(String param) {
        this.params.add(param);
    }

    public ArrayList getParamValues() {
        return this.paramValues;
    }

    public void setParamValues(ArrayList paramValues) {
        this.paramValues = paramValues;
    }
    
    public void setParamValues(String paramValue) {
        this.paramValues.add(paramValue);
    }
    public String getPatientId() {
        return this.patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getPatprotId() {
        return this.patprotId;
    }

    public void setPatprotId(String patprotId) {
        this.patprotId = patprotId;
    }

    public String getRecordType() {
        return this.recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getStudyId() {
        return this.studyId;
    }

    public void setStudyId(String studyId) {
        this.studyId = studyId;
    }

    public String getFormLibVer() {
        return this.formLibVer;
    }

    public void setFormLibVer(String formLibVer) {
        this.formLibVer = formLibVer;
    }

    // end of getter and setter methods

    /**
     * Default Constructor
     * 
     */
    public SaveFormStateKeeper() {

        this.params = new ArrayList();
        this.paramValues = new ArrayList();

    }

    public String getDisplayType() {
        return this.displayType;
    }

    public void setDisplayType(String displayType) {
        this.displayType = displayType;
    }

    /**
     * Holds all the Save Form details either from DB/CLIENT
     * 
     */
    public SaveFormStateKeeper(int id, String formId, String studyId,
            String accountId, String patientId, String patprotId,
            String formFillDate, String formXML, String formCompleted,
            String isValid, String creator, String lastModifiedBy,
            String recordType, String lastModifiedDate, String ipAdd,
            ArrayList params, ArrayList paramValues, String displayType,
            String schevent, String fkcrk, String formLibVer, String specimenPk) {

        this.params = new ArrayList();
        this.paramValues = new ArrayList();

        Rlog
                .debug("saveForm",
                        "SaveFormStateKeeper full argument constructor ");

        setAccountId(accountId);
        setCreator(creator);
        setFormCompleted(formCompleted);
        setFormFillDate(formFillDate);
        setFormId(formId);
        setFormXML(formXML);
        setId(id);
        setIpAdd(ipAdd);
        setIsValid(isValid);
        setLastModifiedBy(lastModifiedBy);
        setLastModifiedDate(lastModifiedDate);
        setParams(params);
        setParamValues(paramValues);
        setPatientId(patientId);
        setPatprotId(patprotId);
        setRecordType(recordType);
        setStudyId(studyId);
        setDisplayType(displayType);
        setSchEvent(schevent);
        setFkCrf(fkcrf);
        setFormLibVer(formLibVer);
        setSpecimenPk(specimenPk);
    }

    /**
     * Returns SaveFormStateKeeper which holds all the details of the SaveForm
     * 
     * @return SaveFormStateKeeper
     */
    public SaveFormStateKeeper getSaveFormStateKeeper() {
        Rlog.debug("saveForm", "SaveFormStateKeeper.getSaveFormStateKeeper()");
        return new SaveFormStateKeeper(id, formId, studyId, accountId,
                patientId, patprotId, formFillDate, formXML, formCompleted,
                isValid, creator, lastModifiedBy, recordType, lastModifiedDate,
                ipAdd, params, paramValues, displayType, schevent, fkcrf,
                formLibVer,specimenPk);

    }

    /**
     * Setter for all attributes.<br>
     * Receives a SaveFormStateKeeper and assigns attributes to local variables
     * 
     * @param sfsk
     *            the SaveFormStateKeeper to set to.
     */

    public void setSaveFormStateKeeper(SaveFormStateKeeper sfsk) {

        setAccountId(sfsk.getAccountId());
        setCreator(sfsk.getCreator());
        setFormCompleted(sfsk.getFormCompleted());
        setFormFillDate(sfsk.getFormFillDate());
        setFormId(sfsk.getFormId());
        setFormXML(sfsk.getFormXML());
        setId(sfsk.getId());
        setIpAdd(sfsk.getIpAdd());
        setIsValid(sfsk.getIsValid());
        setLastModifiedBy(sfsk.getLastModifiedBy());
        setLastModifiedDate(sfsk.getLastModifiedDate());
        setParams(sfsk.getParams());
        setParamValues(sfsk.getParamValues());
        setPatientId(sfsk.getPatientId());
        setPatprotId(sfsk.getPatprotId());
        setRecordType(sfsk.getRecordType());
        setStudyId(sfsk.getStudyId());
        setDisplayType(sfsk.getDisplayType());
        setSchEvent(sfsk.getSchEvent());
        setFkCrf(sfsk.getFkCrf());
        setFormLibVer(sfsk.getFormLibVer());
        setSpecimenPk(sfsk.getSpecimenPk());

    }

    /**
     * Overriden for unit testing purposes
     * 
     * @param anObject
     *            Object on which to check equality.
     */
    public boolean equals(Object anObject) {
        Rlog.debug("saveForm", "SaveFormStateKeeper.equals(Object) ");

        if (!(anObject instanceof SaveFormStateKeeper))
            return false;

        SaveFormStateKeeper aBean = (SaveFormStateKeeper) anObject;

        return (aBean.getId() == getId() && aBean.getSpecimenPk().equals(getSpecimenPk())
                && aBean.getAccountId().equals(getAccountId())
                && aBean.getCreator().equals(getCreator())
                && aBean.getFormCompleted().equals(getFormCompleted())
                && aBean.getFormFillDate().equals(getFormFillDate())
                && aBean.getFormId().equals(getFormId())
                && aBean.getFormXML().equals(getFormXML())
                && aBean.getIpAdd().equals(getIpAdd())
                && aBean.getIsValid().equals(getIsValid())
                && aBean.getLastModifiedBy().equals(getLastModifiedBy())
                && aBean.getLastModifiedDate().equals(getLastModifiedDate())
                && aBean.getParams().equals(getParams())
                && aBean.getParamValues().equals(getParamValues())
                && aBean.getPatientId().equals(getPatientId())
                && aBean.getPatprotId().equals(getPatprotId())
                && aBean.getRecordType().equals(getRecordType())
                && aBean.getStudyId().equals(getStudyId())
                && aBean.getDisplayType().equals(getDisplayType())
                && aBean.getSchEvent().equals(getSchEvent())
                && aBean.getFkCrf().equals(getFkCrf()) && aBean.getFormLibVer()
                .equals(getFormLibVer()));

    }

	public String getSpecimenPk() {
		return specimenPk;
	}

	public void setSpecimenPk(String specimenPk) {
		this.specimenPk = specimenPk;
	}

}// end of class

