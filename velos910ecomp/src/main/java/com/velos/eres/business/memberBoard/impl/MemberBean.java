package com.velos.eres.business.memberBoard.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;

@Entity
@Table(name = "er_review_board_member")
public class MemberBean implements Serializable
{
	private static final long serialVersionUID = 3761122760600400971L;
	
	public Integer pkRBMember;    

	public Integer fkBoardId;
    
    public Integer fkMemberRole;
    
    public Integer fkMemberUser; 
    
    public String brdMemStatus;
    
    public String brdMemRights;

	private String ipAdd;
	
	private String creator;
	
	private String modifiedBy;
    
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_REVIEW_BOARD_MEMBER", allocationSize=1)
    
    @Column(name = "PK_REVIEW_BOARD_MEMBER")     
    public Integer getPkRBMember() {
		return this.pkRBMember;
	}
	public void setPkRBMember(Integer pkRBMember) {
		this.pkRBMember = pkRBMember;
	}
// NO SETTER METHOD FOR PK REQUIRED
	
	@Column(name = "FK_REVIEW_BOARD")
	public Integer getFkBoardId() {
		return this.fkBoardId;
	}
	public void setFkBoardId(Integer fkBoardId) {
		this.fkBoardId = fkBoardId;
	}
	
	@Column(name = "FK_CODELST_MEMBER_ROLE")
	public Integer getFkMemberRole() {
		return this.fkMemberRole;
	}
	public void setFkMemberRole(Integer fkMemberRole) {
		this.fkMemberRole = fkMemberRole;
	}

	@Column(name = "FK_USER")
	public Integer getFkMemberUser() {
		return this.fkMemberUser;
	}
	public void setFkMemberUser(Integer fkMemberUser) {
		this.fkMemberUser = fkMemberUser;
	}

	
	@Column(name = "BOARD_MEMBER_STATUS")
	public String getBrdMemStatus() {
		return this.brdMemStatus;
	}
	public void setBrdMemStatus(String brdMemStatus) {
		this.brdMemStatus = brdMemStatus;
	}

	
	@Column(name = "BOARD_MEMBER_RIGHTS")
	public String getBrdMemRights() {
		return this.brdMemRights;
	}
	public void setBrdMemRights(String brdMemRights) {
		this.brdMemRights = brdMemRights;
	}
    
	@Column(name = "IP_ADD")
    public String getIpAdd() {
		return ipAdd;
	}
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
	
	@Column(name = "CREATOR")
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	
	@Column(name = "LAST_MODIFIED_BY")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}	
	
	//End Of Getter/Setter
	
	public MemberBean()
	{		
	}
	
	public MemberBean(int pkRBMember,int fkBoardId,int fkMemberRole,int fkMemberUser,String brdMemStatus,String brdMemRights,
			String ipAdd,String creator,String modifiedBy)
	{
		setPkRBMember(pkRBMember);
		setFkBoardId(fkBoardId);
		setFkMemberRole(fkMemberRole);
		setFkMemberUser(fkMemberUser);
		setBrdMemStatus(brdMemStatus);
		setBrdMemRights(brdMemRights);
		setIpAdd(ipAdd);
		setCreator(creator);
		setModifiedBy(modifiedBy);		
	}
	
	public int updateMember(MemberBean mb) {
		 Rlog.debug("team", "IN ENTITY BEAN - Board Member  STATE HOLDER");
    	try 
    	{        	
        	setFkBoardId(mb.getFkBoardId());
        	setFkMemberRole(mb.getFkMemberRole());
        	setFkMemberUser(mb.getFkMemberUser());
        	setBrdMemStatus(mb.getBrdMemStatus());        	
        	setBrdMemRights(mb.getBrdMemRights()); 
        	setIpAdd(mb.getIpAdd());
			setCreator(mb.getCreator());
			setModifiedBy(mb.getModifiedBy());        	
        	Rlog.debug("member", "SET THE VALUES");
            return 0;
        }
    	catch (Exception e)
    	{
            Rlog.fatal("member", "EXCEPTION IN UPDATING NEW MEMBER " + e);
            return -2;
        }
    }
}
