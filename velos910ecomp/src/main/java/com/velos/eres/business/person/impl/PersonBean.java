/*


 * Classname : PersonBean


 * 


 * Version information: 1.0


 *


 * Date: 06/03/2001


 * 


 * Copyright notice: Velos, Inc


 *


 * Author: Sonia Sahni


 */

package com.velos.eres.business.person.impl;

/* Import Statements */

import java.io.Serializable;
import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * 
 * 
 * The Person CMP entity bean.<br>
 * <br>
 * 
 * 
 * @author Sonia Sahni
 * 
 * 
 * @vesion 1.0 06/03/2001
 * 
 * 
 * @ejbHome PersonHome
 * 
 * 
 * @ejbRemote PersonRObj
 * 
 * 
 */

@Entity
@Table(name = "person")
public class PersonBean implements Serializable

{

    /**
     * 
     */
    private static final long serialVersionUID = 3257568425328194097L;

    /**
     * 
     * 
     * the Person PK Id
     * 
     * 
     */

    public int personPKId;

    /**
     * 
     * 
     * the Person ID
     * 
     * 
     */

    public String personPId;

    /**
     * 
     * 
     * the Person Account
     * 
     * 
     */

    public Integer personAccount;

    /**
     * 
     * 
     * the Person Location
     * 
     * 
     */

    public Integer personLocation;

    public String personAltId;

    public String personLname;

    public String personFname;

    public String personMname;

    public String personSuffix;

    public String personPrefix;

    public String personDegree;

    public String personMotherName;

    public java.util.Date personDob;

    public Integer personGender;

    public String personAlias;

    public Integer personRace;

    public Integer personEthnicity;

    public Integer personAddressId;

    public Integer personPrimaryLang;

    public Integer personMarital;

    public Integer personRelegion;

    public String personSSN;

    public String personDrivLic;

    public Integer personMotherId;

    public Integer personEthGroup;

    public String personBirthPlace;

    public String personMultiBirth;

    public Integer personBirthOrder;

    public Integer personCitizen;

    public String personVetMilStatus;

    public Integer personNationality;

    public java.util.Date personDeathDate  ;

    public String personDeathIndicator;

    public Integer personEmployment;

    public Integer personEducation;

    public String personNotes;

    public Integer personStatus;

    public Integer personBloodGr;

    public String personAddress1;

    public String personAddress2;

    public String personCity;

    public String personState;

    public String personZip;

    public String personCountry;

    public String personHphone;

    public String personBphone;

    public String personEmail;

    public String personCounty;

    public java.util.Date personRegDate ;

    public Integer personRegBy;

    public Integer timeZoneId;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    public String phyOther;

    public String orgOther;

    public String personSplAccess;

    
                                                                              
    public String personAddRace;

    public String personAddEthnicity;

    public Integer patDthCause;

    public String dthCauseOther;
    
    /**
     * the Patient Facility Id
     */
    public String patientFacilityId;
	    

    // GETTER SETTER METHODS

    /**
     * 
     * 
     * @return PK Id for a person
     * 
     * 
     */

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "ERES.SEQ_ER_PER", allocationSize=1)
    @Column(name = "PK_PERSON")
    public int getPersonPKId() {

        return this.personPKId;

    }

    public void setPersonPKId(int personPk) {
        this.personPKId = personPk;
    }

    /**
     * 
     * 
     * returns person location
     * 
     * 
     * @return location
     * 
     * 
     */
    @Column(name = "FK_SITE")
    public String getPersonLocation()

    {

        return StringUtil.integerToString(this.personLocation);

    }

    /**
     * 
     * 
     * 
     * @param Location
     * 
     * 
     */

    public void setPersonLocation(String pl)

    {
        this.personLocation = StringUtil.stringToInteger(pl);
    }

    /**
     * 
     * 
     * 
     * @return personPId
     * 
     * 
     */
    @Column(name = "PERSON_CODE")
    public String getPersonPId()

    {

        return this.personPId;

    }

    /**
     * 
     * 
     * 
     * @param personPId
     * 
     * 
     */

    public void setPersonPId(String pPId)

    {

        this.personPId = pPId;

    }

    /**
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * @return personPId
     * 
     * 
     */
    @Column(name = "FK_ACCOUNT")
    public String getPersonAccount()

    {

        return StringUtil.integerToString(this.personAccount);

    }

    /**
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * @param personAccount
     * 
     * 
     */

    public void setPersonAccount(String pAccount)

    {
        this.personAccount = StringUtil.stringToInteger(pAccount);

    }

    @Column(name = "PERSON_AKA")
    public String getPersonAlias() {
        return this.personAlias;
    }

    public void setPersonAlias(String personAlias) {
        this.personAlias = personAlias;
    }

    @Column(name = "PERSON_ALTID")
    public String getPersonAltId() {
        return this.personAltId;
    }

    public void setPersonAltId(String personAltId) {
        this.personAltId = personAltId;
    }

    @Transient
    public String getPersonBirthOrder() {
        return StringUtil.integerToString(this.personBirthOrder);
    }

    public void setPersonBirthOrder(String personBirthOrder) {
        this.personBirthOrder = StringUtil.stringToInteger(personBirthOrder);
    }

    @Column(name = "PERSON_BIRTH_PLACE")
    public String getPersonBirthPlace() {
        return this.personBirthPlace;
    }

    public void setPersonBirthPlace(String personBirthPlace) {
        this.personBirthPlace = personBirthPlace;
    }

    @Column(name = "FK_CODELST_BLOODGRP")
    public String getPersonBloodGr() {
        return StringUtil.integerToString(this.personBloodGr);
    }

    public void setPersonBloodGr(String personBloodGr) {
        this.personBloodGr = StringUtil.stringToInteger(personBloodGr);
    }

    @Column(name = "FK_CODELST_CITIZEN")
    public String getPersonCitizen() {
        return StringUtil.integerToString(this.personCitizen);
    }

    public void setPersonCitizen(String personCitizen) {
        this.personCitizen = StringUtil.stringToInteger(personCitizen);
    }

    @Column(name = "PERSON_DEATHDT")
    public Date getPersonDeathDt() {
        return this.personDeathDate;

    }

    public void setPersonDeathDt(Date personDeathDate) {

        this.personDeathDate = personDeathDate;
    }

    @Transient
    public String getPersonDeathDate() {

        return DateUtil.dateToString(getPersonDeathDt());

    }

    public void setPersonDeathDate(String personDeathDate) {

        setPersonDeathDt(DateUtil.stringToDate(personDeathDate, null));
    }

    @Transient
    public String getPersonDeathIndicator() {
        return this.personDeathIndicator;
    }

    public void setPersonDeathIndicator(String personDeathIndicator) {
        this.personDeathIndicator = personDeathIndicator;
    }

    @Column(name = "PERSON_DEGREE")
    public String getPersonDegree() {
        return this.personDegree;
    }

    public void setPersonDegree(String personDegree) {
        this.personDegree = personDegree;
    }

    @Column(name = "PERSON_DOB")
    public Date getPersonDb() {
        return this.personDob;
    }

    public void setPersonDb(Date personDob) {
        this.personDob = personDob;
    }

    @Transient
    public String getPersonDob() {
        return DateUtil.dateToString(getPersonDb());

    }

    public void setPersonDob(String personDob) {
        setPersonDb(DateUtil.stringToDate(personDob, null));
    }

    @Column(name = "PERSON_DRIV_LIC")
    public String getPersonDrivLic() {
        return this.personDrivLic;
    }

    public void setPersonDrivLic(String personDrivLic) {
        this.personDrivLic = personDrivLic;
    }

    @Column(name = "FK_CODELST_EDU")
    public String getPersonEducation() {
        return StringUtil.integerToString(this.personEducation);
    }

    public void setPersonEducation(String personEducation) {
        this.personEducation = StringUtil.stringToInteger(personEducation);
    }

    @Column(name = "FK_CODELST_EMPLOYMENT")
    public String getPersonEmployment() {
        return StringUtil.integerToString(this.personEmployment);
    }

    public void setPersonEmployment(String personEmployment) {
        this.personEmployment = StringUtil.stringToInteger(personEmployment);
    }

    @Column(name = "PERSON_ETHGRP")
    public String getPersonEthGroup() {
        return StringUtil.integerToString(this.personEthGroup);
    }

    public void setPersonEthGroup(String personEthGroup) {
        this.personEthGroup = StringUtil.stringToInteger(personEthGroup);
    }

    @Column(name = "PERSON_FNAME")
    public String getPersonFname() {
        return this.personFname;
    }

    public void setPersonFname(String personFname) {
        this.personFname = personFname;
    }

    @Column(name = "FK_CODELST_GENDER")
    public String getPersonGender() {
        return StringUtil.integerToString(this.personGender);
    }

    public void setPersonGender(String personGender) {
        this.personGender = StringUtil.stringToInteger(personGender);

    }

    @Column(name = "PERSON_LNAME")
    public String getPersonLname() {
        return this.personLname;
    }

    public void setPersonLname(String personLname) {
        this.personLname = personLname;
    }

    @Column(name = "FK_CODELST_MARITAL")
    public String getPersonMarital() {
        return StringUtil.integerToString(this.personMarital);
    }

    public void setPersonMarital(String personMarital) {
        this.personMarital = StringUtil.stringToInteger(personMarital);
    }

    @Column(name = "PERSON_MNAME")
    public String getPersonMname() {
        return this.personMname;
    }

    public void setPersonMname(String personMname) {
        this.personMname = personMname;
    }

    @Column(name = "FK_PERSON_MOTHER")
    public String getPersonMotherId() {
        return StringUtil.integerToString(this.personMotherId);
    }

    public void setPersonMotherId(String personMotherId) {
        this.personMotherId = StringUtil.stringToInteger(personMotherId);

    }

    @Column(name = "PERSON_MOTHER_NAME")
    public String getPersonMotherName() {
        return this.personMotherName;
    }

    public void setPersonMotherName(String personMotherName) {
        this.personMotherName = personMotherName;
    }

    @Column(name = "PERSON_MULTI_BIRTH")
    public String getPersonMultiBirth() {
        return this.personMultiBirth;
    }

    public void setPersonMultiBirth(String personMultiBirth) {
        this.personMultiBirth = personMultiBirth;
    }

    @Column(name = "FK_CODELST_NATIONALITY")
    public String getPersonNationality() {
        return StringUtil.integerToString(this.personNationality);
    }

    public void setPersonNationality(String personNationality) {
        this.personNationality = StringUtil.stringToInteger(personNationality);

    }
    
    //JM: blocked
    //@Column(name = "PERSON_NOTES")
    @Transient
    public String getPersonNotes() {
        return this.personNotes;
    }

    public void setPersonNotes(String personNotes) {
        this.personNotes = personNotes;
    }

    @Column(name = "PERSON_PREFIX")
    public String getPersonPrefix() {
        return this.personPrefix;
    }

    public void setPersonPrefix(String personPrefix) {
        this.personPrefix = personPrefix;
    }

    @Column(name = "FK_CODELST_PRILANG")
    public String getPersonPrimaryLang() {
        return StringUtil.integerToString(this.personPrimaryLang);
    }

    public void setPersonPrimaryLang(String personPrimaryLang) {
        this.personPrimaryLang = StringUtil.stringToInteger(personPrimaryLang);
    }

    @Column(name = "FK_CODELST_RACE")
    public String getPersonRace() {
        return StringUtil.integerToString(this.personRace);
    }

    public void setPersonRace(String personRace) {
        this.personRace = StringUtil.stringToInteger(personRace);

    }

    @Column(name = "FK_CODELST_ETHNICITY")
    public String getPersonEthnicity() {
        return StringUtil.integerToString(this.personEthnicity);
    }

    public void setPersonEthnicity(String personEthnicity) {
        this.personEthnicity = StringUtil.stringToInteger(personEthnicity);

    }

    @Column(name = "FK_CODELST_RELIGION")
    public String getPersonRelegion() {
        return StringUtil.integerToString(this.personRelegion);
    }

    public void setPersonRelegion(String personRelegion) {
        this.personRelegion = StringUtil.stringToInteger(personRelegion);

    }

    @Column(name = "PERSON_SSN")
    public String getPersonSSN() {
        return this.personSSN;
    }

    public void setPersonSSN(String personSSN) {
        this.personSSN = personSSN;
    }

    @Column(name = "FK_CODELST_PSTAT")
    public String getPersonStatus() {
        return StringUtil.integerToString(this.personStatus);
    }

    public void setPersonStatus(String personStatus) {
        this.personStatus = StringUtil.stringToInteger(personStatus);

    }

    @Column(name = "PERSON_SUFFIX")
    public String getPersonSuffix() {
        return this.personSuffix;
    }

    public void setPersonSuffix(String personSuffix) {
        this.personSuffix = personSuffix;
    }

    @Column(name = "PERSON_MILVET")
    public String getPersonVetMilStatus() {
        return this.personVetMilStatus;
    }

    public void setPersonVetMilStatus(String personVetMilStatus) {
        this.personVetMilStatus = personVetMilStatus;
    }

    @Column(name = "PERSON_ADDRESS1")
    public String getPersonAddress1() {
        return this.personAddress1;
    }

    public void setPersonAddress1(String personAddress1) {
        this.personAddress1 = personAddress1;
    }

    @Column(name = "PERSON_ADDRESS2")
    public String getPersonAddress2() {
        return this.personAddress2;
    }

    public void setPersonAddress2(String personAddress2) {
        this.personAddress2 = personAddress2;
    }

    @Column(name = "PERSON_CITY")
    public String getPersonCity() {
        return this.personCity;
    }

    public void setPersonCity(String personCity) {
        this.personCity = personCity;
    }

    @Column(name = "PERSON_COUNTRY")
    public String getPersonCountry() {
        return this.personCountry;
    }

    public void setPersonCountry(String personCountry) {
        this.personCountry = personCountry;
    }

    @Column(name = "PERSON_COUNTY")
    public String getPersonCounty() {
        return this.personCounty;
    }

    public void setPersonCounty(String personCounty) {
        this.personCounty = personCounty;
    }

    @Column(name = "PERSON_EMAIL")
    public String getPersonEmail() {
        return this.personEmail;
    }

    public void setPersonEmail(String personEmail) {
        this.personEmail = personEmail;
    }

    @Column(name = "PERSON_HPHONE")
    public String getPersonHphone() {
        return this.personHphone;
    }

    public void setPersonHphone(String personHphone) {
        this.personHphone = personHphone;
    }

    @Column(name = "PERSON_STATE")
    public String getPersonState() {
        return this.personState;
    }

    public void setPersonState(String personState) {
        this.personState = personState;
    }

    @Column(name = "PERSON_ZIP")
    public String getPersonZip() {
        return this.personZip;
    }

    public void setPersonZip(String personZip) {
        this.personZip = personZip;
    }

    @Column(name = "PERSON_BPHONE")
    public String getPersonBphone() {
        return this.personBphone;
    }

    public void setPersonBphone(String personBphone) {
        this.personBphone = personBphone;
    }

    @Column(name = "PERSON_REGBY")
    public String getPersonRegBy() {
        return StringUtil.integerToString(this.personRegBy);
    }

    public void setPersonRegBy(String personRegBy) {
        this.personRegBy = StringUtil.stringToInteger(personRegBy);

    }

    @Column(name = "fk_timezone")
    public String getTimeZoneId() {
        return StringUtil.integerToString(this.timeZoneId);
    }

    public void setTimeZoneId(String timeZoneId) {
        this.timeZoneId = StringUtil.stringToInteger(timeZoneId);
    }
   // Modified by Amarnadh for issue #3013
    @Column(name = "PERSON_REGDATE" , nullable = false)
    public Date getPersonRegDt() {
    
    	  return this.personRegDate;

    }

    public void setPersonRegDt(Date personRegDate) {
    	if(personRegDate == null)
    	{
    		personRegDate = new Date();
    	}
        this.personRegDate = personRegDate;

    }

    @Transient
    public String getPersonRegDate() {
    	  
    	   return DateUtil.dateToString(getPersonRegDt());

    }

    public void setPersonRegDate(String personRegDate) {
        setPersonRegDt(DateUtil.stringToDate(personRegDate, null));

    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        this.creator = StringUtil.stringToInteger(creator);
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = StringUtil.stringToInteger(modifiedBy);
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public void setPhyOther(String phyOther) {
        this.phyOther = phyOther;
    }

    @Column(name = "PERSON_PHYOTHER")
    public String getPhyOther() {
        return this.phyOther;
    }

    public void setOrgOther(String orgOther) {
        this.orgOther = orgOther;
    }

    @Column(name = "PERSON_ORGOTHER")
    public String getOrgOther() {
        return this.orgOther;
    }

    public void setPersonSplAccess(String personSplAccess) {
        this.personSplAccess = personSplAccess;
    }

    @Column(name = "PERSON_SPLACCESS")
    public String getPersonSplAccess() {
        return this.personSplAccess;
    }

    @Column(name = "PERSON_ADD_RACE")
    public String getPersonAddRace() {
        return this.personAddRace;
    }

    public void setPersonAddRace(String personAddRace) {
        this.personAddRace = personAddRace;
    }

    @Column(name = "PERSON_ADD_ETHNICITY")
    public String getPersonAddEthnicity() {
        return this.personAddEthnicity;
    }

    public void setPersonAddEthnicity(String personAddEthnicity) {
        this.personAddEthnicity = personAddEthnicity;
    }

    @Column(name = "FK_CODELST_PAT_DTH_CAUSE")
    public String getPatDthCause() {
        return StringUtil.integerToString(this.patDthCause);
    }

    public void setPatDthCause(String patDthCause) {
        this.patDthCause = StringUtil.stringToInteger(patDthCause);

    }

    @Column(name = "CAUSE_OF_DEATH_OTHER")
    public String getDthCauseOther() {
        return this.dthCauseOther;
    }

    public void setDthCauseOther(String dthCauseOther) {
        this.dthCauseOther = dthCauseOther;

    }
    
     /**
	 * Returns the value of patientFacilityId.
	 */
	 @Column(name = "PAT_FACILITYID")
	public String getPatientFacilityId()
	{
		return patientFacilityId;
	}

	/**
	 * Sets the value of patientFacilityId.
	 * @param patientFacilityId The value to assign patientFacilityId.
	 */
	public void setPatientFacilityId(String patientFacilityId)
	{
		this.patientFacilityId = patientFacilityId;
	}
	

    // END OF GETTER SETTER METHODS

    
    
    /**
     * updates the Person Record
     * 
     * @param psk
     *            PersonStateKeeper
     * @return int 0: in case of successful updation; -2 in caseof exception
     */

    public int updatePerson(PersonBean psk) {

        try {

            setPersonLocation(psk.getPersonLocation());

            setPersonPId(psk.getPersonPId());

            setPersonAccount(psk.getPersonAccount());

            Rlog.debug("person", "PersonBean.updatePerson");

            setPersonAlias(psk.getPersonAlias());
            setPersonAltId(psk.getPersonAltId());
            setPersonBirthOrder(psk.getPersonBirthOrder());
            setPersonBirthPlace(psk.getPersonBirthPlace());
            setPersonBloodGr(psk.getPersonBloodGr());
            setPersonCitizen(psk.getPersonCitizen());
            setPersonDeathDate(psk.getPersonDeathDate());
            setPersonDeathIndicator(psk.getPersonDeathIndicator());
            setPersonDegree(psk.getPersonDegree());
            setPersonDob(psk.getPersonDob());
            setPersonDrivLic(psk.getPersonDrivLic());
            setPersonEducation(psk.getPersonEducation());
            setPersonEmployment(psk.getPersonEmployment());
            setPersonEthGroup(psk.getPersonEthGroup());
            setPersonFname(psk.getPersonFname());
            setPersonGender(psk.getPersonGender());
            setPersonLname(psk.getPersonLname());
            setPersonMarital(psk.getPersonMarital());
            setPersonMname(psk.getPersonMname());
            setPersonMotherId(psk.getPersonMotherId());
            setPersonMotherName(psk.getPersonMotherName());
            setPersonMultiBirth(psk.getPersonMultiBirth());
            setPersonNationality(psk.getPersonNationality());
            //setPersonNotes(psk.getPersonNotes());
            setPersonPrefix(psk.getPersonPrefix());
            setPersonPrimaryLang(psk.getPersonPrimaryLang());
            setPersonRace(psk.getPersonRace());
            setPersonAddRace(psk.getPersonAddRace());
            setPersonEthnicity(psk.getPersonEthnicity());
            setPersonAddEthnicity(psk.getPersonAddEthnicity());
            setPersonRelegion(psk.getPersonRelegion());
            setPersonSSN(psk.getPersonSSN());
            setPersonStatus(psk.getPersonStatus());
            setPersonSuffix(psk.getPersonSuffix());
            setPersonVetMilStatus(psk.getPersonVetMilStatus());

            setPersonAddress1(psk.getPersonAddress1());
            setPersonAddress2(psk.getPersonAddress2());
            setPersonCity(psk.getPersonCity());
            setPersonCountry(psk.getPersonCountry());
            setPersonCounty(psk.getPersonCounty());
            setPersonEmail(psk.getPersonEmail());
            setPersonHphone(psk.getPersonHphone());
            setPersonBphone(psk.getPersonBphone());
            setPersonState(psk.getPersonState());
            setPersonZip(psk.getPersonZip());
          
            setPersonRegDate(psk.getPersonRegDate());
            setPersonRegBy(psk.getPersonRegBy());
            setTimeZoneId(psk.getTimeZoneId());
            setCreator(psk.getCreator());
            setModifiedBy(psk.getModifiedBy());
            setIpAdd(psk.getIpAdd());
            setPhyOther(psk.getPhyOther());
            setOrgOther(psk.getOrgOther());
            setPersonSplAccess(psk.getPersonSplAccess());

            setPatDthCause(psk.getPatDthCause());

            setDthCauseOther(psk.getDthCauseOther());
            setPatientFacilityId(psk.getPatientFacilityId());
            Rlog.debug("person", "SET ALL ATTRIBUTES");

            return 0;

        } catch (Exception e) {

            Rlog.fatal("person", " error in PersonBean.updatePerson");

            return -2;

        }

    }

    public PersonBean() {

    }

    public PersonBean(int personPKId, String personPId, String personAccount,
            String personLocation, String personAltId, String personLname,
            String personFname, String personMname, String personSuffix,
            String personPrefix, String personDegree, String personMotherName,
            String personDob, String personGender, String personAlias,
            String personRace, String personEthnicity,
            String personPrimaryLang, String personMarital,
            String personRelegion, String personSSN, String personDrivLic,
            String personMotherId, String personEthGroup,
            String personBirthPlace, String personMultiBirth,
            String personBirthOrder, String personCitizen,
            String personVetMilStatus, String personNationality,
            String personDeathDate, String personDeathIndicator,
            String personEmployment, String personEducation,
            String personNotes, String personStatus, String personBloodGr,
            String personAddress1, String personAddress2, String personCity,
            String personState, String personZip, String personCountry,
            String personHphone, String personBphone, String personEmail,
            String personCounty, String personRegDate, String personRegBy,
            String timeZoneId, String creator, String modifiedBy, String ipAdd,
            String phyOther, String orgOther, String personSplAccess,
            String personAddRace, String personAddEthnicity,
            String patDthCause, String dthCauseOther, String patientFacilityId) {
        super();
        // TODO Auto-generated constructor stub
        setPersonPKId(personPKId);
        setPersonPId(personPId);
        setPersonAccount(personAccount);
        setPersonLocation(personLocation);
        setPersonAltId(personAltId);
        setPersonLname(personLname);
        setPersonFname(personFname);
        setPersonMname(personMname);
        setPersonSuffix(personSuffix);
        setPersonPrefix(personPrefix);
        setPersonDegree(personDegree);
        setPersonMotherName(personMotherName);
        setPersonDob(personDob);
        setPersonGender(personGender);
        setPersonAlias(personAlias);
        setPersonRace(personRace);
        setPersonEthnicity(personEthnicity);
        // setPersonAddressId(personAddressId);
        setPersonPrimaryLang(personPrimaryLang);
        setPersonMarital(personMarital);
        setPersonRelegion(personRelegion);
        setPersonSSN(personSSN);
        setPersonDrivLic(personDrivLic);
        setPersonMotherId(personMotherId);
        setPersonEthGroup(personEthGroup);
        setPersonBirthPlace(personBirthPlace);
        setPersonMultiBirth(personMultiBirth);
        setPersonBirthOrder(personBirthOrder);
        setPersonCitizen(personCitizen);
        setPersonVetMilStatus(personVetMilStatus);
        setPersonNationality(personNationality);
        setPersonDeathDate(personDeathDate);
        setPersonDeathIndicator(personDeathIndicator);
        setPersonEmployment(personEmployment);
        setPersonEducation(personEducation);
        //setPersonNotes(personNotes);
        setPersonStatus(personStatus);
        setPersonBloodGr(personBloodGr);
        setPersonAddress1(personAddress1);
        setPersonAddress2(personAddress2);
        setPersonCity(personCity);
        setPersonState(personState);
        setPersonZip(personZip);
        setPersonCountry(personCountry);
        setPersonHphone(personHphone);
        setPersonBphone(personBphone);
        setPersonEmail(personEmail);
        setPersonCounty(personCounty);
        setPersonRegDate(personRegDate);
        setPersonRegBy(personRegBy);
        setTimeZoneId(timeZoneId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setPhyOther(phyOther);
        setOrgOther(orgOther);
        setPersonSplAccess(personSplAccess);
        setPersonAddRace(personAddRace);
        setPersonAddEthnicity(personAddEthnicity);
        setPatDthCause(patDthCause);
        setDthCauseOther(dthCauseOther);
	setPatientFacilityId(patientFacilityId);
    }

}// end of class

