/*
 * Classname : PerApndxDao
 * @author: Sonika Talwar 
 * Date: August 28, 2002
 * @version information: 1.0 
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

public class PerApndxDao extends CommonDAO implements java.io.Serializable {
    private ArrayList perApndxIds;

    private ArrayList perApndxPerIds;

    private ArrayList perApndxDates;

    private ArrayList perApndxDescs;

    private ArrayList perApndxUris;

    private ArrayList perApndxTypes;

    private int cRows;

    public PerApndxDao() {
        perApndxIds = new ArrayList();
        perApndxPerIds = new ArrayList();
        perApndxDates = new ArrayList();
        perApndxDescs = new ArrayList();
        perApndxUris = new ArrayList();
        perApndxTypes = new ArrayList();
    }

    public ArrayList getPerApndxIds() {
        return this.perApndxIds;
    }

    public void setPerApndxIds(ArrayList perApndxIds) {
        this.perApndxIds = perApndxIds;
    }

    public ArrayList getPerApndxPerIds() {
        return this.perApndxPerIds;
    }

    public void setPerApndxPerIds(ArrayList perApndxPerIds) {
        this.perApndxPerIds = perApndxPerIds;
    }

    public ArrayList getPerApndxDates() {
        return this.perApndxDates;
    }

    public void setPerApndxDates(ArrayList perApndxDates) {
        this.perApndxDates = perApndxDates;
    }

    public ArrayList getPerApndxDescs() {
        return this.perApndxDescs;
    }

    public void setPerApndxDescs(ArrayList perApndxDescs) {
        this.perApndxDescs = perApndxDescs;
    }

    public ArrayList getPerApndxUris() {
        return this.perApndxUris;
    }

    public void setPerApndxUris(ArrayList perApndxUris) {
        this.perApndxUris = perApndxUris;
    }

    public ArrayList getPerApndxTypes() {
        return this.perApndxTypes;
    }

    public void setPerApndxTypes(ArrayList perApndxTypes) {
        this.perApndxTypes = perApndxTypes;
    }

    // **************

    public void setPerApndxIds(String perApndxId) {
        perApndxIds.add(perApndxId);
    }

    public void setPerApndxPerIds(String perApndxPerId) {
        perApndxPerIds.add(perApndxPerId);
    }

    public void setPerApndxDates(String perApndxDate) {
        perApndxDates.add(perApndxDate);
    }

    public void setPerApndxDescs(String perApndxDesc) {
        perApndxDescs.add(perApndxDesc);
    }

    public void setPerApndxUris(String perApndxUri) {
        perApndxUris.add(perApndxUri);
    }

    public void setPerApndxTypes(String perApndxType) {
        perApndxTypes.add(perApndxType);
    }

    public void getPerApndxUris(int patId) {
        Connection conn = null;
        String sqlString = null;
        PreparedStatement pstmt = null;

        try {

            conn = getConnection();
            sqlString = "select PK_PERAPNDX,"
                    + "to_char(PERAPNDX_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) as PERAPNDX_DATE,"
                    + "PERAPNDX_DESC,"
                    + "PERAPNDX_URI,"
                    + "PERAPNDX_TYPE"
                    + " from PAT_PERAPNDX where FK_PER = ? and PERAPNDX_TYPE='U'";

            pstmt = conn.prepareStatement(sqlString);
            Rlog.debug("perapndx", "after prepareStatement");
            pstmt.setInt(1, patId);
            Rlog.debug("perapndx", "sql is " + sqlString);

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("perapndx", "after execute" + rs);

            while (rs.next()) {
                Rlog.debug("perapndx", "inside loop");
                setPerApndxIds(rs.getString("PK_PERAPNDX"));
                setPerApndxDates(rs.getString("PERAPNDX_DATE"));
                setPerApndxDescs(rs.getString("PERAPNDX_DESC"));
                setPerApndxUris(rs.getString("PERAPNDX_URI"));
                setPerApndxTypes(rs.getString("PERAPNDX_TYPE"));
            }
        } catch (SQLException e) {
            Rlog.fatal("perapndx",
                    "error in database fetch operation in getPerApndxDetails "
                            + e);
        } catch (Exception e) {
            Rlog.fatal("perapndx", "some error in getPerApndxDetails " + e);

        } finally {
            
            try {
                pstmt.close();
            } catch (SQLException e) {
                Rlog.debug("perapndx",
                        "Could not close the prepared statement");
            }
            
            try {
                conn.close();
            } catch (SQLException e) {
                Rlog.debug("perapndx",
                        "Could not close the connection");
            }
            
          }
    }

    public void getPerApndxFiles(int patId) {
        Connection conn = null;
        String sqlString = null;
        PreparedStatement pstmt = null;

        try {

            conn = getConnection();
            sqlString = "select PK_PERAPNDX,"
                    + "to_char(PERAPNDX_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) as PERAPNDX_DATE,"
                    + "PERAPNDX_DESC,"
                    + "PERAPNDX_URI,"
                    + "PERAPNDX_TYPE"
                    + " from PAT_PERAPNDX where FK_PER = ? and PERAPNDX_TYPE='F'";

            Rlog.debug("perapndx", "sql is" + sqlString);

            pstmt = conn.prepareStatement(sqlString);
            Rlog.debug("perapndx", "after prepareStatement");
            pstmt.setInt(1, patId);
            Rlog.debug("perapndx", "after setting the study");

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("perapndx", "after execute" + rs);

            while (rs.next()) {
                Rlog.debug("perapndx", "inside loop");
                setPerApndxIds(rs.getString("PK_PERAPNDX"));
                setPerApndxDates(rs.getString("PERAPNDX_DATE"));
                setPerApndxDescs(rs.getString("PERAPNDX_DESC"));
                setPerApndxUris(rs.getString("PERAPNDX_URI"));
                setPerApndxTypes(rs.getString("PERAPNDX_TYPE"));
            }
        } catch (SQLException e) {
            Rlog.fatal("perapndx",
                    "error in database fetch operation in getPerApndxDetails");
        } catch (Exception e) {
            Rlog.fatal("perapndx", "some error in getPerApndxDetails");

        } finally {
        	try {
                pstmt.close();
            } catch (SQLException e) {
                Rlog.debug("perapndx",
                        "Could not close the prepared statement");
            }
            
            try {
                conn.close();
            } catch (SQLException e) {
                Rlog.debug("perapndx",
                        "Could not close the connection");
            }

        }
    }

}
