/**
 * Classname	StudyTXArmDao.class
 * 
 * Version information 	1.0
 *
 * Date	05/06/2005
 * 
 * Author -- Anu
 *
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

public class StudyTXArmDao extends CommonDAO implements java.io.Serializable {

    private ArrayList studyTXArmIds;

    private ArrayList txNames;

    private ArrayList txDescs;

    private ArrayList txDrugInfos;

    public StudyTXArmDao() {

        studyTXArmIds = new ArrayList();
        txNames = new ArrayList();
        txDescs = new ArrayList();
        txDrugInfos = new ArrayList();

    }

    // Getter and Setter methods

    public ArrayList getStudyTXArmIds() {
        return this.studyTXArmIds;
    }

    public void setStudyTXArmIds(ArrayList studyTXArmIds) {
        this.studyTXArmIds = studyTXArmIds;
    }

    public ArrayList getTXNames() {
        return this.txNames;
    }

    public void setTXNames(ArrayList txNames) {
        this.txNames = txNames;
    }

    public ArrayList getTXDescs() {
        return this.txDescs;
    }

    public void setTXDescs(ArrayList txDescs) {
        this.txDescs = txDescs;
    }

    public ArrayList getTXDrugInfos() {
        return this.txDrugInfos;
    }

    public void setTXDrugInfos(ArrayList txDrugInfos) {
        this.txDrugInfos = txDrugInfos;
    }

    // //

    public void setStudyTXArmIds(Integer studyTXArmId) {
        studyTXArmIds.add(studyTXArmId);
    }

    public void setTXNames(String txName) {
        txNames.add(txName);
    }

    public void setTXDescs(String txDesc) {
        txDescs.add(txDesc);
    }

    public void setTXDrugInfos(String txDrugInfo) {
        txDrugInfos.add(txDrugInfo);
    }

    // end of getter and setter methods

    // /////////
    /**
     * Retreives all treatment arms for a study
     * 
     * @param studyId -
     *            study Id
     */

    public void getStudyTrtmtArms(int studyId) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        try {
            conn = getConnection();
            sql = "SELECT PK_STUDYTXARM, " + " TX_NAME, " + " TX_DRUG_INFO, "
                    + " TX_DESC " + " from er_studytxarm where fk_study = ? ";
            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, studyId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setStudyTXArmIds(new Integer(rs.getInt("PK_STUDYTXARM")));

                setTXNames(rs.getString("TX_NAME"));

                setTXDescs(rs.getString("TX_DESC"));

                setTXDrugInfos(rs.getString("TX_DRUG_INFO"));

                rows++;

            }

        } catch (SQLException ex) {
            Rlog.fatal("studyTXArm",
                    "getStudyTrtmtArms EXCEPTION IN FETCHING FROM TABLE" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
    }

    /**
     * Returns the number of patients using a particular treatment arm
     * 
     * @param StudyId
     *            Study id
     * @param StudyTxArmId
     *            treatment arm id
     * @return int The number of users with the same Login Id as userLogin
     */
    public int getCntTXForPat(int studyId, int studyTXArmId) {
        int count = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select count(*) as count "
                    + " from er_pattxarm, er_studytxarm "
                    + " where pk_studytxarm = fk_studytxarm "
                    + " and fk_study = ?" + " and fk_studytxarm = ? ");
            pstmt.setInt(1, studyId);
            pstmt.setInt(2, studyTXArmId);
            ResultSet rs = pstmt.executeQuery();

            rs.next();
            count = rs.getInt("count");

        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "studyTXArm.getCount EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return count;
    }

}
