/*
 * Classname			StudyNIHGrantDao.class
 *
 * Version information 	1.0
 *
 * Date					11/23/2007
 *
 * Copyright notice		Velos, Inc.
 *
 * Author 				Ashu Khatkar
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

public class StudyNIHGrantDao extends CommonDAO implements java.io.Serializable{
	private ArrayList pkStudyNIhGrants;
	private ArrayList fkStudys;
	private ArrayList fundMechsms;
   	private ArrayList institudeCodes;
	private ArrayList programCodes;
 	private ArrayList nihGrantSerials;
 	 
    public StudyNIHGrantDao(){
 		pkStudyNIhGrants= new ArrayList();
 		fkStudys=new ArrayList();
 		fundMechsms=new ArrayList();
 		institudeCodes=new ArrayList();
 		programCodes=new ArrayList();
 		nihGrantSerials=new ArrayList();
	
 	}
   
	public ArrayList getPkStudyNIhGrants() {
		return pkStudyNIhGrants;
	}

	public void setPkStudyNIhGrants(ArrayList pkStudyNIhGrants) {
		this.pkStudyNIhGrants = pkStudyNIhGrants;
	}
	public void setPkStudyNIhGrants(Integer pkStudyNIhGrant) {
        this.pkStudyNIhGrants.add(pkStudyNIhGrant);
    }

	public ArrayList getFkStudys() {
			return fkStudys;
	}
	public void setFkStudys(ArrayList fkStudys) {
		this.fkStudys = fkStudys;
	}
	public void setFkStudys(Integer fkStudy) {
		this.fkStudys.add(fkStudy);
	}
	
	public ArrayList getFundMechsms() {
			return fundMechsms;
	}
	public void setFundMechsms(ArrayList fundMechsms) {
			this.fundMechsms = fundMechsms;
	}	
	public void setFundMechsms(Integer fundMechsms) {
			this.fundMechsms.add(fundMechsms);
	}
	public ArrayList getInstitudeCodes() {
			return institudeCodes;
	}
	public void setInstitudeCodes(ArrayList institudeCodes) {
			this.institudeCodes = institudeCodes;
	}
	public void setInstitudeCodes(Integer institudeCode) {
		this.institudeCodes.add(institudeCode);
    }
    public ArrayList getProgramCodes() {
		return programCodes;
	}

	public void setProgramCodes(ArrayList programCodes) {
		this.programCodes = programCodes;
	}
	public void setProgramCodes(Integer programCode) {
		this.programCodes.add(programCode);
	}
	
    public ArrayList getNihGrantSerials() {
		return nihGrantSerials;
	}

	public void setNihGrantSerials(ArrayList nihGrantSerials) {
		this.nihGrantSerials = nihGrantSerials;
	}
	public void setNihGrantSerials(String nihGrantSerial) {
		this.nihGrantSerials.add(nihGrantSerial);
	}
	
	public void getStudyNIHGrantRecords(int fkStudyId) {

		PreparedStatement pstmt = null;
	    Connection conn = null;
	    try {
	        conn = getConnection();
	        pstmt = conn.prepareStatement("select PK_STUDY_NIH_GRANT, FK_STUDY, FK_CODELST_FUNDMECH, FK_CODELST_INSTCODE," +
	        		" FK_CODELST_PROGRAM_CODE, NIH_GRANT_SERIAL" +
	        		" from er_study_nih_grant where FK_STUDY=?");



	        pstmt.setInt(1, fkStudyId);
	        ResultSet rs = pstmt.executeQuery();
	        while (rs.next()) {

                setPkStudyNIhGrants(new Integer(rs.getInt("PK_STUDY_NIH_GRANT")));
                setFkStudys(new Integer(rs.getInt("FK_STUDY")));
                setFundMechsms(new Integer(rs.getInt("FK_CODELST_FUNDMECH")));
                setInstitudeCodes(new Integer(rs.getInt("FK_CODELST_INSTCODE")));
                setProgramCodes(new Integer(rs.getInt("FK_CODELST_PROGRAM_CODE")));
                setNihGrantSerials(rs.getString("NIH_GRANT_SERIAL"));
	        }

	   } catch (SQLException ex) {
	        Rlog.fatal("StudyNIHGrant",
	                "StudyNIHGrantDao.getStudyNIHGrantAttributes EXCEPTION IN FETCHING FROM NIHGrant table"
	                        + ex);
	    } finally {
	        try {
	            if (pstmt != null)
	                pstmt.close();
	        } catch (Exception e) {
	        }
	        try {
	            if (conn != null)
	                conn.close();
	        } catch (Exception e) {
	        }

	    }

	}


}
