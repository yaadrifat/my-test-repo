//Vishal dt 03/29/2001

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/*Modified by Sonia Abrol 01/14/05
 Added a new attribute lViewColDataType, populated  the same in getReturnValues*/
public class LookupDao extends CommonDAO implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 3258128063845183543L;

    private String LViewDropDown = null;

    private ArrayList lViewCIds;

    private ArrayList lViewCNames;

    private ArrayList lViewColumns;

    private ArrayList lViewColLen;

    private ArrayList lViewRetKeywords;

    private ArrayList lViewRetColumns;

    private ArrayList LViewIsDisplay;

    private int verifyValue;

    private int cRows;

    private String retDispValue = "";

    private String retDataValue = "";

    private String viewName = "";

    private String viewDesc = "";

    private String viewFilter = "";

    private ArrayList lViewIds;

    private ArrayList lViewNames;
    
    private int lookupLib;

    /** ArrayList of COlumn Data Types */
    private ArrayList lViewColDataTypes;

    private ArrayList lViewTableNames;
    
    private ArrayList listCustom1;
    private ArrayList listCustom2;
    private ArrayList listCustom3;
    private ArrayList listCustom4;
    private ArrayList listCustom5;
    private ArrayList listCustom6;
    private ArrayList listCustom7;
    private ArrayList listCustom8;
    private ArrayList listCustom9;
    private ArrayList listCustom10;
    

   
	public LookupDao() {
        lViewCIds = new ArrayList();
        lViewCNames = new ArrayList();
        lViewColumns = new ArrayList();
        lViewColLen = new ArrayList();
        lViewRetKeywords = new ArrayList();
        lViewRetColumns = new ArrayList();
        LViewIsDisplay = new ArrayList();

        lViewIds = new ArrayList();
        lViewNames = new ArrayList();
        lViewColDataTypes = new ArrayList();
        lViewTableNames = new ArrayList();
        
        
        
        listCustom1=new ArrayList();
        listCustom2=new ArrayList();
        listCustom3=new ArrayList();
        listCustom4=new ArrayList();
        listCustom5=new ArrayList();
        listCustom6=new ArrayList();
        listCustom7=new ArrayList();
        listCustom8=new ArrayList();
        listCustom9=new ArrayList();
        listCustom10=new ArrayList();

    }

    // Getter and Setter methods

    public String getLookupDropDown() {
        return this.LViewDropDown;
    }

    public void setLookupDropDown(String LookupDropDown) {
        this.LViewDropDown = LookupDropDown;
    }

    public ArrayList getLViewColumns() {
        return this.lViewColumns;
    }

    public void setLViewColumns(ArrayList lViewColumns) {
        this.lViewColumns = lViewColumns;
    }

    public ArrayList getLViewColLen() {
        return this.lViewColLen;
    }

    public void setLViewColLen(ArrayList lViewColLen) {
        this.lViewColLen = lViewColLen;
    }

    public ArrayList getLViewCIds() {
        return this.lViewCIds;
    }

    public void setLViewCIds(ArrayList lViewCIds) {
        this.lViewCIds = lViewCIds;
    }

    public void setLViewCIds(Integer vColId) {
        lViewCIds.add(vColId);
    }

    public ArrayList getLViewCNames() {
        return this.lViewCNames;
    }

    public void setLViewCNames(ArrayList lViewCNames) {
        this.lViewCNames = lViewCNames;
    }

    public void setLViewCNames(String vColName) {
        lViewCNames.add(vColName);
    }

    public void setLViewColumns(String vColumn) {
        lViewColumns.add(vColumn);
    }

    public void setLViewColLen(String vColLen) {
        lViewColLen.add(vColLen);
    }

    public void setRetDispValue(String retDispValue) {

        this.retDispValue = retDispValue;
    }

    public String getRetDispValue() {
        return this.retDispValue;
    }

    public void setRetDataValue(String retDataValue) {

        this.retDataValue = retDataValue;
    }

    public String getRetDataValue() {
        return this.retDataValue;
    }

    public String getViewName() {
        return this.viewName;
    }

    public void setViewName(String viewName) {
        this.viewName = viewName;
    }

    public ArrayList getLViewIsDisplay() {
        return this.LViewIsDisplay;
    }

    public void setLViewIsDisplay(String vDisplay) {
        LViewIsDisplay.add(vDisplay);
    }

    public ArrayList getLViewRetKeywords() {
        return this.lViewRetKeywords;
    }

    public void setLViewRetKeywords(String lKeyword) {
        lViewRetKeywords.add(lKeyword);
    }

    public ArrayList getLViewRetColumns() {
        return this.lViewRetColumns;
    }

    public void setLViewRetColumns(String lColumns) {
        lViewRetColumns.add(lColumns);
    }

    public ArrayList getLViewIds() {
        return lViewIds;
    }

    public ArrayList getLViewNames() {
        return lViewNames;
    }

    public void setLViewIds(ArrayList lViewIds) {
        this.lViewIds = lViewIds;
    }

    public void setLViewNames(ArrayList lViewNames) {
        this.lViewNames = lViewNames;
    }

    public void setLViewIds(Integer lViewId) {
        lViewIds.add(lViewId);
    }

    public void setLViewNames(String lViewName) {
        lViewNames.add(lViewName);
    }

    /**
     * Returns the value of lViewColDataTypes.
     */
    public ArrayList getLViewColDataTypes() {
        return lViewColDataTypes;
    }

    /**
     * Sets the value of lViewColDataTypes.
     * 
     * @param lViewColDataTypes
     *            The value to assign lViewColDataTypes
     */
    public void setLViewColDataTypes(ArrayList lViewColDataTypes) {
        this.lViewColDataTypes = lViewColDataTypes;
    }

    /**
     * Appends a column data type value to ArrayList lViewColDataTypes.
     * 
     * @param lViewColDataType
     *            The lViewColDataType to added to lViewColDataTypes.
     */
    public void setLViewColDataTypes(String lViewColDataType) {
        this.lViewColDataTypes.add(lViewColDataType);
    }

    /**
     * Returns the value of lViewTableNames.
     */
    public ArrayList getLViewTableNames() {
        return lViewTableNames;
    }

    /**
     * Sets the value of lViewTableNames.
     * 
     * @param lViewTableNames
     *            The value to assign lViewTableNames.
     */
    public void setLViewTableNames(ArrayList lViewTableNames) {
        this.lViewTableNames = lViewTableNames;
    }

    /**
     * Appends a Lookup View Column Table Name to lViewTableNames
     * 
     * @param lViewTableNames
     *            The value to add to lViewTableNames.
     */
    public void setLViewTableNames(String lViewTableName) {
        this.lViewTableNames.add(lViewTableName);
    }

    public String getViewFilter() {
        return this.viewFilter;

    }

    public void setViewFilter(String filter) {
        this.viewFilter = filter;

    }

    public String getViewDesc() {
        return this.viewDesc;

    }

    public void setViewDesc(String desc) {
        this.viewDesc = desc;

    }
    
    /**
	 * @return the listCustom1
	 */
	public ArrayList getListCustom1() {
		return listCustom1;
	}

	/**
	 * @param listCustom1 the listCustom1 to set
	 */
	public void setListCustom1(ArrayList listCustom1) {
		this.listCustom1 = listCustom1;
	}
	
	public void setListCustom1(String value) {
		this.listCustom1.add(value);
	}
    /**
	 * @return the listCustom2
	 */
	public ArrayList getListCustom2() {
		return listCustom2;
	}

	/**
	 * @param listCustom1 the listCustom1 to set
	 */
	public void setListCustom2(ArrayList listCustom) {
		this.listCustom2 = listCustom;
	}
	
	public void setListCustom2(String value) {
		this.listCustom2.add(value);
	}

	/**
	 * @return the listCustom2
	 */
	public ArrayList getListCustom3() {
		return listCustom3;
	}

	/**
	 * @param listCustom1 the listCustom1 to set
	 */
	public void setListCustom3(ArrayList listCustom) {
		this.listCustom3 = listCustom;
	}
	
	public void setListCustom3(String value) {
		this.listCustom3.add(value);
	}

	/**
	 * @return the listCustom2
	 */
	public ArrayList getListCustom4() {
		return listCustom4;
	}

	/**
	 * @param listCustom1 the listCustom1 to set
	 */
	public void setListCustom4(ArrayList listCustom) {
		this.listCustom4 = listCustom;
	}
	
	public void setListCustom4(String value) {
		this.listCustom4.add(value);
	}
	/**
	 * @return the listCustom2
	 */
	public ArrayList getListCustom5() {
		return listCustom5;
	}

	/**
	 * @param listCustom1 the listCustom1 to set
	 */
	public void setListCustom5(ArrayList listCustom) {
		this.listCustom5 = listCustom;
	}
	
	public void setListCustom5(String value) {
		this.listCustom5.add(value);
	}
	/**
	 * @return the listCustom2
	 */
	public ArrayList getListCustom6() {
		return listCustom6;
	}

	/**
	 * @param listCustom1 the listCustom1 to set
	 */
	public void setListCustom6(ArrayList listCustom) {
		this.listCustom6 = listCustom;
	}
	
	public void setListCustom6(String value) {
		this.listCustom6.add(value);
	}

	/**
	 * @return the listCustom2
	 */
	public ArrayList getListCustom7() {
		return listCustom7;
	}

	/**
	 * @param listCustom1 the listCustom1 to set
	 */
	public void setListCustom7(ArrayList listCustom) {
		this.listCustom7 = listCustom;
	}
	
	public void setListCustom7(String value) {
		this.listCustom7.add(value);
	}
	/**
	 * @return the listCustom2
	 */
	public ArrayList getListCustom8() {
		return listCustom8;
	}

	/**
	 * @param listCustom1 the listCustom1 to set
	 */
	public void setListCustom8(ArrayList listCustom) {
		this.listCustom8 = listCustom;
	}
	
	public void setListCustom8(String value) {
		this.listCustom8.add(value);
	}
	/**
	 * @return the listCustom2
	 */
	public ArrayList getListCustom9() {
		return listCustom9;
	}

	/**
	 * @param listCustom1 the listCustom1 to set
	 */
	public void setListCustom9(ArrayList listCustom) {
		this.listCustom9 = listCustom;
	}
	
	public void setListCustom9(String value) {
		this.listCustom9.add(value);
	}
	
	/**
	 * @return the listCustom2
	 */
	public ArrayList getListCustom10() {
		return listCustom10;
	}

	/**
	 * @param listCustom1 the listCustom1 to set
	 */
	public void setListCustom10(ArrayList listCustom) {
		this.listCustom10 = listCustom;
	}
	
	public void setListCustom10(String value) {
		this.listCustom10.add(value);
	}
	
	/**
	 * @return the lookupLib
	 */
	public int getLookupLib() {
		return lookupLib;
	}

	/**
	 * @param lookupLib the lookupLib to set
	 */
	public void setLookupLib(int lookupLib) {
		this.lookupLib = lookupLib;
	}
	
    // end of getter and setter methods
    public String getViewId(String viewName) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        String retStr = "";
        String right = "";
        String sql = "select pk_lkpview from er_lkpview where trim(lower(lkpview_name))=? ";
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, (viewName.toLowerCase()).trim());
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                retStr = StringUtil.integerToString(new Integer(rs
                        .getInt("pk_lkpview")));

            }
            return retStr;
        } catch (SQLException ex) {
            Rlog
                    .fatal("lookup",
                            "LookupDao.getviewId EXCEPTION IN FETCHING FROM table"
                                    + ex);
            return "error";
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getDispLength(String viewId, Connection conn) {
        PreparedStatement pstmt = null;

        String retStr = "";
        String right = "";
        String sql = "select a.lkpcol_name,a.lkpcol_dispval,b.lkpview_displen,a.pk_LKPcol,b.lkpview_is_display from er_lkpcol a,er_lkpviewcol b "
                + " where b.fk_lkpcol=a.pk_lkpcol and b.fk_lkpview=?  order by lkpview_seq ";

        try {

            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, StringUtil.stringToNum(viewId));

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setLViewColLen(rs.getString("lkpview_displen"));
                setLViewIsDisplay(rs.getString("lkpview_is_display"));

            }

            // setCRows(rows) ;

        } catch (SQLException ex) {
            Rlog.fatal("lookup",
                    "LookupDao.getDispLength EXCEPTION IN FETCHING FROM table"
                            + ex);
            retStr = null;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
 

        }

    }

    /*
     * public void getReturnValues(String viewId) { PreparedStatement pstmt =
     * null; Connection conn = null; String retStr = ""; String right = "";
     * String sql="select lkpview_retdata,lkpview_retdisp,lkpview_name from
     * er_lkpview"+ " where pk_lkpview=? ";
     * 
     * 
     * try { conn = getConnection();
     * 
     * 
     * 
     * 
     * pstmt = conn.prepareStatement(sql);
     * 
     * 
     * 
     * pstmt.setInt(1,StringUtil.stringToNum(viewId));
     * 
     * 
     * 
     * ResultSet rs = pstmt.executeQuery();
     * 
     * while (rs.next()) {
     * 
     * setRetDataValue(rs.getString("lkpview_retdata"));
     * setRetDispValue(rs.getString("lkpview_retdisp"));
     * setViewName(rs.getString("lkpview_name")); }
     * 
     * //setCRows(rows) ; } catch (SQLException ex) {
     * Rlog.fatal("lookup","LookupDao.getresultvalues EXCEPTION IN FETCHING FROM
     * table" + ex); retStr = null; } finally { try { if (pstmt != null)
     * pstmt.close(); } catch (Exception e) { } try { if (conn!=null)
     * conn.close(); } catch (Exception e) {} } }
     */

    /*
     * Modified by Sonia Abrol Date: 01/25/05 To Populate the value of
     * lkpcol_datatype, lkpview_is_display,lkpcol_table
     */

    public void getReturnValues(String viewId) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        String retStr = "";
        String right = "";
        String lkpcoldatatype = "";

        String sql = "select lkpcol_name,lkpcol_keyword,lkpcol_dispval,lkpcol_datatype,nvl(lkpview_is_display,'Y') lkpview_is_display,lkpcol_table from er_lkpcol,er_lkpviewcol "
                + "where fk_lkpview=? and pk_lkpcol=fk_lkpcol order by lkpview_seq";

        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, StringUtil.stringToNum(viewId));

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setLViewColumns(rs.getString("lkpcol_name"));
                setLViewRetColumns(rs.getString("lkpcol_dispval"));
                setLViewRetKeywords(rs.getString("lkpcol_keyword"));

                lkpcoldatatype = rs.getString("lkpcol_datatype");

                if (StringUtil.isEmpty(lkpcoldatatype)) {
                    lkpcoldatatype = "";
                }

                setLViewColDataTypes(lkpcoldatatype);
                setLViewIsDisplay(rs.getString("lkpview_is_display"));
                setLViewTableNames(rs.getString("lkpcol_table"));

            }

            // setCRows(rows) ;

        } catch (SQLException ex) {
            Rlog.fatal("lookup",
                    "LookupDao.getreturnvalues EXCEPTION IN FETCHING FROM table"
                            + ex);
            retStr = null;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getLookupView(String viewId, String dName, int selVal,
            String flag) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String retStr = "";
        String right = "";
        String sql = "select a.lkpcol_name,a.lkpcol_dispval,b.lkpview_displen,a.pk_LKPcol from er_lkpcol a,er_lkpviewcol b "
                + " where b.fk_lkpcol=a.pk_lkpcol and b.fk_lkpview=? and b.lkpview_is_search='Y'  order by lkpview_seq ";

        try {
            conn = getConnection();

            getDispLength(viewId, conn);// retrieve the length of fields defined

            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, StringUtil.stringToNum(viewId));

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setLViewCIds(new Integer(rs.getInt("pk_lkpcol")));
                setLViewCNames(rs.getString("lkpcol_dispval"));
                setLViewColumns(rs.getString("lkpcol_name"));
                // setLViewColLen(rs.getString("lkpview_displen"));
                rows++;

            }

            // setCRows(rows) ;

            retStr = toPullDown(dName, selVal);

        } catch (SQLException ex) {
            Rlog.fatal("lookup",
                    "LookupDao.getLookupview EXCEPTION IN FETCHING FROM table"
                            + ex);
            ex.printStackTrace();
            retStr = null;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
            setLookupDropDown(retStr);

        }

    }

    // /////////////////////

    public void getAllViews(int account) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        String retStr = "";
        String right = "";

        StringBuffer sbSql = new StringBuffer();

        sbSql
                .append("select pk_lkpview,lkpview_name    from er_lkpview,er_lkplib ");
        sbSql.append("where (nvl(fk_account,0) = 0 or fk_account = ? ) and ");
        sbSql.append(" fk_lkplib = pk_lkplib order by lkpview_name ");

        try {
            conn = getConnection();

            pstmt = conn.prepareStatement(sbSql.toString());
            pstmt.setInt(1, account);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setLViewIds(new Integer(rs.getInt("pk_lkpview")));
                setLViewNames(rs.getString("lkpview_name"));
                rows++;

            }

        } catch (SQLException ex) {
            Rlog.fatal("lookup",
                    "LookupDao.getAllViews EXCEPTION IN FETCHING FROM table"
                            + ex);

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // ////////////////////////////////////////

    // /////////////////////////////

    public String toPullDown(String dName, int selValue) {
        Integer vColId = null;
        String vColName = "", vColumn = "";
        String disp = "";

        StringBuffer mainStr = new StringBuffer();

        // Debug.println("SIZE IS" + studyIds.size());
        Rlog.debug("common", "SIZE IS" + lViewCIds.size());

        try {
            int counter = 0;
            mainStr.append("<SELECT NAME=" + dName + ">");
            Integer val = new java.lang.Integer(selValue);

            Rlog.debug("common", "* " + val.toString() + "*");

            mainStr
                    .append("<OPTION value = 0 SELECTED>"+LC.L_Select_AnOption+"</OPTION>");
            mainStr.append("<OPTION value = -1~[VELALL] SELECTED>"+LC.L_All+"</OPTION>");
            for (counter = 0; counter <= lViewCIds.size() - 1; counter++) {
                vColId = (Integer) lViewCIds.get(counter);
                vColName = (String) lViewCNames.get(counter);
                vColumn = (String) lViewColumns.get(counter);
                disp = vColName;

                if (selValue == vColId.intValue()) {
                    mainStr.append("<OPTION value = '"
                            + (vColId + "~" + vColumn) + "' SELECTED>" + disp
                            + "</OPTION>");
                } else {
                    mainStr.append("<OPTION value = '"
                            + (vColId + "~" + vColumn) + "'>" + disp
                            + "</OPTION>");
                }
            }

            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in toPullDown" + e;
        }
        return mainStr.toString();
    }

    // ///////////////////////

    public String getLkpTypeVersionForView(int viewpk) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        String retStr = "";

        StringBuffer sbSql = new StringBuffer();

        sbSql.append("select lkptype_version  from er_lkpview,er_lkplib ");
        sbSql.append(" Where pk_lkpview = ? and fk_lkplib = pk_lkplib ");

        try {
            conn = getConnection();

            pstmt = conn.prepareStatement(sbSql.toString());
            pstmt.setInt(1, viewpk);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                retStr = rs.getString("lkptype_version");

            }

            return retStr;
        } catch (SQLException ex) {
            Rlog.fatal("lookup",
                    "LookupDao.getLkpTypeVersionForView EXCEPTION IN FETCHING FROM table"
                            + ex);
            return retStr;

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * get the filter codes for filters that should not be applied in a Lookup
     * View, will be implemented in AdHoc right now and not in generic lookup
     */

    public String getLkpIgnoreFilters(int viewpk) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        String retStr = "";

        String sql = "select LKPVIEW_IGNOREFILTER from er_lkpview Where pk_lkpview = ?";

        try {
            conn = getConnection();

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, viewpk);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                retStr = rs.getString("LKPVIEW_IGNOREFILTER");

            }

            if (StringUtil.isEmpty(retStr))
                retStr = "";

            return retStr;
        } catch (SQLException ex) {
            Rlog.fatal("lookup",
                    "LookupDao.getLkpIgnoreFilters EXCEPTION IN FETCHING FROM table"
                            + ex);
            return retStr;

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * get the filter codes for filters that should not be applied in a Lookup
     * View, will be implemented in AdHoc right now and not in generic lookup
     */

    public String getViewDetail(int viewpk) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        String retStr = "";

        String sql = "select lkpview_name,lkpview_desc,lkpview_filter from er_lkpview Where pk_lkpview = ?";

        try {
            conn = getConnection();

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, viewpk);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setViewName(rs.getString("LKPVIEW_name"));
                setViewDesc(rs.getString("LKPVIEW_desc"));
                setViewFilter(rs.getString("LKPVIEW_filter"));

            }

            if (StringUtil.isEmpty(retStr))
                retStr = "";

            return retStr;
        } catch (SQLException ex) {
            Rlog.fatal("lookup",
                    "LookupDao.getLkpIgnoreFilters EXCEPTION IN FETCHING FROM table"
                            + ex);
            return retStr;

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getAllViewsForLkpType(int account, String lkptype) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        String retStr = "";
        String right = "";

        StringBuffer sbSql = new StringBuffer();

        sbSql
                .append("select pk_lkpview, lkpview_name    from er_lkpview,er_lkplib ");
        sbSql
                .append("where lkptype_type = ? and (nvl(fk_account,0) = 0 or fk_account = ? ) and ");
        sbSql.append(" fk_lkplib = pk_lkplib order by lkpview_name ");

        try {
            conn = getConnection();

            pstmt = conn.prepareStatement(sbSql.toString());

            pstmt.setString(1, lkptype);
            pstmt.setInt(2, account);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setLViewIds(new Integer(rs.getInt("pk_lkpview")));
                setLViewNames(rs.getString("lkpview_name"));
                rows++;

            }

        } catch (SQLException ex) {
            Rlog.fatal("lookup",
                    "LookupDao.getAllViewsForLkpType EXCEPTION IN FETCHING FROM table"
                            + ex);

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    public void getLookupData(int lkpId, String orderBy) {
        PreparedStatement pstmt = null; 
        Connection conn = null;
        String retStr = "";
        String right = "";
        String sql = "";
        if( StringUtil.isEmpty(orderBy)) {
        	 sql = "select * from er_lkpdata where fk_lkplib=? ";
        }else { 	
             sql = "select * from er_lkpdata where fk_lkplib=?  order by " + orderBy; 
        }
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, lkpId);
            ResultSet rs = pstmt.executeQuery();
            //FIXME Right now we are just returning 10 custom field and this may need to be extended
            while (rs.next()) {
            	this.setListCustom1(rs.getString("custom001"));
            	this.setListCustom2(rs.getString("custom002"));
            	this.setListCustom3(rs.getString("custom003"));
            	this.setListCustom4(rs.getString("custom004"));
            	this.setListCustom5(rs.getString("custom005"));
            	this.setListCustom6(rs.getString("custom006"));
            	this.setListCustom7(rs.getString("custom007"));
            	this.setListCustom8(rs.getString("custom008"));
            	this.setListCustom9(rs.getString("custom009"));
            	this.setListCustom10(rs.getString("custom010"));
              }
            
        } catch (SQLException ex) {
            Rlog
                    .fatal("lookup",
                            "LookupDao.getviewId EXCEPTION IN FETCHING FROM table"
                                    + ex);
          
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public String getViewIdByViewKeyword(String viewkeyword) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        String retStr = "";
        String right = "";
        String sql = "select pk_lkpview from er_lkpview where trim(lower(LKPVIEW_KEYWORD))=? ";
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, (viewkeyword.toLowerCase()).trim());
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                retStr = StringUtil.integerToString(new Integer(rs
                        .getInt("pk_lkpview")));

            }
            return retStr;
        } catch (SQLException ex) {
            Rlog
                    .fatal("lookup",
                            "LookupDao.getViewIdByViewKeyword EXCEPTION IN FETCHING FROM table"
                                    + ex);
            return "error";
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // ///////////////////////////
}
