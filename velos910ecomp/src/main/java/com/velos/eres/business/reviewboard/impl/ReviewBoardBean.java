/*
 * Class name			ReviewBoardBean
 *
 * Version information	1.0
 *
 * Date					12/28/2012
 
 *
 * Copyright notice		Velos Inc.
 */
package com.velos.eres.business.reviewboard.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.velos.eres.service.util.Rlog;

/**
 * The Review Board CMP entity bean for Review Board.<br>
 * <br>
 * 
 * @author Amit srivastava
 * @version 1.0 01/28/2013
 * @ejbHome ReviewBoardHome
 * @ejbRemote ReviewBoardRObj
 */

@Entity
@Table(name = "er_review_board")
public class ReviewBoardBean implements Serializable {

	private static final long serialVersionUID = 3258407348337914424L;

	private int reviewBoardId;

	private String reviewBoardName;

	private String reviewBoardDescription;

	private int reviewboardAccId;

	private String boardGroupAccess;

	private String boardMomLogic;

	private int reviewBoardDefault;
	
	private String ipAdd;
	
	private String creator;
	
	private String modifiedBy;	
	
	/**
	 * 
	 * @return id of the Review Board
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_GEN")
	@SequenceGenerator(name = "SEQ_GEN", sequenceName = "ERES.SEQ_ER_REVIEW_BOARD", allocationSize = 1)
	
	@Column(name = "PK_REVIEW_BOARD")
	public int getReviewBoardId() {
		return this.reviewBoardId;
	}

	/** 
	 * @param id of the Review Board 
	 */
	public void setReviewBoardId(int reviewBoardId) {
		this.reviewBoardId = reviewBoardId;
	}
	

	/**
	 * 
	 * @return name of the Review board
	 */
	@Column(name = "REVIEW_BOARD_NAME")
	public String getReviewBoardName() {
		String reviewBoardName;
		reviewBoardName = this.reviewBoardName;
		return reviewBoardName;
	}

	/** 
	 * @param name of the Review board            
	 */
	public void setReviewBoardName(String reviewBoardName) {
		this.reviewBoardName = reviewBoardName;
	}

	/**
	 * 
	 * @return value of the REVIEW BOARD DESCRIPTION
	 */
	@Column(name = "REVIEW_BOARD_DESCRIPTION")
	public String getReviewBoardDescription() {
		String reviewBoardDescription;
		reviewBoardDescription = this.reviewBoardDescription;
		return reviewBoardDescription;
	}

	/** 
	 * @param value of the REVIEW BOARD DESCRIPTION            
	 */
	public void setReviewBoardDescription(String reviewBoardDescription) {
		this.reviewBoardDescription = reviewBoardDescription;
	}
	
	/**
	 * 
	 * @return id of the ReviewboardAcc
	 */
	@Column(name = "FK_ACCOUNT")
	public int getReviewboardAccId() {
		return this.reviewboardAccId;
	}

	/**
	 * 
	 * @param id of the ReviewboardAcc            
	 */
	public void setReviewboardAccId(int reviewboardAccId) {
		this.reviewboardAccId = reviewboardAccId;
	}

	
	/**
	 * 
	 * @return value of the BOARD GROUP ACCESS
	 */
	@Column(name = "BOARD_GROUP_ACCESS")
	public String getBoardGroupAccess() {
		String boardGroupAccess;
		boardGroupAccess = this.boardGroupAccess;
		return boardGroupAccess;
	}

	/** 
	 * @param value of the BOARD GROUP ACCESS
	 */
	public void setBoardGroupAccess(String boardGroupAccess) {
		this.boardGroupAccess = boardGroupAccess;
	}

	/**
	 * 
	 * @return value of the BOARD MOM LOGIC
	 */
	@Column(name = "BOARD_MOM_LOGIC")
	public String getBoardMomLogic() {
		String boardMomLogic;
		boardMomLogic = this.boardMomLogic;
		return boardMomLogic;
	}

	/** 
	 * @param value of the BOARD MOM LOGIC
	 */
	public void setBoardMomLogic(String boardMomLogic) {
		this.boardMomLogic = boardMomLogic;
	}

	/**
	 * 
	 * @return value of the REVIEW BOARD DEFAULT
	 */
	@Column(name = "REVIEW_BOARD_DEFAULT")
	public int getReviewBoardDefault() {
		return this.reviewBoardDefault;
	}

	
	@Column(name = "IP_ADD")
	public String getIpAdd() {
		return ipAdd;
	}

	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
	
	@Column(name = "CREATOR")
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}

	@Column(name = "LAST_MODIFIED_BY")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/** 
	 * @param value of the REVIEW BOARD DEFAULT
	 */
	public void setReviewBoardDefault(int reviewBoardDefault) {
		this.reviewBoardDefault = reviewBoardDefault;
	}

	// END OF GETTER SETTER METHODS

	/**
	 * Returns the state keeper object of the review board which holds all the
	 * details of the review board.
	 * 
	 * @return State keeper of the review board
	 */
	
	public ReviewBoardBean() {
	}

	public ReviewBoardBean(int reviewBoardId,String reviewBoardName, String reviewBoardDescription, int reviewboardAccId,
			String boardGroupAccess, String boardMomLogic,int reviewBoardDefault,String ipAdd,String creator,String modifiedBy)
	{
			setReviewBoardId(reviewBoardId);		
			setReviewBoardName(reviewBoardName);
			setReviewBoardDescription(reviewBoardDescription);
			setReviewboardAccId(reviewboardAccId);
			setBoardGroupAccess(boardGroupAccess);
			setBoardMomLogic(boardMomLogic);
			setReviewBoardDefault(reviewBoardDefault);
			setIpAdd(ipAdd);
			setCreator(creator);
			setModifiedBy(modifiedBy);

	}

	public int updateReviewBoard(ReviewBoardBean rsk) {
		try {
			setReviewBoardId(rsk.getReviewBoardId());			
			setReviewBoardName(rsk.getReviewBoardName());
			setReviewBoardDescription(rsk.getReviewBoardDescription());
			setReviewboardAccId(rsk.getReviewboardAccId());
			setBoardGroupAccess(rsk.getBoardGroupAccess());
			setBoardMomLogic(rsk.getBoardMomLogic());
			setReviewBoardDefault(rsk.getReviewBoardDefault());
			setIpAdd(rsk.getIpAdd());
			setCreator(rsk.getCreator());
			setModifiedBy(rsk.getModifiedBy());
			Rlog.debug("reviewboard", "ReviewBoardBean.updateReviewBoard");
			return 0;

		} catch (Exception e) {
			Rlog.fatal("reviewboard"," error in ReviewBoardBean.updateReviewBoard");
			return -2;
		}
	}
}