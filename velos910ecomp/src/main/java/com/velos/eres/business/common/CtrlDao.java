/*
 * Classname : ControlDao
 * 
 * Version information: 1.0
 *
 * Copyright notice: Velos, Inc
 * date: 03/09/2001
 *
 * Author: sonia sahni
 * Data Access  class for Control Table 
 */

package com.velos.eres.business.common;

/* import statements */
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.velos.eres.service.util.Rlog;

/* end of import statements */

/**
 * The ControlDao.<br>
 * <br>
 * Data Access class for control values
 * 
 * @author Sonia Sahni
 * @vesion 1.0 03/09/2001
 */

public class CtrlDao extends CommonDAO {

    
	private ArrayList cValue;

    private ArrayList cDesc;

    private ArrayList cSeq;
    
    private ArrayList cKey;

    private int cRows;

    private ArrayList cCustom1;

    private ArrayList cCustom2;

	/**
     * default constructor
     * 
     * 
     */

    public CtrlDao() {
    	
        cValue = new ArrayList();
        cDesc = new ArrayList();
        cSeq = new ArrayList();
        cKey=new ArrayList();
        cCustom2=new ArrayList();
        cCustom1=new ArrayList();
    }

    /**
     * Gets Control table data for a key value and populates the class
     * attributes
     * 
     * @param cType
     *            Key Value
     */

    public boolean getControlValues(String cType) {
        int rows = 0,sepList=0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String Sql="";
        if (cType.indexOf(",")>=0){ 
        	Sql="select CTRL_KEY, CTRL_DESC, CTRL_VALUE , CTRL_SEQ,ctrl_custom_col1,ctrl_custom_col2 from er_ctrltab where CTRL_KEY in ( "+ cType +") order by CTRL_SEQ";
        	sepList=1;
        }
        else
        {
        	Sql="select CTRL_KEY, CTRL_DESC, CTRL_VALUE , CTRL_SEQ ,ctrl_custom_col1,ctrl_custom_col2 from er_ctrltab where CTRL_KEY = ? order by CTRL_SEQ";
        	sepList=0;
        }
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement(Sql);
            if (sepList==0)     pstmt.setString(1, cType);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCValue(rs.getString("CTRL_VALUE"));
                setCDesc(rs.getString("CTRL_DESC"));
                setCSeq(new Integer(rs.getInt("CTRL_SEQ")));
                setCKey(rs.getString("CTRL_KEY"));
                
                setCCustom2(rs.getString("ctrl_custom_col2"));
                
                setCCustom1(rs.getString("ctrl_custom_col1"));
                
                rows++;
                // Debug.println("GOT ROWS" + rows);
                Rlog.debug("common", "Got Rows" + rows);

            }

            setCRows(rows);
            return true;
        } catch (SQLException ex) {
            // Debug.println("EXCEPTION IN FETCHING FROMCONTROL TABLE " + ex);
            Rlog.fatal("common", "EXCEPTION IN FETCHING FROMCONTROL TABLE "
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    

    public boolean getControlValuesAlphaBetized(String cType, String orderColumn) {
    	int rows = 0,sepList=0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String Sql="";
        if("key".equals(orderColumn)){
        	orderColumn ="CTRL_KEY";
        }else if("desc".equals(orderColumn)){
        	orderColumn ="CTRL_DESC";
        }else if("value".equals(orderColumn)){
        	orderColumn ="CTRL_VALUE";
        }else {
        	orderColumn ="CTRL_SEQ";
        }
        	
        if (cType.indexOf(",")>=0){ 
        	Sql="select CTRL_KEY, CTRL_DESC, CTRL_VALUE , CTRL_SEQ,ctrl_custom_col1,ctrl_custom_col2 from er_ctrltab where CTRL_KEY in ( "+ cType +") order by "+orderColumn;
        	sepList=1;
        }
        else
        {
        	Sql="select CTRL_KEY, CTRL_DESC, CTRL_VALUE , CTRL_SEQ ,ctrl_custom_col1,ctrl_custom_col2 from er_ctrltab where CTRL_KEY = ? order by "+orderColumn;
        	sepList=0;
        }
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement(Sql);
            if (sepList==0)     pstmt.setString(1, cType);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCValue(rs.getString("CTRL_VALUE"));
                setCDesc(rs.getString("CTRL_DESC"));
                setCSeq(new Integer(rs.getInt("CTRL_SEQ")));
                setCKey(rs.getString("CTRL_KEY"));
                
                setCCustom2(rs.getString("ctrl_custom_col2"));
                
                setCCustom1(rs.getString("ctrl_custom_col1"));
                
                rows++;
                // Debug.println("GOT ROWS" + rows);
                Rlog.debug("common", "Got Rows" + rows);

            }

            setCRows(rows);
            return true;
        } catch (SQLException ex) {
            // Debug.println("EXCEPTION IN FETCHING FROMCONTROL TABLE " + ex);
            Rlog.fatal("common", "EXCEPTION IN FETCHING FROMCONTROL TABLE "
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
    }


    /**
     * Gets the data in Control table's ctrl_value column for key as
     * acc_type_rights and specified seq number
     * 
     * @param seqNo
     *            the sequence number for which the value is required
     */

    public String getAccTypeRight(int seqNo) {
        String accTypeRight = "";
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select CTRL_VALUE "
                    + "from er_ctrltab "
                    + "where CTRL_KEY = 'acc_type_rights' and CTRL_SEQ = ?"
                    + " order by CTRL_SEQ");
            pstmt.setInt(1, seqNo);

            ResultSet rs = pstmt.executeQuery();

            rs.next();
            accTypeRight = rs.getString("CTRL_VALUE");
        } catch (SQLException ex) {
            Rlog.fatal("common", "EXCEPTION IN FETCHING FROMCONTROL TABLE "
                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return accTypeRight;
    }

    // GETTER SETTER METHODS

    
    
    public ArrayList getCDesc() {
        return this.cDesc;
    }

    public void setCDesc(ArrayList cDesc) {
        this.cDesc = cDesc;
    }

    public void setCDesc(String str) {
        cDesc.add(str);
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public ArrayList getCSeq() {
        return this.cSeq;
    }

    public void setCSeq(ArrayList cSeq) {
        this.cSeq = cSeq;
    }

    public void setCSeq(Integer iseq) {
        cSeq.add(iseq);
    }

    
    public ArrayList getCValue() {
        return this.cValue;
    }

    public void setCValue(ArrayList cValue) {
        this.cValue = cValue;
    }

    public void setCValue(String str) {
        cValue.add(str);
    }
    
    /**
	 * @return the cKey
	 */
	public ArrayList getCKey() {
		return cKey;
	}

	/**
	 * @param key the cKey to set
	 */
	public void setCKey(ArrayList key) {
		cKey = key;
	}
	
	public void setCKey(String key) {
		cKey.add(key);
	}


    public long getFreeSpace(int accId) {
        long freeSpace = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("select ac_free_space(?) as space from dual");
            pstmt.setInt(1, accId);

            ResultSet rs = pstmt.executeQuery();
            rs.next();
            freeSpace = rs.getLong("space");
        } catch (SQLException ex) {
            Rlog.fatal("common", "EXCEPTION IN FETCHING FROMCONTROL TABLE "
                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }

            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return freeSpace;
    }

    /**
     * Gets Control table data corresponding to ctrl_key and ctrl_value
     * 
     * @param ctrlKey
     *            Key Value
     * @param ctrlValue
     *            Key Value
     */

    public void getControlValues(String ctrlKey, String ctrlValue) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("select CTRL_DESC, CTRL_SEQ from er_ctrltab where CTRL_KEY = ? and upper(ctrl_value) = ? ");
            pstmt.setString(1, ctrlKey);
            pstmt.setString(2, ctrlValue);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCDesc(rs.getString("CTRL_DESC"));
                setCSeq(new Integer(rs.getInt("CTRL_SEQ")));
                rows++;
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("common",
                    "EXCEPTION IN FETCHING FROM CONTROL TABLE IN getControlValues in CtrlDao "
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public String getControlValue(String ctrlkey){
        String ctrlval="";
	PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select CTRL_VALUE "
                    + "from er_ctrltab "
                    + "where CTRL_KEY = ?");
            pstmt.setString(1, ctrlkey);

            ResultSet rs = pstmt.executeQuery();

            rs.next();
            ctrlval = rs.getString("CTRL_VALUE");
        } catch (SQLException ex) {
            Rlog.fatal("common", "EXCEPTION IN FETCHING FROMCONTROL TABLE "
                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return ctrlval;
    }
    
    
    
    //Added by Manimaran  on 19,May for role based access rights in study team
    public int getPkCtrlTab(String ctrlkey){
        int pkCtrlTab=0;
	PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select PK_CTRLTAB "
                    + "from er_ctrltab "
                    + "where CTRL_KEY = ?");
            pstmt.setString(1, ctrlkey);

            ResultSet rs = pstmt.executeQuery();

            rs.next();
            pkCtrlTab = rs.getInt("PK_CTRLTAB");
        } catch (SQLException ex) {
            Rlog.fatal("common", "EXCEPTION IN FETCHING FROMCONTROL TABLE "
                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return pkCtrlTab;
    }

	public ArrayList getCCustom1() {
		return cCustom1;
	}

	public void setCCustom1(ArrayList custom1) {
		cCustom1 = custom1;
	}

	public void setCCustom1(String custom1) {
		cCustom1.add(custom1);
	}

	
	public ArrayList getCCustom2() {
		return cCustom2;
	}

	public void setCCustom2(ArrayList custom2) {
		cCustom2 = custom2;
	}
    
    
	public void setCCustom2(String custom2) {
		cCustom2.add(custom2);
	}
    
	public boolean chechkXmlVerWithDB(String realpath) {
		boolean isSameVer=true;
		  try {
			  
		  File file = new File(realpath);
		  DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		  DocumentBuilder db = dbf.newDocumentBuilder();
		  Document doc = db.parse(file);
		  doc.getDocumentElement().normalize();
		  System.out.println("Root element " + doc.getDocumentElement().getNodeName());
		  NodeList nodeLst = doc.getElementsByTagName("ReleaseInfo");
		  System.out.println("Information of all codes");
		  Node fstNode = nodeLst.item(0);
		  if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
			  
	      Element fstElmnt = (Element) fstNode;
	      NodeList verNo=fstElmnt.getElementsByTagName("VersionNo");
	      Element verElmnt=(Element) verNo.item(0);
	      NodeList verNumLst=verElmnt.getChildNodes();
	      String App_Ver=((Node) verNumLst.item(0)).getNodeValue();
	      if(App_Ver.equals(getControlValue("app_version"))){
	    	  isSameVer=true;
	      		}
	      else{
	    	  isSameVer=false;
	      }
		  	}
		  } catch (Exception e) {
		    e.printStackTrace();	    
		  }
		 return isSameVer;
		  
	}

    // END OF CLASS
}
