package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

public class MemberDao extends CommonDAO implements java.io.Serializable{
	
	private static final long serialVersionUID = 1L;

	private ArrayList pkRBMember;
    
    private ArrayList fkBoardId;
   
    private ArrayList fkMemberRole;
    
    private ArrayList fkMemberUser;
    
    private ArrayList brdMemStatus;
    
    private ArrayList brdMemRights;
    
    private int cRows;  
    
    public MemberDao() {
    	pkRBMember = new ArrayList();
    	fkBoardId = new ArrayList();
    	fkMemberRole = new ArrayList();
    	fkMemberUser = new ArrayList();
    	brdMemStatus = new ArrayList();
    	brdMemRights = new ArrayList();
           
    }
    
    public ArrayList getPkRBMember() {
    	Rlog.debug("member", "MemberDao.getPkRBMember size "+ this.pkRBMember.size());
		return this.pkRBMember;
	}
	public void setPkRBMember(ArrayList pkRBMember) {
		this.pkRBMember = pkRBMember;
	}
	public void setPkRBMember(int pkRBMember) {
		this.pkRBMember.add(pkRBMember);
	}



	public ArrayList getFkBoardId() {
		Rlog.debug("member", "MemberDao.getFkBoardId size "+ this.fkBoardId.size());
		return this.fkBoardId;
	}
	public void setFkBoardId(ArrayList fkBoardId) {
		this.fkBoardId = fkBoardId;
	}
	public void setFkBoardId(int fkBoardId) {
		this.fkBoardId.add(fkBoardId);
	}



	public ArrayList getFkMemberRole() {
		Rlog.debug("member", "MemberDao.getFkMemberRole size "+ this.fkMemberRole.size());
		return this.fkMemberRole;
	}
	public void setFkMemberRole(ArrayList fkMemberRole) {
		this.fkMemberRole = fkMemberRole;
	}
	public void setFkMemberRole(int fkMemberRole) {
		this.fkMemberRole.add(fkMemberRole);
	}



	public ArrayList getFkMemberUser() {
		Rlog.debug("member", "MemberDao.getFkMemberUser size "+ this.fkMemberUser.size());
		return this.fkMemberUser;
	}
	public void setFkMemberUser(ArrayList fkMemberUser) {
		this.fkMemberUser = fkMemberUser;
	}
	public void setFkMemberUser(int fkMemberUser) {
		this.fkMemberUser.add(fkMemberUser);
	}



	public ArrayList getBrdMemStatus() {
		Rlog.debug("member", "MemberDao.getBrdMemStatus size "+ this.brdMemStatus.size());
		return this.brdMemStatus;
	}
	public void setBrdMemStatus(ArrayList brdMemStatus) {
		this.brdMemStatus = brdMemStatus;
	}
	public void setBrdMemStatus(String brdMemStatus) {
		this.brdMemStatus.add(brdMemStatus);
	}



	public ArrayList getBrdMemRights() {
		Rlog.debug("member", "MemberDao.getBrdMemRights size "+ this.brdMemRights.size());
		return this.brdMemRights;
	}
	public void setBrdMemRights(ArrayList brdMemRights) {
		this.brdMemRights = brdMemRights;
	}
	public void setBrdMemRights(String brdMemStatus) {
		this.brdMemStatus.add(brdMemStatus);
	}
	
	public int getcRows() {
		return cRows;
	}
	public void setcRows(int cRows) {
		this.cRows = cRows;
	}
	
	 public void getMemberValues(int fkBoardId) {

	    int rows = 0;
	    PreparedStatement pstmt = null;
	    Connection conn = null;
	    try 
	    {
	            conn = getConnection();
	            String Query= "Select PK_REVIEW_BOARD_MEMBER,FK_REVIEW_BOARD,FK_CODELST_MEMBER_ROLE," 
	            			  +	"FK_USER,BOARD_MEMBER_STATUS,BOARD_MEMBER_RIGHTS " 
	            			  +	"from er_review_board_member where FK_REVIEW_BOARD=?";
        
	            pstmt = conn.prepareStatement(Query);			
	            pstmt.setInt(1, fkBoardId);
	            ResultSet rs = pstmt.executeQuery();
	        while (rs.next()) {
	        	setPkRBMember(new Integer(rs.getInt("PK_REVIEW_BOARD_MEMBER")));
	        	setFkBoardId(new Integer(rs.getInt("FK_REVIEW_BOARD")));
	        	setFkMemberRole(new Integer(rs.getInt("FK_CODELST_MEMBER_ROLE")));
	        	setFkMemberUser(new Integer(rs.getInt("FK_USER")));
	        	setBrdMemStatus(rs.getString("BOARD_MEMBER_STATUS"));
	        	setBrdMemRights(rs.getString("BOARD_MEMBER_RIGHTS"));        	               
	        
	        	Rlog.debug("member", "MemberDao.getMemberValues rows " + rows);
	        }
	    } 
	    catch (SQLException ex) {
        Rlog.fatal("member","MemberDao.getMemberValues EXCEPTION IN FETCHING FROM er_review_board_member table" + ex);
    } finally {
        try {
            if (pstmt != null)
                pstmt.close();
        } catch (Exception e) {
        }
        try {
            if (conn != null)
                conn.close();
        	} 
        catch (Exception e)
	        {
	        }
    	}
	 }
}