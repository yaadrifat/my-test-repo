/*
 * Classname : UserSiteJB
 * 
 * Version information: 1.0
 *
 * Date: 04/23/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sajal
 */

package com.velos.eres.web.userSite;

/* Import Statements */
import com.velos.eres.business.common.UserSiteDao;
import com.velos.eres.business.userSite.impl.UserSiteBean;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.userSiteAgent.impl.UserSiteAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

/**
 * Client side bean for UserSite
 * 
 * @author Sajal
 * @version 1.0 04/23/2003
 */

public class UserSiteJB {

    /**
     * the user site Id
     */
    private int userSiteId;

    /**
     * the user site's user Id
     */
    private String userSiteUserId;

    /**
     * the user site's site Id
     */

    private String userSiteSiteId;

    /**
     * the user site right
     */

    private String userSiteRight;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    // GETTER SETTER METHODS

    /**
     * Returns User Site id
     * 
     * @return User Site id
     */
    public int getUserSiteId() {
        return this.userSiteId;
    }

    /**
     * Sets User Site id
     * 
     * @param userSiteId
     *            User Site id
     */
    public void setUserSiteId(int userSiteId) {
        this.userSiteId = userSiteId;
    }

    /**
     * Returns User Site's User Id
     * 
     * @return User Site's User Id
     */
    public String getUserSiteUserId() {
        return this.userSiteUserId;
    }

    /**
     * Sets User Site's User Id
     * 
     * @param userSiteUserId
     *            User Site's User Id
     */
    public void setUserSiteUserId(String userSiteUserId) {
        this.userSiteUserId = userSiteUserId;
    }

    /**
     * Returns User Site's Site Id
     * 
     * @return User Site's Site Id
     */
    public String getUserSiteSiteId() {
        return this.userSiteSiteId;
    }

    /**
     * Sets User Site's Site Id
     * 
     * @param userSiteSiteId
     *            User Site's Site Id
     */
    public void setUserSiteSiteId(String userSiteSiteId) {
        this.userSiteSiteId = userSiteSiteId;
    }

    /**
     * Returns User Site's Right
     * 
     * @return User Site's Right
     */
    public String getUserSiteRight() {
        return this.userSiteRight;
    }

    /**
     * Sets User Site's Right
     * 
     * @param userSiteRight
     *            User Site's Right
     */
    public void setUserSiteRight(String userSiteRight) {
        this.userSiteRight = userSiteRight;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // end of getter and setter methods

    /**
     * Full arguments constructor.
     */

    // COMMENTED FOR TIME BEING
    /**
     * Constructor
     * 
     * @param userSiteId
     */
    public UserSiteJB(int userSiteId) {
        setUserSiteId(userSiteId);
    }

    /**
     * Default Constructor
     * 
     */
    public UserSiteJB() {
        Rlog.debug("userSite", "UserSiteJB.UserSiteJB() ");
    }

    /**
     * Full Argument Constructor
     * 
     * @param userSiteId
     * @param userSiteUserId
     * @param userSiteSiteId
     * @param userSiteRight
     * @param creator
     * @param modifiedBy
     * @param ipAdd
     */
    public UserSiteJB(int userSiteId, String userSiteUserId,
            String userSiteSiteId, String userSiteRight, String creator,
            String modifiedBy, String ipAdd) {
        setUserSiteId(userSiteId);
        setUserSiteUserId(userSiteUserId);
        setUserSiteSiteId(userSiteSiteId);
        setUserSiteRight(userSiteRight);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        Rlog.debug("userSite", "UserSiteJB.UserSiteJB(all parameters)");
    }

    /**
     * Calls getUserSiteDetails of User Site Session Bean: UserSiteAgentBean
     * 
     * @returns UserSiteBean object
     * @see UserSiteBean
     */
    public UserSiteBean getUserSiteDetails() {
        UserSiteBean ussk = null;

        try {

            UserSiteAgentRObj userSiteAgent = EJBUtil.getUserSiteAgentHome();
            ussk = userSiteAgent.getUserSiteDetails(this.userSiteId);
            Rlog.debug("userSite",
                    "UserSiteJB.getUserSiteDetails() UserSiteStateKeeper "
                            + ussk);
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in getUserSiteDetails() in UserSiteJB " + e);
        }
        if (ussk != null) {
            this.userSiteId = ussk.getUserSiteId();
            this.userSiteUserId = ussk.getUserSiteUserId();
            this.userSiteSiteId = ussk.getUserSiteSiteId();
            this.userSiteRight = ussk.getUserSiteRight();
            this.creator = ussk.getCreator();
            this.modifiedBy = ussk.getModifiedBy();
            this.ipAdd = ussk.getIpAdd();
        }
        return ussk;
    }

    /**
     * Calls setUserSiteDetails of User Site Session Bean: UserSiteAgentBean
     * 
     * @see UserSiteAgentBean
     */
    public void setUserSiteDetails() {
        try {
            UserSiteAgentRObj userSiteAgent = EJBUtil.getUserSiteAgentHome();
            this.setUserSiteId(userSiteAgent.setUserSiteDetails(this
                    .createUserSiteStateKeeper()));
            Rlog.debug("userSite", "UserSiteJB.setUserSiteDetails()");
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in setUserSiteDetails() in UserSiteJB " + e);
        }
    }

    /**
     * Calls updateUserSite of User Site Session Bean: UserSiteAgentBean
     * 
     * @return int 0 if successful, -2 for exception
     * @see UserSiteAgentBean
     */
    public int updateUserSite() {
        int output;
        try {
            UserSiteAgentRObj userSiteAgentRObj = EJBUtil
                    .getUserSiteAgentHome();
            output = userSiteAgentRObj.updateUserSite(this
                    .createUserSiteStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("userSite", "Error in updateUserSite() in UserSiteJB "
                    + e);
            return -2;
        }
        return output;
    }

    /**
     * Calls updateUserSite of User Site Session Bean: UserSiteAgentBean
     * Overloaded method
     * 
     * @param Array
     *            of pkUserSites
     * @param Array
     *            of corresponding rights for each user site
     * @param userId
     * @param IP
     *            Address
     * @returns 0 if successful, -1 exception
     * @see UserSiteAgentBean
     */
    public int updateUserSite(String[] pkUserSites, String[] rights, int usr,
            String ipAdd) {
        int ret = 0;
        try {
            UserSiteAgentRObj userSiteAgentRObj = EJBUtil
                    .getUserSiteAgentHome();
            ret = userSiteAgentRObj.updateUserSite(pkUserSites, rights, usr,
                    ipAdd);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in updateUserSite() calling SP in UserSiteJB " + e);
            return -1;
        }

    }
    /**
     * Calls updateUserSite of User Site Session Bean: UserSiteAgentBean
     * Overloaded method
     * 
     * @param Array
     *            of pkUserSites
     * @param Array
     *            of corresponding rights for each user site
     * @param userId
     * @param IP
     *            Address
     * @returns 0 if successful, -1 exception
     * @see UserSiteAgentBean
     */
    public int updateUserSite(String[] pkUserSites, String[] rights, int usr,int userID,
            String ipAdd) {
        int ret = 0;
        try {
            UserSiteAgentRObj userSiteAgentRObj = EJBUtil
                    .getUserSiteAgentHome();
            ret = userSiteAgentRObj.updateUserSite(pkUserSites, rights, usr,userID,
                    ipAdd);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in updateUserSite() calling SP in UserSiteJB " + e);
            return -1;
        }
    }
    /**
     * Calls getUserSiteTree of User Site Session Bean: UserSiteAgentBean
     * 
     * @param Account
     *            Id
     * @param userId
     * @returns UserSiteDao
     * @see UserSiteAgentBean
     */
    public UserSiteDao getUserSiteTree(int accountId, int loginUser) {
        UserSiteDao userSiteDao = new UserSiteDao();
        try {
            UserSiteAgentRObj userSiteAgentRObj = EJBUtil
                    .getUserSiteAgentHome();
            userSiteDao = userSiteAgentRObj.getUserSiteTree(accountId,
                    loginUser);
        } catch (Exception e) {
            Rlog.fatal("userSite", "Error in getUserSiteTree() in UserSiteJB "
                    + e);
        }
        return userSiteDao;
    }

    /**
     * Get organizations with view access for an account user, calls
     * getSitesWithViewRight of Session Bean
     * 
     * @param Account
     *            Id
     * @param userId
     * @returns UserSiteDao
     * @see UserSiteAgentBean
     */
    public UserSiteDao getSitesWithViewRight(int accId, int userId) {
        UserSiteDao userSiteDao = new UserSiteDao();
        try {
            UserSiteAgentRObj userSiteAgentRObj = EJBUtil
                    .getUserSiteAgentHome();
            userSiteDao = userSiteAgentRObj
                    .getSitesWithViewRight(accId, userId);
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in getSitesWithViewRight() in UserSiteJB " + e);
        }
        return userSiteDao;
    }

    /**
     * Get organizations with new access for an account user, calls
     * getSitesWithNewRight of Session Bean
     * 
     * @param Account
     *            Id
     * @param userId
     * @returns UserSiteDao
     * @see UserSiteAgentBean
     */
    public UserSiteDao getSitesWithNewRight(int accId, int userId) {
        UserSiteDao userSiteDao = new UserSiteDao();
        try {
            UserSiteAgentRObj userSiteAgentRObj = EJBUtil
                    .getUserSiteAgentHome();
            userSiteDao = userSiteAgentRObj.getSitesWithNewRight(accId, userId);
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in getSitesWithNewRight() in UserSiteJB " + e);
        }
        return userSiteDao;
    }

    /**
     * Get organizations with view access for an account user, calls
     * getSitesWithViewRight of Session Bean
     * 
     * @param user
     * @param Account
     *            Id
     * @param def_group
     * @param old
     *            site id
     * @param new
     *            site id
     * @param creator
     * @param ip
     * @param mode
     * @returns 0 for success, -1 for error
     * @see UserSiteAgentBean
     */

    public int createUserSiteData(int user, int account, int def_group,
            int old_site, int def_site, int creator, String ip, String mode) {

        int ret = 0;
        Rlog.debug("userSite", "second step ");
        try {
            UserSiteAgentRObj userSiteAgentRObj = EJBUtil
                    .getUserSiteAgentHome();
            ret = userSiteAgentRObj.createUserSiteData(user, account,
                    def_group, old_site, def_site, creator, ip, mode);
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in createUserSiteData() in UserSiteJB " + e);
            return -1;
        }
        return ret;
    }

    /**
     * Creates a UserSiteBean object with the attribute vales of the bean
     * 
     * @return UserSiteBean
     */
    public UserSiteBean createUserSiteStateKeeper() {
        Rlog.debug("userSite", "UserSiteJB.createUserSiteStateKeeper ");
        return new UserSiteBean(userSiteId, userSiteUserId, userSiteSiteId,
                userSiteRight, creator, modifiedBy, ipAdd);
    }

    /**
     * Calls getRightForUserSite of User Site Session Bean: UserSiteAgentBean
     * 
     * @return right
     * @see UserSiteAgentBean
     */
    public int getRightForUserSite(int userId, int siteId) {
        int output = 0;
        try {
            UserSiteAgentRObj userSiteAgentRObj = EJBUtil
                    .getUserSiteAgentHome();
            output = userSiteAgentRObj.getRightForUserSite(userId, siteId);
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in getRightForUserSite() in UserSiteJB " + e);
        }
        return output;
    }
    
    
    /** Returns the maximum right the user has on any of patient facilities. User's user site access rights are also
     * checked for all the patient facilities*/
    public int getUserPatientFacilityRight(int user , int perPk){
        int output = 0;
        try {
            UserSiteAgentRObj userSiteAgentRObj = EJBUtil
                    .getUserSiteAgentHome();
            output = userSiteAgentRObj.getUserPatientFacilityRight(user, perPk);
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in getUserPatientFacilityRight() in UserSiteJB " + e);
        }
        return output;
    } 
    
    
    

    /*
     * generates a String of XML for the user details entry form.<br><br> Not
     * in use
     */

    /**
     * Not in Use
     * 
     * @return
     */
    public String toXML() {
        Rlog.debug("userSite", "UserSiteJB.toXML()");
        return new String(
                "<?xml version=\"1.0\"?>"
                        + "<?cocoon-process type=\"xslt\"?>"
                        + "<?xml-stylesheet href=\"/stylesheet/form-html.xsl\" type=\"text/xsl\"?>"
                        + "<?xml-stylesheet href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>"
                        +
                        // to be done "<form action=\"usersite.jsp\">" +
                        "<head>"
                        +
                        // to be done "<title>User Site </title>" +
                        "</head>"
                        + "<input type=\"text\" name=\"userSiteId\" value=\""
                        + this.getUserSiteId()
                        + "\" size=\"10\"/>"
                        + "<input type=\"text\" name=\"userSiteUserId\" value=\""
                        + this.getUserSiteUserId()
                        + "\" size=\"10\"/>"
                        + "<input type=\"text\" name=\"userSiteSiteId\" value=\""
                        + this.getUserSiteSiteId()
                        + "\" size=\"10\"/>"
                        + "<input type=\"text\" name=\"userSiteRight\" value=\""
                        + this.getUserSiteRight() + "\" size=\"38\"/>"
                        + "</form>");

    }// end of method

    /**
     * Get all organizations with view access for an account user and org given
     * rights in any of the study, calls getSitesWithViewRight of Session Bean
     * 
     * @param Account
     *            Id
     * @param userId
     * @returns UserSiteDao
     * @see UserSiteAgentBean
     */
    public UserSiteDao getAllSitesWithViewRight(int accId, int userId) {
        UserSiteDao userSiteDao = new UserSiteDao();
        try {
            UserSiteAgentRObj userSiteAgentRObj = EJBUtil
                    .getUserSiteAgentHome();
            userSiteDao = userSiteAgentRObj.getAllSitesWithViewRight(accId,
                    userId);
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in getAllSitesWithViewRight() in UserSiteJB " + e);
        }
        return userSiteDao;
    }

    /**
     * Get all organizations with access rights given for a study
     * 
     * @param int
     *            studyId - Study Id
     * @param int
     *            accId - Account Id
     * @param int
     *            userId - User Id
     */
    public UserSiteDao getSitesWithRightForStudy(int studyId, int accId,
            int userId) {
        UserSiteDao userSiteDao = new UserSiteDao();
        try {
            UserSiteAgentRObj userSiteAgentRObj = EJBUtil
                    .getUserSiteAgentHome();
            userSiteDao = userSiteAgentRObj.getSitesWithRightForStudy(studyId,
                    accId, userId);
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in getSitesWithRightForStudy() in UserSiteJB " + e);
        }
        return userSiteDao;
    }

    public UserSiteDao getSitesToEnrollPat(int studyId, int accId, int userId) {
        UserSiteDao userSiteDao = new UserSiteDao();
        try {
            UserSiteAgentRObj userSiteAgentRObj = EJBUtil
                    .getUserSiteAgentHome();
            userSiteDao = userSiteAgentRObj.getSitesToEnrollPat(studyId, accId,
                    userId);
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in getSitesToEnrollPat() in UserSiteJB " + e);
        }
        return userSiteDao;
    }
    
    /**
     *  Function to get user's right in  patient's registering sites for a given study patient enrollments
     *  
     * @param int
     *            userId - User Id
     * @param int
     *            patprot -patprot id
     */
    
    public int getMaxRightForStudyPatient(int user, int pat, int study) 
    {
        int right = 0;
        try {
            UserSiteAgentRObj userSiteAgentRObj = EJBUtil.getUserSiteAgentHome();
            right = userSiteAgentRObj.getMaxRightForStudyPatient(user, pat, study);
            
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in getMaxRightForStudyPatient() in UserSiteJB " + e);
        }
        return right;
    }
    
    /**JM: 032406 function added 
     * Function to get site names in account
     * @param int
     *    		  accId - Account Id  
     */
    public UserSiteDao getAllSitesInAccount(int accId){
        UserSiteDao userSiteDao = new UserSiteDao();
        try {
            UserSiteAgentRObj userSiteAgentRObj = EJBUtil.getUserSiteAgentHome();
            userSiteDao = userSiteAgentRObj.getAllSitesInAccount(accId);
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in getAllSitesInAccount() in UserSiteJB " + e);
        }
        return userSiteDao;
    }
    
    

}