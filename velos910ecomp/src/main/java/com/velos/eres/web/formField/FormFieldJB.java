/*
 * Classname : FormFieldJB
 * 
 * Version information: 1.0
 *
 * Date: 17/07/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Kaura
 */

package com.velos.eres.web.formField;

/* IMPORT STATEMENTS */
import java.util.Hashtable;

import com.velos.eres.business.formField.impl.FormFieldBean;
import com.velos.eres.service.formFieldAgent.FormFieldAgentRObj;
import com.velos.eres.service.formFieldAgent.impl.FormFieldAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.web.formLib.FormLibJB;
import com.velos.eres.web.formSec.FormSecJB;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for Form Notification
 * 
 * @author Sonia Kaura
 * @version 1.0 24/06/2003
 */

public class FormFieldJB {

    /**
     * The form primary key:pk_formfld
     */

    private int formFieldId;

    /**
     * The foreign key reference from the Field : FK_FORMSEC
     */
    private String formSecId;

    /**
     * The Msg Type, Pop or Notification : FK_FIELD
     */

    private String fieldId;

    /**
     * The Sequence number of the Field: FORMFLD_SEQ
     */
    private String formFldSeq;

    /**
     * Flag to identify whether field is mandatory: 0 - false , 1 - true
     * :FORMFLD_MANDATORY
     * 
     */

    private String formFldMandatory;

    /**
     * Flag to identify whether field will be displayed in filled forms'
     * browser: 0 - false 1 - true :FORMFLD_BROWSERFLG
     * 
     */
    private String formFldBrowserFlg;

    /**
     * Stores XSL for the field :FORMFLD_XSL
     * 
     */

    private String formFldXsl;

    /**
     * Stores JavaScript for the field. Initially it will be used for 'Required'
     * attribute :FORMFLD_JAVASCR
     * 
     */

    private String formFldJavaScr;

    /**
     * This column stores the record type setting: N - New M - Modified D -
     * Deleted : RECORD_TYPE
     */

    private String recordType;

    /**
     * 
     * The ID of the Creator is sotred : CREATOR
     */

    private String creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    private String modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    private String ipAdd;

    /**
     * offline flag
     */
    private String offlineFlag;

    // /////////////////////////////////////////////////////////////////////////////////

    // //////////////////////////////////////////////////////////////////////

    // GETTER AND SETTER METHODS

    /**
     * 
     * 
     * @return formFieldId
     */
    public int getFormFieldId() {
        return formFieldId;
    }

    /**
     * 
     * 
     * @param formFieldId
     */
    public void setFormFieldId(int formFieldId) {
        this.formFieldId = formFieldId;
    }

    /**
     * 
     * 
     * @return formSecId
     */
    public String getFormSecId() {
        return formSecId;

    }

    /**
     * 
     * 
     * @param formSecId
     */
    public void setFormSecId(String formSecId) {

        this.formSecId = formSecId;
    }

    /**
     * 
     * 
     * @return fieldId
     */
    public String getFieldId() {

        return fieldId;

    }

    /**
     * 
     * 
     * @param fieldId
     */
    public void setFieldId(String fieldId) {

        this.fieldId = fieldId;

    }

    /**
     * 
     * 
     * @return formFldSeq
     */
    public String getFormFldSeq() {

        return formFldSeq;

    }

    /**
     * 
     * 
     * @param formFldSeq
     */
    public void setFormFldSeq(String formFldSeq) {

        this.formFldSeq = formFldSeq;
    }

    /**
     * 
     * 
     * @return formFldMandatory
     */
    public String getFormFldMandatory() {

        return formFldMandatory;

    }

    /**
     * 
     * 
     * @param formFldMandatory
     */
    public void setFormFldMandatory(String formFldMandatory) {

        this.formFldMandatory = formFldMandatory;

    }

    /**
     * 
     * 
     * @return formFldBrowserFlg
     */
    public String getFormFldBrowserFlg() {

        return formFldBrowserFlg;
    }

    /**
     * 
     * 
     * @param formFldBrowserFlg
     */
    public void setFormFldBrowserFlg(String formFldBrowserFlg) {
        this.formFldBrowserFlg = formFldBrowserFlg;

    }

    /**
     * 
     * 
     * @return formFldXsl
     */
    public String getFormFldXsl() {

        return formFldXsl;

    }

    /**
     * 
     * 
     * @param formFldXsl
     */
    public void setFormFldXsl(String formFldXsl) {
        this.formFldXsl = formFldXsl;

    }

    /**
     * 
     * 
     * @return formFldJavaScr
     */
    public String getFormFldJavaScr() {

        return formFldJavaScr;
    }

    /**
     * 
     * 
     * @param formFldJavaScr
     */
    public void setFormFldJavaScr(String formFldJavaScr) {

        this.formFldJavaScr = formFldJavaScr;

    }

    /**
     * @return The Creator of the Form Notification
     */

    public String getCreator() {
        return this.creator;
    }

    /**
     * 
     * @param to
     *            set the Creator of the Form Notification
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return the Record Type : m: modified n:new d: delet
     */
    public String getRecordType() {
        return this.recordType;
    }

    /**
     * @param set
     *            the Record Type to be modified,new or delete
     */
    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the IP Address of the Form Notification request
     */

    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Notification request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * @return the offline flag
     */

    public String getOfflineFlag() {
        return this.offlineFlag;
    }

    /**
     * @param set
     *            the offline flag
     */

    public void setOfflineFlag(String offlineFlag) {
        this.offlineFlag = offlineFlag;
    }

    // //////////////////////////////////////////////////////////////////

    // END OF THE GETTER AND SETTER METHODS

    /**
     * Constructor
     * 
     * @param frmNotifyId:
     *            Unique Id constructor
     * 
     */

    public FormFieldJB(int formFieldId) {
        setFormFieldId(formFieldId);
    }

    /**
     * Default Constructor
     */

    public FormFieldJB() {
        Rlog.debug("formfield", "FormFieldJB.FormNotifyJB() ");
    }

    public FormFieldJB(int formFieldId, String formSecId, String fieldId,
            String formFldSeq, String formFldMandatory,
            String formFldBrowserFlg, String formFldXsl, String formFldJavaScr,
            String recordType, String creator, String modifiedBy, String ipAdd,
            String offlineFlag) {

        setFormFieldId(formFieldId);
        setFormSecId(formSecId);
        setFieldId(fieldId);
        setFormFldSeq(formFldSeq);
        setFormFldMandatory(formFldMandatory);
        setFormFldBrowserFlg(formFldBrowserFlg);
        setFormFldXsl(formFldXsl);
        setFormFldJavaScr(formFldJavaScr);
        setRecordType(recordType);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setOfflineFlag(offlineFlag);

    }

    /**
     * Calls getFormFieldDetails of FormField Session Bean: FormFieldAgentBean
     * 
     * @return FormFieldStatKeeper
     * @see FormFieldAgentBean
     */

    public FormFieldBean getFormFieldDetails() {
        FormFieldBean formFldsk = null;
        try {
            FormFieldAgentRObj formFieldAgentRObj = EJBUtil
                    .getFormFieldAgentHome();

            formFldsk = formFieldAgentRObj
                    .getFormFieldDetails(this.formFieldId);
            Rlog.debug("formfield",
                    "FormFieldJB.getFormFieldDetails() FormFieldStateKeeper "
                            + formFldsk);
        } catch (Exception e) {
            Rlog.fatal("formfield", "Error in FormField getFormFieldDetails"
                    + e);
        }

        if (formFldsk != null) {

            this.formFieldId = formFldsk.getFormFieldId();
            this.formSecId = formFldsk.getFormSecId();
            this.fieldId = formFldsk.getFieldId();
            this.formFldSeq = formFldsk.getFormFldSeq();
            this.formFldMandatory = formFldsk.getFormFldMandatory();
            this.formFldBrowserFlg = formFldsk.getFormFldBrowserFlg();
            this.formFldXsl = formFldsk.getFormFldXsl();
            this.formFldJavaScr = formFldsk.getFormFldJavaScr();
            this.recordType = formFldsk.getRecordType();
            this.creator = formFldsk.getCreator();
            this.modifiedBy = formFldsk.getModifiedBy();
            this.ipAdd = formFldsk.getIpAdd();
            this.offlineFlag = formFldsk.getOfflineFlag();

        }

        return formFldsk;

    }

    /**
     * Calls setFormFieldDetails() of Form Field Session Bean:
     * FormFieldAgentBean
     * 
     */

    public int setFormFieldDetails() {
        int toReturn;
        try {

            FormFieldAgentRObj formFieldAgentRObj = EJBUtil
                    .getFormFieldAgentHome();

            FormFieldBean tempStateKeeper = new FormFieldBean();
            tempStateKeeper = this.createFormFieldStateKeeper();
            toReturn = formFieldAgentRObj.setFormFieldDetails(tempStateKeeper);
            this.setFormFieldId(toReturn);

            Rlog.debug("formfield", "FormFieldJB.setFormFieldDetails()");

            return toReturn;

        } catch (Exception e) {
            Rlog.fatal("formfield",
                    "Error in setFormFieldDetails() in FormFieldJB " + e);
            return -2;
        }
    }

    /**
     * 
     * @return a statekeeper object for the FormField Record with the current
     *         values of the bean
     */
    public FormFieldBean createFormFieldStateKeeper() {

        Rlog.debug("formfield",
                "FormFieldJB.createFormFieldStateKeeper browser flag "
                        + this.formFldBrowserFlg);

        return new FormFieldBean(formFieldId, formSecId, fieldId, formFldSeq,
                formFldMandatory, formFldBrowserFlg, formFldXsl,
                formFldJavaScr, recordType, creator, modifiedBy, ipAdd,
                offlineFlag);

    }

    /**
     * Calls updateFormField() of FormField Session Bean: FormFieldAgentBean
     * 
     * @return The status as an integer
     */
    public int updateFormField() {
        int output;
        int fkFormLib;
        Rlog.debug("formfield", "in the update formfield**** ");

        try {
            FormFieldAgentRObj formFieldAgentRObj = EJBUtil
                    .getFormFieldAgentHome();
            output = formFieldAgentRObj.updateFormField(this
                    .createFormFieldStateKeeper());
            if (output >= 0) {
                Rlog.debug("formfield",
                        "in the update formfield before formsec **");
                FormSecJB formSecJB = new FormSecJB();
                formSecJB.setFormSecId(Integer.parseInt(getFormSecId()));
                Rlog.debug("formfield",
                        "in the update formfield before formsec%% "
                                + getFormSecId());

                formSecJB.getFormSecDetails();

                fkFormLib = Integer.parseInt(formSecJB.getFormLibId());

                Rlog
                        .debug("formfield",
                                "in the update formfield before formlib$$ "
                                        + fkFormLib);
                FormLibJB formLibJB = new FormLibJB();
                formLibJB.setFormLibId(fkFormLib);
                formLibJB.getFormLibDetails();

                formLibJB.setFormLibXslRefresh("1");
                formLibJB.setRecordType("M");
                formLibJB.setLastModifiedBy(getModifiedBy());
                formLibJB.setIpAdd(getIpAdd());
                formLibJB.updateFormLib();
                Rlog.debug("formfield",
                        "in the update formfield after formlib 777 ");

            }

        } catch (Exception e) {
            Rlog.fatal("formfield", "EXCEPTION IN SETTING formfieldJB " + e);
            return -2;
        }
        return output;
    }
    
    //Overloaded for INF-18183 ::: Ankit
    public int updateFormField(Hashtable<String, String> auditInfo) {
        int output;
        int fkFormLib;
        Rlog.debug("formfield", "in the update formfield**** ");

        try {
            FormFieldAgentRObj formFieldAgentRObj = EJBUtil
                    .getFormFieldAgentHome();
            output = formFieldAgentRObj.updateFormField(this
                    .createFormFieldStateKeeper(),auditInfo);
            if (output >= 0) {
                Rlog.debug("formfield",
                        "in the update formfield before formsec **");
                FormSecJB formSecJB = new FormSecJB();
                formSecJB.setFormSecId(Integer.parseInt(getFormSecId()));
                Rlog.debug("formfield",
                        "in the update formfield before formsec%% "
                                + getFormSecId());

                formSecJB.getFormSecDetails();
                fkFormLib = Integer.parseInt(formSecJB.getFormLibId());

                Rlog
                        .debug("formfield",
                                "in the update formfield before formlib$$ "
                                        + fkFormLib);
                FormLibJB formLibJB = new FormLibJB();
                formLibJB.setFormLibId(fkFormLib);
                formLibJB.getFormLibDetails();

                formLibJB.setFormLibXslRefresh("1");
                formLibJB.setRecordType("M");
                formLibJB.setLastModifiedBy(getModifiedBy());
                formLibJB.setIpAdd(getIpAdd());
                formLibJB.updateFormLib();
                Rlog.debug("formfield",
                        "in the update formfield after formlib 777 ");

            }

        } catch (Exception e) {
            Rlog.fatal("formfield", "EXCEPTION IN SETTING formfieldJB " + e);
            return -2;
        }
        return output;
    }
    
    public int removeFormField() {

        int output;

        try {

            FormFieldAgentRObj formFieldAgentRObj = EJBUtil
                    .getFormFieldAgentHome();
            output = formFieldAgentRObj.removeFormField(this.formFieldId);
            return output;

        } catch (Exception e) {
            Rlog
                    .fatal("formfield",
                            "in FormFieldJB -removeFormNotify() method");
            return -1;
        }

    }

    // //////////////////////////////////////////
    /**
     * method added by Chennai dev team on 03/12/2004
     * 
     */
    public int removeFormField(int formId, String[] formFieldId1,
            String lastModifiedBy) {

        int output = 1;

        try {

            FormFieldAgentRObj formFieldAgentRObj = EJBUtil
                    .getFormFieldAgentHome();

            formFieldAgentRObj.removeFormField(formId, formFieldId1,
                    lastModifiedBy);
            return output;

        } catch (Exception e) {
            Rlog
                    .fatal(
                            "formfield",
                            "in FormFieldJB -removeFormField(int formId,String[] formFieldId1,String lastModifiedBy) method");
            return -1;
        }

    }
    
    //Overloaded for INF-18183 ::: Ankit
    public int removeFormField(int formId, String[] formFieldId1,
            String lastModifiedBy,Hashtable<String, String> auditInfo) {

        int output = 1;

        try {

            FormFieldAgentRObj formFieldAgentRObj = EJBUtil
                    .getFormFieldAgentHome();

            formFieldAgentRObj.removeFormField(formId, formFieldId1,
                    lastModifiedBy,auditInfo);
            return output;

        } catch (Exception e) {
            Rlog
                    .fatal(
                            "formfield",
                            "in FormFieldJB -removeFormField(int formId,String[] formFieldId1,String lastModifiedBy) method");
            return -1;
        }

    }

    // ////////////

    /**
     * Calls setFormFieldDetails() of Form Field Session Bean:
     * FormFieldAgentBean
     * 
     */

    public int repeatLookupField(int pkField, int formField, int section) {
        int toReturn;
        try {

            FormFieldAgentRObj formFieldAgentRObj = EJBUtil
                    .getFormFieldAgentHome();

            toReturn = formFieldAgentRObj.repeatLookupField(pkField, formField,
                    section);
            return toReturn;

        } catch (Exception e) {
            Rlog.fatal("formfield",
                    "Error in repeatLookupField() in FormFieldJB " + e);
            return -1;
        }
    }

}
