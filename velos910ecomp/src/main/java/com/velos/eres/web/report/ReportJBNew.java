/*
 * Classname : ReportJB.java
 * 
 * Version information
 *
 * Date 06/02/2001
 * 
 * Copyright notice: Aithent
 */

package com.velos.eres.web.report;

import com.velos.eres.business.common.ReportDaoNew;
import com.velos.eres.service.reportAgent.ReportAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/**
 * Client side bean for the Report
 * 
 * @author Sunita Pahwa
 */

public class ReportJBNew {

    public ReportDaoNew getRepXml(int repId, int accId, String args) {
        ReportDaoNew rD = new ReportDaoNew();

        try {
            Rlog.debug("Report", "ReportJB.getRepXml starting");
            ReportAgentRObj repAgentRObj = EJBUtil.getReportAgentHome();
            rD = repAgentRObj.getRepXml(repId, accId, args);
            Rlog.debug("Report", "ReportJB.getRepXml after Dao");
            return rD;
        } catch (Exception e) {
            Rlog.fatal("Report", "Error in getRepXml in ReportJB " + e);
            e.printStackTrace();
            return null;
        }

    }

    public ReportDaoNew getRepXsl(int xslId) {
        ReportDaoNew rD = new ReportDaoNew();

        try {
            Rlog.debug("Report", "ReportJB.getRepXsl starting");

            ReportAgentRObj repAgentRObj = EJBUtil.getReportAgentHome();
            rD = repAgentRObj.getRepXsl(xslId);
            Rlog.debug("Report", "ReportJB.getRepXsl after Dao");
            return rD;
        } catch (Exception e) {
            Rlog.fatal("Report", "Error in getRepXsl in ReportJB " + e);
            e.printStackTrace();
            return null;
        }

    }

    public ReportDaoNew getRepXslList(int repId) {
        ReportDaoNew rD = new ReportDaoNew();

        try {
            Rlog.debug("Report", "ReportJB.getRepXslList starting");

            ReportAgentRObj repAgentRObj = EJBUtil.getReportAgentHome();
            rD = repAgentRObj.getRepXslList(repId);
            Rlog.debug("Report", "ReportJB.getRepXslList after Dao");
            return rD;
        } catch (Exception e) {
            Rlog.fatal("Report", "Error in getRepXslList in ReportJB " + e);
            e.printStackTrace();
            return null;
        }

    }

    public ReportDaoNew getAllRep(int accId) {
        ReportDaoNew rD = new ReportDaoNew();

        try {
            Rlog.debug("Report", "ReportJB.getAllRep starting");

            ReportAgentRObj repAgentRObj = EJBUtil.getReportAgentHome();
            rD = repAgentRObj.getAllRep(accId);
            Rlog.debug("Report", "ReportJB.getAllRep after Dao");
            return rD;
        } catch (Exception e) {
            Rlog.fatal("Report", "Error in getAllRep in ReportJB " + e);
            e.printStackTrace();
            return null;
        }

    }
    
    public ReportDaoNew getReport(int accountId, int reportId) {
        ReportDaoNew rD = new ReportDaoNew();
        try {
            ReportAgentRObj repAgentRObj = EJBUtil.getReportAgentHome();
            rD = repAgentRObj.getReport(accountId, reportId);
            return rD;
        } catch (Exception e) {
            Rlog.fatal("Report", "Error in getReport in ReportJB " + e);
            e.printStackTrace();
            return null;
        }
    }

    public ReportDaoNew getFilterColumns(String repCat) {
        ReportDaoNew rD = new ReportDaoNew();

        try {
            ReportAgentRObj repAgentRObj = EJBUtil.getReportAgentHome();
            rD = repAgentRObj.getFilterColumns(repCat);
            return rD;
        } catch (Exception e) {
            Rlog.fatal("Report", "Error in getFilterColumns in ReportJB " + e);
            e.printStackTrace();
            return null;
        }
        
    }

    public ReportDaoNew getRepHdFtr(int repId, int accId, int userId) {
        ReportDaoNew rD = new ReportDaoNew();
        try {

            ReportAgentRObj repAgentRObj = EJBUtil.getReportAgentHome();
            rD = repAgentRObj.getRepHdFtr(repId, accId, userId);
            Rlog.debug("Report", "ReportJB.getRepHdFtr after Dao");
            return rD;
        } catch (Exception e) {
            Rlog.fatal("Report", "Error in getRepHdFtr in ReportJB " + e);
            e.printStackTrace();
            return null;
        }
    }

    /*
     * @param reportType F(Financial report), S(Study wise report), P(Patient
     * wise report), r (Payment report)
     */

    public ReportDaoNew getRepMileXml(String reportType, int studyId,
            int patId, String startDate, String endDate, String intervalType,
            int orgId) {
        ReportDaoNew rD = new ReportDaoNew();

        try {
            Rlog.debug("Report", "ReportJB.getRepMileXml starting");
            ReportAgentRObj repAgentRObj = EJBUtil.getReportAgentHome();
            rD = repAgentRObj.getRepMileXml(reportType, studyId, patId,
                    startDate, endDate, intervalType, orgId);
            Rlog.debug("Report", "ReportJB.getRepMileXml after Dao");
            return rD;
        } catch (Exception e) {
            Rlog.fatal("Report", "Error in getRepMileXml in ReportJB " + e);
            e.printStackTrace();
            return null;
        }

    }

    /**
     * generates a String of XML for the customer details entry form.<br>
     * <br>
     * Note that it is planned to encapsulate this detail in another object.
     */
    public String toXML() {
        return null;
    }// end of method

}// end of class
