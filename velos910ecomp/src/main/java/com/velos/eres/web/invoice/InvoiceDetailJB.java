/*
 * Classname : 				InvoiceDetailJB
 *
 * Version information : 	1.0
 *
 * Date 					10/19/2005
 *
 * Copyright notice: 		Velos Inc
 *
 * Author 					Sonia Abrol
 */

package com.velos.eres.web.invoice;

/* IMPORT STATEMENTS */


import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

import com.velos.eres.business.common.InvoiceDetailDao;
import com.velos.eres.business.common.MileAchievedDao;
import com.velos.eres.business.invoice.InvoiceDetailBean;
import com.velos.eres.service.invoiceAgent.InvoiceDetailAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.milestone.MilestoneJB;



/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for Invoice Detail module.
 *
 * @author Sonia Abrol
 * @version 1.0, 10/19/2005
 */

public class InvoiceDetailJB {
	/**
	 * the Invoice Primary Key
	 */
	private int id;

	/**
	 * the main Invoice PK
	 */
	private String invPk;

	/**
	 * the associated milestone
	 */
	private String milestone;

	/**
	 * the associated patient
	 */
	private String patient;

	/**
	 * the associated study
	 */
	private String study;

	/**
	 * the associated patient enrollment
	 */
	private String patProt;

	/**
	 * Amount due
	 */
	private String amountDue;

	/**
	 * the amount invoiced
	 */
	private String amountInvoiced;

	/**
	 * Payment due date
	 */
	private String paymentDueDate;


	/**
	 * the id of the user who created the record
	 */
	private String creator;

	/**
	 * Id of the user who last modified the record
	 */
	private String lastModifiedBy;

	/**
	 * the IP address of the user who created or modified the record
	 */
	private String iPAdd;

	/** indicates if it is high level or detailed record*/
	private  String detailType;

	/** indicates if detail records will be displayed in final invoice*/
	private String displayDetail;

	/** the number of milestones achieved for the invoice detail (will only be stored for the high level record)*/
	private String milestonesAchievedCount;

	/**
	 * Achieved on
	 */
	private String achievedOn;

	/**
	 * FK to er_mileachieved, will be used for milestone related records. <br>
	 * (this will help identify the exact milestone achievement record for multiple achievements for the same patient and milestone combination)
	 */
	private String linkMileAchieved;

	public String getLinkMileAchieved() {
		return linkMileAchieved;
	}

	public void setLinkMileAchieved(String linkMileAchieved) {
		this.linkMileAchieved = linkMileAchieved;
	}

	public String getAchievedOn() {
		return achievedOn;
	}

	public void setAchievedOn(String achievedOn) {
		this.achievedOn = achievedOn;
	}


    public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}





	public String getAmountDue() {
		return amountDue;
	}

	public void setAmountDue(String amountDue) {
		this.amountDue = amountDue;
	}

	public String getAmountInvoiced() {
		return amountInvoiced;
	}

	public void setAmountInvoiced(String amountInvoiced) {
		this.amountInvoiced = amountInvoiced;
	}

	public String getInvPk() {
		return invPk;
	}

	public void setInvPk(String invPk) {
		this.invPk = invPk;
	}

	public String getIPAdd() {
		return iPAdd;
	}

	public void setIPAdd(String add) {
		iPAdd = add;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getMilestone() {
		return milestone;
	}

	public void setMilestone(String milestone) {
		this.milestone = milestone;
	}

	public String getPatient() {
		return patient;
	}

	public void setPatient(String patient) {
		this.patient = patient;
	}

	public String getPatProt() {
		return patProt;
	}

	public void setPatProt(String patProt) {
		this.patProt = patProt;
	}


	public String getPaymentDueDate() {
		return paymentDueDate;
	}

	public void setPaymentDueDate(String paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}

	public void setPaymentDueDate(Date paymentDueDate) {
		setPaymentDueDate(DateUtil.dateToString(paymentDueDate));
	}

	public String getStudy() {
		return study;
	}

	public void setStudy(String study) {
		this.study = study;
	}


	public String getDetailType() {
		return detailType;
	}

	public void setDetailType(String detailType) {
		this.detailType = detailType;
	}


	public String getDisplayDetail() {
		return displayDetail;
	}

	public void setDisplayDetail(String displayDetail) {
		this.displayDetail = displayDetail;
	}


	/**
     * Defines an InvoiceDetailJB object with the specified category Id
     */
    public InvoiceDetailJB(int id) {
        setId(id);
    }

    /**
     * Defines an InvoiceDetailJB object with default values for fields
     */
    public InvoiceDetailJB() {
        Rlog.debug("Invoice", "InvoiceDetailJB.InvoiceDetailJB() ");
    };


	public String getMilestonesAchievedCount() {
		return milestonesAchievedCount;
	}

	public void setMilestonesAchievedCount(String milestonesAchievedCount) {
		this.milestonesAchievedCount = milestonesAchievedCount;
	}



    /**
     * Defines an InvoiceDetailJB object with the specified values for the fields
     *
     */
     /** Full Argument constructor*/

    /** Full Argument constructor */
	public InvoiceDetailJB(String amountDue, String amountInvoiced, String creator, int id, String invPk, String iPAdd, String lastModifiedBy, String milestone, String patient, String patProt, String paymentDueDate , String study,
			String displayDetail, String detailType,String achCount,String mileach ) {
		this.amountDue = amountDue;
		this.amountInvoiced = amountInvoiced;
		this.creator = creator;
		this.id = id;
		this.invPk = invPk;
		this.iPAdd = iPAdd;
		this.lastModifiedBy = lastModifiedBy;
		this.milestone = milestone;
		this.patient = patient;
		this.patProt = patProt;
		this.paymentDueDate = paymentDueDate ;
		this.study = study;
		setDisplayDetail(displayDetail);
		setDetailType(detailType);
		setMilestonesAchievedCount(achCount);
		setLinkMileAchieved(mileach);
	}


    /**
     * Retrieves the details of an category in InvoiceStateKeeper Object
     *
     * @return InvoiceDetailBean
     * @see InvoiceDetailBean
     */
    public InvoiceDetailBean getInvoiceDetail() {
        InvoiceDetailBean iBean = null;
        try {
            InvoiceDetailAgentRObj invoiceDetailAgentRObj = EJBUtil.getInvoiceDetailAgentHome();
            iBean = invoiceDetailAgentRObj.getInvoiceDetail(this.id);
            Rlog.debug("Invoice", "In InvoiceDetailJB.getInvoiceDetails() " + iBean);
        } catch (Exception e) {
            Rlog.fatal("Invoice", "Error in getInvoiceDetails() in InvoiceDetailJB "
                            + e);
        }
        if (iBean != null) {
        	setAmountDue(iBean.getAmountDue());
			setAmountInvoiced(iBean.getAmountInvoiced());
			setInvPk(iBean.getInvPk()) ;
			setMilestone(iBean.getMilestone());
			setPatient(iBean.getPatient()) ;
			setPatProt(iBean.getPatProt()) ;
			setPaymentDueDate(iBean.getPaymentDueDate());
			setStudy(iBean.getStudy()) ;
			setCreator(iBean.getCreator());
			setIPAdd(iBean.getIPAdd()) ;
			setLastModifiedBy(iBean.getLastModifiedBy());

			setDisplayDetail(iBean.getDisplayDetail());
			setDetailType(iBean.getDetailType());
			setMilestonesAchievedCount(iBean.getMilestonesAchievedCount());
			setLinkMileAchieved(iBean.getLinkMileAchieved());

        }
        return iBean;
    }

    public void setInvoiceDetails() {
        int output = 0;
        try {
            InvoiceDetailAgentRObj invoiceDetailAgentRObj = EJBUtil.getInvoiceDetailAgentHome();
            output = invoiceDetailAgentRObj.setInvoiceDetail(this.createInvoiceDetailBean());
            this.setId(output);
            Rlog.debug("Invoice", "InvoiceDetailJB.setInvoiceDetails()");



        } catch (Exception e) {
            Rlog
                    .fatal("Invoice", "Error in setInvoiceDetails() in InvoiceDetailJB "
                            + e);
            e.printStackTrace();
        }
    }

    /**
     * Creates a new Invoice record from data from InvoiceDetailDdao
     *
     * @param inv
     *            A InvoiceDetailDao containing patient invoice details.
     * @return The Invoice Id for the last record <br>
     *         0 - if the method fails
     */
    public int setInvoiceDetails(InvoiceDetailDao inv)
    {
	    int output = 0;
	    try {
	        InvoiceDetailAgentRObj invoiceDetailAgentRObj = EJBUtil.getInvoiceDetailAgentHome();
	        output = invoiceDetailAgentRObj.setInvoiceDetails(inv);

	        Rlog.debug("Invoice", "InvoiceDetailJB.setInvoiceDetails()");

	        return output;

	    } catch (Exception e) {
	        Rlog
	                .fatal("Invoice", "Error in setInvoiceDetails() in InvoiceDetailJB "
	                        + e);
	        e.printStackTrace();
	    }
	    return output;
    }


    /**
     * Saves the modified details of the category to the database
     *
     * @return 0 - If update succeeds <BR>
     *         -2 - If update fails
     */
    public int updateInvoice() {
        int output;
        try {
            InvoiceDetailAgentRObj invoiceDetailAgentRObj = EJBUtil.getInvoiceDetailAgentHome();
            output = invoiceDetailAgentRObj.updateInvoiceDetail(this.createInvoiceDetailBean());


        } catch (Exception e) {
            Rlog.fatal("Invoice",    "EXCEPTION IN updateInvoice in InvoiceDetailJB BEAN"
                            + e.getMessage());

            return -2;
        }
        return output;
    }

    /**
     * Uses the values of the current InvoiceDetailJB object to create an
     * InvoiceDetailBean Object
     *
     * @return InvoiceDetailBean object
      */
    public InvoiceDetailBean createInvoiceDetailBean() {
        Rlog.debug("Invoice", "InvoiceDetailJB.createInvoiceDetailBean ");

        Date dtPaymentDue = new Date();
        dtPaymentDue  = DateUtil.stringToDate(this.paymentDueDate, null);

        Date dtAchievedOn = new Date();
        dtAchievedOn  = DateUtil.stringToDate(this.achievedOn , null);


        return new InvoiceDetailBean(amountDue, amountInvoiced, creator, id, invPk, iPAdd, lastModifiedBy, milestone,
        		patient, patProt, dtPaymentDue ,study,displayDetail,detailType, milestonesAchievedCount,dtAchievedOn,linkMileAchieved);
    }

     /**
     * Deletes Invoice Record
     *
     * @return 0 - If update succeeds <BR>
     *         -2 - If update fails
     */
    public int deleteInvoiceDetail() {
        int output;
        try {
            InvoiceDetailAgentRObj invoiceDetailAgentRObj = EJBUtil.getInvoiceDetailAgentHome();
            output = invoiceDetailAgentRObj.deleteInvoiceDetail(this.id);
        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "EXCEPTION IN deleteInvoice"      + e.getMessage());

            return -2;
        }
        return output;
    }

    /** returns InvoiceDetailDao with details of a generated invoice (for a milestone type)*/
    public InvoiceDetailDao getInvoiceDetailsForMilestoneType(int invId, String mileStoneType, Hashtable htAdditionalParam)
    {
    	InvoiceDetailDao inv = new InvoiceDetailDao();

    	   try {
               InvoiceDetailAgentRObj invoiceDetailAgentRObj = EJBUtil.getInvoiceDetailAgentHome();
               inv = invoiceDetailAgentRObj.getInvoiceDetailsForMilestoneType(invId,mileStoneType,htAdditionalParam);
           } catch (Exception e) {
               Rlog.fatal("Invoice",
                       "EXCEPTION IN getInvoiceDetailsForMilestoneType"      + e.getMessage());


           }

           return inv;

    }
    
    /** returns InvoiceDetailDao with details of a generated invoice (for Event and Additional milestone type)*/
    public InvoiceDetailDao  getInvoiceDetailsForMilestoneTypeEMAM(int invId, String mileStoneType, Hashtable htAdditionalParam)
    {
    	InvoiceDetailDao inv = new InvoiceDetailDao();

    	   try {
               InvoiceDetailAgentRObj invoiceDetailAgentRObj = EJBUtil.getInvoiceDetailAgentHome();
               inv = invoiceDetailAgentRObj.getInvoiceDetailsForMilestoneTypeEMAM(invId,mileStoneType,htAdditionalParam);
           } catch (Exception e) {
               Rlog.fatal("Invoice",
                       "EXCEPTION IN getInvoiceDetailsForMilestoneTypeEMAM"      + e.getMessage());


           }

           return inv;

    }

    /**
     * Updates Invoice detail data from InvoiceDetailDdao
     *
     * @param inv
     *            A InvoiceDetailDao containing patient invoice details.
     * @return The Invoice Id for the last record <br>
     *         0 - if the method fails
     */
    public int updateInvoiceDetails(InvoiceDetailDao inv)
    {
	    int output = 0;
	    try {
	        InvoiceDetailAgentRObj invoiceDetailAgentRObj = EJBUtil.getInvoiceDetailAgentHome();
	        output = invoiceDetailAgentRObj.updateInvoiceDetails(inv);

	        Rlog.debug("Invoice", "InvoiceDetailJB.updateInvoiceDetails(inv)");

	    	//all rows processed

	        ArrayList arLastModifiedBy = new ArrayList<String>();
	    	ArrayList arIPAdd = new ArrayList<String>();
	    	ArrayList arInvPK = new ArrayList<String>();

	    	String invPkVal = "";
	    	String last_modified_by = "";
	    	String ipAdd = "";

			arLastModifiedBy = inv.getLastModifiedBy();
    		arIPAdd = inv.getIPAdd() ;
    		arInvPK = inv.getInvPk();

    		if (arInvPK.size() > 0)
    		{
    			invPkVal =(String) arInvPK.get(0);
    			last_modified_by = (String)arLastModifiedBy.get(0);
    			ipAdd = (String)arIPAdd.get(0);
    		}


	    	inv.synchInvoicedAmount(EJBUtil.stringToNum(invPkVal),EJBUtil.stringToNum(last_modified_by),ipAdd);


	        return output;

	    } catch (Exception e) {
	        Rlog
	                .fatal("Invoice", "Error in updateInvoiceDetails() in InvoiceDetailJB "
	                        + e);
	        e.printStackTrace();
	    }
	    return output;
    }

    /*
     * Method to generate Invoice Details (FIN-22378)
     */
    
    public int generateInvoiceDetails(int invoicePK, int studyId, int userId, String ipAdd, 
    		ArrayList mileAchiveList,  ArrayList milestoneList,	ArrayList milestoneAmtList, ArrayList invoiceAmtList) {
        int returnVal = 0;

        try {
        	int headerIndex = 0;
        	ArrayList arrayZeros = new ArrayList(); //Array list containing zeros
        	
        	ArrayList arrayEmptyStrings = new ArrayList(); //Array list containing empty strings
        	
        	ArrayList doneList = new ArrayList();
        	ArrayList invoicePks = new ArrayList();
	    	ArrayList milestoneFks = new ArrayList();
	    	ArrayList patients = new ArrayList();
	    	ArrayList studyIds = new ArrayList();
	    	ArrayList ipAdds = new ArrayList();
	    	ArrayList detailTypes = new ArrayList();
	    	ArrayList achieveDates = new ArrayList();
	    	ArrayList fkMileAchieved = new ArrayList();
	    	ArrayList achievedCounts = new ArrayList();
	    	ArrayList displayDetails = new ArrayList();
	    	
	    	ArrayList amountDues = new ArrayList();
	    	ArrayList invoicedNow  = new ArrayList();
	    	ArrayList userIds = new ArrayList();
	    	
        	for (int iX=0; iX<mileAchiveList.size(); iX++) {
        		doneList.add("0");
        		arrayZeros.add("0");
        		arrayEmptyStrings.add("");

        		invoicePks.add(""+invoicePK);
        		studyIds.add(""+studyId);
        		ipAdds.add(ipAdd);
        		userIds.add(""+userId);
        	}

        	for (int iX=0; iX<mileAchiveList.size(); iX++) {
    			String milestoneInvoiced = (String)doneList.get(iX);
    			if (mileAchiveList.get(iX) == null || "1".equals(milestoneInvoiced)) { continue; }
    	    	
    			int mileAchieveId = StringUtil.stringToNum(""+mileAchiveList.get(iX));
    			int milestoneId = StringUtil.stringToNum(""+milestoneList.get(iX));
    			
    			MilestoneJB milestoneB = new MilestoneJB();
    			milestoneB.setId(milestoneId);
    	    	milestoneB.getMilestoneDetails();
    	    	
    	    	milestoneFks.add(""+milestoneId);
    	    	patients.add("0");
    	    	detailTypes.add("H");
    	    	achieveDates.add("");
    	    	fkMileAchieved.add("0");
        		
        		if (mileAchieveId > 0){
        			displayDetails.add("1");
        			
        			/*** Special Actions for headers, One additional row. ***/
        	    	headerIndex = fkMileAchieved.size()-1;
        	    	arrayZeros.add("0");
            		arrayEmptyStrings.add("");

            		invoicePks.add(""+invoicePK);
            		studyIds.add(""+studyId);
            		ipAdds.add(ipAdd);
            		userIds.add(""+userId);
            		/*** End of special actions ***/
        		}else{
        			//These are additional milestones. 
        			displayDetails.add("0");
        			/*** Special Actions for headers are not applicable to additional milestones. ***/
        		}
    	    	
        		int achievedCount = 0; 
    	    	achievedCounts.add("0");
    	    	
    	    	String amountDueStr = milestoneB.getMilestoneAmount(); 
    	    	amountDueStr = milestoneAmtList.get(iX).toString();
    	    	String amountInvoicedStr = invoiceAmtList.get(iX).toString();

    	    	double amountDue = 0.0, amountPrevInvoiced = 0.0, amountInvoiced = 0.0;
    	    	amountDue += Double.parseDouble(amountDueStr); 
    	    	amountInvoiced += Double.parseDouble(amountInvoicedStr);
    	    	
    	    	amountDues.add(amountDueStr);
    	    	invoicedNow.add(amountInvoicedStr);
    			
    			MileAchievedDao mileAchieveDao = new MileAchievedDao(); 
    			if (mileAchieveId > 0){
	    			mileAchieveDao.setId(""+mileAchieveId);
	    			mileAchieveDao.getAchievementDetails();
	    			ArrayList milePatients = mileAchieveDao.getPatient();
	    			ArrayList mileAchieveDates = mileAchieveDao.getAchievedOn();

	    			milestoneFks.add(""+milestoneId);
	    			patients.add(milePatients.get(0).toString());
	    	    	detailTypes.add("D");
	    	    	achieveDates.add(mileAchieveDates.get(0).toString());
	    	    	fkMileAchieved.add(""+mileAchieveId);
	    	    	achievedCounts.add("0"); achievedCount++;
	    	    	displayDetails.add("1");

	    	    	amountDues.add(amountDueStr);
	    	    	invoicedNow.add(amountInvoicedStr);
	    			
	    			doneList.set(iX, "1");
	    			
	    			for (int iY=0; iY<mileAchiveList.size(); iY++) {
	        			String alreadyInvoiced = (String)doneList.get(iY);
	        			if (mileAchiveList.get(iY) == null || "1".equals(alreadyInvoiced)) { continue; }
	        			
	        			int milestoneIdMore = StringUtil.stringToNum(""+milestoneList.get(iY));
	        			int mileAchiveIdMore = StringUtil.stringToNum(""+mileAchiveList.get(iY));
	        			
	        			if (milestoneIdMore == milestoneId && mileAchiveIdMore != mileAchieveId){
	        				MileAchievedDao mileAchieveDaoMore = new MileAchievedDao(); 
	        				mileAchieveDaoMore.setId(""+mileAchiveIdMore);
	        				mileAchieveDaoMore.getAchievementDetails();

	        				ArrayList milePatientsMore = mileAchieveDaoMore.getPatient();
	            			ArrayList mileAchieveDatesMore = mileAchieveDao.getAchievedOn();
	
	        				milestoneFks.add(""+milestoneId);
	            			patients.add(milePatientsMore.get(0).toString());
	            	    	detailTypes.add("D");
	            	    	achieveDates.add(mileAchieveDatesMore.get(0).toString());
	            	    	fkMileAchieved.add(""+mileAchiveIdMore);
	            	    	achievedCounts.add("0"); achievedCount++;
	            	    	displayDetails.add("1");
	            	    	
	            	    	amountDueStr = milestoneAmtList.get(iY).toString();
	            	    	amountInvoicedStr = invoiceAmtList.get(iY).toString();    	    	
	            	    	amountDues.add(amountDueStr);
	            	    	invoicedNow.add(amountInvoicedStr);

	            	    	amountDue += Double.parseDouble(amountDueStr); 
	            	    	amountInvoiced += Double.parseDouble(amountInvoicedStr);
	            	    	
	            			doneList.set(iY, "1");
	        			}
	    			}
        			achievedCounts.set(headerIndex, ""+achievedCount);
        			amountDues.set(headerIndex, ""+amountDue);
        			invoicedNow.set(headerIndex, ""+amountInvoiced);
    			}else{
    				//These are additional milestones. Invoice Details are not created for such milestones.    			
	    			doneList.set(iX, "1");
    			}
    		}
	    	
        	InvoiceDetailDao invDetDao = new InvoiceDetailDao();   	
        	invDetDao.setAmountDue(amountDues); 
			invDetDao.setAmountInvoiced(invoicedNow); 
			invDetDao.setInvPk(invoicePks) ;
			invDetDao.setMilestone(milestoneFks); 
			invDetDao.setPatient(patients) ;
			invDetDao.setPatProt(arrayZeros);
			invDetDao.setPaymentDueDate(arrayEmptyStrings); 
			invDetDao.setStudy(studyIds);
			invDetDao.setCreator(userIds); 
			invDetDao.setIPAdd(ipAdds) ;
			invDetDao.setDisplayDetail(displayDetails);
			invDetDao.setDetailType(detailTypes);
			invDetDao.setMilestonesAchievedCount(achievedCounts);
			invDetDao.setLastModifiedBy(arrayZeros);
			invDetDao.setAchDate(achieveDates) ;
			invDetDao.setLinkMileAchieved(fkMileAchieved);
        	
			InvoiceDetailAgentRObj invoiceDetailAgentRObj = EJBUtil.getInvoiceDetailAgentHome();
			returnVal = invoiceDetailAgentRObj.setInvoiceDetails(invDetDao);
			
            return returnVal;

        } catch (Exception e) {
            Rlog.fatal("invoice",
                    "EXCEPTION in InvoiceDetailJB.generateInvoiceDetails "
                            + e);
            return -1;
        } finally {
        	return returnVal;
        }
    }
    //end of class
}
