/*
 * Classname : UserSessionLogJB
 * 
 * Version information: 1.0
 *
 * Date: 03/04/2003
 * 
 * Copyright notice: Velos, Inc
 */

package com.velos.eres.web.userSessionLog;

/* Import Statements */
import com.velos.eres.business.userSessionLog.impl.UserSessionLogBean;
import com.velos.eres.service.userSessionLogAgent.UserSessionLogAgentRObj;
import com.velos.eres.service.userSessionLogAgent.impl.UserSessionLogAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

/**
 * Client side bean for milepayment
 * 
 * @author Sajal
 * @version 1.0 03/04/2003
 */

public class UserSessionLogJB {
    /**
     * the UserSessionLog Id
     */
    private int uslId;

    /**
     * the fk_usersession
     */
    private String userSession;

    /**
     * the URL
     */
    private String uslURL;

    /**
     * the Access Time
     */
    private String uslAccessTime;

    /*
     * the URL parameters
     */
    private String uslURLParam;

    /*
     * the module name
     */
    private String uslModule;
    
    /*
     * Primary key of the Entity user has viewed/changed
     */
    public Long entityId;
    
    /*
     * Type of the entity
     */
    public String entityType;
    
    /*
     * Sub Module User Focused On
     */
    public String entityIdentifier;
    
    /*
     * Code of the event performed(Add,View,Edit,Delete)
     */
    public String eventCode;

    // GETTER SETTER METHODS

    public int getUslId() {
        return this.uslId;
    }

    public void setUslId(int uslId) {
        this.uslId = uslId;
    }

    public String getUserSession() {
        return this.userSession;
    }

    public void setUserSession(String userSession) {
        this.userSession = userSession;
    }

    public String getUslURL() {
        return this.uslURL;
    }

    public void setUslURL(String uslURL) {
        this.uslURL = uslURL;
    }

    public String getUslAccessTime() {
        return this.uslAccessTime;
    }

    public void setUslAccessTime(String uslAccessTime) {
        Rlog.debug("usl", "UserSessionLogJB.setUslAccessTime uslAccessTime "
                + uslAccessTime);
        this.uslAccessTime = uslAccessTime;
    }

    public String getUslURLParam() {
        return this.uslURLParam;
    }

    public void setUslURLParam(String uslURLParam) {
        this.uslURLParam = uslURLParam;
    }

    public String getUslModule() {
        return this.uslModule;
    }

    public void setUslModule(String uslModule) {
        this.uslModule = uslModule;
    }
    
    public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public String getEntityIdentifier() {
		return entityIdentifier;
	}

	public void setEntityIdentifier(String entityIdentifier) {
		this.entityIdentifier = entityIdentifier;
	}

	public String getEventCode() {
		return eventCode;
	}

	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}

    // end of getter and setter methods

    /**
     * Constructor
     * 
     * @param userSessionLogId
     */
    public UserSessionLogJB(int uslId) {
        setUslId(uslId);
    }

    /**
     * Default Constructor
     * 
     */
    public UserSessionLogJB() {
    }

    /**
     * Full Argument Constructor
     */
    public UserSessionLogJB(int uslId, String userSession, String uslURL,
            String uslAccessTime, String uslURLParam, String uslModule) {
        setUslId(uslId);
        setUserSession(userSession);
        setUslURL(uslURL);
        setUslAccessTime(uslAccessTime);
        setUslURLParam(uslURLParam);
        setUslModule(uslModule);
    }

    /**
     * Calls getUserSessionLogDetails of UserSessionLog Session Bean:
     * UserSessionLogAgentBean
     * 
     * @returns UserSessionLogStateKeeper object
     * @see UserSessionLogAgentBean
     */
    public UserSessionLogBean getUserSessionLogDetails() {
        UserSessionLogBean uslsk = null;

        try {

            UserSessionLogAgentRObj userSessionLogAgent = EJBUtil
                    .getUserSessionLogAgentHome();
            uslsk = userSessionLogAgent.getUserSessionLogDetails(this.uslId);
        } catch (Exception e) {
            Rlog.fatal("userSessionLog",
                    "Error in getUserSessionLogDetails() in UserSessionLogJB "
                            + e);
        }
        if (uslsk != null) {
            this.uslId = uslsk.getId();
            setUserSession(uslsk.getUserSession());
            setUslURL(uslsk.getUslURL());
            setUslAccessTime(uslsk.getUslAccessTime());
            setUslURLParam(uslsk.getUslURLParam());
            setUslModule(uslsk.getUslURLModule());
        }
        return uslsk;
    }

    /**
     * Calls setUserSessionLogDetails of UserSessionLog Session Bean:
     * userSessionLogAgentBean
     * 
     * @see userSessionLogAgentBean
     */
    public void setUserSessionLogDetails() {
        try {
            UserSessionLogAgentRObj userSessionLogAgent = EJBUtil
                    .getUserSessionLogAgentHome();
            this.setUslId(userSessionLogAgent.setUserSessionLogDetails(this
                    .createUserSessionLogStateKeeper()));
        } catch (Exception e) {
            Rlog.debug("userSessionLog",
                    "Error in setUserSessionLogDetails() in userSessionLogJB "
                            + e);
        }
    }

    /**
     * Calls updateUserSessionLog of UserSessionLog Session Bean:
     * userSessionLogAgentBean
     * 
     * @return int 0 if successful, -2 for exception
     * @see UserSessionLogAgentBean
     */
    public int updateUserSessionLog() {
        int output;
        try {
            UserSessionLogAgentRObj userSessionLogAgentRObj = EJBUtil
                    .getUserSessionLogAgentHome();
            output = userSessionLogAgentRObj.updateUserSessionLog(this
                    .createUserSessionLogStateKeeper());
        } catch (Exception e) {
            Rlog
                    .fatal("userSessionLog", "UserSessionLogJB.UserSessionLog "
                            + e);
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * Creates a UserSessionLogStateKeeper onject with the attribute vales of
     * the bean
     * 
     * @return UserSessionLogStateKeeper
     */
    public UserSessionLogBean createUserSessionLogStateKeeper() {

    	if(entityId!=null && entityType!=null && entityIdentifier!=null && eventCode!=null){
    		return new UserSessionLogBean(uslId, userSession, uslAccessTime, uslURLParam, uslModule, uslURL,
    				entityId, entityType, entityIdentifier, eventCode);
    	}else{
        return new UserSessionLogBean(uslId, userSession, uslAccessTime,
                uslURLParam, uslModule, uslURL);
    	}
    }

    
} // end of class
