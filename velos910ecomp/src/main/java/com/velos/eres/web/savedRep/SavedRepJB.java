/*
 * Classname : SavedRepJB.java
 * 
 * Version information
 *
 * Date 05/30/2002
 * 
 * Copyright notice: Aithent 
 */

package com.velos.eres.web.savedRep;

import com.velos.eres.business.common.SavedRepDao;
import com.velos.eres.business.savedRep.impl.SavedRepBean;
import com.velos.eres.service.savedRepAgent.SavedRepAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/**
 * Client side bean for SavedRep
 * 
 * @author Arvind
 */

public class SavedRepJB {
    /**
     * the savedRep Id
     */
    private int savedRepId;

    /**
     * the Therapeutic area from codelist
     */
    private String savedRepStudyId;

    /**
     * the savedRep Report Id
     */
    private String savedRepReportId;

    /**
     * the savedRep Report Name
     */
    private String savedRepRepName;

    /**
     * the savedRep As Of Date
     */
    private String savedRepAsOfDate;

    /**
     * the savedRep User Id
     */
    private String savedRepUserId;

    /**
     * the savedRep Patient Id
     */
    private String savedRepPatId;

    /**
     * the savedRep Filter
     */
    private String savedRepFilter;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    // GETTER SETTER METHODS

    public int getSavedRepId() {
        return this.savedRepId;
    }

    public void setSavedRepId(int savedRepId) {
        this.savedRepId = savedRepId;
    }

    public String getSavedRepStudyId() {
        return this.savedRepStudyId;
    }

    public void setSavedRepStudyId(String savedRepStudyId) {
        this.savedRepStudyId = savedRepStudyId;
    }

    public String getSavedRepReportId() {
        return this.savedRepReportId;
    }

    public void setSavedRepReportId(String savedRepReportId) {
        this.savedRepReportId = savedRepReportId;
    }

    public String getSavedRepRepName() {
        return this.savedRepRepName;
    }

    public void setSavedRepRepName(String savedRepRepName) {
        this.savedRepRepName = savedRepRepName;
    }

    public String getSavedRepAsOfDate() {
        return this.savedRepAsOfDate;
    }

    public void setSavedRepAsOfDate(String savedRepAsOfDate) {
        this.savedRepAsOfDate = savedRepAsOfDate;
    }

    public String getSavedRepUserId() {
        return this.savedRepUserId;
    }

    public void setSavedRepUserId(String savedRepUserId) {
        this.savedRepUserId = savedRepUserId;
    }

    public String getSavedRepPatId() {
        return this.savedRepPatId;
    }

    public void setSavedRepPatId(String savedRepPatId) {
        this.savedRepPatId = savedRepPatId;
    }

    public String getSavedRepFilter() {
        return this.savedRepFilter;
    }

    public void setSavedRepFilter(String savedRepFilter) {
        this.savedRepFilter = savedRepFilter;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
        Rlog.debug("savedRep", "SavedRepJB.setCreator creator " + creator);
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
        Rlog.debug("savedRep", "SavedRepJB.setModifiedBy modifiedBy "
                + modifiedBy);
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
        Rlog.debug("savedRep", "SavedRepJB.setIpAdd ipAdd " + ipAdd);
    }

    // END OF GETTER SETTER METHODS

    public SavedRepJB(int savedRepId) {
        setSavedRepId(savedRepId);
        Rlog.debug("savedRep", "Constructor of SavedRepJB with savedRepId ");
    }

    public SavedRepJB() {
        Rlog.debug("savedRep", "Blank constructor of SavedRepJB");
    }

    /**
     * Full arguments constructor.
     */

    public SavedRepJB(int savedRepId, String savedRepStudyId,
            String savedRepReportId, String savedRepRepName,
            String savedRepAsOfDate, String savedRepUserId,
            String savedRepPatId, String savedRepFilter, String creator,
            String modifiedBy, String ipAdd) {
        setSavedRepId(savedRepId);
        setSavedRepStudyId(savedRepStudyId);
        setSavedRepReportId(savedRepReportId);
        setSavedRepRepName(savedRepRepName);
        setSavedRepAsOfDate(savedRepAsOfDate);
        setSavedRepUserId(savedRepUserId);
        setSavedRepPatId(savedRepPatId);
        setSavedRepFilter(savedRepFilter);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        Rlog.debug("savedRep", "Full arguments constructor of SavedRepJB");
    }

    public SavedRepBean getSavedRepDetails() {
        SavedRepBean srsk = null;
        try {

            SavedRepAgentRObj savedRepAgentRObj = EJBUtil
                    .getSavedRepAgentHome();
            srsk = savedRepAgentRObj.getSavedRepDetails(this.savedRepId);

        } catch (Exception e) {
            Rlog.fatal("savedRep",
                    "Error in getSavedRepDetails() in SavedRepJB " + e);
        }
        if (srsk != null) {

            this.savedRepId = srsk.getSavedRepId();
            this.savedRepStudyId = srsk.getSavedRepStudyId();
            this.savedRepReportId = srsk.getSavedRepReportId();
            this.savedRepRepName = srsk.getSavedRepRepName();
            this.savedRepAsOfDate = srsk.getSavedRepAsOfDate();
            this.savedRepUserId = srsk.getSavedRepUserId();
            this.savedRepPatId = srsk.getSavedRepPatId();
            this.savedRepFilter = srsk.getSavedRepFilter();
            this.creator = srsk.getCreator();
            this.modifiedBy = srsk.getModifiedBy();
            this.ipAdd = srsk.getIpAdd();
        }
        return srsk;
    }

    public int setSavedRepDetails() {
        try {

            SavedRepAgentRObj savedRepAgentRObj = EJBUtil
                    .getSavedRepAgentHome();
            this.setSavedRepId(savedRepAgentRObj.setSavedRepDetails(this
                    .createSavedRepStateKeeper()));
            Rlog.debug("savedRep", "SavedRepJB.setSavedRepDetails()");

        } catch (Exception e) {
            Rlog.fatal("savedRep",
                    "Error in setSavedRepDetails() in SavedRepJB " + e);
            return -2;
        }
        return this.getSavedRepId();
    }

    public int updateSavedRep() {
        int output;
        try {

            SavedRepAgentRObj savedRepAgentRObj = EJBUtil
                    .getSavedRepAgentHome();
            output = savedRepAgentRObj.updateSavedRep(this
                    .createSavedRepStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("savedRep",
                    "EXCEPTION IN SETTING SAVEDREP DETAILS TO  SESSION BEAN"
                            + e);
            return -2;
        }
        return output;
    }

    public SavedRepBean createSavedRepStateKeeper() {

        return new SavedRepBean(savedRepId, savedRepStudyId, savedRepReportId,
                savedRepRepName, savedRepAsOfDate, savedRepUserId,
                savedRepPatId, savedRepFilter, creator, modifiedBy, ipAdd);

    }

    public SavedRepDao getAllReps(int studyId) {
        SavedRepDao savedRepDao = new SavedRepDao();
        try {

            SavedRepAgentRObj savedRepAgentRObj = EJBUtil
                    .getSavedRepAgentHome();
            savedRepDao = savedRepAgentRObj.getAllReps(studyId);
            Rlog.debug("savedRep", "SavedRepDaoJB.getAllReps after Dao");
            return savedRepDao;
        } catch (Exception e) {
            Rlog.fatal("savedRep",
                    "Error in getAllReps(int studyId) in SavedRepJB " + e);
            return null;
        }
    }

    public SavedRepDao getReportContent(int savedRepId) {
        SavedRepDao savedRepDao = new SavedRepDao();
        try {
            Rlog.debug("savedRep", "SavedRepDaoJB.getReportContent starting");
            SavedRepAgentRObj savedRepAgentRObj = EJBUtil
                    .getSavedRepAgentHome();
            savedRepDao = savedRepAgentRObj.getReportContent(savedRepId);
            Rlog.debug("savedRep", "SavedRepDaoJB.getReportContent after Dao");
            return savedRepDao;
        } catch (Exception e) {
            Rlog.fatal("savedRep", "Error in getReportContent in SavedRepJB "
                    + e);
            return null;
        }
    }

    public SavedRepDao validateReportName(int studyId, String savedRepName) {
        SavedRepDao savedRepDao = new SavedRepDao();
        try {
            Rlog.debug("savedRep", "SavedRepDaoJB.validateReportName starting");

            SavedRepAgentRObj savedRepAgentRObj = EJBUtil
                    .getSavedRepAgentHome();
            savedRepDao = savedRepAgentRObj.validateReportName(studyId,
                    savedRepName);
            Rlog
                    .debug("savedRep",
                            "SavedRepDaoJB.validateReportName after Dao");
            return savedRepDao;
        } catch (Exception e) {
            Rlog.fatal("savedRep", "Error in validateReportName in SavedRepJB "
                    + e);
            return null;
        }
    }

    public int removeSavedRep(int savedRepId) {

        int output = 0;

        try {

            SavedRepAgentRObj savedRepAgentRobj = EJBUtil
                    .getSavedRepAgentHome();
            output = savedRepAgentRobj.removeSavedRep(savedRepId);

        } catch (Exception e) {
            Rlog.fatal("savedRep", "Exception in removing savedRep");
            return -1;
        }
        return output;
    }

}// end of class

