/*
 * Classname : extURightsJB.java
 * 
 * Version information
 *
 * Date 03/16/2001
 * 
 * Copyright notice: Aithent Technologies 
 */

package com.velos.eres.web.extURights;

import java.util.ArrayList;

import com.velos.eres.business.extURights.impl.ExtURightsBean;
import com.velos.eres.service.extURightsAgent.ExtURightsAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/**
 * Client side bean for the External Users Study Rights
 * 
 * @author Sonia Sahni
 */

public class ExtURightsJB {

    /**
     * the id
     */
    private int id;

    /**
     * Array List of Feature Sequence
     */
    private ArrayList grSeq;

    /**
     * the Array List of Feature Rights
     */
    private ArrayList ftrRights;

    /**
     * the Array List containing reference name of the feature
     */
    private ArrayList grValue;

    /**
     * the Array List containing user friendly name of the feature
     */
    private ArrayList grDesc;

    private String studyId;

    private String userId;

    private String grantor;

    private String xStatus;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    // GETTER SETTER METHODS

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList getGrSeq() {
        return this.grSeq;
    }

    public void setGrSeq(String seq) {
        grSeq.add(seq);
    }

    public void setGrSeq(ArrayList seq) {
        this.grSeq = seq;
    }

    public ArrayList getFtrRights() {
        return this.ftrRights;
    }

    public String getFtrRightsBySeq(String seq) {
        // int count = Integer.parseInt(seq) - 1;
        Rlog.debug("studyrights", "Getting Rights By Sequence"
                + this.ftrRights.get(Integer.parseInt(seq) - 1));
        return this.ftrRights.get(Integer.parseInt(seq) - 1).toString();
    }

    public String getFtrRightsByValue(String val) {
        int seq = grValue.indexOf(val);
        return this.ftrRights.get(seq).toString();
    }

    public void setFtrRights(String rights) {
        ftrRights.add(rights);
    }

    public void setFtrRights(ArrayList rights) {
        this.ftrRights = rights;
    }

    public ArrayList getGrValue() {
        return this.grValue;
    }

    public String getGrValueBySeq(String seq) {
        int count = Integer.parseInt(seq) - 1;
        return this.grValue.get(count).toString();
    }

    public void setGrValue(String val) {
        grValue.add(val);
    }

    public void setGrValue(ArrayList val) {
        this.grValue = val;
    }

    public ArrayList getGrDesc() {
        return this.grDesc;
    }

    public String getGrDescBySeq(String seq) {
        int count = Integer.parseInt(seq) - 1;
        return this.grDesc.get(count).toString();
    }

    public String getGrDescByValue(String val) {
        int seq = grValue.indexOf(val);
        return this.grDesc.get(seq).toString();
    }

    public void setGrDesc(String desc) {
        grDesc.add(desc);
    }

    public void setGrDesc(ArrayList desc) {
        this.grDesc = desc;
    }

    public String getGrantor() {
        return this.grantor;
    }

    public void setGrantor(String grantor) {
        this.grantor = grantor;
    }

    public String getStudyId() {
        return this.studyId;
    }

    public void setStudyId(String studyId) {
        this.studyId = studyId;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getXStatus() {
        return this.xStatus;
    }

    public void setXStatus(String xStatus) {
        this.xStatus = xStatus;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS
    public ExtURightsJB(int id) {
        setId(id);
    }

    public ExtURightsJB() {
        grSeq = new ArrayList();
        ftrRights = new ArrayList();
        grValue = new ArrayList();
        grDesc = new ArrayList();
    }

    /**
     * Full arguments constructor.
     * 
     * @param
     */
    public void ExtURightsJB(int id, ArrayList grSeq, ArrayList ftrRights,
            ArrayList grValue, ArrayList grDesc, String studyId, String userId,
            String grantor, String xStatus, String creator, String modifiedBy,
            String ipAdd) {
        Rlog.debug("studyrights", "StudyRightsJB Start constructor");
        setId(id);
        setGrSeq(grSeq);
        setFtrRights(ftrRights);
        setGrValue(grValue);
        setGrDesc(grDesc);
        setStudyId(studyId);
        setUserId(userId);
        setGrantor(grantor);
        setXStatus(xStatus);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        Rlog.debug("studyrights", "StudyRightsJB End constructor");

    }

    /**
     * calls getExtURightsDetails() on the ExtURightsAgent facade and extracts
     * this state holder into the been attribute fields.
     */
    public ExtURightsBean getExtURightsDetails() {
        ExtURightsBean srk = null;
        try {
            ExtURightsAgentRObj extURightsAgentRObj = EJBUtil
                    .getExtURightsAgentHome();
            srk = extURightsAgentRObj.getExtURightsDetails(this.id);
        } catch (Exception e) {
            System.out.print("Test Error");
        }
        if (srk != null) {
            Rlog.debug("exturights",
                    "Team JB GETTEAM DETAILS SETTING THE VAULUES");
            this.id = srk.getId();
            this.grSeq = srk.getGrSeq();
            this.ftrRights = srk.getFtrRights();
            this.grValue = srk.getGrValue();
            this.grDesc = srk.getGrDesc();
            this.studyId = srk.getStudyId();
            this.userId = srk.getUserId();
            this.grantor = srk.getGrantor();
            this.xStatus = srk.getXStatus();
            this.creator = srk.getCreator();
            this.modifiedBy = srk.getModifiedBy();
            this.ipAdd = srk.getIpAdd();
        }
        return srk;
    }

    /**
     * calls setGrpRightsDetails() on the GrpRightsAgent facade.
     */
    public int setExtURightsDetails() {
        try {
            int result = 0;
            Rlog.debug("exturights", "In try block of Set GRp Rights of JB ");
            ExtURightsAgentRObj extURightsAgentRObj = EJBUtil
                    .getExtURightsAgentHome();
            result = extURightsAgentRObj.setExtURightsDetails(this
                    .createExtURightsStateKeeper());
            return result;
        } catch (Exception e) {
            Rlog.debug("exturights",
                    "Exception in setting ext user rights- client jb" + e);
            return -2; // In case of Exception
        }
    }

    public int updateExtURights() {
        try {
            int result = 0;
            Rlog.debug("exturights",
                    "In try block of Update extuser Rights of JB ");
            ExtURightsAgentRObj extURightsAgentRObj = EJBUtil
                    .getExtURightsAgentHome();
            result = extURightsAgentRObj.updateExtURights(this
                    .createExtURightsStateKeeper());
            return result;
        } catch (Exception e) {
            Rlog.debug("exturights",
                    "Exception in updating ext user rights- client jb" + e);
            return -2; // In case of Exception
        }
    }

    /*
     * places bean attributes into a Group Rights StateHolder.
     */
    public ExtURightsBean createExtURightsStateKeeper() {

        return new ExtURightsBean(id, studyId, userId, grantor, xStatus,
                creator, modifiedBy, ipAdd, grSeq, ftrRights, grValue, grDesc);
    }

    /*
     * generates a String of XML for the Group Rights details entry form.<br><br>
     * Note that it is planned to encapsulate this detail in another object.
     */
    /*
     * public String toXML() { Debug.println("GENEERATION OF XML IN TEAMJB");
     * return new String( "<?xml version=\"1.0\"?>" + "<?cocoon-process
     * type=\"xslt\"?>" + "<?xml-stylesheet href=\"/stylesheet/form-html.xsl\"
     * type=\"text/xsl\"?>" + "<?xml-stylesheet
     * href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>" + "<form
     * action=\"UpdateTeam.jsp\">" + "<head>" + "<title>Team Details </title>" + "</head>" + "<input
     * type=\"hidden\" name=\"id\" value=\"" + this.getId() + "\" size=\"10\"/>" + "<input
     * type=\"hidden \" name=\"Study\" value=\"" + this.getStudyId() + "\"
     * size=\"10\"/>" + "<input type=\"text\" name=\" User\" value=\"" +
     * this.getTeamUser() + "\" size=\"12\"/>" + "<input type=\"text\" name=\"
     * Role\" value=\"" + this.getTeamUserRole ()+ "\" size=\"30\"/>" + "</form>" );
     * }//end of method
     */

}// end of class

