/*
 * Classname : StudyVerJB.java
 * 
 * Version information
 *
 * Date 11/25/2002
 * 
 * Copyright notice: Aithent 
 */

package com.velos.eres.web.studyVer;

import java.util.Hashtable;
import com.velos.eres.business.common.StudyVerDao;
import com.velos.eres.business.studyVer.impl.StudyVerBean;
import com.velos.eres.service.studyVerAgent.StudyVerAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/**
 * Client side bean for StudyVer
 * 
 * @author Arvind
 */

public class StudyVerJB {
    /**
     * the studyVer Id
     */
    private int studyVerId;

    /**
     * the Study Id
     */
    private String studyVerStudyId;

    /**
     * the studyVer Number
     */
    private String studyVerNumber;

    /**
     * the studyVer Status
     */
    private String studyVerStatus;

    /**
     * the studyVer Notes
     */
    private String studyVerNotes;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;
    
    /*
     * Vesion Date
     */
    private String versionDate; //JM
    //private java.util.Date versionDate;
    
    /*
     * Vesion Category
     */
    private String studyVercat;//JM
    
    /*
     * Vesion Type
     */
    private String studyVertype;//JM

    // GETTER SETTER METHODS

    public int getStudyVerId() {
        return this.studyVerId;
    }

    public void setStudyVerId(int studyVerId) {
        this.studyVerId = studyVerId;
    }

    public String getStudyVerStudyId() {
        return this.studyVerStudyId;
    }

    public void setStudyVerStudyId(String studyVerStudyId) {
        this.studyVerStudyId = studyVerStudyId;
    }

    public String getStudyVerNumber() {
        return this.studyVerNumber;
    }

    public void setStudyVerNumber(String studyVerNumber) {
        this.studyVerNumber = studyVerNumber;
    }

    public String getStudyVerStatus() {
        return this.studyVerStatus;
    }

    public void setStudyVerStatus(String studyVerStatus) {
        this.studyVerStatus = studyVerStatus;
    }

    public String getStudyVerNotes() {
        return this.studyVerNotes;
    }

    public void setStudyVerNotes(String studyVerNotes) {
        this.studyVerNotes = studyVerNotes;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    
    
    
    /**
     * @return Version Date
     */
    
    public String getVersionDate(){
    	return this.versionDate;
    }
    
    /**
     * @param versionDate
     *            
     */
    public void setVersionDate(String versionDate){
    	this.versionDate = versionDate;
    	//this.versionDate = DateUtil.stringToDate(versionDate,"mm/dd/yyyy");
    }
    

    /**
     * @return Version Category
     */
    public String getStudyVercat(){
    	return this.studyVercat;
    }
    /**
     * @param studyVercat
     *            
     */
    public void setStudyVercat(String studyVercat){
    	this.studyVercat = studyVercat;
    }
    
    /**
     * 
     * @return Stduy Version Type
     */
    public String getStudyVertype(){
    	return this.studyVertype;
    }
    /**
     * @param studyVertype
     *            
     */
    public void setStudyVertype(String studyVertype){
    	this.studyVertype = studyVertype;
    }
    
    
    
    // END OF GETTER SETTER METHODS

    public StudyVerJB(int studyVerId) {
        setStudyVerId(studyVerId);
    }

    public StudyVerJB() {
        Rlog.debug("studyVer", "Blank constructor of StudyVerJB");
    }

    /**
     * Full arguments constructor.
     */

    public StudyVerJB(int studyVerId, String studyVerStudyId,
            String studyVerNumber, String studyVerStatus, String studyVerNotes,
            String creator, String modifiedBy, String ipAdd, String versionDate,String studyVercat, String studyVertype) {
        setStudyVerId(studyVerId);
        setStudyVerStudyId(studyVerStudyId);
        setStudyVerNumber(studyVerNumber);
        setStudyVerStatus(studyVerStatus);
        setStudyVerNotes(studyVerNotes);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setVersionDate(versionDate);//JM
        setStudyVercat(studyVercat);//JM
        setStudyVertype(studyVertype);//JM
        
        Rlog.debug("studyVer", "Full arguments constructor of StudyVerJB");
    }

    public StudyVerBean getStudyVerDetails() {
        StudyVerBean srsk = null;
        try {
            StudyVerAgentRObj studyVerAgentRObj = EJBUtil
                    .getStudyVerAgentHome();

            srsk = studyVerAgentRObj.getStudyVerDetails(this.studyVerId);
            Rlog.debug("studyVer",
                    "StudyVerJB.getStudyVerDetails() StudyVerStateKeeper "
                            + srsk);
        } catch (Exception e) {
            Rlog.fatal("studyVer",
                    "Error in getStudyVerDetails() in StudyVerJB " + e);
        }
        if (srsk != null) {

            this.studyVerId = srsk.getStudyVerId();
            this.studyVerStudyId = srsk.getStudyVerStudyId();
            this.studyVerNumber = srsk.getStudyVerNumber();
            this.studyVerStatus = srsk.getStudyVerStatus();
            this.studyVerNotes = srsk.getStudyVerNotes();
            this.creator = srsk.getCreator();
            this.modifiedBy = srsk.getModifiedBy();
            this.ipAdd = srsk.getIpAdd();
            this.versionDate = srsk.returnVersionDate();
            this.studyVercat = srsk.getStudyVercat();
            this.studyVertype = srsk.getStudyVertype();
        }
               
        return srsk;
    }

    public int setStudyVerDetails() {
        try {
            StudyVerAgentRObj studyVerAgentRObj = EJBUtil
                    .getStudyVerAgentHome();

            this.setStudyVerId(studyVerAgentRObj.setStudyVerDetails(this
                    .createStudyVerStateKeeper()));

            Rlog.debug("studyVer", "StudyVerJB.setStudyVerDetails()");

        } catch (Exception e) {
            Rlog.fatal("studyVer",
                    "Error in setStudyVerDetails() in StudyVerJB " + e);
            return -2;
        }
        return this.getStudyVerId();
    }

    public int updateStudyVer() {
        int output;
        try {
            StudyVerAgentRObj studyVerAgentRObj = EJBUtil
                    .getStudyVerAgentHome();
           
            output = studyVerAgentRObj.updateStudyVer(this
                    .createStudyVerStateKeeper());
            Rlog
                    .debug("studyVer", "StudyVerJB.updateStudyVer output "
                            + output);

        } catch (Exception e) {
            Rlog.fatal("studyVer",
                    "EXCEPTION IN SETTING STUDYVER DETAILS TO  SESSION BEAN "
                            + e);

            return -2;
        }
        return output;
    }

    public StudyVerBean createStudyVerStateKeeper() {
        return new StudyVerBean(studyVerId, studyVerStudyId, studyVerNumber,
                studyVerStatus, studyVerNotes, creator, modifiedBy, ipAdd, versionDate, studyVercat,studyVertype);

    }

    public StudyVerDao getAllVers(int studyId) {
        StudyVerDao studyVerDao = new StudyVerDao();
        try {
            Rlog.debug("studyVer", "StudyVerJB.getAllVers starting");
            StudyVerAgentRObj studyVerAgentRObj = EJBUtil
                    .getStudyVerAgentHome();

            studyVerDao = studyVerAgentRObj.getAllVers(studyId);
            return studyVerDao;
        } catch (Exception e) {
            Rlog.fatal("studyVer",
                    "Error in getAllVers(int studyId) in StudyVerJB " + e);
            return null;
        }
    }

    public StudyVerDao getAllVers(int studyId, String status) {
        StudyVerDao studyVerDao = new StudyVerDao();
        try {
            Rlog.debug("studyVer", "StudyVerJB.getAllVers starting");
            StudyVerAgentRObj studyVerAgentRObj = EJBUtil
                    .getStudyVerAgentHome();

            studyVerDao = studyVerAgentRObj.getAllVers(studyId, status);
            return studyVerDao;
        } catch (Exception e) {
            Rlog.fatal("studyVer",
                    "Error in getAllVers(int studyId) in StudyVerJB " + e);
            return null;
        }
    }
/*
    //JM: 11/17/2005: Added: new method
    
    public StudyVerDao getAllStudyVers(int studyId,String studyVerNumber,String verCat, String verType, String verStat, String orderBy, String orderType) {
        StudyVerDao studyVerDao = new StudyVerDao();
        try {
            Rlog.debug("studyVer", "StudyVerJB.getAllVers starting");
            StudyVerAgentRObj studyVerAgentRObj = EJBUtil
                    .getStudyVerAgentHome();

            studyVerDao = studyVerAgentRObj.getAllStudyVers(studyId,studyVerNumber,verCat, verType, verStat, orderBy, orderType);
            return studyVerDao;
        } catch (Exception e) {
            Rlog.fatal("studyVer",
                    "Error in getAllVers(int studyId) in StudyVerJB " + e);
            return null;
        }
    }
  */
    public int removeStudyVer(int studyVerId) {

        int output = 0;

        try {
            StudyVerAgentRObj studyVerAgentRObj = EJBUtil
                    .getStudyVerAgentHome();

            output = studyVerAgentRObj.removeStudyVer(studyVerId);

        } catch (Exception e) {
            Rlog.fatal("studyVer", "Exception in removing studyVer");
            return -1;
        }
        return output;
    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int removeStudyVer(int studyVerId,Hashtable<String, String> auditInfo) {

        int output = 0;
        try {
            StudyVerAgentRObj studyVerAgentRObj = EJBUtil
                    .getStudyVerAgentHome();
            output = studyVerAgentRObj.removeStudyVer(studyVerId,auditInfo);

        } catch (Exception e) {
            Rlog.fatal("studyVer", "Exception in removing studyVer");
            return -1;
        }
        return output;
    }

    public int copyStudyVersion(int studyVerId, String newVerNum, String verDate,String verCat, String verType , int usr) {
        int output = 0;
        try {
            Rlog.debug("studyVer", "StudyVerJB.copyStudyVersion starting");
            StudyVerAgentRObj studyVerAgentRObj = EJBUtil
                    .getStudyVerAgentHome();

            output = studyVerAgentRObj.copyStudyVersion(studyVerId, newVerNum, verDate, verCat, verType,usr);
            return output;
        } catch (Exception e) {
            Rlog.fatal("studyVer", "Error in copyStudyVersion in StudyVerJB "
                    + e);
            return -1;
        }
    }
//JM: 11/17/2005: Modified: parameter String versionDate, String studyVercat, String studyVertype added
    //public int newStudyVersion(int studyId, String verNum, String verNotes,int usr) {
    public int newStudyVersion(int studyId, String verNum, String verNotes, String versionDate, String studyVercat, String studyVertype,int usr) {    	
        int output = 0;
        try {
            StudyVerAgentRObj studyVerAgentRObj = EJBUtil
                    .getStudyVerAgentHome();
            // Bug 4114 - Trim spaces in Version Number
            if (verNum != null) { verNum = verNum.trim(); }

            output = studyVerAgentRObj.newStudyVersion(studyId, verNum,
                    verNotes,versionDate ,studyVercat,  studyVertype, usr);
            return output;
        } catch (Exception e) {
            Rlog.fatal("studyVer", "Error in newStudyVersion in StudyVerJB "
                    + e);
            return -1;
        }
    }

}// end of class

