package com.velos.eres.web.dynrep.holder;

import java.util.ArrayList;

/** Data Holder class to contain the 'ID' (example-patient/stuyd/account) mapping with the data count
 * Ideally the object will contain data for one form
 * ArrayLists are used for better performance over Vectors/Hashtable
 *  */
public class IdDataCount {
	ArrayList arId;
	ArrayList arCount;
	
	
	public ArrayList getArCount() {
		return arCount;
	}
	
	public int getArCount(String id) {
		return getLastCount(id);
	}
	
	public void setArCount(ArrayList arCount) {
		this.arCount = arCount;
	}
	
	public void setArCount(int count) {
		this.arCount.add(new Integer(count));
	}
	
	
	public void setArCount(String id, int count) {
		
		int index = 0;
		if (count <= 1) //first record for the ID
		{
			//append to ArrayList
			setArCount(count);
			setArId(id);
		}
		else // the id already exists
		{
			index = arId.indexOf(id);
			
			if (index > -1) //data for ID already exists
			{
				 arCount.set(index, new Integer(count));
			}
					
			
		}
	}
	
	
	public ArrayList getArId() {
		return arId;
	}
		
	public void setArId(ArrayList arId) {
		this.arId = arId;
	}
	
	public void setArId(String id) {
		this.arId.add(id);
	}

	
	public int getLastCount(String id)
	{
		int index = 0;
		int count = 0;
		
		index = arId.indexOf(id);
		
		if (index > -1) //data for ID already exists
		{
			count = ((Integer)arCount.get(index)).intValue();
		}
		else
		{
			count = 0; //does not exist
			
		}
		return count;
	}
	
	
	
	
	public IdDataCount() {
		super();
		
		arCount = new ArrayList();
		arId  = new ArrayList();
		// TODO Auto-generated constructor stub
	}
	
	

}
