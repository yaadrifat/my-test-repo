package com.velos.eres.web.reviewBoard;

import com.velos.eres.business.reviewboard.impl.ReviewBoardBean;
import com.velos.eres.service.reviewBoardAgent.ReviewBoardAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/**
 * The Review Board CMP entity bean for Review Board.
 * 
 * @author Amit
 * @version 1.0 01/21/2013
 */

public class ReviewBoardJB
{
	private int reviewBoardId;	
	private String reviewBoardName;
	private String reviewBoardDescription;
	private int reviewboardAccId;
	private String boardGroupAccess;
	private String boardMomLogic;
	private int reviewBoardDefault;
    private String ipAdd;
    private String creator;
    private String modifiedBy;

	// GETTER SETTER METHODS

	public int getReviewBoardId() {
		return reviewBoardId;
	}	
	public void setReviewBoardId(int reviewBoardId) {
		this.reviewBoardId = reviewBoardId;
	}

	
	
	public String getReviewBoardName() {
		return reviewBoardName;
	}	
	public void setReviewBoardName(String reviewBoardName) {
		this.reviewBoardName = reviewBoardName;
	}
	
	
	
	public String getReviewBoardDescription() {
		return reviewBoardDescription;
	}
	public void setReviewBoardDescription(String reviewBoardDescription) {
		this.reviewBoardDescription = reviewBoardDescription;
	}

	
	public int getReviewboardAccId() {
		return reviewboardAccId;
	}
	public void setReviewboardAccId(int reviewboardAccId) {
		this.reviewboardAccId = reviewboardAccId;
	}

	
	
	public String getBoardGroupAccess() {
		return boardGroupAccess;
	}	
	public void setBoardGroupAccess(String boardGroupAccess) {
		this.boardGroupAccess = boardGroupAccess;
	}

	
	
	public String getBoardMomLogic() {
		return boardMomLogic;
	}	
	public void setBoardMomLogic(String boardMomLogic) {
		this.boardMomLogic = boardMomLogic;
	}

	
	public int getReviewBoardDefault() {
		return reviewBoardDefault;
	}	
	public void setReviewBoardDefault(int reviewBoardDefault) {
		this.reviewBoardDefault = reviewBoardDefault;
	}
	
    public String getIpAdd() {
		return ipAdd;
	}
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
	
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	// END OF GETTER SETTER METHODS

	
	public ReviewBoardJB() {
		Rlog.debug("reviewBoard", "ReviewBoardJB blank constructor");
	}
	
	public ReviewBoardJB(int reviewBoardId) {
		setReviewBoardId(reviewBoardId);
		Rlog.debug("reviewBoard","ReviewBoardJB constructor with reviewBoardId");
	}
	public ReviewBoardJB(int reviewBoardId, String reviewBoardName, String reviewBoardDescription,int reviewboardAccId,String boardGroupAccess, String boardMomLogic,int reviewBoardDefault,String ipAdd,String creator,String modifiedBy)
	{
		setReviewBoardId(reviewBoardId);		
		setReviewBoardName(reviewBoardName);
		setReviewBoardDescription(reviewBoardDescription);
		setReviewboardAccId(reviewboardAccId);
		setBoardGroupAccess(boardGroupAccess);
		setBoardMomLogic(boardMomLogic);
		setReviewBoardDefault(reviewBoardDefault);
		setIpAdd(ipAdd);
		setCreator(creator);
		setModifiedBy(modifiedBy);		
		Rlog.debug("reviewBoard", "ReviewBoardJB full arguments constructor");
	}

	
	public ReviewBoardBean getReviewBoardAllDetails() 
	{
		ReviewBoardBean rsk = null;
		try
		{
			ReviewBoardAgentRObj reviewBoardAgentRObj = EJBUtil.getReviewBoardAgentHome();		
			rsk = reviewBoardAgentRObj.getReviewBoardAllDetails();
			Rlog.debug("reviewBoard","ReviewBoardJB.getReviewBoardAllDetails() ReviewBoardBean "+ rsk);
		}
		catch (Exception e)
		{
			Rlog.fatal("reviewBoard","Error in getReviewBoardAllDetails in ReviewBoardJB " + e);
		}
		 
		if (rsk != null)
		{
        	this.reviewBoardId = rsk.getReviewBoardId();	        	
        	this.reviewBoardName=rsk.getReviewBoardName();
        	this.reviewBoardDescription=rsk.getReviewBoardDescription();	  
        	this.reviewboardAccId=rsk.getReviewboardAccId();
        	this.boardGroupAccess=rsk.getBoardGroupAccess();
        	this.boardMomLogic=rsk.getBoardMomLogic();
        	this.reviewBoardDefault=rsk.getReviewBoardDefault();    	
    	    this.ipAdd=rsk.getIpAdd();
    	    this.creator=rsk.getCreator();
    	    this.modifiedBy=rsk.getModifiedBy();
        	
    	    setReviewBoardId(rsk.getReviewBoardId());	    	   
    	    setReviewBoardName(rsk.getReviewBoardName());
    	    setReviewBoardDescription(rsk.getReviewBoardDescription());	  
    	    setReviewboardAccId(rsk.getReviewboardAccId());
    	    setBoardGroupAccess(rsk.getBoardGroupAccess());
    	    setBoardMomLogic(rsk.getBoardMomLogic());
    	    setReviewBoardDefault(rsk.getReviewBoardDefault());
    	    setIpAdd(rsk.getIpAdd());
    	    setCreator(rsk.getCreator());
    	    setModifiedBy(rsk.getModifiedBy());
	    }
		return rsk;
	}
	
	
	public ReviewBoardBean getReviewBoardDetails(int reviewBoardId) 
	{
		ReviewBoardBean rsk = null;
		try {
			ReviewBoardAgentRObj reviewBoardAgentRObj = EJBUtil.getReviewBoardAgentHome();
			rsk = reviewBoardAgentRObj.getReviewBoardDetails(this.reviewBoardId);
			Rlog.debug("reviewBoard","ReviewBoardJB.getReviewBoardDetails() ReviewBoardBean "+ rsk);
		} 
		catch (Exception e)
		{
			Rlog.fatal("reviewBoard","Error in getReviewBoardDetails in ReviewBoardJB " + e);
		}
		if (rsk != null) 
		{
			this.reviewBoardId = rsk.getReviewBoardId();        	
        	this.reviewBoardName=rsk.getReviewBoardName();
        	this.reviewBoardDescription=rsk.getReviewBoardDescription();	 
        	this.reviewboardAccId=rsk.getReviewboardAccId();
        	this.boardGroupAccess=rsk.getBoardGroupAccess();
        	this.boardMomLogic=rsk.getBoardMomLogic();
        	this.reviewBoardDefault=rsk.getReviewBoardDefault();    
        	this.ipAdd=rsk.getIpAdd();
        	this.creator=rsk.getCreator();
        	this.modifiedBy=rsk.getModifiedBy();

    	    setReviewBoardId(rsk.getReviewBoardId());    	    
    	    setReviewBoardName(rsk.getReviewBoardName());
    	    setReviewBoardDescription(rsk.getReviewBoardDescription());
    	    setReviewboardAccId(rsk.getReviewboardAccId());    	    
    	    setBoardGroupAccess(rsk.getBoardGroupAccess());
    	    setBoardMomLogic(rsk.getBoardMomLogic());
    	    setReviewBoardDefault(rsk.getReviewBoardDefault());
    	    setIpAdd(rsk.getIpAdd());
    	    setCreator(rsk.getCreator());
    	    setModifiedBy(rsk.getModifiedBy());
        }
		return rsk;
	}	
	
	public ReviewBoardBean getReviewBoardNames(int reviewBoardId) 
	{
		ReviewBoardBean rsk = null;
		try {
			ReviewBoardAgentRObj reviewBoardAgentRObj = EJBUtil.getReviewBoardAgentHome();
			rsk = reviewBoardAgentRObj.getReviewBoardNames(this.reviewBoardId);
			Rlog.debug("reviewBoard","ReviewBoardJB.getReviewBoardDetails() ReviewBoardBean "+ rsk);
		}
		catch (Exception e) 
		{
			Rlog.fatal("reviewBoard","Error in getReviewBoardDetails in ReviewBoardJB " + e);
		}
		
		if (rsk != null) 
		{
        	this.reviewBoardId = rsk.getReviewBoardId();        	
        	this.reviewBoardName=rsk.getReviewBoardName();      		
    	    
    	    setReviewBoardId(rsk.getReviewBoardId());    	    
    	    setReviewBoardName(rsk.getReviewBoardName());
     
        }
		return rsk;
	}		

	public int setReviewBoardDetails() 
	{
		int output = 0;
        try
        {
        	ReviewBoardAgentRObj reviewBoardAgentRObj = EJBUtil.getReviewBoardAgentHome();
            output = reviewBoardAgentRObj.setReviewBoardDetails(this.createReviewBoardStateKeeper());
            this.setReviewBoardId(output); 
        }
        catch (Exception e)
        {
            Rlog.fatal("reviewBoard", "Error in setReviewBoardDetails() in ReviewBoardJB "   + e);
            return -2;
        }
        return output;
    }
	
	
	public int updateReviewBoard() {
		int output;
		try
		{
			ReviewBoardAgentRObj reviewBoardAgentRObj = EJBUtil.getReviewBoardAgentHome();
			output = reviewBoardAgentRObj.updateReviewBoard(this.createReviewBoardStateKeeper());
		}
		catch (Exception e) 
		{
			Rlog.fatal("group","EXCEPTION IN SETTING GROUP DETAILS TO  SESSION BEAN " + e);
			return -2;
		}
		return output;
	}
	
	
	public ReviewBoardBean createReviewBoardStateKeeper() 
	{
		return new ReviewBoardBean(this.reviewBoardId, this.reviewBoardName, this.reviewBoardDescription,this.reviewboardAccId,this.boardGroupAccess, this.boardMomLogic,this.reviewBoardDefault,this.ipAdd,this.creator,this.modifiedBy);
	}
	
	public int getRBNameCount(String rbName) 
	{
        int count = 0;
        try 
        {
            Rlog.debug("reviewBoard", "ReviewBoardJB.getRBNameCount starting");
            ReviewBoardAgentRObj reviewBoardAgentRObj = EJBUtil.getReviewBoardAgentHome();
            Rlog.debug("reviewBoard", "ReviewBoardJB.getRBNameCount after remote");
            count = reviewBoardAgentRObj.getRBNameCount(rbName);
            Rlog.debug("reviewBoard", "ReviewBoardJB.getRBNameCount after Dao");
        }
        catch (Exception e)
        {
            Rlog.fatal("reviewBoard","Error in getRBNameCount(String rbName) in ReviewBoardJB " + e);
        }
        return count;
    }
}