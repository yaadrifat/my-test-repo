/**
 * Classname : StdSiteRightsJB
 * 
 * Version information: 1.0
 *
 * Date: 11/17/2004
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.web.stdSiteRights;

/* Import Statements */
import com.velos.eres.business.common.StdSiteRightsDao;
import com.velos.eres.business.stdSiteRights.impl.StdSiteRightsBean;
import com.velos.eres.service.stdSiteRightsAgent.StdSiteRightsAgentRObj;
import com.velos.eres.service.stdSiteRightsAgent.impl.StdSiteRightsAgentBean;
import com.velos.eres.service.userSiteAgent.impl.UserSiteAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

/**
 * Client side bean for stdSiteRights
 * 
 * @author Anu Khanna
 * @version 1.0 11/17/2004
 */

public class StdSiteRightsJB {

    /**
     * the std site rights Id
     */
    private int stdSiteRightsId;

    /**
     * the site Id
     */
    private String siteId;

    /**
     * the study Id
     */
    private String studyId;

    /**
     * the user Id
     */

    private String userId;

    /**
     * the study site user rights
     */

    private String studySiteRights;

    /**
     * creator
     */
    private String creator;

    /**
     * last modified by
     */
    private String modifiedBy;

    /**
     * IP Address
     */
    private String ipAdd;

    // GETTER SETTER METHODS

    /**
     * Returns study Site Rights id
     * 
     * @return stdSiteRightsId
     */
    public int getStdSiteRightsId() {
        return this.stdSiteRightsId;
    }

    /**
     * Sets Study Site Rights id
     * 
     * @param stdSiteRightsId
     *            Study Site Rights id
     */
    public void setStdSiteRightsId(int stdSiteRightsId) {
        this.stdSiteRightsId = stdSiteRightsId;
    }

    /**
     * Returns Site Id
     * 
     * @return siteId
     */
    public String getSiteId() {
        return this.siteId;
    }

    /**
     * Sets site Id
     * 
     * @param siteId
     */
    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    /**
     * Returns Study Id
     * 
     * @return studyId
     */
    public String getStudyId() {
        return this.studyId;
    }

    /**
     * Sets study Id
     * 
     * @param studyId
     */
    public void setStudyId(String studyId) {
        this.studyId = studyId;
    }

    /**
     * Returns User Id
     * 
     * @return User Id
     */
    public String getUserId() {
        return this.userId;
    }

    /**
     * Sets User Id
     * 
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * Returns study Site Rights
     * 
     * @return study Site Rights
     */
    public String getStudySiteRights() {
        return this.studySiteRights;
    }

    /**
     * Sets Study Site Rights
     * 
     * @param studySiteRights
     */
    public void setStudySiteRights(String studySiteRights) {
        this.studySiteRights = studySiteRights;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // end of getter and setter methods

    /**
     * Full arguments constructor.
     */

    // COMMENTED FOR TIME BEING
    /**
     * Constructor
     * 
     * @param stdSiteRightsId
     */
    public StdSiteRightsJB(int stdSiteRightsId) {
        setStdSiteRightsId(stdSiteRightsId);
    }

    /**
     * Default Constructor
     * 
     */
    public StdSiteRightsJB() {
        Rlog.debug("stdSiteRights", "StdSiteRightsJB.StdSiteRightsJB() ");
    }

    /**
     * Full Argument Constructor
     * 
     * @param stdSiteRightsId
     * @param siteId
     * @param studyId
     * @param userId
     * @param studySiteRights
     * @param creator
     * @param modifiedBy
     * @param ipAdd
     */
    public StdSiteRightsJB(int stdSiteRightsId, String siteId, String studyId,
            String userId, String studySiteRights, String creator,
            String modifiedBy, String ipAdd) {
        setStdSiteRightsId(stdSiteRightsId);
        setSiteId(siteId);
        setStudyId(studyId);
        setUserId(userId);
        setStudySiteRights(studySiteRights);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        Rlog.debug("stdSiteRights",
                "StdSiteRightsJB.StdSiteRightsJB(all parameters)");
    }

    /**
     * Calls getStdSiteRightsDetails of Std Site Session Bean:
     * StdSiteRightsAgentBean
     * 
     * @returns StdSiteRightsStateKeeper object
     * @see StdSiteRightsAgentBean
     */
    public StdSiteRightsBean getStdSiteRightsDetails() {
        StdSiteRightsBean ssk = null;
        try {

            StdSiteRightsAgentRObj stdSiteRightsAgent = EJBUtil
                    .getStdSiteRightsAgentHome();
            ssk = stdSiteRightsAgent
                    .getStdSiteRightsDetails(this.stdSiteRightsId);
            Rlog.debug("stdSiteRights",
                    "StdSiteRightsJB.getStdSiteRightsDetails() StdSiteRightsStateKeeper "
                            + ssk);
        } catch (Exception e) {
            Rlog.fatal("stdSiteRights",
                    "Error in getStdSiteRightsDetails() in StdSiteRightsJB "
                            + e);
        }
        if (ssk != null) {
            this.stdSiteRightsId = ssk.getStdSiteRightsId();
            this.siteId = ssk.getSiteId();
            this.studyId = ssk.getStudyId();
            this.userId = ssk.getUserId();
            this.studySiteRights = ssk.getStudySiteRights();
            this.creator = ssk.getCreator();
            this.modifiedBy = ssk.getModifiedBy();
            this.ipAdd = ssk.getIpAdd();
        }
        return ssk;
    }

    /**
     * Calls setStdSiteDetails of User Site Session Bean: UserSiteAgentBean
     * 
     * @see UserSiteAgentBean
     */
    public void setStdSiteRightsDetails() {
        try {

            StdSiteRightsAgentRObj stdSiteRightsAgent = EJBUtil
                    .getStdSiteRightsAgentHome();
            this.setStdSiteRightsId(stdSiteRightsAgent
                    .setStdSiteRightsDetails(this
                            .createStdSiteRightsStateKeeper()));
            Rlog.debug("stdSiteRights", "UserSiteJB.setUserSiteDetails()");
        } catch (Exception e) {
            Rlog.fatal("stdSiteRights",
                    "Error in setUserSiteDetails() in UserSiteJB " + e);
        }
    }

    /**
     * Calls updateStdSiteRights of Study Site rights Session Bean:
     * StdSiteRightsAgentBean
     * 
     * @return int 0 if successful, -2 for exception
     * @see StdSiteRightsAgentBean
     */
    public int updateStdSiteRights() {
        int output;
        try {

            StdSiteRightsAgentRObj stdSiteRightsAgentRObj = EJBUtil
                    .getStdSiteRightsAgentHome();
            output = stdSiteRightsAgentRObj.updateStdSiteRights(this
                    .createStdSiteRightsStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("stdSiteRights",
                    "Error in updateStdSiteRights() in StdSiteRightsJB " + e);
            return -2;
        }
        return output;
    }

    /**
     * Calls updateUserSite of User Site Session Bean: UserSiteAgentBean
     * Overloaded method
     * 
     * @param Array
     *            of pkUserSites
     * @param Array
     *            of corresponding rights for each user site
     * @param userId
     * @param IP
     *            Address
     * @returns 0 if successful, -1 exception
     * @see UserSiteAgentBean
     */
    public int updateStudySite(String[] pkStudySites, String[] siteIds,
            String[] rights, int studyId, int usr, String ipAdd,String creator) {
        int ret = 0;
        try {

            StdSiteRightsAgentRObj stdSiteRightsAgentRObj = EJBUtil
                    .getStdSiteRightsAgentHome();
            ret = stdSiteRightsAgentRObj.updateStudySite(pkStudySites, siteIds,
                    rights, studyId, usr, ipAdd,creator);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("stdSiteRights",
                    "Error in updateStudySite() calling SP in JB " + e);
            return -1;
        }

    }

    /**
     * Calls getUserSiteTree of User Site Session Bean: UserSiteAgentBean
     * 
     * @param Account
     *            Id
     * @param userId
     * @returns UserSiteDao
     * @see UserSiteAgentBean
     */
    public StdSiteRightsDao getStudySiteTree(int accountId, int loginUser,
            int studyId) {
        StdSiteRightsDao stdSiteDao = new StdSiteRightsDao();
        try {

            StdSiteRightsAgentRObj stdSiteRightsAgentRObj = EJBUtil
                    .getStdSiteRightsAgentHome();
            stdSiteDao = stdSiteRightsAgentRObj.getStudySiteTree(accountId,
                    loginUser, studyId);
        } catch (Exception e) {
            Rlog.fatal("stdSiteRights", "Error in getStudySiteTree() in JB "
                    + e);
        }
        return stdSiteDao;
    }

    /**
     * Get organizations with view access for an account user for a study.
     * 
     * @param Account
     *            Id
     * @param userId
     * @param studyId
     * @returns StdSiteRightsDao
     * @see StdSiteRightsAgentBean
     */
    public StdSiteRightsDao getStudySitesWithViewRight(int accId, int userId,
            int studyId) {
        StdSiteRightsDao stdSiteDao = new StdSiteRightsDao();
        try {

            StdSiteRightsAgentRObj stdSiteRightsAgentRObj = EJBUtil
                    .getStdSiteRightsAgentHome();
            stdSiteDao = stdSiteRightsAgentRObj.getStudySitesWithViewRight(
                    accId, userId, studyId);
        } catch (Exception e) {
            Rlog.fatal("stdSiteRights",
                    "Error in getStudySitesWithViewRight() in JB " + e);
        }
        return stdSiteDao;
    }

    /**
     * Get organizations with new access for an account user, calls
     * getSitesWithNewRight of Session Bean
     * 
     * @param Account
     *            Id
     * @param userId
     * @returns UserSiteDao
     * @see UserSiteAgentBean
     */
    /*
     * public UserSiteDao getSitesWithNewRight(int accId, int userId) {
     * UserSiteDao userSiteDao = new UserSiteDao(); try{ UserSiteAgentHome
     * userSiteAgentHome = EJBUtil.getUserSiteAgentHome(); UserSiteAgentRObj
     * userSiteAgentRObj = userSiteAgentHome.create(); userSiteDao =
     * userSiteAgentRObj.getSitesWithNewRight(accId,userId); } catch(Exception
     * e){ Rlog.fatal("userSite","Error in getSitesWithNewRight() in UserSiteJB " +
     * e); } return userSiteDao; }
     * 
     */
    /**
     * Get organizations with view access for an account user, calls
     * getSitesWithViewRight of Session Bean
     * 
     * @param user
     * @param Account
     *            Id
     * @param study
     *            Id
     * @param creator
     * @param ip
     * @param mode
     * @returns 0 for success, -1 for error
     * @see StdSiteRightsAgentBean
     */

    public int createStudySiteRightsData(int user, int account, int studyId,
            int creator, String ipAdd) {

        int ret = 0;
        Rlog.debug("stdSiteRights", "inside JB createStudySiteRightsData");
        try {

            StdSiteRightsAgentRObj stdSiteRightsAgentRObj = EJBUtil
                    .getStdSiteRightsAgentHome();
            ret = stdSiteRightsAgentRObj.createStudySiteRightsData(user,
                    account, studyId, creator, ipAdd);
        } catch (Exception e) {
            Rlog.fatal("stdSiteRights",
                    "Error in createStudySiteRightsData() in JB " + e);
            return -1;
        }
        return ret;
    }

    /**
     * Creates a StdSiteRightsStateKeeper object with the attribute vales of the
     * bean
     * 
     * @return StdSiteRightsStateKeeper
     */
    public StdSiteRightsBean createStdSiteRightsStateKeeper() {
        Rlog.debug("stdSiteRights",
                "StdSiteRightsJB.createStdSiteRightsStateKeeper ");

        return new StdSiteRightsBean(stdSiteRightsId, siteId, studyId, userId,
                studySiteRights, creator, modifiedBy, ipAdd);
    }

    public int deleteStdSiteRights(int studyId, int userId) {

        int ret = 0;

        try {

            StdSiteRightsAgentRObj stdSiteRightsAgentRObj = EJBUtil
                    .getStdSiteRightsAgentHome();
            ret = stdSiteRightsAgentRObj.deleteStdSiteRights(studyId, userId);
        } catch (Exception e) {
            Rlog.fatal("stdSiteRights", "Error in deleteStdSiteRights in JB "
                    + e);
            return -1;
        }
        return ret;
    }

    /**
     * Calls getRightForUserSite of User Site Session Bean: UserSiteAgentBean
     * 
     * @return right
     * @see UserSiteAgentBean
     */
    /*
     * public int getRightForUserSite(int userId, int siteId) { int output = 0;
     * try{ UserSiteAgentHome userSiteAgentHome =
     * EJBUtil.getUserSiteAgentHome(); UserSiteAgentRObj userSiteAgentRObj =
     * userSiteAgentHome.create(); output =
     * userSiteAgentRObj.getRightForUserSite(userId,siteId); } catch(Exception
     * e){ Rlog.fatal("userSite","Error in getRightForUserSite() in UserSiteJB " +
     * e); } return output; }
     */

    /*
     * generates a String of XML for the user details entry form.<br><br> Not
     * in use
     */

    /**
     * Not in Use
     * 
     * @return
     */
    /*
     * public String toXML() { Rlog.debug("userSite","UserSiteJB.toXML()");
     * return new String ("<?xml version=\"1.0\"?>" + "<?cocoon-process
     * type=\"xslt\"?>" + "<?xml-stylesheet href=\"/stylesheet/form-html.xsl\"
     * type=\"text/xsl\"?>" + "<?xml-stylesheet
     * href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>" +
     * //to be done "<form action=\"usersite.jsp\">" + "<head>" + //to be done "<title>User
     * Site </title>" + "</head>" + "<input type=\"text\" name=\"userSiteId\"
     * value=\"" + this.getUserSiteId()+ "\" size=\"10\"/>" + "<input
     * type=\"text\" name=\"userSiteUserId\" value=\"" +
     * this.getUserSiteUserId() + "\" size=\"10\"/>" + "<input type=\"text\"
     * name=\"userSiteSiteId\" value=\"" + this.getUserSiteSiteId() + "\"
     * size=\"10\"/>" + "<input type=\"text\" name=\"userSiteRight\" value=\"" +
     * this.getUserSiteRight() + "\" size=\"38\"/>" + "</form>");
     * 
     * }//end of method
     */

}
