package com.velos.eres.web.memberBoard;

import com.velos.eres.business.common.MemberDao;
import com.velos.eres.business.memberBoard.impl.MemberBean;
import com.velos.eres.service.memberAgent.MemberAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

public class MemberJB {
	
	private int pkRBMember;
    
    private int fkBoardId;
   
    private int fkMemberRole;
    
    private int fkMemberUser;
    
    private String brdMemStatus;
    
    private String brdMemRights;    
    
	private String ipAdd;
    
    private String creator;
    
    private String modifiedBy;
    
    public int getPkRBMember() {
		return pkRBMember;
	}
	public void setPkRBMember(int pkRBMember) {
		this.pkRBMember = pkRBMember;
	}

	public int getFkBoardId() {
		return fkBoardId;
	}
	public void setFkBoardId(int fkBoardId) {
		this.fkBoardId = fkBoardId;
	}
	

	public int getFkMemberRole() {
		return fkMemberRole;
	}
	public void setFkMemberRole(int fkMemberRole) {
		this.fkMemberRole = fkMemberRole;
	}


	public int getFkMemberUser() {
		return fkMemberUser;
	}
	public void setFkMemberUser(int fkMemberUser) {
		this.fkMemberUser = fkMemberUser;
	}

	public String getBrdMemStatus() {
		return brdMemStatus;
	}
	public void setBrdMemStatus(String brdMemStatus) {
		this.brdMemStatus = brdMemStatus;
	}


	public String getBrdMemRights() {
		return brdMemRights;
	}
	public void setBrdMemRights(String brdMemRights) {
		this.brdMemRights = brdMemRights;
	}
	
	public String getIpAdd() {
		return ipAdd;
	}
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
	
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	//End Of Getter/Setter
	
	public MemberJB() {
    }
    
    public MemberJB(int pkRBMember)
    {
        setPkRBMember(pkRBMember);
        Rlog.debug("boardMember","MemberJB constructor with pkRBMember");
    }
    
    public MemberJB(int pkRBMember, int fkBoardId, int fkMemberRole, int fkMemberUser,
            String memberStatus, String brdMemRights,String ipAdd,String creator,String modifiedBy)
    {
    	setPkRBMember(pkRBMember);
    	setFkBoardId(fkBoardId);
    	setFkMemberRole(fkMemberRole);
    	setFkMemberUser(fkMemberUser);
    	setBrdMemStatus(memberStatus);
    	setBrdMemRights(brdMemRights);
    	setIpAdd(ipAdd);
		setCreator(creator);
		setModifiedBy(modifiedBy);		
		Rlog.debug("reviewBoard", "MemberJB full arguments constructor");
    }
    
    public MemberBean getMemberDetails() {
        MemberBean mb = null;
        try        
        {        	
            MemberAgentRObj memberAgentRObj = EJBUtil.getBoardMemberAgentHome();
            mb = memberAgentRObj.getMemberDetails(this.fkBoardId);            
            Rlog.debug("boardMember","MemberJB.getMemberDetails() MemberBean "+ mb);            
        }
        catch (Exception e)
        {
        	Rlog.fatal("boardMember","Error in getMemberDetails() in MemberJB " + e);
        }
        if (mb != null) {        	
        	this.pkRBMember=mb.getPkRBMember();
        	this.fkBoardId=mb.getFkBoardId();
        	this.fkMemberRole=mb.getFkMemberRole();        	
        	this.fkMemberUser=mb.getFkMemberUser();
    		this.brdMemStatus=mb.getBrdMemStatus();
    		this.brdMemRights=mb.getBrdMemRights();
    		this.ipAdd=mb.getIpAdd();
    	    this.creator=mb.getCreator();
    	    this.modifiedBy=mb.getModifiedBy();
    		
    		setPkRBMember(mb.getPkRBMember());
    	    setFkBoardId(mb.getFkBoardId());
    	    setFkMemberRole(mb.getFkMemberRole());
    	    setFkMemberUser(mb.getFkMemberUser());
    	    setBrdMemStatus(mb.getBrdMemStatus());
    	    setBrdMemRights(mb.getBrdMemRights());  
    	    setIpAdd(mb.getIpAdd());
    	    setCreator(mb.getCreator());
    	    setModifiedBy(mb.getModifiedBy());
        }
        return mb;
    }
    
    public MemberBean getMemberDetailsById(int pkRBMember) {
        MemberBean mb = null;
        try        
        {        	
            MemberAgentRObj memberAgentRObj = EJBUtil.getBoardMemberAgentHome();
            mb = memberAgentRObj.getMemberDetailsById(this.pkRBMember);            
            Rlog.debug("boardMember","MemberJB.getMemberDetails() MemberBean "+ mb);            
        }
        catch (Exception e)
        {
        	Rlog.fatal("boardMember","Error in getMemberDetails() in MemberJB " + e);
        }
        if (mb != null) {        	
        	this.pkRBMember=mb.getPkRBMember();
        	this.fkBoardId=mb.getFkBoardId();
        	this.fkMemberRole=mb.getFkMemberRole();        	
        	this.fkMemberUser=mb.getFkMemberUser();
    		this.brdMemStatus=mb.getBrdMemStatus();
    		this.brdMemRights=mb.getBrdMemRights();
    		this.ipAdd=mb.getIpAdd();
        	this.creator=mb.getCreator();
        	this.modifiedBy=mb.getModifiedBy();
    		
    		setPkRBMember(mb.getPkRBMember());
    	    setFkBoardId(mb.getFkBoardId());
    	    setFkMemberRole(mb.getFkMemberRole());
    	    setFkMemberUser(mb.getFkMemberUser());
    	    setBrdMemStatus(mb.getBrdMemStatus());
    	    setBrdMemRights(mb.getBrdMemRights());    
    	    setIpAdd(mb.getIpAdd());
    	    setCreator(mb.getCreator());
    	    setModifiedBy(mb.getModifiedBy());
        }
        return mb;
    }    
    
    public void setMemberDetails() {    	
        try
        {        	
        	MemberAgentRObj memberAgentRObj = EJBUtil.getBoardMemberAgentHome();
        	this.setPkRBMember(memberAgentRObj.setMemberDetails(this.createMemberStateKeeper()));            
        }
        catch (Exception e)
        {
        	 Rlog.fatal("member", "MemberJB.setMemberDetails exception - " + e);            
        }      
    }
    
    public int updateMember() {
    	int output = 0;
        try
        {
        	MemberAgentRObj memberAgentRObj = EJBUtil.getBoardMemberAgentHome();
            output = memberAgentRObj.updateMember(this.createMemberStateKeeper());
            this.setPkRBMember(output);
        }
        catch (Exception e)
        {
            Rlog.fatal("member", "MemberJB.updateMember exception - " + e);
            return -2;
        }
        return output;
    }
    
    public MemberBean createMemberStateKeeper()
    {
        return new MemberBean(this.pkRBMember,this.fkBoardId,this.fkMemberRole,this.fkMemberUser,this.brdMemStatus,this.brdMemRights,this.ipAdd,this.creator,this.modifiedBy);
    }
    
    public MemberDao getMemberValues(int pkRBMember, int fkBoardId) {
        MemberDao memberDao = new MemberDao();
        try {
            MemberAgentRObj memberAgentRObj = EJBUtil.getBoardMemberAgentHome();
            memberDao = memberAgentRObj.getMemberValues(pkRBMember);
            return memberDao;
        } catch (Exception e) {
            Rlog.fatal("member","Error in getMemberValues(int pkRBMember,int fkBoardId) in MemberJB "+ e);
        }
        return memberDao;
    }    
    
    public int deleteFromBoard() {
        int ret = 0;
        try
        {
        	MemberAgentRObj memberAgentRObj =  EJBUtil.getBoardMemberAgentHome();
            ret = memberAgentRObj.deleteFromBoard(this.createMemberStateKeeper());
            return ret;
        }
        catch (Exception e)
        {
            Rlog.fatal("team", "MemberJB.deleteFromBoard exception - " + e);
            return -2;
        }
    }   
}