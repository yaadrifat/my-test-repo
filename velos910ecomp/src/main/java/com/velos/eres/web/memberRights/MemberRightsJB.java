package com.velos.eres.web.memberRights;

import java.util.ArrayList;

import com.velos.eres.business.memberRights.impl.MemberRightsBean;
import com.velos.eres.service.memberRightsAgent.MemberRightsAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

public class MemberRightsJB 
{	 
    private int id;

    private ArrayList grSeq;

    private ArrayList ftrRights;

    private ArrayList grValue;

    private ArrayList grDesc;
    
    private String ipAdd;
    
    private String creator;
    
    private String modifiedBy;
    
    // GETTER SETTER METHODS

    public int getId() {
        return this.id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    public ArrayList getGrSeq() {
        return this.grSeq;
    }
    public void setGrSeq(String seq) {
        grSeq.add(seq);
    } 
    public void setGrSeq(ArrayList seq) {
        this.grSeq = seq;
    }
    
    public ArrayList getFtrRights() {
        return this.ftrRights;
    }
    public String getFtrRightsBySeq(String seq) {               
    	if (this.ftrRights != null && this.ftrRights.size() > 0)
    	{
    		return this.ftrRights.get(Integer.parseInt(seq) - 1).toString();
    	}
    	else
    	{
    		return "0";
    	}
    }
    
    public String getFtrRightsByValue(String val) {
    	if (grValue != null && grValue.size()> 0 )
    	{
    		int seq = grValue.indexOf(val);
    		return this.ftrRights.get(seq).toString();
    	}else
    	{
    		return "0";
    	}
    }

    public void setFtrRights(String rights) {
        ftrRights.add(rights);
    }
    public void setFtrRights(ArrayList rights) {
        this.ftrRights = rights;
    }

    public ArrayList getGrValue() {
        return this.grValue;
    }
    public String getGrValueBySeq(String seq) {
        int count = Integer.parseInt(seq) - 1;
        return this.grValue.get(count).toString();
    }

    public void setGrValue(String val) {
        grValue.add(val);
    }
    public void setGrValue(ArrayList val) {
        this.grValue = val;
    }

    public ArrayList getGrDesc() {
        return this.grDesc;
    }
    public void setGrDesc(String desc) {
        grDesc.add(desc);
    }
    public void setGrDesc(ArrayList desc) {
        this.grDesc = desc;
    }
    
    public String getGrDescBySeq(String seq) {
        int count = Integer.parseInt(seq) - 1;
        return this.grDesc.get(count).toString();
    }
    public String getGrDescByValue(String val) {
        int seq = grValue.indexOf(val);
        return this.grDesc.get(seq).toString();
    }
    
    public String getIpAdd() {
		return ipAdd;
	}
    
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
	
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	
	public String getModifiedBy() {
		return modifiedBy;
	}
	
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	//End Of Getter/Setter

    public MemberRightsJB(int id) {
        setId(id);
    }    

    public MemberRightsJB() {
        grSeq = new ArrayList();
        ftrRights = new ArrayList();
        grValue = new ArrayList();
        grDesc = new ArrayList();
    }
    
    
    public void MemberRightsJB(int id, ArrayList grSeq, ArrayList ftrRights, ArrayList grValue, ArrayList grDesc,String ipAdd,String creator,String modifiedBy)
    {
        Rlog.debug("memRights", "memRightsJB Start constructor");
        setId(id);
        setGrSeq(grSeq);
        setFtrRights(ftrRights);
        setGrValue(grValue);
        setGrDesc(grDesc);  
        setIpAdd(ipAdd);
		setCreator(creator);
		setModifiedBy(modifiedBy);		
		Rlog.debug("reviewBoard", "MemberRightsJB full arguments constructor");
    }
    
    public MemberRightsBean getMemberRightsDetails() {
    	MemberRightsBean mrk = null;
        try {        	
        	MemberRightsAgentRObj memberRightsAgentRObj = EJBUtil.getMemberRightsAgentHome();
            mrk = memberRightsAgentRObj.getMemberRightsDetails(this.id);            
        } catch (Exception e) {
            Rlog.fatal("memRights", "Exception in getMemberRightsDetails" + e);
        }
        if (mrk != null) {
            this.id = mrk.getId();
            this.grSeq = mrk.getGrSeq();
            this.ftrRights = mrk.getFtrRights();
            this.grValue = mrk.getGrValue();
            this.grDesc = mrk.getGrDesc(); 
            this.ipAdd=mrk.getIpAdd();
    	    this.creator=mrk.getCreator();
    	    this.modifiedBy=mrk.getModifiedBy();
    	    
    	    setId(mrk.getId());
    	    setGrSeq(mrk.getGrSeq());
    	    setFtrRights(mrk.getFtrRights());
    	    setGrValue(mrk.getGrValue());
    	    setGrDesc(mrk.getGrDesc());    	      
    	    setIpAdd(mrk.getIpAdd());
    	    setCreator(mrk.getCreator());
    	    setModifiedBy(mrk.getModifiedBy());
        }
        return mrk;
    }
    
    public int updateMemberRights() {
        try {        	
            int result = 0;
            Rlog.debug("memberRights", "update Member Rights");
            MemberRightsAgentRObj memberRightsAgentRObj = EJBUtil.getMemberRightsAgentHome();
            result = memberRightsAgentRObj.updateMemberRights(this.createMemberRightsStateKeeper());
            return result;
        } catch (Exception e) {
            Rlog.fatal("memberRights", "EXCEPTION IN UPDATE Member RIGHTS"+e);
            return -2;
        }
    }    
    
    public MemberRightsBean createMemberRightsStateKeeper() {         	
        return new MemberRightsBean(this.id, this.grSeq, this.ftrRights, this.grValue, this.grDesc,this.ipAdd,this.creator,this.modifiedBy);
    } 
}