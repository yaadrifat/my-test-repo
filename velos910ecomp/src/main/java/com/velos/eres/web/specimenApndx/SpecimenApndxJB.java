package com.velos.eres.web.specimenApndx;


import java.util.Hashtable;

import com.velos.eres.business.common.SpecimenApndxDao;
import com.velos.eres.business.common.StorageAllowedItemsDao;
import com.velos.eres.business.specimenApndx.impl.SpecimenApndxBean;
import com.velos.eres.service.specimenApndxAgent.SpecimenApndxAgentRObj;
import com.velos.eres.service.storageAllowedItemsAgent.StorageAllowedItemsAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/**
 * Client side bean for the Patient Appendix
 */

public class SpecimenApndxJB {

    private int    specApndxId;

    private String fKSpecimen;
      
    private String specApndxDesc;
    
    private String specApndxUri;

    private String specApndxType;
    
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    // GETTER SETTER METHODS

    public int getSpecimenApndxId() {
        return specApndxId;
    }

    public void setSpecimenApndxId(int specApndxId) {
    	this.specApndxId = specApndxId;
    }

    public String getSpecFkSpecimen() {
        return this.fKSpecimen;
    }

    public void setSpecFkSpecimen(String fKSpecimen) {
        this.fKSpecimen = fKSpecimen;
    }

    public String getSpecApndxDesc() {
        return this.specApndxDesc;
    }

    public void setSpecApndxDesc(String specApndxDesc) {
        this.specApndxDesc = specApndxDesc;
    }

    public String getSpecApndxUri() {
        return this.specApndxUri;
    }

    public void setSpecApndxUri(String specApndxUri) {
        this.specApndxUri = specApndxUri;
    }

    public String getSpecApndxType() {
        return this.specApndxType;
    }

    public void setSpecApndxType(String specApndxType) {
        this.specApndxType = specApndxType;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    // CONSTRUCTOR TO CREATE patient appendix OBJ WITH ID

    public SpecimenApndxJB(String specApndxId) {
        setSpecimenApndxId(EJBUtil.stringToNum(specApndxId));
    }

    // DEFAULT CONSTRUCTOR

    public SpecimenApndxJB() {

        Rlog.debug("specimenapndx", "SpecimenApndx JB  : Default Constructor");

    }

    public SpecimenApndxBean getSpecimenApndxDetails() {
        SpecimenApndxBean pask = null;

        try {

            SpecimenApndxAgentRObj specimenApndxRObj = EJBUtil.getSpecimenApndxAgentHome();
            pask = specimenApndxRObj.getSpecimenApndxDetails(this.specApndxId);

        } catch (Exception e) {
            Rlog.debug("perapndx",
                    "Exception in the function getPerApndxDetails");
        }
        if (pask != null) {
        	specApndxId = pask.getSpecApndxId();
        	fKSpecimen    = pask.getFkSpecimen();
            specApndxDesc  = pask.getSpecApndxDesc();
            specApndxUri   = pask.getSpecApndxUri();
            specApndxType  = pask.getSpecApndxType();
            creator        = pask.getCreator();
            modifiedBy     = pask.getModifiedBy();
            ipAdd          = pask.getIpAdd();
        }
         return pask;
    }

    public int setSpecimenApndxDetails() {
    	
        int specApndxId = 0;
        try {
        	
    		
            SpecimenApndxAgentRObj specApndxAgentRobj = EJBUtil
                    .getSpecimenApndxAgentHome();
            SpecimenApndxBean sabean =  createSpecimenApndxStateKeeper();
             specApndxId = specApndxAgentRobj.setSpecimenApndxDetails(this
                    .createSpecimenApndxStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("specapndx", "Exception in setSpecimenApndxDetails()" + e);
            return -1;
        }

        return specApndxId;
    }


    public int removeSpecApndx(int specApndxId) {
    	int output = 0;
        try {

            SpecimenApndxAgentRObj specApndxAgentRobj = EJBUtil
                    .getSpecimenApndxAgentHome();
            output = specApndxAgentRobj.removeSpecApndx(specApndxId);

        } catch (Exception e) {
            Rlog.fatal("specapndx", "Exception in removeSpecApndx");
            return -1;
        }
        return output;
    }

    // removeSpecApndx() Overloaded for INF-18183 ::: Raviesh
    public int removeSpecApndx(int specApndxId,Hashtable<String, String> args) {
    	int output = 0;
        try {

            SpecimenApndxAgentRObj specApndxAgentRobj = EJBUtil
                    .getSpecimenApndxAgentHome();
            output = specApndxAgentRobj.removeSpecApndx(specApndxId,args);

        } catch (Exception e) {
            Rlog.fatal("specapndx", "Exception in removeSpecApndx");
            return -1;
        }
        return output;
    }
    
    public int updateSpecimenApndx() {
        int output = 0;
        try {

            SpecimenApndxAgentRObj specApndxAgentRObj = EJBUtil
                    .getSpecimenApndxAgentHome();
         
            
            
            
            output = specApndxAgentRObj.updateSpecimenApndx(this
                    .createSpecimenApndxStateKeeper());
            
        } catch (Exception e) {
            Rlog.fatal("specapndx", "Exception in updating SpecApndxDetails()");
            return -2;
        }

        return output;
    }

    public SpecimenApndxBean createSpecimenApndxStateKeeper() {
    	  return new SpecimenApndxBean(this.specApndxId,this.fKSpecimen,this.specApndxDesc,
        		this.specApndxUri, this.specApndxType, this.creator, this.modifiedBy, this.ipAdd);
    }
    
    public SpecimenApndxDao getSpecimenApndxUris(int pkSpecimen) {

    	SpecimenApndxDao specimenApndxDao = new SpecimenApndxDao();
    	try {
    		SpecimenApndxAgentRObj specApndxAgentRobj = EJBUtil
            .getSpecimenApndxAgentHome();
    		specimenApndxDao= specApndxAgentRobj.getSpecimenApndxUris(pkSpecimen);
            return specimenApndxDao;
           
        } catch (Exception e) {
            Rlog.fatal("specapndx", "Error in getSpecimenApndxUris() in StorageJB " + e);
        }
        return specimenApndxDao;
    }
    /**
     * 
     * 
     * generates a String of XML for the customer details entry form.<br>
     * <br>
     * 
     * 
     * Note that it is planned to encapsulate this detail in another object.
     * 
     * 
     */

    public String toXML()

    {
        return null;
    }

    // end of method

}// end of class
