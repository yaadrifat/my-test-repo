package com.velos.eres.web.moreDetails;



/* IMPORT STATEMENTS */

import java.util.ArrayList;

import com.velos.eres.business.common.MoreDetailsDao;
import com.velos.eres.business.common.StudyIdDao;
import com.velos.eres.business.moreDetails.impl.MoreDetailsBean;
import com.velos.eres.business.studyId.impl.StudyIdBean;
import com.velos.eres.service.moreDetailsAgent.MoreDetailsAgentRObj;
import com.velos.eres.service.studyIdAgent.StudyIdAgentRObj;
import com.velos.eres.service.studyIdAgent.impl.StudyIdAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */


public class MoreDetailsJB {

    

    private int id;

    /**
     * The foreign key reference to modules
     */
    private String modId;
    private String modName;

    /**
     * The Id Type
     */
 
  
    private String modElementId;

    /**
     * The Alternate Study Id
     */
    private String modElementData;

    private String creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    private String modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    private String ipAdd;

    // /////////////////////////////////////////////////////////////////////////////////

    // GETTER AND SETTER METHODS

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

   

    /**
	 * @return the modElementData
	 */
	public String getModElementData() {
		return modElementData;
	}

	/**
	 * @param modElementData the modElementData to set
	 */
	public void setModElementData(String modElementData) {
		this.modElementData = modElementData;
	}

	/**
	 * @return the modElementId
	 */
	public String getModElementId() {
		return modElementId;
	}

	/**
	 * @param modElementId the modElementId to set
	 */
	public void setModElementId(String modElementId) {
		this.modElementId = modElementId;
	}

	/**
	 * @return the modId
	 */
	public String getModId() {
		return modId;
	}

	/**
	 * @param modId the modId to set
	 */
	public void setModId(String modId) {
		this.modId = modId;
	}

	/**
	 * @return the modName
	 */
	public String getModName() {
		return modName;
	}

	/**
	 * @param modName the modName to set
	 */
	public void setModName(String modName) {
		this.modName = modName;
	}

	/**
     * @return The Creator of the Form Section
     */

    public String getCreator() {
        return this.creator;
    }

    /**
     * 
     * @param to
     *            set the Creator of the Form Section
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the IP Address of the Form Section request
     */

    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Section request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // //////////////////////////////////////////////////////////////////

    // END OF THE GETTER AND SETTER METHODS

   

    public MoreDetailsJB(int id) {
        setId(id);
    }

    /**
     * Default Constructor
     */

    public MoreDetailsJB() {
        
    }

    public MoreDetailsJB(int id, String modId, String modName,
            String modElementId, String modElementData,String creator, String modifiedBy,
            String ipAdd) {
    	setId(id);
        setModId(modId);
        setModName(modName);
        setModElementId(modElementId);
        setModElementData(modElementData);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }
    

   

    public MoreDetailsBean getMoreDetailsBean() {
        MoreDetailsBean mdb = null;
        try {
            MoreDetailsAgentRObj mdAgentRObj = EJBUtil.getMoreDetailsAgentHome();
            mdb = mdAgentRObj.getMoreDetailsBean(this.id);
            
        } catch (Exception e) {
            Rlog.fatal("moredetails", "Error in  getNoreDetails" + e);
            e.printStackTrace();
        }

        if (mdb != null) {
        	setId(mdb.getId());
            setModId(mdb.getModId());
            setModName(mdb.getModName());
            setModElementId(mdb.getModElementId());
            setModElementData(mdb.getModElementData());
            setCreator(mdb.getCreator());
            setModifiedBy(mdb.getModifiedBy());
            setIpAdd(mdb.getIpAdd());

        }

        return mdb;

    }

   
    public int setMoreDetails() {
        int toReturn;
        try {

            MoreDetailsAgentRObj mdAgentRObj = EJBUtil.getMoreDetailsAgentHome();

            MoreDetailsBean mdb= new MoreDetailsBean();

            mdb = this.createMoreDetailsStateKeeper();

            toReturn = mdAgentRObj.setMoreDetails(mdb);
            this.setId(toReturn);

           

            return toReturn;

        } catch (Exception e) {
            Rlog.fatal("moredetails", "Error in setMoreDetails() in MoreDetailsJB "
                    + e);
            e.printStackTrace();
            return -2;
        }
    }

    
    public MoreDetailsBean createMoreDetailsStateKeeper() {

        return new MoreDetailsBean(id,  modId,modName,
                modElementId,  modElementData,creator,  modifiedBy,
                 ipAdd);

    }

   
    public int updateMoreDetails() {
        int output;
        try {
            MoreDetailsAgentRObj mdAgentRObj = EJBUtil.getMoreDetailsAgentHome();
            output = mdAgentRObj.updateMoreDetails(this
                    .createMoreDetailsStateKeeper());
        } catch (Exception e) {
            Rlog
                    .debug("moredetails",
                            "EXCEPTION IN SETTING DETAILS TO  SESSION BEAN"
                                    + e);
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    public int removeMoreDetails() {

        int output;

        try {

            MoreDetailsAgentRObj mdAgentRObj = EJBUtil.getMoreDetailsAgentHome();
            output = mdAgentRObj.removeMoreDetails(this.id);
            return output;

        } catch (Exception e) {
            Rlog.fatal("moredetails", "in moredetailsJB -removeMoreDetails() method" + e);
            e.printStackTrace();
            return -1;
        }

    }

   

    public int createMultipleMoreDetails(MoreDetailsBean mdb , String defUserGroup ) {

        int ret = 0;

        try {
            
            MoreDetailsAgentRObj mdAgentRObj = EJBUtil.getMoreDetailsAgentHome();
            ret = mdAgentRObj.createMultipleMoreDetails(mdb,  defUserGroup );

            return ret;
        } catch (Exception e) {
            Rlog.fatal("moredetails", "createMultipleMoreDetails in MoreDetailsIdJB " + e);
            e.printStackTrace();
            return -2;
        }

    }

   
    public int updateMultipleMoreDetails(MoreDetailsBean mdb) {

        int ret = 0;

        try {
            
            MoreDetailsAgentRObj mdAgentRObj = EJBUtil.getMoreDetailsAgentHome();
            ret = mdAgentRObj.updateMultipleMoreDetails(mdb);

            return ret;
        } catch (Exception e) {
            Rlog.fatal("moredetails", "updateMultipleMoreDetails() in MoreDetailsJB " + e);
            e.printStackTrace();
            return -2;
        }

    }

    public MoreDetailsDao getMoreDetails(int studyId,String modName, String defUserGroup ) {

        MoreDetailsDao mdDao = new MoreDetailsDao();

        try {
          
            MoreDetailsAgentRObj mdAgentRObj = EJBUtil.getMoreDetailsAgentHome();

            mdDao = mdAgentRObj.getMoreDetails(studyId,modName, defUserGroup );

            return mdDao;
        } catch (Exception e) {
            Rlog.fatal("moredetails", "getMoreDetails() in MoreDetailsJB " + e);
            e.printStackTrace();
            return null;
        }

    }

    public MoreDetailsDao getEventMoreDetails(int studyId,String modName,String defUserGroup,int finDetRight) {
        MoreDetailsDao mdDao = new MoreDetailsDao();

        try {
            MoreDetailsAgentRObj mdAgentRObj = EJBUtil.getMoreDetailsAgentHome();
            mdDao = mdAgentRObj.getEventMoreDetails(studyId,modName,defUserGroup,finDetRight);
            return mdDao;
        } catch (Exception e) {
            Rlog.fatal("moredetails", "getEventMoreDetails() in MoreDetailsJB " + e);
            e.printStackTrace();
            return null;
        }

    }

}
