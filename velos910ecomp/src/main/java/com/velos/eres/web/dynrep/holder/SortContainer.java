package com.velos.eres.web.dynrep.holder;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Description of the Class
 * 
 * @author vabrol
 * @created October 25, 2004
 */
public class SortContainer implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 3257289149374477366L;

    ArrayList sortFields;

    ArrayList sortOrderBy;

    String sortStr;

    /**
     * Constructor for the SortHolder object
     */
    public SortContainer() {
        sortFields = new ArrayList();
        sortOrderBy = new ArrayList();
        sortStr = "";
    }

    /**
     * Returns the value of sortFields.
     * 
     * @return The sortFields value
     */
    public ArrayList getSortFields() {
        return sortFields;
    }

    /**
     * Sets the value of sortFields.
     * 
     * @param sortFields
     *            The value to assign sortFields.
     */
    public void setSortFields(ArrayList sortFields) {
        this.sortFields = sortFields;
    }

    /**
     * Sets the value of sortFields.
     * 
     * @param sortFields
     *            The value to assign sortFields.
     */
    public void setSortFields(String sortField) {
        this.sortFields.add(sortField);
    }

    /**
     * Returns the value of sortOrderBy.
     * 
     * @return The sortOrderBy value
     */
    public ArrayList getSortOrderBy() {
        return sortOrderBy;
    }

    /**
     * Sets the value of sortOrderBy.
     * 
     * @param sortOrderBy
     *            The value to assign sortOrderBy.
     */
    public void setSortOrderBy(ArrayList sortOrderBy) {
        this.sortOrderBy = sortOrderBy;
    }

    /**
     * Sets the value of sortOrderBy.
     * 
     * @param sortOrderBy
     *            The value to assign sortOrderBy.
     */
    public void setSortOrderBy(String orderBy) {
        this.sortOrderBy.add(orderBy);
    }

    /**
     * Returns the value of sortStr.
     * 
     * @return The sortStr value
     */
    public String getSortStr() {
        return sortStr;
    }

    /**
     * Sets the value of sortStr.
     * 
     * @param sortStr
     *            The value to assign sortStr.
     */
    public void setSortStr(String sortStr) {
        this.sortStr = sortStr;
    }

    public void reset() {
        sortFields.clear();
        sortOrderBy.clear();
    }

    public void Display() {

        System.out.println("sortFields: " + sortFields + "\n" + "sortOrderBy:"
                + sortOrderBy + "\n" + "sortStr:" + sortStr);

    }

}
