package com.velos.eres.tags;

import com.velos.eres.business.common.Style;

public class VLookup extends VTag {
    // /__astcwz_attrb_idt#(1466)

    private String displayText;

    private String lkpView;

    private String lkpFilter;

    private String lkpKeyword;

    private String lkpType;

    private Style displayStyle;

    // /__astcwz_default_constructor
    public VLookup() {
        this.lkpView = "";
        this.displayText = "";
        this.lkpFilter = "";
        this.lkpKeyword = "";
        this.lkpType = "";
        this.displayStyle = new Style();

    }

    public VLookup(String displayText, String lkpView, String lkpFilter,
            String lkpKeyword, Style displayStyle) {
        this.lkpView = lkpView;
        this.displayText = displayText;
        this.lkpFilter = lkpFilter;
        this.lkpKeyword = lkpKeyword;
        this.displayStyle = displayStyle;

    }

    public VLookup(String displayText) {
        this.displayText = displayText;
        this.displayStyle = new Style();
        this.lkpView = "";
        this.lkpFilter = "";
        this.lkpKeyword = "";

    }

    public Style getDisplayStyle() {
        return this.displayStyle;
    }

    public void setDisplayStyle(Style displayStyle) {
        this.displayStyle = displayStyle;
    }

    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    public void setLkpView(String lkpView) {
        this.lkpView = lkpView;
    }

    public void setLkpFilter(String lkpFilter) {
        this.lkpFilter = lkpFilter;
    }

    public void setLkpKeyword(String lkpKeyword) {
        this.lkpKeyword = lkpKeyword;
    }

    public void setLkpType(String lkptype) {
        this.lkpType = lkptype;
    }

    public String getDisplayText() {
        return displayText;
    }

    public String getLkpView() {
        return lkpView;
    }

    public String getLkpFilter() {
        return lkpFilter;
    }

    public String getLkpKeyword() {
        return lkpKeyword;
    }

    public String getLkpType() {
        return lkpType;
    }

    public String drawXSL() {

        StringBuffer xslStr = new StringBuffer();

        xslStr.append("<td><A ");
        // xslStr.append(" href= \"#\" ");
        // modified by Chn dev on 07Jan05
        xslStr.append(" href= \"Javascript:void(0)\" ");
        xslStr.append(" onClick=\"return openlookup(" + this.lkpView + ",'"
                + this.lkpFilter + "','" + this.lkpKeyword + "','"
                + this.lkpType + "')\"");
        xslStr.append(getTagClass());
        xslStr.append(getStyle());
        // xslStr.append(getTitle());
        xslStr.append(getToolTip());
        xslStr.append(getId());
        xslStr.append(" >");

        xslStr.append((getDisplayStyle()).getDisplayStartTags());
        xslStr.append(getDisplayText());
        xslStr.append((getDisplayStyle()).getDisplayEndTags());

        xslStr.append("</A>");
        xslStr.append(getHelpIcon());
        xslStr.append("  &#xa0;&#xa0;&#xa0;</td>");

        return xslStr.toString();

    }

    public String drawXSL(boolean withTD) {
        if (withTD) {
            StringBuffer xslStr = new StringBuffer();
            xslStr.append("<td " + ((getDisplayStyle()).getAlignVal()) + " >");
            xslStr.append(drawXSL());
            xslStr.append("</td>");
            return xslStr.toString();
        } else {
            return drawXSL();
        }

    }

}
