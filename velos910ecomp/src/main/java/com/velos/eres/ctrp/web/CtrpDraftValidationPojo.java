package com.velos.eres.ctrp.web;

public class CtrpDraftValidationPojo {
	private String label = null;
	private Boolean isSubHeading = false;
	private Boolean isMandatory = false;
	private String value = null;
	private String message = null;
	
	// Bug #8272 3rd point
	public CtrpDraftValidationPojo (String label, Boolean isSubHeading, Boolean isMandatory, 
			String value, String message) {
		this.setLabel(label);
		this.setIsMandatory(isMandatory);
		this.setValue(value);
		this.setMessage(message);
		this.setIsSubHeading(isSubHeading);
	}
	
	public CtrpDraftValidationPojo (String label, Boolean isMandatory, 
			String value, String message) {
		this.setLabel(label);
		this.setIsMandatory(isMandatory);
		this.setValue(value);
		this.setMessage(message);
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public void setIsMandatory(Boolean isMandatory) {
		this.isMandatory = isMandatory;
	}

	public Boolean getIsMandatory() {
		return isMandatory;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public Boolean getIsSubHeading() {
		return isSubHeading;
	}

	public void setIsSubHeading(Boolean isSubHeading) {
		this.isSubHeading = isSubHeading;
	}
	
}
