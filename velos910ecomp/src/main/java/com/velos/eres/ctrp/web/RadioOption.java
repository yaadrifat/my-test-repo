package com.velos.eres.ctrp.web;

public class RadioOption {
	
	public RadioOption(String d, Integer c) {
		setRadioDisplay(d);
		setRadioCode(c);
	}
	
	private String radioDisplay;
	private Integer radioCode;

	public void setRadioDisplay(String display) {
		this.radioDisplay = display;
	}
	
	public String getRadioDisplay() {
		return radioDisplay;
	}
	
	public void setRadioCode(Integer code) {
		this.radioCode = code;
	}
	
	public Integer getRadioCode() {
		return radioCode;
	}
}
