/*
 * Classname : CTRPDraftDocAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 12/20/2011
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Ashu Khatkar
 */

package com.velos.eres.ctrp.service;
/* Import Statements */
import java.util.Hashtable;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.studyNIHGrant.impl.StudyNIHGrantBean;
import com.velos.eres.ctrp.business.CTRPDraftDocBean;
import com.velos.eres.ctrp.business.CTRPDraftDocDao;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;


/**
 * The stateless session EJB acting as a facade for the entity CMP -
 * CTRPDraftDocAgentBean.<br>
 * <br>
 *
 * @author Ashu Khatkar
 * @see CTRPDraftDocAgentBean
 * @ejbRemote CTRPDraftDocAgent
 */
@Stateless
public class CTRPDraftDocAgentBean
  implements CTRPDraftDocAgent
{

  @PersistenceContext(unitName="eres")
  protected EntityManager em;

  @Resource
  private SessionContext context;
 
  /**
 *@param fkCtrpDraft
 *  return CTRPDraftDocDao
 */
  
  public CTRPDraftDocDao getCTRPDraftDocDetails(int fkCtrpDraft)
  {
	  try {
		  CTRPDraftDocDao ctrpDraftDocDao = new CTRPDraftDocDao();
	       	
		  ctrpDraftDocDao.getCtrpDocsValues(fkCtrpDraft);
	       	
	           return ctrpDraftDocDao;
	       } catch (Exception e) {
	           Rlog.fatal("ctrpDraftDoc", "Exception in getCTRPDraftDocDetails in CTRPDraftDocAgentBean " + e);
	       }
	       return null;
  }
  
  /**
   *@param ctrpDraftDocBean
   * return output
   */
  public Integer setCtrpDraftDocDetails(CTRPDraftDocBean ctrpDraftDocBean) {
	  Integer output = 0;
		try {
			em.persist(ctrpDraftDocBean);
			output = ctrpDraftDocBean.getPkCtrpDoc();
		} catch (Exception e) {
			Rlog.fatal("ctrpDraftDoc", "Exception in CTRPDraftDocAgentBean.setCtrpDraftDocDetails: "+e);
			output = -1;
		}
		return output;
	}
   
  /**
   *@param ctrpDraftDocBean
   * return output
   */
	public Integer updateCtrpDraftDocDetails(CTRPDraftDocBean ctrpDraftDocBean) {
		Integer output = 0;
		if (ctrpDraftDocBean.getPkCtrpDoc() < 1) { return output; }
		try {
			CTRPDraftDocBean originalBean = (CTRPDraftDocBean) em.find(CTRPDraftDocBean.class, ctrpDraftDocBean.getPkCtrpDoc());
			ctrpDraftDocBean.setCreator(originalBean.getCreator()); // Keep the original
			if (ctrpDraftDocBean.getIpAdd() == null) {
				ctrpDraftDocBean.setIpAdd(originalBean.getIpAdd()); // Keep the original if new is null
			}
			em.merge(ctrpDraftDocBean);
			output = ctrpDraftDocBean.getPkCtrpDoc();
		} catch (Exception e) {
			Rlog.fatal("ctrpDraftDoc", "Exception in CtrpDraftDocAgentBean.updateCtrpDraftDocDetails: "+e);
			output = -1;
		}
		return output;
	}
	
	/**
	 *  return int
	 */
	
	public int deleteCTRPDraftDocRecord(int ctrpDocId) {
		CTRPDraftDocBean retrieved = null;
	       int output = 0;
	       try {

	           retrieved = (CTRPDraftDocBean) em.find(CTRPDraftDocBean.class, new Integer(
	        		   ctrpDocId));
	           if (retrieved == null) {
	               return -2;
	           }            
	           em.remove(retrieved);	           
	       } catch (Exception e) {
	    	   context.setRollbackOnly();
	           Rlog.debug("ctrpDraftDoc", "Exception in deleteCTRPDraftDocRecord in CTRPDraftDocAgentBean"
	                   + e);
	           output = -2;
	       }
	      return output;
	   }

  
}