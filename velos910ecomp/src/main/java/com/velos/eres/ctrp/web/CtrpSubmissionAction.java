package com.velos.eres.ctrp.web;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class CtrpSubmissionAction extends ActionSupport  {
	private static final long serialVersionUID = -4944853697792820322L;
	private HttpServletRequest request = null;
	private static final String CTRP_SUBMISSION_BROWSER_TILE = "ctrpSubmissionBrowserTile";
	
	public CtrpSubmissionAction() {
		ActionContext ac = ActionContext.getContext();
		request = (HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	}
	
	public String getCtrpSubmissionBrowser() {
		return CTRP_SUBMISSION_BROWSER_TILE;
	}
}
